<s>
Meningitida	meningitida	k1gFnSc1	meningitida
čili	čili	k8xC	čili
zánět	zánět	k1gInSc1	zánět
mozkových	mozkový	k2eAgFnPc2d1	mozková
blan	blána	k1gFnPc2	blána
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc1	zánět
ochranných	ochranný	k2eAgFnPc2d1	ochranná
membrán	membrána	k1gFnPc2	membrána
pokrývajících	pokrývající	k2eAgInPc2d1	pokrývající
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
míchu	mícha	k1gFnSc4	mícha
<g/>
,	,	kIx,	,
známých	známý	k1gMnPc2	známý
pod	pod	k7c7	pod
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
mozkomíšní	mozkomíšní	k2eAgFnSc2d1	mozkomíšní
pleny	plena	k1gFnSc2	plena
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
infekce	infekce	k1gFnSc2	infekce
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
bakteriemi	bakterie	k1gFnPc7	bakterie
či	či	k8xC	či
jinými	jiný	k2eAgInPc7d1	jiný
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pak	pak	k6eAd1	pak
vlivem	vlivem	k7c2	vlivem
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
může	moct	k5eAaImIp3nS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
zánětu	zánět	k1gInSc3	zánět
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
míchy	mícha	k1gFnSc2	mícha
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nemoc	nemoc	k1gFnSc1	nemoc
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xC	jako
stav	stav	k1gInSc4	stav
vyžadující	vyžadující	k2eAgInSc4d1	vyžadující
akutní	akutní	k2eAgInSc4d1	akutní
zásah	zásah	k1gInSc4	zásah
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejčastějším	častý	k2eAgInPc3d3	nejčastější
symptomům	symptom	k1gInPc3	symptom
meningitidy	meningitida	k1gFnSc2	meningitida
patří	patřit	k5eAaImIp3nP	patřit
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
šíje	šíj	k1gFnSc2	šíj
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
zmateností	zmatenost	k1gFnSc7	zmatenost
nebo	nebo	k8xC	nebo
poruchami	porucha	k1gFnPc7	porucha
vědomí	vědomí	k1gNnPc2	vědomí
<g/>
,	,	kIx,	,
zvracením	zvracení	k1gNnSc7	zvracení
a	a	k8xC	a
neschopností	neschopnost	k1gFnSc7	neschopnost
snášet	snášet	k5eAaImF	snášet
světlo	světlo	k1gNnSc4	světlo
(	(	kIx(	(
<g/>
fotofobie	fotofobie	k1gFnSc2	fotofobie
<g/>
)	)	kIx)	)
či	či	k8xC	či
hlasité	hlasitý	k2eAgInPc4d1	hlasitý
zvuky	zvuk	k1gInPc4	zvuk
(	(	kIx(	(
<g/>
fonofobie	fonofobie	k1gFnPc4	fonofobie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
nespecifické	specifický	k2eNgInPc1d1	nespecifický
symptomy	symptom	k1gInPc1	symptom
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
podrážděnost	podrážděnost	k1gFnSc1	podrážděnost
a	a	k8xC	a
ospalost	ospalost	k1gFnSc1	ospalost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
vyrážka	vyrážka	k1gFnSc1	vyrážka
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
znamením	znamení	k1gNnSc7	znamení
specifické	specifický	k2eAgFnSc2d1	specifická
příčiny	příčina	k1gFnSc2	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
;	;	kIx,	;
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
vyrážka	vyrážka	k1gFnSc1	vyrážka
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
například	například	k6eAd1	například
meningitidu	meningitida	k1gFnSc4	meningitida
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
bakteriemi	bakterie	k1gFnPc7	bakterie
Neisseria	Neisserium	k1gNnSc2	Neisserium
meningitidis	meningitidis	k1gFnPc2	meningitidis
<g/>
.	.	kIx.	.
</s>
<s>
Diagnózu	diagnóza	k1gFnSc4	diagnóza
meningitidy	meningitida	k1gFnSc2	meningitida
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
nebo	nebo	k8xC	nebo
vyvrátí	vyvrátit	k5eAaPmIp3nS	vyvrátit
lumbální	lumbální	k2eAgFnSc1d1	lumbální
punkce	punkce	k1gFnSc1	punkce
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
páteřního	páteřní	k2eAgInSc2d1	páteřní
kanálu	kanál	k1gInSc2	kanál
lékař	lékař	k1gMnSc1	lékař
vpraví	vpravit	k5eAaPmIp3nS	vpravit
injekční	injekční	k2eAgFnSc4d1	injekční
jehlu	jehla	k1gFnSc4	jehla
a	a	k8xC	a
odebere	odebrat	k5eAaPmIp3nS	odebrat
vzorek	vzorek	k1gInSc1	vzorek
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
míchu	mícha	k1gFnSc4	mícha
<g/>
.	.	kIx.	.
</s>
<s>
Mozkomíšní	mozkomíšní	k2eAgInSc1d1	mozkomíšní
mok	mok	k1gInSc1	mok
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
podroben	podroben	k2eAgMnSc1d1	podroben
zkoumání	zkoumání	k1gNnSc4	zkoumání
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
akutní	akutní	k2eAgFnSc2d1	akutní
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
nasazení	nasazení	k1gNnSc4	nasazení
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
antivirotik	antivirotik	k1gMnSc1	antivirotik
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prevenci	prevence	k1gFnSc4	prevence
komplikací	komplikace	k1gFnPc2	komplikace
z	z	k7c2	z
rozsáhlého	rozsáhlý	k2eAgInSc2d1	rozsáhlý
zánětu	zánět	k1gInSc2	zánět
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
kortikosteroidy	kortikosteroid	k1gInPc1	kortikosteroid
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
závažným	závažný	k2eAgInPc3d1	závažný
dlouhodobým	dlouhodobý	k2eAgInPc3d1	dlouhodobý
důsledkům	důsledek	k1gInPc3	důsledek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
ztráta	ztráta	k1gFnSc1	ztráta
sluchu	sluch	k1gInSc2	sluch
<g/>
,	,	kIx,	,
epilepsie	epilepsie	k1gFnSc1	epilepsie
<g/>
,	,	kIx,	,
hydrocefalus	hydrocefalus	k1gInSc1	hydrocefalus
a	a	k8xC	a
kognitivní	kognitivní	k2eAgFnPc1d1	kognitivní
poruchy	porucha	k1gFnPc1	porucha
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
léčba	léčba	k1gFnSc1	léčba
zahájena	zahájit	k5eAaPmNgFnS	zahájit
urychleně	urychleně	k6eAd1	urychleně
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
formám	forma	k1gFnPc3	forma
meningitidy	meningitida	k1gFnSc2	meningitida
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
těm	ten	k3xDgInPc3	ten
spojeným	spojený	k2eAgInPc3d1	spojený
s	s	k7c7	s
infekcí	infekce	k1gFnSc7	infekce
meningokoky	meningokok	k1gInPc7	meningokok
<g/>
,	,	kIx,	,
bakterií	bakterie	k1gFnSc7	bakterie
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
typu	typ	k1gInSc2	typ
B	B	kA	B
<g/>
,	,	kIx,	,
pneumokoky	pneumokok	k1gInPc7	pneumokok
nebo	nebo	k8xC	nebo
virem	vir	k1gInSc7	vir
příušnic	příušnice	k1gFnPc2	příušnice
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
předejít	předejít	k5eAaPmF	předejít
imunizací	imunizace	k1gFnSc7	imunizace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
je	být	k5eAaImIp3nS	být
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
příznakem	příznak	k1gInSc7	příznak
meningitidy	meningitida	k1gFnSc2	meningitida
prudká	prudký	k2eAgFnSc1d1	prudká
bolest	bolest	k1gFnSc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c4	v
téměř	téměř	k6eAd1	téměř
90	[number]	k4	90
%	%	kIx~	%
případů	případ	k1gInPc2	případ
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
a	a	k8xC	a
opozice	opozice	k1gFnSc1	opozice
šíje	šíje	k1gFnSc1	šíje
(	(	kIx(	(
<g/>
neschopnost	neschopnost	k1gFnSc1	neschopnost
ohnout	ohnout	k5eAaPmF	ohnout
krk	krk	k1gInSc4	krk
pasivně	pasivně	k6eAd1	pasivně
vpřed	vpřed	k6eAd1	vpřed
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvýšení	zvýšení	k1gNnSc2	zvýšení
svalového	svalový	k2eAgNnSc2d1	svalové
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
ztuhlosti	ztuhlost	k1gFnSc2	ztuhlost
šíje	šíj	k1gFnSc2	šíj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
trio	trio	k1gNnSc1	trio
diagnostických	diagnostický	k2eAgInPc2d1	diagnostický
příznaků	příznak	k1gInPc2	příznak
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
náhlou	náhlý	k2eAgFnSc4d1	náhlá
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
změny	změna	k1gFnPc4	změna
psychického	psychický	k2eAgInSc2d1	psychický
stavu	stav	k1gInSc2	stav
<g/>
;	;	kIx,	;
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
prvky	prvek	k1gInPc1	prvek
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
přítomny	přítomen	k2eAgInPc4d1	přítomen
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
44	[number]	k4	44
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
%	%	kIx~	%
případů	případ	k1gInPc2	případ
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
u	u	k7c2	u
nemocného	nemocný	k1gMnSc2	nemocný
neobjevil	objevit	k5eNaPmAgInS	objevit
ani	ani	k9	ani
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
tří	tři	k4xCgInPc2	tři
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1	nepravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc4d1	další
symptomy	symptom	k1gInPc4	symptom
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
u	u	k7c2	u
meningitidy	meningitida	k1gFnSc2	meningitida
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
fotofobii	fotofobie	k1gFnSc4	fotofobie
(	(	kIx(	(
<g/>
nesnášenlivost	nesnášenlivost	k1gFnSc1	nesnášenlivost
jasného	jasný	k2eAgNnSc2d1	jasné
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
fonofobii	fonofobie	k1gFnSc4	fonofobie
(	(	kIx(	(
<g/>
nesnášenlivost	nesnášenlivost	k1gFnSc4	nesnášenlivost
hlasitých	hlasitý	k2eAgInPc2d1	hlasitý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgInPc1d1	uvedený
příznaky	příznak	k1gInPc1	příznak
často	často	k6eAd1	často
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
;	;	kIx,	;
patrná	patrný	k2eAgFnSc1d1	patrná
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
podrážděnost	podrážděnost	k1gFnSc4	podrážděnost
a	a	k8xC	a
nezdravý	zdravý	k2eNgInSc4d1	nezdravý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kojenců	kojenec	k1gMnPc2	kojenec
mladších	mladý	k2eAgInPc2d2	mladší
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
výduti	výduť	k1gFnSc3	výduť
fontanely	fontanela	k1gFnSc2	fontanela
(	(	kIx(	(
<g/>
měkké	měkký	k2eAgNnSc1d1	měkké
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
hlavy	hlava	k1gFnSc2	hlava
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
rysům	rys	k1gInPc3	rys
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
meningitidu	meningitida	k1gFnSc4	meningitida
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
závažných	závažný	k2eAgFnPc2d1	závažná
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
bolesti	bolest	k1gFnPc1	bolest
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
studené	studený	k2eAgFnPc1d1	studená
končetiny	končetina	k1gFnPc1	končetina
a	a	k8xC	a
abnormální	abnormální	k2eAgFnSc1d1	abnormální
barva	barva	k1gFnSc1	barva
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
ztuhnutí	ztuhnutí	k1gNnSc3	ztuhnutí
šíje	šíj	k1gFnSc2	šíj
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
70	[number]	k4	70
%	%	kIx~	%
případů	případ	k1gInPc2	případ
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
symptomy	symptom	k1gInPc7	symptom
meningismu	meningismus	k1gInSc2	meningismus
jsou	být	k5eAaImIp3nP	být
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
Kernigův	Kernigův	k2eAgInSc4d1	Kernigův
příznak	příznak	k1gInSc4	příznak
či	či	k8xC	či
Brudzińského	Brudzińského	k2eAgInSc4d1	Brudzińského
příznak	příznak	k1gInSc4	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Kernigův	Kernigův	k2eAgInSc1d1	Kernigův
příznak	příznak	k1gInSc1	příznak
je	být	k5eAaImIp3nS	být
posuzován	posuzovat	k5eAaImNgInS	posuzovat
u	u	k7c2	u
osoby	osoba	k1gFnSc2	osoba
ležící	ležící	k2eAgInSc4d1	ležící
naznak	naznak	k6eAd1	naznak
s	s	k7c7	s
kyčlemi	kyčel	k1gFnPc7	kyčel
a	a	k8xC	a
koleny	koleno	k1gNnPc7	koleno
ohnutými	ohnutý	k2eAgInPc7d1	ohnutý
v	v	k7c6	v
úhlu	úhel	k1gInSc6	úhel
90	[number]	k4	90
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osoby	osoba	k1gFnSc2	osoba
s	s	k7c7	s
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
Kernigovým	Kernigův	k2eAgInSc7d1	Kernigův
příznakem	příznak	k1gInSc7	příznak
není	být	k5eNaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
bolesti	bolest	k1gFnSc3	bolest
možné	možný	k2eAgNnSc1d1	možné
pasivní	pasivní	k2eAgNnSc1d1	pasivní
natažení	natažení	k1gNnSc1	natažení
kolene	kolen	k1gMnSc5	kolen
<g/>
.	.	kIx.	.
</s>
<s>
Brudzińského	Brudzińského	k2eAgInSc1d1	Brudzińského
příznak	příznak	k1gInSc1	příznak
je	být	k5eAaImIp3nS	být
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
<g/>
,	,	kIx,	,
když	když	k8xS	když
ohnutí	ohnutí	k1gNnSc1	ohnutí
šíje	šíj	k1gFnSc2	šíj
působí	působit	k5eAaImIp3nS	působit
mimovolní	mimovolní	k2eAgNnSc1d1	mimovolní
ohnutí	ohnutí	k1gNnSc1	ohnutí
kolene	kolen	k1gInSc5	kolen
a	a	k8xC	a
kyčle	kyčel	k1gFnSc2	kyčel
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Kernigův	Kernigův	k2eAgInSc1d1	Kernigův
a	a	k8xC	a
Brudzińského	Brudzińského	k2eAgInSc1d1	Brudzińského
příznak	příznak	k1gInSc1	příznak
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
výskytu	výskyt	k1gInSc2	výskyt
meningitidy	meningitida	k1gFnSc2	meningitida
používají	používat	k5eAaImIp3nP	používat
<g/>
,	,	kIx,	,
senzitivita	senzitivita	k1gFnSc1	senzitivita
obou	dva	k4xCgInPc2	dva
testů	test	k1gInPc2	test
je	být	k5eAaImIp3nS	být
omezená	omezený	k2eAgFnSc1d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
meningitidu	meningitida	k1gFnSc4	meningitida
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgInPc1	dva
testy	test	k1gInPc1	test
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
specificitu	specificita	k1gFnSc4	specificita
-	-	kIx~	-
uvedené	uvedený	k2eAgInPc1d1	uvedený
symptomy	symptom	k1gInPc1	symptom
se	se	k3xPyFc4	se
u	u	k7c2	u
jiných	jiný	k2eAgFnPc2d1	jiná
chorob	choroba	k1gFnPc2	choroba
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
test	test	k1gInSc1	test
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
"	"	kIx"	"
<g/>
jolt	jolt	k2eAgInSc1d1	jolt
accentuation	accentuation	k1gInSc1	accentuation
maneuver	maneuvra	k1gFnPc2	maneuvra
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyskytla	vyskytnout	k5eAaPmAgFnS	vyskytnout
horečka	horečka	k1gFnSc1	horečka
a	a	k8xC	a
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nP	trpět
meningitidou	meningitida	k1gFnSc7	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
je	být	k5eAaImIp3nS	být
požádán	požádán	k2eAgMnSc1d1	požádán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
rychle	rychle	k6eAd1	rychle
v	v	k7c6	v
horizontálním	horizontální	k2eAgInSc6d1	horizontální
směru	směr	k1gInSc6	směr
otáčel	otáčet	k5eAaImAgMnS	otáčet
hlavou	hlava	k1gFnSc7	hlava
-	-	kIx~	-
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
nevede	vést	k5eNaImIp3nS	vést
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
bolesti	bolest	k1gFnSc2	bolest
<g/>
,	,	kIx,	,
meningitida	meningitida	k1gFnSc1	meningitida
není	být	k5eNaImIp3nS	být
pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
způsobená	způsobený	k2eAgFnSc1d1	způsobená
bakterií	bakterie	k1gFnSc7	bakterie
Neisseria	Neisserium	k1gNnSc2	Neisserium
meningitidis	meningitidis	k1gFnSc2	meningitidis
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
meningokoková	meningokokový	k2eAgFnSc1d1	meningokoková
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
od	od	k7c2	od
meningitidy	meningitida	k1gFnSc2	meningitida
způsobené	způsobený	k2eAgInPc1d1	způsobený
jinými	jiný	k2eAgInPc7d1	jiný
činiteli	činitel	k1gInPc7	činitel
odlišena	odlišit	k5eAaPmNgFnS	odlišit
díky	dík	k1gInPc7	dík
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgFnSc3d1	šířící
petechiální	petechiální	k2eAgFnSc3d1	petechiální
vyrážce	vyrážka	k1gFnSc3	vyrážka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
může	moct	k5eAaImIp3nS	moct
předcházet	předcházet	k5eAaImF	předcházet
dalším	další	k2eAgInPc3d1	další
symptomům	symptom	k1gInPc3	symptom
<g/>
.	.	kIx.	.
</s>
<s>
Vyrážku	vyrážka	k1gFnSc4	vyrážka
tvoří	tvořit	k5eAaImIp3nS	tvořit
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
malých	malý	k2eAgFnPc2d1	malá
<g/>
,	,	kIx,	,
nepravidelných	pravidelný	k2eNgFnPc2d1	nepravidelná
fialových	fialový	k2eAgFnPc2d1	fialová
nebo	nebo	k8xC	nebo
červených	červený	k2eAgFnPc2d1	červená
skvrnek	skvrnka	k1gFnPc2	skvrnka
(	(	kIx(	(
<g/>
petechií	petechie	k1gFnPc2	petechie
<g/>
)	)	kIx)	)
na	na	k7c6	na
trupu	trup	k1gInSc6	trup
<g/>
,	,	kIx,	,
dolních	dolní	k2eAgFnPc6d1	dolní
končetinách	končetina	k1gFnPc6	končetina
<g/>
,	,	kIx,	,
sliznicích	sliznice	k1gFnPc6	sliznice
<g/>
,	,	kIx,	,
spojivkách	spojivka	k1gFnPc6	spojivka
a	a	k8xC	a
(	(	kIx(	(
<g/>
občas	občas	k6eAd1	občas
<g/>
)	)	kIx)	)
na	na	k7c6	na
dlaních	dlaň	k1gFnPc6	dlaň
rukou	ruka	k1gFnPc6	ruka
nebo	nebo	k8xC	nebo
ploskách	ploska	k1gFnPc6	ploska
chodidel	chodidlo	k1gNnPc2	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrážka	vyrážka	k1gFnSc1	vyrážka
obvykle	obvykle	k6eAd1	obvykle
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
i	i	k9	i
po	po	k7c6	po
zatlačení	zatlačení	k1gNnSc6	zatlačení
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
výskytu	výskyt	k1gInSc2	výskyt
-	-	kIx~	-
červené	červený	k2eAgFnPc1d1	červená
skvrny	skvrna	k1gFnPc1	skvrna
nemizí	mizet	k5eNaImIp3nP	mizet
ani	ani	k8xC	ani
při	pře	k1gFnSc4	pře
tlaku	tlak	k1gInSc2	tlak
prstem	prst	k1gInSc7	prst
či	či	k8xC	či
sklenicí	sklenice	k1gFnSc7	sklenice
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
vyrážka	vyrážka	k1gFnSc1	vyrážka
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
u	u	k7c2	u
meningokokové	meningokokový	k2eAgFnSc2d1	meningokoková
meningitidy	meningitida	k1gFnSc2	meningitida
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
chorobu	choroba	k1gFnSc4	choroba
relativně	relativně	k6eAd1	relativně
specifická	specifický	k2eAgFnSc1d1	specifická
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
ale	ale	k9	ale
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
i	i	k9	i
u	u	k7c2	u
meningitidy	meningitida	k1gFnSc2	meningitida
způsobené	způsobený	k2eAgInPc1d1	způsobený
jinými	jiný	k2eAgFnPc7d1	jiná
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
příčinách	příčina	k1gFnPc6	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
mohou	moct	k5eAaImIp3nP	moct
dále	daleko	k6eAd2	daleko
svědčit	svědčit	k5eAaImF	svědčit
i	i	k9	i
další	další	k2eAgInPc4d1	další
kožní	kožní	k2eAgInPc4d1	kožní
projevy	projev	k1gInPc4	projev
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nemoc	nemoc	k1gFnSc4	nemoc
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
nohou	noha	k1gFnPc2	noha
a	a	k8xC	a
úst	ústa	k1gNnPc2	ústa
nebo	nebo	k8xC	nebo
genitální	genitální	k2eAgInSc1d1	genitální
herpes	herpes	k1gInSc1	herpes
<g/>
;	;	kIx,	;
oba	dva	k4xCgInPc1	dva
symptomy	symptom	k1gInPc1	symptom
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
formami	forma	k1gFnPc7	forma
virové	virový	k2eAgFnSc2d1	virová
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
fázích	fáze	k1gFnPc6	fáze
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
další	další	k2eAgFnPc4d1	další
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
specifickou	specifický	k2eAgFnSc4d1	specifická
léčbu	léčba	k1gFnSc4	léčba
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
znakem	znak	k1gInSc7	znak
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
choroba	choroba	k1gFnSc1	choroba
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
závažná	závažný	k2eAgFnSc1d1	závažná
nebo	nebo	k8xC	nebo
že	že	k8xS	že
prognóza	prognóza	k1gFnSc1	prognóza
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
může	moct	k5eAaImIp3nS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
sepsi	sepse	k1gFnSc4	sepse
<g/>
,	,	kIx,	,
syndrom	syndrom	k1gInSc4	syndrom
systémové	systémový	k2eAgFnSc2d1	systémová
zánětové	zánětový	k2eAgFnSc2d1	zánětová
odpovědi	odpověď	k1gFnSc2	odpověď
nebo	nebo	k8xC	nebo
pokles	pokles	k1gInSc1	pokles
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
zrychlenou	zrychlený	k2eAgFnSc4d1	zrychlená
srdeční	srdeční	k2eAgFnSc4d1	srdeční
frekvenci	frekvence	k1gFnSc4	frekvence
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc4d1	vysoká
či	či	k8xC	či
abnormálně	abnormálně	k6eAd1	abnormálně
nízkou	nízký	k2eAgFnSc4d1	nízká
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
zrychlené	zrychlený	k2eAgNnSc4d1	zrychlené
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
může	moct	k5eAaImIp3nS	moct
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
značně	značně	k6eAd1	značně
poklesnout	poklesnout	k5eAaPmF	poklesnout
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
ne	ne	k9	ne
ale	ale	k9	ale
výhradně	výhradně	k6eAd1	výhradně
<g/>
,	,	kIx,	,
u	u	k7c2	u
meningokokové	meningokokový	k2eAgFnSc2d1	meningokoková
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
nedostatečného	dostatečný	k2eNgInSc2d1	nedostatečný
přísunu	přísun	k1gInSc2	přísun
krve	krev	k1gFnSc2	krev
dalším	další	k2eAgInPc3d1	další
orgánům	orgán	k1gInPc3	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Diseminovaná	Diseminovaný	k2eAgFnSc1d1	Diseminovaná
intravaskulární	intravaskulární	k2eAgFnSc1d1	intravaskulární
koagulace	koagulace	k1gFnSc1	koagulace
<g/>
,	,	kIx,	,
nadměrná	nadměrný	k2eAgFnSc1d1	nadměrná
aktivace	aktivace	k1gFnSc1	aktivace
srážení	srážení	k1gNnSc2	srážení
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
překážkou	překážka	k1gFnSc7	překážka
průtoku	průtok	k1gInSc2	průtok
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
paradoxně	paradoxně	k6eAd1	paradoxně
zvýšit	zvýšit	k5eAaPmF	zvýšit
riziko	riziko	k1gNnSc4	riziko
krvácení	krvácení	k1gNnSc2	krvácení
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
meningokokových	meningokokový	k2eAgNnPc2d1	meningokokové
onemocnění	onemocnění	k1gNnPc2	onemocnění
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
i	i	k9	i
ke	k	k7c3	k
gangréně	gangréna	k1gFnSc3	gangréna
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Závažné	závažný	k2eAgFnPc1d1	závažná
meningokokové	meningokokový	k2eAgFnPc1d1	meningokoková
a	a	k8xC	a
pneumokokové	pneumokokový	k2eAgFnPc1d1	pneumokoková
infekce	infekce	k1gFnPc1	infekce
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
krvácení	krvácení	k1gNnSc3	krvácení
do	do	k7c2	do
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
Waterhousova-Friderichsenova	Waterhousova-Friderichsenův	k2eAgInSc2d1	Waterhousova-Friderichsenův
syndromu	syndrom	k1gInSc2	syndrom
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
způsobujícího	způsobující	k2eAgNnSc2d1	způsobující
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Dojít	dojít	k5eAaPmF	dojít
může	moct	k5eAaImIp3nS	moct
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
otoku	otok	k1gInSc3	otok
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
tlaku	tlak	k1gInSc2	tlak
uvnitř	uvnitř	k7c2	uvnitř
lebky	lebka	k1gFnSc2	lebka
a	a	k8xC	a
následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
k	k	k7c3	k
vyhřeznutí	vyhřeznutí	k1gNnSc3	vyhřeznutí
oteklého	oteklý	k2eAgInSc2d1	oteklý
mozku	mozek	k1gInSc2	mozek
spodinou	spodina	k1gFnSc7	spodina
lebeční	lebeční	k2eAgFnPc1d1	lebeční
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
ve	v	k7c6	v
snížené	snížený	k2eAgFnSc6d1	snížená
úrovni	úroveň	k1gFnSc6	úroveň
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
absenci	absence	k1gFnSc4	absence
zornicového	zornicový	k2eAgInSc2d1	zornicový
reflexu	reflex	k1gInSc2	reflex
a	a	k8xC	a
abnormálním	abnormální	k2eAgNnSc6d1	abnormální
držení	držení	k1gNnSc6	držení
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
bránit	bránit	k5eAaImF	bránit
normálnímu	normální	k2eAgInSc3d1	normální
průtoku	průtok	k1gInSc3	průtok
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
hydrocefalus	hydrocefalus	k1gInSc1	hydrocefalus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
příčin	příčina	k1gFnPc2	příčina
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
záchvaty	záchvat	k1gInPc1	záchvat
<g/>
;	;	kIx,	;
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
stadiích	stadion	k1gNnPc6	stadion
meningitidy	meningitida	k1gFnSc2	meningitida
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
běžné	běžný	k2eAgFnPc1d1	běžná
(	(	kIx(	(
<g/>
ve	v	k7c6	v
30	[number]	k4	30
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
naznačovat	naznačovat	k5eAaImF	naznačovat
příčinu	příčina	k1gFnSc4	příčina
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
vlivem	vliv	k1gInSc7	vliv
zanícených	zanícený	k2eAgFnPc2d1	zanícená
oblastí	oblast	k1gFnPc2	oblast
mozkové	mozkový	k2eAgFnSc2d1	mozková
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Fokální	fokální	k2eAgInPc1d1	fokální
záchvaty	záchvat	k1gInPc1	záchvat
(	(	kIx(	(
<g/>
záchvaty	záchvat	k1gInPc1	záchvat
postihující	postihující	k2eAgFnSc4d1	postihující
jednu	jeden	k4xCgFnSc4	jeden
končetinu	končetina	k1gFnSc4	končetina
či	či	k8xC	či
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přetrvávající	přetrvávající	k2eAgInPc4d1	přetrvávající
záchvaty	záchvat	k1gInPc4	záchvat
<g/>
,	,	kIx,	,
záchvaty	záchvat	k1gInPc4	záchvat
s	s	k7c7	s
pozdním	pozdní	k2eAgInSc7d1	pozdní
nástupem	nástup	k1gInSc7	nástup
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
při	při	k7c6	při
užití	užití	k1gNnSc6	užití
léků	lék	k1gInPc2	lék
těžko	těžko	k6eAd1	těžko
kontrolovatelné	kontrolovatelný	k2eAgFnPc1d1	kontrolovatelná
<g/>
,	,	kIx,	,
signalizují	signalizovat	k5eAaImIp3nP	signalizovat
nepříliš	příliš	k6eNd1	příliš
dobrý	dobrý	k2eAgInSc4d1	dobrý
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
výhled	výhled	k1gInSc4	výhled
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
anomáliím	anomálie	k1gFnPc3	anomálie
kraniálních	kraniální	k2eAgMnPc2d1	kraniální
nervů	nerv	k1gInPc2	nerv
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnPc1	skupina
nervů	nerv	k1gInPc2	nerv
vedoucích	vedoucí	k2eAgInPc2d1	vedoucí
z	z	k7c2	z
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmene	kmen	k1gInSc2	kmen
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
šíje	šíj	k1gFnSc2	šíj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc1d1	jiný
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
pohyby	pohyb	k1gInPc1	pohyb
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
obličejové	obličejový	k2eAgInPc4d1	obličejový
svaly	sval	k1gInPc4	sval
a	a	k8xC	a
sluch	sluch	k1gInSc4	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
viděním	vidění	k1gNnSc7	vidění
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
sluchu	sluch	k1gInSc2	sluch
mohou	moct	k5eAaImIp3nP	moct
přetrvat	přetrvat	k5eAaPmF	přetrvat
i	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
encefalitida	encefalitida	k1gFnSc1	encefalitida
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mozkových	mozkový	k2eAgFnPc2d1	mozková
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
(	(	kIx(	(
<g/>
cerebrální	cerebrální	k2eAgFnSc1d1	cerebrální
vaskulitida	vaskulitida	k1gFnSc1	vaskulitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
vznik	vznik	k1gInSc4	vznik
krevních	krevní	k2eAgFnPc2d1	krevní
sraženin	sraženina	k1gFnPc2	sraženina
v	v	k7c6	v
cévách	céva	k1gFnPc6	céva
(	(	kIx(	(
<g/>
mozková	mozkový	k2eAgFnSc1d1	mozková
žilní	žilní	k2eAgFnSc1d1	žilní
trombóza	trombóza	k1gFnSc1	trombóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
slabost	slabost	k1gFnSc4	slabost
<g/>
,	,	kIx,	,
ztrátu	ztráta	k1gFnSc4	ztráta
citlivosti	citlivost	k1gFnSc2	citlivost
či	či	k8xC	či
abnormální	abnormální	k2eAgInSc4d1	abnormální
pohyb	pohyb	k1gInSc4	pohyb
nebo	nebo	k8xC	nebo
funkci	funkce	k1gFnSc4	funkce
části	část	k1gFnSc2	část
těla	tělo	k1gNnSc2	tělo
řízené	řízený	k2eAgNnSc1d1	řízené
postiženou	postižený	k2eAgFnSc7d1	postižená
oblastí	oblast	k1gFnSc7	oblast
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
způsobena	způsobit	k5eAaPmNgFnS	způsobit
infekcí	infekce	k1gFnSc7	infekce
přenesenou	přenesený	k2eAgFnSc7d1	přenesená
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
infekcí	infekce	k1gFnPc2	infekce
zapříčiňují	zapříčiňovat	k5eAaImIp3nP	zapříčiňovat
viry	vir	k1gInPc1	vir
<g/>
;	;	kIx,	;
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
plísně	plíseň	k1gFnPc1	plíseň
a	a	k8xC	a
prvoci	prvok	k1gMnPc1	prvok
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
další	další	k2eAgFnSc7d1	další
častou	častý	k2eAgFnSc7d1	častá
příčinou	příčina	k1gFnSc7	příčina
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
i	i	k8xC	i
neinfekčního	infekční	k2eNgInSc2d1	neinfekční
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
aseptická	aseptický	k2eAgFnSc1d1	aseptická
(	(	kIx(	(
<g/>
nehnisavá	hnisavý	k2eNgFnSc1d1	nehnisavá
<g/>
)	)	kIx)	)
meningitida	meningitida	k1gFnSc1	meningitida
označuje	označovat	k5eAaImIp3nS	označovat
ty	ten	k3xDgInPc4	ten
případy	případ	k1gInPc4	případ
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
nebyla	být	k5eNaImAgFnS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
meningitidy	meningitida	k1gFnSc2	meningitida
obvykle	obvykle	k6eAd1	obvykle
působí	působit	k5eAaImIp3nP	působit
viry	vir	k1gInPc1	vir
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
po	po	k7c6	po
prodělané	prodělaný	k2eAgFnSc6d1	prodělaná
bakteriální	bakteriální	k2eAgFnSc6d1	bakteriální
infekci	infekce	k1gFnSc6	infekce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
částečně	částečně	k6eAd1	částečně
vyléčena	vyléčen	k2eAgFnSc1d1	vyléčena
<g/>
,	,	kIx,	,
když	když	k8xS	když
bakterie	bakterie	k1gFnSc1	bakterie
z	z	k7c2	z
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
zmizely	zmizet	k5eAaPmAgFnP	zmizet
<g/>
;	;	kIx,	;
patogenní	patogenní	k2eAgFnPc1d1	patogenní
látky	látka	k1gFnPc1	látka
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
infikovat	infikovat	k5eAaBmF	infikovat
prostor	prostor	k1gInSc4	prostor
kolem	kolem	k7c2	kolem
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
sinusitidě	sinusitida	k1gFnSc6	sinusitida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
aseptické	aseptický	k2eAgFnSc2d1	aseptická
meningitidy	meningitida	k1gFnSc2	meningitida
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
endokarditida	endokarditida	k1gFnSc1	endokarditida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
srdečních	srdeční	k2eAgFnPc2d1	srdeční
chlopní	chlopeň	k1gFnPc2	chlopeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozsévání	rozsévání	k1gNnSc3	rozsévání
malých	malý	k2eAgInPc2d1	malý
shluků	shluk	k1gInPc2	shluk
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
řečišti	řečiště	k1gNnSc6	řečiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aseptická	aseptický	k2eAgFnSc1d1	aseptická
meningitida	meningitida	k1gFnSc1	meningitida
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
také	také	k9	také
z	z	k7c2	z
infekce	infekce	k1gFnSc2	infekce
vyvolané	vyvolaný	k2eAgFnPc1d1	vyvolaná
spirochetami	spirocheta	k1gFnPc7	spirocheta
<g/>
,	,	kIx,	,
jistým	jistý	k2eAgInSc7d1	jistý
typem	typ	k1gInSc7	typ
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Treponema	Treponema	k1gNnSc1	Treponema
pallidum	pallidum	k1gNnSc1	pallidum
(	(	kIx(	(
<g/>
příčina	příčina	k1gFnSc1	příčina
syfilidy	syfilida	k1gFnSc2	syfilida
<g/>
)	)	kIx)	)
a	a	k8xC	a
Borrelia	Borrelia	k1gFnSc1	Borrelia
burgdorferi	burgdorfer	k1gFnSc2	burgdorfer
(	(	kIx(	(
<g/>
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
lymské	lymský	k2eAgFnSc2d1	lymská
boreliózy	borelióza	k1gFnSc2	borelióza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
může	moct	k5eAaImIp3nS	moct
souviset	souviset	k5eAaImF	souviset
s	s	k7c7	s
cerebrální	cerebrální	k2eAgFnSc7d1	cerebrální
malárií	malárie	k1gFnSc7	malárie
(	(	kIx(	(
<g/>
malárie	malárie	k1gFnSc2	malárie
zasahující	zasahující	k2eAgInSc1d1	zasahující
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
či	či	k8xC	či
amébovou	amébův	k2eAgFnSc7d1	amébův
meningitidou	meningitida	k1gFnSc7	meningitida
<g/>
,	,	kIx,	,
způsobenou	způsobený	k2eAgFnSc7d1	způsobená
infekcí	infekce	k1gFnSc7	infekce
amébami	améba	k1gFnPc7	améba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Naegleria	Naeglerium	k1gNnPc4	Naeglerium
fowleri	fowleri	k1gNnSc2	fowleri
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
nakazit	nakazit	k5eAaPmF	nakazit
vodou	voda	k1gFnSc7	voda
ze	z	k7c2	z
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Typy	typ	k1gInPc1	typ
bakterií	bakterie	k1gFnPc2	bakterie
způsobujících	způsobující	k2eAgFnPc2d1	způsobující
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
meningitidu	meningitida	k1gFnSc4	meningitida
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
infikovaného	infikovaný	k2eAgMnSc2d1	infikovaný
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
předčasně	předčasně	k6eAd1	předčasně
narozených	narozený	k2eAgFnPc2d1	narozená
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
novorozenců	novorozenec	k1gMnPc2	novorozenec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
měsíců	měsíc	k1gInPc2	měsíc
jsou	být	k5eAaImIp3nP	být
častými	častý	k2eAgFnPc7d1	častá
příčinami	příčina	k1gFnPc7	příčina
streptokok	streptokok	k1gMnSc1	streptokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
(	(	kIx(	(
<g/>
podtyp	podtyp	k1gInSc1	podtyp
III	III	kA	III
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
vagíně	vagína	k1gFnSc6	vagína
a	a	k8xC	a
onemocnění	onemocnění	k1gNnSc6	onemocnění
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
zejména	zejména	k9	zejména
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obvykle	obvykle	k6eAd1	obvykle
osidlují	osidlovat	k5eAaImIp3nP	osidlovat
zažívací	zažívací	k2eAgInSc4d1	zažívací
trakt	trakt	k1gInSc4	trakt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Escherichia	Escherichia	k1gFnSc1	Escherichia
coli	col	k1gFnSc2	col
(	(	kIx(	(
<g/>
nositel	nositel	k1gMnSc1	nositel
antigenu	antigen	k1gInSc2	antigen
K	k	k7c3	k
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Novorozence	novorozenec	k1gMnSc4	novorozenec
může	moct	k5eAaImIp3nS	moct
postihnout	postihnout	k5eAaPmF	postihnout
i	i	k9	i
bakterie	bakterie	k1gFnSc1	bakterie
Listeria	Listerium	k1gNnSc2	Listerium
monocytogenes	monocytogenesa	k1gFnPc2	monocytogenesa
(	(	kIx(	(
<g/>
sérotyp	sérotypit	k5eAaPmRp2nS	sérotypit
IVb	IVb	k1gMnSc5	IVb
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
epidemie	epidemie	k1gFnSc1	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
děti	dítě	k1gFnPc1	dítě
bývají	bývat	k5eAaImIp3nP	bývat
častěji	často	k6eAd2	často
nakaženy	nakažen	k2eAgInPc1d1	nakažen
organismy	organismus	k1gInPc1	organismus
Neisseria	Neisserium	k1gNnSc2	Neisserium
meningitidis	meningitidis	k1gFnSc2	meningitidis
(	(	kIx(	(
<g/>
meningokok	meningokok	k1gInSc1	meningokok
<g/>
)	)	kIx)	)
a	a	k8xC	a
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumoniaat	k5eAaPmIp3nS	pneumoniaat
(	(	kIx(	(
<g/>
sérotypy	sérotyp	k1gInPc1	sérotyp
6	[number]	k4	6
<g/>
,	,	kIx,	,
9	[number]	k4	9
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
,	,	kIx,	,
18	[number]	k4	18
a	a	k8xC	a
23	[number]	k4	23
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
děti	dítě	k1gFnPc1	dítě
mladší	mladý	k2eAgFnSc3d2	mladší
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
pak	pak	k8xC	pak
bakterií	bakterie	k1gFnPc2	bakterie
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
typu	typ	k1gInSc2	typ
B	B	kA	B
(	(	kIx(	(
<g/>
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
vakcinace	vakcinace	k1gFnSc1	vakcinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
80	[number]	k4	80
%	%	kIx~	%
případů	případ	k1gInPc2	případ
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
bakterie	bakterie	k1gFnSc2	bakterie
Neisseria	Neisserium	k1gNnSc2	Neisserium
meningitidis	meningitidis	k1gInSc1	meningitidis
a	a	k8xC	a
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
pneumoniae	pneumoniaat	k5eAaPmIp3nS	pneumoniaat
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
infekce	infekce	k1gFnSc2	infekce
bakterií	bakterie	k1gFnPc2	bakterie
Listeria	Listerium	k1gNnSc2	Listerium
monocytogenes	monocytogenes	k1gInSc1	monocytogenes
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
starších	starší	k1gMnPc2	starší
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
pneumokokové	pneumokokový	k2eAgFnSc2d1	pneumokoková
vakcíny	vakcína	k1gFnSc2	vakcína
snížilo	snížit	k5eAaPmAgNnS	snížit
počet	počet	k1gInSc1	počet
případů	případ	k1gInPc2	případ
pneumokokové	pneumokokový	k2eAgFnSc2d1	pneumokoková
meningitidy	meningitida	k1gFnSc2	meningitida
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
i	i	k8xC	i
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedávno	nedávno	k6eAd1	nedávno
prodělaného	prodělaný	k2eAgNnSc2d1	prodělané
poranění	poranění	k1gNnSc2	poranění
lebky	lebka	k1gFnSc2	lebka
může	moct	k5eAaImIp3nS	moct
potenciálně	potenciálně	k6eAd1	potenciálně
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
proniknutí	proniknutí	k1gNnSc3	proniknutí
bakterií	bakterie	k1gFnPc2	bakterie
dutiny	dutina	k1gFnSc2	dutina
nosní	nosní	k2eAgInSc4d1	nosní
do	do	k7c2	do
meningeálního	meningeální	k2eAgInSc2d1	meningeální
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
zvýšené	zvýšený	k2eAgNnSc1d1	zvýšené
riziko	riziko	k1gNnSc1	riziko
meningitidy	meningitida	k1gFnSc2	meningitida
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
užíváním	užívání	k1gNnSc7	užívání
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
mozkový	mozkový	k2eAgInSc1d1	mozkový
shunt	shunt	k1gInSc1	shunt
<g/>
,	,	kIx,	,
externí	externí	k2eAgInSc1d1	externí
ventrikulární	ventrikulární	k2eAgInSc1d1	ventrikulární
drén	drén	k1gInSc1	drén
či	či	k8xC	či
Ommaya	Ommaya	k1gFnSc1	Ommaya
rezervoár	rezervoár	k1gInSc1	rezervoár
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
pravděpodobnější	pravděpodobný	k2eAgNnSc1d2	pravděpodobnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemocní	nemocný	k1gMnPc1	nemocný
budou	být	k5eAaImBp3nP	být
nakaženi	nakazit	k5eAaPmNgMnP	nakazit
stafylokokem	stafylokok	k1gInSc7	stafylokok
<g/>
,	,	kIx,	,
pseudomonádami	pseudomonáda	k1gFnPc7	pseudomonáda
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
gramnegativními	gramnegativní	k2eAgFnPc7d1	gramnegativní
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
patogeny	patogen	k1gInPc1	patogen
také	také	k9	také
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
meningitidou	meningitida	k1gFnSc7	meningitida
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
poškozeným	poškozený	k2eAgInSc7d1	poškozený
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
malého	malý	k2eAgInSc2d1	malý
počtu	počet	k1gInSc2	počet
lidí	člověk	k1gMnPc2	člověk
může	moct	k5eAaImIp3nS	moct
k	k	k7c3	k
meningitidě	meningitida	k1gFnSc3	meningitida
vést	vést	k5eAaImF	vést
infekce	infekce	k1gFnPc4	infekce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc1	zánět
středního	střední	k2eAgNnSc2d1	střední
ucha	ucho	k1gNnSc2	ucho
nebo	nebo	k8xC	nebo
mastoiditida	mastoiditida	k1gFnSc1	mastoiditida
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
kochleárních	kochleární	k2eAgInPc2d1	kochleární
implantátů	implantát	k1gInPc2	implantát
<g/>
,	,	kIx,	,
užívaných	užívaný	k2eAgFnPc2d1	užívaná
osobami	osoba	k1gFnPc7	osoba
s	s	k7c7	s
postižením	postižení	k1gNnSc7	postižení
sluchu	sluch	k1gInSc2	sluch
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
pneumokoková	pneumokokový	k2eAgFnSc1d1	pneumokoková
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulózní	tuberkulózní	k2eAgFnSc1d1	tuberkulózní
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
meningitida	meningitida	k1gFnSc1	meningitida
způsobená	způsobený	k2eAgFnSc1d1	způsobená
bakterií	bakterie	k1gFnSc7	bakterie
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gFnPc4	tuberculosis
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgInSc1d2	častější
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
endemický	endemický	k2eAgInSc1d1	endemický
charakter	charakter	k1gInSc4	charakter
<g/>
,	,	kIx,	,
setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
však	však	k8xC	však
i	i	k9	i
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
imunitními	imunitní	k2eAgInPc7d1	imunitní
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
<s>
Opakující	opakující	k2eAgFnSc1d1	opakující
se	se	k3xPyFc4	se
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
meningitida	meningitida	k1gFnSc1	meningitida
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
přetrvávajícím	přetrvávající	k2eAgInSc7d1	přetrvávající
anatomickým	anatomický	k2eAgInSc7d1	anatomický
defektem	defekt	k1gInSc7	defekt
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
vrozeným	vrozený	k2eAgInSc7d1	vrozený
nebo	nebo	k8xC	nebo
získaným	získaný	k2eAgInSc7d1	získaný
<g/>
,	,	kIx,	,
či	či	k8xC	či
poruchami	porucha	k1gFnPc7	porucha
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Anatomické	anatomický	k2eAgInPc1d1	anatomický
defekty	defekt	k1gInPc1	defekt
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
průchodnost	průchodnost	k1gFnSc4	průchodnost
mezi	mezi	k7c7	mezi
vnějším	vnější	k2eAgNnSc7d1	vnější
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
nervovým	nervový	k2eAgInSc7d1	nervový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnSc7d3	nejběžnější
příčinou	příčina	k1gFnSc7	příčina
opakující	opakující	k2eAgFnSc2d1	opakující
se	se	k3xPyFc4	se
meningitidy	meningitida	k1gFnSc2	meningitida
jsou	být	k5eAaImIp3nP	být
fraktury	fraktura	k1gFnPc1	fraktura
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
takové	takový	k3xDgFnPc1	takový
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
spodinu	spodina	k1gFnSc4	spodina
lebeční	lebeční	k2eAgFnSc1d1	lebeční
nebo	nebo	k8xC	nebo
dutiny	dutina	k1gFnPc1	dutina
a	a	k8xC	a
skalní	skalní	k2eAgFnPc1d1	skalní
kosti	kost	k1gFnPc1	kost
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
59	[number]	k4	59
%	%	kIx~	%
případů	případ	k1gInPc2	případ
opakující	opakující	k2eAgMnSc1d1	opakující
se	se	k3xPyFc4	se
meningitidy	meningitida	k1gFnSc2	meningitida
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podobných	podobný	k2eAgFnPc2d1	podobná
anatomických	anatomický	k2eAgFnPc2d1	anatomická
anomálií	anomálie	k1gFnPc2	anomálie
<g/>
,	,	kIx,	,
36	[number]	k4	36
%	%	kIx~	%
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
imunitní	imunitní	k2eAgFnSc2d1	imunitní
nedostatečnosti	nedostatečnost	k1gFnSc2	nedostatečnost
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
deficit	deficit	k1gInSc4	deficit
komplementu	komplement	k1gInSc2	komplement
způsobující	způsobující	k2eAgFnSc4d1	způsobující
náchylnost	náchylnost	k1gFnSc4	náchylnost
zvláště	zvláště	k9	zvláště
k	k	k7c3	k
opakující	opakující	k2eAgFnSc3d1	opakující
se	se	k3xPyFc4	se
meningokokové	meningokokový	k2eAgFnSc3d1	meningokoková
meningitidě	meningitida	k1gFnSc3	meningitida
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
%	%	kIx~	%
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
probíhajících	probíhající	k2eAgInPc2d1	probíhající
zánětů	zánět	k1gInPc2	zánět
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
virům	vir	k1gInPc3	vir
způsobujícím	způsobující	k2eAgInPc3d1	způsobující
meningitidu	meningitida	k1gFnSc4	meningitida
patří	patřit	k5eAaImIp3nP	patřit
enteroviry	enterovira	k1gFnPc1	enterovira
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gNnSc7	simplex
typu	typ	k1gInSc2	typ
2	[number]	k4	2
(	(	kIx(	(
<g/>
a	a	k8xC	a
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
typu	typ	k1gInSc2	typ
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
Varicella	Varicella	k1gMnSc1	Varicella
zoster	zoster	k1gMnSc1	zoster
(	(	kIx(	(
<g/>
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
příčina	příčina	k1gFnSc1	příčina
planých	planý	k2eAgFnPc2d1	planá
neštovic	neštovice	k1gFnPc2	neštovice
a	a	k8xC	a
pásového	pásový	k2eAgInSc2d1	pásový
oparu	opar	k1gInSc2	opar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příušnice	příušnice	k1gFnPc4	příušnice
<g/>
,	,	kIx,	,
HIV	HIV	kA	HIV
a	a	k8xC	a
LCM	LCM	kA	LCM
<g/>
.	.	kIx.	.
</s>
<s>
Vzniku	vznik	k1gInSc2	vznik
fungální	fungální	k2eAgFnSc2d1	fungální
meningitidy	meningitida	k1gFnSc2	meningitida
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
řada	řada	k1gFnSc1	řada
rizikových	rizikový	k2eAgInPc2d1	rizikový
faktorů	faktor	k1gInPc2	faktor
-	-	kIx~	-
například	například	k6eAd1	například
užívání	užívání	k1gNnSc1	užívání
imunosupresiv	imunosupresit	k5eAaPmDgInS	imunosupresit
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
po	po	k7c4	po
transplantaci	transplantace	k1gFnSc4	transplantace
orgánu	orgán	k1gInSc2	orgán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
či	či	k8xC	či
ztrátě	ztráta	k1gFnSc3	ztráta
imunity	imunita	k1gFnSc2	imunita
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
normálně	normálně	k6eAd1	normálně
fungujícím	fungující	k2eAgInSc7d1	fungující
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
ale	ale	k8xC	ale
při	při	k7c6	při
kontaminaci	kontaminace	k1gFnSc6	kontaminace
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
postupně	postupně	k6eAd1	postupně
-	-	kIx~	-
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
horečka	horečka	k1gFnSc1	horečka
mohou	moct	k5eAaImIp3nP	moct
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
formou	forma	k1gFnSc7	forma
fungální	fungální	k2eAgFnSc2d1	fungální
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
kryptokoková	kryptokokový	k2eAgFnSc1d1	kryptokokový
meningitida	meningitida	k1gFnSc1	meningitida
způsobená	způsobený	k2eAgFnSc1d1	způsobená
kvasinkou	kvasinka	k1gFnSc7	kvasinka
Cryptococcus	Cryptococcus	k1gMnSc1	Cryptococcus
neoformans	oformansit	k5eNaPmRp2nS	oformansit
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
je	být	k5eAaImIp3nS	být
kryptokoková	kryptokokový	k2eAgFnSc1d1	kryptokokový
meningitida	meningitida	k1gFnSc1	meningitida
nejběžnější	běžný	k2eAgFnSc1d3	nejběžnější
příčinou	příčina	k1gFnSc7	příčina
meningitidy	meningitida	k1gFnPc4	meningitida
a	a	k8xC	a
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
%	%	kIx~	%
úmrtí	úmrť	k1gFnPc2	úmrť
souvisejících	související	k2eAgFnPc2d1	související
s	s	k7c7	s
AIDS	AIDS	kA	AIDS
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
častým	častý	k2eAgFnPc3d1	častá
fungálním	fungální	k2eAgFnPc3d1	fungální
příčinám	příčina	k1gFnPc3	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
patří	patřit	k5eAaImIp3nS	patřit
patogenní	patogenní	k2eAgFnSc1d1	patogenní
houba	houba	k1gFnSc1	houba
Histoplasma	Histoplasmum	k1gNnSc2	Histoplasmum
capsulatum	capsulatum	k1gNnSc1	capsulatum
<g/>
,	,	kIx,	,
Coccidioides	Coccidioides	k1gInSc1	Coccidioides
immitis	immitis	k1gFnSc1	immitis
<g/>
,	,	kIx,	,
dimorfní	dimorfní	k2eAgFnSc1d1	dimorfní
houba	houba	k1gFnSc1	houba
Blastomyces	Blastomyces	k1gMnSc1	Blastomyces
dermatitidis	dermatitidis	k1gInSc1	dermatitidis
a	a	k8xC	a
představitelé	představitel	k1gMnPc1	představitel
rodu	rod	k1gInSc2	rod
Candida	Candida	k1gFnSc1	Candida
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
převaha	převaha	k1gFnSc1	převaha
eozinofilů	eozinofil	k1gMnPc2	eozinofil
(	(	kIx(	(
<g/>
druh	druh	k1gInSc1	druh
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
meningitida	meningitida	k1gFnSc1	meningitida
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
parazity	parazit	k1gMnPc7	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
implikovanými	implikovaný	k2eAgMnPc7d1	implikovaný
parazity	parazit	k1gMnPc7	parazit
jsou	být	k5eAaImIp3nP	být
hlístice	hlístice	k1gFnSc1	hlístice
Angiostrongylus	Angiostrongylus	k1gInSc4	Angiostrongylus
cantonensis	cantonensis	k1gFnSc2	cantonensis
<g/>
,	,	kIx,	,
Gnathostoma	Gnathostoma	k1gNnSc1	Gnathostoma
spinigerum	spinigerum	k1gNnSc1	spinigerum
a	a	k8xC	a
krevnička	krevnička	k1gFnSc1	krevnička
střevní	střevní	k2eAgFnSc1d1	střevní
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
nemoci	nemoc	k1gFnPc1	nemoc
způsobené	způsobený	k2eAgMnPc4d1	způsobený
parazity	parazit	k1gMnPc4	parazit
-	-	kIx~	-
cysticerkóza	cysticerkóza	k1gFnSc1	cysticerkóza
<g/>
,	,	kIx,	,
toxokaróza	toxokaróza	k1gFnSc1	toxokaróza
<g/>
,	,	kIx,	,
nemoc	nemoc	k1gFnSc1	nemoc
způsobená	způsobený	k2eAgFnSc1d1	způsobená
škrkavkou	škrkavka	k1gFnSc7	škrkavka
Baylisascariasis	Baylisascariasis	k1gInSc1	Baylisascariasis
<g/>
,	,	kIx,	,
paragonimóza	paragonimóza	k1gFnSc1	paragonimóza
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
vzácných	vzácný	k2eAgNnPc2d1	vzácné
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
neinfekčních	infekční	k2eNgFnPc2d1	neinfekční
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
meningitidě	meningitida	k1gFnSc3	meningitida
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
řady	řada	k1gFnSc2	řada
neinfekčních	infekční	k2eNgFnPc2d1	neinfekční
příčin	příčina	k1gFnPc2	příčina
-	-	kIx~	-
rozšíření	rozšíření	k1gNnSc1	rozšíření
rakoviny	rakovina	k1gFnSc2	rakovina
na	na	k7c4	na
mozkomíšní	mozkomíšní	k2eAgInPc4d1	mozkomíšní
pleny	plen	k1gInPc4	plen
(	(	kIx(	(
<g/>
maligní	maligní	k2eAgFnSc1d1	maligní
neboli	neboli	k8xC	neboli
neoplastická	oplastický	k2eNgFnSc1d1	oplastický
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
)	)	kIx)	)
či	či	k8xC	či
působení	působení	k1gNnSc1	působení
některých	některý	k3yIgInPc2	některý
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
nesteroidních	steroidní	k2eNgInPc2d1	nesteroidní
protizánětlivých	protizánětlivý	k2eAgInPc2d1	protizánětlivý
léků	lék	k1gInPc2	lék
<g/>
,	,	kIx,	,
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
a	a	k8xC	a
nitrožilních	nitrožilní	k2eAgInPc2d1	nitrožilní
imunoglobulinů	imunoglobulin	k1gInPc2	imunoglobulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vině	vina	k1gFnSc6	vina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
další	další	k2eAgNnPc4d1	další
zánětlivá	zánětlivý	k2eAgNnPc4d1	zánětlivé
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
sarkoidóza	sarkoidóza	k1gFnSc1	sarkoidóza
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nazývá	nazývat	k5eAaImIp3nS	nazývat
neurosarkoidóza	neurosarkoidóza	k1gFnSc1	neurosarkoidóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
choroby	choroba	k1gFnPc4	choroba
pojivových	pojivový	k2eAgFnPc2d1	pojivová
tkání	tkáň	k1gFnPc2	tkáň
jako	jako	k8xS	jako
systémový	systémový	k2eAgInSc4d1	systémový
lupus	lupus	k1gInSc4	lupus
erythematodes	erythematodesa	k1gFnPc2	erythematodesa
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
formy	forma	k1gFnPc1	forma
vaskulitidy	vaskulitis	k1gFnSc2	vaskulitis
(	(	kIx(	(
<g/>
zánětlivé	zánětlivý	k2eAgNnSc1d1	zánětlivé
onemocnění	onemocnění	k1gNnSc1	onemocnění
stěn	stěna	k1gFnPc2	stěna
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Behçetova	Behçetův	k2eAgFnSc1d1	Behçetův
choroba	choroba	k1gFnSc1	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
epidermoidní	epidermoidní	k2eAgFnPc4d1	epidermoidní
cysty	cysta	k1gFnPc4	cysta
a	a	k8xC	a
dermoidní	dermoidní	k2eAgFnPc4d1	dermoidní
cysty	cysta	k1gFnPc4	cysta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
uvolňování	uvolňování	k1gNnSc3	uvolňování
dráždivých	dráždivý	k2eAgFnPc2d1	dráždivá
látek	látka	k1gFnPc2	látka
do	do	k7c2	do
subarachnoidálního	subarachnoidální	k2eAgInSc2d1	subarachnoidální
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Mollaretova	Mollaretův	k2eAgFnSc1d1	Mollaretův
meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
syndromem	syndrom	k1gInSc7	syndrom
opakujících	opakující	k2eAgFnPc2d1	opakující
se	se	k3xPyFc4	se
epizod	epizoda	k1gFnPc2	epizoda
aseptické	aseptický	k2eAgFnSc2d1	aseptická
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
domněnky	domněnka	k1gFnPc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
virem	vir	k1gInSc7	vir
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gNnSc7	simplex
typu	typ	k1gInSc2	typ
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
příčinou	příčina	k1gFnSc7	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
migréna	migréna	k1gFnSc1	migréna
<g/>
,	,	kIx,	,
diagnóza	diagnóza	k1gFnSc1	diagnóza
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
stanovena	stanovit	k5eAaPmNgFnS	stanovit
až	až	k9	až
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
další	další	k2eAgFnPc1d1	další
příčiny	příčina	k1gFnPc1	příčina
vyloučeny	vyloučit	k5eAaPmNgFnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Mozkomíšní	mozkomíšní	k2eAgFnPc1d1	mozkomíšní
pleny	plena	k1gFnPc1	plena
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
třemi	tři	k4xCgFnPc7	tři
membránami	membrána	k1gFnPc7	membrána
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
mozkomíšním	mozkomíšní	k2eAgInSc7d1	mozkomíšní
mokem	mok	k1gInSc7	mok
obalují	obalovat	k5eAaImIp3nP	obalovat
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nP	chránit
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
míchu	mícha	k1gFnSc4	mícha
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgInSc1d1	centrální
nervový	nervový	k2eAgInSc1d1	nervový
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měkká	měkký	k2eAgFnSc1d1	měkká
plena	plena	k1gFnSc1	plena
mozková	mozkový	k2eAgFnSc1d1	mozková
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jemná	jemný	k2eAgFnSc1d1	jemná
nepropustná	propustný	k2eNgFnSc1d1	nepropustná
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přiléhá	přiléhat	k5eAaImIp3nS	přiléhat
pevně	pevně	k6eAd1	pevně
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
všechny	všechen	k3xTgInPc1	všechen
jeho	jeho	k3xOp3gInPc1	jeho
obrysy	obrys	k1gInPc1	obrys
a	a	k8xC	a
tvary	tvar	k1gInPc1	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pavučnice	pavučnice	k1gFnSc1	pavučnice
(	(	kIx(	(
<g/>
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
takto	takto	k6eAd1	takto
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
podobnosti	podobnost	k1gFnSc3	podobnost
s	s	k7c7	s
pavučinou	pavučina	k1gFnSc7	pavučina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
jakýsi	jakýsi	k3yIgInSc1	jakýsi
měchýř	měchýř	k1gInSc1	měchýř
přiléhající	přiléhající	k2eAgInSc1d1	přiléhající
volně	volně	k6eAd1	volně
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
měkké	měkký	k2eAgFnSc2d1	měkká
pleny	plena	k1gFnSc2	plena
mozkové	mozkový	k2eAgFnSc2d1	mozková
<g/>
.	.	kIx.	.
</s>
<s>
Subarachnoidální	Subarachnoidální	k2eAgInSc1d1	Subarachnoidální
prostor	prostor	k1gInSc1	prostor
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
pavučnici	pavučnice	k1gFnSc4	pavučnice
a	a	k8xC	a
měkkou	měkký	k2eAgFnSc4d1	měkká
plenu	plena	k1gFnSc4	plena
mozkovou	mozkový	k2eAgFnSc4d1	mozková
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
mozkomíšním	mozkomíšní	k2eAgInSc7d1	mozkomíšní
mokem	mok	k1gInSc7	mok
<g/>
.	.	kIx.	.
</s>
<s>
Vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
plena	plena	k1gFnSc1	plena
mozková	mozkový	k2eAgFnSc1d1	mozková
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
odolná	odolný	k2eAgFnSc1d1	odolná
membrána	membrána	k1gFnSc1	membrána
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
pavučnicí	pavučnice	k1gFnSc7	pavučnice
i	i	k8xC	i
lebkou	lebka	k1gFnSc7	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bakteriální	bakteriální	k2eAgFnSc6d1	bakteriální
meningitidě	meningitida	k1gFnSc6	meningitida
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
k	k	k7c3	k
mozkomíšním	mozkomíšní	k2eAgFnPc3d1	mozkomíšní
plenám	plena	k1gFnPc3	plena
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
dvěma	dva	k4xCgFnPc7	dva
hlavními	hlavní	k2eAgFnPc7d1	hlavní
cestami	cesta	k1gFnPc7	cesta
<g/>
:	:	kIx,	:
krevním	krevní	k2eAgNnSc7d1	krevní
řečištěm	řečiště	k1gNnSc7	řečiště
nebo	nebo	k8xC	nebo
přímým	přímý	k2eAgInSc7d1	přímý
kontaktem	kontakt	k1gInSc7	kontakt
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
buď	buď	k8xC	buď
s	s	k7c7	s
dutinou	dutina	k1gFnSc7	dutina
nosní	nosní	k2eAgFnSc7d1	nosní
nebo	nebo	k8xC	nebo
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
meningitida	meningitida	k1gFnSc1	meningitida
následuje	následovat	k5eAaImIp3nS	následovat
po	po	k7c6	po
invazi	invaze	k1gFnSc6	invaze
organismů	organismus	k1gInPc2	organismus
přežívajících	přežívající	k2eAgInPc2d1	přežívající
na	na	k7c6	na
sliznicích	sliznice	k1gFnPc6	sliznice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
dutina	dutina	k1gFnSc1	dutina
nosní	nosní	k2eAgFnSc1d1	nosní
<g/>
,	,	kIx,	,
do	do	k7c2	do
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
často	často	k6eAd1	často
předchází	předcházet	k5eAaImIp3nS	předcházet
virová	virový	k2eAgFnSc1d1	virová
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
bariéru	bariéra	k1gFnSc4	bariéra
mezi	mezi	k7c7	mezi
vnějším	vnější	k2eAgNnSc7d1	vnější
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
organismem	organismus	k1gInSc7	organismus
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
sliznice	sliznice	k1gFnSc2	sliznice
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
naruší	narušit	k5eAaPmIp3nS	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
bakterie	bakterie	k1gFnPc1	bakterie
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
do	do	k7c2	do
subarachnoidálního	subarachnoidální	k2eAgInSc2d1	subarachnoidální
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
krevně	krevně	k6eAd1	krevně
mozková	mozkový	k2eAgFnSc1d1	mozková
bariéra	bariéra	k1gFnSc1	bariéra
zranitelná	zranitelný	k2eAgFnSc1d1	zranitelná
<g/>
–	–	k?	–
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
choroidního	choroidní	k2eAgInSc2d1	choroidní
plexu	plexus	k1gInSc2	plexus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
meningitidě	meningitida	k1gFnSc3	meningitida
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
25	[number]	k4	25
%	%	kIx~	%
novorozenců	novorozenec	k1gMnPc2	novorozenec
s	s	k7c7	s
infekcí	infekce	k1gFnSc7	infekce
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
streptokokem	streptokok	k1gInSc7	streptokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
;	;	kIx,	;
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
méně	málo	k6eAd2	málo
častý	častý	k2eAgInSc1d1	častý
<g/>
.	.	kIx.	.
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
infekce	infekce	k1gFnSc1	infekce
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
i	i	k9	i
přístroji	přístroj	k1gInPc7	přístroj
zavedenými	zavedený	k2eAgFnPc7d1	zavedená
do	do	k7c2	do
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
zlomeninami	zlomenina	k1gFnPc7	zlomenina
kostí	kost	k1gFnPc2	kost
lebečních	lebeční	k2eAgInPc2d1	lebeční
nebo	nebo	k8xC	nebo
infekcí	infekce	k1gFnSc7	infekce
nosohltanu	nosohltan	k1gInSc2	nosohltan
či	či	k8xC	či
dutin	dutina	k1gFnPc2	dutina
nosních	nosní	k2eAgFnPc2d1	nosní
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
se	se	k3xPyFc4	se
subarachnoidálním	subarachnoidálnit	k5eAaPmIp1nS	subarachnoidálnit
prostorem	prostor	k1gInSc7	prostor
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
občas	občas	k6eAd1	občas
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
vině	vina	k1gFnSc6	vina
vrozené	vrozený	k2eAgFnSc2d1	vrozená
vady	vada	k1gFnSc2	vada
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
pleny	plena	k1gFnSc2	plena
mozkové	mozkový	k2eAgFnSc2d1	mozková
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
zánět	zánět	k1gInSc1	zánět
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
subarachnoidálním	subarachnoidální	k2eAgInSc6d1	subarachnoidální
prostoru	prostor	k1gInSc6	prostor
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
přímým	přímý	k2eAgInSc7d1	přímý
následkem	následek	k1gInSc7	následek
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
reakcí	reakce	k1gFnSc7	reakce
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
na	na	k7c4	na
vniknutí	vniknutí	k1gNnSc4	vniknutí
bakterií	bakterie	k1gFnPc2	bakterie
do	do	k7c2	do
centrálního	centrální	k2eAgInSc2d1	centrální
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
imunitní	imunitní	k2eAgFnPc1d1	imunitní
buňky	buňka	k1gFnPc1	buňka
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
astrocyty	astrocyt	k1gInPc1	astrocyt
a	a	k8xC	a
mikroglie	mikroglie	k1gFnPc1	mikroglie
<g/>
)	)	kIx)	)
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
složky	složka	k1gFnPc1	složka
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
<g/>
,	,	kIx,	,
reagují	reagovat	k5eAaBmIp3nP	reagovat
pak	pak	k6eAd1	pak
uvolňováním	uvolňování	k1gNnSc7	uvolňování
značného	značný	k2eAgNnSc2d1	značné
množství	množství	k1gNnSc2	množství
cytokinů	cytokin	k1gInPc2	cytokin
<g/>
,	,	kIx,	,
hormonům	hormon	k1gInPc3	hormon
podobných	podobný	k2eAgInPc2d1	podobný
mediátorů	mediátor	k1gInPc2	mediátor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
aktivují	aktivovat	k5eAaBmIp3nP	aktivovat
další	další	k2eAgFnPc4d1	další
imunitní	imunitní	k2eAgFnPc4d1	imunitní
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posílily	posílit	k5eAaPmAgFnP	posílit
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Krevně	krevně	k6eAd1	krevně
mozková	mozkový	k2eAgFnSc1d1	mozková
bariéra	bariéra	k1gFnSc1	bariéra
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
prostupnější	prostupný	k2eAgNnSc1d2	prostupnější
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
vazogennímu	vazogenní	k2eAgInSc3d1	vazogenní
edému	edém	k1gInSc3	edém
mozku	mozek	k1gInSc2	mozek
(	(	kIx(	(
<g/>
otok	otok	k1gInSc1	otok
mozku	mozek	k1gInSc2	mozek
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
úniku	únik	k1gInSc2	únik
tekutiny	tekutina	k1gFnSc2	tekutina
z	z	k7c2	z
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zánětu	zánět	k1gInSc3	zánět
mozkomíšních	mozkomíšní	k2eAgFnPc2d1	mozkomíšní
plen	plena	k1gFnPc2	plena
a	a	k8xC	a
následně	následně	k6eAd1	následně
k	k	k7c3	k
intersticiálnímu	intersticiální	k2eAgInSc3d1	intersticiální
edému	edém	k1gInSc3	edém
(	(	kIx(	(
<g/>
otok	otok	k1gInSc1	otok
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
na	na	k7c6	na
základě	základ	k1gInSc6	základ
množství	množství	k1gNnSc2	množství
tekutiny	tekutina	k1gFnSc2	tekutina
mezi	mezi	k7c7	mezi
buňkami	buňka	k1gFnPc7	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zanítit	zanítit	k5eAaPmF	zanítit
i	i	k8xC	i
stěny	stěna	k1gFnPc4	stěna
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
(	(	kIx(	(
<g/>
cerebrální	cerebrální	k2eAgFnSc1d1	cerebrální
vaskulitida	vaskulitida	k1gFnSc1	vaskulitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
sníženého	snížený	k2eAgInSc2d1	snížený
průtoku	průtok	k1gInSc2	průtok
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
typu	typ	k1gInSc2	typ
otoku	otok	k1gInSc2	otok
<g/>
,	,	kIx,	,
cytotoxického	cytotoxický	k2eAgInSc2d1	cytotoxický
edému	edém	k1gInSc2	edém
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tři	tři	k4xCgFnPc1	tři
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
formy	forma	k1gFnPc1	forma
cerebrálního	cerebrální	k2eAgInSc2d1	cerebrální
edému	edém	k1gInSc2	edém
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
nitrolebního	nitrolební	k2eAgInSc2d1	nitrolební
tlaku	tlak	k1gInSc2	tlak
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc4	ten
společně	společně	k6eAd1	společně
se	s	k7c7	s
sníženým	snížený	k2eAgInSc7d1	snížený
krevním	krevní	k2eAgInSc7d1	krevní
tlakem	tlak	k1gInSc7	tlak
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytujícím	vyskytující	k2eAgNnPc3d1	vyskytující
při	při	k7c6	při
akutních	akutní	k2eAgFnPc6d1	akutní
infekcích	infekce	k1gFnPc6	infekce
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
mozku	mozek	k1gInSc2	mozek
mnohem	mnohem	k6eAd1	mnohem
obtížněji	obtížně	k6eAd2	obtížně
<g/>
,	,	kIx,	,
následkem	následkem	k7c2	následkem
čehož	což	k3yQnSc2	což
trpí	trpět	k5eAaImIp3nP	trpět
mozkové	mozkový	k2eAgFnSc2d1	mozková
buňky	buňka	k1gFnSc2	buňka
nedostatkem	nedostatek	k1gInSc7	nedostatek
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
k	k	k7c3	k
apoptóze	apoptóza	k1gFnSc3	apoptóza
(	(	kIx(	(
<g/>
programovaná	programovaný	k2eAgFnSc1d1	programovaná
buněčná	buněčný	k2eAgFnSc1d1	buněčná
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Připouští	připouštět	k5eAaImIp3nS	připouštět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
výše	vysoce	k6eAd2	vysoce
popsaného	popsaný	k2eAgInSc2d1	popsaný
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
z	z	k7c2	z
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
rozkládajících	rozkládající	k2eAgFnPc2d1	rozkládající
se	se	k3xPyFc4	se
bakterií	bakterie	k1gFnPc2	bakterie
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
množství	množství	k1gNnSc4	množství
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
léčebné	léčebný	k2eAgInPc1d1	léčebný
postupy	postup	k1gInPc1	postup
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
podávání	podávání	k1gNnSc1	podávání
kortikosteroidů	kortikosteroid	k1gInPc2	kortikosteroid
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
utlumit	utlumit	k5eAaPmF	utlumit
reakci	reakce	k1gFnSc4	reakce
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Osobám	osoba	k1gFnPc3	osoba
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgNnPc2	jenž
existuje	existovat	k5eAaImIp3nS	existovat
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
meningitidu	meningitida	k1gFnSc4	meningitida
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
krevní	krevní	k2eAgInPc1d1	krevní
testy	test	k1gInPc1	test
na	na	k7c4	na
zjištění	zjištění	k1gNnSc4	zjištění
markerů	marker	k1gInPc2	marker
zánětu	zánět	k1gInSc2	zánět
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
C-reaktivní	Ceaktivní	k2eAgInSc1d1	C-reaktivní
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgInSc1d1	krevní
obraz	obraz	k1gInSc1	obraz
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
hemokultura	hemokultura	k1gFnSc1	hemokultura
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
testem	test	k1gInSc7	test
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
může	moct	k5eAaImIp3nS	moct
meningitidu	meningitida	k1gFnSc4	meningitida
prokázat	prokázat	k5eAaPmF	prokázat
nebo	nebo	k8xC	nebo
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
analýza	analýza	k1gFnSc1	analýza
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
získaného	získaný	k2eAgInSc2d1	získaný
lumbální	lumbální	k2eAgFnSc1d1	lumbální
punkcí	punkce	k1gFnSc7	punkce
(	(	kIx(	(
<g/>
LP	LP	kA	LP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
kontraindikována	kontraindikován	k2eAgFnSc1d1	kontraindikována
při	při	k7c6	při
nahromadění	nahromadění	k1gNnSc6	nahromadění
tkáně	tkáň	k1gFnSc2	tkáň
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
(	(	kIx(	(
<g/>
tumor	tumor	k1gInSc1	tumor
nebo	nebo	k8xC	nebo
absces	absces	k1gInSc1	absces
<g/>
)	)	kIx)	)
či	či	k8xC	či
při	při	k7c6	při
zvýšeném	zvýšený	k2eAgInSc6d1	zvýšený
nitrolebním	nitrolební	k2eAgInSc6d1	nitrolební
tlaku	tlak	k1gInSc6	tlak
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
vyhřeznutí	vyhřeznutí	k1gNnSc3	vyhřeznutí
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
u	u	k7c2	u
pacienta	pacient	k1gMnSc2	pacient
existuje	existovat	k5eAaImIp3nS	existovat
riziko	riziko	k1gNnSc1	riziko
nahromadění	nahromadění	k1gNnSc2	nahromadění
tkáně	tkáň	k1gFnSc2	tkáň
nebo	nebo	k8xC	nebo
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
nitrolebního	nitrolební	k2eAgInSc2d1	nitrolební
tlaku	tlak	k1gInSc2	tlak
(	(	kIx(	(
<g/>
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nedávného	dávný	k2eNgNnSc2d1	nedávné
poranění	poranění	k1gNnSc2	poranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
diagnostikovaných	diagnostikovaný	k2eAgFnPc2d1	diagnostikovaná
poruch	porucha	k1gFnPc2	porucha
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
lokalizačních	lokalizační	k2eAgInPc2d1	lokalizační
neurologických	neurologický	k2eAgInPc2d1	neurologický
symptomů	symptom	k1gInPc2	symptom
nebo	nebo	k8xC	nebo
průkazně	průkazně	k6eAd1	průkazně
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
nitrolebního	nitrolební	k2eAgInSc2d1	nitrolební
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
před	před	k7c7	před
lumbální	lumbální	k2eAgFnSc7d1	lumbální
punkcí	punkce	k1gFnSc7	punkce
provést	provést	k5eAaPmF	provést
CT	CT	kA	CT
nebo	nebo	k8xC	nebo
MR	MR	kA	MR
vyšetření	vyšetření	k1gNnSc1	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
45	[number]	k4	45
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
případů	případ	k1gInPc2	případ
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
LP	LP	kA	LP
nutno	nutno	k6eAd1	nutno
provést	provést	k5eAaPmF	provést
CT	CT	kA	CT
nebo	nebo	k8xC	nebo
MR	MR	kA	MR
vyšetření	vyšetření	k1gNnSc1	vyšetření
či	či	k8xC	či
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc4d1	obtížné
LP	LP	kA	LP
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
odborných	odborný	k2eAgFnPc2d1	odborná
směrnic	směrnice	k1gFnPc2	směrnice
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
podána	podán	k2eAgNnPc1d1	podáno
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
opoždění	opoždění	k1gNnSc1	opoždění
účinků	účinek	k1gInPc2	účinek
léčby	léčba	k1gFnSc2	léčba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
dobu	doba	k1gFnSc4	doba
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
MR	MR	kA	MR
nebo	nebo	k8xC	nebo
CT	CT	kA	CT
sken	sken	k1gInSc1	sken
se	se	k3xPyFc4	se
často	často	k6eAd1	často
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
stadiu	stadion	k1gNnSc6	stadion
pro	pro	k7c4	pro
posouzení	posouzení	k1gNnSc4	posouzení
komplikací	komplikace	k1gFnPc2	komplikace
meningitidy	meningitida	k1gFnPc4	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těžkých	těžký	k2eAgInPc2d1	těžký
případů	případ	k1gInPc2	případ
meningitidy	meningitida	k1gFnSc2	meningitida
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
kontrola	kontrola	k1gFnSc1	kontrola
hladiny	hladina	k1gFnSc2	hladina
elektrolytů	elektrolyt	k1gInPc2	elektrolyt
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
;	;	kIx,	;
u	u	k7c2	u
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
běžná	běžný	k2eAgFnSc1d1	běžná
hyponatrémie	hyponatrémie	k1gFnSc1	hyponatrémie
vznikající	vznikající	k2eAgFnSc1d1	vznikající
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kombinace	kombinace	k1gFnSc2	kombinace
řady	řada	k1gFnSc2	řada
faktorů	faktor	k1gInPc2	faktor
včetně	včetně	k7c2	včetně
dehydratace	dehydratace	k1gFnSc2	dehydratace
<g/>
,	,	kIx,	,
nepřiměřeného	přiměřený	k2eNgNnSc2d1	nepřiměřené
vylučování	vylučování	k1gNnSc2	vylučování
antidiuretického	antidiuretický	k2eAgInSc2d1	antidiuretický
hormonu	hormon	k1gInSc2	hormon
(	(	kIx(	(
<g/>
SIADH	SIADH	kA	SIADH
<g/>
)	)	kIx)	)
či	či	k8xC	či
příliš	příliš	k6eAd1	příliš
razantního	razantní	k2eAgNnSc2d1	razantní
podávání	podávání	k1gNnSc2	podávání
nitrožilních	nitrožilní	k2eAgFnPc2d1	nitrožilní
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Lumbální	lumbální	k2eAgFnSc1d1	lumbální
punkce	punkce	k1gFnSc1	punkce
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
u	u	k7c2	u
pacienta	pacient	k1gMnSc2	pacient
ležícího	ležící	k2eAgInSc2d1	ležící
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
boku	bok	k1gInSc6	bok
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
lokálních	lokální	k2eAgNnPc2d1	lokální
anestetik	anestetikum	k1gNnPc2	anestetikum
<g/>
;	;	kIx,	;
do	do	k7c2	do
durálního	durální	k2eAgInSc2d1	durální
vaku	vak	k1gInSc2	vak
(	(	kIx(	(
<g/>
vak	vak	k1gInSc1	vak
přiléhající	přiléhající	k2eAgInSc1d1	přiléhající
k	k	k7c3	k
míše	mícha	k1gFnSc3	mícha
<g/>
)	)	kIx)	)
vpraví	vpravit	k5eAaPmIp3nS	vpravit
lékař	lékař	k1gMnSc1	lékař
injekční	injekční	k2eAgFnSc4d1	injekční
jehlu	jehla	k1gFnSc4	jehla
a	a	k8xC	a
odebere	odebrat	k5eAaPmIp3nS	odebrat
vzorek	vzorek	k1gInSc1	vzorek
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
odběru	odběr	k1gInSc2	odběr
se	se	k3xPyFc4	se
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
manometru	manometr	k1gInSc2	manometr
změří	změřit	k5eAaPmIp3nS	změřit
"	"	kIx"	"
<g/>
otevírací	otevírací	k2eAgInSc1d1	otevírací
tlak	tlak	k1gInSc1	tlak
<g/>
"	"	kIx"	"
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
normálně	normálně	k6eAd1	normálně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
6	[number]	k4	6
a	a	k8xC	a
18	[number]	k4	18
cm	cm	kA	cm
sloupce	sloupec	k1gInSc2	sloupec
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
cm	cm	kA	cm
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
u	u	k7c2	u
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
typicky	typicky	k6eAd1	typicky
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kryptokokové	kryptokokový	k2eAgFnSc2d1	kryptokokový
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
nadměrně	nadměrně	k6eAd1	nadměrně
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
nitrolební	nitrolební	k2eAgInSc1d1	nitrolební
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
moku	mok	k1gInSc2	mok
po	po	k7c6	po
odběru	odběr	k1gInSc6	odběr
může	moct	k5eAaImIp3nS	moct
naznačovat	naznačovat	k5eAaImF	naznačovat
povahu	povaha	k1gFnSc4	povaha
infekce	infekce	k1gFnSc2	infekce
-	-	kIx~	-
zakalený	zakalený	k2eAgInSc1d1	zakalený
mozkomíšní	mozkomíšní	k2eAgInSc1d1	mozkomíšní
mok	mok	k1gInSc1	mok
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
hladině	hladina	k1gFnSc6	hladina
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
bílých	bílý	k2eAgFnPc2d1	bílá
a	a	k8xC	a
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
příznakem	příznak	k1gInSc7	příznak
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Vzorek	vzorek	k1gInSc1	vzorek
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
zkoumán	zkoumat	k5eAaImNgInS	zkoumat
-	-	kIx~	-
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
se	se	k3xPyFc4	se
přítomnost	přítomnost	k1gFnSc1	přítomnost
a	a	k8xC	a
typ	typ	k1gInSc1	typ
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc4	obsah
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
hladina	hladina	k1gFnSc1	hladina
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
.	.	kIx.	.
</s>
<s>
Gramovo	Gramův	k2eAgNnSc1d1	Gramovo
barvení	barvení	k1gNnSc1	barvení
vzorku	vzorek	k1gInSc2	vzorek
může	moct	k5eAaImIp3nS	moct
prokázat	prokázat	k5eAaPmF	prokázat
přítomnost	přítomnost	k1gFnSc4	přítomnost
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
absence	absence	k1gFnSc2	absence
však	však	k9	však
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
meningitidu	meningitida	k1gFnSc4	meningitida
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
-	-	kIx~	-
bakterie	bakterie	k1gFnPc1	bakterie
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
patrné	patrný	k2eAgInPc1d1	patrný
v	v	k7c6	v
pouhých	pouhý	k2eAgInPc6d1	pouhý
60	[number]	k4	60
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
údaj	údaj	k1gInSc1	údaj
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
20	[number]	k4	20
%	%	kIx~	%
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
odebráním	odebrání	k1gNnSc7	odebrání
vzorku	vzorek	k1gInSc2	vzorek
podána	podán	k2eAgNnPc1d1	podáno
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jistých	jistý	k2eAgFnPc2d1	jistá
infekcí	infekce	k1gFnPc2	infekce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
listerióza	listerióza	k1gFnSc1	listerióza
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Gramovo	Gramův	k2eAgNnSc1d1	Gramovo
barvení	barvení	k1gNnSc1	barvení
méně	málo	k6eAd2	málo
průkazné	průkazný	k2eAgFnPc4d1	průkazná
<g/>
.	.	kIx.	.
</s>
<s>
Podstatně	podstatně	k6eAd1	podstatně
citlivější	citlivý	k2eAgFnSc7d2	citlivější
je	být	k5eAaImIp3nS	být
mikrobiologický	mikrobiologický	k2eAgInSc1d1	mikrobiologický
rozbor	rozbor	k1gInSc1	rozbor
vzorku	vzorek	k1gInSc2	vzorek
(	(	kIx(	(
<g/>
organismy	organismus	k1gInPc4	organismus
dokáže	dokázat	k5eAaPmIp3nS	dokázat
identifikovat	identifikovat	k5eAaBmF	identifikovat
v	v	k7c6	v
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
85	[number]	k4	85
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
než	než	k8xS	než
budou	být	k5eAaImBp3nP	být
výsledky	výsledek	k1gInPc4	výsledek
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
převažujících	převažující	k2eAgFnPc2d1	převažující
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
)	)	kIx)	)
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
meningitidu	meningitida	k1gFnSc4	meningitida
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
převažují	převažovat	k5eAaImIp3nP	převažovat
neutrofily	neutrofil	k1gMnPc7	neutrofil
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
virovou	virový	k2eAgFnSc4d1	virová
(	(	kIx(	(
<g/>
s	s	k7c7	s
převažujícími	převažující	k2eAgInPc7d1	převažující
lymfocyty	lymfocyt	k1gInPc7	lymfocyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
onemocnění	onemocnění	k1gNnSc2	onemocnění
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
spolehlivý	spolehlivý	k2eAgInSc4d1	spolehlivý
ukazatel	ukazatel	k1gInSc4	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
často	často	k6eAd1	často
převažují	převažovat	k5eAaImIp3nP	převažovat
eozinofily	eozinofil	k1gMnPc4	eozinofil
<g/>
,	,	kIx,	,
ukazující	ukazující	k2eAgInPc4d1	ukazující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
na	na	k7c4	na
parazitický	parazitický	k2eAgInSc4d1	parazitický
nebo	nebo	k8xC	nebo
fungální	fungální	k2eAgInSc4d1	fungální
původ	původ	k1gInSc4	původ
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
se	se	k3xPyFc4	se
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
40	[number]	k4	40
%	%	kIx~	%
hodnoty	hodnota	k1gFnSc2	hodnota
koncentrace	koncentrace	k1gFnSc2	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
;	;	kIx,	;
hladina	hladina	k1gFnSc1	hladina
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dělí	dělit	k5eAaImIp3nS	dělit
hodnotou	hodnota	k1gFnSc7	hodnota
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
(	(	kIx(	(
<g/>
poměr	poměr	k1gInSc1	poměr
koncentrace	koncentrace	k1gFnSc2	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
ku	k	k7c3	k
koncentraci	koncentrace	k1gFnSc3	koncentrace
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
krevním	krevní	k2eAgNnSc6d1	krevní
séru	sérum	k1gNnSc6	sérum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
≤	≤	k?	≤
<g/>
0,4	[number]	k4	0,4
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
meningitidu	meningitida	k1gFnSc4	meningitida
<g/>
;	;	kIx,	;
u	u	k7c2	u
novorozenců	novorozenec	k1gMnPc2	novorozenec
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
hladina	hladina	k1gFnSc1	hladina
glukózy	glukóza	k1gFnSc2	glukóza
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
obvykle	obvykle	k6eAd1	obvykle
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
abnormální	abnormální	k2eAgInSc4d1	abnormální
považován	považován	k2eAgInSc4d1	považován
poměr	poměr	k1gInSc4	poměr
nižší	nízký	k2eAgInSc4d2	nižší
než	než	k8xS	než
0,6	[number]	k4	0,6
(	(	kIx(	(
<g/>
60	[number]	k4	60
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
hladina	hladina	k1gFnSc1	hladina
laktátů	laktát	k1gInPc2	laktát
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
počet	počet	k1gInSc1	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
laktátů	laktát	k1gInPc2	laktát
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
35	[number]	k4	35
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
dl	dl	k?	dl
a	a	k8xC	a
nemocnému	nemocný	k1gMnSc3	nemocný
předtím	předtím	k6eAd1	předtím
nebyla	být	k5eNaImAgNnP	být
podána	podán	k2eAgNnPc1d1	podáno
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
meningitidu	meningitida	k1gFnSc4	meningitida
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
meningitidy	meningitida	k1gFnSc2	meningitida
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Latexový	latexový	k2eAgInSc1d1	latexový
aglutinační	aglutinační	k2eAgInSc1d1	aglutinační
test	test	k1gInSc1	test
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
u	u	k7c2	u
meningitidy	meningitida	k1gFnSc2	meningitida
způsobené	způsobený	k2eAgFnSc2d1	způsobená
bakteriemi	bakterie	k1gFnPc7	bakterie
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pneumoniae	pneumonia	k1gFnSc2	pneumonia
<g/>
,	,	kIx,	,
Neisseria	Neisserium	k1gNnSc2	Neisserium
meningitidis	meningitidis	k1gFnSc2	meningitidis
<g/>
,	,	kIx,	,
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
influenzae	influenza	k1gMnSc2	influenza
<g/>
,	,	kIx,	,
Escherichia	Escherichius	k1gMnSc2	Escherichius
coli	col	k1gFnSc2	col
a	a	k8xC	a
streptokok	streptokok	k1gMnSc1	streptokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
;	;	kIx,	;
rutinní	rutinní	k2eAgNnSc4d1	rutinní
užívání	užívání	k1gNnSc4	užívání
tohoto	tento	k3xDgInSc2	tento
testu	test	k1gInSc2	test
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
léčebného	léčebný	k2eAgInSc2d1	léčebný
postupu	postup	k1gInSc2	postup
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jiné	jiný	k2eAgInPc1d1	jiný
testy	test	k1gInPc1	test
diagnózu	diagnóza	k1gFnSc4	diagnóza
neprokázaly	prokázat	k5eNaPmAgInP	prokázat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
meningitidy	meningitida	k1gFnSc2	meningitida
způsobené	způsobený	k2eAgInPc1d1	způsobený
gramnegativními	gramnegativní	k2eAgFnPc7d1	gramnegativní
bakteriemi	bakterie	k1gFnPc7	bakterie
vykazovat	vykazovat	k5eAaImF	vykazovat
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
výsledky	výsledek	k1gInPc4	výsledek
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
limulus	limulus	k1gInSc1	limulus
test	test	k1gInSc1	test
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
omezeno	omezen	k2eAgNnSc1d1	omezeno
na	na	k7c4	na
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jiné	jiný	k2eAgInPc1d1	jiný
testy	test	k1gInPc1	test
selhaly	selhat	k5eAaPmAgInP	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Polymerázová	Polymerázový	k2eAgFnSc1d1	Polymerázová
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
PCR	PCR	kA	PCR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
užívaná	užívaný	k2eAgFnSc1d1	užívaná
ke	k	k7c3	k
zvýraznění	zvýraznění	k1gNnSc3	zvýraznění
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgNnPc2d1	malé
množství	množství	k1gNnPc2	množství
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zjistit	zjistit	k5eAaPmF	zjistit
přítomnost	přítomnost	k1gFnSc1	přítomnost
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
či	či	k8xC	či
virové	virový	k2eAgFnSc2d1	virová
DNA	DNA	kA	DNA
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vysoce	vysoce	k6eAd1	vysoce
citlivý	citlivý	k2eAgInSc4d1	citlivý
a	a	k8xC	a
specifický	specifický	k2eAgInSc4d1	specifický
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
odhalit	odhalit	k5eAaPmF	odhalit
stopová	stopový	k2eAgNnPc4d1	stopové
množství	množství	k1gNnPc4	množství
DNA	DNA	kA	DNA
činitelů	činitel	k1gInPc2	činitel
vyvolávajících	vyvolávající	k2eAgInPc2d1	vyvolávající
zánět	zánět	k1gInSc4	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
identifikovat	identifikovat	k5eAaBmF	identifikovat
bakterie	bakterie	k1gFnPc4	bakterie
u	u	k7c2	u
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
nebo	nebo	k8xC	nebo
pomoci	pomoc	k1gFnSc2	pomoc
rozlišit	rozlišit	k5eAaPmF	rozlišit
různé	různý	k2eAgFnPc4d1	různá
příčiny	příčina	k1gFnPc4	příčina
virové	virový	k2eAgFnPc4d1	virová
meningitidy	meningitida	k1gFnPc4	meningitida
(	(	kIx(	(
<g/>
enterovirus	enterovirus	k1gInSc1	enterovirus
<g/>
,	,	kIx,	,
virus	virus	k1gInSc1	virus
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gNnSc7	simplex
typu	typ	k1gInSc2	typ
2	[number]	k4	2
a	a	k8xC	a
virus	virus	k1gInSc1	virus
příušnic	příušnice	k1gFnPc2	příušnice
u	u	k7c2	u
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
očkováni	očkovat	k5eAaImNgMnP	očkovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
virové	virový	k2eAgFnSc2d1	virová
meningitidy	meningitida	k1gFnSc2	meningitida
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
užitečné	užitečný	k2eAgFnPc4d1	užitečná
provést	provést	k5eAaPmF	provést
sérologické	sérologický	k2eAgInPc4d1	sérologický
testy	test	k1gInPc4	test
(	(	kIx(	(
<g/>
identifikace	identifikace	k1gFnSc1	identifikace
protilátek	protilátka	k1gFnPc2	protilátka
virů	vir	k1gInPc2	vir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
<g/>
-li	i	k?	-li
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
tuberkulózní	tuberkulózní	k2eAgFnSc4d1	tuberkulózní
meningitidu	meningitida	k1gFnSc4	meningitida
<g/>
,	,	kIx,	,
vzorek	vzorek	k1gInSc1	vzorek
je	být	k5eAaImIp3nS	být
podroben	podrobit	k5eAaPmNgInS	podrobit
Ziehl-Neelsenově	Ziehl-Neelsenův	k2eAgNnSc6d1	Ziehl-Neelsenův
barvení	barvení	k1gNnSc1	barvení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
typická	typický	k2eAgFnSc1d1	typická
nízká	nízký	k2eAgFnSc1d1	nízká
senzitivita	senzitivita	k1gFnSc1	senzitivita
<g/>
,	,	kIx,	,
a	a	k8xC	a
testům	test	k1gInPc3	test
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
činitelů	činitel	k1gMnPc2	činitel
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
zpracování	zpracování	k1gNnSc1	zpracování
trvá	trvat	k5eAaImIp3nS	trvat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
;	;	kIx,	;
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
využívána	využívat	k5eAaImNgFnS	využívat
polymerázová	polymerázový	k2eAgFnSc1d1	polymerázová
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Kryptokokovou	Kryptokokový	k2eAgFnSc4d1	Kryptokokový
meningitidu	meningitida	k1gFnSc4	meningitida
lze	lze	k6eAd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
s	s	k7c7	s
vynaložením	vynaložení	k1gNnSc7	vynaložení
minimálních	minimální	k2eAgInPc2d1	minimální
nákladů	náklad	k1gInPc2	náklad
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
barvení	barvení	k1gNnSc4	barvení
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
;	;	kIx,	;
testy	test	k1gInPc1	test
na	na	k7c4	na
zjištění	zjištění	k1gNnSc4	zjištění
kryptokokových	kryptokokový	k2eAgInPc2d1	kryptokokový
antigenů	antigen	k1gInPc2	antigen
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
nebo	nebo	k8xC	nebo
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
citlivější	citlivý	k2eAgFnPc1d2	citlivější
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
trpících	trpící	k2eAgMnPc2d1	trpící
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
diagnostického	diagnostický	k2eAgNnSc2d1	diagnostické
a	a	k8xC	a
léčebného	léčebný	k2eAgNnSc2d1	léčebné
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
problémem	problém	k1gInSc7	problém
tzv.	tzv.	kA	tzv.
částečně	částečně	k6eAd1	částečně
léčená	léčený	k2eAgFnSc1d1	léčená
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
symptomy	symptom	k1gInPc1	symptom
meningitidy	meningitida	k1gFnSc2	meningitida
po	po	k7c4	po
podání	podání	k1gNnSc4	podání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
předpokládané	předpokládaný	k2eAgFnSc2d1	předpokládaná
sinusitidy	sinusitida	k1gFnSc2	sinusitida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
podobat	podobat	k5eAaImF	podobat
těm	ten	k3xDgMnPc3	ten
při	při	k7c6	při
virové	virový	k2eAgFnSc6d1	virová
meningitidě	meningitida	k1gFnSc6	meningitida
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nebude	být	k5eNaImBp3nS	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
příčina	příčina	k1gFnSc1	příčina
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
virová	virový	k2eAgFnSc1d1	virová
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
výsledky	výsledek	k1gInPc4	výsledek
polymerázové	polymerázový	k2eAgFnSc2d1	polymerázová
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
enteroviry	enterovir	k1gMnPc4	enterovir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meningitidu	meningitida	k1gFnSc4	meningitida
lze	lze	k6eAd1	lze
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
i	i	k9	i
po	po	k7c4	po
úmrtí	úmrtí	k1gNnSc4	úmrtí
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Pitevním	pitevní	k2eAgInSc7d1	pitevní
nálezem	nález	k1gInSc7	nález
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
široce	široko	k6eAd1	široko
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
zánět	zánět	k1gInSc1	zánět
měkké	měkký	k2eAgFnSc2d1	měkká
pleny	plena	k1gFnSc2	plena
mozkové	mozkový	k2eAgFnSc2d1	mozková
a	a	k8xC	a
vrstev	vrstva	k1gFnPc2	vrstva
pavučnice	pavučnice	k1gFnSc2	pavučnice
na	na	k7c6	na
mozkomíšních	mozkomíšní	k2eAgFnPc6d1	mozkomíšní
plenách	plena	k1gFnPc6	plena
<g/>
.	.	kIx.	.
</s>
<s>
Neutrofilní	neutrofilní	k2eAgInPc1d1	neutrofilní
granulocyty	granulocyt	k1gInPc1	granulocyt
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
přesunuly	přesunout	k5eAaPmAgInP	přesunout
do	do	k7c2	do
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
a	a	k8xC	a
spodní	spodní	k2eAgFnSc2d1	spodní
části	část	k1gFnSc2	část
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
a	a	k8xC	a
kraniální	kraniální	k2eAgInPc1d1	kraniální
nervy	nerv	k1gInPc1	nerv
a	a	k8xC	a
mícha	mícha	k1gFnSc1	mícha
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
obaleny	obalen	k2eAgInPc1d1	obalen
hnisem	hnis	k1gInSc7	hnis
–	–	k?	–
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
meningeální	meningeální	k2eAgFnPc4d1	meningeální
cévy	céva	k1gFnPc4	céva
<g/>
.	.	kIx.	.
</s>
<s>
Ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
některými	některý	k3yIgFnPc7	některý
příčinami	příčina	k1gFnPc7	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
lze	lze	k6eAd1	lze
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
výhledu	výhled	k1gInSc6	výhled
vakcinací	vakcinace	k1gFnPc2	vakcinace
<g/>
,	,	kIx,	,
v	v	k7c6	v
krátkodobé	krátkodobý	k2eAgFnSc6d1	krátkodobá
perspektivě	perspektiva	k1gFnSc6	perspektiva
pak	pak	k6eAd1	pak
podáváním	podávání	k1gNnSc7	podávání
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgMnPc1d1	efektivní
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
jistá	jistý	k2eAgNnPc4d1	jisté
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
vedou	vést	k5eAaImIp3nP	vést
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
i	i	k8xC	i
virová	virový	k2eAgFnSc1d1	virová
meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
nakažlivá	nakažlivý	k2eAgFnSc1d1	nakažlivá
<g/>
;	;	kIx,	;
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
však	však	k9	však
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
infekční	infekční	k2eAgNnSc1d1	infekční
jako	jako	k8xC	jako
nachlazení	nachlazení	k1gNnSc1	nachlazení
nebo	nebo	k8xC	nebo
chřipka	chřipka	k1gFnSc1	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nP	přenášet
kapičkami	kapička	k1gFnPc7	kapička
sekretu	sekret	k1gInSc2	sekret
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
při	při	k7c6	při
blízkém	blízký	k2eAgInSc6d1	blízký
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
líbání	líbání	k1gNnSc1	líbání
<g/>
,	,	kIx,	,
kýchání	kýchání	k1gNnSc1	kýchání
nebo	nebo	k8xC	nebo
kašlání	kašlání	k1gNnSc1	kašlání
<g/>
,	,	kIx,	,
nešíří	šířit	k5eNaImIp3nS	šířit
se	se	k3xPyFc4	se
však	však	k9	však
pouhým	pouhý	k2eAgNnSc7d1	pouhé
vdechováním	vdechování	k1gNnSc7	vdechování
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
meningitidou	meningitida	k1gFnSc7	meningitida
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
způsobována	způsobován	k2eAgFnSc1d1	způsobována
enteroviry	enterovir	k1gInPc7	enterovir
a	a	k8xC	a
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
způsobem	způsob	k1gInSc7	způsob
jejího	její	k3xOp3gNnSc2	její
šíření	šíření	k1gNnSc2	šíření
je	být	k5eAaImIp3nS	být
fekální	fekální	k2eAgFnSc1d1	fekální
kontaminace	kontaminace	k1gFnSc1	kontaminace
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
nákazy	nákaza	k1gFnSc2	nákaza
lze	lze	k6eAd1	lze
snížit	snížit	k5eAaPmF	snížit
změnami	změna	k1gFnPc7	změna
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
k	k	k7c3	k
přenosu	přenos	k1gInSc3	přenos
nemoci	nemoc	k1gFnSc2	nemoc
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
imunizaci	imunizace	k1gFnSc4	imunizace
proti	proti	k7c3	proti
bakterii	bakterie	k1gFnSc3	bakterie
Haemophilus	Haemophilus	k1gInSc4	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
typu	typ	k1gInSc2	typ
B	B	kA	B
do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
programů	program	k1gInPc2	program
rutinního	rutinní	k2eAgNnSc2d1	rutinní
očkování	očkování	k1gNnSc2	očkování
dětí	dítě	k1gFnPc2	dítě
mnoho	mnoho	k4c4	mnoho
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
prakticky	prakticky	k6eAd1	prakticky
eliminovalo	eliminovat	k5eAaBmAgNnS	eliminovat
tento	tento	k3xDgInSc4	tento
patogen	patogen	k1gInSc4	patogen
jakožto	jakožto	k8xS	jakožto
příčinu	příčina	k1gFnSc4	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
onemocněním	onemocnění	k1gNnSc7	onemocnění
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
vakcína	vakcína	k1gFnSc1	vakcína
stále	stále	k6eAd1	stále
příliš	příliš	k6eAd1	příliš
drahá	drahý	k2eAgFnSc1d1	drahá
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
vedla	vést	k5eAaImAgFnS	vést
imunizace	imunizace	k1gFnSc1	imunizace
proti	proti	k7c3	proti
příušnicím	příušnice	k1gFnPc3	příušnice
k	k	k7c3	k
prudkému	prudký	k2eAgInSc3d1	prudký
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
případů	případ	k1gInPc2	případ
příušnicové	příušnicový	k2eAgFnSc2d1	příušnicový
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
před	před	k7c7	před
vakcinací	vakcinace	k1gFnPc2	vakcinace
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
15	[number]	k4	15
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
případů	případ	k1gInPc2	případ
příušnic	příušnice	k1gFnPc2	příušnice
<g/>
.	.	kIx.	.
</s>
<s>
Meningokokové	meningokokový	k2eAgFnPc1d1	meningokoková
vakcíny	vakcína	k1gFnPc1	vakcína
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
proti	proti	k7c3	proti
meningokokům	meningokok	k1gInPc3	meningokok
skupiny	skupina	k1gFnSc2	skupina
A	A	kA	A
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
W135	W135	k1gFnSc1	W135
a	a	k8xC	a
Y.	Y.	kA	Y.
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
meningokokům	meningokok	k1gInPc3	meningokok
skupiny	skupina	k1gFnSc2	skupina
C	C	kA	C
zavedena	zavést	k5eAaPmNgNnP	zavést
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
významnému	významný	k2eAgNnSc3d1	významné
snížení	snížení	k1gNnSc3	snížení
počtu	počet	k1gInSc2	počet
případů	případ	k1gInPc2	případ
způsobených	způsobený	k2eAgInPc2d1	způsobený
tímto	tento	k3xDgInSc7	tento
patogenem	patogen	k1gInSc7	patogen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
kvadrivalentní	kvadrivalentní	k2eAgFnSc1d1	kvadrivalentní
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
všech	všecek	k3xTgFnPc2	všecek
čtyř	čtyři	k4xCgFnPc2	čtyři
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Imunizace	imunizace	k1gFnSc1	imunizace
vakcínou	vakcína	k1gFnSc7	vakcína
ACW135Y	ACW135Y	k1gFnPc4	ACW135Y
proti	proti	k7c3	proti
všem	všecek	k3xTgInPc3	všecek
čtyřem	čtyři	k4xCgInPc3	čtyři
kmenům	kmen	k1gInPc3	kmen
meningokoků	meningokok	k1gInPc2	meningokok
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
požadavkem	požadavek	k1gInSc7	požadavek
i	i	k9	i
k	k	k7c3	k
udělení	udělení	k1gNnSc3	udělení
víza	vízo	k1gNnSc2	vízo
pro	pro	k7c4	pro
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
pouti	pouť	k1gFnSc6	pouť
Hadždž	Hadždž	k1gFnSc4	Hadždž
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
meningokokům	meningokok	k1gInPc3	meningokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jejich	jejich	k3xOp3gInPc1	jejich
povrchové	povrchový	k2eAgInPc1d1	povrchový
proteiny	protein	k1gInPc1	protein
(	(	kIx(	(
<g/>
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
sloužily	sloužit	k5eAaImAgFnP	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
)	)	kIx)	)
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
jen	jen	k9	jen
slabou	slabý	k2eAgFnSc4d1	slabá
reakci	reakce	k1gFnSc4	reakce
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
nebo	nebo	k8xC	nebo
reagují	reagovat	k5eAaBmIp3nP	reagovat
křížově	křížově	k6eAd1	křížově
s	s	k7c7	s
běžnými	běžný	k2eAgInPc7d1	běžný
lidskými	lidský	k2eAgInPc7d1	lidský
proteiny	protein	k1gInPc7	protein
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
a	a	k8xC	a
Chile	Chile	k1gNnSc1	Chile
<g/>
)	)	kIx)	)
však	však	k9	však
proti	proti	k7c3	proti
místním	místní	k2eAgInPc3d1	místní
kmenům	kmen	k1gInPc3	kmen
meningokoků	meningokok	k1gInPc2	meningokok
skupiny	skupina	k1gFnPc1	skupina
B	B	kA	B
dokázaly	dokázat	k5eAaPmAgFnP	dokázat
vakcíny	vakcína	k1gFnPc4	vakcína
vyvinout	vyvinout	k5eAaPmF	vyvinout
<g/>
;	;	kIx,	;
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
dobré	dobrý	k2eAgInPc4d1	dobrý
výsledky	výsledek	k1gInPc4	výsledek
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
imunizačních	imunizační	k2eAgInPc6d1	imunizační
programech	program	k1gInPc6	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
byl	být	k5eAaImAgMnS	být
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
prevenci	prevence	k1gFnSc3	prevence
a	a	k8xC	a
kontrole	kontrola	k1gFnSc3	kontrola
meningokokových	meningokokový	k2eAgFnPc2d1	meningokoková
epidemií	epidemie	k1gFnPc2	epidemie
až	až	k9	až
donedávna	donedávna	k6eAd1	donedávna
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
včasném	včasný	k2eAgNnSc6d1	včasné
odhalení	odhalení	k1gNnSc6	odhalení
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
reaktivním	reaktivní	k2eAgNnSc6d1	reaktivní
hromadném	hromadný	k2eAgNnSc6d1	hromadné
očkování	očkování	k1gNnSc6	očkování
ohrožené	ohrožený	k2eAgFnSc2d1	ohrožená
populace	populace	k1gFnSc2	populace
bivalentními	bivalentní	k2eAgFnPc7d1	bivalentní
A	a	k9	a
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
nebo	nebo	k8xC	nebo
trivalentními	trivalentní	k2eAgFnPc7d1	trivalentní
A	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
W	W	kA	W
<g/>
135	[number]	k4	135
polysacharidovými	polysacharidový	k2eAgFnPc7d1	polysacharidová
vakcínami	vakcína	k1gFnPc7	vakcína
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zavedení	zavedení	k1gNnSc1	zavedení
preparátu	preparát	k1gInSc2	preparát
MenAfriVac	MenAfriVac	k1gFnSc1	MenAfriVac
(	(	kIx(	(
<g/>
vakcína	vakcína	k1gFnSc1	vakcína
proti	proti	k7c3	proti
meningokokům	meningokok	k1gInPc3	meningokok
skupiny	skupina	k1gFnSc2	skupina
A	a	k8xC	a
<g/>
)	)	kIx)	)
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
jeho	jeho	k3xOp3gFnSc4	jeho
účinnost	účinnost	k1gFnSc4	účinnost
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
vyzdviženo	vyzdvihnout	k5eAaPmNgNnS	vyzdvihnout
jako	jako	k8xS	jako
příkladný	příkladný	k2eAgInSc1d1	příkladný
model	model	k1gInSc1	model
partnerství	partnerství	k1gNnSc2	partnerství
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
produktu	produkt	k1gInSc2	produkt
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
omezenými	omezený	k2eAgInPc7d1	omezený
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Rutinní	rutinní	k2eAgFnSc1d1	rutinní
vakcinace	vakcinace	k1gFnSc1	vakcinace
proti	proti	k7c3	proti
bakterii	bakterie	k1gFnSc3	bakterie
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
pneumoniae	pneumoniae	k6eAd1	pneumoniae
pneumokokovou	pneumokokový	k2eAgFnSc7d1	pneumokoková
konjugovanou	konjugovaný	k2eAgFnSc7d1	konjugovaná
vakcínou	vakcína	k1gFnSc7	vakcína
(	(	kIx(	(
<g/>
PCV	PCV	kA	PCV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
proti	proti	k7c3	proti
sedmi	sedm	k4xCc3	sedm
běžným	běžný	k2eAgInPc3d1	běžný
sérotypům	sérotyp	k1gInPc3	sérotyp
tohoto	tento	k3xDgInSc2	tento
patogenu	patogen	k1gInSc2	patogen
<g/>
,	,	kIx,	,
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
snižuje	snižovat	k5eAaImIp3nS	snižovat
počet	počet	k1gInSc1	počet
případů	případ	k1gInPc2	případ
pneumokokové	pneumokokový	k2eAgFnSc2d1	pneumokoková
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Pneumokoková	Pneumokokový	k2eAgFnSc1d1	Pneumokoková
polysacharidová	polysacharidový	k2eAgFnSc1d1	polysacharidová
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
proti	proti	k7c3	proti
23	[number]	k4	23
kmenům	kmen	k1gInPc3	kmen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podávána	podáván	k2eAgFnSc1d1	podávána
pouze	pouze	k6eAd1	pouze
určitým	určitý	k2eAgFnPc3d1	určitá
skupinám	skupina	k1gFnPc3	skupina
nemocných	nemocný	k2eAgMnPc2d1	nemocný
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
podstoupili	podstoupit	k5eAaPmAgMnP	podstoupit
splenektomii	splenektomie	k1gFnSc4	splenektomie
<g/>
,	,	kIx,	,
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
odnětí	odnětí	k1gNnSc4	odnětí
sleziny	slezina	k1gFnSc2	slezina
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nevyvolává	vyvolávat	k5eNaImIp3nS	vyvolávat
totiž	totiž	k9	totiž
významnou	významný	k2eAgFnSc4d1	významná
imunitní	imunitní	k2eAgFnSc4d1	imunitní
reakci	reakce	k1gFnSc4	reakce
u	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
příjemců	příjemce	k1gMnPc2	příjemce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Očkování	očkování	k1gNnSc1	očkování
dětí	dítě	k1gFnPc2	dítě
Calmette-Guérinovým	Calmette-Guérinův	k2eAgInSc7d1	Calmette-Guérinův
bacilem	bacil	k1gInSc7	bacil
výrazně	výrazně	k6eAd1	výrazně
snižuje	snižovat	k5eAaImIp3nS	snižovat
počet	počet	k1gInSc1	počet
případů	případ	k1gInPc2	případ
tuberkulózní	tuberkulózní	k2eAgFnSc2d1	tuberkulózní
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
mizivá	mizivý	k2eAgFnSc1d1	mizivá
účinnost	účinnost	k1gFnSc1	účinnost
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
však	však	k9	však
podněcuje	podněcovat	k5eAaImIp3nS	podněcovat
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
lepší	dobrý	k2eAgFnSc2d2	lepší
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
metodou	metoda	k1gFnSc7	metoda
prevence	prevence	k1gFnSc2	prevence
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
meningokokové	meningokokový	k2eAgFnSc2d1	meningokoková
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
antibiotická	antibiotický	k2eAgFnSc1d1	antibiotická
profylaxe	profylaxe	k1gFnSc1	profylaxe
(	(	kIx(	(
<g/>
preventivní	preventivní	k2eAgFnSc1d1	preventivní
léčba	léčba	k1gFnSc1	léčba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případech	případ	k1gInPc6	případ
meningokokové	meningokokový	k2eAgFnSc2d1	meningokoková
meningitidy	meningitida	k1gFnSc2	meningitida
může	moct	k5eAaImIp3nS	moct
preventivní	preventivní	k2eAgFnSc1d1	preventivní
léčba	léčba	k1gFnSc1	léčba
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
styku	styk	k1gInSc6	styk
s	s	k7c7	s
nakaženým	nakažený	k2eAgInSc7d1	nakažený
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rifampicinu	rifampicin	k2eAgFnSc4d1	rifampicin
<g/>
,	,	kIx,	,
ciprofloxacinu	ciprofloxacin	k2eAgFnSc4d1	ciprofloxacin
nebo	nebo	k8xC	nebo
ceftriaxonu	ceftriaxona	k1gFnSc4	ceftriaxona
<g/>
)	)	kIx)	)
snížit	snížit	k5eAaPmF	snížit
riziko	riziko	k1gNnSc4	riziko
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
však	však	k9	však
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
budoucími	budoucí	k2eAgFnPc7d1	budoucí
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podání	podání	k1gNnSc6	podání
rifampicinu	rifampicin	k2eAgFnSc4d1	rifampicin
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
rezistence	rezistence	k1gFnSc1	rezistence
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
někteří	některý	k3yIgMnPc1	některý
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
zvážit	zvážit	k5eAaPmF	zvážit
jiné	jiný	k2eAgInPc4d1	jiný
přípravky	přípravek	k1gInPc4	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zabránit	zabránit	k5eAaPmF	zabránit
meningitidě	meningitida	k1gFnSc3	meningitida
často	často	k6eAd1	často
používána	používán	k2eAgFnSc1d1	používána
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
bazilární	bazilární	k2eAgFnSc7d1	bazilární
frakturou	fraktura	k1gFnSc7	fraktura
lebky	lebka	k1gFnSc2	lebka
,	,	kIx,	,
neexistuje	existovat	k5eNaImIp3nS	existovat
dostatek	dostatek	k1gInSc1	dostatek
důkazů	důkaz	k1gInPc2	důkaz
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
nebo	nebo	k8xC	nebo
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
u	u	k7c2	u
pacientů	pacient	k1gMnPc2	pacient
s	s	k7c7	s
únikem	únik	k1gInSc7	únik
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
i	i	k8xC	i
bez	bez	k7c2	bez
úniku	únik	k1gInSc2	únik
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
představuje	představovat	k5eAaImIp3nS	představovat
potenciální	potenciální	k2eAgNnSc1d1	potenciální
ohrožení	ohrožení	k1gNnSc1	ohrožení
života	život	k1gInSc2	život
a	a	k8xC	a
neléčená	léčený	k2eNgFnSc1d1	neléčená
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
vysokou	vysoká	k1gFnSc7	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
;	;	kIx,	;
opoždění	opoždění	k1gNnSc1	opoždění
léčby	léčba	k1gFnSc2	léčba
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
horších	zlý	k2eAgInPc6d2	horší
výsledcích	výsledek	k1gInPc6	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
širokospektrálními	širokospektrální	k2eAgFnPc7d1	širokospektrální
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
odkládána	odkládat	k5eAaImNgFnS	odkládat
kvůli	kvůli	k7c3	kvůli
čekání	čekání	k1gNnSc3	čekání
na	na	k7c4	na
výsledky	výsledek	k1gInPc4	výsledek
potvrzujících	potvrzující	k2eAgInPc2d1	potvrzující
testů	test	k1gInPc2	test
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
meningokokové	meningokokový	k2eAgNnSc4d1	meningokokové
onemocnění	onemocnění	k1gNnSc4	onemocnění
existuje	existovat	k5eAaImIp3nS	existovat
už	už	k9	už
během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
vyšetření	vyšetření	k1gNnSc2	vyšetření
<g/>
,	,	kIx,	,
směrnice	směrnice	k1gFnSc2	směrnice
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
podání	podání	k1gNnSc3	podání
benzylpenicilinu	benzylpenicilin	k1gInSc2	benzylpenicilin
ještě	ještě	k9	ještě
před	před	k7c7	před
převozem	převoz	k1gInSc7	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
hypotenze	hypotenze	k1gFnSc2	hypotenze
(	(	kIx(	(
<g/>
nízkého	nízký	k2eAgInSc2d1	nízký
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
šoku	šok	k1gInSc2	šok
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
podány	podán	k2eAgFnPc4d1	podána
nitrožilní	nitrožilní	k2eAgFnPc4d1	nitrožilní
tekutiny	tekutina	k1gFnPc4	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
meningitida	meningitida	k1gFnSc1	meningitida
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
řadu	řada	k1gFnSc4	řada
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
zhoršujících	zhoršující	k2eAgFnPc2d1	zhoršující
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
,	,	kIx,	,
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
se	se	k3xPyFc4	se
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
lékařské	lékařský	k2eAgFnPc1d1	lékařská
prohlídky	prohlídka	k1gFnPc1	prohlídka
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
včasného	včasný	k2eAgNnSc2d1	včasné
rozpoznání	rozpoznání	k1gNnSc2	rozpoznání
těchto	tento	k3xDgFnPc2	tento
komplikací	komplikace	k1gFnPc2	komplikace
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
osoby	osoba	k1gFnSc2	osoba
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Mechanická	mechanický	k2eAgFnSc1d1	mechanická
ventilace	ventilace	k1gFnSc1	ventilace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
při	při	k7c6	při
poruchách	porucha	k1gFnPc6	porucha
vědomí	vědomí	k1gNnSc2	vědomí
nebo	nebo	k8xC	nebo
při	při	k7c6	při
respiračním	respirační	k2eAgNnSc6d1	respirační
selhání	selhání	k1gNnSc6	selhání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
příznaků	příznak	k1gInPc2	příznak
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
nitrolebního	nitrolební	k2eAgInSc2d1	nitrolební
tlaku	tlak	k1gInSc2	tlak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
podniknuta	podniknut	k2eAgNnPc4d1	podniknuto
opatření	opatření	k1gNnPc4	opatření
vedoucí	vedoucí	k2eAgNnPc4d1	vedoucí
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
soustavné	soustavný	k2eAgFnSc3d1	soustavná
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
umožní	umožnit	k5eAaPmIp3nS	umožnit
optimalizaci	optimalizace	k1gFnSc4	optimalizace
mozkového	mozkový	k2eAgInSc2d1	mozkový
perfúzního	perfúzní	k2eAgInSc2d1	perfúzní
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
způsobů	způsob	k1gInPc2	způsob
léčby	léčba	k1gFnSc2	léčba
vedoucích	vedoucí	k1gFnPc2	vedoucí
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nitrolebního	nitrolební	k2eAgInSc2d1	nitrolební
tlaku	tlak	k1gInSc2	tlak
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
léků	lék	k1gInPc2	lék
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
mannitolu	mannitol	k1gInSc3	mannitol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záchvaty	záchvat	k1gInPc1	záchvat
jsou	být	k5eAaImIp3nP	být
léčeny	léčen	k2eAgInPc1d1	léčen
antikonvulzivy	antikonvulziv	k1gInPc1	antikonvulziv
<g/>
.	.	kIx.	.
</s>
<s>
Hydrocefalus	hydrocefalus	k1gInSc1	hydrocefalus
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc1	omezení
průtoku	průtok	k1gInSc2	průtok
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
dočasné	dočasný	k2eAgNnSc4d1	dočasné
nebo	nebo	k8xC	nebo
dlouhodobé	dlouhodobý	k2eAgNnSc4d1	dlouhodobé
vložení	vložení	k1gNnSc4	vložení
drenážního	drenážní	k2eAgNnSc2d1	drenážní
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
mozkového	mozkový	k2eAgInSc2d1	mozkový
shuntu	shunt	k1gInSc2	shunt
<g/>
.	.	kIx.	.
</s>
<s>
Empirická	empirický	k2eAgFnSc1d1	empirická
léčba	léčba	k1gFnSc1	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
(	(	kIx(	(
<g/>
léčba	léčba	k1gFnSc1	léčba
bez	bez	k7c2	bez
přesné	přesný	k2eAgFnSc2d1	přesná
diagnózy	diagnóza	k1gFnSc2	diagnóza
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
výsledky	výsledek	k1gInPc1	výsledek
lumbální	lumbální	k2eAgFnSc2d1	lumbální
punkce	punkce	k1gFnSc2	punkce
a	a	k8xC	a
analýzy	analýza	k1gFnSc2	analýza
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
počáteční	počáteční	k2eAgFnSc2d1	počáteční
léčby	léčba	k1gFnSc2	léčba
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
meningitidu	meningitida	k1gFnSc4	meningitida
v	v	k7c6	v
jisté	jistý	k2eAgFnSc6d1	jistá
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
u	u	k7c2	u
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
empirická	empirický	k2eAgFnSc1d1	empirická
léčba	léčba	k1gFnSc1	léčba
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
podávání	podávání	k1gNnSc2	podávání
cefalosporinů	cefalosporin	k1gInPc2	cefalosporin
třetí	třetí	k4xOgFnSc2	třetí
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
cefotaxim	cefotaxim	k1gInSc4	cefotaxim
nebo	nebo	k8xC	nebo
ceftriaxon	ceftriaxon	k1gInSc4	ceftriaxon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
streptokoků	streptokok	k1gMnPc2	streptokok
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
rezistence	rezistence	k1gFnSc1	rezistence
na	na	k7c4	na
cefalosporiny	cefalosporin	k1gInPc4	cefalosporin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
přidat	přidat	k5eAaPmF	přidat
k	k	k7c3	k
počáteční	počáteční	k2eAgFnSc3d1	počáteční
léčbě	léčba	k1gFnSc3	léčba
vankomycin	vankomycin	k1gInSc4	vankomycin
<g/>
.	.	kIx.	.
</s>
<s>
Chloramfenikol	chloramfenikol	k1gInSc1	chloramfenikol
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
ampicilinem	ampicilin	k1gInSc7	ampicilin
<g/>
,	,	kIx,	,
však	však	k9	však
funguje	fungovat	k5eAaImIp3nS	fungovat
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
empirické	empirický	k2eAgFnSc3d1	empirická
léčbě	léčba	k1gFnSc3	léčba
lze	lze	k6eAd1	lze
přistoupit	přistoupit	k5eAaPmF	přistoupit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
věku	věk	k1gInSc2	věk
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
infekci	infekce	k1gFnSc3	infekce
předcházelo	předcházet	k5eAaImAgNnS	předcházet
poranění	poranění	k1gNnSc1	poranění
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
osoba	osoba	k1gFnSc1	osoba
nedávno	nedávno	k6eAd1	nedávno
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
neurochirurgický	neurochirurgický	k2eAgInSc4d1	neurochirurgický
zákrok	zákrok	k1gInSc4	zákrok
a	a	k8xC	a
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
mozkový	mozkový	k2eAgInSc1d1	mozkový
shunt	shunt	k1gInSc1	shunt
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
starších	starší	k1gMnPc2	starší
50	[number]	k4	50
let	léto	k1gNnPc2	léto
a	a	k8xC	a
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
imunitou	imunita	k1gFnSc7	imunita
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
podání	podání	k1gNnSc1	podání
ampicilinu	ampicilin	k1gInSc2	ampicilin
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
pokrytí	pokrytí	k1gNnSc1	pokrytí
bakterie	bakterie	k1gFnSc2	bakterie
Listeria	Listerium	k1gNnSc2	Listerium
monocytogenes	monocytogenesa	k1gFnPc2	monocytogenesa
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
výsledky	výsledek	k1gInPc1	výsledek
Gramova	Gramův	k2eAgNnSc2d1	Gramovo
barvení	barvení	k1gNnSc2	barvení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
znám	znám	k2eAgInSc1d1	znám
typ	typ	k1gInSc1	typ
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
příčiny	příčina	k1gFnSc2	příčina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
změnit	změnit	k5eAaPmF	změnit
podávaná	podávaný	k2eAgNnPc1d1	podávané
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
na	na	k7c4	na
taková	takový	k3xDgNnPc4	takový
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
s	s	k7c7	s
domnělou	domnělý	k2eAgFnSc7d1	domnělá
skupinou	skupina	k1gFnSc7	skupina
patogenů	patogen	k1gInPc2	patogen
poradí	poradit	k5eAaPmIp3nS	poradit
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
rozboru	rozbor	k1gInSc2	rozbor
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgFnPc4d1	možná
empirickou	empirický	k2eAgFnSc4d1	empirická
léčbu	léčba	k1gFnSc4	léčba
změnit	změnit	k5eAaPmF	změnit
na	na	k7c4	na
specifickou	specifický	k2eAgFnSc4d1	specifická
antibiotickou	antibiotický	k2eAgFnSc4d1	antibiotická
léčbu	léčba	k1gFnSc4	léčba
zacílenou	zacílený	k2eAgFnSc4d1	zacílená
na	na	k7c4	na
konkrétního	konkrétní	k2eAgMnSc4d1	konkrétní
organického	organický	k2eAgMnSc4d1	organický
původce	původce	k1gMnSc4	původce
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
citlivost	citlivost	k1gFnSc4	citlivost
na	na	k7c4	na
antibiotika	antibiotikum	k1gNnPc4	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
meningitidy	meningitida	k1gFnSc2	meningitida
účinné	účinný	k2eAgFnSc2d1	účinná
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nejen	nejen	k6eAd1	nejen
aktivní	aktivní	k2eAgMnSc1d1	aktivní
proti	proti	k7c3	proti
patogenní	patogenní	k2eAgFnSc3d1	patogenní
bakterii	bakterie	k1gFnSc3	bakterie
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
v	v	k7c6	v
adekvátním	adekvátní	k2eAgNnSc6d1	adekvátní
množství	množství	k1gNnSc6	množství
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
mozkomíšním	mozkomíšní	k2eAgFnPc3d1	mozkomíšní
plenám	plena	k1gFnPc3	plena
<g/>
;	;	kIx,	;
některá	některý	k3yIgNnPc1	některý
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
nepronikají	pronikat	k5eNaImIp3nP	pronikat
k	k	k7c3	k
cílovým	cílový	k2eAgInPc3d1	cílový
orgánům	orgán	k1gInPc3	orgán
adekvátně	adekvátně	k6eAd1	adekvátně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
meningitidy	meningitida	k1gFnSc2	meningitida
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
užitek	užitek	k1gInSc4	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
používaných	používaný	k2eAgNnPc2d1	používané
k	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
meningitidy	meningitida	k1gFnSc2	meningitida
nebyla	být	k5eNaImAgFnS	být
během	během	k7c2	během
klinických	klinický	k2eAgNnPc2d1	klinické
hodnocení	hodnocení	k1gNnSc2	hodnocení
testována	testovat	k5eAaImNgFnS	testovat
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
osobách	osoba	k1gFnPc6	osoba
s	s	k7c7	s
meningitidou	meningitida	k1gFnSc7	meningitida
-	-	kIx~	-
relevantní	relevantní	k2eAgFnPc1d1	relevantní
znalosti	znalost	k1gFnPc1	znalost
jsou	být	k5eAaImIp3nP	být
povětšinou	povětšinou	k6eAd1	povětšinou
odvozovány	odvozován	k2eAgInPc1d1	odvozován
od	od	k7c2	od
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
studií	studie	k1gFnPc2	studie
na	na	k7c6	na
králících	králík	k1gMnPc6	králík
<g/>
.	.	kIx.	.
</s>
<s>
Tuberkulózní	tuberkulózní	k2eAgFnSc1d1	tuberkulózní
meningitida	meningitida	k1gFnSc1	meningitida
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
léčbu	léčba	k1gFnSc4	léčba
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
plic	plíce	k1gFnPc2	plíce
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
léčí	léčit	k5eAaImIp3nP	léčit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
léčba	léčba	k1gFnSc1	léčba
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
tuberkulózní	tuberkulózní	k2eAgFnSc7d1	tuberkulózní
meningitidou	meningitida	k1gFnSc7	meningitida
zpravidla	zpravidla	k6eAd1	zpravidla
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k9	i
rok	rok	k1gInSc4	rok
či	či	k8xC	či
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Pomocná	pomocný	k2eAgFnSc1d1	pomocná
léčba	léčba	k1gFnSc1	léčba
kortikosteroidy	kortikosteroid	k1gInPc1	kortikosteroid
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
dexametazon	dexametazon	k1gNnSc1	dexametazon
<g/>
)	)	kIx)	)
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
určité	určitý	k2eAgFnPc4d1	určitá
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
snížení	snížení	k1gNnSc1	snížení
rozsahu	rozsah	k1gInSc2	rozsah
možné	možný	k2eAgFnSc2d1	možná
ztráty	ztráta	k1gFnSc2	ztráta
sluchu	sluch	k1gInSc2	sluch
a	a	k8xC	a
lepší	dobrý	k2eAgInPc4d2	lepší
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
neurologické	neurologický	k2eAgInPc4d1	neurologický
výsledky	výsledek	k1gInPc4	výsledek
u	u	k7c2	u
dospívajících	dospívající	k2eAgMnPc2d1	dospívající
a	a	k8xC	a
dospělých	dospělý	k2eAgMnPc2d1	dospělý
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
vyššími	vysoký	k2eAgInPc7d2	vyšší
příjmy	příjem	k1gInPc7	příjem
a	a	k8xC	a
nízkým	nízký	k2eAgInSc7d1	nízký
počtem	počet	k1gInSc7	počet
případů	případ	k1gInPc2	případ
nákazy	nákaza	k1gFnSc2	nákaza
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
<s>
Jistý	jistý	k2eAgInSc1d1	jistý
výzkum	výzkum	k1gInSc1	výzkum
zjistil	zjistit	k5eAaPmAgInS	zjistit
nižší	nízký	k2eAgFnSc4d2	nižší
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
užívání	užívání	k1gNnSc6	užívání
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiný	jiný	k1gMnSc1	jiný
ji	on	k3xPp3gFnSc4	on
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Steroidy	steroid	k1gInPc1	steroid
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
užitečné	užitečný	k2eAgInPc1d1	užitečný
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
tuberkulózní	tuberkulózní	k2eAgFnSc7d1	tuberkulózní
meningitidou	meningitida	k1gFnSc7	meningitida
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
HIV	HIV	kA	HIV
negativních	negativní	k2eAgFnPc2d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
směrnice	směrnice	k1gFnSc1	směrnice
proto	proto	k8xC	proto
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
podáváním	podávání	k1gNnSc7	podávání
dexametazonu	dexametazon	k1gInSc2	dexametazon
nebo	nebo	k8xC	nebo
obdobného	obdobný	k2eAgInSc2d1	obdobný
kortikosteroidu	kortikosteroid	k1gInSc2	kortikosteroid
ještě	ještě	k9	ještě
před	před	k7c7	před
podáním	podání	k1gNnSc7	podání
první	první	k4xOgFnSc2	první
dávky	dávka	k1gFnSc2	dávka
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
podávání	podávání	k1gNnSc4	podávání
pokračovat	pokračovat	k5eAaImF	pokračovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
čtyř	čtyři	k4xCgInPc2	čtyři
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výhody	výhoda	k1gFnPc1	výhoda
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
léčby	léčba	k1gFnSc2	léčba
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
omezeny	omezit	k5eAaPmNgFnP	omezit
na	na	k7c4	na
pneumokokovou	pneumokokový	k2eAgFnSc4d1	pneumokoková
meningitidu	meningitida	k1gFnSc4	meningitida
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
směrnice	směrnice	k1gFnPc1	směrnice
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
vysazení	vysazení	k1gNnSc4	vysazení
dexametazonu	dexametazon	k1gInSc2	dexametazon
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zjistí	zjistit	k5eAaPmIp3nS	zjistit
jiná	jiný	k2eAgFnSc1d1	jiná
příčina	příčina	k1gFnSc1	příčina
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobným	pravděpodobný	k2eAgInSc7d1	pravděpodobný
mechanismem	mechanismus	k1gInSc7	mechanismus
je	být	k5eAaImIp3nS	být
potlačení	potlačení	k1gNnSc1	potlačení
abnormálně	abnormálně	k6eAd1	abnormálně
aktivního	aktivní	k2eAgInSc2d1	aktivní
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Užití	užití	k1gNnSc1	užití
pomocných	pomocný	k2eAgInPc2d1	pomocný
kortikosteroidů	kortikosteroid	k1gInPc2	kortikosteroid
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
odlišný	odlišný	k2eAgInSc4d1	odlišný
dopad	dopad	k1gInSc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
příjmy	příjem	k1gInPc7	příjem
byl	být	k5eAaImAgInS	být
účinek	účinek	k1gInSc1	účinek
kortikosteroidů	kortikosteroid	k1gInPc2	kortikosteroid
prokázán	prokázán	k2eAgMnSc1d1	prokázán
jak	jak	k8xS	jak
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc3	jejich
použití	použití	k1gNnSc1	použití
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
příjmynení	příjmynení	k1gNnSc1	příjmynení
podpořeno	podpořit	k5eAaPmNgNnS	podpořit
důkazy	důkaz	k1gInPc7	důkaz
<g/>
;	;	kIx,	;
důvod	důvod	k1gInSc1	důvod
tohoto	tento	k3xDgInSc2	tento
rozporu	rozpor	k1gInSc2	rozpor
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ve	v	k7c6	v
vysokopříjmových	vysokopříjmův	k2eAgFnPc6d1	vysokopříjmův
zemích	zem	k1gFnPc6	zem
je	být	k5eAaImIp3nS	být
však	však	k9	však
účinek	účinek	k1gInSc1	účinek
kortikosteroidů	kortikosteroid	k1gInPc2	kortikosteroid
pozorován	pozorovat	k5eAaImNgInS	pozorovat
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podány	podat	k5eAaPmNgInP	podat
před	před	k7c7	před
první	první	k4xOgFnSc7	první
dávkou	dávka	k1gFnSc7	dávka
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
v	v	k7c6	v
případech	případ	k1gInPc6	případ
meningitidy	meningitida	k1gFnSc2	meningitida
způsobené	způsobený	k2eAgFnSc2d1	způsobená
bakterií	bakterie	k1gFnSc7	bakterie
H.	H.	kA	H.
influenzae	influenzae	k1gFnSc7	influenzae
<g/>
;	;	kIx,	;
výskyt	výskyt	k1gInSc1	výskyt
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
meningitidy	meningitida	k1gFnSc2	meningitida
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
Hib	Hib	k1gFnSc2	Hib
vakcíny	vakcína	k1gFnSc2	vakcína
dramaticky	dramaticky	k6eAd1	dramaticky
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Kortikosteroidy	kortikosteroid	k1gInPc1	kortikosteroid
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
doporučovaným	doporučovaný	k2eAgInSc7d1	doporučovaný
léčebným	léčebný	k2eAgInSc7d1	léčebný
prostředkem	prostředek	k1gInSc7	prostředek
u	u	k7c2	u
pediatrické	pediatrický	k2eAgFnSc2d1	pediatrická
meningitidy	meningitida	k1gFnSc2	meningitida
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
H.	H.	kA	H.
influenzae	influenzae	k1gFnSc7	influenzae
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
podány	podat	k5eAaPmNgInP	podat
před	před	k7c7	před
první	první	k4xOgFnSc7	první
dávkou	dávka	k1gFnSc7	dávka
antibiotik	antibiotikum	k1gNnPc2	antibiotikum
<g/>
;	;	kIx,	;
ostatní	ostatní	k2eAgInPc1d1	ostatní
způsoby	způsob	k1gInPc1	způsob
použití	použití	k1gNnSc2	použití
jsou	být	k5eAaImIp3nP	být
kontroverzní	kontroverzní	k2eAgInPc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
meningitida	meningitida	k1gFnSc1	meningitida
obvykle	obvykle	k6eAd1	obvykle
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pouze	pouze	k6eAd1	pouze
podpůrnou	podpůrný	k2eAgFnSc4d1	podpůrná
léčbu	léčba	k1gFnSc4	léčba
<g/>
;	;	kIx,	;
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
virů	vir	k1gInPc2	vir
odpovědných	odpovědný	k2eAgInPc2d1	odpovědný
za	za	k7c4	za
vyvolání	vyvolání	k1gNnSc4	vyvolání
meningitidy	meningitida	k1gFnSc2	meningitida
se	se	k3xPyFc4	se
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
specifická	specifický	k2eAgFnSc1d1	specifická
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Virová	virový	k2eAgFnSc1d1	virová
meningitida	meningitida	k1gFnSc1	meningitida
mívá	mívat	k5eAaImIp3nS	mívat
méně	málo	k6eAd2	málo
obtížný	obtížný	k2eAgInSc1d1	obtížný
průběh	průběh	k1gInSc1	průběh
než	než	k8xS	než
meningitida	meningitida	k1gFnSc1	meningitida
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
<g/>
.	.	kIx.	.
</s>
<s>
Virus	virus	k1gInSc1	virus
Herpes	herpes	k1gInSc1	herpes
simplex	simplex	k1gInSc1	simplex
a	a	k8xC	a
virus	virus	k1gInSc1	virus
Varicella	Varicella	k1gMnSc1	Varicella
zoster	zoster	k1gMnSc1	zoster
může	moct	k5eAaImIp3nS	moct
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
antivirotiky	antivirotika	k1gFnSc2	antivirotika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
acyclovir	acyclovir	k1gInSc4	acyclovir
<g/>
,	,	kIx,	,
nebyla	být	k5eNaImAgFnS	být
však	však	k9	však
provedena	provést	k5eAaPmNgFnS	provést
žádná	žádný	k3yNgNnPc1	žádný
klinická	klinický	k2eAgNnPc1d1	klinické
hodnocení	hodnocení	k1gNnPc1	hodnocení
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
léčba	léčba	k1gFnSc1	léčba
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
Lehčí	lehký	k2eAgInPc1d2	lehčí
případy	případ	k1gInPc1	případ
virové	virový	k2eAgFnSc2d1	virová
meningitidy	meningitida	k1gFnSc2	meningitida
lze	lze	k6eAd1	lze
léčit	léčit	k5eAaImF	léčit
doma	doma	k6eAd1	doma
při	při	k7c6	při
uplatnění	uplatnění	k1gNnSc6	uplatnění
obvyklých	obvyklý	k2eAgFnPc2d1	obvyklá
konzervativních	konzervativní	k2eAgFnPc2d1	konzervativní
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dostatečného	dostatečný	k2eAgInSc2d1	dostatečný
příjmu	příjem	k1gInSc2	příjem
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
,	,	kIx,	,
klidu	klid	k1gInSc2	klid
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
a	a	k8xC	a
podávání	podávání	k1gNnSc6	podávání
analgetik	analgetikum	k1gNnPc2	analgetikum
<g/>
.	.	kIx.	.
</s>
<s>
Fungální	Fungálnit	k5eAaPmIp3nS	Fungálnit
(	(	kIx(	(
<g/>
plísňová	plísňový	k2eAgFnSc1d1	plísňová
<g/>
)	)	kIx)	)
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
meningitida	meningitida	k1gFnSc1	meningitida
kryptokoková	kryptokokový	k2eAgFnSc1d1	kryptokokový
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
léčena	léčit	k5eAaImNgFnS	léčit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
podávání	podávání	k1gNnSc2	podávání
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
fungicidů	fungicid	k1gInPc2	fungicid
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
amfotericinu	amfotericin	k2eAgFnSc4d1	amfotericin
B	B	kA	B
a	a	k8xC	a
flucytosinu	flucytosina	k1gFnSc4	flucytosina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
fungální	fungální	k2eAgFnSc2d1	fungální
meningitidy	meningitida	k1gFnSc2	meningitida
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc1d1	běžný
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
nitrolební	nitrolební	k2eAgInSc1d1	nitrolební
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
častá	častý	k2eAgFnSc1d1	častá
(	(	kIx(	(
<g/>
nejlépe	dobře	k6eAd3	dobře
každodenní	každodenní	k2eAgFnSc2d1	každodenní
<g/>
)	)	kIx)	)
lumbální	lumbální	k2eAgFnSc2d1	lumbální
punkce	punkce	k1gFnSc2	punkce
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
lumbální	lumbální	k2eAgInSc4d1	lumbální
drén	drén	k1gInSc4	drén
pro	pro	k7c4	pro
uvolnění	uvolnění	k1gNnSc4	uvolnění
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Neléčená	léčený	k2eNgFnSc1d1	neléčená
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
<g/>
,	,	kIx,	,
virová	virový	k2eAgFnSc1d1	virová
meningitida	meningitida	k1gFnSc1	meningitida
naopak	naopak	k6eAd1	naopak
spontánně	spontánně	k6eAd1	spontánně
ustupuje	ustupovat	k5eAaImIp3nS	ustupovat
a	a	k8xC	a
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
léčby	léčba	k1gFnSc2	léčba
závisí	záviset	k5eAaImIp3nS	záviset
mortalita	mortalita	k1gFnSc1	mortalita
(	(	kIx(	(
<g/>
riziko	riziko	k1gNnSc1	riziko
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
)	)	kIx)	)
u	u	k7c2	u
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
na	na	k7c6	na
věku	věk	k1gInSc6	věk
osoby	osoba	k1gFnSc2	osoba
a	a	k8xC	a
základní	základní	k2eAgFnSc6d1	základní
příčině	příčina	k1gFnSc6	příčina
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
novorozenců	novorozenec	k1gMnPc2	novorozenec
nakažených	nakažený	k2eAgFnPc2d1	nakažená
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
meningitidou	meningitida	k1gFnSc7	meningitida
umírá	umírat	k5eAaImIp3nS	umírat
20-30	[number]	k4	20-30
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
2	[number]	k4	2
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
se	se	k3xPyFc4	se
ale	ale	k9	ale
opět	opět	k6eAd1	opět
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
na	na	k7c4	na
19-37	[number]	k4	19-37
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
úmrtí	úmrtí	k1gNnSc2	úmrtí
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
věku	věk	k1gInSc2	věk
určováno	určován	k2eAgNnSc1d1	určováno
i	i	k8xC	i
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
druhem	druh	k1gInSc7	druh
patogenu	patogen	k1gInSc2	patogen
a	a	k8xC	a
časem	časem	k6eAd1	časem
potřebným	potřebný	k2eAgNnSc7d1	potřebné
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
patogenu	patogen	k1gInSc2	patogen
z	z	k7c2	z
mozkomíšního	mozkomíšní	k2eAgInSc2d1	mozkomíšní
moku	mok	k1gInSc2	mok
<g/>
,	,	kIx,	,
celkovou	celkový	k2eAgFnSc7d1	celková
závažností	závažnost	k1gFnSc7	závažnost
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
poruchami	porucha	k1gFnPc7	porucha
vědomí	vědomí	k1gNnSc2	vědomí
či	či	k8xC	či
abnormálně	abnormálně	k6eAd1	abnormálně
nízkým	nízký	k2eAgInSc7d1	nízký
počtem	počet	k1gInSc7	počet
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
v	v	k7c6	v
mozkomíšním	mozkomíšní	k2eAgInSc6d1	mozkomíšní
moku	mok	k1gInSc6	mok
<g/>
.	.	kIx.	.
</s>
<s>
Meningitida	meningitida	k1gFnSc1	meningitida
způsobenáH	způsobenáH	k?	způsobenáH
<g/>
.	.	kIx.	.
influenzae	influenza	k1gFnPc4	influenza
a	a	k8xC	a
meningokoky	meningokok	k1gInPc4	meningokok
má	mít	k5eAaImIp3nS	mít
lepší	dobrý	k2eAgFnSc4d2	lepší
prognózu	prognóza	k1gFnSc4	prognóza
než	než	k8xS	než
případy	případ	k1gInPc1	případ
způsobené	způsobený	k2eAgInPc1d1	způsobený
streptokoky	streptokok	k1gInPc1	streptokok
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
koliformními	koliformní	k2eAgFnPc7d1	koliformní
bakteriemi	bakterie	k1gFnPc7	bakterie
a	a	k8xC	a
S.	S.	kA	S.
pneumonia	pneumonium	k1gNnSc2	pneumonium
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
je	být	k5eAaImIp3nS	být
meningokoková	meningokokový	k2eAgFnSc1d1	meningokoková
meningitida	meningitida	k1gFnSc1	meningitida
spojována	spojovat	k5eAaImNgFnS	spojovat
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
než	než	k8xS	než
meningitida	meningitida	k1gFnSc1	meningitida
pneumokoková	pneumokokový	k2eAgFnSc1d1	pneumokoková
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
potenciálních	potenciální	k2eAgNnPc2d1	potenciální
postižení	postižení	k1gNnPc2	postižení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
poškození	poškození	k1gNnSc2	poškození
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
percepční	percepční	k2eAgFnSc2d1	percepční
nedoslýchavosti	nedoslýchavost	k1gFnSc2	nedoslýchavost
<g/>
,	,	kIx,	,
epilepsie	epilepsie	k1gFnSc1	epilepsie
<g/>
,	,	kIx,	,
poruch	poruch	k1gInSc1	poruch
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
snížené	snížený	k2eAgFnSc2d1	snížená
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
postižením	postižení	k1gNnSc7	postižení
dochází	docházet	k5eAaImIp3nS	docházet
u	u	k7c2	u
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
%	%	kIx~	%
přeživších	přeživší	k2eAgFnPc2d1	přeživší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
sluchu	sluch	k1gInSc2	sluch
vratná	vratný	k2eAgFnSc1d1	vratná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
končí	končit	k5eAaImIp3nS	končit
66	[number]	k4	66
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
případů	případ	k1gInPc2	případ
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
postižení	postižení	k1gNnSc2	postižení
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
problémy	problém	k1gInPc7	problém
jsou	být	k5eAaImIp3nP	být
hluchota	hluchota	k1gFnSc1	hluchota
(	(	kIx(	(
<g/>
ve	v	k7c6	v
14	[number]	k4	14
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
a	a	k8xC	a
poruchy	porucha	k1gFnPc1	porucha
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
v	v	k7c6	v
10	[number]	k4	10
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
meningitida	meningitida	k1gFnSc1	meningitida
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
nemocí	nemoc	k1gFnPc2	nemoc
podléhající	podléhající	k2eAgNnSc1d1	podléhající
hlášení	hlášení	k1gNnSc1	hlášení
<g/>
,	,	kIx,	,
přesná	přesný	k2eAgFnSc1d1	přesná
relativní	relativní	k2eAgFnSc1d1	relativní
incidence	incidence	k1gFnSc1	incidence
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
meningitida	meningitida	k1gFnSc1	meningitida
ročně	ročně	k6eAd1	ročně
objevuje	objevovat	k5eAaImIp3nS	objevovat
u	u	k7c2	u
3	[number]	k4	3
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgFnPc1d1	populační
studie	studie	k1gFnPc1	studie
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
virová	virový	k2eAgFnSc1d1	virová
meningitida	meningitida	k1gFnSc1	meningitida
je	být	k5eAaImIp3nS	být
běžnější	běžný	k2eAgFnSc1d2	běžnější
-	-	kIx~	-
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
10,9	[number]	k4	10,9
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
výskyt	výskyt	k1gInSc1	výskyt
je	být	k5eAaImIp3nS	být
častější	častý	k2eAgInSc1d2	častější
v	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgInSc1d2	vyšší
počet	počet	k1gInSc1	počet
případů	případ	k1gInPc2	případ
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
45,8	[number]	k4	45,8
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Subsaharská	subsaharský	k2eAgFnSc1d1	subsaharská
Afrika	Afrika	k1gFnSc1	Afrika
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
sto	sto	k4xCgNnSc4	sto
letech	let	k1gInPc6	let
soužena	soužit	k5eAaImNgFnS	soužit
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
epidemiemi	epidemie	k1gFnPc7	epidemie
meningokokové	meningokokový	k2eAgFnSc2d1	meningokoková
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
dostalo	dostat	k5eAaPmAgNnS	dostat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
meningitický	meningitický	k2eAgInSc1d1	meningitický
pás	pás	k1gInSc1	pás
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
epidemiím	epidemie	k1gFnPc3	epidemie
typicky	typicky	k6eAd1	typicky
dochází	docházet	k5eAaImIp3nS	docházet
během	během	k7c2	během
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
(	(	kIx(	(
<g/>
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
následná	následný	k2eAgFnSc1d1	následná
epidemická	epidemický	k2eAgFnSc1d1	epidemická
vlna	vlna	k1gFnSc1	vlna
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
odeznívá	odeznívat	k5eAaImIp3nS	odeznívat
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
100-800	[number]	k4	100-800
případů	případ	k1gInPc2	případ
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
přispívá	přispívat	k5eAaImIp3nS	přispívat
i	i	k9	i
nízká	nízký	k2eAgFnSc1d1	nízká
kvalita	kvalita	k1gFnSc1	kvalita
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
případy	případ	k1gInPc4	případ
onemocnění	onemocnění	k1gNnPc2	onemocnění
jsou	být	k5eAaImIp3nP	být
způsobovány	způsobovat	k5eAaImNgInP	způsobovat
převážně	převážně	k6eAd1	převážně
meningokoky	meningokok	k1gInPc1	meningokok
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
historicky	historicky	k6eAd1	historicky
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
epidemie	epidemie	k1gFnSc1	epidemie
postihla	postihnout	k5eAaPmAgFnS	postihnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
celý	celý	k2eAgInSc4d1	celý
region	region	k1gInSc4	region
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
250	[number]	k4	250
000	[number]	k4	000
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
25	[number]	k4	25
000	[number]	k4	000
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Meningokokové	meningokokový	k2eAgNnSc1d1	meningokokové
onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
epidemií	epidemie	k1gFnPc2	epidemie
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spolu	spolu	k6eAd1	spolu
poprvé	poprvé	k6eAd1	poprvé
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
styku	styk	k1gInSc2	styk
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
vojenských	vojenský	k2eAgFnPc6d1	vojenská
kasárnách	kasárny	k1gFnPc6	kasárny
během	během	k7c2	během
mobilizace	mobilizace	k1gFnSc2	mobilizace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
školních	školní	k2eAgInPc6d1	školní
kampusech	kampus	k1gInPc6	kampus
a	a	k8xC	a
při	při	k7c6	při
každoroční	každoroční	k2eAgFnSc6d1	každoroční
pouti	pouť	k1gFnSc6	pouť
Hadždž	Hadždž	k1gFnSc4	Hadždž
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
vzorec	vzorec	k1gInSc1	vzorec
epidemických	epidemický	k2eAgInPc2d1	epidemický
cyklů	cyklus	k1gInPc2	cyklus
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
není	být	k5eNaImIp3nS	být
dobře	dobře	k6eAd1	dobře
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
s	s	k7c7	s
šířením	šíření	k1gNnSc7	šíření
epidemií	epidemie	k1gFnPc2	epidemie
v	v	k7c6	v
meningitickém	meningitický	k2eAgInSc6d1	meningitický
pásu	pás	k1gInSc6	pás
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
několik	několik	k4yIc1	několik
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
faktory	faktor	k1gInPc4	faktor
patří	patřit	k5eAaImIp3nS	patřit
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
podmínky	podmínka	k1gFnPc1	podmínka
(	(	kIx(	(
<g/>
imunologická	imunologický	k2eAgFnSc1d1	imunologická
náchylnost	náchylnost	k1gFnSc1	náchylnost
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
demografické	demografický	k2eAgFnPc1d1	demografická
podmínky	podmínka	k1gFnPc1	podmínka
(	(	kIx(	(
<g/>
cestování	cestování	k1gNnSc1	cestování
a	a	k8xC	a
přesuny	přesun	k1gInPc1	přesun
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
socioekonomické	socioekonomický	k2eAgFnPc1d1	socioekonomická
podmínky	podmínka	k1gFnPc1	podmínka
(	(	kIx(	(
<g/>
přelidnění	přelidnění	k1gNnPc1	přelidnění
a	a	k8xC	a
špatné	špatný	k2eAgFnPc4d1	špatná
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
(	(	kIx(	(
<g/>
sucho	sucho	k6eAd1	sucho
a	a	k8xC	a
prachové	prachový	k2eAgFnPc4d1	prachová
bouře	bouř	k1gFnPc4	bouř
<g/>
)	)	kIx)	)
a	a	k8xC	a
souběžné	souběžný	k2eAgFnPc4d1	souběžná
infekce	infekce	k1gFnPc4	infekce
(	(	kIx(	(
<g/>
akutní	akutní	k2eAgFnPc4d1	akutní
infekce	infekce	k1gFnPc4	infekce
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
významné	významný	k2eAgInPc4d1	významný
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
příčiny	příčina	k1gFnPc4	příčina
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
lokálně	lokálně	k6eAd1	lokálně
rozloženy	rozložit	k5eAaPmNgFnP	rozložit
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
bakterie	bakterie	k1gFnSc2	bakterie
N.	N.	kA	N.
meningitides	meningitides	k1gInSc4	meningitides
skupin	skupina	k1gFnPc2	skupina
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nejvíce	nejvíce	k6eAd1	nejvíce
případů	případ	k1gInPc2	případ
onemocnění	onemocnění	k1gNnSc2	onemocnění
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
A	a	k8xC	a
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
i	i	k9	i
nadále	nadále	k6eAd1	nadále
převažuje	převažovat	k5eAaImIp3nS	převažovat
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
meningitickém	meningitický	k2eAgInSc6d1	meningitický
pásu	pás	k1gInSc6	pás
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
epidemie	epidemie	k1gFnPc4	epidemie
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
přičítá	přičítat	k5eAaImIp3nS	přičítat
80	[number]	k4	80
až	až	k9	až
85	[number]	k4	85
%	%	kIx~	%
zdokumentovaných	zdokumentovaný	k2eAgInPc2d1	zdokumentovaný
případů	případ	k1gInPc2	případ
meningokokové	meningokokový	k2eAgFnSc2d1	meningokoková
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existenci	existence	k1gFnSc4	existence
meningitidy	meningitida	k1gFnSc2	meningitida
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgInS	moct
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
už	už	k6eAd1	už
Hippokratés	Hippokratés	k1gInSc4	Hippokratés
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
<g/>
,	,	kIx,	,
že	že	k8xS	že
meningismus	meningismus	k1gInSc1	meningismus
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
také	také	k9	také
lékařům	lékař	k1gMnPc3	lékař
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
renesancí	renesance	k1gFnSc7	renesance
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
byl	být	k5eAaImAgInS	být
Avicenna	Avicenn	k1gMnSc4	Avicenn
<g/>
.	.	kIx.	.
</s>
<s>
Popis	popis	k1gInSc1	popis
tuberkulózní	tuberkulózní	k2eAgFnSc2d1	tuberkulózní
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
"	"	kIx"	"
<g/>
vodnatelnost	vodnatelnost	k1gFnSc1	vodnatelnost
<g/>
"	"	kIx"	"
mozku	mozek	k1gInSc2	mozek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přisuzován	přisuzován	k2eAgInSc1d1	přisuzován
edinburskému	edinburský	k2eAgMnSc3d1	edinburský
lékaři	lékař	k1gMnSc3	lékař
Siru	sir	k1gMnSc3	sir
Robertu	Robert	k1gMnSc3	Robert
Whyttovi	Whytt	k1gMnSc3	Whytt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
zprávě	zpráva	k1gFnSc6	zpráva
o	o	k7c6	o
úmrtí	úmrtí	k1gNnPc2	úmrtí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1768	[number]	k4	1768
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
souvislost	souvislost	k1gFnSc1	souvislost
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
patogenu	patogen	k1gInSc2	patogen
byla	být	k5eAaImAgFnS	být
objasněna	objasnit	k5eAaPmNgFnS	objasnit
až	až	k9	až
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
epidemie	epidemie	k1gFnPc1	epidemie
meningitidy	meningitida	k1gFnSc2	meningitida
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
současným	současný	k2eAgInSc7d1	současný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
velká	velký	k2eAgFnSc1d1	velká
epidemie	epidemie	k1gFnSc1	epidemie
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
epidemií	epidemie	k1gFnPc2	epidemie
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
epidemii	epidemie	k1gFnSc4	epidemie
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
africké	africký	k2eAgFnPc1d1	africká
epidemie	epidemie	k1gFnPc1	epidemie
staly	stát	k5eAaPmAgFnP	stát
mnohem	mnohem	k6eAd1	mnohem
běžnějšími	běžný	k2eAgFnPc7d2	běžnější
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířící	šířící	k2eAgFnSc1d1	šířící
epidemie	epidemie	k1gFnSc1	epidemie
v	v	k7c6	v
letech	let	k1gInPc6	let
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
postihly	postihnout	k5eAaPmAgFnP	postihnout
Nigérii	Nigérie	k1gFnSc4	Nigérie
a	a	k8xC	a
Ghanu	Ghana	k1gFnSc4	Ghana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
infekci	infekce	k1gFnSc4	infekce
způsobující	způsobující	k2eAgFnSc4d1	způsobující
meningitidu	meningitida	k1gFnSc4	meningitida
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
rakouského	rakouský	k2eAgMnSc2d1	rakouský
bakteriologa	bakteriolog	k1gMnSc2	bakteriolog
Antona	Anton	k1gMnSc2	Anton
Weichselbauma	Weichselbaum	k1gMnSc2	Weichselbaum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
popsal	popsat	k5eAaPmAgMnS	popsat
bakterii	bakterie	k1gFnSc4	bakterie
meningococcus	meningococcus	k1gMnSc1	meningococcus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
zprávách	zpráva	k1gFnPc6	zpráva
o	o	k7c6	o
nemoci	nemoc	k1gFnSc6	nemoc
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
na	na	k7c4	na
meningitidu	meningitida	k1gFnSc4	meningitida
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
antisérum	antisérum	k1gNnSc1	antisérum
účinné	účinný	k2eAgNnSc1d1	účinné
u	u	k7c2	u
koní	koní	k2eAgFnSc2d1	koní
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zdokonaleno	zdokonalit	k5eAaPmNgNnS	zdokonalit
americkým	americký	k2eAgMnSc7d1	americký
vědcem	vědec	k1gMnSc7	vědec
Simonem	Simon	k1gMnSc7	Simon
Flexnerem	Flexner	k1gMnSc7	Flexner
a	a	k8xC	a
značně	značně	k6eAd1	značně
snížilo	snížit	k5eAaPmAgNnS	snížit
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
na	na	k7c4	na
meningokokové	meningokokový	k2eAgNnSc4d1	meningokokové
onemocnění	onemocnění	k1gNnSc4	onemocnění
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
potvrzena	potvrzen	k2eAgFnSc1d1	potvrzena
účinnost	účinnost	k1gFnSc1	účinnost
penicilinu	penicilin	k1gInSc2	penicilin
u	u	k7c2	u
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
užívání	užívání	k1gNnSc2	užívání
vakcín	vakcína	k1gFnPc2	vakcína
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
na	na	k7c6	na
konci	konec	k1gInSc6	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
ke	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
případů	případ	k1gInPc2	případ
meningitidy	meningitida	k1gFnSc2	meningitida
spojovaných	spojovaný	k2eAgInPc2d1	spojovaný
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
patogenem	patogen	k1gInSc7	patogen
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prognózu	prognóza	k1gFnSc4	prognóza
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
meningitidy	meningitida	k1gFnSc2	meningitida
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
zlepšit	zlepšit	k5eAaPmF	zlepšit
léčba	léčba	k1gFnSc1	léčba
steroidy	steroid	k1gInPc4	steroid
<g/>
.	.	kIx.	.
</s>
