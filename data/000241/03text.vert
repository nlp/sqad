<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kinematografii	kinematografie	k1gFnSc4	kinematografie
a	a	k8xC	a
veškeré	veškerý	k3xTgInPc4	veškerý
její	její	k3xOp3gInPc4	její
aspekty	aspekt	k1gInPc4	aspekt
-	-	kIx~	-
umělecké	umělecký	k2eAgInPc4d1	umělecký
<g/>
,	,	kIx,	,
technické	technický	k2eAgInPc4d1	technický
<g/>
,	,	kIx,	,
komerční	komerční	k2eAgInPc4d1	komerční
a	a	k8xC	a
společenské	společenský	k2eAgInPc4d1	společenský
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tiskem	tisk	k1gInSc7	tisk
<g/>
,	,	kIx,	,
rozhlasem	rozhlas	k1gInSc7	rozhlas
a	a	k8xC	a
televizí	televize	k1gFnSc7	televize
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
už	už	k9	už
i	i	k9	i
internetem	internet	k1gInSc7	internet
patří	patřit	k5eAaImIp3nP	patřit
film	film	k1gInSc4	film
mezi	mezi	k7c4	mezi
komunikační	komunikační	k2eAgInPc4d1	komunikační
prostředky	prostředek	k1gInPc4	prostředek
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
dosahem	dosah	k1gInSc7	dosah
a	a	k8xC	a
vlivem	vliv	k1gInSc7	vliv
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
televizí	televize	k1gFnSc7	televize
bývá	bývat	k5eAaImIp3nS	bývat
řazen	řazen	k2eAgInSc1d1	řazen
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
scénická	scénický	k2eAgNnPc4d1	scénické
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
technickému	technický	k2eAgInSc3d1	technický
vynálezu	vynález	k1gInSc3	vynález
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
druh	druh	k1gInSc1	druh
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kulturního	kulturní	k2eAgInSc2d1	kulturní
<g/>
,	,	kIx,	,
společenského	společenský	k2eAgInSc2d1	společenský
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
fenoménu	fenomén	k1gInSc2	fenomén
došlo	dojít	k5eAaPmAgNnS	dojít
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
veřejné	veřejný	k2eAgNnSc4d1	veřejné
představení	představení	k1gNnSc4	představení
uskutečnili	uskutečnit	k5eAaPmAgMnP	uskutečnit
francouzští	francouzský	k2eAgMnPc1d1	francouzský
vynálezci	vynálezce	k1gMnPc1	vynálezce
kinematografického	kinematografický	k2eAgInSc2d1	kinematografický
přístroje	přístroj	k1gInSc2	přístroj
bratři	bratr	k1gMnPc1	bratr
Auguste	August	k1gMnSc5	August
a	a	k8xC	a
Louis	Louis	k1gMnSc1	Louis
Lumiè	Lumiè	k1gFnSc2	Lumiè
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1895	[number]	k4	1895
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
promítalo	promítat	k5eAaImAgNnS	promítat
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1896	[number]	k4	1896
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
srpna	srpen	k1gInSc2	srpen
1897	[number]	k4	1897
natočila	natočit	k5eAaBmAgFnS	natočit
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc4	první
film	film	k1gInSc4	film
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
věnovaný	věnovaný	k2eAgMnSc1d1	věnovaný
pašijovým	pašijový	k2eAgFnPc3d1	pašijová
hrám	hra	k1gFnPc3	hra
v	v	k7c6	v
Hořicích	Hořice	k1gFnPc6	Hořice
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
The	The	k1gFnSc1	The
Horitz	Horitz	k1gMnSc1	Horitz
Passion	Passion	k1gInSc1	Passion
Play	play	k0	play
uvedeny	uvést	k5eAaPmNgInP	uvést
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
české	český	k2eAgInPc1d1	český
snímky	snímek	k1gInPc1	snímek
natočil	natočit	k5eAaBmAgMnS	natočit
Jan	Jan	k1gMnSc1	Jan
Kříženecký	kříženecký	k2eAgMnSc1d1	kříženecký
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
je	být	k5eAaImIp3nS	být
promítal	promítat	k5eAaImAgMnS	promítat
na	na	k7c6	na
Výstavě	výstava	k1gFnSc6	výstava
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
inženýrství	inženýrství	k1gNnSc2	inženýrství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
pavilonu	pavilon	k1gInSc6	pavilon
Český	český	k2eAgInSc1d1	český
kinematograf	kinematograf	k1gInSc1	kinematograf
(	(	kIx(	(
<g/>
Svatojánská	svatojánský	k2eAgFnSc1d1	Svatojánská
pouť	pouť	k1gFnSc1	pouť
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgInSc4d1	poslední
výstřel	výstřel	k1gInSc4	výstřel
<g/>
,	,	kIx,	,
Purkyňovo	Purkyňův	k2eAgNnSc4d1	Purkyňovo
náměstí	náměstí	k1gNnSc4	náměstí
na	na	k7c6	na
Královských	královský	k2eAgInPc6d1	královský
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
realizoval	realizovat	k5eAaBmAgMnS	realizovat
hrané	hraný	k2eAgInPc4d1	hraný
snímky	snímek	k1gInPc4	snímek
<g/>
:	:	kIx,	:
Dostaveníčko	dostaveníčko	k1gNnSc1	dostaveníčko
v	v	k7c6	v
mlýnici	mlýnice	k1gFnSc6	mlýnice
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgMnSc1d1	výstavní
párkař	párkař	k1gMnSc1	párkař
a	a	k8xC	a
lepič	lepič	k1gMnSc1	lepič
plakátů	plakát	k1gInPc2	plakát
<g/>
,	,	kIx,	,
Smích	smích	k1gInSc1	smích
a	a	k8xC	a
pláč	pláč	k1gInSc1	pláč
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
filmových	filmový	k2eAgFnPc2d1	filmová
společností	společnost	k1gFnPc2	společnost
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Kinofa	Kinof	k1gMnSc2	Kinof
<g/>
,	,	kIx,	,
Ilusion	Ilusion	k1gInSc1	Ilusion
<g/>
,	,	kIx,	,
ASUM	ASUM	kA	ASUM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
kromě	kromě	k7c2	kromě
Lucernafilmu	Lucernafilm	k1gInSc2	Lucernafilm
<g/>
,	,	kIx,	,
založeného	založený	k2eAgInSc2d1	založený
a	a	k8xC	a
zařízeného	zařízený	k2eAgInSc2d1	zařízený
Milošem	Miloš	k1gMnSc7	Miloš
Havlem	Havel	k1gMnSc7	Havel
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
raného	raný	k2eAgNnSc2d1	rané
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
filmy	film	k1gInPc1	film
<g/>
:	:	kIx,	:
Zkažená	zkažený	k2eAgFnSc1d1	zkažená
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
tchýně	tchýně	k?	tchýně
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
nepřítel	nepřítel	k1gMnSc1	nepřítel
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ahasver	Ahasver	k1gMnSc1	Ahasver
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pražští	pražský	k2eAgMnPc1d1	pražský
adamité	adamita	k1gMnPc1	adamita
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
nastal	nastat	k5eAaPmAgInS	nastat
rozkvět	rozkvět	k1gInSc1	rozkvět
filmové	filmový	k2eAgFnSc2d1	filmová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vystavěny	vystavět	k5eAaPmNgFnP	vystavět
stálé	stálý	k2eAgFnPc1d1	stálá
filmové	filmový	k2eAgFnPc1d1	filmová
produkce	produkce	k1gFnPc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
filmy	film	k1gInPc4	film
promítali	promítat	k5eAaImAgMnP	promítat
bratři	bratr	k1gMnPc1	bratr
Lumiè	Lumiè	k1gFnPc2	Lumiè
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
(	(	kIx(	(
<g/>
příjezd	příjezd	k1gInSc4	příjezd
lokomotivy	lokomotiva	k1gFnSc2	lokomotiva
na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
<g/>
)	)	kIx)	)
a	a	k8xC	a
viděli	vidět	k5eAaImAgMnP	vidět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
pouhou	pouhý	k2eAgFnSc4d1	pouhá
pouťovou	pouťový	k2eAgFnSc4d1	pouťová
atrakci	atrakce	k1gFnSc4	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
zásluha	zásluha	k1gFnSc1	zásluha
za	za	k7c4	za
promítání	promítání	k1gNnSc4	promítání
filmu	film	k1gInSc2	film
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
sál	sál	k1gInSc4	sál
plný	plný	k2eAgInSc4d1	plný
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
další	další	k2eAgMnSc1d1	další
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
Georges	Georges	k1gMnSc1	Georges
Méliè	Méliè	k1gMnSc1	Méliè
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
film	film	k1gInSc4	film
vyprávět	vyprávět	k5eAaImF	vyprávět
příběhy	příběh	k1gInPc4	příběh
(	(	kIx(	(
<g/>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Měsíc	měsíc	k1gInSc4	měsíc
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
neznáma	neznámo	k1gNnSc2	neznámo
<g/>
,	,	kIx,	,
Dobytí	dobytí	k1gNnSc2	dobytí
pólu	pól	k1gInSc2	pól
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
film	film	k1gInSc1	film
stával	stávat	k5eAaImAgInS	stávat
médiem	médium	k1gNnSc7	médium
poměrně	poměrně	k6eAd1	poměrně
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
zejména	zejména	k9	zejména
žánr	žánr	k1gInSc4	žánr
grotesky	groteska	k1gFnSc2	groteska
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
byl	být	k5eAaImAgMnS	být
Charlie	Charlie	k1gMnSc1	Charlie
Chaplin	Chaplina	k1gFnPc2	Chaplina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
vesničce	vesnička	k1gFnSc6	vesnička
Hollywood	Hollywood	k1gInSc1	Hollywood
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
předměstí	předměstí	k1gNnPc1	předměstí
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
vybudována	vybudován	k2eAgNnPc1d1	vybudováno
filmová	filmový	k2eAgNnPc1d1	filmové
studia	studio	k1gNnPc1	studio
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
na	na	k7c4	na
největší	veliký	k2eAgFnSc4d3	veliký
světovou	světový	k2eAgFnSc4d1	světová
"	"	kIx"	"
<g/>
továrnu	továrna	k1gFnSc4	továrna
na	na	k7c4	na
sny	sen	k1gInPc4	sen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
získal	získat	k5eAaPmAgMnS	získat
film	film	k1gInSc4	film
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
černobílý	černobílý	k2eAgInSc4d1	černobílý
a	a	k8xC	a
nezvukový	zvukový	k2eNgInSc4d1	zvukový
<g/>
,	,	kIx,	,
natáčený	natáčený	k2eAgInSc4d1	natáčený
kamerou	kamera	k1gFnSc7	kamera
s	s	k7c7	s
ruční	ruční	k2eAgFnSc7d1	ruční
klikou	klika	k1gFnSc7	klika
<g/>
)	)	kIx)	)
různé	různý	k2eAgFnSc2d1	různá
technické	technický	k2eAgFnSc2d1	technická
vymoženosti	vymoženost	k1gFnSc2	vymoženost
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc1d1	zajišťující
rovnoměrné	rovnoměrný	k2eAgNnSc4d1	rovnoměrné
natáčení	natáčení	k1gNnSc4	natáčení
i	i	k8xC	i
promítání	promítání	k1gNnSc4	promítání
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
také	také	k9	také
zvuk	zvuk	k1gInSc1	zvuk
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
první	první	k4xOgInSc1	první
zvukový	zvukový	k2eAgInSc1d1	zvukový
film	film	k1gInSc1	film
prezentovaný	prezentovaný	k2eAgInSc1d1	prezentovaný
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
-	-	kIx~	-
Jazzový	jazzový	k2eAgMnSc1d1	jazzový
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
)	)	kIx)	)
a	a	k8xC	a
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
z	z	k7c2	z
filmu	film	k1gInSc2	film
stal	stát	k5eAaPmAgInS	stát
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
uměleckých	umělecký	k2eAgInPc2d1	umělecký
průmyslů	průmysl	k1gInPc2	průmysl
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInPc1d3	nejlepší
filmy	film	k1gInPc1	film
(	(	kIx(	(
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
herci	herec	k1gMnPc1	herec
<g/>
,	,	kIx,	,
režiséři	režisér	k1gMnPc1	režisér
<g/>
,	,	kIx,	,
kameramani	kameraman	k1gMnPc1	kameraman
<g/>
,	,	kIx,	,
střihači	střihač	k1gMnPc1	střihač
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
filmové	filmový	k2eAgFnPc1d1	filmová
profese	profes	k1gFnPc1	profes
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
odměňovány	odměňován	k2eAgFnPc1d1	odměňována
cenami	cena	k1gFnPc7	cena
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejprestižnější	prestižní	k2eAgFnSc1d3	nejprestižnější
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
Americké	americký	k2eAgFnSc2d1	americká
filmové	filmový	k2eAgFnSc2d1	filmová
akademie	akademie	k1gFnSc2	akademie
Oscar	Oscara	k1gFnPc2	Oscara
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
filmů	film	k1gInPc2	film
hraných	hraný	k2eAgInPc2d1	hraný
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgInP	prosadit
i	i	k9	i
filmy	film	k1gInPc1	film
animované	animovaný	k2eAgInPc1d1	animovaný
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
kreslené	kreslený	k2eAgNnSc1d1	kreslené
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
loutkové	loutkový	k2eAgFnPc1d1	loutková
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
filmové	filmový	k2eAgInPc1d1	filmový
triky	trik	k1gInPc1	trik
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tak	tak	k6eAd1	tak
někdy	někdy	k6eAd1	někdy
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
hraný	hraný	k2eAgInSc1d1	hraný
a	a	k8xC	a
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
se	se	k3xPyFc4	se
ale	ale	k9	ale
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
filmových	filmový	k2eAgFnPc2d1	filmová
iluzí	iluze	k1gFnPc2	iluze
používají	používat	k5eAaImIp3nP	používat
zpravidla	zpravidla	k6eAd1	zpravidla
počítačové	počítačový	k2eAgFnPc1d1	počítačová
a	a	k8xC	a
digitální	digitální	k2eAgFnPc1d1	digitální
technologie	technologie	k1gFnPc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
do	do	k7c2	do
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
povědomí	povědomí	k1gNnSc2	povědomí
dostala	dostat	k5eAaPmAgFnS	dostat
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
českých	český	k2eAgMnPc2d1	český
filmařů	filmař	k1gMnPc2	filmař
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Svěrák	Svěrák	k1gMnSc1	Svěrák
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Michálek	Michálek	k1gMnSc1	Michálek
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Hřebejk	Hřebejk	k1gMnSc1	Hřebejk
<g/>
,	,	kIx,	,
Saša	Saša	k1gMnSc1	Saša
Gedeon	Gedeon	k1gMnSc1	Gedeon
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Zelenka	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
Alice	Alice	k1gFnSc1	Alice
Nellis	Nellis	k1gFnSc1	Nellis
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
největší	veliký	k2eAgInSc1d3	veliký
úspěch	úspěch	k1gInSc1	úspěch
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Oscarem	Oscar	k1gInSc7	Oscar
oceněný	oceněný	k2eAgInSc4d1	oceněný
film	film	k1gInSc4	film
Kolja	Kolj	k1gInSc2	Kolj
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Jana	Jan	k1gMnSc2	Jan
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
Nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
získaly	získat	k5eAaPmAgFnP	získat
filmy	film	k1gInPc4	film
<g/>
:	:	kIx,	:
Musíme	muset	k5eAaImIp1nP	muset
si	se	k3xPyFc3	se
pomáhat	pomáhat	k5eAaImF	pomáhat
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Jana	Jana	k1gFnSc1	Jana
Hřebejka	Hřebejka	k1gFnSc1	Hřebejka
a	a	k8xC	a
Želary	Želar	k1gInPc1	Želar
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Trojana	Trojan	k1gMnSc2	Trojan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
filmu	film	k1gInSc2	film
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k9	i
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nejen	nejen	k6eAd1	nejen
promítá	promítat	k5eAaImIp3nS	promítat
filmy	film	k1gInPc4	film
určené	určený	k2eAgInPc4d1	určený
původně	původně	k6eAd1	původně
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
natáčí	natáčet	k5eAaImIp3nP	natáčet
i	i	k9	i
vlastní	vlastní	k2eAgInPc4d1	vlastní
televizní	televizní	k2eAgInPc4d1	televizní
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Specialitou	specialita	k1gFnSc7	specialita
televizní	televizní	k2eAgFnSc2d1	televizní
filmové	filmový	k2eAgFnSc2d1	filmová
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
televizní	televizní	k2eAgInPc4d1	televizní
seriály	seriál	k1gInPc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
hraných	hraný	k2eAgNnPc2d1	hrané
a	a	k8xC	a
animovaných	animovaný	k2eAgNnPc2d1	animované
filmových	filmový	k2eAgNnPc2d1	filmové
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
na	na	k7c6	na
umělecké	umělecký	k2eAgFnSc6d1	umělecká
fikci	fikce	k1gFnSc6	fikce
<g/>
,	,	kIx,	,
velké	velký	k2eAgFnSc6d1	velká
stylizaci	stylizace	k1gFnSc6	stylizace
a	a	k8xC	a
iluzi	iluze	k1gFnSc6	iluze
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgNnPc4d1	filmové
díla	dílo	k1gNnPc4	dílo
toho	ten	k3xDgInSc2	ten
typu	typ	k1gInSc2	typ
bývají	bývat	k5eAaImIp3nP	bývat
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
<g/>
)	)	kIx)	)
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
všední	všední	k2eAgFnSc2d1	všední
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
uplatněn	uplatnit	k5eAaPmNgInS	uplatnit
umělecký	umělecký	k2eAgInSc1d1	umělecký
princip	princip	k1gInSc1	princip
licentia	licentius	k1gMnSc2	licentius
poetica	poeticus	k1gMnSc2	poeticus
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
realitou	realita	k1gFnSc7	realita
(	(	kIx(	(
<g/>
reálný	reálný	k2eAgInSc1d1	reálný
svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
takovou	takový	k3xDgFnSc4	takový
pracuje	pracovat	k5eAaImIp3nS	pracovat
především	především	k9	především
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
samostatný	samostatný	k2eAgInSc1d1	samostatný
a	a	k8xC	a
specifický	specifický	k2eAgInSc1d1	specifický
filmový	filmový	k2eAgInSc1d1	filmový
žánr	žánr	k1gInSc1	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgFnSc1d1	Česká
kinematografie	kinematografie	k1gFnSc1	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgInPc2d1	český
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
chronologicky	chronologicky	k6eAd1	chronologicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
režisérů	režisér	k1gMnPc2	režisér
a	a	k8xC	a
režisérek	režisérka	k1gFnPc2	režisérka
seznamy	seznam	k1gInPc1	seznam
filmů	film	k1gInPc2	film
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
titulky	titulek	k1gInPc1	titulek
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc4	film
němý	němý	k2eAgInSc4d1	němý
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
zvukový	zvukový	k2eAgInSc4d1	zvukový
film	film	k1gInSc4	film
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
filmový	filmový	k2eAgInSc4d1	filmový
žánr	žánr	k1gInSc4	žánr
kino	kino	k1gNnSc4	kino
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
film	film	k1gInSc1	film
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Film	film	k1gInSc1	film
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
film	film	k1gInSc1	film
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Česko-Slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
filmová	filmový	k2eAgFnSc1d1	filmová
databáze	databáze	k1gFnSc1	databáze
NaFilmu	NaFilm	k1gInSc2	NaFilm
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Filmové	filmový	k2eAgFnPc1d1	filmová
novinky	novinka	k1gFnPc1	novinka
<g/>
,	,	kIx,	,
aktuality	aktualita	k1gFnPc1	aktualita
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
IMDb	IMDb	k1gInSc1	IMDb
<g/>
)	)	kIx)	)
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
All	All	k1gMnSc5	All
Movie	Movius	k1gMnSc5	Movius
Guide	Guid	k1gMnSc5	Guid
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
Krátkometrážní	krátkometrážní	k2eAgInPc4d1	krátkometrážní
snímky	snímek	k1gInPc4	snímek
-	-	kIx~	-
historie	historie	k1gFnSc1	historie
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
efektů	efekt	k1gInPc2	efekt
Filmová	filmový	k2eAgNnPc4d1	filmové
místa	místo	k1gNnPc4	místo
</s>
