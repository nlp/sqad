<p>
<s>
Waltari	Waltari	k6eAd1	Waltari
je	být	k5eAaImIp3nS	být
finská	finský	k2eAgFnSc1d1	finská
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
kombinováním	kombinování	k1gNnSc7	kombinování
mnoha	mnoho	k4c2	mnoho
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
/	/	kIx~	/
<g/>
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Kärtsym	Kärtsym	k1gInSc4	Kärtsym
Hatakkou	Hatakka	k1gFnSc7	Hatakka
<g/>
,	,	kIx,	,
kytaristou	kytarista	k1gMnSc7	kytarista
Jariotem	Jariot	k1gMnSc7	Jariot
Lehtinenem	Lehtinen	k1gMnSc7	Lehtinen
a	a	k8xC	a
bubeníkem	bubeník	k1gMnSc7	bubeník
Salem	Sal	k1gMnSc7	Sal
Suomalainenem	Suomalainen	k1gMnSc7	Suomalainen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
ke	k	k7c3	k
skupině	skupina	k1gFnSc3	skupina
přidává	přidávat	k5eAaImIp3nS	přidávat
kytarista	kytarista	k1gMnSc1	kytarista
Sami	sám	k3xTgMnPc1	sám
Yli-Sirniö	Yli-Sirniö	k1gMnPc1	Yli-Sirniö
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vydává	vydávat	k5eAaPmIp3nS	vydávat
kapela	kapela	k1gFnSc1	kapela
své	svůj	k3xOyFgFnPc4	svůj
první	první	k4xOgFnPc4	první
EP	EP	kA	EP
<g/>
,	,	kIx,	,
Mut	Mut	k?	Mut
<g/>
,	,	kIx,	,
Hei	Hei	k1gFnSc1	Hei
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Albový	albový	k2eAgInSc1d1	albový
debut	debut	k1gInSc1	debut
Monk	Monk	k1gInSc1	Monk
Punk	punk	k1gInSc4	punk
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
především	především	k6eAd1	především
punkové	punkový	k2eAgNnSc1d1	punkové
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zvuk	zvuk	k1gInSc4	zvuk
spočívající	spočívající	k2eAgInSc4d1	spočívající
v	v	k7c6	v
míchání	míchání	k1gNnSc6	míchání
mnoha	mnoho	k4c2	mnoho
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
skupina	skupina	k1gFnSc1	skupina
nachází	nacházet	k5eAaImIp3nS	nacházet
až	až	k9	až
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
druhém	druhý	k4xOgNnSc6	druhý
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
Torcha	Torcha	k1gFnSc1	Torcha
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
kompilace	kompilace	k1gFnSc2	kompilace
raných	raný	k2eAgFnPc2d1	raná
nahrávek	nahrávka	k1gFnPc2	nahrávka
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pala	Pala	k1gMnSc1	Pala
Leipää	Leipää	k1gMnSc1	Leipää
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kus	kus	k1gInSc1	kus
chleba	chléb	k1gInSc2	chléb
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
vyšla	vyjít	k5eAaPmAgFnS	vyjít
skupina	skupina	k1gFnSc1	skupina
s	s	k7c7	s
albem	album	k1gNnSc7	album
So	So	kA	So
Fine	Fin	k1gMnSc5	Fin
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebně	hudebně	k6eAd1	hudebně
podobným	podobný	k2eAgNnPc3d1	podobné
Torcha	Torcha	k1gFnSc1	Torcha
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Další	další	k2eAgFnSc1d1	další
řadová	řadový	k2eAgFnSc1d1	řadová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
Big	Big	k1gFnSc1	Big
Bang	Bang	k1gMnSc1	Bang
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jejich	jejich	k3xOp3gInSc7	jejich
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
míchání	míchání	k1gNnPc4	míchání
techna	techno	k1gNnSc2	techno
s	s	k7c7	s
heavy	heava	k1gFnPc4	heava
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Samiho	Sami	k1gMnSc4	Sami
Yli-Sirenö	Yli-Sirenö	k1gMnSc4	Yli-Sirenö
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
kytaristy	kytarista	k1gMnSc2	kytarista
současný	současný	k2eAgMnSc1d1	současný
druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
Children	Childrna	k1gFnPc2	Childrna
of	of	k?	of
Bodom	Bodom	k1gInSc1	Bodom
Roope	Roop	k1gMnSc5	Roop
Latvala	Latvala	k1gMnSc5	Latvala
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
Kärtsy	Kärts	k1gMnPc4	Kärts
Hatakka	Hatakka	k1gMnSc1	Hatakka
s	s	k7c7	s
dirigentem	dirigent	k1gMnSc7	dirigent
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
Avanti	avanti	k0	avanti
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Rikuou	Rikuý	k2eAgFnSc7d1	Rikuý
Niemim	Niemim	k1gInSc4	Niemim
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
kombinujícím	kombinující	k2eAgInSc6d1	kombinující
Heavy	Heav	k1gInPc7	Heav
metal	metat	k5eAaImAgMnS	metat
s	s	k7c7	s
Klasickou	klasický	k2eAgFnSc7d1	klasická
Hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
znám	znám	k2eAgInSc1d1	znám
jako	jako	k8xC	jako
Yeah	Yeah	k1gInSc1	Yeah
<g/>
!	!	kIx.	!
</s>
<s>
Yeah	Yeah	k1gMnSc1	Yeah
<g/>
!	!	kIx.	!
</s>
<s>
Die	Die	k?	Die
<g/>
!	!	kIx.	!
</s>
<s>
Die	Die	k?	Die
<g/>
!	!	kIx.	!
</s>
<s>
Death	Death	k1gInSc1	Death
Metal	metat	k5eAaImAgInS	metat
Symphony	Symphona	k1gFnPc4	Symphona
in	in	k?	in
Deep	Deep	k1gInSc1	Deep
C.	C.	kA	C.
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
helsinském	helsinský	k2eAgInSc6d1	helsinský
hudebním	hudební	k2eAgInSc6d1	hudební
festivalu	festival	k1gInSc6	festival
Taiteiden	Taiteidna	k1gFnPc2	Taiteidna
Yö	Yö	k1gFnPc2	Yö
předveden	předveden	k2eAgInSc1d1	předveden
živě	živě	k6eAd1	živě
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
spatřilo	spatřit	k5eAaPmAgNnS	spatřit
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
album	album	k1gNnSc1	album
Space	Space	k1gFnSc2	Space
Avenue	avenue	k1gFnSc2	avenue
<g/>
,	,	kIx,	,
progresivnější	progresivní	k2eAgFnSc2d2	progresivnější
práce	práce	k1gFnSc2	práce
obsahující	obsahující	k2eAgFnSc2d1	obsahující
víc	hodně	k6eAd2	hodně
elektronických	elektronický	k2eAgFnPc2d1	elektronická
bicích	bicí	k2eAgFnPc2d1	bicí
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
nahrávání	nahrávání	k1gNnSc6	nahrávání
Waltari	Waltar	k1gMnPc1	Waltar
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
Apocalypticou	Apocalyptica	k1gFnSc7	Apocalyptica
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
svého	svůj	k3xOyFgNnSc2	svůj
desátého	desátý	k4xOgInSc2	desátý
výročí	výročí	k1gNnSc3	výročí
kompilaci	kompilace	k1gFnSc3	kompilace
jménem	jméno	k1gNnSc7	jméno
Decade	Decad	k1gInSc5	Decad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
pracoval	pracovat	k5eAaImAgInS	pracovat
Kärtsy	Kärtsa	k1gFnSc2	Kärtsa
Hatakka	Hatakek	k1gMnSc2	Hatakek
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
klasicko-metalové	klasickoetalový	k2eAgFnSc6d1	klasicko-metalový
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dala	dát	k5eAaPmAgFnS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
pódiové	pódiový	k2eAgFnSc3d1	pódiová
show	show	k1gFnSc3	show
Evangelicum	Evangelicum	k1gInSc4	Evangelicum
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
se	se	k3xPyFc4	se
lišilo	lišit	k5eAaImAgNnS	lišit
od	od	k7c2	od
Yeah	Yeaha	k1gFnPc2	Yeaha
<g/>
!	!	kIx.	!
</s>
<s>
Yeah	Yeah	k1gMnSc1	Yeah
<g/>
!	!	kIx.	!
</s>
<s>
Die	Die	k?	Die
<g/>
!	!	kIx.	!
</s>
<s>
Die	Die	k?	Die
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k9	jen
hudbu	hudba	k1gFnSc4	hudba
<g/>
;	;	kIx,	;
součástí	součást	k1gFnSc7	součást
představení	představení	k1gNnSc2	představení
byl	být	k5eAaImAgInS	být
i	i	k9	i
balet	balet	k1gInSc1	balet
a	a	k8xC	a
světelná	světelný	k2eAgFnSc1d1	světelná
show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Evangelicum	Evangelicum	k1gNnSc1	Evangelicum
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
vydáno	vydat	k5eAaPmNgNnS	vydat
jako	jako	k9	jako
album	album	k1gNnSc1	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Radium	radium	k1gNnSc1	radium
Round	round	k1gInSc1	round
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
velmi	velmi	k6eAd1	velmi
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
popu	pop	k1gInSc2	pop
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
se	se	k3xPyFc4	se
Waltari	Waltar	k1gMnPc1	Waltar
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
na	na	k7c4	na
méně	málo	k6eAd2	málo
komerční	komerční	k2eAgFnSc4d1	komerční
práci	práce	k1gFnSc4	práce
jako	jako	k8xC	jako
Channel	Channel	k1gMnSc1	Channel
Nordica	Nordica	k1gMnSc1	Nordica
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sámským	sámský	k2eAgInSc7d1	sámský
pěveckým	pěvecký	k2eAgInSc7d1	pěvecký
sborem	sbor	k1gInSc7	sbor
Angelit	Angelit	k1gFnPc2	Angelit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odvážnou	odvážný	k2eAgFnSc4d1	odvážná
kombinaci	kombinace	k1gFnSc4	kombinace
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
laponskou	laponský	k2eAgFnSc7d1	laponská
lidovou	lidový	k2eAgFnSc7d1	lidová
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
punkové	punkový	k2eAgFnSc2d1	punková
EP	EP	kA	EP
Back	Back	k1gMnSc1	Back
To	ten	k3xDgNnSc1	ten
Persepolis	Persepolis	k1gFnSc1	Persepolis
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
nahrávky	nahrávka	k1gFnPc4	nahrávka
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
sehnat	sehnat	k5eAaPmF	sehnat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Waltari	Waltari	k1gNnSc2	Waltari
jistý	jistý	k2eAgInSc4d1	jistý
čas	čas	k1gInSc4	čas
přežívali	přežívat	k5eAaImAgMnP	přežívat
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
koncertování	koncertování	k1gNnSc2	koncertování
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
skupině	skupina	k1gFnSc3	skupina
vrátil	vrátit	k5eAaPmAgMnS	vrátit
původní	původní	k2eAgMnSc1d1	původní
kytarista	kytarista	k1gMnSc1	kytarista
Sami	sám	k3xTgMnPc1	sám
Yli-Sirniö	Yli-Sirniö	k1gMnSc5	Yli-Sirniö
a	a	k8xC	a
Roope	Roop	k1gMnSc5	Roop
Latvala	Latvala	k1gMnSc1	Latvala
kapelu	kapela	k1gFnSc4	kapela
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
tříleté	tříletý	k2eAgFnSc6d1	tříletá
odmlce	odmlka	k1gFnSc6	odmlka
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
Waltari	Waltar	k1gMnPc1	Waltar
vrátili	vrátit	k5eAaPmAgMnP	vrátit
na	na	k7c4	na
scénu	scéna	k1gFnSc4	scéna
s	s	k7c7	s
kritiky	kritik	k1gMnPc7	kritik
i	i	k8xC	i
fanoušky	fanoušek	k1gMnPc7	fanoušek
velmi	velmi	k6eAd1	velmi
kladně	kladně	k6eAd1	kladně
přijatým	přijatý	k2eAgNnSc7d1	přijaté
albem	album	k1gNnSc7	album
Rare	Rar	k1gFnSc2	Rar
Species	species	k1gFnSc7	species
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
turné	turné	k1gNnSc7	turné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydali	vydat	k5eAaPmAgMnP	vydat
Waltari	Waltare	k1gFnSc4	Waltare
album	album	k1gNnSc4	album
Blood	Blooda	k1gFnPc2	Blooda
Sample	Sample	k1gFnPc2	Sample
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
potom	potom	k6eAd1	potom
kompilaci	kompilace	k1gFnSc4	kompilace
Early	earl	k1gMnPc4	earl
Years	Yearsa	k1gFnPc2	Yearsa
(	(	kIx(	(
<g/>
remasterované	remasterovaný	k2eAgFnSc2d1	remasterovaná
verze	verze	k1gFnSc2	verze
Monk	Monk	k1gMnSc1	Monk
Punk	punk	k1gMnSc1	punk
<g/>
,	,	kIx,	,
Pala	Pala	k1gMnSc1	Pala
Leipää	Leipää	k1gMnSc1	Leipää
a	a	k8xC	a
nikdy	nikdy	k6eAd1	nikdy
nevydané	vydaný	k2eNgFnSc2d1	nevydaná
písně	píseň	k1gFnSc2	píseň
z	z	k7c2	z
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
znovu	znovu	k6eAd1	znovu
nahrané	nahraný	k2eAgInPc1d1	nahraný
původní	původní	k2eAgFnSc7d1	původní
trojicí	trojice	k1gFnSc7	trojice
Jariot	Jariota	k1gFnPc2	Jariota
<g/>
,	,	kIx,	,
Kärtsy	Kärtsa	k1gFnSc2	Kärtsa
a	a	k8xC	a
Sale	Sal	k1gFnSc2	Sal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
také	také	k9	také
vychází	vycházet	k5eAaImIp3nS	vycházet
album	album	k1gNnSc4	album
Polib	políbit	k5eAaPmRp2nS	políbit
si	se	k3xPyFc3	se
dědu	děda	k1gMnSc4	děda
české	český	k2eAgFnSc2d1	Česká
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Wohnout	Wohnout	k1gFnSc2	Wohnout
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
skladba	skladba	k1gFnSc1	skladba
Činely	činela	k1gFnSc2	činela
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
spolupráce	spolupráce	k1gFnSc2	spolupráce
skupiny	skupina	k1gFnSc2	skupina
Wohnout	Wohnout	k1gFnSc2	Wohnout
s	s	k7c7	s
Waltari	Waltari	k1gNnSc7	Waltari
a	a	k8xC	a
Lenkou	Lenka	k1gFnSc7	Lenka
Dusilovou	Dusilová	k1gFnSc7	Dusilová
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
studiovým	studiový	k2eAgInSc7d1	studiový
počinem	počin	k1gInSc7	počin
této	tento	k3xDgFnSc2	tento
finské	finský	k2eAgFnSc2d1	finská
kapely	kapela	k1gFnSc2	kapela
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc4	album
Release	Releasa	k1gFnSc3	Releasa
Date	Dat	k1gFnSc2	Dat
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
Waltari	Waltar	k1gInSc6	Waltar
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
fanouškům	fanoušek	k1gMnPc3	fanoušek
svou	svůj	k3xOyFgFnSc4	svůj
tvrdší	tvrdý	k2eAgFnSc4d2	tvrdší
<g/>
,	,	kIx,	,
metalovější	metalový	k2eAgFnSc4d2	metalovější
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
bonusovou	bonusový	k2eAgFnSc4d1	bonusová
skladbu	skladba	k1gFnSc4	skladba
Spokebone	Spokebon	k1gMnSc5	Spokebon
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgMnPc4	který
kapela	kapela	k1gFnSc1	kapela
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Tomim	Tomim	k1gInSc4	Tomim
Joutsenem	Joutsen	k1gInSc7	Joutsen
z	z	k7c2	z
Amorphis	Amorphis	k1gFnSc2	Amorphis
a	a	k8xC	a
s	s	k7c7	s
finskou	finský	k2eAgFnSc7d1	finská
etnoskupinou	etnoskupin	k2eAgFnSc7d1	etnoskupin
Värttinä	Värttinä	k1gFnSc7	Värttinä
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vydali	vydat	k5eAaPmAgMnP	vydat
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Below	Below	k1gMnSc1	Below
Zero	Zero	k1gMnSc1	Zero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Kärtsy	Kärtsa	k1gFnPc1	Kärtsa
Hatakka	Hatakko	k1gNnSc2	Hatakko
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jariot	Jariot	k1gInSc1	Jariot
Lehtinen	Lehtinen	k1gInSc1	Lehtinen
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sami	sám	k3xTgMnPc1	sám
Yli-Sirniö	Yli-Sirniö	k1gMnPc1	Yli-Sirniö
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
a	a	k8xC	a
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nino	Nina	k1gFnSc5	Nina
Silvennoinen	Silvennoinen	k1gInSc1	Silvennoinen
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ville	Ville	k6eAd1	Ville
Vehviläinen	Vehviläinen	k1gInSc4	Vehviläinen
–	–	k?	–
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jani	Jani	k?	Jani
Hölli	Hölle	k1gFnSc4	Hölle
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kimmo	Kimmo	k6eAd1	Kimmo
Korhonen	Korhonen	k1gInSc1	Korhonen
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
časté	častý	k2eAgInPc1d1	častý
záskoky	záskok	k1gInPc1	záskok
<g/>
,	,	kIx,	,
od	od	k7c2	od
2013	[number]	k4	2013
uveden	uvést	k5eAaPmNgMnS	uvést
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Dřívější	dřívější	k2eAgMnPc1d1	dřívější
členové	člen	k1gMnPc1	člen
===	===	k?	===
</s>
</p>
<p>
<s>
Sale	Salat	k5eAaPmIp3nS	Salat
Suomalainen	Suomalainen	k1gInSc4	Suomalainen
–	–	k?	–
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Janne	Jannout	k5eAaPmIp3nS	Jannout
Parviainen	Parviainen	k1gInSc4	Parviainen
–	–	k?	–
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roope	Roopat	k5eAaPmIp3nS	Roopat
Latvala	Latvala	k1gFnSc1	Latvala
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tote	Tote	k1gFnSc1	Tote
Hatakka	Hatakek	k1gInSc2	Hatakek
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
</s>
</p>
<p>
<s>
Janne	Jannout	k5eAaImIp3nS	Jannout
Immonen	Immonen	k1gInSc4	Immonen
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mika	Mik	k1gMnSc2	Mik
Järveläinen	Järveläinen	k1gInSc1	Järveläinen
–	–	k?	–
bicí	bicí	k2eAgInPc4d1	bicí
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
Mut	Mut	k?	Mut
Hei	Hei	k1gMnSc1	Hei
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Monk	Monk	k1gMnSc1	Monk
Punk	punk	k1gMnSc1	punk
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Torcha	Torcha	k1gFnSc1	Torcha
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pala	Pala	k1gMnSc1	Pala
Leipää	Leipää	k1gMnSc1	Leipää
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
So	So	kA	So
Fine	Fin	k1gMnSc5	Fin
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Big	Big	k?	Big
Bang	Bang	k1gInSc1	Bang
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Yeah	Yeah	k1gMnSc1	Yeah
<g/>
!	!	kIx.	!
</s>
<s>
Yeah	Yeah	k1gMnSc1	Yeah
<g/>
!	!	kIx.	!
</s>
<s>
Die	Die	k?	Die
<g/>
!	!	kIx.	!
</s>
<s>
Die	Die	k?	Die
<g/>
!	!	kIx.	!
</s>
<s>
Death	Death	k1gInSc1	Death
Metal	metat	k5eAaImAgInS	metat
Symphony	Symphona	k1gFnPc4	Symphona
in	in	k?	in
Deep	Deep	k1gMnSc1	Deep
C	C	kA	C
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Space	Space	k1gFnSc1	Space
Avenue	avenue	k1gFnSc1	avenue
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Decade	Decást	k5eAaPmIp3nS	Decást
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Radium	radium	k1gNnSc1	radium
Round	round	k1gInSc1	round
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Channel	Channel	k1gMnSc1	Channel
Nordica	Nordica	k1gMnSc1	Nordica
(	(	kIx(	(
<g/>
+	+	kIx~	+
Angelit	Angelit	k1gMnSc1	Angelit
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Back	Back	k6eAd1	Back
To	to	k9	to
Persepolis	Persepolis	k1gFnSc1	Persepolis
(	(	kIx(	(
<g/>
EP	EP	kA	EP
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rare	Rare	k1gFnSc1	Rare
Species	species	k1gFnSc1	species
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blood	Blood	k1gInSc1	Blood
Sample	Sample	k1gFnSc2	Sample
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Early	earl	k1gMnPc4	earl
Years	Yearsa	k1gFnPc2	Yearsa
(	(	kIx(	(
<g/>
dvojalbum	dvojalbum	k1gNnSc1	dvojalbum
<g/>
,	,	kIx,	,
remasterováno	remasterovat	k5eAaBmNgNnS	remasterovat
+	+	kIx~	+
přidány	přidán	k2eAgFnPc4d1	přidána
stopy	stopa	k1gFnPc4	stopa
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Release	Release	k6eAd1	Release
Date	Date	k1gNnSc1	Date
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
2	[number]	k4	2
<g/>
nd	nd	k?	nd
Decade	Decad	k1gInSc5	Decad
-	-	kIx~	-
In	In	k1gMnSc3	In
The	The	k1gMnSc3	The
Cradle	Cradle	k1gMnSc3	Cradle
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Below	Below	k?	Below
Zero	Zero	k1gMnSc1	Zero
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Covers	Covers	k1gInSc1	Covers
All	All	k1gFnSc2	All
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
25	[number]	k4	25
<g/>
th	th	k?	th
Anniversary	Anniversar	k1gInPc1	Anniversar
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
You	You	k?	You
are	ar	k1gInSc5	ar
Waltari	Waltar	k1gMnSc6	Waltar
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
-	-	kIx~	-
nové	nový	k2eAgFnPc1d1	nová
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
-	-	kIx~	-
původní	původní	k2eAgFnSc1d1	původní
</s>
</p>
<p>
<s>
Waltari	Waltari	k6eAd1	Waltari
na	na	k7c4	na
Myspace	Myspace	k1gFnPc4	Myspace
</s>
</p>
