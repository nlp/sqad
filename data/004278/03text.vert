<s>
Vyšší	vysoký	k2eAgInSc1d2	vyšší
princip	princip	k1gInSc1	princip
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jiřího	Jiří	k1gMnSc2	Jiří
Krejčíka	Krejčík	k1gMnSc2	Krejčík
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
povídky	povídka	k1gFnSc2	povídka
Jana	Jan	k1gMnSc2	Jan
Drdy	Drda	k1gMnSc2	Drda
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc1	příběh
středoškolského	středoškolský	k2eAgMnSc2d1	středoškolský
profesora	profesor	k1gMnSc2	profesor
Málka	Málek	k1gMnSc2	Málek
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
František	František	k1gMnSc1	František
Smolík	Smolík	k1gMnSc1	Smolík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
souhlasit	souhlasit	k5eAaImF	souhlasit
s	s	k7c7	s
popravou	poprava	k1gFnSc7	poprava
nevinných	vinný	k2eNgMnPc2d1	nevinný
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
filmu	film	k1gInSc6	film
zazní	zaznět	k5eAaImIp3nS	zaznět
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
úst	ústa	k1gNnPc2	ústa
legendární	legendární	k2eAgFnSc1d1	legendární
věta	věta	k1gFnSc1	věta
"	"	kIx"	"
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vyššího	vysoký	k2eAgInSc2d2	vyšší
principu	princip	k1gInSc2	princip
mravního	mravní	k2eAgInSc2d1	mravní
vražda	vražda	k1gFnSc1	vražda
na	na	k7c6	na
tyranu	tyran	k1gMnSc6	tyran
není	být	k5eNaImIp3nS	být
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
zakázán	zakázán	k2eAgInSc1d1	zakázán
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
údajnému	údajný	k2eAgNnSc3d1	údajné
antiněmectví	antiněmectví	k1gNnSc3	antiněmectví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
spáchán	spáchat	k5eAaPmNgInS	spáchat
atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
zastupujícího	zastupující	k2eAgMnSc4d1	zastupující
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
má	mít	k5eAaImIp3nS	mít
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
gymnaziálních	gymnaziální	k2eAgFnPc2d1	gymnaziální
tříd	třída	k1gFnPc2	třída
vystavené	vystavená	k1gFnSc2	vystavená
své	svůj	k3xOyFgNnSc4	svůj
maturitní	maturitní	k2eAgNnSc4d1	maturitní
tablo	tablo	k1gNnSc4	tablo
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
je	být	k5eAaImIp3nS	být
rozhlasem	rozhlas	k1gInSc7	rozhlas
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
v	v	k7c6	v
protektorátu	protektorát	k1gInSc6	protektorát
zákaz	zákaz	k1gInSc1	zákaz
vycházení	vycházení	k1gNnSc2	vycházení
po	po	k7c4	po
20	[number]	k4	20
<g/>
.	.	kIx.	.
hodině	hodina	k1gFnSc6	hodina
a	a	k8xC	a
slíbena	slíben	k2eAgFnSc1d1	slíbena
odměna	odměna	k1gFnSc1	odměna
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
korun	koruna	k1gFnPc2	koruna
za	za	k7c2	za
odhalení	odhalení	k1gNnSc2	odhalení
jmen	jméno	k1gNnPc2	jméno
atentátníků	atentátník	k1gMnPc2	atentátník
<g/>
.	.	kIx.	.
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Ryšánek	Ryšánek	k1gMnSc1	Ryšánek
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Ivan	Ivan	k1gMnSc1	Ivan
Mistrík	Mistrík	k1gMnSc1	Mistrík
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šikovný	šikovný	k2eAgMnSc1d1	šikovný
student	student	k1gMnSc1	student
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
učiteli	učitel	k1gMnSc6	učitel
i	i	k9	i
v	v	k7c6	v
kolektivu	kolektiv	k1gInSc6	kolektiv
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
pomoci	pomoct	k5eAaPmF	pomoct
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
jednomu	jeden	k4xCgNnSc3	jeden
spolužákovi	spolužák	k1gMnSc6	spolužák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
překladem	překlad	k1gInSc7	překlad
novinového	novinový	k2eAgInSc2d1	novinový
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Učitel	učitel	k1gMnSc1	učitel
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Václav	Václav	k1gMnSc1	Václav
Lohniský	Lohniský	k1gMnSc1	Lohniský
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
všimne	všimnout	k5eAaPmIp3nS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ryšánek	Ryšánek	k1gMnSc1	Ryšánek
napovídá	napovídat	k5eAaBmIp3nS	napovídat
a	a	k8xC	a
okřikne	okřiknout	k5eAaPmIp3nS	okřiknout
jej	on	k3xPp3gInSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
se	se	k3xPyFc4	se
hájí	hájit	k5eAaImIp3nS	hájit
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
prosím	prosit	k5eAaImIp1nS	prosit
nenapovídám	napovídat	k5eNaBmIp1nS	napovídat
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
si	se	k3xPyFc3	se
jen	jen	k9	jen
nahlas	nahlas	k6eAd1	nahlas
opakuji	opakovat	k5eAaImIp1nS	opakovat
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
nezapomněl	zapomnět	k5eNaImAgMnS	zapomnět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Celá	celý	k2eAgFnSc1d1	celá
třída	třída	k1gFnSc1	třída
se	se	k3xPyFc4	se
baví	bavit	k5eAaImIp3nS	bavit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyučování	vyučování	k1gNnSc2	vyučování
si	se	k3xPyFc3	se
dělají	dělat	k5eAaImIp3nP	dělat
tři	tři	k4xCgMnPc1	tři
studenti	student	k1gMnPc1	student
legraci	legrace	k1gFnSc4	legrace
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
když	když	k8xS	když
píšou	psát	k5eAaImIp3nP	psát
studenti	student	k1gMnPc1	student
maturitní	maturitní	k2eAgFnSc2d1	maturitní
písemky	písemka	k1gFnSc2	písemka
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
vtrhnou	vtrhnout	k5eAaPmIp3nP	vtrhnout
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
příslušníci	příslušník	k1gMnPc1	příslušník
gestapa	gestapo	k1gNnSc2	gestapo
v	v	k7c6	v
černých	černý	k2eAgFnPc6d1	černá
uniformách	uniforma	k1gFnPc6	uniforma
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
si	se	k3xPyFc3	se
přizve	přizvat	k5eAaPmIp3nS	přizvat
kolegu	kolega	k1gMnSc4	kolega
třídního	třídní	k1gMnSc4	třídní
učitele	učitel	k1gMnSc4	učitel
Málka	Málek	k1gMnSc4	Málek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Vyšší	vysoký	k2eAgInSc4d2	vyšší
princip	princip	k1gInSc4	princip
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc4	gestapo
s	s	k7c7	s
sebou	se	k3xPyFc7	se
odvede	odvést	k5eAaPmIp3nS	odvést
tři	tři	k4xCgMnPc4	tři
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Ryšánek	Ryšánek	k1gMnSc1	Ryšánek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
milá	milý	k2eAgFnSc1d1	Milá
spolužačka	spolužačka	k1gFnSc1	spolužačka
Jana	Jana	k1gFnSc1	Jana
Skálová	Skálová	k1gFnSc1	Skálová
(	(	kIx(	(
<g/>
Jana	Jana	k1gFnSc1	Jana
Brejchová	Brejchová	k1gFnSc1	Brejchová
<g/>
)	)	kIx)	)
prosí	prosit	k5eAaImIp3nS	prosit
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
právníka	právník	k1gMnSc4	právník
(	(	kIx(	(
<g/>
Otomar	Otomar	k1gMnSc1	Otomar
Krejča	Krejča	k1gMnSc1	Krejča
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
šel	jít	k5eAaImAgMnS	jít
na	na	k7c4	na
gestapo	gestapo	k1gNnSc4	gestapo
za	za	k7c7	za
spolužáky	spolužák	k1gMnPc7	spolužák
orodovat	orodovat	k5eAaImF	orodovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
osobně	osobně	k6eAd1	osobně
zná	znát	k5eAaImIp3nS	znát
s	s	k7c7	s
velitelem	velitel	k1gMnSc7	velitel
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
však	však	k9	však
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
úkolu	úkol	k1gInSc3	úkol
zhostí	zhostit	k5eAaPmIp3nS	zhostit
třídní	třídní	k2eAgMnSc1d1	třídní
profesor	profesor	k1gMnSc1	profesor
Málek	Málek	k1gMnSc1	Málek
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Smolík	Smolík	k1gMnSc1	Smolík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odejde	odejít	k5eAaPmIp3nS	odejít
na	na	k7c4	na
služebnu	služebna	k1gFnSc4	služebna
gestapa	gestapo	k1gNnSc2	gestapo
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
milost	milost	k1gFnSc4	milost
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
služebny	služebna	k1gFnSc2	služebna
gestapa	gestapo	k1gNnSc2	gestapo
(	(	kIx(	(
<g/>
Hannjo	Hannjo	k6eAd1	Hannjo
Hasse	Hasse	k1gFnPc4	Hasse
<g/>
)	)	kIx)	)
profesorovy	profesorův	k2eAgFnPc4d1	profesorova
pohnutky	pohnutka	k1gFnPc4	pohnutka
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
chápe	chápat	k5eAaImIp3nS	chápat
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
vyzná	vyznat	k5eAaPmIp3nS	vyznat
v	v	k7c6	v
antické	antický	k2eAgFnSc6d1	antická
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zjednat	zjednat	k5eAaPmF	zjednat
nápravu	náprava	k1gFnSc4	náprava
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
skautů	skaut	k1gMnPc2	skaut
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
vykonat	vykonat	k5eAaPmF	vykonat
jeden	jeden	k4xCgInSc4	jeden
dobrý	dobrý	k2eAgInSc4d1	dobrý
skutek	skutek	k1gInSc4	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
skutkem	skutek	k1gInSc7	skutek
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
milost	milost	k1gFnSc4	milost
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
velitelství	velitelství	k1gNnSc6	velitelství
se	se	k3xPyFc4	se
zajímá	zajímat	k5eAaImIp3nS	zajímat
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
studenti	student	k1gMnPc1	student
provedli	provést	k5eAaPmAgMnP	provést
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
oč	oč	k6eAd1	oč
šlo	jít	k5eAaImAgNnS	jít
<g/>
,	,	kIx,	,
chce	chtít	k5eAaImIp3nS	chtít
případ	případ	k1gInSc1	případ
studentů	student	k1gMnPc2	student
řešit	řešit	k5eAaImF	řešit
až	až	k9	až
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
upozorněn	upozornit	k5eAaPmNgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
případ	případ	k1gInSc1	případ
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
"	"	kIx"	"
<g/>
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zvedá	zvedat	k5eAaImIp3nS	zvedat
telefon	telefon	k1gInSc4	telefon
(	(	kIx(	(
<g/>
snad	snad	k9	snad
aby	aby	kYmCp3nS	aby
popravu	poprava	k1gFnSc4	poprava
studentu	student	k1gMnSc3	student
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
rozmýšlení	rozmýšlení	k1gNnSc2	rozmýšlení
schuchátko	schuchátko	k1gNnSc1	schuchátko
pokládá	pokládat	k5eAaImIp3nS	pokládat
<g/>
.	.	kIx.	.
</s>
<s>
Osud	osud	k1gInSc1	osud
studentů	student	k1gMnPc2	student
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
zpečetěn	zpečetěn	k2eAgMnSc1d1	zpečetěn
<g/>
.	.	kIx.	.
</s>
<s>
Slíbený	slíbený	k2eAgInSc1d1	slíbený
jeden	jeden	k4xCgInSc1	jeden
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
"	"	kIx"	"
skutek	skutek	k1gInSc1	skutek
splní	splnit	k5eAaPmIp3nS	splnit
alespoň	alespoň	k9	alespoň
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zamítne	zamítnout	k5eAaPmIp3nS	zamítnout
vyplatit	vyplatit	k5eAaPmF	vyplatit
odměnu	odměna	k1gFnSc4	odměna
udavači	udavač	k1gMnSc3	udavač
<g/>
.	.	kIx.	.
</s>
<s>
Večer	večer	k6eAd1	večer
jsou	být	k5eAaImIp3nP	být
rozhlasem	rozhlas	k1gInSc7	rozhlas
oznámena	oznámit	k5eAaPmNgFnS	oznámit
jména	jméno	k1gNnSc2	jméno
zastřelených	zastřelený	k2eAgMnPc2d1	zastřelený
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
Vlastimilovo	Vlastimilův	k2eAgNnSc1d1	Vlastimilův
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Ryšánkova	Ryšánkův	k2eAgFnSc1d1	Ryšánkova
matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
Vášová	Vášová	k1gFnSc1	Vášová
<g/>
)	)	kIx)	)
běží	běžet	k5eAaImIp3nS	běžet
<g/>
,	,	kIx,	,
pronásledována	pronásledovat	k5eAaImNgFnS	pronásledovat
spolužačkou	spolužačka	k1gFnSc7	spolužačka
Vlastíka	Vlastík	k1gMnSc2	Vlastík
<g/>
,	,	kIx,	,
Janou	Jana	k1gFnSc7	Jana
Skálovou	Skálová	k1gFnSc7	Skálová
<g/>
,	,	kIx,	,
k	k	k7c3	k
velitelství	velitelství	k1gNnSc3	velitelství
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
strážný	strážný	k1gMnSc1	strážný
odstrčí	odstrčit	k5eAaPmIp3nS	odstrčit
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
hodí	hodit	k5eAaImIp3nS	hodit
dlažební	dlažební	k2eAgFnSc4d1	dlažební
kostku	kostka	k1gFnSc4	kostka
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
ji	on	k3xPp3gFnSc4	on
strážný	strážný	k1gMnSc1	strážný
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
ráno	ráno	k6eAd1	ráno
dostane	dostat	k5eAaPmIp3nS	dostat
třídní	třídní	k2eAgMnSc1d1	třídní
profesor	profesor	k1gMnSc1	profesor
Málek	Málek	k1gMnSc1	Málek
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
odsoudit	odsoudit	k5eAaPmF	odsoudit
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
studenty	student	k1gMnPc7	student
chování	chování	k1gNnSc4	chování
jejich	jejich	k3xOp3gMnPc2	jejich
popravených	popravený	k2eAgMnPc2d1	popravený
spolužáků	spolužák	k1gMnPc2	spolužák
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
svědomí	svědomí	k1gNnSc1	svědomí
mu	on	k3xPp3gInSc3	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
a	a	k8xC	a
před	před	k7c7	před
celou	celý	k2eAgFnSc7d1	celá
třídou	třída	k1gFnSc7	třída
pronese	pronést	k5eAaPmIp3nS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vyššího	vysoký	k2eAgInSc2d2	vyšší
principu	princip	k1gInSc2	princip
mravního	mravní	k2eAgInSc2d1	mravní
<g/>
,	,	kIx,	,
vražda	vražda	k1gFnSc1	vražda
na	na	k7c6	na
tyranu	tyran	k1gMnSc6	tyran
není	být	k5eNaImIp3nS	být
zločinem	zločin	k1gInSc7	zločin
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Třída	třída	k1gFnSc1	třída
mlčky	mlčky	k6eAd1	mlčky
povstane	povstat	k5eAaPmIp3nS	povstat
<g/>
.	.	kIx.	.
</s>
