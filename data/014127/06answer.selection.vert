<s>
Rakouská	rakouský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
je	být	k5eAaImIp3nS
škola	škola	k1gFnSc1
ekonomického	ekonomický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
vzniklá	vzniklý	k2eAgFnSc1d1
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
dílech	díl	k1gInPc6
vídeňských	vídeňský	k2eAgMnPc2d1
ekonomů	ekonom	k1gMnPc2
Carla	Carl	k1gMnSc2
Mengera	Menger	k1gMnSc2
<g/>
,	,	kIx,
Friedricha	Friedrich	k1gMnSc2
von	von	k1gInSc4
Wiesera	Wiesero	k1gNnSc2
a	a	k8xC
Eugena	Eugena	k1gFnSc1
von	von	k1gInSc1
Böhm-Bawerka	Böhm-Bawerka	k1gFnSc1
<g/>
.	.	kIx.
</s>