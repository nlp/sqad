<s>
Komsomolec	komsomolec	k1gMnSc1
(	(	kIx(
<g/>
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Komsomolec	komsomolec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
KomsomolecК	KomsomolecК	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Krenkelova	Krenkelův	k2eAgFnSc1d1
zátoka	zátoka	k1gFnSc1
</s>
<s>
Lokalizace	lokalizace	k1gFnSc1
</s>
<s>
Karské	karský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
,	,	kIx,
moře	moře	k1gNnSc4
Laptěvů	Laptěv	k1gInPc2
Stát	stát	k5eAaImF,k5eAaPmF
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc4
•	•	k?
subjekt	subjekt	k1gInSc1
RF	RF	kA
</s>
<s>
Krasnojarský	Krasnojarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Topografie	topografie	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
9	#num#	k4
600	#num#	k4
km²	km²	k?
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
80	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
3	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
94	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
781	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Osídlení	osídlení	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
0	#num#	k4
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
0	#num#	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Komsomolec	komsomolec	k1gMnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
К	К	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejsevernější	severní	k2eAgInSc1d3
ostrov	ostrov	k1gInSc1
souostroví	souostroví	k1gNnSc2
Severní	severní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
omýván	omýván	k2eAgMnSc1d1
vodami	voda	k1gFnPc7
Karského	karský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
moře	moře	k1gNnSc2
Laptěvů	Laptěv	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Ostrova	ostrov	k1gInSc2
Říjnové	říjnový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
je	být	k5eAaImIp3nS
oddělen	oddělit	k5eAaPmNgInS
Průlivem	průliv	k1gInSc7
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
od	od	k7c2
ostrova	ostrov	k1gInSc2
Pioněr	Pioněr	k1gInSc4
jej	on	k3xPp3gMnSc4
odděluje	oddělovat	k5eAaImIp3nS
průliv	průliv	k1gInSc1
Junyj	Junyj	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
do	do	k7c2
rozlohy	rozloha	k1gFnSc2
je	být	k5eAaImIp3nS
třetím	třetí	k4xOgInSc7
ostrovem	ostrov	k1gInSc7
souostroví	souostroví	k1gNnSc2
a	a	k8xC
osmdesátým	osmdesátý	k4xOgInPc3
druhým	druhý	k4xOgMnSc7
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejsevernějším	severní	k2eAgInSc7d3
bodem	bod	k1gInSc7
je	být	k5eAaImIp3nS
mys	mys	k1gInSc1
Arktičeskij	Arktičeskij	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
něho	on	k3xPp3gMnSc2
vyrážejí	vyrážet	k5eAaImIp3nP
mnohé	mnohý	k2eAgFnPc4d1
arktické	arktický	k2eAgFnPc4d1
expedice	expedice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Rozloha	rozloha	k1gFnSc1
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
9	#num#	k4
600	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
výše	vysoce	k6eAd2
781	#num#	k4
m.	m.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
65	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnPc4
pokrývají	pokrývat	k5eAaImIp3nP
ledovce	ledovec	k1gInPc1
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
písky	písek	k1gInPc4
a	a	k8xC
hlíny	hlína	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
ostrova	ostrov	k1gInSc2
zaujímá	zaujímat	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
ledová	ledový	k2eAgFnSc1d1
pokrývka	pokrývka	k1gFnSc1
souostroví	souostroví	k1gNnSc2
–	–	k?
ledovec	ledovec	k1gInSc1
Akademii	akademie	k1gFnSc4
nauk	nauka	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
plocha	plocha	k1gFnSc1
je	být	k5eAaImIp3nS
5	#num#	k4
900	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
tloušťka	tloušťka	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
500	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
nad	nad	k7c4
hladinu	hladina	k1gFnSc4
moře	moře	k1gNnSc2
ční	čnět	k5eAaImIp3nP
do	do	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
749	#num#	k4
m.	m.	k?
Z	z	k7c2
něj	on	k3xPp3gNnSc2
vybíhají	vybíhat	k5eAaImIp3nP
ledovcové	ledovcový	k2eAgInPc4d1
splazy	splaz	k1gInPc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
je	být	k5eAaImIp3nS
nejdelší	dlouhý	k2eAgInSc1d3
ledovec	ledovec	k1gInSc1
Arktičeskogo	Arktičeskogo	k1gNnSc1
instituta	institut	k1gMnSc2
na	na	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dosahuje	dosahovat	k5eAaImIp3nS
délky	délka	k1gFnSc2
40	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objevy	objev	k1gInPc1
</s>
<s>
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
spatřen	spatřit	k5eAaPmNgInS
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1913	#num#	k4
expedicí	expedice	k1gFnPc2
B.	B.	kA
A.	A.	kA
Vilkického	Vilkický	k2eAgInSc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xC,k8xS
úplně	úplně	k6eAd1
poslední	poslední	k2eAgFnSc1d1
pevnina	pevnina	k1gFnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
jej	on	k3xPp3gInSc4
Tajvaj	Tajvaj	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
akronym	akronym	k1gInSc4
z	z	k7c2
počátečních	počáteční	k2eAgNnPc2d1
písmen	písmeno	k1gNnPc2
ledoborců	ledoborec	k1gInPc2
Tajmyr	Tajmyr	k1gInSc1
a	a	k8xC
Vajgač	Vajgač	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
expedice	expedice	k1gFnSc1
do	do	k7c2
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
plula	plout	k5eAaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poprvé	poprvé	k6eAd1
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
zkoumáno	zkoumat	k5eAaImNgNnS
v	v	k7c6
letech	let	k1gInPc6
1930	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
expedicí	expedice	k1gFnPc2
Georgije	Georgije	k1gMnPc2
Ušakova	Ušakův	k2eAgMnSc2d1
a	a	k8xC
Nikolaje	Nikolaj	k1gMnSc2
Urvanceva	Urvancev	k1gMnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
prokázali	prokázat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
ostrov	ostrov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byl	být	k5eAaImAgInS
i	i	k9
nově	nově	k6eAd1
pojmenován	pojmenovat	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Rozlehlou	rozlehlý	k2eAgFnSc4d1
zátoku	zátoka	k1gFnSc4
moře	moře	k1gNnSc2
Laptěvů	Laptěv	k1gInPc2
u	u	k7c2
jihovýchodního	jihovýchodní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
pojmenovali	pojmenovat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1973	#num#	k4
na	na	k7c4
počest	počest	k1gFnSc4
radisty	radista	k1gMnSc2
Krenkela	Krenkel	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
letech	let	k1gInPc6
1935	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
na	na	k7c6
Severní	severní	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
přezimoval	přezimovat	k5eAaBmAgInS
v	v	k7c6
polárních	polární	k2eAgFnPc6d1
stanicích	stanice	k1gFnPc6
М	М	k?
О	О	k?
(	(	kIx(
<g/>
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Ostrova	ostrov	k1gInSc2
Říjnové	říjnový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
О	О	k?
Д	Д	k?
v	v	k7c6
Sedovově	Sedovův	k2eAgNnSc6d1
souostroví	souostroví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Návrh	návrh	k1gInSc1
na	na	k7c6
přejmenování	přejmenování	k1gNnSc6
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2006	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c6
pravidelném	pravidelný	k2eAgNnSc6d1
plenárním	plenární	k2eAgNnSc6d1
zasedání	zasedání	k1gNnSc6
dumy	duma	k1gFnSc2
Tajmyrského	tajmyrský	k2eAgInSc2d1
autonomního	autonomní	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
přijato	přijat	k2eAgNnSc4d1
usnesení	usnesení	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
se	se	k3xPyFc4
navrhuje	navrhovat	k5eAaImIp3nS
vrátit	vrátit	k5eAaPmF
souostroví	souostroví	k1gNnSc3
Severní	severní	k2eAgFnSc2d1
země	zem	k1gFnSc2
původní	původní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
Země	zem	k1gFnSc2
imperátora	imperátor	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
bylo	být	k5eAaImAgNnS
navrženo	navrhnout	k5eAaPmNgNnS
přejmenování	přejmenování	k1gNnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
na	na	k7c4
památku	památka	k1gFnSc4
rodinných	rodinný	k2eAgMnPc2d1
příslušníků	příslušník	k1gMnPc2
posledního	poslední	k2eAgMnSc2d1
cara	car	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komsomolec	komsomolec	k1gMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
přejmenován	přejmenovat	k5eAaPmNgMnS
na	na	k7c4
ostrov	ostrov	k1gInSc4
Svaté	svatý	k2eAgFnSc2d1
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
podle	podle	k7c2
Marie	Maria	k1gFnSc2
Nikolajevny	Nikolajevna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
územněsprávní	územněsprávní	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
autonomní	autonomní	k2eAgInSc1d1
okruh	okruh	k1gInSc1
stal	stát	k5eAaPmAgInS
součástí	součást	k1gFnSc7
Krasnojarského	Krasnojarský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
posuzování	posuzování	k1gNnSc1
této	tento	k3xDgFnSc2
záležitosti	záležitost	k1gFnSc2
na	na	k7c6
federální	federální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
pozastaveno	pozastaven	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
К	К	k?
(	(	kIx(
<g/>
о	о	k?
<g/>
)	)	kIx)
na	na	k7c6
ruské	ruský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Velká	velký	k2eAgFnSc1d1
ruská	ruský	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
věd	věda	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
К	К	k?
<g/>
́	́	k?
<g/>
Л	Л	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Malá	malý	k2eAgFnSc1d1
Československá	československý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
ČSAV	ČSAV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
V.	V.	kA
svazek	svazek	k1gInSc1
Pom	Pom	k1gFnSc1
<g/>
–	–	k?
<g/>
S.	S.	kA
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Severní	severní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
581	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Poslední	poslední	k2eAgInSc1d1
velký	velký	k2eAgInSc1d1
zeměpisný	zeměpisný	k2eAgInSc1d1
objev	objev	k1gInSc1
se	se	k3xPyFc4
podařil	podařit	k5eAaPmAgInS
před	před	k7c7
100	#num#	k4
lety	let	k1gInPc7
v	v	k7c6
Severním	severní	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-03	2013-09-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
К	К	k?
ч	ч	k?
о	о	k?
О	О	k?
Р	Р	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Т	Т	k?
з	з	k?
п	п	k?
в	в	k?
а	а	k?
С	С	k?
З	З	k?
е	е	k?
и	и	k?
н	н	k?
«	«	k?
<g/>
З	З	k?
И	И	k?
Н	Н	k?
II	II	kA
<g/>
»	»	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
patriarchia	patriarchia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ru	ru	k?
<g/>
,	,	kIx,
2006-12-01	2006-12-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
Ruské	ruský	k2eAgFnSc2d1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Д	Д	k?
З	З	k?
К	К	k?
к	к	k?
п	п	k?
п	п	k?
о	о	k?
а	а	k?
С	С	k?
З	З	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
REGNUM	REGNUM	kA
<g/>
,	,	kIx,
2007-05-24	2007-05-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Komsomolec	komsomolec	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
