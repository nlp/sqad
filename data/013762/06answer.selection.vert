<s>
Rozloha	rozloha	k1gFnSc1
ostrova	ostrov	k1gInSc2
je	být	k5eAaImIp3nS
9	#num#	k4
600	#num#	k4
km²	km²	k?
<g/>
,	,	kIx,
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
dosahuje	dosahovat	k5eAaImIp3nS
výše	vysoce	k6eAd2
781	#num#	k4
m.	m.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Přibližně	přibližně	k6eAd1
65	#num#	k4
%	%	kIx~
plochy	plocha	k1gFnPc4
pokrývají	pokrývat	k5eAaImIp3nP
ledovce	ledovec	k1gInPc1
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
písky	písek	k1gInPc4
a	a	k8xC
hlíny	hlína	k1gFnPc4
<g/>
.	.	kIx.
</s>