<s>
Kure	kur	k1gMnSc5	kur
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
呉	呉	k?	呉
<g/>
;	;	kIx,	;
Kure-ši	Kure-š	k1gMnPc1	Kure-š
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
město	město	k1gNnSc1	město
ležící	ležící	k2eAgNnSc1d1	ležící
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
prefektury	prefektura	k1gFnSc2	prefektura
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2007	[number]	k4	2007
mělo	mít	k5eAaImAgNnS	mít
247	[number]	k4	247
412	[number]	k4	412
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
353,29	[number]	k4	353,29
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
市	市	k?	市
<g/>
,	,	kIx,	,
ši	ši	k?	ši
<g/>
)	)	kIx)	)
Kure	kur	k1gMnSc5	kur
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
základna	základna	k1gFnSc1	základna
Japonského	japonský	k2eAgNnSc2d1	Japonské
císařského	císařský	k2eAgNnSc2d1	císařské
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Kure	kur	k1gMnSc5	kur
bylo	být	k5eAaImAgNnS	být
domovskou	domovský	k2eAgFnSc7d1	domovská
základnou	základna	k1gFnSc7	základna
bitevní	bitevní	k2eAgFnSc7d1	bitevní
lodě	loď	k1gFnPc4	loď
Jamato	Jamat	k2eAgNnSc4d1	Jamato
–	–	k?	–
největší	veliký	k2eAgFnSc2d3	veliký
bitevní	bitevní	k2eAgFnSc2d1	bitevní
lodě	loď	k1gFnSc2	loď
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
24	[number]	k4	24
<g/>
.	.	kIx.	.
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
1945	[number]	k4	1945
bombardovaly	bombardovat	k5eAaImAgInP	bombardovat
americké	americký	k2eAgInPc1d1	americký
letouny	letoun	k1gInPc1	letoun
poslední	poslední	k2eAgInPc1d1	poslední
zbytky	zbytek	k1gInPc1	zbytek
japonského	japonský	k2eAgNnSc2d1	Japonské
loďstva	loďstvo	k1gNnSc2	loďstvo
na	na	k7c6	na
námořní	námořní	k2eAgFnSc6d1	námořní
základně	základna	k1gFnSc6	základna
v	v	k7c6	v
Kure	kur	k1gMnSc5	kur
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kure	kur	k1gMnSc5	kur
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
Kure	kur	k1gMnSc5	kur
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
Kure	kur	k1gMnSc5	kur
</s>
