<p>
<s>
Kanton	Kanton	k1gInSc1	Kanton
Beaufort	Beaufort	k1gInSc1	Beaufort
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Beaufort	Beaufort	k1gInSc1	Beaufort
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
kanton	kanton	k1gInSc4	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Jura	Jura	k1gMnSc1	Jura
v	v	k7c6	v
regionu	region	k1gInSc6	region
Franche-Comté	Franche-Comtý	k2eAgNnSc1d1	Franche-Comté
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
ho	on	k3xPp3gNnSc4	on
18	[number]	k4	18
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnSc2	obec
kantonu	kanton	k1gInSc2	kanton
==	==	k?	==
</s>
</p>
<p>
<s>
Augea	Augea	k6eAd1	Augea
</s>
</p>
<p>
<s>
Augisey	Augisea	k1gFnPc1	Augisea
</s>
</p>
<p>
<s>
Beaufort	Beaufort	k1gInSc1	Beaufort
</s>
</p>
<p>
<s>
Bonnaud	Bonnaud	k6eAd1	Bonnaud
</s>
</p>
<p>
<s>
Cesancey	Cesancea	k1gFnPc1	Cesancea
</s>
</p>
<p>
<s>
Cousance	Cousance	k1gFnSc1	Cousance
</s>
</p>
<p>
<s>
Cuisia	Cuisia	k1gFnSc1	Cuisia
</s>
</p>
<p>
<s>
Gizia	Gizia	k1gFnSc1	Gizia
</s>
</p>
<p>
<s>
Grusse	Grusse	k6eAd1	Grusse
</s>
</p>
<p>
<s>
Mallerey	Mallerea	k1gFnPc1	Mallerea
</s>
</p>
<p>
<s>
Maynal	Maynat	k5eAaImAgMnS	Maynat
</s>
</p>
<p>
<s>
Orbagna	Orbagna	k6eAd1	Orbagna
</s>
</p>
<p>
<s>
Rosay	Rosaa	k1gFnPc1	Rosaa
</s>
</p>
<p>
<s>
Rotalier	Rotalier	k1gMnSc1	Rotalier
</s>
</p>
<p>
<s>
Sainte-Agnè	Sainte-Agnè	k?	Sainte-Agnè
</s>
</p>
<p>
<s>
Saint-Laurent-la-Roche	Saint-Laurenta-Roche	k6eAd1	Saint-Laurent-la-Roche
</s>
</p>
<p>
<s>
Vercia	Vercia	k1gFnSc1	Vercia
</s>
</p>
<p>
<s>
Vincelles	Vincelles	k1gMnSc1	Vincelles
</s>
</p>
