<s>
Ganymed	Ganymed	k1gMnSc1	Ganymed
objevil	objevit	k5eAaPmAgMnS	objevit
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galilei	k1gNnPc6	Galilei
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
pozorování	pozorování	k1gNnSc2	pozorování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měsíc	měsíc	k1gInSc4	měsíc
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
jiný	jiný	k2eAgMnSc1d1	jiný
astronom	astronom	k1gMnSc1	astronom
Simon	Simon	k1gMnSc1	Simon
Marius	Marius	k1gMnSc1	Marius
dle	dle	k7c2	dle
postavy	postava	k1gFnSc2	postava
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
Ganyméda	Ganyméd	k1gMnSc2	Ganyméd
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
milencem	milenec	k1gMnSc7	milenec
boha	bůh	k1gMnSc2	bůh
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
číšníkem	číšník	k1gMnSc7	číšník
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
