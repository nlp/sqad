<s>
Lughnasadh	Lughnasadh	k1gInSc1	Lughnasadh
(	(	kIx(	(
<g/>
také	také	k9	také
Lughnasad	Lughnasad	k1gInSc4	Lughnasad
<g/>
,	,	kIx,	,
čti	číst	k5eAaImRp2nS	číst
"	"	kIx"	"
<g/>
lúnə	lúnə	k?	lúnə
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Lughovy	Lughov	k1gInPc1	Lughov
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
hry	hra	k1gFnSc2	hra
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
též	též	k9	též
"	"	kIx"	"
<g/>
Lughova	Lughův	k2eAgFnSc1d1	Lughův
svatba	svatba	k1gFnSc1	svatba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
keltský	keltský	k2eAgInSc4d1	keltský
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Lughnasadh	Lughnasadh	k1gInSc1	Lughnasadh
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
hlavních	hlavní	k2eAgFnPc2d1	hlavní
keltských	keltský	k2eAgFnPc2d1	keltská
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgMnPc4	který
patřily	patřit	k5eAaImAgFnP	patřit
také	také	k6eAd1	také
Imbolc	Imbolc	k1gInSc4	Imbolc
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
února	únor	k1gInSc2	únor
<g/>
,	,	kIx,	,
Beltine	Beltin	k1gInSc5	Beltin
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
a	a	k8xC	a
Samhain	Samhaina	k1gFnPc2	Samhaina
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Lughnasadh	Lughnasadh	k1gMnSc1	Lughnasadh
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
začátek	začátek	k1gInSc4	začátek
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
scházeli	scházet	k5eAaImAgMnP	scházet
se	s	k7c7	s
vzdáleným	vzdálený	k2eAgNnSc7d1	vzdálené
příbuzenstvem	příbuzenstvo	k1gNnSc7	příbuzenstvo
<g/>
,	,	kIx,	,
konaly	konat	k5eAaImAgInP	konat
se	se	k3xPyFc4	se
trhy	trh	k1gInPc1	trh
i	i	k8xC	i
koňské	koňský	k2eAgInPc1d1	koňský
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
bylo	být	k5eAaImAgNnS	být
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
"	"	kIx"	"
<g/>
manželství	manželství	k1gNnSc1	manželství
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
také	také	k9	také
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nechal	nechat	k5eAaPmAgMnS	nechat
připouštět	připouštět	k5eAaImF	připouštět
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
dny	den	k1gInPc1	den
byly	být	k5eAaImAgInP	být
dobou	doba	k1gFnSc7	doba
plodnosti	plodnost	k1gFnPc1	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Lughnasadh	Lughnasadh	k1gMnSc1	Lughnasadh
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
započat	započnout	k5eAaPmNgInS	započnout
samotným	samotný	k2eAgMnSc7d1	samotný
bohem	bůh	k1gMnSc7	bůh
Lughem	Lugh	k1gInSc7	Lugh
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
pohřební	pohřební	k2eAgFnSc1d1	pohřební
hostina	hostina	k1gFnSc1	hostina
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
jeho	jeho	k3xOp3gFnSc2	jeho
nevlastní	vlastní	k2eNgFnSc2d1	nevlastní
matky	matka	k1gFnSc2	matka
Tailtiu	Tailtius	k1gMnSc3	Tailtius
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zemřela	zemřít	k5eAaPmAgFnS	zemřít
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zúrodnění	zúrodnění	k1gNnSc6	zúrodnění
irských	irský	k2eAgFnPc2d1	irská
plání	pláň	k1gFnPc2	pláň
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
snopku	snopek	k1gInSc2	snopek
nedozrálého	dozrálý	k2eNgNnSc2d1	nedozrálé
obilí	obilí	k1gNnSc2	obilí
se	se	k3xPyFc4	se
pekly	péct	k5eAaImAgInP	péct
malé	malý	k2eAgInPc1d1	malý
bochníčky	bochníček	k1gInPc1	bochníček
chleba	chléb	k1gInSc2	chléb
<g/>
.	.	kIx.	.
</s>
<s>
Snop	snop	k1gInSc1	snop
obilí	obilí	k1gNnSc2	obilí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
rukou	ruka	k1gFnPc2	ruka
utrhl	utrhnout	k5eAaPmAgMnS	utrhnout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ponechával	ponechávat	k5eAaImAgMnS	ponechávat
až	až	k9	až
do	do	k7c2	do
příštího	příští	k2eAgNnSc2d1	příští
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Časně	časně	k6eAd1	časně
ráno	ráno	k1gNnSc1	ráno
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
jít	jít	k5eAaImF	jít
na	na	k7c4	na
kopec	kopec	k1gInSc4	kopec
pozdravit	pozdravit	k5eAaPmF	pozdravit
vycházející	vycházející	k2eAgNnSc4d1	vycházející
slunce	slunce	k1gNnSc4	slunce
a	a	k8xC	a
prosit	prosit	k5eAaImF	prosit
o	o	k7c4	o
přízeň	přízeň	k1gFnSc4	přízeň
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Lugh	Lugh	k1gInSc1	Lugh
byl	být	k5eAaImAgInS	být
patronem	patron	k1gInSc7	patron
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
,	,	kIx,	,
učenců	učenec	k1gMnPc2	učenec
i	i	k8xC	i
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
