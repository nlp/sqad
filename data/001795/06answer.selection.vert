<s>
Lughnasadh	Lughnasadh	k1gInSc1	Lughnasadh
(	(	kIx(	(
<g/>
také	také	k9	také
Lughnasad	Lughnasad	k1gInSc4	Lughnasad
<g/>
,	,	kIx,	,
čti	číst	k5eAaImRp2nS	číst
"	"	kIx"	"
<g/>
lúnə	lúnə	k?	lúnə
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Lughovy	Lughov	k1gInPc1	Lughov
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
hry	hra	k1gFnSc2	hra
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
též	též	k9	též
"	"	kIx"	"
<g/>
Lughova	Lughův	k2eAgFnSc1d1	Lughův
svatba	svatba	k1gFnSc1	svatba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
keltský	keltský	k2eAgInSc4d1	keltský
svátek	svátek	k1gInSc4	svátek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
