<p>
<s>
Martín	Martín	k1gInSc1	Martín
Fierro	Fierro	k1gNnSc1	Fierro
je	být	k5eAaImIp3nS	být
epická	epický	k2eAgFnSc1d1	epická
báseň	báseň	k1gFnSc1	báseň
argentinského	argentinský	k2eAgMnSc2d1	argentinský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Josého	Josého	k2eAgFnSc2d1	Josého
Hernándeze	Hernándeze	k1gFnSc2	Hernándeze
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
2316	[number]	k4	2316
veršů	verš	k1gInPc2	verš
a	a	k8xC	a
původně	původně	k6eAd1	původně
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
El	Ela	k1gFnPc2	Ela
Gaucho	Gaucha	k1gFnSc5	Gaucha
Martín	Martín	k1gInSc1	Martín
Fierro	Fierro	k1gNnSc1	Fierro
(	(	kIx(	(
<g/>
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
a	a	k8xC	a
La	la	k1gNnSc1	la
Vuelta	Vuelt	k1gInSc2	Vuelt
de	de	k?	de
Martín	Martín	k1gInSc1	Martín
Fierro	Fierro	k1gNnSc1	Fierro
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
argentinský	argentinský	k2eAgInSc4d1	argentinský
národní	národní	k2eAgInSc4d1	národní
epos	epos	k1gInSc4	epos
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
argentinské	argentinský	k2eAgNnSc4d1	argentinské
národní	národní	k2eAgNnSc4d1	národní
obrození	obrození	k1gNnSc4	obrození
a	a	k8xC	a
boj	boj	k1gInSc4	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hráli	hrát	k5eAaImAgMnP	hrát
velkou	velká	k1gFnSc7	velká
roli	role	k1gFnSc3	role
gaučové	gaučo	k1gMnPc1	gaučo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
eposu	epos	k1gInSc2	epos
je	být	k5eAaImIp3nS	být
zchudlý	zchudlý	k2eAgMnSc1d1	zchudlý
gaučo	gaučo	k1gMnSc1	gaučo
Martín	Martín	k1gMnSc1	Martín
Fierro	Fierro	k1gNnSc4	Fierro
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
naverbován	naverbován	k2eAgMnSc1d1	naverbován
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
pevnosti	pevnost	k1gFnSc6	pevnost
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
hranici	hranice	k1gFnSc4	hranice
Argentiny	Argentina	k1gFnSc2	Argentina
a	a	k8xC	a
chránil	chránit	k5eAaImAgMnS	chránit
ji	on	k3xPp3gFnSc4	on
před	před	k7c7	před
indiány	indián	k1gMnPc7	indián
<g/>
.	.	kIx.	.
</s>
<s>
Báseň	báseň	k1gFnSc1	báseň
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaBmNgFnS	napsat
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
používané	používaný	k2eAgFnSc2d1	používaná
na	na	k7c6	na
argentinském	argentinský	k2eAgInSc6d1	argentinský
venkově	venkov	k1gInSc6	venkov
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
národní	národní	k2eAgFnSc2d1	národní
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Přeložena	přeložen	k2eAgFnSc1d1	přeložena
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Báseň	báseň	k1gFnSc1	báseň
Martín	Martína	k1gFnPc2	Martína
Fierro	Fierra	k1gFnSc5	Fierra
ocenili	ocenit	k5eAaPmAgMnP	ocenit
významní	významný	k2eAgMnPc1d1	významný
literáti	literát	k1gMnPc1	literát
jako	jako	k8xS	jako
Leopoldo	Leopolda	k1gFnSc5	Leopolda
Lugones	Lugones	k1gMnSc1	Lugones
<g/>
,	,	kIx,	,
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Unamuno	Unamuna	k1gFnSc5	Unamuna
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gFnPc3	Jorge
Luis	Luisa	k1gFnPc2	Luisa
Borges	Borgesa	k1gFnPc2	Borgesa
či	či	k8xC	či
Rafael	Rafaela	k1gFnPc2	Rafaela
Squirru	Squirr	k1gInSc2	Squirr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
jsou	být	k5eAaImIp3nP	být
každoročně	každoročně	k6eAd1	každoročně
udělovány	udělovat	k5eAaImNgFnP	udělovat
ceny	cena	k1gFnPc1	cena
Martína	Martín	k1gInSc2	Martín
Fierra	Fierro	k1gNnSc2	Fierro
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnPc4d1	televizní
a	a	k8xC	a
rozhlasové	rozhlasový	k2eAgFnPc4d1	rozhlasová
osobnosti	osobnost	k1gFnPc4	osobnost
a	a	k8xC	a
pořady	pořad	k1gInPc4	pořad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Martín	Martína	k1gFnPc2	Martína
Fierro	Fierro	k1gNnSc4	Fierro
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Martín	Martína	k1gFnPc2	Martína
Fierro	Fierro	k1gNnSc4	Fierro
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
