<s>
Klisna	klisna	k1gFnSc1	klisna
Registana	Registana	k1gFnSc1	Registana
je	být	k5eAaImIp3nS	být
dvojnásobná	dvojnásobný	k2eAgFnSc1d1	dvojnásobná
vítězka	vítězka	k1gFnSc1	vítězka
Velké	velký	k2eAgFnSc2d1	velká
pardubické	pardubický	k2eAgFnSc2d1	pardubická
steeplechase	steeplechase	k1gFnSc2	steeplechase
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2003	[number]	k4	2003
a	a	k8xC	a
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
ročnících	ročník	k1gInPc6	ročník
s	s	k7c7	s
žokejem	žokej	k1gMnSc7	žokej
Peterem	Peter	k1gMnSc7	Peter
Gehmem	Gehm	k1gMnSc7	Gehm
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1996	[number]	k4	1996
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
z	z	k7c2	z
klisny	klisna	k1gFnSc2	klisna
Reklame	Reklam	k1gInSc5	Reklam
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
Tauchsport	Tauchsport	k1gInSc4	Tauchsport
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
novým	nový	k2eAgMnSc7d1	nový
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
dostihová	dostihový	k2eAgFnSc1d1	dostihová
stáj	stáj	k1gFnSc1	stáj
Wrbna	Wrbn	k1gInSc2	Wrbn
Racing	Racing	k1gInSc1	Racing
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2006	[number]	k4	2006
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
dostihovou	dostihový	k2eAgFnSc4d1	dostihová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
33	[number]	k4	33
závodů	závod	k1gInPc2	závod
steeplechase	steeplechase	k1gFnSc2	steeplechase
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
21	[number]	k4	21
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
10	[number]	k4	10
444	[number]	k4	444
608	[number]	k4	608
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
12	[number]	k4	12
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2008	[number]	k4	2008
-	-	kIx~	-
klisna	klisna	k1gFnSc1	klisna
Regine	Regin	k1gInSc5	Regin
(	(	kIx(	(
<g/>
GER	Gera	k1gFnPc2	Gera
<g/>
)	)	kIx)	)
po	po	k7c4	po
Oscar	Oscar	k1gInSc4	Oscar
(	(	kIx(	(
<g/>
IRE	Ir	k1gMnSc5	Ir
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
-	-	kIx~	-
valach	valach	k1gMnSc1	valach
Reaper	Reaper	k1gMnSc1	Reaper
(	(	kIx(	(
<g/>
GER	Gera	k1gFnPc2	Gera
<g/>
)	)	kIx)	)
po	po	k7c4	po
Sholokhov	Sholokhov	k1gInSc4	Sholokhov
(	(	kIx(	(
<g/>
IRE	Ir	k1gMnSc5	Ir
<g/>
)	)	kIx)	)
28	[number]	k4	28
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
valach	valach	k1gInSc1	valach
Rafa	Raf	k1gInSc2	Raf
po	po	k7c4	po
Linngari	Linngare	k1gFnSc4	Linngare
(	(	kIx(	(
<g/>
IRE	Ir	k1gMnSc5	Ir
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
2014	[number]	k4	2014
utracen	utracen	k2eAgInSc4d1	utracen
7	[number]	k4	7
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
-	-	kIx~	-
valach	valach	k1gInSc1	valach
Reki	Rek	k1gFnSc2	Rek
po	po	k7c4	po
Look	Look	k1gInSc4	Look
Honay	Honaa	k1gFnSc2	Honaa
(	(	kIx(	(
<g/>
IRE	Ir	k1gMnSc5	Ir
<g/>
)	)	kIx)	)
7	[number]	k4	7
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
-	-	kIx~	-
valach	valach	k1gInSc1	valach
Randy	rand	k1gInPc4	rand
po	po	k7c4	po
Next	Next	k2eAgInSc4d1	Next
Desert	desert	k1gInSc4	desert
(	(	kIx(	(
<g/>
IRE	Ir	k1gMnSc5	Ir
<g/>
)	)	kIx)	)
13	[number]	k4	13
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
-	-	kIx~	-
klisna	klisna	k1gFnSc1	klisna
Relandi	Reland	k1gMnPc1	Reland
po	po	k7c6	po
Alandi	Aland	k1gMnPc1	Aland
(	(	kIx(	(
<g/>
IRE	Ir	k1gMnSc5	Ir
<g/>
)	)	kIx)	)
</s>
