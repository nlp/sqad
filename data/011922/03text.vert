<p>
<s>
První	první	k4xOgFnSc1	první
intifáda	intifáda	k1gFnSc1	intifáda
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
procitnutí	procitnutí	k1gNnSc2	procitnutí
<g/>
"	"	kIx"	"
či	či	k8xC	či
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
"	"	kIx"	"
<g/>
válka	válka	k1gFnSc1	válka
kamenů	kámen	k1gInPc2	kámen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
palestinské	palestinský	k2eAgNnSc1d1	palestinské
povstání	povstání	k1gNnSc1	povstání
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
palestinskými	palestinský	k2eAgMnPc7d1	palestinský
Araby	Arab	k1gMnPc7	Arab
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
intifáda	intifáda	k1gFnSc1	intifáda
vedena	vést	k5eAaImNgFnS	vést
Organizací	organizace	k1gFnSc7	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
(	(	kIx(	(
<g/>
OOP	OOP	kA	OOP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
výraznou	výrazný	k2eAgFnSc4d1	výrazná
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
povstání	povstání	k1gNnSc6	povstání
získala	získat	k5eAaPmAgFnS	získat
palestinská	palestinský	k2eAgFnSc1d1	palestinská
militantní	militantní	k2eAgFnSc1d1	militantní
islamistická	islamistický	k2eAgFnSc1d1	islamistická
skupina	skupina	k1gFnSc1	skupina
Hamás	Hamása	k1gFnPc2	Hamása
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
první	první	k4xOgFnSc2	první
intifády	intifáda	k1gFnSc2	intifáda
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
ozbrojeným	ozbrojený	k2eAgInPc3d1	ozbrojený
bojům	boj	k1gInPc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Palestinské	palestinský	k2eAgFnPc1d1	palestinská
akce	akce	k1gFnPc1	akce
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
porušování	porušování	k1gNnSc4	porušování
veřejného	veřejný	k2eAgInSc2d1	veřejný
pořádku	pořádek	k1gInSc2	pořádek
<g/>
,	,	kIx,	,
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
generální	generální	k2eAgFnPc1d1	generální
stávky	stávka	k1gFnPc1	stávka
<g/>
,	,	kIx,	,
bojkoty	bojkot	k1gInPc1	bojkot
<g/>
,	,	kIx,	,
házení	házení	k1gNnSc1	házení
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
zapalování	zapalování	k1gNnSc4	zapalování
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
graffiti	graffiti	k1gNnSc2	graffiti
<g/>
,	,	kIx,	,
barikády	barikáda	k1gFnSc2	barikáda
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
občas	občas	k6eAd1	občas
byly	být	k5eAaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
skutečné	skutečný	k2eAgFnPc1d1	skutečná
teroristické	teroristický	k2eAgFnPc1d1	teroristická
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgNnPc6	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
tzv.	tzv.	kA	tzv.
Molotovových	Molotovův	k2eAgInPc2d1	Molotovův
koktejlů	koktejl	k1gInPc2	koktejl
či	či	k8xC	či
granátů	granát	k1gInPc2	granát
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
se	se	k3xPyFc4	se
rozrostly	rozrůst	k5eAaPmAgInP	rozrůst
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
ukončeny	ukončit	k5eAaPmNgInP	ukončit
až	až	k6eAd1	až
podepsáním	podepsání	k1gNnSc7	podepsání
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Rozbuškou	rozbuška	k1gFnSc7	rozbuška
první	první	k4xOgFnSc2	první
intifády	intifáda	k1gFnSc2	intifáda
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
dopravní	dopravní	k2eAgFnSc1d1	dopravní
nehoda	nehoda	k1gFnSc1	nehoda
izraelského	izraelský	k2eAgInSc2d1	izraelský
a	a	k8xC	a
palestinského	palestinský	k2eAgInSc2d1	palestinský
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
čtyři	čtyři	k4xCgMnPc4	čtyři
palestinští	palestinský	k2eAgMnPc1d1	palestinský
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
palestinské	palestinský	k2eAgFnSc2d1	palestinská
strany	strana	k1gFnSc2	strana
měl	mít	k5eAaImAgMnS	mít
Izraelec	Izraelec	k1gMnSc1	Izraelec
nehodu	nehoda	k1gFnSc4	nehoda
způsobit	způsobit	k5eAaPmF	způsobit
záměrně	záměrně	k6eAd1	záměrně
jako	jako	k8xC	jako
pomstu	pomsta	k1gFnSc4	pomsta
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
Izraelce	Izraelec	k1gMnSc2	Izraelec
spáchanou	spáchaný	k2eAgFnSc7d1	spáchaná
v	v	k7c6	v
Gaze	Gaze	k1gFnPc6	Gaze
o	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
intifáda	intifáda	k1gFnSc1	intifáda
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
demonstrací	demonstrace	k1gFnPc2	demonstrace
<g/>
,	,	kIx,	,
nepokoje	nepokoj	k1gInSc2	nepokoj
se	se	k3xPyFc4	se
však	však	k9	však
zakrátko	zakrátko	k6eAd1	zakrátko
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
i	i	k9	i
na	na	k7c4	na
Západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Násilnostmi	násilnost	k1gFnPc7	násilnost
nepokojů	nepokoj	k1gInPc2	nepokoj
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
jejich	jejich	k3xOp3gInSc7	jejich
rozsahem	rozsah	k1gInSc7	rozsah
byli	být	k5eAaImAgMnP	být
zaskočeni	zaskočen	k2eAgMnPc1d1	zaskočen
izraelští	izraelský	k2eAgMnPc1d1	izraelský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nebyli	být	k5eNaImAgMnP	být
vycvičeni	vycvičit	k5eAaPmNgMnP	vycvičit
k	k	k7c3	k
potírání	potírání	k1gNnSc3	potírání
lidových	lidový	k2eAgNnPc2d1	lidové
povstání	povstání	k1gNnPc2	povstání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
cizí	cizí	k2eAgFnSc3d1	cizí
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
mnohých	mnohý	k2eAgFnPc2d1	mnohá
palestinských	palestinský	k2eAgFnPc2d1	palestinská
vesnic	vesnice	k1gFnPc2	vesnice
jim	on	k3xPp3gMnPc3	on
byl	být	k5eAaImAgInS	být
zablokován	zablokovat	k5eAaPmNgInS	zablokovat
přístup	přístup	k1gInSc1	přístup
a	a	k8xC	a
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
měst	město	k1gNnPc2	město
a	a	k8xC	a
sídel	sídlo	k1gNnPc2	sídlo
v	v	k7c6	v
Gaze	Gaze	k1gFnSc6	Gaze
a	a	k8xC	a
na	na	k7c6	na
Západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
jim	on	k3xPp3gMnPc3	on
Arabové	Arab	k1gMnPc1	Arab
nadávali	nadávat	k5eAaImAgMnP	nadávat
a	a	k8xC	a
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
kamením	kamenit	k5eAaImIp1nS	kamenit
a	a	k8xC	a
zápalnými	zápalný	k2eAgFnPc7d1	zápalná
lahvemi	lahev	k1gFnPc7	lahev
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
nepokojů	nepokoj	k1gInPc2	nepokoj
byly	být	k5eAaImAgFnP	být
dále	daleko	k6eAd2	daleko
generální	generální	k2eAgFnPc1d1	generální
stávky	stávka	k1gFnPc1	stávka
<g/>
,	,	kIx,	,
bojkoty	bojkot	k1gInPc1	bojkot
<g/>
,	,	kIx,	,
zapalování	zapalování	k1gNnSc1	zapalování
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
graffiti	graffiti	k1gNnSc1	graffiti
<g/>
,	,	kIx,	,
vyvěšování	vyvěšování	k1gNnSc1	vyvěšování
palestinských	palestinský	k2eAgFnPc2d1	palestinská
vlajek	vlajka	k1gFnPc2	vlajka
a	a	k8xC	a
barikády	barikáda	k1gFnPc4	barikáda
<g/>
;	;	kIx,	;
použití	použití	k1gNnSc4	použití
těžkých	těžký	k2eAgFnPc2d1	těžká
zbraní	zbraň	k1gFnPc2	zbraň
či	či	k8xC	či
sebevražedných	sebevražedný	k2eAgInPc2d1	sebevražedný
útoků	útok	k1gInPc2	útok
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
násilností	násilnost	k1gFnPc2	násilnost
jednali	jednat	k5eAaImAgMnP	jednat
vojáci	voják	k1gMnPc1	voják
podrážděně	podrážděně	k6eAd1	podrážděně
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
razantně	razantně	k6eAd1	razantně
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Jicchak	Jicchak	k1gInSc1	Jicchak
Rabin	Rabin	k1gInSc1	Rabin
přijal	přijmout	k5eAaPmAgInS	přijmout
tvrdá	tvrdý	k2eAgNnPc4d1	tvrdé
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
demonstrací	demonstrace	k1gFnPc2	demonstrace
a	a	k8xC	a
zastrašení	zastrašení	k1gNnSc1	zastrašení
demonstrantů	demonstrant	k1gMnPc2	demonstrant
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
politiky	politika	k1gFnSc2	politika
"	"	kIx"	"
<g/>
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
bití	bití	k1gNnSc2	bití
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
používala	používat	k5eAaImAgFnS	používat
slzný	slzný	k2eAgInSc4d1	slzný
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
kaučukové	kaučukový	k2eAgFnPc4d1	kaučuková
a	a	k8xC	a
plastové	plastový	k2eAgFnPc4d1	plastová
kulky	kulka	k1gFnPc4	kulka
a	a	k8xC	a
v	v	k7c6	v
krajní	krajní	k2eAgFnSc6d1	krajní
nouzi	nouze	k1gFnSc6	nouze
ostrou	ostrý	k2eAgFnSc4d1	ostrá
munici	munice	k1gFnSc4	munice
<g/>
.	.	kIx.	.
</s>
<s>
Gilbert	Gilbert	k1gMnSc1	Gilbert
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Izrael	Izrael	k1gInSc4	Izrael
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc4	dějiny
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Rabinovu	Rabinův	k2eAgFnSc4d1	Rabinova
instruktáž	instruktáž	k1gFnSc4	instruktáž
armádních	armádní	k2eAgMnPc2d1	armádní
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
Martin	Martin	k1gMnSc1	Martin
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
s.	s.	k?	s.
513	[number]	k4	513
</s>
</p>
<p>
<s>
Údajný	údajný	k2eAgInSc1d1	údajný
výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
zlámejte	zlámat	k5eAaPmRp2nP	zlámat
jim	on	k3xPp3gFnPc3	on
kosti	kost	k1gFnPc4	kost
<g/>
"	"	kIx"	"
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
rezonoval	rezonovat	k5eAaImAgMnS	rezonovat
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
izraelští	izraelský	k2eAgMnPc1d1	izraelský
poslanci	poslanec	k1gMnPc1	poslanec
žádali	žádat	k5eAaImAgMnP	žádat
ustanovení	ustanovení	k1gNnSc4	ustanovení
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
prošetřila	prošetřit	k5eAaPmAgFnS	prošetřit
Rabinovy	Rabinův	k2eAgInPc4d1	Rabinův
rozkazy	rozkaz	k1gInPc4	rozkaz
a	a	k8xC	a
postupy	postup	k1gInPc4	postup
během	během	k7c2	během
intifády	intifáda	k1gFnSc2	intifáda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ley	Lea	k1gFnSc2	Lea
Rabinové	Rabinová	k1gFnSc2	Rabinová
<g/>
,	,	kIx,	,
manželky	manželka	k1gFnPc1	manželka
Jicchaka	Jicchak	k1gMnSc2	Jicchak
Rabina	Rabina	k1gFnSc1	Rabina
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
takový	takový	k3xDgInSc4	takový
výrok	výrok	k1gInSc4	výrok
nikdy	nikdy	k6eAd1	nikdy
nepronesl	pronést	k5eNaPmAgMnS	pronést
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
verze	verze	k1gFnSc2	verze
ho	on	k3xPp3gMnSc4	on
skutečně	skutečně	k6eAd1	skutečně
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
izraelské	izraelský	k2eAgMnPc4d1	izraelský
vojáky	voják	k1gMnPc4	voják
odradil	odradit	k5eAaPmAgInS	odradit
od	od	k7c2	od
střelby	střelba	k1gFnSc2	střelba
na	na	k7c4	na
palestinské	palestinský	k2eAgMnPc4d1	palestinský
Araby	Arab	k1gMnPc4	Arab
házející	házející	k2eAgInPc1d1	házející
kameny	kámen	k1gInPc1	kámen
<g/>
.	.	kIx.	.
<g/>
Samotné	samotný	k2eAgNnSc1d1	samotné
povstání	povstání	k1gNnSc1	povstání
překvapilo	překvapit	k5eAaPmAgNnS	překvapit
i	i	k9	i
Organizaci	organizace	k1gFnSc4	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
v	v	k7c6	v
Tunisku	Tunisko	k1gNnSc6	Tunisko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
povstání	povstání	k1gNnSc4	povstání
"	"	kIx"	"
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
vést	vést	k5eAaImF	vést
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
však	však	k9	však
výraznou	výrazný	k2eAgFnSc4d1	výrazná
moc	moc	k1gFnSc4	moc
u	u	k7c2	u
povstalců	povstalec	k1gMnPc2	povstalec
získala	získat	k5eAaPmAgFnS	získat
palestinská	palestinský	k2eAgFnSc1d1	palestinská
militantní	militantní	k2eAgFnSc1d1	militantní
islamistická	islamistický	k2eAgFnSc1d1	islamistická
skupina	skupina	k1gFnSc1	skupina
Hamás	Hamása	k1gFnPc2	Hamása
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
první	první	k4xOgFnSc2	první
intifády	intifáda	k1gFnSc2	intifáda
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
se	se	k3xPyFc4	se
rozrostly	rozrůst	k5eAaPmAgInP	rozrůst
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
ukončeny	ukončit	k5eAaPmNgInP	ukončit
až	až	k6eAd1	až
podepsáním	podepsání	k1gNnSc7	podepsání
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
selhání	selhání	k1gNnSc1	selhání
politiky	politika	k1gFnSc2	politika
"	"	kIx"	"
<g/>
železné	železný	k2eAgFnSc2d1	železná
pěsti	pěst	k1gFnSc2	pěst
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
izraelské	izraelský	k2eAgFnSc2d1	izraelská
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
prestiže	prestiž	k1gFnSc2	prestiž
<g/>
,	,	kIx,	,
přerušení	přerušení	k1gNnSc1	přerušení
administrativních	administrativní	k2eAgFnPc2d1	administrativní
vazeb	vazba	k1gFnPc2	vazba
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
na	na	k7c4	na
Západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
americké	americký	k2eAgNnSc4d1	americké
uznání	uznání	k1gNnSc4	uznání
OOP	OOP	kA	OOP
zástupcem	zástupce	k1gMnSc7	zástupce
palestinského	palestinský	k2eAgInSc2d1	palestinský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
donutily	donutit	k5eAaPmAgFnP	donutit
Rabina	Rabin	k2eAgNnSc2d1	Rabin
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
násilí	násilí	k1gNnPc2	násilí
jednáním	jednání	k1gNnSc7	jednání
a	a	k8xC	a
dialogem	dialog	k1gInSc7	dialog
s	s	k7c7	s
Arafatovou	Arafatův	k2eAgFnSc7d1	Arafatova
Organizací	organizace	k1gFnSc7	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběti	oběť	k1gFnPc1	oběť
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
intifáda	intifáda	k1gFnSc1	intifáda
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
množství	množství	k1gNnSc4	množství
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uváděné	uváděný	k2eAgInPc4d1	uváděný
údaje	údaj	k1gInPc4	údaj
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
statistik	statistika	k1gFnPc2	statistika
izraelské	izraelský	k2eAgFnSc2d1	izraelská
nevládní	vládní	k2eNgFnSc2d1	nevládní
organizace	organizace	k1gFnSc2	organizace
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
Be-celem	Beel	k1gMnSc7	Be-cel
a	a	k8xC	a
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
od	od	k7c2	od
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
tohoto	tento	k3xDgInSc2	tento
konfliktu	konflikt	k1gInSc2	konflikt
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
mírové	mírový	k2eAgFnSc2d1	mírová
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
Osla	Oslo	k1gNnSc2	Oslo
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
jak	jak	k6eAd1	jak
oběti	oběť	k1gFnPc1	oběť
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
izraelských	izraelský	k2eAgFnPc2d1	izraelská
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
zelené	zelený	k2eAgFnSc2d1	zelená
linie	linie	k1gFnSc2	linie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
Pásmo	pásmo	k1gNnSc4	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
,	,	kIx,	,
Západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
Jordánu	Jordán	k1gInSc2	Jordán
a	a	k8xC	a
podle	podle	k7c2	podle
Be-celem	Beel	k1gInSc7	Be-cel
též	též	k6eAd1	též
východní	východní	k2eAgInSc4d1	východní
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
palestinských	palestinský	k2eAgFnPc2d1	palestinská
obětí	oběť	k1gFnPc2	oběť
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k6eAd1	jak
ty	ten	k3xDgMnPc4	ten
zabité	zabitý	k1gMnPc4	zabitý
izraelskými	izraelský	k2eAgFnPc7d1	izraelská
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
složkami	složka	k1gFnPc7	složka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ty	ten	k3xDgMnPc4	ten
zabité	zabitý	k1gMnPc4	zabitý
izraelskými	izraelský	k2eAgMnPc7d1	izraelský
civilisty	civilista	k1gMnPc4	civilista
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
izraelských	izraelský	k2eAgFnPc2d1	izraelská
obětí	oběť	k1gFnPc2	oběť
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k6eAd1	jak
izraelské	izraelský	k2eAgFnPc4d1	izraelská
civilní	civilní	k2eAgFnPc4d1	civilní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
vojenské	vojenský	k2eAgFnPc4d1	vojenská
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
tak	tak	k6eAd1	tak
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
100	[number]	k4	100
izraelských	izraelský	k2eAgMnPc2d1	izraelský
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
47	[number]	k4	47
byli	být	k5eAaImAgMnP	být
osadníci	osadník	k1gMnPc1	osadník
<g/>
,	,	kIx,	,
a	a	k8xC	a
60	[number]	k4	60
příslušníků	příslušník	k1gMnPc2	příslušník
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
1087	[number]	k4	1087
Palestinců	Palestinec	k1gMnPc2	Palestinec
zabito	zabít	k5eAaPmNgNnS	zabít
izraelskými	izraelský	k2eAgFnPc7d1	izraelská
bezpečnostními	bezpečnostní	k2eAgFnPc7d1	bezpečnostní
složkami	složka	k1gFnPc7	složka
a	a	k8xC	a
75	[number]	k4	75
dalších	další	k2eAgFnPc2d1	další
zabito	zabít	k5eAaPmNgNnS	zabít
izraelskými	izraelský	k2eAgMnPc7d1	izraelský
civilisty	civilista	k1gMnPc7	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
tabulka	tabulka	k1gFnSc1	tabulka
je	být	k5eAaImIp3nS	být
souhrnem	souhrn	k1gInSc7	souhrn
těchto	tento	k3xDgFnPc2	tento
statistik	statistika	k1gFnPc2	statistika
za	za	k7c4	za
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
roky	rok	k1gInPc4	rok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Přibližně	přibližně	k6eAd1	přibližně
1000	[number]	k4	1000
dalších	další	k2eAgFnPc2d1	další
obětí	oběť	k1gFnPc2	oběť
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Palestinců	Palestinec	k1gMnPc2	Palestinec
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
sami	sám	k3xTgMnPc1	sám
Palestinci	Palestinec	k1gMnPc1	Palestinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Časová	časový	k2eAgFnSc1d1	časová
osa	osa	k1gFnSc1	osa
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ACKERMAN	ACKERMAN	kA	ACKERMAN
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
;	;	kIx,	;
DUVALL	DUVALL	kA	DUVALL
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Force	force	k1gFnSc1	force
More	mor	k1gInSc5	mor
Powerful	Powerfula	k1gFnPc2	Powerfula
<g/>
:	:	kIx,	:
A	a	k9	a
Century	Centura	k1gFnSc2	Centura
of	of	k?	of
Nonviolent	Nonviolent	k1gMnSc1	Nonviolent
Conflict	Conflict	k1gMnSc1	Conflict
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Palgrave	Palgrav	k1gInSc5	Palgrav
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
544	[number]	k4	544
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
312	[number]	k4	312
<g/>
-	-	kIx~	-
<g/>
24050	[number]	k4	24050
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ALIMI	ALIMI	kA	ALIMI
<g/>
,	,	kIx,	,
Eitan	Eitan	k1gMnSc1	Eitan
Y.	Y.	kA	Y.
Israeli	Israel	k1gInSc3	Israel
Politics	Politics	k1gInSc1	Politics
and	and	k?	and
the	the	k?	the
First	First	k1gInSc1	First
Palestinian	Palestinian	k1gMnSc1	Palestinian
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
.	.	kIx.	.
</s>
<s>
Abingdon	Abingdon	k1gNnSc1	Abingdon
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gNnSc1	Routledge
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
38560	[number]	k4	38560
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ARONSON	ARONSON	kA	ARONSON
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc7	Geoffrey
<g/>
.	.	kIx.	.
</s>
<s>
Israel	Israel	k1gInSc1	Israel
<g/>
,	,	kIx,	,
Palestinians	Palestinians	k1gInSc1	Palestinians
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
Creating	Creating	k1gInSc1	Creating
Facts	Factsa	k1gFnPc2	Factsa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
West	West	k2eAgInSc1d1	West
Bank	bank	k1gInSc1	bank
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Kegan	Kegan	k1gMnSc1	Kegan
Paul	Paul	k1gMnSc1	Paul
International	International	k1gMnSc1	International
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
376	[number]	k4	376
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7103	[number]	k4	7103
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
336	[number]	k4	336
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FINKELSTEIN	FINKELSTEIN	kA	FINKELSTEIN
<g/>
,	,	kIx,	,
Norman	Norman	k1gMnSc1	Norman
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Rise	Rise	k1gInSc1	Rise
and	and	k?	and
Fall	Fall	k1gInSc1	Fall
of	of	k?	of
Palestine	Palestin	k1gInSc5	Palestin
<g/>
:	:	kIx,	:
A	a	k9	a
Personal	Personal	k1gMnSc1	Personal
Account	Account	k1gMnSc1	Account
of	of	k?	of
the	the	k?	the
Intifada	Intifada	k1gFnSc1	Intifada
Years	Years	k1gInSc1	Years
<g/>
.	.	kIx.	.
</s>
<s>
Minneapolis	Minneapolis	k1gInSc1	Minneapolis
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Minnesota	Minnesota	k1gFnSc1	Minnesota
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
232	[number]	k4	232
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
816	[number]	k4	816
<g/>
-	-	kIx~	-
<g/>
62858	[number]	k4	62858
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HILTERMANN	HILTERMANN	kA	HILTERMANN
<g/>
,	,	kIx,	,
Joost	Joost	k1gMnSc1	Joost
R.	R.	kA	R.
Behind	Behind	k1gMnSc1	Behind
the	the	k?	the
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
Labor	Labor	k1gInSc1	Labor
and	and	k?	and
Women	Women	k1gInSc1	Women
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Movements	Movements	k1gInSc1	Movements
in	in	k?	in
the	the	k?	the
Occupied	Occupied	k1gMnSc1	Occupied
Territories	Territories	k1gMnSc1	Territories
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
<g/>
:	:	kIx,	:
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
265	[number]	k4	265
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7869	[number]	k4	7869
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KING	KING	kA	KING
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Quiet	Quiet	k1gInSc1	Quiet
Revolution	Revolution	k1gInSc1	Revolution
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
First	First	k1gMnSc1	First
Palestinian	Palestinian	k1gMnSc1	Palestinian
Intifada	Intifada	k1gFnSc1	Intifada
and	and	k?	and
Nonviolent	Nonviolent	k1gInSc4	Nonviolent
Resistance	Resistance	k1gFnSc2	Resistance
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Nation	Nation	k1gInSc1	Nation
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
488	[number]	k4	488
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
56025	[number]	k4	56025
<g/>
-	-	kIx~	-
<g/>
802	[number]	k4	802
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
LOCKMAN	LOCKMAN	kA	LOCKMAN
<g/>
,	,	kIx,	,
Zachary	Zachara	k1gFnSc2	Zachara
<g/>
;	;	kIx,	;
BEININ	BEININ	kA	BEININ
<g/>
,	,	kIx,	,
Joel	Joel	k1gInSc1	Joel
<g/>
.	.	kIx.	.
</s>
<s>
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Palestinian	Palestiniana	k1gFnPc2	Palestiniana
Uprising	Uprising	k1gInSc4	Uprising
Against	Against	k1gFnSc4	Against
Israeli	Israel	k1gInSc6	Israel
Occupation	Occupation	k1gInSc1	Occupation
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
:	:	kIx,	:
South	South	k1gInSc1	South
End	End	k1gFnSc2	End
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
423	[number]	k4	423
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
89608	[number]	k4	89608
<g/>
-	-	kIx~	-
<g/>
363	[number]	k4	363
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MCDOWALL	MCDOWALL	kA	MCDOWALL
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
Palestine	Palestin	k1gMnSc5	Palestin
and	and	k?	and
Israel	Israel	k1gInSc1	Israel
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Uprising	Uprising	k1gInSc1	Uprising
and	and	k?	and
Beyond	Beyond	k1gInSc1	Beyond
<g/>
.	.	kIx.	.
</s>
<s>
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
322	[number]	k4	322
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
52006	[number]	k4	52006
<g/>
-	-	kIx~	-
<g/>
902	[number]	k4	902
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MORRIS	MORRIS	kA	MORRIS
<g/>
,	,	kIx,	,
Benny	Benn	k1gInPc7	Benn
<g/>
.	.	kIx.	.
</s>
<s>
Righteous	Righteous	k1gInSc1	Righteous
Victims	Victims	k1gInSc1	Victims
<g/>
:	:	kIx,	:
a	a	k8xC	a
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Zionist-Arab	Zionist-Arab	k1gMnSc1	Zionist-Arab
conflict	conflict	k1gMnSc1	conflict
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
-	-	kIx~	-
<g/>
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Knopf	Knopf	k1gInSc1	Knopf
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
751	[number]	k4	751
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
679	[number]	k4	679
<g/>
-	-	kIx~	-
<g/>
74475	[number]	k4	74475
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NASSAR	NASSAR	kA	NASSAR
<g/>
,	,	kIx,	,
Jamal	Jamal	k1gMnSc1	Jamal
Raji	Raj	k1gFnSc2	Raj
<g/>
;	;	kIx,	;
HEACOCK	HEACOCK	kA	HEACOCK
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
<g/>
.	.	kIx.	.
</s>
<s>
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
Palestine	Palestin	k1gInSc5	Palestin
at	at	k?	at
the	the	k?	the
Crossroads	Crossroads	k1gInSc1	Crossroads
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Praeger	Praeger	k1gInSc1	Praeger
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
347	[number]	k4	347
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
275	[number]	k4	275
<g/>
-	-	kIx~	-
<g/>
93411	[number]	k4	93411
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
PERETZ	PERETZ	kA	PERETZ
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
<g/>
.	.	kIx.	.
</s>
<s>
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Palestinian	Palestinian	k1gMnSc1	Palestinian
Uprising	Uprising	k1gInSc1	Uprising
<g/>
.	.	kIx.	.
</s>
<s>
Boulder	Boulder	k1gInSc1	Boulder
<g/>
:	:	kIx,	:
Westview	Westview	k1gFnSc1	Westview
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
246	[number]	k4	246
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
81330	[number]	k4	81330
<g/>
-	-	kIx~	-
<g/>
860	[number]	k4	860
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RIGBY	RIGBY	kA	RIGBY
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gFnSc1	Andrew
<g/>
.	.	kIx.	.
</s>
<s>
Living	Living	k1gInSc1	Living
the	the	k?	the
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Zed	Zed	k1gFnSc1	Zed
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
233	[number]	k4	233
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
85649	[number]	k4	85649
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ROBERTS	ROBERTS	kA	ROBERTS
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
<g/>
;	;	kIx,	;
GARTON	GARTON	kA	GARTON
ASH	ASH	kA	ASH
<g/>
,	,	kIx,	,
Timothy	Timoth	k1gInPc7	Timoth
<g/>
.	.	kIx.	.
</s>
<s>
Civil	civil	k1gMnSc1	civil
Resistance	Resistance	k1gFnSc2	Resistance
and	and	k?	and
Power	Power	k1gInSc1	Power
Politics	Politics	k1gInSc1	Politics
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Experience	Experience	k1gFnSc2	Experience
of	of	k?	of
Non-violent	Noniolent	k1gInSc1	Non-violent
Action	Action	k1gInSc1	Action
from	from	k1gInSc1	from
Ghandi	Ghand	k1gMnPc1	Ghand
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Present	Present	k1gInSc1	Present
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
/	/	kIx~	/
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
407	[number]	k4	407
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
955201	[number]	k4	955201
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHAY	SHAY	kA	SHAY
<g/>
,	,	kIx,	,
Shaul	Shaul	k1gInSc1	Shaul
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Axis	Axis	k1gInSc1	Axis
of	of	k?	of
Evil	Evil	k1gInSc1	Evil
<g/>
:	:	kIx,	:
Iran	Iran	k1gMnSc1	Iran
<g/>
,	,	kIx,	,
Hizballah	Hizballah	k1gMnSc1	Hizballah
<g/>
,	,	kIx,	,
and	and	k?	and
the	the	k?	the
Palestinian	Palestinian	k1gMnSc1	Palestinian
Terror	Terror	k1gMnSc1	Terror
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Brunswick	Brunswick	k1gInSc1	Brunswick
<g/>
:	:	kIx,	:
Transaction	Transaction	k1gInSc1	Transaction
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
262	[number]	k4	262
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7658	[number]	k4	7658
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
255	[number]	k4	255
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SCHIFF	SCHIFF	kA	SCHIFF
<g/>
,	,	kIx,	,
Ze	z	k7c2	z
<g/>
'	'	kIx"	'
<g/>
ev	ev	k?	ev
<g/>
;	;	kIx,	;
YA	YA	kA	YA
<g/>
'	'	kIx"	'
<g/>
ARI	ARI	kA	ARI
<g/>
,	,	kIx,	,
Ehud	Ehud	k1gMnSc1	Ehud
<g/>
.	.	kIx.	.
</s>
<s>
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Palestinian	Palestinian	k1gMnSc1	Palestinian
Uprising	Uprising	k1gInSc1	Uprising
<g/>
:	:	kIx,	:
Israel	Israel	k1gInSc1	Israel
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Third	Third	k1gInSc1	Third
Front	front	k1gInSc1	front
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Simon	Simon	k1gMnSc1	Simon
&	&	k?	&
Schuster	Schuster	k1gMnSc1	Schuster
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
671	[number]	k4	671
<g/>
-	-	kIx~	-
<g/>
67530	[number]	k4	67530
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHALEV	SHALEV	kA	SHALEV
<g/>
,	,	kIx,	,
Aryeh	Aryeh	k1gInSc1	Aryeh
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Intifada	Intifada	k1gFnSc1	Intifada
<g/>
:	:	kIx,	:
Causes	Causes	k1gInSc1	Causes
and	and	k?	and
Effects	Effects	k1gInSc1	Effects
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
<g/>
:	:	kIx,	:
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
Post	post	k1gInSc1	post
&	&	k?	&
Westview	Westview	k1gFnSc2	Westview
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8133	[number]	k4	8133
<g/>
-	-	kIx~	-
<g/>
8303	[number]	k4	8303
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SHLAIM	SHLAIM	kA	SHLAIM
<g/>
,	,	kIx,	,
Avi	Avi	k1gFnSc1	Avi
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Iron	iron	k1gInSc1	iron
Wall	Wall	k1gInSc1	Wall
<g/>
:	:	kIx,	:
Israel	Israel	k1gInSc1	Israel
and	and	k?	and
the	the	k?	the
Arab	Arab	k1gMnSc1	Arab
World	World	k1gMnSc1	World
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Penguin	Penguin	k1gInSc1	Penguin
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
670	[number]	k4	670
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
140	[number]	k4	140
<g/>
-	-	kIx~	-
<g/>
28870	[number]	k4	28870
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
První	první	k4xOgFnPc1	první
intifáda	intifáda	k1gFnSc1	intifáda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
První	první	k4xOgFnSc1	první
intifáda	intifáda	k1gFnSc1	intifáda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
BBC	BBC	kA	BBC
–	–	k?	–
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
First	First	k1gFnSc1	First
Intifada	Intifada	k1gFnSc1	Intifada
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
MidEastWeb	MidEastWba	k1gFnPc2	MidEastWba
–	–	k?	–
Intifada	Intifada	k1gFnSc1	Intifada
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
My	my	k3xPp1nPc1	my
Jewish	Jewish	k1gInSc1	Jewish
Learning	Learning	k1gInSc1	Learning
–	–	k?	–
The	The	k1gFnSc1	The
First	First	k1gFnSc1	First
Intifada	Intifada	k1gFnSc1	Intifada
</s>
</p>
