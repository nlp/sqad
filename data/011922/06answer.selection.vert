<s>
První	první	k4xOgFnSc1	první
intifáda	intifáda	k1gFnSc1	intifáda
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ا	ا	k?	ا
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
procitnutí	procitnutí	k1gNnSc2	procitnutí
<g/>
"	"	kIx"	"
či	či	k8xC	či
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
lidové	lidový	k2eAgNnSc1d1	lidové
povstání	povstání	k1gNnSc1	povstání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
též	též	k9	též
"	"	kIx"	"
<g/>
válka	válka	k1gFnSc1	válka
kamenů	kámen	k1gInPc2	kámen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
palestinské	palestinský	k2eAgNnSc1d1	palestinské
povstání	povstání	k1gNnSc1	povstání
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
palestinskými	palestinský	k2eAgMnPc7d1	palestinský
Araby	Arab	k1gMnPc7	Arab
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1987	[number]	k4	1987
v	v	k7c6	v
Pásmu	pásmo	k1gNnSc6	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
.	.	kIx.	.
</s>
