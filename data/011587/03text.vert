<p>
<s>
Cairn	Cairn	k1gInSc1	Cairn
de	de	k?	de
Barnenez	Barnenez	k1gInSc1	Barnenez
je	být	k5eAaImIp3nS	být
kamenná	kamenný	k2eAgFnSc1d1	kamenná
neolitická	neolitický	k2eAgFnSc1d1	neolitická
mohyla	mohyla	k1gFnSc1	mohyla
nedaleko	nedaleko	k7c2	nedaleko
obce	obec	k1gFnSc2	obec
Plouezoc	Plouezoc	k1gInSc1	Plouezoc
<g/>
'	'	kIx"	'
<g/>
h	h	k?	h
v	v	k7c6	v
Bretani	Bretaň	k1gFnSc6	Bretaň
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
4800	[number]	k4	4800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tak	tak	k9	tak
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
budov	budova	k1gFnPc2	budova
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
snad	snad	k9	snad
dokonce	dokonce	k9	dokonce
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgMnSc1d3	nejstarší
<g/>
.	.	kIx.	.
</s>
<s>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
72	[number]	k4	72
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
až	až	k9	až
25	[number]	k4	25
m	m	kA	m
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
vysoký	vysoký	k2eAgMnSc1d1	vysoký
přes	přes	k7c4	přes
8	[number]	k4	8
m.	m.	k?	m.
Byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
etapách	etapa	k1gFnPc6	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
část	část	k1gFnSc1	část
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
5	[number]	k4	5
pohřebních	pohřební	k2eAgFnPc2d1	pohřební
komor	komora	k1gFnPc2	komora
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc4d2	mladší
část	část	k1gFnSc4	část
6	[number]	k4	6
komor	komora	k1gFnPc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Mohyla	mohyla	k1gFnSc1	mohyla
je	být	k5eAaImIp3nS	být
zčásti	zčásti	k6eAd1	zčásti
poškozena	poškodit	k5eAaPmNgFnS	poškodit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
odebírán	odebírán	k2eAgInSc1d1	odebírán
kámen	kámen	k1gInSc1	kámen
pro	pro	k7c4	pro
stavební	stavební	k2eAgInPc4d1	stavební
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
vcelku	vcelku	k6eAd1	vcelku
je	být	k5eAaImIp3nS	být
však	však	k9	však
dobře	dobře	k6eAd1	dobře
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
