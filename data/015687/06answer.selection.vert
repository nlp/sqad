<s>
Hlavním	hlavní	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
válek	válka	k1gFnPc2
potvrzeným	potvrzený	k2eAgFnPc3d1
v	v	k7c6
Cášském	cášský	k2eAgInSc6d1
míru	mír	k1gInSc6
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Habsburskou	habsburský	k2eAgFnSc4d1
monarchii	monarchie	k1gFnSc4
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
uznání	uznání	k1gNnSc1
nástupnictví	nástupnictví	k1gNnSc2
Marie	Marie	k1gFnSc2
Terezie	Terezie	k1gFnSc2
za	za	k7c4
cenu	cena	k1gFnSc4
ztráty	ztráta	k1gFnSc2
většiny	většina	k1gFnSc2
Slezska	Slezsko	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>