<s>
Války	válek	k1gInPc1
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
je	být	k5eAaImIp3nS
souhrnné	souhrnný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
války	válka	k1gFnPc4
z	z	k7c2
let	léto	k1gNnPc2
1740	#num#	k4
až	až	k6eAd1
1748	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vypukly	vypuknout	k5eAaPmAgInP
po	po	k7c6
smrti	smrt	k1gFnSc6
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
když	když	k8xS
Bavorsko	Bavorsko	k1gNnSc1
a	a	k8xC
Sasko	Sasko	k1gNnSc1
odmítly	odmítnout	k5eAaPmAgInP
uznat	uznat	k5eAaPmF
Pragmatickou	pragmatický	k2eAgFnSc4d1
sankci	sankce	k1gFnSc4
a	a	k8xC
nástupnictví	nástupnictví	k1gNnSc4
Marie	Marie	k1gFnSc2
Terezie	Terezie	k1gFnSc2
v	v	k7c6
Habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
a	a	k8xC
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
vznesl	vznést	k5eAaPmAgMnS
územní	územní	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
Slezsko	Slezsko	k1gNnSc4
<g/>
.	.	kIx.
</s>