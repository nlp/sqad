<s>
Války	válek	k1gInPc1
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Slezské	slezský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Slezské	slezský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
dědictví	dědictví	k1gNnSc4
rakouské	rakouský	k2eAgFnSc2d1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Hohenfriedbergu	Hohenfriedberg	k1gInSc2
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1740	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1748	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Střední	střední	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
;	;	kIx,
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
;	;	kIx,
Indie	Indie	k1gFnSc1
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Cášský	cášský	k2eAgInSc1d1
mír	mír	k1gInSc1
(	(	kIx(
<g/>
1748	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Francie	Francie	k1gFnSc1
Prusko	Prusko	k1gNnSc4
(	(	kIx(
<g/>
1740	#num#	k4
<g/>
–	–	k?
<g/>
1742	#num#	k4
<g/>
,	,	kIx,
1744	#num#	k4
<g/>
–	–	k?
<g/>
1745	#num#	k4
<g/>
)	)	kIx)
Španělsko	Španělsko	k1gNnSc1
Bavorsko	Bavorsko	k1gNnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1745	#num#	k4
<g/>
)	)	kIx)
Sasko	Sasko	k1gNnSc4
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1742	#num#	k4
<g/>
)	)	kIx)
Savojsko-Sardinie	Savojsko-Sardinie	k1gFnSc1
Neapolsko	Neapolsko	k1gNnSc1
Janovská	Janovská	k1gFnSc1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1745	#num#	k4
<g/>
–	–	k?
<g/>
1748	#num#	k4
<g/>
)	)	kIx)
Švédsko	Švédsko	k1gNnSc1
Hesensko-Kasselsko	Hesensko-Kasselsko	k1gNnSc1
Falcké	falcký	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc1
Kolínské	kolínský	k2eAgNnSc1d1
kurfiřtství	kurfiřtství	k1gNnSc3
jakobité	jakobita	k1gMnPc1
(	(	kIx(
<g/>
1745	#num#	k4
<g/>
–	–	k?
<g/>
1746	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
Hannoversko	Hannoversko	k1gNnSc1
Nizozemsko	Nizozemsko	k1gNnSc1
Sasko	Sasko	k1gNnSc1
(	(	kIx(
<g/>
1743	#num#	k4
<g/>
–	–	k?
<g/>
1745	#num#	k4
<g/>
)	)	kIx)
Savojsko-Sardinie	Savojsko-Sardinie	k1gFnSc2
(	(	kIx(
<g/>
1742	#num#	k4
<g/>
–	–	k?
<g/>
1748	#num#	k4
<g/>
)	)	kIx)
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
,	,	kIx,
1748	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
XV	XV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mořic	Mořic	k1gMnSc1
Saský	saský	k2eAgMnSc1d1
François-Marie	François-Marie	k1gFnPc4
de	de	k?
Broglie	Broglie	k1gFnSc2
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Filip	Filip	k1gMnSc1
V.	V.	kA
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgMnSc1d1
Charles	Charles	k1gMnSc1
Emil	Emil	k1gMnSc1
Lewenhaupt	Lewenhaupt	k1gMnSc1
Lorenzo	Lorenza	k1gFnSc5
de	de	k?
Mari	Mar	k1gFnSc6
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
Ludwig	Ludwig	k1gMnSc1
Khevenhüller	Khevenhüller	k1gMnSc1
Karel	Karel	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
Otto	Otto	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
vonAbensberg-Traun	vonAbensberg-Traun	k1gMnSc1
Jiří	Jiří	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
August	August	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alžběta	Alžběta	k1gFnSc1
Petrovna	Petrovna	k1gFnSc1
Peter	Petra	k1gFnPc2
Lacy	Laca	k1gFnSc2
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
23	#num#	k4
100	#num#	k4
Prusů	Prus	k1gMnPc2
<g/>
158	#num#	k4
000	#num#	k4
Francouzů	Francouz	k1gMnPc2
<g/>
3	#num#	k4
000	#num#	k4
Španělů	Španěl	k1gMnPc2
</s>
<s>
120	#num#	k4
000	#num#	k4
Rakušanů	Rakušan	k1gMnPc2
<g/>
26	#num#	k4
400	#num#	k4
Britů	Brit	k1gMnPc2
<g/>
14	#num#	k4
630	#num#	k4
Nizozemců	Nizozemec	k1gMnPc2
<g/>
7	#num#	k4
840	#num#	k4
Sardiňanů	Sardiňan	k1gMnPc2
</s>
<s>
Války	válek	k1gInPc1
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
je	být	k5eAaImIp3nS
souhrnné	souhrnný	k2eAgNnSc1d1
označení	označení	k1gNnSc1
pro	pro	k7c4
války	válka	k1gFnPc4
z	z	k7c2
let	léto	k1gNnPc2
1740	#num#	k4
až	až	k6eAd1
1748	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
vypukly	vypuknout	k5eAaPmAgInP
po	po	k7c6
smrti	smrt	k1gFnSc6
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Karla	Karel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
když	když	k8xS
Bavorsko	Bavorsko	k1gNnSc1
a	a	k8xC
Sasko	Sasko	k1gNnSc1
odmítly	odmítnout	k5eAaPmAgInP
uznat	uznat	k5eAaPmF
Pragmatickou	pragmatický	k2eAgFnSc4d1
sankci	sankce	k1gFnSc4
a	a	k8xC
nástupnictví	nástupnictví	k1gNnSc4
Marie	Marie	k1gFnSc2
Terezie	Terezie	k1gFnSc2
v	v	k7c6
Habsburské	habsburský	k2eAgFnSc6d1
monarchii	monarchie	k1gFnSc6
a	a	k8xC
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
vznesl	vznést	k5eAaPmAgMnS
územní	územní	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
Slezsko	Slezsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
bylo	být	k5eAaImAgNnS
předmětem	předmět	k1gInSc7
sporů	spor	k1gInPc2
také	také	k9
nástupnictví	nástupnictví	k1gNnSc4
na	na	k7c6
císařském	císařský	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
<g/>
,	,	kIx,
o	o	k7c4
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
usilovali	usilovat	k5eAaImAgMnP
bavorský	bavorský	k2eAgMnSc1d1
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
a	a	k8xC
manžel	manžel	k1gMnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
František	František	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válka	k1gFnPc1
se	se	k3xPyFc4
rozrostly	rozrůst	k5eAaPmAgFnP
v	v	k7c4
mnohostranný	mnohostranný	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
na	na	k7c6
jedné	jeden	k4xCgFnSc6
či	či	k8xC
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
zapojily	zapojit	k5eAaPmAgFnP
další	další	k2eAgFnPc4d1
evropské	evropský	k2eAgFnPc4d1
mocnosti	mocnost	k1gFnPc4
–	–	k?
především	především	k9
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
a	a	k8xC
Španělsko	Španělsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivým	jednotlivý	k2eAgNnSc7d1
zemím	zem	k1gFnPc3
šlo	jít	k5eAaImAgNnS
nejen	nejen	k6eAd1
o	o	k7c4
územní	územní	k2eAgInPc4d1
zisky	zisk	k1gInPc4
(	(	kIx(
<g/>
na	na	k7c4
úkor	úkor	k1gInSc4
Habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
o	o	k7c4
mocenské	mocenský	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boje	boj	k1gInSc2
o	o	k7c4
Slezsko	Slezsko	k1gNnSc4
bývají	bývat	k5eAaImIp3nP
také	také	k9
nazývány	nazývat	k5eAaImNgFnP
jako	jako	k8xC,k8xS
slezské	slezský	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
výsledkem	výsledek	k1gInSc7
válek	válka	k1gFnPc2
potvrzeným	potvrzený	k2eAgFnPc3d1
v	v	k7c6
Cášském	cášský	k2eAgInSc6d1
míru	mír	k1gInSc6
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Habsburskou	habsburský	k2eAgFnSc4d1
monarchii	monarchie	k1gFnSc4
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
uznání	uznání	k1gNnSc1
nástupnictví	nástupnictví	k1gNnSc2
Marie	Marie	k1gFnSc2
Terezie	Terezie	k1gFnSc2
za	za	k7c4
cenu	cena	k1gFnSc4
ztráty	ztráta	k1gFnSc2
většiny	většina	k1gFnSc2
Slezska	Slezsko	k1gNnSc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
stejném	stejný	k2eAgNnSc6d1
období	období	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
dalším	další	k2eAgInPc3d1
dvoustranným	dvoustranný	k2eAgInPc3d1
konfliktům	konflikt	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
bývají	bývat	k5eAaImIp3nP
pojímány	pojímán	k2eAgInPc1d1
jako	jako	k8xC,k8xS
samostatné	samostatný	k2eAgFnSc2d1
války	válka	k1gFnSc2
–	–	k?
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
koloniální	koloniální	k2eAgFnPc4d1
války	válka	k1gFnPc4
Španělska	Španělsko	k1gNnSc2
s	s	k7c7
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
(	(	kIx(
<g/>
válka	válka	k1gFnSc1
o	o	k7c4
Jenkinsovo	Jenkinsův	k2eAgNnSc4d1
ucho	ucho	k1gNnSc4
1739	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
s	s	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
dále	daleko	k6eAd2
o	o	k7c4
švédsko	švédsko	k6eAd1
<g/>
–	–	k?
<g/>
ruskou	ruský	k2eAgFnSc4d1
válku	válka	k1gFnSc4
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
kontextu	kontext	k1gInSc2
těchto	tento	k3xDgInPc2
bojů	boj	k1gInPc2
bývá	bývat	k5eAaImIp3nS
řazeno	řazen	k2eAgNnSc1d1
i	i	k8xC
jakobitské	jakobitský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Pozadí	pozadí	k1gNnSc1
a	a	k8xC
příčiny	příčina	k1gFnPc1
</s>
<s>
Mapa	mapa	k1gFnSc1
Evropy	Evropa	k1gFnSc2
před	před	k7c7
rokem	rok	k1gInSc7
1740	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
Slezsko	Slezsko	k1gNnSc4
jednou	jednou	k6eAd1
ze	z	k7c2
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
České	český	k2eAgFnSc2d1
</s>
<s>
Během	během	k7c2
války	válka	k1gFnSc2
o	o	k7c4
španělské	španělský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
uzavřel	uzavřít	k5eAaPmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Leopold	Leopold	k1gMnSc1
I.	I.	kA
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
syny	syn	k1gMnPc7
dohodu	dohoda	k1gFnSc4
o	o	k7c4
nástupnictví	nástupnictví	k1gNnSc4
označovanou	označovaný	k2eAgFnSc4d1
tradičně	tradičně	k6eAd1
jako	jako	k8xC,k8xS
Pactum	Pactum	k1gNnSc1
mutuae	mutua	k1gInSc2
succesionis	succesionis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládal	předpokládat	k5eAaImAgMnS
totiž	totiž	k9
<g/>
,	,	kIx,
že	že	k8xS
založí	založit	k5eAaPmIp3nP
nové	nový	k2eAgFnPc1d1
habsburské	habsburský	k2eAgFnPc1d1
rodové	rodový	k2eAgFnPc1d1
větve	větev	k1gFnPc1
<g/>
,	,	kIx,
starší	starý	k2eAgMnSc1d2
Josef	Josef	k1gMnSc1
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
mladší	mladý	k2eAgMnSc1d2
Karel	Karel	k1gMnSc1
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
vzájemného	vzájemný	k2eAgNnSc2d1
následnictví	následnictví	k1gNnSc2
v	v	k7c6
případě	případ	k1gInSc6
vymření	vymření	k1gNnSc2
mužských	mužský	k2eAgMnPc2d1
členů	člen	k1gMnPc2
rodiny	rodina	k1gFnSc2
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
zmíněno	zmíněn	k2eAgNnSc4d1
nástupnické	nástupnický	k2eAgNnSc4d1
právo	právo	k1gNnSc4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Předčasná	předčasný	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
Leopoldova	Leopoldův	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
roku	rok	k1gInSc2
1711	#num#	k4
způsobila	způsobit	k5eAaPmAgFnS
nejen	nejen	k6eAd1
obrat	obrat	k1gInSc4
ve	v	k7c6
válce	válka	k1gFnSc6
o	o	k7c4
dědictví	dědictví	k1gNnSc4
španělské	španělský	k2eAgFnSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
nutila	nutit	k5eAaImAgFnS
posledního	poslední	k2eAgMnSc4d1
mužského	mužský	k2eAgMnSc4d1
příslušníka	příslušník	k1gMnSc4
rodu	rod	k1gInSc2
rakouských	rakouský	k2eAgMnPc2d1
Habsburků	Habsburk	k1gMnPc2
připravit	připravit	k5eAaPmF
řešení	řešení	k1gNnSc4
pro	pro	k7c4
situaci	situace	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
zemřel	zemřít	k5eAaPmAgMnS
také	také	k9
bez	bez	k7c2
dědice	dědic	k1gMnSc2
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1713	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
Pragmatická	pragmatický	k2eAgFnSc1d1
sankce	sankce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Karlem	Karel	k1gMnSc7
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburkové	Habsburk	k1gMnPc1
vymřeli	vymřít	k5eAaPmAgMnP
<g/>
,	,	kIx,
stanovovala	stanovovat	k5eAaImAgFnS
následující	následující	k2eAgNnSc4d1
pořadí	pořadí	k1gNnSc4
dědiců	dědic	k1gMnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
dcery	dcera	k1gFnSc2
Karla	Karel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
dcery	dcera	k1gFnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
dcery	dcera	k1gFnSc2
a	a	k8xC
poté	poté	k6eAd1
sestry	sestra	k1gFnPc1
Leopolda	Leopold	k1gMnSc2
I.	I.	kA
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
potomstvo	potomstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
se	se	k3xPyFc4
císař	císař	k1gMnSc1
zasazoval	zasazovat	k5eAaImAgMnS
o	o	k7c4
přijetí	přijetí	k1gNnSc4
toho	ten	k3xDgInSc2
dokumentu	dokument	k1gInSc2
jak	jak	k6eAd1
sněmy	sněm	k1gInPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
sousedními	sousední	k2eAgMnPc7d1
panovníky	panovník	k1gMnPc7
a	a	k8xC
možnými	možný	k2eAgMnPc7d1
nápadníky	nápadník	k1gMnPc7
trůnu	trůn	k1gInSc2
<g/>
,	,	kIx,
vzápětí	vzápětí	k6eAd1
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1740	#num#	k4
byly	být	k5eAaImAgInP
nároky	nárok	k1gInPc1
jeho	jeho	k3xOp3gFnSc2
dcery	dcera	k1gFnSc2
a	a	k8xC
nové	nový	k2eAgFnSc2d1
panovnice	panovnice	k1gFnSc2
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
zpochybněny	zpochybnit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
bylo	být	k5eAaImAgNnS
udržení	udržení	k1gNnSc1
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
ve	v	k7c6
zděděné	zděděný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
,	,	kIx,
její	její	k3xOp3gNnPc1
nástupnická	nástupnický	k2eAgNnPc1d1
práva	právo	k1gNnPc1
však	však	k9
napadli	napadnout	k5eAaPmAgMnP
kurfiřti	kurfiřt	k1gMnPc1
bavorský	bavorský	k2eAgMnSc1d1
<g/>
,	,	kIx,
saský	saský	k2eAgMnSc1d1
a	a	k8xC
braniborský	braniborský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bavorský	bavorský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
a	a	k8xC
manžel	manžel	k1gMnSc1
dcery	dcera	k1gFnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
císařskou	císařský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
a	a	k8xC
habsburské	habsburský	k2eAgFnPc4d1
dědičné	dědičný	k2eAgFnPc4d1
země	zem	k1gFnPc4
na	na	k7c6
základě	základ	k1gInSc6
středověkého	středověký	k2eAgNnSc2d1
Privilegia	privilegium	k1gNnSc2
minus	minus	k1gInSc1
<g/>
,	,	kIx,
saský	saský	k2eAgMnSc1d1
kurfiřt	kurfiřt	k1gMnSc1
a	a	k8xC
polský	polský	k2eAgMnSc1d1
král	král	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
August	August	k1gMnSc1
své	svůj	k3xOyFgInPc4
nároky	nárok	k1gInPc4
odůvodňoval	odůvodňovat	k5eAaImAgMnS
sňatkem	sňatek	k1gInSc7
s	s	k7c7
jednou	jeden	k4xCgFnSc7
z	z	k7c2
dcer	dcera	k1gFnPc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
Braniborský	braniborský	k2eAgMnSc1d1
kurfiřt	kurfiřt	k1gMnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
vznesl	vznést	k5eAaPmAgMnS
na	na	k7c6
základě	základ	k1gInSc6
polozapomenutých	polozapomenutý	k2eAgFnPc2d1
smluv	smlouva	k1gFnPc2
z	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
dědické	dědický	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
některá	některý	k3yIgNnPc4
slezská	slezský	k2eAgNnPc4d1
knížectví	knížectví	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
slezská	slezský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
před	před	k7c7
rokem	rok	k1gInSc7
1740	#num#	k4
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
patřilo	patřit	k5eAaImAgNnS
Habsburkům	Habsburk	k1gMnPc3
po	po	k7c4
vymření	vymření	k1gNnSc4
Piastovského	Piastovský	k2eAgInSc2d1
rodu	rod	k1gInSc2
díky	díky	k7c3
sankcím	sankce	k1gFnPc3
z	z	k7c2
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1713	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
vytvořil	vytvořit	k5eAaPmAgMnS
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
císařovy	císařův	k2eAgFnSc2d1
doby	doba	k1gFnSc2
byla	být	k5eAaImAgFnS
existence	existence	k1gFnSc1
sankce	sankce	k1gFnSc1
obecně	obecně	k6eAd1
potvrzena	potvrdit	k5eAaPmNgFnS
německými	německý	k2eAgInPc7d1
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
následujícím	následující	k2eAgNnSc7d1
úmrtím	úmrtí	k1gNnSc7
císaře	císař	k1gMnSc2
si	se	k3xPyFc3
však	však	k9
Prusko	Prusko	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
klást	klást	k5eAaImF
na	na	k7c4
Slezsko	Slezsko	k1gNnSc4
nárok	nárok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
král	král	k1gMnSc1
Pruska	Prusko	k1gNnSc2
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
požadavky	požadavek	k1gInPc4
nad	nad	k7c7
tvrzením	tvrzení	k1gNnSc7
nesplnění	nesplnění	k1gNnSc2
Schwiebusové	Schwiebusový	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
slezské	slezský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
Lehnice	Lehnice	k1gFnSc1
<g/>
,	,	kIx,
Volovsko	Volovsko	k1gNnSc1
a	a	k8xC
Břeh	břeh	k1gInSc1
budou	být	k5eAaImBp3nP
odevzdané	odevzdaný	k2eAgFnPc1d1
Braniborsku	Braniborsko	k1gNnSc6
po	po	k7c6
vymření	vymření	k1gNnSc6
Piastovské	Piastovský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1675	#num#	k4
s	s	k7c7
úmrtím	úmrtí	k1gNnSc7
posledního	poslední	k2eAgMnSc2d1
Piastovce	Piastovec	k1gMnSc2
vymřel	vymřít	k5eAaPmAgInS
rod	rod	k1gInSc4
Piastovců	Piastovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
nebyl	být	k5eNaImAgMnS
žádný	žádný	k3yNgInSc4
pokus	pokus	k1gInSc4
o	o	k7c6
realizaci	realizace	k1gFnSc6
této	tento	k3xDgFnSc2
staré	starý	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
a	a	k8xC
pruský	pruský	k2eAgMnSc1d1
vladař	vladař	k1gMnSc1
byl	být	k5eAaImAgMnS
za	za	k7c4
platbu	platba	k1gFnSc4
ochoten	ochoten	k2eAgMnSc1d1
se	se	k3xPyFc4
smlouvy	smlouva	k1gFnSc2
zříci	zříct	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
postoupení	postoupení	k1gNnSc2
Slezska	Slezsko	k1gNnSc2
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
nabízel	nabízet	k5eAaImAgInS
uznání	uznání	k1gNnSc4
Pragmatické	pragmatický	k2eAgFnSc2d1
sankce	sankce	k1gFnSc2
<g/>
,	,	kIx,
finanční	finanční	k2eAgNnSc4d1
odškodné	odškodné	k1gNnSc4
i	i	k8xC
podporu	podpora	k1gFnSc4
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
proti	proti	k7c3
útokům	útok	k1gInPc3
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezitím	mezitím	k6eAd1
však	však	k9
již	již	k6eAd1
nařídil	nařídit	k5eAaPmAgMnS
mobilizaci	mobilizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojenské	vojenský	k2eAgFnSc2d1
operace	operace	k1gFnSc2
zahájil	zahájit	k5eAaPmAgInS
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1740	#num#	k4
vpádem	vpád	k1gInSc7
do	do	k7c2
Slezska	Slezsko	k1gNnSc2
bez	bez	k7c2
vyhlášení	vyhlášení	k1gNnSc2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
byly	být	k5eAaImAgInP
pruské	pruský	k2eAgInPc1d1
návrhy	návrh	k1gInPc1
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
odmítnuty	odmítnut	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
byl	být	k5eAaImAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
poměrně	poměrně	k6eAd1
malý	malý	k2eAgInSc1d1
počet	počet	k1gInSc1
rakouských	rakouský	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
ho	on	k3xPp3gMnSc4
Prusové	Prus	k1gMnPc1
během	během	k7c2
šesti	šest	k4xCc2
týdnů	týden	k1gInPc2
téměř	téměř	k6eAd1
celé	celá	k1gFnPc4
ovládli	ovládnout	k5eAaPmAgMnP
<g/>
,	,	kIx,
kromě	kromě	k7c2
několika	několik	k4yIc2
pevností	pevnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pruském	pruský	k2eAgInSc6d1
útoku	útok	k1gInSc6
požádala	požádat	k5eAaPmAgFnS
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
o	o	k7c4
pomoc	pomoc	k1gFnSc4
britského	britský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
rozpad	rozpad	k1gInSc1
podunajského	podunajský	k2eAgNnSc2d1
soustátí	soustátí	k1gNnSc2
a	a	k8xC
narušení	narušení	k1gNnSc2
mocenské	mocenský	k2eAgFnSc2d1
rovnováhy	rovnováha	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
nebyly	být	k5eNaImAgFnP
ani	ani	k8xC
v	v	k7c6
zájmu	zájem	k1gInSc6
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
v	v	k7c6
únoru	únor	k1gInSc6
1741	#num#	k4
na	na	k7c4
setkání	setkání	k1gNnSc4
diplomatů	diplomat	k1gMnPc2
v	v	k7c6
Drážďanech	Drážďany	k1gInPc6
uzavřena	uzavřen	k2eAgFnSc1d1
tajná	tajný	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Rakouska	Rakousko	k1gNnSc2
<g/>
,	,	kIx,
Spojených	spojený	k2eAgFnPc2d1
provincií	provincie	k1gFnPc2
<g/>
,	,	kIx,
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
Hannoverska	Hannoversko	k1gNnSc2
a	a	k8xC
Saska	Sasko	k1gNnSc2
na	na	k7c4
obranu	obrana	k1gFnSc4
nedělitelnosti	nedělitelnost	k1gFnSc2
habsburské	habsburský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	K	kA
přislíbené	přislíbený	k2eAgFnSc2d1
vojenské	vojenský	k2eAgFnSc2d1
a	a	k8xC
finanční	finanční	k2eAgFnSc2d1
pomoci	pomoc	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
nikdo	nikdo	k3yNnSc1
neměl	mít	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Armáda	armáda	k1gFnSc1
Friedricha	Friedrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
nejmoderněji	moderně	k6eAd3
vybavenou	vybavený	k2eAgFnSc7d1
a	a	k8xC
vedenou	vedený	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
rakouské	rakouský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
byly	být	k5eAaImAgInP
oslabeny	oslabit	k5eAaPmNgInP
nedávno	nedávno	k6eAd1
skončenými	skončený	k2eAgFnPc7d1
válkami	válka	k1gFnPc7
s	s	k7c7
Osmany	Osman	k1gMnPc7
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
organizace	organizace	k1gFnSc2
nebyla	být	k5eNaImAgFnS
valná	valný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narychlo	narychlo	k6eAd1
shromážděná	shromážděný	k2eAgFnSc1d1
rakouská	rakouský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
podmaršála	podmaršál	k1gMnSc2
Neipperga	Neipperg	k1gMnSc2
podlehla	podlehnout	k5eAaPmAgFnS
Prusům	Prus	k1gMnPc3
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Mollwitz	Mollwitza	k1gFnPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1741	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
ovšem	ovšem	k9
také	také	k9
potřeboval	potřebovat	k5eAaImAgMnS
spojence	spojenec	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
si	se	k3xPyFc3
Slezsko	Slezsko	k1gNnSc4
udržel	udržet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připojil	připojit	k5eAaPmAgMnS
se	se	k3xPyFc4
proto	proto	k8xC
k	k	k7c3
mezitím	mezitím	k6eAd1
vzniklé	vzniklý	k2eAgFnSc3d1
francouzsko-bavorské	francouzsko-bavorský	k2eAgFnSc3d1
koalici	koalice	k1gFnSc3
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
patřilo	patřit	k5eAaImAgNnS
dále	daleko	k6eAd2
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
Neapolské	neapolský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Janovská	janovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
a	a	k8xC
později	pozdě	k6eAd2
k	k	k7c3
ní	on	k3xPp3gFnSc3
přešlo	přejít	k5eAaPmAgNnS
i	i	k9
Sasko	Sasko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
podzimu	podzim	k1gInSc2
se	se	k3xPyFc4
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
pod	pod	k7c7
nátlakem	nátlak	k1gInSc7
okolí	okolí	k1gNnSc2
a	a	k8xC
vlivem	vliv	k1gInSc7
zhoršené	zhoršený	k2eAgFnSc2d1
válečné	válečný	k2eAgFnSc2d1
situace	situace	k1gFnSc2
odhodlala	odhodlat	k5eAaPmAgFnS
k	k	k7c3
jednání	jednání	k1gNnSc3
s	s	k7c7
Pruskem	Prusko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tajné	tajný	k2eAgFnSc6d1
schůzce	schůzka	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1741	#num#	k4
v	v	k7c6
Klein-Schellendorfu	Klein-Schellendorf	k1gInSc6
podepsal	podepsat	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Neipperg	Neipperg	k1gInSc4
příměří	příměří	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
se	se	k3xPyFc4
Rakousko	Rakousko	k1gNnSc1
vzdalo	vzdát	k5eAaPmAgNnS
Dolního	dolní	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
se	se	k3xPyFc4
stáhla	stáhnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
pruská	pruský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
poté	poté	k6eAd1
přezimovala	přezimovat	k5eAaBmAgNnP
ve	v	k7c6
východních	východní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Začátkem	začátkem	k7c2
roku	rok	k1gInSc2
1742	#num#	k4
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
příměří	příměří	k1gNnSc1
porušil	porušit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc1
vojska	vojsko	k1gNnPc1
vpadla	vpadnout	k5eAaPmAgNnP
na	na	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
obsadila	obsadit	k5eAaPmAgFnS
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
táhl	táhnout	k5eAaImAgInS
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Chotusic	Chotusice	k1gInPc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1742	#num#	k4
<g/>
)	)	kIx)
střetl	střetnout	k5eAaPmAgInS
s	s	k7c7
rakouskou	rakouský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
vedenou	vedený	k2eAgFnSc4d1
švagrem	švagr	k1gMnSc7
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
Karlem	Karel	k1gMnSc7
Lotrinským	lotrinský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pruské	pruský	k2eAgFnPc4d1
vítězství	vítězství	k1gNnSc3
donutilo	donutit	k5eAaPmAgNnS
Marii	Maria	k1gFnSc4
Terezii	Terezie	k1gFnSc3
uzavřít	uzavřít	k5eAaPmF
příměří	příměří	k1gNnSc4
ve	v	k7c6
Vratislavi	Vratislav	k1gFnSc6
a	a	k8xC
berlínskou	berlínský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
z	z	k7c2
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1742	#num#	k4
přistoupit	přistoupit	k5eAaPmF
na	na	k7c4
nevýhodný	výhodný	k2eNgInSc4d1
mír	mír	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
ztratila	ztratit	k5eAaPmAgFnS
Kladsko	Kladsko	k1gNnSc4
a	a	k8xC
téměř	téměř	k6eAd1
celé	celý	k2eAgNnSc4d1
Slezsko	Slezsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nutně	nutně	k6eAd1
však	však	k9
potřebovala	potřebovat	k5eAaImAgFnS
uvolnění	uvolnění	k1gNnSc4
svých	svůj	k3xOyFgFnPc2
armád	armáda	k1gFnPc2
pro	pro	k7c4
boje	boj	k1gInPc4
se	s	k7c7
zbylými	zbylý	k2eAgMnPc7d1
členy	člen	k1gMnPc7
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgFnSc2d1
situace	situace	k1gFnSc2
využilo	využít	k5eAaPmAgNnS
násilně	násilně	k6eAd1
rekatolizované	rekatolizovaný	k2eAgNnSc1d1
české	český	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
k	k	k7c3
hromadné	hromadný	k2eAgFnSc3d1
emigraci	emigrace	k1gFnSc3
do	do	k7c2
Münsterbergu	Münsterberg	k1gInSc2
pod	pod	k7c7
ochranou	ochrana	k1gFnSc7
červeného	červený	k2eAgInSc2d1
šátku	šátek	k1gInSc2
pruské	pruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
Slezska	Slezsko	k1gNnSc2
(	(	kIx(
<g/>
Opavsko	Opavsko	k1gNnSc1
avšak	avšak	k8xC
bez	bez	k7c2
Hlučínska	Hlučínsko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
připadlo	připadnout	k5eAaPmAgNnS
Prusku	Prusko	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
Těšínsko	Těšínsko	k1gNnSc1
<g/>
)	)	kIx)
zůstalo	zůstat	k5eAaPmAgNnS
pod	pod	k7c7
habsburskou	habsburský	k2eAgFnSc7d1
kontrolou	kontrola	k1gFnSc7
a	a	k8xC
bylo	být	k5eAaImAgNnS
nazýváno	nazývat	k5eAaImNgNnS
české	český	k2eAgNnSc1d1
Slezsko	Slezsko	k1gNnSc1
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
rakouské	rakouský	k2eAgNnSc1d1
Slezsko	Slezsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malé	Malé	k2eAgFnSc6d1
části	část	k1gFnSc6
polského	polský	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
(	(	kIx(
<g/>
Osvětimsko	Osvětimsko	k1gNnSc1
<g/>
,	,	kIx,
Zatorsko	Zatorsko	k1gNnSc1
<g/>
,	,	kIx,
Zywiec	Zywiec	k1gInSc1
<g/>
,	,	kIx,
Seveřsko	Seveřsko	k1gNnSc1
<g/>
)	)	kIx)
nebyly	být	k5eNaImAgFnP
spojeny	spojit	k5eAaPmNgInP
s	s	k7c7
touto	tento	k3xDgFnSc7
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Bavorsko-rakouská	bavorsko-rakouský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Francouzi	Francouz	k1gMnPc1
a	a	k8xC
Bavoři	Bavor	k1gMnPc1
útočí	útočit	k5eAaImIp3nP
na	na	k7c4
Prahu	Praha	k1gFnSc4
roku	rok	k1gInSc2
1741	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
měla	mít	k5eAaImAgFnS
zájem	zájem	k1gInSc4
na	na	k7c4
oslabení	oslabení	k1gNnSc4
rakouské	rakouský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
,	,	kIx,
podporovala	podporovat	k5eAaImAgFnS
nároky	nárok	k1gInPc1
bavorského	bavorský	k2eAgMnSc2d1
kurfiřta	kurfiřt	k1gMnSc2
Karla	Karel	k1gMnSc2
Albrechta	Albrecht	k1gMnSc2
na	na	k7c4
českou	český	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
i	i	k8xC
rakouské	rakouský	k2eAgFnPc4d1
země	zem	k1gFnPc4
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1741	#num#	k4
uzavřely	uzavřít	k5eAaPmAgFnP
Francie	Francie	k1gFnPc1
<g/>
,	,	kIx,
Bavorsko	Bavorsko	k1gNnSc1
a	a	k8xC
Sasko	Sasko	k1gNnSc1
v	v	k7c6
bavorském	bavorský	k2eAgInSc6d1
Nymphenburgu	Nymphenburg	k1gInSc6
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
základě	základ	k1gInSc6
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
přerozděleno	přerozdělen	k2eAgNnSc1d1
rakouské	rakouský	k2eAgNnSc1d1
dědictví	dědictví	k1gNnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Prusko	Prusko	k1gNnSc1
se	se	k3xPyFc4
připojilo	připojit	k5eAaPmAgNnS
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Čechy	Čechy	k1gFnPc1
<g/>
,	,	kIx,
Tyrolsko	Tyrolsko	k1gNnSc1
a	a	k8xC
Horní	horní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc1
měly	mít	k5eAaImAgInP
připadnout	připadnout	k5eAaPmF
Bavorsku	Bavorsko	k1gNnSc3
<g/>
,	,	kIx,
část	část	k1gFnSc1
Slezska	Slezsko	k1gNnSc2
a	a	k8xC
Morava	Morava	k1gFnSc1
Sasku	Saska	k1gFnSc4
<g/>
,	,	kIx,
Rakouské	rakouský	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
Francii	Francie	k1gFnSc3
a	a	k8xC
většina	většina	k1gFnSc1
Slezska	Slezsko	k1gNnSc2
Prusku	Prusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojená	spojený	k2eAgNnPc1d1
bavorsko-francouzská	bavorsko-francouzský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
vpadla	vpadnout	k5eAaPmAgNnP
v	v	k7c6
září	září	k1gNnSc6
1741	#num#	k4
do	do	k7c2
Horních	horní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
a	a	k8xC
posléze	posléze	k6eAd1
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
byla	být	k5eAaImAgFnS
obsazena	obsazen	k2eAgFnSc1d1
Praha	Praha	k1gFnSc1
a	a	k8xC
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1741	#num#	k4
se	se	k3xPyFc4
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
českými	český	k2eAgInPc7d1
stavy	stav	k1gInPc7
provolat	provolat	k5eAaPmF
králem	král	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
článku	článek	k1gInSc6
Okupace	okupace	k1gFnSc2
Prahy	Praha	k1gFnSc2
Francouzi	Francouz	k1gMnSc3
<g/>
,	,	kIx,
Bavory	Bavor	k1gMnPc7
a	a	k8xC
Sasy	Sas	k1gMnPc7
(	(	kIx(
<g/>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1742	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
nyní	nyní	k6eAd1
musela	muset	k5eAaImAgFnS
čelit	čelit	k5eAaImF
nepřátelům	nepřítel	k1gMnPc3
na	na	k7c6
dvou	dva	k4xCgFnPc6
frontách	fronta	k1gFnPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
vypravila	vypravit	k5eAaPmAgFnS
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
získala	získat	k5eAaPmAgFnS
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
uherských	uherský	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získala	získat	k5eAaPmAgFnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
uherskou	uherský	k2eAgFnSc4d1
šlechtu	šlechta	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
byla	být	k5eAaImAgFnS
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1741	#num#	k4
v	v	k7c6
Prešpurku	Prešpurku	k?
korunována	korunovat	k5eAaBmNgFnS
uherskou	uherský	k2eAgFnSc7d1
královnou	královna	k1gFnSc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
i	i	k9
při	při	k7c6
jednání	jednání	k1gNnSc6
s	s	k7c7
uherským	uherský	k2eAgInSc7d1
sněmem	sněm	k1gInSc7
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
za	za	k7c4
cenu	cena	k1gFnSc4
řady	řada	k1gFnSc2
politických	politický	k2eAgInPc2d1
ústupků	ústupek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uzavření	uzavření	k1gNnSc1
příměří	příměří	k1gNnSc2
s	s	k7c7
Pruskem	Prusko	k1gNnSc7
uvolnilo	uvolnit	k5eAaPmAgNnS
Marii	Maria	k1gFnSc3
Terezii	Terezie	k1gFnSc3
ruce	ruka	k1gFnSc3
a	a	k8xC
umožnilo	umožnit	k5eAaPmAgNnS
habsburským	habsburský	k2eAgNnPc3d1
vojskům	vojsko	k1gNnPc3
vedeným	vedený	k2eAgMnSc7d1
polním	polní	k2eAgMnSc7d1
maršálem	maršál	k1gMnSc7
Khevenhüllerem	Khevenhüller	k1gInSc7
přejít	přejít	k5eAaPmF
do	do	k7c2
protiútoku	protiútok	k1gInSc2
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1742	#num#	k4
<g/>
,	,	kIx,
právě	právě	k6eAd1
v	v	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
zvolení	zvolení	k1gNnSc6
císařem	císař	k1gMnSc7
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
korunován	korunovat	k5eAaBmNgMnS
svým	svůj	k3xOyFgMnSc7
bratrem	bratr	k1gMnSc7
arcibiskupem	arcibiskup	k1gMnSc7
kolínským	kolínský	k2eAgMnSc7d1
jako	jako	k8xC,k8xS
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
vstoupily	vstoupit	k5eAaPmAgInP
rakouské	rakouský	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
do	do	k7c2
Mnichova	Mnichov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouská	rakouský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
zamířila	zamířit	k5eAaPmAgFnS
do	do	k7c2
Čech	Čechy	k1gFnPc2
a	a	k8xC
po	po	k7c6
delším	dlouhý	k2eAgNnSc6d2
obléhání	obléhání	k1gNnSc6
osvobodila	osvobodit	k5eAaPmAgFnS
i	i	k9
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnPc1d1
bavorsko-francouzské	bavorsko-francouzský	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
opustily	opustit	k5eAaPmAgFnP
Prahu	Praha	k1gFnSc4
v	v	k7c6
prosinci	prosinec	k1gInSc6
1742	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
dubna	duben	k1gInSc2
1743	#num#	k4
se	se	k3xPyFc4
vypravila	vypravit	k5eAaPmAgFnS
do	do	k7c2
Prahy	Praha	k1gFnSc2
i	i	k8xC
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
nechala	nechat	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1743	#num#	k4
korunovat	korunovat	k5eAaBmF
českou	český	k2eAgFnSc7d1
královnou	královna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Habsburská	habsburský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
okupovala	okupovat	k5eAaBmAgFnS
Bavorsko	Bavorsko	k1gNnSc4
s	s	k7c7
přestávkami	přestávka	k1gFnPc7
více	hodně	k6eAd2
než	než	k8xS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
během	během	k7c2
válek	válka	k1gFnPc2
byly	být	k5eAaImAgFnP
bavorské	bavorský	k2eAgFnPc1d1
země	zem	k1gFnPc1
dost	dost	k6eAd1
zpustošeny	zpustošen	k2eAgFnPc1d1
zejména	zejména	k9
nepříliš	příliš	k6eNd1
disciplinovanými	disciplinovaný	k2eAgInPc7d1
uherskými	uherský	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
VII	VII	kA
<g/>
.	.	kIx.
v	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
1745	#num#	k4
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
syn	syn	k1gMnSc1
a	a	k8xC
následník	následník	k1gMnSc1
Maxmilián	Maxmilián	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
se	se	k3xPyFc4
vzdal	vzdát	k5eAaPmAgMnS
nároků	nárok	k1gInPc2
na	na	k7c4
císařskou	císařský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
a	a	k8xC
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
uzavřel	uzavřít	k5eAaPmAgMnS
s	s	k7c7
Marií	Maria	k1gFnSc7
Terezií	Terezie	k1gFnSc7
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novým	nový	k2eAgMnSc7d1
římským	římský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1745	#num#	k4
manžel	manžel	k1gMnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
František	František	k1gMnSc1
I.	I.	kA
Štěpán	Štěpán	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhá	druhý	k4xOgFnSc1
slezská	slezský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Posílení	posílení	k1gNnSc1
pozice	pozice	k1gFnSc2
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
zájmům	zájem	k1gInPc3
pruského	pruský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1744	#num#	k4
proto	proto	k8xC
uzavřel	uzavřít	k5eAaPmAgInS
spojeneckou	spojenecký	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
v	v	k7c6
srpnu	srpen	k1gInSc6
vpadl	vpadnout	k5eAaPmAgMnS
znovu	znovu	k6eAd1
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
obsadil	obsadit	k5eAaPmAgMnS
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
kapitulovala	kapitulovat	k5eAaBmAgFnS
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytlačit	vytlačit	k5eAaPmF
Prusy	Prus	k1gMnPc4
z	z	k7c2
Čech	Čechy	k1gFnPc2
se	se	k3xPyFc4
rakouské	rakouský	k2eAgFnSc3d1
armádě	armáda	k1gFnSc3
vedené	vedený	k2eAgFnSc2d1
Karlem	Karel	k1gMnSc7
Lotrinským	lotrinský	k2eAgMnSc7d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
musela	muset	k5eAaImAgFnS
vrátit	vrátit	k5eAaPmF
z	z	k7c2
Porýní	Porýní	k1gNnSc2
<g/>
,	,	kIx,
podařilo	podařit	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pruská	pruský	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
Prahy	Praha	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
přes	přes	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
byla	být	k5eAaImAgFnS
osvobozena	osvobozen	k2eAgFnSc1d1
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
1745	#num#	k4
uzavřely	uzavřít	k5eAaPmAgInP
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Nizozemsko	Nizozemsko	k1gNnSc1
a	a	k8xC
Sasko	Sasko	k1gNnSc1
Varšavskou	varšavský	k2eAgFnSc4d1
alianci	aliance	k1gFnSc4
na	na	k7c4
omezení	omezení	k1gNnSc4
vlivu	vliv	k1gInSc2
Pruska	Prusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
protiofenzívě	protiofenzíva	k1gFnSc6
obsadily	obsadit	k5eAaPmAgFnP
rakousko-saské	rakousko-saský	k2eAgInPc4d1
oddíly	oddíl	k1gInPc4
Horní	horní	k2eAgNnSc4d1
Slezsko	Slezsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
však	však	k9
opět	opět	k6eAd1
osvědčil	osvědčit	k5eAaPmAgInS
své	svůj	k3xOyFgFnPc4
vojevůdcovské	vojevůdcovský	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
a	a	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
roku	rok	k1gInSc2
1745	#num#	k4
dokázal	dokázat	k5eAaPmAgMnS
v	v	k7c6
několika	několik	k4yIc6
bitvách	bitva	k1gFnPc6
zvítězit	zvítězit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
bylo	být	k5eAaImAgNnS
rakousko-saské	rakousko-saský	k2eAgNnSc1d1
vojsko	vojsko	k1gNnSc1
drtivě	drtivě	k6eAd1
poraženo	porazit	k5eAaPmNgNnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Hohenfriedbergu	Hohenfriedberg	k1gInSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1745	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Británie	Británie	k1gFnSc1
doporučila	doporučit	k5eAaPmAgFnS
Prusku	Prusko	k1gNnSc6
uzavřít	uzavřít	k5eAaPmF
mír	mír	k1gInSc1
za	za	k7c2
podmínek	podmínka	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1742	#num#	k4
a	a	k8xC
uznala	uznat	k5eAaPmAgFnS
pruskou	pruský	k2eAgFnSc4d1
držbu	držba	k1gFnSc4
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
Slezska	Slezsko	k1gNnSc2
nechtěla	chtít	k5eNaImAgFnS
smířit	smířit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
další	další	k2eAgFnPc4d1
porážky	porážka	k1gFnPc4
v	v	k7c6
bitvách	bitva	k1gFnPc6
u	u	k7c2
Žďáru	Žďár	k1gInSc2
(	(	kIx(
<g/>
poblíž	poblíž	k7c2
Trutnova	Trutnov	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
Katholisch-Hennersdorfu	Katholisch-Hennersdorf	k1gInSc2
a	a	k8xC
u	u	k7c2
Kesseldorfu	Kesseldorf	k1gInSc2
(	(	kIx(
<g/>
u	u	k7c2
Drážďan	Drážďany	k1gInPc2
<g/>
)	)	kIx)
ji	on	k3xPp3gFnSc4
přiměly	přimět	k5eAaPmAgInP
přistoupit	přistoupit	k5eAaPmF
na	na	k7c4
mírová	mírový	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
za	za	k7c2
britského	britský	k2eAgNnSc2d1
prostřednictví	prostřednictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
kapitulaci	kapitulace	k1gFnSc6
Saska	Sasko	k1gNnSc2
byl	být	k5eAaImAgInS
nakonec	nakonec	k6eAd1
Drážďanskou	drážďanský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1745	#num#	k4
mír	mír	k1gInSc1
mezi	mezi	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
,	,	kIx,
Saskem	Sasko	k1gNnSc7
a	a	k8xC
Pruskem	Prusko	k1gNnSc7
uzavřen	uzavřít	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prusku	Prusko	k1gNnSc6
zůstalo	zůstat	k5eAaPmAgNnS
Slezsko	Slezsko	k1gNnSc1
<g/>
,	,	kIx,
oplátkou	oplátka	k1gFnSc7
uznal	uznat	k5eAaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františka	Františka	k1gFnSc1
Štěpána	Štěpána	k1gFnSc1
císařem	císař	k1gMnSc7
a	a	k8xC
Marii	Maria	k1gFnSc3
Terezii	Terezie	k1gFnSc3
jako	jako	k8xC,k8xS
„	„	k?
<g/>
císařovnu	císařovna	k1gFnSc4
a	a	k8xC
královnu	královna	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Války	válek	k1gInPc1
o	o	k7c4
Rakouské	rakouský	k2eAgNnSc4d1
Nizozemí	Nizozemí	k1gNnSc4
a	a	k8xC
habsburské	habsburský	k2eAgFnPc4d1
državy	država	k1gFnPc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Fontenoy	Fontenoa	k1gFnSc2
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1745	#num#	k4
</s>
<s>
Paralelně	paralelně	k6eAd1
se	s	k7c7
slezskými	slezský	k2eAgFnPc7d1
válkami	válka	k1gFnPc7
a	a	k8xC
bavorsko-rakouskou	bavorsko-rakouský	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
probíhaly	probíhat	k5eAaImAgFnP
války	válka	k1gFnPc4
mezi	mezi	k7c7
koalicemi	koalice	k1gFnPc7
i	i	k8xC
na	na	k7c6
jiných	jiný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
pokračovaly	pokračovat	k5eAaImAgFnP
i	i	k9
po	po	k7c6
roce	rok	k1gInSc6
1745	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
musela	muset	k5eAaImAgNnP
rakouská	rakouský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
Španělsku	Španělsko	k1gNnSc3
a	a	k8xC
Neapolskému	neapolský	k2eAgNnSc3d1
království	království	k1gNnSc3
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
získání	získání	k1gNnSc1
rakouského	rakouský	k2eAgNnSc2d1
Milánska	Milánsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojencem	spojenec	k1gMnSc7
Rakouska	Rakousko	k1gNnSc2
na	na	k7c6
italské	italský	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
Sardinie	Sardinie	k1gFnSc2
a	a	k8xC
Piemontu	Piemont	k1gInSc2
Karel	Karel	k1gMnSc1
Emanuel	Emanuel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aliance	aliance	k1gFnSc1
byla	být	k5eAaImAgFnS
upevněna	upevněn	k2eAgFnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1743	#num#	k4
smlouvou	smlouva	k1gFnSc7
z	z	k7c2
Wormsu	Worms	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
zapojila	zapojit	k5eAaPmAgFnS
i	i	k9
Británie	Británie	k1gFnSc1
<g/>
;	;	kIx,
za	za	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
a	a	k8xC
zaručení	zaručení	k1gNnSc4
držení	držení	k1gNnSc2
Milánska	Milánsko	k1gNnSc2
a	a	k8xC
Parmy	Parma	k1gFnSc2
se	se	k3xPyFc4
však	však	k9
muselo	muset	k5eAaImAgNnS
Rakousko	Rakousko	k1gNnSc1
vzdát	vzdát	k5eAaPmF
některých	některý	k3yIgNnPc2
území	území	k1gNnPc2
ve	v	k7c4
prospěch	prospěch	k1gInSc4
Sardinského	sardinský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milánsko	Milánsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
uhájeno	uhájit	k5eAaPmNgNnS
<g/>
,	,	kIx,
Parma	Parma	k1gFnSc1
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
postoupena	postoupen	k2eAgFnSc1d1
Španělům	Španěl	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1743	#num#	k4
válčilo	válčit	k5eAaImAgNnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
Porýní	Porýní	k1gNnSc1
a	a	k8xC
Rakouské	rakouský	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
(	(	kIx(
<g/>
též	též	k9
Jižní	jižní	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
<g/>
,	,	kIx,
dnešní	dnešní	k2eAgFnSc1d1
Belgie	Belgie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
bojovala	bojovat	k5eAaImAgNnP
alianční	alianční	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
pragmatická	pragmatický	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
)	)	kIx)
proti	proti	k7c3
Francii	Francie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Dettingenu	Dettingen	k1gInSc2
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1743	#num#	k4
<g/>
)	)	kIx)
zvítězila	zvítězit	k5eAaPmAgFnS
pragmatická	pragmatický	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
britským	britský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Jiřím	Jiří	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1744	#num#	k4
napadla	napadnout	k5eAaPmAgFnS
francouzská	francouzský	k2eAgFnSc1d1
vojska	vojsko	k1gNnSc2
Rakouské	rakouský	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
a	a	k8xC
po	po	k7c6
vítězné	vítězný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Fontenoy	Fontenoa	k1gFnSc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1745	#num#	k4
<g/>
)	)	kIx)
pak	pak	k6eAd1
okupovala	okupovat	k5eAaBmAgFnS
většinu	většina	k1gFnSc4
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
války	válka	k1gFnSc2
byly	být	k5eAaImAgInP
zataženy	zatáhnout	k5eAaPmNgInP
i	i	k9
Spojené	spojený	k2eAgFnPc1d1
provincie	provincie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1746	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
nizozemské	nizozemský	k2eAgFnSc6d1
Bredě	Breda	k1gFnSc6
zahájena	zahájen	k2eAgNnPc4d1
mírová	mírový	k2eAgNnPc4d1
jednání	jednání	k1gNnPc4
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
Británií	Británie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
poté	poté	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
Cáchách	Cáchy	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ukončení	ukončení	k1gNnSc1
válek	válka	k1gFnPc2
</s>
<s>
Války	válek	k1gInPc1
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
ukončil	ukončit	k5eAaPmAgInS
Cášský	cášský	k2eAgInSc1d1
mír	mír	k1gInSc1
–	–	k?
mírová	mírový	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
uzavřená	uzavřený	k2eAgFnSc1d1
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1748	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
výsledkem	výsledek	k1gInSc7
mezinárodního	mezinárodní	k2eAgInSc2d1
kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
v	v	k7c6
Cáchách	Cáchy	k1gFnPc6
konal	konat	k5eAaImAgInS
od	od	k7c2
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1748	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
kongresu	kongres	k1gInSc6
zastupoval	zastupovat	k5eAaImAgMnS
habsburskou	habsburský	k2eAgFnSc4d1
monarchii	monarchie	k1gFnSc4
Václav	Václav	k1gMnSc1
Antonín	Antonín	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Kounic	Kounice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsah	obsah	k1gInSc1
smlouvy	smlouva	k1gFnSc2
byl	být	k5eAaImAgInS
především	především	k9
výsledkem	výsledek	k1gInSc7
vyjednávání	vyjednávání	k1gNnSc2
Británie	Británie	k1gFnSc2
a	a	k8xC
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
se	se	k3xPyFc4
připojily	připojit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecně	obecně	k6eAd1
byl	být	k5eAaImAgInS
dohodnut	dohodnout	k5eAaPmNgInS
návrat	návrat	k1gInSc1
ke	k	k7c3
stavu	stav	k1gInSc3
před	před	k7c7
válkou	válka	k1gFnSc7
<g/>
,	,	kIx,
tzn.	tzn.	kA
navrácení	navrácení	k1gNnSc1
zachvácených	zachvácený	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
potvrdil	potvrdit	k5eAaPmAgMnS
Pragmatickou	pragmatický	k2eAgFnSc4d1
sankci	sankce	k1gFnSc4
(	(	kIx(
<g/>
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
však	však	k9
Habsburská	habsburský	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
musela	muset	k5eAaImAgFnS
odstoupit	odstoupit	k5eAaPmF
řadu	řada	k1gFnSc4
území	území	k1gNnSc2
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
zisk	zisk	k1gInSc1
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
Slezska	Slezsko	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
Kladska	Kladsko	k1gNnSc2
<g/>
)	)	kIx)
Pruskem	Prusko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Chronologický	chronologický	k2eAgInSc4d1
přehled	přehled	k1gInSc4
válečných	válečný	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Brna	Brno	k1gNnSc2
prusko-saským	prusko-saský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
roku	rok	k1gInSc2
1742	#num#	k4
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
XV	XV	kA
<g/>
.	.	kIx.
a	a	k8xC
Mořic	Mořic	k1gMnSc1
hrabě	hrabě	k1gMnSc1
Saský	saský	k2eAgMnSc1d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Lauffeldu	Lauffeld	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1747	#num#	k4
</s>
<s>
Vítězství	vítězství	k1gNnSc1
britského	britský	k2eAgMnSc2d1
admirála	admirál	k1gMnSc2
Ansona	Anson	k1gMnSc2
nad	nad	k7c7
Francouzi	Francouz	k1gMnPc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Finisterre	Finisterr	k1gInSc5
roku	rok	k1gInSc2
1747	#num#	k4
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1
tábor	tábor	k1gInSc1
v	v	k7c6
Bavorsku	Bavorsko	k1gNnSc6
během	během	k7c2
války	válka	k1gFnSc2
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
</s>
<s>
Následující	následující	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
shrnuje	shrnovat	k5eAaImIp3nS
válečné	válečný	k2eAgFnSc3d1
události	událost	k1gFnSc3
na	na	k7c6
všech	všecek	k3xTgFnPc6
frontách	fronta	k1gFnPc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
akcí	akce	k1gFnPc2
souvisejících	související	k2eAgFnPc2d1
s	s	k7c7
válkami	válka	k1gFnPc7
o	o	k7c4
dědictví	dědictví	k1gNnSc4
rakouské	rakouský	k2eAgFnSc2d1
pouze	pouze	k6eAd1
okrajově	okrajově	k6eAd1
(	(	kIx(
<g/>
jsou	být	k5eAaImIp3nP
odlišeny	odlišit	k5eAaPmNgInP
kurzívou	kurzíva	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1740	#num#	k4
</s>
<s>
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
útok	útok	k1gInSc1
Pruska	Prusko	k1gNnSc2
na	na	k7c4
rakouské	rakouský	k2eAgNnSc4d1
Slezsko	Slezsko	k1gNnSc4
</s>
<s>
1741	#num#	k4
</s>
<s>
duben	duben	k1gInSc1
<g/>
:	:	kIx,
vítězství	vítězství	k1gNnSc1
Prusů	Prus	k1gMnPc2
nad	nad	k7c7
Rakušany	Rakušan	k1gMnPc7
u	u	k7c2
Mollwitz	Mollwitza	k1gFnPc2
</s>
<s>
září	září	k1gNnSc4
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
vpád	vpád	k1gInSc1
francouzsko-bavorského	francouzsko-bavorský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
do	do	k7c2
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
do	do	k7c2
Čech	Čechy	k1gFnPc2
</s>
<s>
1742	#num#	k4
</s>
<s>
leden	leden	k1gInSc1
<g/>
:	:	kIx,
vpád	vpád	k1gInSc1
rakouských	rakouský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
do	do	k7c2
Bavorska	Bavorsko	k1gNnSc2
<g/>
,	,	kIx,
španělský	španělský	k2eAgInSc4d1
vpád	vpád	k1gInSc4
do	do	k7c2
Itálie	Itálie	k1gFnSc2
</s>
<s>
únor	únor	k1gInSc1
–	–	k?
květen	květen	k1gInSc4
<g/>
:	:	kIx,
pruský	pruský	k2eAgInSc4d1
vpád	vpád	k1gInSc4
na	na	k7c4
Moravu	Morava	k1gFnSc4
(	(	kIx(
<g/>
vítězství	vítězství	k1gNnSc1
Prusů	Prus	k1gMnPc2
nad	nad	k7c7
Rakušany	Rakušan	k1gMnPc7
u	u	k7c2
Chotusic	Chotusice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Dne	den	k1gInSc2
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1742	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
bitva	bitva	k1gFnSc1
u	u	k7c2
Zahájí	zahájit	k5eAaPmIp3nS
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
se	se	k3xPyFc4
spolu	spolu	k6eAd1
střetla	střetnout	k5eAaPmAgFnS
rakouská	rakouský	k2eAgFnSc1d1
a	a	k8xC
francouzská	francouzský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
</s>
<s>
srpen	srpen	k1gInSc1
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
útok	útok	k1gInSc1
španělsko-neapolské	španělsko-neapolský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
na	na	k7c4
Piemont	Piemont	k1gInSc4
a	a	k8xC
rakouskou	rakouský	k2eAgFnSc4d1
Lombardii	Lombardie	k1gFnSc4
</s>
<s>
1743	#num#	k4
</s>
<s>
únor	únor	k1gInSc1
<g/>
:	:	kIx,
vítězství	vítězství	k1gNnSc1
rakousko-sardinské	rakousko-sardinský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
nad	nad	k7c4
Španěly	Španěl	k1gMnPc4
u	u	k7c2
Camposanto	Camposanta	k1gFnSc5
</s>
<s>
duben	duben	k1gInSc1
–	–	k?
říjen	říjen	k1gInSc1
<g/>
:	:	kIx,
boje	boj	k1gInPc1
pragmatické	pragmatický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
s	s	k7c7
Francouzi	Francouz	k1gMnPc7
v	v	k7c6
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
v	v	k7c6
červnu	červen	k1gInSc6
vítězství	vítězství	k1gNnSc2
pragmatické	pragmatický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
u	u	k7c2
Dettingenu	Dettingen	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
1744	#num#	k4
</s>
<s>
únor	únor	k1gInSc1
<g/>
:	:	kIx,
vítězství	vítězství	k1gNnSc1
britského	britský	k2eAgNnSc2d1
loďstva	loďstvo	k1gNnSc2
nad	nad	k7c7
francouzsko-španělským	francouzsko-španělský	k2eAgNnSc7d1
loďstvem	loďstvo	k1gNnSc7
u	u	k7c2
Toulonu	Toulon	k1gInSc2
</s>
<s>
květen	květen	k1gInSc1
–	–	k?
srpen	srpen	k1gInSc1
<g/>
:	:	kIx,
francouzsko-rakouské	francouzsko-rakouský	k2eAgInPc1d1
boje	boj	k1gInPc1
v	v	k7c6
Rakouském	rakouský	k2eAgNnSc6d1
Nizozemí	Nizozemí	k1gNnSc6
a	a	k8xC
severní	severní	k2eAgFnSc6d1
Francii	Francie	k1gFnSc6
</s>
<s>
červen	červen	k1gInSc1
–	–	k?
září	září	k1gNnSc2
<g/>
:	:	kIx,
francouzsko-španělské	francouzsko-španělský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
proti	proti	k7c3
Piemontu	Piemont	k1gInSc3
(	(	kIx(
<g/>
porážka	porážka	k1gFnSc1
sardinských	sardinský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
u	u	k7c2
Madonny	Madonna	k1gFnSc2
dell	dell	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Olmo	Olmo	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
srpen	srpen	k1gInSc1
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
pruský	pruský	k2eAgInSc1d1
vpád	vpád	k1gInSc1
do	do	k7c2
Čech	Čechy	k1gFnPc2
</s>
<s>
1745	#num#	k4
</s>
<s>
duben	duben	k1gInSc1
–	–	k?
září	září	k1gNnSc2
<g/>
:	:	kIx,
boje	boj	k1gInSc2
o	o	k7c4
Rakouské	rakouský	k2eAgNnSc4d1
Nizozemí	Nizozemí	k1gNnSc4
mezi	mezi	k7c7
Francií	Francie	k1gFnSc7
a	a	k8xC
britsko-nizozemsko-rakouským	britsko-nizozemsko-rakouský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
(	(	kIx(
<g/>
vítězství	vítězství	k1gNnSc1
Francouzů	Francouz	k1gMnPc2
u	u	k7c2
Fontenoy	Fontenoa	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
květen	květen	k1gInSc1
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
francouzsko-španělské	francouzsko-španělský	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
proti	proti	k7c3
Piemontu	Piemont	k1gInSc3
(	(	kIx(
<g/>
porážka	porážka	k1gFnSc1
Sardiňanů	Sardiňan	k1gMnPc2
u	u	k7c2
Bassignana	Bassignan	k1gMnSc2
<g/>
,	,	kIx,
obsazení	obsazení	k1gNnSc1
rakouského	rakouský	k2eAgInSc2d1
Milána	Milán	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
červen	červen	k1gInSc1
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
válka	válka	k1gFnSc1
Rakouska	Rakousko	k1gNnSc2
a	a	k8xC
Saska	Sasko	k1gNnSc2
proti	proti	k7c3
Prusku	Prusko	k1gNnSc3
(	(	kIx(
<g/>
pruská	pruský	k2eAgNnPc1d1
vítězství	vítězství	k1gNnPc1
u	u	k7c2
Hohenfriedbergu	Hohenfriedberg	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
Žďáru	Žďár	k1gInSc2
a	a	k8xC
u	u	k7c2
Kesselsdorfu	Kesselsdorf	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
červen	červen	k1gInSc1
<g/>
:	:	kIx,
dobytí	dobytí	k1gNnSc1
francouzské	francouzský	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
Louisbourg	Louisbourg	k1gInSc1
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
Brity	Brit	k1gMnPc4
</s>
<s>
červenec	červenec	k1gInSc1
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
jakobitské	jakobitský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
,	,	kIx,
tažení	tažení	k1gNnSc1
jakobitů	jakobita	k1gMnPc2
na	na	k7c4
Londýn	Londýn	k1gInSc4
</s>
<s>
1746	#num#	k4
</s>
<s>
leden	leden	k1gInSc1
–	–	k?
říjen	říjen	k1gInSc1
<g/>
:	:	kIx,
válka	válka	k1gFnSc1
Francie	Francie	k1gFnSc2
proti	proti	k7c3
britsko-rakouským	britsko-rakouský	k2eAgNnPc3d1
vojskům	vojsko	k1gNnPc3
v	v	k7c6
Rakouském	rakouský	k2eAgNnSc6d1
Nizozemí	Nizozemí	k1gNnSc6
(	(	kIx(
<g/>
vítězství	vítězství	k1gNnSc1
Francouzů	Francouz	k1gMnPc2
u	u	k7c2
Rocourtu	Rocourt	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
leden	leden	k1gInSc1
–	–	k?
duben	duben	k1gInSc1
<g/>
:	:	kIx,
boje	boj	k1gInSc2
jakobitů	jakobita	k1gMnPc2
s	s	k7c7
britskou	britský	k2eAgFnSc7d1
armádou	armáda	k1gFnSc7
(	(	kIx(
<g/>
vítězství	vítězství	k1gNnSc1
jakobitů	jakobita	k1gMnPc2
u	u	k7c2
Falkirku	Falkirek	k1gInSc2
<g/>
,	,	kIx,
konečná	konečný	k2eAgFnSc1d1
porážka	porážka	k1gFnSc1
u	u	k7c2
Cullodenu	Culloden	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
březen	březen	k1gInSc1
–	–	k?
prosinec	prosinec	k1gInSc1
<g/>
:	:	kIx,
boje	boj	k1gInPc1
rakousko-sardinských	rakousko-sardinský	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
proti	proti	k7c3
Španělům	Španěl	k1gMnPc3
<g/>
,	,	kIx,
Francouzům	Francouz	k1gMnPc3
a	a	k8xC
Janovanům	Janovan	k1gMnPc3
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
(	(	kIx(
<g/>
vítězství	vítězství	k1gNnSc1
Rakušanů	Rakušan	k1gMnPc2
u	u	k7c2
Piacenzy	Piacenza	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1747	#num#	k4
</s>
<s>
duben	duben	k1gInSc1
–	–	k?
září	září	k1gNnSc2
<g/>
:	:	kIx,
války	válka	k1gFnSc2
o	o	k7c4
Rakouské	rakouský	k2eAgNnSc4d1
Nizozemí	Nizozemí	k1gNnSc4
(	(	kIx(
<g/>
francouzské	francouzský	k2eAgNnSc4d1
vítězství	vítězství	k1gNnSc4
nad	nad	k7c7
aliančními	alianční	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
u	u	k7c2
Lafeultu	Lafeult	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
počátky	počátek	k1gInPc1
jednání	jednání	k1gNnSc2
o	o	k7c4
míru	míra	k1gFnSc4
</s>
<s>
květen	květen	k1gInSc1
–	–	k?
říjen	říjen	k1gInSc1
<g/>
:	:	kIx,
britsko-francouzské	britsko-francouzský	k2eAgFnSc2d1
námořní	námořní	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
úspěšné	úspěšný	k2eAgNnSc1d1
pro	pro	k7c4
Brity	Brit	k1gMnPc4
<g/>
)	)	kIx)
</s>
<s>
červenec	červenec	k1gInSc1
–	–	k?
září	září	k1gNnSc2
<g/>
:	:	kIx,
pokračování	pokračování	k1gNnSc1
války	válka	k1gFnSc2
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Itálii	Itálie	k1gFnSc6
</s>
<s>
1748	#num#	k4
</s>
<s>
květen	květen	k1gInSc4
<g/>
:	:	kIx,
dobytí	dobytí	k1gNnSc4
Maastrichtu	Maastricht	k1gInSc2
Francouzi	Francouz	k1gMnPc1
</s>
<s>
červen	červen	k1gInSc1
<g/>
:	:	kIx,
všeobecné	všeobecný	k2eAgNnSc1d1
příměří	příměří	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ŠTĚŘÍKOVÁ	ŠTĚŘÍKOVÁ	kA
<g/>
,	,	kIx,
EDITA	Edita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozváni	pozvat	k5eAaPmNgMnP
do	do	k7c2
Slezska	Slezsko	k1gNnSc2
:	:	kIx,
vznik	vznik	k1gInSc1
prvních	první	k4xOgFnPc2
českých	český	k2eAgFnPc2d1
emigrantských	emigrantský	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
pruském	pruský	k2eAgNnSc6d1
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
KALICH	kalich	k1gInSc1
599	#num#	k4
pages	pages	k1gMnSc1
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7017	#num#	k4
<g/>
-	-	kIx~
<g/>
553	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
TAPIÉ	TAPIÉ	kA
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
Lucien	Lucien	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc1
a	a	k8xC
Evropa	Evropa	k1gFnSc1
:	:	kIx,
od	od	k7c2
baroka	baroko	k1gNnSc2
k	k	k7c3
osvícenství	osvícenství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
398	#num#	k4
s.	s.	k?
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
616	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
67	#num#	k4
<g/>
-	-	kIx~
<g/>
99	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
SKŘIVAN	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válek	k1gInPc1
o	o	k7c4
dědictví	dědictví	k1gNnSc4
rakouské	rakouský	k2eAgFnSc2d1
1740	#num#	k4
<g/>
-	-	kIx~
<g/>
1748	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
↑	↑	k?
TARABA	TARABA	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sukně	sukně	k1gFnSc1
proti	proti	k7c3
kalhotám	kalhoty	k1gFnPc3
:	:	kIx,
válka	válka	k1gFnSc1
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
1740	#num#	k4
<g/>
-	-	kIx~
<g/>
1748	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
462	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7557	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BĚLINA	Bělina	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
KAŠE	kaše	k1gFnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
KUČERA	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
P.	P.	kA
Velké	velký	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
zemí	zem	k1gFnSc7
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
X.	X.	kA
1740	#num#	k4
<g/>
-	-	kIx~
<g/>
1792	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
768	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
384	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GAWRECKI	GAWRECKI	kA
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
1740-2000	1740-2000	k4
I.	I.	kA
<g/>
-II	-II	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opava	Opava	k1gFnSc1
<g/>
:	:	kIx,
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Filozoficko-přírodovědecká	filozoficko-přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
Ústav	ústav	k1gInSc1
historie	historie	k1gFnSc2
a	a	k8xC
muzeologie	muzeologie	k1gFnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
656	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7248	#num#	k4
<g/>
-	-	kIx~
<g/>
226	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HLAVAČKA	hlavačka	k1gFnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Albrecht	Albrecht	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
druhého	druhý	k4xOgMnSc2
zimního	zimní	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Akropolis	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
155	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80-85770-50-4	80-85770-50-4	k4
</s>
<s>
HOŘČIČKA	HOŘČIČKA	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruska	Rusko	k1gNnPc1
a	a	k8xC
Prusko	Prusko	k1gNnSc1
v	v	k7c6
období	období	k1gNnSc6
slezských	slezský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
146	#num#	k4
<g/>
-	-	kIx~
<g/>
153	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAUR	Maur	k1gMnSc1
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1743	#num#	k4
-	-	kIx~
korunovace	korunovace	k1gFnSc2
na	na	k7c4
usmířenou	usmířená	k1gFnSc4
:	:	kIx,
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Havran	Havran	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
187	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86515	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
555	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SKŘIVAN	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válek	k1gInPc4
o	o	k7c6
dědictví	dědictví	k1gNnSc6
rakouské	rakouský	k2eAgInPc1d1
<g/>
,	,	kIx,
1740	#num#	k4
<g/>
-	-	kIx~
<g/>
1748	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historický	historický	k2eAgInSc1d1
obzor	obzor	k1gInSc1
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
6097	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
STELLNER	STELLNER	kA
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
Veliký	veliký	k2eAgMnSc1d1
:	:	kIx,
cesta	cesta	k1gFnSc1
Pruska	Prusko	k1gNnSc2
k	k	k7c3
velmocenskému	velmocenský	k2eAgNnSc3d1
postavení	postavení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panevropa	Panevropa	k1gFnSc1
<g/>
,	,	kIx,
1998	#num#	k4
<g/>
.	.	kIx.
552	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85846	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VONDRA	Vondra	k1gMnSc1
<g/>
,	,	kIx,
Roman	Roman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
země	zem	k1gFnPc4
v	v	k7c6
letech	let	k1gInPc6
1705	#num#	k4
<g/>
–	–	k?
<g/>
1792	#num#	k4
:	:	kIx,
doba	doba	k1gFnSc1
absolutismu	absolutismus	k1gInSc2
<g/>
,	,	kIx,
osvícenství	osvícenství	k1gNnSc2
<g/>
,	,	kIx,
paruk	paruka	k1gFnPc2
a	a	k8xC
třírohých	třírohý	k2eAgInPc2d1
klobouků	klobouk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
384	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
448	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TAPIÉ	TAPIÉ	kA
<g/>
,	,	kIx,
Victor	Victor	k1gMnSc1
Lucien	Lucien	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc1
a	a	k8xC
Evropa	Evropa	k1gFnSc1
:	:	kIx,
od	od	k7c2
baroka	baroko	k1gNnSc2
k	k	k7c3
osvícenství	osvícenství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
398	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
204	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
616	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
TARABA	TARABA	kA
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sukně	sukně	k1gFnSc1
proti	proti	k7c3
kalhotám	kalhoty	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
1740	#num#	k4
<g/>
–	–	k?
<g/>
1748	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Epocha	epocha	k1gFnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
.	.	kIx.
462	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7557	#num#	k4
<g/>
-	-	kIx~
<g/>
176	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Války	válka	k1gFnSc2
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
války	válka	k1gFnSc2
o	o	k7c4
rakouské	rakouský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Dějiny	dějiny	k1gFnPc1
Těšínska	Těšínsko	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Války	válek	k1gInPc1
slezské	slezský	k2eAgInPc1d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Článek	článek	k1gInSc1
Aleše	Aleš	k1gMnSc2
Skřivana	Skřivan	k1gMnSc2
v	v	k7c6
Historickém	historický	k2eAgInSc6d1
obzoru	obzor	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Slezsko	Slezsko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
116660	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4172460-4	4172460-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85009756	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85009756	#num#	k4
</s>
