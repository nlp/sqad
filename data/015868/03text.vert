<s>
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
World	World	k1gMnSc1
of	of	k?
TanksVývojářWargamingVydavatelWargamingEngineENCORE	TanksVývojářWargamingVydavatelWargamingEngineENCORE	k1gMnSc1
EnginePlatformyMicrosoft	EnginePlatformyMicrosoft	k1gMnSc1
WindowsXbox	WindowsXbox	k1gInSc4
360	#num#	k4
<g/>
Xbox	Xbox	k1gInSc4
OnePlaystation	OnePlaystation	k1gInSc4
4	#num#	k4
<g/>
macOSDatum	macOSDatum	k1gNnSc4
vydání	vydání	k1gNnSc2
<g/>
12	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
<g/>
Žánryakční	Žánryakční	k2eAgFnSc1d1
videohramasivně	videohramasivně	k6eAd1
multiplayerová	multiplayerový	k2eAgFnSc1d1
online	onlinout	k5eAaPmIp3nS
hrasimulátor	hrasimulátor	k1gInSc4
tankuHerní	tankuHerní	k2eAgFnSc1d1
módvideohra	módvideohra	k1gFnSc1
pro	pro	k7c4
více	hodně	k6eAd2
hráčůKlasifikacePEGI	hráčůKlasifikacePEGI	k?
ESRB	ESRB	kA
Oficiální	oficiální	k2eAgFnSc1d1
webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
WoT	WoT	k1gFnSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
Svět	svět	k1gInSc1
tanků	tank	k1gInPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
akční	akční	k2eAgInSc1d1
MMO	MMO	kA
free	freat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
play	play	k0
hra	hra	k1gFnSc1
s	s	k7c7
tématikou	tématika	k1gFnSc7
tankových	tankový	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
vyvíjená	vyvíjený	k2eAgFnSc1d1
společností	společnost	k1gFnSc7
Wargaming	Wargaming	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
hráč	hráč	k1gMnSc1
hraje	hrát	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
třetí	třetí	k4xOgFnSc2
osoby	osoba	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
odstřelovacím	odstřelovací	k2eAgInSc6d1
módu	mód	k1gInSc6
z	z	k7c2
pohledu	pohled	k1gInSc2
střelce	střelec	k1gMnSc2
tanku	tank	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
dělostřeleckém	dělostřelecký	k2eAgInSc6d1
módu	mód	k1gInSc6
z	z	k7c2
ptačí	ptačí	k2eAgFnSc2d1
perspektivy	perspektiva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
je	být	k5eAaImIp3nS
přes	přes	k7c4
600	#num#	k4
vozidel	vozidlo	k1gNnPc2
od	od	k7c2
konce	konec	k1gInSc2
První	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
meziválečného	meziválečný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
hojně	hojně	k6eAd1
zastoupenou	zastoupený	k2eAgFnSc4d1
vojenskou	vojenský	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
z	z	k7c2
období	období	k1gNnSc2
Druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
konče	konče	k7c7
technikou	technika	k1gFnSc7
z	z	k7c2
doby	doba	k1gFnSc2
Korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
začátku	začátek	k1gInSc2
války	válka	k1gFnSc2
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
má	mít	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
3	#num#	k4
miliardy	miliarda	k4xCgFnSc2
registrovaných	registrovaný	k2eAgMnPc2d1
uživatelů	uživatel	k1gMnPc2
a	a	k8xC
180	#num#	k4
milionů	milion	k4xCgInPc2
aktivních	aktivní	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
aktualizaci	aktualizace	k1gFnSc6
1.0	1.0	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byla	být	k5eAaImAgFnS
zásadně	zásadně	k6eAd1
přepracována	přepracován	k2eAgFnSc1d1
grafická	grafický	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
využitím	využití	k1gNnSc7
nového	nový	k2eAgMnSc2d1
ENCORE	ENCORE	kA
Engine	Engin	k1gInSc5
byly	být	k5eAaImAgFnP
mapy	mapa	k1gFnPc1
přepracovány	přepracovat	k5eAaPmNgFnP
do	do	k7c2
HD	HD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
válečná	válečný	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
speciálně	speciálně	k6eAd1
vytvořená	vytvořený	k2eAgFnSc1d1
pro	pro	k7c4
každou	každý	k3xTgFnSc4
mapu	mapa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Hra	hra	k1gFnSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
herních	herní	k2eAgMnPc2d1
vývojářů	vývojář	k1gMnPc2
2011	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
–	–	k?
hra	hra	k1gFnSc1
oceněná	oceněný	k2eAgFnSc1d1
volbou	volba	k1gFnSc7
publika	publikum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Guinnessova	Guinnessův	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
rekordů	rekord	k1gInPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
–	–	k?
hra	hra	k1gFnSc1
s	s	k7c7
největším	veliký	k2eAgInSc7d3
počtem	počet	k1gInSc7
současně	současně	k6eAd1
hrajících	hrající	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
na	na	k7c6
jednom	jeden	k4xCgInSc6
serveru	server	k1gInSc6
(	(	kIx(
<g/>
jednou	jednou	k9
sama	sám	k3xTgFnSc1
sebe	sebe	k3xPyFc4
překonala	překonat	k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Golden	Goldna	k1gFnPc2
Joystick	Joysticka	k1gFnPc2
Awards	Awards	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
<g/>
–	–	k?
nejlepší	dobrý	k2eAgInPc1d3
masivně	masivně	k6eAd1
multiplayer	multiplayer	k1gInSc4
(	(	kIx(
<g/>
MMO	MMO	kA
<g/>
)	)	kIx)
Online	Onlin	k1gInSc5
hra	hra	k1gFnSc1
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
Nejlepší	dobrý	k2eAgFnSc1d3
online	onlinout	k5eAaPmIp3nS
hra	hra	k1gFnSc1
roku	rok	k1gInSc2
2013	#num#	k4
a	a	k8xC
stále	stále	k6eAd1
se	se	k3xPyFc4
hraje	hrát	k5eAaImIp3nS
2017	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
a	a	k8xC
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
bitev	bitva	k1gFnPc2
</s>
<s>
Hra	hra	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
6	#num#	k4
rozdílných	rozdílný	k2eAgInPc2d1
typů	typ	k1gInPc2
bitev	bitva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
provozuje	provozovat	k5eAaImIp3nS
6	#num#	k4
herních	herní	k2eAgInPc2d1
serverů	server	k1gInPc2
podle	podle	k7c2
geografického	geografický	k2eAgNnSc2d1
umístění	umístění	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1
(	(	kIx(
<g/>
spuštěn	spustit	k5eAaPmNgInS
jako	jako	k9
první	první	k4xOgInSc1
<g/>
,	,	kIx,
8	#num#	k4
clusterů	cluster	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Evropský	evropský	k2eAgMnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
clustery	cluster	k1gInPc7
<g/>
,	,	kIx,
funguje	fungovat	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
severní	severní	k2eAgFnSc4d1
a	a	k8xC
západní	západní	k2eAgFnSc4d1
Afriku	Afrika	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Severoamerický	severoamerický	k2eAgMnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
clustery	cluster	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
clustery	cluster	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
Korejský	korejský	k2eAgInSc1d1
</s>
<s>
Vietnamský	vietnamský	k2eAgInSc1d1
</s>
<s>
Hra	hra	k1gFnSc1
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
modelu	model	k1gInSc6
free-to-play	free-to-plaa	k1gFnSc2
<g/>
,	,	kIx,
samotné	samotný	k2eAgNnSc1d1
hraní	hraní	k1gNnSc1
je	být	k5eAaImIp3nS
zdarma	zdarma	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
za	za	k7c4
peníze	peníz	k1gInPc4
koupit	koupit	k5eAaPmF
prémiový	prémiový	k2eAgInSc4d1
účet	účet	k1gInSc4
(	(	kIx(
<g/>
zvyšující	zvyšující	k2eAgInPc1d1
výnosy	výnos	k1gInPc1
z	z	k7c2
každé	každý	k3xTgFnSc2
bitvy	bitva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
speciální	speciální	k2eAgInPc1d1
prémiové	prémiový	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
zlaťáky	zlaťák	k1gInPc1
(	(	kIx(
<g/>
prémiová	prémiový	k2eAgFnSc1d1
měna	měna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kamufláže	kamufláž	k1gFnPc4
aj.	aj.	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
registraci	registrace	k1gFnSc6
dostává	dostávat	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
zdarma	zdarma	k6eAd1
sérii	série	k1gFnSc4
základních	základní	k2eAgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
po	po	k7c6
dokončení	dokončení	k1gNnSc6
základního	základní	k2eAgInSc2d1
tutoriálu	tutoriál	k1gInSc2
je	být	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
umožněno	umožněn	k2eAgNnSc1d1
vyjet	vyjet	k5eAaPmF
se	se	k3xPyFc4
zvoleným	zvolený	k2eAgInSc7d1
strojem	stroj	k1gInSc7
do	do	k7c2
bitvy	bitva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitva	bitva	k1gFnSc1
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
typicky	typicky	k6eAd1
<g/>
)	)	kIx)
souboj	souboj	k1gInSc1
15	#num#	k4
proti	proti	k7c3
15	#num#	k4
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gInSc6
konci	konec	k1gInSc6
je	být	k5eAaImIp3nS
vyhodnocena	vyhodnocen	k2eAgFnSc1d1
účast	účast	k1gFnSc1
každého	každý	k3xTgMnSc4
na	na	k7c6
bitvě	bitva	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
odměněna	odměnit	k5eAaPmNgFnS
patřičným	patřičný	k2eAgInSc7d1
obnosem	obnos	k1gInSc7
kreditů	kredit	k1gInPc2
(	(	kIx(
<g/>
herní	herní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
zkušeností	zkušenost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
kredity	kredit	k1gInPc4
pak	pak	k6eAd1
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
investovat	investovat	k5eAaBmF
do	do	k7c2
nových	nový	k2eAgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
ovšem	ovšem	k9
musí	muset	k5eAaImIp3nP
mít	mít	k5eAaImF
dostatek	dostatek	k1gInSc4
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
na	na	k7c4
nákup	nákup	k1gInSc4
munice	munice	k1gFnSc2
<g/>
,	,	kIx,
vybavení	vybavení	k1gNnSc2
tanků	tank	k1gInPc2
aj.	aj.	kA
</s>
<s>
Hratelných	hratelný	k2eAgInPc2d1
tanků	tank	k1gInPc2
za	za	k7c4
jedenáct	jedenáct	k4xCc4
národů	národ	k1gInPc2
(	(	kIx(
<g/>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
Británie	Británie	k1gFnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
a	a	k8xC
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
připraveno	připravit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
550	#num#	k4
(	(	kIx(
<g/>
a	a	k8xC
nabídka	nabídka	k1gFnSc1
neustále	neustále	k6eAd1
roste	růst	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
přístup	přístup	k1gInSc1
k	k	k7c3
nim	on	k3xPp3gFnPc3
je	být	k5eAaImIp3nS
omezen	omezit	k5eAaPmNgInS
přes	přes	k7c4
kariérní	kariérní	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tanky	tank	k1gInPc7
jsou	být	k5eAaImIp3nP
rozděleny	rozdělen	k2eAgInPc1d1
jak	jak	k8xC,k8xS
podle	podle	k7c2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
podle	podle	k7c2
charakteristiky	charakteristika	k1gFnSc2
(	(	kIx(
<g/>
lehký	lehký	k2eAgInSc4d1
tank	tank	k1gInSc4
<g/>
,	,	kIx,
střední	střední	k2eAgInSc4d1
tank	tank	k1gInSc4
<g/>
,	,	kIx,
těžký	těžký	k2eAgInSc4d1
tank	tank	k1gInSc4
<g/>
,	,	kIx,
stíhač	stíhač	k1gMnSc1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
artilerie	artilerie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
tvořící	tvořící	k2eAgFnSc4d1
vývojovou	vývojový	k2eAgFnSc4d1
větev	větev	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
hráč	hráč	k1gMnSc1
může	moct	k5eAaImIp3nS
za	za	k7c4
získané	získaný	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
postupně	postupně	k6eAd1
odemykat	odemykat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
tanků	tank	k1gInPc2
má	mít	k5eAaImIp3nS
různé	různý	k2eAgInPc4d1
druhy	druh	k1gInPc4
děl	dělo	k1gNnPc2
<g/>
,	,	kIx,
pásů	pás	k1gInPc2
<g/>
,	,	kIx,
rádií	rádio	k1gNnPc2
či	či	k8xC
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
možné	možný	k2eAgNnSc1d1
za	za	k7c4
zkušenosti	zkušenost	k1gFnPc4
získávat	získávat	k5eAaImF
a	a	k8xC
zároveň	zároveň	k6eAd1
namontovat	namontovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
tanků	tank	k1gInPc2
úrovně	úroveň	k1gFnSc2
10	#num#	k4
je	být	k5eAaImIp3nS
však	však	k9
od	od	k7c2
počátku	počátek	k1gInSc2
elitních	elitní	k2eAgInPc2d1
(	(	kIx(
<g/>
všechny	všechen	k3xTgInPc1
moduly	modul	k1gInPc1
vyzkoumány	vyzkoumán	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mapy	mapa	k1gFnPc1
</s>
<s>
Bitvy	bitva	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
40	#num#	k4
různých	různý	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
připomínají	připomínat	k5eAaImIp3nP
skutečné	skutečný	k2eAgFnPc1d1
scenérie	scenérie	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
místa	místo	k1gNnPc4
bitev	bitva	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Prochorovka	Prochorovka	k1gFnSc1
či	či	k8xC
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Cílem	cíl	k1gInSc7
každé	každý	k3xTgFnSc2
bitvy	bitva	k1gFnSc2
je	být	k5eAaImIp3nS
vyhrát	vyhrát	k5eAaPmF
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
lze	lze	k6eAd1
docílit	docílit	k5eAaPmF
buďto	buďto	k8xC
zničením	zničení	k1gNnSc7
všech	všecek	k3xTgInPc2
nepřátelských	přátelský	k2eNgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
obsazením	obsazení	k1gNnSc7
obsazovacího	obsazovací	k2eAgInSc2d1
bodu	bod	k1gInSc2
v	v	k7c6
nepřátelské	přátelský	k2eNgFnSc6d1
základně	základna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
0.7	0.7	k4
<g/>
.4	.4	k4
přinesla	přinést	k5eAaPmAgFnS
dva	dva	k4xCgInPc4
nové	nový	k2eAgInPc4d1
herní	herní	k2eAgInPc4d1
módy	mód	k1gInPc4
<g/>
:	:	kIx,
Assault	Assault	k1gMnSc1
a	a	k8xC
Encounter	Encounter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
módu	mód	k1gInSc6
Assault	Assaulta	k1gFnPc2
(	(	kIx(
<g/>
útok	útok	k1gInSc1
<g/>
)	)	kIx)
kde	kde	k6eAd1
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
dobývá	dobývat	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
stačí	stačit	k5eAaBmIp3nP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
bránit	bránit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
módu	mód	k1gInSc6
Encounter	Encounter	k1gInSc1
(	(	kIx(
<g/>
střetnutí	střetnutí	k1gNnSc1
<g/>
)	)	kIx)
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
soupeří	soupeřit	k5eAaImIp3nP
o	o	k7c4
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
na	na	k7c6
mapě	mapa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
módy	mód	k1gInPc1
jsou	být	k5eAaImIp3nP
aktivovány	aktivovat	k5eAaBmNgInP
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
mapy	mapa	k1gFnPc4
podle	podle	k7c2
jejich	jejich	k3xOp3gFnSc2
charakteristiky	charakteristika	k1gFnSc2
a	a	k8xC
hráči	hráč	k1gMnPc1
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gInPc4
dostávají	dostávat	k5eAaImIp3nP
náhodně	náhodně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
tyto	tento	k3xDgInPc1
mody	modus	k1gInPc1
map	mapa	k1gFnPc2
se	se	k3xPyFc4
dají	dát	k5eAaPmIp3nP
vypnout	vypnout	k5eAaPmF
v	v	k7c6
nastavení	nastavení	k1gNnSc6
hry	hra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Režimy	režim	k1gInPc1
bitev	bitva	k1gFnPc2
</s>
<s>
Náhodné	náhodný	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
</s>
<s>
Bitvy	bitva	k1gFnPc1
ve	v	k7c6
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
členné	členný	k2eAgFnSc6d1
četě	četa	k1gFnSc6
v	v	k7c6
Náhodných	náhodný	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
</s>
<s>
Hodnocené	hodnocený	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
</s>
<s>
Týmové	týmový	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
(	(	kIx(
<g/>
odebrány	odebrán	k2eAgFnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Bitvy	bitva	k1gFnPc1
rot	rota	k1gFnPc2
(	(	kIx(
<g/>
odebrány	odebrán	k2eAgFnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
</s>
<s>
Bitvy	bitva	k1gFnPc1
opevnění	opevnění	k1gNnSc2
</s>
<s>
Historické	historický	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
(	(	kIx(
<g/>
byly	být	k5eAaImAgFnP
brzy	brzy	k6eAd1
po	po	k7c6
vydání	vydání	k1gNnSc6
odebrány	odebrat	k5eAaPmNgInP
kvůli	kvůli	k7c3
špatnému	špatný	k2eAgNnSc3d1
vyvážení	vyvážení	k1gNnSc3
<g/>
)	)	kIx)
</s>
<s>
Velké	velký	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
(	(	kIx(
<g/>
bitvy	bitva	k1gFnSc2
30	#num#	k4
vs	vs	k?
30	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
pro	pro	k7c4
tanky	tank	k1gInPc4
IX	IX	kA
<g/>
.	.	kIx.
a	a	k8xC
X.	X.	kA
úrovně	úroveň	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Frontová	frontový	k2eAgFnSc1d1
linie	linie	k1gFnSc1
</s>
<s>
V	v	k7c6
náhodných	náhodný	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
proti	proti	k7c3
sobě	se	k3xPyFc3
stojí	stát	k5eAaImIp3nP
15	#num#	k4
hráčů	hráč	k1gMnPc2
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
typ	typ	k1gInSc1
bitvy	bitva	k1gFnSc2
je	být	k5eAaImIp3nS
nejhranější	hraný	k2eAgNnSc1d3
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
až	až	k9
3	#num#	k4
hráčům	hráč	k1gMnPc3
vstoupit	vstoupit	k5eAaPmF
do	do	k7c2
jedné	jeden	k4xCgFnSc2
samé	samý	k3xTgFnSc2
bitvy	bitva	k1gFnSc2
pomocí	pomocí	k7c2
čety	četa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
3	#num#	k4
typy	typ	k1gInPc7
náhodných	náhodný	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
<g/>
:	:	kIx,
standardní	standardní	k2eAgFnSc1d1
bitva	bitva	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
oba	dva	k4xCgInPc1
týmy	tým	k1gInPc1
mají	mít	k5eAaImIp3nP
svou	svůj	k3xOyFgFnSc4
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střetnutí	střetnutí	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
vybraných	vybraný	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
pouze	pouze	k6eAd1
jedna	jeden	k4xCgFnSc1
neutrální	neutrální	k2eAgFnSc1d1
základna	základna	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
mohou	moct	k5eAaImIp3nP
obsadit	obsadit	k5eAaPmF
oba	dva	k4xCgInPc1
týmy	tým	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útok	útok	k1gInSc1
<g/>
/	/	kIx~
<g/>
Obrana	obrana	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc4
základnu	základna	k1gFnSc4
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
z	z	k7c2
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
tým	tým	k1gInSc1
musí	muset	k5eAaImIp3nS
pak	pak	k6eAd1
svou	svůj	k3xOyFgFnSc4
základnu	základna	k1gFnSc4
buď	buď	k8xC
ubránit	ubránit	k5eAaPmF
<g/>
,	,	kIx,
nebo	nebo	k8xC
obsadit	obsadit	k5eAaPmF
nepřátelskou	přátelský	k2eNgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
týmových	týmový	k2eAgFnPc6d1
bitvách	bitva	k1gFnPc6
proti	proti	k7c3
sobě	se	k3xPyFc3
stojí	stát	k5eAaImIp3nP
5	#num#	k4
až	až	k9
7	#num#	k4
tanků	tank	k1gInPc2
na	na	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
maximálně	maximálně	k6eAd1
však	však	k9
tier	tier	k1gMnSc1
VII	VII	kA
x	x	k?
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
jsou	být	k5eAaImIp3nP
předposledním	předposlední	k2eAgInSc7d1
přidaným	přidaný	k2eAgInSc7d1
typem	typ	k1gInSc7
(	(	kIx(
<g/>
v	v	k7c6
patchi	patch	k1gInSc6
9.0	9.0	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Utkávaly	utkávat	k5eAaImAgInP
se	se	k3xPyFc4
zde	zde	k6eAd1
pouze	pouze	k6eAd1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
opravdu	opravdu	k6eAd1
bojovaly	bojovat	k5eAaImAgFnP
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historických	historický	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
účastnit	účastnit	k5eAaImF
pouze	pouze	k6eAd1
sovětské	sovětský	k2eAgInPc1d1
<g/>
,	,	kIx,
německé	německý	k2eAgInPc1d1
a	a	k8xC
americké	americký	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Hodnocené	hodnocený	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
jsou	být	k5eAaImIp3nP
spuštěny	spuštěn	k2eAgFnPc1d1
pouze	pouze	k6eAd1
na	na	k7c4
určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
a	a	k8xC
pouze	pouze	k6eAd1
pro	pro	k7c4
vozidla	vozidlo	k1gNnPc4
10	#num#	k4
úrovně	úroveň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
získávat	získávat	k5eAaImF
„	„	k?
<g/>
prýmky	prýmka	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
body	bod	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
díky	díky	k7c3
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
dostává	dostávat	k5eAaImIp3nS
na	na	k7c4
vyšší	vysoký	k2eAgFnPc4d2
úrovně	úroveň	k1gFnPc4
v	v	k7c6
hodnocení	hodnocení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
jsou	být	k5eAaImIp3nP
určeny	určit	k5eAaPmNgFnP
pro	pro	k7c4
zkušenější	zkušený	k2eAgMnPc4d2
hráče	hráč	k1gMnPc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
tyto	tento	k3xDgInPc1
3	#num#	k4
typy	typ	k1gInPc7
<g/>
:	:	kIx,
Klanové	klanový	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
CW	CW	kA
<g/>
)	)	kIx)
o	o	k7c6
provincii	provincie	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
ochrana	ochrana	k1gFnSc1
Klanového	klanový	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
(	(	kIx(
<g/>
již	již	k6eAd1
není	být	k5eNaImIp3nS
součástí	součást	k1gFnSc7
hry	hra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Turnaje	turnaj	k1gInPc1
(	(	kIx(
<g/>
např.	např.	kA
Fun	Fun	k1gMnSc1
Cap	cap	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
samostatně	samostatně	k6eAd1
sestavené	sestavený	k2eAgInPc1d1
týmy	tým	k1gInPc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
typem	typ	k1gInSc7
Speciálních	speciální	k2eAgFnPc2d1
bitev	bitva	k1gFnPc2
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
Utkání	utkání	k1gNnSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
neboli	neboli	k8xC
„	„	k?
<g/>
Šampionáty	šampionát	k1gInPc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
určené	určený	k2eAgFnPc4d1
pro	pro	k7c4
setkání	setkání	k1gNnPc4
profesionálních	profesionální	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
World	World	k1gMnSc1
of	of	k?
Tanks	Tanks	k1gInSc1
a	a	k8xC
jsou	být	k5eAaImIp3nP
o	o	k7c4
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
zlaťáků	zlaťák	k1gInPc2
či	či	k8xC
o	o	k7c4
faktické	faktický	k2eAgInPc4d1
peníze	peníz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šampionáty	šampionát	k1gInPc4
a	a	k8xC
turnaje	turnaj	k1gInPc4
pořádá	pořádat	k5eAaImIp3nS
Wargaming	Wargaming	k1gInSc1
<g/>
,	,	kIx,
CW	CW	kA
pořádá	pořádat	k5eAaImIp3nS
klan	klan	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
je	být	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CW	CW	kA
může	moct	k5eAaImIp3nS
WG	WG	kA
pouze	pouze	k6eAd1
pozastavit	pozastavit	k5eAaPmF
a	a	k8xC
vytvořit	vytvořit	k5eAaPmF
předem	předem	k6eAd1
oznamovanou	oznamovaný	k2eAgFnSc4d1
2	#num#	k4
<g/>
.	.	kIx.
mapu	mapa	k1gFnSc4
kvůli	kvůli	k7c3
zvláštní	zvláštní	k2eAgFnSc3d1
WoT	WoT	k1gFnSc3
akci	akce	k1gFnSc3
či	či	k8xC
Tažení	tažení	k1gNnSc3
klanových	klanový	k2eAgFnPc2d1
válek	válka	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
3	#num#	k4
<g/>
.	.	kIx.
tažení	tažení	k1gNnSc4
–	–	k?
Svět	svět	k1gInSc1
v	v	k7c6
plamenech	plamen	k1gInPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
se	se	k3xPyFc4
ve	v	k7c6
WoT	WoT	k1gFnSc6
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
garážích	garáž	k1gFnPc6
jednou	jednou	k6eAd1
za	za	k7c4
čas	čas	k1gInSc4
speciální	speciální	k2eAgInPc4d1
provizorní	provizorní	k2eAgInPc4d1
tanky	tank	k1gInPc4
kvůli	kvůli	k7c3
zvláštním	zvláštní	k2eAgFnPc3d1
akcím	akce	k1gFnPc3
(	(	kIx(
<g/>
např.	např.	kA
dělostřelectvo	dělostřelectvo	k1gNnSc1
moždíř	moždíř	k1gInSc1
tier	tier	k1gMnSc1
1	#num#	k4
-	-	kIx~
Karl	Karla	k1gFnPc2
jako	jako	k8xC,k8xS
upozornění	upozornění	k1gNnSc2
na	na	k7c4
Update	update	k1gInSc4
9.0	9.0	k4
-	-	kIx~
zlepšení	zlepšení	k1gNnSc3
grafiky	grafika	k1gFnSc2
či	či	k8xC
od	od	k7c2
poloviny	polovina	k1gFnSc2
června	červen	k1gInSc2
2014	#num#	k4
T	T	kA
-	-	kIx~
62	#num#	k4
A	a	k9
Sport	sport	k1gInSc1
díky	díky	k7c3
mistrovství	mistrovství	k1gNnSc3
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
)	)	kIx)
včetně	včetně	k7c2
zvláštních	zvláštní	k2eAgFnPc2d1
vlastních	vlastní	k2eAgFnPc2d1
map	mapa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
verzi	verze	k1gFnSc6
9.3	9.3	k4
přibyl	přibýt	k5eAaPmAgInS
závodní	závodní	k2eAgInSc1d1
mód	mód	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
hráči	hráč	k1gMnPc1
dostali	dostat	k5eAaPmAgMnP
upravenou	upravený	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
tanku	tank	k1gInSc2
chaffee	chaffe	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
M24	M24	k1gFnSc1
Supersport	supersport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jezdilo	jezdit	k5eAaImAgNnS
se	se	k3xPyFc4
na	na	k7c4
závodní	závodní	k2eAgFnPc4d1
trati	trať	k1gFnPc4
vytvořené	vytvořený	k2eAgFnPc4d1
z	z	k7c2
mapy	mapa	k1gFnSc2
„	„	k?
<g/>
Přístav	přístav	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
bitvy	bitva	k1gFnSc2
byly	být	k5eAaImAgInP
3	#num#	k4
vs	vs	k?
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
vyhrál	vyhrát	k5eAaPmAgInS
tým	tým	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
jako	jako	k8xC,k8xS
první	první	k4xOgMnSc1
obsadil	obsadit	k5eAaPmAgMnS
základnu	základna	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
zničil	zničit	k5eAaPmAgInS
nepřátelská	přátelský	k2eNgNnPc4d1
vozidla	vozidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
další	další	k2eAgFnSc6d1
aktualizaci	aktualizace	k1gFnSc6
zmizel	zmizet	k5eAaPmAgMnS
mód	mód	k1gInSc4
i	i	k8xC
tank	tank	k1gInSc4
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
jen	jen	k9
posádka	posádka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Frontová	frontový	k2eAgFnSc1d1
linie	linie	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
epizody	epizoda	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
probíhají	probíhat	k5eAaImIp3nP
vždy	vždy	k6eAd1
jeden	jeden	k4xCgInSc4
kalendářní	kalendářní	k2eAgInSc4d1
týden	týden	k1gInSc4
každého	každý	k3xTgInSc2
kalendářního	kalendářní	k2eAgInSc2d1
měsíce	měsíc	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bitvy	bitva	k1gFnPc1
probíhají	probíhat	k5eAaImIp3nP
v	v	k7c6
režimu	režim	k1gInSc6
30	#num#	k4
na	na	k7c4
30	#num#	k4
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
tým	tým	k1gInSc1
útočí	útočit	k5eAaImIp3nS
a	a	k8xC
jejím	její	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
dobít	dobít	k5eAaPmF
území	území	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
brání	bránit	k5eAaImIp3nS
tým	tým	k1gInSc1
druhý	druhý	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatou	podstata	k1gFnSc7
tohoto	tento	k3xDgInSc2
režimu	režim	k1gInSc2
je	být	k5eAaImIp3nS
získávání	získávání	k1gNnSc1
postupu	postup	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c4
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
získávají	získávat	k5eAaImIp3nP
speciální	speciální	k2eAgFnPc4d1
odměny	odměna	k1gFnPc4
a	a	k8xC
možnost	možnost	k1gFnSc4
vyjet	vyjet	k5eAaPmF
speciální	speciální	k2eAgInSc4d1
prémiový	prémiový	k2eAgInSc4d1
tank	tank	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Statistiky	statistika	k1gFnPc1
</s>
<s>
Hra	hra	k1gFnSc1
zaznamenává	zaznamenávat	k5eAaImIp3nS
úspěchy	úspěch	k1gInPc4
každého	každý	k3xTgMnSc4
hráče	hráč	k1gMnSc4
ve	v	k7c6
formě	forma	k1gFnSc6
statistik	statistika	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
počet	počet	k1gInSc1
odehraných	odehraný	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
počet	počet	k1gInSc4
zničených	zničený	k2eAgNnPc2d1
nepřátelských	přátelský	k2eNgNnPc2d1
vozidel	vozidlo	k1gNnPc2
nebo	nebo	k8xC
přesnost	přesnost	k1gFnSc1
střelby	střelba	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
těchto	tento	k3xDgFnPc2
statistik	statistika	k1gFnPc2
se	se	k3xPyFc4
poté	poté	k6eAd1
vypočítá	vypočítat	k5eAaPmIp3nS
pomocí	pomocí	k7c2
vzorců	vzorec	k1gInPc2
celkové	celkový	k2eAgNnSc4d1
hodnocení	hodnocení	k1gNnSc4
daného	daný	k2eAgMnSc2d1
hráče	hráč	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
mimořádně	mimořádně	k6eAd1
úspěšnou	úspěšný	k2eAgFnSc4d1
bitvu	bitva	k1gFnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
hráč	hráč	k1gMnSc1
odměněn	odměněn	k2eAgMnSc1d1
medailemi	medaile	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nP
do	do	k7c2
určitých	určitý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
např.	např.	kA
HRDINOVÉ	Hrdinová	k1gFnSc2
BITVY	bitva	k1gFnSc2
(	(	kIx(
<g/>
Top	topit	k5eAaImRp2nS
Gun	Gun	k1gMnSc1
(	(	kIx(
<g/>
válečník	válečník	k1gMnSc1
<g/>
)	)	kIx)
za	za	k7c4
6	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
zničených	zničený	k2eAgInPc2d1
tanků	tank	k1gInPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
Invader	Invader	k1gMnSc1
(	(	kIx(
<g/>
útočník	útočník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
sám	sám	k3xTgInSc4
obsadit	obsadit	k5eAaPmF
alespoň	alespoň	k9
80	#num#	k4
%	%	kIx~
obsazovacího	obsazovací	k2eAgInSc2d1
bodu	bod	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
mimořádně	mimořádně	k6eAd1
úspěšné	úspěšný	k2eAgFnSc2d1
bitvy	bitva	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
hráč	hráč	k1gMnSc1
oceněn	oceněn	k2eAgMnSc1d1
medailí	medaile	k1gFnPc2
nesoucí	nesoucí	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
skutečných	skutečný	k2eAgNnPc2d1
tankových	tankový	k2eAgNnPc2d1
es	es	k1gNnSc2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
např.	např.	kA
Radley-Waltersova	Radley-Waltersův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
udělována	udělovat	k5eAaImNgFnS
za	za	k7c4
zničení	zničení	k1gNnSc4
8	#num#	k4
nebo	nebo	k8xC
9	#num#	k4
vozidel	vozidlo	k1gNnPc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
náhodné	náhodný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jsou	být	k5eAaImIp3nP
ocenění	ocenění	k1gNnSc4
„	„	k?
<g/>
Vzorný	vzorný	k2eAgMnSc1d1
tankista	tankista	k1gMnSc1
<g/>
“	“	k?
ukazující	ukazující	k2eAgFnSc4d1
zdatnost	zdatnost	k1gFnSc4
hráče	hráč	k1gMnSc2
se	s	k7c7
specifickým	specifický	k2eAgNnSc7d1
vozidlem	vozidlo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
vítězné	vítězný	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
přibývají	přibývat	k5eAaImIp3nP
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
hráč	hráč	k1gMnSc1
s	s	k7c7
daným	daný	k2eAgInSc7d1
strojem	stroj	k1gInSc7
v	v	k7c6
uděleném	udělený	k2eAgNnSc6d1
průměrném	průměrný	k2eAgNnSc6d1
kombinovaném	kombinovaný	k2eAgNnSc6d1
(	(	kIx(
<g/>
poškození	poškození	k1gNnSc6
+	+	kIx~
asistované	asistovaný	k2eAgNnSc1d1
poškození	poškození	k1gNnSc1
<g/>
)	)	kIx)
poškození	poškození	k1gNnSc1
způsobeným	způsobený	k2eAgNnSc7d1
nepříteli	nepřítel	k1gMnSc3
lepší	dobrý	k2eAgNnSc4d2
než	než	k8xS
určité	určitý	k2eAgNnSc4d1
procento	procento	k1gNnSc4
hráčů	hráč	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dostupná	dostupný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
</s>
<s>
Ve	v	k7c6
hře	hra	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
víc	hodně	k6eAd2
než	než	k8xS
600	#num#	k4
obrněných	obrněný	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
ze	z	k7c2
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
,	,	kIx,
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
,	,	kIx,
Švédska	Švédsko	k1gNnSc2
<g/>
,	,	kIx,
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc2
a	a	k8xC
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrněná	obrněný	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
vozidel	vozidlo	k1gNnPc2
zařazených	zařazený	k2eAgNnPc2d1
do	do	k7c2
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
prošlých	prošlý	k2eAgInPc2d1
i	i	k9
několika	několik	k4yIc7
vojenskými	vojenský	k2eAgInPc7d1
konflikty	konflikt	k1gInPc7
přes	přes	k7c4
nikdy	nikdy	k6eAd1
nenasazené	nasazený	k2eNgInPc4d1
prototypy	prototyp	k1gInPc4
až	až	k9
po	po	k7c4
projekty	projekt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
nikdy	nikdy	k6eAd1
neopustily	opustit	k5eNaPmAgInP
rýsovací	rýsovací	k2eAgInPc1d1
prkna	prkno	k1gNnSc2
svých	svůj	k3xOyFgMnPc2
tvůrců	tvůrce	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c4
World	World	k1gInSc4
of	of	k?
Tanks	Tanksa	k1gFnPc2
rozděleny	rozdělit	k5eAaPmNgInP
podle	podle	k7c2
tříd	třída	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
tier	tier	k1gInSc1
<g/>
,	,	kIx,
často	často	k6eAd1
chybně	chybně	k6eAd1
označováno	označovat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
level	level	k1gInSc1
<g/>
)	)	kIx)
kterých	který	k3yRgInPc2,k3yIgInPc2,k3yQgInPc2
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
10	#num#	k4
<g/>
,	,	kIx,
každý	každý	k3xTgInSc1
tank	tank	k1gInSc1
je	být	k5eAaImIp3nS
něčím	něco	k3yInSc7
specifický	specifický	k2eAgInSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
určitou	určitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
celkem	celkem	k6eAd1
rychlé	rychlý	k2eAgFnPc1d1
a	a	k8xC
dobře	dobře	k6eAd1
opancéřované	opancéřovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnPc1
děla	dělo	k1gNnPc1
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
malou	malý	k2eAgFnSc7d1
přesností	přesnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Německý	německý	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
silně	silně	k6eAd1
opancéřované	opancéřovaný	k2eAgFnPc1d1
a	a	k8xC
přesné	přesný	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
pomalejší	pomalý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Americký	americký	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
kompromisem	kompromis	k1gInSc7
<g/>
,	,	kIx,
vyznačují	vyznačovat	k5eAaImIp3nP
se	se	k3xPyFc4
především	především	k9
vyšší	vysoký	k2eAgFnSc7d2
rychlostí	rychlost	k1gFnSc7
střelby	střelba	k1gFnSc2
a	a	k8xC
slušnými	slušný	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
menší	malý	k2eAgInPc1d2
<g/>
,	,	kIx,
s	s	k7c7
minimem	minimum	k1gNnSc7
pancéřování	pancéřování	k1gNnSc2
<g/>
,	,	kIx,
pohyblivé	pohyblivý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnPc1
děla	dělo	k1gNnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c6
vyšších	vysoký	k2eAgInPc6d2
tierech	tier	k1gInPc6
nabíjecí	nabíjecí	k2eAgInSc1d1
automat	automat	k1gInSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
„	„	k?
<g/>
zásobník	zásobník	k1gMnSc1
<g/>
“	“	k?
umožňující	umožňující	k2eAgNnSc1d1
vystřelit	vystřelit	k5eAaPmF
opakovaně	opakovaně	k6eAd1
v	v	k7c6
krátkém	krátký	k2eAgInSc6d1
sledu	sled	k1gInSc6
<g/>
,	,	kIx,
tanku	tank	k1gInSc6
s	s	k7c7
tímto	tento	k3xDgInSc7
typem	typ	k1gInSc7
dělem	dělo	k1gNnSc7
hráči	hráč	k1gMnPc1
často	často	k6eAd1
označují	označovat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Autoloader	Autoloader	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
„	„	k?
<g/>
automatický	automatický	k2eAgInSc4d1
nabíječ	nabíječ	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Do	do	k7c2
francouzské	francouzský	k2eAgFnSc2d1
linii	linie	k1gFnSc6
přidány	přidán	k2eAgFnPc4d1
kolové	kolová	k1gFnPc4
vozidla	vozidlo	k1gNnSc2
v	v	k7c6
aktualizaci	aktualizace	k1gFnSc6
1.4	1.4	k4
.	.	kIx.
</s>
<s>
Italský	italský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
od	od	k7c2
8	#num#	k4
<g/>
.	.	kIx.
tieru	tier	k1gInSc2
mají	mít	k5eAaImIp3nP
nový	nový	k2eAgInSc4d1
systém	systém	k1gInSc4
automatického	automatický	k2eAgNnSc2d1
dobíjení	dobíjení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělí	dělit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
dva	dva	k4xCgInPc4
typy	typ	k1gInPc4
<g/>
:	:	kIx,
pro	pro	k7c4
těžké	těžký	k2eAgInPc4d1
tanky	tank	k1gInPc4
a	a	k8xC
pro	pro	k7c4
střední	střední	k2eAgInPc4d1
tanky	tank	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
velmi	velmi	k6eAd1
málo	málo	k4c4
niancí	niance	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobný	podobný	k2eAgInSc1d1
již	již	k6eAd1
zmíněnému	zmíněný	k2eAgMnSc3d1
„	„	k?
<g/>
autoloaderu	autoloadero	k1gNnSc6
<g/>
“	“	k?
ale	ale	k8xC
nenabíjí	nabíjet	k5eNaImIp3nS
celý	celý	k2eAgInSc1d1
zásobník	zásobník	k1gInSc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
střely	střela	k1gFnPc4
v	v	k7c6
zásobníku	zásobník	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomuto	tento	k3xDgInSc3
systému	systém	k1gInSc3
mohou	moct	k5eAaImIp3nP
Italské	italský	k2eAgInPc1d1
stroje	stroj	k1gInPc1
zastupovat	zastupovat	k5eAaImF
tanky	tank	k1gInPc4
s	s	k7c7
normálním	normální	k2eAgNnSc7d1
dobíjením	dobíjení	k1gNnSc7
i	i	k9
ty	ten	k3xDgFnPc1
se	se	k3xPyFc4
zásobníkem	zásobník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Britský	britský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
kompromisem	kompromis	k1gInSc7
<g/>
,	,	kIx,
vyznačují	vyznačovat	k5eAaImIp3nP
se	se	k3xPyFc4
především	především	k9
vyšší	vysoký	k2eAgFnSc7d2
rychlostí	rychlost	k1gFnSc7
střelby	střelba	k1gFnSc2
a	a	k8xC
slušnými	slušný	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
většině	většina	k1gFnSc6
parametrů	parametr	k1gInPc2
podobné	podobný	k2eAgInPc4d1
sovětským	sovětský	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Střední	střední	k2eAgInPc1d1
tanky	tank	k1gInPc1
vynikají	vynikat	k5eAaImIp3nP
silnými	silný	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
<g/>
,	,	kIx,
slabým	slabý	k2eAgInSc7d1
pancířem	pancíř	k1gInSc7
a	a	k8xC
průměrnou	průměrný	k2eAgFnSc7d1
pohyblivostí	pohyblivost	k1gFnSc7
<g/>
,	,	kIx,
ty	ten	k3xDgInPc1
těžké	těžký	k2eAgNnSc1d1
silným	silný	k2eAgInSc7d1
pancířem	pancíř	k1gInSc7
<g/>
,	,	kIx,
silným	silný	k2eAgNnSc7d1
dělem	dělo	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
slabší	slabý	k2eAgFnSc4d2
pohyblivost	pohyblivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Československý	československý	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
mají	mít	k5eAaImIp3nP
velkou	velký	k2eAgFnSc4d1
pohyblivost	pohyblivost	k1gFnSc4
a	a	k8xC
silný	silný	k2eAgInSc4d1
kanón	kanón	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
slabší	slabý	k2eAgNnSc4d2
opancéřování	opancéřování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Švédský	švédský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Tanky	tank	k1gInPc1
do	do	k7c2
7	#num#	k4
<g/>
.	.	kIx.
tieru	tier	k1gInSc2
mají	mít	k5eAaImIp3nP
průměrný	průměrný	k2eAgInSc4d1
kanón	kanón	k1gInSc4
s	s	k7c7
velice	velice	k6eAd1
dobrou	dobrý	k2eAgFnSc7d1
depresí	deprese	k1gFnSc7
(	(	kIx(
<g/>
sklopením	sklopení	k1gNnSc7
děla	dělo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průměrnou	průměrný	k2eAgFnSc4d1
pohyblivost	pohyblivost	k1gFnSc4
a	a	k8xC
průměrné	průměrný	k2eAgNnSc4d1
opancéřování	opancéřování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
tieru	tier	k1gInSc2
VIII	VIII	kA
začínají	začínat	k5eAaImIp3nP
těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
se	s	k7c7
zásobníky	zásobník	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
boje	boj	k1gInSc2
mají	mít	k5eAaImIp3nP
švédské	švédský	k2eAgInPc1d1
stíhače	stíhač	k1gMnSc2
tanků	tank	k1gInPc2
-	-	kIx~
2	#num#	k4
režimy	režim	k1gInPc4
,	,	kIx,
bojový	bojový	k2eAgInSc4d1
a	a	k8xC
<g/>
,,	,,	k?
<g/>
cestovní	cestovní	k2eAgFnSc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Polský	polský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vývojový	vývojový	k2eAgInSc1d1
strom	strom	k1gInSc1
</s>
<s>
Společný	společný	k2eAgInSc1d1
rys	rys	k1gInSc1
polských	polský	k2eAgMnPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
začínají	začínat	k5eAaImIp3nP
u	u	k7c2
lehkých	lehký	k2eAgMnPc2d1
<g/>
,	,	kIx,
pokračují	pokračovat	k5eAaImIp3nP
přes	přes	k7c4
střední	střední	k2eAgFnSc4d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
větví	větvit	k5eAaImIp3nS
na	na	k7c4
těžké	těžký	k2eAgInPc4d1
tanky	tank	k1gInPc4
vysokých	vysoký	k2eAgFnPc2d1
úrovní	úroveň	k1gFnPc2
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
poškozením	poškození	k1gNnSc7
za	za	k7c4
jeden	jeden	k4xCgInSc4
výstřel	výstřel	k1gInSc4
a	a	k8xC
na	na	k7c4
střední	střední	k2eAgInPc4d1
tanky	tank	k1gInPc4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
jsou	být	k5eAaImIp3nP
vcelku	vcelku	k6eAd1
podobné	podobný	k2eAgInPc1d1
těm	ten	k3xDgInPc3
sovětským	sovětský	k2eAgInPc3d1
<g/>
,	,	kIx,
až	až	k9
na	na	k7c4
tank	tank	k1gInSc4
10	#num#	k4
tieru	tier	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
unikátní	unikátní	k2eAgFnSc4d1
vlastnost	vlastnost	k1gFnSc4
zapnutelného	zapnutelný	k2eAgInSc2d1
režimu	režim	k1gInSc2
„	„	k?
<g/>
turbo	turba	k1gFnSc5
<g/>
“	“	k?
při	při	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
dokáže	dokázat	k5eAaPmIp3nS
tank	tank	k1gInSc4
vyvinout	vyvinout	k5eAaPmF
velice	velice	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
na	na	k7c4
úkor	úkor	k1gInSc4
přesnosti	přesnost	k1gFnSc2
děla	dělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
tanků	tank	k1gInPc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobná	podobný	k2eAgFnSc1d1
ruské	ruský	k2eAgFnSc3d1
větvi	větev	k1gFnSc3
a	a	k8xC
technoligickému	technoligický	k2eAgInSc3d1
stromu	strom	k1gInSc3
<g/>
:	:	kIx,
nízká	nízký	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
palby	palba	k1gFnSc2
<g/>
,	,	kIx,
vysoká	vysoký	k2eAgFnSc1d1
alfa	alfa	k1gFnSc1
<g/>
,	,	kIx,
dobré	dobrý	k2eAgNnSc1d1
brnění	brnění	k1gNnSc1
a	a	k8xC
dobrá	dobrý	k2eAgFnSc1d1
mobilita	mobilita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kategorie	kategorie	k1gFnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
Tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
rozdělený	rozdělený	k2eAgInSc4d1
do	do	k7c2
5	#num#	k4
tříd	třída	k1gFnPc2
-	-	kIx~
Lehké	Lehké	k2eAgInPc1d1
tanky	tank	k1gInPc1
+	+	kIx~
Kolová	kolový	k2eAgNnPc1d1
vozidla	vozidlo	k1gNnPc1
<g/>
,	,	kIx,
Střední	střední	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
Těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
Stíhače	stíhač	k1gMnSc2
tanků	tank	k1gInPc2
a	a	k8xC
Dělostřelectvo	dělostřelectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
tato	tento	k3xDgFnSc1
třída	třída	k1gFnSc1
má	mít	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
specifickou	specifický	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
na	na	k7c6
bojišti	bojiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Lehké	Lehké	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Zastupují	zastupovat	k5eAaImIp3nP
roli	role	k1gFnSc4
scoutu	scout	k1gInSc2
(	(	kIx(
<g/>
průzkumníků	průzkumník	k1gMnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
přepadové	přepadový	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
pomalých	pomalý	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyznačují	vyznačovat	k5eAaImIp3nP
se	se	k3xPyFc4
malou	malý	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
<g/>
,	,	kIx,
pohyblivostí	pohyblivost	k1gFnSc7
a	a	k8xC
dobrým	dobrý	k2eAgNnSc7d1
maskováním	maskování	k1gNnSc7
společně	společně	k6eAd1
s	s	k7c7
dohledem	dohled	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Střední	střední	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Víceúčelové	víceúčelový	k2eAgInPc1d1
<g/>
,	,	kIx,
všestranné	všestranný	k2eAgInPc1d1
tanky	tank	k1gInPc1
s	s	k7c7
vyváženými	vyvážený	k2eAgFnPc7d1
charakteristikami	charakteristika	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
ve	v	k7c6
hře	hra	k1gFnSc6
převzít	převzít	k5eAaPmF
mnoho	mnoho	k6eAd1
různých	různý	k2eAgFnPc2d1
rolí	role	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnPc4
vlastnosti	vlastnost	k1gFnPc4
ve	v	k7c6
velikosti	velikost	k1gFnSc6
<g/>
,	,	kIx,
palebné	palebný	k2eAgFnSc6d1
síle	síla	k1gFnSc6
<g/>
,	,	kIx,
manévrovatelnosti	manévrovatelnost	k1gFnSc6
a	a	k8xC
v	v	k7c6
pancíři	pancíř	k1gInSc6
leží	ležet	k5eAaImIp3nS
někde	někde	k6eAd1
mezi	mezi	k7c7
těžkými	těžký	k2eAgInPc7d1
tanky	tank	k1gInPc7
a	a	k8xC
lehkými	lehký	k2eAgInPc7d1
tanky	tank	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
velké	velký	k2eAgInPc1d1
<g/>
,	,	kIx,
těžké	těžký	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
často	často	k6eAd1
působí	působit	k5eAaImIp3nP
v	v	k7c6
první	první	k4xOgFnSc6
linii	linie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
jsou	být	k5eAaImIp3nP
nejlépe	dobře	k6eAd3
obrněnými	obrněný	k2eAgNnPc7d1
vozidly	vozidlo	k1gNnPc7
ve	v	k7c6
hře	hra	k1gFnSc6
a	a	k8xC
obvykle	obvykle	k6eAd1
mají	mít	k5eAaImIp3nP
větší	veliký	k2eAgFnSc4d2
palební	palební	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
než	než	k8xS
lehké	lehký	k2eAgFnPc1d1
a	a	k8xC
střední	střední	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
kompenzováno	kompenzován	k2eAgNnSc1d1
jejich	jejich	k3xOp3gNnSc4
menší	malý	k2eAgNnSc4d2
pohyblivostí	pohyblivost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Stíhače	stíhač	k1gMnSc2
tanků	tank	k1gInPc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
specializované	specializovaný	k2eAgInPc1d1
obrněné	obrněný	k2eAgInPc1d1
bojové	bojový	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
,	,	kIx,
určené	určený	k2eAgFnPc1d1
převážně	převážně	k6eAd1
pro	pro	k7c4
likvidaci	likvidace	k1gFnSc4
silně	silně	k6eAd1
obrněných	obrněný	k2eAgInPc2d1
tanků	tank	k1gInPc2
díky	díky	k7c3
jejich	jejich	k3xOp3gNnPc3
silným	silný	k2eAgNnPc3d1
dělům	dělo	k1gNnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodně	hodně	k6eAd1
stíhačů	stíhač	k1gMnPc2
nemá	mít	k5eNaImIp3nS
otočnou	otočný	k2eAgFnSc4d1
věž	věž	k1gFnSc4
a	a	k8xC
dělo	dělo	k1gNnSc4
mají	mít	k5eAaImIp3nP
namontované	namontovaný	k2eAgInPc1d1
přímo	přímo	k6eAd1
na	na	k7c4
trupu	trupa	k1gFnSc4
tanku	tank	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dělostřelectvo	dělostřelectvo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
slabě	slabě	k6eAd1
opancéřované	opancéřovaný	k2eAgInPc1d1
tanky	tank	k1gInPc1
s	s	k7c7
velmi	velmi	k6eAd1
nízkým	nízký	k2eAgInSc7d1
počtem	počet	k1gInSc7
bodů	bod	k1gInPc2
výdrže	výdrž	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
poskytují	poskytovat	k5eAaImIp3nP
nepřímou	přímý	k2eNgFnSc4d1
podporu	podpora	k1gFnSc4
palbou	palba	k1gFnSc7
z	z	k7c2
velké	velký	k2eAgFnSc2d1
vzdálenosti	vzdálenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějším	důležitý	k2eAgInSc7d3
znakem	znak	k1gInSc7
dělostřelectva	dělostřelectvo	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
schopnost	schopnost	k1gFnSc4
mířit	mířit	k5eAaImF
a	a	k8xC
střílet	střílet	k5eAaImF
daleko	daleko	k6eAd1
přes	přes	k7c4
terén	terén	k1gInSc4
a	a	k8xC
překážky	překážka	k1gFnPc4
na	na	k7c6
něm	on	k3xPp3gNnSc6
a	a	k8xC
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
SPG	SPG	kA
režimu	režim	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jim	on	k3xPp3gMnPc3
umožní	umožnit	k5eAaPmIp3nS
podívat	podívat	k5eAaPmF,k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
omezenou	omezený	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
libovolně	libovolně	k6eAd1
vybrané	vybraný	k2eAgFnSc2d1
části	část	k1gFnSc2
mapy	mapa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aktualizace	aktualizace	k1gFnSc1
</s>
<s>
Hra	hra	k1gFnSc1
má	mít	k5eAaImIp3nS
stálé	stálý	k2eAgFnPc4d1
aktualizace	aktualizace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
primárně	primárně	k6eAd1
opravují	opravovat	k5eAaImIp3nP
bugy	bug	k2eAgFnPc4d1
hry	hra	k1gFnPc4
a	a	k8xC
přidávají	přidávat	k5eAaImIp3nP
nový	nový	k2eAgInSc4d1
obsah	obsah	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
aktualizací	aktualizace	k1gFnPc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stěžejní	stěžejní	k2eAgFnPc1d1
změny	změna	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
2014	#num#	k4
</s>
<s>
9.0	9.0	k4
Modely	model	k1gInPc1
HD	HD	kA
</s>
<s>
9.1	9.1	k4
Vítězné	vítězný	k2eAgInPc1d1
znaky	znak	k1gInPc1
</s>
<s>
9.4	9.4	k4
Týmové	týmový	k2eAgFnPc4d1
bitvy	bitva	k1gFnPc4
</s>
<s>
2015	#num#	k4
</s>
<s>
9.7	9.7	k4
Změny	změna	k1gFnPc4
ve	v	k7c4
vyvážení	vyvážení	k1gNnSc4
dělostřelectva	dělostřelectvo	k1gNnSc2
</s>
<s>
9.8	9.8	k4
Osobní	osobní	k2eAgFnPc4d1
zálohy	záloha	k1gFnPc4
</s>
<s>
9.10	9.10	k4
Nové	Nové	k2eAgInPc1d1
japonské	japonský	k2eAgInPc1d1
těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
</s>
<s>
9.13	9.13	k4
Nový	nový	k2eAgInSc1d1
národ	národ	k1gInSc1
Československo	Československo	k1gNnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
9.14	9.14	k4
Přepracovaný	přepracovaný	k2eAgInSc1d1
systém	systém	k1gInSc1
zvuku	zvuk	k1gInSc2
<g/>
,	,	kIx,
Vylepšená	vylepšený	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
9.17	9.17	k4
Nový	nový	k2eAgInSc1d1
národ	národ	k1gInSc1
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
9.17	9.17	k4
<g/>
.1	.1	k4
Přepracované	přepracovaný	k2eAgFnSc2d1
minimapy	minimapa	k1gFnSc2
</s>
<s>
9.18	9.18	k4
Nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
matchmakingu	matchmaking	k1gInSc2
<g/>
,	,	kIx,
Přepracovaný	přepracovaný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
hry	hra	k1gFnSc2
s	s	k7c7
dělostřelectvem	dělostřelectvo	k1gNnSc7
(	(	kIx(
<g/>
ochromení	ochromení	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lehké	Lehké	k2eAgInPc1d1
tanky	tank	k1gInPc1
X.	X.	kA
úrovně	úroveň	k1gFnSc2
<g/>
,	,	kIx,
Znovu	znovu	k6eAd1
použitelné	použitelný	k2eAgInPc1d1
spotřební	spotřební	k2eAgInPc1d1
doplňky	doplněk	k1gInPc1
</s>
<s>
9.19	9.19	k4
Nová	nový	k2eAgFnSc1d1
měna	měna	k1gFnSc1
(	(	kIx(
<g/>
bony	bona	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hodnocené	hodnocený	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
</s>
<s>
9.20	9.20	k4
Velké	velký	k2eAgFnPc1d1
bitvy	bitva	k1gFnPc1
(	(	kIx(
<g/>
30	#num#	k4
proti	proti	k7c3
30	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
</s>
<s>
1.0	1.0	k4
Grafika	grafika	k1gFnSc1
<g/>
,	,	kIx,
HD	HD	kA
Mapy	mapa	k1gFnPc1
<g/>
,	,	kIx,
Muzika	muzika	k1gFnSc1
2.0	2.0	k4
</s>
<s>
1.0	1.0	k4
<g/>
.1	.1	k4
Nový	nový	k2eAgInSc1d1
národ	národ	k1gInSc1
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
Mechanika	mechanika	k1gFnSc1
automatického	automatický	k2eAgNnSc2d1
dobíjení	dobíjení	k1gNnSc2
<g/>
,	,	kIx,
Režim	režim	k1gInSc4
Frontová	frontový	k2eAgFnSc1d1
linie	linie	k1gFnPc1
</s>
<s>
1.1	1.1	k4
Nový	nový	k2eAgInSc1d1
národ	národ	k1gInSc1
Polsko	Polsko	k1gNnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
1.4	1.4	k4
Francouzská	francouzský	k2eAgFnSc1d1
kolová	kolová	k1gFnSc1
vozidla	vozidlo	k1gNnSc2
</s>
<s>
1.5	1.5	k4
Nový	nový	k2eAgInSc4d1
prémiový	prémiový	k2eAgInSc4d1
účet	účet	k1gInSc4
</s>
<s>
1.5	1.5	k4
<g/>
.1	.1	k4
Knihy	kniha	k1gFnPc4
posádky	posádka	k1gFnSc2
</s>
<s>
1.6	1.6	k4
Střelba	střelba	k1gFnSc1
do	do	k7c2
vlastních	vlastní	k2eAgFnPc2d1
řad	řada	k1gFnPc2
deaktivována	deaktivován	k2eAgFnSc1d1
</s>
<s>
1.7	1.7	k4
Anonymizér	Anonymizér	k1gInSc1
<g/>
,	,	kIx,
směrnice	směrnice	k1gFnSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
1.7	1.7	k4
<g/>
.1	.1	k4
Sovětská	sovětský	k2eAgFnSc1d1
dvouhlavňová	dvouhlavňový	k2eAgFnSc1d1
vozidla	vozidlo	k1gNnPc1
</s>
<s>
1.8	1.8	k4
Přidány	přidán	k2eAgFnPc1d1
denní	denní	k2eAgFnPc1d1
mise	mise	k1gFnPc1
</s>
<s>
1.9	1.9	k4
Přepracování	přepracování	k1gNnPc1
všech	všecek	k3xTgInPc2
vývojových	vývojový	k2eAgInPc2d1
stromů	strom	k1gInPc2
</s>
<s>
1.9	1.9	k4
<g/>
.1	.1	k4
Přidána	přidán	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Berlín	Berlín	k1gInSc1
<g/>
,	,	kIx,
Battle	Battle	k1gFnSc1
Pass	Passa	k1gFnPc2
</s>
<s>
1.10	1.10	k4
Vybavení	vybavení	k1gNnSc1
2.0	2.0	k4
<g/>
,	,	kIx,
<g/>
Mapy	mapa	k1gFnPc1
<g/>
:	:	kIx,
Do	do	k7c2
hry	hra	k1gFnSc2
se	se	k3xPyFc4
vrátí	vrátit	k5eAaPmIp3nS
značně	značně	k6eAd1
přepracovaná	přepracovaný	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
„	„	k?
<g/>
Perlová	perlový	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
2021	#num#	k4
</s>
<s>
1.11	1.11	k4
Italské	italský	k2eAgInPc1d1
těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
,	,	kIx,
Čety	četa	k1gFnPc1
2.0	2.0	k4
<g/>
WG	WG	kA
fest	fest	k6eAd1
2017	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
poprvé	poprvé	k6eAd1
uvedeny	uvést	k5eAaPmNgFnP
informace	informace	k1gFnPc1
o	o	k7c6
aktualizaci	aktualizace	k1gFnSc6
1.0	1.0	k4
</s>
<s>
Aktualizace	aktualizace	k1gFnSc1
1.0	1.0	k4
</s>
<s>
Po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
práce	práce	k1gFnSc2
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
aktualizace	aktualizace	k1gFnSc1
na	na	k7c6
verzi	verze	k1gFnSc6
1.0	1.0	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
je	být	k5eAaImIp3nS
výměna	výměna	k1gFnSc1
grafického	grafický	k2eAgInSc2d1
enginu	engin	k1gInSc2
BigWorld	BigWorlda	k1gFnPc2
za	za	k7c4
nový	nový	k2eAgInSc4d1
Encore	Encor	k1gInSc5
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
produktem	produkt	k1gInSc7
vývojářského	vývojářský	k2eAgInSc2d1
týmu	tým	k1gInSc2
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nabízí	nabízet	k5eAaImIp3nS
mj.	mj.	kA
detailnější	detailní	k2eAgInSc1d2
destrukční	destrukční	k2eAgInSc1d1
model	model	k1gInSc1
Havok	Havok	k1gInSc1
Destruction	Destruction	k1gInSc4
<g/>
,	,	kIx,
globální	globální	k2eAgNnSc4d1
osvětlení	osvětlení	k1gNnSc4
a	a	k8xC
vodu	voda	k1gFnSc4
čeřenou	čeřený	k2eAgFnSc4d1
průjezdem	průjezd	k1gInSc7
tanků	tank	k1gInPc2
i	i	k8xC
střelbou	střelba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
získala	získat	k5eAaPmAgFnS
zcela	zcela	k6eAd1
nový	nový	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
:	:	kIx,
ostřejší	ostrý	k2eAgFnSc4d2
grafiku	grafika	k1gFnSc4
<g/>
,	,	kIx,
podrobnější	podrobný	k2eAgFnPc4d2
textury	textura	k1gFnPc4
<g/>
,	,	kIx,
obrázky	obrázek	k1gInPc4
ve	v	k7c6
vyšším	vysoký	k2eAgNnSc6d2
rozlišení	rozlišení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
Encore	Encor	k1gInSc5
byla	být	k5eAaImAgFnS
zlepšena	zlepšit	k5eAaPmNgFnS
nejen	nejen	k6eAd1
grafická	grafický	k2eAgFnSc1d1
část	část	k1gFnSc1
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
optimalizace	optimalizace	k1gFnSc1
výkonu	výkon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
technické	technický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
Encore	Encor	k1gInSc5
snižuje	snižovat	k5eAaImIp3nS
požadavky	požadavek	k1gInPc4
díky	díky	k7c3
např.	např.	kA
<g/>
:	:	kIx,
virtuálnímu	virtuální	k2eAgNnSc3d1
texturování	texturování	k1gNnSc3
<g/>
,	,	kIx,
odrazivost	odrazivost	k1gFnSc1
lesklých	lesklý	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
nebo	nebo	k8xC
adaptivním	adaptivní	k2eAgInPc3d1
stínům	stín	k1gInPc3
atd.	atd.	kA
Engine	Engin	k1gInSc5
s	s	k7c7
předstihem	předstih	k1gInSc7
vypočítá	vypočítat	k5eAaPmIp3nS
všechny	všechen	k3xTgInPc4
vysoce	vysoce	k6eAd1
náročné	náročný	k2eAgFnPc1d1
operace	operace	k1gFnPc1
<g/>
,	,	kIx,
ukládá	ukládat	k5eAaImIp3nS
je	on	k3xPp3gInPc4
jako	jako	k8xC,k8xS
virtuální	virtuální	k2eAgFnSc4d1
texturu	textura	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgMnPc4
předem	předem	k6eAd1
načte	načíst	k5eAaBmIp3nS,k5eAaPmIp3nS
a	a	k8xC
zobrazíme	zobrazit	k5eAaPmIp1nP
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nezbytné	zbytný	k2eNgNnSc1d1,k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Stará	starý	k2eAgFnSc1d1
garáž	garáž	k1gFnSc1
byla	být	k5eAaImAgFnS
úplně	úplně	k6eAd1
předělána	předělán	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
železného	železný	k2eAgInSc2d1
hangáru	hangár	k1gInSc2
byl	být	k5eAaImAgInS
tank	tank	k1gInSc1
přesunut	přesunout	k5eAaPmNgInS
na	na	k7c4
otevřenou	otevřený	k2eAgFnSc4d1
mýtinu	mýtina	k1gFnSc4
v	v	k7c6
lese	les	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
lze	lze	k6eAd1
sledovat	sledovat	k5eAaImF
přípravy	příprava	k1gFnPc4
na	na	k7c4
boj	boj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
přidán	přidán	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
soundtrack	soundtrack	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
pro	pro	k7c4
WoT	WoT	k1gFnSc4
složili	složit	k5eAaPmAgMnP
Andrey	Andrea	k1gFnPc4
Kulik	Kulik	k1gMnSc1
s	s	k7c7
Andriusem	Andrius	k1gInSc7
Klimkou	Klimka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizace	aktualizace	k1gFnSc1
vyšla	vyjít	k5eAaPmAgFnS
také	také	k9
díky	díky	k7c3
českému	český	k2eAgMnSc3d1
vývojáři	vývojář	k1gMnSc3
Miloši	Miloš	k1gMnSc3
Jeřábkovi	Jeřábek	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgMnSc7d1
vývojářem	vývojář	k1gMnSc7
týmu	tým	k1gInSc2
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
opravení	opravení	k1gNnSc6
počátečních	počáteční	k2eAgInPc2d1
bugů	bug	k1gInPc2
a	a	k8xC
nedodělaných	dodělaný	k2eNgFnPc2d1
textur	textura	k1gFnPc2
aktualizace	aktualizace	k1gFnSc1
sklidila	sklidit	k5eAaPmAgFnS
velký	velký	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Přepracování	přepracování	k1gNnSc1
map	mapa	k1gFnPc2
</s>
<s>
Ke	k	k7c3
každé	každý	k3xTgFnSc3
mapě	mapa	k1gFnSc3
byly	být	k5eAaImAgInP
přidány	přidán	k2eAgInPc1d1
pohybující	pohybující	k2eAgInPc1d1
se	se	k3xPyFc4
mraky	mrak	k1gInPc7
a	a	k8xC
vytvořeny	vytvořen	k2eAgInPc1d1
fotorealistické	fotorealistický	k2eAgInPc1d1
prvky	prvek	k1gInPc1
nebe	nebe	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojiště	bojiště	k1gNnSc4
byla	být	k5eAaImAgFnS
zasazena	zasazen	k2eAgFnSc1d1
do	do	k7c2
kilometrů	kilometr	k1gInPc2
okolních	okolní	k2eAgFnPc2d1
krajin	krajina	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vidět	vidět	k5eAaImF
za	za	k7c4
hranice	hranice	k1gFnPc4
mapy	mapa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ledovec	ledovec	k1gInSc1
<g/>
:	:	kIx,
Byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Ledovec	ledovec	k1gInSc1
<g/>
,	,	kIx,
inspirovaná	inspirovaný	k2eAgFnSc1d1
Švédskem	Švédsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
ledová	ledový	k2eAgFnSc1d1
pustina	pustina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
ohraničena	ohraničit	k5eAaPmNgFnS
zasněženými	zasněžený	k2eAgInPc7d1
horskými	horský	k2eAgInPc7d1
hřebeny	hřeben	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	s	k7c7
zde	zde	k6eAd1
gejzíry	gejzír	k1gInPc7
nebo	nebo	k8xC
třeba	třeba	k6eAd1
zamrzlé	zamrzlý	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
;	;	kIx,
jednou	jednou	k6eAd1
z	z	k7c2
hlavních	hlavní	k2eAgFnPc2d1
částí	část	k1gFnPc2
mapy	mapa	k1gFnPc1
je	být	k5eAaImIp3nS
právě	právě	k6eAd1
přelomená	přelomený	k2eAgFnSc1d1
letadlová	letadlový	k2eAgFnSc1d1
loď	loď	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
mapa	mapa	k1gFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
původně	původně	k6eAd1
vydána	vydán	k2eAgFnSc1d1
společně	společně	k6eAd1
s	s	k7c7
příchodem	příchod	k1gInSc7
švédského	švédský	k2eAgInSc2d1
tankového	tankový	k2eAgInSc2d1
vývojového	vývojový	k2eAgInSc2d1
stromu	strom	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vývojáři	vývojář	k1gMnPc1
nakonec	nakonec	k6eAd1
počkali	počkat	k5eAaPmAgMnP
na	na	k7c4
nový	nový	k2eAgInSc4d1
engine	enginout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Rybářský	rybářský	k2eAgInSc1d1
přístav	přístav	k1gInSc1
<g/>
:	:	kIx,
Střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
mapy	mapa	k1gFnSc2
byla	být	k5eAaImAgFnS
vyvýšena	vyvýšen	k2eAgFnSc1d1
<g/>
,	,	kIx,
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
byly	být	k5eAaImAgFnP
odstraněny	odstranit	k5eAaPmNgFnP
skály	skála	k1gFnPc1
a	a	k8xC
přidána	přidán	k2eAgFnSc1d1
zeleň	zeleň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Erlenberg	Erlenberg	k1gInSc1
<g/>
:	:	kIx,
Mapa	mapa	k1gFnSc1
je	být	k5eAaImIp3nS
celá	celá	k1gFnSc1
pokryta	pokrýt	k5eAaPmNgFnS
sněhem	sníh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
podél	podél	k6eAd1
řeky	řeka	k1gFnSc2
bylo	být	k5eAaImAgNnS
rozšířeno	rozšířit	k5eAaPmNgNnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
toku	tok	k1gInSc6
a	a	k8xC
kopce	kopka	k1gFnSc6
na	na	k7c6
západní	západní	k2eAgFnSc6d1
i	i	k8xC
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byly	být	k5eAaImAgInP
sníženy	snížit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Step	step	k1gInSc1
<g/>
:	:	kIx,
Skály	skála	k1gFnPc1
na	na	k7c6
západní	západní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
byly	být	k5eAaImAgInP
odstraněny	odstranit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
přidána	přidán	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Fjordy	fjord	k1gInPc1
<g/>
:	:	kIx,
Střed	střed	k1gInSc1
mapy	mapa	k1gFnSc2
byl	být	k5eAaImAgInS
srovnán	srovnat	k5eAaPmNgInS
s	s	k7c7
okolním	okolní	k2eAgInSc7d1
terénem	terén	k1gInSc7
a	a	k8xC
na	na	k7c4
kopec	kopec	k1gInSc4
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
části	část	k1gFnSc6
mapy	mapa	k1gFnSc2
byla	být	k5eAaImAgFnS
přidána	přidán	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c6
níž	jenž	k3xRgFnSc6
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
vyjet	vyjet	k5eAaPmF
až	až	k9
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Aktualizace	aktualizace	k1gFnSc1
1.10	1.10	k4
</s>
<s>
Aktualizace	aktualizace	k1gFnSc1
1.10	1.10	k4
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc7d3
aktualizací	aktualizace	k1gFnSc7
od	od	k7c2
vydání	vydání	k1gNnPc2
1.0	1.0	k4
<g/>
.	.	kIx.
</s>
<s>
Byla	být	k5eAaImAgFnS
přidána	přidat	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
větev	větev	k1gFnSc1
polských	polský	k2eAgInPc2d1
středních	střední	k2eAgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
přepracováno	přepracován	k2eAgNnSc1d1
vybavení	vybavení	k1gNnSc1
na	na	k7c6
verzi	verze	k1gFnSc6
2.0	2.0	k4
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
tanky	tank	k1gInPc1
byly	být	k5eAaImAgInP
upraveny	upravit	k5eAaPmNgInP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
zapadaly	zapadat	k5eAaPmAgInP,k5eAaImAgInP
do	do	k7c2
současného	současný	k2eAgInSc2d1
konceptu	koncept	k1gInSc2
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
změněna	změnit	k5eAaPmNgFnS
komunikace	komunikace	k1gFnSc1
<g/>
,	,	kIx,
přidána	přidán	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Perlová	perlová	k1gFnSc1
řeka	řeka	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
po	po	k7c6
letech	let	k1gInPc6
přepracována	přepracován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Módy	móda	k1gFnPc1
</s>
<s>
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejmodifikovanějších	modifikovaný	k2eAgFnPc2d3
her	hra	k1gFnPc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obzvláště	obzvláště	k6eAd1
na	na	k7c6
poměru	poměr	k1gInSc6
MMO	MMO	kA
her	hra	k1gFnPc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
fenomén	fenomén	k1gInSc4
dnešní	dnešní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsáhlé	rozsáhlý	k2eAgFnPc4d1
modifikace	modifikace	k1gFnPc4
jsou	být	k5eAaImIp3nP
umožněny	umožnit	k5eAaPmNgFnP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
probíhají	probíhat	k5eAaImIp3nP
na	na	k7c6
straně	strana	k1gFnSc6
klienta	klient	k1gMnSc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
serveru	server	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
projevují	projevovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
u	u	k7c2
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
modifikace	modifikace	k1gFnPc4
nainstaloval	nainstalovat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
oblíbené	oblíbený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
především	především	k9
skiny	skin	k1gMnPc4
a	a	k8xC
různé	různý	k2eAgFnPc4d1
úpravy	úprava	k1gFnPc4
HUD.	HUD.	k1gFnSc2
Například	například	k6eAd1
přehlednější	přehlední	k2eAgInPc1d2
zaměřovače	zaměřovač	k1gInPc1
jsou	být	k5eAaImIp3nP
mezi	mezi	k7c7
hráči	hráč	k1gMnPc7
World	Worlda	k1gFnPc2
of	of	k?
Tanks	Tanksa	k1gFnPc2
velmi	velmi	k6eAd1
oblíbené	oblíbený	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Není	být	k5eNaImIp3nS
uveřejněn	uveřejněn	k2eAgInSc1d1
oficiální	oficiální	k2eAgInSc1d1
seznam	seznam	k1gInSc1
zakázaných	zakázaný	k2eAgInPc2d1
módů	mód	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c4
používání	používání	k1gNnSc4
některých	některý	k3yIgInPc2
módů	mód	k1gInPc2
<g/>
,	,	kIx,
respektive	respektive	k9
balíčků	balíček	k1gInPc2
módů	mód	k1gInPc2
jsou	být	k5eAaImIp3nP
uživatelé	uživatel	k1gMnPc1
banováni	banovat	k5eAaImNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
všechny	všechen	k3xTgFnPc4
takové	takový	k3xDgFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
výrazně	výrazně	k6eAd1
zvýhodňují	zvýhodňovat	k5eAaImIp3nP
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	on	k3xPp3gNnSc4
používá	používat	k5eAaImIp3nS
<g/>
,	,	kIx,
například	například	k6eAd1
díky	díky	k7c3
oddálení	oddálení	k1gNnSc3
kamery	kamera	k1gFnSc2
kvůli	kvůli	k7c3
objevení	objevení	k1gNnSc3
artilerie	artilerie	k1gFnSc2
nebo	nebo	k8xC
laserové	laserový	k2eAgNnSc4d1
zaměřování	zaměřování	k1gNnSc4
a	a	k8xC
podobně	podobně	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
Warpack	Warpack	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Módy	mód	k1gInPc7
dnes	dnes	k6eAd1
existují	existovat	k5eAaImIp3nP
téměř	téměř	k6eAd1
na	na	k7c4
vše	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k9
hráče	hráč	k1gMnPc4
napadne	napadnout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tolerují	tolerovat	k5eAaImIp3nP
se	se	k3xPyFc4
módy	móda	k1gFnSc2
měnící	měnící	k2eAgFnSc4d1
grafickou	grafický	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
si	se	k3xPyFc3
hráči	hráč	k1gMnPc1
můžou	můžou	k?
stáhnout	stáhnout	k5eAaPmF
jiný	jiný	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
skin	skin	k1gMnSc1
<g/>
)	)	kIx)
tanku	tank	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráčskou	hráčský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
kritizovány	kritizovat	k5eAaImNgFnP
módy	móda	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
odhalí	odhalit	k5eAaPmIp3nP
pozici	pozice	k1gFnSc4
nepřátelských	přátelský	k2eNgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
zvýrazní	zvýraznit	k5eAaPmIp3nP
pozici	pozice	k1gFnSc4
dělostřelectva	dělostřelectvo	k1gNnSc2
při	při	k7c6
výstřelu	výstřel	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
minimapě	minimapa	k1gFnSc6
zobrazí	zobrazit	k5eAaPmIp3nS
padající	padající	k2eAgFnPc4d1
překážky	překážka	k1gFnPc4
<g/>
,	,	kIx,
vypočítá	vypočítat	k5eAaPmIp3nS
a	a	k8xC
ukáže	ukázat	k5eAaPmIp3nS
pravděpodobnou	pravděpodobný	k2eAgFnSc4d1
trasu	trasa	k1gFnSc4
nepřátelských	přátelský	k2eNgInPc2d1
tanků	tank	k1gInPc2
<g/>
,	,	kIx,
zobrazí	zobrazit	k5eAaPmIp3nS
udělování	udělování	k1gNnSc4
rozkazů	rozkaz	k1gInPc2
klikáním	klikání	k1gNnSc7
do	do	k7c2
minimapy	minimapa	k1gFnSc2
<g/>
,	,	kIx,
automaticky	automaticky	k6eAd1
zaměřují	zaměřovat	k5eAaImIp3nP
i	i	k8xC
pohybující	pohybující	k2eAgInPc1d1
se	se	k3xPyFc4
tanky	tank	k1gInPc7
a	a	k8xC
střílí	střílet	k5eAaImIp3nS
do	do	k7c2
nejslabších	slabý	k2eAgNnPc2d3
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
z	z	k7c2
manuálních	manuální	k2eAgInPc2d1
doplňků	doplněk	k1gInPc2
(	(	kIx(
<g/>
hasicí	hasicí	k2eAgInSc1d1
přístroj	přístroj	k1gInSc1
<g/>
)	)	kIx)
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
automatický	automatický	k2eAgInSc1d1
atd.	atd.	kA
</s>
<s>
Wargaming	Wargaming	k1gInSc1
pro	pro	k7c4
ulehčení	ulehčení	k1gNnSc4
stahování	stahování	k1gNnSc2
povolených	povolený	k2eAgInPc2d1
módů	mód	k1gInPc2
vytvořil	vytvořit	k5eAaPmAgInS
stránku	stránka	k1gFnSc4
„	„	k?
<g/>
Oficiální	oficiální	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
módů	mód	k1gInPc2
<g/>
“	“	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Mod	Mod	k1gFnSc1
Hub	houba	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
hráči	hráč	k1gMnPc1
mohou	moct	k5eAaImIp3nP
bez	bez	k7c2
obav	obava	k1gFnPc2
stahovat	stahovat	k5eAaImF
ověřené	ověřený	k2eAgFnPc4d1
módy	móda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1
akce	akce	k1gFnSc1
</s>
<s>
Wargaming	Wargaming	k1gInSc1
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
různých	různý	k2eAgNnPc2d1
válečných	válečný	k2eAgNnPc2d1
výročí	výročí	k1gNnPc2
umožňuje	umožňovat	k5eAaImIp3nS
hráčům	hráč	k1gMnPc3
hrát	hrát	k5eAaImF
zvláštní	zvláštní	k2eAgInSc4d1
režim	režim	k1gInSc4
hry	hra	k1gFnSc2
nebo	nebo	k8xC
zvláštní	zvláštní	k2eAgFnSc2d1
mise	mise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
při	při	k7c6
100	#num#	k4
výročí	výročí	k1gNnPc2
bojového	bojový	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
tanku	tank	k1gInSc2
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2016	#num#	k4
byl	být	k5eAaImAgInS
zpřístupněn	zpřístupnit	k5eAaPmNgInS
herní	herní	k2eAgInSc1d1
mód	mód	k1gInSc1
Konvoj	konvoj	k1gInSc1
jako	jako	k8xC,k8xS
oslava	oslava	k1gFnSc1
nasazení	nasazení	k1gNnSc2
tanku	tank	k1gInSc2
Mark	Mark	k1gMnSc1
I.	I.	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
aprílový	aprílový	k2eAgInSc1d1
žertík	žertík	k1gInSc1
uvolnil	uvolnit	k5eAaPmAgInS
Wargaming	Wargaming	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
herní	herní	k2eAgInSc1d1
mód	mód	k1gInSc1
s	s	k7c7
měsíční	měsíční	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
a	a	k8xC
hypotetickým	hypotetický	k2eAgInSc7d1
sférickým	sférický	k2eAgInSc7d1
tankem	tank	k1gInSc7
IS-8	IS-8	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
BALL	BALL	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
byl	být	k5eAaImAgInS
zase	zase	k9
uvolněn	uvolněn	k2eAgInSc1d1
herní	herní	k2eAgInSc1d1
mód	mód	k1gInSc1
Winter	Winter	k1gMnSc1
Showdown	Showdown	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
zvláštní	zvláštní	k2eAgFnSc7d1
kostičkovou	kostičkový	k2eAgFnSc7d1
grafikou	grafika	k1gFnSc7
ve	v	k7c6
stylu	styl	k1gInSc6
8	#num#	k4
<g/>
bitových	bitový	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Měsíčně	měsíčně	k6eAd1
se	se	k3xPyFc4
ve	v	k7c6
hře	hra	k1gFnSc6
objevuje	objevovat	k5eAaImIp3nS
herní	herní	k2eAgInSc1d1
mód	mód	k1gInSc1
Frontline	Frontlin	k1gInSc5
30	#num#	k4
vs	vs	k?
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Aktualizovaná	aktualizovaný	k2eAgFnSc1d1
grafika	grafika	k1gFnSc1
a	a	k8xC
nový	nový	k2eAgInSc1d1
soundtrack	soundtrack	k1gInSc1
pro	pro	k7c4
legendární	legendární	k2eAgFnSc4d1
hru	hra	k1gFnSc4
World	Worlda	k1gFnPc2
of	of	k?
Tanks	Tanksa	k1gFnPc2
v	v	k7c6
aktualizaci	aktualizace	k1gFnSc6
1.0	1.0	k4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Prémiový	prémiový	k2eAgInSc4d1
obchod	obchod	k1gInSc4
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
eu	eu	k?
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://worldoftanks.eu/encyclopedia/vehicles/	http://worldoftanks.eu/encyclopedia/vehicles/	k?
<g/>
↑	↑	k?
http://wiki.worldoftanks.eu/Maps	http://wiki.worldoftanks.eu/Maps	k1gInSc1
<g/>
↑	↑	k?
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
úspěchy	úspěch	k1gInPc1
|	|	kIx~
Obecné	obecný	k2eAgFnSc3d1
|	|	kIx~
Guide	Guid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
-	-	kIx~
Global	globat	k5eAaImAgInS
wiki	wiki	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Okřídlení	okřídlený	k2eAgMnPc1d1
husaři	husar	k1gMnPc1
dorazili	dorazit	k5eAaPmAgMnP
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lehké	Lehké	k2eAgInPc7d1
tanky	tank	k1gInPc7
–	–	k?
Global	globat	k5eAaImAgMnS
wiki	wiki	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Střední	střední	k2eAgInPc1d1
tanky	tank	k1gInPc1
–	–	k?
Global	globat	k5eAaImAgMnS
wiki	wiki	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Těžké	těžký	k2eAgInPc1d1
tanky	tank	k1gInPc1
–	–	k?
Global	globat	k5eAaImAgMnS
wiki	wiki	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Stíhače	stíhač	k1gMnSc4
tanků	tank	k1gInPc2
–	–	k?
Global	globat	k5eAaImAgMnS
wiki	wiki	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Dělostřelectvo	dělostřelectvo	k1gNnSc4
–	–	k?
Global	globat	k5eAaImAgMnS
wiki	wiki	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wargaming	Wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
wargaming	wargaming	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
aktualizací	aktualizace	k1gFnPc2
|	|	kIx~
docs	docsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Aktualizace	aktualizace	k1gFnPc4
1.0	1.0	k4
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-21	2018-03-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
oslaví	oslavit	k5eAaPmIp3nS
100	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
prvního	první	k4xOgNnSc2
bojového	bojový	k2eAgNnSc2d1
nasazení	nasazení	k1gNnSc2
tanku	tank	k1gInSc2
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
stylu	styl	k1gInSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GAMES	GAMES	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Apílový	Apílový	k2eAgInSc4d1
žertík	žertík	k1gInSc4
World	World	k1gMnSc1
of	of	k?
Tanks	Tanks	k1gInSc1
vás	vy	k3xPp2nPc4
pošle	poslat	k5eAaPmIp3nS
bojovat	bojovat	k5eAaImF
na	na	k7c4
Měsíc	měsíc	k1gInSc4
<g/>
...	...	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GAMES	GAMES	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Herní	herní	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
World	Worlda	k1gFnPc2
of	of	k?
Tanks	Tanksa	k1gFnPc2
rozšíří	rozšířit	k5eAaPmIp3nS
na	na	k7c6
konci	konec	k1gInSc6
ledna	leden	k1gInSc2
„	„	k?
<g/>
retro	retro	k1gNnSc2
<g/>
“	“	k?
mód	móda	k1gFnPc2
s	s	k7c7
kostičkovanými	kostičkovaný	k2eAgInPc7d1
tanky	tank	k1gInPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
GAMES	GAMES	kA
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
30.12	30.12	k4
<g/>
.2014	.2014	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
World	Worlda	k1gFnPc2
of	of	k?
Tanks	Tanksa	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
evropské	evropský	k2eAgFnSc2d1
části	část	k1gFnSc2
hráčů	hráč	k1gMnPc2
</s>
<s>
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
World	World	k1gInSc1
of	of	k?
Tanks	Tanks	k1gInSc1
Blitz	Blitz	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Počítačové	počítačový	k2eAgFnPc1d1
hry	hra	k1gFnPc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
