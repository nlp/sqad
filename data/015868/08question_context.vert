<s desamb="1">
Hra	hra	k1gFnSc1
je	být	k5eAaImIp3nS
vyvíjená	vyvíjený	k2eAgFnSc1d1
společností	společnost	k1gFnSc7
Wargaming	Wargaming	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
hráč	hráč	k1gMnSc1
hraje	hrát	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
třetí	třetí	k4xOgFnSc2
osoby	osoba	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
odstřelovacím	odstřelovací	k2eAgInSc6d1
módu	mód	k1gInSc6
z	z	k7c2
pohledu	pohled	k1gInSc2
střelce	střelec	k1gMnSc2
tanku	tank	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
dělostřeleckém	dělostřelecký	k2eAgInSc6d1
módu	mód	k1gInSc6
z	z	k7c2
ptačí	ptačí	k2eAgFnSc2d1
perspektivy	perspektiva	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>