<p>
<s>
Znojemský	znojemský	k2eAgInSc1d1	znojemský
úděl	úděl	k1gInSc1	úděl
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
historického	historický	k2eAgNnSc2d1	historické
území	území	k1gNnSc2	území
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vládla	vládnout	k5eAaImAgFnS	vládnout
knížata	kníže	k1gMnPc1wR	kníže
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
určil	určit	k5eAaPmAgMnS	určit
kníže	kníže	k1gMnSc1	kníže
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
bude	být	k5eAaImBp3nS	být
vládnout	vládnout	k5eAaImF	vládnout
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
celé	celý	k2eAgFnSc2d1	celá
zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
seniorát	seniorát	k1gInSc1	seniorát
<g/>
)	)	kIx)	)
a	a	k8xC	a
mladší	mladý	k2eAgMnPc1d2	mladší
synové	syn	k1gMnPc1	syn
dostanou	dostat	k5eAaPmIp3nP	dostat
území	území	k1gNnSc4	území
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Břetislavův	Břetislavův	k2eAgMnSc1d1	Břetislavův
nástupce	nástupce	k1gMnSc1	nástupce
Spytihněv	Spytihněv	k1gFnSc2	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc4	tento
rozdělení	rozdělení	k1gNnSc4	rozdělení
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
založili	založit	k5eAaPmAgMnP	založit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
větve	větev	k1gFnPc4	větev
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
definitivním	definitivní	k2eAgNnSc6d1	definitivní
rozdělení	rozdělení	k1gNnSc6	rozdělení
Moravy	Morava	k1gFnSc2	Morava
roku	rok	k1gInSc2	rok
1061	[number]	k4	1061
Vratislavem	Vratislav	k1gMnSc7	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
tři	tři	k4xCgInPc1	tři
celky	celek	k1gInPc1	celek
–	–	k?	–
brněnský	brněnský	k2eAgInSc4d1	brněnský
úděl	úděl	k1gInSc4	úděl
<g/>
,	,	kIx,	,
znojemský	znojemský	k2eAgInSc4d1	znojemský
úděl	úděl	k1gInSc4	úděl
a	a	k8xC	a
olomoucký	olomoucký	k2eAgInSc4d1	olomoucký
úděl	úděl	k1gInSc4	úděl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
dědičně	dědičně	k6eAd1	dědičně
vládnout	vládnout	k5eAaImF	vládnout
potomci	potomek	k1gMnPc1	potomek
bratrů	bratr	k1gMnPc2	bratr
Vratislava	Vratislav	k1gMnSc2	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Konráda	Konrád	k1gMnSc2	Konrád
I.	I.	kA	I.
Brněnského	brněnský	k2eAgMnSc2d1	brněnský
a	a	k8xC	a
Oty	Ota	k1gMnSc2	Ota
Olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
zaujímalo	zaujímat	k5eAaImAgNnS	zaujímat
prostor	prostor	k1gInSc4	prostor
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Brněnskem	Brněnsko	k1gNnSc7	Brněnsko
plnilo	plnit	k5eAaImAgNnS	plnit
úlohu	úloha	k1gFnSc4	úloha
ochrany	ochrana	k1gFnSc2	ochrana
státu	stát	k1gInSc2	stát
před	před	k7c7	před
napadením	napadení	k1gNnSc7	napadení
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
(	(	kIx(	(
<g/>
Olomoucko	Olomoucko	k1gNnSc1	Olomoucko
chránilo	chránit	k5eAaImAgNnS	chránit
české	český	k2eAgFnPc4d1	Česká
země	zem	k1gFnPc4	zem
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Konráda	Konrád	k1gMnSc2	Konrád
Brněnského	brněnský	k2eAgMnSc2d1	brněnský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1092	[number]	k4	1092
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
jeho	on	k3xPp3gInSc2	on
údělu	úděl	k1gInSc2	úděl
<g/>
.	.	kIx.	.
</s>
<s>
Konrádův	Konrádův	k2eAgMnSc1d1	Konrádův
starší	starý	k2eAgMnSc1d2	starší
syn	syn	k1gMnSc1	syn
Oldřich	Oldřich	k1gMnSc1	Oldřich
získal	získat	k5eAaPmAgMnS	získat
Brněnsko	Brněnsko	k1gNnSc4	Brněnsko
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgInSc4d2	mladší
Litold	Litold	k1gInSc4	Litold
Znojemsko	Znojemsko	k1gNnSc1	Znojemsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
panovníci	panovník	k1gMnPc1	panovník
do	do	k7c2	do
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
údělech	úděl	k1gInPc6	úděl
často	často	k6eAd1	často
zasahovali	zasahovat	k5eAaImAgMnP	zasahovat
<g/>
,	,	kIx,	,
zbavovali	zbavovat	k5eAaImAgMnP	zbavovat
jejich	jejich	k3xOp3gMnSc4	jejich
vládce	vládce	k1gMnSc4	vládce
moci	moct	k5eAaImF	moct
a	a	k8xC	a
dosazovali	dosazovat	k5eAaImAgMnP	dosazovat
své	svůj	k3xOyFgMnPc4	svůj
chráněnce	chráněnec	k1gMnPc4	chráněnec
nebo	nebo	k8xC	nebo
vládli	vládnout	k5eAaImAgMnP	vládnout
přímo	přímo	k6eAd1	přímo
oni	onen	k3xDgMnPc1	onen
sami	sám	k3xTgMnPc1	sám
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
českého	český	k2eAgMnSc2d1	český
knížete	kníže	k1gMnSc2	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
i	i	k9	i
mnozí	mnohý	k2eAgMnPc1d1	mnohý
moravští	moravský	k2eAgMnPc1d1	moravský
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
prosadili	prosadit	k5eAaPmAgMnP	prosadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
znojemských	znojemský	k2eAgMnPc2d1	znojemský
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
byl	být	k5eAaImAgInS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
poslední	poslední	k2eAgInSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
moravský	moravský	k2eAgMnSc1d1	moravský
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
poté	poté	k6eAd1	poté
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ota	Ota	k1gMnSc1	Ota
<g/>
.	.	kIx.	.
</s>
<s>
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
větev	větev	k1gFnSc1	větev
rodu	rod	k1gInSc2	rod
vymřela	vymřít	k5eAaPmAgNnP	vymřít
právě	právě	k6eAd1	právě
Konrádem	Konrád	k1gMnSc7	Konrád
Otou	Ota	k1gMnSc7	Ota
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1191	[number]	k4	1191
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jeho	jeho	k3xOp3gNnSc1	jeho
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Hellichou	Hellicha	k1gFnSc7	Hellicha
z	z	k7c2	z
Wittelsbachu	Wittelsbach	k1gInSc2	Wittelsbach
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
bezdětné	bezdětný	k2eAgNnSc1d1	bezdětné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Údělná	údělný	k2eAgNnPc1d1	údělné
knížata	kníže	k1gNnPc1	kníže
znojemská	znojemský	k2eAgNnPc1d1	Znojemské
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgInPc1d1	moravský
přemyslovské	přemyslovský	k2eAgInPc1d1	přemyslovský
úděly	úděl	k1gInPc1	úděl
</s>
</p>
<p>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
úděl	úděl	k1gInSc1	úděl
</s>
</p>
<p>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
úděl	úděl	k1gInSc1	úděl
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgNnSc1d1	Moravské
markrabství	markrabství	k1gNnSc1	markrabství
</s>
</p>
