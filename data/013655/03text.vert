<s>
Elektroencefalografie	Elektroencefalografie	k1gFnSc1
</s>
<s>
Záznam	záznam	k1gInSc1
spícího	spící	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
</s>
<s>
Elektroencefalografie	Elektroencefalografie	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
EEG	EEG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
záznamu	záznam	k1gInSc2
časové	časový	k2eAgFnSc2d1
změny	změna	k1gFnSc2
elektrického	elektrický	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
způsobeného	způsobený	k2eAgInSc2d1
mozkovou	mozkový	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
záznam	záznam	k1gInSc4
je	být	k5eAaImIp3nS
pořízen	pořídit	k5eAaPmNgMnS
elektroencefalografem	elektroencefalograf	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
EEG	EEG	kA
vyšetření	vyšetření	k1gNnSc1
</s>
<s>
EEG	EEG	kA
je	být	k5eAaImIp3nS
standardní	standardní	k2eAgFnSc7d1
neinvazivní	invazivní	k2eNgFnSc7d1
metodou	metoda	k1gFnSc7
funkčního	funkční	k2eAgNnSc2d1
vyšetření	vyšetření	k1gNnSc2
elektrické	elektrický	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
centrálního	centrální	k2eAgInSc2d1
nervového	nervový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumační	sumační	k2eAgInPc1d1
signály	signál	k1gInPc1
z	z	k7c2
neuronů	neuron	k1gInPc2
jsou	být	k5eAaImIp3nP
snímány	snímán	k2eAgFnPc1d1
elektrodami	elektroda	k1gFnPc7
z	z	k7c2
povrchu	povrch	k1gInSc2
skalpu	skalp	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problémem	problém	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
průchodem	průchod	k1gInSc7
přes	přes	k7c4
relativně	relativně	k6eAd1
málo	málo	k6eAd1
vodivou	vodivý	k2eAgFnSc4d1
lebku	lebka	k1gFnSc4
je	být	k5eAaImIp3nS
amplituda	amplituda	k1gFnSc1
signálu	signál	k1gInSc2
zeslabena	zeslabit	k5eAaPmNgFnS
na	na	k7c4
úroveň	úroveň	k1gFnSc4
řádově	řádově	k6eAd1
desítek	desítka	k1gFnPc2
mikrovoltů	mikrovolt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
EEG	EEG	kA
signál	signál	k1gInSc1
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k9
důsledek	důsledek	k1gInSc1
vážené	vážený	k2eAgFnSc2d1
sumace	sumace	k1gFnSc2
aktivity	aktivita	k1gFnSc2
extrémně	extrémně	k6eAd1
vysokého	vysoký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
neuronů	neuron	k1gInPc2
<g/>
,	,	kIx,
nejsme	být	k5eNaImIp1nP
již	již	k9
v	v	k7c6
EEG	EEG	kA
signálu	signál	k1gInSc2
schopni	schopen	k2eAgMnPc1d1
odlišit	odlišit	k5eAaPmF
jednotlivé	jednotlivý	k2eAgInPc1d1
akční	akční	k2eAgInPc1d1
potenciály	potenciál	k1gInPc1
buněk	buňka	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
v	v	k7c6
EMG	EMG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typický	typický	k2eAgInSc1d1
průběh	průběh	k1gInSc1
EEG	EEG	kA
má	mít	k5eAaImIp3nS
proto	proto	k8xC
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
dost	dost	k6eAd1
nepravidelný	pravidelný	k2eNgInSc1d1
a	a	k8xC
chaotický	chaotický	k2eAgInSc1d1
průběh	průběh	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
jsme	být	k5eAaImIp1nP
občas	občas	k6eAd1
schopni	schopen	k2eAgMnPc1d1
zahlédnout	zahlédnout	k5eAaPmF
vlny	vlna	k1gFnPc4
s	s	k7c7
určitou	určitý	k2eAgFnSc7d1
periodicitou	periodicita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
aktivita	aktivita	k1gFnSc1
alfa	alfa	k1gFnSc1
s	s	k7c7
frekvencí	frekvence	k1gFnSc7
cca	cca	kA
12	#num#	k4
Hz	Hz	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
sledujeme	sledovat	k5eAaImIp1nP
u	u	k7c2
dospělých	dospělí	k1gMnPc2
v	v	k7c6
occipitální	occipitální	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
hlavy	hlava	k1gFnSc2
při	při	k7c6
zavřených	zavřený	k2eAgNnPc6d1
očích	oko	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomalejší	pomalý	k2eAgFnSc1d2
frekvence	frekvence	k1gFnSc1
(	(	kIx(
<g/>
theta	theta	k1gFnSc1
a	a	k8xC
delta	delta	k1gFnSc1
<g/>
)	)	kIx)
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
v	v	k7c6
bdělém	bdělý	k2eAgInSc6d1
stavu	stav	k1gInSc6
u	u	k7c2
dospělých	dospělý	k2eAgFnPc2d1
patologickým	patologický	k2eAgInSc7d1
příznakem	příznak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
spánku	spánek	k1gInSc2
jsou	být	k5eAaImIp3nP
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
identifikátorem	identifikátor	k1gInSc7
různých	různý	k2eAgNnPc2d1
spánkových	spánkový	k2eAgNnPc2d1
stadií	stadion	k1gNnPc2
<g/>
,	,	kIx,
čehož	což	k3yQnSc2,k3yRnSc2
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
ve	v	k7c6
spánkových	spánkový	k2eAgFnPc6d1
laboratořích	laboratoř	k1gFnPc6
<g/>
;	;	kIx,
u	u	k7c2
dětí	dítě	k1gFnPc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
tyto	tento	k3xDgFnPc4
frekvence	frekvence	k1gFnPc4
měřítkem	měřítko	k1gNnSc7
vyzrálosti	vyzrálost	k1gFnSc2
CNS	CNS	kA
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
rozmístění	rozmístění	k1gNnSc4
elektrod	elektroda	k1gFnPc2
na	na	k7c6
povrchu	povrch	k1gInSc6
lbi	lbi	k?
je	být	k5eAaImIp3nS
standardně	standardně	k6eAd1
používán	používán	k2eAgMnSc1d1
tzv.	tzv.	kA
systém	systém	k1gInSc4
10	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
(	(	kIx(
<g/>
čti	číst	k5eAaImRp2nS
deset	deset	k4xCc4
–	–	k?
dvacet	dvacet	k4xCc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
název	název	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
ze	z	k7c2
způsobu	způsob	k1gInSc2
rozměření	rozměření	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
obvod	obvod	k1gInSc1
hlavy	hlava	k1gFnSc2
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
úseky	úsek	k1gInPc4
po	po	k7c6
10	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
20	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analogickým	analogický	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
probíhá	probíhat	k5eAaImIp3nS
rozměření	rozměření	k1gNnSc1
ve	v	k7c6
zbývajících	zbývající	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
kolmých	kolmý	k2eAgFnPc6d1
rovinách	rovina	k1gFnPc6
<g/>
,	,	kIx,
výsledkem	výsledek	k1gInSc7
něhož	jenž	k3xRgNnSc2
je	být	k5eAaImIp3nS
síť	síť	k1gFnSc1
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgInPc1d1
průsečíky	průsečík	k1gInPc1
poledníků	poledník	k1gInPc2
a	a	k8xC
rovnoběžek	rovnoběžka	k1gFnPc2
na	na	k7c6
zemském	zemský	k2eAgInSc6d1
globu	globus	k1gInSc6
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yQgFnSc2,k3yRgFnSc2,k3yIgFnSc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
umísťovány	umísťovat	k5eAaImNgFnP
elektrody	elektroda	k1gFnPc1
na	na	k7c4
standardní	standardní	k2eAgNnPc4d1
místa	místo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elektrody	elektroda	k1gFnPc4
umístěné	umístěný	k2eAgFnPc1d1
nejvíce	hodně	k6eAd3,k6eAd1
vpředu	vpředu	k6eAd1
nazýváme	nazývat	k5eAaImIp1nP
prefrontální	prefrontální	k2eAgFnPc1d1
<g/>
,	,	kIx,
za	za	k7c7
nimi	on	k3xPp3gInPc7
je	být	k5eAaImIp3nS
rozmístěna	rozmístěn	k2eAgFnSc1d1
řada	řada	k1gFnSc1
elektrod	elektroda	k1gFnPc2
frontálních	frontální	k2eAgFnPc2d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
následují	následovat	k5eAaImIp3nP
elektrody	elektroda	k1gFnPc4
centrální	centrální	k2eAgFnPc4d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
parietální	parietální	k2eAgFnSc1d1
a	a	k8xC
nejvíce	nejvíce	k6eAd1,k6eAd3
vzadu	vzadu	k6eAd1
jsou	být	k5eAaImIp3nP
elektrody	elektroda	k1gFnPc4
occipitální	occipitální	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stranách	strana	k1gFnPc6
umísťujeme	umísťovat	k5eAaImIp1nP
elektrody	elektroda	k1gFnPc4
temporální	temporální	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
EEG	EEG	kA
se	se	k3xPyFc4
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
obou	dva	k4xCgNnPc2
základních	základní	k2eAgNnPc2d1
zapojení	zapojení	k1gNnPc2
elektrod	elektroda	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
unipolárního	unipolární	k2eAgInSc2d1
i	i	k8xC
bipolárního	bipolární	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
bipolárního	bipolární	k2eAgNnSc2d1
zapojení	zapojení	k1gNnSc2
ještě	ještě	k9
rozlišujeme	rozlišovat	k5eAaImIp1nP
podle	podle	k7c2
směru	směr	k1gInSc2
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
jsou	být	k5eAaImIp3nP
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
řetězce	řetězec	k1gInPc1
<g/>
,	,	kIx,
zapojení	zapojení	k1gNnSc1
longitudinální	longitudinální	k2eAgNnSc1d1
(	(	kIx(
<g/>
předo-zadní	předo-zadní	k2eAgInSc4d1
směr	směr	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
transversální	transversální	k2eAgInSc1d1
(	(	kIx(
<g/>
levo-pravý	levo-pravý	k2eAgInSc1d1
směr	směr	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
jejich	jejich	k3xOp3gFnSc1
kombinace	kombinace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
EEG	EEG	kA
přístroj	přístroj	k1gInSc1
(	(	kIx(
<g/>
elektroencefalograf	elektroencefalograf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Diferenční	diferenční	k2eAgInSc4d1
zesilovač	zesilovač	k1gInSc4
</s>
<s>
Elektrické	elektrický	k2eAgInPc1d1
potenciály	potenciál	k1gInPc1
<g/>
,	,	kIx,
snímané	snímaný	k2eAgFnPc1d1
elektrodami	elektroda	k1gFnPc7
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
velmi	velmi	k6eAd1
nízkou	nízký	k2eAgFnSc4d1
amplitudu	amplituda	k1gFnSc4
<g/>
:	:	kIx,
u	u	k7c2
EEG	EEG	kA
jde	jít	k5eAaImIp3nS
o	o	k7c6
napětí	napětí	k1gNnSc6
řádově	řádově	k6eAd1
desítek	desítka	k1gFnPc2
mikrovoltů	mikrovolt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
nízká	nízký	k2eAgNnPc4d1
napětí	napětí	k1gNnPc4
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
nejprve	nejprve	k6eAd1
zesílit	zesílit	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
je	on	k3xPp3gInPc4
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
dále	daleko	k6eAd2
zpracovat	zpracovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Problémem	problém	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
amplitudou	amplituda	k1gFnSc7
sledovaných	sledovaný	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
rušení	rušení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
industrializované	industrializovaný	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
žijeme	žít	k5eAaImIp1nP
v	v	k7c6
prostředí	prostředí	k1gNnSc6
<g/>
,	,	kIx,
vyplněném	vyplněný	k2eAgInSc6d1
„	„	k?
<g/>
elektromagnetickým	elektromagnetický	k2eAgInSc7d1
smogem	smog	k1gInSc7
<g/>
“	“	k?
všeho	všecek	k3xTgInSc2
druhu	druh	k1gInSc2
<g/>
;	;	kIx,
vysílají	vysílat	k5eAaImIp3nP
je	on	k3xPp3gInPc4
nejen	nejen	k6eAd1
televizní	televizní	k2eAgInPc4d1
a	a	k8xC
rozhlasové	rozhlasový	k2eAgInPc4d1
vysílače	vysílač	k1gInPc4
a	a	k8xC
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
počítače	počítač	k1gInPc4
a	a	k8xC
prakticky	prakticky	k6eAd1
jakékoliv	jakýkoliv	k3yIgNnSc4
elektrické	elektrický	k2eAgNnSc4d1
vedení	vedení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nemocničním	nemocniční	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
k	k	k7c3
tomuto	tento	k3xDgNnSc3
rušení	rušení	k1gNnSc3
přispívá	přispívat	k5eAaImIp3nS
užití	užití	k1gNnSc4
řady	řada	k1gFnSc2
dalších	další	k2eAgInPc2d1
elektrických	elektrický	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
–	–	k?
elektroléčba	elektroléčba	k1gFnSc1
<g/>
,	,	kIx,
operační	operační	k2eAgInPc1d1
sály	sál	k1gInPc1
<g/>
,	,	kIx,
anesteziologicko-resuscitační	anesteziologicko-resuscitační	k2eAgNnPc1d1
oddělení	oddělení	k1gNnPc1
<g/>
,	,	kIx,
rentgeny	rentgen	k1gInPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
zobrazovací	zobrazovací	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
apod.	apod.	kA
jsou	být	k5eAaImIp3nP
neustálými	neustálý	k2eAgInPc7d1
zdroji	zdroj	k1gInPc7
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
rušení	rušení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
minulých	minulý	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
bývalo	bývat	k5eAaImAgNnS
pravidlem	pravidlem	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
např.	např.	kA
EEG	EEG	kA
přístroje	přístroj	k1gInPc1
se	se	k3xPyFc4
umísťovaly	umísťovat	k5eAaImAgInP
ve	v	k7c6
Faradayových	Faradayův	k2eAgInPc6d1
klecích	klek	k1gInPc6
<g/>
:	:	kIx,
původně	původně	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
klec	klec	k1gFnSc4
z	z	k7c2
drátěného	drátěný	k2eAgNnSc2d1
pletiva	pletivo	k1gNnSc2
nebo	nebo	k8xC
drátěné	drátěný	k2eAgFnSc2d1
mříže	mříž	k1gFnSc2
s	s	k7c7
pečlivě	pečlivě	k6eAd1
elektricky	elektricky	k6eAd1
propojenými	propojený	k2eAgInPc7d1
spoji	spoj	k1gInPc7
a	a	k8xC
uzemněnou	uzemněný	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
stínící	stínící	k2eAgInSc4d1
kryt	kryt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faradayovu	Faradayov	k1gInSc2
klec	klec	k1gFnSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
řešit	řešit	k5eAaImF
také	také	k9
umístěním	umístění	k1gNnSc7
uzemněného	uzemněný	k2eAgNnSc2d1
drátěného	drátěný	k2eAgNnSc2d1
pletiva	pletivo	k1gNnSc2
na	na	k7c4
omítku	omítka	k1gFnSc4
anebo	anebo	k8xC
pod	pod	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
použití	použití	k1gNnSc1
takových	takový	k3xDgFnPc2
stavebních	stavební	k2eAgFnPc2d1
úprav	úprava	k1gFnPc2
omezuje	omezovat	k5eAaImIp3nS
z	z	k7c2
důvodu	důvod	k1gInSc2
finančních	finanční	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
minimalizovat	minimalizovat	k5eAaBmF
indukovaná	indukovaný	k2eAgNnPc4d1
rušivá	rušivý	k2eAgNnPc4d1
napětí	napětí	k1gNnSc4
jednak	jednak	k8xC
konstrukcí	konstrukce	k1gFnSc7
přístroje	přístroj	k1gInSc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
pečlivým	pečlivý	k2eAgNnSc7d1
umístěním	umístění	k1gNnSc7
elektrod	elektroda	k1gFnPc2
na	na	k7c6
těle	tělo	k1gNnSc6
pacienta	pacient	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
zesílení	zesílení	k1gNnSc4
malých	malý	k2eAgInPc2d1
signálů	signál	k1gInPc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
vstupní	vstupní	k2eAgInSc4d1
zesilovač	zesilovač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
ze	z	k7c2
způsobů	způsob	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
minimalizovat	minimalizovat	k5eAaBmF
vliv	vliv	k1gInSc4
rušení	rušení	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
použít	použít	k5eAaPmF
k	k	k7c3
zesílení	zesílení	k1gNnSc3
nízkých	nízký	k2eAgInPc2d1
signálů	signál	k1gInPc2
vstupní	vstupní	k2eAgInSc4d1
zesilovač	zesilovač	k1gInSc4
v	v	k7c6
diferenčním	diferenční	k2eAgNnSc6d1
zapojení	zapojení	k1gNnSc6
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
řečeno	říct	k5eAaPmNgNnS
diferenční	diferenční	k2eAgFnSc1d1
zesilovač	zesilovač	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
citlivý	citlivý	k2eAgInSc4d1
zesilovač	zesilovač	k1gInSc4
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
napěťovým	napěťový	k2eAgNnSc7d1
zesílením	zesílení	k1gNnSc7
a	a	k8xC
se	s	k7c7
dvěma	dva	k4xCgInPc7
vstupy	vstup	k1gInPc7
<g/>
,	,	kIx,
jedním	jeden	k4xCgMnSc7
přímým	přímý	k2eAgMnSc7d1
(	(	kIx(
<g/>
aktivním	aktivní	k2eAgMnSc7d1
<g/>
)	)	kIx)
a	a	k8xC
druhým	druhý	k4xOgMnPc3
invertovaným	invertovaný	k2eAgMnPc3d1
(	(	kIx(
<g/>
referenčním	referenční	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diferenční	diferenční	k2eAgInSc1d1
zesilovač	zesilovač	k1gInSc1
pracuje	pracovat	k5eAaImIp3nS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
zesiluje	zesilovat	k5eAaImIp3nS
napěťový	napěťový	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
(	(	kIx(
<g/>
diferenci	diference	k1gFnSc4
<g/>
)	)	kIx)
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
vstupy	vstup	k1gInPc7
<g/>
:	:	kIx,
od	od	k7c2
napětí	napětí	k1gNnSc2
na	na	k7c6
přímém	přímý	k2eAgInSc6d1
vstupu	vstup	k1gInSc6
odečte	odečíst	k5eAaPmIp3nS
napětí	napětí	k1gNnSc1
na	na	k7c6
referenčním	referenční	k2eAgInSc6d1
vstupu	vstup	k1gInSc6
a	a	k8xC
pak	pak	k6eAd1
zesílí	zesílit	k5eAaPmIp3nS
pouze	pouze	k6eAd1
vzniklý	vzniklý	k2eAgInSc4d1
rozdíl	rozdíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc4
smysl	smysl	k1gInSc4
tohoto	tento	k3xDgNnSc2
zapojení	zapojení	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
na	na	k7c4
oba	dva	k4xCgInPc4
vstupy	vstup	k1gInPc4
přivede	přivést	k5eAaPmIp3nS
signál	signál	k1gInSc1
od	od	k7c2
elektrod	elektroda	k1gFnPc2
a	a	k8xC
na	na	k7c4
oba	dva	k4xCgInPc4
vstupy	vstup	k1gInPc4
se	se	k3xPyFc4
naindukuje	naindukovat	k5eAaPmIp3nS
stejně	stejně	k6eAd1
velké	velký	k2eAgNnSc4d1
rušivé	rušivý	k2eAgNnSc4d1
napětí	napětí	k1gNnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
rušivé	rušivý	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
od	od	k7c2
sebe	sebe	k3xPyFc4
vzájemně	vzájemně	k6eAd1
odečte	odečíst	k5eAaPmIp3nS
a	a	k8xC
na	na	k7c6
výstupu	výstup	k1gInSc6
zesilovače	zesilovač	k1gInSc2
se	se	k3xPyFc4
neprojeví	projevit	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Kanály	kanál	k1gInPc1
</s>
<s>
Výstup	výstup	k1gInSc4
příslušně	příslušně	k6eAd1
zapojeného	zapojený	k2eAgInSc2d1
zesilovače	zesilovač	k1gInSc2
(	(	kIx(
<g/>
ať	ať	k8xS,k8xC
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
bipolární	bipolární	k2eAgFnPc4d1
či	či	k8xC
unipolární	unipolární	k2eAgNnSc1d1
zapojení	zapojení	k1gNnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
kanál	kanál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
principu	princip	k1gInSc6
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
každý	každý	k3xTgInSc4
kanál	kanál	k1gInSc4
potřebujeme	potřebovat	k5eAaImIp1nP
jeden	jeden	k4xCgInSc4
vstupní	vstupní	k2eAgInSc4d1
zesilovač	zesilovač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolika	kolika	k1gFnSc1
kanály	kanál	k1gInPc4
je	být	k5eAaImIp3nS
aparatura	aparatura	k1gFnSc1
vybavena	vybavit	k5eAaPmNgFnS
<g/>
,	,	kIx,
tolik	tolik	k4xDc1,k4yIc1
různých	různý	k2eAgInPc2d1
signálů	signál	k1gInPc2
může	moct	k5eAaImIp3nS
sejmout	sejmout	k5eAaPmF
a	a	k8xC
zpracovat	zpracovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
postupu	postup	k1gInSc2
signálů	signál	k1gInPc2
jednotlivými	jednotlivý	k2eAgInPc7d1
kanály	kanál	k1gInPc7
je	být	k5eAaImIp3nS
možné	možný	k2eAgInPc4d1
signály	signál	k1gInPc4
v	v	k7c6
elektrické	elektrický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
různým	různý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
upravovat	upravovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickou	typický	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
je	být	k5eAaImIp3nS
použití	použití	k1gNnSc1
nastavitelných	nastavitelný	k2eAgInPc2d1
filtrů	filtr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Výkonové	výkonový	k2eAgInPc1d1
zesilovače	zesilovač	k1gInPc1
<g/>
,	,	kIx,
zapisovací	zapisovací	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Po	po	k7c6
výstupu	výstup	k1gInSc6
z	z	k7c2
filtrů	filtr	k1gInPc2
je	být	k5eAaImIp3nS
biosignál	biosignál	k1gInSc1
v	v	k7c6
tradičních	tradiční	k2eAgFnPc6d1
aparaturách	aparatura	k1gFnPc6
zesílen	zesílit	k5eAaPmNgInS
výkonovými	výkonový	k2eAgMnPc7d1
(	(	kIx(
<g/>
neboli	neboli	k8xC
výstupními	výstupní	k2eAgInPc7d1
<g/>
)	)	kIx)
zesilovači	zesilovač	k1gInPc7
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
výstup	výstup	k1gInSc1
má	mít	k5eAaImIp3nS
dostatečný	dostatečný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
na	na	k7c4
pohybování	pohybování	k1gNnSc4
pisátek	pisátko	k1gNnPc2
v	v	k7c6
záznamovém	záznamový	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záznamové	záznamový	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
tvořeno	tvořit	k5eAaImNgNnS
válcem	válec	k1gInSc7
s	s	k7c7
navinutým	navinutý	k2eAgInSc7d1
rastrovaným	rastrovaný	k2eAgInSc7d1
papírem	papír	k1gInSc7
a	a	k8xC
mechanikou	mechanika	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
posouvá	posouvat	k5eAaImIp3nS
papír	papír	k1gInSc4
konstantní	konstantní	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
(	(	kIx(
<g/>
ven	ven	k6eAd1
z	z	k7c2
přístroje	přístroj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
papíře	papír	k1gInSc6
se	se	k3xPyFc4
ve	v	k7c6
směru	směr	k1gInSc6
kolmém	kolmý	k2eAgInSc6d1
na	na	k7c4
směr	směr	k1gInSc4
pohybu	pohyb	k1gInSc2
papíru	papír	k1gInSc2
pohybují	pohybovat	k5eAaImIp3nP
pisátka	pisátko	k1gNnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
okamžitá	okamžitý	k2eAgFnSc1d1
výchylka	výchylka	k1gFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
okamžité	okamžitý	k2eAgFnSc2d1
velikosti	velikost	k1gFnSc2
biosignálu	biosignál	k1gInSc2
v	v	k7c6
příslušném	příslušný	k2eAgInSc6d1
kanálu	kanál	k1gInSc6
(	(	kIx(
<g/>
pisátek	pisátko	k1gNnPc2
je	být	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
stejný	stejný	k2eAgInSc1d1
počet	počet	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
počet	počet	k1gInSc1
kanálů	kanál	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
postupem	postup	k1gInSc7
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
biosignály	biosignála	k1gFnPc1
<g/>
,	,	kIx,
proměnlivé	proměnlivý	k2eAgFnPc1d1
v	v	k7c6
čase	čas	k1gInSc6
<g/>
,	,	kIx,
zapisují	zapisovat	k5eAaImIp3nP
na	na	k7c4
pohyblivý	pohyblivý	k2eAgInSc4d1
papír	papír	k1gInSc4
a	a	k8xC
vykreslují	vykreslovat	k5eAaImIp3nP
tam	tam	k6eAd1
graf	graf	k1gInSc4
příslušné	příslušný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgInSc7
pádem	pád	k1gInSc7
je	být	k5eAaImIp3nS
časově	časově	k6eAd1
proměnlivý	proměnlivý	k2eAgInSc1d1
signál	signál	k1gInSc1
fixován	fixován	k2eAgInSc1d1
do	do	k7c2
časově	časově	k6eAd1
stálé	stálý	k2eAgFnSc2d1
křivky	křivka	k1gFnSc2
na	na	k7c6
papíře	papír	k1gInSc6
a	a	k8xC
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
jako	jako	k9
předloha	předloha	k1gFnSc1
pro	pro	k7c4
vyhodnocení	vyhodnocení	k1gNnSc4
příslušným	příslušný	k2eAgMnSc7d1
specialistou	specialista	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
EEG	EEG	kA
elektrody	elektroda	k1gFnPc1
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1
elektrod	elektroda	k1gFnPc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
způsobu	způsob	k1gInSc6
jejich	jejich	k3xOp3gNnSc2
použití	použití	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Primárně	primárně	k6eAd1
je	být	k5eAaImIp3nS
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
povrchové	povrchový	k2eAgFnPc4d1
<g/>
,	,	kIx,
podpovrchové	podpovrchový	k2eAgFnPc4d1
a	a	k8xC
mikroelektrody	mikroelektroda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpovrchové	podpovrchový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
podkožní	podkožní	k2eAgMnPc4d1
(	(	kIx(
<g/>
subdermální	subdermální	k2eAgMnPc4d1
<g/>
)	)	kIx)
anebo	anebo	k8xC
implantabilní	implantabilní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrchové	povrchový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
plovoucí	plovoucí	k2eAgInSc4d1
(	(	kIx(
<g/>
využívající	využívající	k2eAgInSc4d1
vodivých	vodivý	k2eAgMnPc2d1
past	past	k1gFnSc4
<g/>
)	)	kIx)
a	a	k8xC
suché	suchý	k2eAgInPc1d1
izolované	izolovaný	k2eAgInPc1d1
nebo	nebo	k8xC
neizolované	izolovaný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikroelektrody	Mikroelektroda	k1gFnPc1
jsou	být	k5eAaImIp3nP
speciální	speciální	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
zkonstruované	zkonstruovaný	k2eAgFnPc4d1
pro	pro	k7c4
měření	měření	k1gNnPc4
například	například	k6eAd1
buněčného	buněčný	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
jediného	jediný	k2eAgInSc2d1
neuronu	neuron	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
rozhraní	rozhraní	k1gNnSc6
s	s	k7c7
elektrodou	elektroda	k1gFnSc7
vždy	vždy	k6eAd1
dochází	docházet	k5eAaImIp3nS
ke	k	k7c3
změně	změna	k1gFnSc3
vodivosti	vodivost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organismus	organismus	k1gInSc1
se	se	k3xPyFc4
chová	chovat	k5eAaImIp3nS
jako	jako	k9
vodič	vodič	k1gInSc1
druhé	druhý	k4xOgFnSc2
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
elektrický	elektrický	k2eAgInSc1d1
proud	proud	k1gInSc1
je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
pohybem	pohyb	k1gInSc7
nabitých	nabitý	k2eAgInPc2d1
iontů	ion	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
od	od	k7c2
elektrody	elektroda	k1gFnSc2
vede	vést	k5eAaImIp3nS
proud	proud	k1gInSc1
vodič	vodič	k1gInSc1
první	první	k4xOgFnSc2
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
pohyb	pohyb	k1gInSc1
volných	volný	k2eAgInPc2d1
elektronů	elektron	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
rozhraní	rozhraní	k1gNnSc1
se	se	k3xPyFc4
principielně	principielně	k6eAd1
chová	chovat	k5eAaImIp3nS
jako	jako	k9
galvanický	galvanický	k2eAgInSc1d1
článek	článek	k1gInSc1
a	a	k8xC
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
rozdíl	rozdíl	k1gInSc1
potenciálů	potenciál	k1gInPc2
o	o	k7c6
velikosti	velikost	k1gFnSc6
dané	daný	k2eAgFnSc6d1
rovnicí	rovnice	k1gFnSc7
</s>
<s>
Δ	Δ	k?
</s>
<s>
Φ	Φ	k?
</s>
<s>
=	=	kIx~
</s>
<s>
Φ	Φ	k?
</s>
<s>
m	m	kA
</s>
<s>
−	−	k?
</s>
<s>
Φ	Φ	k?
</s>
<s>
e	e	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
Delta	delta	k1gFnSc1
\	\	kIx~
<g/>
Phi	Phi	k1gFnSc1
=	=	kIx~
<g/>
\	\	kIx~
<g/>
Phi	Phi	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}	}	kIx)
<g/>
-	-	kIx~
<g/>
\	\	kIx~
<g/>
Phi	Phi	k1gFnSc1
_	_	kIx~
<g/>
{	{	kIx(
<g/>
e	e	k0
<g/>
}}	}}	k?
</s>
<s>
Kde	kde	k6eAd1
</s>
<s>
Φ	Φ	k?
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
Phi	Phi	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
m	m	kA
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
potenciál	potenciál	k1gInSc4
elektrody	elektroda	k1gFnSc2
a	a	k8xC
</s>
<s>
Φ	Φ	k?
</s>
<s>
e	e	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
\	\	kIx~
<g/>
Phi	Phi	k1gFnSc6
_	_	kIx~
<g/>
{	{	kIx(
<g/>
e	e	k0
<g/>
}}	}}	k?
</s>
<s>
potenciál	potenciál	k1gInSc1
styčné	styčný	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
nebo	nebo	k8xC
roztoku	roztok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
ty	ten	k3xDgMnPc4
to	ten	k3xDgNnSc1
vlastnosti	vlastnost	k1gFnSc3
jsou	být	k5eAaImIp3nP
vybírány	vybírán	k2eAgInPc1d1
materiály	materiál	k1gInPc1
pro	pro	k7c4
konstrukci	konstrukce	k1gFnSc4
elektrod	elektroda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
existují	existovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
skupiny	skupina	k1gFnPc1
plovoucích	plovoucí	k2eAgFnPc2d1
elektrod	elektroda	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Elektrody	elektroda	k1gFnPc4
prvního	první	k4xOgInSc2
druhu	druh	k1gInSc2
–	–	k?
Kovové	kovový	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
pokryté	pokrytý	k2eAgFnPc4d1
kationty	kation	k1gInPc4
téhož	týž	k3xTgInSc2
kovu	kov	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
vodíková	vodíkový	k2eAgFnSc1d1
<g/>
,	,	kIx,
stříbrná	stříbrnat	k5eAaImIp3nS
<g/>
)	)	kIx)
</s>
<s>
Elektrody	elektroda	k1gFnPc4
druhého	druhý	k4xOgInSc2
druhu	druh	k1gInSc2
–	–	k?
Elektrody	elektroda	k1gFnSc2
s	s	k7c7
povrchovou	povrchový	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
těžko	těžko	k6eAd1
rozpustné	rozpustný	k2eAgFnSc2d1
soli	sůl	k1gFnSc2
nebo	nebo	k8xC
hydroxidu	hydroxid	k1gInSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
elektrolyt	elektrolyt	k1gInSc1
má	mít	k5eAaImIp3nS
s	s	k7c7
touto	tento	k3xDgFnSc7
povrchovou	povrchový	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
společný	společný	k2eAgInSc1d1
aniont	aniont	k1gInSc1
(	(	kIx(
<g/>
např.	např.	kA
argentchloridová	argentchloridový	k2eAgFnSc1d1
<g/>
,	,	kIx,
kalomelová	kalomelový	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Suché	Suché	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
nevyžadují	vyžadovat	k5eNaImIp3nP
použití	použití	k1gNnSc3
vodivých	vodivý	k2eAgFnPc2d1
past	pasta	k1gFnPc2
a	a	k8xC
konstruují	konstruovat	k5eAaImIp3nP
se	se	k3xPyFc4
buď	buď	k8xC
pouze	pouze	k6eAd1
s	s	k7c7
kovovým	kovový	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
(	(	kIx(
<g/>
neizolované	izolovaný	k2eNgInPc1d1
<g/>
)	)	kIx)
nebo	nebo	k8xC
s	s	k7c7
kovovým	kovový	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
pokrytým	pokrytý	k2eAgInSc7d1
vrstvou	vrstva	k1gFnSc7
dielektrika	dielektrikum	k1gNnSc2
(	(	kIx(
<g/>
izolované	izolovaný	k2eAgInPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taková	takový	k3xDgFnSc1
elektroda	elektroda	k1gFnSc1
pak	pak	k6eAd1
klade	klást	k5eAaImIp3nS
vysoké	vysoký	k2eAgInPc4d1
nároky	nárok	k1gInPc4
na	na	k7c4
vstupní	vstupní	k2eAgInSc4d1
odpor	odpor	k1gInSc4
zesilovače	zesilovač	k1gInSc2
(	(	kIx(
<g/>
0,1	0,1	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
GΩ	GΩ	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Artefakty	artefakt	k1gInPc1
</s>
<s>
Jako	jako	k9
artefakt	artefakt	k1gInSc4
označujeme	označovat	k5eAaImIp1nP
nežádoucí	žádoucí	k2eNgInSc4d1
signál	signál	k1gInSc4
transponovaný	transponovaný	k2eAgInSc4d1
na	na	k7c4
EEG	EEG	kA
záznam	záznam	k1gInSc4
<g/>
,	,	kIx,
mající	mající	k2eAgInSc4d1
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
jinde	jinde	k6eAd1
než	než	k8xS
ve	v	k7c6
zkoumané	zkoumaný	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišujeme	rozlišovat	k5eAaImIp1nP
artefakty	artefakt	k1gInPc7
biologické	biologický	k2eAgFnSc2d1
a	a	k8xC
technické	technický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Artefakty	artefakt	k1gInPc1
technické	technický	k2eAgFnSc2d1
</s>
<s>
•	•	k?
<g/>
Síťový	síťový	k2eAgInSc1d1
brum	brum	k1gInSc1
–	–	k?
Artefakt	artefakt	k1gInSc4
síťového	síťový	k2eAgInSc2d1
kmitočtu	kmitočet	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
ČR	ČR	kA
50	#num#	k4
Hz	Hz	kA
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gInPc4
násobky	násobek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
jak	jak	k6eAd1
z	z	k7c2
vlastního	vlastní	k2eAgNnSc2d1
EEG	EEG	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
ze	z	k7c2
spotřebičů	spotřebič	k1gInPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
zapojených	zapojený	k2eAgMnPc2d1
do	do	k7c2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
vznikat	vznikat	k5eAaImF
zejména	zejména	k9
při	při	k7c6
špatném	špatný	k2eAgNnSc6d1
uzemnění	uzemnění	k1gNnSc6
přístroje	přístroj	k1gInSc2
a	a	k8xC
ze	z	k7c2
záznamu	záznam	k1gInSc2
se	se	k3xPyFc4
odstraňuje	odstraňovat	k5eAaImIp3nS
zařazením	zařazení	k1gNnSc7
pásmové	pásmový	k2eAgFnSc2d1
zádrže	zádrž	k1gFnSc2
resp.	resp.	kA
dolní	dolní	k2eAgFnSc2d1
propusti	propust	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
•	•	k?
<g/>
Šum	šum	k1gInSc1
přístroje	přístroj	k1gInSc2
–	–	k?
Každá	každý	k3xTgFnSc1
elektronická	elektronický	k2eAgFnSc1d1
součástka	součástka	k1gFnSc1
protékaná	protékaný	k2eAgFnSc1d1
proudem	proud	k1gInSc7
generuje	generovat	k5eAaImIp3nS
šum	šum	k1gInSc1
(	(	kIx(
<g/>
vlivem	vliv	k1gInSc7
tepla	teplo	k1gNnSc2
<g/>
,	,	kIx,
nerovnoměrného	rovnoměrný	k2eNgInSc2d1
průchodu	průchod	k1gInSc2
proudu	proud	k1gInSc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
šum	šum	k1gInSc1
nepříznivě	příznivě	k6eNd1
ovlivňuje	ovlivňovat	k5eAaImIp3nS
dosažitelnou	dosažitelný	k2eAgFnSc4d1
citlivost	citlivost	k1gFnSc4
přístroje	přístroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
přístroje	přístroj	k1gInSc2
EEG	EEG	kA
mají	mít	k5eAaImIp3nP
největší	veliký	k2eAgInSc4d3
vliv	vliv	k1gInSc4
na	na	k7c4
generaci	generace	k1gFnSc4
šumu	šum	k1gInSc2
jeho	jeho	k3xOp3gInPc4
vstupní	vstupní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
je	být	k5eAaImIp3nS
šum	šum	k1gInSc4
součástek	součástka	k1gFnPc2
signálem	signál	k1gInSc7
náhodným	náhodný	k2eAgInSc7d1
<g/>
,	,	kIx,
nelze	lze	k6eNd1
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vliv	vliv	k1gInSc4
odstranit	odstranit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
•	•	k?
<g/>
Elektrostatické	elektrostatický	k2eAgInPc4d1
potenciály	potenciál	k1gInPc4
–	–	k?
Jejich	jejich	k3xOp3gFnSc7
příčinou	příčina	k1gFnSc7
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
špatný	špatný	k2eAgInSc4d1
kontakt	kontakt	k1gInSc4
elektrody	elektroda	k1gFnSc2
s	s	k7c7
měřeným	měřený	k2eAgInSc7d1
subjektem	subjekt	k1gInSc7
<g/>
,	,	kIx,
drift	drift	k1gInSc1
parametrů	parametr	k1gInPc2
zesilovače	zesilovač	k1gInSc2
v	v	k7c6
přístroji	přístroj	k1gInSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
změny	změna	k1gFnPc1
parametrů	parametr	k1gInPc2
elektrod	elektroda	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
polarizace	polarizace	k1gFnSc1
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
vyskytnout	vyskytnout	k5eAaPmF
v	v	k7c6
důsledku	důsledek	k1gInSc6
jakéhokoli	jakýkoli	k3yIgInSc2
pohybu	pohyb	k1gInSc2
elektrostaticky	elektrostaticky	k6eAd1
nabitých	nabitý	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
přístroje	přístroj	k1gInSc2
nebo	nebo	k8xC
měřeného	měřený	k2eAgInSc2d1
subjektu	subjekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Artefakty	artefakt	k1gInPc1
biologické	biologický	k2eAgFnSc2d1
</s>
<s>
•	•	k?
<g/>
Artefakty	artefakt	k1gInPc1
srdeční	srdeční	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
–	–	k?
Na	na	k7c4
záznam	záznam	k1gInSc4
se	se	k3xPyFc4
transponuje	transponovat	k5eAaBmIp3nS
signál	signál	k1gInSc1
s	s	k7c7
kmitočtem	kmitočet	k1gInSc7
srdečních	srdeční	k2eAgInPc2d1
stahů	stah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amplituda	amplituda	k1gFnSc1
signálu	signál	k1gInSc2
bývá	bývat	k5eAaImIp3nS
nízká	nízký	k2eAgFnSc1d1
<g/>
,	,	kIx,
tvar	tvar	k1gInSc1
signálu	signál	k1gInSc2
ale	ale	k8xC
může	moct	k5eAaImIp3nS
připomínat	připomínat	k5eAaImF
tzv.	tzv.	kA
„	„	k?
<g/>
hrot	hrot	k1gInSc1
<g/>
“	“	k?
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
bývá	bývat	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
faktorů	faktor	k1gInPc2
naznačujících	naznačující	k2eAgInPc2d1
např.	např.	kA
epilepsii	epilepsie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
současně	současně	k6eAd1
s	s	k7c7
EEG	EEG	kA
záznamem	záznam	k1gInSc7
pořizován	pořizovat	k5eAaImNgInS
i	i	k9
záznam	záznam	k1gInSc4
EKG	EKG	kA
k	k	k7c3
určení	určení	k1gNnSc3
těchto	tento	k3xDgInPc2
falešných	falešný	k2eAgInPc2d1
signálů	signál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
•	•	k?
<g/>
Artefakty	artefakt	k1gInPc1
vyvolané	vyvolaný	k2eAgInPc1d1
pohybem	pohyb	k1gInSc7
očí	oko	k1gNnPc2
–	–	k?
Oční	oční	k2eAgFnSc1d1
bulva	bulva	k1gFnSc1
svým	svůj	k3xOyFgNnSc7
uspořádáním	uspořádání	k1gNnSc7
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
elektrický	elektrický	k2eAgInSc1d1
dipól	dipól	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
při	při	k7c6
svém	svůj	k3xOyFgInSc6
pohybu	pohyb	k1gInSc6
generuje	generovat	k5eAaImIp3nS
do	do	k7c2
elektrod	elektroda	k1gFnPc2
signál	signál	k1gInSc1
poměrně	poměrně	k6eAd1
vysoké	vysoký	k2eAgFnSc2d1
amplitudy	amplituda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
umístění	umístění	k1gNnSc3
oka	oko	k1gNnSc2
jsou	být	k5eAaImIp3nP
těmito	tento	k3xDgInPc7
artefakty	artefakt	k1gInPc7
nejvíce	nejvíce	k6eAd1,k6eAd3
zatíženy	zatížen	k2eAgFnPc4d1
elektrody	elektroda	k1gFnPc4
nad	nad	k7c7
přední	přední	k2eAgFnSc7d1
(	(	kIx(
<g/>
frontální	frontální	k2eAgFnSc7d1
<g/>
)	)	kIx)
částí	část	k1gFnSc7
lebky	lebka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
signály	signál	k1gInPc4
lze	lze	k6eAd1
ze	z	k7c2
záznamu	záznam	k1gInSc2
odstranit	odstranit	k5eAaPmF
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
EKG	EKG	kA
artefakty	artefakt	k1gInPc1
<g/>
,	,	kIx,
současným	současný	k2eAgInSc7d1
záznamem	záznam	k1gInSc7
a	a	k8xC
následným	následný	k2eAgNnSc7d1
odečtením	odečtení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očními	oční	k2eAgInPc7d1
biopotenciály	biopotenciál	k1gInPc7
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
samostatná	samostatný	k2eAgFnSc1d1
disciplína	disciplína	k1gFnSc1
–	–	k?
elektrookulografie	elektrookulografie	k1gFnSc1
(	(	kIx(
<g/>
EOG	EOG	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
záznamu	záznam	k1gInSc2
(	(	kIx(
<g/>
elektrookulogramu	elektrookulogram	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vypozorovat	vypozorovat	k5eAaPmF
např.	např.	kA
poruchy	porucha	k1gFnPc4
zraku	zrak	k1gInSc2
či	či	k8xC
dyslexii	dyslexie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
ho	on	k3xPp3gMnSc4
také	také	k9
k	k	k7c3
rozpoznání	rozpoznání	k1gNnSc3
jednotlivých	jednotlivý	k2eAgFnPc2d1
fází	fáze	k1gFnPc2
spánku	spánek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
•	•	k?
<g/>
Artefakty	artefakt	k1gInPc1
vyvolané	vyvolaný	k2eAgInPc1d1
svalovou	svalový	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
–	–	k?
Jejich	jejich	k3xOp3gFnSc7
příčinou	příčina	k1gFnSc7
je	být	k5eAaImIp3nS
elektrické	elektrický	k2eAgNnSc4d1
pole	pole	k1gNnSc4
<g/>
,	,	kIx,
generované	generovaný	k2eAgNnSc4d1
při	při	k7c6
kontrakci	kontrakce	k1gFnSc6
svalu	sval	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
se	se	k3xPyFc4
vyskytujícím	vyskytující	k2eAgInSc7d1
artefaktem	artefakt	k1gInSc7
v	v	k7c6
EEG	EEG	kA
záznamu	záznam	k1gInSc2
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvýznamnější	významný	k2eAgInSc4d3
podíl	podíl	k1gInSc4
na	na	k7c6
vzniku	vznik	k1gInSc6
artefaktu	artefakt	k1gInSc2
mají	mít	k5eAaImIp3nP
obličejové	obličejový	k2eAgInPc1d1
svaly	sval	k1gInPc1
<g/>
,	,	kIx,
zejména	zejména	k9
svaly	sval	k1gInPc1
čelistí	čelist	k1gFnPc2
,	,	kIx,
svaly	sval	k1gInPc1
okolo	okolo	k7c2
očí	oko	k1gNnPc2
a	a	k8xC
jazyk	jazyk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měřením	měření	k1gNnSc7
svalových	svalový	k2eAgInPc2d1
biopotenciálů	biopotenciál	k1gInPc2
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
elektromyografie	elektromyografie	k1gFnSc1
(	(	kIx(
<g/>
EMG	EMG	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
artefakty	artefakt	k1gInPc1
lze	lze	k6eAd1
přirozeně	přirozeně	k6eAd1
odstranit	odstranit	k5eAaPmF
uvolněním	uvolnění	k1gNnSc7
svalů	sval	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Monitorovaní	monitorovaný	k2eAgMnPc1d1
a	a	k8xC
diagnostika	diagnostika	k1gFnSc1
různých	různý	k2eAgInPc2d1
stavů	stav	k1gInPc2
a	a	k8xC
chorob	choroba	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
vyzrálost	vyzrálost	k1gFnSc1
CNS	CNS	kA
u	u	k7c2
dětí	dítě	k1gFnPc2
</s>
<s>
epilepsie	epilepsie	k1gFnSc1
</s>
<s>
poruchy	porucha	k1gFnPc1
spánku	spánek	k1gInSc2
</s>
<s>
kóma	kóma	k1gNnSc1
</s>
<s>
mozková	mozkový	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektromyografie	elektromyografie	k1gFnSc1
</s>
<s>
Elektrokortikografie	Elektrokortikografie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
elektroencefalografie	elektroencefalografie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Medicína	medicína	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4014254-1	4014254-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
12722	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85042138	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85042138	#num#	k4
</s>
