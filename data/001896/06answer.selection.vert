<s>
U	u	k7c2	u
sinic	sinice	k1gFnPc2	sinice
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
bičíky	bičík	k1gInPc1	bičík
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
zástupci	zástupce	k1gMnPc1	zástupce
řádu	řád	k1gInSc2	řád
Oscillatoriales	Oscillatoriales	k1gInSc1	Oscillatoriales
<g/>
)	)	kIx)	)
dokáží	dokázat	k5eAaPmIp3nP	dokázat
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
aktivně	aktivně	k6eAd1	aktivně
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
.	.	kIx.	.
</s>
