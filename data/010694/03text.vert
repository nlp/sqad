<p>
<s>
Linková	linkový	k2eAgFnSc1d1	Linková
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
spojová	spojový	k2eAgFnSc1d1	spojová
vrstva	vrstva	k1gFnSc1	vrstva
nebo	nebo	k8xC	nebo
vrstva	vrstva	k1gFnSc1	vrstva
datového	datový	k2eAgInSc2d1	datový
spoje	spoj	k1gInSc2	spoj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
data	datum	k1gNnPc4	datum
link	link	k1gMnSc1	link
layer	layer	k1gMnSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
vrstva	vrstva	k1gFnSc1	vrstva
v	v	k7c6	v
referenčním	referenční	k2eAgInSc6d1	referenční
modelu	model	k1gInSc6	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
uzly	uzel	k1gInPc7	uzel
propojenými	propojený	k2eAgInPc7d1	propojený
tímtéž	týž	k3xTgInSc7	týž
datovým	datový	k2eAgInSc7d1	datový
spojem	spoj	k1gInSc7	spoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dvoubodový	dvoubodový	k2eAgMnSc1d1	dvoubodový
(	(	kIx(	(
<g/>
např.	např.	kA	např.
sériová	sériový	k2eAgFnSc1d1	sériová
linka	linka	k1gFnSc1	linka
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
mnohabodový	mnohabodový	k2eAgMnSc1d1	mnohabodový
(	(	kIx(	(
<g/>
lokální	lokální	k2eAgFnPc1d1	lokální
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Ethernet	Ethernet	k1gInSc1	Ethernet
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
specifickým	specifický	k2eAgNnSc7d1	specifické
uspořádáním	uspořádání	k1gNnSc7	uspořádání
je	být	k5eAaImIp3nS	být
point-to-multipoint	pointoultipoint	k1gInSc1	point-to-multipoint
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
jedna	jeden	k4xCgFnSc1	jeden
řídicí	řídicí	k2eAgFnSc1d1	řídicí
stanice	stanice	k1gFnSc1	stanice
komunikuje	komunikovat	k5eAaImIp3nS	komunikovat
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
stanic	stanice	k1gFnPc2	stanice
podřízených	podřízený	k1gMnPc2	podřízený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Linková	linkový	k2eAgFnSc1d1	Linková
vrstva	vrstva	k1gFnSc1	vrstva
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
služby	služba	k1gFnPc4	služba
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
nebo	nebo	k8xC	nebo
nespolehlivou	spolehlivý	k2eNgFnSc4d1	nespolehlivá
komunikaci	komunikace	k1gFnSc4	komunikace
</s>
</p>
<p>
<s>
spojovanou	spojovaný	k2eAgFnSc7d1	spojovaná
nebo	nebo	k8xC	nebo
nespojovanouLinková	spojovanouLinkový	k2eNgFnSc1d1	spojovanouLinkový
vrstva	vrstva	k1gFnSc1	vrstva
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
z	z	k7c2	z
informací	informace	k1gFnPc2	informace
od	od	k7c2	od
vyšší	vysoký	k2eAgFnSc2d2	vyšší
vrstvy	vrstva	k1gFnSc2	vrstva
rámce	rámec	k1gInSc2	rámec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
opatřuje	opatřovat	k5eAaImIp3nS	opatřovat
fyzickou	fyzický	k2eAgFnSc7d1	fyzická
adresou	adresa	k1gFnSc7	adresa
(	(	kIx(	(
<g/>
MAC	Mac	kA	Mac
adresou	adresa	k1gFnSc7	adresa
<g/>
)	)	kIx)	)
a	a	k8xC	a
předává	předávat	k5eAaImIp3nS	předávat
fyzické	fyzický	k2eAgFnSc3d1	fyzická
vrstvě	vrstva	k1gFnSc3	vrstva
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
odeslání	odeslání	k1gNnSc4	odeslání
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
přijatých	přijatý	k2eAgInPc2d1	přijatý
rámců	rámec	k1gInPc2	rámec
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
adresu	adresa	k1gFnSc4	adresa
a	a	k8xC	a
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
součet	součet	k1gInSc4	součet
<g/>
.	.	kIx.	.
</s>
<s>
Rámce	rámec	k1gInPc1	rámec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nejsou	být	k5eNaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
aktuálnímu	aktuální	k2eAgInSc3d1	aktuální
uzlu	uzel	k1gInSc3	uzel
a	a	k8xC	a
chybné	chybný	k2eAgInPc4d1	chybný
rámce	rámec	k1gInPc4	rámec
zahazuje	zahazovat	k5eAaImIp3nS	zahazovat
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
jejich	jejich	k3xOp3gInSc4	jejich
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
přenos	přenos	k1gInSc4	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Seřazuje	seřazovat	k5eAaImIp3nS	seřazovat
přijaté	přijatý	k2eAgInPc4d1	přijatý
rámce	rámec	k1gInPc4	rámec
<g/>
,	,	kIx,	,
a	a	k8xC	a
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
jejich	jejich	k3xOp3gNnSc4	jejich
demultiplexování	demultiplexování	k1gNnSc4	demultiplexování
podle	podle	k7c2	podle
linkového	linkový	k2eAgInSc2d1	linkový
přístupového	přístupový	k2eAgInSc2d1	přístupový
bodu	bod	k1gInSc2	bod
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
uvedeného	uvedený	k2eAgInSc2d1	uvedený
protokolu	protokol	k1gInSc2	protokol
vyšší	vysoký	k2eAgFnSc2d2	vyšší
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Linková	linkový	k2eAgFnSc1d1	Linková
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
starat	starat	k5eAaImF	starat
o	o	k7c4	o
nastavení	nastavení	k1gNnSc4	nastavení
parametrů	parametr	k1gInPc2	parametr
přenosu	přenos	k1gInSc2	přenos
linky	linka	k1gFnPc1	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
linkové	linkový	k2eAgFnSc6d1	Linková
vrstvě	vrstva	k1gFnSc6	vrstva
pracují	pracovat	k5eAaImIp3nP	pracovat
mosty	most	k1gInPc4	most
(	(	kIx(	(
<g/>
bridge	bridgat	k5eAaPmIp3nS	bridgat
<g/>
)	)	kIx)	)
a	a	k8xC	a
přepínače	přepínač	k1gInPc1	přepínač
(	(	kIx(	(
<g/>
switch	switch	k1gInSc1	switch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Linkové	linkový	k2eAgFnPc1d1	Linková
vrstvy	vrstva	k1gFnPc1	vrstva
se	se	k3xPyFc4	se
také	také	k9	také
týkáLineární	týkáLineárnit	k5eAaPmIp3nS	týkáLineárnit
uspořádání	uspořádání	k1gNnSc4	uspořádání
Point-to-point	Pointooint	k1gMnSc1	Point-to-point
<g/>
,	,	kIx,	,
multipoint	multipoint	k1gMnSc1	multipoint
nebo	nebo	k8xC	nebo
point-to-multipoint	pointoultipoint	k1gMnSc1	point-to-multipoint
</s>
</p>
<p>
<s>
Topologie	topologie	k1gFnSc1	topologie
fyzické	fyzický	k2eAgFnSc2d1	fyzická
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
síť	síť	k1gFnSc1	síť
zapojená	zapojený	k2eAgFnSc1d1	zapojená
jako	jako	k8xC	jako
kruh	kruh	k1gInSc1	kruh
<g/>
,	,	kIx,	,
sběrnice	sběrnice	k1gFnSc1	sběrnice
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
nebo	nebo	k8xC	nebo
obecný	obecný	k2eAgInSc1d1	obecný
graf	graf	k1gInSc1	graf
</s>
</p>
<p>
<s>
Simplexní	simplexní	k2eAgNnSc4d1	simplexní
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
poloviční	poloviční	k2eAgInSc4d1	poloviční
duplex	duplex	k1gInSc4	duplex
nebo	nebo	k8xC	nebo
plný	plný	k2eAgInSc4d1	plný
duplex	duplex	k1gInSc4	duplex
</s>
</p>
<p>
<s>
Autonegotiation	Autonegotiation	k1gInSc1	Autonegotiation
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
Správný	správný	k2eAgInSc1d1	správný
český	český	k2eAgInSc1d1	český
terminus	terminus	k1gInSc1	terminus
technicus	technicus	k1gInSc4	technicus
pro	pro	k7c4	pro
druhou	druhý	k4xOgFnSc4	druhý
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
vrstvu	vrstva	k1gFnSc4	vrstva
referenčního	referenční	k2eAgInSc2d1	referenční
modelu	model	k1gInSc2	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
je	být	k5eAaImIp3nS	být
spojová	spojový	k2eAgFnSc1d1	spojová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vrstva	vrstva	k1gFnSc1	vrstva
datového	datový	k2eAgInSc2d1	datový
spoje	spoj	k1gInSc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
linková	linkový	k2eAgFnSc1d1	Linková
vrstva	vrstva	k1gFnSc1	vrstva
by	by	kYmCp3nS	by
spíše	spíše	k9	spíše
odpovídal	odpovídat	k5eAaImAgMnS	odpovídat
anglickému	anglický	k2eAgInSc3d1	anglický
termínu	termín	k1gInSc2	termín
link	linka	k1gFnPc2	linka
layer	layero	k1gNnPc2	layero
používanému	používaný	k2eAgInSc3d1	používaný
v	v	k7c6	v
modelu	model	k1gInSc6	model
TCP	TCP	kA	TCP
<g/>
/	/	kIx~	/
<g/>
IP	IP	kA	IP
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
vrstvy	vrstva	k1gFnPc4	vrstva
pod	pod	k7c4	pod
Internet	Internet	k1gInSc4	Internet
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
.	.	kIx.	.
</s>
<s>
Zaměňování	zaměňování	k1gNnSc4	zaměňování
obou	dva	k4xCgInPc2	dva
názvů	název	k1gInPc2	název
však	však	k9	však
nepřináší	přinášet	k5eNaImIp3nS	přinášet
žádné	žádný	k3yNgFnPc4	žádný
praktické	praktický	k2eAgFnPc4d1	praktická
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podvrstva	Podvrstvo	k1gNnPc1	Podvrstvo
Media	medium	k1gNnSc2	medium
Access	Accessa	k1gFnPc2	Accessa
Control	Controla	k1gFnPc2	Controla
==	==	k?	==
</s>
</p>
<p>
<s>
MAC	Mac	kA	Mac
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
hardwarově	hardwarově	k6eAd1	hardwarově
závislá	závislý	k2eAgNnPc4d1	závislé
podvrstva	podvrstvo	k1gNnPc4	podvrstvo
linkové	linkový	k2eAgFnSc2d1	Linková
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAC	Mac	kA	Mac
zajišťujefyzické	zajišťujefyzický	k2eAgNnSc1d1	zajišťujefyzický
adresování	adresování	k1gNnSc1	adresování
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
řízení	řízení	k1gNnSc1	řízení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
médiu	médium	k1gNnSc3	médium
</s>
</p>
<p>
<s>
===	===	k?	===
Adresování	adresování	k1gNnSc2	adresování
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
na	na	k7c6	na
dvoubodových	dvoubodový	k2eAgFnPc6d1	dvoubodová
spojích	spoj	k1gFnPc6	spoj
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgNnSc1	žádný
adresy	adresa	k1gFnPc1	adresa
potřebné	potřebný	k2eAgFnPc1d1	potřebná
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
protokolu	protokol	k1gInSc2	protokol
linkové	linkový	k2eAgFnSc2d1	Linková
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nepoužívá	používat	k5eNaImIp3nS	používat
adresy	adresa	k1gFnPc4	adresa
je	být	k5eAaImIp3nS	být
protokol	protokol	k1gInSc1	protokol
MTP-	MTP-	k1gFnSc2	MTP-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protokol	protokol	k1gInSc1	protokol
SDLC	SDLC	kA	SDLC
navržený	navržený	k2eAgInSc1d1	navržený
pro	pro	k7c4	pro
spoje	spoj	k1gInPc4	spoj
typu	typ	k1gInSc2	typ
point-to-multipoint	pointoultipointo	k1gNnPc2	point-to-multipointo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
komunikace	komunikace	k1gFnSc1	komunikace
vždy	vždy	k6eAd1	vždy
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
mezi	mezi	k7c7	mezi
řídicí	řídicí	k2eAgFnSc7d1	řídicí
stanicí	stanice	k1gFnSc7	stanice
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
podřízených	podřízený	k2eAgFnPc2d1	podřízená
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
adresu	adresa	k1gFnSc4	adresa
-	-	kIx~	-
adresu	adresa	k1gFnSc4	adresa
podřízené	podřízený	k2eAgFnSc2d1	podřízená
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
u	u	k7c2	u
dalších	další	k2eAgInPc2d1	další
protokolů	protokol	k1gInPc2	protokol
z	z	k7c2	z
SDLC	SDLC	kA	SDLC
odvozených	odvozený	k2eAgFnPc2d1	odvozená
<g/>
,	,	kIx,	,
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
HDLC	HDLC	kA	HDLC
<g/>
,	,	kIx,	,
ADCCP	ADCCP	kA	ADCCP
<g/>
,	,	kIx,	,
LAPM	LAPM	kA	LAPM
<g/>
,	,	kIx,	,
LAPD	LAPD	kA	LAPD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lokálních	lokální	k2eAgFnPc6d1	lokální
sítích	síť	k1gFnPc6	síť
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
rámce	rámec	k1gInPc1	rámec
adresu	adresa	k1gFnSc4	adresa
odesilatele	odesilatel	k1gMnSc2	odesilatel
a	a	k8xC	a
adresu	adresa	k1gFnSc4	adresa
příjemce	příjemce	k1gMnSc2	příjemce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Metody	metoda	k1gFnSc2	metoda
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
médiu	médium	k1gNnSc3	médium
===	===	k?	===
</s>
</p>
<p>
<s>
deterministické	deterministický	k2eAgFnPc1d1	deterministická
–	–	k?	–
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
maximální	maximální	k2eAgInSc4d1	maximální
časový	časový	k2eAgInSc4d1	časový
interval	interval	k1gInSc4	interval
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
pracovní	pracovní	k2eAgFnSc1d1	pracovní
stanice	stanice	k1gFnSc1	stanice
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
médiu	médium	k1gNnSc3	médium
</s>
</p>
<p>
<s>
Token	Token	k2eAgInSc1d1	Token
passing	passing	k1gInSc1	passing
(	(	kIx(	(
<g/>
předávání	předávání	k1gNnSc1	předávání
peška	pešek	k1gInSc2	pešek
<g/>
)	)	kIx)	)
–	–	k?	–
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
ztratit	ztratit	k5eAaPmF	ztratit
pešek	pešek	k1gInSc4	pešek
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
chybně	chybně	k6eAd1	chybně
objeví	objevit	k5eAaPmIp3nS	objevit
další	další	k2eAgNnSc1d1	další
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
Monitor	monitor	k1gInSc1	monitor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
předávání	předávání	k1gNnSc4	předávání
sleduje	sledovat	k5eAaImIp3nS	sledovat
a	a	k8xC	a
řídí	řídit	k5eAaImIp3nS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Monitor	monitor	k1gInSc1	monitor
je	být	k5eAaImIp3nS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
dohodou	dohoda	k1gFnSc7	dohoda
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vypne	vypnout	k5eAaPmIp3nS	vypnout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
aktivní	aktivní	k2eAgMnPc1d1	aktivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Implementace	implementace	k1gFnSc1	implementace
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
(	(	kIx(	(
<g/>
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polling	Polling	k1gInSc1	Polling
(	(	kIx(	(
<g/>
řízený	řízený	k2eAgInSc1d1	řízený
přístup	přístup	k1gInSc1	přístup
<g/>
)	)	kIx)	)
–	–	k?	–
řídící	řídící	k2eAgInSc4d1	řídící
počítač	počítač	k1gInSc4	počítač
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
stanice	stanice	k1gFnSc1	stanice
bude	být	k5eAaImBp3nS	být
vysílat	vysílat	k5eAaImF	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
spolehlivý	spolehlivý	k2eAgInSc1d1	spolehlivý
řídící	řídící	k2eAgInSc1d1	řídící
počítač	počítač	k1gInSc1	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
stochastické	stochastický	k2eAgFnPc1d1	stochastická
(	(	kIx(	(
<g/>
contention	contention	k1gInSc1	contention
=	=	kIx~	=
soupeření	soupeření	k1gNnSc1	soupeření
<g/>
)	)	kIx)	)
–	–	k?	–
nelze	lze	k6eNd1	lze
zaručit	zaručit	k5eAaPmF	zaručit
časový	časový	k2eAgInSc4d1	časový
interval	interval	k1gInSc4	interval
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
stanice	stanice	k1gFnSc1	stanice
musí	muset	k5eAaImIp3nS	muset
čekat	čekat	k5eAaImF	čekat
</s>
</p>
<p>
<s>
CSMA	CSMA	kA	CSMA
<g/>
/	/	kIx~	/
<g/>
CD	CD	kA	CD
(	(	kIx(	(
<g/>
Carrier	Carrier	k1gMnSc1	Carrier
Sense	Sense	k1gFnSc2	Sense
Multiple	multipl	k1gInSc5	multipl
Access	Access	k1gInSc1	Access
<g/>
/	/	kIx~	/
<g/>
Collision	Collision	k1gInSc1	Collision
Detection	Detection	k1gInSc1	Detection
<g/>
)	)	kIx)	)
–	–	k?	–
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
vyslat	vyslat	k5eAaPmF	vyslat
datový	datový	k2eAgInSc4d1	datový
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
naslouchá	naslouchat	k5eAaImIp3nS	naslouchat
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
a	a	k8xC	a
když	když	k8xS	když
žádná	žádný	k3yNgFnSc1	žádný
stanice	stanice	k1gFnSc1	stanice
nevysílá	vysílat	k5eNaImIp3nS	vysílat
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
vysílat	vysílat	k5eAaImF	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Zahájí	zahájit	k5eAaPmIp3nS	zahájit
<g/>
-li	i	k?	-li
vysílání	vysílání	k1gNnSc1	vysílání
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
okamžiku	okamžik	k1gInSc6	okamžik
i	i	k9	i
jiná	jiný	k2eAgFnSc1d1	jiná
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
kolizi	kolize	k1gFnSc3	kolize
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
detekci	detekce	k1gFnSc6	detekce
kolize	kolize	k1gFnSc2	kolize
stanice	stanice	k1gFnSc2	stanice
vyšle	vyslat	k5eAaPmIp3nS	vyslat
kolizní	kolizní	k2eAgInSc1d1	kolizní
signál	signál	k1gInSc1	signál
(	(	kIx(	(
<g/>
JAM	jam	k1gInSc1	jam
PATTERN	PATTERN	kA	PATTERN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
stanice	stanice	k1gFnPc1	stanice
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
kolizního	kolizní	k2eAgInSc2d1	kolizní
signálu	signál	k1gInSc2	signál
obdržený	obdržený	k2eAgInSc4d1	obdržený
rámec	rámec	k1gInSc4	rámec
stornují	stornovat	k5eAaBmIp3nP	stornovat
<g/>
.	.	kIx.	.
</s>
<s>
Vysílající	vysílající	k2eAgFnSc1d1	vysílající
stanice	stanice	k1gFnSc1	stanice
se	se	k3xPyFc4	se
odmlčí	odmlčet	k5eAaPmIp3nS	odmlčet
na	na	k7c4	na
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
kolizi	kolize	k1gFnSc3	kolize
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
kolizi	kolize	k1gFnSc4	kolize
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
všechna	všechen	k3xTgNnPc4	všechen
zařízení	zařízení	k1gNnPc4	zařízení
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
minimální	minimální	k2eAgFnSc1d1	minimální
délka	délka	k1gFnSc1	délka
rámce	rámec	k1gInSc2	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
klasický	klasický	k2eAgInSc4d1	klasický
Ethernet	Ethernet	k1gInSc4	Ethernet
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgFnSc1d1	minimální
délka	délka	k1gFnSc1	délka
rámce	rámec	k1gInSc2	rámec
64	[number]	k4	64
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
započítat	započítat	k5eAaPmF	započítat
7	[number]	k4	7
bitů	bit	k1gInPc2	bit
preambule	preambule	k1gFnSc2	preambule
a	a	k8xC	a
1	[number]	k4	1
synchronizační	synchronizační	k2eAgInSc4d1	synchronizační
bit	bit	k1gInSc4	bit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rychlost	rychlost	k1gFnSc1	rychlost
šíření	šíření	k1gNnSc2	šíření
signálu	signál	k1gInSc2	signál
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
<g/>
,	,	kIx,	,
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kolizi	kolize	k1gFnSc4	kolize
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
všechna	všechen	k3xTgNnPc4	všechen
zařízení	zařízení	k1gNnPc4	zařízení
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
<g/>
,	,	kIx,	,
omezuje	omezovat	k5eAaImIp3nS	omezovat
fyzické	fyzický	k2eAgInPc4d1	fyzický
rozměry	rozměr	k1gInPc4	rozměr
linky	linka	k1gFnSc2	linka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kolizní	kolizní	k2eAgFnSc1d1	kolizní
doména	doména	k1gFnSc1	doména
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
sítě	síť	k1gFnSc2	síť
typu	typ	k1gInSc2	typ
Ethernet	Etherneta	k1gFnPc2	Etherneta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
propojena	propojit	k5eAaPmNgFnS	propojit
pouze	pouze	k6eAd1	pouze
repeatery	repeater	k1gMnPc7	repeater
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kolize	kolize	k1gFnPc1	kolize
jsou	být	k5eAaImIp3nP	být
normální	normální	k2eAgFnSc7d1	normální
funkční	funkční	k2eAgFnSc7d1	funkční
záležitostí	záležitost	k1gFnSc7	záležitost
této	tento	k3xDgFnSc2	tento
přístupové	přístupový	k2eAgFnSc2d1	přístupová
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velkém	velký	k2eAgInSc6d1	velký
počtu	počet	k1gInSc6	počet
stanic	stanice	k1gFnPc2	stanice
jejich	jejich	k3xOp3gFnSc4	jejich
četnost	četnost	k1gFnSc4	četnost
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
průchodnost	průchodnost	k1gFnSc1	průchodnost
linky	linka	k1gFnSc2	linka
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
kolizi	kolize	k1gFnSc3	kolize
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
po	po	k7c6	po
minimální	minimální	k2eAgFnSc6d1	minimální
délce	délka	k1gFnSc6	délka
rámce	rámec	k1gInSc2	rámec
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tak	tak	k6eAd1	tak
zvanou	zvaný	k2eAgFnSc4d1	zvaná
pozdní	pozdní	k2eAgFnSc4d1	pozdní
kolizi	kolize	k1gFnSc4	kolize
(	(	kIx(	(
<g/>
late	lat	k1gInSc5	lat
collision	collision	k1gInSc1	collision
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
buď	buď	k8xC	buď
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
kolizní	kolizní	k2eAgFnSc2d1	kolizní
domény	doména	k1gFnSc2	doména
překračuje	překračovat	k5eAaImIp3nS	překračovat
povolenou	povolený	k2eAgFnSc4d1	povolená
mez	mez	k1gFnSc4	mez
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
některé	některý	k3yIgNnSc1	některý
zařízení	zařízení	k1gNnSc1	zařízení
nedodržuje	dodržovat	k5eNaImIp3nS	dodržovat
linkovou	linkový	k2eAgFnSc4d1	Linková
disciplínu	disciplína	k1gFnSc4	disciplína
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
příznakem	příznak	k1gInSc7	příznak
elektrického	elektrický	k2eAgInSc2d1	elektrický
problému	problém	k1gInSc2	problém
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
odraz	odraz	k1gInSc1	odraz
vlivem	vlivem	k7c2	vlivem
chybějícího	chybějící	k2eAgInSc2d1	chybějící
terminátoru	terminátor	k1gInSc2	terminátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgFnSc1d2	častější
pozdní	pozdní	k2eAgFnSc1d1	pozdní
kolize	kolize	k1gFnSc1	kolize
obvykle	obvykle	k6eAd1	obvykle
zcela	zcela	k6eAd1	zcela
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
síťový	síťový	k2eAgInSc4d1	síťový
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CSMA	CSMA	kA	CSMA
<g/>
/	/	kIx~	/
<g/>
CA	ca	kA	ca
(	(	kIx(	(
<g/>
Carrier	Carrier	k1gMnSc1	Carrier
Sense	Sense	k1gFnSc2	Sense
Multiple	multipl	k1gInSc5	multipl
Aceess	Aceess	k1gInSc1	Aceess
<g/>
/	/	kIx~	/
<g/>
Collision	Collision	k1gInSc1	Collision
Avoidance	Avoidanec	k1gInSc2	Avoidanec
<g/>
)	)	kIx)	)
–	–	k?	–
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
volno	volno	k1gNnSc4	volno
<g/>
,	,	kIx,	,
oznámí	oznámit	k5eAaPmIp3nS	oznámit
vysílání	vysílání	k1gNnSc1	vysílání
<g/>
,	,	kIx,	,
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
kolizi	kolize	k1gFnSc3	kolize
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
kolizi	kolize	k1gFnSc3	kolize
vysílací	vysílací	k2eAgFnSc2d1	vysílací
sekvence	sekvence	k1gFnSc2	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Pomalá	pomalý	k2eAgFnSc1d1	pomalá
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podvrstva	Podvrstvo	k1gNnSc2	Podvrstvo
Logical	Logical	k1gFnSc2	Logical
Link	Link	k1gMnSc1	Link
Control	Control	k1gInSc1	Control
==	==	k?	==
</s>
</p>
<p>
<s>
LLC	LLC	kA	LLC
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
hardwarově	hardwarově	k6eAd1	hardwarově
nezávislá	závislý	k2eNgNnPc4d1	nezávislé
podvrstva	podvrstvo	k1gNnPc4	podvrstvo
linkové	linkový	k2eAgFnSc2d1	Linková
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LLC	LLC	kA	LLC
zajišťujemultiplexování	zajišťujemultiplexování	k1gNnSc1	zajišťujemultiplexování
protokolů	protokol	k1gInPc2	protokol
předávaných	předávaný	k2eAgInPc2d1	předávaný
MAC	Mac	kA	Mac
vrstvě	vrstva	k1gFnSc6	vrstva
(	(	kIx(	(
<g/>
při	při	k7c6	při
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
)	)	kIx)	)
a	a	k8xC	a
demultiplexování	demultiplexování	k1gNnSc1	demultiplexování
(	(	kIx(	(
<g/>
při	při	k7c6	při
příjmu	příjem	k1gInSc6	příjem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
řízení	řízení	k1gNnSc1	řízení
toku	tok	k1gInSc2	tok
</s>
</p>
<p>
<s>
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
proti	proti	k7c3	proti
chybám	chyba	k1gFnPc3	chyba
</s>
</p>
<p>
<s>
===	===	k?	===
Řízení	řízení	k1gNnSc4	řízení
toku	tok	k1gInSc2	tok
dat	datum	k1gNnPc2	datum
===	===	k?	===
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
s	s	k7c7	s
automatickým	automatický	k2eAgNnSc7d1	automatické
opakováním	opakování	k1gNnSc7	opakování
ARQ	ARQ	kA	ARQ
(	(	kIx(	(
<g/>
Automatic	Automatice	k1gFnPc2	Automatice
Repeat-reQuest	RepeateQuest	k1gInSc1	Repeat-reQuest
<g/>
)	)	kIx)	)
metodami	metoda	k1gFnPc7	metoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgNnSc1d1	jednotlivé
potvrzování	potvrzování	k1gNnSc1	potvrzování
(	(	kIx(	(
<g/>
Stop	stop	k1gInSc1	stop
<g/>
&	&	k?	&
<g/>
Wait	Wait	k1gInSc1	Wait
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kontinuální	kontinuální	k2eAgNnSc1d1	kontinuální
potvrzování	potvrzování	k1gNnSc1	potvrzování
s	s	k7c7	s
návratem	návrat	k1gInSc7	návrat
(	(	kIx(	(
<g/>
Go-Back-N	Go-Back-N	k1gFnSc1	Go-Back-N
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kontinuální	kontinuální	k2eAgNnSc1d1	kontinuální
potvrzování	potvrzování	k1gNnSc1	potvrzování
se	s	k7c7	s
selektivním	selektivní	k2eAgNnSc7d1	selektivní
opakováním	opakování	k1gNnSc7	opakování
(	(	kIx(	(
<g/>
Selective	Selectiv	k1gInSc5	Selectiv
Repeat	Repeat	k1gInSc4	Repeat
<g/>
)	)	kIx)	)
<g/>
více	hodně	k6eAd2	hodně
na	na	k7c6	na
následujících	následující	k2eAgFnPc6d1	následující
stránkách	stránka	k1gFnPc6	stránka
http://www.earchiv.cz/a92/a219c110.php3	[url]	k4	http://www.earchiv.cz/a92/a219c110.php3
</s>
</p>
<p>
<s>
===	===	k?	===
Detekce	detekce	k1gFnPc1	detekce
a	a	k8xC	a
korekce	korekce	k1gFnPc1	korekce
chyb	chyba	k1gFnPc2	chyba
===	===	k?	===
</s>
</p>
<p>
<s>
Realizace	realizace	k1gFnSc1	realizace
např.	např.	kA	např.
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
součet	součet	k1gInSc4	součet
(	(	kIx(	(
<g/>
CRC	CRC	kA	CRC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hammingovy	Hammingův	k2eAgInPc1d1	Hammingův
kódy	kód	k1gInPc1	kód
</s>
</p>
<p>
<s>
parita	parita	k1gFnSc1	parita
(	(	kIx(	(
<g/>
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
lichá	lichý	k2eAgFnSc1d1	lichá
<g/>
/	/	kIx~	/
<g/>
sudá	sudý	k2eAgFnSc1d1	sudá
<g/>
,	,	kIx,	,
křížová	křížový	k2eAgFnSc1d1	křížová
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Protokoly	protokol	k1gInPc4	protokol
==	==	k?	==
</s>
</p>
<p>
<s>
SDLC	SDLC	kA	SDLC
-	-	kIx~	-
Synchronous	Synchronous	k1gInSc1	Synchronous
Data	datum	k1gNnSc2	datum
Link	Linka	k1gFnPc2	Linka
Control	Controla	k1gFnPc2	Controla
</s>
</p>
<p>
<s>
HDLC	HDLC	kA	HDLC
–	–	k?	–
High-Level	High-Level	k1gInSc1	High-Level
Data	datum	k1gNnSc2	datum
Link	Linka	k1gFnPc2	Linka
Control	Controla	k1gFnPc2	Controla
</s>
</p>
<p>
<s>
LAPB	LAPB	kA	LAPB
–	–	k?	–
Link	Link	k1gInSc1	Link
Access	Access	k1gInSc1	Access
Procedure	Procedur	k1gInSc5	Procedur
Balanced	Balanced	k1gMnSc1	Balanced
</s>
</p>
<p>
<s>
MTP	MTP	kA	MTP
<g/>
,	,	kIx,	,
MTP-2	MTP-2	k1gFnSc1	MTP-2
–	–	k?	–
Message	Message	k1gInSc1	Message
Transfer	transfer	k1gInSc1	transfer
Part	part	k1gInSc1	part
<g/>
,	,	kIx,	,
Message	Message	k1gInSc1	Message
Transfer	transfer	k1gInSc1	transfer
Part	part	k1gInSc1	part
2	[number]	k4	2
</s>
</p>
<p>
<s>
Ethernet	Ethernet	k1gMnSc1	Ethernet
</s>
</p>
<p>
<s>
ODI	ODI	kA	ODI
–	–	k?	–
Open	Open	k1gInSc1	Open
Data-Link	Data-Link	k1gInSc1	Data-Link
Interface	interface	k1gInSc1	interface
</s>
</p>
<p>
<s>
NDIS	NDIS	kA	NDIS
–	–	k?	–
Network	network	k1gInSc1	network
Driver	driver	k1gInSc1	driver
Interface	interface	k1gInSc1	interface
Specification	Specification	k1gInSc4	Specification
</s>
</p>
<p>
<s>
SANA	SANA	kA	SANA
II	II	kA	II
</s>
</p>
<p>
<s>
==	==	k?	==
Zařízení	zařízení	k1gNnSc1	zařízení
==	==	k?	==
</s>
</p>
<p>
<s>
NIC	nic	k6eAd1	nic
–	–	k?	–
Network	network	k1gInSc1	network
Interface	interface	k1gInSc1	interface
Card	Card	k1gInSc1	Card
<g/>
,	,	kIx,	,
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
</s>
</p>
<p>
<s>
bridge	bridge	k1gNnSc1	bridge
-	-	kIx~	-
mosty	most	k1gInPc1	most
</s>
</p>
<p>
<s>
switch	switch	k1gInSc1	switch
-	-	kIx~	-
přepínače	přepínač	k1gInPc1	přepínač
</s>
</p>
