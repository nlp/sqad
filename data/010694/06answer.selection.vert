<s>
Linková	linkový	k2eAgFnSc1d1	Linková
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
spojová	spojový	k2eAgFnSc1d1	spojová
vrstva	vrstva	k1gFnSc1	vrstva
nebo	nebo	k8xC	nebo
vrstva	vrstva	k1gFnSc1	vrstva
datového	datový	k2eAgInSc2d1	datový
spoje	spoj	k1gInSc2	spoj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
data	datum	k1gNnPc4	datum
link	link	k1gMnSc1	link
layer	layer	k1gMnSc1	layer
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
vrstva	vrstva	k1gFnSc1	vrstva
v	v	k7c6	v
referenčním	referenční	k2eAgInSc6d1	referenční
modelu	model	k1gInSc6	model
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
OSI	OSI	kA	OSI
<g/>
.	.	kIx.	.
</s>
