<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
anatomie	anatomie	k1gFnSc2	anatomie
jako	jako	k8xC	jako
vědního	vědní	k2eAgInSc2d1	vědní
oboru	obor	k1gInSc2	obor
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
starověcí	starověký	k2eAgMnPc1d1	starověký
lékaři	lékař	k1gMnPc1	lékař
Herophilos	Herophilos	k1gMnSc1	Herophilos
a	a	k8xC	a
Erasistrakos	Erasistrakos	k1gMnSc1	Erasistrakos
<g/>
,	,	kIx,	,
představitelé	představitel	k1gMnPc1	představitel
alexandrijské	alexandrijský	k2eAgFnSc2d1	Alexandrijská
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
270	[number]	k4	270
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kromě	kromě	k7c2	kromě
mrtvol	mrtvola	k1gFnPc2	mrtvola
pitvali	pitvat	k5eAaImAgMnP	pitvat
údajně	údajně	k6eAd1	údajně
i	i	k8xC	i
odsouzence	odsouzenec	k1gMnPc4	odsouzenec
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
