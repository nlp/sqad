<s>
Harmonie	harmonie	k1gFnSc1	harmonie
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
melodií	melodie	k1gFnSc7	melodie
a	a	k8xC	a
rytmem	rytmus	k1gInSc7	rytmus
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
základním	základní	k2eAgInPc3d1	základní
prvkům	prvek	k1gInPc3	prvek
<g/>
,	,	kIx,	,
definujícím	definující	k2eAgInPc3d1	definující
hudební	hudební	k2eAgInSc1d1	hudební
projev	projev	k1gInSc4	projev
klasického	klasický	k2eAgInSc2d1	klasický
evropského	evropský	k2eAgInSc2d1	evropský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
