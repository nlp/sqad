<p>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
A13	A13	k1gFnSc2	A13
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Autobahn	Autobahn	k1gMnSc1	Autobahn
A13	A13	k1gMnSc1	A13
nebo	nebo	k8xC	nebo
Brenner	Brenner	k1gMnSc1	Brenner
Autobahn	Autobahn	k1gMnSc1	Autobahn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
35	[number]	k4	35
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
rakouská	rakouský	k2eAgFnSc1d1	rakouská
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
větvemi	větev	k1gFnPc7	větev
odpojuje	odpojovat	k5eAaImIp3nS	odpojovat
z	z	k7c2	z
dálnice	dálnice	k1gFnSc2	dálnice
A	a	k9	a
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
vede	vést	k5eAaImIp3nS	vést
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Sill	Sill	k1gMnSc1	Sill
jižním	jižní	k2eAgInSc7d1	jižní
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
italským	italský	k2eAgFnPc3d1	italská
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
překračuje	překračovat	k5eAaImIp3nS	překračovat
v	v	k7c6	v
Brennerském	Brennerský	k2eAgInSc6d1	Brennerský
průsmyku	průsmyk	k1gInSc6	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
italské	italský	k2eAgFnSc6d1	italská
straně	strana	k1gFnSc6	strana
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
dálnice	dálnice	k1gFnSc1	dálnice
A	a	k9	a
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
A13	A13	k1gMnSc2	A13
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
jako	jako	k8xS	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
horských	horský	k2eAgFnPc2d1	horská
dálnic	dálnice	k1gFnPc2	dálnice
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Patsch	Patscha	k1gFnPc2	Patscha
a	a	k8xC	a
Schönberg	Schönberg	k1gMnSc1	Schönberg
im	im	k?	im
Stubaital	Stubaital	k1gMnSc1	Stubaital
nedaleko	nedaleko	k7c2	nedaleko
Innsbrucku	Innsbruck	k1gInSc2	Innsbruck
přechází	přecházet	k5eAaImIp3nP	přecházet
údolí	údolí	k1gNnPc1	údolí
Wipptal	Wipptal	k1gMnSc1	Wipptal
po	po	k7c6	po
mostě	most	k1gInSc6	most
Europabrücke	Europabrück	k1gFnSc2	Europabrück
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
820	[number]	k4	820
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc7d1	maximální
výškou	výška	k1gFnSc7	výška
190	[number]	k4	190
m	m	kA	m
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgMnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1960	[number]	k4	1960
až	až	k9	až
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dálniční	dálniční	k2eAgFnSc2d1	dálniční
křižovatky	křižovatka	k1gFnSc2	křižovatka
==	==	k?	==
</s>
</p>
<p>
<s>
Innsbruck-Amras	Innsbruck-Amras	k1gMnSc1	Innsbruck-Amras
(	(	kIx(	(
<g/>
km	km	kA	km
0	[number]	k4	0
<g/>
;	;	kIx,	;
východní	východní	k2eAgFnSc1d1	východní
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
–	–	k?	–
dálnice	dálnice	k1gFnSc1	dálnice
A12	A12	k1gFnSc1	A12
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
45	[number]	k4	45
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
60	[number]	k4	60
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Innsbruck-Wilten	Innsbruck-Wilten	k2eAgMnSc1d1	Innsbruck-Wilten
(	(	kIx(	(
<g/>
km	km	kA	km
0	[number]	k4	0
<g/>
;	;	kIx,	;
západní	západní	k2eAgFnSc1d1	západní
větev	větev	k1gFnSc1	větev
<g/>
)	)	kIx)	)
–	–	k?	–
dálnice	dálnice	k1gFnSc1	dálnice
A12	A12	k1gFnSc1	A12
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
60	[number]	k4	60
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
533	[number]	k4	533
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Innsbruck-Berg	Innsbruck-Berg	k1gMnSc1	Innsbruck-Berg
Isel	Isel	k1gMnSc1	Isel
(	(	kIx(	(
<g/>
km	km	kA	km
2	[number]	k4	2
<g/>
)	)	kIx)	)
–	–	k?	–
spojení	spojení	k1gNnSc1	spojení
obou	dva	k4xCgFnPc2	dva
větví	větev	k1gFnPc2	větev
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Brenner	Brenner	k1gMnSc1	Brenner
Autobahn	Autobahn	k1gMnSc1	Autobahn
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dálnice	dálnice	k1gFnSc2	dálnice
A13	A13	k1gFnSc2	A13
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
A13	A13	k1gMnSc1	A13
Brenner	Brenner	k1gMnSc1	Brenner
Autobahn	Autobahn	k1gMnSc1	Autobahn
<g/>
,	,	kIx,	,
motorways-exitlists	motorwaysxitlists	k1gInSc1	motorways-exitlists
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
