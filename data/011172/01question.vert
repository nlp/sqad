<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
35	[number]	k4	35
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
rakouská	rakouský	k2eAgFnSc1d1	rakouská
dálnice	dálnice	k1gFnSc1	dálnice
,	,	kIx,	,
<g/>
která	který	k3yQgFnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
?	?	kIx.	?
</s>
