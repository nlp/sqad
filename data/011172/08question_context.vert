<s>
Dálnice	dálnice	k1gFnSc1	dálnice
A13	A13	k1gFnSc2	A13
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Autobahn	Autobahn	k1gMnSc1	Autobahn
A13	A13	k1gMnSc1	A13
nebo	nebo	k8xC	nebo
Brenner	Brenner	k1gMnSc1	Brenner
Autobahn	Autobahn	k1gMnSc1	Autobahn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
35	[number]	k4	35
kilometrů	kilometr	k1gInPc2	kilometr
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
rakouská	rakouský	k2eAgFnSc1d1	rakouská
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
Innsbrucku	Innsbruck	k1gInSc6	Innsbruck
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
větvemi	větev	k1gFnPc7	větev
odpojuje	odpojovat	k5eAaImIp3nS	odpojovat
z	z	k7c2	z
dálnice	dálnice	k1gFnSc2	dálnice
A	a	k9	a
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
