<s>
Balada	balada	k1gFnSc1
capartova	capartův	k2eAgFnSc1d1
</s>
<s>
Balada	balada	k1gFnSc1
capartova	capartův	k2eAgFnSc1d1
Země	země	k1gFnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
dokumentární	dokumentární	k2eAgInSc4d1
film	film	k1gInSc4
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
1969	#num#	k4
Balada	balada	k1gFnSc1
capartova	capartův	k2eAgFnSc1d1
na	na	k7c4
ČSFDNěkterá	ČSFDNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Balada	balada	k1gFnSc1
capartova	capartův	k2eAgInSc2d1
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
film	film	k1gInSc1
dokončený	dokončený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Film	film	k1gInSc1
je	být	k5eAaImIp3nS
sestaven	sestavit	k5eAaPmNgInS
z	z	k7c2
původních	původní	k2eAgInPc2d1
dokumentárních	dokumentární	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
z	z	k7c2
okupace	okupace	k1gFnSc2
Plzně	Plzeň	k1gFnSc2
Rudou	ruda	k1gFnSc7
armádou	armáda	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
materiály	materiál	k1gInPc4
natočili	natočit	k5eAaBmAgMnP
kameramani	kameraman	k1gMnPc1
František	František	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
Miloš	Miloš	k1gMnSc1
Havránek	Havránek	k1gMnSc1
za	za	k7c2
asistence	asistence	k1gFnSc2
Josefa	Josef	k1gMnSc2
Peška	Pešek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druzí	druhý	k4xOgMnPc1
dva	dva	k4xCgMnPc1
jmenovaní	jmenovaný	k1gMnPc1
byli	být	k5eAaImAgMnP
později	pozdě	k6eAd2
zakladateli	zakladatel	k1gMnPc7
plzeňského	plzeňský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
Krátkého	Krátkého	k2eAgInSc2d1
filmu	film	k1gInSc2
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Materiály	materiál	k1gInPc7
z	z	k7c2
mateřské	mateřský	k2eAgFnSc2d1
školy	škola	k1gFnSc2
byly	být	k5eAaImAgFnP
dotočeny	dotočit	k5eAaPmNgFnP
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
v	v	k7c6
režii	režie	k1gFnSc6
Ing.	ing.	kA
Milana	Milan	k1gMnSc2
Kazdy	Kazda	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
rovněž	rovněž	k6eAd1
režisérem	režisér	k1gMnSc7
původních	původní	k2eAgInPc2d1
dokumentárních	dokumentární	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
na	na	k7c6
festivalu	festival	k1gInSc6
v	v	k7c6
Přerově	Přerov	k1gInSc6
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
iniciativa	iniciativa	k1gFnSc1
stíhat	stíhat	k5eAaImF
autory	autor	k1gMnPc4
za	za	k7c4
hanobení	hanobení	k1gNnSc4
spřátelené	spřátelený	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
a	a	k8xC
vedoucích	vedoucí	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
ČSSR	ČSSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
jmenovaní	jmenovaný	k1gMnPc1
byli	být	k5eAaImAgMnP
zatčeni	zatknout	k5eAaPmNgMnP
a	a	k8xC
vyšetřováni	vyšetřovat	k5eAaImNgMnP
na	na	k7c6
StB	StB	k1gFnSc6
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
žaloba	žaloba	k1gFnSc1
vedena	veden	k2eAgFnSc1d1
pouze	pouze	k6eAd1
proti	proti	k7c3
režisérovi	režisér	k1gMnSc3
filmu	film	k1gInSc2
Milanu	Milan	k1gMnSc3
Kazdovi	Kazda	k1gMnSc3
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgMnPc1d1
zúčastnění	zúčastněný	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
překvalifikováni	překvalifikovat	k5eAaImNgMnP
na	na	k7c4
svědky	svědek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
vznikly	vzniknout	k5eAaPmAgInP
další	další	k2eAgInPc1d1
filmy	film	k1gInPc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Normalizace	normalizace	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
parafrázoval	parafrázovat	k5eAaBmAgInS
tehdejší	tehdejší	k2eAgInSc1d1
trend	trend	k1gInSc1
normalizace	normalizace	k1gFnSc2
vstupu	vstup	k1gInSc2
vojsk	vojsko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
film	film	k1gInSc1
„	„	k?
<g/>
Proč	proč	k6eAd1
<g/>
“	“	k?
bez	bez	k7c2
příkras	příkrasa	k1gFnPc2
ukazoval	ukazovat	k5eAaImAgInS
skutečné	skutečný	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
ve	v	k7c6
Škodě	škoda	k1gFnSc6
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
nebyl	být	k5eNaImAgInS
pouze	pouze	k6eAd1
kritický	kritický	k2eAgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
ukazoval	ukazovat	k5eAaImAgInS
i	i	k9
východisko	východisko	k1gNnSc4
z	z	k7c2
této	tento	k3xDgFnSc2
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
nabízel	nabízet	k5eAaImAgInS
útvar	útvar	k1gInSc1
hlavního	hlavní	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
podniku	podnik	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
otřesné	otřesný	k2eAgInPc4d1
záběry	záběr	k1gInPc4
z	z	k7c2
prostředí	prostředí	k1gNnSc2
závodu	závod	k1gInSc2
nebyl	být	k5eNaImAgMnS
film	film	k1gInSc4
nikdy	nikdy	k6eAd1
promítán	promítán	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
natočení	natočení	k1gNnSc4
těchto	tento	k3xDgInPc2
filmů	film	k1gInPc2
byl	být	k5eAaImAgMnS
Milan	Milan	k1gMnSc1
Kazda	Kazda	k1gMnSc1
podmínečně	podmínečně	k6eAd1
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
již	již	k9
nezveřejněným	zveřejněný	k2eNgInSc7d1
trestem	trest	k1gInSc7
byl	být	k5eAaImAgInS
zákaz	zákaz	k1gInSc1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
propuštění	propuštění	k1gNnSc4
ze	z	k7c2
zaměstnání	zaměstnání	k1gNnSc2
a	a	k8xC
přesunutí	přesunutí	k1gNnSc4
na	na	k7c4
manuální	manuální	k2eAgFnPc4d1
práce	práce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
postihlo	postihnout	k5eAaPmAgNnS
i	i	k9
všechny	všechen	k3xTgFnPc4
další	další	k2eAgFnPc4d1
zúčastněné	zúčastněný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milan	Milan	k1gMnSc1
Kazda	Kazda	k1gMnSc1
nemohl	moct	k5eNaImAgMnS
nikde	nikde	k6eAd1
ve	v	k7c6
filmu	film	k1gInSc6
pracovat	pracovat	k5eAaImF
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgMnPc1d1
byli	být	k5eAaImAgMnP
po	po	k7c6
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
vzati	vzat	k2eAgMnPc1d1
na	na	k7c4
milost	milost	k1gFnSc4
a	a	k8xC
pracovat	pracovat	k5eAaImF
mohli	moct	k5eAaImAgMnP
pouze	pouze	k6eAd1
pod	pod	k7c7
dohledem	dohled	k1gInSc7
prověřených	prověřený	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Omezení	omezení	k1gNnSc1
bylo	být	k5eAaImAgNnS
zrušeno	zrušit	k5eAaPmNgNnS
až	až	k9
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Film	film	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
producenta	producent	k1gMnSc2
</s>
<s>
Film	film	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
ČT	ČT	kA
</s>
<s>
Balada	balada	k1gFnSc1
capartova	capartův	k2eAgFnSc1d1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
