<p>
<s>
Samba	samba	k1gFnSc1	samba
je	být	k5eAaImIp3nS	být
latinskoamerický	latinskoamerický	k2eAgInSc4d1	latinskoamerický
tanec	tanec	k1gInSc4	tanec
na	na	k7c4	na
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
takt	takt	k1gInSc1	takt
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
kolem	kolem	k7c2	kolem
50	[number]	k4	50
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
samba	samba	k1gFnSc1	samba
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
portugalském	portugalský	k2eAgNnSc6d1	portugalské
slově	slovo	k1gNnSc6	slovo
semba	semba	k1gFnSc1	semba
a	a	k8xC	a
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
božská	božský	k2eAgFnSc1d1	božská
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
variací	variace	k1gFnPc2	variace
samby	samba	k1gFnSc2	samba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
karnevalová	karnevalový	k2eAgFnSc1d1	karnevalová
samba	samba	k1gFnSc1	samba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
můžete	moct	k5eAaImIp2nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
karnevalech	karneval	k1gInPc6	karneval
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Figury	figura	k1gFnSc2	figura
==	==	k?	==
</s>
</p>
<p>
<s>
Figury	figura	k1gFnPc1	figura
podle	podle	k7c2	podle
ČSTS	ČSTS	kA	ČSTS
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
-	-	kIx~	-
Basic	Basic	kA	Basic
Movement	Movement	k1gMnSc1	Movement
</s>
</p>
<p>
<s>
vpravo	vpravo	k6eAd1	vpravo
-	-	kIx~	-
Natural	Natural	k?	Natural
</s>
</p>
<p>
<s>
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Reverse	reverse	k1gFnSc1	reverse
</s>
</p>
<p>
<s>
stranou	stranou	k6eAd1	stranou
-	-	kIx~	-
Side	Side	k1gFnSc1	Side
</s>
</p>
<p>
<s>
alternativní	alternativní	k2eAgInPc4d1	alternativní
-	-	kIx~	-
Alternative	Alternativ	k1gInSc5	Alternativ
</s>
</p>
<p>
<s>
Postupový	postupový	k2eAgInSc1d1	postupový
základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
-	-	kIx~	-
Progressive	Progressiev	k1gFnSc2	Progressiev
Basic	Basic	kA	Basic
Movement	Movement	k1gMnSc1	Movement
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc4d1	základní
pohyb	pohyb	k1gInSc4	pohyb
mimo	mimo	k7c4	mimo
-	-	kIx~	-
Outside	Outsid	k1gInSc5	Outsid
Basic	Basic	kA	Basic
Movement	Movement	k1gMnSc1	Movement
</s>
</p>
<p>
<s>
Zášvihy	zášvih	k1gInPc4	zášvih
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Whisks	Whisks	k1gInSc1	Whisks
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
Sambová	Sambový	k2eAgFnSc1d1	Sambový
chůze	chůze	k1gFnSc1	chůze
-	-	kIx~	-
Samba	samba	k1gFnSc1	samba
Walks	Walksa	k1gFnPc2	Walksa
</s>
</p>
<p>
<s>
v	v	k7c6	v
promenádním	promenádní	k2eAgNnSc6d1	promenádní
postavení	postavení	k1gNnSc6	postavení
-	-	kIx~	-
in	in	k?	in
Promenade	Promenad	k1gInSc5	Promenad
Position	Position	k1gInSc4	Position
</s>
</p>
<p>
<s>
stranou	stranou	k6eAd1	stranou
-	-	kIx~	-
Side	Side	k1gFnSc1	Side
</s>
</p>
<p>
<s>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
-	-	kIx~	-
Stationary	Stationar	k1gInPc4	Stationar
</s>
</p>
<p>
<s>
Nůžky	nůžky	k1gFnPc1	nůžky
-	-	kIx~	-
Bota	bota	k1gFnSc1	bota
Fogos	Fogosa	k1gFnPc2	Fogosa
</s>
</p>
<p>
<s>
Postupové	postupový	k2eAgFnPc1d1	postupová
nůžky	nůžky	k1gFnPc1	nůžky
-	-	kIx~	-
Travelling	Travelling	k1gInSc1	Travelling
Bota	bota	k1gFnSc1	bota
Fogos	Fogos	k1gMnSc1	Fogos
/	/	kIx~	/
Forward	Forward	k1gMnSc1	Forward
<g/>
,	,	kIx,	,
<g/>
Backward	Backward	k1gMnSc1	Backward
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Promenádní	promenádní	k2eAgFnPc1d1	promenádní
nůžky	nůžky	k1gFnPc1	nůžky
-	-	kIx~	-
Promenade	Promenad	k1gInSc5	Promenad
Bota	bota	k1gFnSc1	bota
Fogos	Fogos	k1gInSc1	Fogos
/	/	kIx~	/
nůžky	nůžky	k1gFnPc1	nůžky
do	do	k7c2	do
promenádního	promenádní	k2eAgNnSc2d1	promenádní
a	a	k8xC	a
obráceného	obrácený	k2eAgNnSc2d1	obrácené
promenádního	promenádní	k2eAgNnSc2d1	promenádní
postavení	postavení	k1gNnSc2	postavení
-	-	kIx~	-
Bota	bota	k1gFnSc1	bota
Fogos	Fogosa	k1gFnPc2	Fogosa
to	ten	k3xDgNnSc1	ten
Promenade	Promenad	k1gInSc5	Promenad
Position	Position	k1gInSc4	Position
and	and	k?	and
Counter	Counter	k1gInSc1	Counter
Promenade	Promenad	k1gInSc5	Promenad
Position	Position	k1gInSc4	Position
</s>
</p>
<p>
<s>
Stínové	stínový	k2eAgFnPc1d1	stínová
nůžky	nůžky	k1gFnPc1	nůžky
-	-	kIx~	-
Shadow	Shadow	k1gFnSc1	Shadow
Bota	bota	k1gFnSc1	bota
Fogos	Fogos	k1gInSc4	Fogos
</s>
</p>
<p>
<s>
Protisměrné	protisměrný	k2eAgFnPc1d1	protisměrná
nůžky	nůžky	k1gFnPc1	nůžky
-	-	kIx~	-
Contra	Contra	k1gFnSc1	Contra
Bota	bota	k1gFnSc1	bota
Fogos	Fogos	k1gInSc4	Fogos
</s>
</p>
<p>
<s>
nůžky	nůžky	k1gFnPc1	nůžky
ve	v	k7c6	v
stínovém	stínový	k2eAgInSc6d1	stínový
postavení	postavení	k1gNnSc2	postavení
stejnou	stejný	k2eAgFnSc7d1	stejná
nohou	noha	k1gFnSc7	noha
-	-	kIx~	-
Bota	bota	k1gFnSc1	bota
Fogos	Fogos	k1gMnSc1	Fogos
in	in	k?	in
Shadow	Shadow	k1gMnSc4	Shadow
Position	Position	k1gInSc1	Position
on	on	k3xPp3gMnSc1	on
Same	Sam	k1gMnSc5	Sam
Foot	Foot	k1gMnSc1	Foot
</s>
</p>
<p>
<s>
Otáčka	otáčka	k1gFnSc1	otáčka
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Reverse	reverse	k1gFnSc1	reverse
Turn	Turna	k1gFnPc2	Turna
</s>
</p>
<p>
<s>
Valení	valení	k1gNnSc4	valení
vpravo	vpravo	k6eAd1	vpravo
-	-	kIx~	-
Natural	Natural	k?	Natural
Roll	Roll	k1gInSc1	Roll
</s>
</p>
<p>
<s>
Kortadžaka	Kortadžak	k1gMnSc4	Kortadžak
-	-	kIx~	-
Corta	Cort	k1gMnSc4	Cort
Jaca	Jacus	k1gMnSc4	Jacus
</s>
</p>
<p>
<s>
Kolébky	kolébka	k1gFnPc1	kolébka
-	-	kIx~	-
Rocks	Rocks	k1gInSc1	Rocks
</s>
</p>
<p>
<s>
zavřené	zavřený	k2eAgFnPc1d1	zavřená
-	-	kIx~	-
Closed	Closed	k1gInSc1	Closed
</s>
</p>
<p>
<s>
otevřené	otevřený	k2eAgNnSc1d1	otevřené
-	-	kIx~	-
Open	Open	k1gNnSc1	Open
</s>
</p>
<p>
<s>
vzad	vzad	k6eAd1	vzad
-	-	kIx~	-
Backward	Backward	k1gInSc1	Backward
</s>
</p>
<p>
<s>
Voltové	voltový	k2eAgInPc1d1	voltový
pohyby	pohyb	k1gInPc1	pohyb
-	-	kIx~	-
Voltas	Voltas	k1gMnSc1	Voltas
Movement	Movement	k1gMnSc1	Movement
</s>
</p>
<p>
<s>
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
volta	volta	k1gFnSc1	volta
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Simple	Simple	k1gMnSc1	Simple
Voltas	Voltas	k1gMnSc1	Voltas
to	ten	k3xDgNnSc4	ten
Right	Right	k1gMnSc1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
křížem	křížem	k6eAd1	křížem
krážem	krážem	k6eAd1	krážem
-	-	kIx~	-
Criss	Criss	k1gInSc1	Criss
Cross	Crossa	k1gFnPc2	Crossa
</s>
</p>
<p>
<s>
postupující	postupující	k2eAgFnSc1d1	postupující
volta	volta	k1gFnSc1	volta
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Travelling	Travelling	k1gInSc1	Travelling
Voltas	Voltas	k1gInSc1	Voltas
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
májka	májka	k1gFnSc1	májka
-	-	kIx~	-
Maypole	Maypole	k1gFnSc1	Maypole
</s>
</p>
<p>
<s>
voltová	voltový	k2eAgFnSc1d1	voltová
otáčka	otáčka	k1gFnSc1	otáčka
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
pro	pro	k7c4	pro
dámu	dáma	k1gFnSc4	dáma
-	-	kIx~	-
Voltas	Voltas	k1gInSc1	Voltas
Spot	spot	k1gInSc1	spot
Turn	Turn	k1gInSc1	Turn
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gInSc1	Left
for	forum	k1gNnPc2	forum
Lady	Lada	k1gFnSc2	Lada
</s>
</p>
<p>
<s>
sólová	sólový	k2eAgFnSc1d1	sólová
volta	volta	k1gFnSc1	volta
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Solo	Solo	k6eAd1	Solo
volta	volta	k1gFnSc1	volta
Spot	spot	k1gInSc4	spot
Turns	Turns	k1gInSc1	Turns
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
pokračující	pokračující	k2eAgFnSc1d1	pokračující
volta	volta	k1gFnSc1	volta
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Continuous	Continuous	k1gInSc1	Continuous
volta	volta	k1gFnSc1	volta
Spot	spot	k1gInSc4	spot
Turn	Turn	k1gInSc1	Turn
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
kruhová	kruhový	k2eAgFnSc1d1	kruhová
volta	volta	k1gFnSc1	volta
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Circular	Circular	k1gMnSc1	Circular
Voltas	Voltas	k1gMnSc1	Voltas
to	ten	k3xDgNnSc4	ten
Right	Right	k1gMnSc1	Right
and	and	k?	and
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
kolotoč	kolotoč	k1gInSc1	kolotoč
-	-	kIx~	-
Rondabout	Rondabout	k1gMnSc1	Rondabout
</s>
</p>
<p>
<s>
volta	volta	k1gFnSc1	volta
ve	v	k7c6	v
stínovém	stínový	k2eAgNnSc6d1	stínové
postavení	postavení	k1gNnSc6	postavení
stejnou	stejný	k2eAgFnSc4d1	stejná
nohou	nohý	k2eAgFnSc4d1	nohá
vpravo	vpravo	k6eAd1	vpravo
a	a	k8xC	a
vlevo	vlevo	k6eAd1	vlevo
-	-	kIx~	-
Voltas	Voltas	k1gInSc1	Voltas
in	in	k?	in
Shadow	Shadow	k1gFnSc1	Shadow
Position	Position	k1gInSc1	Position
on	on	k3xPp3gMnSc1	on
Same	Sam	k1gMnSc5	Sam
Foot	Foot	k1gInSc1	Foot
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
or	or	k?	or
Left	Left	k1gMnSc1	Left
</s>
</p>
<p>
<s>
zavřená	zavřený	k2eAgFnSc1d1	zavřená
volta	volta	k1gFnSc1	volta
-	-	kIx~	-
Closed	Closed	k1gMnSc1	Closed
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Argentinská	argentinský	k2eAgNnPc1d1	argentinské
křížení	křížení	k1gNnPc1	křížení
-	-	kIx~	-
Argentine	Argentin	k1gMnSc5	Argentin
Crosses	Crosses	k1gMnSc1	Crosses
</s>
</p>
<p>
<s>
Pletenec	pletenec	k1gInSc1	pletenec
-	-	kIx~	-
Plait	Plait	k1gInSc1	Plait
</s>
</p>
<p>
<s>
Změny	změna	k1gFnPc1	změna
nohou	noha	k1gFnSc7	noha
-	-	kIx~	-
Foot	Foot	k2eAgInSc1d1	Foot
Changes	Changes	k1gInSc1	Changes
</s>
</p>
<p>
<s>
Odvalení	odvalení	k1gNnSc1	odvalení
z	z	k7c2	z
paže	paže	k1gFnSc2	paže
-	-	kIx~	-
Rolling	Rolling	k1gInSc1	Rolling
off	off	k?	off
the	the	k?	the
Arm	Arm	k1gFnSc2	Arm
</s>
</p>
<p>
<s>
Sambová	Sambový	k2eAgNnPc1d1	Sambový
křížení	křížení	k1gNnPc1	křížení
v	v	k7c6	v
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
promenádním	promenádní	k2eAgNnSc6d1	promenádní
postavení	postavení	k1gNnSc6	postavení
a	a	k8xC	a
otevřeném	otevřený	k2eAgNnSc6d1	otevřené
obráceném	obrácený	k2eAgNnSc6d1	obrácené
promenádním	promenádní	k2eAgNnSc6d1	promenádní
postavení	postavení	k1gNnSc6	postavení
-	-	kIx~	-
Samba	samba	k1gFnSc1	samba
Locks	Locks	k1gInSc1	Locks
Open	Open	k1gMnSc1	Open
PP	PP	kA	PP
and	and	k?	and
Open	Open	k1gInSc1	Open
CPP	CPP	kA	CPP
</s>
</p>
<p>
<s>
Běhy	běh	k1gInPc1	běh
v	v	k7c6	v
promenádě	promenáda	k1gFnSc6	promenáda
a	a	k8xC	a
obrácené	obrácený	k2eAgFnSc3d1	obrácená
promenádě	promenáda	k1gFnSc3	promenáda
-	-	kIx~	-
Promenade	Promenad	k1gInSc5	Promenad
to	ten	k3xDgNnSc1	ten
Counter	Countra	k1gFnPc2	Countra
Promenade	Promenad	k1gInSc5	Promenad
Runs	Runs	k1gInSc4	Runs
</s>
</p>
<p>
<s>
Cruzado	Cruzada	k1gFnSc5	Cruzada
křížení	křížení	k1gNnSc3	křížení
ve	v	k7c6	v
stínovém	stínový	k2eAgNnSc6d1	stínové
postavení	postavení	k1gNnSc6	postavení
-	-	kIx~	-
Cruzado	Cruzada	k1gFnSc5	Cruzada
Locks	Locks	k1gInSc1	Locks
in	in	k?	in
Shadow	Shadow	k1gFnSc1	Shadow
Position	Position	k1gInSc1	Position
</s>
</p>
<p>
<s>
Cruzado	Cruzada	k1gFnSc5	Cruzada
chůze	chůze	k1gFnSc5	chůze
-	-	kIx~	-
Cruzado	Cruzada	k1gFnSc5	Cruzada
Walks	Walksa	k1gFnPc2	Walksa
</s>
</p>
<p>
<s>
Tříkroková	tříkrokový	k2eAgFnSc1d1	tříkroková
otáčka	otáčka	k1gFnSc1	otáčka
-	-	kIx~	-
Three	Thre	k1gInPc1	Thre
Step	step	k1gFnSc1	step
Turn	Turn	k1gInSc1	Turn
</s>
</p>
<p>
<s>
Rytmické	rytmický	k2eAgNnSc1d1	rytmické
houpání	houpání	k1gNnSc1	houpání
-	-	kIx~	-
Rhythm	Rhythm	k1gInSc1	Rhythm
Bounce	Bounec	k1gInSc2	Bounec
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
samba	samba	k1gFnSc1	samba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
IDSF	IDSF	kA	IDSF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
rada	rada	k1gFnSc1	rada
tanečních	taneční	k2eAgMnPc2d1	taneční
profesionálů	profesionál	k1gMnPc2	profesionál
(	(	kIx(	(
<g/>
WDC	WDC	kA	WDC
<g/>
)	)	kIx)	)
</s>
</p>
