<p>
<s>
Kardiologie	kardiologie	k1gFnSc1	kardiologie
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc4d1	samostatný
lékařský	lékařský	k2eAgInSc4d1	lékařský
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyčleněný	vyčleněný	k2eAgInSc4d1	vyčleněný
z	z	k7c2	z
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
diagnostikou	diagnostika	k1gFnSc7	diagnostika
a	a	k8xC	a
terapií	terapie	k1gFnSc7	terapie
onemocnění	onemocnění	k1gNnSc2	onemocnění
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
diagnostiku	diagnostika	k1gFnSc4	diagnostika
a	a	k8xC	a
terapii	terapie	k1gFnSc4	terapie
srdečních	srdeční	k2eAgFnPc2d1	srdeční
vad	vada	k1gFnPc2	vada
<g/>
,	,	kIx,	,
ischemické	ischemický	k2eAgFnPc1d1	ischemická
choroby	choroba	k1gFnPc1	choroba
srdeční	srdeční	k2eAgFnPc4d1	srdeční
(	(	kIx(	(
<g/>
nepoměr	nepoměr	k1gInSc4	nepoměr
mezi	mezi	k7c7	mezi
potřebou	potřeba	k1gFnSc7	potřeba
a	a	k8xC	a
dodávkou	dodávka	k1gFnSc7	dodávka
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
srdečního	srdeční	k2eAgNnSc2d1	srdeční
selhání	selhání	k1gNnSc2	selhání
poruch	porucha	k1gFnPc2	porucha
srdečního	srdeční	k2eAgInSc2d1	srdeční
rytmu	rytmus	k1gInSc2	rytmus
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatních	ostatní	k2eAgNnPc2d1	ostatní
onemocnění	onemocnění	k1gNnPc2	onemocnění
srdce	srdce	k1gNnSc2	srdce
buď	buď	k8xC	buď
samostatně	samostatně	k6eAd1	samostatně
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
kardiochirurgií	kardiochirurgie	k1gFnSc7	kardiochirurgie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
samostatný	samostatný	k2eAgInSc1d1	samostatný
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
onemocněním	onemocnění	k1gNnSc7	onemocnění
cév	céva	k1gFnPc2	céva
se	se	k3xPyFc4	se
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
angiologie	angiologie	k1gFnSc1	angiologie
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
specializující	specializující	k2eAgMnSc1d1	specializující
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
obor	obor	k1gInSc4	obor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kardiolog	kardiolog	k1gMnSc1	kardiolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diagnostické	diagnostický	k2eAgFnPc1d1	diagnostická
metody	metoda	k1gFnPc1	metoda
kardiologie	kardiologie	k1gFnSc2	kardiologie
==	==	k?	==
</s>
</p>
<p>
<s>
Elektrokardiografie	elektrokardiografie	k1gFnSc1	elektrokardiografie
</s>
</p>
<p>
<s>
Měření	měření	k1gNnSc1	měření
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
</s>
</p>
<p>
<s>
Holterovské	Holterovský	k2eAgNnSc1d1	Holterovské
monitorování	monitorování	k1gNnSc1	monitorování
</s>
</p>
<p>
<s>
Tlakové	tlakový	k2eAgNnSc1d1	tlakové
holterovo	holterův	k2eAgNnSc1d1	holterův
monitorování	monitorování	k1gNnSc1	monitorování
</s>
</p>
<p>
<s>
Holterovo	Holterův	k2eAgNnSc1d1	Holterovo
monitorování	monitorování	k1gNnSc1	monitorování
EKG	EKG	kA	EKG
</s>
</p>
<p>
<s>
Echokardiografie	Echokardiografie	k1gFnSc1	Echokardiografie
</s>
</p>
<p>
<s>
Pravostranná	pravostranný	k2eAgFnSc1d1	pravostranná
a	a	k8xC	a
levostranná	levostranný	k2eAgFnSc1d1	levostranná
srdeční	srdeční	k2eAgFnSc1d1	srdeční
katetrizace	katetrizace	k1gFnSc1	katetrizace
</s>
</p>
<p>
<s>
Elektrofyziologické	Elektrofyziologický	k2eAgNnSc1d1	Elektrofyziologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
</s>
</p>
<p>
<s>
Další	další	k2eAgFnPc1d1	další
zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
metody	metoda	k1gFnPc1	metoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kardio-CT	Kardio-CT	k?	Kardio-CT
</s>
</p>
<p>
<s>
Kardio-MR	Kardio-MR	k?	Kardio-MR
</s>
</p>
<p>
<s>
==	==	k?	==
Kardiologická	kardiologický	k2eAgNnPc1d1	kardiologické
onemocnění	onemocnění	k1gNnPc1	onemocnění
==	==	k?	==
</s>
</p>
<p>
<s>
Srdeční	srdeční	k2eAgFnSc1d1	srdeční
arytmie	arytmie	k1gFnSc1	arytmie
</s>
</p>
<p>
<s>
Infarkt	infarkt	k1gInSc1	infarkt
myokardu	myokard	k1gInSc2	myokard
</s>
</p>
<p>
<s>
ICHS	ICHS	kA	ICHS
</s>
</p>
<p>
<s>
Hypertenze	hypertenze	k1gFnSc1	hypertenze
</s>
</p>
<p>
<s>
Hypotenze	hypotenze	k1gFnSc1	hypotenze
</s>
</p>
<p>
<s>
Ateroskleróza	ateroskleróza	k1gFnSc1	ateroskleróza
</s>
</p>
<p>
<s>
Kardiomyopatie	Kardiomyopatie	k1gFnSc1	Kardiomyopatie
</s>
</p>
<p>
<s>
Chlopení	Chlopení	k1gNnSc1	Chlopení
vady	vada	k1gFnSc2	vada
</s>
</p>
<p>
<s>
Srdeční	srdeční	k2eAgNnSc1d1	srdeční
selhání	selhání	k1gNnSc1	selhání
</s>
</p>
<p>
<s>
==	==	k?	==
Čeští	český	k2eAgMnPc1d1	český
kardiologové	kardiolog	k1gMnPc1	kardiolog
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Brod	Brod	k1gInSc1	Brod
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Ganz	Ganz	k1gMnSc1	Ganz
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kautzner	Kautzner	k1gMnSc1	Kautzner
</s>
</p>
<p>
<s>
Emerich	Emerich	k1gMnSc1	Emerich
Maixner	Maixner	k1gMnSc1	Maixner
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Maštálka	maštálka	k1gFnSc1	maštálka
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Neužil	Neužil	k1gMnSc1	Neužil
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Štejfa	Štejf	k1gMnSc2	Štejf
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Štejfa	Štejf	k1gMnSc2	Štejf
ml.	ml.	kA	ml.
</s>
</p>
<p>
<s>
Klement	Klement	k1gMnSc1	Klement
Weber	Weber	k1gMnSc1	Weber
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Zimmerman	Zimmerman	k1gMnSc1	Zimmerman
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kardiologie	kardiologie	k1gFnSc2	kardiologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kardiologie	kardiologie	k1gFnSc2	kardiologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
