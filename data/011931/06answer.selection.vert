<s>
Kardiologie	kardiologie	k1gFnSc1	kardiologie
je	být	k5eAaImIp3nS	být
samostatný	samostatný	k2eAgInSc4d1	samostatný
lékařský	lékařský	k2eAgInSc4d1	lékařský
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyčleněný	vyčleněný	k2eAgInSc4d1	vyčleněný
z	z	k7c2	z
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
diagnostikou	diagnostika	k1gFnSc7	diagnostika
a	a	k8xC	a
terapií	terapie	k1gFnSc7	terapie
onemocnění	onemocnění	k1gNnSc2	onemocnění
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
</s>
