<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
aktivních	aktivní	k2eAgMnPc2d1
středoškoláků	středoškolák	k1gMnPc2
zastupujících	zastupující	k2eAgFnPc2d1
žákovské	žákovský	k2eAgFnPc4d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ti	ten	k3xDgMnPc1
volali	volat	k5eAaImAgMnP
po	po	k7c6
změnách	změna	k1gFnPc6
českého	český	k2eAgInSc2d1
vzdělávacího	vzdělávací	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
posilování	posilování	k1gNnSc2
občanské	občanský	k2eAgFnSc2d1
angažovanosti	angažovanost	k1gFnSc2
a	a	k8xC
občanského	občanský	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
podpoře	podpora	k1gFnSc6
všeobecného	všeobecný	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
roli	role	k1gFnSc4
kritického	kritický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
reprezentativní	reprezentativní	k2eAgFnSc2d1
žákovské	žákovský	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
organizovaně	organizovaně	k6eAd1
a	a	k8xC
aktivně	aktivně	k6eAd1
prosazovala	prosazovat	k5eAaImAgFnS
jejich	jejich	k3xOp3gInPc4
názory	názor	k1gInPc4
a	a	k8xC
hájila	hájit	k5eAaImAgFnS
jejich	jejich	k3xOp3gNnPc4
práva	právo	k1gNnPc4
a	a	k8xC
zájmy	zájem	k1gInPc4
na	na	k7c6
lokální	lokální	k2eAgFnSc6d1
<g/>
,	,	kIx,
celostátní	celostátní	k2eAgFnSc6d1
i	i	k8xC
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>