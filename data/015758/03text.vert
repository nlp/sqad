<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ČSU	ČSU	kA
Vznik	vznik	k1gInSc1
</s>
<s>
2013	#num#	k4
Typ	typ	k1gInSc1
</s>
<s>
spolek	spolek	k1gInSc1
Účel	účel	k1gInSc1
</s>
<s>
Hájení	hájení	k1gNnSc4
a	a	k8xC
prosazování	prosazování	k1gNnSc4
zájmů	zájem	k1gInPc2
středoškoláků	středoškolák	k1gMnPc2
na	na	k7c6
celostátní	celostátní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Plovdivská	plovdivský	k2eAgFnSc1d1
3426	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Modřany	Modřany	k1gInPc1
<g/>
,	,	kIx,
143	#num#	k4
00	#num#	k4
Praha	Praha	k1gFnSc1
Předseda	předseda	k1gMnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
René	René	k1gMnSc1
Schilke	Schilke	k1gInSc4
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Nováček	Nováček	k1gMnSc1
2	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
</s>
<s>
Erika	Erika	k1gFnSc1
Kubálková	Kubálková	k1gFnSc1
Členové	člen	k1gMnPc1
předsednictva	předsednictvo	k1gNnSc2
</s>
<s>
Petr	Petr	k1gMnSc1
Franc	Franc	k1gMnSc1
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
Slaný	Slaný	k1gInSc4
Hlavní	hlavní	k2eAgInSc4d1
orgán	orgán	k1gInSc4
</s>
<s>
republikový	republikový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
Dobrovolníků	dobrovolník	k1gMnPc2
</s>
<s>
30	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
stredoskolskaunie	stredoskolskaunie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
xup	xup	k?
<g/>
9	#num#	k4
<g/>
gf	gf	k?
IČO	IČO	kA
</s>
<s>
01494813	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
,	,	kIx,
z.	z.	k?
s.	s.	k?
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
ČSU	ČSU	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nezávislým	závislý	k2eNgInSc7d1
spolkem	spolek	k1gInSc7
žáků	žák	k1gMnPc2
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
dobrovolně	dobrovolně	k6eAd1
sdružení	sdružený	k2eAgMnPc1d1
zástupci	zástupce	k1gMnPc1
samosprávných	samosprávný	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
žáků	žák	k1gMnPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
žákovských	žákovský	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
,	,	kIx,
rad	rada	k1gFnPc2
<g/>
,	,	kIx,
senátů	senát	k1gInPc2
a	a	k8xC
podobně	podobně	k6eAd1
pojmenovaných	pojmenovaný	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
působících	působící	k2eAgMnPc2d1
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
ve	v	k7c6
smyslu	smysl	k1gInSc6
§	§	k?
21	#num#	k4
odst	odsta	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
písm	písmo	k1gNnPc2
<g/>
.	.	kIx.
d	d	k?
<g/>
)	)	kIx)
školského	školský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSU	ČSU	kA
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
evropské	evropský	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
středoškolských	středoškolský	k2eAgFnPc2d1
unií	unie	k1gFnPc2
OBESSU	OBESSU	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracuje	spolupracovat	k5eAaImIp3nS
také	také	k9
se	s	k7c7
Studentskou	studentský	k2eAgFnSc7d1
komorou	komora	k1gFnSc7
Rady	rada	k1gFnSc2
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
Stálou	stálý	k2eAgFnSc7d1
konferencí	konference	k1gFnSc7
asociací	asociace	k1gFnSc7
ve	v	k7c6
vzdělávání	vzdělávání	k1gNnSc6
(	(	kIx(
<g/>
SKAV	SKAV	kA
<g/>
)	)	kIx)
či	či	k8xC
obecně	obecně	k6eAd1
prospěšnou	prospěšný	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
EDUin	EDUina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
aktivních	aktivní	k2eAgMnPc2d1
středoškoláků	středoškolák	k1gMnPc2
zastupujících	zastupující	k2eAgFnPc2d1
žákovské	žákovský	k2eAgFnPc4d1
samosprávy	samospráva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ti	ten	k3xDgMnPc1
volali	volat	k5eAaImAgMnP
po	po	k7c6
změnách	změna	k1gFnPc6
českého	český	k2eAgInSc2d1
vzdělávacího	vzdělávací	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
posilování	posilování	k1gNnSc2
občanské	občanský	k2eAgFnSc2d1
angažovanosti	angažovanost	k1gFnSc2
a	a	k8xC
občanského	občanský	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
podpoře	podpora	k1gFnSc6
všeobecného	všeobecný	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
s	s	k7c7
důrazem	důraz	k1gInSc7
na	na	k7c4
roli	role	k1gFnSc4
kritického	kritický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
po	po	k7c6
vytvoření	vytvoření	k1gNnSc6
reprezentativní	reprezentativní	k2eAgFnSc2d1
žákovské	žákovský	k2eAgFnSc2d1
platformy	platforma	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
organizovaně	organizovaně	k6eAd1
a	a	k8xC
aktivně	aktivně	k6eAd1
prosazovala	prosazovat	k5eAaImAgFnS
jejich	jejich	k3xOp3gInPc4
názory	názor	k1gInPc4
a	a	k8xC
hájila	hájit	k5eAaImAgFnS
jejich	jejich	k3xOp3gNnPc4
práva	právo	k1gNnPc4
a	a	k8xC
zájmy	zájem	k1gInPc4
na	na	k7c6
lokální	lokální	k2eAgFnSc6d1
<g/>
,	,	kIx,
celostátní	celostátní	k2eAgFnSc6d1
i	i	k8xC
mezinárodní	mezinárodní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
zakladatele	zakladatel	k1gMnSc2
spolku	spolek	k1gInSc2
jsou	být	k5eAaImIp3nP
považováni	považován	k2eAgMnPc1d1
Filip	Filip	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Papajanovský	Papajanovský	k2eAgMnSc1d1
a	a	k8xC
Sebastian	Sebastian	k1gMnSc1
Schupke	Schupk	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členství	členství	k1gNnSc1
a	a	k8xC
struktura	struktura	k1gFnSc1
</s>
<s>
Členská	členský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
demokraticky	demokraticky	k6eAd1
volených	volený	k2eAgMnPc2d1
delegátů	delegát	k1gMnPc2
žákovských	žákovský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
(	(	kIx(
<g/>
žákovské	žákovský	k2eAgInPc1d1
parlamenty	parlament	k1gInPc1
<g/>
,	,	kIx,
rady	rada	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgMnPc2d1
aktivních	aktivní	k2eAgMnPc2d1
středoškoláků	středoškolák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gInSc7
unie	unie	k1gFnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
každý	každý	k3xTgMnSc1
žák	žák	k1gMnSc1
gymnázia	gymnázium	k1gNnSc2
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc2d1
odborné	odborný	k2eAgFnSc2d1
školy	škola	k1gFnSc2
nebo	nebo	k8xC
středního	střední	k2eAgNnSc2d1
odborného	odborný	k2eAgNnSc2d1
učiliště	učiliště	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dovršil	dovršit	k5eAaPmAgInS
15	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlasovací	hlasovací	k2eAgInSc4d1
právo	právo	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
podmíněno	podmíněn	k2eAgNnSc1d1
pověřením	pověření	k1gNnPc3
žákovskou	žákovský	k2eAgFnSc7d1
samosprávou	samospráva	k1gFnSc7
(	(	kIx(
<g/>
člen	člen	k1gMnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
delegátem	delegát	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
demokratickými	demokratický	k2eAgInPc7d1
principy	princip	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3
orgánem	orgán	k1gInSc7
České	český	k2eAgFnSc2d1
středoškolské	středoškolský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
je	být	k5eAaImIp3nS
republikový	republikový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
delegáti	delegát	k1gMnPc1
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
všech	všecek	k3xTgFnPc6
zásadních	zásadní	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
směřování	směřování	k1gNnSc2
spolku	spolek	k1gInSc2
a	a	k8xC
volí	volit	k5eAaImIp3nP
jeho	jeho	k3xOp3gInPc4
další	další	k2eAgInPc4d1
orgány	orgán	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
jej	on	k3xPp3gInSc4
zástupci	zástupce	k1gMnPc7
studentských	studentský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
(	(	kIx(
<g/>
delegáti	delegát	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
každý	každý	k3xTgMnSc1
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
hlas	hlas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhodobou	dlouhodobý	k2eAgFnSc4d1
kontinuitu	kontinuita	k1gFnSc4
fungování	fungování	k1gNnSc2
organizace	organizace	k1gFnSc2
zajišťuje	zajišťovat	k5eAaImIp3nS
kontrolní	kontrolní	k2eAgFnSc1d1
a	a	k8xC
rozhodčí	rozhodčí	k1gMnPc4
orgán	orgán	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
je	být	k5eAaImIp3nS
dozorčí	dozorčí	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statutárním	statutární	k2eAgInSc7d1
a	a	k8xC
výkonným	výkonný	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
je	být	k5eAaImIp3nS
předsednictvo	předsednictvo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
sestává	sestávat	k5eAaImIp3nS
z	z	k7c2
předsedy	předseda	k1gMnSc2
<g/>
,	,	kIx,
dvou	dva	k4xCgMnPc6
místopředsedů	místopředseda	k1gMnPc2
spolku	spolek	k1gInSc2
a	a	k8xC
dvou	dva	k4xCgInPc2
dalších	další	k2eAgInPc2d1
členů	člen	k1gInPc2
předsednictva	předsednictvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cíle	cíl	k1gInPc1
</s>
<s>
Posláním	poslání	k1gNnSc7
České	český	k2eAgFnSc2d1
středoškolské	středoškolský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
je	být	k5eAaImIp3nS
zastřešovat	zastřešovat	k5eAaImF
<g/>
,	,	kIx,
aktivně	aktivně	k6eAd1
prosazovat	prosazovat	k5eAaImF
a	a	k8xC
hájit	hájit	k5eAaImF
názory	názor	k1gInPc4
<g/>
,	,	kIx,
zájmy	zájem	k1gInPc4
a	a	k8xC
potřeby	potřeba	k1gFnPc4
žáků	žák	k1gMnPc2
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
na	na	k7c6
místní	místní	k2eAgFnSc6d1
<g/>
,	,	kIx,
regionální	regionální	k2eAgFnSc6d1
<g/>
,	,	kIx,
celostátní	celostátní	k2eAgFnSc6d1
i	i	k8xC
mezinárodní	mezinárodní	k2eAgFnSc6d1
<g/>
/	/	kIx~
<g/>
evropské	evropský	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
(	(	kIx(
<g/>
skrze	skrze	k?
sdružení	sdružení	k1gNnSc1
OBESSU	OBESSU	kA
<g/>
)	)	kIx)
a	a	k8xC
rozvíjet	rozvíjet	k5eAaImF
liberálně	liberálně	k6eAd1
demokratické	demokratický	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Unie	unie	k1gFnSc2
od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
změny	změna	k1gFnPc4
českého	český	k2eAgInSc2d1
vzdělávacího	vzdělávací	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
v	v	k7c6
těchto	tento	k3xDgFnPc6
oblastech	oblast	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
aktivní	aktivní	k2eAgNnSc1d1
zapojení	zapojení	k1gNnSc1
středoškoláků	středoškolák	k1gMnPc2
do	do	k7c2
rozhodování	rozhodování	k1gNnSc2
o	o	k7c6
jejich	jejich	k3xOp3gNnSc6
vzdělávání	vzdělávání	k1gNnSc6
a	a	k8xC
školách	škola	k1gFnPc6
<g/>
;	;	kIx,
</s>
<s>
samosprávné	samosprávný	k2eAgInPc1d1
orgány	orgán	k1gInPc1
žáků	žák	k1gMnPc2
(	(	kIx(
<g/>
žákovské	žákovský	k2eAgInPc1d1
parlamenty	parlament	k1gInPc1
<g/>
,	,	kIx,
rady	rada	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
všeobecné	všeobecný	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
a	a	k8xC
kritické	kritický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
<g/>
;	;	kIx,
</s>
<s>
inovativní	inovativní	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
a	a	k8xC
pestrost	pestrost	k1gFnSc1
ve	v	k7c6
výuce	výuka	k1gFnSc6
a	a	k8xC
obecně	obecně	k6eAd1
ve	v	k7c6
vzdělávání	vzdělávání	k1gNnSc6
<g/>
;	;	kIx,
</s>
<s>
Kampaň	kampaň	k1gFnSc1
Revoluce	revoluce	k1gFnSc1
na	na	k7c6
střední	střední	k2eAgFnSc6d1
</s>
<s>
Předávání	předávání	k1gNnSc1
ceny	cena	k1gFnSc2
Gratias	Gratiasa	k1gFnPc2
Tibi	Tibi	k1gNnSc7
za	za	k7c4
kampaň	kampaň	k1gFnSc4
Revoluce	revoluce	k1gFnSc2
na	na	k7c4
střední	střední	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
kampaň	kampaň	k1gFnSc4
Revoluce	revoluce	k1gFnSc1
na	na	k7c4
střední	střední	k2eAgMnPc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
schválena	schválit	k5eAaPmNgFnS
na	na	k7c4
IX	IX	kA
<g/>
.	.	kIx.
republikovém	republikový	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
a	a	k8xC
představena	představit	k5eAaPmNgFnS
předsedou	předseda	k1gMnSc7
Štěpánem	Štěpán	k1gMnSc7
Kmentem	Kment	k1gMnSc7
a	a	k8xC
tehdejší	tehdejší	k2eAgFnSc1d1
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyní	místopředsedkyně	k1gFnSc7
Lenkou	Lenka	k1gFnSc7
Štěpánovou	Štěpánová	k1gFnSc7
na	na	k7c6
souběžné	souběžný	k2eAgFnSc6d1
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Té	ten	k3xDgFnSc2
se	se	k3xPyFc4
účastnili	účastnit	k5eAaImAgMnP
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
i	i	k9
zástupci	zástupce	k1gMnPc1
rodičů	rodič	k1gMnPc2
<g/>
,	,	kIx,
učitelů	učitel	k1gMnPc2
<g/>
,	,	kIx,
neziskového	ziskový	k2eNgInSc2d1
sektoru	sektor	k1gInSc2
nebo	nebo	k8xC
ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc1
v	v	k7c6
kampani	kampaň	k1gFnSc6
prosazuje	prosazovat	k5eAaImIp3nS
vzdělávání	vzdělávání	k1gNnSc4
zaměřené	zaměřený	k2eAgNnSc4d1
na	na	k7c4
žáka	žák	k1gMnSc4
(	(	kIx(
<g/>
student-centered	student-centered	k1gInSc1
learning	learning	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pedagogickou	pedagogický	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
konstruktivismu	konstruktivismus	k1gInSc2
nebo	nebo	k8xC
humanistické	humanistický	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
konceptů	koncept	k1gInPc2
Revoluce	revoluce	k1gFnSc2
na	na	k7c4
střední	střední	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
vytyčeno	vytyčit	k5eAaPmNgNnS
9	#num#	k4
konkrétních	konkrétní	k2eAgInPc2d1
kroků	krok	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
unie	unie	k1gFnSc1
navrhuje	navrhovat	k5eAaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
<g/>
:	:	kIx,
</s>
<s>
Posílení	posílení	k1gNnSc1
žákovských	žákovský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
(	(	kIx(
<g/>
podpora	podpora	k1gFnSc1
demokratizace	demokratizace	k1gFnSc2
škol	škola	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
Důraz	důraz	k1gInSc1
na	na	k7c4
kvalitu	kvalita	k1gFnSc4
učitele	učitel	k1gMnSc2
<g/>
,	,	kIx,
</s>
<s>
Důraz	důraz	k1gInSc1
na	na	k7c4
metodickou	metodický	k2eAgFnSc4d1
pestrost	pestrost	k1gFnSc4
ve	v	k7c6
výuce	výuka	k1gFnSc6
<g/>
,	,	kIx,
</s>
<s>
Snížení	snížení	k1gNnSc1
maximálního	maximální	k2eAgInSc2d1
počtu	počet	k1gInSc2
žáků	žák	k1gMnPc2
ve	v	k7c6
třídě	třída	k1gFnSc6
na	na	k7c4
24	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1
odměn	odměna	k1gFnPc2
za	za	k7c2
praxe	praxe	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
kvalita	kvalita	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Větší	veliký	k2eAgFnSc1d2
volitelnost	volitelnost	k1gFnSc1
vyučovaných	vyučovaný	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
Uznání	uznání	k1gNnSc1
jazykového	jazykový	k2eAgInSc2d1
certifikátu	certifikát	k1gInSc2
u	u	k7c2
státní	státní	k2eAgFnSc2d1
maturity	maturita	k1gFnSc2
<g/>
,	,	kIx,
</s>
<s>
Odvolatelnost	odvolatelnost	k1gFnSc4
vůči	vůči	k7c3
kázeňským	kázeňský	k2eAgNnPc3d1
opatřením	opatření	k1gNnPc3
<g/>
,	,	kIx,
</s>
<s>
Apolitické	apolitický	k2eAgNnSc4d1
jmenování	jmenování	k1gNnSc4
ředitelů	ředitel	k1gMnPc2
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kampaň	kampaň	k1gFnSc1
si	se	k3xPyFc3
získala	získat	k5eAaPmAgFnS
pozornost	pozornost	k1gFnSc4
v	v	k7c6
médiích	médium	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
předseda	předseda	k1gMnSc1
unie	unie	k1gFnSc2
o	o	k7c6
kampani	kampaň	k1gFnSc6
hovořil	hovořit	k5eAaImAgMnS
jako	jako	k9
o	o	k7c6
změně	změna	k1gFnSc6
v	v	k7c6
komunikaci	komunikace	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
jsou	být	k5eAaImIp3nP
středoškoláci	středoškolák	k1gMnPc1
připraveni	připravit	k5eAaPmNgMnP
diskutovat	diskutovat	k5eAaImF
o	o	k7c6
podobě	podoba	k1gFnSc6
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
jako	jako	k9
o	o	k7c4
kultivaci	kultivace	k1gFnSc4
dialogu	dialog	k1gInSc2
mezi	mezi	k7c7
aktéry	aktér	k1gMnPc7
ve	v	k7c6
vzdělávání	vzdělávání	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Server	server	k1gInSc1
Aktuálně	aktuálně	k6eAd1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
cz	cz	k?
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
s	s	k7c7
další	další	k2eAgFnSc7d1
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
nově	nově	k6eAd1
zvolenou	zvolený	k2eAgFnSc7d1
předsedkyní	předsedkyně	k1gFnSc7
o	o	k7c6
kampani	kampaň	k1gFnSc6
mj.	mj.	kA
píše	psát	k5eAaImIp3nS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
...	...	k?
<g/>
místo	místo	k7c2
naštvaného	naštvaný	k2eAgInSc2d1
protestu	protest	k1gInSc2
nabízejí	nabízet	k5eAaImIp3nP
(	(	kIx(
<g/>
středoškoláci	středoškolák	k1gMnPc1
<g/>
)	)	kIx)
konstruktivní	konstruktivní	k2eAgNnSc4d1
řešení	řešení	k1gNnSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
změnit	změnit	k5eAaPmF
české	český	k2eAgNnSc4d1
školství	školství	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odpovídalo	odpovídat	k5eAaImAgNnS
požadavkům	požadavek	k1gInPc3
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Revoluce	revoluce	k1gFnSc1
na	na	k7c6
střední	střední	k2eAgFnSc6d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
vyhrála	vyhrát	k5eAaPmAgFnS
cenu	cena	k1gFnSc4
Gratias	Gratias	k1gInSc1
Tibi	Tibi	k1gNnSc2
organizace	organizace	k1gFnSc2
Člověk	člověk	k1gMnSc1
v	v	k7c6
tísni	tíseň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dotazník	dotazník	k1gInSc1
k	k	k7c3
distanční	distanční	k2eAgFnSc3d1
výuce	výuka	k1gFnSc3
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
prováděla	provádět	k5eAaImAgFnS
dotazníkové	dotazníkový	k2eAgNnSc4d1
šetření	šetření	k1gNnSc4
ohledně	ohledně	k7c2
kvality	kvalita	k1gFnSc2
distanční	distanční	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
víceletých	víceletý	k2eAgNnPc6d1
gymnáziích	gymnázium	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
tohoto	tento	k3xDgNnSc2
šetření	šetření	k1gNnSc2
bylo	být	k5eAaImAgNnS
odhalit	odhalit	k5eAaPmF
a	a	k8xC
zmapovat	zmapovat	k5eAaPmF
silné	silný	k2eAgFnPc4d1
a	a	k8xC
slabé	slabý	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
distanční	distanční	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
a	a	k8xC
porovnat	porovnat	k5eAaPmF
změny	změna	k1gFnPc4
v	v	k7c6
její	její	k3xOp3gFnSc6
kvalitě	kvalita	k1gFnSc6
na	na	k7c6
jaře	jaro	k1gNnSc6
a	a	k8xC
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
respondentů	respondent	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
přispěli	přispět	k5eAaPmAgMnP
svými	svůj	k3xOyFgFnPc7
zkušenostmi	zkušenost	k1gFnPc7
<g/>
,	,	kIx,
dosahoval	dosahovat	k5eAaImAgInS
čísla	číslo	k1gNnSc2
9	#num#	k4
199	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
posouvá	posouvat	k5eAaImIp3nS
toto	tento	k3xDgNnSc4
dotazníkové	dotazníkový	k2eAgNnSc4d1
šetření	šetření	k1gNnSc4
na	na	k7c4
první	první	k4xOgFnSc4
příčku	příčka	k1gFnSc4
nejúspěšnějších	úspěšný	k2eAgNnPc2d3
dotazníkových	dotazníkový	k2eAgNnPc2d1
šetření	šetření	k1gNnPc2
ČSU	ČSU	kA
<g/>
.	.	kIx.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
byly	být	k5eAaImAgInP
postupně	postupně	k6eAd1
zpracovány	zpracovat	k5eAaPmNgInP
a	a	k8xC
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
základě	základ	k1gInSc6
byla	být	k5eAaImAgFnS
sepsána	sepsán	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
z	z	k7c2
dotazníku	dotazník	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
ČSU	ČSU	kA
zmiňuje	zmiňovat	k5eAaImIp3nS
hlavní	hlavní	k2eAgInPc1d1
problémy	problém	k1gInPc1
spojené	spojený	k2eAgInPc1d1
s	s	k7c7
distančním	distanční	k2eAgNnSc7d1
vzděláváním	vzdělávání	k1gNnSc7
a	a	k8xC
skrze	skrze	k?
tiskovou	tiskový	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
apeluje	apelovat	k5eAaImIp3nS
na	na	k7c4
Ministerstvo	ministerstvo	k1gNnSc4
školství	školství	k1gNnSc2
a	a	k8xC
ředitele	ředitel	k1gMnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
této	tento	k3xDgFnSc3
problematice	problematika	k1gFnSc3
věnovali	věnovat	k5eAaImAgMnP,k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Dle	dle	k7c2
studentů	student	k1gMnPc2
se	se	k3xPyFc4
kvalita	kvalita	k1gFnSc1
distanční	distanční	k2eAgFnSc2d1
výuky	výuka	k1gFnSc2
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
jarem	jaro	k1gNnSc7
sice	sice	k8xC
zlepšila	zlepšit	k5eAaPmAgFnS
<g/>
,	,	kIx,
stále	stále	k6eAd1
má	mít	k5eAaImIp3nS
však	však	k9
spoustu	spousta	k1gFnSc4
nedostatků	nedostatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
ty	ty	k3xPp2nSc1
hlavní	hlavní	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
uvést	uvést	k5eAaPmF
zhoršené	zhoršený	k2eAgNnSc4d1
psychické	psychický	k2eAgNnSc4d1
zdraví	zdraví	k1gNnSc4
studentů	student	k1gMnPc2
způsobené	způsobený	k2eAgFnSc2d1
absencí	absence	k1gFnSc7
sociálního	sociální	k2eAgInSc2d1
kontaktu	kontakt	k1gInSc2
<g/>
,	,	kIx,
vysokou	vysoký	k2eAgFnSc4d1
zátěž	zátěž	k1gFnSc4
<g/>
,	,	kIx,
nevyhovující	vyhovující	k2eNgFnPc4d1
online	onlinout	k5eAaPmIp3nS
hodiny	hodina	k1gFnPc4
a	a	k8xC
vysoké	vysoký	k2eAgInPc4d1
nároky	nárok	k1gInPc4
ze	z	k7c2
strany	strana	k1gFnSc2
školy	škola	k1gFnSc2
a	a	k8xC
učitelů	učitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevyhovující	vyhovující	k2eNgInSc1d1
je	být	k5eAaImIp3nS
pro	pro	k7c4
mnohé	mnohý	k2eAgMnPc4d1
studenty	student	k1gMnPc4
také	také	k6eAd1
způsob	způsob	k1gInSc1
testování	testování	k1gNnSc2
a	a	k8xC
známkování	známkování	k1gNnSc2
<g/>
,	,	kIx,
domácí	domácí	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
a	a	k8xC
nedostatek	nedostatek	k1gInSc4
motivace	motivace	k1gFnSc2
ke	k	k7c3
studiu	studio	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenti	student	k1gMnPc1
si	se	k3xPyFc3
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
pochvalují	pochvalovat	k5eAaImIp3nP
například	například	k6eAd1
větší	veliký	k2eAgFnSc4d2
možnost	možnost	k1gFnSc4
vlastní	vlastní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
času	čas	k1gInSc2
a	a	k8xC
větší	veliký	k2eAgFnSc4d2
volnost	volnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
ČSU	ČSU	kA
na	na	k7c6
svých	svůj	k3xOyFgFnPc6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
a	a	k8xC
webu	web	k1gInSc2
sdílela	sdílet	k5eAaImAgFnS
tipy	tip	k1gInPc4
a	a	k8xC
doporučení	doporučení	k1gNnPc4
pro	pro	k7c4
své	svůj	k3xOyFgMnPc4
sledující	sledující	k2eAgNnSc1d1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
tuto	tento	k3xDgFnSc4
těžkou	těžký	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
psychicky	psychicky	k6eAd1
zvládat	zvládat	k5eAaImF
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
dělat	dělat	k5eAaImF
pro	pro	k7c4
lepší	dobrý	k2eAgFnSc4d2
organizaci	organizace	k1gFnSc4
vlastního	vlastní	k2eAgInSc2d1
času	čas	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c4
koho	kdo	k3yInSc4,k3yQnSc4,k3yRnSc4
se	se	k3xPyFc4
případně	případně	k6eAd1
obrátit	obrátit	k5eAaPmF
a	a	k8xC
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Českou	český	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
sokolskou	sokolský	k2eAgFnSc7d1
sdílela	sdílet	k5eAaImAgFnS
i	i	k8xC
videa	video	k1gNnSc2
například	například	k6eAd1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
správně	správně	k6eAd1
cvičit	cvičit	k5eAaImF
vzhledem	vzhled	k1gInSc7
k	k	k7c3
nedostatku	nedostatek	k1gInSc3
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Vypadá	vypadat	k5eAaPmIp3nS,k5eAaImIp3nS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
distanční	distanční	k2eAgFnSc6d1
výuce	výuka	k1gFnSc6
ještě	ještě	k9
nějakou	nějaký	k3yIgFnSc4
chvíli	chvíle	k1gFnSc4
zůstaneme	zůstat	k5eAaPmIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
si	se	k3xPyFc3
myslíme	myslet	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ministerstvo	ministerstvo	k1gNnSc1
zajímalo	zajímat	k5eAaImAgNnS
o	o	k7c4
názory	názor	k1gInPc4
studentů	student	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
potřeby	potřeba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metodická	metodický	k2eAgFnSc1d1
doporučení	doporučení	k1gNnSc4
nesou	nést	k5eAaImIp3nP
velmi	velmi	k6eAd1
dobré	dobrý	k2eAgFnPc1d1
myšlenky	myšlenka	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
nedodržují	dodržovat	k5eNaImIp3nP
a	a	k8xC
studenti	student	k1gMnPc1
i	i	k8xC
pedagogové	pedagog	k1gMnPc1
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
mnohdy	mnohdy	k6eAd1
vystaveni	vystavit	k5eAaPmNgMnP
zbytečnému	zbytečný	k2eAgNnSc3d1
stresu	stres	k1gInSc2
a	a	k8xC
vzdělávání	vzdělávání	k1gNnSc2
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
není	být	k5eNaImIp3nS
komfortní	komfortní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSU	ČSU	kA
již	již	k9
kroky	krok	k1gInPc4
k	k	k7c3
lepší	dobrý	k2eAgFnSc3d2
distanční	distanční	k2eAgFnSc3d1
výuce	výuka	k1gFnSc3
podniká	podnikat	k5eAaImIp3nS
<g/>
,	,	kIx,
vše	všechen	k3xTgNnSc1
ale	ale	k9
sami	sám	k3xTgMnPc1
změnit	změnit	k5eAaPmF
nezvládneme	zvládnout	k5eNaPmIp1nP
<g/>
;	;	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
obracíme	obracet	k5eAaImIp1nP
na	na	k7c6
MŠMT	MŠMT	kA
pro	pro	k7c4
pomoc	pomoc	k1gFnSc4
<g/>
,	,	kIx,
<g/>
”	”	k?
shrnul	shrnout	k5eAaPmAgMnS
celou	celý	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
Ondřej	Ondřej	k1gMnSc1
Nováček	Nováček	k1gMnSc1
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnSc1d1
první	první	k4xOgMnSc1
místopředseda	místopředseda	k1gMnSc1
ČSU	ČSU	kA
<g/>
.	.	kIx.
</s>
<s>
Kampaň	kampaň	k1gFnSc4
Hlásíme	hlásit	k5eAaImIp1nP
se	se	k3xPyFc4
o	o	k7c4
slovo	slovo	k1gNnSc4
<g/>
!	!	kIx.
</s>
<s>
Studenti	student	k1gMnPc1
před	před	k7c7
budovou	budova	k1gFnSc7
ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
</s>
<s>
Středoškoláci	středoškolák	k1gMnPc1
od	od	k7c2
začátku	začátek	k1gInSc2
uveřejnění	uveřejnění	k1gNnSc2
plánu	plán	k1gInSc2
na	na	k7c4
zavedení	zavedení	k1gNnSc4
matematiky	matematika	k1gFnSc2
jako	jako	k8xS,k8xC
povinného	povinný	k2eAgInSc2d1
předmětu	předmět	k1gInSc2
společné	společný	k2eAgFnSc2d1
části	část	k1gFnSc2
maturitní	maturitní	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
toto	tento	k3xDgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
kritizují	kritizovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poukazují	poukazovat	k5eAaImIp3nP
přitom	přitom	k6eAd1
na	na	k7c4
nepříznivý	příznivý	k2eNgInSc4d1
vývoj	vývoj	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
propadlíků	propadlík	k1gMnPc2
v	v	k7c6
současné	současný	k2eAgFnSc6d1
volitelné	volitelný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
a	a	k8xC
předpovídají	předpovídat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
při	při	k7c6
zavedení	zavedení	k1gNnSc6
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
k	k	k7c3
více	hodně	k6eAd2
případům	případ	k1gInPc3
selhání	selhání	k1gNnSc2
zkoušky	zkouška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unie	unie	k1gFnSc1
kritizovala	kritizovat	k5eAaImAgFnS
rovněž	rovněž	k9
vliv	vliv	k1gInSc4
Svazu	svaz	k1gInSc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
dopravy	doprava	k1gFnSc2
na	na	k7c4
rozhodování	rozhodování	k1gNnSc4
ministerstva	ministerstvo	k1gNnSc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
věci	věc	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jako	jako	k9
součást	součást	k1gFnSc1
jejich	jejich	k3xOp3gFnSc2
kampaně	kampaň	k1gFnSc2
za	za	k7c4
získání	získání	k1gNnSc4
více	hodně	k6eAd2
kvalifikovaných	kvalifikovaný	k2eAgMnPc2d1
zájemců	zájemce	k1gMnPc2
o	o	k7c4
technické	technický	k2eAgInPc4d1
obory	obor	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Zavedení	zavedení	k1gNnSc1
zkoušky	zkouška	k1gFnSc2
jako	jako	k8xS,k8xC
povinné	povinný	k2eAgFnPc1d1
podmiňují	podmiňovat	k5eAaImIp3nP
zástupci	zástupce	k1gMnPc7
spolku	spolek	k1gInSc2
změnou	změna	k1gFnSc7
výuky	výuka	k1gFnSc2
tohoto	tento	k3xDgInSc2
předmětu	předmět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
Unie	unie	k1gFnSc1
opětovně	opětovně	k6eAd1
ohradila	ohradit	k5eAaPmAgFnS
vůči	vůči	k7c3
tvrzením	tvrzení	k1gNnSc7
tehdejší	tehdejší	k2eAgFnSc2d1
ministryně	ministryně	k1gFnSc2
školství	školství	k1gNnSc2
Valachové	Valachová	k1gFnSc2
a	a	k8xC
ředitele	ředitel	k1gMnSc2
Cermatu	Cerma	k1gNnSc2
Jiřího	Jiří	k1gMnSc2
Zíky	Zíka	k1gMnSc2
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
výsledcích	výsledek	k1gInPc6
maturitní	maturitní	k2eAgFnSc2d1
zkoušky	zkouška	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
pro	pro	k7c4
ČT	ČT	kA
zástupci	zástupce	k1gMnPc1
Unie	unie	k1gFnSc2
uvedli	uvést	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
neúspěšnost	neúspěšnost	k1gFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
velmi	velmi	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
počet	počet	k1gInSc1
studentů	student	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
si	se	k3xPyFc3
dobrovolně	dobrovolně	k6eAd1
vybírají	vybírat	k5eAaImIp3nP
matematiku	matematika	k1gFnSc4
jako	jako	k8xC,k8xS
maturitní	maturitní	k2eAgInSc4d1
předmět	předmět	k1gInSc4
<g/>
,	,	kIx,
rok	rok	k1gInSc4
od	od	k7c2
roku	rok	k1gInSc2
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jak	jak	k8xS,k8xC
Marcel	Marcel	k1gMnSc1
Chládek	Chládek	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
Kateřina	Kateřina	k1gFnSc1
Valachová	Valachová	k1gFnSc1
<g/>
,	,	kIx,
ministři	ministr	k1gMnPc1
školství	školství	k1gNnPc2
během	během	k7c2
iniciativy	iniciativa	k1gFnSc2
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
odmítli	odmítnout	k5eAaPmAgMnP
povinnou	povinný	k2eAgFnSc4d1
maturitu	maturita	k1gFnSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
zruši	zrusat	k5eAaPmIp1nSwK
</s>
<s>
S	s	k7c7
prvním	první	k4xOgInSc7
školním	školní	k2eAgInSc7d1
dnem	den	k1gInSc7
v	v	k7c6
září	září	k1gNnSc6
2019	#num#	k4
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
odstartovala	odstartovat	k5eAaPmAgFnS
kampaň	kampaň	k1gFnSc4
Hlásíme	hlásit	k5eAaImIp1nP
se	se	k3xPyFc4
o	o	k7c4
slovo	slovo	k1gNnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vyjadřovala	vyjadřovat	k5eAaImAgFnS
nesouhlas	nesouhlas	k1gInSc4
Unie	unie	k1gFnSc2
se	s	k7c7
zavedením	zavedení	k1gNnSc7
povinné	povinný	k2eAgFnSc2d1
maturity	maturita	k1gFnSc2
z	z	k7c2
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvního	první	k4xOgInSc2
října	říjen	k1gInSc2
byla	být	k5eAaImAgFnS
uspořádána	uspořádán	k2eAgFnSc1d1
tisková	tiskový	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
kampaň	kampaň	k1gFnSc1
představena	představit	k5eAaPmNgFnS
a	a	k8xC
zároveň	zároveň	k6eAd1
podpořena	podpořit	k5eAaPmNgFnS
odborníky	odborník	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jmenovitě	jmenovitě	k6eAd1
Tomášem	Tomáš	k1gMnSc7
Hazlbauerem	Hazlbauer	k1gMnSc7
<g/>
,	,	kIx,
Oldřichem	Oldřich	k1gMnSc7
Botlíkem	Botlík	k1gMnSc7
a	a	k8xC
Pavlem	Pavel	k1gMnSc7
Beránkem	Beránek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tiskovou	tiskový	k2eAgFnSc4d1
konferenci	konference	k1gFnSc4
pak	pak	k6eAd1
členové	člen	k1gMnPc1
spolku	spolek	k1gInSc2
ukončili	ukončit	k5eAaPmAgMnP
před	před	k7c7
budovou	budova	k1gFnSc7
Ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
zapálili	zapálit	k5eAaPmAgMnP
svíčky	svíčka	k1gFnPc4
za	za	k7c4
studenty	student	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
u	u	k7c2
maturity	maturita	k1gFnSc2
z	z	k7c2
matematiky	matematika	k1gFnSc2
neuspěli	uspět	k5eNaPmAgMnP
a	a	k8xC
za	za	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
by	by	kYmCp3nP
vinou	vinou	k7c2
zavedení	zavedení	k1gNnSc2
povinné	povinný	k2eAgFnSc2d1
maturity	maturita	k1gFnSc2
z	z	k7c2
matematiky	matematika	k1gFnSc2
u	u	k7c2
maturit	maturita	k1gFnPc2
neobstáli	obstát	k5eNaPmAgMnP
<g/>
.	.	kIx.
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celá	celý	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
byla	být	k5eAaImAgFnS
zakončena	zakončit	k5eAaPmNgFnS
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
hromadným	hromadný	k2eAgInSc7d1
protestem	protest	k1gInSc7
na	na	k7c6
Malostranském	malostranský	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástup	zástup	k1gInSc4
studentů	student	k1gMnPc2
se	se	k3xPyFc4
během	během	k7c2
něj	on	k3xPp3gMnSc2
vydal	vydat	k5eAaPmAgInS
k	k	k7c3
ministerstvu	ministerstvo	k1gNnSc3
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Viktor	Viktor	k1gMnSc1
René	René	k1gMnSc1
Schilke	Schilke	k1gFnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
ČSU	ČSU	kA
<g/>
,	,	kIx,
předal	předat	k5eAaPmAgMnS
petici	petice	k1gFnSc4
proti	proti	k7c3
zavedení	zavedení	k1gNnSc3
povinné	povinný	k2eAgFnSc2d1
maturity	maturita	k1gFnSc2
z	z	k7c2
matematiky	matematika	k1gFnSc2
státnímu	státní	k2eAgMnSc3d1
tajemníkovi	tajemník	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dav	Dav	k1gInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
odebral	odebrat	k5eAaPmAgInS
zpátky	zpátky	k6eAd1
na	na	k7c6
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
vyslechli	vyslechnout	k5eAaPmAgMnP
projevy	projev	k1gInPc4
hostů	host	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patřila	patřit	k5eAaImAgFnS
například	například	k6eAd1
i	i	k9
Taťána	Taťána	k1gFnSc1
Fisherová	Fisherová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
ministr	ministr	k1gMnSc1
školství	školství	k1gNnSc2
Robert	Robert	k1gMnSc1
Plaga	Plag	k1gMnSc2
akce	akce	k1gFnSc2
zúčastnit	zúčastnit	k5eAaPmF
nemohl	moct	k5eNaImAgMnS
<g/>
,	,	kIx,
protest	protest	k1gInSc4
podpořil	podpořit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
listopadu	listopad	k1gInSc2
doputoval	doputovat	k5eAaPmAgInS
návrh	návrh	k1gInSc1
do	do	k7c2
garančního	garanční	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
poslancům	poslanec	k1gMnPc3
doporučoval	doporučovat	k5eAaImAgMnS
návrh	návrh	k1gInSc4
schválit	schválit	k5eAaPmF
nebo	nebo	k8xC
schválit	schválit	k5eAaPmF
pozměněný	pozměněný	k2eAgInSc4d1
návrh	návrh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pouze	pouze	k6eAd1
maturitu	maturita	k1gFnSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
posouval	posouvat	k5eAaImAgMnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
před	před	k7c7
třetím	třetí	k4xOgNnSc7
čtením	čtení	k1gNnSc7
byly	být	k5eAaImAgFnP
tři	tři	k4xCgFnPc4
různé	různý	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
(	(	kIx(
<g/>
schválení	schválení	k1gNnSc1
<g/>
;	;	kIx,
neschválení	neschválení	k1gNnSc1
<g/>
;	;	kIx,
posunutí	posunutí	k1gNnSc1
o	o	k7c4
2	#num#	k4
roky	rok	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ČSU	ČSU	kA
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
uspořádat	uspořádat	k5eAaPmF
dopisovou	dopisový	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učitelé	učitel	k1gMnPc1
<g/>
,	,	kIx,
studenti	student	k1gMnPc1
i	i	k8xC
jejich	jejich	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
posílali	posílat	k5eAaImAgMnP
zákonodárcům	zákonodárce	k1gMnPc3
dopisy	dopis	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yRgInPc6,k3yQgInPc6
vysvětlovali	vysvětlovat	k5eAaImAgMnP
svůj	svůj	k3xOyFgInSc4
názor	názor	k1gInSc4
na	na	k7c4
povinnou	povinný	k2eAgFnSc4d1
maturitu	maturita	k1gFnSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
či	či	k8xC
argumenty	argument	k1gInPc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
by	by	kYmCp3nS
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
zavedení	zavedení	k1gNnSc3
nemělo	mít	k5eNaImAgNnS
dojít	dojít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
odesláno	odeslat	k5eAaPmNgNnS
kolem	kolem	k7c2
300	#num#	k4
dopisů	dopis	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
důvodem	důvod	k1gInSc7
změny	změna	k1gFnSc2
názoru	názor	k1gInSc2
některých	některý	k3yIgMnPc2
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
45	#num#	k4
<g/>
.	.	kIx.
schůzi	schůze	k1gFnSc6
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
schválen	schválit	k5eAaPmNgInS
a	a	k8xC
k	k	k7c3
zavedení	zavedení	k1gNnSc3
povinné	povinný	k2eAgFnSc2d1
maturity	maturita	k1gFnSc2
tak	tak	k6eAd1
nedošlo	dojít	k5eNaPmAgNnS
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
se	se	k3xPyFc4
aktivně	aktivně	k6eAd1
zapojuje	zapojovat	k5eAaImIp3nS
do	do	k7c2
ovlivňování	ovlivňování	k1gNnSc2
vzdělávací	vzdělávací	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
a	a	k8xC
politiky	politika	k1gFnSc2
mládeže	mládež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkává	setkávat	k5eAaImIp3nS
se	se	k3xPyFc4
se	s	k7c7
zástupci	zástupce	k1gMnPc7
Ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
komunikuje	komunikovat	k5eAaImIp3nS
s	s	k7c7
poslanci	poslanec	k1gMnPc7
a	a	k8xC
senátory	senátor	k1gMnPc7
zaměřenými	zaměřený	k2eAgNnPc7d1
na	na	k7c4
vzdělávací	vzdělávací	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
rovněž	rovněž	k9
s	s	k7c7
politiky	politik	k1gMnPc7
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
krajích	kraj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapojuje	zapojovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
do	do	k7c2
veřejné	veřejný	k2eAgFnSc2d1
debaty	debata	k1gFnSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
diskuzí	diskuze	k1gFnPc2
s	s	k7c7
neziskovými	ziskový	k2eNgFnPc7d1
organizacemi	organizace	k1gFnPc7
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svá	svůj	k3xOyFgNnPc4
stanoviska	stanovisko	k1gNnPc4
pravidelně	pravidelně	k6eAd1
prezentuje	prezentovat	k5eAaBmIp3nS
v	v	k7c6
médiích	médium	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
své	svůj	k3xOyFgFnSc2
politické	politický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
spolek	spolek	k1gInSc1
vyvíjí	vyvíjet	k5eAaImIp3nS
i	i	k9
řadu	řada	k1gFnSc4
jiných	jiný	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Republikové	republikový	k2eAgInPc1d1
sněmy	sněm	k1gInPc1
</s>
<s>
Dvakrát	dvakrát	k6eAd1
ročně	ročně	k6eAd1
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
pořádá	pořádat	k5eAaImIp3nS
republikový	republikový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
se	se	k3xPyFc4
kromě	kromě	k7c2
delegátů	delegát	k1gMnPc2
žákovských	žákovský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
účastní	účastnit	k5eAaImIp3nP
další	další	k2eAgMnPc1d1
členové	člen	k1gMnPc1
i	i	k8xC
aktivní	aktivní	k2eAgMnPc1d1
středoškoláci	středoškolák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastníci	účastník	k1gMnPc1
zde	zde	k6eAd1
mohou	moct	k5eAaImIp3nP
debatovat	debatovat	k5eAaImF
o	o	k7c6
problémech	problém	k1gInPc6
středního	střední	k2eAgNnSc2d1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
konfrontovat	konfrontovat	k5eAaBmF
své	svůj	k3xOyFgInPc4
názory	názor	k1gInPc4
se	s	k7c7
zástupci	zástupce	k1gMnPc7
státu	stát	k1gInSc2
i	i	k8xC
neziskového	ziskový	k2eNgInSc2d1
sektoru	sektor	k1gInSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
sdílet	sdílet	k5eAaImF
své	svůj	k3xOyFgFnPc4
pozitivní	pozitivní	k2eAgFnPc4d1
zkušenosti	zkušenost	k1gFnPc4
s	s	k7c7
vlastními	vlastní	k2eAgInPc7d1
středoškolskými	středoškolský	k2eAgInPc7d1
projekty	projekt	k1gInPc7
<g/>
,	,	kIx,
fungováním	fungování	k1gNnSc7
žákovských	žákovský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
inspirovat	inspirovat	k5eAaBmF
tak	tak	k6eAd1
ostatní	ostatní	k2eAgMnSc1d1
k	k	k7c3
vlastní	vlastní	k2eAgFnSc3d1
aktivní	aktivní	k2eAgFnSc3d1
činnosti	činnost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znovuzavedení	znovuzavedení	k1gNnSc1
dílen	dílna	k1gFnPc2
a	a	k8xC
pozemků	pozemek	k1gInPc2
na	na	k7c6
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
</s>
<s>
Na	na	k7c4
plán	plán	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
představen	představit	k5eAaPmNgInS
českým	český	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
vlády	vláda	k1gFnSc2
v	v	k7c4
demisi	demise	k1gFnSc4
<g/>
,	,	kIx,
Andrejem	Andrej	k1gMnSc7
Babišem	Babiš	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
v	v	k7c6
úmyslu	úmysl	k1gInSc6
opětovné	opětovný	k2eAgNnSc1d1
zavedení	zavedení	k1gNnSc1
pozemků	pozemek	k1gInPc2
a	a	k8xC
dílen	dílna	k1gFnPc2
(	(	kIx(
<g/>
tj.	tj.	kA
vyučovací	vyučovací	k2eAgFnPc4d1
hodiny	hodina	k1gFnPc4
věnované	věnovaný	k2eAgFnSc3d1
výuce	výuka	k1gFnSc3
manuálních	manuální	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
)	)	kIx)
do	do	k7c2
osnov	osnova	k1gFnPc2
výuky	výuka	k1gFnSc2
na	na	k7c6
základních	základní	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
reagovala	reagovat	k5eAaBmAgFnS
Unie	unie	k1gFnSc1
kritikou	kritika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argumenty	argument	k1gInPc4
středoškoláků	středoškolák	k1gMnPc2
proti	proti	k7c3
plánu	plán	k1gInSc3
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
uvedla	uvést	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnSc1
tehdejší	tehdejší	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
Lenka	Lenka	k1gFnSc1
Štěpánová	Štěpánová	k1gFnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
doložené	doložený	k2eAgNnSc4d1
ohrožení	ohrožení	k1gNnSc4
profesí	profes	k1gFnPc2
založených	založený	k2eAgFnPc2d1
na	na	k7c6
manuální	manuální	k2eAgFnSc6d1
práci	práce	k1gFnSc6
z	z	k7c2
důvodu	důvod	k1gInSc2
automatizace	automatizace	k1gFnSc2
<g/>
,	,	kIx,
problém	problém	k1gInSc4
špatné	špatný	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
středních	střední	k2eAgFnPc2d1
odborných	odborný	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
učilišť	učiliště	k1gNnPc2
se	se	k3xPyFc4
zaměstnavateli	zaměstnavatel	k1gMnSc3
a	a	k8xC
z	z	k7c2
toho	ten	k3xDgNnSc2
plynoucí	plynoucí	k2eAgFnSc1d1
špatná	špatný	k2eAgFnSc1d1
kvalita	kvalita	k1gFnSc1
středoškolských	středoškolský	k2eAgFnPc2d1
praxí	praxe	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
absence	absence	k1gFnSc1
studií	studio	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
přínosnost	přínosnost	k1gFnSc4
pozemků	pozemek	k1gInPc2
a	a	k8xC
dílen	dílna	k1gFnPc2
podporovaly	podporovat	k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Středoškolský	středoškolský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
</s>
<s>
Středoškolský	středoškolský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
založila	založit	k5eAaPmAgFnS
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
pražským	pražský	k2eAgInSc7d1
Magistrátem	magistrát	k1gInSc7
a	a	k8xC
radní	radní	k1gFnSc7
pro	pro	k7c4
školství	školství	k1gNnSc4
Irenou	Irena	k1gFnSc7
Ropkovou	Ropkův	k2eAgFnSc7d1
(	(	kIx(
<g/>
ČSSD	ČSSD	kA
<g/>
)	)	kIx)
jakožto	jakožto	k8xS
poradní	poradní	k2eAgInSc1d1
orgán	orgán	k1gInSc1
města	město	k1gNnSc2
tvořený	tvořený	k2eAgInSc1d1
zástupci	zástupce	k1gMnPc7
studentských	studentský	k2eAgFnPc2d1
rad	rada	k1gFnPc2
z	z	k7c2
pražských	pražský	k2eAgFnPc2d1
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnPc1
zasedání	zasedání	k1gNnSc1
proběhlo	proběhnout	k5eAaPmAgNnS
16	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
v	v	k7c6
Brožíkově	Brožíkův	k2eAgFnSc6d1
síni	síň	k1gFnSc6
Staroměstské	staroměstský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
ku	k	k7c3
příležitosti	příležitost	k1gFnSc3
svátku	svátek	k1gInSc2
Dne	den	k1gInSc2
boje	boj	k1gInSc2
za	za	k7c4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
demokracii	demokracie	k1gFnSc4
a	a	k8xC
Mezinárodního	mezinárodní	k2eAgInSc2d1
dne	den	k1gInSc2
studentstva	studentstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tehdejší	tehdejší	k2eAgFnSc2d1
předsedkyně	předsedkyně	k1gFnSc2
Unie	unie	k1gFnSc2
Lenky	Lenka	k1gFnSc2
Štěpánové	Štěpánová	k1gFnSc2
je	být	k5eAaImIp3nS
cílem	cíl	k1gInSc7
projektu	projekt	k1gInSc2
"	"	kIx"
<g/>
aktivizovat	aktivizovat	k5eAaImF
studentské	studentský	k2eAgFnPc4d1
rady	rada	k1gFnPc4
<g/>
,	,	kIx,
podporovat	podporovat	k5eAaImF
je	být	k5eAaImIp3nS
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
činnosti	činnost	k1gFnSc6
a	a	k8xC
rovněž	rovněž	k9
přesvědčit	přesvědčit	k5eAaPmF
širokou	široký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
i	i	k8xC
ředitele	ředitel	k1gMnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
participace	participace	k1gFnSc1
studentů	student	k1gMnPc2
má	mít	k5eAaImIp3nS
hlubší	hluboký	k2eAgInSc4d2
smysl	smysl	k1gInSc4
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
role	role	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
demokracii	demokracie	k1gFnSc4
naprosto	naprosto	k6eAd1
zásadní	zásadní	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Jedním	jeden	k4xCgInSc7
z	z	k7c2
největších	veliký	k2eAgInPc2d3
úspěchů	úspěch	k1gInPc2
bylo	být	k5eAaImAgNnS
přidání	přidání	k1gNnSc1
kuchařkám	kuchařka	k1gFnPc3
ze	z	k7c2
školních	školní	k2eAgFnPc2d1
jídelen	jídelna	k1gFnPc2
100	#num#	k4
000	#num#	k4
000	#num#	k4
z	z	k7c2
rozpočtu	rozpočet	k1gInSc2
na	na	k7c4
výzvu	výzva	k1gFnSc4
z	z	k7c2
usnesení	usnesení	k1gNnSc2
sněmu	sněm	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Po	po	k7c6
dvou	dva	k4xCgNnPc6
letech	léto	k1gNnPc6
fungovaní	fungovaný	k2eAgMnPc1d1
se	se	k3xPyFc4
sněm	sněm	k1gInSc1
od	od	k7c2
ČSU	ČSU	kA
odpojil	odpojit	k5eAaPmAgInS
a	a	k8xC
nyní	nyní	k6eAd1
již	již	k6eAd1
funguje	fungovat	k5eAaImIp3nS
autonomně	autonomně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fond	fond	k1gInSc1
pro	pro	k7c4
samosprávy	samospráva	k1gFnPc4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
spustila	spustit	k5eAaPmAgFnS
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Magistrátem	magistrát	k1gInSc7
hl.	hl.	k?
m.	m.	k?
Prahy	Praha	k1gFnSc2
projekt	projekt	k1gInSc4
s	s	k7c7
názvem	název	k1gInSc7
Fond	fond	k1gInSc1
pro	pro	k7c4
samosprávy	samospráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
něj	on	k3xPp3gMnSc4
rozdělovala	rozdělovat	k5eAaImAgFnS
ČSU	ČSU	kA
grant	grant	k1gInSc4
svěřený	svěřený	k2eAgInSc4d1
pražským	pražský	k2eAgInSc7d1
Magistrátem	magistrát	k1gInSc7
mezi	mezi	k7c4
studentské	studentský	k2eAgFnPc4d1
samosprávy	samospráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
mohly	moct	k5eAaImAgInP
dostat	dostat	k5eAaPmF
příspěvek	příspěvek	k1gInSc4
až	až	k9
3000	#num#	k4
Kč	Kč	kA
na	na	k7c4
své	svůj	k3xOyFgFnPc4
akce	akce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
podporoval	podporovat	k5eAaImAgInS
pouze	pouze	k6eAd1
studentské	studentský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ho	on	k3xPp3gMnSc4
nicméně	nicméně	k8xC
dle	dle	k7c2
svých	svůj	k3xOyFgNnPc2
slov	slovo	k1gNnPc2
plánuje	plánovat	k5eAaImIp3nS
rozšířit	rozšířit	k5eAaPmF
i	i	k9
do	do	k7c2
dalších	další	k2eAgNnPc2d1
měst	město	k1gNnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
je	být	k5eAaImIp3nS
projekt	projekt	k1gInSc1
pozastaven	pozastavit	k5eAaPmNgInS
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Školský	školský	k2eAgMnSc1d1
ombudsman	ombudsman	k1gMnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
od	od	k7c2
vzniku	vznik	k1gInSc2
funkce	funkce	k1gFnSc2
školského	školský	k2eAgMnSc2d1
ombudsmana	ombudsman	k1gMnSc2
kritizovala	kritizovat	k5eAaImAgFnS
výběr	výběr	k1gInSc4
prvního	první	k4xOgMnSc2
ombudsmana	ombudsman	k1gMnSc2
Eduarda	Eduard	k1gMnSc2
Zemana	Zeman	k1gMnSc2
dosazeného	dosazený	k2eAgMnSc2d1
do	do	k7c2
funkce	funkce	k1gFnSc2
Marcelem	Marcel	k1gMnSc7
Chládkem	Chládek	k1gMnSc7
<g/>
,	,	kIx,
tehdejším	tehdejší	k2eAgMnSc7d1
ministrem	ministr	k1gMnSc7
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
dosazení	dosazení	k1gNnSc3
hodnotila	hodnotit	k5eAaImAgFnS
jako	jako	k9
netransparentní	transparentní	k2eNgFnSc1d1
a	a	k8xC
politicky	politicky	k6eAd1
motivovaný	motivovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkci	funkce	k1gFnSc4
jako	jako	k8xS,k8xC
takovou	takový	k3xDgFnSc4
ovšem	ovšem	k9
podporuje	podporovat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
vyvrcholení	vyvrcholení	k1gNnSc3
kritiky	kritika	k1gFnSc2
Eduarda	Eduard	k1gMnSc2
Zemana	Zeman	k1gMnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
unie	unie	k1gFnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
případem	případ	k1gInSc7
šikany	šikana	k1gFnSc2
učitele	učitel	k1gMnSc2
kritizovala	kritizovat	k5eAaImAgFnS
Zemana	Zeman	k1gMnSc4
za	za	k7c4
nečinnost	nečinnost	k1gFnSc4
a	a	k8xC
"	"	kIx"
<g/>
neviditelnost	neviditelnost	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Tématu	téma	k1gNnSc6
se	se	k3xPyFc4
chopila	chopit	k5eAaPmAgFnS
i	i	k9
bulvární	bulvární	k2eAgNnPc4d1
média	médium	k1gNnPc4
a	a	k8xC
kritiku	kritika	k1gFnSc4
školského	školský	k2eAgMnSc2d1
ombudsmana	ombudsman	k1gMnSc2
rozšířila	rozšířit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Ministryně	ministryně	k1gFnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
Kateřina	Kateřina	k1gFnSc1
Valachová	Valachová	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
ohlasy	ohlas	k1gInPc4
reagovala	reagovat	k5eAaBmAgFnS
odvoláním	odvolání	k1gNnSc7
Zemana	Zeman	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnPc3
nahrazením	nahrazení	k1gNnPc3
organizátorem	organizátor	k1gMnSc7
soutěže	soutěž	k1gFnSc2
Zlatý	zlatý	k2eAgMnSc1d1
Ámos	Ámos	k1gMnSc1
Ladislavem	Ladislav	k1gMnSc7
Hrzalem	Hrzal	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dělení	dělení	k1gNnSc1
výuky	výuka	k1gFnSc2
matematiky	matematika	k1gFnSc2
do	do	k7c2
skupin	skupina	k1gFnPc2
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
protestu	protest	k1gInSc2
proti	proti	k7c3
povinné	povinný	k2eAgFnSc3d1
maturitě	maturita	k1gFnSc3
z	z	k7c2
matematiky	matematika	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgInS
spolek	spolek	k1gInSc1
dílčího	dílčí	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
větší	veliký	k2eAgFnSc1d2
individualizace	individualizace	k1gFnSc1
výuky	výuka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
novely	novela	k1gFnSc2
financování	financování	k1gNnSc2
regionálního	regionální	k2eAgNnSc2d1
školství	školství	k1gNnSc2
jsou	být	k5eAaImIp3nP
totiž	totiž	k9
prostředky	prostředek	k1gInPc1
pro	pro	k7c4
dělení	dělení	k1gNnSc4
tříd	třída	k1gFnPc2
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
běžné	běžný	k2eAgNnSc1d1
například	například	k6eAd1
u	u	k7c2
cizích	cizí	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgMnPc1d1
předseda	předseda	k1gMnSc1
Štěpán	Štěpán	k1gMnSc1
Kment	Kment	k1gMnSc1
tento	tento	k3xDgInSc4
krok	krok	k1gInSc4
označil	označit	k5eAaPmAgMnS
jako	jako	k8xC,k8xS
cestu	cesta	k1gFnSc4
k	k	k7c3
přizpůsobení	přizpůsobení	k1gNnSc3
tempa	tempo	k1gNnSc2
výuky	výuka	k1gFnSc2
žákům	žák	k1gMnPc3
a	a	k8xC
většímu	veliký	k2eAgInSc3d2
prostoru	prostor	k1gInSc3
na	na	k7c4
vysvětlování	vysvětlování	k1gNnSc4
žákovských	žákovský	k2eAgInPc2d1
dotazů	dotaz	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kampaň	kampaň	k1gFnSc1
Odtajni	odtajnit	k5eAaPmRp2nS
učebnu	učebna	k1gFnSc4
</s>
<s>
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
novelu	novela	k1gFnSc4
zákona	zákon	k1gInSc2
o	o	k7c6
pedagogických	pedagogický	k2eAgMnPc6d1
pracovnících	pracovník	k1gMnPc6
Unie	unie	k1gFnSc2
v	v	k7c4
září	září	k1gNnSc4
roku	rok	k1gInSc2
2016	#num#	k4
iniciovala	iniciovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
součástí	součást	k1gFnPc2
portfolia	portfolio	k1gNnSc2
pro	pro	k7c4
hodnocení	hodnocení	k1gNnSc4
učitele	učitel	k1gMnSc2
při	při	k7c6
přechodu	přechod	k1gInSc6
do	do	k7c2
vyššího	vysoký	k2eAgInSc2d2
kariérního	kariérní	k2eAgInSc2d1
stupně	stupeň	k1gInSc2
bylo	být	k5eAaImAgNnS
i	i	k9
žákovské	žákovský	k2eAgNnSc1d1
hodnocení	hodnocení	k1gNnSc1
učitele	učitel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tehdejšího	tehdejší	k2eAgMnSc2d1
předsedy	předseda	k1gMnSc2
Štěpána	Štěpán	k1gMnSc2
Kmenta	Kment	k1gMnSc2
unie	unie	k1gFnSc2
poukazoval	poukazovat	k5eAaImAgInS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
názory	názor	k1gInPc1
žáků	žák	k1gMnPc2
na	na	k7c4
jejich	jejich	k3xOp3gNnSc4
vlastní	vlastní	k2eAgNnSc4d1
vzdělávání	vzdělávání	k1gNnSc4
většinou	většinou	k6eAd1
v	v	k7c6
diskuzi	diskuze	k1gFnSc6
o	o	k7c6
výuce	výuka	k1gFnSc6
nejsou	být	k5eNaImIp3nP
slyšet	slyšet	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
vzdělávací	vzdělávací	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
neexistují	existovat	k5eNaImIp3nP
nástroje	nástroj	k1gInPc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
měnící	měnící	k2eAgFnSc4d1
se	se	k3xPyFc4
kvalitu	kvalita	k1gFnSc4
výuky	výuka	k1gFnSc2
u	u	k7c2
pedagoga	pedagog	k1gMnSc2
za	za	k7c7
zavřenými	zavřený	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
učebny	učebna	k1gFnSc2
měřit	měřit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
kampaně	kampaň	k1gFnSc2
Unie	unie	k1gFnSc2
kladla	klást	k5eAaImAgFnS
důraz	důraz	k1gInSc4
na	na	k7c4
zjištění	zjištění	k1gNnSc4
zahraničního	zahraniční	k2eAgInSc2d1
pedagogického	pedagogický	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
návrhu	návrh	k1gInSc6
se	se	k3xPyFc4
inspirovala	inspirovat	k5eAaBmAgFnS
systémem	systém	k1gInSc7
žákovské	žákovský	k2eAgFnSc2d1
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
STeP	step	k1gFnSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
jehož	jehož	k3xOyRp3gInSc1
vývoj	vývoj	k1gInSc1
na	na	k7c4
americké	americký	k2eAgInPc4d1
Vanderbilt	Vanderbilt	k1gInSc4
University	universita	k1gFnSc2
financovala	financovat	k5eAaBmAgFnS
Nadace	nadace	k1gFnSc1
Billa	Bill	k1gMnSc2
Gatese	Gatese	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
své	svůj	k3xOyFgFnSc6
iniciativě	iniciativa	k1gFnSc6
za	za	k7c4
zavádění	zavádění	k1gNnPc4
zpětnovazebních	zpětnovazební	k2eAgInPc2d1
dotazníků	dotazník	k1gInPc2
do	do	k7c2
výuky	výuka	k1gFnSc2
Unie	unie	k1gFnSc2
akcentuje	akcentovat	k5eAaImIp3nS
především	především	k9
důležitost	důležitost	k1gFnSc4
správného	správný	k2eAgNnSc2d1
metodického	metodický	k2eAgNnSc2d1
sestavení	sestavení	k1gNnSc2
dotazníků	dotazník	k1gInPc2
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgFnSc4d1
inspiraci	inspirace	k1gFnSc4
hledala	hledat	k5eAaImAgFnS
i	i	k9
v	v	k7c6
Norsku	Norsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Návrh	návrh	k1gInSc1
narazil	narazit	k5eAaPmAgInS
na	na	k7c4
negativní	negativní	k2eAgFnPc4d1
reakce	reakce	k1gFnPc4
ze	z	k7c2
strany	strana	k1gFnSc2
ministerstva	ministerstvo	k1gNnSc2
i	i	k8xC
poslanců	poslanec	k1gMnPc2
ze	z	k7c2
školského	školský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdejší	tehdejší	k2eAgFnSc1d1
ministryně	ministryně	k1gFnSc1
Valachová	Valachová	k1gFnSc1
návrh	návrh	k1gInSc4
odmítla	odmítnout	k5eAaPmAgFnS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
"	"	kIx"
<g/>
kariérní	kariérní	k2eAgInSc1d1
řád	řád	k1gInSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
profesního	profesní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
učitelů	učitel	k1gMnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc2
platového	platový	k2eAgNnSc2d1
ohodnocení	ohodnocení	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc2
nároků	nárok	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
vyplývají	vyplývat	k5eAaImIp3nP
ze	z	k7c2
zákoníku	zákoník	k1gInSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
reagovali	reagovat	k5eAaBmAgMnP
i	i	k9
poslanci	poslanec	k1gMnPc1
školského	školský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
na	na	k7c6
uzavřené	uzavřený	k2eAgFnSc6d1
schůzce	schůzka	k1gFnSc6
kontaktní	kontaktní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
Unie	unie	k1gFnSc1
svolala	svolat	k5eAaPmAgFnS
na	na	k7c6
začátku	začátek	k1gInSc6
ledna	leden	k1gInSc2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kariérní	kariérní	k2eAgInSc1d1
řád	řád	k1gInSc1
navrhovaný	navrhovaný	k2eAgInSc1d1
Valachovou	Valachová	k1gFnSc7
nakonec	nakonec	k9
sněmovnou	sněmovna	k1gFnSc7
vůbec	vůbec	k9
neprošel	projít	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1
předsedkyně	předsedkyně	k1gFnSc1
Unie	unie	k1gFnSc1
Lenka	Lenka	k1gFnSc1
Štěpánová	Štěpánová	k1gFnSc1
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
o	o	k7c6
návrhu	návrh	k1gInSc6
pro	pro	k7c4
Rádio	rádio	k1gNnSc4
Junior	junior	k1gMnSc1
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
Unie	unie	k1gFnSc1
udělala	udělat	k5eAaPmAgFnS
chybu	chyba	k1gFnSc4
v	v	k7c6
komunikaci	komunikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Média	médium	k1gNnPc1
si	se	k3xPyFc3
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
převzala	převzít	k5eAaPmAgFnS
pouze	pouze	k6eAd1
zkratku	zkratka	k1gFnSc4
a	a	k8xC
spojení	spojení	k1gNnSc4
žákovského	žákovský	k2eAgNnSc2d1
hodnocení	hodnocení	k1gNnSc2
a	a	k8xC
platů	plat	k1gInPc2
učitelů	učitel	k1gMnPc2
a	a	k8xC
z	z	k7c2
debaty	debata	k1gFnSc2
vymizel	vymizet	k5eAaPmAgMnS
akcent	akcent	k1gInSc4
na	na	k7c4
zaměření	zaměření	k1gNnSc4
na	na	k7c4
kvalitu	kvalita	k1gFnSc4
vzdělávání	vzdělávání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
návrhem	návrh	k1gInSc7
na	na	k7c4
plošné	plošný	k2eAgNnSc4d1
zavedení	zavedení	k1gNnSc4
žákovské	žákovský	k2eAgFnSc2d1
zpětné	zpětný	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
si	se	k3xPyFc3
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
Unie	unie	k1gFnSc2
stále	stále	k6eAd1
stojí	stát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Víkend	víkend	k1gInSc1
studentských	studentský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
půdě	půda	k1gFnSc6
pražského	pražský	k2eAgNnSc2d1
gymnázia	gymnázium	k1gNnSc2
sešlo	sejít	k5eAaPmAgNnS
přes	přes	k7c4
40	#num#	k4
studentů	student	k1gMnPc2
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
středních	střední	k2eAgFnPc2d1
škol	škola	k1gFnPc2
z	z	k7c2
celé	celý	k2eAgFnSc2d1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pod	pod	k7c7
křídly	křídlo	k1gNnPc7
České	český	k2eAgFnSc2d1
středoškolské	středoškolský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
diskutovali	diskutovat	k5eAaImAgMnP
o	o	k7c4
fungování	fungování	k1gNnSc4
rad	rada	k1gFnPc2
<g/>
,	,	kIx,
parlamentů	parlament	k1gInPc2
a	a	k8xC
senátů	senát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Událost	událost	k1gFnSc1
spolek	spolek	k1gInSc1
zacílil	zacílit	k5eAaPmAgInS
na	na	k7c4
výměnu	výměna	k1gFnSc4
dobré	dobrý	k2eAgFnSc2d1
praxe	praxe	k1gFnSc2
účastníků	účastník	k1gMnPc2
a	a	k8xC
rozvoj	rozvoj	k1gInSc1
jejich	jejich	k3xOp3gFnPc2
schopností	schopnost	k1gFnPc2
a	a	k8xC
dovedností	dovednost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Unie	unie	k1gFnSc1
Víkend	víkend	k1gInSc1
studentských	studentský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
pořádá	pořádat	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Zažij	zažít	k5eAaPmRp2nS
střední	střední	k1gMnSc5
jinak	jinak	k6eAd1
</s>
<s>
Součástí	součást	k1gFnSc7
snahy	snaha	k1gFnSc2
rozpoutat	rozpoutat	k5eAaPmF
diskusi	diskuse	k1gFnSc3
o	o	k7c6
školství	školství	k1gNnSc6
mezi	mezi	k7c7
středoškoláky	středoškolák	k1gMnPc7
Unie	unie	k1gFnSc2
pořádala	pořádat	k5eAaImAgFnS
události	událost	k1gFnSc3
zvané	zvaný	k2eAgFnSc3d1
Zapoj	zapojit	k5eAaPmRp2nS
se	se	k3xPyFc4
na	na	k7c4
střední	střední	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
se	se	k3xPyFc4
zaměřovaly	zaměřovat	k5eAaImAgFnP
vždy	vždy	k6eAd1
na	na	k7c4
určité	určitý	k2eAgNnSc4d1
téma	téma	k1gNnSc4
týkající	týkající	k2eAgNnSc4d1
se	se	k3xPyFc4
středoškolského	středoškolský	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
(	(	kIx(
<g/>
např.	např.	kA
kvalita	kvalita	k1gFnSc1
výuky	výuka	k1gFnSc2
nebo	nebo	k8xC
praktické	praktický	k2eAgNnSc4d1
vyučování	vyučování	k1gNnSc4
na	na	k7c6
odborných	odborný	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
a	a	k8xC
kombinovaly	kombinovat	k5eAaImAgInP
panelovou	panelový	k2eAgFnSc4d1
debatu	debata	k1gFnSc4
odborníků	odborník	k1gMnPc2
s	s	k7c7
následnými	následný	k2eAgInPc7d1
workshopy	workshop	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Otevřený	otevřený	k2eAgInSc1d1
dopis	dopis	k1gInSc1
<g/>
:	:	kIx,
středoškoláci	středoškolák	k1gMnPc1
nechtějí	chtít	k5eNaImIp3nP
přijít	přijít	k5eAaPmF
o	o	k7c4
bufety	bufet	k1gInPc4
</s>
<s>
Začátkem	začátkem	k7c2
března	březen	k1gInSc2
2016	#num#	k4
publikovala	publikovat	k5eAaBmAgFnS
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
kampaň	kampaň	k1gFnSc1
proti	proti	k7c3
vyhlášce	vyhláška	k1gFnSc3
chystané	chystaný	k2eAgFnSc2d1
Ministerstvem	ministerstvo	k1gNnSc7
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
mělo	mít	k5eAaImAgNnS
regulovat	regulovat	k5eAaImF
prodej	prodej	k1gInSc4
až	až	k9
80	#num#	k4
%	%	kIx~
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
ministerstvo	ministerstvo	k1gNnSc1
označilo	označit	k5eAaPmAgNnS
za	za	k7c4
nezdravé	zdravý	k2eNgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc4
středoškoláků	středoškolák	k1gMnPc2
se	se	k3xPyFc4
vyhrazoval	vyhrazovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zákazem	zákaz	k1gInSc7
řeší	řešit	k5eAaImIp3nS
ministerstvo	ministerstvo	k1gNnSc1
problém	problém	k1gInSc1
nezdravého	zdravý	k2eNgInSc2d1
životního	životní	k2eAgInSc2d1
stylu	styl	k1gInSc2
u	u	k7c2
mladých	mladý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
nekoncepčně	koncepčně	k6eNd1
a	a	k8xC
navrhoval	navrhovat	k5eAaImAgMnS
omezení	omezení	k1gNnSc4
platnosti	platnost	k1gFnSc2
zákazu	zákaz	k1gInSc2
prodeje	prodej	k1gInSc2
určených	určený	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
jen	jen	k9
na	na	k7c4
základní	základní	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
finanční	finanční	k2eAgNnSc4d1
zvýhodnění	zvýhodnění	k1gNnSc4
zdravých	zdravý	k2eAgFnPc2d1
alternativ	alternativa	k1gFnPc2
oproti	oproti	k7c3
nezdravým	zdravý	k2eNgFnPc3d1
potravinám	potravina	k1gFnPc3
a	a	k8xC
revizi	revize	k1gFnSc3
vyhlášky	vyhláška	k1gFnSc2
o	o	k7c6
školním	školní	k2eAgNnSc6d1
stravování	stravování	k1gNnSc6
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
spotřební	spotřební	k2eAgInSc1d1
koš	koš	k1gInSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
požadavků	požadavek	k1gInPc2
na	na	k7c4
jídla	jídlo	k1gNnPc4
<g/>
)	)	kIx)
jídelen	jídelna	k1gFnPc2
odpovídal	odpovídat	k5eAaImAgMnS
současným	současný	k2eAgInPc3d1
trendům	trend	k1gInPc3
zdravé	zdravý	k2eAgFnSc2d1
výživy	výživa	k1gFnSc2
a	a	k8xC
školní	školní	k2eAgInPc4d1
obědy	oběd	k1gInPc4
přispívaly	přispívat	k5eAaImAgFnP
ke	k	k7c3
zdravému	zdravý	k2eAgInSc3d1
životnímu	životní	k2eAgInSc3d1
stylu	styl	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
nedlouho	dlouho	k6eNd1
po	po	k7c6
zveřejnění	zveřejnění	k1gNnSc6
dopisu	dopis	k1gInSc2
aktualizovalo	aktualizovat	k5eAaBmAgNnS
vyhlášku	vyhláška	k1gFnSc4
stanovující	stanovující	k2eAgNnSc1d1
omezení	omezení	k1gNnSc1
prodeje	prodej	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
rámci	rámec	k1gInSc6
níž	nízce	k6eAd2
vyjmulo	vyjmout	k5eAaPmAgNnS
střední	střední	k2eAgFnPc4d1
školy	škola	k1gFnPc4
a	a	k8xC
vyhovělo	vyhovět	k5eAaPmAgNnS
tak	tak	k8xC,k8xS
požadavkům	požadavek	k1gInPc3
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ne	ne	k9
dabingu	dabing	k1gInSc3
<g/>
:	:	kIx,
Učme	učit	k5eAaImRp1nP
se	se	k3xPyFc4
jinak	jinak	k6eAd1
</s>
<s>
Brzy	brzy	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vzniku	vznik	k1gInSc6
unie	unie	k1gFnSc2
realizovala	realizovat	k5eAaBmAgFnS
projekt	projekt	k1gInSc4
Ne	ne	k9
dabingu	dabing	k1gInSc3
<g/>
:	:	kIx,
Učme	učit	k5eAaImRp1nP
se	se	k3xPyFc4
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
upozornit	upozornit	k5eAaPmF
na	na	k7c4
problém	problém	k1gInSc4
nízké	nízký	k2eAgFnSc2d1
jazykové	jazykový	k2eAgFnSc2d1
vybavenosti	vybavenost	k1gFnSc2
české	český	k2eAgFnSc2d1
populace	populace	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc4
korelaci	korelace	k1gFnSc4
s	s	k7c7
dabováním	dabování	k1gNnSc7
televizních	televizní	k2eAgInPc2d1
pořadů	pořad	k1gInPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
prosadit	prosadit	k5eAaPmF
v	v	k7c6
co	co	k9
nejvyšší	vysoký	k2eAgFnSc6d3
míře	míra	k1gFnSc6
tzv.	tzv.	kA
duální	duální	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
(	(	kIx(
<g/>
přepínatelné	přepínatelný	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
v	v	k7c6
obou	dva	k4xCgInPc6
jazycích	jazyk	k1gInPc6
a	a	k8xC
s	s	k7c7
titulky	titulek	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzorová	vzorový	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
dále	daleko	k6eAd2
uskutečnila	uskutečnit	k5eAaPmAgFnS
projekt	projekt	k1gInSc4
Vzorová	vzorový	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
sestávající	sestávající	k2eAgFnSc1d1
z	z	k7c2
několika	několik	k4yIc2
workshopů	workshop	k1gInPc2
pro	pro	k7c4
zástupce	zástupce	k1gMnPc4
žákovských	žákovský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
výstupem	výstup	k1gInSc7
je	být	k5eAaImIp3nS
brožura	brožura	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
poskytuje	poskytovat	k5eAaImIp3nS
rady	rada	k1gFnPc4
pro	pro	k7c4
žáky	žák	k1gMnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
na	na	k7c6
své	svůj	k3xOyFgFnSc6
škole	škola	k1gFnSc6
založit	založit	k5eAaPmF
a	a	k8xC
vést	vést	k5eAaImF
žákovský	žákovský	k2eAgInSc4d1
parlament	parlament	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
aktivity	aktivita	k1gFnPc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
uskutečňuje	uskutečňovat	k5eAaImIp3nS
různé	různý	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
debaty	debata	k1gFnPc4
<g/>
,	,	kIx,
soutěže	soutěž	k1gFnPc4
či	či	k8xC
kulaté	kulatý	k2eAgInPc4d1
stoly	stol	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Financování	financování	k1gNnSc1
</s>
<s>
Spolek	spolek	k1gInSc1
vede	vést	k5eAaImIp3nS
transparentní	transparentní	k2eAgInSc4d1
účet	účet	k1gInSc4
u	u	k7c2
Fio	Fio	k1gFnSc2
Banky	banka	k1gFnSc2
a	a	k8xC
dobrovolně	dobrovolně	k6eAd1
zveřejňuje	zveřejňovat	k5eAaImIp3nS
výroční	výroční	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k9
meziroční	meziroční	k2eAgNnSc1d1
hospodaření	hospodaření	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInPc1d3
příjmy	příjem	k1gInPc1
tvoří	tvořit	k5eAaImIp3nP
získané	získaný	k2eAgFnPc1d1
dotace	dotace	k1gFnPc1
od	od	k7c2
ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
krajů	kraj	k1gInPc2
nebo	nebo	k8xC
granty	grant	k1gInPc4
nadací	nadace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
OBESSU	OBESSU	kA
|	|	kIx~
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
CSU	CSU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.obessu.org	www.obessu.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TZ	TZ	kA
Středoškoláci	středoškolák	k1gMnPc1
se	se	k3xPyFc4
sjednotili	sjednotit	k5eAaPmAgMnP
na	na	k7c6
půdě	půda	k1gFnSc6
Senátu	senát	k1gInSc2
<g/>
↑	↑	k?
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
REPORTÁŽ	reportáž	k1gFnSc1
<g/>
:	:	kIx,
Středoškoláci	středoškolák	k1gMnPc1
se	se	k3xPyFc4
dočkali	dočkat	k5eAaPmAgMnP
-	-	kIx~
mají	mít	k5eAaImIp3nP
vlastní	vlastní	k2eAgFnSc4d1
unii	unie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
vznikala	vznikat	k5eAaImAgNnP
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
chce	chtít	k5eAaImIp3nS
reprezentovat	reprezentovat	k5eAaImF
středoškoláky	středoškolák	k1gMnPc4
z	z	k7c2
celé	celý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
bojovat	bojovat	k5eAaImF
za	za	k7c4
jejich	jejich	k3xOp3gInPc4
zájmy	zájem	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Student	student	k1gMnSc1
Times	Times	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
7.02	7.02	k4
<g/>
.2016	.2016	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
stredoskolskaunie	stredoskolskaunie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
stredoskolskaunie	stredoskolskaunie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Revoluce	revoluce	k1gFnSc1
na	na	k7c4
střední	střední	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Středoškoláci	středoškolák	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
přispět	přispět	k5eAaPmF
ke	k	k7c3
změně	změna	k1gFnSc3
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dobrý	dobrý	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
srdci	srdce	k1gNnSc6
celý	celý	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
by	by	kYmCp3nS
inspirovat	inspirovat	k5eAaBmF
a	a	k8xC
vzbudit	vzbudit	k5eAaPmF
zájem	zájem	k1gInSc4
o	o	k7c4
vědění	vědění	k1gNnSc4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
Kment	Kment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
|	|	kIx~
Video	video	k1gNnSc1
-	-	kIx~
Jen	jen	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
musíte	muset	k5eAaImIp2nP
vidět	vidět	k5eAaImF
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Žáci	Žák	k1gMnPc1
mají	mít	k5eAaImIp3nP
hodnotit	hodnotit	k5eAaImF
učitele	učitel	k1gMnPc4
<g/>
:	:	kIx,
„	„	k?
<g/>
Naším	náš	k3xOp1gInSc7
cílem	cíl	k1gInSc7
není	být	k5eNaImIp3nS
udělat	udělat	k5eAaPmF
revoluci	revoluce	k1gFnSc4
přes	přes	k7c4
noc	noc	k1gFnSc4
<g/>
,	,	kIx,
<g/>
“	“	k?
vysvětluje	vysvětlovat	k5eAaImIp3nS
předseda	předseda	k1gMnSc1
České	český	k2eAgFnSc2d1
středoškolské	středoškolský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
www.rozhlas.cz	www.rozhlas.cz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gratias	Gratias	k1gInSc1
Tibi	Tibi	k1gNnSc1
<g/>
.	.	kIx.
www.gratiastibi.cz	www.gratiastibi.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Středoškoláci	středoškolák	k1gMnPc1
nesouhlasí	souhlasit	k5eNaImIp3nP
s	s	k7c7
povinnou	povinný	k2eAgFnSc7d1
maturitní	maturitní	k2eAgFnSc7d1
zkouškou	zkouška	k1gFnSc7
z	z	k7c2
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Povinná	povinný	k2eAgFnSc1d1
maturita	maturita	k1gFnSc1
z	z	k7c2
matematiky	matematika	k1gFnSc2
znásobí	znásobit	k5eAaPmIp3nS
odpadlíky	odpadlík	k1gMnPc4
<g/>
,	,	kIx,
varují	varovat	k5eAaImIp3nP
středoškoláci	středoškolák	k1gMnPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-16	2017-05-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TELEVIZE	televize	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuka	výuka	k1gFnSc1
prý	prý	k9
stále	stále	k6eAd1
nepřipravuje	připravovat	k5eNaImIp3nS
žáky	žák	k1gMnPc4
dobře	dobře	k6eAd1
na	na	k7c4
maturitu	maturita	k1gFnSc4
z	z	k7c2
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Středoškoláci	středoškolák	k1gMnPc1
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nP
o	o	k7c4
slovo	slovo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perpetuum	perpetuum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Budoucí	budoucí	k2eAgMnPc1d1
maturanti	maturant	k1gMnPc1
protestovali	protestovat	k5eAaBmAgMnP
proti	proti	k7c3
matematice	matematika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pres	pres	k1gInSc1
<g/>
.	.	kIx.
<g/>
UPmedia	UPmedium	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Setkali	setkat	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
se	se	k3xPyFc4
s	s	k7c7
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
pro	pro	k7c4
vzdělávání	vzdělávání	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.vaclavhavel-library.org	www.vaclavhavel-library.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.vaclavhavel-library.org	www.vaclavhavel-library.org	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
EDUIN	EDUIN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
si	se	k3xPyFc3
myslí	myslet	k5eAaImIp3nP
středoškoláci	středoškolák	k1gMnPc1
o	o	k7c6
školách	škola	k1gFnPc6
na	na	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
vzdělávají	vzdělávat	k5eAaImIp3nP
-	-	kIx~
Kulatý	kulatý	k2eAgInSc4d1
stůl	stůl	k1gInSc4
SKAV	SKAV	kA
a	a	k8xC
EDUin	EDUin	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
případu	případ	k1gInSc6
šikany	šikana	k1gFnSc2
selhal	selhat	k5eAaPmAgInS
i	i	k9
školský	školský	k2eAgMnSc1d1
ombudsman	ombudsman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
by	by	kYmCp3nP
z	z	k7c2
toho	ten	k3xDgNnSc2
vyvodit	vyvodit	k5eAaPmF,k5eAaBmF
zodpovědnost	zodpovědnost	k1gFnSc4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
zástupce	zástupce	k1gMnSc1
studentů	student	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.rozhlas.cz	www.rozhlas.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Středoškoláci	středoškolák	k1gMnPc1
žádají	žádat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
vysílala	vysílat	k5eAaImAgFnS
dvojjazyčně	dvojjazyčně	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
E	E	kA
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
RESPEKTU	respekt	k1gInSc2
<g/>
,	,	kIx,
Redakce	redakce	k1gFnSc1
<g/>
.	.	kIx.
www.respekt.cz	www.respekt.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.respekt.cz	www.respekt.cz	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČESKÁ	český	k2eAgFnSc1d1
STŘEDOŠKOLSKÁ	středoškolský	k2eAgFnSc1d1
UNIE	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V.	V.	kA
republikový	republikový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
na	na	k7c6
České	český	k2eAgFnSc6d1
televizi	televize	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČESKÁ	český	k2eAgFnSc1d1
STŘEDOŠKOLSKÁ	středoškolský	k2eAgFnSc1d1
UNIE	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Debata	debata	k1gFnSc1
o	o	k7c6
proměnách	proměna	k1gFnPc6
výuky	výuka	k1gFnSc2
na	na	k7c4
VI	VI	kA
<g/>
.	.	kIx.
republikovém	republikový	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
škol	škola	k1gFnPc2
povinné	povinný	k2eAgFnSc2d1
dílny	dílna	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Firmy	firma	k1gFnPc4
si	se	k3xPyFc3
slibují	slibovat	k5eAaImIp3nP
větší	veliký	k2eAgFnSc4d2
prestiž	prestiž	k1gFnSc4
manuálních	manuální	k2eAgFnPc2d1
profesí	profes	k1gFnPc2
—	—	k?
ČT24	ČT24	k1gFnSc2
—	—	k?
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
www.ceskatelevize.cz	www.ceskatelevize.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
26.2	26.2	k4
<g/>
.2018	.2018	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Do	do	k7c2
škol	škola	k1gFnPc2
se	se	k3xPyFc4
možná	možná	k9
vrátí	vrátit	k5eAaPmIp3nP
dílny	dílna	k1gFnPc1
a	a	k8xC
pozemky	pozemek	k1gInPc1
<g/>
,	,	kIx,
zástupci	zástupce	k1gMnPc1
středoškoláků	středoškolák	k1gMnPc2
kroutí	kroutit	k5eAaImIp3nP
hlavou	hlava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lenka	Lenka	k1gFnSc1
Štěpánová	Štěpánová	k1gFnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
dílen	dílna	k1gFnPc2
a	a	k8xC
pozemků	pozemek	k1gInPc2
do	do	k7c2
škol	škola	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
A	a	k8xC
co	co	k9
učit	učit	k5eAaImF
se	se	k3xPyFc4
prát	prát	k5eAaImF
prádlo	prádlo	k1gNnSc4
na	na	k7c6
valše	valcha	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.ceskaskola.cz	www.ceskaskola.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Středoškoláci	středoškolák	k1gMnPc1
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
podílet	podílet	k5eAaImF
na	na	k7c6
rozvoji	rozvoj	k1gInSc6
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
ustanovili	ustanovit	k5eAaPmAgMnP
na	na	k7c4
to	ten	k3xDgNnSc4
sněm	sněm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fond	fond	k1gInSc1
pro	pro	k7c4
samosprávy	samospráva	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Středoškoláci	středoškolák	k1gMnPc1
kritizují	kritizovat	k5eAaImIp3nP
dosazení	dosazení	k1gNnSc4
nového	nový	k2eAgMnSc2d1
školského	školský	k2eAgMnSc2d1
ombudsmana	ombudsman	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
V	v	k7c6
případu	případ	k1gInSc6
šikany	šikana	k1gFnSc2
selhal	selhat	k5eAaPmAgInS
i	i	k9
školský	školský	k2eAgMnSc1d1
ombudsman	ombudsman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
by	by	kYmCp3nP
z	z	k7c2
toho	ten	k3xDgNnSc2
vyvodit	vyvodit	k5eAaBmF,k5eAaPmF
zodpovědnost	zodpovědnost	k1gFnSc4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
zástupce	zástupce	k1gMnSc1
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
www.rozhlas.cz	www.rozhlas.cz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Expres	expres	k1gInSc1
řeší	řešit	k5eAaImIp3nS
šikanu	šikana	k1gFnSc4
u	u	k7c2
školského	školský	k2eAgMnSc2d1
ombudsmana	ombudsman	k1gMnSc2
<g/>
:	:	kIx,
Marně	marně	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
člověk	člověk	k1gMnSc1
nereaguje	reagovat	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expres	expres	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-03-02	2016-03-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Věděli	vědět	k5eAaImAgMnP
jste	být	k5eAaImIp2nP
<g/>
,	,	kIx,
že	že	k8xS
máme	mít	k5eAaImIp1nP
školského	školský	k2eAgMnSc4d1
ombudsmana	ombudsman	k1gMnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
My	my	k3xPp1nPc1
taky	taky	k9
ne	ne	k9
<g/>
,	,	kIx,
řeší	řešit	k5eAaImIp3nS
se	se	k3xPyFc4
šikana	šikana	k1gFnSc1
a	a	k8xC
on	on	k3xPp3gMnSc1
mlčí	mlčet	k5eAaImIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expres	expres	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-02-26	2016-02-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nový	nový	k2eAgMnSc1d1
školský	školský	k2eAgMnSc1d1
ombudsman	ombudsman	k1gMnSc1
je	být	k5eAaImIp3nS
Slávek	Slávek	k1gMnSc1
Hrzal	Hrzal	k1gMnSc1
<g/>
,	,	kIx,
organizátor	organizátor	k1gMnSc1
Zlatého	zlatý	k2eAgMnSc2d1
Ámose	Ámos	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HRONOVÁ	Hronová	k1gFnSc1
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
hodiny	hodina	k1gFnPc4
matematiky	matematika	k1gFnSc2
budou	být	k5eAaImBp3nP
dva	dva	k4xCgMnPc1
učitelé	učitel	k1gMnPc1
<g/>
,	,	kIx,
třída	třída	k1gFnSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
napůl	napůl	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
jazyků	jazyk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bude	být	k5eAaImBp3nS
víc	hodně	k6eAd2
prostoru	prostor	k1gInSc2
na	na	k7c4
rozvoj	rozvoj	k1gInSc4
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Výzkumy	výzkum	k1gInPc1
podporující	podporující	k2eAgInPc1d1
hodnocení	hodnocení	k1gNnSc3
učitelů	učitel	k1gMnPc2
žáky	žák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Odtajni	odtajnit	k5eAaPmRp2nS
učebnu	učebna	k1gFnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Research	Research	k1gInSc1
on	on	k3xPp3gMnSc1
STeP	step	k1gFnSc4
Survey	Survea	k1gMnSc2
-	-	kIx~
My	my	k3xPp1nPc1
Student	student	k1gMnSc1
Survey	Survea	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
My	my	k3xPp1nPc1
Student	student	k1gMnSc1
Survey	Survea	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Práce	práce	k1gFnSc1
s	s	k7c7
žákovskou	žákovský	k2eAgFnSc7d1
zpětnou	zpětný	k2eAgFnSc7d1
vazbou	vazba	k1gFnSc7
a	a	k8xC
hodnocení	hodnocení	k1gNnPc4
učitelů	učitel	k1gMnPc2
v	v	k7c6
Norsku	Norsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HRONOVÁ	Hronová	k1gFnSc1
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středoškoláci	středoškolák	k1gMnPc1
žádají	žádat	k5eAaImIp3nP
<g/>
:	:	kIx,
plat	plat	k1gInSc1
učitele	učitel	k1gMnSc2
by	by	kYmCp3nP
měl	mít	k5eAaImAgInS
být	být	k5eAaImF
závislý	závislý	k2eAgInSc1d1
i	i	k9
na	na	k7c4
hodnocení	hodnocení	k1gNnPc4
žáků	žák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Poslanci	poslanec	k1gMnPc1
definitivně	definitivně	k6eAd1
pohřbili	pohřbít	k5eAaPmAgMnP
kariérní	kariérní	k2eAgInSc4d1
řád	řád	k1gInSc4
učitelů	učitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neprošla	projít	k5eNaPmAgFnS
ani	ani	k9
verze	verze	k1gFnSc1
Senátu	senát	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-12	2017-07-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rádio	rádio	k1gNnSc1
Junior	junior	k1gMnSc1
(	(	kIx(
<g/>
archiv	archiv	k1gInSc1
-	-	kIx~
Atrium	atrium	k1gNnSc1
<g/>
:	:	kIx,
Škola	škola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
prehravac	prehravac	k1gInSc1
<g/>
.	.	kIx.
<g/>
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČSU	ČSU	kA
<g/>
:	:	kIx,
Víkend	víkend	k1gInSc1
studentských	studentský	k2eAgFnPc2d1
samospráv	samospráva	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strukturovaný	strukturovaný	k2eAgInSc1d1
dialog	dialog	k1gInSc1
s	s	k7c7
mládeží	mládež	k1gFnSc7
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zažij	zažít	k5eAaPmRp2nS
střední	střední	k2eAgFnSc2d1
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Středoškoláci	středoškolák	k1gMnPc1
přemluvili	přemluvit	k5eAaPmAgMnP
Valachovou	Valachová	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
výjimku	výjimka	k1gFnSc4
z	z	k7c2
pamlskové	pamlskový	k2eAgFnSc2d1
vyhlášky	vyhláška	k1gFnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-09-09	2016-09-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
stredoskolskaunie	stredoskolskaunie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
středoškolská	středoškolský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Ne	ne	k9
dabingu	dabing	k1gInSc3
<g/>
:	:	kIx,
Učme	učit	k5eAaImRp1nP
se	se	k3xPyFc4
jinak	jinak	k6eAd1
</s>
<s>
Vzorová	vzorový	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kv	kv	k?
<g/>
2014802666	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
306299731	#num#	k4
</s>
