<s>
Místem	místo	k1gNnSc7
těchto	tento	k3xDgFnPc2
epidemií	epidemie	k1gFnPc2
byly	být	k5eAaImAgFnP
především	především	k9
africké	africký	k2eAgFnPc1d1
země	zem	k1gFnPc1
jako	jako	k8xS
Demokratická	demokratický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Republika	republika	k1gFnSc1
Kongo	Kongo	k1gNnSc1
<g/>
,	,	kIx,
Gabon	Gabon	k1gNnSc1
<g/>
,	,	kIx,
Uganda	Uganda	k1gFnSc1
a	a	k8xC
Súdán	Súdán	k1gInSc1
<g/>
,	,	kIx,
během	během	k7c2
žádné	žádný	k3yNgFnSc2
z	z	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
epidemií	epidemie	k1gFnPc2
nebylo	být	k5eNaImAgNnS
nakaženo	nakazit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
500	[number]	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>