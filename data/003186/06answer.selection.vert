<s>
Metalové	metalový	k2eAgNnSc1d1	metalové
publikum	publikum	k1gNnSc1	publikum
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
zejména	zejména	k9	zejména
z	z	k7c2	z
pracujících	pracující	k2eAgMnPc2d1	pracující
bělochů	běloch	k1gMnPc2	běloch
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
(	(	kIx(	(
<g/>
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
hrabství	hrabství	k1gNnSc6	hrabství
West	West	k2eAgInSc4d1	West
Midlands	Midlands	k1gInSc4	Midlands
a	a	k8xC	a
státech	stát	k1gInPc6	stát
USA	USA	kA	USA
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
byl	být	k5eAaImAgInS	být
metal	metal	k1gInSc1	metal
většinou	většinou	k6eAd1	většinou
hudbou	hudba	k1gFnSc7	hudba
'	'	kIx"	'
<g/>
dělnické	dělnický	k2eAgFnSc2d1	Dělnická
mládeže	mládež	k1gFnSc2	mládež
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
