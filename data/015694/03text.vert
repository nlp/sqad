<s>
Alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
2017	#num#	k4
</s>
<s>
Alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
2017	#num#	k4
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Perman	Perman	k1gMnSc1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Primátorská	primátorský	k2eAgFnSc1d1
296	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
180	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Ideologie	ideologie	k1gFnSc2
</s>
<s>
konzervatismus	konzervatismus	k1gInSc1
<g/>
,	,	kIx,
euroskepticismus	euroskepticismus	k1gInSc1
IČO	IČO	kA
</s>
<s>
03244750	#num#	k4
(	(	kIx(
<g/>
PSH	PSH	kA
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
apcr	apcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
2017	#num#	k4
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
APAČI	Apač	k1gMnSc3
2017	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
české	český	k2eAgFnPc4d1
euroskeptické	euroskeptický	k2eAgFnPc4d1
<g/>
,	,	kIx,
konzervativní	konzervativní	k2eAgFnPc4d1
a	a	k8xC
krajně	krajně	k6eAd1
pravicové	pravicový	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zaregistrováno	zaregistrovat	k5eAaPmNgNnS
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
předsedou	předseda	k1gMnSc7
je	být	k5eAaImIp3nS
Richard	Richard	k1gMnSc1
Perman	Perman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Hnutí	hnutí	k1gNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
okruhu	okruh	k1gInSc6
některých	některý	k3yIgMnPc2
bývalých	bývalý	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Bloku	blok	k1gInSc2
proti	proti	k7c3
islámu	islám	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
rozpustil	rozpustit	k5eAaPmAgInS
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zprvu	zprvu	k6eAd1
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
sporu	spor	k1gInSc2
o	o	k7c4
název	název	k1gInSc4
s	s	k7c7
čerstvě	čerstvě	k6eAd1
zaregistrovanou	zaregistrovaný	k2eAgFnSc7d1
politickou	politický	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
Alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
Česko	Česko	k1gNnSc4
(	(	kIx(
<g/>
APČ	APČ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
chtělo	chtít	k5eAaImAgNnS
hnutí	hnutí	k1gNnSc4
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
islamizaci	islamizace	k1gFnSc3
a	a	k8xC
multikulturalismu	multikulturalismus	k1gInSc2
<g/>
,	,	kIx,
Evropskou	evropský	k2eAgFnSc4d1
unii	unie	k1gFnSc4
chtělo	chtít	k5eAaImAgNnS
transformovat	transformovat	k5eAaBmF
zpět	zpět	k6eAd1
do	do	k7c2
Evropského	evropský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
a	a	k8xC
nevylučovalo	vylučovat	k5eNaImAgNnS
ani	ani	k8xC
podporu	podpora	k1gFnSc4
„	„	k?
<g/>
czexitu	czexit	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtělo	chtít	k5eAaImAgNnS
také	také	k6eAd1
zjednodušit	zjednodušit	k5eAaPmF
daňový	daňový	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
omezit	omezit	k5eAaPmF
zbytečné	zbytečný	k2eAgFnPc4d1
regulace	regulace	k1gFnPc4
či	či	k8xC
zákony	zákon	k1gInPc4
<g/>
,	,	kIx,
„	„	k?
<g/>
odstřihnout	odstřihnout	k5eAaPmF
<g/>
“	“	k?
politické	politický	k2eAgFnSc2d1
neziskové	ziskový	k2eNgFnSc2d1
organizace	organizace	k1gFnSc2
od	od	k7c2
veřejných	veřejný	k2eAgInPc2d1
rozpočtů	rozpočet	k1gInPc2
<g/>
,	,	kIx,
zrušit	zrušit	k5eAaPmF
koncesionářské	koncesionářský	k2eAgInPc4d1
poplatky	poplatek	k1gInPc4
a	a	k8xC
„	„	k?
<g/>
napravit	napravit	k5eAaPmF
<g/>
“	“	k?
veřejnoprávní	veřejnoprávní	k2eAgNnPc1d1
média	médium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lídry	lídr	k1gMnPc4
hnutí	hnutí	k1gNnPc2
byli	být	k5eAaImAgMnP
v	v	k7c6
jeho	jeho	k3xOp3gInPc6
začátcích	začátek	k1gInPc6
Petr	Petr	k1gMnSc1
Hampl	Hampl	k1gMnSc1
(	(	kIx(
<g/>
zvolen	zvolen	k2eAgMnSc1d1
na	na	k7c6
ustavujícím	ustavující	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
předsedou	předseda	k1gMnSc7
<g/>
)	)	kIx)
a	a	k8xC
Martin	Martin	k1gMnSc1
Konvička	konvička	k1gFnSc1
<g/>
,	,	kIx,
místopředsedou	místopředseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hudebník	hudebník	k1gMnSc1
a	a	k8xC
publicista	publicista	k1gMnSc1
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
hnutí	hnutí	k1gNnSc2
vyjádřil	vyjádřit	k5eAaPmAgMnS
také	také	k9
Ladislav	Ladislav	k1gMnSc1
Jakl	Jakl	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
Ivo	Ivo	k1gMnSc1
Strejček	Strejček	k1gMnSc1
z	z	k7c2
Institutu	institut	k1gInSc2
Václava	Václav	k1gMnSc2
Klause	Klaus	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
PČR	PČR	kA
2016	#num#	k4
</s>
<s>
Do	do	k7c2
senátních	senátní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
v	v	k7c6
říjnu	říjen	k1gInSc6
2016	#num#	k4
za	za	k7c4
hnutí	hnutí	k1gNnSc4
kandidovali	kandidovat	k5eAaImAgMnP
její	její	k3xOp3gFnSc4
předseda	předseda	k1gMnSc1
Hampl	Hampl	k1gMnSc1
(	(	kIx(
<g/>
Beroun	Beroun	k1gInSc1
<g/>
)	)	kIx)
s	s	k7c7
místopředsedou	místopředseda	k1gMnSc7
Štěpánkem	Štěpánek	k1gMnSc7
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Martin	Martin	k1gInSc1
Konvička	konvička	k1gFnSc1
(	(	kIx(
<g/>
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Slovenské	slovenský	k2eAgFnSc2d1
televize	televize	k1gFnSc2
Radim	Radim	k1gMnSc1
Hreha	Hreha	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
předsedkyně	předsedkyně	k1gFnPc4
politické	politický	k2eAgFnSc3d1
neziskové	ziskový	k2eNgFnSc3d1
organizce	organizka	k1gFnSc3
Naštvané	naštvaný	k2eAgFnSc2d1
matky	matka	k1gFnSc2
Eva	Eva	k1gFnSc1
Hrindová	Hrindový	k2eAgFnSc1d1
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
souběžných	souběžný	k2eAgFnPc2d1
krajských	krajský	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
hnutí	hnutí	k1gNnSc2
po	po	k7c6
rozkolu	rozkol	k1gInSc6
s	s	k7c7
Úsvitem	úsvit	k1gInSc7
–	–	k?
národní	národní	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
nekandidovalo	kandidovat	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
8	#num#	k4
<g/>
]	]	kIx)
Konvička	konvička	k1gFnSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
8,07	8,07	k4
procenta	procento	k1gNnSc2
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Hampl	Hampl	k1gMnSc1
skončil	skončit	k5eAaPmAgMnS
na	na	k7c6
sedmém	sedmý	k4xOgNnSc6
místě	místo	k1gNnSc6
se	s	k7c7
ziskem	zisk	k1gInSc7
7,1	7,1	k4
procenta	procento	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
ve	v	k7c6
volbách	volba	k1gFnPc6
Hampl	Hampl	k1gMnSc1
i	i	k8xC
Konvička	konvička	k1gFnSc1
hnutí	hnutí	k1gNnSc2
opustili	opustit	k5eAaPmAgMnP
<g/>
,	,	kIx,
ve	v	k7c4
vedení	vedení	k1gNnSc4
měl	mít	k5eAaImAgInS
dále	daleko	k6eAd2
zůstat	zůstat	k5eAaPmF
Petr	Petr	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2017	#num#	k4
Alternativě	alternativa	k1gFnSc6
Nejvyšší	vysoký	k2eAgInSc1d3
správní	správní	k2eAgInSc1d1
soud	soud	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
pozastavil	pozastavit	k5eAaPmAgInS
činnost	činnost	k1gFnSc4
kvůli	kvůli	k7c3
neúplným	úplný	k2eNgFnPc3d1
finančním	finanční	k2eAgFnPc3d1
zprávám	zpráva	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
Česku	Česko	k1gNnSc6
2019	#num#	k4
</s>
<s>
Hnutí	hnutí	k1gNnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
voleb	volba	k1gFnPc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
lídryně	lídryně	k1gFnSc1
kandidátky	kandidátka	k1gFnSc2
byla	být	k5eAaImAgFnS
ohlášena	ohlášen	k2eAgFnSc1d1
Klára	Klára	k1gFnSc1
Samková	Samková	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
dalších	další	k2eAgNnPc6d1
místech	místo	k1gNnPc6
pak	pak	k6eAd1
Luděk	Luděk	k1gMnSc1
Nezmar	nezmar	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Hrbek	Hrbek	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Vodička	Vodička	k1gMnSc1
a	a	k8xC
Vojtěch	Vojtěch	k1gMnSc1
Merunka	Merunka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
těchto	tento	k3xDgFnPc6
volbách	volba	k1gFnPc6
obdrželo	obdržet	k5eAaPmAgNnS
11	#num#	k4
729	#num#	k4
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
0,49	0,49	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
nezískalo	získat	k5eNaPmAgNnS
tak	tak	k9
žádný	žádný	k3yNgInSc4
mandát	mandát	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
ČTK	ČTK	kA
<g/>
;	;	kIx,
KLICNAR	KLICNAR	kA
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
(	(	kIx(
<g/>
fka	fka	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpůrci	odpůrce	k1gMnPc1
islámu	islám	k1gInSc2
založili	založit	k5eAaPmAgMnP
Alternativu	alternativa	k1gFnSc4
pro	pro	k7c4
ČR	ČR	kA
2017	#num#	k4
<g/>
,	,	kIx,
podpořil	podpořit	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
i	i	k9
Jakl	Jakl	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-06-26	2016-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hampl	Hampl	k1gMnSc1
s	s	k7c7
Konvičkou	konvička	k1gFnSc7
chtějí	chtít	k5eAaImIp3nP
získat	získat	k5eAaPmF
název	název	k1gInSc4
Alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Máme	mít	k5eAaImIp1nP
na	na	k7c4
něj	on	k3xPp3gNnSc4
ochrannou	ochranný	k2eAgFnSc4d1
známku	známka	k1gFnSc4
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-06-10	2016-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpůrci	odpůrce	k1gMnPc1
muslimů	muslim	k1gMnPc2
se	se	k3xPyFc4
hádají	hádat	k5eAaImIp3nP
o	o	k7c4
název	název	k1gInSc4
Alternativa	alternativa	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
jsou	být	k5eAaImIp3nP
celkem	celkem	k6eAd1
tři	tři	k4xCgFnPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-06-10	2016-06-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Klausova	Klausův	k2eAgInSc2d1
institutu	institut	k1gInSc2
zní	znět	k5eAaImIp3nS
podpora	podpora	k1gFnSc1
nově	nově	k6eAd1
vznikající	vznikající	k2eAgFnSc3d1
Alternativě	alternativa	k1gFnSc3
pro	pro	k7c4
Česko	Česko	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-05-10	2016-05-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
PŮLPÁN	půlpán	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgNnPc1
senátní	senátní	k2eAgNnPc1d1
křesla	křeslo	k1gNnPc1
z	z	k7c2
kraje	kraj	k1gInSc2
budou	být	k5eAaImBp3nP
hájit	hájit	k5eAaImF
lidovci	lidovec	k1gMnPc1
<g/>
,	,	kIx,
soupeřů	soupeř	k1gMnPc2
je	být	k5eAaImIp3nS
letos	letos	k6eAd1
dost	dost	k6eAd1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Pardubice	Pardubice	k1gInPc1
a	a	k8xC
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-08-31	2016-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
POLÁČEK	Poláček	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matador	matador	k1gMnSc1
Tesařík	Tesařík	k1gMnSc1
čelí	čelit	k5eAaImIp3nS
při	při	k7c6
obhajobě	obhajoba	k1gFnSc6
senátorského	senátorský	k2eAgNnSc2d1
křesla	křeslo	k1gNnSc2
osmi	osm	k4xCc2
vyzyvatelům	vyzyvatelům	k?
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-10-05	2016-10-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alternativa	alternativa	k1gFnSc1
vyšle	vyslat	k5eAaPmIp3nS
do	do	k7c2
senátních	senátní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
Konvičku	konvička	k1gFnSc4
nebo	nebo	k8xC
Hrehu	Hreha	k1gFnSc4
<g/>
,	,	kIx,
shání	shánět	k5eAaImIp3nS
peníze	peníz	k1gInPc4
na	na	k7c4
kampaň	kampaň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-07-13	2016-07-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BENEŠ	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konvička	konvička	k1gFnSc1
vzdal	vzdát	k5eAaPmAgInS
kandidaturu	kandidatura	k1gFnSc4
na	na	k7c4
hejtmana	hejtman	k1gMnSc4
<g/>
,	,	kIx,
chce	chtít	k5eAaImIp3nS
ale	ale	k9
do	do	k7c2
Senátu	senát	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-08-03	2016-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
;	;	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konvička	konvička	k1gFnSc1
s	s	k7c7
oznámením	oznámení	k1gNnSc7
na	na	k7c4
Dolínka	Dolínek	k1gMnSc4
neuspěl	uspět	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nedůvodné	důvodný	k2eNgNnSc1d1
<g/>
,	,	kIx,
slyšel	slyšet	k5eAaImAgMnS
od	od	k7c2
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-11-15	2016-11-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
JANOUŠ	JANOUŠ	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
Alternativy	alternativa	k1gFnSc2
pro	pro	k7c4
ČR	ČR	kA
Hampl	Hampl	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
po	po	k7c6
neúspěchu	neúspěch	k1gInSc6
ve	v	k7c6
volbách	volba	k1gFnPc6
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-10-11	2016-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Konvička	konvička	k1gFnSc1
opouští	opouštět	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
partaj	partaj	k1gFnSc1
<g/>
:	:	kIx,
Apačové	Apač	k1gMnPc1
neplní	plnit	k5eNaImIp3nP
své	svůj	k3xOyFgInPc4
cíle	cíl	k1gInPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nešťastně	šťastně	k6eNd1
zrozený	zrozený	k2eAgInSc1d1
projekt	projekt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-07	2016-12-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soud	soud	k1gInSc1
pozastavil	pozastavit	k5eAaPmAgInS
činnost	činnost	k1gFnSc4
Alternativě	alternativa	k1gFnSc3
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-04-21	2017-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Advokátka	advokátka	k1gFnSc1
Samková	Samková	k1gFnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
kandidátku	kandidátka	k1gFnSc4
Alternativy	alternativa	k1gFnSc2
pro	pro	k7c4
ČR	ČR	kA
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-14	2019-02-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
afi	afi	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kandidáti	kandidát	k1gMnPc1
za	za	k7c4
Alternativu	alternativa	k1gFnSc4
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
2017	#num#	k4
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-05-17	2019-05-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Kandidáti	kandidát	k1gMnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
apcr	apcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
-	-	kIx~
Volební	volební	k2eAgInSc1d1
speciál	speciál	k1gInSc1
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Alternativa	alternativa	k1gFnSc1
pro	pro	k7c4
Českou	český	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
2017	#num#	k4
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
a	a	k8xC
hnutí	hnutí	k1gNnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
(	(	kIx(
<g/>
200	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
2011	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
63	#num#	k4
ANO	ano	k9
<g/>
,	,	kIx,
15	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
21	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
2	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
přímá	přímý	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
SPD	SPD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
13	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
3	#num#	k4
JAP	JAP	kA
<g/>
,	,	kIx,
3	#num#	k4
Trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
Senát	senát	k1gInSc1
(	(	kIx(
<g/>
81	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ODS	ODS	kA
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
16	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
4	#num#	k4
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
6	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
3	#num#	k4
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
a	a	k8xC
2	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
SLK	SLK	kA
<g/>
,	,	kIx,
1	#num#	k4
Ostravak	Ostravak	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
OPAT	opat	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
MHS	MHS	kA
a	a	k8xC
13	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
–	–	k?
11	#num#	k4
za	za	k7c7
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SD	SD	kA
<g/>
–	–	k?
<g/>
SN	SN	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
KDU-ČSL	KDU-ČSL	k1gFnPc2
a	a	k8xC
5	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
4	#num#	k4
za	za	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
NK	NK	kA
<g/>
)	)	kIx)
</s>
<s>
PROREGION	PROREGION	kA
(	(	kIx(
<g/>
9	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
ANO	ano	k9
a	a	k9
6	#num#	k4
bezpartijních	bezpartijní	k2eAgMnPc2d1
<g/>
,	,	kIx,
4	#num#	k4
za	za	k7c4
ANO	ano	k9
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
ČSSD	ČSSD	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
„	„	k?
<g/>
OSN	OSN	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
SEN	sen	k1gInSc1
21	#num#	k4
a	a	k8xC
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
7	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
SEN	sena	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Zelení	zeleň	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
HPP	HPP	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
3	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
;	;	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
Piráty	pirát	k1gMnPc4
a	a	k8xC
1	#num#	k4
za	za	k7c4
HDK	HDK	kA
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
S.	S.	kA
<g/>
cz	cz	k?
a	a	k8xC
2	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
1	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
1	#num#	k4
za	za	k7c4
Svobodní	svobodný	k2eAgMnPc1d1
<g/>
)	)	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
RE	re	k9
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ECR	ECR	kA
</s>
<s>
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
Greens	Greens	k1gInSc4
<g/>
/	/	kIx~
<g/>
EFA	EFA	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
STAN	stan	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
SPD	SPD	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ID	ido	k1gNnPc2
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
KSČM	KSČM	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
GUE-NGL	GUE-NGL	k1gFnSc6
Neparlamentní	parlamentní	k2eNgFnSc2d1
</s>
<s>
Alternativa	alternativa	k1gFnSc1
</s>
<s>
ANK	ANK	kA
2020	#num#	k4
</s>
<s>
APAČI	Apač	k1gMnSc3
2017	#num#	k4
</s>
<s>
ČP	ČP	kA
</s>
<s>
EU	EU	kA
TROLL	troll	k1gMnSc1
</s>
<s>
DSSS	DSSS	kA
</s>
<s>
DV	DV	kA
2016	#num#	k4
</s>
<s>
HLAS	hlas	k1gInSc1
</s>
<s>
IO	IO	kA
</s>
<s>
JsmePRO	JsmePRO	k?
<g/>
!	!	kIx.
</s>
<s>
M	M	kA
</s>
<s>
NBPLK	NBPLK	kA
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1
</s>
<s>
NeKa	NeKa	k6eAd1
</s>
<s>
ODA	ODA	kA
</s>
<s>
HPP	HPP	kA
</s>
<s>
PV	PV	kA
</s>
<s>
PZS	PZS	kA
</s>
<s>
Rozumní	rozumný	k2eAgMnPc1d1
</s>
<s>
SNK	SNK	kA
ED	ED	kA
</s>
<s>
SNK1	SNK1	k4
</s>
<s>
SPOLEHNUTÍ	spolehnutí	k1gNnSc1
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
osobnosti	osobnost	k1gFnPc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
</s>
<s>
Strana	strana	k1gFnSc1
Práv	právo	k1gNnPc2
Občanů	občan	k1gMnPc2
</s>
<s>
Strana	strana	k1gFnSc1
soukromníků	soukromník	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
T2020	T2020	k4
</s>
<s>
UFO	UFO	kA
</s>
<s>
USZ	USZ	kA
</s>
<s>
ZA	za	k7c4
OBČANY	občan	k1gMnPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Z	z	k7c2
2020	#num#	k4
Seznam	seznam	k1gInSc1
neparlamentních	parlamentní	k2eNgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
není	být	k5eNaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Politika	politika	k1gFnSc1
</s>
