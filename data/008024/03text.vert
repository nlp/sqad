<s>
Sarah	Sarah	k1gFnSc1	Sarah
Jessica	Jessica	k1gMnSc1	Jessica
Parker	Parker	k1gMnSc1	Parker
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Nelsonville	Nelsonville	k1gFnSc1	Nelsonville
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
producentka	producentka	k1gFnSc1	producentka
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
ztvárnění	ztvárnění	k1gNnSc4	ztvárnění
role	role	k1gFnSc2	role
Carrie	Carrie	k1gFnSc2	Carrie
Bradshaw	Bradshaw	k1gFnSc2	Bradshaw
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
za	za	k7c4	za
nějž	jenž	k3xRgMnSc4	jenž
obdržela	obdržet	k5eAaPmAgFnS	obdržet
čtyři	čtyři	k4xCgInPc4	čtyři
Zlaté	zlatý	k2eAgInPc4d1	zlatý
glóby	glóbus	k1gInPc4	glóbus
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
Ceny	cena	k1gFnPc4	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Kariéru	kariéra	k1gFnSc4	kariéra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
již	již	k6eAd1	již
jako	jako	k8xC	jako
osmiletá	osmiletý	k2eAgFnSc1d1	osmiletá
dívka	dívka	k1gFnSc1	dívka
a	a	k8xC	a
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
letech	léto	k1gNnPc6	léto
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
Broadwayi	Broadwaye	k1gFnSc4	Broadwaye
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
filmy	film	k1gInPc4	film
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
hrála	hrát	k5eAaImAgFnS	hrát
patří	patřit	k5eAaImIp3nS	patřit
Líbánky	líbánky	k1gInPc4	líbánky
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
Rapsodie	rapsodie	k1gFnSc1	rapsodie
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
obchodníka	obchodník	k1gMnSc2	obchodník
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
osmi	osm	k4xCc7	osm
sourozenci	sourozenec	k1gMnPc7	sourozenec
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
věnovat	věnovat	k5eAaPmF	věnovat
baletu	balet	k1gInSc2	balet
<g/>
.	.	kIx.	.
</s>
<s>
Hereckou	herecký	k2eAgFnSc4d1	herecká
kariéru	kariéra	k1gFnSc4	kariéra
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
osmi	osm	k4xCc6	osm
letech	léto	k1gNnPc6	léto
televizním	televizní	k2eAgInSc7d1	televizní
pořadem	pořad	k1gInSc7	pořad
Andersenovy	Andersenův	k2eAgFnSc2d1	Andersenova
pohádky	pohádka	k1gFnSc2	pohádka
The	The	k1gMnSc1	The
Little	Little	k1gFnSc2	Little
Match	Match	k1gMnSc1	Match
Girl	girl	k1gFnSc1	girl
(	(	kIx(	(
<g/>
Děvčátko	děvčátko	k1gNnSc1	děvčátko
se	s	k7c7	s
zápalkami	zápalka	k1gFnPc7	zápalka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Brodwayi	Brodway	k1gFnSc6	Brodway
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
jedenácti	jedenáct	k4xCc6	jedenáct
v	v	k7c4	v
titulní	titulní	k2eAgFnSc4d1	titulní
roli	role	k1gFnSc4	role
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
muzikálu	muzikál	k1gInSc2	muzikál
Annie	Annie	k1gFnSc2	Annie
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
známá	známý	k2eAgFnSc1d1	známá
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rolí	role	k1gFnSc7	role
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
ze	z	k7c2	z
školního	školní	k2eAgNnSc2d1	školní
prostředí	prostředí	k1gNnSc2	prostředí
Square	square	k1gInSc1	square
Pegs	Pegs	k1gInSc1	Pegs
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
)	)	kIx)	)
a	a	k8xC	a
rolí	role	k1gFnPc2	role
idealistické	idealistický	k2eAgFnSc2d1	idealistická
státní	státní	k2eAgFnSc2d1	státní
zástupkyně	zástupkyně	k1gFnSc2	zástupkyně
Jo	jo	k9	jo
Ann	Ann	k1gFnPc1	Ann
Harrisové	Harrisový	k2eAgFnPc1d1	Harrisová
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Equal	Equal	k1gMnSc1	Equal
Justice	justice	k1gFnSc2	justice
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
protagonistkou	protagonistka	k1gFnSc7	protagonistka
blonďatých	blonďatý	k2eAgFnPc2d1	blonďatá
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
vždy	vždy	k6eAd1	vždy
vztah	vztah	k1gInSc4	vztah
narušily	narušit	k5eAaPmAgFnP	narušit
neočekávané	očekávaný	k2eNgFnPc1d1	neočekávaná
události	událost	k1gFnPc1	událost
jako	jako	k9	jako
například	například	k6eAd1	například
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
L.A.	L.A.	k1gFnSc2	L.A.
<g/>
,	,	kIx,	,
Líbánky	líbánky	k1gFnPc1	líbánky
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
nebo	nebo	k8xC	nebo
Rhapsodie	Rhapsodie	k1gFnSc1	Rhapsodie
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
roli	role	k1gFnSc4	role
Callie	Callie	k1gFnSc1	Callie
Cain	Caina	k1gFnPc2	Caina
v	v	k7c6	v
televizním	televizní	k2eAgNnSc6d1	televizní
dramatu	drama	k1gNnSc6	drama
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
události	událost	k1gFnSc2	událost
V	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
zatím	zatím	k6eAd1	zatím
asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc1d3	nejznámější
role	role	k1gFnSc1	role
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
spisovatelku	spisovatelka	k1gFnSc4	spisovatelka
žijící	žijící	k2eAgFnSc4d1	žijící
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
Carrie	Carrie	k1gFnSc2	Carrie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
řeší	řešit	k5eAaImIp3nS	řešit
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
kamarádkami	kamarádka	k1gFnPc7	kamarádka
problémy	problém	k1gInPc4	problém
nezadaných	zadaný	k2eNgFnPc2d1	nezadaná
třicátnic	třicátnice	k1gFnPc2	třicátnice
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
rolí	role	k1gFnSc7	role
se	se	k3xPyFc4	se
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
žen	žena	k1gFnPc2	žena
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
také	také	k9	také
jako	jako	k9	jako
módní	módní	k2eAgFnSc1d1	módní
ikona	ikona	k1gFnSc1	ikona
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
film	film	k1gInSc4	film
Sex	sex	k1gInSc4	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
2	[number]	k4	2
získala	získat	k5eAaPmAgFnS	získat
spolu	spolu	k6eAd1	spolu
i	i	k9	i
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
hlavními	hlavní	k2eAgFnPc7d1	hlavní
herečkami	herečka	k1gFnPc7	herečka
cenu	cena	k1gFnSc4	cena
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
maliny	malina	k1gFnSc2	malina
za	za	k7c4	za
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
herečku	herečka	k1gFnSc4	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
známosti	známost	k1gFnSc6	známost
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
Downeyem	Downey	k1gMnSc7	Downey
jr	jr	k?	jr
<g/>
.	.	kIx.	.
a	a	k8xC	a
po	po	k7c6	po
pětiletém	pětiletý	k2eAgInSc6d1	pětiletý
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Matthewev	Matthewev	k1gFnSc1	Matthewev
Broderickem	Brodericko	k1gNnSc7	Brodericko
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vzala	vzít	k5eAaPmAgFnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Flight	Flight	k1gInSc1	Flight
of	of	k?	of
the	the	k?	the
Navigator	Navigator	k1gInSc1	Navigator
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Holky	holka	k1gFnPc1	holka
se	se	k3xPyFc4	se
chtějí	chtít	k5eAaImIp3nP	chtít
bavit	bavit	k5eAaImF	bavit
taky	taky	k6eAd1	taky
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Líbánky	líbánky	k1gFnPc1	líbánky
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
(	(	kIx(	(
<g/>
Honeymoon	Honeymoon	k1gInSc1	Honeymoon
in	in	k?	in
Vegas	Vegas	k1gInSc1	Vegas
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Na	na	k7c4	na
dostřel	dostřel	k1gInSc4	dostřel
(	(	kIx(	(
<g/>
Striking	Striking	k1gInSc4	Striking
Distance	distance	k1gFnSc2	distance
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Hokus	Hokus	k1gInSc1	Hokus
Pokus	pokus	k1gInSc1	pokus
(	(	kIx(	(
<g/>
Hocus	Hocus	k1gMnSc1	Hocus
Pocus	Pocus	k1gMnSc1	Pocus
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Ed	Ed	k1gMnSc1	Ed
Wood	Wood	k1gMnSc1	Wood
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Rapsodie	rapsodie	k1gFnSc2	rapsodie
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
(	(	kIx(	(
<g/>
Miami	Miami	k1gNnSc2	Miami
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Klub	klub	k1gInSc1	klub
odložených	odložený	k2eAgFnPc2d1	odložená
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
The	The	k1gFnPc2	The
First	First	k1gInSc1	First
Wives	Wives	k1gMnSc1	Wives
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Zkouška	zkouška	k1gFnSc1	zkouška
ohněm	oheň	k1gInSc7	oheň
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Substance	substance	k1gFnSc2	substance
of	of	k?	of
Fire	Fir	k1gFnSc2	Fir
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Mars	Mars	k1gInSc1	Mars
útočí	útočit	k5eAaImIp3nS	útočit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
Mars	Mars	k1gInSc1	Mars
Attacks	Attacksa	k1gFnPc2	Attacksa
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Ztraceni	ztracen	k2eAgMnPc1d1	ztracen
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
(	(	kIx(	(
<g/>
If	If	k1gFnSc1	If
Lucy	Luca	k1gFnSc2	Luca
Fell	Fella	k1gFnPc2	Fella
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Smrtící	smrtící	k2eAgFnSc1d1	smrtící
léčba	léčba	k1gFnSc1	léčba
(	(	kIx(	(
<g/>
Extreme	Extrem	k1gInSc5	Extrem
Measures	Measures	k1gMnSc1	Measures
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
Sex	sex	k1gInSc1	sex
and	and	k?	and
the	the	k?	the
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Drsňák	drsňák	k1gMnSc1	drsňák
Dudley	Dudlea	k1gMnSc2	Dudlea
(	(	kIx(	(
<g/>
Dudley	Dudlea	k1gMnSc2	Dudlea
Do-Right	Do-Right	k1gInSc1	Do-Right
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Velké	velký	k2eAgInPc1d1	velký
trable	trabl	k1gInPc1	trabl
malého	malý	k2eAgNnSc2d1	malé
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
State	status	k1gInSc5	status
and	and	k?	and
Main	Maino	k1gNnPc2	Maino
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Zamilovaná	zamilovaná	k1gFnSc1	zamilovaná
do	do	k7c2	do
vraha	vrah	k1gMnSc2	vrah
(	(	kIx(	(
<g/>
Life	Lif	k1gMnSc2	Lif
Without	Without	k1gMnSc1	Without
Dick	Dick	k1gMnSc1	Dick
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
-	-	kIx~	-
Loučení	loučení	k1gNnSc1	loučení
(	(	kIx(	(
<g/>
Sex	sex	k1gInSc1	sex
and	and	k?	and
the	the	k?	the
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
A	a	k9	a
Farewell	Farewell	k1gInSc1	Farewell
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Základ	základ	k1gInSc1	základ
rodiny	rodina	k1gFnSc2	rodina
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Family	Famila	k1gFnSc2	Famila
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Lemra	Lemra	k1gFnSc1	Lemra
líná	líný	k2eAgFnSc1d1	líná
(	(	kIx(	(
<g/>
Failure	Failur	k1gMnSc5	Failur
to	ten	k3xDgNnSc1	ten
Launch	Launch	k1gMnSc1	Launch
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Smart	Smart	k1gInSc1	Smart
People	People	k1gFnSc2	People
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
Sex	sex	k1gInSc1	sex
and	and	k?	and
the	the	k?	the
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Morganovi	morgan	k1gMnSc3	morgan
(	(	kIx(	(
<g/>
Did	Did	k1gMnSc1	Did
You	You	k1gMnSc1	You
Hear	Hear	k1gMnSc1	Hear
About	About	k1gMnSc1	About
the	the	k?	the
Morgans	Morgans	k1gInSc1	Morgans
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
;	;	kIx,	;
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
2	[number]	k4	2
(	(	kIx(	(
<g/>
Sex	sex	k1gInSc1	sex
and	and	k?	and
the	the	k?	the
City	city	k1gNnSc1	city
2	[number]	k4	2
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Nechápu	chápat	k5eNaImIp1nS	chápat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
dokáže	dokázat	k5eAaPmIp3nS	dokázat
(	(	kIx(	(
<g/>
I	i	k8xC	i
don	don	k1gMnSc1	don
<g/>
́	́	k?	́
<g/>
t	t	k?	t
know	know	k?	know
<g/>
,	,	kIx,	,
how	how	k?	how
she	she	k?	she
does	does	k1gInSc1	does
it	it	k?	it
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Sex	sex	k1gInSc1	sex
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Sex	sex	k1gInSc1	sex
and	and	k?	and
the	the	k?	the
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Movie	Movie	k1gFnSc1	Movie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sarah	Sarah	k1gFnSc1	Sarah
Jessica	Jessic	k2eAgFnSc1d1	Jessica
Parker	Parker	k1gInSc4	Parker
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Sarah	Sarah	k1gFnSc1	Sarah
Jessica	Jessica	k1gMnSc1	Jessica
Parker	Parker	k1gMnSc1	Parker
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Sarah	Sarah	k1gFnSc1	Sarah
Jessica	Jessica	k1gMnSc1	Jessica
Parker	Parker	k1gMnSc1	Parker
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
