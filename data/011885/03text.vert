<p>
<s>
Los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gInSc1	alces
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
jelenovitých	jelenovití	k1gMnPc2	jelenovití
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
severních	severní	k2eAgInPc6d1	severní
lesích	les	k1gInPc6	les
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
původním	původní	k2eAgNnSc7d1	původní
zvířetem	zvíře	k1gNnSc7	zvíře
i	i	k9	i
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vyhuben	vyhubit	k5eAaPmNgMnS	vyhubit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
geologicky	geologicky	k6eAd1	geologicky
mladý	mladý	k2eAgInSc4d1	mladý
druh	druh	k1gInSc4	druh
(	(	kIx(	(
<g/>
doklady	doklad	k1gInPc4	doklad
až	až	k9	až
od	od	k7c2	od
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
,	,	kIx,	,
cca	cca	kA	cca
před	před	k7c7	před
80.000	[number]	k4	80.000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vývojového	vývojový	k2eAgNnSc2d1	vývojové
hlediska	hledisko	k1gNnSc2	hledisko
stojí	stát	k5eAaImIp3nS	stát
los	los	k1gInSc1	los
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgMnPc7d1	ostatní
jelenovitými	jelenovití	k1gMnPc7	jelenovití
poměrně	poměrně	k6eAd1	poměrně
osamoceně	osamoceně	k6eAd1	osamoceně
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
chromozomomové	chromozomomový	k2eAgFnSc2d1	chromozomomový
výbavy	výbava	k1gFnSc2	výbava
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc2d1	molekulární
analýzy	analýza	k1gFnSc2	analýza
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc3	jeho
nejbližším	blízký	k2eAgMnPc3d3	nejbližší
příbuzným	příbuzný	k1gMnPc3	příbuzný
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
jelenovitými	jelenovití	k1gMnPc7	jelenovití
je	být	k5eAaImIp3nS	být
překvapivě	překvapivě	k6eAd1	překvapivě
srnec	srnec	k1gMnSc1	srnec
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gMnSc1	alces
alces	alces	k1gMnSc1	alces
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Skandinávie	Skandinávie	k1gFnPc1	Skandinávie
<g/>
,	,	kIx,	,
pobaltské	pobaltský	k2eAgInPc1d1	pobaltský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
severní	severní	k2eAgNnSc1d1	severní
Rusko	Rusko	k1gNnSc1	Rusko
po	po	k7c4	po
Ural	Ural	k1gInSc4	Ural
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
kamčatský	kamčatský	k2eAgInSc1d1	kamčatský
(	(	kIx(	(
<g/>
Alces	Alces	k1gMnSc1	Alces
alces	alces	k1gMnSc1	alces
buturlini	buturlin	k2eAgMnPc1d1	buturlin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východ	východ	k1gInSc4	východ
Sibiře	Sibiř	k1gFnSc2	Sibiř
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
sibiřský	sibiřský	k2eAgInSc1d1	sibiřský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alcesa	k1gFnPc2	alcesa
pfizenmayeri	pfizenmayer	k1gFnSc2	pfizenmayer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
Sibiře	Sibiř	k1gFnSc2	Sibiř
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
amurský	amurský	k2eAgInSc1d1	amurský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gMnSc1	alces
cameloides	cameloides	k1gMnSc1	cameloides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povodí	povodí	k1gNnSc6	povodí
Amuru	Amur	k1gInSc2	Amur
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
Mandžusko	Mandžusko	k1gNnSc1	Mandžusko
a	a	k8xC	a
Korejský	korejský	k2eAgInSc1d1	korejský
poloostrov	poloostrov	k1gInSc1	poloostrov
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
aljašský	aljašský	k2eAgInSc1d1	aljašský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gMnSc1	alces
gigas	gigas	k1gMnSc1	gigas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aljaška	Aljaška	k1gFnSc1	Aljaška
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
východokanadský	východokanadský	k2eAgInSc1d1	východokanadský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gMnSc1	alces
americanus	americanus	k1gMnSc1	americanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
východ	východ	k1gInSc4	východ
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Maine	Main	k1gMnSc5	Main
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
západokanadský	západokanadský	k2eAgInSc1d1	západokanadský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gInSc1	alces
andersoni	andersoň	k1gFnSc3	andersoň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
Kanady	Kanada	k1gFnSc2	Kanada
a	a	k8xC	a
Minnesota	Minnesota	k1gFnSc1	Minnesota
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
yellowstonský	yellowstonský	k2eAgInSc1d1	yellowstonský
(	(	kIx(	(
<g/>
Alces	Alces	k1gInSc1	Alces
alces	alcesa	k1gFnPc2	alcesa
shirasi	shiras	k1gMnPc1	shiras
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severozápad	severozápad	k1gInSc1	severozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
kanadská	kanadský	k2eAgFnSc1d1	kanadská
Alberta	Alberta	k1gFnSc1	Alberta
</s>
</p>
<p>
<s>
==	==	k?	==
Tělesné	tělesný	k2eAgInPc1d1	tělesný
znaky	znak	k1gInPc1	znak
==	==	k?	==
</s>
</p>
<p>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
(	(	kIx(	(
<g/>
býk	býk	k1gMnSc1	býk
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
váha	váha	k1gFnSc1	váha
220	[number]	k4	220
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
kg	kg	kA	kg
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
přes	přes	k7c4	přes
500	[number]	k4	500
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
180	[number]	k4	180
<g/>
–	–	k?	–
<g/>
235	[number]	k4	235
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Losice	losice	k1gFnSc1	losice
<g/>
:	:	kIx,	:
celkově	celkově	k6eAd1	celkově
menší	malý	k2eAgFnPc1d2	menší
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
275	[number]	k4	275
<g/>
–	–	k?	–
<g/>
375	[number]	k4	375
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Losi	los	k1gMnPc1	los
mají	mít	k5eAaImIp3nP	mít
šedohnědou	šedohnědý	k2eAgFnSc4d1	šedohnědá
až	až	k9	až
černou	černý	k2eAgFnSc4d1	černá
srst	srst	k1gFnSc4	srst
<g/>
,	,	kIx,	,
na	na	k7c6	na
krku	krk	k1gInSc2	krk
jim	on	k3xPp3gMnPc3	on
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
hříva	hříva	k1gFnSc1	hříva
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
protáhlá	protáhlý	k2eAgFnSc1d1	protáhlá
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
silným	silný	k2eAgInSc7d1	silný
přečnívajícím	přečnívající	k2eAgInSc7d1	přečnívající
horním	horní	k2eAgInSc7d1	horní
pyskem	pysk	k1gInSc7	pysk
<g/>
.	.	kIx.	.
</s>
<s>
Losi	los	k1gMnPc1	los
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
vybaveni	vybavit	k5eAaPmNgMnP	vybavit
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
měkké	měkký	k2eAgFnSc6d1	měkká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc4	jejich
nohy	noha	k1gFnPc4	noha
vybavené	vybavený	k2eAgFnPc4d1	vybavená
roztažitelnými	roztažitelný	k2eAgInPc7d1	roztažitelný
spárky	spárek	k1gInPc7	spárek
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
našlapovací	našlapovací	k2eAgFnSc4d1	našlapovací
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
i	i	k9	i
rychlostí	rychlost	k1gFnSc7	rychlost
50	[number]	k4	50
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Dobře	dobře	k6eAd1	dobře
plavou	plavat	k5eAaImIp3nP	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Paroží	paroží	k1gNnSc1	paroží
losího	losí	k2eAgMnSc2d1	losí
býka	býk	k1gMnSc2	býk
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
20	[number]	k4	20
kg	kg	kA	kg
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rozpětí	rozpětí	k1gNnSc4	rozpětí
160	[number]	k4	160
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tvarem	tvar	k1gInSc7	tvar
připomíná	připomínat	k5eAaImIp3nS	připomínat
lopaty	lopata	k1gFnPc4	lopata
<g/>
.	.	kIx.	.
</s>
<s>
Losi	los	k1gMnPc1	los
ho	on	k3xPp3gInSc4	on
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
shazují	shazovat	k5eAaImIp3nP	shazovat
<g/>
.	.	kIx.	.
</s>
<s>
Dožívají	dožívat	k5eAaImIp3nP	dožívat
se	se	k3xPyFc4	se
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Dospělosti	dospělost	k1gFnPc4	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
losi	los	k1gMnPc1	los
ve	v	k7c6	v
2,5	[number]	k4	2,5
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Říje	říje	k1gFnSc1	říje
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
se	se	k3xPyFc4	se
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
ozývají	ozývat	k5eAaImIp3nP	ozývat
sténavým	sténavý	k2eAgInSc7d1	sténavý
nebo	nebo	k8xC	nebo
kvílivým	kvílivý	k2eAgInSc7d1	kvílivý
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
jelenovitých	jelenovití	k1gMnPc2	jelenovití
nevytvářejí	vytvářet	k5eNaImIp3nP	vytvářet
harém	harém	k1gInSc4	harém
samic	samice	k1gFnPc2	samice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
páří	pářit	k5eAaImIp3nS	pářit
se	se	k3xPyFc4	se
s	s	k7c7	s
několika	několik	k4yIc7	několik
samicemi	samice	k1gFnPc7	samice
postupně	postupně	k6eAd1	postupně
<g/>
.	.	kIx.	.
</s>
<s>
Losí	losí	k2eAgMnPc1d1	losí
býci	býk	k1gMnPc1	býk
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
říje	říje	k1gFnSc2	říje
velmi	velmi	k6eAd1	velmi
agresivní	agresivní	k2eAgFnPc1d1	agresivní
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
zaútočit	zaútočit	k5eAaPmF	zaútočit
i	i	k9	i
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
automobil	automobil	k1gInSc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
březosti	březost	k1gFnSc2	březost
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
36	[number]	k4	36
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
rodí	rodit	k5eAaImIp3nP	rodit
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
kojí	kojit	k5eAaImIp3nP	kojit
asi	asi	k9	asi
4	[number]	k4	4
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
losi	los	k1gMnPc1	los
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
biotopech	biotop	k1gInPc6	biotop
s	s	k7c7	s
bohatými	bohatý	k2eAgInPc7d1	bohatý
porosty	porost	k1gInPc7	porost
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
listy	list	k1gInPc1	list
bříz	bříza	k1gFnPc2	bříza
<g/>
,	,	kIx,	,
vrb	vrba	k1gFnPc2	vrba
a	a	k8xC	a
jeřábů	jeřáb	k1gInPc2	jeřáb
<g/>
,	,	kIx,	,
vodními	vodní	k2eAgFnPc7d1	vodní
a	a	k8xC	a
bažinnými	bažinný	k2eAgFnPc7d1	bažinná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
sušším	suchý	k2eAgNnPc3d2	sušší
stanovištím	stanoviště	k1gNnPc3	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
žijí	žít	k5eAaImIp3nP	žít
osaměle	osaměle	k6eAd1	osaměle
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
==	==	k?	==
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
už	už	k6eAd1	už
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k9	že
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
běžně	běžně	k6eAd1	běžně
žil	žít	k5eAaImAgInS	žít
<g/>
,	,	kIx,	,
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
i	i	k9	i
názvy	název	k1gInPc1	název
řady	řada	k1gFnSc2	řada
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
např.	např.	kA	např.
u	u	k7c2	u
zubra	zubr	k1gMnSc2	zubr
a	a	k8xC	a
pratura	pratur	k1gMnSc2	pratur
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
losa	los	k1gMnSc2	los
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
Losenice	Losenice	k1gFnSc1	Losenice
<g/>
,	,	kIx,	,
Losiny	Losiny	k1gFnPc1	Losiny
nebo	nebo	k8xC	nebo
Losín	Losín	k1gInSc1	Losín
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
názvů	název	k1gInPc2	název
je	být	k5eAaImIp3nS	být
však	však	k9	však
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
losa	los	k1gMnSc2	los
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
slovo	slovo	k1gNnSc1	slovo
los	los	k1gInSc1	los
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
výskytu	výskyt	k1gInSc2	výskyt
losa	los	k1gMnSc2	los
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
losováním	losování	k1gNnSc7	losování
-	-	kIx~	-
(	(	kIx(	(
<g/>
Velké	velký	k2eAgFnSc2d1	velká
Losiny	losina	k1gFnSc2	losina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
odvozením	odvození	k1gNnSc7	odvození
od	od	k7c2	od
vlastního	vlastní	k2eAgNnSc2d1	vlastní
jména	jméno	k1gNnSc2	jméno
lokátora	lokátor	k1gMnSc2	lokátor
obce	obec	k1gFnSc2	obec
<g/>
,	,	kIx,	,
pána	pán	k1gMnSc4	pán
z	z	k7c2	z
Losu	los	k1gInSc2	los
-	-	kIx~	-
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Losimtál	Losimtál	k1gInSc1	Losimtál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
k	k	k7c3	k
několika	několik	k4yIc3	několik
neúspěšným	úspěšný	k2eNgInPc3d1	neúspěšný
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
reintrodukci	reintrodukce	k1gFnSc4	reintrodukce
losa	los	k1gMnSc2	los
<g/>
,	,	kIx,	,
např.	např.	kA	např.
na	na	k7c4	na
Křivoklátsku	Křivoklátska	k1gFnSc4	Křivoklátska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
Čechách	Čechy	k1gFnPc6	Čechy
zaznamenán	zaznamenán	k2eAgInSc1d1	zaznamenán
výskyt	výskyt	k1gInSc1	výskyt
jedince	jedinec	k1gMnSc2	jedinec
migrujícího	migrující	k2eAgMnSc2d1	migrující
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
u	u	k7c2	u
Nové	Nové	k2eAgFnSc2d1	Nové
Bystřice	Bystřice	k1gFnSc2	Bystřice
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
losi	los	k1gMnPc1	los
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
vyhubení	vyhubení	k1gNnSc2	vyhubení
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
opět	opět	k6eAd1	opět
rozmnožovali	rozmnožovat	k5eAaImAgMnP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
migrovalo	migrovat	k5eAaImAgNnS	migrovat
asi	asi	k9	asi
10	[number]	k4	10
jedinců	jedinec	k1gMnPc2	jedinec
mezi	mezi	k7c7	mezi
Třeboňskem	Třeboňsko	k1gNnSc7	Třeboňsko
a	a	k8xC	a
Šumavou	Šumava	k1gFnSc7	Šumava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
výskyt	výskyt	k1gInSc1	výskyt
losí	losí	k2eAgFnSc2d1	losí
samice	samice	k1gFnSc2	samice
s	s	k7c7	s
mládětem	mládě	k1gNnSc7	mládě
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Bílého	bílý	k2eAgInSc2d1	bílý
Kostela	kostel	k1gInSc2	kostel
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
oblasti	oblast	k1gFnSc6	oblast
Lužických	lužický	k2eAgFnPc2d1	Lužická
hor	hora	k1gFnPc2	hora
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Jitravského	Jitravský	k2eAgNnSc2d1	Jitravský
sedla	sedlo	k1gNnSc2	sedlo
a	a	k8xC	a
Hvozdu	hvozd	k1gInSc2	hvozd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
opětovný	opětovný	k2eAgInSc1d1	opětovný
výskyt	výskyt	k1gInSc1	výskyt
losa	los	k1gMnSc2	los
v	v	k7c6	v
Lužických	lužický	k2eAgFnPc6d1	Lužická
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Jedlovských	Jedlovský	k2eAgInPc2d1	Jedlovský
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
Křížového	křížový	k2eAgInSc2d1	křížový
buku	buk	k1gInSc2	buk
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
samce	samec	k1gMnSc4	samec
s	s	k7c7	s
rozvinutým	rozvinutý	k2eAgNnSc7d1	rozvinuté
parožím	paroží	k1gNnSc7	paroží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
trvale	trvale	k6eAd1	trvale
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Lesní	lesní	k2eAgFnSc2d1	lesní
správy	správa	k1gFnSc2	správa
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Svatotomášsku	Svatotomášsek	k1gInSc6	Svatotomášsek
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
lesní	lesní	k2eAgFnSc6d1	lesní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Trvalá	trvalý	k2eAgFnSc1d1	trvalá
losí	losí	k2eAgFnSc1d1	losí
populace	populace	k1gFnSc1	populace
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
cca	cca	kA	cca
10	[number]	k4	10
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Každoročně	každoročně	k6eAd1	každoročně
jsou	být	k5eAaImIp3nP	být
zřetelná	zřetelný	k2eAgNnPc1d1	zřetelné
pobytová	pobytový	k2eAgNnPc1d1	pobytové
znamení	znamení	k1gNnPc1	znamení
(	(	kIx(	(
<g/>
trus	trus	k1gInSc1	trus
<g/>
,	,	kIx,	,
stopy	stopa	k1gFnPc1	stopa
<g/>
,	,	kIx,	,
škody	škoda	k1gFnPc1	škoda
na	na	k7c4	na
jedli	jedle	k1gFnSc4	jedle
<g/>
,	,	kIx,	,
okus	okus	k1gInSc4	okus
na	na	k7c6	na
jeřábu	jeřáb	k1gInSc6	jeřáb
ptačím	ptačí	k2eAgInSc6d1	ptačí
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
dřevinách	dřevina	k1gFnPc6	dřevina
<g/>
,	,	kIx,	,
poškozené	poškozený	k2eAgFnPc1d1	poškozená
oplocenky	oplocenka	k1gFnPc1	oplocenka
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
losi	los	k1gMnPc1	los
byli	být	k5eAaImAgMnP	být
opakovaně	opakovaně	k6eAd1	opakovaně
fotografováni	fotografován	k2eAgMnPc1d1	fotografován
a	a	k8xC	a
filmováni	filmován	k2eAgMnPc1d1	filmován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
a	a	k8xC	a
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
srážkám	srážka	k1gFnPc3	srážka
aut	aut	k1gInSc4	aut
s	s	k7c7	s
losy	los	k1gInPc7	los
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
(	(	kIx(	(
<g/>
poškozené	poškozený	k2eAgNnSc1d1	poškozené
auto	auto	k1gNnSc1	auto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
los	los	k1gInSc1	los
ze	z	k7c2	z
srážky	srážka	k1gFnSc2	srážka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
vypreparován	vypreparovat	k5eAaPmNgMnS	vypreparovat
v	v	k7c6	v
informačním	informační	k2eAgNnSc6d1	informační
středisku	středisko	k1gNnSc6	středisko
NP	NP	kA	NP
Šumava	Šumava	k1gFnSc1	Šumava
ve	v	k7c6	v
Stožci	Stožce	k1gFnSc6	Stožce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
zaznamenány	zaznamenán	k2eAgFnPc4d1	zaznamenána
3	[number]	k4	3
srážky	srážka	k1gFnPc4	srážka
automobilu	automobil	k1gInSc2	automobil
s	s	k7c7	s
losem	los	k1gInSc7	los
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
spatřen	spatřit	k5eAaPmNgInS	spatřit
a	a	k8xC	a
pozorován	pozorovat	k5eAaImNgInS	pozorovat
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Klec	klec	k1gFnSc1	klec
na	na	k7c6	na
Jindřichohradecku	Jindřichohradecko	k1gNnSc6	Jindřichohradecko
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
Třeboňsku	Třeboňsko	k1gNnSc6	Třeboňsko
<g/>
,	,	kIx,	,
okolí	okolí	k1gNnSc6	okolí
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
,	,	kIx,	,
Veselí	veselí	k1gNnSc1	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
<g/>
,	,	kIx,	,
lesy	les	k1gInPc7	les
k	k	k7c3	k
okolí	okolí	k1gNnSc3	okolí
zámku	zámek	k1gInSc2	zámek
Jemčina	jemčina	k1gFnSc1	jemčina
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
osady	osada	k1gFnSc2	osada
Cikar	Cikar	k1gInSc1	Cikar
a	a	k8xC	a
lesy	les	k1gInPc1	les
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Horusic	Horusice	k1gFnPc2	Horusice
a	a	k8xC	a
břehy	břeh	k1gInPc4	břeh
Horusického	horusický	k2eAgInSc2d1	horusický
rybníka	rybník	k1gInSc2	rybník
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
spatřen	spatřen	k2eAgMnSc1d1	spatřen
jedinec	jedinec	k1gMnSc1	jedinec
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tříletého	tříletý	k2eAgMnSc4d1	tříletý
samce	samec	k1gMnSc4	samec
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
sledovala	sledovat	k5eAaImAgFnS	sledovat
Agentura	agentura	k1gFnSc1	agentura
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
hlášeným	hlášený	k2eAgInPc3d1	hlášený
pozorováním	pozorování	k1gNnSc7	pozorování
mohli	moct	k5eAaImAgMnP	moct
správci	správce	k1gMnPc1	správce
přírody	příroda	k1gFnSc2	příroda
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
celou	celý	k2eAgFnSc4d1	celá
jeho	jeho	k3xOp3gFnSc4	jeho
trasu	trasa	k1gFnSc4	trasa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohyb	pohyb	k1gInSc1	pohyb
byl	být	k5eAaImAgInS	být
sledován	sledovat	k5eAaImNgInS	sledovat
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Drahanovice	Drahanovice	k1gFnSc2	Drahanovice
<g/>
,	,	kIx,	,
Slatinice	Slatinice	k1gFnPc4	Slatinice
<g/>
,	,	kIx,	,
Olšany	Olšany	k1gInPc4	Olšany
přes	přes	k7c4	přes
Větrovany	Větrovan	k1gMnPc4	Větrovan
až	až	k9	až
k	k	k7c3	k
Dolanům	Dolany	k1gInPc3	Dolany
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
migrujícího	migrující	k2eAgMnSc4d1	migrující
jedince	jedinec	k1gMnSc4	jedinec
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
populace	populace	k1gFnSc1	populace
losa	los	k1gMnSc2	los
evropského	evropský	k2eAgInSc2d1	evropský
úspěšně	úspěšně	k6eAd1	úspěšně
žije	žít	k5eAaImIp3nS	žít
už	už	k9	už
40	[number]	k4	40
let	léto	k1gNnPc2	léto
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Lipna	Lipno	k1gNnSc2	Lipno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
doloženo	doložit	k5eAaPmNgNnS	doložit
jeho	jeho	k3xOp3gNnSc1	jeho
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Rožďalovických	Rožďalovický	k2eAgInPc2d1	Rožďalovický
rybníků	rybník	k1gInPc2	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
však	však	k9	však
losi	los	k1gMnPc1	los
ze	z	k7c2	z
zdejší	zdejší	k2eAgFnSc2d1	zdejší
krajiny	krajina	k1gFnSc2	krajina
vymizeli	vymizet	k5eAaPmAgMnP	vymizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Los	los	k1gMnSc1	los
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
==	==	k?	==
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
paleolitu	paleolit	k1gInSc6	paleolit
byl	být	k5eAaImAgInS	být
los	los	k1gInSc1	los
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
lovnou	lovný	k2eAgFnSc7d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
jeskynní	jeskynní	k2eAgFnSc2d1	jeskynní
malby	malba	k1gFnSc2	malba
(	(	kIx(	(
<g/>
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
petroglyfy	petroglyf	k1gInPc1	petroglyf
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hliněné	hliněný	k2eAgFnSc2d1	hliněná
i	i	k8xC	i
kostěné	kostěný	k2eAgFnSc2d1	kostěná
sošky	soška	k1gFnSc2	soška
(	(	kIx(	(
<g/>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
vyobrazení	vyobrazení	k1gNnPc1	vyobrazení
losů	los	k1gMnPc2	los
objevují	objevovat	k5eAaImIp3nP	objevovat
na	na	k7c6	na
skythských	skythský	k2eAgInPc6d1	skythský
špercích	šperk	k1gInPc6	šperk
i	i	k8xC	i
textiliích	textilie	k1gFnPc6	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
nejstarší	starý	k2eAgFnSc2d3	nejstarší
písemné	písemný	k2eAgFnSc2d1	písemná
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
losech	los	k1gInPc6	los
je	být	k5eAaImIp3nS	být
Gaius	Gaius	k1gMnSc1	Gaius
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
Zápiscích	zápisek	k1gInPc6	zápisek
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
galské	galský	k2eAgFnSc2d1	galská
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
los	los	k1gInSc1	los
spává	spávat	k5eAaImIp3nS	spávat
opřený	opřený	k2eAgInSc1d1	opřený
o	o	k7c4	o
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
nohou	noha	k1gFnPc6	noha
klouby	kloub	k1gInPc4	kloub
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
si	se	k3xPyFc3	se
lehnout	lehnout	k5eAaPmF	lehnout
<g/>
.	.	kIx.	.
</s>
<s>
Losa	los	k1gMnSc2	los
prý	prý	k9	prý
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ulovit	ulovit	k5eAaPmF	ulovit
jen	jen	k9	jen
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
lovec	lovec	k1gMnSc1	lovec
nařízli	naříznout	k5eAaPmAgMnP	naříznout
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
o	o	k7c4	o
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
zvířata	zvíře	k1gNnPc1	zvíře
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
opírala	opírat	k5eAaImAgFnS	opírat
<g/>
.	.	kIx.	.
naříznutý	naříznutý	k2eAgInSc1d1	naříznutý
strom	strom	k1gInSc1	strom
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
vahou	váha	k1gFnSc7	váha
losů	los	k1gMnPc2	los
zřítil	zřítit	k5eAaPmAgMnS	zřítit
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
upadla	upadnout	k5eAaPmAgFnS	upadnout
a	a	k8xC	a
protože	protože	k8xS	protože
nemohla	moct	k5eNaImAgFnS	moct
vstát	vstát	k5eAaPmF	vstát
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
je	on	k3xPp3gNnSc4	on
možné	možný	k2eAgNnSc1d1	možné
živá	živý	k2eAgFnSc1d1	živá
chytit	chytit	k5eAaPmF	chytit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Caesara	Caesar	k1gMnSc2	Caesar
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
nesmysl	nesmysl	k1gInSc1	nesmysl
dostal	dostat	k5eAaPmAgInS	dostat
i	i	k9	i
do	do	k7c2	do
středověkých	středověký	k2eAgMnPc2d1	středověký
bestiářů	bestiář	k1gInPc2	bestiář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
los	los	k1gInSc1	los
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
dřívějším	dřívější	k2eAgNnSc6d1	dřívější
rozšíření	rozšíření	k1gNnSc6	rozšíření
snad	snad	k9	snad
svědčí	svědčit	k5eAaImIp3nS	svědčit
některá	některý	k3yIgNnPc4	některý
místní	místní	k2eAgNnPc4d1	místní
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
už	už	k6eAd1	už
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
losi	los	k1gMnPc1	los
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
a	a	k8xC	a
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tyrolský	tyrolský	k2eAgMnSc1d1	tyrolský
se	se	k3xPyFc4	se
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
zpětné	zpětný	k2eAgNnSc1d1	zpětné
vysazení	vysazení	k1gNnSc1	vysazení
na	na	k7c4	na
Křivoklátsku	Křivoklátska	k1gFnSc4	Křivoklátska
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
obdobným	obdobný	k2eAgInPc3d1	obdobný
pokusům	pokus	k1gInPc3	pokus
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
ty	ten	k3xDgMnPc4	ten
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
se	se	k3xPyFc4	se
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
přírody	příroda	k1gFnSc2	příroda
vrátil	vrátit	k5eAaPmAgInS	vrátit
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
Finsku	Finsko	k1gNnSc6	Finsko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
lidé	člověk	k1gMnPc1	člověk
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
domestikací	domestikace	k1gFnSc7	domestikace
losa	los	k1gMnSc2	los
<g/>
.	.	kIx.	.
</s>
<s>
Losi	los	k1gMnPc1	los
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
kůži	kůže	k1gFnSc4	kůže
i	i	k8xC	i
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
připisovaly	připisovat	k5eAaImAgInP	připisovat
léčivé	léčivý	k2eAgInPc1d1	léčivý
účinky	účinek	k1gInPc1	účinek
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Los	los	k1gInSc1	los
se	se	k3xPyFc4	se
však	však	k9	však
neukázal	ukázat	k5eNaPmAgInS	ukázat
být	být	k5eAaImF	být
perspektivních	perspektivní	k2eAgInPc2d1	perspektivní
chovným	chovný	k2eAgNnSc7d1	chovné
zvířetem	zvíře	k1gNnSc7	zvíře
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
svéhlavý	svéhlavý	k2eAgMnSc1d1	svéhlavý
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ho	on	k3xPp3gMnSc4	on
chovat	chovat	k5eAaImF	chovat
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
stádech	stádo	k1gNnPc6	stádo
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
velký	velký	k2eAgInSc4d1	velký
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
specifické	specifický	k2eAgInPc4d1	specifický
potravní	potravní	k2eAgInPc4d1	potravní
nároky	nárok	k1gInPc4	nárok
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
doloženy	doložen	k2eAgInPc1d1	doložen
pokusy	pokus	k1gInPc1	pokus
využít	využít	k5eAaPmF	využít
losa	los	k1gMnSc2	los
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
v	v	k7c6	v
bažinatých	bažinatý	k2eAgFnPc6d1	bažinatá
oblastech	oblast	k1gFnPc6	oblast
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
finského	finský	k2eAgInSc2d1	finský
eposu	epos	k1gInSc2	epos
Kalevala	Kalevala	k1gMnSc1	Kalevala
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
losu	los	k1gInSc6	los
čarodějný	čarodějný	k2eAgMnSc1d1	čarodějný
pěvec	pěvec	k1gMnSc1	pěvec
Väinämöinen	Väinämöinna	k1gFnPc2	Väinämöinna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
se	se	k3xPyFc4	se
však	však	k9	však
losi	los	k1gMnPc1	los
neosvědčili	osvědčit	k5eNaPmAgMnP	osvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
svéhlavosti	svéhlavost	k1gFnSc2	svéhlavost
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k8xC	jako
problém	problém	k1gInSc1	problém
jejich	jejich	k3xOp3gFnSc1	jejich
jemná	jemný	k2eAgFnSc1d1	jemná
kůže	kůže	k1gFnSc1	kůže
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
čenichu	čenich	k1gInSc6	čenich
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
velmi	velmi	k6eAd1	velmi
trpí	trpět	k5eAaImIp3nS	trpět
při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
ohlávkou	ohlávka	k1gFnSc7	ohlávka
a	a	k8xC	a
uzdou	uzda	k1gFnSc7	uzda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
švédský	švédský	k2eAgMnSc1d1	švédský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
XI	XI	kA	XI
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
ruská	ruský	k2eAgFnSc1d1	ruská
carevna	carevna	k1gFnSc1	carevna
Kateřina	Kateřina	k1gFnSc1	Kateřina
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc4	používání
losů	los	k1gMnPc2	los
k	k	k7c3	k
jízdě	jízda	k1gFnSc3	jízda
dokonce	dokonce	k9	dokonce
zakazovali	zakazovat	k5eAaImAgMnP	zakazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
nemohli	moct	k5eNaImAgMnP	moct
používat	používat	k5eAaImF	používat
pašeráci	pašerák	k1gMnPc1	pašerák
a	a	k8xC	a
lupiči	lupič	k1gMnPc1	lupič
<g/>
,	,	kIx,	,
prchající	prchající	k2eAgMnPc1d1	prchající
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
je	být	k5eAaImIp3nS	být
chován	chován	k2eAgInSc1d1	chován
v	v	k7c4	v
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
evropských	evropský	k2eAgFnPc2d1	Evropská
zoo	zoo	k1gFnPc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
přitom	přitom	k6eAd1	přitom
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
losích	losí	k2eAgInPc2d1	losí
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
druh	druh	k1gInSc4	druh
chován	chovat	k5eAaImNgInS	chovat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
zoo	zoo	k1gNnPc6	zoo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
</s>
</p>
<p>
<s>
Zoopark	zoopark	k1gInSc1	zoopark
Chomutov	Chomutov	k1gInSc1	Chomutov
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
los	los	k1gInSc1	los
evropský	evropský	k2eAgInSc1d1	evropský
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
pocházel	pocházet	k5eAaImAgInS	pocházet
ze	z	k7c2	z
zoo	zoo	k1gFnSc2	zoo
ve	v	k7c6	v
finských	finský	k2eAgFnPc6d1	finská
Helsinkách	Helsinky	k1gFnPc6	Helsinky
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
samici	samice	k1gFnSc6	samice
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
až	až	k9	až
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1949	[number]	k4	1949
<g/>
−	−	k?	−
<g/>
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgInPc3d1	rozsáhlý
transportům	transport	k1gInPc3	transport
zvířat	zvíře	k1gNnPc2	zvíře
ze	z	k7c2	z
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
nakrátko	nakrátko	k6eAd1	nakrátko
objevili	objevit	k5eAaPmAgMnP	objevit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
jedinci	jedinec	k1gMnPc1	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
los	los	k1gInSc1	los
se	se	k3xPyFc4	se
v	v	k7c6	v
Zoo	zoo	k1gNnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jej	on	k3xPp3gInSc2	on
odchovat	odchovat	k5eAaPmF	odchovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
odchov	odchov	k1gInSc1	odchov
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
až	až	k9	až
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Etapa	etapa	k1gFnSc1	etapa
dlouhodobých	dlouhodobý	k2eAgInPc2d1	dlouhodobý
úspěchů	úspěch	k1gInPc2	úspěch
v	v	k7c6	v
chovu	chov	k1gInSc6	chov
(	(	kIx(	(
<g/>
a	a	k8xC	a
odchovech	odchov	k1gInPc6	odchov
<g/>
)	)	kIx)	)
přišla	přijít	k5eAaPmAgFnS	přijít
ale	ale	k8xC	ale
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
mládě	mládě	k1gNnSc1	mládě
samici	samice	k1gFnSc3	samice
Šárce	Šárka	k1gFnSc3	Šárka
<g/>
;	;	kIx,	;
otcem	otec	k1gMnSc7	otec
samec	samec	k1gInSc4	samec
Krille	Krille	k1gFnSc2	Krille
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
samec	samec	k1gMnSc1	samec
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc1	čtyři
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
Tierparku	Tierpark	k1gInSc2	Tierpark
Hellabrunn	Hellabrunna	k1gFnPc2	Hellabrunna
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
dovezen	dovezen	k2eAgMnSc1d1	dovezen
nový	nový	k2eAgMnSc1d1	nový
samec	samec	k1gMnSc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
chováni	chovat	k5eAaImNgMnP	chovat
dva	dva	k4xCgMnPc1	dva
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
samec	samec	k1gMnSc1	samec
losa	los	k1gMnSc2	los
<g/>
.	.	kIx.	.
<g/>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
chová	chovat	k5eAaImIp3nS	chovat
losy	los	k1gInPc4	los
v	v	k7c6	v
expozičním	expoziční	k2eAgInSc6d1	expoziční
celku	celek	k1gInSc6	celek
Severský	severský	k2eAgInSc4d1	severský
les	les	k1gInSc4	les
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
zoo	zoo	k1gFnSc2	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
skupina	skupina	k1gFnSc1	skupina
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Chovné	chovný	k2eAgFnSc6d1	chovná
a	a	k8xC	a
aklimatizační	aklimatizační	k2eAgFnSc6d1	aklimatizační
stanici	stanice	k1gFnSc6	stanice
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
v	v	k7c6	v
Dolním	dolní	k2eAgInSc6d1	dolní
Dobřejově	Dobřejův	k2eAgInSc6d1	Dobřejův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Baruš	Baruš	k?	Baruš
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
:	:	kIx,	:
Červená	červený	k2eAgFnSc1d1	červená
kniha	kniha	k1gFnSc1	kniha
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
a	a	k8xC	a
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
.	.	kIx.	.
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Bouchner	Bouchner	k1gMnSc1	Bouchner
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
,	,	kIx,	,
RNDr.	RNDr.	kA	RNDr.
<g/>
:	:	kIx,	:
Kapesní	kapesní	k2eAgInSc4d1	kapesní
atlas	atlas	k1gInSc4	atlas
savců	savec	k1gMnPc2	savec
,	,	kIx,	,
Státní	státní	k2eAgFnSc1d1	státní
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Andreska	Andreska	k1gFnSc1	Andreska
<g/>
,	,	kIx,	,
Ing.	ing.	kA	ing.
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
:	:	kIx,	:
Počátky	počátek	k1gInPc1	počátek
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
populace	populace	k1gFnSc2	populace
losa	los	k1gMnSc2	los
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
Lynx	Lynx	k1gInSc1	Lynx
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
los	los	k1gMnSc1	los
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Alces	Alces	k1gInSc1	Alces
alces	alces	k1gInSc1	alces
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
los	los	k1gMnSc1	los
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
Los	los	k1gInSc4	los
evropský	evropský	k2eAgInSc4d1	evropský
-	-	kIx~	-
vymírající	vymírající	k2eAgMnSc1d1	vymírající
velikán	velikán	k1gMnSc1	velikán
českých	český	k2eAgInPc2d1	český
lesů	les	k1gInPc2	les
</s>
</p>
