<s>
Podprogram	podprogram	k1gInSc1	podprogram
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
subroutine	subroutin	k1gMnSc5	subroutin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
části	část	k1gFnSc2	část
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
při	při	k7c6	při
programování	programování	k1gNnSc6	programování
volat	volat	k5eAaImF	volat
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
i	i	k9	i
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
