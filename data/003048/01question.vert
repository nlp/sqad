<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
v	v	k7c6	v
programování	programování	k1gNnSc6	programování
nazývá	nazývat	k5eAaImIp3nS	nazývat
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
opakovaně	opakovaně	k6eAd1	opakovaně
volat	volat	k5eAaImF	volat
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
kódu	kód	k1gInSc2	kód
<g/>
?	?	kIx.	?
</s>
