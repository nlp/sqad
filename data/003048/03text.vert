<s>
Podprogram	podprogram	k1gInSc1	podprogram
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
subroutine	subroutin	k1gMnSc5	subroutin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
části	část	k1gFnSc2	část
počítačového	počítačový	k2eAgInSc2d1	počítačový
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
při	při	k7c6	při
programování	programování	k1gNnSc6	programování
volat	volat	k5eAaImF	volat
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
(	(	kIx(	(
<g/>
i	i	k9	i
opakovaně	opakovaně	k6eAd1	opakovaně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podprogram	podprogram	k1gInSc1	podprogram
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
procedura	procedura	k1gFnSc1	procedura
nebo	nebo	k8xC	nebo
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
vrací	vracet	k5eAaImIp3nS	vracet
hodnotu	hodnota	k1gFnSc4	hodnota
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
objektovém	objektový	k2eAgNnSc6d1	objektové
programování	programování	k1gNnSc6	programování
je	být	k5eAaImIp3nS	být
podprogram	podprogram	k1gInSc1	podprogram
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
metoda	metoda	k1gFnSc1	metoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
paralelním	paralelní	k2eAgNnSc6d1	paralelní
programování	programování	k1gNnSc6	programování
pak	pak	k6eAd1	pak
koprogram	koprogram	k1gInSc4	koprogram
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
též	též	k9	též
jako	jako	k9	jako
volatelná	volatelný	k2eAgFnSc1d1	volatelná
unita	unita	k1gMnSc1	unita
<g/>
,	,	kIx,	,
subprogram	subprogram	k1gInSc1	subprogram
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Podprogram	podprogram	k1gInSc1	podprogram
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
programování	programování	k1gNnSc6	programování
velmi	velmi	k6eAd1	velmi
mocný	mocný	k2eAgInSc4d1	mocný
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
syntaxe	syntaxe	k1gFnSc1	syntaxe
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
podprogramů	podprogram	k1gInPc2	podprogram
zavedena	zaveden	k2eAgFnSc1d1	zavedena
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
podprogramů	podprogram	k1gInPc2	podprogram
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
údržbu	údržba	k1gFnSc4	údržba
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
jejich	jejich	k3xOp3gFnSc4	jejich
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
spolehlivost	spolehlivost	k1gFnSc4	spolehlivost
<g/>
.	.	kIx.	.
</s>
<s>
Podprogramy	podprogram	k1gInPc1	podprogram
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
sdružovány	sdružován	k2eAgFnPc1d1	sdružována
do	do	k7c2	do
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
oblast	oblast	k1gFnSc4	oblast
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
grafikou	grafika	k1gFnSc7	grafika
<g/>
,	,	kIx,	,
zvukem	zvuk	k1gInSc7	zvuk
<g/>
,	,	kIx,	,
šifrování	šifrování	k1gNnSc6	šifrování
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Knihovny	knihovna	k1gFnPc1	knihovna
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
sdílení	sdílení	k1gNnSc4	sdílení
a	a	k8xC	a
prodej	prodej	k1gInSc4	prodej
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc1d1	orientované
programování	programování	k1gNnSc1	programování
přidružilo	přidružit	k5eAaPmAgNnS	přidružit
podprogramy	podprogram	k1gInPc4	podprogram
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
objektů	objekt	k1gInPc2	objekt
nebo	nebo	k8xC	nebo
tříd	třída	k1gFnPc2	třída
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podprogram	podprogram	k1gInSc1	podprogram
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
parametry	parametr	k1gInPc4	parametr
(	(	kIx(	(
<g/>
také	také	k9	také
označované	označovaný	k2eAgFnPc1d1	označovaná
za	za	k7c4	za
"	"	kIx"	"
<g/>
argumenty	argument	k1gInPc4	argument
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
při	při	k7c6	při
volání	volání	k1gNnSc6	volání
zadávané	zadávaný	k2eAgFnSc2d1	zadávaná
vstupní	vstupní	k2eAgFnSc2d1	vstupní
hodnoty	hodnota	k1gFnSc2	hodnota
podprogramu	podprogram	k1gInSc2	podprogram
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
udávají	udávat	k5eAaImIp3nP	udávat
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakými	jaký	k3yRgFnPc7	jaký
hodnotami	hodnota	k1gFnPc7	hodnota
má	mít	k5eAaImIp3nS	mít
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Podprogram	podprogram	k1gInSc1	podprogram
může	moct	k5eAaImIp3nS	moct
vracet	vracet	k5eAaImF	vracet
návratovou	návratový	k2eAgFnSc4d1	návratová
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
členění	členění	k1gNnSc4	členění
programu	program	k1gInSc2	program
na	na	k7c4	na
podprogramy	podprogram	k1gInPc4	podprogram
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
<g/>
:	:	kIx,	:
rozklad	rozklad	k1gInSc1	rozklad
složitých	složitý	k2eAgInPc2d1	složitý
problémů	problém	k1gInPc2	problém
na	na	k7c4	na
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
rekurze	rekurze	k1gFnSc2	rekurze
menší	malý	k2eAgNnSc1d2	menší
odstranění	odstranění	k1gNnSc1	odstranění
opakování	opakování	k1gNnSc2	opakování
kódu	kód	k1gInSc2	kód
v	v	k7c6	v
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
a	a	k8xC	a
díky	díky	k7c3	díky
parametrům	parametr	k1gInPc3	parametr
jeho	on	k3xPp3gNnSc2	on
zobecnění	zobecnění	k1gNnSc1	zobecnění
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
znovupoužití	znovupoužití	k1gNnSc4	znovupoužití
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
programech	program	k1gInPc6	program
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
formou	forma	k1gFnSc7	forma
modulů	modul	k1gInPc2	modul
nebo	nebo	k8xC	nebo
knihoven	knihovna	k1gFnPc2	knihovna
rozvržení	rozvržení	k1gNnSc4	rozvržení
projektu	projekt	k1gInSc2	projekt
mezi	mezi	k7c4	mezi
více	hodně	k6eAd2	hodně
programátorů	programátor	k1gInPc2	programátor
odstínění	odstínění	k1gNnSc2	odstínění
detailů	detail	k1gInPc2	detail
implementace	implementace	k1gFnSc2	implementace
od	od	k7c2	od
konkrétního	konkrétní	k2eAgNnSc2d1	konkrétní
použití	použití	k1gNnSc2	použití
funkce	funkce	k1gFnSc2	funkce
Některé	některý	k3yIgInPc1	některý
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
Pascal	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
Fortran	Fortran	kA	Fortran
<g/>
,	,	kIx,	,
Ada	Ada	kA	Ada
<g/>
)	)	kIx)	)
striktně	striktně	k6eAd1	striktně
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
podprogramů	podprogram	k1gInPc2	podprogram
-	-	kIx~	-
funkce	funkce	k1gFnPc1	funkce
a	a	k8xC	a
procedury	procedura	k1gFnPc1	procedura
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
Lisp	Lisp	k1gInSc1	Lisp
<g/>
)	)	kIx)	)
takto	takto	k6eAd1	takto
striktně	striktně	k6eAd1	striktně
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
podprogramů	podprogram	k1gInPc2	podprogram
nerozlišují	rozlišovat	k5eNaImIp3nP	rozlišovat
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
buď	buď	k8xC	buď
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
názvů	název	k1gInPc2	název
nebo	nebo	k8xC	nebo
je	on	k3xPp3gInPc4	on
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
synonyma	synonymum	k1gNnPc4	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
skupiny	skupina	k1gFnPc4	skupina
podprogramů	podprogram	k1gInPc2	podprogram
<g/>
:	:	kIx,	:
procedura	procedura	k1gFnSc1	procedura
-	-	kIx~	-
podprogram	podprogram	k1gInSc1	podprogram
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nevrací	vracet	k5eNaImIp3nS	vracet
výslednou	výsledný	k2eAgFnSc4d1	výsledná
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
volá	volat	k5eAaImIp3nS	volat
se	se	k3xPyFc4	se
jako	jako	k9	jako
příkaz	příkaz	k1gInSc1	příkaz
<g/>
,	,	kIx,	,
funkce	funkce	k1gFnSc1	funkce
-	-	kIx~	-
podprogram	podprogram	k1gInSc1	podprogram
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vrací	vracet	k5eAaImIp3nS	vracet
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
volat	volat	k5eAaImF	volat
ve	v	k7c6	v
výrazu	výraz	k1gInSc6	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
a	a	k8xC	a
chování	chování	k1gNnSc1	chování
programové	programový	k2eAgFnSc2d1	programová
funkce	funkce	k1gFnSc2	funkce
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
funkce	funkce	k1gFnSc2	funkce
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
projevech	projev	k1gInPc6	projev
<g/>
:	:	kIx,	:
nemusí	muset	k5eNaImIp3nP	muset
záviset	záviset	k5eAaImF	záviset
jen	jen	k9	jen
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
parametrech	parametr	k1gInPc6	parametr
a	a	k8xC	a
při	při	k7c6	při
volání	volání	k1gNnSc6	volání
se	s	k7c7	s
stejnými	stejný	k2eAgInPc7d1	stejný
parametry	parametr	k1gInPc7	parametr
může	moct	k5eAaImIp3nS	moct
podprogram	podprogram	k1gInSc1	podprogram
vracet	vracet	k5eAaImF	vracet
jiné	jiný	k2eAgFnPc4d1	jiná
návratové	návratový	k2eAgFnPc4d1	návratová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
provádění	provádění	k1gNnSc6	provádění
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	s	k7c7	s
zadávanými	zadávaný	k2eAgInPc7d1	zadávaný
parametry	parametr	k1gInPc7	parametr
a	a	k8xC	a
provádět	provádět	k5eAaImF	provádět
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
vstupní	vstupní	k2eAgInPc4d1	vstupní
nebo	nebo	k8xC	nebo
výstupní	výstupní	k2eAgFnPc4d1	výstupní
operace	operace	k1gFnPc4	operace
<g/>
;	;	kIx,	;
nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgInPc2	takový
vstupů	vstup	k1gInPc2	vstup
jsou	být	k5eAaImIp3nP	být
změřené	změřený	k2eAgFnPc1d1	změřená
hodnoty	hodnota	k1gFnPc1	hodnota
nebo	nebo	k8xC	nebo
čas	čas	k1gInSc1	čas
počítače	počítač	k1gInSc2	počítač
obdobně	obdobně	k6eAd1	obdobně
návratová	návratový	k2eAgFnSc1d1	návratová
hodnota	hodnota	k1gFnSc1	hodnota
zdaleka	zdaleka	k6eAd1	zdaleka
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
jeho	jeho	k3xOp3gInSc7	jeho
jediným	jediný	k2eAgInSc7d1	jediný
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
účinek	účinek	k1gInSc1	účinek
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
než	než	k8xS	než
jen	jen	k9	jen
vrácené	vrácený	k2eAgInPc1d1	vrácený
do	do	k7c2	do
výrazu	výraz	k1gInSc2	výraz
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byla	být	k5eAaImAgFnS	být
funkce	funkce	k1gFnSc1	funkce
volána	volán	k2eAgFnSc1d1	volána
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
hodnoty	hodnota	k1gFnSc2	hodnota
ovlivňující	ovlivňující	k2eAgInSc4d1	ovlivňující
vzhled	vzhled	k1gInSc4	vzhled
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Parametr	parametr	k1gInSc1	parametr
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
parametry	parametr	k1gInPc7	parametr
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c4	v
definici	definice	k1gFnSc4	definice
nebo	nebo	k8xC	nebo
volání	volání	k1gNnSc4	volání
podprogramu	podprogram	k1gInSc2	podprogram
<g/>
,	,	kIx,	,
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
<g/>
:	:	kIx,	:
formální	formální	k2eAgInPc1d1	formální
parametry	parametr	k1gInPc1	parametr
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
parametry	parametr	k1gInPc1	parametr
použité	použitý	k2eAgInPc1d1	použitý
v	v	k7c4	v
definici	definice	k1gFnSc4	definice
podprogramu	podprogram	k1gInSc2	podprogram
<g/>
;	;	kIx,	;
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
specifikovat	specifikovat	k5eAaBmF	specifikovat
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgInSc2	jaký
datového	datový	k2eAgInSc2d1	datový
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
parametry	parametr	k1gInPc1	parametr
podprogramu	podprogram	k1gInSc2	podprogram
<g/>
,	,	kIx,	,
a	a	k8xC	a
jaké	jaký	k3yQgFnPc1	jaký
operace	operace	k1gFnPc1	operace
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
uvnitř	uvnitř	k7c2	uvnitř
podprogramu	podprogram	k1gInSc2	podprogram
<g />
.	.	kIx.	.
</s>
<s>
provádějí	provádět	k5eAaImIp3nP	provádět
skutečné	skutečný	k2eAgInPc4d1	skutečný
parametry	parametr	k1gInPc4	parametr
(	(	kIx(	(
<g/>
argumenty	argument	k1gInPc4	argument
<g/>
)	)	kIx)	)
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
parametry	parametr	k1gInPc1	parametr
(	(	kIx(	(
<g/>
výrazy	výraz	k1gInPc1	výraz
nebo	nebo	k8xC	nebo
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
)	)	kIx)	)
použité	použitý	k2eAgNnSc1d1	Použité
ve	v	k7c6	v
volání	volání	k1gNnSc6	volání
funkce	funkce	k1gFnSc1	funkce
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
programovací	programovací	k2eAgInPc1d1	programovací
jazyky	jazyk	k1gInPc1	jazyk
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
funkce	funkce	k1gFnPc4	funkce
s	s	k7c7	s
proměnným	proměnný	k2eAgInSc7d1	proměnný
počtem	počet	k1gInSc7	počet
parametrů	parametr	k1gInPc2	parametr
Skutečné	skutečný	k2eAgInPc1d1	skutečný
parametry	parametr	k1gInPc1	parametr
se	se	k3xPyFc4	se
zpracují	zpracovat	k5eAaPmIp3nP	zpracovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dále	daleko	k6eAd2	daleko
<g/>
)	)	kIx)	)
a	a	k8xC	a
přiřadí	přiřadit	k5eAaPmIp3nP	přiřadit
nebo	nebo	k8xC	nebo
navážou	navázat	k5eAaPmIp3nP	navázat
na	na	k7c4	na
formální	formální	k2eAgInPc4d1	formální
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
tělo	tělo	k1gNnSc4	tělo
podprogramu	podprogram	k1gInSc2	podprogram
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vztahu	vztah	k1gInSc2	vztah
formálního	formální	k2eAgInSc2d1	formální
a	a	k8xC	a
skutečného	skutečný	k2eAgInSc2d1	skutečný
parametru	parametr	k1gInSc2	parametr
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
různé	různý	k2eAgFnPc1d1	různá
metody	metoda	k1gFnPc1	metoda
předávání	předávání	k1gNnSc2	předávání
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
volání	volání	k1gNnSc1	volání
<g/>
)	)	kIx)	)
parametrů	parametr	k1gInPc2	parametr
<g/>
:	:	kIx,	:
volání	volání	k1gNnSc2	volání
hodnotou	hodnota	k1gFnSc7	hodnota
(	(	kIx(	(
<g/>
call	calla	k1gFnPc2	calla
by	by	kYmCp3nS	by
value	value	k6eAd1	value
<g/>
)	)	kIx)	)
volající	volající	k2eAgMnSc1d1	volající
provede	provést	k5eAaPmIp3nS	provést
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
výrazu	výraz	k1gInSc2	výraz
zadaného	zadaný	k2eAgNnSc2d1	zadané
jako	jako	k8xC	jako
argument	argument	k1gInSc1	argument
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
výslednou	výsledný	k2eAgFnSc4d1	výsledná
hodnotu	hodnota	k1gFnSc4	hodnota
předá	předat	k5eAaPmIp3nS	předat
příslušnému	příslušný	k2eAgInSc3d1	příslušný
formálnímu	formální	k2eAgInSc3d1	formální
parametru	parametr	k1gInSc3	parametr
volání	volání	k1gNnSc2	volání
odkazem	odkaz	k1gInSc7	odkaz
(	(	kIx(	(
<g/>
call	call	k1gInSc1	call
by	by	kYmCp3nP	by
reference	reference	k1gFnSc1	reference
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
volající	volající	k2eAgMnSc1d1	volající
předá	předat	k5eAaPmIp3nS	předat
v	v	k7c6	v
argumentu	argument	k1gInSc6	argument
funkce	funkce	k1gFnSc2	funkce
ukazatel	ukazatel	k1gInSc4	ukazatel
nebo	nebo	k8xC	nebo
referenci	reference	k1gFnSc4	reference
na	na	k7c4	na
proměnou	proměna	k1gFnSc7	proměna
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
může	moct	k5eAaImIp3nS	moct
obsah	obsah	k1gInSc4	obsah
této	tento	k3xDgFnSc2	tento
proměnné	proměnná	k1gFnSc2	proměnná
nejen	nejen	k6eAd1	nejen
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
modifikovat	modifikovat	k5eAaBmF	modifikovat
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinek	účinek	k1gInSc4	účinek
<g/>
)	)	kIx)	)
volání	volání	k1gNnSc2	volání
výsledkem	výsledek	k1gInSc7	výsledek
(	(	kIx(	(
<g/>
call	calnout	k5eAaPmAgInS	calnout
by	by	kYmCp3nS	by
result	result	k5eAaPmF	result
<g/>
)	)	kIx)	)
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
má	mít	k5eAaImIp3nS	mít
proměnná	proměnný	k2eAgFnSc1d1	proměnná
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
formálnímu	formální	k2eAgMnSc3d1	formální
<g />
.	.	kIx.	.
</s>
<s>
parametru	parametr	k1gInSc2	parametr
nedefinovanou	definovaný	k2eNgFnSc4d1	nedefinovaná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
;	;	kIx,	;
funkce	funkce	k1gFnSc1	funkce
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
lokální	lokální	k2eAgFnSc4d1	lokální
proměnnou	proměnná	k1gFnSc4	proměnná
<g/>
;	;	kIx,	;
při	při	k7c6	při
skončení	skončení	k1gNnSc6	skončení
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
hodnota	hodnota	k1gFnSc1	hodnota
předá	předat	k5eAaPmIp3nS	předat
skutečnému	skutečný	k2eAgInSc3d1	skutečný
parametru	parametr	k1gInSc3	parametr
volání	volání	k1gNnSc2	volání
hodnotou	hodnota	k1gFnSc7	hodnota
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
(	(	kIx(	(
<g/>
call	calla	k1gFnPc2	calla
by	by	kYmCp3nS	by
value	value	k6eAd1	value
and	and	k?	and
result	result	k1gInSc1	result
<g/>
)	)	kIx)	)
formální	formální	k2eAgInSc1d1	formální
parametr	parametr	k1gInSc1	parametr
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
jako	jako	k9	jako
lokální	lokální	k2eAgFnSc1d1	lokální
proměnná	proměnná	k1gFnSc1	proměnná
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
<g />
.	.	kIx.	.
</s>
<s>
funkce	funkce	k1gFnSc1	funkce
předá	předat	k5eAaPmIp3nS	předat
hodnota	hodnota	k1gFnSc1	hodnota
argumentu	argument	k1gInSc2	argument
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
při	při	k7c6	při
výstupu	výstup	k1gInSc6	výstup
se	se	k3xPyFc4	se
předá	předat	k5eAaPmIp3nS	předat
hodnota	hodnota	k1gFnSc1	hodnota
zpátky	zpátky	k6eAd1	zpátky
<g/>
;	;	kIx,	;
chování	chování	k1gNnSc1	chování
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc1d1	podobné
jako	jako	k8xC	jako
u	u	k7c2	u
volání	volání	k1gNnSc2	volání
odkazem	odkaz	k1gInSc7	odkaz
<g/>
;	;	kIx,	;
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
funkce	funkce	k1gFnSc1	funkce
s	s	k7c7	s
proměnnou	proměnná	k1gFnSc7	proměnná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
skutečný	skutečný	k2eAgInSc1d1	skutečný
parametr	parametr	k1gInSc1	parametr
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
i	i	k9	i
přímo	přímo	k6eAd1	přímo
volání	volání	k1gNnSc1	volání
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
call	calnout	k5eAaPmAgInS	calnout
by	by	kYmCp3nS	by
name	name	k1gInSc1	name
<g/>
)	)	kIx)	)
výraz	výraz	k1gInSc1	výraz
v	v	k7c6	v
argumentu	argument	k1gInSc6	argument
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
nevyhodnocuje	vyhodnocovat	k5eNaImIp3nS	vyhodnocovat
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
se	se	k3xPyFc4	se
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
tak	tak	k6eAd1	tak
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc1	tento
argument	argument	k1gInSc1	argument
vyhodnocovat	vyhodnocovat	k5eAaImF	vyhodnocovat
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
;	;	kIx,	;
v	v	k7c6	v
kompilovaných	kompilovaný	k2eAgInPc6d1	kompilovaný
jazycích	jazyk	k1gInPc6	jazyk
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
anachronismus	anachronismus	k1gInSc4	anachronismus
<g/>
;	;	kIx,	;
nejčastější	častý	k2eAgNnSc4d3	nejčastější
použití	použití	k1gNnSc4	použití
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
symbolických	symbolický	k2eAgNnPc2d1	symbolické
maker	makro	k1gNnPc2	makro
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
funkcionálních	funkcionální	k2eAgInPc6d1	funkcionální
jazycích	jazyk	k1gInPc6	jazyk
Většina	většina	k1gFnSc1	většina
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
má	mít	k5eAaImIp3nS	mít
jenom	jenom	k9	jenom
část	část	k1gFnSc4	část
z	z	k7c2	z
uvedených	uvedený	k2eAgFnPc2d1	uvedená
metod	metoda	k1gFnPc2	metoda
volání	volání	k1gNnSc2	volání
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
volání	volání	k1gNnSc1	volání
hodnotou	hodnota	k1gFnSc7	hodnota
plus	plus	k1gNnSc2	plus
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
volání	volání	k1gNnSc4	volání
odkazem	odkaz	k1gInSc7	odkaz
<g/>
,	,	kIx,	,
volání	volání	k1gNnSc4	volání
hodnotou	hodnota	k1gFnSc7	hodnota
a	a	k8xC	a
výsledkem	výsledek	k1gInSc7	výsledek
a	a	k8xC	a
volání	volání	k1gNnSc1	volání
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
definici	definice	k1gFnSc6	definice
funkce	funkce	k1gFnSc2	funkce
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc1	jaký
způsob	způsob	k1gInSc1	způsob
volání	volání	k1gNnSc2	volání
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
parametr	parametr	k1gInSc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
lze	lze	k6eAd1	lze
vybrat	vybrat	k5eAaPmF	vybrat
jen	jen	k9	jen
některé	některý	k3yIgFnPc4	některý
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pole	pole	k1gNnSc2	pole
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
předává	předávat	k5eAaImIp3nS	předávat
odkazem	odkaz	k1gInSc7	odkaz
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rekurzivní	rekurzivní	k2eAgFnSc2d1	rekurzivní
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
programování	programování	k1gNnSc2	programování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
nazveme	nazvat	k5eAaPmIp1nP	nazvat
rekurzivní	rekurzivní	k2eAgInPc1d1	rekurzivní
pokud	pokud	k8xS	pokud
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
těle	tělo	k1gNnSc6	tělo
zavoláme	zavolat	k5eAaPmIp1nP	zavolat
tutéž	týž	k3xTgFnSc4	týž
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
funkce	funkce	k1gFnSc1	funkce
volá	volat	k5eAaImIp3nS	volat
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zacyklená	zacyklený	k2eAgFnSc1d1	zacyklená
<g/>
.	.	kIx.	.
</s>
<s>
Rekurzivní	rekurzivní	k2eAgFnSc1d1	rekurzivní
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
v	v	k7c6	v
programování	programování	k1gNnSc6	programování
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
složitějších	složitý	k2eAgInPc2d2	složitější
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávné	správný	k2eNgNnSc1d1	nesprávné
užití	užití	k1gNnSc1	užití
rekurze	rekurze	k1gFnSc2	rekurze
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
velkou	velký	k2eAgFnSc4d1	velká
spotřebu	spotřeba	k1gFnSc4	spotřeba
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
velkou	velký	k2eAgFnSc4d1	velká
spotřebu	spotřeba	k1gFnSc4	spotřeba
času	čas	k1gInSc2	čas
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
použití	použití	k1gNnSc2	použití
rekurzivní	rekurzivní	k2eAgFnSc2d1	rekurzivní
funkce	funkce	k1gFnSc2	funkce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
algoritmus	algoritmus	k1gInSc1	algoritmus
výpočtu	výpočet	k1gInSc2	výpočet
faktoriálu	faktoriál	k1gInSc2	faktoriál
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Přetížení	přetížení	k1gNnSc2	přetížení
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Přetížení	přetížení	k1gNnSc1	přetížení
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
overloading	overloading	k1gInSc1	overloading
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
deklarovat	deklarovat	k5eAaBmF	deklarovat
více	hodně	k6eAd2	hodně
funkcí	funkce	k1gFnPc2	funkce
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
lišících	lišící	k2eAgFnPc2d1	lišící
se	se	k3xPyFc4	se
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
seznamu	seznam	k1gInSc2	seznam
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
<g/>
,	,	kIx,	,
datový	datový	k2eAgInSc1d1	datový
typ	typ	k1gInSc1	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volání	volání	k1gNnSc6	volání
funkce	funkce	k1gFnSc2	funkce
překladač	překladač	k1gInSc1	překladač
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
parametry	parametr	k1gInPc4	parametr
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
určí	určit	k5eAaPmIp3nS	určit
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Přetížení	přetížení	k1gNnSc1	přetížení
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
i	i	k9	i
návratové	návratový	k2eAgFnPc4d1	návratová
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
,	,	kIx,	,
překladač	překladač	k1gInSc1	překladač
analyzuje	analyzovat	k5eAaImIp3nS	analyzovat
typ	typ	k1gInSc4	typ
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
návratové	návratový	k2eAgFnSc2d1	návratová
hodnoty	hodnota	k1gFnSc2	hodnota
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
přiřazovacího	přiřazovací	k2eAgInSc2d1	přiřazovací
operátoru	operátor	k1gInSc2	operátor
=	=	kIx~	=
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
vybere	vybrat	k5eAaPmIp3nS	vybrat
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
všechny	všechen	k3xTgInPc4	všechen
podprogramy	podprogram	k1gInPc4	podprogram
nazývá	nazývat	k5eAaImIp3nS	nazývat
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkce	funkce	k1gFnSc1	funkce
nevrací	vracet	k5eNaImIp3nS	vracet
žádnou	žádný	k3yNgFnSc4	žádný
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
parametry	parametr	k1gInPc4	parametr
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
klíčové	klíčový	k2eAgNnSc1d1	klíčové
slovo	slovo	k1gNnSc1	slovo
void	void	k1gInSc1	void
<g/>
:	:	kIx,	:
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
nevrací	vracet	k5eNaImIp3nS	vracet
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
volána	volán	k2eAgFnSc1d1	volána
<g/>
:	:	kIx,	:
funkce	funkce	k1gFnSc1	funkce
<g/>
1	[number]	k4	1
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
navrací	navracet	k5eAaBmIp3nS	navracet
hodnotu	hodnota	k1gFnSc4	hodnota
(	(	kIx(	(
<g/>
číslo	číslo	k1gNnSc4	číslo
5	[number]	k4	5
<g/>
)	)	kIx)	)
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
můžeme	moct	k5eAaImIp1nP	moct
zavolat	zavolat	k5eAaPmF	zavolat
jako	jako	k9	jako
část	část	k1gFnSc4	část
příkazu	příkaz	k1gInSc2	příkaz
<g/>
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
funkce	funkce	k1gFnSc1	funkce
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
)	)	kIx)	)
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
konvertuje	konvertovat	k5eAaBmIp3nS	konvertovat
číslo	číslo	k1gNnSc4	číslo
mezi	mezi	k7c7	mezi
0	[number]	k4	0
a	a	k8xC	a
6	[number]	k4	6
na	na	k7c4	na
počáteční	počáteční	k2eAgNnSc4d1	počáteční
písmeno	písmeno	k1gNnSc4	písmeno
dne	den	k1gInSc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
<g/>
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
'	'	kIx"	'
<g/>
P	P	kA	P
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
1	[number]	k4	1
→	→	k?	→
'	'	kIx"	'
<g/>
U	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
6	[number]	k4	6
→	→	k?	→
'	'	kIx"	'
<g/>
N	N	kA	N
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
volání	volání	k1gNnSc1	volání
funkce	funkce	k1gFnSc2	funkce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přiřazen	přiřazen	k2eAgInSc1d1	přiřazen
proměnné	proměnná	k1gFnSc3	proměnná
<g/>
:	:	kIx,	:
char	char	k1gInSc1	char
pismeno_dne	pismeno_dnout	k5eAaPmIp3nS	pismeno_dnout
=	=	kIx~	=
funkce	funkce	k1gFnSc1	funkce
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
cislo	cislo	k1gNnSc1	cislo
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
nevrací	vracet	k5eNaImIp3nS	vracet
žádnou	žádný	k3yNgFnSc4	žádný
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
modifikuje	modifikovat	k5eAaBmIp3nS	modifikovat
proměnnou	proměnná	k1gFnSc4	proměnná
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
adresa	adresa	k1gFnSc1	adresa
je	být	k5eAaImIp3nS	být
zadána	zadat	k5eAaPmNgFnS	zadat
v	v	k7c6	v
parametru	parametr	k1gInSc6	parametr
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
lze	lze	k6eAd1	lze
volat	volat	k5eAaImF	volat
<g/>
:	:	kIx,	:
funkce	funkce	k1gFnSc1	funkce
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
&	&	k?	&
<g/>
promenna	promenna	k1gFnSc1	promenna
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řádek	řádek	k1gInSc1	řádek
deklaruje	deklarovat	k5eAaBmIp3nS	deklarovat
funkci	funkce	k1gFnSc4	funkce
funkce	funkce	k1gFnSc2	funkce
<g/>
5	[number]	k4	5
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
predeklaraci	predeklarace	k1gFnSc4	predeklarace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vrací	vracet	k5eAaImIp3nS	vracet
ukazatel	ukazatel	k1gInSc4	ukazatel
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
vracející	vracející	k2eAgMnSc1d1	vracející
integer	integer	k1gMnSc1	integer
a	a	k8xC	a
mající	mající	k2eAgInSc1d1	mající
parametr	parametr	k1gInSc1	parametr
typu	typ	k1gInSc2	typ
nespecifikovaný	specifikovaný	k2eNgInSc1d1	nespecifikovaný
ukazatel	ukazatel	k1gInSc1	ukazatel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
parametry	parametr	k1gInPc4	parametr
<g/>
:	:	kIx,	:
první	první	k4xOgInSc1	první
je	být	k5eAaImIp3nS	být
ukazatel	ukazatel	k1gInSc1	ukazatel
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
vracející	vracející	k2eAgInSc1d1	vracející
double	double	k1gInSc1	double
(	(	kIx(	(
<g/>
reálné	reálný	k2eAgNnSc1d1	reálné
<g />
.	.	kIx.	.
</s>
<s>
číslo	číslo	k1gNnSc1	číslo
s	s	k7c7	s
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
přesností	přesnost	k1gFnSc7	přesnost
<g/>
)	)	kIx)	)
s	s	k7c7	s
parametrem	parametr	k1gInSc7	parametr
typu	typ	k1gInSc2	typ
long	longa	k1gFnPc2	longa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
<g/>
"	"	kIx"	"
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
ukazatel	ukazatel	k1gInSc4	ukazatel
na	na	k7c4	na
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
stejného	stejný	k2eAgInSc2d1	stejný
typu	typ	k1gInSc2	typ
jako	jako	k8xS	jako
funkce	funkce	k1gFnSc1	funkce
vracená	vracený	k2eAgFnSc1d1	vracená
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vracející	vracející	k2eAgInSc1d1	vracející
integer	integer	k1gInSc1	integer
a	a	k8xC	a
mající	mající	k2eAgInSc1d1	mající
parametr	parametr	k1gInSc1	parametr
typu	typ	k1gInSc2	typ
nespecifikovaný	specifikovaný	k2eNgInSc1d1	nespecifikovaný
ukazatel	ukazatel	k1gInSc1	ukazatel
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
zápisy	zápis	k1gInPc1	zápis
se	se	k3xPyFc4	se
v	v	k7c6	v
běžných	běžný	k2eAgInPc6d1	běžný
programech	program	k1gInPc6	program
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zřídka	zřídka	k6eAd1	zřídka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bývají	bývat	k5eAaImIp3nP	bývat
zpřehledněny	zpřehlednit	k5eAaPmNgInP	zpřehlednit
pomocí	pomoc	k1gFnSc7	pomoc
typedef	typedef	k1gInSc4	typedef
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
v	v	k7c6	v
chybové	chybový	k2eAgFnSc6d1	chybová
hlášce	hláška	k1gFnSc6	hláška
překladače	překladač	k1gInSc2	překladač
nebo	nebo	k8xC	nebo
v	v	k7c6	v
automaticky	automaticky	k6eAd1	automaticky
generovaném	generovaný	k2eAgInSc6d1	generovaný
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
kódu	kód	k1gInSc6	kód
<g/>
.	.	kIx.	.
</s>
<s>
Ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
definice	definice	k1gFnSc1	definice
pomocí	pomoc	k1gFnPc2	pomoc
typedef	typedef	k1gInSc4	typedef
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
V	v	k7c6	v
netypovaných	typovaný	k2eNgInPc6d1	typovaný
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
jako	jako	k8xS	jako
PHP	PHP	kA	PHP
je	být	k5eAaImIp3nS	být
zápis	zápis	k1gInSc4	zápis
funkce	funkce	k1gFnSc2	funkce
jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
<g/>
:	:	kIx,	:
neuvádí	uvádět	k5eNaImIp3nS	uvádět
se	se	k3xPyFc4	se
typy	typ	k1gInPc1	typ
argumentů	argument	k1gInPc2	argument
<g/>
,	,	kIx,	,
jen	jen	k9	jen
názvy	název	k1gInPc1	název
<g/>
:	:	kIx,	:
Funkce	funkce	k1gFnSc1	funkce
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
její	její	k3xOp3gInSc1	její
argumenty	argument	k1gInPc1	argument
jsou	být	k5eAaImIp3nP	být
asociativní	asociativní	k2eAgNnPc4d1	asociativní
pole	pole	k1gNnPc4	pole
obsahující	obsahující	k2eAgInSc4d1	obsahující
prvek	prvek	k1gInSc4	prvek
num	num	k?	num
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
rozdíl	rozdíl	k1gInSc1	rozdíl
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
funkce	funkce	k1gFnPc1	funkce
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
callback	callback	k1gInSc4	callback
pro	pro	k7c4	pro
řazení	řazení	k1gNnSc4	řazení
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
předají	předat	k5eAaPmIp3nP	předat
se	se	k3xPyFc4	se
jako	jako	k9	jako
argument	argument	k1gInSc1	argument
řadící	řadící	k2eAgFnSc4d1	řadící
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
opakovaně	opakovaně	k6eAd1	opakovaně
volá	volat	k5eAaImIp3nS	volat
na	na	k7c4	na
dvojice	dvojice	k1gFnPc4	dvojice
prvků	prvek	k1gInPc2	prvek
řazeného	řazený	k2eAgNnSc2d1	řazené
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
