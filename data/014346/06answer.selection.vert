<s>
Elektroencefalografie	Elektroencefalografie	k1gFnSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
EEG	EEG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
metoda	metoda	k1gFnSc1
záznamu	záznam	k1gInSc2
časové	časový	k2eAgFnSc2d1
změny	změna	k1gFnSc2
elektrického	elektrický	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
způsobeného	způsobený	k2eAgInSc2d1
mozkovou	mozkový	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
<g/>
.	.	kIx.
</s>