<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
zkráceně	zkráceně	k6eAd1
píše	psát	k5eAaImIp3nS
elektroencefalografie	elektroencefalografie	k1gFnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
metoda	metoda	k1gFnSc1
záznamu	záznam	k1gInSc2
časové	časový	k2eAgFnSc2d1
změny	změna	k1gFnSc2
elektrického	elektrický	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
způsobeného	způsobený	k2eAgInSc2d1
mozkovou	mozkový	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
<g/>
?	?	kIx.
</s>