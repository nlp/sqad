<s>
Důsledkem	důsledek	k1gInSc7	důsledek
působení	působení	k1gNnSc2	působení
gluonů	gluon	k1gInPc2	gluon
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
vzniku	vznik	k1gInSc2	vznik
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
neutronem	neutron	k1gInSc7	neutron
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
