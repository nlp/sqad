<s>
Gluony	gluon	k1gInPc1	gluon
jsou	být	k5eAaImIp3nP	být
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
zprostředkující	zprostředkující	k2eAgFnSc4d1	zprostředkující
silnou	silný	k2eAgFnSc4d1	silná
interakci	interakce	k1gFnSc4	interakce
mezi	mezi	k7c7	mezi
kvarky	kvark	k1gInPc7	kvark
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
působení	působení	k1gNnSc2	působení
gluonů	gluon	k1gInPc2	gluon
je	být	k5eAaImIp3nS	být
možnost	možnost	k1gFnSc4	možnost
vzniku	vznik	k1gInSc2	vznik
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvoření	vytvoření	k1gNnSc4	vytvoření
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
protonem	proton	k1gInSc7	proton
a	a	k8xC	a
neutronem	neutron	k1gInSc7	neutron
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
glue	glu	k1gInSc2	glu
–	–	k?	–
lepidlo	lepidlo	k1gNnSc1	lepidlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
gluony	gluon	k1gInPc1	gluon
jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
drží	držet	k5eAaImIp3nS	držet
jádro	jádro	k1gNnSc1	jádro
atomu	atom	k1gInSc2	atom
pevně	pevně	k6eAd1	pevně
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
gluon	gluon	k1gInSc1	gluon
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
až	až	k9	až
8	[number]	k4	8
variant	varianta	k1gFnPc2	varianta
barevného	barevný	k2eAgInSc2d1	barevný
náboje	náboj	k1gInSc2	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
hadronu	hadron	k1gInSc2	hadron
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
děje	dít	k5eAaImIp3nS	dít
silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
kvarky	kvark	k1gInPc4	kvark
(	(	kIx(	(
<g/>
jež	jenž	k3xRgFnPc1	jenž
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
každý	každý	k3xTgInSc4	každý
svůj	svůj	k3xOyFgInSc4	svůj
barevný	barevný	k2eAgInSc4d1	barevný
náboj	náboj	k1gInSc4	náboj
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
posílají	posílat	k5eAaImIp3nP	posílat
gluony	gluon	k1gInPc1	gluon
o	o	k7c6	o
takových	takový	k3xDgInPc6	takový
barevných	barevný	k2eAgInPc6d1	barevný
nábojích	náboj	k1gInPc6	náboj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
"	"	kIx"	"
<g/>
subtraktivního	subtraktivní	k2eAgNnSc2d1	subtraktivní
míchání	míchání	k1gNnSc2	míchání
barev	barva	k1gFnPc2	barva
<g/>
"	"	kIx"	"
docílilo	docílit	k5eAaPmAgNnS	docílit
zase	zase	k9	zase
neutrální	neutrální	k2eAgFnSc2d1	neutrální
kombinace	kombinace	k1gFnSc2	kombinace
barev	barva	k1gFnPc2	barva
nábojů	náboj	k1gInPc2	náboj
kvarků	kvark	k1gInPc2	kvark
po	po	k7c6	po
narušení	narušení	k1gNnSc4	narušení
rovnováhy	rovnováha	k1gFnSc2	rovnováha
samotnou	samotný	k2eAgFnSc7d1	samotná
interakcí	interakce	k1gFnSc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
v	v	k7c6	v
DESY	DESY	kA	DESY
v	v	k7c6	v
Hamburgu	Hamburg	k1gInSc6	Hamburg
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
předpovězení	předpovězení	k1gNnSc2	předpovězení
<g/>
.	.	kIx.	.
</s>
<s>
Gluony	gluon	k1gInPc1	gluon
mají	mít	k5eAaImIp3nP	mít
nulovou	nulový	k2eAgFnSc4d1	nulová
klidovou	klidový	k2eAgFnSc4d1	klidová
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
nulový	nulový	k2eAgInSc4d1	nulový
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Gluon	gluon	k1gInSc1	gluon
je	být	k5eAaImIp3nS	být
částice	částice	k1gFnSc2	částice
se	s	k7c7	s
spinem	spin	k1gInSc7	spin
1	[number]	k4	1
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
boson	boson	k1gInSc4	boson
<g/>
.	.	kIx.	.
</s>
<s>
Gluon	gluon	k1gInSc1	gluon
je	být	k5eAaImIp3nS	být
svojí	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
antičásticí	antičástice	k1gFnSc7	antičástice
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
vlastností	vlastnost	k1gFnSc7	vlastnost
gluonů	gluon	k1gInPc2	gluon
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc1	jejich
barevný	barevný	k2eAgInSc1d1	barevný
náboj	náboj	k1gInSc1	náboj
<g/>
.	.	kIx.	.
</s>
<s>
Nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
volné	volný	k2eAgFnPc1d1	volná
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Gluony	gluon	k1gInPc1	gluon
tvoří	tvořit	k5eAaImIp3nP	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kvarky	kvark	k1gInPc7	kvark
za	za	k7c2	za
speciálních	speciální	k2eAgFnPc2d1	speciální
podmínek	podmínka	k1gFnPc2	podmínka
kvark-gluonové	kvarkluonový	k2eAgNnSc4d1	kvark-gluonový
plazma	plazma	k1gNnSc4	plazma
<g/>
.	.	kIx.	.
</s>
<s>
Intermediální	intermediální	k2eAgFnSc1d1	intermediální
částice	částice	k1gFnSc1	částice
Silná	silný	k2eAgFnSc1d1	silná
interakce	interakce	k1gFnSc1	interakce
</s>
