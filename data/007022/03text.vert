<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
firma	firma	k1gFnSc1	firma
Nikon	Nikona	k1gFnPc2	Nikona
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Nikon	Nikon	k1gMnSc1	Nikon
Corporation	Corporation	k1gInSc1	Corporation
či	či	k8xC	či
Nikon	Nikon	k1gMnSc1	Nikon
Corp	Corp	k1gMnSc1	Corp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
株	株	k?	株
<g/>
,	,	kIx,	,
Kabušiki	Kabušik	k1gFnPc1	Kabušik
gaiša	gaiš	k1gInSc2	gaiš
Nikon	Nikon	k1gInSc1	Nikon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
světových	světový	k2eAgMnPc2d1	světový
výrobců	výrobce	k1gMnPc2	výrobce
klasických	klasický	k2eAgInPc2d1	klasický
i	i	k8xC	i
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
a	a	k8xC	a
fotografické	fotografický	k2eAgFnSc2d1	fotografická
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
vybavení	vybavení	k1gNnSc4	vybavení
pro	pro	k7c4	pro
fotografii	fotografia	k1gFnSc4	fotografia
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
i	i	k9	i
další	další	k2eAgInPc4d1	další
optické	optický	k2eAgInPc4d1	optický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
mikroskopy	mikroskop	k1gInPc4	mikroskop
<g/>
,	,	kIx,	,
dalekohledy	dalekohled	k1gInPc4	dalekohled
<g/>
,	,	kIx,	,
brýlové	brýlový	k2eAgFnPc4d1	brýlová
čočky	čočka	k1gFnPc4	čočka
<g/>
,	,	kIx,	,
geodetické	geodetický	k2eAgInPc4d1	geodetický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
přístroje	přístroj	k1gInPc4	přístroj
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
polovodičových	polovodičový	k2eAgFnPc2d1	polovodičová
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
jemná	jemný	k2eAgNnPc1d1	jemné
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
krokové	krokový	k2eAgInPc4d1	krokový
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
se	se	k3xPyFc4	se
spojili	spojit	k5eAaPmAgMnP	spojit
tři	tři	k4xCgMnPc1	tři
malí	malý	k2eAgMnPc1d1	malý
japonští	japonský	k2eAgMnPc1d1	japonský
výrobci	výrobce	k1gMnPc1	výrobce
optiky	optika	k1gFnSc2	optika
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
optickou	optický	k2eAgFnSc4d1	optická
firmu	firma	k1gFnSc4	firma
Nippon	Nippon	k1gMnSc1	Nippon
Kogaku	Kogak	k1gInSc2	Kogak
K.K.	K.K.	k1gMnSc1	K.K.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
zahájila	zahájit	k5eAaPmAgFnS	zahájit
firma	firma	k1gFnSc1	firma
výrobu	výroba	k1gFnSc4	výroba
prvních	první	k4xOgInPc2	první
malých	malý	k2eAgInPc2d1	malý
binokulárních	binokulární	k2eAgInPc2d1	binokulární
dalekohledů	dalekohled	k1gInPc2	dalekohled
MIKRON	mikron	k1gInSc4	mikron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výroba	výroba	k1gFnSc1	výroba
mikroskopu	mikroskop	k1gInSc2	mikroskop
JOICO	JOICO	kA	JOICO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
pro	pro	k7c4	pro
fotografickou	fotografický	k2eAgFnSc4d1	fotografická
optiku	optika	k1gFnSc4	optika
použita	použít	k5eAaPmNgFnS	použít
dodnes	dodnes	k6eAd1	dodnes
používaná	používaný	k2eAgFnSc1d1	používaná
značka	značka	k1gFnSc1	značka
Nikkor	Nikkora	k1gFnPc2	Nikkora
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
rozvoj	rozvoj	k1gInSc4	rozvoj
prodělala	prodělat	k5eAaPmAgFnS	prodělat
firma	firma	k1gFnSc1	firma
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
armádním	armádní	k2eAgFnPc3d1	armádní
zakázkám	zakázka	k1gFnPc3	zakázka
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
technologií	technologie	k1gFnPc2	technologie
ze	z	k7c2	z
spřáteleného	spřátelený	k2eAgNnSc2d1	spřátelené
Německa	Německo	k1gNnSc2	Německo
zvětšila	zvětšit	k5eAaPmAgNnP	zvětšit
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
<g/>
x.	x.	k?	x.
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
firma	firma	k1gFnSc1	firma
zahájila	zahájit	k5eAaPmAgFnS	zahájit
výrobu	výroba	k1gFnSc4	výroba
prvního	první	k4xOgInSc2	první
malého	malý	k2eAgInSc2d1	malý
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
Nikon	Nikona	k1gFnPc2	Nikona
I.	I.	kA	I.
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
přestal	přestat	k5eAaPmAgMnS	přestat
optiku	optika	k1gFnSc4	optika
Nikonu	Nikona	k1gFnSc4	Nikona
používat	používat	k5eAaImF	používat
jeho	jeho	k3xOp3gMnSc1	jeho
největší	veliký	k2eAgMnSc1d3	veliký
konkurent	konkurent	k1gMnSc1	konkurent
-	-	kIx~	-
Canon	Canon	kA	Canon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
vyrábět	vyrábět	k5eAaImF	vyrábět
Nikon	Nikon	k1gInSc4	Nikon
F	F	kA	F
–	–	k?	–
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
jednookou	jednooký	k2eAgFnSc4d1	jednooká
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
popularitě	popularita	k1gFnSc3	popularita
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
Nikon	Nikona	k1gFnPc2	Nikona
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
Státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
firmu	firma	k1gFnSc4	firma
Nikon	Nikon	k1gMnSc1	Nikon
vybrala	vybrat	k5eAaPmAgFnS	vybrat
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
kinofilmového	kinofilmový	k2eAgInSc2d1	kinofilmový
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
aparát	aparát	k1gInSc1	aparát
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
výzkum	výzkum	k1gInSc4	výzkum
sice	sice	k8xC	sice
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
osvědčeného	osvědčený	k2eAgInSc2d1	osvědčený
Nikonu	Nikon	k1gInSc2	Nikon
F	F	kA	F
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
pro	pro	k7c4	pro
splnění	splnění	k1gNnSc4	splnění
přísných	přísný	k2eAgNnPc2d1	přísné
kritérií	kritérion	k1gNnPc2	kritérion
NASA	NASA	kA	NASA
výrazně	výrazně	k6eAd1	výrazně
přepracován	přepracovat	k5eAaPmNgInS	přepracovat
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
v	v	k7c6	v
misi	mise	k1gFnSc6	mise
Apollo	Apollo	k1gNnSc1	Apollo
15	[number]	k4	15
(	(	kIx(	(
<g/>
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikon	Nikon	k1gInSc4	Nikon
poznatky	poznatek	k1gInPc4	poznatek
získané	získaný	k2eAgInPc4d1	získaný
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
tohoto	tento	k3xDgInSc2	tento
aparátu	aparát	k1gInSc2	aparát
aplikoval	aplikovat	k5eAaBmAgMnS	aplikovat
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
další	další	k2eAgInPc1d1	další
aparáty	aparát	k1gInPc1	aparát
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
komerčních	komerční	k2eAgFnPc6d1	komerční
F2	F2	k1gFnPc6	F2
a	a	k8xC	a
F3	F3	k1gFnPc6	F3
již	již	k6eAd1	již
potřebovaly	potřebovat	k5eAaImAgInP	potřebovat
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
NASA	NASA	kA	NASA
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnPc4d2	menší
úpravy	úprava	k1gFnPc4	úprava
a	a	k8xC	a
aparát	aparát	k1gInSc4	aparát
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
komerčním	komerční	k2eAgMnSc6d1	komerční
F4	F4	k1gMnSc6	F4
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
jen	jen	k9	jen
několik	několik	k4yIc1	několik
kosmetických	kosmetický	k2eAgFnPc2d1	kosmetická
úprav	úprava	k1gFnPc2	úprava
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
snadnějšího	snadný	k2eAgNnSc2d2	snazší
ovládání	ovládání	k1gNnSc3	ovládání
ve	v	k7c6	v
skafandru	skafandr	k1gInSc6	skafandr
a	a	k8xC	a
beztížném	beztížný	k2eAgInSc6d1	beztížný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
postavila	postavit	k5eAaPmAgFnS	postavit
firma	firma	k1gFnSc1	firma
105	[number]	k4	105
<g/>
cm	cm	kA	cm
Schmidtův	Schmidtův	k2eAgInSc4d1	Schmidtův
teleskop	teleskop	k1gInSc4	teleskop
v	v	k7c6	v
tokijské	tokijský	k2eAgFnSc6d1	Tokijská
observatoři	observatoř	k1gFnSc6	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
zahájila	zahájit	k5eAaPmAgFnS	zahájit
výrobu	výrob	k1gInSc2	výrob
prvního	první	k4xOgInSc2	první
digitálního	digitální	k2eAgInSc2d1	digitální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
Nikon	Nikon	k1gInSc1	Nikon
Coolpix	Coolpix	k1gInSc1	Coolpix
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
digitální	digitální	k2eAgFnSc4d1	digitální
jednookou	jednooký	k2eAgFnSc4d1	jednooká
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
Nikon	Nikon	k1gInSc1	Nikon
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ukončila	ukončit	k5eAaPmAgFnS	ukončit
výrobu	výroba	k1gFnSc4	výroba
všech	všecek	k3xTgFnPc2	všecek
kinofilmových	kinofilmový	k2eAgFnPc2d1	kinofilmová
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
profesionálního	profesionální	k2eAgInSc2d1	profesionální
modelu	model	k1gInSc2	model
Nikon	Nikona	k1gFnPc2	Nikona
F	F	kA	F
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
dala	dát	k5eAaPmAgFnS	dát
na	na	k7c4	na
trh	trh	k1gInSc4	trh
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc6	první
full-frame	fullram	k1gInSc5	full-fram
(	(	kIx(	(
<g/>
senzor	senzor	k1gInSc1	senzor
má	mít	k5eAaImIp3nS	mít
velikost	velikost	k1gFnSc4	velikost
políčka	políčko	k1gNnSc2	políčko
kinofilmu	kinofilm	k1gInSc2	kinofilm
<g/>
)	)	kIx)	)
digitální	digitální	k2eAgFnSc4d1	digitální
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
(	(	kIx(	(
<g/>
i	i	k9	i
přes	přes	k7c4	přes
několik	několik	k4yIc4	několik
vyjádření	vyjádření	k1gNnPc2	vyjádření
firmy	firma	k1gFnSc2	firma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
velikost	velikost	k1gFnSc4	velikost
čipu	čip	k1gInSc2	čip
podporovat	podporovat	k5eAaImF	podporovat
nebude	být	k5eNaImBp3nS	být
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
největší	veliký	k2eAgMnSc1d3	veliký
japonský	japonský	k2eAgMnSc1d1	japonský
rival	rival	k1gMnSc1	rival
Nikonu	Nikon	k1gInSc2	Nikon
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
fotografické	fotografický	k2eAgFnSc2d1	fotografická
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
firma	firma	k1gFnSc1	firma
Canon	Canon	kA	Canon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
prvních	první	k4xOgInPc6	první
aparátech	aparát	k1gInPc6	aparát
v	v	k7c6	v
letech	let	k1gInPc6	let
1935	[number]	k4	1935
–	–	k?	–
1948	[number]	k4	1948
používala	používat	k5eAaImAgFnS	používat
výhradně	výhradně	k6eAd1	výhradně
optiku	optika	k1gFnSc4	optika
Nikkor	Nikkora	k1gFnPc2	Nikkora
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
obou	dva	k4xCgFnPc2	dva
firem	firma	k1gFnPc2	firma
prakticky	prakticky	k6eAd1	prakticky
nekompatibilní	kompatibilní	k2eNgInSc1d1	nekompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
řada	řada	k1gFnSc1	řada
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
své	svůj	k3xOyFgNnSc4	svůj
příslušenství	příslušenství	k1gNnSc4	příslušenství
(	(	kIx(	(
<g/>
objektivy	objektiv	k1gInPc4	objektiv
<g/>
,	,	kIx,	,
blesky	blesk	k1gInPc4	blesk
<g/>
)	)	kIx)	)
ve	v	k7c6	v
variantách	varianta	k1gFnPc6	varianta
přizpůsobených	přizpůsobený	k2eAgFnPc6d1	přizpůsobená
pro	pro	k7c4	pro
těla	tělo	k1gNnSc2	tělo
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
Nikon	Nikona	k1gFnPc2	Nikona
<g/>
,	,	kIx,	,
Canon	Canon	kA	Canon
i	i	k8xC	i
dalších	další	k2eAgFnPc2d1	další
značek	značka	k1gFnPc2	značka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
dalšího	další	k2eAgNnSc2d1	další
příslušenství	příslušenství	k1gNnSc2	příslušenství
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
standardizováno	standardizován	k2eAgNnSc1d1	standardizováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
použitelné	použitelný	k2eAgNnSc1d1	použitelné
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c4	na
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
značku	značka	k1gFnSc4	značka
(	(	kIx(	(
<g/>
filtry	filtr	k1gInPc4	filtr
<g/>
,	,	kIx,	,
stativy	stativ	k1gInPc4	stativ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikon	Nikon	k1gInSc1	Nikon
I	i	k9	i
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gInSc1	Nikon
M	M	kA	M
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gNnSc4	Nikon
S	s	k7c7	s
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gMnSc1	Nikon
S2	S2	k1gMnSc1	S2
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gInSc1	Nikon
SP	SP	kA	SP
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gMnSc1	Nikon
S3	S3	k1gMnSc1	S3
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gMnSc1	Nikon
S4	S4	k1gMnSc1	S4
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Nikon	Nikon	k1gInSc1	Nikon
<g />
.	.	kIx.	.
</s>
<s>
S3M	S3M	k4	S3M
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Nikonos	Nikonosa	k1gFnPc2	Nikonosa
řada	řada	k1gFnSc1	řada
aparátů	aparát	k1gInPc2	aparát
pro	pro	k7c4	pro
fotografování	fotografování	k1gNnSc4	fotografování
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
Nikon	Nikon	k1gMnSc1	Nikon
FM3A	FM3A	k1gMnPc2	FM3A
Nikon	Nikon	k1gMnSc1	Nikon
FM10	FM10	k1gMnSc1	FM10
Nikon	Nikon	k1gMnSc1	Nikon
FE10	FE10	k1gMnSc1	FE10
Nikon	Nikon	k1gMnSc1	Nikon
FA	fa	kA	fa
Nikon	Nikon	k1gMnSc1	Nikon
FE	FE	kA	FE
Nikon	Nikon	k1gMnSc1	Nikon
FE2	FE2	k1gMnSc1	FE2
Nikon	Nikon	k1gMnSc1	Nikon
FG	FG	kA	FG
Nikon	Nikon	k1gMnSc1	Nikon
FG20	FG20	k1gMnSc1	FG20
Nikon	Nikon	k1gMnSc1	Nikon
FM	FM	kA	FM
Nikon	Nikon	k1gMnSc1	Nikon
FM2	FM2	k1gMnSc1	FM2
Nikon	Nikon	k1gMnSc1	Nikon
F	F	kA	F
series	series	k1gMnSc1	series
Nikon	Nikon	k1gMnSc1	Nikon
F2	F2	k1gMnSc1	F2
series	series	k1gMnSc1	series
Nikon	Nikon	k1gMnSc1	Nikon
F3	F3	k1gFnSc2	F3
series	series	k1gInSc1	series
Nikkormat	Nikkormat	k1gInSc1	Nikkormat
series	seriesa	k1gFnPc2	seriesa
Nikkorex	Nikkorex	k1gInSc1	Nikkorex
series	series	k1gMnSc1	series
Nikon	Nikon	k1gMnSc1	Nikon
EL2	EL2	k1gMnSc1	EL2
Nikon	Nikon	k1gMnSc1	Nikon
EM	Ema	k1gFnPc2	Ema
<g />
.	.	kIx.	.
</s>
<s>
Nikon	Nikon	k1gMnSc1	Nikon
F301	F301	k1gMnSc1	F301
Nikon	Nikon	k1gMnSc1	Nikon
F50	F50	k1gMnSc1	F50
Nikon	Nikon	k1gMnSc1	Nikon
F60	F60	k1gMnSc1	F60
Nikon	Nikon	k1gMnSc1	Nikon
F70	F70	k1gMnSc1	F70
Nikon	Nikon	k1gMnSc1	Nikon
F401	F401	k1gMnSc1	F401
Nikon	Nikon	k1gMnSc1	Nikon
F401S	F401S	k1gMnSc1	F401S
Nikon	Nikon	k1gMnSc1	Nikon
F401X	F401X	k1gMnSc1	F401X
Nikon	Nikon	k1gMnSc1	Nikon
F501	F501	k1gMnSc1	F501
Nikon	Nikon	k1gMnSc1	Nikon
F601	F601	k1gMnSc1	F601
Nikon	Nikon	k1gMnSc1	Nikon
F801	F801	k1gMnSc1	F801
Nikon	Nikon	k1gMnSc1	Nikon
F801S	F801S	k1gMnSc1	F801S
Nikon	Nikon	k1gMnSc1	Nikon
F90	F90	k1gMnSc1	F90
Nikon	Nikon	k1gMnSc1	Nikon
F	F	kA	F
<g/>
90	[number]	k4	90
<g/>
x	x	k?	x
Nikon	Nikon	k1gMnSc1	Nikon
F55	F55	k1gMnSc1	F55
Nikon	Nikon	k1gMnSc1	Nikon
F65	F65	k1gMnSc1	F65
Nikon	Nikon	k1gMnSc1	Nikon
F75	F75	k1gMnSc1	F75
Nikon	Nikon	k1gMnSc1	Nikon
F80	F80	k1gMnSc1	F80
Nikon	Nikon	k1gMnSc1	Nikon
F100	F100	k1gMnSc1	F100
Nikon	Nikon	k1gMnSc1	Nikon
F4	F4	k1gMnSc1	F4
Nikon	Nikon	k1gMnSc1	Nikon
F5	F5	k1gFnSc2	F5
Nikon	Nikon	k1gInSc1	Nikon
F6	F6	k1gFnSc4	F6
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
řada	řada	k1gFnSc1	řada
typů	typ	k1gInPc2	typ
Nikon	Nikona	k1gFnPc2	Nikona
<g />
.	.	kIx.	.
</s>
<s>
Coolpix	Coolpix	k1gInSc1	Coolpix
Nikon	Nikon	k1gMnSc1	Nikon
D40	D40	k1gMnSc1	D40
Nikon	Nikon	k1gMnSc1	Nikon
D	D	kA	D
<g/>
40	[number]	k4	40
<g/>
x	x	k?	x
Nikon	Nikon	k1gMnSc1	Nikon
D50	D50	k1gMnSc1	D50
Nikon	Nikon	k1gMnSc1	Nikon
D60	D60	k1gMnSc1	D60
Nikon	Nikon	k1gMnSc1	Nikon
D3000	D3000	k1gMnSc1	D3000
Nikon	Nikon	k1gMnSc1	Nikon
D3100	D3100	k1gMnSc1	D3100
Nikon	Nikon	k1gMnSc1	Nikon
D3200	D3200	k1gMnSc1	D3200
Nikon	Nikon	k1gMnSc1	Nikon
D3300	D3300	k1gMnSc1	D3300
Nikon	Nikon	k1gMnSc1	Nikon
D3400	D3400	k1gMnSc1	D3400
Nikon	Nikon	k1gMnSc1	Nikon
D5000	D5000	k1gMnSc1	D5000
Nikon	Nikon	k1gMnSc1	Nikon
D5100	D5100	k1gMnSc1	D5100
Nikon	Nikon	k1gMnSc1	Nikon
D70	D70	k1gMnSc1	D70
Nikon	Nikon	k1gMnSc1	Nikon
D	D	kA	D
<g/>
70	[number]	k4	70
<g/>
s	s	k7c7	s
Nikon	Nikon	k1gMnSc1	Nikon
D80	D80	k1gMnSc7	D80
Nikon	Nikon	k1gMnSc1	Nikon
D90	D90	k1gMnSc1	D90
Nikon	Nikon	k1gMnSc1	Nikon
D7000	D7000	k1gMnSc1	D7000
Nikon	Nikon	k1gMnSc1	Nikon
D7100	D7100	k1gMnSc1	D7100
Nikon	Nikon	k1gMnSc1	Nikon
D100	D100	k1gMnSc1	D100
Nikon	Nikon	k1gMnSc1	Nikon
D200	D200	k1gMnSc1	D200
Nikon	Nikon	k1gMnSc1	Nikon
D300	D300	k1gMnSc1	D300
Nikon	Nikon	k1gMnSc1	Nikon
<g />
.	.	kIx.	.
</s>
<s>
D700	D700	k4	D700
Nikon	Nikon	k1gMnSc1	Nikon
D1	D1	k1gMnSc1	D1
Nikon	Nikon	k1gMnSc1	Nikon
D1H	D1H	k1gMnSc1	D1H
Nikon	Nikon	k1gMnSc1	Nikon
D1X	D1X	k1gMnSc1	D1X
Nikon	Nikon	k1gMnSc1	Nikon
D2H	D2H	k1gMnSc1	D2H
Nikon	Nikon	k1gMnSc1	Nikon
D2X	D2X	k1gMnSc1	D2X
Nikon	Nikon	k1gMnSc1	Nikon
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
Hs	Hs	k1gMnPc1	Hs
Nikon	Nikona	k1gFnPc2	Nikona
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
Xs	Xs	k1gFnSc2	Xs
Nikon	Nikona	k1gFnPc2	Nikona
D3	D3	k1gMnSc1	D3
Nikon	Nikon	k1gMnSc1	Nikon
D3X	D3X	k1gMnSc1	D3X
Nikon	Nikon	k1gMnSc1	Nikon
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
s	s	k7c7	s
Nikon	Nikon	k1gMnSc1	Nikon
D4	D4	k1gMnSc7	D4
Pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
objektivů	objektiv	k1gInPc2	objektiv
používá	používat	k5eAaImIp3nS	používat
Nikon	Nikon	k1gNnSc1	Nikon
následující	následující	k2eAgFnSc2d1	následující
zkratky	zkratka	k1gFnSc2	zkratka
<g/>
:	:	kIx,	:
AF	AF	kA	AF
-	-	kIx~	-
Autofocus	Autofocus	k1gMnSc1	Autofocus
-	-	kIx~	-
automatické	automatický	k2eAgNnSc1d1	automatické
ostření	ostření	k1gNnSc1	ostření
(	(	kIx(	(
<g/>
motorem	motor	k1gInSc7	motor
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
AF-S	AF-S	k?	AF-S
-	-	kIx~	-
Autofocus-Silent	Autofocus-Silent	k1gMnSc1	Autofocus-Silent
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
ostření	ostření	k1gNnSc4	ostření
ultrazvukový	ultrazvukový	k2eAgInSc1d1	ultrazvukový
piezomotor	piezomotor	k1gInSc1	piezomotor
(	(	kIx(	(
<g/>
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
tiché	tichý	k2eAgNnSc1d1	tiché
ostření	ostření	k1gNnSc1	ostření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
AF-I	AF-I	k?	AF-I
-	-	kIx~	-
Autofocus-Internal	Autofocus-Internal	k1gFnSc1	Autofocus-Internal
-	-	kIx~	-
největší	veliký	k2eAgInPc1d3	veliký
teleobjektivy	teleobjektiv	k1gInPc1	teleobjektiv
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
vestavěný	vestavěný	k2eAgInSc4d1	vestavěný
elektrický	elektrický	k2eAgInSc4d1	elektrický
motor	motor	k1gInSc4	motor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
motor	motor	k1gInSc1	motor
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
by	by	kYmCp3nS	by
je	on	k3xPp3gInPc4	on
neutáhl	utáhnout	k5eNaPmAgMnS	utáhnout
<g/>
.	.	kIx.	.
</s>
<s>
ED	ED	kA	ED
-	-	kIx~	-
Extra-low	Extraow	k1gMnSc2	Extra-low
Dispersion	Dispersion	k1gInSc1	Dispersion
glass	glass	k6eAd1	glass
-	-	kIx~	-
využito	využit	k2eAgNnSc4d1	využito
speciální	speciální	k2eAgNnSc4d1	speciální
sklo	sklo	k1gNnSc4	sklo
snižující	snižující	k2eAgFnSc4d1	snižující
chromatickou	chromatický	k2eAgFnSc4d1	chromatická
aberaci	aberace	k1gFnSc4	aberace
<g/>
.	.	kIx.	.
</s>
<s>
IF	IF	kA	IF
-	-	kIx~	-
Internal	Internal	k1gMnSc1	Internal
Focus	Focus	k1gMnSc1	Focus
-	-	kIx~	-
Při	při	k7c6	při
ostření	ostření	k1gNnSc6	ostření
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
jen	jen	k9	jen
elementy	element	k1gInPc4	element
uvnitř	uvnitř	k7c2	uvnitř
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
objektiv	objektiv	k1gInSc1	objektiv
nemění	měnit	k5eNaImIp3nS	měnit
své	svůj	k3xOyFgInPc4	svůj
vnější	vnější	k2eAgInPc4d1	vnější
rozměry	rozměr	k1gInPc4	rozměr
.	.	kIx.	.
</s>
<s>
DX	DX	kA	DX
-	-	kIx~	-
Lens	Lens	k1gInSc4	Lens
-	-	kIx~	-
objektiv	objektiv	k1gInSc4	objektiv
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
jen	jen	k9	jen
v	v	k7c6	v
digitálních	digitální	k2eAgFnPc6d1	digitální
zrcadlovkách	zrcadlovka	k1gFnPc6	zrcadlovka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
snímač	snímač	k1gInSc4	snímač
formátu	formát	k1gInSc2	formát
APS-C	APS-C	k1gMnPc2	APS-C
(	(	kIx(	(
<g/>
u	u	k7c2	u
digitálních	digitální	k2eAgFnPc2d1	digitální
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
s	s	k7c7	s
čipem	čip	k1gInSc7	čip
FF	ff	kA	ff
nevykreslé	vykreslý	k2eNgFnPc1d1	vykreslý
cel	clo	k1gNnPc2	clo
pole	pole	k1gNnSc2	pole
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
VR	vr	k0	vr
-	-	kIx~	-
Vibration	Vibration	k1gInSc1	Vibration
Reduction	Reduction	k1gInSc1	Reduction
-	-	kIx~	-
Uvnitř	uvnitř	k7c2	uvnitř
objektivu	objektiv	k1gInSc2	objektiv
je	být	k5eAaImIp3nS	být
soustava	soustava	k1gFnSc1	soustava
piezoelementů	piezoelement	k1gInPc2	piezoelement
natáčejících	natáčející	k2eAgInPc2d1	natáčející
obraz	obraz	k1gInSc4	obraz
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
vibrací	vibrace	k1gFnPc2	vibrace
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
fotografování	fotografování	k1gNnSc4	fotografování
"	"	kIx"	"
<g/>
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
<g/>
"	"	kIx"	"
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
normálně	normálně	k6eAd1	normálně
už	už	k6eAd1	už
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
stativ	stativ	k1gInSc4	stativ
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
-	-	kIx~	-
Distance	distance	k1gFnSc1	distance
<g/>
/	/	kIx~	/
<g/>
Dimension	Dimension	k1gInSc1	Dimension
(	(	kIx(	(
<g/>
udáváno	udávat	k5eAaImNgNnS	udávat
za	za	k7c7	za
f-číslem	f-čísl	k1gInSc7	f-čísl
<g/>
)	)	kIx)	)
-	-	kIx~	-
Objektiv	objektiv	k1gInSc1	objektiv
podporuje	podporovat	k5eAaImIp3nS	podporovat
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
expozice	expozice	k1gFnSc2	expozice
3D	[number]	k4	3D
Matrix	Matrix	k1gInSc1	Matrix
<g/>
.	.	kIx.	.
</s>
<s>
G	G	kA	G
-	-	kIx~	-
(	(	kIx(	(
<g/>
udáváno	udávat	k5eAaImNgNnS	udávat
za	za	k7c7	za
f-číslem	f-čísl	k1gInSc7	f-čísl
<g/>
)	)	kIx)	)
Objektiv	objektiv	k1gInSc1	objektiv
nemá	mít	k5eNaImIp3nS	mít
kroužek	kroužek	k1gInSc4	kroužek
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
clony	clona	k1gFnSc2	clona
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
elektronicky	elektronicky	k6eAd1	elektronicky
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
(	(	kIx(	(
<g/>
totéž	týž	k3xTgNnSc4	týž
mají	mít	k5eAaImIp3nP	mít
objektivy	objektiv	k1gInPc1	objektiv
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Micro	Micro	k1gNnSc1	Micro
-	-	kIx~	-
Objektiv	objektiv	k1gInSc1	objektiv
vhodný	vhodný	k2eAgInSc1d1	vhodný
pro	pro	k7c4	pro
makrofotografii	makrofotografie	k1gFnSc4	makrofotografie
<g/>
.	.	kIx.	.
</s>
<s>
N	N	kA	N
-	-	kIx~	-
na	na	k7c6	na
čočkách	čočka	k1gFnPc6	čočka
je	být	k5eAaImIp3nS	být
využito	využít	k5eAaPmNgNnS	využít
antireflexních	antireflexní	k2eAgFnPc2d1	antireflexní
vrstev	vrstva	k1gFnPc2	vrstva
z	z	k7c2	z
nanokrystalů	nanokrystal	k1gMnPc2	nanokrystal
<g/>
.	.	kIx.	.
</s>
<s>
PC	PC	kA	PC
-	-	kIx~	-
Perspective	Perspectiv	k1gInSc5	Perspectiv
Control	Control	k1gInSc4	Control
-	-	kIx~	-
objektiv	objektiv	k1gInSc4	objektiv
umožňující	umožňující	k2eAgFnSc4d1	umožňující
korekci	korekce	k1gFnSc4	korekce
perspektivního	perspektivní	k2eAgNnSc2d1	perspektivní
zkreslení	zkreslení	k1gNnSc2	zkreslení
<g/>
.	.	kIx.	.
</s>
<s>
IX	IX	kA	IX
-	-	kIx~	-
speciální	speciální	k2eAgInPc4d1	speciální
objektivy	objektiv	k1gInPc4	objektiv
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
aparáty	aparát	k1gInPc4	aparát
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
Pronea	Prone	k1gInSc2	Prone
DC	DC	kA	DC
-	-	kIx~	-
Objektiv	objektiv	k1gInSc1	objektiv
má	mít	k5eAaImIp3nS	mít
ovládání	ovládání	k1gNnSc4	ovládání
hloubky	hloubka	k1gFnSc2	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
a	a	k8xC	a
vzhledu	vzhled	k1gInSc2	vzhled
rozostření	rozostření	k1gNnSc2	rozostření
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
bokeh	bokeh	k1gInSc1	bokeh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
AI	AI	kA	AI
<g/>
/	/	kIx~	/
<g/>
AI-S	AI-S	k1gFnSc1	AI-S
-	-	kIx~	-
Auto	auto	k1gNnSc1	auto
(	(	kIx(	(
<g/>
aperture	apertur	k1gMnSc5	apertur
<g/>
)	)	kIx)	)
Indexing	Indexing	k1gInSc4	Indexing
-	-	kIx~	-
objektiv	objektiv	k1gInSc4	objektiv
má	mít	k5eAaImIp3nS	mít
výstupek	výstupek	k1gInSc1	výstupek
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
nastavené	nastavený	k2eAgFnSc6d1	nastavená
cloně	clona	k1gFnSc6	clona
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
aparátu	aparát	k1gInSc2	aparát
(	(	kIx(	(
<g/>
používaly	používat	k5eAaImAgInP	používat
první	první	k4xOgInPc1	první
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
vybavené	vybavený	k2eAgInPc1d1	vybavený
expoziční	expoziční	k2eAgFnSc7d1	expoziční
poloautomatickou	poloautomatický	k2eAgFnSc7d1	poloautomatická
a	a	k8xC	a
automatikou	automatika	k1gFnSc7	automatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
14	[number]	k4	14
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
ED	ED	kA	ED
<g />
.	.	kIx.	.
</s>
<s>
AF	AF	kA	AF
16	[number]	k4	16
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
Fisheye	Fisheye	k1gFnSc1	Fisheye
-	-	kIx~	-
rybí	rybí	k2eAgNnSc1d1	rybí
oko	oko	k1gNnSc1	oko
18	[number]	k4	18
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
AF	AF	kA	AF
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g />
.	.	kIx.	.
</s>
<s>
AF	AF	kA	AF
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
AF	AF	kA	AF
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
<g/>
D	D	kA	D
AF	AF	kA	AF
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
AF	AF	kA	AF
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
AF	AF	kA	AF
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
D	D	kA	D
AF	AF	kA	AF
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
AF	AF	kA	AF
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
<g/>
D	D	kA	D
AF	AF	kA	AF
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
<g/>
G	G	kA	G
AF-S	AF-S	k1gFnSc1	AF-S
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
<g />
.	.	kIx.	.
</s>
<s>
AF	AF	kA	AF
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
<g/>
D	D	kA	D
AF	AF	kA	AF
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
<g/>
D	D	kA	D
AF	AF	kA	AF
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
<g/>
D	D	kA	D
AF	AF	kA	AF
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
D	D	kA	D
AF	AF	kA	AF
DC	DC	kA	DC
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
D	D	kA	D
AF	AF	kA	AF
DC	DC	kA	DC
180	[number]	k4	180
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnPc2	ED-IF
AF	AF	kA	AF
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
II	II	kA	II
<g />
.	.	kIx.	.
</s>
<s>
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF	AF	kA	AF
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnSc1	ED-IF
AF-S	AF-S	k1gFnSc1	AF-S
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF-S	AF-S	k1gFnSc2	AF-S
II	II	kA	II
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF-S	AF-S	k1gFnSc2	AF-S
II	II	kA	II
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF-S	AF-S	k1gFnSc2	AF-S
II	II	kA	II
18-35	[number]	k4	18-35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
4.5	[number]	k4	4.5
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnPc2	ED-IF
AF	AF	kA	AF
20-35	[number]	k4	20-35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
IF	IF	kA	IF
24-85	[number]	k4	24-85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
D	D	kA	D
IF	IF	kA	IF
AF	AF	kA	AF
24-85	[number]	k4	24-85
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
4.5	[number]	k4	4.5
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gFnSc1	ED-IF
AF-S	AF-S	k1gFnSc1	AF-S
24-120	[number]	k4	24-120
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
28-80	[number]	k4	28-80
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.3	[number]	k4	3.3
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
AF	AF	kA	AF
28-100	[number]	k4	28-100
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
AF	AF	kA	AF
28-105	[number]	k4	28-105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
4.5	[number]	k4	4.5
<g/>
D	D	kA	D
AF	AF	kA	AF
28-200	[number]	k4	28-200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gFnPc2	ED-IF
AF	AF	kA	AF
70-210	[number]	k4	70-210
mm	mm	kA	mm
f	f	k?	f
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
AF	AF	kA	AF
70-210	[number]	k4	70-210
mm	mm	kA	mm
f	f	k?	f
<g/>
4	[number]	k4	4
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
D	D	kA	D
AF	AF	kA	AF
70-300	[number]	k4	70-300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
D	D	kA	D
ED	ED	kA	ED
AF	AF	kA	AF
70-300	[number]	k4	70-300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
AF	AF	kA	AF
17-35	[number]	k4	17-35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED-IF	ED-IF	k1gFnSc1	ED-IF
AF-S	AF-S	k1gFnSc1	AF-S
28-70	[number]	k4	28-70
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnSc1	ED-IF
AF-S	AF-S	k1gFnSc1	AF-S
35-70	[number]	k4	35-70
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
70-200	[number]	k4	70-200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
80-200	[number]	k4	80-200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED	ED	kA	ED
AF	AF	kA	AF
80-200	[number]	k4	80-200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
ED	ED	kA	ED
AF	AF	kA	AF
80-400	[number]	k4	80-400
mm	mm	kA	mm
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
D	D	kA	D
ED	ED	kA	ED
AF	AF	kA	AF
VR	vr	k0	vr
200-400	[number]	k4	200-400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
10.5	[number]	k4	10.5
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
G	G	kA	G
ED	ED	kA	ED
AF	AF	kA	AF
DX	DX	kA	DX
12-24	[number]	k4	12-24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF-S	AF-S	k1gFnSc2	AF-S
DX	DX	kA	DX
17-55	[number]	k4	17-55
<g />
.	.	kIx.	.
</s>
<s>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF-S	AF-S	k1gFnSc2	AF-S
DX	DX	kA	DX
18-70	[number]	k4	18-70
mm	mm	kA	mm
f	f	k?	f
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
4.5	[number]	k4	4.5
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gFnSc2	ED-IF
AF-S	AF-S	k1gFnSc2	AF-S
DX	DX	kA	DX
18-55	[number]	k4	18-55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED	ED	kA	ED
AF-S	AF-S	k1gFnPc2	AF-S
DX	DX	kA	DX
18-55	[number]	k4	18-55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED	ED	kA	ED
AF-S	AF-S	k1gFnPc2	AF-S
DX	DX	kA	DX
VR	vr	k0	vr
55-200	[number]	k4	55-200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED	ED	kA	ED
AF-S	AF-S	k1gFnPc2	AF-S
DX	DX	kA	DX
18-200	[number]	k4	18-200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED	ED	kA	ED
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
DX	DX	kA	DX
18	[number]	k4	18
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
<g/>
G	G	kA	G
ED	ED	kA	ED
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
DX	DX	kA	DX
60	[number]	k4	60
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
Micro	Micro	k1gNnSc4	Micro
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
AF	AF	kA	AF
Micro	Micro	k1gNnSc4	Micro
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
G	G	kA	G
ED-IF	ED-IF	k1gMnSc1	ED-IF
AF-S	AF-S	k1gMnSc1	AF-S
VR	vr	k0	vr
Micro	Micro	k1gNnSc1	Micro
200	[number]	k4	200
mm	mm	kA	mm
<g />
.	.	kIx.	.
</s>
<s>
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
D	D	kA	D
ED-IF	ED-IF	k1gFnPc2	ED-IF
AF	AF	kA	AF
Micro	Micro	k1gNnSc4	Micro
70-180	[number]	k4	70-180
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
<g/>
-	-	kIx~	-
<g/>
5.6	[number]	k4	5.6
ED	ED	kA	ED
AF-D	AF-D	k1gFnSc7	AF-D
Micro	Micro	k1gNnSc1	Micro
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
D	D	kA	D
PC	PC	kA	PC
Micro	Micro	k1gNnSc4	Micro
Nikkor	Nikkor	k1gInSc1	Nikkor
40	[number]	k4	40
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
N	N	kA	N
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
N	N	kA	N
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
63	[number]	k4	63
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g/>
N	N	kA	N
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
75	[number]	k4	75
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
N	N	kA	N
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
80	[number]	k4	80
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
<g/>
N	N	kA	N
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
EL-Nikkor	EL-Nikkor	k1gInSc1	EL-Nikkor
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5.6	[number]	k4	5.6
EL-Nikkor	EL-Nikkor	k1gInSc1	EL-Nikkor
150	[number]	k4	150
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
EL-Nikkor	EL-Nikkor	k1gInSc1	EL-Nikkor
180	[number]	k4	180
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
<g/>
A	A	kA	A
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
210	[number]	k4	210
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
<g/>
A	A	kA	A
EL-Nikkor	EL-Nikkor	k1gInSc4	EL-Nikkor
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
EL-Nikkor	EL-Nikkor	k1gInSc1	EL-Nikkor
6	[number]	k4	6
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Fisheye-Nikkor	Fisheye-Nikkor	k1gInSc1	Fisheye-Nikkor
220	[number]	k4	220
<g/>
°	°	k?	°
Non-Ai	Non-A	k1gFnSc6	Non-A
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
objektiv	objektiv	k1gInSc4	objektiv
"	"	kIx"	"
<g/>
vidí	vidět	k5eAaImIp3nS	vidět
za	za	k7c4	za
sebe	sebe	k3xPyFc4	sebe
<g/>
"	"	kIx"	"
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
6	[number]	k4	6
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Fisheye-Nikkor	Fisheye-Nikkor	k1gInSc1	Fisheye-Nikkor
Non-Ai	Non-A	k1gFnSc2	Non-A
*	*	kIx~	*
<g/>
1	[number]	k4	1
8	[number]	k4	8
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Fisheye-Nikkor	Fisheye-Nikkor	k1gInSc1	Fisheye-Nikkor
8	[number]	k4	8
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Fisheye-Nikkor	Fisheye-Nikkor	k1gInSc1	Fisheye-Nikkor
Non-Ai	Non-A	k1gFnSc2	Non-A
*	*	kIx~	*
<g/>
1	[number]	k4	1
10	[number]	k4	10
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Nikkor-OP	Nikkor-OP	k1gFnSc1	Nikkor-OP
180	[number]	k4	180
<g/>
°	°	k?	°
Fisheye	Fishey	k1gInSc2	Fishey
*	*	kIx~	*
<g/>
1	[number]	k4	1
13	[number]	k4	13
<g />
.	.	kIx.	.
</s>
<s>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
13	[number]	k4	13
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
15	[number]	k4	15
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
15	[number]	k4	15
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
15	[number]	k4	15
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Nikkor	Nikkora	k1gFnPc2	Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
15	[number]	k4	15
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5.6	[number]	k4	5.6
Nikkor	Nikkor	k1gInSc1	Nikkor
Non-Ai	Non-A	k1gFnSc2	Non-A
Q.	Q.	kA	Q.
<g/>
DC	DC	kA	DC
16	[number]	k4	16
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkor	k1gMnSc1	Nikkor
Fisheye-Nikkor	Fisheye-Nikkor	k1gMnSc1	Fisheye-Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
16	[number]	k4	16
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkor	k1gMnSc1	Nikkor
Fisheye-Nikkor	Fisheye-Nikkor	k1gMnSc1	Fisheye-Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
18	[number]	k4	18
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
18	[number]	k4	18
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2.8	[number]	k4	2.8
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor-UD	Nikkor-UD	k1gFnSc1	Nikkor-UD
Non-	Non-	k1gFnSc1	Non-
Ai	Ai	k1gFnSc1	Ai
20	[number]	k4	20
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
21	[number]	k4	21
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Nikkor-O	Nikkor-O	k1gFnPc2	Nikkor-O
*	*	kIx~	*
<g/>
1	[number]	k4	1
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkor	k1gMnSc1	Nikkor
<g />
.	.	kIx.	.
</s>
<s>
Ai	Ai	k?	Ai
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor-N	Nikkor-N	k1gFnPc2	Nikkor-N
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
24	[number]	k4	24
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
PC-E	PC-E	k1gFnSc4	PC-E
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor-N	Nikkor-N	k1gFnPc2	Nikkor-N
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
<g />
.	.	kIx.	.
</s>
<s>
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikon	Nikon	k1gInSc1	Nikon
Series	Series	k1gInSc4	Series
E	E	kA	E
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
PC-Nikkor	PC-Nikkora	k1gFnPc2	PC-Nikkora
Ai	Ai	k1gFnSc4	Ai
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
PC-Nikkor	PC-Nikkora	k1gFnPc2	PC-Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
28	[number]	k4	28
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
PC-Nikkor	PC-Nikkora	k1gFnPc2	PC-Nikkora
Ai	Ai	k1gFnSc4	Ai
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor-K	Nikkor-K	k1gFnPc2	Nikkor-K
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor-O	Nikkor-O	k1gFnSc7	Nikkor-O
Non-Ai	Non-Ai	k1gNnSc2	Non-Ai
<g />
.	.	kIx.	.
</s>
<s>
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor-S	Nikkor-S	k1gFnPc2	Nikkor-S
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
35	[number]	k4	35
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2.8	[number]	k4	2.8
PC-Nikkor	PC-Nikkor	k1gInSc1	PC-Nikkor
Non-Ai	Non-A	k1gFnSc2	Non-A
45	[number]	k4	45
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkor	k1gInSc1	Nikkor
GN	GN	kA	GN
Non-Ai	Non-A	k1gFnSc2	Non-A
45	[number]	k4	45
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-P	Ai-P	k1gFnSc4	Ai-P
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor-S	Nikkor-S	k1gFnSc7	Nikkor-S
Non-Ai	Non-Ai	k1gNnSc2	Non-Ai
<g />
.	.	kIx.	.
</s>
<s>
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
Nikon	Nikon	k1gInSc1	Nikon
Series	Series	k1gInSc4	Series
E	E	kA	E
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
50	[number]	k4	50
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
55	[number]	k4	55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
Nikkor-S	Nikkor-S	k1gFnPc2	Nikkor-S
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
55	[number]	k4	55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
55	[number]	k4	55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
55	[number]	k4	55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Micro-Nikkor	Micro-Nikkor	k1gMnSc1	Micro-Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
<g />
.	.	kIx.	.
</s>
<s>
55	[number]	k4	55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor-P	Nikkor-P	k1gFnPc2	Nikkor-P
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
55	[number]	k4	55
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
UV-Nikkor	UV-Nikkor	k1gInSc1	UV-Nikkor
58	[number]	k4	58
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
NOCT-Nikkor	NOCT-Nikkora	k1gFnPc2	NOCT-Nikkora
Ai	Ai	k1gFnSc4	Ai
58	[number]	k4	58
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.2	[number]	k4	1.2
NOCT-Nikkor	NOCT-Nikkora	k1gFnPc2	NOCT-Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
58	[number]	k4	58
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.4	[number]	k4	1.4
Nikkor-S	Nikkor-S	k1gFnPc2	Nikkor-S
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1.4	[number]	k4	1.4
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
85	[number]	k4	85
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
100	[number]	k4	100
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikon	Nikon	k1gInSc1	Nikon
Series	Series	k1gInSc4	Series
E	E	kA	E
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
1.8	[number]	k4	1.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
105	[number]	k4	105
<g />
.	.	kIx.	.
</s>
<s>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Micro-Nikkor	Micro-Nikkora	k1gFnPc2	Micro-Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
105	[number]	k4	105
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Micro-Nikkor	Micro-Nikkora	k1gFnPc2	Micro-Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2.8	[number]	k4	2.8
Nikkor-Q	Nikkor-Q	k1gFnSc1	Nikkor-Q
Non-Ai	Non-A	k1gFnSc2	Non-A
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikon	Nikon	k1gInSc1	Nikon
Series	Series	k1gInSc4	Series
E	E	kA	E
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor-Q	Nikkor-Q	k1gFnPc2	Nikkor-Q
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
135	[number]	k4	135
<g />
.	.	kIx.	.
</s>
<s>
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
135	[number]	k4	135
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
UV-Nikkor	UV-Nikkor	k1gInSc1	UV-Nikkor
180	[number]	k4	180
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
<g />
.	.	kIx.	.
</s>
<s>
Ai-S	Ai-S	k?	Ai-S
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Micro-Nikkor	Micro-Nikkora	k1gFnPc2	Micro-Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Micro-Nikkor	Micro-Nikkora	k1gFnPc2	Micro-Nikkora
Ai	Ai	k1gFnSc4	Ai
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Micro-Nikkor	Micro-Nikkora	k1gFnPc2	Micro-Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
200	[number]	k4	200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
Medical-Nikkor	Medical-Nikkora	k1gFnPc2	Medical-Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
opravdu	opravdu	k6eAd1	opravdu
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
Nikkor	Nikkora	k1gFnPc2	Nikkora
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gInSc1	Nikkor
Non-Ai	Non-Ai	k1gNnSc1	Non-Ai
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
<g />
.	.	kIx.	.
</s>
<s>
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
Nikkor-P	Nikkor-P	k1gFnPc2	Nikkor-P
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
Nikkor-H	Nikkor-H	k1gFnPc2	Nikkor-H
Non-Ai	Non-Ae	k1gFnSc4	Non-Ae
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
Nikkor	Nikkora	k1gFnPc2	Nikkora
Ai	Ai	k1gFnSc4	Ai
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
<g />
.	.	kIx.	.
</s>
<s>
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
300	[number]	k4	300
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4.5	[number]	k4	4.5
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
2.8	[number]	k4	2.8
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2.8	[number]	k4	2.8
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
3.5	[number]	k4	3.5
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
400	[number]	k4	400
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5.6	[number]	k4	5.6
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-P	Ai-P	k1gMnSc1	Ai-P
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g />
.	.	kIx.	.
</s>
<s>
Reflex-Nikkor	Reflex-Nikkor	k1gInSc1	Reflex-Nikkor
Ai	Ai	k1gFnSc1	Ai
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
Reflex-Nikkor	Reflex-Nikkora	k1gFnPc2	Reflex-Nikkora
<g/>
.	.	kIx.	.
<g/>
C	C	kA	C
Non-Ai	Non-A	k1gFnSc2	Non-A
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
Reflex-Nikkor	Reflex-Nikkora	k1gFnPc2	Reflex-Nikkora
Ai	Ai	k1gFnSc4	Ai
500	[number]	k4	500
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
Reflex-Nikkor	Reflex-Nikkora	k1gFnPc2	Reflex-Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
4	[number]	k4	4
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai	Ai	k1gMnSc1	Ai
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
600	[number]	k4	600
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
5.6	[number]	k4	5.6
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
800	[number]	k4	800
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
800	[number]	k4	800
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
IF-ED	IF-ED	k1gMnSc1	IF-ED
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
800	[number]	k4	800
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
5.6	[number]	k4	5.6
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
1000	[number]	k4	1000
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
Reflex-Nikkor	Reflex-Nikkora	k1gFnPc2	Reflex-Nikkora
Ai-S	Ai-S	k1gFnSc4	Ai-S
1200	[number]	k4	1200
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
ED	ED	kA	ED
<g/>
*	*	kIx~	*
<g/>
Nikkor	Nikkor	k1gMnSc1	Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
2000	[number]	k4	2000
mm	mm	kA	mm
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
Reflex-Nikkor	Reflex-Nikkor	k1gMnSc1	Reflex-Nikkor
Ai-S	Ai-S	k1gMnSc1	Ai-S
-	-	kIx~	-
*	*	kIx~	*
<g/>
1	[number]	k4	1
-	-	kIx~	-
funguje	fungovat	k5eAaImIp3nS	fungovat
jen	jen	k9	jen
s	s	k7c7	s
přidruženým	přidružený	k2eAgInSc7d1	přidružený
hledáčkem	hledáček	k1gInSc7	hledáček
na	na	k7c6	na
F	F	kA	F
<g/>
,	,	kIx,	,
F2	F2	k1gFnPc1	F2
a	a	k8xC	a
Nikkormaty	Nikkormat	k1gInPc1	Nikkormat
<g/>
,	,	kIx,	,
co	co	k9	co
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
mirror	mirror	k1gMnSc1	mirror
lockup	lockup	k1gMnSc1	lockup
<g/>
.	.	kIx.	.
</s>
