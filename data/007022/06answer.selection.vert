<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
začala	začít	k5eAaPmAgFnS	začít
firma	firma	k1gFnSc1	firma
vyrábět	vyrábět	k5eAaImF	vyrábět
Nikon	Nikon	k1gInSc4	Nikon
F	F	kA	F
–	–	k?	–
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
jednookou	jednooký	k2eAgFnSc4d1	jednooká
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
popularitě	popularita	k1gFnSc3	popularita
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
Nikon	Nikona	k1gFnPc2	Nikona
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
Státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
NASA	NASA	kA	NASA
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
firmu	firma	k1gFnSc4	firma
Nikon	Nikon	k1gMnSc1	Nikon
vybrala	vybrat	k5eAaPmAgFnS	vybrat
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
kinofilmového	kinofilmový	k2eAgInSc2d1	kinofilmový
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
pro	pro	k7c4	pro
kosmický	kosmický	k2eAgInSc4d1	kosmický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
