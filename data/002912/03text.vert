<s>
Haptika	Haptika	k1gFnSc1	Haptika
je	být	k5eAaImIp3nS	být
kontakt	kontakt	k1gInSc4	kontakt
hmatem	hmat	k1gInSc7	hmat
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
"	"	kIx"	"
<g/>
haptein	haptein	k1gInSc1	haptein
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
dotýkat	dotýkat	k5eAaImF	dotýkat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
do	do	k7c2	do
sociální	sociální	k2eAgFnSc2d1	sociální
psychologie	psychologie	k1gFnSc2	psychologie
zavedl	zavést	k5eAaPmAgMnS	zavést
lingvista	lingvista	k1gMnSc1	lingvista
William	William	k1gInSc4	William
Austin	Austin	k2eAgInSc4d1	Austin
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
způsob	způsob	k1gInSc4	způsob
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
tlumočí	tlumočit	k5eAaImIp3nS	tlumočit
bezprostředním	bezprostřední	k2eAgInSc7d1	bezprostřední
tělesným	tělesný	k2eAgInSc7d1	tělesný
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
druhým	druhý	k4xOgMnSc7	druhý
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podání	podání	k1gNnSc4	podání
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
poplácání	poplácání	k1gNnSc3	poplácání
po	po	k7c6	po
ramenou	rameno	k1gNnPc6	rameno
či	či	k8xC	či
zádech	zádech	k1gInSc1	zádech
či	či	k8xC	či
nabídnutí	nabídnutí	k1gNnSc1	nabídnutí
rámě	rámě	k1gNnSc1	rámě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
haptický	haptický	k2eAgInSc1d1	haptický
kontakt	kontakt	k1gInSc1	kontakt
bývá	bývat	k5eAaImIp3nS	bývat
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
dané	daný	k2eAgFnSc2d1	daná
osoby	osoba	k1gFnSc2	osoba
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
druhy	druh	k1gInPc4	druh
doteku	dotek	k1gInSc2	dotek
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
které	který	k3yIgFnSc6	který
situaci	situace	k1gFnSc6	situace
přijatelné	přijatelný	k2eAgNnSc1d1	přijatelné
<g/>
.	.	kIx.	.
</s>
<s>
Dotek	dotek	k1gInSc1	dotek
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
taktilní	taktilní	k2eAgInSc4d1	taktilní
kontakt	kontakt	k1gInSc4	kontakt
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
interakci	interakce	k1gFnSc6	interakce
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
adresován	adresovat	k5eAaBmNgInS	adresovat
jako	jako	k8xS	jako
projev	projev	k1gInSc1	projev
přátelství	přátelství	k1gNnSc2	přátelství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pohlazení	pohlazení	k1gNnSc1	pohlazení
<g/>
,	,	kIx,	,
objetí	objetí	k1gNnSc1	objetí
<g/>
,	,	kIx,	,
políbení	políbení	k1gNnSc1	políbení
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pohlavek	pohlavek	k1gInSc1	pohlavek
<g/>
,	,	kIx,	,
facka	facka	k1gFnSc1	facka
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komunikační	komunikační	k2eAgInSc1d1	komunikační
význam	význam	k1gInSc1	význam
haptiky	haptika	k1gFnSc2	haptika
(	(	kIx(	(
<g/>
doteku	dotek	k1gInSc2	dotek
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
snadno	snadno	k6eAd1	snadno
rozluštit	rozluštit	k5eAaPmF	rozluštit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
milenecký	milenecký	k2eAgInSc1d1	milenecký
pár	pár	k1gInSc1	pár
držící	držící	k2eAgFnSc2d1	držící
se	se	k3xPyFc4	se
za	za	k7c4	za
ruce	ruka	k1gFnPc4	ruka
dává	dávat	k5eAaImIp3nS	dávat
najevo	najevo	k6eAd1	najevo
"	"	kIx"	"
<g/>
jsme	být	k5eAaImIp1nP	být
svoji	svůj	k3xOyFgFnSc4	svůj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poplácání	poplácání	k1gNnSc1	poplácání
po	po	k7c6	po
rameni	rameno	k1gNnSc6	rameno
znamená	znamenat	k5eAaImIp3nS	znamenat
pochvalu	pochvala	k1gFnSc4	pochvala
či	či	k8xC	či
uznání	uznání	k1gNnSc4	uznání
<g/>
,	,	kIx,	,
dotek	dotek	k1gInSc4	dotek
nohou	noha	k1gFnPc2	noha
pod	pod	k7c7	pod
stolem	stol	k1gInSc7	stol
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
signál	signál	k1gInSc1	signál
druhé	druhý	k4xOgFnSc2	druhý
<g />
.	.	kIx.	.
</s>
<s>
osobě	osoba	k1gFnSc3	osoba
"	"	kIx"	"
<g/>
přestaň	přestat	k5eAaPmRp2nS	přestat
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
uklidni	uklidnit	k5eAaPmRp2nS	uklidnit
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
přímý	přímý	k2eAgInSc1d1	přímý
(	(	kIx(	(
<g/>
dotek	dotek	k1gInSc1	dotek
kůží	kůže	k1gFnPc2	kůže
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
<g/>
)	)	kIx)	)
nepřímý	přímý	k2eNgMnSc1d1	nepřímý
(	(	kIx(	(
<g/>
např.	např.	kA	např.
poplácání	poplácání	k1gNnSc1	poplácání
holou	holý	k2eAgFnSc7d1	holá
rukou	ruka	k1gFnSc7	ruka
po	po	k7c6	po
rameni	rameno	k1gNnSc6	rameno
oblečeného	oblečený	k2eAgMnSc2d1	oblečený
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
částí	část	k1gFnSc7	část
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
osoba	osoba	k1gFnSc1	osoba
dotkla	dotknout	k5eAaPmAgFnS	dotknout
určité	určitý	k2eAgFnPc4d1	určitá
části	část	k1gFnPc4	část
těla	tělo	k1gNnSc2	tělo
jiného	jiný	k2eAgMnSc4d1	jiný
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
hmatem	hmat	k1gInSc7	hmat
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
informační	informační	k2eAgFnSc4d1	informační
a	a	k8xC	a
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
hmat	hmat	k1gInSc1	hmat
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
zraku	zrak	k1gInSc6	zrak
druhým	druhý	k4xOgInSc7	druhý
nejcitlivějším	citlivý	k2eAgInSc7d3	nejcitlivější
smyslem	smysl	k1gInSc7	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Sluch	sluch	k1gInSc1	sluch
je	být	k5eAaImIp3nS	být
až	až	k9	až
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
znamená	znamenat	k5eAaImIp3nS	znamenat
nedostatek	nedostatek	k1gInSc4	nedostatek
lidského	lidský	k2eAgInSc2d1	lidský
doteku	dotek	k1gInSc2	dotek
<g/>
.	.	kIx.	.
</s>
<s>
Touha	touha	k1gFnSc1	touha
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
jen	jen	k6eAd1	jen
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
psychoterapeutické	psychoterapeutický	k2eAgInPc1d1	psychoterapeutický
postupy	postup	k1gInPc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
Sebehaptika	Sebehaptika	k1gFnSc1	Sebehaptika
je	být	k5eAaImIp3nS	být
dotýkání	dotýkání	k1gNnSc4	dotýkání
se	se	k3xPyFc4	se
vlastního	vlastní	k2eAgNnSc2d1	vlastní
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
během	během	k7c2	během
sociální	sociální	k2eAgFnSc2d1	sociální
interakce	interakce	k1gFnSc2	interakce
také	také	k9	také
leccos	leccos	k3yInSc4	leccos
prozradit	prozradit	k5eAaPmF	prozradit
<g/>
.	.	kIx.	.
</s>
<s>
Propletené	propletený	k2eAgFnPc4d1	propletená
ruce	ruka	k1gFnPc4	ruka
v	v	k7c6	v
klíně	klín	k1gInSc6	klín
mohou	moct	k5eAaImIp3nP	moct
značit	značit	k5eAaImF	značit
nervozitu	nervozita	k1gFnSc4	nervozita
aj.	aj.	kA	aj.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
