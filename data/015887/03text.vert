<s>
Organizační	organizační	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
</s>
<s>
Organizační	organizační	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
organization	organization	k1gInSc1
development	development	k1gInSc1
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
OD	od	k7c2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
koncept	koncept	k1gInSc1
a	a	k8xC
metodologie	metodologie	k1gFnSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
teorie	teorie	k1gFnSc2
managementu	management	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
uskutečňování	uskutečňování	k1gNnSc2
plánovaných	plánovaný	k2eAgFnPc2d1
sociálních	sociální	k2eAgFnPc2d1
změn	změna	k1gFnPc2
v	v	k7c6
organizacích	organizace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
OD	od	k7c2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
široce	široko	k6eAd1
definovat	definovat	k5eAaBmF
jako	jako	k8xS,k8xC
„	„	k?
<g/>
intervenční	intervenční	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
skupinově	skupinově	k6eAd1
dynamické	dynamický	k2eAgInPc1d1
procesy	proces	k1gInPc1
zaměřené	zaměřený	k2eAgInPc1d1
na	na	k7c4
organizační	organizační	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
k	k	k7c3
uskutečnění	uskutečnění	k1gNnSc3
plánovaných	plánovaný	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Bowman	Bowman	k1gMnSc1
a	a	k8xC
Asch	Asch	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
definují	definovat	k5eAaBmIp3nP
trochu	trochu	k6eAd1
odlišně	odlišně	k6eAd1
OD	od	k7c2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
dlouhodobý	dlouhodobý	k2eAgInSc4d1
intervenční	intervenční	k2eAgInSc4d1
program	program	k1gInSc4
v	v	k7c6
sociálních	sociální	k2eAgInPc6d1
procesech	proces	k1gInPc6
organizací	organizace	k1gFnPc2
využívající	využívající	k2eAgInPc1d1
principy	princip	k1gInPc1
a	a	k8xC
postupy	postup	k1gInPc1
vědy	věda	k1gFnSc2
o	o	k7c6
chování	chování	k1gNnSc6
s	s	k7c7
cílem	cíl	k1gInSc7
dosáhnout	dosáhnout	k5eAaPmF
změn	změna	k1gFnPc2
v	v	k7c6
chování	chování	k1gNnSc6
a	a	k8xC
postojích	postoj	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
vedou	vést	k5eAaImIp3nP
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
organizační	organizační	k2eAgFnSc2d1
efektivity	efektivita	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Organizační	organizační	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
zkoumá	zkoumat	k5eAaImIp3nS
a	a	k8xC
zabývá	zabývat	k5eAaImIp3nS
se	s	k7c7
vztahy	vztah	k1gInPc7
<g/>
,	,	kIx,
chováním	chování	k1gNnSc7
a	a	k8xC
postoji	postoj	k1gInPc7
vzhledem	vzhledem	k7c3
k	k	k7c3
jednotlivci	jednotlivec	k1gMnSc3
<g/>
,	,	kIx,
pracovní	pracovní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
,	,	kIx,
dalším	další	k2eAgFnPc3d1
pracovním	pracovní	k2eAgFnPc3d1
skupinám	skupina	k1gFnPc3
a	a	k8xC
organizaci	organizace	k1gFnSc3
jako	jako	k8xS,k8xC
celku	celek	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mabey	Mabeum	k1gNnPc7
a	a	k8xC
Pugh	Pugh	k1gInSc4
jmenují	jmenovat	k5eAaImIp3nP,k5eAaBmIp3nP
pět	pět	k4xCc4
charakteristických	charakteristický	k2eAgInPc2d1
rysů	rys	k1gInPc2
OD	od	k7c2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
OD	od	k7c2
je	být	k5eAaImIp3nS
široce	široko	k6eAd1
založený	založený	k2eAgInSc4d1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgInSc4d1
<g/>
,	,	kIx,
střednědobý	střednědobý	k2eAgInSc4d1
až	až	k8xS
dlouhodobý	dlouhodobý	k2eAgInSc4d1
přístup	přístup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
OD	od	k7c2
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
znalostech	znalost	k1gFnPc6
a	a	k8xC
metodách	metoda	k1gFnPc6
věd	věda	k1gFnPc2
o	o	k7c6
chování	chování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
OD	od	k7c2
je	být	k5eAaImIp3nS
orientovaný	orientovaný	k2eAgInSc1d1
na	na	k7c4
proces	proces	k1gInSc4
(	(	kIx(
<g/>
ne	ne	k9
na	na	k7c4
cíle	cíl	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
OD	od	k7c2
vyžaduje	vyžadovat	k5eAaImIp3nS
moderování	moderování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
OD	od	k7c2
je	být	k5eAaImIp3nS
participativní	participativní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Výsledkem	výsledek	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
organizační	organizační	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
znamená	znamenat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
lidského	lidský	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
nejen	nejen	k6eAd1
snížení	snížení	k1gNnSc1
investic	investice	k1gFnPc2
do	do	k7c2
pracovních	pracovní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
OE	OE	kA
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ve	v	k7c6
velkých	velký	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc6d1
správě	správa	k1gFnSc6
<g/>
,	,	kIx,
církvích	církev	k1gFnPc6
<g/>
,	,	kIx,
sociálních	sociální	k2eAgFnPc6d1
institucích	instituce	k1gFnPc6
a	a	k8xC
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
zákonitosti	zákonitost	k1gFnPc1
fungování	fungování	k1gNnSc2
sociálních	sociální	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
a	a	k8xC
při	při	k7c6
navrhování	navrhování	k1gNnSc6
systémů	systém	k1gInPc2
organizace	organizace	k1gFnSc2
se	se	k3xPyFc4
zohledňují	zohledňovat	k5eAaImIp3nP
zájmy	zájem	k1gInPc1
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Organisationsentwicklung	Organisationsentwicklunga	k1gFnPc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
</s>
<s>
P.	P.	kA
Rowlandson	Rowlandson	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
The	The	k1gFnSc1
oddity	oddita	k1gFnSc2
of	of	k?
OD	od	k7c2
<g/>
,	,	kIx,
Management	management	k1gInSc1
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
November	November	k1gMnSc1
<g/>
,	,	kIx,
pp	pp	k?
<g/>
.	.	kIx.
91	#num#	k4
<g/>
-	-	kIx~
<g/>
93	#num#	k4
<g/>
;	;	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
in	in	k?
Christopher	Christophra	k1gFnPc2
Mabey	Mabe	k2eAgFnPc1d1
<g/>
,	,	kIx,
and	and	k?
Derek	Derek	k1gMnSc1
S.	S.	kA
Pugh	Pugh	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Strategies	Strategiesa	k1gFnPc2
for	forum	k1gNnPc2
Managing	Managing	k1gInSc1
Complex	Complex	k1gInSc4
Change	change	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gFnSc2
Open	Opena	k1gFnPc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Milton	Milton	k1gInSc4
Keynes	Keynesa	k1gFnPc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
0-7492-9518-X	0-7492-9518-X	k4
</s>
<s>
↑	↑	k?
</s>
<s>
C.	C.	kA
Bowman	Bowman	k1gMnSc1
and	and	k?
D.	D.	kA
Asch	Asch	k1gInSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
Strategic	Strategice	k1gFnPc2
Management	management	k1gInSc1
<g/>
,	,	kIx,
Macimillan	Macimillan	k1gInSc1
<g/>
,	,	kIx,
p.	p.	k?
219	#num#	k4
<g/>
;	;	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
in	in	k?
Christopher	Christophra	k1gFnPc2
Mabey	Mabe	k2eAgFnPc1d1
<g/>
,	,	kIx,
and	and	k?
Derek	Derek	k1gMnSc1
S.	S.	kA
Pugh	Pugh	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Strategies	Strategiesa	k1gFnPc2
for	forum	k1gNnPc2
Managing	Managing	k1gInSc1
Complex	Complex	k1gInSc4
Change	change	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gFnSc2
Open	Opena	k1gFnPc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Milton	Milton	k1gInSc4
Keynes	Keynesa	k1gFnPc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
0-7492-9518-X	0-7492-9518-X	k4
</s>
<s>
↑	↑	k?
</s>
<s>
Christopher	Christophra	k1gFnPc2
Mabey	Mabea	k1gFnSc2
<g/>
,	,	kIx,
and	and	k?
Derek	Derek	k1gMnSc1
S.	S.	kA
Pugh	Pugh	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
Strategies	Strategiesa	k1gFnPc2
for	forum	k1gNnPc2
Managing	Managing	k1gInSc1
Complex	Complex	k1gInSc4
Change	change	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gFnSc2
Open	Opena	k1gFnPc2
University	universita	k1gFnSc2
<g/>
,	,	kIx,
Milton	Milton	k1gInSc4
Keynes	Keynesa	k1gFnPc2
<g/>
,	,	kIx,
ISBN	ISBN	kA
0-7492-9518-X	0-7492-9518-X	k4
</s>
<s>
↑	↑	k?
</s>
<s>
Ulrike	Ulrike	k1gFnSc1
Kipman	Kipman	k1gMnSc1
<g/>
;	;	kIx,
Organisationsentwicklung	Organisationsentwicklung	k1gMnSc1
und	und	k?
Personalmanagement	Personalmanagement	k1gMnSc1
unter	unter	k1gMnSc1
besonderer	besonderer	k1gMnSc1
Berücksichtigung	Berücksichtigung	k1gMnSc1
der	drát	k5eAaImRp2nS
Potenzialanalyse	Potenzialanalysa	k1gFnSc3
<g/>
;	;	kIx,
Dissertation	Dissertation	k1gInSc1
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Grin	Grin	k1gMnSc1
Verlag	Verlag	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
173	#num#	k4
<g/>
f.	f.	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
