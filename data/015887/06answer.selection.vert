<s>
Organizační	organizační	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
organization	organization	k1gInSc1
development	development	k1gInSc1
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
OD	od	k7c2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
koncept	koncept	k1gInSc1
a	a	k8xC
metodologie	metodologie	k1gFnSc1
z	z	k7c2
oblasti	oblast	k1gFnSc2
teorie	teorie	k1gFnSc2
managementu	management	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
uskutečňování	uskutečňování	k1gNnSc2
plánovaných	plánovaný	k2eAgFnPc2d1
sociálních	sociální	k2eAgFnPc2d1
změn	změna	k1gFnPc2
v	v	k7c6
organizacích	organizace	k1gFnPc6
<g/>
.	.	kIx.
</s>