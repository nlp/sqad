<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Amálie	Amálie	k1gFnSc1	Amálie
Evženie	Evženie	k1gFnSc1	Evženie
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
Amalie	Amalie	k1gFnSc2	Amalie
Eugenie	Eugenie	k1gFnSc2	Eugenie
<g/>
,	,	kIx,	,
Herzogin	Herzogin	k2eAgInSc1d1	Herzogin
in	in	k?	in
Bayern	Bayern	k1gInSc1	Bayern
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
bavorská	bavorský	k2eAgFnSc1d1	bavorská
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Wittelsbachů	Wittelsbach	k1gInPc2	Wittelsbach
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Josefem	Josef	k1gMnSc7	Josef
rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
uherská	uherský	k2eAgFnSc1d1	uherská
královna	královna	k1gFnSc1	královna
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Erzsébet	Erzsébeta	k1gFnPc2	Erzsébeta
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Wittelsbach	Wittelsbach	k1gMnSc1	Wittelsbach
Erzsébet	Erzsébeta	k1gFnPc2	Erzsébeta
magyar	magyar	k1gMnSc1	magyar
királyné	királyná	k1gFnSc2	királyná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
v	v	k7c6	v
Mnichově	mnichův	k2eAgFnSc6d1	Mnichova
vévodkyni	vévodkyně	k1gFnSc6	vévodkyně
Ludovice	Ludovice	k1gFnSc2	Ludovice
<g/>
,	,	kIx,	,
dceři	dcera	k1gFnSc3	dcera
bavorského	bavorský	k2eAgMnSc2d1	bavorský
krále	král	k1gMnSc2	král
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
manželovi	manžel	k1gMnSc3	manžel
<g/>
,	,	kIx,	,
bavorskému	bavorský	k2eAgMnSc3d1	bavorský
vévodovi	vévoda	k1gMnSc3	vévoda
Maxmiliánu	Maxmilián	k1gMnSc3	Maxmilián
Josefovi	Josef	k1gMnSc3	Josef
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
větve	větev	k1gFnSc2	větev
rodu	rod	k1gInSc2	rod
Wittelsbachů	Wittelsbach	k1gInPc2	Wittelsbach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
německém	německý	k2eAgNnSc6d1	německé
jméně	jméno	k1gNnSc6	jméno
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc1	výraz
in	in	k?	in
Bayern	Bayern	k1gInSc1	Bayern
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
von	von	k1gInSc1	von
Bayern	Bayern	k1gInSc1	Bayern
(	(	kIx(	(
<g/>
Herzog	Herzog	k1gMnSc1	Herzog
Maxmilian	Maxmiliana	k1gFnPc2	Maxmiliana
Josef	Josef	k1gMnSc1	Josef
in	in	k?	in
Bayern	Bayern	k1gInSc1	Bayern
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc4	manželství
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
dítětem	dítě	k1gNnSc7	dítě
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
dokonce	dokonce	k9	dokonce
už	už	k6eAd1	už
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
zoubkem	zoubek	k1gInSc7	zoubek
a	a	k8xC	a
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
předzvěst	předzvěst	k1gFnSc4	předzvěst
štěstí	štěstí	k1gNnSc2	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
staršími	starý	k2eAgMnPc7d2	starší
sourozenci	sourozenec	k1gMnPc7	sourozenec
byli	být	k5eAaImAgMnP	být
Ludvík	Ludvík	k1gMnSc1	Ludvík
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vilém	Vilém	k1gMnSc1	Vilém
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
a	a	k8xC	a
zemř	zemř	k1gMnSc1	zemř
<g/>
.	.	kIx.	.
1832	[number]	k4	1832
<g/>
)	)	kIx)	)
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Néné	Néná	k1gFnSc2	Néná
(	(	kIx(	(
<g/>
1834	[number]	k4	1834
<g/>
–	–	k?	–
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Alžbětě	Alžběta	k1gFnSc6	Alžběta
ještě	ještě	k6eAd1	ještě
následovalo	následovat	k5eAaImAgNnS	následovat
pět	pět	k4xCc1	pět
dalších	další	k2eAgMnPc2d1	další
sourozenců	sourozenec	k1gMnPc2	sourozenec
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Teodor	Teodor	k1gMnSc1	Teodor
zvaný	zvaný	k2eAgInSc1d1	zvaný
Gackel	Gackel	k1gInSc1	Gackel
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alžbětin	Alžbětin	k2eAgMnSc1d1	Alžbětin
nejmilejší	milý	k2eAgMnSc1d3	nejmilejší
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1841	[number]	k4	1841
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Matylda	Matylda	k1gFnSc1	Matylda
(	(	kIx(	(
<g/>
1843	[number]	k4	1843
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Vrabčák	vrabčák	k1gMnSc1	vrabčák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sofie	Sofie	k1gFnSc1	Sofie
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
–	–	k?	–
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
a	a	k8xC	a
Max	Max	k1gMnSc1	Max
Emanuel	Emanuel	k1gMnSc1	Emanuel
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Mapperl	Mapperl	k1gInSc1	Mapperl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezstarostné	bezstarostný	k2eAgNnSc4d1	bezstarostné
dětství	dětství	k1gNnSc4	dětství
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
především	především	k6eAd1	především
na	na	k7c6	na
milovaném	milovaný	k2eAgNnSc6d1	milované
venkovském	venkovský	k2eAgNnSc6d1	venkovské
sídle	sídlo	k1gNnSc6	sídlo
v	v	k7c6	v
Possenhofenu	Possenhofen	k1gInSc6	Possenhofen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
spíše	spíše	k9	spíše
než	než	k8xS	než
dvorní	dvorní	k2eAgFnSc3d1	dvorní
etiketě	etiketa	k1gFnSc3	etiketa
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc3	francouzština
naučila	naučit	k5eAaPmAgFnS	naučit
perfektně	perfektně	k6eAd1	perfektně
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
nebo	nebo	k8xC	nebo
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
nejoblíbenějším	oblíbený	k2eAgNnSc7d3	nejoblíbenější
dítětem	dítě	k1gNnSc7	dítě
volnomyšlenkářského	volnomyšlenkářský	k2eAgMnSc2d1	volnomyšlenkářský
vévody	vévoda	k1gMnSc2	vévoda
a	a	k8xC	a
také	také	k9	také
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
zdědila	zdědit	k5eAaPmAgFnS	zdědit
velký	velký	k2eAgInSc4d1	velký
cit	cit	k1gInSc4	cit
pro	pro	k7c4	pro
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
jednu	jeden	k4xCgFnSc4	jeden
důležitou	důležitý	k2eAgFnSc4d1	důležitá
vlastnost	vlastnost	k1gFnSc4	vlastnost
měli	mít	k5eAaImAgMnP	mít
společnou	společný	k2eAgFnSc4d1	společná
–	–	k?	–
byla	být	k5eAaImAgNnP	být
jí	on	k3xPp3gFnSc3	on
stálá	stálý	k2eAgFnSc1d1	stálá
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
a	a	k8xC	a
volnosti	volnost	k1gFnSc6	volnost
<g/>
.	.	kIx.	.
</s>
<s>
Třiadvacetiletý	třiadvacetiletý	k2eAgMnSc1d1	třiadvacetiletý
císař	císař	k1gMnSc1	císař
měl	mít	k5eAaImAgMnS	mít
přání	přání	k1gNnSc4	přání
se	se	k3xPyFc4	se
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
oženit	oženit	k5eAaPmF	oženit
a	a	k8xC	a
založit	založit	k5eAaPmF	založit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
po	po	k7c6	po
nevěstě	nevěsta	k1gFnSc6	nevěsta
moc	moc	k6eAd1	moc
rozhlížet	rozhlížet	k5eAaImF	rozhlížet
nemohl	moct	k5eNaImAgMnS	moct
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gNnSc3	on
seznámení	seznámení	k1gNnSc3	seznámení
s	s	k7c7	s
katolickými	katolický	k2eAgFnPc7d1	katolická
princeznami	princezna	k1gFnPc7	princezna
pomohla	pomoct	k5eAaPmAgFnS	pomoct
připravit	připravit	k5eAaPmF	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Činila	činit	k5eAaImAgFnS	činit
tak	tak	k6eAd1	tak
ráda	rád	k2eAgFnSc1d1	ráda
a	a	k8xC	a
diskrétně	diskrétně	k6eAd1	diskrétně
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnPc1	setkání
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgNnP	odehrávat
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
soukromí	soukromí	k1gNnSc1	soukromí
a	a	k8xC	a
dotyčné	dotyčný	k2eAgFnPc1d1	dotyčná
dámy	dáma	k1gFnPc1	dáma
přijížděly	přijíždět	k5eAaImAgFnP	přijíždět
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
netušily	tušit	k5eNaImAgInP	tušit
<g/>
,	,	kIx,	,
oč	oč	k6eAd1	oč
vlastně	vlastně	k9	vlastně
jde	jít	k5eAaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
poznal	poznat	k5eAaPmAgMnS	poznat
několik	několik	k4yIc4	několik
dam	dáma	k1gFnPc2	dáma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádná	žádný	k3yNgFnSc1	žádný
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dost	dost	k6eAd1	dost
nelíbila	líbit	k5eNaImAgFnS	líbit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
1853	[number]	k4	1853
pozvala	pozvat	k5eAaPmAgFnS	pozvat
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Žofie	Žofie	k1gFnSc1	Žofie
do	do	k7c2	do
Ischlu	Ischl	k1gInSc2	Ischl
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
císařových	císařův	k2eAgFnPc2d1	císařova
narozenin	narozeniny	k1gFnPc2	narozeniny
svou	svůj	k3xOyFgFnSc4	svůj
sestru	sestra	k1gFnSc4	sestra
Luisu	Luisa	k1gFnSc4	Luisa
se	s	k7c7	s
čtyřmi	čtyři	k4xCgFnPc7	čtyři
nejstaršími	starý	k2eAgFnPc7d3	nejstarší
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
omyl	omyl	k1gInSc1	omyl
<g/>
,	,	kIx,	,
že	že	k8xS	že
Žofie	Žofie	k1gFnSc1	Žofie
měla	mít	k5eAaImAgFnS	mít
raději	rád	k6eAd2	rád
Helenu	Helena	k1gFnSc4	Helena
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
připravovala	připravovat	k5eAaImAgFnS	připravovat
její	její	k3xOp3gFnPc4	její
zásnuby	zásnuba	k1gFnPc4	zásnuba
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Josefem	Josef	k1gMnSc7	Josef
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyvrácen	vyvrácen	k2eAgMnSc1d1	vyvrácen
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
strohém	strohý	k2eAgInSc6d1	strohý
ceremoniálním	ceremoniální	k2eAgInSc6d1	ceremoniální
tónu	tón	k1gInSc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
Žofie	Žofie	k1gFnSc1	Žofie
příbuzné	příbuzná	k1gFnSc2	příbuzná
uvítala	uvítat	k5eAaPmAgFnS	uvítat
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
a	a	k8xC	a
sledovala	sledovat	k5eAaImAgFnS	sledovat
úpravu	úprava	k1gFnSc4	úprava
obou	dva	k4xCgFnPc2	dva
neteří	neteř	k1gFnPc2	neteř
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
před	před	k7c7	před
pěti	pět	k4xCc7	pět
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
okouzlena	okouzlen	k2eAgFnSc1d1	okouzlena
půvabem	půvab	k1gInSc7	půvab
mladší	mladý	k2eAgMnPc1d2	mladší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Zavedla	zavést	k5eAaPmAgFnS	zavést
příbuzné	příbuzný	k1gMnPc4	příbuzný
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
toaletním	toaletní	k2eAgInSc6d1	toaletní
pokoji	pokoj	k1gInSc6	pokoj
setkali	setkat	k5eAaPmAgMnP	setkat
hosté	host	k1gMnPc1	host
s	s	k7c7	s
přítomnými	přítomný	k1gMnPc7	přítomný
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
spontánně	spontánně	k6eAd1	spontánně
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Rodiče	rodič	k1gMnPc1	rodič
chtěli	chtít	k5eAaImAgMnP	chtít
dopřát	dopřát	k5eAaPmF	dopřát
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
soukromí	soukromý	k2eAgMnPc1d1	soukromý
svým	svůj	k3xOyFgFnPc3	svůj
zamilovaným	zamilovaný	k2eAgFnPc3d1	zamilovaná
dětem	dítě	k1gFnPc3	dítě
a	a	k8xC	a
na	na	k7c4	na
večeři	večeře	k1gFnSc4	večeře
oddělila	oddělit	k5eAaPmAgFnS	oddělit
Žofie	Žofie	k1gFnSc1	Žofie
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnPc4	on
nesledovali	sledovat	k5eNaImAgMnP	sledovat
a	a	k8xC	a
neposlouchali	poslouchat	k5eNaImAgMnP	poslouchat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
příbuzní	příbuzný	k1gMnPc1	příbuzný
přihlíželi	přihlížet	k5eAaImAgMnP	přihlížet
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
lásce	láska	k1gFnSc3	láska
mladého	mladý	k2eAgInSc2d1	mladý
páru	pár	k1gInSc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
Arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Žofie	Žofie	k1gFnSc1	Žofie
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
Maxmiliánovi	Maxmilián	k1gMnSc3	Maxmilián
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
setkání	setkání	k1gNnSc4	setkání
v	v	k7c6	v
Ischlu	Ischl	k1gInSc6	Ischl
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
gratulaci	gratulace	k1gFnSc4	gratulace
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Láska	láska	k1gFnSc1	láska
snoubenců	snoubenec	k1gMnPc2	snoubenec
roste	růst	k5eAaImIp3nS	růst
každou	každý	k3xTgFnSc4	každý
hodinu	hodina	k1gFnSc4	hodina
takovou	takový	k3xDgFnSc7	takový
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
že	že	k8xS	že
bys	by	kYmCp2nS	by
tomu	ten	k3xDgNnSc3	ten
nevěřil	věřit	k5eNaImAgInS	věřit
<g/>
!	!	kIx.	!
</s>
<s>
Jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jsem	být	k5eAaImIp1nS	být
seděla	sedět	k5eAaImAgFnS	sedět
s	s	k7c7	s
Luisou	Luisa	k1gFnSc7	Luisa
ve	v	k7c6	v
vedlejším	vedlejší	k2eAgInSc6d1	vedlejší
pokoji	pokoj	k1gInSc6	pokoj
na	na	k7c6	na
sofa	sofa	k1gNnSc6	sofa
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
náhle	náhle	k6eAd1	náhle
<g/>
:	:	kIx,	:
'	'	kIx"	'
<g/>
Ale	ale	k9	ale
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
uvnitř	uvnitř	k6eAd1	uvnitř
zachází	zacházet	k5eAaImIp3nS	zacházet
moc	moc	k6eAd1	moc
daleko	daleko	k6eAd1	daleko
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
'	'	kIx"	'
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
tam	tam	k6eAd1	tam
spolu	spolu	k6eAd1	spolu
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
objetí	objetí	k1gNnSc6	objetí
<g/>
,	,	kIx,	,
zaměstnáni	zaměstnán	k2eAgMnPc1d1	zaměstnán
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
polibkem	polibek	k1gInSc7	polibek
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
provdala	provdat	k5eAaPmAgFnS	provdat
Elisa	Elisa	k1gFnSc1	Elisa
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
jí	jíst	k5eAaImIp3nS	jíst
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
říkala	říkat	k5eAaImAgFnS	říkat
teta	teta	k1gFnSc1	teta
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
)	)	kIx)	)
za	za	k7c2	za
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
bratrancem	bratranec	k1gMnSc7	bratranec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
císařova	císařův	k2eAgFnSc1d1	císařova
matka	matka	k1gFnSc1	matka
–	–	k?	–
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Žofie	Žofie	k1gFnSc1	Žofie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sestra	sestra	k1gFnSc1	sestra
Alžbětiny	Alžbětin	k2eAgFnSc2d1	Alžbětina
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zásnubám	zásnuba	k1gFnPc3	zásnuba
dostali	dostat	k5eAaPmAgMnP	dostat
od	od	k7c2	od
arcivévodkyně	arcivévodkyně	k1gFnSc2	arcivévodkyně
Žofie	Žofie	k1gFnSc2	Žofie
jako	jako	k8xS	jako
dárek	dárek	k1gInSc4	dárek
Císařskou	císařský	k2eAgFnSc4d1	císařská
vilu	vila	k1gFnSc4	vila
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Ischlu	Ischl	k1gInSc2	Ischl
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
přistavěna	přistavěn	k2eAgFnSc1d1	přistavěna
ještě	ještě	k6eAd1	ještě
dvě	dva	k4xCgNnPc4	dva
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
stavba	stavba	k1gFnSc1	stavba
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
E	E	kA	E
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zasnoubení	zasnoubení	k1gNnSc6	zasnoubení
ovšem	ovšem	k9	ovšem
tehdy	tehdy	k6eAd1	tehdy
velmi	velmi	k6eAd1	velmi
mladá	mladý	k2eAgFnSc1d1	mladá
Alžběta	Alžběta	k1gFnSc1	Alžběta
pocítila	pocítit	k5eAaPmAgFnS	pocítit
tlak	tlak	k1gInSc4	tlak
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
období	období	k1gNnSc1	období
velkého	velký	k2eAgNnSc2d1	velké
studia	studio	k1gNnSc2	studio
historie	historie	k1gFnSc2	historie
nových	nový	k2eAgFnPc2d1	nová
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
jazyků	jazyk	k1gInPc2	jazyk
"	"	kIx"	"
<g/>
čisté	čistý	k2eAgFnPc4d1	čistá
<g/>
"	"	kIx"	"
němčiny	němčina	k1gFnPc4	němčina
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
bez	bez	k7c2	bez
bavorského	bavorský	k2eAgInSc2d1	bavorský
dialektu	dialekt	k1gInSc2	dialekt
<g/>
)	)	kIx)	)
a	a	k8xC	a
přísné	přísný	k2eAgFnPc1d1	přísná
dvorské	dvorský	k2eAgFnPc1d1	dvorská
etikety	etiketa	k1gFnPc1	etiketa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
rodičů	rodič	k1gMnPc2	rodič
dostala	dostat	k5eAaPmAgFnS	dostat
výbavu	výbava	k1gFnSc4	výbava
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Šila	šít	k5eAaImAgFnS	šít
se	se	k3xPyFc4	se
výbava	výbava	k1gFnSc1	výbava
nevěsty	nevěsta	k1gFnSc2	nevěsta
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Trousseau	Trousseaus	k1gInSc2	Trousseaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
opatřilo	opatřit	k5eAaPmAgNnS	opatřit
čtrnáct	čtrnáct	k4xCc1	čtrnáct
tuctů	tucet	k1gInPc2	tucet
punčochových	punčochový	k2eAgInPc2d1	punčochový
párů	pár	k1gInPc2	pár
(	(	kIx(	(
<g/>
168	[number]	k4	168
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvacet	dvacet	k4xCc4	dvacet
párů	pár	k1gInPc2	pár
rukavic	rukavice	k1gFnPc2	rukavice
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
párů	pár	k1gInPc2	pár
vysokých	vysoký	k2eAgFnPc2d1	vysoká
kožených	kožený	k2eAgFnPc2d1	kožená
bot	bota	k1gFnPc2	bota
a	a	k8xC	a
sto	sto	k4xCgNnSc1	sto
třináct	třináct	k4xCc1	třináct
párů	pár	k1gInPc2	pár
střevíců	střevíc	k1gInPc2	střevíc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařovna	císařovna	k1gFnSc1	císařovna
si	se	k3xPyFc3	se
směla	smět	k5eAaImAgFnS	smět
vzít	vzít	k5eAaPmF	vzít
tytéž	týž	k3xTgFnPc4	týž
boty	bota	k1gFnPc4	bota
a	a	k8xC	a
rukavice	rukavice	k1gFnPc4	rukavice
pouze	pouze	k6eAd1	pouze
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
patřily	patřit	k5eAaImAgFnP	patřit
komorným	komorná	k1gFnPc3	komorná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Alžběta	Alžběta	k1gFnSc1	Alžběta
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
obtěžovaly	obtěžovat	k5eAaImAgInP	obtěžovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nošení	nošení	k1gNnSc1	nošení
stále	stále	k6eAd1	stále
nových	nový	k2eAgFnPc2d1	nová
bot	bota	k1gFnPc2	bota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
to	ten	k3xDgNnSc1	ten
neprospívalo	prospívat	k5eNaImAgNnS	prospívat
jejímu	její	k3xOp3gInSc3	její
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Svatba	svatba	k1gFnSc1	svatba
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1854	[number]	k4	1854
v	v	k7c4	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
hodin	hodina	k1gFnPc2	hodina
večer	večer	k6eAd1	večer
v	v	k7c6	v
augustiniánském	augustiniánský	k2eAgInSc6d1	augustiniánský
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
osvícen	osvítit	k5eAaPmNgInS	osvítit
15	[number]	k4	15
tisíci	tisíc	k4xCgInPc7	tisíc
svíčkami	svíčka	k1gFnPc7	svíčka
a	a	k8xC	a
vyzdoben	vyzdobit	k5eAaPmNgInS	vyzdobit
červeným	červený	k2eAgInSc7d1	červený
sametem	samet	k1gInSc7	samet
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
na	na	k7c4	na
sobě	se	k3xPyFc3	se
měla	mít	k5eAaImAgNnP	mít
bílé	bílý	k2eAgFnPc1d1	bílá
stříbrem	stříbro	k1gNnSc7	stříbro
vyšívané	vyšívaný	k2eAgFnSc2d1	vyšívaná
šaty	šata	k1gFnSc2	šata
a	a	k8xC	a
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
diadém	diadém	k1gInSc4	diadém
své	svůj	k3xOyFgFnSc2	svůj
tchyně	tchyně	k1gFnSc2	tchyně
arcivévodkyně	arcivévodkyně	k1gFnSc2	arcivévodkyně
Žofie	Žofie	k1gFnSc2	Žofie
<g/>
.	.	kIx.	.
</s>
<s>
Obřad	obřad	k1gInSc4	obřad
vedl	vést	k5eAaImAgMnS	vést
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
Joseph	Joseph	k1gMnSc1	Joseph
Othmar	Othmar	k1gMnSc1	Othmar
von	von	k1gInSc4	von
Rauscher	Rauschra	k1gFnPc2	Rauschra
<g/>
,	,	kIx,	,
asistovalo	asistovat	k5eAaImAgNnS	asistovat
mu	on	k3xPp3gMnSc3	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
prelátů	prelát	k1gMnPc2	prelát
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
O	o	k7c4	o
25	[number]	k4	25
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
slavil	slavit	k5eAaImAgInS	slavit
císařský	císařský	k2eAgInSc4d1	císařský
pár	pár	k4xCyI	pár
svou	svůj	k3xOyFgFnSc4	svůj
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
svatbu	svatba	k1gFnSc4	svatba
ve	v	k7c6	v
Votivním	votivní	k2eAgInSc6d1	votivní
kostele	kostel	k1gInSc6	kostel
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
právě	právě	k6eAd1	právě
dokončen	dokončit	k5eAaPmNgInS	dokončit
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Císař	Císař	k1gMnSc1	Císař
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgMnS	projevovat
jako	jako	k9	jako
velkorysý	velkorysý	k2eAgMnSc1d1	velkorysý
choť	choť	k1gMnSc1	choť
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1854	[number]	k4	1854
dostala	dostat	k5eAaPmAgFnS	dostat
Alžběta	Alžběta	k1gFnSc1	Alžběta
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
50	[number]	k4	50
tisíc	tisíc	k4xCgInSc4	tisíc
zlatých	zlatý	k2eAgInPc2d1	zlatý
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
císař	císař	k1gMnSc1	císař
"	"	kIx"	"
<g/>
dorovnal	dorovnat	k5eAaPmAgInS	dorovnat
<g/>
"	"	kIx"	"
o	o	k7c4	o
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dostala	dostat	k5eAaPmAgFnS	dostat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dar	dar	k1gInSc4	dar
svatebního	svatební	k2eAgNnSc2d1	svatební
rána	ráno	k1gNnSc2	ráno
<g/>
"	"	kIx"	"
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
dukátů	dukát	k1gInPc2	dukát
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zvyku	zvyk	k1gInSc2	zvyk
za	za	k7c4	za
ztracené	ztracený	k2eAgNnSc4d1	ztracené
panenství	panenství	k1gNnSc4	panenství
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
apanáž	apanáž	k1gFnSc4	apanáž
bylo	být	k5eAaImAgNnS	být
stanoveno	stanoven	k2eAgNnSc1d1	stanoveno
dalších	další	k2eAgInPc2d1	další
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
výdaje	výdaj	k1gInPc4	výdaj
jako	jako	k8xC	jako
stolování	stolování	k1gNnSc4	stolování
<g/>
,	,	kIx,	,
prádlo	prádlo	k1gNnSc4	prádlo
<g/>
,	,	kIx,	,
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
obživu	obživa	k1gFnSc4	obživa
<g/>
,	,	kIx,	,
personál	personál	k1gInSc1	personál
a	a	k8xC	a
cestování	cestování	k1gNnSc1	cestování
nese	nést	k5eAaImIp3nS	nést
Jeho	jeho	k3xOp3gFnSc4	jeho
Výsost	výsost	k1gFnSc4	výsost
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
jazyky	jazyk	k1gInPc4	jazyk
převyšovala	převyšovat	k5eAaImAgFnS	převyšovat
císařovna	císařovna	k1gFnSc1	císařovna
dámy	dáma	k1gFnSc2	dáma
z	z	k7c2	z
vysokých	vysoký	k2eAgInPc2d1	vysoký
aristokratických	aristokratický	k2eAgInPc2d1	aristokratický
kruhů	kruh	k1gInPc2	kruh
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
němčiny	němčina	k1gFnSc2	němčina
ovládala	ovládat	k5eAaImAgFnS	ovládat
maďarštinu	maďarština	k1gFnSc4	maďarština
<g/>
,	,	kIx,	,
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
polštinu	polština	k1gFnSc4	polština
<g/>
,	,	kIx,	,
rumunštinu	rumunština	k1gFnSc4	rumunština
<g/>
,	,	kIx,	,
italštinu	italština	k1gFnSc4	italština
<g/>
,	,	kIx,	,
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
latinu	latina	k1gFnSc4	latina
a	a	k8xC	a
staro-	staro-	k?	staro-
a	a	k8xC	a
novořečtinu	novořečtina	k1gFnSc4	novořečtina
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
latinu	latina	k1gFnSc4	latina
se	se	k3xPyFc4	se
učila	učít	k5eAaPmAgFnS	učít
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
jazyků	jazyk	k1gInPc2	jazyk
si	se	k3xPyFc3	se
osvojila	osvojit	k5eAaPmAgFnS	osvojit
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůkladněji	důkladně	k6eAd3	důkladně
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
studiem	studio	k1gNnSc7	studio
maďarštiny	maďarština	k1gFnSc2	maďarština
a	a	k8xC	a
obou	dva	k4xCgInPc2	dva
řeckých	řecký	k2eAgInPc2d1	řecký
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
plynně	plynně	k6eAd1	plynně
ovládala	ovládat	k5eAaImAgFnS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
životě	život	k1gInSc6	život
Alžběty	Alžběta	k1gFnSc2	Alžběta
hrály	hrát	k5eAaImAgFnP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
některé	některý	k3yIgFnSc2	některý
dvorní	dvorní	k2eAgFnSc2d1	dvorní
dámy	dáma	k1gFnSc2	dáma
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
spřátelila	spřátelit	k5eAaPmAgFnS	spřátelit
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
hraběnku	hraběnka	k1gFnSc4	hraběnka
Žofii	Žofie	k1gFnSc4	Žofie
Esterházyovou	Esterházyový	k2eAgFnSc7d1	Esterházyový
rozenou	rozený	k2eAgFnSc7d1	rozená
princeznou	princezna	k1gFnSc7	princezna
z	z	k7c2	z
Lichtenštejnska	Lichtenštejnsko	k1gNnSc2	Lichtenštejnsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
přidělena	přidělit	k5eAaPmNgFnS	přidělit
šestnáctileté	šestnáctiletý	k2eAgFnSc3d1	šestnáctiletá
Alžbětě	Alžběta	k1gFnSc3	Alžběta
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hofmistryně	hofmistryně	k1gFnSc1	hofmistryně
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jí	on	k3xPp3gFnSc3	on
tehdy	tehdy	k6eAd1	tehdy
šestapadesát	šestapadesát	k4xCc1	šestapadesát
<g/>
,	,	kIx,	,
již	již	k9	již
necelých	celý	k2eNgNnPc2d1	necelé
dvacet	dvacet	k4xCc1	dvacet
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
a	a	k8xC	a
bezdětná	bezdětný	k2eAgFnSc1d1	bezdětná
<g/>
,	,	kIx,	,
obětovala	obětovat	k5eAaBmAgFnS	obětovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
císařské	císařský	k2eAgFnSc2d1	císařská
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
horlivě	horlivě	k6eAd1	horlivě
zastávala	zastávat	k5eAaImAgFnS	zastávat
dvorní	dvorní	k2eAgInSc4d1	dvorní
ceremoniál	ceremoniál	k1gInSc4	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
problematický	problematický	k2eAgInSc1d1	problematický
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
císařovna	císařovna	k1gFnSc1	císařovna
intenzivně	intenzivně	k6eAd1	intenzivně
zabývala	zabývat	k5eAaImAgFnS	zabývat
studiem	studio	k1gNnSc7	studio
maďarštiny	maďarština	k1gFnSc2	maďarština
a	a	k8xC	a
přála	přát	k5eAaImAgFnS	přát
si	se	k3xPyFc3	se
mít	mít	k5eAaImF	mít
na	na	k7c6	na
blízku	blízko	k1gNnSc6	blízko
dámu	dáma	k1gFnSc4	dáma
na	na	k7c4	na
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
předčítání	předčítání	k1gNnSc4	předčítání
<g/>
.	.	kIx.	.
</s>
<s>
Vybrala	vybrat	k5eAaPmAgFnS	vybrat
si	se	k3xPyFc3	se
maďarskou	maďarský	k2eAgFnSc4d1	maďarská
čtyřiadvacetiletou	čtyřiadvacetiletý	k2eAgFnSc4d1	čtyřiadvacetiletá
dámu	dáma	k1gFnSc4	dáma
z	z	k7c2	z
nižší	nízký	k2eAgFnSc2d2	nižší
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
Idu	Ida	k1gFnSc4	Ida
von	von	k1gInSc4	von
Ferenczyovou	Ferenczyová	k1gFnSc7	Ferenczyová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
znala	znát	k5eAaImAgFnS	znát
s	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
uherskými	uherský	k2eAgMnPc7d1	uherský
politiky	politik	k1gMnPc7	politik
(	(	kIx(	(
<g/>
Gyula	Gyul	k1gMnSc2	Gyul
hrabě	hrabě	k1gMnSc1	hrabě
Andrássy-uherský	Andrássyherský	k2eAgMnSc1d1	Andrássy-uherský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
Deák	Deák	k1gMnSc1	Deák
–	–	k?	–
uherský	uherský	k2eAgMnSc1d1	uherský
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
umírněných	umírněný	k2eAgMnPc2d1	umírněný
reformátorů	reformátor	k1gMnPc2	reformátor
<g/>
,	,	kIx,	,
liberál	liberál	k1gMnSc1	liberál
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
Alžběta	Alžběta	k1gFnSc1	Alžběta
protežovala	protežovat	k5eAaImAgFnS	protežovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jí	on	k3xPp3gFnSc7	on
svou	svůj	k3xOyFgFnSc7	svůj
bystrostí	bystrost	k1gFnSc7	bystrost
a	a	k8xC	a
přirozeností	přirozenost	k1gFnSc7	přirozenost
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
<g/>
.	.	kIx.	.
</s>
<s>
Ida	Ida	k1gFnSc1	Ida
se	se	k3xPyFc4	se
těšila	těšit	k5eAaImAgFnS	těšit
plné	plný	k2eAgFnSc3d1	plná
důvěře	důvěra	k1gFnSc3	důvěra
císařovny	císařovna	k1gFnSc2	císařovna
a	a	k8xC	a
směla	smět	k5eAaImAgFnS	smět
jí	jíst	k5eAaImIp3nS	jíst
dokonce	dokonce	k9	dokonce
tykat	tykat	k5eAaImF	tykat
<g/>
,	,	kIx,	,
v	v	k7c6	v
závěti	závěť	k1gFnSc6	závěť
jí	jíst	k5eAaImIp3nS	jíst
byla	být	k5eAaImAgFnS	být
odkázána	odkázat	k5eAaPmNgFnS	odkázat
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
renta	renta	k1gFnSc1	renta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
čtyř	čtyři	k4xCgInPc2	čtyři
tisíc	tisíc	k4xCgInPc2	tisíc
zlatých	zlatá	k1gFnPc2	zlatá
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
dostala	dostat	k5eAaPmAgFnS	dostat
cenný	cenný	k2eAgInSc4d1	cenný
šperk	šperk	k1gInSc4	šperk
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
srdce	srdce	k1gNnSc4	srdce
s	s	k7c7	s
drahokamy	drahokam	k1gInPc7	drahokam
v	v	k7c6	v
uherských	uherský	k2eAgFnPc6d1	uherská
barvách	barva	k1gFnPc6	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
císařovnině	císařovnin	k2eAgFnSc3d1	Císařovnina
suitě	suita	k1gFnSc3	suita
nová	nový	k2eAgFnSc1d1	nová
dvorní	dvorní	k2eAgFnSc1d1	dvorní
dáma	dáma	k1gFnSc1	dáma
<g/>
,	,	kIx,	,
hraběnka	hraběnka	k1gFnSc1	hraběnka
Marie	Marie	k1gFnSc1	Marie
Festeticsová	Festeticsová	k1gFnSc1	Festeticsová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
politický	politický	k2eAgInSc4d1	politický
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
uherským	uherský	k2eAgFnPc3d1	uherská
dámám	dáma	k1gFnPc3	dáma
patřila	patřit	k5eAaImAgFnS	patřit
hraběnka	hraběnka	k1gFnSc1	hraběnka
Sarolta	Sarolta	k1gFnSc1	Sarolta
Mailáthová	Mailáthová	k1gFnSc1	Mailáthová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
skvělé	skvělý	k2eAgNnSc4d1	skvělé
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
Alžbětu	Alžběta	k1gFnSc4	Alžběta
na	na	k7c6	na
jejích	její	k3xOp3gFnPc6	její
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
a	a	k8xC	a
rychlých	rychlý	k2eAgFnPc6d1	rychlá
túrách	túra	k1gFnPc6	túra
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dvorní	dvorní	k2eAgFnSc7d1	dvorní
dámou	dáma	k1gFnSc7	dáma
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
hraběnka	hraběnka	k1gFnSc1	hraběnka
Irma	Irma	k1gFnSc1	Irma
Sztárayová	Sztárayová	k1gFnSc1	Sztárayová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
v	v	k7c6	v
září	září	k1gNnSc6	září
1898	[number]	k4	1898
císařovnu	císařovna	k1gFnSc4	císařovna
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
poslední	poslední	k2eAgFnSc6d1	poslední
vycházce	vycházka	k1gFnSc6	vycházka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
císařovna	císařovna	k1gFnSc1	císařovna
zavražděna	zavražděn	k2eAgFnSc1d1	zavražděna
<g/>
.	.	kIx.	.
</s>
<s>
Rakušanka	Rakušanka	k1gFnSc1	Rakušanka
Františka	Františka	k1gFnSc1	Františka
Feifaliková	Feifalikový	k2eAgFnSc1d1	Feifalikový
byla	být	k5eAaImAgFnS	být
komorní	komorní	k2eAgFnSc7d1	komorní
kadeřnicí	kadeřnice	k1gFnSc7	kadeřnice
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
Alžbětě	Alžběta	k1gFnSc3	Alžběta
podobná	podobný	k2eAgFnSc1d1	podobná
a	a	k8xC	a
ovládala	ovládat	k5eAaImAgFnS	ovládat
její	její	k3xOp3gFnSc4	její
gestikulaci	gestikulace	k1gFnSc4	gestikulace
<g/>
,	,	kIx,	,
mohla	moct	k5eAaImAgFnS	moct
ji	on	k3xPp3gFnSc4	on
při	při	k7c6	při
oficiálních	oficiální	k2eAgNnPc6d1	oficiální
holdovacích	holdovací	k2eAgNnPc6d1	holdovací
aktech	akta	k1gNnPc6	akta
(	(	kIx(	(
<g/>
z	z	k7c2	z
balkonů	balkon	k1gInPc2	balkon
<g/>
,	,	kIx,	,
kočárů	kočár	k1gInPc2	kočár
a	a	k8xC	a
lodí	loď	k1gFnPc2	loď
<g/>
)	)	kIx)	)
zastupovat	zastupovat	k5eAaImF	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Františka	Františka	k1gFnSc1	Františka
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
porodní	porodní	k2eAgFnSc2d1	porodní
báby	bába	k1gFnSc2	bába
a	a	k8xC	a
vyučila	vyučit	k5eAaPmAgFnS	vyučit
se	se	k3xPyFc4	se
kadeřnicí	kadeřnice	k1gFnSc7	kadeřnice
<g/>
.	.	kIx.	.
</s>
<s>
Česala	česat	k5eAaImAgFnS	česat
slavné	slavný	k2eAgFnPc4d1	slavná
divadelní	divadelní	k2eAgFnPc4d1	divadelní
herečky	herečka	k1gFnPc4	herečka
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
dobré	dobrý	k2eAgFnSc3d1	dobrá
pověsti	pověst	k1gFnSc3	pověst
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
císařovna	císařovna	k1gFnSc1	císařovna
doslechla	doslechnout	k5eAaPmAgFnS	doslechnout
a	a	k8xC	a
přijala	přijmout	k5eAaPmAgFnS	přijmout
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
služby	služba	k1gFnSc2	služba
byla	být	k5eAaImAgFnS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
dvorní	dvorní	k2eAgFnSc7d1	dvorní
radovou	radová	k1gFnSc7	radová
a	a	k8xC	a
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
byla	být	k5eAaImAgFnS	být
povýšena	povýšit	k5eAaPmNgFnS	povýšit
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
svobodných	svobodný	k2eAgMnPc2d1	svobodný
pánů	pan	k1gMnPc2	pan
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
bylo	být	k5eAaImAgNnS	být
císařské	císařský	k2eAgNnSc1d1	císařské
manželství	manželství	k1gNnSc1	manželství
šťastné	šťastný	k2eAgNnSc1d1	šťastné
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
jejich	jejich	k3xOp3gInSc2	jejich
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
trpěla	trpět	k5eAaImAgFnS	trpět
idiosynkrazií	idiosynkrazie	k1gFnSc7	idiosynkrazie
(	(	kIx(	(
<g/>
přecetlivělostí	přecetlivělost	k1gFnSc7	přecetlivělost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nesnášela	snášet	k5eNaImAgFnS	snášet
neustálou	neustálý	k2eAgFnSc4d1	neustálá
pozornost	pozornost	k1gFnSc4	pozornost
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
manželka	manželka	k1gFnSc1	manželka
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
brzy	brzy	k6eAd1	brzy
nudit	nudit	k5eAaImF	nudit
<g/>
,	,	kIx,	,
když	když	k8xS	když
její	její	k3xOp3gMnSc1	její
choť	choť	k1gMnSc1	choť
odjel	odjet	k5eAaPmAgMnS	odjet
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
do	do	k7c2	do
Hofburgu	Hofburg	k1gInSc2	Hofburg
kvůli	kvůli	k7c3	kvůli
vyřizování	vyřizování	k1gNnSc3	vyřizování
státních	státní	k2eAgFnPc2d1	státní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
ani	ani	k8xC	ani
ne	ne	k9	ne
sedmnáctiletá	sedmnáctiletý	k2eAgFnSc1d1	sedmnáctiletá
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgFnS	cítit
opomíjená	opomíjený	k2eAgFnSc1d1	opomíjená
a	a	k8xC	a
opuštěná	opuštěný	k2eAgFnSc1d1	opuštěná
<g/>
.	.	kIx.	.
</s>
<s>
Vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
si	se	k3xPyFc3	se
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zapřáhnout	zapřáhnout	k5eAaPmF	zapřáhnout
kočár	kočár	k1gInSc4	kočár
a	a	k8xC	a
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
se	se	k3xPyFc4	se
projíždět	projíždět	k5eAaImF	projíždět
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
zámku	zámek	k1gInSc2	zámek
Laxenburg	Laxenburg	k1gInSc1	Laxenburg
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
sama	sám	k3xTgFnSc1	sám
procházet	procházet	k5eAaImF	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hofmistryně	hofmistryně	k1gFnSc1	hofmistryně
Marie	Maria	k1gFnSc2	Maria
Žofie	Žofie	k1gFnSc1	Žofie
to	ten	k3xDgNnSc4	ten
pobouřeně	pobouřeně	k6eAd1	pobouřeně
oznámila	oznámit	k5eAaPmAgFnS	oznámit
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
své	svůj	k3xOyFgFnPc4	svůj
ženy	žena	k1gFnPc4	žena
zastal	zastat	k5eAaPmAgMnS	zastat
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgMnS	zakázat
dvoru	dvůr	k1gInSc3	dvůr
se	se	k3xPyFc4	se
vměšovat	vměšovat	k5eAaImF	vměšovat
do	do	k7c2	do
plánů	plán	k1gInPc2	plán
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Alžbětě	Alžběta	k1gFnSc3	Alžběta
a	a	k8xC	a
Františku	František	k1gMnSc3	František
Josefovi	Josef	k1gMnSc3	Josef
se	se	k3xPyFc4	se
narodily	narodit	k5eAaPmAgFnP	narodit
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
Žofie	Žofie	k1gFnSc1	Žofie
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
pouhých	pouhý	k2eAgNnPc6d1	pouhé
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1857	[number]	k4	1857
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
Alžběta	Alžběta	k1gFnSc1	Alžběta
vzala	vzít	k5eAaPmAgFnS	vzít
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c6	na
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
Uhrách	Uhry	k1gFnPc6	Uhry
(	(	kIx(	(
<g/>
malá	malý	k2eAgFnSc1d1	malá
Žofie	Žofie	k1gFnSc1	Žofie
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Alžbětu	Alžběta	k1gFnSc4	Alžběta
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
rána	rána	k1gFnSc1	rána
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdala	vzdát	k5eAaPmAgFnS	vzdát
boj	boj	k1gInSc4	boj
o	o	k7c4	o
zbývající	zbývající	k2eAgFnPc4d1	zbývající
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
chladný	chladný	k2eAgInSc1d1	chladný
<g/>
.	.	kIx.	.
</s>
<s>
Druhorozená	druhorozený	k2eAgFnSc1d1	druhorozená
Gisela	Gisela	k1gFnSc1	Gisela
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
a	a	k8xC	a
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
Rudolf	Rudolf	k1gMnSc1	Rudolf
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1858	[number]	k4	1858
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
vyrůstali	vyrůstat	k5eAaImAgMnP	vyrůstat
v	v	k7c4	v
péči	péče	k1gFnSc4	péče
babičky	babička	k1gFnSc2	babička
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
"	"	kIx"	"
<g/>
jedináčka	jedináček	k1gMnSc2	jedináček
<g/>
"	"	kIx"	"
považovala	považovat	k5eAaImAgFnS	považovat
Sissi	Sisse	k1gFnSc4	Sisse
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc3	Maria
Valerii	Valerie	k1gFnSc3	Valerie
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1868	[number]	k4	1868
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
lidem	lid	k1gInSc7	lid
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
maďarské	maďarský	k2eAgNnSc1d1	Maďarské
dítě	dítě	k1gNnSc1	dítě
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jí	jíst	k5eAaImIp3nS	jíst
Alžběta	Alžběta	k1gFnSc1	Alžběta
říkávala	říkávat	k5eAaImAgFnS	říkávat
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
"	"	kIx"	"
a	a	k8xC	a
chovala	chovat	k5eAaImAgFnS	chovat
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
vřelý	vřelý	k2eAgInSc1d1	vřelý
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svázané	svázaný	k2eAgFnSc6d1	svázaná
atmosféře	atmosféra	k1gFnSc6	atmosféra
rakouského	rakouský	k2eAgInSc2d1	rakouský
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
bez	bez	k7c2	bez
pozornosti	pozornost	k1gFnSc2	pozornost
pracujícího	pracující	k1gMnSc2	pracující
manžela	manžel	k1gMnSc2	manžel
se	se	k3xPyFc4	se
cítila	cítit	k5eAaImAgNnP	cítit
velmi	velmi	k6eAd1	velmi
osamělá	osamělý	k2eAgNnPc1d1	osamělé
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgNnP	začít
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
pomalu	pomalu	k6eAd1	pomalu
stahovat	stahovat	k5eAaImF	stahovat
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
velmi	velmi	k6eAd1	velmi
vážně	vážně	k6eAd1	vážně
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Dvorní	dvorní	k2eAgMnSc1d1	dvorní
lékař	lékař	k1gMnSc1	lékař
ji	on	k3xPp3gFnSc4	on
poslal	poslat	k5eAaPmAgMnS	poslat
na	na	k7c4	na
ozdravný	ozdravný	k2eAgInSc4d1	ozdravný
pobyt	pobyt	k1gInSc4	pobyt
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poprvé	poprvé	k6eAd1	poprvé
ucítila	ucítit	k5eAaPmAgFnS	ucítit
svobodu	svoboda	k1gFnSc4	svoboda
cestování	cestování	k1gNnSc2	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
,	,	kIx,	,
když	když	k8xS	když
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
<g/>
,	,	kIx,	,
stačilo	stačit	k5eAaBmAgNnS	stačit
odjet	odjet	k5eAaPmF	odjet
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
nějaké	nějaký	k3yIgFnSc2	nějaký
daleké	daleký	k2eAgFnSc2d1	daleká
země	zem	k1gFnSc2	zem
a	a	k8xC	a
Sissi	Sisse	k1gFnSc4	Sisse
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
vyměněná	vyměněný	k2eAgFnSc1d1	vyměněná
<g/>
,	,	kIx,	,
smála	smát	k5eAaImAgFnS	smát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
jedla	jíst	k5eAaImAgFnS	jíst
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
chuť	chuť	k1gFnSc4	chuť
do	do	k7c2	do
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
zpět	zpět	k6eAd1	zpět
začaly	začít	k5eAaPmAgFnP	začít
ovšem	ovšem	k9	ovšem
všechny	všechen	k3xTgInPc1	všechen
problémy	problém	k1gInPc1	problém
nanovo	nanovo	k6eAd1	nanovo
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stávalo	stávat	k5eAaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
častěji	často	k6eAd2	často
pryč	pryč	k6eAd1	pryč
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
než	než	k8xS	než
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Oč	oč	k6eAd1	oč
méně	málo	k6eAd2	málo
měla	mít	k5eAaImAgFnS	mít
ráda	rád	k2eAgFnSc1d1	ráda
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
raději	rád	k6eAd2	rád
jezdila	jezdit	k5eAaImAgFnS	jezdit
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přátelila	přátelit	k5eAaImAgFnS	přátelit
s	s	k7c7	s
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
oslněna	oslnit	k5eAaPmNgFnS	oslnit
nejen	nejen	k6eAd1	nejen
jejím	její	k3xOp3gNnSc7	její
jezdeckým	jezdecký	k2eAgNnSc7d1	jezdecké
uměním	umění	k1gNnSc7	umění
a	a	k8xC	a
krásou	krása	k1gFnSc7	krása
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
dokonalým	dokonalý	k2eAgNnSc7d1	dokonalé
ovládáním	ovládání	k1gNnSc7	ovládání
jejich	jejich	k3xOp3gFnSc2	jejich
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
politickým	politický	k2eAgInSc7d1	politický
přehledem	přehled	k1gInSc7	přehled
císařovny	císařovna	k1gFnSc2	císařovna
a	a	k8xC	a
pochopením	pochopení	k1gNnSc7	pochopení
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
samostatnosti	samostatnost	k1gFnSc6	samostatnost
Maďarů	Maďar	k1gMnPc2	Maďar
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
40	[number]	k4	40
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
fenomenální	fenomenální	k2eAgFnSc7d1	fenomenální
jezdkyní	jezdkyně	k1gFnSc7	jezdkyně
–	–	k?	–
i	i	k8xC	i
při	při	k7c6	při
těch	ten	k3xDgInPc6	ten
nejnáročnějších	náročný	k2eAgInPc6d3	nejnáročnější
lovech	lov	k1gInPc6	lov
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
Anglii	Anglie	k1gFnSc3	Anglie
a	a	k8xC	a
Irsku	Irsko	k1gNnSc3	Irsko
<g/>
)	)	kIx)	)
jezdila	jezdit	k5eAaImAgFnS	jezdit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
a	a	k8xC	a
udivovala	udivovat	k5eAaImAgFnS	udivovat
nebojácností	nebojácnost	k1gFnSc7	nebojácnost
a	a	k8xC	a
železnou	železný	k2eAgFnSc7d1	železná
vůlí	vůle	k1gFnSc7	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Právem	právem	k6eAd1	právem
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
<g/>
,	,	kIx,	,
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
jezdkyň	jezdkyně	k1gFnPc2	jezdkyně
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
významně	významně	k6eAd1	významně
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
velmi	velmi	k6eAd1	velmi
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Nadšeně	nadšeně	k6eAd1	nadšeně
se	se	k3xPyFc4	se
angažovala	angažovat	k5eAaBmAgFnS	angažovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rakousko-uherského	rakouskoherský	k2eAgNnSc2d1	rakousko-uherské
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyQgNnSc7	čí
více	hodně	k6eAd2	hodně
byla	být	k5eAaImAgFnS	být
zbožňována	zbožňovat	k5eAaImNgFnS	zbožňovat
Maďary	Maďar	k1gMnPc7	Maďar
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
byla	být	k5eAaImAgFnS	být
oblíbena	oblíbit	k5eAaPmNgFnS	oblíbit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
si	se	k3xPyFc3	se
také	také	k9	také
prosadila	prosadit	k5eAaPmAgFnS	prosadit
maďarské	maďarský	k2eAgFnPc4d1	maďarská
dvorní	dvorní	k2eAgFnPc4d1	dvorní
dámy	dáma	k1gFnPc4	dáma
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
její	její	k3xOp3gFnPc1	její
věrné	věrný	k2eAgFnPc1d1	věrná
přítelkyně	přítelkyně	k1gFnPc1	přítelkyně
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
jedinými	jediný	k2eAgFnPc7d1	jediná
osobami	osoba	k1gFnPc7	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
plně	plně	k6eAd1	plně
důvěřovala	důvěřovat	k5eAaImAgNnP	důvěřovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
zapsala	zapsat	k5eAaPmAgFnS	zapsat
téměř	téměř	k6eAd1	téměř
jen	jen	k9	jen
svou	svůj	k3xOyFgFnSc7	svůj
rolí	role	k1gFnSc7	role
při	při	k7c6	při
státoprávním	státoprávní	k2eAgNnSc6d1	státoprávní
uspořádání	uspořádání	k1gNnSc6	uspořádání
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
České	český	k2eAgNnSc4d1	české
království	království	k1gNnSc4	království
dalekosáhlé	dalekosáhlý	k2eAgInPc4d1	dalekosáhlý
důsledky	důsledek	k1gInPc4	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Duševní	duševní	k2eAgInSc1d1	duševní
úpadek	úpadek	k1gInSc1	úpadek
a	a	k8xC	a
tragická	tragický	k2eAgFnSc1d1	tragická
smrt	smrt	k1gFnSc1	smrt
bratrance	bratranec	k1gMnSc2	bratranec
Ludvíka	Ludvík	k1gMnSc2	Ludvík
<g/>
,	,	kIx,	,
bavorského	bavorský	k2eAgMnSc2d1	bavorský
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
a	a	k8xC	a
choromyslnost	choromyslnost	k1gFnSc1	choromyslnost
jeho	jeho	k3xOp3gMnSc2	jeho
bratra	bratr	k1gMnSc2	bratr
utvrdily	utvrdit	k5eAaPmAgInP	utvrdit
císařovnu	císařovna	k1gFnSc4	císařovna
v	v	k7c6	v
domněnce	domněnka	k1gFnSc6	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
choroba	choroba	k1gFnSc1	choroba
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
dědičná	dědičný	k2eAgFnSc1d1	dědičná
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
postihne	postihnout	k5eAaPmIp3nS	postihnout
i	i	k9	i
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
studovat	studovat	k5eAaImF	studovat
šílené	šílený	k2eAgMnPc4d1	šílený
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
informovala	informovat	k5eAaBmAgFnS	informovat
se	se	k3xPyFc4	se
o	o	k7c6	o
jejich	jejich	k3xOp3gInPc6	jejich
osudech	osud	k1gInPc6	osud
<g/>
,	,	kIx,	,
průběhu	průběh	k1gInSc6	průběh
nemoci	nemoc	k1gFnSc2	nemoc
a	a	k8xC	a
příznacích	příznak	k1gInPc6	příznak
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Například	například	k6eAd1	například
příběh	příběh	k1gInSc1	příběh
Barbary	Barbara	k1gFnSc2	Barbara
Ubrykové	Ubryková	k1gFnSc2	Ubryková
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
zřejmě	zřejmě	k6eAd1	zřejmě
nikdy	nikdy	k6eAd1	nikdy
neuvědomila	uvědomit	k5eNaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
vděčila	vděčit	k5eAaImAgFnS	vděčit
Alžbětě	Alžběta	k1gFnSc3	Alžběta
za	za	k7c4	za
život	život	k1gInSc4	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vztah	vztah	k1gInSc1	vztah
Alžběty	Alžběta	k1gFnSc2	Alžběta
a	a	k8xC	a
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
opět	opět	k6eAd1	opět
harmoničtější	harmonický	k2eAgFnSc1d2	harmoničtější
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
intenzivnější	intenzivní	k2eAgNnSc1d2	intenzivnější
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
něžných	něžný	k2eAgFnPc2d1	něžná
a	a	k8xC	a
děkovných	děkovný	k2eAgFnPc2d1	děkovná
poznámek	poznámka	k1gFnPc2	poznámka
císaře	císař	k1gMnSc2	císař
v	v	k7c6	v
dopisech	dopis	k1gInPc6	dopis
své	svůj	k3xOyFgFnSc2	svůj
choti	choť	k1gFnSc2	choť
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
posedlá	posedlý	k2eAgFnSc1d1	posedlá
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
,	,	kIx,	,
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
největší	veliký	k2eAgFnSc4d3	veliký
pýchu	pýcha	k1gFnSc4	pýcha
považovala	považovat	k5eAaImAgFnS	považovat
své	svůj	k3xOyFgNnSc4	svůj
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
krásné	krásný	k2eAgFnPc1d1	krásná
a	a	k8xC	a
husté	hustý	k2eAgInPc1d1	hustý
hnědé	hnědý	k2eAgInPc1d1	hnědý
vlasy	vlas	k1gInPc1	vlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
později	pozdě	k6eAd2	pozdě
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
až	až	k9	až
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
obdivů	obdiv	k1gInPc2	obdiv
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
jí	jíst	k5eAaImIp3nS	jíst
její	její	k3xOp3gFnPc4	její
osobní	osobní	k2eAgFnPc4d1	osobní
kadeřnice	kadeřnice	k1gFnPc4	kadeřnice
Franciska	Franciska	k1gFnSc1	Franciska
Feifaliková	Feifalikový	k2eAgFnSc1d1	Feifalikový
musela	muset	k5eAaImAgFnS	muset
předkládat	předkládat	k5eAaImF	předkládat
vyčesané	vyčesaný	k2eAgInPc4d1	vyčesaný
vlasy	vlas	k1gInPc4	vlas
na	na	k7c6	na
stříbrném	stříbrný	k2eAgInSc6d1	stříbrný
podnose	podnos	k1gInSc6	podnos
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
kadeřnice	kadeřnice	k1gFnSc1	kadeřnice
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
trik	trik	k1gInSc4	trik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
císařovně	císařovna	k1gFnSc3	císařovna
zbytečně	zbytečně	k6eAd1	zbytečně
nekazila	kazit	k5eNaImAgFnS	kazit
náladu	nálada	k1gFnSc4	nálada
<g/>
:	:	kIx,	:
pod	pod	k7c4	pod
zástěrku	zástěrka	k1gFnSc4	zástěrka
připevnila	připevnit	k5eAaPmAgFnS	připevnit
pásku	páska	k1gFnSc4	páska
pomazanou	pomazaný	k2eAgFnSc4d1	pomazaná
lepidlem	lepidlo	k1gNnSc7	lepidlo
a	a	k8xC	a
tam	tam	k6eAd1	tam
vyčesané	vyčesaný	k2eAgInPc4d1	vyčesaný
vlasy	vlas	k1gInPc4	vlas
lepila	lepit	k5eAaImAgFnS	lepit
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
zvyklá	zvyklý	k2eAgFnSc1d1	zvyklá
<g/>
,	,	kIx,	,
že	že	k8xS	že
smí	smět	k5eAaImIp3nS	smět
s	s	k7c7	s
personálem	personál	k1gInSc7	personál
zacházet	zacházet	k5eAaImF	zacházet
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nezřídka	nezřídka	k6eAd1	nezřídka
stávalo	stávat	k5eAaImAgNnS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
než	než	k8xS	než
Franciska	Franciska	k1gFnSc1	Franciska
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
trik	trik	k1gInSc4	trik
s	s	k7c7	s
lepidlem	lepidlo	k1gNnSc7	lepidlo
<g/>
,	,	kIx,	,
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
vypadlé	vypadlý	k2eAgInPc4d1	vypadlý
vlasy	vlas	k1gInPc4	vlas
políček	políčko	k1gNnPc2	políčko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
toto	tento	k3xDgNnSc4	tento
nespravedlivé	spravedlivý	k2eNgNnSc4d1	nespravedlivé
jednání	jednání	k1gNnSc4	jednání
se	se	k3xPyFc4	se
Franciska	Franciska	k1gFnSc1	Franciska
mstila	mstít	k5eAaImAgFnS	mstít
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
předstírala	předstírat	k5eAaImAgFnS	předstírat
nemoc	nemoc	k1gFnSc1	nemoc
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
se	se	k3xPyFc4	se
i	i	k9	i
odvézt	odvézt	k5eAaPmF	odvézt
záchranným	záchranný	k2eAgInSc7d1	záchranný
vozem	vůz	k1gInSc7	vůz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
císařovnu	císařovna	k1gFnSc4	císařovna
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
do	do	k7c2	do
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ji	on	k3xPp3gFnSc4	on
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
učesat	učesat	k5eAaPmF	učesat
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc4d1	podobná
manipulace	manipulace	k1gFnPc4	manipulace
Alžběta	Alžběta	k1gFnSc1	Alžběta
kupodivu	kupodivu	k6eAd1	kupodivu
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
oblíbenců	oblíbenec	k1gMnPc2	oblíbenec
snášela	snášet	k5eAaImAgFnS	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Česání	česání	k1gNnSc1	česání
se	se	k3xPyFc4	se
odehrávalo	odehrávat	k5eAaImAgNnS	odehrávat
odpoledne	odpoledne	k6eAd1	odpoledne
a	a	k8xC	a
trvalo	trvat	k5eAaImAgNnS	trvat
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
nosívala	nosívat	k5eAaImAgFnS	nosívat
splétanou	splétaný	k2eAgFnSc4d1	splétaná
vlasovou	vlasový	k2eAgFnSc4d1	vlasová
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
označovala	označovat	k5eAaImAgFnS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
účes	účes	k1gInSc1	účes
pro	pro	k7c4	pro
snímek	snímek	k1gInSc4	snímek
na	na	k7c4	na
zatykač	zatykač	k1gInSc4	zatykač
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
nebo	nebo	k8xC	nebo
dvakrát	dvakrát	k6eAd1	dvakrát
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
mytí	mytí	k1gNnSc1	mytí
vlasů	vlas	k1gInPc2	vlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
nesměl	smět	k5eNaImAgMnS	smět
nikdo	nikdo	k3yNnSc1	nikdo
na	na	k7c4	na
císařovnu	císařovna	k1gFnSc4	císařovna
promluvit	promluvit	k5eAaPmF	promluvit
<g/>
.	.	kIx.	.
</s>
<s>
Franciska	Franciska	k1gFnSc1	Franciska
připravovala	připravovat	k5eAaImAgFnS	připravovat
směs	směs	k1gFnSc4	směs
ze	z	k7c2	z
třiceti	třicet	k4xCc2	třicet
žloutků	žloutek	k1gInPc2	žloutek
a	a	k8xC	a
francovky	francovka	k1gFnSc2	francovka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
šampon	šampon	k1gInSc4	šampon
nanášela	nanášet	k5eAaImAgFnS	nanášet
štětcem	štětec	k1gInSc7	štětec
na	na	k7c4	na
prameny	pramen	k1gInPc4	pramen
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vlasy	vlas	k1gInPc7	vlas
umyly	umýt	k5eAaPmAgFnP	umýt
teplou	teplý	k2eAgFnSc7d1	teplá
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
opláchly	opláchnout	k5eAaPmAgFnP	opláchnout
vývarem	vývar	k1gInSc7	vývar
ze	z	k7c2	z
slupek	slupka	k1gFnPc2	slupka
vlašských	vlašský	k2eAgInPc2d1	vlašský
ořechů	ořech	k1gInPc2	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
umytí	umytí	k1gNnSc6	umytí
chodila	chodit	k5eAaImAgFnS	chodit
císařovna	císařovna	k1gFnSc1	císařovna
sem	sem	k6eAd1	sem
a	a	k8xC	a
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
neuschly	uschnout	k5eNaPmAgFnP	uschnout
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
Francisce	Franciska	k1gFnSc3	Franciska
udělalo	udělat	k5eAaPmAgNnS	udělat
skutečně	skutečně	k6eAd1	skutečně
nevolno	nevolno	k6eAd1	nevolno
<g/>
,	,	kIx,	,
museli	muset	k5eAaImAgMnP	muset
sehnat	sehnat	k5eAaPmF	sehnat
náhradnici	náhradnice	k1gFnSc4	náhradnice
<g/>
,	,	kIx,	,
domorodou	domorodý	k2eAgFnSc4d1	domorodá
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
vyčesané	vyčesaný	k2eAgInPc4d1	vyčesaný
vlasy	vlas	k1gInPc4	vlas
strčila	strčit	k5eAaPmAgFnS	strčit
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
císařovna	císařovna	k1gFnSc1	císařovna
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
podivila	podivit	k5eAaPmAgFnS	podivit
se	se	k3xPyFc4	se
a	a	k8xC	a
ptala	ptat	k5eAaImAgFnS	ptat
se	se	k3xPyFc4	se
na	na	k7c4	na
důvod	důvod	k1gInSc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Přistižená	přistižený	k2eAgFnSc1d1	přistižená
kadeřnice	kadeřnice	k1gFnSc1	kadeřnice
plakala	plakat	k5eAaImAgFnS	plakat
<g/>
,	,	kIx,	,
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
kolena	koleno	k1gNnPc4	koleno
a	a	k8xC	a
prosila	prosit	k5eAaImAgFnS	prosit
za	za	k7c4	za
odpuštění	odpuštění	k1gNnSc4	odpuštění
neodolatelné	odolatelný	k2eNgFnSc2d1	neodolatelná
touhy	touha	k1gFnSc2	touha
ponechat	ponechat	k5eAaPmF	ponechat
si	se	k3xPyFc3	se
jako	jako	k8xS	jako
památku	památka	k1gFnSc4	památka
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc1	jeden
císařovnin	císařovnin	k2eAgInSc1d1	císařovnin
vlas	vlas	k1gInSc1	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
dojatá	dojatý	k2eAgFnSc1d1	dojatá
<g/>
,	,	kIx,	,
vzala	vzít	k5eAaPmAgFnS	vzít
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
nůžky	nůžky	k1gFnPc4	nůžky
a	a	k8xC	a
odstřihla	odstřihnout	k5eAaPmAgFnS	odstřihnout
kadeř	kadeř	k1gFnSc1	kadeř
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jí	jíst	k5eAaImIp3nS	jíst
darovala	darovat	k5eAaPmAgFnS	darovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejhorlivějším	horlivý	k2eAgMnPc3d3	nejhorlivější
císařovniným	císařovnin	k2eAgMnPc3d1	císařovnin
obdivovatelům	obdivovatel	k1gMnPc3	obdivovatel
patřil	patřit	k5eAaImAgInS	patřit
perský	perský	k2eAgInSc4d1	perský
šach	šach	k1gInSc4	šach
Nasr-ed-din	Nasrdina	k1gFnPc2	Nasr-ed-dina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
deníku	deník	k1gInSc6	deník
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k4c1	málo
lichotivých	lichotivý	k2eAgFnPc2d1	lichotivá
poznámek	poznámka	k1gFnPc2	poznámka
o	o	k7c6	o
dámách	dáma	k1gFnPc6	dáma
z	z	k7c2	z
vysokých	vysoký	k2eAgInPc2d1	vysoký
kruhů	kruh	k1gInPc2	kruh
<g/>
;	;	kIx,	;
výjimku	výjimka	k1gFnSc4	výjimka
tvořila	tvořit	k5eAaImAgFnS	tvořit
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
shledal	shledat	k5eAaPmAgMnS	shledat
nejkrásnější	krásný	k2eAgFnSc7d3	nejkrásnější
panovnicí	panovnice	k1gFnSc7	panovnice
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dam	dáma	k1gFnPc2	dáma
na	na	k7c6	na
evropských	evropský	k2eAgInPc6d1	evropský
dvorech	dvůr	k1gInPc6	dvůr
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Má	mít	k5eAaImIp3nS	mít
nádhernou	nádherný	k2eAgFnSc4d1	nádherná
bílou	bílý	k2eAgFnSc4d1	bílá
pleť	pleť	k1gFnSc4	pleť
a	a	k8xC	a
postavu	postava	k1gFnSc4	postava
cypřiše	cypřiš	k1gInSc2	cypřiš
<g/>
,	,	kIx,	,
majestátní	majestátní	k2eAgInSc1d1	majestátní
zjev	zjev	k1gInSc1	zjev
od	od	k7c2	od
pěšinky	pěšinka	k1gFnSc2	pěšinka
nádherných	nádherný	k2eAgInPc2d1	nádherný
vlasů	vlas	k1gInPc2	vlas
až	až	k9	až
k	k	k7c3	k
patě	pata	k1gFnSc3	pata
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
znovu	znovu	k6eAd1	znovu
zopakovat	zopakovat	k5eAaPmF	zopakovat
–	–	k?	–
díky	díky	k7c3	díky
půvabům	půvab	k1gInPc3	půvab
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
pastvou	pastva	k1gFnSc7	pastva
pro	pro	k7c4	pro
oči	oko	k1gNnPc4	oko
<g/>
...	...	k?	...
její	její	k3xOp3gInSc1	její
zjev	zjev	k1gInSc1	zjev
je	být	k5eAaImIp3nS	být
opravdovým	opravdový	k2eAgInSc7d1	opravdový
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
i	i	k9	i
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
seděl	sedět	k5eAaImAgMnS	sedět
u	u	k7c2	u
tabule	tabule	k1gFnSc2	tabule
vedle	vedle	k7c2	vedle
mne	já	k3xPp1nSc2	já
<g/>
.	.	kIx.	.
</s>
<s>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
můj	můj	k3xOp1gInSc4	můj
úsudek	úsudek	k1gInSc4	úsudek
potěšil	potěšit	k5eAaPmAgInS	potěšit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Další	další	k2eAgFnSc7d1	další
císařovninou	císařovnin	k2eAgFnSc7d1	Císařovnina
posedlostí	posedlost	k1gFnSc7	posedlost
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gNnSc4	její
váha	váha	k1gFnSc1	váha
–	–	k?	–
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nepřehoupla	přehoupnout	k5eNaPmAgFnS	přehoupnout
přes	přes	k7c4	přes
55	[number]	k4	55
kg	kg	kA	kg
<g/>
,	,	kIx,	,
držela	držet	k5eAaImAgFnS	držet
drastické	drastický	k2eAgFnPc4d1	drastická
diety	dieta	k1gFnPc4	dieta
–	–	k?	–
jedla	jíst	k5eAaImAgFnS	jíst
jen	jen	k6eAd1	jen
ovoce	ovoce	k1gNnSc4	ovoce
a	a	k8xC	a
pila	pít	k5eAaImAgFnS	pít
šťávu	šťáva	k1gFnSc4	šťáva
ze	z	k7c2	z
syrového	syrový	k2eAgNnSc2d1	syrové
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
nosila	nosit	k5eAaImAgFnS	nosit
silně	silně	k6eAd1	silně
utažené	utažený	k2eAgFnPc4d1	utažená
šněrovačky	šněrovačka	k1gFnPc4	šněrovačka
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
si	se	k3xPyFc3	se
nikdy	nikdy	k6eAd1	nikdy
nesedla	sednout	k5eNaPmAgFnS	sednout
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pokojích	pokoj	k1gInPc6	pokoj
neměla	mít	k5eNaImAgFnS	mít
židle	židle	k1gFnSc1	židle
a	a	k8xC	a
při	při	k7c6	při
hostinách	hostina	k1gFnPc6	hostina
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgNnP	používat
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
klekátka	klekátko	k1gNnPc1	klekátko
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
nosívala	nosívat	k5eAaImAgFnS	nosívat
černé	černý	k2eAgInPc4d1	černý
atlasové	atlasový	k2eAgInPc4d1	atlasový
střevíce	střevíc	k1gInPc4	střevíc
s	s	k7c7	s
nízkými	nízký	k2eAgInPc7d1	nízký
podpatky	podpatek	k1gInPc7	podpatek
se	s	k7c7	s
šněrovadly	šněrovadlo	k1gNnPc7	šněrovadlo
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
chladném	chladný	k2eAgNnSc6d1	chladné
počasí	počasí	k1gNnSc6	počasí
si	se	k3xPyFc3	se
přes	přes	k7c4	přes
ně	on	k3xPp3gMnPc4	on
natahovala	natahovat	k5eAaImAgFnS	natahovat
kožené	kožený	k2eAgInPc4d1	kožený
návleky	návlek	k1gInPc4	návlek
podšité	podšitý	k2eAgFnSc6d1	podšitá
hedvábím	hedvábí	k1gNnSc7	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
dávala	dávat	k5eAaImAgFnS	dávat
přednost	přednost	k1gFnSc4	přednost
bílým	bílý	k2eAgInPc3d1	bílý
atlasovým	atlasový	k2eAgInPc3d1	atlasový
střevíčkům	střevíček	k1gInPc3	střevíček
s	s	k7c7	s
krajkovými	krajkový	k2eAgFnPc7d1	krajková
růžicemi	růžice	k1gFnPc7	růžice
s	s	k7c7	s
gumovými	gumový	k2eAgInPc7d1	gumový
pásky	pásek	k1gInPc7	pásek
nebo	nebo	k8xC	nebo
šněrovacím	šněrovací	k2eAgFnPc3d1	šněrovací
polovysokým	polovysoký	k2eAgFnPc3d1	polovysoká
botkám	botka	k1gFnPc3	botka
<g/>
,	,	kIx,	,
šitými	šitý	k2eAgFnPc7d1	šitá
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Punčochy	punčocha	k1gFnSc2	punčocha
objednávala	objednávat	k5eAaImAgFnS	objednávat
u	u	k7c2	u
anglické	anglický	k2eAgFnSc2d1	anglická
firmy	firma	k1gFnSc2	firma
Swears	Swearsa	k1gFnPc2	Swearsa
and	and	k?	and
Wells	Wellsa	k1gFnPc2	Wellsa
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přestala	přestat	k5eAaPmAgFnS	přestat
císařovna	císařovna	k1gFnSc1	císařovna
nosit	nosit	k5eAaImF	nosit
spodničku	spodnička	k1gFnSc4	spodnička
a	a	k8xC	a
setrvala	setrvat	k5eAaPmAgFnS	setrvat
tak	tak	k6eAd1	tak
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Spodničky	spodnička	k1gFnPc4	spodnička
nikdy	nikdy	k6eAd1	nikdy
nenosila	nosit	k5eNaImAgFnS	nosit
a	a	k8xC	a
při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
časných	časný	k2eAgFnPc6d1	časná
ranních	ranní	k2eAgFnPc6d1	ranní
procházkách	procházka	k1gFnPc6	procházka
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
si	se	k3xPyFc3	se
obouvala	obouvat	k5eAaImAgFnS	obouvat
střevíce	střevíc	k1gInPc4	střevíc
na	na	k7c4	na
bosé	bosý	k2eAgFnPc4d1	bosá
nohy	noha	k1gFnPc4	noha
a	a	k8xC	a
šaty	šata	k1gFnPc4	šata
oblékala	oblékat	k5eAaImAgFnS	oblékat
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
nahé	nahý	k2eAgNnSc4d1	nahé
tělo	tělo	k1gNnSc4	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Wallersee	Wallersee	k1gInSc1	Wallersee
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.54	.54	k4	.54
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Císařovnino	císařovnin	k2eAgNnSc4d1	císařovnino
letní	letní	k2eAgNnSc4d1	letní
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
tvořily	tvořit	k5eAaImAgFnP	tvořit
lehké	lehký	k2eAgFnPc1d1	lehká
košilky	košilka	k1gFnPc1	košilka
a	a	k8xC	a
kalhoty	kalhoty	k1gFnPc1	kalhoty
z	z	k7c2	z
hedvábného	hedvábný	k2eAgInSc2d1	hedvábný
trikotu	trikot	k1gInSc2	trikot
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
prádlo	prádlo	k1gNnSc1	prádlo
z	z	k7c2	z
jemné	jemný	k2eAgFnSc2d1	jemná
prací	prací	k2eAgFnSc2d1	prací
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
nechávala	nechávat	k5eAaImAgFnS	nechávat
zašít	zašít	k5eAaPmF	zašít
<g/>
,	,	kIx,	,
nosívala	nosívat	k5eAaImAgFnS	nosívat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zašívání	zašívání	k1gNnSc1	zašívání
si	se	k3xPyFc3	se
časem	časem	k6eAd1	časem
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
i	i	k9	i
u	u	k7c2	u
jezdeckého	jezdecký	k2eAgNnSc2d1	jezdecké
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
si	se	k3xPyFc3	se
nechávala	nechávat	k5eAaImAgFnS	nechávat
šít	šít	k5eAaImF	šít
<g/>
,	,	kIx,	,
sedíc	sedit	k5eAaImSgFnS	sedit
na	na	k7c4	na
dřevěném	dřevěný	k2eAgMnSc6d1	dřevěný
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Teta	Teta	k1gFnSc1	Teta
považovala	považovat	k5eAaImAgFnS	považovat
svůj	svůj	k3xOyFgInSc4	svůj
hlavní	hlavní	k2eAgInSc4d1	hlavní
úkol	úkol	k1gInSc4	úkol
být	být	k5eAaImF	být
dobře	dobře	k6eAd1	dobře
oblečená	oblečený	k2eAgFnSc1d1	oblečená
za	za	k7c4	za
povinnost	povinnost	k1gFnSc4	povinnost
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
očekávají	očekávat	k5eAaImIp3nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
budu	být	k5eAaImBp1nS	být
vždy	vždy	k6eAd1	vždy
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
elegantní	elegantní	k2eAgFnSc1d1	elegantní
<g/>
,	,	kIx,	,
říkávala	říkávat	k5eAaImAgFnS	říkávat
mi	já	k3xPp1nSc3	já
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
lituji	litovat	k5eAaImIp1nS	litovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohu	moct	k5eNaImIp1nS	moct
své	svůj	k3xOyFgMnPc4	svůj
panovníky	panovník	k1gMnPc4	panovník
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
parádě	paráda	k1gFnSc6	paráda
minulých	minulý	k2eAgInPc2d1	minulý
časů	čas	k1gInPc2	čas
jako	jako	k8xS	jako
legendární	legendární	k2eAgMnPc4d1	legendární
krále	král	k1gMnPc4	král
a	a	k8xC	a
královny	královna	k1gFnPc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
knížata	kníže	k1gNnPc1	kníže
se	se	k3xPyFc4	se
oblékají	oblékat	k5eAaImIp3nP	oblékat
jako	jako	k9	jako
měšťáci	měšťák	k1gMnPc1	měšťák
a	a	k8xC	a
namlouvají	namlouvat	k5eAaImIp3nP	namlouvat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
jejich	jejich	k3xOp3gFnSc1	jejich
důstojnost	důstojnost	k1gFnSc1	důstojnost
dodává	dodávat	k5eAaImIp3nS	dodávat
dostatek	dostatek	k1gInSc4	dostatek
vnějšího	vnější	k2eAgInSc2d1	vnější
lesku	lesk	k1gInSc2	lesk
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
mýlí	mýlit	k5eAaImIp3nS	mýlit
<g/>
,	,	kIx,	,
poddaní	poddaný	k1gMnPc1	poddaný
vnímají	vnímat	k5eAaImIp3nP	vnímat
velmi	velmi	k6eAd1	velmi
bolestně	bolestně	k6eAd1	bolestně
jejich	jejich	k3xOp3gInSc1	jejich
nevkusný	vkusný	k2eNgInSc1d1	nevkusný
zjev	zjev	k1gInSc1	zjev
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Wallerse	Wallerse	k1gFnSc1	Wallerse
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
55	[number]	k4	55
<g/>
/	/	kIx~	/
Alžběta	Alžběta	k1gFnSc1	Alžběta
nosívala	nosívat	k5eAaImAgFnS	nosívat
málo	málo	k4c4	málo
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
snubní	snubní	k2eAgInSc4d1	snubní
prsten	prsten	k1gInSc4	prsten
nosila	nosit	k5eAaImAgFnS	nosit
na	na	k7c6	na
zlatém	zlatý	k2eAgInSc6d1	zlatý
řetízku	řetízek	k1gInSc6	řetízek
na	na	k7c6	na
krku	krk	k1gInSc6	krk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
skryt	skryt	k1gInSc1	skryt
pod	pod	k7c4	pod
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
hodinkách	hodinka	k1gFnPc6	hodinka
bylo	být	k5eAaImAgNnS	být
vyryto	vyryt	k2eAgNnSc1d1	vyryto
jméno	jméno	k1gNnSc1	jméno
Achilleus	Achilleus	k1gMnSc1	Achilleus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
její	její	k3xOp3gMnSc1	její
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
hrdina	hrdina	k1gMnSc1	hrdina
řeckých	řecký	k2eAgFnPc2d1	řecká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zápěstí	zápěstí	k1gNnSc4	zápěstí
nosívala	nosívat	k5eAaImAgFnS	nosívat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
pověrčivá	pověrčivý	k2eAgFnSc1d1	pověrčivá
<g/>
,	,	kIx,	,
lebku	lebka	k1gFnSc4	lebka
smrtihlava	smrtihlav	k1gMnSc2	smrtihlav
<g/>
,	,	kIx,	,
znamení	znamení	k1gNnSc1	znamení
Slunce	slunce	k1gNnSc2	slunce
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
nožkami	nožka	k1gFnPc7	nožka
<g/>
,	,	kIx,	,
zlatou	zlatý	k2eAgFnSc4d1	zlatá
ručičku	ručička	k1gFnSc4	ručička
se	s	k7c7	s
zdviženým	zdvižený	k2eAgInSc7d1	zdvižený
ukazováčkem	ukazováček	k1gInSc7	ukazováček
<g/>
,	,	kIx,	,
mariánskými	mariánský	k2eAgFnPc7d1	Mariánská
medailemi	medaile	k1gFnPc7	medaile
<g/>
,	,	kIx,	,
zlatými	zlatý	k2eAgFnPc7d1	zlatá
byzantskými	byzantský	k2eAgFnPc7d1	byzantská
mincemi	mince	k1gFnPc7	mince
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
medailonky	medailonek	k1gInPc7	medailonek
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
byl	být	k5eAaImAgInS	být
pramínek	pramínek	k1gInSc1	pramínek
vlasů	vlas	k1gInPc2	vlas
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Rudolfa	Rudolf	k1gMnSc2	Rudolf
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
21	[number]	k4	21
<g/>
.	.	kIx.	.
žalm	žalm	k1gInSc1	žalm
z	z	k7c2	z
Bible	bible	k1gFnSc2	bible
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
brož	brož	k1gFnSc1	brož
milovala	milovat	k5eAaImAgFnS	milovat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
napodobeninu	napodobenina	k1gFnSc4	napodobenina
koňského	koňský	k2eAgNnSc2d1	koňské
kopyta	kopyto	k1gNnSc2	kopyto
<g/>
,	,	kIx,	,
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
55	[number]	k4	55
kg	kg	kA	kg
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
váhou	váha	k1gFnSc7	váha
jíž	jenž	k3xRgFnSc3	jenž
při	pře	k1gFnSc3	pře
172	[number]	k4	172
cm	cm	kA	cm
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
váha	váha	k1gFnSc1	váha
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c7	mezi
45	[number]	k4	45
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Váhu	váha	k1gFnSc4	váha
si	se	k3xPyFc3	se
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
3	[number]	k4	3
<g/>
×	×	k?	×
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
si	se	k3xPyFc3	se
nechávala	nechávat	k5eAaImAgFnS	nechávat
jednou	jednou	k6eAd1	jednou
denně	denně	k6eAd1	denně
měřit	měřit	k5eAaImF	měřit
pas	pas	k1gInSc4	pas
<g/>
,	,	kIx,	,
stehna	stehno	k1gNnPc4	stehno
a	a	k8xC	a
lýtka	lýtko	k1gNnPc4	lýtko
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přineslo	přinést	k5eAaPmAgNnS	přinést
bezpočet	bezpočet	k1gInSc4	bezpočet
metod	metoda	k1gFnPc2	metoda
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
štíhlé	štíhlý	k2eAgFnSc2d1	štíhlá
linie	linie	k1gFnSc2	linie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
císařovna	císařovna	k1gFnSc1	císařovna
horlivě	horlivě	k6eAd1	horlivě
využívala	využívat	k5eAaImAgFnS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
spávala	spávat	k5eAaImAgFnS	spávat
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
zábalech	zábal	k1gInPc6	zábal
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
udržela	udržet	k5eAaPmAgFnS	udržet
štíhlý	štíhlý	k2eAgInSc4d1	štíhlý
pas	pas	k1gInSc4	pas
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pila	pít	k5eAaImAgFnS	pít
mixturu	mixtura	k1gFnSc4	mixtura
ze	z	k7c2	z
syrových	syrový	k2eAgInPc2d1	syrový
kořeněných	kořeněný	k2eAgInPc2d1	kořeněný
bílků	bílek	k1gInPc2	bílek
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
dietou	dieta	k1gFnSc7	dieta
bylo	být	k5eAaImAgNnS	být
kravské	kravský	k2eAgNnSc1d1	kravské
mléko	mléko	k1gNnSc1	mléko
(	(	kIx(	(
<g/>
krávy	kráva	k1gFnPc1	kráva
se	se	k3xPyFc4	se
chovaly	chovat	k5eAaImAgFnP	chovat
v	v	k7c6	v
schönbrunském	schönbrunský	k2eAgInSc6d1	schönbrunský
parku	park	k1gInSc6	park
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
je	být	k5eAaImIp3nS	být
osobně	osobně	k6eAd1	osobně
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
<g/>
,	,	kIx,	,
také	také	k9	také
je	on	k3xPp3gNnSc4	on
císařovna	císařovna	k1gFnSc1	císařovna
vozila	vozit	k5eAaImAgFnS	vozit
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
trpěly	trpět	k5eAaImAgFnP	trpět
mořskou	mořský	k2eAgFnSc7d1	mořská
nemocí	nemoc	k1gFnSc7	nemoc
a	a	k8xC	a
nedojily	dojit	k5eNaImAgFnP	dojit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
také	také	k9	také
každodenně	každodenně	k6eAd1	každodenně
pěstila	pěstit	k5eAaImAgFnS	pěstit
svou	svůj	k3xOyFgFnSc4	svůj
pleť	pleť	k1gFnSc4	pleť
mastmi	mast	k1gFnPc7	mast
<g/>
,	,	kIx,	,
koupelemi	koupel	k1gFnPc7	koupel
a	a	k8xC	a
bránila	bránit	k5eAaImAgFnS	bránit
se	se	k3xPyFc4	se
i	i	k9	i
slunečním	sluneční	k2eAgInPc3d1	sluneční
paprskům	paprsek	k1gInPc3	paprsek
vějíři	vějíř	k1gInPc7	vějíř
a	a	k8xC	a
závoji	závoj	k1gInPc7	závoj
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
se	se	k3xPyFc4	se
ale	ale	k9	ale
stejně	stejně	k6eAd1	stejně
vracela	vracet	k5eAaImAgFnS	vracet
opálená	opálený	k2eAgFnSc1d1	opálená
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
přípravu	příprava	k1gFnSc4	příprava
vyžadovaly	vyžadovat	k5eAaImAgInP	vyžadovat
výjezdy	výjezd	k1gInPc1	výjezd
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
posilnila	posilnit	k5eAaPmAgFnS	posilnit
silnou	silný	k2eAgFnSc7d1	silná
polévkou	polévka	k1gFnSc7	polévka
z	z	k7c2	z
hovězího	hovězí	k2eAgInSc2d1	hovězí
<g/>
,	,	kIx,	,
kuřecího	kuřecí	k2eAgInSc2d1	kuřecí
<g/>
,	,	kIx,	,
srnčího	srnčí	k2eAgMnSc2d1	srnčí
a	a	k8xC	a
koroptvího	koroptví	k2eAgNnSc2d1	koroptví
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
vypila	vypít	k5eAaPmAgFnS	vypít
dvě	dva	k4xCgFnPc4	dva
sklenky	sklenka	k1gFnPc4	sklenka
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
na	na	k7c4	na
koně	kůň	k1gMnPc4	kůň
<g/>
)	)	kIx)	)
nosívala	nosívat	k5eAaImAgFnS	nosívat
dózičku	dózička	k1gFnSc4	dózička
naplněnou	naplněný	k2eAgFnSc4d1	naplněná
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
císařovnou	císařovna	k1gFnSc7	císařovna
Eugénií	Eugénie	k1gFnSc7	Eugénie
uspořádaly	uspořádat	k5eAaPmAgFnP	uspořádat
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
poměřování	poměřování	k1gNnSc1	poměřování
lýtek	lýtko	k1gNnPc2	lýtko
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Velmi	velmi	k6eAd1	velmi
tiše	tiš	k1gFnPc4	tiš
jsem	být	k5eAaImIp1nS	být
otevřel	otevřít	k5eAaPmAgMnS	otevřít
<g/>
...	...	k?	...
zády	záda	k1gNnPc7	záda
k	k	k7c3	k
mému	můj	k3xOp1gNnSc3	můj
stanovišti	stanoviště	k1gNnSc3	stanoviště
stály	stát	k5eAaImAgFnP	stát
obě	dva	k4xCgFnPc4	dva
císařovny	císařovna	k1gFnPc4	císařovna
a	a	k8xC	a
měřily	měřit	k5eAaImAgFnP	měřit
si	se	k3xPyFc3	se
lýtka	lýtko	k1gNnSc2	lýtko
–	–	k?	–
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejlépe	dobře	k6eAd3	dobře
tvarovaná	tvarovaný	k2eAgFnSc1d1	tvarovaná
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
Evropa	Evropa	k1gFnSc1	Evropa
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
znala	znát	k5eAaImAgFnS	znát
<g/>
!	!	kIx.	!
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
scéna	scéna	k1gFnSc1	scéna
k	k	k7c3	k
nepopsání	nepopsání	k1gNnSc3	nepopsání
a	a	k8xC	a
já	já	k3xPp1nSc1	já
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
nezapomenu	zapomnět	k5eNaImIp1nS	zapomnět
<g/>
...	...	k?	...
Obě	dva	k4xCgFnPc1	dva
císařovny	císařovna	k1gFnPc1	císařovna
se	se	k3xPyFc4	se
otočily	otočit	k5eAaPmAgFnP	otočit
a	a	k8xC	a
když	když	k8xS	když
mě	já	k3xPp1nSc4	já
spatřily	spatřit	k5eAaPmAgInP	spatřit
<g/>
,	,	kIx,	,
milostivě	milostivě	k6eAd1	milostivě
se	se	k3xPyFc4	se
usmály	usmát	k5eAaPmAgInP	usmát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Wilczek	Wilczek	k1gInSc1	Wilczek
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
71	[number]	k4	71
<g/>
/	/	kIx~	/
Hovoří	hovořit	k5eAaImIp3nS	hovořit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c6	o
kráse	krása	k1gFnSc6	krása
císařovny	císařovna	k1gFnSc2	císařovna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
netýká	týkat	k5eNaImIp3nS	týkat
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
rysů	rys	k1gInPc2	rys
její	její	k3xOp3gFnSc2	její
tváře	tvář	k1gFnSc2	tvář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
urostlé	urostlý	k2eAgFnPc1d1	urostlá
<g/>
,	,	kIx,	,
štíhlé	štíhlý	k2eAgFnPc1d1	štíhlá
a	a	k8xC	a
křehké	křehký	k2eAgFnPc1d1	křehká
postavy	postava	k1gFnPc1	postava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
udržovala	udržovat	k5eAaImAgFnS	udržovat
pomocí	pomocí	k7c2	pomocí
diet	dieta	k1gFnPc2	dieta
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
gymnastiky	gymnastika	k1gFnSc2	gymnastika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
<g/>
,	,	kIx,	,
hustých	hustý	k2eAgInPc2d1	hustý
<g/>
,	,	kIx,	,
vlnitých	vlnitý	k2eAgInPc2d1	vlnitý
vlasů	vlas	k1gInPc2	vlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
sahaly	sahat	k5eAaImAgInP	sahat
ke	k	k7c3	k
kolenům	koleno	k1gNnPc3	koleno
<g/>
,	,	kIx,	,
o	o	k7c4	o
něž	jenž	k3xRgFnPc4	jenž
pečovala	pečovat	k5eAaImAgFnS	pečovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
manželství	manželství	k1gNnSc2	manželství
měl	mít	k5eAaImAgInS	mít
císařský	císařský	k2eAgInSc1d1	císařský
pár	pár	k1gInSc1	pár
na	na	k7c6	na
programu	program	k1gInSc6	program
společné	společný	k2eAgFnSc2d1	společná
cesty	cesta	k1gFnSc2	cesta
po	po	k7c6	po
zemích	zem	k1gFnPc6	zem
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
cestovala	cestovat	k5eAaImAgFnS	cestovat
Alžběta	Alžběta	k1gFnSc1	Alžběta
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
sama	sám	k3xTgFnSc1	sám
(	(	kIx(	(
<g/>
císař	císař	k1gMnSc1	císař
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
nemohl	moct	k5eNaImAgMnS	moct
připojit	připojit	k5eAaPmF	připojit
z	z	k7c2	z
pracovních	pracovní	k2eAgInPc2d1	pracovní
důvodů	důvod	k1gInPc2	důvod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
vášeň	vášeň	k1gFnSc4	vášeň
objevila	objevit	k5eAaPmAgFnS	objevit
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
bronchitidě	bronchitida	k1gFnSc6	bronchitida
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
lékaři	lékař	k1gMnPc1	lékař
doporučili	doporučit	k5eAaPmAgMnP	doporučit
pobyt	pobyt	k1gInSc4	pobyt
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
krajích	kraj	k1gInPc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
snažil	snažit	k5eAaImAgMnS	snažit
strávit	strávit	k5eAaPmF	strávit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zimy	zima	k1gFnSc2	zima
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
chotí	choť	k1gFnSc7	choť
na	na	k7c6	na
Riviéře	Riviéra	k1gFnSc6	Riviéra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
cestovali	cestovat	k5eAaImAgMnP	cestovat
ještě	ještě	k9	ještě
společně	společně	k6eAd1	společně
do	do	k7c2	do
Korutan	Korutany	k1gInPc2	Korutany
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Klagenfurtu	Klagenfurt	k1gInSc2	Klagenfurt
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Semmering	Semmering	k1gInSc4	Semmering
<g/>
,	,	kIx,	,
Mürzzuschlag	Mürzzuschlag	k1gInSc4	Mürzzuschlag
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
zastávce	zastávka	k1gFnSc6	zastávka
byl	být	k5eAaImAgInS	být
pár	pár	k1gInSc1	pár
vítán	vítán	k2eAgInSc1d1	vítán
jásajícím	jásající	k2eAgNnSc7d1	jásající
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Klagenfurtu	Klagenfurt	k1gInSc6	Klagenfurt
navštívili	navštívit	k5eAaPmAgMnP	navštívit
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnSc4d1	divadelní
představení	představení	k1gNnSc4	představení
a	a	k8xC	a
cestovali	cestovat	k5eAaImAgMnP	cestovat
dál	daleko	k6eAd2	daleko
do	do	k7c2	do
Villachu	Villach	k1gInSc2	Villach
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ubytovali	ubytovat	k5eAaPmAgMnP	ubytovat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
také	také	k9	také
k	k	k7c3	k
nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
vrcholu	vrchol	k1gInSc3	vrchol
Rakouska	Rakousko	k1gNnSc2	Rakousko
Grossglockneru	Grossglockner	k1gMnSc3	Grossglockner
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
Vysoké	vysoký	k2eAgNnSc4d1	vysoké
Sedlo	sedlo	k1gNnSc4	sedlo
(	(	kIx(	(
<g/>
2	[number]	k4	2
536	[number]	k4	536
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Návrší	návrší	k1gNnSc4	návrší
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
Laretterské	Laretterský	k2eAgFnSc6d1	Laretterský
plošině	plošina	k1gFnSc6	plošina
(	(	kIx(	(
<g/>
2	[number]	k4	2
127	[number]	k4	127
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
teď	teď	k6eAd1	teď
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Alžbětino	Alžbětin	k2eAgNnSc1d1	Alžbětino
odpočívadlo	odpočívadlo	k1gNnSc1	odpočívadlo
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
pár	pár	k1gInSc4	pár
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pobyt	pobyt	k1gInSc1	pobyt
znamenal	znamenat	k5eAaImAgInS	znamenat
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
zlom	zlom	k1gInSc4	zlom
v	v	k7c6	v
doposud	doposud	k6eAd1	doposud
klidném	klidný	k2eAgInSc6d1	klidný
životě	život	k1gInSc6	život
mladé	mladý	k2eAgFnSc2d1	mladá
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
stála	stát	k5eAaImAgFnS	stát
čelem	čelo	k1gNnSc7	čelo
italské	italský	k2eAgFnSc2d1	italská
antipatii	antipatie	k1gFnSc3	antipatie
vůči	vůči	k7c3	vůči
rakouské	rakouský	k2eAgFnSc3d1	rakouská
dynastii	dynastie	k1gFnSc3	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
představení	představení	k1gNnSc4	představení
v	v	k7c4	v
Teatro	Teatro	k1gNnSc4	Teatro
La	la	k1gNnSc2	la
Fenice	Fenice	k1gFnSc2	Fenice
musel	muset	k5eAaImAgInS	muset
pár	pár	k1gInSc1	pár
strpět	strpět	k5eAaPmF	strpět
urážku	urážka	k1gFnSc4	urážka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
poloprázdného	poloprázdný	k2eAgNnSc2d1	poloprázdné
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
poprvé	poprvé	k6eAd1	poprvé
cestovala	cestovat	k5eAaImAgFnS	cestovat
sama	sám	k3xTgFnSc1	sám
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dovolenou	dovolená	k1gFnSc4	dovolená
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
nemoci	nemoc	k1gFnSc3	nemoc
na	na	k7c4	na
Madeiru	Madeira	k1gFnSc4	Madeira
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
ji	on	k3xPp3gFnSc4	on
zde	zde	k6eAd1	zde
mimořádně	mimořádně	k6eAd1	mimořádně
krásný	krásný	k2eAgMnSc1d1	krásný
hrabě	hrabě	k1gMnSc1	hrabě
Imre	Imr	k1gFnSc2	Imr
Hunyady	Hunyada	k1gFnSc2	Hunyada
jako	jako	k8xC	jako
čestný	čestný	k2eAgMnSc1d1	čestný
kavalír	kavalír	k1gMnSc1	kavalír
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
věnovala	věnovat	k5eAaImAgFnS	věnovat
císařovna	císařovna	k1gFnSc1	císařovna
dvě	dva	k4xCgFnPc1	dva
sloky	sloka	k1gFnPc1	sloka
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Kabinet	kabinet	k1gInSc1	kabinet
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
její	její	k3xOp3gMnPc1	její
ctitelé	ctitel	k1gMnPc1	ctitel
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
oslím	oslí	k2eAgNnSc6d1	oslí
vzezření	vzezření	k1gNnSc6	vzezření
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
Hunyady	Hunyada	k1gFnSc2	Hunyada
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
služby	služba	k1gFnPc4	služba
u	u	k7c2	u
rakouského	rakouský	k2eAgInSc2d1	rakouský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
za	za	k7c2	za
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pobytu	pobyt	k1gInSc6	pobyt
na	na	k7c6	na
Madeiře	Madeira	k1gFnSc6	Madeira
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Císařovna	císařovna	k1gFnSc1	císařovna
Sevillu	Sevilla	k1gFnSc4	Sevilla
<g/>
,	,	kIx,	,
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
a	a	k8xC	a
Mallorcu	Mallorca	k1gFnSc4	Mallorca
a	a	k8xC	a
dojela	dojet	k5eAaPmAgFnS	dojet
až	až	k9	až
na	na	k7c4	na
Korfu	Korfu	k1gNnSc4	Korfu
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
Laxenburg	Laxenburg	k1gInSc4	Laxenburg
<g/>
,	,	kIx,	,
po	po	k7c6	po
půlroční	půlroční	k2eAgFnSc6d1	půlroční
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
viděla	vidět	k5eAaImAgFnS	vidět
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
arcivévodkyně	arcivévodkyně	k1gFnSc1	arcivévodkyně
Gisela	Gisela	k1gFnSc1	Gisela
tehdy	tehdy	k6eAd1	tehdy
čtyřletá	čtyřletý	k2eAgFnSc1d1	čtyřletá
a	a	k8xC	a
korunní	korunní	k2eAgMnSc1d1	korunní
princ	princ	k1gMnSc1	princ
Rudolf	Rudolf	k1gMnSc1	Rudolf
jedenapůlletý	jedenapůlletý	k2eAgMnSc1d1	jedenapůlletý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
po	po	k7c6	po
měsíci	měsíc	k1gInSc6	měsíc
odjela	odjet	k5eAaPmAgFnS	odjet
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
Korfu	Korfu	k1gNnSc4	Korfu
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
budovat	budovat	k5eAaImF	budovat
zámek	zámek	k1gInSc4	zámek
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
Achillovi	Achilles	k1gMnSc3	Achilles
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
umírající	umírající	k2eAgMnSc1d1	umírající
Achilles	Achilles	k1gMnSc1	Achilles
<g/>
,	,	kIx,	,
pravila	pravit	k5eAaBmAgFnS	pravit
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
jsem	být	k5eAaImIp1nS	být
zasvětila	zasvětit	k5eAaPmAgFnS	zasvětit
svůj	svůj	k3xOyFgInSc4	svůj
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
ztělesňuje	ztělesňovat	k5eAaImIp3nS	ztělesňovat
řeckou	řecký	k2eAgFnSc4d1	řecká
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
krásu	krása	k1gFnSc4	krása
krajiny	krajina	k1gFnSc2	krajina
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Miluji	milovat	k5eAaImIp1nS	milovat
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měl	mít	k5eAaImAgInS	mít
tak	tak	k6eAd1	tak
rychlé	rychlý	k2eAgFnPc4d1	rychlá
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
vzpurný	vzpurný	k2eAgInSc1d1	vzpurný
<g/>
,	,	kIx,	,
pohrdal	pohrdat	k5eAaImAgInS	pohrdat
všemi	všecek	k3xTgMnPc7	všecek
králi	král	k1gMnPc7	král
a	a	k8xC	a
tradicemi	tradice	k1gFnPc7	tradice
a	a	k8xC	a
lidskou	lidský	k2eAgFnSc4d1	lidská
masu	masa	k1gFnSc4	masa
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nicotnou	nicotný	k2eAgFnSc4d1	nicotná
<g/>
,	,	kIx,	,
dobrou	dobrý	k2eAgFnSc4d1	dobrá
leda	leda	k6eAd1	leda
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
smrt	smrt	k1gFnSc4	smrt
zkosila	zkosit	k5eAaPmAgFnS	zkosit
jako	jako	k8xS	jako
stébla	stéblo	k1gNnPc1	stéblo
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
svou	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
svatou	svatá	k1gFnSc4	svatá
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
jen	jen	k9	jen
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
sny	sen	k1gInPc4	sen
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
smutek	smutek	k1gInSc1	smutek
měl	mít	k5eAaImAgInS	mít
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
větší	veliký	k2eAgFnSc1d2	veliký
cenu	cena	k1gFnSc4	cena
než	než	k8xS	než
celý	celý	k2eAgInSc4d1	celý
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Christomanos-	Christomanos-	k1gFnSc2	Christomanos-
řecký	řecký	k2eAgMnSc1d1	řecký
učitel	učitel	k1gMnSc1	učitel
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.107	.107	k4	.107
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Říjen	říjen	k1gInSc4	říjen
1861	[number]	k4	1861
strávila	strávit	k5eAaPmAgFnS	strávit
císařovna	císařovna	k1gFnSc1	císařovna
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Červen	červen	k1gInSc1	červen
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Kůru	kůra	k1gFnSc4	kůra
zde	zde	k6eAd1	zde
opakovala	opakovat	k5eAaImAgFnS	opakovat
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
1864	[number]	k4	1864
a	a	k8xC	a
1865	[number]	k4	1865
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
začaly	začít	k5eAaPmAgFnP	začít
časté	častý	k2eAgFnPc1d1	častá
cesty	cesta	k1gFnPc1	cesta
do	do	k7c2	do
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
znamenaly	znamenat	k5eAaImAgFnP	znamenat
její	její	k3xOp3gFnSc4	její
vášeň	vášeň	k1gFnSc4	vášeň
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
zem	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1867	[number]	k4	1867
strávila	strávit	k5eAaPmAgFnS	strávit
jako	jako	k9	jako
host	host	k1gMnSc1	host
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Tokay	Tokaa	k1gFnSc2	Tokaa
u	u	k7c2	u
Gyuly	Gyula	k1gMnSc2	Gyula
hraběte	hrabě	k1gMnSc2	hrabě
Adrássyho	Adrássy	k1gMnSc2	Adrássy
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
manželka	manželka	k1gFnSc1	manželka
nebyla	být	k5eNaImAgFnS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
později	pozdě	k6eAd2	pozdě
dalo	dát	k5eAaPmAgNnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
dohadům	dohad	k1gInPc3	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
sloky	sloka	k1gFnPc1	sloka
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Kabinet	kabinet	k1gInSc4	kabinet
mu	on	k3xPp3gMnSc3	on
věnovala	věnovat	k5eAaPmAgFnS	věnovat
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
,	,	kIx,	,
týkají	týkat	k5eAaImIp3nP	týkat
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnSc2	jejich
vřelého	vřelý	k2eAgNnSc2d1	vřelé
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
i	i	k9	i
otcovství	otcovství	k1gNnSc6	otcovství
posledního	poslední	k2eAgNnSc2d1	poslední
dítěte	dítě	k1gNnSc2	dítě
Marie	Marie	k1gFnSc1	Marie
Valerie	Valerie	k1gFnSc1	Valerie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1867	[number]	k4	1867
se	se	k3xPyFc4	se
v	v	k7c6	v
Salzburku	Salzburk	k1gInSc6	Salzburk
setkaly	setkat	k5eAaPmAgFnP	setkat
oba	dva	k4xCgInPc4	dva
(	(	kIx(	(
<g/>
francouzský	francouzský	k2eAgInSc1d1	francouzský
a	a	k8xC	a
rakouský	rakouský	k2eAgInSc1d1	rakouský
<g/>
)	)	kIx)	)
císařské	císařský	k2eAgInPc1d1	císařský
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
pobytu	pobyt	k1gInSc2	pobyt
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
i	i	k9	i
slavná	slavný	k2eAgFnSc1d1	slavná
soutěž	soutěž	k1gFnSc1	soutěž
měření	měření	k1gNnPc2	měření
lýtek	lýtko	k1gNnPc2	lýtko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1874	[number]	k4	1874
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
císařovna	císařovna	k1gFnSc1	císařovna
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívila	navštívit	k5eAaPmAgFnS	navštívit
anglickou	anglický	k2eAgFnSc4d1	anglická
královnu	královna	k1gFnSc4	královna
Viktorii	Viktoria	k1gFnSc4	Viktoria
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Osborne	Osborn	k1gInSc5	Osborn
(	(	kIx(	(
<g/>
vděčila	vděčit	k5eAaImAgFnS	vděčit
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
vypůjčení	vypůjčení	k1gNnSc4	vypůjčení
britské	britský	k2eAgFnSc2d1	britská
královské	královský	k2eAgFnSc2d1	královská
jachty	jachta	k1gFnSc2	jachta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navštívila	navštívit	k5eAaPmAgFnS	navštívit
i	i	k9	i
ústav	ústav	k1gInSc4	ústav
pro	pro	k7c4	pro
choromyslné	choromyslný	k2eAgInPc4d1	choromyslný
a	a	k8xC	a
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
loveckých	lovecký	k2eAgFnPc2d1	lovecká
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
delším	dlouhý	k2eAgInSc6d2	delší
pobytu	pobyt	k1gInSc6	pobyt
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
Bad	Bad	k1gFnSc6	Bad
Ischlu	Ischl	k1gInSc2	Ischl
prožila	prožít	k5eAaPmAgFnS	prožít
léto	léto	k1gNnSc4	léto
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
pohřbu	pohřeb	k1gInSc2	pohřeb
Franze	Franze	k1gFnSc1	Franze
Deáka	Deáka	k1gFnSc1	Deáka
navštívila	navštívit	k5eAaPmAgFnS	navštívit
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1876	[number]	k4	1876
Uhry	Uhry	k1gFnPc4	Uhry
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1876	[number]	k4	1876
odcestovala	odcestovat	k5eAaPmAgFnS	odcestovat
na	na	k7c4	na
jezdecké	jezdecký	k2eAgInPc4d1	jezdecký
hony	hon	k1gInPc4	hon
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1877	[number]	k4	1877
s	s	k7c7	s
doprovodem	doprovod	k1gInSc7	doprovod
neteře	neteř	k1gFnSc2	neteř
hraběnky	hraběnka	k1gFnSc2	hraběnka
Marie	Maria	k1gFnSc2	Maria
Wallersee	Wallerse	k1gFnSc2	Wallerse
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkým	velký	k2eAgMnPc3d1	velký
obdivovatelům	obdivovatel	k1gMnPc3	obdivovatel
patřil	patřit	k5eAaImAgInS	patřit
Kníže	kníže	k1gMnSc1	kníže
Eduard	Eduard	k1gMnSc1	Eduard
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
VII	VII	kA	VII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Alžběta	Alžběta	k1gFnSc1	Alžběta
napsala	napsat	k5eAaBmAgFnS	napsat
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
soukromého	soukromý	k2eAgNnSc2d1	soukromé
setkání	setkání	k1gNnSc2	setkání
úsměvnou	úsměvný	k2eAgFnSc4d1	úsměvná
báseň	báseň	k1gFnSc4	báseň
There	Ther	k1gInSc5	Ther
is	is	k?	is
somebody	somebod	k1gInPc7	somebod
coming	coming	k1gInSc1	coming
upstairs	upstairs	k6eAd1	upstairs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
sem	sem	k6eAd1	sem
jela	jet	k5eAaImAgFnS	jet
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Rudolfem	Rudolf	k1gMnSc7	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
1879	[number]	k4	1879
to	ten	k3xDgNnSc1	ten
sem	sem	k6eAd1	sem
táhlo	táhnout	k5eAaImAgNnS	táhnout
Alžbětu	Alžběta	k1gFnSc4	Alžběta
kvůli	kvůli	k7c3	kvůli
možnosti	možnost	k1gFnSc3	možnost
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nebezpečnějších	bezpečný	k2eNgInPc2d2	nebezpečnější
honů	hon	k1gInPc2	hon
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
oslavila	oslavit	k5eAaPmAgFnS	oslavit
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
svatbu	svatba	k1gFnSc4	svatba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1883	[number]	k4	1883
navštívila	navštívit	k5eAaPmAgFnS	navštívit
lázně	lázeň	k1gFnPc4	lázeň
Baden-Baden	Baden-Baden	k2eAgMnSc1d1	Baden-Baden
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stupňující	stupňující	k2eAgFnPc4d1	stupňující
se	se	k3xPyFc4	se
revmatické	revmatický	k2eAgFnSc2d1	revmatická
potíže	potíž	k1gFnSc2	potíž
podnikla	podniknout	k5eAaPmAgFnS	podniknout
cestu	cesta	k1gFnSc4	cesta
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1884	[number]	k4	1884
do	do	k7c2	do
Wiesbadenu	Wiesbaden	k1gInSc2	Wiesbaden
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
svěřila	svěřit	k5eAaPmAgFnS	svěřit
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
doktora	doktor	k1gMnSc2	doktor
Metzgera	Metzger	k1gMnSc2	Metzger
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
masážních	masážní	k2eAgFnPc6d1	masážní
kůrách	kůra	k1gFnPc6	kůra
Alžběta	Alžběta	k1gFnSc1	Alžběta
závislá	závislý	k2eAgFnSc1d1	závislá
<g/>
,	,	kIx,	,
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Amsterodamu	Amsterodam	k1gInSc2	Amsterodam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
ho	on	k3xPp3gMnSc4	on
císařovna	císařovna	k1gFnSc1	císařovna
následovala	následovat	k5eAaImAgFnS	následovat
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
její	její	k3xOp3gNnSc4	její
nadšení	nadšení	k1gNnSc4	nadšení
pro	pro	k7c4	pro
tohoto	tento	k3xDgMnSc4	tento
lékaře	lékař	k1gMnSc4	lékař
nesdílel	sdílet	k5eNaImAgMnS	sdílet
a	a	k8xC	a
varoval	varovat	k5eAaImAgMnS	varovat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
vývěsní	vývěsní	k2eAgInSc4d1	vývěsní
štít	štít	k1gInSc4	štít
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
si	se	k3xPyFc3	se
i	i	k9	i
sama	sám	k3xTgFnSc1	sám
císařovna	císařovna	k1gFnSc1	císařovna
stěžovala	stěžovat	k5eAaImAgFnS	stěžovat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
hrubé	hrubý	k2eAgFnPc4d1	hrubá
metody	metoda	k1gFnPc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Patřilo	patřit	k5eAaImAgNnS	patřit
ale	ale	k9	ale
k	k	k7c3	k
rysům	rys	k1gInPc3	rys
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrubé	hrubý	k2eAgNnSc1d1	hrubé
zacházení	zacházení	k1gNnSc1	zacházení
–	–	k?	–
spíše	spíše	k9	spíše
než	než	k8xS	než
mírnost	mírnost	k1gFnSc1	mírnost
a	a	k8xC	a
shovívavost	shovívavost	k1gFnSc1	shovívavost
manžela	manžel	k1gMnSc2	manžel
–	–	k?	–
snášela	snášet	k5eAaImAgFnS	snášet
bez	bez	k7c2	bez
velkého	velký	k2eAgInSc2d1	velký
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Lankraběnka	Lankraběnka	k1gFnSc1	Lankraběnka
Fürstenbergová	Fürstenbergový	k2eAgFnSc1d1	Fürstenbergová
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
udivena	udivit	k5eAaPmNgFnS	udivit
<g/>
,	,	kIx,	,
když	když	k8xS	když
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
paní	paní	k1gFnSc1	paní
dá	dát	k5eAaPmIp3nS	dát
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
neotesaným	otesaný	k2eNgMnSc7d1	neotesaný
doktorem	doktor	k1gMnSc7	doktor
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
poslušně	poslušně	k6eAd1	poslušně
dbá	dbát	k5eAaImIp3nS	dbát
jeho	jeho	k3xOp3gInSc1	jeho
rad	rad	k1gInSc1	rad
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
žádný	žádný	k3yNgMnSc1	žádný
lékař	lékař	k1gMnSc1	lékař
ještě	ještě	k6eAd1	ještě
nedocílil	docílit	k5eNaPmAgMnS	docílit
<g/>
)	)	kIx)	)
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
si	se	k3xPyFc3	se
od	od	k7c2	od
něho	on	k3xPp3gInSc2	on
nechá	nechat	k5eAaPmIp3nS	nechat
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
líbit	líbit	k5eAaImF	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zestárlá	zestárlý	k2eAgFnSc1d1	zestárlá
a	a	k8xC	a
vrásčitá	vrásčitý	k2eAgFnSc1d1	vrásčitá
a	a	k8xC	a
že	že	k8xS	že
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
bude	být	k5eAaImBp3nS	být
stařena	stařena	k1gFnSc1	stařena
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
neuspořádá	uspořádat	k5eNaPmIp3nS	uspořádat
život	život	k1gInSc4	život
jinak	jinak	k6eAd1	jinak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
císařovna	císařovna	k1gFnSc1	císařovna
léčbě	léčba	k1gFnSc3	léčba
Metzgera	Metzgera	k1gFnSc1	Metzgera
unikla	uniknout	k5eAaPmAgFnS	uniknout
<g/>
,	,	kIx,	,
upadla	upadnout	k5eAaPmAgFnS	upadnout
znovu	znovu	k6eAd1	znovu
do	do	k7c2	do
starého	starý	k2eAgInSc2d1	starý
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
s	s	k7c7	s
hladověním	hladovění	k1gNnSc7	hladovění
a	a	k8xC	a
cestováním	cestování	k1gNnSc7	cestování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1884	[number]	k4	1884
se	se	k3xPyFc4	se
císařovna	císařovna	k1gFnSc1	císařovna
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
setkala	setkat	k5eAaPmAgFnS	setkat
se	s	k7c7	s
spřízněnou	spřízněný	k2eAgFnSc7d1	spřízněná
duší	duše	k1gFnSc7	duše
<g/>
,	,	kIx,	,
veršující	veršující	k2eAgFnSc7d1	veršující
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
Rumunskou	rumunský	k2eAgFnSc7d1	rumunská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Carmen	Carmen	k1gInSc4	Carmen
Sylva	Sylva	k1gFnSc1	Sylva
publikovala	publikovat	k5eAaBmAgFnS	publikovat
pohádky	pohádka	k1gFnSc2	pohádka
a	a	k8xC	a
pověsti	pověst	k1gFnSc2	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Říjen	říjen	k1gInSc4	říjen
1885	[number]	k4	1885
trávila	trávit	k5eAaImAgFnS	trávit
Alžběta	Alžběta	k1gFnSc1	Alžběta
měsíc	měsíc	k1gInSc4	měsíc
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
jachty	jachta	k1gFnSc2	jachta
Murammare	Murammar	k1gMnSc5	Murammar
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
1886	[number]	k4	1886
střídala	střídat	k5eAaImAgFnS	střídat
lázně	lázeň	k1gFnPc4	lázeň
kvůli	kvůli	k7c3	kvůli
bolesti	bolest	k1gFnSc3	bolest
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
strávila	strávit	k5eAaPmAgFnS	strávit
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Gödöllö	Gödöllö	k1gFnSc2	Gödöllö
<g/>
,	,	kIx,	,
zimu	zima	k1gFnSc4	zima
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
jaro	jaro	k6eAd1	jaro
1887	[number]	k4	1887
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Uher	Uhry	k1gFnPc2	Uhry
potkala	potkat	k5eAaPmAgFnS	potkat
opět	opět	k6eAd1	opět
rumunskou	rumunský	k2eAgFnSc4d1	rumunská
královnu	královna	k1gFnSc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Rumunský	rumunský	k2eAgInSc1d1	rumunský
zámek	zámek	k1gInSc1	zámek
Peleš	peleš	k1gFnSc4	peleš
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
elektrifikován	elektrifikovat	k5eAaBmNgMnS	elektrifikovat
(	(	kIx(	(
<g/>
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
návštěvy	návštěva	k1gFnSc2	návštěva
příznivce	příznivec	k1gMnSc2	příznivec
korunního	korunní	k2eAgMnSc2d1	korunní
prince	princ	k1gMnSc2	princ
Rudolfa	Rudolf	k1gMnSc2	Rudolf
a	a	k8xC	a
korunní	korunní	k2eAgFnSc2d1	korunní
princezny	princezna	k1gFnSc2	princezna
Štefanie	Štefanie	k1gFnSc2	Štefanie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léto	léto	k1gNnSc1	léto
strávila	strávit	k5eAaPmAgFnS	strávit
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
v	v	k7c6	v
Norfolku	Norfolek	k1gInSc6	Norfolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc6	Hamburk
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgNnP	setkat
se	s	k7c7	s
sestrou	sestra	k1gFnSc7	sestra
Heina	Hein	k1gMnSc2	Hein
<g/>
,	,	kIx,	,
podzim	podzim	k1gInSc4	podzim
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
cestovala	cestovat	k5eAaImAgFnS	cestovat
v	v	k7c6	v
intervalech	interval	k1gInPc6	interval
do	do	k7c2	do
stejných	stejná	k1gFnPc2	stejná
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
přerušila	přerušit	k5eAaPmAgFnS	přerušit
cestu	cesta	k1gFnSc4	cesta
kvůli	kvůli	k7c3	kvůli
miléniu	milénium	k1gNnSc3	milénium
milovaného	milovaný	k2eAgNnSc2d1	milované
uherského	uherský	k2eAgNnSc2d1	Uherské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
její	její	k3xOp3gNnSc1	její
poslední	poslední	k2eAgNnSc1d1	poslední
velké	velký	k2eAgNnSc1d1	velké
mytí	mytí	k1gNnSc1	mytí
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rukou	ruka	k1gFnSc7	ruka
vraha	vrah	k1gMnSc2	vrah
skončil	skončit	k5eAaPmAgMnS	skončit
její	její	k3xOp3gInSc4	její
neklidný	klidný	k2eNgInSc4d1	neklidný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
postupem	postupem	k7c2	postupem
let	léto	k1gNnPc2	léto
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
podléhala	podléhat	k5eAaImAgFnS	podléhat
depresím	deprese	k1gFnPc3	deprese
<g/>
,	,	kIx,	,
myslela	myslet	k5eAaImAgFnS	myslet
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
po	po	k7c6	po
tragické	tragický	k2eAgFnSc6d1	tragická
smrti	smrt	k1gFnSc6	smrt
syna	syn	k1gMnSc4	syn
Rudolfa	Rudolf	k1gMnSc4	Rudolf
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
se	se	k3xPyFc4	se
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
milenkou	milenka	k1gFnSc7	milenka
Mary	Mary	k1gFnSc7	Mary
Vetserovou	Vetserová	k1gFnSc7	Vetserová
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Mayerling	Mayerling	k1gInSc1	Mayerling
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
zcela	zcela	k6eAd1	zcela
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
neoblékala	oblékat	k5eNaImAgFnS	oblékat
jiné	jiný	k2eAgInPc4d1	jiný
než	než	k8xS	než
černé	černý	k2eAgInPc4d1	černý
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Rozdala	rozdat	k5eAaPmAgFnS	rozdat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
svoje	svůj	k3xOyFgInPc4	svůj
šperky	šperk	k1gInPc4	šperk
a	a	k8xC	a
neklidně	klidně	k6eNd1	klidně
spěchala	spěchat	k5eAaImAgFnS	spěchat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
cíle	cíl	k1gInSc2	cíl
k	k	k7c3	k
druhému	druhý	k4xOgMnSc3	druhý
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
v	v	k7c6	v
září	září	k1gNnSc6	září
1898	[number]	k4	1898
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
v	v	k7c6	v
Territetu	Territet	k1gInSc6	Territet
u	u	k7c2	u
Montreux	Montreux	k1gInSc1	Montreux
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
společnicí	společnice	k1gFnSc7	společnice
dvorní	dvorní	k2eAgFnSc7d1	dvorní
dámou	dáma	k1gFnSc7	dáma
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Stárayovou	Stárayová	k1gFnSc7	Stárayová
vydala	vydat	k5eAaPmAgFnS	vydat
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
blízké	blízký	k2eAgFnSc2d1	blízká
přítelkyně	přítelkyně	k1gFnSc2	přítelkyně
baronky	baronka	k1gFnSc2	baronka
Julie	Julie	k1gFnSc2	Julie
Rothschildové	Rothschildová	k1gFnSc2	Rothschildová
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
Pregny	Pregna	k1gFnSc2	Pregna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
srdečném	srdečný	k2eAgNnSc6d1	srdečné
přivítání	přivítání	k1gNnSc6	přivítání
spolu	spolu	k6eAd1	spolu
dámy	dáma	k1gFnSc2	dáma
něco	něco	k3yInSc4	něco
lehkého	lehký	k2eAgMnSc4d1	lehký
pojedly	pojíst	k5eAaPmAgFnP	pojíst
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jim	on	k3xPp3gMnPc3	on
hrál	hrát	k5eAaImAgInS	hrát
diskrétně	diskrétně	k6eAd1	diskrétně
orchestr	orchestr	k1gInSc1	orchestr
a	a	k8xC	a
dámy	dáma	k1gFnPc1	dáma
se	se	k3xPyFc4	se
živě	živě	k6eAd1	živě
bavily	bavit	k5eAaImAgFnP	bavit
o	o	k7c6	o
Heinrichu	Heinrich	k1gMnSc6	Heinrich
Heinovi	Hein	k1gMnSc6	Hein
a	a	k8xC	a
císařovna	císařovna	k1gFnSc1	císařovna
na	na	k7c6	na
zdraví	zdraví	k1gNnSc6	zdraví
oblíbenému	oblíbený	k2eAgMnSc3d1	oblíbený
básníkovi	básník	k1gMnSc3	básník
připila	připít	k5eAaPmAgFnS	připít
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
skvělé	skvělý	k2eAgFnSc6d1	skvělá
náladě	nálada	k1gFnSc6	nálada
<g/>
,	,	kIx,	,
dokud	dokud	k6eAd1	dokud
nezahlédla	zahlédnout	k5eNaPmAgFnS	zahlédnout
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
hostí	hostit	k5eAaImIp3nS	hostit
záznam	záznam	k1gInSc4	záznam
jejího	její	k3xOp3gMnSc2	její
zemřelého	zemřelý	k1gMnSc2	zemřelý
syna	syn	k1gMnSc2	syn
Rudolfa	Rudolf	k1gMnSc2	Rudolf
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Pregnu	Pregna	k1gFnSc4	Pregna
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
její	její	k3xOp3gMnSc1	její
život	život	k1gInSc4	život
ukončil	ukončit	k5eAaPmAgMnS	ukončit
italský	italský	k2eAgMnSc1d1	italský
anarchista	anarchista	k1gMnSc1	anarchista
Luigi	Luigi	k1gNnSc2	Luigi
Lucheni	Luchen	k2eAgMnPc1d1	Luchen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
probodl	probodnout	k5eAaPmAgMnS	probodnout
pilníkem	pilník	k1gInSc7	pilník
(	(	kIx(	(
<g/>
nástroj	nástroj	k1gInSc1	nástroj
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
8,5	[number]	k4	8,5
centimetru	centimetr	k1gInSc2	centimetr
<g/>
,	,	kIx,	,
zlomil	zlomit	k5eAaPmAgMnS	zlomit
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
žebro	žebro	k1gNnSc4	žebro
a	a	k8xC	a
pronikl	proniknout	k5eAaPmAgInS	proniknout
mezižeberním	mezižeberní	k2eAgInSc7d1	mezižeberní
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
prorazil	prorazit	k5eAaPmAgInS	prorazit
dolní	dolní	k2eAgInSc1d1	dolní
kraj	kraj	k1gInSc1	kraj
plicního	plicní	k2eAgInSc2d1	plicní
laloku	lalok	k1gInSc2	lalok
a	a	k8xC	a
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
levou	levý	k2eAgFnSc4d1	levá
část	část	k1gFnSc4	část
srdeční	srdeční	k2eAgFnSc2d1	srdeční
komory	komora	k1gFnSc2	komora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
ženevského	ženevský	k2eAgInSc2d1	ženevský
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Podvědomě	podvědomě	k6eAd1	podvědomě
jsem	být	k5eAaImIp1nS	být
udělala	udělat	k5eAaPmAgFnS	udělat
krok	krok	k1gInSc4	krok
kupředu	kupředu	k6eAd1	kupředu
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jsem	být	k5eAaImIp1nS	být
císařovnu	císařovna	k1gFnSc4	císařovna
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
kryla	krýt	k5eAaImAgFnS	krýt
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
udělal	udělat	k5eAaPmAgMnS	udělat
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
zakopl	zakopnout	k5eAaPmAgMnS	zakopnout
<g/>
,	,	kIx,	,
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
kupředu	kupředu	k6eAd1	kupředu
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
okamžiku	okamžik	k1gInSc6	okamžik
také	také	k9	také
pěstí	pěstit	k5eAaImIp3nS	pěstit
proti	proti	k7c3	proti
císařovně	císařovna	k1gFnSc3	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
zasažená	zasažený	k2eAgFnSc1d1	zasažená
bleskem	blesk	k1gInSc7	blesk
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
hlesu	hles	k1gInSc2	hles
zvrátila	zvrátit	k5eAaPmAgFnS	zvrátit
dozadu	dozadu	k6eAd1	dozadu
a	a	k8xC	a
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
se	s	k7c7	s
zoufalým	zoufalý	k2eAgInSc7d1	zoufalý
výkřikem	výkřik	k1gInSc7	výkřik
sklonila	sklonit	k5eAaPmAgFnS	sklonit
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
otevřela	otevřít	k5eAaPmAgFnS	otevřít
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
rozhlédla	rozhlédnout	k5eAaPmAgFnS	rozhlédnout
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
pohled	pohled	k1gInSc1	pohled
prozrazoval	prozrazovat	k5eAaImAgInS	prozrazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
opírajíc	opírat	k5eAaImSgFnS	opírat
o	o	k7c4	o
mne	já	k3xPp1nSc4	já
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
zvedla	zvednout	k5eAaPmAgFnS	zvednout
a	a	k8xC	a
vstala	vstát	k5eAaPmAgFnS	vstát
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
leskly	lesknout	k5eAaImAgInP	lesknout
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
zrudlou	zrudlý	k2eAgFnSc4d1	zrudlá
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
nádherné	nádherný	k2eAgInPc1d1	nádherný
copy	cop	k1gInPc1	cop
uvolněné	uvolněný	k2eAgInPc1d1	uvolněný
pádem	pád	k1gInSc7	pád
jí	on	k3xPp3gFnSc3	on
visely	viset	k5eAaImAgFnP	viset
jako	jako	k9	jako
ležérně	ležérně	k6eAd1	ležérně
spletený	spletený	k2eAgInSc1d1	spletený
věnec	věnec	k1gInSc1	věnec
kolem	kolem	k7c2	kolem
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nevýslovně	výslovně	k6eNd1	výslovně
krásná	krásný	k2eAgFnSc1d1	krásná
a	a	k8xC	a
majestátní	majestátní	k2eAgFnSc1d1	majestátní
<g/>
...	...	k?	...
<g/>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
odevšad	odevšad	k6eAd1	odevšad
sbíhali	sbíhat	k5eAaImAgMnP	sbíhat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
zděšení	zděšení	k1gNnSc1	zděšení
brutálním	brutální	k2eAgInSc7d1	brutální
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
se	se	k3xPyFc4	se
císařovny	císařovna	k1gFnPc4	císařovna
vyptávali	vyptávat	k5eAaImAgMnP	vyptávat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
opravdu	opravdu	k6eAd1	opravdu
nic	nic	k3yNnSc1	nic
nepřihodilo	přihodit	k5eNaPmAgNnS	přihodit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
ona	onen	k3xDgFnSc1	onen
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
srdečně	srdečně	k6eAd1	srdečně
a	a	k8xC	a
vlídně	vlídně	k6eAd1	vlídně
každému	každý	k3xTgMnSc3	každý
děkovala	děkovat	k5eAaImAgFnS	děkovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
jazyce	jazyk	k1gInSc6	jazyk
<g/>
...	...	k?	...
<g/>
Mám	mít	k5eAaImIp1nS	mít
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
mě	já	k3xPp1nSc4	já
trochu	trochu	k6eAd1	trochu
bolí	bolet	k5eAaImIp3nP	bolet
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
<g/>
,	,	kIx,	,
pravila	pravit	k5eAaBmAgFnS	pravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsem	být	k5eNaImIp1nS	být
si	se	k3xPyFc3	se
tím	ten	k3xDgNnSc7	ten
jistá	jistý	k2eAgFnSc1d1	jistá
<g/>
.	.	kIx.	.
</s>
<s>
Došly	dojít	k5eAaPmAgFnP	dojít
jsme	být	k5eAaImIp1nP	být
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Lodní	lodní	k2eAgInSc4d1	lodní
můstek	můstek	k1gInSc4	můstek
ještě	ještě	k6eAd1	ještě
přešla	přejít	k5eAaPmAgFnS	přejít
lehkým	lehký	k2eAgInSc7d1	lehký
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
sotva	sotva	k6eAd1	sotva
však	však	k9	však
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
udělalo	udělat	k5eAaPmAgNnS	udělat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
mdlo	mdlo	k6eAd1	mdlo
<g/>
.	.	kIx.	.
</s>
<s>
Potřebuji	potřebovat	k5eAaImIp1nS	potřebovat
se	se	k3xPyFc4	se
o	o	k7c4	o
vás	vy	k3xPp2nPc4	vy
opřít	opřít	k5eAaPmF	opřít
<g/>
,	,	kIx,	,
zamumlala	zamumlat	k5eAaPmAgFnS	zamumlat
přiškrceně	přiškrceně	k6eAd1	přiškrceně
<g/>
...	...	k?	...
<g/>
Císařovna	císařovna	k1gFnSc1	císařovna
spočívala	spočívat	k5eAaImAgFnS	spočívat
smrtelně	smrtelně	k6eAd1	smrtelně
bledá	bledý	k2eAgFnSc1d1	bledá
v	v	k7c6	v
mém	můj	k3xOp1gNnSc6	můj
náručí	náručí	k1gNnSc6	náručí
<g/>
...	...	k?	...
<g/>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dvou	dva	k4xCgInPc2	dva
mužů	muž	k1gMnPc2	muž
jsme	být	k5eAaImIp1nP	být
ji	on	k3xPp3gFnSc4	on
odnesli	odnést	k5eAaPmAgMnP	odnést
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
a	a	k8xC	a
položili	položit	k5eAaPmAgMnP	položit
na	na	k7c4	na
lavici	lavice	k1gFnSc4	lavice
<g/>
..	..	k?	..
<g/>
Madame	madame	k1gFnSc1	madame
Dardelle	Dardelle	k1gFnSc1	Dardelle
dala	dát	k5eAaPmAgFnS	dát
přinést	přinést	k5eAaPmF	přinést
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
kolínskou	kolínská	k1gFnSc4	kolínská
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
hned	hned	k6eAd1	hned
s	s	k7c7	s
oživováním	oživování	k1gNnSc7	oživování
<g />
.	.	kIx.	.
</s>
<s>
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
...	...	k?	...
<g/>
Rozstřihla	rozstřihnout	k5eAaPmAgFnS	rozstřihnout
jsem	být	k5eAaImIp1nS	být
jí	on	k3xPp3gFnSc3	on
šněrovačku	šněrovačka	k1gFnSc4	šněrovačka
<g/>
,	,	kIx,	,
zastrčila	zastrčit	k5eAaPmAgFnS	zastrčit
mezi	mezi	k7c4	mezi
zuby	zub	k1gInPc4	zub
kousek	kousek	k1gInSc4	kousek
cukru	cukr	k1gInSc2	cukr
namočený	namočený	k2eAgInSc4d1	namočený
v	v	k7c6	v
éteru	éter	k1gInSc6	éter
a	a	k8xC	a
projela	projet	k5eAaPmAgFnS	projet
mnou	já	k3xPp1nSc7	já
jiskřička	jiskřička	k1gFnSc1	jiskřička
naděje	naděje	k1gFnPc1	naděje
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
zaslechla	zaslechnout	k5eAaPmAgFnS	zaslechnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
jednou	jednou	k6eAd1	jednou
či	či	k8xC	či
dvakrát	dvakrát	k6eAd1	dvakrát
kousla	kousnout	k5eAaPmAgFnS	kousnout
<g/>
...	...	k?	...
<g/>
Císařovna	císařovna	k1gFnSc1	císařovna
pomalu	pomalu	k6eAd1	pomalu
otevřela	otevřít	k5eAaPmAgFnS	otevřít
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
zmateně	zmateně	k6eAd1	zmateně
rozhlížela	rozhlížet	k5eAaImAgFnS	rozhlížet
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
chtěla	chtít	k5eAaImAgFnS	chtít
zorientovat	zorientovat	k5eAaPmF	zorientovat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zvedat	zvedat	k5eAaImF	zvedat
a	a	k8xC	a
s	s	k7c7	s
naší	náš	k3xOp1gFnSc7	náš
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
posadila	posadit	k5eAaPmAgFnS	posadit
<g/>
.	.	kIx.	.
</s>
<s>
Otočila	otočit	k5eAaPmAgFnS	otočit
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
k	k	k7c3	k
cizí	cizí	k2eAgFnSc3d1	cizí
dámě	dáma	k1gFnSc3	dáma
a	a	k8xC	a
vydechla	vydechnout	k5eAaPmAgFnS	vydechnout
<g/>
:	:	kIx,	:
Merci	Merce	k1gFnSc4	Merce
<g/>
...	...	k?	...
<g/>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
císařovna	císařovna	k1gFnSc1	císařovna
držela	držet	k5eAaImAgFnS	držet
vsedě	vsedě	k6eAd1	vsedě
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
<g/>
,	,	kIx,	,
vypadala	vypadat	k5eAaPmAgFnS	vypadat
velmi	velmi	k6eAd1	velmi
vyčerpaně	vyčerpaně	k6eAd1	vyčerpaně
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
zastřený	zastřený	k2eAgInSc1d1	zastřený
pohled	pohled	k1gInSc1	pohled
<g/>
,	,	kIx,	,
nejistý	jistý	k2eNgInSc1d1	nejistý
a	a	k8xC	a
nechápavý	chápavý	k2eNgMnSc1d1	nechápavý
<g/>
,	,	kIx,	,
smutně	smutně	k6eAd1	smutně
kroužil	kroužit	k5eAaImAgInS	kroužit
kolem	kolem	k6eAd1	kolem
<g/>
...	...	k?	...
<g/>
Oči	oko	k1gNnPc1	oko
zabloudily	zabloudit	k5eAaPmAgInP	zabloudit
k	k	k7c3	k
nebi	nebe	k1gNnSc3	nebe
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zastavily	zastavit	k5eAaPmAgFnP	zastavit
na	na	k7c4	na
Dent	Dent	k1gInSc4	Dent
du	du	k?	du
Midi	Mid	k1gFnSc2	Mid
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
zvolna	zvolna	k6eAd1	zvolna
klouzaly	klouzat	k5eAaImAgFnP	klouzat
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
spočinuly	spočinout	k5eAaPmAgFnP	spočinout
na	na	k7c6	na
mně	já	k3xPp1nSc6	já
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
navždy	navždy	k6eAd1	navždy
vryly	vrýt	k5eAaPmAgFnP	vrýt
do	do	k7c2	do
duše	duše	k1gFnSc2	duše
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
se	s	k7c7	s
mnou	já	k3xPp1nSc7	já
stalo	stát	k5eAaPmAgNnS	stát
<g/>
!	!	kIx.	!
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
její	její	k3xOp3gNnPc4	její
poslední	poslední	k2eAgNnPc4d1	poslední
slova	slovo	k1gNnPc4	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
v	v	k7c6	v
bezvědomí	bezvědomí	k1gNnSc6	bezvědomí
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
lavici	lavice	k1gFnSc4	lavice
<g/>
....	....	k?	....
<g/>
Když	když	k8xS	když
jsem	být	k5eAaImIp1nS	být
roztrhla	roztrhnout	k5eAaPmAgFnS	roztrhnout
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
živůtek	živůtek	k1gInSc4	živůtek
<g/>
,	,	kIx,	,
spatřila	spatřit	k5eAaPmAgFnS	spatřit
jsem	být	k5eAaImIp1nS	být
tmavou	tmavý	k2eAgFnSc4d1	tmavá
skvrnu	skvrna	k1gFnSc4	skvrna
velikosti	velikost	k1gFnSc2	velikost
mince	mince	k1gFnSc2	mince
<g/>
...	...	k?	...
<g/>
Odhrnula	odhrnout	k5eAaPmAgFnS	odhrnout
jsem	být	k5eAaImIp1nS	být
košilku	košilka	k1gFnSc4	košilka
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
srdeční	srdeční	k2eAgFnSc6d1	srdeční
krajině	krajina	k1gFnSc6	krajina
malou	malá	k1gFnSc4	malá
<g/>
,	,	kIx,	,
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
ranku	ranka	k1gFnSc4	ranka
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
ulpěla	ulpět	k5eAaPmAgFnS	ulpět
kapka	kapka	k1gFnSc1	kapka
sražené	sražený	k2eAgFnSc2d1	sražená
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
–	–	k?	–
Luccheni	Lucchen	k2eAgMnPc1d1	Lucchen
<g />
.	.	kIx.	.
</s>
<s>
císařovnu	císařovna	k1gFnSc4	císařovna
probodl	probodnout	k5eAaPmAgMnS	probodnout
dýkou	dýka	k1gFnSc7	dýka
<g/>
...	...	k?	...
<g/>
Ihned	ihned	k6eAd1	ihned
obraťte	obrátit	k5eAaPmRp2nP	obrátit
loď	loď	k1gFnSc4	loď
zpátky	zpátky	k6eAd1	zpátky
<g/>
..	..	k?	..
<g/>
Vpluli	vplout	k5eAaPmAgMnP	vplout
jsme	být	k5eAaImIp1nP	být
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
<g/>
,	,	kIx,	,
položili	položit	k5eAaPmAgMnP	položit
císařovnu	císařovna	k1gFnSc4	císařovna
na	na	k7c4	na
improvizovaná	improvizovaný	k2eAgNnPc4d1	improvizované
nosítka	nosítka	k1gNnPc4	nosítka
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
chopilo	chopit	k5eAaPmAgNnS	chopit
šest	šest	k4xCc1	šest
mužů	muž	k1gMnPc2	muž
<g/>
...	...	k?	...
<g/>
Agonie	agonie	k1gFnSc1	agonie
probíhala	probíhat	k5eAaImAgFnS	probíhat
mírně	mírně	k6eAd1	mírně
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
známek	známka	k1gFnPc2	známka
zápasu	zápas	k1gInSc2	zápas
<g/>
,	,	kIx,	,
císařovna	císařovna	k1gFnSc1	císařovna
jen	jen	k9	jen
neklidně	klidně	k6eNd1	klidně
otočila	otočit	k5eAaPmAgFnS	otočit
hlavu	hlava	k1gFnSc4	hlava
stranou	stranou	k6eAd1	stranou
<g/>
...	...	k?	...
<g/>
V	v	k7c6	v
hotelovém	hotelový	k2eAgInSc6d1	hotelový
pokoji	pokoj	k1gInSc6	pokoj
jsme	být	k5eAaImIp1nP	být
ji	on	k3xPp3gFnSc4	on
položili	položit	k5eAaPmAgMnP	položit
na	na	k7c4	na
postel	postel	k1gFnSc4	postel
<g/>
.	.	kIx.	.
</s>
<s>
Doktor	doktor	k1gMnSc1	doktor
Golay	Golaa	k1gMnSc2	Golaa
už	už	k6eAd1	už
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
<g/>
...	...	k?	...
<g/>
není	být	k5eNaImIp3nS	být
už	už	k6eAd1	už
žádná	žádný	k3yNgFnSc1	žádný
naděje	naděje	k1gFnSc1	naděje
pravil	pravit	k5eAaImAgInS	pravit
po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
...	...	k?	...
<g/>
Přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
udělil	udělit	k5eAaPmAgMnS	udělit
církevní	církevní	k2eAgNnSc4d1	církevní
rozhřešení	rozhřešení	k1gNnSc4	rozhřešení
<g/>
...	...	k?	...
<g/>
Ve	v	k7c4	v
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
lékař	lékař	k1gMnSc1	lékař
to	ten	k3xDgNnSc1	ten
strašné	strašný	k2eAgNnSc1d1	strašné
slovo	slovo	k1gNnSc1	slovo
<g/>
...	...	k?	...
<g/>
Konec	konec	k1gInSc1	konec
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Výňatek	výňatek	k1gInSc1	výňatek
z	z	k7c2	z
<g/>
:	:	kIx,	:
Irma	Irma	k1gFnSc1	Irma
Sztáray	Sztáraa	k1gFnSc2	Sztáraa
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
císařovny	císařovna	k1gFnSc2	císařovna
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
235	[number]	k4	235
a	a	k8xC	a
d.	d.	k?	d.
Vídeň	Vídeň	k1gFnSc1	Vídeň
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
citováno	citován	k2eAgNnSc1d1	citováno
z	z	k7c2	z
Výstavního	výstavní	k2eAgInSc2d1	výstavní
katalogu	katalog	k1gInSc2	katalog
<g/>
/	/	kIx~	/
Atentátník	atentátník	k1gMnSc1	atentátník
chtěl	chtít	k5eAaImAgMnS	chtít
údajně	údajně	k6eAd1	údajně
původně	původně	k6eAd1	původně
zabít	zabít	k5eAaPmF	zabít
prince	princ	k1gMnPc4	princ
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Orléans	Orléansa	k1gFnPc2	Orléansa
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
nepřijel	přijet	k5eNaPmAgMnS	přijet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
císařovnu	císařovna	k1gFnSc4	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
neslo	nést	k5eAaImAgNnS	nést
ztrátu	ztráta	k1gFnSc4	ztráta
velice	velice	k6eAd1	velice
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
vídeňské	vídeňský	k2eAgFnPc1d1	Vídeňská
ženy	žena	k1gFnPc1	žena
se	se	k3xPyFc4	se
činem	čin	k1gInSc7	čin
spáchaným	spáchaný	k2eAgInSc7d1	spáchaný
na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
příslušnici	příslušnice	k1gFnSc6	příslušnice
jejich	jejich	k3xOp3gNnSc2	jejich
pohlaví	pohlaví	k1gNnSc2	pohlaví
cítily	cítit	k5eAaImAgInP	cítit
hluboce	hluboko	k6eAd1	hluboko
dotčené	dotčený	k2eAgInPc1d1	dotčený
<g/>
.	.	kIx.	.
</s>
<s>
Vrahovi	vrahův	k2eAgMnPc1d1	vrahův
poslaly	poslat	k5eAaPmAgFnP	poslat
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
šestnáct	šestnáct	k4xCc4	šestnáct
tisíc	tisíc	k4xCgInSc4	tisíc
stejně	stejně	k6eAd1	stejně
smýšlejících	smýšlející	k2eAgFnPc2d1	smýšlející
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
značně	značně	k6eAd1	značně
brutální	brutální	k2eAgMnSc1d1	brutální
<g/>
.	.	kIx.	.
</s>
<s>
Tělesné	tělesný	k2eAgInPc1d1	tělesný
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
císařovny	císařovna	k1gFnSc2	císařovna
Alžběty	Alžběta	k1gFnSc2	Alžběta
byly	být	k5eAaImAgInP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Beau	Bea	k2eAgFnSc4d1	Bea
Rivage	Rivage	k1gFnSc4	Rivage
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
byl	být	k5eAaImAgInS	být
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
vlakem	vlak	k1gInSc7	vlak
dopraveno	dopraven	k2eAgNnSc4d1	dopraveno
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníci	důstojník	k1gMnPc1	důstojník
stojící	stojící	k2eAgMnPc1d1	stojící
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
věnců	věnec	k1gInPc2	věnec
a	a	k8xC	a
květin	květina	k1gFnPc2	květina
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
ukořistit	ukořistit	k5eAaPmF	ukořistit
alespoň	alespoň	k9	alespoň
kvítek	kvítek	k1gInSc4	kvítek
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
Smuteční	smuteční	k2eAgInSc1d1	smuteční
průvod	průvod	k1gInSc1	průvod
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
bezpočet	bezpočet	k1gInSc4	bezpočet
monarchů	monarcha	k1gMnPc2	monarcha
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
,	,	kIx,	,
osmdesát	osmdesát	k4xCc1	osmdesát
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
a	a	k8xC	a
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
tisíce	tisíc	k4xCgInPc1	tisíc
poddaných	poddaný	k1gMnPc2	poddaný
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
zvonily	zvonit	k5eAaImAgInP	zvonit
všechny	všechen	k3xTgInPc1	všechen
vídeňské	vídeňský	k2eAgInPc1d1	vídeňský
zvony	zvon	k1gInPc1	zvon
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
největším	veliký	k2eAgMnSc7d3	veliký
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
Svatoštěpánského	svatoštěpánský	k2eAgInSc2d1	svatoštěpánský
dómu	dóm	k1gInSc2	dóm
<g/>
,	,	kIx,	,
Pummerinem	Pummerin	k1gInSc7	Pummerin
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ztratil	ztratit	k5eAaPmAgMnS	ztratit
své	svůj	k3xOyFgFnPc4	svůj
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
teď	teď	k6eAd1	teď
oplakával	oplakávat	k5eAaImAgMnS	oplakávat
choť	choť	k1gFnSc4	choť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
životě	život	k1gInSc6	život
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
nestálou	stálý	k2eNgFnSc7d1	nestálá
průvodkyní	průvodkyně	k1gFnSc7	průvodkyně
a	a	k8xC	a
již	již	k6eAd1	již
přesto	přesto	k8xC	přesto
nikdy	nikdy	k6eAd1	nikdy
nepřestal	přestat	k5eNaPmAgInS	přestat
milovat	milovat	k5eAaImF	milovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Včerejší	včerejší	k2eAgInSc1d1	včerejší
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
mě	já	k3xPp1nSc4	já
opět	opět	k6eAd1	opět
zvláště	zvláště	k6eAd1	zvláště
smutný	smutný	k2eAgMnSc1d1	smutný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsem	být	k5eAaImIp1nS	být
znovu	znovu	k6eAd1	znovu
viděl	vidět	k5eAaImAgMnS	vidět
mnoho	mnoho	k4c4	mnoho
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
mi	já	k3xPp1nSc3	já
bolestně	bolestně	k6eAd1	bolestně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k9	jen
s	s	k7c7	s
jistým	jistý	k2eAgNnSc7d1	jisté
nostalgickým	nostalgický	k2eAgNnSc7d1	nostalgické
uspokojením	uspokojení	k1gNnSc7	uspokojení
<g/>
,	,	kIx,	,
připomněly	připomnět	k5eAaPmAgFnP	připomnět
naši	náš	k3xOp1gFnSc4	náš
drahou	drahý	k2eAgFnSc4d1	drahá
zesnulou	zesnulá	k1gFnSc4	zesnulá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ofenu	Ofen	k1gInSc6	Ofen
jsem	být	k5eAaImIp1nS	být
prošel	projít	k5eAaPmAgMnS	projít
všechny	všechen	k3xTgInPc4	všechen
její	její	k3xOp3gInPc4	její
pokoje	pokoj	k1gInPc4	pokoj
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
patře	patro	k1gNnSc6	patro
i	i	k8xC	i
přízemí	přízemí	k1gNnSc6	přízemí
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
bylo	být	k5eAaImAgNnS	být
jako	jako	k9	jako
jindy	jindy	k6eAd1	jindy
přichystáno	přichystat	k5eAaPmNgNnS	přichystat
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
uvítání	uvítání	k1gNnSc4	uvítání
<g/>
..	..	k?	..
<g/>
Nový	nový	k2eAgInSc1d1	nový
balkon	balkon	k1gInSc1	balkon
s	s	k7c7	s
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
vyhlídkou	vyhlídka	k1gFnSc7	vyhlídka
na	na	k7c4	na
Pešť	Pešť	k1gFnSc4	Pešť
a	a	k8xC	a
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
loni	loni	k6eAd1	loni
tak	tak	k6eAd1	tak
těšil	těšit	k5eAaImAgMnS	těšit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
všemožným	všemožný	k2eAgInSc7d1	všemožný
elegantním	elegantní	k2eAgInSc7d1	elegantní
nábytkem	nábytek	k1gInSc7	nábytek
–	–	k?	–
a	a	k8xC	a
přec	přec	k9	přec
je	být	k5eAaImIp3nS	být
teď	teď	k6eAd1	teď
tak	tak	k6eAd1	tak
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
života	život	k1gInSc2	život
a	a	k8xC	a
bez	bez	k7c2	bez
naděje	naděje	k1gFnSc2	naděje
na	na	k7c4	na
shledání	shledání	k1gNnSc4	shledání
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
světě	svět	k1gInSc6	svět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Císař	Císař	k1gMnSc1	Císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Kateřině	Kateřina	k1gFnSc3	Kateřina
Schrattové	Schrattové	k2eAgInSc6d1	Schrattové
z	z	k7c2	z
Gödöllö	Gödöllö	k1gFnPc2	Gödöllö
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1898	[number]	k4	1898
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
víku	víko	k1gNnSc6	víko
sarkofágu	sarkofág	k1gInSc2	sarkofág
v	v	k7c6	v
kapucinské	kapucinský	k2eAgFnSc6d1	kapucinský
hrobce	hrobka	k1gFnSc6	hrobka
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
má	mít	k5eAaImIp3nS	mít
vyrytý	vyrytý	k2eAgInSc1d1	vyrytý
nápis	nápis	k1gInSc1	nápis
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
ELISABETH	ELISABETH	kA	ELISABETH
//	//	k?	//
AMALIA	AMALIA	kA	AMALIA
.	.	kIx.	.
</s>
<s>
EVGENIA	EVGENIA	kA	EVGENIA
//	//	k?	//
IMPERATRIX	IMPERATRIX	kA	IMPERATRIX
.	.	kIx.	.
</s>
<s>
AVSTRIAE	AVSTRIAE	kA	AVSTRIAE
//	//	k?	//
ET	ET	kA	ET
.	.	kIx.	.
</s>
<s>
REGINA	Regina	k1gFnSc1	Regina
.	.	kIx.	.
</s>
<s>
HVNGARIAE	HVNGARIAE	kA	HVNGARIAE
//	//	k?	//
MAXIMILIANI	MAXIMILIANI	kA	MAXIMILIANI
.	.	kIx.	.
</s>
<s>
IOSEPHI	IOSEPHI	kA	IOSEPHI
//	//	k?	//
ET	ET	kA	ET
.	.	kIx.	.
</s>
<s>
LVDOVICAE	LVDOVICAE	kA	LVDOVICAE
//	//	k?	//
DVCVM	DVCVM	kA	DVCVM
.	.	kIx.	.
</s>
<s>
IN	IN	kA	IN
.	.	kIx.	.
</s>
<s>
BAVARIA	BAVARIA	kA	BAVARIA
//	//	k?	//
FILIA	FILIA	kA	FILIA
//	//	k?	//
NATA	NATA	kA	NATA
.	.	kIx.	.
</s>
<s>
MONACHI	MONACHI	kA	MONACHI
//	//	k?	//
DIE	DIE	kA	DIE
.	.	kIx.	.
</s>
<s>
XXIV	XXIV	kA	XXIV
.	.	kIx.	.
</s>
<s>
MENSIS	MENSIS	kA	MENSIS
.	.	kIx.	.
</s>
<s>
DECEMBRIS	DECEMBRIS	kA	DECEMBRIS
//	//	k?	//
ANNI	ANNI	kA	ANNI
.	.	kIx.	.
</s>
<s>
MDCCCXXXVII	MDCCCXXXVII	kA	MDCCCXXXVII
//	//	k?	//
NVPTA	NVPTA	kA	NVPTA
//	//	k?	//
FRANCISCO	FRANCISCO	kA	FRANCISCO
.	.	kIx.	.
</s>
<s>
IOSEPHO	IOSEPHO	kA	IOSEPHO
.	.	kIx.	.
</s>
<s>
I	I	kA	I
//	//	k?	//
IMPERATORI	IMPERATORI	kA	IMPERATORI
//	//	k?	//
VINDOBONAE	VINDOBONAE	kA	VINDOBONAE
.	.	kIx.	.
</s>
<s>
DIE	DIE	kA	DIE
.	.	kIx.	.
</s>
<s>
XXIV	XXIV	kA	XXIV
.	.	kIx.	.
</s>
<s>
M	M	kA	M
.	.	kIx.	.
APRILIS	APRILIS	kA	APRILIS
//	//	k?	//
A	A	kA	A
.	.	kIx.	.
MDCCCLIV	MDCCCLIV	kA	MDCCCLIV
//	//	k?	//
CORONATA	CORONATA	kA	CORONATA
.	.	kIx.	.
</s>
<s>
REGINA	Regina	k1gFnSc1	Regina
.	.	kIx.	.
</s>
<s>
HVNGARIAE	HVNGARIAE	kA	HVNGARIAE
//	//	k?	//
BVDAE	BVDAE	kA	BVDAE
.	.	kIx.	.
</s>
<s>
DIE	DIE	kA	DIE
.	.	kIx.	.
</s>
<s>
VIII	VIII	kA	VIII
.	.	kIx.	.
</s>
<s>
M	M	kA	M
.	.	kIx.	.
IVNII	IVNII	kA	IVNII
.	.	kIx.	.
</s>
<s>
A	A	kA	A
.	.	kIx.	.
MDCCCLXVII	MDCCCLXVII	kA	MDCCCLXVII
//	//	k?	//
DENATA	DENATA	kA	DENATA
.	.	kIx.	.
</s>
<s>
GENEVAE	GENEVAE	kA	GENEVAE
//	//	k?	//
DIE	DIE	kA	DIE
.	.	kIx.	.
</s>
<s>
X	X	kA	X
.	.	kIx.	.
M	M	kA	M
.	.	kIx.	.
SEPTEMBRIS	SEPTEMBRIS	kA	SEPTEMBRIS
//	//	k?	//
A	A	kA	A
.	.	kIx.	.
MDCCCIIC	MDCCCIIC	kA	MDCCCIIC
//	//	k?	//
H	H	kA	H
.	.	kIx.	.
S	s	k7c7	s
.	.	kIx.	.
E	E	kA	E
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podnikala	podnikat	k5eAaImAgFnS	podnikat
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
a	a	k8xC	a
náročné	náročný	k2eAgFnPc4d1	náročná
túry	túra	k1gFnPc4	túra
za	za	k7c2	za
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Vycházky	vycházka	k1gFnPc1	vycházka
trvaly	trvat	k5eAaImAgFnP	trvat
čtyři	čtyři	k4xCgFnPc4	čtyři
nebo	nebo	k8xC	nebo
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
unavená	unavený	k2eAgFnSc1d1	unavená
<g/>
.	.	kIx.	.
</s>
<s>
Dokázala	dokázat	k5eAaPmAgFnS	dokázat
ovšem	ovšem	k9	ovšem
pochodovat	pochodovat	k5eAaImF	pochodovat
devět	devět	k4xCc4	devět
či	či	k8xC	či
deset	deset	k4xCc4	deset
hodin	hodina	k1gFnPc2	hodina
bez	bez	k7c2	bez
přestávky	přestávka	k1gFnSc2	přestávka
a	a	k8xC	a
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
únavy	únava	k1gFnSc2	únava
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
takové	takový	k3xDgFnSc2	takový
chůze	chůze	k1gFnSc2	chůze
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
učil	učít	k5eAaPmAgMnS	učít
jejich	jejich	k3xOp3gNnPc4	jejich
otec	otec	k1gMnSc1	otec
Max	Max	k1gMnSc1	Max
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
již	již	k6eAd1	již
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
dětství	dětství	k1gNnSc6	dětství
(	(	kIx(	(
<g/>
,,	,,	k?	,,
<g/>
...	...	k?	...
<g/>
během	během	k7c2	během
dalšího	další	k2eAgInSc2d1	další
kroku	krok	k1gInSc2	krok
si	se	k3xPyFc3	se
musíte	muset	k5eAaImIp2nP	muset
umět	umět	k5eAaImF	umět
odpočinout	odpočinout	k5eAaPmF	odpočinout
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
nejste	být	k5eNaImIp2nP	být
unavení	unavený	k2eAgMnPc1d1	unavený
a	a	k8xC	a
můžete	moct	k5eAaImIp2nP	moct
pochodovat	pochodovat	k5eAaImF	pochodovat
třeba	třeba	k6eAd1	třeba
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
její	její	k3xOp3gFnPc1	její
dámy	dáma	k1gFnPc1	dáma
nebyly	být	k5eNaImAgFnP	být
stejně	stejně	k6eAd1	stejně
disponované	disponovaný	k2eAgFnPc1d1	disponovaná
a	a	k8xC	a
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
služeb	služba	k1gFnPc2	služba
postupně	postupně	k6eAd1	postupně
upustily	upustit	k5eAaPmAgInP	upustit
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Nakonec	nakonec	k6eAd1	nakonec
neúměrné	úměrný	k2eNgNnSc1d1	neúměrné
sportování	sportování	k1gNnSc1	sportování
podlomilo	podlomit	k5eAaPmAgNnS	podlomit
zdraví	zdraví	k1gNnSc4	zdraví
i	i	k9	i
císařovně	císařovna	k1gFnSc3	císařovna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jedla	jíst	k5eAaImAgFnS	jíst
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
při	při	k7c6	při
životě	život	k1gInSc6	život
<g/>
"	"	kIx"	"
/	/	kIx~	/
<g/>
Wallersee	Wallersee	k1gInSc1	Wallersee
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
54	[number]	k4	54
a	a	k8xC	a
d.	d.	k?	d.
<g/>
/	/	kIx~	/
Od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
byla	být	k5eAaImAgFnS	být
vynikající	vynikající	k2eAgFnSc1d1	vynikající
jezdkyně	jezdkyně	k1gFnSc1	jezdkyně
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
otec	otec	k1gMnSc1	otec
ji	on	k3xPp3gFnSc4	on
učil	učit	k5eAaImAgMnS	učit
i	i	k9	i
akrobatickým	akrobatický	k2eAgInPc3d1	akrobatický
kouskům	kousek	k1gInPc3	kousek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
,	,	kIx,	,
že	že	k8xS	že
začala	začít	k5eAaPmAgFnS	začít
brát	brát	k5eAaImF	brát
lekce	lekce	k1gFnSc1	lekce
od	od	k7c2	od
cirkusových	cirkusový	k2eAgFnPc2d1	cirkusová
krasojezdkyň	krasojezdkyně	k1gFnPc2	krasojezdkyně
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
nechávala	nechávat	k5eAaImAgFnS	nechávat
stavět	stavět	k5eAaImF	stavět
svou	svůj	k3xOyFgFnSc4	svůj
malou	malý	k2eAgFnSc4d1	malá
manéž	manéž	k1gFnSc4	manéž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
dál	daleko	k6eAd2	daleko
trénovala	trénovat	k5eAaImAgFnS	trénovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
době	doba	k1gFnSc6	doba
a	a	k8xC	a
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
společenském	společenský	k2eAgNnSc6d1	společenské
postavení	postavení	k1gNnSc6	postavení
si	se	k3xPyFc3	se
dovolila	dovolit	k5eAaPmAgFnS	dovolit
jezdit	jezdit	k5eAaImF	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
"	"	kIx"	"
<g/>
mužským	mužský	k2eAgInSc7d1	mužský
<g/>
"	"	kIx"	"
způsobem	způsob	k1gInSc7	způsob
bez	bez	k7c2	bez
dámského	dámský	k2eAgNnSc2d1	dámské
sedla	sedlo	k1gNnSc2	sedlo
rozkročmo	rozkročmo	k6eAd1	rozkročmo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prý	prý	k9	prý
tehdy	tehdy	k6eAd1	tehdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vůbec	vůbec	k9	vůbec
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
světovou	světový	k2eAgFnSc4d1	světová
jezdkyni	jezdkyně	k1gFnSc4	jezdkyně
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kvůli	kvůli	k7c3	kvůli
revmatickým	revmatický	k2eAgFnPc3d1	revmatická
potížím	potíž	k1gFnPc3	potíž
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
jízd	jízda	k1gFnPc2	jízda
upustit	upustit	k5eAaPmF	upustit
<g/>
,	,	kIx,	,
našla	najít	k5eAaPmAgFnS	najít
si	se	k3xPyFc3	se
náhradu	náhrada	k1gFnSc4	náhrada
v	v	k7c6	v
šermování	šermování	k1gNnSc6	šermování
<g/>
.	.	kIx.	.
</s>
<s>
Rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
učila	učit	k5eAaImAgFnS	učit
<g/>
:	:	kIx,	:
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnPc1	její
učitelé	učitel	k1gMnPc1	učitel
mělo	mít	k5eAaImAgNnS	mít
co	co	k3yInSc4	co
dělat	dělat	k5eAaImF	dělat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ze	z	k7c2	z
zápasu	zápas	k1gInSc2	zápas
nevyšli	vyjít	k5eNaPmAgMnP	vyjít
často	často	k6eAd1	často
jako	jako	k8xC	jako
poražení	poražený	k1gMnPc1	poražený
<g/>
..	..	k?	..
<g/>
"	"	kIx"	"
Byla	být	k5eAaImAgFnS	být
i	i	k9	i
vynikající	vynikající	k2eAgFnSc1d1	vynikající
plavkyně	plavkyně	k1gFnSc1	plavkyně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
době	doba	k1gFnSc6	doba
také	také	k9	také
nebylo	být	k5eNaImAgNnS	být
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Věnovala	věnovat	k5eAaImAgFnS	věnovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
jiným	jiný	k2eAgInPc3d1	jiný
sportům	sport	k1gInPc3	sport
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
pro	pro	k7c4	pro
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
postavení	postavení	k1gNnSc6	postavení
byly	být	k5eAaImAgFnP	být
něco	něco	k3yInSc4	něco
neobvyklého	obvyklý	k2eNgNnSc2d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
obývala	obývat	k5eAaImAgFnS	obývat
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
tělocvična	tělocvična	k1gFnSc1	tělocvična
se	s	k7c7	s
základním	základní	k2eAgNnSc7d1	základní
gymnastickým	gymnastický	k2eAgNnSc7d1	gymnastické
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
postavě	postava	k1gFnSc3	postava
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejkrásnější	krásný	k2eAgFnSc4d3	nejkrásnější
panovnici	panovnice	k1gFnSc4	panovnice
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
se	se	k3xPyFc4	se
styděla	stydět	k5eAaImAgFnS	stydět
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
zuby	zub	k1gInPc4	zub
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
neusmívala	usmívat	k5eNaImAgFnS	usmívat
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
zakrývala	zakrývat	k5eAaImAgFnS	zakrývat
úsměv	úsměv	k1gInSc4	úsměv
vějířem	vějíř	k1gInSc7	vějíř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nosila	nosit	k5eAaImAgFnS	nosit
vždy	vždy	k6eAd1	vždy
při	při	k7c6	při
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
nepila	pít	k5eNaImAgFnS	pít
kávu	káva	k1gFnSc4	káva
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
čaj	čaj	k1gInSc4	čaj
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
tekutiny	tekutina	k1gFnPc4	tekutina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ztmavují	ztmavovat	k5eAaPmIp3nP	ztmavovat
zubní	zubní	k2eAgFnSc4d1	zubní
sklovinu	sklovina	k1gFnSc4	sklovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
45	[number]	k4	45
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
nenechala	nechat	k5eNaPmAgFnS	nechat
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
portrétovat	portrétovat	k5eAaImF	portrétovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chtěla	chtít	k5eAaImAgFnS	chtít
zůstat	zůstat	k5eAaPmF	zůstat
věčně	věčně	k6eAd1	věčně
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
ve	v	k7c6	v
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
generace	generace	k1gFnPc4	generace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
její	její	k3xOp3gFnSc7	její
vášní	vášeň	k1gFnSc7	vášeň
bylo	být	k5eAaImAgNnS	být
už	už	k6eAd1	už
od	od	k7c2	od
útlého	útlý	k2eAgNnSc2d1	útlé
dětství	dětství	k1gNnSc2	dětství
skládání	skládání	k1gNnSc1	skládání
básní	básnit	k5eAaImIp3nS	básnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
desátého	desátý	k4xOgInSc2	desátý
roku	rok	k1gInSc2	rok
psávala	psávat	k5eAaImAgFnS	psávat
básničky	básnička	k1gFnPc4	básnička
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
si	se	k3xPyFc3	se
romantický	romantický	k2eAgInSc1d1	romantický
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
milovala	milovat	k5eAaImAgFnS	milovat
jak	jak	k8xS	jak
aktivně	aktivně	k6eAd1	aktivně
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
jako	jako	k8xS	jako
posluchačka	posluchačka	k1gFnSc1	posluchačka
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	oboj	k1gFnSc7	oboj
zřejmě	zřejmě	k6eAd1	zřejmě
zdědila	zdědit	k5eAaPmAgFnS	zdědit
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Henricha	Henrich	k1gMnSc4	Henrich
Heineho	Heine	k1gMnSc4	Heine
psal	psát	k5eAaImAgInS	psát
a	a	k8xC	a
uveřejňoval	uveřejňovat	k5eAaImAgInS	uveřejňovat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Phantasus	Phantasus	k1gMnSc1	Phantasus
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
svými	svůj	k3xOyFgFnPc7	svůj
sympatiemi	sympatie	k1gFnPc7	sympatie
překročil	překročit	k5eAaPmAgInS	překročit
společenské	společenský	k2eAgFnPc4d1	společenská
normy	norma	k1gFnPc4	norma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Heine	Hein	k1gInSc5	Hein
měl	mít	k5eAaImAgInS	mít
pověst	pověst	k1gFnSc4	pověst
volnomyšlenkáře	volnomyšlenkář	k1gMnSc2	volnomyšlenkář
<g/>
,	,	kIx,	,
rouhače	rouhač	k1gMnSc2	rouhač
a	a	k8xC	a
kritika	kritik	k1gMnSc2	kritik
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1835	[number]	k4	1835
bylo	být	k5eAaImAgNnS	být
dekretem	dekret	k1gInSc7	dekret
německého	německý	k2eAgInSc2d1	německý
parlamentu	parlament	k1gInSc2	parlament
šíření	šíření	k1gNnSc2	šíření
Heinových	Heinův	k2eAgNnPc2d1	Heinův
děl	dělo	k1gNnPc2	dělo
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc2	jeho
spolubojovníků	spolubojovník	k1gMnPc2	spolubojovník
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Vévoda	vévoda	k1gMnSc1	vévoda
Max	Max	k1gMnSc1	Max
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
miloval	milovat	k5eAaImAgMnS	milovat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
několik	několik	k4yIc4	několik
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Všude	všude	k6eAd1	všude
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vozíval	vozívat	k5eAaImAgMnS	vozívat
citeru	citera	k1gFnSc4	citera
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
hospodách	hospodách	k?	hospodách
dával	dávat	k5eAaImAgInS	dávat
k	k	k7c3	k
lepšímu	dobrý	k2eAgNnSc3d2	lepší
své	svůj	k3xOyFgInPc4	svůj
zhudebněné	zhudebněný	k2eAgInPc4d1	zhudebněný
verše	verš	k1gInPc4	verš
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Alžběta	Alžběta	k1gFnSc1	Alžběta
s	s	k7c7	s
vlajícími	vlající	k2eAgInPc7d1	vlající
copy	cop	k1gInPc7	cop
a	a	k8xC	a
zrudlými	zrudlý	k2eAgFnPc7d1	zrudlá
tvářemi	tvář	k1gFnPc7	tvář
tančila	tančit	k5eAaImAgFnS	tančit
a	a	k8xC	a
do	do	k7c2	do
zástěrky	zástěrka	k1gFnSc2	zástěrka
chytala	chytat	k5eAaImAgFnS	chytat
mince	mince	k1gFnSc1	mince
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
jí	on	k3xPp3gFnSc3	on
házeli	házet	k5eAaImAgMnP	házet
venkovští	venkovský	k2eAgMnPc1d1	venkovský
chasníci	chasník	k1gMnPc1	chasník
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
jedné	jeden	k4xCgFnSc6	jeden
udivené	udivený	k2eAgFnSc6d1	udivená
dámě	dáma	k1gFnSc6	dáma
několik	několik	k4yIc4	několik
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
si	se	k3xPyFc3	se
uchovala	uchovat	k5eAaPmAgFnS	uchovat
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
žertovným	žertovný	k2eAgInSc7d1	žertovný
a	a	k8xC	a
smutným	smutný	k2eAgInSc7d1	smutný
tónem	tón	k1gInSc7	tón
vysvětlovala	vysvětlovat	k5eAaImAgFnS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc1	její
jediné	jediný	k2eAgInPc1d1	jediný
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
si	se	k3xPyFc3	se
kdy	kdy	k6eAd1	kdy
poctivě	poctivě	k6eAd1	poctivě
vydělala	vydělat	k5eAaPmAgFnS	vydělat
<g/>
.	.	kIx.	.
</s>
<s>
Vevoda	Vevoda	k1gMnSc1	Vevoda
Max	Max	k1gMnSc1	Max
dal	dát	k5eAaPmAgMnS	dát
Alžbětu	Alžběta	k1gFnSc4	Alžběta
učit	učit	k5eAaImF	učit
také	také	k9	také
na	na	k7c4	na
citeru	citera	k1gFnSc4	citera
u	u	k7c2	u
přítele	přítel	k1gMnSc2	přítel
a	a	k8xC	a
učitele	učitel	k1gMnSc2	učitel
Johanna	Johann	k1gMnSc4	Johann
Petzmayera	Petzmayer	k1gMnSc4	Petzmayer
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
s	s	k7c7	s
Franzem	Franz	k1gMnSc7	Franz
Kropfem	Kropf	k1gMnSc7	Kropf
a	a	k8xC	a
nástroj	nástroj	k1gInSc4	nástroj
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ovládala	ovládat	k5eAaImAgFnS	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
módním	módní	k2eAgInSc7d1	módní
hudebním	hudební	k2eAgInSc7d1	hudební
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hrála	hrát	k5eAaImAgFnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
zdokonalovala	zdokonalovat	k5eAaImAgFnS	zdokonalovat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
Franze	Franze	k1gFnSc2	Franze
Liszta	Liszt	k1gInSc2	Liszt
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
hrála	hrát	k5eAaImAgFnS	hrát
čtyřručně	čtyřručně	k6eAd1	čtyřručně
<g/>
.	.	kIx.	.
</s>
<s>
Podporovala	podporovat	k5eAaImAgFnS	podporovat
řadu	řad	k1gInSc2	řad
mladých	mladý	k2eAgMnPc2d1	mladý
talentovaných	talentovaný	k2eAgMnPc2d1	talentovaný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Raimanna	Raimann	k1gMnSc2	Raimann
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
slyšela	slyšet	k5eAaImAgFnS	slyšet
jako	jako	k8xS	jako
sedmiletého	sedmiletý	k2eAgNnSc2d1	sedmileté
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mu	on	k3xPp3gNnSc3	on
platila	platit	k5eAaImAgFnS	platit
stipendium	stipendium	k1gNnSc4	stipendium
a	a	k8xC	a
věnovala	věnovat	k5eAaImAgFnS	věnovat
křídlo	křídlo	k1gNnSc4	křídlo
značky	značka	k1gFnSc2	značka
Bösendorfer	Bösendorfra	k1gFnPc2	Bösendorfra
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
dostala	dostat	k5eAaPmAgFnS	dostat
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
ke	k	k7c3	k
svým	svůj	k3xOyFgInSc7	svůj
30	[number]	k4	30
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
od	od	k7c2	od
dvorního	dvorní	k2eAgMnSc2d1	dvorní
výrobce	výrobce	k1gMnSc2	výrobce
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Bösendorfera	Bösendorfer	k1gMnSc2	Bösendorfer
nádherný	nádherný	k2eAgInSc4d1	nádherný
klavír	klavír	k1gInSc4	klavír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
milovala	milovat	k5eAaImAgFnS	milovat
cikánskou	cikánský	k2eAgFnSc4d1	cikánská
muziku	muzika	k1gFnSc4	muzika
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
jim	on	k3xPp3gInPc3	on
dovolila	dovolit	k5eAaPmAgFnS	dovolit
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
žít	žít	k5eAaImF	žít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
zámku	zámek	k1gInSc2	zámek
Gödöllö	Gödöllö	k1gFnSc2	Gödöllö
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
ráda	rád	k2eAgFnSc1d1	ráda
naslouchala	naslouchat	k5eAaImAgFnS	naslouchat
hlasu	hlas	k1gInSc6	hlas
své	svůj	k3xOyFgFnSc3	svůj
neteři	neteř	k1gFnSc3	neteř
Marii	Maria	k1gFnSc3	Maria
Larischové-Wallersee	Larischové-Wallersee	k1gFnSc3	Larischové-Wallersee
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
sama	sám	k3xTgMnSc4	sám
doprovázela	doprovázet	k5eAaImAgFnS	doprovázet
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
nebo	nebo	k8xC	nebo
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oblíbeným	oblíbený	k2eAgFnPc3d1	oblíbená
písním	píseň	k1gFnPc3	píseň
patřily	patřit	k5eAaImAgInP	patřit
zhudebněné	zhudebněný	k2eAgInPc1d1	zhudebněný
verše	verš	k1gInPc1	verš
Heinricha	Heinrich	k1gMnSc2	Heinrich
Heineho	Heine	k1gMnSc2	Heine
<g/>
,	,	kIx,	,
milovaného	milovaný	k1gMnSc2	milovaný
básníka	básník	k1gMnSc2	básník
Alžběty	Alžběta	k1gFnSc2	Alžběta
i	i	k8xC	i
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lotosový	lotosový	k2eAgInSc1d1	lotosový
květ	květ	k1gInSc1	květ
<g/>
,	,	kIx,	,
Asra	Asra	k1gFnSc1	Asra
a	a	k8xC	a
Lesní	lesní	k2eAgInSc1d1	lesní
rozhovor	rozhovor	k1gInSc1	rozhovor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
běl	běl	k1gFnSc4	běl
být	být	k5eAaImF	být
Heinemu	Heinema	k1gFnSc4	Heinema
postaven	postaven	k2eAgInSc1d1	postaven
pomník	pomník	k1gInSc1	pomník
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dán	Dán	k1gMnSc1	Dán
Louis	Louis	k1gMnSc1	Louis
Hasselriis	Hasselriis	k1gFnSc4	Hasselriis
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pro	pro	k7c4	pro
Alžbětu	Alžběta	k1gFnSc4	Alžběta
básníkův	básníkův	k2eAgInSc4d1	básníkův
pomníček	pomníček	k1gInSc4	pomníček
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
v	v	k7c6	v
parku	park	k1gInSc6	park
Achilleonu	Achilleon	k1gInSc2	Achilleon
na	na	k7c4	na
Korfu	Korfu	k1gNnSc4	Korfu
<g/>
,	,	kIx,	,
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc1	jméno
objednavatelky	objednavatelka	k1gFnSc2	objednavatelka
"	"	kIx"	"
<g/>
Frau	Fraus	k1gInSc2	Fraus
Heine	Hein	k1gMnSc5	Hein
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
za	za	k7c7	za
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
skrývá	skrývat	k5eAaImIp3nS	skrývat
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Vilém	Vilém	k1gMnSc1	Vilém
po	po	k7c6	po
císařovnině	císařovnin	k2eAgFnSc6d1	Císařovnina
smrti	smrt	k1gFnSc6	smrt
získal	získat	k5eAaPmAgMnS	získat
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
dcery	dcera	k1gFnSc2	dcera
Gisely	Gisela	k1gFnSc2	Gisela
Bavorské	bavorský	k2eAgNnSc4d1	bavorské
Achilleon	Achilleon	k1gNnSc4	Achilleon
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
sochu	socha	k1gFnSc4	socha
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ho	on	k3xPp3gMnSc4	on
neměl	mít	k5eNaImAgMnS	mít
rád	rád	k6eAd1	rád
vůči	vůči	k7c3	vůči
jeho	jeho	k3xOp3gNnSc3	jeho
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
k	k	k7c3	k
Prusku	Prusko	k1gNnSc3	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
dal	dát	k5eAaPmAgMnS	dát
zhotovit	zhotovit	k5eAaPmF	zhotovit
pomník	pomník	k1gInSc4	pomník
císařovně	císařovna	k1gFnSc3	císařovna
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Heineho	Heine	k1gMnSc4	Heine
sestru	sestra	k1gFnSc4	sestra
Charlottu	Charlotta	k1gFnSc4	Charlotta
von	von	k1gInSc1	von
Emden-Heine	Emden-Hein	k1gInSc5	Emden-Hein
v	v	k7c6	v
Hamburku	Hamburk	k1gInSc2	Hamburk
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
99	[number]	k4	99
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
přijala	přijmout	k5eAaPmAgFnS	přijmout
jeho	jeho	k3xOp3gMnPc4	jeho
synovce	synovec	k1gMnPc4	synovec
<g/>
.	.	kIx.	.
</s>
<s>
Filmaři	filmař	k1gMnPc1	filmař
<g/>
,	,	kIx,	,
malíři	malíř	k1gMnPc1	malíř
i	i	k8xC	i
spisovatelé	spisovatel	k1gMnPc1	spisovatel
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gFnSc7	její
osobou	osoba	k1gFnSc7	osoba
dodnes	dodnes	k6eAd1	dodnes
fascinováni	fascinovat	k5eAaBmNgMnP	fascinovat
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
jsou	být	k5eAaImIp3nP	být
napodobovány	napodobován	k2eAgInPc1d1	napodobován
její	její	k3xOp3gInPc1	její
svatební	svatební	k2eAgInPc1d1	svatební
šaty	šat	k1gInPc1	šat
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
účes	účes	k1gInSc4	účes
a	a	k8xC	a
obdivují	obdivovat	k5eAaImIp3nP	obdivovat
ji	on	k3xPp3gFnSc4	on
ženy	žena	k1gFnPc1	žena
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
s	s	k7c7	s
jejím	její	k3xOp3gInSc7	její
portrétem	portrét	k1gInSc7	portrét
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
pohledem	pohled	k1gInSc7	pohled
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hrdinkou	hrdinka	k1gFnSc7	hrdinka
četných	četný	k2eAgInPc2d1	četný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
operet	opereta	k1gFnPc2	opereta
<g/>
,	,	kIx,	,
muzikálů	muzikál	k1gInPc2	muzikál
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
kreslených	kreslený	k2eAgFnPc2d1	kreslená
pohádek	pohádka	k1gFnPc2	pohádka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Romány	román	k1gInPc1	román
a	a	k8xC	a
filmy	film	k1gInPc1	film
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
,	,	kIx,	,
Sissi	Sisse	k1gFnSc4	Sisse
-	-	kIx~	-
mladá	mladý	k2eAgFnSc1d1	mladá
císařovna	císařovna	k1gFnSc1	císařovna
<g/>
,	,	kIx,	,
Sissi	Sisse	k1gFnSc6	Sisse
<g/>
,	,	kIx,	,
osudová	osudový	k2eAgNnPc4d1	osudové
léta	léto	k1gNnPc4	léto
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc7	on
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
herečka	herečka	k1gFnSc1	herečka
Romy	Rom	k1gMnPc4	Rom
Schneiderová	Schneiderová	k1gFnSc1	Schneiderová
jsou	být	k5eAaImIp3nP	být
nejen	nejen	k6eAd1	nejen
povrchní	povrchní	k2eAgFnPc1d1	povrchní
a	a	k8xC	a
naivní	naivní	k2eAgFnPc1d1	naivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
velmi	velmi	k6eAd1	velmi
nepravdivé	pravdivý	k2eNgNnSc1d1	nepravdivé
<g/>
.	.	kIx.	.
</s>
<s>
Realita	realita	k1gFnSc1	realita
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
byla	být	k5eAaImAgNnP	být
mnohem	mnohem	k6eAd1	mnohem
drsnější	drsný	k2eAgNnPc1d2	drsnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jako	jako	k9	jako
její	její	k3xOp3gFnSc1	její
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
jejím	její	k3xOp3gInSc7	její
životopisem	životopis	k1gInSc7	životopis
<g/>
,	,	kIx,	,
pochopí	pochopit	k5eAaPmIp3nP	pochopit
větu	věta	k1gFnSc4	věta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
vyřkla	vyřknout	k5eAaPmAgFnS	vyřknout
skoro	skoro	k6eAd1	skoro
v	v	k7c6	v
šedesáti	šedesát	k4xCc6	šedesát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Manželství	manželství	k1gNnSc1	manželství
je	být	k5eAaImIp3nS	být
nesmyslná	smyslný	k2eNgFnSc1d1	nesmyslná
instituce	instituce	k1gFnSc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
prodán	prodán	k2eAgMnSc1d1	prodán
a	a	k8xC	a
vysloví	vyslovit	k5eAaPmIp3nS	vyslovit
přísahu	přísaha	k1gFnSc4	přísaha
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nerozumí	rozumět	k5eNaImIp3nS	rozumět
a	a	k8xC	a
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
zbavit	zbavit	k5eAaPmF	zbavit
30	[number]	k4	30
a	a	k8xC	a
více	hodně	k6eAd2	hodně
let	léto	k1gNnPc2	léto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
založil	založit	k5eAaPmAgMnS	založit
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
počest	počest	k1gFnSc4	počest
Řád	řád	k1gInSc1	řád
Alžběty	Alžběta	k1gFnPc1	Alžběta
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Gabriela	Gabriela	k1gFnSc1	Gabriela
Praschlová-Bichlerová	Praschlová-Bichlerová	k1gFnSc1	Praschlová-Bichlerová
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnSc1	náš
milá	milá	k1gFnSc1	milá
Sisi	Sisi	k1gNnSc2	Sisi
<g/>
,	,	kIx,	,
Pravda	pravda	k9	pravda
o	o	k7c6	o
arcivévodkyni	arcivévodkyně	k1gFnSc6	arcivévodkyně
Žofii	Žofie	k1gFnSc6	Žofie
a	a	k8xC	a
císařovně	císařovna	k1gFnSc6	císařovna
Alžbětě	Alžběta	k1gFnSc6	Alžběta
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dosud	dosud	k6eAd1	dosud
nezveřejněných	zveřejněný	k2eNgInPc2d1	nezveřejněný
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
nakl	nakla	k1gFnPc2	nakla
<g/>
.	.	kIx.	.
</s>
<s>
Ikar	Ikar	k1gMnSc1	Ikar
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
224	[number]	k4	224
stran	strana	k1gFnPc2	strana
+	+	kIx~	+
16	[number]	k4	16
stran	strana	k1gFnPc2	strana
přílohy	příloha	k1gFnSc2	příloha
G.	G.	kA	G.
Praschlová-Bichlerová	Praschlová-Bichlerová	k1gFnSc1	Praschlová-Bichlerová
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Cachée	Cachée	k1gFnSc4	Cachée
<g/>
,	,	kIx,	,
Korunu	koruna	k1gFnSc4	koruna
snímám	snímat	k5eAaImIp1nS	snímat
z	z	k7c2	z
unavené	unavený	k2eAgFnSc2d1	unavená
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
císařovny	císařovna	k1gFnSc2	císařovna
"	"	kIx"	"
<g/>
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ikar	Ikar	k1gInSc1	Ikar
<g/>
,	,	kIx,	,
Czech	Czech	k1gInSc1	Czech
edition	edition	k1gInSc1	edition
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc1	vydání
první	první	k4xOgNnPc1	první
<g/>
,	,	kIx,	,
168	[number]	k4	168
<g/>
.	.	kIx.	.
<g/>
publikace	publikace	k1gFnSc2	publikace
<g/>
,	,	kIx,	,
192	[number]	k4	192
stran	strana	k1gFnPc2	strana
Soňa	Soňa	k1gFnSc1	Soňa
Sirotková	Sirotková	k1gFnSc1	Sirotková
<g/>
,	,	kIx,	,
Říkali	říkat	k5eAaImAgMnP	říkat
mi	já	k3xPp1nSc3	já
Sisi	Sisi	k1gNnSc1	Sisi
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Petrklíč	petrklíč	k1gInSc1	petrklíč
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7229-197-7	[number]	k4	978-80-7229-197-7
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
GRÖSSING	GRÖSSING	kA	GRÖSSING
Sigrid-Maria	Sigrid-Marium	k1gNnPc4	Sigrid-Marium
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
nevěsty	nevěsta	k1gFnPc1	nevěsta
pro	pro	k7c4	pro
císaře	císař	k1gMnSc4	císař
:	:	kIx,	:
Sissi	Sisse	k1gFnSc4	Sisse
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Nené	Nené	k1gNnSc2	Nené
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Ikar	Ikara	k1gFnPc2	Ikara
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-249-0109-9	[number]	k4	80-249-0109-9
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
A.	A.	kA	A.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
155	[number]	k4	155
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
215	[number]	k4	215
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
69	[number]	k4	69
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
CARTLAND	CARTLAND	kA	CARTLAND
<g/>
,	,	kIx,	,
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
.	.	kIx.	.
</s>
<s>
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
:	:	kIx,	:
soukromý	soukromý	k2eAgInSc1d1	soukromý
život	život	k1gInSc1	život
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
,	,	kIx,	,
císařovny	císařovna	k1gFnSc2	císařovna
rakouské	rakouský	k2eAgFnSc2d1	rakouská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901243	[number]	k4	901243
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
BESTENREINER	BESTENREINER	kA	BESTENREINER
Erika	Erika	k1gFnSc1	Erika
<g/>
,	,	kIx,	,
Sisi	Sis	k1gMnPc1	Sis
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Brána	brána	k1gFnSc1	brána
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7243-232-X	[number]	k4	80-7243-232-X
HAMANNOVÁ	HAMANNOVÁ	kA	HAMANNOVÁ
<g/>
,	,	kIx,	,
Brigitte	Brigitte	k1gFnSc1	Brigitte
<g/>
.	.	kIx.	.
</s>
<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Císařovna	císařovna	k1gFnSc1	císařovna
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
533	[number]	k4	533
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
546	[number]	k4	546
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
PRASCHL-Bichler	PRASCHL-Bichler	k1gInSc1	PRASCHL-Bichler
Gabriele	Gabriela	k1gFnSc3	Gabriela
<g/>
,	,	kIx,	,
Císařovna	císařovna	k1gFnSc1	císařovna
Alžběta	Alžběta	k1gFnSc1	Alžběta
:	:	kIx,	:
mýtus	mýtus	k1gInSc1	mýtus
a	a	k8xC	a
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Ikar	Ikara	k1gFnPc2	Ikara
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7202-241-5	[number]	k4	80-7202-241-5
PRASCHL-Bichler	PRASCHL-Bichler	k1gInSc1	PRASCHL-Bichler
Gabriele	Gabriela	k1gFnSc3	Gabriela
<g/>
,	,	kIx,	,
Kondiční	kondiční	k2eAgFnSc3d1	kondiční
a	a	k8xC	a
dietní	dietní	k2eAgInSc1d1	dietní
program	program	k1gInSc1	program
císařovny	císařovna	k1gFnSc2	císařovna
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7312-030-5	[number]	k4	80-7312-030-5
RALL	RALL	kA	RALL
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
<g/>
;	;	kIx,	;
RALL	RALL	kA	RALL
<g/>
,	,	kIx,	,
Marga	Marga	k1gFnSc1	Marga
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Wittelsbacher	Wittelsbachra	k1gFnPc2	Wittelsbachra
in	in	k?	in
Lebensbildern	Lebensbilderna	k1gFnPc2	Lebensbilderna
<g/>
.	.	kIx.	.
</s>
<s>
Graz	Graz	k1gMnSc1	Graz
;	;	kIx,	;
Wien	Wien	k1gMnSc1	Wien
;	;	kIx,	;
Köln	Köln	k1gMnSc1	Köln
;	;	kIx,	;
Regensburg	Regensburg	k1gInSc1	Regensburg
:	:	kIx,	:
Styria	Styrium	k1gNnSc2	Styrium
;	;	kIx,	;
Pustet	Pustet	k1gMnSc1	Pustet
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
431	[number]	k4	431
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
222	[number]	k4	222
<g/>
-	-	kIx~	-
<g/>
11669	[number]	k4	11669
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Galerie	galerie	k1gFnSc1	galerie
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
