<s>
Alžběta	Alžběta	k1gFnSc1	Alžběta
Amálie	Amálie	k1gFnSc1	Amálie
Evženie	Evženie	k1gFnSc1	Evženie
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1837	[number]	k4	1837
<g/>
,	,	kIx,	,
Mnichov	Mnichov	k1gInSc1	Mnichov
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Ženeva	Ženeva	k1gFnSc1	Ženeva
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
Amalie	Amalie	k1gFnSc2	Amalie
Eugenie	Eugenie	k1gFnSc2	Eugenie
<g/>
,	,	kIx,	,
Herzogin	Herzogin	k2eAgInSc1d1	Herzogin
in	in	k?	in
Bayern	Bayern	k1gInSc1	Bayern
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Sissi	Sisse	k1gFnSc4	Sisse
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
bavorská	bavorský	k2eAgFnSc1d1	bavorská
princezna	princezna	k1gFnSc1	princezna
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Wittelsbachů	Wittelsbach	k1gInPc2	Wittelsbach
a	a	k8xC	a
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Josefem	Josef	k1gMnSc7	Josef
rakouská	rakouský	k2eAgFnSc1d1	rakouská
císařovna	císařovna	k1gFnSc1	císařovna
a	a	k8xC	a
korunovaná	korunovaný	k2eAgFnSc1d1	korunovaná
uherská	uherský	k2eAgFnSc1d1	uherská
královna	královna	k1gFnSc1	královna
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Erzsébet	Erzsébeta	k1gFnPc2	Erzsébeta
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Wittelsbach	Wittelsbach	k1gMnSc1	Wittelsbach
Erzsébet	Erzsébeta	k1gFnPc2	Erzsébeta
magyar	magyar	k1gMnSc1	magyar
királyné	királyná	k1gFnSc2	királyná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
