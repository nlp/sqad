<p>
<s>
Klokanovití	Klokanovitý	k2eAgMnPc1d1	Klokanovitý
(	(	kIx(	(
<g/>
Macropodidae	Macropodidae	k1gNnSc7	Macropodidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
čeleď	čeleď	k1gFnSc1	čeleď
vačnatců	vačnatec	k1gMnPc2	vačnatec
(	(	kIx(	(
<g/>
po	po	k7c6	po
vačicovitých	vačicovitý	k2eAgFnPc6d1	vačicovitý
<g/>
)	)	kIx)	)
s	s	k7c7	s
54	[number]	k4	54
druhy	druh	k1gInPc7	druh
v	v	k7c6	v
11	[number]	k4	11
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Klokani	klokan	k1gMnPc1	klokan
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
v	v	k7c6	v
období	období	k1gNnSc6	období
pozdního	pozdní	k2eAgInSc2d1	pozdní
miocénu	miocén	k1gInSc2	miocén
až	až	k8xS	až
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
5	[number]	k4	5
až	až	k9	až
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
však	však	k9	však
klokánkovité	klokánkovitý	k2eAgNnSc1d1	klokánkovitý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
klokanovitým	klokanovitý	k2eAgInPc3d1	klokanovitý
podobají	podobat	k5eAaImIp3nP	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
středně	středně	k6eAd1	středně
velké	velký	k2eAgMnPc4d1	velký
až	až	k8xS	až
velké	velký	k2eAgMnPc4d1	velký
vačnatce	vačnatec	k1gMnPc4	vačnatec
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
největší	veliký	k2eAgInPc1d3	veliký
druhy	druh	k1gInPc1	druh
žijících	žijící	k2eAgMnPc2d1	žijící
vačnatců	vačnatec	k1gMnPc2	vačnatec
<g/>
)	)	kIx)	)
s	s	k7c7	s
prodlouženými	prodloužený	k2eAgFnPc7d1	prodloužená
zadními	zadní	k2eAgFnPc7d1	zadní
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
u	u	k7c2	u
pozemních	pozemní	k2eAgInPc2d1	pozemní
druhů	druh	k1gInPc2	druh
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
skákavému	skákavý	k2eAgInSc3d1	skákavý
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stromových	stromový	k2eAgInPc2d1	stromový
druhů	druh	k1gInPc2	druh
není	být	k5eNaImIp3nS	být
prodloužení	prodloužení	k1gNnSc4	prodloužení
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
tak	tak	k6eAd1	tak
výrazné	výrazný	k2eAgFnPc4d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
býložravých	býložravý	k2eAgMnPc2d1	býložravý
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc4d1	české
slovo	slovo	k1gNnSc4	slovo
klokan	klokan	k1gMnSc1	klokan
nenajdeme	najít	k5eNaPmIp1nP	najít
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
jazyce	jazyk	k1gInSc6	jazyk
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
chorvatštiny	chorvatština	k1gFnSc2	chorvatština
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obrozenecký	obrozenecký	k2eAgInSc4d1	obrozenecký
výtvor	výtvor	k1gInSc4	výtvor
J.	J.	kA	J.
S.	S.	kA	S.
Presla	Presla	k1gFnSc1	Presla
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
etymologie	etymologie	k1gFnSc1	etymologie
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
inspirací	inspirace	k1gFnSc7	inspirace
snad	snad	k9	snad
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
slova	slovo	k1gNnSc2	slovo
kloaka	kloaka	k1gFnSc1	kloaka
a	a	k8xC	a
skokan	skokan	k1gMnSc1	skokan
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
používané	používaný	k2eAgInPc1d1	používaný
ve	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kangaroo	kangaroo	k1gNnSc4	kangaroo
<g/>
,	,	kIx,	,
känguru	kängura	k1gFnSc4	kängura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
slovenské	slovenský	k2eAgFnPc1d1	slovenská
kengura	kengura	k1gFnSc1	kengura
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
domorodý	domorodý	k2eAgInSc4d1	domorodý
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
guugu	guug	k1gInSc2	guug
jimidhirr	jimidhirra	k1gFnPc2	jimidhirra
z	z	k7c2	z
Yorského	yorský	k2eAgInSc2d1	yorský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
jsou	být	k5eAaImIp3nP	být
endemity	endemit	k1gInPc7	endemit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc6d1	Nové
Guineji	Guinea	k1gFnSc6	Guinea
a	a	k8xC	a
na	na	k7c6	na
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
biomy	biom	k1gInPc1	biom
<g/>
,	,	kIx,	,
od	od	k7c2	od
pouště	poušť	k1gFnSc2	poušť
po	po	k7c4	po
deštný	deštný	k2eAgInSc4d1	deštný
prales	prales	k1gInSc4	prales
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klokan	klokan	k1gMnSc1	klokan
rudokrký	rudokrký	k2eAgMnSc1d1	rudokrký
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vysazený	vysazený	k2eAgInSc1d1	vysazený
i	i	k9	i
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
chován	chován	k2eAgMnSc1d1	chován
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
u	u	k7c2	u
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
úzkou	úzký	k2eAgFnSc4d1	úzká
lebku	lebka	k1gFnSc4	lebka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
čenich	čenich	k1gInSc4	čenich
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vypadá	vypadat	k5eAaPmIp3nS	vypadat
drobná	drobný	k2eAgFnSc1d1	drobná
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
zbývajícího	zbývající	k2eAgNnSc2d1	zbývající
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vzrůst	vzrůst	k1gInSc1	vzrůst
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgMnSc1d1	střední
až	až	k8xS	až
velký	velký	k2eAgMnSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Váhy	Váhy	k1gFnPc1	Váhy
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
od	od	k7c2	od
0,5	[number]	k4	0,5
kg	kg	kA	kg
do	do	k7c2	do
90	[number]	k4	90
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Klokanovití	Klokanovitý	k2eAgMnPc1d1	Klokanovitý
jsou	být	k5eAaImIp3nP	být
ploskochodci	ploskochodec	k1gMnPc1	ploskochodec
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
úzkou	úzký	k2eAgFnSc4d1	úzká
zadní	zadní	k2eAgFnSc4d1	zadní
tlapu	tlapa	k1gFnSc4	tlapa
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc1d1	silná
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
tlapě	tlapa	k1gFnSc6	tlapa
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
a	a	k8xC	a
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
,	,	kIx,	,
klokanovití	klokanovitý	k2eAgMnPc1d1	klokanovitý
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
odrážení	odrážení	k1gNnSc3	odrážení
při	při	k7c6	při
skákání	skákání	k1gNnSc6	skákání
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgMnSc1	pátý
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgMnSc1d1	vnější
<g/>
)	)	kIx)	)
prst	prst	k1gInSc1	prst
je	být	k5eAaImIp3nS	být
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
dvojitozubci	dvojitozubce	k1gMnPc1	dvojitozubce
jsou	být	k5eAaImIp3nP	být
syndaktilní	syndaktilní	k2eAgMnPc1d1	syndaktilní
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc1	třetí
prst	prst	k1gInSc1	prst
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
je	být	k5eAaImIp3nS	být
srostlý	srostlý	k2eAgInSc1d1	srostlý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palec	palec	k1gInSc1	palec
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
úplně	úplně	k6eAd1	úplně
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
mohutný	mohutný	k2eAgInSc1d1	mohutný
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
chapavý	chapavý	k2eAgInSc1d1	chapavý
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
balancující	balancující	k2eAgInSc1d1	balancující
či	či	k8xC	či
stabilizační	stabilizační	k2eAgInSc1d1	stabilizační
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pohyb	pohyb	k1gInSc1	pohyb
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
jsou	být	k5eAaImIp3nP	být
endemity	endemit	k1gInPc7	endemit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc6d1	Nové
Guineji	Guinea	k1gFnSc6	Guinea
a	a	k8xC	a
na	na	k7c6	na
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
biomy	biom	k1gInPc1	biom
<g/>
,	,	kIx,	,
od	od	k7c2	od
pouště	poušť	k1gFnSc2	poušť
po	po	k7c4	po
deštný	deštný	k2eAgInSc4d1	deštný
prales	prales	k1gInSc4	prales
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
klokanovitých	klokanovitý	k2eAgMnPc2d1	klokanovitý
nedovede	dovést	k5eNaPmIp3nS	dovést
pohybovat	pohybovat	k5eAaImF	pohybovat
nezávisle	závisle	k6eNd1	závisle
svýma	svůj	k3xOyFgFnPc7	svůj
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
nemohou	moct	k5eNaImIp3nP	moct
normálně	normálně	k6eAd1	normálně
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
poskakovat	poskakovat	k5eAaImF	poskakovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
nemohou	moct	k5eNaImIp3nP	moct
chodit	chodit	k5eAaImF	chodit
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
přináší	přinášet	k5eAaImIp3nS	přinášet
velké	velký	k2eAgInPc4d1	velký
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
úniková	únikový	k2eAgFnSc1d1	úniková
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pomalý	pomalý	k2eAgInSc4d1	pomalý
pohyb	pohyb	k1gInSc4	pohyb
používá	používat	k5eAaImIp3nS	používat
klokan	klokan	k1gMnSc1	klokan
chůzi	chůze	k1gFnSc4	chůze
"	"	kIx"	"
<g/>
po	po	k7c6	po
pěti	pět	k4xCc6	pět
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
ze	z	k7c2	z
sedu	sed	k1gInSc2	sed
se	se	k3xPyFc4	se
nadzvedne	nadzvednout	k5eAaPmIp3nS	nadzvednout
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
nohách	noha	k1gFnPc6	noha
a	a	k8xC	a
ocasu	ocas	k1gInSc6	ocas
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesune	přesunout	k5eAaPmIp3nS	přesunout
zadní	zadní	k2eAgFnPc4d1	zadní
nohy	noha	k1gFnPc4	noha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velcí	velký	k2eAgMnPc1d1	velký
klokani	klokan	k1gMnPc1	klokan
pohybující	pohybující	k2eAgMnSc1d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
křovinaté	křovinatý	k2eAgFnSc6d1	křovinatá
stepi	step	k1gFnSc6	step
skoky	skok	k1gInPc4	skok
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
až	až	k9	až
11	[number]	k4	11
m	m	kA	m
a	a	k8xC	a
rychlostí	rychlost	k1gFnSc7	rychlost
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
velkých	velký	k2eAgNnPc6d1	velké
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
stěhující	stěhující	k2eAgNnPc1d1	stěhující
stáda	stádo	k1gNnPc1	stádo
jsou	být	k5eAaImIp3nP	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
urazit	urazit	k5eAaPmF	urazit
za	za	k7c7	za
pastvou	pastva	k1gFnSc7	pastva
dráhu	dráha	k1gFnSc4	dráha
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Klokan	klokan	k1gMnSc1	klokan
rudý	rudý	k1gMnSc1	rudý
může	moct	k5eAaImIp3nS	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
70	[number]	k4	70
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
jsou	být	k5eAaImIp3nP	být
býložravci	býložravec	k1gMnPc7	býložravec
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
spásá	spásat	k5eAaPmIp3nS	spásat
trávu	tráva	k1gFnSc4	tráva
a	a	k8xC	a
byliny	bylina	k1gFnPc4	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
živit	živit	k5eAaImF	živit
kapradinami	kapradina	k1gFnPc7	kapradina
<g/>
,	,	kIx,	,
výhonky	výhonek	k1gInPc7	výhonek
<g/>
,	,	kIx,	,
listy	list	k1gInPc7	list
a	a	k8xC	a
lesními	lesní	k2eAgInPc7d1	lesní
plody	plod	k1gInPc7	plod
<g/>
.	.	kIx.	.
</s>
<s>
Nevyhýbají	vyhýbat	k5eNaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
jedovatým	jedovatý	k2eAgFnPc3d1	jedovatá
rostlinám	rostlina	k1gFnPc3	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tráva	tráva	k1gFnSc1	tráva
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tuhá	tuhý	k2eAgFnSc1d1	tuhá
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
žvýkání	žvýkání	k1gNnSc1	žvýkání
silně	silně	k6eAd1	silně
opotřebovává	opotřebovávat	k5eAaImIp3nS	opotřebovávat
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
mají	mít	k5eAaImIp3nP	mít
klokani	klokan	k1gMnPc1	klokan
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
čtyři	čtyři	k4xCgInPc4	čtyři
páry	pár	k1gInPc4	pár
stoliček	stolička	k1gFnPc2	stolička
<g/>
,	,	kIx,	,
jen	jen	k9	jen
přední	přední	k2eAgInPc1d1	přední
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
opotřebují	opotřebovat	k5eAaPmIp3nP	opotřebovat
a	a	k8xC	a
vypadnou	vypadnout	k5eAaPmIp3nP	vypadnout
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
dalšími	další	k2eAgInPc7d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
klokan	klokan	k1gMnSc1	klokan
dožije	dožít	k5eAaPmIp3nS	dožít
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
opotřebován	opotřebován	k2eAgInSc1d1	opotřebován
i	i	k8xC	i
poslední	poslední	k2eAgInSc1d1	poslední
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
uhynou	uhynout	k5eAaPmIp3nP	uhynout
hladem	hlad	k1gInSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
klokanovi	klokanův	k2eAgMnPc1d1	klokanův
rezavému	rezavý	k2eAgInSc3d1	rezavý
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
stoličky	stolička	k1gFnPc1	stolička
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
placentálních	placentální	k2eAgMnPc2d1	placentální
býložravců	býložravec	k1gMnPc2	býložravec
<g/>
,	,	kIx,	,
spásají	spásat	k5eAaImIp3nP	spásat
klokanovití	klokanovitý	k2eAgMnPc1d1	klokanovitý
trávu	tráva	k1gFnSc4	tráva
a	a	k8xC	a
byliny	bylina	k1gFnPc4	bylina
velmi	velmi	k6eAd1	velmi
šetrně	šetrně	k6eAd1	šetrně
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
ji	on	k3xPp3gFnSc4	on
nevypasou	vypást	k5eNaPmIp3nP	vypást
až	až	k6eAd1	až
ke	k	k7c3	k
kořenům	kořen	k1gInPc3	kořen
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
jí	on	k3xPp3gFnSc3	on
neznemožňují	znemožňovat	k5eNaImIp3nP	znemožňovat
obnovení	obnovení	k1gNnSc3	obnovení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
klokani	klokan	k1gMnPc1	klokan
dovedou	dovést	k5eAaPmIp3nP	dovést
přežvykovat	přežvykovat	k5eAaImF	přežvykovat
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
svalnatý	svalnatý	k2eAgInSc1d1	svalnatý
žaludek	žaludek	k1gInSc1	žaludek
je	být	k5eAaImIp3nS	být
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
několik	několik	k4yIc4	několik
částí	část	k1gFnPc2	část
a	a	k8xC	a
pojme	pojmout	k5eAaPmIp3nS	pojmout
mnoho	mnoho	k4c4	mnoho
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
trávena	trávit	k5eAaImNgFnS	trávit
pomocí	pomocí	k7c2	pomocí
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývin	vývin	k1gInSc4	vývin
a	a	k8xC	a
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Klokanovití	Klokanovitý	k2eAgMnPc1d1	Klokanovitý
jsou	být	k5eAaImIp3nP	být
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
savců	savec	k1gMnPc2	savec
živorodí	živorodý	k2eAgMnPc1d1	živorodý
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
samicím	samice	k1gFnPc3	samice
placentálních	placentální	k2eAgMnPc2d1	placentální
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
jednu	jeden	k4xCgFnSc4	jeden
dělohu	děloha	k1gFnSc4	děloha
a	a	k8xC	a
pochvu	pochva	k1gFnSc4	pochva
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
samice	samice	k1gFnPc1	samice
klokanů	klokan	k1gMnPc2	klokan
zdvojený	zdvojený	k2eAgInSc4d1	zdvojený
systém	systém	k1gInSc4	systém
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
dělohami	děloha	k1gFnPc7	děloha
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
boční	boční	k2eAgFnSc7d1	boční
pochvou	pochva	k1gFnSc7	pochva
-	-	kIx~	-
vagínou	vagína	k1gFnSc7	vagína
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
prochází	procházet	k5eAaImIp3nS	procházet
odděleným	oddělený	k2eAgNnSc7d1	oddělené
středním	střední	k2eAgNnSc7d1	střední
porodným	porodné	k1gNnSc7	porodné
kanálem	kanál	k1gInSc7	kanál
(	(	kIx(	(
<g/>
centrální	centrální	k2eAgFnSc7d1	centrální
vagínou	vagína	k1gFnSc7	vagína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
trubice	trubice	k1gFnSc1	trubice
se	se	k3xPyFc4	se
u	u	k7c2	u
klokanů	klokan	k1gMnPc2	klokan
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
před	před	k7c7	před
prvním	první	k4xOgInSc7	první
porodem	porod	k1gInSc7	porod
a	a	k8xC	a
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
klokanů	klokan	k1gMnPc2	klokan
mají	mít	k5eAaImIp3nP	mít
penis	penis	k1gInSc4	penis
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rozeklaný	rozeklaný	k2eAgInSc4d1	rozeklaný
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
semene	semeno	k1gNnSc2	semeno
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
pochev	pochva	k1gFnPc2	pochva
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
u	u	k7c2	u
klokana	klokan	k1gMnSc2	klokan
obrovského	obrovský	k2eAgNnSc2d1	obrovské
trvá	trvat	k5eAaImIp3nS	trvat
36	[number]	k4	36
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Novorozené	novorozený	k2eAgFnSc3d1	novorozená
klokáně	klokána	k1gFnSc3	klokána
je	být	k5eAaImIp3nS	být
však	však	k9	však
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
velmi	velmi	k6eAd1	velmi
nevyvinuté	vyvinutý	k2eNgFnSc2d1	nevyvinutá
–	–	k?	–
je	být	k5eAaImIp3nS	být
holé	holý	k2eAgNnSc1d1	holé
<g/>
,	,	kIx,	,
slepé	slepý	k2eAgNnSc1d1	slepé
a	a	k8xC	a
schází	scházet	k5eAaImIp3nS	scházet
mu	on	k3xPp3gNnSc3	on
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Přichytí	přichytit	k5eAaPmIp3nS	přichytit
drápky	drápek	k1gInPc4	drápek
k	k	k7c3	k
matčině	matčin	k2eAgFnSc3d1	matčina
srsti	srst	k1gFnSc3	srst
a	a	k8xC	a
šplhá	šplhat	k5eAaImIp3nS	šplhat
jen	jen	k9	jen
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
svého	svůj	k3xOyFgInSc2	svůj
čichu	čich	k1gInSc2	čich
a	a	k8xC	a
hmatu	hmat	k1gInSc2	hmat
k	k	k7c3	k
jejímu	její	k3xOp3gInSc3	její
vaku	vak	k1gInSc3	vak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přisaje	přisát	k5eAaPmIp3nS	přisát
na	na	k7c4	na
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
bradavku	bradavka	k1gFnSc4	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
mláděti	mládě	k1gNnSc6	mládě
neulizuje	ulizovat	k5eNaImIp3nS	ulizovat
cestičku	cestička	k1gFnSc4	cestička
mezi	mezi	k7c7	mezi
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
čistí	čistit	k5eAaImIp3nS	čistit
srst	srst	k1gFnSc4	srst
od	od	k7c2	od
porodní	porodní	k2eAgFnSc2d1	porodní
tekutiny	tekutina	k1gFnSc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vaku	vak	k1gInSc6	vak
mu	on	k3xPp3gMnSc3	on
ztvrdnou	ztvrdnout	k5eAaPmIp3nP	ztvrdnout
koutky	koutek	k1gInPc4	koutek
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
dokonale	dokonale	k6eAd1	dokonale
drží	držet	k5eAaImIp3nS	držet
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
si	se	k3xPyFc3	se
dříve	dříve	k6eAd2	dříve
australští	australský	k2eAgMnPc1d1	australský
usedlíci	usedlík	k1gMnPc1	usedlík
myslili	myslit	k5eAaImAgMnP	myslit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
bradavky	bradavka	k1gFnSc2	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vaku	vak	k1gInSc6	vak
saje	sát	k5eAaImIp3nS	sát
mléko	mléko	k1gNnSc1	mléko
do	do	k7c2	do
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
dovyvine	dovyvinout	k5eAaPmIp3nS	dovyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
vaku	vak	k1gInSc6	vak
čtyři	čtyři	k4xCgInPc4	čtyři
ústí	ústí	k1gNnSc6	ústí
mléčných	mléčný	k2eAgFnPc2d1	mléčná
žláz	žláza	k1gFnPc2	žláza
<g/>
,	,	kIx,	,
bradavky	bradavka	k1gFnSc2	bradavka
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
mléko	mléko	k1gNnSc4	mléko
s	s	k7c7	s
rozdílným	rozdílný	k2eAgNnSc7d1	rozdílné
složením	složení	k1gNnSc7	složení
<g/>
.	.	kIx.	.
</s>
<s>
Mláděti	mládě	k1gNnSc3	mládě
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
zadní	zadní	k2eAgFnPc4d1	zadní
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
uši	ucho	k1gNnPc1	ucho
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vzpřímí	vzpřímit	k5eAaPmIp3nS	vzpřímit
a	a	k8xC	a
naroste	narůst	k5eAaPmIp3nS	narůst
mu	on	k3xPp3gMnSc3	on
srst	srst	k1gFnSc1	srst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
7	[number]	k4	7
<g/>
.	.	kIx.	.
měsíce	měsíc	k1gInSc2	měsíc
mládě	mládě	k1gNnSc1	mládě
vystrčí	vystrčit	k5eAaPmIp3nS	vystrčit
hlavu	hlava	k1gFnSc4	hlava
z	z	k7c2	z
vaku	vak	k1gInSc2	vak
<g/>
,	,	kIx,	,
ochutná	ochutnat	k5eAaPmIp3nS	ochutnat
trávu	tráva	k1gFnSc4	tráva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
zaleze	zalézt	k5eAaPmIp3nS	zalézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vaku	vak	k1gInSc2	vak
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
leze	lézt	k5eAaImIp3nS	lézt
hlavou	hlava	k1gFnSc7	hlava
napřed	napřed	k6eAd1	napřed
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
ve	v	k7c6	v
vaku	vak	k1gInSc6	vak
musí	muset	k5eAaImIp3nS	muset
udělat	udělat	k5eAaPmF	udělat
kotoul	kotoul	k1gInSc4	kotoul
<g/>
.	.	kIx.	.
</s>
<s>
Vak	vak	k1gInSc1	vak
je	být	k5eAaImIp3nS	být
však	však	k9	však
brzy	brzy	k6eAd1	brzy
obsazen	obsadit	k5eAaPmNgInS	obsadit
dalšími	další	k2eAgInPc7d1	další
přírůstky	přírůstek	k1gInPc7	přírůstek
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
klokáně	klokána	k1gFnSc6	klokána
strká	strkat	k5eAaImIp3nS	strkat
hlavu	hlava	k1gFnSc4	hlava
do	do	k7c2	do
vaku	vak	k1gInSc2	vak
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
napít	napít	k5eAaBmF	napít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
již	již	k6eAd1	již
pase	pást	k5eAaImIp3nS	pást
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
<g/>
,	,	kIx,	,
neleze	lézt	k5eNaImIp3nS	lézt
již	již	k6eAd1	již
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
vaku	vak	k1gInSc2	vak
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
matky	matka	k1gFnSc2	matka
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
9	[number]	k4	9
až	až	k9	až
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
28	[number]	k4	28
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klokaní	klokaní	k2eAgFnSc1d1	klokaní
slepota	slepota	k1gFnSc1	slepota
==	==	k?	==
</s>
</p>
<p>
<s>
Choroba	choroba	k1gFnSc1	choroba
očí	oko	k1gNnPc2	oko
u	u	k7c2	u
klokanovitých	klokanovitý	k2eAgInPc2d1	klokanovitý
je	být	k5eAaImIp3nS	být
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
nová	nový	k2eAgFnSc1d1	nová
<g/>
.	.	kIx.	.
</s>
<s>
Prvá	prvý	k4xOgFnSc1	prvý
úřední	úřední	k2eAgFnSc1d1	úřední
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
klokaní	klokaní	k2eAgFnSc6d1	klokaní
slepotě	slepota	k1gFnSc6	slepota
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
slepotě	slepota	k1gFnSc6	slepota
přišly	přijít	k5eAaPmAgFnP	přijít
i	i	k8xC	i
z	z	k7c2	z
jižních	jižní	k2eAgInPc2d1	jižní
států	stát	k1gInPc2	stát
Viktorie	Viktoria	k1gFnSc2	Viktoria
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
choroba	choroba	k1gFnSc1	choroba
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
pouštěmi	poušť	k1gFnPc7	poušť
až	až	k8xS	až
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Australané	Australan	k1gMnPc1	Australan
se	se	k3xPyFc4	se
znepokojovali	znepokojovat	k5eAaImAgMnP	znepokojovat
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gInSc3	její
možnému	možný	k2eAgInSc3d1	možný
přenosu	přenos	k1gInSc3	přenos
na	na	k7c4	na
dobytek	dobytek	k1gInSc4	dobytek
a	a	k8xC	a
lidi	člověk	k1gMnPc4	člověk
samé	samý	k3xTgFnSc3	samý
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
v	v	k7c6	v
Australských	australský	k2eAgFnPc6d1	australská
živočišných	živočišný	k2eAgFnPc6d1	živočišná
zdravotních	zdravotní	k2eAgFnPc6d1	zdravotní
laboratořích	laboratoř	k1gFnPc6	laboratoř
(	(	kIx(	(
<g/>
AAHL	AAHL	kA	AAHL
<g/>
)	)	kIx)	)
v	v	k7c6	v
Geelongu	Geelong	k1gInSc6	Geelong
objevilo	objevit	k5eAaPmAgNnS	objevit
virus	virus	k1gInSc4	virus
zvaný	zvaný	k2eAgInSc4d1	zvaný
Wallal	Wallal	k1gFnSc7	Wallal
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
druzích	druh	k1gInPc6	druh
komárů	komár	k1gMnPc2	komár
(	(	kIx(	(
<g/>
mušek	muška	k1gFnPc2	muška
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
písečných	písečný	k2eAgFnPc2d1	písečná
much	moucha	k1gFnPc2	moucha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tento	tento	k3xDgInSc1	tento
virus	virus	k1gInSc1	virus
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přenášely	přenášet	k5eAaImAgInP	přenášet
<g/>
.	.	kIx.	.
</s>
<s>
Zvěrolékaři	zvěrolékař	k1gMnPc1	zvěrolékař
sledováním	sledování	k1gNnSc7	sledování
klokaní	klokaní	k2eAgFnSc2d1	klokaní
populace	populace	k1gFnSc2	populace
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
méně	málo	k6eAd2	málo
jak	jak	k8xC	jak
3	[number]	k4	3
%	%	kIx~	%
klokanů	klokan	k1gMnPc2	klokan
vystavených	vystavená	k1gFnPc2	vystavená
viru	vir	k1gInSc2	vir
se	se	k3xPyFc4	se
slepota	slepota	k1gFnSc1	slepota
projevila	projevit	k5eAaPmAgFnS	projevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
klokani	klokan	k1gMnPc1	klokan
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
australské	australský	k2eAgMnPc4d1	australský
domorodce	domorodec	k1gMnPc4	domorodec
byl	být	k5eAaImAgMnS	být
klokan	klokan	k1gMnSc1	klokan
významnou	významný	k2eAgFnSc4d1	významná
lovnou	lovný	k2eAgFnSc4d1	lovná
zvěří	zvěř	k1gFnSc7	zvěř
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
kmeny	kmen	k1gInPc4	kmen
dokonce	dokonce	k9	dokonce
představovalo	představovat	k5eAaImAgNnS	představovat
klokaní	klokaní	k2eAgNnSc4d1	klokaní
maso	maso	k1gNnSc4	maso
hlavní	hlavní	k2eAgInSc1d1	hlavní
zdroj	zdroj	k1gInSc1	zdroj
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Domorodci	domorodec	k1gMnPc1	domorodec
lovili	lovit	k5eAaImAgMnP	lovit
klokany	klokan	k1gMnPc4	klokan
pomocí	pomocí	k7c2	pomocí
oštěpů	oštěp	k1gInPc2	oštěp
a	a	k8xC	a
bumerangů	bumerang	k1gInPc2	bumerang
nebo	nebo	k8xC	nebo
je	on	k3xPp3gNnPc4	on
chytali	chytat	k5eAaImAgMnP	chytat
do	do	k7c2	do
pastí	past	k1gFnPc2	past
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zabitého	zabitý	k1gMnSc2	zabitý
klokana	klokan	k1gMnSc2	klokan
uměli	umět	k5eAaImAgMnP	umět
využít	využít	k5eAaPmF	využít
vše	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc4	vnitřnost
jedli	jíst	k5eAaImAgMnP	jíst
<g/>
,	,	kIx,	,
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
zhotovovali	zhotovovat	k5eAaImAgMnP	zhotovovat
oděvy	oděv	k1gInPc4	oděv
,	,	kIx,	,
vaky	vak	k1gInPc1	vak
a	a	k8xC	a
masky	maska	k1gFnPc1	maska
<g/>
,	,	kIx,	,
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
nástroje	nástroj	k1gInSc2	nástroj
a	a	k8xC	a
ozdoby	ozdoba	k1gFnSc2	ozdoba
<g/>
,	,	kIx,	,
šlachy	šlacha	k1gFnPc1	šlacha
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
provazy	provaz	k1gInPc4	provaz
a	a	k8xC	a
Kurnaiové	Kurnaius	k1gMnPc1	Kurnaius
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Austrálie	Austrálie	k1gFnSc2	Austrálie
používali	používat	k5eAaImAgMnP	používat
sušený	sušený	k2eAgInSc4d1	sušený
šourek	šourek	k1gInSc4	šourek
klokaního	klokaní	k2eAgInSc2d1	klokaní
samce	samec	k1gInSc2	samec
místo	místo	k7c2	místo
míče	míč	k1gInSc2	míč
při	při	k7c6	při
tradiční	tradiční	k2eAgFnSc6d1	tradiční
hře	hra	k1gFnSc6	hra
marngrook	marngrook	k1gInSc4	marngrook
<g/>
.	.	kIx.	.
</s>
<s>
Klokan	klokan	k1gMnSc1	klokan
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
také	také	k9	také
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
mytologii	mytologie	k1gFnSc6	mytologie
a	a	k8xC	a
rituálech	rituál	k1gInPc6	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
<g/>
,	,	kIx,	,
odehrávající	odehrávající	k2eAgInPc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
mýtickém	mýtický	k2eAgInSc6d1	mýtický
čase	čas	k1gInSc6	čas
snění	snění	k1gNnSc2	snění
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
klokanech	klokan	k1gMnPc6	klokan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tehdy	tehdy	k6eAd1	tehdy
vypadali	vypadat	k5eAaImAgMnP	vypadat
jinak	jinak	k6eAd1	jinak
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
podobnější	podobný	k2eAgMnPc1d2	podobnější
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
kmeny	kmen	k1gInPc1	kmen
spojují	spojovat	k5eAaImIp3nP	spojovat
klokana	klokan	k1gMnSc4	klokan
se	s	k7c7	s
sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
s	s	k7c7	s
mýty	mýtus	k1gInPc7	mýtus
o	o	k7c6	o
původu	původ	k1gInSc6	původ
ohně	oheň	k1gInSc2	oheň
či	či	k8xC	či
některých	některý	k3yIgInPc2	některý
útvarů	útvar	k1gInPc2	útvar
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
mající	mající	k2eAgMnPc1d1	mající
možnost	možnost	k1gFnSc4	možnost
vidět	vidět	k5eAaImF	vidět
klokany	klokan	k1gMnPc4	klokan
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
byl	být	k5eAaImAgMnS	být
holandský	holandský	k2eAgMnSc1d1	holandský
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
François	François	k1gFnPc2	François
Pelsaert	Pelsaert	k1gMnSc1	Pelsaert
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
1629	[number]	k4	1629
ztroskotal	ztroskotat	k5eAaPmAgInS	ztroskotat
u	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
klokany	klokan	k1gMnPc7	klokan
setkal	setkat	k5eAaPmAgMnS	setkat
také	také	k9	také
pirát	pirát	k1gMnSc1	pirát
William	William	k1gInSc4	William
Dampier	Dampira	k1gFnPc2	Dampira
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
společník	společník	k1gMnSc1	společník
<g/>
,	,	kIx,	,
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Joseph	Josepha	k1gFnPc2	Josepha
Banks	Banks	k1gInSc1	Banks
<g/>
,	,	kIx,	,
Cook	Cook	k1gMnSc1	Cook
klokana	klokan	k1gMnSc2	klokan
popsal	popsat	k5eAaPmAgInS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
zvíře	zvíře	k1gNnSc1	zvíře
podobné	podobný	k2eAgNnSc1d1	podobné
chrtu	chrt	k1gMnSc3	chrt
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
poskakuje	poskakovat	k5eAaImIp3nS	poskakovat
po	po	k7c6	po
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
zadních	zadní	k2eAgFnPc6d1	zadní
nohách	noha	k1gFnPc6	noha
jako	jako	k9	jako
luční	luční	k2eAgFnSc1d1	luční
kobylka	kobylka	k1gFnSc1	kobylka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
klokani	klokan	k1gMnPc1	klokan
jako	jako	k9	jako
atrakce	atrakce	k1gFnSc2	atrakce
dovezeni	dovézt	k5eAaPmNgMnP	dovézt
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
angličtí	anglický	k2eAgMnPc1d1	anglický
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
trestanci	trestanec	k1gMnPc1	trestanec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
živili	živit	k5eAaImAgMnP	živit
klokaním	klokaní	k2eAgNnSc7d1	klokaní
masem	maso	k1gNnSc7	maso
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
domorodci	domorodec	k1gMnPc1	domorodec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
hlavně	hlavně	k9	hlavně
němečtí	německý	k2eAgMnPc1d1	německý
a	a	k8xC	a
polští	polský	k2eAgMnPc1d1	polský
emigranti	emigrant	k1gMnPc1	emigrant
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
australští	australský	k2eAgMnPc1d1	australský
farmáři	farmář	k1gMnPc1	farmář
jej	on	k3xPp3gMnSc4	on
obvykle	obvykle	k6eAd1	obvykle
nejedí	jíst	k5eNaImIp3nP	jíst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
probíhal	probíhat	k5eAaImAgInS	probíhat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
odstřel	odstřel	k1gInSc1	odstřel
klokanů	klokan	k1gMnPc2	klokan
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
pro	pro	k7c4	pro
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	s	k7c7	s
kvalitou	kvalita	k1gFnSc7	kvalita
podobá	podobat	k5eAaImIp3nS	podobat
teletině	teletina	k1gFnSc3	teletina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
hlavně	hlavně	k9	hlavně
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
klokaní	klokaní	k2eAgNnSc1d1	klokaní
maso	maso	k1gNnSc1	maso
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
vyváženo	vyvážet	k5eAaImNgNnS	vyvážet
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
USA	USA	kA	USA
i	i	k8xC	i
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
většinou	většinou	k6eAd1	většinou
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
konzervy	konzerva	k1gFnPc1	konzerva
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
však	však	k9	však
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
mnoho	mnoho	k6eAd1	mnoho
států	stát	k1gInPc2	stát
dovoz	dovoz	k1gInSc4	dovoz
klokaního	klokaní	k2eAgNnSc2d1	klokaní
masa	maso	k1gNnSc2	maso
omezuje	omezovat	k5eAaImIp3nS	omezovat
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
již	již	k9	již
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
psího	psí	k2eAgNnSc2d1	psí
krmení	krmení	k1gNnSc2	krmení
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
našlo	najít	k5eAaPmAgNnS	najít
uplatnění	uplatnění	k1gNnSc4	uplatnění
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
i	i	k9	i
v	v	k7c6	v
evropských	evropský	k2eAgInPc6d1	evropský
obchodech	obchod	k1gInPc6	obchod
a	a	k8xC	a
restauracích	restaurace	k1gFnPc6	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
má	mít	k5eAaImIp3nS	mít
tmavou	tmavý	k2eAgFnSc4d1	tmavá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
vláknité	vláknitý	k2eAgNnSc1d1	vláknité
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
chuť	chuť	k1gFnSc4	chuť
podobnou	podobný	k2eAgFnSc4d1	podobná
srnčí	srnčí	k2eAgFnSc4d1	srnčí
nebo	nebo	k8xC	nebo
daňčí	daňčí	k2eAgFnSc4d1	daňčí
zvěřině	zvěřina	k1gFnSc3	zvěřina
<g/>
.	.	kIx.	.
</s>
<s>
Nejchutnější	chutný	k2eAgNnSc1d3	nejchutnější
je	být	k5eAaImIp3nS	být
dušené	dušený	k2eAgNnSc1d1	dušené
nebo	nebo	k8xC	nebo
vařené	vařený	k2eAgNnSc1d1	vařené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Australští	australský	k2eAgMnPc1d1	australský
farmáři	farmář	k1gMnPc1	farmář
vidí	vidět	k5eAaImIp3nP	vidět
v	v	k7c6	v
klokanech	klokan	k1gMnPc6	klokan
škůdce	škůdce	k1gMnSc1	škůdce
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
je	on	k3xPp3gFnPc4	on
zničili	zničit	k5eAaPmAgMnP	zničit
<g/>
,	,	kIx,	,
otrávili	otrávit	k5eAaPmAgMnP	otrávit
farmáři	farmář	k1gMnPc1	farmář
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
oblasti	oblast	k1gFnSc6	oblast
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
během	během	k7c2	během
dvou	dva	k4xCgInPc2	dva
měsíců	měsíc	k1gInPc2	měsíc
13	[number]	k4	13
000	[number]	k4	000
klokanů	klokan	k1gMnPc2	klokan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
farmě	farma	k1gFnSc6	farma
otrávili	otrávit	k5eAaPmAgMnP	otrávit
během	během	k7c2	během
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
90	[number]	k4	90
000	[number]	k4	000
klokanů	klokan	k1gMnPc2	klokan
a	a	k8xC	a
pobouřené	pobouřený	k2eAgFnPc1d1	pobouřená
noviny	novina	k1gFnPc1	novina
psaly	psát	k5eAaImAgFnP	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Symbol	symbol	k1gInSc1	symbol
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
krmíme	krmit	k5eAaImIp1nP	krmit
strychninem	strychnin	k1gInSc7	strychnin
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dále	daleko	k6eAd2	daleko
farmáři	farmář	k1gMnPc1	farmář
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
zahnat	zahnat	k5eAaPmF	zahnat
klokany	klokan	k1gMnPc4	klokan
ohněm	oheň	k1gInSc7	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
vypalováním	vypalování	k1gNnSc7	vypalování
se	se	k3xPyFc4	se
pastviny	pastvina	k1gFnPc1	pastvina
pohnojí	pohnojit	k5eAaPmIp3nP	pohnojit
<g/>
.	.	kIx.	.
</s>
<s>
Vypalování	vypalování	k1gNnSc1	vypalování
však	však	k9	však
dobře	dobře	k6eAd1	dobře
snášejí	snášet	k5eAaImIp3nP	snášet
nejodolnější	odolný	k2eAgFnPc1d3	nejodolnější
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
výživné	výživný	k2eAgFnPc4d1	výživná
trávy	tráva	k1gFnPc4	tráva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
již	již	k6eAd1	již
ovcím	ovce	k1gFnPc3	ovce
nevyhovují	vyhovovat	k5eNaImIp3nP	vyhovovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
klokanům	klokan	k1gMnPc3	klokan
stále	stále	k6eAd1	stále
postačují	postačovat	k5eAaImIp3nP	postačovat
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
pastvin	pastvina	k1gFnPc2	pastvina
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
neustále	neustále	k6eAd1	neustále
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Vypalování	vypalování	k1gNnSc1	vypalování
pastvin	pastvina	k1gFnPc2	pastvina
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vymýtit	vymýtit	k5eAaPmF	vymýtit
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
také	také	k9	také
zničující	zničující	k2eAgInPc4d1	zničující
lesní	lesní	k2eAgInPc4d1	lesní
požáry	požár	k1gInPc4	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přirozené	přirozený	k2eAgNnSc1d1	přirozené
prostředí	prostředí	k1gNnSc1	prostředí
klokanovitých	klokanovitý	k2eAgInPc2d1	klokanovitý
se	se	k3xPyFc4	se
vlivem	vlivem	k7c2	vlivem
člověka	člověk	k1gMnSc2	člověk
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
ohrožení	ohrožení	k1gNnSc4	ohrožení
těchto	tento	k3xDgMnPc2	tento
zajímavých	zajímavý	k2eAgMnPc2d1	zajímavý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Zavlečení	zavlečený	k2eAgMnPc1d1	zavlečený
králíci	králík	k1gMnPc1	králík
a	a	k8xC	a
ovce	ovce	k1gFnPc1	ovce
spásají	spásat	k5eAaPmIp3nP	spásat
klokanům	klokan	k1gMnPc3	klokan
trávu	tráva	k1gFnSc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
přináší	přinášet	k5eAaImIp3nS	přinášet
i	i	k9	i
zavlečené	zavlečený	k2eAgFnPc4d1	zavlečená
lišky	liška	k1gFnPc4	liška
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
raději	rád	k6eAd2	rád
než	než	k8xS	než
králíky	králík	k1gMnPc4	králík
loví	lovit	k5eAaImIp3nP	lovit
méně	málo	k6eAd2	málo
pohyblivá	pohyblivý	k2eAgNnPc1d1	pohyblivé
klokaní	klokaní	k2eAgNnPc1d1	klokaní
mláďata	mládě	k1gNnPc1	mládě
nebo	nebo	k8xC	nebo
malé	malý	k2eAgInPc1d1	malý
druhy	druh	k1gInPc1	druh
klokanů	klokan	k1gMnPc2	klokan
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
klokan	klokan	k1gMnSc1	klokan
quokka	quokka	k1gMnSc1	quokka
a	a	k8xC	a
klokánek	klokánek	k1gMnSc1	klokánek
králíkovitý	králíkovitý	k2eAgMnSc1d1	králíkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
klokanů	klokan	k1gMnPc2	klokan
také	také	k9	také
zahyne	zahynout	k5eAaPmIp3nS	zahynout
na	na	k7c6	na
silnicích	silnice	k1gFnPc6	silnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
aut	auto	k1gNnPc2	auto
s	s	k7c7	s
klokany	klokan	k1gMnPc7	klokan
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Nové	Nové	k2eAgFnSc6d1	Nové
Guineji	Guinea	k1gFnSc6	Guinea
netrpí	trpět	k5eNaImIp3nP	trpět
ztrátou	ztráta	k1gFnSc7	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
až	až	k9	až
tak	tak	k6eAd1	tak
jako	jako	k8xS	jako
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
stromovým	stromový	k2eAgMnPc3d1	stromový
druhům	druh	k1gMnPc3	druh
však	však	k9	však
ubývají	ubývat	k5eAaImIp3nP	ubývat
lesy	les	k1gInPc1	les
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nadměrného	nadměrný	k2eAgNnSc2d1	nadměrné
kácení	kácení	k1gNnSc2	kácení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rody	rod	k1gInPc1	rod
a	a	k8xC	a
druhy	druh	k1gInPc1	druh
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dendrolagus	Dendrolagus	k1gInSc4	Dendrolagus
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
pralesní	pralesní	k2eAgMnSc1d1	pralesní
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
bennettianus	bennettianus	k1gMnSc1	bennettianus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Doriův	Doriův	k2eAgMnSc1d1	Doriův
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
dorianus	dorianus	k1gMnSc1	dorianus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Goodfellowův	Goodfellowův	k2eAgMnSc1d1	Goodfellowův
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
goodfellowi	goodfellow	k1gFnSc2	goodfellow
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
hnědý	hnědý	k2eAgMnSc1d1	hnědý
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
inustus	inustus	k1gMnSc1	inustus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
stromový	stromový	k2eAgMnSc1d1	stromový
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
lumholtzi	lumholtze	k1gFnSc4	lumholtze
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Matschieův	Matschieův	k2eAgMnSc1d1	Matschieův
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
matschiei	matschie	k1gFnSc2	matschie
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
tmavý	tmavý	k2eAgMnSc1d1	tmavý
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
scottae	scottaat	k5eAaPmIp3nS	scottaat
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
nížinný	nížinný	k2eAgInSc1d1	nížinný
Dendrolagus	Dendrolagus	k1gInSc1	Dendrolagus
spadix	spadix	k1gInSc1	spadix
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
medvědí	medvědí	k2eAgMnSc1d1	medvědí
Dendrolagus	Dendrolagus	k1gMnSc1	Dendrolagus
ursinus	ursinus	k1gMnSc1	ursinus
</s>
</p>
<p>
<s>
===	===	k?	===
Dorcopsis	Dorcopsis	k1gFnPc7	Dorcopsis
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
černý	černý	k2eAgInSc1d1	černý
Dorcopsis	Dorcopsis	k1gInSc1	Dorcopsis
atrata	atrat	k1gMnSc2	atrat
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Hagenův	Hagenův	k2eAgMnSc1d1	Hagenův
Dorcopsis	Dorcopsis	k1gFnPc4	Dorcopsis
hageni	haget	k5eAaImNgMnP	haget
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
žlutoskvrný	žlutoskvrný	k2eAgInSc4d1	žlutoskvrný
Dorcopsis	Dorcopsis	k1gInSc4	Dorcopsis
luctuosa	luctuosa	k1gFnSc1	luctuosa
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
novoguinejský	novoguinejský	k2eAgInSc4d1	novoguinejský
Dorcopsis	Dorcopsis	k1gInSc4	Dorcopsis
muelleri	muelleri	k6eAd1	muelleri
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
MacLayův	MacLayův	k2eAgMnSc1d1	MacLayův
Dorcopsulus	Dorcopsulus	k1gMnSc1	Dorcopsulus
macleayi	macleay	k1gFnSc2	macleay
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
lysoocasý	lysoocasý	k2eAgMnSc1d1	lysoocasý
Dorcopsulus	Dorcopsulus	k1gMnSc1	Dorcopsulus
vanheurni	vanheurnit	k5eAaPmRp2nS	vanheurnit
</s>
</p>
<p>
<s>
===	===	k?	===
Lagorchestes	Lagorchestes	k1gInSc4	Lagorchestes
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Finlaysonův	Finlaysonův	k2eAgMnSc1d1	Finlaysonův
Lagorchestes	Lagorchestes	k1gMnSc1	Lagorchestes
asomatus	asomatus	k1gMnSc1	asomatus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
pruhovaný	pruhovaný	k2eAgMnSc1d1	pruhovaný
Lagorchestes	Lagorchestes	k1gMnSc1	Lagorchestes
conspicillatus	conspicillatus	k1gMnSc1	conspicillatus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
kosmatý	kosmatý	k2eAgMnSc1d1	kosmatý
Lagorchestes	Lagorchestes	k1gMnSc1	Lagorchestes
hirsutus	hirsutus	k1gMnSc1	hirsutus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
zaječí	zaječet	k5eAaPmIp3nS	zaječet
Lagorchestes	Lagorchestes	k1gMnSc1	Lagorchestes
leporides	leporides	k1gMnSc1	leporides
</s>
</p>
<p>
<s>
===	===	k?	===
Lagostrophus	Lagostrophus	k1gInSc4	Lagostrophus
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
páskovaný	páskovaný	k2eAgMnSc1d1	páskovaný
Lagostrophus	Lagostrophus	k1gMnSc1	Lagostrophus
fasciatus	fasciatus	k1gMnSc1	fasciatus
</s>
</p>
<p>
<s>
===	===	k?	===
Macropus	Macropus	k1gInSc4	Macropus
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
hbitý	hbitý	k2eAgMnSc1d1	hbitý
Macropus	Macropus	k1gMnSc1	Macropus
agilis	agilis	k1gFnSc2	agilis
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
antilopí	antilopí	k2eAgMnSc1d1	antilopí
Macropus	Macropus	k1gMnSc1	Macropus
antilopinus	antilopinus	k1gMnSc1	antilopinus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
vraný	vraný	k2eAgMnSc1d1	vraný
Macropus	Macropus	k1gMnSc1	Macropus
bernardus	bernardus	k1gMnSc1	bernardus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
hřbetopásý	hřbetopásý	k2eAgMnSc1d1	hřbetopásý
Macropus	Macropus	k1gMnSc1	Macropus
dorsalis	dorsalis	k1gFnSc2	dorsalis
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
dama	dama	k1gMnSc1	dama
Macropus	Macropus	k1gMnSc1	Macropus
eugenii	eugenie	k1gFnSc4	eugenie
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
velký	velký	k2eAgMnSc1d1	velký
Macropus	Macropus	k1gMnSc1	Macropus
fuliginosus	fuliginosus	k1gMnSc1	fuliginosus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
obrovský	obrovský	k2eAgMnSc1d1	obrovský
Macropus	Macropus	k1gMnSc1	Macropus
giganteus	giganteus	k1gMnSc1	giganteus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Greyův	Greyův	k2eAgMnSc1d1	Greyův
Macropus	Macropus	k1gMnSc1	Macropus
greyi	grey	k1gFnSc2	grey
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
irma	irma	k1gMnSc1	irma
Macropus	Macropus	k1gMnSc1	Macropus
irma	irma	k1gMnSc1	irma
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
parma	parma	k1gFnSc1	parma
Macropus	Macropus	k1gInSc1	Macropus
parma	parma	k1gFnSc1	parma
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Parryův	Parryův	k2eAgMnSc1d1	Parryův
Macropus	Macropus	k1gMnSc1	Macropus
parryi	parry	k1gFnSc2	parry
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
horský	horský	k2eAgMnSc1d1	horský
Macropus	Macropus	k1gMnSc1	Macropus
robustus	robustus	k1gMnSc1	robustus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
rudokrký	rudokrký	k2eAgMnSc1d1	rudokrký
Macropus	Macropus	k1gMnSc1	Macropus
rufogriseus	rufogriseus	k1gMnSc1	rufogriseus
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
rudý	rudý	k1gMnSc1	rudý
Macropus	Macropus	k1gMnSc1	Macropus
rufus	rufus	k1gMnSc1	rufus
</s>
</p>
<p>
<s>
===	===	k?	===
Onychogalea	Onychogaleum	k1gNnSc2	Onychogaleum
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
uzdičkový	uzdičkový	k2eAgMnSc1d1	uzdičkový
Onychogalea	Onychogalea	k1gMnSc1	Onychogalea
fraenata	fraenat	k1gMnSc2	fraenat
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
měsíčitopásý	měsíčitopásý	k2eAgMnSc1d1	měsíčitopásý
Onychogalea	Onychogalea	k1gMnSc1	Onychogalea
lunata	lunat	k1gMnSc2	lunat
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
hrotochvostý	hrotochvostý	k2eAgMnSc1d1	hrotochvostý
Onychogalea	Onychogalea	k1gMnSc1	Onychogalea
unguifera	unguifero	k1gNnSc2	unguifero
</s>
</p>
<p>
<s>
===	===	k?	===
Petrogale	Petrogala	k1gFnSc6	Petrogala
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
společenský	společenský	k2eAgInSc1d1	společenský
Petrogale	Petrogala	k1gFnSc3	Petrogala
assimilis	assimilis	k1gFnSc2	assimilis
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
krátkouchý	krátkouchý	k2eAgInSc1d1	krátkouchý
Petrogale	Petrogala	k1gFnSc3	Petrogala
brachyotis	brachyotis	k1gFnSc2	brachyotis
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
warabi	warabi	k6eAd1	warabi
Petrogale	Petrogala	k1gFnSc6	Petrogala
burbidgei	burbidge	k1gFnSc6	burbidge
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
rezavý	rezavý	k2eAgMnSc1d1	rezavý
Petrogale	Petrogala	k1gFnSc6	Petrogala
concinna	concinn	k1gMnSc2	concinn
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Godmanův	Godmanův	k2eAgMnSc1d1	Godmanův
Petrogale	Petrogal	k1gMnSc5	Petrogal
godmani	godman	k1gMnPc1	godman
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
nevzhledný	vzhledný	k2eNgMnSc1d1	nevzhledný
Petrogale	Petrogala	k1gFnSc6	Petrogala
inornata	inornat	k2eAgMnSc4d1	inornat
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
černonohý	černonohý	k2eAgInSc1d1	černonohý
Petrogale	Petrogala	k1gFnSc3	Petrogala
lateralis	lateralis	k1gFnSc2	lateralis
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
skalní	skalní	k2eAgFnSc3d1	skalní
Petrogale	Petrogala	k1gFnSc3	Petrogala
penicillata	penicille	k1gNnPc5	penicille
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
východní	východní	k2eAgFnSc3d1	východní
Petrogale	Petrogala	k1gFnSc3	Petrogala
persephone	persephon	k1gInSc5	persephon
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
Rotschildův	Rotschildův	k2eAgMnSc1d1	Rotschildův
Petrogale	Petrogala	k1gFnSc3	Petrogala
rothschildi	rothschild	k1gMnPc5	rothschild
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
žlutonohý	žlutonohý	k2eAgMnSc1d1	žlutonohý
Petrogale	Petrogala	k1gFnSc3	Petrogala
xanthopus	xanthopus	k1gInSc4	xanthopus
</s>
</p>
<p>
<s>
===	===	k?	===
Setonix	Setonix	k1gInSc4	Setonix
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
quokka	quokek	k1gInSc2	quokek
Setonix	Setonix	k1gInSc1	Setonix
brachyurus	brachyurus	k1gInSc1	brachyurus
</s>
</p>
<p>
<s>
===	===	k?	===
Thylogale	Thylogala	k1gFnSc6	Thylogala
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
rudoboký	rudoboký	k2eAgMnSc1d1	rudoboký
Thylogale	Thylogala	k1gFnSc3	Thylogala
billardierii	billardierie	k1gFnSc3	billardierie
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
uru	uru	k?	uru
Thylogale	Thylogala	k1gFnSc6	Thylogala
brunii	brunie	k1gFnSc3	brunie
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
znamenaný	znamenaný	k1gMnSc1	znamenaný
Thylogale	Thylogala	k1gFnSc3	Thylogala
stigmatica	stigmatica	k6eAd1	stigmatica
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
pademelon	pademelon	k1gInSc4	pademelon
Thylogale	Thylogala	k1gFnSc3	Thylogala
thetis	thetis	k1gInSc1	thetis
</s>
</p>
<p>
<s>
===	===	k?	===
Wallabia	Wallabium	k1gNnSc2	Wallabium
===	===	k?	===
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
bažinný	bažinný	k2eAgMnSc1d1	bažinný
Wallabia	Wallabium	k1gNnSc2	Wallabium
bicolor	bicolor	k1gMnSc1	bicolor
</s>
</p>
<p>
<s>
klokan	klokan	k1gMnSc1	klokan
dlouhoocasý	dlouhoocasý	k2eAgMnSc1d1	dlouhoocasý
Wallabia	Wallabium	k1gNnSc2	Wallabium
elegans	elegans	k6eAd1	elegans
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zoo	zoo	k1gFnPc6	zoo
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
chováno	chovat	k5eAaImNgNnS	chovat
14	[number]	k4	14
druhů	druh	k1gInPc2	druh
klokanovitých	klokanovitý	k2eAgInPc2d1	klokanovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
chováno	chovat	k5eAaImNgNnS	chovat
deset	deset	k4xCc1	deset
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tyto	tento	k3xDgInPc4	tento
taxony	taxon	k1gInPc4	taxon
<g/>
:	:	kIx,	:
klokan	klokan	k1gMnSc1	klokan
Hagenův	Hagenův	k2eAgMnSc1d1	Hagenův
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
obrovský	obrovský	k2eAgMnSc1d1	obrovský
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
parma	parma	k1gFnSc1	parma
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
horský	horský	k2eAgMnSc1d1	horský
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
rudokrký	rudokrký	k2eAgMnSc1d1	rudokrký
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
rudý	rudý	k1gMnSc1	rudý
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
žlutonohý	žlutonohý	k2eAgMnSc1d1	žlutonohý
<g/>
,	,	kIx,	,
klokan	klokan	k1gMnSc1	klokan
uru	uru	k?	uru
a	a	k8xC	a
klokan	klokan	k1gMnSc1	klokan
bažinný	bažinný	k2eAgMnSc1d1	bažinný
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
kolekce	kolekce	k1gFnSc1	kolekce
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
sedm	sedm	k4xCc4	sedm
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
následovaná	následovaný	k2eAgFnSc1d1	následovaná
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
žije	žít	k5eAaImIp3nS	žít
šest	šest	k4xCc4	šest
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
klokana	klokan	k1gMnSc2	klokan
Hagenova	Hagenův	k2eAgFnSc1d1	Hagenova
se	se	k3xPyFc4	se
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
stala	stát	k5eAaPmAgFnS	stát
přitom	přitom	k6eAd1	přitom
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
zoo	zoo	k1gFnSc1	zoo
mimo	mimo	k7c4	mimo
Asii	Asie	k1gFnSc4	Asie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
odchov	odchov	k1gInSc1	odchov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Klokánkovití	Klokánkovitý	k2eAgMnPc1d1	Klokánkovitý
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
klokanovití	klokanovitý	k2eAgMnPc1d1	klokanovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
choroba	choroba	k1gFnSc1	choroba
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
milióny	milión	k4xCgInPc1	milión
klokanů	klokan	k1gMnPc2	klokan
In	In	k1gFnSc2	In
<g/>
:	:	kIx,	:
Novinky	novinka	k1gFnPc1	novinka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2017-12-30	[number]	k4	2017-12-30
</s>
</p>
<p>
<s>
Video	video	k1gNnSc1	video
o	o	k7c6	o
evoluci	evoluce	k1gFnSc6	evoluce
klokanovitých	klokanovitý	k2eAgFnPc2d1	klokanovitý
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
12	[number]	k4	12
milionech	milion	k4xCgInPc6	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
