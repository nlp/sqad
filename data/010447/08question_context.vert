<s>
Klokanovití	Klokanovitý	k2eAgMnPc1d1	Klokanovitý
(	(	kIx(	(
<g/>
Macropodidae	Macropodidae	k1gNnSc7	Macropodidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
čeleď	čeleď	k1gFnSc1	čeleď
vačnatců	vačnatec	k1gMnPc2	vačnatec
(	(	kIx(	(
<g/>
po	po	k7c6	po
vačicovitých	vačicovitý	k2eAgFnPc6d1	vačicovitý
<g/>
)	)	kIx)	)
s	s	k7c7	s
54	[number]	k4	54
druhy	druh	k1gInPc7	druh
v	v	k7c6	v
11	[number]	k4	11
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Klokani	klokan	k1gMnPc1	klokan
se	se	k3xPyFc4	se
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
v	v	k7c6	v
období	období	k1gNnSc6	období
pozdního	pozdní	k2eAgInSc2d1	pozdní
miocénu	miocén	k1gInSc2	miocén
až	až	k8xS	až
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
5	[number]	k4	5
až	až	k9	až
12	[number]	k4	12
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
však	však	k9	však
klokánkovité	klokánkovitý	k2eAgNnSc1d1	klokánkovitý
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
čeleď	čeleď	k1gFnSc4	čeleď
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
klokanovitým	klokanovitý	k2eAgInPc3d1	klokanovitý
podobají	podobat	k5eAaImIp3nP	podobat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
středně	středně	k6eAd1	středně
velké	velký	k2eAgMnPc4d1	velký
až	až	k8xS	až
velké	velký	k2eAgMnPc4d1	velký
vačnatce	vačnatec	k1gMnPc4	vačnatec
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
největší	veliký	k2eAgInPc1d3	veliký
druhy	druh	k1gInPc1	druh
žijících	žijící	k2eAgMnPc2d1	žijící
vačnatců	vačnatec	k1gMnPc2	vačnatec
<g/>
)	)	kIx)	)
s	s	k7c7	s
prodlouženými	prodloužený	k2eAgFnPc7d1	prodloužená
zadními	zadní	k2eAgFnPc7d1	zadní
končetinami	končetina	k1gFnPc7	končetina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
u	u	k7c2	u
pozemních	pozemní	k2eAgInPc2d1	pozemní
druhů	druh	k1gInPc2	druh
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
skákavému	skákavý	k2eAgInSc3d1	skákavý
pohybu	pohyb	k1gInSc3	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stromových	stromový	k2eAgInPc2d1	stromový
druhů	druh	k1gInPc2	druh
není	být	k5eNaImIp3nS	být
prodloužení	prodloužení	k1gNnSc4	prodloužení
zadních	zadní	k2eAgFnPc2d1	zadní
končetin	končetina	k1gFnPc2	končetina
tak	tak	k6eAd1	tak
výrazné	výrazný	k2eAgFnPc4d1	výrazná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
býložravých	býložravý	k2eAgMnPc2d1	býložravý
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc4d1	české
slovo	slovo	k1gNnSc4	slovo
klokan	klokan	k1gMnSc1	klokan
nenajdeme	najít	k5eNaPmIp1nP	najít
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
jazyce	jazyk	k1gInSc6	jazyk
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
chorvatštiny	chorvatština	k1gFnSc2	chorvatština
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
obrozenecký	obrozenecký	k2eAgInSc4d1	obrozenecký
výtvor	výtvor	k1gInSc4	výtvor
J.	J.	kA	J.
S.	S.	kA	S.
Presla	Presla	k1gFnSc1	Presla
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
etymologie	etymologie	k1gFnSc1	etymologie
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
inspirací	inspirace	k1gFnSc7	inspirace
snad	snad	k9	snad
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
slova	slovo	k1gNnSc2	slovo
kloaka	kloaka	k1gFnSc1	kloaka
a	a	k8xC	a
skokan	skokan	k1gMnSc1	skokan
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
používané	používaný	k2eAgInPc1d1	používaný
ve	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kangaroo	kangaroo	k1gNnSc4	kangaroo
<g/>
,	,	kIx,	,
känguru	kängura	k1gFnSc4	kängura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
slovenské	slovenský	k2eAgFnPc1d1	slovenská
kengura	kengura	k1gFnSc1	kengura
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
domorodý	domorodý	k2eAgInSc4d1	domorodý
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
guugu	guug	k1gInSc2	guug
jimidhirr	jimidhirra	k1gFnPc2	jimidhirra
z	z	k7c2	z
Yorského	yorský	k2eAgInSc2d1	yorský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
jsou	být	k5eAaImIp3nP	být
endemity	endemit	k1gInPc7	endemit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc6d1	Nové
Guineji	Guinea	k1gFnSc6	Guinea
a	a	k8xC	a
na	na	k7c6	na
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
biomy	biom	k1gInPc1	biom
<g/>
,	,	kIx,	,
od	od	k7c2	od
pouště	poušť	k1gFnSc2	poušť
po	po	k7c4	po
deštný	deštný	k2eAgInSc4d1	deštný
prales	prales	k1gInSc4	prales
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klokan	klokan	k1gMnSc1	klokan
rudokrký	rudokrký	k2eAgMnSc1d1	rudokrký
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vysazený	vysazený	k2eAgInSc1d1	vysazený
i	i	k9	i
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
chován	chován	k2eAgMnSc1d1	chován
i	i	k9	i
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
oboře	obora	k1gFnSc6	obora
u	u	k7c2	u
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Klokani	klokan	k1gMnPc1	klokan
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
a	a	k8xC	a
úzkou	úzký	k2eAgFnSc4d1	úzká
lebku	lebka	k1gFnSc4	lebka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
čenich	čenich	k1gInSc4	čenich
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vypadá	vypadat	k5eAaPmIp3nS	vypadat
drobná	drobný	k2eAgFnSc1d1	drobná
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velikosti	velikost	k1gFnSc3	velikost
zbývajícího	zbývající	k2eAgNnSc2d1	zbývající
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vzrůst	vzrůst	k1gInSc1	vzrůst
je	být	k5eAaImIp3nS	být
střední	střední	k2eAgMnSc1d1	střední
až	až	k8xS	až
velký	velký	k2eAgMnSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Váhy	Váhy	k1gFnPc1	Váhy
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
od	od	k7c2	od
0,5	[number]	k4	0,5
kg	kg	kA	kg
do	do	k7c2	do
90	[number]	k4	90
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Klokanovití	Klokanovitý	k2eAgMnPc1d1	Klokanovitý
jsou	být	k5eAaImIp3nP	být
ploskochodci	ploskochodec	k1gMnPc1	ploskochodec
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
úzkou	úzký	k2eAgFnSc4d1	úzká
zadní	zadní	k2eAgFnSc4d1	zadní
tlapu	tlapa	k1gFnSc4	tlapa
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc1d1	silná
zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
tlapě	tlapa	k1gFnSc6	tlapa
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
a	a	k8xC	a
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
,	,	kIx,	,
klokanovití	klokanovitý	k2eAgMnPc1d1	klokanovitý
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
míře	míra	k1gFnSc6	míra
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
odrážení	odrážení	k1gNnSc3	odrážení
při	při	k7c6	při
skákání	skákání	k1gNnSc6	skákání
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgMnSc1	pátý
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgMnSc1d1	vnější
<g/>
)	)	kIx)	)
prst	prst	k1gInSc1	prst
je	být	k5eAaImIp3nS	být
také	také	k9	také
velký	velký	k2eAgInSc1d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
dvojitozubci	dvojitozubce	k1gMnPc1	dvojitozubce
jsou	být	k5eAaImIp3nP	být
syndaktilní	syndaktilní	k2eAgMnPc1d1	syndaktilní
(	(	kIx(	(
<g/>
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
třetí	třetí	k4xOgInSc1	třetí
prst	prst	k1gInSc1	prst
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
je	být	k5eAaImIp3nS	být
srostlý	srostlý	k2eAgInSc1d1	srostlý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Palec	palec	k1gInSc1	palec
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
úplně	úplně	k6eAd1	úplně
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
má	mít	k5eAaImIp3nS	mít
mohutný	mohutný	k2eAgInSc1d1	mohutný
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
ocas	ocas	k1gInSc1	ocas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
chapavý	chapavý	k2eAgInSc1d1	chapavý
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
balancující	balancující	k2eAgInSc1d1	balancující
či	či	k8xC	či
stabilizační	stabilizační	k2eAgInSc1d1	stabilizační
orgán	orgán	k1gInSc1	orgán
<g/>
.	.	kIx.	.
</s>
