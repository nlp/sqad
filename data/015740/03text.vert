<s>
Frank	Frank	k1gMnSc1
Wilczek	Wilczka	k1gFnPc2
</s>
<s>
Frank	Frank	k1gMnSc1
Wilczek	Wilczek	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
<g/>
New	New	k1gMnSc2
York	York	k1gInSc4
<g/>
,	,	kIx,
USA	USA	kA
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Princetonská	Princetonský	k2eAgFnSc1d1
univerzitaChicagská	univerzitaChicagská	k1gFnSc1
univerzitaMartin	univerzitaMartina	k1gFnPc2
Van	vana	k1gFnPc2
Buren	Buren	k2eAgInSc4d1
High	High	k1gInSc4
School	Schoola	k1gFnPc2
Povolání	povolání	k1gNnSc2
</s>
<s>
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
v	v	k7c6
Leidenu	Leideno	k1gNnSc6
(	(	kIx(
<g/>
od	od	k7c2
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
University	universita	k1gFnSc2
of	of	k?
California	Californium	k1gNnSc2
<g/>
,	,	kIx,
Santa	Sant	k1gInSc2
BarbaraMassachusettský	BarbaraMassachusettský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
MacArthurova	MacArthurův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
Sakurai	Sakura	k1gFnSc2
Prize	Prize	k1gFnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
Diracova	Diracův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
ICTP	ICTP	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
Dirac	Dirac	k1gFnSc1
Prize	Prize	k1gFnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
Lorentzova	Lorentzův	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Nábož	Nábož	k1gFnSc4
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc4
</s>
<s>
agnosticismus	agnosticismus	k1gInSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Betsy	Bets	k1gInPc1
Devineová	Devineová	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1973	#num#	k4
<g/>
)	)	kIx)
Web	web	k1gInSc1
</s>
<s>
www.frankwilczek.com	www.frankwilczek.com	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Frank	Frank	k1gMnSc1
Wilczek	Wilczka	k1gFnPc2
(	(	kIx(
<g/>
*	*	kIx~
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
New	New	k1gFnPc2
York	York	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
polsko-italského	polsko-italský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
fyziky	fyzika	k1gFnSc2
na	na	k7c6
MIT	MIT	kA
a	a	k8xC
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
fyziku	fyzika	k1gFnSc4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
obdržel	obdržet	k5eAaPmAgMnS
spolu	spolu	k6eAd1
s	s	k7c7
Davidem	David	k1gMnSc7
Grossem	Gross	k1gMnSc7
z	z	k7c2
Kalifornské	kalifornský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
Davidem	David	k1gMnSc7
Politzerem	Politzer	k1gMnSc7
z	z	k7c2
Kalifornského	kalifornský	k2eAgInSc2d1
technologického	technologický	k2eAgInSc2d1
institutu	institut	k1gInSc2
„	„	k?
<g/>
za	za	k7c4
objev	objev	k1gInSc4
asymptotické	asymptotický	k2eAgFnSc2d1
volnosti	volnost	k1gFnSc2
v	v	k7c6
teorii	teorie	k1gFnSc6
silné	silný	k2eAgFnSc2d1
interakce	interakce	k1gFnSc2
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
nositelem	nositel	k1gMnSc7
mj.	mj.	kA
Diracovy	Diracův	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
,	,	kIx,
Sakuraiovy	Sakuraiův	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
<g/>
,	,	kIx,
Lorentzovy	Lorentzův	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
a	a	k8xC
pamětní	pamětní	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
MFF	MFF	kA
UK	UK	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1951	#num#	k4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
matka	matka	k1gFnSc1
byla	být	k5eAaImAgFnS
italského	italský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
pocházel	pocházet	k5eAaImAgMnS
s	z	k7c2
Polska	Polsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilczek	Wilczek	k1gInSc1
navštěvoval	navštěvovat	k5eAaImAgInS
střední	střední	k2eAgFnSc4d1
školu	škola	k1gFnSc4
Martina	Martina	k1gFnSc1
van	vana	k1gFnPc2
Burena	Burena	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeho	jeho	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
a	a	k8xC
vyučující	vyučující	k2eAgMnPc1d1
rozpoznali	rozpoznat	k5eAaPmAgMnP
Wilczekův	Wilczekův	k2eAgInSc4d1
talent	talent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
dětství	dětství	k1gNnSc2
ho	on	k3xPp3gMnSc4
velmi	velmi	k6eAd1
bavily	bavit	k5eAaImAgFnP
matematické	matematický	k2eAgFnPc4d1
hádanky	hádanka	k1gFnPc4
a	a	k8xC
záhady	záhada	k1gFnPc4
<g/>
,	,	kIx,
zaujal	zaujmout	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
také	také	k9
výzkum	výzkum	k1gInSc4
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
tedy	tedy	k9
pro	pro	k7c4
vysokoškolské	vysokoškolský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1970	#num#	k4
získal	získat	k5eAaPmAgInS
bakalářský	bakalářský	k2eAgInSc1d1
titul	titul	k1gInSc1
na	na	k7c6
Chicagské	chicagský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
magisterský	magisterský	k2eAgInSc4d1
titul	titul	k1gInSc4
na	na	k7c6
Princetonské	Princetonský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
,	,	kIx,
již	již	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
ho	on	k3xPp3gMnSc4
ale	ale	k9
začala	začít	k5eAaPmAgFnS
zajímat	zajímat	k5eAaImF
fyzika	fyzika	k1gFnSc1
<g/>
,	,	kIx,
především	především	k6eAd1
fyzikální	fyzikální	k2eAgFnPc4d1
symetrie	symetrie	k1gFnPc4
a	a	k8xC
elektroslabé	elektroslabý	k2eAgFnPc4d1
interakce	interakce	k1gFnPc4
<g/>
,	,	kIx,
proto	proto	k8xC
nastoupil	nastoupit	k5eAaPmAgInS
k	k	k7c3
doktorskému	doktorský	k2eAgNnSc3d1
studiu	studio	k1gNnSc3
u	u	k7c2
Davida	David	k1gMnSc2
Grosse	Gross	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
získal	získat	k5eAaPmAgInS
z	z	k7c2
fyziky	fyzika	k1gFnSc2
doktorský	doktorský	k2eAgInSc4d1
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Princetonu	Princeton	k1gInSc6
poté	poté	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
vyučoval	vyučovat	k5eAaImAgInS
a	a	k8xC
získal	získat	k5eAaPmAgInS
zde	zde	k6eAd1
i	i	k9
titul	titul	k1gInSc4
profesora	profesor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
působil	působit	k5eAaImAgMnS
na	na	k7c6
Kalifornské	kalifornský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c4
Santa	Sant	k1gMnSc4
Barbaře	Barbara	k1gFnSc6
<g/>
,	,	kIx,
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
a	a	k8xC
na	na	k7c6
Institutu	institut	k1gInSc6
pro	pro	k7c4
pokročilá	pokročilý	k2eAgNnPc4d1
studia	studio	k1gNnPc4
v	v	k7c6
Princetonu	Princeton	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
zastává	zastávat	k5eAaImIp3nS
funkci	funkce	k1gFnSc4
profesora	profesor	k1gMnSc2
na	na	k7c6
Massachusettském	massachusettský	k2eAgInSc6d1
technologickém	technologický	k2eAgInSc6d1
institutu	institut	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
získal	získat	k5eAaPmAgInS
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
postgraduální	postgraduální	k2eAgMnSc1d1
vedoucí	vedoucí	k2eAgMnSc1d1
David	David	k1gMnSc1
Gross	Gross	k1gMnSc1
a	a	k8xC
také	také	k9
David	David	k1gMnSc1
Politzer	Politzer	k1gMnSc1
Nobelovu	Nobelův	k2eAgFnSc4d1
cena	cena	k1gFnSc1
za	za	k7c4
fyziku	fyzika	k1gFnSc4
za	za	k7c4
31	#num#	k4
let	léto	k1gNnPc2
starou	starý	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
objevili	objevit	k5eAaPmAgMnP
asymptotickou	asymptotický	k2eAgFnSc4d1
volnost	volnost	k1gFnSc4
v	v	k7c6
silné	silný	k2eAgFnSc6d1
interakci	interakce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
kdy	kdy	k6eAd1
k	k	k7c3
sobě	se	k3xPyFc3
budeme	být	k5eAaImBp1nP
přibližovat	přibližovat	k5eAaImF
dvě	dva	k4xCgFnPc4
barevně	barevně	k6eAd1
nabité	nabitý	k2eAgFnPc1d1
částice	částice	k1gFnPc1
(	(	kIx(
<g/>
barva	barva	k1gFnSc1
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
náboje	náboj	k1gInSc2
v	v	k7c6
silné	silný	k2eAgFnSc6d1
interakci	interakce	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velikost	velikost	k1gFnSc1
interakce	interakce	k1gFnSc2
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
bude	být	k5eAaImBp3nS
slábnout	slábnout	k5eAaImF
až	až	k9
k	k	k7c3
nule	nula	k1gFnSc3
pro	pro	k7c4
velmi	velmi	k6eAd1
malé	malý	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
mezi	mezi	k7c7
částicemi	částice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
budeme	být	k5eAaImBp1nP
<g/>
-li	-li	k?
od	od	k7c2
sebe	sebe	k3xPyFc4
částice	částice	k1gFnPc4
vzdalovat	vzdalovat	k5eAaImF
<g/>
,	,	kIx,
intenzita	intenzita	k1gFnSc1
silné	silný	k2eAgFnSc2d1
interakce	interakce	k1gFnSc2
bude	být	k5eAaImBp3nS
rychle	rychle	k6eAd1
narůstat	narůstat	k5eAaImF
a	a	k8xC
znemožní	znemožnit	k5eAaPmIp3nS
nám	my	k3xPp1nPc3
dvě	dva	k4xCgFnPc4
barevně	barevně	k6eAd1
nabité	nabitý	k2eAgFnPc1d1
částice	částice	k1gFnPc1
(	(	kIx(
<g/>
například	například	k6eAd1
kvarky	kvark	k1gInPc1
<g/>
)	)	kIx)
pozorovat	pozorovat	k5eAaImF
odděleně	odděleně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokusíme	pokusit	k5eAaPmIp1nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
částici	částice	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
z	z	k7c2
kvarků	kvark	k1gInPc2
rozdělit	rozdělit	k5eAaPmF
<g/>
,	,	kIx,
musíme	muset	k5eAaImIp1nP
vložit	vložit	k5eAaPmF
tak	tak	k6eAd1
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
vzniknou	vzniknout	k5eAaPmIp3nP
dvě	dva	k4xCgFnPc1
nové	nový	k2eAgFnPc1d1
částice	částice	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
budou	být	k5eAaImBp3nP
kvarky	kvark	k1gInPc1
opět	opět	k5eAaPmF
pevně	pevně	k6eAd1
vázány	vázat	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
výsledek	výsledek	k1gInSc4
byl	být	k5eAaImAgInS
nezbytný	nezbytný	k2eAgInSc1d1,k2eNgInSc1d1
pro	pro	k7c4
formulaci	formulace	k1gFnSc4
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
silné	silný	k2eAgFnSc2d1
interakce	interakce	k1gFnSc2
nazvanou	nazvaný	k2eAgFnSc4d1
kvantová	kvantový	k2eAgFnSc1d1
chromodynamika	chromodynamika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilczekovi	Wilczeek	k1gMnSc3
bylo	být	k5eAaImAgNnS
v	v	k7c6
době	doba	k1gFnSc6
objevu	objev	k1gInSc6
pouhých	pouhý	k2eAgNnPc2d1
21	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
další	další	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
se	se	k3xPyFc4
týkal	týkat	k5eAaImAgInS
zejména	zejména	k9
hypotetických	hypotetický	k2eAgFnPc2d1
elementárních	elementární	k2eAgFnPc2d1
částic	částice	k1gFnPc2
zvaných	zvaný	k2eAgFnPc2d1
axiony	axiona	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
skládat	skládat	k5eAaImF
temná	temný	k2eAgFnSc1d1
hmota	hmota	k1gFnSc1
<g/>
,	,	kIx,
kvazičástic	kvazičástice	k1gFnPc2
anyonů	anyon	k1gInPc2
a	a	k8xC
kvantové	kvantový	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
pole	pole	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílel	podílet	k5eAaImAgMnS
se	se	k3xPyFc4
však	však	k9
i	i	k9
na	na	k7c6
pracích	práce	k1gFnPc6
z	z	k7c2
oblasti	oblast	k1gFnSc2
astrofyziky	astrofyzika	k1gFnSc2
a	a	k8xC
částicové	částicový	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
souvislostmi	souvislost	k1gFnPc7
mezi	mezi	k7c7
částicovou	částicový	k2eAgFnSc7d1
fyzikou	fyzika	k1gFnSc7
a	a	k8xC
kosmologií	kosmologie	k1gFnSc7
nebo	nebo	k8xC
kvantovou	kvantový	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
černých	černý	k2eAgFnPc2d1
děr	děra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
získal	získat	k5eAaPmAgMnS
i	i	k9
další	další	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
UNESCO	UNESCO	kA
Dirac	Dirac	k1gFnSc1
Medal	Medal	k1gInSc1
<g/>
,	,	kIx,
Sakurai	Sakurai	k1gNnSc1
Prize	Prize	k1gFnSc2
nebo	nebo	k8xC
Lorentzovu	Lorentzův	k2eAgFnSc4d1
medaile	medaile	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
členem	člen	k1gMnSc7
Nizozemské	nizozemský	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
umění	umění	k1gNnSc2
a	a	k8xC
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
navštívil	navštívit	k5eAaPmAgInS
Česko	Česko	k1gNnSc4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
zde	zde	k6eAd1
Pamětní	pamětní	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
Matematicko-fyzikální	matematicko-fyzikální	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
UK	UK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
2013	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
čestný	čestný	k2eAgInSc1d1
doktorát	doktorát	k1gInSc1
na	na	k7c6
Uppsalské	uppsalský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Profesor	profesor	k1gMnSc1
Wilczek	Wilczka	k1gFnPc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgMnPc2d3
žijících	žijící	k2eAgMnPc2d1
teoretických	teoretický	k2eAgMnPc2d1
fyziků	fyzik	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ovšem	ovšem	k9
též	též	k9
popularizátorem	popularizátor	k1gMnSc7
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píše	psát	k5eAaImIp3nS
do	do	k7c2
časopisů	časopis	k1gInPc2
Physics	Physics	k1gInSc4
Today	Todaa	k1gFnSc2
a	a	k8xC
Nature	Natur	k1gMnSc5
<g/>
,	,	kIx,
přednáší	přednášet	k5eAaImIp3nS
pro	pro	k7c4
veřejnost	veřejnost	k1gFnSc4
a	a	k8xC
píše	psát	k5eAaImIp3nS
knihy	kniha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rovněž	rovněž	k9
členem	člen	k1gMnSc7
Society	societa	k1gFnSc2
for	forum	k1gNnPc2
Science	Science	k1gFnSc2
&	&	k?
the	the	k?
Public	publicum	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
o	o	k7c4
šíření	šíření	k1gNnSc4
nejnovějších	nový	k2eAgInPc2d3
objevů	objev	k1gInPc2
mezi	mezi	k7c4
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Překlad	překlad	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
publikace	publikace	k1gFnSc2
The	The	k1gMnSc2
Lightness	Lightnessa	k1gFnPc2
of	of	k?
Being	Being	k1gMnSc1
vyšel	vyjít	k5eAaPmAgMnS
pod	pod	k7c7
názvem	název	k1gInSc7
Lehkost	lehkost	k1gFnSc4
bytí	bytí	k1gNnSc1
i	i	k9
v	v	k7c6
českém	český	k2eAgInSc6d1
překladu	překlad	k1gInSc6
od	od	k7c2
Nakladatelství	nakladatelství	k1gNnSc2
Paseka	paseka	k1gFnSc1
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Argo	Argo	k1gNnSc1
a	a	k8xC
Nakladatelství	nakladatelství	k1gNnSc1
Dokořán	dokořán	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS
se	se	k3xPyFc4
také	také	k9
v	v	k7c6
televizní	televizní	k2eAgFnSc6d1
show	show	k1gFnSc6
iluzionistů	iluzionista	k1gMnPc2
Penne	Penn	k1gInSc5
Jilletteho	Jillette	k1gMnSc2
a	a	k8xC
Raymonda	Raymond	k1gMnSc2
Tellera	Teller	k1gMnSc2
<g/>
,	,	kIx,
známých	známý	k2eAgMnPc2d1
ateistů	ateista	k1gMnPc2
a	a	k8xC
skeptiků	skeptik	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
jako	jako	k8xS,k8xC
nejchytřejší	chytrý	k2eAgMnSc1d3
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
tuto	tento	k3xDgFnSc4
show	show	k1gFnSc4
navštívil	navštívit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Frank	Frank	k1gMnSc1
Wilczek	Wilczka	k1gFnPc2
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgMnSc1d1
s	s	k7c7
americkou	americký	k2eAgFnSc7d1
bloggerkou	bloggerka	k1gFnSc7
a	a	k8xC
novinářkou	novinářka	k1gFnSc7
Betsy	Betsa	k1gFnSc2
Devine	Devin	k1gMnSc5
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
spolu	spolu	k6eAd1
dvě	dva	k4xCgFnPc4
dcery	dcera	k1gFnPc4
Amity	Amita	k1gFnSc2
a	a	k8xC
Miru	mir	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vybrané	vybraný	k2eAgFnPc1d1
publikace	publikace	k1gFnPc1
</s>
<s>
Frank	frank	k1gInSc1
Wilczek	Wilczek	k1gInSc1
na	na	k7c6
Harvardu	Harvard	k1gInSc6
</s>
<s>
D.	D.	kA
J.	J.	kA
Gross	Gross	k1gMnSc1
and	and	k?
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Asymptotically	Asymptoticalla	k1gFnPc1
Free	Fre	k1gFnSc2
Gauge	Gauge	k1gFnSc1
Theories	Theories	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
D8	D8	k1gFnSc1
3633	#num#	k4
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
D.	D.	kA
J.	J.	kA
Gross	Gross	k1gMnSc1
and	and	k?
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Ultraviolet	Ultraviolet	k1gInSc1
Behavior	Behavior	k1gMnSc1
of	of	k?
non-Abelian	non-Abelian	k1gMnSc1
Gauge	Gaug	k1gFnSc2
Theories	Theories	k1gMnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
1343	#num#	k4
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Quantum	Quantum	k1gNnSc1
Mechanics	Mechanicsa	k1gFnPc2
Of	Of	k1gFnSc2
Fractional	Fractional	k1gMnPc2
Spin	spin	k1gInSc1
Particles	Particlesa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
49	#num#	k4
<g/>
,	,	kIx,
957	#num#	k4
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Problem	Problo	k1gNnSc7
Of	Of	k1gMnSc1
Strong	Strong	k1gMnSc1
P	P	kA
And	Anda	k1gFnPc2
T	T	kA
Invariance	invariance	k1gFnSc1
In	In	k1gFnSc1
The	The	k1gFnSc1
Presence	presence	k1gFnSc1
Of	Of	k1gFnSc1
Instantons	Instantons	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
40	#num#	k4
<g/>
,	,	kIx,
279	#num#	k4
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
T.	T.	kA
Schafer	Schafer	k1gMnSc1
and	and	k?
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Continuity	Continuita	k1gFnPc1
of	of	k?
quark	quark	k1gInSc1
and	and	k?
hadron	hadron	k1gInSc1
matter	matter	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
82	#num#	k4
<g/>
,	,	kIx,
3956	#num#	k4
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
hep-ph	hep-ph	k1gInSc1
<g/>
/	/	kIx~
<g/>
9811473	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Quantum	Quantum	k1gNnSc4
field	fieldo	k1gNnPc2
theory	theora	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Rev	Rev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mod.	Mod.	k1gFnSc1
Phys	Physa	k1gFnPc2
<g/>
.	.	kIx.
71	#num#	k4
<g/>
,	,	kIx,
S85	S85	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
hep-th	hep-th	k1gInSc1
<g/>
/	/	kIx~
<g/>
9803075	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
F.	F.	kA
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Riemann-Einstein	Riemann-Einstein	k1gInSc1
structure	structur	k1gMnSc5
from	from	k1gInSc1
volume	volum	k1gInSc5
and	and	k?
gauge	gauge	k1gFnSc1
symmetry	symmetr	k1gInPc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lett	Lett	k1gInSc1
<g/>
.	.	kIx.
80	#num#	k4
<g/>
,	,	kIx,
4851	#num#	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arXiv	arXiv	k6eAd1
<g/>
:	:	kIx,
<g/>
hep-th	hep-th	k1gInSc1
<g/>
/	/	kIx~
<g/>
9801184	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Frank	frank	k1gInSc1
Wilczek	Wilczek	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Official	Official	k1gMnSc1
Nobel	Nobel	k1gMnSc1
site	sitat	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KULHÁNEK	Kulhánek	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilczek	Wilczek	k1gInSc1
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
aldebaran	aldebaran	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Fractional	Fractionat	k5eAaPmAgInS,k5eAaImAgInS
Statistics	Statistics	k1gInSc1
and	and	k?
Anyon	Anyon	k1gInSc1
Superconductivity	Superconductivita	k1gFnSc2
<g/>
,	,	kIx,
December	December	k1gInSc4
1990	#num#	k4
</s>
<s>
Geometric	Geometric	k1gMnSc1
Phases	Phases	k1gMnSc1
in	in	k?
Physics	Physics	k1gInSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
1988	#num#	k4
</s>
<s>
Longing	Longing	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Harmonies	Harmonies	k1gMnSc1
<g/>
:	:	kIx,
Themes	Themes	k1gMnSc1
and	and	k?
Variations	Variations	k1gInSc1
in	in	k?
Modern	Modern	k1gInSc1
Physics	Physics	k1gInSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
1989	#num#	k4
(	(	kIx(
<g/>
with	with	k1gInSc1
Betsy	Betsa	k1gFnSc2
Devine	Devin	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Fantastic	Fantastice	k1gFnPc2
Realities	Realitiesa	k1gFnPc2
<g/>
:	:	kIx,
49	#num#	k4
Mind	Mindo	k1gNnPc2
Journeys	Journeysa	k1gFnPc2
And	Anda	k1gFnPc2
a	a	k8xC
Trip	Trip	k1gInSc4
to	ten	k3xDgNnSc1
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
March	March	k1gInSc1
2006	#num#	k4
</s>
<s>
La	la	k1gNnSc1
musica	music	k1gInSc2
del	del	k?
vuoto	vuoto	k1gNnSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
Roma	Rom	k1gMnSc4
<g/>
,	,	kIx,
Di	Di	k1gMnSc4
Renzo	Renza	k1gFnSc5
Editore	editor	k1gMnSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Frank	Frank	k1gMnSc1
Wilczek	Wilczka	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
homepage	homepage	k1gFnSc1
of	of	k?
Frank	frank	k1gInSc1
Wilczek	Wilczek	k1gInSc1
</s>
<s>
Longer	Longer	k1gMnSc1
biography	biographa	k1gFnSc2
at	at	k?
Lifeboat	Lifeboat	k2eAgInSc4d1
Foundation	Foundation	k1gInSc4
website	websit	k1gInSc5
</s>
<s>
Papers	Papers	k6eAd1
in	in	k?
ArXiv	ArXiva	k1gFnPc2
</s>
<s>
The	The	k?
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Numerical	Numerical	k1gFnSc7
Recipe	recipe	k1gNnSc2
</s>
<s>
Scientific	Scientific	k1gMnSc1
articles	articles	k1gMnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
by	by	kYmCp3nS
Wilczek	Wilczek	k1gInSc1
in	in	k?
the	the	k?
SLAC	SLAC	kA
database	database	k6eAd1
</s>
<s>
Wilczek	Wilczek	k1gInSc1
on	on	k3xPp3gMnSc1
anyons	anyons	k1gInSc4
and	and	k?
superconductivity	superconductivita	k1gFnSc2
</s>
<s>
Blog	Blog	k1gInSc1
of	of	k?
the	the	k?
Wilczek	Wilczek	k1gInSc4
family	famila	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Nobel	Nobel	k1gMnSc1
adventures	adventures	k1gMnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
fyziku	fyzika	k1gFnSc4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2025	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Eric	Eric	k1gFnSc1
Cornell	Cornell	k1gMnSc1
/	/	kIx~
Wolfgang	Wolfgang	k1gMnSc1
Ketterle	Ketterl	k1gMnSc2
/	/	kIx~
Carl	Carl	k1gMnSc1
Wieman	Wieman	k1gMnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Raymond	Raymond	k1gMnSc1
Davis	Davis	k1gFnSc2
mladší	mladý	k2eAgMnSc1d2
/	/	kIx~
Masatoši	Masatoš	k1gMnPc1
Košiba	Košiba	k1gMnSc1
/	/	kIx~
Riccardo	Riccardo	k1gNnSc1
Giacconi	Giaccoň	k1gFnSc6
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexej	Alexej	k1gMnSc1
Abrikosov	Abrikosov	k1gInSc1
/	/	kIx~
Vitalij	Vitalij	k1gMnSc1
Ginzburg	Ginzburg	k1gMnSc1
/	/	kIx~
Anthony	Anthon	k1gInPc1
Leggett	Leggetta	k1gFnPc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
David	David	k1gMnSc1
Gross	Gross	k1gMnSc1
/	/	kIx~
David	David	k1gMnSc1
Politzer	Politzer	k1gMnSc1
/	/	kIx~
Frank	Frank	k1gMnSc1
Wilczek	Wilczka	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roy	Roy	k1gMnSc1
Glauber	Glauber	k1gMnSc1
/	/	kIx~
John	John	k1gMnSc1
Hall	Hall	k1gMnSc1
/	/	kIx~
Theodor	Theodor	k1gMnSc1
Hänsch	Hänsch	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
C.	C.	kA
Mather	Mathra	k1gFnPc2
/	/	kIx~
George	Georg	k1gMnSc2
Smoot	Smoot	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Albert	Albert	k1gMnSc1
Fert	Fert	k1gMnSc1
/	/	kIx~
Peter	Peter	k1gMnSc1
Grünberg	Grünberg	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jóičiró	Jóičiró	k1gMnSc4
Nambu	Namba	k1gMnSc4
/	/	kIx~
Makoto	Makota	k1gFnSc5
Kobajaši	Kobajaše	k1gFnSc4
/	/	kIx~
Tošihide	Tošihid	k1gMnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
Masukawa	Masukawum	k1gNnPc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charles	Charles	k1gMnSc1
Kuen	Kuen	k1gMnSc1
Kao	Kao	k1gMnSc1
/	/	kIx~
Willard	Willard	k1gInSc1
Sterling	sterling	k1gInSc1
Boyle	Boyl	k1gMnSc2
/	/	kIx~
George	Georg	k1gMnSc2
E.	E.	kA
Smith	Smith	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Andre	Andr	k1gInSc5
Geim	Geim	k1gMnSc1
/	/	kIx~
Konstantin	Konstantin	k1gMnSc1
Novoselov	Novoselov	k1gInSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Saul	Saul	k1gMnSc1
Perlmutter	Perlmutter	k1gMnSc1
/	/	kIx~
Adam	Adam	k1gMnSc1
Riess	Riessa	k1gFnPc2
/	/	kIx~
Brian	Brian	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Serge	Serge	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Haroche	Haroche	k1gNnSc1
/	/	kIx~
David	David	k1gMnSc1
J.	J.	kA
Wineland	Wineland	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
François	François	k1gInSc1
Englert	Englert	k1gMnSc1
/	/	kIx~
Peter	Peter	k1gMnSc1
Higgs	Higgsa	k1gFnPc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Isamu	Isam	k1gMnSc3
Akasaki	Akasak	k1gMnSc3
/	/	kIx~
Hiroši	Hiroš	k1gMnSc3
Amano	Amano	k6eAd1
/	/	kIx~
Shuji	Shuje	k1gFnSc4
Nakamura	Nakamur	k1gMnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Takaaki	Takaaki	k1gNnSc1
Kadžita	Kadžita	k1gMnSc1
/	/	kIx~
Arthur	Arthur	k1gMnSc1
B.	B.	kA
McDonald	McDonald	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
David	David	k1gMnSc1
J.	J.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Thouless	Thouless	k1gInSc1
/	/	kIx~
Duncan	Duncan	k1gInSc1
Haldane	Haldan	k1gMnSc5
/	/	kIx~
Michael	Michael	k1gMnSc1
Kosterlitz	Kosterlitz	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rainer	Rainer	k1gMnSc1
Weiss	Weiss	k1gMnSc1
/	/	kIx~
Barry	Barra	k1gMnSc2
Barish	Barish	k1gMnSc1
/	/	kIx~
Kip	Kip	k1gMnSc1
Thorne	Thorn	k1gInSc5
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arthur	Arthur	k1gMnSc1
Ashkin	Ashkin	k1gMnSc1
/	/	kIx~
Gérard	Gérard	k1gMnSc1
Mourou	Mourou	k?
/	/	kIx~
Donna	donna	k1gFnSc1
Stricklandová	Stricklandový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
James	James	k1gMnSc1
Peebles	Peebles	k1gMnSc1
/	/	kIx~
Michel	Michel	k1gMnSc1
Mayor	Mayor	k1gMnSc1
/	/	kIx~
Didier	Didier	k1gMnSc1
Queloz	Queloz	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roger	Roger	k1gInSc1
Penrose	Penrosa	k1gFnSc3
/	/	kIx~
Reinhard	Reinhard	k1gMnSc1
Genzel	Genzel	k1gMnSc1
/	/	kIx~
Andrea	Andrea	k1gFnSc1
Ghezová	Ghezová	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
1901	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Nositelé	nositel	k1gMnPc1
Lorentzovy	Lorentzův	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
</s>
<s>
Max	Max	k1gMnSc1
Planck	Planck	k1gMnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Wolfgang	Wolfgang	k1gMnSc1
Pauli	Paule	k1gFnSc4
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Peter	Peter	k1gMnSc1
Debye	Deby	k1gMnSc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arnold	Arnold	k1gMnSc1
Sommerfeld	Sommerfeld	k1gMnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hendrik	Hendrik	k1gMnSc1
Kramers	Kramers	k1gInSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fritz	Fritz	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
London	London	k1gMnSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lars	Lars	k1gInSc1
Onsager	Onsager	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudolf	Rudolf	k1gMnSc1
Peierls	Peierls	k1gInSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Freeman	Freeman	k1gMnSc1
Dyson	Dyson	k1gMnSc1
(	(	kIx(
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
•	•	k?
George	Georg	k1gMnSc4
Uhlenbeck	Uhlenbeck	k1gMnSc1
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Hasbrouck	Hasbrouck	k1gMnSc1
van	vana	k1gFnPc2
Vleck	Vleck	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nicolaas	Nicolaas	k1gInSc1
Bloembergen	Bloembergen	k1gInSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Anatole	Anatola	k1gFnSc3
Abragam	Abragam	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gerardus	Gerardus	k1gInSc1
'	'	kIx"
<g/>
t	t	k?
Hooft	Hooft	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pierre-Gilles	Pierre-Gilles	k1gMnSc1
de	de	k?
Gennes	Gennes	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Poljakov	Poljakov	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carl	Carl	k1gMnSc1
Wieman	Wieman	k1gMnSc1
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
Allin	Allin	k1gMnSc1
Cornell	Cornell	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Frank	frank	k1gInSc1
Wilczek	Wilczek	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Leo	Leo	k1gMnSc1
Kadanoff	Kadanoff	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Edward	Edward	k1gMnSc1
Witten	Witten	k2eAgMnSc1d1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Berry	Berra	k1gMnSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Juan	Juan	k1gMnSc1
Maldacena	Maldacen	k2eAgFnSc1d1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20051114020	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
131653369	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0920	#num#	k4
2854	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
86010219	#num#	k4
|	|	kIx~
ORCID	ORCID	kA
<g/>
:	:	kIx,
0000-0002-6489-6155	0000-0002-6489-6155	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
84241989	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
86010219	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fyzika	fyzika	k1gFnSc1
</s>
