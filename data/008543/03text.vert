<p>
<s>
Keratinocyt	Keratinocyt	k1gInSc1	Keratinocyt
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
pokožková	pokožkový	k2eAgFnSc1d1	pokožková
buňka	buňka	k1gFnSc1	buňka
<g/>
;	;	kIx,	;
představuje	představovat	k5eAaImIp3nS	představovat
až	až	k9	až
95	[number]	k4	95
<g/>
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
množství	množství	k1gNnSc4	množství
bílkoviny	bílkovina	k1gFnSc2	bílkovina
keratinu	keratin	k1gInSc2	keratin
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
také	také	k9	také
melanin	melanin	k1gInSc4	melanin
<g/>
,	,	kIx,	,
barvivo	barvivo	k1gNnSc4	barvivo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
dodávají	dodávat	k5eAaImIp3nP	dodávat
buňky	buňka	k1gFnPc4	buňka
známé	známá	k1gFnPc4	známá
jako	jako	k8xC	jako
melanocyty	melanocyt	k1gInPc4	melanocyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
keratinocyty	keratinocyt	k1gInPc4	keratinocyt
5	[number]	k4	5
rozlišitelných	rozlišitelný	k2eAgFnPc2d1	rozlišitelná
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Nejníže	nízce	k6eAd3	nízce
(	(	kIx(	(
<g/>
ve	v	k7c4	v
stratum	stratum	k1gNnSc4	stratum
basale	basale	k6eAd1	basale
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
keratinocyty	keratinocyt	k1gInPc4	keratinocyt
krychlového	krychlový	k2eAgInSc2d1	krychlový
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
kompaktní	kompaktní	k2eAgFnSc4d1	kompaktní
pevnou	pevný	k2eAgFnSc4d1	pevná
tkáň	tkáň	k1gFnSc4	tkáň
pospojovanou	pospojovaný	k2eAgFnSc7d1	pospojovaná
buněčnými	buněčný	k2eAgInPc7d1	buněčný
spoji	spoj	k1gInPc7	spoj
a	a	k8xC	a
vyztuženou	vyztužený	k2eAgFnSc4d1	vyztužená
vnitrobuněčnými	vnitrobuněčný	k2eAgNnPc7d1	vnitrobuněčné
vlákny	vlákno	k1gNnPc7	vlákno
keratinu	keratin	k1gInSc2	keratin
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
je	být	k5eAaImIp3nS	být
stratum	stratum	k1gNnSc4	stratum
spinosum	spinosum	k1gInSc1	spinosum
s	s	k7c7	s
rovněž	rovněž	k6eAd1	rovněž
kubickými	kubický	k2eAgFnPc7d1	kubická
buňkami	buňka	k1gFnPc7	buňka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
výše	vysoce	k6eAd2	vysoce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stratum	stratum	k1gNnSc1	stratum
granulosum	granulosum	k1gInSc4	granulosum
tvořené	tvořený	k2eAgFnSc2d1	tvořená
buňkami	buňka	k1gFnPc7	buňka
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
váčků	váček	k1gInPc2	váček
obsahujících	obsahující	k2eAgInPc2d1	obsahující
například	například	k6eAd1	například
lipidy	lipid	k1gInPc1	lipid
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
stratum	stratum	k1gNnSc1	stratum
lucidum	lucidum	k1gInSc1	lucidum
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
buňky	buňka	k1gFnPc1	buňka
začínají	začínat	k5eAaImIp3nP	začínat
degenerovat	degenerovat	k5eAaBmF	degenerovat
<g/>
,	,	kIx,	,
zplošťovat	zplošťovat	k5eAaImF	zplošťovat
se	se	k3xPyFc4	se
a	a	k8xC	a
mizí	mizet	k5eAaImIp3nP	mizet
některé	některý	k3yIgFnPc1	některý
buněčné	buněčný	k2eAgFnPc1d1	buněčná
organely	organela	k1gFnPc1	organela
<g/>
.	.	kIx.	.
</s>
<s>
Nejsvrchnější	svrchní	k2eAgFnSc7d3	nejsvrchnější
vrstvou	vrstva	k1gFnSc7	vrstva
je	být	k5eAaImIp3nS	být
stratum	stratum	k1gNnSc1	stratum
corneum	corneum	k1gInSc1	corneum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
buňky	buňka	k1gFnPc1	buňka
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
<g/>
,	,	kIx,	,
vyplněné	vyplněný	k2eAgFnPc1d1	vyplněná
keratinem	keratin	k1gInSc7	keratin
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
nejsvrchnější	svrchní	k2eAgFnPc1d3	nejsvrchnější
se	se	k3xPyFc4	se
odlupují	odlupovat	k5eAaImIp3nP	odlupovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Keratinocyt	Keratinocyt	k1gInSc1	Keratinocyt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
