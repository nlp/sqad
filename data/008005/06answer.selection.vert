<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
třináct	třináct	k4xCc4	třináct
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc4	sedm
červených	červený	k2eAgFnPc2d1	červená
a	a	k8xC	a
šest	šest	k4xCc4	šest
bílých	bílý	k2eAgFnPc2d1	bílá
<g/>
)	)	kIx)	)
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
kanton	kanton	k1gInSc1	kanton
(	(	kIx(	(
<g/>
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
sedmi	sedm	k4xCc2	sedm
pruhů	pruh	k1gInPc2	pruh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
padesát	padesát	k4xCc1	padesát
bílých	bílý	k2eAgFnPc2d1	bílá
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
devíti	devět	k4xCc6	devět
řadách	řada	k1gFnPc6	řada
(	(	kIx(	(
<g/>
střídavě	střídavě	k6eAd1	střídavě
po	po	k7c6	po
pěti	pět	k4xCc6	pět
a	a	k8xC	a
šesti	šest	k4xCc6	šest
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
