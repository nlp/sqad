<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
státních	státní	k2eAgInPc2d1	státní
symbolů	symbol	k1gInPc2	symbol
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
třináct	třináct	k4xCc4	třináct
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc4	sedm
červených	červený	k2eAgFnPc2d1	červená
a	a	k8xC	a
šest	šest	k4xCc4	šest
bílých	bílý	k2eAgFnPc2d1	bílá
<g/>
)	)	kIx)	)
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
kanton	kanton	k1gInSc1	kanton
(	(	kIx(	(
<g/>
o	o	k7c6	o
šířce	šířka	k1gFnSc6	šířka
sedmi	sedm	k4xCc2	sedm
pruhů	pruh	k1gInPc2	pruh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
padesát	padesát	k4xCc1	padesát
bílých	bílý	k2eAgFnPc2d1	bílá
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
devíti	devět	k4xCc6	devět
řadách	řada	k1gFnPc6	řada
(	(	kIx(	(
<g/>
střídavě	střídavě	k6eAd1	střídavě
po	po	k7c6	po
pěti	pět	k4xCc6	pět
a	a	k8xC	a
šesti	šest	k4xCc6	šest
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
představují	představovat	k5eAaImIp3nP	představovat
třináct	třináct	k4xCc4	třináct
původních	původní	k2eAgMnPc2d1	původní
<g/>
,	,	kIx,	,
zakládajících	zakládající	k2eAgMnPc2d1	zakládající
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnSc2	hvězda
současný	současný	k2eAgInSc1d1	současný
počet	počet	k1gInSc1	počet
států	stát	k1gInPc2	stát
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
varianta	varianta	k1gFnSc1	varianta
vlajky	vlajka	k1gFnSc2	vlajka
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
státu	stát	k1gInSc2	stát
Havaj	Havaj	k1gFnSc1	Havaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
již	již	k9	již
o	o	k7c4	o
28	[number]	k4	28
<g/>
.	.	kIx.	.
verzi	verze	k1gFnSc3	verze
vlajky	vlajka	k1gFnSc2	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
na	na	k7c6	na
první	první	k4xOgFnSc6	první
vlajce	vlajka	k1gFnSc6	vlajka
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
kantonu	kanton	k1gInSc6	kanton
ještě	ještě	k6eAd1	ještě
britský	britský	k2eAgInSc1d1	britský
Union	union	k1gInSc1	union
Jack	Jacka	k1gFnPc2	Jacka
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
různý	různý	k2eAgInSc1d1	různý
počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
americká	americký	k2eAgFnSc1d1	americká
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
třinácti	třináct	k4xCc7	třináct
pruhy	pruh	k1gInPc7	pruh
(	(	kIx(	(
<g/>
Grand	grand	k1gMnSc1	grand
Union	union	k1gInSc4	union
Flag	flag	k1gInSc4	flag
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
národní	národní	k2eAgFnSc7d1	národní
vlajkou	vlajka	k1gFnSc7	vlajka
USA	USA	kA	USA
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
kantonu	kanton	k1gInSc6	kanton
ještě	ještě	k9	ještě
britský	britský	k2eAgInSc4d1	britský
Union	union	k1gInSc4	union
Jack	Jack	k1gMnSc1	Jack
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
povstalci	povstalec	k1gMnPc1	povstalec
chtěli	chtít	k5eAaImAgMnP	chtít
zůstat	zůstat	k5eAaPmF	zůstat
loajální	loajální	k2eAgMnPc4d1	loajální
k	k	k7c3	k
britskému	britský	k2eAgMnSc3d1	britský
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1777	[number]	k4	1777
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Kongres	kongres	k1gInSc1	kongres
USA	USA	kA	USA
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
budou	být	k5eAaImBp3nP	být
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
pruhy	pruh	k1gInPc4	pruh
budou	být	k5eAaImBp3nP	být
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
počet	počet	k1gInSc4	počet
členů	člen	k1gMnPc2	člen
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
Vermontu	Vermont	k1gInSc2	Vermont
a	a	k8xC	a
Kentucky	Kentucka	k1gFnSc2	Kentucka
do	do	k7c2	do
federace	federace	k1gFnSc2	federace
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgMnS	zvýšit
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1795	[number]	k4	1795
počet	počet	k1gInSc4	počet
pruhů	pruh	k1gInPc2	pruh
na	na	k7c4	na
patnáct	patnáct	k4xCc4	patnáct
<g/>
.	.	kIx.	.
</s>
<s>
Hrozilo	hrozit	k5eAaImAgNnS	hrozit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
pruhů	pruh	k1gInPc2	pruh
bude	být	k5eAaImBp3nS	být
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
špatně	špatně	k6eAd1	špatně
rozlišitelná	rozlišitelný	k2eAgFnSc1d1	rozlišitelná
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
nových	nový	k2eAgFnPc2d1	nová
vlajek	vlajka	k1gFnPc2	vlajka
značně	značně	k6eAd1	značně
nákladná	nákladný	k2eAgNnPc1d1	nákladné
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Kongres	kongres	k1gInSc1	kongres
USA	USA	kA	USA
přijal	přijmout	k5eAaPmAgInS	přijmout
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
zásadu	zásada	k1gFnSc4	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
pruhů	pruh	k1gInPc2	pruh
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
na	na	k7c4	na
třináct	třináct	k4xCc4	třináct
<g/>
,	,	kIx,	,
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
pouze	pouze	k6eAd1	pouze
počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vstup	vstup	k1gInSc1	vstup
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
k	k	k7c3	k
jinému	jiný	k2eAgNnSc3d1	jiné
datu	datum	k1gNnSc3	datum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
vyložil	vyložit	k5eAaPmAgMnS	vyložit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
prvky	prvek	k1gInPc4	prvek
vlajky	vlajka	k1gFnSc2	vlajka
sám	sám	k3xTgInSc1	sám
George	George	k1gInSc1	George
Washington	Washington	k1gInSc4	Washington
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
sebrané	sebraný	k2eAgFnPc1d1	sebraná
z	z	k7c2	z
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
bílé	bílý	k2eAgInPc1d1	bílý
pruhy	pruh	k1gInPc1	pruh
představují	představovat	k5eAaImIp3nP	představovat
odloučení	odloučení	k1gNnSc4	odloučení
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
vlajku	vlajka	k1gFnSc4	vlajka
údajně	údajně	k6eAd1	údajně
ušila	ušít	k5eAaPmAgFnS	ušít
švadlena	švadlena	k1gFnSc1	švadlena
Betsy	Betsa	k1gFnSc2	Betsa
Rossová	Rossová	k1gFnSc1	Rossová
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
USA	USA	kA	USA
se	se	k3xPyFc4	se
vztyčuje	vztyčovat	k5eAaImIp3nS	vztyčovat
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
amerických	americký	k2eAgNnPc6d1	americké
závislých	závislý	k2eAgNnPc6d1	závislé
územích	území	k1gNnPc6	území
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vlajkou	vlajka	k1gFnSc7	vlajka
OSN	OSN	kA	OSN
a	a	k8xC	a
vlajkou	vlajka	k1gFnSc7	vlajka
toho	ten	k3xDgNnSc2	ten
daného	daný	k2eAgNnSc2d1	dané
závislého	závislý	k2eAgNnSc2d1	závislé
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Slib	slib	k1gInSc1	slib
věrnosti	věrnost	k1gFnSc2	věrnost
<g/>
.	.	kIx.	.
</s>
<s>
Slib	slib	k1gInSc1	slib
věrnosti	věrnost	k1gFnSc2	věrnost
vlajce	vlajka	k1gFnSc3	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Pledge	Pledg	k1gFnSc2	Pledg
of	of	k?	of
Allegiance	Allegiance	k1gFnSc2	Allegiance
to	ten	k3xDgNnSc1	ten
the	the	k?	the
flag	flag	k1gInSc1	flag
of	of	k?	of
the	the	k?	the
United	United	k1gMnSc1	United
States	States	k1gMnSc1	States
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přísahou	přísaha	k1gFnSc7	přísaha
loajality	loajalita	k1gFnSc2	loajalita
Američanů	Američan	k1gMnPc2	Američan
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
napsal	napsat	k5eAaBmAgInS	napsat
Francis	Francis	k1gInSc4	Francis
Bellamy	Bellam	k1gInPc1	Bellam
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
:	:	kIx,	:
A	A	kA	A
=	=	kIx~	=
1	[number]	k4	1
(	(	kIx(	(
<g/>
základ	základ	k1gInSc1	základ
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
)	)	kIx)	)
Délka	délka	k1gFnSc1	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
:	:	kIx,	:
B	B	kA	B
=	=	kIx~	=
1,9	[number]	k4	1,9
Výška	výška	k1gFnSc1	výška
kantonu	kanton	k1gInSc2	kanton
<g/>
:	:	kIx,	:
C	C	kA	C
=	=	kIx~	=
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
≈	≈	k?	≈
0,538	[number]	k4	0,538
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc1	sedm
ze	z	k7c2	z
třinácti	třináct	k4xCc2	třináct
pruhů	pruh	k1gInPc2	pruh
<g/>
)	)	kIx)	)
Délka	délka	k1gFnSc1	délka
kantonu	kanton	k1gInSc2	kanton
<g/>
:	:	kIx,	:
D	D	kA	D
=	=	kIx~	=
0,76	[number]	k4	0,76
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
40	[number]	k4	40
%	%	kIx~	%
délky	délka	k1gFnSc2	délka
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
)	)	kIx)	)
E	E	kA	E
=	=	kIx~	=
F	F	kA	F
=	=	kIx~	=
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
≈	≈	k?	≈
0,0538	[number]	k4	0,0538
(	(	kIx(	(
<g/>
desetina	desetina	k1gFnSc1	desetina
výšky	výška	k1gFnSc2	výška
kantonu	kanton	k1gInSc2	kanton
<g/>
)	)	kIx)	)
G	G	kA	G
=	=	kIx~	=
H	H	kA	H
=	=	kIx~	=
D	D	kA	D
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
≈	≈	k?	≈
0,0633	[number]	k4	0,0633
(	(	kIx(	(
<g/>
dvanáctina	dvanáctina	k1gFnSc1	dvanáctina
délky	délka	k1gFnSc2	délka
kantonu	kanton	k1gInSc2	kanton
<g/>
)	)	kIx)	)
Průměr	průměr	k1gInSc1	průměr
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
:	:	kIx,	:
K	K	kA	K
≈	≈	k?	≈
0,0616	[number]	k4	0,0616
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
80,104	[number]	k4	80,104
%	%	kIx~	%
výšky	výška	k1gFnSc2	výška
pruhu	pruh	k1gInSc2	pruh
<g/>
)	)	kIx)	)
Výška	výška	k1gFnSc1	výška
pruhu	pruh	k1gInSc2	pruh
<g/>
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
≈	≈	k?	≈
0,0769	[number]	k4	0,0769
Poměr	poměr	k1gInSc1	poměr
stran	stran	k7c2	stran
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
státní	státní	k2eAgInSc4d1	státní
(	(	kIx(	(
<g/>
federální	federální	k2eAgFnPc1d1	federální
<g/>
)	)	kIx)	)
vlajky	vlajka	k1gFnPc1	vlajka
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
označovány	označován	k2eAgInPc1d1	označován
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
G-spec	Gpec	k1gInSc1	G-spec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pro	pro	k7c4	pro
vládní	vládní	k2eAgInPc4d1	vládní
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnosti	veřejnost	k1gFnPc1	veřejnost
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k8xC	i
(	(	kIx(	(
<g/>
alternativní	alternativní	k2eAgFnPc1d1	alternativní
nebo	nebo	k8xC	nebo
přijatelné	přijatelný	k2eAgFnPc1d1	přijatelná
<g/>
)	)	kIx)	)
varianty	varianta	k1gFnPc1	varianta
vlajky	vlajka	k1gFnPc1	vlajka
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
stran	stran	k7c2	stran
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
s	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
a	a	k8xC	a
pruhy	pruh	k1gInPc7	pruh
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
vlajky	vlajka	k1gFnPc4	vlajka
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
Libérie	Libérie	k1gFnSc2	Libérie
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
Portorika	Portorico	k1gNnSc2	Portorico
<g/>
,	,	kIx,	,
Toga	Togo	k1gNnSc2	Togo
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vlajky	vlajka	k1gFnPc4	vlajka
závislých	závislý	k2eAgNnPc2d1	závislé
území	území	k1gNnPc2	území
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
historické	historický	k2eAgFnPc4d1	historická
vlajky	vlajka	k1gFnPc4	vlajka
<g/>
.	.	kIx.	.
</s>
