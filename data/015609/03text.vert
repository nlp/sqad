<s>
Láhaurská	láhaurský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
</s>
<s>
Láhaurská	láhaurský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Láhaur	Láhaur	k1gInSc1
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
Pákistán	Pákistán	k1gInSc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
31	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
25	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
74	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
35	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šalimarovy	Šalimarův	k2eAgFnPc4d1
zahrady	zahrada	k1gFnPc4
a	a	k8xC
Láhaurská	láhaurský	k2eAgNnPc4d1
pevnostSvětové	pevnostSvětové	k2eAgNnPc4d1
dědictví	dědictví	k1gNnPc4
UNESCOSmluvní	UNESCOSmluvný	k2eAgMnPc1d1
stát	stát	k1gInSc1
</s>
<s>
Pákistán	Pákistán	k1gInSc4
Pákistán	Pákistán	k1gInSc1
Typ	typ	k1gInSc1
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
i	i	k9
<g/>
,	,	kIx,
ii	ii	k?
<g/>
,	,	kIx,
iii	iii	k?
Odkaz	odkaz	k1gInSc1
</s>
<s>
171	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
do	do	k7c2
seznamu	seznam	k1gInSc2
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1981	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc6
<g/>
)	)	kIx)
V	v	k7c6
ohrožení	ohrožení	k1gNnSc6
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
</s>
<s>
Láhaurská	láhaurský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
je	být	k5eAaImIp3nS
citadela	citadela	k1gFnSc1
v	v	k7c6
Láhauru	Láhaur	k1gInSc6
v	v	k7c6
pákistánské	pákistánský	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Paňdžáb	Paňdžáb	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stavba	stavba	k1gFnSc1
má	mít	k5eAaImIp3nS
lichoběžníkový	lichoběžníkový	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
a	a	k8xC
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ploše	plocha	k1gFnSc6
přesahující	přesahující	k2eAgFnSc6d1
20	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátky	počátek	k1gInPc4
stavby	stavba	k1gFnSc2
sice	sice	k8xC
sahají	sahat	k5eAaImIp3nP
až	až	k9
do	do	k7c2
starověku	starověk	k1gInSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
současná	současný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
však	však	k9
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
doby	doba	k1gFnSc2
mughalského	mughalský	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
Akbara	Akbar	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
(	(	kIx(
<g/>
1542	#num#	k4
<g/>
–	–	k?
<g/>
1605	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pevnost	pevnost	k1gFnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
spolu	spolu	k6eAd1
se	s	k7c7
Šalimarovymi	Šalimarovy	k1gFnPc7
zahradami	zahrada	k1gFnPc7
součástí	součást	k1gFnSc7
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Láhaurská	láhaurský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Láhaurská	láhaurský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Lahore	Lahor	k1gMnSc5
Fort	Fort	k?
snapshots	snapshotsit	k5eAaPmRp2nS
</s>
<s>
Panografické	Panografický	k2eAgInPc1d1
obrázky	obrázek	k1gInPc1
pevnosti	pevnost	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
UNESCO	UNESCO	kA
</s>
<s>
Fotografie	fotografia	k1gFnPc1
a	a	k8xC
informace	informace	k1gFnPc1
o	o	k7c6
pevnosti	pevnost	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Pákistánské	pákistánský	k2eAgFnPc1d1
památky	památka	k1gFnPc1
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Mohendžodaro	Mohendžodara	k1gFnSc5
•	•	k?
Takht-i-Bahi	Takht-i-Bahi	k1gNnPc7
a	a	k8xC
Sehri-Bahlol	Sehri-Bahlol	k1gInSc1
•	•	k?
Taxila	Taxila	k1gFnSc1
•	•	k?
Šalimarovy	Šalimarův	k2eAgFnPc4d1
zahrady	zahrada	k1gFnPc4
a	a	k8xC
Láhaurská	láhaurský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
•	•	k?
Thatta	Thatta	k1gFnSc1
•	•	k?
pevnost	pevnost	k1gFnSc1
Rohtas	Rohtas	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
315148146	#num#	k4
</s>
