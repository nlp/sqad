<p>
<s>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
PřF	PřF	k1gFnSc2	PřF
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
biologických	biologický	k2eAgFnPc6d1	biologická
<g/>
,	,	kIx,	,
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
<g/>
,	,	kIx,	,
chemických	chemický	k2eAgInPc2d1	chemický
<g/>
,	,	kIx,	,
matematických	matematický	k2eAgInPc2d1	matematický
<g/>
,	,	kIx,	,
geologických	geologický	k2eAgInPc2d1	geologický
a	a	k8xC	a
geografických	geografický	k2eAgFnPc6d1	geografická
vědách	věda	k1gFnPc6	věda
a	a	k8xC	a
ve	v	k7c6	v
vyučování	vyučování	k1gNnSc6	vyučování
těmto	tento	k3xDgFnPc3	tento
vědám	věda	k1gFnPc3	věda
<g/>
.	.	kIx.	.
</s>
<s>
Výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
zahájila	zahájit	k5eAaPmAgFnS	zahájit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
/	/	kIx~	/
<g/>
1922	[number]	k4	1922
probíhala	probíhat	k5eAaImAgFnS	probíhat
výuka	výuka	k1gFnSc1	výuka
již	již	k6eAd1	již
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
míře	míra	k1gFnSc6	míra
<g/>
.	.	kIx.	.
</s>
<s>
Fakultě	fakulta	k1gFnSc6	fakulta
byly	být	k5eAaImAgFnP	být
dočasně	dočasně	k6eAd1	dočasně
přiděleny	přidělen	k2eAgFnPc1d1	přidělena
budovy	budova	k1gFnPc1	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
chudobince	chudobinec	k1gInSc2	chudobinec
na	na	k7c6	na
Kotlářské	kotlářský	k2eAgFnSc6d1	Kotlářská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
fakulta	fakulta	k1gFnSc1	fakulta
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozrůstáním	rozrůstání	k1gNnSc7	rozrůstání
fakulty	fakulta	k1gFnSc2	fakulta
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gFnSc1	jejich
kapacita	kapacita	k1gFnSc1	kapacita
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
musely	muset	k5eAaImAgFnP	muset
umístit	umístit	k5eAaPmF	umístit
mimo	mimo	k7c4	mimo
komplex	komplex	k1gInSc4	komplex
na	na	k7c6	na
Kotlářské	kotlářský	k2eAgFnSc6d1	Kotlářská
ulici	ulice	k1gFnSc6	ulice
do	do	k7c2	do
různých	různý	k2eAgNnPc2d1	různé
provizorií	provizorium	k1gNnPc2	provizorium
(	(	kIx(	(
<g/>
bývalá	bývalý	k2eAgNnPc1d1	bývalé
kasárna	kasárna	k1gNnPc1	kasárna
v	v	k7c6	v
Řečkovicích	Řečkovice	k1gFnPc6	Řečkovice
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgInSc1d1	univerzitní
komplex	komplex	k1gInSc1	komplex
Vinařská	vinařský	k2eAgFnSc1d1	vinařská
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
VUT	VUT	kA	VUT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzrůstající	vzrůstající	k2eAgInPc1d1	vzrůstající
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
a	a	k8xC	a
vybavení	vybavení	k1gNnSc4	vybavení
pracovišť	pracoviště	k1gNnPc2	pracoviště
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
Univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
kampusu	kampus	k1gInSc2	kampus
v	v	k7c6	v
Bohunicích	Bohunice	k1gFnPc6	Bohunice
<g/>
.	.	kIx.	.
<g/>
Součástí	součást	k1gFnSc7	součást
stávajícího	stávající	k2eAgInSc2d1	stávající
areálu	areál	k1gInSc2	areál
v	v	k7c6	v
Kotlářské	kotlářský	k2eAgFnSc6d1	Kotlářská
ulici	ulice	k1gFnSc6	ulice
je	být	k5eAaImIp3nS	být
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
fakulty	fakulta	k1gFnSc2	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstala	zůstat	k5eAaPmAgFnS	zůstat
pouze	pouze	k6eAd1	pouze
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
moravská	moravský	k2eAgFnSc1d1	Moravská
i	i	k8xC	i
česká	český	k2eAgFnSc1d1	Česká
inteligence	inteligence	k1gFnSc1	inteligence
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
či	či	k8xC	či
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
i	i	k9	i
pražská	pražský	k2eAgFnSc1d1	Pražská
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
obě	dva	k4xCgFnPc4	dva
technické	technický	k2eAgFnPc4d1	technická
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
zasedání	zasedání	k1gNnSc2	zasedání
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1918	[number]	k4	1918
podán	podat	k5eAaPmNgInS	podat
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
protagonisty	protagonista	k1gMnPc7	protagonista
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Engliš	Engliš	k1gMnSc1	Engliš
a	a	k8xC	a
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc4	návrh
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc4	sídlo
stanovil	stanovit	k5eAaPmAgInS	stanovit
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
univerzita	univerzita	k1gFnSc1	univerzita
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
tři	tři	k4xCgFnPc4	tři
fakulty	fakulta	k1gFnPc4	fakulta
-	-	kIx~	-
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
<g/>
,	,	kIx,	,
právnickou	právnický	k2eAgFnSc4d1	právnická
a	a	k8xC	a
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
<g/>
.	.	kIx.	.
</s>
<s>
Školský	školský	k2eAgInSc1d1	školský
výbor	výbor	k1gInSc1	výbor
doporučil	doporučit	k5eAaPmAgInS	doporučit
název	název	k1gInSc4	název
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkušeností	zkušenost	k1gFnPc2	zkušenost
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zřízení	zřízení	k1gNnSc4	zřízení
samostatné	samostatný	k2eAgFnSc2d1	samostatná
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
jako	jako	k8xS	jako
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
50	[number]	k4	50
(	(	kIx(	(
<g/>
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
druhé	druhý	k4xOgFnSc2	druhý
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1919	[number]	k4	1919
a	a	k8xC	a
přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
nejstarší	starý	k2eAgInSc1d3	nejstarší
samostatnou	samostatný	k2eAgFnSc7d1	samostatná
fakultou	fakulta	k1gFnSc7	fakulta
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
přednášky	přednáška	k1gFnPc1	přednáška
a	a	k8xC	a
praktika	praktika	k1gFnSc1	praktika
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1920	[number]	k4	1920
v	v	k7c6	v
posluchárnách	posluchárna	k1gFnPc6	posluchárna
mineralogie	mineralogie	k1gFnSc2	mineralogie
a	a	k8xC	a
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
v	v	k7c6	v
praktikách	praktika	k1gFnPc6	praktika
chemie	chemie	k1gFnSc2	chemie
na	na	k7c6	na
vysokém	vysoký	k2eAgNnSc6d1	vysoké
učení	učení	k1gNnSc6	učení
technickém	technický	k2eAgNnSc6d1	technické
<g/>
.	.	kIx.	.
</s>
<s>
Výuku	výuka	k1gFnSc4	výuka
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
přednáškou	přednáška	k1gFnSc7	přednáška
profesor	profesor	k1gMnSc1	profesor
Matyáš	Matyáš	k1gMnSc1	Matyáš
Lerch	Lerch	k1gMnSc1	Lerch
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
pro	pro	k7c4	pro
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
profesor	profesor	k1gMnSc1	profesor
PhDr.	PhDr.	kA	PhDr.
et	et	k?	et
PhMr	PhMr	k1gInSc1	PhMr
<g/>
.	.	kIx.	.
</s>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kužma	Kužm	k1gMnSc2	Kužm
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přešel	přejít	k5eAaPmAgInS	přejít
z	z	k7c2	z
brněnské	brněnský	k2eAgFnSc2d1	brněnská
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
obor	obor	k1gInSc4	obor
anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Proděkanem	proděkan	k1gMnSc7	proděkan
byl	být	k5eAaImAgMnS	být
mineralog	mineralog	k1gMnSc1	mineralog
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Rosický	rosický	k2eAgMnSc1d1	rosický
<g/>
.	.	kIx.	.
</s>
<s>
Fakultu	fakulta	k1gFnSc4	fakulta
navštěvovalo	navštěvovat	k5eAaImAgNnS	navštěvovat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
pouhých	pouhý	k2eAgMnPc2d1	pouhý
9	[number]	k4	9
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
výuka	výuka	k1gFnSc1	výuka
zahájena	zahájen	k2eAgFnSc1d1	zahájena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
/	/	kIx~	/
<g/>
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
vybrány	vybrán	k2eAgFnPc1d1	vybrána
budovy	budova	k1gFnPc1	budova
městského	městský	k2eAgInSc2d1	městský
chudobince	chudobinec	k1gInSc2	chudobinec
mezi	mezi	k7c7	mezi
ulicemi	ulice	k1gFnPc7	ulice
Kounicova	Kounicův	k2eAgNnPc1d1	Kounicův
<g/>
,	,	kIx,	,
Veveří	veveří	k2eAgNnPc1d1	veveří
a	a	k8xC	a
Kotlářská	kotlářský	k2eAgNnPc1d1	Kotlářské
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
se	se	k3xPyFc4	se
provizorium	provizorium	k1gNnSc1	provizorium
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
a	a	k8xC	a
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
budovat	budovat	k5eAaImF	budovat
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
pro	pro	k7c4	pro
ústavy	ústav	k1gInPc4	ústav
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
<g/>
Zeměpisné	zeměpisný	k2eAgInPc1d1	zeměpisný
ústavy	ústav	k1gInPc1	ústav
a	a	k8xC	a
antropologické	antropologický	k2eAgInPc1d1	antropologický
ústavy	ústav	k1gInPc1	ústav
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
mimo	mimo	k7c4	mimo
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
bývalé	bývalý	k2eAgFnSc2d1	bývalá
obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Kounicova	Kounicův	k2eAgNnSc2d1	Kounicův
38	[number]	k4	38
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
adaptovány	adaptován	k2eAgInPc1d1	adaptován
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
k	k	k7c3	k
stávajícím	stávající	k2eAgInPc3d1	stávající
pavilonům	pavilon	k1gInPc3	pavilon
přibyl	přibýt	k5eAaPmAgInS	přibýt
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1931	[number]	k4	1931
pavilon	pavilon	k1gInSc1	pavilon
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Dalšímu	další	k2eAgNnSc3d1	další
rozšíření	rozšíření	k1gNnSc3	rozšíření
bránilo	bránit	k5eAaImAgNnS	bránit
budování	budování	k1gNnSc4	budování
botanické	botanický	k2eAgFnSc2d1	botanická
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
ústavy	ústav	k1gInPc1	ústav
a	a	k8xC	a
přednášející	přednášející	k2eAgMnPc1d1	přednášející
profesoři	profesor	k1gMnPc1	profesor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1921	[number]	k4	1921
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
matematiky	matematika	k1gFnSc2	matematika
-	-	kIx~	-
Matyáš	Matyáš	k1gMnSc1	Matyáš
Lerch	Lerch	k1gMnSc1	Lerch
(	(	kIx(	(
<g/>
matematická	matematický	k2eAgFnSc1d1	matematická
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Seifert	Seifert	k1gMnSc1	Seifert
(	(	kIx(	(
<g/>
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
algebra	algebra	k1gFnSc1	algebra
a	a	k8xC	a
analýza	analýza	k1gFnSc1	analýza
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
fyziku	fyzika	k1gFnSc4	fyzika
-	-	kIx~	-
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Hostinský	hostinský	k1gMnSc1	hostinský
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
-	-	kIx~	-
Bedřich	Bedřich	k1gMnSc1	Bedřich
Macků	Macků	k1gMnSc1	Macků
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
chemii	chemie	k1gFnSc4	chemie
-	-	kIx~	-
Antonín	Antonín	k1gMnSc1	Antonín
Šimek	Šimek	k1gMnSc1	Šimek
(	(	kIx(	(
<g/>
výzkum	výzkum	k1gInSc1	výzkum
krystalické	krystalický	k2eAgFnSc2d1	krystalická
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
anorganické	anorganický	k2eAgFnSc2d1	anorganická
chemie	chemie	k1gFnSc2	chemie
-	-	kIx~	-
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kužma	Kužm	k1gMnSc2	Kužm
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Frejka	Frejka	k1gFnSc1	Frejka
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
mineralogický	mineralogický	k2eAgInSc1d1	mineralogický
-	-	kIx~	-
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Rosický	rosický	k2eAgMnSc1d1	rosický
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
geologický	geologický	k2eAgInSc1d1	geologický
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Woldřich	Woldřich	k1gMnSc1	Woldřich
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
Zahálka	Zahálka	k1gMnSc1	Zahálka
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
zoologický	zoologický	k2eAgInSc1d1	zoologický
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Zavřel	Zavřel	k1gMnSc1	Zavřel
</s>
</p>
<p>
<s>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Dubský	Dubský	k1gMnSc1	Dubský
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
botanický	botanický	k2eAgInSc1d1	botanický
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
Podpěra	podpěra	k1gFnSc1	podpěra
(	(	kIx(	(
<g/>
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
a	a	k8xC	a
systematická	systematický	k2eAgFnSc1d1	systematická
botanika	botanika	k1gFnSc1	botanika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
fyziologii	fyziologie	k1gFnSc4	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
-	-	kIx~	-
Vladimír	Vladimír	k1gMnSc1	Vladimír
Úlehla	Úlehla	k1gMnSc1	Úlehla
</s>
</p>
<p>
<s>
1922-1923	[number]	k4	1922-1923
</s>
</p>
<p>
<s>
Geografický	geografický	k2eAgInSc1d1	geografický
ústav	ústav	k1gInSc1	ústav
a	a	k8xC	a
seminář	seminář	k1gInSc1	seminář
-	-	kIx~	-
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vitásek	Vitásek	k1gMnSc1	Vitásek
(	(	kIx(	(
<g/>
fyzický	fyzický	k2eAgInSc1d1	fyzický
zeměpis	zeměpis	k1gInSc1	zeměpis
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1923-1924	[number]	k4	1923-1924
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
astronomický	astronomický	k2eAgInSc1d1	astronomický
-	-	kIx~	-
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kladivo	kladivo	k1gNnSc4	kladivo
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
antropologický	antropologický	k2eAgInSc1d1	antropologický
-	-	kIx~	-
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Suk	Suk	k1gMnSc1	Suk
</s>
</p>
<p>
<s>
1934-1935	[number]	k4	1934-1935
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
botaniky	botanika	k1gFnSc2	botanika
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
-	-	kIx~	-
Josef	Josef	k1gMnSc1	Josef
PodpěraAčkoli	PodpěraAčkole	k1gFnSc6	PodpěraAčkole
se	se	k3xPyFc4	se
zemská	zemský	k2eAgFnSc1d1	zemská
správa	správa	k1gFnSc1	správa
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
univerzitní	univerzitní	k2eAgInSc4d1	univerzitní
areál	areál	k1gInSc4	areál
na	na	k7c6	na
Kraví	kraví	k2eAgFnSc6d1	kraví
hoře	hora	k1gFnSc6	hora
a	a	k8xC	a
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
zřízena	zřízen	k2eAgFnSc1d1	zřízena
stavební	stavební	k2eAgFnSc1d1	stavební
kancelář	kancelář	k1gFnSc1	kancelář
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
pozemků	pozemek	k1gInPc2	pozemek
vykoupena	vykoupit	k5eAaPmNgFnS	vykoupit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Budovám	budova	k1gFnPc3	budova
fakulty	fakulta	k1gFnSc2	fakulta
byl	být	k5eAaImAgInS	být
vyčleněn	vyčlenit	k5eAaPmNgInS	vyčlenit
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Kumpošta	Kumpošt	k1gMnSc2	Kumpošt
a	a	k8xC	a
Bohuslava	Bohuslav	k1gMnSc2	Bohuslav
Fuchse	Fuchs	k1gMnSc2	Fuchs
prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
Babákovým	Babákův	k2eAgNnSc7d1	Babákovo
náměstím	náměstí	k1gNnSc7	náměstí
a	a	k8xC	a
ulicí	ulice	k1gFnSc7	ulice
Březinova	Březinův	k2eAgNnSc2d1	Březinovo
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1	vládní
rada	rada	k1gFnSc1	rada
Bohumil	Bohumil	k1gMnSc1	Bohumil
Šele	Šele	k1gInSc1	Šele
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
však	však	k9	však
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ponechat	ponechat	k5eAaPmF	ponechat
fakultu	fakulta	k1gFnSc4	fakulta
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
umístění	umístění	k1gNnSc6	umístění
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
nestačila	stačit	k5eNaBmAgFnS	stačit
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
stavby	stavba	k1gFnSc2	stavba
právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
vyřešit	vyřešit	k5eAaPmF	vyřešit
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zamýšlený	zamýšlený	k2eAgInSc1d1	zamýšlený
projekt	projekt	k1gInSc1	projekt
výstavby	výstavba	k1gFnSc2	výstavba
nových	nový	k2eAgFnPc2d1	nová
fakultních	fakultní	k2eAgFnPc2d1	fakultní
budov	budova	k1gFnPc2	budova
byl	být	k5eAaImAgInS	být
realizován	realizovat	k5eAaBmNgInS	realizovat
až	až	k9	až
kampusem	kampus	k1gInSc7	kampus
v	v	k7c6	v
Bohunicích	Bohunice	k1gFnPc6	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
musela	muset	k5eAaImAgFnS	muset
fakulta	fakulta	k1gFnSc1	fakulta
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
existenci	existence	k1gFnSc4	existence
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
návrhu	návrh	k1gInSc2	návrh
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
zrušení	zrušení	k1gNnSc4	zrušení
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Kramáře	kramář	k1gMnSc2	kramář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
udělovány	udělován	k2eAgInPc4d1	udělován
čestné	čestný	k2eAgInPc4d1	čestný
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1926	[number]	k4	1926
Františkovi	František	k1gMnSc3	František
Vejdovskému	Vejdovský	k2eAgMnSc3d1	Vejdovský
<g/>
,	,	kIx,	,
profesoru	profesor	k1gMnSc6	profesor
zoologie	zoologie	k1gFnSc1	zoologie
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
udělen	udělit	k5eAaPmNgInS	udělit
Alešovi	Aleš	k1gMnSc3	Aleš
Hrdličkovi	Hrdlička	k1gMnSc3	Hrdlička
<g/>
,	,	kIx,	,
následoval	následovat	k5eAaImAgMnS	následovat
Jan	Jan	k1gMnSc1	Jan
Bašta	Bašta	k1gMnSc1	Bašta
(	(	kIx(	(
<g/>
sekční	sekční	k2eAgMnSc1d1	sekční
šéf	šéf	k1gMnSc1	šéf
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
železnic	železnice	k1gFnPc2	železnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Karel	Karel	k1gMnSc1	Karel
Petr	Petr	k1gMnSc1	Petr
(	(	kIx(	(
<g/>
profesor	profesor	k1gMnSc1	profesor
matematiky	matematika	k1gFnSc2	matematika
na	na	k7c6	na
přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Němec	Němec	k1gMnSc1	Němec
(	(	kIx(	(
<g/>
botanik	botanik	k1gMnSc1	botanik
<g/>
)	)	kIx)	)
a	a	k8xC	a
prvním	první	k4xOgMnPc3	první
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
nositelem	nositel	k1gMnSc7	nositel
čestného	čestný	k2eAgInSc2d1	čestný
doktorátu	doktorát	k1gInSc2	doktorát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americký	americký	k2eAgMnSc1d1	americký
botanik	botanik	k1gMnSc1	botanik
Francis	Francis	k1gFnSc2	Francis
Ernest	Ernest	k1gMnSc1	Ernest
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
obnova	obnova	k1gFnSc1	obnova
===	===	k?	===
</s>
</p>
<p>
<s>
Budova	budova	k1gFnSc1	budova
zeměpisného	zeměpisný	k2eAgInSc2d1	zeměpisný
a	a	k8xC	a
antropologického	antropologický	k2eAgInSc2d1	antropologický
ústavu	ústav	k1gInSc2	ústav
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
,	,	kIx,	,
náhradou	náhrada	k1gFnSc7	náhrada
dostala	dostat	k5eAaPmAgFnS	dostat
fakulta	fakulta	k1gFnSc1	fakulta
německou	německý	k2eAgFnSc4d1	německá
lidovou	lidový	k2eAgFnSc4d1	lidová
školu	škola	k1gFnSc4	škola
na	na	k7c6	na
Janáčkově	Janáčkův	k2eAgNnSc6d1	Janáčkovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
na	na	k7c6	na
Kotlářské	kotlářský	k2eAgFnSc6d1	Kotlářská
ulici	ulice	k1gFnSc6	ulice
za	za	k7c2	za
války	válka	k1gFnSc2	válka
používala	používat	k5eAaImAgFnS	používat
německá	německý	k2eAgFnSc1d1	německá
technika	technika	k1gFnSc1	technika
ke	k	k7c3	k
školským	školský	k2eAgInPc3d1	školský
účelům	účel	k1gInPc3	účel
a	a	k8xC	a
proto	proto	k8xC	proto
adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
byla	být	k5eAaImAgFnS	být
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
se	se	k3xPyFc4	se
i	i	k9	i
vybavení	vybavení	k1gNnSc1	vybavení
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
války	válka	k1gFnSc2	válka
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
profesoři	profesor	k1gMnPc1	profesor
Bohumil	Bohumil	k1gMnSc1	Bohumil
Kladivo	kladivo	k1gNnSc1	kladivo
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Koláček	Koláček	k1gMnSc1	Koláček
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Sahánek	Sahánek	k1gMnSc1	Sahánek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
Šimek	Šimek	k1gMnSc1	Šimek
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Rosický	rosický	k2eAgMnSc1d1	rosický
a	a	k8xC	a
František	František	k1gMnSc1	František
Říkovský	Říkovský	k1gMnSc1	Říkovský
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
Mauthausenu	Mauthausen	k1gInSc6	Mauthausen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
umučen	umučit	k5eAaPmNgMnS	umučit
i	i	k9	i
docent	docent	k1gMnSc1	docent
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Hrudička	hrudička	k1gFnSc1	hrudička
a	a	k8xC	a
asistent	asistent	k1gMnSc1	asistent
Vladimír	Vladimír	k1gMnSc1	Vladimír
Krist	Krista	k1gFnPc2	Krista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
zbylo	zbýt	k5eAaPmAgNnS	zbýt
jedenáct	jedenáct	k4xCc1	jedenáct
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
4	[number]	k4	4
mimořádní	mimořádný	k2eAgMnPc1d1	mimořádný
profesoři	profesor	k1gMnPc1	profesor
(	(	kIx(	(
<g/>
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
získali	získat	k5eAaPmAgMnP	získat
řádnou	řádný	k2eAgFnSc4d1	řádná
profesuru	profesura	k1gFnSc4	profesura
<g/>
)	)	kIx)	)
a	a	k8xC	a
14	[number]	k4	14
soukromých	soukromý	k2eAgMnPc2d1	soukromý
docentů	docent	k1gMnPc2	docent
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
jmenováno	jmenovat	k5eAaImNgNnS	jmenovat
7	[number]	k4	7
mimořádných	mimořádný	k2eAgMnPc2d1	mimořádný
profesorů	profesor	k1gMnPc2	profesor
(	(	kIx(	(
<g/>
František	František	k1gMnSc1	František
Toul	toul	k1gInSc4	toul
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Babička	babička	k1gFnSc1	babička
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Knichel	Knichel	k1gMnSc1	Knichel
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Sekanina	Sekanina	k1gMnSc1	Sekanina
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Krejčí	Krejčí	k1gMnSc1	Krejčí
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Hadáček	Hadáček	k1gMnSc1	Hadáček
<g/>
)	)	kIx)	)
a	a	k8xC	a
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
soukromých	soukromý	k2eAgMnPc2d1	soukromý
docentů	docent	k1gMnPc2	docent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgInSc6d1	letní
semestru	semestr	k1gInSc6	semestr
měla	mít	k5eAaImAgFnS	mít
fakulta	fakulta	k1gFnSc1	fakulta
528	[number]	k4	528
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
/	/	kIx~	/
<g/>
46	[number]	k4	46
již	již	k9	již
718	[number]	k4	718
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgInSc7d1	nový
oborem	obor	k1gInSc7	obor
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1945	[number]	k4	1945
dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
studium	studium	k1gNnSc4	studium
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
vyučujících	vyučující	k2eAgMnPc2d1	vyučující
měla	mít	k5eAaImAgFnS	mít
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
pedagogické	pedagogický	k2eAgFnSc2d1	pedagogická
fakulty	fakulta	k1gFnSc2	fakulta
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
či	či	k8xC	či
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
jiným	jiný	k2eAgFnPc3d1	jiná
univerzitám	univerzita	k1gFnPc3	univerzita
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ubytování	ubytování	k1gNnSc3	ubytování
studentům	student	k1gMnPc3	student
sloužily	sloužit	k5eAaImAgFnP	sloužit
Kounicovy	Kounicův	k2eAgFnPc1d1	Kounicova
koleje	kolej	k1gFnPc1	kolej
<g/>
,	,	kIx,	,
dívčí	dívčí	k2eAgInSc1d1	dívčí
studentský	studentský	k2eAgInSc1d1	studentský
dům	dům	k1gInSc1	dům
na	na	k7c4	na
ulici	ulice	k1gFnSc4	ulice
Mučednická	mučednický	k2eAgFnSc1d1	mučednická
<g/>
,	,	kIx,	,
dívčí	dívčí	k2eAgFnSc1d1	dívčí
kolej	kolej	k1gFnSc1	kolej
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
(	(	kIx(	(
<g/>
Tvrdého	tvrdé	k1gNnSc2	tvrdé
7	[number]	k4	7
<g/>
)	)	kIx)	)
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
kolej	kolej	k1gFnSc1	kolej
Sušilova	Sušilův	k2eAgFnSc1d1	Sušilova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
měla	mít	k5eAaImAgFnS	mít
fakulta	fakulta	k1gFnSc1	fakulta
17	[number]	k4	17
ústavů	ústav	k1gInPc2	ústav
-	-	kIx~	-
Matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
Geografie	geografie	k1gFnSc1	geografie
<g/>
,	,	kIx,	,
Astronomie	astronomie	k1gFnSc1	astronomie
<g/>
,	,	kIx,	,
Experimentální	experimentální	k2eAgFnSc1d1	experimentální
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Organická	organický	k2eAgFnSc1d1	organická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Mineralogie	mineralogie	k1gFnSc1	mineralogie
a	a	k8xC	a
petrografie	petrografie	k1gFnSc1	petrografie
<g/>
,	,	kIx,	,
Geologie	geologie	k1gFnSc1	geologie
<g/>
,	,	kIx,	,
Zoologie	zoologie	k1gFnSc1	zoologie
<g/>
,	,	kIx,	,
Antropologie	antropologie	k1gFnSc1	antropologie
<g/>
,	,	kIx,	,
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
a	a	k8xC	a
systematická	systematický	k2eAgFnSc1d1	systematická
botanika	botanika	k1gFnSc1	botanika
<g/>
,	,	kIx,	,
Fyziologie	fyziologie	k1gFnSc1	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
,	,	kIx,	,
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
Farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
botanika	botanika	k1gFnSc1	botanika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
přibyl	přibýt	k5eAaPmAgInS	přibýt
ústav	ústav	k1gInSc1	ústav
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
.	.	kIx.	.
</s>
<s>
Zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
190	[number]	k4	190
<g/>
/	/	kIx~	/
<g/>
48	[number]	k4	48
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
28	[number]	k4	28
<g/>
.	.	kIx.	.
červenci	červenec	k1gInSc6	červenec
obecně	obecně	k6eAd1	obecně
studium	studium	k1gNnSc1	studium
farmakologie	farmakologie	k1gFnSc2	farmakologie
přiřazeno	přiřadit	k5eAaPmNgNnS	přiřadit
k	k	k7c3	k
lékařským	lékařský	k2eAgFnPc3d1	lékařská
fakultám	fakulta	k1gFnPc3	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Padesátá	padesátý	k4xOgNnPc4	padesátý
a	a	k8xC	a
šedesátá	šedesátý	k4xOgNnPc4	šedesátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
sovětském	sovětský	k2eAgInSc6d1	sovětský
vzoru	vzor	k1gInSc6	vzor
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1950-51	[number]	k4	1950-51
všechny	všechen	k3xTgFnPc1	všechen
ústavy	ústava	k1gFnPc4	ústava
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
katedrami	katedra	k1gFnPc7	katedra
jako	jako	k8xC	jako
nadstavbovými	nadstavbový	k2eAgInPc7d1	nadstavbový
orgány	orgán	k1gInPc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nepokládala	pokládat	k5eNaImAgFnS	pokládat
za	za	k7c4	za
vhodné	vhodný	k2eAgNnSc4d1	vhodné
úzká	úzký	k2eAgFnSc1d1	úzká
specializace	specializace	k1gFnSc1	specializace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
široký	široký	k2eAgInSc4d1	široký
obzor	obzor	k1gInSc4	obzor
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
pouze	pouze	k6eAd1	pouze
šest	šest	k4xCc1	šest
odborných	odborný	k2eAgFnPc2d1	odborná
kateder	katedra	k1gFnPc2	katedra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
matematiky	matematika	k1gFnSc2	matematika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
fyziky	fyzika	k1gFnSc2	fyzika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
biologie	biologie	k1gFnSc2	biologie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
geologie	geologie	k1gFnSc2	geologie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
geografiea	geografiea	k1gFnSc1	geografiea
chybět	chybět	k5eAaImF	chybět
nemohla	moct	k5eNaImAgFnS	moct
katedra	katedra	k1gFnSc1	katedra
marxismu-leninismu	marxismueninismus	k1gInSc2	marxismu-leninismus
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
astronomie	astronomie	k1gFnSc2	astronomie
bylo	být	k5eAaImAgNnS	být
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
astronomický	astronomický	k2eAgInSc1d1	astronomický
ústav	ústav	k1gInSc1	ústav
však	však	k9	však
fungoval	fungovat	k5eAaImAgInS	fungovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
byly	být	k5eAaImAgInP	být
učitelské	učitelský	k2eAgInPc1d1	učitelský
obory	obor	k1gInPc1	obor
převedeny	převést	k5eAaPmNgInP	převést
na	na	k7c4	na
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
učitelské	učitelský	k2eAgNnSc1d1	učitelské
studium	studium	k1gNnSc1	studium
v	v	k7c6	v
dvoupředmětných	dvoupředmětný	k2eAgFnPc6d1	dvoupředmětný
kombinacích	kombinace	k1gFnPc6	kombinace
znovu	znovu	k6eAd1	znovu
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
zabránit	zabránit	k5eAaPmF	zabránit
i	i	k8xC	i
přesunutí	přesunutí	k1gNnSc4	přesunutí
zejména	zejména	k9	zejména
biologických	biologický	k2eAgInPc2d1	biologický
směrů	směr	k1gInPc2	směr
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
(	(	kIx(	(
<g/>
argumentem	argument	k1gInSc7	argument
bylo	být	k5eAaImAgNnS	být
užší	úzký	k2eAgNnSc1d2	užší
sepětí	sepětí	k1gNnSc1	sepětí
se	s	k7c7	s
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ministerských	ministerský	k2eAgInPc2d1	ministerský
výnosů	výnos	k1gInPc2	výnos
byli	být	k5eAaImAgMnP	být
studenti	student	k1gMnPc1	student
striktně	striktně	k6eAd1	striktně
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
do	do	k7c2	do
ročníků	ročník	k1gInPc2	ročník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výuce	výuka	k1gFnSc6	výuka
byl	být	k5eAaImAgInS	být
vypracován	vypracován	k2eAgInSc1d1	vypracován
přesný	přesný	k2eAgInSc1d1	přesný
sled	sled	k1gInSc1	sled
přednášek	přednáška	k1gFnPc2	přednáška
<g/>
,	,	kIx,	,
seminářů	seminář	k1gInPc2	seminář
a	a	k8xC	a
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
návštěva	návštěva	k1gFnSc1	návštěva
byla	být	k5eAaImAgFnS	být
povinnou	povinný	k2eAgFnSc7d1	povinná
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
dvou	dva	k4xCgFnPc2	dva
státních	státní	k2eAgFnPc2d1	státní
zkoušek	zkouška	k1gFnPc2	zkouška
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dílčími	dílčí	k2eAgFnPc7d1	dílčí
zkouškami	zkouška	k1gFnPc7	zkouška
a	a	k8xC	a
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
obhajoba	obhajoba	k1gFnSc1	obhajoba
diplomové	diplomový	k2eAgFnSc2d1	Diplomová
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
semestru	semestr	k1gInSc2	semestr
1948	[number]	k4	1948
<g/>
/	/	kIx~	/
<g/>
49	[number]	k4	49
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
povinná	povinný	k2eAgFnSc1d1	povinná
výuka	výuka	k1gFnSc1	výuka
společenských	společenský	k2eAgFnPc2d1	společenská
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1952	[number]	k4	1952
vojenská	vojenský	k2eAgFnSc1d1	vojenská
příprava	příprava	k1gFnSc1	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přijímacích	přijímací	k2eAgInPc6d1	přijímací
pohovorech	pohovor	k1gInPc6	pohovor
se	se	k3xPyFc4	se
uplatňovala	uplatňovat	k5eAaImAgNnP	uplatňovat
třídní	třídní	k2eAgNnPc1d1	třídní
politická	politický	k2eAgNnPc1d1	politické
hlediska	hledisko	k1gNnPc1	hledisko
<g/>
,	,	kIx,	,
převládající	převládající	k2eAgInSc1d1	převládající
do	do	k7c2	do
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
zahájila	zahájit	k5eAaPmAgFnS	zahájit
soutěž	soutěž	k1gFnSc1	soutěž
tvořivosti	tvořivost	k1gFnSc2	tvořivost
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reforma	reforma	k1gFnSc1	reforma
školské	školský	k2eAgFnSc2d1	školská
soustavy	soustava	k1gFnSc2	soustava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
zrušila	zrušit	k5eAaPmAgFnS	zrušit
tradiční	tradiční	k2eAgInPc4d1	tradiční
tituly	titul	k1gInPc4	titul
absolventů	absolvent	k1gMnPc2	absolvent
a	a	k8xC	a
stanovila	stanovit	k5eAaPmAgFnS	stanovit
délku	délka	k1gFnSc4	délka
studia	studio	k1gNnSc2	studio
jako	jako	k8xS	jako
pětileté	pětiletý	k2eAgFnSc6d1	pětiletá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
vydávala	vydávat	k5eAaPmAgFnS	vydávat
fakulta	fakulta	k1gFnSc1	fakulta
časopis	časopis	k1gInSc1	časopis
Spisy	spis	k1gInPc7	spis
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
probíhaly	probíhat	k5eAaImAgFnP	probíhat
obhajoby	obhajoba	k1gFnPc1	obhajoba
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Astronomický	astronomický	k2eAgInSc1d1	astronomický
ústav	ústav	k1gInSc1	ústav
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
a	a	k8xC	a
zřízení	zřízení	k1gNnSc4	zřízení
observatoře	observatoř	k1gFnSc2	observatoř
na	na	k7c6	na
Kraví	kraví	k2eAgFnSc6d1	kraví
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
usnesení	usnesení	k1gNnSc2	usnesení
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1959	[number]	k4	1959
"	"	kIx"	"
<g/>
O	o	k7c6	o
těsném	těsný	k2eAgNnSc6d1	těsné
spojení	spojení	k1gNnSc6	spojení
školy	škola	k1gFnSc2	škola
se	s	k7c7	s
životem	život	k1gInSc7	život
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
posluchači	posluchač	k1gMnPc7	posluchač
povinně	povinně	k6eAd1	povinně
účastnili	účastnit	k5eAaImAgMnP	účastnit
praxe	praxe	k1gFnSc2	praxe
v	v	k7c6	v
Závodech	závod	k1gInPc6	závod
Jana	Jan	k1gMnSc2	Jan
Švermy	Šverma	k1gFnSc2	Šverma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
příznivá	příznivý	k2eAgFnSc1d1	příznivá
byla	být	k5eAaImAgFnS	být
věková	věkový	k2eAgFnSc1d1	věková
skladba	skladba	k1gFnSc1	skladba
pedagogického	pedagogický	k2eAgInSc2d1	pedagogický
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
tabulka	tabulka	k1gFnSc1	tabulka
věku	věk	k1gInSc2	věk
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
uchazečů	uchazeč	k1gMnPc2	uchazeč
o	o	k7c4	o
jmenovací	jmenovací	k2eAgNnSc4d1	jmenovací
a	a	k8xC	a
habilitační	habilitační	k2eAgNnSc4d1	habilitační
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
profesorů	profesor	k1gMnPc2	profesor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ludvík	Ludvík	k1gMnSc1	Ludvík
Sommer	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
a	a	k8xC	a
již	již	k6eAd1	již
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
specializovala	specializovat	k5eAaBmAgFnS	specializovat
výuka	výuka	k1gFnSc1	výuka
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
nové	nový	k2eAgInPc4d1	nový
obory	obor	k1gInPc4	obor
a	a	k8xC	a
katedry	katedra	k1gFnPc4	katedra
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
/	/	kIx~	/
<g/>
53	[number]	k4	53
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
naftové	naftový	k2eAgFnSc2d1	naftová
geologie	geologie	k1gFnSc2	geologie
(	(	kIx(	(
<g/>
fungovala	fungovat	k5eAaImAgFnS	fungovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
dialektického	dialektický	k2eAgInSc2d1	dialektický
a	a	k8xC	a
historického	historický	k2eAgInSc2d1	historický
materialismu	materialismus	k1gInSc2	materialismus
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
botaniky	botanika	k1gFnSc2	botanika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
antropologie	antropologie	k1gFnSc2	antropologie
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
/	/	kIx~	/
<g/>
54	[number]	k4	54
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
teoretické	teoretický	k2eAgFnSc2d1	teoretická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
anorganické	anorganický	k2eAgFnSc2d1	anorganická
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
+	+	kIx~	+
biochemie	biochemie	k1gFnSc2	biochemie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
paleontologie	paleontologie	k1gFnSc2	paleontologie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
mineralogie	mineralogie	k1gFnSc1	mineralogie
a	a	k8xC	a
petrografie	petrografie	k1gFnSc1	petrografie
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
/	/	kIx~	/
<g/>
55	[number]	k4	55
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
fyziologie	fyziologie	k1gFnSc2	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
a	a	k8xC	a
genetiky	genetika	k1gFnSc2	genetika
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
<g/>
/	/	kIx~	/
<g/>
58	[number]	k4	58
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
fyziologie	fyziologie	k1gFnSc2	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
genetiky	genetika	k1gFnSc2	genetika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
<g/>
/	/	kIx~	/
<g/>
60	[number]	k4	60
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
algebry	algebra	k1gFnSc2	algebra
a	a	k8xC	a
geometrie	geometrie	k1gFnSc2	geometrie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
experimentální	experimentální	k2eAgFnSc2d1	experimentální
fyziky	fyzika	k1gFnSc2	fyzika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
/	/	kIx~	/
<g/>
62	[number]	k4	62
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
fyziky	fyzika	k1gFnSc2	fyzika
pevné	pevný	k2eAgFnSc2d1	pevná
fáze	fáze	k1gFnSc2	fáze
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
elektroniky	elektronika	k1gFnSc2	elektronika
a	a	k8xC	a
vakuové	vakuový	k2eAgFnSc2d1	vakuová
fyziky	fyzika	k1gFnSc2	fyzika
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
/	/	kIx~	/
<g/>
63	[number]	k4	63
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
biochemie	biochemie	k1gFnSc2	biochemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
fyzilogie	fyzilogie	k1gFnSc2	fyzilogie
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
zoologie	zoologie	k1gFnSc2	zoologie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
biofyziky	biofyzika	k1gFnSc2	biofyzika
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
/	/	kIx~	/
<g/>
64	[number]	k4	64
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
těleské	těleský	k2eAgFnSc2d1	těleský
výchovy	výchova	k1gFnSc2	výchova
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
numerické	numerický	k2eAgFnSc2d1	numerická
matematiky	matematika	k1gFnSc2	matematika
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
/	/	kIx~	/
<g/>
68	[number]	k4	68
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
politické	politický	k2eAgFnSc2d1	politická
ekonomieCelkem	ekonomieCelek	k1gInSc7	ekonomieCelek
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
22	[number]	k4	22
odborných	odborný	k2eAgFnPc2d1	odborná
kateder	katedra	k1gFnPc2	katedra
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
působilo	působit	k5eAaImAgNnS	působit
186	[number]	k4	186
pedagogů	pedagog	k1gMnPc2	pedagog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vládního	vládní	k2eAgNnSc2d1	vládní
nařízení	nařízení	k1gNnSc2	nařízení
č.	č.	k?	č.
120	[number]	k4	120
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
univerzity	univerzita	k1gFnSc2	univerzita
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
měla	mít	k5eAaImAgFnS	mít
fakulta	fakulta	k1gFnSc1	fakulta
140	[number]	k4	140
pedagogů	pedagog	k1gMnPc2	pedagog
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
13	[number]	k4	13
profesorů	profesor	k1gMnPc2	profesor
a	a	k8xC	a
14	[number]	k4	14
docentů	docent	k1gMnPc2	docent
<g/>
.	.	kIx.	.
</s>
<s>
Řešeny	řešen	k2eAgFnPc1d1	řešena
byly	být	k5eAaImAgFnP	být
2	[number]	k4	2
státní	státní	k2eAgInPc4d1	státní
úkoly	úkol	k1gInPc4	úkol
<g/>
,	,	kIx,	,
23	[number]	k4	23
resortních	resortní	k2eAgFnPc2d1	resortní
14	[number]	k4	14
fakultních	fakultní	k2eAgFnPc2d1	fakultní
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
byla	být	k5eAaImAgFnS	být
dvě	dva	k4xCgNnPc4	dva
specializovaná	specializovaný	k2eAgNnPc4d1	specializované
pracoviště	pracoviště	k1gNnPc4	pracoviště
-	-	kIx~	-
laboratoř	laboratoř	k1gFnSc1	laboratoř
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
laboratoř	laboratoř	k1gFnSc1	laboratoř
fyziologie	fyziologie	k1gFnSc2	fyziologie
a	a	k8xC	a
anatomie	anatomie	k1gFnSc2	anatomie
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
externí	externí	k2eAgFnSc1d1	externí
katedra	katedra	k1gFnSc1	katedra
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
chemie	chemie	k1gFnSc2	chemie
při	při	k7c6	při
Ústavu	ústav	k1gInSc6	ústav
makromolekulární	makromolekulární	k2eAgFnSc2d1	makromolekulární
chemie	chemie	k1gFnSc2	chemie
a	a	k8xC	a
katedra	katedra	k1gFnSc1	katedra
muzeologie	muzeologie	k1gFnSc2	muzeologie
pro	pro	k7c4	pro
filozofické	filozofický	k2eAgInPc4d1	filozofický
a	a	k8xC	a
přírodovědné	přírodovědný	k2eAgInPc4d1	přírodovědný
obory	obor	k1gInPc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Sloučením	sloučení	k1gNnSc7	sloučení
sbírky	sbírka	k1gFnSc2	sbírka
bakterií	bakterie	k1gFnPc2	bakterie
katedry	katedra	k1gFnSc2	katedra
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
a	a	k8xC	a
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
Mikrobiologického	mikrobiologický	k2eAgInSc2d1	mikrobiologický
ústavu	ústav	k1gInSc2	ústav
ČSAV	ČSAV	kA	ČSAV
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Československá	československý	k2eAgFnSc1d1	Československá
sbírka	sbírka	k1gFnSc1	sbírka
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
samostatná	samostatný	k2eAgFnSc1d1	samostatná
katedra	katedra	k1gFnSc1	katedra
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závěry	závěr	k1gInPc1	závěr
XII	XII	kA	XII
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
konaného	konaný	k2eAgInSc2d1	konaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
zdůraznily	zdůraznit	k5eAaPmAgFnP	zdůraznit
potřebu	potřeba	k1gFnSc4	potřeba
růstu	růst	k1gInSc2	růst
počtu	počet	k1gInSc2	počet
posluchačů	posluchač	k1gMnPc2	posluchač
přírodovědeckých	přírodovědecký	k2eAgMnPc2d1	přírodovědecký
(	(	kIx(	(
<g/>
a	a	k8xC	a
technických	technický	k2eAgInPc2d1	technický
<g/>
)	)	kIx)	)
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
stát	stát	k1gInSc1	stát
největší	veliký	k2eAgInSc1d3	veliký
fakultou	fakulta	k1gFnSc7	fakulta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
veškeré	veškerý	k3xTgInPc1	veškerý
učitelské	učitelský	k2eAgInPc1d1	učitelský
obory	obor	k1gInPc1	obor
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
převedeny	převést	k5eAaPmNgInP	převést
do	do	k7c2	do
Olomouce	Olomouc	k1gFnSc2	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
byl	být	k5eAaImAgInS	být
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
oborovou	oborový	k2eAgFnSc7d1	oborová
komisí	komise	k1gFnSc7	komise
Státního	státní	k2eAgInSc2d1	státní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
plán	plán	k1gInSc1	plán
rozvoje	rozvoj	k1gInSc2	rozvoj
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
radou	rada	k1gFnSc7	rada
fakulty	fakulta	k1gFnSc2	fakulta
schválen	schválit	k5eAaPmNgInS	schválit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sedmdesátá	sedmdesátý	k4xOgNnPc4	sedmdesátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
počátek	počátek	k1gInSc1	počátek
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
stranickými	stranický	k2eAgFnPc7d1	stranická
prověrkami	prověrka	k1gFnPc7	prověrka
vyučujících	vyučující	k2eAgInPc2d1	vyučující
před	před	k7c7	před
politicko-výchovnými	politickoýchovný	k2eAgFnPc7d1	politicko-výchovný
komisemi	komise	k1gFnPc7	komise
<g/>
,	,	kIx,	,
odstraněním	odstranění	k1gNnSc7	odstranění
liberalizace	liberalizace	k1gFnSc2	liberalizace
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
a	a	k8xC	a
integrací	integrace	k1gFnPc2	integrace
kateder	katedra	k1gFnPc2	katedra
<g/>
.	.	kIx.	.
</s>
<s>
Veškerou	veškerý	k3xTgFnSc4	veškerý
publikační	publikační	k2eAgFnSc4d1	publikační
činnost	činnost	k1gFnSc4	činnost
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
společná	společný	k2eAgFnSc1d1	společná
redakční	redakční	k2eAgFnSc1d1	redakční
rada	rada	k1gFnSc1	rada
<g/>
.	.	kIx.	.
</s>
<s>
Geografické	geografický	k2eAgInPc4d1	geografický
obory	obor	k1gInPc4	obor
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
smrt	smrt	k1gFnSc1	smrt
profesorů	profesor	k1gMnPc2	profesor
Františka	František	k1gMnSc2	František
Vitáska	Vitásek	k1gMnSc2	Vitásek
a	a	k8xC	a
Otokara	Otokar	k1gMnSc2	Otokar
Šlampy	Šlampy	k?	Šlampy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
zemřel	zemřít	k5eAaPmAgMnS	zemřít
vedoucí	vedoucí	k1gMnSc1	vedoucí
katedry	katedra	k1gFnSc2	katedra
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
paleontologie	paleontologie	k1gFnSc2	paleontologie
doc.	doc.	kA	doc.
Jiří	Jiří	k1gMnSc1	Jiří
Tejkal	Tejkal	k1gMnSc1	Tejkal
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
doc.	doc.	kA	doc.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Novotný	Novotný	k1gMnSc1	Novotný
z	z	k7c2	z
katedry	katedra	k1gFnSc2	katedra
mineralogie	mineralogie	k1gFnSc2	mineralogie
a	a	k8xC	a
petrografie	petrografie	k1gFnSc2	petrografie
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
přijatých	přijatý	k2eAgMnPc2d1	přijatý
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
o	o	k7c4	o
přijetí	přijetí	k1gNnSc4	přijetí
rozhodoval	rozhodovat	k5eAaImAgInS	rozhodovat
i	i	k9	i
třídní	třídní	k2eAgInSc1d1	třídní
původ	původ	k1gInSc1	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přihlášky	přihláška	k1gFnSc2	přihláška
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
podat	podat	k5eAaPmF	podat
na	na	k7c4	na
učitelské	učitelský	k2eAgFnPc4d1	učitelská
kombinace	kombinace	k1gFnPc4	kombinace
matematika-deskriptivní	matematikaeskriptivnit	k5eAaPmIp3nS	matematika-deskriptivnit
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
,	,	kIx,	,
matematika-fyzika	matematikayzika	k1gFnSc1	matematika-fyzika
<g/>
,	,	kIx,	,
matematika-chemie	matematikahemie	k1gFnSc1	matematika-chemie
<g/>
,	,	kIx,	,
biologie-chemie	biologiehemie	k1gFnSc1	biologie-chemie
<g/>
,	,	kIx,	,
matematika-výtvarná	matematikaýtvarný	k2eAgFnSc1d1	matematika-výtvarný
výchova	výchova	k1gFnSc1	výchova
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
/	/	kIx~	/
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematika-biologie	matematikaiologie	k1gFnSc1	matematika-biologie
a	a	k8xC	a
matematika-zeměpis	matematikaeměpis	k1gFnSc1	matematika-zeměpis
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc1	dva
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
/	/	kIx~	/
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzika-geologie	fyzikaeologie	k1gFnSc1	fyzika-geologie
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c4	na
odborné	odborný	k2eAgNnSc4d1	odborné
studium	studium	k1gNnSc4	studium
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
matematické	matematický	k2eAgFnSc2d1	matematická
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
chemie	chemie	k1gFnSc2	chemie
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
geografie	geografie	k1gFnSc2	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
zkvalitnění	zkvalitnění	k1gNnSc3	zkvalitnění
výuky	výuka	k1gFnSc2	výuka
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
didaktické	didaktický	k2eAgNnSc1d1	didaktické
oddělení	oddělení	k1gNnSc1	oddělení
při	při	k7c6	při
katedře	katedra	k1gFnSc6	katedra
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
kabinet	kabinet	k1gInSc1	kabinet
didaktiky	didaktika	k1gFnSc2	didaktika
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
oddělení	oddělení	k1gNnSc2	oddělení
didaktiky	didaktika	k1gFnSc2	didaktika
chemie	chemie	k1gFnSc2	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
7	[number]	k4	7
biologických	biologický	k2eAgFnPc2d1	biologická
kateder	katedra	k1gFnPc2	katedra
zůstaly	zůstat	k5eAaPmAgInP	zůstat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
pouhé	pouhý	k2eAgFnSc6d1	pouhá
3	[number]	k4	3
<g/>
,	,	kIx,	,
sloučením	sloučení	k1gNnSc7	sloučení
katedry	katedra	k1gFnSc2	katedra
numerické	numerický	k2eAgFnSc2d1	numerická
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
katedry	katedra	k1gFnSc2	katedra
matematických	matematický	k2eAgInPc2d1	matematický
strojů	stroj	k1gInPc2	stroj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
katedra	katedra	k1gFnSc1	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
výpočetní	výpočetní	k2eAgNnSc1d1	výpočetní
středisko	středisko	k1gNnSc1	středisko
bylo	být	k5eAaImAgNnS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
číslicovým	číslicový	k2eAgInSc7d1	číslicový
počítačem	počítač	k1gInSc7	počítač
MSP2A	MSP2A	k1gFnSc2	MSP2A
a	a	k8xC	a
analogovým	analogový	k2eAgMnSc7d1	analogový
AP	ap	kA	ap
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
ke	k	k7c3	k
katedře	katedra	k1gFnSc3	katedra
fyziky	fyzika	k1gFnSc2	fyzika
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
připojena	připojen	k2eAgFnSc1d1	připojena
laboratoř	laboratoř	k1gFnSc1	laboratoř
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
katedře	katedra	k1gFnSc3	katedra
anorganické	anorganický	k2eAgFnSc2d1	anorganická
chemie	chemie	k1gFnSc2	chemie
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
oddělení	oddělení	k1gNnSc1	oddělení
radiochemie	radiochemie	k1gFnSc2	radiochemie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kateder	katedra	k1gFnPc2	katedra
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
srovnávací	srovnávací	k2eAgFnSc2d1	srovnávací
fyziologie	fyziologie	k1gFnSc2	fyziologie
a	a	k8xC	a
obecné	obecný	k2eAgFnSc2d1	obecná
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
hydrobiologické	hydrobiologický	k2eAgFnSc2d1	hydrobiologická
laboratoře	laboratoř	k1gFnSc2	laboratoř
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jediná	jediný	k2eAgFnSc1d1	jediná
katedra	katedra	k1gFnSc1	katedra
biologie	biologie	k1gFnSc2	biologie
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
13	[number]	k4	13
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
35	[number]	k4	35
docentů	docent	k1gMnPc2	docent
a	a	k8xC	a
97	[number]	k4	97
odborných	odborný	k2eAgMnPc2d1	odborný
asistentů	asistent	k1gMnPc2	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
odborném	odborný	k2eAgNnSc6d1	odborné
studiu	studio	k1gNnSc6	studio
i	i	k8xC	i
učitelských	učitelský	k2eAgFnPc6d1	učitelská
kombinacích	kombinace	k1gFnPc6	kombinace
přinesly	přinést	k5eAaPmAgFnP	přinést
zejména	zejména	k9	zejména
nové	nový	k2eAgInPc4d1	nový
učební	učební	k2eAgInPc4d1	učební
plány	plán	k1gInPc4	plán
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
kateder	katedra	k1gFnPc2	katedra
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
algebry	algebra	k1gFnSc2	algebra
a	a	k8xC	a
geometrie	geometrie	k1gFnSc2	geometrie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
obecné	obecná	k1gFnSc2	obecná
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
didaktiky	didaktika	k1gFnPc1	didaktika
fyziky	fyzika	k1gFnSc2	fyzika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
fyziky	fyzika	k1gFnSc2	fyzika
pevné	pevný	k2eAgFnSc2d1	pevná
fáze	fáze	k1gFnSc2	fáze
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
elektroniky	elektronika	k1gFnSc2	elektronika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
anorganické	anorganický	k2eAgFnSc2d1	anorganická
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
teoretické	teoretický	k2eAgFnSc2d1	teoretická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
organické	organický	k2eAgFnSc2d1	organická
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
biochemie	biochemie	k1gFnSc2	biochemie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
biologie	biologie	k1gFnSc2	biologie
rostlin	rostlina	k1gFnPc2	rostlina
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
biologie	biologie	k1gFnSc2	biologie
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
,	,	kIx,	,
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
biofyziky	biofyzika	k1gFnSc2	biofyzika
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
geologie	geologie	k1gFnSc2	geologie
a	a	k8xC	a
paleontologie	paleontologie	k1gFnSc2	paleontologie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
mineralogie	mineralogie	k1gFnSc1	mineralogie
a	a	k8xC	a
petrografie	petrografie	k1gFnSc1	petrografie
</s>
</p>
<p>
<s>
katedra	katedra	k1gFnSc1	katedra
geografie	geografie	k1gFnSc2	geografie
</s>
</p>
<p>
<s>
===	===	k?	===
Osmdesátá	osmdesátý	k4xOgNnPc4	osmdesátý
léta	léto	k1gNnPc4	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
Negativním	negativní	k2eAgInSc7d1	negativní
zásahem	zásah	k1gInSc7	zásah
do	do	k7c2	do
kvality	kvalita	k1gFnSc2	kvalita
výuky	výuka	k1gFnSc2	výuka
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
39	[number]	k4	39
<g/>
/	/	kIx~	/
<g/>
1980	[number]	k4	1980
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
děkani	děkan	k1gMnPc1	děkan
a	a	k8xC	a
vedoucí	vedoucí	k1gFnSc1	vedoucí
kateder	katedra	k1gFnPc2	katedra
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
5	[number]	k4	5
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
prodloužení	prodloužení	k1gNnSc2	prodloužení
na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
docentů	docent	k1gMnPc2	docent
byla	být	k5eAaImAgFnS	být
vynechána	vynechat	k5eAaPmNgFnS	vynechat
podmínka	podmínka	k1gFnSc1	podmínka
obhájit	obhájit	k5eAaPmF	obhájit
doktorát	doktorát	k1gInSc4	doktorát
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
postačil	postačit	k5eAaPmAgInS	postačit
titul	titul	k1gInSc1	titul
kandidát	kandidát	k1gMnSc1	kandidát
věd	věda	k1gFnPc2	věda
<g/>
)	)	kIx)	)
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
titul	titul	k1gInSc4	titul
mimořádný	mimořádný	k2eAgMnSc1d1	mimořádný
profesor	profesor	k1gMnSc1	profesor
(	(	kIx(	(
<g/>
nadále	nadále	k6eAd1	nadále
pouze	pouze	k6eAd1	pouze
řádný	řádný	k2eAgMnSc1d1	řádný
profesor	profesor	k1gMnSc1	profesor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Absolventi	absolvent	k1gMnPc1	absolvent
mohli	moct	k5eAaImAgMnP	moct
získat	získat	k5eAaPmF	získat
doktorský	doktorský	k2eAgInSc4d1	doktorský
titul	titul	k1gInSc4	titul
bez	bez	k7c2	bez
vykonání	vykonání	k1gNnSc2	vykonání
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
ústní	ústní	k2eAgFnSc1d1	ústní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vykonali	vykonat	k5eAaPmAgMnP	vykonat
s	s	k7c7	s
výborným	výborný	k2eAgInSc7d1	výborný
prospěchem	prospěch	k1gInSc7	prospěch
závěrečnou	závěrečný	k2eAgFnSc4d1	závěrečná
zkoušku	zkouška	k1gFnSc4	zkouška
a	a	k8xC	a
celé	celý	k2eAgNnSc4d1	celé
studium	studium	k1gNnSc4	studium
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Program	program	k1gInSc1	program
činnosti	činnost	k1gFnSc2	činnost
fakulty	fakulta	k1gFnSc2	fakulta
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
dán	dát	k5eAaPmNgInS	dát
závěry	závěr	k1gInPc7	závěr
17	[number]	k4	17
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc2	sjezd
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
24	[number]	k4	24
profesorů	profesor	k1gMnPc2	profesor
<g/>
,	,	kIx,	,
40	[number]	k4	40
docentů	docent	k1gMnPc2	docent
<g/>
,	,	kIx,	,
100	[number]	k4	100
odborných	odborný	k2eAgMnPc2d1	odborný
asistentů	asistent	k1gMnPc2	asistent
<g/>
,	,	kIx,	,
5	[number]	k4	5
asistentů	asistent	k1gMnPc2	asistent
a	a	k8xC	a
80	[number]	k4	80
externích	externí	k2eAgMnPc2d1	externí
vyučujících	vyučující	k2eAgMnPc2d1	vyučující
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Byly	být	k5eAaImAgFnP	být
inovovány	inovovat	k5eAaBmNgInP	inovovat
světonázorové	světonázorový	k2eAgInPc1d1	světonázorový
projekty	projekt	k1gInPc1	projekt
kateder	katedra	k1gFnPc2	katedra
<g/>
"	"	kIx"	"
a	a	k8xC	a
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nové	nový	k2eAgInPc1d1	nový
učební	učební	k2eAgInPc1d1	učební
plány	plán	k1gInPc1	plán
<g/>
.	.	kIx.	.
</s>
<s>
Dvouoborové	dvouoborový	k2eAgFnPc1d1	dvouoborová
učitelské	učitelský	k2eAgFnPc1d1	učitelská
kombinace	kombinace	k1gFnPc1	kombinace
byly	být	k5eAaImAgFnP	být
chemie-fyzika	chemieyzika	k1gFnSc1	chemie-fyzika
<g/>
,	,	kIx,	,
fyzika-výpočetní	fyzikaýpočetní	k2eAgFnSc1d1	fyzika-výpočetní
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
matematika-biologie	matematikaiologie	k1gFnSc1	matematika-biologie
<g/>
,	,	kIx,	,
matematika-chemie	matematikahemie	k1gFnSc1	matematika-chemie
<g/>
,	,	kIx,	,
matematika-fyzika	matematikayzika	k1gFnSc1	matematika-fyzika
a	a	k8xC	a
matematika-zeměpis	matematikaeměpis	k1gFnSc1	matematika-zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
Odbornými	odborný	k2eAgInPc7d1	odborný
obory	obor	k1gInPc7	obor
byly	být	k5eAaImAgInP	být
matematická	matematický	k2eAgFnSc1d1	matematická
analýza	analýza	k1gFnSc1	analýza
<g/>
,	,	kIx,	,
teoretická	teoretický	k2eAgFnSc1d1	teoretická
kybernetika	kybernetika	k1gFnSc1	kybernetika
<g/>
,	,	kIx,	,
matematická	matematický	k2eAgFnSc1d1	matematická
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
teorie	teorie	k1gFnSc1	teorie
systémů	systém	k1gInPc2	systém
<g/>
,	,	kIx,	,
fyzika	fyzika	k1gFnSc1	fyzika
pevných	pevný	k2eAgFnPc2d1	pevná
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
mikroelektronika	mikroelektronika	k1gFnSc1	mikroelektronika
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
optika	optika	k1gFnSc1	optika
<g/>
,	,	kIx,	,
geochemie	geochemie	k1gFnSc1	geochemie
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
ložisková	ložiskový	k2eAgFnSc1d1	ložisková
geologie	geologie	k1gFnSc1	geologie
<g/>
,	,	kIx,	,
užitá	užitý	k2eAgFnSc1d1	užitá
geofyzika	geofyzika	k1gFnSc1	geofyzika
<g/>
,	,	kIx,	,
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
geologie	geologie	k1gFnSc1	geologie
a	a	k8xC	a
hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
geografie	geografie	k1gFnSc1	geografie
a	a	k8xC	a
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
organická	organický	k2eAgFnSc1d1	organická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
biochemie	biochemie	k1gFnSc1	biochemie
<g/>
,	,	kIx,	,
obecná	obecný	k2eAgFnSc1d1	obecná
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
speciální	speciální	k2eAgFnSc1d1	speciální
biologie	biologie	k1gFnSc1	biologie
a	a	k8xC	a
ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
a	a	k8xC	a
genetika	genetika	k1gFnSc1	genetika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
bylo	být	k5eAaImAgNnS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
299	[number]	k4	299
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
312	[number]	k4	312
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
studovalo	studovat	k5eAaImAgNnS	studovat
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
celkem	celkem	k6eAd1	celkem
1296	[number]	k4	1296
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Prioritními	prioritní	k2eAgInPc7d1	prioritní
obory	obor	k1gInPc7	obor
byly	být	k5eAaImAgFnP	být
biotechnologie	biotechnologie	k1gFnSc1	biotechnologie
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc1d1	nový
materiály	materiál	k1gInPc1	materiál
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
při	při	k7c6	při
zaměstnání	zaměstnání	k1gNnSc6	zaměstnání
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
8	[number]	k4	8
odborných	odborný	k2eAgInPc2d1	odborný
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
6	[number]	k4	6
učitelských	učitelský	k2eAgFnPc2d1	učitelská
kombinací	kombinace	k1gFnPc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
6	[number]	k4	6
odborných	odborný	k2eAgInPc6d1	odborný
oborech	obor	k1gInPc6	obor
a	a	k8xC	a
6	[number]	k4	6
učitelských	učitelský	k2eAgFnPc6d1	učitelská
kombinacích	kombinace	k1gFnPc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
z	z	k7c2	z
katedry	katedra	k1gFnSc2	katedra
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
matematiky	matematika	k1gFnSc2	matematika
výpočetní	výpočetní	k2eAgNnSc1d1	výpočetní
středisko	středisko	k1gNnSc1	středisko
a	a	k8xC	a
převedeno	převeden	k2eAgNnSc1d1	převedeno
pod	pod	k7c4	pod
nově	nově	k6eAd1	nově
zřízený	zřízený	k2eAgInSc4d1	zřízený
celouniverzitní	celouniverzitní	k2eAgInSc4d1	celouniverzitní
Ústav	ústav	k1gInSc4	ústav
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
učebna	učebna	k1gFnSc1	učebna
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
v	v	k7c6	v
září	září	k1gNnSc6	září
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
vybavená	vybavený	k2eAgFnSc1d1	vybavená
20	[number]	k4	20
ks	ks	kA	ks
mikropočítačů	mikropočítač	k1gInPc2	mikropočítač
IQ151	IQ151	k1gFnSc2	IQ151
a	a	k8xC	a
TNS	TNS	kA	TNS
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
katedře	katedra	k1gFnSc3	katedra
obecné	obecný	k2eAgFnSc3d1	obecná
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
byla	být	k5eAaImAgFnS	být
připojena	připojit	k5eAaPmNgFnS	připojit
laboratoř	laboratoř	k1gFnSc1	laboratoř
elektronové	elektronový	k2eAgFnSc2d1	elektronová
mikroskopie	mikroskopie	k1gFnSc2	mikroskopie
<g/>
,	,	kIx,	,
společná	společný	k2eAgFnSc1d1	společná
s	s	k7c7	s
ÚSEB	ÚSEB	kA	ÚSEB
ČSAV	ČSAV	kA	ČSAV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Katedry	katedra	k1gFnPc1	katedra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
matematická	matematický	k2eAgFnSc1d1	matematická
analýza	analýza	k1gFnSc1	analýza
</s>
</p>
<p>
<s>
algebra	algebra	k1gFnSc1	algebra
a	a	k8xC	a
geometrie	geometrie	k1gFnSc1	geometrie
</s>
</p>
<p>
<s>
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
matematika	matematika	k1gFnSc1	matematika
</s>
</p>
<p>
<s>
matematika	matematika	k1gFnSc1	matematika
(	(	kIx(	(
<g/>
od	od	k7c2	od
1.9	[number]	k4	1.9
<g/>
.1982	.1982	k4	.1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výuka	výuka	k1gFnSc1	výuka
vedena	vést	k5eAaImNgFnS	vést
i	i	k9	i
pro	pro	k7c4	pro
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
fakultu	fakulta	k1gFnSc4	fakulta
</s>
</p>
<p>
<s>
obecná	obecný	k2eAgFnSc1d1	obecná
fyzika	fyzika	k1gFnSc1	fyzika
(	(	kIx(	(
<g/>
od	od	k7c2	od
1.9	[number]	k4	1.9
<g/>
.1980	.1980	k4	.1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
katedra	katedra	k1gFnSc1	katedra
obecné	obecný	k2eAgFnSc2d1	obecná
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
didaktiky	didaktika	k1gFnSc2	didaktika
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
vyučující	vyučující	k2eAgMnPc1d1	vyučující
didaktiky	didaktika	k1gFnSc2	didaktika
přešli	přejít	k5eAaPmAgMnP	přejít
na	na	k7c4	na
pedagogickou	pedagogický	k2eAgFnSc4d1	pedagogická
fakultu	fakulta	k1gFnSc4	fakulta
</s>
</p>
<p>
<s>
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
elektronika	elektronika	k1gFnSc1	elektronika
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
odbor	odbor	k1gInSc1	odbor
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
vakuová	vakuový	k2eAgFnSc1d1	vakuová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
optika	optika	k1gFnSc1	optika
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
fyzika	fyzika	k1gFnSc1	fyzika
pevné	pevný	k2eAgFnSc2d1	pevná
fáze	fáze	k1gFnSc2	fáze
</s>
</p>
<p>
<s>
teoretická	teoretický	k2eAgFnSc1d1	teoretická
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
</s>
</p>
<p>
<s>
analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
</s>
</p>
<p>
<s>
teoretická	teoretický	k2eAgFnSc1d1	teoretická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
chemie	chemie	k1gFnSc1	chemie
</s>
</p>
<p>
<s>
anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
výuky	výuka	k1gFnSc2	výuka
jaderné	jaderný	k2eAgFnSc2d1	jaderná
chemie	chemie	k1gFnSc2	chemie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
organická	organický	k2eAgFnSc1d1	organická
chemie	chemie	k1gFnSc1	chemie
</s>
</p>
<p>
<s>
biochemie	biochemie	k1gFnSc1	biochemie
</s>
</p>
<p>
<s>
biologie	biologie	k1gFnSc1	biologie
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
se	s	k7c7	s
specializacemi	specializace	k1gFnPc7	specializace
cytologie	cytologie	k1gFnSc1	cytologie
<g/>
,	,	kIx,	,
anatomie	anatomie	k1gFnSc1	anatomie
a	a	k8xC	a
morfologie	morfologie	k1gFnSc1	morfologie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
fyziologie	fyziologie	k1gFnSc2	fyziologie
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
taxonomie	taxonomie	k1gFnSc1	taxonomie
a	a	k8xC	a
geobotanika	geobotanika	k1gFnSc1	geobotanika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
se	se	k3xPyFc4	se
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
dle	dle	k7c2	dle
plánu	plán	k1gInSc2	plán
rozvoje	rozvoj	k1gInSc2	rozvoj
dopravy	doprava	k1gFnSc2	doprava
vytipována	vytipován	k2eAgFnSc1d1	vytipována
jako	jako	k8xS	jako
vhodná	vhodný	k2eAgFnSc1d1	vhodná
lokalita	lokalita	k1gFnSc1	lokalita
pro	pro	k7c4	pro
stanici	stanice	k1gFnSc4	stanice
podzemní	podzemní	k2eAgFnSc2d1	podzemní
tramvaje	tramvaj	k1gFnSc2	tramvaj
</s>
</p>
<p>
<s>
biologie	biologie	k1gFnSc1	biologie
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
oddělení	oddělení	k1gNnSc1	oddělení
systematické	systematický	k2eAgFnSc2d1	systematická
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
,	,	kIx,	,
parazitologie	parazitologie	k1gFnSc2	parazitologie
<g/>
,	,	kIx,	,
hydrobiologie	hydrobiologie	k1gFnSc2	hydrobiologie
a	a	k8xC	a
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
1.9	[number]	k4	1.9
<g/>
.1983	.1983	k4	.1983
přešlo	přejít	k5eAaPmAgNnS	přejít
oddělení	oddělení	k1gNnSc1	oddělení
obecné	obecný	k2eAgFnSc2d1	obecná
zoologie	zoologie	k1gFnSc2	zoologie
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
katedru	katedra	k1gFnSc4	katedra
obecné	obecný	k2eAgFnSc2d1	obecná
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mikrobiologie	mikrobiologie	k1gFnSc1	mikrobiologie
a	a	k8xC	a
genetika	genetika	k1gFnSc1	genetika
(	(	kIx(	(
<g/>
od	od	k7c2	od
1.9	[number]	k4	1.9
<g/>
.1980	.1980	k4	.1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
katedry	katedra	k1gFnSc2	katedra
mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
<g/>
,	,	kIx,	,
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
přešlo	přejít	k5eAaPmAgNnS	přejít
pracoviště	pracoviště	k1gNnSc1	pracoviště
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
na	na	k7c4	na
katedru	katedra	k1gFnSc4	katedra
obecné	obecný	k2eAgFnSc2d1	obecná
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
</s>
</p>
<p>
<s>
obecné	obecná	k1gFnPc1	obecná
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
(	(	kIx(	(
<g/>
od	od	k7c2	od
1.7	[number]	k4	1.7
<g/>
.1983	.1983	k4	.1983
<g/>
)	)	kIx)	)
-	-	kIx~	-
z	z	k7c2	z
pracovišť	pracoviště	k1gNnPc2	pracoviště
obecné	obecný	k2eAgFnSc2d1	obecná
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
fyziologie	fyziologie	k1gFnSc2	fyziologie
živočichů	živočich	k1gMnPc2	živočich
</s>
</p>
<p>
<s>
ochrana	ochrana	k1gFnSc1	ochrana
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
od	od	k7c2	od
1.7	[number]	k4	1.7
<g/>
.1983	.1983	k4	.1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celouniverzitní	celouniverzitní	k2eAgNnSc1d1	celouniverzitní
koordinační	koordinační	k2eAgNnSc1d1	koordinační
centrum	centrum	k1gNnSc1	centrum
</s>
</p>
<p>
<s>
obecná	obecná	k1gFnSc1	obecná
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
</s>
</p>
<p>
<s>
geologie	geologie	k1gFnSc1	geologie
a	a	k8xC	a
paleontologie	paleontologie	k1gFnSc1	paleontologie
s	s	k7c7	s
obory	obor	k1gInPc7	obor
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
ložisková	ložiskový	k2eAgFnSc1d1	ložisková
geologie	geologie	k1gFnSc1	geologie
<g/>
,	,	kIx,	,
hydrogeologie	hydrogeologie	k1gFnSc1	hydrogeologie
<g/>
,	,	kIx,	,
inženýrská	inženýrský	k2eAgFnSc1d1	inženýrská
geologie	geologie	k1gFnSc1	geologie
a	a	k8xC	a
geofyzika	geofyzika	k1gFnSc1	geofyzika
</s>
</p>
<p>
<s>
mineralogie	mineralogie	k1gFnSc1	mineralogie
a	a	k8xC	a
petrografie	petrografie	k1gFnSc1	petrografie
</s>
</p>
<p>
<s>
geografie	geografie	k1gFnSc1	geografie
s	s	k7c7	s
obory	obor	k1gInPc4	obor
fyzická	fyzický	k2eAgFnSc1d1	fyzická
geografie	geografie	k1gFnSc1	geografie
<g/>
,	,	kIx,	,
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
regionální	regionální	k2eAgFnSc1d1	regionální
geografie	geografie	k1gFnSc1	geografie
</s>
</p>
<p>
<s>
==	==	k?	==
Organizační	organizační	k2eAgFnSc1d1	organizační
struktura	struktura	k1gFnSc1	struktura
fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
Základními	základní	k2eAgFnPc7d1	základní
organizačními	organizační	k2eAgFnPc7d1	organizační
jednotkami	jednotka	k1gFnPc7	jednotka
jsou	být	k5eAaImIp3nP	být
ústavy	ústav	k1gInPc1	ústav
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gMnSc1	vedoucí
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
)	)	kIx)	)
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
děkanát	děkanát	k1gInSc1	děkanát
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
pracoviště	pracoviště	k1gNnPc1	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
ústavu	ústav	k1gInSc2	ústav
je	být	k5eAaImIp3nS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
<g/>
,	,	kIx,	,
vyhlášeného	vyhlášený	k2eAgInSc2d1	vyhlášený
děkanem	děkan	k1gMnSc7	děkan
<g/>
,	,	kIx,	,
po	po	k7c4	po
vyjádření	vyjádření	k1gNnSc4	vyjádření
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
je	být	k5eAaImIp3nS	být
2-4	[number]	k4	2-4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
(	(	kIx(	(
<g/>
vede	vést	k5eAaImIp3nS	vést
mzdovou	mzdový	k2eAgFnSc4d1	mzdová
<g/>
,	,	kIx,	,
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
a	a	k8xC	a
účetní	účetní	k2eAgFnSc4d1	účetní
agendu	agenda	k1gFnSc4	agenda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
správním	správní	k2eAgNnSc6d1	správní
(	(	kIx(	(
<g/>
personální	personální	k2eAgFnSc1d1	personální
agenda	agenda	k1gFnSc1	agenda
<g/>
,	,	kIx,	,
agenda	agenda	k1gFnSc1	agenda
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
styků	styk	k1gInPc2	styk
<g/>
)	)	kIx)	)
a	a	k8xC	a
administrativním	administrativní	k2eAgMnSc6d1	administrativní
(	(	kIx(	(
<g/>
matrika	matrika	k1gFnSc1	matrika
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
evidence	evidence	k1gFnSc1	evidence
jejich	jejich	k3xOp3gNnSc2	jejich
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
útvarem	útvar	k1gInSc7	útvar
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
děkanát	děkanát	k1gInSc1	děkanát
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1	jiné
pracoviště	pracoviště	k1gNnPc1	pracoviště
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
specifické	specifický	k2eAgFnPc4d1	specifická
činnosti	činnost	k1gFnPc4	činnost
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
děkan	děkan	k1gMnSc1	děkan
a	a	k8xC	a
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
zřízení	zřízení	k1gNnSc4	zřízení
(	(	kIx(	(
<g/>
zrušení	zrušení	k1gNnSc4	zrušení
<g/>
)	)	kIx)	)
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
děkana	děkan	k1gMnSc2	děkan
senát	senát	k1gInSc4	senát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
zaměření	zaměření	k1gNnSc2	zaměření
různých	různý	k2eAgInPc2d1	různý
oborů	obor	k1gInPc2	obor
rozdělena	rozdělen	k2eAgFnSc1d1	rozdělena
na	na	k7c4	na
následující	následující	k2eAgInPc4d1	následující
ústavy	ústav	k1gInPc4	ústav
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
statistiky	statistika	k1gFnSc2	statistika
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
kondenzovaných	kondenzovaný	k2eAgFnPc2d1	kondenzovaná
látek	látka	k1gFnPc2	látka
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
elektroniky	elektronika	k1gFnSc2	elektronika
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
chemie	chemie	k1gFnSc2	chemie
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
biochemie	biochemie	k1gFnSc2	biochemie
</s>
</p>
<p>
<s>
Výzkumné	výzkumný	k2eAgNnSc1d1	výzkumné
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
chemii	chemie	k1gFnSc4	chemie
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
ekotoxikologii	ekotoxikologie	k1gFnSc4	ekotoxikologie
</s>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
biomolekul	biomolekula	k1gFnPc2	biomolekula
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
antropologie	antropologie	k1gFnSc2	antropologie
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
botaniky	botanika	k1gFnSc2	botanika
a	a	k8xC	a
zoologie	zoologie	k1gFnSc2	zoologie
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
experimentální	experimentální	k2eAgFnSc2d1	experimentální
biologie	biologie	k1gFnSc2	biologie
</s>
</p>
<p>
<s>
Geografický	geografický	k2eAgInSc1d1	geografický
ústav	ústav	k1gInSc1	ústav
</s>
</p>
<p>
<s>
Ústav	ústav	k1gInSc1	ústav
geologických	geologický	k2eAgFnPc2d1	geologická
věd	věda	k1gFnPc2	věda
</s>
</p>
<p>
<s>
==	==	k?	==
Studijní	studijní	k2eAgInPc1d1	studijní
obory	obor	k1gInPc1	obor
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
akademickém	akademický	k2eAgInSc6d1	akademický
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
programech	program	k1gInPc6	program
a	a	k8xC	a
oborech	obor	k1gInPc6	obor
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Orgány	orgán	k1gInPc1	orgán
fakulty	fakulta	k1gFnSc2	fakulta
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Senát	senát	k1gInSc1	senát
===	===	k?	===
</s>
</p>
<p>
<s>
Dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc1d1	tvořený
komorou	komora	k1gFnSc7	komora
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
15	[number]	k4	15
členů	člen	k1gInPc2	člen
a	a	k8xC	a
komorou	komora	k1gFnSc7	komora
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
12	[number]	k4	12
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
s	s	k7c7	s
nejvýše	nejvýše	k6eAd1	nejvýše
tříletým	tříletý	k2eAgNnSc7d1	tříleté
funkčním	funkční	k2eAgNnSc7d1	funkční
obdobím	období	k1gNnSc7	období
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
samosprávným	samosprávný	k2eAgMnSc7d1	samosprávný
zastupitelským	zastupitelský	k2eAgInSc7d1	zastupitelský
akademickým	akademický	k2eAgInSc7d1	akademický
orgánem	orgán	k1gInSc7	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
akademických	akademický	k2eAgMnPc2d1	akademický
pracovníků	pracovník	k1gMnPc2	pracovník
je	být	k5eAaImIp3nS	být
rozložena	rozložen	k2eAgFnSc1d1	rozložena
do	do	k7c2	do
pěti	pět	k4xCc2	pět
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
rovnoměrné	rovnoměrný	k2eAgNnSc1d1	rovnoměrné
zastoupení	zastoupení	k1gNnSc1	zastoupení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Děkan	děkan	k1gMnSc1	děkan
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
fakulty	fakulta	k1gFnSc2	fakulta
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
přímo	přímo	k6eAd1	přímo
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
rektorovi	rektor	k1gMnSc6	rektor
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ho	on	k3xPp3gInSc4	on
také	také	k9	také
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
stejná	stejný	k2eAgFnSc1d1	stejná
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
nejvýše	nejvýše	k6eAd1	nejvýše
dvě	dva	k4xCgFnPc4	dva
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
bezprostředně	bezprostředně	k6eAd1	bezprostředně
jdoucí	jdoucí	k2eAgNnSc1d1	jdoucí
funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
po	po	k7c6	po
vyjádření	vyjádření	k1gNnSc6	vyjádření
senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
proděkany	proděkan	k1gMnPc4	proděkan
<g/>
,	,	kIx,	,
po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
radou	rada	k1gFnSc7	rada
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
členy	člen	k1gInPc4	člen
zkušebních	zkušební	k2eAgFnPc2d1	zkušební
komisí	komise	k1gFnPc2	komise
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
projednání	projednání	k1gNnSc4	projednání
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
a	a	k8xC	a
radou	rada	k1gFnSc7	rada
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
ředitele	ředitel	k1gMnPc4	ředitel
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
ústavů	ústav	k1gInPc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
předsedá	předsedat	k5eAaImIp3nS	předsedat
vědecké	vědecký	k2eAgFnSc3d1	vědecká
radě	rada	k1gFnSc3	rada
<g/>
.	.	kIx.	.
</s>
<s>
Poradním	poradní	k2eAgInSc7d1	poradní
orgánem	orgán	k1gInSc7	orgán
děkana	děkan	k1gMnSc2	děkan
je	být	k5eAaImIp3nS	být
kolegium	kolegium	k1gNnSc1	kolegium
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
další	další	k2eAgInPc1d1	další
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
může	moct	k5eAaImIp3nS	moct
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
děkan	děkan	k1gMnSc1	děkan
zřídit	zřídit	k5eAaPmF	zřídit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
===	===	k?	===
</s>
</p>
<p>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
členové	člen	k1gMnPc1	člen
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejméně	málo	k6eAd3	málo
třetina	třetina	k1gFnSc1	třetina
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
odborníků	odborník	k1gMnPc2	odborník
mimo	mimo	k7c4	mimo
akademickou	akademický	k2eAgFnSc4d1	akademická
obec	obec	k1gFnSc4	obec
MU	MU	kA	MU
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
či	či	k8xC	či
odvoláváni	odvolávat	k5eAaImNgMnP	odvolávat
děkanem	děkan	k1gMnSc7	děkan
po	po	k7c4	po
schválení	schválení	k1gNnSc4	schválení
senátem	senát	k1gInSc7	senát
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
Vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
jsou	být	k5eAaImIp3nP	být
významní	významný	k2eAgMnPc1d1	významný
představitelé	představitel	k1gMnPc1	představitel
všech	všecek	k3xTgInPc2	všecek
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
fakulta	fakulta	k1gFnSc1	fakulta
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
a	a	k8xC	a
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
nebo	nebo	k8xC	nebo
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Disciplinární	disciplinární	k2eAgFnSc1d1	disciplinární
komise	komise	k1gFnSc1	komise
===	===	k?	===
</s>
</p>
<p>
<s>
Šestičlenná	šestičlenný	k2eAgFnSc1d1	šestičlenná
komise	komise	k1gFnSc1	komise
(	(	kIx(	(
<g/>
tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
jsou	být	k5eAaImIp3nP	být
studenti	student	k1gMnPc1	student
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
na	na	k7c4	na
nejvýše	nejvýše	k6eAd1	nejvýše
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
členů	člen	k1gMnPc2	člen
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tajemník	tajemník	k1gMnSc1	tajemník
===	===	k?	===
</s>
</p>
<p>
<s>
Děkan	děkan	k1gMnSc1	děkan
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
(	(	kIx(	(
<g/>
a	a	k8xC	a
odvolává	odvolávat	k5eAaImIp3nS	odvolávat
<g/>
)	)	kIx)	)
tajemníka	tajemník	k1gMnSc2	tajemník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
přímo	přímo	k6eAd1	přímo
podřízen	podřídit	k5eAaPmNgMnS	podřídit
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
mu	on	k3xPp3gMnSc3	on
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výběrového	výběrový	k2eAgNnSc2d1	výběrové
řízení	řízení	k1gNnSc2	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Činností	činnost	k1gFnSc7	činnost
tajemníka	tajemník	k1gMnSc2	tajemník
je	být	k5eAaImIp3nS	být
řízení	řízení	k1gNnSc4	řízení
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
hospodaření	hospodaření	k1gNnSc2	hospodaření
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Insignie	insignie	k1gFnPc4	insignie
==	==	k?	==
</s>
</p>
<p>
<s>
Děkanský	děkanský	k2eAgInSc1d1	děkanský
řetěz	řetěz	k1gInSc1	řetěz
s	s	k7c7	s
medailí	medaile	k1gFnSc7	medaile
<g/>
,	,	kIx,	,
raženou	ražený	k2eAgFnSc7d1	ražená
v	v	k7c6	v
kremnické	kremnický	k2eAgFnSc6d1	Kremnická
mincovně	mincovna	k1gFnSc6	mincovna
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lícovou	lícový	k2eAgFnSc4d1	lícová
stranu	strana	k1gFnSc4	strana
medaile	medaile	k1gFnSc2	medaile
autora	autor	k1gMnSc2	autor
Otakara	Otakar	k1gMnSc2	Otakar
Španiela	Španiel	k1gMnSc2	Španiel
bylo	být	k5eAaImAgNnS	být
vybráno	vybrat	k5eAaPmNgNnS	vybrat
poprsí	poprsí	k1gNnSc1	poprsí
Řehoře	Řehoř	k1gMnSc2	Řehoř
Jana	Jan	k1gMnSc2	Jan
Mendela	Mendel	k1gMnSc2	Mendel
<g/>
,	,	kIx,	,
na	na	k7c6	na
reverzu	reverz	k1gInSc6	reverz
je	být	k5eAaImIp3nS	být
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
IN	IN	kA	IN
NATURA	NATURA	kA	NATURA
SPES	SPES	kA	SPES
ET	ET	kA	ET
VERITAS	VERITAS	kA	VERITAS
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
je	být	k5eAaImIp3nS	být
naděje	naděje	k1gFnSc1	naděje
a	a	k8xC	a
pravda	pravda	k1gFnSc1	pravda
<g/>
)	)	kIx)	)
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
je	být	k5eAaImIp3nS	být
lipová	lipový	k2eAgFnSc1d1	Lipová
ratolest	ratolest	k1gFnSc1	ratolest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
polovině	polovina	k1gFnSc6	polovina
je	být	k5eAaImIp3nS	být
slunce	slunce	k1gNnSc1	slunce
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
hvězdami	hvězda	k1gFnPc7	hvězda
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavice	hlavice	k1gFnSc1	hlavice
žezla	žezlo	k1gNnSc2	žezlo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
111	[number]	k4	111
cm	cm	kA	cm
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
zlatá	zlatý	k2eAgFnSc1d1	zlatá
koule	koule	k1gFnSc1	koule
s	s	k7c7	s
paprsky	paprsek	k1gInPc7	paprsek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
kometa	kometa	k1gFnSc1	kometa
s	s	k7c7	s
ohonem	ohon	k1gInSc7	ohon
mířícím	mířící	k2eAgInSc7d1	mířící
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prstenci	prstenec	k1gInSc6	prstenec
je	být	k5eAaImIp3nS	být
zobrazen	zobrazen	k2eAgInSc4d1	zobrazen
měsíc	měsíc	k1gInSc4	měsíc
s	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Dřík	dřík	k1gInSc1	dřík
žezla	žezlo	k1gNnSc2	žezlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
autory	autor	k1gMnPc7	autor
jsou	být	k5eAaImIp3nP	být
Václav	Václav	k1gMnSc1	Václav
Rada	Rada	k1gMnSc1	Rada
a	a	k8xC	a
Ladislav	Ladislav	k1gMnSc1	Ladislav
Bartončík	Bartončík	k1gMnSc1	Bartončík
<g/>
,	,	kIx,	,
zdobí	zdobit	k5eAaImIp3nP	zdobit
vltavíny	vltavín	k1gInPc4	vltavín
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
slavnostnímu	slavnostní	k2eAgNnSc3d1	slavnostní
předání	předání	k1gNnSc3	předání
insignií	insignie	k1gFnPc2	insignie
došlo	dojít	k5eAaPmAgNnS	dojít
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
společně	společně	k6eAd1	společně
s	s	k7c7	s
insigniemi	insignie	k1gFnPc7	insignie
dalších	další	k2eAgFnPc2d1	další
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
rektorátu	rektorát	k1gInSc2	rektorát
v	v	k7c6	v
trezorech	trezor	k1gInPc6	trezor
Hypoteční	hypoteční	k2eAgFnSc2d1	hypoteční
a	a	k8xC	a
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
banky	banka	k1gFnSc2	banka
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Symbol	symbol	k1gInSc1	symbol
fakulty	fakulta	k1gFnSc2	fakulta
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
šroubovice	šroubovice	k1gFnSc1	šroubovice
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
Y	Y	kA	Y
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
replikaci	replikace	k1gFnSc6	replikace
</s>
</p>
<p>
<s>
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
kružnici	kružnice	k1gFnSc4	kružnice
zdobí	zdobit	k5eAaImIp3nS	zdobit
kruhový	kruhový	k2eAgInSc1d1	kruhový
nápis	nápis	k1gInSc1	nápis
"	"	kIx"	"
<g/>
SCIENTIA	SCIENTIA	kA	SCIENTIA
EST	Est	k1gMnSc1	Est
POTENTIA	POTENTIA	kA	POTENTIA
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Poznání	poznání	k1gNnSc1	poznání
je	být	k5eAaImIp3nS	být
síla	síla	k1gFnSc1	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc4d1	dolní
a	a	k8xC	a
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
vnější	vnější	k2eAgFnSc2d1	vnější
kružnice	kružnice	k1gFnSc2	kružnice
tvoří	tvořit	k5eAaImIp3nP	tvořit
nápisy	nápis	k1gInPc1	nápis
"	"	kIx"	"
<g/>
UNIVERSITAS	UNIVERSITAS	kA	UNIVERSITAS
MASARYKIANA	MASARYKIANA	kA	MASARYKIANA
BRUNENSIS	BRUNENSIS	kA	BRUNENSIS
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
FACULTAS	FACULTAS	kA	FACULTAS
RERUM	RERUM	kA	RERUM
NATURALIUM	NATURALIUM	kA	NATURALIUM
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Logo	logo	k1gNnSc1	logo
a	a	k8xC	a
barva	barva	k1gFnSc1	barva
===	===	k?	===
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
fakulty	fakulta	k1gFnSc2	fakulta
je	být	k5eAaImIp3nS	být
zelená	zelený	k2eAgFnSc1d1	zelená
(	(	kIx(	(
<g/>
Pantone	Panton	k1gInSc5	Panton
354	[number]	k4	354
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kulturní	kulturní	k2eAgInPc1d1	kulturní
ohlasy	ohlas	k1gInPc1	ohlas
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
právě	právě	k9	právě
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Radek	Radek	k1gMnSc1	Radek
Zelenka	Zelenka	k1gMnSc1	Zelenka
–	–	k?	–
hrdina	hrdina	k1gMnSc1	hrdina
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
(	(	kIx(	(
<g/>
ztvárněný	ztvárněný	k2eAgInSc1d1	ztvárněný
Čecho-Kanaďanem	Čecho-Kanaďan	k1gMnSc7	Čecho-Kanaďan
Davidem	David	k1gMnSc7	David
Nyklem	Nykl	k1gMnSc7	Nykl
<g/>
)	)	kIx)	)
–	–	k?	–
předtím	předtím	k6eAd1	předtím
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
expedice	expedice	k1gFnSc2	expedice
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
desátém	desátý	k4xOgInSc6	desátý
díle	dílo	k1gNnSc6	dílo
třetí	třetí	k4xOgFnSc2	třetí
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
po	po	k7c6	po
znovuobsazení	znovuobsazení	k1gNnSc6	znovuobsazení
Atlantidy	Atlantida	k1gFnSc2	Atlantida
Lanteany	Lanteana	k1gFnSc2	Lanteana
<g/>
,	,	kIx,	,
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
a	a	k8xC	a
přijetí	přijetí	k1gNnSc4	přijetí
nabízeného	nabízený	k2eAgNnSc2d1	nabízené
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
MU	MU	kA	MU
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
</s>
</p>
<p>
<s>
Aktuality	aktualita	k1gFnPc1	aktualita
z	z	k7c2	z
přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c6	na
zpravodajském	zpravodajský	k2eAgInSc6d1	zpravodajský
portálu	portál	k1gInSc6	portál
MU	MU	kA	MU
</s>
</p>
