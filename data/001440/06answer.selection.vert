<s>
Sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gFnSc1	Isaac
Newton	Newton	k1gMnSc1	Newton
(	(	kIx(	(
<g/>
[	[	kIx(	[
<g/>
ˌ	ˌ	k?	ˌ
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1643	[number]	k4	1643
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1727	[number]	k4	1727
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
(	(	kIx(	(
<g/>
profesor	profesor	k1gMnSc1	profesor
naturální	naturální	k2eAgFnSc2d1	naturální
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
alchymista	alchymista	k1gMnSc1	alchymista
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
