<p>
<s>
Široké	Široké	k2eAgNnSc1d1	Široké
okno	okno	k1gNnSc1	okno
je	být	k5eAaImIp3nS	být
dětská	dětský	k2eAgFnSc1d1	dětská
novela	novela	k1gFnSc1	novela
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
Řadě	řada	k1gFnSc6	řada
nešťastných	šťastný	k2eNgFnPc2d1	nešťastná
příhod	příhoda	k1gFnPc2	příhoda
od	od	k7c2	od
Lemonyho	Lemony	k1gMnSc2	Lemony
Snicketa	Snicket	k1gMnSc2	Snicket
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Sirotci	Sirotek	k1gMnPc1	Sirotek
přijíždějí	přijíždět	k5eAaImIp3nP	přijíždět
k	k	k7c3	k
slzavému	slzavý	k2eAgNnSc3d1	slzavé
jezeru	jezero	k1gNnSc3	jezero
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
jejich	jejich	k3xOp3gFnSc1	jejich
další	další	k2eAgFnSc1d1	další
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
příbuzná	příbuzná	k1gFnSc1	příbuzná
a	a	k8xC	a
opatrovnice	opatrovnice	k1gFnSc1	opatrovnice
–	–	k?	–
teta	teta	k1gFnSc1	teta
Josephine	Josephin	k1gMnSc5	Josephin
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
věčně	věčně	k6eAd1	věčně
z	z	k7c2	z
něčeho	něco	k3yInSc2	něco
strach	strach	k1gInSc1	strach
–	–	k?	–
neohřívá	ohřívat	k5eNaImIp3nS	ohřívat
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
vyhořet	vyhořet	k5eAaPmF	vyhořet
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
nechává	nechávat	k5eAaImIp3nS	nechávat
dveře	dveře	k1gFnPc4	dveře
pootevřené	pootevřený	k2eAgFnPc4d1	pootevřená
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
kliky	klika	k1gFnSc2	klika
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vyletět	vyletět	k5eAaPmF	vyletět
smrtící	smrtící	k2eAgFnSc1d1	smrtící
pružinka	pružinka	k1gFnSc1	pružinka
atd.	atd.	kA	atd.
Ironicky	ironicky	k6eAd1	ironicky
přitom	přitom	k6eAd1	přitom
bydlí	bydlet	k5eAaImIp3nS	bydlet
na	na	k7c6	na
útesu	útes	k1gInSc6	útes
ve	v	k7c6	v
vratkém	vratký	k2eAgInSc6d1	vratký
domku	domek	k1gInSc6	domek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
spadnutí	spadnutí	k1gNnSc4	spadnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
má	můj	k3xOp1gFnSc1	můj
teta	teta	k1gFnSc1	teta
věčně	věčně	k6eAd1	věčně
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
pravé	pravý	k2eAgNnSc1d1	pravé
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
nepozná	poznat	k5eNaPmIp3nS	poznat
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
přicestuje	přicestovat	k5eAaPmIp3nS	přicestovat
přestrojený	přestrojený	k2eAgMnSc1d1	přestrojený
hrabě	hrabě	k1gMnSc1	hrabě
Olaf	Olaf	k1gMnSc1	Olaf
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
jej	on	k3xPp3gInSc4	on
za	za	k7c4	za
námořníka	námořník	k1gMnSc4	námořník
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
jí	on	k3xPp3gFnSc7	on
mohl	moct	k5eAaImAgMnS	moct
nahradit	nahradit	k5eAaPmF	nahradit
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
manžela	manžel	k1gMnSc4	manžel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přijíždějící	přijíždějící	k2eAgMnSc1d1	přijíždějící
pan	pan	k1gMnSc1	pan
Poe	Poe	k1gMnSc1	Poe
samozřejmě	samozřejmě	k6eAd1	samozřejmě
dětím	dětí	k1gMnSc7	dětí
pomoci	pomoc	k1gFnSc2	pomoc
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
věci	věc	k1gFnPc1	věc
ještě	ještě	k6eAd1	ještě
zhorší	zhoršit	k5eAaPmIp3nP	zhoršit
<g/>
.	.	kIx.	.
</s>
</p>
