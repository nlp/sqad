<s>
Bernard	Bernard	k1gMnSc1
Stern	sternum	k1gNnPc2
</s>
<s>
Bernard	Bernard	k1gMnSc1
Stern	sternum	k1gNnPc2
Bernard	Bernard	k1gMnSc1
Stern	sternum	k1gNnPc2
</s>
<s>
Starosta	Starosta	k1gMnSc1
Bučače	Bučač	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1876	#num#	k4
–	–	k?
???	???	k?
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1911	#num#	k4
–	–	k?
1918	#num#	k4
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Sejmu	sejmout	k5eAaPmIp1nS
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1919	#num#	k4
–	–	k?
1920	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
(	(	kIx(
<g/>
Polský	polský	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
Klub	klub	k1gInSc1
Pracy	Praca	k1gMnSc2
Konstytucyjnej	Konstytucyjnej	k1gMnSc2
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1848	#num#	k4
BučačRakouské	BučačRakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
VaršavaPolsko	VaršavaPolsko	k1gNnSc4
Polsko	Polsko	k1gNnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bernard	Bernard	k1gMnSc1
Stern	Stern	k1gMnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1848	#num#	k4
Bučač	Bučač	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
–	–	k?
6	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
Varšava	Varšava	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
politik	politik	k1gMnSc1
polské	polský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
z	z	k7c2
Haliče	Halič	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
poslanec	poslanec	k1gMnSc1
polského	polský	k2eAgInSc2d1
Sejmu	Sejm	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS
z	z	k7c2
židovské	židovský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřil	patřit	k5eAaImAgMnS
mezi	mezi	k7c4
stoupence	stoupenec	k1gMnPc4
asimilačního	asimilační	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
prosazoval	prosazovat	k5eAaImAgInS
národní	národní	k2eAgFnSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgFnSc4d1
polonizaci	polonizace	k1gFnSc4
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystudoval	vystudovat	k5eAaPmAgMnS
gymnázium	gymnázium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1876	#num#	k4
byl	být	k5eAaImAgMnS
starostou	starosta	k1gMnSc7
rodného	rodný	k2eAgInSc2d1
Bučače	Bučač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastnil	vlastnit	k5eAaImAgMnS
pivovar	pivovar	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
působení	působení	k1gNnSc2
v	v	k7c6
parlamentu	parlament	k1gInSc6
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
majitel	majitel	k1gMnSc1
pivovaru	pivovar	k1gInSc2
v	v	k7c6
Bučači	Bučač	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Byl	být	k5eAaImAgMnS
místopředsedou	místopředseda	k1gMnSc7
školní	školní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
v	v	k7c6
Bučači	Bučač	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Působil	působit	k5eAaImAgMnS
také	také	k9
coby	coby	k?
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
celostátního	celostátní	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kam	kam	k6eAd1
usedl	usednout	k5eAaPmAgMnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
roku	rok	k1gInSc2
1911	#num#	k4
<g/>
,	,	kIx,
konaných	konaný	k2eAgFnPc2d1
podle	podle	k7c2
všeobecného	všeobecný	k2eAgNnSc2d1
a	a	k8xC
rovného	rovný	k2eAgNnSc2d1
volebního	volební	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c4
obvod	obvod	k1gInSc4
Halič	Halič	k1gFnSc4
32	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
polský	polský	k2eAgMnSc1d1
konzervativec	konzervativec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1911	#num#	k4
byl	být	k5eAaImAgMnS
na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
členem	člen	k1gMnSc7
poslaneckého	poslanecký	k2eAgInSc2d1
Polského	polský	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1915	#num#	k4
tisk	tisk	k1gInSc1
uveřejnil	uveřejnit	k5eAaPmAgInS
zprávu	zpráva	k1gFnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
poslanec	poslanec	k1gMnSc1
Stern	sternum	k1gNnPc2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
On	on	k3xPp3gMnSc1
sám	sám	k3xTgMnSc1
to	ten	k3xDgNnSc4
ale	ale	k9
v	v	k7c6
září	září	k1gNnSc6
1915	#num#	k4
dementoval	dementovat	k5eAaBmAgInS
a	a	k8xC
uvedl	uvést	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Bučače	Bučač	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
zpětně	zpětně	k6eAd1
dobyt	dobýt	k5eAaPmNgInS
rakousko-uherskými	rakousko-uherský	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Veřejně	veřejně	k6eAd1
a	a	k8xC
politicky	politicky	k6eAd1
činným	činný	k2eAgMnSc7d1
zůstal	zůstat	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
meziválečném	meziválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1919	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
1920	#num#	k4
byl	být	k5eAaImAgMnS
poslancem	poslanec	k1gMnSc7
polského	polský	k2eAgInSc2d1
ústavodárného	ústavodárný	k2eAgInSc2d1
Sejmu	Sejm	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zastupoval	zastupovat	k5eAaImAgInS
poslanecký	poslanecký	k2eAgInSc1d1
Klub	klub	k1gInSc1
Pracy	Praca	k1gMnSc2
Konstytucyjnej	Konstytucyjnej	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bernhard	Bernhard	k1gInSc1
Stern	sternum	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
parlament	parlament	k1gInSc1
<g/>
.	.	kIx.
<g/>
gv	gv	k?
<g/>
.	.	kIx.
<g/>
at	at	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Stern	sternum	k1gNnPc2
Bernard	Bernard	k1gMnSc1
1848-1920	1848-1920	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sejm	sejm	k1gInSc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
pl	pl	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Brzoza	Brzoz	k1gMnSc2
<g/>
,	,	kIx,
Czesław	Czesław	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Żydowska	Żydowska	k1gFnSc1
mozaika	mozaika	k1gFnSc1
polityczna	polityczna	k1gFnSc1
w	w	k?
Polsce	Polska	k1gFnSc6
1917	#num#	k4
<g/>
-	-	kIx~
<g/>
1927	#num#	k4
<g/>
:	:	kIx,
wybór	wybór	k1gInSc1
dokumentów	dokumentów	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Księgarnia	Księgarnium	k1gNnSc2
Akademicka	Akademicko	k1gNnSc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
291	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788371886089	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
240	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stern	sternum	k1gNnPc2
<g/>
,	,	kIx,
Bernard	Bernard	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
baza-nazwisk	baza-nazwisk	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bernard	Bernard	k1gMnSc1
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WITOS	WITOS	kA
<g/>
,	,	kIx,
Wincenty	Wincent	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dzieła	Dzieł	k1gInSc2
wybrane	wybran	k1gInSc5
<g/>
:	:	kIx,
Moje	můj	k3xOp1gNnPc4
wspomnienia	wspomnienium	k1gNnPc4
<g/>
,	,	kIx,
część	część	k?
I.	I.	kA
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Ludowa	Ludowa	k1gFnSc1
Spółdzielnia	Spółdzielnium	k1gNnSc2
Wydawnicza	Wydawnicza	k1gFnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
482	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86903	#num#	k4
<g/>
-	-	kIx~
<g/>
53	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
458	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Databáze	databáze	k1gFnSc1
stenografických	stenografický	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
a	a	k8xC
rejstříků	rejstřík	k1gInPc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
z	z	k7c2
příslušných	příslušný	k2eAgNnPc2d1
volebních	volební	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
,	,	kIx,
http://alex.onb.ac.at/spa.htm.	http://alex.onb.ac.at/spa.htm.	k?
<g/>
↑	↑	k?
Neue	Neue	k1gFnSc1
Freie	Freie	k1gFnSc1
Presse	Presse	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
1917	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Z	z	k7c2
Vídně	Vídeň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Září	září	k1gNnSc1
1915	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
55	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
262	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
|	|	kIx~
Polsko	Polsko	k1gNnSc1
</s>
