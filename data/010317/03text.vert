<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
nebo	nebo	k8xC	nebo
jižní	jižní	k2eAgFnSc1d1	jižní
hemisféra	hemisféra	k1gFnSc1	hemisféra
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
označující	označující	k2eAgInSc1d1	označující
část	část	k1gFnSc4	část
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
°	°	k?	°
s.	s.	k?	s.
<g/>
/	/	kIx~	/
<g/>
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
až	až	k9	až
k	k	k7c3	k
jižnímu	jižní	k2eAgInSc3d1	jižní
pólu	pól	k1gInSc3	pól
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
j.	j.	k?	j.
z.	z.	k?	z.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
čtyři	čtyři	k4xCgInPc4	čtyři
kontinenty	kontinent	k1gInPc4	kontinent
(	(	kIx(	(
<g/>
Antarktida	Antarktida	k1gFnSc1	Antarktida
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
část	část	k1gFnSc1	část
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
a	a	k8xC	a
čtyři	čtyři	k4xCgInPc4	čtyři
oceány	oceán	k1gInPc4	oceán
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
Indický	indický	k2eAgInSc1d1	indický
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
Jižní	jižní	k2eAgInSc1d1	jižní
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
leží	ležet	k5eAaImIp3nS	ležet
též	též	k9	též
několik	několik	k4yIc4	několik
asijských	asijský	k2eAgInPc2d1	asijský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
sklonu	sklon	k1gInSc3	sklon
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
ekliptice	ekliptika	k1gFnSc6	ekliptika
začíná	začínat	k5eAaImIp3nS	začínat
léto	léto	k1gNnSc1	léto
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
začíná	začínat	k5eAaImIp3nS	začínat
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnPc4	geografie
jižní	jižní	k2eAgFnSc2d1	jižní
polokoule	polokoule	k1gFnSc2	polokoule
==	==	k?	==
</s>
</p>
<p>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
bývá	bývat	k5eAaImIp3nS	bývat
obecně	obecně	k6eAd1	obecně
mírnější	mírný	k2eAgFnSc1d2	mírnější
než	než	k8xS	než
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
kromě	kromě	k7c2	kromě
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
studenější	studený	k2eAgFnSc1d2	studenější
než	než	k8xS	než
Arktida	Arktida	k1gFnSc1	Arktida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
větší	veliký	k2eAgFnSc7d2	veliký
převahou	převaha	k1gFnSc7	převaha
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
než	než	k8xS	než
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
a	a	k8xC	a
ochlazuje	ochlazovat	k5eAaImIp3nS	ochlazovat
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polokoule	polokoule	k1gFnSc1	polokoule
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
méně	málo	k6eAd2	málo
znečištěná	znečištěný	k2eAgFnSc1d1	znečištěná
než	než	k8xS	než
severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
menší	malý	k2eAgFnSc3d2	menší
hustotě	hustota	k1gFnSc3	hustota
osídlení	osídlení	k1gNnSc2	osídlení
(	(	kIx(	(
<g/>
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
žije	žít	k5eAaImIp3nS	žít
celkově	celkově	k6eAd1	celkově
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
%	%	kIx~	%
lidské	lidský	k2eAgFnSc2d1	lidská
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc3d2	menší
úrovni	úroveň	k1gFnSc3	úroveň
industrializace	industrializace	k1gFnSc2	industrializace
a	a	k8xC	a
menší	malý	k2eAgFnSc3d2	menší
ploše	plocha	k1gFnSc3	plocha
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
převažuje	převažovat	k5eAaImIp3nS	převažovat
západní	západní	k2eAgNnSc1d1	západní
proudění	proudění	k1gNnSc1	proudění
větru	vítr	k1gInSc2	vítr
a	a	k8xC	a
znečištění	znečištění	k1gNnSc2	znečištění
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
nemůže	moct	k5eNaImIp3nS	moct
šířit	šířit	k5eAaImF	šířit
jižně	jižně	k6eAd1	jižně
nebo	nebo	k8xC	nebo
severně	severně	k6eAd1	severně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
je	být	k5eAaImIp3nS	být
orientován	orientován	k2eAgInSc4d1	orientován
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jasnějším	jasný	k2eAgNnSc7d2	jasnější
nebem	nebe	k1gNnSc7	nebe
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
krásnou	krásný	k2eAgFnSc4d1	krásná
noční	noční	k2eAgFnSc4d1	noční
oblohu	obloha	k1gFnSc4	obloha
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
hvězd	hvězda	k1gFnPc2	hvězda
než	než	k8xS	než
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
kontinentů	kontinent	k1gInPc2	kontinent
a	a	k8xC	a
států	stát	k1gInPc2	stát
ležících	ležící	k2eAgInPc2d1	ležící
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kontinenty	kontinent	k1gInPc1	kontinent
===	===	k?	===
</s>
</p>
<p>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
-	-	kIx~	-
hranici	hranice	k1gFnSc6	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Libreville	Libreville	k1gFnSc2	Libreville
v	v	k7c6	v
Gabonu	Gabon	k1gInSc6	Gabon
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jih	jih	k1gInSc1	jih
Somálska	Somálsko	k1gNnSc2	Somálsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Amerika	Amerika	k1gFnSc1	Amerika
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
-	-	kIx~	-
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
ústí	ústí	k1gNnSc1	ústí
Amazonky	Amazonka	k1gFnSc2	Amazonka
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
město	město	k1gNnSc1	město
Quito	Quit	k2eAgNnSc1d1	Quito
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Africké	africký	k2eAgInPc1d1	africký
státy	stát	k1gInPc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Celé	celá	k1gFnPc1	celá
územíAngola	územíAngola	k1gFnSc1	územíAngola
</s>
</p>
<p>
<s>
Botswana	Botswana	k1gFnSc1	Botswana
</s>
</p>
<p>
<s>
Burundi	Burundi	k1gNnSc1	Burundi
</s>
</p>
<p>
<s>
Komory	komora	k1gFnPc1	komora
</s>
</p>
<p>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Lesotho	Lesotze	k6eAd1	Lesotze
</s>
</p>
<p>
<s>
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
</s>
</p>
<p>
<s>
Malawi	Malawi	k1gNnSc1	Malawi
</s>
</p>
<p>
<s>
Mosambik	Mosambik	k1gInSc1	Mosambik
</s>
</p>
<p>
<s>
Namibie	Namibie	k1gFnSc1	Namibie
</s>
</p>
<p>
<s>
Rwanda	Rwanda	k1gFnSc1	Rwanda
</s>
</p>
<p>
<s>
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
</s>
</p>
<p>
<s>
Tanzanie	Tanzanie	k1gFnSc1	Tanzanie
</s>
</p>
<p>
<s>
Zambie	Zambie	k1gFnSc1	Zambie
</s>
</p>
<p>
<s>
ZimbabweVětšinaDemokratická	ZimbabweVětšinaDemokratický	k2eAgFnSc1d1	ZimbabweVětšinaDemokratický
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
</s>
</p>
<p>
<s>
Gabon	Gabon	k1gMnSc1	Gabon
</s>
</p>
<p>
<s>
Republika	republika	k1gFnSc1	republika
KongoČástiKeňa	KongoČástiKeň	k1gInSc2	KongoČástiKeň
</s>
</p>
<p>
<s>
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Somálsko	Somálsko	k1gNnSc1	Somálsko
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
</s>
</p>
<p>
<s>
Uganda	Uganda	k1gFnSc1	Uganda
</s>
</p>
<p>
<s>
===	===	k?	===
Asijské	asijský	k2eAgInPc1d1	asijský
státy	stát	k1gInPc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Asijské	asijský	k2eAgFnPc1d1	asijská
země	zem	k1gFnPc1	zem
ležící	ležící	k2eAgFnPc1d1	ležící
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
souvislé	souvislý	k2eAgFnSc2d1	souvislá
země	zem	k1gFnSc2	zem
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celé	celá	k1gFnPc1	celá
územíVýchodní	územíVýchodní	k2eAgFnSc2d1	územíVýchodní
TimorVětšinaIndonésie	TimorVětšinaIndonésie	k1gFnSc2	TimorVětšinaIndonésie
</s>
</p>
<p>
<s>
===	===	k?	===
Státy	stát	k1gInPc1	stát
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Celé	celá	k1gFnPc1	celá
územíMadagaskar	územíMadagaskara	k1gFnPc2	územíMadagaskara
</s>
</p>
<p>
<s>
Mauricius	Mauricius	k1gMnSc1	Mauricius
</s>
</p>
<p>
<s>
SeychelyČástiMaledivy	SeychelyČástiMalediva	k1gFnPc1	SeychelyČástiMalediva
</s>
</p>
<p>
<s>
===	===	k?	===
Státy	stát	k1gInPc1	stát
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
===	===	k?	===
</s>
</p>
<p>
<s>
Celé	celý	k2eAgFnPc1d1	celá
územíAustrálie	územíAustrálie	k1gFnPc1	územíAustrálie
</s>
</p>
<p>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
Fidži	Fidzat	k5eAaPmIp1nS	Fidzat
</s>
</p>
<p>
<s>
Nauru	Naura	k1gFnSc4	Naura
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
</s>
</p>
<p>
<s>
Niue	Niue	k6eAd1	Niue
</s>
</p>
<p>
<s>
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Pitcairnovy	Pitcairnův	k2eAgInPc1d1	Pitcairnův
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
Samoa	Samoa	k1gFnSc1	Samoa
</s>
</p>
<p>
<s>
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
</s>
</p>
<p>
<s>
Tahiti	Tahiti	k1gNnSc1	Tahiti
</s>
</p>
<p>
<s>
Tokelau	Tokelau	k6eAd1	Tokelau
</s>
</p>
<p>
<s>
Tonga	Tonga	k1gFnSc1	Tonga
</s>
</p>
<p>
<s>
Tuvalu	Tuvala	k1gFnSc4	Tuvala
</s>
</p>
<p>
<s>
Vanuatu	Vanuata	k1gFnSc4	Vanuata
</s>
</p>
<p>
<s>
Velikonoční	velikonoční	k2eAgFnSc1d1	velikonoční
ostrovVětšinaKiribati	ostrovVětšinaKiribat	k5eAaPmF	ostrovVětšinaKiribat
</s>
</p>
<p>
<s>
===	===	k?	===
Státy	stát	k1gInPc1	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
===	===	k?	===
</s>
</p>
<p>
<s>
Celé	celá	k1gFnPc1	celá
územíArgentina	územíArgentin	k2eAgInSc2d1	územíArgentin
</s>
</p>
<p>
<s>
Bolívie	Bolívie	k1gFnSc1	Bolívie
</s>
</p>
<p>
<s>
Chile	Chile	k1gNnSc1	Chile
</s>
</p>
<p>
<s>
Paraguay	Paraguay	k1gFnSc1	Paraguay
</s>
</p>
<p>
<s>
Peru	Peru	k1gNnSc1	Peru
</s>
</p>
<p>
<s>
UruguayVětšinaBrazílie	UruguayVětšinaBrazílie	k1gFnSc1	UruguayVětšinaBrazílie
</s>
</p>
<p>
<s>
EkvádorČástiKolumbie	EkvádorČástiKolumbie	k1gFnSc1	EkvádorČástiKolumbie
</s>
</p>
<p>
<s>
===	===	k?	===
Jiná	jiný	k2eAgNnPc1d1	jiné
území	území	k1gNnPc1	území
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
polokoule	polokoule	k1gFnSc1	polokoule
</s>
</p>
<p>
<s>
Roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
</s>
</p>
<p>
<s>
Rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
</s>
</p>
<p>
<s>
Obratník	obratník	k1gInSc1	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
kříž	kříž	k1gInSc1	kříž
</s>
</p>
