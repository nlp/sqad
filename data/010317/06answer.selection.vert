<s>
Díky	díky	k7c3	díky
sklonu	sklon	k1gInSc3	sklon
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
ekliptice	ekliptika	k1gFnSc6	ekliptika
začíná	začínat	k5eAaImIp3nS	začínat
léto	léto	k1gNnSc1	léto
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
zima	zima	k1gFnSc1	zima
začíná	začínat	k5eAaImIp3nS	začínat
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
