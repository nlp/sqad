<s>
Česká	český	k2eAgNnPc1d1	české
nářečí	nářečí	k1gNnPc1	nářečí
se	se	k3xPyFc4	se
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
do	do	k7c2	do
4	[number]	k4	4
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
kterým	který	k3yQgNnSc7	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
4	[number]	k4	4
české	český	k2eAgInPc1d1	český
interdialekty	interdialekt	k1gInPc1	interdialekt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
češtinou	čeština	k1gFnSc7	čeština
jako	jako	k8xC	jako
interdialektem	interdialekt	k1gInSc7	interdialekt
<g/>
)	)	kIx)	)
Středomoravská	středomoravský	k2eAgFnSc1d1	Středomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
<g/>
)	)	kIx)	)
Východomoravská	východomoravský	k2eAgFnSc1d1	východomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
moravskoslovenská	moravskoslovenský	k2eAgFnSc1d1	moravskoslovenská
<g/>
)	)	kIx)	)
Slezská	Slezská	k1gFnSc1	Slezská
nářečí	nářečí	k1gNnSc2	nářečí
(	(	kIx(	(
<g/>
lašská	lašský	k2eAgFnSc1d1	Lašská
<g/>
)	)	kIx)	)
České	český	k2eAgInPc1d1	český
interdialekty	interdialekt	k1gInPc1	interdialekt
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
čtyři	čtyři	k4xCgNnPc1	čtyři
<g/>
:	:	kIx,	:
obecná	obecná	k1gFnSc1	obecná
čeština	čeština	k1gFnSc1	čeština
obecná	obecná	k1gFnSc1	obecná
hanáčtina	hanáčtina	k1gFnSc1	hanáčtina
obecná	obecná	k1gFnSc1	obecná
moravská	moravský	k2eAgFnSc1d1	Moravská
slovenština	slovenština	k1gFnSc1	slovenština
obecná	obecná	k1gFnSc1	obecná
laština	laština	k1gFnSc1	laština
Pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
území	území	k1gNnSc4	území
osídlená	osídlený	k2eAgNnPc4d1	osídlené
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1945	[number]	k4	1945
Němci	Němec	k1gMnPc7	Němec
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
Sudety	Sudety	k1gInPc1	Sudety
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k8xC	jako
nářečně	nářečně	k6eAd1	nářečně
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
.	.	kIx.	.
</s>
