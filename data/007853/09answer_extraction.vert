Česká	český	k2eAgFnSc1d1	Česká
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
s	s	k7c7	s
obecnou	obecný	k2eAgFnSc7d1	obecná
češtinou	čeština	k1gFnSc7	čeština
jako	jako	k8xC	jako
interdialektem	interdialekt	k1gInSc7	interdialekt
<g/>
)	)	kIx)	)
Středomoravská	středomoravský	k2eAgFnSc1d1	Středomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
<g/>
)	)	kIx)	)
Východomoravská	východomoravský	k2eAgFnSc1d1	východomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
moravskoslovenská	moravskoslovenský	k2eAgFnSc1d1	moravskoslovenská
<g/>
)	)	kIx)	)
Slezská	Slezská	k1gFnSc1	Slezská
nářečí	nářečí	k1gNnSc2	nářečí
