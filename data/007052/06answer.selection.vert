<s>
Komorová	komorový	k2eAgFnSc1d1	komorová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
z	z	k7c2	z
99	[number]	k4	99
<g/>
%	%	kIx~	%
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
některé	některý	k3yIgInPc4	některý
ionty	ion	k1gInPc4	ion
<g/>
,	,	kIx,	,
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
vitamín	vitamín	k1gInSc4	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
glukózu	glukóza	k1gFnSc4	glukóza
a	a	k8xC	a
kyselinu	kyselina	k1gFnSc4	kyselina
mléčnou	mléčný	k2eAgFnSc4d1	mléčná
či	či	k8xC	či
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
.	.	kIx.	.
</s>
