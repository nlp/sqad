<p>
<s>
Tráva	tráva	k1gFnSc1	tráva
je	být	k5eAaImIp3nS	být
pojmenování	pojmenování	k1gNnSc4	pojmenování
pro	pro	k7c4	pro
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
povrch	povrch	k1gInSc4	povrch
jako	jako	k8xC	jako
zelený	zelený	k2eAgInSc4d1	zelený
koberec	koberec	k1gInSc4	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
chladno	chladno	k1gNnSc1	chladno
nebo	nebo	k8xC	nebo
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
travní	travní	k2eAgInSc1d1	travní
porost	porost	k1gInSc1	porost
čili	čili	k8xC	čili
trávník	trávník	k1gInSc1	trávník
se	se	k3xPyFc4	se
zbarvuje	zbarvovat	k5eAaImIp3nS	zbarvovat
do	do	k7c2	do
hněda	hnědo	k1gNnSc2	hnědo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
rostliny	rostlina	k1gFnPc4	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gFnPc2	Poaceae
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
nazývají	nazývat	k5eAaImIp3nP	nazývat
trávy	tráva	k1gFnPc4	tráva
nebo	nebo	k8xC	nebo
traviny	travina	k1gFnPc4	travina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
travin	travina	k1gFnPc2	travina
nedosahuje	dosahovat	k5eNaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
0,5	[number]	k4	0,5
m.	m.	k?	m.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
bambus	bambus	k1gInSc1	bambus
<g/>
,	,	kIx,	,
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Trávník	trávník	k1gInSc1	trávník
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tráva	tráva	k1gFnSc1	tráva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tráva	tráva	k1gFnSc1	tráva
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
