<s>
Gejša	gejša	k1gFnSc1	gejša
(	(	kIx(	(
<g/>
芸	芸	k?	芸
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
označujícící	označujícící	k2eAgFnSc4d1	označujícící
profesionální	profesionální	k2eAgFnSc4d1	profesionální
společnici	společnice	k1gFnSc4	společnice
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Gejša	gejša	k1gFnSc1	gejša
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
gej	gej	k?	gej
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
umění	umění	k1gNnSc1	umění
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
sha	sha	k?	sha
<g/>
"	"	kIx"	"
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
přítomnost	přítomnost	k1gFnSc1	přítomnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
bytí	bytí	k1gNnSc2	bytí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
