<s>
Současné	současný	k2eAgFnPc1d1	současná
studie	studie	k1gFnPc1	studie
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
nálezy	nález	k1gInPc4	nález
kostí	kost	k1gFnPc2	kost
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Bison	Bisona	k1gFnPc2	Bisona
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
