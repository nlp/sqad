<s>
Zubr	zubr	k1gMnSc1	zubr
(	(	kIx(	(
<g/>
Bison	Bison	k1gMnSc1	Bison
bonasus	bonasus	k1gMnSc1	bonasus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
zubr	zubr	k1gMnSc1	zubr
evropský	evropský	k2eAgMnSc1d1	evropský
či	či	k8xC	či
bizon	bizon	k1gMnSc1	bizon
evropský	evropský	k2eAgMnSc1d1	evropský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zvíře	zvíře	k1gNnSc1	zvíře
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
turovitých	turovití	k1gMnPc2	turovití
(	(	kIx(	(
<g/>
Bovidae	Bovidae	k1gNnSc1	Bovidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bizonem	bizon	k1gMnSc7	bizon
americkým	americký	k2eAgNnSc7d1	americké
(	(	kIx(	(
<g/>
B.	B.	kA	B.
bison	bison	k1gInSc1	bison
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jedinými	jediný	k2eAgMnPc7d1	jediný
dvěma	dva	k4xCgMnPc7	dva
žijícími	žijící	k2eAgMnPc7d1	žijící
zástupci	zástupce	k1gMnPc7	zástupce
rodu	rod	k1gInSc2	rod
bizon	bizon	k1gMnSc1	bizon
(	(	kIx(	(
<g/>
Bison	Bison	k1gMnSc1	Bison
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zubr	zubr	k1gMnSc1	zubr
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
bizon	bizon	k1gMnSc1	bizon
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
290	[number]	k4	290
cm	cm	kA	cm
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
530	[number]	k4	530
<g/>
–	–	k?	–
<g/>
920	[number]	k4	920
kg	kg	kA	kg
<g/>
,	,	kIx,	,
kohoutková	kohoutkový	k2eAgFnSc1d1	kohoutková
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
180	[number]	k4	180
<g/>
–	–	k?	–
<g/>
195	[number]	k4	195
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
320-540	[number]	k4	320-540
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Zubr	zubr	k1gMnSc1	zubr
se	se	k3xPyFc4	se
vzhledově	vzhledově	k6eAd1	vzhledově
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
bizonovi	bizon	k1gMnSc3	bizon
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
světle	světle	k6eAd1	světle
až	až	k9	až
tmavě	tmavě	k6eAd1	tmavě
hnědou	hnědý	k2eAgFnSc4d1	hnědá
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Líná	línat	k5eAaImIp3nS	línat
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
.	.	kIx.	.
</s>
<s>
Zubr	zubr	k1gMnSc1	zubr
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnSc1d1	žijící
přežvýkavec	přežvýkavec	k1gMnSc1	přežvýkavec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zubr	zubr	k1gMnSc1	zubr
(	(	kIx(	(
<g/>
Bison	Bison	k1gMnSc1	Bison
bonasus	bonasus	k1gMnSc1	bonasus
Linneaus	Linneaus	k1gMnSc1	Linneaus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
a	a	k8xC	a
bizon	bizon	k1gMnSc1	bizon
americký	americký	k2eAgMnSc1d1	americký
(	(	kIx(	(
<g/>
B.	B.	kA	B.
americanus	americanus	k1gInSc1	americanus
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
poslední	poslední	k2eAgMnPc1d1	poslední
dva	dva	k4xCgMnPc1	dva
žijící	žijící	k2eAgMnPc1d1	žijící
zástupci	zástupce	k1gMnPc1	zástupce
rodu	rod	k1gInSc2	rod
bizon	bizon	k1gMnSc1	bizon
(	(	kIx(	(
<g/>
Bison	Bison	k1gMnSc1	Bison
H.	H.	kA	H.
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
turovití	turovitý	k2eAgMnPc1d1	turovitý
(	(	kIx(	(
<g/>
Bovidae	Bovidae	k1gNnSc7	Bovidae
Gray	Graa	k1gFnSc2	Graa
<g/>
,	,	kIx,	,
1872	[number]	k4	1872
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
bizon	bizon	k1gMnSc1	bizon
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
podčeledi	podčeleď	k1gFnSc2	podčeleď
tuři	tur	k1gMnPc5	tur
(	(	kIx(	(
<g/>
Bovinae	Bovinae	k1gNnSc4	Bovinae
Gray	Graa	k1gFnSc2	Graa
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
druh	druh	k1gInSc4	druh
Bison	Bison	k1gMnSc1	Bison
bonasus	bonasus	k1gMnSc1	bonasus
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
tři	tři	k4xCgInPc1	tři
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
B.	B.	kA	B.
b.	b.	k?	b.
bonasus	bonasus	k1gMnSc1	bonasus
Linneaus	Linneaus	k1gMnSc1	Linneaus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
původně	původně	k6eAd1	původně
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
Bělověžském	Bělověžský	k2eAgInSc6d1	Bělověžský
pralese	prales	k1gInSc6	prales
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
poddruh	poddruh	k1gInSc4	poddruh
B.	B.	kA	B.
b.	b.	k?	b.
hungarorum	hungarorum	k1gInSc1	hungarorum
Kretzoi	Kretzo	k1gFnSc2	Kretzo
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
–	–	k?	–
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
původně	původně	k6eAd1	původně
v	v	k7c6	v
Karpatech	Karpaty	k1gInPc6	Karpaty
a	a	k8xC	a
v	v	k7c6	v
Transylvánii	Transylvánie	k1gFnSc6	Transylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
poddruhem	poddruh	k1gInSc7	poddruh
je	být	k5eAaImIp3nS	být
vyhynulý	vyhynulý	k2eAgInSc1d1	vyhynulý
B.	B.	kA	B.
b.	b.	k?	b.
caucasicus	caucasicus	k1gMnSc1	caucasicus
Turkin	Turkin	k1gMnSc1	Turkin
et	et	k?	et
Saturnin	Saturnin	k1gMnSc1	Saturnin
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
záchrany	záchrana	k1gFnSc2	záchrana
kriticky	kriticky	k6eAd1	kriticky
ohroženého	ohrožený	k2eAgMnSc2d1	ohrožený
zubra	zubr	k1gMnSc2	zubr
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
následující	následující	k2eAgFnPc1d1	následující
linie	linie	k1gFnPc1	linie
<g/>
:	:	kIx,	:
Nížinná	nížinný	k2eAgFnSc1d1	nížinná
linie	linie	k1gFnPc1	linie
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgNnPc1d1	označované
též	též	k6eAd1	též
písmenem	písmeno	k1gNnSc7	písmeno
L	L	kA	L
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
Bělověžská	Bělověžský	k2eAgFnSc1d1	Bělověžská
linie	linie	k1gFnSc1	linie
<g/>
)	)	kIx)	)
–	–	k?	–
tvořena	tvořen	k2eAgFnSc1d1	tvořena
výhradně	výhradně	k6eAd1	výhradně
zvířaty	zvíře	k1gNnPc7	zvíře
pocházejícími	pocházející	k2eAgInPc7d1	pocházející
z	z	k7c2	z
bělověžské	bělověžský	k2eAgFnSc2d1	bělověžský
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Nížinně-kavkazská	Nížinněavkazský	k2eAgFnSc1d1	Nížinně-kavkazský
linie	linie	k1gFnSc1	linie
(	(	kIx(	(
<g/>
označovaná	označovaný	k2eAgFnSc1d1	označovaná
písmeny	písmeno	k1gNnPc7	písmeno
LC	LC	kA	LC
<g/>
)	)	kIx)	)
–	–	k?	–
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
11	[number]	k4	11
příslušníků	příslušník	k1gMnPc2	příslušník
poddruhu	poddruh	k1gInSc2	poddruh
B.	B.	kA	B.
b.	b.	k?	b.
bonasus	bonasus	k1gInSc1	bonasus
a	a	k8xC	a
1	[number]	k4	1
býka	býk	k1gMnSc2	býk
poddruhu	poddruh	k1gInSc2	poddruh
B.	B.	kA	B.
b.	b.	k?	b.
caucasicus	caucasicus	k1gInSc1	caucasicus
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
studie	studie	k1gFnPc1	studie
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
nálezy	nález	k1gInPc4	nález
kostí	kost	k1gFnPc2	kost
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
Bison	Bisona	k1gFnPc2	Bisona
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pliocénu	pliocén	k1gInSc2	pliocén
a	a	k8xC	a
pleistocénu	pleistocén	k1gInSc2	pleistocén
byli	být	k5eAaImAgMnP	být
zástupci	zástupce	k1gMnPc1	zástupce
tohoto	tento	k3xDgInSc2	tento
rodu	rod	k1gInSc2	rod
rozšířeni	rozšířen	k2eAgMnPc1d1	rozšířen
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
přecházeli	přecházet	k5eAaImAgMnP	přecházet
přes	přes	k7c4	přes
Beringovu	Beringův	k2eAgFnSc4d1	Beringova
úžinu	úžina	k1gFnSc4	úžina
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
paleontologické	paleontologický	k2eAgInPc1d1	paleontologický
nálezy	nález	k1gInPc1	nález
pak	pak	k6eAd1	pak
pocházejí	pocházet	k5eAaImIp3nP	pocházet
právě	právě	k9	právě
z	z	k7c2	z
pliocénu	pliocén	k1gInSc2	pliocén
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhy	druh	k1gInPc4	druh
Probison	Probisona	k1gFnPc2	Probisona
dehmi	deh	k1gFnPc7	deh
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
druh	druh	k1gInSc1	druh
Protobison	Protobisona	k1gFnPc2	Protobisona
kushkunensis	kushkunensis	k1gFnSc2	kushkunensis
z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nP	vyvíjet
dvě	dva	k4xCgFnPc1	dva
formy	forma	k1gFnPc1	forma
zubra	zubr	k1gMnSc2	zubr
<g/>
:	:	kIx,	:
krátkorohý	krátkorohý	k2eAgInSc1d1	krátkorohý
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
dlouhorohý	dlouhorohý	k2eAgMnSc1d1	dlouhorohý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
tělesného	tělesný	k2eAgInSc2d1	tělesný
rámce	rámec	k1gInSc2	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
B.	B.	kA	B.
bonasus	bonasus	k1gInSc1	bonasus
vzniká	vznikat	k5eAaImIp3nS	vznikat
právě	právě	k9	právě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
posledního	poslední	k2eAgInSc2d1	poslední
glaciálu	glaciál	k1gInSc2	glaciál
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
druhu	druh	k1gInSc2	druh
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xC	jako
B.	B.	kA	B.
priscus	priscus	k1gInSc1	priscus
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nových	nový	k2eAgFnPc2d1	nová
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
však	však	k9	však
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
zubr	zubr	k1gMnSc1	zubr
je	být	k5eAaImIp3nS	být
mezidruhovým	mezidruhový	k2eAgMnSc7d1	mezidruhový
křížencem	kříženec	k1gMnSc7	kříženec
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
předci	předek	k1gMnPc1	předek
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyhynulí	vyhynulý	k2eAgMnPc1d1	vyhynulý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
pratur	pratur	k1gMnSc1	pratur
(	(	kIx(	(
<g/>
Bos	bos	k1gMnSc1	bos
primigenius	primigenius	k1gMnSc1	primigenius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Bison	Bison	k1gMnSc1	Bison
priscus	priscus	k1gMnSc1	priscus
nebo	nebo	k8xC	nebo
Bison	Bison	k1gMnSc1	Bison
schoetensacki	schoetensack	k1gFnSc2	schoetensack
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
modelu	model	k1gInSc2	model
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc1	rozšíření
zubra	zubr	k1gMnSc2	zubr
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
včetně	včetně	k7c2	včetně
jižní	jižní	k2eAgFnSc2d1	jižní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
60	[number]	k4	60
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
a	a	k8xC	a
severu	sever	k1gInSc6	sever
Iberského	iberský	k2eAgInSc2d1	iberský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
rozšíření	rozšíření	k1gNnSc1	rozšíření
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Volze	Volha	k1gFnSc3	Volha
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
dál	daleko	k6eAd2	daleko
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k9	až
k	k	k7c3	k
jižnímu	jižní	k2eAgNnSc3d1	jižní
pobřeží	pobřeží	k1gNnSc3	pobřeží
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
do	do	k7c2	do
areálu	areál	k1gInSc2	areál
rozšíření	rozšíření	k1gNnSc4	rozšíření
zubra	zubr	k1gMnSc4	zubr
také	také	k6eAd1	také
asijskou	asijský	k2eAgFnSc4d1	asijská
část	část	k1gFnSc4	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
mezi	mezi	k7c7	mezi
50	[number]	k4	50
<g/>
°	°	k?	°
a	a	k8xC	a
60	[number]	k4	60
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
možná	možná	k9	možná
až	až	k6eAd1	až
po	po	k7c4	po
jezero	jezero	k1gNnSc4	jezero
Bajkal	Bajkal	k1gInSc4	Bajkal
<g/>
.	.	kIx.	.
</s>
<s>
Nejnovější	nový	k2eAgFnPc1d3	nejnovější
vědecké	vědecký	k2eAgFnPc1d1	vědecká
studie	studie	k1gFnPc1	studie
posunují	posunovat	k5eAaImIp3nP	posunovat
areál	areál	k1gInSc4	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
zubra	zubr	k1gMnSc2	zubr
v	v	k7c6	v
období	období	k1gNnSc6	období
posledních	poslední	k2eAgInPc2d1	poslední
osmi	osm	k4xCc2	osm
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nástupu	nástup	k1gInSc2	nástup
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
rozmachu	rozmach	k1gInSc2	rozmach
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
především	především	k9	především
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
chyběl	chybět	k5eAaImAgInS	chybět
jak	jak	k6eAd1	jak
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
části	část	k1gFnSc6	část
jižní	jižní	k2eAgFnSc2d1	jižní
i	i	k8xC	i
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
vymírání	vymírání	k1gNnSc2	vymírání
zubrů	zubr	k1gMnPc2	zubr
postupoval	postupovat	k5eAaImAgMnS	postupovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
civilizace	civilizace	k1gFnSc2	civilizace
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
Ardenách	Ardeny	k1gFnPc6	Ardeny
a	a	k8xC	a
ve	v	k7c6	v
Vogézách	Vogézy	k1gFnPc6	Vogézy
přežili	přežít	k5eAaPmAgMnP	přežít
až	až	k6eAd1	až
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Braniborsku	Braniborsko	k1gNnSc6	Braniborsko
byli	být	k5eAaImAgMnP	být
zubři	zubr	k1gMnPc1	zubr
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
odchytáváni	odchytávat	k5eAaImNgMnP	odchytávat
a	a	k8xC	a
chováni	chovat	k5eAaImNgMnP	chovat
v	v	k7c6	v
oborách	obora	k1gFnPc6	obora
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
dlouho	dlouho	k6eAd1	dlouho
přežili	přežít	k5eAaPmAgMnP	přežít
ve	v	k7c6	v
Východním	východní	k2eAgNnSc6d1	východní
Prusku	Prusko	k1gNnSc6	Prusko
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
díky	díky	k7c3	díky
ochranářským	ochranářský	k2eAgFnPc3d1	ochranářská
snahám	snaha	k1gFnPc3	snaha
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
I.	I.	kA	I.
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dobách	doba	k1gFnPc6	doba
spočívala	spočívat	k5eAaImAgFnS	spočívat
ochrana	ochrana	k1gFnSc1	ochrana
v	v	k7c6	v
zákazu	zákaz	k1gInSc6	zákaz
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
přikrmování	přikrmování	k1gNnSc6	přikrmování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
zubři	zubr	k1gMnPc1	zubr
začali	začít	k5eAaPmAgMnP	začít
vymírat	vymírat	k5eAaImF	vymírat
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
zvířata	zvíře	k1gNnPc4	zvíře
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
přežila	přežít	k5eAaPmAgFnS	přežít
v	v	k7c6	v
Transylvánii	Transylvánie	k1gFnSc6	Transylvánie
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tamních	tamní	k2eAgFnPc6d1	tamní
Rodenských	Rodenský	k2eAgFnPc6d1	Rodenský
horách	hora	k1gFnPc6	hora
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
zubr	zubr	k1gMnSc1	zubr
uloven	uloven	k2eAgInSc4d1	uloven
r.	r.	kA	r.
1762	[number]	k4	1762
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
vyhuben	vyhuben	k2eAgMnSc1d1	vyhuben
zubr	zubr	k1gMnSc1	zubr
karpatský	karpatský	k2eAgMnSc1d1	karpatský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
zubři	zubr	k1gMnPc1	zubr
prokazatelně	prokazatelně	k6eAd1	prokazatelně
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Bělověžském	Bělověžský	k2eAgInSc6d1	Bělověžský
pralese	prales	k1gInSc6	prales
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Bělorusku	Bělorusko	k1gNnSc6	Bělorusko
a	a	k8xC	a
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1918	[number]	k4	1918
zbývalo	zbývat	k5eAaImAgNnS	zbývat
v	v	k7c6	v
Bělověži	Bělověž	k1gFnSc6	Bělověž
posledních	poslední	k2eAgInPc2d1	poslední
68	[number]	k4	68
zubrů	zubr	k1gMnPc2	zubr
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
r.	r.	kA	r.
1919	[number]	k4	1919
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
zabit	zabít	k5eAaPmNgMnS	zabít
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgInSc4d1	poslední
kus	kus	k1gInSc4	kus
zastřelen	zastřelen	k2eAgInSc4d1	zastřelen
r.	r.	kA	r.
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
zubři	zubr	k1gMnPc1	zubr
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
naštěstí	naštěstí	k6eAd1	naštěstí
několik	několik	k4yIc1	několik
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
soukromých	soukromý	k2eAgInPc6d1	soukromý
chovech	chov	k1gInPc6	chov
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1923	[number]	k4	1923
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
založena	založit	k5eAaPmNgFnS	založit
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
zubra	zubr	k1gMnSc2	zubr
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
shromáždila	shromáždit	k5eAaPmAgFnS	shromáždit
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
posledních	poslední	k2eAgNnPc2d1	poslední
54	[number]	k4	54
jedincích	jedinec	k1gMnPc6	jedinec
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
56	[number]	k4	56
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tehdy	tehdy	k6eAd1	tehdy
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
zvířat	zvíře	k1gNnPc2	zvíře
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
chovu	chov	k1gInSc3	chov
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
nížinné	nížinný	k2eAgFnSc2d1	nížinná
linie	linie	k1gFnSc2	linie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
pouhých	pouhý	k2eAgNnPc2d1	pouhé
sedm	sedm	k4xCc1	sedm
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
základem	základ	k1gInSc7	základ
nížinně-kavkazské	nížinněavkazský	k2eAgFnSc2d1	nížinně-kavkazský
linie	linie	k1gFnSc2	linie
pak	pak	k6eAd1	pak
dvanáct	dvanáct	k4xCc4	dvanáct
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1932	[number]	k4	1932
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
registr	registr	k1gInSc1	registr
chovných	chovný	k2eAgNnPc2d1	chovné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
změnil	změnit	k5eAaPmAgInS	změnit
v	v	k7c4	v
Plemennou	plemenný	k2eAgFnSc4d1	plemenná
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
vedena	vést	k5eAaImNgFnS	vést
odborníky	odborník	k1gMnPc7	odborník
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Bělověži	Bělověž	k1gFnSc6	Bělověž
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvýšení	zvýšení	k1gNnSc6	zvýšení
početního	početní	k2eAgInSc2d1	početní
stavu	stav	k1gInSc2	stav
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
světě	svět	k1gInSc6	svět
již	již	k6eAd1	již
184	[number]	k4	184
zubrů	zubr	k1gMnPc2	zubr
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
započaly	započnout	k5eAaPmAgFnP	započnout
snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
reintrodukci	reintrodukce	k1gFnSc6	reintrodukce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
reintrodukční	reintrodukční	k2eAgInSc1d1	reintrodukční
projekt	projekt	k1gInSc1	projekt
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Bělověži	Bělověž	k1gFnSc6	Bělověž
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1952	[number]	k4	1952
<g/>
-	-	kIx~	-
<g/>
1966	[number]	k4	1966
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
posledních	poslední	k2eAgInPc2d1	poslední
dostupných	dostupný	k2eAgInPc2d1	dostupný
údajů	údaj	k1gInPc2	údaj
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
4431	[number]	k4	4431
zubrů	zubr	k1gMnPc2	zubr
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
a	a	k8xC	a
v	v	k7c6	v
polodivokých	polodivoký	k2eAgInPc6d1	polodivoký
chovech	chov	k1gInPc6	chov
2956	[number]	k4	2956
a	a	k8xC	a
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
1475	[number]	k4	1475
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgNnSc7d1	původní
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
zubra	zubr	k1gMnSc2	zubr
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc1	oblast
listnatých	listnatý	k2eAgInPc2d1	listnatý
a	a	k8xC	a
smíšených	smíšený	k2eAgInPc2d1	smíšený
lesů	les	k1gInPc2	les
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
porostům	porost	k1gInPc3	porost
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
podrostem	podrost	k1gInSc7	podrost
a	a	k8xC	a
mýtinami	mýtina	k1gFnPc7	mýtina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
adaptabilita	adaptabilita	k1gFnSc1	adaptabilita
je	být	k5eAaImIp3nS	být
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
v	v	k7c6	v
otevřených	otevřený	k2eAgFnPc6d1	otevřená
lesostepích	lesostep	k1gFnPc6	lesostep
i	i	k8xC	i
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
strmých	strmý	k2eAgInPc6d1	strmý
horských	horský	k2eAgInPc6d1	horský
terénech	terén	k1gInPc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
až	až	k9	až
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
2100	[number]	k4	2100
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
praxe	praxe	k1gFnSc1	praxe
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těmto	tento	k3xDgNnPc3	tento
zvířatům	zvíře	k1gNnPc3	zvíře
nevadí	vadit	k5eNaImIp3nS	vadit
ani	ani	k8xC	ani
poměrně	poměrně	k6eAd1	poměrně
fragmentovaná	fragmentovaný	k2eAgFnSc1d1	fragmentovaná
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
se	se	k3xPyFc4	se
zubři	zubr	k1gMnPc1	zubr
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
kulturní	kulturní	k2eAgFnSc6d1	kulturní
<g/>
,	,	kIx,	,
mozaikovité	mozaikovitý	k2eAgFnSc6d1	mozaikovitá
krajině	krajina	k1gFnSc6	krajina
s	s	k7c7	s
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
loukami	louka	k1gFnPc7	louka
a	a	k8xC	a
zemědělsky	zemědělsky	k6eAd1	zemědělsky
obdělávanými	obdělávaný	k2eAgFnPc7d1	obdělávaná
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Zubry	zubr	k1gMnPc4	zubr
neodrazují	odrazovat	k5eNaImIp3nP	odrazovat
ani	ani	k8xC	ani
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
smrkové	smrkový	k2eAgFnPc4d1	smrková
monokultury	monokultura	k1gFnPc4	monokultura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běloruské	běloruský	k2eAgFnSc6d1	Běloruská
části	část	k1gFnSc6	část
Bělověžského	Bělověžský	k2eAgInSc2d1	Bělověžský
pralesa	prales	k1gInSc2	prales
obývají	obývat	k5eAaImIp3nP	obývat
zubři	zubr	k1gMnPc1	zubr
porosty	porost	k1gInPc4	porost
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
jehličnanů	jehličnan	k1gInPc2	jehličnan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
zástupců	zástupce	k1gMnPc2	zástupce
velkých	velký	k2eAgMnPc2d1	velký
turů	tur	k1gMnPc2	tur
se	se	k3xPyFc4	se
zubr	zubr	k1gMnSc1	zubr
nikdy	nikdy	k6eAd1	nikdy
nekoupe	koupat	k5eNaImIp3nS	koupat
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
překonávání	překonávání	k1gNnSc2	překonávání
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
při	při	k7c6	při
migraci	migrace	k1gFnSc6	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
potravu	potrava	k1gFnSc4	potrava
zubrů	zubr	k1gMnPc2	zubr
tvoří	tvořit	k5eAaImIp3nS	tvořit
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
10	[number]	k4	10
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
zubrů	zubr	k1gMnPc2	zubr
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
části	část	k1gFnSc6	část
Bělověže	Bělověž	k1gFnSc2	Bělověž
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celkem	celkem	k6eAd1	celkem
131	[number]	k4	131
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
27	[number]	k4	27
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
14	[number]	k4	14
druhů	druh	k1gInPc2	druh
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
ostřic	ostřice	k1gFnPc2	ostřice
a	a	k8xC	a
96	[number]	k4	96
druhů	druh	k1gInPc2	druh
dvouděložných	dvouděložný	k2eAgFnPc2d1	dvouděložná
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
a	a	k8xC	a
keře	keř	k1gInPc1	keř
tvoří	tvořit	k5eAaImIp3nP	tvořit
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
skladbě	skladba	k1gFnSc6	skladba
potravy	potrava	k1gFnSc2	potrava
33	[number]	k4	33
%	%	kIx~	%
<g/>
,	,	kIx,	,
trávy	tráva	k1gFnPc1	tráva
a	a	k8xC	a
byliny	bylina	k1gFnPc1	bylina
67	[number]	k4	67
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
potravou	potrava	k1gFnSc7	potrava
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
žaludy	žalud	k1gInPc1	žalud
<g/>
,	,	kIx,	,
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
lesní	lesní	k2eAgInPc1d1	lesní
plody	plod	k1gInPc1	plod
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ostružiny	ostružina	k1gFnPc1	ostružina
<g/>
,	,	kIx,	,
a	a	k8xC	a
houby	houba	k1gFnPc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běloruské	běloruský	k2eAgFnSc6d1	Běloruská
části	část	k1gFnSc6	část
Bělověžského	Bělověžský	k2eAgInSc2d1	Bělověžský
pralesa	prales	k1gInSc2	prales
je	být	k5eAaImIp3nS	být
potravní	potravní	k2eAgFnSc1d1	potravní
nabídka	nabídka	k1gFnSc1	nabídka
pro	pro	k7c4	pro
zubry	zubr	k1gMnPc4	zubr
pestřejší	pestrý	k2eAgMnPc1d2	pestřejší
<g/>
;	;	kIx,	;
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
331	[number]	k4	331
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
zastoupené	zastoupený	k2eAgFnPc4d1	zastoupená
lipnicovité	lipnicovitý	k2eAgFnPc4d1	lipnicovitý
trávy	tráva	k1gFnPc4	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Zubr	zubr	k1gMnSc1	zubr
evropský	evropský	k2eAgMnSc1d1	evropský
byl	být	k5eAaImAgInS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
žijících	žijící	k2eAgNnPc2d1	žijící
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žila	žít	k5eAaImAgFnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
přítomnost	přítomnost	k1gFnSc4	přítomnost
na	na	k7c4	na
území	území	k1gNnSc4	území
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
dodnes	dodnes	k6eAd1	dodnes
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
místopisné	místopisný	k2eAgInPc4d1	místopisný
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
obce	obec	k1gFnPc1	obec
Zubří	zubří	k2eAgFnPc1d1	zubří
<g/>
,	,	kIx,	,
Zubrnice	Zubrnice	k1gFnPc1	Zubrnice
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnPc1d1	vodní
toky	toka	k1gFnPc1	toka
Zubřina	Zubřina	k1gFnSc1	Zubřina
<g/>
,	,	kIx,	,
Zubřinka	Zubřinka	k1gFnSc1	Zubřinka
nebo	nebo	k8xC	nebo
erby	erb	k1gInPc1	erb
některých	některý	k3yIgInPc2	některý
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Zubry	zubr	k1gMnPc4	zubr
chová	chovat	k5eAaImIp3nS	chovat
řada	řada	k1gFnSc1	řada
tuzemských	tuzemský	k2eAgFnPc2d1	tuzemská
zoologických	zoologický	k2eAgFnPc2d1	zoologická
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
první	první	k4xOgMnPc1	první
čistokrevní	čistokrevný	k2eAgMnPc1d1	čistokrevný
zubři	zubr	k1gMnPc1	zubr
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
soukromých	soukromý	k2eAgInPc2d1	soukromý
chovů	chov	k1gInPc2	chov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
později	pozdě	k6eAd2	pozdě
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
propaguje	propagovat	k5eAaImIp3nS	propagovat
ochranářská	ochranářský	k2eAgFnSc1d1	ochranářská
společnost	společnost	k1gFnSc1	společnost
Česká	český	k2eAgFnSc1d1	Česká
krajina	krajina	k1gFnSc1	krajina
návrat	návrat	k1gInSc1	návrat
zubrů	zubr	k1gMnPc2	zubr
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
oborou	obora	k1gFnSc7	obora
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
zubři	zubr	k1gMnPc1	zubr
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
obývají	obývat	k5eAaImIp3nP	obývat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Židlov	Židlov	k1gInSc4	Židlov
v	v	k7c6	v
Ralsku	Ralsek	k1gInSc6	Ralsek
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
přivezeni	přivezen	k2eAgMnPc1d1	přivezen
zubři	zubr	k1gMnPc1	zubr
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
drženi	držen	k2eAgMnPc1d1	držen
v	v	k7c6	v
karanténě	karanténa	k1gFnSc6	karanténa
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
vypuštěni	vypustit	k5eAaPmNgMnP	vypustit
do	do	k7c2	do
obory	obora	k1gFnSc2	obora
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
stádo	stádo	k1gNnSc1	stádo
tvořené	tvořený	k2eAgNnSc1d1	tvořené
jedním	jeden	k4xCgMnSc7	jeden
býkem	býk	k1gMnSc7	býk
<g/>
,	,	kIx,	,
čtyřmi	čtyři	k4xCgFnPc7	čtyři
kravami	kráva	k1gFnPc7	kráva
a	a	k8xC	a
třemi	tři	k4xCgNnPc7	tři
telaty	tele	k1gNnPc7	tele
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
do	do	k7c2	do
obory	obora	k1gFnSc2	obora
až	až	k9	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
další	další	k2eAgNnSc1d1	další
stádo	stádo	k1gNnSc1	stádo
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
v	v	k7c6	v
ohradě	ohrada	k1gFnSc6	ohrada
nedaleko	nedaleko	k7c2	nedaleko
Benátek	Benátky	k1gFnPc2	Benátky
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
<s>
Zubr	zubr	k1gMnSc1	zubr
byl	být	k5eAaImAgMnS	být
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
vnímán	vnímán	k2eAgInSc1d1	vnímán
především	především	k9	především
jako	jako	k9	jako
cíl	cíl	k1gInSc4	cíl
ochranářských	ochranářský	k2eAgFnPc2d1	ochranářská
snah	snaha	k1gFnPc2	snaha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
pohled	pohled	k1gInSc1	pohled
výrazně	výrazně	k6eAd1	výrazně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Zubři	zubr	k1gMnPc1	zubr
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nezbytnou	zbytný	k2eNgFnSc4d1	zbytný
součást	součást	k1gFnSc4	součást
fungujícího	fungující	k2eAgInSc2d1	fungující
evropského	evropský	k2eAgInSc2d1	evropský
ekosystému	ekosystém	k1gInSc2	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
vymírání	vymírání	k1gNnSc4	vymírání
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
závislých	závislý	k2eAgInPc2d1	závislý
na	na	k7c6	na
přirozených	přirozený	k2eAgFnPc6d1	přirozená
loukách	louka	k1gFnPc6	louka
a	a	k8xC	a
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
motýli	motýl	k1gMnPc1	motýl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc7d1	moderní
vědou	věda	k1gFnSc7	věda
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
vysvětlováno	vysvětlovat	k5eAaImNgNnS	vysvětlovat
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
absence	absence	k1gFnSc2	absence
zubrů	zubr	k1gMnPc2	zubr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
velkých	velký	k2eAgInPc2d1	velký
kopytníků	kopytník	k1gInPc2	kopytník
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
divocí	divoký	k2eAgMnPc1d1	divoký
koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
pratuři	pratur	k1gMnPc1	pratur
<g/>
,	,	kIx,	,
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
zubři	zubr	k1gMnPc1	zubr
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nezbytný	zbytný	k2eNgInSc4d1	zbytný
prostředek	prostředek	k1gInSc4	prostředek
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
mnoha	mnoho	k4c2	mnoho
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
i	i	k9	i
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
zubroň	zubronit	k5eAaPmRp2nS	zubronit
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zubr	zubr	k1gMnSc1	zubr
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zpráva	zpráva	k1gFnSc1	zpráva
Do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
doupovských	doupovský	k2eAgFnPc2d1	doupovský
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
zubr	zubr	k1gMnSc1	zubr
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
YouTube	YouTub	k1gInSc5	YouTub
<g/>
:	:	kIx,	:
Zubr	zubr	k1gMnSc1	zubr
evropský	evropský	k2eAgMnSc1d1	evropský
-	-	kIx~	-
návrat	návrat	k1gInSc1	návrat
krále	král	k1gMnSc2	král
lesů	les	k1gInPc2	les
Zubři	zubr	k1gMnPc1	zubr
nepatří	patřit	k5eNaImIp3nP	patřit
do	do	k7c2	do
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
otevřené	otevřený	k2eAgFnSc3d1	otevřená
krajině	krajina	k1gFnSc3	krajina
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
paleontologové	paleontolog	k1gMnPc1	paleontolog
</s>
