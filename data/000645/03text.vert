<p>
<s>
Poušť	poušť	k1gFnSc1	poušť
je	být	k5eAaImIp3nS	být
neúrodná	úrodný	k2eNgFnSc1d1	neúrodná
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trpí	trpět	k5eAaImIp3nS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
za	za	k7c4	za
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
250	[number]	k4	250
mm	mm	kA	mm
ročně	ročně	k6eAd1	ročně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
rysů	rys	k1gInPc2	rys
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Pouště	poušť	k1gFnPc1	poušť
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
kolem	kolem	k7c2	kolem
obratníků	obratník	k1gInPc2	obratník
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
pouště	poušť	k1gFnSc2	poušť
je	být	k5eAaImIp3nS	být
Sahara	Sahara	k1gFnSc1	Sahara
či	či	k8xC	či
Arabská	arabský	k2eAgFnSc1d1	arabská
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
tzv.	tzv.	kA	tzv.
horkých	horký	k2eAgFnPc2d1	horká
pouští	poušť	k1gFnPc2	poušť
existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
polární	polární	k2eAgFnPc4d1	polární
pouště	poušť	k1gFnPc4	poušť
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
či	či	k8xC	či
Antarktidě	Antarktida	k1gFnSc3	Antarktida
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
polárních	polární	k2eAgFnPc2d1	polární
tlakových	tlakový	k2eAgFnPc2d1	tlaková
výší	výše	k1gFnPc2	výše
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
pouští	poušť	k1gFnPc2	poušť
==	==	k?	==
</s>
</p>
<p>
<s>
Erg	erg	k1gInSc1	erg
je	být	k5eAaImIp3nS	být
písečná	písečný	k2eAgFnSc1d1	písečná
poušť	poušť	k1gFnSc1	poušť
pohyblivých	pohyblivý	k2eAgFnPc2d1	pohyblivá
dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
je	být	k5eAaImIp3nS	být
zastoupena	zastoupen	k2eAgFnSc1d1	zastoupena
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
pětinu	pětina	k1gFnSc4	pětina
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
.	.	kIx.	.
</s>
<s>
Erg	erg	k1gInSc1	erg
je	být	k5eAaImIp3nS	být
skoro	skoro	k6eAd1	skoro
bez	bez	k7c2	bez
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
pár	pár	k4xCyI	pár
tamaryšků	tamaryšek	k1gInPc2	tamaryšek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ergy	erg	k1gInPc7	erg
se	se	k3xPyFc4	se
setkáme	setkat	k5eAaPmIp1nP	setkat
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
Sahaře	Sahara	k1gFnSc6	Sahara
(	(	kIx(	(
<g/>
Erg	erg	k1gInSc4	erg
Igidi	Igid	k1gMnPc1	Igid
<g/>
,	,	kIx,	,
Erg	erg	k1gInSc4	erg
Šeš	Šeš	k1gFnSc2	Šeš
<g/>
,	,	kIx,	,
Erg	erg	k1gInSc4	erg
Šigaga	Šigag	k1gMnSc2	Šigag
<g/>
,	,	kIx,	,
Erg	erg	k1gInSc4	erg
Šebbí	Šebbí	k1gNnSc4	Šebbí
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc4d1	velký
západní	západní	k2eAgInSc4d1	západní
a	a	k8xC	a
Velký	velký	k2eAgInSc4d1	velký
východní	východní	k2eAgInSc4d1	východní
erg	erg	k1gInSc4	erg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Libyjské	libyjský	k2eAgFnSc6d1	Libyjská
poušti	poušť	k1gFnSc6	poušť
a	a	k8xC	a
Nigeru	Niger	k1gInSc6	Niger
(	(	kIx(	(
<g/>
pouště	poušť	k1gFnSc2	poušť
Ténéré	Ténérý	k2eAgFnSc2d1	Ténérý
a	a	k8xC	a
Grand	grand	k1gMnSc1	grand
Erg	erg	k1gInSc4	erg
de	de	k?	de
Bilma	Bilm	k1gMnSc2	Bilm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Saharský	saharský	k2eAgInSc1d1	saharský
písek	písek	k1gInSc1	písek
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
mořského	mořský	k2eAgMnSc2d1	mořský
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pevninského	pevninský	k2eAgInSc2d1	pevninský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
zvětralý	zvětralý	k2eAgInSc4d1	zvětralý
kenozoický	kenozoický	k2eAgInSc4d1	kenozoický
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reg	Reg	k?	Reg
nebo	nebo	k8xC	nebo
serír	serír	k1gInSc1	serír
je	být	k5eAaImIp3nS	být
šterkovitá	šterkovitý	k2eAgFnSc1d1	šterkovitý
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
oblázková	oblázkový	k2eAgFnSc1d1	oblázková
<g/>
)	)	kIx)	)
poušť	poušť	k1gFnSc1	poušť
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
převažující	převažující	k2eAgInSc1d1	převažující
typ	typ	k1gInSc1	typ
na	na	k7c6	na
Sahaře	Sahara	k1gFnSc6	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hamada	Hamada	k1gFnSc1	Hamada
je	být	k5eAaImIp3nS	být
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
poušť	poušť	k1gFnSc1	poušť
s	s	k7c7	s
holými	holý	k2eAgInPc7d1	holý
skalními	skalní	k2eAgInPc7d1	skalní
výchozy	výchoz	k1gInPc7	výchoz
a	a	k8xC	a
kameny	kámen	k1gInPc7	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Hamáda	Hamáda	k1gFnSc1	Hamáda
al-Hamrá	al-Hamrý	k2eAgFnSc1d1	al-Hamrý
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Libyi	Libye	k1gFnSc6	Libye
nebo	nebo	k8xC	nebo
hamady	hamada	k1gFnSc2	hamada
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
pohoří	pohoří	k1gNnSc2	pohoří
Ahaggar	Ahaggara	k1gFnPc2	Ahaggara
<g/>
,	,	kIx,	,
Tassili	Tassili	k1gFnSc2	Tassili
a	a	k8xC	a
Tibesti	Tibest	k1gFnSc2	Tibest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sebh	Sebh	k1gInSc1	Sebh
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgInSc4d1	vzácný
typ	typ	k1gInSc4	typ
hlinité	hlinitý	k2eAgFnSc2d1	hlinitá
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
klima	klima	k1gNnSc1	klima
==	==	k?	==
</s>
</p>
<p>
<s>
Obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
je	být	k5eAaImIp3nS	být
řídká	řídký	k2eAgFnSc1d1	řídká
vegetace	vegetace	k1gFnSc1	vegetace
specifického	specifický	k2eAgInSc2d1	specifický
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
kaktusy	kaktus	k1gInPc1	kaktus
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
sukulenty	sukulent	k1gInPc1	sukulent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
výjimečně	výjimečně	k6eAd1	výjimečně
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
i	i	k9	i
rozsáhlejší	rozsáhlý	k2eAgInPc1d2	rozsáhlejší
porosty	porost	k1gInPc1	porost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
lesy	les	k1gInPc1	les
kaktusů	kaktus	k1gInPc2	kaktus
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasičtější	klasický	k2eAgFnSc1d2	klasičtější
vegetace	vegetace	k1gFnSc1	vegetace
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
zdrojů	zdroj	k1gInPc2	zdroj
povrchové	povrchový	k2eAgFnSc2d1	povrchová
nebo	nebo	k8xC	nebo
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
oázy	oáza	k1gFnSc2	oáza
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
řek	řeka	k1gFnPc2	řeka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
typech	typ	k1gInPc6	typ
pouští	pouštět	k5eAaImIp3nS	pouštět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
příděl	příděl	k1gInSc1	příděl
srážek	srážka	k1gFnPc2	srážka
padá	padat	k5eAaImIp3nS	padat
naráz	naráz	k6eAd1	naráz
v	v	k7c6	v
úzkém	úzký	k2eAgNnSc6d1	úzké
časovém	časový	k2eAgNnSc6d1	časové
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
krátké	krátký	k2eAgNnSc4d1	krátké
vegetační	vegetační	k2eAgNnSc4d1	vegetační
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
režim	režim	k1gInSc4	režim
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
prudké	prudký	k2eAgNnSc1d1	prudké
střídání	střídání	k1gNnSc1	střídání
teplot	teplota	k1gFnPc2	teplota
<g/>
:	:	kIx,	:
ve	v	k7c6	v
dne	den	k1gInSc2	den
vedra	vedro	k1gNnSc2	vedro
až	až	k9	až
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
prudké	prudký	k2eAgNnSc1d1	prudké
ochlazení	ochlazení	k1gNnSc1	ochlazení
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
až	až	k9	až
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
mrazu	mráz	k1gInSc2	mráz
nebo	nebo	k8xC	nebo
i	i	k9	i
pod	pod	k7c4	pod
nulu	nula	k1gFnSc4	nula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrovská	obrovský	k2eAgNnPc1d1	obrovské
denní	denní	k2eAgNnPc1d1	denní
vedra	vedro	k1gNnPc1	vedro
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
mez	mez	k1gFnSc4	mez
snesitelnosti	snesitelnost	k1gFnSc2	snesitelnost
většiny	většina	k1gFnSc2	většina
živočichů	živočich	k1gMnPc2	živočich
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
denní	denní	k2eAgFnSc2d1	denní
aktivity	aktivita	k1gFnSc2	aktivita
obyvatel	obyvatel	k1gMnPc2	obyvatel
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgNnPc1d1	pouštní
zvířata	zvíře	k1gNnPc1	zvíře
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
na	na	k7c6	na
vysoké	vysoká	k1gFnSc6	vysoká
teploty	teplota	k1gFnSc2	teplota
aklimatizována	aklimatizovat	k5eAaBmNgFnS	aklimatizovat
a	a	k8xC	a
uzpůsobena	uzpůsobit	k5eAaPmNgFnS	uzpůsobit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
si	se	k3xPyFc3	se
nemůže	moct	k5eNaImIp3nS	moct
dovolit	dovolit	k5eAaPmF	dovolit
přehřívat	přehřívat	k5eAaImF	přehřívat
svůj	svůj	k3xOyFgInSc4	svůj
organismus	organismus	k1gInSc4	organismus
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
při	při	k7c6	při
nich	on	k3xPp3gMnPc6	on
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
nějakou	nějaký	k3yIgFnSc4	nějaký
aktivitu	aktivita	k1gFnSc4	aktivita
a	a	k8xC	a
plýtvat	plýtvat	k5eAaImF	plýtvat
tělesnými	tělesný	k2eAgFnPc7d1	tělesná
tekutinami	tekutina	k1gFnPc7	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgInSc1d1	pouštní
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zpravidla	zpravidla	k6eAd1	zpravidla
život	život	k1gInSc4	život
noční	noční	k2eAgInSc4d1	noční
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
s	s	k7c7	s
večerním	večerní	k2eAgInSc7d1	večerní
šerem	šer	k1gInSc7wB	šer
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
nejpozději	pozdě	k6eAd3	pozdě
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
rostliny	rostlina	k1gFnPc1	rostlina
–	–	k?	–
klasické	klasický	k2eAgFnPc4d1	klasická
pouštní	pouštní	k2eAgFnPc4d1	pouštní
rostliny	rostlina	k1gFnPc4	rostlina
zpravidla	zpravidla	k6eAd1	zpravidla
přes	přes	k7c4	přes
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
na	na	k7c4	na
největší	veliký	k2eAgNnSc4d3	veliký
vedro	vedro	k1gNnSc4	vedro
<g/>
)	)	kIx)	)
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
průduchy	průduch	k1gInPc4	průduch
a	a	k8xC	a
výměnu	výměna	k1gFnSc4	výměna
plynů	plyn	k1gInPc2	plyn
si	se	k3xPyFc3	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
na	na	k7c4	na
večer	večer	k1gInSc4	večer
a	a	k8xC	a
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
omezily	omezit	k5eAaPmAgFnP	omezit
výpar	výpar	k1gInSc4	výpar
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
můžeme	moct	k5eAaImIp1nP	moct
specifikovat	specifikovat	k5eAaBmF	specifikovat
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
deflační	deflační	k2eAgFnSc1d1	deflační
oblast	oblast	k1gFnSc1	oblast
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
je	být	k5eAaImIp3nS	být
větrem	vítr	k1gInSc7	vítr
odnášen	odnášen	k2eAgInSc1d1	odnášen
jemný	jemný	k2eAgInSc1d1	jemný
písčitý	písčitý	k2eAgInSc1d1	písčitý
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Odnos	odnos	k1gInSc1	odnos
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vznik	vznik	k1gInSc4	vznik
pouštní	pouštní	k2eAgFnSc2d1	pouštní
dlažby	dlažba	k1gFnSc2	dlažba
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
pouště	poušť	k1gFnSc2	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
písčitý	písčitý	k2eAgInSc1d1	písčitý
materiál	materiál	k1gInSc1	materiál
ukládán	ukládán	k2eAgInSc1d1	ukládán
a	a	k8xC	a
kde	kde	k6eAd1	kde
vznikají	vznikat	k5eAaImIp3nP	vznikat
písečné	písečný	k2eAgFnPc1d1	písečná
duny	duna	k1gFnPc1	duna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Důvody	důvod	k1gInPc1	důvod
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
pouští	poušť	k1gFnPc2	poušť
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
vznikají	vznikat	k5eAaImIp3nP	vznikat
pouště	poušť	k1gFnPc4	poušť
kvůli	kvůli	k7c3	kvůli
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tlakové	tlakový	k2eAgFnSc3d1	tlaková
výši	výše	k1gFnSc3	výše
–	–	k?	–
vzduch	vzduch	k1gInSc1	vzduch
v	v	k7c6	v
tlakové	tlakový	k2eAgFnSc6d1	tlaková
výši	výše	k1gFnSc6	výše
se	se	k3xPyFc4	se
tlačí	tlačit	k5eAaImIp3nS	tlačit
směrem	směr	k1gInSc7	směr
dolů	dol	k1gInPc2	dol
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
otepluje	oteplovat	k5eAaImIp3nS	oteplovat
a	a	k8xC	a
"	"	kIx"	"
<g/>
nasává	nasávat	k5eAaImIp3nS	nasávat
<g/>
"	"	kIx"	"
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
následně	následně	k6eAd1	následně
odvádí	odvádět	k5eAaImIp3nS	odvádět
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
tlaková	tlakový	k2eAgFnSc1d1	tlaková
výše	výše	k1gFnSc1	výše
brání	bránit	k5eAaImIp3nS	bránit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
tlaku	tlak	k1gInSc3	tlak
<g/>
,	,	kIx,	,
vlhkému	vlhký	k2eAgInSc3d1	vlhký
vzduchu	vzduch	k1gInSc3	vzduch
od	od	k7c2	od
moří	mořit	k5eAaImIp3nP	mořit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Sahara	Sahara	k1gFnSc1	Sahara
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srážkovému	srážkový	k2eAgInSc3d1	srážkový
stínu	stín	k1gInSc3	stín
–	–	k?	–
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
mohutná	mohutný	k2eAgNnPc1d1	mohutné
pohoří	pohoří	k1gNnPc1	pohoří
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
doslova	doslova	k6eAd1	doslova
vyčnívají	vyčnívat	k5eAaImIp3nP	vyčnívat
nad	nad	k7c7	nad
mraky	mrak	k1gInPc7	mrak
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakousi	jakýsi	k3yIgFnSc4	jakýsi
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
například	například	k6eAd1	například
u	u	k7c2	u
amerických	americký	k2eAgFnPc2d1	americká
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studeným	studený	k2eAgInPc3d1	studený
proudům	proud	k1gInPc3	proud
–	–	k?	–
proudy	proud	k1gInPc1	proud
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
vzduch	vzduch	k1gInSc4	vzduch
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
nad	nad	k7c4	nad
teplou	teplý	k2eAgFnSc4d1	teplá
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
rozpínat	rozpínat	k5eAaImF	rozpínat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
srážek	srážka	k1gFnPc2	srážka
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zapotřebí	zapotřebí	k6eAd1	zapotřebí
opačného	opačný	k2eAgInSc2d1	opačný
procesu	proces	k1gInSc2	proces
(	(	kIx(	(
<g/>
ochlazování	ochlazování	k1gNnSc3	ochlazování
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
stačí	stačit	k5eAaBmIp3nS	stačit
podívat	podívat	k5eAaPmF	podívat
na	na	k7c4	na
mapu	mapa	k1gFnSc4	mapa
studených	studený	k2eAgInPc2d1	studený
proudů	proud	k1gInPc2	proud
(	(	kIx(	(
<g/>
pouště	poušť	k1gFnPc1	poušť
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Největší	veliký	k2eAgFnPc1d3	veliký
světové	světový	k2eAgFnPc1d1	světová
pouště	poušť	k1gFnPc1	poušť
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Polopoušť	polopoušť	k1gFnSc1	polopoušť
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
poušť	poušť	k1gFnSc1	poušť
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Poušť	poušť	k1gFnSc1	poušť
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
poušť	poušť	k1gFnSc1	poušť
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
