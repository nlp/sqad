<p>
<s>
Kundelungu	Kundelung	k1gInSc3	Kundelung
je	on	k3xPp3gInPc4	on
pohoří	pohořet	k5eAaPmIp3nS	pohořet
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
republiky	republika	k1gFnSc2	republika
Kongo	Kongo	k1gNnSc1	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
240	[number]	k4	240
km	km	kA	km
od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
ploché	plochý	k2eAgNnSc1d1	ploché
temeno	temeno	k1gNnSc1	temeno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
severu	sever	k1gInSc2	sever
je	být	k5eAaImIp3nS	být
pohoří	pohoří	k1gNnSc1	pohoří
odděleno	oddělit	k5eAaPmNgNnS	oddělit
údolím	údolí	k1gNnSc7	údolí
od	od	k7c2	od
pohoří	pohoří	k1gNnPc2	pohoří
Mitumba	Mitumb	k1gMnSc2	Mitumb
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
750	[number]	k4	750
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
Mweru	Mwer	k1gInSc2	Mwer
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
odvodňovány	odvodňovat	k5eAaImNgInP	odvodňovat
do	do	k7c2	do
řeky	řeka	k1gFnSc2	řeka
Lufira	Lufiro	k1gNnSc2	Lufiro
–	–	k?	–
pravostranným	pravostranný	k2eAgInSc7d1	pravostranný
přítokem	přítok	k1gInSc7	přítok
Lualaby	Lualaba	k1gFnSc2	Lualaba
a	a	k8xC	a
východní	východní	k2eAgInPc1d1	východní
svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
odvodňovány	odvodňovat	k5eAaImNgInP	odvodňovat
do	do	k7c2	do
Luapuly	Luapula	k1gFnSc2	Luapula
<g/>
.	.	kIx.	.
</s>
<s>
Svahy	svah	k1gInPc1	svah
jsou	být	k5eAaImIp3nP	být
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
lesem	les	k1gInSc7	les
<g/>
,	,	kIx,	,
temeno	temeno	k1gNnSc4	temeno
je	být	k5eAaImIp3nS	být
odlesněné	odlesněný	k2eAgNnSc1d1	odlesněné
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
pohoří	pohoří	k1gNnSc2	pohoří
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
Kundelungu	Kundelung	k1gInSc2	Kundelung
<g/>
.	.	kIx.	.
</s>
</p>
