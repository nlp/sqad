<s>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
byl	být	k5eAaImAgInS	být
český	český	k2eAgInSc1d1	český
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Československa	Československo	k1gNnSc2	Československo
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
zněl	znět	k5eAaImAgInS	znět
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federatívna	Federatívna	k1gFnSc1	Federatívna
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInSc1d1	předchozí
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
Československá	československý	k2eAgFnSc1d1	Československá
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
byla	být	k5eAaImAgFnS	být
federací	federace	k1gFnSc7	federace
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Název	název	k1gInSc1	název
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
Pomlčková	pomlčkový	k2eAgFnSc1d1	pomlčková
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc4	název
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
"	"	kIx"	"
<g/>
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
názvu	název	k1gInSc2	název
Československé	československý	k2eAgFnSc2d1	Československá
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
<g/>
"	"	kIx"	"
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
pomlčkové	pomlčkový	k2eAgFnSc6d1	pomlčková
válce	válka	k1gFnSc6	válka
místo	místo	k1gNnSc4	místo
předchozích	předchozí	k2eAgFnPc2d1	předchozí
Československá	československý	k2eAgFnSc1d1	Československá
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
Česko-slovenská	českolovenský	k2eAgFnSc1d1	česko-slovenská
federatívna	federatívna	k1gFnSc1	federatívna
republika	republika	k1gFnSc1	republika
ve	v	k7c6	v
slovenském	slovenský	k2eAgInSc6d1	slovenský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
pravopisně	pravopisně	k6eAd1	pravopisně
nesprávný	správný	k2eNgInSc1d1	nesprávný
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kvůli	kvůli	k7c3	kvůli
dalšímu	další	k2eAgInSc3d1	další
ze	z	k7c2	z
sporů	spor	k1gInPc2	spor
pomlčkové	pomlčkový	k2eAgFnSc2d1	pomlčková
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
velké	velká	k1gFnPc4	velká
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
slovenských	slovenský	k2eAgMnPc2d1	slovenský
představitelů	představitel	k1gMnPc2	představitel
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
požadovala	požadovat	k5eAaImAgFnS	požadovat
také	také	k9	také
velké	velký	k2eAgNnSc4d1	velké
písmeno	písmeno	k1gNnSc4	písmeno
"	"	kIx"	"
<g/>
S	s	k7c7	s
<g/>
"	"	kIx"	"
u	u	k7c2	u
"	"	kIx"	"
<g/>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
čeští	český	k2eAgMnPc1d1	český
zástupci	zástupce	k1gMnPc1	zástupce
nechtěli	chtít	k5eNaImAgMnP	chtít
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
přistoupit	přistoupit	k5eAaPmF	přistoupit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
sporu	spor	k1gInSc2	spor
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
vedla	vést	k5eAaImAgFnS	vést
o	o	k7c4	o
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
vs	vs	k?	vs
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
slovenská	slovenský	k2eAgFnSc1d1	slovenská
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
i	i	k8xC	i
slovenského	slovenský	k2eAgInSc2d1	slovenský
pravopisu	pravopis	k1gInSc2	pravopis
se	se	k3xPyFc4	se
přídavná	přídavný	k2eAgNnPc1d1	přídavné
jména	jméno	k1gNnPc1	jméno
vytvořená	vytvořený	k2eAgNnPc1d1	vytvořené
z	z	k7c2	z
vlastních	vlastní	k2eAgNnPc2d1	vlastní
jmen	jméno	k1gNnPc2	jméno
píší	psát	k5eAaImIp3nP	psát
malým	malý	k2eAgNnSc7d1	malé
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
u	u	k7c2	u
víceslovných	víceslovný	k2eAgInPc2d1	víceslovný
názvů	název	k1gInPc2	název
píše	psát	k5eAaImIp3nS	psát
velké	velký	k2eAgNnSc4d1	velké
písmeno	písmeno	k1gNnSc4	písmeno
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
prvního	první	k4xOgNnSc2	první
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
další	další	k2eAgNnPc1d1	další
slova	slovo	k1gNnPc1	slovo
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
jiné	jiný	k2eAgNnSc4d1	jiné
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
šalamounský	šalamounský	k2eAgInSc1d1	šalamounský
<g/>
"	"	kIx"	"
kompromis	kompromis	k1gInSc1	kompromis
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
"	"	kIx"	"
schválen	schválen	k2eAgInSc1d1	schválen
Federálním	federální	k2eAgNnSc7d1	federální
shromážděním	shromáždění	k1gNnSc7	shromáždění
jako	jako	k9	jako
ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
názvu	název	k1gInSc2	název
Československé	československý	k2eAgFnSc2d1	Československá
federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
odporoval	odporovat	k5eAaImAgMnS	odporovat
pravopisu	pravopis	k1gInSc6	pravopis
obou	dva	k4xCgInPc2	dva
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Česka	Česko	k1gNnSc2	Česko
<g/>
#	#	kIx~	#
<g/>
Pád	Pád	k1gInSc1	Pád
komunismu	komunismus	k1gInSc2	komunismus
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc2	rozdělení
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
jako	jako	k9	jako
nástupce	nástupce	k1gMnSc1	nástupce
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Československa	Československo	k1gNnSc2	Československo
<g/>
#	#	kIx~	#
<g/>
Porevoluční	porevoluční	k2eAgInSc1d1	porevoluční
vývoj	vývoj	k1gInSc1	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zánik	zánik	k1gInSc1	zánik
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
přijetím	přijetí	k1gNnSc7	přijetí
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
o	o	k7c6	o
zániku	zánik	k1gInSc6	zánik
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
Federativní	federativní	k2eAgFnSc2d1	federativní
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
národní	národní	k2eAgInPc1d1	národní
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvořily	tvořit	k5eAaImAgFnP	tvořit
federaci	federace	k1gFnSc4	federace
<g/>
,	,	kIx,	,
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
osamostatnily	osamostatnit	k5eAaPmAgFnP	osamostatnit
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Česká	český	k2eAgFnSc1d1	Česká
a	a	k8xC	a
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
Federativní	federativní	k2eAgFnSc1d1	federativní
Republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
vám	vy	k3xPp2nPc3	vy
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidíte	vidět	k5eAaImIp2nP	vidět
pravopisnou	pravopisný	k2eAgFnSc4d1	pravopisná
chybu	chyba	k1gFnSc4	chyba
<g/>
?	?	kIx.	?
</s>
