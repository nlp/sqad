<p>
<s>
Kapusta	kapusta	k1gFnSc1	kapusta
neboli	neboli	k8xC	neboli
brukev	brukev	k1gFnSc1	brukev
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc4d1	české
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
dvouleté	dvouletý	k2eAgFnSc2d1	dvouletá
košťálové	košťálový	k2eAgFnSc2d1	košťálová
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
kapusty	kapusta	k1gFnSc2	kapusta
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
středozemní	středozemní	k2eAgFnSc2d1	středozemní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Kapustu	kapusta	k1gFnSc4	kapusta
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
již	již	k9	již
Egypťané	Egypťan	k1gMnPc1	Egypťan
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
znali	znát	k5eAaImAgMnP	znát
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
druhy	druh	k1gInPc4	druh
kapusty	kapusta	k1gFnSc2	kapusta
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
kapusta	kapusta	k1gFnSc1	kapusta
objevuje	objevovat	k5eAaImIp3nS	objevovat
koncem	koncem	k7c2	koncem
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
o	o	k7c6	o
kapustě	kapusta	k1gFnSc6	kapusta
a	a	k8xC	a
jejích	její	k3xOp3gInPc6	její
blahodárných	blahodárný	k2eAgInPc6d1	blahodárný
účincích	účinek	k1gInPc6	účinek
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
se	se	k3xPyFc4	se
odrůdy	odrůda	k1gFnPc1	odrůda
podobaly	podobat	k5eAaImAgFnP	podobat
těm	ten	k3xDgMnPc3	ten
dnešním	dnešní	k2eAgMnPc3d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
začala	začít	k5eAaPmAgFnS	začít
kapusta	kapusta	k1gFnSc1	kapusta
pěstovat	pěstovat	k5eAaImF	pěstovat
a	a	k8xC	a
šlechtit	šlechtit	k5eAaImF	šlechtit
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
podobná	podobný	k2eAgFnSc1d1	podobná
starým	starý	k2eAgFnPc3d1	stará
odrůdám	odrůda	k1gFnPc3	odrůda
je	být	k5eAaImIp3nS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
kapusta	kapusta	k1gFnSc1	kapusta
hlávková	hlávkový	k2eAgFnSc1d1	hlávková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
oleracea	oleracea	k1gFnSc1	oleracea
convar	convar	k1gInSc1	convar
<g/>
.	.	kIx.	.
sabauda	sabauda	k1gMnSc1	sabauda
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
O.	O.	kA	O.
<g/>
F.	F.	kA	F.
<g/>
SCHULZ	Schulz	k1gMnSc1	Schulz
<g/>
.	.	kIx.	.
–	–	k?	–
hlávková	hlávkový	k2eAgFnSc1d1	hlávková
kapusta	kapusta	k1gFnSc1	kapusta
</s>
</p>
<p>
<s>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
oleracea	oleracea	k1gFnSc1	oleracea
convar	convar	k1gInSc1	convar
<g/>
.	.	kIx.	.
gemmifera	gemmifera	k1gFnSc1	gemmifera
(	(	kIx(	(
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Markgr	Markgr	k1gInSc1	Markgr
<g/>
.	.	kIx.	.
–	–	k?	–
růžičková	růžičkový	k2eAgFnSc1d1	růžičková
kapusta	kapusta	k1gFnSc1	kapusta
</s>
</p>
<p>
<s>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
oleracea	oleracea	k1gFnSc1	oleracea
var.	var.	k?	var.
medullosa	medullosa	k1gFnSc1	medullosa
Thell	Thell	k1gMnSc1	Thell
<g/>
.	.	kIx.	.
–	–	k?	–
dřeňová	dřeňový	k2eAgFnSc1d1	dřeňová
kapusta	kapusta	k1gFnSc1	kapusta
</s>
</p>
<p>
<s>
Brassica	Brassic	k2eAgFnSc1d1	Brassica
oleracea	oleracea	k1gFnSc1	oleracea
var.	var.	k?	var.
acephala	acephat	k5eAaPmAgFnS	acephat
Dc	Dc	k1gFnSc1	Dc
<g/>
.	.	kIx.	.
–	–	k?	–
jarmuz	jarmuz	k1gInSc1	jarmuz
(	(	kIx(	(
<g/>
krmná	krmný	k2eAgFnSc1d1	krmná
kapusta	kapusta	k1gFnSc1	kapusta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brassica	Brassica	k1gMnSc1	Brassica
oleracea	oleracea	k1gMnSc1	oleracea
var.	var.	k?	var.
sabellica	sabellica	k1gMnSc1	sabellica
L.	L.	kA	L.
–	–	k?	–
kadeřavá	kadeřavý	k2eAgFnSc1d1	kadeřavá
kapusta	kapusta	k1gFnSc1	kapusta
(	(	kIx(	(
<g/>
kadeřávek	kadeřávek	k1gInSc1	kadeřávek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
látky	látka	k1gFnPc4	látka
==	==	k?	==
</s>
</p>
<p>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
B9	B9	k1gFnSc1	B9
(	(	kIx(	(
<g/>
kyselina	kyselina	k1gFnSc1	kyselina
listová	listový	k2eAgFnSc1d1	listová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vitamin	vitamin	k1gInSc1	vitamin
C	C	kA	C
<g/>
,	,	kIx,	,
vitamin	vitamin	k1gInSc1	vitamin
E	E	kA	E
<g/>
,	,	kIx,	,
vitamin	vitamin	k1gInSc1	vitamin
K	K	kA	K
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
selen	selen	k1gInSc1	selen
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
sodík	sodík	k1gInSc1	sodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsoby	způsob	k1gInPc1	způsob
přípravy	příprava	k1gFnSc2	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kapustu	kapusta	k1gFnSc4	kapusta
můžeme	moct	k5eAaImIp1nP	moct
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vařit	vařit	k5eAaImF	vařit
<g/>
,	,	kIx,	,
smažit	smažit	k5eAaImF	smažit
<g/>
,	,	kIx,	,
dusit	dusit	k5eAaImF	dusit
<g/>
,	,	kIx,	,
zapékat	zapékat	k5eAaImF	zapékat
<g/>
,	,	kIx,	,
pařit	pařit	k5eAaImF	pařit
apod.	apod.	kA	apod.
Syrová	syrový	k2eAgFnSc1d1	syrová
se	se	k3xPyFc4	se
však	však	k9	však
nejí	jíst	k5eNaImIp3nS	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
do	do	k7c2	do
salátů	salát	k1gInPc2	salát
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
i	i	k9	i
do	do	k7c2	do
studených	studený	k2eAgInPc2d1	studený
salátů	salát	k1gInPc2	salát
ale	ale	k8xC	ale
před	před	k7c7	před
konzumací	konzumace	k1gFnSc7	konzumace
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
povařit	povařit	k5eAaPmF	povařit
ve	v	k7c6	v
vroucí	vroucí	k2eAgFnSc6d1	vroucí
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstitelství	pěstitelství	k1gNnSc2	pěstitelství
==	==	k?	==
</s>
</p>
<p>
<s>
Kapusta	kapusta	k1gFnSc1	kapusta
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
odolná	odolný	k2eAgFnSc1d1	odolná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vydrží	vydržet	k5eAaPmIp3nS	vydržet
i	i	k9	i
teplotu	teplota	k1gFnSc4	teplota
kolem	kolem	k7c2	kolem
-	-	kIx~	-
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
známek	známka	k1gFnPc2	známka
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
však	však	k9	však
kapusta	kapusta	k1gFnSc1	kapusta
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
Brassica	Brassica	k1gFnSc1	Brassica
oleracea	oleracea	k1gFnSc1	oleracea
==	==	k?	==
</s>
</p>
