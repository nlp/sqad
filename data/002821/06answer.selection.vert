<s>
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahen	k2eAgMnSc1d1	Mahen
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Antonín	Antonín	k1gMnSc1	Antonín
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Čáslav	Čáslav	k1gFnSc1	Čáslav
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
,	,	kIx,	,
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
