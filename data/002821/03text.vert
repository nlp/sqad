<s>
Jiří	Jiří	k1gMnSc1	Jiří
Mahen	Mahen	k2eAgMnSc1d1	Mahen
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Antonín	Antonín	k1gMnSc1	Antonín
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Čáslav	Čáslav	k1gFnSc1	Čáslav
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
,	,	kIx,	,
knihovník	knihovník	k1gMnSc1	knihovník
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
filozofickou	filozofický	k2eAgFnSc4d1	filozofická
fakultu	fakulta	k1gFnSc4	fakulta
<g/>
,	,	kIx,	,
obory	obor	k1gInPc4	obor
čeština	čeština	k1gFnSc1	čeština
a	a	k8xC	a
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
před	před	k7c7	před
závěrečnou	závěrečný	k2eAgFnSc7d1	závěrečná
zkouškou	zkouška	k1gFnSc7	zkouška
studia	studio	k1gNnSc2	studio
opustil	opustit	k5eAaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1919	[number]	k4	1919
byl	být	k5eAaImAgInS	být
redaktorem	redaktor	k1gMnSc7	redaktor
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1918	[number]	k4	1918
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
dramatik	dramatik	k1gMnSc1	dramatik
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nese	nést	k5eAaImIp3nS	nést
budova	budova	k1gFnSc1	budova
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Brno	Brno	k1gNnSc4	Brno
na	na	k7c6	na
Malinovského	Malinovský	k2eAgInSc2d1	Malinovský
náměstí	náměstí	k1gNnSc4	náměstí
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
byl	být	k5eAaImAgInS	být
knihovníkem	knihovník	k1gMnSc7	knihovník
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
i	i	k9	i
ředitelem	ředitel	k1gMnSc7	ředitel
městské	městský	k2eAgFnSc2d1	městská
knihovny	knihovna	k1gFnSc2	knihovna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejímuž	jejíž	k3xOyRp3gInSc3	jejíž
vybudování	vybudování	k1gNnSc4	vybudování
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
přispěl	přispět	k5eAaPmAgInS	přispět
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
Knihovna	knihovna	k1gFnSc1	knihovna
Jiřího	Jiří	k1gMnSc2	Jiří
Mahena	Mahen	k1gMnSc2	Mahen
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
život	život	k1gInSc1	život
ukončil	ukončit	k5eAaPmAgInS	ukončit
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c6	na
okupaci	okupace	k1gFnSc6	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
lyrická	lyrický	k2eAgFnSc1d1	lyrická
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
životním	životní	k2eAgInSc7d1	životní
postojem	postoj	k1gInSc7	postoj
tzv.	tzv.	kA	tzv.
generace	generace	k1gFnPc1	generace
anarchistických	anarchistický	k2eAgMnPc2d1	anarchistický
buřičů	buřič	k1gMnPc2	buřič
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
pozdější	pozdní	k2eAgFnSc1d2	pozdější
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
impresionismem	impresionismus	k1gInSc7	impresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Plamínky	plamínek	k1gInPc1	plamínek
Duha	duha	k1gFnSc1	duha
<g/>
:	:	kIx,	:
cyklus	cyklus	k1gInSc1	cyklus
veršů	verš	k1gInPc2	verš
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
Dostupné	dostupný	k2eAgNnSc4d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
Tiché	Tichá	k1gFnPc4	Tichá
srdce	srdce	k1gNnSc2	srdce
Balady	balada	k1gFnSc2	balada
Rozloučení	rozloučení	k1gNnSc2	rozloučení
s	s	k7c7	s
jihem	jih	k1gInSc7	jih
Požár	požár	k1gInSc1	požár
Tater	Tatra	k1gFnPc2	Tatra
Kamarádi	kamarádit	k5eAaImRp2nS	kamarádit
svobody	svoboda	k1gFnSc2	svoboda
-	-	kIx~	-
román	román	k1gInSc1	román
Podívíni	Podívín	k1gMnPc1	Podívín
Rybářská	rybářský	k2eAgFnSc1d1	rybářská
knížka	knížka	k1gFnSc1	knížka
Měsíc	měsíc	k1gInSc4	měsíc
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
Díže	díž	k1gFnSc2	díž
Dvě	dva	k4xCgFnPc4	dva
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
-	-	kIx~	-
Daemoni	Daemon	k1gMnPc1	Daemon
<g/>
,	,	kIx,	,
Opájejme	opájet	k5eAaImRp1nP	opájet
se	se	k3xPyFc4	se
<g/>
!	!	kIx.	!
</s>
<s>
Co	co	k3yRnSc1	co
mi	já	k3xPp1nSc3	já
liška	liška	k1gFnSc1	liška
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
Dvanáct	dvanáct	k4xCc4	dvanáct
pohádek	pohádka	k1gFnPc2	pohádka
Před	před	k7c7	před
oponou	opona	k1gFnSc7	opona
Režisérův	režisérův	k2eAgMnSc1d1	režisérův
zápisník	zápisník	k1gMnSc1	zápisník
Janošík	Janošík	k1gMnSc1	Janošík
Nebe	nebe	k1gNnSc4	nebe
<g/>
,	,	kIx,	,
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
ráj	ráj	k1gInSc4	ráj
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
Dostupné	dostupný	k2eAgFnSc6d1	dostupná
online	onlinout	k5eAaPmIp3nS	onlinout
Generace	generace	k1gFnSc1	generace
Dezertér	dezertér	k1gMnSc1	dezertér
Ulička	ulička	k1gFnSc1	ulička
odvahy	odvaha	k1gFnSc2	odvaha
Nasredin	Nasredina	k1gFnPc2	Nasredina
čili	čili	k8xC	čili
Nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
pomsta	pomsta	k1gFnSc1	pomsta
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
-	-	kIx~	-
libreta	libreto	k1gNnSc2	libreto
</s>
