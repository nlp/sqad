<p>
<s>
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Knud	Knuda	k1gFnPc2	Knuda
Pedersen	Pedersen	k1gInSc4	Pedersen
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1859	[number]	k4	1859
Vå	Vå	k1gFnSc2	Vå
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
Grimstad	Grimstad	k1gInSc1	Grimstad
<g/>
,	,	kIx,	,
Nø	Nø	k1gMnSc1	Nø
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
norský	norský	k2eAgMnSc1d1	norský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1920	[number]	k4	1920
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Hamsun	Hamsun	k1gMnSc1	Hamsun
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
rodině	rodina	k1gFnSc6	rodina
krejčího	krejčí	k1gMnSc2	krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Neveselé	Neveselé	k2eAgNnSc4d1	Neveselé
mládí	mládí	k1gNnSc4	mládí
prožil	prožít	k5eAaPmAgMnS	prožít
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
rodiči	rodič	k1gMnPc7	rodič
poslán	poslat	k5eAaPmNgMnS	poslat
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
ke	k	k7c3	k
strýci	strýc	k1gMnSc3	strýc
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yIgMnSc2	který
záhy	záhy	k6eAd1	záhy
utekl	utéct	k5eAaPmAgMnS	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
pracoval	pracovat	k5eAaImAgMnS	pracovat
coby	coby	k?	coby
učeň	učeň	k1gMnSc1	učeň
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatelské	spisovatelský	k2eAgFnPc4d1	spisovatelská
ambice	ambice	k1gFnPc4	ambice
měl	mít	k5eAaImAgMnS	mít
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
povídku	povídka	k1gFnSc4	povídka
Bjø	Bjø	k1gFnSc2	Bjø
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ale	ale	k8xC	ale
u	u	k7c2	u
vydavatele	vydavatel	k1gMnSc2	vydavatel
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
do	do	k7c2	do
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1882-1885	[number]	k4	1882-1885
a	a	k8xC	a
1886-1888	[number]	k4	1886-1888
žil	žíla	k1gFnPc2	žíla
jako	jako	k8xS	jako
vystěhovalec	vystěhovalec	k1gMnSc1	vystěhovalec
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
rybář	rybář	k1gMnSc1	rybář
a	a	k8xC	a
průvodčí	průvodčí	k1gMnSc1	průvodčí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
začal	začít	k5eAaPmAgInS	začít
hluboce	hluboko	k6eAd1	hluboko
nesouhlasit	souhlasit	k5eNaImF	souhlasit
s	s	k7c7	s
"	"	kIx"	"
<g/>
anglickým	anglický	k2eAgInSc7d1	anglický
a	a	k8xC	a
americkým	americký	k2eAgInSc7d1	americký
tržním	tržní	k2eAgInSc7d1	tržní
a	a	k8xC	a
nemorálním	morální	k2eNgInSc7d1	nemorální
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
společnost	společnost	k1gFnSc4	společnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
podrobil	podrobit	k5eAaPmAgMnS	podrobit
ostré	ostrý	k2eAgFnSc3d1	ostrá
kritice	kritika	k1gFnSc3	kritika
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
eseji	esej	k1gFnSc6	esej
Fra	Fra	k1gFnSc2	Fra
det	det	k?	det
moderne	modernout	k5eAaPmIp3nS	modernout
Amerikas	Amerikas	k1gInSc1	Amerikas
andsliv	andslit	k5eAaPmDgInS	andslit
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Z	z	k7c2	z
duchovního	duchovní	k2eAgInSc2d1	duchovní
života	život	k1gInSc2	život
moderní	moderní	k2eAgFnSc2d1	moderní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
díle	dílo	k1gNnSc6	dílo
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
Anglosasy	Anglosas	k1gMnPc4	Anglosas
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgMnPc4d1	hlavní
iniciátory	iniciátor	k1gMnPc4	iniciátor
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ničí	ničit	k5eAaImIp3nS	ničit
odvěké	odvěký	k2eAgFnPc4d1	odvěká
morální	morální	k2eAgFnSc4d1	morální
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
jeho	jeho	k3xOp3gInPc6	jeho
názorech	názor	k1gInPc6	názor
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vystopovat	vystopovat	k5eAaPmF	vystopovat
prvotní	prvotní	k2eAgFnPc4d1	prvotní
příčiny	příčina	k1gFnPc4	příčina
jeho	jeho	k3xOp3gInSc2	jeho
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
příklonu	příklon	k1gInSc2	příklon
k	k	k7c3	k
německému	německý	k2eAgInSc3d1	německý
nacismu	nacismus	k1gInSc3	nacismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
se	se	k3xPyFc4	se
Knut	knuta	k1gFnPc2	knuta
navrátil	navrátit	k5eAaPmAgMnS	navrátit
do	do	k7c2	do
Kodaně	Kodaň	k1gFnSc2	Kodaň
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1898-1899	[number]	k4	1898-1899
cestoval	cestovat	k5eAaImAgInS	cestovat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
žil	žít	k5eAaImAgMnS	žít
opět	opět	k6eAd1	opět
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
poblíž	poblíž	k7c2	poblíž
Narviku	Narvik	k1gInSc2	Narvik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvního	první	k4xOgNnSc2	první
uznání	uznání	k1gNnSc2	uznání
se	se	k3xPyFc4	se
Hamsunovi	Hamsun	k1gMnSc3	Hamsun
dostalo	dostat	k5eAaPmAgNnS	dostat
za	za	k7c4	za
částečně	částečně	k6eAd1	částečně
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
román	román	k1gInSc4	román
Sult	Sult	k2eAgInSc4d1	Sult
(	(	kIx(	(
<g/>
Hlad	hlad	k1gInSc4	hlad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
šílenství	šílenství	k1gNnSc1	šílenství
mladého	mladý	k2eAgMnSc2d1	mladý
spisovatele	spisovatel	k1gMnSc2	spisovatel
způsobené	způsobený	k2eAgNnSc4d1	způsobené
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
chudobou	chudoba	k1gFnSc7	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
přitom	přitom	k6eAd1	přitom
propracovaných	propracovaný	k2eAgInPc2d1	propracovaný
psychologických	psychologický	k2eAgInPc2d1	psychologický
monologů	monolog	k1gInPc2	monolog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
mezním	mezní	k2eAgInSc7d1	mezní
dílem	díl	k1gInSc7	díl
celé	celý	k2eAgFnSc2d1	celá
moderní	moderní	k2eAgFnSc2d1	moderní
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
dílo	dílo	k1gNnSc1	dílo
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
spisovatelů	spisovatel	k1gMnPc2	spisovatel
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
rovněž	rovněž	k9	rovněž
řešili	řešit	k5eAaImAgMnP	řešit
problematiku	problematika	k1gFnSc4	problematika
života	život	k1gInSc2	život
jedince	jedinec	k1gMnSc2	jedinec
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
pak	pak	k6eAd1	pak
Hamsun	Hamsun	k1gMnSc1	Hamsun
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
k	k	k7c3	k
předchozí	předchozí	k2eAgFnSc3d1	předchozí
generaci	generace	k1gFnSc3	generace
naturalistů	naturalista	k1gMnPc2	naturalista
pomocí	pomocí	k7c2	pomocí
rafinované	rafinovaný	k2eAgFnSc2d1	rafinovaná
vypravěčské	vypravěčský	k2eAgFnSc2d1	vypravěčská
techniky	technika	k1gFnSc2	technika
s	s	k7c7	s
vnitřními	vnitřní	k2eAgInPc7d1	vnitřní
monology	monolog	k1gInPc7	monolog
sérii	série	k1gFnSc4	série
dušezpytných	dušezpytný	k2eAgInPc2d1	dušezpytný
románů	román	k1gInPc2	román
o	o	k7c6	o
rozervaných	rozervaný	k2eAgMnPc6d1	rozervaný
jedincích	jedinec	k1gMnPc6	jedinec
<g/>
,	,	kIx,	,
nezvládajících	zvládající	k2eNgMnPc6d1	nezvládající
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc2	jenž
předvedl	předvést	k5eAaPmAgMnS	předvést
svou	svůj	k3xOyFgFnSc4	svůj
koncepci	koncepce	k1gFnSc4	koncepce
složité	složitý	k2eAgFnSc2d1	složitá
psychiky	psychika	k1gFnSc2	psychika
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
mistrovskými	mistrovský	k2eAgInPc7d1	mistrovský
díly	díl	k1gInPc7	díl
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
především	především	k9	především
romány	román	k1gInPc1	román
Mystérie	mystérie	k1gFnSc2	mystérie
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
a	a	k8xC	a
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
romány	román	k1gInPc1	román
Blouznivci	blouznivec	k1gMnSc6	blouznivec
<g/>
,	,	kIx,	,
Podzimní	podzimní	k2eAgFnSc2d1	podzimní
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
Tlumeně	tlumeně	k6eAd1	tlumeně
hude	houst	k5eAaImIp3nS	houst
poutník	poutník	k1gMnSc1	poutník
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
tematikou	tematika	k1gFnSc7	tematika
samotářských	samotářský	k2eAgMnPc2d1	samotářský
romantických	romantický	k2eAgMnPc2d1	romantický
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
zesílil	zesílit	k5eAaPmAgInS	zesílit
Hamsun	Hamsun	k1gInSc1	Hamsun
v	v	k7c6	v
románech	román	k1gInPc6	román
Děti	dítě	k1gFnPc1	dítě
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
Město	město	k1gNnSc1	město
Segelfoss	Segelfoss	k1gInSc4	Segelfoss
kritiku	kritik	k1gMnSc3	kritik
moderní	moderní	k2eAgFnSc2d1	moderní
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
vydal	vydat	k5eAaPmAgInS	vydat
své	svůj	k3xOyFgNnSc4	svůj
vrcholné	vrcholný	k2eAgNnSc1d1	vrcholné
dílo	dílo	k1gNnSc1	dílo
Matka	matka	k1gFnSc1	matka
země	země	k1gFnSc1	země
s	s	k7c7	s
ideou	idea	k1gFnSc7	idea
osvobozujícího	osvobozující	k2eAgInSc2d1	osvobozující
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
rustikálnímu	rustikální	k2eAgInSc3d1	rustikální
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
"	"	kIx"	"
<g/>
...	...	k?	...
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
monumentální	monumentální	k2eAgNnSc1d1	monumentální
dílo	dílo	k1gNnSc1	dílo
Matka	matka	k1gFnSc1	matka
Země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Hamsun	Hamsun	k1gMnSc1	Hamsun
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
uznávanou	uznávaný	k2eAgFnSc7d1	uznávaná
osobností	osobnost	k1gFnSc7	osobnost
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc7d1	známá
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
začal	začít	k5eAaPmAgInS	začít
Hamsun	Hamsun	k1gInSc1	Hamsun
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
věku	věk	k1gInSc6	věk
podporovat	podporovat	k5eAaImF	podporovat
pronacistický	pronacistický	k2eAgInSc4d1	pronacistický
režim	režim	k1gInSc4	režim
norského	norský	k2eAgMnSc2d1	norský
kolaboranta	kolaborant	k1gMnSc2	kolaborant
Vidkuna	Vidkuna	k1gFnSc1	Vidkuna
Quislinga	quisling	k1gMnSc2	quisling
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
jeho	jeho	k3xOp3gInSc6	jeho
kroku	krok	k1gInSc6	krok
měla	mít	k5eAaImAgFnS	mít
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
i	i	k8xC	i
Hamsunova	Hamsunův	k2eAgFnSc1d1	Hamsunova
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Hamsun	Hamsun	k1gMnSc1	Hamsun
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
kompromitoval	kompromitovat	k5eAaBmAgMnS	kompromitovat
a	a	k8xC	a
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
se	se	k3xPyFc4	se
například	například	k6eAd1	například
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
Josephem	Joseph	k1gInSc7	Joseph
Goebbelsem	Goebbels	k1gInSc7	Goebbels
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
mu	on	k3xPp3gMnSc3	on
svou	svůj	k3xOyFgFnSc4	svůj
medaili	medaile	k1gFnSc4	medaile
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Hamsun	Hamsun	k1gMnSc1	Hamsun
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
dokonce	dokonce	k9	dokonce
i	i	k9	i
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
odvolat	odvolat	k5eAaPmF	odvolat
říšského	říšský	k2eAgMnSc4d1	říšský
komisaře	komisař	k1gMnSc4	komisař
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
Josefa	Josef	k1gMnSc2	Josef
Terbovena	Terboven	k2eAgFnSc1d1	Terboven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Hitlerově	Hitlerův	k2eAgFnSc6d1	Hitlerova
smrti	smrt	k1gFnSc6	smrt
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
otištěny	otisknout	k5eAaPmNgInP	otisknout
Hamsunovy	Hamsunův	k2eAgInPc1d1	Hamsunův
články	článek	k1gInPc1	článek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
Hitler	Hitler	k1gMnSc1	Hitler
nekriticky	kriticky	k6eNd1	kriticky
vychvalován	vychvalovat	k5eAaImNgMnS	vychvalovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
Hamsun	Hamsun	k1gInSc1	Hamsun
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
psychiatrické	psychiatrický	k2eAgFnSc2d1	psychiatrická
léčebny	léčebna	k1gFnSc2	léčebna
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
do	do	k7c2	do
domova	domov	k1gInSc2	domov
důchodců	důchodce	k1gMnPc2	důchodce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Hamsunova	Hamsunův	k2eAgInSc2d1	Hamsunův
případu	případ	k1gInSc2	případ
byly	být	k5eAaImAgFnP	být
norskými	norský	k2eAgFnPc7d1	norská
lékaři	lékař	k1gMnPc1	lékař
jeho	jeho	k3xOp3gFnSc2	jeho
psychické	psychický	k2eAgFnSc2d1	psychická
schopnosti	schopnost	k1gFnSc2	schopnost
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c2	za
"	"	kIx"	"
<g/>
trvale	trvale	k6eAd1	trvale
oslabené	oslabený	k2eAgInPc1d1	oslabený
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
kolaboraci	kolaborace	k1gFnSc4	kolaborace
odsouzen	odsouzen	k2eAgMnSc1d1	odsouzen
k	k	k7c3	k
pokutě	pokuta	k1gFnSc3	pokuta
325	[number]	k4	325
000	[number]	k4	000
norských	norský	k2eAgFnPc2d1	norská
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Hamsun	Hamsun	k1gMnSc1	Hamsun
sám	sám	k3xTgMnSc1	sám
toto	tento	k3xDgNnSc4	tento
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Po	po	k7c6	po
zarostlých	zarostlý	k2eAgFnPc6d1	zarostlá
stezkách	stezka	k1gFnPc6	stezka
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc1d1	vydaná
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
obhajoby	obhajoba	k1gFnSc2	obhajoba
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
nadhled	nadhled	k1gInSc4	nadhled
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
sebereflexe	sebereflexe	k1gFnSc2	sebereflexe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Hamsunově	Hamsunův	k2eAgFnSc6d1	Hamsunova
smrti	smrt	k1gFnSc6	smrt
byla	být	k5eAaImAgFnS	být
témata	téma	k1gNnPc4	téma
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
a	a	k8xC	a
životě	život	k1gInSc6	život
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
tabuizována	tabuizován	k2eAgFnSc1d1	tabuizována
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
svou	svůj	k3xOyFgFnSc7	svůj
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
,	,	kIx,	,
poetickou	poetický	k2eAgFnSc7d1	poetická
a	a	k8xC	a
stylistickou	stylistický	k2eAgFnSc7d1	stylistická
hodnotou	hodnota	k1gFnSc7	hodnota
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
(	(	kIx(	(
<g/>
i	i	k8xC	i
při	při	k7c6	při
vypjatém	vypjatý	k2eAgInSc6d1	vypjatý
individualismu	individualismus	k1gInSc6	individualismus
<g/>
)	)	kIx)	)
svědectvím	svědectví	k1gNnSc7	svědectví
úporného	úporný	k2eAgNnSc2d1	úporné
hledání	hledání	k1gNnSc2	hledání
<g/>
,	,	kIx,	,
lidských	lidský	k2eAgFnPc2d1	lidská
tužeb	tužba	k1gFnPc2	tužba
<g/>
,	,	kIx,	,
omylů	omyl	k1gInPc2	omyl
a	a	k8xC	a
zmatků	zmatek	k1gInPc2	zmatek
<g/>
,	,	kIx,	,
jednoty	jednota	k1gFnSc2	jednota
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
snu	sen	k1gInSc2	sen
a	a	k8xC	a
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
ústřední	ústřední	k2eAgFnSc7d1	ústřední
postavou	postava	k1gFnSc7	postava
jeh	jho	k1gNnPc2	jho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
osamocený	osamocený	k2eAgMnSc1d1	osamocený
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ztratil	ztratit	k5eAaPmAgMnS	ztratit
důvěru	důvěra	k1gFnSc4	důvěra
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
poražen	poražen	k2eAgInSc1d1	poražen
<g/>
,	,	kIx,	,
smeten	smeten	k2eAgInSc1d1	smeten
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
společenskými	společenský	k2eAgNnPc7d1	společenské
pravidly	pravidlo	k1gNnPc7	pravidlo
a	a	k8xC	a
sobeckostí	sobeckost	k1gFnSc7	sobeckost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
neodvratitelným	odvratitelný	k2eNgInPc3d1	neodvratitelný
zákonům	zákon	k1gInPc3	zákon
vítězství	vítězství	k1gNnSc2	vítězství
silnějšího	silný	k2eAgNnSc2d2	silnější
(	(	kIx(	(
<g/>
Hamsun	Hamsun	k1gNnSc1	Hamsun
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
Nietzscheho	Nietzsche	k1gMnSc2	Nietzsche
filozofií	filozofie	k1gFnPc2	filozofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
švédský	švédský	k2eAgMnSc1d1	švédský
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Troell	Troell	k1gMnSc1	Troell
natočil	natočit	k5eAaBmAgMnS	natočit
filmovou	filmový	k2eAgFnSc4d1	filmová
biografii	biografie	k1gFnSc4	biografie
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
<g/>
.	.	kIx.	.
</s>
<s>
Knuta	knuta	k1gFnSc1	knuta
Hamsuna	Hamsuna	k1gFnSc1	Hamsuna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Max	Max	k1gMnSc1	Max
von	von	k1gInSc4	von
Sydow	Sydow	k1gFnSc2	Sydow
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
za	za	k7c4	za
roli	role	k1gFnSc4	role
dostalo	dostat	k5eAaPmAgNnS	dostat
několika	několik	k4yIc2	několik
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Básně	báseň	k1gFnSc2	báseň
===	===	k?	===
</s>
</p>
<p>
<s>
Det	Det	k?	Det
ville	ville	k1gInSc1	ville
kor	kor	k?	kor
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Divoký	divoký	k2eAgInSc1d1	divoký
chór	chór	k1gInSc1	chór
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
autorova	autorův	k2eAgFnSc1d1	autorova
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
milníků	milník	k1gInPc2	milník
norské	norský	k2eAgFnSc2d1	norská
lyriky	lyrika	k1gFnSc2	lyrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dramata	drama	k1gNnPc4	drama
===	===	k?	===
</s>
</p>
<p>
<s>
Ved	Ved	k?	Ved
rikets	rikets	k1gInSc1	rikets
port	port	k1gInSc1	port
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Před	před	k7c7	před
branou	brána	k1gFnSc7	brána
království	království	k1gNnSc4	království
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Livets	Livets	k1gInSc1	Livets
spill	spill	k1gInSc1	spill
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
Hra	hra	k1gFnSc1	hra
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
a	a	k8xC	a
Aftenrø	Aftenrø	k1gMnSc1	Aftenrø
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Večerní	večerní	k2eAgInPc1d1	večerní
červánky	červánek	k1gInPc1	červánek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatická	dramatický	k2eAgFnSc1d1	dramatická
trilogie	trilogie	k1gFnSc1	trilogie
o	o	k7c6	o
měnícím	měnící	k2eAgMnSc6d1	měnící
se	se	k3xPyFc4	se
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
pravdě	pravda	k1gFnSc3	pravda
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
životních	životní	k2eAgFnPc6d1	životní
etapách	etapa	k1gFnPc6	etapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Munken	Munken	k2eAgInSc1d1	Munken
Vendt	Vendt	k1gInSc1	Vendt
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historická	historický	k2eAgFnSc1d1	historická
romantická	romantický	k2eAgFnSc1d1	romantická
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dronning	Dronning	k1gInSc1	Dronning
Tamara	Tamara	k1gFnSc1	Tamara
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
Královna	královna	k1gFnSc1	královna
Tamara	Tamara	k1gFnSc1	Tamara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
byl	být	k5eAaImAgMnS	být
autor	autor	k1gMnSc1	autor
inspirován	inspirován	k2eAgMnSc1d1	inspirován
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Gruzii	Gruzie	k1gFnSc6	Gruzie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Livet	Livet	k1gMnSc1	Livet
ivold	ivold	k1gMnSc1	ivold
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
život	život	k1gInSc1	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
satirická	satirický	k2eAgFnSc1d1	satirická
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
</s>
</p>
<p>
<s>
===	===	k?	===
Próza	próza	k1gFnSc1	próza
===	===	k?	===
</s>
</p>
<p>
<s>
Bjø	Bjø	k?	Bjø
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
autorova	autorův	k2eAgFnSc1d1	autorova
vydaná	vydaný	k2eAgFnSc1d1	vydaná
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Fra	Fra	k?	Fra
det	det	k?	det
moderne	modernout	k5eAaPmIp3nS	modernout
Amerikas	Amerikas	k1gInSc1	Amerikas
andsliv	andslit	k5eAaPmDgInS	andslit
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Z	z	k7c2	z
duchovního	duchovní	k2eAgInSc2d1	duchovní
života	život	k1gInSc2	život
moderní	moderní	k2eAgFnSc2d1	moderní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
esej	esej	k1gInSc1	esej
odsuzující	odsuzující	k2eAgInSc1d1	odsuzující
Anglosasy	Anglosas	k1gMnPc7	Anglosas
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgInPc4d1	hlavní
iniciátory	iniciátor	k1gInPc4	iniciátor
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
kapitalismu	kapitalismus	k1gInSc2	kapitalismus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ničí	ničit	k5eAaImIp3nS	ničit
odvěké	odvěký	k2eAgFnPc4d1	odvěká
morální	morální	k2eAgFnSc4d1	morální
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sult	Sult	k1gInSc1	Sult
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
Hlad	hlad	k1gInSc1	hlad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
napsaný	napsaný	k2eAgInSc4d1	napsaný
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
vystihující	vystihující	k2eAgFnSc2d1	vystihující
formou	forma	k1gFnSc7	forma
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
monologu	monolog	k1gInSc2	monolog
složitou	složitý	k2eAgFnSc4d1	složitá
psychiku	psychika	k1gFnSc4	psychika
moderního	moderní	k2eAgMnSc2d1	moderní
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
reminiscenci	reminiscence	k1gFnSc6	reminiscence
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
hladová	hladový	k2eAgNnPc4d1	hladové
léta	léto	k1gNnPc4	léto
začínajícího	začínající	k2eAgMnSc2d1	začínající
spisovatele	spisovatel	k1gMnSc2	spisovatel
v	v	k7c6	v
Kristianii	Kristianie	k1gFnSc6	Kristianie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mysterier	Mysterier	k1gInSc1	Mysterier
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Mystérie	mystérie	k1gFnSc1	mystérie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
promyšlenou	promyšlený	k2eAgFnSc7d1	promyšlená
vypravěčskou	vypravěčský	k2eAgFnSc7d1	vypravěčská
technikou	technika	k1gFnSc7	technika
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
nevyzpytatelnost	nevyzpytatelnost	k1gFnSc1	nevyzpytatelnost
člověka	člověk	k1gMnSc2	člověk
v	v	k7c6	v
polemice	polemika	k1gFnSc6	polemika
se	s	k7c7	s
soudobým	soudobý	k2eAgInSc7d1	soudobý
pragmatickým	pragmatický	k2eAgInSc7d1	pragmatický
racionalismem	racionalismus	k1gInSc7	racionalismus
(	(	kIx(	(
<g/>
román	román	k1gInSc1	román
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
nelibost	nelibost	k1gFnSc4	nelibost
výstředními	výstřední	k2eAgInPc7d1	výstřední
názory	názor	k1gInPc7	názor
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redaktø	Redaktø	k?	Redaktø
Lynge	Lynge	k1gInSc1	Lynge
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ny	Ny	k1gMnSc1	Ny
Jord	Jord	k1gMnSc1	Jord
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
romány	román	k1gInPc1	román
napsané	napsaný	k2eAgInPc1d1	napsaný
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
s	s	k7c7	s
jedinečným	jedinečný	k2eAgNnSc7d1	jedinečné
vylíčením	vylíčení	k1gNnSc7	vylíčení
severonorské	severonorský	k2eAgFnSc2d1	severonorský
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Victoria	Victorium	k1gNnPc1	Victorium
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyrický	lyrický	k2eAgInSc1d1	lyrický
román	román	k1gInSc1	román
o	o	k7c6	o
osudové	osudový	k2eAgFnSc6d1	osudová
<g/>
,	,	kIx,	,
nedosažitelné	dosažitelný	k2eNgFnSc3d1	nedosažitelná
lásce	láska	k1gFnSc3	láska
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Hamsun	Hamsun	k1gMnSc1	Hamsun
dovršil	dovršit	k5eAaPmAgMnS	dovršit
své	svůj	k3xOyFgNnSc4	svůj
stylové	stylový	k2eAgNnSc4d1	stylové
novátorství	novátorství	k1gNnSc4	novátorství
a	a	k8xC	a
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vystihl	vystihnout	k5eAaPmAgMnS	vystihnout
psychiku	psychika	k1gFnSc4	psychika
ženy	žena	k1gFnSc2	žena
i	i	k8xC	i
hluboké	hluboký	k2eAgNnSc4d1	hluboké
kouzlo	kouzlo	k1gNnSc4	kouzlo
a	a	k8xC	a
jemné	jemný	k2eAgInPc4d1	jemný
odstíny	odstín	k1gInPc4	odstín
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
žárlivosti	žárlivost	k1gFnSc2	žárlivost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Siesta	siesta	k1gFnSc1	siesta
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
I	i	k9	i
Æ	Æ	k?	Æ
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
,	,	kIx,	,
V	v	k7c6	v
pohádkové	pohádkový	k2eAgFnSc6d1	pohádková
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Under	Under	k1gInSc1	Under
halvmanen	halvmanen	k1gInSc1	halvmanen
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Pod	pod	k7c7	pod
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
impresionistické	impresionistický	k2eAgFnPc4d1	impresionistická
reportáže	reportáž	k1gFnPc4	reportáž
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svæ	Svæ	k?	Svæ
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
Blouznivci	blouznivec	k1gMnPc7	blouznivec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Benoni	Benon	k1gMnPc1	Benon
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rosa	Rosa	k1gMnSc1	Rosa
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
humorné	humorný	k2eAgInPc1d1	humorný
romány	román	k1gInPc1	román
ze	z	k7c2	z
severonorského	severonorský	k2eAgNnSc2d1	severonorský
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Under	Under	k1gInSc1	Under
hoststajrnen	hoststajrnen	k1gInSc1	hoststajrnen
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Podzimní	podzimní	k2eAgFnPc4d1	podzimní
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
En	En	k1gFnSc1	En
vandrer	vandrer	k1gInSc1	vandrer
spiller	spiller	k1gInSc1	spiller
med	med	k1gInSc1	med
sordin	sordina	k1gFnPc2	sordina
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Tlumeně	tlumeně	k6eAd1	tlumeně
hude	houst	k5eAaImIp3nS	houst
poutník	poutník	k1gMnSc1	poutník
<g/>
)	)	kIx)	)
a	a	k8xC	a
Den	dna	k1gFnPc2	dna
sidste	sidsit	k5eAaImRp2nP	sidsit
Glæ	Glæ	k1gFnPc4	Glæ
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
radost	radost	k1gFnSc1	radost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trilogie	trilogie	k1gFnSc1	trilogie
teskně	teskně	k6eAd1	teskně
náladových	náladový	k2eAgInPc2d1	náladový
románů	román	k1gInPc2	román
o	o	k7c6	o
tulákovi	tulák	k1gMnSc6	tulák
Knutovi	Knut	k1gMnSc6	Knut
vyrovnávajícím	vyrovnávající	k2eAgFnPc3d1	vyrovnávající
se	se	k3xPyFc4	se
s	s	k7c7	s
nastupujícím	nastupující	k2eAgNnSc7d1	nastupující
stářím	stáří	k1gNnSc7	stáří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bø	Bø	k?	Bø
av	av	k?	av
Tiden	Tiden	k1gInSc1	Tiden
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Děti	dítě	k1gFnPc1	dítě
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
a	a	k8xC	a
Segelfoss	Segelfoss	k1gInSc1	Segelfoss
by	by	k9	by
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
Město	město	k1gNnSc1	město
Segelfoss	Segelfossa	k1gFnPc2	Segelfossa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
romány	román	k1gInPc1	román
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
kritikou	kritika	k1gFnSc7	kritika
moderní	moderní	k2eAgFnSc2d1	moderní
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Markens	Markens	k1gInSc1	Markens
Grø	Grø	k1gFnSc2	Grø
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Matka	matka	k1gFnSc1	matka
země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
ideu	idea	k1gFnSc4	idea
osvobozujícího	osvobozující	k2eAgInSc2d1	osvobozující
návratu	návrat	k1gInSc2	návrat
k	k	k7c3	k
rustikálnímu	rustikální	k2eAgInSc3d1	rustikální
životu	život	k1gInSc3	život
utvrzenou	utvrzený	k2eAgFnSc4d1	utvrzená
propuknutím	propuknutí	k1gNnSc7	propuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
autorovo	autorův	k2eAgNnSc4d1	autorovo
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
oslavující	oslavující	k2eAgInSc1d1	oslavující
venkov	venkov	k1gInSc1	venkov
<g/>
,	,	kIx,	,
přírodu	příroda	k1gFnSc4	příroda
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
velebností	velebnost	k1gFnSc7	velebnost
a	a	k8xC	a
moudrým	moudrý	k2eAgInSc7d1	moudrý
koloběhem	koloběh	k1gInSc7	koloběh
života	život	k1gInSc2	život
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k9	pak
práci	práce	k1gFnSc4	práce
rolníka	rolník	k1gMnSc2	rolník
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
opravdovou	opravdový	k2eAgFnSc7d1	opravdová
matkou	matka	k1gFnSc7	matka
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konerne	Konernout	k5eAaPmIp3nS	Konernout
ved	ved	k?	ved
vandposten	vandposten	k2eAgMnSc1d1	vandposten
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Ženy	žena	k1gFnPc1	žena
u	u	k7c2	u
studny	studna	k1gFnSc2	studna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krutě	krutě	k6eAd1	krutě
ironický	ironický	k2eAgInSc4d1	ironický
román	román	k1gInSc4	román
o	o	k7c6	o
maloměstě	maloměsto	k1gNnSc6	maloměsto
zkaženém	zkažený	k2eAgNnSc6d1	zkažené
bohatstvím	bohatství	k1gNnPc3	bohatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Siste	Siste	k5eAaPmIp2nP	Siste
kapitel	kapitel	k1gInSc4	kapitel
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
kapitola	kapitola	k1gFnSc1	kapitola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
o	o	k7c6	o
vyšinutých	vyšinutý	k2eAgInPc6d1	vyšinutý
lidech	lid	k1gInPc6	lid
v	v	k7c6	v
horském	horský	k2eAgNnSc6d1	horské
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Landstrykere	Landstrykrat	k5eAaPmIp3nS	Landstrykrat
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Tuláci	tulák	k1gMnPc1	tulák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
a	a	k8xC	a
Men	Men	k1gMnSc1	Men
livet	livet	k1gMnSc1	livet
lever	lever	k1gMnSc1	lever
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
však	však	k9	však
žije	žít	k5eAaImIp3nS	žít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
románová	románový	k2eAgFnSc1d1	románová
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
,	,	kIx,	,
další	další	k2eAgInSc1d1	další
vrchol	vrchol	k1gInSc1	vrchol
autorovy	autorův	k2eAgFnSc2d1	autorova
realistické	realistický	k2eAgFnSc2d1	realistická
tvorby	tvorba	k1gFnSc2	tvorba
a	a	k8xC	a
fabulačního	fabulační	k2eAgInSc2d1	fabulační
umu	um	k1gInSc2	um
<g/>
,	,	kIx,	,
poutavé	poutavý	k2eAgNnSc4d1	poutavé
a	a	k8xC	a
úsměvné	úsměvný	k2eAgNnSc4d1	úsměvné
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c4	o
pronikání	pronikání	k1gNnSc4	pronikání
podnikatelského	podnikatelský	k2eAgMnSc2d1	podnikatelský
ducha	duch	k1gMnSc2	duch
do	do	k7c2	do
severonorské	severonorský	k2eAgFnSc2d1	severonorský
osady	osada	k1gFnSc2	osada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ringen	Ringen	k1gInSc1	Ringen
sluttet	sluttet	k1gInSc1	sluttet
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
Kruh	kruh	k1gInSc1	kruh
uzavřen	uzavřen	k2eAgInSc1d1	uzavřen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
autorovo	autorův	k2eAgNnSc4d1	autorovo
románové	románový	k2eAgNnSc4d1	románové
dílo	dílo	k1gNnSc4	dílo
plné	plný	k2eAgFnSc2d1	plná
beznaděje	beznaděj	k1gFnSc2	beznaděj
</s>
</p>
<p>
<s>
Pa	Pa	kA	Pa
gjengrodde	gjengrodde	k6eAd1	gjengrodde
stier	stiero	k1gNnPc2	stiero
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Po	po	k7c6	po
zarostlých	zarostlý	k2eAgFnPc6d1	zarostlá
stezkách	stezka	k1gFnPc6	stezka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svěží	svěží	k2eAgFnPc1d1	svěží
a	a	k8xC	a
nad	nad	k7c4	nad
vlastní	vlastní	k2eAgFnSc4d1	vlastní
vinu	vina	k1gFnSc4	vina
povznesené	povznesený	k2eAgFnSc2d1	povznesená
zápisky	zápiska	k1gFnSc2	zápiska
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
internace	internace	k1gFnSc2	internace
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Česká	český	k2eAgNnPc1d1	české
vydání	vydání	k1gNnPc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Stanislav	Stanislav	k1gMnSc1	Stanislav
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Viktorie	Viktorie	k1gFnSc1	Viktorie
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
novelly	novella	k1gFnPc1	novella
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
19	[number]	k4	19
<g/>
xx	xx	k?	xx
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Václav	Václav	k1gMnSc1	Václav
Petrů	Petrů	k1gMnSc1	Petrů
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hlad	hlad	k1gMnSc1	hlad
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1917	[number]	k4	1917
a	a	k8xC	a
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Osten	osten	k1gInSc1	osten
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Rudolf	Rudolf	k1gMnSc1	Rudolf
Těsnohlídek	Těsnohlídka	k1gFnPc2	Těsnohlídka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mysterie	mysterie	k1gFnSc1	mysterie
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Dyrynk	Dyrynk	k1gMnSc1	Dyrynk
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
Veraikon	veraikon	k1gInSc1	veraikon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Gustav	Gustav	k1gMnSc1	Gustav
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
a	a	k8xC	a
1919	[number]	k4	1919
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Gustav	Gustav	k1gMnSc1	Gustav
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1918	[number]	k4	1918
a	a	k8xC	a
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
ze	z	k7c2	z
Sáby	Sába	k1gFnSc2	Sába
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Gustav	Gustav	k1gMnSc1	Gustav
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
,	,	kIx,	,
Kamilla	Kamilla	k1gFnSc1	Kamilla
Neumannová	Neumannová	k1gFnSc1	Neumannová
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1919	[number]	k4	1919
a	a	k8xC	a
1943	[number]	k4	1943
(	(	kIx(	(
<g/>
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Benoni	Benon	k1gMnPc1	Benon
<g/>
,	,	kIx,	,
Veraikon	veraikon	k1gInSc1	veraikon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Emil	Emil	k1gMnSc1	Emil
Walter	Walter	k1gMnSc1	Walter
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Blouznivci	blouznivec	k1gMnPc1	blouznivec
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ivan	Ivan	k1gMnSc1	Ivan
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rosa	Rosa	k1gFnSc1	Rosa
<g/>
,	,	kIx,	,
Veraikon	veraikon	k1gInSc1	veraikon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Emil	Emil	k1gMnSc1	Emil
Walter	Walter	k1gMnSc1	Walter
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Šuman	Šuman	k1gMnSc1	Šuman
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Podzimní	podzimní	k2eAgFnPc1d1	podzimní
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Šuman	Šuman	k1gMnSc1	Šuman
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tlumeně	tlumeně	k6eAd1	tlumeně
hude	houst	k5eAaImIp3nS	houst
poutník	poutník	k1gMnSc1	poutník
<g/>
,	,	kIx,	,
Topič	topič	k1gMnSc1	topič
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ivan	Ivan	k1gMnSc1	Ivan
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
půlměsíce	půlměsíc	k1gInSc2	půlměsíc
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Svěcený	svěcený	k2eAgMnSc1d1	svěcený
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
A.	A.	kA	A.
Chudák	chudák	k1gMnSc1	chudák
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
Toužimský	Toužimský	k2eAgMnSc1d1	Toužimský
a	a	k8xC	a
Moravec	Moravec	k1gMnSc1	Moravec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Urban	Urban	k1gMnSc1	Urban
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Trumpus	Trumpus	k1gMnSc1	Trumpus
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Benoni	Benon	k1gMnPc1	Benon
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Emil	Emil	k1gMnSc1	Emil
Walter	Walter	k1gMnSc1	Walter
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ivan	Ivan	k1gMnSc1	Ivan
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Segelfoss	Segelfoss	k1gInSc1	Segelfoss
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Šuman	Šuman	k1gMnSc1	Šuman
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Ivan	Ivan	k1gMnSc1	Ivan
Schulz	Schulz	k1gMnSc1	Schulz
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ženy	žena	k1gFnPc1	žena
u	u	k7c2	u
studny	studna	k1gFnSc2	studna
<g/>
,	,	kIx,	,
Hejda	Hejda	k1gMnSc1	Hejda
a	a	k8xC	a
Tuček	Tuček	k1gMnSc1	Tuček
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Zátiší	zátiší	k1gNnSc1	zátiší
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Viktor	Viktor	k1gMnSc1	Viktor
Šuman	Šuman	k1gMnSc1	Šuman
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Nová	nový	k2eAgFnSc1d1	nová
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hanuš	Hanuš	k1gMnSc1	Hanuš
Hackenschmied	Hackenschmied	k1gMnSc1	Hackenschmied
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Děti	dítě	k1gFnPc1	dítě
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Osada	osada	k1gFnSc1	osada
Segelfoss	Segelfoss	k1gInSc1	Segelfoss
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1928	[number]	k4	1928
a	a	k8xC	a
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuláci	tulák	k1gMnPc1	tulák
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,,	,,	k?	,,
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
kapitola	kapitola	k1gFnSc1	kapitola
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Žikeš	Žikeš	k1gMnSc1	Žikeš
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,,	,,	k?	,,
</s>
</p>
<p>
<s>
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Misterie	Misterie	k1gFnSc1	Misterie
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,,	,,	k?	,,
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,,	,,	k?	,,
</s>
</p>
<p>
<s>
Hlad	hlad	k1gMnSc1	hlad
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLHU	SNKLHU	kA	SNKLHU
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pohádkové	pohádkový	k2eAgFnSc6d1	pohádková
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Hynek	Hynek	k1gMnSc1	Hynek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Tulácké	tulácký	k2eAgInPc4d1	tulácký
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Krausová-Lesná	Krausová-Lesný	k2eAgFnSc1d1	Krausová-Lesný
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Královna	královna	k1gFnSc1	královna
ze	z	k7c2	z
Sáby	Sába	k1gFnSc2	Sába
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Milada	Milada	k1gFnSc1	Milada
Lesná-Krausová	Lesná-Krausový	k2eAgFnSc1d1	Lesná-Krausová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Viktorie	Viktorie	k1gFnSc1	Viktorie
<g/>
,	,	kIx,	,
SNDK	SNDK	kA	SNDK
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Božena	Božena	k1gFnSc1	Božena
Köllnová-Ehrmannová	Köllnová-Ehrmannová	k1gFnSc1	Köllnová-Ehrmannová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Albatros	albatros	k1gMnSc1	albatros
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matka	matka	k1gFnSc1	matka
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Božena	Božena	k1gFnSc1	Božena
Köllnová-Ehrmannová	Köllnová-Ehrmannová	k1gFnSc1	Köllnová-Ehrmannová
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Mysterie	mysterie	k1gFnSc1	mysterie
<g/>
,	,	kIx,	,
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Helena	Helena	k1gFnSc1	Helena
Kadečková	Kadečková	k1gFnSc1	Kadečková
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
branou	brána	k1gFnSc7	brána
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
xx	xx	k?	xx
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Petr	Petr	k1gMnSc1	Petr
Suchý	Suchý	k1gMnSc1	Suchý
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zarostlých	zarostlý	k2eAgFnPc6d1	zarostlá
stezkách	stezka	k1gFnPc6	stezka
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Veronika	Veronika	k1gFnSc1	Veronika
Dudková	Dudková	k1gFnSc1	Dudková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlad	hlad	k1gMnSc1	hlad
<g/>
,	,	kIx,	,
Dybbuk	Dybbuk	k1gMnSc1	Dybbuk
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Helena	Helena	k1gFnSc1	Helena
Kadečková	Kadečková	k1gFnSc1	Kadečková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HUMPÁL	HUMPÁL	kA	HUMPÁL
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
KADEČKOVÁ	KADEČKOVÁ	kA	KADEČKOVÁ
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
;	;	kIx,	;
PARENTE-ČAPKOVÁ	PARENTE-ČAPKOVÁ	k1gFnSc1	PARENTE-ČAPKOVÁ
<g/>
,	,	kIx,	,
Viola	Viola	k1gFnSc1	Viola
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
skandinávské	skandinávský	k2eAgFnPc1d1	skandinávská
literatury	literatura	k1gFnPc1	literatura
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
472	[number]	k4	472
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1174	[number]	k4	1174
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PALLAS	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
.	.	kIx.	.
</s>
<s>
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
a	a	k8xC	a
soudobá	soudobý	k2eAgFnSc1d1	soudobá
norská	norský	k2eAgFnSc1d1	norská
beletrie	beletrie	k1gFnSc1	beletrie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
87	[number]	k4	87
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsun	k1gNnSc4	Hamsun
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsun	k1gMnSc1	Hamsun
</s>
</p>
<p>
<s>
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
</s>
</p>
<p>
<s>
http://nobelprize.org/nobel_prizes/literature/laureates/1920/hamsun-bio.html	[url]	k1gMnSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1920/hamsun-bio.html
</s>
</p>
<p>
<s>
Hamsunova	Hamsunův	k2eAgFnSc1d1	Hamsunova
bibliografie	bibliografie	k1gFnSc1	bibliografie
</s>
</p>
<p>
<s>
Knut	knuta	k1gFnPc2	knuta
Hamsun	Hamsuna	k1gFnPc2	Hamsuna
Online	Onlin	k1gInSc5	Onlin
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
dánsky	dánsky	k6eAd1	dánsky
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
https://web.archive.org/web/20090706100054/http://www.kirjasto.sci.fi/khamsun.htm	[url]	k1gInSc1	https://web.archive.org/web/20090706100054/http://www.kirjasto.sci.fi/khamsun.htm
-	-	kIx~	-
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
