<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
zneužívání	zneužívání	k1gNnSc1	zneužívání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
terminologii	terminologie	k1gFnSc6	terminologie
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
skutková	skutkový	k2eAgFnSc1d1	skutková
podstata	podstata	k1gFnSc1	podstata
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vykonání	vykonání	k1gNnSc6	vykonání
soulože	soulož	k1gFnSc2	soulož
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pod	pod	k7c7	pod
stanovenou	stanovený	k2eAgFnSc7d1	stanovená
hranicí	hranice	k1gFnSc7	hranice
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
způsobu	způsob	k1gInSc6	způsob
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužití	zneužití	k1gNnSc2	zneužití
takové	takový	k3xDgFnSc2	takový
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
