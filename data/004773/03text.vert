<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
zneužívání	zneužívání	k1gNnSc1	zneužívání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
terminologii	terminologie	k1gFnSc6	terminologie
trestního	trestní	k2eAgNnSc2d1	trestní
práva	právo	k1gNnSc2	právo
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
skutková	skutkový	k2eAgFnSc1d1	skutková
podstata	podstata	k1gFnSc1	podstata
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vykonání	vykonání	k1gNnSc6	vykonání
soulože	soulož	k1gFnSc2	soulož
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
pod	pod	k7c7	pod
stanovenou	stanovený	k2eAgFnSc7d1	stanovená
hranicí	hranice	k1gFnSc7	hranice
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
způsobu	způsob	k1gInSc6	způsob
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužití	zneužití	k1gNnSc2	zneužití
takové	takový	k3xDgFnSc2	takový
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
a	a	k8xC	a
některých	některý	k3yIgNnPc6	některý
obdobích	období	k1gNnPc6	období
je	být	k5eAaImIp3nS	být
či	či	k9wB	či
byl	být	k5eAaImAgInS	být
shodný	shodný	k2eAgInSc1d1	shodný
pojem	pojem	k1gInSc1	pojem
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
pro	pro	k7c4	pro
sexuální	sexuální	k2eAgInSc4d1	sexuální
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
osobami	osoba	k1gFnPc7	osoba
v	v	k7c6	v
závislém	závislý	k2eAgNnSc6d1	závislé
postavení	postavení	k1gNnSc6	postavení
nebo	nebo	k8xC	nebo
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
způsobilostí	způsobilost	k1gFnSc7	způsobilost
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
zneužívání	zneužívání	k1gNnSc1	zneužívání
ve	v	k7c6	v
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
nebo	nebo	k8xC	nebo
psychologickém	psychologický	k2eAgInSc6d1	psychologický
významu	význam	k1gInSc6	význam
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pojmem	pojem	k1gInSc7	pojem
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
širším	široký	k2eAgInSc7d2	širší
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
takové	takový	k3xDgFnPc4	takový
formy	forma	k1gFnPc4	forma
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesplňují	splňovat	k5eNaImIp3nP	splňovat
kritéria	kritérion	k1gNnPc4	kritérion
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
podle	podle	k7c2	podle
trestního	trestní	k2eAgInSc2d1	trestní
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
judikatury	judikatura	k1gFnSc2	judikatura
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
i	i	k9	i
bezdotykové	bezdotykový	k2eAgNnSc4d1	bezdotykové
chování	chování	k1gNnSc4	chování
nebo	nebo	k8xC	nebo
nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
chování	chování	k1gNnSc4	chování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
svou	svůj	k3xOyFgFnSc7	svůj
formou	forma	k1gFnSc7	forma
nebo	nebo	k8xC	nebo
intenzitou	intenzita	k1gFnSc7	intenzita
se	se	k3xPyFc4	se
souloží	souložit	k5eAaImIp3nS	souložit
nesrovnatelné	srovnatelný	k2eNgNnSc1d1	nesrovnatelné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
opakovaně	opakovaně	k6eAd1	opakovaně
citována	citován	k2eAgFnSc1d1	citována
část	část	k1gFnSc1	část
definice	definice	k1gFnSc1	definice
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgFnPc1d1	pocházející
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
jakéhosi	jakýsi	k3yIgNnSc2	jakýsi
blíže	blízce	k6eAd2	blízce
nespecifikovaného	specifikovaný	k2eNgNnSc2d1	nespecifikované
doporučení	doporučení	k1gNnSc2	doporučení
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
komise	komise	k1gFnSc2	komise
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Empirický	empirický	k2eAgInSc1d1	empirický
výzkum	výzkum	k1gInSc1	výzkum
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sexuální	sexuální	k2eAgFnSc1d1	sexuální
orientace	orientace	k1gFnSc1	orientace
osob	osoba	k1gFnPc2	osoba
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
zneužívání	zneužívání	k1gNnSc2	zneužívání
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
zneužívání	zneužívání	k1gNnSc1	zneužívání
svéprávných	svéprávný	k2eAgFnPc2d1	svéprávná
osob	osoba	k1gFnPc2	osoba
nebo	nebo	k8xC	nebo
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
zneužívání	zneužívání	k1gNnSc1	zneužívání
menší	malý	k2eAgFnSc2d2	menší
intenzity	intenzita	k1gFnSc2	intenzita
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nespadá	spadat	k5eNaImIp3nS	spadat
pod	pod	k7c4	pod
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
spadat	spadat	k5eAaPmF	spadat
pod	pod	k7c4	pod
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
jiného	jiný	k2eAgInSc2d1	jiný
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
(	(	kIx(	(
<g/>
útisk	útisk	k1gInSc1	útisk
<g/>
,	,	kIx,	,
ohrožování	ohrožování	k1gNnSc1	ohrožování
výchovy	výchova	k1gFnSc2	výchova
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přestupku	přestupek	k1gInSc2	přestupek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
významu	význam	k1gInSc6	význam
není	být	k5eNaImIp3nS	být
pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
zneužívání	zneužívání	k1gNnSc1	zneužívání
právnickým	právnický	k2eAgFnPc3d1	právnická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
psychologickým	psychologický	k2eAgInSc7d1	psychologický
<g/>
,	,	kIx,	,
sociologickým	sociologický	k2eAgInSc7d1	sociologický
nebo	nebo	k8xC	nebo
etickým	etický	k2eAgInSc7d1	etický
termínem	termín	k1gInSc7	termín
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgNnSc2	jenž
naopak	naopak	k6eAd1	naopak
nemusejí	muset	k5eNaImIp3nP	muset
spadat	spadat	k5eAaPmF	spadat
některé	některý	k3yIgInPc4	některý
konsensuální	konsensuální	k2eAgInPc4d1	konsensuální
případy	případ	k1gInPc4	případ
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
bývá	bývat	k5eAaImIp3nS	bývat
například	například	k6eAd1	například
opakovaně	opakovaně	k6eAd1	opakovaně
citována	citován	k2eAgFnSc1d1	citována
část	část	k1gFnSc1	část
definice	definice	k1gFnSc1	definice
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
údajně	údajně	k6eAd1	údajně
byla	být	k5eAaImAgFnS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
neurčeném	určený	k2eNgInSc6d1	neurčený
dokumentu	dokument	k1gInSc6	dokument
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
komise	komise	k1gFnSc2	komise
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
platného	platný	k2eAgInSc2d1	platný
českého	český	k2eAgInSc2d1	český
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
(	(	kIx(	(
<g/>
§	§	k?	§
187	[number]	k4	187
tr	tr	k?	tr
<g/>
.	.	kIx.	.
z.	z.	k?	z.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
dopustí	dopustit	k5eAaPmIp3nS	dopustit
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vykoná	vykonat	k5eAaPmIp3nS	vykonat
soulož	soulož	k1gFnSc1	soulož
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
mladší	mladý	k2eAgMnSc1d2	mladší
než	než	k8xS	než
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
kdo	kdo	k3yInSc1	kdo
takové	takový	k3xDgInPc4	takový
osoby	osoba	k1gFnSc2	osoba
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
pohlavně	pohlavně	k6eAd1	pohlavně
zneužije	zneužít	k5eAaPmIp3nS	zneužít
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
zneužívaje	zneužívat	k5eAaImSgInS	zneužívat
závislosti	závislost	k1gFnSc6	závislost
osoby	osoba	k1gFnPc4	osoba
mladší	mladý	k2eAgFnPc4d2	mladší
než	než	k8xS	než
osmnáct	osmnáct	k4xCc1	osmnáct
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
osoby	osoba	k1gFnSc2	osoba
svěřené	svěřený	k2eAgFnSc2d1	svěřená
jeho	on	k3xPp3gInSc2	on
dozoru	dozor	k1gInSc2	dozor
<g/>
,	,	kIx,	,
přiměje	přimět	k5eAaPmIp3nS	přimět
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
mimomanželské	mimomanželský	k2eAgFnSc3d1	mimomanželská
souloži	soulož	k1gFnSc3	soulož
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kdo	kdo	k3yInSc1	kdo
takové	takový	k3xDgNnSc1	takový
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
zneužívaje	zneužívat	k5eAaImSgMnS	zneužívat
její	její	k3xOp3gFnSc2	její
závislosti	závislost	k1gFnSc2	závislost
<g/>
,	,	kIx,	,
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
pohlavně	pohlavně	k6eAd1	pohlavně
zneužije	zneužít	k5eAaPmIp3nS	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgInSc1d1	povinen
trestný	trestný	k2eAgInSc1d1	trestný
čin	čin	k1gInSc1	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
překazit	překazit	k5eAaPmF	překazit
(	(	kIx(	(
<g/>
§	§	k?	§
167	[number]	k4	167
tr	tr	k?	tr
<g/>
.	.	kIx.	.
z.	z.	k?	z.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nevztahuje	vztahovat	k5eNaImIp3nS	vztahovat
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
oznamovací	oznamovací	k2eAgFnSc1d1	oznamovací
povinnost	povinnost	k1gFnSc1	povinnost
(	(	kIx(	(
<g/>
t.	t.	k?	t.
j.	j.	k?	j.
obecně	obecně	k6eAd1	obecně
není	být	k5eNaImIp3nS	být
trestné	trestný	k2eAgNnSc1d1	trestné
neoznámení	neoznámení	k1gNnSc1	neoznámení
již	již	k6eAd1	již
dokonaného	dokonaný	k2eAgInSc2d1	dokonaný
činu	čin	k1gInSc2	čin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
sociálně-právní	sociálněrávní	k2eAgFnSc6d1	sociálně-právní
ochraně	ochrana	k1gFnSc6	ochrana
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
359	[number]	k4	359
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
však	však	k9	však
v	v	k7c6	v
§	§	k?	§
10	[number]	k4	10
stanoví	stanovit	k5eAaPmIp3nS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obce	obec	k1gFnPc1	obec
<g/>
,	,	kIx,	,
státní	státní	k2eAgInPc1d1	státní
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
pověřené	pověřený	k2eAgFnPc1d1	pověřená
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
školská	školská	k1gFnSc1	školská
zařízení	zařízení	k1gNnSc1	zařízení
a	a	k8xC	a
zdravotnická	zdravotnický	k2eAgNnPc1d1	zdravotnické
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
další	další	k2eAgNnPc4d1	další
zařízení	zařízení	k1gNnPc4	zařízení
určená	určený	k2eAgFnSc1d1	určená
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
jsou	být	k5eAaImIp3nP	být
povinny	povinen	k2eAgInPc1d1	povinen
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
oznámit	oznámit	k5eAaPmF	oznámit
obecnímu	obecní	k2eAgInSc3d1	obecní
úřadu	úřad	k1gInSc3	úřad
obce	obec	k1gFnSc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
<g/>
,	,	kIx,	,
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
prostitucí	prostituce	k1gFnSc7	prostituce
atd.	atd.	kA	atd.
S	s	k7c7	s
trestným	trestný	k2eAgInSc7d1	trestný
činem	čin	k1gInSc7	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
svádění	svádění	k1gNnPc2	svádění
k	k	k7c3	k
pohlavnímu	pohlavní	k2eAgInSc3d1	pohlavní
styku	styk	k1gInSc3	styk
(	(	kIx(	(
<g/>
§	§	k?	§
202	[number]	k4	202
tr	tr	k?	tr
<g/>
.	.	kIx.	.
z.	z.	k?	z.
<g/>
)	)	kIx)	)
Trestný	trestný	k2eAgInSc1d1	trestný
čin	čin	k1gInSc1	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
nesl	nést	k5eAaImAgInS	nést
dříve	dříve	k6eAd2	dříve
název	název	k1gInSc1	název
trestný	trestný	k2eAgInSc1d1	trestný
čin	čin	k1gInSc4	čin
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužití	zneužití	k1gNnSc2	zneužití
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
či	či	k8xC	či
zločin	zločin	k1gInSc1	zločin
zprznění	zprznění	k1gNnSc2	zprznění
(	(	kIx(	(
<g/>
do	do	k7c2	do
r.	r.	kA	r.
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgInSc1d1	bývalý
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
judikoval	judikovat	k5eAaBmAgMnS	judikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
skutkové	skutkový	k2eAgFnSc3d1	skutková
podstatě	podstata	k1gFnSc3	podstata
zločinu	zločin	k1gInSc2	zločin
zprznění	zprznění	k1gNnSc2	zprznění
stačí	stačit	k5eAaBmIp3nS	stačit
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
objektivní	objektivní	k2eAgMnSc1d1	objektivní
již	již	k6eAd1	již
pouhý	pouhý	k2eAgInSc1d1	pouhý
dotek	dotek	k1gInSc1	dotek
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
nahého	nahý	k2eAgMnSc2d1	nahý
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
R	R	kA	R
3417	[number]	k4	3417
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
sahání	sahání	k1gNnSc1	sahání
pod	pod	k7c4	pod
sukně	sukně	k1gFnPc4	sukně
nedospělého	dospělý	k2eNgNnSc2d1	nedospělé
děvčete	děvče	k1gNnSc2	děvče
(	(	kIx(	(
<g/>
R	R	kA	R
4151	[number]	k4	4151
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
nikoli	nikoli	k9	nikoli
však	však	k9	však
pouhé	pouhý	k2eAgNnSc4d1	pouhé
vyzvednutí	vyzvednutí	k1gNnSc4	vyzvednutí
sukně	sukně	k1gFnSc2	sukně
a	a	k8xC	a
obnažení	obnažení	k1gNnSc2	obnažení
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
R	R	kA	R
1622	[number]	k4	1622
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zneužíváním	zneužívání	k1gNnSc7	zneužívání
jest	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
dotýkání	dotýkání	k1gNnSc4	dotýkání
se	se	k3xPyFc4	se
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
údů	úd	k1gInPc2	úd
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
pakli	pakli	k8xS	pakli
směřovalo	směřovat	k5eAaImAgNnS	směřovat
k	k	k7c3	k
ukojení	ukojení	k1gNnSc3	ukojení
chlípných	chlípný	k2eAgInPc2d1	chlípný
chtíčů	chtíč	k1gInPc2	chtíč
pachatelových	pachatelův	k2eAgInPc2d1	pachatelův
(	(	kIx(	(
<g/>
R	R	kA	R
1230	[number]	k4	1230
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiném	jiný	k2eAgNnSc6d1	jiné
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
pak	pak	k6eAd1	pak
specifikoval	specifikovat	k5eAaBmAgMnS	specifikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
například	například	k6eAd1	například
o	o	k7c4	o
ohmatávání	ohmatávání	k1gNnSc4	ohmatávání
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
neb	neb	k8xC	neb
jiných	jiný	k2eAgFnPc6d1	jiná
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
i	i	k9	i
oděvem	oděv	k1gInSc7	oděv
zakrytých	zakrytý	k2eAgFnPc6d1	zakrytá
<g/>
)	)	kIx)	)
částech	část	k1gFnPc6	část
těla	tělo	k1gNnSc2	tělo
nedospělců	nedospělec	k1gMnPc2	nedospělec
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ukojení	ukojení	k1gNnSc2	ukojení
chlípných	chlípný	k2eAgInPc2d1	chlípný
chtíčů	chtíč	k1gInPc2	chtíč
(	(	kIx(	(
<g/>
R	R	kA	R
1192	[number]	k4	1192
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ukojením	ukojení	k1gNnSc7	ukojení
chtíčů	chtíč	k1gInPc2	chtíč
jest	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
podráždění	podráždění	k1gNnSc1	podráždění
nebo	nebo	k8xC	nebo
stupňování	stupňování	k1gNnSc1	stupňování
smyslnosti	smyslnost	k1gFnSc2	smyslnost
(	(	kIx(	(
<g/>
R	R	kA	R
316	[number]	k4	316
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
případy	případ	k1gInPc1	případ
obvinění	obvinění	k1gNnPc2	obvinění
z	z	k7c2	z
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
zneužívání	zneužívání	k1gNnSc2	zneužívání
dětí	dítě	k1gFnPc2	dítě
nebo	nebo	k8xC	nebo
mladistvých	mladistvý	k1gMnPc2	mladistvý
mohutně	mohutně	k6eAd1	mohutně
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
soustavně	soustavně	k6eAd1	soustavně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
medializovány	medializovat	k5eAaImNgFnP	medializovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
vlna	vlna	k1gFnSc1	vlna
obvinění	obvinění	k1gNnSc2	obvinění
katolických	katolický	k2eAgMnPc2d1	katolický
duchovních	duchovní	k1gMnPc2	duchovní
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
proces	proces	k1gInSc1	proces
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Michaelem	Michael	k1gMnSc7	Michael
Jacksonem	Jackson	k1gMnSc7	Jackson
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dramatického	dramatický	k2eAgInSc2d1	dramatický
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
zproštěn	zprostit	k5eAaPmNgMnS	zprostit
viny	vina	k1gFnSc2	vina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vzbuzoval	vzbuzovat	k5eAaImAgInS	vzbuzovat
velký	velký	k2eAgInSc1d1	velký
mediální	mediální	k2eAgInSc1d1	mediální
zájem	zájem	k1gInSc1	zájem
případ	případ	k1gInSc1	případ
sochaře	sochař	k1gMnSc2	sochař
Pavla	Pavel	k1gMnSc2	Pavel
Opočenského	opočenský	k2eAgMnSc2d1	opočenský
<g/>
,	,	kIx,	,
katarského	katarský	k2eAgMnSc2d1	katarský
prince	princ	k1gMnSc2	princ
Sáního	Sání	k1gMnSc2	Sání
nebo	nebo	k8xC	nebo
sbormistra	sbormistr	k1gMnSc2	sbormistr
Bohumila	Bohumil	k1gMnSc2	Bohumil
Kulínského	Kulínský	k2eAgMnSc2d1	Kulínský
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádnému	mimořádný	k2eAgInSc3d1	mimořádný
zájmu	zájem	k1gInSc3	zájem
médií	médium	k1gNnPc2	médium
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
obviněním	obvinění	k1gNnSc7	obvinění
a	a	k8xC	a
odsouzením	odsouzení	k1gNnSc7	odsouzení
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
obecně	obecně	k6eAd1	obecně
známí	známý	k2eAgMnPc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
trestního	trestní	k2eAgNnSc2d1	trestní
stíhání	stíhání	k1gNnSc2	stíhání
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
případech	případ	k1gInPc6	případ
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
odhadnutelný	odhadnutelný	k2eAgMnSc1d1	odhadnutelný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
definice	definice	k1gFnSc1	definice
skutkové	skutkový	k2eAgFnSc2d1	skutková
podstaty	podstata	k1gFnSc2	podstata
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vágní	vágní	k2eAgNnSc1d1	vágní
(	(	kIx(	(
<g/>
nejde	jít	k5eNaImIp3nS	jít
<g/>
-li	i	k?	-li
přímo	přímo	k6eAd1	přímo
o	o	k7c4	o
soulož	soulož	k1gFnSc4	soulož
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
přímé	přímý	k2eAgNnSc4d1	přímé
objektivní	objektivní	k2eAgNnSc4d1	objektivní
dokazování	dokazování	k1gNnSc4	dokazování
skutkového	skutkový	k2eAgInSc2d1	skutkový
děje	děj	k1gInSc2	děj
většinou	většina	k1gFnSc7	většina
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tak	tak	k6eAd1	tak
bývá	bývat	k5eAaImIp3nS	bývat
výsledek	výsledek	k1gInSc1	výsledek
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
dáván	dávat	k5eAaImNgMnS	dávat
do	do	k7c2	do
spojitosti	spojitost	k1gFnSc2	spojitost
především	především	k6eAd1	především
s	s	k7c7	s
kvalitami	kvalita	k1gFnPc7	kvalita
advokáta	advokát	k1gMnSc2	advokát
nebo	nebo	k8xC	nebo
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
