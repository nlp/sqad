<s>
Galapágy	Galapágy	k1gFnPc1	Galapágy
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Islas	Islas	k1gMnSc1	Islas
Galápagos	Galápagos	k1gMnSc1	Galápagos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Galápagos	Galápagos	k1gMnSc1	Galápagos
archipiélago	archipiélago	k1gMnSc1	archipiélago
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Galapážské	galapážský	k2eAgNnSc1d1	galapážské
souostroví	souostroví	k1gNnSc1	souostroví
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
ekvádorského	ekvádorský	k2eAgNnSc2d1	ekvádorské
souostroví	souostroví	k1gNnSc2	souostroví
18	[number]	k4	18
sopečných	sopečný	k2eAgInPc2d1	sopečný
ostrovů	ostrov	k1gInPc2	ostrov
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
