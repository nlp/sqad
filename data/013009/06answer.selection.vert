<s>
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
též	též	k9	též
jako	jako	k9	jako
Charles	Charles	k1gMnSc1	Charles
Luciano	Luciana	k1gFnSc5	Luciana
nebo	nebo	k8xC	nebo
Salvatore	Salvator	k1gMnSc5	Salvator
Luciano	Luciana	k1gFnSc5	Luciana
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1897	[number]	k4	1897
Lercara	Lercara	k1gFnSc1	Lercara
Friddi	Fridd	k1gMnPc1	Fridd
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1962	[number]	k4	1962
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
