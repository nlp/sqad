<p>
<s>
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
též	též	k9	též
jako	jako	k9	jako
Charles	Charles	k1gMnSc1	Charles
Luciano	Luciana	k1gFnSc5	Luciana
nebo	nebo	k8xC	nebo
Salvatore	Salvator	k1gMnSc5	Salvator
Luciano	Luciana	k1gFnSc5	Luciana
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1897	[number]	k4	1897
Lercara	Lercara	k1gFnSc1	Lercara
Friddi	Fridd	k1gMnPc1	Fridd
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1962	[number]	k4	1962
Neapol	Neapol	k1gFnSc1	Neapol
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Zmodernizoval	zmodernizovat	k5eAaPmAgInS	zmodernizovat
americkou	americký	k2eAgFnSc4d1	americká
mafii	mafie	k1gFnSc4	mafie
a	a	k8xC	a
stál	stát	k5eAaImAgMnS	stát
za	za	k7c7	za
obrovským	obrovský	k2eAgInSc7d1	obrovský
poválečným	poválečný	k2eAgInSc7d1	poválečný
obchodem	obchod	k1gInSc7	obchod
s	s	k7c7	s
heroinem	heroin	k1gInSc7	heroin
(	(	kIx(	(
<g/>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
spojka	spojka	k1gFnSc1	spojka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejasnosti	nejasnost	k1gFnSc6	nejasnost
==	==	k?	==
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
největšího	veliký	k2eAgMnSc4d3	veliký
mafiánského	mafiánský	k2eAgMnSc4d1	mafiánský
bosse	boss	k1gMnSc4	boss
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
mnoho	mnoho	k4c4	mnoho
nejasností	nejasnost	k1gFnPc2	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
vznik	vznik	k1gInSc4	vznik
jeho	jeho	k3xOp3gFnSc2	jeho
přezdívky	přezdívka	k1gFnSc2	přezdívka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Datum	datum	k1gInSc4	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
===	===	k?	===
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgFnPc1d3	nejrozšířenější
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc4	tři
verze	verze	k1gFnPc4	verze
data	datum	k1gNnSc2	datum
jeho	jeho	k3xOp3gNnSc2	jeho
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Encyclopæ	Encyclopæ	k1gMnSc2	Encyclopæ
Britannica	Britannicus	k1gMnSc2	Britannicus
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
www.who2.com	www.who2.com	k1gInSc1	www.who2.com
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1898	[number]	k4	1898
a	a	k8xC	a
kniha	kniha	k1gFnSc1	kniha
Mafie	mafie	k1gFnSc1	mafie
od	od	k7c2	od
Gábora	Gábor	k1gMnSc2	Gábor
Gellérta	Gellért	k1gMnSc2	Gellért
uvádí	uvádět	k5eAaImIp3nS	uvádět
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
přezdívky	přezdívka	k1gFnSc2	přezdívka
Lucky	Lucka	k1gFnSc2	Lucka
===	===	k?	===
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
hlavní	hlavní	k2eAgFnPc1d1	hlavní
verze	verze	k1gFnPc1	verze
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
Lucianovi	Lucian	k1gMnSc3	Lucian
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Lucky	lucky	k6eAd1	lucky
(	(	kIx(	(
<g/>
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1929	[number]	k4	1929
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
Luciana	Lucian	k1gMnSc4	Lucian
čtyři	čtyři	k4xCgMnPc1	čtyři
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
zmlátili	zmlátit	k5eAaPmAgMnP	zmlátit
ho	on	k3xPp3gNnSc4	on
a	a	k8xC	a
pořezali	pořezat	k5eAaPmAgMnP	pořezat
na	na	k7c6	na
krku	krk	k1gInSc6	krk
a	a	k8xC	a
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
členové	člen	k1gMnPc1	člen
gangu	gang	k1gInSc2	gang
Salvatora	Salvator	k1gMnSc4	Salvator
Maranzana	Maranzan	k1gMnSc4	Maranzan
<g/>
.	.	kIx.	.
</s>
<s>
Lucianova	Lucianův	k2eAgNnPc1d1	Lucianův
zranění	zranění	k1gNnPc1	zranění
byla	být	k5eAaImAgNnP	být
tak	tak	k6eAd1	tak
velká	velký	k2eAgNnPc1d1	velké
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
útočníci	útočník	k1gMnPc1	útočník
nechali	nechat	k5eAaPmAgMnP	nechat
ležet	ležet	k5eAaImF	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každou	každý	k3xTgFnSc4	každý
chvíli	chvíle	k1gFnSc4	chvíle
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
však	však	k9	však
přežil	přežít	k5eAaPmAgMnS	přežít
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
říkat	říkat	k5eAaImF	říkat
Lucky	lucky	k6eAd1	lucky
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
měl	mít	k5eAaImAgInS	mít
Luciano	Luciana	k1gFnSc5	Luciana
na	na	k7c6	na
krku	krk	k1gInSc6	krk
jizvy	jizva	k1gFnSc2	jizva
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
pravé	pravý	k2eAgNnSc1d1	pravé
oční	oční	k2eAgNnSc1d1	oční
víčko	víčko	k1gNnSc1	víčko
bylo	být	k5eAaImAgNnS	být
vždy	vždy	k6eAd1	vždy
trochu	trochu	k6eAd1	trochu
přivřené	přivřený	k2eAgNnSc1d1	přivřené
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
druhé	druhý	k4xOgFnSc2	druhý
verze	verze	k1gFnSc2	verze
Luciano	Luciana	k1gFnSc5	Luciana
často	často	k6eAd1	často
sázel	sázet	k5eAaImAgMnS	sázet
na	na	k7c6	na
dostizích	dostih	k1gInPc6	dostih
a	a	k8xC	a
většinou	většina	k1gFnSc7	většina
úspěšně	úspěšně	k6eAd1	úspěšně
tipoval	tipovat	k5eAaImAgMnS	tipovat
vítězné	vítězný	k2eAgMnPc4d1	vítězný
koně	kůň	k1gMnPc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
začalo	začít	k5eAaPmAgNnS	začít
říkat	říkat	k5eAaImF	říkat
Lucky	lucky	k6eAd1	lucky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
od	od	k7c2	od
Gábora	Gábor	k1gMnSc2	Gábor
Gellérta	Gellért	k1gMnSc2	Gellért
<g/>
:	:	kIx,	:
Konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
gang	gang	k1gInSc1	gang
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Maranzanem	Maranzan	k1gMnSc7	Maranzan
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ukryl	ukrýt	k5eAaPmAgMnS	ukrýt
Luciano	Luciana	k1gFnSc5	Luciana
jistou	jistý	k2eAgFnSc4d1	jistá
dávku	dávka	k1gFnSc4	dávka
narkotik	narkotik	k1gMnSc1	narkotik
<g/>
.	.	kIx.	.
</s>
<s>
Chytli	chytnout	k5eAaPmAgMnP	chytnout
ho	on	k3xPp3gMnSc4	on
na	na	k7c6	na
Manhattanu	Manhattan	k1gInSc6	Manhattan
a	a	k8xC	a
přes	přes	k7c4	přes
přístavní	přístavní	k2eAgFnSc4d1	přístavní
čtvrť	čtvrť	k1gFnSc4	čtvrť
vlekli	vleknout	k5eAaImAgMnP	vleknout
na	na	k7c4	na
Staten	Staten	k2eAgInSc4d1	Staten
Island	Island	k1gInSc4	Island
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opuštěném	opuštěný	k2eAgInSc6d1	opuštěný
úseku	úsek	k1gInSc6	úsek
ho	on	k3xPp3gInSc4	on
za	za	k7c4	za
prsty	prst	k1gInPc4	prst
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
ho	on	k3xPp3gNnSc4	on
"	"	kIx"	"
<g/>
zpracovali	zpracovat	k5eAaPmAgMnP	zpracovat
<g/>
"	"	kIx"	"
jenom	jenom	k9	jenom
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
pálili	pálit	k5eAaImAgMnP	pálit
cigaretami	cigareta	k1gFnPc7	cigareta
ve	v	k7c6	v
tváři	tvář	k1gFnSc6	tvář
<g/>
,	,	kIx,	,
když	když	k8xS	když
nepromluvil	promluvit	k5eNaPmAgMnS	promluvit
<g/>
,	,	kIx,	,
sundali	sundat	k5eAaPmAgMnP	sundat
mu	on	k3xPp3gMnSc3	on
boty	bota	k1gFnPc4	bota
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
mu	on	k3xPp3gMnSc3	on
opalovat	opalovat	k5eAaImF	opalovat
i	i	k9	i
chodidla	chodidlo	k1gNnPc4	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Omdlel	omdlet	k5eAaPmAgMnS	omdlet
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
trýznitelé	trýznitel	k1gMnPc1	trýznitel
v	v	k7c6	v
jistotě	jistota	k1gFnSc6	jistota
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
schází	scházet	k5eAaImIp3nS	scházet
málo	málo	k1gNnSc4	málo
ke	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
nechali	nechat	k5eAaPmAgMnP	nechat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
však	však	k9	však
kolem	kolem	k6eAd1	kolem
projížděla	projíždět	k5eAaImAgFnS	projíždět
policejní	policejní	k2eAgFnSc1d1	policejní
hlídka	hlídka	k1gFnSc1	hlídka
a	a	k8xC	a
sundala	sundat	k5eAaPmAgFnS	sundat
Luciana	Luciana	k1gFnSc1	Luciana
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ho	on	k3xPp3gMnSc4	on
profackovali	profackovat	k5eAaPmAgMnP	profackovat
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
<s>
Pravda	pravda	k9	pravda
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
ani	ani	k8xC	ani
tušení	tušení	k1gNnSc4	tušení
<g/>
,	,	kIx,	,
s	s	k7c7	s
kým	kdo	k3yInSc7	kdo
měl	mít	k5eAaImAgMnS	mít
tu	tu	k6eAd1	tu
čest	čest	k1gFnSc4	čest
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
měl	mít	k5eAaImAgInS	mít
zjizvenou	zjizvený	k2eAgFnSc4d1	zjizvená
tvář	tvář	k1gFnSc4	tvář
<g/>
,	,	kIx,	,
zůstal	zůstat	k5eAaPmAgMnS	zůstat
mu	on	k3xPp3gMnSc3	on
tik	tik	k1gInSc4	tik
v	v	k7c6	v
pravém	pravý	k2eAgNnSc6d1	pravé
oku	oko	k1gNnSc6	oko
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
přezdívku	přezdívka	k1gFnSc4	přezdívka
Lucky	Lucka	k1gFnSc2	Lucka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Lucky	lucky	k6eAd1	lucky
Luciano	Luciana	k1gFnSc5	Luciana
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
Salvatore	Salvator	k1gMnSc5	Salvator
Lucania	Lucanium	k1gNnSc2	Lucanium
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Lercara	Lercar	k1gMnSc2	Lercar
Friddi	Fridd	k1gMnPc1	Fridd
ležící	ležící	k2eAgInSc4d1	ležící
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
km	km	kA	km
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Corleone	Corleon	k1gInSc5	Corleon
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
si	se	k3xPyFc3	se
změnil	změnit	k5eAaPmAgMnS	změnit
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Salvatore	Salvator	k1gMnSc5	Salvator
Luciano	Luciana	k1gFnSc5	Luciana
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Charles	Charles	k1gMnSc1	Charles
Luciano	Luciana	k1gFnSc5	Luciana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
dětství	dětství	k1gNnSc4	dětství
si	se	k3xPyFc3	se
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
peníze	peníz	k1gInPc4	peníz
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vybíral	vybírat	k5eAaImAgMnS	vybírat
vždy	vždy	k6eAd1	vždy
pár	pár	k4xCyI	pár
centů	cent	k1gInPc2	cent
denně	denně	k6eAd1	denně
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
spolužáků	spolužák	k1gMnPc2	spolužák
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
bude	být	k5eAaImBp3nS	být
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
někdo	někdo	k3yInSc1	někdo
zdráhal	zdráhat	k5eAaImAgMnS	zdráhat
mu	on	k3xPp3gMnSc3	on
peníze	peníz	k1gInPc4	peníz
vydat	vydat	k5eAaPmF	vydat
<g/>
,	,	kIx,	,
Luciano	Luciana	k1gFnSc5	Luciana
ho	on	k3xPp3gMnSc4	on
zbil	zbít	k5eAaPmAgMnS	zbít
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
i	i	k9	i
chlapec	chlapec	k1gMnSc1	chlapec
jménem	jméno	k1gNnSc7	jméno
Meyer	Meyero	k1gNnPc2	Meyero
Lansky	Lanska	k1gFnSc2	Lanska
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
mafiánem	mafián	k1gMnSc7	mafián
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
chtěl	chtít	k5eAaImAgMnS	chtít
Luciano	Luciana	k1gFnSc5	Luciana
peníze	peníz	k1gInPc1	peníz
<g/>
,	,	kIx,	,
Lansky	Lansek	k1gInPc1	Lansek
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
porval	porvat	k5eAaPmAgInS	porvat
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Lanskymu	Lanskym	k1gInSc3	Lanskym
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
Luciano	Luciana	k1gFnSc5	Luciana
seznámit	seznámit	k5eAaPmF	seznámit
také	také	k9	také
s	s	k7c7	s
Bugsym	Bugsym	k1gInSc1	Bugsym
Siegelem	Siegel	k1gInSc7	Siegel
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
později	pozdě	k6eAd2	pozdě
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
Luciano	Luciana	k1gFnSc5	Luciana
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
gangu	gang	k1gInSc3	gang
Five	Fiv	k1gFnSc2	Fiv
Points	Pointsa	k1gFnPc2	Pointsa
Gang	gang	k1gInSc1	gang
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
členy	člen	k1gMnPc7	člen
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Johnny	Johnna	k1gMnSc2	Johnna
Torrio	Torrio	k1gMnSc1	Torrio
nebo	nebo	k8xC	nebo
Al	ala	k1gFnPc2	ala
Capone	Capon	k1gMnSc5	Capon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
byl	být	k5eAaImAgInS	být
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
heroinem	heroin	k1gInSc7	heroin
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
ke	k	k7c3	k
krátkému	krátký	k2eAgInSc3d1	krátký
trestu	trest	k1gInSc3	trest
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
vězení	vězení	k1gNnSc4	vězení
si	se	k3xPyFc3	se
odseděl	odsedět	k5eAaPmAgInS	odsedět
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
Luciano	Luciana	k1gFnSc5	Luciana
pracovat	pracovat	k5eAaImF	pracovat
pro	pro	k7c4	pro
Joea	Joe	k2eAgNnPc4d1	Joe
Masseria	Masserium	k1gNnPc4	Masserium
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
mafiánů	mafián	k1gMnPc2	mafián
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
však	však	k9	však
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
Masseriou	Masseria	k1gFnSc7	Masseria
v	v	k7c6	v
mnoha	mnoho	k4c2	mnoho
věcech	věc	k1gFnPc6	věc
<g/>
.	.	kIx.	.
</s>
<s>
Masseria	Masserium	k1gNnPc4	Masserium
byl	být	k5eAaImAgInS	být
konzervativní	konzervativní	k2eAgInSc1d1	konzervativní
a	a	k8xC	a
důvěřoval	důvěřovat	k5eAaImAgInS	důvěřovat
jenom	jenom	k6eAd1	jenom
Sicilanům	Sicilan	k1gMnPc3	Sicilan
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
byl	být	k5eAaImAgInS	být
liberálnější	liberální	k2eAgMnSc1d2	liberálnější
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
mafie	mafie	k1gFnSc2	mafie
zařadili	zařadit	k5eAaPmAgMnP	zařadit
i	i	k9	i
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
irských	irský	k2eAgInPc2d1	irský
a	a	k8xC	a
židovských	židovský	k2eAgInPc2d1	židovský
gangů	gang	k1gInPc2	gang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
pašoval	pašovat	k5eAaImAgInS	pašovat
Luciano	Luciana	k1gFnSc5	Luciana
alkohol	alkohol	k1gInSc4	alkohol
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
kuplířství	kuplířství	k1gNnSc6	kuplířství
<g/>
.	.	kIx.	.
</s>
<s>
Lucianovým	Lucianův	k2eAgMnSc7d1	Lucianův
mužem	muž	k1gMnSc7	muž
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
starší	starý	k2eAgMnSc1d2	starší
přítel	přítel	k1gMnSc1	přítel
Frank	Frank	k1gMnSc1	Frank
Costello	Costello	k1gNnSc4	Costello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Castellamarská	Castellamarský	k2eAgFnSc1d1	Castellamarský
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vyvrcholila	vyvrcholit	k5eAaPmAgFnS	vyvrcholit
Castellamarská	Castellamarský	k2eAgFnSc1d1	Castellamarský
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
krvavý	krvavý	k2eAgInSc4d1	krvavý
konflikt	konflikt	k1gInSc4	konflikt
mezi	mezi	k7c7	mezi
Salvatorem	Salvator	k1gInSc7	Salvator
Maranzanou	Maranzaný	k2eAgFnSc7d1	Maranzaný
a	a	k8xC	a
Joem	Joe	k1gNnSc7	Joe
Masseriou	Masseria	k1gMnSc7	Masseria
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
vedli	vést	k5eAaImAgMnP	vést
dva	dva	k4xCgInPc1	dva
největší	veliký	k2eAgInPc1d3	veliký
newyorské	newyorský	k2eAgInPc1d1	newyorský
klany	klan	k1gInPc1	klan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
konfliktu	konflikt	k1gInSc2	konflikt
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Maranzanovy	Maranzanův	k2eAgFnSc2d1	Maranzanův
strany	strana	k1gFnSc2	strana
byli	být	k5eAaImAgMnP	být
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
Castellammare	Castellammar	k1gMnSc5	Castellammar
del	del	k?	del
Golfo	Golfo	k1gMnSc1	Golfo
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
např.	např.	kA	např.
Joseph	Joseph	k1gMnSc1	Joseph
Bonanno	Bonanno	k6eAd1	Bonanno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
protivníci	protivník	k1gMnPc1	protivník
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
vedl	vést	k5eAaImAgMnS	vést
Joe	Joe	k1gMnPc4	Joe
Masseria	Masserium	k1gNnSc2	Masserium
<g/>
,	,	kIx,	,
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Sicílie	Sicílie	k1gFnSc2	Sicílie
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Masseriovi	Masserius	k1gMnSc3	Masserius
patřili	patřit	k5eAaImAgMnP	patřit
kromě	kromě	k7c2	kromě
Luciana	Lucian	k1gMnSc2	Lucian
např.	např.	kA	např.
Al	ala	k1gFnPc2	ala
Capone	Capon	k1gInSc5	Capon
<g/>
,	,	kIx,	,
Vito	vít	k5eAaImNgNnS	vít
Genovese	Genovesa	k1gFnSc3	Genovesa
<g/>
,	,	kIx,	,
Bugsy	Bugs	k1gInPc1	Bugs
Siegel	Siegel	k1gInSc1	Siegel
nebo	nebo	k8xC	nebo
Frank	frank	k1gInSc1	frank
Costello	Costello	k1gNnSc1	Costello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
konfliktu	konflikt	k1gInSc2	konflikt
však	však	k8xC	však
lidé	člověk	k1gMnPc1	člověk
přecházeli	přecházet	k5eAaImAgMnP	přecházet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
a	a	k8xC	a
tak	tak	k9	tak
původní	původní	k2eAgNnSc1d1	původní
rozdělení	rozdělení	k1gNnSc1	rozdělení
přestalo	přestat	k5eAaPmAgNnS	přestat
platit	platit	k5eAaImF	platit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
sice	sice	k8xC	sice
původně	původně	k6eAd1	původně
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Masseriu	Masserium	k1gNnSc3	Masserium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1931	[number]	k4	1931
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Meyerem	Meyer	k1gInSc7	Meyer
Lanskym	Lanskym	k1gInSc1	Lanskym
připravil	připravit	k5eAaPmAgInS	připravit
jeho	jeho	k3xOp3gFnSc4	jeho
vraždu	vražda	k1gFnSc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Odehrálo	odehrát	k5eAaPmAgNnS	odehrát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
restauraci	restaurace	k1gFnSc6	restaurace
<g/>
,	,	kIx,	,
když	když	k8xS	když
Luciano	Luciana	k1gFnSc5	Luciana
s	s	k7c7	s
Masseriou	Masseria	k1gFnSc7	Masseria
obědvali	obědvat	k5eAaImAgMnP	obědvat
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
toaletu	toaleta	k1gFnSc4	toaleta
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
čtyři	čtyři	k4xCgMnPc1	čtyři
střelci	střelec	k1gMnPc1	střelec
Masseriu	Masserium	k1gNnSc6	Masserium
zabili	zabít	k5eAaPmAgMnP	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maranzano	Maranzana	k1gFnSc5	Maranzana
tímto	tento	k3xDgNnSc7	tento
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Castellamarskou	Castellamarský	k2eAgFnSc4d1	Castellamarský
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
se	se	k3xPyFc4	se
Capem	cap	k1gMnSc7	cap
di	di	k?	di
tutti	tutti	k2eAgMnPc1d1	tutti
capi	cap	k1gMnPc1	cap
(	(	kIx(	(
<g/>
šéfem	šéf	k1gMnSc7	šéf
všech	všecek	k3xTgMnPc2	všecek
šéfů	šéf	k1gMnPc2	šéf
<g/>
,	,	kIx,	,
bossem	boss	k1gMnSc7	boss
všech	všecek	k3xTgMnPc2	všecek
bossů	boss	k1gMnPc2	boss
<g/>
)	)	kIx)	)
a	a	k8xC	a
učinil	učinit	k5eAaPmAgMnS	učinit
Luciana	Lucian	k1gMnSc4	Lucian
svým	svůj	k3xOyFgInSc7	svůj
mužem	muž	k1gMnSc7	muž
číslo	číslo	k1gNnSc1	číslo
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
si	se	k3xPyFc3	se
však	však	k9	však
přestali	přestat	k5eAaPmAgMnP	přestat
důvěřovat	důvěřovat	k5eAaImF	důvěřovat
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
připravovali	připravovat	k5eAaImAgMnP	připravovat
vraždu	vražda	k1gFnSc4	vražda
toho	ten	k3xDgNnSc2	ten
druhého	druhý	k4xOgMnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
byl	být	k5eAaImAgInS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1931	[number]	k4	1931
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
Maranzanovy	Maranzanův	k2eAgFnSc2d1	Maranzanův
kanceláře	kancelář	k1gFnSc2	kancelář
čtyři	čtyři	k4xCgMnPc1	čtyři
muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
jako	jako	k9	jako
daňoví	daňový	k2eAgMnPc1d1	daňový
úředníci	úředník	k1gMnPc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Maranzano	Maranzana	k1gFnSc5	Maranzana
slyšel	slyšet	k5eAaImAgMnS	slyšet
rámus	rámus	k1gInSc4	rámus
z	z	k7c2	z
chodby	chodba	k1gFnSc2	chodba
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
se	se	k3xPyFc4	se
z	z	k7c2	z
kanceláře	kancelář	k1gFnSc2	kancelář
kouknout	kouknout	k5eAaPmF	kouknout
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
si	se	k3xPyFc3	se
rozepnul	rozepnout	k5eAaPmAgMnS	rozepnout
sako	sako	k1gNnSc4	sako
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vidět	vidět	k5eAaImF	vidět
jeho	jeho	k3xOp3gFnSc1	jeho
pistole	pistole	k1gFnSc1	pistole
<g/>
.	.	kIx.	.
</s>
<s>
Útočníci	útočník	k1gMnPc1	útočník
ho	on	k3xPp3gMnSc4	on
měli	mít	k5eAaImAgMnP	mít
jen	jen	k9	jen
pobodat	pobodat	k5eAaPmF	pobodat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
změně	změna	k1gFnSc3	změna
situace	situace	k1gFnSc2	situace
ho	on	k3xPp3gInSc2	on
zastřelili	zastřelit	k5eAaPmAgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
měl	mít	k5eAaImAgMnS	mít
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
Luciano	Luciana	k1gFnSc5	Luciana
zabít	zabít	k5eAaPmF	zabít
ještě	ještě	k9	ještě
desítky	desítka	k1gFnPc1	desítka
Maranzanových	Maranzanův	k2eAgMnPc2d1	Maranzanův
věrných	věrný	k2eAgMnPc2d1	věrný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
jen	jen	k9	jen
asi	asi	k9	asi
dva	dva	k4xCgInPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgInPc1	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Masserii	Masserie	k1gFnSc6	Masserie
a	a	k8xC	a
Maranzana	Maranzana	k1gFnSc1	Maranzana
se	se	k3xPyFc4	se
Luciano	Luciana	k1gFnSc5	Luciana
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
newyorské	newyorský	k2eAgFnSc2d1	newyorská
mafie	mafie	k1gFnSc2	mafie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
zrušil	zrušit	k5eAaPmAgMnS	zrušit
místo	místo	k7c2	místo
Capa	cap	k1gMnSc2	cap
di	di	k?	di
tutti	tutti	k2eAgMnPc1d1	tutti
capi	cap	k1gMnPc1	cap
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
decentralizovanou	decentralizovaný	k2eAgFnSc4d1	decentralizovaná
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
mafie	mafie	k1gFnSc2	mafie
přivedl	přivést	k5eAaPmAgMnS	přivést
i	i	k9	i
ostatní	ostatní	k2eAgNnPc1d1	ostatní
etnika	etnikum	k1gNnPc1	etnikum
<g/>
,	,	kIx,	,
nejenom	nejenom	k6eAd1	nejenom
Sicilany	Sicilan	k1gMnPc4	Sicilan
<g/>
.	.	kIx.	.
</s>
<s>
Rodiny	rodina	k1gFnPc1	rodina
si	se	k3xPyFc3	se
rozdělily	rozdělit	k5eAaPmAgFnP	rozdělit
teritoria	teritorium	k1gNnSc2	teritorium
a	a	k8xC	a
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
Komise	komise	k1gFnSc1	komise
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
bossů	boss	k1gMnPc2	boss
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vězení	vězení	k1gNnPc4	vězení
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
tušil	tušit	k5eAaImAgMnS	tušit
mafián	mafián	k1gMnSc1	mafián
Dutch	Dutch	k1gMnSc1	Dutch
Schultz	Schultz	k1gMnSc1	Schultz
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
jde	jít	k5eAaImIp3nS	jít
státní	státní	k2eAgMnSc1d1	státní
prokurátor	prokurátor	k1gMnSc1	prokurátor
Thomas	Thomas	k1gMnSc1	Thomas
E.	E.	kA	E.
Dewey	Dewey	k1gInPc1	Dewey
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
ho	on	k3xPp3gMnSc4	on
nechat	nechat	k5eAaPmF	nechat
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Luciano	Luciana	k1gFnSc5	Luciana
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spolupracovníci	spolupracovník	k1gMnPc1	spolupracovník
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
<g/>
,	,	kIx,	,
že	že	k8xS	že
Schultz	Schultz	k1gInSc1	Schultz
nevezme	vzít	k5eNaPmIp3nS	vzít
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
výsledek	výsledek	k1gInSc1	výsledek
tohoto	tento	k3xDgNnSc2	tento
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Schultze	Schultze	k1gFnSc1	Schultze
zbaví	zbavit	k5eAaPmIp3nS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
jej	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
nechali	nechat	k5eAaPmAgMnP	nechat
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
však	však	k8xC	však
prokurátor	prokurátor	k1gMnSc1	prokurátor
Dewey	Dewea	k1gFnSc2	Dewea
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Luciana	Lucian	k1gMnSc4	Lucian
z	z	k7c2	z
kuplířství	kuplířství	k1gNnSc2	kuplířství
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
vykonstruovaný	vykonstruovaný	k2eAgInSc1d1	vykonstruovaný
a	a	k8xC	a
svědkové	svědek	k1gMnPc1	svědek
podplacení	podplacení	k1gNnSc2	podplacení
<g/>
.	.	kIx.	.
</s>
<s>
Svědkové	svědek	k1gMnPc1	svědek
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
svědkyně	svědkyně	k1gFnPc1	svědkyně
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
prostitutky	prostitutka	k1gFnPc1	prostitutka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
podle	podle	k7c2	podle
Luciana	Lucian	k1gMnSc2	Lucian
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
prokurátora	prokurátor	k1gMnSc2	prokurátor
Deweye	Dewey	k1gInSc2	Dewey
různé	různý	k2eAgFnPc4d1	různá
peněžní	peněžní	k2eAgFnPc4d1	peněžní
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
byl	být	k5eAaImAgInS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
na	na	k7c4	na
30	[number]	k4	30
-	-	kIx~	-
50	[number]	k4	50
let	léto	k1gNnPc2	léto
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Odseděl	odsedět	k5eAaPmAgMnS	odsedět
si	se	k3xPyFc3	se
však	však	k9	však
jen	jen	k9	jen
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
využila	využít	k5eAaPmAgFnS	využít
americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
Lucianových	Lucianův	k2eAgInPc2d1	Lucianův
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
mu	on	k3xPp3gMnSc3	on
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
Franka	Frank	k1gMnSc2	Frank
Costella	Costell	k1gMnSc2	Costell
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
doky	dok	k1gInPc7	dok
v	v	k7c6	v
New	New	k1gFnPc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
odbory	odbor	k1gInPc1	odbor
přístavních	přístavní	k2eAgMnPc2d1	přístavní
dělníků	dělník	k1gMnPc2	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
vláda	vláda	k1gFnSc1	vláda
si	se	k3xPyFc3	se
byla	být	k5eAaImAgFnS	být
vědoma	vědom	k2eAgFnSc1d1	vědoma
sabotáží	sabotáž	k1gFnSc7	sabotáž
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
německými	německý	k2eAgMnPc7d1	německý
špiony	špion	k1gMnPc7	špion
nebo	nebo	k8xC	nebo
samotnou	samotný	k2eAgFnSc7d1	samotná
mafií	mafie	k1gFnSc7	mafie
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
Lucianem	Lucian	k1gMnSc7	Lucian
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
ničení	ničení	k1gNnSc1	ničení
lodí	loď	k1gFnPc2	loď
přestane	přestat	k5eAaPmIp3nS	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bylo	být	k5eAaImAgNnS	být
Lucianovým	Lucianův	k2eAgInSc7d1	Lucianův
úkolem	úkol	k1gInSc7	úkol
spojit	spojit	k5eAaPmF	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
ulehčit	ulehčit	k5eAaPmF	ulehčit
tak	tak	k9	tak
vylodění	vylodění	k1gNnSc4	vylodění
amerických	americký	k2eAgNnPc2d1	americké
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Invaze	invaze	k1gFnSc1	invaze
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
byl	být	k5eAaImAgInS	být
tenkrát	tenkrát	k6eAd1	tenkrát
u	u	k7c2	u
moci	moc	k1gFnSc3	moc
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tamější	tamější	k2eAgFnSc4d1	tamější
mafii	mafie	k1gFnSc4	mafie
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
tyto	tento	k3xDgFnPc4	tento
služby	služba	k1gFnPc4	služba
bylo	být	k5eAaImAgNnS	být
Lucianovi	Lucian	k1gMnSc3	Lucian
umožněno	umožněn	k2eAgNnSc1d1	umožněno
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
řídit	řídit	k5eAaImF	řídit
mafii	mafie	k1gFnSc4	mafie
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
aktivity	aktivita	k1gFnPc4	aktivita
za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
jeho	on	k3xPp3gNnSc2	on
věznění	věznění	k1gNnSc2	věznění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
vláda	vláda	k1gFnSc1	vláda
Lucianovi	Lucian	k1gMnSc3	Lucian
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
propustí	propustit	k5eAaPmIp3nS	propustit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
však	však	k9	však
opustit	opustit	k5eAaPmF	opustit
americkou	americký	k2eAgFnSc4d1	americká
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1946	[number]	k4	1946
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
Luciano	Luciana	k1gFnSc5	Luciana
deportován	deportovat	k5eAaBmNgMnS	deportovat
a	a	k8xC	a
usídlil	usídlit	k5eAaPmAgMnS	usídlit
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
odletěl	odletět	k5eAaPmAgMnS	odletět
do	do	k7c2	do
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
,	,	kIx,	,
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
poté	poté	k6eAd1	poté
do	do	k7c2	do
Havany	Havana	k1gFnSc2	Havana
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
konference	konference	k1gFnSc1	konference
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sám	sám	k3xTgMnSc1	sám
zorganizoval	zorganizovat	k5eAaPmAgInS	zorganizovat
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
utvrdil	utvrdit	k5eAaPmAgMnS	utvrdit
své	svůj	k3xOyFgNnSc4	svůj
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
dohodnuty	dohodnut	k2eAgInPc1d1	dohodnut
obchody	obchod	k1gInPc1	obchod
s	s	k7c7	s
kubánským	kubánský	k2eAgMnSc7d1	kubánský
diktátorem	diktátor	k1gMnSc7	diktátor
Fulgenciem	Fulgencium	k1gNnSc7	Fulgencium
Batistou	Batistý	k2eAgFnSc7d1	Batistý
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
kasin	kasino	k1gNnPc2	kasino
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
odsouhlaseno	odsouhlasen	k2eAgNnSc1d1	odsouhlaseno
odstranění	odstranění	k1gNnSc1	odstranění
Bugsyho	Bugsy	k1gMnSc2	Bugsy
Siegela	Siegel	k1gMnSc2	Siegel
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
kasina	kasino	k1gNnSc2	kasino
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Flamingo	Flamingo	k1gMnSc1	Flamingo
<g/>
)	)	kIx)	)
ho	on	k3xPp3gMnSc4	on
totiž	totiž	k9	totiž
stála	stát	k5eAaImAgFnS	stát
obrovské	obrovský	k2eAgInPc4d1	obrovský
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nemohl	moct	k5eNaImAgMnS	moct
splácet	splácet	k5eAaImF	splácet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1947	[number]	k4	1947
byl	být	k5eAaImAgInS	být
Luciano	Luciana	k1gFnSc5	Luciana
poslán	poslat	k5eAaPmNgMnS	poslat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
heroinem	heroin	k1gInSc7	heroin
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vyhoštění	vyhoštění	k1gNnSc6	vyhoštění
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
se	se	k3xPyFc4	se
Luciano	Luciana	k1gFnSc5	Luciana
usídlil	usídlit	k5eAaPmAgMnS	usídlit
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tam	tam	k6eAd1	tam
navázal	navázat	k5eAaPmAgInS	navázat
nové	nový	k2eAgInPc4d1	nový
kontakty	kontakt	k1gInPc4	kontakt
se	s	k7c7	s
sicilskými	sicilský	k2eAgInPc7d1	sicilský
klany	klan	k1gInPc7	klan
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
mafiánskými	mafiánský	k2eAgFnPc7d1	mafiánská
organizacemi	organizace	k1gFnPc7	organizace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
Camorrou	Camorra	k1gFnSc7	Camorra
a	a	k8xC	a
'	'	kIx"	'
<g/>
Ndranghetou	Ndrangheta	k1gFnSc7	Ndrangheta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
dále	daleko	k6eAd2	daleko
Luciano	Luciana	k1gFnSc5	Luciana
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dobré	dobrý	k2eAgInPc4d1	dobrý
styky	styk	k1gInPc4	styk
s	s	k7c7	s
korsickou	korsický	k2eAgFnSc7d1	Korsická
mafií	mafie	k1gFnSc7	mafie
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
tzv.	tzv.	kA	tzv.
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
spojku	spojka	k1gFnSc4	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Opium	opium	k1gNnSc1	opium
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
pašovalo	pašovat	k5eAaImAgNnS	pašovat
z	z	k7c2	z
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyráběl	vyrábět	k5eAaImAgInS	vyrábět
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
heroin	heroin	k1gInSc1	heroin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
dále	daleko	k6eAd2	daleko
putoval	putovat	k5eAaImAgInS	putovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontakty	kontakt	k1gInPc4	kontakt
==	==	k?	==
</s>
</p>
<p>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
se	se	k3xPyFc4	se
přátelil	přátelit	k5eAaImAgMnS	přátelit
s	s	k7c7	s
Frankem	frank	k1gInSc7	frank
Sinatrou	Sinatra	k1gFnSc7	Sinatra
a	a	k8xC	a
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podporoval	podporovat	k5eAaImAgMnS	podporovat
také	také	k9	také
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Franklina	Franklin	k2eAgInSc2d1	Franklin
D.	D.	kA	D.
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Smrt	smrt	k1gFnSc1	smrt
==	==	k?	==
</s>
</p>
<p>
<s>
Luciano	Luciana	k1gFnSc5	Luciana
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
prodat	prodat	k5eAaPmF	prodat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
životní	životní	k2eAgInSc4d1	životní
příběh	příběh	k1gInSc4	příběh
filmařské	filmařský	k2eAgFnSc2d1	filmařská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1962	[number]	k4	1962
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
sejít	sejít	k5eAaPmF	sejít
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
producentem	producent	k1gMnSc7	producent
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
tam	tam	k6eAd1	tam
však	však	k9	však
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
se	se	k3xPyFc4	se
dohady	dohad	k1gInPc7	dohad
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
mrtvicí	mrtvice	k1gFnSc7	mrtvice
stojí	stát	k5eAaImIp3nS	stát
mafie	mafie	k1gFnSc1	mafie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gNnSc4	on
otrávila	otrávit	k5eAaPmAgFnS	otrávit
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
totiž	totiž	k9	totiž
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
záměrem	záměr	k1gInSc7	záměr
zveřejnit	zveřejnit	k5eAaPmF	zveřejnit
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
nesměl	smět	k5eNaImAgMnS	smět
Luciano	Luciana	k1gFnSc5	Luciana
vstoupit	vstoupit	k5eAaPmF	vstoupit
na	na	k7c6	na
území	území	k1gNnSc6	území
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
totiž	totiž	k9	totiž
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mrtvola	mrtvola	k1gFnSc1	mrtvola
není	být	k5eNaImIp3nS	být
občanem	občan	k1gMnSc7	občan
žádného	žádný	k1gMnSc2	žádný
státu	stát	k1gInSc2	stát
a	a	k8xC	a
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
tak	tak	k9	tak
vyhoštění	vyhoštění	k1gNnSc1	vyhoštění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lucianovým	Lucianův	k2eAgMnSc7d1	Lucianův
nástupcem	nástupce	k1gMnSc7	nástupce
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
mafiánského	mafiánský	k2eAgMnSc2d1	mafiánský
bosse	boss	k1gMnSc2	boss
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Frank	Frank	k1gMnSc1	Frank
Costello	Costello	k1gNnSc4	Costello
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lucky	Lucka	k1gFnSc2	Lucka
Luciano	Luciana	k1gFnSc5	Luciana
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Time	Time	k1gFnSc1	Time
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Gangstersinc	Gangstersinc	k1gFnSc1	Gangstersinc
<g/>
.	.	kIx.	.
<g/>
tripod	tripod	k1gInSc1	tripod
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Britannica	Britannica	k1gFnSc1	Britannica
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Paulandkaja	Paulandkaja	k1gFnSc1	Paulandkaja
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Mafiahouse	Mafiahouse	k1gFnSc1	Mafiahouse
<g/>
.	.	kIx.	.
<g/>
bravehost	bravehost	k1gFnSc1	bravehost
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Bugsyclub	Bugsyclub	k1gInSc1	Bugsyclub
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Find	Find	k?	Find
A	a	k9	a
Grave	grave	k6eAd1	grave
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
</s>
</p>
