<s>
The	The	k?	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
je	být	k5eAaImIp3nS	být
čtrnácté	čtrnáctý	k4xOgNnSc1	čtrnáctý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
britské	britský	k2eAgFnSc2d1	britská
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1994	[number]	k4	1994
a	a	k8xC	a
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
i	i	k8xC	i
americkém	americký	k2eAgInSc6d1	americký
žebříčku	žebříček	k1gInSc6	žebříček
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgFnSc2	druhý
takové	takový	k3xDgInPc4	takový
album	album	k1gNnSc1	album
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc4	Floyd
<g/>
,	,	kIx,	,
po	po	k7c4	po
Wish	Wish	k1gInSc4	Wish
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
baskytaristy	baskytarista	k1gMnSc2	baskytarista
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
hlavního	hlavní	k2eAgMnSc2d1	hlavní
textaře	textař	k1gMnSc2	textař
Rogera	Roger	k1gMnSc2	Roger
Waterse	Waterse	k1gFnSc2	Waterse
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
bylo	být	k5eAaImAgNnS	být
nahráváno	nahrávat	k5eAaImNgNnS	nahrávat
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
studiích	studio	k1gNnPc6	studio
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
studio	studio	k1gNnSc1	studio
Astoria	Astorium	k1gNnSc2	Astorium
umístěné	umístěný	k2eAgFnPc1d1	umístěná
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
hausbótu	hausbót	k1gInSc6	hausbót
na	na	k7c6	na
Temži	Temže	k1gFnSc6	Temže
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
majitelem	majitel	k1gMnSc7	majitel
je	být	k5eAaImIp3nS	být
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc4	jméno
alba	album	k1gNnSc2	album
vybral	vybrat	k5eAaPmAgMnS	vybrat
z	z	k7c2	z
textu	text	k1gInSc2	text
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
High	High	k1gMnSc1	High
Hopes	Hopes	k1gMnSc1	Hopes
<g/>
"	"	kIx"	"
spisovatel	spisovatel	k1gMnSc1	spisovatel
Douglas	Douglas	k1gMnSc1	Douglas
Adams	Adams	k1gInSc4	Adams
<g/>
,	,	kIx,	,
Gilmourův	Gilmourův	k2eAgMnSc1d1	Gilmourův
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
desky	deska	k1gFnSc2	deska
je	být	k5eAaImIp3nS	být
zvukově	zvukově	k6eAd1	zvukově
podobná	podobný	k2eAgFnSc1d1	podobná
albům	album	k1gNnPc3	album
Meddle	Meddle	k1gFnPc2	Meddle
nebo	nebo	k8xC	nebo
Obscured	Obscured	k1gInSc1	Obscured
by	by	kYmCp3nP	by
Clouds	Clouds	k1gInSc1	Clouds
s	s	k7c7	s
odvážnějšími	odvážný	k2eAgInPc7d2	odvážnější
a	a	k8xC	a
tvrdšími	tvrdý	k2eAgInPc7d2	tvrdší
tóny	tón	k1gInPc7	tón
alb	album	k1gNnPc2	album
Animals	Animals	k1gInSc4	Animals
a	a	k8xC	a
The	The	k1gFnSc4	The
Wall	Walla	k1gFnPc2	Walla
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
a	a	k8xC	a
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
The	The	k1gFnSc1	The
Division	Division	k1gInSc4	Division
Bell	bell	k1gInSc1	bell
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vydali	vydat	k5eAaPmAgMnP	vydat
Wish	Wish	k1gInSc4	Wish
You	You	k1gMnSc4	You
Were	Wer	k1gMnSc4	Wer
Here	Her	k1gMnSc4	Her
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
The	The	k1gFnSc1	The
Division	Division	k1gInSc4	Division
Bell	bell	k1gInSc1	bell
není	být	k5eNaImIp3nS	být
klasické	klasický	k2eAgNnSc4d1	klasické
koncepční	koncepční	k2eAgNnSc4d1	koncepční
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
skladbami	skladba	k1gFnPc7	skladba
spojitost	spojitost	k1gFnSc1	spojitost
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
i	i	k8xC	i
tématu	téma	k1gNnSc6	téma
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
jako	jako	k8xC	jako
protiklad	protiklad	k1gInSc1	protiklad
k	k	k7c3	k
desce	deska	k1gFnSc3	deska
The	The	k1gFnSc2	The
Wall	Walla	k1gFnPc2	Walla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
izolaci	izolace	k1gFnSc4	izolace
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
vůči	vůči	k7c3	vůči
společnosti	společnost	k1gFnSc3	společnost
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
The	The	k1gFnSc1	The
Division	Division	k1gInSc4	Division
Bell	bell	k1gInSc1	bell
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c4	o
udržovaní	udržovaný	k2eAgMnPc1d1	udržovaný
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
What	What	k1gInSc1	What
Do	do	k7c2	do
You	You	k1gFnSc2	You
Want	Want	k1gMnSc1	Want
from	from	k1gMnSc1	from
Me	Me	k1gMnSc1	Me
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
A	a	k8xC	a
Great	Great	k2eAgMnSc1d1	Great
Day	Day	k1gMnPc1	Day
for	forum	k1gNnPc2	forum
Freedom	Freedom	k1gInSc1	Freedom
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Take	Take	k1gFnSc1	Take
It	It	k1gMnSc1	It
Back	Back	k1gMnSc1	Back
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
zaměřeny	zaměřit	k5eAaPmNgFnP	zaměřit
především	především	k9	především
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
komunikace	komunikace	k1gFnSc2	komunikace
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
<g/>
,	,	kIx,	,
téma	téma	k1gNnSc1	téma
písně	píseň	k1gFnSc2	píseň
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
Talking	Talking	k1gInSc1	Talking
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
důležitostí	důležitost	k1gFnSc7	důležitost
udržování	udržování	k1gNnSc2	udržování
dialogu	dialog	k1gInSc2	dialog
a	a	k8xC	a
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
izolování	izolování	k1gNnSc2	izolování
jednotlivce	jednotlivec	k1gMnSc2	jednotlivec
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Poles	Poles	k1gInSc1	Poles
Apart	Apart	k1gInSc1	Apart
<g/>
"	"	kIx"	"
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
roztříštěných	roztříštěný	k2eAgInPc6d1	roztříštěný
rodinných	rodinný	k2eAgInPc6d1	rodinný
a	a	k8xC	a
přátelských	přátelský	k2eAgInPc6d1	přátelský
vztazích	vztah	k1gInPc6	vztah
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
Sydem	Syd	k1gMnSc7	Syd
Barrettem	Barrett	k1gMnSc7	Barrett
a	a	k8xC	a
Rogerem	Roger	k1gMnSc7	Roger
Watersem	Waters	k1gMnSc7	Waters
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
The	The	k1gFnSc6	The
Division	Division	k1gInSc4	Division
Bell	bell	k1gInSc1	bell
se	se	k3xPyFc4	se
v	v	k7c6	v
roli	role	k1gFnSc6	role
hlavního	hlavní	k2eAgMnSc2d1	hlavní
zpěváka	zpěvák	k1gMnSc2	zpěvák
objevil	objevit	k5eAaPmAgInS	objevit
po	po	k7c6	po
21	[number]	k4	21
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
od	od	k7c2	od
The	The	k1gFnSc2	The
Dark	Darka	k1gFnPc2	Darka
Side	Sid	k1gFnSc2	Sid
of	of	k?	of
the	the	k?	the
Moon	Moon	k1gMnSc1	Moon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
i	i	k8xC	i
klávesista	klávesista	k1gMnSc1	klávesista
Rick	Rick	k1gMnSc1	Rick
Wright	Wright	k1gMnSc1	Wright
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Wearing	Wearing	k1gInSc1	Wearing
the	the	k?	the
Inside	Insid	k1gMnSc5	Insid
Out	Out	k1gMnSc5	Out
<g/>
"	"	kIx"	"
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
napsal	napsat	k5eAaBmAgMnS	napsat
(	(	kIx(	(
<g/>
první	první	k4xOgInSc4	první
Wrightův	Wrightův	k2eAgInSc4d1	Wrightův
autorský	autorský	k2eAgInSc4d1	autorský
podíl	podíl	k1gInSc4	podíl
od	od	k7c2	od
Wish	Wisha	k1gFnPc2	Wisha
You	You	k1gMnSc2	You
Were	Wer	k1gMnSc2	Wer
Here	Her	k1gMnSc2	Her
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Marooned	Marooned	k1gInSc1	Marooned
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
skladbou	skladba	k1gFnSc7	skladba
od	od	k7c2	od
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
obdržela	obdržet	k5eAaPmAgFnS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
rockový	rockový	k2eAgInSc4d1	rockový
instrumentální	instrumentální	k2eAgInSc4d1	instrumentální
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Autorkou	autorka	k1gFnSc7	autorka
textů	text	k1gInPc2	text
většiny	většina	k1gFnSc2	většina
písní	píseň	k1gFnPc2	píseň
je	být	k5eAaImIp3nS	být
současná	současný	k2eAgFnSc1d1	současná
manželka	manželka	k1gFnSc1	manželka
Davida	David	k1gMnSc2	David
Gilmoura	Gilmour	k1gMnSc2	Gilmour
Polly	Polla	k1gMnSc2	Polla
Samsonová	Samsonová	k1gFnSc1	Samsonová
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
"	"	kIx"	"
<g/>
Cluster	cluster	k1gInSc1	cluster
One	One	k1gFnSc2	One
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Marooned	Marooned	k1gInSc1	Marooned
<g/>
"	"	kIx"	"
jsou	být	k5eAaImIp3nP	být
instrumentální	instrumentální	k2eAgNnPc1d1	instrumentální
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
následovalo	následovat	k5eAaImAgNnS	následovat
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c4	na
CD	CD	kA	CD
a	a	k8xC	a
VHS	VHS	kA	VHS
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Pulse	puls	k1gInSc5	puls
<g/>
.	.	kIx.	.
</s>
<s>
Přebal	přebal	k1gInSc1	přebal
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
dlouhodobého	dlouhodobý	k2eAgMnSc2d1	dlouhodobý
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
a	a	k8xC	a
autora	autor	k1gMnSc2	autor
většiny	většina	k1gFnSc2	většina
obalů	obal	k1gInPc2	obal
alb	alba	k1gFnPc2	alba
skupiny	skupina	k1gFnSc2	skupina
Storma	Storm	k1gMnSc4	Storm
Thorgersona	Thorgerson	k1gMnSc4	Thorgerson
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
skulptury	skulptura	k1gFnPc4	skulptura
dvou	dva	k4xCgFnPc2	dva
ocelových	ocelový	k2eAgFnPc2d1	ocelová
hlav	hlava	k1gFnPc2	hlava
<g/>
,	,	kIx,	,
vysokých	vysoký	k2eAgFnPc6d1	vysoká
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
a	a	k8xC	a
vážících	vážící	k2eAgInPc2d1	vážící
po	po	k7c6	po
1500	[number]	k4	1500
kilogramech	kilogram	k1gInPc6	kilogram
<g/>
.	.	kIx.	.
</s>
<s>
Hlavy	hlava	k1gFnPc1	hlava
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgInPc1d1	postaven
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
v	v	k7c6	v
Cambridgeshire	Cambridgeshir	k1gInSc5	Cambridgeshir
a	a	k8xC	a
fotografovány	fotografovat	k5eAaImNgFnP	fotografovat
za	za	k7c2	za
různého	různý	k2eAgNnSc2d1	různé
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
různým	různý	k2eAgNnSc7d1	různé
osvětlením	osvětlení	k1gNnSc7	osvětlení
a	a	k8xC	a
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
úhlů	úhel	k1gInPc2	úhel
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Použitá	použitý	k2eAgFnSc1d1	použitá
fotografie	fotografie	k1gFnSc1	fotografie
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
obě	dva	k4xCgFnPc4	dva
hlavy	hlava	k1gFnPc4	hlava
z	z	k7c2	z
profilu	profil	k1gInSc2	profil
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c4	v
Ely	Ela	k1gFnPc4	Ela
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
čtyři	čtyři	k4xCgNnPc1	čtyři
světla	světlo	k1gNnPc1	světlo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
svítí	svítit	k5eAaImIp3nP	svítit
mezi	mezi	k7c7	mezi
ústy	ústa	k1gNnPc7	ústa
skulptur	skulptura	k1gFnPc2	skulptura
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
na	na	k7c6	na
přebalu	přebal	k1gInSc6	přebal
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
vydavateli	vydavatel	k1gMnSc3	vydavatel
(	(	kIx(	(
<g/>
EMI	EMI	kA	EMI
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obale	obal	k1gInSc6	obal
alba	album	k1gNnSc2	album
je	být	k5eAaImIp3nS	být
Braillovým	Braillův	k2eAgNnSc7d1	Braillovo
písmem	písmo	k1gNnSc7	písmo
napsáno	napsat	k5eAaPmNgNnS	napsat
"	"	kIx"	"
<g/>
Pink	pink	k6eAd1	pink
Floyd	Floyd	k1gInSc1	Floyd
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vydání	vydání	k1gNnSc4	vydání
alba	album	k1gNnSc2	album
na	na	k7c6	na
magnetofonových	magnetofonový	k2eAgFnPc6d1	magnetofonová
kazetách	kazeta	k1gFnPc6	kazeta
a	a	k8xC	a
pro	pro	k7c4	pro
ilustraci	ilustrace	k1gFnSc4	ilustrace
koncertních	koncertní	k2eAgFnPc2d1	koncertní
brožur	brožura	k1gFnPc2	brožura
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
i	i	k9	i
další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
kamenných	kamenný	k2eAgFnPc2d1	kamenná
skulptur	skulptura	k1gFnPc2	skulptura
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Publius	Publius	k1gMnSc1	Publius
Enigma	enigma	k1gFnSc1	enigma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
The	The	k1gFnSc7	The
Division	Division	k1gInSc4	Division
Bell	bell	k1gInSc1	bell
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
turné	turné	k1gNnSc2	turné
v	v	k7c6	v
diskusní	diskusní	k2eAgFnSc6d1	diskusní
skupině	skupina	k1gFnSc6	skupina
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
na	na	k7c6	na
Usenetu	Usenet	k1gInSc6	Usenet
(	(	kIx(	(
<g/>
news://alt.music.pink	news://alt.music.pink	k1gInSc1	news://alt.music.pink
<g/>
-floyd	loyd	k1gInSc1	-floyd
<g/>
)	)	kIx)	)
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
vzkazy	vzkaz	k1gInPc1	vzkaz
<g/>
,	,	kIx,	,
zasílané	zasílaný	k2eAgFnPc1d1	zasílaná
přes	přes	k7c4	přes
Anonymous	Anonymous	k1gInSc4	Anonymous
contact	contact	k5eAaPmF	contact
service	service	k1gFnPc4	service
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nabádaly	nabádat	k5eAaBmAgFnP	nabádat
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
,	,	kIx,	,
aby	aby	k9	aby
obsahu	obsah	k1gInSc2	obsah
alba	album	k1gNnSc2	album
věnovali	věnovat	k5eAaImAgMnP	věnovat
obzvláštní	obzvláštní	k2eAgFnSc4d1	obzvláštní
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
nabádal	nabádat	k5eAaBmAgMnS	nabádat
k	k	k7c3	k
odhalení	odhalení	k1gNnSc3	odhalení
záhady	záhada	k1gFnSc2	záhada
<g/>
,	,	kIx,	,
hádanky	hádanka	k1gFnSc2	hádanka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
enigma	enigma	k1gFnSc1	enigma
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
albu	album	k1gNnSc6	album
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
obsahu	obsah	k1gInSc6	obsah
vložená	vložený	k2eAgFnSc1d1	vložená
<g/>
.	.	kIx.	.
</s>
<s>
Odesílatel	odesílatel	k1gMnSc1	odesílatel
těchto	tento	k3xDgInPc2	tento
vzkazů	vzkaz	k1gInPc2	vzkaz
se	se	k3xPyFc4	se
podepisoval	podepisovat	k5eAaImAgMnS	podepisovat
jako	jako	k9	jako
Publius	Publius	k1gMnSc1	Publius
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
a	a	k8xC	a
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
tajného	tajný	k2eAgInSc2d1	tajný
obsahu	obsah	k1gInSc2	obsah
alba	album	k1gNnSc2	album
sám	sám	k3xTgMnSc1	sám
použil	použít	k5eAaPmAgInS	použít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
záležitost	záležitost	k1gFnSc1	záležitost
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
Publius	Publius	k1gInSc1	Publius
Enigma	enigma	k1gFnSc1	enigma
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
Hádanka	hádanka	k1gFnSc1	hádanka
Publius	Publius	k1gMnSc1	Publius
<g/>
"	"	kIx"	"
případně	případně	k6eAd1	případně
též	též	k9	též
"	"	kIx"	"
<g/>
Publiova	Publiův	k2eAgFnSc1d1	Publiův
hádanka	hádanka	k1gFnSc1	hádanka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
či	či	k8xC	či
někdy	někdy	k6eAd1	někdy
zkráceně	zkráceně	k6eAd1	zkráceně
jen	jen	k9	jen
Enigma	enigma	k1gFnSc1	enigma
<g/>
.	.	kIx.	.
</s>
<s>
Hádanka	hádanka	k1gFnSc1	hádanka
samotná	samotný	k2eAgFnSc1d1	samotná
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
dlouho	dlouho	k6eAd1	dlouho
nevyřešená	vyřešený	k2eNgFnSc1d1	nevyřešená
a	a	k8xC	a
žádné	žádný	k3yNgFnPc1	žádný
nové	nový	k2eAgFnPc1d1	nová
Publiovy	Publiův	k2eAgFnPc1d1	Publiův
zprávy	zpráva	k1gFnPc1	zpráva
se	se	k3xPyFc4	se
od	od	k7c2	od
konce	konec	k1gInSc2	konec
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
neobjevily	objevit	k5eNaPmAgFnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
autogramiádě	autogramiáda	k1gFnSc6	autogramiáda
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
knize	kniha	k1gFnSc3	kniha
Pink	pink	k2eAgFnPc2d1	pink
Floyd	Floyda	k1gFnPc2	Floyda
<g/>
:	:	kIx,	:
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bubeník	bubeník	k1gMnSc1	bubeník
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
<g/>
,	,	kIx,	,
že	že	k8xS	že
Enigma	enigma	k1gFnSc1	enigma
skutečně	skutečně	k6eAd1	skutečně
existovala	existovat	k5eAaImAgFnS	existovat
a	a	k8xC	a
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
tato	tento	k3xDgFnSc1	tento
záležitost	záležitost	k1gFnSc1	záležitost
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
EMI	EMI	kA	EMI
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zvýšení	zvýšení	k1gNnSc1	zvýšení
prodeje	prodej	k1gInSc2	prodej
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc4	bell
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1994	[number]	k4	1994
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
EMI	EMI	kA	EMI
<g/>
)	)	kIx)	)
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
žebříčků	žebříček	k1gInPc2	žebříček
jak	jak	k8xC	jak
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
200	[number]	k4	200
udržela	udržet	k5eAaPmAgFnS	udržet
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnaní	porovnaný	k2eAgMnPc1d1	porovnaný
s	s	k7c7	s
předchozím	předchozí	k2eAgNnSc7d1	předchozí
albem	album	k1gNnSc7	album
A	a	k8xC	a
Momentary	Momentar	k1gInPc7	Momentar
Lapse	lapsus	k1gInSc5	lapsus
of	of	k?	of
Reason	Reason	k1gInSc1	Reason
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
pouze	pouze	k6eAd1	pouze
třetí	třetí	k4xOgFnPc4	třetí
pozice	pozice	k1gFnPc4	pozice
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
státech	stát	k1gInPc6	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Division	Division	k1gInSc1	Division
Bell	bell	k1gInSc1	bell
bylo	být	k5eAaImAgNnS	být
oceněno	oceněn	k2eAgNnSc1d1	oceněno
zlatou	zlatá	k1gFnSc7	zlatá
<g/>
,	,	kIx,	,
platinovou	platinový	k2eAgFnSc7d1	platinová
a	a	k8xC	a
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
platinovou	platinový	k2eAgFnSc7d1	platinová
deskou	deska	k1gFnSc7	deska
v	v	k7c6	v
USA	USA	kA	USA
již	již	k6eAd1	již
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
platinovým	platinový	k2eAgInSc7d1	platinový
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Pink	pink	k2eAgMnSc1d1	pink
Floyd	Floyd	k1gMnSc1	Floyd
<g/>
:	:	kIx,	:
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
-	-	kIx~	-
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
programování	programování	k1gNnSc1	programování
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Rick	Rick	k1gInSc1	Rick
Wright	Wright	k1gInSc1	Wright
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
piano	piano	k1gNnSc1	piano
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Wearing	Wearing	k1gInSc1	Wearing
the	the	k?	the
Inside	Insid	k1gMnSc5	Insid
Out	Out	k1gMnSc5	Out
<g/>
"	"	kIx"	"
Nick	Nick	k1gMnSc1	Nick
Mason	mason	k1gMnSc1	mason
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
programování	programování	k1gNnSc2	programování
Tim	Tim	k?	Tim
Renwick	Renwicka	k1gFnPc2	Renwicka
-	-	kIx~	-
kytary	kytara	k1gFnSc2	kytara
Guy	Guy	k1gMnSc1	Guy
Pratt	Pratt	k1gMnSc1	Pratt
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Jon	Jon	k1gMnSc1	Jon
Carin	Carin	k1gInSc1	Carin
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
Bob	Bob	k1gMnSc1	Bob
Ezrin	Ezrin	k1gMnSc1	Ezrin
-	-	kIx~	-
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
Dick	Dick	k1gInSc1	Dick
Parry	Parra	k1gFnSc2	Parra
-	-	kIx~	-
tenorsaxofon	tenorsaxofon	k1gInSc1	tenorsaxofon
Gary	Gara	k1gFnSc2	Gara
Wallis	Wallis	k1gFnSc2	Wallis
-	-	kIx~	-
perkuse	perkuse	k1gFnSc1	perkuse
Carol	Carola	k1gFnPc2	Carola
Kenyon	Kenyon	k1gMnSc1	Kenyon
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
Brown	Brown	k1gMnSc1	Brown
-	-	kIx~	-
vokály	vokál	k1gInPc1	vokál
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc4	Hawking
-	-	kIx~	-
syntetizovaný	syntetizovaný	k2eAgInSc4d1	syntetizovaný
hlas	hlas	k1gInSc4	hlas
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
"	"	kIx"	"
<g/>
Keep	Keep	k1gInSc1	Keep
Talking	Talking	k1gInSc1	Talking
<g/>
"	"	kIx"	"
Michael	Michael	k1gMnSc1	Michael
Kamen	kamna	k1gNnPc2	kamna
-	-	kIx~	-
orchestrální	orchestrální	k2eAgNnSc1d1	orchestrální
aranžmá	aranžmá	k1gNnSc1	aranžmá
David	David	k1gMnSc1	David
Gilmour	Gilmour	k1gMnSc1	Gilmour
-	-	kIx~	-
mixování	mixování	k1gNnSc1	mixování
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc1	produkce
Bob	Bob	k1gMnSc1	Bob
Eztrin	Eztrin	k1gInSc1	Eztrin
-	-	kIx~	-
produkce	produkce	k1gFnSc2	produkce
Chris	Chris	k1gFnSc2	Chris
Thomas	Thomas	k1gMnSc1	Thomas
-	-	kIx~	-
mixování	mixování	k1gNnSc1	mixování
Andrew	Andrew	k1gMnSc1	Andrew
Jackson	Jackson	k1gMnSc1	Jackson
-	-	kIx~	-
zvukový	zvukový	k2eAgMnSc1d1	zvukový
inženýr	inženýr	k1gMnSc1	inženýr
</s>
