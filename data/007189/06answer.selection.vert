<s>
Dačice	Dačice	k1gFnPc1	Dačice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Datschitz	Datschitz	k1gInSc1	Datschitz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Jindřichův	Jindřichův	k2eAgInSc1d1	Jindřichův
Hradec	Hradec	k1gInSc1	Hradec
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
12	[number]	k4	12
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Telče	Telč	k1gFnSc2	Telč
na	na	k7c6	na
Moravské	moravský	k2eAgFnSc6d1	Moravská
Dyji	Dyje	k1gFnSc6	Dyje
v	v	k7c6	v
nejjižnějším	jižní	k2eAgInSc6d3	nejjižnější
cípu	cíp	k1gInSc6	cíp
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
