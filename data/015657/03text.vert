<s>
Ciferník	ciferník	k1gInSc1
</s>
<s>
Nástěnné	nástěnný	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
</s>
<s>
Ciferník	ciferník	k1gInSc1
nebo	nebo	k8xC
též	též	k9
číselník	číselník	k1gInSc1
je	být	k5eAaImIp3nS
součást	součást	k1gFnSc4
ručičkových	ručičkový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
a	a	k8xC
měřících	měřící	k2eAgInPc2d1
přístrojů	přístroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
okrouhlý	okrouhlý	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
i	i	k9
jiný	jiný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
na	na	k7c6
něm	on	k3xPp3gInSc6
umístěny	umístit	k5eAaPmNgFnP
číslice	číslice	k1gFnPc1
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgFnSc3d1
měřené	měřený	k2eAgFnSc3d1
veličině	veličina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
čas	čas	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
obvykle	obvykle	k6eAd1
12	#num#	k4
číslic	číslice	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
starších	starý	k2eAgFnPc2d2
hodin	hodina	k1gFnPc2
mívají	mívat	k5eAaImIp3nP
ciferníky	ciferník	k1gInPc4
čísla	číslo	k1gNnSc2
od	od	k7c2
jedné	jeden	k4xCgFnSc2
až	až	k9
do	do	k7c2
čtyřiadvacítky	čtyřiadvacítka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hodinový	hodinový	k2eAgInSc1d1
ciferník	ciferník	k1gInSc1
</s>
<s>
Nádražní	nádražní	k2eAgFnPc1d1
hodiny	hodina	k1gFnPc1
bez	bez	k7c2
čísel	číslo	k1gNnPc2
</s>
<s>
Ciferník	ciferník	k1gInSc1
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
část	část	k1gFnSc1
analogových	analogový	k2eAgFnPc2d1
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pomocí	pomocí	k7c2
pohybujících	pohybující	k2eAgFnPc2d1
se	se	k3xPyFc4
ručiček	ručička	k1gFnPc2
ukazuje	ukazovat	k5eAaImIp3nS
čas	čas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
má	mít	k5eAaImIp3nS
ciferník	ciferník	k1gInSc4
kruhový	kruhový	k2eAgInSc4d1
tvar	tvar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gInSc6
obvodu	obvod	k1gInSc6
jsou	být	k5eAaImIp3nP
pravidelně	pravidelně	k6eAd1
rozmístěna	rozmístěn	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
od	od	k7c2
1	#num#	k4
do	do	k7c2
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malá	malý	k2eAgFnSc1d1
hodinová	hodinový	k2eAgFnSc1d1
ručička	ručička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
oběhne	oběhnout	k5eAaPmIp3nS
ciferník	ciferník	k1gInSc4
dvakrát	dvakrát	k6eAd1
za	za	k7c4
den	den	k1gInSc4
<g/>
,	,	kIx,
ukazuje	ukazovat	k5eAaImIp3nS
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
minutová	minutový	k2eAgFnSc1d1
ručička	ručička	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
minuty	minuta	k1gFnPc4
a	a	k8xC
oběhne	oběhnout	k5eAaPmIp3nS
ciferník	ciferník	k1gInSc4
každou	každý	k3xTgFnSc4
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
ciferníku	ciferník	k1gInSc6
také	také	k9
ručička	ručička	k1gFnSc1
vteřinová	vteřinový	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
oběhne	oběhnout	k5eAaPmIp3nS
ciferník	ciferník	k1gInSc4
jednou	jednou	k6eAd1
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Číslice	číslice	k1gFnSc1
na	na	k7c6
ciferníku	ciferník	k1gInSc6
jsou	být	k5eAaImIp3nP
buď	buď	k8xC
arabské	arabský	k2eAgInPc1d1
<g/>
,	,	kIx,
nebo	nebo	k8xC
římské	římský	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
římské	římský	k2eAgFnPc4d1
číslice	číslice	k1gFnPc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
4	#num#	k4
obvykle	obvykle	k6eAd1
znázorňováno	znázorňovat	k5eAaImNgNnS
symbolem	symbol	k1gInSc7
IIII	IIII	kA
místo	místo	k7c2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zřejmě	zřejmě	k6eAd1
kvůli	kvůli	k7c3
grafické	grafický	k2eAgFnSc3d1
symetrii	symetrie	k1gFnSc3
s	s	k7c7
číslem	číslo	k1gNnSc7
VIII	VIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vnějším	vnější	k2eAgInSc6d1
obvodu	obvod	k1gInSc6
ciferníku	ciferník	k1gInSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
i	i	k9
jiné	jiný	k2eAgFnPc1d1
pomocné	pomocný	k2eAgFnPc1d1
stupnice	stupnice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Ciferník	ciferník	k1gInSc1
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
tak	tak	k6eAd1
známá	známý	k2eAgFnSc1d1
věc	věc	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
mnohdy	mnohdy	k6eAd1
na	na	k7c4
ciferníků	ciferník	k1gInPc2
vůbec	vůbec	k9
žádná	žádný	k3yNgNnPc1
čísla	číslo	k1gNnPc1
nejsou	být	k5eNaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
nahrazena	nahradit	k5eAaPmNgFnS
různými	různý	k2eAgInPc7d1
indiferentními	indiferentní	k2eAgInPc7d1
symboly	symbol	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
totiž	totiž	k9
odečítá	odečítat	k5eAaImIp3nS
čas	čas	k1gInSc4
z	z	k7c2
ciferníků	ciferník	k1gInPc2
ne	ne	k9
podle	podle	k7c2
čísel	číslo	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
podle	podle	k7c2
polohy	poloha	k1gFnSc2
ručiček	ručička	k1gFnPc2
(	(	kIx(
<g/>
úhlu	úhel	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
svírají	svírat	k5eAaImIp3nP
se	s	k7c7
spojnicí	spojnice	k1gFnSc7
středu	střed	k1gInSc2
ciferníku	ciferník	k1gInSc2
a	a	k8xC
polohou	poloha	k1gFnSc7
číslice	číslice	k1gFnSc2
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Pozici	pozice	k1gFnSc4
ručiček	ručička	k1gFnPc2
lze	lze	k6eAd1
vypočítat	vypočítat	k5eAaPmF
i	i	k9
matematicky	matematicky	k6eAd1
</s>
<s>
úhel	úhel	k1gInSc1
minutové	minutový	k2eAgFnSc2d1
ručičky	ručička	k1gFnSc2
=	=	kIx~
počet	počet	k1gInSc1
minut	minuta	k1gFnPc2
*	*	kIx~
6	#num#	k4
<g/>
°	°	k?
</s>
<s>
úhel	úhel	k1gInSc1
hodinové	hodinový	k2eAgFnSc2d1
ručičky	ručička	k1gFnSc2
=	=	kIx~
počet	počet	k1gInSc1
hodin	hodina	k1gFnPc2
*	*	kIx~
30	#num#	k4
<g/>
°	°	k?
+	+	kIx~
počet	počet	k1gInSc1
minut	minuta	k1gFnPc2
*	*	kIx~
0,5	0,5	k4
<g/>
°	°	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ciferník	ciferník	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ciferník	ciferník	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4213293-9	4213293-9	k4
</s>
