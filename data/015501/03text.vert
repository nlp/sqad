<s>
Bodiam	Bodiam	k6eAd1
</s>
<s>
Bodiam	Bodiam	k6eAd1
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Sloh	sloha	k1gFnPc2
</s>
<s>
středověká	středověký	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
anonym	anonym	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1385	#num#	k4
Materiál	materiál	k1gInSc1
</s>
<s>
pískovec	pískovec	k1gInSc1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Bodiam	Bodiam	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
0	#num#	k4
<g/>
′	′	k?
<g/>
8,14	8,14	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
°	°	k?
<g/>
32	#num#	k4
<g/>
′	′	k?
<g/>
36,71	36,71	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
1044134	#num#	k4
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Letecký	letecký	k2eAgInSc4d1
snímek	snímek	k1gInSc4
hradu	hrad	k1gInSc2
Bodiam	Bodiam	k1gInSc1
</s>
<s>
Bodiam	Bodiam	k1gInSc1
je	být	k5eAaImIp3nS
anglický	anglický	k2eAgInSc1d1
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
rozbořený	rozbořený	k2eAgInSc1d1
vodní	vodní	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
,	,	kIx,
obklopený	obklopený	k2eAgInSc1d1
rozsáhlým	rozsáhlý	k2eAgInSc7d1
vodním	vodní	k2eAgInSc7d1
příkopem	příkop	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
u	u	k7c2
vesnice	vesnice	k1gFnSc2
Robertsbridge	Robertsbridg	k1gFnSc2
v	v	k7c6
hrabství	hrabství	k1gNnSc6
Východní	východní	k2eAgInSc1d1
Sussex	Sussex	k1gInSc1
a	a	k8xC
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sirem	sir	k1gMnSc7
Edwardem	Edward	k1gMnSc7
Dalyngriggem	Dalyngrigg	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Celá	celý	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
budova	budova	k1gFnSc1
má	mít	k5eAaImIp3nS
čtyřúhelníkový	čtyřúhelníkový	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bez	bez	k7c2
donjonu	donjon	k1gInSc2
<g/>
,	,	kIx,
rámovaná	rámovaný	k2eAgFnSc1d1
sedmi	sedm	k4xCc7
věžemi	věž	k1gFnPc7
a	a	k8xC
velikou	veliký	k2eAgFnSc7d1
branou	brána	k1gFnSc7
<g/>
,	,	kIx,
obojí	oboj	k1gFnSc7
je	být	k5eAaImIp3nS
na	na	k7c6
vrcholku	vrcholek	k1gInSc6
zakončeno	zakončen	k2eAgNnSc1d1
cimbuřím	cimbuří	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obytné	obytný	k2eAgFnPc1d1
komnaty	komnata	k1gFnPc1
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
vnějšího	vnější	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělá	umělý	k2eAgFnSc1d1
vodní	vodní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
vybudovaná	vybudovaný	k2eAgFnSc1d1
kolem	kolem	k7c2
Bodiamu	Bodiam	k1gInSc2
měla	mít	k5eAaImAgFnS
nejen	nejen	k6eAd1
funkci	funkce	k1gFnSc4
obrannou	obranný	k2eAgFnSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
estetickou	estetický	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
zvýšit	zvýšit	k5eAaPmF
okázalost	okázalost	k1gFnSc4
dojmu	dojem	k1gInSc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
na	na	k7c4
své	svůj	k3xOyFgNnSc4
okolí	okolí	k1gNnSc4
působí	působit	k5eAaImIp3nP
hrad	hrad	k1gInSc4
<g/>
,	,	kIx,
týčící	týčící	k2eAgInPc4d1
se	se	k3xPyFc4
uprostřed	uprostřed	k7c2
vodní	vodní	k2eAgFnSc2d1
hladiny	hladina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zdát	zdát	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
sir	sir	k1gMnSc1
Dalyngrigge	Dalyngrigg	k1gFnSc2
ho	on	k3xPp3gMnSc4
postavil	postavit	k5eAaPmAgMnS
za	za	k7c7
účelem	účel	k1gInSc7
ochrany	ochrana	k1gFnSc2
anglického	anglický	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
proti	proti	k7c3
hrozícímu	hrozící	k2eAgInSc3d1
vpádu	vpád	k1gInSc3
Francouzů	Francouz	k1gMnPc2
během	během	k7c2
stoleté	stoletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
tomu	ten	k3xDgNnSc3
tak	tak	k6eAd1
není	být	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
historiků	historik	k1gMnPc2
proti	proti	k7c3
této	tento	k3xDgFnSc3
teorii	teorie	k1gFnSc3
svědčí	svědčit	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
hradu	hrad	k1gInSc2
od	od	k7c2
středověké	středověký	k2eAgFnSc2d1
pobřežní	pobřežní	k2eAgFnSc2d1
linie	linie	k1gFnSc2
(	(	kIx(
<g/>
pobřeží	pobřeží	k1gNnSc1
je	být	k5eAaImIp3nS
vzdáleno	vzdálit	k5eAaPmNgNnS
zhruba	zhruba	k6eAd1
20	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Povolení	povolení	k1gNnSc4
postavit	postavit	k5eAaPmF
hrad	hrad	k1gInSc4
dostal	dostat	k5eAaPmAgMnS
Edward	Edward	k1gMnSc1
Dalyngrigge	Dalyngrigg	k1gInSc2
od	od	k7c2
krále	král	k1gMnSc2
Richarda	Richard	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1385	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
rodina	rodina	k1gFnSc1
ho	on	k3xPp3gInSc4
vlastnila	vlastnit	k5eAaImAgFnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1470	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
Bodiam	Bodiam	k1gInSc1
získal	získat	k5eAaPmAgInS
rod	rod	k1gInSc4
Lewknorů	Lewknor	k1gMnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1543	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
vystřídalo	vystřídat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
jiných	jiný	k2eAgMnPc2d1
majitelů	majitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
anglické	anglický	k2eAgFnSc6d1
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
začal	začít	k5eAaPmAgMnS
hrad	hrad	k1gInSc4
postupně	postupně	k6eAd1
chátrat	chátrat	k5eAaImF
<g/>
,	,	kIx,
částečně	částečně	k6eAd1
opravován	opravován	k2eAgMnSc1d1
začal	začít	k5eAaPmAgMnS
být	být	k5eAaImF
až	až	k9
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
čtvrtině	čtvrtina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gFnSc4
obnovu	obnova	k1gFnSc4
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
i	i	k9
lord	lord	k1gMnSc1
Curzon	Curzon	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
ho	on	k3xPp3gMnSc4
zakoupil	zakoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1916	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1925	#num#	k4
Bodiam	Bodiam	k1gInSc1
spravuje	spravovat	k5eAaImIp3nS
anglická	anglický	k2eAgFnSc1d1
charitativní	charitativní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
The	The	k1gFnSc2
National	National	k1gFnSc2
Trust	trust	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
přístupný	přístupný	k2eAgInSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bodiam	Bodiam	k1gInSc1
Castle	Castle	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
LIDDIARD	LIDDIARD	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Castles	Castles	k1gMnSc1
in	in	k?
Context	Context	k1gMnSc1
<g/>
:	:	kIx,
Power	Power	k1gMnSc1
<g/>
,	,	kIx,
Symbolism	Symbolism	k1gMnSc1
and	and	k?
Landscape	Landscap	k1gMnSc5
<g/>
,	,	kIx,
1066	#num#	k4
to	ten	k3xDgNnSc1
1500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macclesfield	Macclesfielda	k1gFnPc2
<g/>
:	:	kIx,
Windgather	Windgathra	k1gFnPc2
Press	Pressa	k1gFnPc2
Ltd	ltd	kA
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
178	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
9545575	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Bodiam	Bodiam	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
National	National	k1gFnSc2
trust	trust	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
223072	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
308216814	#num#	k4
</s>
