<s>
Celá	celý	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
budova	budova	k1gFnSc1
má	mít	k5eAaImIp3nS
čtyřúhelníkový	čtyřúhelníkový	k2eAgInSc4d1
půdorys	půdorys	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bez	bez	k7c2
donjonu	donjon	k1gInSc2
<g/>
,	,	kIx,
rámovaná	rámovaný	k2eAgFnSc1d1
sedmi	sedm	k4xCc7
věžemi	věž	k1gFnPc7
a	a	k8xC
velikou	veliký	k2eAgFnSc7d1
branou	brána	k1gFnSc7
<g/>
,	,	kIx,
obojí	oboj	k1gFnSc7
je	být	k5eAaImIp3nS
na	na	k7c6
vrcholku	vrcholek	k1gInSc6
zakončeno	zakončen	k2eAgNnSc1d1
cimbuřím	cimbuří	k1gNnSc7
<g/>
.	.	kIx.
</s>