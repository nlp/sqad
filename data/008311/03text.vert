<p>
<s>
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
<g/>
,	,	kIx,	,
Smiljan	Smiljan	k1gInSc1	Smiljan
<g/>
,	,	kIx,	,
Rakouské	rakouský	k2eAgNnSc1d1	rakouské
císařství	císařství	k1gNnSc1	císařství
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
srbský	srbský	k2eAgMnSc1d1	srbský
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
konstruktér	konstruktér	k1gMnSc1	konstruktér
elektrických	elektrický	k2eAgInPc2d1	elektrický
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
,	,	kIx,	,
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
asynchronní	asynchronní	k2eAgInSc1d1	asynchronní
motor	motor	k1gInSc1	motor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnPc1	práce
formovaly	formovat	k5eAaImAgFnP	formovat
základ	základ	k1gInSc4	základ
moderních	moderní	k2eAgInPc2d1	moderní
systémů	systém	k1gInPc2	systém
na	na	k7c4	na
vícefázový	vícefázový	k2eAgInSc4d1	vícefázový
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předvedení	předvedení	k1gNnSc6	předvedení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
telekomunikace	telekomunikace	k1gFnSc2	telekomunikace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
a	a	k8xC	a
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
válce	válec	k1gInSc2	válec
proudů	proud	k1gInPc2	proud
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
Thomasi	Thomas	k1gMnSc3	Thomas
A.	A.	kA	A.
Edisonovi	Edison	k1gMnSc3	Edison
byl	být	k5eAaImAgMnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
respektován	respektován	k2eAgMnSc1d1	respektován
jako	jako	k8xS	jako
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
srbský	srbský	k2eAgMnSc1d1	srbský
elektrotechnický	elektrotechnický	k2eAgMnSc1d1	elektrotechnický
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
daly	dát	k5eAaPmAgFnP	dát
základ	základ	k1gInSc4	základ
moderní	moderní	k2eAgFnSc2d1	moderní
elektrotechnice	elektrotechnika	k1gFnSc3	elektrotechnika
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
jeho	jeho	k3xOp3gInPc2	jeho
objevů	objev	k1gInPc2	objev
mělo	mít	k5eAaImAgNnS	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgMnS	být
Tesla	Tesla	k1gMnSc1	Tesla
velmi	velmi	k6eAd1	velmi
plodný	plodný	k2eAgMnSc1d1	plodný
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
výraznější	výrazný	k2eAgFnPc4d2	výraznější
podnikatelské	podnikatelský	k2eAgFnPc4d1	podnikatelská
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
či	či	k8xC	či
ambice	ambice	k1gFnPc1	ambice
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgInPc3	svůj
experimentům	experiment	k1gInPc3	experiment
a	a	k8xC	a
projektům	projekt	k1gInPc3	projekt
zadlužoval	zadlužovat	k5eAaImAgMnS	zadlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
uznáváním	uznávání	k1gNnSc7	uznávání
svých	svůj	k3xOyFgInPc2	svůj
patentů	patent	k1gInPc2	patent
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
dostával	dostávat	k5eAaImAgMnS	dostávat
do	do	k7c2	do
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
vynálezci	vynálezce	k1gMnPc7	vynálezce
a	a	k8xC	a
podnikateli	podnikatel	k1gMnPc7	podnikatel
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
Edisonem	Edison	k1gInSc7	Edison
a	a	k8xC	a
s	s	k7c7	s
Marconim	Marconi	k1gNnSc7	Marconi
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
vizionářskými	vizionářský	k2eAgMnPc7d1	vizionářský
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
potenciálně	potenciálně	k6eAd1	potenciálně
převratnými	převratný	k2eAgFnPc7d1	převratná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gMnPc2	jeho
sponzorů	sponzor	k1gMnPc2	sponzor
<g/>
,	,	kIx,	,
především	především	k9	především
J.	J.	kA	J.
P.	P.	kA	P.
Morgana	morgan	k1gMnSc2	morgan
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
výdělečnými	výdělečný	k2eAgInPc7d1	výdělečný
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
všem	všecek	k3xTgInPc3	všecek
těmto	tento	k3xDgInPc3	tento
faktorům	faktor	k1gInPc3	faktor
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgInPc2d1	finanční
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
se	se	k3xPyFc4	se
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poměrně	poměrně	k6eAd1	poměrně
chudý	chudý	k2eAgMnSc1d1	chudý
a	a	k8xC	a
zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
(	(	kIx(	(
<g/>
v	v	k7c6	v
srbské	srbský	k2eAgFnSc6d1	Srbská
cyrilici	cyrilice	k1gFnSc6	cyrilice
Н	Н	k?	Н
Т	Т	k?	Т
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1856	[number]	k4	1856
srbským	srbský	k2eAgMnPc3d1	srbský
rodičům	rodič	k1gMnPc3	rodič
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Smiljan	Smiljan	k1gMnSc1	Smiljan
blízko	blízko	k7c2	blízko
města	město	k1gNnSc2	město
Gospić	Gospić	k1gFnSc2	Gospić
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
Lika	Lik	k1gInSc2	Lik
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ležícím	ležící	k2eAgInSc7d1	ležící
v	v	k7c6	v
Chorvatském	chorvatský	k2eAgNnSc6d1	Chorvatské
království	království	k1gNnSc6	království
(	(	kIx(	(
<g/>
v	v	k7c6	v
personální	personální	k2eAgFnSc6d1	personální
unii	unie	k1gFnSc6	unie
s	s	k7c7	s
Uherskem	Uhersko	k1gNnSc7	Uhersko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Karlovaci	Karlovace	k1gFnSc6	Karlovace
odešel	odejít	k5eAaPmAgMnS	odejít
studovat	studovat	k5eAaImF	studovat
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
matematiku	matematika	k1gFnSc4	matematika
na	na	k7c4	na
polytechniku	polytechnika	k1gFnSc4	polytechnika
ve	v	k7c6	v
Štýrském	štýrský	k2eAgInSc6d1	štýrský
Hradci	Hradec	k1gInSc6	Hradec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
složil	složit	k5eAaPmAgMnS	složit
deset	deset	k4xCc4	deset
zkoušek	zkouška	k1gFnPc2	zkouška
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
Profesoři	profesor	k1gMnPc1	profesor
brzy	brzy	k6eAd1	brzy
poznali	poznat	k5eAaPmAgMnP	poznat
Teslův	Teslův	k2eAgInSc4d1	Teslův
talent	talent	k1gInSc4	talent
a	a	k8xC	a
umožnili	umožnit	k5eAaPmAgMnP	umožnit
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
při	při	k7c6	při
fyzikálních	fyzikální	k2eAgInPc6d1	fyzikální
pokusech	pokus	k1gInPc6	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
se	se	k3xPyFc4	se
Tesla	Tesla	k1gMnSc1	Tesla
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Gramovým	gramový	k2eAgNnSc7d1	gramové
dynamem	dynamo	k1gNnSc7	dynamo
a	a	k8xC	a
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
se	se	k3xPyFc4	se
utvrdil	utvrdit	k5eAaPmAgMnS	utvrdit
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
motory	motor	k1gInPc1	motor
na	na	k7c4	na
stejnosměrný	stejnosměrný	k2eAgInSc4d1	stejnosměrný
proud	proud	k1gInSc4	proud
se	se	k3xPyFc4	se
nehodí	hodit	k5eNaPmIp3nS	hodit
pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1883	[number]	k4	1883
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
první	první	k4xOgInSc4	první
motor	motor	k1gInSc4	motor
na	na	k7c4	na
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Teslu	Tesla	k1gFnSc4	Tesla
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
v	v	k7c6	v
letním	letní	k2eAgInSc6d1	letní
semestru	semestr	k1gInSc6	semestr
1880	[number]	k4	1880
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
Ernstem	Ernst	k1gMnSc7	Ernst
Machem	Mach	k1gMnSc7	Mach
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
ale	ale	k8xC	ale
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
zanechal	zanechat	k5eAaPmAgMnS	zanechat
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgInS	mít
dokončen	dokončit	k5eAaPmNgInS	dokončit
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
semestr	semestr	k1gInSc1	semestr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
telegrafní	telegrafní	k2eAgFnSc6d1	telegrafní
společnosti	společnost	k1gFnSc6	společnost
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
intenzivně	intenzivně	k6eAd1	intenzivně
zabývat	zabývat	k5eAaImF	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
objevil	objevit	k5eAaPmAgInS	objevit
princip	princip	k1gInSc1	princip
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1884	[number]	k4	1884
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
usadil	usadit	k5eAaPmAgInS	usadit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
(	(	kIx(	(
<g/>
občanství	občanství	k1gNnSc4	občanství
USA	USA	kA	USA
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gMnSc1	Tesla
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c4	v
společnosti	společnost	k1gFnPc4	společnost
Edison	Edisona	k1gFnPc2	Edisona
Machine	Machin	k1gInSc5	Machin
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neshodách	neshoda	k1gFnPc6	neshoda
s	s	k7c7	s
Edisonem	Edison	k1gInSc7	Edison
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
firmu	firma	k1gFnSc4	firma
Tesla	Tesla	k1gFnSc1	Tesla
Electric	Electrice	k1gFnPc2	Electrice
Light	Light	k2eAgInSc1d1	Light
&	&	k?	&
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
především	především	k9	především
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
a	a	k8xC	a
patentovala	patentovat	k5eAaBmAgFnS	patentovat
vylepšení	vylepšení	k1gNnSc4	vylepšení
pro	pro	k7c4	pro
obloukové	obloukový	k2eAgFnPc4d1	oblouková
lampy	lampa	k1gFnPc4	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
se	se	k3xPyFc4	se
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
věnovat	věnovat	k5eAaImF	věnovat
strojům	stroj	k1gInPc3	stroj
na	na	k7c4	na
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yRnSc7	což
investoři	investor	k1gMnPc1	investor
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
a	a	k8xC	a
propustili	propustit	k5eAaPmAgMnP	propustit
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Nenašel	najít	k5eNaPmAgInS	najít
si	se	k3xPyFc3	se
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
a	a	k8xC	a
jistý	jistý	k2eAgInSc4d1	jistý
čas	čas	k1gInSc4	čas
jako	jako	k8xS	jako
dělník	dělník	k1gMnSc1	dělník
kopal	kopat	k5eAaImAgMnS	kopat
příkopy	příkop	k1gInPc4	příkop
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Edisonovým	Edisonův	k2eAgMnSc7d1	Edisonův
velkým	velký	k2eAgMnSc7d1	velký
rivalem	rival	k1gMnSc7	rival
Westinghousem	Westinghous	k1gMnSc7	Westinghous
<g/>
,	,	kIx,	,
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
laboratořích	laboratoř	k1gFnPc6	laboratoř
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
udělal	udělat	k5eAaPmAgMnS	udělat
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgInPc2d1	jiný
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
učinil	učinit	k5eAaPmAgInS	učinit
objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
lze	lze	k6eAd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
točivé	točivý	k2eAgNnSc4d1	točivé
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
cívky	cívka	k1gFnPc1	cívka
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnPc1d1	postavená
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
úhlu	úhel	k1gInSc2	úhel
<g/>
,	,	kIx,	,
napájeny	napájen	k2eAgMnPc4d1	napájen
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
s	s	k7c7	s
fázovým	fázový	k2eAgInSc7d1	fázový
posunem	posun	k1gInSc7	posun
90	[number]	k4	90
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
umožnil	umožnit	k5eAaPmAgInS	umožnit
vynález	vynález	k1gInSc4	vynález
střídavého	střídavý	k2eAgInSc2d1	střídavý
indukčního	indukční	k2eAgInSc2d1	indukční
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
obvykle	obvykle	k6eAd1	obvykle
nazývá	nazývat	k5eAaImIp3nS	nazývat
asynchronní	asynchronní	k2eAgInSc1d1	asynchronní
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
asynchronního	asynchronní	k2eAgInSc2d1	asynchronní
motoru	motor	k1gInSc2	motor
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
stejnosměrným	stejnosměrný	k2eAgInPc3d1	stejnosměrný
a	a	k8xC	a
komutátorovým	komutátorový	k2eAgInPc3d1	komutátorový
motorům	motor	k1gInPc3	motor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
činnosti	činnost	k1gFnSc3	činnost
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
komutátor	komutátor	k1gInSc1	komutátor
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
levnější	levný	k2eAgMnSc1d2	levnější
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
životnost	životnost	k1gFnSc4	životnost
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
vynálezem	vynález	k1gInSc7	vynález
odstranil	odstranit	k5eAaPmAgMnS	odstranit
poslední	poslední	k2eAgFnSc4d1	poslední
překážku	překážka	k1gFnSc4	překážka
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
distribučních	distribuční	k2eAgFnPc2d1	distribuční
sítí	síť	k1gFnPc2	síť
střídavého	střídavý	k2eAgInSc2d1	střídavý
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zabývat	zabývat	k5eAaImF	zabývat
technologií	technologie	k1gFnSc7	technologie
rádiového	rádiový	k2eAgInSc2d1	rádiový
přenosu	přenos	k1gInSc2	přenos
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
veřejně	veřejně	k6eAd1	veřejně
předvedl	předvést	k5eAaPmAgMnS	předvést
radiokomunikační	radiokomunikační	k2eAgInSc4d1	radiokomunikační
přístroj	přístroj	k1gInSc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Teslovy	Teslův	k2eAgInPc1d1	Teslův
objevy	objev	k1gInPc1	objev
a	a	k8xC	a
vynálezy	vynález	k1gInPc1	vynález
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
<g/>
:	:	kIx,	:
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
přihlásit	přihlásit	k5eAaPmF	přihlásit
kolem	kolem	k7c2	kolem
300	[number]	k4	300
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
jeho	jeho	k3xOp3gInPc2	jeho
objevů	objev	k1gInPc2	objev
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
produkt	produkt	k1gInSc4	produkt
základního	základní	k2eAgInSc2d1	základní
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
si	se	k3xPyFc3	se
ihned	ihned	k6eAd1	ihned
neuvědomil	uvědomit	k5eNaPmAgMnS	uvědomit
jejich	jejich	k3xOp3gInSc4	jejich
budoucí	budoucí	k2eAgInSc4d1	budoucí
potenciál	potenciál	k1gInSc4	potenciál
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
nepublikoval	publikovat	k5eNaBmAgMnS	publikovat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
mu	on	k3xPp3gMnSc3	on
nebylo	být	k5eNaImAgNnS	být
přisouzeno	přisoudit	k5eAaPmNgNnS	přisoudit
prvenství	prvenství	k1gNnSc1	prvenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
narůstaly	narůstat	k5eAaImAgInP	narůstat
Teslovy	Teslův	k2eAgInPc4d1	Teslův
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
Edisonem	Edison	k1gInSc7	Edison
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
válce	válec	k1gInSc2	válec
proudů	proud	k1gInPc2	proud
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
horlivě	horlivě	k6eAd1	horlivě
diskutovalo	diskutovat	k5eAaImAgNnS	diskutovat
o	o	k7c6	o
vhodnosti	vhodnost	k1gFnSc6	vhodnost
užití	užití	k1gNnSc2	užití
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
nebo	nebo	k8xC	nebo
střídavého	střídavý	k2eAgInSc2d1	střídavý
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
v	v	k7c6	v
rozvodné	rozvodný	k2eAgFnSc6d1	rozvodná
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Edison	Edison	k1gMnSc1	Edison
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
chtěl	chtít	k5eAaImAgMnS	chtít
udržet	udržet	k5eAaPmF	udržet
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
patentů	patent	k1gInPc2	patent
na	na	k7c4	na
stejnosměrné	stejnosměrný	k2eAgInPc4d1	stejnosměrný
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
vynaložil	vynaložit	k5eAaPmAgMnS	vynaložit
mnoho	mnoho	k4c4	mnoho
úsilí	úsilí	k1gNnPc2	úsilí
a	a	k8xC	a
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Teslův	Teslův	k2eAgInSc1d1	Teslův
koncept	koncept	k1gInSc1	koncept
znevěrohodnil	znevěrohodnit	k5eAaPmAgInS	znevěrohodnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
neuspěl	uspět	k5eNaPmAgMnS	uspět
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
firma	firma	k1gFnSc1	firma
Westinghouse	Westinghouse	k1gFnSc2	Westinghouse
Electric	Electrice	k1gFnPc2	Electrice
Corporation	Corporation	k1gInSc1	Corporation
uvedla	uvést	k5eAaPmAgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
velkou	velký	k2eAgFnSc4d1	velká
elektrárnu	elektrárna	k1gFnSc4	elektrárna
na	na	k7c6	na
Niagarských	niagarský	k2eAgInPc6d1	niagarský
vodopádech	vodopád	k1gInPc6	vodopád
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
Teslových	Teslových	k2eAgInPc2d1	Teslových
generátorů	generátor	k1gInPc2	generátor
<g/>
,	,	kIx,	,
motorů	motor	k1gInPc2	motor
na	na	k7c4	na
střídavý	střídavý	k2eAgInSc4d1	střídavý
proud	proud	k1gInSc4	proud
(	(	kIx(	(
<g/>
AC	AC	kA	AC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zanedlouho	zanedlouho	k6eAd1	zanedlouho
zcela	zcela	k6eAd1	zcela
vytlačil	vytlačit	k5eAaPmAgInS	vytlačit
stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
proud	proud	k1gInSc1	proud
(	(	kIx(	(
<g/>
DC	DC	kA	DC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
se	se	k3xPyFc4	se
Tesla	Tesla	k1gMnSc1	Tesla
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
městečka	městečko	k1gNnSc2	městečko
Colorado	Colorado	k1gNnSc1	Colorado
Springs	Springs	k1gInSc1	Springs
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
velkou	velký	k2eAgFnSc4d1	velká
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
laboratoř	laboratoř	k1gFnSc4	laboratoř
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
zabýval	zabývat	k5eAaImAgInS	zabývat
pokročilými	pokročilý	k2eAgInPc7d1	pokročilý
a	a	k8xC	a
vizionářskými	vizionářský	k2eAgInPc7d1	vizionářský
výzkumy	výzkum	k1gInPc7	výzkum
souvisejícími	související	k2eAgInPc7d1	související
hlavně	hlavně	k6eAd1	hlavně
s	s	k7c7	s
bezdrátovým	bezdrátový	k2eAgInSc7d1	bezdrátový
přenosem	přenos	k1gInSc7	přenos
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
Tesla	Tesla	k1gFnSc1	Tesla
Colorado	Colorado	k1gNnSc1	Colorado
Springs	Springsa	k1gFnPc2	Springsa
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
finance	finance	k1gFnPc4	finance
od	od	k7c2	od
miliardáře	miliardář	k1gMnSc2	miliardář
J.	J.	kA	J.
P.	P.	kA	P.
Morgana	morgan	k1gMnSc4	morgan
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
projektu	projekt	k1gInSc2	projekt
tzv.	tzv.	kA	tzv.
Wardenclyffské	Wardenclyffský	k2eAgFnSc2d1	Wardenclyffský
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
umožňovat	umožňovat	k5eAaImF	umožňovat
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
distribuci	distribuce	k1gFnSc4	distribuce
elektřiny	elektřina	k1gFnSc2	elektřina
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
57	[number]	k4	57
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
věže	věž	k1gFnSc2	věž
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
na	na	k7c4	na
Long	Long	k1gInSc4	Long
Islandu	Island	k1gInSc2	Island
následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
projekt	projekt	k1gInSc4	projekt
si	se	k3xPyFc3	se
však	však	k9	však
vyžadoval	vyžadovat	k5eAaImAgMnS	vyžadovat
další	další	k2eAgNnSc4d1	další
financování	financování	k1gNnSc4	financování
a	a	k8xC	a
když	když	k8xS	když
Morgan	morgan	k1gMnSc1	morgan
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	let	k1gInPc6	let
neviděl	vidět	k5eNaImAgMnS	vidět
návratnost	návratnost	k1gFnSc4	návratnost
svých	svůj	k3xOyFgFnPc2	svůj
investic	investice	k1gFnPc2	investice
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgInS	přestat
Teslu	Tesla	k1gFnSc4	Tesla
podporovat	podporovat	k5eAaImF	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
Tesla	Tesla	k1gMnSc1	Tesla
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
prostředky	prostředek	k1gInPc4	prostředek
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
projekt	projekt	k1gInSc1	projekt
nebyl	být	k5eNaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
a	a	k8xC	a
Wardenclyffská	Wardenclyffský	k2eAgFnSc1d1	Wardenclyffský
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
zbořena	zbořit	k5eAaPmNgFnS	zbořit
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
právních	právní	k2eAgFnPc2d1	právní
tahanic	tahanice	k1gFnPc2	tahanice
s	s	k7c7	s
Guglielmem	Guglielmo	k1gNnSc7	Guglielmo
Marconim	Marconima	k1gFnPc2	Marconima
o	o	k7c4	o
vynález	vynález	k1gInSc4	vynález
rádia	rádio	k1gNnSc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Teslu	Tesla	k1gFnSc4	Tesla
velmi	velmi	k6eAd1	velmi
znechutilo	znechutit	k5eAaPmAgNnS	znechutit
<g/>
,	,	kIx,	,
když	když	k8xS	když
Marconi	Marcoň	k1gFnSc3	Marcoň
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
pro	pro	k7c4	pro
objev	objev	k1gInSc4	objev
rádia	rádio	k1gNnSc2	rádio
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
on	on	k3xPp3gMnSc1	on
ho	on	k3xPp3gInSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gNnSc1	jeho
prvenství	prvenství	k1gNnSc1	prvenství
bylo	být	k5eAaImAgNnS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prokazatelné	prokazatelný	k2eAgNnSc1d1	prokazatelné
<g/>
,	,	kIx,	,
Marconi	Marcoň	k1gFnSc3	Marcoň
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
tak	tak	k9	tak
tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
nakonec	nakonec	k6eAd1	nakonec
vyřešil	vyřešit	k5eAaPmAgInS	vyřešit
soud	soud	k1gInSc1	soud
v	v	k7c4	v
Teslův	Teslův	k2eAgInSc4d1	Teslův
prospěch	prospěch	k1gInSc4	prospěch
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
Teslově	Teslův	k2eAgFnSc6d1	Teslova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teslovy	Teslův	k2eAgInPc1d1	Teslův
finanční	finanční	k2eAgInPc1d1	finanční
problémy	problém	k1gInPc1	problém
narůstaly	narůstat	k5eAaImAgInP	narůstat
<g/>
,	,	kIx,	,
nedařilo	dařit	k5eNaImAgNnS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
získat	získat	k5eAaPmF	získat
finance	finance	k1gFnPc4	finance
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
projekty	projekt	k1gInPc4	projekt
a	a	k8xC	a
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgMnS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
více	hodně	k6eAd2	hodně
v	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
futuristickými	futuristický	k2eAgInPc7d1	futuristický
projekty	projekt	k1gInPc7	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
paprsky	paprsek	k1gInPc1	paprsek
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
teleforce	teleforka	k1gFnSc6	teleforka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
vládě	vláda	k1gFnSc3	vláda
USA	USA	kA	USA
k	k	k7c3	k
případné	případný	k2eAgFnSc3d1	případná
obraně	obrana	k1gFnSc3	obrana
proti	proti	k7c3	proti
nacistickému	nacistický	k2eAgNnSc3d1	nacistické
Německu	Německo	k1gNnSc3	Německo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
létajícími	létající	k2eAgInPc7d1	létající
stroji	stroj	k1gInPc7	stroj
poháněnými	poháněný	k2eAgInPc7d1	poháněný
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
šestadvaceti	šestadvacet	k4xCc6	šestadvacet
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
uplynuly	uplynout	k5eAaPmAgInP	uplynout
od	od	k7c2	od
chvíle	chvíle	k1gFnSc2	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
Tesla	Tesla	k1gFnSc1	Tesla
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Edisonovou	Edisonová	k1gFnSc7	Edisonová
medailí	medaile	k1gFnPc2	medaile
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obecný	obecný	k2eAgInSc4d1	obecný
názor	názor	k1gInSc4	názor
na	na	k7c4	na
Teslu	Tesla	k1gFnSc4	Tesla
postupně	postupně	k6eAd1	postupně
zcela	zcela	k6eAd1	zcela
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Přestal	přestat	k5eAaPmAgMnS	přestat
být	být	k5eAaImF	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
seriózního	seriózní	k2eAgMnSc4d1	seriózní
inženýra	inženýr	k1gMnSc4	inženýr
a	a	k8xC	a
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
veřejnosti	veřejnost	k1gFnSc2	veřejnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
stárnoucím	stárnoucí	k2eAgMnSc7d1	stárnoucí
podivínem	podivín	k1gMnSc7	podivín
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
chrlí	chrlit	k5eAaImIp3nS	chrlit
fantastické	fantastický	k2eAgInPc4d1	fantastický
články	článek	k1gInPc4	článek
pro	pro	k7c4	pro
populární	populární	k2eAgInPc4d1	populární
časopisy	časopis	k1gInPc4	časopis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
spisech	spis	k1gInPc6	spis
se	se	k3xPyFc4	se
zmínil	zmínit	k5eAaPmAgMnS	zmínit
o	o	k7c6	o
jím	on	k3xPp3gMnSc7	on
předpokládaných	předpokládaný	k2eAgInPc6d1	předpokládaný
dalekosáhlých	dalekosáhlý	k2eAgInPc6d1	dalekosáhlý
důsledcích	důsledek	k1gInPc6	důsledek
rozvoje	rozvoj	k1gInSc2	rozvoj
elektrických	elektrický	k2eAgNnPc2d1	elektrické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
zařadil	zařadit	k5eAaPmAgInS	zařadit
vznik	vznik	k1gInSc1	vznik
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
informační	informační	k2eAgFnSc2d1	informační
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
hlasu	hlas	k1gInSc2	hlas
<g/>
,	,	kIx,	,
textu	text	k1gInSc2	text
a	a	k8xC	a
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
sestrojil	sestrojit	k5eAaPmAgMnS	sestrojit
a	a	k8xC	a
použil	použít	k5eAaPmAgMnS	použít
rádiové	rádiový	k2eAgNnSc4d1	rádiové
dálkové	dálkový	k2eAgNnSc4d1	dálkové
ovládání	ovládání	k1gNnSc4	ovládání
<g/>
,	,	kIx,	,
předpokládal	předpokládat	k5eAaImAgInS	předpokládat
vývoj	vývoj	k1gInSc1	vývoj
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
umělé	umělý	k2eAgFnSc3d1	umělá
inteligenci	inteligence	k1gFnSc3	inteligence
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
a	a	k8xC	a
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
lidstvo	lidstvo	k1gNnSc1	lidstvo
bude	být	k5eAaImBp3nS	být
používat	používat	k5eAaImF	používat
k	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
převážně	převážně	k6eAd1	převážně
bojové	bojový	k2eAgInPc4d1	bojový
stroje	stroj	k1gInPc4	stroj
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
<g/>
,	,	kIx,	,
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
pohledu	pohled	k1gInSc2	pohled
logicky	logicky	k6eAd1	logicky
časem	časem	k6eAd1	časem
přestane	přestat	k5eAaPmIp3nS	přestat
k	k	k7c3	k
válkám	válka	k1gFnPc3	válka
docházet	docházet	k5eAaImF	docházet
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
se	se	k3xPyFc4	se
v	v	k7c6	v
pokročilém	pokročilý	k2eAgInSc6d1	pokročilý
věku	věk	k1gInSc6	věk
stal	stát	k5eAaPmAgInS	stát
vegetariánem	vegetarián	k1gMnSc7	vegetarián
<g/>
,	,	kIx,	,
žijícím	žijící	k2eAgMnSc7d1	žijící
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
mléku	mléko	k1gNnSc6	mléko
<g/>
,	,	kIx,	,
chlebu	chléb	k1gInSc6	chléb
<g/>
,	,	kIx,	,
medu	med	k1gInSc6	med
a	a	k8xC	a
zeleninových	zeleninový	k2eAgFnPc6d1	zeleninová
šťávách	šťáva	k1gFnPc6	šťáva
<g/>
.	.	kIx.	.
</s>
<s>
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
zemřel	zemřít	k5eAaPmAgMnS	zemřít
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1943	[number]	k4	1943
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
hotelu	hotel	k1gInSc2	hotel
New	New	k1gMnSc1	New
Yorker	Yorker	k1gMnSc1	Yorker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pocty	pocta	k1gFnPc4	pocta
a	a	k8xC	a
pomníky	pomník	k1gInPc4	pomník
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Teslovi	Tesla	k1gMnSc6	Tesla
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
tesla	tesla	k1gFnSc1	tesla
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
magnetické	magnetický	k2eAgFnSc2d1	magnetická
indukce	indukce	k1gFnSc2	indukce
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gMnSc2	Tesla
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
nese	nést	k5eAaImIp3nS	nést
např.	např.	kA	např.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
odvrácené	odvrácený	k2eAgFnSc6d1	odvrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
</s>
</p>
<p>
<s>
planetka	planetka	k1gFnSc1	planetka
2244	[number]	k4	2244
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
objevil	objevit	k5eAaPmAgInS	objevit
Milorad	Milorad	k1gInSc1	Milorad
B.	B.	kA	B.
Protić	Protić	k1gMnSc2	Protić
</s>
</p>
<p>
<s>
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Bělehradě	Bělehrad	k1gInSc6	Bělehrad
</s>
</p>
<p>
<s>
automobilka	automobilka	k1gFnSc1	automobilka
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomník	pomník	k1gInSc1	pomník
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
Teslovi	Tesla	k1gMnSc3	Tesla
byl	být	k5eAaImAgInS	být
postaven	postaven	k2eAgInSc4d1	postaven
mj.	mj.	kA	mj.
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
na	na	k7c6	na
Niagarských	niagarský	k2eAgInPc6d1	niagarský
vodopádech	vodopád	k1gInPc6	vodopád
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
</s>
</p>
<p>
<s>
v	v	k7c4	v
Queen	Queen	k1gInSc4	Queen
Victoria	Victorium	k1gNnSc2	Victorium
Parku	park	k1gInSc2	park
u	u	k7c2	u
Niagarských	niagarský	k2eAgInPc2d1	niagarský
vodopádů	vodopád	k1gInPc2	vodopád
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Ontariu	Ontario	k1gNnSc6	Ontario
(	(	kIx(	(
<g/>
představuje	představovat	k5eAaImIp3nS	představovat
Teslu	Tesla	k1gFnSc4	Tesla
stojícího	stojící	k2eAgMnSc2d1	stojící
na	na	k7c6	na
části	část	k1gFnSc6	část
alternátoru	alternátor	k1gInSc2	alternátor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
Záhřebu	Záhřeb	k1gInSc6	Záhřeb
</s>
</p>
<p>
<s>
ve	v	k7c6	v
Vidovdanu	Vidovdan	k1gInSc6	Vidovdan
(	(	kIx(	(
<g/>
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Tesla	Tesla	k1gFnSc1	Tesla
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
studoval	studovat	k5eAaImAgMnS	studovat
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1936	[number]	k4	1936
byl	být	k5eAaImAgMnS	být
Teslovi	Tesla	k1gMnSc3	Tesla
udělen	udělen	k2eAgInSc4d1	udělen
čestný	čestný	k2eAgInSc4d1	čestný
titul	titul	k1gInSc4	titul
doktora	doktor	k1gMnSc2	doktor
technických	technický	k2eAgFnPc2d1	technická
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
h.	h.	k?	h.
c.	c.	k?	c.
<g/>
)	)	kIx)	)
Českého	český	k2eAgNnSc2d1	české
vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
Dejvicích	Dejvice	k1gFnPc6	Dejvice
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
Teslovi	Tesla	k1gMnSc3	Tesla
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
ulice	ulice	k1gFnPc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
obdržel	obdržet	k5eAaPmAgInS	obdržet
také	také	k9	také
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1937	[number]	k4	1937
na	na	k7c6	na
Vysokém	vysoký	k2eAgNnSc6d1	vysoké
učení	učení	k1gNnSc6	učení
technickém	technický	k2eAgNnSc6d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1937	[number]	k4	1937
na	na	k7c6	na
oslavě	oslava	k1gFnSc6	oslava
Teslových	Teslových	k2eAgInPc2d1	Teslových
81	[number]	k4	81
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
mu	on	k3xPp3gMnSc3	on
předal	předat	k5eAaPmAgMnS	předat
český	český	k2eAgMnSc1d1	český
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
Řád	řád	k1gInSc1	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
<g />
.	.	kIx.	.
</s>
<s>
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
s	s	k7c7	s
vyobrazením	vyobrazení	k1gNnSc7	vyobrazení
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gFnSc2	Tesla
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
25	[number]	k4	25
haléřů	haléř	k1gInPc2	haléř
<g/>
,	,	kIx,	,
navržená	navržený	k2eAgFnSc1d1	navržená
Cyrilem	Cyril	k1gMnSc7	Cyril
Boudou	Bouda	k1gMnSc7	Bouda
a	a	k8xC	a
vyrytá	vyrytý	k2eAgFnSc1d1	vyrytá
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Goldschmiedem	Goldschmied	k1gMnSc7	Goldschmied
v	v	k7c6	v
nákladu	náklad	k1gInSc6	náklad
470	[number]	k4	470
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
odhalena	odhalit	k5eAaPmNgFnS	odhalit
busta	busta	k1gFnSc1	busta
Tesly	Tesla	k1gFnSc2	Tesla
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gMnSc2	Tesla
odhalen	odhalen	k2eAgInSc4d1	odhalen
pomník	pomník	k1gInSc4	pomník
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stylizovaného	stylizovaný	k2eAgInSc2d1	stylizovaný
výboje	výboj	k1gInSc2	výboj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
270	[number]	k4	270
<g/>
×	×	k?	×
<g/>
495	[number]	k4	495
<g/>
×	×	k?	×
<g/>
225	[number]	k4	225
cm	cm	kA	cm
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
největším	veliký	k2eAgInSc7d3	veliký
pomníkem	pomník	k1gInSc7	pomník
Nikoly	Nikola	k1gMnSc2	Nikola
Tesly	Tesla	k1gFnSc2	Tesla
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc1	pomník
je	být	k5eAaImIp3nS	být
dílem	díl	k1gInSc7	díl
akademického	akademický	k2eAgMnSc2d1	akademický
sochaře	sochař	k1gMnSc2	sochař
Stefana	Stefan	k1gMnSc2	Stefan
Milkova	Milkův	k2eAgMnSc2d1	Milkův
a	a	k8xC	a
architekta	architekt	k1gMnSc2	architekt
Jiřího	Jiří	k1gMnSc2	Jiří
Trojana	Trojan	k1gMnSc2	Trojan
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
pomníku	pomník	k1gInSc2	pomník
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
150	[number]	k4	150
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
narození	narození	k1gNnSc2	narození
Tesly	Tesla	k1gFnSc2	Tesla
a	a	k8xC	a
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
chorvatského	chorvatský	k2eAgMnSc4d1	chorvatský
prezidenta	prezident	k1gMnSc4	prezident
Stjepana	Stjepan	k1gMnSc4	Stjepan
Mesiće	Mesić	k1gMnSc4	Mesić
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc4d1	významný
vynálezy	vynález	k1gInPc4	vynález
==	==	k?	==
</s>
</p>
<p>
<s>
Asynchronní	asynchronní	k2eAgInSc1d1	asynchronní
motor	motor	k1gInSc1	motor
</s>
</p>
<p>
<s>
Teslův	Teslův	k2eAgInSc1d1	Teslův
transformátor	transformátor	k1gInSc1	transformátor
</s>
</p>
<p>
<s>
Teslova	Teslův	k2eAgFnSc1d1	Teslova
turbína	turbína	k1gFnSc1	turbína
</s>
</p>
<p>
<s>
Bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
komunikace	komunikace	k1gFnSc1	komunikace
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnPc4	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
LOMAS	LOMAS	kA	LOMAS
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
who	who	k?	who
Invented	Invented	k1gMnSc1	Invented
the	the	k?	the
Twentieth	Twentieth	k1gMnSc1	Twentieth
Century	Centura	k1gFnSc2	Centura
<g/>
:	:	kIx,	:
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
<g/>
,	,	kIx,	,
Forgotten	Forgotten	k2eAgMnSc1d1	Forgotten
Genius	genius	k1gMnSc1	genius
of	of	k?	of
Electricity	Electricita	k1gFnSc2	Electricita
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Headline	Headlin	k1gInSc5	Headlin
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
9780747275886	[number]	k4	9780747275886
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHANEY	CHANEY	kA	CHANEY
<g/>
,	,	kIx,	,
Margaret	Margareta	k1gFnPc2	Margareta
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
mimo	mimo	k7c4	mimo
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Vladislav	Vladislav	k1gMnSc1	Vladislav
Malát	Malát	k1gMnSc1	Malát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Citadela	citadela	k1gFnSc1	citadela
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
970875	[number]	k4	970875
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KENT	KENT	kA	KENT
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
J.	J.	kA	J.
Tesla	Tesla	k1gMnSc1	Tesla
–	–	k?	–
génius	génius	k1gMnSc1	génius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zkrotil	zkrotit	k5eAaPmAgMnS	zkrotit
elektřinu	elektřina	k1gFnSc4	elektřina
(	(	kIx(	(
<g/>
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Wizard	Wizard	k1gMnSc1	Wizard
of	of	k?	of
Electricity	Electricita	k1gFnSc2	Electricita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Radek	Radek	k1gMnSc1	Radek
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
..	..	k?	..
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gNnSc1	jota
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7565	[number]	k4	7565
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Tesla	Tesla	k1gFnSc1	Tesla
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Nikola	Nikola	k1gFnSc1	Nikola
Tesla	Tesla	k1gFnSc1	Tesla
</s>
</p>
<p>
<s>
Fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
změnil	změnit	k5eAaPmAgInS	změnit
svět	svět	k1gInSc4	svět
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Údajný	údajný	k2eAgInSc1d1	údajný
vlastní	vlastní	k2eAgInSc1d1	vlastní
Teslův	Teslův	k2eAgInSc1d1	Teslův
životopis	životopis	k1gInSc1	životopis
</s>
</p>
<p>
<s>
Disidenti	disident	k1gMnPc1	disident
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
vědecky	vědecky	k6eAd1	vědecky
nepodložené	podložený	k2eNgInPc1d1	nepodložený
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
bulvární	bulvární	k2eAgFnSc6d1	bulvární
rovině	rovina	k1gFnSc6	rovina
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
Teslově	Teslův	k2eAgNnSc6d1	Teslovo
díle	dílo	k1gNnSc6	dílo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
srbsky	srbsky	k6eAd1	srbsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Nikola	Nikola	k1gMnSc1	Nikola
Tesla	Tesla	k1gMnSc1	Tesla
Museum	museum	k1gNnSc4	museum
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Vybrané	vybraný	k2eAgInPc1d1	vybraný
články	článek	k1gInPc1	článek
od	od	k7c2	od
Tesly	Tesla	k1gFnSc2	Tesla
a	a	k8xC	a
o	o	k7c6	o
Teslovi	Tesla	k1gMnSc6	Tesla
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
s	s	k7c7	s
Teslovou	Teslův	k2eAgFnSc7d1	Teslova
pozůstalostí	pozůstalost	k1gFnSc7	pozůstalost
</s>
</p>
