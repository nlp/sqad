<p>
<s>
Kartografie	kartografie	k1gFnSc1	kartografie
je	být	k5eAaImIp3nS	být
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
tvorbou	tvorba	k1gFnSc7	tvorba
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
OSN	OSN	kA	OSN
definuje	definovat	k5eAaBmIp3nS	definovat
kartografii	kartografie	k1gFnSc4	kartografie
jako	jako	k8xC	jako
vědu	věda	k1gFnSc4	věda
o	o	k7c6	o
sestavování	sestavování	k1gNnSc6	sestavování
map	mapa	k1gFnPc2	mapa
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
veškeré	veškerý	k3xTgFnPc4	veškerý
operace	operace	k1gFnPc4	operace
od	od	k7c2	od
počátečního	počáteční	k2eAgNnSc2d1	počáteční
vyměřování	vyměřování	k1gNnSc2	vyměřování
až	až	k9	až
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
hotové	hotový	k2eAgFnSc2d1	hotová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kartografická	kartografický	k2eAgFnSc1d1	kartografická
asociace	asociace	k1gFnSc1	asociace
(	(	kIx(	(
<g/>
ICA	ICA	kA	ICA
<g/>
)	)	kIx)	)
definuje	definovat	k5eAaBmIp3nS	definovat
kartografii	kartografie	k1gFnSc4	kartografie
jako	jako	k8xS	jako
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
technologie	technologie	k1gFnSc1	technologie
vytváření	vytváření	k1gNnSc2	vytváření
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jejich	jejich	k3xOp3gNnSc2	jejich
studia	studio	k1gNnSc2	studio
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k9	jako
vědeckých	vědecký	k2eAgMnPc2d1	vědecký
dokumentů	dokument	k1gInPc2	dokument
a	a	k8xC	a
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
<g/>
ČSN	ČSN	kA	ČSN
definují	definovat	k5eAaBmIp3nP	definovat
kartografii	kartografie	k1gFnSc4	kartografie
jako	jako	k8xC	jako
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
znázorněním	znázornění	k1gNnSc7	znázornění
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
a	a	k8xC	a
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jevů	jev	k1gInPc2	jev
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
vztahů	vztah	k1gInPc2	vztah
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kartografického	kartografický	k2eAgNnSc2d1	kartografické
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
soubor	soubor	k1gInSc1	soubor
činností	činnost	k1gFnPc2	činnost
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
a	a	k8xC	a
využívání	využívání	k1gNnSc6	využívání
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
<g/>
Geoinformační	Geoinformační	k2eAgFnSc1d1	Geoinformační
definice	definice	k1gFnSc1	definice
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
Kartografie	kartografie	k1gFnSc1	kartografie
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
přenosu	přenos	k1gInSc2	přenos
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
prostorová	prostorový	k2eAgFnSc1d1	prostorová
datová	datový	k2eAgFnSc1d1	datová
báze	báze	k1gFnSc1	báze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
mnohovrstevný	mnohovrstevný	k2eAgInSc4d1	mnohovrstevný
model	model	k1gInSc4	model
geografické	geografický	k2eAgFnSc2d1	geografická
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
prostorová	prostorový	k2eAgFnSc1d1	prostorová
datová	datový	k2eAgFnSc1d1	datová
báze	báze	k1gFnSc1	báze
je	být	k5eAaImIp3nS	být
základnou	základna	k1gFnSc7	základna
pro	pro	k7c4	pro
dílčí	dílčí	k2eAgInPc4d1	dílčí
kartografické	kartografický	k2eAgInPc4d1	kartografický
procesy	proces	k1gInPc4	proces
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
čerpá	čerpat	k5eAaImIp3nS	čerpat
data	datum	k1gNnPc4	datum
z	z	k7c2	z
rozmanitých	rozmanitý	k2eAgInPc2d1	rozmanitý
vstupů	vstup	k1gInPc2	vstup
a	a	k8xC	a
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
informačních	informační	k2eAgInPc2d1	informační
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Země	země	k1gFnSc1	země
a	a	k8xC	a
síť	síť	k1gFnSc1	síť
myšlených	myšlený	k2eAgFnPc2d1	myšlená
čar	čára	k1gFnPc2	čára
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
běžně	běžně	k6eAd1	běžně
představujeme	představovat	k5eAaImIp1nP	představovat
jako	jako	k9	jako
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
poněkud	poněkud	k6eAd1	poněkud
zploštělá	zploštělý	k2eAgFnSc1d1	zploštělá
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
v	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
rovině	rovina	k1gFnSc6	rovina
je	být	k5eAaImIp3nS	být
6378	[number]	k4	6378
km	km	kA	km
<g/>
,	,	kIx,	,
poloměr	poloměr	k1gInSc4	poloměr
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
oběma	dva	k4xCgInPc7	dva
póly	pólo	k1gNnPc7	pólo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
6356	[number]	k4	6356
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
jako	jako	k9	jako
kouli	koule	k1gFnSc4	koule
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
popsat	popsat	k5eAaPmF	popsat
Zemi	zem	k1gFnSc4	zem
jako	jako	k8xS	jako
rotační	rotační	k2eAgInSc4d1	rotační
elipsoid	elipsoid	k1gInSc4	elipsoid
<g/>
.	.	kIx.	.
</s>
<s>
Problematikou	problematika	k1gFnSc7	problematika
volby	volba	k1gFnSc2	volba
referenčních	referenční	k2eAgFnPc2d1	referenční
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
zobrazení	zobrazení	k1gNnSc6	zobrazení
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
matematická	matematický	k2eAgFnSc1d1	matematická
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Síť	síť	k1gFnSc1	síť
myšlených	myšlený	k2eAgFnPc2d1	myšlená
čar	čára	k1gFnPc2	čára
na	na	k7c6	na
Zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
====	====	k?	====
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
myšlená	myšlený	k2eAgFnSc1d1	myšlená
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
procházející	procházející	k2eAgFnSc1d1	procházející
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
oběma	dva	k4xCgInPc7	dva
zeměpisnými	zeměpisný	k2eAgInPc7d1	zeměpisný
póly	pól	k1gInPc7	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
póly	pól	k1gInPc4	pól
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
póly	pól	k1gInPc4	pól
jsou	být	k5eAaImIp3nP	být
myšlené	myšlený	k2eAgInPc4d1	myšlený
body	bod	k1gInPc4	bod
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
protíná	protínat	k5eAaImIp3nS	protínat
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
zeměpisném	zeměpisný	k2eAgInSc6d1	zeměpisný
pólu	pól	k1gInSc6	pól
se	se	k3xPyFc4	se
Země	země	k1gFnSc1	země
otáčí	otáčet	k5eAaImIp3nS	otáčet
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
po	po	k7c6	po
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
A	a	k9	a
–	–	k?	–
střed	střed	k1gInSc1	střed
Země	země	k1gFnSc1	země
<g/>
;	;	kIx,	;
B	B	kA	B
–	–	k?	–
zemská	zemský	k2eAgFnSc1d1	zemská
osa	osa	k1gFnSc1	osa
<g/>
;	;	kIx,	;
C	C	kA	C
–	–	k?	–
poledník	poledník	k1gMnSc1	poledník
<g/>
;	;	kIx,	;
D	D	kA	D
–	–	k?	–
protipoledník	protipoledník	k1gMnSc1	protipoledník
poledníku	poledník	k1gInSc2	poledník
C	C	kA	C
<g/>
;	;	kIx,	;
C	C	kA	C
<g/>
–	–	k?	–
<g/>
D	D	kA	D
–	–	k?	–
poledníková	poledníkový	k2eAgFnSc1d1	Poledníková
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
;	;	kIx,	;
E	E	kA	E
–	–	k?	–
rovina	rovina	k1gFnSc1	rovina
proložená	proložený	k2eAgFnSc1d1	proložená
zemskou	zemský	k2eAgFnSc7d1	zemská
osou	osa	k1gFnSc7	osa
<g/>
;	;	kIx,	;
F	F	kA	F
–	–	k?	–
rovník	rovník	k1gInSc1	rovník
<g/>
;	;	kIx,	;
G	G	kA	G
–	–	k?	–
rovina	rovina	k1gFnSc1	rovina
kolmá	kolmý	k2eAgFnSc1d1	kolmá
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
proložená	proložený	k2eAgFnSc1d1	proložená
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Poledníky	poledník	k1gInPc4	poledník
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Proložíme	proložit	k5eAaPmIp1nP	proložit
<g/>
-li	i	k?	-li
rovinu	rovina	k1gFnSc4	rovina
zemskou	zemský	k2eAgFnSc7d1	zemská
osou	osa	k1gFnSc7	osa
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tato	tento	k3xDgFnSc1	tento
rovina	rovina	k1gFnSc1	rovina
protíná	protínat	k5eAaImIp3nS	protínat
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
myšlenou	myšlený	k2eAgFnSc4d1	myšlená
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
poledníková	poledníkový	k2eAgFnSc1d1	Poledníková
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
této	tento	k3xDgFnSc2	tento
kružnice	kružnice	k1gFnSc2	kružnice
je	být	k5eAaImIp3nS	být
poledník	poledník	k1gInSc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Kterýkoliv	kterýkoliv	k3yIgInSc1	kterýkoliv
poledník	poledník	k1gInSc1	poledník
je	být	k5eAaImIp3nS	být
nejkratší	krátký	k2eAgFnSc7d3	nejkratší
spojnicí	spojnice	k1gFnSc7	spojnice
protilehlých	protilehlý	k2eAgInPc2d1	protilehlý
pólů	pól	k1gInPc2	pól
po	po	k7c6	po
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
určuje	určovat	k5eAaImIp3nS	určovat
proto	proto	k8xC	proto
vždy	vždy	k6eAd1	vždy
směr	směr	k1gInSc4	směr
na	na	k7c4	na
zeměpisný	zeměpisný	k2eAgInSc4d1	zeměpisný
sever	sever	k1gInSc4	sever
a	a	k8xC	a
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Poledník	poledník	k1gMnSc1	poledník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
určitým	určitý	k2eAgInSc7d1	určitý
bodem	bod	k1gInSc7	bod
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
místním	místní	k2eAgInSc7d1	místní
poledníkem	poledník	k1gInSc7	poledník
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Protilehlá	protilehlý	k2eAgFnSc1d1	protilehlá
polovina	polovina	k1gFnSc1	polovina
poledníkové	poledníkový	k2eAgFnSc2d1	Poledníková
kružnice	kružnice	k1gFnSc2	kružnice
je	být	k5eAaImIp3nS	být
protipoledníkem	protipoledník	k1gMnSc7	protipoledník
místního	místní	k2eAgInSc2d1	místní
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poledník	poledník	k1gInSc1	poledník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
londýnskou	londýnský	k2eAgFnSc7d1	londýnská
hvězdárnou	hvězdárna	k1gFnSc7	hvězdárna
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
základní	základní	k2eAgFnSc4d1	základní
(	(	kIx(	(
<g/>
nultý	nultý	k4xOgInSc4	nultý
<g/>
)	)	kIx)	)
poledník	poledník	k1gInSc4	poledník
<g/>
,	,	kIx,	,
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
greenwichský	greenwichský	k2eAgInSc1d1	greenwichský
základní	základní	k2eAgInSc1d1	základní
poledník	poledník	k1gInSc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Pootočíme	pootočit	k5eAaPmIp1nP	pootočit
<g/>
-li	i	k?	-li
poledníkovou	poledníkový	k2eAgFnSc4d1	Poledníková
kružnici	kružnice	k1gFnSc4	kružnice
tohoto	tento	k3xDgInSc2	tento
poledníku	poledník	k1gInSc2	poledník
kolem	kolem	k7c2	kolem
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
vždy	vždy	k6eAd1	vždy
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
celkem	celkem	k6eAd1	celkem
180	[number]	k4	180
poledníkových	poledníkový	k2eAgFnPc2d1	Poledníková
kružnic	kružnice	k1gFnPc2	kružnice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
360	[number]	k4	360
poledníků	poledník	k1gInPc2	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
poledníky	poledník	k1gInPc1	poledník
jsou	být	k5eAaImIp3nP	být
číslovány	číslovat	k5eAaImNgInP	číslovat
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
i	i	k8xC	i
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Protipoledník	Protipoledník	k1gMnSc1	Protipoledník
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k8xC	tedy
číslo	číslo	k1gNnSc1	číslo
180	[number]	k4	180
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
<g/>
-li	i	k?	-li
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
sousedními	sousední	k2eAgInPc7d1	sousední
poledníky	poledník	k1gInPc7	poledník
na	na	k7c4	na
60	[number]	k4	60
stejných	stejný	k2eAgInPc2d1	stejný
dílků	dílek	k1gInPc2	dílek
(	(	kIx(	(
<g/>
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
a	a	k8xC	a
každým	každý	k3xTgInSc7	každý
tímto	tento	k3xDgInSc7	tento
dílkem	dílek	k1gInSc7	dílek
vedeme	vést	k5eAaImIp1nP	vést
další	další	k2eAgInSc4d1	další
poledník	poledník	k1gInSc4	poledník
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
minutové	minutový	k2eAgInPc4d1	minutový
poledníky	poledník	k1gInPc4	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Obdobným	obdobný	k2eAgNnSc7d1	obdobné
dělením	dělení	k1gNnSc7	dělení
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
mezi	mezi	k7c7	mezi
minutovými	minutový	k2eAgInPc7d1	minutový
poledníky	poledník	k1gInPc7	poledník
dostaneme	dostat	k5eAaPmIp1nP	dostat
60	[number]	k4	60
<g/>
vteřinových	vteřinový	k2eAgMnPc2d1	vteřinový
poledníků	poledník	k1gMnPc2	poledník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Rovník	rovník	k1gInSc4	rovník
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Proložíme	proložit	k5eAaPmIp1nP	proložit
<g/>
-li	i	k?	-li
rovinu	rovina	k1gFnSc4	rovina
kolmou	kolmý	k2eAgFnSc4d1	kolmá
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
středem	středem	k7c2	středem
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tato	tento	k3xDgFnSc1	tento
rovina	rovina	k1gFnSc1	rovina
protíná	protínat	k5eAaImIp3nS	protínat
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
myšlenou	myšlený	k2eAgFnSc4d1	myšlená
kružnici	kružnice	k1gFnSc4	kružnice
–	–	k?	–
rovník	rovník	k1gInSc1	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
dělí	dělit	k5eAaImIp3nS	dělit
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
kružnicí	kružnice	k1gFnSc7	kružnice
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přibližné	přibližný	k2eAgNnSc4d1	přibližné
40	[number]	k4	40
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
je	být	k5eAaImIp3nS	být
kružnice	kružnice	k1gFnSc1	kružnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
,	,	kIx,	,
protneme	protnout	k5eAaPmIp1nP	protnout
<g/>
-li	i	k?	-li
zemský	zemský	k2eAgInSc1d1	zemský
povrch	povrch	k1gInSc1	povrch
rovinou	rovina	k1gFnSc7	rovina
kolmou	kolmý	k2eAgFnSc7d1	kolmá
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
rovnoběžkou	rovnoběžka	k1gFnSc7	rovnoběžka
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rovník	rovník	k1gInSc1	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
k	k	k7c3	k
pólům	pól	k1gInPc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
Rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
leží	ležet	k5eAaImIp3nP	ležet
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
východ	východ	k1gInSc4	východ
–	–	k?	–
západ	západ	k1gInSc1	západ
a	a	k8xC	a
protínají	protínat	k5eAaImIp3nP	protínat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
s	s	k7c7	s
poledníky	poledník	k1gMnPc7	poledník
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
<g/>
-li	i	k?	-li
kterýkoliv	kterýkoliv	k3yIgInSc4	kterýkoliv
poledník	poledník	k1gInSc4	poledník
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
na	na	k7c4	na
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
a	a	k8xC	a
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsme	být	k5eAaImIp1nP	být
takto	takto	k6eAd1	takto
získali	získat	k5eAaPmAgMnP	získat
<g/>
,	,	kIx,	,
proložíme	proložit	k5eAaPmIp1nP	proložit
rovinami	rovina	k1gFnPc7	rovina
kolmými	kolmý	k2eAgFnPc7d1	kolmá
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
protnutím	protnutí	k1gNnSc7	protnutí
těchto	tento	k3xDgFnPc2	tento
rovin	rovina	k1gFnPc2	rovina
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
Země	zem	k1gFnSc2	zem
180	[number]	k4	180
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
<g/>
,	,	kIx,	,
90	[number]	k4	90
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
a	a	k8xC	a
90	[number]	k4	90
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
jsou	být	k5eAaImIp3nP	být
číslovány	číslovat	k5eAaImNgFnP	číslovat
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólům	pól	k1gInPc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
Rovník	rovník	k1gInSc1	rovník
je	být	k5eAaImIp3nS	být
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
nultou	nultý	k4xOgFnSc4	nultý
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
<g/>
,	,	kIx,	,
póly	pól	k1gInPc4	pól
jsou	být	k5eAaImIp3nP	být
devadesátými	devadesátý	k4xOgFnPc7	devadesátý
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
<g/>
-li	i	k?	-li
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
sousedními	sousední	k2eAgFnPc7d1	sousední
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
na	na	k7c4	na
60	[number]	k4	60
stejných	stejný	k2eAgInPc2d1	stejný
dílů	díl	k1gInPc2	díl
(	(	kIx(	(
<g/>
minut	minuta	k1gFnPc2	minuta
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedeme	vést	k5eAaImIp1nP	vést
těmito	tento	k3xDgNnPc7	tento
dílky	dílek	k1gInPc4	dílek
roviny	rovina	k1gFnSc2	rovina
kolmé	kolmý	k2eAgFnSc2d1	kolmá
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
minutové	minutový	k2eAgFnPc4d1	minutová
rovnoběžky	rovnoběžka	k1gFnPc4	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
<g/>
-li	i	k?	-li
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
sousedními	sousední	k2eAgFnPc7d1	sousední
minutovými	minutový	k2eAgFnPc7d1	minutová
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
na	na	k7c4	na
dalších	další	k2eAgInPc2d1	další
60	[number]	k4	60
stejných	stejný	k2eAgInPc2d1	stejný
dílků	dílek	k1gInPc2	dílek
(	(	kIx(	(
<g/>
vteřin	vteřina	k1gFnPc2	vteřina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získáme	získat	k5eAaPmIp1nP	získat
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
vteřinové	vteřinový	k2eAgFnSc2d1	vteřinová
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Udávání	udávání	k1gNnPc1	udávání
poloh	poloha	k1gFnPc2	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Udávání	udávání	k1gNnSc1	udávání
poloh	poloha	k1gFnPc2	poloha
pomocí	pomocí	k7c2	pomocí
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
souřadnic	souřadnice	k1gFnPc2	souřadnice
===	===	k?	===
</s>
</p>
<p>
<s>
Zeměpisnými	zeměpisný	k2eAgFnPc7d1	zeměpisná
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
rozumíme	rozumět	k5eAaImIp1nP	rozumět
poledníky	poledník	k1gMnPc4	poledník
a	a	k8xC	a
rovnoběžky	rovnoběžka	k1gFnPc4	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
bod	bod	k1gInSc1	bod
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
průsečíku	průsečík	k1gInSc6	průsečík
některého	některý	k3yIgInSc2	některý
poledníku	poledník	k1gInSc2	poledník
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
<g/>
-li	i	k?	-li
určit	určit	k5eAaPmF	určit
polohu	poloha	k1gFnSc4	poloha
některého	některý	k3yIgInSc2	některý
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
pomocí	pomocí	k7c2	pomocí
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
,	,	kIx,	,
označíme	označit	k5eAaPmIp1nP	označit
ji	on	k3xPp3gFnSc4	on
číslem	číslo	k1gNnSc7	číslo
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
a	a	k8xC	a
číslem	číslo	k1gNnSc7	číslo
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tímto	tento	k3xDgInSc7	tento
bodem	bod	k1gInSc7	bod
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Používáme	používat	k5eAaImIp1nP	používat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
číslovaní	číslovaný	k2eAgMnPc1d1	číslovaný
ve	v	k7c6	v
stupních	stupeň	k1gInPc6	stupeň
<g/>
,	,	kIx,	,
minutách	minuta	k1gFnPc6	minuta
a	a	k8xC	a
vteřinách	vteřina	k1gFnPc6	vteřina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
udávaného	udávaný	k2eAgInSc2d1	udávaný
bodu	bod	k1gInSc2	bod
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
nazýváme	nazývat	k5eAaImIp1nP	nazývat
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
šířkou	šířka	k1gFnSc7	šířka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
severní	severní	k2eAgNnSc1d1	severní
(	(	kIx(	(
<g/>
s.	s.	k?	s.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
udávaný	udávaný	k2eAgInSc4d1	udávaný
bod	bod	k1gInSc4	bod
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
,	,	kIx,	,
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
(	(	kIx(	(
<g/>
j.	j.	k?	j.
š.	š.	k?	š.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
poledníku	poledník	k1gInSc2	poledník
udávaného	udávaný	k2eAgInSc2d1	udávaný
bodu	bod	k1gInSc2	bod
od	od	k7c2	od
nultého	nultý	k4xOgInSc2	nultý
poledníku	poledník	k1gInSc2	poledník
nazýváme	nazývat	k5eAaImIp1nP	nazývat
zeměpisnou	zeměpisný	k2eAgFnSc7d1	zeměpisná
délkou	délka	k1gFnSc7	délka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
východní	východní	k2eAgInSc1d1	východní
(	(	kIx(	(
<g/>
v.	v.	k?	v.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
udávaný	udávaný	k2eAgInSc4d1	udávaný
bod	bod	k1gInSc4	bod
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
a	a	k8xC	a
západní	západní	k2eAgFnSc1d1	západní
(	(	kIx(	(
<g/>
z.	z.	k?	z.
d.	d.	k?	d.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
uvádí	uvádět	k5eAaImIp3nS	uvádět
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
a	a	k8xC	a
jako	jako	k8xS	jako
druhá	druhý	k4xOgFnSc1	druhý
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
délka	délka	k1gFnSc1	délka
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
také	také	k9	také
použít	použít	k5eAaPmF	použít
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
anglosaského	anglosaský	k2eAgNnSc2d1	anglosaské
značení	značení	k1gNnSc2	značení
základních	základní	k2eAgInPc2d1	základní
zeměpisných	zeměpisný	k2eAgInPc2d1	zeměpisný
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
jest	být	k5eAaImIp3nS	být
N	N	kA	N
pro	pro	k7c4	pro
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
E	E	kA	E
pro	pro	k7c4	pro
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
S	s	k7c7	s
pro	pro	k7c4	pro
jih	jih	k1gInSc4	jih
a	a	k8xC	a
W	W	kA	W
pro	pro	k7c4	pro
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
údaj	údaj	k1gInSc1	údaj
příslušné	příslušný	k2eAgFnSc2d1	příslušná
polohy	poloha	k1gFnSc2	poloha
vypadal	vypadat	k5eAaImAgInS	vypadat
např.	např.	kA	např.
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
N	N	kA	N
60	[number]	k4	60
<g/>
°	°	k?	°
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
'	'	kIx"	'
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
"	"	kIx"	"
–	–	k?	–
E	E	kA	E
82	[number]	k4	82
<g/>
°	°	k?	°
<g/>
33	[number]	k4	33
<g/>
'	'	kIx"	'
<g/>
14	[number]	k4	14
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čísla	číslo	k1gNnPc1	číslo
poledníků	poledník	k1gInPc2	poledník
a	a	k8xC	a
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
bývají	bývat	k5eAaImIp3nP	bývat
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
map	mapa	k1gFnPc2	mapa
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgInPc1d1	celý
stupně	stupeň	k1gInPc1	stupeň
bývají	bývat	k5eAaImIp3nP	bývat
označeny	označit	k5eAaPmNgInP	označit
číselně	číselně	k6eAd1	číselně
<g/>
,	,	kIx,	,
minuty	minuta	k1gFnPc1	minuta
zpravidla	zpravidla	k6eAd1	zpravidla
graficky	graficky	k6eAd1	graficky
po	po	k7c6	po
desítkách	desítka	k1gFnPc6	desítka
<g/>
,	,	kIx,	,
vteřiny	vteřina	k1gFnPc1	vteřina
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
musí	muset	k5eAaImIp3nP	muset
odměřovat	odměřovat	k5eAaImF	odměřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Určovaní	určovaný	k2eAgMnPc1d1	určovaný
poloh	poloha	k1gFnPc2	poloha
jiným	jiný	k2eAgInSc7d1	jiný
souřadicovým	souřadicův	k2eAgInSc7d1	souřadicův
systémem	systém	k1gInSc7	systém
nebo	nebo	k8xC	nebo
kódem	kód	k1gInSc7	kód
====	====	k?	====
</s>
</p>
<p>
<s>
K	k	k7c3	k
různým	různý	k2eAgInPc3d1	různý
účelům	účel	k1gInPc3	účel
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
zakresleny	zakreslen	k2eAgInPc4d1	zakreslen
jiné	jiný	k2eAgInPc4d1	jiný
než	než	k8xS	než
zeměpisné	zeměpisný	k2eAgInPc4d1	zeměpisný
souřadnicové	souřadnicový	k2eAgInPc4d1	souřadnicový
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
více	hodně	k6eAd2	hodně
či	či	k8xC	či
měně	měna	k1gFnSc3	měna
přesnému	přesný	k2eAgInSc3d1	přesný
určovaní	určovaný	k2eAgMnPc1d1	určovaný
poloh	poloha	k1gFnPc2	poloha
příjemcem	příjemce	k1gMnSc7	příjemce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mapu	mapa	k1gFnSc4	mapa
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
souřadnicovým	souřadnicový	k2eAgInSc7d1	souřadnicový
systémem	systém	k1gInSc7	systém
nebo	nebo	k8xC	nebo
kódem	kód	k1gInSc7	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
kružnice	kružnice	k1gFnSc1	kružnice
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
<g/>
,	,	kIx,	,
loxodroma	loxodroma	k1gFnSc1	loxodroma
a	a	k8xC	a
ortodroma	ortodroma	k1gFnSc1	ortodroma
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Velká	velký	k2eAgFnSc1d1	velká
kružnice	kružnice	k1gFnSc1	kružnice
===	===	k?	===
</s>
</p>
<p>
<s>
Protneme	protnout	k5eAaPmIp1nP	protnout
<g/>
-li	i	k?	-li
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
rovinou	rovina	k1gFnSc7	rovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nám	my	k3xPp1nPc3	my
průsečnice	průsečnice	k1gFnSc1	průsečnice
této	tento	k3xDgFnSc2	tento
roviny	rovina	k1gFnSc2	rovina
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
velkou	velký	k2eAgFnSc4d1	velká
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velkými	velký	k2eAgFnPc7d1	velká
kružnicemi	kružnice	k1gFnPc7	kružnice
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
i	i	k8xC	i
všechny	všechen	k3xTgFnPc4	všechen
poledníkové	poledníkový	k2eAgFnPc4d1	Poledníková
kružnice	kružnice	k1gFnPc4	kružnice
a	a	k8xC	a
rovník	rovník	k1gInSc4	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Naprosto	naprosto	k6eAd1	naprosto
však	však	k9	však
není	být	k5eNaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
roviny	rovina	k1gFnPc4	rovina
velkých	velký	k2eAgFnPc2d1	velká
kružnic	kružnice	k1gFnPc2	kružnice
byly	být	k5eAaImAgInP	být
kolmé	kolmý	k2eAgInPc1d1	kolmý
k	k	k7c3	k
zemské	zemský	k2eAgFnSc3d1	zemská
ose	osa	k1gFnSc3	osa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Malá	malý	k2eAgFnSc1d1	malá
kružnice	kružnice	k1gFnSc1	kružnice
===	===	k?	===
</s>
</p>
<p>
<s>
Protneme	protnout	k5eAaPmIp1nP	protnout
<g/>
-li	i	k?	-li
povrch	povrch	k1gInSc4	povrch
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
rovinou	rovina	k1gFnSc7	rovina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neprochází	procházet	k5eNaImIp3nS	procházet
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nám	my	k3xPp1nPc3	my
průsečnice	průsečnice	k1gFnSc1	průsečnice
této	tento	k3xDgFnSc2	tento
roviny	rovina	k1gFnSc2	rovina
s	s	k7c7	s
povrchem	povrch	k1gInSc7	povrch
zeměkoule	zeměkoule	k1gFnSc1	zeměkoule
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
malou	malý	k2eAgFnSc4d1	malá
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
malými	malý	k2eAgFnPc7d1	malá
kružnicemi	kružnice	k1gFnPc7	kružnice
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
všechny	všechen	k3xTgFnPc1	všechen
rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
kromě	kromě	k7c2	kromě
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Loxodroma	loxodroma	k1gFnSc1	loxodroma
===	===	k?	===
</s>
</p>
<p>
<s>
Loxodroma	loxodroma	k1gFnSc1	loxodroma
je	být	k5eAaImIp3nS	být
křivka	křivka	k1gFnSc1	křivka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
protíná	protínat	k5eAaImIp3nS	protínat
všechny	všechen	k3xTgInPc4	všechen
poledníky	poledník	k1gInPc4	poledník
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
úhlem	úhel	k1gInSc7	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
tento	tento	k3xDgInSc4	tento
úhel	úhel	k1gInSc4	úhel
nulový	nulový	k2eAgInSc1d1	nulový
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
loxodroma	loxodroma	k1gFnSc1	loxodroma
uzavřenou	uzavřený	k2eAgFnSc7d1	uzavřená
kružnicí	kružnice	k1gFnSc7	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Loxodromami	loxodroma	k1gFnPc7	loxodroma
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k8xC	tedy
všechny	všechen	k3xTgMnPc4	všechen
poledníky	poledník	k1gMnPc4	poledník
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
rovnoběžky	rovnoběžka	k1gFnPc4	rovnoběžka
včetně	včetně	k7c2	včetně
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
úhel	úhel	k1gInSc1	úhel
mezi	mezi	k7c7	mezi
loxodromou	loxodroma	k1gFnSc7	loxodroma
a	a	k8xC	a
poledníky	poledník	k1gInPc1	poledník
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
0	[number]	k4	0
<g/>
°	°	k?	°
a	a	k8xC	a
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
loxodroma	loxodroma	k1gFnSc1	loxodroma
spirálou	spirála	k1gFnSc7	spirála
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obtáčí	obtáčet	k5eAaImIp3nS	obtáčet
zeměkouli	zeměkoule	k1gFnSc4	zeměkoule
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
zeměpisných	zeměpisný	k2eAgInPc6d1	zeměpisný
pólech	pól	k1gInPc6	pól
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ortodroma	ortodroma	k1gFnSc1	ortodroma
===	===	k?	===
</s>
</p>
<p>
<s>
Ortodroma	ortodroma	k1gFnSc1	ortodroma
je	být	k5eAaImIp3nS	být
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
spojnice	spojnice	k1gFnSc1	spojnice
dvou	dva	k4xCgNnPc2	dva
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
vedená	vedený	k2eAgFnSc1d1	vedená
po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Ortodroma	ortodroma	k1gFnSc1	ortodroma
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
část	část	k1gFnSc1	část
velké	velký	k2eAgFnSc2d1	velká
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
oběma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
chceme	chtít	k5eAaImIp1nP	chtít
spojit	spojit	k5eAaPmF	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Protíná	protínat	k5eAaImIp3nS	protínat
poledníky	poledník	k1gInPc4	poledník
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
úhly	úhel	k1gInPc7	úhel
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
část	část	k1gFnSc4	část
rovníku	rovník	k1gInSc2	rovník
nebo	nebo	k8xC	nebo
poledníku	poledník	k1gInSc2	poledník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zobrazení	zobrazení	k1gNnSc4	zobrazení
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
povrchu	povrch	k1gInSc2	povrch
–	–	k?	–
mapa	mapa	k1gFnSc1	mapa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Referenční	referenční	k2eAgFnPc1d1	referenční
plochy	plocha	k1gFnPc1	plocha
===	===	k?	===
</s>
</p>
<p>
<s>
Skutečný	skutečný	k2eAgInSc1d1	skutečný
tvar	tvar	k1gInSc1	tvar
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
složitý	složitý	k2eAgInSc1d1	složitý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
svými	svůj	k3xOyFgFnPc7	svůj
deformacemi	deformace	k1gFnPc7	deformace
matematicky	matematicky	k6eAd1	matematicky
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
kartografie	kartografie	k1gFnSc2	kartografie
jej	on	k3xPp3gMnSc4	on
proto	proto	k6eAd1	proto
nahrazujeme	nahrazovat	k5eAaImIp1nP	nahrazovat
referenční	referenční	k2eAgFnSc7d1	referenční
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
referenční	referenční	k2eAgFnSc2d1	referenční
plochy	plocha	k1gFnSc2	plocha
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Její	její	k3xOp3gInSc1	její
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
tvaru	tvar	k1gInSc3	tvar
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Matematicky	matematicky	k6eAd1	matematicky
snadno	snadno	k6eAd1	snadno
definovatelná	definovatelný	k2eAgFnSc1d1	definovatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
Zemi	zem	k1gFnSc4	zem
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
nebo	nebo	k8xC	nebo
lokálně	lokálně	k6eAd1	lokálně
<g/>
.	.	kIx.	.
<g/>
Typy	typ	k1gInPc1	typ
referenčních	referenční	k2eAgFnPc2d1	referenční
ploch	plocha	k1gFnPc2	plocha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
====	====	k?	====
Geoid	geoid	k1gInSc4	geoid
====	====	k?	====
</s>
</p>
<p>
<s>
Nejpřesnější	přesný	k2eAgFnSc1d3	nejpřesnější
aproximace	aproximace	k1gFnSc1	aproximace
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kartografii	kartografie	k1gFnSc6	kartografie
kvůli	kvůli	k7c3	kvůli
těžké	těžký	k2eAgFnSc3d1	těžká
matematické	matematický	k2eAgFnSc3d1	matematická
popsatelnosti	popsatelnost	k1gFnSc3	popsatelnost
většinou	většinou	k6eAd1	většinou
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
jinými	jiný	k2eAgFnPc7d1	jiná
referenčními	referenční	k2eAgFnPc7d1	referenční
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
průběh	průběh	k1gInSc1	průběh
geoidu	geoid	k1gInSc2	geoid
znám	znám	k2eAgMnSc1d1	znám
s	s	k7c7	s
přesností	přesnost	k1gFnSc7	přesnost
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
0,1	[number]	k4	0,1
–	–	k?	–
1	[number]	k4	1
m.	m.	k?	m.
(	(	kIx(	(
<g/>
neustále	neustále	k6eAd1	neustále
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
zpřesňuje	zpřesňovat	k5eAaImIp3nS	zpřesňovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Elipsoid	elipsoid	k1gInSc4	elipsoid
====	====	k?	====
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
tvar	tvar	k1gInSc4	tvar
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
definovatelný	definovatelný	k2eAgInSc1d1	definovatelný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
řada	řada	k1gFnSc1	řada
různých	různý	k2eAgInPc2d1	různý
elipsoidů	elipsoid	k1gInPc2	elipsoid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svými	svůj	k3xOyFgInPc7	svůj
parametry	parametr	k1gInPc7	parametr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Typy	typ	k1gInPc1	typ
elipsoidů	elipsoid	k1gInPc2	elipsoid
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tříosý	tříosý	k2eAgInSc4d1	tříosý
elipsoidVzhledem	elipsoidVzhled	k1gInSc7	elipsoidVzhled
ke	k	k7c3	k
složitější	složitý	k2eAgFnSc3d2	složitější
geometrii	geometrie	k1gFnSc3	geometrie
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rotační	rotační	k2eAgInSc1d1	rotační
elipsoid	elipsoid	k1gInSc1	elipsoid
(	(	kIx(	(
<g/>
dvouosý	dvouosý	k2eAgMnSc1d1	dvouosý
<g/>
)	)	kIx)	)
<g/>
Je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
poloosami	poloosa	k1gFnPc7	poloosa
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b.	b.	k?	b.
Dvouosý	dvouosý	k2eAgInSc4d1	dvouosý
elipsoid	elipsoid	k1gInSc4	elipsoid
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
rotací	rotace	k1gFnSc7	rotace
elipsy	elipsa	k1gFnSc2	elipsa
kolem	kolem	k7c2	kolem
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
poloosy	poloosa	k1gFnSc2	poloosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
aproximace	aproximace	k1gFnSc2	aproximace
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
ho	on	k3xPp3gMnSc4	on
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k8xC	a
<g/>
)	)	kIx)	)
Zemský	zemský	k2eAgInSc1d1	zemský
elipsoid	elipsoid	k1gInSc1	elipsoid
(	(	kIx(	(
<g/>
aproximace	aproximace	k1gFnSc1	aproximace
genoidu	genoid	k1gInSc2	genoid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Střed	střed	k1gInSc1	střed
Zemského	zemský	k2eAgInSc2d1	zemský
elipsoidu	elipsoid	k1gInSc2	elipsoid
je	být	k5eAaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
hmotným	hmotný	k2eAgInSc7d1	hmotný
středem	střed	k1gInSc7	střed
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
geocentrem	geocentr	k1gMnSc7	geocentr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
malá	malý	k2eAgFnSc1d1	malá
poloosa	poloosa	k1gFnSc1	poloosa
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
středem	střed	k1gInSc7	střed
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
b	b	k?	b
<g/>
)	)	kIx)	)
Referenční	referenční	k2eAgInSc1d1	referenční
elipsoid	elipsoid	k1gInSc1	elipsoid
(	(	kIx(	(
<g/>
aproximace	aproximace	k1gFnSc1	aproximace
části	část	k1gFnSc2	část
genoidu	genoid	k1gInSc2	genoid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Střed	střed	k1gInSc1	střed
Referenčního	referenční	k2eAgInSc2d1	referenční
elipsoidu	elipsoid	k1gInSc2	elipsoid
není	být	k5eNaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
se	s	k7c7	s
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vybraném	vybraný	k2eAgNnSc6d1	vybrané
území	území	k1gNnSc6	území
aproximuje	aproximovat	k5eAaBmIp3nS	aproximovat
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
Zemský	zemský	k2eAgInSc1d1	zemský
elipsoid	elipsoid	k1gInSc1	elipsoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
známých	známý	k2eAgInPc2d1	známý
elipsoidů	elipsoid	k1gInPc2	elipsoid
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Koule	koule	k1gFnSc2	koule
====	====	k?	====
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnPc4d2	veliký
odchylky	odchylka	k1gFnPc4	odchylka
od	od	k7c2	od
geoidu	geoid	k1gInSc2	geoid
než	než	k8xS	než
elipsoid	elipsoid	k1gInSc4	elipsoid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
jednodušší	jednoduchý	k2eAgFnSc4d2	jednodušší
matematickou	matematický	k2eAgFnSc4d1	matematická
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
koulí	koulet	k5eAaImIp3nS	koulet
mnohokrát	mnohokrát	k6eAd1	mnohokrát
elipsoid	elipsoid	k1gInSc1	elipsoid
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
se	s	k7c7	s
zachováním	zachování	k1gNnSc7	zachování
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
plochy	plocha	k1gFnSc2	plocha
a	a	k8xC	a
přepočtením	přepočtení	k1gNnSc7	přepočtení
poloměru	poloměr	k1gInSc2	poloměr
podle	podle	k7c2	podle
aritmetického	aritmetický	k2eAgInSc2d1	aritmetický
průměru	průměr	k1gInSc2	průměr
poloos	poloosa	k1gFnPc2	poloosa
elipsoidu	elipsoid	k1gInSc2	elipsoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rovina	rovina	k1gFnSc1	rovina
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
kartografii	kartografie	k1gFnSc6	kartografie
představuje	představovat	k5eAaImIp3nS	představovat
rovina	rovina	k1gFnSc1	rovina
cílovou	cílový	k2eAgFnSc4d1	cílová
plochu	plocha	k1gFnSc4	plocha
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
</s>
</p>
<p>
<s>
zobrazujeme	zobrazovat	k5eAaImIp1nP	zobrazovat
rovinu	rovina	k1gFnSc4	rovina
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tečná	tečný	k2eAgFnSc1d1	tečná
rovina	rovina	k1gFnSc1	rovina
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
malých	malý	k2eAgNnPc2d1	malé
území	území	k1gNnPc2	území
(	(	kIx(	(
<g/>
20	[number]	k4	20
x	x	k?	x
20	[number]	k4	20
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
vznikají	vznikat	k5eAaImIp3nP	vznikat
velké	velký	k2eAgFnPc4d1	velká
výškové	výškový	k2eAgFnPc4d1	výšková
a	a	k8xC	a
polohové	polohový	k2eAgFnPc4d1	polohová
odchylky	odchylka	k1gFnPc4	odchylka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Referenční	referenční	k2eAgFnSc1d1	referenční
rovina	rovina	k1gFnSc1	rovina
nebere	brát	k5eNaImIp3nS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
zakřivení	zakřivení	k1gNnSc2	zakřivení
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Topografická	topografický	k2eAgFnSc1d1	topografická
plocha	plocha	k1gFnSc1	plocha
===	===	k?	===
</s>
</p>
<p>
<s>
Skutečný	skutečný	k2eAgMnSc1d1	skutečný
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
holý	holý	k2eAgInSc1d1	holý
povrch	povrch	k1gInSc1	povrch
Země	zem	k1gFnSc2	zem
bez	bez	k7c2	bez
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
svými	svůj	k3xOyFgFnPc7	svůj
nerovnostmi	nerovnost	k1gFnPc7	nerovnost
včetně	včetně	k7c2	včetně
hladin	hladina	k1gFnPc2	hladina
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
topografickou	topografický	k2eAgFnSc7d1	topografická
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Topografická	topografický	k2eAgFnSc1d1	topografická
situace	situace	k1gFnSc1	situace
===	===	k?	===
</s>
</p>
<p>
<s>
Souhrn	souhrn	k1gInSc1	souhrn
všech	všecek	k3xTgFnPc2	všecek
terénních	terénní	k2eAgFnPc2d1	terénní
útvaru	útvar	k1gInSc3	útvar
na	na	k7c6	na
topografické	topografický	k2eAgFnSc6d1	topografická
ploše	plocha	k1gFnSc6	plocha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
jezera	jezero	k1gNnPc1	jezero
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
sněhové	sněhový	k2eAgFnPc1d1	sněhová
a	a	k8xC	a
ledové	ledový	k2eAgFnPc1d1	ledová
pláne	plán	k1gInSc5	plán
<g/>
,	,	kIx,	,
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgInPc2	všecek
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
silnice	silnice	k1gFnPc1	silnice
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnPc1	železnice
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
topografickou	topografický	k2eAgFnSc7d1	topografická
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mapa	mapa	k1gFnSc1	mapa
===	===	k?	===
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
je	být	k5eAaImIp3nS	být
zmenšený	zmenšený	k2eAgInSc1d1	zmenšený
rovinný	rovinný	k2eAgInSc1d1	rovinný
geometrický	geometrický	k2eAgInSc1d1	geometrický
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
získaný	získaný	k2eAgMnSc1d1	získaný
některou	některý	k3yIgFnSc7	některý
zobrazovací	zobrazovací	k2eAgFnSc7d1	zobrazovací
metodou	metoda	k1gFnSc7	metoda
-	-	kIx~	-
projekcí	projekce	k1gFnSc7	projekce
celého	celý	k2eAgNnSc2d1	celé
zemského	zemský	k2eAgNnSc2d1	zemské
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
jeho	jeho	k3xOp3gFnPc2	jeho
částí	část	k1gFnPc2	část
-	-	kIx~	-
generalizací	generalizace	k1gFnPc2	generalizace
horizontální	horizontální	k2eAgFnSc2d1	horizontální
i	i	k8xC	i
vertikální	vertikální	k2eAgFnSc2d1	vertikální
členitosti	členitost	k1gFnSc2	členitost
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
s	s	k7c7	s
vyznačením	vyznačení	k1gNnSc7	vyznačení
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
průběhu	průběh	k1gInSc2	průběh
komunikací	komunikace	k1gFnPc2	komunikace
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
technických	technický	k2eAgNnPc2d1	technické
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
osídlení	osídlení	k1gNnSc2	osídlení
<g/>
,	,	kIx,	,
domluvenými	domluvený	k2eAgFnPc7d1	domluvená
značkami	značka	k1gFnPc7	značka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Druhy	druh	k1gInPc4	druh
map	mapa	k1gFnPc2	mapa
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
měřítko	měřítko	k1gNnSc1	měřítko
====	====	k?	====
</s>
</p>
<p>
<s>
Do	do	k7c2	do
mapového	mapový	k2eAgInSc2d1	mapový
obrazu	obraz	k1gInSc2	obraz
jsou	být	k5eAaImIp3nP	být
zahrnuté	zahrnutý	k2eAgInPc1d1	zahrnutý
jen	jen	k9	jen
jevy	jev	k1gInPc7	jev
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
mapy	mapa	k1gFnSc2	mapa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zobrazitelné	zobrazitelný	k2eAgMnPc4d1	zobrazitelný
ve	v	k7c6	v
stanoveném	stanovený	k2eAgNnSc6d1	stanovené
zmenšení	zmenšení	k1gNnSc6	zmenšení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kolmý	kolmý	k2eAgInSc4d1	kolmý
průměr	průměr	k1gInSc4	průměr
obrazu	obraz	k1gInSc2	obraz
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
horizontální	horizontální	k2eAgFnSc2d1	horizontální
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
tento	tento	k3xDgInSc4	tento
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
mapy	mapa	k1gFnSc2	mapa
nazýváme	nazývat	k5eAaImIp1nP	nazývat
plánem	plán	k1gInSc7	plán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
se	s	k7c7	s
zkreslením	zkreslení	k1gNnSc7	zkreslení
rychle	rychle	k6eAd1	rychle
rostoucím	rostoucí	k2eAgNnSc7d1	rostoucí
od	od	k7c2	od
jeho	on	k3xPp3gInSc2	on
středu	střed	k1gInSc2	střed
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
omezuje	omezovat	k5eAaImIp3nS	omezovat
jen	jen	k9	jen
na	na	k7c4	na
malé	malý	k2eAgNnSc4d1	malé
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obsáhlý	obsáhlý	k2eAgInSc1d1	obsáhlý
soubor	soubor	k1gInSc1	soubor
map	mapa	k1gFnPc2	mapa
vybudovaný	vybudovaný	k2eAgMnSc1d1	vybudovaný
na	na	k7c6	na
základě	základ	k1gInSc6	základ
různých	různý	k2eAgInPc2d1	různý
územních	územní	k2eAgInPc2d1	územní
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
potřeb	potřeba	k1gFnPc2	potřeba
kulturně	kulturně	k6eAd1	kulturně
vyspělého	vyspělý	k2eAgInSc2d1	vyspělý
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
roztřídit	roztřídit	k5eAaPmF	roztřídit
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
:	:	kIx,	:
podle	podle	k7c2	podle
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
a	a	k8xC	a
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
měřítka	měřítko	k1gNnSc2	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
podkladě	podklad	k1gInSc6	podklad
celkového	celkový	k2eAgNnSc2d1	celkové
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
částečného	částečný	k2eAgNnSc2d1	částečné
měření	měření	k1gNnSc2	měření
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
je	být	k5eAaImIp3nS	být
mapa	mapa	k1gFnSc1	mapa
původní	původní	k2eAgFnSc1d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
mapa	mapa	k1gFnSc1	mapa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
bez	bez	k7c2	bez
místního	místní	k2eAgNnSc2d1	místní
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
a	a	k8xC	a
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
mapa	mapa	k1gFnSc1	mapa
odvozená	odvozený	k2eAgFnSc1d1	odvozená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účel	účel	k1gInSc1	účel
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
mapy	mapa	k1gFnSc2	mapa
spolu	spolu	k6eAd1	spolu
úzce	úzko	k6eAd1	úzko
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Mapy	mapa	k1gFnPc1	mapa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
jsou	být	k5eAaImIp3nP	být
předměty	předmět	k1gInPc1	předmět
a	a	k8xC	a
jevy	jev	k1gInPc1	jev
volené	volený	k2eAgInPc1d1	volený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyhověly	vyhovět	k5eAaPmAgFnP	vyhovět
většině	většina	k1gFnSc6	většina
uživatelů	uživatel	k1gMnPc2	uživatel
map	mapa	k1gFnPc2	mapa
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
přiměřené	přiměřený	k2eAgNnSc1d1	přiměřené
měřítku	měřítko	k1gNnSc3	měřítko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
od	od	k7c2	od
map	mapa	k1gFnPc2	mapa
účelových	účelový	k2eAgFnPc2d1	účelová
(	(	kIx(	(
<g/>
tematických	tematický	k2eAgFnPc2d1	tematická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
náplň	náplň	k1gFnSc1	náplň
je	být	k5eAaImIp3nS	být
usměrněná	usměrněný	k2eAgFnSc1d1	usměrněná
podle	podle	k7c2	podle
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
účelu	účel	k1gInSc2	účel
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
mají	mít	k5eAaImIp3nP	mít
sloužit	sloužit	k5eAaImF	sloužit
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
mapy	mapa	k1gFnPc1	mapa
letecké	letecký	k2eAgFnPc1d1	letecká
ale	ale	k8xC	ale
i	i	k9	i
geologické	geologický	k2eAgFnSc2d1	geologická
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgFnSc2d1	cestovní
<g/>
,	,	kIx,	,
lesnické	lesnický	k2eAgFnSc2d1	lesnická
<g/>
,	,	kIx,	,
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
<g/>
,	,	kIx,	,
geofyzikální	geofyzikální	k2eAgFnSc2d1	geofyzikální
a	a	k8xC	a
jiné	jiný	k2eAgFnSc2d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měřítko	měřítko	k1gNnSc1	měřítko
mapy	mapa	k1gFnSc2	mapa
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
poměru	poměr	k1gInSc6	poměr
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
zmenšená	zmenšený	k2eAgFnSc1d1	zmenšená
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
část	část	k1gFnSc1	část
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Měřítko	měřítko	k1gNnSc1	měřítko
nám	my	k3xPp1nPc3	my
udává	udávat	k5eAaImIp3nS	udávat
poměr	poměr	k1gInSc1	poměr
délky	délka	k1gFnSc2	délka
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
zobrazované	zobrazovaný	k2eAgFnSc6d1	zobrazovaná
(	(	kIx(	(
<g/>
mapě	mapa	k1gFnSc6	mapa
<g/>
)	)	kIx)	)
ke	k	k7c3	k
stejné	stejný	k2eAgFnSc3d1	stejná
délce	délka	k1gFnSc3	délka
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
originální	originální	k2eAgFnPc1d1	originální
(	(	kIx(	(
<g/>
referenční	referenční	k2eAgFnPc1d1	referenční
koule	koule	k1gFnPc1	koule
<g/>
,	,	kIx,	,
elipsoid	elipsoid	k1gInSc1	elipsoid
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měřítko	měřítko	k1gNnSc4	měřítko
mapy	mapa	k1gFnSc2	mapa
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
zlomkem	zlomek	k1gInSc7	zlomek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
M	M	kA	M
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
́	́	k?	́
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M	M	kA	M
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
=	=	kIx~	=
<g/>
s	s	k7c7	s
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
acute	acute	k5eAaPmIp2nP	acute
{	{	kIx(	{
<g/>
}}	}}	k?	}}
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
m	m	kA	m
-	-	kIx~	-
měřítkové	měřítkový	k2eAgNnSc1d1	měřítkové
číslo	číslo	k1gNnSc1	číslo
-	-	kIx~	-
součinitel	součinitel	k1gInSc1	součinitel
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
násobit	násobit	k5eAaImF	násobit
odměřenou	odměřený	k2eAgFnSc4d1	odměřená
délku	délka	k1gFnSc4	délka
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
skutečné	skutečný	k2eAgFnSc2d1	skutečná
délky	délka	k1gFnSc2	délka
</s>
</p>
<p>
<s>
s	s	k7c7	s
<g/>
́	́	k?	́
-	-	kIx~	-
délka	délka	k1gFnSc1	délka
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
</s>
</p>
<p>
<s>
s	s	k7c7	s
-	-	kIx~	-
délka	délka	k1gFnSc1	délka
ve	v	k7c4	v
skutečnostiMěřítko	skutečnostiMěřítko	k1gNnSc4	skutečnostiMěřítko
mapy	mapa	k1gFnSc2	mapa
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
PoměremJe	PoměremJat	k5eAaPmIp3nS	PoměremJat
to	ten	k3xDgNnSc1	ten
nejčastěji	často	k6eAd3	často
používaný	používaný	k2eAgInSc1d1	používaný
způsob	způsob	k1gInSc1	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Měřítko	měřítko	k1gNnSc1	měřítko
mapy	mapa	k1gFnSc2	mapa
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
100	[number]	k4	100
000	[number]	k4	000
je	být	k5eAaImIp3nS	být
poměr	poměr	k1gInSc1	poměr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nám	my	k3xPp1nPc3	my
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
ke	k	k7c3	k
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
1	[number]	k4	1
cm	cm	kA	cm
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
představuje	představovat	k5eAaImIp3nS	představovat
tedy	tedy	k9	tedy
100	[number]	k4	100
000	[number]	k4	000
cm	cm	kA	cm
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
km	km	kA	km
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GrafickyMěřítko	GrafickyMěřítko	k1gNnSc1	GrafickyMěřítko
bývá	bývat	k5eAaImIp3nS	bývat
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
zpravidla	zpravidla	k6eAd1	zpravidla
vyjádřeno	vyjádřen	k2eAgNnSc1d1	vyjádřeno
také	také	k9	také
graficky	graficky	k6eAd1	graficky
<g/>
.	.	kIx.	.
</s>
<s>
Úsečky	úsečka	k1gFnPc1	úsečka
grafického	grafický	k2eAgNnSc2d1	grafické
měřítka	měřítko	k1gNnSc2	měřítko
představují	představovat	k5eAaImIp3nP	představovat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
u	u	k7c2	u
těchto	tento	k3xDgFnPc2	tento
úseček	úsečka	k1gFnPc2	úsečka
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PřirovnánímNa	PřirovnánímNa	k6eAd1	PřirovnánímNa
některých	některý	k3yIgFnPc6	některý
mapách	mapa	k1gFnPc6	mapa
bývá	bývat	k5eAaImIp3nS	bývat
měřítko	měřítko	k1gNnSc1	měřítko
vyjádřeno	vyjádřen	k2eAgNnSc1d1	vyjádřeno
přesným	přesný	k2eAgNnSc7d1	přesné
přirovnáním	přirovnání	k1gNnSc7	přirovnání
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
k	k	k7c3	k
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
1	[number]	k4	1
cm	cm	kA	cm
=	=	kIx~	=
5	[number]	k4	5
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Rozdělení	rozdělení	k1gNnSc1	rozdělení
map	mapa	k1gFnPc2	mapa
====	====	k?	====
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
významné	významný	k2eAgNnSc4d1	významné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
činitelem	činitel	k1gInSc7	činitel
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc6	hospodářství
<g/>
,	,	kIx,	,
plánování	plánování	k1gNnSc6	plánování
<g/>
,	,	kIx,	,
statistice	statistika	k1gFnSc6	statistika
<g/>
,	,	kIx,	,
výstavbě	výstavba	k1gFnSc6	výstavba
<g/>
,	,	kIx,	,
obchodě	obchod	k1gInSc6	obchod
<g/>
,	,	kIx,	,
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
turistice	turistika	k1gFnSc6	turistika
<g/>
,	,	kIx,	,
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
státu	stát	k1gInSc2	stát
a	a	k8xC	a
při	při	k7c6	při
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgNnPc2	tento
kritérií	kritérion	k1gNnPc2	kritérion
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
rozdělovat	rozdělovat	k5eAaImF	rozdělovat
mapy	mapa	k1gFnPc4	mapa
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
<g/>
,	,	kIx,	,
jaký	jaký	k9	jaký
mapy	mapa	k1gFnPc1	mapa
plní	plnit	k5eAaImIp3nP	plnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dopravy	doprava	k1gFnSc2	doprava
můžeme	moct	k5eAaImIp1nP	moct
členit	členit	k5eAaImF	členit
mapy	mapa	k1gFnPc4	mapa
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
<g/>
,	,	kIx,	,
silniční	silniční	k2eAgFnSc6d1	silniční
<g/>
,	,	kIx,	,
letecké	letecký	k2eAgFnSc6d1	letecká
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc6d1	vodní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
geometrické	geometrický	k2eAgFnSc2d1	geometrická
přesnosti	přesnost	k1gFnSc2	přesnost
a	a	k8xC	a
stupně	stupeň	k1gInSc2	stupeň
generalizace	generalizace	k1gFnSc2	generalizace
mapového	mapový	k2eAgInSc2d1	mapový
obsahu	obsah	k1gInSc2	obsah
je	být	k5eAaImIp3nS	být
nejobvyklejším	obvyklý	k2eAgNnSc7d3	nejobvyklejší
rozdělením	rozdělení	k1gNnSc7	rozdělení
map	mapa	k1gFnPc2	mapa
podle	podle	k7c2	podle
měřítka	měřítko	k1gNnSc2	měřítko
<g/>
.	.	kIx.	.
</s>
<s>
Měřítko	měřítko	k1gNnSc1	měřítko
už	už	k6eAd1	už
udává	udávat	k5eAaImIp3nS	udávat
uživateli	uživatel	k1gMnSc3	uživatel
bez	bez	k7c2	bez
podrobnějšího	podrobný	k2eAgNnSc2d2	podrobnější
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc1	jaký
asi	asi	k9	asi
možnosti	možnost	k1gFnSc3	možnost
mapa	mapa	k1gFnSc1	mapa
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
v	v	k7c6	v
informativním	informativní	k2eAgNnSc6d1	informativní
i	i	k8xC	i
konstrukčním	konstrukční	k2eAgNnSc6d1	konstrukční
použití	použití	k1gNnSc6	použití
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
usuzovat	usuzovat	k5eAaImF	usuzovat
i	i	k8xC	i
původ	původ	k1gInSc4	původ
mapy	mapa	k1gFnSc2	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kartografické	kartografický	k2eAgFnPc1d1	kartografická
projekce	projekce	k1gFnPc1	projekce
==	==	k?	==
</s>
</p>
<p>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
síť	síť	k1gFnSc1	síť
referenčního	referenční	k2eAgInSc2d1	referenční
elipsoidu	elipsoid	k1gInSc2	elipsoid
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
referenční	referenční	k2eAgFnSc1d1	referenční
koule	koule	k1gFnSc1	koule
promítnutá	promítnutý	k2eAgFnSc1d1	promítnutá
anebo	anebo	k8xC	anebo
zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
<g/>
,	,	kIx,	,
válec	válec	k1gInSc4	válec
anebo	anebo	k8xC	anebo
kužel	kužel	k1gInSc4	kužel
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
na	na	k7c6	na
zobrazovacích	zobrazovací	k2eAgFnPc6d1	zobrazovací
plochách	plocha	k1gFnPc6	plocha
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
obrazy	obraz	k1gInPc1	obraz
geografické	geografický	k2eAgFnSc2d1	geografická
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
projekcí	projekce	k1gFnPc2	projekce
je	být	k5eAaImIp3nS	být
účelné	účelný	k2eAgNnSc1d1	účelné
a	a	k8xC	a
potřebné	potřebný	k2eAgNnSc1d1	potřebné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
metody	metoda	k1gFnPc1	metoda
roztřídily	roztřídit	k5eAaPmAgFnP	roztřídit
-	-	kIx~	-
klasifikovaly	klasifikovat	k5eAaImAgFnP	klasifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Roztřídit	roztřídit	k5eAaPmF	roztřídit
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
podle	podle	k7c2	podle
různých	různý	k2eAgFnPc2d1	různá
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
je	on	k3xPp3gInPc4	on
můžeme	moct	k5eAaImIp1nP	moct
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
podle	podle	k7c2	podle
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zobrazovacích	zobrazovací	k2eAgFnPc2d1	zobrazovací
ploch	plocha	k1gFnPc2	plocha
</s>
</p>
<p>
<s>
zkreslení	zkreslení	k1gNnSc1	zkreslení
</s>
</p>
<p>
<s>
===	===	k?	===
Klasifikace	klasifikace	k1gFnPc1	klasifikace
kartografických	kartografický	k2eAgFnPc2d1	kartografická
projekcí	projekce	k1gFnPc2	projekce
podle	podle	k7c2	podle
zobrazovacích	zobrazovací	k2eAgFnPc2d1	zobrazovací
ploch	plocha	k1gFnPc2	plocha
===	===	k?	===
</s>
</p>
<p>
<s>
Projekce	projekce	k1gFnPc4	projekce
třídíme	třídit	k5eAaImIp1nP	třídit
na	na	k7c6	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pravé	pravý	k2eAgNnSc1d1	pravé
</s>
</p>
<p>
<s>
nepravé	pravý	k2eNgFnPc4d1	nepravá
(	(	kIx(	(
<g/>
konvencionální	konvencionální	k2eAgFnPc4d1	konvencionální
<g/>
)	)	kIx)	)
<g/>
Pravé	pravý	k2eAgFnPc4d1	pravá
projekce	projekce	k1gFnPc4	projekce
podle	podle	k7c2	podle
zobrazovacích	zobrazovací	k2eAgFnPc2d1	zobrazovací
ploch	plocha	k1gFnPc2	plocha
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
a	a	k8xC	a
<g/>
)	)	kIx)	)
azimutální	azimutální	k2eAgFnSc1d1	azimutální
</s>
</p>
<p>
<s>
b	b	k?	b
<g/>
)	)	kIx)	)
kuželové	kuželový	k2eAgFnSc2d1	kuželová
(	(	kIx(	(
<g/>
kónické	kónický	k2eAgFnSc2d1	kónická
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
c	c	k0	c
<g/>
)	)	kIx)	)
válcové	válcový	k2eAgMnPc4d1	válcový
(	(	kIx(	(
<g/>
cylindrické	cylindrický	k2eAgMnPc4d1	cylindrický
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
d	d	k?	d
<g/>
)	)	kIx)	)
kulové	kulový	k2eAgInPc1d1	kulový
(	(	kIx(	(
<g/>
globus	globus	k1gInSc1	globus
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Pravé	pravý	k2eAgFnPc1d1	pravá
projekce	projekce	k1gFnPc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Pravé	pravý	k2eAgFnPc1d1	pravá
projekce	projekce	k1gFnPc1	projekce
v	v	k7c6	v
normální	normální	k2eAgFnSc6d1	normální
poloze	poloha	k1gFnSc6	poloha
jsou	být	k5eAaImIp3nP	být
geometricky	geometricky	k6eAd1	geometricky
jednoduše	jednoduše	k6eAd1	jednoduše
definované	definovaný	k2eAgFnPc1d1	definovaná
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
jsou	být	k5eAaImIp3nP	být
funkce	funkce	k1gFnPc1	funkce
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
šířek	šířka	k1gFnPc2	šířka
a	a	k8xC	a
obrazy	obraz	k1gInPc4	obraz
poledníků	poledník	k1gInPc2	poledník
jsou	být	k5eAaImIp3nP	být
funkcí	funkce	k1gFnSc7	funkce
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Pravé	pravý	k2eAgFnPc1d1	pravá
projekce	projekce	k1gFnPc1	projekce
můžeme	moct	k5eAaImIp1nP	moct
dále	daleko	k6eAd2	daleko
třídit	třídit	k5eAaImF	třídit
na	na	k7c4	na
</s>
</p>
<p>
<s>
perspektivníPerspektivní	perspektivníPerspektivní	k2eAgFnPc1d1	perspektivníPerspektivní
projekce	projekce	k1gFnPc1	projekce
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
obrazy	obraz	k1gInPc4	obraz
geografické	geografický	k2eAgFnSc2d1	geografická
sítě	síť	k1gFnSc2	síť
získáme	získat	k5eAaPmIp1nP	získat
na	na	k7c6	na
zobrazovacích	zobrazovací	k2eAgFnPc6d1	zobrazovací
plochách	plocha	k1gFnPc6	plocha
promítnutím	promítnutí	k1gNnPc3	promítnutí
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
neperspektivníNeperspektivní	perspektivníNeperspektivní	k2eNgFnPc1d1	perspektivníNeperspektivní
projekce	projekce	k1gFnPc1	projekce
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
obrazy	obraz	k1gInPc4	obraz
geografické	geografický	k2eAgFnSc2d1	geografická
sítě	síť	k1gFnSc2	síť
nemůžeme	moct	k5eNaImIp1nP	moct
získat	získat	k5eAaPmF	získat
žádným	žádný	k3yNgNnSc7	žádný
promítnutím	promítnutí	k1gNnSc7	promítnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konstruujeme	konstruovat	k5eAaImIp1nP	konstruovat
je	on	k3xPp3gNnSc4	on
na	na	k7c6	na
zobrazovacích	zobrazovací	k2eAgFnPc6d1	zobrazovací
plochách	plocha	k1gFnPc6	plocha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zvolených	zvolený	k2eAgFnPc2d1	zvolená
geometrických	geometrický	k2eAgFnPc2d1	geometrická
a	a	k8xC	a
matematických	matematický	k2eAgFnPc2d1	matematická
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nepravé	pravý	k2eNgFnPc1d1	nepravá
projekce	projekce	k1gFnPc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Nepravé	pravý	k2eNgFnPc1d1	nepravá
projekce	projekce	k1gFnPc1	projekce
jsou	být	k5eAaImIp3nP	být
taková	takový	k3xDgNnPc4	takový
zobrazení	zobrazení	k1gNnPc4	zobrazení
v	v	k7c6	v
poloze	poloha	k1gFnSc6	poloha
normální	normální	k2eAgFnSc6d1	normální
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
obrazy	obraz	k1gInPc7	obraz
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
jsou	být	k5eAaImIp3nP	být
funkcí	funkce	k1gFnSc7	funkce
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
šířek	šířka	k1gFnPc2	šířka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
při	při	k7c6	při
pravých	pravý	k2eAgNnPc6d1	pravé
zobrazeních	zobrazení	k1gNnPc6	zobrazení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obrazy	obraz	k1gInPc1	obraz
poledníků	poledník	k1gMnPc2	poledník
jsou	být	k5eAaImIp3nP	být
funkcí	funkce	k1gFnSc7	funkce
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
šířek	šířka	k1gFnPc2	šířka
a	a	k8xC	a
délek	délka	k1gFnPc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
tedy	tedy	k9	tedy
vzhled	vzhled	k1gInSc4	vzhled
jako	jako	k8xS	jako
u	u	k7c2	u
pravých	pravý	k2eAgFnPc2d1	pravá
projekcí	projekce	k1gFnPc2	projekce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obrazy	obraz	k1gInPc1	obraz
poledníků	poledník	k1gMnPc2	poledník
budou	být	k5eAaImBp3nP	být
čáry	čára	k1gFnPc1	čára
vyššího	vysoký	k2eAgInSc2d2	vyšší
stupně	stupeň	k1gInSc2	stupeň
(	(	kIx(	(
<g/>
elipsy	elipsa	k1gFnSc2	elipsa
<g/>
,	,	kIx,	,
sinusoidy	sinusoida	k1gFnSc2	sinusoida
<g/>
,	,	kIx,	,
křivé	křivý	k2eAgFnSc2d1	křivá
čáry	čára	k1gFnSc2	čára
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
jen	jen	k9	jen
v	v	k7c6	v
kartografické	kartografický	k2eAgFnSc6d1	kartografická
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnPc1	klasifikace
kartografických	kartografický	k2eAgFnPc2d1	kartografická
projekcí	projekce	k1gFnPc2	projekce
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
plochy	plocha	k1gFnSc2	plocha
a	a	k8xC	a
středu	střed	k1gInSc2	střed
promítání	promítání	k1gNnSc2	promítání
==	==	k?	==
</s>
</p>
<p>
<s>
Dělení	dělení	k1gNnSc4	dělení
projekcí	projekce	k1gFnPc2	projekce
podle	podle	k7c2	podle
polohy	poloha	k1gFnSc2	poloha
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
plochy	plocha	k1gFnSc2	plocha
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
při	při	k7c6	při
promítání	promítání	k1gNnSc6	promítání
na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
a	a	k8xC	a
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
zobrazovací	zobrazovací	k2eAgFnPc4d1	zobrazovací
plochy	plocha	k1gFnPc4	plocha
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
roviny	rovina	k1gFnSc2	rovina
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
následující	následující	k2eAgFnPc4d1	následující
polohy	poloha	k1gFnPc4	poloha
</s>
</p>
<p>
<s>
pólová	pólový	k2eAgFnSc1d1	pólová
</s>
</p>
<p>
<s>
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
(	(	kIx(	(
<g/>
šikmá	šikmý	k2eAgFnSc1d1	šikmá
<g/>
,	,	kIx,	,
horizontální	horizontální	k2eAgFnSc1d1	horizontální
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rovníková	rovníkový	k2eAgNnPc4d1	rovníkové
(	(	kIx(	(
<g/>
ekvatoriální	ekvatoriální	k2eAgNnPc4d1	ekvatoriální
<g/>
,	,	kIx,	,
příčná	příčný	k2eAgNnPc4d1	příčné
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
kuželu	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
válce	válka	k1gFnSc6	válka
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
polohy	poloha	k1gFnPc4	poloha
</s>
</p>
<p>
<s>
normální	normální	k2eAgMnSc1d1	normální
</s>
</p>
<p>
<s>
příčná	příčný	k2eAgFnSc1d1	příčná
(	(	kIx(	(
<g/>
transverzální	transverzální	k2eAgFnSc1d1	transverzální
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
šikmá	šikmý	k2eAgNnPc4d1	šikmé
(	(	kIx(	(
<g/>
všeobecná	všeobecný	k2eAgNnPc4d1	všeobecné
<g/>
)	)	kIx)	)
<g/>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
polohu	poloha	k1gFnSc4	poloha
středu	střed	k1gInSc2	střed
promítání	promítání	k1gNnPc2	promítání
třídíme	třídit	k5eAaImIp1nP	třídit
perspektivní	perspektivní	k2eAgFnPc4d1	perspektivní
projekce	projekce	k1gFnPc4	projekce
na	na	k7c6	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
gnómonické	gnómonický	k2eAgFnPc1d1	gnómonický
(	(	kIx(	(
<g/>
střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
stereografické	stereografický	k2eAgFnPc1d1	stereografická
(	(	kIx(	(
<g/>
střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
Země	zem	k1gFnSc2	zem
oproti	oproti	k7c3	oproti
bodu	bod	k1gInSc3	bod
dotyku	dotyk	k1gInSc2	dotyk
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
roviny	rovina	k1gFnSc2	rovina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
externí	externí	k2eAgInSc4d1	externí
(	(	kIx(	(
<g/>
střed	střed	k1gInSc4	střed
promítání	promítání	k1gNnSc2	promítání
mimo	mimo	k7c4	mimo
Zemi	zem	k1gFnSc4	zem
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ortografické	ortografický	k2eAgFnPc1d1	ortografická
(	(	kIx(	(
<g/>
střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
v	v	k7c6	v
nekonečnu	nekonečno	k1gNnSc6	nekonečno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Klasifikace	klasifikace	k1gFnPc1	klasifikace
kartografických	kartografický	k2eAgFnPc2d1	kartografická
projekcí	projekce	k1gFnPc2	projekce
podle	podle	k7c2	podle
zkreslení	zkreslení	k1gNnSc2	zkreslení
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zobrazování	zobrazování	k1gNnSc6	zobrazování
referenční	referenční	k2eAgFnSc2d1	referenční
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
elipsoidu	elipsoid	k1gInSc2	elipsoid
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
na	na	k7c4	na
zobrazovací	zobrazovací	k2eAgFnSc4d1	zobrazovací
plochu	plocha	k1gFnSc4	plocha
(	(	kIx(	(
<g/>
rovina	rovina	k1gFnSc1	rovina
<g/>
,	,	kIx,	,
kužel	kužel	k1gInSc1	kužel
<g/>
,	,	kIx,	,
válec	válec	k1gInSc1	válec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
ke	k	k7c3	k
zkreslení	zkreslení	k1gNnSc3	zkreslení
některých	některý	k3yIgInPc2	některý
zobrazovaných	zobrazovaný	k2eAgInPc2d1	zobrazovaný
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
zkreslení	zkreslení	k1gNnPc2	zkreslení
dělíme	dělit	k5eAaImIp1nP	dělit
kartografické	kartografický	k2eAgFnPc4d1	kartografická
projekce	projekce	k1gFnPc4	projekce
na	na	k7c6	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
délkojevné	délkojevný	k2eAgInPc4d1	délkojevný
(	(	kIx(	(
<g/>
ekvidistanční	ekvidistanční	k2eAgInPc4d1	ekvidistanční
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
úhlojevné	úhlojevný	k2eAgInPc4d1	úhlojevný
(	(	kIx(	(
<g/>
konformní	konformní	k2eAgInPc4d1	konformní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
plochojevné	plochojevný	k2eAgInPc4d1	plochojevný
(	(	kIx(	(
<g/>
ekvivalentní	ekvivalentní	k2eAgInPc4d1	ekvivalentní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tvarojevné	tvarojevný	k2eAgFnPc4d1	tvarojevný
(	(	kIx(	(
<g/>
ortomorfní	ortomorfní	k2eAgFnPc4d1	ortomorfní
<g/>
,	,	kIx,	,
nedají	dát	k5eNaPmIp3nP	dát
se	se	k3xPyFc4	se
matematicky	matematicky	k6eAd1	matematicky
definovat	definovat	k5eAaBmF	definovat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vyrovnávací	vyrovnávací	k2eAgFnSc4d1	vyrovnávací
(	(	kIx(	(
<g/>
kompenzační	kompenzační	k2eAgFnSc4d1	kompenzační
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Azimutální	azimutální	k2eAgFnPc1d1	azimutální
projekce	projekce	k1gFnPc1	projekce
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
azimutálních	azimutální	k2eAgFnPc6d1	azimutální
projekcích	projekce	k1gFnPc6	projekce
je	být	k5eAaImIp3nS	být
zobrazovací	zobrazovací	k2eAgFnPc4d1	zobrazovací
plochou	plochý	k2eAgFnSc4d1	plochá
rovina	rovina	k1gFnSc1	rovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pólová	pólový	k2eAgFnSc1d1	pólová
gnomonická	gnomonický	k2eAgFnSc1d1	gnomonický
projekce	projekce	k1gFnSc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
ortodromy	ortodroma	k1gFnPc1	ortodroma
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nP	jevit
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
této	tento	k3xDgFnSc2	tento
projekce	projekce	k1gFnSc2	projekce
jako	jako	k8xC	jako
přímky	přímka	k1gFnSc2	přímka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Ortodroma	ortodroma	k1gFnSc1	ortodroma
je	být	k5eAaImIp3nS	být
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
rovina	rovina	k1gFnSc1	rovina
prochází	procházet	k5eAaImIp3nS	procházet
středem	střed	k1gInSc7	střed
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
totožný	totožný	k2eAgInSc1d1	totožný
s	s	k7c7	s
rovinou	rovina	k1gFnSc7	rovina
ortodromy	ortodroma	k1gFnSc2	ortodroma
<g/>
,	,	kIx,	,
nutně	nutně	k6eAd1	nutně
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
na	na	k7c6	na
gnómonických	gnómonický	k2eAgFnPc6d1	gnómonický
mapách	mapa	k1gFnPc6	mapa
jako	jako	k8xS	jako
přímka	přímka	k1gFnSc1	přímka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrazy	obraz	k1gInPc1	obraz
poledníků	poledník	k1gMnPc2	poledník
jsou	být	k5eAaImIp3nP	být
přímky	přímka	k1gFnPc1	přímka
sbíhající	sbíhající	k2eAgFnPc1d1	sbíhající
se	se	k3xPyFc4	se
v	v	k7c6	v
bodu	bod	k1gInSc6	bod
dotyku	dotyk	k1gInSc2	dotyk
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
jsou	být	k5eAaImIp3nP	být
koncentrické	koncentrický	k2eAgFnPc1d1	koncentrická
kružnice	kružnice	k1gFnPc1	kružnice
se	se	k3xPyFc4	se
středem	střed	k1gInSc7	střed
v	v	k7c6	v
pólu	pólo	k1gNnSc6	pólo
(	(	kIx(	(
<g/>
bod	bod	k1gInSc1	bod
dotyku	dotyk	k1gInSc2	dotyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pólová	pólový	k2eAgFnSc1d1	pólová
gnomonická	gnomonický	k2eAgFnSc1d1	gnomonický
projekce	projekce	k1gFnSc1	projekce
je	být	k5eAaImIp3nS	být
projekce	projekce	k1gFnSc1	projekce
ortodromická	ortodromický	k2eAgFnSc1d1	ortodromický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ortodroma	ortodroma	k1gFnSc1	ortodroma
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jako	jako	k9	jako
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
zakreslení	zakreslení	k1gNnSc6	zakreslení
průběhu	průběh	k1gInSc6	průběh
ortodromy	ortodroma	k1gFnPc1	ortodroma
do	do	k7c2	do
map	mapa	k1gFnPc2	mapa
jiných	jiný	k2eAgFnPc2d1	jiná
projekcí	projekce	k1gFnPc2	projekce
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zeměpisné	zeměpisný	k2eAgFnPc4d1	zeměpisná
souřadnice	souřadnice	k1gFnPc4	souřadnice
podrobných	podrobný	k2eAgInPc2d1	podrobný
bodů	bod	k1gInPc2	bod
ortodromy	ortodroma	k1gFnSc2	ortodroma
odčítáme	odčítat	k5eAaImIp1nP	odčítat
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
v	v	k7c6	v
gnómonické	gnómonický	k2eAgFnSc6d1	gnómonický
projekci	projekce	k1gFnSc6	projekce
a	a	k8xC	a
vyneseme	vynést	k5eAaPmIp1nP	vynést
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
mapy	mapa	k1gFnSc2	mapa
jiné	jiný	k2eAgFnSc2d1	jiná
projekce	projekce	k1gFnSc2	projekce
<g/>
.	.	kIx.	.
</s>
<s>
Spojením	spojení	k1gNnSc7	spojení
těchto	tento	k3xDgInPc2	tento
bodů	bod	k1gInPc2	bod
dostáváme	dostávat	k5eAaImIp1nP	dostávat
průběh	průběh	k1gInSc4	průběh
ortodromy	ortodroma	k1gFnSc2	ortodroma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ε	ε	k?	ε
=	=	kIx~	=
λ	λ	k?	λ
-	-	kIx~	-
zobrazení	zobrazení	k1gNnSc1	zobrazení
poledníků	poledník	k1gInPc2	poledník
</s>
</p>
<p>
<s>
r	r	kA	r
=	=	kIx~	=
R.	R.	kA	R.
<g/>
cotgφ	cotgφ	k?	cotgφ
-	-	kIx~	-
zobrazení	zobrazení	k1gNnSc1	zobrazení
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
</s>
</p>
<p>
<s>
====	====	k?	====
Stereografická	Stereografický	k2eAgFnSc1d1	Stereografická
polární	polární	k2eAgFnSc1d1	polární
projekce	projekce	k1gFnSc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Střed	střed	k1gInSc1	střed
promítání	promítání	k1gNnSc2	promítání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
pólu	pól	k1gInSc6	pól
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
pól	pól	k1gInSc1	pól
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
Země	země	k1gFnSc1	země
zobrazovací	zobrazovací	k2eAgFnSc1d1	zobrazovací
rovina	rovina	k1gFnSc1	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
od	od	k7c2	od
zobrazovací	zobrazovací	k2eAgFnSc2d1	zobrazovací
roviny	rovina	k1gFnSc2	rovina
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
2	[number]	k4	2
<g/>
R.	R.	kA	R.
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
projekce	projekce	k1gFnPc4	projekce
pravé	pravý	k2eAgFnPc4d1	pravá
<g/>
,	,	kIx,	,
perspektivní	perspektivní	k2eAgFnPc4d1	perspektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ε	ε	k?	ε
=	=	kIx~	=
λ	λ	k?	λ
-	-	kIx~	-
zobrazení	zobrazení	k1gNnSc1	zobrazení
poledníků	poledník	k1gInPc2	poledník
</s>
</p>
<p>
<s>
r	r	kA	r
=	=	kIx~	=
2	[number]	k4	2
<g/>
R.	R.	kA	R.
<g/>
tg	tg	kA	tg
<g/>
(	(	kIx(	(
<g/>
θ	θ	k?	θ
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
kde	kde	k6eAd1	kde
θ	θ	k?	θ
<g/>
=	=	kIx~	=
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
<g/>
-φ	-φ	k?	-φ
poledníků	poledník	k1gInPc2	poledník
jsou	být	k5eAaImIp3nP	být
přímky	přímka	k1gFnPc1	přímka
sbíhající	sbíhající	k2eAgFnPc1d1	sbíhající
se	se	k3xPyFc4	se
v	v	k7c6	v
pólu	pól	k1gInSc6	pól
(	(	kIx(	(
<g/>
bodě	bod	k1gInSc6	bod
dotyku	dotyk	k1gInSc2	dotyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
jsou	být	k5eAaImIp3nP	být
koncentrické	koncentrický	k2eAgFnPc1d1	koncentrická
kružnice	kružnice	k1gFnPc1	kružnice
<g/>
,	,	kIx,	,
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodu	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
geografické	geografický	k2eAgFnSc2d1	geografická
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
podobný	podobný	k2eAgInSc1d1	podobný
gnomonické	gnomonický	k2eAgFnSc3d1	gnomonický
polární	polární	k2eAgFnSc3d1	polární
projekci	projekce	k1gFnSc3	projekce
<g/>
.	.	kIx.	.
</s>
<s>
Délkové	délkový	k2eAgNnSc1d1	délkové
zkreslení	zkreslení	k1gNnSc1	zkreslení
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
poledníků	poledník	k1gInPc2	poledník
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
projekce	projekce	k1gFnSc1	projekce
je	být	k5eAaImIp3nS	být
konformní	konformní	k2eAgFnSc1d1	konformní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kuželové	Kuželové	k2eAgFnPc1d1	Kuželové
projekce	projekce	k1gFnPc1	projekce
===	===	k?	===
</s>
</p>
<p>
<s>
Sem	sem	k6eAd1	sem
zařazujeme	zařazovat	k5eAaImIp1nP	zařazovat
všechna	všechen	k3xTgNnPc4	všechen
zobrazení	zobrazení	k1gNnPc4	zobrazení
na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
kuželovou	kuželový	k2eAgFnSc4d1	kuželová
<g/>
,	,	kIx,	,
dotýkající	dotýkající	k2eAgInSc4d1	dotýkající
se	se	k3xPyFc4	se
referenčního	referenční	k2eAgInSc2d1	referenční
elipsoidu	elipsoid	k1gInSc2	elipsoid
či	či	k8xC	či
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
protínající	protínající	k2eAgFnPc4d1	protínající
tyto	tento	k3xDgFnPc4	tento
plochy	plocha	k1gFnPc4	plocha
(	(	kIx(	(
<g/>
sečný	sečný	k2eAgInSc4d1	sečný
kužel	kužel	k1gInSc4	kužel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kužel	kužel	k1gInSc1	kužel
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
referenční	referenční	k2eAgFnSc1d1	referenční
koule	koule	k1gFnSc1	koule
(	(	kIx(	(
<g/>
anebo	anebo	k8xC	anebo
ji	on	k3xPp3gFnSc4	on
protíná	protínat	k5eAaImIp3nS	protínat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dotyková	dotykový	k2eAgFnSc1d1	dotyková
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
(	(	kIx(	(
<g/>
sečná	sečný	k2eAgFnSc1d1	sečná
rovnoběžka	rovnoběžka	k1gFnSc1	rovnoběžka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
nulové	nulový	k2eAgNnSc1d1	nulové
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
použitý	použitý	k2eAgInSc4d1	použitý
kužel	kužel	k1gInSc4	kužel
dotykový	dotykový	k2eAgInSc4d1	dotykový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
sečný	sečný	k2eAgInSc1d1	sečný
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
projekci	projekce	k1gFnSc4	projekce
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgFnPc7	dva
základními	základní	k2eAgFnPc7d1	základní
(	(	kIx(	(
<g/>
standardními	standardní	k2eAgFnPc7d1	standardní
<g/>
)	)	kIx)	)
rovnoběžkami	rovnoběžka	k1gFnPc7	rovnoběžka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
kuželových	kuželový	k2eAgFnPc6d1	kuželová
projekcích	projekce	k1gFnPc6	projekce
v	v	k7c6	v
normální	normální	k2eAgFnSc6d1	normální
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obrazem	obraz	k1gInSc7	obraz
poledníků	poledník	k1gInPc2	poledník
přímky	přímka	k1gFnPc1	přímka
sbíhající	sbíhající	k2eAgFnPc1d1	sbíhající
se	se	k3xPyFc4	se
ve	v	k7c6	v
vrcholu	vrchol	k1gInSc6	vrchol
kužele	kužel	k1gInSc2	kužel
<g/>
.	.	kIx.	.
</s>
<s>
Úhel	úhel	k1gInSc1	úhel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
svírají	svírat	k5eAaImIp3nP	svírat
se	se	k3xPyFc4	se
nerovná	rovnat	k5eNaImIp3nS	rovnat
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
délce	délka	k1gFnSc3	délka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
azimutálních	azimutální	k2eAgFnPc6d1	azimutální
projekcích	projekce	k1gFnPc6	projekce
v	v	k7c6	v
pólové	pólový	k2eAgFnSc6d1	pólová
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
zkreslené	zkreslený	k2eAgFnPc4d1	zkreslená
koeficientem	koeficient	k1gInSc7	koeficient
sbíhavosti	sbíhavost	k1gFnSc2	sbíhavost
poledníků	poledník	k1gInPc2	poledník
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
sin	sin	kA	sin
<g/>
.	.	kIx.	.
<g/>
φ	φ	k?	φ
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
φ	φ	k?	φ
<g/>
0	[number]	k4	0
-	-	kIx~	-
zeměpisná	zeměpisný	k2eAgFnSc1d1	zeměpisná
šířka	šířka	k1gFnSc1	šířka
dotykové	dotykový	k2eAgFnSc2d1	dotyková
rovnoběžky	rovnoběžka	k1gFnSc2	rovnoběžka
</s>
</p>
<p>
<s>
====	====	k?	====
Normální	normální	k2eAgFnSc1d1	normální
kuželová	kuželový	k2eAgFnSc1d1	kuželová
projekce	projekce	k1gFnSc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
zkreslená	zkreslený	k2eAgFnSc1d1	zkreslená
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
jen	jen	k9	jen
jako	jako	k9	jako
mapa	mapa	k1gFnSc1	mapa
informativní	informativní	k2eAgFnSc1d1	informativní
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
projekce	projekce	k1gFnPc4	projekce
pravé	pravý	k2eAgFnPc4d1	pravá
<g/>
,	,	kIx,	,
perspektivní	perspektivní	k2eAgFnPc4d1	perspektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ε	ε	k?	ε
=	=	kIx~	=
n.	n.	k?	n.
<g/>
λ	λ	k?	λ
</s>
</p>
<p>
<s>
r	r	kA	r
=	=	kIx~	=
R.	R.	kA	R.
<g/>
cotgφ	cotgφ	k?	cotgφ
</s>
</p>
<p>
<s>
====	====	k?	====
Konformní	konformní	k2eAgFnSc1d1	konformní
kuželová	kuželový	k2eAgFnSc1d1	kuželová
Lambertova	Lambertův	k2eAgFnSc1d1	Lambertova
projekce	projekce	k1gFnSc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Projekce	projekce	k1gFnSc1	projekce
je	být	k5eAaImIp3nS	být
konformní	konformní	k2eAgFnSc1d1	konformní
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
zobrazení	zobrazení	k1gNnSc4	zobrazení
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
sečný	sečný	k2eAgInSc1d1	sečný
kužel	kužel	k1gInSc1	kužel
v	v	k7c6	v
polykónickém	polykónický	k2eAgNnSc6d1	polykónický
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ortodroma	ortodroma	k1gFnSc1	ortodroma
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
1	[number]	k4	1
200	[number]	k4	200
km	km	kA	km
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
přímka	přímka	k1gFnSc1	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Loxodroma	loxodroma	k1gFnSc1	loxodroma
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jako	jako	k9	jako
logaritmická	logaritmický	k2eAgFnSc1d1	logaritmická
spirála	spirála	k1gFnSc1	spirála
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
km	km	kA	km
ji	on	k3xPp3gFnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
nahradit	nahradit	k5eAaPmF	nahradit
přímkou	přímka	k1gFnSc7	přímka
<g/>
.	.	kIx.	.
</s>
<s>
Traťový	traťový	k2eAgInSc4d1	traťový
úhel	úhel	k1gInSc4	úhel
je	být	k5eAaImIp3nS	být
potřebné	potřebný	k2eAgNnSc1d1	potřebné
odměřovat	odměřovat	k5eAaImF	odměřovat
bud	bouda	k1gFnPc2	bouda
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
trati	trať	k1gFnSc2	trať
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gInSc4	on
opravovat	opravovat	k5eAaImF	opravovat
o	o	k7c6	o
sbíhavosti	sbíhavost	k1gFnSc6	sbíhavost
poledníků	poledník	k1gInPc2	poledník
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
kuželové	kuželový	k2eAgFnSc6d1	kuželová
a	a	k8xC	a
azimutální	azimutální	k2eAgFnSc6d1	azimutální
pólové	pólový	k2eAgFnSc6d1	pólová
projekci	projekce	k1gFnSc6	projekce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
projekce	projekce	k1gFnPc4	projekce
pravé	pravý	k2eAgFnPc4d1	pravá
<g/>
,	,	kIx,	,
neperspektivní	perspektivní	k2eNgFnPc4d1	neperspektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ε	ε	k?	ε
=	=	kIx~	=
n.	n.	k?	n.
<g/>
λ	λ	k?	λ
</s>
</p>
<p>
<s>
ρ	ρ	k?	ρ
=	=	kIx~	=
ρ	ρ	k?	ρ
<g/>
0	[number]	k4	0
<g/>
[	[	kIx(	[
<g/>
(	(	kIx(	(
<g/>
tg	tg	kA	tg
(	(	kIx(	(
<g/>
φ	φ	k?	φ
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
+	+	kIx~	+
45	[number]	k4	45
<g/>
∘	∘	k?	∘
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
tg	tg	kA	tg
(	(	kIx(	(
<g/>
φ	φ	k?	φ
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
+	+	kIx~	+
45	[number]	k4	45
<g/>
∘	∘	k?	∘
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
sinρ	sinρ	k?	sinρ
<g/>
0	[number]	k4	0
</s>
</p>
<p>
<s>
====	====	k?	====
Křovákovo	Křovákův	k2eAgNnSc1d1	Křovákovo
zobrazení	zobrazení	k1gNnSc1	zobrazení
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
r.	r.	kA	r.
1918	[number]	k4	1918
bylo	být	k5eAaImAgNnS	být
potřebné	potřebný	k2eAgNnSc1d1	potřebné
zavést	zavést	k5eAaPmF	zavést
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
vhodnou	vhodný	k2eAgFnSc4d1	vhodná
<g/>
,	,	kIx,	,
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
zobrazovací	zobrazovací	k2eAgFnSc4d1	zobrazovací
soustavu	soustava	k1gFnSc4	soustava
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vícerých	vícerý	k4xRyIgInPc2	vícerý
návrhů	návrh	k1gInPc2	návrh
se	se	k3xPyFc4	se
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1937	[number]	k4	1937
definitivně	definitivně	k6eAd1	definitivně
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
přednosta	přednosta	k1gMnSc1	přednosta
Triangulační	triangulační	k2eAgFnSc2d1	triangulační
kanceláře	kancelář	k1gFnSc2	kancelář
Ing.	ing.	kA	ing.
Křovák	Křovák	k1gMnSc1	Křovák
<g/>
.	.	kIx.	.
</s>
<s>
Ing.	ing.	kA	ing.
Křovák	Křovák	k1gMnSc1	Křovák
použil	použít	k5eAaPmAgMnS	použít
konformní	konformní	k2eAgNnSc4d1	konformní
zobrazení	zobrazení	k1gNnSc4	zobrazení
koule	koule	k1gFnSc2	koule
na	na	k7c4	na
sečný	sečný	k2eAgInSc4d1	sečný
kužel	kužel	k1gInSc4	kužel
ve	v	k7c6	v
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
poloze	poloha	k1gFnSc6	poloha
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
na	na	k7c6	na
krajních	krajní	k2eAgFnPc6d1	krajní
kartografických	kartografický	k2eAgFnPc6d1	kartografická
rovnoběžkách	rovnoběžka	k1gFnPc6	rovnoběžka
délkové	délkový	k2eAgNnSc1d1	délkové
zkreslení	zkreslení	k1gNnSc1	zkreslení
+14	+14	k4	+14
cm	cm	kA	cm
na	na	k7c4	na
km	km	kA	km
a	a	k8xC	a
-10	-10	k4	-10
cm	cm	kA	cm
na	na	k7c4	na
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
použitý	použitý	k2eAgInSc4d1	použitý
kužel	kužel	k1gInSc4	kužel
ve	v	k7c6	v
všeobecné	všeobecný	k2eAgFnSc6d1	všeobecná
poloze	poloha	k1gFnSc6	poloha
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
standardní	standardní	k2eAgFnPc1d1	standardní
rovnoběžky	rovnoběžka	k1gFnPc1	rovnoběžka
sledují	sledovat	k5eAaImIp3nP	sledovat
protáhlý	protáhlý	k2eAgInSc4d1	protáhlý
tvar	tvar	k1gInSc4	tvar
našeho	náš	k3xOp1gInSc2	náš
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
přesnosti	přesnost	k1gFnPc4	přesnost
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
projekce	projekce	k1gFnSc1	projekce
nejlépe	dobře	k6eAd3	dobře
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Projekce	projekce	k1gFnSc1	projekce
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
pravé	pravý	k2eAgNnSc4d1	pravé
<g/>
,	,	kIx,	,
neperspektivní	perspektivní	k2eNgNnSc4d1	neperspektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Válcová	válcový	k2eAgFnSc1d1	válcová
projekce	projekce	k1gFnSc1	projekce
====	====	k?	====
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
válcové	válcový	k2eAgFnPc1d1	válcová
projekce	projekce	k1gFnPc1	projekce
jsou	být	k5eAaImIp3nP	být
promítnutím	promítnutí	k1gNnSc7	promítnutí
referenční	referenční	k2eAgFnSc2d1	referenční
koule	koule	k1gFnSc2	koule
na	na	k7c4	na
dotykový	dotykový	k2eAgInSc4d1	dotykový
nebo	nebo	k8xC	nebo
sečný	sečný	k2eAgInSc4d1	sečný
válec	válec	k1gInSc4	válec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
do	do	k7c2	do
roviny	rovina	k1gFnSc2	rovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Mercatorova	Mercatorův	k2eAgFnSc1d1	Mercatorova
mapa	mapa	k1gFnSc1	mapa
====	====	k?	====
</s>
</p>
<p>
<s>
Síť	síť	k1gFnSc1	síť
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
normální	normální	k2eAgFnSc1d1	normální
válcové	válcový	k2eAgFnSc3d1	válcová
projekci	projekce	k1gFnSc3	projekce
perspektivní	perspektivní	k2eAgFnSc1d1	perspektivní
<g/>
.	.	kIx.	.
</s>
<s>
Kartografická	kartografický	k2eAgFnSc1d1	kartografická
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
upravená	upravený	k2eAgFnSc1d1	upravená
<g/>
,	,	kIx,	,
umělým	umělý	k2eAgNnSc7d1	umělé
zvětšením	zvětšení	k1gNnSc7	zvětšení
měřítka	měřítko	k1gNnSc2	měřítko
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
poledníků	poledník	k1gInPc2	poledník
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc7	jejich
roztáhnutím	roztáhnutí	k1gNnSc7	roztáhnutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
je	být	k5eAaImIp3nS	být
úměrné	úměrný	k2eAgFnSc3d1	úměrná
změně	změna	k1gFnSc3	změna
měřítka	měřítko	k1gNnSc2	měřítko
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
X	X	kA	X
=	=	kIx~	=
R	R	kA	R
.	.	kIx.	.
λ	λ	k?	λ
</s>
</p>
<p>
<s>
Y	Y	kA	Y
=	=	kIx~	=
R	R	kA	R
.	.	kIx.	.
ln	ln	k?	ln
tg	tg	kA	tg
<g/>
(	(	kIx(	(
<g/>
φ	φ	k?	φ
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
+	+	kIx~	+
45	[number]	k4	45
<g/>
∘	∘	k?	∘
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Merkátorova	Merkátorův	k2eAgFnSc1d1	Merkátorův
mapa	mapa	k1gFnSc1	mapa
je	být	k5eAaImIp3nS	být
mapa	mapa	k1gFnSc1	mapa
loxodromická	loxodromický	k2eAgFnSc1d1	loxodromický
(	(	kIx(	(
<g/>
loxodroma	loxodroma	k1gFnSc1	loxodroma
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jako	jako	k9	jako
přímka	přímka	k1gFnSc1	přímka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ortodroma	ortodroma	k1gFnSc1	ortodroma
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jako	jako	k9	jako
křivka	křivka	k1gFnSc1	křivka
vypouklá	vypouklý	k2eAgFnSc1d1	vypouklá
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
bližšímu	blízký	k2eAgInSc3d2	bližší
pólu	pól	k1gInSc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
už	už	k6eAd1	už
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
svůj	svůj	k3xOyFgInSc4	svůj
původní	původní	k2eAgInSc4d1	původní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
jí	on	k3xPp3gFnSc3	on
ještě	ještě	k9	ještě
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
zákres	zákres	k1gInSc4	zákres
průběhu	průběh	k1gInSc2	průběh
loxodromy	loxodroma	k1gFnSc2	loxodroma
do	do	k7c2	do
map	mapa	k1gFnPc2	mapa
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
projekci	projekce	k1gFnSc6	projekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
projekce	projekce	k1gFnPc4	projekce
pravé	pravý	k2eAgFnPc4d1	pravá
<g/>
,	,	kIx,	,
neperspektivní	perspektivní	k2eNgFnPc4d1	neperspektivní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Gauss-Krügerova	Gauss-Krügerův	k2eAgFnSc1d1	Gauss-Krügerův
projekce	projekce	k1gFnSc1	projekce
=====	=====	k?	=====
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
zobrazení	zobrazení	k1gNnSc6	zobrazení
se	se	k3xPyFc4	se
povrch	povrch	k1gInSc1	povrch
referenčního	referenční	k2eAgInSc2d1	referenční
elipsoidu	elipsoid	k1gInSc2	elipsoid
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
poledníky	poledník	k1gInPc4	poledník
na	na	k7c4	na
poledníkové	poledníkový	k2eAgInPc4d1	poledníkový
pásy	pás	k1gInPc4	pás
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
takový	takový	k3xDgInSc4	takový
poledníkový	poledníkový	k2eAgInSc4d1	poledníkový
pás	pás	k1gInSc4	pás
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
konformně	konformně	k6eAd1	konformně
na	na	k7c4	na
samostatný	samostatný	k2eAgInSc4d1	samostatný
válec	válec	k1gInSc4	válec
v	v	k7c6	v
příčné	příčný	k2eAgFnSc6d1	příčná
(	(	kIx(	(
<g/>
transverzální	transverzální	k2eAgFnSc6d1	transverzální
<g/>
)	)	kIx)	)
poloze	poloha	k1gFnSc6	poloha
a	a	k8xC	a
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
se	se	k3xPyFc4	se
referenčního	referenční	k2eAgInSc2d1	referenční
elipsoidu	elipsoid	k1gInSc2	elipsoid
podél	podél	k7c2	podél
středního	střední	k2eAgInSc2d1	střední
poledníkového	poledníkový	k2eAgInSc2d1	poledníkový
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
základního	základní	k2eAgInSc2d1	základní
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
má	mít	k5eAaImIp3nS	mít
nulové	nulový	k2eAgNnSc1d1	nulové
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
Gaussovo	Gaussův	k2eAgNnSc1d1	Gaussovo
konformního	konformní	k2eAgNnSc2d1	konformní
zobrazení	zobrazení	k1gNnSc2	zobrazení
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
nezobrazují	zobrazovat	k5eNaImIp3nP	zobrazovat
poledníkové	poledníkový	k2eAgInPc1d1	poledníkový
pásy	pás	k1gInPc1	pás
referenční	referenční	k2eAgFnSc2d1	referenční
koule	koule	k1gFnSc2	koule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zemského	zemský	k2eAgInSc2d1	zemský
elipsoidu	elipsoid	k1gInSc2	elipsoid
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazovací	zobrazovací	k2eAgFnPc1d1	zobrazovací
rovnice	rovnice	k1gFnPc1	rovnice
pro	pro	k7c4	pro
konformní	konformní	k2eAgNnSc4d1	konformní
zobrazení	zobrazení	k1gNnSc4	zobrazení
elipsoidických	elipsoidický	k2eAgInPc2d1	elipsoidický
pásů	pás	k1gInPc2	pás
na	na	k7c4	na
transverzální	transverzální	k2eAgInSc4d1	transverzální
válec	válec	k1gInSc4	válec
odvodil	odvodit	k5eAaPmAgMnS	odvodit
Krůger	Krůger	k1gInSc4	Krůger
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
zobrazení	zobrazení	k1gNnSc1	zobrazení
nazývá	nazývat	k5eAaImIp3nS	nazývat
Gauss-Krůgerovo	Gauss-Krůgerův	k2eAgNnSc1d1	Gauss-Krůgerův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
byly	být	k5eAaImAgInP	být
použité	použitý	k2eAgNnSc4d1	Použité
6	[number]	k4	6
<g/>
°	°	k?	°
a	a	k8xC	a
3	[number]	k4	3
<g/>
°	°	k?	°
poledníkové	poledníkový	k2eAgInPc1d1	poledníkový
pásy	pás	k1gInPc1	pás
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rovník	rovník	k1gInSc1	rovník
se	se	k3xPyFc4	se
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
jako	jako	k9	jako
přímka	přímka	k1gFnSc1	přímka
<g/>
,	,	kIx,	,
zvolená	zvolený	k2eAgFnSc1d1	zvolená
za	za	k7c4	za
osu	osa	k1gFnSc4	osa
y	y	k?	y
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kolmý	kolmý	k2eAgInSc1d1	kolmý
na	na	k7c4	na
střední	střední	k2eAgInSc4d1	střední
poledník	poledník	k1gInSc4	poledník
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
zvoleného	zvolený	k2eAgInSc2d1	zvolený
za	za	k7c4	za
osu	osa	k1gFnSc4	osa
x.	x.	k?	x.
Každý	každý	k3xTgInSc4	každý
pás	pás	k1gInSc4	pás
má	mít	k5eAaImIp3nS	mít
svojí	svojit	k5eAaImIp3nS	svojit
souřadnicovou	souřadnicový	k2eAgFnSc4d1	souřadnicová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
obrazu	obraz	k1gInSc3	obraz
rovníku	rovník	k1gInSc2	rovník
symetricky	symetricky	k6eAd1	symetricky
zakřivené	zakřivený	k2eAgInPc4d1	zakřivený
čáry	čár	k1gInPc4	čár
<g/>
,	,	kIx,	,
vypouklé	vypouklý	k2eAgInPc4d1	vypouklý
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
nevzniklo	vzniknout	k5eNaPmAgNnS	vzniknout
promítáním	promítání	k1gNnSc7	promítání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
matematicky	matematicky	k6eAd1	matematicky
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
neperspektivní	perspektivní	k2eNgNnSc4d1	neperspektivní
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poledníkové	poledníkový	k2eAgInPc1d1	poledníkový
pásy	pás	k1gInPc1	pás
jsou	být	k5eAaImIp3nP	být
číslované	číslovaný	k2eAgInPc1d1	číslovaný
arabskými	arabský	k2eAgFnPc7d1	arabská
číslicemi	číslice	k1gFnPc7	číslice
začínající	začínající	k2eAgInSc4d1	začínající
od	od	k7c2	od
180	[number]	k4	180
<g/>
°	°	k?	°
průběžně	průběžně	k6eAd1	průběžně
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
poledníkový	poledníkový	k2eAgInSc1d1	poledníkový
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
potom	potom	k6eAd1	potom
dělený	dělený	k2eAgMnSc1d1	dělený
na	na	k7c4	na
pásy	pás	k1gInPc4	pás
rovnoběžkové	rovnoběžkový	k2eAgFnSc2d1	rovnoběžková
(	(	kIx(	(
<g/>
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
)	)	kIx)	)
široké	široký	k2eAgNnSc1d1	široké
4	[number]	k4	4
<g/>
°	°	k?	°
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
označené	označený	k2eAgInPc1d1	označený
velkými	velký	k2eAgNnPc7d1	velké
písmeny	písmeno	k1gNnPc7	písmeno
latinské	latinský	k2eAgFnSc2d1	Latinská
abecedy	abeceda	k1gFnSc2	abeceda
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
uspořádání	uspořádání	k1gNnSc6	uspořádání
pásů	pás	k1gInPc2	pás
a	a	k8xC	a
vrstev	vrstva	k1gFnPc2	vrstva
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
ČR	ČR	kA	ČR
a	a	k8xC	a
SR	SR	kA	SR
označené	označený	k2eAgFnSc2d1	označená
M-	M-	k1gFnSc2	M-
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
M-34	M-34	k1gFnSc1	M-34
a	a	k8xC	a
L-	L-	k1gFnSc1	L-
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
L-	L-	k1gFnSc1	L-
<g/>
34	[number]	k4	34
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Projekce	projekce	k1gFnSc1	projekce
tvoří	tvořit	k5eAaImIp3nS	tvořit
základ	základ	k1gInSc4	základ
leteckých	letecký	k2eAgFnPc2d1	letecká
map	mapa	k1gFnPc2	mapa
používaných	používaný	k2eAgFnPc2d1	používaná
naší	náš	k3xOp1gFnSc7	náš
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
letectví	letectví	k1gNnSc6	letectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
přijatelnou	přijatelný	k2eAgFnSc7d1	přijatelná
přesností	přesnost	k1gFnSc7	přesnost
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
této	tento	k3xDgFnSc2	tento
projekce	projekce	k1gFnSc2	projekce
měřit	měřit	k5eAaImF	měřit
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
i	i	k9	i
úhly	úhel	k1gInPc1	úhel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgFnSc2d1	Česká
osobnosti	osobnost	k1gFnSc2	osobnost
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
==	==	k?	==
</s>
</p>
<p>
<s>
v	v	k7c6	v
abecedním	abecední	k2eAgNnSc6d1	abecední
pořadíPavel	pořadíPavlo	k1gNnPc2	pořadíPavlo
Arentin	Arentin	k2eAgInSc1d1	Arentin
z	z	k7c2	z
Ehrenfeldu	Ehrenfeld	k1gInSc2	Ehrenfeld
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Petr	Petr	k1gMnSc1	Petr
Buchar	buchar	k1gInSc4	buchar
</s>
</p>
<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Criginger	Criginger	k1gMnSc1	Criginger
</s>
</p>
<p>
<s>
Vincento	Vincento	k1gNnSc1	Vincento
Maria	Mario	k1gMnSc2	Mario
Coronelli	Coronell	k1gMnPc1	Coronell
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Milan	Milan	k1gMnSc1	Milan
V.	V.	kA	V.
Drápela	Drápela	k1gMnSc1	Drápela
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Fabricius	Fabricius	k1gMnSc1	Fabricius
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Aleš	Aleš	k1gMnSc1	Aleš
Hašek	Hašek	k1gMnSc1	Hašek
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Helwig	Helwig	k1gMnSc1	Helwig
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Vladislav	Vladislav	k1gMnSc1	Vladislav
Hojovec	Hojovec	k1gMnSc1	Hojovec
<g/>
,	,	kIx,	,
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
SC	SC	kA	SC
</s>
</p>
<p>
<s>
pplk.	pplk.	kA	pplk.
Ing.	ing.	kA	ing.
Jiří	Jiří	k1gMnSc1	Jiří
Kánský	Kánský	k1gMnSc1	Kánský
</s>
</p>
<p>
<s>
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Klaudyán	Klaudyán	k1gMnSc1	Klaudyán
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Milan	Milan	k1gMnSc1	Milan
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Karel	Karel	k1gMnSc1	Karel
František	František	k1gMnSc1	František
Edvard	Edvard	k1gMnSc1	Edvard
Kořistka	Kořistka	k1gFnSc1	Kořistka
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ing.	ing.	kA	ing.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Josef	Josef	k1gMnSc1	Josef
Křovák	Křovák	k1gMnSc1	Křovák
</s>
</p>
<p>
<s>
RNDr.	RNDr.	kA	RNDr.
Olga	Olga	k1gFnSc1	Olga
Kudrnovská	Kudrnovský	k2eAgFnSc1d1	Kudrnovská
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Karel	Karel	k1gMnSc1	Karel
Kuchař	Kuchař	k1gMnSc1	Kuchař
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Lubomír	Lubomír	k1gMnSc1	Lubomír
Lauermann	Lauermann	k1gMnSc1	Lauermann
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Milan	Milan	k1gMnSc1	Milan
Martinek	Martinek	k1gMnSc1	Martinek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
Ing.	ing.	kA	ing.
Miroslav	Miroslav	k1gMnSc1	Miroslav
Mikšovský	Mikšovský	k1gMnSc1	Mikšovský
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
RNDr.	RNDr.	kA	RNDr.
Ludvík	Ludvík	k1gMnSc1	Ludvík
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kryštof	Kryštof	k1gMnSc1	Kryštof
Müller	Müller	k1gMnSc1	Müller
</s>
</p>
<p>
<s>
doc.	doc.	kA	doc.
Ing.	ing.	kA	ing.
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Václav	Václav	k1gMnSc1	Václav
Novák	Novák	k1gMnSc1	Novák
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Alena	Alena	k1gFnSc1	Alena
Rottová	Rottová	k1gFnSc1	Rottová
</s>
</p>
<p>
<s>
PhDr.	PhDr.	kA	PhDr.
Ondřej	Ondřej	k1gMnSc1	Ondřej
Roubík	Roubík	k1gMnSc1	Roubík
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Zdena	Zdena	k1gFnSc1	Zdena
Roulová	Roulová	k1gFnSc1	Roulová
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Erhart	Erharta	k1gFnPc2	Erharta
Srnka	srnka	k1gFnSc1	srnka
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Bohumil	Bohumil	k1gMnSc1	Bohumil
Šídlo	Šídlo	k1gMnSc1	Šídlo
</s>
</p>
<p>
<s>
RNDr.	RNDr.	kA	RNDr.
Ing.	ing.	kA	ing.
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
</s>
</p>
<p>
<s>
Ing.	ing.	kA	ing.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vahala	Vahal	k1gMnSc2	Vahal
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
Vetter	Vetter	k1gMnSc1	Vetter
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
Ing.	ing.	kA	ing.
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Veverka	Veverka	k1gMnSc1	Veverka
<g/>
,	,	kIx,	,
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Matyáš	Matyáš	k1gMnSc1	Matyáš
Vischer	Vischra	k1gFnPc2	Vischra
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Vogt	Vogt	k1gMnSc1	Vogt
<g/>
,	,	kIx,	,
řeholním	řeholní	k2eAgNnSc7d1	řeholní
jménem	jméno	k1gNnSc7	jméno
Mořic	Mořic	k1gMnSc1	Mořic
(	(	kIx(	(
<g/>
Mauritius	Mauritius	k1gInSc1	Mauritius
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
prof.	prof.	kA	prof.
RNDr.	RNDr.	kA	RNDr.
Vít	Vít	k1gMnSc1	Vít
Voženílek	Voženílek	k1gMnSc1	Voženílek
<g/>
,	,	kIx,	,
CSc.	CSc.	kA	CSc.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BENEŠ	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
pilota	pilota	k1gFnSc1	pilota
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svět	svět	k1gInSc1	svět
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
+	+	kIx~	+
starší	starý	k2eAgNnSc4d2	starší
vydání	vydání	k1gNnSc4	vydání
Učebnic	učebnice	k1gFnPc2	učebnice
pilota	pilot	k1gMnSc2	pilot
</s>
</p>
<p>
<s>
ČAPEK	Čapek	k1gMnSc1	Čapek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
373	[number]	k4	373
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
25153	[number]	k4	25153
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MIKLÍN	MIKLÍN	kA	MIKLÍN
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
DUŠEK	Dušek	k1gMnSc1	Dušek
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
;	;	kIx,	;
KRTIČKA	KRTIČKA	kA	KRTIČKA
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
<g/>
;	;	kIx,	;
KALÁB	Kaláb	k1gMnSc1	Kaláb
<g/>
,	,	kIx,	,
Oto	Oto	k1gMnSc1	Oto
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
map	mapa	k1gFnPc2	mapa
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
:	:	kIx,	:
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7599	[number]	k4	7599
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Mapování	mapování	k1gNnSc1	mapování
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
</s>
</p>
<p>
<s>
Geodézie	geodézie	k1gFnSc1	geodézie
</s>
</p>
<p>
<s>
Katastr	katastr	k1gInSc1	katastr
nemovitostí	nemovitost	k1gFnPc2	nemovitost
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Geometrický	geometrický	k2eAgInSc1d1	geometrický
plán	plán	k1gInSc1	plán
</s>
</p>
<p>
<s>
Vytyčení	vytyčení	k1gNnSc1	vytyčení
hranic	hranice	k1gFnPc2	hranice
pozemků	pozemek	k1gInPc2	pozemek
</s>
</p>
<p>
<s>
Zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
</s>
</p>
<p>
<s>
Kartografická	kartografický	k2eAgNnPc1d1	kartografické
díla	dílo	k1gNnPc1	dílo
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kartografie	kartografie	k1gFnSc2	kartografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kartografie	kartografie	k1gFnSc2	kartografie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
ICA	ICA	kA	ICA
</s>
</p>
<p>
<s>
On-line	Onin	k1gInSc5	On-lin
mapa	mapa	k1gFnSc1	mapa
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
plány	plán	k1gInPc4	plán
měst	město	k1gNnPc2	město
</s>
</p>
<p>
<s>
Multimediální	multimediální	k2eAgFnSc1d1	multimediální
učebnice	učebnice	k1gFnSc1	učebnice
dějin	dějiny	k1gFnPc2	dějiny
kartografie	kartografie	k1gFnSc1	kartografie
<g/>
,	,	kIx,	,
Geografický	geografický	k2eAgInSc1d1	geografický
ústav	ústav	k1gInSc1	ústav
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1	Přírodovědecká
fakulty	fakulta	k1gFnSc2	fakulta
MU	MU	kA	MU
</s>
</p>
<p>
<s>
https://web.archive.org/web/20110127162043/http://web.natur.cuni.cz/~bayertom/Mmk/	[url]	k4	https://web.archive.org/web/20110127162043/http://web.natur.cuni.cz/~bayertom/Mmk/
</s>
</p>
<p>
<s>
Geografická	geografický	k2eAgFnSc1d1	geografická
bibliografie	bibliografie	k1gFnSc1	bibliografie
ČR	ČR	kA	ČR
online	onlinout	k5eAaPmIp3nS	onlinout
Informace	informace	k1gFnPc4	informace
o	o	k7c6	o
kartografické	kartografický	k2eAgFnSc6d1	kartografická
produkci	produkce	k1gFnSc6	produkce
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zpřístupňuje	zpřístupňovat	k5eAaImIp3nS	zpřístupňovat
bibliografie	bibliografie	k1gFnSc1	bibliografie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vybrané	vybraný	k2eAgInPc4d1	vybraný
plnotextové	plnotextový	k2eAgInPc4d1	plnotextový
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
http://www.gis.zcu.cz/studium/mk2/multimedialni_texty/	[url]	k4	http://www.gis.zcu.cz/studium/mk2/multimedialni_texty/
</s>
</p>
