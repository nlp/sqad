<p>
<s>
Kerrith	Kerrith	k1gMnSc1	Kerrith
Brown	Brown	k1gMnSc1	Brown
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1962	[number]	k4	1962
v	v	k7c6	v
Wolverhamptonu	Wolverhampton	k1gInSc6	Wolverhampton
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
britský	britský	k2eAgMnSc1d1	britský
zápasník	zápasník	k1gMnSc1	zápasník
–	–	k?	–
judista	judista	k1gMnSc1	judista
tmavé	tmavý	k2eAgFnSc2d1	tmavá
pleti	pleť	k1gFnSc2	pleť
a	a	k8xC	a
anglické	anglický	k2eAgFnSc2d1	anglická
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
bronzový	bronzový	k2eAgMnSc1d1	bronzový
olympijský	olympijský	k2eAgMnSc1d1	olympijský
medailista	medailista	k1gMnSc1	medailista
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
judem	judo	k1gNnSc7	judo
začínal	začínat	k5eAaImAgMnS	začínat
ve	v	k7c6	v
12	[number]	k4	12
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Wolverhamptonu	Wolverhampton	k1gInSc6	Wolverhampton
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Maca	Macus	k1gMnSc2	Macus
Abbotse	Abbots	k1gMnSc2	Abbots
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Brookse	Brooks	k1gMnPc4	Brooks
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vyučeným	vyučený	k2eAgMnSc7d1	vyučený
kadeřníkem	kadeřník	k1gMnSc7	kadeřník
<g/>
.	.	kIx.	.
</s>
<s>
Specializoval	specializovat	k5eAaBmAgMnS	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
techniky	technika	k1gFnPc4	technika
submisson	submissona	k1gFnPc2	submissona
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
osobní	osobní	k2eAgFnSc7d1	osobní
technikou	technika	k1gFnSc7	technika
bylo	být	k5eAaImAgNnS	být
sankaku-jime	sankakuim	k1gMnSc5	sankaku-jim
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
sankaku-gatame	sankakuatam	k1gInSc5	sankaku-gatam
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
ho	on	k3xPp3gMnSc4	on
viděli	vidět	k5eAaImAgMnP	vidět
jako	jako	k8xS	jako
nástupce	nástupce	k1gMnSc4	nástupce
Neila	Neil	k1gMnSc4	Neil
Adamse	Adams	k1gMnSc4	Adams
<g/>
.	.	kIx.	.
</s>
<s>
Adams	Adams	k6eAd1	Adams
se	se	k3xPyFc4	se
však	však	k9	však
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
nechystal	chystat	k5eNaImAgMnS	chystat
a	a	k8xC	a
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgMnS	muset
hubnout	hubnout	k5eAaImF	hubnout
do	do	k7c2	do
lehké	lehký	k2eAgFnSc2d1	lehká
váhové	váhový	k2eAgFnSc2d1	váhová
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
a	a	k8xC	a
po	po	k7c6	po
dobrém	dobrý	k2eAgInSc6d1	dobrý
taktickém	taktický	k2eAgInSc6d1	taktický
výkonu	výkon	k1gInSc6	výkon
získal	získat	k5eAaPmAgInS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Soulu	Soul	k1gInSc6	Soul
a	a	k8xC	a
opět	opět	k6eAd1	opět
jeho	jeho	k3xOp3gFnPc4	jeho
ambice	ambice	k1gFnPc4	ambice
na	na	k7c4	na
výhru	výhra	k1gFnSc4	výhra
skončili	skončit	k5eAaPmAgMnP	skončit
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
porazil	porazit	k5eAaPmAgMnS	porazit
účadujícího	účadující	k2eAgMnSc4d1	účadující
mistra	mistr	k1gMnSc4	mistr
světa	svět	k1gInSc2	svět
Američana	Američan	k1gMnSc2	Američan
Swaina	Swain	k1gMnSc2	Swain
a	a	k8xC	a
obhájil	obhájit	k5eAaPmAgInS	obhájit
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
však	však	k9	však
musel	muset	k5eAaImAgMnS	muset
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vrátit	vrátit	k5eAaPmF	vrátit
kvůli	kvůli	k7c3	kvůli
pozitivnímu	pozitivní	k2eAgInSc3d1	pozitivní
dopingovému	dopingový	k2eAgInSc3d1	dopingový
testu	test	k1gInSc3	test
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
předturnajou	předturnajou	k6eAd1	předturnajou
shazování	shazování	k1gNnSc6	shazování
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
pomohl	pomoct	k5eAaPmAgInS	pomoct
nedovoleným	dovolený	k2eNgInSc7d1	nedovolený
přípravkem	přípravek	k1gInSc7	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvouletém	dvouletý	k2eAgInSc6d1	dvouletý
trestu	trest	k1gInSc6	trest
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
polostřední	polostřední	k2eAgFnSc6d1	polostřední
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
olympijské	olympijský	k2eAgFnSc6d1	olympijská
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
prohrál	prohrát	k5eAaPmAgMnS	prohrát
s	s	k7c7	s
Ryanem	Ryan	k1gMnSc7	Ryan
Birchem	Birch	k1gMnSc7	Birch
a	a	k8xC	a
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
sportovní	sportovní	k2eAgFnSc2d1	sportovní
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
trenérské	trenérský	k2eAgFnSc3d1	trenérská
práci	práce	k1gFnSc3	práce
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
funkcionářské	funkcionářský	k2eAgNnSc1d1	funkcionářské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentnem	prezidentno	k1gNnSc7	prezidentno
amatérské	amatérský	k2eAgFnSc2d1	amatérská
federace	federace	k1gFnSc2	federace
Mixed	Mixed	k1gMnSc1	Mixed
martial	martial	k1gMnSc1	martial
arts	arts	k1gInSc1	arts
(	(	kIx(	(
<g/>
IMMAF	IMMAF	kA	IMMAF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Kerritha	Kerrith	k1gMnSc2	Kerrith
Browna	Brown	k1gMnSc2	Brown
na	na	k7c6	na
Judoinside	Judoinsid	k1gInSc5	Judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
