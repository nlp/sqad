<p>
<s>
Hřbitov	hřbitov	k1gInSc4	hřbitov
Grove	Groev	k1gFnSc2	Groev
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Chinbrooku	Chinbrook	k1gInSc6	Chinbrook
<g/>
,	,	kIx,	,
v	v	k7c6	v
Lewishamu	Lewisham	k1gInSc6	Lewisham
<g/>
,	,	kIx,	,
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
Grove	Groev	k1gFnSc2	Groev
Park	park	k1gInSc1	park
na	na	k7c4	na
Marvels	Marvels	k1gInSc4	Marvels
Lane	Lan	k1gFnSc2	Lan
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
Chinbrook	Chinbrook	k1gInSc4	Chinbrook
Meadows	Meadowsa	k1gFnPc2	Meadowsa
a	a	k8xC	a
Marvels	Marvelsa	k1gFnPc2	Marvelsa
Wood	Wooda	k1gFnPc2	Wooda
<g/>
,	,	kIx,	,
SE	s	k7c7	s
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
Hřbitov	hřbitov	k1gInSc4	hřbitov
Grove	Groev	k1gFnSc2	Groev
Park	park	k1gInSc1	park
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
anglickou	anglický	k2eAgFnSc7d1	anglická
památkou	památka	k1gFnSc7	památka
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2003	[number]	k4	2003
jako	jako	k8xS	jako
krajina	krajina	k1gFnSc1	krajina
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
historického	historický	k2eAgInSc2d1	historický
významu	význam	k1gInSc2	význam
(	(	kIx(	(
<g/>
stupeň	stupeň	k1gInSc1	stupeň
II	II	kA	II
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
má	mít	k5eAaImIp3nS	mít
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
pečlivě	pečlivě	k6eAd1	pečlivě
plánované	plánovaný	k2eAgNnSc4d1	plánované
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
specifické	specifický	k2eAgInPc4d1	specifický
pro	pro	k7c4	pro
třicátá	třicátý	k4xOgNnPc4	třicátý
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
56	[number]	k4	56
vojenských	vojenský	k2eAgInPc2d1	vojenský
hrobů	hrob	k1gInPc2	hrob
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
;	;	kIx,	;
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
hroby	hrob	k1gInPc1	hrob
nejsou	být	k5eNaImIp3nP	být
označeny	označit	k5eAaPmNgInP	označit
náhrobními	náhrobní	k2eAgInPc7d1	náhrobní
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uvedeni	uvést	k5eAaPmNgMnP	uvést
na	na	k7c4	na
pamětní	pamětní	k2eAgFnPc4d1	pamětní
zdi	zeď	k1gFnPc4	zeď
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
uprostřed	uprostřed	k7c2	uprostřed
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Grove	Groev	k1gFnSc2	Groev
Park	park	k1gInSc1	park
Cemetery	Cemeter	k1gMnPc7	Cemeter
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
