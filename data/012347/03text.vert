<p>
<s>
Křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
použitý	použitý	k2eAgInSc4d1	použitý
k	k	k7c3	k
vyprodukování	vyprodukování	k1gNnSc3	vyprodukování
aerodynamické	aerodynamický	k2eAgFnSc2d1	aerodynamická
síly	síla	k1gFnSc2	síla
při	při	k7c6	při
cestování	cestování	k1gNnSc6	cestování
vzduchem	vzduch	k1gInSc7	vzduch
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
plynným	plynný	k2eAgNnSc7d1	plynné
médiem	médium	k1gNnSc7	médium
pomocí	pomocí	k7c2	pomocí
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
používalo	používat	k5eAaImAgNnS	používat
pro	pro	k7c4	pro
přední	přední	k2eAgFnPc4d1	přední
končetiny	končetina	k1gFnPc4	končetina
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
význam	význam	k1gInSc1	význam
však	však	k9	však
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
křídla	křídlo	k1gNnSc2	křídlo
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
netopýrů	netopýr	k1gMnPc2	netopýr
a	a	k8xC	a
pterosaurů	pterosaurus	k1gMnPc2	pterosaurus
<g/>
;	;	kIx,	;
křídla	křídlo	k1gNnSc2	křídlo
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
mytologické	mytologický	k2eAgFnPc1d1	mytologická
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
andělé	anděl	k1gMnPc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
zařízení	zařízení	k1gNnPc1	zařízení
vytvořená	vytvořený	k2eAgNnPc1d1	vytvořené
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
samokřídla	samokřídnout	k5eAaPmAgFnS	samokřídnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křídla	křídlo	k1gNnPc4	křídlo
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
ne	ne	k9	ne
všichni	všechen	k3xTgMnPc1	všechen
ptáci	pták	k1gMnPc1	pták
je	on	k3xPp3gFnPc4	on
dokážou	dokázat	k5eAaPmIp3nP	dokázat
využívat	využívat	k5eAaPmF	využívat
k	k	k7c3	k
letu	let	k1gInSc3	let
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
u	u	k7c2	u
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
–	–	k?	–
u	u	k7c2	u
významné	významný	k2eAgFnSc2d1	významná
části	část	k1gFnSc2	část
hmyzu	hmyz	k1gInSc2	hmyz
nebo	nebo	k8xC	nebo
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
letounů	letoun	k1gMnPc2	letoun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Křídla	křídlo	k1gNnPc4	křídlo
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
stavební	stavební	k2eAgInSc4d1	stavební
prvek	prvek	k1gInSc4	prvek
letadla	letadlo	k1gNnSc2	letadlo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
letadlo	letadlo	k1gNnSc4	letadlo
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
udrží	udržet	k5eAaPmIp3nS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Křídlo	křídlo	k1gNnSc1	křídlo
bylo	být	k5eAaImAgNnS	být
vyvíjeno	vyvíjet	k5eAaImNgNnS	vyvíjet
již	již	k9	již
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
počátku	počátek	k1gInSc2	počátek
letectví	letectví	k1gNnSc2	letectví
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
napodobování	napodobování	k1gNnSc3	napodobování
ptačích	ptačí	k2eAgNnPc2d1	ptačí
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tvary	tvar	k1gInPc4	tvar
křídla	křídlo	k1gNnSc2	křídlo
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
modely	model	k1gInPc1	model
<g/>
.	.	kIx.	.
</s>
<s>
Vztlak	vztlak	k1gInSc1	vztlak
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
síla	síla	k1gFnSc1	síla
působící	působící	k2eAgFnSc1d1	působící
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
vznikající	vznikající	k2eAgInSc1d1	vznikající
právě	právě	k6eAd1	právě
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
při	při	k7c6	při
proudění	proudění	k1gNnSc6	proudění
vzduchu	vzduch	k1gInSc2	vzduch
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
podtlakem	podtlak	k1gInSc7	podtlak
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
povrchu	povrch	k1gInSc6	povrch
křídla	křídlo	k1gNnSc2	křídlo
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
přetlakem	přetlak	k1gInSc7	přetlak
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozbor	rozbor	k1gInSc1	rozbor
křídla	křídlo	k1gNnSc2	křídlo
v	v	k7c6	v
technice	technika	k1gFnSc6	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Podívame	Podívam	k1gInSc5	Podívam
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c4	na
průřez	průřez	k1gInSc4	průřez
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
,	,	kIx,	,
všimneme	všimnout	k5eAaPmIp1nP	všimnout
si	se	k3xPyFc3	se
několika	několik	k4yIc2	několik
důležitých	důležitý	k2eAgFnPc2d1	důležitá
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
náběžná	náběžný	k2eAgNnPc4d1	náběžné
hrana	hrana	k1gNnPc4	hrana
<g/>
,	,	kIx,	,
odtoková	odtokový	k2eAgNnPc4d1	odtokové
hrana	hrana	k1gNnPc4	hrana
<g/>
,	,	kIx,	,
tětiva	tětiva	k1gFnSc1	tětiva
<g/>
,	,	kIx,	,
profil	profil	k1gInSc4	profil
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
maximální	maximální	k2eAgNnSc4d1	maximální
prohnutí	prohnutí	k1gNnSc4	prohnutí
<g/>
.	.	kIx.	.
</s>
<s>
Proudnice	proudnice	k1gFnSc1	proudnice
vzduchu	vzduch	k1gInSc2	vzduch
za	za	k7c2	za
letu	let	k1gInSc2	let
jako	jako	k8xS	jako
první	první	k4xOgFnSc2	první
potkají	potkat	k5eAaPmIp3nP	potkat
náběžnou	náběžný	k2eAgFnSc4d1	náběžná
hranu	hrana	k1gFnSc4	hrana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
odvádí	odvádět	k5eAaImIp3nS	odvádět
proudnice	proudnice	k1gFnPc4	proudnice
do	do	k7c2	do
středu	střed	k1gInSc2	střed
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
<g/>
,	,	kIx,	,
v	v	k7c6	v
laminárním	laminární	k2eAgNnSc6d1	laminární
proudění	proudění	k1gNnSc6	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Proudnice	proudnice	k1gFnPc1	proudnice
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
a	a	k8xC	a
proudy	proud	k1gInPc1	proud
potkávají	potkávat	k5eAaImIp3nP	potkávat
odtokovou	odtokový	k2eAgFnSc4d1	odtoková
hranu	hrana	k1gFnSc4	hrana
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
stavěná	stavěný	k2eAgFnSc1d1	stavěná
vždy	vždy	k6eAd1	vždy
proti	proti	k7c3	proti
vytváření	vytváření	k1gNnSc3	vytváření
vírů	vír	k1gInPc2	vír
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
způsobily	způsobit	k5eAaPmAgFnP	způsobit
ztrátu	ztráta	k1gFnSc4	ztráta
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Tětivou	tětiva	k1gFnSc7	tětiva
profilu	profil	k1gInSc2	profil
je	být	k5eAaImIp3nS	být
úsečka	úsečka	k1gFnSc1	úsečka
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
koncích	konec	k1gInPc6	konec
je	být	k5eAaImIp3nS	být
náběžná	náběžný	k2eAgFnSc1d1	náběžná
a	a	k8xC	a
odtoková	odtokový	k2eAgFnSc1d1	odtoková
hrana	hrana	k1gFnSc1	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Uděláme	udělat	k5eAaPmIp1nP	udělat
<g/>
-li	i	k?	-li
soustavu	soustava	k1gFnSc4	soustava
kružnic	kružnice	k1gFnPc2	kružnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrany	hrana	k1gFnPc1	hrana
profilu	profil	k1gInSc2	profil
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
tečnami	tečna	k1gFnPc7	tečna
a	a	k8xC	a
spojíme	spojit	k5eAaPmIp1nP	spojit
jejich	jejich	k3xOp3gInPc4	jejich
středy	střed	k1gInPc4	střed
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nám	my	k3xPp1nPc3	my
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
či	či	k8xC	či
maximální	maximální	k2eAgNnSc4d1	maximální
prohnutí	prohnutí	k1gNnSc4	prohnutí
křídla	křídlo	k1gNnSc2	křídlo
vůči	vůči	k7c3	vůči
tetivě	tetiva	k1gFnSc3	tetiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Aerodynamická	aerodynamický	k2eAgFnSc1d1	aerodynamická
vztlaková	vztlakový	k2eAgFnSc1d1	vztlaková
síla	síla	k1gFnSc1	síla
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
křídlo	křídlo	k1gNnSc4	křídlo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
křídlo	křídlo	k1gNnSc4	křídlo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
