<s>
Křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
povrch	povrch	k1gInSc4	povrch
použitý	použitý	k2eAgInSc4d1	použitý
k	k	k7c3	k
vyprodukování	vyprodukování	k1gNnSc3	vyprodukování
aerodynamické	aerodynamický	k2eAgFnSc2d1	aerodynamická
síly	síla	k1gFnSc2	síla
při	při	k7c6	při
cestování	cestování	k1gNnSc6	cestování
vzduchem	vzduch	k1gInSc7	vzduch
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
plynným	plynný	k2eAgNnSc7d1	plynné
médiem	médium	k1gNnSc7	médium
pomocí	pomocí	k7c2	pomocí
letu	let	k1gInSc2	let
<g/>
.	.	kIx.	.
</s>
