<s>
Optimalizace	optimalizace	k1gFnSc1	optimalizace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
takový	takový	k3xDgInSc4	takový
proces	proces	k1gInSc4	proces
modifikace	modifikace	k1gFnSc2	modifikace
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
vyšší	vysoký	k2eAgFnSc3d2	vyšší
efektivitě	efektivita	k1gFnSc3	efektivita
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nároků	nárok	k1gInPc2	nárok
celého	celý	k2eAgInSc2d1	celý
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
