<p>
<s>
Optimalizace	optimalizace	k1gFnSc1	optimalizace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
takový	takový	k3xDgInSc4	takový
proces	proces	k1gInSc4	proces
modifikace	modifikace	k1gFnSc2	modifikace
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
vyšší	vysoký	k2eAgFnSc3d2	vyšší
efektivitě	efektivita	k1gFnSc3	efektivita
nebo	nebo	k8xC	nebo
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
nároků	nárok	k1gInPc2	nárok
celého	celý	k2eAgInSc2d1	celý
výpočetního	výpočetní	k2eAgInSc2d1	výpočetní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Výpočetním	výpočetní	k2eAgInSc7d1	výpočetní
systémem	systém	k1gInSc7	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
počítačová	počítačový	k2eAgFnSc1d1	počítačová
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
komplexní	komplexní	k2eAgNnSc1d1	komplexní
řešení	řešení	k1gNnSc1	řešení
určitého	určitý	k2eAgInSc2d1	určitý
problému	problém	k1gInSc2	problém
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Například	například	k6eAd1	například
program	program	k1gInSc1	program
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
optimalizován	optimalizovat	k5eAaBmNgMnS	optimalizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pracoval	pracovat	k5eAaImAgMnS	pracovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
výpočtu	výpočet	k1gInSc2	výpočet
méně	málo	k6eAd2	málo
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
systémových	systémový	k2eAgInPc2d1	systémový
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
rychleji	rychle	k6eAd2	rychle
spustil	spustit	k5eAaPmAgInS	spustit
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
optimalizace	optimalizace	k1gFnSc2	optimalizace
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
provádění	provádění	k1gNnSc2	provádění
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
výsledného	výsledný	k2eAgInSc2d1	výsledný
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
na	na	k7c6	na
paměťové	paměťový	k2eAgFnSc6d1	paměťová
náročnosti	náročnost	k1gFnSc6	náročnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úrovně	úroveň	k1gFnSc2	úroveň
optimalizace	optimalizace	k1gFnSc2	optimalizace
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
optimalizace	optimalizace	k1gFnSc1	optimalizace
prováděna	provádět	k5eAaImNgFnS	provádět
hlavně	hlavně	k6eAd1	hlavně
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
již	již	k6eAd1	již
strojově	strojově	k6eAd1	strojově
<g/>
.	.	kIx.	.
</s>
<s>
Optimalizace	optimalizace	k1gFnPc1	optimalizace
mohou	moct	k5eAaImIp3nP	moct
probíhat	probíhat	k5eAaImF	probíhat
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Úroveň	úroveň	k1gFnSc4	úroveň
návrhu	návrh	k1gInSc2	návrh
(	(	kIx(	(
<g/>
designu	design	k1gInSc2	design
<g/>
)	)	kIx)	)
programu	program	k1gInSc2	program
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
návrh	návrh	k1gInSc1	návrh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
optimalizován	optimalizovat	k5eAaBmNgMnS	optimalizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
co	co	k3yQnSc4	co
nejlépe	dobře	k6eAd3	dobře
využíval	využívat	k5eAaImAgMnS	využívat
dostupné	dostupný	k2eAgInPc4d1	dostupný
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Implementace	implementace	k1gFnSc1	implementace
tohoto	tento	k3xDgInSc2	tento
návrhu	návrh	k1gInSc2	návrh
bude	být	k5eAaImBp3nS	být
těžit	těžit	k5eAaImF	těžit
z	z	k7c2	z
dobrého	dobrý	k2eAgInSc2d1	dobrý
výběru	výběr	k1gInSc2	výběr
efektivních	efektivní	k2eAgInPc2d1	efektivní
algoritmů	algoritmus	k1gInPc2	algoritmus
a	a	k8xC	a
implementace	implementace	k1gFnSc2	implementace
těchto	tento	k3xDgInPc2	tento
algoritmů	algoritmus	k1gInPc2	algoritmus
bude	být	k5eAaImBp3nS	být
profitovat	profitovat	k5eAaBmF	profitovat
z	z	k7c2	z
dobře	dobře	k6eAd1	dobře
napsaného	napsaný	k2eAgInSc2d1	napsaný
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Architektonický	architektonický	k2eAgInSc1d1	architektonický
návrh	návrh	k1gInSc1	návrh
systému	systém	k1gInSc2	systém
má	mít	k5eAaImIp3nS	mít
převážně	převážně	k6eAd1	převážně
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
algoritmu	algoritmus	k1gInSc2	algoritmus
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
účinnost	účinnost	k1gFnSc4	účinnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jakékoli	jakýkoli	k3yIgFnPc4	jakýkoli
jiné	jiný	k2eAgFnPc4d1	jiná
položky	položka	k1gFnPc4	položka
návrhu	návrh	k1gInSc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
algoritmu	algoritmus	k1gInSc2	algoritmus
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
první	první	k4xOgFnSc1	první
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rozhodnuta	rozhodnout	k5eAaPmNgFnS	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
však	však	k9	však
optimalizace	optimalizace	k1gFnPc4	optimalizace
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
složitějších	složitý	k2eAgInPc2d2	složitější
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
,	,	kIx,	,
využití	využití	k1gNnSc1	využití
"	"	kIx"	"
<g/>
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
případů	případ	k1gInPc2	případ
<g/>
"	"	kIx"	"
a	a	k8xC	a
speciálních	speciální	k2eAgInPc2d1	speciální
"	"	kIx"	"
<g/>
triků	trik	k1gInPc2	trik
<g/>
"	"	kIx"	"
a	a	k8xC	a
realizaci	realizace	k1gFnSc4	realizace
složitých	složitý	k2eAgInPc2d1	složitý
kompromisů	kompromis	k1gInPc2	kompromis
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Plně	plně	k6eAd1	plně
optimalizován	optimalizován	k2eAgInSc1d1	optimalizován
<g/>
"	"	kIx"	"
program	program	k1gInSc1	program
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obtížné	obtížný	k2eAgNnSc4d1	obtížné
pochopitelný	pochopitelný	k2eAgInSc4d1	pochopitelný
a	a	k8xC	a
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
více	hodně	k6eAd2	hodně
softwarových	softwarový	k2eAgFnPc2d1	softwarová
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
než	než	k8xS	než
neoptimalizovaná	optimalizovaný	k2eNgFnSc1d1	neoptimalizovaná
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úroveň	úroveň	k1gFnSc4	úroveň
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
===	===	k?	===
</s>
</p>
<p>
<s>
Vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
nekvalitnímu	kvalitní	k2eNgNnSc3d1	nekvalitní
kódování	kódování	k1gNnSc3	kódování
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
zlepšit	zlepšit	k5eAaPmF	zlepšit
výkonnost	výkonnost	k1gFnSc4	výkonnost
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zamezíme	zamezit	k5eAaPmIp1nP	zamezit
se	se	k3xPyFc4	se
zjevnému	zjevný	k2eAgMnSc3d1	zjevný
"	"	kIx"	"
<g/>
zpomalení	zpomalení	k1gNnSc3	zpomalení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
optimalizace	optimalizace	k1gFnPc1	optimalizace
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
provádí	provádět	k5eAaImIp3nS	provádět
optimalizace	optimalizace	k1gFnSc1	optimalizace
kompilátoru	kompilátor	k1gInSc2	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
systému	systém	k1gInSc2	systém
optimalizace	optimalizace	k1gFnSc2	optimalizace
kompilátoru	kompilátor	k1gInSc2	kompilátor
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
spustitelný	spustitelný	k2eAgInSc1d1	spustitelný
program	program	k1gInSc1	program
byl	být	k5eAaImAgInS	být
optimalizován	optimalizovat	k5eAaBmNgInS	optimalizovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
stejně	stejně	k6eAd1	stejně
tolik	tolik	k6eAd1	tolik
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kompilátor	kompilátor	k1gInSc1	kompilátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úroveň	úroveň	k1gFnSc1	úroveň
sestavování	sestavování	k1gNnSc2	sestavování
kódu	kód	k1gInSc2	kód
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
psaní	psaní	k1gNnSc2	psaní
kódu	kód	k1gInSc2	kód
pomocí	pomocí	k7c2	pomocí
jazyku	jazyk	k1gInSc3	jazyk
symbolických	symbolický	k2eAgFnPc2d1	symbolická
adres	adresa	k1gFnPc2	adresa
určená	určený	k2eAgFnSc1d1	určená
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
hardware	hardware	k1gInSc4	hardware
může	moct	k5eAaImIp3nS	moct
programátor	programátor	k1gMnSc1	programátor
vyrobit	vyrobit	k5eAaPmF	vyrobit
co	co	k9	co
nejúčinnější	účinný	k2eAgInSc4d3	nejúčinnější
a	a	k8xC	a
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
využívá	využívat	k5eAaPmIp3nS	využívat
plného	plný	k2eAgInSc2d1	plný
repertoáru	repertoár	k1gInSc2	repertoár
strojových	strojový	k2eAgFnPc2d1	strojová
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
vestavěné	vestavěný	k2eAgInPc4d1	vestavěný
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
tradičně	tradičně	k6eAd1	tradičně
napsány	napsat	k5eAaPmNgInP	napsat
v	v	k7c6	v
assembleru	assembler	k1gInSc6	assembler
právě	právě	k9	právě
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
programy	program	k1gInPc1	program
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgInPc2d1	malý
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
psaný	psaný	k2eAgInSc1d1	psaný
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
do	do	k7c2	do
konce	konec	k1gInSc2	konec
v	v	k7c6	v
assembleru	assembler	k1gInSc6	assembler
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
času	čas	k1gInSc3	čas
a	a	k8xC	a
nákladům	náklad	k1gInPc3	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
sestaví	sestavit	k5eAaPmIp3nP	sestavit
z	z	k7c2	z
vyššího	vysoký	k2eAgInSc2d2	vyšší
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ručně	ručně	k6eAd1	ručně
optimalizované	optimalizovaný	k2eAgInPc1d1	optimalizovaný
právě	právě	k9	právě
odtamtud	odtamtud	k6eAd1	odtamtud
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
kódu	kód	k1gInSc2	kód
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
důležité	důležitý	k2eAgFnPc1d1	důležitá
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
napsány	napsat	k5eAaPmNgInP	napsat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
vyšší	vysoký	k2eAgFnSc2d2	vyšší
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
modernějšími	moderní	k2eAgFnPc7d2	modernější
optimalizacemi	optimalizace	k1gFnPc7	optimalizace
kompilátoru	kompilátor	k1gInSc2	kompilátor
a	a	k8xC	a
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
složitosti	složitost	k1gFnSc3	složitost
CPU	CPU	kA	CPU
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
obtížnější	obtížný	k2eAgNnSc1d2	obtížnější
psát	psát	k5eAaImF	psát
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
optimalizován	optimalizován	k2eAgInSc1d1	optimalizován
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
kód	kód	k1gInSc4	kód
generovaný	generovaný	k2eAgInSc4d1	generovaný
kompilátorem	kompilátor	k1gMnSc7	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kódu	kód	k1gInSc2	kód
napsaného	napsaný	k2eAgInSc2d1	napsaný
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
sestaveno	sestavit	k5eAaPmNgNnS	sestavit
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
běžet	běžet	k5eAaImF	běžet
na	na	k7c4	na
co	co	k3yRnSc4	co
největším	veliký	k2eAgNnSc6d3	veliký
množství	množství	k1gNnSc6	množství
možných	možný	k2eAgInPc2d1	možný
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
programátoři	programátor	k1gMnPc1	programátor
a	a	k8xC	a
překladače	překladač	k1gMnSc2	překladač
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
využívají	využívat	k5eAaImIp3nP	využívat
účinnějších	účinný	k2eAgInPc2d2	účinnější
nebo	nebo	k8xC	nebo
novější	nový	k2eAgInPc1d2	novější
procesory	procesor	k1gInPc1	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kód	kód	k1gInSc4	kód
vyladěný	vyladěný	k2eAgInSc4d1	vyladěný
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
procesor	procesor	k1gInSc4	procesor
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
těchto	tento	k3xDgInPc2	tento
pokynů	pokyn	k1gInPc2	pokyn
stále	stále	k6eAd1	stále
optimální	optimální	k2eAgInSc4d1	optimální
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
procesoru	procesor	k1gInSc6	procesor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
jiný	jiný	k2eAgInSc1d1	jiný
způsob	způsob	k1gInSc1	způsob
ladění	ladění	k1gNnSc2	ladění
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
runtime	runtime	k1gInSc1	runtime
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Just-in-time	Justnimat	k5eAaPmIp3nS	Just-in-timat
překladače	překladač	k1gInPc4	překladač
assembleru	assembler	k1gInSc2	assembler
a	a	k8xC	a
programátoři	programátor	k1gMnPc1	programátor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
schopni	schopen	k2eAgMnPc1d1	schopen
provést	provést	k5eAaPmF	provést
optimalizace	optimalizace	k1gFnPc4	optimalizace
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
optimalizace	optimalizace	k1gFnPc1	optimalizace
přesahují	přesahovat	k5eAaImIp3nP	přesahovat
schopnosti	schopnost	k1gFnSc3	schopnost
statických	statický	k2eAgMnPc2d1	statický
kompilátorů	kompilátor	k1gMnPc2	kompilátor
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dynamicky	dynamicky	k6eAd1	dynamicky
nastavují	nastavovat	k5eAaImIp3nP	nastavovat
parametry	parametr	k1gInPc1	parametr
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
aktuálním	aktuální	k2eAgInSc6d1	aktuální
vstupu	vstup	k1gInSc6	vstup
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
faktorů	faktor	k1gInPc2	faktor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Optimalizace	optimalizace	k1gFnSc2	optimalizace
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
==	==	k?	==
</s>
</p>
<p>
<s>
Optimalizace	optimalizace	k1gFnSc1	optimalizace
kódu	kód	k1gInSc2	kód
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
také	také	k9	také
obecně	obecně	k6eAd1	obecně
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
jako	jako	k8xC	jako
platformy	platforma	k1gFnPc1	platforma
závislé	závislý	k2eAgFnPc1d1	závislá
a	a	k8xC	a
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
účinná	účinný	k2eAgFnSc1d1	účinná
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
nebo	nebo	k8xC	nebo
všech	všecek	k3xTgFnPc6	všecek
platformách	platforma	k1gFnPc6	platforma
<g/>
,	,	kIx,	,
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
závislé	závislý	k2eAgFnSc2d1	závislá
techniky	technika	k1gFnSc2	technika
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
specifické	specifický	k2eAgFnPc1d1	specifická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jedné	jeden	k4xCgFnSc2	jeden
platformy	platforma	k1gFnSc2	platforma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
parametry	parametr	k1gInPc4	parametr
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
jediné	jediný	k2eAgFnSc6d1	jediná
platformě	platforma	k1gFnSc6	platforma
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
procesoru	procesor	k1gInSc6	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
nebo	nebo	k8xC	nebo
vyrábění	vyrábění	k1gNnSc1	vyrábění
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
stejného	stejný	k2eAgInSc2d1	stejný
kódu	kód	k1gInSc2	kód
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
procesory	procesor	k1gInPc4	procesor
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
kompilace	kompilace	k1gFnSc2	kompilace
úrovně	úroveň	k1gFnSc2	úroveň
optimalizace	optimalizace	k1gFnSc2	optimalizace
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
techniky	technika	k1gFnSc2	technika
jsou	být	k5eAaImIp3nP	být
obecné	obecný	k2eAgFnPc1d1	obecná
techniky	technika	k1gFnPc1	technika
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
smyčky	smyčka	k1gFnPc1	smyčka
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
volání	volání	k1gNnSc1	volání
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
paměťové	paměťový	k2eAgFnSc2d1	paměťová
efektivní	efektivní	k2eAgFnSc2d1	efektivní
rutiny	rutina	k1gFnSc2	rutina
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
podmínek	podmínka	k1gFnPc2	podmínka
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejvíce	hodně	k6eAd3	hodně
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
CPU	CPU	kA	CPU
architektury	architektura	k1gFnSc2	architektura
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
techniky	technika	k1gFnPc1	technika
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
celkové	celkový	k2eAgFnSc2d1	celková
délky	délka	k1gFnSc2	délka
instrukční	instrukční	k2eAgFnSc2d1	instrukční
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
programu	program	k1gInSc2	program
a	a	k8xC	a
nebo	nebo	k8xC	nebo
snížení	snížení	k1gNnSc1	snížení
celkové	celkový	k2eAgNnSc1d1	celkové
využití	využití	k1gNnSc1	využití
paměti	paměť	k1gFnSc2	paměť
během	během	k7c2	během
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
závislé	závislý	k2eAgFnSc2d1	závislá
techniky	technika	k1gFnSc2	technika
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
plánování	plánování	k1gNnSc4	plánování
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
instrukční	instrukční	k2eAgInSc1d1	instrukční
paralelismus	paralelismus	k1gInSc1	paralelismus
<g/>
,	,	kIx,	,
datový	datový	k2eAgInSc1d1	datový
paralelismus	paralelismus	k1gInSc1	paralelismus
<g/>
,	,	kIx,	,
technologie	technologie	k1gFnSc1	technologie
vyrovnávací	vyrovnávací	k2eAgFnSc2d1	vyrovnávací
paměti	paměť	k1gFnSc2	paměť
optimalizace	optimalizace	k1gFnSc2	optimalizace
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
parametry	parametr	k1gInPc1	parametr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
platformami	platforma	k1gFnPc7	platforma
<g/>
)	)	kIx)	)
a	a	k8xC	a
optimální	optimální	k2eAgNnSc1d1	optimální
plánování	plánování	k1gNnSc1	plánování
instrukcí	instrukce	k1gFnPc2	instrukce
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
odlišné	odlišný	k2eAgNnSc1d1	odlišné
i	i	k9	i
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
procesorech	procesor	k1gInPc6	procesor
stejné	stejný	k2eAgFnSc2d1	stejná
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odlišné	odlišný	k2eAgInPc4d1	odlišný
algoritmy	algoritmus	k1gInPc4	algoritmus
==	==	k?	==
</s>
</p>
<p>
<s>
Výpočetní	výpočetní	k2eAgFnPc1d1	výpočetní
úlohy	úloha	k1gFnPc1	úloha
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
několika	několik	k4yIc7	několik
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
účinností	účinnost	k1gFnSc7	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
například	například	k6eAd1	například
následující	následující	k2eAgInSc4d1	následující
C	C	kA	C
fragment	fragment	k1gInSc4	fragment
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
záměrem	záměr	k1gInSc7	záměr
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
součet	součet	k1gInSc4	součet
všech	všecek	k3xTgNnPc2	všecek
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
N	N	kA	N
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
kód	kód	k1gInSc1	kód
může	moct	k5eAaImIp3nS	moct
(	(	kIx(	(
<g/>
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenastane	nastat	k5eNaPmIp3nS	nastat
aritmetické	aritmetický	k2eAgNnSc4d1	aritmetické
přetečení	přetečení	k1gNnSc4	přetečení
<g/>
)	)	kIx)	)
být	být	k5eAaImF	být
přepsán	přepsat	k5eAaPmNgInS	přepsat
pomocí	pomocí	k7c2	pomocí
matematického	matematický	k2eAgInSc2d1	matematický
vzorce	vzorec	k1gInSc2	vzorec
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Optimalizace	optimalizace	k1gFnSc1	optimalizace
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
provádí	provádět	k5eAaImIp3nS	provádět
automaticky	automaticky	k6eAd1	automaticky
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
optimalizace	optimalizace	k1gFnSc2	optimalizace
kompilátoru	kompilátor	k1gInSc2	kompilátor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vybrat	vybrat	k5eAaPmF	vybrat
metodu	metoda	k1gFnSc4	metoda
(	(	kIx(	(
<g/>
algoritmus	algoritmus	k1gInSc1	algoritmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
výpočetně	výpočetně	k6eAd1	výpočetně
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
,	,	kIx,	,
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
stejné	stejný	k2eAgFnSc2d1	stejná
funkčnosti	funkčnost	k1gFnSc2	funkčnost
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
algoritmická	algoritmický	k2eAgFnSc1d1	algoritmická
účinnost	účinnost	k1gFnSc1	účinnost
k	k	k7c3	k
diskusi	diskuse	k1gFnSc3	diskuse
o	o	k7c6	o
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
technik	technika	k1gFnPc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
významné	významný	k2eAgNnSc1d1	významné
zlepšení	zlepšení	k1gNnSc1	zlepšení
výkonu	výkon	k1gInSc2	výkon
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dalo	dát	k5eAaPmAgNnS	dát
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
odstraní	odstranit	k5eAaPmIp3nP	odstranit
nadbytečné	nadbytečný	k2eAgFnPc4d1	nadbytečná
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optimalizace	optimalizace	k1gFnSc1	optimalizace
není	být	k5eNaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jasný	jasný	k2eAgInSc1d1	jasný
a	a	k8xC	a
intuitivní	intuitivní	k2eAgInSc1d1	intuitivní
proces	proces	k1gInSc1	proces
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedeném	uvedený	k2eAgInSc6d1	uvedený
příkladu	příklad	k1gInSc6	příklad
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
"	"	kIx"	"
<g/>
optimální	optimální	k2eAgFnSc1d1	optimální
<g/>
"	"	kIx"	"
verze	verze	k1gFnSc1	verze
vlastně	vlastně	k9	vlastně
být	být	k5eAaImF	být
pomalejší	pomalý	k2eAgFnSc1d2	pomalejší
než	než	k8xS	než
původní	původní	k2eAgFnSc1d1	původní
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
N	N	kA	N
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
malé	malý	k2eAgNnSc1d1	malé
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
si	se	k3xPyFc3	se
také	také	k9	také
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
předsudky	předsudek	k1gInPc4	předsudek
založené	založený	k2eAgInPc4d1	založený
na	na	k7c6	na
zastaralých	zastaralý	k2eAgFnPc6d1	zastaralá
informacích	informace	k1gFnPc6	informace
jako	jako	k8xS	jako
například	například	k6eAd1	například
stále	stále	k6eAd1	stále
odolávající	odolávající	k2eAgNnSc4d1	odolávající
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
sčítání	sčítání	k1gNnSc1	sčítání
je	být	k5eAaImIp3nS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
na	na	k7c6	na
násobení	násobení	k1gNnSc6	násobení
a	a	k8xC	a
dělení	dělení	k1gNnSc6	dělení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozsah	rozsah	k1gInSc1	rozsah
optimalizací	optimalizace	k1gFnPc2	optimalizace
==	==	k?	==
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
optimalizace	optimalizace	k1gFnSc1	optimalizace
</s>
</p>
<p>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bloku	blok	k1gInSc2	blok
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
skoků	skok	k1gInPc2	skok
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgNnSc1d1	lineární
zpracování	zpracování	k1gNnSc1	zpracování
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nemusíme	muset	k5eNaImIp1nP	muset
znát	znát	k5eAaImF	znát
kontextovou	kontextový	k2eAgFnSc4d1	kontextová
gramatiku	gramatika	k1gFnSc4	gramatika
</s>
</p>
<p>
<s>
Globální	globální	k2eAgFnSc1d1	globální
optimalizace	optimalizace	k1gFnSc1	optimalizace
</s>
</p>
<p>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
podprogramů	podprogram	k1gInPc2	podprogram
(	(	kIx(	(
<g/>
reorganizace	reorganizace	k1gFnSc1	reorganizace
kódu	kód	k1gInSc2	kód
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
procedur	procedura	k1gFnPc2	procedura
<g/>
,	,	kIx,	,
zánik	zánik	k1gInSc1	zánik
částí	část	k1gFnPc2	část
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgInSc2d1	celý
programu	program	k1gInSc2	program
(	(	kIx(	(
<g/>
spojování	spojování	k1gNnSc1	spojování
funkcí	funkce	k1gFnPc2	funkce
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpoužívanější	používaný	k2eAgInPc1d3	nejpoužívanější
jsou	být	k5eAaImIp3nP	být
pospolu	pospolu	k6eAd1	pospolu
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Optimalizace	optimalizace	k1gFnSc1	optimalizace
cyklů	cyklus	k1gInPc2	cyklus
==	==	k?	==
</s>
</p>
<p>
<s>
Analýza	analýza	k1gFnSc1	analýza
indukčních	indukční	k2eAgFnPc2d1	indukční
proměnných	proměnná	k1gFnPc2	proměnná
</s>
</p>
<p>
<s>
Indukční	indukční	k2eAgFnPc1d1	indukční
proměnné	proměnná	k1gFnPc1	proměnná
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc1	takový
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
iteraci	iterace	k1gFnSc6	iterace
cyklu	cyklus	k1gInSc2	cyklus
změní	změnit	k5eAaPmIp3nS	změnit
o	o	k7c4	o
konstantní	konstantní	k2eAgFnSc4d1	konstantní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výpočet	výpočet	k1gInSc4	výpočet
proměnné	proměnná	k1gFnSc2	proměnná
j	j	k?	j
<g/>
:	:	kIx,	:
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
*	*	kIx~	*
<g/>
i	i	k9	i
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
i	i	k9	i
je	být	k5eAaImIp3nS	být
číslo	číslo	k1gNnSc4	číslo
iterace	iterace	k1gFnSc2	iterace
<g/>
.	.	kIx.	.
</s>
<s>
Náročná	náročný	k2eAgFnSc1d1	náročná
operace	operace	k1gFnSc1	operace
násobení	násobení	k1gNnSc2	násobení
zde	zde	k6eAd1	zde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
rychlejším	rychlý	k2eAgNnSc7d2	rychlejší
postupným	postupný	k2eAgNnSc7d1	postupné
přičítáním	přičítání	k1gNnSc7	přičítání
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
iteraci	iterace	k1gFnSc6	iterace
<g/>
.	.	kIx.	.
<g/>
Štěpení	štěpení	k1gNnSc1	štěpení
cyklů	cyklus	k1gInPc2	cyklus
</s>
</p>
<p>
<s>
Při	při	k7c6	při
štěpení	štěpení	k1gNnSc6	štěpení
cyklů	cyklus	k1gInPc2	cyklus
se	se	k3xPyFc4	se
překladač	překladač	k1gInSc1	překladač
snaží	snažit	k5eAaImIp3nS	snažit
rozdělit	rozdělit	k5eAaPmF	rozdělit
cykly	cyklus	k1gInPc4	cyklus
na	na	k7c4	na
několik	několik	k4yIc4	několik
jednodušších	jednoduchý	k2eAgFnPc2d2	jednodušší
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každý	každý	k3xTgMnSc1	každý
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
těla	tělo	k1gNnSc2	tělo
původního	původní	k2eAgInSc2d1	původní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
lze	lze	k6eAd1	lze
optimalizovat	optimalizovat	k5eAaBmF	optimalizovat
lokální	lokální	k2eAgFnPc4d1	lokální
proměnné	proměnná	k1gFnPc4	proměnná
i	i	k8xC	i
zpracovávaných	zpracovávaný	k2eAgNnPc2d1	zpracovávané
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
<g/>
Slučování	slučování	k1gNnPc2	slučování
cyklů	cyklus	k1gInPc2	cyklus
</s>
</p>
<p>
<s>
Opak	opak	k1gInSc1	opak
štěpení	štěpení	k1gNnSc2	štěpení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
těl	tělo	k1gNnPc2	tělo
dvou	dva	k4xCgInPc2	dva
cyklů	cyklus	k1gInPc2	cyklus
se	s	k7c7	s
stejným	stejný	k2eAgInSc7d1	stejný
rozsahem	rozsah	k1gInSc7	rozsah
hodnot	hodnota	k1gFnPc2	hodnota
řídící	řídící	k2eAgFnSc2d1	řídící
proměnné	proměnná	k1gFnSc2	proměnná
(	(	kIx(	(
<g/>
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
rozsah	rozsah	k1gInSc4	rozsah
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
při	při	k7c6	při
kompilaci	kompilace	k1gFnSc6	kompilace
známý	známý	k1gMnSc1	známý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
lze	lze	k6eAd1	lze
redukovat	redukovat	k5eAaBmF	redukovat
režijní	režijní	k2eAgInPc4d1	režijní
náklady	náklad	k1gInPc4	náklad
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
<g/>
Zaměňování	zaměňování	k1gNnSc1	zaměňování
cyklů	cyklus	k1gInPc2	cyklus
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
technice	technika	k1gFnSc6	technika
se	se	k3xPyFc4	se
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
vnější	vnější	k2eAgInPc1d1	vnější
a	a	k8xC	a
vnořené	vnořený	k2eAgInPc4d1	vnořený
cykly	cyklus	k1gInPc4	cyklus
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zlepšena	zlepšen	k2eAgFnSc1d1	zlepšena
lokálnost	lokálnost	k1gFnSc1	lokálnost
proměnných	proměnná	k1gFnPc2	proměnná
nebo	nebo	k8xC	nebo
zpracovávaných	zpracovávaný	k2eAgNnPc2d1	zpracovávané
dat	datum	k1gNnPc2	datum
a	a	k8xC	a
lépe	dobře	k6eAd2	dobře
využívána	využívat	k5eAaImNgFnS	využívat
cache	cache	k1gFnSc7	cache
procesoru	procesor	k1gInSc2	procesor
<g/>
.	.	kIx.	.
<g/>
Invariant	invariant	k1gInSc1	invariant
cyklu	cyklus	k1gInSc2	cyklus
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc4	nějaký
výraz	výraz	k1gInSc4	výraz
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
přepočítáván	přepočítáván	k2eAgMnSc1d1	přepočítáván
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
iteraci	iterace	k1gFnSc6	iterace
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
má	mít	k5eAaImIp3nS	mít
konstantní	konstantní	k2eAgFnSc4d1	konstantní
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gMnSc4	on
vyhodnotit	vyhodnotit	k5eAaPmF	vyhodnotit
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
výpočet	výpočet	k1gInSc4	výpočet
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
<g/>
Rozvinutí	rozvinutí	k1gNnSc2	rozvinutí
cyklu	cyklus	k1gInSc2	cyklus
</s>
</p>
<p>
<s>
Tělo	tělo	k1gNnSc1	tělo
původního	původní	k2eAgInSc2d1	původní
cyklu	cyklus	k1gInSc2	cyklus
se	se	k3xPyFc4	se
provede	provést	k5eAaPmIp3nS	provést
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
iteraci	iterace	k1gFnSc6	iterace
několikrát	několikrát	k6eAd1	několikrát
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
iterací	iterace	k1gFnPc2	iterace
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
režijních	režijní	k2eAgMnPc2d1	režijní
nákladů	náklad	k1gInPc2	náklad
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
tak	tak	k6eAd1	tak
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
počtu	počet	k1gInSc2	počet
skoků	skok	k1gInPc2	skok
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
využívá	využívat	k5eAaPmIp3nS	využívat
pipeline	pipelin	k1gInSc5	pipelin
procesoru	procesor	k1gInSc3	procesor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
úplné	úplný	k2eAgNnSc4d1	úplné
rozvinutí	rozvinutí	k1gNnSc4	rozvinutí
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
znát	znát	k5eAaImF	znát
počet	počet	k1gInSc4	počet
iterací	iterace	k1gFnPc2	iterace
již	již	k6eAd1	již
při	při	k7c6	při
kompilaci	kompilace	k1gFnSc6	kompilace
<g/>
.	.	kIx.	.
<g/>
Unswitching	Unswitching	k1gInSc1	Unswitching
</s>
</p>
<p>
<s>
Unswitching	Unswitching	k1gInSc1	Unswitching
je	být	k5eAaImIp3nS	být
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
kompilátor	kompilátor	k1gMnSc1	kompilátor
snaží	snažit	k5eAaImIp3nS	snažit
odstranit	odstranit	k5eAaPmF	odstranit
podmíněné	podmíněný	k2eAgInPc4d1	podmíněný
skoky	skok	k1gInPc4	skok
uvnitř	uvnitř	k7c2	uvnitř
těla	tělo	k1gNnSc2	tělo
cyklu	cyklus	k1gInSc2	cyklus
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
přesune	přesunout	k5eAaPmIp3nS	přesunout
vně	vně	k7c2	vně
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
tělo	tělo	k1gNnSc4	tělo
cyklu	cyklus	k1gInSc2	cyklus
poté	poté	k6eAd1	poté
duplikuje	duplikovat	k5eAaBmIp3nS	duplikovat
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
větve	větev	k1gFnSc2	větev
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Podmínka	podmínka	k1gFnSc1	podmínka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS	vyhodnocovat
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
<g/>
Automatická	automatický	k2eAgFnSc1d1	automatická
paralelizace	paralelizace	k1gFnSc1	paralelizace
</s>
</p>
<p>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
je	být	k5eAaImIp3nS	být
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
vícevláknový	vícevláknový	k2eAgInSc4d1	vícevláknový
nebo	nebo	k8xC	nebo
vektorizovaný	vektorizovaný	k2eAgInSc4d1	vektorizovaný
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
efektivnější	efektivní	k2eAgInSc4d2	efektivnější
výpočet	výpočet	k1gInSc4	výpočet
na	na	k7c6	na
víceprocesorových	víceprocesorový	k2eAgInPc6d1	víceprocesorový
počítačích	počítač	k1gInPc6	počítač
se	s	k7c7	s
sdílenou	sdílený	k2eAgFnSc7d1	sdílená
pamětí	paměť	k1gFnSc7	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Analýza	analýza	k1gFnSc1	analýza
toku	tok	k1gInSc2	tok
dat	datum	k1gNnPc2	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Eliminace	eliminace	k1gFnSc1	eliminace
společných	společný	k2eAgInPc2d1	společný
podvýrazů	podvýraz	k1gInPc2	podvýraz
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
výrazy	výraz	k1gInPc1	výraz
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
opakující	opakující	k2eAgInPc1d1	opakující
se	se	k3xPyFc4	se
podvýrazy	podvýraz	k1gInPc7	podvýraz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
není	být	k5eNaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
počítat	počítat	k5eAaImF	počítat
vždy	vždy	k6eAd1	vždy
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
výrazu	výraz	k1gInSc6	výraz
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
se	se	k3xPyFc4	se
podvýraz	podvýraz	k1gInSc1	podvýraz
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
během	během	k7c2	během
výpočtu	výpočet	k1gInSc2	výpočet
nemění	měnit	k5eNaImIp3nS	měnit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gNnSc4	on
vypočítat	vypočítat	k5eAaPmF	vypočítat
pouze	pouze	k6eAd1	pouze
jednou	jednou	k6eAd1	jednou
<g/>
.	.	kIx.	.
<g/>
Výpočet	výpočet	k1gInSc1	výpočet
konstantních	konstantní	k2eAgInPc2d1	konstantní
výrazů	výraz	k1gInPc2	výraz
</s>
</p>
<p>
<s>
Konstantní	konstantní	k2eAgInPc1d1	konstantní
výrazy	výraz	k1gInPc1	výraz
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
již	již	k6eAd1	již
během	během	k7c2	během
překladu	překlad	k1gInSc2	překlad
<g/>
,	,	kIx,	,
nahradit	nahradit	k5eAaPmF	nahradit
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
kódu	kód	k1gInSc6	kód
jejich	jejich	k3xOp3gFnSc7	jejich
výslednou	výsledný	k2eAgFnSc7d1	výsledná
hodnotou	hodnota	k1gFnSc7	hodnota
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ušetřit	ušetřit	k5eAaPmF	ušetřit
čas	čas	k1gInSc4	čas
při	při	k7c6	při
běhu	běh	k1gInSc6	běh
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
<g/>
Analýza	analýza	k1gFnSc1	analýza
ukazatelů	ukazatel	k1gMnPc2	ukazatel
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
kódu	kód	k1gInSc6	kód
použity	použit	k2eAgInPc4d1	použit
ukazatele	ukazatel	k1gInPc4	ukazatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
ho	on	k3xPp3gMnSc4	on
optimalizovat	optimalizovat	k5eAaBmF	optimalizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
pomocí	pomocí	k7c2	pomocí
ukazatele	ukazatel	k1gInSc2	ukazatel
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
libovolné	libovolný	k2eAgFnSc2d1	libovolná
proměnné	proměnná	k1gFnSc2	proměnná
uložené	uložený	k2eAgFnSc2d1	uložená
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
zápisu	zápis	k1gInSc2	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
ukazatelů	ukazatel	k1gMnPc2	ukazatel
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
snaží	snažit	k5eAaImIp3nS	snažit
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ukazatele	ukazatel	k1gInPc1	ukazatel
a	a	k8xC	a
proměnné	proměnná	k1gFnPc1	proměnná
operují	operovat	k5eAaImIp3nP	operovat
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
pamětí	paměť	k1gFnSc7	paměť
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
pak	pak	k9	pak
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
optimalizaci	optimalizace	k1gFnSc6	optimalizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Optimalizace	optimalizace	k1gFnSc1	optimalizace
při	při	k7c6	při
generování	generování	k1gNnSc6	generování
kódu	kód	k1gInSc2	kód
==	==	k?	==
</s>
</p>
<p>
<s>
Alokace	alokace	k1gFnSc1	alokace
registrů	registr	k1gInPc2	registr
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
používané	používaný	k2eAgFnPc1d1	používaná
proměnné	proměnná	k1gFnPc1	proměnná
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
registrech	registr	k1gInPc6	registr
procesoru	procesor	k1gInSc2	procesor
kvůli	kvůli	k7c3	kvůli
rychlému	rychlý	k2eAgInSc3d1	rychlý
přístupu	přístup	k1gInSc3	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
platnosti	platnost	k1gFnPc1	platnost
se	se	k3xPyFc4	se
vzájemně	vzájemně	k6eAd1	vzájemně
překrývají	překrývat	k5eAaImIp3nP	překrývat
(	(	kIx(	(
<g/>
existují	existovat	k5eAaImIp3nP	existovat
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
současně	současně	k6eAd1	současně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
graf	graf	k1gInSc1	graf
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
uzly	uzel	k1gInPc4	uzel
představují	představovat	k5eAaImIp3nP	představovat
proměnné	proměnná	k1gFnPc1	proměnná
a	a	k8xC	a
hrany	hrana	k1gFnPc1	hrana
označují	označovat	k5eAaImIp3nP	označovat
překrývání	překrývání	k1gNnSc4	překrývání
platností	platnost	k1gFnPc2	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Uzly	uzel	k1gInPc1	uzel
tohoto	tento	k3xDgInSc2	tento
grafu	graf	k1gInSc2	graf
jsou	být	k5eAaImIp3nP	být
poté	poté	k6eAd1	poté
obarveny	obarven	k2eAgFnPc1d1	obarvena
nějakým	nějaký	k3yIgInSc7	nějaký
algoritmem	algoritmus	k1gInSc7	algoritmus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
počet	počet	k1gInSc1	počet
použitých	použitý	k2eAgFnPc2d1	použitá
barev	barva	k1gFnPc2	barva
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc2	počet
registrů	registr	k1gInPc2	registr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
algoritmus	algoritmus	k1gInSc1	algoritmus
barvení	barvení	k1gNnSc2	barvení
selže	selhat	k5eAaPmIp3nS	selhat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
proměnná	proměnná	k1gFnSc1	proměnná
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
z	z	k7c2	z
grafu	graf	k1gInSc2	graf
<g/>
,	,	kIx,	,
přemístěna	přemístěn	k2eAgFnSc1d1	přemístěna
do	do	k7c2	do
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
algoritmus	algoritmus	k1gInSc1	algoritmus
zopakován	zopakován	k2eAgInSc1d1	zopakován
<g/>
.	.	kIx.	.
<g/>
Výběr	výběr	k1gInSc1	výběr
instrukcí	instrukce	k1gFnPc2	instrukce
</s>
</p>
<p>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
procesorových	procesorový	k2eAgFnPc2d1	procesorová
architektur	architektura	k1gFnPc2	architektura
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
CISC	CISC	kA	CISC
a	a	k8xC	a
takových	takový	k3xDgMnPc2	takový
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
mnoho	mnoho	k4c4	mnoho
adresových	adresový	k2eAgInPc2d1	adresový
módů	mód	k1gInPc2	mód
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
možné	možný	k2eAgInPc1d1	možný
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
operaci	operace	k1gFnSc4	operace
provést	provést	k5eAaPmF	provést
různými	různý	k2eAgInPc7d1	různý
sekvencemi	sekvence	k1gFnPc7	sekvence
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
.	.	kIx.	.
</s>
<s>
Generátor	generátor	k1gInSc1	generátor
kódu	kód	k1gInSc2	kód
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
snaží	snažit	k5eAaImIp3nP	snažit
vybrat	vybrat	k5eAaPmF	vybrat
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
operátor	operátor	k1gInSc4	operátor
tyto	tento	k3xDgFnPc4	tento
instrukce	instrukce	k1gFnPc4	instrukce
co	co	k9	co
nejvhodněji	vhodně	k6eAd3	vhodně
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
požadavcích	požadavek	k1gInPc6	požadavek
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Plánování	plánování	k1gNnSc1	plánování
instrukcí	instrukce	k1gFnPc2	instrukce
</s>
</p>
<p>
<s>
Plánování	plánování	k1gNnSc1	plánování
instrukcí	instrukce	k1gFnPc2	instrukce
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
správné	správný	k2eAgNnSc4d1	správné
zřetězení	zřetězení	k1gNnSc4	zřetězení
a	a	k8xC	a
efektivní	efektivní	k2eAgNnSc4d1	efektivní
využití	využití	k1gNnSc4	využití
pipeline	pipelin	k1gInSc5	pipelin
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
nevznikají	vznikat	k5eNaImIp3nP	vznikat
prázdná	prázdný	k2eAgNnPc1d1	prázdné
místa	místo	k1gNnPc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
docíleno	docílen	k2eAgNnSc1d1	docíleno
shlukováním	shlukování	k1gNnSc7	shlukování
instrukcí	instrukce	k1gFnPc2	instrukce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
závislé	závislý	k2eAgInPc1d1	závislý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vždy	vždy	k6eAd1	vždy
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dodržena	dodržet	k5eAaPmNgFnS	dodržet
sémantika	sémantika	k1gFnSc1	sémantika
původního	původní	k2eAgInSc2d1	původní
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
<g/>
Opakovaný	opakovaný	k2eAgInSc1d1	opakovaný
výpočet	výpočet	k1gInSc1	výpočet
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
výpočet	výpočet	k1gInSc4	výpočet
nějaké	nějaký	k3yIgFnSc2	nějaký
hodnoty	hodnota	k1gFnSc2	hodnota
efektivnějším	efektivní	k2eAgInSc7d2	efektivnější
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
než	než	k8xS	než
její	její	k3xOp3gNnSc4	její
načtení	načtení	k1gNnSc4	načtení
z	z	k7c2	z
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
přenášení	přenášení	k1gNnSc2	přenášení
proměnných	proměnná	k1gFnPc2	proměnná
z	z	k7c2	z
registrů	registr	k1gInPc2	registr
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Compiler	Compiler	k1gInSc1	Compiler
optimization	optimization	k1gInSc1	optimization
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
Paula	Paul	k1gMnSc2	Paul
Hsiehe	Hsieh	k1gMnSc2	Hsieh
o	o	k7c6	o
optimalizaci	optimalizace	k1gFnSc6	optimalizace
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
příklady	příklad	k1gInPc4	příklad
některých	některý	k3yIgFnPc2	některý
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
optimalizací	optimalizace	k1gFnPc2	optimalizace
v	v	k7c6	v
C	C	kA	C
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
