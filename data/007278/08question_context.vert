<s>
Typická	typický	k2eAgFnSc1d1
celková	celkový	k2eAgFnSc1d1
doba	doba	k1gFnSc1
spánku	spánek	k1gInSc2
za	za	k7c4
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
je	být	k5eAaImIp3nS
u	u	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
živočichů	živočich	k1gMnPc2
velmi	velmi	k6eAd1
odlišná	odlišný	k2eAgFnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
přibližně	přibližně	k6eAd1
2	[number]	k4
hodin	hodina	k1gFnPc2
až	až	k9
po	po	k7c4
20	[number]	k4
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>