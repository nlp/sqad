<p>
<s>
Kermadecký	Kermadecký	k2eAgInSc1d1	Kermadecký
příkop	příkop	k1gInSc1	příkop
je	být	k5eAaImIp3nS	být
hlubokomořský	hlubokomořský	k2eAgInSc1d1	hlubokomořský
příkop	příkop	k1gInSc1	příkop
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
zhruba	zhruba	k6eAd1	zhruba
150	[number]	k4	150
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Kermadeckého	Kermadecký	k2eAgInSc2d1	Kermadecký
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
trojmezí	trojmezí	k1gNnSc2	trojmezí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stýká	stýkat	k5eAaImIp3nS	stýkat
s	s	k7c7	s
Tonžským	Tonžský	k2eAgInSc7d1	Tonžský
příkopem	příkop	k1gInSc7	příkop
a	a	k8xC	a
Lousvilleským	Lousvilleský	k2eAgInSc7d1	Lousvilleský
hřbetem	hřbet	k1gInSc7	hřbet
<g/>
,	,	kIx,	,
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
jihojihozápadu	jihojihozápad	k1gInSc3	jihojihozápad
a	a	k8xC	a
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
se	se	k3xPyFc4	se
stáčí	stáčet	k5eAaImIp3nS	stáčet
k	k	k7c3	k
jihojihozápadu	jihojihozápad	k1gInSc3	jihojihozápad
k	k	k7c3	k
Severnímu	severní	k2eAgInSc3d1	severní
ostrovu	ostrov	k1gInSc3	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
1270	[number]	k4	1270
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Tonžský	Tonžský	k2eAgInSc4d1	Tonžský
příkop	příkop	k1gInSc4	příkop
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
Creanově	Creanův	k2eAgFnSc6d1	Creanův
hlubině	hlubina	k1gFnSc6	hlubina
(	(	kIx(	(
<g/>
Crean	Crean	k1gInSc1	Crean
Deep	Deep	k1gInSc1	Deep
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgInSc1d1	ležící
mezi	mezi	k7c7	mezi
plošinou	plošina	k1gFnSc7	plošina
Hikurangi	Hikurangi	k1gNnSc2	Hikurangi
a	a	k8xC	a
hřbetem	hřbet	k1gInSc7	hřbet
East	East	k1gInSc1	East
Cape	capat	k5eAaImIp3nS	capat
(	(	kIx(	(
<g/>
East	East	k2eAgInSc1d1	East
Cape	capat	k5eAaImIp3nS	capat
Ridge	Ridge	k1gInSc1	Ridge
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnSc2	hloubka
10	[number]	k4	10
047	[number]	k4	047
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tichomořská	tichomořský	k2eAgFnSc1d1	tichomořská
deska	deska	k1gFnSc1	deska
se	se	k3xPyFc4	se
v	v	k7c6	v
Kemradeckém	Kemradecký	k2eAgInSc6d1	Kemradecký
příkopu	příkop	k1gInSc6	příkop
podsunuje	podsunovat	k5eAaImIp3nS	podsunovat
pod	pod	k7c4	pod
australskou	australský	k2eAgFnSc4d1	australská
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
kermadecko-tonžského	kermadeckoonžský	k2eAgInSc2d1	kermadecko-tonžský
subdukčního	subdukční	k2eAgInSc2d1	subdukční
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
subdukční	subdukční	k2eAgFnSc4d1	subdukční
zónu	zóna	k1gFnSc4	zóna
jsou	být	k5eAaImIp3nP	být
vázána	vázán	k2eAgNnPc1d1	vázáno
zemětřesení	zemětřesení	k1gNnPc1	zemětřesení
a	a	k8xC	a
tsunami	tsunami	k1gNnPc1	tsunami
<g/>
.	.	kIx.	.
</s>
</p>
