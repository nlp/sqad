<s>
Bývá	bývat	k5eAaImIp3nS	bývat
umístěno	umístit	k5eAaPmNgNnS	umístit
docela	docela	k6eAd1	docela
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
mezi	mezi	k7c7	mezi
hustými	hustý	k2eAgFnPc7d1	hustá
větvemi	větev	k1gFnPc7	větev
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
v	v	k7c6	v
bylinách	bylina	k1gFnPc6	bylina
<g/>
,	,	kIx,	,
v	v	k7c6	v
hromádce	hromádka	k1gFnSc6	hromádka
roští	roští	k1gNnSc2	roští
<g/>
,	,	kIx,	,
v	v	k7c6	v
hromadě	hromada	k1gFnSc6	hromada
suchého	suchý	k2eAgNnSc2d1	suché
listí	listí	k1gNnSc2	listí
nebo	nebo	k8xC	nebo
v	v	k7c6	v
trsu	trs	k1gInSc6	trs
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
