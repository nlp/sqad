<p>
<s>
Slavík	Slavík	k1gMnSc1	Slavík
tmavý	tmavý	k2eAgMnSc1d1	tmavý
(	(	kIx(	(
<g/>
Luscinia	Luscinium	k1gNnSc2	Luscinium
luscinia	luscinium	k1gNnSc2	luscinium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lejskovitých	lejskovitý	k2eAgFnPc2d1	lejskovitý
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Shora	shora	k6eAd1	shora
tmavěji	tmavě	k6eAd2	tmavě
hnědý	hnědý	k2eAgInSc1d1	hnědý
než	než	k8xS	než
velmi	velmi	k6eAd1	velmi
podoný	podoný	k2eAgInSc1d1	podoný
slavík	slavík	k1gInSc1	slavík
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
nevýrazně	výrazně	k6eNd1	výrazně
rezavohnědý	rezavohnědý	k2eAgInSc1d1	rezavohnědý
<g/>
,	,	kIx,	,
na	na	k7c6	na
prsou	prsa	k1gNnPc6	prsa
i	i	k8xC	i
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
šedohnědavém	šedohnědavý	k2eAgInSc6d1	šedohnědavý
podkladu	podklad	k1gInSc6	podklad
tmavěji	tmavě	k6eAd2	tmavě
nevýrazně	výrazně	k6eNd1	výrazně
skvrněný	skvrněný	k2eAgInSc1d1	skvrněný
a	a	k8xC	a
vlnkovaný	vlnkovaný	k2eAgInSc1d1	vlnkovaný
<g/>
,	,	kIx,	,
na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
bělavý	bělavý	k2eAgInSc1d1	bělavý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
nevýrazný	výrazný	k2eNgInSc4d1	nevýrazný
oční	oční	k2eAgInSc4d1	oční
kroužek	kroužek	k1gInSc4	kroužek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpěv	zpěv	k1gInSc1	zpěv
==	==	k?	==
</s>
</p>
<p>
<s>
Slavík	slavík	k1gInSc1	slavík
tmavý	tmavý	k2eAgInSc1d1	tmavý
je	být	k5eAaImIp3nS	být
výborným	výborný	k2eAgMnSc7d1	výborný
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
,	,	kIx,	,
rozhodně	rozhodně	k6eAd1	rozhodně
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
slavík	slavík	k1gInSc1	slavík
obecný	obecný	k2eAgInSc1d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
mu	on	k3xPp3gMnSc3	on
totiž	totiž	k9	totiž
chybí	chybit	k5eAaPmIp3nS	chybit
úvodní	úvodní	k2eAgInPc4d1	úvodní
táhlé	táhlý	k2eAgInPc4d1	táhlý
hvizdy	hvizd	k1gInPc4	hvizd
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
typickým	typický	k2eAgInSc7d1	typický
trylkovitý	trylkovitý	k2eAgInSc1d1	trylkovitý
tlukot	tlukot	k1gInSc1	tlukot
slavíka	slavík	k1gMnSc2	slavík
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
,,	,,	k?	,,
<g/>
srrr	srrr	k1gInSc1	srrr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
téměř	téměř	k6eAd1	téměř
všech	všecek	k3xTgMnPc2	všecek
samců	samec	k1gMnPc2	samec
slavíka	slavík	k1gMnSc2	slavík
tmavého	tmavý	k2eAgMnSc2d1	tmavý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tón	tón	k1gInSc1	tón
hlasu	hlas	k1gInSc2	hlas
je	být	k5eAaImIp3nS	být
hlubší	hluboký	k2eAgInSc1d2	hlubší
a	a	k8xC	a
dojem	dojem	k1gInSc1	dojem
budí	budit	k5eAaImIp3nS	budit
vyšší	vysoký	k2eAgFnSc7d2	vyšší
hlasitostí	hlasitost	k1gFnSc7	hlasitost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
připomíná	připomínat	k5eAaImIp3nS	připomínat
drozdy	drozd	k1gMnPc4	drozd
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
klidných	klidný	k2eAgFnPc2d1	klidná
nocí	noc	k1gFnPc2	noc
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
na	na	k7c4	na
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Slavík	Slavík	k1gMnSc1	Slavík
tmavý	tmavý	k2eAgMnSc1d1	tmavý
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
příbuzného	příbuzný	k2eAgMnSc4d1	příbuzný
slavíka	slavík	k1gMnSc4	slavík
obecného	obecný	k2eAgMnSc4d1	obecný
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
slavík	slavík	k1gInSc1	slavík
tmavý	tmavý	k2eAgInSc1d1	tmavý
nehnízdí	hnízdit	k5eNaImIp3nS	hnízdit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
známa	známo	k1gNnPc4	známo
jen	jen	k6eAd1	jen
ojedinělá	ojedinělý	k2eAgNnPc4d1	ojedinělé
data	datum	k1gNnPc4	datum
z	z	k7c2	z
hnízdního	hnízdní	k2eAgNnSc2d1	hnízdní
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
částech	část	k1gFnPc6	část
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
nevelký	velký	k2eNgInSc1d1	nevelký
areál	areál	k1gInSc1	areál
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
také	také	k9	také
východnější	východní	k2eAgFnPc4d2	východnější
oblasti	oblast	k1gFnPc4	oblast
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Tažný	tažný	k2eAgInSc1d1	tažný
druh	druh	k1gInSc1	druh
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Pobřežní	pobřežní	k2eAgFnPc1d1	pobřežní
houštiny	houština	k1gFnPc1	houština
řek	řeka	k1gFnPc2	řeka
s	s	k7c7	s
hustým	hustý	k2eAgNnSc7d1	husté
bylinným	bylinný	k2eAgNnSc7d1	bylinné
patrem	patro	k1gNnSc7	patro
<g/>
,	,	kIx,	,
zaplavované	zaplavovaný	k2eAgInPc4d1	zaplavovaný
porosty	porost	k1gInPc4	porost
vrb	vrba	k1gFnPc2	vrba
kolem	kolem	k7c2	kolem
potoků	potok	k1gInPc2	potok
a	a	k8xC	a
vlhké	vlhký	k2eAgInPc1d1	vlhký
lesíky	lesík	k1gInPc1	lesík
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tahu	tah	k1gInSc6	tah
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Našim	náš	k3xOp1gMnPc3	náš
uzemím	uzemit	k5eAaPmIp1nS	uzemit
pravidelně	pravidelně	k6eAd1	pravidelně
protahuje	protahovat	k5eAaImIp3nS	protahovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hnízdění	hnízdění	k1gNnSc2	hnízdění
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
slavíka	slavík	k1gInSc2	slavík
tmavého	tmavý	k2eAgInSc2d1	tmavý
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
neuměle	uměle	k6eNd1	uměle
zhotovené	zhotovený	k2eAgNnSc1d1	zhotovené
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
spleteno	spleten	k2eAgNnSc1d1	spleteno
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
kořínků	kořínek	k1gInPc2	kořínek
<g/>
,	,	kIx,	,
mechu	mech	k1gInSc2	mech
a	a	k8xC	a
suchého	suchý	k2eAgNnSc2d1	suché
listí	listí	k1gNnSc2	listí
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
umístěno	umístit	k5eAaPmNgNnS	umístit
docela	docela	k6eAd1	docela
nízko	nízko	k6eAd1	nízko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
mezi	mezi	k7c7	mezi
hustými	hustý	k2eAgFnPc7d1	hustá
větvemi	větev	k1gFnPc7	větev
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
v	v	k7c6	v
bylinách	bylina	k1gFnPc6	bylina
<g/>
,	,	kIx,	,
v	v	k7c6	v
hromádce	hromádka	k1gFnSc6	hromádka
roští	roští	k1gNnSc2	roští
<g/>
,	,	kIx,	,
v	v	k7c6	v
hromadě	hromada	k1gFnSc6	hromada
suchého	suchý	k2eAgNnSc2d1	suché
listí	listí	k1gNnSc2	listí
nebo	nebo	k8xC	nebo
v	v	k7c6	v
trsu	trs	k1gInSc6	trs
trávy	tráva	k1gFnSc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Vajíček	vajíčko	k1gNnPc2	vajíčko
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
4	[number]	k4	4
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nazelenalá	nazelenalý	k2eAgFnSc1d1	nazelenalá
nebo	nebo	k8xC	nebo
hnědá	hnědý	k2eAgFnSc1d1	hnědá
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
13	[number]	k4	13
až	až	k9	až
14	[number]	k4	14
denní	denní	k2eAgFnSc2d1	denní
inkubace	inkubace	k1gFnSc2	inkubace
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
na	na	k7c6	na
sezení	sezení	k1gNnSc6	sezení
nepodílí	podílet	k5eNaImIp3nS	podílet
<g/>
,	,	kIx,	,
sedí	sedit	k5eAaImIp3nS	sedit
pouze	pouze	k6eAd1	pouze
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
však	však	k9	však
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
samec	samec	k1gMnSc1	samec
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
krmení	krmení	k1gNnSc6	krmení
<g/>
.	.	kIx.	.
</s>
<s>
Nosí	nosit	k5eAaImIp3nS	nosit
jim	on	k3xPp3gMnPc3	on
různé	různý	k2eAgFnPc4d1	různá
červy	červ	k1gMnPc7	červ
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnPc4	housenka
<g/>
,	,	kIx,	,
larvy	larva	k1gFnPc1	larva
brouků	brouk	k1gMnPc2	brouk
nebo	nebo	k8xC	nebo
měkkýše	měkkýš	k1gMnSc2	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
12	[number]	k4	12
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
mláďata	mládě	k1gNnPc4	mládě
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
rozbíhají	rozbíhat	k5eAaImIp3nP	rozbíhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
létat	létat	k5eAaImF	létat
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
až	až	k9	až
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
společného	společný	k2eAgInSc2d1	společný
výskytu	výskyt	k1gInSc2	výskyt
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
kříží	křížit	k5eAaImIp3nP	křížit
se	s	k7c7	s
slavíkem	slavík	k1gInSc7	slavík
obecným	obecný	k2eAgInSc7d1	obecný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
příčiny	příčina	k1gFnPc4	příčina
ohrožení	ohrožení	k1gNnSc2	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sekundární	sekundární	k2eAgMnSc1d1	sekundární
konzument	konzument	k1gMnSc1	konzument
drobných	drobný	k2eAgMnPc2d1	drobný
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
ekosystémech	ekosystém	k1gInPc6	ekosystém
vlhkých	vlhký	k2eAgInPc2d1	vlhký
břehových	břehový	k2eAgInPc2d1	břehový
porostů	porost	k1gInPc2	porost
<g/>
.	.	kIx.	.
</s>
<s>
Esteticky	esteticky	k6eAd1	esteticky
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
svým	svůj	k3xOyFgInSc7	svůj
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
lovil	lovit	k5eAaImAgMnS	lovit
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
v	v	k7c6	v
klecích	klec	k1gFnPc6	klec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
význačný	význačný	k2eAgInSc1d1	význačný
z	z	k7c2	z
kulturního	kulturní	k2eAgNnSc2d1	kulturní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
,	,	kIx,	,
vědecky	vědecky	k6eAd1	vědecky
je	být	k5eAaImIp3nS	být
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
svým	svůj	k3xOyFgNnSc7	svůj
rozšířením	rozšíření	k1gNnSc7	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
populace	populace	k1gFnPc1	populace
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
odchytem	odchyt	k1gInSc7	odchyt
zpívajících	zpívající	k2eAgInPc2d1	zpívající
samců	samec	k1gInPc2	samec
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
ohroženy	ohrozit	k5eAaPmNgFnP	ohrozit
především	především	k9	především
likvidací	likvidace	k1gFnSc7	likvidace
břehových	břehový	k2eAgInPc2d1	břehový
porostů	porost	k1gInPc2	porost
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
vodotečí	vodoteč	k1gFnPc2	vodoteč
a	a	k8xC	a
změnami	změna	k1gFnPc7	změna
prostředí	prostředí	k1gNnSc2	prostředí
na	na	k7c6	na
zimovištích	zimoviště	k1gNnPc6	zimoviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
slavík	slavík	k1gMnSc1	slavík
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
slavík	slavík	k1gMnSc1	slavík
tmavý	tmavý	k2eAgMnSc1d1	tmavý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Luscinia	Luscinium	k1gNnSc2	Luscinium
luscinia	luscinium	k1gNnSc2	luscinium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
