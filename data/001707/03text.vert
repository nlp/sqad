<s>
The	The	k?	The
Doors	Doors	k1gInSc1	Doors
[	[	kIx(	[
<g/>
ð	ð	k?	ð
dɔ	dɔ	k?	dɔ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
The	The	k1gMnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
of	of	k?	of
Perception	Perception	k1gInSc1	Perception
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
dveře	dveře	k1gFnPc4	dveře
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
,	,	kIx,	,
překládáno	překládán	k2eAgNnSc4d1	překládáno
jako	jako	k8xC	jako
Brány	brána	k1gFnSc2	brána
vnímání	vnímání	k1gNnSc2	vnímání
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Patřila	patřit	k5eAaImAgFnS	patřit
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
skupinám	skupina	k1gFnPc3	skupina
zejména	zejména	k9	zejména
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
The	The	k1gFnSc2	The
Doors	Doors	k1gInSc1	Doors
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
klávesové	klávesový	k2eAgInPc4d1	klávesový
nástroje	nástroj	k1gInPc4	nástroj
Ray	Ray	k1gFnSc2	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gNnSc4	Morrison
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
ke	k	k7c3	k
kapele	kapela	k1gFnSc3	kapela
přidali	přidat	k5eAaPmAgMnP	přidat
kytarista	kytarista	k1gMnSc1	kytarista
Robby	Robba	k1gFnSc2	Robba
Krieger	Krieger	k1gMnSc1	Krieger
a	a	k8xC	a
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Densmore	Densmor	k1gMnSc5	Densmor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
pod	pod	k7c7	pod
prostým	prostý	k2eAgInSc7d1	prostý
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Doors	Doors	k1gInSc1	Doors
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
i	i	k8xC	i
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
skladeb	skladba	k1gFnPc2	skladba
kapely	kapela	k1gFnSc2	kapela
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Firus	k1gMnSc5	Firus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
alba	album	k1gNnSc2	album
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vydáno	vydán	k2eAgNnSc1d1	vydáno
album	album	k1gNnSc1	album
Strange	Strang	k1gFnSc2	Strang
Days	Daysa	k1gFnPc2	Daysa
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
singly	singl	k1gInPc4	singl
"	"	kIx"	"
<g/>
People	People	k1gFnPc4	People
Are	ar	k1gInSc5	ar
Strange	Strange	k1gNnPc2	Strange
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gFnSc2	Me
Two	Two	k1gMnSc1	Two
Times	Times	k1gMnSc1	Times
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
následující	následující	k2eAgInSc1d1	následující
rok	rok	k1gInSc1	rok
pak	pak	k6eAd1	pak
The	The	k1gFnSc4	The
Doors	Doorsa	k1gFnPc2	Doorsa
nahráli	nahrát	k5eAaBmAgMnP	nahrát
jedno	jeden	k4xCgNnSc4	jeden
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
s	s	k7c7	s
Morrisonem	Morrison	k1gMnSc7	Morrison
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
L.A.	L.A.	k1gFnSc4	L.A.
Woman	Womany	k1gInPc2	Womany
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
Morrison	Morrison	k1gMnSc1	Morrison
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Zbylí	zbylý	k2eAgMnPc1d1	zbylý
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
a	a	k8xC	a
vydávali	vydávat	k5eAaImAgMnP	vydávat
alba	album	k1gNnSc2	album
i	i	k8xC	i
bez	bez	k7c2	bez
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
však	však	k9	však
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doorsa	k1gFnPc2	Doorsa
měli	mít	k5eAaImAgMnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
muzikantů	muzikant	k1gMnPc2	muzikant
a	a	k8xC	a
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
hudby	hudba	k1gFnSc2	hudba
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
prvky	prvek	k1gInPc4	prvek
různých	různý	k2eAgInPc2d1	různý
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
acid	acid	k6eAd1	acid
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
či	či	k8xC	či
blues	blues	k1gNnSc4	blues
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
založili	založit	k5eAaPmAgMnP	založit
Krieger	Krieger	k1gMnSc1	Krieger
a	a	k8xC	a
Manzarek	Manzarka	k1gFnPc2	Manzarka
kapelu	kapela	k1gFnSc4	kapela
Riders	Riders	k1gInSc1	Riders
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Storm	Storm	k1gInSc4	Storm
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
skupiny	skupina	k1gFnSc2	skupina
sahají	sahat	k5eAaImIp3nP	sahat
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
pláži	pláž	k1gFnSc6	pláž
Venice	Venice	k1gFnSc2	Venice
Beach	Beacha	k1gFnPc2	Beacha
setkali	setkat	k5eAaPmAgMnP	setkat
zakládající	zakládající	k2eAgMnPc1d1	zakládající
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gMnSc1	Morrison
a	a	k8xC	a
Ray	Ray	k1gMnSc1	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
znali	znát	k5eAaImAgMnP	znát
již	již	k9	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
studií	studie	k1gFnPc2	studie
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Morrison	Morrison	k1gMnSc1	Morrison
zarecitoval	zarecitovat	k5eAaPmAgMnS	zarecitovat
Manzarekovi	Manzareek	k1gMnSc3	Manzareek
svou	svůj	k3xOyFgFnSc4	svůj
báseň	báseň	k1gFnSc4	báseň
"	"	kIx"	"
<g/>
Moonlight	Moonlight	k2eAgInSc1d1	Moonlight
Drive	drive	k1gInSc1	drive
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Manzarek	Manzarka	k1gFnPc2	Manzarka
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
založit	založit	k5eAaPmF	založit
kapelu	kapela	k1gFnSc4	kapela
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
Morrison	Morrison	k1gMnSc1	Morrison
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Manzarek	Manzarka	k1gFnPc2	Manzarka
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
členem	člen	k1gMnSc7	člen
skupiny	skupina	k1gFnSc2	skupina
Rick	Rick	k1gMnSc1	Rick
&	&	k?	&
the	the	k?	the
Ravens	Ravens	k1gInSc1	Ravens
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
klávesy	klávesa	k1gFnPc4	klávesa
a	a	k8xC	a
zpíval	zpívat	k5eAaImAgMnS	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
členy	člen	k1gInPc7	člen
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
byli	být	k5eAaImAgMnP	být
Manzarekovi	Manzarekův	k2eAgMnPc1d1	Manzarekův
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
Rick	Ricka	k1gFnPc2	Ricka
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Ray	Ray	k1gFnSc1	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
a	a	k8xC	a
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gInSc1	Morrison
nejdříve	dříve	k6eAd3	dříve
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
začali	začít	k5eAaPmAgMnP	začít
spolu	spolu	k6eAd1	spolu
zkoušet	zkoušet	k5eAaImF	zkoušet
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Manzarek	Manzarka	k1gFnPc2	Manzarka
v	v	k7c6	v
losangeleském	losangeleský	k2eAgNnSc6d1	losangeleské
meditačním	meditační	k2eAgNnSc6d1	meditační
centru	centrum	k1gNnSc6	centrum
Mahariši	Mahariše	k1gFnSc4	Mahariše
Maheš	Maheš	k1gMnSc4	Maheš
Jógiho	Jógi	k1gMnSc4	Jógi
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Johnem	John	k1gMnSc7	John
Densmorem	Densmor	k1gMnSc7	Densmor
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
a	a	k8xC	a
Densmore	Densmor	k1gInSc5	Densmor
tuto	tento	k3xDgFnSc4	tento
nabídku	nabídka	k1gFnSc4	nabídka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
pak	pak	k6eAd1	pak
nově	nově	k6eAd1	nově
sestavená	sestavený	k2eAgFnSc1d1	sestavená
skupina	skupina	k1gFnSc1	skupina
využila	využít	k5eAaPmAgFnS	využít
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
měli	mít	k5eAaImAgMnP	mít
Rick	Rick	k1gMnSc1	Rick
&	&	k?	&
the	the	k?	the
Ravens	Ravensa	k1gFnPc2	Ravensa
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
vydavatelství	vydavatelství	k1gNnSc6	vydavatelství
Aura	aura	k1gFnSc1	aura
a	a	k8xC	a
nahrála	nahrát	k5eAaPmAgFnS	nahrát
šest	šest	k4xCc4	šest
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
první	první	k4xOgFnSc7	první
nahrávkou	nahrávka	k1gFnSc7	nahrávka
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgFnPc2	všecek
šest	šest	k4xCc1	šest
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
:	:	kIx,	:
Box	box	k1gInSc1	box
Set	set	k1gInSc1	set
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Demo	demo	k2eAgNnSc1d1	demo
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
třech	tři	k4xCgFnPc6	tři
gramofonových	gramofonový	k2eAgFnPc6d1	gramofonová
deskách	deska	k1gFnPc6	deska
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterými	který	k3yQgFnPc7	který
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
obcházeli	obcházet	k5eAaImAgMnP	obcházet
různá	různý	k2eAgNnPc4d1	různé
hudební	hudební	k2eAgNnPc4d1	hudební
vydavatelství	vydavatelství	k1gNnPc4	vydavatelství
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
podepsat	podepsat	k5eAaPmF	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kapelu	kapela	k1gFnSc4	kapela
opustili	opustit	k5eAaPmAgMnP	opustit
bratři	bratr	k1gMnPc1	bratr
Raye	Raye	k1gFnSc1	Raye
Manzareka	Manzareka	k1gFnSc1	Manzareka
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
se	se	k3xPyFc4	se
zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
poohlížet	poohlížet	k5eAaImF	poohlížet
po	po	k7c6	po
novém	nový	k2eAgInSc6d1	nový
členu	člen	k1gInSc6	člen
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kytarista	kytarista	k1gMnSc1	kytarista
Robby	Robba	k1gFnSc2	Robba
Krieger	Krieger	k1gMnSc1	Krieger
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
býval	bývat	k5eAaImAgInS	bývat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Densmorem	Densmor	k1gMnSc7	Densmor
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
Psychedelic	Psychedelice	k1gFnPc2	Psychedelice
Rangers	Rangersa	k1gFnPc2	Rangersa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
The	The	k1gFnSc4	The
Doors	Doorsa	k1gFnPc2	Doorsa
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Aldouse	Aldouse	k1gFnSc2	Aldouse
Huxleyho	Huxley	k1gMnSc2	Huxley
Brány	brána	k1gFnSc2	brána
vnímání	vnímání	k1gNnSc2	vnímání
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
The	The	k1gFnSc1	The
Doors	Doors	k1gInSc1	Doors
of	of	k?	of
Perception	Perception	k1gInSc1	Perception
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fráze	fráze	k1gFnSc1	fráze
"	"	kIx"	"
<g/>
doors	doors	k1gInSc1	doors
of	of	k?	of
perception	perception	k1gInSc1	perception
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
Huxleyově	Huxleyův	k2eAgFnSc6d1	Huxleyův
knize	kniha	k1gFnSc6	kniha
použita	použit	k2eAgFnSc1d1	použita
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
citací	citace	k1gFnSc7	citace
Williama	William	k1gMnSc2	William
Blakea	Blakeus	k1gMnSc2	Blakeus
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
byla	být	k5eAaImAgFnS	být
dost	dost	k6eAd1	dost
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
rockových	rockový	k2eAgFnPc2d1	rocková
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
neměla	mít	k5eNaImAgFnS	mít
basového	basový	k2eAgMnSc4d1	basový
kytaristu	kytarista	k1gMnSc4	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Manzarek	Manzarka	k1gFnPc2	Manzarka
však	však	k9	však
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc2	jeho
klávesy	klávesa	k1gFnSc2	klávesa
Fender	Fender	k1gMnSc1	Fender
Rhodes	Rhodes	k1gMnSc1	Rhodes
basspiano	basspiana	k1gFnSc5	basspiana
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
měl	mít	k5eAaImAgInS	mít
již	již	k6eAd1	již
z	z	k7c2	z
dřívější	dřívější	k2eAgFnSc2d1	dřívější
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
zvuk	zvuk	k1gInSc4	zvuk
basové	basový	k2eAgFnSc2d1	basová
kytary	kytara	k1gFnSc2	kytara
zastoupit	zastoupit	k5eAaPmF	zastoupit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
basového	basový	k2eAgMnSc2d1	basový
kytaristy	kytarista	k1gMnSc2	kytarista
už	už	k9	už
nebylo	být	k5eNaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
The	The	k1gMnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
dále	daleko	k6eAd2	daleko
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
koncertovat	koncertovat	k5eAaImF	koncertovat
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
angažmá	angažmá	k1gNnSc4	angažmá
nejdříve	dříve	k6eAd3	dříve
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
losangeleském	losangeleský	k2eAgInSc6d1	losangeleský
klubu	klub	k1gInSc6	klub
s	s	k7c7	s
názvem	název	k1gInSc7	název
London	London	k1gMnSc1	London
Fog	Fog	k1gMnSc1	Fog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
kapele	kapela	k1gFnSc3	kapela
podařilo	podařit	k5eAaPmAgNnS	podařit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
domovskou	domovský	k2eAgFnSc7d1	domovská
kapelou	kapela	k1gFnSc7	kapela
prestižního	prestižní	k2eAgInSc2d1	prestižní
klubu	klub	k1gInSc2	klub
Whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
Go	Go	k1gFnSc2	Go
Go	Go	k1gFnSc2	Go
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
však	však	k9	však
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
vyhozeni	vyhozen	k2eAgMnPc1d1	vyhozen
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
zahráli	zahrát	k5eAaPmAgMnP	zahrát
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc4	The
End	End	k1gFnSc2	End
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skladba	skladba	k1gFnSc1	skladba
totiž	totiž	k9	totiž
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
dramatické	dramatický	k2eAgNnSc4d1	dramatické
Morrisonovo	Morrisonův	k2eAgNnSc4d1	Morrisonovo
recitování	recitování	k1gNnSc4	recitování
včetně	včetně	k7c2	včetně
veršů	verš	k1gInPc2	verš
"	"	kIx"	"
<g/>
Otče	otec	k1gMnSc5	otec
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
Ano	ano	k9	ano
synu	syn	k1gMnSc3	syn
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
Chci	chtít	k5eAaImIp1nS	chtít
tě	ty	k3xPp2nSc4	ty
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
/	/	kIx~	/
Matko	matka	k1gFnSc5	matka
<g/>
?	?	kIx.	?
</s>
<s>
/	/	kIx~	/
Tebe	ty	k3xPp2nSc4	ty
chci	chtít	k5eAaImIp1nS	chtít
ojet	ojet	k5eAaPmF	ojet
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
The	The	k1gMnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
museli	muset	k5eAaImAgMnP	muset
opustit	opustit	k5eAaPmF	opustit
klub	klub	k1gInSc4	klub
Whisky	whisky	k1gFnSc2	whisky
a	a	k8xC	a
Go	Go	k1gFnSc2	Go
Go	Go	k1gFnSc2	Go
<g/>
,	,	kIx,	,
stačil	stačit	k5eAaBmAgMnS	stačit
Arthur	Arthur	k1gMnSc1	Arthur
Lee	Lea	k1gFnSc3	Lea
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Love	lov	k1gInSc5	lov
nalákat	nalákat	k5eAaPmF	nalákat
na	na	k7c4	na
koncert	koncert	k1gInSc4	koncert
The	The	k1gMnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
Jaca	Jacus	k1gMnSc2	Jacus
Holzmana	Holzman	k1gMnSc2	Holzman
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k1gMnSc2	vedoucí
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Elektra	Elektr	k1gMnSc2	Elektr
<g/>
.	.	kIx.	.
</s>
<s>
Holzman	Holzman	k1gMnSc1	Holzman
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
členům	člen	k1gMnPc3	člen
The	The	k1gFnSc4	The
Doors	Doorsa	k1gFnPc2	Doorsa
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
ji	on	k3xPp3gFnSc4	on
podepsali	podepsat	k5eAaPmAgMnP	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nedlouho	dlouho	k6eNd1	dlouho
začali	začít	k5eAaPmAgMnP	začít
The	The	k1gMnSc1	The
Doors	Doors	k1gInSc4	Doors
nahrávat	nahrávat	k5eAaImF	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
název	název	k1gInSc1	název
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Break	break	k1gInSc4	break
on	on	k3xPp3gMnSc1	on
Through	Through	k1gMnSc1	Through
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
the	the	k?	the
Other	Othero	k1gNnPc2	Othero
Side	Side	k1gInSc1	Side
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
stačil	stačit	k5eAaBmAgMnS	stačit
vyjít	vyjít	k5eAaPmF	vyjít
ještě	ještě	k9	ještě
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
před	před	k7c7	před
samotným	samotný	k2eAgNnSc7d1	samotné
albem	album	k1gNnSc7	album
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doorsa	k1gFnPc2	Doorsa
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
skladeb	skladba	k1gFnPc2	skladba
skupiny	skupina	k1gFnSc2	skupina
včetně	včetně	k7c2	včetně
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Fire	k1gNnPc2	Fire
<g/>
"	"	kIx"	"
či	či	k8xC	či
již	již	k6eAd1	již
zmiňované	zmiňovaný	k2eAgFnPc4d1	zmiňovaná
písně	píseň	k1gFnPc4	píseň
"	"	kIx"	"
<g/>
Break	break	k1gInSc4	break
On	on	k3xPp3gMnSc1	on
Through	Through	k1gMnSc1	Through
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
the	the	k?	the
Other	Othero	k1gNnPc2	Othero
Side	Side	k1gInSc1	Side
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
End	End	k1gFnSc1	End
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Výjimečně	výjimečně	k6eAd1	výjimečně
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
album	album	k1gNnSc4	album
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
dvě	dva	k4xCgFnPc1	dva
převzaté	převzatý	k2eAgFnPc1d1	převzatá
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
"	"	kIx"	"
<g/>
Alabama	Alabama	k1gNnSc1	Alabama
Song	song	k1gInSc1	song
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Back	Back	k1gMnSc1	Back
Door	Door	k1gMnSc1	Door
Man	Man	k1gMnSc1	Man
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	alba	k1gFnSc1	alba
kapela	kapela	k1gFnSc1	kapela
dále	daleko	k6eAd2	daleko
koncertovala	koncertovat	k5eAaImAgFnS	koncertovat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
,	,	kIx,	,
jednou	jednou	k6eAd1	jednou
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Grateful	Gratefula	k1gFnPc2	Gratefula
Dead	Deada	k1gFnPc2	Deada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
byla	být	k5eAaImAgFnS	být
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Fir	k1gFnPc1	Fir
<g/>
"	"	kIx"	"
vydána	vydán	k2eAgFnSc1d1	vydána
jako	jako	k8xC	jako
singl	singl	k1gInSc1	singl
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
oproti	oproti	k7c3	oproti
přes	přes	k7c4	přes
sedm	sedm	k4xCc4	sedm
minut	minuta	k1gFnPc2	minuta
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
verzi	verze	k1gFnSc3	verze
písně	píseň	k1gFnSc2	píseň
z	z	k7c2	z
alba	album	k1gNnSc2	album
měla	mít	k5eAaImAgFnS	mít
singlová	singlový	k2eAgFnSc1d1	singlová
verze	verze	k1gFnSc1	verze
jen	jen	k9	jen
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
tři	tři	k4xCgFnPc4	tři
minuty	minuta	k1gFnPc4	minuta
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
bylo	být	k5eAaImAgNnS	být
vynecháno	vynechán	k2eAgNnSc4d1	vynecháno
Kriegerovo	Kriegerův	k2eAgNnSc4d1	Kriegerův
a	a	k8xC	a
Manzarekovo	Manzarekův	k2eAgNnSc4d1	Manzarekův
sólo	sólo	k1gNnSc4	sólo
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
rádiové	rádiový	k2eAgNnSc4d1	rádiové
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Fire	k1gNnSc3	Fire
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
Krieger	Krieger	k1gMnSc1	Krieger
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Fire	k1gNnPc6	Fire
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
dobře	dobře	k6eAd1	dobře
prodávat	prodávat	k5eAaImF	prodávat
a	a	k8xC	a
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
1967	[number]	k4	1967
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
milion	milion	k4xCgInSc1	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
a	a	k8xC	a
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
Strange	Strange	k1gNnSc2	Strange
Days	Daysa	k1gFnPc2	Daysa
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
písní	píseň	k1gFnPc2	píseň
ze	z	k7c2	z
Strange	Strang	k1gInSc2	Strang
Days	Daysa	k1gFnPc2	Daysa
bylo	být	k5eAaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Strange	Strange	k1gNnSc3	Strange
Days	Daysa	k1gFnPc2	Daysa
vyšly	vyjít	k5eAaPmAgInP	vyjít
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
People	People	k1gFnSc1	People
Are	ar	k1gInSc5	ar
Strange	Strange	k1gNnPc2	Strange
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gFnSc2	Me
Two	Two	k1gMnSc1	Two
Times	Times	k1gMnSc1	Times
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
předchozí	předchozí	k2eAgNnSc4d1	předchozí
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
i	i	k8xC	i
toto	tento	k3xDgNnSc1	tento
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
The	The	k1gFnSc4	The
Doors	Doorsa	k1gFnPc2	Doorsa
tehdy	tehdy	k6eAd1	tehdy
často	často	k6eAd1	často
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1967	[number]	k4	1967
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
show	show	k1gFnSc6	show
Eda	Eda	k1gMnSc1	Eda
Sullivana	Sullivan	k1gMnSc2	Sullivan
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Fire	k1gFnSc3	Fire
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sullivan	Sullivan	k1gMnSc1	Sullivan
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kapela	kapela	k1gFnSc1	kapela
trošku	trošku	k6eAd1	trošku
upravila	upravit	k5eAaPmAgFnS	upravit
text	text	k1gInSc4	text
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
členové	člen	k1gMnPc1	člen
The	The	k1gFnSc2	The
Doors	Doors	k1gInSc1	Doors
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Morrisonem	Morrison	k1gInSc7	Morrison
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
živém	živý	k2eAgNnSc6d1	živé
vystoupení	vystoupení	k1gNnSc6	vystoupení
však	však	k9	však
Morrison	Morrison	k1gMnSc1	Morrison
zazpíval	zazpívat	k5eAaPmAgMnS	zazpívat
text	text	k1gInSc4	text
v	v	k7c4	v
původní	původní	k2eAgNnSc4d1	původní
podobně	podobně	k6eAd1	podobně
a	a	k8xC	a
kapele	kapela	k1gFnSc3	kapela
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
příště	příště	k6eAd1	příště
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
Sullivanově	Sullivanův	k2eAgInSc6d1	Sullivanův
pořadu	pořad	k1gInSc6	pořad
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nahrávání	nahrávání	k1gNnSc2	nahrávání
třetího	třetí	k4xOgNnSc2	třetí
alba	album	k1gNnSc2	album
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Sun	Sun	kA	Sun
byl	být	k5eAaImAgMnS	být
výjimečně	výjimečně	k6eAd1	výjimečně
přizván	přizvat	k5eAaPmNgMnS	přizvat
hostující	hostující	k2eAgMnSc1d1	hostující
basový	basový	k2eAgMnSc1d1	basový
kytarista	kytarista	k1gMnSc1	kytarista
Doug	Doug	k1gMnSc1	Doug
Lubahn	Lubahn	k1gMnSc1	Lubahn
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
Clear	Clear	k1gMnSc1	Clear
Light	Light	k1gMnSc1	Light
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doors	k1gInSc1	Doors
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nahrávání	nahrávání	k1gNnSc2	nahrávání
alba	album	k1gNnPc4	album
potýkali	potýkat	k5eAaImAgMnP	potýkat
s	s	k7c7	s
nedostatkem	nedostatek	k1gInSc7	nedostatek
hudebního	hudební	k2eAgInSc2d1	hudební
materiálu	materiál	k1gInSc2	materiál
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Jim	on	k3xPp3gMnPc3	on
Morrison	Morrison	k1gInSc1	Morrison
navíc	navíc	k6eAd1	navíc
přicházel	přicházet	k5eAaImAgInS	přicházet
na	na	k7c4	na
nahrávání	nahrávání	k1gNnSc4	nahrávání
alba	album	k1gNnSc2	album
často	často	k6eAd1	často
opilý	opilý	k2eAgMnSc1d1	opilý
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nahrávání	nahrávání	k1gNnSc1	nahrávání
bylo	být	k5eAaImAgNnS	být
problémové	problémový	k2eAgNnSc1d1	problémové
a	a	k8xC	a
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předchozími	předchozí	k2eAgInPc7d1	předchozí
dvěma	dva	k4xCgInPc7	dva
alby	album	k1gNnPc7	album
však	však	k9	však
Waiting	Waiting	k1gInSc4	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Sun	Sun	kA	Sun
přineslo	přinést	k5eAaPmAgNnS	přinést
určité	určitý	k2eAgNnSc1d1	určité
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
vyšla	vyjít	k5eAaPmAgFnS	vyjít
i	i	k9	i
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Unknown	Unknown	k1gMnSc1	Unknown
Soldier	Soldier	k1gMnSc1	Soldier
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgInPc4	který
sama	sám	k3xTgFnSc1	sám
kapela	kapela	k1gFnSc1	kapela
natočila	natočit	k5eAaBmAgFnS	natočit
krátký	krátký	k2eAgInSc4d1	krátký
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
The	The	k1gMnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
zavítali	zavítat	k5eAaPmAgMnP	zavítat
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
zahrát	zahrát	k5eAaPmF	zahrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1969	[number]	k4	1969
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
na	na	k7c6	na
Floridě	Florida	k1gFnSc6	Florida
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
kapely	kapela	k1gFnSc2	kapela
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gInSc1	Morrison
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Miami	Miami	k1gNnSc2	Miami
zmeškal	zmeškat	k5eAaPmAgMnS	zmeškat
dvě	dva	k4xCgNnPc4	dva
letadla	letadlo	k1gNnPc4	letadlo
a	a	k8xC	a
čekání	čekání	k1gNnSc4	čekání
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
lety	let	k1gInPc4	let
strávil	strávit	k5eAaPmAgMnS	strávit
pitím	pití	k1gNnSc7	pití
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
při	při	k7c6	při
vystoupení	vystoupení	k1gNnSc6	vystoupení
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
opilý	opilý	k2eAgInSc1d1	opilý
<g/>
,	,	kIx,	,
přerušoval	přerušovat	k5eAaImAgMnS	přerušovat
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
často	často	k6eAd1	často
mluvil	mluvit	k5eAaImAgMnS	mluvit
k	k	k7c3	k
publiku	publikum	k1gNnSc3	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stejně	stejně	k6eAd1	stejně
všichni	všechen	k3xTgMnPc1	všechen
přišli	přijít	k5eAaPmAgMnP	přijít
jenom	jenom	k6eAd1	jenom
vidět	vidět	k5eAaImF	vidět
jeho	on	k3xPp3gInSc4	on
penis	penis	k1gInSc4	penis
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
si	se	k3xPyFc3	se
měl	mít	k5eAaImAgMnS	mít
svléci	svléct	k5eAaPmF	svléct
košili	košile	k1gFnSc4	košile
a	a	k8xC	a
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
penis	penis	k1gInSc4	penis
skutečně	skutečně	k6eAd1	skutečně
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
však	však	k9	však
není	být	k5eNaImIp3nS	být
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
na	na	k7c6	na
žádné	žádný	k3yNgFnSc6	žádný
z	z	k7c2	z
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
skutečně	skutečně	k6eAd1	skutečně
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
koncertě	koncert	k1gInSc6	koncert
kapela	kapela	k1gFnSc1	kapela
odjela	odjet	k5eAaPmAgFnS	odjet
na	na	k7c4	na
dovolenou	dovolená	k1gFnSc4	dovolená
na	na	k7c4	na
Jamajku	Jamajka	k1gFnSc4	Jamajka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
miamské	miamský	k2eAgInPc1d1	miamský
úřady	úřad	k1gInPc1	úřad
rozhodly	rozhodnout	k5eAaPmAgInP	rozhodnout
o	o	k7c6	o
podání	podání	k1gNnSc6	podání
žaloby	žaloba	k1gFnSc2	žaloba
na	na	k7c4	na
Morrisona	Morrison	k1gMnSc4	Morrison
za	za	k7c4	za
obnažování	obnažování	k1gNnSc4	obnažování
<g/>
,	,	kIx,	,
urážku	urážka	k1gFnSc4	urážka
<g/>
,	,	kIx,	,
oplzlé	oplzlý	k2eAgNnSc4d1	oplzlé
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
opilství	opilství	k1gNnSc4	opilství
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Miami	Miami	k1gNnSc2	Miami
na	na	k7c4	na
Morrisona	Morrison	k1gMnSc4	Morrison
navíc	navíc	k6eAd1	navíc
vydala	vydat	k5eAaPmAgFnS	vydat
zatykač	zatykač	k1gInSc4	zatykač
FBI	FBI	kA	FBI
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jej	on	k3xPp3gMnSc4	on
obvinila	obvinit	k5eAaPmAgFnS	obvinit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nezákonně	zákonně	k6eNd1	zákonně
odletěl	odletět	k5eAaPmAgMnS	odletět
na	na	k7c4	na
Jamajku	Jamajka	k1gFnSc4	Jamajka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
tam	tam	k6eAd1	tam
Morrison	Morrison	k1gMnSc1	Morrison
odletěl	odletět	k5eAaPmAgMnS	odletět
ještě	ještě	k9	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
miamského	miamský	k2eAgInSc2d1	miamský
zatykače	zatykač	k1gInSc2	zatykač
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tíhou	tíha	k1gFnSc7	tíha
těchto	tento	k3xDgFnPc2	tento
obvinění	obvinění	k1gNnSc2	obvinění
začala	začít	k5eAaPmAgFnS	začít
rádia	rádius	k1gInSc2	rádius
stahovat	stahovat	k5eAaImF	stahovat
písně	píseň	k1gFnPc4	píseň
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
z	z	k7c2	z
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
nadcházející	nadcházející	k2eAgNnSc1d1	nadcházející
turné	turné	k1gNnSc1	turné
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
nemohla	moct	k5eNaImAgFnS	moct
vystoupit	vystoupit	k5eAaPmF	vystoupit
ani	ani	k8xC	ani
na	na	k7c4	na
Woodstocku	Woodstocka	k1gFnSc4	Woodstocka
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gMnSc1	Morrison
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
kauci	kauce	k1gFnSc4	kauce
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
případ	případ	k1gInSc1	případ
byl	být	k5eAaImAgInS	být
prozatím	prozatím	k6eAd1	prozatím
odročen	odročit	k5eAaPmNgInS	odročit
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
v	v	k7c6	v
Miami	Miami	k1gNnSc6	Miami
se	s	k7c7	s
The	The	k1gFnSc7	The
Doors	Doors	k1gInSc4	Doors
začali	začít	k5eAaPmAgMnP	začít
věnovat	věnovat	k5eAaPmF	věnovat
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Soft	Soft	k?	Soft
Parade	Parad	k1gInSc5	Parad
a	a	k8xC	a
vyšly	vyjít	k5eAaPmAgFnP	vyjít
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Touch	Touch	k1gMnSc1	Touch
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
hostující	hostující	k2eAgMnSc1d1	hostující
saxofonista	saxofonista	k1gMnSc1	saxofonista
Curtis	Curtis	k1gFnSc2	Curtis
Amy	Amy	k1gMnSc1	Amy
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Wishful	Wishful	k1gInSc1	Wishful
Sinful	Sinful	k1gInSc1	Sinful
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Runnin	Runnin	k1gInSc1	Runnin
<g/>
'	'	kIx"	'
Blue	Blue	k1gInSc1	Blue
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tell	Tell	k1gInSc1	Tell
All	All	k1gFnSc2	All
the	the	k?	the
People	People	k1gFnSc2	People
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
však	však	k9	však
nesetkalo	setkat	k5eNaPmAgNnS	setkat
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgNnPc3d1	předchozí
albům	album	k1gNnPc3	album
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
bylo	být	k5eAaImAgNnS	být
Soft	Soft	k?	Soft
Parade	Parad	k1gInSc5	Parad
zatím	zatím	k6eAd1	zatím
asi	asi	k9	asi
nejslabší	slabý	k2eAgFnSc1d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
alba	album	k1gNnSc2	album
napsal	napsat	k5eAaBmAgMnS	napsat
Morrison	Morrison	k1gInSc4	Morrison
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgFnPc4d1	zbylá
písně	píseň	k1gFnPc4	píseň
pak	pak	k6eAd1	pak
Krieger	Kriegra	k1gFnPc2	Kriegra
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Morrison	Morrison	k1gMnSc1	Morrison
prodělal	prodělat	k5eAaPmAgMnS	prodělat
sérii	série	k1gFnSc4	série
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
Jerrym	Jerrym	k1gInSc1	Jerrym
Hopkinsem	Hopkinso	k1gNnSc7	Hopkinso
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
poté	poté	k6eAd1	poté
otištěny	otisknout	k5eAaPmNgInP	otisknout
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
a	a	k8xC	a
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
pak	pak	k6eAd1	pak
Hopkins	Hopkins	k1gInSc4	Hopkins
čerpal	čerpat	k5eAaImAgMnS	čerpat
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
knihy	kniha	k1gFnPc4	kniha
o	o	k7c4	o
The	The	k1gFnSc4	The
Doors	Doorsa	k1gFnPc2	Doorsa
Nikdo	nikdo	k3yNnSc1	nikdo
to	ten	k3xDgNnSc4	ten
tu	tu	k6eAd1	tu
nepřežije	přežít	k5eNaPmIp3nS	přežít
a	a	k8xC	a
Ještěrčí	ještěrčí	k2eAgMnSc1d1	ještěrčí
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Soft	Soft	k?	Soft
Parade	Parad	k1gInSc5	Parad
začali	začít	k5eAaPmAgMnP	začít
The	The	k1gMnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
opět	opět	k6eAd1	opět
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgInS	být
prakticky	prakticky	k6eAd1	prakticky
každý	každý	k3xTgInSc1	každý
koncert	koncert	k1gInSc1	koncert
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
strážců	strážce	k1gMnPc2	strážce
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
Morrison	Morrison	k1gMnSc1	Morrison
tolik	tolik	k6eAd1	tolik
nepil	pít	k5eNaImAgMnS	pít
alkohol	alkohol	k1gInSc4	alkohol
ani	ani	k8xC	ani
nebral	brát	k5eNaImAgMnS	brát
narkotika	narkotikon	k1gNnPc4	narkotikon
a	a	k8xC	a
koncerty	koncert	k1gInPc4	koncert
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stávaly	stávat	k5eAaImAgFnP	stávat
po	po	k7c6	po
hudební	hudební	k2eAgFnSc6d1	hudební
stránce	stránka	k1gFnSc6	stránka
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Morrison	Morrison	k1gMnSc1	Morrison
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
natáčení	natáčení	k1gNnSc2	natáčení
filmu	film	k1gInSc2	film
s	s	k7c7	s
názvem	název	k1gInSc7	název
HWY	HWY	kA	HWY
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Riders	Riders	k1gInSc1	Riders
on	on	k3xPp3gInSc1	on
the	the	k?	the
Storm	Storm	k1gInSc1	Storm
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doors	k1gInSc4	Doors
stále	stále	k6eAd1	stále
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
vydavatelství	vydavatelství	k1gNnSc3	vydavatelství
Elektra	Elektrum	k1gNnSc2	Elektrum
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
vedení	vedení	k1gNnSc1	vedení
začalo	začít	k5eAaPmAgNnS	začít
po	po	k7c6	po
finančním	finanční	k2eAgInSc6d1	finanční
neúspěchu	neúspěch	k1gInSc6	neúspěch
Soft	Soft	k?	Soft
Parade	Parad	k1gInSc5	Parad
tlačit	tlačit	k5eAaImF	tlačit
na	na	k7c4	na
členy	člen	k1gInPc4	člen
The	The	k1gFnPc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nahráli	nahrát	k5eAaBmAgMnP	nahrát
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doors	k1gInSc1	Doors
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
nahrávání	nahrávání	k1gNnSc2	nahrávání
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
dostalo	dostat	k5eAaPmAgNnS	dostat
název	název	k1gInSc4	název
Morrison	Morrison	k1gInSc1	Morrison
Hotel	hotel	k1gInSc1	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
bluesové	bluesový	k2eAgNnSc1d1	bluesové
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neobsahovalo	obsahovat	k5eNaImAgNnS	obsahovat
žádný	žádný	k3yNgInSc4	žádný
singlový	singlový	k2eAgInSc4d1	singlový
hit	hit	k1gInSc4	hit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
mnohem	mnohem	k6eAd1	mnohem
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Morrison	Morrison	k1gInSc1	Morrison
Hotel	hotel	k1gInSc1	hotel
vyšel	vyjít	k5eAaPmAgInS	vyjít
jediný	jediný	k2eAgInSc1d1	jediný
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Roadhouse	Roadhouse	k1gFnSc1	Roadhouse
Blues	blues	k1gFnSc2	blues
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncertní	koncertní	k2eAgNnSc1d1	koncertní
vystoupení	vystoupení	k1gNnSc1	vystoupení
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
bývala	bývat	k5eAaImAgFnS	bývat
zaznamenávána	zaznamenávat	k5eAaImNgFnS	zaznamenávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
pak	pak	k6eAd1	pak
vybraly	vybrat	k5eAaPmAgFnP	vybrat
písně	píseň	k1gFnPc1	píseň
pro	pro	k7c4	pro
chystané	chystaný	k2eAgNnSc4d1	chystané
první	první	k4xOgNnSc4	první
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c6	na
dvou	dva	k4xCgNnPc6	dva
albech	album	k1gNnPc6	album
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Absolutely	Absolutela	k1gFnSc2	Absolutela
Live	Liv	k1gFnSc2	Liv
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1970	[number]	k4	1970
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
některé	některý	k3yIgFnPc4	některý
písně	píseň	k1gFnPc4	píseň
zaznamenané	zaznamenaný	k2eAgFnPc4d1	zaznamenaná
i	i	k9	i
předešlý	předešlý	k2eAgInSc4d1	předešlý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gMnSc1	Morrison
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
trávil	trávit	k5eAaImAgMnS	trávit
čas	čas	k1gInSc4	čas
cestováním	cestování	k1gNnSc7	cestování
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
i	i	k8xC	i
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
Absolutely	Absolutela	k1gFnSc2	Absolutela
Live	Live	k1gFnSc7	Live
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Miami	Miami	k1gNnSc2	Miami
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
jeho	jeho	k3xOp3gNnSc4	jeho
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvojím	dvojí	k4xRgInSc6	dvojí
odročení	odročení	k1gNnSc6	odročení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
The	The	k1gFnSc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
hráli	hrát	k5eAaImAgMnP	hrát
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
festivalu	festival	k1gInSc6	festival
Isle	Isl	k1gFnSc2	Isl
of	of	k?	of
Wight	Wight	k1gMnSc1	Wight
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Morrison	Morrison	k1gMnSc1	Morrison
zbaven	zbavit	k5eAaPmNgMnS	zbavit
obvinění	obvinění	k1gNnSc3	obvinění
z	z	k7c2	z
opilství	opilství	k1gNnSc2	opilství
a	a	k8xC	a
obscénního	obscénní	k2eAgNnSc2d1	obscénní
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
užívání	užívání	k1gNnSc2	užívání
hrubého	hrubý	k2eAgInSc2d1	hrubý
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
obnažování	obnažování	k1gNnSc2	obnažování
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
vinným	vinný	k1gMnSc7	vinný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
soud	soud	k1gInSc1	soud
nestanovil	stanovit	k5eNaPmAgInS	stanovit
výši	výše	k1gFnSc4	výše
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
vynesení	vynesení	k1gNnSc2	vynesení
byl	být	k5eAaImAgInS	být
Morrison	Morrison	k1gInSc1	Morrison
propuštěn	propustit	k5eAaPmNgInS	propustit
na	na	k7c4	na
kauci	kauce	k1gFnSc4	kauce
padesáti	padesát	k4xCc2	padesát
tisíc	tisíc	k4xCgInPc2	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
šesti	šest	k4xCc3	šest
měsícům	měsíc	k1gInPc3	měsíc
vězení	vězení	k1gNnSc1	vězení
za	za	k7c2	za
obnažování	obnažování	k1gNnSc2	obnažování
a	a	k8xC	a
šedesáti	šedesát	k4xCc2	šedesát
pracovním	pracovní	k2eAgFnPc3d1	pracovní
dnům	den	k1gInPc3	den
za	za	k7c4	za
vulgární	vulgární	k2eAgInPc4d1	vulgární
projev	projev	k1gInSc4	projev
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doorsa	k1gFnPc2	Doorsa
odehráli	odehrát	k5eAaPmAgMnP	odehrát
svůj	svůj	k3xOyFgInSc4	svůj
předposlední	předposlední	k2eAgInSc4d1	předposlední
koncert	koncert	k1gInSc4	koncert
s	s	k7c7	s
Morrisonem	Morrison	k1gInSc7	Morrison
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
v	v	k7c6	v
McFarlin	McFarlina	k1gFnPc2	McFarlina
Auditorium	auditorium	k1gNnSc1	auditorium
Dallas	Dallas	k1gMnSc1	Dallas
a	a	k8xC	a
poslední	poslední	k2eAgNnSc4d1	poslední
vystoupení	vystoupení	k1gNnSc4	vystoupení
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
ve	v	k7c4	v
"	"	kIx"	"
<g/>
Warehouse	Warehouse	k1gFnPc4	Warehouse
<g/>
"	"	kIx"	"
v	v	k7c6	v
New	New	k1gFnSc6	New
Orleans	Orleans	k1gInSc4	Orleans
<g/>
.	.	kIx.	.
</s>
<s>
Morrison	Morrison	k1gMnSc1	Morrison
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
rozsudku	rozsudek	k1gInSc3	rozsudek
odvolal	odvolat	k5eAaPmAgMnS	odvolat
a	a	k8xC	a
před	před	k7c7	před
započetím	započetí	k1gNnSc7	započetí
odvolacího	odvolací	k2eAgInSc2d1	odvolací
procesu	proces	k1gInSc2	proces
začali	začít	k5eAaPmAgMnP	začít
The	The	k1gMnSc1	The
Doors	Doors	k1gInSc4	Doors
natáčet	natáčet	k5eAaImF	natáčet
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poslední	poslední	k2eAgNnSc1d1	poslední
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
Morrisonem	Morrison	k1gMnSc7	Morrison
natočeno	natočit	k5eAaBmNgNnS	natočit
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
producenta	producent	k1gMnSc4	producent
všech	všecek	k3xTgNnPc2	všecek
alb	album	k1gNnPc2	album
The	The	k1gFnPc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
Paula	Paul	k1gMnSc2	Paul
Rothchilda	Rothchildo	k1gNnSc2	Rothchildo
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Bruce	Bruce	k1gMnSc1	Bruce
Botnick	Botnick	k1gMnSc1	Botnick
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
L.A.	L.A.	k1gFnSc2	L.A.
Woman	Womana	k1gFnPc2	Womana
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
mixování	mixování	k1gNnSc2	mixování
L.A.	L.A.	k1gMnSc1	L.A.
Woman	Woman	k1gMnSc1	Woman
odjel	odjet	k5eAaPmAgMnS	odjet
Morrison	Morrison	k1gInSc4	Morrison
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlel	bydlet	k5eAaImAgMnS	bydlet
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Pamelou	Pamela	k1gFnSc7	Pamela
Cursonovou	Cursonová	k1gFnSc7	Cursonová
a	a	k8xC	a
kde	kde	k6eAd1	kde
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
oficiální	oficiální	k2eAgFnSc1d1	oficiální
diagnóza	diagnóza	k1gFnSc1	diagnóza
byla	být	k5eAaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
srdeční	srdeční	k2eAgFnSc1d1	srdeční
zástava	zástava	k1gFnSc1	zástava
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgFnPc1d1	přesná
okolnosti	okolnost	k1gFnPc1	okolnost
Morrisonovy	Morrisonův	k2eAgFnSc2d1	Morrisonova
smrti	smrt	k1gFnSc2	smrt
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
nebyly	být	k5eNaImAgFnP	být
zcela	zcela	k6eAd1	zcela
vyjasněny	vyjasněn	k2eAgFnPc1d1	vyjasněna
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
různých	různý	k2eAgInPc2d1	různý
dohadů	dohad	k1gInPc2	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
Morrisonově	Morrisonův	k2eAgFnSc6d1	Morrisonova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
na	na	k7c4	na
veřejnost	veřejnost	k1gFnSc4	veřejnost
až	až	k6eAd1	až
po	po	k7c6	po
pohřbení	pohřbení	k1gNnSc6	pohřbení
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Pè	Pè	k1gFnSc2	Pè
Lachaise	Lachaise	k1gFnSc2	Lachaise
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
částečně	částečně	k6eAd1	částečně
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
stále	stále	k6eAd1	stále
odmítají	odmítat	k5eAaImIp3nP	odmítat
jeho	jeho	k3xOp3gFnPc4	jeho
smrti	smrt	k1gFnPc4	smrt
uvěřit	uvěřit	k5eAaPmF	uvěřit
<g/>
.	.	kIx.	.
</s>
<s>
Zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpívat	zpívat	k5eAaImF	zpívat
bude	být	k5eAaImBp3nS	být
Manzarek	Manzarka	k1gFnPc2	Manzarka
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
nových	nový	k2eAgFnPc6d1	nová
písních	píseň	k1gFnPc6	píseň
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
Other	Other	k1gMnSc1	Other
Voices	Voices	k1gMnSc1	Voices
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
Bruce	Bruce	k1gMnSc1	Bruce
Botnick	Botnick	k1gMnSc1	Botnick
<g/>
.	.	kIx.	.
</s>
<s>
Zbylí	zbylý	k2eAgMnPc1d1	zbylý
členové	člen	k1gMnPc1	člen
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vydali	vydat	k5eAaPmAgMnP	vydat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
začali	začít	k5eAaPmAgMnP	začít
natáčet	natáčet	k5eAaImF	natáčet
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
název	název	k1gInSc1	název
Full	Fulla	k1gFnPc2	Fulla
Circle	Circle	k1gNnSc2	Circle
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
uspořádat	uspořádat	k5eAaPmF	uspořádat
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
konkurz	konkurz	k1gInSc4	konkurz
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
nového	nový	k2eAgMnSc2d1	nový
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
přicházel	přicházet	k5eAaImAgMnS	přicházet
i	i	k8xC	i
Iggy	Igg	k1gMnPc4	Igg
Pop	pop	k1gInSc4	pop
<g/>
,	,	kIx,	,
Manzarek	Manzarka	k1gFnPc2	Manzarka
se	se	k3xPyFc4	se
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
všeho	všecek	k3xTgMnSc4	všecek
už	už	k6eAd1	už
bylo	být	k5eAaImAgNnS	být
moc	moc	k6eAd1	moc
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Chtěli	chtít	k5eAaImAgMnP	chtít
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
dobít	dobít	k5eAaPmF	dobít
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
baterie	baterie	k1gFnPc4	baterie
<g/>
,	,	kIx,	,
právě	právě	k9	právě
jako	jako	k9	jako
Jim	on	k3xPp3gMnPc3	on
<g/>
,	,	kIx,	,
když	když	k8xS	když
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
...	...	k?	...
ale	ale	k8xC	ale
ono	onen	k3xDgNnSc1	onen
to	ten	k3xDgNnSc1	ten
nefungovalo	fungovat	k5eNaImAgNnS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
čas	čas	k1gInSc1	čas
s	s	k7c7	s
The	The	k1gFnPc7	The
Doors	Doors	k1gInSc4	Doors
skončit	skončit	k5eAaPmF	skončit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
se	se	k3xPyFc4	se
The	The	k1gFnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
definitivně	definitivně	k6eAd1	definitivně
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
opustil	opustit	k5eAaPmAgMnS	opustit
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
Ray	Ray	k1gFnSc2	Ray
Manazarek	Manazarka	k1gFnPc2	Manazarka
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Nahrál	nahrát	k5eAaBmAgMnS	nahrát
několik	několik	k4yIc4	několik
alb	album	k1gNnPc2	album
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
producentské	producentský	k2eAgFnSc2d1	producentská
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Krieger	Krieger	k1gMnSc1	Krieger
i	i	k9	i
Densmore	Densmor	k1gInSc5	Densmor
se	se	k3xPyFc4	se
také	také	k9	také
začali	začít	k5eAaPmAgMnP	začít
věnovat	věnovat	k5eAaPmF	věnovat
soukromým	soukromý	k2eAgMnPc3d1	soukromý
hudebním	hudební	k2eAgMnPc3d1	hudební
projektům	projekt	k1gInPc3	projekt
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doors	k1gInSc1	Doors
však	však	k9	však
dále	daleko	k6eAd2	daleko
vycházela	vycházet	k5eAaImAgFnS	vycházet
pod	pod	k7c7	pod
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Elektra	Elektrum	k1gNnSc2	Elektrum
či	či	k8xC	či
Rhino	Rhino	k6eAd1	Rhino
koncertní	koncertní	k2eAgNnPc1d1	koncertní
alba	album	k1gNnPc1	album
jako	jako	k8xS	jako
Alive	Aliev	k1gFnPc1	Aliev
<g/>
,	,	kIx,	,
She	She	k1gMnSc1	She
Cried	Cried	k1gMnSc1	Cried
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
či	či	k8xC	či
Legacy	Legacy	k1gInPc1	Legacy
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Absolute	Absolut	k1gMnSc5	Absolut
Best	Best	k1gMnSc1	Best
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
The	The	k1gFnSc6	The
Doors	Doorsa	k1gFnPc2	Doorsa
natočen	natočen	k2eAgInSc1d1	natočen
film	film	k1gInSc1	film
režírovaný	režírovaný	k2eAgInSc1d1	režírovaný
Oliverem	Oliver	k1gMnSc7	Oliver
Stonem	ston	k1gInSc7	ston
The	The	k1gFnPc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
Morrisona	Morrison	k1gMnSc2	Morrison
zahrál	zahrát	k5eAaPmAgInS	zahrát
Val	val	k1gInSc4	val
Kilmer	Kilmra	k1gFnPc2	Kilmra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byli	být	k5eAaImAgMnP	být
The	The	k1gMnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollové	rollový	k2eAgFnSc2d1	rollová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Pearl	Pearl	k1gInSc4	Pearl
Jam	jam	k1gInSc4	jam
Eddie	Eddie	k1gFnSc2	Eddie
Vedderem	Veddero	k1gNnSc7	Veddero
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
nazkoušela	nazkoušet	k5eAaPmAgFnS	nazkoušet
a	a	k8xC	a
zahrála	zahrát	k5eAaPmAgFnS	zahrát
skladby	skladba	k1gFnPc4	skladba
"	"	kIx"	"
<g/>
Roadhouse	Roadhouse	k1gFnSc1	Roadhouse
Blues	blues	k1gNnSc2	blues
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Break	break	k1gInSc4	break
on	on	k3xPp3gMnSc1	on
Through	Through	k1gMnSc1	Through
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
my	my	k3xPp1nPc1	my
Fire	Firus	k1gMnSc5	Firus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
poté	poté	k6eAd1	poté
vyšly	vyjít	k5eAaPmAgFnP	vyjít
na	na	k7c6	na
albu	album	k1gNnSc6	album
The	The	k1gFnSc2	The
Doors	Doors	k1gInSc4	Doors
&	&	k?	&
Eddie	Eddie	k1gFnSc2	Eddie
Vedder	Vedder	k1gMnSc1	Vedder
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
<g/>
&	&	k?	&
<g/>
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
Of	Of	k1gMnSc2	Of
Fame	Fam	k1gMnSc2	Fam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Stoned	Stoned	k1gMnSc1	Stoned
Immaculate	Immaculat	k1gInSc5	Immaculat
-	-	kIx~	-
The	The	k1gMnSc1	The
Music	Music	k1gMnSc1	Music
of	of	k?	of
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
písně	píseň	k1gFnPc4	píseň
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
různých	různý	k2eAgMnPc2d1	různý
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
Stone	ston	k1gInSc5	ston
Temple	templ	k1gInSc5	templ
Pilots	Pilotsa	k1gFnPc2	Pilotsa
<g/>
,	,	kIx,	,
Aerosmith	Aerosmitha	k1gFnPc2	Aerosmitha
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Lee	Lea	k1gFnSc6	Lea
Hookera	Hookero	k1gNnSc2	Hookero
či	či	k8xC	či
skupiny	skupina	k1gFnSc2	skupina
Creed	Creed	k1gInSc1	Creed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
Manzarek	Manzarka	k1gFnPc2	Manzarka
a	a	k8xC	a
Krieger	Kriegra	k1gFnPc2	Kriegra
dali	dát	k5eAaPmAgMnP	dát
opět	opět	k6eAd1	opět
dohromady	dohromady	k6eAd1	dohromady
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gFnSc2	The
Doors	Doors	k1gInSc1	Doors
of	of	k?	of
the	the	k?	the
21	[number]	k4	21
<g/>
st	st	kA	st
Century	Centura	k1gFnSc2	Centura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Morrisonovo	Morrisonův	k2eAgNnSc4d1	Morrisonovo
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
The	The	k1gMnSc1	The
Cult	Cult	k1gMnSc1	Cult
Ian	Ian	k1gMnSc1	Ian
Astbury	Astbura	k1gFnSc2	Astbura
<g/>
.	.	kIx.	.
</s>
<s>
Manzareka	Manzareka	k1gFnSc1	Manzareka
a	a	k8xC	a
Kriegera	Kriegera	k1gFnSc1	Kriegera
však	však	k9	však
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
obvinil	obvinit	k5eAaPmAgMnS	obvinit
John	John	k1gMnSc1	John
Densmore	Densmor	k1gInSc5	Densmor
a	a	k8xC	a
nedlouho	nedlouho	k1gNnSc4	nedlouho
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
i	i	k9	i
Morrisonova	Morrisonův	k2eAgFnSc1d1	Morrisonova
rodina	rodina	k1gFnSc1	rodina
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
názvu	název	k1gInSc6	název
značku	značka	k1gFnSc4	značka
The	The	k1gFnSc4	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
tak	tak	k6eAd1	tak
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Riders	Riders	k1gInSc4	Riders
on	on	k3xPp3gInSc1	on
the	the	k?	the
Storm	Storm	k1gInSc1	Storm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Astbury	Astbura	k1gFnSc2	Astbura
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
k	k	k7c3	k
The	The	k1gFnSc3	The
Cult	Culta	k1gFnPc2	Culta
<g/>
,	,	kIx,	,
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gMnSc4	on
zakládající	zakládající	k2eAgMnSc1d1	zakládající
člen	člen	k1gMnSc1	člen
skupiny	skupina	k1gFnSc2	skupina
Fuel	Fuel	k1gMnSc1	Fuel
Brett	Brett	k1gMnSc1	Brett
Scallions	Scallions	k1gInSc4	Scallions
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
The	The	k1gFnSc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
udělena	udělit	k5eAaPmNgFnS	udělit
první	první	k4xOgFnSc1	první
cena	cena	k1gFnSc1	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doorsa	k1gFnPc2	Doorsa
též	též	k9	též
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
floridský	floridský	k2eAgMnSc1d1	floridský
guvernér	guvernér	k1gMnSc1	guvernér
Charlie	Charlie	k1gMnSc1	Charlie
Crist	Crist	k1gMnSc1	Crist
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
symbolicky	symbolicky	k6eAd1	symbolicky
zrušit	zrušit	k5eAaPmF	zrušit
stále	stále	k6eAd1	stále
platný	platný	k2eAgInSc4d1	platný
rozsudek	rozsudek	k1gInSc4	rozsudek
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
posílal	posílat	k5eAaImAgMnS	posílat
Morrisona	Morrison	k1gMnSc2	Morrison
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
Manzarek	Manzarka	k1gFnPc2	Manzarka
a	a	k8xC	a
Krieger	Kriegra	k1gFnPc2	Kriegra
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
přejmenováni	přejmenován	k2eAgMnPc1d1	přejmenován
na	na	k7c4	na
Ray	Ray	k1gFnSc4	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
&	&	k?	&
Robby	Robb	k1gInPc1	Robb
Krieger	Krieger	k1gInSc1	Krieger
of	of	k?	of
the	the	k?	the
Doors	Doors	k1gInSc1	Doors
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
různá	různý	k2eAgNnPc4d1	různé
vystoupení	vystoupení	k1gNnPc4	vystoupení
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
s	s	k7c7	s
různými	různý	k2eAgMnPc7d1	různý
hudebníky	hudebník	k1gMnPc7	hudebník
a	a	k8xC	a
zpěváky	zpěvák	k1gMnPc7	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2010	[number]	k4	2010
poprvé	poprvé	k6eAd1	poprvé
navštívili	navštívit	k5eAaPmAgMnP	navštívit
také	také	k9	také
Česko	Česko	k1gNnSc4	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Kongresovém	kongresový	k2eAgNnSc6d1	Kongresové
centru	centrum	k1gNnSc6	centrum
odehráli	odehrát	k5eAaPmAgMnP	odehrát
koncert	koncert	k1gInSc4	koncert
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Brettem	Brett	k1gMnSc7	Brett
Scallionsem	Scallions	k1gMnSc7	Scallions
<g/>
,	,	kIx,	,
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Philem	Phil	k1gMnSc7	Phil
Chenem	Chen	k1gMnSc7	Chen
a	a	k8xC	a
bubeníkem	bubeník	k1gMnSc7	bubeník
Tyem	Tyema	k1gFnPc2	Tyema
Dennisem	Dennis	k1gInSc7	Dennis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
den	den	k1gInSc4	den
přesně	přesně	k6eAd1	přesně
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
druhý	druhý	k4xOgInSc1	druhý
pražský	pražský	k2eAgInSc1d1	pražský
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Kulturním	kulturní	k2eAgInSc6d1	kulturní
domě	dům	k1gInSc6	dům
Vltavská	vltavský	k2eAgFnSc1d1	Vltavská
se	s	k7c7	s
stejném	stejný	k2eAgNnSc6d1	stejné
složení	složení	k1gNnSc6	složení
vyjma	vyjma	k7c2	vyjma
zpěváka	zpěvák	k1gMnSc2	zpěvák
Bretta	Brett	k1gMnSc2	Brett
Scallionse	Scallions	k1gMnSc2	Scallions
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
Dave	Dav	k1gInSc5	Dav
Brock	Brocko	k1gNnPc2	Brocko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
videoklip	videoklip	k1gInSc4	videoklip
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
"	"	kIx"	"
<g/>
L.A.	L.A.	k1gMnSc1	L.A.
Woman	Woman	k1gMnSc1	Woman
<g/>
"	"	kIx"	"
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čtyřicátého	čtyřicátý	k4xOgNnSc2	čtyřicátý
výročí	výročí	k1gNnSc2	výročí
od	od	k7c2	od
vydání	vydání	k1gNnSc2	vydání
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
dřívější	dřívější	k2eAgInPc4d1	dřívější
značné	značný	k2eAgInPc4d1	značný
spory	spor	k1gInPc4	spor
objevil	objevit	k5eAaPmAgMnS	objevit
vedle	vedle	k7c2	vedle
kytaristy	kytarista	k1gMnSc2	kytarista
Robbyho	Robby	k1gMnSc2	Robby
Kriegera	Krieger	k1gMnSc2	Krieger
i	i	k9	i
bubeník	bubeník	k1gMnSc1	bubeník
John	John	k1gMnSc1	John
Densmore	Densmor	k1gMnSc5	Densmor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
přibyl	přibýt	k5eAaPmAgMnS	přibýt
ještě	ještě	k9	ještě
Jac	Jac	k1gMnSc1	Jac
Holzman	Holzman	k1gMnSc1	Holzman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
zemřel	zemřít	k5eAaPmAgMnS	zemřít
klávesista	klávesista	k1gMnSc1	klávesista
Ray	Ray	k1gFnSc2	Ray
Manzarek	Manzarka	k1gFnPc2	Manzarka
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
čtyřiasedmdesáti	čtyřiasedmdesát	k4xCc6	čtyřiasedmdesát
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
odehráli	odehrát	k5eAaPmAgMnP	odehrát
několik	několik	k4yIc4	několik
koncertu	koncert	k1gInSc2	koncert
John	John	k1gMnSc1	John
Densmore	Densmor	k1gInSc5	Densmor
a	a	k8xC	a
Robby	Robb	k1gInPc7	Robb
Krieger	Kriegra	k1gFnPc2	Kriegra
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
hudby	hudba	k1gFnSc2	hudba
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
specifický	specifický	k2eAgInSc1d1	specifický
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
prvky	prvek	k1gInPc4	prvek
různých	různý	k2eAgInPc2d1	různý
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
blues-rock	bluesock	k1gInSc4	blues-rock
<g/>
,	,	kIx,	,
psychedelický	psychedelický	k2eAgInSc4d1	psychedelický
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
acid	acid	k6eAd1	acid
rock	rock	k1gInSc1	rock
i	i	k9	i
hard	hard	k6eAd1	hard
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Raye	Ray	k1gMnSc2	Ray
Manzareka	Manzareek	k1gMnSc2	Manzareek
byli	být	k5eAaImAgMnP	být
The	The	k1gMnPc1	The
Doors	Doorsa	k1gFnPc2	Doorsa
ovlivněni	ovlivnit	k5eAaPmNgMnP	ovlivnit
jazzovými	jazzový	k2eAgMnPc7d1	jazzový
a	a	k8xC	a
bluesovými	bluesový	k2eAgMnPc7d1	bluesový
hudebníky	hudebník	k1gMnPc7	hudebník
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
Muddy	Mudda	k1gFnPc4	Mudda
Waters	Watersa	k1gFnPc2	Watersa
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Lee	Lea	k1gFnSc3	Lea
Hooker	Hooker	k1gMnSc1	Hooker
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Coltrane	Coltran	k1gInSc5	Coltran
či	či	k8xC	či
Miles	Miles	k1gInSc4	Miles
Davis	Davis	k1gFnSc2	Davis
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
vlivy	vliv	k1gInPc1	vliv
na	na	k7c6	na
The	The	k1gFnSc6	The
Doors	Doorsa	k1gFnPc2	Doorsa
měly	mít	k5eAaImAgFnP	mít
anglické	anglický	k2eAgFnPc1d1	anglická
kapely	kapela	k1gFnPc1	kapela
The	The	k1gFnSc2	The
Beatles	beatles	k1gMnPc2	beatles
a	a	k8xC	a
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xS	jako
Little	Little	k1gFnSc1	Little
Richard	Richard	k1gMnSc1	Richard
<g/>
,	,	kIx,	,
Chuck	Chuck	k1gMnSc1	Chuck
Berry	Berra	k1gFnSc2	Berra
i	i	k8xC	i
Elvis	Elvis	k1gMnSc1	Elvis
Presley	Preslea	k1gFnSc2	Preslea
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
klasiků	klasik	k1gMnPc2	klasik
měli	mít	k5eAaImAgMnP	mít
na	na	k7c4	na
The	The	k1gFnSc4	The
Doors	Doors	k1gInSc1	Doors
vliv	vliv	k1gInSc4	vliv
např.	např.	kA	např.
Igor	Igor	k1gMnSc1	Igor
Fjodorovič	Fjodorovič	k1gMnSc1	Fjodorovič
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
či	či	k8xC	či
Claude	Claud	k1gInSc5	Claud
Debussy	Debussa	k1gFnPc1	Debussa
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Morrison	Morrison	k1gInSc1	Morrison
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
např.	např.	kA	např.
hudbou	hudba	k1gFnSc7	hudba
Boba	Bob	k1gMnSc2	Bob
Dylana	Dylan	k1gMnSc2	Dylan
či	či	k8xC	či
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
,	,	kIx,	,
z	z	k7c2	z
literátů	literát	k1gMnPc2	literát
pak	pak	k6eAd1	pak
Aldousem	Aldous	k1gInSc7	Aldous
Huxleym	Huxleymum	k1gNnPc2	Huxleymum
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Blakem	Blaek	k1gInSc7	Blaek
či	či	k8xC	či
Jackem	Jacek	k1gMnSc7	Jacek
Kerouacem	Kerouace	k1gMnSc7	Kerouace
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Doors	Doors	k1gInSc4	Doors
patřili	patřit	k5eAaImAgMnP	patřit
k	k	k7c3	k
nejvlivnějším	vlivný	k2eAgFnPc3d3	nejvlivnější
kapelám	kapela	k1gFnPc3	kapela
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Iggy	Igga	k1gMnSc2	Igga
Pop	pop	k1gMnSc1	pop
založil	založit	k5eAaPmAgMnS	založit
The	The	k1gMnSc3	The
Stooges	Stooges	k1gInSc4	Stooges
pod	pod	k7c4	pod
dojmy	dojem	k1gInPc4	dojem
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
českých	český	k2eAgMnPc2d1	český
umělců	umělec	k1gMnPc2	umělec
měli	mít	k5eAaImAgMnP	mít
The	The	k1gMnPc1	The
Doors	Doors	k1gInSc1	Doors
vliv	vliv	k1gInSc4	vliv
např.	např.	kA	např.
na	na	k7c4	na
Filipa	Filip	k1gMnSc4	Filip
Topola	Topol	k1gMnSc4	Topol
a	a	k8xC	a
Psí	psí	k2eAgMnPc4d1	psí
vojáky	voják	k1gMnPc4	voják
či	či	k8xC	či
Josefa	Josef	k1gMnSc4	Josef
Rauvolfa	Rauvolf	k1gMnSc4	Rauvolf
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
skupiny	skupina	k1gFnSc2	skupina
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
End	End	k1gFnSc1	End
<g/>
"	"	kIx"	"
zazněla	zaznět	k5eAaImAgFnS	zaznět
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Apokalypsa	apokalypsa	k1gFnSc1	apokalypsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
písně	píseň	k1gFnPc1	píseň
byly	být	k5eAaImAgFnP	být
též	též	k9	též
mnohokrát	mnohokrát	k6eAd1	mnohokrát
přezpívány	přezpíván	k2eAgFnPc1d1	přezpívána
<g/>
.	.	kIx.	.
</s>
<s>
Aerosmith	Aerosmith	k1gInSc4	Aerosmith
přezpívali	přezpívat	k5eAaPmAgMnP	přezpívat
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gFnSc2	Me
Two	Two	k1gMnSc1	Two
Times	Times	k1gMnSc1	Times
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
podání	podání	k1gNnSc6	podání
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Air	Air	k1gFnSc2	Air
America	Americ	k1gInSc2	Americ
i	i	k9	i
na	na	k7c6	na
unpluggedovém	unpluggedový	k2eAgNnSc6d1	unpluggedový
vystoupení	vystoupení	k1gNnSc6	vystoupení
Aerosmith	Aerosmitha	k1gFnPc2	Aerosmitha
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
přezpívávaná	přezpívávaný	k2eAgFnSc1d1	přezpívávaný
píseň	píseň	k1gFnSc1	píseň
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Firus	k1gMnSc5	Firus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přezpívala	přezpívat	k5eAaPmAgFnS	přezpívat
řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
zpěvák	zpěvák	k1gMnSc1	zpěvák
Will	Will	k1gMnSc1	Will
Young	Young	k1gMnSc1	Young
a	a	k8xC	a
portorikánský	portorikánský	k2eAgMnSc1d1	portorikánský
písničkář	písničkář	k1gMnSc1	písničkář
José	Josá	k1gFnSc2	Josá
Feliciano	Feliciana	k1gFnSc5	Feliciana
tak	tak	k6eAd1	tak
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
The	The	k1gFnSc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Break	break	k1gInSc4	break
on	on	k3xPp3gMnSc1	on
Through	Through	k1gMnSc1	Through
(	(	kIx(	(
<g/>
To	ten	k3xDgNnSc1	ten
the	the	k?	the
Other	Othero	k1gNnPc2	Othero
Side	Side	k1gInSc1	Side
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
"	"	kIx"	"
<g/>
Light	Light	k1gInSc1	Light
My	my	k3xPp1nPc1	my
Fire	Firus	k1gMnSc5	Firus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
"	"	kIx"	"
<g/>
People	People	k1gFnSc1	People
Are	ar	k1gInSc5	ar
Strange	Strangus	k1gMnSc5	Strangus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Love	lov	k1gInSc5	lov
Me	Me	k1gFnSc2	Me
Two	Two	k1gMnSc1	Two
Times	Times	k1gMnSc1	Times
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Unknown	Unknown	k1gMnSc1	Unknown
Soldier	Soldier	k1gMnSc1	Soldier
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
,	,	kIx,	,
I	i	k9	i
Love	lov	k1gInSc5	lov
You	You	k1gMnSc5	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
"	"	kIx"	"
<g/>
Touch	Touch	k1gMnSc1	Touch
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
"	"	kIx"	"
<g/>
Wishful	Wishful	k1gInSc1	Wishful
Sinful	Sinful	k1gInSc1	Sinful
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1969	[number]	k4	1969
"	"	kIx"	"
<g/>
Tell	Tell	k1gMnSc1	Tell
All	All	k1gMnSc1	All
the	the	k?	the
People	People	k1gMnSc1	People
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
"	"	kIx"	"
<g/>
Runnin	Runnin	k1gInSc1	Runnin
<g/>
'	'	kIx"	'
Blue	Blue	k1gInSc1	Blue
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
"	"	kIx"	"
<g/>
You	You	k1gFnPc2	You
Make	Make	k1gInSc1	Make
Me	Me	k1gMnSc1	Me
Real	Real	k1gInSc1	Real
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
"	"	kIx"	"
<g/>
Roadhouse	Roadhouse	k1gFnSc1	Roadhouse
Blues	blues	k1gNnSc2	blues
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Her	hra	k1gFnPc2	hra
Madly	Madla	k1gFnSc2	Madla
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
"	"	kIx"	"
<g/>
Riders	Riders	k1gInSc1	Riders
on	on	k3xPp3gInSc1	on
the	the	k?	the
Storm	Storm	k1gInSc1	Storm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
The	The	k1gFnPc2	The
Doors	Doorsa	k1gFnPc2	Doorsa
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
Strange	Strange	k1gNnSc2	Strange
Days	Daysa	k1gFnPc2	Daysa
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
Waiting	Waiting	k1gInSc1	Waiting
for	forum	k1gNnPc2	forum
the	the	k?	the
Sun	Sun	kA	Sun
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
The	The	k1gFnSc1	The
Soft	Soft	k?	Soft
Parade	Parad	k1gInSc5	Parad
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
Morrison	Morrisona	k1gFnPc2	Morrisona
Hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
L.A.	L.A.	k1gFnPc2	L.A.
Woman	Womana	k1gFnPc2	Womana
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
Bez	bez	k7c2	bez
Morrisona	Morrison	k1gMnSc2	Morrison
ještě	ještě	k6eAd1	ještě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
alba	alba	k1gFnSc1	alba
<g/>
:	:	kIx,	:
Other	Other	k1gMnSc1	Other
Voices	Voices	k1gMnSc1	Voices
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
Full	Full	k1gInSc1	Full
Circle	Circle	k1gFnSc2	Circle
<g/>
,	,	kIx,	,
1972	[number]	k4	1972
An	An	k1gMnPc2	An
American	American	k1gMnSc1	American
Prayer	Prayer	k1gMnSc1	Prayer
<g/>
,	,	kIx,	,
1978	[number]	k4	1978
</s>
