<s>
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
je	být	k5eAaImIp3nS	být
úzkotrupé	úzkotrupý	k2eAgNnSc4d1	úzkotrupý
dvoumotorové	dvoumotorový	k2eAgNnSc4d1	dvoumotorové
proudové	proudový	k2eAgNnSc4d1	proudové
dopravní	dopravní	k2eAgNnSc4d1	dopravní
letadlo	letadlo	k1gNnSc4	letadlo
pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
a	a	k8xC	a
střední	střední	k2eAgFnPc4d1	střední
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
dopravní	dopravní	k2eAgInSc4d1	dopravní
letoun	letoun	k1gInSc4	letoun
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
vyrobený	vyrobený	k2eAgInSc1d1	vyrobený
již	již	k9	již
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
300	[number]	k4	300
exemplářích	exemplář	k1gInPc6	exemplář
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
jako	jako	k8xS	jako
menší	malý	k2eAgNnSc1d2	menší
dvoumotorové	dvoumotorový	k2eAgNnSc1d1	dvoumotorové
letadlo	letadlo	k1gNnSc1	letadlo
s	s	k7c7	s
nižšími	nízký	k2eAgInPc7d2	nižší
náklady	náklad	k1gInPc7	náklad
na	na	k7c4	na
provoz	provoz	k1gInSc4	provoz
odvozené	odvozený	k2eAgInPc4d1	odvozený
od	od	k7c2	od
Boeingu	boeing	k1gInSc2	boeing
707	[number]	k4	707
a	a	k8xC	a
Boeingu	boeing	k1gInSc2	boeing
727	[number]	k4	727
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
letadla	letadlo	k1gNnSc2	letadlo
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
9	[number]	k4	9
modelů	model	k1gInPc2	model
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
přepravu	přeprava	k1gFnSc4	přeprava
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
od	od	k7c2	od
85	[number]	k4	85
až	až	k9	až
do	do	k7c2	do
215	[number]	k4	215
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
737	[number]	k4	737
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
úzkotrupé	úzkotrupý	k2eAgNnSc1d1	úzkotrupý
dopravní	dopravní	k2eAgNnSc1d1	dopravní
letadlo	letadlo	k1gNnSc1	letadlo
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Boeing	boeing	k1gInSc1	boeing
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
společností	společnost	k1gFnPc2	společnost
vyráběno	vyrábět	k5eAaImNgNnS	vyrábět
ve	v	k7c6	v
variantách	varianta	k1gFnPc6	varianta
-600	-600	k4	-600
<g/>
,	,	kIx,	,
-700	-700	k4	-700
<g/>
,	,	kIx,	,
-800	-800	k4	-800
<g/>
,	,	kIx,	,
-900	-900	k4	-900
<g/>
.	.	kIx.	.
</s>
<s>
Modernizovaná	modernizovaný	k2eAgFnSc1d1	modernizovaná
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
motory	motor	k1gInPc7	motor
–	–	k?	–
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
MAX	Max	k1gMnSc1	Max
má	mít	k5eAaImIp3nS	mít
přijít	přijít	k5eAaPmF	přijít
na	na	k7c4	na
trh	trh	k1gInSc4	trh
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
a	a	k8xC	a
první	první	k4xOgFnSc2	první
verze	verze	k1gFnSc2	verze
Boeing	boeing	k1gInSc4	boeing
737-100	[number]	k4	737-100
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
leteckého	letecký	k2eAgInSc2d1	letecký
provozu	provoz	k1gInSc2	provoz
jako	jako	k8xC	jako
komerční	komerční	k2eAgNnSc4d1	komerční
proudové	proudový	k2eAgNnSc4d1	proudové
letadlo	letadlo	k1gNnSc4	letadlo
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
verze	verze	k1gFnSc1	verze
737-200	[number]	k4	737-200
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
služby	služba	k1gFnSc2	služba
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
společnost	společnost	k1gFnSc1	společnost
Boeing	boeing	k1gInSc4	boeing
vyrábět	vyrábět	k5eAaImF	vyrábět
verze	verze	k1gFnPc4	verze
-300	-300	k4	-300
<g/>
,	,	kIx,	,
-400	-400	k4	-400
a	a	k8xC	a
-500	-500	k4	-500
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
označované	označovaný	k2eAgFnPc1d1	označovaná
i	i	k8xC	i
jako	jako	k8xC	jako
série	série	k1gFnSc1	série
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
Classic	Classice	k1gFnPc2	Classice
<g/>
.	.	kIx.	.
</s>
<s>
Modelem	model	k1gInSc7	model
série	série	k1gFnSc2	série
737	[number]	k4	737
Classic	Classice	k1gFnPc2	Classice
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
na	na	k7c6	na
kapacitě	kapacita	k1gFnSc6	kapacita
a	a	k8xC	a
proudových	proudový	k2eAgInPc6d1	proudový
motorech	motor	k1gInPc6	motor
CFM56	CFM56	k1gFnSc2	CFM56
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vylepšenými	vylepšený	k2eAgNnPc7d1	vylepšené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
představila	představit	k5eAaPmAgFnS	představit
společnost	společnost	k1gFnSc1	společnost
model	model	k1gInSc1	model
737	[number]	k4	737
Next	Nexta	k1gFnPc2	Nexta
Generation	Generation	k1gInSc4	Generation
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
změnami	změna	k1gFnPc7	změna
jako	jako	k8xS	jako
přestavěná	přestavěný	k2eAgNnPc1d1	přestavěné
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
obnovený	obnovený	k2eAgInSc1d1	obnovený
kokpit	kokpit	k1gInSc1	kokpit
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
interiér	interiér	k1gInSc1	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
737	[number]	k4	737
New	New	k1gFnSc2	New
Generation	Generation	k1gInSc1	Generation
jsou	být	k5eAaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
-600	-600	k4	-600
<g/>
,	,	kIx,	,
-700	-700	k4	-700
<g/>
,	,	kIx,	,
-800	-800	k4	-800
<g/>
,	,	kIx,	,
a-	a-	k?	a-
<g/>
900	[number]	k4	900
<g/>
ER	ER	kA	ER
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
délkou	délka	k1gFnSc7	délka
od	od	k7c2	od
31	[number]	k4	31
<g/>
m	m	kA	m
do	do	k7c2	do
42	[number]	k4	42
<g/>
m.	m.	k?	m.
Byznys	byznys	k1gInSc1	byznys
verze	verze	k1gFnSc1	verze
737	[number]	k4	737
Next	Nexta	k1gFnPc2	Nexta
Generation	Generation	k1gInSc4	Generation
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
BBJ	BBJ	kA	BBJ
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
737	[number]	k4	737
je	být	k5eAaImIp3nS	být
nejprodávanější	prodávaný	k2eAgNnSc1d3	nejprodávanější
tryskové	tryskový	k2eAgNnSc1d1	tryskové
dopravní	dopravní	k2eAgNnSc1d1	dopravní
letadlo	letadlo	k1gNnSc1	letadlo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
letectví	letectví	k1gNnSc2	letectví
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
737	[number]	k4	737
se	se	k3xPyFc4	se
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
Boeing	boeing	k1gInSc1	boeing
nepřetržitě	přetržitě	k6eNd1	přetržitě
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
9	[number]	k4	9
335	[number]	k4	335
dosud	dosud	k6eAd1	dosud
dodaných	dodaný	k2eAgNnPc2d1	dodané
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
prosinec	prosinec	k1gInSc1	prosinec
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
a	a	k8xC	a
4	[number]	k4	4
404	[number]	k4	404
objednávek	objednávka	k1gFnPc2	objednávka
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
737	[number]	k4	737
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
Boeing	boeing	k1gInSc1	boeing
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Renton	Renton	k1gInSc1	Renton
<g/>
,	,	kIx,	,
stát	stát	k1gInSc1	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
737	[number]	k4	737
<g/>
-ček	-čeka	k1gFnPc2	-čeka
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
dříve	dříve	k6eAd2	dříve
provozované	provozovaný	k2eAgFnPc4d1	provozovaná
letadly	letadlo	k1gNnPc7	letadlo
707	[number]	k4	707
<g/>
,	,	kIx,	,
727	[number]	k4	727
<g/>
,	,	kIx,	,
757	[number]	k4	757
<g/>
,	,	kIx,	,
DC-	DC-	k1gFnSc1	DC-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
a	a	k8xC	a
MD-	MD-	k1gFnSc1	MD-
<g/>
80	[number]	k4	80
<g/>
/	/	kIx~	/
<g/>
MD-	MD-	k1gFnSc2	MD-
<g/>
90	[number]	k4	90
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
představuje	představovat	k5eAaImIp3nS	představovat
pro	pro	k7c4	pro
letadlo	letadlo	k1gNnSc4	letadlo
největšího	veliký	k2eAgMnSc2d3	veliký
konkurenta	konkurent	k1gMnSc2	konkurent
rodina	rodina	k1gFnSc1	rodina
letadel	letadlo	k1gNnPc2	letadlo
Airbus	airbus	k1gInSc4	airbus
A	a	k8xC	a
<g/>
320	[number]	k4	320
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
průměrně	průměrně	k6eAd1	průměrně
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
1	[number]	k4	1
250	[number]	k4	250
Boeingů	boeing	k1gInPc2	boeing
737	[number]	k4	737
a	a	k8xC	a
každých	každý	k3xTgFnPc2	každý
5	[number]	k4	5
sekund	sekunda	k1gFnPc2	sekunda
přistávala	přistávat	k5eAaImAgFnS	přistávat
nebo	nebo	k8xC	nebo
vzlétala	vzlétat	k5eAaImAgFnS	vzlétat
2	[number]	k4	2
letadla	letadlo	k1gNnSc2	letadlo
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Boeing	boeing	k1gInSc1	boeing
se	se	k3xPyFc4	se
věnovala	věnovat	k5eAaImAgFnS	věnovat
studiu	studio	k1gNnSc3	studio
návrhu	návrh	k1gInSc2	návrh
proudových	proudový	k2eAgNnPc2d1	proudové
letadel	letadlo	k1gNnPc2	letadlo
s	s	k7c7	s
kratším	krátký	k2eAgInSc7d2	kratší
doletem	dolet	k1gInSc7	dolet
a	a	k8xC	a
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
doplňujícího	doplňující	k2eAgNnSc2d1	doplňující
letadla	letadlo	k1gNnSc2	letadlo
k	k	k7c3	k
Boeingu	boeing	k1gInSc3	boeing
727	[number]	k4	727
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
tratě	trata	k1gFnSc6	trata
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
obsazeností	obsazenost	k1gFnSc7	obsazenost
<g/>
.	.	kIx.	.
</s>
<s>
Přípravné	přípravný	k2eAgFnSc2d1	přípravná
projektové	projektový	k2eAgFnSc2d1	projektová
práce	práce	k1gFnSc2	práce
začaly	začít	k5eAaPmAgFnP	začít
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1964	[number]	k4	1964
a	a	k8xC	a
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
průzkum	průzkum	k1gInSc1	průzkum
trhu	trh	k1gInSc2	trh
Boeingu	boeing	k1gInSc2	boeing
přinesl	přinést	k5eAaPmAgMnS	přinést
plány	plán	k1gInPc4	plán
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
50	[number]	k4	50
až	až	k8xS	až
60	[number]	k4	60
místného	místný	k2eAgNnSc2d1	místné
dopravního	dopravní	k2eAgNnSc2d1	dopravní
letadla	letadlo	k1gNnSc2	letadlo
pro	pro	k7c4	pro
tratě	trať	k1gFnPc4	trať
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
80	[number]	k4	80
až	až	k9	až
1	[number]	k4	1
600	[number]	k4	600
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
se	s	k7c7	s
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgMnSc7	první
zákazníkem	zákazník	k1gMnSc7	zákazník
<g/>
,	,	kIx,	,
s	s	k7c7	s
objednávkou	objednávka	k1gFnSc7	objednávka
21	[number]	k4	21
letadel	letadlo	k1gNnPc2	letadlo
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
částce	částka	k1gFnSc6	částka
67	[number]	k4	67
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
r.	r.	kA	r.
1965	[number]	k4	1965
190,28	[number]	k4	190,28
milionů	milion	k4xCgInPc2	milion
USD	USD	kA	USD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tato	tento	k3xDgFnSc1	tento
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
dostala	dostat	k5eAaPmAgFnS	dostat
záruku	záruka	k1gFnSc4	záruka
od	od	k7c2	od
Boeingu	boeing	k1gInSc2	boeing
<g/>
,	,	kIx,	,
že	že	k8xS	že
projekt	projekt	k1gInSc1	projekt
737	[number]	k4	737
nebude	být	k5eNaImBp3nS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Konzultace	konzultace	k1gFnPc1	konzultace
a	a	k8xC	a
domluvení	domluvení	k1gNnPc1	domluvení
se	se	k3xPyFc4	se
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
v	v	k7c6	v
předchozím	předchozí	k2eAgNnSc6d1	předchozí
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
měla	mít	k5eAaImAgFnS	mít
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
projektu	projekt	k1gInSc2	projekt
za	za	k7c4	za
následek	následek	k1gInSc4	následek
zvýšení	zvýšení	k1gNnSc2	zvýšení
sedadlové	sedadlový	k2eAgFnSc2d1	sedadlová
kapacity	kapacita	k1gFnSc2	kapacita
na	na	k7c4	na
100	[number]	k4	100
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Boeing	boeing	k1gInSc4	boeing
objednávku	objednávka	k1gFnSc4	objednávka
společnosti	společnost	k1gFnSc2	společnost
United	United	k1gMnSc1	United
Airlines	Airlines	k1gMnSc1	Airlines
na	na	k7c4	na
40	[number]	k4	40
letadel	letadlo	k1gNnPc2	letadlo
737	[number]	k4	737
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gMnSc1	United
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgNnSc4d2	veliký
letadlo	letadlo	k1gNnSc4	letadlo
než	než	k8xS	než
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
Boeing	boeing	k1gInSc1	boeing
protáhl	protáhnout	k5eAaPmAgInS	protáhnout
trup	trup	k1gInSc4	trup
o	o	k7c4	o
91	[number]	k4	91
centimetrů	centimetr	k1gInPc2	centimetr
směrem	směr	k1gInSc7	směr
dopředu	dopředu	k6eAd1	dopředu
a	a	k8xC	a
102	[number]	k4	102
centimetrů	centimetr	k1gInPc2	centimetr
za	za	k7c4	za
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgFnSc1d2	delší
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
označena	označen	k2eAgFnSc1d1	označena
jako	jako	k8xS	jako
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vycházela	vycházet	k5eAaImAgFnS	vycházet
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
verze	verze	k1gFnSc1	verze
737-100	[number]	k4	737-100
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
trupem	trup	k1gInSc7	trup
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
návrhové	návrhový	k2eAgFnPc1d1	návrhová
práce	práce	k1gFnPc1	práce
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
variantách	varianta	k1gFnPc6	varianta
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc4	boeing
zaostával	zaostávat	k5eAaImAgMnS	zaostávat
za	za	k7c7	za
svými	svůj	k3xOyFgMnPc7	svůj
konkurenty	konkurent	k1gMnPc7	konkurent
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
projekt	projekt	k1gInSc1	projekt
737	[number]	k4	737
protože	protože	k8xS	protože
u	u	k7c2	u
konkurenčních	konkurenční	k2eAgNnPc2d1	konkurenční
letadel	letadlo	k1gNnPc2	letadlo
BAC	bac	k0	bac
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
Douglas	Douglas	k1gMnSc1	Douglas
DC-9	DC-9	k1gMnSc1	DC-9
a	a	k8xC	a
Fokker	Fokker	k1gMnSc1	Fokker
F.	F.	kA	F.
<g/>
28	[number]	k4	28
již	již	k6eAd1	již
probíhalo	probíhat	k5eAaImAgNnS	probíhat
letové	letový	k2eAgNnSc1d1	letové
ověřování	ověřování	k1gNnSc1	ověřování
a	a	k8xC	a
certifikace	certifikace	k1gFnSc1	certifikace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
urychlení	urychlení	k1gNnSc4	urychlení
vývoje	vývoj	k1gInSc2	vývoj
použila	použít	k5eAaPmAgFnS	použít
společnost	společnost	k1gFnSc1	společnost
Boeing	boeing	k1gInSc1	boeing
60	[number]	k4	60
%	%	kIx~	%
struktury	struktura	k1gFnSc2	struktura
a	a	k8xC	a
systémů	systém	k1gInPc2	systém
z	z	k7c2	z
již	již	k6eAd1	již
existujícího	existující	k2eAgInSc2d1	existující
typu	typ	k1gInSc2	typ
727	[number]	k4	727
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
průřez	průřez	k1gInSc1	průřez
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
trup	trup	k1gInSc1	trup
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
6	[number]	k4	6
sedadlové	sedadlový	k2eAgNnSc4d1	sedadlové
uspořádání	uspořádání	k1gNnSc4	uspořádání
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
5	[number]	k4	5
sedadlovým	sedadlový	k2eAgNnSc7d1	sedadlové
uspořádáním	uspořádání	k1gNnSc7	uspořádání
u	u	k7c2	u
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
letadel	letadlo	k1gNnPc2	letadlo
BAC	bac	k0	bac
1-11	[number]	k4	1-11
a	a	k8xC	a
DC-	DC-	k1gMnSc1	DC-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktéři	konstruktér	k1gMnPc1	konstruktér
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
osadit	osadit	k5eAaPmF	osadit
gondoly	gondola	k1gFnPc4	gondola
motorů	motor	k1gInPc2	motor
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
spodní	spodní	k2eAgFnSc4d1	spodní
stranu	strana	k1gFnSc4	strana
křídel	křídlo	k1gNnPc2	křídlo
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
délky	délka	k1gFnSc2	délka
přistávacího	přistávací	k2eAgInSc2d1	přistávací
podvozku	podvozek	k1gInSc2	podvozek
a	a	k8xC	a
ponechání	ponechání	k1gNnSc2	ponechání
motorů	motor	k1gInPc2	motor
níže	nízce	k6eAd2	nízce
pro	pro	k7c4	pro
snadnější	snadný	k2eAgFnSc4d2	snazší
obsluhu	obsluha	k1gFnSc4	obsluha
a	a	k8xC	a
prohlídky	prohlídka	k1gFnPc4	prohlídka
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
testováno	testovat	k5eAaImNgNnS	testovat
mnoho	mnoho	k4c1	mnoho
variant	varianta	k1gFnPc2	varianta
tlouštěk	tloušťka	k1gFnPc2	tloušťka
pro	pro	k7c4	pro
podpěry	podpěra	k1gFnPc4	podpěra
uchycení	uchycení	k1gNnSc2	uchycení
motoru	motor	k1gInSc2	motor
v	v	k7c6	v
aerodynamickém	aerodynamický	k2eAgInSc6d1	aerodynamický
tunely	tunel	k1gInPc7	tunel
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
větší	veliký	k2eAgFnPc4d2	veliký
rychlosti	rychlost	k1gFnPc4	rychlost
nejvíce	hodně	k6eAd3	hodně
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
drážku	drážka	k1gFnSc4	drážka
mezi	mezi	k7c7	mezi
křídlem	křídlo	k1gNnSc7	křídlo
a	a	k8xC	a
horní	horní	k2eAgFnSc7d1	horní
stranou	strana	k1gFnSc7	strana
gondoly	gondola	k1gFnSc2	gondola
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
vnější	vnější	k2eAgFnSc6d1	vnější
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
plánováno	plánovat	k5eAaImNgNnS	plánovat
použití	použití	k1gNnSc1	použití
velmi	velmi	k6eAd1	velmi
podobných	podobný	k2eAgFnPc2d1	podobná
částí	část	k1gFnPc2	část
aerodynamických	aerodynamický	k2eAgInPc2d1	aerodynamický
profilů	profil	k1gInPc2	profil
křídla	křídlo	k1gNnSc2	křídlo
jako	jako	k8xS	jako
u	u	k7c2	u
modelů	model	k1gInPc2	model
707	[number]	k4	707
a	a	k8xC	a
727	[number]	k4	727
ač	ač	k8xS	ač
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
tlustší	tlustý	k2eAgNnSc4d2	tlustší
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
podstatné	podstatný	k2eAgNnSc1d1	podstatné
zlepšení	zlepšení	k1gNnSc1	zlepšení
odporových	odporový	k2eAgFnPc2d1	odporová
charakteristik	charakteristika	k1gFnPc2	charakteristika
při	při	k7c6	při
vyšších	vysoký	k2eAgNnPc6d2	vyšší
Machových	Machových	k2eAgNnPc6d1	Machových
číslech	číslo	k1gNnPc6	číslo
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
změnou	změna	k1gFnSc7	změna
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
gondoly	gondola	k1gFnSc2	gondola
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
proudový	proudový	k2eAgInSc1d1	proudový
motor	motor	k1gInSc1	motor
Pratt	Pratt	k1gInSc1	Pratt
&	&	k?	&
Whitney	Whitnea	k1gMnSc2	Whitnea
JT8D-1	JT8D-1	k1gMnSc2	JT8D-1
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obtokovým	obtokový	k2eAgInSc7d1	obtokový
poměrem	poměr	k1gInSc7	poměr
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
motory	motor	k1gInPc7	motor
nainstalovanými	nainstalovaný	k2eAgInPc7d1	nainstalovaný
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
letounu	letoun	k1gInSc2	letoun
se	se	k3xPyFc4	se
Boeing	boeing	k1gInSc1	boeing
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
osadit	osadit	k5eAaPmF	osadit
na	na	k7c4	na
trup	trup	k1gInSc4	trup
konvenční	konvenční	k2eAgFnSc4d1	konvenční
vodorovnou	vodorovný	k2eAgFnSc4d1	vodorovná
ocasní	ocasní	k2eAgFnSc4d1	ocasní
plochu	plocha	k1gFnSc4	plocha
před	před	k7c7	před
ocasním	ocasní	k2eAgInSc7d1	ocasní
tvarem	tvar	k1gInSc7	tvar
T	T	kA	T
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Boeingu	boeing	k1gInSc2	boeing
727	[number]	k4	727
<g/>
.	.	kIx.	.
</s>
<s>
B-737-100	B-737-100	k4	B-737-100
byla	být	k5eAaImAgFnS	být
nejmenší	malý	k2eAgFnSc1d3	nejmenší
a	a	k8xC	a
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgNnSc2	tento
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
společností	společnost	k1gFnSc7	společnost
používající	používající	k2eAgInSc4d1	používající
Boeing	boeing	k1gInSc4	boeing
737-100	[number]	k4	737-100
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
německá	německý	k2eAgFnSc1d1	německá
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odebrala	odebrat	k5eAaPmAgFnS	odebrat
22	[number]	k4	22
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc1	pět
koupila	koupit	k5eAaPmAgFnS	koupit
Malaysia-Singapore	Malaysia-Singapor	k1gInSc5	Malaysia-Singapor
Airlines	Airlines	k1gInSc4	Airlines
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
kusy	kus	k1gInPc4	kus
kolumbijská	kolumbijský	k2eAgFnSc1d1	kolumbijská
Avianca	Avianca	k1gFnSc1	Avianca
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
letadlo	letadlo	k1gNnSc4	letadlo
kapacitu	kapacita	k1gFnSc4	kapacita
60-85	[number]	k4	60-85
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
;	;	kIx,	;
po	po	k7c6	po
konzultacích	konzultace	k1gFnPc6	konzultace
s	s	k7c7	s
prvním	první	k4xOgMnSc7	první
objednatelem	objednatel	k1gMnSc7	objednatel
-	-	kIx~	-
Lufthansou	Lufthansa	k1gFnSc7	Lufthansa
-	-	kIx~	-
bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
letadlo	letadlo	k1gNnSc1	letadlo
pro	pro	k7c4	pro
100	[number]	k4	100
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
zkonstruováno	zkonstruovat	k5eAaPmNgNnS	zkonstruovat
30	[number]	k4	30
strojů	stroj	k1gInPc2	stroj
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
737-200	[number]	k4	737-200
je	být	k5eAaImIp3nS	být
upravená	upravený	k2eAgFnSc1d1	upravená
(	(	kIx(	(
<g/>
prodloužená	prodloužená	k1gFnSc1	prodloužená
o	o	k7c4	o
1,93	[number]	k4	1,93
m	m	kA	m
<g/>
)	)	kIx)	)
verze	verze	k1gFnSc1	verze
modelu	model	k1gInSc2	model
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Zálet	zálet	k1gInSc1	zálet
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
společností	společnost	k1gFnSc7	společnost
používající	používající	k2eAgFnSc7d1	používající
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
byla	být	k5eAaImAgFnS	být
americká	americký	k2eAgFnSc1d1	americká
United	United	k1gMnSc1	United
Airlines	Airlines	k1gInSc1	Airlines
(	(	kIx(	(
<g/>
75	[number]	k4	75
letounů	letoun	k1gInPc2	letoun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
zákazníkem	zákazník	k1gMnSc7	zákazník
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
Western	western	k1gInSc4	western
Airlines	Airlinesa	k1gFnPc2	Airlinesa
(	(	kIx(	(
<g/>
30	[number]	k4	30
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Piedmont	Piedmont	k1gMnSc1	Piedmont
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
GATX-Boothe	GATX-Boothe	k1gFnSc1	GATX-Boothe
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
All	All	k1gFnSc1	All
Nippon	Nippon	k1gMnSc1	Nippon
Airways	Airways	k1gInSc1	Airways
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pacific	Pacific	k1gMnSc1	Pacific
Southwest	Southwest	k1gMnSc1	Southwest
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
Boeing	boeing	k1gInSc1	boeing
737-200	[number]	k4	737-200
měla	mít	k5eAaImAgFnS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
115-130	[number]	k4	115-130
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
737-200C	[number]	k4	737-200C
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
nákladovými	nákladový	k2eAgInPc7d1	nákladový
vraty	vrat	k1gInPc7	vrat
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
trupu	trup	k1gInSc2	trup
za	za	k7c7	za
předními	přední	k2eAgFnPc7d1	přední
dveřmi	dveře	k1gFnPc7	dveře
a	a	k8xC	a
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
dopravu	doprava	k1gFnSc4	doprava
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
135	[number]	k4	135
<g/>
.	.	kIx.	.
kusu	kus	k1gInSc2	kus
<g/>
,	,	kIx,	,
dodaného	dodaný	k2eAgMnSc2d1	dodaný
United	United	k1gMnSc1	United
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zaváděny	zaváděn	k2eAgFnPc4d1	zaváděna
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
výkonnějších	výkonný	k2eAgInPc2d2	výkonnější
motorů	motor	k1gInPc2	motor
JT8D-15	JT8D-15	k1gFnSc2	JT8D-15
s	s	k7c7	s
účinnějšími	účinný	k2eAgMnPc7d2	účinnější
obraceči	obraceč	k1gMnPc7	obraceč
tahu	tah	k1gInSc2	tah
a	a	k8xC	a
změněnou	změněný	k2eAgFnSc7d1	změněná
geometriií	geometriie	k1gFnSc7	geometriie
Kruegerových	Kruegerův	k2eAgFnPc2d1	Kruegerův
klapek	klapka	k1gFnPc2	klapka
a	a	k8xC	a
slotu	slot	k1gInSc2	slot
na	na	k7c6	na
náběžné	náběžný	k2eAgFnSc6d1	náběžná
hraně	hrana	k1gFnSc6	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
postupně	postupně	k6eAd1	postupně
zaváděná	zaváděný	k2eAgNnPc1d1	zaváděné
zlepšení	zlepšení	k1gNnPc1	zlepšení
vyvrcholila	vyvrcholit	k5eAaPmAgNnP	vyvrcholit
ve	v	k7c4	v
verzi	verze	k1gFnSc4	verze
Advanced	Advanced	k1gInSc1	Advanced
737	[number]	k4	737
se	s	k7c7	s
startovní	startovní	k2eAgFnSc7d1	startovní
hmotností	hmotnost	k1gFnSc7	hmotnost
52	[number]	k4	52
620	[number]	k4	620
kg	kg	kA	kg
a	a	k8xC	a
novou	nový	k2eAgFnSc7d1	nová
nádrží	nádrž	k1gFnSc7	nádrž
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
osazená	osazený	k2eAgFnSc1d1	osazená
motory	motor	k1gInPc4	motor
JT	JT	kA	JT
<g/>
8	[number]	k4	8
<g/>
D-	D-	k1gFnSc2	D-
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1980	[number]	k4	1980
byly	být	k5eAaImAgInP	být
první	první	k4xOgNnSc4	první
z	z	k7c2	z
28	[number]	k4	28
kusů	kus	k1gInPc2	kus
verze	verze	k1gFnSc1	verze
Advanced	Advanced	k1gInSc1	Advanced
dodány	dodat	k5eAaPmNgInP	dodat
dopravci	dopravce	k1gMnPc1	dopravce
British	British	k1gMnSc1	British
Airways	Airways	k1gInSc1	Airways
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
1	[number]	k4	1
114	[number]	k4	114
strojů	stroj	k1gInPc2	stroj
modelu	model	k1gInSc2	model
-200	-200	k4	-200
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
modelu	model	k1gInSc2	model
737-200	[number]	k4	737-200
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
také	také	k6eAd1	také
19	[number]	k4	19
strojů	stroj	k1gInPc2	stroj
T-43A	T-43A	k1gFnSc2	T-43A
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
vojenských	vojenský	k2eAgMnPc2d1	vojenský
letovodů	letovod	k1gMnPc2	letovod
<g/>
.	.	kIx.	.
737-300	[number]	k4	737-300
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
základní	základní	k2eAgInSc1d1	základní
model	model	k1gInSc1	model
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
Boeingu	boeing	k1gInSc2	boeing
737	[number]	k4	737
zalétaný	zalétaný	k2eAgInSc1d1	zalétaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgMnSc1d2	delší
o	o	k7c4	o
2,13	[number]	k4	2,13
m	m	kA	m
a	a	k8xC	a
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
100	[number]	k4	100
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
zástavbu	zástavba	k1gFnSc4	zástavba
dalších	další	k2eAgFnPc2d1	další
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
po	po	k7c6	po
šesti	šest	k4xCc6	šest
sedadlech	sedadlo	k1gNnPc6	sedadlo
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
řady	řada	k1gFnSc2	řada
po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
sedadlech	sedadlo	k1gNnPc6	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kabině	kabina	k1gFnSc6	kabina
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevila	objevit	k5eAaPmAgFnS	objevit
kombinace	kombinace	k1gFnSc1	kombinace
klasických	klasický	k2eAgInPc2d1	klasický
budíků	budík	k1gInPc2	budík
a	a	k8xC	a
nového	nový	k2eAgInSc2d1	nový
EFIS	EFIS	kA	EFIS
<g/>
/	/	kIx~	/
<g/>
Electronic	Electronice	k1gFnPc2	Electronice
Flight	Flight	k2eAgInSc4d1	Flight
Instrumention	Instrumention	k1gInSc4	Instrumention
System	Syst	k1gInSc7	Syst
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
Boeingu	boeing	k1gInSc2	boeing
737	[number]	k4	737
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
nové	nový	k2eAgInPc4d1	nový
motory	motor	k1gInPc4	motor
značky	značka	k1gFnSc2	značka
CFM	CFM	kA	CFM
(	(	kIx(	(
<g/>
model	model	k1gInSc1	model
56	[number]	k4	56
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
byly	být	k5eAaImAgFnP	být
nejradikálnější	radikální	k2eAgFnPc1d3	nejradikálnější
změny	změna	k1gFnPc1	změna
oproti	oproti	k7c3	oproti
verzím	verze	k1gFnPc3	verze
-100	-100	k4	-100
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
737-300	[number]	k4	737-300
má	mít	k5eAaImIp3nS	mít
při	při	k7c6	při
standardním	standardní	k2eAgNnSc6d1	standardní
<g/>
,	,	kIx,	,
dvoutřídním	dvoutřídní	k2eAgNnSc6d1	dvoutřídní
uspořádání	uspořádání	k1gNnSc6	uspořádání
kapacitu	kapacita	k1gFnSc4	kapacita
128	[number]	k4	128
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
148	[number]	k4	148
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
objednáno	objednat	k5eAaPmNgNnS	objednat
1	[number]	k4	1
104	[number]	k4	104
strojů	stroj	k1gInPc2	stroj
modelu	model	k1gInSc2	model
-300	-300	k4	-300
<g/>
.	.	kIx.	.
737-400	[number]	k4	737-400
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
jako	jako	k9	jako
150	[number]	k4	150
<g/>
-místná	ístný	k2eAgFnSc1d1	-místný
náhrada	náhrada	k1gFnSc1	náhrada
letadel	letadlo	k1gNnPc2	letadlo
Boeing	boeing	k1gInSc4	boeing
727	[number]	k4	727
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
varianty	varianta	k1gFnSc2	varianta
Boeing	boeing	k1gInSc4	boeing
737-400	[number]	k4	737-400
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1986	[number]	k4	1986
na	na	k7c6	na
základě	základ	k1gInSc6	základ
požadavku	požadavek	k1gInSc2	požadavek
dopravní	dopravní	k2eAgFnSc2d1	dopravní
společnosti	společnost	k1gFnSc2	společnost
Piedmont	Piedmont	k1gMnSc1	Piedmont
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1988	[number]	k4	1988
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gMnSc7	její
prvním	první	k4xOgMnSc7	první
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úvodnímu	úvodní	k2eAgInSc3d1	úvodní
vzletu	vzlet	k1gInSc3	vzlet
"	"	kIx"	"
<g/>
čtyřstovky	čtyřstovka	k1gFnSc2	čtyřstovka
<g/>
"	"	kIx"	"
došlo	dojít	k5eAaPmAgNnS	dojít
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
-400	-400	k4	-400
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
také	také	k9	také
mimořádně	mimořádně	k6eAd1	mimořádně
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
flotilu	flotila	k1gFnSc4	flotila
této	tento	k3xDgFnSc2	tento
verze	verze	k1gFnSc2	verze
má	mít	k5eAaImIp3nS	mít
Malaysia	Malaysia	k1gFnSc1	Malaysia
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
737-400	[number]	k4	737-400
má	mít	k5eAaImIp3nS	mít
standardně	standardně	k6eAd1	standardně
kapacitu	kapacita	k1gFnSc4	kapacita
146	[number]	k4	146
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
u	u	k7c2	u
dvoutřídného	dvoutřídný	k2eAgNnSc2d1	dvoutřídný
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
188	[number]	k4	188
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
bylo	být	k5eAaImAgNnS	být
kolem	kolem	k6eAd1	kolem
500	[number]	k4	500
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
737-500	[number]	k4	737-500
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
verze	verze	k1gFnSc1	verze
Boeingu	boeing	k1gInSc2	boeing
737-300	[number]	k4	737-300
a	a	k8xC	a
přímá	přímý	k2eAgFnSc1d1	přímá
náhrada	náhrada	k1gFnSc1	náhrada
modelů	model	k1gInPc2	model
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
108	[number]	k4	108
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
u	u	k7c2	u
dvoutřídní	dvoutřídní	k2eAgFnSc7d1	dvoutřídní
konfiguraci	konfigurace	k1gFnSc4	konfigurace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
133	[number]	k4	133
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vyrobených	vyrobený	k2eAgMnPc2d1	vyrobený
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
400	[number]	k4	400
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
série	série	k1gFnSc1	série
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
letadla	letadlo	k1gNnSc2	letadlo
pro	pro	k7c4	pro
krátké	krátký	k2eAgFnPc4d1	krátká
a	a	k8xC	a
střední	střední	k2eAgFnPc4d1	střední
tratě	trať	k1gFnPc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
odpovědí	odpověď	k1gFnPc2	odpověď
na	na	k7c6	na
konkurenci	konkurence	k1gFnSc6	konkurence
konsorcia	konsorcium	k1gNnSc2	konsorcium
Airbus	airbus	k1gInSc1	airbus
s	s	k7c7	s
modely	model	k1gInPc7	model
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
320	[number]	k4	320
<g/>
.	.	kIx.	.
</s>
<s>
NG	NG	kA	NG
verze	verze	k1gFnSc1	verze
má	mít	k5eAaImIp3nS	mít
novější	nový	k2eAgFnSc1d2	novější
digitální	digitální	k2eAgFnSc1d1	digitální
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
skleněný	skleněný	k2eAgMnSc1d1	skleněný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
kokpit	kokpit	k1gInSc1	kokpit
<g/>
,	,	kIx,	,
nová	nový	k2eAgNnPc4d1	nové
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
737-600	[number]	k4	737-600
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
NG	NG	kA	NG
verzí	verze	k1gFnSc7	verze
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
verzi	verze	k1gFnSc4	verze
-600	-600	k4	-600
byl	být	k5eAaImAgInS	být
model	model	k1gInSc1	model
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
737-600	[number]	k4	737-600
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
110	[number]	k4	110
pasažérů	pasažér	k1gMnPc2	pasažér
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třídách	třída	k1gFnPc6	třída
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
133	[number]	k4	133
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
jednotřídka	jednotřídka	k1gFnSc1	jednotřídka
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
mu	on	k3xPp3gMnSc3	on
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
318	[number]	k4	318
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
737-600	[number]	k4	737-600
má	mít	k5eAaImIp3nS	mít
dolet	dolet	k1gInSc4	dolet
2	[number]	k4	2
480	[number]	k4	480
km	km	kA	km
<g/>
,	,	kIx,	,
verze-	verze-	k?	verze-
<g/>
600	[number]	k4	600
<g/>
HGW	HGW	kA	HGW
5	[number]	k4	5
648	[number]	k4	648
km	km	kA	km
<g/>
.	.	kIx.	.
737-700	[number]	k4	737-700
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
model	model	k1gInSc4	model
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
nahradil	nahradit	k5eAaPmAgMnS	nahradit
předchozí	předchozí	k2eAgFnSc3d1	předchozí
verzi	verze	k1gFnSc3	verze
Boeing	boeing	k1gInSc1	boeing
737-300	[number]	k4	737-300
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
i	i	k9	i
koncepčně	koncepčně	k6eAd1	koncepčně
vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nabízen	nabízet	k5eAaImNgInS	nabízet
ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
QC	QC	kA	QC
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
HGW	HGW	kA	HGW
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
ER	ER	kA	ER
a	a	k8xC	a
BBJ	BBJ	kA	BBJ
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
verze	verze	k1gFnSc1	verze
-700	-700	k4	-700
má	mít	k5eAaImIp3nS	mít
u	u	k7c2	u
dvoutřídného	dvoutřídný	k2eAgNnSc2d1	dvoutřídný
uspořádání	uspořádání	k1gNnSc2	uspořádání
kapacitu	kapacita	k1gFnSc4	kapacita
126	[number]	k4	126
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
149	[number]	k4	149
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
mu	on	k3xPp3gMnSc3	on
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
Airbus	airbus	k1gInSc4	airbus
A	a	k9	a
<g/>
319	[number]	k4	319
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
737-700	[number]	k4	737-700
má	mít	k5eAaImIp3nS	mít
dolet	dolet	k1gInSc4	dolet
2	[number]	k4	2
852	[number]	k4	852
km	km	kA	km
<g/>
,	,	kIx,	,
verze-	verze-	k?	verze-
<g/>
700	[number]	k4	700
<g/>
HGW	HGW	kA	HGW
6	[number]	k4	6
037	[number]	k4	037
km	km	kA	km
<g/>
.	.	kIx.	.
737-800	[number]	k4	737-800
je	být	k5eAaImIp3nS	být
prodloužená	prodloužený	k2eAgFnSc1d1	prodloužená
verze	verze	k1gFnSc1	verze
Boeingu	boeing	k1gInSc2	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
modelu	model	k1gInSc2	model
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
400	[number]	k4	400
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
přímo	přímo	k6eAd1	přímo
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
standardním	standardní	k2eAgNnSc6d1	standardní
uspořádání	uspořádání	k1gNnSc6	uspořádání
sedadel	sedadlo	k1gNnPc2	sedadlo
nabízí	nabízet	k5eAaImIp3nS	nabízet
kapacitu	kapacita	k1gFnSc4	kapacita
162	[number]	k4	162
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
189	[number]	k4	189
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc1	boeing
737-800	[number]	k4	737-800
má	mít	k5eAaImIp3nS	mít
dolet	dolet	k1gInSc4	dolet
3	[number]	k4	3
815	[number]	k4	815
km	km	kA	km
<g/>
,	,	kIx,	,
verze-	verze-	k?	verze-
<g/>
800	[number]	k4	800
<g/>
HGW	HGW	kA	HGW
5	[number]	k4	5
445	[number]	k4	445
km	km	kA	km
<g/>
.	.	kIx.	.
737-900	[number]	k4	737-900
/	/	kIx~	/
<g/>
-	-	kIx~	-
<g/>
900	[number]	k4	900
<g/>
ER	ER	kA	ER
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
variantou	varianta	k1gFnSc7	varianta
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
737	[number]	k4	737
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
vývoje	vývoj	k1gInSc2	vývoj
letounu	letoun	k1gInSc2	letoun
Boeing	boeing	k1gInSc4	boeing
737-900	[number]	k4	737-900
stála	stát	k5eAaImAgFnS	stát
objednávka	objednávka	k1gFnSc1	objednávka
dopravní	dopravní	k2eAgFnSc2d1	dopravní
společnosti	společnost	k1gFnSc2	společnost
Alaska	Alasko	k1gNnSc2	Alasko
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
objednala	objednat	k5eAaPmAgFnS	objednat
10	[number]	k4	10
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Konstruktéři	konstruktér	k1gMnPc1	konstruktér
vyšli	vyjít	k5eAaPmAgMnP	vyjít
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
modelu	model	k1gInSc2	model
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vložením	vložení	k1gNnSc7	vložení
dvou	dva	k4xCgInPc2	dva
trupových	trupový	k2eAgMnPc2d1	trupový
segmentů	segment	k1gInPc2	segment
před	před	k7c4	před
a	a	k8xC	a
za	za	k7c4	za
křídlo	křídlo	k1gNnSc4	křídlo
prodloužili	prodloužit	k5eAaPmAgMnP	prodloužit
o	o	k7c4	o
2,60	[number]	k4	2,60
m	m	kA	m
na	na	k7c4	na
celkovou	celkový	k2eAgFnSc4d1	celková
délku	délka	k1gFnSc4	délka
42,1	[number]	k4	42,1
m.	m.	k?	m.
Výroba	výroba	k1gFnSc1	výroba
prvního	první	k4xOgInSc2	první
stroje	stroj	k1gInSc2	stroj
započala	započnout	k5eAaPmAgFnS	započnout
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
ve	v	k7c6	v
Wichitě	Wichita	k1gFnSc6	Wichita
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
konstrukčních	konstrukční	k2eAgFnPc2d1	konstrukční
prací	práce	k1gFnPc2	práce
zapojil	zapojit	k5eAaPmAgInS	zapojit
také	také	k9	také
závod	závod	k1gInSc1	závod
v	v	k7c6	v
Rentonu	Renton	k1gInSc6	Renton
kompletací	kompletace	k1gFnPc2	kompletace
nosníku	nosník	k1gInSc2	nosník
křídla	křídlo	k1gNnSc2	křídlo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
provedl	provést	k5eAaPmAgInS	provést
i	i	k9	i
finální	finální	k2eAgFnSc4d1	finální
montáž	montáž	k1gFnSc4	montáž
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úvodnímu	úvodní	k2eAgInSc3d1	úvodní
vzletu	vzlet	k1gInSc3	vzlet
737-900	[number]	k4	737-900
(	(	kIx(	(
<g/>
výr	výr	k1gMnSc1	výr
<g/>
.	.	kIx.	.
č.	č.	k?	č.
30017	[number]	k4	30017
<g/>
/	/	kIx~	/
<g/>
596	[number]	k4	596
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
737	[number]	k4	737
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
motorů	motor	k1gInPc2	motor
CFM56-7B	CFM56-7B	k1gMnPc2	CFM56-7B
došlo	dojít	k5eAaPmAgNnS	dojít
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
realizovali	realizovat	k5eAaBmAgMnP	realizovat
piloti	pilot	k1gMnPc1	pilot
Mike	Mik	k1gFnSc2	Mik
Carriker	Carriker	k1gMnSc1	Carriker
a	a	k8xC	a
Mark	Mark	k1gMnSc1	Mark
Feuerstein	Feuerstein	k1gMnSc1	Feuerstein
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
let	léto	k1gNnPc2	léto
v	v	k7c6	v
čase	čas	k1gInSc6	čas
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
58	[number]	k4	58
minut	minuta	k1gFnPc2	minuta
vedl	vést	k5eAaImAgMnS	vést
z	z	k7c2	z
rentonského	rentonský	k2eAgNnSc2d1	rentonský
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
Boeing	boeing	k1gInSc4	boeing
Field	Fielda	k1gFnPc2	Fielda
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
exemplář	exemplář	k1gInSc1	exemplář
(	(	kIx(	(
<g/>
30016	[number]	k4	30016
<g/>
/	/	kIx~	/
<g/>
683	[number]	k4	683
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
1786	[number]	k4	1786
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
sloužil	sloužit	k5eAaImAgMnS	sloužit
k	k	k7c3	k
testům	test	k1gInPc3	test
prostředků	prostředek	k1gInPc2	prostředek
aktivní	aktivní	k2eAgFnSc2d1	aktivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Certifikační	certifikační	k2eAgFnPc1d1	certifikační
zkoušky	zkouška	k1gFnPc1	zkouška
probíhaly	probíhat	k5eAaImAgFnP	probíhat
jak	jak	k6eAd1	jak
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
základně	základna	k1gFnSc6	základna
Boeing	boeing	k1gInSc1	boeing
Field	Fielda	k1gFnPc2	Fielda
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
ve	v	k7c6	v
vybraných	vybraný	k2eAgFnPc6d1	vybraná
lokalitách	lokalita	k1gFnPc6	lokalita
států	stát	k1gInPc2	stát
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
Montana	Montana	k1gFnSc1	Montana
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
a	a	k8xC	a
Oregon	Oregon	k1gInSc1	Oregon
<g/>
,	,	kIx,	,
testy	test	k1gInPc1	test
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
pak	pak	k6eAd1	pak
na	na	k7c6	na
Islandu	Island	k1gInSc6	Island
a	a	k8xC	a
Newfoundlandu	Newfoundland	k1gInSc6	Newfoundland
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
absolvování	absolvování	k1gNnSc6	absolvování
celého	celý	k2eAgInSc2d1	celý
zkušebního	zkušební	k2eAgInSc2d1	zkušební
programu	program	k1gInSc2	program
bylo	být	k5eAaImAgNnS	být
Boeingu	boeing	k1gInSc2	boeing
737-900	[number]	k4	737-900
vydáno	vydat	k5eAaPmNgNnS	vydat
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
americkým	americký	k2eAgInSc7d1	americký
úřadem	úřad	k1gInSc7	úřad
FAA	FAA	kA	FAA
Osvědčení	osvědčení	k1gNnSc2	osvědčení
letové	letový	k2eAgFnSc2d1	letová
způsobilosti	způsobilost	k1gFnSc2	způsobilost
<g/>
,	,	kIx,	,
obdobný	obdobný	k2eAgInSc4d1	obdobný
dokument	dokument	k1gInSc4	dokument
vydaly	vydat	k5eAaPmAgInP	vydat
Evropské	evropský	k2eAgInPc1d1	evropský
sdružené	sdružený	k2eAgInPc1d1	sdružený
úřady	úřad	k1gInPc1	úřad
JAA	JAA	kA	JAA
tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc2	typ
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2001	[number]	k4	2001
zahájili	zahájit	k5eAaPmAgMnP	zahájit
u	u	k7c2	u
Boeingu	boeing	k1gInSc2	boeing
kompletaci	kompletace	k1gFnSc4	kompletace
prvního	první	k4xOgInSc2	první
letounu	letoun	k1gInSc2	letoun
pro	pro	k7c4	pro
nizozemskou	nizozemský	k2eAgFnSc4d1	nizozemská
KLM	KLM	kA	KLM
(	(	kIx(	(
<g/>
29599	[number]	k4	29599
<g/>
/	/	kIx~	/
<g/>
866	[number]	k4	866
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
čtyřmi	čtyři	k4xCgInPc7	čtyři
objednanými	objednaný	k2eAgInPc7d1	objednaný
stroji	stroj	k1gInPc7	stroj
prvním	první	k4xOgMnSc6	první
evropským	evropský	k2eAgMnSc7d1	evropský
uživatelem	uživatel	k1gMnSc7	uživatel
"	"	kIx"	"
<g/>
Devítistovky	Devítistovka	k1gFnSc2	Devítistovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Předání	předání	k1gNnSc3	předání
prvního	první	k4xOgInSc2	první
letounu	letoun	k1gInSc2	letoun
pro	pro	k7c4	pro
Alaska	Alasko	k1gNnPc4	Alasko
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
,	,	kIx,	,
z	z	k7c2	z
objednávky	objednávka	k1gFnSc2	objednávka
navýšené	navýšený	k2eAgFnPc1d1	navýšená
na	na	k7c4	na
13	[number]	k4	13
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
30013	[number]	k4	30013
<g/>
/	/	kIx~	/
<g/>
774	[number]	k4	774
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
305	[number]	k4	305
<g/>
AS	as	k1gNnSc2	as
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
Boeingy	boeing	k1gInPc1	boeing
tento	tento	k3xDgInSc4	tento
přepravce	přepravce	k1gMnSc1	přepravce
nasadil	nasadit	k5eAaPmAgMnS	nasadit
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
Anchorage-Seattle	Anchorage-Seattle	k1gFnSc2	Anchorage-Seattle
<g/>
,	,	kIx,	,
Phoenix	Phoenix	k1gInSc4	Phoenix
<g/>
,	,	kIx,	,
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
,	,	kIx,	,
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
a	a	k8xC	a
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zařadila	zařadit	k5eAaPmAgFnS	zařadit
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
do	do	k7c2	do
letadlového	letadlový	k2eAgInSc2d1	letadlový
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
americká	americký	k2eAgFnSc1d1	americká
Continental	Continental	k1gMnSc1	Continental
Airlines	Airlines	k1gInSc1	Airlines
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
stroje	stroj	k1gInPc1	stroj
jsou	být	k5eAaImIp3nP	být
označeny	označen	k2eAgInPc1d1	označen
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
924	[number]	k4	924
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
předání	předání	k1gNnSc3	předání
prvního	první	k4xOgInSc2	první
z	z	k7c2	z
objednaných	objednaný	k2eAgMnPc2d1	objednaný
15	[number]	k4	15
letounů	letoun	k1gMnPc2	letoun
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
30118	[number]	k4	30118
<g/>
/	/	kIx~	/
<g/>
820	[number]	k4	820
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
30401	[number]	k4	30401
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
mateřského	mateřský	k2eAgInSc2d1	mateřský
Houstonu	Houston	k1gInSc2	Houston
jej	on	k3xPp3gInSc4	on
přelétla	přelétnout	k5eAaPmAgFnS	přelétnout
osádka	osádka	k1gFnSc1	osádka
kapitána	kapitán	k1gMnSc2	kapitán
C.	C.	kA	C.
D.	D.	kA	D.
McLeana	McLean	k1gMnSc2	McLean
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
byly	být	k5eAaImAgInP	být
letouny	letoun	k1gInPc1	letoun
nasazeny	nasadit	k5eAaPmNgInP	nasadit
na	na	k7c4	na
linky	linka	k1gFnPc4	linka
Houston-Cozumel	Houston-Cozumela	k1gFnPc2	Houston-Cozumela
<g/>
,	,	kIx,	,
Cancún	Cancúna	k1gFnPc2	Cancúna
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
<g/>
,	,	kIx,	,
Denver	Denver	k1gInSc1	Denver
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Orlando	Orlanda	k1gFnSc5	Orlanda
a	a	k8xC	a
Dallas	Dallas	k1gInSc1	Dallas
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2001	[number]	k4	2001
převzali	převzít	k5eAaPmAgMnP	převzít
zástupci	zástupce	k1gMnPc1	zástupce
KLM	KLM	kA	KLM
první	první	k4xOgFnSc6	první
737-9K2	[number]	k4	737-9K2
(	(	kIx(	(
<g/>
29599	[number]	k4	29599
<g/>
/	/	kIx~	/
<g/>
866	[number]	k4	866
<g/>
,	,	kIx,	,
PH-BXO	PH-BXO	k1gFnSc1	PH-BXO
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Plover	Plover	k1gMnSc1	Plover
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	s	k7c7	s
178	[number]	k4	178
sedadly	sedadlo	k1gNnPc7	sedadlo
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
třídách	třída	k1gFnPc6	třída
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
provozovatelem	provozovatel	k1gMnSc7	provozovatel
737-9B5	[number]	k4	737-9B5
(	(	kIx(	(
<g/>
29987	[number]	k4	29987
<g/>
/	/	kIx~	/
<g/>
999	[number]	k4	999
<g/>
,	,	kIx,	,
HL	HL	kA	HL
<g/>
7569	[number]	k4	7569
<g/>
)	)	kIx)	)
se	s	k7c7	s
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
staly	stát	k5eAaPmAgFnP	stát
aerolinie	aerolinie	k1gFnPc1	aerolinie
Korean	Korean	k1gInSc1	Korean
Air	Air	k1gFnSc2	Air
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Boeing	boeing	k1gInSc1	boeing
737-900	[number]	k4	737-900
neměl	mít	k5eNaImAgInS	mít
dostatek	dostatek	k1gInSc1	dostatek
únikových	únikový	k2eAgInPc2d1	únikový
východů	východ	k1gInPc2	východ
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
předpisy	předpis	k1gInPc7	předpis
FAA	FAA	kA	FAA
<g/>
,	,	kIx,	,
kapacita	kapacita	k1gFnSc1	kapacita
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
maximálně	maximálně	k6eAd1	maximálně
189	[number]	k4	189
míst	místo	k1gNnPc2	místo
(	(	kIx(	(
<g/>
rozteč	rozteč	k1gFnSc1	rozteč
sedadel	sedadlo	k1gNnPc2	sedadlo
0,78	[number]	k4	0,78
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
standardům	standard	k1gInPc3	standard
Federal	Federal	k1gFnSc1	Federal
Aviation	Aviation	k1gInSc4	Aviation
Administration	Administration	k1gInSc1	Administration
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
produkce	produkce	k1gFnSc2	produkce
letadla	letadlo	k1gNnSc2	letadlo
Boeing	boeing	k1gInSc4	boeing
757	[number]	k4	757
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
modelem	model	k1gInSc7	model
Boeing	boeing	k1gInSc4	boeing
737-900ER	[number]	k4	737-900ER
s	s	k7c7	s
novými	nový	k2eAgInPc7d1	nový
únikovými	únikový	k2eAgInPc7d1	únikový
východy	východ	k1gInPc7	východ
a	a	k8xC	a
motory	motor	k1gInPc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nabídce	nabídka	k1gFnSc6	nabídka
i	i	k9	i
jako	jako	k9	jako
verze	verze	k1gFnSc1	verze
BBJ	BBJ	kA	BBJ
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
MAX	max	kA	max
<g/>
.	.	kIx.	.
</s>
<s>
Boeing	boeing	k1gInSc4	boeing
oznámil	oznámit	k5eAaPmAgInS	oznámit
vývoj	vývoj	k1gInSc1	vývoj
nového	nový	k2eAgInSc2d1	nový
Boeingu	boeing	k1gInSc2	boeing
737	[number]	k4	737
MAX	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nahradí	nahradit	k5eAaPmIp3nS	nahradit
verze	verze	k1gFnPc4	verze
B	B	kA	B
<g/>
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
700	[number]	k4	700
<g/>
,	,	kIx,	,
800	[number]	k4	800
a	a	k8xC	a
900	[number]	k4	900
<g/>
ER	ER	kA	ER
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
prodáván	prodáván	k2eAgMnSc1d1	prodáván
ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
B737	B737	k1gFnSc2	B737
MAX	max	kA	max
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
deštivého	deštivý	k2eAgMnSc2d1	deštivý
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
let	let	k1gInSc1	let
Boeingu	boeing	k1gInSc2	boeing
737	[number]	k4	737
MAX	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc4	první
dodávky	dodávka	k1gFnPc4	dodávka
leteckým	letecký	k2eAgFnPc3d1	letecká
společnostem	společnost	k1gFnPc3	společnost
jsou	být	k5eAaImIp3nP	být
plánované	plánovaný	k2eAgMnPc4d1	plánovaný
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
uživatelé	uživatel	k1gMnPc1	uživatel
k	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2016	[number]	k4	2016
byly	být	k5eAaImAgFnP	být
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
Southwest	Southwest	k1gMnSc1	Southwest
Airlines	Airlines	k1gMnSc1	Airlines
s	s	k7c7	s
720	[number]	k4	720
kusy	kus	k1gInPc7	kus
<g/>
,	,	kIx,	,
Ryanair	Ryanair	k1gInSc1	Ryanair
s	s	k7c7	s
355	[number]	k4	355
kusy	kus	k1gInPc7	kus
<g/>
,	,	kIx,	,
United	United	k1gMnSc1	United
Airlines	Airlines	k1gMnSc1	Airlines
s	s	k7c7	s
314	[number]	k4	314
kusy	kus	k1gInPc7	kus
<g/>
,	,	kIx,	,
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
s	s	k7c7	s
274	[number]	k4	274
kusy	kus	k1gInPc7	kus
a	a	k8xC	a
z	z	k7c2	z
česka	česko	k1gNnSc2	česko
například	například	k6eAd1	například
Travel	Travel	k1gInSc4	Travel
Service	Service	k1gFnSc2	Service
s	s	k7c7	s
19	[number]	k4	19
kusy	kus	k1gInPc7	kus
a	a	k8xC	a
Smart	Smart	k1gInSc1	Smart
Wings	Wings	k1gInSc1	Wings
s	s	k7c7	s
18	[number]	k4	18
kusy	kus	k1gInPc7	kus
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
vybraných	vybraný	k2eAgFnPc2d1	vybraná
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Aeroflot	aeroflot	k1gInSc1	aeroflot
(	(	kIx(	(
<g/>
17	[number]	k4	17
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Air	Air	k1gFnSc1	Air
Berlin	berlina	k1gFnPc2	berlina
(	(	kIx(	(
<g/>
18	[number]	k4	18
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Air	Air	k1gFnSc1	Air
China	China	k1gFnSc1	China
(	(	kIx(	(
<g/>
141	[number]	k4	141
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
American	American	k1gMnSc1	American
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
274	[number]	k4	274
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
China	China	k1gFnSc1	China
Eastern	Eastern	k1gMnSc1	Eastern
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
120	[number]	k4	120
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
China	China	k1gFnSc1	China
Southern	Southern	k1gMnSc1	Southern
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
164	[number]	k4	164
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Delta	delta	k1gFnSc1	delta
<g />
.	.	kIx.	.
</s>
<s>
Air	Air	k?	Air
Lines	Lines	k1gInSc1	Lines
(	(	kIx(	(
<g/>
144	[number]	k4	144
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Ethiopian	Ethiopian	k1gMnSc1	Ethiopian
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
20	[number]	k4	20
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Flydubai	Flydubai	k1gNnSc1	Flydubai
(	(	kIx(	(
<g/>
51	[number]	k4	51
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Korean	Korean	k1gMnSc1	Korean
Air	Air	k1gMnSc1	Air
(	(	kIx(	(
<g/>
40	[number]	k4	40
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
(	(	kIx(	(
<g/>
7	[number]	k4	7
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Norwegian	Norwegian	k1gInSc1	Norwegian
Air	Air	k1gFnSc2	Air
Shuttle	Shuttle	k1gFnSc2	Shuttle
(	(	kIx(	(
<g/>
58	[number]	k4	58
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1	[number]	k4	1
kus	kus	k1gInSc1	kus
<g/>
)	)	kIx)	)
Ryanair	Ryanair	k1gInSc1	Ryanair
(	(	kIx(	(
<g/>
355	[number]	k4	355
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Scandinavian	Scandinavian	k1gMnSc1	Scandinavian
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
82	[number]	k4	82
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Smart	Smart	k1gInSc1	Smart
Wings	Wings	k1gInSc1	Wings
(	(	kIx(	(
<g/>
18	[number]	k4	18
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Southwest	Southwest	k1gMnSc1	Southwest
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
720	[number]	k4	720
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Travel	Travel	k1gInSc1	Travel
Service	Service	k1gFnSc2	Service
(	(	kIx(	(
<g/>
19	[number]	k4	19
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Transavia	Transavia	k1gFnSc1	Transavia
(	(	kIx(	(
<g/>
44	[number]	k4	44
kusů	kus	k1gInPc2	kus
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
United	United	k1gMnSc1	United
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
314	[number]	k4	314
kusů	kus	k1gInPc2	kus
<g/>
)	)	kIx)	)
Letadla	letadlo	k1gNnSc2	letadlo
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
využívá	využívat	k5eAaPmIp3nS	využívat
nebo	nebo	k8xC	nebo
využívala	využívat	k5eAaImAgFnS	využívat
v	v	k7c6	v
armádní	armádní	k2eAgFnSc6d1	armádní
verzi	verze	k1gFnSc6	verze
vláda	vláda	k1gFnSc1	vláda
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
,	,	kIx,	,
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
Mongolska	Mongolsko	k1gNnSc2	Mongolsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Nigérie	Nigérie	k1gFnSc1	Nigérie
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Saudská	saudský	k2eAgFnSc1d1	Saudská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Tajwan	Tajwan	k1gInSc1	Tajwan
(	(	kIx(	(
<g/>
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
především	především	k9	především
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
–	–	k?	–
ve	v	k7c4	v
verzi	verze	k1gFnSc4	verze
Boeing	boeing	k1gInSc1	boeing
C-40	C-40	k1gMnSc1	C-40
Clipper	clipper	k1gInSc1	clipper
<g/>
.	.	kIx.	.
</s>
<s>
Travel	Travel	k1gInSc1	Travel
Service	Service	k1gFnSc2	Service
(	(	kIx(	(
<g/>
15	[number]	k4	15
ks	ks	kA	ks
–	–	k?	–
prosinec	prosinec	k1gInSc1	prosinec
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
SmartWings	SmartWingsa	k1gFnPc2	SmartWingsa
(	(	kIx(	(
<g/>
15	[number]	k4	15
ks	ks	kA	ks
–	–	k?	–
prosinec	prosinec	k1gInSc1	prosinec
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
(	(	kIx(	(
<g/>
1	[number]	k4	1
ks	ks	kA	ks
-	-	kIx~	-
soukromé	soukromý	k2eAgInPc4d1	soukromý
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
(	(	kIx(	(
<g/>
30	[number]	k4	30
ks	ks	kA	ks
<g/>
)	)	kIx)	)
–	–	k?	–
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
letouny	letoun	k1gInPc4	letoun
B737	B737	k1gMnPc2	B737
používaly	používat	k5eAaImAgFnP	používat
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
(	(	kIx(	(
<g/>
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
vyřadily	vyřadit	k5eAaPmAgFnP	vyřadit
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
linkách	linka	k1gFnPc6	linka
ČSA	ČSA	kA	ČSA
byla	být	k5eAaImAgNnP	být
letadla	letadlo	k1gNnSc2	letadlo
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
první	první	k4xOgInSc1	první
stroj	stroj	k1gInSc1	stroj
(	(	kIx(	(
<g/>
N	N	kA	N
<g/>
1790	[number]	k4	1790
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
imatrikulace	imatrikulace	k1gFnSc1	imatrikulace
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
OK-XGA	OK-XGA	k1gFnSc4	OK-XGA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
pak	pak	k6eAd1	pak
ČSA	ČSA	kA	ČSA
převzaly	převzít	k5eAaPmAgInP	převzít
další	další	k2eAgInPc1d1	další
dva	dva	k4xCgInPc1	dva
letouny	letoun	k1gInPc1	letoun
(	(	kIx(	(
<g/>
OK-XGB	OK-XGB	k1gFnSc1	OK-XGB
a	a	k8xC	a
-XGC	-XGC	k?	-XGC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
následované	následovaný	k2eAgNnSc1d1	následované
zbývajícími	zbývající	k2eAgFnPc7d1	zbývající
dvěma	dva	k4xCgFnPc7	dva
(	(	kIx(	(
<g/>
OK-XGD	OK-XGD	k1gMnSc7	OK-XGD
a	a	k8xC	a
-XGE	-XGE	k?	-XGE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
flotila	flotila	k1gFnSc1	flotila
Boeingů	boeing	k1gInPc2	boeing
737	[number]	k4	737
ČSA	ČSA	kA	ČSA
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
letouny	letoun	k1gInPc4	letoun
prodloužené	prodloužený	k2eAgFnSc2d1	prodloužená
verze	verze	k1gFnSc2	verze
-400	-400	k4	-400
(	(	kIx(	(
<g/>
OK-WGF	OK-WGF	k1gMnSc1	OK-WGF
<g/>
,	,	kIx,	,
výr	výr	k1gMnSc1	výr
<g/>
.	.	kIx.	.
č.	č.	k?	č.
24903	[number]	k4	24903
<g/>
/	/	kIx~	/
<g/>
1978	[number]	k4	1978
ex	ex	k6eAd1	ex
9M-MJN	[number]	k4	9M-MJN
Malaysia	Malaysium	k1gNnSc2	Malaysium
Airlines	Airlines	k1gInSc1	Airlines
a	a	k8xC	a
OK-WGG	OK-WGG	k1gMnSc1	OK-WGG
<g/>
,	,	kIx,	,
výr	výr	k1gMnSc1	výr
<g/>
.	.	kIx.	.
č.	č.	k?	č.
24693	[number]	k4	24693
<g/>
/	/	kIx~	/
<g/>
1972	[number]	k4	1972
ex	ex	k6eAd1	ex
9	[number]	k4	9
<g/>
M-MJM	M-MJM	k1gFnPc2	M-MJM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fischer	Fischer	k1gMnSc1	Fischer
Air	Air	k1gMnSc1	Air
(	(	kIx(	(
<g/>
3	[number]	k4	3
ks	ks	kA	ks
<g/>
)	)	kIx)	)
Holidays	Holidays	k1gInSc1	Holidays
Czech	Czech	k1gMnSc1	Czech
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
2	[number]	k4	2
ks	ks	kA	ks
<g/>
)	)	kIx)	)
Czech	Czech	k1gMnSc1	Czech
Connect	Connect	k1gMnSc1	Connect
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
2	[number]	k4	2
ks	ks	kA	ks
<g/>
)	)	kIx)	)
Letadla	letadlo	k1gNnPc1	letadlo
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
říjnu	říjen	k1gInSc3	říjen
2015	[number]	k4	2015
celkem	celkem	k6eAd1	celkem
368	[number]	k4	368
leteckých	letecký	k2eAgFnPc2d1	letecká
nehod	nehoda	k1gFnPc2	nehoda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
184	[number]	k4	184
vážných	vážná	k1gFnPc2	vážná
(	(	kIx(	(
<g/>
poškození	poškození	k1gNnSc2	poškození
trupu	trup	k1gInSc2	trup
letadla	letadlo	k1gNnSc2	letadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
kterých	který	k3yQgMnPc2	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
4862	[number]	k4	4862
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Oficiální	oficiální	k2eAgInSc4d1	oficiální
stránkyg	stránkyg	k1gInSc4	stránkyg
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Technický	technický	k2eAgMnSc1d1	technický
průvodce	průvodce	k1gMnSc1	průvodce
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zbarvení	zbarvení	k1gNnSc1	zbarvení
letounů	letoun	k1gInPc2	letoun
Boeing	boeing	k1gInSc1	boeing
737	[number]	k4	737
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Fotogalerie	Fotogalerie	k1gFnSc1	Fotogalerie
letounu	letoun	k1gInSc2	letoun
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
