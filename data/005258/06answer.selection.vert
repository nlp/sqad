<s>
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
Francouzský	francouzský	k2eAgInSc1d1	francouzský
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Philippe	Philipp	k1gInSc5	Philipp
VI	VI	kA	VI
de	de	k?	de
France	Franc	k1gMnSc2	Franc
řečený	řečený	k2eAgInSc4d1	řečený
Philippe	Philipp	k1gInSc5	Philipp
de	de	k?	de
Valois	Valois	k1gFnPc2	Valois
<g/>
,	,	kIx,	,
1293	[number]	k4	1293
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
1350	[number]	k4	1350
klášter	klášter	k1gInSc1	klášter
Coulombs	Coulombs	k1gInSc4	Coulombs
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
regent	regent	k1gMnSc1	regent
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Navarry	Navarra	k1gFnSc2	Navarra
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
panovník	panovník	k1gMnSc1	panovník
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
