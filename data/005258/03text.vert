<s>
Filip	Filip	k1gMnSc1	Filip
VI	VI	kA	VI
<g/>
.	.	kIx.	.
Francouzský	francouzský	k2eAgInSc1d1	francouzský
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Philippe	Philipp	k1gInSc5	Philipp
VI	VI	kA	VI
de	de	k?	de
France	Franc	k1gMnSc2	Franc
řečený	řečený	k2eAgInSc4d1	řečený
Philippe	Philipp	k1gInSc5	Philipp
de	de	k?	de
Valois	Valois	k1gFnPc2	Valois
<g/>
,	,	kIx,	,
1293	[number]	k4	1293
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
1350	[number]	k4	1350
klášter	klášter	k1gInSc1	klášter
Coulombs	Coulombs	k1gInSc4	Coulombs
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
regent	regent	k1gMnSc1	regent
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Navarry	Navarra	k1gFnSc2	Navarra
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
panovník	panovník	k1gMnSc1	panovník
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Valois	Valois	k1gFnSc2	Valois
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
zvolení	zvolení	k1gNnSc4	zvolení
vděčil	vděčit	k5eAaImAgMnS	vděčit
francouzské	francouzský	k2eAgFnSc3d1	francouzská
šlechtě	šlechta	k1gFnSc3	šlechta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
jej	on	k3xPp3gMnSc4	on
provázela	provázet	k5eAaImAgFnS	provázet
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Držel	držet	k5eAaImAgInS	držet
skvělý	skvělý	k2eAgInSc1d1	skvělý
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
marně	marně	k6eAd1	marně
plánoval	plánovat	k5eAaImAgMnS	plánovat
křížovou	křížový	k2eAgFnSc4d1	křížová
výpravu	výprava	k1gFnSc4	výprava
<g/>
.	.	kIx.	.
</s>
<s>
Chronické	chronický	k2eAgFnPc1d1	chronická
neshody	neshoda	k1gFnPc1	neshoda
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
vyústily	vyústit	k5eAaPmAgFnP	vyústit
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
panování	panování	k1gNnSc2	panování
v	v	k7c4	v
tzv.	tzv.	kA	tzv.
stoletou	stoletý	k2eAgFnSc4d1	stoletá
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
pro	pro	k7c4	pro
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
Filipem	Filip	k1gMnSc7	Filip
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
krále	král	k1gMnSc2	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
Markéty	Markéta	k1gFnSc2	Markéta
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
neapolského	neapolský	k2eAgMnSc4d1	neapolský
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Anjou	Anjý	k2eAgFnSc7d1	Anjý
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1314	[number]	k4	1314
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
zděděné	zděděný	k2eAgNnSc4d1	zděděné
hrabství	hrabství	k1gNnSc4	hrabství
Maine	Main	k1gInSc5	Main
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vlády	vláda	k1gFnSc2	vláda
bratrance	bratranec	k1gMnSc2	bratranec
Karla	Karel	k1gMnSc2	Karel
Sličného	sličný	k2eAgMnSc2d1	sličný
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
králova	králův	k2eAgInSc2d1	králův
poradního	poradní	k2eAgInSc2d1	poradní
sboru	sbor	k1gInSc2	sbor
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
roku	rok	k1gInSc2	rok
1325	[number]	k4	1325
zdědil	zdědit	k5eAaPmAgInS	zdědit
titul	titul	k1gInSc1	titul
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
a	a	k8xC	a
Anjou	Anja	k1gFnSc7	Anja
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1328	[number]	k4	1328
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
náhle	náhle	k6eAd1	náhle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
královna	královna	k1gFnSc1	královna
Jana	Jana	k1gFnSc1	Jana
z	z	k7c2	z
Évreux	Évreux	k1gInSc4	Évreux
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gInSc2	jeho
skonu	skon	k1gInSc2	skon
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vyčkat	vyčkat	k5eAaPmF	vyčkat
na	na	k7c4	na
porod	porod	k1gInSc4	porod
a	a	k8xC	a
pohlaví	pohlaví	k1gNnSc1	pohlaví
očekávaného	očekávaný	k2eAgMnSc2d1	očekávaný
královského	královský	k2eAgMnSc2d1	královský
potomka	potomek	k1gMnSc2	potomek
a	a	k8xC	a
zvolit	zvolit	k5eAaPmF	zvolit
regenta	regens	k1gMnSc4	regens
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
spravoval	spravovat	k5eAaImAgMnS	spravovat
království	království	k1gNnSc4	království
v	v	k7c6	v
době	doba	k1gFnSc6	doba
do	do	k7c2	do
porodu	porod	k1gInSc2	porod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
narození	narození	k1gNnSc2	narození
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
regent	regent	k1gMnSc1	regent
zůstal	zůstat	k5eAaPmAgMnS	zůstat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
zletilosti	zletilost	k1gFnSc2	zletilost
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
děvče	děvče	k1gNnSc1	děvče
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
právníků	právník	k1gMnPc2	právník
a	a	k8xC	a
šlechty	šlechta	k1gFnSc2	šlechta
svolané	svolaný	k2eAgFnSc2d1	svolaná
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
poselství	poselství	k1gNnSc4	poselství
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
připomínal	připomínat	k5eAaImAgMnS	připomínat
svůj	svůj	k3xOyFgInSc4	svůj
po	po	k7c6	po
matce	matka	k1gFnSc6	matka
zděděný	zděděný	k2eAgInSc1d1	zděděný
nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
francouzský	francouzský	k2eAgInSc4d1	francouzský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
předejít	předejít	k5eAaPmF	předejít
možnosti	možnost	k1gFnSc3	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
království	království	k1gNnSc1	království
připadlo	připadnout	k5eAaPmAgNnS	připadnout
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
nejenže	nejenže	k6eAd1	nejenže
nemohou	moct	k5eNaImIp3nP	moct
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
korunu	koruna	k1gFnSc4	koruna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tento	tento	k3xDgInSc4	tento
nárok	nárok	k1gInSc4	nárok
předávat	předávat	k5eAaImF	předávat
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
vydalo	vydat	k5eAaPmAgNnS	vydat
prohlášení	prohlášení	k1gNnSc1	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
francouzské	francouzský	k2eAgNnSc1d1	francouzské
království	království	k1gNnSc1	království
nikdy	nikdy	k6eAd1	nikdy
nepřipadne	připadnout	k5eNaPmIp3nS	připadnout
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
porodila	porodit	k5eAaPmAgFnS	porodit
královna	královna	k1gFnSc1	královna
děvče	děvče	k1gNnSc1	děvče
a	a	k8xC	a
Filip	Filip	k1gMnSc1	Filip
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
radou	rada	k1gMnSc7	rada
ustanovený	ustanovený	k2eAgMnSc1d1	ustanovený
regent	regent	k1gMnSc1	regent
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgInS	převzít
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1328	[number]	k4	1328
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
bohaté	bohatý	k2eAgFnSc2d1	bohatá
účasti	účast	k1gFnSc2	účast
francouzských	francouzský	k2eAgMnPc2d1	francouzský
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
také	také	k6eAd1	také
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
společně	společně	k6eAd1	společně
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
v	v	k7c6	v
Remeši	Remeš	k1gFnSc6	Remeš
slavnostně	slavnostně	k6eAd1	slavnostně
korunován	korunován	k2eAgMnSc1d1	korunován
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
korunovaci	korunovace	k1gFnSc6	korunovace
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
žádosti	žádost	k1gFnSc2	žádost
flanderského	flanderský	k2eAgMnSc2d1	flanderský
hraběte	hrabě	k1gMnSc2	hrabě
Ludvíka	Ludvík	k1gMnSc2	Ludvík
a	a	k8xC	a
pomohl	pomoct	k5eAaPmAgMnS	pomoct
mu	on	k3xPp3gMnSc3	on
proti	proti	k7c3	proti
revoltujícím	revoltující	k2eAgFnPc3d1	revoltující
poddaným	poddaná	k1gFnPc3	poddaná
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Casselu	Cassel	k1gInSc2	Cassel
<g/>
.	.	kIx.	.
</s>
<s>
Vlámové	Vlám	k1gMnPc1	Vlám
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
zdrcující	zdrcující	k2eAgFnSc4d1	zdrcující
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
se	se	k3xPyFc4	se
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
dočkali	dočkat	k5eAaPmAgMnP	dočkat
pomsty	pomsta	k1gFnPc4	pomsta
za	za	k7c4	za
prohranou	prohraný	k2eAgFnSc4d1	prohraná
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Courtrai	Courtra	k1gFnSc2	Courtra
<g/>
,	,	kIx,	,
možnosti	možnost	k1gFnSc2	možnost
doprovodit	doprovodit	k5eAaPmF	doprovodit
své	své	k1gNnSc4	své
vítězství	vítězství	k1gNnSc2	vítězství
hrůzným	hrůzný	k2eAgInSc7d1	hrůzný
masakrem	masakr	k1gInSc7	masakr
a	a	k8xC	a
také	také	k9	také
konfiskacemi	konfiskace	k1gFnPc7	konfiskace
majetku	majetek	k1gInSc2	majetek
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
<g/>
.	.	kIx.	.
</s>
<s>
Vítězstvím	vítězství	k1gNnSc7	vítězství
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
prestiže	prestiž	k1gFnSc2	prestiž
Filip	Filip	k1gMnSc1	Filip
využil	využít	k5eAaPmAgMnS	využít
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
poselstvo	poselstvo	k1gNnSc4	poselstvo
s	s	k7c7	s
předvoláním	předvolání	k1gNnSc7	předvolání
pro	pro	k7c4	pro
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
jej	on	k3xPp3gMnSc4	on
vyzýval	vyzývat	k5eAaImAgInS	vyzývat
k	k	k7c3	k
složení	složení	k1gNnSc3	složení
holdu	hold	k1gInSc2	hold
za	za	k7c4	za
akvitánské	akvitánský	k2eAgNnSc4d1	akvitánské
vévodství	vévodství	k1gNnSc4	vévodství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
neochotného	ochotný	k2eNgMnSc4d1	neochotný
mladého	mladý	k1gMnSc4	mladý
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
zabrala	zabrat	k5eAaPmAgFnS	zabrat
až	až	k6eAd1	až
výhrůžka	výhrůžka	k1gFnSc1	výhrůžka
konfiskace	konfiskace	k1gFnSc1	konfiskace
francouzského	francouzský	k2eAgNnSc2d1	francouzské
léna	léno	k1gNnSc2	léno
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1329	[number]	k4	1329
hold	hold	k1gInSc4	hold
Filipovi	Filip	k1gMnSc6	Filip
v	v	k7c6	v
Amiensu	Amiens	k1gInSc6	Amiens
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
mnoha	mnoho	k4c2	mnoho
dvorských	dvorský	k2eAgFnPc2d1	dvorská
slavností	slavnost	k1gFnPc2	slavnost
složil	složit	k5eAaPmAgMnS	složit
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
dvou	dva	k4xCgFnPc2	dva
let	let	k1gInSc4	let
strávené	strávený	k2eAgInPc1d1	strávený
dohady	dohad	k1gInPc1	dohad
o	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
složeného	složený	k2eAgInSc2d1	složený
holdu	hold	k1gInSc2	hold
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jím	jíst	k5eAaImIp1nS	jíst
byl	být	k5eAaImAgInS	být
skutečně	skutečně	k6eAd1	skutečně
uznán	uznat	k5eAaPmNgInS	uznat
lenní	lenní	k2eAgInSc1d1	lenní
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
spor	spor	k1gInSc1	spor
dočasně	dočasně	k6eAd1	dočasně
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
smírem	smír	k1gInSc7	smír
<g/>
,	,	kIx,	,
když	když	k8xS	když
Eduard	Eduard	k1gMnSc1	Eduard
roku	rok	k1gInSc2	rok
1331	[number]	k4	1331
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
lenní	lenní	k2eAgFnSc4d1	lenní
přísahu	přísaha	k1gFnSc4	přísaha
skutečně	skutečně	k6eAd1	skutečně
šlo	jít	k5eAaImAgNnS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Pozici	pozice	k1gFnSc4	pozice
rodu	rod	k1gInSc2	rod
Filip	Filip	k1gMnSc1	Filip
začal	začít	k5eAaPmAgMnS	začít
upevňovat	upevňovat	k5eAaImF	upevňovat
sňatky	sňatek	k1gInPc4	sňatek
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
potomky	potomek	k1gMnPc7	potomek
vladařů	vladař	k1gMnPc2	vladař
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
plánováním	plánování	k1gNnSc7	plánování
křížové	křížový	k2eAgFnSc2d1	křížová
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
posléze	posléze	k6eAd1	posléze
pro	pro	k7c4	pro
vzrůstající	vzrůstající	k2eAgNnSc4d1	vzrůstající
napětí	napětí	k1gNnSc4	napětí
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
koruna	koruna	k1gFnSc1	koruna
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
zastavení	zastavení	k1gNnSc1	zastavení
výběru	výběr	k1gInSc2	výběr
křížového	křížový	k2eAgInSc2d1	křížový
desátku	desátek	k1gInSc2	desátek
a	a	k8xC	a
také	také	k9	také
smrt	smrt	k1gFnSc4	smrt
princezny	princezna	k1gFnSc2	princezna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
konec	konec	k1gInSc4	konec
výběru	výběr	k1gInSc2	výběr
svatební	svatební	k2eAgFnSc2d1	svatební
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Daň	daň	k1gFnSc1	daň
z	z	k7c2	z
pasování	pasování	k1gNnPc2	pasování
prince	princ	k1gMnSc2	princ
Jana	Jan	k1gMnSc2	Jan
se	se	k3xPyFc4	se
vybírala	vybírat	k5eAaImAgFnS	vybírat
těžce	těžce	k6eAd1	těžce
a	a	k8xC	a
když	když	k8xS	když
roku	rok	k1gInSc2	rok
1335	[number]	k4	1335
následník	následník	k1gMnSc1	následník
těžce	těžce	k6eAd1	těžce
ochořel	ochořet	k5eAaPmAgMnS	ochořet
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
král	král	k1gMnSc1	král
výběr	výběr	k1gInSc4	výběr
daně	daň	k1gFnSc2	daň
zrušit	zrušit	k5eAaPmF	zrušit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
za	za	k7c4	za
synovu	synův	k2eAgFnSc4d1	synova
úzdravu	úzdrava	k1gFnSc4	úzdrava
vrátil	vrátit	k5eAaPmAgInS	vrátit
již	již	k6eAd1	již
vybrané	vybraný	k2eAgInPc4d1	vybraný
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zhoršovaly	zhoršovat	k5eAaImAgFnP	zhoršovat
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
strana	strana	k1gFnSc1	strana
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
s	s	k7c7	s
plány	plán	k1gInPc7	plán
Eduarda	Eduard	k1gMnSc2	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c4	na
dobytí	dobytí	k1gNnSc4	dobytí
Skotska	Skotsko	k1gNnSc2	Skotsko
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
skotské	skotský	k2eAgFnSc2d1	skotská
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1336	[number]	k4	1336
se	se	k3xPyFc4	se
francouzská	francouzský	k2eAgFnSc1d1	francouzská
flotila	flotila	k1gFnSc1	flotila
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
určená	určený	k2eAgNnPc1d1	určené
ke	k	k7c3	k
křížové	křížový	k2eAgFnSc3d1	křížová
výpravě	výprava	k1gFnSc3	výprava
<g/>
,	,	kIx,	,
přepravila	přepravit	k5eAaPmAgFnS	přepravit
do	do	k7c2	do
severofrancouzských	severofrancouzský	k2eAgInPc2d1	severofrancouzský
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1337	[number]	k4	1337
Filip	Filip	k1gMnSc1	Filip
nechal	nechat	k5eAaPmAgMnS	nechat
pro	pro	k7c4	pro
neplnění	neplnění	k1gNnSc4	neplnění
lenních	lenní	k2eAgFnPc2d1	lenní
povinností	povinnost	k1gFnPc2	povinnost
zkonfiskovat	zkonfiskovat	k5eAaPmF	zkonfiskovat
Eduardovo	Eduardův	k2eAgNnSc4d1	Eduardovo
akvitánské	akvitánský	k2eAgNnSc4d1	akvitánské
vévodství	vévodství	k1gNnSc4	vévodství
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
své	svůj	k3xOyFgInPc4	svůj
závazky	závazek	k1gInPc4	závazek
vůči	vůči	k7c3	vůči
Filipovi	Filip	k1gMnSc3	Filip
z	z	k7c2	z
Valois	Valois	k1gFnSc2	Valois
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
francouzským	francouzský	k2eAgMnSc7d1	francouzský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
začali	začít	k5eAaPmAgMnP	začít
shánět	shánět	k5eAaImF	shánět
spojence	spojenec	k1gMnPc4	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Eduardovi	Eduard	k1gMnSc3	Eduard
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
navázat	navázat	k5eAaPmF	navázat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
říšskými	říšský	k2eAgMnPc7d1	říšský
knížaty	kníže	k1gMnPc7wR	kníže
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Bavorem	Bavor	k1gMnSc7	Bavor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejaktivnějšími	aktivní	k2eAgMnPc7d3	nejaktivnější
spojenci	spojenec	k1gMnPc7	spojenec
byli	být	k5eAaImAgMnP	být
vlámští	vlámský	k2eAgMnPc1d1	vlámský
vzbouřenci	vzbouřenec	k1gMnPc1	vzbouřenec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Jakubem	Jakub	k1gMnSc7	Jakub
van	vana	k1gFnPc2	vana
Antervelde	Anterveld	k1gMnSc5	Anterveld
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1341	[number]	k4	1341
se	se	k3xPyFc4	se
otevřela	otevřít	k5eAaPmAgFnS	otevřít
druhá	druhý	k4xOgFnSc1	druhý
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
Bretani	Bretaň	k1gFnSc6	Bretaň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
utkávali	utkávat	k5eAaImAgMnP	utkávat
dva	dva	k4xCgMnPc1	dva
uchazeči	uchazeč	k1gMnPc1	uchazeč
o	o	k7c6	o
úmrtím	úmrtí	k1gNnSc7	úmrtí
posledního	poslední	k2eAgMnSc2d1	poslední
vévody	vévoda	k1gMnSc2	vévoda
osiřelou	osiřelý	k2eAgFnSc4d1	osiřelá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Filipa	Filip	k1gMnSc2	Filip
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vlády	vláda	k1gFnSc2	vláda
stigmatizovaly	stigmatizovat	k5eAaBmAgFnP	stigmatizovat
okolnosti	okolnost	k1gFnPc1	okolnost
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
nenavazoval	navazovat	k5eNaImAgMnS	navazovat
na	na	k7c4	na
tradici	tradice	k1gFnSc4	tradice
svých	svůj	k3xOyFgMnPc2	svůj
předchůdců	předchůdce	k1gMnPc2	předchůdce
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zavázán	zavázat	k5eAaPmNgMnS	zavázat
svým	svůj	k3xOyFgMnPc3	svůj
volitelům	volitel	k1gMnPc3	volitel
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
odměnil	odměnit	k5eAaPmAgInS	odměnit
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
<g/>
,	,	kIx,	,
příliš	příliš	k6eAd1	příliš
nárokující	nárokující	k2eAgInPc1d1	nárokující
<g/>
,	,	kIx,	,
ode	ode	k7c2	ode
dvora	dvůr	k1gInSc2	dvůr
postupně	postupně	k6eAd1	postupně
vzdálil	vzdálit	k5eAaPmAgInS	vzdálit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
kategorie	kategorie	k1gFnSc2	kategorie
patřil	patřit	k5eAaImAgMnS	patřit
Robert	Robert	k1gMnSc1	Robert
z	z	k7c2	z
Artois	Artois	k1gFnSc2	Artois
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
pařížským	pařížský	k2eAgInSc7d1	pařížský
soudem	soud	k1gInSc7	soud
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
za	za	k7c4	za
padělání	padělání	k1gNnSc4	padělání
listin	listina	k1gFnPc2	listina
vztahujících	vztahující	k2eAgFnPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
následnictví	následnictví	k1gNnSc3	následnictví
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Artois	Artois	k1gFnSc2	Artois
k	k	k7c3	k
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Provinilec	provinilec	k1gMnSc1	provinilec
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
na	na	k7c4	na
anglický	anglický	k2eAgInSc4d1	anglický
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
ihned	ihned	k6eAd1	ihned
intrikovat	intrikovat	k5eAaImF	intrikovat
proti	proti	k7c3	proti
svému	svůj	k3xOyFgMnSc3	svůj
bývalému	bývalý	k2eAgMnSc3d1	bývalý
vládci	vládce	k1gMnSc3	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Královský	královský	k2eAgInSc1d1	královský
dvůr	dvůr	k1gInSc1	dvůr
postupně	postupně	k6eAd1	postupně
početně	početně	k6eAd1	početně
narůstal	narůstat	k5eAaImAgInS	narůstat
<g/>
,	,	kIx,	,
podstatné	podstatný	k2eAgNnSc4d1	podstatné
místo	místo	k1gNnSc4	místo
zaujímaly	zaujímat	k5eAaImAgInP	zaujímat
klany	klan	k1gInPc1	klan
různých	různý	k2eAgMnPc2d1	různý
cílů	cíl	k1gInPc2	cíl
a	a	k8xC	a
významů	význam	k1gInPc2	význam
<g/>
.	.	kIx.	.
<g/>
Filip	Filip	k1gMnSc1	Filip
byl	být	k5eAaImAgMnS	být
ctižádostivý	ctižádostivý	k2eAgMnSc1d1	ctižádostivý
milovník	milovník	k1gMnSc1	milovník
nádhery	nádhera	k1gFnSc2	nádhera
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nerad	nerad	k2eAgMnSc1d1	nerad
rozhazoval	rozhazovat	k5eAaImAgInS	rozhazovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
dvůr	dvůr	k1gInSc1	dvůr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
středobodem	středobod	k1gInSc7	středobod
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
rytířstva	rytířstvo	k1gNnSc2	rytířstvo
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
býval	bývat	k5eAaImAgMnS	bývat
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
měl	mít	k5eAaImAgMnS	mít
Filip	Filip	k1gMnSc1	Filip
stejné	stejná	k1gFnSc2	stejná
politické	politický	k2eAgInPc4d1	politický
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1349	[number]	k4	1349
se	se	k3xPyFc4	se
králi	král	k1gMnSc3	král
podařilo	podařit	k5eAaPmAgNnS	podařit
po	po	k7c6	po
předchozím	předchozí	k2eAgNnSc6d1	předchozí
jednání	jednání	k1gNnSc6	jednání
odkoupit	odkoupit	k5eAaPmF	odkoupit
od	od	k7c2	od
bezdětného	bezdětný	k2eAgMnSc2d1	bezdětný
hraběte	hrabě	k1gMnSc2	hrabě
Humberta	Humbert	k1gMnSc2	Humbert
z	z	k7c2	z
Viennois	Viennois	k1gFnSc2	Viennois
tzv.	tzv.	kA	tzv.
delfinát	delfinát	k1gInSc1	delfinát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
majitelem	majitel	k1gMnSc7	majitel
se	se	k3xPyFc4	se
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
stal	stát	k5eAaPmAgMnS	stát
vždy	vždy	k6eAd1	vždy
následník	následník	k1gMnSc1	následník
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
z	z	k7c2	z
držení	držení	k1gNnSc2	držení
země	zem	k1gFnSc2	zem
vyplýval	vyplývat	k5eAaImAgMnS	vyplývat
jeho	on	k3xPp3gInSc4	on
titul	titul	k1gInSc4	titul
dauphina	dauphin	k1gMnSc2	dauphin
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1313	[number]	k4	1313
se	se	k3xPyFc4	se
ve	v	k7c6	v
Fontainebleau	Fontainebleaum	k1gNnSc6	Fontainebleaum
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
burgundského	burgundský	k2eAgMnSc2d1	burgundský
vévody	vévoda	k1gMnSc2	vévoda
Roberta	Robert	k1gMnSc2	Robert
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
vrstevnicí	vrstevnice	k1gFnSc7	vrstevnice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gInSc6	její
skonu	skon	k1gInSc6	skon
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
mladičkou	mladičký	k2eAgFnSc7d1	mladičká
Blankou	Blanka	k1gFnSc7	Blanka
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
navarrského	navarrský	k2eAgMnSc4d1	navarrský
krále	král	k1gMnSc4	král
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Pozici	pozice	k1gFnSc4	pozice
rodu	rod	k1gInSc2	rod
Filip	Filip	k1gMnSc1	Filip
začal	začít	k5eAaPmAgMnS	začít
upevňovat	upevňovat	k5eAaImF	upevňovat
sňatky	sňatek	k1gInPc4	sňatek
svých	svůj	k3xOyFgFnPc2	svůj
dětí	dítě	k1gFnPc2	dítě
s	s	k7c7	s
potomky	potomek	k1gMnPc7	potomek
vladařů	vladař	k1gMnPc2	vladař
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Marii	Maria	k1gFnSc4	Maria
provdal	provdat	k5eAaPmAgMnS	provdat
za	za	k7c4	za
syna	syn	k1gMnSc4	syn
brabantského	brabantský	k2eAgMnSc2d1	brabantský
vévody	vévoda	k1gMnSc2	vévoda
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
staršího	starší	k1gMnSc2	starší
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jana	Jan	k1gMnSc2	Jan
<g/>
.	.	kIx.	.
</s>
