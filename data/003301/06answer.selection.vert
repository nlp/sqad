<s>
Skupina	skupina	k1gFnSc1	skupina
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
železná	železný	k2eAgFnSc1d1	železná
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
mučicí	mučicí	k2eAgInSc1d1	mučicí
nástroj	nástroj	k1gInSc1	nástroj
<g/>
)	)	kIx)	)
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Harris	Harris	k1gFnSc1	Harris
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Matthews	Matthews	k1gInSc1	Matthews
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dave	Dav	k1gInSc5	Dav
Sullivan	Sullivan	k1gMnSc1	Sullivan
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Day	Day	k1gMnSc1	Day
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terry	Terr	k1gInPc1	Terr
Rance	ranec	k1gInSc2	ranec
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
