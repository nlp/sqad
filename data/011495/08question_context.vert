<s>
Richard	Richard	k1gMnSc1	Richard
Milhous	Milhous	k1gMnSc1	Milhous
Nixon	Nixon	k1gMnSc1	Nixon
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1913	[number]	k4	1913
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
37	[number]	k4	37
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
36	[number]	k4	36
<g/>
.	.	kIx.	.
viceprezident	viceprezident	k1gMnSc1	viceprezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
-	-	kIx~	-
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
prezident	prezident	k1gMnSc1	prezident
Dwight	Dwight	k1gMnSc1	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
<g/>
.	.	kIx.	.
</s>
<s>
Nixon	Nixon	k1gMnSc1	Nixon
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
zvolen	zvolit	k5eAaPmNgMnS	zvolit
americkým	americký	k2eAgMnSc7d1	americký
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
i	i	k8xC	i
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
<g/>
.	.	kIx.	.
</s>

