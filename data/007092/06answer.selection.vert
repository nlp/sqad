<s>
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
pozdější	pozdní	k2eAgFnSc1d2	pozdější
hovorová	hovorový	k2eAgFnSc1d1	hovorová
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
Manhattan	Manhattan	k1gInSc4	Manhattan
Engineering	Engineering	k1gInSc1	Engineering
District	District	k2eAgInSc1d1	District
(	(	kIx(	(
<g/>
MED	med	k1gInSc1	med
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
krycí	krycí	k2eAgInSc1d1	krycí
název	název	k1gInSc1	název
pro	pro	k7c4	pro
utajený	utajený	k2eAgInSc4d1	utajený
americký	americký	k2eAgInSc4d1	americký
vývoj	vývoj	k1gInSc4	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
