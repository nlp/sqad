<s>
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
je	být	k5eAaImIp3nS	být
pozdější	pozdní	k2eAgFnSc1d2	pozdější
hovorová	hovorový	k2eAgFnSc1d1	hovorová
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
Manhattan	Manhattan	k1gInSc4	Manhattan
Engineering	Engineering	k1gInSc1	Engineering
District	District	k2eAgInSc1d1	District
(	(	kIx(	(
<g/>
MED	med	k1gInSc1	med
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
krycí	krycí	k2eAgInSc1d1	krycí
název	název	k1gInSc1	název
pro	pro	k7c4	pro
utajený	utajený	k2eAgInSc4d1	utajený
americký	americký	k2eAgInSc4d1	americký
vývoj	vývoj	k1gInSc4	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
označuje	označovat	k5eAaImIp3nS	označovat
zejména	zejména	k9	zejména
začátek	začátek	k1gInSc4	začátek
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
období	období	k1gNnSc1	období
1942	[number]	k4	1942
<g/>
–	–	k?	–
<g/>
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
sumy	suma	k1gFnPc1	suma
přes	přes	k7c4	přes
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
sumě	suma	k1gFnSc3	suma
26	[number]	k4	26
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumné	výzkumný	k2eAgInPc1d1	výzkumný
a	a	k8xC	a
výrobní	výrobní	k2eAgInPc1d1	výrobní
provozy	provoz	k1gInPc1	provoz
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
na	na	k7c6	na
30	[number]	k4	30
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
zaměstnával	zaměstnávat	k5eAaImAgInS	zaměstnávat
celkem	celkem	k6eAd1	celkem
225	[number]	k4	225
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
600	[number]	k4	600
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
pracovalo	pracovat	k5eAaImAgNnS	pracovat
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
personálu	personál	k1gInSc2	personál
ani	ani	k8xC	ani
nevěděla	vědět	k5eNaImAgFnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
atomové	atomový	k2eAgFnSc6d1	atomová
bombě	bomba	k1gFnSc6	bomba
–	–	k?	–
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
válečných	válečný	k2eAgInPc2d1	válečný
úkolů	úkol	k1gInPc2	úkol
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poznatky	poznatek	k1gInPc1	poznatek
získané	získaný	k2eAgInPc1d1	získaný
v	v	k7c6	v
Projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
byly	být	k5eAaImAgFnP	být
využity	využít	k5eAaPmNgInP	využít
při	při	k7c6	při
konstrukci	konstrukce	k1gFnSc6	konstrukce
pum	puma	k1gFnPc2	puma
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
a	a	k8xC	a
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
byly	být	k5eAaImAgFnP	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
shozeny	shozen	k2eAgMnPc4d1	shozen
na	na	k7c4	na
japonská	japonský	k2eAgNnPc4d1	Japonské
města	město	k1gNnPc4	město
Hirošima	Hirošima	k1gFnSc1	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc1	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
štěpením	štěpení	k1gNnSc7	štěpení
atomu	atom	k1gInSc2	atom
zabývali	zabývat	k5eAaImAgMnP	zabývat
převážně	převážně	k6eAd1	převážně
evropští	evropský	k2eAgMnPc1d1	evropský
vědci	vědec	k1gMnPc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
objevil	objevit	k5eAaPmAgInS	objevit
Henri	Henr	k1gFnSc2	Henr
Becquerel	becquerel	k1gInSc1	becquerel
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
sloučeniny	sloučenina	k1gFnPc1	sloučenina
uranu	uran	k1gInSc2	uran
jsou	být	k5eAaImIp3nP	být
radioaktivní	radioaktivní	k2eAgInPc1d1	radioaktivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
objevili	objevit	k5eAaPmAgMnP	objevit
Pierre	Pierr	k1gInSc5	Pierr
a	a	k8xC	a
Marie	Maria	k1gFnSc2	Maria
Curie	Curie	k1gMnPc2	Curie
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
prvek	prvek	k1gInSc4	prvek
radium	radium	k1gNnSc4	radium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
další	další	k2eAgInPc4d1	další
výzkumy	výzkum	k1gInPc4	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
teorii	teorie	k1gFnSc4	teorie
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
objevil	objevit	k5eAaPmAgMnS	objevit
atomové	atomový	k2eAgNnSc4d1	atomové
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Rutherfordův	Rutherfordův	k2eAgInSc1d1	Rutherfordův
model	model	k1gInSc1	model
atomu	atom	k1gInSc2	atom
využil	využít	k5eAaPmAgInS	využít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
Niels	Nielsa	k1gFnPc2	Nielsa
Bohr	Bohra	k1gFnPc2	Bohra
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
teorie	teorie	k1gFnSc2	teorie
stavby	stavba	k1gFnSc2	stavba
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Neutron	neutron	k1gInSc1	neutron
následně	následně	k6eAd1	následně
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
James	James	k1gMnSc1	James
Chadwick	Chadwick	k1gMnSc1	Chadwick
a	a	k8xC	a
Enrico	Enrico	k1gMnSc1	Enrico
Fermi	Fer	k1gFnPc7	Fer
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
poprvé	poprvé	k6eAd1	poprvé
neutrony	neutron	k1gInPc4	neutron
použil	použít	k5eAaPmAgMnS	použít
k	k	k7c3	k
"	"	kIx"	"
<g/>
bombardování	bombardování	k1gNnSc3	bombardování
<g/>
"	"	kIx"	"
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
rozeznal	rozeznat	k5eAaPmAgMnS	rozeznat
a	a	k8xC	a
popsal	popsat	k5eAaPmAgMnS	popsat
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
i	i	k8xC	i
princip	princip	k1gInSc1	princip
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Leó	Leó	k1gMnSc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
přeměnu	přeměna	k1gFnSc4	přeměna
prvků	prvek	k1gInPc2	prvek
pomocí	pomocí	k7c2	pomocí
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
dal	dát	k5eAaPmAgInS	dát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
i	i	k9	i
patentovat	patentovat	k5eAaBmF	patentovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
Irè	Irè	k1gFnSc1	Irè
Joliot-Curie	Joliot-Curie	k1gFnSc1	Joliot-Curie
objevila	objevit	k5eAaPmAgFnS	objevit
dělení	dělení	k1gNnSc4	dělení
uranu	uran	k1gInSc2	uran
působením	působení	k1gNnSc7	působení
pomalých	pomalý	k2eAgInPc2d1	pomalý
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Atomová	atomový	k2eAgFnSc1d1	atomová
fyzika	fyzika	k1gFnSc1	fyzika
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rychle	rychle	k6eAd1	rychle
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zjištění	zjištění	k1gNnSc2	zjištění
Irè	Irè	k1gFnSc2	Irè
Curieové	Curieová	k1gFnSc2	Curieová
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
i	i	k8xC	i
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
berlínský	berlínský	k2eAgInSc1d1	berlínský
experiment	experiment	k1gInSc1	experiment
profesora	profesor	k1gMnSc2	profesor
Otta	Otta	k1gMnSc1	Otta
Hahna	Hahna	k1gFnSc1	Hahna
<g/>
,	,	kIx,	,
Fritze	Fritze	k1gFnSc1	Fritze
Strassmanna	Strassmann	k1gMnSc2	Strassmann
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Skupině	skupina	k1gFnSc3	skupina
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
experimentálně	experimentálně	k6eAd1	experimentálně
rozštěpit	rozštěpit	k5eAaPmF	rozštěpit
jádra	jádro	k1gNnPc4	jádro
uranu	uran	k1gInSc2	uran
jejich	jejich	k3xOp3gNnSc7	jejich
bombardováním	bombardování	k1gNnSc7	bombardování
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
uvolnilo	uvolnit	k5eAaPmAgNnS	uvolnit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
těchto	tento	k3xDgInPc2	tento
pokusů	pokus	k1gInPc2	pokus
nakonec	nakonec	k6eAd1	nakonec
správně	správně	k6eAd1	správně
objasnila	objasnit	k5eAaPmAgFnS	objasnit
Lise	Lisa	k1gFnSc3	Lisa
Meitnerová	Meitnerová	k1gFnSc1	Meitnerová
<g/>
,	,	kIx,	,
asistentka	asistentka	k1gFnSc1	asistentka
Otta	Otta	k1gMnSc1	Otta
Hahna	Hahn	k1gInSc2	Hahn
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
synovec	synovec	k1gMnSc1	synovec
Otto	Otto	k1gMnSc1	Otto
Frisch	Frisch	k1gMnSc1	Frisch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
u	u	k7c2	u
Nielse	Niels	k1gMnSc2	Niels
Bohra	Bohr	k1gMnSc2	Bohr
<g/>
.	.	kIx.	.
</s>
<s>
Meitnerová	Meitnerová	k1gFnSc1	Meitnerová
a	a	k8xC	a
Frisch	Frisch	k1gMnSc1	Frisch
společně	společně	k6eAd1	společně
zavedli	zavést	k5eAaPmAgMnP	zavést
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
dělení	dělení	k1gNnSc1	dělení
jádra	jádro	k1gNnSc2	jádro
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
analogii	analogie	k1gFnSc4	analogie
k	k	k7c3	k
dělení	dělení	k1gNnSc3	dělení
buněk	buňka	k1gFnPc2	buňka
v	v	k7c6	v
biologii	biologie	k1gFnSc6	biologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
udělali	udělat	k5eAaPmAgMnP	udělat
si	se	k3xPyFc3	se
obraz	obraz	k1gInSc4	obraz
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
jádra	jádro	k1gNnSc2	jádro
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádro	jádro	k1gNnSc1	jádro
uranu	uran	k1gInSc2	uran
představuje	představovat	k5eAaImIp3nS	představovat
nestabilní	stabilní	k2eNgInSc1d1	nestabilní
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgInS	připravit
na	na	k7c4	na
dělení	dělení	k1gNnSc4	dělení
už	už	k6eAd1	už
při	při	k7c6	při
malých	malý	k2eAgInPc6d1	malý
podnětech	podnět	k1gInPc6	podnět
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
dělení	dělení	k1gNnSc6	dělení
jsou	být	k5eAaImIp3nP	být
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
části	část	k1gFnPc4	část
lehčí	lehčit	k5eAaImIp3nS	lehčit
asi	asi	k9	asi
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
pětinu	pětina	k1gFnSc4	pětina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
protonu	proton	k1gInSc2	proton
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
těchto	tento	k3xDgInPc2	tento
pokusů	pokus	k1gInPc2	pokus
si	se	k3xPyFc3	se
Otto	Otto	k1gMnSc1	Otto
Frisch	Frisch	k1gMnSc1	Frisch
ověřil	ověřit	k5eAaPmAgMnS	ověřit
experimentálně	experimentálně	k6eAd1	experimentálně
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
správně	správně	k6eAd1	správně
interpretoval	interpretovat	k5eAaBmAgMnS	interpretovat
–	–	k?	–
rozdělením	rozdělení	k1gNnSc7	rozdělení
jádra	jádro	k1gNnPc1	jádro
uranu	uran	k1gInSc2	uran
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
neutronu	neutron	k1gInSc2	neutron
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
tak	tak	k6eAd1	tak
jaderný	jaderný	k2eAgInSc4d1	jaderný
rozpad	rozpad	k1gInSc4	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Enrico	Enrico	k1gNnSc4	Enrico
Fermi	Fer	k1gFnPc7	Fer
i	i	k9	i
Frédéric	Frédéric	k1gMnSc1	Frédéric
Joliot-Curie	Joliot-Curie	k1gFnSc2	Joliot-Curie
nezávisle	závisle	k6eNd1	závisle
na	na	k7c4	na
sobě	se	k3xPyFc3	se
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
pokusy	pokus	k1gInPc4	pokus
předchozích	předchozí	k2eAgMnPc2d1	předchozí
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
stejné	stejný	k2eAgInPc4d1	stejný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Frisch	Frisch	k1gInSc1	Frisch
a	a	k8xC	a
Meitnerová	Meitnerová	k1gFnSc1	Meitnerová
zaslali	zaslat	k5eAaPmAgMnP	zaslat
redakci	redakce	k1gFnSc4	redakce
časopisu	časopis	k1gInSc2	časopis
Nature	Natur	k1gMnSc5	Natur
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
obsahu	obsah	k1gInSc6	obsah
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
jen	jen	k9	jen
telefonicky	telefonicky	k6eAd1	telefonicky
<g/>
.	.	kIx.	.
</s>
<s>
Článek	článek	k1gInSc1	článek
měl	mít	k5eAaImAgInS	mít
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Rozpad	rozpad	k1gInSc1	rozpad
uranu	uran	k1gInSc2	uran
působením	působení	k1gNnSc7	působení
neutronů	neutron	k1gInPc2	neutron
–	–	k?	–
nový	nový	k2eAgInSc4d1	nový
druh	druh	k1gInSc4	druh
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
redakce	redakce	k1gFnSc2	redakce
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
zveřejněn	zveřejněn	k2eAgMnSc1d1	zveřejněn
byl	být	k5eAaImAgMnS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
předložil	předložit	k5eAaPmAgInS	předložit
své	svůj	k3xOyFgFnPc4	svůj
práce	práce	k1gFnPc4	práce
ke	k	k7c3	k
zveřejnění	zveřejnění	k1gNnSc3	zveřejnění
i	i	k8xC	i
F.	F.	kA	F.
Joliot-Curie	Joliot-Curie	k1gFnPc4	Joliot-Curie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Experimentální	experimentální	k2eAgInSc4d1	experimentální
důkaz	důkaz	k1gInSc4	důkaz
výbušného	výbušný	k2eAgInSc2d1	výbušný
rozpadu	rozpad	k1gInSc2	rozpad
jader	jádro	k1gNnPc2	jádro
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
působením	působení	k1gNnSc7	působení
neutronů	neutron	k1gInPc2	neutron
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Niels	Niels	k6eAd1	Niels
Bohr	Bohr	k1gMnSc1	Bohr
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Meitnerová	Meitnerová	k1gFnSc1	Meitnerová
a	a	k8xC	a
Frisch	Frisch	k1gMnSc1	Frisch
podali	podat	k5eAaPmAgMnP	podat
správný	správný	k2eAgInSc4d1	správný
výklad	výklad	k1gInSc4	výklad
Hahnových	Hahnův	k2eAgInPc2d1	Hahnův
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1939	[number]	k4	1939
Bohr	Bohr	k1gMnSc1	Bohr
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
kratší	krátký	k2eAgInSc4d2	kratší
pobyt	pobyt	k1gInSc4	pobyt
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
uvědomoval	uvědomovat	k5eAaImAgInS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
události	událost	k1gFnSc3	událost
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
štěpením	štěpení	k1gNnPc3	štěpení
atomů	atom	k1gInPc2	atom
zabývali	zabývat	k5eAaImAgMnP	zabývat
vědci	vědec	k1gMnPc1	vědec
působící	působící	k2eAgMnPc1d1	působící
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yQgInPc4	který
patřili	patřit	k5eAaImAgMnP	patřit
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
<g/>
,	,	kIx,	,
Leó	Leó	k1gMnPc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
Zinn	Zinn	k1gMnSc1	Zinn
a	a	k8xC	a
Herbert	Herbert	k1gMnSc1	Herbert
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
.	.	kIx.	.
</s>
<s>
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
a	a	k8xC	a
Leó	Leó	k1gFnSc7	Leó
Szilárd	Szilárda	k1gFnPc2	Szilárda
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
definitivně	definitivně	k6eAd1	definitivně
zformulovali	zformulovat	k5eAaPmAgMnP	zformulovat
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
<g/>
:	:	kIx,	:
atom	atom	k1gInSc4	atom
uranu	uran	k1gInSc2	uran
lze	lze	k6eAd1	lze
rozštěpit	rozštěpit	k5eAaPmF	rozštěpit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
procesu	proces	k1gInSc6	proces
tohoto	tento	k3xDgNnSc2	tento
štěpení	štěpení	k1gNnSc2	štěpení
vznikají	vznikat	k5eAaImIp3nP	vznikat
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
následně	následně	k6eAd1	následně
rozštěpit	rozštěpit	k5eAaPmF	rozštěpit
další	další	k2eAgNnPc4d1	další
jádra	jádro	k1gNnPc4	jádro
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tak	tak	k6eAd1	tak
vznik	vznik	k1gInSc4	vznik
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
vědcům	vědec	k1gMnPc3	vědec
bylo	být	k5eAaImAgNnS	být
již	již	k9	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
štěpení	štěpení	k1gNnSc1	štěpení
atomů	atom	k1gInPc2	atom
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
zkonstruování	zkonstruování	k1gNnSc3	zkonstruování
neobyčejně	obyčejně	k6eNd1	obyčejně
účinných	účinný	k2eAgFnPc2d1	účinná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
Szilárd	Szilárd	k1gMnSc1	Szilárd
však	však	k9	však
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
fyzici	fyzik	k1gMnPc1	fyzik
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
okamžitě	okamžitě	k6eAd1	okamžitě
přestali	přestat	k5eAaPmAgMnP	přestat
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
své	svůj	k3xOyFgFnPc4	svůj
studie	studie	k1gFnPc4	studie
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
jaderného	jaderný	k2eAgInSc2d1	jaderný
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
však	však	k9	však
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
iniciativou	iniciativa	k1gFnSc7	iniciativa
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
–	–	k?	–
zejména	zejména	k9	zejména
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
emigroval	emigrovat	k5eAaBmAgInS	emigrovat
do	do	k7c2	do
USA	USA	kA	USA
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
kvůli	kvůli	k7c3	kvůli
nástupu	nástup	k1gInSc3	nástup
fašismu	fašismus	k1gInSc2	fašismus
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
silné	silný	k2eAgFnSc3d1	silná
cenzuře	cenzura	k1gFnSc3	cenzura
na	na	k7c6	na
tamních	tamní	k2eAgFnPc6d1	tamní
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
vědců	vědec	k1gMnPc2	vědec
k	k	k7c3	k
takové	takový	k3xDgFnSc3	takový
iniciativě	iniciativa	k1gFnSc3	iniciativa
byl	být	k5eAaImAgInS	být
samozřejmý	samozřejmý	k2eAgInSc1d1	samozřejmý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
pracovní	pracovní	k2eAgFnSc7d1	pracovní
náplní	náplň	k1gFnSc7	náplň
bylo	být	k5eAaImAgNnS	být
právě	právě	k9	právě
publikování	publikování	k1gNnSc1	publikování
nově	nově	k6eAd1	nově
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tlaku	tlak	k1gInSc3	tlak
představitelů	představitel	k1gMnPc2	představitel
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Szilárd	Szilárd	k1gMnSc1	Szilárd
nucen	nutit	k5eAaImNgMnS	nutit
publikovat	publikovat	k5eAaBmF	publikovat
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
"	"	kIx"	"
<g/>
Řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
v	v	k7c6	v
uranu	uran	k1gInSc6	uran
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
průkopnická	průkopnický	k2eAgFnSc1d1	průkopnická
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Bohr	Bohr	k1gMnSc1	Bohr
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
zdržel	zdržet	k5eAaPmAgInS	zdržet
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
května	květen	k1gInSc2	květen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
poznatkům	poznatek	k1gInPc3	poznatek
<g/>
,	,	kIx,	,
že	že	k8xS	že
uran	uran	k1gInSc1	uran
235	[number]	k4	235
a	a	k8xC	a
plutonium	plutonium	k1gNnSc1	plutonium
mají	mít	k5eAaImIp3nP	mít
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
náchylnost	náchylnost	k1gFnSc4	náchylnost
ke	k	k7c3	k
štěpení	štěpení	k1gNnSc3	štěpení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1939	[number]	k4	1939
Bohr	Bohra	k1gFnPc2	Bohra
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
Wheelerem	Wheeler	k1gMnSc7	Wheeler
upozornil	upozornit	k5eAaPmAgMnS	upozornit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
teoretické	teoretický	k2eAgFnSc6d1	teoretická
studii	studie	k1gFnSc6	studie
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
všechna	všechen	k3xTgNnPc1	všechen
jádra	jádro	k1gNnPc1	jádro
uranu	uran	k1gInSc2	uran
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
se	se	k3xPyFc4	se
štěpí	štěpit	k5eAaImIp3nS	štěpit
uran	uran	k1gInSc1	uran
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hmotě	hmota	k1gFnSc6	hmota
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc1	jeden
atom	atom	k1gInSc1	atom
235U	[number]	k4	235U
na	na	k7c4	na
140	[number]	k4	140
atomů	atom	k1gInPc2	atom
238	[number]	k4	238
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
oddělování	oddělování	k1gNnSc1	oddělování
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Bohr	Bohr	k1gMnSc1	Bohr
svými	svůj	k3xOyFgFnPc7	svůj
pracemi	práce	k1gFnPc7	práce
položil	položit	k5eAaPmAgInS	položit
základ	základ	k1gInSc1	základ
k	k	k7c3	k
intenzivnějšímu	intenzivní	k2eAgNnSc3d2	intenzivnější
zkoumání	zkoumání	k1gNnSc3	zkoumání
problémů	problém	k1gInPc2	problém
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
vědci	vědec	k1gMnPc1	vědec
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
již	již	k6eAd1	již
mohli	moct	k5eAaImAgMnP	moct
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
významné	významný	k2eAgInPc4d1	významný
teoretické	teoretický	k2eAgInPc4d1	teoretický
i	i	k8xC	i
experimentální	experimentální	k2eAgInPc4d1	experimentální
objevy	objev	k1gInPc4	objev
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
problémem	problém	k1gInSc7	problém
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
samotný	samotný	k2eAgInSc1d1	samotný
uran	uran	k1gInSc1	uran
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
štěpný	štěpný	k2eAgMnSc1d1	štěpný
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
izotop	izotop	k1gInSc1	izotop
235	[number]	k4	235
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
podíl	podíl	k1gInSc1	podíl
mezi	mezi	k7c7	mezi
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytujícími	vyskytující	k2eAgInPc7d1	vyskytující
izotopy	izotop	k1gInPc7	izotop
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
zvýšit	zvýšit	k5eAaPmF	zvýšit
jeho	jeho	k3xOp3gFnSc4	jeho
koncentraci	koncentrace	k1gFnSc4	koncentrace
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyřešit	vyřešit	k5eAaPmF	vyřešit
Výzkumné	výzkumný	k2eAgFnSc3d1	výzkumná
laboratoři	laboratoř	k1gFnSc3	laboratoř
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
–	–	k?	–
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Ross	Rossa	k1gFnPc2	Rossa
Gunn	Gunn	k1gInSc1	Gunn
a	a	k8xC	a
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Philip	Philip	k1gMnSc1	Philip
Abelson	Abelson	k1gMnSc1	Abelson
objevili	objevit	k5eAaPmAgMnP	objevit
účinnou	účinný	k2eAgFnSc4d1	účinná
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
jednoduchou	jednoduchý	k2eAgFnSc4d1	jednoduchá
metodu	metoda	k1gFnSc4	metoda
separace	separace	k1gFnSc1	separace
235	[number]	k4	235
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc4	název
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
objevitelích	objevitel	k1gMnPc6	objevitel
–	–	k?	–
Gunn-Abelsonova	Gunn-Abelsonův	k2eAgFnSc1d1	Gunn-Abelsonův
metoda	metoda	k1gFnSc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
objevy	objev	k1gInPc4	objev
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
fyziky	fyzika	k1gFnSc2	fyzika
senzaci	senzace	k1gFnSc4	senzace
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
začalo	začít	k5eAaPmAgNnS	začít
mít	mít	k5eAaImF	mít
i	i	k9	i
obavy	obava	k1gFnPc4	obava
z	z	k7c2	z
možného	možný	k2eAgNnSc2d1	možné
využití	využití	k1gNnSc2	využití
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
vojenským	vojenský	k2eAgInPc3d1	vojenský
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
tisk	tisk	k1gInSc1	tisk
již	již	k6eAd1	již
začal	začít	k5eAaPmAgInS	začít
veřejně	veřejně	k6eAd1	veřejně
psát	psát	k5eAaImF	psát
o	o	k7c6	o
atomové	atomový	k2eAgFnSc6d1	atomová
bombě	bomba	k1gFnSc6	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
znepokojeni	znepokojen	k2eAgMnPc1d1	znepokojen
však	však	k9	však
byli	být	k5eAaImAgMnP	být
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
USA	USA	kA	USA
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
vzrůstající	vzrůstající	k2eAgFnSc3d1	vzrůstající
moci	moc	k1gFnSc3	moc
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
sílícímu	sílící	k2eAgInSc3d1	sílící
antisemitismu	antisemitismus	k1gInSc3	antisemitismus
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
okupace	okupace	k1gFnSc2	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
německými	německý	k2eAgNnPc7d1	německé
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
informoval	informovat	k5eAaBmAgMnS	informovat
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
o	o	k7c6	o
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
berlínském	berlínský	k2eAgInSc6d1	berlínský
experimentu	experiment	k1gInSc6	experiment
profesora	profesor	k1gMnSc2	profesor
Otto	Otto	k1gMnSc1	Otto
Hahna	Hahn	k1gMnSc2	Hahn
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
admirál	admirál	k1gMnSc1	admirál
S.	S.	kA	S.
K.	K.	kA	K.
Hooper	Hooper	k1gMnSc1	Hooper
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
zástupce	zástupce	k1gMnSc2	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
námořních	námořní	k2eAgFnPc2d1	námořní
operací	operace	k1gFnPc2	operace
pro	pro	k7c4	pro
techniku	technika	k1gFnSc4	technika
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
kruhy	kruh	k1gInPc1	kruh
si	se	k3xPyFc3	se
však	však	k8xC	však
dostatečně	dostatečně	k6eAd1	dostatečně
neuvědomovaly	uvědomovat	k5eNaImAgFnP	uvědomovat
možnou	možný	k2eAgFnSc4d1	možná
hrozbu	hrozba	k1gFnSc4	hrozba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
objevům	objev	k1gInPc3	objev
na	na	k7c4	na
poli	pole	k1gFnSc4	pole
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
které	který	k3yIgNnSc1	který
disponovalo	disponovat	k5eAaBmAgNnS	disponovat
dostatečnými	dostatečný	k2eAgInPc7d1	dostatečný
materiálními	materiální	k2eAgInPc7d1	materiální
i	i	k8xC	i
lidskými	lidský	k2eAgInPc7d1	lidský
zdroji	zdroj	k1gInPc7	zdroj
k	k	k7c3	k
sestrojení	sestrojení	k1gNnSc3	sestrojení
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
připlul	připlout	k5eAaPmAgMnS	připlout
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
Hamburk	Hamburk	k1gInSc1	Hamburk
do	do	k7c2	do
New	New	k1gMnPc2	New
Yorku	York	k1gInSc2	York
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
i	i	k9	i
s	s	k7c7	s
několika	několik	k4yIc7	několik
kolegy	kolega	k1gMnPc7	kolega
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
amerických	americký	k2eAgFnPc6d1	americká
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Fermim	Fermi	k1gNnSc7	Fermi
a	a	k8xC	a
s	s	k7c7	s
Georgem	Georg	k1gInSc7	Georg
B.	B.	kA	B.
Pegramem	Pegram	k1gInSc7	Pegram
<g/>
,	,	kIx,	,
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
děkanem	děkan	k1gMnSc7	děkan
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
fakulty	fakulta	k1gFnSc2	fakulta
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
místo	místo	k1gNnSc4	místo
stálého	stálý	k2eAgMnSc2d1	stálý
profesora	profesor	k1gMnSc2	profesor
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
nebyl	být	k5eNaImAgMnS	být
přesvědčený	přesvědčený	k2eAgMnSc1d1	přesvědčený
nacista	nacista	k1gMnSc1	nacista
<g/>
,	,	kIx,	,
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
hlavní	hlavní	k2eAgFnSc2d1	hlavní
osobou	osoba	k1gFnSc7	osoba
německého	německý	k2eAgInSc2d1	německý
uranového	uranový	k2eAgInSc2d1	uranový
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
až	až	k9	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1939	[number]	k4	1939
mohlo	moct	k5eAaImAgNnS	moct
ještě	ještě	k6eAd1	ještě
dvanáct	dvanáct	k4xCc1	dvanáct
lidí	člověk	k1gMnPc2	člověk
zabránit	zabránit	k5eAaPmF	zabránit
výrobě	výroba	k1gFnSc3	výroba
atomových	atomový	k2eAgFnPc2d1	atomová
bomb	bomba	k1gFnPc2	bomba
–	–	k?	–
kdyby	kdyby	kYmCp3nP	kdyby
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
1939	[number]	k4	1939
se	se	k3xPyFc4	se
Leó	Leó	k1gMnSc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Eugenem	Eugen	k1gMnSc7	Eugen
Wignerem	Wigner	k1gMnSc7	Wigner
a	a	k8xC	a
Edwardem	Edward	k1gMnSc7	Edward
Tellerem	Teller	k1gMnSc7	Teller
shodli	shodnout	k5eAaBmAgMnP	shodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
situaci	situace	k1gFnSc4	situace
kolem	kolem	k7c2	kolem
řetězových	řetězový	k2eAgFnPc2d1	řetězová
reakcí	reakce	k1gFnPc2	reakce
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc3	jejich
využití	využití	k1gNnSc3	využití
upozornit	upozornit	k5eAaPmF	upozornit
některého	některý	k3yIgNnSc2	některý
z	z	k7c2	z
vlivných	vlivný	k2eAgMnPc2d1	vlivný
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
napsat	napsat	k5eAaPmF	napsat
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
odeslán	odeslat	k5eAaPmNgInS	odeslat
prezidentovi	prezident	k1gMnSc3	prezident
Franklinovi	Franklin	k1gMnSc3	Franklin
D.	D.	kA	D.
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
a	a	k8xC	a
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejpopulárnější	populární	k2eAgMnSc1d3	nejpopulárnější
fyzik	fyzik	k1gMnSc1	fyzik
v	v	k7c6	v
celých	celý	k2eAgInPc6d1	celý
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
známým	známý	k2eAgMnSc7d1	známý
pacifistou	pacifista	k1gMnSc7	pacifista
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentu	prezident	k1gMnSc3	prezident
Rooseveltovi	Roosevelt	k1gMnSc6	Roosevelt
byl	být	k5eAaImAgInS	být
však	však	k9	však
Einstein-Szilárdův	Einstein-Szilárdův	k2eAgInSc1d1	Einstein-Szilárdův
list	list	k1gInSc1	list
doručen	doručit	k5eAaPmNgInS	doručit
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
přes	přes	k7c4	přes
měsíc	měsíc	k1gInSc4	měsíc
probíhala	probíhat	k5eAaImAgFnS	probíhat
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Upozornili	upozornit	k5eAaPmAgMnP	upozornit
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
sestrojení	sestrojení	k1gNnSc2	sestrojení
"	"	kIx"	"
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
silných	silný	k2eAgFnPc2d1	silná
bomb	bomba	k1gFnPc2	bomba
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prezidenta	prezident	k1gMnSc4	prezident
zpočátku	zpočátku	k6eAd1	zpočátku
dopis	dopis	k1gInSc4	dopis
vědců	vědec	k1gMnPc2	vědec
příliš	příliš	k6eAd1	příliš
nepřesvědčil	přesvědčit	k5eNaPmAgMnS	přesvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Sachsem	Sachs	k1gMnSc7	Sachs
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
dopis	dopis	k1gInSc4	dopis
den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
doručil	doručit	k5eAaPmAgMnS	doručit
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
přikázal	přikázat	k5eAaPmAgMnS	přikázat
řediteli	ředitel	k1gMnPc7	ředitel
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
Lymanu	Lyman	k1gMnSc3	Lyman
Briggsovi	Briggs	k1gMnSc3	Briggs
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
co	co	k9	co
nejkratším	krátký	k2eAgInSc6d3	nejkratší
čase	čas	k1gInSc6	čas
vypracována	vypracován	k2eAgFnSc1d1	vypracována
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
perspektivách	perspektiva	k1gFnPc6	perspektiva
využití	využití	k1gNnSc2	využití
jaderných	jaderný	k2eAgFnPc2d1	jaderná
vlastností	vlastnost	k1gFnPc2	vlastnost
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
Poradní	poradní	k2eAgInSc1d1	poradní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
uran	uran	k1gInSc4	uran
(	(	kIx(	(
<g/>
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
"	"	kIx"	"
<g/>
Uranový	uranový	k2eAgInSc1d1	uranový
výbor	výbor	k1gInSc1	výbor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgFnSc4	první
schůzi	schůze	k1gFnSc4	schůze
"	"	kIx"	"
<g/>
Uranového	uranový	k2eAgInSc2d1	uranový
výboru	výbor	k1gInSc2	výbor
<g/>
"	"	kIx"	"
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1939	[number]	k4	1939
byli	být	k5eAaImAgMnP	být
přizváni	přizván	k2eAgMnPc1d1	přizván
kromě	kromě	k7c2	kromě
Alexandra	Alexandr	k1gMnSc2	Alexandr
Sachse	Sachs	k1gMnSc2	Sachs
a	a	k8xC	a
dvou	dva	k4xCgMnPc2	dva
dělostřeleckých	dělostřelecký	k2eAgMnPc2d1	dělostřelecký
odborníků	odborník	k1gMnPc2	odborník
i	i	k9	i
fyzici	fyzik	k1gMnPc1	fyzik
Szilárd	Szilárd	k1gMnSc1	Szilárd
<g/>
,	,	kIx,	,
Wigner	Wigner	k1gMnSc1	Wigner
a	a	k8xC	a
Teller	Teller	k1gMnSc1	Teller
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
předložil	předložit	k5eAaPmAgInS	předložit
prezidentovi	prezident	k1gMnSc3	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
svou	svůj	k3xOyFgFnSc4	svůj
zprávu	zpráva	k1gFnSc4	zpráva
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
reálnou	reálný	k2eAgFnSc4d1	reálná
možnost	možnost	k1gFnSc4	možnost
využití	využití	k1gNnSc2	využití
atomové	atomový	k2eAgFnSc2d1	atomová
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
výroby	výroba	k1gFnSc2	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
udělení	udělení	k1gNnSc6	udělení
první	první	k4xOgFnPc1	první
dotace	dotace	k1gFnPc1	dotace
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
6	[number]	k4	6
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
informoval	informovat	k5eAaBmAgInS	informovat
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1940	[number]	k4	1940
Lyman	Lymana	k1gFnPc2	Lymana
Briggs	Briggsa	k1gFnPc2	Briggsa
vojenského	vojenský	k2eAgMnSc2d1	vojenský
pobočníka	pobočník	k1gMnSc2	pobočník
prezidenta	prezident	k1gMnSc2	prezident
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
<g/>
,	,	kIx,	,
generála	generál	k1gMnSc2	generál
Watsona	Watson	k1gMnSc2	Watson
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
Szilárdova	Szilárdův	k2eAgInSc2d1	Szilárdův
podnětu	podnět	k1gInSc2	podnět
poslal	poslat	k5eAaPmAgMnS	poslat
Einstein	Einstein	k1gMnSc1	Einstein
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
další	další	k2eAgInSc1d1	další
dopis	dopis	k1gInSc1	dopis
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
ho	on	k3xPp3gMnSc4	on
upozorňoval	upozorňovat	k5eAaImAgInS	upozorňovat
na	na	k7c4	na
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
zájem	zájem	k1gInSc4	zájem
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
o	o	k7c4	o
uran	uran	k1gInSc4	uran
a	a	k8xC	a
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
práce	práce	k1gFnSc1	práce
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
urychlit	urychlit	k5eAaPmF	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Einstein	Einstein	k1gMnSc1	Einstein
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
jsem	být	k5eAaImIp1nS	být
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
v	v	k7c6	v
tajnosti	tajnost	k1gFnSc6	tajnost
provádí	provádět	k5eAaImIp3nS	provádět
výzkum	výzkum	k1gInSc4	výzkum
zejména	zejména	k9	zejména
ve	v	k7c6	v
Fyzikálním	fyzikální	k2eAgInSc6d1	fyzikální
ústavu	ústav	k1gInSc6	ústav
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ústav	ústav	k1gInSc1	ústav
byl	být	k5eAaImAgInS	být
přesunut	přesunout	k5eAaPmNgInS	přesunout
pod	pod	k7c4	pod
správu	správa	k1gFnSc4	správa
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tam	tam	k6eAd1	tam
pracuje	pracovat	k5eAaImIp3nS	pracovat
skupina	skupina	k1gFnSc1	skupina
fyziků	fyzik	k1gMnPc2	fyzik
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Carla	Carl	k1gMnSc2	Carl
Friedricha	Friedrich	k1gMnSc2	Friedrich
von	von	k1gInSc4	von
Weizsäcker	Weizsäcker	k1gMnSc1	Weizsäcker
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Chemickým	chemický	k2eAgInSc7d1	chemický
ústavem	ústav	k1gInSc7	ústav
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
problémů	problém	k1gInPc2	problém
uranu	uran	k1gInSc2	uran
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
celý	celý	k2eAgInSc1d1	celý
výzkum	výzkum	k1gInSc1	výzkum
postupoval	postupovat	k5eAaImAgInS	postupovat
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
byrokracii	byrokracie	k1gFnSc3	byrokracie
okolo	okolo	k7c2	okolo
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgFnSc1d1	příští
schůze	schůze	k1gFnSc1	schůze
"	"	kIx"	"
<g/>
Uranového	uranový	k2eAgInSc2d1	uranový
výboru	výbor	k1gInSc2	výbor
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k9	už
vědci	vědec	k1gMnPc1	vědec
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
štěpení	štěpení	k1gNnSc4	štěpení
vyvolané	vyvolaný	k2eAgInPc4d1	vyvolaný
neutrony	neutron	k1gInPc4	neutron
probíhá	probíhat	k5eAaImIp3nS	probíhat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
uranu	uran	k1gInSc2	uran
235	[number]	k4	235
<g/>
U.	U.	kA	U.
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byli	být	k5eAaImAgMnP	být
výzkumem	výzkum	k1gInSc7	výzkum
uranu	uran	k1gInSc2	uran
pověřeni	pověřit	k5eAaPmNgMnP	pověřit
pracovníci	pracovník	k1gMnPc1	pracovník
Fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
ústavu	ústav	k1gInSc2	ústav
císaře	císař	k1gMnSc2	císař
Viléma	Vilém	k1gMnSc2	Vilém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výboru	výbor	k1gInSc6	výbor
byla	být	k5eAaImAgFnS	být
projednána	projednán	k2eAgFnSc1d1	projednána
i	i	k9	i
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
celý	celý	k2eAgInSc4d1	celý
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
USA	USA	kA	USA
podpořit	podpořit	k5eAaPmF	podpořit
a	a	k8xC	a
jak	jak	k6eAd1	jak
ho	on	k3xPp3gMnSc4	on
i	i	k9	i
lépe	dobře	k6eAd2	dobře
zorganizovat	zorganizovat	k5eAaPmF	zorganizovat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
poradní	poradní	k2eAgFnSc1d1	poradní
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Lyman	Lyman	k1gInSc1	Lyman
Briggs	Briggs	k1gInSc1	Briggs
při	při	k7c6	při
úřadu	úřad	k1gInSc6	úřad
pro	pro	k7c4	pro
normalizaci	normalizace	k1gFnSc4	normalizace
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
projednala	projednat	k5eAaPmAgFnS	projednat
celkovou	celkový	k2eAgFnSc4d1	celková
situaci	situace	k1gFnSc4	situace
kolem	kolem	k7c2	kolem
uranového	uranový	k2eAgInSc2d1	uranový
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
vysloven	vyslovit	k5eAaPmNgInS	vyslovit
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Uranový	uranový	k2eAgInSc1d1	uranový
výbor	výbor	k1gInSc1	výbor
získal	získat	k5eAaPmAgInS	získat
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
uranouhlíkového	uranouhlíkový	k2eAgInSc2d1	uranouhlíkový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
výbor	výbor	k1gInSc1	výbor
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
(	(	kIx(	(
<g/>
NDRC	NDRC	kA	NDRC
<g/>
)	)	kIx)	)
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
Uranový	uranový	k2eAgInSc1d1	uranový
výbor	výbor	k1gInSc1	výbor
přeměněn	přeměněn	k2eAgInSc1d1	přeměněn
na	na	k7c4	na
podvýbor	podvýbor	k1gInSc4	podvýbor
Výzkumného	výzkumný	k2eAgInSc2d1	výzkumný
výboru	výbor	k1gInSc2	výbor
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
NDRC	NDRC	kA	NDRC
byl	být	k5eAaImAgInS	být
jmenován	jmenován	k2eAgInSc4d1	jmenován
Vannevar	Vannevar	k1gInSc4	Vannevar
Bush	Bush	k1gMnSc1	Bush
<g/>
,	,	kIx,	,
podvýbor	podvýbor	k1gInSc1	podvýbor
měl	mít	k5eAaImAgInS	mít
nadále	nadále	k6eAd1	nadále
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
Briggs	Briggsa	k1gFnPc2	Briggsa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
výboru	výbor	k1gInSc3	výbor
už	už	k6eAd1	už
nebyl	být	k5eNaImAgMnS	být
přizván	přizvat	k5eAaPmNgMnS	přizvat
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
požádali	požádat	k5eAaPmAgMnP	požádat
fyzici	fyzik	k1gMnPc1	fyzik
působící	působící	k2eAgMnPc1d1	působící
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
o	o	k7c4	o
částku	částka	k1gFnSc4	částka
140	[number]	k4	140
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
výzkumy	výzkum	k1gInPc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
1941	[number]	k4	1941
stál	stát	k5eAaImAgInS	stát
výzkum	výzkum	k1gInSc4	výzkum
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
300	[number]	k4	300
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
atomový	atomový	k2eAgInSc1d1	atomový
projekt	projekt	k1gInSc1	projekt
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
S	s	k7c7	s
1	[number]	k4	1
NDRC	NDRC	kA	NDRC
<g/>
.	.	kIx.	.
</s>
<s>
Briggs	Briggs	k6eAd1	Briggs
se	se	k3xPyFc4	se
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
obrátil	obrátit	k5eAaPmAgInS	obrátit
na	na	k7c4	na
Bushe	Bush	k1gMnSc4	Bush
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
zřízení	zřízení	k1gNnSc4	zřízení
informačního	informační	k2eAgInSc2d1	informační
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
výbor	výbor	k1gInSc1	výbor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
při	při	k7c6	při
Národní	národní	k2eAgFnSc6d1	národní
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
stál	stát	k5eAaImAgMnS	stát
Arthur	Arthur	k1gMnSc1	Arthur
H.	H.	kA	H.
Compton	Compton	k1gInSc1	Compton
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výbor	výbor	k1gInSc1	výbor
měl	mít	k5eAaImAgInS	mít
zhodnotit	zhodnotit	k5eAaPmF	zhodnotit
vojenský	vojenský	k2eAgInSc1d1	vojenský
význam	význam	k1gInSc1	význam
uranového	uranový	k2eAgInSc2d1	uranový
projektu	projekt	k1gInSc2	projekt
a	a	k8xC	a
stanovit	stanovit	k5eAaPmF	stanovit
výši	výše	k1gFnSc4	výše
prostředků	prostředek	k1gInPc2	prostředek
potřebných	potřebný	k2eAgInPc2d1	potřebný
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInSc4	jeho
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přezkoumání	přezkoumání	k1gNnSc6	přezkoumání
celého	celý	k2eAgInSc2d1	celý
problému	problém	k1gInSc2	problém
v	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
byly	být	k5eAaImAgFnP	být
předloženy	předložen	k2eAgFnPc1d1	předložena
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
Bush	Bush	k1gMnSc1	Bush
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
výzkum	výzkum	k1gInSc1	výzkum
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
značně	značně	k6eAd1	značně
urychlit	urychlit	k5eAaPmF	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
předložil	předložit	k5eAaPmAgMnS	předložit
i	i	k9	i
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
výzkum	výzkum	k1gInSc1	výzkum
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jednak	jednak	k8xC	jednak
rozšířit	rozšířit	k5eAaPmF	rozšířit
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
i	i	k9	i
podrobnou	podrobný	k2eAgFnSc4d1	podrobná
výměnu	výměna	k1gFnSc4	výměna
informací	informace	k1gFnPc2	informace
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
výzkumu	výzkum	k1gInSc6	výzkum
již	již	k6eAd1	již
také	také	k9	také
značně	značně	k6eAd1	značně
pokročili	pokročit	k5eAaPmAgMnP	pokročit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgNnSc4d1	další
směřování	směřování	k1gNnSc4	směřování
uranového	uranový	k2eAgInSc2d1	uranový
výzkumu	výzkum	k1gInSc2	výzkum
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
dva	dva	k4xCgInPc4	dva
Bushovy	Bushův	k2eAgInPc4d1	Bushův
závěry	závěr	k1gInPc4	závěr
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
atomových	atomový	k2eAgFnPc2d1	atomová
bomb	bomba	k1gFnPc2	bomba
využitelných	využitelný	k2eAgFnPc2d1	využitelná
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
válce	válka	k1gFnSc6	válka
je	být	k5eAaImIp3nS	být
dostatečné	dostatečná	k1gFnPc4	dostatečná
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
zdůvodněno	zdůvodněn	k2eAgNnSc1d1	zdůvodněno
obrovské	obrovský	k2eAgNnSc1d1	obrovské
úsilí	úsilí	k1gNnSc1	úsilí
potřebné	potřebný	k2eAgNnSc1d1	potřebné
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
vývoji	vývoj	k1gInSc3	vývoj
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
uranová	uranový	k2eAgFnSc1d1	uranová
sekce	sekce	k1gFnSc1	sekce
NDRC	NDRC	kA	NDRC
neplní	plnit	k5eNaImIp3nS	plnit
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jí	on	k3xPp3gFnSc2	on
byly	být	k5eAaImAgFnP	být
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
závěry	závěr	k1gInPc7	závěr
se	se	k3xPyFc4	se
ztotožnil	ztotožnit	k5eAaPmAgInS	ztotožnit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bílém	bílý	k2eAgInSc6d1	bílý
domě	dům	k1gInSc6	dům
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
sešel	sejít	k5eAaPmAgMnS	sejít
viceprezident	viceprezident	k1gMnSc1	viceprezident
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
náčelník	náčelník	k1gMnSc1	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
NDRC	NDRC	kA	NDRC
Vannevar	Vannevar	k1gInSc1	Vannevar
Bush	Bush	k1gMnSc1	Bush
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
zástupce	zástupce	k1gMnSc1	zástupce
James	James	k1gMnSc1	James
Conant	Conant	k1gMnSc1	Conant
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
politické	politický	k2eAgFnSc6d1	politická
úrovni	úroveň	k1gFnSc6	úroveň
projednali	projednat	k5eAaPmAgMnP	projednat
otázky	otázka	k1gFnPc4	otázka
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
atomové	atomový	k2eAgFnSc2d1	atomová
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
vedení	vedení	k1gNnSc2	vedení
sekce	sekce	k1gFnSc2	sekce
S	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zástupcem	zástupce	k1gMnSc7	zástupce
vedoucího	vedoucí	k2eAgInSc2d1	vedoucí
uranového	uranový	k2eAgInSc2d1	uranový
podvýboru	podvýbor	k1gInSc2	podvýbor
Briggse	Briggse	k1gFnSc2	Briggse
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
G.	G.	kA	G.
Pegram	Pegram	k1gInSc4	Pegram
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Conant	Conant	k1gMnSc1	Conant
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
i	i	k9	i
zmocněnci	zmocněnec	k1gMnPc1	zmocněnec
pro	pro	k7c4	pro
nové	nový	k2eAgInPc4d1	nový
úkoly	úkol	k1gInPc4	úkol
–	–	k?	–
Arthur	Arthura	k1gFnPc2	Arthura
Compton	Compton	k1gInSc1	Compton
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
Orlando	Orlanda	k1gFnSc5	Orlanda
Lawrence	Lawrence	k1gFnPc4	Lawrence
a	a	k8xC	a
Harold	Harold	k1gInSc4	Harold
Urey	urea	k1gFnSc2	urea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
výzkumech	výzkum	k1gInPc6	výzkum
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
pokročili	pokročit	k5eAaPmAgMnP	pokročit
i	i	k9	i
vědci	vědec	k1gMnPc1	vědec
<g/>
.	.	kIx.	.
</s>
<s>
Skupině	skupina	k1gFnSc3	skupina
chemiků	chemik	k1gMnPc2	chemik
a	a	k8xC	a
fyziků	fyzik	k1gMnPc2	fyzik
vedené	vedený	k2eAgFnSc2d1	vedená
Glennem	Glenn	k1gMnSc7	Glenn
Seaborgem	Seaborg	k1gMnSc7	Seaborg
a	a	k8xC	a
Edwardem	Edward	k1gMnSc7	Edward
McMillanem	McMillan	k1gMnSc7	McMillan
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1941	[number]	k4	1941
přeměnit	přeměnit	k5eAaPmF	přeměnit
jeden	jeden	k4xCgInSc4	jeden
chemický	chemický	k2eAgInSc4d1	chemický
prvek	prvek	k1gInSc4	prvek
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Bombardováním	bombardování	k1gNnSc7	bombardování
238U	[number]	k4	238U
neutrony	neutron	k1gInPc7	neutron
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
dosud	dosud	k6eAd1	dosud
neznámý	známý	k2eNgInSc4d1	neznámý
prvek	prvek	k1gInSc4	prvek
s	s	k7c7	s
protonovým	protonový	k2eAgNnSc7d1	protonové
číslem	číslo	k1gNnSc7	číslo
94	[number]	k4	94
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
nazván	nazvat	k5eAaBmNgInS	nazvat
plutoniem	plutonium	k1gNnSc7	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vědci	vědec	k1gMnPc7	vědec
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
pokusem	pokus	k1gInSc7	pokus
otevřel	otevřít	k5eAaPmAgInS	otevřít
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
štěpení	štěpení	k1gNnSc4	štěpení
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
tuny	tuna	k1gFnSc2	tuna
přírodního	přírodní	k2eAgInSc2d1	přírodní
uranu	uran	k1gInSc2	uran
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
separací	separace	k1gFnSc7	separace
vyrobit	vyrobit	k5eAaPmF	vyrobit
4	[number]	k4	4
kg	kg	kA	kg
235	[number]	k4	235
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
řetězovou	řetězový	k2eAgFnSc7d1	řetězová
reakcí	reakce	k1gFnSc7	reakce
neutronů	neutron	k1gInPc2	neutron
s	s	k7c7	s
uranem	uran	k1gInSc7	uran
mohli	moct	k5eAaImAgMnP	moct
získat	získat	k5eAaPmF	získat
0,60	[number]	k4	0,60
kg	kg	kA	kg
plutonia	plutonium	k1gNnSc2	plutonium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vynikající	vynikající	k2eAgInPc4d1	vynikající
explozivní	explozivní	k2eAgInPc4d1	explozivní
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
experimentální	experimentální	k2eAgInSc1d1	experimentální
důkaz	důkaz	k1gInSc1	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vyrobit	vyrobit	k5eAaPmF	vyrobit
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
z	z	k7c2	z
plutonia	plutonium	k1gNnSc2	plutonium
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
pozdější	pozdní	k2eAgInPc4d2	pozdější
výpočty	výpočet	k1gInPc4	výpočet
a	a	k8xC	a
zkušenosti	zkušenost	k1gFnSc2	zkušenost
tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
upravily	upravit	k5eAaPmAgInP	upravit
na	na	k7c4	na
7	[number]	k4	7
kg	kg	kA	kg
uranu	uran	k1gInSc2	uran
z	z	k7c2	z
tuny	tuna	k1gFnSc2	tuna
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
zasedla	zasednout	k5eAaPmAgFnS	zasednout
komise	komise	k1gFnSc1	komise
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
reorganizaci	reorganizace	k1gFnSc6	reorganizace
S	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
Japonská	japonský	k2eAgFnSc1d1	japonská
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
přepadla	přepadnout	k5eAaPmAgFnS	přepadnout
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
základnu	základna	k1gFnSc4	základna
Pearl	Pearl	k1gMnSc1	Pearl
Harbour	Harbour	k1gMnSc1	Harbour
a	a	k8xC	a
v	v	k7c6	v
myšlení	myšlení	k1gNnSc6	myšlení
politických	politický	k2eAgMnPc2d1	politický
i	i	k8xC	i
vojenských	vojenský	k2eAgMnPc2d1	vojenský
představitelů	představitel	k1gMnPc2	představitel
nastal	nastat	k5eAaPmAgInS	nastat
zlom	zlom	k1gInSc1	zlom
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
předložil	předložit	k5eAaPmAgMnS	předložit
Bush	Bush	k1gMnSc1	Bush
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nastínila	nastínit	k5eAaPmAgFnS	nastínit
možnosti	možnost	k1gFnPc4	možnost
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Rýsovaly	rýsovat	k5eAaImAgFnP	rýsovat
se	se	k3xPyFc4	se
čtyři	čtyři	k4xCgFnPc1	čtyři
cesty	cesta	k1gFnPc1	cesta
k	k	k7c3	k
získání	získání	k1gNnSc3	získání
štěpných	štěpný	k2eAgInPc2d1	štěpný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
uranu	uran	k1gInSc2	uran
to	ten	k3xDgNnSc4	ten
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
metoda	metoda	k1gFnSc1	metoda
difuzního	difuzní	k2eAgMnSc2d1	difuzní
<g/>
,	,	kIx,	,
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
nebo	nebo	k8xC	nebo
odstředivého	odstředivý	k2eAgNnSc2d1	odstředivé
dělení	dělení	k1gNnSc2	dělení
izotopů	izotop	k1gInPc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc7	čtvrtý
cestou	cesta	k1gFnSc7	cesta
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
plutonia	plutonium	k1gNnSc2	plutonium
řetězovou	řetězový	k2eAgFnSc7d1	řetězová
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
však	však	k9	však
ani	ani	k9	ani
vědci	vědec	k1gMnPc1	vědec
pracující	pracující	k2eAgMnPc1d1	pracující
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
výzkumech	výzkum	k1gInPc6	výzkum
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
metoda	metoda	k1gFnSc1	metoda
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
nejúčinnější	účinný	k2eAgFnSc1d3	nejúčinnější
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejrychlejší	rychlý	k2eAgMnSc1d3	nejrychlejší
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
také	také	k9	také
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
už	už	k6eAd1	už
nebude	být	k5eNaImBp3nS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
řídit	řídit	k5eAaImF	řídit
přímo	přímo	k6eAd1	přímo
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
obrany	obrana	k1gFnSc2	obrana
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
velitelem	velitel	k1gMnSc7	velitel
atomových	atomový	k2eAgInPc2d1	atomový
výzkumů	výzkum	k1gInPc2	výzkum
jmenovalo	jmenovat	k5eAaBmAgNnS	jmenovat
plukovníka	plukovník	k1gMnSc2	plukovník
ženijní	ženijní	k2eAgFnSc2d1	ženijní
služby	služba	k1gFnSc2	služba
Jamese	Jamese	k1gFnSc2	Jamese
C.	C.	kA	C.
Marshalla	Marshall	k1gMnSc2	Marshall
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sídla	sídlo	k1gNnSc2	sídlo
jeho	jeho	k3xOp3gInSc2	jeho
štábu	štáb	k1gInSc2	štáb
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Manhattan	Manhattan	k1gInSc1	Manhattan
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
i	i	k9	i
nové	nový	k2eAgNnSc1d1	nové
označení	označení	k1gNnSc1	označení
projektu	projekt	k1gInSc2	projekt
–	–	k?	–
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
útvar	útvar	k1gInSc1	útvar
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
krycí	krycí	k2eAgInSc4d1	krycí
název	název	k1gInSc4	název
Projekt	projekt	k1gInSc1	projekt
DSM	DSM	kA	DSM
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Development	Development	k1gInSc1	Development
of	of	k?	of
Substitute	substitut	k1gInSc5	substitut
Materials	Materials	k1gInSc4	Materials
–	–	k?	–
Vývoj	vývoj	k1gInSc1	vývoj
náhradních	náhradní	k2eAgInPc2d1	náhradní
materiálů	materiál	k1gInPc2	materiál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
brigádní	brigádní	k2eAgMnSc1d1	brigádní
generál	generál	k1gMnSc1	generál
ženijních	ženijní	k2eAgFnPc2d1	ženijní
vojsk	vojsko	k1gNnPc2	vojsko
Leslie	Leslie	k1gFnSc2	Leslie
Groves	Grovesa	k1gFnPc2	Grovesa
<g/>
.	.	kIx.	.
</s>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
Marshall	Marshall	k1gMnSc1	Marshall
získal	získat	k5eAaPmAgInS	získat
funkci	funkce	k1gFnSc4	funkce
hlavního	hlavní	k2eAgMnSc2d1	hlavní
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgMnPc2d1	jiný
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
i	i	k9	i
Franklin	Franklina	k1gFnPc2	Franklina
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
a	a	k8xC	a
Leslie	Leslie	k1gFnSc1	Leslie
Groves	Grovesa	k1gFnPc2	Grovesa
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
program	program	k1gInSc1	program
značně	značně	k6eAd1	značně
urychlen	urychlit	k5eAaPmNgInS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
závodů	závod	k1gInPc2	závod
a	a	k8xC	a
potřebných	potřebný	k2eAgFnPc2d1	potřebná
laboratoří	laboratoř	k1gFnPc2	laboratoř
se	se	k3xPyFc4	se
vycházelo	vycházet	k5eAaImAgNnS	vycházet
především	především	k9	především
z	z	k7c2	z
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
utajení	utajení	k1gNnSc4	utajení
celého	celý	k2eAgInSc2d1	celý
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Tennessee	Tennessee	k1gFnPc2	Tennessee
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
město	město	k1gNnSc4	město
Oak	Oak	k1gFnSc3	Oak
Ridge	Ridg	k1gFnSc2	Ridg
se	se	k3xPyFc4	se
79	[number]	k4	79
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závodech	závod	k1gInPc6	závod
tohoto	tento	k3xDgNnSc2	tento
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
uranové	uranový	k2eAgFnSc2d1	uranová
rudy	ruda	k1gFnSc2	ruda
získáván	získáván	k2eAgInSc4d1	získáván
uran	uran	k1gInSc4	uran
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Columbia	Columbia	k1gFnSc1	Columbia
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
město	město	k1gNnSc1	město
Hanford	Hanford	k1gInSc1	Hanford
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
uran	uran	k1gInSc4	uran
238	[number]	k4	238
přeměňován	přeměňován	k2eAgInSc4d1	přeměňován
na	na	k7c4	na
plutonium	plutonium	k1gNnSc4	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1942	[number]	k4	1942
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Roberta	Robert	k1gMnSc2	Robert
Oppenheimera	Oppenheimero	k1gNnSc2	Oppenheimero
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
zařízení	zařízení	k1gNnSc4	zařízení
ke	k	k7c3	k
zkonstruování	zkonstruování	k1gNnSc3	zkonstruování
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
získalo	získat	k5eAaPmAgNnS	získat
kódové	kódový	k2eAgNnSc1d1	kódové
označení	označení	k1gNnSc1	označení
Y.	Y.	kA	Y.
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
řešit	řešit	k5eAaImF	řešit
všechny	všechen	k3xTgInPc4	všechen
problémy	problém	k1gInPc4	problém
týkající	týkající	k2eAgInPc4d1	týkající
se	se	k3xPyFc4	se
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
od	od	k7c2	od
matematických	matematický	k2eAgInPc2d1	matematický
výpočtů	výpočet	k1gInPc2	výpočet
až	až	k9	až
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
dílčích	dílčí	k2eAgInPc2d1	dílčí
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
řízeny	řídit	k5eAaImNgFnP	řídit
různými	různý	k2eAgMnPc7d1	různý
fyziky	fyzik	k1gMnPc7	fyzik
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
byl	být	k5eAaImAgMnS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
vývoje	vývoj	k1gInSc2	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
v	v	k7c4	v
Los	los	k1gInSc4	los
Alamos	Alamosa	k1gFnPc2	Alamosa
<g/>
.	.	kIx.	.
</s>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Lawrence	Lawrenec	k1gInSc2	Lawrenec
řídil	řídit	k5eAaImAgMnS	řídit
radiační	radiační	k2eAgFnSc4d1	radiační
laboratoř	laboratoř	k1gFnSc4	laboratoř
Kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
laboratoř	laboratoř	k1gFnSc1	laboratoř
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdokonalovala	zdokonalovat	k5eAaImAgFnS	zdokonalovat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
metoda	metoda	k1gFnSc1	metoda
štěpení	štěpení	k1gNnSc2	štěpení
izotopu	izotop	k1gInSc2	izotop
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Laboratoř	laboratoř	k1gFnSc1	laboratoř
sloužila	sloužit	k5eAaImAgFnS	sloužit
jako	jako	k9	jako
zkušební	zkušební	k2eAgInSc4d1	zkušební
závod	závod	k1gInSc4	závod
podniku	podnik	k1gInSc2	podnik
Y-12	Y-12	k1gFnSc2	Y-12
v	v	k7c6	v
Oak	Oak	k1gFnSc6	Oak
Ridge	Ridg	k1gFnSc2	Ridg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
získáno	získán	k2eAgNnSc1d1	získáno
i	i	k8xC	i
podstatné	podstatný	k2eAgNnSc1d1	podstatné
množství	množství	k1gNnSc1	množství
uranu	uran	k1gInSc2	uran
potřebného	potřebné	k1gNnSc2	potřebné
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
bomby	bomba	k1gFnSc2	bomba
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
shozena	shodit	k5eAaPmNgFnS	shodit
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
<g/>
.	.	kIx.	.
</s>
<s>
Harold	Harold	k1gInSc1	Harold
Urey	urea	k1gFnSc2	urea
řídil	řídit	k5eAaImAgInS	řídit
projekt	projekt	k1gInSc1	projekt
Kolumbijské	kolumbijský	k2eAgFnSc2d1	kolumbijská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
štěpení	štěpení	k1gNnSc4	štěpení
izotopů	izotop	k1gInPc2	izotop
235U	[number]	k4	235U
metodou	metoda	k1gFnSc7	metoda
plynové	plynový	k2eAgFnSc2d1	plynová
difúze	difúze	k1gFnSc2	difúze
v	v	k7c6	v
Oak	Oak	k1gFnSc6	Oak
Ridge	Ridg	k1gInSc2	Ridg
<g/>
.	.	kIx.	.
</s>
<s>
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
<g/>
,	,	kIx,	,
Leó	Leó	k1gMnPc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
Compton	Compton	k1gInSc1	Compton
<g/>
,	,	kIx,	,
Eugene	Eugen	k1gInSc5	Eugen
Wigner	Wigner	k1gMnSc1	Wigner
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
pracovali	pracovat	k5eAaImAgMnP	pracovat
v	v	k7c6	v
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
hutní	hutní	k2eAgFnSc6d1	hutní
laboratoři	laboratoř	k1gFnSc6	laboratoř
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
X	X	kA	X
-10	-10	k4	-10
v	v	k7c6	v
Oak	Oak	k1gFnSc6	Oak
Ridge	Ridg	k1gFnSc2	Ridg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
položili	položit	k5eAaPmAgMnP	položit
základy	základ	k1gInPc4	základ
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
výstavby	výstavba	k1gFnSc2	výstavba
velkých	velký	k2eAgInPc2d1	velký
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
reaktorů	reaktor	k1gInPc2	reaktor
v	v	k7c6	v
Hanfordu	Hanford	k1gInSc6	Hanford
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
reaktorech	reaktor	k1gInPc6	reaktor
bylo	být	k5eAaImAgNnS	být
získáno	získán	k2eAgNnSc1d1	získáno
plutonium	plutonium	k1gNnSc1	plutonium
pro	pro	k7c4	pro
bombu	bomba	k1gFnSc4	bomba
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
shozenou	shozený	k2eAgFnSc7d1	shozená
na	na	k7c4	na
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
atomovou	atomový	k2eAgFnSc4d1	atomová
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
vyzkoušena	vyzkoušet	k5eAaPmNgFnS	vyzkoušet
v	v	k7c6	v
Alamogordu	Alamogord	k1gInSc6	Alamogord
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
úspěšně	úspěšně	k6eAd1	úspěšně
zvládnout	zvládnout	k5eAaPmF	zvládnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
řízená	řízený	k2eAgFnSc1d1	řízená
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
vědcům	vědec	k1gMnPc3	vědec
podařilo	podařit	k5eAaPmAgNnS	podařit
překonat	překonat	k5eAaPmF	překonat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
překážky	překážka	k1gFnPc4	překážka
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
<g/>
.	.	kIx.	.
</s>
<s>
Fyzici	fyzik	k1gMnPc1	fyzik
už	už	k6eAd1	už
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
množství	množství	k1gNnSc1	množství
grafitu	grafit	k1gInSc2	grafit
<g/>
,	,	kIx,	,
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uranového	uranový	k2eAgInSc2d1	uranový
<g/>
.	.	kIx.	.
</s>
<s>
Atomový	atomový	k2eAgInSc1d1	atomový
reaktor	reaktor	k1gInSc1	reaktor
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
postaven	postavit	k5eAaPmNgInS	postavit
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Arggonském	Arggonský	k2eAgInSc6d1	Arggonský
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
laboratoře	laboratoř	k1gFnPc1	laboratoř
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Fermiho	Fermi	k1gMnSc2	Fermi
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
postaven	postavit	k5eAaPmNgMnS	postavit
pod	pod	k7c7	pod
západními	západní	k2eAgFnPc7d1	západní
tribunami	tribuna	k1gFnPc7	tribuna
univerzitního	univerzitní	k2eAgInSc2d1	univerzitní
stadionu	stadion	k1gInSc2	stadion
Stagg	Stagg	k1gMnSc1	Stagg
Field	Field	k1gMnSc1	Field
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
reaktoru	reaktor	k1gInSc2	reaktor
začala	začít	k5eAaPmAgFnS	začít
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1942	[number]	k4	1942
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
spotřebováno	spotřebovat	k5eAaPmNgNnS	spotřebovat
46	[number]	k4	46
tun	tuna	k1gFnPc2	tuna
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
385	[number]	k4	385
tun	tuna	k1gFnPc2	tuna
grafitu	grafit	k1gInSc2	grafit
<g/>
.	.	kIx.	.
</s>
<s>
Montáž	montáž	k1gFnSc1	montáž
reaktoru	reaktor	k1gInSc2	reaktor
probíhala	probíhat	k5eAaImAgFnS	probíhat
jen	jen	k9	jen
podle	podle	k7c2	podle
celkového	celkový	k2eAgInSc2d1	celkový
plánu	plán	k1gInSc2	plán
a	a	k8xC	a
podrobně	podrobně	k6eAd1	podrobně
propracované	propracovaný	k2eAgInPc1d1	propracovaný
výkresy	výkres	k1gInPc1	výkres
se	se	k3xPyFc4	se
nedělaly	dělat	k5eNaImAgInP	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
všechny	všechen	k3xTgFnPc1	všechen
přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
štěpení	štěpení	k1gNnSc2	štěpení
jader	jádro	k1gNnPc2	jádro
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
také	také	k9	také
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Site	Site	k1gFnSc1	Site
W	W	kA	W
(	(	kIx(	(
Hanford	Hanford	k1gMnSc1	Hanford
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zařízení	zařízení	k1gNnPc4	zařízení
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
plutonia	plutonium	k1gNnSc2	plutonium
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Hanford	Hanforda	k1gFnPc2	Hanforda
Site	Site	k1gFnSc1	Site
<g/>
)	)	kIx)	)
Site	Site	k1gInSc1	Site
X	X	kA	X
(	(	kIx(	(
<g/>
Oak	Oak	k1gFnSc1	Oak
Ridge	Ridge	k1gFnSc1	Ridge
<g/>
,	,	kIx,	,
Tennessee	Tennessee	k1gFnSc1	Tennessee
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
výroba	výroba	k1gFnSc1	výroba
obohaceného	obohacený	k2eAgInSc2d1	obohacený
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
výroby	výroba	k1gFnSc2	výroba
plutonia	plutonium	k1gNnSc2	plutonium
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Oak	Oak	k1gMnSc2	Oak
Ridge	Ridg	k1gMnSc2	Ridg
National	National	k1gMnSc2	National
<g />
.	.	kIx.	.
</s>
<s>
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
)	)	kIx)	)
Site	Site	k1gInSc1	Site
Y	Y	kA	Y
(	(	kIx(	(
<g/>
Los	los	k1gMnSc1	los
Alamos	Alamos	k1gMnSc1	Alamos
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
laboratoře	laboratoř	k1gFnSc2	laboratoř
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Los	los	k1gInSc1	los
Alamos	Alamosa	k1gFnPc2	Alamosa
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
)	)	kIx)	)
Metallurgical	Metallurgical	k1gFnSc1	Metallurgical
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vývoj	vývoj	k1gInSc1	vývoj
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
(	(	kIx(	(
<g/>
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
současnosti	současnost	k1gFnPc4	současnost
Argonne	Argonn	k1gInSc5	Argonn
National	National	k1gFnSc1	National
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
)	)	kIx)	)
Projekt	projekt	k1gInSc1	projekt
Alberta	Albert	k1gMnSc2	Albert
(	(	kIx(	(
<g/>
Wendover	Wendover	k1gInSc1	Wendover
<g/>
,	,	kIx,	,
Utah	Utah	k1gInSc1	Utah
a	a	k8xC	a
Tinian	Tinian	k1gInSc1	Tinian
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
bojové	bojový	k2eAgNnSc4d1	bojové
použití	použití	k1gNnSc4	použití
atomových	atomový	k2eAgFnPc2d1	atomová
bomb	bomba	k1gFnPc2	bomba
Projekt	projekt	k1gInSc1	projekt
Ames	Ames	k1gInSc1	Ames
(	(	kIx(	(
<g/>
Ames	Ames	k1gInSc1	Ames
<g/>
,	,	kIx,	,
Iowa	Iowa	k1gFnSc1	Iowa
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
výroba	výroba	k1gFnSc1	výroba
surového	surový	k2eAgInSc2d1	surový
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Ames	Ames	k1gInSc1	Ames
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
)	)	kIx)	)
Dayton	Dayton	k1gInSc1	Dayton
<g />
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
(	(	kIx(	(
<g/>
Dayton	Dayton	k1gInSc1	Dayton
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
zušlechťování	zušlechťování	k1gNnSc2	zušlechťování
polonia	polonium	k1gNnSc2	polonium
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
pro	pro	k7c4	pro
roznětné	roznětný	k2eAgFnPc4d1	roznětná
atomové	atomový	k2eAgFnPc4d1	atomová
bomby	bomba	k1gFnPc4	bomba
Projekt	projekt	k1gInSc1	projekt
Camel	Camel	k1gInSc1	Camel
(	(	kIx(	(
<g/>
Inyokern	Inyokern	k1gInSc1	Inyokern
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
výzkum	výzkum	k1gInSc4	výzkum
výbušnin	výbušnina	k1gFnPc2	výbušnina
a	a	k8xC	a
nejaderné	jaderný	k2eNgNnSc1d1	nejaderné
inženýrství	inženýrství	k1gNnSc1	inženýrství
pro	pro	k7c4	pro
bombu	bomba	k1gFnSc4	bomba
Fat	fatum	k1gNnPc2	fatum
Man	Man	k1gMnSc1	Man
Projekt	projekt	k1gInSc4	projekt
Trinity	Trinita	k1gFnSc2	Trinita
(	(	kIx(	(
<g/>
Alamogordo	Alamogordo	k1gNnSc1	Alamogordo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
testování	testování	k1gNnSc4	testování
první	první	k4xOgFnSc2	první
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
Radiation	Radiation	k1gInSc1	Radiation
Laboratory	Laborator	k1gInPc1	Laborator
(	(	kIx(	(
<g/>
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
výzkum	výzkum	k1gInSc1	výzkum
obohacování	obohacování	k1gNnSc2	obohacování
elektromagnetickou	elektromagnetický	k2eAgFnSc7d1	elektromagnetická
separací	separace	k1gFnSc7	separace
(	(	kIx(	(
<g/>
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Lawrence	Lawrence	k1gFnSc2	Lawrence
Berkeley	Berkelea	k1gFnSc2	Berkelea
National	National	k1gFnSc2	National
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
)	)	kIx)	)
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
Groves	Groves	k1gMnSc1	Groves
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
Robertu	Robert	k1gMnSc3	Robert
Oppenheiemrovi	Oppenheiemr	k1gMnSc3	Oppenheiemr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ředitelem	ředitel	k1gMnSc7	ředitel
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Los	los	k1gInSc1	los
Alamos	Alamos	k1gInSc1	Alamos
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1943	[number]	k4	1943
a	a	k8xC	a
do	do	k7c2	do
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
zapojili	zapojit	k5eAaPmAgMnP	zapojit
i	i	k9	i
vědecký	vědecký	k2eAgMnSc1d1	vědecký
pracovníci	pracovník	k1gMnPc1	pracovník
z	z	k7c2	z
Princetonské	Princetonský	k2eAgFnSc2d1	Princetonská
<g/>
,	,	kIx,	,
Kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
<g/>
,	,	kIx,	,
Wisconsinské	wisconsinský	k2eAgFnSc2d1	Wisconsinská
a	a	k8xC	a
Minnesotské	minnesotský	k2eAgFnSc2d1	Minnesotská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Trinity	Trinita	k1gFnSc2	Trinita
(	(	kIx(	(
<g/>
jaderný	jaderný	k2eAgInSc4d1	jaderný
test	test	k1gInSc4	test
<g/>
)	)	kIx)	)
a	a	k8xC	a
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
atomové	atomový	k2eAgFnSc6d1	atomová
bombě	bomba	k1gFnSc6	bomba
pokročily	pokročit	k5eAaPmAgInP	pokročit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
vědci	vědec	k1gMnPc1	vědec
pracující	pracující	k1gMnPc1	pracující
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vývoji	vývoj	k1gInSc6	vývoj
byli	být	k5eAaImAgMnP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
bude	být	k5eAaImBp3nS	být
bomba	bomba	k1gFnSc1	bomba
hotová	hotový	k2eAgFnSc1d1	hotová
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
na	na	k7c4	na
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k9	už
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
si	se	k3xPyFc3	se
však	však	k9	však
mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
začali	začít	k5eAaPmAgMnP	začít
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
možná	možný	k2eAgNnPc1d1	možné
rizika	riziko	k1gNnPc1	riziko
reálného	reálný	k2eAgNnSc2d1	reálné
použití	použití	k1gNnSc2	použití
této	tento	k3xDgFnSc2	tento
zbraně	zbraň	k1gFnSc2	zbraň
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Szilárd	Szilárd	k6eAd1	Szilárd
ještě	ještě	k9	ještě
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1945	[number]	k4	1945
poslal	poslat	k5eAaPmAgInS	poslat
další	další	k2eAgInSc1d1	další
dopis	dopis	k1gInSc1	dopis
prezidentu	prezident	k1gMnSc3	prezident
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
hluboké	hluboký	k2eAgNnSc4d1	hluboké
znepokojení	znepokojení	k1gNnSc4	znepokojení
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
možným	možný	k2eAgNnSc7d1	možné
bombardováním	bombardování	k1gNnSc7	bombardování
japonských	japonský	k2eAgNnPc2d1	Japonské
měst	město	k1gNnPc2	město
atomovými	atomový	k2eAgFnPc7d1	atomová
bombami	bomba	k1gFnPc7	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Franklin	Franklina	k1gFnPc2	Franklina
D.	D.	kA	D.
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
však	však	k8xC	však
zemřel	zemřít	k5eAaPmAgMnS	zemřít
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
viceprezident	viceprezident	k1gMnSc1	viceprezident
Harry	Harra	k1gFnSc2	Harra
S.	S.	kA	S.
Truman	Truman	k1gMnSc1	Truman
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Projekt	projekt	k1gInSc1	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
byl	být	k5eAaImAgInS	být
přísně	přísně	k6eAd1	přísně
utajován	utajovat	k5eAaImNgInS	utajovat
<g/>
,	,	kIx,	,
Truman	Truman	k1gMnSc1	Truman
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zastával	zastávat	k5eAaImAgMnS	zastávat
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
ústavní	ústavní	k2eAgFnSc4d1	ústavní
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
vůbec	vůbec	k9	vůbec
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
zahájení	zahájení	k1gNnSc2	zahájení
Jaltské	jaltský	k2eAgFnSc2d1	Jaltská
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hutní	hutní	k2eAgFnSc6d1	hutní
laboratoři	laboratoř	k1gFnSc6	laboratoř
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
se	s	k7c7	s
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Franckem	Francek	k1gMnSc7	Francek
sešlo	sejít	k5eAaPmAgNnS	sejít
sedm	sedm	k4xCc1	sedm
fyziků	fyzik	k1gMnPc2	fyzik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
znění	znění	k1gNnSc6	znění
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nP	by
následně	následně	k6eAd1	následně
odeslali	odeslat	k5eAaPmAgMnP	odeslat
do	do	k7c2	do
Bílého	bílý	k2eAgInSc2d1	bílý
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
navrhnout	navrhnout	k5eAaPmF	navrhnout
možná	možný	k2eAgNnPc4d1	možné
řešení	řešení	k1gNnPc4	řešení
jak	jak	k8xC	jak
zabránit	zabránit	k5eAaPmF	zabránit
použití	použití	k1gNnSc4	použití
nové	nový	k2eAgFnSc2d1	nová
bomby	bomba	k1gFnSc2	bomba
proti	proti	k7c3	proti
Japonsku	Japonsko	k1gNnSc3	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Franck	Franck	k1gMnSc1	Franck
odeslal	odeslat	k5eAaPmAgMnS	odeslat
memorandum	memorandum	k1gNnSc1	memorandum
o	o	k7c6	o
"	"	kIx"	"
<g/>
společenských	společenský	k2eAgInPc6d1	společenský
a	a	k8xC	a
politických	politický	k2eAgInPc6d1	politický
důsledcích	důsledek	k1gInPc6	důsledek
atomové	atomový	k2eAgFnSc2d1	atomová
energie	energie	k1gFnSc2	energie
<g/>
"	"	kIx"	"
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
ministru	ministr	k1gMnSc3	ministr
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Memorandum	memorandum	k1gNnSc1	memorandum
bylo	být	k5eAaImAgNnS	být
projednáno	projednat	k5eAaPmNgNnS	projednat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
názor	názor	k1gInSc1	názor
vědců	vědec	k1gMnPc2	vědec
nebyl	být	k5eNaImAgInS	být
při	při	k7c6	při
dalších	další	k2eAgNnPc6d1	další
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
brán	brát	k5eAaImNgInS	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
jadernému	jaderný	k2eAgInSc3d1	jaderný
testu	test	k1gInSc3	test
vybrána	vybrán	k2eAgFnSc1d1	vybrána
opuštěná	opuštěný	k2eAgFnSc1d1	opuštěná
letecká	letecký	k2eAgFnSc1d1	letecká
základna	základna	k1gFnSc1	základna
Alamogordo	Alamogordo	k1gNnSc1	Alamogordo
v	v	k7c6	v
poušti	poušť	k1gFnSc6	poušť
White	Whit	k1gInSc5	Whit
Sands	Sands	k1gInSc4	Sands
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
kódové	kódový	k2eAgNnSc4d1	kódové
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
Trinity	Trinit	k2eAgMnPc4d1	Trinit
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Trojice	trojice	k1gFnSc1	trojice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
dopravena	dopraven	k2eAgFnSc1d1	dopravena
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
součást	součást	k1gFnSc1	součást
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
–	–	k?	–
plutoniová	plutoniový	k2eAgFnSc1d1	plutoniová
nálož	nálož	k1gFnSc1	nálož
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
Alamogorodské	Alamogorodský	k2eAgFnSc2d1	Alamogorodský
střelnice	střelnice	k1gFnSc2	střelnice
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
30	[number]	k4	30
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
vážící	vážící	k2eAgFnSc1d1	vážící
32	[number]	k4	32
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
rozmístěny	rozmístěn	k2eAgInPc4d1	rozmístěn
měřicí	měřicí	k2eAgInPc4d1	měřicí
přístroje	přístroj	k1gInPc4	přístroj
a	a	k8xC	a
devět	devět	k4xCc4	devět
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
sever	sever	k1gInSc4	sever
a	a	k8xC	a
východ	východ	k1gInSc4	východ
od	od	k7c2	od
věže	věž	k1gFnSc2	věž
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
umístěna	umístěn	k2eAgNnPc4d1	umístěno
pozorovací	pozorovací	k2eAgNnPc4d1	pozorovací
stanoviště	stanoviště	k1gNnPc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Velitelské	velitelský	k2eAgNnSc1d1	velitelské
stanoviště	stanoviště	k1gNnSc1	stanoviště
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
věže	věž	k1gFnSc2	věž
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
16	[number]	k4	16
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
30	[number]	k4	30
km	km	kA	km
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
základní	základní	k2eAgInSc1d1	základní
tábor	tábor	k1gInSc1	tábor
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
měli	mít	k5eAaImAgMnP	mít
výbuch	výbuch	k1gInSc4	výbuch
sledovat	sledovat	k5eAaImF	sledovat
vědci	vědec	k1gMnPc1	vědec
a	a	k8xC	a
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Přípravné	přípravný	k2eAgFnSc2d1	přípravná
práce	práce	k1gFnSc2	práce
trvaly	trvat	k5eAaImAgInP	trvat
dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
bomba	bomba	k1gFnSc1	bomba
připravena	připravit	k5eAaPmNgFnS	připravit
a	a	k8xC	a
vyzvednuta	vyzvednout	k5eAaPmNgFnS	vyzvednout
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
testovací	testovací	k2eAgFnSc2d1	testovací
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Zástupce	zástupce	k1gMnSc1	zástupce
armády	armáda	k1gFnSc2	armáda
podepsal	podepsat	k5eAaPmAgMnS	podepsat
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
kterým	který	k3yQgInPc3	který
byla	být	k5eAaImAgFnS	být
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
předána	předat	k5eAaPmNgFnS	předat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
vědců	vědec	k1gMnPc2	vědec
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Zkouška	zkouška	k1gFnSc1	zkouška
první	první	k4xOgFnSc2	první
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
v	v	k7c4	v
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Ohnivá	ohnivý	k2eAgFnSc1d1	ohnivá
koule	koule	k1gFnSc1	koule
způsobená	způsobený	k2eAgFnSc1d1	způsobená
výbuchem	výbuch	k1gInSc7	výbuch
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výšky	výška	k1gFnSc2	výška
1,5	[number]	k4	1,5
kilometru	kilometr	k1gInSc2	kilometr
a	a	k8xC	a
kouř	kouř	k1gInSc1	kouř
způsobený	způsobený	k2eAgInSc1d1	způsobený
výbuchem	výbuch	k1gInSc7	výbuch
vystoupal	vystoupat	k5eAaPmAgInS	vystoupat
až	až	k9	až
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
12	[number]	k4	12
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
výbuchu	výbuch	k1gInSc2	výbuch
byla	být	k5eAaImAgFnS	být
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
20	[number]	k4	20
000	[number]	k4	000
tunám	tuna	k1gFnPc3	tuna
trinitrotoluenu	trinitrotoluen	k1gInSc2	trinitrotoluen
<g/>
.	.	kIx.	.
</s>
<s>
Bomba	bomba	k1gFnSc1	bomba
vyzkoušená	vyzkoušený	k2eAgFnSc1d1	vyzkoušená
v	v	k7c6	v
Alamogordu	Alamogordo	k1gNnSc6	Alamogordo
byla	být	k5eAaImAgFnS	být
vyrobena	vyrobit	k5eAaPmNgFnS	vyrobit
stejnou	stejný	k2eAgFnSc7d1	stejná
technologií	technologie	k1gFnSc7	technologie
jako	jako	k8xS	jako
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
Fat	fatum	k1gNnPc2	fatum
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
shozená	shozený	k2eAgFnSc1d1	shozená
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
na	na	k7c4	na
japonské	japonský	k2eAgNnSc4d1	Japonské
město	město	k1gNnSc4	město
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
součástí	součást	k1gFnSc7	součást
obou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgFnPc2	tento
bomb	bomba	k1gFnPc2	bomba
byl	být	k5eAaImAgMnS	být
izotop	izotop	k1gInSc4	izotop
plutonia	plutonium	k1gNnSc2	plutonium
239	[number]	k4	239
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
tohoto	tento	k3xDgInSc2	tento
izotopu	izotop	k1gInSc2	izotop
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
uran	uran	k1gInSc1	uran
238	[number]	k4	238
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
sám	sám	k3xTgInSc1	sám
nevhodný	vhodný	k2eNgInSc1d1	nevhodný
jako	jako	k8xS	jako
štěpný	štěpný	k2eAgInSc1d1	štěpný
izotop	izotop	k1gInSc1	izotop
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
štěpení	štěpení	k1gNnSc6	štěpení
<g/>
235	[number]	k4	235
<g/>
U	U	kA	U
vznikají	vznikat	k5eAaImIp3nP	vznikat
relativně	relativně	k6eAd1	relativně
pomalé	pomalý	k2eAgInPc1d1	pomalý
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
238U	[number]	k4	238U
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
dnech	den	k1gInPc6	den
se	s	k7c7	s
238U	[number]	k4	238U
promění	proměnit	k5eAaPmIp3nS	proměnit
na	na	k7c4	na
plutonium	plutonium	k1gNnSc4	plutonium
239	[number]	k4	239
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
výroby	výroba	k1gFnSc2	výroba
plutonia	plutonium	k1gNnSc2	plutonium
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
Glennem	Glenn	k1gMnSc7	Glenn
Seaborgem	Seaborg	k1gMnSc7	Seaborg
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
<g/>
.	.	kIx.	.
</s>
<s>
Bomba	bomba	k1gFnSc1	bomba
Little	Little	k1gFnSc2	Little
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
shozená	shozený	k2eAgFnSc1d1	shozená
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
na	na	k7c4	na
Hirošimu	Hirošima	k1gFnSc4	Hirošima
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
uranu	uran	k1gInSc2	uran
235	[number]	k4	235
<g/>
U.	U.	kA	U.
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vzácný	vzácný	k2eAgInSc4d1	vzácný
izotop	izotop	k1gInSc4	izotop
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
oddělován	oddělovat	k5eAaImNgInS	oddělovat
od	od	k7c2	od
uranu	uran	k1gInSc2	uran
238	[number]	k4	238
<g/>
U	U	kA	U
<g/>
,	,	kIx,	,
nevhodného	vhodný	k2eNgInSc2d1	nevhodný
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
výbušných	výbušný	k2eAgNnPc6d1	výbušné
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Metodu	metoda	k1gFnSc4	metoda
separace	separace	k1gFnSc2	separace
235U	[number]	k4	235U
byla	být	k5eAaImAgFnS	být
vypracována	vypracován	k2eAgFnSc1d1	vypracována
Franzem	Franz	k1gMnSc7	Franz
Simonem	Simon	k1gMnSc7	Simon
a	a	k8xC	a
Nicholasem	Nicholas	k1gMnSc7	Nicholas
Kurtim	Kurtima	k1gFnPc2	Kurtima
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc4d1	jiná
metodu	metoda	k1gFnSc4	metoda
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
Ernest	Ernest	k1gMnSc1	Ernest
Lawrence	Lawrenec	k1gInSc2	Lawrenec
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
v	v	k7c6	v
Oak	Oak	k1gFnSc6	Oak
Ridge	Ridg	k1gInSc2	Ridg
<g/>
.	.	kIx.	.
</s>
<s>
Uranová	uranový	k2eAgFnSc1d1	uranová
bomba	bomba	k1gFnSc1	bomba
nebyla	být	k5eNaImAgFnS	být
předem	předem	k6eAd1	předem
otestována	otestován	k2eAgFnSc1d1	otestována
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přímo	přímo	k6eAd1	přímo
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
bombardování	bombardování	k1gNnSc3	bombardování
města	město	k1gNnSc2	město
Hirošima	Hirošima	k1gFnSc1	Hirošima
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
výrobě	výroba	k1gFnSc6	výroba
bylo	být	k5eAaImAgNnS	být
spotřebováno	spotřebovat	k5eAaPmNgNnS	spotřebovat
celé	celý	k2eAgNnSc1d1	celé
množství	množství	k1gNnSc1	množství
tehdy	tehdy	k6eAd1	tehdy
dostupného	dostupný	k2eAgInSc2d1	dostupný
čistého	čistý	k2eAgInSc2d1	čistý
235	[number]	k4	235
U	U	kA	U
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
test	test	k1gInSc1	test
by	by	kYmCp3nS	by
ani	ani	k9	ani
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
uskutečněn	uskutečnit	k5eAaPmNgInS	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Atomové	atomový	k2eAgNnSc1d1	atomové
bombardování	bombardování	k1gNnSc1	bombardování
Hirošimy	Hirošima	k1gFnSc2	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc2	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Poznatky	poznatek	k1gInPc1	poznatek
získané	získaný	k2eAgInPc1d1	získaný
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
posloužily	posloužit	k5eAaPmAgInP	posloužit
k	k	k7c3	k
sestrojení	sestrojení	k1gNnSc3	sestrojení
pum	puma	k1gFnPc2	puma
Fat	fatum	k1gNnPc2	fatum
Man	Man	k1gMnSc1	Man
a	a	k8xC	a
Little	Little	k1gFnSc1	Little
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
byly	být	k5eAaImAgFnP	být
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
japonská	japonský	k2eAgNnPc4d1	Japonské
města	město	k1gNnPc4	město
Hirošimu	Hirošima	k1gFnSc4	Hirošima
a	a	k8xC	a
Nagasaki	Nagasaki	k1gNnSc4	Nagasaki
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
shození	shození	k1gNnSc1	shození
prvních	první	k4xOgFnPc2	první
jaderných	jaderný	k2eAgFnPc2d1	jaderná
pum	puma	k1gFnPc2	puma
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vojenská	vojenský	k2eAgFnSc1d1	vojenská
skupina	skupina	k1gFnSc1	skupina
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
509	[number]	k4	509
<g/>
.	.	kIx.	.
</s>
<s>
Smíšená	smíšený	k2eAgFnSc1d1	smíšená
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
velitelem	velitel	k1gMnSc7	velitel
byl	být	k5eAaImAgInS	být
plk.	plk.	kA	plk.
Paul	Paula	k1gFnPc2	Paula
W.	W.	kA	W.
Tibbets	Tibbets	k1gInSc1	Tibbets
<g/>
.	.	kIx.	.
</s>
<s>
Vytrénoval	vytrénovat	k5eAaPmAgMnS	vytrénovat
několik	několik	k4yIc4	několik
posádek	posádka	k1gFnPc2	posádka
letadel	letadlo	k1gNnPc2	letadlo
B-	B-	k1gMnPc2	B-
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Posádky	posádka	k1gFnPc1	posádka
byly	být	k5eAaImAgFnP	být
nejprve	nejprve	k6eAd1	nejprve
pod	pod	k7c7	pod
vysokou	vysoký	k2eAgFnSc7d1	vysoká
bezpečnostní	bezpečnostní	k2eAgFnSc7d1	bezpečnostní
ochranou	ochrana	k1gFnSc7	ochrana
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
ve	v	k7c6	v
Wendoveru	Wendover	k1gInSc6	Wendover
poblíž	poblíž	k6eAd1	poblíž
Los	los	k1gMnSc1	los
Alamos	Alamos	k1gMnSc1	Alamos
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
dopisy	dopis	k1gInPc1	dopis
podléhaly	podléhat	k5eAaImAgInP	podléhat
cenzuře	cenzura	k1gFnSc3	cenzura
<g/>
,	,	kIx,	,
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
něco	něco	k3yInSc4	něco
prozradil	prozradit	k5eAaPmAgMnS	prozradit
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
či	či	k8xC	či
jejím	její	k3xOp3gNnSc6	její
poslání	poslání	k1gNnSc6	poslání
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
šel	jít	k5eAaImAgMnS	jít
bez	bez	k7c2	bez
milosti	milost	k1gFnSc2	milost
před	před	k7c4	před
vojenský	vojenský	k2eAgInSc4d1	vojenský
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
posádky	posádka	k1gFnPc1	posádka
přesunuly	přesunout	k5eAaPmAgFnP	přesunout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Tinian	Tiniany	k1gInPc2	Tiniany
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobývaly	pobývat	k5eAaImAgFnP	pobývat
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
North	North	k1gMnSc1	North
Field	Field	k1gMnSc1	Field
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
největší	veliký	k2eAgFnSc3d3	veliký
americké	americký	k2eAgFnSc3d1	americká
letecké	letecký	k2eAgFnSc3d1	letecká
základně	základna	k1gFnSc3	základna
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
ve	v	k7c4	v
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
opustil	opustit	k5eAaPmAgMnS	opustit
bombardér	bombardér	k1gMnSc1	bombardér
B-29	B-29	k1gMnSc1	B-29
Enola	Enola	k1gMnSc1	Enola
Gay	gay	k1gMnSc1	gay
ostrov	ostrov	k1gInSc4	ostrov
Tinian	Tinian	k1gMnSc1	Tinian
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
letouny	letoun	k1gInPc7	letoun
(	(	kIx(	(
<g/>
Great	Great	k1gInSc1	Great
Artiste	Artist	k1gMnSc5	Artist
a	a	k8xC	a
No	no	k9	no
<g/>
.	.	kIx.	.
91	[number]	k4	91
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
14	[number]	k4	14
minut	minuta	k1gFnPc2	minuta
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
letouny	letoun	k1gInPc1	letoun
Hirošimy	Hirošima	k1gFnSc2	Hirošima
a	a	k8xC	a
o	o	k7c4	o
2	[number]	k4	2
minuty	minuta	k1gFnSc2	minuta
později	pozdě	k6eAd2	pozdě
Paul	Paul	k1gMnSc1	Paul
Tibbets	Tibbetsa	k1gFnPc2	Tibbetsa
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
posádkou	posádka	k1gFnSc7	posádka
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
světě	svět	k1gInSc6	svět
svrhli	svrhnout	k5eAaPmAgMnP	svrhnout
atomovou	atomový	k2eAgFnSc4d1	atomová
pumu	puma	k1gFnSc4	puma
<g/>
.	.	kIx.	.
</s>
<s>
Radista	radista	k1gMnSc1	radista
Nelson	Nelson	k1gMnSc1	Nelson
poslal	poslat	k5eAaPmAgMnS	poslat
následně	následně	k6eAd1	následně
telegram	telegram	k1gInSc4	telegram
pro	pro	k7c4	pro
velení	velení	k1gNnSc4	velení
509	[number]	k4	509
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
</s>
