<p>
<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
VZP	VZP	kA	VZP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
veřejné	veřejný	k2eAgNnSc4d1	veřejné
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zřizována	zřizovat	k5eAaImNgFnS	zřizovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
551	[number]	k4	551
<g/>
/	/	kIx~	/
<g/>
1991	[number]	k4	1991
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
vede	vést	k5eAaImIp3nS	vést
i	i	k9	i
registr	registr	k1gInSc1	registr
pojištěnců	pojištěnec	k1gMnPc2	pojištěnec
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
pojištění	pojištění	k1gNnSc2	pojištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VZP	VZP	kA	VZP
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
klientů	klient	k1gMnPc2	klient
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Asociace	asociace	k1gFnSc2	asociace
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
neziskových	ziskový	k2eNgFnPc2d1	nezisková
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
a	a	k8xC	a
nemocenských	nemocenský	k2eAgFnPc2d1	nemocenská
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
(	(	kIx(	(
<g/>
fran	frana	k1gFnPc2	frana
<g/>
.	.	kIx.	.
</s>
<s>
Association	Association	k1gInSc1	Association
Internationale	Internationale	k1gFnPc2	Internationale
de	de	k?	de
la	la	k1gNnSc4	la
Mutualité	Mutualitý	k2eAgFnSc2d1	Mutualitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kód	kód	k1gInSc1	kód
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
je	být	k5eAaImIp3nS	být
111	[number]	k4	111
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Zdravotní	zdravotní	k2eAgNnSc1d1	zdravotní
pojištění	pojištění	k1gNnSc1	pojištění
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
