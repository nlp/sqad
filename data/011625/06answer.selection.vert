<s>
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
zkratkou	zkratka	k1gFnSc7	zkratka
VZP	VZP	kA	VZP
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
veřejné	veřejný	k2eAgNnSc4d1	veřejné
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zřizována	zřizovat	k5eAaImNgFnS	zřizovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
