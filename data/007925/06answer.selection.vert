<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
krvinka	krvinka	k1gFnSc1	krvinka
či	či	k8xC	či
leukocyt	leukocyt	k1gInSc1	leukocyt
je	být	k5eAaImIp3nS	být
krevní	krevní	k2eAgFnSc1d1	krevní
buňka	buňka	k1gFnSc1	buňka
mnohých	mnohý	k2eAgMnPc2d1	mnohý
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
fungování	fungování	k1gNnSc4	fungování
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
