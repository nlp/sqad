<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k6eAd1	také
Siniristilippu	Siniristilipp	k1gInSc3	Siniristilipp
(	(	kIx(	(
<g/>
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
křížem	kříž	k1gInSc7	kříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
bílého	bílý	k2eAgInSc2d1	bílý
obdélníkového	obdélníkový	k2eAgInSc2d1	obdélníkový
listu	list	k1gInSc2	list
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
