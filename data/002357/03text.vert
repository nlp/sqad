<s>
Bičování	bičování	k1gNnSc1	bičování
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
mrskání	mrskání	k1gNnSc1	mrskání
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
trest	trest	k1gInSc1	trest
<g/>
,	,	kIx,	,
široce	široko	k6eAd1	široko
používaný	používaný	k2eAgInSc1d1	používaný
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
židé	žid	k1gMnPc1	žid
odsuzovali	odsuzovat	k5eAaImAgMnP	odsuzovat
k	k	k7c3	k
bičování	bičování	k1gNnSc3	bičování
za	za	k7c4	za
trest	trest	k1gInSc4	trest
při	při	k7c6	při
porušení	porušení	k1gNnSc6	porušení
obřadních	obřadní	k2eAgInPc2d1	obřadní
předpisů	předpis	k1gInPc2	předpis
či	či	k8xC	či
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
proti	proti	k7c3	proti
mravnosti	mravnost	k1gFnSc3	mravnost
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzený	odsouzený	k1gMnSc1	odsouzený
mohl	moct	k5eAaImAgMnS	moct
dostat	dostat	k5eAaPmF	dostat
maximálně	maximálně	k6eAd1	maximálně
40	[number]	k4	40
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
trest	trest	k1gInSc1	trest
nebyl	být	k5eNaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
společensky	společensky	k6eAd1	společensky
potupný	potupný	k2eAgInSc4d1	potupný
a	a	k8xC	a
ponižující	ponižující	k2eAgInSc4d1	ponižující
<g/>
.	.	kIx.	.
</s>
<s>
Bičování	bičování	k1gNnSc1	bičování
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
v	v	k7c6	v
synagoze	synagoga	k1gFnSc6	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bičování	bičování	k1gNnSc2	bičování
byly	být	k5eAaImAgFnP	být
trestanému	trestaný	k2eAgInSc3d1	trestaný
předčítány	předčítán	k2eAgFnPc1d1	předčítána
texty	text	k1gInPc4	text
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc4d1	samotný
průběh	průběh	k1gInSc4	průběh
bičování	bičování	k1gNnSc2	bičování
sledoval	sledovat	k5eAaImAgMnS	sledovat
vždy	vždy	k6eAd1	vždy
soudce	soudce	k1gMnSc1	soudce
<g/>
,	,	kIx,	,
bičovaný	bičovaný	k2eAgMnSc1d1	bičovaný
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
používali	používat	k5eAaImAgMnP	používat
hůl	hůl	k1gFnSc4	hůl
či	či	k8xC	či
prut	prut	k1gInSc4	prut
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
bič	bič	k1gInSc1	bič
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
řemeny	řemen	k1gInPc4	řemen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
dostával	dostávat	k5eAaImAgMnS	dostávat
maximálně	maximálně	k6eAd1	maximálně
třináct	třináct	k4xCc4	třináct
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Bičování	bičování	k1gNnSc1	bičování
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
formách	forma	k1gFnPc6	forma
<g/>
,	,	kIx,	,
novými	nový	k2eAgInPc7d1	nový
nástroji	nástroj	k1gInPc7	nástroj
jako	jako	k8xS	jako
devítiocasá	devítiocasý	k2eAgFnSc1d1	devítiocasá
kočka	kočka	k1gFnSc1	kočka
nebo	nebo	k8xC	nebo
tradiční	tradiční	k2eAgInSc1d1	tradiční
bič	bič	k1gInSc1	bič
<g/>
.	.	kIx.	.
</s>
<s>
Bičování	bičování	k1gNnSc1	bičování
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k8xS	jako
trest	trest	k1gInSc1	trest
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
neznamená	znamenat	k5eNaImIp3nS	znamenat
mrzačení	mrzačení	k1gNnSc1	mrzačení
(	(	kIx(	(
<g/>
nenávratné	návratný	k2eNgNnSc1d1	nenávratné
poškozování	poškozování	k1gNnSc1	poškozování
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
trest	trest	k1gInSc1	trest
méně	málo	k6eAd2	málo
nepříjemný	příjemný	k2eNgInSc1d1	nepříjemný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
u	u	k7c2	u
biče	bič	k1gInSc2	bič
člověk	člověk	k1gMnSc1	člověk
mohl	moct	k5eAaImAgMnS	moct
dostat	dostat	k5eAaPmF	dostat
39	[number]	k4	39
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
hluboce	hluboko	k6eAd1	hluboko
a	a	k8xC	a
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
formy	forma	k1gFnPc1	forma
byly	být	k5eAaImAgFnP	být
potom	potom	k6eAd1	potom
takzvané	takzvaný	k2eAgInPc1d1	takzvaný
plané	planý	k2eAgInPc1d1	planý
biče	bič	k1gInPc1	bič
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
měly	mít	k5eAaImAgInP	mít
na	na	k7c6	na
konci	konec	k1gInSc6	konec
veliké	veliký	k2eAgInPc4d1	veliký
ostré	ostrý	k2eAgInPc4d1	ostrý
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
podle	podle	k7c2	podle
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
možné	možný	k2eAgNnSc1d1	možné
člověka	člověk	k1gMnSc2	člověk
odsoudit	odsoudit	k5eAaPmF	odsoudit
maximálně	maximálně	k6eAd1	maximálně
ke	k	k7c3	k
40	[number]	k4	40
ranám	rána	k1gFnPc3	rána
bičem	bič	k1gInSc7	bič
<g/>
.	.	kIx.	.
</s>
<s>
Bičování	bičování	k1gNnSc1	bičování
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
používáno	používán	k2eAgNnSc1d1	používáno
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c6	v
Bruneji	Brunej	k1gInSc6	Brunej
<g/>
,	,	kIx,	,
Malajsii	Malajsie	k1gFnSc6	Malajsie
<g/>
,	,	kIx,	,
Singapuru	Singapur	k1gInSc6	Singapur
<g/>
,	,	kIx,	,
Tanzánii	Tanzánie	k1gFnSc6	Tanzánie
<g/>
,	,	kIx,	,
Zimbabwe	Zimbabwe	k1gFnSc6	Zimbabwe
a	a	k8xC	a
řadě	řada	k1gFnSc6	řada
islámských	islámský	k2eAgFnPc2d1	islámská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
udělit	udělit	k5eAaPmF	udělit
trest	trest	k1gInSc4	trest
bičování	bičování	k1gNnSc2	bičování
například	například	k6eAd1	například
za	za	k7c4	za
nelegální	legální	k2eNgNnSc4d1	nelegální
graffiti	graffiti	k1gNnSc4	graffiti
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgInSc1d1	maximální
počet	počet	k1gInSc1	počet
ran	rána	k1gFnPc2	rána
je	být	k5eAaImIp3nS	být
36	[number]	k4	36
<g/>
,	,	kIx,	,
odsouzený	odsouzený	k1gMnSc1	odsouzený
si	se	k3xPyFc3	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vyměnit	vyměnit	k5eAaPmF	vyměnit
jednu	jeden	k4xCgFnSc4	jeden
ránu	rána	k1gFnSc4	rána
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
trestu	trest	k1gInSc2	trest
je	být	k5eAaImIp3nS	být
odsouzený	odsouzený	k2eAgMnSc1d1	odsouzený
připoután	připoutat	k5eAaPmNgMnS	připoutat
na	na	k7c6	na
lavici	lavice	k1gFnSc6	lavice
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
kat	kat	k1gInSc1	kat
musí	muset	k5eAaImIp3nS	muset
trefit	trefit	k5eAaPmF	trefit
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
vodící	vodící	k2eAgInPc4d1	vodící
plechy	plech	k1gInPc4	plech
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
ráně	rána	k1gFnSc6	rána
posunuty	posunout	k5eAaPmNgInP	posunout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
další	další	k2eAgFnSc1d1	další
rána	rána	k1gFnSc1	rána
byla	být	k5eAaImAgFnS	být
mířena	mířen	k2eAgFnSc1d1	mířena
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
části	část	k1gFnSc2	část
zad	záda	k1gNnPc2	záda
<g/>
.	.	kIx.	.
</s>
<s>
Výkonu	výkon	k1gInSc3	výkon
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
může	moct	k5eAaImIp3nS	moct
exekuci	exekuce	k1gFnSc4	exekuce
přerušit	přerušit	k5eAaPmF	přerušit
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
problémů	problém	k1gInPc2	problém
odsouzeného	odsouzený	k1gMnSc2	odsouzený
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
při	při	k7c6	při
vědomí	vědomí	k1gNnSc6	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Rány	Rána	k1gFnPc1	Rána
nezpůsobí	způsobit	k5eNaPmIp3nP	způsobit
protržení	protržení	k1gNnSc4	protržení
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
,	,	kIx,	,
vyvolají	vyvolat	k5eAaPmIp3nP	vyvolat
pouze	pouze	k6eAd1	pouze
podlitiny	podlitina	k1gFnPc1	podlitina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nP	léčit
od	od	k7c2	od
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
do	do	k7c2	do
cca	cca	kA	cca
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bičování	bičování	k1gNnSc4	bičování
je	být	k5eAaImIp3nS	být
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
trestu	trest	k1gInSc2	trest
používaných	používaný	k2eAgFnPc2d1	používaná
podle	podle	k7c2	podle
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
šaría	šaríum	k1gNnSc2	šaríum
za	za	k7c4	za
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
jako	jako	k8xC	jako
smilstvo	smilstvo	k1gNnSc4	smilstvo
<g/>
,	,	kIx,	,
užívání	užívání	k1gNnSc4	užívání
alkoholu	alkohol	k1gInSc2	alkohol
nebo	nebo	k8xC	nebo
za	za	k7c4	za
pomluvu	pomluva	k1gFnSc4	pomluva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
jako	jako	k8xC	jako
volný	volný	k2eAgInSc1d1	volný
trest	trest	k1gInSc1	trest
přo	přo	k?	přo
řadu	řad	k1gInSc2	řad
jiných	jiný	k2eAgInPc2d1	jiný
přestupků	přestupek	k1gInPc2	přestupek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
byly	být	k5eAaImAgFnP	být
nevěrnice	nevěrnice	k1gFnPc1	nevěrnice
původně	původně	k6eAd1	původně
jen	jen	k9	jen
bičovány	bičován	k2eAgFnPc1d1	bičována
-	-	kIx~	-
kamenování	kamenování	k1gNnSc6	kamenování
převzali	převzít	k5eAaPmAgMnP	převzít
muslimové	muslim	k1gMnPc1	muslim
od	od	k7c2	od
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k6eAd1	ještě
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
bežným	bežné	k1gNnSc7	bežné
tresten	tresten	k2eAgInSc4d1	tresten
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
i	i	k9	i
za	za	k7c4	za
lehčí	lehký	k2eAgInPc4d2	lehčí
za	za	k7c4	za
mravnostní	mravnostní	k2eAgInPc4d1	mravnostní
delikty	delikt	k1gInPc4	delikt
40	[number]	k4	40
ran	rána	k1gFnPc2	rána
bičem	bič	k1gInSc7	bič
(	(	kIx(	(
<g/>
např.	např.	kA	např.
za	za	k7c4	za
nošení	nošení	k1gNnSc4	nošení
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
ke	k	k7c3	k
12	[number]	k4	12
měsícům	měsíc	k1gInPc3	měsíc
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
tři	tři	k4xCgNnPc1	tři
sta	sto	k4xCgNnPc1	sto
padesáti	padesát	k4xCc7	padesát
ranám	rána	k1gFnPc3	rána
bičem	bič	k1gInSc7	bič
za	za	k7c4	za
nelegální	legální	k2eNgNnSc4d1	nelegální
přechovávání	přechovávání	k1gNnSc4	přechovávání
několika	několik	k4yIc2	několik
lahví	lahev	k1gFnPc2	lahev
vína	víno	k1gNnSc2	víno
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
odsoudit	odsoudit	k5eAaPmF	odsoudit
mužského	mužský	k2eAgMnSc4d1	mužský
viníka	viník	k1gMnSc4	viník
v	v	k7c6	v
nábožensky	nábožensky	k6eAd1	nábožensky
rigidní	rigidní	k2eAgFnSc6d1	rigidní
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Bičování	bičování	k1gNnSc1	bičování
se	se	k3xPyFc4	se
též	též	k9	též
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
forem	forma	k1gFnPc2	forma
spankingu	spanking	k1gInSc2	spanking
<g/>
,	,	kIx,	,
sadomasochistické	sadomasochistický	k2eAgFnPc1d1	sadomasochistická
sexuální	sexuální	k2eAgFnPc1d1	sexuální
praktiky	praktika	k1gFnPc1	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Knuta	knuta	k1gFnSc1	knuta
Důtky	důtka	k1gFnSc2	důtka
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bičování	bičování	k1gNnSc2	bičování
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Bičování	bičování	k1gNnSc2	bičování
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
Súdánu	Súdán	k1gInSc6	Súdán
</s>
