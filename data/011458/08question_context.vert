<s>
Martin	Martin	k1gMnSc1	Martin
Van	vana	k1gFnPc2	vana
Buren	Burna	k1gFnPc2	Burna
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1782	[number]	k4	1782
Kinderhook	Kinderhook	k1gInSc1	Kinderhook
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
New	New	k1gFnSc7	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1862	[number]	k4	1862
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Starý	starý	k2eAgInSc4d1	starý
Kinderhook	Kinderhook	k1gInSc4	Kinderhook
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
osmým	osmý	k4xOgMnSc7	osmý
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
klíčový	klíčový	k2eAgInSc1d1	klíčový
organizátor	organizátor	k1gInSc1	organizátor
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
anglické	anglický	k2eAgMnPc4d1	anglický
<g/>
,	,	kIx,	,
irské	irský	k2eAgMnPc4d1	irský
nebo	nebo	k8xC	nebo
skotské	skotský	k2eAgMnPc4d1	skotský
předky	předek	k1gMnPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
mateřštinou	mateřština	k1gFnSc7	mateřština
nebyla	být	k5eNaImAgFnS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
rodný	rodný	k2eAgInSc4d1	rodný
jazyk	jazyk	k1gInSc4	jazyk
používal	používat	k5eAaImAgInS	používat
nizozemštinu	nizozemština	k1gFnSc4	nizozemština
<g/>
.	.	kIx.	.
</s>
