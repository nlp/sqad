<s>
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
jediný	jediný	k2eAgMnSc1d1	jediný
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
materštinou	materština	k1gFnSc7	materština
nebyla	být	k5eNaImAgFnS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
?	?	kIx.	?
</s>
