<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Van	vana	k1gFnPc2	vana
Buren	Burna	k1gFnPc2	Burna
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1782	[number]	k4	1782
Kinderhook	Kinderhook	k1gInSc1	Kinderhook
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
New	New	k1gFnSc7	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1862	[number]	k4	1862
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Starý	starý	k2eAgInSc4d1	starý
Kinderhook	Kinderhook	k1gInSc4	Kinderhook
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
osmým	osmý	k4xOgMnSc7	osmý
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
klíčový	klíčový	k2eAgInSc1d1	klíčový
organizátor	organizátor	k1gInSc1	organizátor
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
první	první	k4xOgMnSc1	první
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
neměl	mít	k5eNaImAgMnS	mít
anglické	anglický	k2eAgMnPc4d1	anglický
<g/>
,	,	kIx,	,
irské	irský	k2eAgMnPc4d1	irský
nebo	nebo	k8xC	nebo
skotské	skotský	k2eAgMnPc4d1	skotský
předky	předek	k1gMnPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
mateřštinou	mateřština	k1gFnSc7	mateřština
nebyla	být	k5eNaImAgFnS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jako	jako	k9	jako
svůj	svůj	k3xOyFgInSc4	svůj
rodný	rodný	k2eAgInSc4d1	rodný
jazyk	jazyk	k1gInSc4	jazyk
používal	používat	k5eAaImAgInS	používat
nizozemštinu	nizozemština	k1gFnSc4	nizozemština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1832	[number]	k4	1832
<g/>
–	–	k?	–
<g/>
1836	[number]	k4	1836
byl	být	k5eAaImAgInS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
viceprezidentem	viceprezident	k1gMnSc7	viceprezident
USA	USA	kA	USA
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Andrewa	Andrewus	k1gMnSc2	Andrewus
Jacksona	Jackson	k1gMnSc2	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1836	[number]	k4	1836
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
viceprezident	viceprezident	k1gMnSc1	viceprezident
po	po	k7c6	po
schválení	schválení	k1gNnSc6	schválení
12	[number]	k4	12
<g/>
.	.	kIx.	.
dodatku	dodatek	k1gInSc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
8	[number]	k4	8
<g/>
.	.	kIx.	.
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
osmi	osm	k4xCc2	osm
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
maximálně	maximálně	k6eAd1	maximálně
jedno	jeden	k4xCgNnSc1	jeden
volební	volební	k2eAgNnSc1d1	volební
období	období	k1gNnSc1	období
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
vláda	vláda	k1gFnSc1	vláda
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
Andrewem	Andrew	k1gMnSc7	Andrew
Jacksonem	Jackson	k1gMnSc7	Jackson
a	a	k8xC	a
Abrahamem	Abraham	k1gMnSc7	Abraham
Lincolnem	Lincoln	k1gMnSc7	Lincoln
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
o	o	k7c4	o
vývoj	vývoj	k1gInSc4	vývoj
moderních	moderní	k2eAgFnPc2d1	moderní
politických	politický	k2eAgFnPc2d1	politická
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Coby	Coby	k?	Coby
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
pod	pod	k7c7	pod
Andrewem	Andrew	k1gMnSc7	Andrew
Jacksonem	Jackson	k1gMnSc7	Jackson
a	a	k8xC	a
pak	pak	k6eAd1	pak
viceprezident	viceprezident	k1gMnSc1	viceprezident
byl	být	k5eAaImAgMnS	být
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
organizační	organizační	k2eAgFnSc2d1	organizační
struktury	struktura	k1gFnSc2	struktura
pro	pro	k7c4	pro
jacksoniánskou	jacksoniánský	k2eAgFnSc4d1	jacksoniánský
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
výkonu	výkon	k1gInSc2	výkon
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
potýkala	potýkat	k5eAaImAgFnS	potýkat
s	s	k7c7	s
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
potížemi	potíž	k1gFnPc7	potíž
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
krizí	krize	k1gFnSc7	krize
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Panic	panic	k1gMnSc1	panic
of	of	k?	of
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nekrvavou	krvavý	k2eNgFnSc7d1	nekrvavá
válkou	válka	k1gFnSc7	válka
"	"	kIx"	"
<g/>
Aroostook	Aroostook	k1gInSc1	Aroostook
War	War	k1gFnSc2	War
<g/>
"	"	kIx"	"
a	a	k8xC	a
aférou	aféra	k1gFnSc7	aféra
"	"	kIx"	"
<g/>
Caroline	Carolin	k1gInSc5	Carolin
Affair	Affair	k1gInSc4	Affair
<g/>
"	"	kIx"	"
byly	být	k5eAaImAgInP	být
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
jejími	její	k3xOp3gFnPc7	její
koloniemi	kolonie	k1gFnPc7	kolonie
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
napjaté	napjatý	k2eAgNnSc1d1	napjaté
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
Van	van	k1gInSc4	van
Burenova	Burenův	k2eAgFnSc1d1	Burenův
vina	vina	k1gFnSc1	vina
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
,	,	kIx,	,
po	po	k7c6	po
čtyřech	čtyři	k4xCgNnPc6	čtyři
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
drtivou	drtivý	k2eAgFnSc7d1	drtivá
většinou	většina	k1gFnSc7	většina
volitelů	volitel	k1gMnPc2	volitel
a	a	k8xC	a
nezískal	získat	k5eNaPmAgInS	získat
ani	ani	k9	ani
absolutní	absolutní	k2eAgFnSc4d1	absolutní
většinu	většina	k1gFnSc4	většina
hlasů	hlas	k1gInPc2	hlas
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
46,9	[number]	k4	46,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
jako	jako	k9	jako
kandidát	kandidát	k1gMnSc1	kandidát
menšinové	menšinový	k2eAgFnSc2d1	menšinová
Strany	strana	k1gFnSc2	strana
svobodné	svobodný	k2eAgFnSc2d1	svobodná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgInS	získat
však	však	k9	však
podporu	podpora	k1gFnSc4	podpora
jen	jen	k9	jen
10,1	[number]	k4	10,1
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1862	[number]	k4	1862
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
Van	van	k1gInSc1	van
Burena	Burena	k1gFnSc1	Burena
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Martin	Martin	k2eAgInSc4d1	Martin
Van	van	k1gInSc4	van
Buren	Burna	k1gFnPc2	Burna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bílý	bílý	k2eAgInSc1d1	bílý
dům	dům	k1gInSc1	dům
–	–	k?	–
Martin	Martin	k2eAgInSc1d1	Martin
Van	van	k1gInSc1	van
Buren	Buren	k2eAgInSc1d1	Buren
</s>
</p>
