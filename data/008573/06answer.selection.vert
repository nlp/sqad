<s>
Hra	hra	k1gFnSc1
pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
středověké	středověký	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
doložena	doložit	k5eAaPmNgFnS
k	k	k7c3
roku	rok	k1gInSc3
1275	[number]	k4
pod	pod	k7c7
názvem	název	k1gInSc7
jeu	jeu	k?
de	de	k?
la	la	k1gNnSc2
chasse	chasse	k1gFnSc1
(	(	kIx(
<g/>
hra	hra	k1gFnSc1
na	na	k7c4
lov	lov	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
napodobovala	napodobovat	k5eAaImAgFnS
chytání	chytání	k1gNnSc4
ptáků	pták	k1gMnPc2
do	do	k7c2
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
</s>