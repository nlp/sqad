<p>
<s>
Tenis	tenis	k1gInSc1	tenis
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
tennis	tennis	k1gInSc1	tennis
<	<	kIx(	<
angl.	angl.	k?	angl.
tenes	tenes	k1gMnSc1	tenes
<g/>
,	,	kIx,	,
tenetz	tenetz	k1gMnSc1	tenetz
<	<	kIx(	<
fr.	fr.	k?	fr.
tenez	tenez	k1gInSc1	tenez
<g/>
!	!	kIx.	!
</s>
<s>
=	=	kIx~	=
berte	brát	k5eAaImRp2nP	brát
<g/>
,	,	kIx,	,
držte	držet	k5eAaImRp2nP	držet
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
imperativ	imperativ	k1gInSc1	imperativ
pl.	pl.	k?	pl.
slova	slovo	k1gNnSc2	slovo
držet	držet	k5eAaImF	držet
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k8xS	jako
bílý	bílý	k2eAgInSc1d1	bílý
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
míčová	míčový	k2eAgFnSc1d1	Míčová
hra	hra	k1gFnSc1	hra
pro	pro	k7c4	pro
2	[number]	k4	2
nebo	nebo	k8xC	nebo
4	[number]	k4	4
hráče	hráč	k1gMnSc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Varianta	varianta	k1gFnSc1	varianta
se	s	k7c7	s
2	[number]	k4	2
hráči	hráč	k1gMnPc7	hráč
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
dvouhra	dvouhra	k1gFnSc1	dvouhra
<g/>
,	,	kIx,	,
varianta	varianta	k1gFnSc1	varianta
se	s	k7c7	s
4	[number]	k4	4
hráči	hráč	k1gMnPc7	hráč
pak	pak	k6eAd1	pak
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
se	se	k3xPyFc4	se
také	také	k9	také
smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
dvojici	dvojice	k1gFnSc6	dvojice
hraje	hrát	k5eAaImIp3nS	hrát
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soupeři	soupeř	k1gMnPc1	soupeř
stojí	stát	k5eAaImIp3nP	stát
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
na	na	k7c6	na
obdélníkovém	obdélníkový	k2eAgNnSc6d1	obdélníkové
hřišti	hřiště	k1gNnSc6	hřiště
(	(	kIx(	(
<g/>
tenisovém	tenisový	k2eAgInSc6d1	tenisový
dvorci	dvorec	k1gInSc6	dvorec
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
se	se	k3xPyFc4	se
odrazit	odrazit	k5eAaPmF	odrazit
tenisový	tenisový	k2eAgInSc4d1	tenisový
míček	míček	k1gInSc4	míček
tenisovou	tenisový	k2eAgFnSc7d1	tenisová
raketou	raketa	k1gFnSc7	raketa
do	do	k7c2	do
pole	pole	k1gNnSc2	pole
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gNnSc4	on
soupeř	soupeř	k1gMnSc1	soupeř
nemohl	moct	k5eNaImAgMnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
míčkem	míček	k1gInSc7	míček
trefil	trefit	k5eAaPmAgMnS	trefit
vedle	vedle	k7c2	vedle
tenisového	tenisový	k2eAgInSc2d1	tenisový
dvorce	dvorec	k1gInSc2	dvorec
(	(	kIx(	(
<g/>
do	do	k7c2	do
autu	aut	k1gInSc2	aut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
středověké	středověký	k2eAgFnSc2d1	středověká
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1275	[number]	k4	1275
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
jeu	jeu	k?	jeu
de	de	k?	de
la	la	k1gNnSc2	la
chasse	chasse	k1gFnSc1	chasse
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
napodobovala	napodobovat	k5eAaImAgFnS	napodobovat
chytání	chytání	k1gNnSc4	chytání
ptáků	pták	k1gMnPc2	pták
do	do	k7c2	do
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Provozovali	provozovat	k5eAaImAgMnP	provozovat
ji	on	k3xPp3gFnSc4	on
zejména	zejména	k9	zejména
řeholníci	řeholník	k1gMnPc1	řeholník
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
urozených	urozený	k2eAgFnPc2d1	urozená
rodin	rodina	k1gFnPc2	rodina
jako	jako	k8xS	jako
náhradu	náhrada	k1gFnSc4	náhrada
rytířských	rytířský	k2eAgFnPc2d1	rytířská
zábav	zábava	k1gFnPc2	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Míčem	míč	k1gInSc7	míč
se	se	k3xPyFc4	se
trefovalo	trefovat	k5eAaImAgNnS	trefovat
do	do	k7c2	do
branky	branka	k1gFnSc2	branka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
hájila	hájit	k5eAaImAgFnS	hájit
služebná	služebný	k2eAgFnSc1d1	služebná
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
service	service	k1gFnSc1	service
side	sidat	k5eAaPmIp3nS	sidat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
podání	podání	k1gNnSc1	podání
slovem	slovem	k6eAd1	slovem
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
na	na	k7c6	na
klášterních	klášterní	k2eAgInPc6d1	klášterní
dvorech	dvůr	k1gInPc6	dvůr
(	(	kIx(	(
francouzsky	francouzsky	k6eAd1	francouzsky
cour	cour	k?	cour
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
označení	označení	k1gNnSc1	označení
tenisového	tenisový	k2eAgNnSc2d1	tenisové
hřiště	hřiště	k1gNnSc2	hřiště
jako	jako	k9	jako
dvorec	dvorec	k1gInSc4	dvorec
nebo	nebo	k8xC	nebo
kurt	kurt	k1gInSc4	kurt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
sport	sport	k1gInSc1	sport
provozoval	provozovat	k5eAaImAgInS	provozovat
u	u	k7c2	u
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc3	století
jsou	být	k5eAaImIp3nP	být
zmiňovány	zmiňovat	k5eAaImNgInP	zmiňovat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
první	první	k4xOgInPc1	první
zápasy	zápas	k1gInPc1	zápas
profesionálů	profesionál	k1gMnPc2	profesionál
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
výsledky	výsledek	k1gInPc4	výsledek
se	se	k3xPyFc4	se
sázely	sázet	k5eAaImAgFnP	sázet
značné	značný	k2eAgFnPc1d1	značná
částky	částka	k1gFnPc1	částka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hazardní	hazardní	k2eAgFnSc2d1	hazardní
hry	hra	k1gFnSc2	hra
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
dosud	dosud	k6eAd1	dosud
používané	používaný	k2eAgNnSc4d1	používané
bodování	bodování	k1gNnSc4	bodování
tenisu	tenis	k1gInSc2	tenis
<g/>
:	:	kIx,	:
za	za	k7c4	za
vyhraný	vyhraný	k2eAgInSc4d1	vyhraný
míč	míč	k1gInSc4	míč
se	se	k3xPyFc4	se
vyplácelo	vyplácet	k5eAaImAgNnS	vyplácet
patnáct	patnáct	k4xCc1	patnáct
sou	sou	k1gInPc2	sou
a	a	k8xC	a
cílem	cíl	k1gInSc7	cíl
hry	hra	k1gFnSc2	hra
bylo	být	k5eAaImAgNnS	být
získat	získat	k5eAaPmF	získat
jich	on	k3xPp3gNnPc2	on
šedesát	šedesát	k4xCc4	šedesát
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kopu	kopat	k5eAaImIp1nS	kopat
<g/>
.	.	kIx.	.
</s>
<s>
Počítalo	počítat	k5eAaImAgNnS	počítat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
45	[number]	k4	45
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
a	a	k8xC	a
celá	celý	k2eAgFnSc1d1	celá
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
měli	mít	k5eAaImAgMnP	mít
oba	dva	k4xCgMnPc1	dva
hráči	hráč	k1gMnPc1	hráč
45	[number]	k4	45
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
chyběly	chybět	k5eAaImAgInP	chybět
jim	on	k3xPp3gMnPc3	on
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
dva	dva	k4xCgInPc4	dva
míče	míč	k1gInPc4	míč
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
deux	deux	k1gInSc1	deux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
označoval	označovat	k5eAaImAgInS	označovat
jako	jako	k9	jako
shoda	shoda	k1gFnSc1	shoda
(	(	kIx(	(
<g/>
deuce	deuce	k1gFnSc1	deuce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nula	nula	k1gFnSc1	nula
se	se	k3xPyFc4	se
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
láska	láska	k1gFnSc1	láska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pořekadla	pořekadlo	k1gNnSc2	pořekadlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdo	kdo	k3yQnSc1	kdo
nevyhrál	vyhrát	k5eNaPmAgMnS	vyhrát
žádné	žádný	k3yNgInPc4	žádný
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
zbývá	zbývat	k5eAaImIp3nS	zbývat
ještě	ještě	k9	ještě
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
upadl	upadnout	k5eAaPmAgMnS	upadnout
původ	původ	k1gInSc4	původ
tohoto	tento	k3xDgNnSc2	tento
bodování	bodování	k1gNnSc2	bodování
v	v	k7c4	v
zapomnění	zapomnění	k1gNnSc4	zapomnění
a	a	k8xC	a
číslo	číslo	k1gNnSc4	číslo
45	[number]	k4	45
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
jednodušším	jednoduchý	k2eAgInSc7d2	jednodušší
40	[number]	k4	40
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
hrálo	hrát	k5eAaImAgNnS	hrát
holou	holý	k2eAgFnSc7d1	holá
dlaní	dlaň	k1gFnSc7	dlaň
<g/>
,	,	kIx,	,
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
rakety	raketa	k1gFnPc4	raketa
(	(	kIx(	(
<g/>
ze	z	k7c2	z
starofrancouzského	starofrancouzský	k2eAgMnSc2d1	starofrancouzský
racachier	racachier	k1gMnSc1	racachier
<g/>
,	,	kIx,	,
vracet	vracet	k5eAaImF	vracet
míč	míč	k1gInSc4	míč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
slovního	slovní	k2eAgInSc2d1	slovní
základu	základ	k1gInSc2	základ
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
fríský	fríský	k2eAgInSc1d1	fríský
národní	národní	k2eAgInSc1d1	národní
sport	sport	k1gInSc1	sport
keatsen	keatsen	k2eAgInSc1d1	keatsen
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
bez	bez	k7c2	bez
raket	raketa	k1gFnPc2	raketa
se	se	k3xPyFc4	se
udržela	udržet	k5eAaPmAgFnS	udržet
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
jeu	jeu	k?	jeu
de	de	k?	de
paume	paumat	k5eAaPmIp3nS	paumat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devatenáctém	devatenáctý	k4xOgInSc6	devatenáctý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
lidová	lidový	k2eAgFnSc1d1	lidová
varianta	varianta	k1gFnSc1	varianta
zvaná	zvaný	k2eAgFnSc1d1	zvaná
lawn	lawn	k1gNnSc4	lawn
tennis	tennis	k1gInSc1	tennis
(	(	kIx(	(
<g/>
trávníkový	trávníkový	k2eAgInSc1d1	trávníkový
tenis	tenis	k1gInSc1	tenis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
původní	původní	k2eAgFnPc1d1	původní
branky	branka	k1gFnPc1	branka
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
základními	základní	k2eAgFnPc7d1	základní
čarami	čára	k1gFnPc7	čára
a	a	k8xC	a
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
pro	pro	k7c4	pro
původní	původní	k2eAgFnSc4d1	původní
hru	hra	k1gFnSc4	hra
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
název	název	k1gInSc1	název
royal	royal	k1gInSc1	royal
tennis	tennis	k1gInSc4	tennis
(	(	kIx(	(
<g/>
královský	královský	k2eAgInSc4d1	královský
tenis	tenis	k1gInSc4	tenis
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
real	reanout	k5eAaPmAgMnS	reanout
tennis	tennis	k1gInSc4	tennis
(	(	kIx(	(
<g/>
skutečný	skutečný	k2eAgInSc4d1	skutečný
tenis	tenis	k1gInSc4	tenis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1592	[number]	k4	1592
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
zaznamenána	zaznamenán	k2eAgNnPc1d1	zaznamenáno
pravidla	pravidlo	k1gNnPc1	pravidlo
francouzské	francouzský	k2eAgFnSc2d1	francouzská
hry	hra	k1gFnSc2	hra
paume	paumat	k5eAaPmIp3nS	paumat
<g/>
.	.	kIx.	.
</s>
<s>
Angličan	Angličan	k1gMnSc1	Angličan
Walter	Walter	k1gMnSc1	Walter
Clopton	Clopton	k1gInSc4	Clopton
Wingfield	Wingfield	k1gInSc1	Wingfield
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
těmito	tento	k3xDgFnPc7	tento
pravidly	pravidlo	k1gNnPc7	pravidlo
inspirovat	inspirovat	k5eAaBmF	inspirovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
nechal	nechat	k5eAaPmAgMnS	nechat
patentovat	patentovat	k5eAaBmF	patentovat
hru	hra	k1gFnSc4	hra
zvanou	zvaný	k2eAgFnSc4d1	zvaná
tenis	tenis	k1gInSc4	tenis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
se	se	k3xPyFc4	se
ve	v	k7c6	v
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
konalo	konat	k5eAaImAgNnS	konat
první	první	k4xOgNnSc1	první
mistrovství	mistrovství	k1gNnSc1	mistrovství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
největší	veliký	k2eAgInPc1d3	veliký
turnaje	turnaj	k1gInPc1	turnaj
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
(	(	kIx(	(
<g/>
Flushing	Flushing	k1gInSc1	Flushing
Meadows	Meadowsa	k1gFnPc2	Meadowsa
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
australském	australský	k2eAgInSc6d1	australský
Melbournu	Melbourn	k1gInSc6	Melbourn
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Roland	Roland	k1gInSc1	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
programu	program	k1gInSc6	program
byl	být	k5eAaImAgInS	být
tenis	tenis	k1gInSc1	tenis
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
do	do	k7c2	do
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
opět	opět	k6eAd1	opět
mezi	mezi	k7c4	mezi
olympijské	olympijský	k2eAgInPc4d1	olympijský
sporty	sport	k1gInPc4	sport
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
ILTF	ILTF	kA	ILTF
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
ITF	ITF	kA	ITF
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tenis	tenis	k1gInSc1	tenis
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
olympijským	olympijský	k2eAgInPc3d1	olympijský
sportům	sport	k1gInPc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tenisu	tenis	k1gInSc2	tenis
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
několik	několik	k4yIc1	několik
podobných	podobný	k2eAgInPc2d1	podobný
sportů	sport	k1gInPc2	sport
hraných	hraný	k2eAgInPc2d1	hraný
zejména	zejména	k9	zejména
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
na	na	k7c6	na
speciálním	speciální	k2eAgInSc6d1	speciální
kurtu	kurt	k1gInSc6	kurt
např.	např.	kA	např.
<g/>
:	:	kIx,	:
plážový	plážový	k2eAgInSc1d1	plážový
tenis	tenis	k1gInSc1	tenis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pravidla	pravidlo	k1gNnPc1	pravidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
nesmí	smět	k5eNaImIp3nS	smět
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
nesmí	smět	k5eNaImIp3nS	smět
ani	ani	k8xC	ani
tenisovou	tenisový	k2eAgFnSc7d1	tenisová
raketou	raketa	k1gFnSc7	raketa
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
polovinu	polovina	k1gFnSc4	polovina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Při	při	k7c6	při
servisu	servis	k1gInSc6	servis
tenista	tenista	k1gMnSc1	tenista
nesmí	smět	k5eNaImIp3nS	smět
přešlápnout	přešlápnout	k5eAaPmF	přešlápnout
čáru	čára	k1gFnSc4	čára
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
trefit	trefit	k5eAaPmF	trefit
křížem	kříž	k1gInSc7	kříž
na	na	k7c4	na
první	první	k4xOgNnPc4	první
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
podání	podání	k1gNnSc6	podání
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
pokusy	pokus	k1gInPc4	pokus
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
musí	muset	k5eAaImIp3nS	muset
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
umístit	umístit	k5eAaPmF	umístit
míč	míč	k1gInSc4	míč
do	do	k7c2	do
kurtu	kurt	k1gInSc2	kurt
</s>
</p>
<p>
<s>
===	===	k?	===
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Tenis	tenis	k1gInSc1	tenis
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
elegantní	elegantní	k2eAgFnSc1d1	elegantní
míčová	míčový	k2eAgFnSc1d1	Míčová
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
hrát	hrát	k5eAaImF	hrát
dva	dva	k4xCgMnPc1	dva
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgMnPc1	čtyři
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
tenisová	tenisový	k2eAgFnSc1d1	tenisová
raketa	raketa	k1gFnSc1	raketa
<g/>
,	,	kIx,	,
míček	míček	k1gInSc1	míček
<g/>
,	,	kIx,	,
tenisový	tenisový	k2eAgInSc1d1	tenisový
dvorec	dvorec	k1gInSc1	dvorec
a	a	k8xC	a
tenisová	tenisový	k2eAgFnSc1d1	tenisová
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Hráči	hráč	k1gMnPc1	hráč
tenisu	tenis	k1gInSc2	tenis
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
váhy	váha	k1gFnSc2	váha
či	či	k8xC	či
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
hrají	hrát	k5eAaImIp3nP	hrát
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
ženy	žena	k1gFnPc1	žena
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nS	tvořit
turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
muž	muž	k1gMnSc1	muž
i	i	k8xC	i
žena	žena	k1gFnSc1	žena
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
čtyřhře	čtyřhra	k1gFnSc3	čtyřhra
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
smíšená	smíšený	k2eAgFnSc1d1	smíšená
čtyřhra	čtyřhra	k1gFnSc1	čtyřhra
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
mix	mix	k1gInSc1	mix
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
vrátit	vrátit	k5eAaPmF	vrátit
míč	míč	k1gInSc4	míč
na	na	k7c4	na
soupeřovu	soupeřův	k2eAgFnSc4d1	soupeřova
polovinu	polovina	k1gFnSc4	polovina
dvorce	dvorec	k1gInSc2	dvorec
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gNnSc4	on
soupeř	soupeř	k1gMnSc1	soupeř
vůbec	vůbec	k9	vůbec
nezahrál	zahrát	k5eNaPmAgMnS	zahrát
či	či	k8xC	či
nedoběhl	doběhnout	k5eNaPmAgMnS	doběhnout
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gNnSc3	on
vrácení	vrácení	k1gNnSc3	vrácení
míče	míč	k1gInSc2	míč
dělalo	dělat	k5eAaImAgNnS	dělat
potíže	potíž	k1gFnSc2	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
sporty	sport	k1gInPc7	sport
se	se	k3xPyFc4	se
tenis	tenis	k1gInSc1	tenis
hraje	hrát	k5eAaImIp3nS	hrát
se	s	k7c7	s
střední	střední	k2eAgFnSc7d1	střední
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
desítky	desítka	k1gFnPc4	desítka
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
klidně	klidně	k6eAd1	klidně
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tenisové	tenisový	k2eAgFnSc6d1	tenisová
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
vůle	vůle	k1gFnSc1	vůle
<g/>
,	,	kIx,	,
cílevědomost	cílevědomost	k1gFnSc1	cílevědomost
<g/>
,	,	kIx,	,
rychlý	rychlý	k2eAgInSc1d1	rychlý
odhad	odhad	k1gInSc1	odhad
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
spoléhání	spoléhání	k1gNnSc1	spoléhání
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
připravenost	připravenost	k1gFnSc4	připravenost
<g/>
,	,	kIx,	,
taktické	taktický	k2eAgNnSc4d1	taktické
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
soustředění	soustředění	k1gNnSc2	soustředění
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
klid	klid	k1gInSc4	klid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hra	hra	k1gFnSc1	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
hra	hra	k1gFnSc1	hra
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
získání	získání	k1gNnSc6	získání
gamu	game	k1gInSc2	game
(	(	kIx(	(
<g/>
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Bodování	bodování	k1gNnSc4	bodování
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejčastější	častý	k2eAgFnPc4d3	nejčastější
chyby	chyba	k1gFnPc4	chyba
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
získání	získání	k1gNnSc6	získání
gamu	game	k1gInSc2	game
(	(	kIx(	(
<g/>
hry	hra	k1gFnSc2	hra
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Bodování	bodování	k1gNnSc4	bodování
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
druhé	druhý	k4xOgNnSc1	druhý
podání	podání	k1gNnSc1	podání
nedopadlo	dopadnout	k5eNaPmAgNnS	dopadnout
do	do	k7c2	do
obdélníku	obdélník	k1gInSc2	obdélník
pro	pro	k7c4	pro
podání	podání	k1gNnSc4	podání
nebo	nebo	k8xC	nebo
nepřeletělo	přeletět	k5eNaPmAgNnS	přeletět
síť	síť	k1gFnSc4	síť
</s>
</p>
<p>
<s>
míček	míček	k1gInSc1	míček
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
dvakrát	dvakrát	k6eAd1	dvakrát
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
ho	on	k3xPp3gMnSc4	on
hráč	hráč	k1gMnSc1	hráč
odehraje	odehrát	k5eAaPmIp3nS	odehrát
</s>
</p>
<p>
<s>
hráč	hráč	k1gMnSc1	hráč
zahraje	zahrát	k5eAaPmIp3nS	zahrát
tzv.	tzv.	kA	tzv.
dvojdotek	dvojdotek	k1gInSc1	dvojdotek
(	(	kIx(	(
<g/>
při	při	k7c6	při
úderu	úder	k1gInSc6	úder
se	se	k3xPyFc4	se
míčkem	míček	k1gInSc7	míček
hráč	hráč	k1gMnSc1	hráč
úmyslně	úmyslně	k6eAd1	úmyslně
dvakrát	dvakrát	k6eAd1	dvakrát
dotkne	dotknout	k5eAaPmIp3nS	dotknout
rakety	raketa	k1gFnPc4	raketa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
míč	míč	k1gInSc4	míč
raketou	raketa	k1gFnSc7	raketa
nese	nést	k5eAaImIp3nS	nést
</s>
</p>
<p>
<s>
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
míčkem	míček	k1gInSc7	míček
objektu	objekt	k1gInSc2	objekt
vně	vně	k7c2	vně
dvorce	dvorec	k1gInSc2	dvorec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
stropu	strop	k1gInSc2	strop
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hráč	hráč	k1gMnSc1	hráč
se	se	k3xPyFc4	se
dotkne	dotknout	k5eAaPmIp3nS	dotknout
sítě	síť	k1gFnSc2	síť
(	(	kIx(	(
<g/>
raketou	raketa	k1gFnSc7	raketa
či	či	k8xC	či
tělem	tělo	k1gNnSc7	tělo
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
výměny	výměna	k1gFnSc2	výměna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hráč	hráč	k1gMnSc1	hráč
odehraje	odehrát	k5eAaPmIp3nS	odehrát
míč	míč	k1gInSc4	míč
na	na	k7c6	na
soupeřově	soupeřův	k2eAgFnSc6d1	soupeřova
straně	strana	k1gFnSc6	strana
dvorce	dvorec	k1gInSc2	dvorec
</s>
</p>
<p>
<s>
===	===	k?	===
Bodování	bodování	k1gNnSc2	bodování
===	===	k?	===
</s>
</p>
<p>
<s>
Získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
hráč	hráč	k1gMnSc1	hráč
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
30	[number]	k4	30
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
třetí	třetí	k4xOgMnPc1	třetí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
40	[number]	k4	40
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
game	game	k1gInSc4	game
(	(	kIx(	(
<g/>
hru	hra	k1gFnSc4	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
40	[number]	k4	40
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgInSc1d3	Nejbližší
bod	bod	k1gInSc1	bod
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
jako	jako	k9	jako
výhoda	výhoda	k1gFnSc1	výhoda
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
hráče	hráč	k1gMnSc4	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
získal	získat	k5eAaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Získá	získat	k5eAaPmIp3nS	získat
<g/>
-li	i	k?	-li
týž	týž	k3xTgInSc4	týž
hráč	hráč	k1gMnSc1	hráč
následující	následující	k2eAgInSc4d1	následující
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
vyhrává	vyhrávat	k5eAaImIp3nS	vyhrávat
hru	hra	k1gFnSc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jej	on	k3xPp3gMnSc4	on
nezíská	získat	k5eNaPmIp3nS	získat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
shoda	shoda	k1gFnSc1	shoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
získá	získat	k5eAaPmIp3nS	získat
nejméně	málo	k6eAd3	málo
6	[number]	k4	6
gamů	game	k1gInPc2	game
(	(	kIx(	(
<g/>
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vede	vést	k5eAaImIp3nS	vést
alespoň	alespoň	k9	alespoň
o	o	k7c4	o
2	[number]	k4	2
gamy	game	k1gInPc4	game
(	(	kIx(	(
<g/>
hry	hra	k1gFnPc4	hra
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
např.	např.	kA	např.
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
nebo	nebo	k8xC	nebo
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
set	set	k1gInSc4	set
(	(	kIx(	(
<g/>
sadu	sada	k1gFnSc4	sada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
James	James	k1gMnSc1	James
Van	vana	k1gFnPc2	vana
Alen	Alena	k1gFnPc2	Alena
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
pro	pro	k7c4	pro
zrychlení	zrychlení	k1gNnSc4	zrychlení
hry	hra	k1gFnSc2	hra
následující	následující	k2eAgNnSc4d1	následující
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
:	:	kIx,	:
Za	za	k7c2	za
stavu	stav	k1gInSc2	stav
sety	set	k1gInPc1	set
(	(	kIx(	(
<g/>
sady	sada	k1gFnPc1	sada
<g/>
)	)	kIx)	)
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
jeden	jeden	k4xCgInSc1	jeden
poslední	poslední	k2eAgInSc1d1	poslední
game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
setu	set	k1gInSc2	set
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tiebreak	tiebreak	k1gInSc1	tiebreak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
se	se	k3xPyFc4	se
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
střídají	střídat	k5eAaImIp3nP	střídat
podání	podání	k1gNnSc4	podání
a	a	k8xC	a
strany	strana	k1gFnPc4	strana
hřiště	hřiště	k1gNnSc2	hřiště
a	a	k8xC	a
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
jej	on	k3xPp3gMnSc4	on
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
získá	získat	k5eAaPmIp3nS	získat
nejméně	málo	k6eAd3	málo
7	[number]	k4	7
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vede	vést	k5eAaImIp3nS	vést
alespoň	alespoň	k9	alespoň
o	o	k7c4	o
2	[number]	k4	2
body	bod	k1gInPc4	bod
(	(	kIx(	(
<g/>
např.	např.	kA	např.
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
tiebreaku	tiebreak	k1gInSc2	tiebreak
získává	získávat	k5eAaImIp3nS	získávat
sadu	sada	k1gFnSc4	sada
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
závěrečné	závěrečný	k2eAgFnSc6d1	závěrečná
sadě	sada	k1gFnSc6	sada
zápasu	zápas	k1gInSc2	zápas
určitých	určitý	k2eAgInPc2d1	určitý
turnajů	turnaj	k1gInPc2	turnaj
se	se	k3xPyFc4	se
pravidlo	pravidlo	k1gNnSc1	pravidlo
tiebreaku	tiebreak	k1gInSc2	tiebreak
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
se	se	k3xPyFc4	se
tiebreak	tiebreak	k6eAd1	tiebreak
hraje	hrát	k5eAaImIp3nS	hrát
až	až	k9	až
za	za	k7c2	za
shody	shoda	k1gFnSc2	shoda
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
se	se	k3xPyFc4	se
od	od	k7c2	od
ročníku	ročník	k1gInSc2	ročník
2019	[number]	k4	2019
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
setu	set	k1gInSc6	set
tiebreak	tiebreak	k6eAd1	tiebreak
až	až	k9	až
za	za	k7c2	za
stavu	stav	k1gInSc2	stav
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyhrát	vyhrát	k5eAaPmF	vyhrát
tento	tento	k3xDgInSc4	tento
set	set	k1gInSc4	set
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
celý	celý	k2eAgInSc4d1	celý
zápas	zápas	k1gInSc4	zápas
vedením	vedení	k1gNnSc7	vedení
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
hry	hra	k1gFnPc4	hra
např.	např.	kA	např.
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
dosud	dosud	k6eAd1	dosud
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
tiebreak	tiebreak	k1gInSc1	tiebreak
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
na	na	k7c6	na
Wimbledonu	Wimbledon	k1gInSc6	Wimbledon
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
John	John	k1gMnSc1	John
Isner	Isner	k1gMnSc1	Isner
a	a	k8xC	a
Nicolas	Nicolas	k1gMnSc1	Nicolas
Mahut	Mahut	k1gMnSc1	Mahut
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
John	John	k1gMnSc1	John
Isner	Isner	k1gMnSc1	Isner
(	(	kIx(	(
<g/>
70	[number]	k4	70
<g/>
:	:	kIx,	:
<g/>
68	[number]	k4	68
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
hrál	hrát	k5eAaImAgInS	hrát
přes	přes	k7c4	přes
3	[number]	k4	3
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
hry	hra	k1gFnSc2	hra
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
požadovaný	požadovaný	k2eAgInSc4d1	požadovaný
počet	počet	k1gInSc4	počet
setů	set	k1gInPc2	set
(	(	kIx(	(
<g/>
sad	sada	k1gFnPc2	sada
<g/>
)	)	kIx)	)
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
2	[number]	k4	2
vítězné	vítězný	k2eAgInPc4d1	vítězný
sety	set	k1gInPc4	set
(	(	kIx(	(
<g/>
sady	sada	k1gFnPc4	sada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organizátor	organizátor	k1gInSc1	organizátor
soutěže	soutěž	k1gFnSc2	soutěž
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
pak	pak	k6eAd1	pak
hráč	hráč	k1gMnSc1	hráč
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
získat	získat	k5eAaPmF	získat
3	[number]	k4	3
sety	set	k1gInPc4	set
(	(	kIx(	(
<g/>
sady	sada	k1gFnPc4	sada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
muži	muž	k1gMnPc1	muž
na	na	k7c6	na
grandslamových	grandslamový	k2eAgInPc6d1	grandslamový
turnajích	turnaj	k1gInPc6	turnaj
nebo	nebo	k8xC	nebo
v	v	k7c6	v
utkáních	utkání	k1gNnPc6	utkání
Davisova	Davisův	k2eAgInSc2d1	Davisův
poháru	pohár	k1gInSc2	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pravidlech	pravidlo	k1gNnPc6	pravidlo
tenisu	tenis	k1gInSc2	tenis
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
další	další	k2eAgFnPc1d1	další
doplňkové	doplňkový	k2eAgFnPc1d1	doplňková
metody	metoda	k1gFnPc1	metoda
počítání	počítání	k1gNnSc2	počítání
skóre	skóre	k1gNnSc2	skóre
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
hra	hra	k1gFnSc1	hra
bez	bez	k7c2	bez
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
krátké	krátký	k2eAgInPc4d1	krátký
sety	set	k1gInPc4	set
(	(	kIx(	(
<g/>
do	do	k7c2	do
4	[number]	k4	4
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tie-break	tiereak	k1gInSc1	tie-break
nebo	nebo	k8xC	nebo
královský	královský	k2eAgInSc1d1	královský
tie-break	tiereak	k1gInSc1	tie-break
(	(	kIx(	(
<g/>
do	do	k7c2	do
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
místo	místo	k7c2	místo
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
sady	sada	k1gFnSc2	sada
za	za	k7c2	za
nerozhodného	rozhodný	k2eNgInSc2d1	nerozhodný
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přestávky	přestávka	k1gFnPc1	přestávka
===	===	k?	===
</s>
</p>
<p>
<s>
Vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
gamu	game	k1gInSc6	game
každého	každý	k3xTgInSc2	každý
setu	set	k1gInSc2	set
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
následně	následně	k6eAd1	následně
po	po	k7c6	po
každých	každý	k3xTgInPc6	každý
dvou	dva	k4xCgInPc6	dva
gamech	game	k1gInPc6	game
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
setu	set	k1gInSc2	set
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
každou	každý	k3xTgFnSc7	každý
takovou	takový	k3xDgFnSc7	takový
přestávkou	přestávka	k1gFnSc7	přestávka
souvisí	souviset	k5eAaImIp3nS	souviset
změna	změna	k1gFnSc1	změna
stran	strana	k1gFnPc2	strana
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hřiště	hřiště	k1gNnSc1	hřiště
a	a	k8xC	a
vybavení	vybavení	k1gNnSc1	vybavení
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tenisový	tenisový	k2eAgInSc1d1	tenisový
dvorec	dvorec	k1gInSc1	dvorec
====	====	k?	====
</s>
</p>
<p>
<s>
Tenisový	tenisový	k2eAgInSc1d1	tenisový
dvorec	dvorec	k1gInSc1	dvorec
má	mít	k5eAaImIp3nS	mít
obdélníkový	obdélníkový	k2eAgInSc4d1	obdélníkový
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
rovný	rovný	k2eAgInSc4d1	rovný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tenisové	tenisový	k2eAgNnSc1d1	tenisové
hřiště	hřiště	k1gNnSc1	hřiště
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
23,78	[number]	k4	23,78
m	m	kA	m
a	a	k8xC	a
široké	široký	k2eAgInPc1d1	široký
8,23	[number]	k4	8,23
m	m	kA	m
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
je	být	k5eAaImIp3nS	být
dvorec	dvorec	k1gInSc1	dvorec
široký	široký	k2eAgInSc1d1	široký
10,97	[number]	k4	10,97
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kurt	Kurt	k1gMnSc1	Kurt
je	být	k5eAaImIp3nS	být
uprostřed	uprostřed	k6eAd1	uprostřed
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
sítí	síť	k1gFnSc7	síť
zavěšenou	zavěšený	k2eAgFnSc7d1	zavěšená
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
0,914	[number]	k4	0,914
m	m	kA	m
(	(	kIx(	(
<g/>
měřeno	měřit	k5eAaImNgNnS	měřit
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
kurtu	kurt	k1gInSc2	kurt
má	mít	k5eAaImIp3nS	mít
síť	síť	k1gFnSc4	síť
výšku	výška	k1gFnSc4	výška
1,07	[number]	k4	1,07
m.	m.	k?	m.
Při	při	k7c6	při
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
využíváno	využívat	k5eAaImNgNnS	využívat
postranní	postranní	k2eAgNnSc1d1	postranní
rozšíření	rozšíření	k1gNnSc1	rozšíření
kurtu	kurt	k1gInSc2	kurt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povrchy	povrch	k1gInPc1	povrch
kurtu	kurt	k1gInSc2	kurt
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
jinou	jiný	k2eAgFnSc4d1	jiná
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
odrazu	odraz	k1gInSc2	odraz
míče	míč	k1gInSc2	míč
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
pohybu	pohyb	k1gInSc2	pohyb
hráčů	hráč	k1gMnPc2	hráč
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
mění	měnit	k5eAaImIp3nS	měnit
styl	styl	k1gInSc4	styl
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Čtyřmi	čtyři	k4xCgInPc7	čtyři
nejběžnějšími	běžný	k2eAgInPc7d3	nejběžnější
typy	typ	k1gInPc7	typ
povrchů	povrch	k1gInPc2	povrch
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
antuka	antuka	k1gFnSc1	antuka
–	–	k?	–
nejčastěji	často	k6eAd3	často
červená	červený	k2eAgFnSc1d1	červená
(	(	kIx(	(
<g/>
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
např.	např.	kA	např.
na	na	k7c6	na
French	Fren	k1gFnPc6	Fren
Open	Openo	k1gNnPc2	Openo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zelená	zelený	k2eAgFnSc1d1	zelená
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
povrch	povrch	k1gInSc1	povrch
–	–	k?	–
například	například	k6eAd1	například
beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
beton	beton	k1gInSc1	beton
potažený	potažený	k2eAgInSc1d1	potažený
asfaltem	asfalt	k1gInSc7	asfalt
(	(	kIx(	(
<g/>
US	US	kA	US
Open	Open	k1gNnSc4	Open
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
tráva	tráva	k1gFnSc1	tráva
–	–	k?	–
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
britský	britský	k2eAgInSc4d1	britský
Wimbledon	Wimbledon	k1gInSc4	Wimbledon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tartan	tartan	k1gInSc1	tartan
–	–	k?	–
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
povrch	povrch	k1gInSc1	povrch
</s>
</p>
<p>
<s>
====	====	k?	====
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
raketa	raketa	k1gFnSc1	raketa
====	====	k?	====
</s>
</p>
<p>
<s>
Tenisová	tenisový	k2eAgFnSc1d1	tenisová
raketa	raketa	k1gFnSc1	raketa
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
rovná	rovný	k2eAgFnSc1d1	rovná
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
ji	on	k3xPp3gFnSc4	on
struny	struna	k1gFnPc1	struna
střídavě	střídavě	k6eAd1	střídavě
propletené	propletený	k2eAgInPc4d1	propletený
nebo	nebo	k8xC	nebo
svázané	svázaný	k2eAgInPc4d1	svázaný
a	a	k8xC	a
připojené	připojený	k2eAgInPc4d1	připojený
k	k	k7c3	k
rámu	rám	k1gInSc3	rám
rakety	raketa	k1gFnSc2	raketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
rakety	raketa	k1gFnSc2	raketa
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
73,66	[number]	k4	73,66
cm	cm	kA	cm
<g/>
.	.	kIx.	.
<g/>
Rám	rám	k1gInSc1	rám
rakety	raketa	k1gFnSc2	raketa
nesmí	smět	k5eNaImIp3nS	smět
mít	mít	k5eAaImF	mít
celkovou	celkový	k2eAgFnSc4d1	celková
šířku	šířka	k1gFnSc4	šířka
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
31,75	[number]	k4	31,75
cm	cm	kA	cm
<g/>
.	.	kIx.	.
<g/>
Plocha	plocha	k1gFnSc1	plocha
výpletu	výplet	k1gInSc2	výplet
nesmí	smět	k5eNaImIp3nS	smět
mít	mít	k5eAaImF	mít
celkovou	celkový	k2eAgFnSc4d1	celková
délku	délka	k1gFnSc4	délka
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
39,37	[number]	k4	39,37
cm	cm	kA	cm
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc4d1	celková
šířku	šířka	k1gFnSc4	šířka
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
29,21	[number]	k4	29,21
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Tenisový	tenisový	k2eAgInSc4d1	tenisový
míč	míč	k1gInSc4	míč
====	====	k?	====
</s>
</p>
<p>
<s>
Míč	míč	k1gInSc1	míč
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
vnější	vnější	k2eAgInSc4d1	vnější
povrch	povrch	k1gInSc4	povrch
všude	všude	k6eAd1	všude
stejný	stejný	k2eAgInSc4d1	stejný
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgInSc4d1	sestávající
z	z	k7c2	z
látkového	látkový	k2eAgInSc2d1	látkový
potahu	potah	k1gInSc2	potah
a	a	k8xC	a
žluté	žlutý	k2eAgFnPc4d1	žlutá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
švy	šev	k1gInPc4	šev
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
bez	bez	k7c2	bez
stehů	steh	k1gInPc2	steh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Správný	správný	k2eAgInSc1d1	správný
míč	míč	k1gInSc1	míč
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
odskok	odskok	k1gInSc1	odskok
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
1,35	[number]	k4	1,35
m	m	kA	m
a	a	k8xC	a
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
1,47	[number]	k4	1,47
m	m	kA	m
<g/>
,	,	kIx,	,
pustí	pustý	k2eAgMnPc1d1	pustý
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
na	na	k7c4	na
betonový	betonový	k2eAgInSc4d1	betonový
podklad	podklad	k1gInSc4	podklad
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
2,54	[number]	k4	2,54
m.	m.	k?	m.
<g/>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
barevné	barevný	k2eAgInPc4d1	barevný
tréninkové	tréninkový	k2eAgInPc4d1	tréninkový
míče	míč	k1gInPc4	míč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Grandslamové	grandslamový	k2eAgInPc4d1	grandslamový
turnaje	turnaj	k1gInPc4	turnaj
==	==	k?	==
</s>
</p>
<p>
<s>
Wimbledon	Wimbledon	k1gInSc1	Wimbledon
</s>
</p>
<p>
<s>
US	US	kA	US
Open	Open	k1gMnSc1	Open
</s>
</p>
<p>
<s>
French	French	k1gMnSc1	French
Open	Open	k1gMnSc1	Open
</s>
</p>
<p>
<s>
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
</s>
</p>
<p>
<s>
==	==	k?	==
Soutěže	soutěž	k1gFnPc4	soutěž
družstev	družstvo	k1gNnPc2	družstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Davis	Davis	k1gInSc1	Davis
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Světový	světový	k2eAgInSc1d1	světový
pohár	pohár	k1gInSc1	pohár
družstev	družstvo	k1gNnPc2	družstvo
</s>
</p>
<p>
<s>
Fed	Fed	k?	Fed
Cup	cup	k1gInSc1	cup
</s>
</p>
<p>
<s>
Hopmanův	Hopmanův	k2eAgInSc4d1	Hopmanův
pohár	pohár	k1gInSc4	pohár
</s>
</p>
<p>
<s>
==	==	k?	==
Tenisové	tenisový	k2eAgFnPc1d1	tenisová
organizace	organizace	k1gFnPc1	organizace
==	==	k?	==
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
-	-	kIx~	-
Asociace	asociace	k1gFnSc1	asociace
tenisových	tenisový	k2eAgMnPc2d1	tenisový
profesionálů	profesionál	k1gMnPc2	profesionál
</s>
</p>
<p>
<s>
WTA	WTA	kA	WTA
-	-	kIx~	-
Ženská	ženský	k2eAgFnSc1d1	ženská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
asociace	asociace	k1gFnSc1	asociace
</s>
</p>
<p>
<s>
ITF	ITF	kA	ITF
-	-	kIx~	-
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
tenisový	tenisový	k2eAgInSc1d1	tenisový
svaz	svaz	k1gInSc1	svaz
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tenis	tenis	k1gInSc1	tenis
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tenis	tenis	k1gInSc1	tenis
(	(	kIx(	(
<g/>
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
)	)	kIx)	)
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Adresář	adresář	k1gInSc1	adresář
užitečných	užitečný	k2eAgFnPc2d1	užitečná
českých	český	k2eAgFnPc2d1	Česká
tenisových	tenisový	k2eAgFnPc2d1	tenisová
stránek	stránka	k1gFnPc2	stránka
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
tenisový	tenisový	k2eAgInSc1d1	tenisový
svaz	svaz	k1gInSc1	svaz
</s>
</p>
<p>
<s>
ATP	atp	kA	atp
-	-	kIx~	-
Asociace	asociace	k1gFnSc1	asociace
tenisových	tenisový	k2eAgMnPc2d1	tenisový
profesionálů	profesionál	k1gMnPc2	profesionál
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
WTA	WTA	kA	WTA
-	-	kIx~	-
Ženská	ženský	k2eAgFnSc1d1	ženská
tenisová	tenisový	k2eAgFnSc1d1	tenisová
asociace	asociace	k1gFnSc1	asociace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ITF	ITF	kA	ITF
-	-	kIx~	-
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tenis	tenis	k1gInSc4	tenis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
