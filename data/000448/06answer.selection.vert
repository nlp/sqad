<s>
Kationt	kationt	k1gInSc1	kationt
je	být	k5eAaImIp3nS	být
kladně	kladně	k6eAd1	kladně
nabitý	nabitý	k2eAgInSc1d1	nabitý
iont	iont	k1gInSc1	iont
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
atom	atom	k1gInSc1	atom
nebo	nebo	k8xC	nebo
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odevzdala	odevzdat	k5eAaPmAgFnS	odevzdat
elektron	elektron	k1gInSc4	elektron
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
pohltila	pohltit	k5eAaPmAgFnS	pohltit
kationty	kation	k1gInPc4	kation
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc1d1	volný
protony	proton	k1gInPc1	proton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
