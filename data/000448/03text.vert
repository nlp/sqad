<s>
Kationt	kationt	k1gInSc1	kationt
je	být	k5eAaImIp3nS	být
kladně	kladně	k6eAd1	kladně
nabitý	nabitý	k2eAgInSc1d1	nabitý
iont	iont	k1gInSc1	iont
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
atom	atom	k1gInSc1	atom
nebo	nebo	k8xC	nebo
molekula	molekula	k1gFnSc1	molekula
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odevzdala	odevzdat	k5eAaPmAgFnS	odevzdat
elektron	elektron	k1gInSc4	elektron
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
pohltila	pohltit	k5eAaPmAgFnS	pohltit
kationty	kation	k1gInPc4	kation
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
volné	volný	k2eAgInPc1d1	volný
protony	proton	k1gInPc1	proton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kationt	kationt	k1gInSc1	kationt
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
elektronovém	elektronový	k2eAgInSc6d1	elektronový
obalu	obal	k1gInSc6	obal
méně	málo	k6eAd2	málo
elektronů	elektron	k1gInPc2	elektron
než	než	k8xS	než
odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
atom	atom	k1gInSc4	atom
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
putuje	putovat	k5eAaImIp3nS	putovat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
katodě	katoda	k1gFnSc3	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Kationty	kation	k1gInPc1	kation
většinou	většinou	k6eAd1	většinou
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
elektropozitivních	elektropozitivní	k2eAgInPc2d1	elektropozitivní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
nebo	nebo	k8xC	nebo
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
:	:	kIx,	:
K	k	k7c3	k
→	→	k?	→
K	K	kA	K
<g/>
+	+	kIx~	+
+	+	kIx~	+
e-	e-	k?	e-
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
také	také	k9	také
vznikat	vznikat	k5eAaImF	vznikat
z	z	k7c2	z
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
např.	např.	kA	např.
amonný	amonný	k2eAgInSc1d1	amonný
kationt	kationt	k1gInSc1	kationt
z	z	k7c2	z
molekuly	molekula	k1gFnSc2	molekula
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
:	:	kIx,	:
NH3	NH3	k1gMnSc1	NH3
+	+	kIx~	+
H	H	kA	H
<g/>
+	+	kIx~	+
→	→	k?	→
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
nebo	nebo	k8xC	nebo
oxoniový	oxoniový	k2eAgInSc1d1	oxoniový
kationt	kationt	k1gInSc1	kationt
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
:	:	kIx,	:
H2O	H2O	k1gMnSc1	H2O
+	+	kIx~	+
H	H	kA	H
<g/>
+	+	kIx~	+
→	→	k?	→
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
jedno	jeden	k4xCgNnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
atomové	atomový	k2eAgInPc4d1	atomový
kationty	kation	k1gInPc4	kation
sodný	sodný	k2eAgInSc4d1	sodný
kationt	kationt	k1gInSc4	kationt
Na	na	k7c4	na
<g/>
+	+	kIx~	+
<g/>
I	i	k8xC	i
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
kationt	kationt	k1gInSc1	kationt
Ca	ca	kA	ca
<g/>
+	+	kIx~	+
<g/>
II	II	kA	II
hlinitý	hlinitý	k2eAgInSc1d1	hlinitý
kationt	kationt	k1gInSc1	kationt
Al	ala	k1gFnPc2	ala
<g/>
+	+	kIx~	+
<g/>
III	III	kA	III
titaničitý	titaničitý	k2eAgInSc4d1	titaničitý
kationt	kationt	k1gInSc4	kationt
Ti	ten	k3xDgMnPc1	ten
<g/>
+	+	kIx~	+
<g/>
IV	IV	kA	IV
molekulové	molekulový	k2eAgInPc4d1	molekulový
kationty	kation	k1gInPc4	kation
amonný	amonný	k2eAgInSc4d1	amonný
kationt	kationt	k1gInSc4	kationt
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
oxoniový	oxoniový	k2eAgInSc1d1	oxoniový
kationt	kationt	k1gInSc1	kationt
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
+	+	kIx~	+
uranylový	uranylový	k2eAgInSc4d1	uranylový
kationt	kationt	k1gInSc4	kationt
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
II	II	kA	II
</s>
