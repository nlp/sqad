<s>
Markvart	Markvart	k1gInSc1
z	z	k7c2
Annweileru	Annweiler	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Markward	Markward	k1gInSc1
von	von	k1gInSc1
Annweiler	Annweiler	k1gInSc4
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
Marcovaldo	Marcovaldo	k1gNnSc1
di	di	k?
Annweiler	Annweiler	k1gMnSc1
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
1202	#num#	k4
Patti	Patti	k1gNnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
říšský	říšský	k2eAgMnSc1d1
ministeriál	ministeriál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
třetí	třetí	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
a	a	k8xC
štaufský	štaufský	k2eAgMnSc1d1
regent	regent	k1gMnSc1
sicilského	sicilský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>