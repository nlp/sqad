<s>
Markvart	Markvart	k1gInSc1
z	z	k7c2
Annweileru	Annweiler	k1gInSc2
</s>
<s>
Markvart	Markvart	k1gInSc1
z	z	k7c2
Annweileru	Annweiler	k1gInSc2
Narození	narození	k1gNnSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
1202	#num#	k4
<g/>
Patti	Patti	k1gNnSc7
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
úplavice	úplavice	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
politik	politik	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Markvart	Markvart	k1gInSc1
z	z	k7c2
Annweileru	Annweiler	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Markward	Markward	k1gInSc1
von	von	k1gInSc1
Annweiler	Annweiler	k1gInSc4
<g/>
,	,	kIx,
italsky	italsky	k6eAd1
Marcovaldo	Marcovaldo	k1gNnSc1
di	di	k?
Annweiler	Annweiler	k1gMnSc1
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
1202	#num#	k4
Patti	Patti	k1gNnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
říšský	říšský	k2eAgMnSc1d1
ministeriál	ministeriál	k1gMnSc1
<g/>
,	,	kIx,
účastník	účastník	k1gMnSc1
třetí	třetí	k4xOgFnSc2
křížové	křížový	k2eAgFnSc2d1
výpravy	výprava	k1gFnSc2
a	a	k8xC
štaufský	štaufský	k2eAgMnSc1d1
regent	regent	k1gMnSc1
sicilského	sicilský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Markward	Markward	k1gInSc1
von	von	k1gInSc1
Annweiler	Annweiler	k1gInSc4
</s>
<s>
Markward	Markward	k1gMnSc1
byl	být	k5eAaImAgMnS
původně	původně	k6eAd1
říšským	říšský	k2eAgMnSc7d1
ministeriálem	ministeriál	k1gMnSc7
<g/>
,	,	kIx,
poprvé	poprvé	k6eAd1
je	být	k5eAaImIp3nS
písemně	písemně	k6eAd1
doložen	doložen	k2eAgInSc1d1
roku	rok	k1gInSc3
1184	#num#	k4
jako	jako	k8xS,k8xC
společník	společník	k1gMnSc1
mladého	mladý	k2eAgMnSc2d1
římského	římský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
Barbarossy	Barbarossa	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1189	#num#	k4
vyrazil	vyrazit	k5eAaPmAgInS
s	s	k7c7
císařem	císař	k1gMnSc7
na	na	k7c4
kruciátu	kruciáta	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
poslů	posel	k1gMnPc2
k	k	k7c3
byzantskému	byzantský	k2eAgMnSc3d1
císaři	císař	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
krachu	krach	k1gInSc6
výpravy	výprava	k1gFnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Říše	říš	k1gFnSc2
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
1193-1195	1193-1195	k4
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgInS
na	na	k7c6
dobývání	dobývání	k1gNnSc6
Sicílie	Sicílie	k1gFnSc2
a	a	k8xC
za	za	k7c4
odměnu	odměna	k1gFnSc4
mu	on	k3xPp3gNnSc3
Jindřich	Jindřich	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
udělil	udělit	k5eAaPmAgMnS
svobody	svoboda	k1gFnSc2
a	a	k8xC
připsal	připsat	k5eAaPmAgMnS
vévodství	vévodství	k1gNnSc4
Ravennu	Ravenn	k1gInSc2
<g/>
,	,	kIx,
Romagnu	Romagen	k2eAgFnSc4d1
a	a	k8xC
anconskou	anconský	k2eAgFnSc4d1
marku	marka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1197	#num#	k4
společně	společně	k6eAd1
s	s	k7c7
maršálkem	maršálek	k1gMnSc7
Jindřichem	Jindřich	k1gMnSc7
z	z	k7c2
Kadeln	Kadelna	k1gFnPc2
utopil	utopit	k5eAaPmAgInS
v	v	k7c6
krvi	krev	k1gFnSc6
protištaufské	protištaufský	k2eAgNnSc1d1
normanské	normanský	k2eAgNnSc1d1
povstání	povstání	k1gNnSc1
na	na	k7c6
Sicílii	Sicílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
</s>
<s>
Nebylo	být	k5eNaImAgNnS
žádného	žádný	k3yNgNnSc2
útočiště	útočiště	k1gNnSc2
pro	pro	k7c4
obyvatelé	obyvatel	k1gMnPc1
té	ten	k3xDgFnSc2
země	zem	k1gFnSc2
<g/>
,	,	kIx,
kromě	kromě	k7c2
jediného	jediný	k2eAgInSc2d1
hradu	hrad	k1gInSc2
San	San	k1gMnSc3
Giovanni	Giovanň	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
mezi	mezi	k7c7
pevnostmi	pevnost	k1gFnPc7
onoho	onen	k3xDgInSc2
kraje	kraj	k1gInSc2
nejpevnější	pevný	k2eAgFnSc1d3
a	a	k8xC
nejlépe	dobře	k6eAd3
opevněný	opevněný	k2eAgInSc1d1
přirozenou	přirozený	k2eAgFnSc7d1
polohou	poloha	k1gFnSc7
<g/>
...	...	k?
</s>
<s>
“	“	k?
</s>
<s>
—	—	k?
Ansbert	Ansbert	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soudní	soudní	k2eAgInSc1d1
proces	proces	k1gInSc1
se	s	k7c7
vzbouřenci	vzbouřenec	k1gMnPc7
byl	být	k5eAaImAgMnS
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
neobvykle	obvykle	k6eNd1
krutý	krutý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajatci	zajatec	k1gMnPc1
byli	být	k5eAaImAgMnP
upáleni	upálit	k5eAaPmNgMnP
či	či	k8xC
zaživa	zaživa	k6eAd1
rozřezáni	rozřezán	k2eAgMnPc1d1
pilou	pila	k1gFnSc7
nebo	nebo	k8xC
nabodnuti	nabodnut	k2eAgMnPc1d1
na	na	k7c4
kůl	kůl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
vůdci	vůdce	k1gMnSc3
na	na	k7c4
hlavu	hlava	k1gFnSc4
přibili	přibít	k5eAaPmAgMnP
rozžhavenou	rozžhavený	k2eAgFnSc4d1
železnou	železný	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastrašení	zastrašení	k1gNnPc2
Normanů	Norman	k1gMnPc2
bylo	být	k5eAaImAgNnS
díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
morbidnímu	morbidní	k2eAgNnSc3d1
divadlu	divadlo	k1gNnSc3
úspěšné	úspěšný	k2eAgFnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přípravy	příprava	k1gFnPc1
na	na	k7c4
novou	nový	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
do	do	k7c2
Svaté	svatý	k2eAgFnSc2d1
země	zem	k1gFnSc2
byly	být	k5eAaImAgFnP
přerušeny	přerušen	k2eAgMnPc4d1
nečekaným	čekaný	k2eNgInSc7d1
skonem	skon	k1gInSc7
Jindřicha	Jindřich	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
Markward	Markward	k1gMnSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
konfliktu	konflikt	k1gInSc2
s	s	k7c7
královskou	královský	k2eAgFnSc7d1
vdovou	vdova	k1gFnSc7
Konstancií	Konstancie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
normanské	normanský	k2eAgMnPc4d1
krve	krev	k1gFnSc2
a	a	k8xC
jménem	jméno	k1gNnSc7
tříletého	tříletý	k2eAgMnSc2d1
syna	syn	k1gMnSc2
zrušila	zrušit	k5eAaPmAgFnS
vazby	vazba	k1gFnSc2
vytvořené	vytvořený	k2eAgFnSc2d1
zesnulým	zesnulý	k1gMnSc7
manželem	manžel	k1gMnSc7
mezi	mezi	k7c7
vládou	vláda	k1gFnSc7
Sicílie	Sicílie	k1gFnSc2
a	a	k8xC
Říší	říš	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obklopila	obklopit	k5eAaPmAgNnP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
místními	místní	k2eAgMnPc7d1
rádci	rádce	k1gMnPc7
a	a	k8xC
Markwarda	Markwarda	k1gFnSc1
zbavila	zbavit	k5eAaPmAgFnS
moci	moct	k5eAaImF
a	a	k8xC
pokusila	pokusit	k5eAaPmAgFnS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
omezit	omezit	k5eAaPmF
na	na	k7c4
jeho	jeho	k3xOp3gNnSc4
léno	léno	k1gNnSc4
v	v	k7c6
Molise	Molis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1198	#num#	k4
královna	královna	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markward	Markward	k1gMnSc1
se	se	k3xPyFc4
znovu	znovu	k6eAd1
pokusil	pokusit	k5eAaPmAgMnS
získat	získat	k5eAaPmF
post	post	k1gInSc4
regenta	regens	k1gMnSc2
sicilského	sicilský	k2eAgNnSc2d1
království	království	k1gNnSc2
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ovládnout	ovládnout	k5eAaPmF
Palermo	Palermo	k1gNnSc4
a	a	k8xC
získat	získat	k5eAaPmF
do	do	k7c2
péče	péče	k1gFnSc2
osiřelého	osiřelý	k2eAgMnSc2d1
Fridricha	Fridrich	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následný	následný	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
s	s	k7c7
papežem	papež	k1gMnSc7
Inocencem	Inocenc	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
vygradoval	vygradovat	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
proti	proti	k7c3
němu	on	k3xPp3gNnSc3
svatý	svatý	k1gMnSc1
otec	otec	k1gMnSc1
roku	rok	k1gInSc2
1199	#num#	k4
vyhlásil	vyhlásit	k5eAaPmAgInS
křížovou	křížový	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
považovanou	považovaný	k2eAgFnSc4d1
za	za	k7c4
první	první	k4xOgFnSc4
"	"	kIx"
<g/>
politickou	politický	k2eAgFnSc4d1
křížovou	křížový	k2eAgFnSc4d1
kruciátu	kruciáta	k1gFnSc4
<g/>
"	"	kIx"
své	svůj	k3xOyFgFnPc4
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
do	do	k7c2
jejíhož	jejíž	k3xOyRp3gNnSc2
čela	čelo	k1gNnSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
postavil	postavit	k5eAaPmAgMnS
Gautier	Gautier	k1gInSc4
z	z	k7c2
Brienne	Brienn	k1gInSc5
<g/>
,	,	kIx,
zeť	zeť	k1gMnSc1
bývalého	bývalý	k2eAgMnSc2d1
krále	král	k1gMnSc2
Tankreda	Tankred	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečná	válečná	k1gFnSc1
štěstěna	štěstěna	k1gFnSc1
byla	být	k5eAaImAgFnS
vrtkavá	vrtkavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Gautier	Gautier	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
pánem	pán	k1gMnSc7
pevniny	pevnina	k1gFnSc2
a	a	k8xC
tituloval	titulovat	k5eAaImAgMnS
se	s	k7c7
králem	král	k1gMnSc7
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
Markwardovi	Markwardův	k2eAgMnPc1d1
se	s	k7c7
za	za	k7c2
pomoci	pomoc	k1gFnSc2
muslimských	muslimský	k2eAgMnPc2d1
spojenců	spojenec	k1gMnPc2
podařilo	podařit	k5eAaPmAgNnS
udržet	udržet	k5eAaPmF
ostrovní	ostrovní	k2eAgFnSc4d1
Siciílii	Siciílie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c6
úplavici	úplavice	k1gFnSc6
roku	rok	k1gInSc2
1202	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.bookrags.com	www.bookrags.com	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
www	www	k?
<g/>
.	.	kIx.
<g/>
treccani	treccan	k1gMnPc1
<g/>
.	.	kIx.
<g/>
i	i	k9
<g/>
↑	↑	k?
OPLL	OPLL	kA
<g/>
,	,	kIx,
Ferdinand	Ferdinand	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fridrich	Fridrich	k1gMnSc1
Barbarossa	Barbarossa	k1gMnSc1
:	:	kIx,
císař	císař	k1gMnSc1
a	a	k8xC
rytíř	rytíř	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	Paseka	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
354	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
342	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
248	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MUNDY	MUNDY	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
Hine	hin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropa	Evropa	k1gFnSc1
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
1150	#num#	k4
<g/>
-	-	kIx~
<g/>
1300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7021	#num#	k4
<g/>
-	-	kIx~
<g/>
927	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
jen	jen	k9
Evropa	Evropa	k1gFnSc1
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
↑	↑	k?
SOUKUP	Soukup	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc1
křížová	křížový	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
dle	dle	k7c2
kronikáře	kronikář	k1gMnSc2
Ansberta	Ansbert	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbram	Příbram	k1gFnSc1
<g/>
:	:	kIx,
Knihovna	knihovna	k1gFnSc1
Jana	Jan	k1gMnSc2
Drdy	Drda	k1gMnSc2
v	v	k7c6
Příbrami	Příbram	k1gFnSc6
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
151	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86240	#num#	k4
<g/>
-	-	kIx~
<g/>
67	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
126	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VEBER	VEBER	kA
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
Rakouska	Rakousko	k1gNnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
doplněné	doplněný	k2eAgInPc4d1
a	a	k8xC
aktualizované	aktualizovaný	k2eAgInPc4d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
105	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Evropa	Evropa	k1gFnSc1
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
55	#num#	k4
<g/>
↑	↑	k?
TYERMAN	TYERMAN	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svaté	svatý	k2eAgInPc1d1
války	válek	k1gInPc1
:	:	kIx,
dějiny	dějiny	k1gFnPc1
křížových	křížový	k2eAgFnPc2d1
výprav	výprava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
926	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
497	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Thomas	Thomas	k1gMnSc1
Curtis	Curtis	k1gFnSc2
Van	van	k1gInSc1
Cleve	Cleev	k1gFnSc2
<g/>
:	:	kIx,
Markward	Markward	k1gMnSc1
of	of	k?
Anweiler	Anweiler	k1gMnSc1
and	and	k?
the	the	k?
Sicilian	Sicilian	k1gMnSc1
Regency	Regenca	k1gFnSc2
–	–	k?
A	a	k8xC
Study	stud	k1gInPc1
of	of	k?
Hohenstaufen	Hohenstaufen	k1gInSc1
Policy	Polica	k1gMnSc2
in	in	k?
Sicily	Sicil	k1gInPc1
during	during	k1gInSc1
the	the	k?
Minority	minorita	k1gFnSc2
of	of	k?
Frederick	Frederick	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc4
University	universita	k1gFnSc2
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
1937	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Markvart	Markvarta	k1gFnPc2
z	z	k7c2
Annweileru	Annweiler	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118781987	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
18018476	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Středověk	středověk	k1gInSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
