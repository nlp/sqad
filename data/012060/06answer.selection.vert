<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
okupace	okupace	k1gFnSc1	okupace
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
dočasná	dočasný	k2eAgFnSc1d1	dočasná
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
cizím	cizí	k2eAgNnSc7d1	cizí
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
pod	pod	k7c4	pod
formální	formální	k2eAgFnSc4d1	formální
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
okupující	okupující	k2eAgFnSc2d1	okupující
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
.	.	kIx.	.
</s>
