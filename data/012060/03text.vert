<p>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
okupace	okupace	k1gFnSc1	okupace
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
dočasná	dočasný	k2eAgFnSc1d1	dočasná
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
cizím	cizí	k2eAgNnSc7d1	cizí
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
nepřísluší	příslušet	k5eNaImIp3nS	příslušet
pod	pod	k7c4	pod
formální	formální	k2eAgFnSc4d1	formální
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
okupující	okupující	k2eAgFnSc2d1	okupující
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
okupované	okupovaný	k2eAgNnSc1d1	okupované
území	území	k1gNnSc1	území
nebo	nebo	k8xC	nebo
okupační	okupační	k2eAgFnSc1d1	okupační
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnSc2	definice
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
znepřátelených	znepřátelený	k2eAgFnPc2d1	znepřátelená
stran	strana	k1gFnPc2	strana
obsadí	obsadit	k5eAaPmIp3nP	obsadit
část	část	k1gFnSc4	část
území	území	k1gNnSc2	území
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
je	být	k5eAaImIp3nS	být
udržet	udržet	k5eAaPmF	udržet
a	a	k8xC	a
vykonávat	vykonávat	k5eAaImF	vykonávat
zde	zde	k6eAd1	zde
své	svůj	k3xOyFgFnPc4	svůj
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
okupaci	okupace	k1gFnSc6	okupace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
okupaci	okupace	k1gFnSc6	okupace
jde	jít	k5eAaImIp3nS	jít
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
okupující	okupující	k2eAgFnSc1d1	okupující
mocnost	mocnost	k1gFnSc1	mocnost
nesetkává	setkávat	k5eNaImIp3nS	setkávat
s	s	k7c7	s
ozbrojeným	ozbrojený	k2eAgInSc7d1	ozbrojený
odporem	odpor	k1gInSc7	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Svojí	svůj	k3xOyFgFnSc7	svůj
dočasnou	dočasný	k2eAgFnSc7d1	dočasná
podstatou	podstata	k1gFnSc7	podstata
se	se	k3xPyFc4	se
vojenská	vojenský	k2eAgFnSc1d1	vojenská
okupace	okupace	k1gFnSc1	okupace
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
kolonialismu	kolonialismus	k1gInSc2	kolonialismus
nebo	nebo	k8xC	nebo
anexe	anexe	k1gFnSc2	anexe
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
daného	daný	k2eAgNnSc2d1	dané
území	území	k1gNnSc2	území
k	k	k7c3	k
státnímu	státní	k2eAgNnSc3d1	státní
území	území	k1gNnSc3	území
okupační	okupační	k2eAgFnSc2d1	okupační
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
o	o	k7c4	o
prozatímní	prozatímní	k2eAgInSc4d1	prozatímní
výkon	výkon	k1gInSc4	výkon
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Válečné	válečný	k2eAgNnSc4d1	válečné
právo	právo	k1gNnSc4	právo
==	==	k?	==
</s>
</p>
<p>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
anexí	anexe	k1gFnSc7	anexe
a	a	k8xC	a
okupací	okupace	k1gFnSc7	okupace
poprvé	poprvé	k6eAd1	poprvé
formuloval	formulovat	k5eAaImAgMnS	formulovat
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
Emerich	Emerich	k1gMnSc1	Emerich
de	de	k?	de
Vattel	Vattel	k1gMnSc1	Vattel
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Zákon	zákon	k1gInSc1	zákon
národů	národ	k1gInPc2	národ
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozlišení	rozlišení	k1gNnSc1	rozlišení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnPc2	součást
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
po	po	k7c6	po
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zakotveno	zakotvit	k5eAaPmNgNnS	zakotvit
v	v	k7c6	v
Čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
úmluvě	úmluva	k1gFnSc3	úmluva
druhé	druhý	k4xOgFnSc2	druhý
haagské	haagský	k2eAgFnSc2d1	Haagská
konference	konference	k1gFnSc2	konference
a	a	k8xC	a
Čtvrté	čtvrtý	k4xOgFnSc3	čtvrtý
ženevské	ženevský	k2eAgFnSc3d1	Ženevská
úmluvě	úmluva	k1gFnSc3	úmluva
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
předpisy	předpis	k1gInPc1	předpis
také	také	k9	také
stanovily	stanovit	k5eAaPmAgInP	stanovit
základní	základní	k2eAgInPc1d1	základní
pravidla	pravidlo	k1gNnPc4	pravidlo
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Povinností	povinnost	k1gFnSc7	povinnost
okupující	okupující	k2eAgFnSc2d1	okupující
mocnosti	mocnost	k1gFnSc2	mocnost
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
obnovit	obnovit	k5eAaPmF	obnovit
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
v	v	k7c6	v
maximálním	maximální	k2eAgInSc6d1	maximální
možném	možný	k2eAgInSc6d1	možný
rozsahu	rozsah	k1gInSc6	rozsah
veřejný	veřejný	k2eAgInSc4d1	veřejný
pořádek	pořádek	k1gInSc4	pořádek
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
zásobování	zásobování	k1gNnSc4	zásobování
potravinami	potravina	k1gFnPc7	potravina
<g/>
,	,	kIx,	,
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
a	a	k8xC	a
zajistit	zajistit	k5eAaPmF	zajistit
administrativní	administrativní	k2eAgNnSc4d1	administrativní
fungování	fungování	k1gNnSc4	fungování
okupované	okupovaný	k2eAgFnSc2d1	okupovaná
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
Německo	Německo	k1gNnSc4	Německo
Belgii	Belgie	k1gFnSc3	Belgie
<g/>
,	,	kIx,	,
východ	východ	k1gInSc4	východ
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
západ	západ	k1gInSc1	západ
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc1	Rakousko-Uhersko
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
Srbsko	Srbsko	k1gNnSc4	Srbsko
a	a	k8xC	a
Černou	černý	k2eAgFnSc4d1	černá
Horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
většinu	většina	k1gFnSc4	většina
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
o	o	k7c4	o
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
okupaci	okupace	k1gFnSc4	okupace
šlo	jít	k5eAaImAgNnS	jít
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
částí	část	k1gFnSc7	část
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnPc1d1	ostatní
území	území	k1gNnPc1	území
byla	být	k5eAaImAgNnP	být
pod	pod	k7c7	pod
civilní	civilní	k2eAgFnSc7d1	civilní
správou	správa	k1gFnSc7	správa
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Česko	Česko	k1gNnSc1	Česko
nebo	nebo	k8xC	nebo
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
vládly	vládnout	k5eAaImAgInP	vládnout
loutkové	loutkový	k2eAgInPc1d1	loutkový
režimy	režim	k1gInPc1	režim
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vichistická	vichistický	k2eAgFnSc1d1	vichistická
Francie	Francie	k1gFnSc1	Francie
nebo	nebo	k8xC	nebo
ustašovské	ustašovský	k2eAgNnSc1d1	ustašovské
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
Spojenci	spojenec	k1gMnSc3	spojenec
okupováno	okupován	k2eAgNnSc1d1	okupováno
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
a	a	k8xC	a
také	také	k9	také
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Military	Militara	k1gFnSc2	Militara
occupation	occupation	k1gInSc1	occupation
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vojenská	vojenský	k2eAgFnSc1d1	vojenská
okupace	okupace	k1gFnSc1	okupace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
okupace	okupace	k1gFnSc2	okupace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Okkupace	Okkupace	k1gFnSc1	Okkupace
(	(	kIx(	(
<g/>
vojenství	vojenství	k1gNnSc1	vojenství
<g/>
)	)	kIx)	)
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
