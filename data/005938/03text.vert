<s>
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
překladatel	překladatel	k1gMnSc1	překladatel
převážně	převážně	k6eAd1	převážně
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
science	science	k1gFnPc1	science
fiction	fiction	k1gInSc1	fiction
<g/>
,	,	kIx,	,
komiksů	komiks	k1gInPc2	komiks
a	a	k8xC	a
westernů	western	k1gInPc2	western
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc7	jeho
nejznámějšími	známý	k2eAgInPc7d3	nejznámější
překlady	překlad	k1gInPc7	překlad
jsou	být	k5eAaImIp3nP	být
cyklus	cyklus	k1gInSc4	cyklus
Zeměplocha	Zeměploch	k1gMnSc2	Zeměploch
od	od	k7c2	od
Terryho	Terry	k1gMnSc2	Terry
Pratchetta	Pratchett	k1gMnSc2	Pratchett
a	a	k8xC	a
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
barbaru	barbar	k1gMnSc6	barbar
Conanovi	Conan	k1gMnSc6	Conan
od	od	k7c2	od
R.	R.	kA	R.
E.	E.	kA	E.
Howarda	Howard	k1gMnSc2	Howard
i	i	k9	i
jeho	jeho	k3xOp3gMnPc2	jeho
pokračovatelů	pokračovatel	k1gMnPc2	pokračovatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
děti	dítě	k1gFnPc4	dítě
<g/>
;	;	kIx,	;
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
technický	technický	k2eAgMnSc1d1	technický
redaktor	redaktor	k1gMnSc1	redaktor
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Artia	Artium	k1gNnSc2	Artium
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
odbytu	odbyt	k1gInSc2	odbyt
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Aventinum	Aventinum	k1gInSc1	Aventinum
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
překladatelem	překladatel	k1gMnSc7	překladatel
na	na	k7c6	na
volné	volný	k2eAgFnSc6d1	volná
noze	noha	k1gFnSc6	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
obnově	obnova	k1gFnSc6	obnova
Klubu	klub	k1gInSc2	klub
Julese	Julese	k1gFnSc1	Julese
Vernea	Vernea	k1gFnSc1	Vernea
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jehož	jehož	k3xOyRp3gInPc4	jehož
fanziny	fanzin	k1gInPc4	fanzin
brzy	brzy	k6eAd1	brzy
začal	začít	k5eAaPmAgInS	začít
překládat	překládat	k5eAaImF	překládat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
několik	několik	k4yIc4	několik
krátkých	krátké	k1gNnPc2	krátké
Howardových	Howardův	k2eAgFnPc2d1	Howardova
povídek	povídka	k1gFnPc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc7	jeho
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sběratelem	sběratel	k1gMnSc7	sběratel
komiksů	komiks	k1gInPc2	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
i	i	k9	i
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
svého	svůj	k1gMnSc2	svůj
vydavatele	vydavatel	k1gMnSc2	vydavatel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Talaše	Talaš	k1gMnSc2	Talaš
anglicky	anglicky	k6eAd1	anglicky
umí	umět	k5eAaImIp3nS	umět
číst	číst	k5eAaImF	číst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
našim	náš	k3xOp1gMnPc3	náš
nejpopulárnějším	populární	k2eAgMnPc3d3	nejpopulárnější
překladatelům	překladatel	k1gMnPc3	překladatel
jak	jak	k8xS	jak
u	u	k7c2	u
fanoušků	fanoušek	k1gMnPc2	fanoušek
SF	SF	kA	SF
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
širší	široký	k2eAgFnSc6d2	širší
čtenářské	čtenářský	k2eAgFnSc6d1	čtenářská
veřejnosti	veřejnost	k1gFnSc6	veřejnost
díky	díky	k7c3	díky
vynalézavosti	vynalézavost	k1gFnSc3	vynalézavost
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
převádí	převádět	k5eAaImIp3nP	převádět
slovní	slovní	k2eAgFnPc1d1	slovní
hříčky	hříčka	k1gFnPc1	hříčka
v	v	k7c4	v
Zeměploše	Zeměploš	k1gMnPc4	Zeměploš
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ve	v	k7c6	v
fandomu	fandom	k1gInSc6	fandom
je	být	k5eAaImIp3nS	být
oblíben	oblíben	k2eAgInSc1d1	oblíben
rovněž	rovněž	k9	rovněž
díky	díky	k7c3	díky
svému	svůj	k3xOyFgNnSc3	svůj
předčítání	předčítání	k1gNnSc1	předčítání
z	z	k7c2	z
připravovaných	připravovaný	k2eAgFnPc2d1	připravovaná
Pratchettových	Pratchettův	k2eAgFnPc2d1	Pratchettova
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
občasným	občasný	k2eAgInPc3d1	občasný
jiným	jiný	k2eAgInPc3d1	jiný
pořadům	pořad	k1gInPc3	pořad
na	na	k7c4	na
conech	conech	k1gInSc4	conech
<g/>
;	;	kIx,	;
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
sklidil	sklidit	k5eAaPmAgMnS	sklidit
v	v	k7c6	v
roli	role	k1gFnSc6	role
Knihovníka	knihovník	k1gMnSc2	knihovník
v	v	k7c6	v
ochotnických	ochotnický	k2eAgFnPc6d1	ochotnická
inscenacích	inscenace	k1gFnPc6	inscenace
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
ze	z	k7c2	z
Zeměplochy	Zeměploch	k1gMnPc4	Zeměploch
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
jedinečně	jedinečně	k6eAd1	jedinečně
fyzicky	fyzicky	k6eAd1	fyzicky
disponován	disponován	k2eAgMnSc1d1	disponován
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Získal	získat	k5eAaPmAgMnS	získat
ceny	cena	k1gFnPc4	cena
Akademie	akademie	k1gFnSc2	akademie
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc1	fantas
a	a	k8xC	a
hororu	horor	k1gInSc2	horor
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
překladatele	překladatel	k1gMnSc4	překladatel
let	let	k1gInSc4	let
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
97	[number]	k4	97
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
v	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
2003	[number]	k4	2003
také	také	k6eAd1	také
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
SF	SF	kA	SF
<g/>
.	.	kIx.	.
</s>
<s>
Menšina	menšina	k1gFnSc1	menšina
čtenářů	čtenář	k1gMnPc2	čtenář
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
jeho	jeho	k3xOp3gFnPc4	jeho
věcné	věcný	k2eAgFnPc4d1	věcná
chyby	chyba	k1gFnPc4	chyba
a	a	k8xC	a
anglicismy	anglicismus	k1gInPc4	anglicismus
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Přeložil	přeložit	k5eAaPmAgMnS	přeložit
ke	k	k7c3	k
stovce	stovka	k1gFnSc3	stovka
titulů	titul	k1gInPc2	titul
<g/>
;	;	kIx,	;
autoři	autor	k1gMnPc1	autor
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
soustavněji	soustavně	k6eAd2	soustavně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
kromě	kromě	k7c2	kromě
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
<g/>
:	:	kIx,	:
Poul	Poul	k1gMnSc1	Poul
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
Piers	Piers	k1gInSc1	Piers
Anthony	Anthona	k1gFnSc2	Anthona
(	(	kIx(	(
<g/>
dva	dva	k4xCgInPc1	dva
díly	díl	k1gInPc1	díl
humoristické	humoristický	k2eAgInPc1d1	humoristický
fantasy	fantas	k1gInPc1	fantas
Xanth	Xantha	k1gFnPc2	Xantha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Rice	Ric	k1gFnSc2	Ric
Burroughs	Burroughs	k1gInSc1	Burroughs
(	(	kIx(	(
<g/>
cykly	cyklus	k1gInPc4	cyklus
John	John	k1gMnSc1	John
Carter	Carter	k1gMnSc1	Carter
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
pán	pán	k1gMnSc1	pán
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Pellucidar	Pellucidara	k1gFnPc2	Pellucidara
a	a	k8xC	a
několik	několik	k4yIc1	několik
pozdních	pozdní	k2eAgInPc2d1	pozdní
titulů	titul	k1gInPc2	titul
o	o	k7c6	o
Tarzanovi	Tarzan	k1gMnSc6	Tarzan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lyon	Lyon	k1gInSc1	Lyon
Sprague	Spragu	k1gFnSc2	Spragu
de	de	k?	de
Camp	camp	k1gInSc1	camp
<g/>
,	,	kIx,	,
Lin	Lina	k1gFnPc2	Lina
Carter	Cartra	k1gFnPc2	Cartra
<g/>
,	,	kIx,	,
Lester	Lester	k1gMnSc1	Lester
Del	Del	k1gFnSc2	Del
Rey	Rea	k1gFnSc2	Rea
(	(	kIx(	(
<g/>
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
tří	tři	k4xCgFnPc2	tři
zejména	zejména	k6eAd1	zejména
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gMnSc4	jejich
conanovská	conanovský	k2eAgNnPc1d1	conanovský
díla	dílo	k1gNnPc1	dílo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
westernů	western	k1gInPc2	western
Louis	Louis	k1gMnSc1	Louis
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
<g/>
,	,	kIx,	,
Roger	Roger	k1gMnSc1	Roger
Zelazny	Zelazna	k1gFnSc2	Zelazna
a	a	k8xC	a
komiks	komiks	k1gInSc1	komiks
Sláine	Sláin	k1gInSc5	Sláin
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc4	Commons
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Kantůrek	kantůrek	k1gMnSc1	kantůrek
</s>
