<p>
<s>
Myšivka	myšivka	k1gFnSc1	myšivka
horská	horský	k2eAgFnSc1d1	horská
(	(	kIx(	(
<g/>
Sicista	Sicist	k1gMnSc2	Sicist
betulina	betulin	k2eAgMnSc2d1	betulin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
tarbíkovitých	tarbíkovitý	k2eAgInPc2d1	tarbíkovitý
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
fauny	fauna	k1gFnSc2	fauna
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vzácného	vzácný	k2eAgMnSc4d1	vzácný
savce	savec	k1gMnSc4	savec
žijícího	žijící	k2eAgNnSc2d1	žijící
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k9	i
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
chráněná	chráněný	k2eAgFnSc1d1	chráněná
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
Bernskou	bernský	k2eAgFnSc7d1	Bernská
úmluvou	úmluva	k1gFnSc7	úmluva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
cm	cm	kA	cm
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
5	[number]	k4	5
až	až	k9	až
13	[number]	k4	13
g.	g.	k?	g.
</s>
</p>
<p>
<s>
Ocásek	ocásek	k1gInSc1	ocásek
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
tělíčko	tělíčko	k1gNnSc1	tělíčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myšivka	myšivka	k1gFnSc1	myšivka
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
nápadná	nápadný	k2eAgNnPc4d1	nápadné
tmavým	tmavý	k2eAgInSc7d1	tmavý
podélným	podélný	k2eAgInSc7d1	podélný
pruhem	pruh	k1gInSc7	pruh
od	od	k7c2	od
špičky	špička	k1gFnSc2	špička
ocásku	ocásek	k1gInSc2	ocásek
až	až	k9	až
po	po	k7c4	po
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
zbarvena	zbarvit	k5eAaPmNgFnS	zbarvit
okrově	okrově	k6eAd1	okrově
<g/>
,	,	kIx,	,
na	na	k7c6	na
bříšku	bříško	k1gNnSc6	bříško
šedavě	šedavě	k6eAd1	šedavě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zadní	zadní	k2eAgFnPc1d1	zadní
končetiny	končetina	k1gFnPc1	končetina
jsou	být	k5eAaImIp3nP	být
mohutněji	mohutně	k6eAd2	mohutně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
než	než	k8xS	než
přední	přední	k2eAgInPc1d1	přední
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
jako	jako	k8xC	jako
u	u	k7c2	u
tarbíků	tarbík	k1gMnPc2	tarbík
<g/>
)	)	kIx)	)
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
jsou	být	k5eAaImIp3nP	být
prsty	prst	k1gInPc1	prst
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tlamičce	tlamička	k1gFnSc6	tlamička
mají	mít	k5eAaImIp3nP	mít
před	před	k7c7	před
přední	přední	k2eAgFnSc7d1	přední
stoličkou	stolička	k1gFnSc7	stolička
navíc	navíc	k6eAd1	navíc
jeden	jeden	k4xCgInSc4	jeden
zakrnělý	zakrnělý	k2eAgInSc4d1	zakrnělý
zub	zub	k1gInSc4	zub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horní	horní	k2eAgInSc1d1	horní
ret	ret	k1gInSc1	ret
není	být	k5eNaImIp3nS	být
rozštěpen	rozštěpit	k5eAaPmNgInS	rozštěpit
jako	jako	k9	jako
u	u	k7c2	u
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
všežravec	všežravec	k1gMnSc1	všežravec
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
travními	travní	k2eAgNnPc7d1	travní
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
bobulemi	bobule	k1gFnPc7	bobule
nebo	nebo	k8xC	nebo
drobným	drobný	k2eAgInSc7d1	drobný
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Místa	místo	k1gNnSc2	místo
jejího	její	k3xOp3gInSc2	její
výskytu	výskyt	k1gInSc2	výskyt
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
až	až	k9	až
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Areál	areál	k1gInSc1	areál
jejího	její	k3xOp3gInSc2	její
výskytu	výskyt	k1gInSc2	výskyt
bývá	bývat	k5eAaImIp3nS	bývat
udáván	udávat	k5eAaImNgInS	udávat
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
rozmezí	rozmezit	k5eAaPmIp3nS	rozmezit
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
až	až	k9	až
1900	[number]	k4	1900
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
myšivka	myšivka	k1gFnSc1	myšivka
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
nad	nad	k7c7	nad
horní	horní	k2eAgFnSc7d1	horní
hranicí	hranice	k1gFnSc7	hranice
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
vlhčím	vlhký	k2eAgNnPc3d2	vlhčí
místům	místo	k1gNnPc3	místo
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
spatřit	spatřit	k5eAaPmF	spatřit
na	na	k7c6	na
rašeliništích	rašeliniště	k1gNnPc6	rašeliniště
a	a	k8xC	a
horských	horský	k2eAgFnPc6d1	horská
loukách	louka	k1gFnPc6	louka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	V	kA	V
ČR	ČR	kA	ČR
její	její	k3xOp3gInSc4	její
výskyt	výskyt	k1gInSc4	výskyt
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
poměrně	poměrně	k6eAd1	poměrně
nedávno	nedávno	k6eAd1	nedávno
(	(	kIx(	(
<g/>
až	až	k9	až
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
<g/>
,	,	kIx,	,
na	na	k7c6	na
Králickém	králický	k2eAgInSc6d1	králický
Sněžníku	Sněžník	k1gInSc6	Sněžník
nebo	nebo	k8xC	nebo
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
a	a	k8xC	a
v	v	k7c6	v
Novohradských	novohradský	k2eAgFnPc6d1	Novohradská
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Hojnější	hojný	k2eAgFnSc1d2	hojnější
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tvor	tvor	k1gMnSc1	tvor
aktivní	aktivní	k2eAgMnSc1d1	aktivní
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ale	ale	k9	ale
spatřit	spatřit	k5eAaPmF	spatřit
i	i	k9	i
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokáže	dokázat	k5eAaPmIp3nS	dokázat
i	i	k9	i
obratně	obratně	k6eAd1	obratně
šplhat	šplhat	k5eAaImF	šplhat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
ocasu	ocas	k1gInSc2	ocas
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
vegetaci	vegetace	k1gFnSc6	vegetace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letní	letní	k2eAgNnSc1d1	letní
hnízdo	hnízdo	k1gNnSc1	hnízdo
si	se	k3xPyFc3	se
staví	stavit	k5eAaPmIp3nS	stavit
pod	pod	k7c7	pod
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kořeny	kořen	k1gInPc7	kořen
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
pařezů	pařez	k1gInPc2	pařez
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
nebo	nebo	k8xC	nebo
mechu	mech	k1gInSc2	mech
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vychovává	vychovávat	k5eAaImIp3nS	vychovávat
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
sezónu	sezóna	k1gFnSc4	sezóna
2	[number]	k4	2
až	až	k9	až
7	[number]	k4	7
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hnízdo	hnízdo	k1gNnSc1	hnízdo
mívá	mívat	k5eAaImIp3nS	mívat
často	často	k6eAd1	často
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
místnosti	místnost	k1gFnPc4	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgNnSc4d1	zimní
hnízdo	hnízdo	k1gNnSc4	hnízdo
si	se	k3xPyFc3	se
myšivka	myšivka	k1gFnSc1	myšivka
vyhrabává	vyhrabávat	k5eAaImIp3nS	vyhrabávat
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
období	období	k1gNnSc2	období
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
května	květen	k1gInSc2	květen
přečká	přečkat	k5eAaPmIp3nS	přečkat
stočená	stočený	k2eAgFnSc1d1	stočená
v	v	k7c6	v
klubíčku	klubíčko	k1gNnSc6	klubíčko
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
hibernace	hibernace	k1gFnSc2	hibernace
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
byl	být	k5eAaImAgInS	být
někdy	někdy	k6eAd1	někdy
pozorován	pozorovat	k5eAaImNgInS	pozorovat
i	i	k9	i
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
zhoršených	zhoršený	k2eAgFnPc6d1	zhoršená
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Záměna	záměna	k1gFnSc1	záměna
==	==	k?	==
</s>
</p>
<p>
<s>
Myšivka	myšivka	k1gFnSc1	myšivka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zaměněna	zaměnit	k5eAaPmNgFnS	zaměnit
s	s	k7c7	s
myšicí	myšice	k1gFnSc7	myšice
temnopásou	temnopása	k1gFnSc7	temnopása
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgInSc1d1	tmavý
pruh	pruh	k1gInSc1	pruh
jí	on	k3xPp3gFnSc2	on
začíná	začínat	k5eAaImIp3nS	začínat
až	až	k9	až
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kratší	krátký	k2eAgInSc4d2	kratší
ocas	ocas	k1gInSc4	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
(	(	kIx(	(
<g/>
jen	jen	k9	jen
asi	asi	k9	asi
do	do	k7c2	do
1000	[number]	k4	1000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejpodobnější	podobný	k2eAgFnSc7d3	nejpodobnější
příbuznou	příbuzná	k1gFnSc7	příbuzná
je	být	k5eAaImIp3nS	být
myšivka	myšivka	k1gFnSc1	myšivka
stepní	stepní	k2eAgFnSc1d1	stepní
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
myšivka	myšivka	k1gFnSc1	myšivka
horská	horský	k2eAgFnSc1d1	horská
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
