<s>
Lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
(	(	kIx(	(
<g/>
Lemmus	Lemmus	k1gMnSc1	Lemmus
lemmus	lemmus	k1gMnSc1	lemmus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc4d1	běžný
druh	druh	k1gInSc4	druh
lumíka	lumík	k1gMnSc2	lumík
endemický	endemický	k2eAgInSc1d1	endemický
pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
populace	populace	k1gFnSc1	populace
bychom	by	kYmCp1nP	by
pak	pak	k6eAd1	pak
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
tundry	tundra	k1gFnPc1	tundra
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
ostřicemi	ostřice	k1gFnPc7	ostřice
<g/>
,	,	kIx,	,
travinami	travina	k1gFnPc7	travina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mechem	mech	k1gInSc7	mech
<g/>
.	.	kIx.	.
</s>

