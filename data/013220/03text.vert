<p>
<s>
Lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
(	(	kIx(	(
<g/>
Lemmus	Lemmus	k1gMnSc1	Lemmus
lemmus	lemmus	k1gMnSc1	lemmus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc4d1	běžný
druh	druh	k1gInSc4	druh
lumíka	lumík	k1gMnSc2	lumík
endemický	endemický	k2eAgInSc1d1	endemický
pro	pro	k7c4	pro
severní	severní	k2eAgFnSc4d1	severní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
populace	populace	k1gFnSc1	populace
bychom	by	kYmCp1nP	by
pak	pak	k6eAd1	pak
našli	najít	k5eAaPmAgMnP	najít
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
tundry	tundra	k1gFnPc1	tundra
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
skal	skála	k1gFnPc2	skála
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
především	především	k9	především
ostřicemi	ostřice	k1gFnPc7	ostřice
<g/>
,	,	kIx,	,
travinami	travina	k1gFnPc7	travina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mechem	mech	k1gInSc7	mech
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
aktivní	aktivní	k2eAgInPc4d1	aktivní
hlavně	hlavně	k6eAd1	hlavně
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
střídají	střídat	k5eAaImIp3nP	střídat
spánek	spánek	k1gInSc4	spánek
a	a	k8xC	a
občasné	občasný	k2eAgFnPc4d1	občasná
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomie	taxonomie	k1gFnSc2	taxonomie
a	a	k8xC	a
populace	populace	k1gFnSc2	populace
==	==	k?	==
</s>
</p>
<p>
<s>
Lumíka	lumík	k1gMnSc4	lumík
norského	norský	k2eAgInSc2d1	norský
poprvé	poprvé	k6eAd1	poprvé
popsal	popsat	k5eAaPmAgMnS	popsat
Linnaeus	Linnaeus	k1gMnSc1	Linnaeus
roku	rok	k1gInSc2	rok
1758	[number]	k4	1758
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dospělého	dospělý	k1gMnSc4	dospělý
jedince	jedinec	k1gMnSc4	jedinec
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
ale	ale	k9	ale
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
starý	starý	k2eAgInSc4d1	starý
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
i	i	k9	i
Pleistocén	pleistocén	k1gInSc4	pleistocén
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
blízkými	blízký	k2eAgMnPc7d1	blízký
příbuznými	příbuzný	k1gMnPc7	příbuzný
jsou	být	k5eAaImIp3nP	být
lumík	lumík	k1gMnSc1	lumík
amurský	amurský	k2eAgMnSc1d1	amurský
(	(	kIx(	(
<g/>
Lemmus	Lemmus	k1gInSc1	Lemmus
amurensis	amurensis	k1gFnSc2	amurensis
<g/>
)	)	kIx)	)
a	a	k8xC	a
lumík	lumík	k1gMnSc1	lumík
sibiřský	sibiřský	k2eAgMnSc1d1	sibiřský
(	(	kIx(	(
<g/>
Lemmus	Lemmus	k1gMnSc1	Lemmus
sibiricus	sibiricus	k1gMnSc1	sibiricus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
panuje	panovat	k5eAaImIp3nS	panovat
i	i	k9	i
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
lumíci	lumík	k1gMnPc1	lumík
sibiřští	sibiřský	k2eAgMnPc1d1	sibiřský
jsou	být	k5eAaImIp3nP	být
předky	předek	k1gMnPc4	předek
těch	ten	k3xDgFnPc2	ten
norských	norský	k2eAgFnPc2d1	norská
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k8xC	ale
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýz	analýza	k1gFnPc2	analýza
DNA	DNA	kA	DNA
není	být	k5eNaImIp3nS	být
pravda	pravda	k1gFnSc1	pravda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Populace	populace	k1gFnSc1	populace
lumíků	lumík	k1gMnPc2	lumík
norských	norský	k2eAgInPc2d1	norský
má	mít	k5eAaImIp3nS	mít
obecně	obecně	k6eAd1	obecně
čtyřleté	čtyřletý	k2eAgInPc4d1	čtyřletý
cykly	cyklus	k1gInPc4	cyklus
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
populace	populace	k1gFnSc1	populace
udržitelná	udržitelný	k2eAgFnSc1d1	udržitelná
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
rok	rok	k1gInSc4	rok
ale	ale	k8xC	ale
dramaticky	dramaticky	k6eAd1	dramaticky
narůstá	narůstat	k5eAaImIp3nS	narůstat
a	a	k8xC	a
hned	hned	k6eAd1	hned
zase	zase	k9	zase
upadá	upadat	k5eAaImIp3nS	upadat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tak	tak	k9	tak
ale	ale	k9	ale
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
druh	druh	k1gInSc4	druh
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgMnSc1d1	dotčený
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nechráněný	chráněný	k2eNgInSc4d1	nechráněný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
je	být	k5eAaImIp3nS	být
drobný	drobný	k2eAgMnSc1d1	drobný
hlodavec	hlodavec	k1gMnSc1	hlodavec
s	s	k7c7	s
nevýrazným	výrazný	k2eNgInSc7d1	nevýrazný
pohlavním	pohlavní	k2eAgInSc7d1	pohlavní
dimorfismem	dimorfismus	k1gInSc7	dimorfismus
<g/>
.	.	kIx.	.
</s>
<s>
Samce	samec	k1gInPc1	samec
od	od	k7c2	od
samice	samice	k1gFnSc2	samice
lze	lze	k6eAd1	lze
rozeznat	rozeznat	k5eAaPmF	rozeznat
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
<g/>
,	,	kIx,	,
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
měří	měřit	k5eAaImIp3nS	měřit
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
okolo	okolo	k7c2	okolo
155	[number]	k4	155
mm	mm	kA	mm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
do	do	k7c2	do
130	[number]	k4	130
g	g	kA	g
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
stravě	strava	k1gFnSc6	strava
a	a	k8xC	a
jejím	její	k3xOp3gNnSc7	její
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
10	[number]	k4	10
až	až	k8xS	až
19	[number]	k4	19
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
tělo	tělo	k1gNnSc1	tělo
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
řidší	řídký	k2eAgNnSc1d2	řidší
<g/>
,	,	kIx,	,
na	na	k7c4	na
dotek	dotek	k1gInSc4	dotek
hrubá	hrubý	k2eAgFnSc1d1	hrubá
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mívá	mívat	k5eAaImIp3nS	mívat
žluto-černé	žluto-černý	k2eAgNnSc4d1	žluto-černé
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
tráví	trávit	k5eAaImIp3nS	trávit
zimu	zima	k1gFnSc4	zima
v	v	k7c6	v
norách	nora	k1gFnPc6	nora
pod	pod	k7c7	pod
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jarního	jarní	k2eAgNnSc2d1	jarní
tání	tání	k1gNnSc2	tání
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stěhuje	stěhovat	k5eAaImIp3nS	stěhovat
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
poloh	poloha	k1gFnPc2	poloha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
sníh	sníh	k1gInSc1	sníh
stále	stále	k6eAd1	stále
dostatečně	dostatečně	k6eAd1	dostatečně
pevný	pevný	k2eAgInSc1d1	pevný
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
podmínek	podmínka	k1gFnPc2	podmínka
migrují	migrovat	k5eAaImIp3nP	migrovat
i	i	k9	i
na	na	k7c6	na
velké	velký	k2eAgFnSc6d1	velká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
vědecky	vědecky	k6eAd1	vědecky
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
lumíci	lumík	k1gMnPc1	lumík
již	již	k6eAd1	již
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
měsíci	měsíc	k1gInSc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
tak	tak	k9	tak
velké	velká	k1gFnPc1	velká
množství	množství	k1gNnSc2	množství
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stáří	stář	k1gFnPc2	stář
jednoho	jeden	k4xCgInSc2	jeden
měsíce	měsíc	k1gInSc2	měsíc
pak	pak	k6eAd1	pak
každá	každý	k3xTgFnSc1	každý
samička	samička	k1gFnSc1	samička
porodí	porodit	k5eAaPmIp3nS	porodit
vrh	vrh	k1gInSc4	vrh
o	o	k7c6	o
šesti	šest	k4xCc6	šest
až	až	k9	až
osmi	osm	k4xCc2	osm
mladých	mladý	k1gMnPc2	mladý
každé	každý	k3xTgInPc4	každý
tři	tři	k4xCgInPc4	tři
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
mláďat	mládě	k1gNnPc2	mládě
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedožije	dožít	k5eNaPmIp3nS	dožít
ani	ani	k8xC	ani
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
a	a	k8xC	a
slabší	slabý	k2eAgMnPc1d2	slabší
jedinci	jedinec	k1gMnPc1	jedinec
umírají	umírat	k5eAaImIp3nP	umírat
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Norway	Norwaa	k1gFnSc2	Norwaa
lemming	lemming	k1gInSc1	lemming
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lumík	lumík	k1gMnSc1	lumík
norský	norský	k2eAgMnSc1d1	norský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Lemmus	Lemmus	k1gInSc1	Lemmus
lemmus	lemmus	k1gInSc1	lemmus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
