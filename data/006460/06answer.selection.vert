<s>
Standardní	standardní	k2eAgFnPc1d1	standardní
malé	malý	k2eAgFnPc1d1	malá
zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kuřácké	kuřácký	k2eAgInPc1d1	kuřácký
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
cca	cca	kA	cca
4	[number]	k4	4
cm	cm	kA	cm
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
papírových	papírový	k2eAgFnPc6d1	papírová
krabičkách	krabička	k1gFnPc6	krabička
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
x	x	k?	x
3,5	[number]	k4	3,5
x	x	k?	x
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
se	s	k7c7	s
škrtátkem	škrtátko	k1gNnSc7	škrtátko
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
krabičky	krabička	k1gFnSc2	krabička
<g/>
.	.	kIx.	.
</s>
