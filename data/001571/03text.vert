<s>
Erwin	Erwin	k1gMnSc1	Erwin
Rudolf	Rudolf	k1gMnSc1	Rudolf
Josef	Josef	k1gMnSc1	Josef
Alexander	Alexandra	k1gFnPc2	Alexandra
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1887	[number]	k4	1887
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
především	především	k9	především
formulací	formulace	k1gFnPc2	formulace
nerelativistické	relativistický	k2eNgFnSc2d1	nerelativistická
vlnové	vlnový	k2eAgFnSc2d1	vlnová
rovnice	rovnice	k1gFnSc2	rovnice
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
hmotných	hmotný	k2eAgFnPc2d1	hmotná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
nazýváme	nazývat	k5eAaImIp1nP	nazývat
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
základní	základní	k2eAgFnSc4d1	základní
práci	práce	k1gFnSc4	práce
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
společně	společně	k6eAd1	společně
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Diracem	Dirace	k1gMnSc7	Dirace
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
proslulého	proslulý	k2eAgInSc2d1	proslulý
myšlenkového	myšlenkový	k2eAgInSc2d1	myšlenkový
experimentu	experiment	k1gInSc2	experiment
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
asi	asi	k9	asi
nejlépe	dobře	k6eAd3	dobře
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
Schrödingerův	Schrödingerův	k2eAgInSc1d1	Schrödingerův
(	(	kIx(	(
<g/>
a	a	k8xC	a
také	také	k9	také
mnoha	mnoho	k4c2	mnoho
dalších	další	k1gNnPc2	další
<g/>
)	)	kIx)	)
skeptický	skeptický	k2eAgInSc1d1	skeptický
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
obecně	obecně	k6eAd1	obecně
přijímané	přijímaný	k2eAgFnSc3d1	přijímaná
pravděpodobnostní	pravděpodobnostní	k2eAgFnSc3d1	pravděpodobnostní
interpretaci	interpretace	k1gFnSc3	interpretace
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Erwin	Erwin	k1gMnSc1	Erwin
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
přispěl	přispět	k5eAaPmAgMnS	přispět
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
nejen	nejen	k6eAd1	nejen
ke	k	k7c3	k
kvantové	kvantový	k2eAgFnSc3d1	kvantová
fyzice	fyzika	k1gFnSc3	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
též	též	k9	též
historií	historie	k1gFnSc7	historie
a	a	k8xC	a
filozofií	filozofie	k1gFnSc7	filozofie
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
obecně	obecně	k6eAd1	obecně
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
What	Whatum	k1gNnPc2	Whatum
is	is	k?	is
Life	Lif	k1gFnSc2	Lif
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
,	,	kIx,	,
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
stáli	stát	k5eAaImAgMnP	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
mezi	mezi	k7c7	mezi
kterými	který	k3yRgInPc7	který
nechyběli	chybět	k5eNaImAgMnP	chybět
objevitelé	objevitel	k1gMnPc1	objevitel
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
Francis	Francis	k1gFnSc1	Francis
Crick	Crick	k1gMnSc1	Crick
či	či	k8xC	či
James	James	k1gMnSc1	James
D.	D.	kA	D.
Watson	Watson	k1gMnSc1	Watson
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
za	za	k7c4	za
Rakouska-Uherska	Rakouska-Uhersko	k1gNnPc4	Rakouska-Uhersko
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
jako	jako	k9	jako
jedináček	jedináček	k1gMnSc1	jedináček
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
majitelem	majitel	k1gMnSc7	majitel
továrny	továrna	k1gFnSc2	továrna
<g/>
,	,	kIx,	,
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
italskému	italský	k2eAgNnSc3d1	italské
umění	umění	k1gNnSc3	umění
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
i	i	k8xC	i
teta	teta	k1gFnSc1	teta
chlapce	chlapec	k1gMnSc2	chlapec
zbožnovaly	zbožnovat	k5eAaBmAgFnP	zbožnovat
a	a	k8xC	a
Erwin	Erwin	k1gMnSc1	Erwin
prožil	prožít	k5eAaPmAgMnS	prožít
ideální	ideální	k2eAgNnSc4d1	ideální
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
měl	mít	k5eAaImAgInS	mít
soukromé	soukromý	k2eAgMnPc4d1	soukromý
učitele	učitel	k1gMnPc4	učitel
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
akademické	akademický	k2eAgNnSc4d1	akademické
gymnázium	gymnázium	k1gNnSc4	gymnázium
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Vídeňskou	vídeňský	k2eAgFnSc4d1	Vídeňská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
ho	on	k3xPp3gMnSc4	on
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
přednášky	přednáška	k1gFnPc1	přednáška
Fridericha	Friderich	k1gMnSc2	Friderich
Hasenöhrla	Hasenöhrl	k1gMnSc2	Hasenöhrl
z	z	k7c2	z
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
ho	on	k3xPp3gMnSc4	on
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
algebra	algebra	k1gFnSc1	algebra
přednášená	přednášený	k2eAgFnSc1d1	přednášená
Franzem	Franz	k1gMnSc7	Franz
Mertensenem	Mertensen	k1gMnSc7	Mertensen
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
studoval	studovat	k5eAaImAgMnS	studovat
teorii	teorie	k1gFnSc4	teorie
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
diferenciální	diferenciální	k2eAgFnPc4d1	diferenciální
rovnice	rovnice	k1gFnPc4	rovnice
a	a	k8xC	a
matematickou	matematický	k2eAgFnSc4d1	matematická
statistiku	statistika	k1gFnSc4	statistika
u	u	k7c2	u
Wilhelma	Wilhelmum	k1gNnSc2	Wilhelmum
Wirtinhera	Wirtinhero	k1gNnSc2	Wirtinhero
a	a	k8xC	a
projektivní	projektivní	k2eAgFnSc6d1	projektivní
geometrii	geometrie	k1gFnSc6	geometrie
<g/>
,	,	kIx,	,
algebraické	algebraický	k2eAgFnPc4d1	algebraická
křivky	křivka	k1gFnPc4	křivka
a	a	k8xC	a
spojité	spojitý	k2eAgFnPc1d1	spojitá
grupy	grupa	k1gFnPc1	grupa
v	v	k7c6	v
přednáškách	přednáška	k1gFnPc6	přednáška
Gustava	Gustav	k1gMnSc2	Gustav
Kohna	Kohn	k1gMnSc2	Kohn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
fyzice	fyzika	k1gFnSc6	fyzika
studoval	studovat	k5eAaImAgMnS	studovat
analytickou	analytický	k2eAgFnSc4d1	analytická
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
aplikace	aplikace	k1gFnPc4	aplikace
parciálních	parciální	k2eAgFnPc2d1	parciální
diferenciálních	diferenciální	k2eAgFnPc2d1	diferenciální
rovnic	rovnice	k1gFnPc2	rovnice
v	v	k7c6	v
dynamice	dynamika	k1gFnSc6	dynamika
<g/>
,	,	kIx,	,
problém	problém	k1gInSc1	problém
vlastních	vlastní	k2eAgNnPc2d1	vlastní
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
Maxwellovy	Maxwellův	k2eAgInPc1d1	Maxwellův
rovnice	rovnice	k1gFnPc4	rovnice
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
optiku	optika	k1gFnSc4	optika
<g/>
,	,	kIx,	,
termodynamiku	termodynamika	k1gFnSc4	termodynamika
a	a	k8xC	a
statistickou	statistický	k2eAgFnSc4d1	statistická
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
se	se	k3xPyFc4	se
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
stal	stát	k5eAaPmAgMnS	stát
asistentem	asistent	k1gMnSc7	asistent
Maxe	Max	k1gMnSc2	Max
Wiena	Wien	k1gMnSc2	Wien
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
Jeně	Jena	k1gFnSc6	Jena
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
Stuttgartu	Stuttgart	k1gInSc6	Stuttgart
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Reichenbachem	Reichenbach	k1gMnSc7	Reichenbach
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
místo	místo	k1gNnSc4	místo
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
pak	pak	k6eAd1	pak
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
katedře	katedra	k1gFnSc6	katedra
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Zürichu	Zürich	k1gInSc6	Zürich
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zabýval	zabývat	k5eAaImAgInS	zabývat
studiem	studio	k1gNnSc7	studio
struktury	struktura	k1gFnSc2	struktura
atomu	atom	k1gInSc2	atom
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
prací	práce	k1gFnSc7	práce
Louise	Louis	k1gMnSc2	Louis
de	de	k?	de
Broglie	Broglie	k1gFnSc2	Broglie
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
začal	začít	k5eAaPmAgMnS	začít
věnovat	věnovat	k5eAaImF	věnovat
kvantové	kvantový	k2eAgFnSc3d1	kvantová
statistice	statistika	k1gFnSc3	statistika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
publikoval	publikovat	k5eAaBmAgMnS	publikovat
svoji	svůj	k3xOyFgFnSc4	svůj
revoluční	revoluční	k2eAgFnSc4d1	revoluční
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
mechanice	mechanika	k1gFnSc6	mechanika
a	a	k8xC	a
obecné	obecný	k2eAgFnSc3d1	obecná
teorii	teorie	k1gFnSc3	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
úspěchu	úspěch	k1gInSc3	úspěch
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
Planckovo	Planckův	k2eAgNnSc4d1	Planckovo
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1927	[number]	k4	1927
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Einsteinovým	Einsteinův	k2eAgMnSc7d1	Einsteinův
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
perzekucí	perzekuce	k1gFnSc7	perzekuce
Židů	Žid	k1gMnPc2	Žid
se	se	k3xPyFc4	se
Schrödinger	Schrödinger	k1gInSc1	Schrödinger
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
žít	žít	k5eAaImF	žít
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
takovouto	takovýto	k3xDgFnSc7	takovýto
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
získal	získat	k5eAaPmAgInS	získat
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Magdalen	Magdalena	k1gFnPc2	Magdalena
College	Colleg	k1gInSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
se	se	k3xPyFc4	se
dověděl	dovědět	k5eAaPmAgMnS	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
ve	v	k7c6	v
vlnové	vlnový	k2eAgFnSc6d1	vlnová
mechanice	mechanika	k1gFnSc6	mechanika
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Schrödinger	Schrödinger	k1gInSc1	Schrödinger
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
pozván	pozvat	k5eAaPmNgInS	pozvat
do	do	k7c2	do
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
přednášel	přednášet	k5eAaImAgMnS	přednášet
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gNnSc3	on
dokonce	dokonce	k9	dokonce
nabídnuto	nabídnut	k2eAgNnSc4d1	nabídnuto
stálé	stálý	k2eAgNnSc4d1	stálé
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
byl	být	k5eAaImAgMnS	být
ale	ale	k8xC	ale
nucen	nutit	k5eAaImNgMnS	nutit
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
důvodů	důvod	k1gInPc2	důvod
odmítnout	odmítnout	k5eAaPmF	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
publikoval	publikovat	k5eAaBmAgMnS	publikovat
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
třech	tři	k4xCgFnPc6	tři
částech	část	k1gFnPc6	část
"	"	kIx"	"
<g/>
Současná	současný	k2eAgFnSc1d1	současná
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgInS	objevit
známý	známý	k2eAgInSc1d1	známý
paradox	paradox	k1gInSc1	paradox
Schrödingerovy	Schrödingerův	k2eAgFnSc2d1	Schrödingerova
kočky	kočka	k1gFnSc2	kočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1936	[number]	k4	1936
až	až	k9	až
1938	[number]	k4	1938
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Grazu	Graz	k1gInSc6	Graz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
postojem	postoj	k1gInSc7	postoj
vůči	vůči	k7c3	vůči
připojení	připojení	k1gNnSc3	připojení
Rakouska	Rakousko	k1gNnSc2	Rakousko
k	k	k7c3	k
Třetí	třetí	k4xOgFnSc3	třetí
říši	říš	k1gFnSc3	říš
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
"	"	kIx"	"
<g/>
politickou	politický	k2eAgFnSc4d1	politická
nespolehlivost	nespolehlivost	k1gFnSc4	nespolehlivost
<g/>
"	"	kIx"	"
odvolán	odvolán	k2eAgMnSc1d1	odvolán
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
narychlo	narychlo	k6eAd1	narychlo
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stěhoval	stěhovat	k5eAaImAgMnS	stěhovat
do	do	k7c2	do
Oxfordu	Oxford	k1gInSc2	Oxford
a	a	k8xC	a
Gentu	Gent	k1gInSc2	Gent
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
usadil	usadit	k5eAaPmAgMnS	usadit
v	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
zde	zde	k6eAd1	zde
teorii	teorie	k1gFnSc4	teorie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
také	také	k9	také
zabývat	zabývat	k5eAaImF	zabývat
sjednocenou	sjednocený	k2eAgFnSc7d1	sjednocená
teorií	teorie	k1gFnSc7	teorie
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
fyziky	fyzika	k1gFnSc2	fyzika
studoval	studovat	k5eAaImAgMnS	studovat
také	také	k9	také
řeckou	řecký	k2eAgFnSc4d1	řecká
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dublinu	Dublin	k1gInSc6	Dublin
pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
se	se	k3xPyFc4	se
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Annemarie	Annemarie	k1gFnPc1	Annemarie
Bertelovou	Bertelová	k1gFnSc4	Bertelová
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgFnP	být
příliš	příliš	k6eAd1	příliš
dobré	dobrý	k2eAgFnPc1d1	dobrá
a	a	k8xC	a
s	s	k7c7	s
jejím	její	k3xOp3gNnSc7	její
vědomím	vědomí	k1gNnSc7	vědomí
měl	mít	k5eAaImAgInS	mít
řadu	řada	k1gFnSc4	řada
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Anny	Anna	k1gFnPc4	Anna
již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
milovala	milovat	k5eAaImAgFnS	milovat
Schrödingerova	Schrödingerův	k2eAgMnSc4d1	Schrödingerův
přítele	přítel	k1gMnSc4	přítel
Weyla	Weyl	k1gMnSc4	Weyl
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1934	[number]	k4	1934
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Ruth	Ruth	k1gFnSc1	Ruth
Georgie	Georgie	k1gFnSc1	Georgie
Erica	Erica	k1gFnSc1	Erica
<g/>
.	.	kIx.	.
</s>
<s>
Matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
Hilde	Hild	k1gInSc5	Hild
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
jeho	on	k3xPp3gMnSc2	on
asistenta	asistent	k1gMnSc2	asistent
Arthura	Arthur	k1gMnSc2	Arthur
Marcha	March	k1gMnSc2	March
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgFnPc3	tento
okolnostem	okolnost	k1gFnPc3	okolnost
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Princetonu	Princeton	k1gInSc6	Princeton
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
<g/>
.	.	kIx.	.
</s>
<s>
Žít	žít	k5eAaImF	žít
otevřeně	otevřeně	k6eAd1	otevřeně
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
mít	mít	k5eAaImF	mít
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
jiného	jiný	k2eAgMnSc2d1	jiný
muže	muž	k1gMnSc2	muž
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
společensky	společensky	k6eAd1	společensky
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
měl	mít	k5eAaImAgInS	mít
Schrödinger	Schrödinger	k1gInSc1	Schrödinger
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
irskými	irský	k2eAgFnPc7d1	irská
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
73	[number]	k4	73
let	léto	k1gNnPc2	léto
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rakouském	rakouský	k2eAgInSc6d1	rakouský
Alpbachu	Alpbach	k1gInSc6	Alpbach
<g/>
.	.	kIx.	.
</s>
