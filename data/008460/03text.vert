<p>
<s>
Suur	Suur	k1gInSc1	Suur
Munamägi	Munamäg	k1gFnSc2	Munamäg
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
Munameģ	Munameģ	k1gFnSc1	Munameģ
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
Sū	Sū	k1gFnSc1	Sū
Munamiagis	Munamiagis	k1gFnSc2	Munamiagis
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
С	С	k?	С
М	М	k?	М
<g/>
;	;	kIx,	;
estonské	estonský	k2eAgNnSc4d1	Estonské
jméno	jméno	k1gNnSc4	jméno
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
vejcová	vejcový	k2eAgFnSc1d1	vejcový
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
jazycích	jazyk	k1gInPc6	jazyk
jsou	být	k5eAaImIp3nP	být
odvozena	odvodit	k5eAaPmNgNnP	odvodit
z	z	k7c2	z
estonštiny	estonština	k1gFnSc2	estonština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
také	také	k6eAd1	také
všech	všecek	k3xTgInPc2	všecek
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
jejího	její	k3xOp3gInSc2	její
vrcholu	vrchol	k1gInSc2	vrchol
je	být	k5eAaImIp3nS	být
318,1	[number]	k4	318,1
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Haanijské	Haanijský	k2eAgFnSc6d1	Haanijský
vysočině	vysočina	k1gFnSc6	vysočina
poblíž	poblíž	k7c2	poblíž
vesnice	vesnice	k1gFnSc2	vesnice
Haanja	Haanj	k1gInSc2	Haanj
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
estonského	estonský	k2eAgInSc2d1	estonský
kraje	kraj	k1gInSc2	kraj
Võ	Võ	k1gFnSc2	Võ
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
lotyšských	lotyšský	k2eAgFnPc2d1	lotyšská
a	a	k8xC	a
ruských	ruský	k2eAgFnPc2d1	ruská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1	okolní
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
kopcovitá	kopcovitý	k2eAgFnSc1d1	kopcovitá
a	a	k8xC	a
zalesněná	zalesněný	k2eAgFnSc1d1	zalesněná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
místě	místo	k1gNnSc6	místo
je	být	k5eAaImIp3nS	být
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
vysoká	vysoká	k1gFnSc1	vysoká
29,1	[number]	k4	29,1
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
346,7	[number]	k4	346,7
metrů	metr	k1gInPc2	metr
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
výhled	výhled	k1gInSc4	výhled
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
vrcholu	vrchol	k1gInSc2	vrchol
je	být	k5eAaImIp3nS	být
pramen	pramen	k1gInSc1	pramen
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
vytékající	vytékající	k2eAgFnSc1d1	vytékající
voda	voda	k1gFnSc1	voda
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
malá	malý	k2eAgNnPc1d1	malé
jezera	jezero	k1gNnPc1	jezero
a	a	k8xC	a
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
souřadnice	souřadnice	k1gFnPc1	souřadnice
hory	hora	k1gFnSc2	hora
Suur	Suura	k1gFnPc2	Suura
Munamägi	Munamägi	k1gNnSc2	Munamägi
jsou	být	k5eAaImIp3nP	být
57	[number]	k4	57
<g/>
°	°	k?	°
42	[number]	k4	42
<g/>
'	'	kIx"	'
52	[number]	k4	52
<g/>
"	"	kIx"	"
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
27	[number]	k4	27
<g/>
°	°	k?	°
3	[number]	k4	3
<g/>
'	'	kIx"	'
33	[number]	k4	33
<g/>
"	"	kIx"	"
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
zprvu	zprvu	k6eAd1	zprvu
jen	jen	k9	jen
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
necelých	celý	k2eNgInPc2d1	necelý
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
stavět	stavět	k5eAaImF	stavět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1816	[number]	k4	1816
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Friedricha	Friedrich	k1gMnSc2	Friedrich
Georga	Georg	k1gMnSc2	Georg
Wilhelma	Wilhelma	k1gFnSc1	Wilhelma
von	von	k1gInSc1	von
Struve	Struev	k1gFnSc2	Struev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1870	[number]	k4	1870
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
nové	nový	k2eAgFnSc2d1	nová
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měřila	měřit	k5eAaImAgFnS	měřit
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výhled	výhled	k1gInSc1	výhled
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
omezen	omezen	k2eAgInSc1d1	omezen
okolními	okolní	k2eAgInPc7d1	okolní
stromy	strom	k1gInPc7	strom
a	a	k8xC	a
plošina	plošina	k1gFnSc1	plošina
věže	věž	k1gFnSc2	věž
byla	být	k5eAaImAgFnS	být
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
pro	pro	k7c4	pro
velký	velký	k2eAgInSc4d1	velký
turistický	turistický	k2eAgInSc4d1	turistický
zájem	zájem	k1gInSc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měřila	měřit	k5eAaImAgFnS	měřit
12	[number]	k4	12
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
stromy	strom	k1gInPc7	strom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
byla	být	k5eAaImAgFnS	být
místo	místo	k1gNnSc4	místo
starší	starý	k2eAgFnSc2d2	starší
věže	věž	k1gFnSc2	věž
postavena	postaven	k2eAgFnSc1d1	postavena
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
17	[number]	k4	17
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
;	;	kIx,	;
její	její	k3xOp3gFnSc1	její
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
hnít	hnít	k5eAaImF	hnít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
rozhodnuto	rozhodnut	k2eAgNnSc4d1	rozhodnuto
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
nové	nový	k2eAgFnSc2d1	nová
železobetonové	železobetonový	k2eAgFnSc2d1	železobetonová
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
výška	výška	k1gFnSc1	výška
byla	být	k5eAaImAgFnS	být
25,7	[number]	k4	25,7
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
věž	věž	k1gFnSc1	věž
dočkala	dočkat	k5eAaPmAgFnS	dočkat
generální	generální	k2eAgFnPc4d1	generální
opravy	oprava	k1gFnPc4	oprava
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
pak	pak	k6eAd1	pak
přístavby	přístavba	k1gFnPc4	přístavba
dalšího	další	k2eAgNnSc2d1	další
patra	patro	k1gNnSc2	patro
na	na	k7c4	na
výšku	výška	k1gFnSc4	výška
29,1	[number]	k4	29,1
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
přestavby	přestavba	k1gFnSc2	přestavba
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
nová	nový	k2eAgFnSc1d1	nová
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
,	,	kIx,	,
zpevněny	zpevněn	k2eAgFnPc1d1	zpevněna
cesty	cesta	k1gFnPc1	cesta
z	z	k7c2	z
Haanji	Haanj	k1gMnSc3	Haanj
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
a	a	k8xC	a
provedena	proveden	k2eAgFnSc1d1	provedena
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
i	i	k9	i
výtah	výtah	k1gInSc1	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
byly	být	k5eAaImAgInP	být
cca	cca	kA	cca
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
estonských	estonský	k2eAgFnPc2d1	Estonská
korun	koruna	k1gFnPc2	koruna
(	(	kIx(	(
<g/>
cca	cca	kA	cca
18,5	[number]	k4	18,5
miliónu	milión	k4xCgInSc2	milión
českých	český	k2eAgFnPc2d1	Česká
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
věž	věž	k1gFnSc1	věž
znovuotevřena	znovuotevřít	k5eAaPmNgFnS	znovuotevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Návštěva	návštěva	k1gFnSc1	návštěva
rozhledny	rozhledna	k1gFnSc2	rozhledna
===	===	k?	===
</s>
</p>
<p>
<s>
Suur	Suur	k1gInSc1	Suur
Munamägi	Munamäg	k1gFnSc2	Munamäg
a	a	k8xC	a
rozhledna	rozhledna	k1gFnSc1	rozhledna
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
jsou	být	k5eAaImIp3nP	být
navštěvovány	navštěvován	k2eAgFnPc1d1	navštěvována
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
pro	pro	k7c4	pro
výborný	výborný	k2eAgInSc4d1	výborný
výhled	výhled	k1gInSc4	výhled
do	do	k7c2	do
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozhledny	rozhledna	k1gFnSc2	rozhledna
je	být	k5eAaImIp3nS	být
krásný	krásný	k2eAgInSc1d1	krásný
pohled	pohled	k1gInSc1	pohled
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
Haanja	Haanj	k1gInSc2	Haanj
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
město	město	k1gNnSc4	město
Võ	Võ	k1gFnSc2	Võ
<g/>
,	,	kIx,	,
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnSc6d1	dobrá
viditelnosti	viditelnost	k1gFnSc6	viditelnost
i	i	k9	i
Pskov	Pskov	k1gInSc1	Pskov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
nebo	nebo	k8xC	nebo
výtahem	výtah	k1gInSc7	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
je	být	k5eAaImIp3nS	být
2,5	[number]	k4	2,5
eura	euro	k1gNnSc2	euro
<g/>
,	,	kIx,	,
výtahem	výtah	k1gInSc7	výtah
4	[number]	k4	4
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
obce	obec	k1gFnSc2	obec
Haanja	Haanj	k1gInSc2	Haanj
<g/>
,	,	kIx,	,
předškolní	předškolní	k2eAgFnPc4d1	předškolní
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
těžce	těžce	k6eAd1	těžce
postižené	postižený	k1gMnPc4	postižený
je	být	k5eAaImIp3nS	být
vstup	vstup	k1gInSc1	vstup
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
žáky	žák	k1gMnPc4	žák
a	a	k8xC	a
studenty	student	k1gMnPc4	student
1,5	[number]	k4	1,5
eura	euro	k1gNnPc4	euro
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyjdou	vyjít	k5eAaPmIp3nP	vyjít
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
rovněž	rovněž	k9	rovněž
1,5	[number]	k4	1,5
eura	euro	k1gNnSc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
1	[number]	k4	1
euro	euro	k1gNnSc4	euro
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
půjčit	půjčit	k5eAaPmF	půjčit
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
.	.	kIx.	.
</s>
<s>
Otevírací	otevírací	k2eAgFnSc1d1	otevírací
doba	doba	k1gFnSc1	doba
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
přizpůsobena	přizpůsobit	k5eAaPmNgFnS	přizpůsobit
světelným	světelný	k2eAgFnPc3d1	světelná
podmínkám	podmínka	k1gFnPc3	podmínka
a	a	k8xC	a
měnící	měnící	k2eAgFnPc1d1	měnící
se	se	k3xPyFc4	se
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
So	So	kA	So
<g/>
–	–	k?	–
<g/>
Ne	ne	k9	ne
12.00	[number]	k4	12.00
<g/>
–	–	k?	–
<g/>
15.00	[number]	k4	15.00
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
<g/>
–	–	k?	–
<g/>
Ne	ne	k9	ne
10.00	[number]	k4	10.00
<g/>
–	–	k?	–
<g/>
20.00	[number]	k4	20.00
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
až	až	k9	až
30	[number]	k4	30
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
<g/>
–	–	k?	–
<g/>
Ne	ne	k9	ne
10.00	[number]	k4	10.00
<g/>
–	–	k?	–
<g/>
17.00	[number]	k4	17.00
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
až	až	k9	až
31	[number]	k4	31
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
So	So	kA	So
<g/>
–	–	k?	–
<g/>
Ne	ne	k9	ne
10.00	[number]	k4	10.00
<g/>
–	–	k?	–
<g/>
17.00	[number]	k4	17.00
</s>
</p>
<p>
<s>
==	==	k?	==
Příjezdová	příjezdový	k2eAgFnSc1d1	příjezdová
cesta	cesta	k1gFnSc1	cesta
==	==	k?	==
</s>
</p>
<p>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
hoře	hora	k1gFnSc3	hora
je	být	k5eAaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
Haanja	Haanj	k1gInSc2	Haanj
(	(	kIx(	(
<g/>
18	[number]	k4	18
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Võ	Võ	k1gFnSc2	Võ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
turistická	turistický	k2eAgFnSc1d1	turistická
stezka	stezka	k1gFnSc1	stezka
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
(	(	kIx(	(
<g/>
asi	asi	k9	asi
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
chůze	chůze	k1gFnSc2	chůze
po	po	k7c6	po
mírném	mírný	k2eAgInSc6d1	mírný
zalesněném	zalesněný	k2eAgInSc6d1	zalesněný
svahu	svah	k1gInSc6	svah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příjezdová	příjezdový	k2eAgFnSc1d1	příjezdová
silnice	silnice	k1gFnSc1	silnice
do	do	k7c2	do
Haanji	Haanj	k1gMnSc6	Haanj
vede	vést	k5eAaImIp3nS	vést
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
estonských	estonský	k2eAgFnPc2d1	Estonská
destinací	destinace	k1gFnPc2	destinace
přes	přes	k7c4	přes
Võ	Võ	k1gFnSc4	Võ
(	(	kIx(	(
<g/>
z	z	k7c2	z
Tallinnu	Tallinn	k1gInSc2	Tallinn
po	po	k7c6	po
silnici	silnice	k1gFnSc6	silnice
A	a	k9	a
<g/>
202	[number]	k4	202
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
odbočit	odbočit	k5eAaPmF	odbočit
na	na	k7c4	na
jih	jih	k1gInSc4	jih
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Haanju	Haanju	k1gFnSc4	Haanju
a	a	k8xC	a
Misso	Missa	k1gFnSc5	Missa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
z	z	k7c2	z
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
po	po	k7c6	po
rižsko-pskovské	rižskoskovský	k2eAgFnSc6d1	rižsko-pskovský
silnici	silnice	k1gFnSc6	silnice
se	se	k3xPyFc4	se
před	před	k7c7	před
Misso	Missa	k1gFnSc5	Missa
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Võ	Võ	k1gFnSc4	Võ
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
Haanja	Haanj	k1gInSc2	Haanj
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
na	na	k7c4	na
půli	půle	k1gFnSc4	půle
cesty	cesta	k1gFnSc2	cesta
od	od	k7c2	od
odbočky	odbočka	k1gFnSc2	odbočka
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
příjezdová	příjezdový	k2eAgFnSc1d1	příjezdová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
z	z	k7c2	z
Tallinnu	Tallinn	k1gInSc2	Tallinn
je	být	k5eAaImIp3nS	být
270	[number]	k4	270
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
Rigy	Riga	k1gFnSc2	Riga
250	[number]	k4	250
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
1	[number]	k4	1
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
koupit	koupit	k5eAaPmF	koupit
poštovní	poštovní	k2eAgFnSc1d1	poštovní
známka	známka	k1gFnSc1	známka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
známce	známka	k1gFnSc6	známka
je	být	k5eAaImIp3nS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
věž	věž	k1gFnSc1	věž
a	a	k8xC	a
okolní	okolní	k2eAgFnSc1d1	okolní
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
5,20	[number]	k4	5,20
estonských	estonský	k2eAgFnPc2d1	Estonská
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Suur	Suura	k1gFnPc2	Suura
Munamägi	Munamäg	k1gFnSc2	Munamäg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Suur	Suur	k1gInSc1	Suur
Munamägi	Munamäg	k1gFnSc2	Munamäg
na	na	k7c4	na
HoryEvropy	HoryEvrop	k1gInPc4	HoryEvrop
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Suur	Suur	k1gInSc4	Suur
Munamägi	Munamäg	k1gFnSc2	Munamäg
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
<g/>
,	,	kIx,	,
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
