<s>
Suur	Suur	k1gInSc1	Suur
Munamägi	Munamäg	k1gFnSc2	Munamäg
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
Munameģ	Munameģ	k1gFnSc1	Munameģ
<g/>
,	,	kIx,	,
litevsky	litevsky	k6eAd1	litevsky
Sū	Sū	k1gFnSc1	Sū
Munamiagis	Munamiagis	k1gFnSc2	Munamiagis
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
С	С	k?	С
М	М	k?	М
<g/>
;	;	kIx,	;
estonské	estonský	k2eAgNnSc4d1	Estonské
jméno	jméno	k1gNnSc4	jméno
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
vejcová	vejcový	k2eAgFnSc1d1	vejcový
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jména	jméno	k1gNnPc1	jméno
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
jazycích	jazyk	k1gInPc6	jazyk
jsou	být	k5eAaImIp3nP	být
odvozena	odvodit	k5eAaPmNgNnP	odvodit
z	z	k7c2	z
estonštiny	estonština	k1gFnSc2	estonština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
také	také	k6eAd1	také
všech	všecek	k3xTgInPc2	všecek
pobaltských	pobaltský	k2eAgInPc2d1	pobaltský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
