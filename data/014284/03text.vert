<s>
STS-81	STS-81	k4
</s>
<s>
STS-81	STS-81	k4
</s>
<s>
Znak	znak	k1gInSc1
expedice	expedice	k1gFnSc2
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c6
expedici	expedice	k1gFnSc6
</s>
<s>
Na	na	k7c6
stanici	stanice	k1gFnSc6
</s>
<s>
Mir	mir	k1gInSc1
</s>
<s>
Loď	loď	k1gFnSc1
</s>
<s>
Atlantis	Atlantis	k1gFnSc1
</s>
<s>
COSPAR	COSPAR	kA
</s>
<s>
1997-001A	1997-001A	k4
</s>
<s>
Členů	člen	k1gInPc2
expedice	expedice	k1gFnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
Hostitelé	hostitel	k1gMnPc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
základní	základní	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
(	(	kIx(
<g/>
Mir	mir	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Datum	datum	k1gNnSc1
startu	start	k1gInSc2
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
UTC	UTC	kA
</s>
<s>
Kosmodrom	kosmodrom	k1gInSc1
</s>
<s>
Kennedyho	Kennedyze	k6eAd1
vesmírné	vesmírný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Vzletová	vzletový	k2eAgFnSc1d1
rampa	rampa	k1gFnSc1
</s>
<s>
39-B	39-B	k4
</s>
<s>
Spojení	spojení	k1gNnSc1
se	s	k7c7
stanicí	stanice	k1gFnSc7
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
UTC	UTC	kA
</s>
<s>
Délka	délka	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
10	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
4	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
56	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
na	na	k7c4
stanici	stanice	k1gFnSc4
</s>
<s>
4	#num#	k4
dny	den	k1gInPc4
<g/>
,	,	kIx,
22	#num#	k4
hodina	hodina	k1gFnSc1
<g/>
,	,	kIx,
21	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Odlet	odlet	k1gInSc1
ze	z	k7c2
stanice	stanice	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
UTC	UTC	kA
</s>
<s>
Datum	datum	k1gNnSc1
přistání	přistání	k1gNnSc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
24	#num#	k4
UTC	UTC	kA
</s>
<s>
Místo	místo	k7c2
přistání	přistání	k1gNnSc2
</s>
<s>
Kennedyho	Kennedyze	k6eAd1
vesmírné	vesmírný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
</s>
<s>
John	John	k1gMnSc1
Blaha	Blaha	k1gMnSc1
</s>
<s>
Přechod	přechod	k1gInSc1
z	z	k7c2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
základní	základní	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
(	(	kIx(
<g/>
Mir	mir	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Délka	délka	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
128	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
5	#num#	k4
hodin	hodina	k1gFnPc2
<g/>
,	,	kIx,
28	#num#	k4
minut	minuta	k1gFnPc2
</s>
<s>
Jerry	Jerra	k1gFnPc1
Linenger	Linengra	k1gFnPc2
</s>
<s>
Délka	délka	k1gFnSc1
letu	let	k1gInSc2
</s>
<s>
132	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
4	#num#	k4
hodiny	hodina	k1gFnPc4
</s>
<s>
Přechod	přechod	k1gInSc1
do	do	k7c2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
základní	základní	k2eAgFnSc2d1
expedice	expedice	k1gFnSc2
(	(	kIx(
<g/>
Mir	mir	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Parametry	parametr	k1gInPc1
dráhy	dráha	k1gFnSc2
</s>
<s>
Perigeum	perigeum	k1gNnSc1
<g/>
380	#num#	k4
km	km	kA
</s>
<s>
Apogeum	apogeum	k1gNnSc1
<g/>
392	#num#	k4
km	km	kA
</s>
<s>
Doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
<g/>
92,2	92,2	k4
min	mina	k1gFnPc2
</s>
<s>
Sklon	sklon	k1gInSc1
dráhy	dráha	k1gFnSc2
<g/>
51,6	51,6	k4
<g/>
°	°	k?
</s>
<s>
Fotografie	fotografia	k1gFnPc1
posádky	posádka	k1gFnSc2
</s>
<s>
Navigace	navigace	k1gFnSc1
</s>
<s>
Předcházející	předcházející	k2eAgInPc1d1
</s>
<s>
Následující	následující	k2eAgInSc1d1
</s>
<s>
STS-80	STS-80	k4
</s>
<s>
STS-82	STS-82	k4
</s>
<s>
STS-81	STS-81	k4
byla	být	k5eAaImAgFnS
mise	mise	k1gFnSc1
raketoplánu	raketoplán	k1gInSc2
Atlantis	Atlantis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
letu	let	k1gInSc2
bylo	být	k5eAaImAgNnS
páté	pátý	k4xOgNnSc1
setkání	setkání	k1gNnSc1
raketoplánu	raketoplán	k1gInSc2
s	s	k7c7
ruskou	ruský	k2eAgFnSc7d1
orbitální	orbitální	k2eAgFnSc7d1
stanicí	stanice	k1gFnSc7
Mir	Mira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raketoplánem	raketoplán	k1gInSc7
se	se	k3xPyFc4
na	na	k7c6
Zemi	zem	k1gFnSc6
vracel	vracet	k5eAaImAgMnS
John	John	k1gMnSc1
E.	E.	kA
Blaha	Blaha	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
vesmírné	vesmírný	k2eAgFnSc6d1
stanici	stanice	k1gFnSc6
a	a	k8xC
kterého	který	k3yIgMnSc4,k3yRgMnSc4,k3yQgMnSc4
zde	zde	k6eAd1
vystřídal	vystřídat	k5eAaPmAgMnS
Jerry	Jerr	k1gInPc4
M.	M.	kA
Linenger	Linenger	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Posádka	posádka	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
A.	A.	kA
Baker	Baker	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
</s>
<s>
Brent	Brent	k?
W.	W.	kA
Jett	Jett	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pilot	pilot	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
J.K.	J.K.	k1gMnSc1
Wisoff	Wisoff	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
1	#num#	k4
</s>
<s>
John	John	k1gMnSc1
M.	M.	kA
Grunsfeld	Grunsfeld	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
2	#num#	k4
</s>
<s>
Marsha	Marsha	k1gFnSc1
S.	S.	kA
Ivinsová	Ivinsová	k1gFnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
3	#num#	k4
</s>
<s>
Nový	nový	k2eAgMnSc1d1
člen	člen	k1gMnSc1
posádky	posádka	k1gFnSc2
Miru	mir	k1gInSc2
</s>
<s>
Jerry	Jerr	k1gInPc1
M.	M.	kA
Linenger	Linengra	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
4	#num#	k4
</s>
<s>
Návrat	návrat	k1gInSc1
z	z	k7c2
mise	mise	k1gFnSc2
na	na	k7c4
Miru	Mira	k1gFnSc4
</s>
<s>
John	John	k1gMnSc1
E.	E.	kA
Blaha	Blaha	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
letový	letový	k2eAgMnSc1d1
specialista	specialista	k1gMnSc1
4	#num#	k4
</s>
<s>
Průběh	průběh	k1gInSc1
mise	mise	k1gFnSc2
</s>
<s>
STS-81	STS-81	k4
byla	být	k5eAaImAgFnS
pátou	pátý	k4xOgFnSc4
z	z	k7c2
devíti	devět	k4xCc2
plánovaných	plánovaný	k2eAgFnPc2d1
misí	mise	k1gFnPc2
na	na	k7c4
Mir	mir	k1gInSc4
a	a	k8xC
druhá	druhý	k4xOgFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měnila	měnit	k5eAaImAgFnS
amerického	americký	k2eAgMnSc4d1
astronauta	astronaut	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Blaha	Blaha	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
na	na	k7c6
Miru	mir	k1gInSc6
118	#num#	k4
dní	den	k1gInPc2
od	od	k7c2
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vystřídán	vystřídat	k5eAaPmNgInS
astronautem	astronaut	k1gMnSc7
Jerrym	Jerrym	k1gInSc1
Linengerem	Linenger	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linenger	Linenger	k1gInSc1
pak	pak	k6eAd1
strávil	strávit	k5eAaPmAgInS
na	na	k7c4
Miru	Mira	k1gFnSc4
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Zemi	zem	k1gFnSc6
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
s	s	k7c7
misí	mise	k1gFnSc7
STS-	STS-	k1gFnSc7
<g/>
84	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
odstartovat	odstartovat	k5eAaPmF
v	v	k7c6
květnu	květen	k1gInSc6
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nákladovém	nákladový	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
Atlantisu	Atlantis	k1gInSc2
byl	být	k5eAaImAgInS
uložen	uložit	k5eAaPmNgInS
dvojitý	dvojitý	k2eAgInSc1d1
modul	modul	k1gInSc1
SPACEHAB-DM	SPACEHAB-DM	k1gFnSc1
(	(	kIx(
<g/>
Double	double	k1gInSc1
Module	modul	k1gInSc5
<g/>
)	)	kIx)
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
sloužil	sloužit	k5eAaImAgMnS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
prostoru	prostor	k1gInSc2
pro	pro	k7c4
experimenty	experiment	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raketoplán	raketoplán	k1gInSc1
úspěšně	úspěšně	k6eAd1
odstartoval	odstartovat	k5eAaPmAgInS
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
z	z	k7c2
Kennedyho	Kennedy	k1gMnSc2
vesmírného	vesmírný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
a	a	k8xC
u	u	k7c2
Miru	mir	k1gInSc2
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
54	#num#	k4
<g/>
:	:	kIx,
<g/>
49	#num#	k4
UTC	UTC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
přivítání	přivítání	k1gNnSc4
posádek	posádka	k1gFnPc2
začali	začít	k5eAaPmAgMnP
všichni	všechen	k3xTgMnPc1
plnit	plnit	k5eAaImF
hlavní	hlavní	k2eAgInSc4d1
úkol	úkol	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
dopravit	dopravit	k5eAaPmF
dovezený	dovezený	k2eAgInSc1d1
materiál	materiál	k1gInSc1
na	na	k7c4
palubu	paluba	k1gFnSc4
vesmírné	vesmírný	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
pěti	pět	k4xCc2
dní	den	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
raketoplán	raketoplán	k1gInSc1
zakotven	zakotvit	k5eAaPmNgInS
u	u	k7c2
Miru	mir	k1gInSc2
posádka	posádka	k1gFnSc1
překládala	překládat	k5eAaImAgFnS
vodu	voda	k1gFnSc4
-	-	kIx~
635	#num#	k4
kg	kg	kA
a	a	k8xC
zásoby	zásoba	k1gFnPc4
z	z	k7c2
jedné	jeden	k4xCgFnSc2
kosmické	kosmický	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
na	na	k7c4
druhou	druhý	k4xOgFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Mir	mir	k1gInSc4
se	se	k3xPyFc4
přesunulo	přesunout	k5eAaPmAgNnS
516	#num#	k4
kg	kg	kA
amerického	americký	k2eAgNnSc2d1
vědeckého	vědecký	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
<g/>
,	,	kIx,
1	#num#	k4
000	#num#	k4
kg	kg	kA
ruských	ruský	k2eAgFnPc2d1
zásob	zásoba	k1gFnPc2
a	a	k8xC
122	#num#	k4
kg	kg	kA
ostatního	ostatní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
největší	veliký	k2eAgFnSc2d3
zásobovací	zásobovací	k2eAgFnSc2d1
mise	mise	k1gFnSc2
raketoplánu	raketoplán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpět	zpět	k6eAd1
na	na	k7c4
Zem	zem	k1gFnSc4
dopravil	dopravit	k5eAaPmAgMnS
Atlantis	Atlantis	k1gFnSc1
570	#num#	k4
kg	kg	kA
materiálu	materiál	k1gInSc2
z	z	k7c2
provedených	provedený	k2eAgInPc2d1
amerických	americký	k2eAgInPc2d1
vědeckých	vědecký	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
na	na	k7c6
Miru	mir	k1gInSc6
<g/>
,	,	kIx,
405	#num#	k4
kg	kg	kA
ruského	ruský	k2eAgInSc2d1
nákladu	náklad	k1gInSc2
a	a	k8xC
97	#num#	k4
kg	kg	kA
ostatního	ostatní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Fotografie	fotografia	k1gFnPc1
orbitální	orbitální	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
Mir	Mira	k1gFnPc2
z	z	k7c2
raketoplánu	raketoplán	k1gInSc2
Atlantis	Atlantis	k1gFnSc1
při	při	k7c6
misi	mise	k1gFnSc6
STS-81	STS-81	k1gFnSc2
</s>
<s>
Vedle	vedle	k7c2
doplňování	doplňování	k1gNnSc2
zásob	zásoba	k1gFnPc2
kosmické	kosmický	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
se	se	k3xPyFc4
posádky	posádka	k1gFnPc1
také	také	k6eAd1
věnovaly	věnovat	k5eAaPmAgFnP,k5eAaImAgFnP
naplánovaným	naplánovaný	k2eAgInPc3d1
vědeckým	vědecký	k2eAgInPc3d1
experimentům	experiment	k1gInPc3
zaměřených	zaměřený	k2eAgFnPc2d1
na	na	k7c4
biologii	biologie	k1gFnSc4
<g/>
,	,	kIx,
studium	studium	k1gNnSc4
mikrogravitace	mikrogravitace	k1gFnSc2
a	a	k8xC
zkoumání	zkoumání	k1gNnSc2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
pokusů	pokus	k1gInPc2
bylo	být	k5eAaImAgNnS
především	především	k6eAd1
získat	získat	k5eAaPmF
informace	informace	k1gFnPc4
pro	pro	k7c4
budoucí	budoucí	k2eAgNnSc4d1
plánování	plánování	k1gNnSc4
a	a	k8xC
výstavbu	výstavba	k1gFnSc4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
kosmické	kosmický	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
(	(	kIx(
<g/>
ISS	ISS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
experimenty	experiment	k1gInPc1
</s>
<s>
skleník	skleník	k1gInSc4
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
rostlin	rostlina	k1gFnPc2
Biorack	Bioracka	k1gFnPc2
</s>
<s>
experiment	experiment	k1gInSc1
s	s	k7c7
fotografováním	fotografování	k1gNnSc7
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
byly	být	k5eAaImAgFnP
zapojeny	zapojit	k5eAaPmNgFnP
některé	některý	k3yIgFnPc1
školy	škola	k1gFnPc1
-	-	kIx~
KidSat	KidSat	k1gInSc1
</s>
<s>
měřič	měřič	k1gInSc1
radiace	radiace	k1gFnSc2
kosmického	kosmický	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
,	,	kIx,
umístěný	umístěný	k2eAgInSc1d1
ve	v	k7c6
skleníku	skleník	k1gInSc6
Biorack	Bioracka	k1gFnPc2
-	-	kIx~
Cosmic	Cosmice	k1gFnPc2
Radiation	Radiation	k1gInSc1
and	and	k?
Effects	Effects	k1gInSc1
Monitor	monitor	k1gInSc1
(	(	kIx(
<g/>
CREAM	CREAM	kA
<g/>
)	)	kIx)
</s>
<s>
test	test	k1gInSc1
vibrací	vibrace	k1gFnPc2
vznikajících	vznikající	k2eAgFnPc2d1
při	při	k7c6
cvičení	cvičení	k1gNnSc6
kosmonautů	kosmonaut	k1gMnPc2
(	(	kIx(
<g/>
Treadmill	Treadmill	k1gInSc1
Vibration	Vibration	k1gInSc1
Isolation	Isolation	k1gInSc1
and	and	k?
Stabilization	Stabilization	k1gInSc1
System	Systo	k1gNnSc7
–	–	k?
TVIS	TVIS	kA
<g/>
)	)	kIx)
</s>
<s>
zjištění	zjištění	k1gNnSc1
chování	chování	k1gNnSc2
stanice	stanice	k1gFnSc2
a	a	k8xC
raketoplánu	raketoplán	k1gInSc2
při	při	k7c6
působení	působení	k1gNnSc6
tahu	tah	k1gInSc2
malého	malý	k2eAgInSc2d1
raketového	raketový	k2eAgInSc2d1
motoru	motor	k1gInSc2
</s>
<s>
Ukončení	ukončení	k1gNnSc1
mise	mise	k1gFnSc2
</s>
<s>
K	k	k7c3
odpojení	odpojení	k1gNnSc3
raketoplánu	raketoplán	k1gInSc2
od	od	k7c2
stanice	stanice	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1997	#num#	k4
ve	v	k7c4
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
<g/>
15	#num#	k4
<g/>
:	:	kIx,
<g/>
44	#num#	k4
UTC	UTC	kA
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
obletu	oblet	k1gInSc6
stanice	stanice	k1gFnSc2
vydal	vydat	k5eAaPmAgInS
Atlantis	Atlantis	k1gFnSc1
na	na	k7c4
zpáteční	zpáteční	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
na	na	k7c4
Zem	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raketoplán	raketoplán	k1gInSc1
přistál	přistát	k5eAaImAgInS,k5eAaPmAgInS
bez	bez	k7c2
problémů	problém	k1gInPc2
na	na	k7c6
Kennedyho	Kennedy	k1gMnSc2
vesmírném	vesmírný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ledna	leden	k1gInSc2
1997	#num#	k4
ve	v	k7c4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
UTC	UTC	kA
po	po	k7c6
letu	let	k1gInSc6
trvajícím	trvající	k2eAgMnSc6d1
10	#num#	k4
dní	den	k1gInPc2
4	#num#	k4
hodiny	hodina	k1gFnSc2
a	a	k8xC
56	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
STS-81	STS-81	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránka	stránka	k1gFnSc1
o	o	k7c4
STS-81	STS-81	k1gFnSc4
na	na	k7c6
webu	web	k1gInSc6
NASA	NASA	kA
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
letu	let	k1gInSc6
STS-81	STS-81	k1gFnSc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Raketoplán	raketoplán	k1gInSc1
Atlantis	Atlantis	k1gFnSc1
(	(	kIx(
<g/>
OV-	OV-	k1gFnSc1
<g/>
104	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
STS-51-J	STS-51-J	k4
•	•	k?
STS-61-B	STS-61-B	k1gMnSc1
•	•	k?
STS-27	STS-27	k1gMnSc1
•	•	k?
STS-30	STS-30	k1gMnSc1
•	•	k?
STS-34	STS-34	k1gMnSc1
•	•	k?
STS-36	STS-36	k1gMnSc1
•	•	k?
STS-38	STS-38	k1gMnSc1
•	•	k?
STS-37	STS-37	k1gMnSc1
•	•	k?
STS-43	STS-43	k1gMnSc1
•	•	k?
STS-44	STS-44	k1gMnSc1
•	•	k?
STS-45	STS-45	k1gMnSc1
•	•	k?
STS-46	STS-46	k1gMnSc1
•	•	k?
STS-66	STS-66	k1gMnSc1
•	•	k?
STS-71	STS-71	k1gMnSc1
•	•	k?
STS-74	STS-74	k1gMnSc1
•	•	k?
STS-76	STS-76	k1gMnSc1
•	•	k?
STS-79	STS-79	k1gMnSc1
•	•	k?
STS-81	STS-81	k1gMnSc1
•	•	k?
STS-84	STS-84	k1gMnSc1
•	•	k?
STS-86	STS-86	k1gMnSc1
•	•	k?
STS-101	STS-101	k1gMnSc1
•	•	k?
STS-106	STS-106	k1gMnSc1
•	•	k?
STS-98	STS-98	k1gMnSc1
•	•	k?
STS-104	STS-104	k1gMnSc1
•	•	k?
STS-110	STS-110	k1gMnSc1
•	•	k?
STS-112	STS-112	k1gMnSc1
•	•	k?
STS-115	STS-115	k1gMnSc1
•	•	k?
STS-117	STS-117	k1gMnSc1
•	•	k?
STS-122	STS-122	k1gMnSc1
•	•	k?
STS-125	STS-125	k1gMnSc1
•	•	k?
STS-129	STS-129	k1gMnSc1
•	•	k?
STS-132	STS-132	k1gMnSc1
•	•	k?
STS-135	STS-135	k1gFnSc1
Stav	stav	k1gInSc1
<g/>
:	:	kIx,
Vyřazen	vyřazen	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystaven	vystaven	k2eAgInSc4d1
v	v	k7c4
Kennedyho	Kennedy	k1gMnSc2
vesmírném	vesmírný	k2eAgNnSc6d1
středisku	středisko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Vesmírná	vesmírný	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Mir	Mira	k1gFnPc2
Moduly	modul	k1gInPc4
</s>
<s>
Základní	základní	k2eAgInSc1d1
modul	modul	k1gInSc1
•	•	k?
Kvant-	Kvant-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
Kvant-	Kvant-	k1gFnPc2
<g/>
2	#num#	k4
•	•	k?
Kristall	Kristall	k1gMnSc1
•	•	k?
Spektr	Spektr	k1gInSc1
•	•	k?
Stykový	stykový	k2eAgInSc1d1
modul	modul	k1gInSc1
•	•	k?
Priroda	Priroda	k1gFnSc1
Lodě	loď	k1gFnSc2
</s>
<s>
Sojuz	Sojuz	k1gInSc1
•	•	k?
Progress	Progress	k1gInSc1
•	•	k?
Space	Space	k1gFnSc2
Shuttle	Shuttle	k1gFnSc2
Programy	program	k1gInPc1
</s>
<s>
Shuttle-Mir	Shuttle-Mir	k1gInSc1
Expedice	expedice	k1gFnSc2
na	na	k7c4
Mir	mir	k1gInSc4
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
EO-1	EO-1	k4
•	•	k?
EO-2	EO-2	k1gMnSc1
•	•	k?
EO-3	EO-3	k1gMnSc1
•	•	k?
EO-4	EO-4	k1gMnSc1
•	•	k?
EO-5	EO-5	k1gMnSc1
•	•	k?
EO-6	EO-6	k1gMnSc1
•	•	k?
EO-7	EO-7	k1gMnSc1
•	•	k?
EO-8	EO-8	k1gMnSc1
•	•	k?
EO-9	EO-9	k1gMnSc1
•	•	k?
EO-10	EO-10	k1gMnSc1
•	•	k?
EO-11	EO-11	k1gMnSc1
•	•	k?
EO-12	EO-12	k1gMnSc1
•	•	k?
EO-13	EO-13	k1gMnSc1
•	•	k?
EO-14	EO-14	k1gMnSc1
•	•	k?
EO-15	EO-15	k1gMnSc1
•	•	k?
EO-16	EO-16	k1gMnSc1
•	•	k?
EO-17	EO-17	k1gMnSc1
•	•	k?
EO-18	EO-18	k1gMnSc1
•	•	k?
EO-19	EO-19	k1gMnSc1
•	•	k?
EO-20	EO-20	k1gMnSc1
•	•	k?
EO-21	EO-21	k1gMnSc1
•	•	k?
EO-22	EO-22	k1gMnSc1
•	•	k?
EO-23	EO-23	k1gMnSc1
•	•	k?
EO-24	EO-24	k1gMnSc1
•	•	k?
EO-25	EO-25	k1gMnSc1
•	•	k?
EO-26	EO-26	k1gMnSc1
•	•	k?
EO-27	EO-27	k1gMnSc1
•	•	k?
EO-28	EO-28	k1gMnSc1
návštěvní	návštěvní	k2eAgMnSc1d1
</s>
<s>
EP-1	EP-1	k4
•	•	k?
EP-2	EP-2	k1gMnSc1
•	•	k?
EP-3	EP-3	k1gMnSc1
•	•	k?
EP-4	EP-4	k1gMnSc1
•	•	k?
EP-5	EP-5	k1gMnSc1
•	•	k?
EP-6	EP-6	k1gMnSc1
•	•	k?
EP-7	EP-7	k1gMnSc1
•	•	k?
EP-8	EP-8	k1gMnSc1
•	•	k?
EP-9	EP-9	k1gMnSc1
•	•	k?
EP-10	EP-10	k1gMnSc1
•	•	k?
EP-11	EP-11	k1gMnSc1
•	•	k?
EP-12	EP-12	k1gMnSc1
•	•	k?
EP-13	EP-13	k1gMnSc1
•	•	k?
EP-14	EP-14	k1gMnSc1
•	•	k?
EP-15	EP-15	k1gMnSc1
•	•	k?
EP-16	EP-16	k1gMnSc1
•	•	k?
EP-17	EP-17	k1gFnSc4
raketoplánů	raketoplán	k1gInPc2
</s>
<s>
STS-71	STS-71	k4
•	•	k?
STS-74	STS-74	k1gMnSc1
•	•	k?
STS-76	STS-76	k1gMnSc1
•	•	k?
STS-79	STS-79	k1gMnSc1
•	•	k?
STS-81	STS-81	k1gMnSc1
•	•	k?
STS-84	STS-84	k1gMnSc1
•	•	k?
STS-86	STS-86	k1gMnSc1
•	•	k?
STS-89	STS-89	k1gMnSc1
•	•	k?
STS-91	STS-91	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Kosmonautika	kosmonautika	k1gFnSc1
</s>
