<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
distrikt	distrikt	k1gInSc1	distrikt
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
מ	מ	k?	מ
ה	ה	k?	ה
<g/>
,	,	kIx,	,
mechoz	mechoz	k1gInSc1	mechoz
ha-Cafon	ha-Cafon	k1gInSc1	ha-Cafon
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
ا	ا	k?	ا
<g/>
,	,	kIx,	,
mintaqatu-š-Šamál	mintaqatu-š-Šamál	k1gInSc1	mintaqatu-š-Šamál
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
šesti	šest	k4xCc2	šest
izraelských	izraelský	k2eAgInPc2d1	izraelský
distriktů	distrikt	k1gInPc2	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
od	od	k7c2	od
Golanských	Golanský	k2eAgFnPc2d1	Golanský
výšin	výšina	k1gFnPc2	výšina
k	k	k7c3	k
pohoří	pohoří	k1gNnSc3	pohoří
Karmel	Karmela	k1gFnPc2	Karmela
<g/>
.	.	kIx.	.
</s>
<s>
Začlenění	začlenění	k1gNnSc1	začlenění
Golanských	Golanský	k2eAgFnPc2d1	Golanský
výšin	výšina	k1gFnPc2	výšina
do	do	k7c2	do
distriktu	distrikt	k1gInSc2	distrikt
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
nepovažuje	považovat	k5eNaImIp3nS	považovat
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
Státu	stát	k1gInSc2	stát
Izrael	Izrael	k1gInSc1	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Správním	správní	k2eAgNnSc7d1	správní
centrem	centrum	k1gNnSc7	centrum
je	být	k5eAaImIp3nS	být
Nazaret	Nazaret	k1gInSc1	Nazaret
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
v	v	k7c6	v
distriktu	distrikt	k1gInSc6	distrikt
žilo	žít	k5eAaImAgNnS	žít
1	[number]	k4	1
358	[number]	k4	358
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
630	[number]	k4	630
500	[number]	k4	500
(	(	kIx(	(
<g/>
46,4	[number]	k4	46,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
Židé	Žid	k1gMnPc1	Žid
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
"	"	kIx"	"
a	a	k8xC	a
728	[number]	k4	728
100	[number]	k4	100
(	(	kIx(	(
<g/>
53,6	[number]	k4	53,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
North	North	k1gMnSc1	North
District	District	k1gMnSc1	District
(	(	kIx(	(
<g/>
Israel	Israel	k1gMnSc1	Israel
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
מ	מ	k?	מ
ה	ה	k?	ה
na	na	k7c6	na
hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Dystrykt	Dystrykt	k1gInSc4	Dystrykt
Północny	Północna	k1gFnSc2	Północna
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Golanské	Golanský	k2eAgFnPc4d1	Golanský
výšiny	výšina	k1gFnPc4	výšina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Severní	severní	k2eAgInSc4d1	severní
distrikt	distrikt	k1gInSc4	distrikt
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
