<s>
Valčík	valčík	k1gInSc1	valčík
je	být	k5eAaImIp3nS	být
postupový	postupový	k2eAgInSc1d1	postupový
<g/>
,	,	kIx,	,
kolový	kolový	k2eAgInSc1d1	kolový
tanec	tanec	k1gInSc1	tanec
v	v	k7c6	v
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
taktu	takt	k1gInSc2	takt
nebo	nebo	k8xC	nebo
zřídka	zřídka	k6eAd1	zřídka
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
dříve	dříve	k6eAd2	dříve
<g/>
)	)	kIx)	)
v	v	k7c6	v
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
taktu	takt	k1gInSc2	takt
<g/>
,	,	kIx,	,
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
první	první	k4xOgFnSc4	první
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
