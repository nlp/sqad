<s>
Topeka	Topeka	k6eAd1	Topeka
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
státu	stát	k1gInSc2	stát
Kansas	Kansas	k1gInSc1	Kansas
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řeky	řeka	k1gFnSc2	řeka
Kansas	Kansas	k1gInSc1	Kansas
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Shawnee	Shawne	k1gFnSc2	Shawne
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zastávky	zastávka	k1gFnSc2	zastávka
Oregonské	Oregonský	k2eAgFnSc2d1	Oregonská
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Kansasu	Kansas	k1gInSc2	Kansas
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1861	[number]	k4	1861
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
vydal	vydat	k5eAaPmAgMnS	vydat
starosta	starosta	k1gMnSc1	starosta
Topeky	Topek	k1gInPc1	Topek
Bill	Bill	k1gMnSc1	Bill
Bunten	Bunten	k2eAgInSc1d1	Bunten
prohlášení	prohlášení	k1gNnSc3	prohlášení
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
měsíce	měsíc	k1gInSc2	měsíc
března	březen	k1gInSc2	březen
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaBmF	jmenovat
Google	Google	k1gFnSc1	Google
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Google	Google	k1gInSc1	Google
<g/>
,	,	kIx,	,
Kansas	Kansas	k1gInSc1	Kansas
<g/>
,	,	kIx,	,
the	the	k?	the
capital	capital	k1gMnSc1	capital
city	city	k1gFnSc2	city
of	of	k?	of
fiber	fiber	k1gInSc1	fiber
optics	optics	k1gInSc1	optics
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
pilotní	pilotní	k2eAgInSc4d1	pilotní
projekt	projekt	k1gInSc4	projekt
Google	Google	k1gFnSc2	Google
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
vysokorychlostního	vysokorychlostní	k2eAgInSc2d1	vysokorychlostní
internetu	internet	k1gInSc2	internet
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
127	[number]	k4	127
473	[number]	k4	473
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
76,2	[number]	k4	76,2
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
11,3	[number]	k4	11,3
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
4,7	[number]	k4	4,7
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,9	[number]	k4	4,9
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
13,4	[number]	k4	13,4
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Topeka	Topeek	k1gInSc2	Topeek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Topeka	Topeko	k1gNnSc2	Topeko
-	-	kIx~	-
aktuální	aktuální	k2eAgNnSc1d1	aktuální
počasí	počasí	k1gNnSc1	počasí
Topeka	Topeka	k1gFnSc1	Topeka
-	-	kIx~	-
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
</s>
