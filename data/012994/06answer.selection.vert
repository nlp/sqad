<s>
Binární	binární	k2eAgInSc1d1	binární
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
způsob	způsob	k1gInSc4	způsob
uložení	uložení	k1gNnSc2	uložení
informace	informace	k1gFnSc2	informace
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
definovaný	definovaný	k2eAgMnSc1d1	definovaný
jako	jako	k8xS	jako
konečný	konečný	k2eAgInSc1d1	konečný
počet	počet	k1gInSc1	počet
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
právě	právě	k9	právě
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
označených	označený	k2eAgInPc2d1	označený
0	[number]	k4	0
nebo	nebo	k8xC	nebo
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
