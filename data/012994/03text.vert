<p>
<s>
Binární	binární	k2eAgInSc1d1	binární
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
způsob	způsob	k1gInSc4	způsob
uložení	uložení	k1gNnSc2	uložení
informace	informace	k1gFnSc2	informace
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
definovaný	definovaný	k2eAgMnSc1d1	definovaný
jako	jako	k8xC	jako
konečný	konečný	k2eAgInSc1d1	konečný
počet	počet	k1gInSc1	počet
bitů	bit	k1gInPc2	bit
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
právě	právě	k9	právě
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
označených	označený	k2eAgInPc2d1	označený
0	[number]	k4	0
nebo	nebo	k8xC	nebo
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snadnější	snadný	k2eAgInSc4d2	snadnější
zápis	zápis	k1gInSc4	zápis
uložených	uložený	k2eAgFnPc2d1	uložená
hodnot	hodnota	k1gFnPc2	hodnota
(	(	kIx(	(
<g/>
čísel	číslo	k1gNnPc2	číslo
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
převážně	převážně	k6eAd1	převážně
používá	používat	k5eAaImIp3nS	používat
byte	byte	k1gInSc1	byte
(	(	kIx(	(
<g/>
bajt	bajt	k1gInSc1	bajt
<g/>
)	)	kIx)	)
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
slova	slovo	k1gNnSc2	slovo
osm	osm	k4xCc1	osm
bitů	bit	k1gInPc2	bit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
hodnoty	hodnota	k1gFnSc2	hodnota
binárního	binární	k2eAgInSc2d1	binární
zápisu	zápis	k1gInSc2	zápis
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
binární	binární	k2eAgFnSc1d1	binární
soustava	soustava	k1gFnSc1	soustava
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
poziční	poziční	k2eAgFnSc1d1	poziční
číselná	číselný	k2eAgFnSc1d1	číselná
soustava	soustava	k1gFnSc1	soustava
se	s	k7c7	s
základem	základ	k1gInSc7	základ
dva	dva	k4xCgMnPc1	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Binární	binární	k2eAgInSc4d1	binární
kód	kód	k1gInSc4	kód
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
informace	informace	k1gFnPc4	informace
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zápisu	zápis	k1gInSc6	zápis
hodnot	hodnota	k1gFnPc2	hodnota
použita	použit	k2eAgFnSc1d1	použita
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
označován	označovat	k5eAaImNgInS	označovat
obsah	obsah	k1gInSc1	obsah
souboru	soubor	k1gInSc2	soubor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
strojový	strojový	k2eAgInSc4d1	strojový
kód	kód	k1gInSc4	kód
procesoru	procesor	k1gInSc2	procesor
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
nebo	nebo	k8xC	nebo
data	datum	k1gNnPc1	datum
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgNnPc4d1	jiné
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
člověk	člověk	k1gMnSc1	člověk
nemůže	moct	k5eNaImIp3nS	moct
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
pochopit	pochopit	k5eAaPmF	pochopit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
digitální	digitální	k2eAgInSc1d1	digitální
obrázek	obrázek	k1gInSc1	obrázek
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
informace	informace	k1gFnSc1	informace
uložena	uložen	k2eAgFnSc1d1	uložena
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
opět	opět	k6eAd1	opět
obnovena	obnovit	k5eAaPmNgNnP	obnovit
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
převodu	převod	k1gInSc6	převod
do	do	k7c2	do
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
vždy	vždy	k6eAd1	vždy
nějaké	nějaký	k3yIgNnSc4	nějaký
kódování	kódování	k1gNnSc4	kódování
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
informace	informace	k1gFnSc1	informace
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
číselného	číselný	k2eAgInSc2d1	číselný
zápisu	zápis	k1gInSc2	zápis
(	(	kIx(	(
<g/>
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
i	i	k9	i
zpět	zpět	k6eAd1	zpět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
text	text	k1gInSc4	text
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
dohodnutá	dohodnutý	k2eAgFnSc1d1	dohodnutá
znaková	znakový	k2eAgFnSc1d1	znaková
sada	sada	k1gFnSc1	sada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každému	každý	k3xTgInSc3	každý
znaku	znak	k1gInSc3	znak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nějaké	nějaký	k3yIgNnSc4	nějaký
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
kódování	kódování	k1gNnSc6	kódování
ASCII	ascii	kA	ascii
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
znak	znak	k1gInSc4	znak
písmene	písmeno	k1gNnSc2	písmeno
A	a	k9	a
definován	definován	k2eAgInSc4d1	definován
kód	kód	k1gInSc4	kód
65	[number]	k4	65
(	(	kIx(	(
<g/>
v	v	k7c6	v
desítkové	desítkový	k2eAgFnSc6d1	desítková
soustavě	soustava	k1gFnSc6	soustava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
šestnáctkově	šestnáctkově	k6eAd1	šestnáctkově
jako	jako	k8xS	jako
číslo	číslo	k1gNnSc1	číslo
41	[number]	k4	41
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
0	[number]	k4	0
<g/>
x	x	k?	x
<g/>
41	[number]	k4	41
<g/>
)	)	kIx)	)
a	a	k8xC	a
binárně	binárně	k6eAd1	binárně
1000001	[number]	k4	1000001
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
jsou	být	k5eAaImIp3nP	být
ukládány	ukládán	k2eAgFnPc4d1	ukládána
i	i	k8xC	i
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
procesoru	procesor	k1gInSc2	procesor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každá	každý	k3xTgFnSc1	každý
instrukce	instrukce	k1gFnSc1	instrukce
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
vybraným	vybraný	k2eAgNnSc7d1	vybrané
číslem	číslo	k1gNnSc7	číslo
a	a	k8xC	a
procesor	procesor	k1gInSc1	procesor
musí	muset	k5eAaImIp3nS	muset
toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
po	po	k7c6	po
načtení	načtení	k1gNnSc6	načtení
z	z	k7c2	z
paměti	paměť	k1gFnSc2	paměť
nejprve	nejprve	k6eAd1	nejprve
dekódovat	dekódovat	k5eAaBmF	dekódovat
(	(	kIx(	(
<g/>
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
<g/>
)	)	kIx)	)
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
potom	potom	k6eAd1	potom
ji	on	k3xPp3gFnSc4	on
může	moct	k5eAaImIp3nS	moct
provést	provést	k5eAaPmF	provést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgInPc1d1	různý
počítačové	počítačový	k2eAgInPc1d1	počítačový
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
uloženou	uložený	k2eAgFnSc4d1	uložená
informaci	informace	k1gFnSc4	informace
interpretovat	interpretovat	k5eAaBmF	interpretovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
se	se	k3xPyFc4	se
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
pomocí	pomocí	k7c2	pomocí
programu	program	k1gInSc2	program
označovaného	označovaný	k2eAgMnSc2d1	označovaný
jako	jako	k8xS	jako
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
obrázků	obrázek	k1gInPc2	obrázek
<g/>
,	,	kIx,	,
strojový	strojový	k2eAgInSc4d1	strojový
kód	kód	k1gInSc4	kód
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zobrazit	zobrazit	k5eAaPmF	zobrazit
disassembler	disassembler	k1gInSc1	disassembler
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
zobrazován	zobrazovat	k5eAaImNgInS	zobrazovat
pomocí	pomocí	k7c2	pomocí
textového	textový	k2eAgInSc2d1	textový
editoru	editor	k1gInSc2	editor
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
racionálně	racionálně	k6eAd1	racionálně
rozhodovat	rozhodovat	k5eAaImF	rozhodovat
i	i	k9	i
na	na	k7c6	na
základě	základ	k1gInSc6	základ
dvoustavových	dvoustavový	k2eAgInPc2d1	dvoustavový
podnětů	podnět	k1gInPc2	podnět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Binární	binární	k2eAgInSc1d1	binární
kód	kód	k1gInSc1	kód
poprvé	poprvé	k6eAd1	poprvé
představil	představit	k5eAaPmAgInS	představit
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
německý	německý	k2eAgMnSc1d1	německý
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
<g/>
.	.	kIx.	.
</s>
<s>
Leibniz	Leibniz	k1gMnSc1	Leibniz
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
najít	najít	k5eAaPmF	najít
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
převáděl	převádět	k5eAaImAgInS	převádět
logické	logický	k2eAgNnSc4d1	logické
slovní	slovní	k2eAgNnSc4d1	slovní
prohlášení	prohlášení	k1gNnSc4	prohlášení
na	na	k7c4	na
čistě	čistě	k6eAd1	čistě
matematické	matematický	k2eAgInPc4d1	matematický
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
nápady	nápad	k1gInPc1	nápad
ignorovány	ignorován	k2eAgInPc1d1	ignorován
<g/>
,	,	kIx,	,
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
klasický	klasický	k2eAgInSc4d1	klasický
čínský	čínský	k2eAgInSc4d1	čínský
text	text	k1gInSc4	text
nazvaný	nazvaný	k2eAgInSc4d1	nazvaný
I-ťing	I-ťing	k1gInSc4	I-ťing
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Book	Book	k1gMnSc1	Book
of	of	k?	of
Changes	Changes	k1gMnSc1	Changes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
používá	používat	k5eAaImIp3nS	používat
určitý	určitý	k2eAgInSc1d1	určitý
typ	typ	k1gInSc1	typ
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
jeho	jeho	k3xOp3gFnSc4	jeho
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
život	život	k1gInSc1	život
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zjednodušen	zjednodušen	k2eAgInSc4d1	zjednodušen
nebo	nebo	k8xC	nebo
zredukován	zredukován	k2eAgInSc4d1	zredukován
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
problémů	problém	k1gInPc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
nul	nula	k1gFnPc2	nula
a	a	k8xC	a
jedniček	jednička	k1gFnPc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
Leibiniz	Leibiniz	k1gMnSc1	Leibiniz
nenašel	najít	k5eNaPmAgMnS	najít
využití	využití	k1gNnSc4	využití
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
George	Georg	k1gMnSc2	Georg
Boole	Boole	k1gNnSc2	Boole
vydal	vydat	k5eAaPmAgMnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
článek	článek	k1gInSc1	článek
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
Matematická	matematický	k2eAgFnSc1d1	matematická
analýza	analýza	k1gFnSc1	analýza
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
algebraický	algebraický	k2eAgInSc1d1	algebraický
systém	systém	k1gInSc1	systém
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
booleova	booleův	k2eAgFnSc1d1	booleův
algebra	algebra	k1gFnSc1	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
binárním	binární	k2eAgNnSc6d1	binární
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
ano	ano	k9	ano
<g/>
/	/	kIx~	/
<g/>
ne	ne	k9	ne
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
<g/>
/	/	kIx~	/
<g/>
off	off	k?	off
<g/>
)	)	kIx)	)
přístupu	přístup	k1gInSc2	přístup
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
tří	tři	k4xCgFnPc2	tři
základní	základní	k2eAgFnPc4d1	základní
operace	operace	k1gFnSc2	operace
<g/>
:	:	kIx,	:
AND	Anda	k1gFnPc2	Anda
<g/>
,	,	kIx,	,
OR	OR	kA	OR
a	a	k8xC	a
NOT	nota	k1gFnPc2	nota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
nebyl	být	k5eNaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
si	se	k3xPyFc3	se
postgraduální	postgraduální	k2eAgMnSc1d1	postgraduální
student	student	k1gMnSc1	student
Claude	Claud	k1gInSc5	Claud
Shannon	Shannon	k1gNnSc4	Shannon
z	z	k7c2	z
Massachusettského	massachusettský	k2eAgInSc2d1	massachusettský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
nevšiml	všimnout	k5eNaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
booleovská	booleovský	k2eAgFnSc1d1	booleovská
algebra	algebra	k1gFnSc1	algebra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
elektrickému	elektrický	k2eAgInSc3d1	elektrický
obvodu	obvod	k1gInSc3	obvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
Shannon	Shannon	k1gInSc1	Shannon
napsal	napsat	k5eAaPmAgInS	napsat
svoji	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
jeho	jeho	k3xOp3gNnSc4	jeho
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
.	.	kIx.	.
</s>
<s>
Shannonova	Shannonův	k2eAgFnSc1d1	Shannonova
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
východiskem	východisko	k1gNnSc7	východisko
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
v	v	k7c6	v
praktických	praktický	k2eAgFnPc6d1	praktická
aplikacích	aplikace	k1gFnPc6	aplikace
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
počítače	počítač	k1gInPc1	počítač
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgInPc1d1	elektrický
obvody	obvod	k1gInPc1	obvod
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
==	==	k?	==
</s>
</p>
<p>
<s>
Řetězec	řetězec	k1gInSc1	řetězec
bitů	bit	k1gInPc2	bit
není	být	k5eNaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
typ	typ	k1gInSc1	typ
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
<s>
Binární	binární	k2eAgInSc1d1	binární
systém	systém	k1gInSc1	systém
obecně	obecně	k6eAd1	obecně
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
všechny	všechen	k3xTgInPc4	všechen
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
spínač	spínač	k1gInSc4	spínač
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
systému	systém	k1gInSc6	systém
nebo	nebo	k8xC	nebo
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
test	test	k1gInSc1	test
pravda	pravda	k1gFnSc1	pravda
nebo	nebo	k8xC	nebo
nepravda	nepravda	k1gFnSc1	nepravda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ba	ba	k9	ba
Gua	Gua	k1gFnSc1	Gua
===	===	k?	===
</s>
</p>
<p>
<s>
Ba	ba	k9	ba
Gua	Gua	k1gMnSc1	Gua
(	(	kIx(	(
<g/>
Pa	Pa	kA	Pa
Kua	Kua	k1gFnSc1	Kua
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
osmi	osm	k4xCc2	osm
diagramů	diagram	k1gInPc2	diagram
které	který	k3yIgInPc1	který
v	v	k7c6	v
taoistické	taoistický	k2eAgFnSc6d1	taoistická
kosmologii	kosmologie	k1gFnSc6	kosmologie
reprezentují	reprezentovat	k5eAaImIp3nP	reprezentovat
základní	základní	k2eAgInPc1d1	základní
principy	princip	k1gInPc1	princip
reality	realita	k1gFnSc2	realita
(	(	kIx(	(
<g/>
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
země	zem	k1gFnPc1	zem
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
čínské	čínský	k2eAgFnSc2d1	čínská
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Tchaj-ťi	Tchaj-ť	k1gFnSc6	Tchaj-ť
<g/>
,	,	kIx,	,
Feng-šuej	Feng-šuej	k1gInSc1	Feng-šuej
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Ba	ba	k9	ba
v	v	k7c6	v
názvu	název	k1gInSc6	název
znamená	znamenat	k5eAaImIp3nS	znamenat
8	[number]	k4	8
<g/>
,	,	kIx,	,
gua	gua	k?	gua
označuje	označovat	k5eAaImIp3nS	označovat
věštebný	věštebný	k2eAgInSc4d1	věštebný
obrazec	obrazec	k1gInSc4	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
osmi	osm	k4xCc2	osm
diagramů	diagram	k1gInPc2	diagram
<g/>
,	,	kIx,	,
zvaných	zvaný	k2eAgInPc2d1	zvaný
trigramy	trigram	k1gInPc7	trigram
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
čar	čára	k1gFnPc2	čára
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
přerušená	přerušený	k2eAgFnSc1d1	přerušená
nebo	nebo	k8xC	nebo
souvislá	souvislý	k2eAgFnSc1d1	souvislá
a	a	k8xC	a
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
jin	jin	k?	jin
nebo	nebo	k8xC	nebo
jang	jang	k1gInSc1	jang
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
trigramů	trigram	k1gInPc2	trigram
tvoří	tvořit	k5eAaImIp3nS	tvořit
64	[number]	k4	64
hexagramů	hexagram	k1gInPc2	hexagram
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
Knihy	kniha	k1gFnSc2	kniha
proměn	proměna	k1gFnPc2	proměna
(	(	kIx(	(
<g/>
I-ťing	I-ťing	k1gInSc1	I-ťing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Braillovo	Braillův	k2eAgNnSc4d1	Braillovo
písmo	písmo	k1gNnSc4	písmo
===	===	k?	===
</s>
</p>
<p>
<s>
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
používán	používat	k5eAaImNgInS	používat
slepými	slepý	k2eAgMnPc7d1	slepý
lidmi	člověk	k1gMnPc7	člověk
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
6	[number]	k4	6
pozic	pozice	k1gFnPc2	pozice
teček	tečka	k1gFnPc2	tečka
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
sloupci	sloupec	k1gInSc6	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
bod	bod	k1gInSc1	bod
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
stavy	stav	k1gInPc4	stav
<g/>
,	,	kIx,	,
vystouplý	vystouplý	k2eAgInSc4d1	vystouplý
nebo	nebo	k8xC	nebo
nevystouplý	vystouplý	k2eNgInSc4d1	vystouplý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
IFÁ	IFÁ	kA	IFÁ
Oracle	Oracla	k1gFnSc6	Oracla
===	===	k?	===
</s>
</p>
<p>
<s>
IFÁ	IFÁ	kA	IFÁ
Oracle	Oracle	k1gNnSc1	Oracle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
mezi	mezi	k7c7	mezi
Jorubským	Jorubský	k2eAgNnSc7d1	Jorubský
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
v	v	k7c6	v
Nigérii	Nigérie	k1gFnSc6	Nigérie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
7000	[number]	k4	7000
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
16	[number]	k4	16
hlavních	hlavní	k2eAgMnPc2d1	hlavní
tzv.	tzv.	kA	tzv.
zpívaných	zpívaný	k2eAgMnPc2d1	zpívaný
(	(	kIx(	(
<g/>
Odu	Odu	k1gMnPc2	Odu
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
spojením	spojení	k1gNnSc7	spojení
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
256	[number]	k4	256
(	(	kIx(	(
<g/>
Odus	odusit	k5eAaPmRp2nS	odusit
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
binárním	binární	k2eAgInSc6d1	binární
kódu	kód	k1gInSc6	kód
<g/>
.	.	kIx.	.
</s>
<s>
Kó	Kó	k?	Kó
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
pomocí	pomocí	k7c2	pomocí
linek	linka	k1gFnPc2	linka
<g/>
,	,	kIx,	,
dvou	dva	k4xCgInPc6	dva
linek	linka	k1gFnPc2	linka
nebo	nebo	k8xC	nebo
nul	nula	k1gFnPc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Oracle	Oracle	k6eAd1	Oracle
používá	používat	k5eAaImIp3nS	používat
Babalawos	Babalawos	k1gInSc1	Babalawos
(	(	kIx(	(
<g/>
vysoký	vysoký	k2eAgMnSc1d1	vysoký
kněz	kněz	k1gMnSc1	kněz
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
konzultace	konzultace	k1gFnPc4	konzultace
s	s	k7c7	s
IFA	IFA	kA	IFA
(	(	kIx(	(
<g/>
zdroj	zdroj	k1gInSc1	zdroj
univerzální	univerzální	k2eAgFnSc2d1	univerzální
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Orunmila	Orunmila	k1gFnSc1	Orunmila
(	(	kIx(	(
<g/>
Orisa	Orisa	k1gFnSc1	Orisa
<g/>
,	,	kIx,	,
Boží	boží	k2eAgFnSc2d1	boží
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předpovídání	předpovídání	k1gNnSc1	předpovídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
a	a	k8xC	a
doporučení	doporučení	k1gNnSc4	doporučení
řešení	řešení	k1gNnSc2	řešení
denních	denní	k2eAgFnPc2d1	denní
lidských	lidský	k2eAgFnPc2d1	lidská
otázek	otázka	k1gFnPc2	otázka
o	o	k7c6	o
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
věštění	věštění	k1gNnSc2	věštění
Ifa	Ifa	k1gFnSc2	Ifa
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
do	do	k7c2	do
seznamu	seznam	k1gInSc2	seznam
UNESCO	Unesco	k1gNnSc1	Unesco
"	"	kIx"	"
<g/>
Mistrovská	mistrovský	k2eAgNnPc1d1	mistrovské
díla	dílo	k1gNnPc1	dílo
ústního	ústní	k2eAgNnSc2d1	ústní
a	a	k8xC	a
nehmotného	hmotný	k2eNgNnSc2d1	nehmotné
dědictví	dědictví	k1gNnSc2	dědictví
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Systémy	systém	k1gInPc1	systém
kódování	kódování	k1gNnSc1	kódování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
kódování	kódování	k1gNnSc1	kódování
ASCII	ascii	kA	ascii
===	===	k?	===
</s>
</p>
<p>
<s>
American	American	k1gInSc1	American
Standard	standard	k1gInSc1	standard
Code	Code	k1gFnPc2	Code
for	forum	k1gNnPc2	forum
Information	Information	k1gInSc4	Information
Interchange	Interchange	k1gNnPc7	Interchange
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
americký	americký	k2eAgInSc4d1	americký
standardní	standardní	k2eAgInSc4d1	standardní
kód	kód	k1gInSc4	kód
pro	pro	k7c4	pro
výměnu	výměna	k1gFnSc4	výměna
informací	informace	k1gFnPc2	informace
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
ASCII	ascii	kA	ascii
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
8	[number]	k4	8
<g/>
bitový	bitový	k2eAgInSc1d1	bitový
binární	binární	k2eAgInSc1d1	binární
kód	kód	k1gInSc1	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
text	text	k1gInSc4	text
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
komunikační	komunikační	k2eAgNnPc4d1	komunikační
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
používají	používat	k5eAaImIp3nP	používat
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgNnSc3	každý
písmenu	písmeno	k1gNnSc3	písmeno
nebo	nebo	k8xC	nebo
znaku	znak	k1gInSc3	znak
je	být	k5eAaImIp3nS	být
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
127	[number]	k4	127
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
8	[number]	k4	8
<g/>
bitovém	bitový	k2eAgInSc6d1	bitový
kódu	kód	k1gInSc6	kód
ASCII	ascii	kA	ascii
<g/>
,	,	kIx,	,
malé	malý	k2eAgInPc1d1	malý
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
reprezentováno	reprezentovat	k5eAaImNgNnS	reprezentovat
řetězcem	řetězec	k1gInSc7	řetězec
bitů	bit	k1gInPc2	bit
0	[number]	k4	0
<g/>
1100001	[number]	k4	1100001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kódování	kódování	k1gNnSc1	kódování
BCD	BCD	kA	BCD
===	===	k?	===
</s>
</p>
<p>
<s>
Binary	Binara	k1gFnPc1	Binara
Coded	Coded	k1gMnSc1	Coded
Decimal	Decimal	k1gMnSc1	Decimal
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dvojkově	dvojkově	k6eAd1	dvojkově
reprezentované	reprezentovaný	k2eAgNnSc1d1	reprezentované
dekadické	dekadický	k2eAgNnSc1d1	dekadické
číslo	číslo	k1gNnSc1	číslo
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
BCD	BCD	kA	BCD
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
kódování	kódování	k1gNnSc2	kódování
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
pouze	pouze	k6eAd1	pouze
desítkových	desítkový	k2eAgFnPc2d1	desítková
číslic	číslice	k1gFnPc2	číslice
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
čtveřic	čtveřice	k1gFnPc2	čtveřice
bitů	bit	k1gInPc2	bit
(	(	kIx(	(
<g/>
nibblů	nibbl	k1gInPc2	nibbl
<g/>
)	)	kIx)	)
tím	ten	k3xDgInSc7	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
nibble	nibble	k6eAd1	nibble
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
jedné	jeden	k4xCgFnSc3	jeden
desítkové	desítkový	k2eAgFnSc3d1	desítková
číslici	číslice	k1gFnSc3	číslice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Počátky	počátek	k1gInPc4	počátek
využití	využití	k1gNnSc2	využití
binárního	binární	k2eAgNnSc2d1	binární
kódování	kódování	k1gNnSc2	kódování
==	==	k?	==
</s>
</p>
<p>
<s>
1875	[number]	k4	1875
<g/>
:	:	kIx,	:
Émile	Émile	k1gFnSc1	Émile
Baudot	Baudot	k1gMnSc1	Baudot
<g/>
:	:	kIx,	:
přidal	přidat	k5eAaPmAgInS	přidat
binární	binární	k2eAgInSc1d1	binární
řetězec	řetězec	k1gInSc1	řetězec
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
šifrovacího	šifrovací	k2eAgInSc2d1	šifrovací
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dnešnímu	dnešní	k2eAgNnSc3d1	dnešní
kódování	kódování	k1gNnSc3	kódování
ASCII	ascii	kA	ascii
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
<g/>
:	:	kIx,	:
C.	C.	kA	C.
E.	E.	kA	E.
Wynn-Williams	Wynn-Williamsa	k1gFnPc2	Wynn-Williamsa
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Scale	Scale	k1gFnSc1	Scale
of	of	k?	of
Two	Two	k1gFnSc1	Two
<g/>
"	"	kIx"	"
počítač	počítač	k1gInSc1	počítač
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
<g/>
:	:	kIx,	:
Konrád	Konrád	k1gMnSc1	Konrád
Zuse	Zus	k1gFnSc2	Zus
<g/>
:	:	kIx,	:
počítač	počítač	k1gInSc1	počítač
Z1	Z1	k1gFnSc2	Z1
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
<g/>
:	:	kIx,	:
Alan	alan	k1gInSc1	alan
Turing	Turing	k1gInSc1	Turing
<g/>
:	:	kIx,	:
elektricko-mechanická	elektrickoechanický	k2eAgFnSc1d1	elektricko-mechanický
binární	binární	k2eAgFnSc1d1	binární
násobička	násobička	k1gFnSc1	násobička
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
Atanasoff-Berry	Atanasoff-Berra	k1gFnSc2	Atanasoff-Berra
Computer	computer	k1gInSc1	computer
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
George	Georg	k1gInSc2	Georg
Stibitz	Stibitz	k1gInSc1	Stibitz
<g/>
:	:	kIx,	:
realizace	realizace	k1gFnSc1	realizace
Booleovy	Booleův	k2eAgFnSc2d1	Booleova
algebry	algebra	k1gFnSc2	algebra
logickým	logický	k2eAgInSc7d1	logický
obvodem	obvod	k1gInSc7	obvod
za	za	k7c4	za
využití	využití	k1gNnSc4	využití
elektromechanického	elektromechanický	k2eAgNnSc2d1	elektromechanické
relé	relé	k1gNnSc2	relé
jako	jako	k8xC	jako
přepínacího	přepínací	k2eAgInSc2d1	přepínací
prvku	prvek	k1gInSc2	prvek
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Binární	binární	k2eAgInSc1d1	binární
soubor	soubor	k1gInSc1	soubor
</s>
</p>
