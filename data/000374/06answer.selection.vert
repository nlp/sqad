<s>
Byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
německy	německy	k6eAd1	německy
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Prozess	Prozess	k1gInSc1	Prozess
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc3	rok
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
v	v	k7c6	v
berlínském	berlínský	k2eAgNnSc6d1	berlínské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Die	Die	k1gMnSc5	Die
Schmiede	Schmied	k1gMnSc5	Schmied
<g/>
.	.	kIx.	.
</s>
