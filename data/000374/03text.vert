<s>
Proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
román	román	k1gInSc4	román
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
německy	německy	k6eAd1	německy
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Prozess	Prozess	k1gInSc1	Prozess
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vydán	vydán	k2eAgInSc1d1	vydán
roku	rok	k1gInSc3	rok
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
rok	rok	k1gInSc4	rok
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
)	)	kIx)	)
v	v	k7c6	v
berlínském	berlínský	k2eAgNnSc6d1	berlínské
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Die	Die	k1gMnSc5	Die
Schmiede	Schmied	k1gMnSc5	Schmied
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
román	román	k1gInSc4	román
vydal	vydat	k5eAaPmAgMnS	vydat
Kafkův	Kafkův	k2eAgMnSc1d1	Kafkův
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
společník	společník	k1gMnSc1	společník
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
mládí	mládí	k1gNnSc2	mládí
i	i	k8xC	i
dospělosti	dospělost	k1gFnSc2	dospělost
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
rukopis	rukopis	k1gInSc1	rukopis
byl	být	k5eAaImAgInS	být
bez	bez	k7c2	bez
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
Proces	proces	k1gInSc1	proces
dal	dát	k5eAaPmAgInS	dát
románu	román	k1gInSc2	román
Brod	Brod	k1gInSc1	Brod
podle	podle	k7c2	podle
poznámek	poznámka	k1gFnPc2	poznámka
v	v	k7c6	v
Kafkově	Kafkův	k2eAgInSc6d1	Kafkův
deníku	deník	k1gInSc6	deník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Proces	proces	k1gInSc4	proces
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
má	mít	k5eAaImIp3nS	mít
deset	deset	k4xCc4	deset
kapitol	kapitola	k1gFnPc2	kapitola
se	s	k7c7	s
známkou	známka	k1gFnSc7	známka
autobiografických	autobiografický	k2eAgInPc2d1	autobiografický
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
blíže	blízce	k6eAd2	blízce
neurčeném	určený	k2eNgNnSc6d1	neurčené
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Prokurista	prokurista	k1gMnSc1	prokurista
banky	banka	k1gFnSc2	banka
Josef	Josef	k1gMnSc1	Josef
K.	K.	kA	K.
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
den	den	k1gInSc4	den
svých	svůj	k3xOyFgFnPc2	svůj
třicátých	třicátý	k4xOgFnPc2	třicátý
narozenin	narozeniny	k1gFnPc2	narozeniny
zatčen	zatknout	k5eAaPmNgMnS	zatknout
dvěma	dva	k4xCgMnPc7	dva
úředníky	úředník	k1gMnPc7	úředník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
ponechán	ponechat	k5eAaPmNgInS	ponechat
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
je	být	k5eAaImIp3nS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
k	k	k7c3	k
výslechu	výslech	k1gInSc3	výslech
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
činžáku	činžák	k1gInSc2	činžák
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
shromážděno	shromáždit	k5eAaPmNgNnS	shromáždit
mnoho	mnoho	k4c1	mnoho
neznámých	známý	k2eNgFnPc2d1	neznámá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
K.	K.	kA	K.
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
vedení	vedení	k1gNnSc2	vedení
případu	případ	k1gInSc2	případ
nezná	znát	k5eNaImIp3nS	znát
předmět	předmět	k1gInSc1	předmět
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
nebyl	být	k5eNaImAgInS	být
opět	opět	k6eAd1	opět
zván	zván	k2eAgInSc1d1	zván
<g/>
,	,	kIx,	,
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
půdě	půda	k1gFnSc6	půda
je	být	k5eAaImIp3nS	být
soudní	soudní	k2eAgNnSc4d1	soudní
pracoviště	pracoviště	k1gNnSc4	pracoviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nP	působit
úředníci	úředník	k1gMnPc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
K.	K.	kA	K.
není	být	k5eNaImIp3nS	být
nikdy	nikdy	k6eAd1	nikdy
přítomen	přítomen	k2eAgMnSc1d1	přítomen
standardnímu	standardní	k2eAgInSc3d1	standardní
soudnímu	soudní	k2eAgInSc3d1	soudní
procesu	proces	k1gInSc3	proces
<g/>
,	,	kIx,	,
jen	jen	k9	jen
z	z	k7c2	z
doslechu	doslech	k1gInSc2	doslech
od	od	k7c2	od
starého	starý	k2eAgMnSc2d1	starý
advokáta	advokát	k1gMnSc2	advokát
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
nakonec	nakonec	k6eAd1	nakonec
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
dále	daleko	k6eAd2	daleko
nezastupoval	zastupovat	k5eNaImAgMnS	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
nejasné	jasný	k2eNgFnSc2d1	nejasná
zprávy	zpráva	k1gFnSc2	zpráva
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
jeho	jeho	k3xOp3gFnSc2	jeho
skutečné	skutečný	k2eAgFnSc2d1	skutečná
a	a	k8xC	a
přímé	přímý	k2eAgFnSc2d1	přímá
obhajoby	obhajoba	k1gFnSc2	obhajoba
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
šance	šance	k1gFnSc2	šance
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
přímo	přímo	k6eAd1	přímo
vstoupit	vstoupit	k5eAaPmF	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
ředitele	ředitel	k1gMnSc2	ředitel
banky	banka	k1gFnSc2	banka
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
provést	provést	k5eAaPmF	provést
italského	italský	k2eAgMnSc4d1	italský
klienta	klient	k1gMnSc4	klient
po	po	k7c6	po
pražském	pražský	k2eAgInSc6d1	pražský
chrámu	chrám	k1gInSc6	chrám
na	na	k7c4	na
hostovo	hostův	k2eAgNnSc4d1	hostovo
přání	přání	k1gNnSc4	přání
<g/>
,	,	kIx,	,
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Ital	Ital	k1gMnSc1	Ital
nedostavil	dostavit	k5eNaPmAgMnS	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
Venku	venku	k6eAd1	venku
prší	pršet	k5eAaImIp3nS	pršet
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
,	,	kIx,	,
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
panuje	panovat	k5eAaImIp3nS	panovat
tma	tma	k1gFnSc1	tma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chce	chtít	k5eAaImIp3nS	chtít
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
kostelník	kostelník	k1gMnSc1	kostelník
posunky	posunek	k1gInPc4	posunek
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pod	pod	k7c7	pod
kazatelnou	kazatelna	k1gFnSc7	kazatelna
stojí	stát	k5eAaImIp3nS	stát
farář	farář	k1gMnSc1	farář
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
když	když	k8xS	když
jej	on	k3xPp3gInSc4	on
z	z	k7c2	z
kazatelny	kazatelna	k1gFnSc2	kazatelna
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
knězovo	knězův	k2eAgNnSc1d1	knězovo
oslovení	oslovení	k1gNnSc1	oslovení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Josefe	Josef	k1gMnSc5	Josef
K.	K.	kA	K.
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Je	být	k5eAaImIp3nS	být
překvapen	překvapit	k5eAaPmNgMnS	překvapit
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
se	se	k3xPyFc4	se
otočit	otočit	k5eAaPmF	otočit
a	a	k8xC	a
rozrušen	rozrušen	k2eAgMnSc1d1	rozrušen
míří	mířit	k5eAaImIp3nS	mířit
potemnělým	potemnělý	k2eAgInSc7d1	potemnělý
kostelem	kostel	k1gInSc7	kostel
k	k	k7c3	k
faráři	farář	k1gMnSc3	farář
<g/>
.	.	kIx.	.
</s>
<s>
Dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vězeňského	vězeňský	k2eAgMnSc4d1	vězeňský
kaplana	kaplan	k1gMnSc4	kaplan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
případem	případ	k1gInSc7	případ
obeznámen	obeznámen	k2eAgMnSc1d1	obeznámen
<g/>
.	.	kIx.	.
</s>
<s>
Sděluje	sdělovat	k5eAaImIp3nS	sdělovat
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
případ	případ	k1gInSc1	případ
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
špatně	špatně	k6eAd1	špatně
a	a	k8xC	a
asi	asi	k9	asi
také	také	k9	také
tak	tak	k9	tak
i	i	k9	i
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
k	k	k7c3	k
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ptá	ptat	k5eAaImIp3nS	ptat
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgInPc4	jaký
kroky	krok	k1gInPc4	krok
má	mít	k5eAaImIp3nS	mít
obviněný	obviněný	k2eAgMnSc1d1	obviněný
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rozmluvě	rozmluva	k1gFnSc3	rozmluva
o	o	k7c6	o
klamu	klam	k1gInSc6	klam
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
"	"	kIx"	"
<g/>
o	o	k7c6	o
dveřníkovi	dveřník	k1gMnSc6	dveřník
stojícím	stojící	k2eAgMnSc6d1	stojící
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
<g/>
"	"	kIx"	"
a	a	k8xC	a
uvádějícím	uvádějící	k2eAgMnPc3d1	uvádějící
do	do	k7c2	do
něj	on	k3xPp3gMnSc4	on
muže	muž	k1gMnSc4	muž
z	z	k7c2	z
venkova	venkov	k1gInSc2	venkov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
svých	svůj	k3xOyFgMnPc2	svůj
31	[number]	k4	31
<g/>
.	.	kIx.	.
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
večer	večer	k6eAd1	večer
opět	opět	k6eAd1	opět
zatčen	zatčen	k2eAgInSc1d1	zatčen
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
úředníci	úředník	k1gMnPc1	úředník
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
kamenolomu	kamenolom	k1gInSc2	kamenolom
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
domu	dům	k1gInSc2	dům
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
okraji	okraj	k1gInSc6	okraj
kdosi	kdosi	k3yInSc1	kdosi
otevírá	otevírat	k5eAaImIp3nS	otevírat
okenici	okenice	k1gFnSc4	okenice
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	Hlava	k1gMnSc1	Hlava
Josefa	Josef	k1gMnSc2	Josef
K.	K.	kA	K.
je	být	k5eAaImIp3nS	být
přitisknuta	přitisknout	k5eAaPmNgFnS	přitisknout
na	na	k7c4	na
kámen	kámen	k1gInSc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mužů	muž	k1gMnPc2	muž
jej	on	k3xPp3gInSc4	on
drží	držet	k5eAaImIp3nP	držet
pod	pod	k7c7	pod
hrdlem	hrdlo	k1gNnSc7	hrdlo
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gInSc3	on
vrazí	vrazit	k5eAaPmIp3nP	vrazit
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgMnS	zabít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
soud	soud	k1gInSc1	soud
-	-	kIx~	-
jako	jako	k8xS	jako
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
1	[number]	k4	1
<g/>
/	/	kIx~	/
A-	A-	k1gMnPc2	A-
<g/>
L.	L.	kA	L.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Oden	Oden	k1gNnSc1	Oden
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
948	[number]	k4	948
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
475	[number]	k4	475
<g/>
.	.	kIx.	.
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
knih	kniha	k1gFnPc2	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Monde	Mond	k1gMnSc5	Mond
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Proces	proces	k1gInSc1	proces
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Zápis	zápis	k1gInSc4	zápis
do	do	k7c2	do
čtenářského	čtenářský	k2eAgInSc2d1	čtenářský
deníku	deník	k1gInSc2	deník
-	-	kIx~	-
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
ideovou	ideový	k2eAgFnSc4d1	ideová
interpretaci	interpretace	k1gFnSc4	interpretace
</s>
