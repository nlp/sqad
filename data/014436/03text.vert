<s>
Long	Long	k1gMnSc1
Term	termy	k1gFnPc2
Evolution	Evolution	k1gInSc4
</s>
<s>
Stav	stav	k1gInSc1
nasazení	nasazení	k1gNnSc2
LTE	LTE	kA
sítí	sítí	k1gNnSc1
ve	v	k7c6
světě	svět	k1gInSc6
k	k	k7c3
7	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2014	#num#	k4
–	–	k?
rok	rok	k1gInSc4
předtím	předtím	k6eAd1
měla	mít	k5eAaImAgFnS
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
potupnou	potupný	k2eAgFnSc4d1
světle	světle	k6eAd1
modrou	modrý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
země	zem	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
probíhalo	probíhat	k5eAaImAgNnS
pouze	pouze	k6eAd1
testování	testování	k1gNnSc1
technologie	technologie	k1gFnSc2
LTE	LTE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
je	být	k5eAaImIp3nS
LTE	LTE	kA
v	v	k7c6
komerčním	komerční	k2eAgInSc6d1
provozu	provoz	k1gInSc6
</s>
<s>
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
je	být	k5eAaImIp3nS
síť	síť	k1gFnSc1
LTE	LTE	kA
instalována	instalovat	k5eAaBmNgFnS
nebo	nebo	k8xC
plánována	plánovat	k5eAaImNgFnS
</s>
<s>
Státy	stát	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
probíhá	probíhat	k5eAaImIp3nS
testování	testování	k1gNnSc3
LTE	LTE	kA
</s>
<s>
LTE	LTE	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
3GPP	3GPP	k4
Long	Longa	k1gFnPc2
Term	termy	k1gFnPc2
Evolution	Evolution	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
dlouhodobý	dlouhodobý	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
3	#num#	k4
<g/>
GPP	GPP	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
označení	označení	k1gNnSc1
technologie	technologie	k1gFnSc2
pro	pro	k7c4
vysokorychlostní	vysokorychlostní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
v	v	k7c6
mobilních	mobilní	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
blíží	blížit	k5eAaImIp3nS
požadavkům	požadavek	k1gInPc3
pro	pro	k7c4
sítě	síť	k1gFnPc1
4G	4G	k4
a	a	k8xC
proto	proto	k8xC
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
označována	označován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
3.95	3.95	k4
<g/>
G	G	kA
–	–	k?
„	„	k?
<g/>
téměř	téměř	k6eAd1
<g/>
“	“	k?
čtvrtá	čtvrtá	k1gFnSc1
generace	generace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
v	v	k7c6
zpřístupnění	zpřístupnění	k1gNnSc6
vysokorychlostního	vysokorychlostní	k2eAgInSc2d1
Internetu	Internet	k1gInSc2
pro	pro	k7c4
větší	veliký	k2eAgInSc4d2
počet	počet	k1gInSc4
mobilních	mobilní	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
(	(	kIx(
<g/>
mobilní	mobilní	k2eAgInPc1d1
telefony	telefon	k1gInPc1
<g/>
,	,	kIx,
přenosné	přenosný	k2eAgInPc1d1
počítače	počítač	k1gInPc1
<g/>
,	,	kIx,
Wi-Fi	Wi-F	k1gFnPc1
hotspoty	hotspota	k1gFnPc1
<g/>
,	,	kIx,
webové	webový	k2eAgFnPc1d1
kamery	kamera	k1gFnPc1
<g/>
,	,	kIx,
zařízení	zařízení	k1gNnPc1
na	na	k7c4
sledování	sledování	k1gNnSc4
pomocí	pomoc	k1gFnPc2
GPS	GPS	kA
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefon	telefon	k1gInSc4
používající	používající	k2eAgNnPc4d1
data	datum	k1gNnPc4
(	(	kIx(
<g/>
tj.	tj.	kA
v	v	k7c6
režimu	režim	k1gInSc6
LTE	LTE	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
obvykle	obvykle	k6eAd1
pro	pro	k7c4
uskutečnění	uskutečnění	k1gNnSc4
hlasového	hlasový	k2eAgNnSc2d1
volání	volání	k1gNnSc2
automaticky	automaticky	k6eAd1
přepne	přepnout	k5eAaPmIp3nS
do	do	k7c2
sítě	síť	k1gFnSc2
2G	2G	k4
nebo	nebo	k8xC
3G	3G	k4
(	(	kIx(
<g/>
tzv.	tzv.	kA
CS	CS	kA
fallback	fallback	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
přenos	přenos	k1gInSc1
hlasu	hlas	k1gInSc2
technologií	technologie	k1gFnPc2
VoLTE	volit	k5eAaImRp2nP
využívající	využívající	k2eAgInSc4d1
datový	datový	k2eAgInSc4d1
přenos	přenos	k1gInSc4
(	(	kIx(
<g/>
tj.	tj.	kA
přepojování	přepojování	k1gNnSc1
paketů	paket	k1gInPc2
<g/>
)	)	kIx)
dosud	dosud	k6eAd1
podporují	podporovat	k5eAaImIp3nP
jen	jen	k9
vybrané	vybraný	k2eAgInPc4d1
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Formálně	formálně	k6eAd1
LTE	LTE	kA
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
3G	3G	k4
standardů	standard	k1gInPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnSc7
předchůdcem	předchůdce	k1gMnSc7
je	být	k5eAaImIp3nS
UMTS	UMTS	kA
se	s	k7c7
svými	svůj	k3xOyFgNnPc7
rozšířeními	rozšíření	k1gNnPc7
HSDPA	HSDPA	kA
a	a	k8xC
HSUPA	HSUPA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástupcem	nástupce	k1gMnSc7
LTE	LTE	kA
je	být	k5eAaImIp3nS
LTE	LTE	kA
Advanced	Advanced	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
již	již	k6eAd1
splňuje	splňovat	k5eAaImIp3nS
požadavky	požadavek	k1gInPc4
4	#num#	k4
<g/>
G.	G.	kA
V	v	k7c6
Česku	Česko	k1gNnSc6
je	být	k5eAaImIp3nS
LTE	LTE	kA
experimentálně	experimentálně	k6eAd1
nasazováno	nasazovat	k5eAaImNgNnS
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
budování	budování	k1gNnSc1
celoplošných	celoplošný	k2eAgFnPc2d1
LTE	LTE	kA
sítí	sítí	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
je	být	k5eAaImIp3nS
pokryta	pokryt	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Plné	plný	k2eAgNnSc1d1
pokrytí	pokrytí	k1gNnSc1
ČR	ČR	kA
očekává	očekávat	k5eAaImIp3nS
T-Mobile	T-Mobila	k1gFnSc3
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Teoretická	teoretický	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
LTE	LTE	kA
pro	pro	k7c4
stahování	stahování	k1gNnSc4
(	(	kIx(
<g/>
downlink	downlink	k1gInSc1
<g/>
)	)	kIx)
172,8	172,8	k4
Mbps	Mbpsa	k1gFnPc2
a	a	k8xC
pro	pro	k7c4
odesílání	odesílání	k1gNnSc4
(	(	kIx(
<g/>
uplink	uplink	k1gInSc1
<g/>
)	)	kIx)
57,6	57,6	k4
Mbps	Mbpsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
kapacita	kapacita	k1gFnSc1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
mezi	mezi	k7c4
všechny	všechen	k3xTgMnPc4
účastníky	účastník	k1gMnPc4
připojené	připojený	k2eAgNnSc1d1
k	k	k7c3
danému	daný	k2eAgInSc3d1
zdroji	zdroj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblastech	oblast	k1gFnPc6
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
počtem	počet	k1gInSc7
účastníků	účastník	k1gMnPc2
se	se	k3xPyFc4
tak	tak	k6eAd1
běžné	běžný	k2eAgFnPc4d1
buňky	buňka	k1gFnPc4
pracující	pracující	k2eAgFnPc4d1
obvykle	obvykle	k6eAd1
v	v	k7c6
pásmu	pásmo	k1gNnSc6
800	#num#	k4
MHz	Mhz	kA
zahušťují	zahušťovat	k5eAaImIp3nP
pomocí	pomocí	k7c2
mikrobuněk	mikrobuňka	k1gFnPc2
(	(	kIx(
<g/>
s	s	k7c7
pokrytím	pokrytí	k1gNnSc7
do	do	k7c2
1	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pikobuněk	pikobuněk	k1gInSc1
(	(	kIx(
<g/>
s	s	k7c7
pokrytím	pokrytí	k1gNnSc7
do	do	k7c2
200	#num#	k4
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
pracujících	pracující	k1gMnPc2
ve	v	k7c6
vyšších	vysoký	k2eAgNnPc6d2
pásmech	pásmo	k1gNnPc6
–	–	k?
1800	#num#	k4
<g/>
,	,	kIx,
2100	#num#	k4
a	a	k8xC
2600	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
soukromě	soukromě	k6eAd1
budovaných	budovaný	k2eAgFnPc2d1
femtobuněk	femtobuňka	k1gFnPc2
(	(	kIx(
<g/>
s	s	k7c7
dosahem	dosah	k1gInSc7
desítky	desítka	k1gFnSc2
metrů	metr	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zabezpečují	zabezpečovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
velký	velký	k2eAgInSc1d1
počet	počet	k1gInSc1
účastníků	účastník	k1gMnPc2
na	na	k7c6
malé	malý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
nebude	být	k5eNaImBp3nS
dělit	dělit	k5eAaImF
o	o	k7c4
jediný	jediný	k2eAgInSc4d1
přístupový	přístupový	k2eAgInSc4d1
bod	bod	k1gInSc4
eNodeB	eNodeB	k?
s	s	k7c7
uvedenou	uvedený	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
byla	být	k5eAaImAgFnS
infrastruktura	infrastruktura	k1gFnSc1
800	#num#	k4
MHz	Mhz	kA
již	již	k6eAd1
značně	značně	k6eAd1
rozšířena	rozšířit	k5eAaPmNgFnS
a	a	k8xC
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
byla	být	k5eAaImAgFnS
doplněna	doplnit	k5eAaPmNgFnS
pásmy	pásmo	k1gNnPc7
1800	#num#	k4
a	a	k8xC
2100	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záleží	záležet	k5eAaImIp3nS
na	na	k7c6
operátorovi	operátor	k1gMnSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
pásmo	pásmo	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
a	a	k8xC
v	v	k7c6
jaké	jaký	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
šířce	šířka	k1gFnSc6
použije	použít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pásmo	pásmo	k1gNnSc1
2600	#num#	k4
MHz	Mhz	kA
je	být	k5eAaImIp3nS
na	na	k7c6
okraji	okraj	k1gInSc6
zájmu	zájem	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
roku	rok	k1gInSc2
2018	#num#	k4
zaznamenal	zaznamenat	k5eAaPmAgInS
pomalý	pomalý	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
v	v	k7c6
budování	budování	k1gNnSc6
sítí	síť	k1gFnPc2
v	v	k7c6
tomto	tento	k3xDgNnSc6
pásmu	pásmo	k1gNnSc6
v	v	k7c6
městských	městský	k2eAgFnPc6d1
aglomeracích	aglomerace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
se	se	k3xPyFc4
odvíjí	odvíjet	k5eAaImIp3nS
od	od	k7c2
obsazenosti	obsazenost	k1gFnSc2
zdroje	zdroj	k1gInSc2
signálu	signál	k1gInSc2
<g/>
,	,	kIx,
vzdálenosti	vzdálenost	k1gFnSc2
<g/>
,	,	kIx,
množství	množství	k1gNnSc1
překážek	překážka	k1gFnPc2
<g/>
,	,	kIx,
počasí	počasí	k1gNnSc2
a	a	k8xC
velikosti	velikost	k1gFnSc2
pásma	pásmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šířka	šířka	k1gFnSc1
pásma	pásmo	k1gNnSc2
20	#num#	k4
MHz	Mhz	kA
na	na	k7c6
frekvencích	frekvence	k1gFnPc6
1800	#num#	k4
a	a	k8xC
2600	#num#	k4
MHz	Mhz	kA
poskytuje	poskytovat	k5eAaImIp3nS
přenosovou	přenosový	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
až	až	k9
150	#num#	k4
Mbps	Mbpsa	k1gFnPc2
pro	pro	k7c4
downlink	downlink	k1gInSc4
a	a	k8xC
50	#num#	k4
Mbps	Mbpsa	k1gFnPc2
pro	pro	k7c4
uplink	uplink	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ČR	ČR	kA
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
standardně	standardně	k6eAd1
i	i	k9
menší	malý	k2eAgFnSc2d2
šířky	šířka	k1gFnSc2
pásem	pásmo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
800	#num#	k4
a	a	k8xC
2100	#num#	k4
MHz	Mhz	kA
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
10	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
je	být	k5eAaImIp3nS
přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
okolo	okolo	k7c2
75	#num#	k4
Mbps	Mbpsa	k1gFnPc2
pro	pro	k7c4
downlink	downlink	k1gInSc4
a	a	k8xC
25	#num#	k4
Mbps	Mbpsa	k1gFnPc2
pro	pro	k7c4
uplink	uplink	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
frekvenci	frekvence	k1gFnSc4
900	#num#	k4
MHz	Mhz	kA
Vodafone	Vodafon	k1gInSc5
používal	používat	k5eAaImAgInS
a	a	k8xC
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
stále	stále	k6eAd1
používá	používat	k5eAaImIp3nS
3	#num#	k4
MHz	Mhz	kA
kanál	kanál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
je	být	k5eAaImIp3nS
přenosová	přenosový	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
okolo	okolo	k7c2
22,5	22,5	k4
Mbps	Mbpsa	k1gFnPc2
pro	pro	k7c4
downlink	downlink	k1gInSc4
a	a	k8xC
7,5	7,5	k4
Mbps	Mbpsa	k1gFnPc2
pro	pro	k7c4
uplink	uplink	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standard	standard	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
i	i	k9
menší	malý	k2eAgInSc1d2
frekvenční	frekvenční	k2eAgInSc1d1
blok	blok	k1gInSc1
o	o	k7c6
velikosti	velikost	k1gFnSc6
1,4	1,4	k4
MHz	Mhz	kA
na	na	k7c4
kanál	kanál	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
dokáže	dokázat	k5eAaPmIp3nS
přenášet	přenášet	k5eAaImF
data	datum	k1gNnPc4
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
okolo	okolo	k7c2
10	#num#	k4
Mbps	Mbpsa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
takto	takto	k6eAd1
úzké	úzký	k2eAgInPc4d1
kanály	kanál	k1gInPc4
se	se	k3xPyFc4
v	v	k7c6
ČR	ČR	kA
nevyužívají	využívat	k5eNaImIp3nP
a	a	k8xC
využívat	využívat	k5eAaPmF,k5eAaImF
nejspíš	nejspíš	k9
nebudou	být	k5eNaImBp3nP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
technologii	technologie	k1gFnSc6
LTE	LTE	kA
se	se	k3xPyFc4
již	již	k6eAd1
nepočítá	počítat	k5eNaImIp3nS
s	s	k7c7
odděleným	oddělený	k2eAgInSc7d1
systémem	systém	k1gInSc7
pro	pro	k7c4
přenos	přenos	k1gInSc4
hlasu	hlas	k1gInSc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
použito	použít	k5eAaPmNgNnS
přenosu	přenos	k1gInSc2
hlasu	hlas	k1gInSc2
přes	přes	k7c4
datové	datový	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
spolu	spolu	k6eAd1
s	s	k7c7
LTE	LTE	kA
v	v	k7c6
Česku	Česko	k1gNnSc6
používán	používat	k5eAaImNgInS
tzv.	tzv.	kA
CS	CS	kA
fallback	fallbacka	k1gFnPc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yIgNnSc3,k3yRgNnSc3,k3yQgNnSc3
LTE	LTE	kA
telefon	telefon	k1gInSc1
nepodporující	podporující	k2eNgFnSc4d1
technologii	technologie	k1gFnSc4
VoLTE	volit	k5eAaImRp2nP
přepne	přepnout	k5eAaPmIp3nS
na	na	k7c4
klasickou	klasický	k2eAgFnSc4d1
2	#num#	k4
<g/>
G	G	kA
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
G	G	kA
technologii	technologie	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
uskutečnit	uskutečnit	k5eAaPmF
hlasové	hlasový	k2eAgNnSc4d1
volání	volání	k1gNnSc4
a	a	k8xC
zachovat	zachovat	k5eAaPmF
datové	datový	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
operátor	operátor	k1gMnSc1
mobilní	mobilní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
pokrývá	pokrývat	k5eAaImIp3nS
území	území	k1gNnSc4
oběma	dva	k4xCgFnPc7
technologiemi	technologie	k1gFnPc7
zároveň	zároveň	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volání	volání	k1gNnSc1
přes	přes	k7c4
datové	datový	k2eAgInPc4d1
přenosy	přenos	k1gInPc4
(	(	kIx(
<g/>
VoLTE	volit	k5eAaImRp2nP
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zatím	zatím	k6eAd1
pouze	pouze	k6eAd1
pro	pro	k7c4
vybrané	vybraný	k2eAgInPc4d1
modely	model	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
seznam	seznam	k1gInSc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
dle	dle	k7c2
operátora	operátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
VoLTE	volit	k5eAaImRp2nP
v	v	k7c6
Česku	Česko	k1gNnSc6
spustil	spustit	k5eAaPmAgInS
komerčně	komerčně	k6eAd1
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc3
T-Mobile	T-Mobila	k1gFnSc3
roce	rok	k1gInSc6
2015	#num#	k4
po	po	k7c6
ročním	roční	k2eAgNnSc6d1
testování	testování	k1gNnSc6
<g/>
,	,	kIx,
O2	O2	k1gFnSc6
a	a	k8xC
Vodafone	Vodafon	k1gInSc5
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komerční	komerční	k2eAgInSc1d1
provoz	provoz	k1gInSc1
</s>
<s>
V	v	k7c6
komerčním	komerční	k2eAgInSc6d1
provozu	provoz	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
např.	např.	kA
v	v	k7c6
severských	severský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
či	či	k8xC
Estonsku	Estonsko	k1gNnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
pokryty	pokrýt	k5eAaPmNgFnP
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
hustě	hustě	k6eAd1
osídlené	osídlený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
–	–	k?
v	v	k7c6
Norsku	Norsko	k1gNnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Oslo	Oslo	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
historické	historický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Stockholmu	Stockholm	k1gInSc2
apod.	apod.	kA
Ceny	cena	k1gFnPc1
připojení	připojení	k1gNnSc2
zhruba	zhruba	k6eAd1
odpovídají	odpovídat	k5eAaImIp3nP
cenám	cena	k1gFnPc3
za	za	k7c4
pevné	pevný	k2eAgNnSc4d1
připojení	připojení	k1gNnSc4
(	(	kIx(
<g/>
ADSL	ADSL	kA
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
např.	např.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
stál	stát	k5eAaImAgInS
v	v	k7c6
dubnu	duben	k1gInSc6
2011	#num#	k4
měsíční	měsíční	k2eAgInSc1d1
tarif	tarif	k1gInSc1
bez	bez	k7c2
FUP	FUP	kA
50	#num#	k4
€	€	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
používání	používání	k1gNnSc4
LTE	LTE	kA
předmětem	předmět	k1gInSc7
zkoumání	zkoumání	k1gNnSc2
úřadů	úřad	k1gInPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
dle	dle	k7c2
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
mohou	moct	k5eAaImIp3nP
vysílače	vysílač	k1gInPc1
firmy	firma	k1gFnSc2
LightSquared	LightSquared	k1gMnSc1
pracující	pracující	k1gMnSc1
v	v	k7c6
pásmu	pásmo	k1gNnSc6
1559	#num#	k4
až	až	k9
1610	#num#	k4
MHz	Mhz	kA
rušit	rušit	k5eAaImF
GPS	GPS	kA
navigaci	navigace	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c6
sousedícím	sousedící	k2eAgNnSc6d1
pásmu	pásmo	k1gNnSc6
1525	#num#	k4
až	až	k9
1559	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pásma	pásmo	k1gNnPc1
LTE	LTE	kA
</s>
<s>
LTE	LTE	kA
je	být	k5eAaImIp3nS
šířeno	šířen	k2eAgNnSc1d1
v	v	k7c6
několika	několik	k4yIc2
vysílacích	vysílací	k2eAgNnPc6d1
<g/>
/	/	kIx~
<g/>
přijímacích	přijímací	k2eAgNnPc6d1
frekvenčních	frekvenční	k2eAgNnPc6d1
pásmech	pásmo	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
LTE	LTE	kA
přidělena	přidělen	k2eAgFnSc1d1
pásma	pásmo	k1gNnSc2
1	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
a	a	k8xC
20	#num#	k4
<g/>
;	;	kIx,
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
minimálně	minimálně	k6eAd1
v	v	k7c6
pásmech	pásmo	k1gNnPc6
3	#num#	k4
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pásmo	pásmo	k1gNnSc1
<g/>
(	(	kIx(
<g/>
Band	band	k1gInSc1
<g/>
)	)	kIx)
<g/>
Frekvence	frekvence	k1gFnSc1
<g/>
(	(	kIx(
<g/>
MHz	Mhz	kA
<g/>
)	)	kIx)
<g/>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
12100	#num#	k4
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
s	s	k7c7
technologií	technologie	k1gFnSc7
Huawei	Huawei	k1gNnSc2
SingleRAN	SingleRAN	k1gFnSc2
jej	on	k3xPp3gMnSc4
Vodafone	Vodafon	k1gInSc5
a	a	k8xC
T-Mobile	T-Mobila	k1gFnSc3
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
i	i	k9
pro	pro	k7c4
LTE	LTE	kA
<g/>
,	,	kIx,
protože	protože	k8xS
umožňuje	umožňovat	k5eAaImIp3nS
vysílat	vysílat	k5eAaImF
3G	3G	k4
i	i	k8xC
LTE	LTE	kA
z	z	k7c2
jednoho	jeden	k4xCgInSc2
transceiveru	transceiver	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
výhodu	výhoda	k1gFnSc4
využil	využít	k5eAaPmAgMnS
jako	jako	k9
první	první	k4xOgMnPc1
v	v	k7c6
ČR	ČR	kA
ve	v	k7c6
městech	město	k1gNnPc6
Vodafone	Vodafon	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
31800	#num#	k4
3G	3G	k4
i	i	k8xC
LTE	LTE	kA
<g/>
,	,	kIx,
určeno	určen	k2eAgNnSc1d1
do	do	k7c2
míst	místo	k1gNnPc2
s	s	k7c7
vysokými	vysoký	k2eAgInPc7d1
datovými	datový	k2eAgInPc7d1
nároky	nárok	k1gInPc7
jako	jako	k8xC,k8xS
obchodní	obchodní	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
<g/>
,	,	kIx,
knihovny	knihovna	k1gFnPc4
a	a	k8xC
jiné	jiný	k2eAgInPc4d1
veřejné	veřejný	k2eAgInPc4d1
prostory	prostor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
72600	#num#	k4
Na	na	k7c6
většině	většina	k1gFnSc6
míst	místo	k1gNnPc2
se	se	k3xPyFc4
nepoužívá	používat	k5eNaImIp3nS
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
jako	jako	k9
doplněk	doplněk	k1gInSc1
k	k	k7c3
800	#num#	k4
<g/>
/	/	kIx~
<g/>
1800	#num#	k4
<g/>
/	/	kIx~
<g/>
2100	#num#	k4
pro	pro	k7c4
fungování	fungování	k1gNnSc4
LTE	LTE	kA
Advanced	Advanced	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uplatnění	uplatnění	k1gNnSc1
nachází	nacházet	k5eAaImIp3nS
také	také	k9
v	v	k7c6
pohraničí	pohraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
8900	#num#	k4
Primárně	primárně	k6eAd1
je	být	k5eAaImIp3nS
využíváno	využívat	k5eAaImNgNnS,k5eAaPmNgNnS
pro	pro	k7c4
2	#num#	k4
<g/>
G.	G.	kA
Vodafone	Vodafon	k1gInSc5
toto	tento	k3xDgNnSc4
pásmo	pásmo	k1gNnSc4
používal	používat	k5eAaImAgInS
k	k	k7c3
pokrytí	pokrytí	k1gNnSc3
mimo	mimo	k7c4
velká	velký	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
pásma	pásmo	k1gNnSc2
většinou	většinou	k6eAd1
přechází	přecházet	k5eAaImIp3nS
na	na	k7c4
800	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
nařízení	nařízení	k1gNnSc3
ČTÚ	ČTÚ	kA
<g/>
.	.	kIx.
</s>
<s>
20800	#num#	k4
<g/>
Pokrývá	pokrývat	k5eAaImIp3nS
většinu	většina	k1gFnSc4
území	území	k1gNnSc2
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
přiděleno	přidělit	k5eAaPmNgNnS
LTE	LTE	kA
po	po	k7c6
uvolnění	uvolnění	k1gNnSc6
frekvencí	frekvence	k1gFnPc2
při	při	k7c6
přechodu	přechod	k1gInSc6
na	na	k7c4
digitální	digitální	k2eAgNnSc4d1
televizní	televizní	k2eAgNnSc4d1
vysílání	vysílání	k1gNnSc4
(	(	kIx(
<g/>
původní	původní	k2eAgInPc4d1
televizní	televizní	k2eAgInPc4d1
kanály	kanál	k1gInPc4
61	#num#	k4
<g/>
-	-	kIx~
<g/>
69	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
LTE	LTE	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
Nástup	nástup	k1gInSc1
sítě	síť	k1gFnSc2
LTE	LTE	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
byl	být	k5eAaImAgInS
rozpačitý	rozpačitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
ve	v	k7c6
třech	tři	k4xCgFnPc6
sousedních	sousední	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
–	–	k?
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Německu	Německo	k1gNnSc6
a	a	k8xC
Rakousku	Rakousko	k1gNnSc6
fungovaly	fungovat	k5eAaImAgFnP
komerční	komerční	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
LTE	LTE	kA
již	již	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
v	v	k7c6
letech	léto	k1gNnPc6
2012-13	2012-13	k4
probíhala	probíhat	k5eAaImAgFnS
pouze	pouze	k6eAd1
zkušební	zkušební	k2eAgNnSc4d1
nasazení	nasazení	k1gNnSc4
v	v	k7c6
nevelkých	velký	k2eNgFnPc6d1
lokalitách	lokalita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aukce	aukce	k1gFnSc1
LTE	LTE	kA
kmitočtů	kmitočet	k1gInPc2
v	v	k7c6
pásmech	pásmo	k1gNnPc6
800	#num#	k4
<g/>
,	,	kIx,
1800	#num#	k4
a	a	k8xC
2600	#num#	k4
MHz	Mhz	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
otevřela	otevřít	k5eAaPmAgFnS
cestu	cesta	k1gFnSc4
plošnému	plošný	k2eAgNnSc3d1
nasazení	nasazení	k1gNnSc3
LTE	LTE	kA
<g/>
,	,	kIx,
proběhla	proběhnout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
polovině	polovina	k1gFnSc6
listopadu	listopad	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2014	#num#	k4
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
výstavba	výstavba	k1gFnSc1
sítí	síť	k1gFnPc2
LTE	LTE	kA
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
bylo	být	k5eAaImAgNnS
pokryto	pokrýt	k5eAaPmNgNnS
80	#num#	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2018	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
Asociace	asociace	k1gFnSc1
provozovatelů	provozovatel	k1gMnPc2
mobilních	mobilní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
podle	podle	k7c2
mapových	mapový	k2eAgInPc2d1
podkladů	podklad	k1gInPc2
OpenSignal	OpenSignal	k1gFnSc2
patří	patřit	k5eAaImIp3nS
ČR	ČR	kA
v	v	k7c6
pokrytí	pokrytí	k1gNnSc6
signálem	signál	k1gInSc7
LTE	LTE	kA
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
ke	k	k7c3
špičce	špička	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
map	mapa	k1gFnPc2
v	v	k7c6
článku	článek	k1gInSc6
je	být	k5eAaImIp3nS
plošné	plošný	k2eAgNnSc1d1
pokrytí	pokrytí	k1gNnSc1
signálem	signál	k1gInSc7
LTE	LTE	kA
v	v	k7c6
ČR	ČR	kA
lepší	dobrý	k2eAgMnSc1d2
než	než	k8xS
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
Francii	Francie	k1gFnSc6
nebo	nebo	k8xC
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
na	na	k7c4
úkor	úkor	k1gInSc4
sítí	síť	k1gFnPc2
3	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
hodlají	hodlat	k5eAaImIp3nP
operátoři	operátor	k1gMnPc1
po	po	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
utlumovat	utlumovat	k5eAaImF
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
vypínat	vypínat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Aktuální	aktuální	k2eAgNnSc1d1
pokrytí	pokrytí	k1gNnSc1
LTE	LTE	kA
všech	všecek	k3xTgInPc2
operátorů	operátor	k1gInPc2
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
frekvence	frekvence	k1gFnPc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
přímo	přímo	k6eAd1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Českého	český	k2eAgInSc2d1
telekomunikačního	telekomunikační	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1
pokrytí	pokrytí	k1gNnSc1
území	území	k1gNnSc2
je	být	k5eAaImIp3nS
realizováno	realizován	k2eAgNnSc1d1
v	v	k7c6
pásmu	pásmo	k1gNnSc6
800	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pásmu	pásmo	k1gNnSc6
1800	#num#	k4
MHz	Mhz	kA
je	být	k5eAaImIp3nS
pokryto	pokrýt	k5eAaPmNgNnS
30-60	30-60	k4
%	%	kIx~
území	území	k1gNnSc4
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
koncentrací	koncentrace	k1gFnSc7
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
pásmu	pásmo	k1gNnSc6
2100	#num#	k4
MHz	Mhz	kA
pak	pak	k6eAd1
menší	malý	k2eAgFnPc4d2
oblasti	oblast	k1gFnPc4
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
provozem	provoz	k1gInSc7
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
1	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
plochy	plocha	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pásmo	pásmo	k1gNnSc1
2600	#num#	k4
MHz	Mhz	kA
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
kapacity	kapacita	k1gFnSc2
připojení	připojení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
spustila	spustit	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
Telefónica	Telefónic	k1gInSc2
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
v	v	k7c6
obci	obec	k1gFnSc6
Jesenice	Jesenice	k1gFnSc2
u	u	k7c2
Prahy	Praha	k1gFnSc2
první	první	k4xOgMnSc1
komerční	komerční	k2eAgInSc1d1
provoz	provoz	k1gInSc1
sítě	síť	k1gFnSc2
čtvrté	čtvrtá	k1gFnSc2
generace	generace	k1gFnSc2
LTE	LTE	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2012	#num#	k4
pokryla	pokrýt	k5eAaPmAgFnS
společnost	společnost	k1gFnSc1
T-Mobile	T-Mobila	k1gFnSc3
LTE	LTE	kA
sítí	síť	k1gFnSc7
Mladou	mladá	k1gFnSc7
Boleslav	Boleslav	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
T-Mobile	T-Mobila	k1gFnSc3
zkušební	zkušební	k2eAgInSc4d1
provoz	provoz	k1gInSc4
LTE	LTE	kA
v	v	k7c6
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
a	a	k8xC
části	část	k1gFnSc6
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Vodafone	Vodafon	k1gInSc5
spustil	spustit	k5eAaPmAgInS
ostrý	ostrý	k2eAgInSc4d1
provoz	provoz	k1gInSc4
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Koncem	koncem	k7c2
března	březen	k1gInSc2
2014	#num#	k4
měl	mít	k5eAaImAgMnS
zahájit	zahájit	k5eAaPmF
T-Mobile	T-Mobila	k1gFnSc3
pokrývání	pokrývání	k1gNnSc1
v	v	k7c6
okrese	okres	k1gInSc6
Plzeň	Plzeň	k1gFnSc1
-	-	kIx~
jih	jih	k1gInSc1
(	(	kIx(
<g/>
obce	obec	k1gFnPc1
Roupov	Roupovo	k1gNnPc2
<g/>
,	,	kIx,
Svárkov	Svárkov	k1gInSc1
<g/>
,	,	kIx,
Dolce	dolec	k1gInPc1
<g/>
,	,	kIx,
Plevňov	Plevňov	k1gInSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
listopadu	listopad	k1gInSc2
2013	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
aukce	aukce	k1gFnSc1
LTE	LTE	kA
kmitočtů	kmitočet	k1gInPc2
v	v	k7c6
pásmech	pásmo	k1gNnPc6
800	#num#	k4
<g/>
,	,	kIx,
1800	#num#	k4
a	a	k8xC
2600	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aukce	aukce	k1gFnSc1
nepřinesla	přinést	k5eNaPmAgFnS
konkurenci	konkurence	k1gFnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
čtvrtého	čtvrtý	k4xOgMnSc2
operátora	operátor	k1gMnSc2
a	a	k8xC
stát	stát	k1gInSc1
počítal	počítat	k5eAaImAgInS
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
výnosem	výnos	k1gInSc7
jako	jako	k8xS,k8xC
příjmem	příjem	k1gInSc7
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
pro	pro	k7c4
rok	rok	k1gInSc4
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Stávající	stávající	k2eAgMnPc1d1
tři	tři	k4xCgMnPc1
mobilní	mobilní	k2eAgMnPc1d1
operátoři	operátor	k1gMnPc1
(	(	kIx(
<g/>
Telefonica	Telefonica	k1gMnSc1
O	o	k7c4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
T-Mobile	T-Mobila	k1gFnSc6
a	a	k8xC
Vodafone	Vodafon	k1gInSc5
<g/>
)	)	kIx)
se	se	k3xPyFc4
zavázali	zavázat	k5eAaPmAgMnP
pokrýt	pokrýt	k5eAaPmF
98	#num#	k4
%	%	kIx~
území	území	k1gNnSc6
ČR	ČR	kA
signálem	signál	k1gInSc7
LTE	LTE	kA
do	do	k7c2
pěti	pět	k4xCc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
technologii	technologie	k1gFnSc3
dodává	dodávat	k5eAaImIp3nS
bezpečnostně	bezpečnostně	k6eAd1
kontroverzní	kontroverzní	k2eAgNnSc4d1
Huawei	Huawei	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
obava	obava	k1gFnSc1
o	o	k7c4
interference	interference	k1gFnPc4
s	s	k7c7
některými	některý	k3yIgInPc7
kanály	kanál	k1gInPc7
DVB-	DVB-	k1gFnSc2
<g/>
T.	T.	kA
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
ČTÚ	ČTÚ	kA
chce	chtít	k5eAaImIp3nS
rozšířit	rozšířit	k5eAaPmF
využití	využití	k1gNnSc3
pásma	pásmo	k1gNnSc2
pro	pro	k7c4
CDMA	CDMA	kA
na	na	k7c4
další	další	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
2017	#num#	k4
proběhl	proběhnout	k5eAaPmAgMnS
v	v	k7c6
pásmu	pásmo	k1gNnSc6
1800	#num#	k4
MHz	Mhz	kA
refarming	refarming	k1gInSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
operátoři	operátor	k1gMnPc1
vyměnili	vyměnit	k5eAaPmAgMnP
vydražené	vydražený	k2eAgFnPc4d1
frekvence	frekvence	k1gFnPc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
získali	získat	k5eAaPmAgMnP
souvislé	souvislý	k2eAgInPc4d1
bloky	blok	k1gInPc4
frekvencí	frekvence	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
používají	používat	k5eAaImIp3nP
operátoři	operátor	k1gMnPc1
mobilních	mobilní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
pásma	pásmo	k1gNnSc2
800	#num#	k4
<g/>
,	,	kIx,
900	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
bylo	být	k5eAaImAgNnS
stále	stále	k6eAd1
Vodafonem	Vodafon	k1gInSc7
v	v	k7c6
omezené	omezený	k2eAgFnSc6d1
míře	míra	k1gFnSc6
používáno	používat	k5eAaImNgNnS
asi	asi	k9
10	#num#	k4
vysílačů	vysílač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
se	se	k3xPyFc4
bude	být	k5eAaImBp3nS
stale	stale	k6eAd1
snižovat	snižovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
Vejprty	Vejprta	k1gFnSc2
na	na	k7c6
severu	sever	k1gInSc6
Čech	Čechy	k1gFnPc2
mají	mít	k5eAaImIp3nP
dostupné	dostupný	k2eAgInPc1d1
od	od	k7c2
Vodafonu	Vodafon	k1gInSc2
v	v	k7c6
místě	místo	k1gNnSc6
jen	jen	k9
LTE	LTE	kA
900	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1800	#num#	k4
a	a	k8xC
2100	#num#	k4
a	a	k8xC
2600	#num#	k4
MHz	Mhz	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pásmech	pásmo	k1gNnPc6
800	#num#	k4
<g/>
,	,	kIx,
1800	#num#	k4
a	a	k8xC
2600	#num#	k4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
primárně	primárně	k6eAd1
technologii	technologie	k1gFnSc3
LTE	LTE	kA
<g/>
,	,	kIx,
v	v	k7c6
pásmu	pásmo	k1gNnSc6
900	#num#	k4
MHz	Mhz	kA
klasickou	klasický	k2eAgFnSc7d1
GSM	GSM	kA
a	a	k8xC
v	v	k7c6
pásmu	pásmo	k1gNnSc6
2100	#num#	k4
MHz	Mhz	kA
sítě	síť	k1gFnSc2
třetí	třetí	k4xOgFnSc1
generace	generace	k1gFnSc1
UMTS	UMTS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
LTE	LTE	kA
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
</s>
<s>
Na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
přivedla	přivést	k5eAaPmAgFnS
aukce	aukce	k1gFnSc1
kmitočtů	kmitočet	k1gInPc2
na	na	k7c4
trh	trh	k1gInSc4
nového	nový	k2eAgInSc2d1
čtvrtého	čtvrtý	k4xOgMnSc4
operátora	operátor	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
počátku	počátek	k1gInSc3
roku	rok	k1gInSc2
2020	#num#	k4
pokrývala	pokrývat	k5eAaImAgFnS
firma	firma	k1gFnSc1
Swan	Swan	k1gInSc4
Mobile	mobile	k1gNnPc2
sítí	síť	k1gFnSc7
zvanou	zvaný	k2eAgFnSc7d1
4	#num#	k4
<g/>
ka	ka	k?
v	v	k7c6
pásmu	pásmo	k1gNnSc6
1800	#num#	k4
MHz	Mhz	kA
všechna	všechen	k3xTgNnPc4
hustě	hustě	k6eAd1
obydlená	obydlený	k2eAgNnPc4d1
území	území	k1gNnPc4
Slovenska	Slovensko	k1gNnSc2
(	(	kIx(
<g/>
75	#num#	k4
<g/>
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
rychlostí	rychlost	k1gFnSc7
4	#num#	k4
<g/>
G	G	kA
<g/>
,	,	kIx,
dalších	další	k2eAgNnPc2d1
20	#num#	k4
<g/>
%	%	kIx~
v	v	k7c6
3	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
Technologie	technologie	k1gFnSc1
LTE	LTE	kA
(	(	kIx(
<g/>
i	i	k8xC
návrh	návrh	k1gInSc4
5	#num#	k4
<g/>
G	G	kA
<g/>
)	)	kIx)
obsahuje	obsahovat	k5eAaImIp3nS
bezpečnostní	bezpečnostní	k2eAgFnSc4d1
slabinu	slabina	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
nelze	lze	k6eNd1
odstranit	odstranit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
LTE	LTE	kA
obsahuje	obsahovat	k5eAaImIp3nS
desítky	desítka	k1gFnPc4
zranitelností	zranitelnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
o	o	k7c6
LTE	LTE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
telekomunikační	telekomunikační	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KILIÁN	Kilián	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
je	on	k3xPp3gInPc4
VoLTE	volit	k5eAaImRp2nP
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
Androida	Androida	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-18	2018-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyzkoušeli	vyzkoušet	k5eAaPmAgMnP
jsme	být	k5eAaImIp1nP
internet	internet	k1gInSc4
budoucnosti	budoucnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluční	revoluční	k2eAgInSc1d1
LTE	LTE	kA
mění	měnit	k5eAaImIp3nS
pravidla	pravidlo	k1gNnPc4
hry	hra	k1gFnSc2
<g/>
↑	↑	k?
LTE	LTE	kA
děsí	děsit	k5eAaImIp3nS
americké	americký	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prý	prý	k9
mu	on	k3xPp3gMnSc3
ruší	rušit	k5eAaImIp3nS
GPS	GPS	kA
<g/>
↑	↑	k?
SVOZIL	Svozil	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
s	s	k7c7
LTE	LTE	kA
u	u	k7c2
nás	my	k3xPp1nPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GizChina	GizChina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-11-30	2014-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DVZD	DVZD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trendy	trend	k1gInPc4
a	a	k8xC
novinky	novinka	k1gFnPc4
⇒	⇒	k?
LTE	LTE	kA
-	-	kIx~
pásma	pásmo	k1gNnPc1
a	a	k8xC
frekvence	frekvence	k1gFnSc1
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
AndroidForum	AndroidForum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-09-27	2014-09-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Čtvrtý	čtvrtý	k4xOgMnSc1
operátor	operátor	k1gMnSc1
nebude	být	k5eNaImBp3nS
<g/>
,	,	kIx,
aukci	aukce	k1gFnSc4
LTE	LTE	kA
opanovala	opanovat	k5eAaPmAgFnS
současná	současný	k2eAgFnSc1d1
trojka	trojka	k1gFnSc1
operátorů	operátor	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
19.11	19.11	k4
<g/>
.2013	.2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zkušební	zkušební	k2eAgInSc1d1
provoz	provoz	k1gInSc1
základnových	základnový	k2eAgFnPc2d1
stanice	stanice	k1gFnSc1
LTE	LTE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
telekomunikační	telekomunikační	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BERÁNEK	Beránek	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operátoři	operátor	k1gMnPc1
<g/>
:	:	kIx,
LTE	LTE	kA
už	už	k6eAd1
v	v	k7c6
Česku	Česko	k1gNnSc6
pokrývá	pokrývat	k5eAaImIp3nS
přes	přes	k7c4
80	#num#	k4
procent	procento	k1gNnPc2
populace	populace	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016-01-07	2016-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
V	v	k7c6
pokrytí	pokrytí	k1gNnSc6
signálem	signál	k1gInSc7
je	být	k5eAaImIp3nS
Česko	Česko	k1gNnSc1
evropskou	evropský	k2eAgFnSc7d1
elitou	elita	k1gFnSc7
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
může	moct	k5eAaImIp3nS
tiše	tiš	k1gFnPc4
závidět	závidět	k5eAaImF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-06-28	2018-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DRGÁČ	DRGÁČ	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
;	;	kIx,
ZELENÝ	Zelený	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operátor	operátor	k1gInSc1
vypne	vypnout	k5eAaPmIp3nS
3G	3G	k4
síť	síť	k1gFnSc4
<g/>
,	,	kIx,
starší	starý	k2eAgInPc1d2
mobily	mobil	k1gInPc1
přijdou	přijít	k5eAaPmIp3nP
o	o	k7c4
internet	internet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
ti	ten	k3xDgMnPc1
zbývající	zbývající	k2eAgMnSc1d1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Associated	Associated	k1gMnSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
televize	televize	k1gFnSc1
Nova	nova	k1gFnSc1
<g/>
,	,	kIx,
2020-07-09	2020-07-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Aktuální	aktuální	k2eAgNnSc4d1
pokrytí	pokrytí	k1gNnSc4
LTE	LTE	kA
v	v	k7c6
ČR	ČR	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
telekomunikační	telekomunikační	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pokrytí	pokrytí	k1gNnSc1
|	|	kIx~
Veřejné	veřejný	k2eAgFnPc1d1
širokopásmové	širokopásmový	k2eAgFnPc1d1
mobilní	mobilní	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
LTE	LTE	kA
<g/>
.	.	kIx.
digi	digi	k6eAd1
<g/>
.	.	kIx.
<g/>
ctu	ctu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LTE	LTE	kA
vtrhlo	vtrhnout	k5eAaPmAgNnS
do	do	k7c2
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rychlejší	rychlý	k2eAgFnSc1d2
než	než	k8xS
ADSL	ADSL	kA
<g/>
,	,	kIx,
ale	ale	k8xC
zatím	zatím	k6eAd1
jen	jen	k9
pro	pro	k7c4
vyvolené	vyvolená	k1gFnPc4
<g/>
↑	↑	k?
T-Mobile	T-Mobila	k1gFnSc6
přejmenoval	přejmenovat	k5eAaPmAgMnS
Mladou	mladá	k1gFnSc4
Boleslav	Boleslav	k1gMnSc1
na	na	k7c6
LTE	LTE	kA
City	city	k1gNnSc1
<g/>
↑	↑	k?
Vodafone	Vodafon	k1gInSc5
spouští	spoušť	k1gFnPc2
LTE	LTE	kA
naostro	naostro	k6eAd1
<g/>
,	,	kIx,
T-Mobile	T-Mobila	k1gFnSc3
zkušebně	zkušebně	k6eAd1
<g/>
↑	↑	k?
http://www.parabola.cz/clanky/5200/ctu-chysta-serii-opatreni-pro-pripady-ruseni-televizniho-prijmu/	http://www.parabola.cz/clanky/5200/ctu-chysta-serii-opatreni-pro-pripady-ruseni-televizniho-prijmu/	k4
-	-	kIx~
ČTÚ	ČTÚ	kA
chystá	chystat	k5eAaImIp3nS
sérii	série	k1gFnSc4
opatření	opatření	k1gNnSc2
pro	pro	k7c4
případy	případ	k1gInPc4
rušení	rušení	k1gNnSc2
televizního	televizní	k2eAgInSc2d1
příjmu	příjem	k1gInSc2
<g/>
↑	↑	k?
ČTÚ	ČTÚ	kA
chce	chtít	k5eAaImIp3nS
rozšířit	rozšířit	k5eAaPmF
využití	využití	k1gNnSc3
pásma	pásmo	k1gNnSc2
pro	pro	k7c4
CDMA	CDMA	kA
na	na	k7c4
další	další	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
<g/>
1	#num#	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operátoři	operátor	k1gMnPc1
si	se	k3xPyFc3
po	po	k7c6
aukci	aukce	k1gFnSc6
přehazují	přehazovat	k5eAaImIp3nP
LTE	LTE	kA
frekvence	frekvence	k1gFnSc1
<g/>
.	.	kIx.
e	e	k0
<g/>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-04-17	2017-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovensko	Slovensko	k1gNnSc1
prodalo	prodat	k5eAaPmAgNnS
mobilní	mobilní	k2eAgInPc4d1
kmitočty	kmitočet	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c4
trh	trh	k1gInSc4
vstoupí	vstoupit	k5eAaPmIp3nS
nový	nový	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
,	,	kIx,
idnes	idnes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
8.1	8.1	k4
<g/>
.2014	.2014	k4
<g/>
↑	↑	k?
A.S	A.S	k1gMnSc1
<g/>
,	,	kIx,
SWAN	SWAN	kA
Mobile	mobile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mapa	mapa	k1gFnSc1
pokrytia	pokrytia	k1gFnSc1
-	-	kIx~
Dostupnosť	Dostupnosť	k1gFnSc1
internetu	internet	k1gInSc2
•	•	k?
4	#num#	k4
<g/>
ka	ka	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
.	.	kIx.
4	#num#	k4
<g/>
ka	ka	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://phys.org/news/2018-06-gaps-lte-mobile-telephony-standard.html	https://phys.org/news/2018-06-gaps-lte-mobile-telephony-standard.html	k1gInSc1
-	-	kIx~
Security	Securita	k1gFnSc2
gaps	gaps	k1gInSc1
identified	identified	k1gMnSc1
in	in	k?
LTE	LTE	kA
mobile	mobile	k1gNnSc4
telephony	telephona	k1gFnSc2
standard	standard	k1gInSc4
<g/>
↑	↑	k?
https://techxplore.com/news/2019-03-kaist-team-fuzzing-lte-protocol.html	https://techxplore.com/news/2019-03-kaist-team-fuzzing-lte-protocol.html	k1gInSc1
-	-	kIx~
KAIST	KAIST	kA
team	team	k1gInSc1
used	used	k1gInSc1
fuzzing	fuzzing	k1gInSc1
to	ten	k3xDgNnSc1
spot	spot	k1gInSc1
newer	newer	k1gInSc4
LTE	LTE	kA
protocol	protocol	k1gInSc1
vulnerabilities	vulnerabilities	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
VoLTE	volit	k5eAaImRp2nP
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Standardy	standard	k1gInPc1
mobilních	mobilní	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
0G	0G	k4
</s>
<s>
PTT	PTT	kA
</s>
<s>
MTS	MTS	kA
</s>
<s>
IMTS	IMTS	kA
</s>
<s>
AMTS	AMTS	kA
</s>
<s>
AMR	AMR	kA
</s>
<s>
0.5	0.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
Autotel	Autotel	k1gMnSc1
<g/>
/	/	kIx~
<g/>
PALM	PALM	kA
</s>
<s>
ARP	ARP	kA
1G	1G	k4
</s>
<s>
NMT	NMT	kA
</s>
<s>
AMPS	AMPS	kA
2G	2G	k4
</s>
<s>
GSM	GSM	kA
</s>
<s>
cdmaOne	cdmaOnout	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
iDEN	iDEN	k?
</s>
<s>
D-AMPS	D-AMPS	k?
<g/>
/	/	kIx~
<g/>
IS-	IS-	k1gFnSc1
<g/>
136	#num#	k4
<g/>
/	/	kIx~
<g/>
TDMA	TDMA	kA
</s>
<s>
PDC	PDC	kA
</s>
<s>
2.5	2.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
GPRS	GPRS	kA
</s>
<s>
2.75	2.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
CDMA2000	CDMA2000	k1gFnSc1
1	#num#	k4
<g/>
xRTT	xRTT	k?
</s>
<s>
EDGE	EDGE	kA
</s>
<s>
EGPRS	EGPRS	kA
3G	3G	k4
</s>
<s>
W-CDMA	W-CDMA	k?
</s>
<s>
UMTS	UMTS	kA
</s>
<s>
FOMA	FOMA	kA
</s>
<s>
CDMA2000	CDMA2000	k4
1	#num#	k4
<g/>
xEV	xEV	k?
</s>
<s>
TD-SCDMA	TD-SCDMA	k?
</s>
<s>
3.5	3.5	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSDPA	HSDPA	kA
</s>
<s>
3.75	3.75	k4
<g/>
G	G	kA
<g/>
:	:	kIx,
HSUPA	HSUPA	kA
Pre-	Pre-	k1gFnSc1
<g/>
4	#num#	k4
<g/>
G	G	kA
</s>
<s>
Mobile	mobile	k1gNnSc1
WiMAX	WiMAX	k1gFnSc2
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
e	e	k0
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
(	(	kIx(
<g/>
E-UTRA	E-UTRA	k1gMnSc1
<g/>
)	)	kIx)
4G	4G	k4
</s>
<s>
WiMAX-Advanced	WiMAX-Advanced	k1gMnSc1
(	(	kIx(
<g/>
IEEE	IEEE	kA
802.16	802.16	k4
<g/>
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
LTE	LTE	kA
Advanced	Advanced	k1gInSc4
5G	5G	k4
</s>
<s>
eMBB	eMBB	k?
</s>
<s>
URLLC	URLLC	kA
</s>
<s>
MMTC	MMTC	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
2010010510	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
2010010510	#num#	k4
</s>
