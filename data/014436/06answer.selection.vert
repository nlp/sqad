<s>
LTE	LTE	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
3GPP	3GPP	k4
Long	Longa	k1gFnPc2
Term	termy	k1gFnPc2
Evolution	Evolution	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
dlouhodobý	dlouhodobý	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
3	#num#	k4
<g/>
GPP	GPP	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
telekomunikacích	telekomunikace	k1gFnPc6
označení	označení	k1gNnSc1
technologie	technologie	k1gFnSc2
pro	pro	k7c4
vysokorychlostní	vysokorychlostní	k2eAgInSc4d1
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
v	v	k7c6
mobilních	mobilní	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
blíží	blížit	k5eAaImIp3nS
požadavkům	požadavek	k1gInPc3
pro	pro	k7c4
sítě	síť	k1gFnPc1
4G	4G	k4
a	a	k8xC
proto	proto	k8xC
bývá	bývat	k5eAaImIp3nS
někdy	někdy	k6eAd1
označována	označován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
3.95	3.95	k4
<g/>
G	G	kA
–	–	k?
„	„	k?
<g/>
téměř	téměř	k6eAd1
<g/>
“	“	k?
čtvrtá	čtvrtá	k1gFnSc1
generace	generace	k1gFnSc1
<g/>
.	.	kIx.
</s>