<s>
Délka	délka	k1gFnSc1
</s>
<s>
DélkaNázev	DélkaNázet	k5eAaPmDgInS
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
,	,	kIx,
l	l	kA
Hlavní	hlavní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
SI	se	k3xPyFc3
</s>
<s>
metr	metr	k1gInSc1
Značka	značka	k1gFnSc1
hlavní	hlavní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
SI	se	k3xPyFc3
</s>
<s>
m	m	kA
Rozměrový	rozměrový	k2eAgInSc4d1
symbol	symbol	k1gInSc4
SI	se	k3xPyFc3
</s>
<s>
L	L	kA
Dle	dle	k7c2
transformace	transformace	k1gFnSc2
složek	složka	k1gFnPc2
</s>
<s>
skalární	skalární	k2eAgNnSc1d1
Zařazení	zařazení	k1gNnSc1
v	v	k7c6
soustavě	soustava	k1gFnSc6
SI	se	k3xPyFc3
</s>
<s>
základní	základní	k2eAgInSc1d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
fyzikální	fyzikální	k2eAgFnSc6d1
veličině	veličina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Délka	délka	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
ze	z	k7c2
základních	základní	k2eAgFnPc2d1
fyzikálních	fyzikální	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
délky	délka	k1gFnSc2
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
vzájemná	vzájemný	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
a	a	k8xC
rozprostraněnost	rozprostraněnost	k1gFnSc1
objektů	objekt	k1gInPc2
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
jako	jako	k8xS,k8xC
fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
geometrického	geometrický	k2eAgInSc2d1
pojmu	pojem	k1gInSc2
délky	délka	k1gFnSc2
jako	jako	k8xS,k8xC
míry	míra	k1gFnSc2
ohraničené	ohraničený	k2eAgFnSc2d1
části	část	k1gFnSc2
křivky	křivka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závisí	záviset	k5eAaImIp3nS
však	však	k9
také	také	k9
na	na	k7c6
fyzikálním	fyzikální	k2eAgNnSc6d1
chápání	chápání	k1gNnSc6
prostoru	prostor	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
relativnosti	relativnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
běžném	běžný	k2eAgNnSc6d1
nefyzikálním	fyzikální	k2eNgNnSc6d1
použití	použití	k1gNnSc6
se	se	k3xPyFc4
délkou	délka	k1gFnSc7
charakterizuje	charakterizovat	k5eAaBmIp3nS
velikost	velikost	k1gFnSc1
nejdelšího	dlouhý	k2eAgInSc2d3
rozměru	rozměr	k1gInSc2
určitého	určitý	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
či	či	k8xC
abstraktního	abstraktní	k2eAgInSc2d1
objektu	objekt	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
přímou	přímý	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
dvou	dva	k4xCgMnPc2
jeho	jeho	k3xOp3gInPc2
krajních	krajní	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k1gNnSc1
délkové	délkový	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
mají	mít	k5eAaImIp3nP
speciální	speciální	k2eAgInPc4d1
názvy	název	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
v	v	k7c6
běžném	běžný	k2eAgNnSc6d1
použití	použití	k1gNnSc6
</s>
<s>
U	u	k7c2
jednorozměrných	jednorozměrný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
úsečka	úsečka	k1gFnSc1
<g/>
,	,	kIx,
spojnice	spojnice	k1gFnSc1
bodů	bod	k1gInPc2
<g/>
;	;	kIx,
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
úhlopříčka	úhlopříčka	k1gFnSc1
<g/>
,	,	kIx,
těžnice	těžnice	k1gFnSc1
nebo	nebo	k8xC
jiná	jiný	k2eAgFnSc1d1
významná	významný	k2eAgFnSc1d1
úsečka	úsečka	k1gFnSc1
plošného	plošný	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
<g/>
;	;	kIx,
hrana	hrana	k1gFnSc1
<g/>
,	,	kIx,
tělesová	tělesový	k2eAgFnSc1d1
úhlopříčka	úhlopříčka	k1gFnSc1
<g/>
,	,	kIx,
těžnice	těžnice	k1gFnSc1
nebo	nebo	k8xC
jiná	jiný	k2eAgFnSc1d1
významná	významný	k2eAgFnSc1d1
úsečka	úsečka	k1gFnSc1
tělesa	těleso	k1gNnSc2
apod.	apod.	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
délka	délka	k1gFnSc1
jeho	jeho	k3xOp3gInSc7
jediným	jediný	k2eAgInSc7d1
rozměrem	rozměr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vícerozměrných	vícerozměrný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
(	(	kIx(
<g/>
dvourozměrných	dvourozměrný	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
a	a	k8xC
trojrozměrných	trojrozměrný	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jako	jako	k9
délka	délka	k1gFnSc1
označován	označovat	k5eAaImNgInS
zpravidla	zpravidla	k6eAd1
větší	veliký	k2eAgInSc4d2
resp.	resp.	kA
největší	veliký	k2eAgInSc4d3
rozměr	rozměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
uplatňuje	uplatňovat	k5eAaImIp3nS
rozlišení	rozlišení	k1gNnSc4
rozměrů	rozměr	k1gInPc2
těles	těleso	k1gNnPc2
na	na	k7c6
</s>
<s>
šířku	šířka	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
tloušťku	tloušťka	k1gFnSc4
<g/>
,	,	kIx,
</s>
<s>
výšku	výška	k1gFnSc4
</s>
<s>
hloubku	hloubka	k1gFnSc4
</s>
<s>
podle	podle	k7c2
různých	různý	k2eAgNnPc2d1
kriterií	kriterium	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
poměrů	poměr	k1gInPc2
rozměrů	rozměr	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
orientace	orientace	k1gFnSc2
stran	strana	k1gFnPc2
tělesa	těleso	k1gNnSc2
vůči	vůči	k7c3
pozorovateli	pozorovatel	k1gMnSc3
a	a	k8xC
</s>
<s>
orientace	orientace	k1gFnSc2
stran	strana	k1gFnPc2
tělesa	těleso	k1gNnSc2
vůči	vůči	k7c3
zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
O	o	k7c6
"	"	kIx"
<g/>
délce	délka	k1gFnSc6
<g/>
"	"	kIx"
nehovoříme	hovořit	k5eNaImIp1nP
v	v	k7c6
případě	případ	k1gInSc6
shody	shoda	k1gFnSc2
všech	všecek	k3xTgInPc2
kolmých	kolmý	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
objektu	objekt	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
čtverec	čtverec	k1gInSc1
<g/>
,	,	kIx,
kruh	kruh	k1gInSc1
<g/>
,	,	kIx,
krychle	krychle	k1gFnSc1
nebo	nebo	k8xC
koule	koule	k1gFnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
u	u	k7c2
tělesa	těleso	k1gNnSc2
s	s	k7c7
nepříliš	příliš	k6eNd1
větším	veliký	k2eAgInSc7d2
třetím	třetí	k4xOgInSc7
rozměrem	rozměr	k1gInSc7
(	(	kIx(
<g/>
"	"	kIx"
<g/>
výškou	výška	k1gFnSc7
<g/>
"	"	kIx"
<g/>
)	)	kIx)
,	,	kIx,
než	než	k8xS
dva	dva	k4xCgInPc1
shodné	shodný	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
v	v	k7c6
geometrii	geometrie	k1gFnSc6
</s>
<s>
Délkou	délka	k1gFnSc7
úsečky	úsečka	k1gFnSc2
je	být	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc1
jejich	jejich	k3xOp3gInPc2
koncových	koncový	k2eAgInPc2d1
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délku	délka	k1gFnSc4
křivky	křivka	k1gFnSc2
lze	lze	k6eAd1
určit	určit	k5eAaPmF
jako	jako	k8xC,k8xS
limitu	limita	k1gFnSc4
délek	délka	k1gFnPc2
lomených	lomený	k2eAgFnPc2d1
čar	čára	k1gFnPc2
složených	složený	k2eAgFnPc2d1
z	z	k7c2
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
úseků	úsek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
</s>
<s>
n	n	k0
</s>
<s>
→	→	k?
</s>
<s>
∞	∞	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
\	\	kIx~
<g/>
to	ten	k3xDgNnSc1
\	\	kIx~
<g/>
infty	inft	k1gInPc4
}	}	kIx)
</s>
<s>
se	se	k3xPyFc4
délka	délka	k1gFnSc1
každé	každý	k3xTgFnSc2
úsečky	úsečka	k1gFnSc2
lomené	lomený	k2eAgInPc1d1
čáry	čár	k1gInPc1
blíží	blížit	k5eAaImIp3nP
nule	nula	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Vzdáleností	vzdálenost	k1gFnSc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
délka	délka	k1gFnSc1
nejkratší	krátký	k2eAgFnSc1d3
spojnice	spojnice	k1gFnSc1
dvou	dva	k4xCgInPc2
objektů	objekt	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
body	bod	k1gInPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
bodem	bod	k1gInSc7
a	a	k8xC
přímkou	přímka	k1gFnSc7
<g/>
,	,	kIx,
dvěma	dva	k4xCgFnPc7
rovnoběžnými	rovnoběžný	k2eAgFnPc7d1
přímkami	přímka	k1gFnPc7
apod.	apod.	kA
</s>
<s>
Délku	délka	k1gFnSc4
obvykle	obvykle	k6eAd1
vztahujeme	vztahovat	k5eAaImIp1nP
k	k	k7c3
charakteristickým	charakteristický	k2eAgInPc3d1
bodům	bod	k1gInPc3
či	či	k8xC
hranicím	hranice	k1gFnPc3
určitého	určitý	k2eAgInSc2d1
plošného	plošný	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
nebo	nebo	k8xC
tělesa	těleso	k1gNnSc2
<g/>
,	,	kIx,
např.	např.	kA
délka	délka	k1gFnSc1
úsečky	úsečka	k1gFnSc2
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
strany	strana	k1gFnSc2
krychle	krychle	k1gFnSc2
apod.	apod.	kA
</s>
<s>
Délka	délka	k1gFnSc1
jako	jako	k8xC,k8xS
fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
</s>
<s>
Délku	délka	k1gFnSc4
jako	jako	k8xC,k8xS
fyzikální	fyzikální	k2eAgFnSc4d1
veličinu	veličina	k1gFnSc4
lze	lze	k6eAd1
v	v	k7c6
nejobecnějším	obecní	k2eAgInSc6d3
smyslu	smysl	k1gInSc6
chápat	chápat	k5eAaImF
nejen	nejen	k6eAd1
jako	jako	k8xC,k8xS
skalární	skalární	k2eAgFnSc4d1
veličinu	veličina	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
některých	některý	k3yIgInPc6
významech	význam	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
charakterizuje	charakterizovat	k5eAaBmIp3nS
přímou	přímý	k2eAgFnSc4d1
orientovanou	orientovaný	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
jí	on	k3xPp3gFnSc3
lze	lze	k6eAd1
přiřadit	přiřadit	k5eAaPmF
vektorový	vektorový	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
i	i	k9
speciální	speciální	k2eAgInSc4d1
název	název	k1gInSc4
(	(	kIx(
<g/>
polohový	polohový	k2eAgInSc4d1
vektor	vektor	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
geometrického	geometrický	k2eAgNnSc2d1
<g/>
,	,	kIx,
absolutního	absolutní	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
délky	délka	k1gFnSc2
<g/>
,	,	kIx,
zohledňuje	zohledňovat	k5eAaImIp3nS
fyzikální	fyzikální	k2eAgNnSc4d1
chápání	chápání	k1gNnSc4
délky	délka	k1gFnSc2
její	její	k3xOp3gFnSc1
relativnost	relativnost	k1gFnSc1
a	a	k8xC
kvantová	kvantový	k2eAgNnPc1d1
omezení	omezení	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s>
Jednotky	jednotka	k1gFnPc1
a	a	k8xC
značení	značení	k1gNnSc1
</s>
<s>
Značka	značka	k1gFnSc1
<g/>
:	:	kIx,
normy	norma	k1gFnPc1
a	a	k8xC
učebnice	učebnice	k1gFnPc1
doporučují	doporučovat	k5eAaImIp3nP
různá	různý	k2eAgNnPc4d1
označení	označení	k1gNnPc4
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
charakterizující	charakterizující	k2eAgInSc1d1
„	„	k?
<g/>
druh	druh	k1gInSc1
<g/>
“	“	k?
délky	délka	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
l	l	kA
<g/>
,	,	kIx,
d	d	k?
<g/>
,	,	kIx,
x	x	k?
(	(	kIx(
<g/>
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
,	,	kIx,
<g/>
…	…	k?
(	(	kIx(
<g/>
délky	délka	k1gFnSc2
úseček	úsečka	k1gFnPc2
<g/>
/	/	kIx~
<g/>
hran	hran	k1gInSc1
těles	těleso	k1gNnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
vizte	vidět	k5eAaImRp2nP
též	též	k9
oddíl	oddíl	k1gInSc4
Příbuzné	příbuzný	k2eAgFnSc2d1
veličiny	veličina	k1gFnSc2
níže	nízce	k6eAd2
</s>
<s>
Základní	základní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
v	v	k7c6
SI	SI	kA
<g/>
:	:	kIx,
metr	metr	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
m	m	kA
</s>
<s>
Další	další	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
</s>
<s>
dekadické	dekadický	k2eAgInPc4d1
násobky	násobek	k1gInPc4
a	a	k8xC
díly	díl	k1gInPc4
<g/>
:	:	kIx,
kilometr	kilometr	k1gInSc4
(	(	kIx(
<g/>
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
decimetr	decimetr	k1gInSc1
(	(	kIx(
<g/>
dm	dm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
centimetr	centimetr	k1gInSc1
(	(	kIx(
<g/>
cm	cm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
milimetr	milimetr	k1gInSc1
(	(	kIx(
<g/>
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mikrometr	mikrometr	k1gInSc1
(	(	kIx(
<g/>
μ	μ	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
å	å	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
Å	Å	k?
=	=	kIx~
10	#num#	k4
<g/>
−	−	k?
<g/>
10	#num#	k4
m	m	kA
<g/>
)	)	kIx)
</s>
<s>
přirozené	přirozený	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
délky	délka	k1gFnSc2
<g/>
:	:	kIx,
Planckova	Planckův	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
,	,	kIx,
atomová	atomový	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
délky	délka	k1gFnSc2
a	a	k8xC
<g/>
0	#num#	k4
(	(	kIx(
<g/>
=	=	kIx~
Bohrův	Bohrův	k2eAgInSc1d1
poloměr	poloměr	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
jednotky	jednotka	k1gFnPc1
v	v	k7c6
astronomii	astronomie	k1gFnSc6
<g/>
:	:	kIx,
astronomická	astronomický	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
(	(	kIx(
<g/>
AU	au	k0
<g/>
,	,	kIx,
UA	UA	kA
<g/>
,	,	kIx,
au	au	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
světelný	světelný	k2eAgInSc4d1
rok	rok	k1gInSc4
(	(	kIx(
<g/>
ly	ly	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
parsec	parsec	k1gMnSc1
(	(	kIx(
<g/>
pc	pc	k?
<g/>
)	)	kIx)
</s>
<s>
anglo-americké	anglo-americký	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
:	:	kIx,
palec	palec	k1gInSc1
<g/>
,	,	kIx,
stopa	stopa	k1gFnSc1
<g/>
,	,	kIx,
yard	yard	k1gInSc1
<g/>
,	,	kIx,
míle	míle	k1gFnSc1
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnSc1d1
míle	míle	k1gFnSc1
</s>
<s>
starší	starší	k1gMnSc5
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
nepoužívané	používaný	k2eNgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
:	:	kIx,
sáh	sáh	k1gInSc1
<g/>
,	,	kIx,
loket	loket	k1gInSc1
<g/>
,	,	kIx,
versta	versta	k1gFnSc1
<g/>
,	,	kIx,
bara	bara	k1gFnSc1
<g/>
,	,	kIx,
decimo	decima	k1gFnSc5
…	…	k?
</s>
<s>
Měření	měření	k1gNnSc1
délky	délka	k1gFnSc2
</s>
<s>
Měřicí	měřicí	k2eAgInPc1d1
nástroje	nástroj	k1gInPc1
a	a	k8xC
přístroje	přístroj	k1gInPc1
délky	délka	k1gFnSc2
jsou	být	k5eAaImIp3nP
</s>
<s>
Pevné	pevný	k2eAgNnSc4d1
<g/>
:	:	kIx,
pravítko	pravítko	k1gNnSc4
<g/>
,	,	kIx,
pevný	pevný	k2eAgInSc4d1
metr	metr	k1gInSc4
(	(	kIx(
<g/>
např.	např.	kA
tyčový	tyčový	k2eAgInSc1d1
<g/>
)	)	kIx)
</s>
<s>
Skladné	skladné	k1gNnSc1
<g/>
:	:	kIx,
skládací	skládací	k2eAgFnSc1d1
<g/>
,	,	kIx,
svinovací	svinovací	k2eAgFnSc1d1
(	(	kIx(
<g/>
často	často	k6eAd1
mnohem	mnohem	k6eAd1
delší	dlouhý	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
1	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
samosvinovací	samosvinovací	k2eAgInSc1d1
<g/>
,	,	kIx,
krejčovský	krejčovský	k2eAgInSc1d1
metr	metr	k1gInSc1
<g/>
,	,	kIx,
pásmo	pásmo	k1gNnSc1
<g/>
,	,	kIx,
</s>
<s>
Měřidlo	měřidlo	k1gNnSc1
s	s	k7c7
měrným	měrný	k2eAgInSc7d1
kotoučem	kotouč	k1gInSc7
<g/>
,	,	kIx,
</s>
<s>
pro	pro	k7c4
malé	malý	k2eAgInPc4d1
rozměry	rozměr	k1gInPc4
<g/>
:	:	kIx,
posuvné	posuvný	k2eAgNnSc4d1
měřítko	měřítko	k1gNnSc4
(	(	kIx(
<g/>
šuplera	šuplera	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mikrometr	mikrometr	k1gInSc1
</s>
<s>
Laserové	laserový	k2eAgNnSc1d1
měřidlo	měřidlo	k1gNnSc1
</s>
<s>
Příbuzné	příbuzný	k2eAgFnPc1d1
veličiny	veličina	k1gFnPc1
</s>
<s>
Stejný	stejný	k2eAgInSc1d1
charakter	charakter	k1gInSc1
jako	jako	k8xC,k8xS
délka	délka	k1gFnSc1
má	mít	k5eAaImIp3nS
mnoho	mnoho	k4c4
fyzikálních	fyzikální	k2eAgFnPc2d1
veličin	veličina	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
závorce	závorka	k1gFnSc6
doporučené	doporučený	k2eAgFnSc2d1
značky	značka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
jiné	jiný	k2eAgInPc1d1
názvy	název	k1gInPc1
charakteristických	charakteristický	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
útvarů	útvar	k1gInPc2
a	a	k8xC
těles	těleso	k1gNnPc2
<g/>
:	:	kIx,
šířka	šířka	k1gFnSc1
<g/>
,	,	kIx,
tloušťka	tloušťka	k1gFnSc1
<g/>
,	,	kIx,
výška	výška	k1gFnSc1
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
a	a	k8xC
<g/>
,	,	kIx,
b	b	k?
<g/>
,	,	kIx,
c	c	k0
pro	pro	k7c4
rozměry	rozměr	k1gInPc4
těles	těleso	k1gNnPc2
tvaru	tvar	k1gInSc2
hranolu	hranol	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
pro	pro	k7c4
svislý	svislý	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
,	,	kIx,
d	d	k?
pro	pro	k7c4
tloušťku	tloušťka	k1gFnSc4
vrstvy	vrstva	k1gFnSc2
<g/>
)	)	kIx)
,	,	kIx,
poloměr	poloměr	k1gInSc1
(	(	kIx(
<g/>
r	r	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
průměr	průměr	k1gInSc1
(	(	kIx(
<g/>
d	d	k?
<g/>
)	)	kIx)
</s>
<s>
charakteristiky	charakteristika	k1gFnPc1
umístění	umístění	k1gNnSc2
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
:	:	kIx,
výška	výška	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
<g/>
,	,	kIx,
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hloubku	hloubka	k1gFnSc4
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
polohový	polohový	k2eAgInSc1d1
vektor	vektor	k1gInSc1
(	(	kIx(
<g/>
r	r	kA
<g/>
,	,	kIx,
R	R	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
souřadnice	souřadnice	k1gFnSc1
(	(	kIx(
<g/>
x	x	k?
<g/>
,	,	kIx,
y	y	k?
<g/>
,	,	kIx,
z	z	k7c2
/	/	kIx~
x	x	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
x	x	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
x	x	k?
<g/>
3	#num#	k4
/	/	kIx~
h	h	k?
<g/>
1	#num#	k4
<g/>
,	,	kIx,
h	h	k?
<g/>
2	#num#	k4
<g/>
,	,	kIx,
h	h	k?
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
délka	délka	k1gFnSc1
dráhy	dráha	k1gFnSc2
(	(	kIx(
<g/>
trajektorie	trajektorie	k1gFnSc2
<g/>
)	)	kIx)
pohybu	pohyb	k1gInSc2
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
</s>
<s>
délkové	délkový	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
vlnění	vlnění	k1gNnSc2
<g/>
:	:	kIx,
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
(	(	kIx(
<g/>
λ	λ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Comptonova	Comptonův	k2eAgFnSc1d1
vlnová	vlnový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
(	(	kIx(
<g/>
λ	λ	k?
<g/>
)	)	kIx)
…	…	k?
</s>
<s>
délkové	délkový	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
dosahu	dosah	k1gInSc2
pole	pole	k1gNnSc2
<g/>
/	/	kIx~
<g/>
vlnění	vlnění	k1gNnSc2
<g/>
/	/	kIx~
<g/>
záření	záření	k1gNnSc2
<g/>
/	/	kIx~
<g/>
tepla	teplo	k1gNnSc2
<g/>
:	:	kIx,
hloubka	hloubka	k1gFnSc1
průniku	průnik	k1gInSc2
(	(	kIx(
<g/>
d	d	k?
<g/>
,	,	kIx,
λ	λ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
délka	délka	k1gFnSc1
zeslabení	zeslabení	k1gNnSc2
<g/>
(	(	kIx(
<g/>
λ	λ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
polotloušťka	polotloušťka	k1gFnSc1
<g/>
(	(	kIx(
<g/>
d½	d½	k?
<g/>
)	)	kIx)
<g/>
…	…	k?
</s>
<s>
délkové	délkový	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
optických	optický	k2eAgFnPc2d1
soustav	soustava	k1gFnPc2
<g/>
:	:	kIx,
ohnisková	ohniskový	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
(	(	kIx(
<g/>
f	f	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
optický	optický	k2eAgInSc4d1
interval	interval	k1gInSc4
soustavy	soustava	k1gFnSc2
(	(	kIx(
<g/>
Δ	Δ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
délkové	délkový	k2eAgFnPc1d1
charakteristiky	charakteristika	k1gFnPc1
atomu	atom	k1gInSc2
<g/>
/	/	kIx~
<g/>
částic	částice	k1gFnPc2
<g/>
:	:	kIx,
Bohrův	Bohrův	k2eAgInSc1d1
poloměr	poloměr	k1gInSc1
(	(	kIx(
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
klasický	klasický	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
elektronu	elektron	k1gInSc2
<g/>
(	(	kIx(
<g/>
re	re	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
…	…	k?
</s>
<s>
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Relativnost	relativnost	k1gFnSc1
délky	délka	k1gFnSc2
</s>
<s>
Současná	současný	k2eAgFnSc1d1
fyzika	fyzika	k1gFnSc1
nechápe	chápat	k5eNaImIp3nS
délku	délka	k1gFnSc4
jako	jako	k8xS,k8xC
veličinu	veličina	k1gFnSc4
absolutní	absolutní	k2eAgFnSc4d1
<g/>
,	,	kIx,
tedy	tedy	k9
v	v	k7c6
čistě	čistě	k6eAd1
geometrickém	geometrický	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jako	jako	k9
veličinu	veličina	k1gFnSc4
závislou	závislý	k2eAgFnSc4d1
na	na	k7c6
pohybovém	pohybový	k2eAgInSc6d1
stavu	stav	k1gInSc6
vzhledem	vzhledem	k7c3
k	k	k7c3
soustavě	soustava	k1gFnSc3
pozorovatele	pozorovatel	k1gMnSc2
a	a	k8xC
na	na	k7c6
gravitačním	gravitační	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
speciální	speciální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
relativity	relativita	k1gFnSc2
vysvětluje	vysvětlovat	k5eAaImIp3nS
tzv.	tzv.	kA
kontrakci	kontrakce	k1gFnSc4
délky	délka	k1gFnSc2
či	či	k8xC
jev	jev	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
aberace	aberace	k1gFnSc1
<g/>
,	,	kIx,
obecná	obecný	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
relativity	relativita	k1gFnSc2
pak	pak	k6eAd1
přidává	přidávat	k5eAaImIp3nS
závislost	závislost	k1gFnSc4
na	na	k7c4
zrychlení	zrychlení	k1gNnSc4
a	a	k8xC
gravitaci	gravitace	k1gFnSc4
a	a	k8xC
vysvětluje	vysvětlovat	k5eAaImIp3nS
např.	např.	kA
tzv.	tzv.	kA
gravitační	gravitační	k2eAgInSc1d1
rudý	rudý	k2eAgInSc1d1
posuv	posuv	k1gInSc1
elektromagnetického	elektromagnetický	k2eAgNnSc2d1
vlnění	vlnění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Délka	délka	k1gFnSc1
v	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
fyzice	fyzika	k1gFnSc6
</s>
<s>
V	v	k7c6
klasickém	klasický	k2eAgInSc6d1
případě	případ	k1gInSc6
lze	lze	k6eAd1
délku	délka	k1gFnSc4
vyjádřit	vyjádřit	k5eAaPmF
změnou	změna	k1gFnSc7
polohového	polohový	k2eAgInSc2d1
vektoru	vektor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kvantové	kvantový	k2eAgFnSc6d1
mechanice	mechanika	k1gFnSc6
je	být	k5eAaImIp3nS
obdobou	obdoba	k1gFnSc7
vektor	vektor	k1gInSc4
polohy	poloha	k1gFnPc4
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
značený	značený	k2eAgInSc1d1
x	x	k?
či	či	k8xC
X	X	kA
<g/>
)	)	kIx)
resp.	resp.	kA
jeho	jeho	k3xOp3gFnPc4
tři	tři	k4xCgFnPc4
složky	složka	k1gFnPc4
(	(	kIx(
<g/>
souřadnice	souřadnice	k1gFnPc4
<g/>
)	)	kIx)
v	v	k7c6
daném	daný	k2eAgInSc6d1
vztažném	vztažný	k2eAgInSc6d1
systému	systém	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souřadnice	souřadnice	k1gFnPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
kompatibilní	kompatibilní	k2eAgInPc4d1
pozorovatelné	pozorovatelný	k2eAgInPc4d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
lze	lze	k6eAd1
u	u	k7c2
kvantových	kvantový	k2eAgInPc2d1
systémů	systém	k1gInPc2
principiálně	principiálně	k6eAd1
změřit	změřit	k5eAaPmF
všechny	všechen	k3xTgFnPc1
tři	tři	k4xCgFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
nelze	lze	k6eNd1
určit	určit	k5eAaPmF
danou	daný	k2eAgFnSc4d1
složku	složka	k1gFnSc4
polohy	poloha	k1gFnSc2
částice	částice	k1gFnSc2
s	s	k7c7
odpovídající	odpovídající	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
její	její	k3xOp3gFnSc2
hybnosti	hybnost	k1gFnSc2
–	–	k?
tyto	tento	k3xDgFnPc1
dvě	dva	k4xCgFnPc1
veličiny	veličina	k1gFnPc1
jsou	být	k5eAaImIp3nP
nekompatibilní	kompatibilní	k2eNgFnPc1d1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
určitelnost	určitelnost	k1gFnSc1
podléhá	podléhat	k5eAaImIp3nS
Heisenbergovu	Heisenbergův	k2eAgInSc3d1
principu	princip	k1gInSc3
neurčitosti	neurčitost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přenesený	přenesený	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Slovo	slovo	k1gNnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
přeneseném	přenesený	k2eAgInSc6d1
významu	význam	k1gInSc6
pro	pro	k7c4
</s>
<s>
časové	časový	k2eAgInPc1d1
údaje	údaj	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
délka	délka	k1gFnSc1
dne	den	k1gInSc2
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc1d1
délka	délka	k1gFnSc1
života	život	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
pro	pro	k7c4
počty	počet	k1gInPc4
základních	základní	k2eAgFnPc2d1
informačních	informační	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
počet	počet	k1gInSc1
znaků	znak	k1gInPc2
v	v	k7c6
textu	text	k1gInSc6
<g/>
,	,	kIx,
např.	např.	kA
délka	délka	k1gFnSc1
slov	slovo	k1gNnPc2
v	v	k7c6
češtině	čeština	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
počet	počet	k1gInSc1
bajtů	bajt	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
délka	délka	k1gFnSc1
klíče	klíč	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
speciální	speciální	k2eAgInSc4d1
význam	význam	k1gInSc4
má	mít	k5eAaImIp3nS
zeměpisná	zeměpisný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
<g/>
,	,	kIx,
vyjadřující	vyjadřující	k2eAgInSc1d1
úhel	úhel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
svírá	svírat	k5eAaImIp3nS
spojnice	spojnice	k1gFnSc1
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
a	a	k8xC
daného	daný	k2eAgNnSc2d1
místa	místo	k1gNnSc2
s	s	k7c7
rovinou	rovina	k1gFnSc7
zemského	zemský	k2eAgInSc2d1
rovníku	rovník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Řádová	řádový	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
(	(	kIx(
<g/>
délka	délka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
</s>
<s>
Zeměpisná	zeměpisný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
délka	délka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
délka	délka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4224141-8	4224141-8	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
2919	#num#	k4
</s>
