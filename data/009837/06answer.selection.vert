<s>
Celsiův	Celsiův	k2eAgInSc1d1	Celsiův
stupeň	stupeň	k1gInSc1	stupeň
(	(	kIx(	(
<g/>
značený	značený	k2eAgMnSc1d1	značený
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1742	[number]	k4	1742
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
švédský	švédský	k2eAgMnSc1d1	švédský
astronom	astronom	k1gMnSc1	astronom
Anders	Andersa	k1gFnPc2	Andersa
Celsius	Celsius	k1gMnSc1	Celsius
<g/>
.	.	kIx.	.
</s>
