<p>
<s>
Otmar	Otmar	k1gMnSc1	Otmar
Oliva	Oliva	k1gMnSc1	Oliva
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
(	(	kIx(	(
<g/>
kamenosochař	kamenosochař	k1gMnSc1	kamenosochař
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
liturgických	liturgický	k2eAgInPc2d1	liturgický
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
úpravu	úprava	k1gFnSc4	úprava
chrámových	chrámový	k2eAgFnPc2d1	chrámová
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1972	[number]	k4	1972
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Střední	střední	k2eAgFnSc6d1	střední
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
také	také	k9	také
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
studijní	studijní	k2eAgInSc1d1	studijní
pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
akademickým	akademický	k2eAgMnSc7d1	akademický
malířem	malíř	k1gMnSc7	malíř
Vladislavem	Vladislav	k1gMnSc7	Vladislav
Vaculkou	Vaculka	k1gFnSc7	Vaculka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgMnS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
vězněn	věznit	k5eAaImNgMnS	věznit
za	za	k7c4	za
šíření	šíření	k1gNnSc4	šíření
materiálů	materiál	k1gInPc2	materiál
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakmile	jakmile	k8xS	jakmile
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
a	a	k8xC	a
usadil	usadit	k5eAaPmAgMnS	usadit
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
ateliér	ateliér	k1gInSc1	ateliér
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
také	také	k6eAd1	také
kovoliteckou	kovolitecký	k2eAgFnSc4d1	kovolitecká
dílnu	dílna	k1gFnSc4	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
samizdatového	samizdatový	k2eAgNnSc2d1	samizdatové
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Refugium	refugium	k1gNnSc1	refugium
Velehrad	Velehrad	k1gInSc1	Velehrad
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
se	s	k7c7	s
zvonařskou	zvonařský	k2eAgFnSc7d1	zvonařská
dílnou	dílna	k1gFnSc7	dílna
Ditrichových	Ditrichová	k1gFnPc2	Ditrichová
v	v	k7c6	v
Brodku	brodek	k1gInSc6	brodek
u	u	k7c2	u
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgInS	navštívit
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
začal	začít	k5eAaPmAgInS	začít
tvořit	tvořit	k5eAaImF	tvořit
pro	pro	k7c4	pro
Vatikán	Vatikán	k1gInSc4	Vatikán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
prací	práce	k1gFnSc7	práce
je	být	k5eAaImIp3nS	být
výzdoba	výzdoba	k1gFnSc1	výzdoba
kaple	kaple	k1gFnSc2	kaple
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Redemptoris	Redemptoris	k1gFnSc1	Redemptoris
Mater	mater	k1gFnSc1	mater
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
papežský	papežský	k2eAgInSc4d1	papežský
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
papežským	papežský	k2eAgInSc7d1	papežský
trůnem	trůn	k1gInSc7	trůn
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Berniniho	Bernini	k1gMnSc2	Bernini
<g/>
.	.	kIx.	.
</s>
<s>
Olivovi	Oliva	k1gMnSc3	Oliva
se	se	k3xPyFc4	se
také	také	k6eAd1	také
dostalo	dostat	k5eAaPmAgNnS	dostat
cti	čest	k1gFnSc3	čest
vytvořit	vytvořit	k5eAaPmF	vytvořit
medaili	medaile	k1gFnSc4	medaile
k	k	k7c3	k
významnému	významný	k2eAgInSc3d1	významný
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
pontifikátu	pontifikát	k1gInSc2	pontifikát
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oliva	Oliva	k1gMnSc1	Oliva
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
Čechem	Čech	k1gMnSc7	Čech
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tuto	tento	k3xDgFnSc4	tento
medaili	medaile	k1gFnSc4	medaile
kdy	kdy	k6eAd1	kdy
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výzdoba	výzdoba	k1gFnSc1	výzdoba
pro	pro	k7c4	pro
trnavskou	trnavský	k2eAgFnSc4d1	Trnavská
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
u	u	k7c2	u
Otmara	Otmar	k1gMnSc2	Otmar
Olivy	Oliva	k1gMnSc2	Oliva
objednal	objednat	k5eAaPmAgMnS	objednat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Róbert	Róbert	k1gMnSc1	Róbert
Bezák	Bezák	k1gMnSc1	Bezák
liturgickou	liturgický	k2eAgFnSc4d1	liturgická
výzdobu	výzdoba	k1gFnSc4	výzdoba
pro	pro	k7c4	pro
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
<g/>
.	.	kIx.	.
</s>
<s>
Oliva	oliva	k1gFnSc1	oliva
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zakázky	zakázka	k1gFnSc2	zakázka
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
korun	koruna	k1gFnPc2	koruna
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
spolupracovníky	spolupracovník	k1gMnPc7	spolupracovník
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
bronzový	bronzový	k2eAgInSc4d1	bronzový
oltář	oltář	k1gInSc4	oltář
<g/>
,	,	kIx,	,
křtitelnici	křtitelnice	k1gFnSc4	křtitelnice
<g/>
,	,	kIx,	,
monstrance	monstrance	k1gFnPc4	monstrance
<g/>
,	,	kIx,	,
svícny	svícen	k1gInPc4	svícen
či	či	k8xC	či
biskupskou	biskupský	k2eAgFnSc4d1	biskupská
berlu	berla	k1gFnSc4	berla
<g/>
.	.	kIx.	.
</s>
<s>
Bezák	Bezák	k1gMnSc1	Bezák
byl	být	k5eAaImAgMnS	být
však	však	k9	však
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
úřadu	úřad	k1gInSc2	úřad
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
odvolán	odvolat	k5eAaPmNgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oliva	Oliva	k1gMnSc1	Oliva
dílo	dílo	k1gNnSc4	dílo
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
Bezákův	Bezákův	k2eAgMnSc1d1	Bezákův
nástupce	nástupce	k1gMnSc1	nástupce
Ján	Ján	k1gMnSc1	Ján
Orosch	Orosch	k1gMnSc1	Orosch
však	však	k9	však
jeho	jeho	k3xOp3gFnSc4	jeho
smlouvu	smlouva	k1gFnSc4	smlouva
neuznává	uznávat	k5eNaImIp3nS	uznávat
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
po	po	k7c6	po
výtvarníkovi	výtvarník	k1gMnSc6	výtvarník
nově	nově	k6eAd1	nově
požaduje	požadovat	k5eAaImIp3nS	požadovat
zaplatit	zaplatit	k5eAaPmF	zaplatit
DPH	DPH	kA	DPH
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
700	[number]	k4	700
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
si	se	k3xPyFc3	se
nepřeje	přát	k5eNaImIp3nS	přát
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
moderní	moderní	k2eAgNnSc1d1	moderní
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Instalaci	instalace	k1gFnSc4	instalace
děl	dít	k5eAaBmAgMnS	dít
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
podle	podle	k7c2	podle
Orosche	Orosch	k1gFnSc2	Orosch
brání	bránit	k5eAaImIp3nP	bránit
technické	technický	k2eAgInPc4d1	technický
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
by	by	kYmCp3nS	by
proto	proto	k8xC	proto
přednost	přednost	k1gFnSc1	přednost
jejich	jejich	k3xOp3gNnSc2	jejich
umístění	umístění	k1gNnSc2	umístění
v	v	k7c6	v
méně	málo	k6eAd2	málo
významném	významný	k2eAgInSc6d1	významný
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
dělal	dělat	k5eAaImAgMnS	dělat
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
...	...	k?	...
Když	když	k8xS	když
sochař	sochař	k1gMnSc1	sochař
dělá	dělat	k5eAaImIp3nS	dělat
nějakou	nějaký	k3yIgFnSc4	nějaký
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
to	ten	k3xDgNnSc1	ten
dělá	dělat	k5eAaImIp3nS	dělat
přesně	přesně	k6eAd1	přesně
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
Oliva	Oliva	k1gMnSc1	Oliva
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2013	[number]	k4	2013
Oliva	Oliva	k1gMnSc1	Oliva
nechal	nechat	k5eAaPmAgMnS	nechat
dopravit	dopravit	k5eAaPmF	dopravit
dílo	dílo	k1gNnSc4	dílo
do	do	k7c2	do
Trnavy	Trnava	k1gFnSc2	Trnava
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
v	v	k7c6	v
katedrálo	katedrála	k1gFnSc5	katedrála
skončilo	skončit	k5eAaPmAgNnS	skončit
ve	v	k7c6	v
skladu	sklad	k1gInSc6	sklad
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
Olivovy	Olivův	k2eAgFnPc4d1	Olivova
práce	práce	k1gFnPc4	práce
patří	patřit	k5eAaImIp3nP	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
interiéry	interiér	k1gInPc1	interiér
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kaple	kaple	k1gFnSc1	kaple
Redemptoris	Redemptoris	k1gFnSc1	Redemptoris
Mater	mater	k1gFnSc1	mater
</s>
</p>
<p>
<s>
výzdoba	výzdoba	k1gFnSc1	výzdoba
liturgického	liturgický	k2eAgInSc2d1	liturgický
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
na	na	k7c6	na
Velehradě	Velehrad	k1gInSc6	Velehrad
<g/>
,	,	kIx,	,
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
v	v	k7c6	v
Kyselovicích	Kyselovice	k1gFnPc6	Kyselovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
Salaši	salaš	k1gInSc6	salaš
u	u	k7c2	u
Velehradu	Velehrad	k1gInSc2	Velehrad
<g/>
,	,	kIx,	,
v	v	k7c6	v
Olomouci-Chválkovicích	Olomouci-Chválkovice	k1gFnPc6	Olomouci-Chválkovice
<g/>
,	,	kIx,	,
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Hoře	hora	k1gFnSc6	hora
u	u	k7c2	u
Příbrami	Příbram	k1gFnSc2	Příbram
<g/>
,	,	kIx,	,
kostele	kostel	k1gInSc6	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Vítězné	vítězný	k2eAgFnSc2d1	vítězná
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
</s>
</p>
<p>
<s>
řešení	řešení	k1gNnSc1	řešení
interiérů	interiér	k1gInPc2	interiér
chrámu	chrámat	k5eAaImIp1nS	chrámat
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Matky	matka	k1gFnSc2	matka
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Mariboru	Maribor	k1gInSc6	Maribor
ve	v	k7c6	v
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
</s>
</p>
<p>
<s>
relikviář	relikviář	k1gInSc1	relikviář
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Sarkandera	Sarkander	k1gMnSc2	Sarkander
pro	pro	k7c4	pro
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
návrh	návrh	k1gInSc1	návrh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
interiér	interiér	k1gInSc1	interiér
adorační	adorační	k2eAgFnSc2d1	adorační
kaple	kaple	k1gFnSc2	kaple
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
katedrále	katedrála	k1gFnSc6	katedrála
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
bronzový	bronzový	k2eAgInSc4d1	bronzový
oltář	oltář	k1gInSc4	oltář
s	s	k7c7	s
mramorovou	mramorový	k2eAgFnSc7d1	mramorová
deskou	deska	k1gFnSc7	deska
pro	pro	k7c4	pro
kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
relikviáře	relikviář	k1gInPc1	relikviář
<g/>
,	,	kIx,	,
medaile	medaile	k1gFnPc1	medaile
<g/>
,	,	kIx,	,
biskupské	biskupský	k2eAgFnPc1d1	biskupská
hole	hole	k1gFnPc1	hole
(	(	kIx(	(
<g/>
např.	např.	kA	např.
biskupskou	biskupský	k2eAgFnSc4d1	biskupská
hůl	hůl	k1gFnSc4	hůl
zhotovenou	zhotovený	k2eAgFnSc4d1	zhotovená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
pro	pro	k7c4	pro
Josefa	Josef	k1gMnSc4	Josef
Hrdličku	Hrdlička	k1gMnSc4	Hrdlička
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
Jan	Jan	k1gMnSc1	Jan
Graubner	Graubner	k1gMnSc1	Graubner
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
liturgická	liturgický	k2eAgFnSc1d1	liturgická
výzdoba	výzdoba	k1gFnSc1	výzdoba
pro	pro	k7c4	pro
katedrálu	katedrála	k1gFnSc4	katedrála
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
–	–	k?	–
neinstalovánanáhrobky	neinstalovánanáhrobek	k1gInPc4	neinstalovánanáhrobek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
náhrobek	náhrobek	k1gInSc1	náhrobek
Msgr	Msgr	k1gInSc1	Msgr
<g/>
.	.	kIx.	.
</s>
<s>
Jana	Jan	k1gMnSc2	Jan
Šrámka	Šrámek	k1gMnSc2	Šrámek
na	na	k7c4	na
Velehraděpomníky	Velehraděpomník	k1gMnPc4	Velehraděpomník
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
-	-	kIx~	-
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
Jan	Jan	k1gMnSc1	Jan
Zahradníček	Zahradníček	k1gMnSc1	Zahradníček
a	a	k8xC	a
Akord	akord	k1gInSc1	akord
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
pomník	pomník	k1gInSc1	pomník
česko-moravsko-slovenské	českooravskolovenský	k2eAgFnSc2d1	česko-moravsko-slovenský
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Javořině	Javořina	k1gFnSc6	Javořina
</s>
</p>
<p>
<s>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
bustou	busta	k1gFnSc7	busta
Františka	František	k1gMnSc2	František
kardinála	kardinál	k1gMnSc2	kardinál
Tomáška	Tomášek	k1gMnSc2	Tomášek
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Huzové	Huzová	k1gFnSc6	Huzová
</s>
</p>
<p>
<s>
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
na	na	k7c6	na
olomouckém	olomoucký	k2eAgNnSc6d1	olomoucké
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
Antonínu	Antonín	k1gMnSc3	Antonín
Cyrilu	Cyril	k1gMnSc3	Cyril
Stojanovi	Stojan	k1gMnSc3	Stojan
<g/>
,	,	kIx,	,
Josefu	Josef	k1gMnSc3	Josef
Karlu	Karel	k1gMnSc3	Karel
Matochovi	Matoch	k1gMnSc3	Matoch
a	a	k8xC	a
návštěvě	návštěva	k1gFnSc3	návštěva
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
fontány	fontána	k1gFnPc1	fontána
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
–	–	k?	–
fontána	fontána	k1gFnSc1	fontána
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
kapucínského	kapucínský	k2eAgInSc2d1	kapucínský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
–	–	k?	–
fontána	fontána	k1gFnSc1	fontána
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
Reduty	reduta	k1gFnSc2	reduta
v	v	k7c6	v
Uherském	uherský	k2eAgNnSc6d1	Uherské
Hradišti	Hradiště	k1gNnSc6	Hradiště
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Pramen	pramen	k1gInSc1	pramen
živé	živý	k2eAgFnSc2d1	živá
vody	voda	k1gFnSc2	voda
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Sarkandra	Sarkandr	k1gMnSc2	Sarkandr
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Malý	malý	k2eAgInSc1d1	malý
slovník	slovník	k1gInSc1	slovník
osobností	osobnost	k1gFnPc2	osobnost
českého	český	k2eAgInSc2d1	český
katolicismu	katolicismus	k1gInSc2	katolicismus
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
antologií	antologie	k1gFnSc7	antologie
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
308	[number]	k4	308
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Otmar	Otmar	k1gMnSc1	Otmar
Oliva	Oliva	k1gMnSc1	Oliva
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Otmar	Otmar	k1gMnSc1	Otmar
Oliva	oliva	k1gFnSc1	oliva
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Otmar	Otmar	k1gMnSc1	Otmar
Oliva	Oliva	k1gMnSc1	Oliva
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
</s>
</p>
<p>
<s>
Otmar	Otmar	k1gMnSc1	Otmar
Oliva	Oliva	k1gMnSc1	Oliva
–	–	k?	–
portrét	portrét	k1gInSc1	portrét
ČT	ČT	kA	ČT
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
on-line	onin	k1gInSc5	on-lin
přehrání	přehrání	k1gNnSc3	přehrání
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Oliva	Oliva	k1gMnSc1	Oliva
Otmar	Otmar	k1gMnSc1	Otmar
</s>
</p>
