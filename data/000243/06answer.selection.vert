<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc1	sklep
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
pražských	pražský	k2eAgNnPc2d1	Pražské
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
Davidem	David	k1gMnSc7	David
Vávrou	Vávra	k1gMnSc7	Vávra
a	a	k8xC	a
Milanem	Milan	k1gMnSc7	Milan
Šteindlerem	Šteindler	k1gMnSc7	Šteindler
<g/>
.	.	kIx.	.
</s>
