<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc1	sklep
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
pražských	pražský	k2eAgNnPc2d1	Pražské
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
Davidem	David	k1gMnSc7	David
Vávrou	Vávra	k1gMnSc7	Vávra
a	a	k8xC	a
Milanem	Milan	k1gMnSc7	Milan
Šteindlerem	Šteindler	k1gMnSc7	Šteindler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
Dobeška	Dobešek	k1gInSc2	Dobešek
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
<g/>
-Braníku	-Braník	k1gInSc2	-Braník
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
osobitým	osobitý	k2eAgNnSc7d1	osobité
zpracováním	zpracování	k1gNnSc7	zpracování
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
divadla	divadlo	k1gNnSc2	divadlo
Sklep	sklep	k1gInSc1	sklep
pochází	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
skutečného	skutečný	k2eAgInSc2d1	skutečný
sklepního	sklepní	k2eAgInSc2d1	sklepní
prostoru	prostor	k1gInSc2	prostor
domku	domek	k1gInSc2	domek
babičky	babička	k1gFnSc2	babička
Davida	David	k1gMnSc2	David
Vávry	Vávra	k1gMnSc2	Vávra
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
soubor	soubor	k1gInSc4	soubor
založil	založit	k5eAaPmAgMnS	založit
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
původním	původní	k2eAgNnSc7d1	původní
jménem	jméno	k1gNnSc7	jméno
Kobil	Kobil	k1gInSc1	Kobil
klub	klub	k1gInSc1	klub
<g/>
)	)	kIx)	)
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
čtrnáctiletým	čtrnáctiletý	k2eAgMnSc7d1	čtrnáctiletý
spolužákem	spolužák	k1gMnSc7	spolužák
Milanem	Milan	k1gMnSc7	Milan
Šteindlerem	Šteindler	k1gMnSc7	Šteindler
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
rané	raný	k2eAgFnPc4d1	raná
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
krátké	krátký	k2eAgFnPc4d1	krátká
scénky	scénka	k1gFnPc4	scénka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
představeních	představení	k1gNnPc6	představení
pořadu	pořad	k1gInSc2	pořad
Besídka	besídka	k1gFnSc1	besídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
tyto	tento	k3xDgInPc4	tento
texty	text	k1gInPc4	text
představovali	představovat	k5eAaImAgMnP	představovat
pouze	pouze	k6eAd1	pouze
svým	svůj	k3xOyFgMnPc3	svůj
kamarádům	kamarád	k1gMnPc3	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
soukromá	soukromý	k2eAgFnSc1d1	soukromá
aktivita	aktivita	k1gFnSc1	aktivita
zakládající	zakládající	k2eAgFnSc2d1	zakládající
dvojice	dvojice	k1gFnSc2	dvojice
a	a	k8xC	a
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
přátel	přítel	k1gMnPc2	přítel
začínala	začínat	k5eAaImAgFnS	začínat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
přerůstat	přerůstat	k5eAaImF	přerůstat
ve	v	k7c4	v
veřejná	veřejný	k2eAgNnPc4d1	veřejné
vystoupení	vystoupení	k1gNnPc4	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
Branickém	branický	k2eAgNnSc6d1	Branické
loutkovém	loutkový	k2eAgNnSc6d1	loutkové
divadle	divadlo	k1gNnSc6	divadlo
Zvoneček	zvoneček	k1gInSc4	zvoneček
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
otec	otec	k1gMnSc1	otec
člena	člen	k1gMnSc2	člen
souboru	soubor	k1gInSc2	soubor
Tomáše	Tomáš	k1gMnSc4	Tomáš
Vorla	Vorel	k1gMnSc4	Vorel
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
ZV	ZV	kA	ZV
ROH	roh	k1gInSc1	roh
Tesla	Tesla	k1gFnSc1	Tesla
Karlín	Karlín	k1gInSc4	Karlín
<g/>
,	,	kIx,	,
restauraci	restaurace	k1gFnSc4	restaurace
U	u	k7c2	u
Jaurisů	Jauris	k1gInPc2	Jauris
v	v	k7c6	v
Michli	Michli	k1gFnSc6	Michli
<g/>
,	,	kIx,	,
strahovském	strahovský	k2eAgInSc6d1	strahovský
studentském	studentský	k2eAgInSc6d1	studentský
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
mnohých	mnohý	k2eAgNnPc6d1	mnohé
dalších	další	k1gNnPc6	další
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
utvářet	utvářet	k5eAaImF	utvářet
jádro	jádro	k1gNnSc1	jádro
souboru	soubor	k1gInSc2	soubor
-	-	kIx~	-
krom	krom	k7c2	krom
Vávry	Vávra	k1gMnSc2	Vávra
a	a	k8xC	a
Šteindlera	Šteindler	k1gMnSc2	Šteindler
ještě	ještě	k6eAd1	ještě
přibyl	přibýt	k5eAaPmAgMnS	přibýt
Jiří	Jiří	k1gMnSc1	Jiří
Burda	Burda	k1gMnSc1	Burda
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vorel	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Váša	Váša	k1gMnSc1	Váša
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
Holubová	Holubová	k1gFnSc1	Holubová
a	a	k8xC	a
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
<g/>
.	.	kIx.	.
</s>
<s>
Holubová	holubový	k2eAgFnSc1d1	Holubová
se	se	k3xPyFc4	se
k	k	k7c3	k
souboru	soubor	k1gInSc2	soubor
přidala	přidat	k5eAaPmAgFnS	přidat
po	po	k7c6	po
přijímacích	přijímací	k2eAgFnPc6d1	přijímací
zkouškách	zkouška	k1gFnPc6	zkouška
na	na	k7c6	na
DAMU	DAMU	kA	DAMU
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jí	jíst	k5eAaImIp3nS	jíst
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
roli	role	k1gFnSc4	role
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Podivuhodný	podivuhodný	k2eAgInSc4d1	podivuhodný
cestopis	cestopis	k1gInSc4	cestopis
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Vávry	Vávra	k1gMnSc2	Vávra
a	a	k8xC	a
Šteindlera	Šteindler	k1gMnSc2	Šteindler
předvedl	předvést	k5eAaPmAgMnS	předvést
poprvé	poprvé	k6eAd1	poprvé
přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
profesory	profesor	k1gMnPc7	profesor
DAMU	DAMU	kA	DAMU
<g/>
.	.	kIx.	.
</s>
<s>
Upoutali	upoutat	k5eAaPmAgMnP	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
ohromným	ohromný	k2eAgNnSc7d1	ohromné
množstvím	množství	k1gNnSc7	množství
rekvizit	rekvizita	k1gFnPc2	rekvizita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
nesli	nést	k5eAaImAgMnP	nést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
"	"	kIx"	"
<g/>
krupobití	krupobití	k1gNnSc6	krupobití
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgMnSc6	jenž
házeli	házet	k5eAaImAgMnP	házet
kroupy	kroupa	k1gFnPc4	kroupa
i	i	k9	i
po	po	k7c6	po
porotě	porota	k1gFnSc6	porota
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
přijímacích	přijímací	k2eAgFnPc2d1	přijímací
zkoušek	zkouška	k1gFnPc2	zkouška
vyhozeni	vyhodit	k5eAaPmNgMnP	vyhodit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
soubor	soubor	k1gInSc4	soubor
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
domě	dům	k1gInSc6	dům
Dobeška	Dobešek	k1gInSc2	Dobešek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gFnSc7	jeho
domovskou	domovský	k2eAgFnSc7d1	domovská
scénou	scéna	k1gFnSc7	scéna
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
soubor	soubor	k1gInSc1	soubor
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
významnějších	významný	k2eAgNnPc6d2	významnější
pražských	pražský	k2eAgNnPc6d1	Pražské
místech	místo	k1gNnPc6	místo
jako	jako	k8xC	jako
Malostranská	malostranský	k2eAgFnSc1d1	Malostranská
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
Futurum	futurum	k1gNnSc1	futurum
nebo	nebo	k8xC	nebo
Branické	branický	k2eAgNnSc1d1	Branické
divadlo	divadlo	k1gNnSc1	divadlo
pantomimy	pantomima	k1gFnSc2	pantomima
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
-	-	kIx~	-
například	například	k6eAd1	například
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
a	a	k8xC	a
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
započala	započnout	k5eAaPmAgFnS	započnout
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
spolupráce	spolupráce	k1gFnSc1	spolupráce
Vávry	Vávra	k1gMnSc2	Vávra
a	a	k8xC	a
Vorla	Vorel	k1gMnSc2	Vorel
se	s	k7c7	s
spolkem	spolek	k1gInSc7	spolek
Mimóza	mimóza	k1gFnSc1	mimóza
a	a	k8xC	a
recitačním	recitační	k2eAgInSc7d1	recitační
spolkem	spolek	k1gInSc7	spolek
Vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začíná	začínat	k5eAaImIp3nS	začínat
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
pražskými	pražský	k2eAgNnPc7d1	Pražské
divadly	divadlo	k1gNnPc7	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc1	sklep
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
tahounem	tahoun	k1gMnSc7	tahoun
skupiny	skupina	k1gFnSc2	skupina
Pražská	pražský	k2eAgFnSc1d1	Pražská
pětka	pětka	k1gFnSc1	pětka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
tvořené	tvořený	k2eAgNnSc1d1	tvořené
divadly	divadlo	k1gNnPc7	divadlo
Mimóza	mimóza	k1gFnSc1	mimóza
<g/>
,	,	kIx,	,
Vpřed	vpřed	k6eAd1	vpřed
<g/>
,	,	kIx,	,
Kolotoč	kolotoč	k1gInSc1	kolotoč
a	a	k8xC	a
Křeč	křeč	k1gFnSc1	křeč
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
představovala	představovat	k5eAaImAgFnS	představovat
nejvýraznější	výrazný	k2eAgNnSc4d3	nejvýraznější
generační	generační	k2eAgNnSc4d1	generační
hnutí	hnutí	k1gNnSc4	hnutí
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
přinesla	přinést	k5eAaPmAgFnS	přinést
oživení	oživení	k1gNnSc4	oživení
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
divadelní	divadelní	k2eAgFnSc2d1	divadelní
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
spolky	spolek	k1gInPc4	spolek
propojoval	propojovat	k5eAaImAgInS	propojovat
osobní	osobní	k2eAgInSc1d1	osobní
<g/>
,	,	kIx,	,
životní	životní	k2eAgInSc1d1	životní
<g/>
,	,	kIx,	,
filosofický	filosofický	k2eAgInSc1d1	filosofický
a	a	k8xC	a
estetický	estetický	k2eAgInSc1d1	estetický
názor	názor	k1gInSc1	názor
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
společných	společný	k2eAgInPc2d1	společný
projektů	projekt	k1gInPc2	projekt
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
divadelní	divadelní	k2eAgInSc4d1	divadelní
karneval	karneval	k1gInSc4	karneval
Maskáč	maskáč	k1gInSc4	maskáč
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
Tichý	Tichý	k1gMnSc1	Tichý
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInSc1d1	divadelní
bál	bál	k1gInSc1	bál
ve	v	k7c6	v
vysočanském	vysočanský	k2eAgInSc6d1	vysočanský
Lidovém	lidový	k2eAgInSc6d1	lidový
domě	dům	k1gInSc6	dům
anebo	anebo	k8xC	anebo
Mimotočskřed	Mimotočskřed	k1gMnSc1	Mimotočskřed
chmelový	chmelový	k2eAgMnSc1d1	chmelový
aneb	aneb	k?	aneb
bludiště	bludiště	k1gNnSc1	bludiště
v	v	k7c6	v
junior	junior	k1gMnSc1	junior
klubu	klub	k1gInSc2	klub
Na	na	k7c6	na
Chmelnici	chmelnice	k1gFnSc6	chmelnice
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
Pražské	pražský	k2eAgFnSc2d1	Pražská
pětky	pětka	k1gFnSc2	pětka
pronikla	proniknout	k5eAaPmAgFnS	proniknout
i	i	k9	i
do	do	k7c2	do
televizní	televizní	k2eAgFnSc2d1	televizní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
režíroval	režírovat	k5eAaImAgMnS	režírovat
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vorel	Vorel	k1gMnSc1	Vorel
film	film	k1gInSc4	film
Pražská	pražský	k2eAgFnSc1d1	Pražská
pětka	pětka	k1gFnSc1	pětka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
divadlo	divadlo	k1gNnSc1	divadlo
Sklep	sklep	k1gInSc1	sklep
připravilo	připravit	k5eAaPmAgNnS	připravit
pátou	pátý	k4xOgFnSc4	pátý
část	část	k1gFnSc4	část
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Na	na	k7c6	na
Brigádě	brigáda	k1gFnSc6	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
televizními	televizní	k2eAgFnPc7d1	televizní
scénkami	scénka	k1gFnPc7	scénka
a	a	k8xC	a
písničkovými	písničkový	k2eAgInPc7d1	písničkový
klipy	klip	k1gInPc7	klip
přinesl	přinést	k5eAaPmAgInS	přinést
souboru	soubor	k1gInSc2	soubor
další	další	k2eAgMnPc4d1	další
příznivce	příznivec	k1gMnPc4	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Sklep	sklep	k1gInSc1	sklep
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
divadel	divadlo	k1gNnPc2	divadlo
Pražské	pražský	k2eAgFnSc2d1	Pražská
pětky	pětka	k1gFnSc2	pětka
nejen	nejen	k6eAd1	nejen
nejstarší	starý	k2eAgFnSc1d3	nejstarší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nejpočetnější	početní	k2eAgFnSc2d3	nejpočetnější
i	i	k8xC	i
nejvytrvalejší	vytrvalý	k2eAgFnSc2d3	nejvytrvalejší
<g/>
:	:	kIx,	:
bez	bez	k7c2	bez
větších	veliký	k2eAgFnPc2d2	veliký
přestávek	přestávka	k1gFnPc2	přestávka
hraje	hrát	k5eAaImIp3nS	hrát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
sklepovská	sklepovský	k2eAgFnSc1d1	sklepovský
Besídka	besídka	k1gFnSc1	besídka
<g/>
,	,	kIx,	,
každoročně	každoročně	k6eAd1	každoročně
uváděné	uváděný	k2eAgNnSc4d1	uváděné
pásmo	pásmo	k1gNnSc4	pásmo
scének	scénka	k1gFnPc2	scénka
<g/>
,	,	kIx,	,
skečů	skeč	k1gInPc2	skeč
<g/>
,	,	kIx,	,
písní	píseň	k1gFnPc2	píseň
a	a	k8xC	a
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
čerpající	čerpající	k2eAgNnSc4d1	čerpající
z	z	k7c2	z
nasazení	nasazení	k1gNnSc2	nasazení
interpretů	interpret	k1gMnPc2	interpret
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
svérázného	svérázný	k2eAgInSc2d1	svérázný
smyslu	smysl	k1gInSc2	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
<g/>
.	.	kIx.	.
</s>
<s>
Jistý	jistý	k2eAgInSc1d1	jistý
posun	posun	k1gInSc1	posun
během	během	k7c2	během
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
po	po	k7c4	po
která	který	k3yQgNnPc4	který
se	se	k3xPyFc4	se
Besídka	besídka	k1gFnSc1	besídka
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
jazykové	jazykový	k2eAgFnSc6d1	jazyková
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
,	,	kIx,	,
tvůrci	tvůrce	k1gMnPc1	tvůrce
představení	představení	k1gNnSc2	představení
si	se	k3xPyFc3	se
už	už	k6eAd1	už
nevystačí	vystačit	k5eNaBmIp3nP	vystačit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
češtinou	čeština	k1gFnSc7	čeština
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
záměry	záměr	k1gInPc1	záměr
využívají	využívat	k5eAaImIp3nP	využívat
celou	celý	k2eAgFnSc4d1	celá
škálu	škála	k1gFnSc4	škála
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
zkomolenin	zkomolenina	k1gFnPc2	zkomolenina
a	a	k8xC	a
neologismů	neologismus	k1gInPc2	neologismus
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
dění	dění	k1gNnSc2	dění
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
členů	člen	k1gMnPc2	člen
souboru	soubor	k1gInSc2	soubor
aktivně	aktivně	k6eAd1	aktivně
účastnila	účastnit	k5eAaImAgFnS	účastnit
<g/>
,	,	kIx,	,
připravil	připravit	k5eAaPmAgInS	připravit
Sklep	sklep	k1gInSc1	sklep
dvě	dva	k4xCgFnPc4	dva
úspěšné	úspěšný	k2eAgFnPc4d1	úspěšná
<g/>
,	,	kIx,	,
mnohokrát	mnohokrát	k6eAd1	mnohokrát
reprízované	reprízovaný	k2eAgFnSc2d1	reprízovaná
inscenace	inscenace	k1gFnSc2	inscenace
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
objížděl	objíždět	k5eAaImAgMnS	objíždět
celou	celý	k2eAgFnSc4d1	celá
republiku	republika	k1gFnSc4	republika
<g/>
:	:	kIx,	:
Mlýny	mlýn	k1gInPc1	mlýn
a	a	k8xC	a
Tajů	taj	k1gInPc2	taj
plný	plný	k2eAgInSc4d1	plný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
Mlýny	mlýn	k1gInPc1	mlýn
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
inscenace	inscenace	k1gFnSc2	inscenace
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Bryndy	brynda	k1gFnSc2	brynda
<g/>
,	,	kIx,	,
pojetí	pojetí	k1gNnSc1	pojetí
je	být	k5eAaImIp3nS	být
však	však	k9	však
s	s	k7c7	s
dávkou	dávka	k1gFnSc7	dávka
humoru	humor	k1gInSc2	humor
a	a	k8xC	a
nadsázky	nadsázka	k1gFnSc2	nadsázka
<g/>
.	.	kIx.	.
</s>
<s>
Tajů	taj	k1gInPc2	taj
plný	plný	k2eAgInSc4d1	plný
ostrov	ostrov	k1gInSc4	ostrov
zas	zas	k6eAd1	zas
oživuje	oživovat	k5eAaImIp3nS	oživovat
legendy	legenda	k1gFnPc1	legenda
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
nebo	nebo	k8xC	nebo
uvnitř	uvnitř	k7c2	uvnitř
souboru	soubor	k1gInSc2	soubor
vznikalo	vznikat	k5eAaImAgNnS	vznikat
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
vývoje	vývoj	k1gInSc2	vývoj
spoustu	spoustu	k6eAd1	spoustu
drobnějších	drobný	k2eAgInPc2d2	drobnější
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
umělecká	umělecký	k2eAgFnSc1d1	umělecká
skupina	skupina	k1gFnSc1	skupina
Tvrdohlaví	tvrdohlavý	k2eAgMnPc1d1	tvrdohlavý
<g/>
,	,	kIx,	,
B.	B.	kA	B.
S.	S.	kA	S.
P.	P.	kA	P.
bavíme	bavit	k5eAaImIp1nP	bavit
se	se	k3xPyFc4	se
po	po	k7c6	po
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Raut	raut	k1gInSc1	raut
<g/>
,	,	kIx,	,
vysílání	vysílání	k1gNnSc1	vysílání
radia	radio	k1gNnSc2	radio
Limonádový	limonádový	k2eAgMnSc1d1	limonádový
Joe	Joe	k1gMnSc1	Joe
anebo	anebo	k8xC	anebo
exhibicionistické	exhibicionistický	k2eAgNnSc1d1	exhibicionistické
duo	duo	k1gNnSc1	duo
Thomas	Thomas	k1gMnSc1	Thomas
+	+	kIx~	+
Ruhller	Ruhller	k1gMnSc1	Ruhller
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
osobnostem	osobnost	k1gFnPc3	osobnost
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
Aleši	Aleš	k1gMnSc3	Aleš
Najbrtovi	Najbrt	k1gMnSc3	Najbrt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
divadlu	divadlo	k1gNnSc3	divadlo
dostalo	dostat	k5eAaPmAgNnS	dostat
patřičného	patřičný	k2eAgInSc2d1	patřičný
vizuálního	vizuální	k2eAgInSc2d1	vizuální
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Vávra	Vávra	k1gMnSc1	Vávra
-	-	kIx~	-
architekt	architekt	k1gMnSc1	architekt
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hanák	Hanák	k1gMnSc1	Hanák
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
moderátor	moderátor	k1gMnSc1	moderátor
Milan	Milan	k1gMnSc1	Milan
Šteindler	Šteindler	k1gMnSc1	Šteindler
-	-	kIx~	-
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
Jiří	Jiří	k1gMnSc1	Jiří
Fero	Fero	k1gMnSc1	Fero
Burda	Burda	k1gMnSc1	Burda
Robert	Robert	k1gMnSc1	Robert
Nebřenský	Nebřenský	k2eAgMnSc1d1	Nebřenský
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
hudebník	hudebník	k1gMnSc1	hudebník
Jana	Jana	k1gFnSc1	Jana
Hanáková	Hanáková	k1gFnSc1	Hanáková
Tereza	Tereza	k1gFnSc1	Tereza
Kučerová	Kučerová	k1gFnSc1	Kučerová
Marta	Marta	k1gFnSc1	Marta
Marinová	Marinová	k1gFnSc1	Marinová
Hana	Hana	k1gFnSc1	Hana
Navarová	Navarová	k1gFnSc1	Navarová
Lenka	Lenka	k1gFnSc1	Lenka
Andelová	Andelová	k1gFnSc1	Andelová
Hana	Hana	k1gFnSc1	Hana
Pafková	Pafková	k1gFnSc1	Pafková
Bára	Bára	k1gFnSc1	Bára
Trojanová	Trojanová	k1gFnSc1	Trojanová
Lenka	Lenka	k1gFnSc1	Lenka
Vychodilová	Vychodilová	k1gFnSc1	Vychodilová
Roman	Roman	k1gMnSc1	Roman
Fojtíček	Fojtíček	k1gMnSc1	Fojtíček
David	David	k1gMnSc1	David
Noll	Noll	k1gMnSc1	Noll
Jiří	Jiří	k1gMnSc1	Jiří
Podzimek	Podzimek	k1gMnSc1	Podzimek
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vytiska	Vytisko	k1gNnSc2	Vytisko
Václav	Václav	k1gMnSc1	Václav
<g />
.	.	kIx.	.
</s>
<s>
Marhoul	Marhoul	k1gInSc1	Marhoul
-	-	kIx~	-
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
manažer	manažer	k1gMnSc1	manažer
Jiří	Jiří	k1gMnSc1	Jiří
Macháček	Macháček	k1gMnSc1	Macháček
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Monika	Monika	k1gFnSc1	Monika
Načeva	Načeva	k1gFnSc1	Načeva
-	-	kIx~	-
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
šansoniérka	šansoniérka	k1gFnSc1	šansoniérka
Aleš	Aleš	k1gMnSc1	Aleš
Najbrt	Najbrta	k1gFnPc2	Najbrta
František	František	k1gMnSc1	František
Skála	Skála	k1gMnSc1	Skála
Líba	Líba	k1gFnSc1	Líba
Drdáková	Drdáková	k1gFnSc1	Drdáková
Jan	Jan	k1gMnSc1	Jan
Slovák	Slovák	k1gMnSc1	Slovák
Jan	Jan	k1gMnSc1	Jan
Winter	Winter	k1gMnSc1	Winter
Jiří	Jiří	k1gMnSc1	Jiří
Vacek	Vacek	k1gMnSc1	Vacek
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Marek	Marek	k1gMnSc1	Marek
Petr	Petr	k1gMnSc1	Petr
Koutecký	Koutecký	k2eAgMnSc1d1	Koutecký
Ondřej	Ondřej	k1gMnSc1	Ondřej
Trojan	Trojan	k1gMnSc1	Trojan
-	-	kIx~	-
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
František	František	k1gMnSc1	František
Váša	Váša	k1gMnSc1	Váša
-	-	kIx~	-
animátor	animátor	k1gMnSc1	animátor
<g/>
,	,	kIx,	,
loutkář	loutkář	k1gMnSc1	loutkář
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Róna	Róna	k1gMnSc1	Róna
-	-	kIx~	-
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
sochař	sochař	k1gMnSc1	sochař
Eva	Eva	k1gFnSc1	Eva
Holubová	Holubová	k1gFnSc1	Holubová
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Sklep	sklep	k1gInSc1	sklep
<g/>
,	,	kIx,	,
Pražská	pražský	k2eAgFnSc1d1	Pražská
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
