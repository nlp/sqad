<s>
Extensible	Extensible	k6eAd1	Extensible
Messaging	Messaging	k1gInSc1	Messaging
and	and	k?	and
Presence	presence	k1gFnSc2	presence
Protocol	Protocola	k1gFnPc2	Protocola
(	(	kIx(	(
<g/>
XMPP	XMPP	kA	XMPP
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Jabber	Jabber	k1gMnSc1	Jabber
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
rozšiřitelný	rozšiřitelný	k2eAgInSc4d1	rozšiřitelný
protokol	protokol	k1gInSc4	protokol
pro	pro	k7c4	pro
posílání	posílání	k1gNnSc4	posílání
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
zjištění	zjištění	k1gNnSc1	zjištění
stavu	stav	k1gInSc2	stav
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
protokol	protokol	k1gInSc1	protokol
pro	pro	k7c4	pro
instant	instant	k?	instant
messagingovou	messagingový	k2eAgFnSc4d1	messagingový
síť	síť	k1gFnSc4	síť
Jabber	Jabbra	k1gFnPc2	Jabbra
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
IM	IM	kA	IM
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
použit	použít	k5eAaPmNgMnS	použít
i	i	k8xC	i
pro	pro	k7c4	pro
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
komunikaci	komunikace	k1gFnSc4	komunikace
programů	program	k1gInPc2	program
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
různých	různý	k2eAgFnPc2d1	různá
automatických	automatický	k2eAgFnPc2d1	automatická
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
botů	bot	k1gInPc2	bot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
adoptován	adoptovat	k5eAaPmNgInS	adoptovat
jakožto	jakožto	k8xS	jakožto
standard	standard	k1gInSc1	standard
Internetu	Internet	k1gInSc2	Internet
do	do	k7c2	do
RFC	RFC	kA	RFC
dokumentů	dokument	k1gInPc2	dokument
–	–	k?	–
základní	základní	k2eAgFnPc1d1	základní
normy	norma	k1gFnPc1	norma
jsou	být	k5eAaImIp3nP	být
RFC	RFC	kA	RFC
3920	[number]	k4	3920
(	(	kIx(	(
<g/>
obecná	obecná	k1gFnSc1	obecná
specifikace	specifikace	k1gFnSc2	specifikace
protokolu	protokol	k1gInSc2	protokol
<g/>
)	)	kIx)	)
a	a	k8xC	a
RFC	RFC	kA	RFC
3921	[number]	k4	3921
(	(	kIx(	(
<g/>
samotný	samotný	k2eAgInSc1d1	samotný
instant	instant	k?	instant
messaging	messaging	k1gInSc1	messaging
a	a	k8xC	a
zobrazení	zobrazení	k1gNnSc1	zobrazení
stavu	stav	k1gInSc2	stav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
obsahující	obsahující	k2eAgInSc4d1	obsahující
některá	některý	k3yIgNnPc1	některý
další	další	k2eAgNnPc1d1	další
rozšíření	rozšíření	k1gNnPc1	rozšíření
XMPP	XMPP	kA	XMPP
protokolu	protokol	k1gInSc6	protokol
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
RFC	RFC	kA	RFC
3922	[number]	k4	3922
a	a	k8xC	a
RFC	RFC	kA	RFC
3923	[number]	k4	3923
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vývoj	vývoj	k1gInSc4	vývoj
protokolu	protokol	k1gInSc2	protokol
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
XMPP	XMPP	kA	XMPP
Standards	Standards	k1gInSc1	Standards
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
nad	nad	k7c4	nad
rámec	rámec	k1gInSc4	rámec
RFC	RFC	kA	RFC
jsou	být	k5eAaImIp3nP	být
vydávána	vydáván	k2eAgNnPc1d1	vydáváno
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
XEP	XEP	kA	XEP
(	(	kIx(	(
<g/>
XMPP	XMPP	kA	XMPP
Extension	Extension	k1gInSc1	Extension
Protocol	Protocol	k1gInSc1	Protocol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
(	(	kIx(	(
<g/>
v	v	k7c6	v
různém	různý	k2eAgInSc6d1	různý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
návrhů	návrh	k1gInPc2	návrh
až	až	k9	až
po	po	k7c4	po
standardy	standard	k1gInPc4	standard
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
XMPP	XMPP	kA	XMPP
je	být	k5eAaImIp3nS	být
implementací	implementace	k1gFnSc7	implementace
obecného	obecný	k2eAgInSc2d1	obecný
značkovacího	značkovací	k2eAgInSc2d1	značkovací
jazyka	jazyk	k1gInSc2	jazyk
XML	XML	kA	XML
<g/>
.	.	kIx.	.
</s>
<s>
Specifikace	specifikace	k1gFnPc1	specifikace
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
otevřené	otevřený	k2eAgFnPc1d1	otevřená
a	a	k8xC	a
dostupné	dostupný	k2eAgFnPc1d1	dostupná
všem	všecek	k3xTgFnPc3	všecek
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
mají	mít	k5eAaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
implementaci	implementace	k1gFnSc4	implementace
software	software	k1gInSc1	software
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
XMPP	XMPP	kA	XMPP
<g/>
.	.	kIx.	.
</s>
<s>
Servery	server	k1gInPc1	server
XMPP	XMPP	kA	XMPP
protokolu	protokol	k1gInSc2	protokol
běží	běžet	k5eAaImIp3nS	běžet
standardně	standardně	k6eAd1	standardně
na	na	k7c6	na
TCP	TCP	kA	TCP
portu	port	k1gInSc2	port
5222	[number]	k4	5222
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
komunikaci	komunikace	k1gFnSc4	komunikace
serverů	server	k1gInPc2	server
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
port	port	k1gInSc1	port
5269	[number]	k4	5269
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
využívající	využívající	k2eAgFnSc2d1	využívající
XMPP	XMPP	kA	XMPP
protokol	protokol	k1gInSc4	protokol
není	být	k5eNaImIp3nS	být
centralizovaná	centralizovaný	k2eAgFnSc1d1	centralizovaná
do	do	k7c2	do
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
IM	IM	kA	IM
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
distribuovaná	distribuovaný	k2eAgFnSc1d1	distribuovaná
na	na	k7c4	na
servery	server	k1gInPc4	server
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
si	se	k3xPyFc3	se
založit	založit	k5eAaPmF	založit
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
konto	konto	k1gNnSc4	konto
<g/>
.	.	kIx.	.
</s>
<s>
Identifikátory	identifikátor	k1gInPc1	identifikátor
uživatelů	uživatel	k1gMnPc2	uživatel
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
Jabber	Jabber	k1gInSc4	Jabber
ID	Ida	k1gFnPc2	Ida
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
JID	JID	kA	JID
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
základním	základní	k2eAgInSc6d1	základní
tvaru	tvar	k1gInSc6	tvar
syntakticky	syntakticky	k6eAd1	syntakticky
i	i	k9	i
sémanticky	sémanticky	k6eAd1	sémanticky
podobné	podobný	k2eAgFnPc4d1	podobná
e-mailovým	eailův	k2eAgFnPc3d1	e-mailova
adresám	adresa	k1gFnPc3	adresa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
server	server	k1gInSc1	server
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
si	se	k3xPyFc3	se
může	moct	k5eAaImIp3nS	moct
dále	daleko	k6eAd2	daleko
své	svůj	k3xOyFgNnSc4	svůj
konto	konto	k1gNnSc4	konto
rozdělit	rozdělit	k5eAaPmF	rozdělit
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jeho	jeho	k3xOp3gMnSc1	jeho
klient	klient	k1gMnSc1	klient
z	z	k7c2	z
domova	domov	k1gInSc2	domov
hlásit	hlásit	k5eAaImF	hlásit
jako	jako	k9	jako
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
server	server	k1gInSc1	server
<g/>
/	/	kIx~	/
<g/>
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
třeba	třeba	k6eAd1	třeba
i	i	k9	i
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
server	server	k1gInSc1	server
<g/>
/	/	kIx~	/
<g/>
prace	prace	k1gFnSc1	prace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
konto	konto	k1gNnSc4	konto
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
být	být	k5eAaImF	být
přihlášeno	přihlásit	k5eAaPmNgNnS	přihlásit
i	i	k9	i
více	hodně	k6eAd2	hodně
klientů	klient	k1gMnPc2	klient
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
buď	buď	k8xC	buď
podle	podle	k7c2	podle
celé	celý	k2eAgFnSc2d1	celá
adresy	adresa	k1gFnSc2	adresa
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ji	on	k3xPp3gFnSc4	on
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
zadá	zadat	k5eAaPmIp3nS	zadat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
nastavené	nastavený	k2eAgFnSc2d1	nastavená
priority	priorita	k1gFnSc2	priorita
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
serveru	server	k1gInSc3	server
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jenom	jenom	k9	jenom
tento	tento	k3xDgInSc1	tento
server	server	k1gInSc1	server
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ověřit	ověřit	k5eAaPmF	ověřit
jeho	jeho	k3xOp3gFnSc4	jeho
identitu	identita	k1gFnSc4	identita
například	například	k6eAd1	například
pomocí	pomocí	k7c2	pomocí
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
jména	jméno	k1gNnSc2	jméno
a	a	k8xC	a
hesla	heslo	k1gNnSc2	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
potřeba	potřeba	k6eAd1	potřeba
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
uživateli	uživatel	k1gMnPc7	uživatel
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
serverech	server	k1gInPc6	server
<g/>
,	,	kIx,	,
připojí	připojit	k5eAaPmIp3nS	připojit
se	se	k3xPyFc4	se
uživatelův	uživatelův	k2eAgInSc1d1	uživatelův
server	server	k1gInSc1	server
na	na	k7c4	na
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
server	server	k1gInSc4	server
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
si	se	k3xPyFc3	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
potřebné	potřebný	k2eAgFnPc4d1	potřebná
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vykonávat	vykonávat	k5eAaImF	vykonávat
například	například	k6eAd1	například
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zjistit	zjistit	k5eAaPmF	zjistit
"	"	kIx"	"
<g/>
prezenci	prezence	k1gFnSc4	prezence
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
uživatel	uživatel	k1gMnSc1	uživatel
na	na	k7c6	na
vzdáleném	vzdálený	k2eAgInSc6d1	vzdálený
serveru	server	k1gInSc6	server
přihlášen	přihlásit	k5eAaPmNgInS	přihlásit
a	a	k8xC	a
v	v	k7c6	v
jakém	jaký	k3yQgNnSc6	jaký
je	být	k5eAaImIp3nS	být
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
serverů	server	k1gInPc2	server
a	a	k8xC	a
klientů	klient	k1gMnPc2	klient
se	se	k3xPyFc4	se
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ještě	ještě	k9	ještě
různé	různý	k2eAgFnPc1d1	různá
služby	služba	k1gFnPc1	služba
–	–	k?	–
například	například	k6eAd1	například
služba	služba	k1gFnSc1	služba
víceuživatelských	víceuživatelský	k2eAgFnPc2d1	víceuživatelská
diskuzí	diskuze	k1gFnPc2	diskuze
(	(	kIx(	(
<g/>
funkčně	funkčně	k6eAd1	funkčně
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
IRC	IRC	kA	IRC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uživatelské	uživatelský	k2eAgInPc1d1	uživatelský
adresáře	adresář	k1gInPc1	adresář
a	a	k8xC	a
transporty	transport	k1gInPc1	transport
<g/>
.	.	kIx.	.
</s>
<s>
Transporty	transporta	k1gFnPc1	transporta
jsou	být	k5eAaImIp3nP	být
vlastně	vlastně	k9	vlastně
brány	brána	k1gFnPc1	brána
mezi	mezi	k7c7	mezi
XMPP	XMPP	kA	XMPP
sítí	síť	k1gFnSc7	síť
a	a	k8xC	a
IM	IM	kA	IM
sítí	síť	k1gFnSc7	síť
pracující	pracující	k2eAgFnSc7d1	pracující
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
protokolu	protokol	k1gInSc6	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
XMPP	XMPP	kA	XMPP
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
klient-server	klientervero	k1gNnPc2	klient-servero
(	(	kIx(	(
<g/>
klienti	klient	k1gMnPc1	klient
zpravidla	zpravidla	k6eAd1	zpravidla
nekomunikují	komunikovat	k5eNaImIp3nP	komunikovat
přímo	přímo	k6eAd1	přímo
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
decentralizována	decentralizovat	k5eAaBmNgFnS	decentralizovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
e-mail	eail	k1gInSc4	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
žádný	žádný	k3yNgInSc1	žádný
centrální	centrální	k2eAgInSc1d1	centrální
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
spojoval	spojovat	k5eAaImAgInS	spojovat
uživatele	uživatel	k1gMnSc4	uživatel
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
například	například	k6eAd1	například
u	u	k7c2	u
ICQ	ICQ	kA	ICQ
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
může	moct	k5eAaImIp3nS	moct
zřídit	zřídit	k5eAaPmF	zřídit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
server	server	k1gInSc4	server
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
uživateli	uživatel	k1gMnPc7	uživatel
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
serverech	server	k1gInPc6	server
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
svobodu	svoboda	k1gFnSc4	svoboda
volby	volba	k1gFnSc2	volba
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
má	mít	k5eAaImIp3nS	mít
důvěru	důvěra	k1gFnSc4	důvěra
a	a	k8xC	a
který	který	k3yRgInSc4	který
mu	on	k3xPp3gMnSc3	on
nabízí	nabízet	k5eAaImIp3nS	nabízet
lepší	dobrý	k2eAgFnPc4d2	lepší
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
změny	změna	k1gFnSc2	změna
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
e-mailu	eail	k1gInSc2	e-mail
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
existuje	existovat	k5eAaImIp3nS	existovat
spousta	spousta	k1gFnSc1	spousta
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
zdarma	zdarma	k6eAd1	zdarma
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
bez	bez	k7c2	bez
potřeby	potřeba	k1gFnSc2	potřeba
tvorby	tvorba	k1gFnSc2	tvorba
vlastního	vlastní	k2eAgInSc2d1	vlastní
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
je	být	k5eAaImIp3nS	být
identifikován	identifikován	k2eAgInSc1d1	identifikován
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
názvem	název	k1gInSc7	název
serveru	server	k1gInSc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
odděleny	oddělit	k5eAaPmNgFnP	oddělit
znakem	znak	k1gInSc7	znak
@	@	kIx~	@
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
např.	např.	kA	např.
pepa@jehoserver.cz	pepa@jehoserver.cz	k1gInSc1	pepa@jehoserver.cz
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
řetězec	řetězec	k1gInSc1	řetězec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Jabber	Jabber	k1gInSc1	Jabber
ID	Ida	k1gFnPc2	Ida
nebo	nebo	k8xC	nebo
také	také	k9	také
JID	JID	kA	JID
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
při	při	k7c6	při
komunikaci	komunikace	k1gFnSc6	komunikace
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
uživateli	uživatel	k1gMnPc7	uživatel
<g/>
,	,	kIx,	,
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
názorný	názorný	k2eAgInSc1d1	názorný
příklad	příklad	k1gInSc1	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelka	uživatelka	k1gFnSc1	uživatelka
Julie	Julie	k1gFnSc1	Julie
má	mít	k5eAaImIp3nS	mít
účet	účet	k1gInSc4	účet
na	na	k7c6	na
serveru	server	k1gInSc6	server
Kapuletova	Kapuletův	k2eAgInSc2d1	Kapuletův
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
její	její	k3xOp3gFnSc1	její
JID	JID	kA	JID
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc4	tvar
julie@kapuletova.cz	julie@kapuletova.cza	k1gFnPc2	julie@kapuletova.cza
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
si	se	k3xPyFc3	se
povídat	povídat	k5eAaImF	povídat
s	s	k7c7	s
Romeem	Romeo	k1gMnSc7	Romeo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
JID	JID	kA	JID
je	být	k5eAaImIp3nS	být
romeo@montek.com	romeo@montek.com	k1gInSc1	romeo@montek.com
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Julie	Julie	k1gFnSc1	Julie
napíše	napsat	k5eAaPmIp3nS	napsat
zprávu	zpráva	k1gFnSc4	zpráva
a	a	k8xC	a
pošle	pošle	k6eAd1	pošle
ji	on	k3xPp3gFnSc4	on
Romeovi	Romeův	k2eAgMnPc1d1	Romeův
<g/>
,	,	kIx,	,
provede	provést	k5eAaPmIp3nS	provést
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
akcí	akce	k1gFnPc2	akce
<g/>
:	:	kIx,	:
XMPP	XMPP	kA	XMPP
klient	klient	k1gMnSc1	klient
Julie	Julie	k1gFnSc2	Julie
pošle	poslat	k5eAaPmIp3nS	poslat
její	její	k3xOp3gFnSc4	její
zprávu	zpráva	k1gFnSc4	zpráva
serveru	server	k1gInSc2	server
Kapuletova	Kapuletův	k2eAgInSc2d1	Kapuletův
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Montek	Montek	k1gInSc1	Montek
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
blokován	blokován	k2eAgInSc1d1	blokován
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
smazána	smazat	k5eAaPmNgFnS	smazat
(	(	kIx(	(
<g/>
zpět	zpět	k6eAd1	zpět
je	být	k5eAaImIp3nS	být
zasláno	zaslat	k5eAaPmNgNnS	zaslat
chybové	chybový	k2eAgNnSc1d1	chybové
hlášení	hlášení	k1gNnSc1	hlášení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Kapuletova	Kapuletův	k2eAgMnSc2d1	Kapuletův
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
otevře	otevřít	k5eAaPmIp3nS	otevřít
spojení	spojení	k1gNnSc4	spojení
k	k	k7c3	k
serveru	server	k1gInSc3	server
Montek	Montka	k1gFnPc2	Montka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
a	a	k8xC	a
předá	předat	k5eAaPmIp3nS	předat
mu	on	k3xPp3gMnSc3	on
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
Montek	Montek	k1gInSc1	Montek
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
doručí	doručit	k5eAaPmIp3nS	doručit
zprávu	zpráva	k1gFnSc4	zpráva
klientovi	klient	k1gMnSc3	klient
Romea	Romeo	k1gMnSc2	Romeo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
server	server	k1gInSc1	server
Kapuletova	Kapuletův	k2eAgMnSc2d1	Kapuletův
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
na	na	k7c4	na
Montek	Montek	k1gInSc4	Montek
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
blokován	blokován	k2eAgInSc1d1	blokován
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
bude	být	k5eAaImBp3nS	být
smazána	smazat	k5eAaPmNgFnS	smazat
(	(	kIx(	(
<g/>
zpět	zpět	k6eAd1	zpět
je	být	k5eAaImIp3nS	být
zaslána	zaslat	k5eAaPmNgFnS	zaslat
chybová	chybový	k2eAgFnSc1d1	chybová
hláška	hláška	k1gFnSc1	hláška
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
Romeo	Romeo	k1gMnSc1	Romeo
právě	právě	k6eAd1	právě
připojen	připojen	k2eAgMnSc1d1	připojen
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
uschová	uschovat	k5eAaPmIp3nS	uschovat
(	(	kIx(	(
<g/>
na	na	k7c4	na
montek	montek	k1gInSc4	montek
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
doručena	doručit	k5eAaPmNgFnS	doručit
při	při	k7c6	při
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
příležitosti	příležitost	k1gFnSc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Jabber	Jabber	k1gInSc1	Jabber
ID	Ida	k1gFnPc2	Ida
neboli	neboli	k8xC	neboli
také	také	k9	také
JID	JID	kA	JID
jsou	být	k5eAaImIp3nP	být
uživatelská	uživatelský	k2eAgNnPc1d1	Uživatelské
jména	jméno	k1gNnPc1	jméno
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
k	k	k7c3	k
XMPP	XMPP	kA	XMPP
účtu	účet	k1gInSc6	účet
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tvaru	tvar	k1gInSc2	tvar
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
domena	domena	k1gFnSc1	domena
<g/>
/	/	kIx~	/
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgNnPc1d1	podobné
e-mailovým	eailův	k2eAgFnPc3d1	e-mailova
adresám	adresa	k1gFnPc3	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
zdroj	zdroj	k1gInSc1	zdroj
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
uživateli	uživatel	k1gMnSc3	uživatel
individuální	individuální	k2eAgFnSc2d1	individuální
připojení	připojení	k1gNnSc4	připojení
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
účet	účet	k1gInSc4	účet
z	z	k7c2	z
více	hodně	k6eAd2	hodně
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
domena	domena	k1gFnSc1	domena
<g/>
/	/	kIx~	/
<g/>
doma	doma	k6eAd1	doma
a	a	k8xC	a
uzivatel	uzivatel	k1gMnSc1	uzivatel
<g/>
@	@	kIx~	@
<g/>
domena	domena	k1gFnSc1	domena
<g/>
/	/	kIx~	/
<g/>
prace	prace	k1gFnSc1	prace
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
není	být	k5eNaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
uvádět	uvádět	k5eAaImF	uvádět
pro	pro	k7c4	pro
kontaktování	kontaktování	k1gNnSc4	kontaktování
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyžadován	vyžadovat	k5eAaImNgMnS	vyžadovat
například	například	k6eAd1	například
při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
JID	JID	kA	JID
může	moct	k5eAaImIp3nS	moct
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
několik	několik	k4yIc4	několik
výjimek	výjimka	k1gFnPc2	výjimka
<g/>
,	,	kIx,	,
obsahovat	obsahovat	k5eAaImF	obsahovat
libovolné	libovolný	k2eAgInPc4d1	libovolný
znaky	znak	k1gInPc4	znak
unicode	unicod	k1gInSc5	unicod
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
i	i	k8xC	i
česká	český	k2eAgNnPc1d1	české
písmena	písmeno	k1gNnPc1	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
.	.	kIx.	.
rok	rok	k1gInSc1	rok
1998	[number]	k4	1998
–	–	k?	–
Jeremie	Jeremie	k1gFnSc2	Jeremie
Miller	Miller	k1gMnSc1	Miller
založil	založit	k5eAaPmAgMnS	založit
projekt	projekt	k1gInSc4	projekt
Jabber	Jabber	k1gMnSc1	Jabber
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1999	[number]	k4	1999
–	–	k?	–
Jeremie	Jeremie	k1gFnSc2	Jeremie
podepsal	podepsat	k5eAaPmAgMnS	podepsat
prohlášení	prohlášení	k1gNnSc4	prohlášení
zaručující	zaručující	k2eAgFnSc4d1	zaručující
podporu	podpora	k1gFnSc4	podpora
Jabber	Jabber	k1gMnSc1	Jabber
komunity	komunita	k1gFnSc2	komunita
IETF	IETF	kA	IETF
standardizaci	standardizace	k1gFnSc6	standardizace
<g/>
.	.	kIx.	.
květen	květen	k1gInSc1	květen
2000	[number]	k4	2000
–	–	k?	–
Uvolněn	uvolněn	k2eAgInSc4d1	uvolněn
první	první	k4xOgInSc4	první
software	software	k1gInSc4	software
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2000	[number]	k4	2000
–	–	k?	–
Jeremie	Jeremie	k1gFnSc1	Jeremie
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
projektu	projekt	k1gInSc2	projekt
Jabber	Jabbra	k1gFnPc2	Jabbra
se	se	k3xPyFc4	se
upsali	upsat	k5eAaPmAgMnP	upsat
koncepci	koncepce	k1gFnSc4	koncepce
IMPP	IMPP	kA	IMPP
dokumentující	dokumentující	k2eAgMnSc1d1	dokumentující
Jabber	Jabber	k1gMnSc1	Jabber
protokol	protokol	k1gInSc4	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nesoustředěnosti	nesoustředěnost	k1gFnSc3	nesoustředěnost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
komunity	komunita	k1gFnSc2	komunita
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
plně	plně	k6eAd1	plně
následovat	následovat	k5eAaImF	následovat
IMPP	IMPP	kA	IMPP
či	či	k8xC	či
jiné	jiný	k2eAgFnSc2d1	jiná
IETF	IETF	kA	IETF
snahy	snaha	k1gFnSc2	snaha
<g/>
.	.	kIx.	.
rok	rok	k1gInSc1	rok
2001	[number]	k4	2001
–	–	k?	–
Vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
Jabber	Jabber	k1gInSc4	Jabber
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
JSF	JSF	kA	JSF
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
organizace	organizace	k1gFnSc2	organizace
nad	nad	k7c7	nad
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
počtem	počet	k1gInSc7	počet
open	open	k1gInSc1	open
source	sourec	k1gInSc2	sourec
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
komerčních	komerční	k2eAgFnPc2d1	komerční
entit	entita	k1gFnPc2	entita
budujících	budující	k2eAgFnPc2d1	budující
či	či	k8xC	či
používajících	používající	k2eAgFnPc2d1	používající
technologie	technologie	k1gFnPc4	technologie
XMPP	XMPP	kA	XMPP
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
cílů	cíl	k1gInPc2	cíl
JSF	JSF	kA	JSF
byla	být	k5eAaImAgFnS	být
dokumentace	dokumentace	k1gFnSc1	dokumentace
XML	XML	kA	XML
protokolu	protokol	k1gInSc2	protokol
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
další	další	k2eAgNnSc4d1	další
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
2002	[number]	k4	2002
–	–	k?	–
Nově	nově	k6eAd1	nově
podepsána	podepsán	k2eAgFnSc1d1	podepsána
koncepce	koncepce	k1gFnSc1	koncepce
IETF	IETF	kA	IETF
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
úspěchu	úspěch	k1gInSc2	úspěch
tohoto	tento	k3xDgInSc2	tento
podpisu	podpis	k1gInSc2	podpis
bylo	být	k5eAaImAgNnS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
možnosti	možnost	k1gFnPc4	možnost
zformování	zformování	k1gNnSc2	zformování
IETF	IETF	kA	IETF
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
pro	pro	k7c4	pro
diskuse	diskuse	k1gFnPc4	diskuse
o	o	k7c4	o
Jabber	Jabber	k1gInSc4	Jabber
protokolu	protokol	k1gInSc2	protokol
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
XMPP	XMPP	kA	XMPP
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
čehož	což	k3yQnSc2	což
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgInPc1d1	podepsán
tři	tři	k4xCgFnPc1	tři
nové	nový	k2eAgFnPc1d1	nová
koncepce	koncepce	k1gFnPc1	koncepce
21	[number]	k4	21
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
29	[number]	k4	29
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
2004	[number]	k4	2004
–	–	k?	–
IESG	IESG	kA	IESG
uznala	uznat	k5eAaPmAgFnS	uznat
XMPP	XMPP	kA	XMPP
Core	Core	k1gFnSc1	Core
a	a	k8xC	a
XMPP	XMPP	kA	XMPP
IM	IM	kA	IM
jako	jako	k8xC	jako
<g />
.	.	kIx.	.
</s>
<s>
navrhované	navrhovaný	k2eAgInPc1d1	navrhovaný
standardy	standard	k1gInPc1	standard
4	[number]	k4	4
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2004	[number]	k4	2004
–	–	k?	–
IETF	IETF	kA	IETF
uznala	uznat	k5eAaPmAgFnS	uznat
XMPP	XMPP	kA	XMPP
jako	jako	k8xS	jako
standard	standard	k1gInSc1	standard
pro	pro	k7c4	pro
IM	IM	kA	IM
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
2001	[number]	k4	2001
–	–	k?	–
založen	založit	k5eAaPmNgInS	založit
server	server	k1gInSc1	server
jabber	jabber	k1gInSc1	jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2001	[number]	k4	2001
–	–	k?	–
založen	založen	k2eAgInSc1d1	založen
server	server	k1gInSc1	server
njs	njs	k?	njs
<g/>
.	.	kIx.	.
<g/>
netlab	netlab	k1gMnSc1	netlab
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
provozovaný	provozovaný	k2eAgInSc1d1	provozovaný
společností	společnost	k1gFnSc7	společnost
HumboldTec	HumboldTec	k1gMnSc1	HumboldTec
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2005	[number]	k4	2005
–	–	k?	–
njs	njs	k?	njs
<g/>
.	.	kIx.	.
<g/>
netlab	netlab	k1gMnSc1	netlab
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
největším	veliký	k2eAgInSc7d3	veliký
serverem	server	k1gInSc7	server
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
pohltil	pohltit	k5eAaPmAgInS	pohltit
server	server	k1gInSc1	server
jabber	jabber	k1gInSc1	jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
–	–	k?	–
za	za	k7c4	za
spolufinancování	spolufinancování	k1gNnSc4	spolufinancování
provozu	provoz	k1gInSc6	provoz
obou	dva	k4xCgInPc2	dva
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
sjednocených	sjednocený	k2eAgFnPc2d1	sjednocená
pod	pod	k7c4	pod
projekt	projekt	k1gInSc4	projekt
jabbim	jabbim	k1gInSc4	jabbim
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uživatelům	uživatel	k1gMnPc3	uživatel
nabídnuty	nabídnut	k2eAgFnPc4d1	nabídnuta
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2007	[number]	k4	2007
–	–	k?	–
Jabbim	Jabbim	k1gInSc1	Jabbim
oznámil	oznámit	k5eAaPmAgInS	oznámit
dosažení	dosažení	k1gNnSc4	dosažení
70	[number]	k4	70
000	[number]	k4	000
registrací	registrace	k1gFnPc2	registrace
<g/>
,	,	kIx,	,
14	[number]	k4	14
000	[number]	k4	000
aktivních	aktivní	k2eAgMnPc2d1	aktivní
uživatelů	uživatel	k1gMnPc2	uživatel
a	a	k8xC	a
přes	přes	k7c4	přes
5	[number]	k4	5
000	[number]	k4	000
přihlášených	přihlášený	k2eAgMnPc2d1	přihlášený
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Google	Google	k1gFnSc2	Google
Talk	Talka	k1gFnPc2	Talka
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gFnSc1	Google
uvedla	uvést	k5eAaPmAgFnS	uvést
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
nový	nový	k2eAgInSc1d1	nový
projekt	projekt	k1gInSc1	projekt
Google	Google	k1gFnSc2	Google
Talk	Talka	k1gFnPc2	Talka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
služba	služba	k1gFnSc1	služba
pro	pro	k7c4	pro
Instant	Instant	k1gInSc4	Instant
Messaging	Messaging	k1gInSc1	Messaging
využívá	využívat	k5eAaImIp3nS	využívat
protokolu	protokol	k1gInSc3	protokol
XMPP	XMPP	kA	XMPP
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
přidala	přidat	k5eAaPmAgFnS	přidat
možnost	možnost	k1gFnSc4	možnost
hlasové	hlasový	k2eAgFnSc2d1	hlasová
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejíž	jejíž	k3xOyRp3gFnSc6	jejíž
specifikaci	specifikace	k1gFnSc6	specifikace
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
XMPP	XMPP	kA	XMPP
Standards	Standards	k1gInSc1	Standards
Foundation	Foundation	k1gInSc1	Foundation
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
možnost	možnost	k1gFnSc1	možnost
video	video	k1gNnSc4	video
hovorů	hovor	k1gInPc2	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Bombus	Bombus	k1gInSc1	Bombus
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
s	s	k7c7	s
Java	Javum	k1gNnPc1	Javum
ME	ME	kA	ME
<g/>
)	)	kIx)	)
Bombus-ng	Bombusg	k1gMnSc1	Bombus-ng
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnPc4	zařízení
s	s	k7c7	s
Windows	Windows	kA	Windows
Mobile	mobile	k1gNnSc1	mobile
<g/>
;	;	kIx,	;
http://bombus-im.org/wiki/bombus/pocketpc	[url]	k1gFnSc1	http://bombus-im.org/wiki/bombus/pocketpc
<g/>
)	)	kIx)	)
–	–	k?	–
neplatný	platný	k2eNgInSc4d1	neplatný
odkaz	odkaz	k1gInSc4	odkaz
!	!	kIx.	!
</s>
<s>
Coccinella	Coccinella	k1gFnSc1	Coccinella
(	(	kIx(	(
<g/>
http://coccinella.im/	[url]	k?	http://coccinella.im/
<g/>
)	)	kIx)	)
Empathy	Empatha	k1gMnSc2	Empatha
(	(	kIx(	(
<g/>
klient	klient	k1gMnSc1	klient
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
hlasu	hlas	k1gInSc2	hlas
a	a	k8xC	a
videa	video	k1gNnSc2	video
<g/>
,	,	kIx,	,
kompatibilní	kompatibilní	k2eAgInPc4d1	kompatibilní
s	s	k7c7	s
Google	Google	k1gNnSc7	Google
Talkem	talek	k1gInSc7	talek
<g/>
;	;	kIx,	;
http://live.gnome.org/Empathy	[url]	k1gFnSc2	http://live.gnome.org/Empathy
<g/>
]	]	kIx)	]
Gajim	Gajim	k1gInSc1	Gajim
(	(	kIx(	(
<g/>
http://www.gajim.org/	[url]	k?	http://www.gajim.org/
<g/>
)	)	kIx)	)
Jabbim	Jabbim	k1gMnSc1	Jabbim
JWChat	JWChat	k1gMnSc1	JWChat
(	(	kIx(	(
<g/>
webový	webový	k2eAgMnSc1d1	webový
klient	klient	k1gMnSc1	klient
<g/>
;	;	kIx,	;
některé	některý	k3yIgInPc1	některý
servery	server	k1gInPc1	server
provozují	provozovat	k5eAaImIp3nP	provozovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
;	;	kIx,	;
http://jwchat.org	[url]	k1gMnSc1	http://jwchat.org
<g/>
)	)	kIx)	)
mcabber	mcabber	k1gMnSc1	mcabber
(	(	kIx(	(
<g/>
http://lilotux.net/~mikael/mcabber/	[url]	k?	http://lilotux.net/~mikael/mcabber/
<g/>
)	)	kIx)	)
Psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
http://psi-im.org/	[url]	k?	http://psi-im.org/
<g/>
)	)	kIx)	)
Spark	Spark	k1gInSc1	Spark
Tkabber	Tkabber	k1gInSc1	Tkabber
(	(	kIx(	(
<g/>
http://tkabber.jabber.ru/	[url]	k?	http://tkabber.jabber.ru/
<g/>
)	)	kIx)	)
Xeus	Xeus	k1gInSc1	Xeus
Messenger	Messenger	k1gInSc1	Messenger
<g/>
/	/	kIx~	/
<g/>
glu	glu	k?	glu
(	(	kIx(	(
<g/>
http://xeus-messenger.blogspot.com/	[url]	k?	http://xeus-messenger.blogspot.com/
<g/>
)	)	kIx)	)
–	–	k?	–
neplatný	platný	k2eNgInSc4d1	neplatný
odkaz	odkaz	k1gInSc4	odkaz
!	!	kIx.	!
</s>
<s>
Xabber	Xabber	k1gMnSc1	Xabber
(	(	kIx(	(
<g/>
open-source	openourka	k1gFnSc6	open-sourka
klient	klient	k1gMnSc1	klient
pro	pro	k7c4	pro
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
Android	android	k1gInSc1	android
<g/>
;	;	kIx,	;
http://www.xabber.com	[url]	k1gInSc1	http://www.xabber.com
<g/>
)	)	kIx)	)
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc4d1	další
klienty	klient	k1gMnPc4	klient
naleznete	nalézt	k5eAaBmIp2nP	nalézt
např.	např.	kA	např.
na	na	k7c4	na
Jabber	Jabber	k1gInSc4	Jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Wiki	Wiki	k1gNnSc2	Wiki
<g/>
.	.	kIx.	.
</s>
<s>
Adium	Adium	k1gNnSc1	Adium
(	(	kIx(	(
<g/>
http://www.adium.im/	[url]	k?	http://www.adium.im/
<g/>
)	)	kIx)	)
BitlBee	BitlBee	k1gFnSc1	BitlBee
(	(	kIx(	(
<g/>
http://www.bitlbee.org/	[url]	k?	http://www.bitlbee.org/
<g/>
)	)	kIx)	)
CenterIM	CenterIM	k1gMnSc1	CenterIM
(	(	kIx(	(
<g/>
pokračovatel	pokračovatel	k1gMnSc1	pokračovatel
CenterICQ	CenterICQ	k1gMnSc1	CenterICQ
<g/>
;	;	kIx,	;
http://www.centerim.org/	[url]	k?	http://www.centerim.org/
<g/>
)	)	kIx)	)
Digsby	Digsba	k1gFnSc2	Digsba
Empathy	Empatha	k1gFnSc2	Empatha
iChat	iChat	k1gInSc1	iChat
(	(	kIx(	(
<g/>
http://www.apple.com/macosx/features/ichat/	[url]	k?	http://www.apple.com/macosx/features/ichat/
<g/>
)	)	kIx)	)
–	–	k?	–
neplatný	platný	k2eNgInSc4d1	neplatný
odkaz	odkaz	k1gInSc4	odkaz
!	!	kIx.	!
</s>
<s>
Kopete	kopat	k5eAaImIp2nP	kopat
Meebo	Meeba	k1gFnSc5	Meeba
(	(	kIx(	(
<g/>
webový	webový	k2eAgMnSc1d1	webový
klient	klient	k1gMnSc1	klient
<g/>
;	;	kIx,	;
http://www.meebo.com	[url]	k1gInSc1	http://www.meebo.com
<g/>
)	)	kIx)	)
Miranda	Miranda	k1gFnSc1	Miranda
Mozilla	Mozilla	k1gMnSc1	Mozilla
Thunderbird	Thunderbird	k1gMnSc1	Thunderbird
(	(	kIx(	(
<g/>
poštovní	poštovní	k1gMnSc1	poštovní
a	a	k8xC	a
IM	IM	kA	IM
klient	klient	k1gMnSc1	klient
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
;	;	kIx,	;
https://www.mozilla.org/cs/thunderbird/	[url]	k?	https://www.mozilla.org/cs/thunderbird/
<g/>
)	)	kIx)	)
Pidgin	Pidgin	k1gMnSc1	Pidgin
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Gaim	Gaim	k1gInSc1	Gaim
<g/>
)	)	kIx)	)
qutIM	qutIM	k?	qutIM
QIP	QIP	kA	QIP
Infium	Infium	k1gNnSc4	Infium
SIM-IM	SIM-IM	k1gFnSc2	SIM-IM
Trillian	Trilliana	k1gFnPc2	Trilliana
Open	Opena	k1gFnPc2	Opena
Discussion	Discussion	k1gInSc1	Discussion
Day	Day	k1gMnSc1	Day
Stav	stav	k1gInSc1	stav
účastníka	účastník	k1gMnSc2	účastník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
<g />
.	.	kIx.	.
</s>
<s>
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Extensible	Extensible	k1gFnSc2	Extensible
Messaging	Messaging	k1gInSc1	Messaging
and	and	k?	and
Presence	presence	k1gFnSc2	presence
Protocol	Protocola	k1gFnPc2	Protocola
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
–	–	k?	–
XMPP	XMPP	kA	XMPP
Standards	Standards	k1gInSc1	Standards
Foundation	Foundation	k1gInSc1	Foundation
Jabber	Jabber	k1gInSc4	Jabber
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Wiki	Wik	k1gFnSc2	Wik
–	–	k?	–
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc4	množství
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
XMPP	XMPP	kA	XMPP
<g/>
/	/	kIx~	/
<g/>
Jabberu	Jabber	k1gInSc2	Jabber
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Jabber	Jabbero	k1gNnPc2	Jabbero
–	–	k?	–
začínáme	začínat	k5eAaImIp1nP	začínat
–	–	k?	–
návod	návod	k1gInSc4	návod
jak	jak	k8xC	jak
začít	začít	k5eAaPmF	začít
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
Jabberem	Jabber	k1gInSc7	Jabber
pro	pro	k7c4	pro
úplné	úplný	k2eAgMnPc4d1	úplný
začátečníky	začátečník	k1gMnPc4	začátečník
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Jabber	Jabber	k1gInSc1	Jabber
–	–	k?	–
komunikačný	komunikačný	k2eAgInSc1d1	komunikačný
protokol	protokol	k1gInSc1	protokol
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
