<s>
Síť	síť	k1gFnSc1	síť
XMPP	XMPP	kA	XMPP
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
klient-server	klientervero	k1gNnPc2	klient-servero
(	(	kIx(	(
<g/>
klienti	klient	k1gMnPc1	klient
zpravidla	zpravidla	k6eAd1	zpravidla
nekomunikují	komunikovat	k5eNaImIp3nP	komunikovat
přímo	přímo	k6eAd1	přímo
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
decentralizována	decentralizovat	k5eAaBmNgFnS	decentralizovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
e-mail	eail	k1gInSc4	e-mail
<g/>
.	.	kIx.	.
</s>
