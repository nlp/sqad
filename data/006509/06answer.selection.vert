<s>
Přitom	přitom	k6eAd1
se	se	k3xPyFc4
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
dalších	další	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
nedochovalo	dochovat	k5eNaPmAgNnS
a	a	k8xC
do	do	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
nejsou	být	k5eNaImIp3nP
započítány	započítat	k5eAaPmNgFnP
ani	ani	k8xC
autorem	autor	k1gMnSc7
revidované	revidovaný	k2eAgFnSc2d1
verze	verze	k1gFnSc2
skladeb	skladba	k1gFnPc2
a	a	k8xC
nejrůznější	různý	k2eAgInPc4d3
hudební	hudební	k2eAgInPc4d1
kusy	kus	k1gInPc4
sloužící	sloužící	k2eAgInPc4d1
k	k	k7c3
výuce	výuka	k1gFnSc3
-	-	kIx~
celkově	celkově	k6eAd1
tak	tak	k9
Bach	Bach	k1gMnSc1
složil	složit	k5eAaPmAgMnS
nejméně	málo	k6eAd3
kolem	kolem	k7c2
1400	[number]	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>