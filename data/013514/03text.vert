<s>
Triskaidekafobie	Triskaidekafobie	k1gFnSc1
</s>
<s>
Číselník	číselník	k1gInSc1
šanghajského	šanghajský	k2eAgInSc2d1
hotelového	hotelový	k2eAgInSc2d1
výtahu	výtah	k1gInSc2
bez	bez	k7c2
„	„	k?
<g/>
nešťastných	šťastný	k2eNgInPc2d1
<g/>
“	“	k?
poschodí	poschodí	k1gNnSc6
</s>
<s>
Triskaidekafobie	Triskaidekafobie	k1gFnSc1
je	být	k5eAaImIp3nS
fobie	fobie	k1gFnPc1
označující	označující	k2eAgInSc4d1
chorobný	chorobný	k2eAgInSc4d1
strach	strach	k1gInSc4
z	z	k7c2
čísla	číslo	k1gNnSc2
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postižení	postižení	k1gNnPc1
se	se	k3xPyFc4
obávají	obávat	k5eAaImIp3nP
jakékoliv	jakýkoliv	k3yIgFnSc2
přítomnosti	přítomnost	k1gFnSc2
čísla	číslo	k1gNnSc2
třináct	třináct	k4xCc4
–	–	k?
13	#num#	k4
<g/>
.	.	kIx.
den	den	k1gInSc4
v	v	k7c6
měsíci	měsíc	k1gInSc6
je	být	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
nešťastný	šťastný	k2eNgInSc1d1
<g/>
,	,	kIx,
nemohou	moct	k5eNaImIp3nP
bydlet	bydlet	k5eAaImF
v	v	k7c6
hotelovém	hotelový	k2eAgInSc6d1
pokoji	pokoj	k1gInSc6
s	s	k7c7
tímto	tento	k3xDgNnSc7
číslem	číslo	k1gNnSc7
apod.	apod.	kA
</s>
<s>
Strach	strach	k1gInSc1
a	a	k8xC
pověra	pověra	k1gFnSc1
ze	z	k7c2
třináctky	třináctka	k1gFnSc2
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
rozšířené	rozšířený	k2eAgFnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
např.	např.	kA
v	v	k7c6
některých	některý	k3yIgInPc6
hotelech	hotel	k1gInPc6
vůbec	vůbec	k9
nevyskytuje	vyskytovat	k5eNaImIp3nS
pokoj	pokoj	k1gInSc4
s	s	k7c7
tímto	tento	k3xDgNnSc7
číslem	číslo	k1gNnSc7
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
zase	zase	k9
na	na	k7c6
číselníku	číselník	k1gInSc6
ve	v	k7c6
výtahu	výtah	k1gInSc6
mezi	mezi	k7c7
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
14	#num#	k4
<g/>
.	.	kIx.
patrem	patro	k1gNnSc7
zcela	zcela	k6eAd1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
13	#num#	k4
<g/>
.	.	kIx.
patro	patro	k1gNnSc1
(	(	kIx(
<g/>
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
jen	jen	k9
o	o	k7c4
klam	klam	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
třinácté	třináctý	k4xOgNnSc1
patro	patro	k1gNnSc1
bude	být	k5eAaImBp3nS
fyzicky	fyzicky	k6eAd1
existovat	existovat	k5eAaImF
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
bude	být	k5eAaImBp3nS
označeno	označit	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
)	)	kIx)
nebo	nebo	k8xC
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
jednat	jednat	k5eAaImF
o	o	k7c4
neobytné	obytný	k2eNgNnSc4d1
technické	technický	k2eAgNnSc4d1
podlaží	podlaží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Podobnou	podobný	k2eAgFnSc7d1
fobií	fobie	k1gFnSc7
je	být	k5eAaImIp3nS
tetrafobie	tetrafobie	k1gFnSc1
<g/>
,	,	kIx,
chorobný	chorobný	k2eAgInSc1d1
strach	strach	k1gInSc1
z	z	k7c2
čísla	číslo	k1gNnSc2
4	#num#	k4
častý	častý	k2eAgInSc1d1
ve	v	k7c6
Východní	východní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
hexakosioihexekontahexafobie	hexakosioihexekontahexafobie	k1gFnSc1
<g/>
,	,	kIx,
chorobný	chorobný	k2eAgInSc1d1
strach	strach	k1gInSc1
z	z	k7c2
čísla	číslo	k1gNnSc2
666	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
fobie	fobie	k1gFnSc1
je	být	k5eAaImIp3nS
občas	občas	k6eAd1
mylně	mylně	k6eAd1
ztotožňována	ztotožňován	k2eAgFnSc1d1
s	s	k7c7
paraskevidekatriafobií	paraskevidekatriafobie	k1gFnSc7
–	–	k?
pověrou	pověra	k1gFnSc7
o	o	k7c6
nešťastném	šťastný	k2eNgInSc6d1
pátku	pátek	k1gInSc6
třináctého	třináctý	k4xOgNnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
mnozí	mnohý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
smolný	smolný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
se	s	k7c7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
odborníci	odborník	k1gMnPc1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
autosugesci	autosugesce	k1gFnSc4
–	–	k?
člověk	člověk	k1gMnSc1
si	se	k3xPyFc3
zafixuje	zafixovat	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c4
tento	tento	k3xDgInSc4
den	den	k1gInSc4
něco	něco	k6eAd1
nepodaří	podařit	k5eNaPmIp3nS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
také	také	k9
často	často	k6eAd1
stane	stanout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roli	role	k1gFnSc4
může	moct	k5eAaImIp3nS
případně	případně	k6eAd1
hrát	hrát	k5eAaImF
i	i	k9
selektivnost	selektivnost	k1gFnSc4
paměti	paměť	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
že	že	k8xS
kdyby	kdyby	kYmCp3nS
se	se	k3xPyFc4
totéž	týž	k3xTgNnSc1
stalo	stát	k5eAaPmAgNnS
jiný	jiný	k2eAgInSc4d1
den	den	k1gInSc4
<g/>
,	,	kIx,
člověk	člověk	k1gMnSc1
si	se	k3xPyFc3
ani	ani	k9
nezapamatuje	zapamatovat	k5eNaPmIp3nS
kdy	kdy	k6eAd1
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pokud	pokud	k8xS
se	se	k3xPyFc4
něco	něco	k3yInSc4
stane	stanout	k5eAaPmIp3nS
zrovna	zrovna	k6eAd1
pátek	pátek	k1gInSc4
13	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
konkrétní	konkrétní	k2eAgFnSc4d1
smůlu	smůla	k1gFnSc4
si	se	k3xPyFc3
s	s	k7c7
datem	datum	k1gNnSc7
spojí	spojit	k5eAaPmIp3nP
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
hexafobie	hexafobie	k1gFnSc1
</s>
<s>
hexakosioihexekontahexafobie	hexakosioihexekontahexafobie	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
triskaidekafobie	triskaidekafobie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
triskaidekafobie	triskaidekafobie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
