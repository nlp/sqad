<s>
Winnipeg	Winnipeg	k1gMnSc1
James	James	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Richardson	Richardson	k1gMnSc1
International	International	k1gMnSc1
Airport	Airport	k1gInSc4
</s>
<s>
Winnipeg	Winnipeg	k1gMnSc1
James	James	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Richardson	Richardson	k1gMnSc1
International	International	k1gMnSc1
Airport	Airport	k1gInSc4
Letiště	letiště	k1gNnSc2
během	během	k7c2
zimyZákladní	zimyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Kód	kód	k1gInSc1
letiště	letiště	k1gNnSc4
ICAO	ICAO	kA
</s>
<s>
CYWG	CYWG	kA
Kód	kód	k1gInSc1
letiště	letiště	k1gNnSc2
IATA	IATA	kA
</s>
<s>
YWG	YWG	kA
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
97	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
Web	web	k1gInSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
Jamese	Jamese	k1gFnSc2
Armstrong	Armstrong	k1gMnSc1
Richardsona	Richardsona	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Winnipeg	Winnipeg	k1gMnSc1
James	James	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Richardson	Richardson	k1gMnSc1
International	International	k1gMnSc1
Airport	Airport	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
Mezinárodní	mezinárodní	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
Winnipeg	Winnipega	k1gFnPc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
IATA	IATA	kA
<g/>
:	:	kIx,
YWG	YWG	kA
<g/>
,	,	kIx,
ICAO	ICAO	kA
<g/>
:	:	kIx,
CYWG	CYWG	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
v	v	k7c6
městě	město	k1gNnSc6
Winnipeg	Winnipega	k1gFnPc2
v	v	k7c6
kanadské	kanadský	k2eAgFnSc6d1
provincii	provincie	k1gFnSc6
Manitoba	Manitoba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
3,4	3,4	k4
miliony	milion	k4xCgInPc7
přepravených	přepravený	k2eAgMnPc2d1
cestujících	cestující	k1gMnPc2
ročně	ročně	k6eAd1
je	být	k5eAaImIp3nS
osmým	osmý	k4xOgNnSc7
nejrušnějším	rušný	k2eAgNnSc7d3
letištěm	letiště	k1gNnSc7
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Letiště	letiště	k1gNnSc1
je	být	k5eAaImIp3nS
uzlem	uzel	k1gInSc7
společností	společnost	k1gFnPc2
Calm	Calm	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
Cargojet	Cargojet	k1gMnSc1
<g/>
,	,	kIx,
Kivalliq	Kivalliq	k1gMnSc1
Air	Air	k1gMnSc1
<g/>
,	,	kIx,
Perimeter	Perimeter	k1gMnSc1
Airlines	Airlines	k1gMnSc1
Purolator	Purolator	k1gMnSc1
a	a	k8xC
jedno	jeden	k4xCgNnSc1
z	z	k7c2
důležitých	důležitý	k2eAgNnPc2d1
letišť	letiště	k1gNnPc2
pro	pro	k7c4
Jazz	jazz	k1gInSc4
and	and	k?
WestJet	WestJet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
také	také	k9
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
základen	základna	k1gFnPc2
kanadské	kanadský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
Canadian	Canadian	k1gMnSc1
Forces	Forces	k1gInSc1
a	a	k8xC
nejvýznamnějším	významný	k2eAgNnSc7d3
letištěm	letiště	k1gNnSc7
Manitoby	Manitoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc1
nese	nést	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
pojmenováno	pojmenovat	k5eAaPmNgNnS
po	po	k7c6
významném	významný	k2eAgNnSc6d1
průkopníkovi	průkopník	k1gMnSc3
komerčního	komerční	k2eAgNnSc2d1
letectví	letectví	k1gNnSc2
v	v	k7c6
Kanadě	Kanada	k1gFnSc6
<g/>
,	,	kIx,
Jamesi	Jamese	k1gFnSc4
Armstrongovi	Armstrong	k1gMnSc3
Richardsonovi	Richardson	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1
letiště	letiště	k1gNnSc2
</s>
<s>
Realizace	realizace	k1gFnSc1
významných	významný	k2eAgFnPc2d1
investic	investice	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
otevření	otevření	k1gNnSc2
nového	nový	k2eAgInSc2d1
terminálu	terminál	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terminál	terminál	k1gInSc1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
známým	známý	k1gMnSc7
architektem	architekt	k1gMnSc7
Césarem	César	k1gMnSc7
Pellim	Pellima	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provozovatel	provozovatel	k1gMnSc1
letiště	letiště	k1gNnSc4
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podaří	podařit	k5eAaPmIp3nS
přilákat	přilákat	k5eAaPmF
velkokapacitní	velkokapacitní	k2eAgNnPc4d1
letadla	letadlo	k1gNnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Boeing	boeing	k1gInSc4
767-300ER	767-300ER	k4
a	a	k8xC
zprovoznit	zprovoznit	k5eAaPmF
lety	let	k1gInPc4
do	do	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Winnipeg	Winnipeg	k1gMnSc1
James	James	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Richardson	Richardson	k1gMnSc1
International	International	k1gMnSc1
Airport	Airport	k1gInSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Winnipeg	Winnipega	k1gFnPc2
James	James	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
Richardson	Richardson	k1gMnSc1
International	International	k1gMnSc3
Airport	Airport	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2014125101	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
258921409	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2014125101	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
|	|	kIx~
Kanada	Kanada	k1gFnSc1
</s>
