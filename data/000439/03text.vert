<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
stříbrobílé	stříbrobílý	k2eAgFnSc2d1	stříbrobílá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
díky	díky	k7c3	díky
oxidaci	oxidace	k1gFnSc3	oxidace
po	po	k7c6	po
čase	čas	k1gInSc6	čas
přechází	přecházet	k5eAaImIp3nS	přecházet
k	k	k7c3	k
šedé	šedý	k2eAgFnSc3d1	šedá
barvě	barva	k1gFnSc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
kovy	kov	k1gInPc7	kov
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
aktinoidů	aktinoid	k1gInPc2	aktinoid
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
Martin	Martin	k1gMnSc1	Martin
Heinrich	Heinrich	k1gMnSc1	Heinrich
Klaproth	Klaproth	k1gMnSc1	Klaproth
a	a	k8xC	a
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
formě	forma	k1gFnSc6	forma
byl	být	k5eAaImAgInS	být
uran	uran	k1gInSc1	uran
izolován	izolován	k2eAgInSc1d1	izolován
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
Eugene-Melchior	Eugene-Melchiora	k1gFnPc2	Eugene-Melchiora
Peligotem	Peligot	k1gInSc7	Peligot
<g/>
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
tehdy	tehdy	k6eAd1	tehdy
nově	nově	k6eAd1	nově
objevené	objevený	k2eAgFnSc2d1	objevená
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
jméno	jméno	k1gNnSc4	jméno
podle	podle	k7c2	podle
boha	bůh	k1gMnSc2	bůh
Urana	Uran	k1gMnSc2	Uran
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Titánů	Titán	k1gMnPc2	Titán
a	a	k8xC	a
první	první	k4xOgMnSc1	první
bůh	bůh	k1gMnSc1	bůh
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
bytosti	bytost	k1gFnSc2	bytost
Gaia	Gaia	k1gMnSc1	Gaia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
prvním	první	k4xOgInSc7	první
prvkem	prvek	k1gInSc7	prvek
pojmenovaným	pojmenovaný	k2eAgInSc7d1	pojmenovaný
podle	podle	k7c2	podle
nově	nova	k1gFnSc3	nova
objevené	objevený	k2eAgFnSc2d1	objevená
planety	planeta	k1gFnSc2	planeta
-	-	kIx~	-
později	pozdě	k6eAd2	pozdě
následovaly	následovat	k5eAaImAgInP	následovat
ještě	ještě	k9	ještě
neptunium	neptunium	k1gNnSc1	neptunium
a	a	k8xC	a
plutonium	plutonium	k1gNnSc1	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
stříbrobílý	stříbrobílý	k2eAgInSc1d1	stříbrobílý
lesklý	lesklý	k2eAgInSc1d1	lesklý
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
pozvolna	pozvolna	k6eAd1	pozvolna
nabíhá	nabíhat	k5eAaImIp3nS	nabíhat
-	-	kIx~	-
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
se	se	k3xPyFc4	se
vrstvou	vrstva	k1gFnSc7	vrstva
oxidů	oxid	k1gInPc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Rozmělněný	rozmělněný	k2eAgMnSc1d1	rozmělněný
na	na	k7c4	na
prášek	prášek	k1gInSc4	prášek
je	být	k5eAaImIp3nS	být
samozápalný	samozápalný	k2eAgMnSc1d1	samozápalný
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
za	za	k7c4	za
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
teploty	teplota	k1gFnPc4	teplota
kovat	kovat	k5eAaImF	kovat
nebo	nebo	k8xC	nebo
válcovat	válcovat	k5eAaImF	válcovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
nejprve	nejprve	k6eAd1	nejprve
křehkým	křehký	k2eAgInSc7d1	křehký
<g/>
,	,	kIx,	,
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
zvyšování	zvyšování	k1gNnSc6	zvyšování
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
však	však	k9	však
plastický	plastický	k2eAgInSc1d1	plastický
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
0,68	[number]	k4	0,68
K	K	kA	K
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
(	(	kIx(	(
<g/>
specifická	specifický	k2eAgFnSc1d1	specifická
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
uranu	uran	k1gInSc2	uran
při	při	k7c6	při
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
19,01	[number]	k4	19,01
g.	g.	k?	g.
<g/>
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
tak	tak	k6eAd1	tak
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejtěžším	těžký	k2eAgInPc3d3	nejtěžší
prvkům	prvek	k1gInPc3	prvek
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
asi	asi	k9	asi
70	[number]	k4	70
%	%	kIx~	%
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
olovo	olovo	k1gNnSc1	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
těžší	těžký	k2eAgInSc1d2	těžší
pouze	pouze	k6eAd1	pouze
osmium	osmium	k1gNnSc4	osmium
(	(	kIx(	(
<g/>
22,57	[number]	k4	22,57
g	g	kA	g
<g/>
×	×	k?	×
<g/>
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
iridium	iridium	k1gNnSc1	iridium
(	(	kIx(	(
<g/>
22,50	[number]	k4	22,50
g	g	kA	g
<g/>
×	×	k?	×
<g/>
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
platina	platina	k1gFnSc1	platina
(	(	kIx(	(
<g/>
21,45	[number]	k4	21,45
g	g	kA	g
<g/>
×	×	k?	×
<g/>
cm	cm	kA	cm
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
či	či	k8xC	či
rhenium	rhenium	k1gNnSc4	rhenium
(	(	kIx(	(
<g/>
20,50	[number]	k4	20,50
g	g	kA	g
<g/>
×	×	k?	×
<g/>
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgFnSc4d2	veliký
hustotu	hustota	k1gFnSc4	hustota
má	mít	k5eAaImIp3nS	mít
wolfram	wolfram	k1gInSc1	wolfram
(	(	kIx(	(
<g/>
19,25	[number]	k4	19,25
g	g	kA	g
<g/>
×	×	k?	×
<g/>
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
zlato	zlato	k1gNnSc4	zlato
(	(	kIx(	(
<g/>
19,30	[number]	k4	19,30
g	g	kA	g
<g/>
×	×	k?	×
<g/>
cm	cm	kA	cm
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
mnohá	mnohé	k1gNnPc4	mnohé
jeho	jeho	k3xOp3gMnPc3	jeho
nejaderná	jaderný	k2eNgNnPc4d1	nejaderné
využití	využití	k1gNnPc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Elektronová	elektronový	k2eAgFnSc1d1	elektronová
konfigurace	konfigurace	k1gFnSc1	konfigurace
atomu	atom	k1gInSc2	atom
<g/>
:	:	kIx,	:
K	k	k7c3	k
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
:	:	kIx,	:
18	[number]	k4	18
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
:	:	kIx,	:
32	[number]	k4	32
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
d	d	k?	d
<g/>
<g />
.	.	kIx.	.
</s>
<s>
10	[number]	k4	10
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
f	f	k?	f
<g/>
3	[number]	k4	3
P	P	kA	P
<g/>
:	:	kIx,	:
6	[number]	k4	6
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
d	d	k?	d
<g/>
1	[number]	k4	1
Q	Q	kA	Q
<g/>
:	:	kIx,	:
7	[number]	k4	7
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
celková	celkový	k2eAgFnSc1d1	celková
konfigurace	konfigurace	k1gFnSc1	konfigurace
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s>
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
2	[number]	k4	2
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
3	[number]	k4	3
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
3	[number]	k4	3
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
3	[number]	k4	3
<g/>
d	d	k?	d
<g/>
10	[number]	k4	10
4	[number]	k4	4
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
4	[number]	k4	4
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
4	[number]	k4	4
<g/>
d	d	k?	d
<g/>
10	[number]	k4	10
5	[number]	k4	5
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
5	[number]	k4	5
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
4	[number]	k4	4
<g/>
f	f	k?	f
<g/>
14	[number]	k4	14
5	[number]	k4	5
<g/>
d	d	k?	d
<g/>
10	[number]	k4	10
6	[number]	k4	6
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
6	[number]	k4	6
<g/>
p	p	k?	p
<g/>
6	[number]	k4	6
5	[number]	k4	5
<g/>
f	f	k?	f
<g/>
3	[number]	k4	3
6	[number]	k4	6
<g/>
d	d	k?	d
<g/>
1	[number]	k4	1
7	[number]	k4	7
<g/>
s	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
se	se	k3xPyFc4	se
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
79	[number]	k4	79
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
používal	používat	k5eAaImAgInS	používat
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
glazur	glazura	k1gFnPc2	glazura
(	(	kIx(	(
<g/>
nálezy	nález	k1gInPc1	nález
poblíž	poblíž	k6eAd1	poblíž
Neapole	Neapol	k1gFnSc2	Neapol
s	s	k7c7	s
1	[number]	k4	1
<g/>
%	%	kIx~	%
výskytem	výskyt	k1gInSc7	výskyt
oxidu	oxid	k1gInSc2	oxid
uranu	uran	k1gInSc2	uran
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
laboratorně	laboratorně	k6eAd1	laboratorně
izolovanou	izolovaný	k2eAgFnSc7d1	izolovaná
sloučeninou	sloučenina	k1gFnSc7	sloučenina
uranu	uran	k1gInSc2	uran
byla	být	k5eAaImAgFnS	být
uranová	uranový	k2eAgFnSc1d1	uranová
žluť	žluť	k1gFnSc1	žluť
1789	[number]	k4	1789
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
lékárníkem	lékárník	k1gMnSc7	lékárník
a	a	k8xC	a
profesorem	profesor	k1gMnSc7	profesor
chemie	chemie	k1gFnSc2	chemie
Martinem	Martin	k1gMnSc7	Martin
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Klaprothem	Klaproth	k1gInSc7	Klaproth
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
objevil	objevit	k5eAaPmAgMnS	objevit
nebo	nebo	k8xC	nebo
spoluobjevil	spoluobjevit	k5eAaPmAgMnS	spoluobjevit
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
-	-	kIx~	-
(	(	kIx(	(
<g/>
zirkonium	zirkonium	k1gNnSc1	zirkonium
<g/>
,	,	kIx,	,
titan	titan	k1gInSc1	titan
<g/>
,	,	kIx,	,
cer	cer	k1gInSc1	cer
a	a	k8xC	a
tellur	tellur	k1gInSc1	tellur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
oznámil	oznámit	k5eAaPmAgInS	oznámit
v	v	k7c6	v
projevu	projev	k1gInSc6	projev
před	před	k7c7	před
Pruskou	pruský	k2eAgFnSc7d1	pruská
akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1789	[number]	k4	1789
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
objevené	objevený	k2eAgFnSc6d1	objevená
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
(	(	kIx(	(
<g/>
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
ovšem	ovšem	k9	ovšem
byl	být	k5eAaImAgInS	být
uranit	uranit	k1gInSc1	uranit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1790	[number]	k4	1790
přejmenován	přejmenovat	k5eAaPmNgMnS	přejmenovat
na	na	k7c4	na
uranium	uranium	k1gNnSc4	uranium
<g/>
.	.	kIx.	.
</s>
<s>
Klaproth	Klaproth	k1gMnSc1	Klaproth
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
rudu	ruda	k1gFnSc4	ruda
z	z	k7c2	z
dolu	dol	k1gInSc2	dol
George	George	k1gInSc1	George
Wagsfort	Wagsfort	k1gInSc1	Wagsfort
ve	v	k7c6	v
Wittingshalu	Wittingshal	k1gInSc6	Wittingshal
u	u	k7c2	u
Johanngeorgstadtu	Johanngeorgstadt	k1gInSc2	Johanngeorgstadt
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Vystavil	vystavit	k5eAaPmAgMnS	vystavit
ji	on	k3xPp3gFnSc4	on
působení	působení	k1gNnSc3	působení
kyseliny	kyselina	k1gFnSc2	kyselina
a	a	k8xC	a
silně	silně	k6eAd1	silně
zahřál	zahřát	k5eAaPmAgInS	zahřát
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
žlutý	žlutý	k2eAgInSc1d1	žlutý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
síran	síran	k1gInSc4	síran
<g/>
,	,	kIx,	,
čistý	čistý	k2eAgInSc4d1	čistý
uran	uran	k1gInSc4	uran
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
Francouzi	Francouz	k1gMnPc1	Francouz
Eugene-Melchior	Eugene-Melchiora	k1gFnPc2	Eugene-Melchiora
Peligotovi	Peligotův	k2eAgMnPc5d1	Peligotův
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
používal	používat	k5eAaImAgMnS	používat
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
glazur	glazura	k1gFnPc2	glazura
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
dodává	dodávat	k5eAaImIp3nS	dodávat
zelenou	zelený	k2eAgFnSc4d1	zelená
nebo	nebo	k8xC	nebo
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
těžen	těžen	k2eAgMnSc1d1	těžen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
a	a	k8xC	a
v	v	k7c6	v
britském	britský	k2eAgNnSc6d1	Britské
Cornwallu	Cornwallo	k1gNnSc6	Cornwallo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
použití	použití	k1gNnSc1	použití
podstatně	podstatně	k6eAd1	podstatně
kleslo	klesnout	k5eAaPmAgNnS	klesnout
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ottova	Ottův	k2eAgInSc2d1	Ottův
slovníku	slovník	k1gInSc2	slovník
naučného	naučný	k2eAgInSc2d1	naučný
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
vytěženo	vytěžit	k5eAaPmNgNnS	vytěžit
17	[number]	k4	17
193	[number]	k4	193
kg	kg	kA	kg
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
zjistil	zjistit	k5eAaPmAgMnS	zjistit
Henri	Henr	k1gMnSc3	Henr
Becquerel	becquerel	k1gInSc4	becquerel
<g/>
,	,	kIx,	,
že	že	k8xS	že
uran	uran	k1gInSc1	uran
je	být	k5eAaImIp3nS	být
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
a	a	k8xC	a
-	-	kIx~	-
pokud	pokud	k8xS	pokud
nepočítáme	počítat	k5eNaImIp1nP	počítat
objev	objev	k1gInSc4	objev
rentgenových	rentgenový	k2eAgInPc2d1	rentgenový
paprsků	paprsek	k1gInPc2	paprsek
krátce	krátce	k6eAd1	krátce
předtím	předtím	k6eAd1	předtím
-	-	kIx~	-
vlastně	vlastně	k9	vlastně
tím	ten	k3xDgNnSc7	ten
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
objevil	objevit	k5eAaPmAgInS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Curie-Skłodowská	Curie-Skłodowský	k2eAgFnSc1d1	Curie-Skłodowský
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Pierrem	Pierr	k1gMnSc7	Pierr
Curie	Curie	k1gMnSc7	Curie
poté	poté	k6eAd1	poté
z	z	k7c2	z
uranové	uranový	k2eAgFnSc2d1	uranová
rudy	ruda	k1gFnSc2	ruda
(	(	kIx(	(
<g/>
jáchymovského	jáchymovský	k2eAgInSc2d1	jáchymovský
smolince	smolinec	k1gInSc2	smolinec
<g/>
)	)	kIx)	)
izolovala	izolovat	k5eAaBmAgFnS	izolovat
2	[number]	k4	2
nové	nový	k2eAgInPc4d1	nový
prvky	prvek	k1gInPc4	prvek
<g/>
:	:	kIx,	:
nejdřív	dříve	k6eAd3	dříve
polonium	polonium	k1gNnSc1	polonium
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
i	i	k9	i
radium	radium	k1gNnSc4	radium
<g/>
.	.	kIx.	.
</s>
<s>
Uranové	uranový	k2eAgFnPc1d1	uranová
rudy	ruda	k1gFnPc1	ruda
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
objev	objev	k1gInSc1	objev
umělých	umělý	k2eAgInPc2d1	umělý
izotopů	izotop	k1gInPc2	izotop
<g/>
)	)	kIx)	)
používány	používat	k5eAaImNgInP	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
radia	radio	k1gNnSc2	radio
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
obsaženého	obsažený	k2eAgNnSc2d1	obsažené
(	(	kIx(	(
<g/>
radia	radio	k1gNnSc2	radio
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
brzo	brzo	k6eAd1	brzo
po	po	k7c6	po
objevu	objev	k1gInSc6	objev
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
malých	malý	k2eAgNnPc6d1	malé
množstvích	množství	k1gNnPc6	množství
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
lékařské	lékařský	k2eAgInPc4d1	lékařský
účely	účel	k1gInPc4	účel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Františka	František	k1gMnSc2	František
Běhounka	běhounek	k1gMnSc2	běhounek
bylo	být	k5eAaImAgNnS	být
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
izolováno	izolovat	k5eAaBmNgNnS	izolovat
jen	jen	k6eAd1	jen
kolem	kolem	k7c2	kolem
1,5	[number]	k4	1,5
kg	kg	kA	kg
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
jaderného	jaderný	k2eAgInSc2d1	jaderný
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
uran	uran	k1gInSc1	uran
využívat	využívat	k5eAaPmF	využívat
až	až	k9	až
během	během	k7c2	během
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
po	po	k7c6	po
<g/>
)	)	kIx)	)
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
umělá	umělý	k2eAgFnSc1d1	umělá
jaderná	jaderný	k2eAgFnSc1d1	jaderná
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Fermiho	Fermi	k1gMnSc2	Fermi
reakce	reakce	k1gFnSc2	reakce
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
spuštěna	spuštěn	k2eAgFnSc1d1	spuštěna
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1942	[number]	k4	1942
italským	italský	k2eAgMnSc7d1	italský
fyzikem	fyzik	k1gMnSc7	fyzik
E.	E.	kA	E.
Fermim	Fermim	k1gInSc1	Fermim
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
Chicagské	chicagský	k2eAgFnSc2d1	Chicagská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
CP-	CP-	k1gFnSc1	CP-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jaderného	jaderný	k2eAgInSc2d1	jaderný
reaktoru	reaktor	k1gInSc2	reaktor
(	(	kIx(	(
<g/>
EBR-	EBR-	k1gFnSc1	EBR-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
proud	proud	k1gInSc1	proud
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovoznit	k5eAaPmNgFnS	zprovoznit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
v	v	k7c6	v
Obinsku	Obinsko	k1gNnSc6	Obinsko
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
uran	uran	k1gInSc1	uran
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
rudách	ruda	k1gFnPc6	ruda
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jen	jen	k9	jen
v	v	k7c6	v
nízkých	nízký	k2eAgFnPc6d1	nízká
koncentracích	koncentrace	k1gFnPc6	koncentrace
0,04	[number]	k4	0,04
-	-	kIx~	-
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
jako	jako	k9	jako
směs	směs	k1gFnSc1	směs
izotopů	izotop	k1gInPc2	izotop
-	-	kIx~	-
238U	[number]	k4	238U
(	(	kIx(	(
<g/>
99,276	[number]	k4	99,276
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
235U	[number]	k4	235U
(	(	kIx(	(
<g/>
0,718	[number]	k4	0,718
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
jen	jen	k9	jen
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
234U	[number]	k4	234U
(	(	kIx(	(
<g/>
0,004	[number]	k4	0,004
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uměle	uměle	k6eAd1	uměle
bylo	být	k5eAaImAgNnS	být
syntetizováno	syntetizovat	k5eAaImNgNnS	syntetizovat
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
izotopů	izotop	k1gInPc2	izotop
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytujících	vyskytující	k2eAgFnPc2d1	vyskytující
tak	tak	k8xS	tak
byla	být	k5eAaImAgNnP	být
dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
objevena	objeven	k2eAgFnSc1d1	objevena
celá	celý	k2eAgFnSc1d1	celá
souvislá	souvislý	k2eAgFnSc1d1	souvislá
řada	řada	k1gFnSc1	řada
s	s	k7c7	s
nukleonovými	nukleonův	k2eAgNnPc7d1	nukleonův
čísly	číslo	k1gNnPc7	číslo
217	[number]	k4	217
až	až	k8xS	až
242	[number]	k4	242
<g/>
:	:	kIx,	:
Nejstarší	starý	k2eAgNnPc1d3	nejstarší
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgNnPc1d3	nejznámější
a	a	k8xC	a
patrně	patrně	k6eAd1	patrně
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
uraninit	uraninit	k1gInSc1	uraninit
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
ledvinitá	ledvinitý	k2eAgFnSc1d1	ledvinitá
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
smolinec	smolinec	k1gInSc1	smolinec
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
nasturan	nasturan	k1gInSc4	nasturan
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
UO2	UO2	k1gFnSc6	UO2
s	s	k7c7	s
příměsemi	příměse	k1gFnPc7	příměse
oxidů	oxid	k1gInPc2	oxid
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
thoria	thorium	k1gNnSc2	thorium
a	a	k8xC	a
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
druhou	druhý	k4xOgFnSc7	druhý
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
mikroskopický	mikroskopický	k2eAgMnSc1d1	mikroskopický
koffinit	koffinit	k5eAaPmF	koffinit
U	u	k7c2	u
<g/>
(	(	kIx(	(
<g/>
SiO	SiO	k1gFnSc2	SiO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
1	[number]	k4	1
<g/>
-x	-x	k?	-x
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
často	často	k6eAd1	často
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
uraninit	uraninit	k1gInSc1	uraninit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
důležité	důležitý	k2eAgFnPc1d1	důležitá
rudy	ruda	k1gFnPc1	ruda
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
rudy	ruda	k1gFnSc2	ruda
vanadu	vanad	k1gInSc2	vanad
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
carnotit	carnotit	k1gInSc4	carnotit
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
a	a	k8xC	a
ťujamunit	ťujamunit	k1gInSc1	ťujamunit
<g />
.	.	kIx.	.
</s>
<s>
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
xH	xH	k?	xH
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
Dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
ulrichity	ulrichit	k1gInPc1	ulrichit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
minerály	minerál	k1gInPc4	minerál
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
poměrem	poměr	k1gInSc7	poměr
oxidu	oxid	k1gInSc2	oxid
uraničitého	uraničitý	k2eAgInSc2d1	uraničitý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uraničito-uranového	uraničitoranový	k2eAgInSc2d1	uraničito-uranový
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
U	u	k7c2	u
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc4d1	významný
minerály	minerál	k1gInPc4	minerál
uranu	uran	k1gInSc2	uran
jsou	být	k5eAaImIp3nP	být
bröggerit	bröggerit	k1gInSc4	bröggerit
<g/>
,	,	kIx,	,
cleveit	cleveit	k1gInSc4	cleveit
<g/>
,	,	kIx,	,
nivenit	nivenit	k5eAaPmF	nivenit
(	(	kIx(	(
<g/>
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
)	)	kIx)	)
či	či	k8xC	či
zippeit	zippeit	k1gInSc1	zippeit
(	(	kIx(	(
<g/>
skupina	skupina	k1gFnSc1	skupina
minerálů	minerál	k1gInPc2	minerál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autunit	autunit	k1gInSc1	autunit
a	a	k8xC	a
johannit	johannit	k1gInSc1	johannit
<g/>
.	.	kIx.	.
</s>
<s>
Uranové	uranový	k2eAgFnPc1d1	uranová
rudy	ruda	k1gFnPc1	ruda
se	se	k3xPyFc4	se
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Nigeru	Niger	k1gInSc2	Niger
<g/>
,	,	kIx,	,
Nigérii	Nigérie	k1gFnSc6	Nigérie
<g/>
,	,	kIx,	,
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
Zairu	Zairo	k1gNnSc6	Zairo
<g/>
,	,	kIx,	,
Namibii	Namibie	k1gFnSc6	Namibie
<g/>
,	,	kIx,	,
Gabonu	Gabon	k1gInSc6	Gabon
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Uzbekistánu	Uzbekistán	k1gInSc6	Uzbekistán
<g/>
,	,	kIx,	,
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
a	a	k8xC	a
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
významné	významný	k2eAgNnSc1d1	významné
ložisko	ložisko	k1gNnSc1	ložisko
bylo	být	k5eAaImAgNnS	být
nedávno	nedávno	k6eAd1	nedávno
(	(	kIx(	(
<g/>
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
uran	uran	k1gInSc1	uran
těží	těžet	k5eAaImIp3nS	těžet
nebo	nebo	k8xC	nebo
těžil	těžit	k5eAaImAgMnS	těžit
v	v	k7c6	v
Sasku	Sasko	k1gNnSc6	Sasko
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
Cornwallu	Cornwall	k1gInSc6	Cornwall
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
dál	daleko	k6eAd2	daleko
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Probíhají	probíhat	k5eAaImIp3nP	probíhat
jednání	jednání	k1gNnSc4	jednání
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
těžbě	těžba	k1gFnSc6	těžba
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
však	však	k9	však
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
z	z	k7c2	z
celosvětového	celosvětový	k2eAgNnSc2d1	celosvětové
hlediska	hledisko	k1gNnSc2	hledisko
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
významná	významný	k2eAgFnSc1d1	významná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ve	v	k7c6	v
světe	svět	k1gInSc5	svět
existují	existovat	k5eAaImIp3nP	existovat
hospodářsky	hospodářsky	k6eAd1	hospodářsky
využitelné	využitelný	k2eAgFnPc1d1	využitelná
zásoby	zásoba	k1gFnPc1	zásoba
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
mezi	mezi	k7c7	mezi
1,73	[number]	k4	1,73
až	až	k9	až
9,4	[number]	k4	9,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
při	při	k7c6	při
připočtení	připočtení	k1gNnSc6	připočtení
zásob	zásoba	k1gFnPc2	zásoba
předpokládaných	předpokládaný	k2eAgMnPc2d1	předpokládaný
činí	činit	k5eAaImIp3nS	činit
celkové	celkový	k2eAgFnPc4d1	celková
zásoby	zásoba	k1gFnPc4	zásoba
16,9	[number]	k4	16,9
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
při	při	k7c6	při
současné	současný	k2eAgFnSc6d1	současná
spotřebě	spotřeba	k1gFnSc6	spotřeba
by	by	kYmCp3nS	by
tak	tak	k9	tak
zásoby	zásoba	k1gFnPc1	zásoba
vystačily	vystačit	k5eAaBmAgFnP	vystačit
na	na	k7c4	na
260	[number]	k4	260
let	léto	k1gNnPc2	léto
<g/>
;	;	kIx,	;
nedostatek	nedostatek	k1gInSc1	nedostatek
uranu	uran	k1gInSc2	uran
se	se	k3xPyFc4	se
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
ani	ani	k8xC	ani
v	v	k7c6	v
případě	případ	k1gInSc6	případ
masivního	masivní	k2eAgInSc2d1	masivní
rozvoje	rozvoj	k1gInSc2	rozvoj
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energetiky	energetika	k1gFnSc2	energetika
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
těžitelných	těžitelný	k2eAgFnPc2d1	těžitelná
zásob	zásoba	k1gFnPc2	zásoba
uranu	uran	k1gInSc2	uran
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
20	[number]	k4	20
Mt	Mt	k1gFnPc2	Mt
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
uranem	uran	k1gInSc7	uran
z	z	k7c2	z
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
z	z	k7c2	z
thoria	thorium	k1gNnSc2	thorium
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
těžitelné	těžitelný	k2eAgFnPc1d1	těžitelná
zásoby	zásoba	k1gFnPc1	zásoba
dokonce	dokonce	k9	dokonce
nejméně	málo	k6eAd3	málo
160	[number]	k4	160
Mt	Mt	k1gFnPc2	Mt
<g/>
.	.	kIx.	.
</s>
<s>
Značné	značný	k2eAgFnPc1d1	značná
rezervy	rezerva	k1gFnPc1	rezerva
navíc	navíc	k6eAd1	navíc
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
recyklaci	recyklace	k1gFnSc6	recyklace
vyhořelého	vyhořelý	k2eAgNnSc2d1	vyhořelé
paliva	palivo	k1gNnSc2	palivo
(	(	kIx(	(
<g/>
zásoby	zásoba	k1gFnPc1	zásoba
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
recyklace	recyklace	k1gFnSc2	recyklace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nevyplatí	vyplatit	k5eNaPmIp3nS	vyplatit
<g/>
,	,	kIx,	,
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
o	o	k7c4	o
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
rychlých	rychlý	k2eAgInPc2d1	rychlý
množivých	množivý	k2eAgInPc2d1	množivý
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
využití	využití	k1gNnSc2	využití
rychlých	rychlý	k2eAgInPc2d1	rychlý
množivých	množivý	k2eAgInPc2d1	množivý
reaktorů	reaktor	k1gInPc2	reaktor
by	by	kYmCp3nP	by
zásoby	zásoba	k1gFnPc4	zásoba
vystačily	vystačit	k5eAaBmAgFnP	vystačit
na	na	k7c6	na
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
při	při	k7c6	při
těžbě	těžba	k1gFnSc6	těžba
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
jeho	jeho	k3xOp3gNnPc2	jeho
nalezišť	naleziště	k1gNnPc2	naleziště
ho	on	k3xPp3gInSc4	on
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
těžit	těžit	k5eAaImF	těžit
současně	současně	k6eAd1	současně
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
surovinami	surovina	k1gFnPc7	surovina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
australský	australský	k2eAgInSc1d1	australský
megadůl	megadůl	k1gInSc1	megadůl
Olympic	Olympice	k1gFnPc2	Olympice
Dam	dáma	k1gFnPc2	dáma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgFnSc6d1	velká
koncentraci	koncentrace	k1gFnSc6	koncentrace
kolem	kolem	k7c2	kolem
3,3	[number]	k4	3,3
mikrogramů	mikrogram	k1gInPc2	mikrogram
na	na	k7c4	na
litr	litr	k1gInSc4	litr
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
jsou	být	k5eAaImIp3nP	být
celkově	celkově	k6eAd1	celkově
obsaženy	obsažen	k2eAgFnPc4d1	obsažena
4	[number]	k4	4
miliardy	miliarda	k4xCgFnPc4	miliarda
tun	tuna	k1gFnPc2	tuna
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
však	však	k9	však
jeho	jeho	k3xOp3gNnSc4	jeho
získávání	získávání	k1gNnSc4	získávání
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
není	být	k5eNaImIp3nS	být
efektivní	efektivní	k2eAgFnSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sladkovodních	sladkovodní	k2eAgFnPc6d1	sladkovodní
vodách	voda	k1gFnPc6	voda
je	být	k5eAaImIp3nS	být
obsah	obsah	k1gInSc1	obsah
uranu	uran	k1gInSc2	uran
velmi	velmi	k6eAd1	velmi
proměnný	proměnný	k2eAgInSc1d1	proměnný
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
uhlí	uhlí	k1gNnSc6	uhlí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
tepelné	tepelný	k2eAgFnPc4d1	tepelná
elektrárny	elektrárna	k1gFnPc4	elektrárna
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
celkově	celkově	k6eAd1	celkově
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
než	než	k8xS	než
elektrárny	elektrárna	k1gFnSc2	elektrárna
jaderné	jaderný	k2eAgFnSc2d1	jaderná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
by	by	kYmCp3nS	by
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
získávána	získáván	k2eAgFnSc1d1	získávána
podstatná	podstatný	k2eAgFnSc1d1	podstatná
část	část	k1gFnSc1	část
světové	světový	k2eAgFnSc2d1	světová
spotřeby	spotřeba	k1gFnSc2	spotřeba
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
významná	významný	k2eAgFnSc1d1	významná
těžba	těžba	k1gFnSc1	těžba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zejména	zejména	k9	zejména
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
(	(	kIx(	(
<g/>
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zdaleka	zdaleka	k6eAd1	zdaleka
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
zdroj	zdroj	k1gInSc1	zdroj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Horním	horní	k2eAgInSc6d1	horní
Slavkově	Slavkov	k1gInSc6	Slavkov
<g/>
,	,	kIx,	,
v	v	k7c6	v
Příbrami	Příbram	k1gFnSc6	Příbram
a	a	k8xC	a
v	v	k7c6	v
křídových	křídový	k2eAgInPc6d1	křídový
pískovcích	pískovec	k1gInPc6	pískovec
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Stráže	stráž	k1gFnSc2	stráž
pod	pod	k7c7	pod
Ralskem	Ralsek	k1gInSc7	Ralsek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
uranová	uranový	k2eAgFnSc1d1	uranová
ruda	ruda	k1gFnSc1	ruda
těží	těžet	k5eAaImIp3nS	těžet
poblíž	poblíž	k6eAd1	poblíž
Dolní	dolní	k2eAgFnPc1d1	dolní
Rožínky	Rožínka	k1gFnPc1	Rožínka
u	u	k7c2	u
Žďáru	Žďár	k1gInSc2	Žďár
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
probíhající	probíhající	k2eAgFnSc4d1	probíhající
těžbu	těžba	k1gFnSc4	těžba
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2007	[number]	k4	2007
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
neurčitou	určitý	k2eNgFnSc4d1	neurčitá
<g/>
,	,	kIx,	,
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
výhodnosti	výhodnost	k1gFnSc2	výhodnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
z	z	k7c2	z
února	únor	k1gInSc2	únor
2015	[number]	k4	2015
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
těžby	těžba	k1gFnSc2	těžba
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
firma	firma	k1gFnSc1	firma
Uran	Uran	k1gInSc1	Uran
Limited	limited	k2eAgMnPc2d1	limited
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
práv	právo	k1gNnPc2	právo
na	na	k7c4	na
průzkum	průzkum	k1gInSc4	průzkum
uranových	uranový	k2eAgNnPc2d1	uranové
ložisek	ložisko	k1gNnPc2	ložisko
na	na	k7c6	na
Jihlavsku	Jihlavsko	k1gNnSc6	Jihlavsko
Spor	spora	k1gFnPc2	spora
o	o	k7c4	o
případnou	případný	k2eAgFnSc4d1	případná
těžbu	těžba	k1gFnSc4	těžba
uranu	uran	k1gInSc2	uran
probíhá	probíhat	k5eAaImIp3nS	probíhat
také	také	k9	také
v	v	k7c6	v
Podještědí	Podještědí	k1gNnSc6	Podještědí
na	na	k7c6	na
Liberecku	Liberecko	k1gNnSc6	Liberecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
těžbu	těžba	k1gFnSc4	těžba
v	v	k7c6	v
ložisku	ložisko	k1gNnSc6	ložisko
Osečná-Kotel	Osečná-Kotela	k1gFnPc2	Osečná-Kotela
má	mít	k5eAaImIp3nS	mít
zájem	zájem	k1gInSc1	zájem
společnost	společnost	k1gFnSc4	společnost
Urania	uranium	k1gNnSc2	uranium
Mining	Mining	k1gInSc1	Mining
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
100	[number]	k4	100
<g/>
%	%	kIx~	%
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
australská	australský	k2eAgFnSc1d1	australská
firma	firma	k1gFnSc1	firma
Discovery	Discovera	k1gFnSc2	Discovera
Minerals	Mineralsa	k1gFnPc2	Mineralsa
Pty	Pty	k1gFnSc2	Pty
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
průzkumné	průzkumný	k2eAgInPc4d1	průzkumný
vrty	vrt	k1gInPc4	vrt
na	na	k7c6	na
ložiscích	ložisko	k1gNnPc6	ložisko
Osečná-Kotel	Osečná-Kotel	k1gInSc1	Osečná-Kotel
a	a	k8xC	a
Ploužnice	Ploužnice	k1gFnSc1	Ploužnice
<g/>
,	,	kIx,	,
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
sice	sice	k8xC	sice
žádost	žádost	k1gFnSc4	žádost
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
zamítlo	zamítnout	k5eAaPmAgNnS	zamítnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
firma	firma	k1gFnSc1	firma
Urania	uranium	k1gNnSc2	uranium
Limited	limited	k2eAgFnSc1d1	limited
se	se	k3xPyFc4	se
odvolala	odvolat	k5eAaPmAgFnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
začalo	začít	k5eAaPmAgNnS	začít
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
opět	opět	k6eAd1	opět
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
těžbě	těžba	k1gFnSc6	těžba
uranu	uran	k1gInSc2	uran
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
paliva	palivo	k1gNnSc2	palivo
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
především	především	k9	především
nedostatkem	nedostatek	k1gInSc7	nedostatek
zkušeností	zkušenost	k1gFnPc2	zkušenost
se	s	k7c7	s
sanací	sanace	k1gFnSc7	sanace
uzavřených	uzavřený	k2eAgInPc2d1	uzavřený
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ložiska	ložisko	k1gNnPc1	ložisko
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
chráněné	chráněný	k2eAgFnSc6d1	chráněná
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Hlubinná	hlubinný	k2eAgFnSc1d1	hlubinná
těžba	těžba	k1gFnSc1	těžba
uranu	uran	k1gInSc2	uran
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Stráže	stráž	k1gFnSc2	stráž
pod	pod	k7c7	pod
Ralskem	Ralsek	k1gInSc7	Ralsek
prováděla	provádět	k5eAaImAgFnS	provádět
po	po	k7c4	po
léta	léto	k1gNnPc4	léto
za	za	k7c2	za
pomocí	pomoc	k1gFnPc2	pomoc
vhánění	vhánění	k1gNnSc2	vhánění
silných	silný	k2eAgFnPc2d1	silná
kyselin	kyselina	k1gFnPc2	kyselina
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
značnému	značný	k2eAgNnSc3d1	značné
zamoření	zamoření	k1gNnSc3	zamoření
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
těžby	těžba	k1gFnSc2	těžba
zamořená	zamořený	k2eAgFnSc1d1	zamořená
podzemní	podzemní	k2eAgFnSc1d1	podzemní
voda	voda	k1gFnSc1	voda
odčerpává	odčerpávat	k5eAaImIp3nS	odčerpávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
chemická	chemický	k2eAgFnSc1d1	chemická
kontaminace	kontaminace	k1gFnSc1	kontaminace
nešířila	šířit	k5eNaImAgFnS	šířit
do	do	k7c2	do
přiléhající	přiléhající	k2eAgFnSc2d1	přiléhající
zásobárny	zásobárna	k1gFnSc2	zásobárna
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
zde	zde	k6eAd1	zde
v	v	k7c6	v
současností	současnost	k1gFnPc2	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
dobíhá	dobíhat	k5eAaImIp3nS	dobíhat
a	a	k8xC	a
získávání	získávání	k1gNnSc1	získávání
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
produktem	produkt	k1gInSc7	produkt
odstraňování	odstraňování	k1gNnSc2	odstraňování
starých	starý	k2eAgFnPc2d1	stará
škod	škoda	k1gFnPc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c6	na
sanaci	sanace	k1gFnSc6	sanace
zamoření	zamoření	k1gNnSc1	zamoření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
způsobila	způsobit	k5eAaPmAgFnS	způsobit
těžba	těžba	k1gFnSc1	těžba
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
50	[number]	k4	50
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bývalém	bývalý	k2eAgNnSc6d1	bývalé
Československu	Československo	k1gNnSc6	Československo
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zpracovávala	zpracovávat	k5eAaImAgFnS	zpracovávat
uranová	uranový	k2eAgFnSc1d1	uranová
ruda	ruda	k1gFnSc1	ruda
v	v	k7c6	v
mydlovarském	mydlovarský	k2eAgInSc6d1	mydlovarský
podniku	podnik	k1gInSc6	podnik
MAPE	MAPE	kA	MAPE
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
zpracovny	zpracovna	k1gFnSc2	zpracovna
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
vážných	vážný	k2eAgFnPc2d1	vážná
ekologických	ekologický	k2eAgFnPc2d1	ekologická
zátěží	zátěž	k1gFnPc2	zátěž
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Uranová	uranový	k2eAgNnPc4d1	uranové
ruda	rudo	k1gNnPc4	rudo
obsahující	obsahující	k2eAgInSc4d1	obsahující
smolinec	smolinec	k1gInSc4	smolinec
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vylouží	vyloužit	k5eAaPmIp3nS	vyloužit
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
<g/>
,	,	kIx,	,
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
nebo	nebo	k8xC	nebo
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roztoku	roztok	k1gInSc3	roztok
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
k	k	k7c3	k
vysrážení	vysrážení	k1gNnSc3	vysrážení
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
manganu	mangan	k1gInSc2	mangan
přidá	přidat	k5eAaPmIp3nS	přidat
přebytek	přebytek	k1gInSc1	přebytek
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k8xS	co
<g/>
3	[number]	k4	3
a	a	k8xC	a
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
rozpustný	rozpustný	k2eAgInSc4d1	rozpustný
uhličitan	uhličitan	k1gInSc4	uhličitan
uranylo-sodný	uranyloodný	k2eAgInSc4d1	uranylo-sodný
se	se	k3xPyFc4	se
rozloží	rozložit	k5eAaPmIp3nS	rozložit
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
a	a	k8xC	a
uran	uran	k1gInSc1	uran
se	se	k3xPyFc4	se
vyloučí	vyloučit	k5eAaPmIp3nS	vyloučit
ze	z	k7c2	z
získaného	získaný	k2eAgInSc2d1	získaný
roztoku	roztok	k1gInSc2	roztok
soli	sůl	k1gFnSc2	sůl
uranylu	uranyl	k1gInSc2	uranyl
zaváděním	zavádění	k1gNnSc7	zavádění
amoniaku	amoniak	k1gInSc2	amoniak
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
žíháním	žíhání	k1gNnSc7	žíhání
převede	převést	k5eAaPmIp3nS	převést
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
U	u	k7c2	u
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
rudy	ruda	k1gFnPc4	ruda
obsahující	obsahující	k2eAgFnSc4d1	obsahující
měď	měď	k1gFnSc4	měď
a	a	k8xC	a
arsen	arsen	k1gInSc4	arsen
<g/>
,	,	kIx,	,
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
se	se	k3xPyFc4	se
uran	uran	k1gInSc1	uran
z	z	k7c2	z
uhličitanového	uhličitanový	k2eAgInSc2d1	uhličitanový
roztoku	roztok	k1gInSc2	roztok
po	po	k7c4	po
okyselení	okyselení	k1gNnSc4	okyselení
kyselinou	kyselina	k1gFnSc7	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1	chlorovodíková
obvykle	obvykle	k6eAd1	obvykle
nejdřív	dříve	k6eAd3	dříve
přidáním	přidání	k1gNnSc7	přidání
NaOH	NaOH	k1gFnPc2	NaOH
jako	jako	k9	jako
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
a	a	k8xC	a
do	do	k7c2	do
roztoku	roztok	k1gInSc2	roztok
se	se	k3xPyFc4	se
zavádí	zavádět	k5eAaImIp3nS	zavádět
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
srazí	srazit	k5eAaPmIp3nS	srazit
CuS	CuS	k1gFnSc1	CuS
a	a	k8xC	a
As	as	k1gNnSc1	as
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
filtrátu	filtrát	k1gInSc2	filtrát
zbaveného	zbavený	k2eAgInSc2d1	zbavený
varem	var	k1gInSc7	var
sirovodíku	sirovodík	k1gInSc2	sirovodík
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
při	při	k7c6	při
přidání	přidání	k1gNnSc6	přidání
amoniaku	amoniak	k1gInSc2	amoniak
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
uran	uran	k1gInSc1	uran
jako	jako	k9	jako
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
z	z	k7c2	z
carnotitu	carnotit	k1gInSc2	carnotit
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
použito	použít	k5eAaPmNgNnS	použít
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dělení	dělení	k1gNnSc4	dělení
vanadu	vanad	k1gInSc2	vanad
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
od	od	k7c2	od
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
rud	ruda	k1gFnPc2	ruda
na	na	k7c4	na
uran	uran	k1gInSc4	uran
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zahřívání	zahřívání	k1gNnSc4	zahřívání
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
chloru	chlor	k1gInSc2	chlor
nebo	nebo	k8xC	nebo
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
chloračními	chlorační	k2eAgNnPc7d1	chlorační
činidly	činidlo	k1gNnPc7	činidlo
SCl	SCl	k1gFnSc2	SCl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
SOCl	SOCl	k1gInSc1	SOCl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
COCl	COCl	k1gInSc1	COCl
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
CCl	CCl	k1gFnSc1	CCl
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
uran	uran	k1gInSc1	uran
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
jako	jako	k9	jako
UCl	UCl	k1gFnSc1	UCl
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
zjistil	zjistit	k5eAaPmAgMnS	zjistit
už	už	k6eAd1	už
Moissan	Moissan	k1gMnSc1	Moissan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
redukce	redukce	k1gFnSc1	redukce
oxidu	oxid	k1gInSc2	oxid
U3O8	U3O8	k1gFnSc2	U3O8
na	na	k7c4	na
kov	kov	k1gInSc4	kov
provádět	provádět	k5eAaImF	provádět
zahříváním	zahřívání	k1gNnSc7	zahřívání
s	s	k7c7	s
uhlím	uhlí	k1gNnSc7	uhlí
v	v	k7c6	v
elektrické	elektrický	k2eAgFnSc6d1	elektrická
obloukové	obloukový	k2eAgFnSc6d1	oblouková
peci	pec	k1gFnSc6	pec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kov	kov	k1gInSc1	kov
připravený	připravený	k2eAgMnSc1d1	připravený
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
karbid	karbid	k1gInSc1	karbid
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovu	kov	k1gInSc2	kov
je	být	k5eAaImIp3nS	být
ztížena	ztížit	k5eAaPmNgFnS	ztížit
nejen	nejen	k6eAd1	nejen
sklonem	sklon	k1gInSc7	sklon
uranu	uran	k1gInSc2	uran
tvořit	tvořit	k5eAaImF	tvořit
karbidy	karbid	k1gInPc1	karbid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jeho	jeho	k3xOp3gFnSc7	jeho
velkou	velký	k2eAgFnSc7d1	velká
afinitou	afinita	k1gFnSc7	afinita
ke	k	k7c3	k
kyslíku	kyslík	k1gInSc3	kyslík
a	a	k8xC	a
dusíku	dusík	k1gInSc3	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Redukce	redukce	k1gFnSc1	redukce
UCl	UCl	k1gFnSc2	UCl
<g/>
4	[number]	k4	4
kovovým	kovový	k2eAgInSc7d1	kovový
vápníkem	vápník	k1gInSc7	vápník
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
:	:	kIx,	:
UCl	UCl	k1gFnSc1	UCl
<g/>
4	[number]	k4	4
+	+	kIx~	+
2	[number]	k4	2
Ca	ca	kA	ca
→	→	k?	→
U	u	k7c2	u
+	+	kIx~	+
2	[number]	k4	2
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
Redukce	redukce	k1gFnSc2	redukce
UCl	UCl	k1gFnSc2	UCl
<g/>
4	[number]	k4	4
kovovým	kovový	k2eAgInSc7d1	kovový
draslíkem	draslík	k1gInSc7	draslík
<g/>
:	:	kIx,	:
UCl	UCl	k1gFnSc1	UCl
<g/>
4	[number]	k4	4
+	+	kIx~	+
4	[number]	k4	4
K	k	k7c3	k
→	→	k?	→
U	U	kA	U
+	+	kIx~	+
4	[number]	k4	4
KCl	KCl	k1gFnPc2	KCl
Běžný	běžný	k2eAgInSc1d1	běžný
způsob	způsob	k1gInSc1	způsob
přípravy	příprava	k1gFnSc2	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovového	kovový	k2eAgInSc2d1	kovový
uranu	uran	k1gInSc2	uran
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
atomových	atomový	k2eAgInPc6d1	atomový
reaktorech	reaktor	k1gInPc6	reaktor
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
fluoridu	fluorid	k1gInSc2	fluorid
uraničitého	uraničitý	k2eAgInSc2d1	uraničitý
kovovým	kovový	k2eAgInSc7d1	kovový
vápníkem	vápník	k1gInSc7	vápník
<g/>
:	:	kIx,	:
UF4	UF4	k1gFnSc1	UF4
+	+	kIx~	+
2	[number]	k4	2
Ca	ca	kA	ca
→	→	k?	→
U	u	k7c2	u
+	+	kIx~	+
2	[number]	k4	2
CaF	CaF	k1gFnPc2	CaF
<g/>
2	[number]	k4	2
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obohacený	obohacený	k2eAgInSc4d1	obohacený
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jaderné	jaderný	k2eAgNnSc4d1	jaderné
využití	využití	k1gNnSc4	využití
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc4d1	vhodný
především	především	k9	především
izotop	izotop	k1gInSc4	izotop
235U	[number]	k4	235U
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
nízkým	nízký	k2eAgNnSc7d1	nízké
zastoupením	zastoupení	k1gNnSc7	zastoupení
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
0,73	[number]	k4	0,73
%	%	kIx~	%
<g/>
)	)	kIx)	)
v	v	k7c6	v
přírodním	přírodní	k2eAgInSc6d1	přírodní
uranu	uran	k1gInSc6	uran
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
paliva	palivo	k1gNnSc2	palivo
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgInPc4d1	jaderný
reaktory	reaktor	k1gInPc4	reaktor
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
uran	uran	k1gInSc4	uran
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
kolem	kolem	k7c2	kolem
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
%	%	kIx~	%
235	[number]	k4	235
<g/>
U.	U.	kA	U.
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
jaderné	jaderný	k2eAgFnSc2d1	jaderná
zbraně	zbraň	k1gFnSc2	zbraň
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
obohacení	obohacení	k1gNnSc4	obohacení
naprosto	naprosto	k6eAd1	naprosto
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
nutný	nutný	k2eAgInSc1d1	nutný
obsah	obsah	k1gInSc1	obsah
235U	[number]	k4	235U
zde	zde	k6eAd1	zde
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
90	[number]	k4	90
%	%	kIx~	%
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
však	však	k9	však
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
využívá	využívat	k5eAaPmIp3nS	využívat
uran	uran	k1gInSc1	uran
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
vyšším	vysoký	k2eAgInSc7d2	vyšší
stupněm	stupeň	k1gInSc7	stupeň
obohacení	obohacení	k1gNnSc1	obohacení
<g/>
.	.	kIx.	.
</s>
<s>
Oddělení	oddělení	k1gNnSc1	oddělení
hlavních	hlavní	k2eAgInPc2d1	hlavní
izotopů	izotop	k1gInPc2	izotop
uranu	uran	k1gInSc2	uran
-	-	kIx~	-
238U	[number]	k4	238U
a	a	k8xC	a
235U	[number]	k4	235U
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
chemického	chemický	k2eAgNnSc2d1	chemické
chování	chování	k1gNnSc2	chování
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgInPc1	dva
izotopy	izotop	k1gInPc1	izotop
prakticky	prakticky	k6eAd1	prakticky
identické	identický	k2eAgInPc1d1	identický
a	a	k8xC	a
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
odlišnosti	odlišnost	k1gFnPc4	odlišnost
ve	v	k7c6	v
fyzikálních	fyzikální	k2eAgFnPc6d1	fyzikální
vlastnostech	vlastnost	k1gFnPc6	vlastnost
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
oba	dva	k4xCgInPc1	dva
izotopy	izotop	k1gInPc1	izotop
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
dostatečně	dostatečně	k6eAd1	dostatečně
odlišné	odlišný	k2eAgFnPc4d1	odlišná
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	být	k5eAaImIp3nS	být
moderními	moderní	k2eAgFnPc7d1	moderní
technologiemi	technologie	k1gFnPc7	technologie
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
oddělit	oddělit	k5eAaPmF	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgInPc4d1	základní
technologické	technologický	k2eAgInPc4d1	technologický
postupy	postup	k1gInPc4	postup
patří	patřit	k5eAaImIp3nS	patřit
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
separace	separace	k1gFnSc1	separace
<g/>
,	,	kIx,	,
difuze	difuze	k1gFnSc1	difuze
<g/>
,	,	kIx,	,
centrifugální	centrifugální	k2eAgFnSc1d1	centrifugální
separace	separace	k1gFnSc1	separace
a	a	k8xC	a
ionizace	ionizace	k1gFnSc1	ionizace
laserem	laser	k1gInSc7	laser
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
a	a	k8xC	a
technologického	technologický	k2eAgNnSc2d1	Technologické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
především	především	k6eAd1	především
jaderné	jaderný	k2eAgNnSc1d1	jaderné
využití	využití	k1gNnSc1	využití
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnPc1d1	ostatní
možnosti	možnost	k1gFnPc1	možnost
využití	využití	k1gNnSc2	využití
jsou	být	k5eAaImIp3nP	být
spíš	spíš	k9	spíš
vedlejší	vedlejší	k2eAgNnSc4d1	vedlejší
<g/>
.	.	kIx.	.
obohacení	obohacení	k1gNnSc4	obohacení
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
zvýšení	zvýšení	k1gNnSc3	zvýšení
koncentrace	koncentrace	k1gFnSc2	koncentrace
izotopu	izotop	k1gInSc2	izotop
235	[number]	k4	235
<g/>
U	U	kA	U
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
v	v	k7c6	v
jaderných	jaderný	k2eAgInPc6d1	jaderný
reaktorech	reaktor	k1gInPc6	reaktor
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
jaderných	jaderný	k2eAgFnPc2d1	jaderná
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
uranu	uran	k1gInSc2	uran
jako	jako	k8xS	jako
jaderného	jaderný	k2eAgNnSc2d1	jaderné
paliva	palivo	k1gNnSc2	palivo
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zvýšit	zvýšit	k5eAaPmF	zvýšit
koncentraci	koncentrace	k1gFnSc4	koncentrace
izotopu	izotop	k1gInSc2	izotop
235U	[number]	k4	235U
z	z	k7c2	z
0,72	[number]	k4	0,72
%	%	kIx~	%
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
2	[number]	k4	2
-	-	kIx~	-
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
bombě	bomba	k1gFnSc6	bomba
je	být	k5eAaImIp3nS	být
koncentraci	koncentrace	k1gFnSc4	koncentrace
třeba	třeba	k6eAd1	třeba
zvýšit	zvýšit	k5eAaPmF	zvýšit
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
přes	přes	k7c4	přes
95	[number]	k4	95
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jaderné	jaderný	k2eAgNnSc4d1	jaderné
palivo	palivo	k1gNnSc4	palivo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
těžkovodních	těžkovodní	k2eAgInPc6d1	těžkovodní
reaktorech	reaktor	k1gInPc6	reaktor
využít	využít	k5eAaPmF	využít
rovněž	rovněž	k6eAd1	rovněž
přírodní	přírodní	k2eAgInSc4d1	přírodní
uran	uran	k1gInSc4	uran
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
náročnější	náročný	k2eAgNnSc1d2	náročnější
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
možnost	možnost	k1gFnSc1	možnost
zatím	zatím	k6eAd1	zatím
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
příliš	příliš	k6eAd1	příliš
nevyužívá	využívat	k5eNaImIp3nS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
izotopu	izotop	k1gInSc2	izotop
238U	[number]	k4	238U
se	se	k3xPyFc4	se
v	v	k7c6	v
rychlých	rychlý	k2eAgInPc6d1	rychlý
množivých	množivý	k2eAgInPc6d1	množivý
reaktorech	reaktor	k1gInPc6	reaktor
dá	dát	k5eAaPmIp3nS	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
plutonium	plutonium	k1gNnSc4	plutonium
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
štěpitelný	štěpitelný	k2eAgInSc4d1	štěpitelný
izotop	izotop	k1gInSc4	izotop
239	[number]	k4	239
<g/>
Pu	Pu	k1gFnPc2	Pu
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
se	se	k3xPyFc4	se
však	však	k9	však
zatím	zatím	k6eAd1	zatím
příliš	příliš	k6eAd1	příliš
nepoužívá	používat	k5eNaImIp3nS	používat
kvůli	kvůli	k7c3	kvůli
vysokým	vysoký	k2eAgInPc3d1	vysoký
investičním	investiční	k2eAgInPc3d1	investiční
nákladům	náklad	k1gInPc3	náklad
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc3d2	vyšší
technologické	technologický	k2eAgFnSc3d1	technologická
náročnosti	náročnost	k1gFnSc3	náročnost
<g/>
.	.	kIx.	.
</s>
<s>
Štěpitelný	štěpitelný	k2eAgInSc1d1	štěpitelný
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
izotop	izotop	k1gInSc4	izotop
233	[number]	k4	233
<g/>
U	u	k7c2	u
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
lze	lze	k6eAd1	lze
množit	množit	k5eAaImF	množit
z	z	k7c2	z
thoria	thorium	k1gNnSc2	thorium
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
energetického	energetický	k2eAgNnSc2d1	energetické
využívání	využívání	k1gNnSc2	využívání
uranu	uran	k1gInSc2	uran
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
samotného	samotný	k2eAgInSc2d1	samotný
uranu	uran	k1gInSc2	uran
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k9	jen
malý	malý	k2eAgInSc1d1	malý
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
nákladech	náklad	k1gInPc6	náklad
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
(	(	kIx(	(
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
kolem	kolem	k7c2	kolem
17	[number]	k4	17
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
elektřiny	elektřina	k1gFnSc2	elektřina
je	být	k5eAaImIp3nS	být
dána	dán	k2eAgFnSc1d1	dána
především	především	k6eAd1	především
náklady	náklad	k1gInPc7	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
jaderného	jaderný	k2eAgNnSc2d1	jaderné
paliva	palivo	k1gNnSc2	palivo
než	než	k8xS	než
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
relativně	relativně	k6eAd1	relativně
snadné	snadný	k2eAgNnSc1d1	snadné
a	a	k8xC	a
levné	levný	k2eAgNnSc1d1	levné
i	i	k8xC	i
shromažďování	shromažďování	k1gNnSc1	shromažďování
zásob	zásoba	k1gFnPc2	zásoba
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
skladování	skladování	k1gNnSc2	skladování
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vlastnost	vlastnost	k1gFnSc1	vlastnost
velmi	velmi	k6eAd1	velmi
výhodná	výhodný	k2eAgFnSc1d1	výhodná
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
energetické	energetický	k2eAgFnSc2d1	energetická
nezávislosti	nezávislost	k1gFnSc2	nezávislost
státu	stát	k1gInSc2	stát
dokonce	dokonce	k9	dokonce
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
zdroje	zdroj	k1gInPc4	zdroj
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
vyvážejících	vyvážející	k2eAgFnPc2d1	vyvážející
uran	uran	k1gInSc4	uran
politicky	politicky	k6eAd1	politicky
stabilní	stabilní	k2eAgFnSc1d1	stabilní
a	a	k8xC	a
demokratická	demokratický	k2eAgFnSc1d1	demokratická
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
uranu	uran	k1gInSc2	uran
představuje	představovat	k5eAaImIp3nS	představovat
vážný	vážný	k2eAgInSc1d1	vážný
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
povrchové	povrchový	k2eAgInPc4d1	povrchový
doly	dol	k1gInPc4	dol
<g/>
,	,	kIx,	,
hlubinnou	hlubinný	k2eAgFnSc4d1	hlubinná
těžbu	těžba	k1gFnSc4	těžba
nebo	nebo	k8xC	nebo
těžbu	těžba	k1gFnSc4	těžba
chemickým	chemický	k2eAgNnSc7d1	chemické
loužením	loužení	k1gNnSc7	loužení
(	(	kIx(	(
<g/>
metoda	metoda	k1gFnSc1	metoda
in	in	k?	in
situ	situs	k1gInSc2	situs
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
pumpovaní	pumpovaný	k2eAgMnPc1d1	pumpovaný
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
roztoku	roztok	k1gInSc2	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
nebo	nebo	k8xC	nebo
uhličité	uhličitý	k2eAgFnSc2d1	uhličitá
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
hydrogenuhličitanu	hydrogenuhličitat	k5eAaPmIp1nS	hydrogenuhličitat
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
do	do	k7c2	do
podzemí	podzemí	k1gNnSc2	podzemí
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
uran	uran	k1gInSc1	uran
rozpustit	rozpustit	k5eAaPmF	rozpustit
a	a	k8xC	a
následně	následně	k6eAd1	následně
jej	on	k3xPp3gMnSc4	on
chemicky	chemicky	k6eAd1	chemicky
extrahovat	extrahovat	k5eAaBmF	extrahovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vlastní	vlastní	k2eAgFnSc2d1	vlastní
devastace	devastace	k1gFnSc2	devastace
těžbou	těžba	k1gFnSc7	těžba
představují	představovat	k5eAaImIp3nP	představovat
problém	problém	k1gInSc4	problém
také	také	k9	také
vytěžené	vytěžený	k2eAgFnPc4d1	vytěžená
horniny	hornina	k1gFnPc4	hornina
kontaminované	kontaminovaný	k2eAgFnPc4d1	kontaminovaná
radioaktivním	radioaktivní	k2eAgInSc7d1	radioaktivní
či	či	k8xC	či
toxickými	toxický	k2eAgFnPc7d1	toxická
rozpadovým	rozpadový	k2eAgFnPc3d1	rozpadová
produkty	produkt	k1gInPc7	produkt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
mnoha	mnoho	k4c2	mnoho
uranových	uranový	k2eAgInPc2d1	uranový
dolů	dol	k1gInPc2	dol
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
kontaminaci	kontaminace	k1gFnSc3	kontaminace
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
špatně	špatně	k6eAd1	špatně
provedeného	provedený	k2eAgNnSc2d1	provedené
chemického	chemický	k2eAgNnSc2d1	chemické
loužení	loužení	k1gNnSc2	loužení
uranu	uran	k1gInSc2	uran
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ohroženy	ohrožen	k2eAgFnPc4d1	ohrožena
zásoby	zásoba	k1gFnPc4	zásoba
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Těžba	těžba	k1gFnSc1	těžba
uranu	uran	k1gInSc2	uran
také	také	k9	také
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
zdraví	zdraví	k1gNnSc4	zdraví
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
historických	historický	k2eAgInPc2d1	historický
špatně	špatně	k6eAd1	špatně
větraných	větraný	k2eAgInPc2d1	větraný
hlubinných	hlubinný	k2eAgInPc2d1	hlubinný
dolů	dol	k1gInPc2	dol
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
riziko	riziko	k1gNnSc4	riziko
rakoviny	rakovina	k1gFnSc2	rakovina
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
radon	radon	k1gInSc1	radon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
odpad	odpad	k1gInSc4	odpad
po	po	k7c4	po
obohacování	obohacování	k1gNnSc4	obohacování
uranu	uran	k1gInSc2	uran
zbude	zbýt	k5eAaPmIp3nS	zbýt
tzv.	tzv.	kA	tzv.
ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
(	(	kIx(	(
<g/>
ochuzený	ochuzený	k2eAgMnSc1d1	ochuzený
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zbaven	zbaven	k2eAgInSc1d1	zbaven
podstatné	podstatný	k2eAgFnSc3d1	podstatná
části	část	k1gFnSc3	část
izotopu	izotop	k1gInSc2	izotop
235U	[number]	k4	235U
využitelného	využitelný	k2eAgNnSc2d1	využitelné
jako	jako	k8xC	jako
palivo	palivo	k1gNnSc4	palivo
pro	pro	k7c4	pro
jaderné	jaderný	k2eAgInPc4d1	jaderný
reaktory	reaktor	k1gInPc4	reaktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
zkratkou	zkratka	k1gFnSc7	zkratka
DU	DU	k?	DU
(	(	kIx(	(
<g/>
depleted	depleted	k1gInSc1	depleted
uranium	uranium	k1gNnSc1	uranium
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
řidčeji	řídce	k6eAd2	řídce
tuballoy	tuballo	k2eAgFnPc1d1	tuballo
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hustotu	hustota	k1gFnSc4	hustota
využíván	využíván	k2eAgMnSc1d1	využíván
všude	všude	k6eAd1	všude
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
vyvážení	vyvážení	k1gNnSc4	vyvážení
<g/>
,	,	kIx,	,
nutnost	nutnost	k1gFnSc4	nutnost
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysoké	vysoký	k2eAgFnPc4d1	vysoká
kinetické	kinetický	k2eAgFnPc4d1	kinetická
energie	energie	k1gFnPc4	energie
při	při	k7c6	při
malém	malý	k2eAgInSc6d1	malý
objemu	objem	k1gInSc6	objem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starším	starý	k2eAgInSc6d2	starší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
používaném	používaný	k2eAgInSc6d1	používaný
Boeingu	boeing	k1gInSc6	boeing
747	[number]	k4	747
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
vyrovnávací	vyrovnávací	k2eAgNnSc1d1	vyrovnávací
závaží	závaží	k1gNnSc1	závaží
na	na	k7c6	na
zádi	záď	k1gFnSc6	záď
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
kolem	kolem	k7c2	kolem
600	[number]	k4	600
exemplářů	exemplář	k1gInPc2	exemplář
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
obsahujícího	obsahující	k2eAgMnSc2d1	obsahující
ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
exempláře	exemplář	k1gInPc1	exemplář
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
400	[number]	k4	400
<g/>
-	-	kIx~	-
<g/>
600	[number]	k4	600
kg	kg	kA	kg
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
pramen	pramen	k1gInSc1	pramen
uvádí	uvádět	k5eAaImIp3nS	uvádět
dokonce	dokonce	k9	dokonce
400	[number]	k4	400
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
500	[number]	k4	500
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgInSc7d1	podobný
způsoben	způsobit	k5eAaPmNgInS	způsobit
je	být	k5eAaImIp3nS	být
využíván	využíván	k2eAgInSc1d1	využíván
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
amerických	americký	k2eAgNnPc6d1	americké
letadlech	letadlo	k1gNnPc6	letadlo
McDonnell	McDonnell	k1gInSc4	McDonnell
Douglas	Douglas	k1gInSc1	Douglas
DC-	DC-	k1gFnSc1	DC-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zátěž	zátěž	k1gFnSc1	zátěž
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
plachetnicích	plachetnice	k1gFnPc6	plachetnice
<g/>
,	,	kIx,	,
rotorech	rotor	k1gInPc6	rotor
gyroskopů	gyroskop	k1gInPc2	gyroskop
<g/>
,	,	kIx,	,
ropných	ropný	k2eAgFnPc6d1	ropná
vrtných	vrtný	k2eAgFnPc6d1	vrtná
soupravách	souprava	k1gFnPc6	souprava
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
dokonce	dokonce	k9	dokonce
i	i	k9	i
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
Formule	formule	k1gFnSc2	formule
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
amerických	americký	k2eAgInPc6d1	americký
tancích	tanec	k1gInPc6	tanec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
M1	M1	k1gFnSc1	M1
Abrams	Abrams	k1gInSc1	Abrams
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
pancíře	pancíř	k1gInSc2	pancíř
<g/>
.	.	kIx.	.
</s>
<s>
Ochuzený	ochuzený	k2eAgInSc1d1	ochuzený
uran	uran	k1gInSc1	uran
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
rovněž	rovněž	k9	rovněž
jako	jako	k8xC	jako
stínění	stínění	k1gNnSc1	stínění
před	před	k7c7	před
radioaktivitou	radioaktivita	k1gFnSc7	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc4	sloučenina
hexahydrát	hexahydrát	k1gInSc4	hexahydrát
diurananu	diuranan	k1gInSc2	diuranan
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7.6	[number]	k4	7.6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
a	a	k8xC	a
hexahydrát	hexahydrát	k1gInSc1	hexahydrát
diurananu	diuranan	k1gInSc2	diuranan
draselného	draselný	k2eAgInSc2d1	draselný
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7.6	[number]	k4	7.6
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k8xS	jako
uranová	uranový	k2eAgFnSc1d1	uranová
žluť	žluť	k1gFnSc1	žluť
používající	používající	k2eAgFnSc1d1	používající
se	se	k3xPyFc4	se
k	k	k7c3	k
barvení	barvení	k1gNnSc3	barvení
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
glazur	glazura	k1gFnPc2	glazura
a	a	k8xC	a
porcelánu	porcelán	k1gInSc2	porcelán
(	(	kIx(	(
<g/>
barví	barvit	k5eAaImIp3nS	barvit
na	na	k7c4	na
žluto	žluto	k1gNnSc4	žluto
až	až	k9	až
žlutozeleno	žlutozelen	k2eAgNnSc1d1	žlutozelen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
fluoreskuje	fluoreskovat	k5eAaImIp3nS	fluoreskovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
barvit	barvit	k5eAaImF	barvit
i	i	k9	i
oranžově	oranžově	k6eAd1	oranžově
až	až	k9	až
rudě	rudě	k6eAd1	rudě
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
tohoto	tento	k3xDgNnSc2	tento
použití	použití	k1gNnSc2	použití
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
výrazně	výrazně	k6eAd1	výrazně
snížila	snížit	k5eAaPmAgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
Státního	státní	k2eAgInSc2d1	státní
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
(	(	kIx(	(
<g/>
SÚJB	SÚJB	kA	SÚJB
<g/>
)	)	kIx)	)
2	[number]	k4	2
výrobci	výrobce	k1gMnPc1	výrobce
skla	sklo	k1gNnSc2	sklo
barveného	barvené	k1gNnSc2	barvené
uranem	uran	k1gInSc7	uran
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyráběné	vyráběný	k2eAgNnSc4d1	vyráběné
sklo	sklo	k1gNnSc4	sklo
zdravotně	zdravotně	k6eAd1	zdravotně
zcela	zcela	k6eAd1	zcela
neškodné	škodný	k2eNgNnSc1d1	neškodné
i	i	k9	i
při	při	k7c6	při
silně	silně	k6eAd1	silně
konzervativním	konzervativní	k2eAgInSc6d1	konzervativní
přístupu	přístup	k1gInSc6	přístup
hodnocení	hodnocení	k1gNnSc2	hodnocení
zdravotního	zdravotní	k2eAgNnSc2d1	zdravotní
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fotografii	fotografia	k1gFnSc6	fotografia
se	s	k7c7	s
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
solí	sůl	k1gFnPc2	sůl
<g/>
)	)	kIx)	)
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
např.	např.	kA	např.
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
-	-	kIx~	-
dusičnan	dusičnan	k1gInSc1	dusičnan
uranylu	uranyl	k1gInSc2	uranyl
<g/>
)	)	kIx)	)
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
zesilování	zesilování	k1gNnSc3	zesilování
negativů	negativ	k1gInPc2	negativ
<g/>
,	,	kIx,	,
do	do	k7c2	do
tónovacích	tónovací	k2eAgFnPc2d1	tónovací
lázní	lázeň	k1gFnPc2	lázeň
<g/>
,	,	kIx,	,
zesilovač	zesilovač	k1gInSc4	zesilovač
světlotisku	světlotisk	k1gInSc2	světlotisk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
chemické	chemický	k2eAgFnSc3d1	chemická
toxicitě	toxicita	k1gFnSc3	toxicita
se	se	k3xPyFc4	se
dusičnan	dusičnan	k1gInSc1	dusičnan
uranylu	uranyl	k1gInSc2	uranyl
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
experimentální	experimentální	k2eAgNnPc4d1	experimentální
vyvolání	vyvolání	k1gNnSc4	vyvolání
patologického	patologický	k2eAgInSc2d1	patologický
stavu	stav	k1gInSc2	stav
ledvin	ledvina	k1gFnPc2	ledvina
u	u	k7c2	u
pokusných	pokusný	k2eAgNnPc2d1	pokusné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
uranylu	uranyl	k1gInSc2	uranyl
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2.2	[number]	k4	2.2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
NaUO	NaUO	k1gFnSc1	NaUO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
diuranan	diuranan	k1gMnSc1	diuranan
amonný	amonný	k2eAgMnSc1d1	amonný
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
v	v	k7c6	v
analytické	analytický	k2eAgFnSc6d1	analytická
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
karbidu	karbid	k1gInSc2	karbid
je	být	k5eAaImIp3nS	být
vhodným	vhodný	k2eAgInSc7d1	vhodný
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
amoniaku	amoniak	k1gInSc2	amoniak
Haberovým	Haberův	k2eAgInSc7d1	Haberův
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
wolframu	wolfram	k1gInSc2	wolfram
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
protipancéřových	protipancéřový	k2eAgInPc2d1	protipancéřový
projektilů	projektil	k1gInPc2	projektil
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
šípové	šípový	k2eAgFnPc1d1	šípová
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
podkaliberní	podkaliberní	k2eAgFnPc4d1	podkaliberní
střely	střela	k1gFnPc4	střela
-	-	kIx~	-
průměr	průměr	k1gInSc1	průměr
střely	střela	k1gFnSc2	střela
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
hlavně	hlavně	k6eAd1	hlavně
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
vystřelena	vystřelen	k2eAgFnSc1d1	vystřelena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
sice	sice	k8xC	sice
především	především	k6eAd1	především
vysoká	vysoký	k2eAgFnSc1d1	vysoká
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
střely	střela	k1gFnSc2	střela
<g/>
,	,	kIx,	,
účinek	účinek	k1gInSc1	účinek
však	však	k9	však
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
průniku	průnik	k1gInSc6	průnik
projektilu	projektil	k1gInSc2	projektil
za	za	k7c4	za
pancíř	pancíř	k1gInSc4	pancíř
se	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
a	a	k8xC	a
třením	tření	k1gNnSc7	tření
rozžhavené	rozžhavený	k2eAgFnSc2d1	rozžhavená
úlomky	úlomek	k1gInPc7	úlomek
uranu	uran	k1gInSc2	uran
vznítí	vznítit	k5eAaPmIp3nP	vznítit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
ničivý	ničivý	k2eAgInSc1d1	ničivý
účinek	účinek	k1gInSc1	účinek
uvnitř	uvnitř	k7c2	uvnitř
obrněného	obrněný	k2eAgInSc2d1	obrněný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Střely	střela	k1gFnPc1	střela
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
též	též	k9	též
mají	mít	k5eAaImIp3nP	mít
výhodnější	výhodný	k2eAgInSc4d2	výhodnější
mezní	mezní	k2eAgInSc4d1	mezní
úhel	úhel	k1gInSc4	úhel
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
opancéřovaného	opancéřovaný	k2eAgInSc2d1	opancéřovaný
cíle	cíl	k1gInSc2	cíl
neodrazí	odrazit	k5eNaPmIp3nS	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
důrazně	důrazně	k6eAd1	důrazně
upozornit	upozornit	k5eAaPmF	upozornit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
použití	použití	k1gNnSc4	použití
jako	jako	k9	jako
takové	takový	k3xDgNnSc1	takový
nemá	mít	k5eNaImIp3nS	mít
absolutně	absolutně	k6eAd1	absolutně
žádnou	žádný	k3yNgFnSc4	žádný
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
jaderným	jaderný	k2eAgNnSc7d1	jaderné
využíváním	využívání	k1gNnSc7	využívání
uranu	uran	k1gInSc2	uran
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
nukleárních	nukleární	k2eAgFnPc2d1	nukleární
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pouze	pouze	k6eAd1	pouze
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
srovnatelná	srovnatelný	k2eAgFnSc1d1	srovnatelná
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
konkurenčním	konkurenční	k2eAgInSc7d1	konkurenční
materiálem	materiál	k1gInSc7	materiál
wolframem	wolfram	k1gInSc7	wolfram
<g/>
,	,	kIx,	,
snadná	snadný	k2eAgFnSc1d1	snadná
vznětlivost	vznětlivost	k1gFnSc1	vznětlivost
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
poměrně	poměrně	k6eAd1	poměrně
nízkou	nízký	k2eAgFnSc4d1	nízká
radioaktivitu	radioaktivita	k1gFnSc4	radioaktivita
238U	[number]	k4	238U
však	však	k9	však
přesto	přesto	k6eAd1	přesto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
slabému	slabý	k2eAgNnSc3d1	slabé
radioaktivnímu	radioaktivní	k2eAgNnSc3d1	radioaktivní
zamoření	zamoření	k1gNnSc3	zamoření
<g/>
,	,	kIx,	,
míra	míra	k1gFnSc1	míra
jeho	jeho	k3xOp3gFnSc2	jeho
neškodnosti	neškodnost	k1gFnSc2	neškodnost
nebo	nebo	k8xC	nebo
škodlivosti	škodlivost	k1gFnSc2	škodlivost
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
dořešena	dořešit	k5eAaPmNgFnS	dořešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
padlo	padnout	k5eAaPmAgNnS	padnout
na	na	k7c6	na
Valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
OSN	OSN	kA	OSN
několik	několik	k4yIc4	několik
návrhů	návrh	k1gInPc2	návrh
na	na	k7c6	na
prozkoumání	prozkoumání	k1gNnSc6	prozkoumání
účinků	účinek	k1gInPc2	účinek
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
munice	munice	k1gFnSc2	munice
z	z	k7c2	z
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
několika	několik	k4yIc2	několik
zemí	zem	k1gFnPc2	zem
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
schváleny	schválit	k5eAaPmNgInP	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc4d2	veliký
roli	role	k1gFnSc4	role
přitom	přitom	k6eAd1	přitom
hraje	hrát	k5eAaImIp3nS	hrát
ani	ani	k8xC	ani
ne	ne	k9	ne
tak	tak	k9	tak
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
ochuzeného	ochuzený	k2eAgInSc2d1	ochuzený
uranu	uran	k1gInSc2	uran
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
dovnitř	dovnitř	k7c2	dovnitř
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
toxicita	toxicita	k1gFnSc1	toxicita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
uran	uran	k1gInSc1	uran
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organizmy	organizmus	k1gInPc4	organizmus
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
velké	velký	k2eAgNnSc1d1	velké
rozptýlení	rozptýlení	k1gNnSc1	rozptýlení
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
možnost	možnost	k1gFnSc4	možnost
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
potravou	potrava	k1gFnSc7	potrava
<g/>
,	,	kIx,	,
pitím	pití	k1gNnSc7	pití
nebo	nebo	k8xC	nebo
vdechnutím	vdechnutí	k1gNnSc7	vdechnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
střely	střela	k1gFnPc1	střela
byly	být	k5eAaImAgFnP	být
použity	použít	k5eAaPmNgFnP	použít
spojenci	spojenec	k1gMnPc1	spojenec
v	v	k7c6	v
první	první	k4xOgFnSc6	první
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
a	a	k8xC	a
od	od	k7c2	od
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
i	i	k8xC	i
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
Irácká	irácký	k2eAgFnSc1d1	irácká
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
irácké	irácký	k2eAgFnSc2d1	irácká
ministryně	ministryně	k1gFnSc2	ministryně
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
Nermin	Nermin	k2eAgMnSc1d1	Nermin
Othman	Othman	k1gMnSc1	Othman
bombardováním	bombardování	k1gNnPc3	bombardování
kontaminováno	kontaminovat	k5eAaBmNgNnS	kontaminovat
přes	přes	k7c4	přes
350	[number]	k4	350
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
rakovinných	rakovinný	k2eAgNnPc2d1	rakovinné
onemocnění	onemocnění	k1gNnPc2	onemocnění
údajně	údajně	k6eAd1	údajně
činí	činit	k5eAaImIp3nS	činit
140	[number]	k4	140
tisíc	tisíc	k4xCgInPc2	tisíc
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
7	[number]	k4	7
až	až	k9	až
8	[number]	k4	8
tisíc	tisíc	k4xCgInSc4	tisíc
případů	případ	k1gInPc2	případ
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
data	datum	k1gNnPc1	datum
však	však	k9	však
nebyla	být	k5eNaImAgNnP	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
nezávislými	závislý	k2eNgInPc7d1	nezávislý
zdroji	zdroj	k1gInPc7	zdroj
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
vyvolání	vyvolání	k1gNnSc4	vyvolání
rakoviny	rakovina	k1gFnSc2	rakovina
plic	plíce	k1gFnPc2	plíce
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vdechnout	vdechnout	k5eAaPmF	vdechnout
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
uranového	uranový	k2eAgInSc2d1	uranový
prachu	prach	k1gInSc2	prach
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
rakoviny	rakovina	k1gFnSc2	rakovina
je	být	k5eAaImIp3nS	být
riziko	riziko	k1gNnSc4	riziko
ještě	ještě	k9	ještě
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
první	první	k4xOgInSc1	první
výbor	výbor	k1gInSc1	výbor
OSN	OSN	kA	OSN
schválil	schválit	k5eAaPmAgInS	schválit
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
návrh	návrh	k1gInSc4	návrh
rezoluce	rezoluce	k1gFnSc2	rezoluce
požadující	požadující	k2eAgNnSc1d1	požadující
přezkoumání	přezkoumání	k1gNnSc1	přezkoumání
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
rizik	riziko	k1gNnPc2	riziko
zbraní	zbraň	k1gFnPc2	zbraň
používajících	používající	k2eAgFnPc2d1	používající
ochuzený	ochuzený	k2eAgInSc4d1	ochuzený
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bylo	být	k5eAaImAgNnS	být
122	[number]	k4	122
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
6	[number]	k4	6
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
proti	proti	k7c3	proti
jednáním	jednání	k1gNnPc3	jednání
o	o	k7c6	o
omezení	omezení	k1gNnSc6	omezení
zbraňových	zbraňový	k2eAgInPc2d1	zbraňový
systémů	systém	k1gInPc2	systém
s	s	k7c7	s
ochuzeným	ochuzený	k2eAgInSc7d1	ochuzený
uranem	uran	k1gInSc7	uran
hlasovala	hlasovat	k5eAaImAgFnS	hlasovat
i	i	k9	i
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uran	Uran	k1gInSc1	Uran
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
sloučeniny	sloučenina	k1gFnPc4	sloučenina
s	s	k7c7	s
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
čísly	číslo	k1gNnPc7	číslo
U	u	k7c2	u
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
až	až	k6eAd1	až
U	u	k7c2	u
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejstabilnější	stabilní	k2eAgFnPc1d3	nejstabilnější
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
s	s	k7c7	s
U	u	k7c2	u
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
soli	sůl	k1gFnSc2	sůl
sloučenin	sloučenina	k1gFnPc2	sloučenina
uranu	uran	k1gInSc2	uran
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgFnPc1d1	barevná
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jejich	jejich	k3xOp3gFnSc1	jejich
barva	barva	k1gFnSc1	barva
není	být	k5eNaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
barvou	barva	k1gFnSc7	barva
aniontu	anion	k1gInSc2	anion
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
uranité	uranitý	k2eAgFnPc4d1	uranitý
soli	sůl	k1gFnPc4	sůl
U	u	k7c2	u
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
mají	mít	k5eAaImIp3nP	mít
hnědočervené	hnědočervený	k2eAgNnSc1d1	hnědočervené
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
,	,	kIx,	,
uraničité	uraničitý	k2eAgNnSc1d1	uraničitý
U	u	k7c2	u
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
zelenou	zelená	k1gFnSc7	zelená
<g/>
,	,	kIx,	,
uraničné	uraničný	k2eAgNnSc1d1	uraničný
U	u	k7c2	u
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
<g />
.	.	kIx.	.
</s>
<s>
kationtu	kation	k1gInSc3	kation
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
disproporcionují	disproporcionovat	k5eAaPmIp3nP	disproporcionovat
<g/>
)	)	kIx)	)
na	na	k7c4	na
směs	směs	k1gFnSc4	směs
solí	sůl	k1gFnPc2	sůl
uraničitých	uraničitý	k2eAgInPc2d1	uraničitý
a	a	k8xC	a
uranových	uranový	k2eAgInPc2d1	uranový
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
soli	sůl	k1gFnPc4	sůl
uranové	uranový	k2eAgFnPc4d1	uranová
U	u	k7c2	u
<g/>
6	[number]	k4	6
<g/>
+	+	kIx~	+
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kationtu	kation	k1gInSc2	kation
UO	UO	kA	UO
<g/>
22	[number]	k4	22
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
uranylový	uranylový	k2eAgInSc1d1	uranylový
kationt	kationt	k1gInSc1	kationt
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgFnPc1d3	nejběžnější
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
setkat	setkat	k5eAaPmF	setkat
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
níže	nízce	k6eAd2	nízce
popsaných	popsaný	k2eAgInPc2d1	popsaný
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
kationt	kationt	k1gInSc4	kationt
uranylu	uranyl	k1gInSc2	uranyl
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
např.	např.	kA	např.
dusičnan	dusičnan	k1gInSc4	dusičnan
uranylu	uranyl	k1gInSc2	uranyl
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgFnPc4d3	nejběžnější
sloučeniny	sloučenina	k1gFnPc4	sloučenina
uranu	uran	k1gInSc2	uran
patří	patřit	k5eAaImIp3nS	patřit
oxid	oxid	k1gInSc1	oxid
uraničitý	uraničitý	k2eAgInSc1d1	uraničitý
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
minerálu	minerál	k1gInSc2	minerál
smolince	smolinec	k1gInSc2	smolinec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kovového	kovový	k2eAgInSc2d1	kovový
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jako	jako	k8xC	jako
uranová	uranový	k2eAgNnPc4d1	uranové
ruda	rudo	k1gNnPc4	rudo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
směsný	směsný	k2eAgInSc1d1	směsný
oxid	oxid	k1gInSc1	oxid
uraničito-uranový	uraničitoranový	k2eAgInSc1d1	uraničito-uranový
U3O8	U3O8	k1gMnSc7	U3O8
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
rozepsat	rozepsat	k5eAaPmF	rozepsat
na	na	k7c4	na
složky	složka	k1gFnPc4	složka
-	-	kIx~	-
UO	UO	kA	UO
<g/>
2.2	[number]	k4	2.2
<g/>
UO	UO	kA	UO
<g/>
3	[number]	k4	3
a	a	k8xC	a
možno	možno	k6eAd1	možno
také	také	k9	také
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
jako	jako	k9	jako
oxid	oxid	k1gInSc1	oxid
uraničito-diuranový	uraničitoiuranový	k2eAgInSc1d1	uraničito-diuranový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
uraničitého	uraničitý	k2eAgInSc2d1	uraničitý
ho	on	k3xPp3gInSc4	on
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
žíháním	žíhání	k1gNnSc7	žíhání
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
významným	významný	k2eAgFnPc3d1	významná
sloučeninám	sloučenina	k1gFnPc3	sloučenina
uranu	uran	k1gInSc2	uran
patří	patřit	k5eAaImIp3nP	patřit
některé	některý	k3yIgFnPc1	některý
halogenidy	halogenida	k1gFnPc1	halogenida
-	-	kIx~	-
konkrétně	konkrétně	k6eAd1	konkrétně
chlorid	chlorid	k1gInSc4	chlorid
uraničitý	uraničitý	k2eAgInSc4d1	uraničitý
UCl	UCl	k1gFnSc7	UCl
<g/>
4	[number]	k4	4
a	a	k8xC	a
fluorid	fluorid	k1gInSc1	fluorid
uraničitý	uraničitý	k2eAgInSc1d1	uraničitý
UF	uf	k0	uf
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
kovového	kovový	k2eAgInSc2d1	kovový
uranu	uran	k1gInSc2	uran
redukcí	redukce	k1gFnSc7	redukce
draslíkem	draslík	k1gInSc7	draslík
<g/>
,	,	kIx,	,
či	či	k8xC	či
vápníkem	vápník	k1gInSc7	vápník
<g/>
,	,	kIx,	,
a	a	k8xC	a
fluorid	fluorid	k1gInSc1	fluorid
uranový	uranový	k2eAgInSc1d1	uranový
UF	uf	k0	uf
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
fluor	fluor	k1gInSc1	fluor
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
jeden	jeden	k4xCgInSc1	jeden
izotop	izotop	k1gInSc1	izotop
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
monoizotopický	monoizotopický	k2eAgInSc1d1	monoizotopický
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
k	k	k7c3	k
separaci	separace	k1gFnSc3	separace
235UF6	[number]	k4	235UF6
a	a	k8xC	a
238UF6	[number]	k4	238UF6
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odlišné	odlišný	k2eAgFnPc4d1	odlišná
hmotnosti	hmotnost	k1gFnPc4	hmotnost
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
izotopických	izotopický	k2eAgFnPc2d1	izotopická
sloučenin	sloučenina	k1gFnPc2	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
o	o	k7c6	o
obohaceném	obohacený	k2eAgInSc6d1	obohacený
uranu	uran	k1gInSc6	uran
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
výrobě	výroba	k1gFnSc3	výroba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
