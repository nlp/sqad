<s>
Werner	Werner	k1gMnSc1	Werner
Karl	Karl	k1gMnSc1	Karl
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1901	[number]	k4	1901
Würzburg	Würzburg	k1gInSc1	Würzburg
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1976	[number]	k4	1976
Mnichov	Mnichov	k1gInSc1	Mnichov
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vytváření	vytváření	k1gNnSc6	vytváření
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
získal	získat	k5eAaPmAgMnS	získat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
teoretické	teoretický	k2eAgFnSc2d1	teoretická
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
nový	nový	k2eAgInSc1d1	nový
výklad	výklad	k1gInSc1	výklad
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
lišil	lišit	k5eAaImAgMnS	lišit
od	od	k7c2	od
Newtonova	Newtonův	k2eAgInSc2d1	Newtonův
výkladu	výklad	k1gInSc2	výklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
formuloval	formulovat	k5eAaImAgInS	formulovat
slavný	slavný	k2eAgInSc1d1	slavný
princip	princip	k1gInSc1	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
<g/>
.	.	kIx.	.
</s>
<s>
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
emigrovat	emigrovat	k5eAaBmF	emigrovat
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
z	z	k7c2	z
rodného	rodný	k2eAgNnSc2d1	rodné
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
za	za	k7c2	za
časů	čas	k1gInPc2	čas
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
byla	být	k5eAaImAgFnS	být
též	též	k9	též
sepsána	sepsán	k2eAgFnSc1d1	sepsána
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
formuloval	formulovat	k5eAaImAgInS	formulovat
slavný	slavný	k2eAgInSc1d1	slavný
princip	princip	k1gInSc1	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
jisté	jistý	k2eAgFnPc4d1	jistá
teoretické	teoretický	k2eAgFnPc4d1	teoretická
hranice	hranice	k1gFnPc4	hranice
naší	náš	k3xOp1gFnSc2	náš
schopnosti	schopnost	k1gFnSc2	schopnost
provádět	provádět	k5eAaImF	provádět
vědecká	vědecký	k2eAgNnPc4d1	vědecké
měření	měření	k1gNnPc4	měření
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
principu	princip	k1gInSc2	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
nám	my	k3xPp1nPc3	my
ani	ani	k8xC	ani
sebelepší	sebelepší	k2eAgNnSc4d1	sebelepší
zdokonalení	zdokonalení	k1gNnSc4	zdokonalení
měřícího	měřící	k2eAgInSc2d1	měřící
přístroje	přístroj	k1gInSc2	přístroj
nedovolí	dovolit	k5eNaPmIp3nS	dovolit
získat	získat	k5eAaPmF	získat
přesné	přesný	k2eAgInPc4d1	přesný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
praktickému	praktický	k2eAgNnSc3d1	praktické
využití	využití	k1gNnSc3	využití
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
patří	patřit	k5eAaImIp3nP	patřit
elektronové	elektronový	k2eAgInPc1d1	elektronový
mikroskopy	mikroskop	k1gInPc1	mikroskop
<g/>
,	,	kIx,	,
lasery	laser	k1gInPc1	laser
<g/>
,	,	kIx,	,
tranzistory	tranzistor	k1gInPc1	tranzistor
<g/>
,	,	kIx,	,
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalezne	nalézt	k5eAaBmIp3nS	nalézt
i	i	k9	i
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
a	a	k8xC	a
atomové	atomový	k2eAgFnSc6d1	atomová
energii	energie	k1gFnSc6	energie
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
v	v	k7c4	v
astronomii	astronomie	k1gFnSc4	astronomie
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
v	v	k7c6	v
teoretickém	teoretický	k2eAgNnSc6d1	teoretické
zkoumání	zkoumání	k1gNnSc6	zkoumání
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kapalného	kapalný	k2eAgNnSc2d1	kapalné
hélia	hélium	k1gNnSc2	hélium
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
konstituce	konstituce	k1gFnSc2	konstituce
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
feromagnetismus	feromagnetismus	k1gInSc1	feromagnetismus
a	a	k8xC	a
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Heisenbergova	Heisenbergův	k2eAgMnSc2d1	Heisenbergův
kvantově	kvantově	k6eAd1	kvantově
mechanického	mechanický	k2eAgInSc2d1	mechanický
modelu	model	k1gInSc2	model
nelze	lze	k6eNd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
(	(	kIx(	(
<g/>
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
přesností	přesnost	k1gFnSc7	přesnost
<g/>
)	)	kIx)	)
dráhu	dráha	k1gFnSc4	dráha
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
elektronů	elektron	k1gInPc2	elektron
-	-	kIx~	-
čili	čili	k8xC	čili
polohu	poloha	k1gFnSc4	poloha
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
-	-	kIx~	-
Heisenbergův	Heisenbergův	k2eAgInSc1d1	Heisenbergův
princip	princip	k1gInSc1	princip
neurčitosti	neurčitost	k1gFnSc2	neurčitost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k6eAd1	již
podle	podle	k7c2	podle
nových	nový	k2eAgInPc2d1	nový
poznatků	poznatek	k1gInPc2	poznatek
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
formulaci	formulace	k1gFnSc6	formulace
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
1970	[number]	k4	1970
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
za	za	k7c4	za
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
prózu	próza	k1gFnSc4	próza
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
pojmenován	pojmenován	k2eAgInSc4d1	pojmenován
asteroid	asteroid	k1gInSc4	asteroid
hlavního	hlavní	k2eAgInSc2d1	hlavní
pásu	pás	k1gInSc2	pás
(	(	kIx(	(
<g/>
13149	[number]	k4	13149
<g/>
)	)	kIx)	)
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gInSc4	Heisenberg
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Werner	Werner	k1gMnSc1	Werner
Karl	Karl	k1gMnSc1	Karl
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
