<s>
Platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Pt	Pt	k1gFnSc1	Pt
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Platinum	Platinum	k1gInSc1	Platinum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc1d1	těžký
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
mimořádně	mimořádně	k6eAd1	mimořádně
odolný	odolný	k2eAgInSc1d1	odolný
drahý	drahý	k2eAgInSc1d1	drahý
kov	kov	k1gInSc1	kov
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílé	bílý	k2eAgFnSc2d1	bílá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Ušlechtilý	ušlechtilý	k2eAgMnSc1d1	ušlechtilý
<g/>
,	,	kIx,	,
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc1d1	kujný
a	a	k8xC	a
tažný	tažný	k2eAgInSc1d1	tažný
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
elektricky	elektricky	k6eAd1	elektricky
i	i	k9	i
tepelně	tepelně	k6eAd1	tepelně
středně	středně	k6eAd1	středně
dobře	dobře	k6eAd1	dobře
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
ryzí	ryzí	k2eAgMnPc4d1	ryzí
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
platina	platina	k1gFnSc1	platina
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k8xC	jako
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
ze	z	k7c2	z
španělského	španělský	k2eAgNnSc2d1	španělské
slova	slovo	k1gNnSc2	slovo
plata	plato	k1gNnSc2	plato
(	(	kIx(	(
<g/>
stříbro	stříbro	k1gNnSc1	stříbro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
ho	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k9	jako
stříbříčko	stříbříčko	k1gNnSc4	stříbříčko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
lučavce	lučavka	k1gFnSc6	lučavka
královské	královský	k2eAgFnSc6d1	královská
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
i	i	k9	i
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
nebo	nebo	k8xC	nebo
peroxidu	peroxid	k1gInSc2	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
osmiem	osmium	k1gNnSc7	osmium
a	a	k8xC	a
iridiem	iridium	k1gNnSc7	iridium
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
prvkům	prvek	k1gInPc3	prvek
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
známou	známý	k2eAgFnSc7d1	známá
hustotou	hustota	k1gFnSc7	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
platiny	platina	k1gFnSc2	platina
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
značné	značný	k2eAgInPc4d1	značný
objemy	objem	k1gInPc4	objem
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Platina	platina	k1gFnSc1	platina
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
také	také	k9	také
značné	značný	k2eAgFnPc4d1	značná
katalytické	katalytický	k2eAgFnPc4d1	katalytická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svým	svůj	k3xOyFgFnPc3	svůj
mechanickým	mechanický	k2eAgFnPc3d1	mechanická
vlastnostem	vlastnost	k1gFnPc3	vlastnost
a	a	k8xC	a
chemické	chemický	k2eAgFnSc2d1	chemická
odolnosti	odolnost	k1gFnSc2	odolnost
jsou	být	k5eAaImIp3nP	být
platina	platina	k1gFnSc1	platina
a	a	k8xC	a
především	především	k9	především
její	její	k3xOp3gFnSc2	její
slitiny	slitina	k1gFnSc2	slitina
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
a	a	k8xC	a
iridiem	iridium	k1gNnSc7	iridium
používány	používat	k5eAaImNgFnP	používat
na	na	k7c6	na
výrobu	výrob	k1gInSc6	výrob
odolného	odolný	k2eAgNnSc2d1	odolné
chemického	chemický	k2eAgNnSc2d1	chemické
nádobí	nádobí	k1gNnSc2	nádobí
pro	pro	k7c4	pro
rozklady	rozklad	k1gInPc4	rozklad
vzorků	vzorek	k1gInPc2	vzorek
tavením	tavení	k1gNnSc7	tavení
nebo	nebo	k8xC	nebo
spalováním	spalování	k1gNnSc7	spalování
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
speciálních	speciální	k2eAgFnPc2d1	speciální
pecí	pec	k1gFnPc2	pec
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
platina	platina	k1gFnSc1	platina
a	a	k8xC	a
především	především	k9	především
její	její	k3xOp3gFnSc2	její
sloučeniny	sloučenina	k1gFnSc2	sloučenina
využívána	využívat	k5eAaImNgFnS	využívat
jako	jako	k8xC	jako
všestranný	všestranný	k2eAgInSc4d1	všestranný
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
organických	organický	k2eAgFnPc2d1	organická
syntéz	syntéza	k1gFnPc2	syntéza
<g/>
.	.	kIx.	.
</s>
<s>
Katalytických	katalytický	k2eAgFnPc2d1	katalytická
vlastností	vlastnost	k1gFnPc2	vlastnost
jemně	jemně	k6eAd1	jemně
rozptýlené	rozptýlený	k2eAgFnSc2d1	rozptýlená
kovové	kovový	k2eAgFnSc2d1	kovová
platiny	platina	k1gFnSc2	platina
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
v	v	k7c6	v
autokatalyzátorech	autokatalyzátor	k1gInPc6	autokatalyzátor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
látek	látka	k1gFnPc2	látka
z	z	k7c2	z
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
farmaceutickém	farmaceutický	k2eAgInSc6d1	farmaceutický
průmyslu	průmysl	k1gInSc6	průmysl
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
cis-platiny	cislatina	k1gFnSc2	cis-platina
základem	základ	k1gInSc7	základ
velmi	velmi	k6eAd1	velmi
účinných	účinný	k2eAgNnPc2d1	účinné
cytostatik	cytostatikum	k1gNnPc2	cytostatikum
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
látek	látka	k1gFnPc2	látka
potlačujících	potlačující	k2eAgInPc2d1	potlačující
rakovinné	rakovinný	k2eAgNnSc4d1	rakovinné
bujení	bujení	k1gNnSc4	bujení
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
velkých	velký	k2eAgInPc2d1	velký
objemů	objem	k1gInPc2	objem
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výroba	výroba	k1gFnSc1	výroba
termočlánků	termočlánek	k1gInPc2	termočlánek
pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
měření	měření	k1gNnSc4	měření
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
slitin	slitina	k1gFnPc2	slitina
platiny	platina	k1gFnSc2	platina
s	s	k7c7	s
rhodiem	rhodium	k1gNnSc7	rhodium
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
využití	využití	k1gNnSc1	využití
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
termočlánků	termočlánek	k1gInPc2	termočlánek
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
sklářském	sklářský	k2eAgInSc6d1	sklářský
a	a	k8xC	a
hutnickém	hutnický	k2eAgInSc6d1	hutnický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
platina	platina	k1gFnSc1	platina
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
šperků	šperk	k1gInPc2	šperk
a	a	k8xC	a
k	k	k7c3	k
pokovování	pokovování	k1gNnSc3	pokovování
méně	málo	k6eAd2	málo
ušlechtilých	ušlechtilý	k2eAgMnPc2d1	ušlechtilý
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
některých	některý	k3yIgFnPc2	některý
dentálních	dentální	k2eAgFnPc2d1	dentální
slitin	slitina	k1gFnPc2	slitina
především	především	k9	především
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
moderními	moderní	k2eAgInPc7d1	moderní
keramickými	keramický	k2eAgInPc7d1	keramický
materiály	materiál	k1gInPc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Platina	platina	k1gFnSc1	platina
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ryzího	ryzí	k2eAgInSc2d1	ryzí
kovu	kov	k1gInSc2	kov
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
přítomny	přítomen	k2eAgInPc1d1	přítomen
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
platinové	platinový	k2eAgInPc1d1	platinový
kovy	kov	k1gInPc1	kov
jako	jako	k8xS	jako
rhodium	rhodium	k1gNnSc1	rhodium
<g/>
,	,	kIx,	,
palladium	palladium	k1gNnSc1	palladium
nebo	nebo	k8xC	nebo
iridium	iridium	k1gNnSc1	iridium
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
zastoupení	zastoupení	k1gNnSc4	zastoupení
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgNnSc1d1	malé
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
průměrný	průměrný	k2eAgInSc1d1	průměrný
výskyt	výskyt	k1gInSc1	výskyt
činí	činit	k5eAaImIp3nS	činit
0,005	[number]	k4	0,005
<g/>
-	-	kIx~	-
<g/>
0,01	[number]	k4	0,01
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nelze	lze	k6eNd1	lze
současnými	současný	k2eAgFnPc7d1	současná
analytickými	analytický	k2eAgFnPc7d1	analytická
metodami	metoda	k1gFnPc7	metoda
spolehlivě	spolehlivě	k6eAd1	spolehlivě
změřit	změřit	k5eAaPmF	změřit
<g/>
.	.	kIx.	.
</s>
<s>
Nejbohatší	bohatý	k2eAgNnPc1d3	nejbohatší
světová	světový	k2eAgNnPc1d1	světové
naleziště	naleziště	k1gNnPc1	naleziště
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
hlubinných	hlubinný	k2eAgInPc6d1	hlubinný
dolech	dol	k1gInPc6	dol
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
těží	těžet	k5eAaImIp3nS	těžet
až	až	k9	až
ve	v	k7c6	v
čtyřkilometrové	čtyřkilometrový	k2eAgFnSc6d1	čtyřkilometrová
hloubce	hloubka	k1gFnSc6	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
však	však	k9	však
i	i	k9	i
naleziště	naleziště	k1gNnSc4	naleziště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hornina	hornina	k1gFnSc1	hornina
s	s	k7c7	s
jemně	jemně	k6eAd1	jemně
rozptýlenými	rozptýlený	k2eAgFnPc7d1	rozptýlená
částečkami	částečka	k1gFnPc7	částečka
kovu	kov	k1gInSc2	kov
těží	těžet	k5eAaImIp3nS	těžet
povrchově	povrchově	k6eAd1	povrchově
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
lokalitami	lokalita	k1gFnPc7	lokalita
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
platiny	platina	k1gFnSc2	platina
je	být	k5eAaImIp3nS	být
Sibiř	Sibiř	k1gFnSc1	Sibiř
a	a	k8xC	a
Ural	Ural	k1gInSc1	Ural
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vzácně	vzácně	k6eAd1	vzácně
nachází	nacházet	k5eAaImIp3nS	nacházet
platina	platina	k1gFnSc1	platina
i	i	k9	i
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nugetů	nuget	k1gInPc2	nuget
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
i	i	k9	i
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
několik	několik	k4yIc1	několik
nalezišť	naleziště	k1gNnPc2	naleziště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
i	i	k8xC	i
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rudy	ruda	k1gFnPc1	ruda
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
využívaných	využívaný	k2eAgNnPc2d1	využívané
nalezišť	naleziště	k1gNnPc2	naleziště
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
kovnatost	kovnatost	k1gFnSc4	kovnatost
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
t.	t.	k?	t.
Obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
způsobem	způsob	k1gInSc7	způsob
zkoncentrování	zkoncentrování	k1gNnSc2	zkoncentrování
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
flotace	flotace	k1gFnSc1	flotace
po	po	k7c6	po
jemném	jemný	k2eAgNnSc6d1	jemné
namletí	namletí	k1gNnSc6	namletí
vytěžené	vytěžený	k2eAgFnSc2d1	vytěžená
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Platina	platina	k1gFnSc1	platina
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
drahým	drahý	k2eAgInSc7d1	drahý
kovem	kov	k1gInSc7	kov
<g/>
,	,	kIx,	,
kujným	kujný	k2eAgMnSc7d1	kujný
<g/>
,	,	kIx,	,
vhodným	vhodný	k2eAgNnSc7d1	vhodné
pro	pro	k7c4	pro
ražbu	ražba	k1gFnSc4	ražba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mince	mince	k1gFnSc1	mince
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
ražené	ražený	k2eAgInPc4d1	ražený
se	se	k3xPyFc4	se
nestaly	stát	k5eNaPmAgInP	stát
moc	moc	k6eAd1	moc
oblíbené	oblíbený	k2eAgInPc1d1	oblíbený
pro	pro	k7c4	pro
běžné	běžný	k2eAgNnSc4d1	běžné
použití	použití	k1gNnSc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
platinové	platinový	k2eAgFnSc2d1	platinová
mince	mince	k1gFnSc2	mince
spíše	spíše	k9	spíše
jako	jako	k9	jako
sběratelské	sběratelský	k2eAgNnSc1d1	sběratelské
<g/>
,	,	kIx,	,
či	či	k8xC	či
investiční	investiční	k2eAgFnSc1d1	investiční
mince	mince	k1gFnSc1	mince
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
platinové	platinový	k2eAgFnPc1d1	platinová
mince	mince	k1gFnPc1	mince
se	se	k3xPyFc4	se
razily	razit	k5eAaImAgFnP	razit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sběratelského	sběratelský	k2eAgNnSc2d1	sběratelské
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
staré	starý	k2eAgFnPc1d1	stará
platinové	platinový	k2eAgFnPc1d1	platinová
mince	mince	k1gFnPc1	mince
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
XPT	XPT	kA	XPT
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
1	[number]	k4	1
trojské	trojský	k2eAgFnSc2d1	Trojská
unce	unce	k1gFnSc2	unce
platiny	platina	k1gFnSc2	platina
podle	podle	k7c2	podle
standardu	standard	k1gInSc2	standard
ISO	ISO	kA	ISO
4217	[number]	k4	4217
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
-	-	kIx~	-
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Drahé	drahý	k2eAgInPc1d1	drahý
kovy	kov	k1gInPc1	kov
Palladium	palladium	k1gNnSc4	palladium
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
platina	platina	k1gFnSc1	platina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
platina	platina	k1gFnSc1	platina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
