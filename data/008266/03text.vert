<p>
<s>
Tzv.	tzv.	kA	tzv.
únos	únos	k1gInSc1	únos
Michala	Michal	k1gMnSc2	Michal
Kováče	Kováč	k1gMnSc2	Kováč
ml.	ml.	kA	ml.
byl	být	k5eAaImAgInS	být
kauzou	kauza	k1gFnSc7	kauza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
prezidenta	prezident	k1gMnSc2	prezident
Slovenska	Slovensko	k1gNnSc2	Slovensko
Michala	Michal	k1gMnSc2	Michal
Kováče	Kováč	k1gMnSc2	Kováč
násilně	násilně	k6eAd1	násilně
zavlečen	zavlečen	k2eAgInSc1d1	zavlečen
do	do	k7c2	do
rakouského	rakouský	k2eAgInSc2d1	rakouský
Hainburgu	Hainburg	k1gInSc2	Hainburg
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
provedené	provedený	k2eAgFnSc2d1	provedená
akce	akce	k1gFnSc2	akce
byla	být	k5eAaImAgFnS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
informační	informační	k2eAgFnSc1d1	informační
služba	služba	k1gFnSc1	služba
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ivana	Ivan	k1gMnSc2	Ivan
Lexy	Lexa	k1gMnSc2	Lexa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Únos	únos	k1gInSc4	únos
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
policejního	policejní	k2eAgNnSc2d1	policejní
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
důstojník	důstojník	k1gMnSc1	důstojník
SIS	SIS	kA	SIS
Ján	Ján	k1gMnSc1	Ján
Takáč	Takáč	k1gMnSc1	Takáč
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1995	[number]	k4	1995
odpoledne	odpoledne	k6eAd1	odpoledne
cestoval	cestovat	k5eAaImAgMnS	cestovat
do	do	k7c2	do
Hainburgu	Hainburg	k1gInSc2	Hainburg
<g/>
.	.	kIx.	.
</s>
<s>
Telefonicky	telefonicky	k6eAd1	telefonicky
(	(	kIx(	(
<g/>
a	a	k8xC	a
anonymně	anonymně	k6eAd1	anonymně
<g/>
)	)	kIx)	)
oznámil	oznámit	k5eAaPmAgMnS	oznámit
rakouské	rakouský	k2eAgFnSc3d1	rakouská
policii	policie	k1gFnSc3	policie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
před	před	k7c7	před
hainburskou	hainburský	k2eAgFnSc7d1	hainburský
policejní	policejní	k2eAgFnSc7d1	policejní
stanicí	stanice	k1gFnSc7	stanice
v	v	k7c6	v
zaparkovaném	zaparkovaný	k2eAgInSc6d1	zaparkovaný
Mercedesu	mercedes	k1gInSc6	mercedes
nachází	nacházet	k5eAaImIp3nS	nacházet
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yIgMnSc4	který
je	být	k5eAaImIp3nS	být
vydán	vydán	k2eAgInSc1d1	vydán
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
zatykač	zatykač	k1gInSc1	zatykač
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
uneseného	unesený	k2eAgMnSc4d1	unesený
<g/>
,	,	kIx,	,
zbitého	zbitý	k2eAgMnSc4d1	zbitý
a	a	k8xC	a
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
opilého	opilý	k2eAgMnSc2d1	opilý
Michala	Michal	k1gMnSc2	Michal
Kováče	Kováč	k1gMnSc2	Kováč
mladšího	mladý	k2eAgMnSc2d2	mladší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
únosu	únos	k1gInSc2	únos
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zřejmě	zřejmě	k6eAd1	zřejmě
odstoupení	odstoupení	k1gNnSc4	odstoupení
prezidenta	prezident	k1gMnSc2	prezident
Michala	Michal	k1gMnSc2	Michal
Kováče	Kováč	k1gMnSc2	Kováč
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
snahou	snaha	k1gFnSc7	snaha
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vlády	vláda	k1gFnSc2	vláda
kvůli	kvůli	k7c3	kvůli
neustálým	neustálý	k2eAgInPc3d1	neustálý
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
ní	on	k3xPp3gFnSc7	on
a	a	k8xC	a
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
však	však	k9	však
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ukončení	ukončení	k1gNnSc2	ukončení
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
prezidenta	prezident	k1gMnSc2	prezident
Kováče	Kováč	k1gMnSc2	Kováč
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zvolit	zvolit	k5eAaPmF	zvolit
nového	nový	k2eAgMnSc4d1	nový
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
po	po	k7c6	po
3	[number]	k4	3
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc6	březen
1998	[number]	k4	1998
převzala	převzít	k5eAaPmAgFnS	převzít
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnPc2	jeho
kompetencí	kompetence	k1gFnPc2	kompetence
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
premiér	premiér	k1gMnSc1	premiér
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mečiar	Mečiar	k1gMnSc1	Mečiar
využil	využít	k5eAaPmAgMnS	využít
získané	získaný	k2eAgFnSc2d1	získaná
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
pravomoci	pravomoc	k1gFnSc2	pravomoc
a	a	k8xC	a
udělil	udělit	k5eAaPmAgInS	udělit
amnestii	amnestie	k1gFnSc4	amnestie
pro	pro	k7c4	pro
pachatele	pachatel	k1gMnSc4	pachatel
únosu	únos	k1gInSc2	únos
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1998	[number]	k4	1998
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Mečiar	Mečiar	k1gMnSc1	Mečiar
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
amnestii	amnestie	k1gFnSc4	amnestie
na	na	k7c4	na
stejné	stejný	k2eAgInPc4d1	stejný
skutky	skutek	k1gInPc4	skutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dohra	dohra	k1gFnSc1	dohra
==	==	k?	==
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
amnestie	amnestie	k1gFnPc1	amnestie
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
několika	několik	k4yIc7	několik
výměnami	výměna	k1gFnPc7	výměna
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
způsobily	způsobit	k5eAaPmAgInP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kauza	kauza	k1gFnSc1	kauza
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
zcela	zcela	k6eAd1	zcela
vyšetřena	vyšetřen	k2eAgFnSc1d1	vyšetřena
a	a	k8xC	a
právně	právně	k6eAd1	právně
dotažena	dotáhnout	k5eAaPmNgFnS	dotáhnout
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2002	[number]	k4	2002
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
SR	SR	kA	SR
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
stížnost	stížnost	k1gFnSc4	stížnost
Generální	generální	k2eAgFnSc2d1	generální
prokuratury	prokuratura	k1gFnSc2	prokuratura
proti	proti	k7c3	proti
zastavení	zastavení	k1gNnSc3	zastavení
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Odvolání	odvolání	k1gNnPc1	odvolání
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Národní	národní	k2eAgFnSc6d1	národní
radě	rada	k1gFnSc6	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
několik	několik	k4yIc4	několik
hlasování	hlasování	k1gNnPc2	hlasování
o	o	k7c6	o
zrušení	zrušení	k1gNnSc6	zrušení
tzv.	tzv.	kA	tzv.
Mečiarových	Mečiarův	k2eAgFnPc2d1	Mečiarova
amnestií	amnestie	k1gFnPc2	amnestie
<g/>
,	,	kIx,	,
platných	platný	k2eAgFnPc2d1	platná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
v	v	k7c6	v
r.	r.	kA	r.
2002	[number]	k4	2002
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
78	[number]	k4	78
poslanců	poslanec	k1gMnPc2	poslanec
koalice	koalice	k1gFnSc2	koalice
vlády	vláda	k1gFnSc2	vláda
Ivety	Iveta	k1gFnSc2	Iveta
Radičové	radič	k1gMnPc1	radič
<g/>
,	,	kIx,	,
potřebných	potřebné	k1gNnPc6	potřebné
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
90	[number]	k4	90
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
slovenského	slovenský	k2eAgMnSc2d1	slovenský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Dominika	Dominik	k1gMnSc2	Dominik
Dána	Dán	k1gMnSc2	Dán
Popel	popel	k1gInSc1	popel
všechny	všechen	k3xTgMnPc4	všechen
zarovná	zarovnat	k5eAaPmIp3nS	zarovnat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
nebyla	být	k5eNaImAgFnS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k8xS	jako
popis	popis	k1gInSc1	popis
skutečného	skutečný	k2eAgInSc2d1	skutečný
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kauza	kauza	k1gFnSc1	kauza
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Únos	únos	k1gInSc1	únos
Mariany	Mariana	k1gFnSc2	Mariana
Čengel	Čengela	k1gFnPc2	Čengela
Solčanské	Solčanský	k2eAgFnSc2d1	Solčanská
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Róbert	Róbert	k1gMnSc1	Róbert
Remiáš	Remiáš	k1gMnSc1	Remiáš
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zavlečenie	Zavlečenie	k1gFnSc1	Zavlečenie
Michala	Michala	k1gFnSc1	Michala
Kováča	Kováča	k?	Kováča
ml.	ml.	kA	ml.
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
