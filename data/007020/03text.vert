<s>
Digitální	digitální	k2eAgInSc1d1	digitální
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
je	být	k5eAaImIp3nS	být
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
<g/>
,	,	kIx,	,
zaznamenávající	zaznamenávající	k2eAgInSc1d1	zaznamenávající
obraz	obraz	k1gInSc1	obraz
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
okamžitě	okamžitě	k6eAd1	okamžitě
zobrazen	zobrazit	k5eAaPmNgInS	zobrazit
na	na	k7c6	na
zabudovaném	zabudovaný	k2eAgInSc6d1	zabudovaný
displeji	displej	k1gInSc6	displej
nebo	nebo	k8xC	nebo
nahrán	nahrát	k5eAaPmNgMnS	nahrát
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
dominují	dominovat	k5eAaImIp3nP	dominovat
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
funkcí	funkce	k1gFnSc7	funkce
digitálního	digitální	k2eAgInSc2d1	digitální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
je	být	k5eAaImIp3nS	být
snímání	snímání	k1gNnSc4	snímání
statických	statický	k2eAgInPc2d1	statický
obrazů	obraz	k1gInPc2	obraz
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tzv.	tzv.	kA	tzv.
digitální	digitální	k2eAgFnSc2d1	digitální
fotografie	fotografia	k1gFnSc2	fotografia
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
tak	tak	k9	tak
jejich	jejich	k3xOp3gNnSc4	jejich
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pomocí	pomocí	k7c2	pomocí
běžného	běžný	k2eAgInSc2d1	běžný
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc4	jejich
tisk	tisk	k1gInSc4	tisk
či	či	k8xC	či
vyvolání	vyvolání	k1gNnSc4	vyvolání
speciální	speciální	k2eAgFnSc7d1	speciální
osvitovou	osvitový	k2eAgFnSc7d1	osvitová
jednotkou	jednotka	k1gFnSc7	jednotka
do	do	k7c2	do
výsledné	výsledný	k2eAgFnSc2d1	výsledná
podoby	podoba	k1gFnSc2	podoba
jako	jako	k8xS	jako
u	u	k7c2	u
klasické	klasický	k2eAgFnSc2d1	klasická
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
nabízí	nabízet	k5eAaImIp3nS	nabízet
kromě	kromě	k7c2	kromě
své	svůj	k3xOyFgFnSc2	svůj
základní	základní	k2eAgFnSc2d1	základní
funkce	funkce	k1gFnSc2	funkce
také	také	k9	také
řadu	řada	k1gFnSc4	řada
další	další	k2eAgFnSc4d1	další
doplňujících	doplňující	k2eAgFnPc2d1	doplňující
a	a	k8xC	a
rozšiřujících	rozšiřující	k2eAgFnPc2d1	rozšiřující
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
souvisejí	souviset	k5eAaImIp3nP	souviset
ať	ať	k9	ať
už	už	k6eAd1	už
přímo	přímo	k6eAd1	přímo
či	či	k8xC	či
nepřímo	přímo	k6eNd1	přímo
se	s	k7c7	s
zpracovávanými	zpracovávaný	k2eAgNnPc7d1	zpracovávané
obrazovými	obrazový	k2eAgNnPc7d1	obrazové
daty	datum	k1gNnPc7	datum
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
tak	tak	k9	tak
dokáží	dokázat	k5eAaPmIp3nP	dokázat
kromě	kromě	k7c2	kromě
obrazu	obraz	k1gInSc2	obraz
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
i	i	k9	i
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
scény	scéna	k1gFnPc1	scéna
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
videa	video	k1gNnSc2	video
nebo	nebo	k8xC	nebo
zvukový	zvukový	k2eAgInSc4d1	zvukový
záznam	záznam	k1gInSc4	záznam
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ozvučeného	ozvučený	k2eAgNnSc2d1	ozvučené
videa	video	k1gNnSc2	video
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
poznámky	poznámka	k1gFnPc4	poznámka
k	k	k7c3	k
pořízeným	pořízený	k2eAgInPc3d1	pořízený
snímkům	snímek	k1gInPc3	snímek
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
digitálního	digitální	k2eAgInSc2d1	digitální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
konstrukce	konstrukce	k1gFnSc2	konstrukce
klasického	klasický	k2eAgInSc2d1	klasický
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Jádrem	jádro	k1gNnSc7	jádro
přístroje	přístroj	k1gInSc2	přístroj
je	být	k5eAaImIp3nS	být
světlocitlivá	světlocitlivý	k2eAgFnSc1d1	světlocitlivá
plocha	plocha	k1gFnSc1	plocha
snímače	snímač	k1gInSc2	snímač
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
technologie	technologie	k1gFnSc2	technologie
CCD	CCD	kA	CCD
nebo	nebo	k8xC	nebo
CMOS	CMOS	kA	CMOS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
plochu	plocha	k1gFnSc4	plocha
senzoru	senzor	k1gInSc2	senzor
je	být	k5eAaImIp3nS	být
promítán	promítán	k2eAgInSc4d1	promítán
obraz	obraz	k1gInSc4	obraz
přes	přes	k7c4	přes
systém	systém	k1gInSc4	systém
optických	optický	k2eAgFnPc2d1	optická
čoček	čočka	k1gFnPc2	čočka
v	v	k7c6	v
objektivu	objektiv	k1gInSc6	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Světelná	světelný	k2eAgFnSc1d1	světelná
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přichází	přicházet	k5eAaImIp3nS	přicházet
ze	z	k7c2	z
snímaného	snímaný	k2eAgInSc2d1	snímaný
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
scény	scéna	k1gFnSc2	scéna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
pixelech	pixel	k1gInPc6	pixel
(	(	kIx(	(
<g/>
obrazových	obrazový	k2eAgInPc6d1	obrazový
bodech	bod	k1gInPc6	bod
<g/>
)	)	kIx)	)
převáděna	převáděn	k2eAgFnSc1d1	převáděna
na	na	k7c4	na
elektrický	elektrický	k2eAgInSc4d1	elektrický
signál	signál	k1gInSc4	signál
a	a	k8xC	a
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vázaného	vázaný	k2eAgInSc2d1	vázaný
náboje	náboj	k1gInSc2	náboj
(	(	kIx(	(
<g/>
u	u	k7c2	u
technologie	technologie	k1gFnSc2	technologie
CCD	CCD	kA	CCD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Náboj	náboj	k1gInSc1	náboj
vzniká	vznikat	k5eAaImIp3nS	vznikat
postupně	postupně	k6eAd1	postupně
během	během	k7c2	během
expozice	expozice	k1gFnSc2	expozice
čipu	čip	k1gInSc2	čip
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
uzávěrka	uzávěrka	k1gFnSc1	uzávěrka
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
a	a	k8xC	a
světlo	světlo	k1gNnSc1	světlo
může	moct	k5eAaImIp3nS	moct
dopadat	dopadat	k5eAaImF	dopadat
na	na	k7c4	na
čip	čip	k1gInSc4	čip
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
vzniku	vznik	k1gInSc2	vznik
elektrického	elektrický	k2eAgInSc2d1	elektrický
náboje	náboj	k1gInSc2	náboj
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
fotoelektrickém	fotoelektrický	k2eAgInSc6d1	fotoelektrický
jevu	jev	k1gInSc6	jev
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboje	náboj	k1gInPc1	náboj
neodtékají	odtékat	k5eNaImIp3nP	odtékat
okamžitě	okamžitě	k6eAd1	okamžitě
do	do	k7c2	do
vnějšího	vnější	k2eAgInSc2d1	vnější
obvodu	obvod	k1gInSc2	obvod
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
izolovány	izolovat	k5eAaBmNgFnP	izolovat
v	v	k7c6	v
nábojových	nábojový	k2eAgInPc6d1	nábojový
zásobnících	zásobník	k1gInPc6	zásobník
v	v	k7c6	v
elektricky	elektricky	k6eAd1	elektricky
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
struktuře	struktura	k1gFnSc6	struktura
čipu	čip	k1gInSc2	čip
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
uzávěrky	uzávěrka	k1gFnSc2	uzávěrka
jsou	být	k5eAaImIp3nP	být
vygenerované	vygenerovaný	k2eAgInPc1d1	vygenerovaný
náboje	náboj	k1gInPc1	náboj
z	z	k7c2	z
čipu	čip	k1gInSc2	čip
postupně	postupně	k6eAd1	postupně
odváděny	odváděn	k2eAgMnPc4d1	odváděn
a	a	k8xC	a
měřeny	měřen	k2eAgMnPc4d1	měřen
speciálním	speciální	k2eAgInSc7d1	speciální
zesilovačem	zesilovač	k1gInSc7	zesilovač
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
jednotlivý	jednotlivý	k2eAgInSc4d1	jednotlivý
pixel	pixel	k1gInSc4	pixel
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
získaný	získaný	k2eAgInSc1d1	získaný
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
převeden	převést	k5eAaPmNgInS	převést
AD	ad	k7c4	ad
převodníkem	převodník	k1gMnSc7	převodník
na	na	k7c4	na
signál	signál	k1gInSc4	signál
v	v	k7c6	v
binárním	binární	k2eAgInSc6d1	binární
kódu	kód	k1gInSc6	kód
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
datový	datový	k2eAgInSc1d1	datový
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pomocí	pomocí	k7c2	pomocí
mikroprocesoru	mikroprocesor	k1gInSc2	mikroprocesor
různě	různě	k6eAd1	různě
upravován	upravovat	k5eAaImNgInS	upravovat
a	a	k8xC	a
převeden	převést	k5eAaPmNgInS	převést
do	do	k7c2	do
některého	některý	k3yIgInSc2	některý
grafického	grafický	k2eAgInSc2d1	grafický
formátu	formát	k1gInSc2	formát
používaného	používaný	k2eAgInSc2d1	používaný
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
obrazových	obrazový	k2eAgNnPc2d1	obrazové
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
např.	např.	kA	např.
raw	raw	k?	raw
<g/>
,	,	kIx,	,
JPEG	JPEG	kA	JPEG
nebo	nebo	k8xC	nebo
TIFF	TIFF	kA	TIFF
<g/>
.	.	kIx.	.
</s>
<s>
Výsledný	výsledný	k2eAgInSc1d1	výsledný
datový	datový	k2eAgInSc1d1	datový
soubor	soubor	k1gInSc1	soubor
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
paměťové	paměťový	k2eAgNnSc4d1	paměťové
médium	médium	k1gNnSc4	médium
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
paměťové	paměťový	k2eAgFnSc2d1	paměťová
karty	karta	k1gFnSc2	karta
nebo	nebo	k8xC	nebo
vestavěné	vestavěný	k2eAgFnSc2d1	vestavěná
paměti	paměť	k1gFnSc2	paměť
typu	typ	k1gInSc2	typ
Flash-EEPROM	Flash-EEPROM	k1gMnPc1	Flash-EEPROM
tj.	tj.	kA	tj.
elektricky	elektricky	k6eAd1	elektricky
mazatelná	mazatelný	k2eAgFnSc1d1	mazatelná
paměť	paměť	k1gFnSc1	paměť
s	s	k7c7	s
trvalým	trvalý	k2eAgInSc7d1	trvalý
záznamem	záznam	k1gInSc7	záznam
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
uchován	uchován	k2eAgInSc1d1	uchován
i	i	k9	i
bez	bez	k7c2	bez
přívodu	přívod	k1gInSc2	přívod
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dokážou	dokázat	k5eAaPmIp3nP	dokázat
fotografie	fotografia	k1gFnPc4	fotografia
nebo	nebo	k8xC	nebo
videosekvence	videosekvence	k1gFnPc4	videosekvence
přímo	přímo	k6eAd1	přímo
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
na	na	k7c6	na
CD	CD	kA	CD
nebo	nebo	k8xC	nebo
magnetické	magnetický	k2eAgFnSc2d1	magnetická
pásky	páska	k1gFnSc2	páska
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
spíše	spíše	k9	spíše
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
digitálních	digitální	k2eAgFnPc2d1	digitální
videokamer	videokamera	k1gFnPc2	videokamera
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
používají	používat	k5eAaImIp3nP	používat
digitální	digitální	k2eAgInPc1d1	digitální
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
se	s	k7c7	s
snímači	snímač	k1gInPc7	snímač
umožňujícími	umožňující	k2eAgInPc7d1	umožňující
pořizovat	pořizovat	k5eAaImF	pořizovat
fotografie	fotografia	k1gFnPc1	fotografia
barevné	barevný	k2eAgFnPc1d1	barevná
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
tzv.	tzv.	kA	tzv.
Bayerova	Bayerův	k2eAgFnSc1d1	Bayerova
maska	maska	k1gFnSc1	maska
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
každých	každý	k3xTgFnPc2	každý
čtyř	čtyři	k4xCgFnPc2	čtyři
buněk	buňka	k1gFnPc2	buňka
snímače	snímač	k1gInSc2	snímač
dva	dva	k4xCgInPc4	dva
překryty	překryt	k2eAgMnPc4d1	překryt
zeleným	zelený	k2eAgInSc7d1	zelený
filtrem	filtr	k1gInSc7	filtr
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
červeným	červený	k2eAgNnSc7d1	červené
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
modrým	modrý	k2eAgInSc7d1	modrý
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
dáno	dán	k2eAgNnSc1d1	dáno
návazností	návaznost	k1gFnSc7	návaznost
na	na	k7c4	na
spektrální	spektrální	k2eAgFnSc4d1	spektrální
citlivost	citlivost	k1gFnSc4	citlivost
lidského	lidský	k2eAgInSc2d1	lidský
zraku	zrak	k1gInSc2	zrak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zelené	zelený	k2eAgFnPc4d1	zelená
barvy	barva	k1gFnPc4	barva
nejcitlivější	citlivý	k2eAgFnPc4d3	nejcitlivější
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
čtyřmegapixelový	čtyřmegapixelový	k2eAgInSc1d1	čtyřmegapixelový
snímač	snímač	k1gInSc1	snímač
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
bodů	bod	k1gInPc2	bod
citlivých	citlivý	k2eAgInPc2d1	citlivý
na	na	k7c4	na
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
milionu	milion	k4xCgInSc2	milion
bodů	bod	k1gInPc2	bod
citlivých	citlivý	k2eAgInPc2d1	citlivý
na	na	k7c4	na
červenou	červený	k2eAgFnSc4d1	červená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
barevná	barevný	k2eAgFnSc1d1	barevná
informace	informace	k1gFnSc1	informace
se	se	k3xPyFc4	se
ve	v	k7c6	v
výsledném	výsledný	k2eAgInSc6d1	výsledný
snímku	snímek	k1gInSc6	snímek
dopočítává	dopočítávat	k5eAaImIp3nS	dopočítávat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
senzory	senzor	k1gInPc1	senzor
Foveon	Foveona	k1gFnPc2	Foveona
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
pronikání	pronikání	k1gNnSc2	pronikání
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
vlnových	vlnový	k2eAgFnPc6d1	vlnová
délkách	délka	k1gFnPc6	délka
do	do	k7c2	do
různé	různý	k2eAgFnSc2d1	různá
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
pixel	pixel	k1gInSc1	pixel
tedy	tedy	k9	tedy
má	mít	k5eAaImIp3nS	mít
zaznamenány	zaznamenán	k2eAgFnPc4d1	zaznamenána
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
třech	tři	k4xCgFnPc6	tři
barvách	barva	k1gFnPc6	barva
a	a	k8xC	a
interpolace	interpolace	k1gFnSc2	interpolace
tedy	tedy	k9	tedy
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
mást	mást	k5eAaImF	mást
rozlišení	rozlišení	k1gNnSc1	rozlišení
−	−	k?	−
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
jej	on	k3xPp3gMnSc4	on
vydělit	vydělit	k5eAaPmF	vydělit
třemi	tři	k4xCgNnPc7	tři
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
alternativním	alternativní	k2eAgInSc7d1	alternativní
typem	typ	k1gInSc7	typ
senzorů	senzor	k1gInPc2	senzor
je	být	k5eAaImIp3nS	být
Super	super	k2eAgInSc1d1	super
CCD	CCD	kA	CCD
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
čtvercovou	čtvercový	k2eAgFnSc4d1	čtvercová
síť	síť	k1gFnSc4	síť
otočenou	otočený	k2eAgFnSc4d1	otočená
o	o	k7c4	o
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
Super	super	k1gInSc1	super
CCD	CCD	kA	CCD
EXR	EXR	kA	EXR
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
:	:	kIx,	:
lidské	lidský	k2eAgNnSc4d1	lidské
oko	oko	k1gNnSc4	oko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cca	cca	kA	cca
6-8	[number]	k4	6-8
milionů	milion	k4xCgInPc2	milion
buněk	buňka	k1gFnPc2	buňka
citlivých	citlivý	k2eAgInPc2d1	citlivý
na	na	k7c4	na
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
čípků	čípek	k1gInPc2	čípek
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
buněk	buňka	k1gFnPc2	buňka
citlivých	citlivý	k2eAgInPc2d1	citlivý
na	na	k7c4	na
jas	jas	k1gInSc4	jas
(	(	kIx(	(
<g/>
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
barevného	barevný	k2eAgNnSc2d1	barevné
rozlišení	rozlišení	k1gNnSc2	rozlišení
tak	tak	k9	tak
digitální	digitální	k2eAgInPc4d1	digitální
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
lidské	lidský	k2eAgNnSc4d1	lidské
oko	oko	k1gNnSc4	oko
prakticky	prakticky	k6eAd1	prakticky
překonaly	překonat	k5eAaPmAgFnP	překonat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
fotoaparáty	fotoaparát	k1gInPc7	fotoaparát
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
dynamického	dynamický	k2eAgInSc2d1	dynamický
rozsahu	rozsah	k1gInSc2	rozsah
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
digitální	digitální	k2eAgInPc4d1	digitální
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
přinesl	přinést	k5eAaPmAgInS	přinést
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
výzkum	výzkum	k1gInSc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Snímkovací	snímkovací	k2eAgInPc1d1	snímkovací
přístroje	přístroj	k1gInPc1	přístroj
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
bezobslužných	bezobslužný	k2eAgFnPc6d1	bezobslužná
vesmírných	vesmírný	k2eAgFnPc6d1	vesmírná
sondách	sonda	k1gFnPc6	sonda
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pořizovaly	pořizovat	k5eAaImAgFnP	pořizovat
snímky	snímka	k1gFnPc1	snímka
zprvu	zprvu	k6eAd1	zprvu
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
snímek	snímek	k1gInSc4	snímek
nafotografovaly	nafotografovat	k5eAaPmAgInP	nafotografovat
na	na	k7c4	na
černobílý	černobílý	k2eAgInSc4d1	černobílý
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
přístroji	přístroj	k1gInSc6	přístroj
automaticky	automaticky	k6eAd1	automaticky
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
<g/>
,	,	kIx,	,
vyvolaný	vyvolaný	k2eAgInSc1d1	vyvolaný
negativ	negativ	k1gInSc1	negativ
byl	být	k5eAaImAgInS	být
oskenován	oskenovat	k5eAaPmNgInS	oskenovat
a	a	k8xC	a
v	v	k7c6	v
číslicové	číslicový	k2eAgFnSc6d1	číslicová
podobě	podoba	k1gFnSc6	podoba
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
uspořádání	uspořádání	k1gNnSc1	uspořádání
tvořilo	tvořit	k5eAaImAgNnS	tvořit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
hybrid	hybrid	k1gInSc4	hybrid
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
digitální	digitální	k2eAgFnSc2d1	digitální
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Odvysílání	odvysílání	k1gNnSc1	odvysílání
obrazového	obrazový	k2eAgInSc2d1	obrazový
signálu	signál	k1gInSc2	signál
číslicově	číslicově	k6eAd1	číslicově
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
výhodu	výhoda	k1gFnSc4	výhoda
odolnosti	odolnost	k1gFnSc2	odolnost
vůči	vůči	k7c3	vůči
poruchám	porucha	k1gFnPc3	porucha
a	a	k8xC	a
snížení	snížení	k1gNnSc3	snížení
potřebného	potřebný	k2eAgInSc2d1	potřebný
vysílacího	vysílací	k2eAgInSc2d1	vysílací
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
šetření	šetření	k1gNnSc1	šetření
omezenými	omezený	k2eAgInPc7d1	omezený
energetickými	energetický	k2eAgInPc7d1	energetický
zdroji	zdroj	k1gInPc7	zdroj
sondy	sonda	k1gFnSc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
vesmírných	vesmírný	k2eAgFnPc2d1	vesmírná
sond	sonda	k1gFnPc2	sonda
vybavovány	vybavován	k2eAgInPc4d1	vybavován
černobílými	černobílý	k2eAgInPc7d1	černobílý
i	i	k8xC	i
barevnými	barevný	k2eAgInPc7d1	barevný
obrazovými	obrazový	k2eAgInPc7d1	obrazový
snímači	snímač	k1gInPc7	snímač
se	s	k7c7	s
snímacími	snímací	k2eAgFnPc7d1	snímací
elektronkami	elektronka	k1gFnPc7	elektronka
<g/>
.	.	kIx.	.
</s>
<s>
Analogový	analogový	k2eAgInSc1d1	analogový
signál	signál	k1gInSc1	signál
přicházející	přicházející	k2eAgInSc1d1	přicházející
ze	z	k7c2	z
snímače	snímač	k1gInSc2	snímač
během	během	k7c2	během
expozice	expozice	k1gFnSc2	expozice
snímku	snímek	k1gInSc2	snímek
je	být	k5eAaImIp3nS	být
převáděn	převádět	k5eAaImNgInS	převádět
na	na	k7c4	na
digitální	digitální	k2eAgInSc4d1	digitální
a	a	k8xC	a
odvysíláván	odvysíláván	k2eAgInSc4d1	odvysíláván
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
přímého	přímý	k2eAgNnSc2d1	přímé
elektronického	elektronický	k2eAgNnSc2d1	elektronické
sejmutí	sejmutí	k1gNnSc2	sejmutí
obrazu	obraz	k1gInSc2	obraz
a	a	k8xC	a
převedení	převedení	k1gNnSc1	převedení
obrazového	obrazový	k2eAgInSc2d1	obrazový
signálu	signál	k1gInSc2	signál
do	do	k7c2	do
číslicové	číslicový	k2eAgFnSc2d1	číslicová
podoby	podoba	k1gFnSc2	podoba
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
již	již	k6eAd1	již
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
u	u	k7c2	u
novodobých	novodobý	k2eAgInPc2d1	novodobý
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc4	první
prototyp	prototyp	k1gInSc4	prototyp
přenosného	přenosný	k2eAgInSc2d1	přenosný
digitálního	digitální	k2eAgInSc2d1	digitální
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
Steve	Steve	k1gMnSc1	Steve
Sasson	Sasson	k1gMnSc1	Sasson
<g/>
,	,	kIx,	,
inženýr	inženýr	k1gMnSc1	inženýr
firmy	firma	k1gFnSc2	firma
Kodak	Kodak	kA	Kodak
<g/>
.	.	kIx.	.
</s>
<s>
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
černobílý	černobílý	k2eAgInSc1d1	černobílý
obrazový	obrazový	k2eAgInSc1d1	obrazový
snímač	snímač	k1gInSc1	snímač
CCD	CCD	kA	CCD
o	o	k7c6	o
rozlišení	rozlišení	k1gNnSc6	rozlišení
0,01	[number]	k4	0,01
megapixelu	megapixel	k1gInSc2	megapixel
<g/>
.	.	kIx.	.
</s>
<s>
Získaný	získaný	k2eAgInSc1d1	získaný
analogový	analogový	k2eAgInSc1d1	analogový
signál	signál	k1gInSc1	signál
ze	z	k7c2	z
snímače	snímač	k1gInSc2	snímač
byl	být	k5eAaImAgInS	být
převeden	převést	k5eAaPmNgInS	převést
na	na	k7c4	na
číslicový	číslicový	k2eAgInSc4d1	číslicový
pomocí	pomocí	k7c2	pomocí
analogově-číslicového	analogově-číslicový	k2eAgInSc2d1	analogově-číslicový
převodníku	převodník	k1gInSc2	převodník
z	z	k7c2	z
digitálního	digitální	k2eAgInSc2d1	digitální
voltmetru	voltmetr	k1gInSc2	voltmetr
a	a	k8xC	a
následně	následně	k6eAd1	následně
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
na	na	k7c4	na
magnetofonovou	magnetofonový	k2eAgFnSc4d1	magnetofonová
kazetu	kazeta	k1gFnSc4	kazeta
<g/>
.	.	kIx.	.
</s>
<s>
Fotopřístroj	fotopřístroj	k1gInSc1	fotopřístroj
byl	být	k5eAaImAgInS	být
napájen	napájet	k5eAaImNgInS	napájet
16	[number]	k4	16
akumulátory	akumulátor	k1gInPc7	akumulátor
NiCd	NiCda	k1gFnPc2	NiCda
<g/>
.	.	kIx.	.
</s>
<s>
Sejmutí	sejmutí	k1gNnSc4	sejmutí
a	a	k8xC	a
uložení	uložení	k1gNnSc4	uložení
jedné	jeden	k4xCgFnSc2	jeden
fotografie	fotografia	k1gFnSc2	fotografia
trvalo	trvat	k5eAaImAgNnS	trvat
23	[number]	k4	23
sekundy	sekunda	k1gFnPc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Výsledné	výsledný	k2eAgInPc1d1	výsledný
snímky	snímek	k1gInPc1	snímek
se	se	k3xPyFc4	se
z	z	k7c2	z
kazety	kazeta	k1gFnSc2	kazeta
promítaly	promítat	k5eAaImAgInP	promítat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
počítače	počítač	k1gInSc2	počítač
na	na	k7c4	na
televizní	televizní	k2eAgFnSc4d1	televizní
obrazovku	obrazovka	k1gFnSc4	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
vážil	vážit	k5eAaImAgInS	vážit
asi	asi	k9	asi
4	[number]	k4	4
kg	kg	kA	kg
a	a	k8xC	a
měřil	měřit	k5eAaImAgInS	měřit
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
×	×	k?	×
26	[number]	k4	26
×	×	k?	×
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
komerčně	komerčně	k6eAd1	komerčně
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zaznamenával	zaznamenávat	k5eAaImAgInS	zaznamenávat
snímky	snímek	k1gInPc4	snímek
do	do	k7c2	do
počítačových	počítačový	k2eAgInPc2d1	počítačový
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Fuji	Fuji	kA	Fuji
DS-	DS-	k1gFnSc1	DS-
<g/>
1	[number]	k4	1
<g/>
P	P	kA	P
<g/>
,	,	kIx,	,
používající	používající	k2eAgFnPc1d1	používající
16	[number]	k4	16
MB	MB	kA	MB
interní	interní	k2eAgFnSc2d1	interní
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
první	první	k4xOgFnSc1	první
digitální	digitální	k2eAgFnSc1d1	digitální
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
<g/>
,	,	kIx,	,
Kodak	kodak	k1gInSc1	kodak
DCS-	DCS-	k1gFnSc2	DCS-
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
1,3	[number]	k4	1,3
<g/>
megapixelový	megapixelový	k2eAgInSc4d1	megapixelový
snímač	snímač	k1gInSc4	snímač
a	a	k8xC	a
stála	stát	k5eAaImAgFnS	stát
13	[number]	k4	13
000	[number]	k4	000
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Zabudovaný	zabudovaný	k2eAgInSc1d1	zabudovaný
displej	displej	k1gInSc1	displej
přišel	přijít	k5eAaPmAgInS	přijít
s	s	k7c7	s
přístrojem	přístroj	k1gInSc7	přístroj
Casio	Casio	k1gMnSc1	Casio
QV-10	QV-10	k1gMnSc1	QV-10
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
zapisujícím	zapisující	k2eAgInSc7d1	zapisující
na	na	k7c4	na
karty	karta	k1gFnPc4	karta
Compact	Compact	k2eAgInSc1d1	Compact
Flash	Flash	k1gInSc1	Flash
byl	být	k5eAaImAgInS	být
Kodak	Kodak	kA	Kodak
DC-25	DC-25	k1gFnSc2	DC-25
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgInPc1d1	digitální
fotoparáty	fotoparát	k1gInPc1	fotoparát
cílené	cílený	k2eAgInPc1d1	cílený
na	na	k7c4	na
běžné	běžný	k2eAgFnPc4d1	běžná
spotřebitele	spotřebitel	k1gMnSc2	spotřebitel
měly	mít	k5eAaImAgFnP	mít
nejdříve	dříve	k6eAd3	dříve
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgNnSc1d1	nízké
rozlišení	rozlišení	k1gNnSc1	rozlišení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
třídě	třída	k1gFnSc6	třída
byla	být	k5eAaImAgFnS	být
hranice	hranice	k1gFnSc1	hranice
jednoho	jeden	k4xCgInSc2	jeden
megapixelu	megapixel	k1gInSc2	megapixel
prolomena	prolomit	k5eAaPmNgFnS	prolomit
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prvním	první	k4xOgInSc7	první
přístrojem	přístroj	k1gInSc7	přístroj
schopným	schopný	k2eAgInSc7d1	schopný
nahrávat	nahrávat	k5eAaImF	nahrávat
video	video	k1gNnSc1	video
byl	být	k5eAaImAgInS	být
Ricoh	Ricoh	k1gInSc4	Ricoh
RDC-	RDC-	k1gMnPc2	RDC-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
prodávaný	prodávaný	k2eAgInSc1d1	prodávaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
digitálních	digitální	k2eAgFnPc2d1	digitální
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
byl	být	k5eAaImAgInS	být
zlomový	zlomový	k2eAgInSc1d1	zlomový
rok	rok	k1gInSc1	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Nikon	Nikon	k1gMnSc1	Nikon
uvedl	uvést	k5eAaPmAgMnS	uvést
model	model	k1gInSc4	model
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
digitální	digitální	k2eAgFnSc1d1	digitální
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
samostatně	samostatně	k6eAd1	samostatně
tradičním	tradiční	k2eAgMnSc7d1	tradiční
výrobcem	výrobce	k1gMnSc7	výrobce
<g/>
,	,	kIx,	,
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
pod	pod	k7c4	pod
6000	[number]	k4	6000
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
dosažitelné	dosažitelný	k2eAgNnSc1d1	dosažitelné
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgMnPc4d1	profesionální
fotografy	fotograf	k1gMnPc4	fotograf
a	a	k8xC	a
další	další	k2eAgMnPc4d1	další
náročné	náročný	k2eAgMnPc4d1	náročný
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
používala	používat	k5eAaImAgFnS	používat
objektivy	objektiv	k1gInPc4	objektiv
s	s	k7c7	s
bajonetem	bajonet	k1gInSc7	bajonet
Nikon	Nikona	k1gFnPc2	Nikona
F	F	kA	F
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
zákazníci	zákazník	k1gMnPc1	zákazník
Nikonu	Nikona	k1gFnSc4	Nikona
mohli	moct	k5eAaImAgMnP	moct
využít	využít	k5eAaPmF	využít
svou	svůj	k3xOyFgFnSc4	svůj
stávající	stávající	k2eAgFnSc4d1	stávající
výbavu	výbava	k1gFnSc4	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
Canon	Canon	kA	Canon
představil	představit	k5eAaPmAgInS	představit
šestimegapixelový	šestimegapixelový	k2eAgInSc4d1	šestimegapixelový
EOS	EOS	kA	EOS
300	[number]	k4	300
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
digitální	digitální	k2eAgFnSc4d1	digitální
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
s	s	k7c7	s
cenou	cena	k1gFnSc7	cena
pod	pod	k7c4	pod
1000	[number]	k4	1000
$	$	kIx~	$
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
amatéry	amatér	k1gMnPc4	amatér
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgNnSc1d1	maximální
dosažené	dosažený	k2eAgNnSc1d1	dosažené
rozlišení	rozlišení	k1gNnSc1	rozlišení
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
4000	[number]	k4	4000
megapixelů	megapixel	k1gMnPc2	megapixel
<g/>
,	,	kIx,	,
získané	získaný	k2eAgFnPc1d1	získaná
skenováním	skenování	k1gNnSc7	skenování
negativu	negativ	k1gInSc2	negativ
z	z	k7c2	z
velkoformátového	velkoformátový	k2eAgInSc2d1	velkoformátový
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
(	(	kIx(	(
<g/>
s	s	k7c7	s
rozměrem	rozměr	k1gInSc7	rozměr
filmu	film	k1gInSc2	film
24	[number]	k4	24
×	×	k?	×
36	[number]	k4	36
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
kategorií	kategorie	k1gFnPc2	kategorie
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
nároky	nárok	k1gInPc4	nárok
a	a	k8xC	a
vyspělost	vyspělost	k1gFnSc4	vyspělost
cílového	cílový	k2eAgMnSc2d1	cílový
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
:	:	kIx,	:
Amatérské	amatérský	k2eAgInPc4d1	amatérský
přístroje	přístroj	k1gInPc4	přístroj
(	(	kIx(	(
<g/>
základní	základní	k2eAgFnSc1d1	základní
<g/>
,	,	kIx,	,
vstupní	vstupní	k2eAgFnSc1d1	vstupní
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
entry-level	entryevel	k1gInSc1	entry-level
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
zákaznický	zákaznický	k2eAgInSc1d1	zákaznický
segment	segment	k1gInSc1	segment
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
customer	customer	k1gInSc1	customer
segment	segment	k1gInSc1	segment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přístroje	přístroj	k1gInPc4	přístroj
pro	pro	k7c4	pro
široké	široký	k2eAgFnPc4d1	široká
masy	masa	k1gFnPc4	masa
zákazníků-amatérů	zákazníkůmatér	k1gMnPc2	zákazníků-amatér
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
převažují	převažovat	k5eAaImIp3nP	převažovat
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zákazníci	zákazník	k1gMnPc1	zákazník
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
hledí	hledět	k5eAaImIp3nS	hledět
na	na	k7c4	na
skladnost	skladnost	k1gFnSc4	skladnost
a	a	k8xC	a
mobilitu	mobilita	k1gFnSc4	mobilita
přístroje	přístroj	k1gInSc2	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
segmentu	segment	k1gInSc6	segment
typicky	typicky	k6eAd1	typicky
vládne	vládnout	k5eAaImIp3nS	vládnout
mezi	mezi	k7c7	mezi
výrobci	výrobce	k1gMnPc7	výrobce
velká	velká	k1gFnSc1	velká
konkurence	konkurence	k1gFnSc1	konkurence
a	a	k8xC	a
cenový	cenový	k2eAgInSc1d1	cenový
boj	boj	k1gInSc1	boj
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
snadnost	snadnost	k1gFnSc4	snadnost
použití	použití	k1gNnSc2	použití
přístroje	přístroj	k1gInSc2	přístroj
<g/>
,	,	kIx,	,
uplatnění	uplatnění	k1gNnSc4	uplatnění
tak	tak	k8xC	tak
hodně	hodně	k6eAd1	hodně
získávají	získávat	k5eAaImIp3nP	získávat
automatické	automatický	k2eAgInPc1d1	automatický
režimy	režim	k1gInPc1	režim
a	a	k8xC	a
několik	několik	k4yIc1	několik
předvybraných	předvybraný	k2eAgInPc2d1	předvybraný
režimů	režim	k1gInPc2	režim
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Záměrně	záměrně	k6eAd1	záměrně
nejsou	být	k5eNaImIp3nP	být
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
pokročilé	pokročilý	k2eAgFnPc1d1	pokročilá
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Očekávaným	očekávaný	k2eAgNnSc7d1	očekávané
využitím	využití	k1gNnSc7	využití
jsou	být	k5eAaImIp3nP	být
momentky	momentka	k1gFnPc1	momentka
a	a	k8xC	a
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
běžného	běžný	k2eAgInSc2d1	běžný
života	život	k1gInSc2	život
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilé	pokročilý	k2eAgInPc1d1	pokročilý
přístroje	přístroj	k1gInPc1	přístroj
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnSc1d1	střední
třída	třída	k1gFnSc1	třída
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
mid-range	midange	k1gInSc1	mid-range
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
patří	patřit	k5eAaImIp3nS	patřit
většinou	většina	k1gFnSc7	většina
přístrojům	přístroj	k1gInPc3	přístroj
dražším	drahý	k2eAgInPc3d2	dražší
a	a	k8xC	a
pokročilejším	pokročilý	k2eAgInPc3d2	pokročilejší
než	než	k8xS	než
běžné	běžný	k2eAgInPc1d1	běžný
kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
fotopřístroje	fotopřístroj	k1gInPc1	fotopřístroj
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
s	s	k7c7	s
kvalitnějším	kvalitní	k2eAgInSc7d2	kvalitnější
transfokátorem	transfokátor	k1gInSc7	transfokátor
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
však	však	k9	však
není	být	k5eNaImIp3nS	být
vyměnitelný	vyměnitelný	k2eAgInSc1d1	vyměnitelný
<g/>
.	.	kIx.	.
</s>
<s>
Cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
fotografičtí	fotografický	k2eAgMnPc1d1	fotografický
nadšenci	nadšenec	k1gMnPc1	nadšenec
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
nestačí	stačit	k5eNaBmIp3nP	stačit
kompakty	kompakt	k1gInPc1	kompakt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevyžadují	vyžadovat	k5eNaImIp3nP	vyžadovat
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
zrcadlovky	zrcadlovka	k1gFnPc4	zrcadlovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
segmentu	segment	k1gInSc6	segment
nevládne	vládnout	k5eNaImIp3nS	vládnout
tak	tak	k6eAd1	tak
lítý	lítý	k2eAgInSc1d1	lítý
konkurenční	konkurenční	k2eAgInSc1d1	konkurenční
boj	boj	k1gInSc1	boj
jako	jako	k8xC	jako
u	u	k7c2	u
vstupní	vstupní	k2eAgFnSc2d1	vstupní
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákazníci	zákazník	k1gMnPc1	zákazník
za	za	k7c4	za
lepší	dobrý	k2eAgFnPc4d2	lepší
vlastnosti	vlastnost	k1gFnPc4	vlastnost
přístroje	přístroj	k1gInSc2	přístroj
(	(	kIx(	(
<g/>
optiku	optika	k1gFnSc4	optika
<g/>
,	,	kIx,	,
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
budou	být	k5eAaImBp3nP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
sem	sem	k6eAd1	sem
spadat	spadat	k5eAaImF	spadat
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
nejlevnější	levný	k2eAgFnPc1d3	nejlevnější
digitální	digitální	k2eAgFnPc1d1	digitální
zrcadlovky	zrcadlovka	k1gFnPc1	zrcadlovka
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
zhoršenými	zhoršený	k2eAgInPc7d1	zhoršený
parametry	parametr	k1gInPc7	parametr
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
přístroji	přístroj	k1gInPc7	přístroj
vyšších	vysoký	k2eAgFnPc2d2	vyšší
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloprofesionální	poloprofesionální	k2eAgInPc1d1	poloprofesionální
přístroje	přístroj	k1gInPc1	přístroj
(	(	kIx(	(
<g/>
třída	třída	k1gFnSc1	třída
high-end	highnd	k1gMnSc1	high-end
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
segment	segment	k1gInSc4	segment
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
speciální	speciální	k2eAgFnSc1d1	speciální
složenina	složenina	k1gFnSc1	složenina
prosumer	prosumra	k1gFnPc2	prosumra
(	(	kIx(	(
<g/>
z	z	k7c2	z
professional	professionat	k5eAaPmAgMnS	professionat
a	a	k8xC	a
consumer	consumer	k1gMnSc1	consumer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
fotografové	fotograf	k1gMnPc1	fotograf
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
zkušeností	zkušenost	k1gFnSc7	zkušenost
s	s	k7c7	s
filmovými	filmový	k2eAgInPc7d1	filmový
fotoaparáty	fotoaparát	k1gInPc7	fotoaparát
a	a	k8xC	a
nadšením	nadšení	k1gNnSc7	nadšení
pro	pro	k7c4	pro
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc1	produkt
pro	pro	k7c4	pro
tyto	tento	k3xDgMnPc4	tento
zájemce	zájemce	k1gMnPc4	zájemce
většinou	většinou	k6eAd1	většinou
tvořeny	tvořit	k5eAaImNgInP	tvořit
kvalitními	kvalitní	k2eAgFnPc7d1	kvalitní
digitálními	digitální	k2eAgFnPc7d1	digitální
zrcadlovkami	zrcadlovka	k1gFnPc7	zrcadlovka
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílem	rozdíl	k1gInSc7	rozdíl
oproti	oproti	k7c3	oproti
profesionální	profesionální	k2eAgFnSc3d1	profesionální
třídě	třída	k1gFnSc3	třída
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
velikost	velikost	k1gFnSc4	velikost
obrazového	obrazový	k2eAgInSc2d1	obrazový
snímače	snímač	k1gInSc2	snímač
(	(	kIx(	(
<g/>
APS-C	APS-C	k1gFnSc1	APS-C
oproti	oproti	k7c3	oproti
full-frame	fullram	k1gInSc5	full-fram
u	u	k7c2	u
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
přístrojů	přístroj	k1gInPc2	přístroj
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
výrobce	výrobce	k1gMnSc1	výrobce
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
segmentu	segment	k1gInSc6	segment
také	také	k9	také
stále	stále	k6eAd1	stále
hledí	hledět	k5eAaImIp3nS	hledět
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1	profesionální
přístroje	přístroj	k1gInPc1	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Nejdražší	drahý	k2eAgFnSc1d3	nejdražší
a	a	k8xC	a
nejpokročilejší	pokročilý	k2eAgFnSc1d3	nejpokročilejší
skupina	skupina	k1gFnSc1	skupina
fotopřístrojů	fotopřístroj	k1gInPc2	fotopřístroj
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgFnP	určit
pro	pro	k7c4	pro
profesionály	profesionál	k1gMnPc4	profesionál
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
fotografováním	fotografování	k1gNnSc7	fotografování
živí	živit	k5eAaImIp3nS	živit
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
finančně	finančně	k6eAd1	finančně
dovolit	dovolit	k5eAaPmF	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
ty	ten	k3xDgFnPc1	ten
nejlepší	dobrý	k2eAgFnPc1d3	nejlepší
zrcadlovky	zrcadlovka	k1gFnPc1	zrcadlovka
se	se	k3xPyFc4	se
snímačem	snímač	k1gInSc7	snímač
velikosti	velikost	k1gFnSc2	velikost
full-frame	fullram	k1gInSc5	full-fram
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
rozhraním	rozhraní	k1gNnSc7	rozhraní
potřebným	potřebný	k2eAgNnSc7d1	potřebné
pro	pro	k7c4	pro
profesionální	profesionální	k2eAgFnSc4d1	profesionální
fotografii	fotografia	k1gFnSc4	fotografia
(	(	kIx(	(
<g/>
např.	např.	kA	např.
připojení	připojení	k1gNnSc1	připojení
externích	externí	k2eAgInPc2d1	externí
blesků	blesk	k1gInPc2	blesk
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
ovládání	ovládání	k1gNnSc4	ovládání
<g/>
,	,	kIx,	,
samočištění	samočištění	k1gNnSc4	samočištění
obrazového	obrazový	k2eAgInSc2d1	obrazový
snímače	snímač	k1gInSc2	snímač
<g/>
,	,	kIx,	,
různá	různý	k2eAgNnPc4d1	různé
příslušenství	příslušenství	k1gNnPc4	příslušenství
<g/>
,	,	kIx,	,
externí	externí	k2eAgNnPc4d1	externí
spouštění	spouštění	k1gNnPc4	spouštění
<g/>
,	,	kIx,	,
časosběr	časosběr	k1gInSc1	časosběr
<g/>
,	,	kIx,	,
měření	měření	k1gNnSc1	měření
expozice	expozice	k1gFnSc2	expozice
<g/>
;	;	kIx,	;
rozhraní	rozhraní	k1gNnSc1	rozhraní
pro	pro	k7c4	pro
nahrávání	nahrávání	k1gNnSc4	nahrávání
získaných	získaný	k2eAgFnPc2d1	získaná
fotografií	fotografia	k1gFnPc2	fotografia
do	do	k7c2	do
editačního	editační	k2eAgNnSc2d1	editační
pracoviště	pracoviště	k1gNnSc2	pracoviště
za	za	k7c2	za
chodu	chod	k1gInSc2	chod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Clonění	clonění	k1gNnSc1	clonění
a	a	k8xC	a
ostření	ostření	k1gNnSc1	ostření
pomocí	pomocí	k7c2	pomocí
kroužků	kroužek	k1gInPc2	kroužek
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
objektivu	objektiv	k1gInSc2	objektiv
<g/>
;	;	kIx,	;
použití	použití	k1gNnSc1	použití
automatických	automatický	k2eAgInPc2d1	automatický
a	a	k8xC	a
scénických	scénický	k2eAgInPc2d1	scénický
režimů	režim	k1gInPc2	režim
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnPc1	využití
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
profesionální	profesionální	k2eAgFnSc6d1	profesionální
fotografii	fotografia	k1gFnSc6	fotografia
a	a	k8xC	a
postprodukci	postprodukce	k1gFnSc6	postprodukce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
přístrojů	přístroj	k1gInPc2	přístroj
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
prodejnost	prodejnost	k1gFnSc1	prodejnost
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
nepoměrně	poměrně	k6eNd1	poměrně
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kusovou	kusový	k2eAgFnSc4d1	kusová
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
digitálních	digitální	k2eAgInPc2d1	digitální
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
podle	podle	k7c2	podle
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
:	:	kIx,	:
Kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
Kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
kompakty	kompakt	k1gInPc1	kompakt
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
navrhovány	navrhovat	k5eAaImNgInP	navrhovat
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
snadné	snadný	k2eAgFnSc2d1	snadná
obsluhy	obsluha	k1gFnSc2	obsluha
a	a	k8xC	a
co	co	k9	co
nejmenších	malý	k2eAgInPc2d3	nejmenší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Přístroje	přístroj	k1gInPc1	přístroj
prodávané	prodávaný	k2eAgInPc1d1	prodávaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
už	už	k6eAd1	už
zcela	zcela	k6eAd1	zcela
eliminovaly	eliminovat	k5eAaBmAgFnP	eliminovat
hledáček	hledáček	k1gInSc4	hledáček
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
velkých	velký	k2eAgInPc2d1	velký
displejů	displej	k1gInPc2	displej
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Falešné	falešný	k2eAgFnPc1d1	falešná
zrcadlovky	zrcadlovka	k1gFnPc1	zrcadlovka
(	(	kIx(	(
<g/>
EVF	EVF	kA	EVF
<g/>
)	)	kIx)	)
EVF	EVF	kA	EVF
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
z	z	k7c2	z
electronic	electronice	k1gFnPc2	electronice
viewfinder	viewfindra	k1gFnPc2	viewfindra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
tedy	tedy	k9	tedy
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
hledáčku	hledáček	k1gInSc6	hledáček
displej	displej	k1gInSc4	displej
ukazující	ukazující	k2eAgInSc1d1	ukazující
obraz	obraz	k1gInSc1	obraz
z	z	k7c2	z
obrazového	obrazový	k2eAgInSc2d1	obrazový
snímače	snímač	k1gInSc2	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
přístroje	přístroj	k1gInPc1	přístroj
EVF	EVF	kA	EVF
mají	mít	k5eAaImIp3nP	mít
rozlišení	rozlišení	k1gNnSc4	rozlišení
přesahující	přesahující	k2eAgFnSc2d1	přesahující
deset	deset	k4xCc4	deset
megapixelů	megapixel	k1gMnPc2	megapixel
a	a	k8xC	a
zoomový	zoomový	k2eAgInSc4d1	zoomový
objektiv	objektiv	k1gInSc4	objektiv
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
přístroje	přístroj	k1gInPc4	přístroj
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
ultrazoom	ultrazoom	k1gInSc1	ultrazoom
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
těchto	tento	k3xDgInPc2	tento
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
optický	optický	k2eAgInSc4d1	optický
stabilizátor	stabilizátor	k1gInSc4	stabilizátor
obrazu	obraz	k1gInSc2	obraz
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Úhlopříčka	úhlopříčka	k1gFnSc1	úhlopříčka
snímače	snímač	k1gInSc2	snímač
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2,5	[number]	k4	2,5
<g/>
"	"	kIx"	"
až	až	k9	až
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1,6	[number]	k4	1,6
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Bezzrcadlovky	Bezzrcadlovka	k1gFnPc1	Bezzrcadlovka
(	(	kIx(	(
<g/>
též	též	k9	též
systémové	systémový	k2eAgInPc4d1	systémový
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
fotoaparáty	fotoaparát	k1gInPc4	fotoaparát
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Bezzrcadlovka	Bezzrcadlovka	k1gFnSc1	Bezzrcadlovka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kategorie	kategorie	k1gFnSc1	kategorie
přístrojů	přístroj	k1gInPc2	přístroj
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
poskytnout	poskytnout	k5eAaPmF	poskytnout
obrazovou	obrazový	k2eAgFnSc4d1	obrazová
kvalitu	kvalita	k1gFnSc4	kvalita
blízkou	blízký	k2eAgFnSc7d1	blízká
zrcadlovkám	zrcadlovka	k1gFnPc3	zrcadlovka
při	při	k7c6	při
výrazně	výrazně	k6eAd1	výrazně
menších	malý	k2eAgInPc6d2	menší
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
typickými	typický	k2eAgInPc7d1	typický
znaky	znak	k1gInPc7	znak
jsou	být	k5eAaImIp3nP	být
výměnné	výměnný	k2eAgInPc1d1	výměnný
objektivy	objektiv	k1gInPc1	objektiv
<g/>
,	,	kIx,	,
obrazový	obrazový	k2eAgInSc1d1	obrazový
snímač	snímač	k1gInSc1	snímač
s	s	k7c7	s
rozměry	rozměr	k1gInPc7	rozměr
odpovídajícími	odpovídající	k2eAgInPc7d1	odpovídající
zrcadlovkám	zrcadlovka	k1gFnPc3	zrcadlovka
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
mikro	mikro	k6eAd1	mikro
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
nebo	nebo	k8xC	nebo
APS-C	APS-C	k1gMnSc1	APS-C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
absence	absence	k1gFnSc1	absence
zrcátka	zrcátko	k1gNnSc2	zrcátko
a	a	k8xC	a
optického	optický	k2eAgInSc2d1	optický
hledáčku	hledáček	k1gInSc2	hledáček
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
přístrojích	přístroj	k1gInPc6	přístroj
pozorován	pozorovat	k5eAaImNgInS	pozorovat
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
či	či	k8xC	či
v	v	k7c6	v
elektronickém	elektronický	k2eAgInSc6d1	elektronický
hledáčku	hledáček	k1gInSc6	hledáček
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
data	datum	k1gNnPc1	datum
ze	z	k7c2	z
snímače	snímač	k1gInSc2	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Chybějící	chybějící	k2eAgNnSc1d1	chybějící
zrcátko	zrcátko	k1gNnSc1	zrcátko
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zmenšit	zmenšit	k5eAaPmF	zmenšit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
objektivem	objektiv	k1gInSc7	objektiv
a	a	k8xC	a
snímačem	snímač	k1gInSc7	snímač
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
celkové	celkový	k2eAgFnSc2d1	celková
rozměry	rozměra	k1gFnSc2	rozměra
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
rozměrnějším	rozměrný	k2eAgInPc3d2	rozměrnější
kompaktním	kompaktní	k2eAgInPc3d1	kompaktní
fotoaparátům	fotoaparát	k1gInPc3	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
představiteli	představitel	k1gMnPc7	představitel
bezzrcadlovek	bezzrcadlovka	k1gFnPc2	bezzrcadlovka
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
řady	řada	k1gFnSc2	řada
Olympus	Olympus	k1gInSc1	Olympus
PEN	PEN	kA	PEN
<g/>
,	,	kIx,	,
Sony	Sony	kA	Sony
NEX	NEX	kA	NEX
<g/>
,	,	kIx,	,
Nikon	Nikon	k1gInSc1	Nikon
1	[number]	k4	1
či	či	k8xC	či
Canon	Canon	kA	Canon
EF-	EF-	k1gFnSc2	EF-
<g/>
M.	M.	kA	M.
Digitální	digitální	k2eAgFnSc2d1	digitální
zrcadlovky	zrcadlovka	k1gFnSc2	zrcadlovka
(	(	kIx(	(
<g/>
DSLR	DSLR	kA	DSLR
<g/>
)	)	kIx)	)
Digitální	digitální	k2eAgFnSc1d1	digitální
zrcadlovka	zrcadlovka	k1gFnSc1	zrcadlovka
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
vzhledem	vzhled	k1gInSc7	vzhled
a	a	k8xC	a
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
konstrukcí	konstrukce	k1gFnSc7	konstrukce
podobá	podobat	k5eAaImIp3nS	podobat
běžné	běžný	k2eAgFnSc3d1	běžná
zrcadlovce	zrcadlovka	k1gFnSc3	zrcadlovka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
filmu	film	k1gInSc2	film
má	mít	k5eAaImIp3nS	mít
obrazový	obrazový	k2eAgInSc1d1	obrazový
snímač	snímač	k1gInSc1	snímač
CCD	CCD	kA	CCD
nebo	nebo	k8xC	nebo
CMOS	CMOS	kA	CMOS
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
principu	princip	k1gInSc2	princip
konstrukce	konstrukce	k1gFnSc2	konstrukce
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
maximální	maximální	k2eAgFnSc1d1	maximální
věrnost	věrnost	k1gFnSc1	věrnost
zobrazení	zobrazení	k1gNnSc2	zobrazení
v	v	k7c6	v
hledáčku	hledáček	k1gInSc6	hledáček
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
použití	použití	k1gNnSc2	použití
výměnných	výměnný	k2eAgInPc2d1	výměnný
objektivů	objektiv	k1gInPc2	objektiv
a	a	k8xC	a
donedávna	donedávna	k6eAd1	donedávna
nemožnost	nemožnost	k1gFnSc1	nemožnost
natáčet	natáčet	k5eAaImF	natáčet
videosekvence	videosekvence	k1gFnPc4	videosekvence
nebo	nebo	k8xC	nebo
používat	používat	k5eAaImF	používat
displej	displej	k1gInSc4	displej
ke	k	k7c3	k
kompozici	kompozice	k1gFnSc3	kompozice
záběru	záběr	k1gInSc2	záběr
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
současných	současný	k2eAgFnPc2d1	současná
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
již	již	k6eAd1	již
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kompozici	kompozice	k1gFnSc4	kompozice
záběru	záběr	k1gInSc2	záběr
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
(	(	kIx(	(
<g/>
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
tzv.	tzv.	kA	tzv.
live	liv	k1gMnSc2	liv
view	view	k?	view
<g/>
)	)	kIx)	)
i	i	k9	i
natáčení	natáčení	k1gNnSc1	natáčení
videosekvencí	videosekvence	k1gFnPc2	videosekvence
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnPc1d1	digitální
zrcadlovky	zrcadlovka	k1gFnPc1	zrcadlovka
mají	mít	k5eAaImIp3nP	mít
obrazové	obrazový	k2eAgInPc4d1	obrazový
snímače	snímač	k1gInPc4	snímač
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
18	[number]	k4	18
×	×	k?	×
13,5	[number]	k4	13,5
mm	mm	kA	mm
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
rozměry	rozměr	k1gInPc4	rozměr
kinofilmového	kinofilmový	k2eAgNnSc2d1	kinofilmové
políčka	políčko	k1gNnSc2	políčko
(	(	kIx(	(
<g/>
36	[number]	k4	36
×	×	k?	×
24	[number]	k4	24
mm	mm	kA	mm
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
full-frame	fullram	k1gInSc5	full-fram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
plnoformátových	plnoformátův	k2eAgInPc2d1	plnoformátův
přístrojů	přístroj	k1gInPc2	přístroj
dnes	dnes	k6eAd1	dnes
největší	veliký	k2eAgNnSc4d3	veliký
rozlišení	rozlišení	k1gNnSc4	rozlišení
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Sony	Sony	kA	Sony
Alfa	alfa	k1gFnSc1	alfa
A900	A900	k1gFnSc1	A900
(	(	kIx(	(
<g/>
24,4	[number]	k4	24,4
Mpx	Mpx	k1gFnPc2	Mpx
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nikon	Nikon	k1gInSc1	Nikon
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
x	x	k?	x
(	(	kIx(	(
<g/>
24,5	[number]	k4	24,5
Mpx	Mpx	k1gFnPc2	Mpx
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
většina	většina	k1gFnSc1	většina
digitálních	digitální	k2eAgFnPc2d1	digitální
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc1d2	menší
obrazový	obrazový	k2eAgInSc1d1	obrazový
snímač	snímač	k1gInSc1	snímač
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
políčko	políčko	k1gNnSc4	políčko
kinofilmu	kinofilm	k1gInSc2	kinofilm
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
tzv.	tzv.	kA	tzv.
ořezový	ořezový	k2eAgInSc1d1	ořezový
faktor	faktor	k1gInSc1	faktor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
crop	crop	k1gMnSc1	crop
factor	factor	k1gMnSc1	factor
<g/>
)	)	kIx)	)
umožňující	umožňující	k2eAgFnSc1d1	umožňující
přepočítat	přepočítat	k5eAaPmF	přepočítat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jaké	jaký	k3yIgFnSc6	jaký
míře	míra	k1gFnSc6	míra
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zúžení	zúžení	k1gNnSc3	zúžení
obrazového	obrazový	k2eAgNnSc2d1	obrazové
pole	pole	k1gNnSc2	pole
při	při	k7c6	při
nasazení	nasazení	k1gNnSc6	nasazení
standardního	standardní	k2eAgInSc2d1	standardní
"	"	kIx"	"
<g/>
kinofilmového	kinofilmový	k2eAgInSc2d1	kinofilmový
<g/>
"	"	kIx"	"
objektivu	objektiv	k1gInSc2	objektiv
na	na	k7c4	na
takovýto	takovýto	k3xDgInSc4	takovýto
přístroj	přístroj	k1gInSc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Vynásobením	vynásobení	k1gNnSc7	vynásobení
reálné	reálný	k2eAgFnSc2d1	reálná
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
takového	takový	k3xDgInSc2	takový
objektivu	objektiv	k1gInSc2	objektiv
ořezovým	ořezův	k2eAgInSc7d1	ořezův
faktorem	faktor	k1gInSc7	faktor
získáme	získat	k5eAaPmIp1nP	získat
ekvivalentní	ekvivalentní	k2eAgFnSc4d1	ekvivalentní
ohniskovou	ohniskový	k2eAgFnSc4d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníme	vlastnit	k5eAaImIp1nP	vlastnit
<g/>
-li	i	k?	-li
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
širokoúhlý	širokoúhlý	k2eAgInSc1d1	širokoúhlý
objektiv	objektiv	k1gInSc1	objektiv
pro	pro	k7c4	pro
kinofilmový	kinofilmový	k2eAgInSc4d1	kinofilmový
přístroj	přístroj	k1gInSc4	přístroj
<g/>
,	,	kIx,	,
zúžení	zúžení	k1gNnSc4	zúžení
jeho	on	k3xPp3gInSc2	on
úhlu	úhel	k1gInSc2	úhel
záběru	záběr	k1gInSc2	záběr
při	při	k7c6	při
nasazení	nasazení	k1gNnSc6	nasazení
např.	např.	kA	např.
na	na	k7c4	na
zrcadlovku	zrcadlovka	k1gFnSc4	zrcadlovka
se	s	k7c7	s
snímačem	snímač	k1gInSc7	snímač
velikosti	velikost	k1gFnSc2	velikost
APS-C	APS-C	k1gFnSc2	APS-C
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
jeho	jeho	k3xOp3gFnSc4	jeho
degradaci	degradace	k1gFnSc4	degradace
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zúžení	zúžení	k1gNnSc4	zúžení
úhlu	úhel	k1gInSc2	úhel
záběru	záběr	k1gInSc2	záběr
u	u	k7c2	u
kinofilmových	kinofilmový	k2eAgInPc2d1	kinofilmový
teleobjektivů	teleobjektiv	k1gInPc2	teleobjektiv
může	moct	k5eAaImIp3nS	moct
naopak	naopak	k6eAd1	naopak
být	být	k5eAaImF	být
přínosem	přínos	k1gInSc7	přínos
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
problémům	problém	k1gInPc3	problém
nedocházelo	docházet	k5eNaImAgNnS	docházet
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
digitální	digitální	k2eAgFnPc4d1	digitální
zrcadlovky	zrcadlovka	k1gFnPc4	zrcadlovka
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
snímačem	snímač	k1gInSc7	snímač
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
speciální	speciální	k2eAgInPc1d1	speciální
objektivy	objektiv	k1gInPc1	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
rozlišení	rozlišení	k1gNnSc1	rozlišení
zrcadlovek	zrcadlovka	k1gFnPc2	zrcadlovka
nemusí	muset	k5eNaImIp3nS	muset
vždy	vždy	k6eAd1	vždy
přesahovat	přesahovat	k5eAaImF	přesahovat
rozlišení	rozlišení	k1gNnSc4	rozlišení
kompaktních	kompaktní	k2eAgInPc2d1	kompaktní
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
větší	veliký	k2eAgInSc4d2	veliký
obrazový	obrazový	k2eAgInSc4d1	obrazový
snímač	snímač	k1gInSc4	snímač
větší	veliký	k2eAgInPc4d2	veliký
obrazové	obrazový	k2eAgInPc4d1	obrazový
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
snímač	snímač	k1gInSc1	snímač
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc4d2	menší
šum	šum	k1gInSc4	šum
a	a	k8xC	a
menší	malý	k2eAgFnSc4d2	menší
difrakci	difrakce	k1gFnSc4	difrakce
<g/>
.	.	kIx.	.
</s>
<s>
Digitální	digitální	k2eAgFnPc1d1	digitální
stěny	stěna	k1gFnPc1	stěna
Ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
středoformátových	středoformátův	k2eAgInPc2d1	středoformátův
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
se	se	k3xPyFc4	se
prosadily	prosadit	k5eAaPmAgFnP	prosadit
digitální	digitální	k2eAgFnPc1d1	digitální
stěny	stěna	k1gFnPc1	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
je	on	k3xPp3gFnPc4	on
obvykle	obvykle	k6eAd1	obvykle
možno	možno	k6eAd1	možno
nasadit	nasadit	k5eAaPmF	nasadit
místo	místo	k1gNnSc4	místo
původní	původní	k2eAgFnSc2d1	původní
kazety	kazeta	k1gFnSc2	kazeta
s	s	k7c7	s
filmem	film	k1gInSc7	film
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
snímač	snímač	k1gInSc1	snímač
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
kinofilmové	kinofilmový	k2eAgNnSc4d1	kinofilmové
políčko	políčko	k1gNnSc4	políčko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
rozlišení	rozlišení	k1gNnPc2	rozlišení
60	[number]	k4	60
MPix	MPix	k1gInSc4	MPix
<g/>
.	.	kIx.	.
</s>
