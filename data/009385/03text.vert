<p>
<s>
Germanizace	germanizace	k1gFnPc1	germanizace
<g/>
,	,	kIx,	,
také	také	k9	také
poněmčování	poněmčování	k1gNnSc4	poněmčování
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
šíření	šíření	k1gNnSc4	šíření
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
na	na	k7c6	na
ovládaných	ovládaný	k2eAgNnPc6d1	ovládané
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nebyli	být	k5eNaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Germanizace	germanizace	k1gFnSc1	germanizace
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
germánských	germánský	k2eAgInPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
germanizaci	germanizace	k1gFnSc3	germanizace
polabských	polabský	k2eAgInPc2d1	polabský
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
německé	německý	k2eAgFnSc2d1	německá
kolonizace	kolonizace	k1gFnSc2	kolonizace
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
-	-	kIx~	-
Ostsiedlung	Ostsiedlung	k1gMnSc1	Ostsiedlung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
kolonizace	kolonizace	k1gFnSc2	kolonizace
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
Němci	Němec	k1gMnPc7	Němec
osídlena	osídlen	k2eAgNnPc1d1	osídleno
hlavně	hlavně	k6eAd1	hlavně
pohraniční	pohraniční	k2eAgNnPc1d1	pohraniční
území	území	k1gNnPc1	území
a	a	k8xC	a
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
době	doba	k1gFnSc6	doba
absolutismu	absolutismus	k1gInSc2	absolutismus
a	a	k8xC	a
centralizace	centralizace	k1gFnSc2	centralizace
moci	moc	k1gFnSc2	moc
===	===	k?	===
</s>
</p>
<p>
<s>
Germanizace	germanizace	k1gFnSc1	germanizace
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
germanizována	germanizován	k2eAgFnSc1d1	germanizována
oblast	oblast	k1gFnSc1	oblast
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Rakouska	Rakousko	k1gNnSc2	Rakousko
(	(	kIx(	(
<g/>
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
germanizaci	germanizace	k1gFnSc3	germanizace
se	se	k3xPyFc4	se
postavilo	postavit	k5eAaPmAgNnS	postavit
české	český	k2eAgNnSc1d1	české
národní	národní	k2eAgNnSc1d1	národní
obrození	obrození	k1gNnSc1	obrození
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
pokoušelo	pokoušet	k5eAaImAgNnS	pokoušet
–	–	k?	–
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
úspěšně	úspěšně	k6eAd1	úspěšně
–	–	k?	–
zvrátit	zvrátit	k5eAaPmF	zvrátit
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
germanizační	germanizační	k2eAgInPc1d1	germanizační
tlaky	tlak	k1gInPc1	tlak
<g/>
,	,	kIx,	,
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
neustoupily	ustoupit	k5eNaPmAgFnP	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Skončily	skončit	k5eAaPmAgFnP	skončit
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
až	až	k9	až
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
;	;	kIx,	;
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byl	být	k5eAaImAgMnS	být
zase	zase	k9	zase
silný	silný	k2eAgInSc4d1	silný
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
slovanské	slovanský	k2eAgInPc4d1	slovanský
národy	národ	k1gInPc4	národ
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
Lužickým	lužický	k2eAgMnPc3d1	lužický
Srbům	Srb	k1gMnPc3	Srb
a	a	k8xC	a
Polákům	Polák	k1gMnPc3	Polák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Za	za	k7c2	za
nacismu	nacismus	k1gInSc2	nacismus
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
situace	situace	k1gFnSc1	situace
obrátila	obrátit	k5eAaPmAgFnS	obrátit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
dříve	dříve	k6eAd2	dříve
utlačovaných	utlačovaný	k2eAgInPc2d1	utlačovaný
národů	národ	k1gInPc2	národ
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zvratem	zvrat	k1gInSc7	zvrat
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
ideologického	ideologický	k2eAgNnSc2d1	ideologické
šíření	šíření	k1gNnSc2	šíření
německé	německý	k2eAgFnSc2d1	německá
kultury	kultura	k1gFnSc2	kultura
jako	jako	k8xC	jako
té	ten	k3xDgFnSc2	ten
nejdokonalejší	dokonalý	k2eAgFnSc1d3	nejdokonalejší
byla	být	k5eAaImAgFnS	být
germanizace	germanizace	k1gFnSc1	germanizace
plánovaně	plánovaně	k6eAd1	plánovaně
uplatňována	uplatňovat	k5eAaImNgFnS	uplatňovat
v	v	k7c6	v
porobených	porobený	k2eAgFnPc6d1	porobená
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
děti	dítě	k1gFnPc1	dítě
byly	být	k5eAaImAgFnP	být
odebírány	odebírat	k5eAaImNgFnP	odebírat
rodičům	rodič	k1gMnPc3	rodič
na	na	k7c4	na
převýchovu	převýchova	k1gFnSc4	převýchova
a	a	k8xC	a
též	též	k9	též
byly	být	k5eAaImAgInP	být
vypracovány	vypracován	k2eAgInPc4d1	vypracován
plány	plán	k1gInPc4	plán
odsunu	odsun	k1gInSc2	odsun
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
od	od	k7c2	od
Říše	říš	k1gFnSc2	říš
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
slovanských	slovanský	k2eAgInPc2d1	slovanský
národů	národ	k1gInPc2	národ
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
konečného	konečný	k2eAgNnSc2d1	konečné
řešení	řešení	k1gNnSc2	řešení
české	český	k2eAgFnSc2d1	Česká
otázky	otázka	k1gFnSc2	otázka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
formou	forma	k1gFnSc7	forma
germanizace	germanizace	k1gFnSc2	germanizace
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
náhradě	náhrada	k1gFnSc3	náhrada
Němci	Němec	k1gMnPc7	Němec
byl	být	k5eAaImAgInS	být
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
Generalplan	Generalplan	k1gInSc1	Generalplan
Ost	Ost	k1gFnSc2	Ost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
porážkou	porážka	k1gFnSc7	porážka
Německa	Německo	k1gNnSc2	Německo
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
se	se	k3xPyFc4	se
s	s	k7c7	s
germanizací	germanizace	k1gFnSc7	germanizace
přestalo	přestat	k5eAaPmAgNnS	přestat
jako	jako	k8xS	jako
se	s	k7c7	s
symbolem	symbol	k1gInSc7	symbol
nacismu	nacismus	k1gInSc2	nacismus
<g/>
;	;	kIx,	;
v	v	k7c6	v
NDR	NDR	kA	NDR
byla	být	k5eAaImAgFnS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
jistá	jistý	k2eAgFnSc1d1	jistá
práva	práv	k2eAgFnSc1d1	práva
například	například	k6eAd1	například
Lužickým	lužický	k2eAgMnPc3d1	lužický
Srbům	Srb	k1gMnPc3	Srb
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
pohraničí	pohraničí	k1gNnSc6	pohraničí
byly	být	k5eAaImAgFnP	být
také	také	k9	také
zavedeny	zaveden	k2eAgInPc4d1	zaveden
dvojjazyčné	dvojjazyčný	k2eAgInPc4d1	dvojjazyčný
(	(	kIx(	(
<g/>
německo-slovinské	německolovinský	k2eAgInPc4d1	německo-slovinský
<g/>
)	)	kIx)	)
nápisy	nápis	k1gInPc4	nápis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lebensraum	Lebensraum	k1gInSc1	Lebensraum
</s>
</p>
<p>
<s>
Intelligenzaktion	Intelligenzaktion	k1gInSc1	Intelligenzaktion
</s>
</p>
<p>
<s>
Drang	Drang	k1gInSc1	Drang
nach	nach	k1gInSc1	nach
Osten	osten	k1gInSc1	osten
</s>
</p>
<p>
<s>
Vysídlení	vysídlení	k1gNnSc1	vysídlení
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
#	#	kIx~	#
<g/>
Odněmčení	odněmčení	k1gNnSc1	odněmčení
</s>
</p>
<p>
<s>
Antigermanismus	Antigermanismus	k1gInSc1	Antigermanismus
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Germanizace	germanizace	k1gFnSc1	germanizace
v	v	k7c6	v
době	doba	k1gFnSc6	doba
baroka	baroko	k1gNnSc2	baroko
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Germanizace	germanizace	k1gFnSc2	germanizace
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
