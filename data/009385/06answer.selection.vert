<s>
Germanizace	germanizace	k1gFnPc1	germanizace
<g/>
,	,	kIx,	,
také	také	k9	také
poněmčování	poněmčování	k1gNnSc4	poněmčování
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
šíření	šíření	k1gNnSc4	šíření
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
na	na	k7c6	na
ovládaných	ovládaný	k2eAgNnPc6d1	ovládané
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc4	jejichž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nebyli	být	k5eNaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
