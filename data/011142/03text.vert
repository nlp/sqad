<p>
<s>
Galaxy	Galax	k1gInPc4	Galax
Pocket	Pocketa	k1gFnPc2	Pocketa
je	být	k5eAaImIp3nS	být
smartphone	smartphon	k1gInSc5	smartphon
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
Androidích	Androiď	k1gFnPc6	Androiď
zařízení	zařízení	k1gNnSc2	zařízení
vyráběných	vyráběný	k2eAgFnPc2d1	vyráběná
společností	společnost	k1gFnPc2	společnost
Samsung	Samsung	kA	Samsung
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
charakteristickými	charakteristický	k2eAgFnPc7d1	charakteristická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
jsou	být	k5eAaImIp3nP	být
malá	malý	k2eAgFnSc1d1	malá
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
spojený	spojený	k2eAgInSc1d1	spojený
2,8	[number]	k4	2,8
<g/>
"	"	kIx"	"
displej	displej	k1gInSc1	displej
<g/>
,	,	kIx,	,
Android	android	k1gInSc1	android
verze	verze	k1gFnSc1	verze
2.3	[number]	k4	2.3
<g/>
.6	.6	k4	.6
a	a	k8xC	a
především	především	k6eAd1	především
nízká	nízký	k2eAgFnSc1d1	nízká
cena	cena	k1gFnSc1	cena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
dotykovým	dotykový	k2eAgInSc7d1	dotykový
displejem	displej	k1gInSc7	displej
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgFnPc7	dva
softwarovými	softwarový	k2eAgFnPc7d1	softwarová
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
ovládacím	ovládací	k2eAgNnSc7d1	ovládací
tlačítkem	tlačítko	k1gNnSc7	tlačítko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
potom	potom	k6eAd1	potom
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
tlačítko	tlačítko	k1gNnSc4	tlačítko
pro	pro	k7c4	pro
nastavení	nastavení	k1gNnSc4	nastavení
hlasitosti	hlasitost	k1gFnSc2	hlasitost
<g/>
,	,	kIx,	,
z	z	k7c2	z
pravé	pravý	k2eAgFnSc2d1	pravá
tlačítko	tlačítko	k1gNnSc4	tlačítko
pro	pro	k7c4	pro
uzamknutí	uzamknutí	k1gNnSc4	uzamknutí
displeje	displej	k1gInSc2	displej
<g/>
/	/	kIx~	/
<g/>
vypnutí	vypnutí	k1gNnSc4	vypnutí
telefonu	telefon	k1gInSc2	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
reproduktor	reproduktor	k1gInSc1	reproduktor
pro	pro	k7c4	pro
poslech	poslech	k1gInSc4	poslech
hovorů	hovor	k1gInPc2	hovor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zadní	zadní	k2eAgInSc4d1	zadní
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
a	a	k8xC	a
výdech	výdech	k1gInSc4	výdech
hlasitého	hlasitý	k2eAgInSc2d1	hlasitý
reproduktoru	reproduktor	k1gInSc2	reproduktor
<g/>
.	.	kIx.	.
</s>
<s>
Vyjímatelná	vyjímatelný	k2eAgFnSc1d1	vyjímatelná
baterie	baterie	k1gFnSc1	baterie
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
1200	[number]	k4	1200
mAh	mAh	k?	mAh
a	a	k8xC	a
dle	dle	k7c2	dle
výrobce	výrobce	k1gMnSc2	výrobce
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
udržet	udržet	k5eAaPmF	udržet
telefon	telefon	k1gInSc4	telefon
v	v	k7c6	v
pohotovostním	pohotovostní	k2eAgInSc6d1	pohotovostní
režimu	režim	k1gInSc6	režim
až	až	k9	až
520	[number]	k4	520
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Celým	celý	k2eAgNnSc7d1	celé
označením	označení	k1gNnSc7	označení
Samsung	Samsung	kA	Samsung
S5300	S5300	k1gFnSc2	S5300
Galaxy	Galax	k1gInPc1	Galax
Pocket	Pocketa	k1gFnPc2	Pocketa
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
Q1	Q1	k1gFnSc6	Q1
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
jako	jako	k8xC	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c2	za
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejnižší	nízký	k2eAgInSc1d3	nejnižší
model	model	k1gInSc1	model
telefonu	telefon	k1gInSc2	telefon
společnosti	společnost	k1gFnSc3	společnost
Samsung	Samsung	kA	Samsung
Galaxy	Galax	k1gInPc4	Galax
Mini	mini	k2eAgInPc4d1	mini
<g/>
.	.	kIx.	.
</s>
<s>
Prioritou	priorita	k1gFnSc7	priorita
bylo	být	k5eAaImAgNnS	být
dostat	dostat	k5eAaPmF	dostat
cenu	cena	k1gFnSc4	cena
na	na	k7c4	na
co	co	k3yRnSc4	co
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
úroveň	úroveň	k1gFnSc1	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
nejen	nejen	k6eAd1	nejen
oslabením	oslabení	k1gNnSc7	oslabení
hardwarových	hardwarový	k2eAgFnPc2d1	hardwarová
specifikací	specifikace	k1gFnPc2	specifikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
úpravou	úprava	k1gFnSc7	úprava
obsahu	obsah	k1gInSc2	obsah
základního	základní	k2eAgNnSc2d1	základní
balení	balení	k1gNnSc2	balení
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
modelům	model	k1gInPc3	model
telefonů	telefon	k1gInPc2	telefon
Samsung	Samsung	kA	Samsung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krabičce	krabička	k1gFnSc6	krabička
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
přístrojem	přístroj	k1gInSc7	přístroj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
zařízení	zařízení	k1gNnSc1	zařízení
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
,	,	kIx,	,
napájecí	napájecí	k2eAgMnSc1d1	napájecí
adaptér	adaptér	k1gMnSc1	adaptér
zakončený	zakončený	k2eAgMnSc1d1	zakončený
microUSB	microUSB	k?	microUSB
konektorem	konektor	k1gInSc7	konektor
a	a	k8xC	a
papírová	papírový	k2eAgFnSc1d1	papírová
dokumentace	dokumentace	k1gFnSc1	dokumentace
<g/>
.	.	kIx.	.
</s>
<s>
USB	USB	kA	USB
kabel	kabel	k1gInSc1	kabel
pro	pro	k7c4	pro
propojení	propojení	k1gNnSc4	propojení
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
či	či	k8xC	či
sluchátka	sluchátko	k1gNnPc4	sluchátko
už	už	k6eAd1	už
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
uživatel	uživatel	k1gMnSc1	uživatel
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zájmu	zájem	k1gInSc2	zájem
dokoupit	dokoupit	k5eAaPmF	dokoupit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
verze	verze	k1gFnSc1	verze
Galaxy	Galax	k1gInPc1	Galax
Pocket	Pocketa	k1gFnPc2	Pocketa
DUOS	DUOS	kA	DUOS
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
totožné	totožný	k2eAgInPc4d1	totožný
parametry	parametr	k1gInPc4	parametr
s	s	k7c7	s
modelem	model	k1gInSc7	model
Pocket	Pocketa	k1gFnPc2	Pocketa
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
možnost	možnost	k1gFnSc4	možnost
vložit	vložit	k5eAaPmF	vložit
druhou	druhý	k4xOgFnSc4	druhý
SIM	SIM	kA	SIM
kartu	karta	k1gFnSc4	karta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
telefon	telefon	k1gInSc1	telefon
dostupný	dostupný	k2eAgInSc1d1	dostupný
z	z	k7c2	z
volného	volný	k2eAgInSc2d1	volný
prodeje	prodej	k1gInSc2	prodej
či	či	k8xC	či
skrze	skrze	k?	skrze
operátora	operátor	k1gMnSc2	operátor
Telefónica	Telefónicus	k1gMnSc2	Telefónicus
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
český	český	k2eAgMnSc1d1	český
operátor	operátor	k1gMnSc1	operátor
tento	tento	k3xDgInSc4	tento
model	model	k1gInSc4	model
nenabízí	nabízet	k5eNaImIp3nS	nabízet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technická	technický	k2eAgFnSc1d1	technická
výbava	výbava	k1gFnSc1	výbava
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozměry	rozměr	k1gInPc4	rozměr
===	===	k?	===
</s>
</p>
<p>
<s>
Telefon	telefon	k1gInSc1	telefon
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc1d1	vysoký
103,9	[number]	k4	103,9
mm	mm	kA	mm
a	a	k8xC	a
široký	široký	k2eAgInSc4d1	široký
57,9	[number]	k4	57,9
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
tloušťky	tloušťka	k1gFnSc2	tloušťka
11,98	[number]	k4	11,98
mm	mm	kA	mm
a	a	k8xC	a
díky	díky	k7c3	díky
celoplastovému	celoplastový	k2eAgNnSc3d1	celoplastové
tělu	tělo	k1gNnSc3	tělo
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
malým	malý	k2eAgInPc3d1	malý
rozměrům	rozměr	k1gInPc3	rozměr
také	také	k9	také
hmotnosti	hmotnost	k1gFnSc3	hmotnost
pouze	pouze	k6eAd1	pouze
97	[number]	k4	97
g.	g.	k?	g.
</s>
</p>
<p>
<s>
===	===	k?	===
Výkon	výkon	k1gInSc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jednojádrový	jednojádrový	k2eAgInSc1d1	jednojádrový
procesor	procesor	k1gInSc1	procesor
Broadcom	Broadcom	k1gInSc1	Broadcom
BCM21553	BCM21553	k1gFnSc2	BCM21553
o	o	k7c6	o
taktu	takt	k1gInSc6	takt
832	[number]	k4	832
MHz	Mhz	kA	Mhz
vyrobený	vyrobený	k2eAgMnSc1d1	vyrobený
65	[number]	k4	65
<g/>
nm	nm	k?	nm
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Přítomen	přítomen	k2eAgInSc1d1	přítomen
je	být	k5eAaImIp3nS	být
i	i	k9	i
grafický	grafický	k2eAgInSc4d1	grafický
čip	čip	k1gInSc4	čip
Broadcom	Broadcom	k1gInSc1	Broadcom
BCM	BCM	kA	BCM
<g/>
2763	[number]	k4	2763
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
operační	operační	k2eAgFnSc2d1	operační
paměti	paměť	k1gFnSc2	paměť
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
velikosti	velikost	k1gFnSc6	velikost
289	[number]	k4	289
MB	MB	kA	MB
je	být	k5eAaImIp3nS	být
uživateli	uživatel	k1gMnSc3	uživatel
po	po	k7c6	po
načtení	načtení	k1gNnSc6	načtení
systému	systém	k1gInSc2	systém
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
paměť	paměť	k1gFnSc1	paměť
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
3	[number]	k4	3
GB	GB	kA	GB
je	být	k5eAaImIp3nS	být
rozšiřitelný	rozšiřitelný	k2eAgInSc1d1	rozšiřitelný
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
externí	externí	k2eAgFnSc2d1	externí
paměťové	paměťový	k2eAgFnSc2d1	paměťová
karty	karta	k1gFnSc2	karta
formátu	formát	k1gInSc2	formát
microSD	microSD	k?	microSD
o	o	k7c6	o
maximální	maximální	k2eAgFnSc6d1	maximální
kapacitě	kapacita	k1gFnSc6	kapacita
32	[number]	k4	32
GB	GB	kA	GB
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Displej	displej	k1gInSc4	displej
===	===	k?	===
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
kapacitního	kapacitní	k2eAgInSc2d1	kapacitní
displeje	displej	k1gInSc2	displej
je	být	k5eAaImIp3nS	být
2,8	[number]	k4	2,8
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
7,112	[number]	k4	7,112
cm	cm	kA	cm
<g/>
)	)	kIx)	)
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
240	[number]	k4	240
<g/>
x	x	k?	x
<g/>
320	[number]	k4	320
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
jemnost	jemnost	k1gFnSc1	jemnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
143	[number]	k4	143
DPI	dpi	kA	dpi
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
pixely	pixel	k1gInPc1	pixel
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
i	i	k9	i
při	při	k7c6	při
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
detailním	detailní	k2eAgNnSc6d1	detailní
zkoumání	zkoumání	k1gNnSc6	zkoumání
znatelné	znatelný	k2eAgInPc1d1	znatelný
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
262	[number]	k4	262
tisíci	tisíc	k4xCgInPc7	tisíc
barvami	barva	k1gFnPc7	barva
a	a	k8xC	a
použitá	použitý	k2eAgFnSc1d1	použitá
výrobní	výrobní	k2eAgFnSc1d1	výrobní
technologie	technologie	k1gFnSc1	technologie
je	být	k5eAaImIp3nS	být
TFT	TFT	kA	TFT
LCD	LCD	kA	LCD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konektivita	konektivita	k1gFnSc1	konektivita
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
straně	strana	k1gFnSc6	strana
zařízení	zařízení	k1gNnSc2	zařízení
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
microUSB	microUSB	k?	microUSB
konektor	konektor	k1gInSc4	konektor
pro	pro	k7c4	pro
nabíjení	nabíjení	k1gNnSc4	nabíjení
a	a	k8xC	a
propojení	propojení	k1gNnSc4	propojení
telefonu	telefon	k1gInSc2	telefon
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
téže	tenže	k3xDgFnSc6	tenže
straně	strana	k1gFnSc6	strana
najdeme	najít	k5eAaPmIp1nP	najít
i	i	k9	i
3,5	[number]	k4	3,5
<g/>
mm	mm	kA	mm
zdířku	zdířka	k1gFnSc4	zdířka
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
sluchátek	sluchátko	k1gNnPc2	sluchátko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
technologií	technologie	k1gFnPc2	technologie
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
WiFi	WiFe	k1gFnSc4	WiFe
a	a	k8xC	a
Bluetooth	Bluetooth	k1gInSc4	Bluetooth
<g/>
.	.	kIx.	.
</s>
<s>
Telefon	telefon	k1gInSc1	telefon
má	mít	k5eAaImIp3nS	mít
zabudovaný	zabudovaný	k2eAgInSc1d1	zabudovaný
GPS	GPS	kA	GPS
modul	modul	k1gInSc1	modul
a	a	k8xC	a
senzor	senzor	k1gInSc1	senzor
polohy	poloha	k1gFnSc2	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fotoaparát	fotoaparát	k1gInSc1	fotoaparát
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
přístroje	přístroj	k1gInSc2	přístroj
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
s	s	k7c7	s
rozlišením	rozlišení	k1gNnSc7	rozlišení
2	[number]	k4	2
megapixely	megapixel	k1gInPc7	megapixel
(	(	kIx(	(
<g/>
snímky	snímek	k1gInPc7	snímek
o	o	k7c6	o
maximální	maximální	k2eAgFnSc6d1	maximální
velikosti	velikost	k1gFnSc6	velikost
1600	[number]	k4	1600
<g/>
×	×	k?	×
<g/>
1200	[number]	k4	1200
bodů	bod	k1gInPc2	bod
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
automatického	automatický	k2eAgNnSc2d1	automatické
ostření	ostření	k1gNnSc2	ostření
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
dvojnásobného	dvojnásobný	k2eAgInSc2d1	dvojnásobný
digitálního	digitální	k2eAgInSc2d1	digitální
zoomu	zoom	k1gInSc2	zoom
<g/>
,	,	kIx,	,
několika	několik	k4yIc2	několik
scénických	scénický	k2eAgInPc2d1	scénický
režimů	režim	k1gInPc2	režim
a	a	k8xC	a
základního	základní	k2eAgNnSc2d1	základní
nastavení	nastavení	k1gNnSc2	nastavení
jasu	jas	k1gInSc2	jas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Telefon	telefon	k1gInSc1	telefon
dokáže	dokázat	k5eAaPmIp3nS	dokázat
nahrávat	nahrávat	k5eAaImF	nahrávat
také	také	k9	také
video	video	k1gNnSc4	video
v	v	k7c6	v
rozlišení	rozlišení	k1gNnSc6	rozlišení
320	[number]	k4	320
<g/>
×	×	k?	×
<g/>
240	[number]	k4	240
bodů	bod	k1gInPc2	bod
při	při	k7c6	při
průměrné	průměrný	k2eAgFnSc6d1	průměrná
frekvenci	frekvence	k1gFnSc6	frekvence
15	[number]	k4	15
snímků	snímek	k1gInPc2	snímek
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Programová	programový	k2eAgFnSc1d1	programová
výbava	výbava	k1gFnSc1	výbava
==	==	k?	==
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
telefonu	telefon	k1gInSc2	telefon
je	být	k5eAaImIp3nS	být
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Android	android	k1gInSc1	android
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
2.3	[number]	k4	2.3
<g/>
.6	.6	k4	.6
<g/>
.	.	kIx.	.
</s>
<s>
Update	update	k1gInSc1	update
na	na	k7c4	na
vyšší	vysoký	k2eAgFnPc4d2	vyšší
verze	verze	k1gFnPc4	verze
se	se	k3xPyFc4	se
neplánuje	plánovat	k5eNaImIp3nS	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Samsung	Samsung	kA	Samsung
doplnil	doplnit	k5eAaPmAgMnS	doplnit
prostředí	prostředí	k1gNnSc4	prostředí
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
grafickou	grafický	k2eAgFnSc4d1	grafická
nadstavbu	nadstavba	k1gFnSc4	nadstavba
TouchWiz	TouchWiza	k1gFnPc2	TouchWiza
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
aplikace	aplikace	k1gFnPc1	aplikace
<g/>
,	,	kIx,	,
sjednocující	sjednocující	k2eAgFnPc1d1	sjednocující
veškerá	veškerý	k3xTgNnPc4	veškerý
zařízení	zařízení	k1gNnSc2	zařízení
Galaxy	Galax	k1gInPc1	Galax
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
základu	základ	k1gInSc6	základ
je	být	k5eAaImIp3nS	být
přítomna	přítomen	k2eAgFnSc1d1	přítomna
aplikace	aplikace	k1gFnSc1	aplikace
pro	pro	k7c4	pro
přehrávání	přehrávání	k1gNnSc4	přehrávání
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
rádia	rádio	k1gNnSc2	rádio
<g/>
,	,	kIx,	,
internetový	internetový	k2eAgInSc1d1	internetový
prohlížeč	prohlížeč	k1gInSc1	prohlížeč
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc1d1	základní
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
software	software	k1gInSc1	software
v	v	k7c6	v
úpravě	úprava	k1gFnSc6	úprava
pro	pro	k7c4	pro
mobilní	mobilní	k2eAgNnSc4d1	mobilní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
budík	budík	k1gInSc4	budík
<g/>
,	,	kIx,	,
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
kalkulačka	kalkulačka	k1gFnSc1	kalkulačka
<g/>
,	,	kIx,	,
mapy	mapa	k1gFnPc1	mapa
a	a	k8xC	a
hlasová	hlasový	k2eAgFnSc1d1	hlasová
navigace	navigace	k1gFnSc1	navigace
<g/>
,	,	kIx,	,
diktafon	diktafon	k1gInSc1	diktafon
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc1	poznámka
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
funkčnosti	funkčnost	k1gFnSc2	funkčnost
lze	lze	k6eAd1	lze
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
získat	získat	k5eAaPmF	získat
tyto	tento	k3xDgInPc4	tento
přes	přes	k7c4	přes
obchod	obchod	k1gInSc4	obchod
Google	Google	k1gFnSc2	Google
Play	play	k0	play
či	či	k8xC	či
Samsung	Samsung	kA	Samsung
Apps	Apps	k1gInSc1	Apps
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Smartphone	Smartphon	k1gMnSc5	Smartphon
</s>
</p>
<p>
<s>
Android	android	k1gInSc1	android
(	(	kIx(	(
<g/>
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zařízení	zařízení	k1gNnSc2	zařízení
se	s	k7c7	s
systémem	systém	k1gInSc7	systém
Android	android	k1gInSc1	android
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Samsung	Samsung	kA	Samsung
Galaxy	Galax	k1gInPc4	Galax
Pocket	Pocketa	k1gFnPc2	Pocketa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.samsung.com	www.samsung.com	k1gInSc1	www.samsung.com
-	-	kIx~	-
Galaxy	Galax	k1gInPc1	Galax
Pocket	Pocketa	k1gFnPc2	Pocketa
</s>
</p>
<p>
<s>
www.mobilmania.cz	www.mobilmania.cz	k1gMnSc1	www.mobilmania.cz
</s>
</p>
<p>
<s>
www.mobilnet.cz	www.mobilnet.cz	k1gMnSc1	www.mobilnet.cz
</s>
</p>
