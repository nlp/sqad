<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
archivů	archiv	k1gInPc2	archiv
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
MDA	MDA	kA	MDA
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
International	International	k1gMnSc1	International
Archives	Archives	k1gMnSc1	Archives
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
svátek	svátek	k1gInSc1	svátek
slavený	slavený	k2eAgInSc1d1	slavený
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gFnSc6	jehož
příležitosti	příležitost	k1gFnSc6	příležitost
archivy	archiv	k1gInPc1	archiv
připravují	připravovat	k5eAaImIp3nP	připravovat
společenské	společenský	k2eAgFnPc4d1	společenská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Posláním	poslání	k1gNnSc7	poslání
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
povědomí	povědomí	k1gNnSc4	povědomí
široké	široký	k2eAgFnSc2d1	široká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c6	o
důležitosti	důležitost	k1gFnSc6	důležitost
archiválií	archiválie	k1gFnPc2	archiválie
a	a	k8xC	a
archivů	archiv	k1gInPc2	archiv
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
archivy	archiv	k1gInPc1	archiv
svým	svůj	k3xOyFgNnSc7	svůj
posláním	poslání	k1gNnSc7	poslání
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
právní	právní	k2eAgFnSc4d1	právní
jistotu	jistota	k1gFnSc4	jistota
společnosti	společnost	k1gFnSc2	společnost
trvale	trvale	k6eAd1	trvale
uloženou	uložený	k2eAgFnSc4d1	uložená
v	v	k7c6	v
dokumentech	dokument	k1gInPc6	dokument
<g/>
;	;	kIx,	;
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
povědomí	povědomí	k1gNnSc4	povědomí
u	u	k7c2	u
výhodách	výhoda	k1gFnPc6	výhoda
spisové	spisový	k2eAgFnSc2d1	spisová
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnSc2d1	jiná
institucionalizované	institucionalizovaný	k2eAgFnSc2d1	institucionalizovaná
správy	správa	k1gFnSc2	správa
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
řádná	řádný	k2eAgFnSc1d1	řádná
péče	péče	k1gFnSc1	péče
o	o	k7c4	o
dokumenty	dokument	k1gInPc4	dokument
může	moct	k5eAaImIp3nS	moct
<g />
.	.	kIx.	.
</s>
<s>
přispívat	přispívat	k5eAaImF	přispívat
ke	k	k7c3	k
kvalitnímu	kvalitní	k2eAgInSc3d1	kvalitní
výkonu	výkon	k1gInSc2	výkon
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
organizace	organizace	k1gFnSc2	organizace
<g/>
;	;	kIx,	;
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
povědomí	povědomí	k1gNnSc4	povědomí
veřejného	veřejný	k2eAgInSc2d1	veřejný
i	i	k8xC	i
komerčního	komerční	k2eAgInSc2d1	komerční
sektoru	sektor	k1gInSc2	sektor
o	o	k7c6	o
potřebnosti	potřebnost	k1gFnSc6	potřebnost
trvalé	trvalý	k2eAgFnSc2d1	trvalá
ochrany	ochrana	k1gFnSc2	ochrana
archivů	archiv	k1gInPc2	archiv
a	a	k8xC	a
o	o	k7c6	o
nezbytném	zbytný	k2eNgNnSc6d1	nezbytné
zajištění	zajištění	k1gNnSc6	zajištění
jejich	jejich	k3xOp3gFnSc2	jejich
přístupnosti	přístupnost	k1gFnSc2	přístupnost
<g/>
;	;	kIx,	;
zpřístupňovat	zpřístupňovat	k5eAaImF	zpřístupňovat
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
výjimečné	výjimečný	k2eAgFnSc3d1	výjimečná
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
vzácné	vzácný	k2eAgFnPc4d1	vzácná
archiválie	archiválie	k1gFnPc4	archiválie
uchovávané	uchovávaný	k2eAgFnPc4d1	uchovávaná
v	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
<g/>
;	;	kIx,	;
zlepšovat	zlepšovat	k5eAaImF	zlepšovat
obraz	obraz	k1gInSc4	obraz
archiválií	archiválie	k1gFnPc2	archiválie
a	a	k8xC	a
archivů	archiv	k1gInPc2	archiv
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
archivy	archiv	k1gInPc1	archiv
celosvětově	celosvětově	k6eAd1	celosvětově
zviditelňovat	zviditelňovat	k5eAaImF	zviditelňovat
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
ustanovení	ustanovení	k1gNnSc4	ustanovení
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
archivů	archiv	k1gInPc2	archiv
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
archivním	archivní	k2eAgInSc6d1	archivní
kongresu	kongres	k1gInSc6	kongres
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
účastníci	účastník	k1gMnPc1	účastník
kongresu	kongres	k1gInSc2	kongres
schválili	schválit	k5eAaPmAgMnP	schválit
žádost	žádost	k1gFnSc4	žádost
adresovanou	adresovaný	k2eAgFnSc4d1	adresovaná
Organizaci	organizace	k1gFnSc4	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
na	na	k7c4	na
prohlášení	prohlášení	k1gNnSc4	prohlášení
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
na	na	k7c6	na
výročním	výroční	k2eAgNnSc6d1	výroční
zasedání	zasedání	k1gNnSc6	zasedání
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
archivní	archivní	k2eAgFnSc2d1	archivní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ICA	ICA	kA	ICA
<g/>
)	)	kIx)	)
v	v	k7c6	v
bylo	být	k5eAaImAgNnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
archivů	archiv	k1gInPc2	archiv
bude	být	k5eAaImBp3nS	být
slavit	slavit	k5eAaImF	slavit
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
60	[number]	k4	60
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
vzniku	vznik	k1gInSc2	vznik
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
archivní	archivní	k2eAgFnSc2d1	archivní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
založena	založit	k5eAaPmNgFnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
červen	červen	k2eAgInSc4d1	červen
jako	jako	k8xS	jako
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
archivů	archiv	k1gInPc2	archiv
slaven	slaven	k2eAgInSc4d1	slaven
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
poprvé	poprvé	k6eAd1	poprvé
připomínán	připomínán	k2eAgInSc4d1	připomínán
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c6	na
celokrajském	celokrajský	k2eAgNnSc6d1	celokrajské
setkání	setkání	k1gNnSc6	setkání
severomoravských	severomoravský	k2eAgMnPc2d1	severomoravský
archivářů	archivář	k1gMnPc2	archivář
<g/>
,	,	kIx,	,
oslavován	oslavovat	k5eAaImNgInS	oslavovat
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
celostátně	celostátně	k6eAd1	celostátně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
propagace	propagace	k1gFnSc1	propagace
MDA	MDA	kA	MDA
v	v	k7c6	v
rukou	ruka	k1gFnPc2	ruka
České	český	k2eAgFnSc2d1	Česká
archivní	archivní	k2eAgFnSc2d1	archivní
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
jednotné	jednotný	k2eAgNnSc4d1	jednotné
motto	motto	k1gNnSc4	motto
a	a	k8xC	a
logo	logo	k1gNnSc4	logo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
realizaci	realizace	k1gFnSc4	realizace
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
archivy	archiv	k1gInPc1	archiv
samy	sám	k3xTgFnPc1	sám
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
probíhající	probíhající	k2eAgFnSc1d1	probíhající
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
Dne	den	k1gInSc2	den
archivů	archiv	k1gInPc2	archiv
jsou	být	k5eAaImIp3nP	být
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
od	od	k7c2	od
individuálních	individuální	k2eAgFnPc2d1	individuální
poradenství	poradenství	k1gNnPc2	poradenství
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
praktická	praktický	k2eAgNnPc4d1	praktické
cvičení	cvičení	k1gNnPc4	cvičení
pro	pro	k7c4	pro
zájemce	zájemce	k1gMnPc4	zájemce
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
komentované	komentovaný	k2eAgFnPc4d1	komentovaná
prohlídky	prohlídka	k1gFnPc4	prohlídka
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
vernisáže	vernisáž	k1gFnPc4	vernisáž
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
speciální	speciální	k2eAgFnPc4d1	speciální
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Den	den	k1gInSc1	den
archivů	archiv	k1gInPc2	archiv
koná	konat	k5eAaImIp3nS	konat
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
Archiv	archiv	k1gInSc1	archiv
-	-	kIx~	-
křižovatka	křižovatka	k1gFnSc1	křižovatka
mezi	mezi	k7c7	mezi
minulostí	minulost	k1gFnSc7	minulost
a	a	k8xC	a
přítomností	přítomnost	k1gFnSc7	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
dne	den	k1gInSc2	den
archivů	archiv	k1gInPc2	archiv
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
International	International	k1gFnSc1	International
Archives	Archivesa	k1gFnPc2	Archivesa
Day	Day	k1gFnSc1	Day
Přehled	přehled	k1gInSc1	přehled
zúčastněných	zúčastněný	k2eAgMnPc2d1	zúčastněný
archivů	archiv	k1gInPc2	archiv
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
České	český	k2eAgFnSc2d1	Česká
archivní	archivní	k2eAgFnSc2d1	archivní
společnosti	společnost	k1gFnSc2	společnost
</s>
