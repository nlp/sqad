<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
archivů	archiv	k1gInPc2	archiv
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
MDA	MDA	kA	MDA
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
International	International	k1gMnSc1	International
Archives	Archives	k1gMnSc1	Archives
Day	Day	k1gMnSc1	Day
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
svátek	svátek	k1gInSc1	svátek
slavený	slavený	k2eAgInSc1d1	slavený
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
při	při	k7c6	při
jehož	jehož	k3xOyRp3gFnSc6	jehož
příležitosti	příležitost	k1gFnSc6	příležitost
archivy	archiv	k1gInPc1	archiv
připravují	připravovat	k5eAaImIp3nP	připravovat
společenské	společenský	k2eAgFnPc4d1	společenská
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
