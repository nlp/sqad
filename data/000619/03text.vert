<s>
Prvoci	prvok	k1gMnPc1	prvok
(	(	kIx(	(
<g/>
Protozoa	Protozoa	k1gMnSc1	Protozoa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
jednobuněčné	jednobuněčný	k2eAgFnPc4d1	jednobuněčná
eukaryotní	eukaryotní	k2eAgFnPc4d1	eukaryotní
heterotrofní	heterotrofní	k2eAgFnPc4d1	heterotrofní
(	(	kIx(	(
<g/>
či	či	k8xC	či
mixotrofní	mixotrofní	k2eAgInPc4d1	mixotrofní
<g/>
)	)	kIx)	)
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
dříve	dříve	k6eAd2	dříve
řazeny	řadit	k5eAaImNgFnP	řadit
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
Animalia	Animalia	k1gFnSc1	Animalia
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
pohyblivost	pohyblivost	k1gFnSc4	pohyblivost
a	a	k8xC	a
neschopnost	neschopnost	k1gFnSc4	neschopnost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
protistů	protista	k1gMnPc2	protista
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
Algae	Algae	k1gInSc1	Algae
<g/>
)	)	kIx)	)
či	či	k8xC	či
houbám	houba	k1gFnPc3	houba
podobní	podobný	k2eAgMnPc1d1	podobný
protisté	protista	k1gMnPc1	protista
(	(	kIx(	(
<g/>
hlenky	hlenka	k1gFnPc4	hlenka
<g/>
,	,	kIx,	,
oomycety	oomycet	k1gInPc4	oomycet
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
kolonie	kolonie	k1gFnSc1	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
tvořili	tvořit	k5eAaImAgMnP	tvořit
podskupinu	podskupina	k1gFnSc4	podskupina
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
také	také	k9	také
latinský	latinský	k2eAgInSc1d1	latinský
termín	termín	k1gInSc1	termín
protozoa	protozo	k1gInSc2	protozo
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
prvotní	prvotní	k2eAgMnPc1d1	prvotní
živočichové	živočich	k1gMnPc1	živočich
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
prvoků	prvok	k1gMnPc2	prvok
však	však	k9	však
prošla	projít	k5eAaPmAgFnS	projít
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
značným	značný	k2eAgInSc7d1	značný
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
prvoci	prvok	k1gMnPc1	prvok
řadí	řadit	k5eAaImIp3nP	řadit
do	do	k7c2	do
několika	několik	k4yIc2	několik
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
víceméně	víceméně	k9	víceméně
přirozených	přirozený	k2eAgFnPc2d1	přirozená
superskupin	superskupina	k1gFnPc2	superskupina
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
Amoebozoa	Amoebozo	k1gInSc2	Amoebozo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Archamoebae	Archamoeba	k1gFnSc2	Archamoeba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Opisthokonta	Opisthokonta	k1gFnSc1	Opisthokonta
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
trubénky	trubénka	k1gFnPc4	trubénka
<g/>
,	,	kIx,	,
Choanozoa	Choanozoa	k1gFnSc1	Choanozoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chromalveolata	Chromalveole	k1gNnPc1	Chromalveole
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nálevníci	nálevník	k1gMnPc1	nálevník
<g/>
,	,	kIx,	,
Ciliophora	Ciliophora	k1gFnSc1	Ciliophora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Excavata	Excavata	k1gFnSc1	Excavata
(	(	kIx(	(
<g/>
např.	např.	kA	např.
krásnoočka	krásnoočko	k1gNnPc4	krásnoočko
<g/>
,	,	kIx,	,
Euglenozoa	Euglenozoa	k1gFnSc1	Euglenozoa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rhizaria	Rhizarium	k1gNnSc2	Rhizarium
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dírkonošci	dírkonošek	k1gMnPc1	dírkonošek
<g/>
,	,	kIx,	,
Foraminifera	Foraminifera	k1gFnSc1	Foraminifera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumáním	zkoumání	k1gNnSc7	zkoumání
prvoků	prvok	k1gMnPc2	prvok
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
protozoologie	protozoologie	k1gFnSc1	protozoologie
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Alternativní	alternativní	k2eAgNnSc1d1	alternativní
pojmenování	pojmenování	k1gNnSc1	pojmenování
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
prvoky	prvok	k1gMnPc4	prvok
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
svých	svůj	k3xOyFgInPc2	svůj
mikroskopů	mikroskop	k1gInPc2	mikroskop
Holanďan	Holanďan	k1gMnSc1	Holanďan
Antoni	Anton	k1gMnPc1	Anton
van	vana	k1gFnPc2	vana
Leeuwenhoek	Leeuwenhoek	k6eAd1	Leeuwenhoek
<g/>
.	.	kIx.	.
</s>
<s>
Popsal	popsat	k5eAaPmAgMnS	popsat
a	a	k8xC	a
zakreslil	zakreslit	k5eAaPmAgMnS	zakreslit
především	především	k9	především
nálevníky	nálevník	k1gMnPc4	nálevník
(	(	kIx(	(
<g/>
Ciliophora	Ciliophor	k1gMnSc4	Ciliophor
<g/>
)	)	kIx)	)
a	a	k8xC	a
nazýval	nazývat	k5eAaImAgMnS	nazývat
je	on	k3xPp3gNnPc4	on
Animalcula	Animalculum	k1gNnPc4	Animalculum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
zvířátka	zvířátko	k1gNnPc4	zvířátko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
Animalcula	Animalcula	k1gFnSc1	Animalcula
Infusoria	Infusorium	k1gNnPc1	Infusorium
<g/>
,	,	kIx,	,
čili	čili	k1gNnSc1	čili
"	"	kIx"	"
<g/>
zvířátka	zvířátko	k1gNnPc4	zvířátko
z	z	k7c2	z
nálevů	nálev	k1gInPc2	nálev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
a	a	k8xC	a
mnohé	mnohé	k1gNnSc1	mnohé
další	další	k2eAgFnSc2d1	další
klasifikace	klasifikace	k1gFnSc2	klasifikace
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
víře	víra	k1gFnSc6	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvoci	prvok	k1gMnPc1	prvok
jsou	být	k5eAaImIp3nP	být
živočichové	živočich	k1gMnPc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
vyčlenil	vyčlenit	k5eAaPmAgInS	vyčlenit
jednobuněčné	jednobuněčný	k2eAgInPc4d1	jednobuněčný
heterotrofy	heterotrof	k1gInPc4	heterotrof
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Leuckart	Leuckarta	k1gFnPc2	Leuckarta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikaly	vznikat	k5eAaImAgInP	vznikat
další	další	k2eAgInPc1d1	další
nové	nový	k2eAgInPc1d1	nový
termíny	termín	k1gInPc1	termín
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Protoctista	Protoctista	k1gMnSc1	Protoctista
a	a	k8xC	a
Protista	protista	k1gMnSc1	protista
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nejen	nejen	k6eAd1	nejen
prvoky	prvok	k1gMnPc4	prvok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnohé	mnohý	k2eAgInPc4d1	mnohý
jiné	jiný	k2eAgInPc4d1	jiný
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
prvoci	prvok	k1gMnPc1	prvok
použil	použít	k5eAaPmAgInS	použít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
Jan	Jan	k1gMnSc1	Jan
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Presl	Presl	k1gInSc1	Presl
jako	jako	k8xC	jako
český	český	k2eAgInSc1d1	český
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
k	k	k7c3	k
termínu	termín	k1gInSc3	termín
Infusoria	Infusorium	k1gNnSc2	Infusorium
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasifikace	klasifikace	k1gFnSc2	klasifikace
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
domény	doména	k1gFnSc2	doména
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
i	i	k8xC	i
klasifikací	klasifikace	k1gFnPc2	klasifikace
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Eukaryotická	Eukaryotický	k2eAgFnSc1d1	Eukaryotická
buňka	buňka	k1gFnSc1	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
prvoků	prvok	k1gMnPc2	prvok
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gNnSc4	on
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
-	-	kIx~	-
nejčastěji	často	k6eAd3	často
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
rozměrů	rozměr	k1gInPc2	rozměr
od	od	k7c2	od
0,005	[number]	k4	0,005
do	do	k7c2	do
0,05	[number]	k4	0,05
mm	mm	kA	mm
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc1d1	běžná
i	i	k8xC	i
formy	forma	k1gFnPc1	forma
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
0,5	[number]	k4	0,5
mm	mm	kA	mm
-	-	kIx~	-
ale	ale	k8xC	ale
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
pozorovatelní	pozorovatelný	k2eAgMnPc1d1	pozorovatelný
pomocí	pomocí	k7c2	pomocí
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
prvoci	prvok	k1gMnPc1	prvok
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
viditelní	viditelný	k2eAgMnPc1d1	viditelný
i	i	k8xC	i
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
měňavka	měňavka	k1gFnSc1	měňavka
bahenní	bahenní	k2eAgFnSc1d1	bahenní
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
velikosti	velikost	k1gFnPc4	velikost
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
buňka	buňka	k1gFnSc1	buňka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
organely	organela	k1gFnPc4	organela
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
pohyb	pohyb	k1gInSc4	pohyb
<g/>
,	,	kIx,	,
získávání	získávání	k1gNnSc4	získávání
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
trávení	trávení	k1gNnSc6	trávení
<g/>
,	,	kIx,	,
vylučování	vylučování	k1gNnSc6	vylučování
nestrávených	strávený	k2eNgInPc2d1	nestrávený
zbytků	zbytek	k1gInPc2	zbytek
a	a	k8xC	a
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
a	a	k8xC	a
často	často	k6eAd1	často
nemají	mít	k5eNaImIp3nP	mít
obdobu	obdoba	k1gFnSc4	obdoba
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
jiných	jiný	k2eAgInPc2d1	jiný
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
např.	např.	kA	např.
buněčná	buněčný	k2eAgNnPc1d1	buněčné
ústa	ústa	k1gNnPc1	ústa
<g/>
,	,	kIx,	,
extruzomy	extruzom	k1gInPc1	extruzom
<g/>
,	,	kIx,	,
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvoci	prvok	k1gMnPc1	prvok
mají	mít	k5eAaImIp3nP	mít
jedno	jeden	k4xCgNnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rovnocenných	rovnocenný	k2eAgFnPc2d1	rovnocenná
či	či	k8xC	či
funkčně	funkčně	k6eAd1	funkčně
odlišných	odlišný	k2eAgNnPc2d1	odlišné
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Množí	množit	k5eAaImIp3nP	množit
se	se	k3xPyFc4	se
nepohlavně	pohlavně	k6eNd1	pohlavně
(	(	kIx(	(
<g/>
dělením	dělení	k1gNnSc7	dělení
nebo	nebo	k8xC	nebo
rozpadem	rozpad	k1gInSc7	rozpad
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pohlavně	pohlavně	k6eAd1	pohlavně
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
oba	dva	k4xCgInPc1	dva
způsoby	způsob	k1gInPc1	způsob
napevno	napevno	k6eAd1	napevno
střídají	střídat	k5eAaImIp3nP	střídat
v	v	k7c6	v
pravidelném	pravidelný	k2eAgInSc6d1	pravidelný
životním	životní	k2eAgInSc6d1	životní
cyklu	cyklus	k1gInSc6	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
volně	volně	k6eAd1	volně
nebo	nebo	k8xC	nebo
cizopasí	cizopasit	k5eAaImIp3nS	cizopasit
<g/>
,	,	kIx,	,
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
a	a	k8xC	a
heterotrofní	heterotrofní	k2eAgInSc1d1	heterotrofní
způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Známo	znám	k2eAgNnSc1d1	známo
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
30	[number]	k4	30
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
semipermeabilní	semipermeabilní	k2eAgFnSc1d1	semipermeabilní
blána	blána	k1gFnSc1	blána
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
(	(	kIx(	(
<g/>
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
však	však	k9	však
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
(	(	kIx(	(
<g/>
mimobuňková	mimobuňková	k1gFnSc1	mimobuňková
stadia	stadion	k1gNnSc2	stadion
Apicomplexa	Apicomplex	k1gInSc2	Apicomplex
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
trojitá	trojitý	k2eAgFnSc1d1	trojitá
(	(	kIx(	(
<g/>
Gregarina	Gregarina	k1gFnSc1	Gregarina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zpevněna	zpevněn	k2eAgFnSc1d1	zpevněna
systémem	systém	k1gInSc7	systém
mikrotubulů	mikrotubul	k1gMnPc2	mikrotubul
<g/>
,	,	kIx,	,
<g/>
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
kortex	kortex	k1gInSc1	kortex
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pelikula	pelikula	k1gFnSc1	pelikula
(	(	kIx(	(
<g/>
Opalinida	Opalinida	k1gFnSc1	Opalinida
<g/>
,	,	kIx,	,
Ciliophora	Ciliophora	k1gFnSc1	Ciliophora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgFnPc2d1	mnohá
skupin	skupina	k1gFnPc2	skupina
produkuje	produkovat	k5eAaImIp3nS	produkovat
schránku	schránka	k1gFnSc4	schránka
<g/>
;	;	kIx,	;
cytoplazma	cytoplazma	k1gFnSc1	cytoplazma
-	-	kIx~	-
není	být	k5eNaImIp3nS	být
organela	organela	k1gFnSc1	organela
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obaluje	obalovat	k5eAaImIp3nS	obalovat
všechny	všechen	k3xTgFnPc4	všechen
organely	organela	k1gFnPc4	organela
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
diferencovaná	diferencovaný	k2eAgFnSc1d1	diferencovaná
na	na	k7c4	na
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
viskózní	viskózní	k2eAgFnSc4d1	viskózní
ektoplazmu	ektoplazma	k1gFnSc4	ektoplazma
a	a	k8xC	a
zrnitější	zrnitý	k2eAgFnSc4d2	zrnitější
entoplazmu	entoplazma	k1gFnSc4	entoplazma
uvnitř	uvnitř	k7c2	uvnitř
buňky	buňka	k1gFnSc2	buňka
(	(	kIx(	(
<g/>
Ciliophora	Ciliophora	k1gFnSc1	Ciliophora
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Sarcodina	Sarcodina	k1gFnSc1	Sarcodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
vrstvy	vrstva	k1gFnPc1	vrstva
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
zřetelně	zřetelně	k6eAd1	zřetelně
oddělené	oddělený	k2eAgNnSc1d1	oddělené
(	(	kIx(	(
<g/>
Lobosia	Lobosia	k1gFnSc1	Lobosia
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
nukleus	nukleus	k1gInSc1	nukleus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
organelou	organela	k1gFnSc7	organela
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
DNA	dna	k1gFnSc1	dna
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
k	k	k7c3	k
syntéze	syntéza	k1gFnSc3	syntéza
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jedno	jeden	k4xCgNnSc4	jeden
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
prvoků	prvok	k1gMnPc2	prvok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc1	dva
(	(	kIx(	(
<g/>
makronukleus	makronukleus	k1gInSc1	makronukleus
a	a	k8xC	a
mikronukleus	mikronukleus	k1gInSc1	mikronukleus
u	u	k7c2	u
Ciliophora	Ciliophor	k1gMnSc2	Ciliophor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
víc	hodně	k6eAd2	hodně
nediferencovaných	diferencovaný	k2eNgNnPc2d1	nediferencované
jader	jádro	k1gNnPc2	jádro
(	(	kIx(	(
<g/>
Opalinata	Opalinata	k1gFnSc1	Opalinata
<g/>
,	,	kIx,	,
víceré	vícerý	k4xRyIgInPc1	vícerý
Sarcodina	Sarcodin	k2eAgMnSc4d1	Sarcodin
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
-	-	kIx~	-
dvojvrstvé	dvojvrstvý	k2eAgFnPc1d1	dvojvrstvá
membránozní	membránozní	k2eAgFnPc1d1	membránozní
organely	organela	k1gFnPc1	organela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
životní	životní	k2eAgInPc4d1	životní
pochody	pochod	k1gInPc4	pochod
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krevních	krevní	k2eAgMnPc2d1	krevní
parazitů	parazit	k1gMnPc2	parazit
mohou	moct	k5eAaImIp3nP	moct
atrofovat	atrofovat	k5eAaImF	atrofovat
(	(	kIx(	(
<g/>
glukóza	glukóza	k1gFnSc1	glukóza
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
živiny	živina	k1gFnPc1	živina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Golgiho	Golgi	k1gMnSc2	Golgi
aparát	aparát	k1gInSc1	aparát
-	-	kIx~	-
tubulózní	tubulózní	k2eAgFnSc1d1	tubulózní
organela	organela	k1gFnSc1	organela
<g/>
,	,	kIx,	,
zabezpečuje	zabezpečovat	k5eAaImIp3nS	zabezpečovat
sekreční	sekreční	k2eAgFnSc4d1	sekreční
činnost	činnost	k1gFnSc4	činnost
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlinných	rostlinný	k2eAgMnPc2d1	rostlinný
bičíkovců	bičíkovec	k1gMnPc2	bičíkovec
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
nazývá	nazývat	k5eAaImIp3nS	nazývat
diktyozom	diktyozom	k1gInSc1	diktyozom
<g/>
;	;	kIx,	;
fagozom	fagozom	k1gInSc1	fagozom
<g/>
,	,	kIx,	,
potravní	potravní	k2eAgFnSc1d1	potravní
vakuola	vakuola	k1gFnSc1	vakuola
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
vlečením	vlečení	k1gNnSc7	vlečení
buňkové	buňkový	k2eAgFnSc2d1	buňková
membrány	membrána	k1gFnSc2	membrána
do	do	k7c2	do
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
Splývá	splývat	k5eAaImIp3nS	splývat
s	s	k7c7	s
lyzozomem	lyzozom	k1gInSc7	lyzozom
<g/>
,	,	kIx,	,
s	s	k7c7	s
měchýřkem	měchýřek	k1gInSc7	měchýřek
naplněným	naplněný	k2eAgInSc7d1	naplněný
trávícími	trávící	k2eAgInPc7d1	trávící
enzymy	enzym	k1gInPc7	enzym
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
typické	typický	k2eAgNnSc1d1	typické
intracelulární	intracelulární	k2eAgNnSc1d1	intracelulární
trávení	trávení	k1gNnSc1	trávení
<g/>
;	;	kIx,	;
pulzující	pulzující	k2eAgFnSc2d1	pulzující
(	(	kIx(	(
<g/>
kontraktilní	kontraktilní	k2eAgFnSc2d1	kontraktilní
<g/>
)	)	kIx)	)
vakuoly	vakuola	k1gFnSc2	vakuola
-	-	kIx~	-
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
nadbytečnou	nadbytečný	k2eAgFnSc4d1	nadbytečná
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
regulují	regulovat	k5eAaImIp3nP	regulovat
tedy	tedy	k9	tedy
osmotický	osmotický	k2eAgInSc4d1	osmotický
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
;	;	kIx,	;
mikrotubuly	mikrotubula	k1gFnSc2	mikrotubula
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
oporně-pohybovou	oporněohybový	k2eAgFnSc4d1	oporně-pohybový
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
základem	základ	k1gInSc7	základ
bičíků	bičík	k1gInPc2	bičík
a	a	k8xC	a
brv	brva	k1gFnPc2	brva
a	a	k8xC	a
podílejí	podílet	k5eAaImIp3nP	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
pelikuly	pelikula	k1gFnSc2	pelikula
a	a	k8xC	a
mitotického	mitotický	k2eAgNnSc2d1	mitotické
vřeténka	vřeténko	k1gNnSc2	vřeténko
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
prvoků	prvok	k1gMnPc2	prvok
je	být	k5eAaImIp3nS	být
pasivní	pasivní	k2eAgFnSc1d1	pasivní
(	(	kIx(	(
<g/>
prouděním	proudění	k1gNnSc7	proudění
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
pohyb	pohyb	k1gInSc1	pohyb
může	moct	k5eAaImIp3nS	moct
byť	byť	k8xS	byť
améboidní	améboidní	k2eAgNnSc1d1	améboidní
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přeléváním	přelévání	k1gNnSc7	přelévání
<g/>
"	"	kIx"	"
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
tohoto	tento	k3xDgInSc2	tento
pohybu	pohyb	k1gInSc2	pohyb
stále	stále	k6eAd1	stále
přesně	přesně	k6eAd1	přesně
neznáme	znát	k5eNaImIp1nP	znát
(	(	kIx(	(
<g/>
půjde	jít	k5eAaImIp3nS	jít
zřejmě	zřejmě	k6eAd1	zřejmě
o	o	k7c4	o
stěhování	stěhování	k1gNnSc4	stěhování
plazmatických	plazmatický	k2eAgFnPc2d1	plazmatická
makromolekul	makromolekula	k1gFnPc2	makromolekula
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
brv	brva	k1gFnPc2	brva
(	(	kIx(	(
<g/>
cilie	cilie	k1gFnSc1	cilie
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
bičíků	bičík	k1gInPc2	bičík
(	(	kIx(	(
<g/>
flagellum	flagellum	k1gNnSc1	flagellum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bičík	bičík	k1gInSc1	bičík
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přiložit	přiložit	k5eAaPmF	přiložit
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
undulující	undulující	k2eAgFnSc1d1	undulující
membrána	membrána	k1gFnSc1	membrána
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
ji	on	k3xPp3gFnSc4	on
např.	např.	kA	např.
Trypanosoma	trypanosoma	k1gFnSc1	trypanosoma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Splýváním	splývání	k1gNnSc7	splývání
brv	brva	k1gFnPc2	brva
vznikají	vznikat	k5eAaImIp3nP	vznikat
ciliální	ciliální	k2eAgInPc1d1	ciliální
deriváty	derivát	k1gInPc1	derivát
-	-	kIx~	-
cirry	cirr	k1gInPc1	cirr
nebo	nebo	k8xC	nebo
membranely	membranel	k1gInPc1	membranel
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Prvoci	prvok	k1gMnPc1	prvok
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
tradičně	tradičně	k6eAd1	tradičně
dělili	dělit	k5eAaImAgMnP	dělit
právě	právě	k9	právě
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
pohybu	pohyb	k1gInSc2	pohyb
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
bičíkovci	bičíkovec	k1gMnPc7	bičíkovec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krásnoočko	krásnoočko	k1gNnSc4	krásnoočko
-	-	kIx~	-
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
bičíků	bičík	k1gInPc2	bičík
<g/>
;	;	kIx,	;
kořenonožci	kořenonožec	k1gMnPc1	kořenonožec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
měňavka	měňavka	k1gFnSc1	měňavka
-	-	kIx~	-
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
panožek	panožka	k1gFnPc2	panožka
<g/>
;	;	kIx,	;
nálevníci	nálevník	k1gMnPc1	nálevník
<g/>
,	,	kIx,	,
např.	např.	kA	např.
trepka	trepka	k1gFnSc1	trepka
-	-	kIx~	-
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
řasinek	řasinka	k1gFnPc2	řasinka
<g/>
;	;	kIx,	;
výtrusovci	výtrusovec	k1gMnPc1	výtrusovec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zimnička	zimnička	k1gFnSc1	zimnička
-	-	kIx~	-
specializovaní	specializovaný	k2eAgMnPc1d1	specializovaný
parazité	parazit	k1gMnPc1	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
potravy	potrava	k1gFnSc2	potrava
se	se	k3xPyFc4	se
uskutečňuje	uskutečňovat	k5eAaImIp3nS	uskutečňovat
<g/>
:	:	kIx,	:
difuzí	difuze	k1gFnSc7	difuze
přes	přes	k7c4	přes
cytoplazmatickou	cytoplazmatický	k2eAgFnSc4d1	cytoplazmatická
membránu	membrána	k1gFnSc4	membrána
(	(	kIx(	(
<g/>
nízkomolekulové	nízkomolekulový	k2eAgFnPc4d1	nízkomolekulový
látky	látka	k1gFnPc4	látka
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
endocytózou	endocytóza	k1gFnSc7	endocytóza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
varianty	varianta	k1gFnSc2	varianta
<g/>
:	:	kIx,	:
pinocytózu	pinocytóza	k1gFnSc4	pinocytóza
-	-	kIx~	-
pohlcování	pohlcování	k1gNnSc1	pohlcování
makromolekul	makromolekula	k1gFnPc2	makromolekula
<g/>
,	,	kIx,	,
tukových	tukový	k2eAgFnPc2d1	tuková
kapiček	kapička	k1gFnPc2	kapička
a	a	k8xC	a
drobných	drobný	k2eAgFnPc2d1	drobná
částeček	částečka	k1gFnPc2	částečka
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
10	[number]	k4	10
μ	μ	k?	μ
nebo	nebo	k8xC	nebo
fagocytózu	fagocytóza	k1gFnSc4	fagocytóza
-	-	kIx~	-
pohlcování	pohlcování	k1gNnSc4	pohlcování
větších	veliký	k2eAgFnPc2d2	veliký
částic	částice	k1gFnPc2	částice
potravy	potrava	k1gFnSc2	potrava
(	(	kIx(	(
<g/>
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
Sarcodina	Sarcodin	k2eAgNnPc1d1	Sarcodin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
endocytóze	endocytóza	k1gFnSc6	endocytóza
sa	sa	k?	sa
částice	částice	k1gFnSc1	částice
dostává	dostávat	k5eAaImIp3nS	dostávat
dovnitř	dovnitř	k7c2	dovnitř
buňky	buňka	k1gFnSc2	buňka
vlečením	vlečení	k1gNnSc7	vlečení
povrchové	povrchový	k2eAgFnSc2d1	povrchová
membrány	membrána	k1gFnSc2	membrána
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
utvoří	utvořit	k5eAaPmIp3nS	utvořit
okolo	okolo	k7c2	okolo
částice	částice	k1gFnSc2	částice
měchýřek	měchýřek	k1gInSc1	měchýřek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
na	na	k7c4	na
fagocytózu	fagocytóza	k1gFnSc4	fagocytóza
specializuje	specializovat	k5eAaBmIp3nS	specializovat
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
příjmu	příjem	k1gInSc6	příjem
potravy	potrava	k1gFnSc2	potrava
cytostómem	cytostóm	k1gInSc7	cytostóm
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
pro	pro	k7c4	pro
Ciliophora	Ciliophor	k1gMnSc4	Ciliophor
<g/>
.	.	kIx.	.
</s>
<s>
Nestrávené	strávený	k2eNgInPc1d1	nestrávený
zbytky	zbytek	k1gInPc1	zbytek
opouštějí	opouštět	k5eAaImIp3nP	opouštět
buňku	buňka	k1gFnSc4	buňka
exocytózou	exocytóza	k1gFnSc7	exocytóza
přes	přes	k7c4	přes
cytopygy	cytopyga	k1gFnPc4	cytopyga
-	-	kIx~	-
buňkový	buňkový	k2eAgInSc4d1	buňkový
"	"	kIx"	"
<g/>
anus	anus	k1gInSc4	anus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
době	doba	k1gFnSc6	doba
defekace	defekace	k1gFnSc2	defekace
<g/>
.	.	kIx.	.
</s>
<s>
Nepohlavní	pohlavní	k2eNgNnSc1d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
je	být	k5eAaImIp3nS	být
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
v	v	k7c6	v
několika	několik	k4yIc6	několik
typech	typ	k1gInPc6	typ
<g/>
:	:	kIx,	:
binární	binární	k2eAgNnSc1d1	binární
dělení	dělení	k1gNnSc1	dělení
-	-	kIx~	-
buňka	buňka	k1gFnSc1	buňka
sa	sa	k?	sa
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
na	na	k7c6	na
2	[number]	k4	2
stejné	stejná	k1gFnSc6	stejná
jedince	jedinec	k1gMnPc4	jedinec
<g/>
,	,	kIx,	,
a	a	k8xC	a
děje	dít	k5eAaImIp3nS	dít
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
podélném	podélný	k2eAgMnSc6d1	podélný
(	(	kIx(	(
<g/>
Mastigophora	Mastigophora	k1gFnSc1	Mastigophora
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
příčném	příčný	k2eAgInSc6d1	příčný
směru	směr	k1gInSc6	směr
(	(	kIx(	(
<g/>
Ciliophora	Ciliophora	k1gFnSc1	Ciliophora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
pučení	pučení	k1gNnSc1	pučení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
mateřského	mateřský	k2eAgMnSc2d1	mateřský
jedince	jedinec	k1gMnSc2	jedinec
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgInPc1d2	menší
pupeny	pupen	k1gInPc1	pupen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
potom	potom	k6eAd1	potom
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
(	(	kIx(	(
<g/>
Suctoria	Suctorium	k1gNnPc4	Suctorium
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
polytomie	polytomie	k1gFnSc1	polytomie
-	-	kIx~	-
rozpad	rozpad	k1gInSc1	rozpad
na	na	k7c4	na
více	hodně	k6eAd2	hodně
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
variantách	varianta	k1gFnPc6	varianta
<g/>
:	:	kIx,	:
sporogonie	sporogonie	k1gFnSc1	sporogonie
a	a	k8xC	a
schizogonie	schizogonie	k1gFnSc1	schizogonie
(	(	kIx(	(
<g/>
Apicomplexa	Apicomplexa	k1gFnSc1	Apicomplexa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
-	-	kIx~	-
Prvoci	prvok	k1gMnPc1	prvok
mají	mít	k5eAaImIp3nP	mít
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
života	život	k1gInSc2	život
haploidní	haploidní	k2eAgFnSc4d1	haploidní
sadu	sada	k1gFnSc4	sada
chromozómů	chromozóm	k1gInPc2	chromozóm
(	(	kIx(	(
<g/>
Mastigophora	Mastigophora	k1gFnSc1	Mastigophora
<g/>
,	,	kIx,	,
Apicomplexa	Apicomplexa	k1gFnSc1	Apicomplexa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diploidní	diploidní	k2eAgFnSc1d1	diploidní
jsou	být	k5eAaImIp3nP	být
Opalinata	Opalinat	k1gMnSc4	Opalinat
a	a	k8xC	a
Ciliophora	Ciliophor	k1gMnSc4	Ciliophor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvém	prvý	k4xOgInSc6	prvý
případě	případ	k1gInSc6	případ
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
redukci	redukce	k1gFnSc3	redukce
počtu	počet	k1gInSc2	počet
chromozómů	chromozóm	k1gInPc2	chromozóm
při	při	k7c6	při
prvém	prvý	k4xOgInSc6	prvý
dělení	dělení	k1gNnSc6	dělení
zygoty	zygota	k1gFnSc2	zygota
<g/>
,	,	kIx,	,
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
meióza	meióza	k1gFnSc1	meióza
probíhá	probíhat	k5eAaImIp3nS	probíhat
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
gamet	gamet	k1gInSc1	gamet
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgNnSc1d1	pohlavní
rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
3	[number]	k4	3
varianty	varianta	k1gFnSc2	varianta
<g/>
:	:	kIx,	:
gametogamie	gametogamie	k1gFnSc1	gametogamie
-	-	kIx~	-
splývání	splývání	k1gNnSc1	splývání
dvou	dva	k4xCgFnPc2	dva
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
gamet	gameta	k1gFnPc2	gameta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
stejné	stejný	k2eAgFnPc1d1	stejná
(	(	kIx(	(
<g/>
izogamie	izogamie	k1gFnPc1	izogamie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
morfologicky	morfologicky	k6eAd1	morfologicky
odlišené	odlišený	k2eAgNnSc1d1	odlišené
(	(	kIx(	(
<g/>
anizogamie	anizogamie	k1gFnSc1	anizogamie
-	-	kIx~	-
Apicomplexa	Apicomplexa	k1gFnSc1	Apicomplexa
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
gamontogamie	gamontogamie	k1gFnSc1	gamontogamie
-	-	kIx~	-
kopulace	kopulace	k1gFnSc1	kopulace
(	(	kIx(	(
<g/>
jader	jádro	k1gNnPc2	jádro
<g/>
)	)	kIx)	)
gamet	gamet	k1gInSc1	gamet
předchází	předcházet	k5eAaImIp3nS	předcházet
spojení	spojení	k1gNnSc4	spojení
celých	celý	k2eAgFnPc2d1	celá
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
aspoň	aspoň	k9	aspoň
jejich	jejich	k3xOp3gFnPc2	jejich
větších	veliký	k2eAgFnPc2d2	veliký
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
Gregarina	Gregarina	k1gFnSc1	Gregarina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
gamontogamie	gamontogamie	k1gFnSc2	gamontogamie
je	být	k5eAaImIp3nS	být
konjugace	konjugace	k1gFnSc1	konjugace
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
u	u	k7c2	u
Ciliophora	Ciliophor	k1gMnSc2	Ciliophor
<g/>
:	:	kIx,	:
Dva	dva	k4xCgMnPc1	dva
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
přiloží	přiložit	k5eAaPmIp3nS	přiložit
cytostomem	cytostom	k1gInSc7	cytostom
a	a	k8xC	a
splynou	splynout	k5eAaPmIp3nP	splynout
<g/>
.	.	kIx.	.
</s>
<s>
Makronukleus	Makronukleus	k1gInSc1	Makronukleus
se	se	k3xPyFc4	se
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
<g/>
,	,	kIx,	,
mikronukleus	mikronukleus	k1gInSc1	mikronukleus
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
buňce	buňka	k1gFnSc6	buňka
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
mitoticky	mitoticky	k6eAd1	mitoticky
dělí	dělit	k5eAaImIp3nP	dělit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikají	vznikat	k5eAaImIp3nP	vznikat
4	[number]	k4	4
malá	malý	k2eAgNnPc4d1	malé
jádra	jádro	k1gNnPc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
rozpouštějí	rozpouštět	k5eAaImIp3nP	rozpouštět
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc4d1	poslední
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
meioticky	meioticky	k6eAd1	meioticky
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
stacionární	stacionární	k2eAgNnSc4d1	stacionární
a	a	k8xC	a
migrativní	migrativní	k2eAgNnSc4d1	migrativní
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Migrativní	Migrativní	k2eAgNnPc1d1	Migrativní
jádra	jádro	k1gNnPc1	jádro
si	se	k3xPyFc3	se
jedince	jedinko	k6eAd1	jedinko
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
vymění	vyměnit	k5eAaPmIp3nP	vyměnit
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
každé	každý	k3xTgNnSc1	každý
splývá	splývat	k5eAaImIp3nS	splývat
se	s	k7c7	s
stacionárním	stacionární	k2eAgNnSc7d1	stacionární
jádrem	jádro	k1gNnSc7	jádro
druhého	druhý	k4xOgMnSc2	druhý
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
synkaryon	synkaryon	k1gNnSc4	synkaryon
a	a	k8xC	a
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
<g/>
.	.	kIx.	.
</s>
<s>
Dost	dost	k6eAd1	dost
složitým	složitý	k2eAgNnSc7d1	složité
dělením	dělení	k1gNnSc7	dělení
synkaryonu	synkaryon	k1gInSc2	synkaryon
v	v	k7c6	v
každém	každý	k3xTgMnSc6	každý
jedinci	jedinec	k1gMnPc1	jedinec
vznikají	vznikat	k5eAaImIp3nP	vznikat
2	[number]	k4	2
makro	makro	k1gNnSc1	makro
<g/>
-	-	kIx~	-
a	a	k8xC	a
2	[number]	k4	2
mikronukleusy	mikronukleus	k1gInPc7	mikronukleus
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výsledkem	výsledek	k1gInSc7	výsledek
konjugace	konjugace	k1gFnSc2	konjugace
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
jedinci	jedinec	k1gMnSc3	jedinec
<g/>
;	;	kIx,	;
autogamie	autogamie	k1gFnSc1	autogamie
-	-	kIx~	-
proces	proces	k1gInSc1	proces
samooplodnění	samooplodnění	k1gNnSc2	samooplodnění
jádry	jádro	k1gNnPc7	jádro
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
samém	samý	k3xTgNnSc6	samý
jedinci	jedinec	k1gMnSc3	jedinec
(	(	kIx(	(
<g/>
Foraminifera	Foraminifera	k1gFnSc1	Foraminifera
<g/>
,	,	kIx,	,
Heliozoa	Heliozoa	k1gFnSc1	Heliozoa
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
jinde	jinde	k6eAd1	jinde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metageneze	metageneze	k1gFnSc1	metageneze
-	-	kIx~	-
střídání	střídání	k1gNnSc1	střídání
pohlavního	pohlavní	k2eAgNnSc2d1	pohlavní
a	a	k8xC	a
nepohlavního	pohlavní	k2eNgNnSc2d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
VYLUČOVÁNÍ	vylučování	k1gNnSc1	vylučování
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Golgiho	Golgi	k1gMnSc2	Golgi
aparát	aparát	k1gInSc4	aparát
Prvoci	prvok	k1gMnPc1	prvok
jsou	být	k5eAaImIp3nP	být
rozšířeni	rozšířit	k5eAaPmNgMnP	rozšířit
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
trochu	trochu	k6eAd1	trochu
vlhké	vlhký	k2eAgNnSc4d1	vlhké
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
je	on	k3xPp3gFnPc4	on
ve	v	k7c6	v
sladké	sladký	k2eAgFnSc6d1	sladká
i	i	k8xC	i
slané	slaný	k2eAgFnSc6d1	slaná
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
i	i	k9	i
uvnitř	uvnitř	k7c2	uvnitř
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Hrají	hrát	k5eAaImIp3nP	hrát
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
ekologii	ekologie	k1gFnSc6	ekologie
-	-	kIx~	-
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
např.	např.	kA	např.
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
samočištění	samočištění	k1gNnSc4	samočištění
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
půdách	půda	k1gFnPc6	půda
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
společenstva	společenstvo	k1gNnSc2	společenstvo
půdních	půdní	k2eAgInPc2d1	půdní
organizmů	organizmus	k1gInPc2	organizmus
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
edafon	edafon	k1gInSc1	edafon
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
přežívat	přežívat	k5eAaImF	přežívat
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
i	i	k8xC	i
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
žít	žít	k5eAaImF	žít
jen	jen	k9	jen
v	v	k7c6	v
teplých	teplý	k2eAgInPc6d1	teplý
mořích	moře	k1gNnPc6	moře
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
studené	studený	k2eAgFnPc1d1	studená
polární	polární	k2eAgFnPc1d1	polární
mořské	mořský	k2eAgFnPc1d1	mořská
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgFnSc7d3	nejvhodnější
teplotou	teplota	k1gFnSc7	teplota
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozmezí	rozmezí	k1gNnSc1	rozmezí
mezi	mezi	k7c7	mezi
15	[number]	k4	15
až	až	k9	až
25	[number]	k4	25
°	°	k?	°
<g/>
C.	C.	kA	C.
I	i	k9	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
prvoci	prvok	k1gMnPc1	prvok
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
42	[number]	k4	42
°	°	k?	°
<g/>
C	C	kA	C
hyne	hynout	k5eAaImIp3nS	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
prvoky	prvok	k1gMnPc4	prvok
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
ke	k	k7c3	k
sterilizaci	sterilizace	k1gFnSc3	sterilizace
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mechu	mech	k1gInSc6	mech
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
převážně	převážně	k6eAd1	převážně
kořenonožce	kořenonožec	k1gMnPc4	kořenonožec
a	a	k8xC	a
nálevníky	nálevník	k1gMnPc4	nálevník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dovedou	dovést	k5eAaPmIp3nP	dovést
snášet	snášet	k5eAaImF	snášet
velké	velký	k2eAgInPc4d1	velký
teplotní	teplotní	k2eAgInPc4d1	teplotní
a	a	k8xC	a
vlhkostní	vlhkostní	k2eAgInPc4d1	vlhkostní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgMnPc4d1	mořský
prvoky	prvok	k1gMnPc4	prvok
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
jak	jak	k6eAd1	jak
u	u	k7c2	u
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
tak	tak	k9	tak
při	při	k7c6	při
mořské	mořský	k2eAgFnSc6d1	mořská
hladině	hladina	k1gFnSc6	hladina
<g/>
.	.	kIx.	.
</s>
<s>
Prvoci	prvok	k1gMnPc1	prvok
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgNnSc1d1	obývající
mořské	mořský	k2eAgNnSc1d1	mořské
dno	dno	k1gNnSc1	dno
(	(	kIx(	(
<g/>
benthos	benthos	k1gInSc1	benthos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
dírkonošci	dírkonošek	k1gMnPc1	dírkonošek
s	s	k7c7	s
pevnou	pevný	k2eAgFnSc7d1	pevná
schránkou	schránka	k1gFnSc7	schránka
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
po	po	k7c6	po
mořském	mořský	k2eAgInSc6d1	mořský
dnu	den	k1gInSc6	den
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
nebo	nebo	k8xC	nebo
žijí	žít	k5eAaImIp3nP	žít
přisedle	přisedle	k6eAd1	přisedle
na	na	k7c6	na
podkladu	podklad	k1gInSc6	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Mřížovci	Mřížovec	k1gMnPc1	Mřížovec
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
volném	volný	k2eAgNnSc6d1	volné
moři	moře	k1gNnSc6	moře
(	(	kIx(	(
<g/>
pelagiál	pelagiál	k1gInSc1	pelagiál
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
mořského	mořský	k2eAgInSc2d1	mořský
planktonu	plankton	k1gInSc2	plankton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
počtu	počet	k1gInSc6	počet
pak	pak	k6eAd1	pak
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
obývají	obývat	k5eAaImIp3nP	obývat
i	i	k9	i
bičíkovci	bičíkovec	k1gMnPc1	bičíkovec
a	a	k8xC	a
dírkonošci	dírkonošek	k1gMnPc1	dírkonošek
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
skupinu	skupina	k1gFnSc4	skupina
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
jejich	jejich	k3xOp3gNnSc2	jejich
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
představují	představovat	k5eAaImIp3nP	představovat
parazitičtí	parazitický	k2eAgMnPc1d1	parazitický
prvoci	prvok	k1gMnPc1	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Protozoózy	Protozoóza	k1gFnSc2	Protozoóza
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
je	on	k3xPp3gFnPc4	on
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c4	na
ektoparazity	ektoparazit	k1gMnPc4	ektoparazit
a	a	k8xC	a
endoparazity	endoparazit	k1gMnPc4	endoparazit
<g/>
.	.	kIx.	.
</s>
<s>
Ektoparazité	ektoparazit	k1gMnPc1	ektoparazit
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vodních	vodní	k2eAgMnPc2d1	vodní
a	a	k8xC	a
vlhkomilných	vlhkomilný	k2eAgMnPc2d1	vlhkomilný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnPc3	svůj
hostitelům	hostitel	k1gMnPc3	hostitel
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
způsobit	způsobit	k5eAaPmF	způsobit
těžká	těžký	k2eAgNnPc4d1	těžké
onemocnění	onemocnění	k1gNnPc4	onemocnění
a	a	k8xC	a
často	často	k6eAd1	často
je	on	k3xPp3gMnPc4	on
i	i	k9	i
usmrcují	usmrcovat	k5eAaImIp3nP	usmrcovat
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgMnPc7d1	typický
představiteli	představitel	k1gMnPc7	představitel
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
prvoků	prvok	k1gMnPc2	prvok
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
bičivka	bičivka	k1gFnSc1	bičivka
rybí	rybí	k2eAgFnSc1d1	rybí
<g/>
,	,	kIx,	,
čepelenka	čepelenka	k1gFnSc1	čepelenka
kapří	kapří	k2eAgFnSc1d1	kapří
nebo	nebo	k8xC	nebo
kožovec	kožovec	k1gInSc1	kožovec
rybí	rybí	k2eAgInSc1d1	rybí
<g/>
.	.	kIx.	.
</s>
<s>
Endoparazité	endoparazit	k1gMnPc1	endoparazit
přežívají	přežívat	k5eAaImIp3nP	přežívat
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
nebo	nebo	k8xC	nebo
orgánech	orgán	k1gInPc6	orgán
svých	svůj	k3xOyFgMnPc2	svůj
hostitelů	hostitel	k1gMnPc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
svému	svůj	k3xOyFgMnSc3	svůj
hostiteli	hostitel	k1gMnSc3	hostitel
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
měňavka	měňavka	k1gFnSc1	měňavka
střevní	střevní	k2eAgFnSc1d1	střevní
nebo	nebo	k8xC	nebo
trypanozomy	trypanozoma	k1gFnPc1	trypanozoma
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
však	však	k9	však
přítomnost	přítomnost	k1gFnSc1	přítomnost
prvoků	prvok	k1gMnPc2	prvok
naopak	naopak	k6eAd1	naopak
hostiteli	hostitel	k1gMnSc3	hostitel
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
-	-	kIx~	-
tak	tak	k8xC	tak
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
termitů	termit	k1gMnPc2	termit
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
střevech	střevo	k1gNnPc6	střevo
bičíkovce	bičíkovec	k1gMnSc2	bičíkovec
nebo	nebo	k8xC	nebo
u	u	k7c2	u
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
s	s	k7c7	s
nálevníky	nálevník	k1gMnPc7	nálevník
v	v	k7c6	v
bachoru	bachor	k1gInSc6	bachor
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
prvoci	prvok	k1gMnPc1	prvok
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
svým	svůj	k3xOyFgMnPc3	svůj
hostitelům	hostitel	k1gMnPc3	hostitel
lépe	dobře	k6eAd2	dobře
trávit	trávit	k5eAaImF	trávit
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
endoparazitů	endoparazit	k1gMnPc2	endoparazit
však	však	k9	však
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
svých	svůj	k3xOyFgMnPc2	svůj
hostitelů	hostitel	k1gMnPc2	hostitel
škodí	škodit	k5eAaImIp3nS	škodit
-	-	kIx~	-
připravují	připravovat	k5eAaImIp3nP	připravovat
ho	on	k3xPp3gNnSc4	on
o	o	k7c4	o
výživné	výživné	k1gNnSc4	výživné
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
jeho	jeho	k3xOp3gFnPc4	jeho
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
i	i	k8xC	i
ničí	ničit	k5eAaImIp3nS	ničit
<g/>
,	,	kIx,	,
vylučují	vylučovat	k5eAaImIp3nP	vylučovat
jedovaté	jedovatý	k2eAgFnPc1d1	jedovatá
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
toxiny	toxin	k1gInPc1	toxin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patogenní	patogenní	k2eAgMnPc1d1	patogenní
prvoci	prvok	k1gMnPc1	prvok
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
mnohé	mnohý	k2eAgFnPc4d1	mnohá
významné	významný	k2eAgFnPc4d1	významná
nemoci	nemoc	k1gFnPc4	nemoc
člověka	člověk	k1gMnSc2	člověk
i	i	k8xC	i
jiných	jiný	k2eAgMnPc2d1	jiný
organismů	organismus	k1gInPc2	organismus
<g/>
:	:	kIx,	:
malárie	malárie	k1gFnSc2	malárie
spavá	spavý	k2eAgFnSc1d1	spavá
nemoc	nemoc	k1gFnSc1	nemoc
Chagasova	Chagasův	k2eAgFnSc1d1	Chagasova
nemoc	nemoc	k1gFnSc1	nemoc
(	(	kIx(	(
<g/>
americká	americký	k2eAgFnSc1d1	americká
trypanozomiáda	trypanozomiáda	k1gFnSc1	trypanozomiáda
<g/>
)	)	kIx)	)
leishmanióza	leishmanióza	k1gFnSc1	leishmanióza
toxoplasmóza	toxoplasmóza	k1gFnSc1	toxoplasmóza
giardióza	giardióza	k1gFnSc1	giardióza
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
lamblióza	lamblióza	k1gFnSc1	lamblióza
balantidióza	balantidióza	k1gFnSc1	balantidióza
měňavkovitá	měňavkovitý	k2eAgFnSc1d1	měňavkovitá
úplavice	úplavice	k1gFnSc1	úplavice
</s>
