<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
horizontální	horizontální	k2eAgInPc4d1	horizontální
pruhy	pruh	k1gInPc4	pruh
–	–	k?	–
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc4d1	modrý
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
se	s	k7c7	s
slovinským	slovinský	k2eAgInSc7d1	slovinský
znakem	znak	k1gInSc7	znak
(	(	kIx(	(
<g/>
štít	štít	k1gInSc1	štít
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Triglavu	Triglav	k1gInSc2	Triglav
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
vrchu	vrch	k1gInSc2	vrch
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
modrým	modrý	k2eAgNnSc7d1	modré
pozadím	pozadí	k1gNnSc7	pozadí
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
vlnité	vlnitý	k2eAgFnPc1d1	vlnitá
modré	modrý	k2eAgFnPc1d1	modrá
čáry	čára	k1gFnPc1	čára
znázorňující	znázorňující	k2eAgInSc1d1	znázorňující
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc1	tři
šesticípé	šesticípý	k2eAgFnPc1d1	šesticípá
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
převráceného	převrácený	k2eAgInSc2d1	převrácený
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
převzaté	převzatý	k2eAgInPc1d1	převzatý
z	z	k7c2	z
brnění	brnění	k1gNnSc2	brnění
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
rodu	rod	k1gInSc2	rod
Celjských	Celjský	k2eAgInPc2d1	Celjský
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátku	začátek	k1gInSc2	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
překrývá	překrývat	k5eAaImIp3nS	překrývat
bílý	bílý	k2eAgInSc1d1	bílý
a	a	k8xC	a
modrý	modrý	k2eAgInSc1d1	modrý
horizontální	horizontální	k2eAgInSc1d1	horizontální
pruh	pruh	k1gInSc1	pruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
přijala	přijmout	k5eAaPmAgFnS	přijmout
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
republiky	republika	k1gFnPc1	republika
národní	národní	k2eAgFnSc4d1	národní
trikolóru	trikolóra	k1gFnSc4	trikolóra
doplněnou	doplněný	k2eAgFnSc4d1	doplněná
"	"	kIx"	"
<g/>
jugoslávskou	jugoslávský	k2eAgFnSc7d1	jugoslávská
<g/>
"	"	kIx"	"
hvězdou	hvězda	k1gFnSc7	hvězda
z	z	k7c2	z
federální	federální	k2eAgFnSc2d1	federální
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
hvězda	hvězda	k1gFnSc1	hvězda
odstraněná	odstraněný	k2eAgFnSc1d1	odstraněná
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
doplněná	doplněná	k1gFnSc1	doplněná
znakem	znak	k1gInSc7	znak
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zavedená	zavedený	k2eAgFnSc1d1	zavedená
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Vlajka	vlajka	k1gFnSc1	vlajka
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
</s>
</p>
<p>
<s>
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slovinská	slovinský	k2eAgFnSc1d1	slovinská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
