<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
horizontální	horizontální	k2eAgInPc4d1	horizontální
pruhy	pruh	k1gInPc4	pruh
–	–	k?	–
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
nahoře	nahoře	k6eAd1	nahoře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc4d1	modrý
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
se	s	k7c7	s
slovinským	slovinský	k2eAgInSc7d1	slovinský
znakem	znak	k1gInSc7	znak
(	(	kIx(	(
<g/>
štít	štít	k1gInSc1	štít
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
Triglavu	Triglav	k1gInSc2	Triglav
<g/>
,	,	kIx,	,
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
vrchu	vrch	k1gInSc2	vrch
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
barvě	barva	k1gFnSc6	barva
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
modrým	modrý	k2eAgNnSc7d1	modré
pozadím	pozadí	k1gNnSc7	pozadí
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
