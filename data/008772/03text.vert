<p>
<s>
Cimbál	cimbál	k1gInSc1	cimbál
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
cembalo	cembalo	k1gNnSc1	cembalo
ungarico	ungarico	k1gNnSc1	ungarico
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
deskový	deskový	k2eAgInSc1d1	deskový
úderný	úderný	k2eAgInSc1d1	úderný
strunný	strunný	k2eAgInSc1d1	strunný
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
pedálem	pedál	k1gInSc7	pedál
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
velkého	velký	k2eAgInSc2d1	velký
maďarského	maďarský	k2eAgInSc2d1	maďarský
cimbálu	cimbál	k1gInSc2	cimbál
naladěné	naladěný	k2eAgNnSc1d1	naladěné
chromaticky	chromaticky	k6eAd1	chromaticky
v	v	k7c6	v
tónovém	tónový	k2eAgInSc6d1	tónový
rozsahu	rozsah	k1gInSc6	rozsah
C	C	kA	C
<g/>
–	–	k?	–
<g/>
a3	a3	k4	a3
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
si	se	k3xPyFc3	se
nejhlubší	hluboký	k2eAgFnPc1d3	nejhlubší
struny	struna	k1gFnPc1	struna
podlaďují	podlaďovat	k5eAaImIp3nP	podlaďovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
většího	veliký	k2eAgNnSc2d2	veliký
rozsahu	rozsah	k1gInSc3	rozsah
nástroje	nástroj	k1gInPc4	nástroj
i	i	k8xC	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
neúplné	úplný	k2eNgFnSc2d1	neúplná
chromatiky	chromatika	k1gFnSc2	chromatika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
starší	starý	k2eAgInPc1d2	starší
lidové	lidový	k2eAgInPc1d1	lidový
cimbály	cimbál	k1gInPc1	cimbál
byly	být	k5eAaImAgInP	být
diatonické	diatonický	k2eAgInPc1d1	diatonický
a	a	k8xC	a
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jistý	jistý	k2eAgInSc4d1	jistý
druh	druh	k1gInSc4	druh
stolní	stolní	k2eAgFnSc2d1	stolní
citery	citera	k1gFnSc2	citera
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
německého	německý	k2eAgInSc2d1	německý
hackbrettu	hackbrett	k1gInSc2	hackbrett
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
cimbalista	cimbalista	k1gMnSc1	cimbalista
<g/>
/	/	kIx~	/
<g/>
cimbalistka	cimbalistka	k1gFnSc1	cimbalistka
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc4	struna
rozeznívají	rozeznívat	k5eAaImIp3nP	rozeznívat
cimbalisté	cimbalista	k1gMnPc1	cimbalista
převážně	převážně	k6eAd1	převážně
úderem	úder	k1gInSc7	úder
paličky	palička	k1gFnSc2	palička
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dvě	dva	k4xCgFnPc1	dva
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
paličky	palička	k1gFnPc1	palička
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Ocelové	ocelový	k2eAgFnPc1d1	ocelová
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
nataženy	natáhnout	k5eAaPmNgFnP	natáhnout
na	na	k7c6	na
rezonanční	rezonanční	k2eAgFnSc6d1	rezonanční
skříni	skříň	k1gFnSc6	skříň
lichoběžníkového	lichoběžníkový	k2eAgInSc2d1	lichoběžníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
jednotlivý	jednotlivý	k2eAgInSc4d1	jednotlivý
tón	tón	k1gInSc4	tón
je	být	k5eAaImIp3nS	být
nataženo	natáhnout	k5eAaPmNgNnS	natáhnout
1	[number]	k4	1
až	až	k9	až
5	[number]	k4	5
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
vedeny	veden	k2eAgFnPc1d1	vedena
střídavě	střídavě	k6eAd1	střídavě
přes	přes	k7c4	přes
kobylky	kobylka	k1gFnPc4	kobylka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přesnější	přesný	k2eAgInSc4d2	přesnější
úder	úder	k1gInSc4	úder
paličkou	palička	k1gFnSc7	palička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
cimbálu	cimbál	k1gInSc2	cimbál
==	==	k?	==
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
paličkových	paličkův	k2eAgFnPc2d1	paličkův
citer	citera	k1gFnPc2	citera
byl	být	k5eAaImAgInS	být
perský	perský	k2eAgInSc1d1	perský
(	(	kIx(	(
<g/>
Írán	Írán	k1gInSc1	Írán
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
santůr	santůr	k1gInSc1	santůr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
lichoběžníkový	lichoběžníkový	k2eAgInSc4d1	lichoběžníkový
tvar	tvar	k1gInSc4	tvar
s	s	k7c7	s
horizontální	horizontální	k2eAgFnSc7d1	horizontální
polohou	poloha	k1gFnSc7	poloha
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nástroj	nástroj	k1gInSc1	nástroj
je	být	k5eAaImIp3nS	být
stár	stár	k2eAgInSc1d1	stár
asi	asi	k9	asi
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
prototyp	prototyp	k1gInSc1	prototyp
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
už	už	k6eAd1	už
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
Babylonské	babylonský	k2eAgFnSc6d1	Babylonská
říši	říš	k1gFnSc6	říš
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Irák	Irák	k1gInSc1	Irák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paličkové	Paličkové	k2eAgFnPc1d1	Paličkové
citery	citera	k1gFnPc1	citera
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Blízký	blízký	k2eAgInSc1d1	blízký
východ	východ	k1gInSc1	východ
<g/>
,	,	kIx,	,
jihovýchodní	jihovýchodní	k2eAgFnSc1d1	jihovýchodní
Asie	Asie	k1gFnSc1	Asie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Balkán	Balkán	k1gInSc1	Balkán
<g/>
,	,	kIx,	,
Východní	východní	k2eAgFnSc1d1	východní
Evropa	Evropa	k1gFnSc1	Evropa
(	(	kIx(	(
<g/>
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nástroje	nástroj	k1gInPc4	nástroj
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
dulcimer	dulcimer	k1gMnSc1	dulcimer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
paličkové	paličkový	k2eAgFnSc2d1	paličkový
citery	citera	k1gFnSc2	citera
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dnes	dnes	k6eAd1	dnes
známého	známý	k2eAgInSc2d1	známý
cimbálu	cimbál	k1gInSc2	cimbál
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
cimbál	cimbál	k1gInSc1	cimbál
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
použití	použití	k1gNnSc1	použití
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
cimbál	cimbál	k1gInSc1	cimbál
především	především	k9	především
nástrojem	nástroj	k1gInSc7	nástroj
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
hudebních	hudební	k2eAgInPc6d1	hudební
souborech	soubor	k1gInPc6	soubor
společně	společně	k6eAd1	společně
s	s	k7c7	s
houslemi	housle	k1gFnPc7	housle
a	a	k8xC	a
kontrabasem	kontrabas	k1gInSc7	kontrabas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
cimbál	cimbál	k1gInSc1	cimbál
společně	společně	k6eAd1	společně
s	s	k7c7	s
dechovými	dechový	k2eAgInPc7d1	dechový
nástroji	nástroj	k1gInPc7	nástroj
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
s	s	k7c7	s
klarinetem	klarinet	k1gInSc7	klarinet
<g/>
)	)	kIx)	)
a	a	k8xC	a
bubnem	buben	k1gInSc7	buben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cimbál	cimbál	k1gInSc1	cimbál
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
často	často	k6eAd1	často
hráli	hrát	k5eAaImAgMnP	hrát
Romové	Rom	k1gMnPc1	Rom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
koncertního	koncertní	k2eAgInSc2d1	koncertní
cimbálu	cimbál	k1gInSc2	cimbál
==	==	k?	==
</s>
</p>
<p>
<s>
Cimbál	cimbál	k1gInSc1	cimbál
byl	být	k5eAaImAgInS	být
zdokonalen	zdokonalit	k5eAaPmNgInS	zdokonalit
do	do	k7c2	do
koncertní	koncertní	k2eAgFnSc2d1	koncertní
podoby	podoba	k1gFnSc2	podoba
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
tvůrcem	tvůrce	k1gMnSc7	tvůrce
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
József	József	k1gMnSc1	József
Schunda	Schunda	k1gFnSc1	Schunda
(	(	kIx(	(
<g/>
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Dubče	Dubč	k1gFnSc2	Dubč
u	u	k7c2	u
Říčan	Říčany	k1gInPc2	Říčany
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1874	[number]	k4	1874
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
pevný	pevný	k2eAgInSc1d1	pevný
pedalizační	pedalizační	k2eAgInSc1d1	pedalizační
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
revolučním	revoluční	k2eAgInSc7d1	revoluční
činem	čin	k1gInSc7	čin
započala	započnout	k5eAaPmAgFnS	započnout
éra	éra	k1gFnSc1	éra
využívání	využívání	k1gNnSc2	využívání
cimbálu	cimbál	k1gInSc2	cimbál
jakožto	jakožto	k8xS	jakožto
koncertního	koncertní	k2eAgInSc2d1	koncertní
hudebního	hudební	k2eAgInSc2d1	hudební
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokračovatelem	pokračovatel	k1gMnSc7	pokračovatel
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
také	také	k9	také
konkurentem	konkurent	k1gMnSc7	konkurent
firmy	firma	k1gFnSc2	firma
Schunda	Schunda	k1gFnSc1	Schunda
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
stala	stát	k5eAaPmAgFnS	stát
firma	firma	k1gFnSc1	firma
Bohák	Bohák	k1gInSc1	Bohák
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
zakladatel	zakladatel	k1gMnSc1	zakladatel
Lajos	Lajos	k1gMnSc1	Lajos
Bohák	Bohák	k1gMnSc1	Bohák
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1870	[number]	k4	1870
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Kvetná	Kvetný	k2eAgFnSc1d1	Kvetný
(	(	kIx(	(
<g/>
teď	teď	k6eAd1	teď
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
velkým	velký	k2eAgMnSc7d1	velký
konkurentem	konkurent	k1gMnSc7	konkurent
V.J.	V.J.	k1gFnSc2	V.J.
<g/>
Schundy	Schunda	k1gFnSc2	Schunda
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
uznal	uznat	k5eAaPmAgMnS	uznat
jeho	on	k3xPp3gInSc4	on
nový	nový	k2eAgInSc4d1	nový
pedalizační	pedalizační	k2eAgInSc4d1	pedalizační
systém	systém	k1gInSc4	systém
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
ho	on	k3xPp3gMnSc4	on
též	též	k9	též
používat	používat	k5eAaImF	používat
u	u	k7c2	u
svých	svůj	k3xOyFgInPc2	svůj
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
L.	L.	kA	L.
Boháka	Bohák	k1gMnSc4	Bohák
přebral	přebrat	k5eAaPmAgMnS	přebrat
firmu	firma	k1gFnSc4	firma
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Lajos	Lajos	k1gMnSc1	Lajos
Bohák	Bohák	k1gMnSc1	Bohák
ml.	ml.	kA	ml.
</s>
</p>
<p>
<s>
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
výroba	výroba	k1gFnSc1	výroba
velkých	velký	k2eAgInPc2d1	velký
cimbálů	cimbál	k1gInPc2	cimbál
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
firmou	firma	k1gFnSc7	firma
Josef	Josef	k1gMnSc1	Josef
Lídl	Lídl	k1gMnSc1	Lídl
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
začala	začít	k5eAaPmAgFnS	začít
výroba	výroba	k1gFnSc1	výroba
velkých	velký	k2eAgInPc2d1	velký
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
konstrukčně	konstrukčně	k6eAd1	konstrukčně
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
osvědčenými	osvědčený	k2eAgInPc7d1	osvědčený
instrumenty	instrument	k1gInPc7	instrument
značek	značka	k1gFnPc2	značka
Schunda	Schunda	k1gFnSc1	Schunda
a	a	k8xC	a
Bohák	Bohák	k1gInSc1	Bohák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
především	především	k9	především
František	František	k1gMnSc1	František
Wallinger	Wallinger	k1gMnSc1	Wallinger
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Stehlík	Stehlík	k1gMnSc1	Stehlík
a	a	k8xC	a
poté	poté	k6eAd1	poté
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Rybníček	Rybníček	k1gMnSc1	Rybníček
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vedoucím	vedoucí	k1gMnSc7	vedoucí
díla	dílo	k1gNnSc2	dílo
M.	M.	kA	M.
Kotoulem	kotoul	k1gInSc7	kotoul
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
postavili	postavit	k5eAaPmAgMnP	postavit
okolo	okolo	k7c2	okolo
100	[number]	k4	100
nástrojů	nástroj	k1gInPc2	nástroj
s	s	k7c7	s
firemní	firemní	k2eAgFnSc7d1	firemní
značkou	značka	k1gFnSc7	značka
PRIMAS	primas	k1gMnSc1	primas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
cimbálů	cimbál	k1gInPc2	cimbál
zastavena	zastavit	k5eAaPmNgFnS	zastavit
"	"	kIx"	"
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
plánovaného	plánovaný	k2eAgNnSc2d1	plánované
řízení	řízení	k1gNnSc2	řízení
hospodářství	hospodářství	k1gNnSc2	hospodářství
jako	jako	k8xS	jako
bezperspektivní	bezperspektivní	k2eAgFnSc2d1	bezperspektivní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
nástroje	nástroj	k1gInPc1	nástroj
však	však	k9	však
nedosahovaly	dosahovat	k5eNaImAgFnP	dosahovat
koncertní	koncertní	k2eAgFnPc1d1	koncertní
kvality	kvalita	k1gFnPc1	kvalita
maďarských	maďarský	k2eAgInPc2d1	maďarský
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
konstrukčním	konstrukční	k2eAgFnPc3d1	konstrukční
změnám	změna	k1gFnPc3	změna
ozvučné	ozvučný	k2eAgFnSc2d1	ozvučná
skříně	skříň	k1gFnSc2	skříň
provedenou	provedený	k2eAgFnSc4d1	provedená
nástrojaři	nástrojař	k1gMnPc7	nástrojař
firmy	firma	k1gFnSc2	firma
Lídl	Lídl	k1gInSc1	Lídl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1956-1992	[number]	k4	1956-1992
se	se	k3xPyFc4	se
velké	velký	k2eAgInPc1d1	velký
cimbály	cimbál	k1gInPc1	cimbál
dovážely	dovážet	k5eAaImAgInP	dovážet
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
výroby	výroba	k1gFnSc2	výroba
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k6eAd1	až
na	na	k7c6	na
počátku	počátek	k1gInSc2	počátek
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
firmách	firma	k1gFnPc6	firma
Josef	Josef	k1gMnSc1	Josef
Lídl	Lídl	k1gMnSc1	Lídl
<g/>
,	,	kIx,	,
Galuška	Galuška	k1gMnSc1	Galuška
a	a	k8xC	a
Holak	Holak	k1gMnSc1	Holak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cimbál	cimbál	k1gInSc4	cimbál
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Cimbál	cimbál	k1gInSc1	cimbál
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
i	i	k9	i
ve	v	k7c6	v
vážné	vážný	k2eAgFnSc6d1	vážná
hudbě	hudba	k1gFnSc6	hudba
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
Ferenci	Ferenc	k1gMnSc3	Ferenc
Lisztovi	Liszt	k1gMnSc3	Liszt
a	a	k8xC	a
dalším	další	k2eAgMnPc3d1	další
maďarským	maďarský	k2eAgMnPc3d1	maďarský
skladatelům	skladatel	k1gMnPc3	skladatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hráč	hráč	k1gMnSc1	hráč
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
==	==	k?	==
</s>
</p>
<p>
<s>
Hráč	hráč	k1gMnSc1	hráč
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
cimbalista	cimbalista	k1gMnSc1	cimbalista
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
o	o	k7c4	o
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
cimbalistka	cimbalistka	k1gFnSc1	cimbalistka
<g/>
.	.	kIx.	.
</s>
<s>
Cimbalista	cimbalista	k1gMnSc1	cimbalista
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
buď	buď	k8xC	buď
úderem	úder	k1gInSc7	úder
paliček	palička	k1gFnPc2	palička
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pizzicatem	pizzicato	k1gNnSc7	pizzicato
<g/>
.	.	kIx.	.
</s>
<s>
Cimbalisté	cimbalista	k1gMnPc1	cimbalista
a	a	k8xC	a
cimbalistky	cimbalistka	k1gFnPc1	cimbalistka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
známí	známý	k2eAgMnPc1d1	známý
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
cimbálových	cimbálový	k2eAgFnPc2d1	cimbálová
muzik	muzika	k1gFnPc2	muzika
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
mohou	moct	k5eAaImIp3nP	moct
studovat	studovat	k5eAaImF	studovat
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
konzervatořích	konzervatoř	k1gFnPc6	konzervatoř
(	(	kIx(	(
<g/>
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
i	i	k8xC	i
na	na	k7c6	na
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
Ference	Ferenc	k1gMnSc2	Ferenc
Liszta	Liszt	k1gMnSc2	Liszt
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vystudovaní	vystudovaný	k2eAgMnPc1d1	vystudovaný
hráči	hráč	k1gMnPc1	hráč
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
mnohdy	mnohdy	k6eAd1	mnohdy
věnují	věnovat	k5eAaPmIp3nP	věnovat
sólové	sólový	k2eAgFnSc3d1	sólová
hře	hra	k1gFnSc3	hra
na	na	k7c4	na
cimbál	cimbál	k1gInSc4	cimbál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgMnPc4d1	známý
české	český	k2eAgMnPc4d1	český
cimbálové	cimbálový	k2eAgMnPc4d1	cimbálový
interprety	interpret	k1gMnPc4	interpret
patří	patřit	k5eAaImIp3nS	patřit
Magdalena	Magdalena	k1gFnSc1	Magdalena
Múčková	Múčková	k1gFnSc1	Múčková
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rokyta	rokyta	k1gFnSc1	rokyta
<g/>
,	,	kIx,	,
Dalibor	Dalibor	k1gMnSc1	Dalibor
Štrunc	Štrunc	k1gMnSc1	Štrunc
<g/>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
Červenková	Červenková	k1gFnSc1	Červenková
<g/>
,	,	kIx,	,
Růžena	Růžena	k1gFnSc1	Růžena
Děcká	Děcká	k1gFnSc1	Děcká
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Lapčíková	Lapčíková	k1gFnSc1	Lapčíková
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
Dadák	Dadák	k1gMnSc1	Dadák
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
Zlatníková	zlatníkový	k2eAgFnSc1d1	Zlatníková
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Dadáková	Dadáková	k1gFnSc1	Dadáková
<g/>
,	,	kIx,	,
Daniel	Daniel	k1gMnSc1	Daniel
Skála	Skála	k1gMnSc1	Skála
<g/>
,	,	kIx,	,
z	z	k7c2	z
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
generace	generace	k1gFnSc2	generace
Michal	Michal	k1gMnSc1	Michal
Grombiřík	Grombiřík	k1gMnSc1	Grombiřík
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Spěvák	Spěvák	k1gMnSc1	Spěvák
<g/>
,	,	kIx,	,
Nikol	nikol	k1gInSc1	nikol
Kuchynková	Kuchynková	k1gFnSc1	Kuchynková
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
Bláhová	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Gilar	Gilar	k1gMnSc1	Gilar
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Gužík	Gužík	k1gMnSc1	Gužík
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Gablas	Gablas	k1gMnSc1	Gablas
<g/>
,	,	kIx,	,
Gabriela	Gabriela	k1gFnSc1	Gabriela
Jílková	Jílková	k1gFnSc1	Jílková
<g/>
,	,	kIx,	,
<g/>
Matěj	Matěj	k1gMnSc1	Matěj
Číp	Číp	k1gMnSc1	Číp
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kneisl	Kneisl	k1gInSc1	Kneisl
<g/>
,	,	kIx,	,
Vladimíra	Vladimíra	k1gFnSc1	Vladimíra
Křenková	Křenková	k1gFnSc1	Křenková
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
Schejbalová	Schejbalová	k1gFnSc1	Schejbalová
<g/>
,	,	kIx,	,
Jura	Jura	k1gMnSc1	Jura
Wajda	Wajda	k1gMnSc1	Wajda
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
Ramdan	Ramdan	k1gMnSc1	Ramdan
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hra	hra	k1gFnSc1	hra
paličkami	palička	k1gFnPc7	palička
===	===	k?	===
</s>
</p>
<p>
<s>
Paličky	palička	k1gFnPc1	palička
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Můžou	Můžou	k?	Můžou
mít	mít	k5eAaImF	mít
úderné	úderný	k2eAgFnPc4d1	úderná
hlavice	hlavice	k1gFnPc4	hlavice
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
nebo	nebo	k8xC	nebo
měkké	měkký	k2eAgFnPc4d1	měkká
<g/>
,	,	kIx,	,
rukojeti	rukojeť	k1gFnPc4	rukojeť
pevné	pevný	k2eAgFnPc4d1	pevná
nebo	nebo	k8xC	nebo
pružné	pružný	k2eAgNnSc1d1	pružné
<g/>
,	,	kIx,	,
s	s	k7c7	s
úchopovou	úchopový	k2eAgFnSc7d1	úchopová
částí	část	k1gFnSc7	část
pro	pro	k7c4	pro
prsty	prst	k1gInPc4	prst
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Cimbáloví	cimbálový	k2eAgMnPc1d1	cimbálový
hráči	hráč	k1gMnPc1	hráč
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
používají	používat	k5eAaImIp3nP	používat
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
pružné	pružný	k2eAgFnPc4d1	pružná
paličky	palička	k1gFnPc4	palička
s	s	k7c7	s
ohnutými	ohnutý	k2eAgFnPc7d1	ohnutá
hlavicemi	hlavice	k1gFnPc7	hlavice
obalené	obalený	k2eAgFnPc1d1	obalená
vatou	vata	k1gFnSc7	vata
či	či	k8xC	či
plstí	plst	k1gFnSc7	plst
a	a	k8xC	a
s	s	k7c7	s
úchopnou	úchopný	k2eAgFnSc7d1	úchopný
částí	část	k1gFnSc7	část
pro	pro	k7c4	pro
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
kratší	krátký	k2eAgFnPc1d2	kratší
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
paličky	palička	k1gFnPc1	palička
bez	bez	k7c2	bez
obalení	obalení	k1gNnSc2	obalení
vatou	vata	k1gFnSc7	vata
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
též	též	k9	též
kovové	kovový	k2eAgFnPc4d1	kovová
paličky	palička	k1gFnPc4	palička
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
cinkavější	cinkavý	k2eAgInSc4d2	cinkavý
zvuk	zvuk	k1gInSc4	zvuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hra	hra	k1gFnSc1	hra
pizzicato	pizzicato	k6eAd1	pizzicato
===	===	k?	===
</s>
</p>
<p>
<s>
Pizzicato	pizzicato	k6eAd1	pizzicato
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
cimbálové	cimbálový	k2eAgFnSc2d1	cimbálová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cimbalista	cimbalista	k1gMnSc1	cimbalista
rozeznívá	rozeznívat	k5eAaImIp3nS	rozeznívat
strunu	struna	k1gFnSc4	struna
pomocí	pomocí	k7c2	pomocí
drnkání	drnkání	k1gNnSc2	drnkání
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většina	k1gFnSc7	většina
vlastními	vlastní	k2eAgInPc7d1	vlastní
nehty	nehet	k1gInPc7	nehet
či	či	k8xC	či
trsátkem	trsátko	k1gNnSc7	trsátko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Některá	některý	k3yIgNnPc1	některý
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
-	-	kIx~	-
Ungarischer	Ungarischra	k1gFnPc2	Ungarischra
Sturmmarsch	Sturmmarsch	k1gInSc1	Sturmmarsch
<g/>
,	,	kIx,	,
1876	[number]	k4	1876
</s>
</p>
<p>
<s>
Ferenc	Ferenc	k1gMnSc1	Ferenc
Liszt	Liszt	k1gMnSc1	Liszt
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Uherské	uherský	k2eAgFnPc1d1	uherská
rapsodie	rapsodie	k1gFnPc1	rapsodie
</s>
</p>
<p>
<s>
Béla	Béla	k1gMnSc1	Béla
Bartók	Bartók	k1gMnSc1	Bartók
-	-	kIx~	-
Rapsodie	rapsodie	k1gFnSc1	rapsodie
č.	č.	k?	č.
1	[number]	k4	1
pro	pro	k7c4	pro
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
orchestr	orchestr	k1gInSc4	orchestr
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Cembalo	cembalo	k1gNnSc1	cembalo
</s>
</p>
<p>
<s>
dulcimer	dulcimer	k1gMnSc1	dulcimer
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cimbál	cimbál	k1gInSc1	cimbál
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnPc1	ukázka
MP3	MP3	k1gMnSc2	MP3
</s>
</p>
