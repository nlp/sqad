<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
</s>
<s>
Ruskoр	Ruskoр	k?
р	р	k?
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
MinceZemě	MinceZema	k1gFnSc3
</s>
<s>
Rusko	Rusko	k1gNnSc1
RuskoAbcházie	RuskoAbcházie	k1gFnSc2
AbcházieJižní	AbcházieJižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
RUB	rub	k1gInSc1
Inflace	inflace	k1gFnSc2
</s>
<s>
4,2	4,2	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2017	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
kopějka	kopějka	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
kopějek	kopějka	k1gFnPc2
<g/>
;	;	kIx,
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
,	,	kIx,
10	#num#	k4
rublů	rubl	k1gInPc2
Bankovky	bankovka	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
,	,	kIx,
5000	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
р	р	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zákonné	zákonný	k2eAgNnSc4d1
platidlo	platidlo	k1gNnSc1
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInPc1
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
RUB	RUB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
roce	rok	k1gInSc6
provedlo	provést	k5eAaPmAgNnS
Rusko	Rusko	k1gNnSc1
měnovou	měnový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
1000	#num#	k4
starých	starý	k2eAgInPc2d1
rublů	rubl	k1gInPc2
RUR	RUR	kA
stal	stát	k5eAaPmAgInS
1	#num#	k4
nový	nový	k2eAgInSc4d1
rubl	rubl	k1gInSc4
RUB	rub	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
rublu	rubl	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
kopějka	kopějka	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
к	к	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
rubl	rubl	k1gInSc4
nesly	nést	k5eAaImAgInP
i	i	k9
měny	měna	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
používaly	používat	k5eAaImAgFnP
předchůdci	předchůdce	k1gMnPc7
současné	současný	k2eAgFnSc2d1
Ruské	ruský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
–	–	k?
carské	carský	k2eAgNnSc1d1
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Symbolem	symbol	k1gInSc7
rublu	rubl	k1gInSc2
je	být	k5eAaImIp3nS
od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
písmeno	písmeno	k1gNnSc4
Р	Р	k?
s	s	k7c7
přetaženým	přetažený	k2eAgNnSc7d1
spodním	spodní	k2eAgNnSc7d1
ramenem	rameno	k1gNnSc7
doplněným	doplněný	k2eAgNnSc7d1
druhou	druhý	k4xOgFnSc7
rovnoběžnou	rovnoběžný	k2eAgFnSc7d1
úsečkou	úsečka	k1gFnSc7
o	o	k7c4
něco	něco	k3yInSc4
níže	nízce	k6eAd2
<g/>
,	,	kIx,
tedy	tedy	k9
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
standardu	standard	k1gInSc6
Unicode	Unicod	k1gInSc5
verze	verze	k1gFnSc1
7.0	7.0	k4
<g/>
.0	.0	k4
<g/>
,	,	kIx,
vydaném	vydaný	k2eAgInSc6d1
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
je	být	k5eAaImIp3nS
už	už	k6eAd1
znak	znak	k1gInSc1
zařazen	zařadit	k5eAaPmNgInS
pod	pod	k7c7
kódem	kód	k1gInSc7
U	u	k7c2
<g/>
+	+	kIx~
<g/>
20	#num#	k4
<g/>
BD	BD	kA
RUBLE	rubl	k1gInSc2
SIGN	signum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Současné	současný	k2eAgFnPc1d1
mince	mince	k1gFnPc1
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnPc4
mince	mince	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
kopějek	kopějka	k1gFnPc2
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
a	a	k8xC
10	#num#	k4
rublů	rubl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
jsou	být	k5eAaImIp3nP
tisknuty	tisknout	k5eAaImNgFnP
v	v	k7c6
nominálních	nominální	k2eAgFnPc6d1
hodnotách	hodnota	k1gFnPc6
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
<g/>
,	,	kIx,
500	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
a	a	k8xC
5000	#num#	k4
rublů	rubl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnSc2
5	#num#	k4
a	a	k8xC
10	#num#	k4
rublů	rubl	k1gInPc2
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
postupně	postupně	k6eAd1
stahovány	stahovat	k5eAaImNgInP
z	z	k7c2
oběhu	oběh	k1gInSc2
a	a	k8xC
nahrazovány	nahrazovat	k5eAaImNgInP
mincemi	mince	k1gFnPc7
o	o	k7c4
stejné	stejná	k1gFnPc4
nominální	nominální	k2eAgFnSc3d1
hodnotě	hodnota	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
</s>
<s>
VyobrazeníHodnotaRozměryBarvaPopis	VyobrazeníHodnotaRozměryBarvaPopis	k1gInSc1
</s>
<s>
LícRubMěsto	LícRubMěsto	k6eAd1
<g/>
/	/	kIx~
<g/>
regionLícRub	regionLícRub	k1gInSc1
</s>
<s>
5	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
137	#num#	k4
×	×	k?
61	#num#	k4
mm	mm	kA
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Novgorod	Novgorod	k1gInSc1
</s>
<s>
Pomník	pomník	k1gInSc1
Tisíciletí	tisíciletí	k1gNnSc2
Ruska	Rusko	k1gNnSc2
a	a	k8xC
chrám	chrám	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Žofie	Žofie	k1gFnSc2
</s>
<s>
Hradba	hradba	k1gFnSc1
Novgorodského	novgorodský	k2eAgInSc2d1
dětince	dětinec	k1gInSc2
</s>
<s>
10	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
150	#num#	k4
×	×	k?
65	#num#	k4
mm	mm	kA
</s>
<s>
Zelenohnědá	zelenohnědý	k2eAgFnSc1d1
</s>
<s>
Krasnojarsk	Krasnojarsk	k1gInSc1
</s>
<s>
Most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Jenisej	Jenisej	k1gInSc1
</s>
<s>
Krasnojarská	Krasnojarský	k2eAgFnSc1d1
přehrada	přehrada	k1gFnSc1
</s>
<s>
50	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Světlomodrá	světlomodrý	k2eAgFnSc1d1
</s>
<s>
Petrohrad	Petrohrad	k1gInSc1
</s>
<s>
Petropavlovská	petropavlovský	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
</s>
<s>
Budova	budova	k1gFnSc1
bývalé	bývalý	k2eAgFnSc2d1
burzy	burza	k1gFnSc2
a	a	k8xC
Rostrální	Rostrální	k2eAgInSc1d1
sloup	sloup	k1gInSc1
</s>
<s>
100	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Hnědozelená	hnědozelený	k2eAgFnSc1d1
</s>
<s>
Moskva	Moskva	k1gFnSc1
</s>
<s>
Sousoší	sousoší	k1gNnSc1
na	na	k7c6
průčelí	průčelí	k1gNnSc6
divadla	divadlo	k1gNnSc2
Bolšoj	Bolšoj	k1gInSc1
těatr	těatr	k1gMnSc1
</s>
<s>
Bolšoj	Bolšoj	k1gInSc1
těatr	těatr	k1gInSc1
</s>
<s>
200	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1
</s>
<s>
Sevastopol	Sevastopol	k1gInSc1
</s>
<s>
Chersonésos	Chersonésos	k1gMnSc1
</s>
<s>
Pomník	pomník	k1gInSc1
potopeným	potopený	k2eAgInPc3d1
lodím	loď	k1gFnPc3
</s>
<s>
500	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Fialovočervená	fialovočervený	k2eAgFnSc1d1
</s>
<s>
Archangelsk	Archangelsk	k1gInSc1
</s>
<s>
Pomník	pomník	k1gInSc1
Petra	Petr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
,	,	kIx,
přístav	přístav	k1gInSc1
</s>
<s>
Solovecký	Solovecký	k2eAgInSc1d1
klášter	klášter	k1gInSc1
na	na	k7c6
Soloveckých	Solovecký	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
</s>
<s>
1000	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
157	#num#	k4
×	×	k?
69	#num#	k4
mm	mm	kA
</s>
<s>
Modrozelená	modrozelený	k2eAgFnSc1d1
</s>
<s>
Jaroslavl	Jaroslavnout	k5eAaPmAgMnS
</s>
<s>
Pomník	pomník	k1gInSc1
Jaroslava	Jaroslav	k1gMnSc2
I.	I.	kA
Moudrého	moudrý	k2eAgInSc2d1
</s>
<s>
Chrám	chrám	k1gInSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
2000	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
</s>
<s>
Dálný	dálný	k2eAgInSc1d1
východ	východ	k1gInSc1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
most	most	k1gInSc1
</s>
<s>
Kosmodrom	kosmodrom	k1gInSc1
Vostočnyj	Vostočnyj	k1gInSc1
</s>
<s>
5000	#num#	k4
rublů	rubl	k1gInPc2
</s>
<s>
Červenooranžová	Červenooranžový	k2eAgFnSc1d1
</s>
<s>
Chabarovsk	Chabarovsk	k1gInSc1
</s>
<s>
Pomník	pomník	k1gInSc1
Nikolaje	Nikolaj	k1gMnSc2
Muravjova-Amurského	Muravjova-Amurský	k2eAgMnSc2d1
</s>
<s>
Chabarovský	chabarovský	k2eAgInSc1d1
most	most	k1gInSc1
přes	přes	k7c4
řeku	řeka	k1gFnSc4
Amur	Amur	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Dolar	dolar	k1gInSc1
<g/>
,	,	kIx,
euro	euro	k1gNnSc1
i	i	k8xC
libra	libra	k1gFnSc1
mají	mít	k5eAaImIp3nP
své	svůj	k3xOyFgInPc4
symboly	symbol	k1gInPc4
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
se	se	k3xPyFc4
přidal	přidat	k5eAaPmAgInS
i	i	k9
rubl	rubl	k1gInSc1
<g/>
.	.	kIx.
novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-11-11	2013-11-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Znak	znak	k1gInSc4
ruského	ruský	k2eAgInSc2d1
rublu	rubl	k1gInSc2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
standardu	standard	k1gInSc6
Unicode	Unicod	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlas	hlas	k1gInSc1
Ruska	Rusko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-7-10	2014-7-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Unicode	Unicod	k1gInSc5
7.0	7.0	k4
<g/>
.0	.0	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Asie	Asie	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ruský	ruský	k2eAgInSc4d1
rubl	rubl	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
)	)	kIx)
Na	na	k7c6
stránkách	stránka	k1gFnPc6
národní	národní	k2eAgFnSc2d1
banky	banka	k1gFnSc2
Ruska	Rusko	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
pamětní	pamětní	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
</s>
<s>
Ruské	ruský	k2eAgFnPc1d1
mince	mince	k1gFnPc1
</s>
<s>
Katalog	katalog	k1gInSc1
ruských	ruský	k2eAgFnPc2d1
pamětních	pamětní	k2eAgFnPc2d1
mincí	mince	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ruské	ruský	k2eAgFnSc2d1
mince	mince	k1gFnSc2
(	(	kIx(
<g/>
katalog	katalog	k1gInSc1
a	a	k8xC
galerie	galerie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Historické	historický	k2eAgFnPc4d1
a	a	k8xC
současné	současný	k2eAgFnPc4d1
bankovky	bankovka	k1gFnPc4
Ruska	Rusko	k1gNnSc2
(	(	kIx(
<g/>
včetně	včetně	k7c2
SSSR	SSSR	kA
<g/>
)	)	kIx)
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Měny	měna	k1gFnSc2
Asie	Asie	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
Střední	střední	k2eAgFnSc2d1
</s>
<s>
Kazachstánský	kazachstánský	k2eAgInSc1d1
tenge	tenge	k1gInSc1
•	•	k?
Kyrgyzský	kyrgyzský	k2eAgInSc1d1
som	soma	k1gFnPc2
•	•	k?
Tádžický	tádžický	k2eAgInSc1d1
somoni	somoň	k1gFnSc3
•	•	k?
Turkmenský	turkmenský	k2eAgMnSc1d1
manat	manat	k2eAgMnSc1d1
•	•	k?
Uzbecký	uzbecký	k2eAgInSc1d1
sum	suma	k1gFnPc2
Jihozápadní	jihozápadní	k2eAgMnPc1d1
</s>
<s>
Abchazský	abchazský	k2eAgInSc1d1
apsar	apsar	k1gInSc1
•	•	k?
Arménský	arménský	k2eAgInSc1d1
dram	drama	k1gFnPc2
•	•	k?
Ázerbájdžánský	ázerbájdžánský	k2eAgInSc1d1
manat	manat	k1gInSc1
•	•	k?
Bahrajnský	Bahrajnský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Irácký	irácký	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Íránský	íránský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
•	•	k?
Jemenský	jemenský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Katarský	katarský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Kuvajtský	kuvajtský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Libanonská	libanonský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Ománský	ománský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Saúdský	saúdský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
</s>
<s>
Afghánský	afghánský	k2eAgInSc1d1
afghání	afghání	k1gNnSc6
•	•	k?
Bangladéšská	bangladéšský	k2eAgFnSc1d1
taka	taka	k1gFnSc1
•	•	k?
Bhútánský	bhútánský	k2eAgInSc1d1
ngultrum	ngultrum	k1gNnSc1
•	•	k?
Indická	indický	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Maledivská	maledivský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Nepálská	nepálský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Pákistánská	pákistánský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Šrílanská	Šrílanský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
jüan	jüan	k1gInSc1
•	•	k?
Hongkongský	hongkongský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
•	•	k?
Jihokorejský	jihokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Macajská	macajský	k2eAgFnSc1d1
pataca	pataca	k1gFnSc1
•	•	k?
Mongolský	mongolský	k2eAgInSc1d1
tugrik	tugrik	k1gInSc1
•	•	k?
Tchajwanský	tchajwanský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
</s>
<s>
Brunejský	brunejský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Filipínské	filipínský	k2eAgNnSc4d1
peso	peso	k1gNnSc4
•	•	k?
Indonéská	indonéský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Kambodžský	kambodžský	k2eAgInSc1d1
riel	riel	k1gInSc1
•	•	k?
Laoský	laoský	k2eAgInSc1d1
kip	kip	k?
•	•	k?
Malajsijský	malajsijský	k2eAgInSc1d1
ringgit	ringgit	k1gInSc1
•	•	k?
Myanmarský	Myanmarský	k2eAgInSc1d1
kyat	kyat	k1gInSc1
•	•	k?
Severokorejský	severokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Singapurský	singapurský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Thajský	thajský	k2eAgInSc1d1
baht	baht	k1gInSc1
•	•	k?
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
•	•	k?
Východotimorské	Východotimorský	k2eAgFnPc4d1
centavové	centavový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
</s>
<s>
Měny	měna	k1gFnPc1
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4178574-5	4178574-5	k4
</s>
