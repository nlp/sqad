<p>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
forma	forma	k1gFnSc1	forma
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
pevného	pevný	k2eAgNnSc2d1	pevné
skupenství	skupenství	k1gNnSc2	skupenství
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
ledovými	ledový	k2eAgInPc7d1	ledový
krystalky	krystalek	k1gInPc7	krystalek
seskupenými	seskupený	k2eAgInPc7d1	seskupený
do	do	k7c2	do
sněhových	sněhový	k2eAgFnPc2d1	sněhová
vloček	vločka	k1gFnPc2	vločka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vzniká	vznikat	k5eAaImIp3nS	vznikat
přirozeně	přirozeně	k6eAd1	přirozeně
za	za	k7c2	za
vhodných	vhodný	k2eAgFnPc2d1	vhodná
klimatických	klimatický	k2eAgFnPc2d1	klimatická
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
na	na	k7c6	na
převážně	převážně	k6eAd1	převážně
biologickém	biologický	k2eAgInSc6d1	biologický
podkladu	podklad	k1gInSc6	podklad
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
bakterie	bakterie	k1gFnSc2	bakterie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
snáší	snášet	k5eAaImIp3nS	snášet
k	k	k7c3	k
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sněžení	sněžení	k1gNnSc1	sněžení
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
nahromaděný	nahromaděný	k2eAgInSc1d1	nahromaděný
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
na	na	k7c6	na
dostatečně	dostatečně	k6eAd1	dostatečně
prochladlém	prochladlý	k2eAgInSc6d1	prochladlý
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
dešťové	dešťový	k2eAgFnSc2d1	dešťová
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
dopadu	dopad	k1gInSc2	dopad
ihned	ihned	k6eAd1	ihned
neodtéká	odtékat	k5eNaImIp3nS	odtékat
<g/>
,	,	kIx,	,
představuje	představovat	k5eAaImIp3nS	představovat
tak	tak	k6eAd1	tak
důležitý	důležitý	k2eAgInSc1d1	důležitý
prvek	prvek	k1gInSc1	prvek
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
tepelně	tepelně	k6eAd1	tepelně
izolační	izolační	k2eAgFnSc1d1	izolační
schopnost	schopnost	k1gFnSc1	schopnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mnoha	mnoho	k4c3	mnoho
rostlinným	rostlinný	k2eAgFnPc3d1	rostlinná
i	i	k8xC	i
živočišným	živočišný	k2eAgFnPc3d1	živočišná
druhů	druh	k1gInPc2	druh
přečkat	přečkat	k5eAaPmF	přečkat
mrazy	mráz	k1gInPc4	mráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
vločka	vločka	k1gFnSc1	vločka
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
panujících	panující	k2eAgFnPc2d1	panující
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
voda	voda	k1gFnSc1	voda
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
ledu	led	k1gInSc2	led
Ih	Ih	k1gFnSc2	Ih
v	v	k7c6	v
šesterečné	šesterečný	k2eAgFnSc6d1	šesterečná
krystalografické	krystalografický	k2eAgFnSc6d1	krystalografická
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
určuje	určovat	k5eAaImIp3nS	určovat
šesterečnou	šesterečný	k2eAgFnSc4d1	šesterečná
symetrii	symetrie	k1gFnSc4	symetrie
vloček	vločka	k1gFnPc2	vločka
<g/>
.	.	kIx.	.
</s>
<s>
Krystalky	krystalek	k1gInPc1	krystalek
jsou	být	k5eAaImIp3nP	být
formovány	formovat	k5eAaImNgInP	formovat
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
nízké	nízký	k2eAgFnSc2d1	nízká
prostorové	prostorový	k2eAgFnSc2d1	prostorová
koncentrace	koncentrace	k1gFnSc2	koncentrace
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nevzniká	vznikat	k5eNaImIp3nS	vznikat
celistvá	celistvý	k2eAgFnSc1d1	celistvá
masa	masa	k1gFnSc1	masa
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
lze	lze	k6eAd1	lze
běžně	běžně	k6eAd1	běžně
pozorovat	pozorovat	k5eAaImF	pozorovat
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
pevného	pevný	k2eAgNnSc2d1	pevné
skupenství	skupenství	k1gNnSc2	skupenství
na	na	k7c6	na
zemském	zemský	k2eAgInSc6d1	zemský
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
lokální	lokální	k2eAgFnSc7d1	lokální
kombinací	kombinace	k1gFnSc7	kombinace
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
koncentrace	koncentrace	k1gFnSc2	koncentrace
molekul	molekula	k1gFnPc2	molekula
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sněhové	sněhový	k2eAgInPc1d1	sněhový
vločky	vloček	k1gInPc1	vloček
vznikají	vznikat	k5eAaImIp3nP	vznikat
akrecí	akrece	k1gFnSc7	akrece
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
symetrie	symetrie	k1gFnSc1	symetrie
mírně	mírně	k6eAd1	mírně
porušena	porušit	k5eAaPmNgFnS	porušit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sestupu	sestup	k1gInSc6	sestup
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
ještě	ještě	k6eAd1	ještě
agregovat	agregovat	k5eAaImF	agregovat
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
shluků	shluk	k1gInPc2	shluk
opět	opět	k6eAd1	opět
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
klimatických	klimatický	k2eAgFnPc6d1	klimatická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
vločky	vločka	k1gFnSc2	vločka
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jevy	jev	k1gInPc1	jev
akrece	akrece	k1gFnSc2	akrece
i	i	k8xC	i
agregace	agregace	k1gFnSc2	agregace
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
fyzické	fyzický	k2eAgNnSc4d1	fyzické
přemisťování	přemisťování	k1gNnSc4	přemisťování
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
relativně	relativně	k6eAd1	relativně
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
napodobování	napodobování	k1gNnSc6	napodobování
tvorby	tvorba	k1gFnSc2	tvorba
sněhu	sníh	k1gInSc2	sníh
pomocí	pomocí	k7c2	pomocí
sněžných	sněžný	k2eAgNnPc2d1	Sněžné
děl	dělo	k1gNnPc2	dělo
jsou	být	k5eAaImIp3nP	být
vločky	vloček	k1gInPc1	vloček
formovány	formován	k2eAgInPc1d1	formován
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
menší	malý	k2eAgFnPc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
technický	technický	k2eAgInSc1d1	technický
sníh	sníh	k1gInSc1	sníh
od	od	k7c2	od
přírodního	přírodní	k2eAgNnSc2d1	přírodní
rozpoznatelný	rozpoznatelný	k2eAgInSc1d1	rozpoznatelný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hutnější	hutný	k2eAgFnSc1d2	hutnější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
symetrických	symetrický	k2eAgInPc2d1	symetrický
krystalků	krystalek	k1gInPc2	krystalek
v	v	k7c6	v
mracích	mrak	k1gInPc6	mrak
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
vhodné	vhodný	k2eAgFnSc6d1	vhodná
poloze	poloha	k1gFnSc6	poloha
Slunce	slunce	k1gNnSc2	slunce
vyvolat	vyvolat	k5eAaPmF	vyvolat
halové	halový	k2eAgInPc4d1	halový
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Molekulární	molekulární	k2eAgFnPc1d1	molekulární
síly	síla	k1gFnPc1	síla
si	se	k3xPyFc3	se
při	při	k7c6	při
formování	formování	k1gNnSc6	formování
sněhové	sněhový	k2eAgFnSc2d1	sněhová
vločky	vločka	k1gFnSc2	vločka
vynutí	vynutit	k5eAaPmIp3nP	vynutit
její	její	k3xOp3gFnSc4	její
skoro	skoro	k6eAd1	skoro
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
šesterečnou	šesterečný	k2eAgFnSc4d1	šesterečná
symetrii	symetrie	k1gFnSc4	symetrie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
vločkami	vločka	k1gFnPc7	vločka
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
již	již	k6eAd1	již
příliš	příliš	k6eAd1	příliš
veliké	veliký	k2eAgFnPc1d1	veliká
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
vločky	vločka	k1gFnPc4	vločka
při	při	k7c6	při
snášení	snášení	k1gNnSc6	snášení
se	se	k3xPyFc4	se
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
(	(	kIx(	(
<g/>
rychlostí	rychlost	k1gFnPc2	rychlost
0,3	[number]	k4	0,3
÷	÷	k?	÷
1	[number]	k4	1
m	m	kA	m
<g/>
·	·	k?	·
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
orientovány	orientovat	k5eAaBmNgFnP	orientovat
chaoticky	chaoticky	k6eAd1	chaoticky
a	a	k8xC	a
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
tak	tak	k9	tak
představuje	představovat	k5eAaImIp3nS	představovat
soubor	soubor	k1gInSc1	soubor
neuspořádaně	uspořádaně	k6eNd1	uspořádaně
nakupených	nakupený	k2eAgFnPc2d1	nakupená
vloček	vločka	k1gFnPc2	vločka
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
vlastností	vlastnost	k1gFnSc7	vlastnost
sněhu	sníh	k1gInSc2	sníh
jeho	jeho	k3xOp3gFnPc1	jeho
izotropie	izotropie	k1gFnPc1	izotropie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
způsob	způsob	k1gInSc4	způsob
jak	jak	k8xS	jak
vločky	vločka	k1gFnPc4	vločka
srovnat	srovnat	k5eAaPmF	srovnat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
vlastností	vlastnost	k1gFnPc2	vlastnost
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
napadaný	napadaný	k2eAgInSc1d1	napadaný
sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jen	jen	k9	jen
ze	z	k7c2	z
3	[number]	k4	3
%	%	kIx~	%
ledovými	ledový	k2eAgInPc7d1	ledový
krystalky	krystalek	k1gInPc7	krystalek
<g/>
,	,	kIx,	,
97	[number]	k4	97
%	%	kIx~	%
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
vzduchové	vzduchový	k2eAgFnPc4d1	vzduchová
mezery	mezera	k1gFnPc4	mezera
mezi	mezi	k7c7	mezi
krystaly	krystal	k1gInPc7	krystal
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hustota	hustota	k1gFnSc1	hustota
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
100	[number]	k4	100
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mechanické	mechanický	k2eAgInPc1d1	mechanický
===	===	k?	===
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
čerstvě	čerstvě	k6eAd1	čerstvě
napadaného	napadaný	k2eAgInSc2d1	napadaný
sněhu	sníh	k1gInSc2	sníh
značně	značně	k6eAd1	značně
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
vlhkosti	vlhkost	k1gFnSc6	vlhkost
začíná	začínat	k5eAaImIp3nS	začínat
na	na	k7c4	na
30	[number]	k4	30
kg	kg	kA	kg
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
a	a	k8xC	a
u	u	k7c2	u
mokrého	mokrý	k2eAgNnSc2d1	mokré
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
600	[number]	k4	600
kg	kg	kA	kg
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
při	při	k7c6	při
silných	silný	k2eAgInPc6d1	silný
deštích	dešť	k1gInPc6	dešť
může	moct	k5eAaImIp3nS	moct
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
až	až	k9	až
k	k	k7c3	k
hustotě	hustota	k1gFnSc3	hustota
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tepelné	tepelný	k2eAgMnPc4d1	tepelný
===	===	k?	===
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
tepelný	tepelný	k2eAgInSc1d1	tepelný
izolant	izolant	k1gInSc1	izolant
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
napadaný	napadaný	k2eAgInSc1d1	napadaný
sníh	sníh	k1gInSc1	sníh
má	mít	k5eAaImIp3nS	mít
součinitel	součinitel	k1gInSc4	součinitel
tepelné	tepelný	k2eAgFnSc2d1	tepelná
vodivosti	vodivost	k1gFnSc2	vodivost
asi	asi	k9	asi
0,03	[number]	k4	0,03
W	W	kA	W
<g/>
·	·	k?	·
<g/>
m	m	kA	m
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
·	·	k?	·
<g/>
K	K	kA	K
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
mokrý	mokrý	k2eAgInSc1d1	mokrý
uleželý	uleželý	k2eAgInSc1d1	uleželý
asi	asi	k9	asi
dvacetinásobně	dvacetinásobně	k6eAd1	dvacetinásobně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgFnSc1d1	zásadní
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
některých	některý	k3yIgInPc2	některý
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
a	a	k8xC	a
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mrzne	mrznout	k5eAaImIp3nS	mrznout
bez	bez	k7c2	bez
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
holomráz	holomráz	k1gInSc1	holomráz
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
sněhu	sníh	k1gInSc2	sníh
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vhodně	vhodně	k6eAd1	vhodně
postavené	postavený	k2eAgNnSc1d1	postavené
iglú	iglú	k1gNnSc1	iglú
vytopit	vytopit	k5eAaPmF	vytopit
relativně	relativně	k6eAd1	relativně
malým	malý	k2eAgInSc7d1	malý
zdrojem	zdroj	k1gInSc7	zdroj
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Téže	týž	k3xTgFnSc2	týž
vlastnosti	vlastnost	k1gFnSc2	vlastnost
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
i	i	k9	i
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neočekávané	očekávaný	k2eNgFnSc6d1	neočekávaná
potřebě	potřeba	k1gFnSc6	potřeba
přečkat	přečkat	k5eAaPmF	přečkat
noc	noc	k1gFnSc4	noc
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
lze	lze	k6eAd1	lze
vybudovat	vybudovat	k5eAaPmF	vybudovat
sněhový	sněhový	k2eAgInSc4d1	sněhový
záhrab	záhrab	k1gInSc4	záhrab
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
omezit	omezit	k5eAaPmF	omezit
tepelné	tepelný	k2eAgFnPc4d1	tepelná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Akustické	akustický	k2eAgMnPc4d1	akustický
===	===	k?	===
</s>
</p>
<p>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
zvukově	zvukově	k6eAd1	zvukově
izolační	izolační	k2eAgInSc4d1	izolační
materiál	materiál	k1gInSc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Deseticentimetrová	deseticentimetrový	k2eAgFnSc1d1	deseticentimetrová
vrstva	vrstva	k1gFnSc1	vrstva
čerstvě	čerstvě	k6eAd1	čerstvě
napadaného	napadaný	k2eAgInSc2d1	napadaný
sněhu	sníh	k1gInSc2	sníh
pohltí	pohltit	k5eAaPmIp3nS	pohltit
45	[number]	k4	45
%	%	kIx~	%
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
frekvenci	frekvence	k1gFnSc6	frekvence
125	[number]	k4	125
Hz	Hz	kA	Hz
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
lidskému	lidský	k2eAgNnSc3d1	lidské
uchu	ucho	k1gNnSc3	ucho
slyšitelné	slyšitelný	k2eAgFnSc2d1	slyšitelná
frekvence	frekvence	k1gFnSc2	frekvence
nad	nad	k7c7	nad
500	[number]	k4	500
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Optické	optický	k2eAgMnPc4d1	optický
===	===	k?	===
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
sněhové	sněhový	k2eAgFnPc1d1	sněhová
vločky	vločka	k1gFnPc1	vločka
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
kostka	kostka	k1gFnSc1	kostka
ledu	led	k1gInSc2	led
průhledné	průhledný	k2eAgFnPc4d1	průhledná
ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
bílá	bílý	k2eAgFnSc1d1	bílá
barva	barva	k1gFnSc1	barva
sněhu	sníh	k1gInSc2	sníh
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidíme	vidět	k5eAaImIp1nP	vidět
mnoho	mnoho	k4c4	mnoho
vloček	vločka	k1gFnPc2	vločka
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
vlastně	vlastně	k9	vlastně
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
hran	hrana	k1gFnPc2	hrana
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
a	a	k8xC	a
láme	lámat	k5eAaImIp3nS	lámat
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Oko	oko	k1gNnSc1	oko
a	a	k8xC	a
mozek	mozek	k1gInSc1	mozek
pak	pak	k6eAd1	pak
výsledek	výsledek	k1gInSc1	výsledek
vyhodnotí	vyhodnotit	k5eAaPmIp3nS	vyhodnotit
jako	jako	k9	jako
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
uváděnou	uváděný	k2eAgFnSc7d1	uváděná
analogií	analogie	k1gFnSc7	analogie
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
je	být	k5eAaImIp3nS	být
efektivní	efektivní	k2eAgNnSc4d1	efektivní
zbělení	zbělení	k1gNnSc4	zbělení
automobilového	automobilový	k2eAgNnSc2d1	automobilové
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
nárazu	náraz	k1gInSc6	náraz
roztříští	roztříštit	k5eAaPmIp3nS	roztříštit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
drobných	drobný	k2eAgInPc2d1	drobný
střípků	střípek	k1gInPc2	střípek
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
hranami	hrana	k1gFnPc7	hrana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
napadaný	napadaný	k2eAgInSc1d1	napadaný
sníh	sníh	k1gInSc1	sníh
odráží	odrážet	k5eAaImIp3nS	odrážet
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc1	albedo
=	=	kIx~	=
0,9	[number]	k4	0,9
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
staršího	starý	k2eAgInSc2d2	starší
sněhu	sníh	k1gInSc2	sníh
odrazivost	odrazivost	k1gFnSc4	odrazivost
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
tedy	tedy	k9	tedy
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
při	při	k7c6	při
odtávání	odtávání	k1gNnSc6	odtávání
kladnou	kladný	k2eAgFnSc4d1	kladná
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
–	–	k?	–
sníh	sníh	k1gInSc1	sníh
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
světla	světlo	k1gNnSc2	světlo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nataje	nataj	k1gInSc2	nataj
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
klesne	klesnout	k5eAaPmIp3nS	klesnout
jeho	jeho	k3xOp3gFnSc1	jeho
odrazivost	odrazivost	k1gFnSc1	odrazivost
<g/>
,	,	kIx,	,
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
více	hodně	k6eAd2	hodně
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
odtává	odtávat	k5eAaImIp3nS	odtávat
stále	stále	k6eAd1	stále
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
klesá	klesat	k5eAaImIp3nS	klesat
jeho	jeho	k3xOp3gFnSc4	jeho
odrazivost	odrazivost	k1gFnSc4	odrazivost
k	k	k7c3	k
nule	nula	k1gFnSc3	nula
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vlnové	vlnový	k2eAgFnSc2d1	vlnová
délky	délka	k1gFnSc2	délka
1,5	[number]	k4	1,5
μ	μ	k?	μ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
dobře	dobře	k6eAd1	dobře
pohlcováno	pohlcovat	k5eAaImNgNnS	pohlcovat
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
ultrafialové	ultrafialový	k2eAgFnSc6d1	ultrafialová
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
odrazivost	odrazivost	k1gFnSc1	odrazivost
čerstvě	čerstvě	k6eAd1	čerstvě
napadaného	napadaný	k2eAgInSc2d1	napadaný
sněhu	sníh	k1gInSc2	sníh
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
z	z	k7c2	z
mikroskopického	mikroskopický	k2eAgInSc2d1	mikroskopický
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
rovinných	rovinný	k2eAgFnPc2d1	rovinná
stěn	stěna	k1gFnPc2	stěna
ledových	ledový	k2eAgInPc2d1	ledový
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
dobře	dobře	k6eAd1	dobře
odrazí	odrazit	k5eAaPmIp3nP	odrazit
krátkovlnné	krátkovlnný	k2eAgNnSc4d1	krátkovlnné
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barva	barva	k1gFnSc1	barva
sněhu	sníh	k1gInSc2	sníh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zásadně	zásadně	k6eAd1	zásadně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
příměsemi	příměse	k1gFnPc7	příměse
přítomnými	přítomný	k2eAgFnPc7d1	přítomná
v	v	k7c6	v
oblacích	oblak	k1gInPc6	oblak
<g/>
.	.	kIx.	.
</s>
<s>
Jemný	jemný	k2eAgInSc1d1	jemný
písek	písek	k1gInSc1	písek
z	z	k7c2	z
pouštních	pouštní	k2eAgFnPc2d1	pouštní
oblastí	oblast	k1gFnPc2	oblast
může	moct	k5eAaImIp3nS	moct
sníh	sníh	k1gInSc1	sníh
zbarvit	zbarvit	k5eAaPmF	zbarvit
do	do	k7c2	do
žluta	žluto	k1gNnSc2	žluto
nebo	nebo	k8xC	nebo
oranžova	oranžovo	k1gNnSc2	oranžovo
<g/>
,	,	kIx,	,
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
znečištění	znečištění	k1gNnSc1	znečištění
pak	pak	k6eAd1	pak
prakticky	prakticky	k6eAd1	prakticky
jakkoli	jakkoli	k6eAd1	jakkoli
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sníh	sníh	k1gInSc4	sníh
ve	v	k7c6	v
městech	město	k1gNnPc6	město
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozbředlý	rozbředlý	k2eAgInSc1d1	rozbředlý
sníh	sníh	k1gInSc1	sníh
nejprve	nejprve	k6eAd1	nejprve
přijme	přijmout	k5eAaPmIp3nS	přijmout
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
(	(	kIx(	(
<g/>
sněhová	sněhový	k2eAgFnSc1d1	sněhová
břečka	břečka	k1gFnSc1	břečka
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
při	při	k7c6	při
nakupení	nakupení	k1gNnSc6	nakupení
do	do	k7c2	do
postupně	postupně	k6eAd1	postupně
odtávajících	odtávající	k2eAgFnPc2d1	odtávající
kup	kupa	k1gFnPc2	kupa
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
černá	černý	k2eAgFnSc1d1	černá
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
usazování	usazování	k1gNnSc2	usazování
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Měření	měření	k1gNnPc1	měření
a	a	k8xC	a
klasifikace	klasifikace	k1gFnSc1	klasifikace
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
==	==	k?	==
</s>
</p>
<p>
<s>
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
klasifikovat	klasifikovat	k5eAaImF	klasifikovat
podle	podle	k7c2	podle
rozličných	rozličný	k2eAgFnPc2d1	rozličná
charakteristik	charakteristika	k1gFnPc2	charakteristika
(	(	kIx(	(
<g/>
hustota	hustota	k1gFnSc1	hustota
<g/>
,	,	kIx,	,
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
typ	typ	k1gInSc4	typ
zrn	zrno	k1gNnPc2	zrno
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Užívána	užíván	k2eAgFnSc1d1	užívána
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
klasifikace	klasifikace	k1gFnSc1	klasifikace
sezónní	sezónní	k2eAgFnSc2d1	sezónní
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
revidovanou	revidovaný	k2eAgFnSc7d1	revidovaná
verzí	verze	k1gFnSc7	verze
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
klasifikace	klasifikace	k1gFnSc2	klasifikace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
klasifikace	klasifikace	k1gFnSc1	klasifikace
třídí	třídit	k5eAaImIp3nS	třídit
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
pokrývku	pokrývka	k1gFnSc4	pokrývka
do	do	k7c2	do
typů	typ	k1gInPc2	typ
dle	dle	k7c2	dle
následujících	následující	k2eAgFnPc2d1	následující
charakteristik	charakteristika	k1gFnPc2	charakteristika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Tvar	tvar	k1gInSc1	tvar
sněhových	sněhový	k2eAgNnPc2d1	sněhové
zrn	zrno	k1gNnPc2	zrno
===	===	k?	===
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
tvaru	tvar	k1gInSc2	tvar
jsou	být	k5eAaImIp3nP	být
sněhová	sněhový	k2eAgNnPc4d1	sněhové
zrna	zrno	k1gNnPc4	zrno
sezónní	sezónní	k2eAgFnSc2d1	sezónní
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
členěna	členit	k5eAaImNgFnS	členit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
jde	jít	k5eAaImIp3nS	jít
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
členit	členit	k5eAaImF	členit
do	do	k7c2	do
řady	řada	k1gFnSc2	řada
dalších	další	k2eAgInPc2d1	další
subtypů	subtyp	k1gInPc2	subtyp
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
typy	typ	k1gInPc7	typ
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Srážkové	srážkový	k2eAgFnPc1d1	srážková
částice	částice	k1gFnPc1	částice
(	(	kIx(	(
<g/>
nový	nový	k2eAgInSc1d1	nový
sníh	sníh	k1gInSc1	sníh
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
sněhová	sněhový	k2eAgNnPc1d1	sněhové
zrna	zrno	k1gNnPc1	zrno
mají	mít	k5eAaImIp3nP	mít
zachovalou	zachovalý	k2eAgFnSc4d1	zachovalá
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
v	v	k7c4	v
jaké	jaký	k3yIgMnPc4	jaký
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
rozličných	rozličný	k2eAgFnPc2d1	rozličná
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
charakteristik	charakteristika	k1gFnPc2	charakteristika
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
nasycení	nasycení	k1gNnSc2	nasycení
<g/>
/	/	kIx~	/
<g/>
přesycení	přesycení	k1gNnSc1	přesycení
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
sněhových	sněhový	k2eAgInPc2d1	sněhový
krystalů	krystal	k1gInPc2	krystal
<g/>
:	:	kIx,	:
sloupečky	sloupeček	k1gInPc1	sloupeček
<g/>
,	,	kIx,	,
jehličky	jehlička	k1gFnPc1	jehlička
<g/>
,	,	kIx,	,
destičky	destička	k1gFnPc1	destička
<g/>
,	,	kIx,	,
hvězdice	hvězdice	k1gFnPc1	hvězdice
(	(	kIx(	(
<g/>
dendrity	dendrit	k1gInPc1	dendrit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepravidelné	pravidelný	k2eNgInPc1d1	nepravidelný
krystaly	krystal	k1gInPc1	krystal
<g/>
,	,	kIx,	,
kroupy	kroupa	k1gFnPc1	kroupa
<g/>
,	,	kIx,	,
krupky	krupka	k1gFnPc1	krupka
<g/>
,	,	kIx,	,
ledová	ledový	k2eAgNnPc4d1	ledové
zrnka	zrnko	k1gNnPc4	zrnko
<g/>
,	,	kIx,	,
jíní	jínit	k5eAaImIp3nS	jínit
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
poměry	poměr	k1gInPc1	poměr
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterých	který	k3yQgInPc2	který
srážkové	srážkový	k2eAgFnPc1d1	srážková
částice	částice	k1gFnPc1	částice
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
poměrů	poměr	k1gInPc2	poměr
ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
pokrývce	pokrývka	k1gFnSc6	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
přesycení	přesycení	k1gNnSc4	přesycení
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ledu	led	k1gInSc3	led
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
pokrývce	pokrývka	k1gFnSc6	pokrývka
obvykle	obvykle	k6eAd1	obvykle
hodnot	hodnota	k1gFnPc2	hodnota
mezi	mezi	k7c7	mezi
0	[number]	k4	0
a	a	k8xC	a
1	[number]	k4	1
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
srážkových	srážkový	k2eAgFnPc2d1	srážková
částic	částice	k1gFnPc2	částice
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zpravidla	zpravidla	k6eAd1	zpravidla
hodnot	hodnota	k1gFnPc2	hodnota
mnohem	mnohem	k6eAd1	mnohem
vyšších	vysoký	k2eAgInPc2d2	vyšší
<g/>
,	,	kIx,	,
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
50	[number]	k4	50
%	%	kIx~	%
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
srážkové	srážkový	k2eAgFnPc1d1	srážková
částice	částice	k1gFnPc1	částice
ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
pokrývce	pokrývka	k1gFnSc6	pokrývka
obvykle	obvykle	k6eAd1	obvykle
nemají	mít	k5eNaImIp3nP	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
prochází	procházet	k5eAaImIp3nS	procházet
metamorfózou	metamorfóza	k1gFnSc7	metamorfóza
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
podobnější	podobný	k2eAgMnSc1d2	podobnější
byly	být	k5eAaImAgInP	být
podmínky	podmínka	k1gFnPc4	podmínka
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
srážkové	srážkový	k2eAgFnSc2d1	srážková
částice	částice	k1gFnSc2	částice
podmínkám	podmínka	k1gFnPc3	podmínka
ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
pokrývce	pokrývka	k1gFnSc6	pokrývka
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
trvalejší	trvalý	k2eAgFnSc1d2	trvalejší
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
daný	daný	k2eAgInSc1d1	daný
subtyp	subtyp	k1gInSc1	subtyp
srážkové	srážkový	k2eAgFnSc2d1	srážková
částice	částice	k1gFnSc2	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zlomkový	zlomkový	k2eAgInSc1d1	zlomkový
(	(	kIx(	(
<g/>
plstnatý	plstnatý	k2eAgInSc1d1	plstnatý
sníh	sníh	k1gInSc1	sníh
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
vzniká	vznikat	k5eAaImIp3nS	vznikat
prvotní	prvotní	k2eAgFnSc7d1	prvotní
přeměnou	přeměna	k1gFnSc7	přeměna
srážkových	srážkový	k2eAgFnPc2d1	srážková
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jejich	jejich	k3xOp3gFnSc7	jejich
fragmentací	fragmentace	k1gFnSc7	fragmentace
působením	působení	k1gNnSc7	působení
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Zrna	zrno	k1gNnPc1	zrno
již	již	k9	již
mohou	moct	k5eAaImIp3nP	moct
nést	nést	k5eAaImF	nést
stopy	stopa	k1gFnPc4	stopa
zaoblování	zaoblování	k1gNnSc2	zaoblování
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
sněhová	sněhový	k2eAgNnPc1d1	sněhové
zrna	zrno	k1gNnPc1	zrno
do	do	k7c2	do
sebe	se	k3xPyFc2	se
zakliňují	zakliňovat	k5eAaImIp3nP	zakliňovat
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
můstky	můstek	k1gInPc7	můstek
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
rychlému	rychlý	k2eAgInSc3d1	rychlý
nárůstu	nárůst	k1gInSc3	nárůst
soudržnosti	soudržnost	k1gFnSc2	soudržnost
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
okrouhlozrnitý	okrouhlozrnitý	k2eAgInSc1d1	okrouhlozrnitý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
zaoblování	zaoblování	k1gNnSc3	zaoblování
sněhových	sněhový	k2eAgNnPc2d1	sněhové
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
důsledek	důsledek	k1gInSc4	důsledek
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
nazýván	nazýván	k2eAgInSc1d1	nazýván
jako	jako	k8xS	jako
bortící	bortící	k2eAgFnSc1d1	bortící
přeměna	přeměna	k1gFnSc1	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
mírného	mírný	k2eAgInSc2d1	mírný
tepelného	tepelný	k2eAgInSc2d1	tepelný
gradientu	gradient	k1gInSc2	gradient
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sublimaci	sublimace	k1gFnSc3	sublimace
ledu	led	k1gInSc2	led
z	z	k7c2	z
exponovaných	exponovaný	k2eAgFnPc2d1	exponovaná
částí	část	k1gFnPc2	část
zrn	zrno	k1gNnPc2	zrno
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
energií	energie	k1gFnSc7	energie
a	a	k8xC	a
desublimaci	desublimace	k1gFnSc3	desublimace
v	v	k7c6	v
konkávních	konkávní	k2eAgFnPc6d1	konkávní
částech	část	k1gFnPc6	část
zrn	zrno	k1gNnPc2	zrno
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
energií	energie	k1gFnSc7	energie
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
propojování	propojování	k1gNnSc3	propojování
sněhových	sněhový	k2eAgNnPc2d1	sněhové
zrn	zrno	k1gNnPc2	zrno
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
soudržnosti	soudržnost	k1gFnSc2	soudržnost
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hranatozrnitý	hranatozrnitý	k2eAgInSc1d1	hranatozrnitý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
vysokého	vysoký	k2eAgInSc2d1	vysoký
tepelného	tepelný	k2eAgInSc2d1	tepelný
gradientu	gradient	k1gInSc2	gradient
a	a	k8xC	a
velkého	velký	k2eAgInSc2d1	velký
tlaku	tlak	k1gInSc2	tlak
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
mezi	mezi	k7c7	mezi
zrny	zrno	k1gNnPc7	zrno
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výstavbové	výstavbový	k2eAgFnSc3d1	výstavbová
přeměně	přeměna	k1gFnSc3	přeměna
sněhových	sněhový	k2eAgNnPc2d1	sněhové
zrn	zrno	k1gNnPc2	zrno
-	-	kIx~	-
vznikají	vznikat	k5eAaImIp3nP	vznikat
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
rovné	rovný	k2eAgFnPc1d1	rovná
plošky	ploška	k1gFnPc1	ploška
s	s	k7c7	s
ostrými	ostrý	k2eAgFnPc7d1	ostrá
hranami	hrana	k1gFnPc7	hrana
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
postupnému	postupný	k2eAgInSc3d1	postupný
vzrůstu	vzrůst	k1gInSc3	vzrůst
velikosti	velikost	k1gFnSc2	velikost
krystalů	krystal	k1gInPc2	krystal
a	a	k8xC	a
k	k	k7c3	k
zanikání	zanikání	k1gNnSc3	zanikání
můstků	můstek	k1gInPc2	můstek
mezi	mezi	k7c7	mezi
zrny	zrno	k1gNnPc7	zrno
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
soudržnosti	soudržnost	k1gFnSc2	soudržnost
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
pohárkové	pohárkový	k2eAgInPc1d1	pohárkový
krystaly	krystal	k1gInPc1	krystal
(	(	kIx(	(
<g/>
dutinová	dutinový	k2eAgFnSc1d1	dutinová
jinovatka	jinovatka	k1gFnSc1	jinovatka
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
rychlou	rychlý	k2eAgFnSc7d1	rychlá
výstavbovou	výstavbový	k2eAgFnSc7d1	výstavbová
přeměnou	přeměna	k1gFnSc7	přeměna
sněhových	sněhový	k2eAgNnPc2d1	sněhové
zrn	zrno	k1gNnPc2	zrno
za	za	k7c2	za
velkého	velký	k2eAgInSc2d1	velký
teplotního	teplotní	k2eAgInSc2d1	teplotní
gradientu	gradient	k1gInSc2	gradient
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgFnPc6	takový
podmínkách	podmínka	k1gFnPc6	podmínka
vznikají	vznikat	k5eAaImIp3nP	vznikat
velmi	velmi	k6eAd1	velmi
velké	velký	k2eAgInPc1d1	velký
krystaly	krystal	k1gInPc1	krystal
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
dutých	dutý	k2eAgInPc2d1	dutý
pohárků	pohárek	k1gInPc2	pohárek
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
podmínky	podmínka	k1gFnPc1	podmínka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
sněhového	sněhový	k2eAgInSc2d1	sněhový
profilu	profil	k1gInSc2	profil
(	(	kIx(	(
<g/>
vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
<g/>
)	)	kIx)	)
v	v	k7c6	v
arktickém	arktický	k2eAgNnSc6d1	arktické
nebo	nebo	k8xC	nebo
kontinentálním	kontinentální	k2eAgNnSc6d1	kontinentální
podnebí	podnebí	k1gNnSc6	podnebí
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
teplotní	teplotní	k2eAgInSc1d1	teplotní
gradient	gradient	k1gInSc1	gradient
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dutinová	dutinový	k2eAgFnSc1d1	dutinová
jinovatka	jinovatka	k1gFnSc1	jinovatka
je	být	k5eAaImIp3nS	být
anizotropní	anizotropní	k2eAgFnSc1d1	anizotropní
-	-	kIx~	-
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
vertikálním	vertikální	k2eAgInSc6d1	vertikální
směru	směr	k1gInSc6	směr
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
pevností	pevnost	k1gFnSc7	pevnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
horizontálním	horizontální	k2eAgInSc6d1	horizontální
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nesoudržná	soudržný	k2eNgFnSc1d1	nesoudržná
a	a	k8xC	a
i	i	k9	i
díky	díky	k7c3	díky
obvykle	obvykle	k6eAd1	obvykle
špatné	špatný	k2eAgFnSc3d1	špatná
vazbě	vazba	k1gFnSc3	vazba
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
sněhové	sněhový	k2eAgFnPc4d1	sněhová
vrstvy	vrstva	k1gFnPc4	vrstva
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vzniku	vznik	k1gInSc2	vznik
značně	značně	k6eAd1	značně
nestabilní	stabilní	k2eNgFnPc1d1	nestabilní
sněhové	sněhový	k2eAgFnPc1d1	sněhová
pokrývky	pokrývka	k1gFnPc1	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
častou	častý	k2eAgFnSc4d1	častá
kritickou	kritický	k2eAgFnSc4d1	kritická
vrstvu	vrstva	k1gFnSc4	vrstva
deskových	deskový	k2eAgFnPc2d1	desková
lavin	lavina	k1gFnPc2	lavina
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poloze	poloha	k1gFnSc3	poloha
uvnitř	uvnitř	k7c2	uvnitř
sněhového	sněhový	k2eAgInSc2d1	sněhový
profilu	profil	k1gInSc2	profil
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
špatně	špatně	k6eAd1	špatně
odhalitelné	odhalitelný	k2eAgNnSc4d1	odhalitelné
riziko	riziko	k1gNnSc4	riziko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
časově	časově	k6eAd1	časově
velmi	velmi	k6eAd1	velmi
trvanlivé	trvanlivý	k2eAgInPc1d1	trvanlivý
-	-	kIx~	-
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pohárkové	pohárkový	k2eAgInPc1d1	pohárkový
krystaly	krystal	k1gInPc1	krystal
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
vydrží	vydržet	k5eAaPmIp3nS	vydržet
celou	celý	k2eAgFnSc4d1	celá
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
povrchová	povrchový	k2eAgFnSc1d1	povrchová
jinovatka	jinovatka	k1gFnSc1	jinovatka
<g/>
:	:	kIx,	:
Vzniká	vznikat	k5eAaImIp3nS	vznikat
za	za	k7c2	za
nízkých	nízký	k2eAgFnPc2d1	nízká
teplot	teplota	k1gFnPc2	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
vlivem	vlivem	k7c2	vlivem
radiačního	radiační	k2eAgNnSc2d1	radiační
ochlazování	ochlazování	k1gNnSc2	ochlazování
vyzařováním	vyzařování	k1gNnSc7	vyzařování
dlouhovlnného	dlouhovlnný	k2eAgNnSc2d1	dlouhovlnné
záření	záření	k1gNnSc2	záření
ze	z	k7c2	z
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
dojde	dojít	k5eAaPmIp3nS	dojít
nad	nad	k7c7	nad
sněhem	sníh	k1gInSc7	sníh
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
mělké	mělký	k2eAgFnPc4d1	mělká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgFnPc4d1	výrazná
teplotní	teplotní	k2eAgFnPc4d1	teplotní
inverze	inverze	k1gFnPc4	inverze
<g/>
,	,	kIx,	,
dojde	dojít	k5eAaPmIp3nS	dojít
přesycení	přesycení	k1gNnSc1	přesycení
vzduchu	vzduch	k1gInSc2	vzduch
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
vůči	vůči	k7c3	vůči
ledu	led	k1gInSc3	led
a	a	k8xC	a
vysrážení	vysrážení	k1gNnSc3	vysrážení
krystalů	krystal	k1gInPc2	krystal
povrchové	povrchový	k2eAgFnSc2d1	povrchová
jinovatky	jinovatka	k1gFnSc2	jinovatka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
anizotropní	anizotropní	k2eAgFnSc1d1	anizotropní
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
malou	malý	k2eAgFnSc4d1	malá
soudržnost	soudržnost	k1gFnSc4	soudržnost
ve	v	k7c6	v
střihu	střih	k1gInSc6	střih
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
zasněžení	zasněžení	k1gNnSc1	zasněžení
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
"	"	kIx"	"
<g/>
ideální	ideální	k2eAgFnSc4d1	ideální
<g/>
"	"	kIx"	"
kritickou	kritický	k2eAgFnSc4d1	kritická
vrstvu	vrstva	k1gFnSc4	vrstva
deskových	deskový	k2eAgFnPc2d1	desková
lavin	lavina	k1gFnPc2	lavina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tavné	tavný	k2eAgFnPc1d1	tavná
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
firn	firn	k1gInSc1	firn
<g/>
:	:	kIx,	:
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
jednak	jednak	k8xC	jednak
sníh	sníh	k1gInSc1	sníh
s	s	k7c7	s
makroskopicky	makroskopicky	k6eAd1	makroskopicky
rozpoznatelným	rozpoznatelný	k2eAgNnSc7d1	rozpoznatelné
množstvím	množství	k1gNnSc7	množství
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
tekutém	tekutý	k2eAgNnSc6d1	tekuté
skupenství	skupenství	k1gNnSc6	skupenství
a	a	k8xC	a
jednak	jednak	k8xC	jednak
sněhové	sněhový	k2eAgFnSc2d1	sněhová
krusty	krusta	k1gFnSc2	krusta
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
střídáním	střídání	k1gNnSc7	střídání
cyklů	cyklus	k1gInPc2	cyklus
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Částice	částice	k1gFnSc1	částice
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
vznikají	vznikat	k5eAaImIp3nP	vznikat
tavnou	tavný	k2eAgFnSc7d1	tavná
přeměnou	přeměna	k1gFnSc7	přeměna
sněhových	sněhový	k2eAgNnPc2d1	sněhové
zrn	zrno	k1gNnPc2	zrno
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Soudržnost	soudržnost	k1gFnSc1	soudržnost
klesá	klesat	k5eAaImIp3nS	klesat
se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
podílem	podíl	k1gInSc7	podíl
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
mokrý	mokrý	k2eAgInSc1d1	mokrý
sníh	sníh	k1gInSc1	sníh
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zdrojem	zdroj	k1gInSc7	zdroj
břečkových	břečkův	k2eAgFnPc2d1	břečkův
lavin	lavina	k1gFnPc2	lavina
(	(	kIx(	(
<g/>
břečkotoků	břečkotok	k1gInPc2	břečkotok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
na	na	k7c6	na
vůbec	vůbec	k9	vůbec
nejmírnějších	mírný	k2eAgInPc6d3	nejmírnější
svazích	svah	k1gInPc6	svah
(	(	kIx(	(
<g/>
od	od	k7c2	od
5	[number]	k4	5
<g/>
°	°	k?	°
výš	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sněhové	sněhový	k2eAgFnPc1d1	sněhová
krusty	krusta	k1gFnPc1	krusta
bývají	bývat	k5eAaImIp3nP	bývat
velmi	velmi	k6eAd1	velmi
soudržné	soudržný	k2eAgInPc1d1	soudržný
(	(	kIx(	(
<g/>
soudržnost	soudržnost	k1gFnSc1	soudržnost
roste	růst	k5eAaImIp3nS	růst
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
cyklů	cyklus	k1gInPc2	cyklus
teplo	teplo	k1gNnSc1	teplo
<g/>
/	/	kIx~	/
<g/>
mráz	mráz	k1gInSc1	mráz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
bariéru	bariéra	k1gFnSc4	bariéra
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
ve	v	k7c6	v
sněhovém	sněhový	k2eAgInSc6d1	sněhový
profilu	profil	k1gInSc6	profil
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
typů	typ	k1gInPc2	typ
sněhu	sníh	k1gInSc2	sníh
(	(	kIx(	(
<g/>
hranatorznitý	hranatorznitý	k2eAgMnSc1d1	hranatorznitý
<g/>
,	,	kIx,	,
pohárkové	pohárkový	k2eAgInPc1d1	pohárkový
krystaly	krystal	k1gInPc1	krystal
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
a	a	k8xC	a
nad	nad	k7c7	nad
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
krustou	krusta	k1gFnSc7	krusta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
led	led	k1gInSc4	led
<g/>
:	:	kIx,	:
ledové	ledový	k2eAgFnPc4d1	ledová
krusty	krusta	k1gFnPc4	krusta
nejrůznější	různý	k2eAgFnSc2d3	nejrůznější
tloušťky	tloušťka	k1gFnSc2	tloušťka
a	a	k8xC	a
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
bariéru	bariéra	k1gFnSc4	bariéra
pohybu	pohyb	k1gInSc2	pohyb
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
profilu	profil	k1gInSc6	profil
nebo	nebo	k8xC	nebo
po	po	k7c6	po
nich	on	k3xPp3gInPc6	on
může	moct	k5eAaImIp3nS	moct
stékat	stékat	k5eAaImF	stékat
tavná	tavný	k2eAgFnSc1d1	tavná
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
umělý	umělý	k2eAgInSc1d1	umělý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
sníh	sníh	k1gInSc1	sníh
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
činností	činnost	k1gFnSc7	činnost
sněžných	sněžný	k2eAgNnPc2d1	Sněžné
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgMnPc4d1	malý
<g/>
,	,	kIx,	,
sférické	sférický	k2eAgFnPc1d1	sférická
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
zrn	zrno	k1gNnPc2	zrno
===	===	k?	===
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
jemný	jemný	k2eAgInSc1d1	jemný
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
pod	pod	k7c4	pod
0,2	[number]	k4	0,2
mm	mm	kA	mm
</s>
</p>
<p>
<s>
jemný	jemný	k2eAgInSc1d1	jemný
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
0,2	[number]	k4	0,2
-	-	kIx~	-
0,5	[number]	k4	0,5
mm	mm	kA	mm
</s>
</p>
<p>
<s>
sníh	sníh	k1gInSc1	sníh
střední	střední	k2eAgFnSc2d1	střední
hrubosti	hrubost	k1gFnSc2	hrubost
<g/>
:	:	kIx,	:
0,5	[number]	k4	0,5
-	-	kIx~	-
1	[number]	k4	1
mm	mm	kA	mm
</s>
</p>
<p>
<s>
hrubý	hrubý	k2eAgInSc1d1	hrubý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
1	[number]	k4	1
-	-	kIx~	-
2	[number]	k4	2
mm	mm	kA	mm
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
hrubý	hrubý	k2eAgInSc1d1	hrubý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
2	[number]	k4	2
-	-	kIx~	-
5	[number]	k4	5
mm	mm	kA	mm
</s>
</p>
<p>
<s>
extrémně	extrémně	k6eAd1	extrémně
hrubý	hrubý	k2eAgInSc1d1	hrubý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
5	[number]	k4	5
a	a	k8xC	a
více	hodně	k6eAd2	hodně
mm	mm	kA	mm
</s>
</p>
<p>
<s>
===	===	k?	===
Obsah	obsah	k1gInSc1	obsah
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
===	===	k?	===
</s>
</p>
<p>
<s>
suchý	suchý	k2eAgInSc1d1	suchý
sníh	sníh	k1gInSc1	sníh
<g/>
:	:	kIx,	:
0	[number]	k4	0
%	%	kIx~	%
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sněhová	sněhový	k2eAgNnPc1d1	sněhové
zrna	zrno	k1gNnPc1	zrno
mají	mít	k5eAaImIp3nP	mít
malou	malý	k2eAgFnSc4d1	malá
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
vázat	vázat	k5eAaImF	vázat
(	(	kIx(	(
<g/>
špatně	špatně	k6eAd1	špatně
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
sněhová	sněhový	k2eAgFnSc1d1	sněhová
koule	koule	k1gFnSc1	koule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vlhký	vlhký	k2eAgMnSc1d1	vlhký
<g/>
:	:	kIx,	:
0	[number]	k4	0
-	-	kIx~	-
3	[number]	k4	3
%	%	kIx~	%
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
není	být	k5eNaImIp3nS	být
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
ani	ani	k8xC	ani
při	při	k7c6	při
desetinásobném	desetinásobný	k2eAgNnSc6d1	desetinásobné
zvětšení	zvětšení	k1gNnSc6	zvětšení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
se	se	k3xPyFc4	se
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
sněhová	sněhový	k2eAgNnPc1d1	sněhové
zrna	zrno	k1gNnPc1	zrno
snadno	snadno	k6eAd1	snadno
vážnou	vážná	k1gFnSc4	vážná
(	(	kIx(	(
<g/>
sníh	sníh	k1gInSc1	sníh
dobře	dobře	k6eAd1	dobře
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
sněhové	sněhový	k2eAgFnSc2d1	sněhová
koule	koule	k1gFnSc2	koule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
mokrý	mokrý	k2eAgMnSc1d1	mokrý
<g/>
:	:	kIx,	:
3	[number]	k4	3
-	-	kIx~	-
8	[number]	k4	8
%	%	kIx~	%
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
makroskopicky	makroskopicky	k6eAd1	makroskopicky
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
ještě	ještě	k9	ještě
ze	z	k7c2	z
sněhu	sníh	k1gInSc2	sníh
vymačkat	vymačkat	k5eAaPmF	vymačkat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
mokrý	mokrý	k2eAgMnSc1d1	mokrý
<g/>
:	:	kIx,	:
8	[number]	k4	8
-	-	kIx~	-
15	[number]	k4	15
%	%	kIx~	%
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
jde	jít	k5eAaImIp3nS	jít
ze	z	k7c2	z
sněhu	sníh	k1gInSc2	sníh
vymačkat	vymačkat	k5eAaPmF	vymačkat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pórech	pór	k1gInPc6	pór
mezi	mezi	k7c7	mezi
zrny	zrno	k1gNnPc7	zrno
však	však	k8xC	však
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
určitý	určitý	k2eAgInSc4d1	určitý
objem	objem	k1gInSc4	objem
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
promoklý	promoklý	k2eAgMnSc1d1	promoklý
<g/>
:	:	kIx,	:
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
15	[number]	k4	15
%	%	kIx~	%
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
Podíl	podíl	k1gInSc1	podíl
vzduchu	vzduch	k1gInSc2	vzduch
klesá	klesat	k5eAaImIp3nS	klesat
na	na	k7c4	na
20	[number]	k4	20
-	-	kIx~	-
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tvrdost	tvrdost	k1gFnSc4	tvrdost
sněhu	sníh	k1gInSc2	sníh
===	===	k?	===
</s>
</p>
<p>
<s>
Tvrdostí	tvrdost	k1gFnSc7	tvrdost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
klasifikaci	klasifikace	k1gFnSc6	klasifikace
myšlena	myšlen	k2eAgFnSc1d1	myšlena
odolnost	odolnost	k1gFnSc1	odolnost
vůči	vůči	k7c3	vůči
průniku	průnik	k1gInSc3	průnik
předmětu	předmět	k1gInSc2	předmět
do	do	k7c2	do
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
terénního	terénní	k2eAgNnSc2d1	terénní
měření	měření	k1gNnSc2	měření
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
tvrdost	tvrdost	k1gFnSc1	tvrdost
sněhu	sníh	k1gInSc2	sníh
určuje	určovat	k5eAaImIp3nS	určovat
ručním	ruční	k2eAgNnSc7d1	ruční
měřením	měření	k1gNnSc7	měření
tvrdosti	tvrdost	k1gFnSc2	tvrdost
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
rukou	ruka	k1gFnPc2	ruka
<g/>
,	,	kIx,	,
tužky	tužka	k1gFnSc2	tužka
a	a	k8xC	a
nože	nůž	k1gInSc2	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesnější	přesný	k2eAgNnSc4d2	přesnější
a	a	k8xC	a
objektivnější	objektivní	k2eAgNnSc4d2	objektivnější
určení	určení	k1gNnSc4	určení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
kladivové	kladivový	k2eAgFnSc2d1	Kladivová
sondy	sonda	k1gFnSc2	sonda
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
R	R	kA	R
hodnota	hodnota	k1gFnSc1	hodnota
odrazu	odraz	k1gInSc2	odraz
v	v	k7c6	v
newtonech	newton	k1gInPc6	newton
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
tvrdosti	tvrdost	k1gFnSc2	tvrdost
sněhu	sníh	k1gInSc2	sníh
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
měkký	měkký	k2eAgInSc1d1	měkký
sníh	sníh	k1gInSc1	sníh
-	-	kIx~	-
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
probořit	probořit	k5eAaPmF	probořit
zaťatou	zaťatý	k2eAgFnSc4d1	zaťatá
pěst	pěst	k1gFnSc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
R	R	kA	R
hodnoty	hodnota	k1gFnPc4	hodnota
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
kladivové	kladivový	k2eAgFnSc2d1	Kladivová
sondy	sonda	k1gFnSc2	sonda
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
0	[number]	k4	0
-	-	kIx~	-
50	[number]	k4	50
N.	N.	kA	N.
</s>
</p>
<p>
<s>
měkký	měkký	k2eAgInSc1d1	měkký
sníh	sníh	k1gInSc1	sníh
-	-	kIx~	-
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
probořit	probořit	k5eAaPmF	probořit
čtyři	čtyři	k4xCgInPc4	čtyři
prsty	prst	k1gInPc4	prst
(	(	kIx(	(
<g/>
narovnaná	narovnaný	k2eAgFnSc1d1	narovnaná
dlaň	dlaň	k1gFnSc1	dlaň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
R	R	kA	R
hodnoty	hodnota	k1gFnSc2	hodnota
50	[number]	k4	50
-	-	kIx~	-
175	[number]	k4	175
N	N	kA	N
</s>
</p>
<p>
<s>
středně	středně	k6eAd1	středně
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
sníh	sníh	k1gInSc1	sníh
-	-	kIx~	-
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
probořit	probořit	k5eAaPmF	probořit
jeden	jeden	k4xCgInSc4	jeden
prst	prst	k1gInSc4	prst
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
R	R	kA	R
hodnoty	hodnota	k1gFnSc2	hodnota
175	[number]	k4	175
-	-	kIx~	-
390	[number]	k4	390
N	N	kA	N
</s>
</p>
<p>
<s>
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
sníh	sníh	k1gInSc1	sníh
-	-	kIx~	-
lze	lze	k6eAd1	lze
probořit	probořit	k5eAaPmF	probořit
hrot	hrot	k1gInSc4	hrot
tužky	tužka	k1gFnSc2	tužka
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
R	R	kA	R
hodnoty	hodnota	k1gFnSc2	hodnota
390	[number]	k4	390
-	-	kIx~	-
715	[number]	k4	715
N	N	kA	N
</s>
</p>
<p>
<s>
velmi	velmi	k6eAd1	velmi
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
sníh	sníh	k1gInSc1	sníh
-	-	kIx~	-
lze	lze	k6eAd1	lze
probořit	probořit	k5eAaPmF	probořit
čepel	čepel	k1gFnSc4	čepel
nože	nůž	k1gInSc2	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídající	odpovídající	k2eAgInSc4d1	odpovídající
R	R	kA	R
hodnoty	hodnota	k1gFnSc2	hodnota
715	[number]	k4	715
-	-	kIx~	-
1200	[number]	k4	1200
N	N	kA	N
</s>
</p>
<p>
<s>
led	led	k1gInSc1	led
-	-	kIx~	-
nelze	lze	k6eNd1	lze
bez	bez	k7c2	bez
velkého	velký	k2eAgInSc2d1	velký
tlaku	tlak	k1gInSc2	tlak
probořit	probořit	k5eAaPmF	probořit
ani	ani	k8xC	ani
čepel	čepel	k1gFnSc4	čepel
nože	nůž	k1gInSc2	nůž
<g/>
.	.	kIx.	.
</s>
<s>
R	R	kA	R
hodnoty	hodnota	k1gFnSc2	hodnota
nad	nad	k7c4	nad
1200	[number]	k4	1200
NRozložení	NRozložení	k1gNnPc2	NRozložení
tvrdostí	tvrdost	k1gFnPc2	tvrdost
sněhu	sníh	k1gInSc2	sníh
ve	v	k7c6	v
sněhovém	sněhový	k2eAgInSc6d1	sněhový
profilu	profil	k1gInSc6	profil
může	moct	k5eAaImIp3nS	moct
posloužit	posloužit	k5eAaPmF	posloužit
jako	jako	k9	jako
dobrý	dobrý	k2eAgInSc4d1	dobrý
ukazatel	ukazatel	k1gInSc4	ukazatel
stability	stabilita	k1gFnSc2	stabilita
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
s	s	k7c7	s
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
sněhem	sníh	k1gInSc7	sníh
na	na	k7c6	na
spodu	spod	k1gInSc6	spod
a	a	k8xC	a
postupným	postupný	k2eAgNnSc7d1	postupné
snižováním	snižování	k1gNnSc7	snižování
tvrdosti	tvrdost	k1gFnSc2	tvrdost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sněhového	sněhový	k2eAgInSc2d1	sněhový
profilu	profil	k1gInSc2	profil
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
rovnoměrnou	rovnoměrný	k2eAgFnSc7d1	rovnoměrná
tvrdostí	tvrdost	k1gFnSc7	tvrdost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celého	celý	k2eAgInSc2d1	celý
profilu	profil	k1gInSc2	profil
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
výrazně	výrazně	k6eAd1	výrazně
soudržnější	soudržný	k2eAgNnSc1d2	soudržnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
skoky	skok	k1gInPc7	skok
v	v	k7c6	v
tvrdosti	tvrdost	k1gFnSc6	tvrdost
či	či	k8xC	či
s	s	k7c7	s
měkkým	měkký	k2eAgInSc7d1	měkký
sněhem	sníh	k1gInSc7	sníh
uprostřed	uprostřed	k6eAd1	uprostřed
či	či	k8xC	či
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
profilu	profil	k1gInSc2	profil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formy	forma	k1gFnPc1	forma
sněžení	sněžení	k1gNnSc2	sněžení
==	==	k?	==
</s>
</p>
<p>
<s>
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
přeháňka	přeháňka	k1gFnSc1	přeháňka
</s>
</p>
<p>
<s>
Srážka	srážka	k1gFnSc1	srážka
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
většinou	většinou	k6eAd1	většinou
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
hvězdice	hvězdice	k1gFnPc4	hvězdice
(	(	kIx(	(
<g/>
sněhové	sněhový	k2eAgFnPc4d1	sněhová
vločky	vločka	k1gFnPc4	vločka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
přeháňka	přeháňka	k1gFnSc1	přeháňka
se	se	k3xPyFc4	se
od	od	k7c2	od
sněžení	sněžení	k1gNnSc2	sněžení
liší	lišit	k5eAaImIp3nP	lišit
rychlostí	rychlost	k1gFnSc7	rychlost
kolísání	kolísání	k1gNnSc2	kolísání
intenzity	intenzita	k1gFnSc2	intenzita
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
náhlostí	náhlost	k1gFnSc7	náhlost
výskytu	výskyt	k1gInSc2	výskyt
<g/>
,	,	kIx,	,
rychlostí	rychlost	k1gFnSc7	rychlost
střídání	střídání	k1gNnSc2	střídání
oblačnosti	oblačnost	k1gFnSc2	oblačnost
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
malým	malý	k2eAgInSc7d1	malý
územním	územní	k2eAgInSc7d1	územní
rozsahem	rozsah	k1gInSc7	rozsah
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
přeháňky	přeháňka	k1gFnSc2	přeháňka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sněhové	sněhový	k2eAgFnSc6d1	sněhová
přeháňce	přeháňka	k1gFnSc6	přeháňka
bývá	bývat	k5eAaImIp3nS	bývat
značný	značný	k2eAgInSc1d1	značný
pokles	pokles	k1gInSc1	pokles
dohlednosti	dohlednost	k1gFnSc2	dohlednost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
přeháňku	přeháňka	k1gFnSc4	přeháňka
bývá	bývat	k5eAaImIp3nS	bývat
dohlednost	dohlednost	k1gFnSc1	dohlednost
dobrá	dobrý	k2eAgFnSc1d1	dobrá
<g/>
.	.	kIx.	.
<g/>
Déšť	déšť	k1gInSc1	déšť
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
</s>
</p>
<p>
<s>
Srážka	srážka	k1gFnSc1	srážka
složená	složený	k2eAgFnSc1d1	složená
současně	současně	k6eAd1	současně
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
kapek	kapka	k1gFnPc2	kapka
a	a	k8xC	a
sněhových	sněhový	k2eAgFnPc2d1	sněhová
vloček	vločka	k1gFnPc2	vločka
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
déšť	déšť	k1gInSc1	déšť
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
<g/>
Sněhové	sněhový	k2eAgFnPc4d1	sněhová
krupky	krupka	k1gFnPc4	krupka
</s>
</p>
<p>
<s>
Srážka	srážka	k1gFnSc1	srážka
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bílých	bílý	k2eAgFnPc2d1	bílá
neprůsvitných	průsvitný	k2eNgFnPc2d1	neprůsvitná
ledových	ledový	k2eAgFnPc2d1	ledová
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zrna	zrno	k1gNnPc1	zrno
jsou	být	k5eAaImIp3nP	být
kulovitá	kulovitý	k2eAgNnPc1d1	kulovité
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
kuželovitá	kuželovitý	k2eAgFnSc1d1	kuželovitá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
průměr	průměr	k1gInSc4	průměr
bývá	bývat	k5eAaImIp3nS	bývat
2	[number]	k4	2
až	až	k9	až
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Sněhové	sněhový	k2eAgFnPc1d1	sněhová
krupky	krupka	k1gFnPc1	krupka
jsou	být	k5eAaImIp3nP	být
křehké	křehký	k2eAgFnPc1d1	křehká
<g/>
,	,	kIx,	,
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
stlačovat	stlačovat	k5eAaImF	stlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
tvrdou	tvrdý	k2eAgFnSc4d1	tvrdá
plochu	plocha	k1gFnSc4	plocha
odskakují	odskakovat	k5eAaImIp3nP	odskakovat
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
tříští	tříštit	k5eAaImIp3nS	tříštit
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
vyskytuji	vyskytovat	k5eAaImIp1nS	vyskytovat
v	v	k7c6	v
přeháňkách	přeháňka	k1gFnPc6	přeháňka
<g/>
.	.	kIx.	.
<g/>
Sněhová	sněhový	k2eAgNnPc4d1	sněhové
zrna	zrno	k1gNnPc4	zrno
</s>
</p>
<p>
<s>
Srážka	srážka	k1gFnSc1	srážka
skládající	skládající	k2eAgFnSc1d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgNnPc2d1	malé
a	a	k8xC	a
neprůsvitných	průsvitný	k2eNgNnPc2d1	neprůsvitné
ledových	ledový	k2eAgNnPc2d1	ledové
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zrna	zrno	k1gNnPc1	zrno
jsou	být	k5eAaImIp3nP	být
zploštělá	zploštělý	k2eAgNnPc1d1	zploštělé
nebo	nebo	k8xC	nebo
podlouhlá	podlouhlý	k2eAgNnPc1d1	podlouhlé
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc4	jejich
průměr	průměr	k1gInSc1	průměr
bývá	bývat	k5eAaImIp3nS	bývat
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
1	[number]	k4	1
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
se	se	k3xPyFc4	se
netříští	tříštit	k5eNaImIp3nP	tříštit
a	a	k8xC	a
neodskakují	odskakovat	k5eNaImIp3nP	odskakovat
<g/>
.	.	kIx.	.
<g/>
Zmrzlý	zmrzlý	k2eAgInSc4d1	zmrzlý
déšť	déšť	k1gInSc4	déšť
</s>
</p>
<p>
<s>
Srážka	srážka	k1gFnSc1	srážka
je	být	k5eAaImIp3nS	být
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
průhledných	průhledný	k2eAgFnPc2d1	průhledná
ledových	ledový	k2eAgFnPc2d1	ledová
zrn	zrno	k1gNnPc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
zmrznutím	zmrznutí	k1gNnSc7	zmrznutí
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
nebo	nebo	k8xC	nebo
sněhových	sněhový	k2eAgFnPc2d1	sněhová
vloček	vločka	k1gFnPc2	vločka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
pádu	pád	k1gInSc2	pád
téměř	téměř	k6eAd1	téměř
roztály	roztát	k5eAaPmAgFnP	roztát
a	a	k8xC	a
opět	opět	k6eAd1	opět
zmrzly	zmrznout	k5eAaPmAgFnP	zmrznout
<g/>
.	.	kIx.	.
</s>
<s>
Ledová	ledový	k2eAgNnPc4d1	ledové
zrna	zrno	k1gNnPc4	zrno
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
deště	dešť	k1gInSc2	dešť
mají	mít	k5eAaImIp3nP	mít
zpravidla	zpravidla	k6eAd1	zpravidla
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
nebo	nebo	k8xC	nebo
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
i	i	k9	i
kuželovitý	kuželovitý	k2eAgInSc4d1	kuželovitý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
<g/>
Námrazové	námrazový	k2eAgFnSc2d1	námrazová
krupky	krupka	k1gFnSc2	krupka
</s>
</p>
<p>
<s>
Srážka	srážka	k1gFnSc1	srážka
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
průsvitných	průsvitný	k2eAgNnPc2d1	průsvitné
ledových	ledový	k2eAgNnPc2d1	ledové
zrn	zrno	k1gNnPc2	zrno
převážně	převážně	k6eAd1	převážně
kulovitého	kulovitý	k2eAgInSc2d1	kulovitý
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
též	též	k9	též
kuželovitého	kuželovitý	k2eAgInSc2d1	kuželovitý
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
kolem	kolem	k7c2	kolem
5	[number]	k4	5
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
sněhová	sněhový	k2eAgNnPc4d1	sněhové
zrnka	zrnko	k1gNnPc4	zrnko
obalená	obalený	k2eAgFnSc1d1	obalená
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvou	vrstva	k1gFnSc7	vrstva
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Námrazové	námrazový	k2eAgFnPc1d1	námrazová
krupky	krupka	k1gFnPc1	krupka
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
výhradně	výhradně	k6eAd1	výhradně
v	v	k7c6	v
přeháňkách	přeháňka	k1gFnPc6	přeháňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
společnost	společnost	k1gFnSc4	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
je	být	k5eAaImIp3nS	být
sníh	sníh	k1gInSc4	sníh
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
součástí	součást	k1gFnSc7	součást
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
obydlí	obydlí	k1gNnSc2	obydlí
–	–	k?	–
iglú	iglú	k1gNnSc2	iglú
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
sněhové	sněhový	k2eAgFnSc6d1	sněhová
pokrývce	pokrývka	k1gFnSc6	pokrývka
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožní	umožnit	k5eAaPmIp3nP	umožnit
rozložení	rozložení	k1gNnSc4	rozložení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
osoby	osoba	k1gFnSc2	osoba
nebo	nebo	k8xC	nebo
vozidla	vozidlo	k1gNnSc2	vozidlo
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
plochu	plocha	k1gFnSc4	plocha
–	–	k?	–
lyže	lyže	k1gFnPc1	lyže
<g/>
,	,	kIx,	,
sněžnice	sněžnice	k1gFnPc1	sněžnice
<g/>
,	,	kIx,	,
saně	saně	k1gFnPc1	saně
<g/>
,	,	kIx,	,
sněžné	sněžný	k2eAgFnPc1d1	sněžná
rolby	rolba	k1gFnPc1	rolba
<g/>
,	,	kIx,	,
sněžné	sněžný	k2eAgInPc1d1	sněžný
skútry	skútr	k1gInPc1	skútr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rekreace	rekreace	k1gFnSc1	rekreace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
je	být	k5eAaImIp3nS	být
sníh	sníh	k1gInSc4	sníh
typickým	typický	k2eAgInSc7d1	typický
atributem	atribut	k1gInSc7	atribut
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
snadno	snadno	k6eAd1	snadno
dostupný	dostupný	k2eAgInSc1d1	dostupný
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
zpracovatelný	zpracovatelný	k2eAgInSc4d1	zpracovatelný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
lehce	lehko	k6eAd1	lehko
použitelný	použitelný	k2eAgMnSc1d1	použitelný
k	k	k7c3	k
zábavě	zábava	k1gFnSc3	zábava
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
koulovat	koulovat	k5eAaImF	koulovat
<g/>
,	,	kIx,	,
stavět	stavět	k5eAaImF	stavět
sněhuláky	sněhulák	k1gMnPc4	sněhulák
<g/>
,	,	kIx,	,
bunkry	bunkr	k1gInPc1	bunkr
<g/>
.	.	kIx.	.
</s>
<s>
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
vhodně	vhodně	k6eAd1	vhodně
zvolených	zvolený	k2eAgInPc6d1	zvolený
prostředcích	prostředek	k1gInPc6	prostředek
usnadnit	usnadnit	k5eAaPmF	usnadnit
pohyb	pohyb	k1gInSc4	pohyb
i	i	k9	i
po	po	k7c6	po
jinak	jinak	k6eAd1	jinak
nepřístupném	přístupný	k2eNgInSc6d1	nepřístupný
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
nejčastěji	často	k6eAd3	často
lyžování	lyžování	k1gNnSc1	lyžování
<g/>
.	.	kIx.	.
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1	výzbroj
potřebná	potřebný	k2eAgFnSc1d1	potřebná
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
provozování	provozování	k1gNnSc3	provozování
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
jako	jako	k9	jako
pro	pro	k7c4	pro
pohyb	pohyb	k1gInSc4	pohyb
v	v	k7c6	v
trvale	trvale	k6eAd1	trvale
zasněžených	zasněžený	k2eAgFnPc6d1	zasněžená
oblastech	oblast	k1gFnPc6	oblast
-	-	kIx~	-
lyže	lyže	k1gFnSc1	lyže
<g/>
,	,	kIx,	,
snowboard	snowboard	k1gInSc1	snowboard
<g/>
,	,	kIx,	,
saně	saně	k1gFnPc1	saně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
===	===	k?	===
</s>
</p>
<p>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
také	také	k9	také
obávané	obávaný	k2eAgNnSc1d1	obávané
riziko	riziko	k1gNnSc1	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Nejničivější	ničivý	k2eAgFnSc1d3	nejničivější
jevem	jev	k1gInSc7	jev
je	být	k5eAaImIp3nS	být
náhlý	náhlý	k2eAgInSc1d1	náhlý
sesuv	sesuv	k1gInSc1	sesuv
sněhové	sněhový	k2eAgFnSc2d1	sněhová
masy	masa	k1gFnSc2	masa
ze	z	k7c2	z
svahu	svah	k1gInSc2	svah
–	–	k?	–
sněhová	sněhový	k2eAgFnSc1d1	sněhová
lavina	lavina	k1gFnSc1	lavina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
hrozí	hrozit	k5eAaImIp3nS	hrozit
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
napadne	napadnout	k5eAaPmIp3nS	napadnout
<g/>
-li	i	k?	-li
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
sníh	sníh	k1gInSc1	sníh
na	na	k7c4	na
již	již	k6eAd1	již
umrzlou	umrzlý	k2eAgFnSc4d1	umrzlá
sněhovou	sněhový	k2eAgFnSc4d1	sněhová
pokrývku	pokrývka	k1gFnSc4	pokrývka
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
pevně	pevně	k6eAd1	pevně
nespojí	spojit	k5eNaPmIp3nS	spojit
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
sklouznout	sklouznout	k5eAaPmF	sklouznout
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jev	jev	k1gInSc1	jev
na	na	k7c6	na
šikmých	šikmý	k2eAgFnPc6d1	šikmá
střechách	střecha	k1gFnPc6	střecha
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgNnSc4	jenž
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
umisťují	umisťovat	k5eAaImIp3nP	umisťovat
sněhové	sněhový	k2eAgFnPc1d1	sněhová
zábrany	zábrana	k1gFnPc1	zábrana
nebo	nebo	k8xC	nebo
sněhové	sněhový	k2eAgInPc1d1	sněhový
háky	hák	k1gInPc1	hák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránily	zabránit	k5eAaPmAgFnP	zabránit
náhlému	náhlý	k2eAgInSc3d1	náhlý
sesuvu	sesuv	k1gInSc3	sesuv
sněhové	sněhový	k2eAgFnSc2d1	sněhová
masy	masa	k1gFnSc2	masa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
budovy	budova	k1gFnPc4	budova
s	s	k7c7	s
plochou	plochý	k2eAgFnSc7d1	plochá
střechou	střecha	k1gFnSc7	střecha
představuje	představovat	k5eAaImIp3nS	představovat
sněhová	sněhový	k2eAgFnSc1d1	sněhová
pokrývka	pokrývka	k1gFnSc1	pokrývka
zase	zase	k9	zase
riziko	riziko	k1gNnSc4	riziko
přetížení	přetížení	k1gNnSc2	přetížení
střešní	střešní	k2eAgFnSc2d1	střešní
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sníh	sníh	k1gInSc1	sníh
také	také	k9	také
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
terénu	terén	k1gInSc6	terén
hrozí	hrozit	k5eAaImIp3nS	hrozit
v	v	k7c6	v
hlubokém	hluboký	k2eAgInSc6d1	hluboký
sněhu	sníh	k1gInSc6	sníh
přecenění	přecenění	k1gNnSc2	přecenění
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
možné	možný	k2eAgNnSc1d1	možné
umrznutí	umrznutí	k1gNnSc1	umrznutí
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
je	být	k5eAaImIp3nS	být
i	i	k9	i
situace	situace	k1gFnPc4	situace
silničních	silniční	k2eAgNnPc2d1	silniční
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
sněhové	sněhový	k2eAgFnSc6d1	sněhová
pokrývce	pokrývka	k1gFnSc6	pokrývka
stávají	stávat	k5eAaImIp3nP	stávat
hůře	zle	k6eAd2	zle
ovladatelná	ovladatelný	k2eAgNnPc1d1	ovladatelné
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
třeba	třeba	k6eAd1	třeba
je	být	k5eAaImIp3nS	být
doplnit	doplnit	k5eAaPmF	doplnit
zimní	zimní	k2eAgFnSc7d1	zimní
výbavou	výbava	k1gFnSc7	výbava
–	–	k?	–
zimními	zimní	k2eAgFnPc7d1	zimní
pneumatikami	pneumatika	k1gFnPc7	pneumatika
nebo	nebo	k8xC	nebo
sněhovými	sněhový	k2eAgInPc7d1	sněhový
řetězy	řetěz	k1gInPc7	řetěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sníh	sníh	k1gInSc1	sníh
je	být	k5eAaImIp3nS	být
zrádný	zrádný	k2eAgInSc1d1	zrádný
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oproti	oproti	k7c3	oproti
jiným	jiná	k1gFnPc3	jiná
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
vyskytujícím	vyskytující	k2eAgFnPc3d1	vyskytující
plochám	plocha	k1gFnPc3	plocha
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odráží	odrážet	k5eAaImIp3nS	odrážet
ultrafialovou	ultrafialový	k2eAgFnSc4d1	ultrafialová
složku	složka	k1gFnSc4	složka
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
i	i	k9	i
pod	pod	k7c7	pod
velkými	velký	k2eAgInPc7d1	velký
úhly	úhel	k1gInPc7	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgFnSc1d2	delší
expozice	expozice	k1gFnSc1	expozice
v	v	k7c6	v
zasněženém	zasněžený	k2eAgInSc6d1	zasněžený
terénu	terén	k1gInSc6	terén
bez	bez	k7c2	bez
ochranných	ochranný	k2eAgFnPc2d1	ochranná
brýlí	brýle	k1gFnPc2	brýle
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
očí	oko	k1gNnPc2	oko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sněžná	sněžný	k2eAgFnSc1d1	sněžná
slepota	slepota	k1gFnSc1	slepota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mytologie	mytologie	k1gFnPc4	mytologie
a	a	k8xC	a
pohádky	pohádka	k1gFnPc4	pohádka
===	===	k?	===
</s>
</p>
<p>
<s>
Četnost	četnost	k1gFnSc1	četnost
výskytu	výskyt	k1gInSc2	výskyt
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
postavení	postavení	k1gNnSc2	postavení
sněhu	sníh	k1gInSc2	sníh
v	v	k7c6	v
místně	místně	k6eAd1	místně
tradovaných	tradovaný	k2eAgInPc6d1	tradovaný
příbězích	příběh	k1gInPc6	příběh
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
severských	severský	k2eAgInPc2d1	severský
národů	národ	k1gInPc2	národ
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
postavy	postava	k1gFnPc1	postava
jako	jako	k8xS	jako
Sněhová	sněhový	k2eAgFnSc1d1	sněhová
královna	královna	k1gFnSc1	královna
nebo	nebo	k8xC	nebo
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc4	my
je	být	k5eAaImIp3nS	být
sníh	sníh	k1gInSc1	sníh
spíše	spíše	k9	spíše
příznakem	příznak	k1gInSc7	příznak
dočasné	dočasný	k2eAgFnSc2d1	dočasná
nehostinnosti	nehostinnost	k1gFnSc2	nehostinnost
krajiny	krajina	k1gFnSc2	krajina
(	(	kIx(	(
<g/>
pohádka	pohádka	k1gFnSc1	pohádka
O	o	k7c6	o
dvanácti	dvanáct	k4xCc6	dvanáct
měsíčkách	měsíček	k1gInPc6	měsíček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
národů	národ	k1gInPc2	národ
je	být	k5eAaImIp3nS	být
opakujícím	opakující	k2eAgMnSc7d1	opakující
se	se	k3xPyFc4	se
motivem	motiv	k1gInSc7	motiv
mouka	mouka	k1gFnSc1	mouka
padající	padající	k2eAgFnSc1d1	padající
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sněhové	sněhový	k2eAgInPc4d1	sněhový
rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
sněžení	sněžení	k1gNnSc1	sněžení
-	-	kIx~	-
Celkem	celkem	k6eAd1	celkem
31	[number]	k4	31
102	[number]	k4	102
mm	mm	kA	mm
sněhu	sníh	k1gInSc2	sníh
napadlo	napadnout	k5eAaPmAgNnS	napadnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
Paradise	Paradis	k1gInSc6	Paradis
Mount	Mount	k1gMnSc1	Mount
Rainier	Rainier	k1gMnSc1	Rainier
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
hloubka	hloubka	k1gFnSc1	hloubka
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
11,46	[number]	k4	11,46
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
v	v	k7c6	v
Tamaraku	Tamarak	k1gInSc6	Tamarak
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
škod	škoda	k1gFnPc2	škoda
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
sněhové	sněhový	k2eAgFnSc2d1	sněhová
bouře	bouř	k1gFnSc2	bouř
-	-	kIx~	-
celkem	celkem	k6eAd1	celkem
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
při	při	k7c6	při
sněhové	sněhový	k2eAgFnSc6d1	sněhová
bouři	bouř	k1gFnSc6	bouř
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	s	k7c7	s
12	[number]	k4	12
<g/>
.	.	kIx.	.
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1993	[number]	k4	1993
přehnala	přehnat	k5eAaPmAgFnS	přehnat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Bouře	bouře	k1gFnSc1	bouře
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
meteorologů	meteorolog	k1gMnPc2	meteorolog
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
vánice	vánice	k1gFnSc1	vánice
se	s	k7c7	s
srdcem	srdce	k1gNnSc7	srdce
sněhové	sněhový	k2eAgFnSc2d1	sněhová
bouře	bouř	k1gFnSc2	bouř
a	a	k8xC	a
duší	duše	k1gFnSc7	duše
uragánu	uragán	k1gInSc2	uragán
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
způsobila	způsobit	k5eAaPmAgFnS	způsobit
škody	škoda	k1gFnPc4	škoda
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
1,2	[number]	k4	1,2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sníh	sníh	k1gInSc1	sníh
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
sníh	sníh	k1gInSc1	sníh
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Sníh	sníh	k1gInSc1	sníh
–	–	k?	–
sníh	sníh	k1gInSc1	sníh
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
a	a	k8xC	a
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgNnSc4d1	aktuální
meteo	meteo	k1gNnSc4	meteo
a	a	k8xC	a
články	článek	k1gInPc4	článek
</s>
</p>
