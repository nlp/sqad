<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
byli	být	k5eAaImAgMnP	být
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
knížecí	knížecí	k2eAgFnSc1d1	knížecí
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
dynastie	dynastie	k1gFnSc1	dynastie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
odnepaměti	odnepaměti	k6eAd1	odnepaměti
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vládli	vládnout	k5eAaImAgMnP	vládnout
také	také	k9	také
v	v	k7c6	v
Rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1251	[number]	k4	1251
<g/>
–	–	k?	–
<g/>
1278	[number]	k4	1278
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
a	a	k8xC	a
Uhersku	Uhersko	k1gNnSc6	Uhersko
(	(	kIx(	(
<g/>
1301	[number]	k4	1301
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
(	(	kIx(	(
<g/>
levobočné	levobočný	k2eAgFnSc6d1	levobočný
<g/>
)	)	kIx)	)
opavské	opavský	k2eAgFnSc6d1	Opavská
linii	linie	k1gFnSc6	linie
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
vymřeli	vymřít	k5eAaPmAgMnP	vymřít
(	(	kIx(	(
<g/>
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
)	)	kIx)	)
až	až	k8xS	až
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
byli	být	k5eAaImAgMnP	být
jedinou	jediný	k2eAgFnSc4d1	jediná
původem	původ	k1gInSc7	původ
českou	český	k2eAgFnSc7d1	Česká
panovnickou	panovnický	k2eAgFnSc7d1	panovnická
dynastií	dynastie	k1gFnSc7	dynastie
<g/>
;	;	kIx,	;
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
vymření	vymření	k1gNnSc6	vymření
po	po	k7c6	po
meči	meč	k1gInSc6	meč
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
jen	jen	k9	jen
panovníci	panovník	k1gMnPc1	panovník
původem	původ	k1gInSc7	původ
z	z	k7c2	z
cizích	cizí	k2eAgInPc2d1	cizí
rodů	rod	k1gInPc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
zachycené	zachycený	k2eAgFnSc2d1	zachycená
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
11	[number]	k4	11
<g/>
.	.	kIx.	.
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
Kronice	kronika	k1gFnSc6	kronika
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
Přemysl	Přemysl	k1gMnSc1	Přemysl
Oráč	oráč	k1gMnSc1	oráč
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnSc1d1	pocházející
ze	z	k7c2	z
Stadic	Stadice	k1gInPc2	Stadice
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
si	se	k3xPyFc3	se
vyvolila	vyvolit	k5eAaPmAgFnS	vyvolit
za	za	k7c4	za
manžela	manžel	k1gMnSc4	manžel
Krokova	Krokov	k1gInSc2	Krokov
dcera	dcera	k1gFnSc1	dcera
Libuše	Libuše	k1gFnSc1	Libuše
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ale	ale	k8xC	ale
Stadice	Stadice	k1gInPc1	Stadice
ještě	ještě	k6eAd1	ještě
neexistovaly	existovat	k5eNaImAgInP	existovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc4	jejich
následníky	následník	k1gMnPc4	následník
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
dle	dle	k7c2	dle
pověstí	pověst	k1gFnPc2	pověst
Nezamysl	Nezamysl	k1gMnSc1	Nezamysl
<g/>
,	,	kIx,	,
Mnata	Mnata	k1gMnSc1	Mnata
<g/>
,	,	kIx,	,
Vojen	vojna	k1gFnPc2	vojna
<g/>
,	,	kIx,	,
Vnislav	Vnislav	k1gMnSc1	Vnislav
<g/>
,	,	kIx,	,
Křesomysl	Křesomysl	k1gMnSc1	Křesomysl
<g/>
,	,	kIx,	,
Neklan	Neklan	k1gMnSc1	Neklan
a	a	k8xC	a
Hostivít	Hostivít	k1gMnSc1	Hostivít
<g/>
.	.	kIx.	.
</s>
<s>
Hostivítovým	Hostivítův	k2eAgMnSc7d1	Hostivítův
synem	syn	k1gMnSc7	syn
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
první	první	k4xOgMnSc1	první
historicky	historicky	k6eAd1	historicky
doložený	doložený	k2eAgMnSc1d1	doložený
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
I.	I.	kA	I.
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
původu	původ	k1gInSc6	původ
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
původu	původ	k1gInSc3	původ
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
také	také	k9	také
tzv.	tzv.	kA	tzv.
Kristiánova	Kristiánův	k2eAgFnSc1d1	Kristiánova
legenda	legenda	k1gFnSc1	legenda
z	z	k7c2	z
konce	konec	k1gInSc2	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
legendy	legenda	k1gFnSc2	legenda
Diffundente	Diffundent	k1gMnSc5	Diffundent
sole	sol	k1gInSc5	sol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
těchto	tento	k3xDgInPc6	tento
starších	starý	k2eAgInPc6d2	starší
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
Přemyslu	Přemysl	k1gMnSc6	Přemysl
Oráči	oráč	k1gMnSc6	oráč
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc4	příběh
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
jako	jako	k9	jako
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
"	"	kIx"	"
<g/>
literárnějším	literární	k2eAgNnSc6d2	literárnější
<g/>
"	"	kIx"	"
podání	podání	k1gNnSc6	podání
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
teorie	teorie	k1gFnPc1	teorie
původu	původ	k1gInSc2	původ
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc4	jejich
moravský	moravský	k2eAgInSc4d1	moravský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
historika	historik	k1gMnSc2	historik
Lubomíra	Lubomír	k1gMnSc2	Lubomír
E.	E.	kA	E.
Havlíka	Havlík	k1gMnSc2	Havlík
<g/>
,	,	kIx,	,
podporované	podporovaný	k2eAgFnPc4d1	podporovaná
J.	J.	kA	J.
Zástěrou	zástěra	k1gFnSc7	zástěra
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
L.	L.	kA	L.
Galuškou	Galuška	k1gMnSc7	Galuška
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
kníže	kníže	k1gMnSc1	kníže
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
synem	syn	k1gMnSc7	syn
moravského	moravský	k2eAgNnSc2d1	Moravské
knížete	kníže	k1gNnSc2wR	kníže
Rostislava	Rostislava	k1gFnSc1	Rostislava
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tamní	tamní	k2eAgFnSc1d1	tamní
dynastie	dynastie	k1gFnSc1	dynastie
(	(	kIx(	(
<g/>
Mojmírovci	Mojmírovec	k1gMnPc1	Mojmírovec
<g/>
)	)	kIx)	)
mohla	moct	k5eAaImAgFnS	moct
pocházet	pocházet	k5eAaImF	pocházet
až	až	k9	až
od	od	k7c2	od
prvního	první	k4xOgMnSc2	první
slovanského	slovanský	k2eAgMnSc2d1	slovanský
vladaře	vladař	k1gMnSc2	vladař
Sáma	Sámo	k1gMnSc2	Sámo
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
ztotožňován	ztotožňován	k2eAgInSc4d1	ztotožňován
někdy	někdy	k6eAd1	někdy
také	také	k9	také
s	s	k7c7	s
Přemyslem	Přemysl	k1gMnSc7	Přemysl
jako	jako	k8xC	jako
zakladatelem	zakladatel	k1gMnSc7	zakladatel
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
další	další	k2eAgFnSc2d1	další
teorie	teorie	k1gFnSc2	teorie
muzikologa	muzikolog	k1gMnSc2	muzikolog
V.	V.	kA	V.
Karbusického	Karbusický	k2eAgInSc2d1	Karbusický
pocházejí	pocházet	k5eAaImIp3nP	pocházet
jména	jméno	k1gNnPc4	jméno
dvanácti	dvanáct	k4xCc2	dvanáct
bájných	bájný	k2eAgMnPc2d1	bájný
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
potomků	potomek	k1gMnPc2	potomek
Přemysla	Přemysl	k1gMnSc2	Přemysl
a	a	k8xC	a
Libuše	Libuše	k1gFnSc2	Libuše
<g/>
,	,	kIx,	,
z	z	k7c2	z
úryvku	úryvek	k1gInSc2	úryvek
staroslověnského	staroslověnský	k2eAgInSc2d1	staroslověnský
(	(	kIx(	(
<g/>
či	či	k8xC	či
staročeského	staročeský	k2eAgMnSc2d1	staročeský
<g/>
)	)	kIx)	)
textu-poselství	textuoselství	k1gNnSc2	textu-poselství
Čechů	Čech	k1gMnPc2	Čech
k	k	k7c3	k
Frankům	Frank	k1gMnPc3	Frank
v	v	k7c4	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
textu	text	k1gInSc6	text
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
psáno	psát	k5eAaImNgNnS	psát
<g/>
:	:	kIx,	:
Krok	krok	k1gInSc1	krok
<g/>
'	'	kIx"	'
kazi	kazi	k1gNnSc1	kazi
(	(	kIx(	(
<g/>
Tetha	Tetha	k1gFnSc1	Tetha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lubo	lubo	k1gMnSc1	lubo
premyšl	premyšl	k1gMnSc1	premyšl
<g/>
,	,	kIx,	,
nezamyšl	nezamyšl	k1gMnSc1	nezamyšl
m	m	kA	m
<g/>
'	'	kIx"	'
<g/>
nata	nat	k2eAgFnSc1d1	nata
voj	voj	k1gFnSc1	voj
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
u	u	k7c2	u
<g/>
'	'	kIx"	'
<g/>
ni	on	k3xPp3gFnSc4	on
zla	zlo	k1gNnPc1	zlo
<g/>
,	,	kIx,	,
kr	kr	k?	kr
<g/>
'	'	kIx"	'
<g/>
z	z	k7c2	z
my	my	k3xPp1nPc1	my
s	s	k7c7	s
<g/>
<g />
.	.	kIx.	.
</s>
<s>
'	'	kIx"	'
neklan	neklana	k1gFnPc2	neklana
(	(	kIx(	(
<g/>
am	am	k?	am
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gosti	gost	k5eAaBmF	gost
vit	vít	k5eAaImNgMnS	vít
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vykládá	vykládat	k5eAaImIp3nS	vykládat
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
Zastav	zastavit	k5eAaPmRp2nS	zastavit
své	svůj	k3xOyFgInPc4	svůj
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
Tetha	Tetha	k1gFnSc1	Tetha
(	(	kIx(	(
<g/>
oslovení	oslovení	k1gNnSc1	oslovení
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
vůdce	vůdce	k1gMnSc2	vůdce
Franků	Franky	k1gInPc2	Franky
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
západních	západní	k2eAgMnPc2d1	západní
sousedů	soused	k1gMnPc2	soused
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
a	a	k8xC	a
raději	rád	k6eAd2	rád
přemýšlej	přemýšlet	k5eAaImRp2nS	přemýšlet
<g/>
,	,	kIx,	,
nezamýšlím	zamýšlet	k5eNaImIp1nS	zamýšlet
na	na	k7c4	na
tebe	ty	k3xPp2nSc4	ty
vojnu	vojna	k1gFnSc4	vojna
ani	ani	k8xC	ani
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
kříži	kříž	k1gInSc6	kříž
my	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
neklaníme	klanit	k5eNaImIp1nP	klanit
<g/>
,	,	kIx,	,
hosty	host	k1gMnPc7	host
vítáme	vítat	k5eAaImIp1nP	vítat
<g/>
.	.	kIx.	.
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Velkomoravský	velkomoravský	k2eAgMnSc1d1	velkomoravský
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
svého	své	k1gNnSc2	své
zhruba	zhruba	k6eAd1	zhruba
patnáctiletého	patnáctiletý	k2eAgMnSc2d1	patnáctiletý
chráněnce	chráněnec	k1gMnSc2	chráněnec
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
867	[number]	k4	867
knížetem	kníže	k1gNnSc7wR	kníže
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
historicky	historicky	k6eAd1	historicky
doložený	doložený	k2eAgMnSc1d1	doložený
Přemyslovec	Přemyslovec	k1gMnSc1	Přemyslovec
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
tedy	tedy	k9	tedy
vládli	vládnout	k5eAaImAgMnP	vládnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
430	[number]	k4	430
let	léto	k1gNnPc2	léto
–	–	k?	–
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1306	[number]	k4	1306
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
30	[number]	k4	30
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
sedm	sedm	k4xCc1	sedm
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
svedli	svést	k5eAaPmAgMnP	svést
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
boj	boj	k1gInSc4	boj
o	o	k7c4	o
vrchní	vrchní	k2eAgFnSc4d1	vrchní
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Čechami	Čechy	k1gFnPc7	Čechy
s	s	k7c7	s
mocným	mocný	k2eAgInSc7d1	mocný
rodem	rod	k1gInSc7	rod
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
vyvražděním	vyvraždění	k1gNnSc7	vyvraždění
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
na	na	k7c6	na
Libici	Libice	k1gFnSc6	Libice
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
995	[number]	k4	995
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
vztahů	vztah	k1gInPc2	vztah
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
rodů	rod	k1gInPc2	rod
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
diskutovaná	diskutovaný	k2eAgFnSc1d1	diskutovaná
<g/>
,	,	kIx,	,
Slavníkovci	Slavníkovec	k1gMnPc1	Slavníkovec
možná	možná	k9	možná
pro	pro	k7c4	pro
Přemyslovce	Přemyslovec	k1gMnPc4	Přemyslovec
nepředstavovali	představovat	k5eNaImAgMnP	představovat
mocenskou	mocenský	k2eAgFnSc4d1	mocenská
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
alternativní	alternativní	k2eAgFnPc1d1	alternativní
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slavníkovci	Slavníkovec	k1gMnPc1	Slavníkovec
byli	být	k5eAaImAgMnP	být
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
větví	větev	k1gFnPc2	větev
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
mocenský	mocenský	k2eAgInSc4d1	mocenský
boj	boj	k1gInSc4	boj
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěších	úspěch	k1gInPc6	úspěch
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
a	a	k8xC	a
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
nastává	nastávat	k5eAaImIp3nS	nastávat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
3	[number]	k4	3
synů	syn	k1gMnPc2	syn
knížete	kníže	k1gMnSc4	kníže
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velký	velký	k2eAgInSc4d1	velký
úpadek	úpadek	k1gInSc4	úpadek
českého	český	k2eAgNnSc2d1	české
knížectví	knížectví	k1gNnSc2	knížectví
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
bratrovražednými	bratrovražedný	k2eAgInPc7d1	bratrovražedný
boji	boj	k1gInPc7	boj
mezi	mezi	k7c7	mezi
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
a	a	k8xC	a
vzestupem	vzestup	k1gInSc7	vzestup
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
–	–	k?	–
především	především	k9	především
pak	pak	k6eAd1	pak
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
krize	krize	k1gFnSc1	krize
je	být	k5eAaImIp3nS	být
překonána	překonat	k5eAaPmNgFnS	překonat
až	až	k6eAd1	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Břetislava	Břetislav	k1gMnSc2	Břetislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
poučen	poučen	k2eAgInSc4d1	poučen
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
zavádí	zavádět	k5eAaImIp3nS	zavádět
seniorátní	seniorátní	k2eAgInSc1d1	seniorátní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
dědictví	dědictví	k1gNnSc2	dědictví
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
člena	člen	k1gMnSc4	člen
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
tím	ten	k3xDgNnSc7	ten
vládu	vláda	k1gFnSc4	vláda
svým	svůj	k3xOyFgInSc7	svůj
5	[number]	k4	5
synům	syn	k1gMnPc3	syn
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc6	jenž
nejstarší	starý	k2eAgMnSc1d3	nejstarší
Spytihněv	Spytihněv	k1gMnSc1	Spytihněv
II	II	kA	II
<g/>
.	.	kIx.	.
vládne	vládnout	k5eAaImIp3nS	vládnout
jako	jako	k9	jako
kníže	kníže	k1gMnSc1	kníže
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
3	[number]	k4	3
mladší	mladý	k2eAgMnPc1d2	mladší
bratři	bratr	k1gMnPc1	bratr
obdrží	obdržet	k5eAaPmIp3nP	obdržet
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
moravskými	moravský	k2eAgInPc7d1	moravský
úděly	úděl	k1gInPc7	úděl
–	–	k?	–
olomouckým	olomoucký	k2eAgMnPc3d1	olomoucký
<g/>
,	,	kIx,	,
brněnským	brněnský	k2eAgMnPc3d1	brněnský
a	a	k8xC	a
znojemským	znojemský	k2eAgNnSc7d1	Znojemské
–	–	k?	–
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
syn	syn	k1gMnSc1	syn
Jaromír	Jaromír	k1gMnSc1	Jaromír
(	(	kIx(	(
<g/>
Gebhart	Gebhart	k1gInSc1	Gebhart
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
církevní	církevní	k2eAgFnSc3d1	církevní
dráze	dráha	k1gFnSc3	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
synů	syn	k1gMnPc2	syn
Břetislava	Břetislav	k1gMnSc4	Břetislav
I.	I.	kA	I.
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
také	také	k9	také
moravští	moravský	k2eAgMnPc1d1	moravský
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vymírají	vymírat	k5eAaImIp3nP	vymírat
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
eliminovat	eliminovat	k5eAaBmF	eliminovat
moc	moc	k1gFnSc4	moc
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
větví	větev	k1gFnPc2	větev
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nP	soustředit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
<g/>
,	,	kIx,	,
pražské	pražský	k2eAgFnSc2d1	Pražská
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1085	[number]	k4	1085
získal	získat	k5eAaPmAgMnS	získat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
z	z	k7c2	z
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
Vratislav	Vratislava	k1gFnPc2	Vratislava
II	II	kA	II
<g/>
..	..	k?	..
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1158	[number]	k4	1158
se	se	k3xPyFc4	se
totéž	týž	k3xTgNnSc1	týž
podařilo	podařit	k5eAaPmAgNnS	podařit
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
vnukovi	vnukův	k2eAgMnPc1d1	vnukův
Vladislavovi	Vladislavův	k2eAgMnPc1d1	Vladislavův
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
získali	získat	k5eAaPmAgMnP	získat
jako	jako	k9	jako
poctu	pocta	k1gFnSc4	pocta
a	a	k8xC	a
odměnu	odměna	k1gFnSc4	odměna
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
i	i	k9	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
v	v	k7c6	v
přemyslovském	přemyslovský	k2eAgInSc6d1	přemyslovský
rodě	rod	k1gInSc6	rod
projevují	projevovat	k5eAaImIp3nP	projevovat
velké	velký	k2eAgInPc1d1	velký
rozpory	rozpor	k1gInPc1	rozpor
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
rodovými	rodový	k2eAgFnPc7d1	rodová
liniemi	linie	k1gFnPc7	linie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sahala	sahat	k5eAaImAgFnS	sahat
moc	moc	k1gFnSc1	moc
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
trvale	trvale	k6eAd1	trvale
také	také	k9	také
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
spravovala	spravovat	k5eAaImAgFnS	spravovat
jejich	jejich	k3xOp3gFnSc1	jejich
moravská	moravský	k2eAgFnSc1d1	Moravská
větev	větev	k1gFnSc1	větev
jako	jako	k8xS	jako
údělná	údělný	k2eAgNnPc1d1	údělné
knížata	kníže	k1gNnPc1	kníže
olomoucká	olomoucký	k2eAgNnPc1d1	olomoucké
<g/>
,	,	kIx,	,
brněnská	brněnský	k2eAgNnPc1d1	brněnské
a	a	k8xC	a
znojemská	znojemský	k2eAgNnPc1d1	Znojemské
<g/>
.	.	kIx.	.
</s>
<s>
Znojemská	znojemský	k2eAgFnSc1d1	Znojemská
větev	větev	k1gFnSc1	větev
poté	poté	k6eAd1	poté
vymřela	vymřít	k5eAaPmAgFnS	vymřít
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
–	–	k?	–
její	její	k3xOp3gMnSc1	její
poslední	poslední	k2eAgMnSc1d1	poslední
představitel	představitel	k1gMnSc1	představitel
Konrád	Konrád	k1gMnSc1	Konrád
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
českým	český	k2eAgMnSc7d1	český
knížetem	kníže	k1gMnSc7	kníže
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ovšem	ovšem	k9	ovšem
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1191	[number]	k4	1191
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
větev	větev	k1gFnSc1	větev
skončila	skončit	k5eAaPmAgFnS	skončit
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
údělníky	údělník	k1gMnPc4	údělník
Spytihněvem	Spytihněv	k1gInSc7	Spytihněv
a	a	k8xC	a
Svatoplukem	Svatopluk	k1gMnSc7	Svatopluk
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
představitelem	představitel	k1gMnSc7	představitel
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
byl	být	k5eAaImAgMnS	být
kaplan	kaplan	k1gMnSc1	kaplan
Sifrid	Sifrid	k1gInSc4	Sifrid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1227	[number]	k4	1227
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
linii	linie	k1gFnSc6	linie
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
Děpolticích	Děpolticí	k2eAgInPc2d1	Děpolticí
<g/>
,	,	kIx,	,
mizí	mizet	k5eAaImIp3nS	mizet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1247	[number]	k4	1247
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Poslední	poslední	k2eAgMnPc1d1	poslední
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
státní	státní	k2eAgFnSc6d1	státní
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
dvanáctého	dvanáctý	k4xOgNnSc2	dvanáctý
století	století	k1gNnSc2	století
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
usedl	usednout	k5eAaPmAgMnS	usednout
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
tak	tak	k9	tak
řada	řada	k1gFnSc1	řada
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
střídajících	střídající	k2eAgMnPc2d1	střídající
knížat	kníže	k1gMnPc2wR	kníže
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
Přemysl	Přemysl	k1gMnSc1	Přemysl
byl	být	k5eAaImAgMnS	být
ostatně	ostatně	k6eAd1	ostatně
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1212	[number]	k4	1212
Přemysl	Přemysl	k1gMnSc1	Přemysl
I.	I.	kA	I.
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
využívající	využívající	k2eAgFnPc4d1	využívající
situace	situace	k1gFnPc4	situace
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
zajistil	zajistit	k5eAaPmAgMnS	zajistit
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
potomky	potomek	k1gMnPc4	potomek
dědičný	dědičný	k2eAgInSc4d1	dědičný
královský	královský	k2eAgInSc4d1	královský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
vlády	vláda	k1gFnSc2	vláda
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
změnila	změnit	k5eAaPmAgFnS	změnit
z	z	k7c2	z
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
víceméně	víceméně	k9	víceméně
defenzivního	defenzivní	k2eAgInSc2d1	defenzivní
postoje	postoj	k1gInSc2	postoj
na	na	k7c4	na
ofenzivní	ofenzivní	k2eAgNnSc4d1	ofenzivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobě	osoba	k1gFnSc6	osoba
krále	král	k1gMnSc2	král
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1253	[number]	k4	1253
počet	počet	k1gInSc4	počet
mužských	mužský	k2eAgInPc2d1	mužský
členů	člen	k1gInPc2	člen
rodu	rod	k1gInSc2	rod
omezil	omezit	k5eAaPmAgMnS	omezit
jen	jen	k6eAd1	jen
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Přemysl	Přemysl	k1gMnSc1	Přemysl
měl	mít	k5eAaImAgMnS	mít
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
legitimního	legitimní	k2eAgMnSc4d1	legitimní
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc4	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
také	také	k9	také
jen	jen	k9	jen
jednoho	jeden	k4xCgMnSc4	jeden
přeživšího	přeživší	k2eAgMnSc4d1	přeživší
mužského	mužský	k2eAgMnSc4d1	mužský
dědice	dědic	k1gMnSc4	dědic
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
linie	linie	k1gFnSc1	linie
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
se	se	k3xPyFc4	se
váží	vážit	k5eAaImIp3nP	vážit
počátky	počátek	k1gInPc1	počátek
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
nečekaně	nečekaně	k6eAd1	nečekaně
zavražděním	zavraždění	k1gNnSc7	zavraždění
mladého	mladý	k2eAgMnSc2d1	mladý
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
linie	linie	k1gFnSc1	linie
však	však	k9	však
poté	poté	k6eAd1	poté
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
po	po	k7c6	po
přeslici	přeslice	k1gFnSc6	přeslice
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
královských	královský	k2eAgFnPc6d1	královská
dynastiích	dynastie	k1gFnPc6	dynastie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byli	být	k5eAaImAgMnP	být
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
(	(	kIx(	(
<g/>
1310	[number]	k4	1310
<g/>
–	–	k?	–
<g/>
1436	[number]	k4	1436
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Habsburkové	Habsburk	k1gMnPc1	Habsburk
(	(	kIx(	(
<g/>
1436	[number]	k4	1436
<g/>
–	–	k?	–
<g/>
1457	[number]	k4	1457
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jagellonci	Jagellonek	k1gMnPc1	Jagellonek
(	(	kIx(	(
<g/>
1471	[number]	k4	1471
<g/>
–	–	k?	–
<g/>
1526	[number]	k4	1526
<g/>
)	)	kIx)	)
a	a	k8xC	a
opět	opět	k6eAd1	opět
Habsburkové	Habsburk	k1gMnPc1	Habsburk
(	(	kIx(	(
<g/>
1526	[number]	k4	1526
<g/>
–	–	k?	–
<g/>
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
Habsbursko-Lotrinská	habsburskootrinský	k2eAgFnSc1d1	habsbursko-lotrinská
dynastie	dynastie	k1gFnSc1	dynastie
(	(	kIx(	(
<g/>
1740	[number]	k4	1740
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
také	také	k9	také
ještě	ještě	k6eAd1	ještě
královští	královský	k2eAgMnPc1d1	královský
levobočkové	levoboček	k1gMnPc1	levoboček
a	a	k8xC	a
hlavně	hlavně	k6eAd1	hlavně
představitelé	představitel	k1gMnPc1	představitel
významné	významný	k2eAgFnSc2d1	významná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tentokrát	tentokrát	k6eAd1	tentokrát
už	už	k6eAd1	už
opravdu	opravdu	k6eAd1	opravdu
poslední	poslední	k2eAgFnPc1d1	poslední
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
linie	linie	k1gFnPc1	linie
rodu	rod	k1gInSc2	rod
–	–	k?	–
opavští	opavský	k2eAgMnPc1d1	opavský
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
,	,	kIx,	,
potomci	potomek	k1gMnPc1	potomek
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
<g/>
,	,	kIx,	,
rakouské	rakouský	k2eAgFnSc2d1	rakouská
šlechtičny	šlechtična	k1gFnSc2	šlechtična
Anežky	Anežka	k1gFnSc2	Anežka
z	z	k7c2	z
Kuenringu	Kuenring	k1gInSc2	Kuenring
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zachovalých	zachovalý	k2eAgInPc2d1	zachovalý
kosterních	kosterní	k2eAgInPc2d1	kosterní
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
(	(	kIx(	(
<g/>
studie	studie	k1gFnSc2	studie
E.	E.	kA	E.
Vlčka	Vlček	k1gMnSc4	Vlček
z	z	k7c2	z
vykopávek	vykopávka	k1gFnPc2	vykopávka
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
přemyslovská	přemyslovský	k2eAgNnPc4d1	přemyslovské
knížata	kníže	k1gNnPc4	kníže
měřila	měřit	k5eAaImAgFnS	měřit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
170	[number]	k4	170
cm	cm	kA	cm
<g/>
,	,	kIx,	,
králové	králová	k1gFnSc2	králová
necelých	celý	k2eNgInPc2d1	necelý
170	[number]	k4	170
cm	cm	kA	cm
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
štíhlí	štíhlet	k5eAaImIp3nP	štíhlet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svalnatí	svalnatět	k5eAaImIp3nS	svalnatět
<g/>
.	.	kIx.	.
</s>
<s>
Svalové	svalový	k2eAgFnPc1d1	svalová
úpony	úpona	k1gFnPc1	úpona
prozrazují	prozrazovat	k5eAaImIp3nP	prozrazovat
záměrný	záměrný	k2eAgInSc4d1	záměrný
tělesný	tělesný	k2eAgInSc4d1	tělesný
trénink	trénink	k1gInSc4	trénink
s	s	k7c7	s
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
mají	mít	k5eAaImIp3nP	mít
králové	králová	k1gFnPc1	králová
drobné	drobná	k1gFnPc1	drobná
<g/>
;	;	kIx,	;
horní	horní	k2eAgFnSc1d1	horní
řada	řada	k1gFnSc1	řada
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
poněkud	poněkud	k6eAd1	poněkud
dolní	dolní	k2eAgInSc1d1	dolní
(	(	kIx(	(
<g/>
předkus	předkus	k1gInSc1	předkus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svalové	svalový	k2eAgFnPc1d1	svalová
úpony	úpona	k1gFnPc1	úpona
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
utvořeny	utvořen	k2eAgFnPc4d1	utvořena
jenom	jenom	k9	jenom
málo	málo	k4c4	málo
<g/>
;	;	kIx,	;
řada	řada	k1gFnSc1	řada
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
typicky	typicky	k6eAd1	typicky
mužské	mužský	k2eAgFnSc2d1	mužská
lebky	lebka	k1gFnSc2	lebka
<g/>
,	,	kIx,	,
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
chlubit	chlubit	k5eAaImF	chlubit
svalnatou	svalnatý	k2eAgFnSc7d1	svalnatá
atletickou	atletický	k2eAgFnSc7d1	atletická
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
nebyli	být	k5eNaImAgMnP	být
žádní	žádný	k3yNgMnPc1	žádný
mohutní	mohutný	k2eAgMnPc1d1	mohutný
válečníci	válečník	k1gMnPc1	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
rysy	rys	k1gInPc1	rys
a	a	k8xC	a
utváření	utváření	k1gNnSc1	utváření
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
vyznačovalo	vyznačovat	k5eAaImAgNnS	vyznačovat
půvabem	půvab	k1gInSc7	půvab
skoro	skoro	k6eAd1	skoro
ženským	ženský	k2eAgInSc7d1	ženský
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
hlavně	hlavně	k9	hlavně
o	o	k7c6	o
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
vousy	vous	k1gInPc4	vous
měli	mít	k5eAaImAgMnP	mít
hnědé	hnědý	k2eAgFnPc1d1	hnědá
nebo	nebo	k8xC	nebo
kaštanové	kaštanový	k2eAgFnPc1d1	Kaštanová
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
galerii	galerie	k1gFnSc4	galerie
nejstarších	starý	k2eAgMnPc2d3	nejstarší
příslušníků	příslušník	k1gMnPc2	příslušník
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
dynastie	dynastie	k1gFnSc2	dynastie
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1134	[number]	k4	1134
<g/>
)	)	kIx)	)
představují	představovat	k5eAaImIp3nP	představovat
nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
ve	v	k7c6	v
znojemské	znojemský	k2eAgFnSc6d1	Znojemská
rotundě	rotunda	k1gFnSc6	rotunda
svaté	svatý	k2eAgFnSc2d1	svatá
Kateřiny	Kateřina	k1gFnSc2	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
+	+	kIx~	+
<g/>
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
Alžběta	Alžběta	k1gFnSc1	Alžběta
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
<g/>
+	+	kIx~	+
<g/>
Albrecht	Albrecht	k1gMnSc1	Albrecht
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pohrobek	pohrobek	k1gMnSc1	pohrobek
Alžběta	Alžběta	k1gFnSc1	Alžběta
Habsburská	habsburský	k2eAgFnSc1d1	habsburská
<g/>
+	+	kIx~	+
<g/>
Kazimír	Kazimír	k1gMnSc1	Kazimír
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
Vladislav	Vladislav	k1gMnSc1	Vladislav
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
Ludvík	Ludvík	k1gMnSc1	Ludvík
Jagellonský	jagellonský	k2eAgMnSc1d1	jagellonský
Anna	Anna	k1gFnSc1	Anna
Jagellonská	jagellonský	k2eAgFnSc1d1	Jagellonská
<g/>
+	+	kIx~	+
<g/>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc1d1	habsburský
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
(	(	kIx(	(
<g/>
její	její	k3xOp3gFnSc6	její
31	[number]	k4	31
<g/>
x	x	k?	x
pradědeček	pradědeček	k1gMnSc1	pradědeček
je	být	k5eAaImIp3nS	být
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIX	XIX	kA	XIX
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc6	jeho
13	[number]	k4	13
<g/>
x	x	k?	x
prababička	prababička	k1gFnSc1	prababička
je	být	k5eAaImIp3nS	být
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
)	)	kIx)	)
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc6	jeho
24	[number]	k4	24
<g/>
x	x	k?	x
prababička	prababička	k1gFnSc1	prababička
je	být	k5eAaImIp3nS	být
Ludmila	Ludmila	k1gFnSc1	Ludmila
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
)	)	kIx)	)
Vzdorokrál	vzdorokrál	k1gMnSc1	vzdorokrál
Fridrich	Fridrich	k1gMnSc1	Fridrich
V.	V.	kA	V.
Falcký	falcký	k2eAgMnSc1d1	falcký
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc6	jeho
14	[number]	k4	14
<g/>
x	x	k?	x
prababička	prababička	k1gFnSc1	prababička
je	být	k5eAaImIp3nS	být
Ludmila	Ludmila	k1gFnSc1	Ludmila
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
)	)	kIx)	)
Jindřich	Jindřich	k1gMnSc1	Jindřich
Korutanský	korutanský	k2eAgMnSc1d1	korutanský
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
prababička	prababička	k1gFnSc1	prababička
je	být	k5eAaImIp3nS	být
Ludmila	Ludmila	k1gFnSc1	Ludmila
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
)	)	kIx)	)
</s>
