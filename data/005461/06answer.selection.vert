<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
byli	být	k5eAaImAgMnP	být
první	první	k4xOgFnSc1	první
česká	český	k2eAgFnSc1d1	Česká
knížecí	knížecí	k2eAgFnSc1d1	knížecí
a	a	k8xC	a
královská	královský	k2eAgFnSc1d1	královská
dynastie	dynastie	k1gFnSc1	dynastie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
odnepaměti	odnepaměti	k6eAd1	odnepaměti
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vládli	vládnout	k5eAaImAgMnP	vládnout
také	také	k9	také
v	v	k7c6	v
Rakouských	rakouský	k2eAgFnPc6d1	rakouská
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
1251	[number]	k4	1251
<g/>
–	–	k?	–
<g/>
1278	[number]	k4	1278
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Polsku	Polska	k1gFnSc4	Polska
(	(	kIx(	(
<g/>
1300	[number]	k4	1300
<g/>
–	–	k?	–
<g/>
1306	[number]	k4	1306
<g/>
)	)	kIx)	)
a	a	k8xC	a
Uhersku	Uhersko	k1gNnSc6	Uhersko
(	(	kIx(	(
<g/>
1301	[number]	k4	1301
<g/>
–	–	k?	–
<g/>
1305	[number]	k4	1305
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
