<s>
Město	město	k1gNnSc1
umění	umění	k1gNnPc2
a	a	k8xC
věd	věda	k1gFnPc2
</s>
<s>
Město	město	k1gNnSc1
umění	umění	k1gNnPc2
a	a	k8xC
věd	věda	k1gFnPc2
Údaje	údaj	k1gInSc2
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Založeno	založit	k5eAaPmNgNnS
</s>
<s>
1998	#num#	k4
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
16,3	16,3	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
1,31	1,31	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Umbraculo	Umbracout	k5eAaPmAgNnS
(	(	kIx(
<g/>
L	L	kA
<g/>
'	'	kIx"
<g/>
Umbracle	Umbracl	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
À	À	k1gMnSc2
</s>
<s>
Palác	palác	k1gInSc1
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
El	Ela	k1gFnPc2
Palau	Palaus	k1gInSc2
de	de	k?
les	les	k1gInSc1
Arts	Artsa	k1gFnPc2
Reina	Rein	k2eAgInSc2d1
Sofía	Sofí	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
Hemisfè	Hemisfè	k1gMnSc1
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
prince	princ	k1gMnSc2
Filipa	Filip	k1gMnSc2
(	(	kIx(
<g/>
El	Ela	k1gFnPc2
Museu	museum	k1gNnSc3
de	de	k?
les	les	k1gInSc1
Ciè	Ciè	k1gInSc1
Príncipe	Príncip	k1gMnSc5
Felipe	Felip	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
Město	město	k1gNnSc1
umění	umění	k1gNnPc2
a	a	k8xC
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
katalánsky	katalánsky	k6eAd1
Ciutat	Ciutat	k1gMnSc2
de	de	k?
les	les	k1gInSc1
Arts	Arts	k1gInSc1
i	i	k8xC
les	les	k1gInSc1
Ciè	Ciè	k1gFnPc2
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
las	laso	k1gNnPc2
Artes	Artes	k1gInSc1
y	y	k?
las	laso	k1gNnPc2
Ciencias	Ciencias	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
reprezentativní	reprezentativní	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
Valencii	Valencie	k1gFnSc6
<g/>
,	,	kIx,
vybudované	vybudovaný	k2eAgFnPc1d1
v	v	k7c6
letech	let	k1gInPc6
1991	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
je	být	k5eAaImIp3nS
dílem	dílo	k1gNnSc7
architekta	architekt	k1gMnSc2
Santiaga	Santiago	k1gNnSc2
Calatravy	Calatrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
jeho	jeho	k3xOp3gNnPc3
největším	veliký	k2eAgNnPc3d3
a	a	k8xC
nejsložitějším	složitý	k2eAgNnPc3d3
dílům	dílo	k1gNnPc3
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	s	k7c7
symbolem	symbol	k1gInSc7
Valencie	Valencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
komplexu	komplex	k1gInSc2
</s>
<s>
Velký	velký	k2eAgInSc1d1
komplex	komplex	k1gInSc1
věnovaný	věnovaný	k2eAgInSc1d1
kultuře	kultura	k1gFnSc3
a	a	k8xC
vědě	věda	k1gFnSc3
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
urbanistického	urbanistický	k2eAgInSc2d1
kontextu	kontext	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
tvořit	tvořit	k5eAaImF
ve	v	k7c6
Valencii	Valencie	k1gFnSc6
po	po	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
kvůli	kvůli	k7c3
ničivé	ničivý	k2eAgFnSc3d1
povodni	povodeň	k1gFnSc3
odkloněna	odkloněn	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
Turia	Turium	k1gNnSc2
a	a	k8xC
v	v	k7c6
jejím	její	k3xOp3gNnSc6
původním	původní	k2eAgNnSc6d1
korytě	koryto	k1gNnSc6
vznikl	vzniknout	k5eAaPmAgInS
pás	pás	k1gInSc4
země	zem	k1gFnSc2
dlouhý	dlouhý	k2eAgInSc4d1
8	#num#	k4
km	km	kA
a	a	k8xC
široký	široký	k2eAgInSc4d1
200	#num#	k4
m	m	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
odděluje	oddělovat	k5eAaImIp3nS
staré	starý	k2eAgNnSc4d1
město	město	k1gNnSc4
od	od	k7c2
přímořského	přímořský	k2eAgNnSc2d1
letoviska	letovisko	k1gNnSc2
Nazaret	Nazaret	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
prostor	prostor	k1gInSc4
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1981	#num#	k4
<g/>
–	–	k?
<g/>
1988	#num#	k4
upravený	upravený	k2eAgInSc4d1
na	na	k7c4
park	park	k1gInSc4
architektem	architekt	k1gMnSc7
Ricardem	Ricard	k1gMnSc7
Bofillem	Bofill	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
projekt	projekt	k1gInSc1
zahrnul	zahrnout	k5eAaPmAgInS
zelené	zelený	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
a	a	k8xC
vodní	vodní	k2eAgNnPc4d1
zrcadla	zrcadlo	k1gNnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
vzpomínku	vzpomínka	k1gFnSc4
na	na	k7c4
řeku	řeka	k1gFnSc4
<g/>
,	,	kIx,
taktéž	taktéž	k?
veřejné	veřejný	k2eAgInPc4d1
prostory	prostor	k1gInPc4
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgInPc4d1
areály	areál	k1gInPc4
a	a	k8xC
botanickou	botanický	k2eAgFnSc4d1
zahradu	zahrada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komplex	komplex	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1987	#num#	k4
a	a	k8xC
2001	#num#	k4
rozšířený	rozšířený	k2eAgInSc4d1
o	o	k7c4
Palác	palác	k1gInSc4
hudby	hudba	k1gFnSc2
José	Josý	k2eAgFnSc2d1
Marie	Maria	k1gFnSc2
de	de	k?
Paredes	Paredes	k1gInSc1
<g/>
,	,	kIx,
Vědecké	vědecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
prince	princ	k1gMnSc2
Filipa	Filip	k1gMnSc2
a	a	k8xC
Mořský	mořský	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
,	,	kIx,
oceánografické	oceánografický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
o	o	k7c6
rozloze	rozloha	k1gFnSc6
80	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
vyčnívající	vyčnívající	k2eAgFnSc1d1
střecha	střecha	k1gFnSc1
je	být	k5eAaImIp3nS
dílem	dílo	k1gNnSc7
architekta	architekt	k1gMnSc2
Felixe	Felix	k1gMnSc2
Candely	Candela	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Městská	městský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Generalitat	Generalitat	k1gFnSc2
<g/>
)	)	kIx)
aby	aby	kYmCp3nS
zdůraznila	zdůraznit	k5eAaPmAgFnS
kulturní	kulturní	k2eAgInSc4d1
význam	význam	k1gInSc4
Valencie	Valencie	k1gFnSc2
<g/>
,	,	kIx,
rozhodla	rozhodnout	k5eAaPmAgFnS
postavit	postavit	k5eAaPmF
na	na	k7c6
tomto	tento	k3xDgInSc6
místě	místo	k1gNnSc6
národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
Santiago	Santiago	k1gNnSc4
Calatrava	Calatrava	k1gFnSc1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
na	na	k7c4
stavbu	stavba	k1gFnSc4
telekomunikační	telekomunikační	k2eAgFnSc2d1
věže	věž	k1gFnSc2
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
areálu	areál	k1gInSc2
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
nahrazena	nahrazen	k2eAgFnSc1d1
Palácem	palác	k1gInSc7
umění	umění	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
krátce	krátce	k6eAd1
na	na	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgInS
pověřen	pověřit	k5eAaPmNgInS
vypracováním	vypracování	k1gNnSc7
projektu	projekt	k1gInSc2
na	na	k7c4
celý	celý	k2eAgInSc4d1
komplex	komplex	k1gInSc4
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
rozloze	rozloha	k1gFnSc6
350	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
zahrnovat	zahrnovat	k5eAaImF
i	i	k9
vědecké	vědecký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
a	a	k8xC
planetárium	planetárium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Inspirace	inspirace	k1gFnSc1
</s>
<s>
Dominantní	dominantní	k2eAgFnSc7d1
inspirací	inspirace	k1gFnSc7
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
příroda	příroda	k1gFnSc1
jako	jako	k8xC,k8xS
matka	matka	k1gFnSc1
a	a	k8xC
učitelka	učitelka	k1gFnSc1
<g/>
:	:	kIx,
čtveřice	čtveřice	k1gFnSc1
budov	budova	k1gFnPc2
tohoto	tento	k3xDgNnSc2
městečka	městečko	k1gNnSc2
<g/>
,	,	kIx,
rozložené	rozložený	k2eAgFnPc4d1
podél	podél	k7c2
osy	osa	k1gFnSc2
východ	východ	k1gInSc1
<g/>
–	–	k?
<g/>
západ	západ	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
na	na	k7c6
biologických	biologický	k2eAgInPc6d1
tvarech	tvar	k1gInPc6
<g/>
,	,	kIx,
od	od	k7c2
kostry	kostra	k1gFnSc2
obrovského	obrovský	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tvoří	tvořit	k5eAaImIp3nS
budovu	budova	k1gFnSc4
Vědeckého	vědecký	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
přes	přes	k7c4
oko	oko	k1gNnSc4
planetária	planetárium	k1gNnSc2
<g/>
,	,	kIx,
určené	určený	k2eAgFnSc2d1
k	k	k7c3
pozorování	pozorování	k1gNnSc3
oblohy	obloha	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
lesu	les	k1gInSc3
zkamenělých	zkamenělý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
zoomorfní	zoomorfní	k2eAgFnPc1d1
konstrukce	konstrukce	k1gFnPc1
se	se	k3xPyFc4
odrážejí	odrážet	k5eAaImIp3nP
ve	v	k7c6
vodních	vodní	k2eAgFnPc6d1
plochách	plocha	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
je	být	k5eAaImIp3nS
jako	jako	k8xS,k8xC
symboly	symbol	k1gInPc1
moře	moře	k1gNnSc2
obklopují	obklopovat	k5eAaImIp3nP
a	a	k8xC
zdvojnásobují	zdvojnásobovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc4
už	už	k9
i	i	k9
tak	tak	k6eAd1
monumentální	monumentální	k2eAgInPc4d1
rozměry	rozměr	k1gInPc4
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
u	u	k7c2
indického	indický	k2eAgMnSc2d1
Tádž	Tádž	k1gFnSc4
Mahalu	Mahal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořením	vytvoření	k1gNnSc7
konstrukcí	konstrukce	k1gFnPc2
výhradně	výhradně	k6eAd1
z	z	k7c2
bílého	bílý	k2eAgInSc2d1
betonu	beton	k1gInSc2
se	se	k3xPyFc4
popřela	popřít	k5eAaPmAgFnS
materiálová	materiálový	k2eAgFnSc1d1
různorodost	různorodost	k1gFnSc1
a	a	k8xC
podtrhla	podtrhnout	k5eAaPmAgFnS
se	se	k3xPyFc4
abstraktní	abstraktní	k2eAgFnSc1d1
a	a	k8xC
symbolická	symbolický	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
L	L	kA
<g/>
'	'	kIx"
<g/>
Hemisfè	Hemisfè	k1gMnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
L	L	kA
<g/>
'	'	kIx"
<g/>
Hemisfè	Hemisfè	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Prostorové	prostorový	k2eAgNnSc1d1
kino	kino	k1gNnSc1
L	L	kA
<g/>
'	'	kIx"
<g/>
Hemisfè	Hemisfè	k1gFnPc2
bylo	být	k5eAaImAgNnS
dokončeno	dokončit	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc4
z	z	k7c2
budov	budova	k1gFnPc2
komplexu	komplex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
100	#num#	k4
metrů	metr	k1gInPc2
dlouhý	dlouhý	k2eAgMnSc1d1
<g/>
,	,	kIx,
55,5	55,5	k4
metrů	metr	k1gInPc2
široký	široký	k2eAgMnSc1d1
a	a	k8xC
v	v	k7c6
nejvyšším	vysoký	k2eAgInSc6d3
bodě	bod	k1gInSc6
asi	asi	k9
26	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvarem	tvar	k1gInSc7
připomíná	připomínat	k5eAaImIp3nS
obrovskou	obrovský	k2eAgFnSc4d1
lasturu	lastura	k1gFnSc4
vystupující	vystupující	k2eAgMnPc1d1
z	z	k7c2
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
o	o	k7c6
rozměrech	rozměr	k1gInPc6
1300	#num#	k4
<g/>
×	×	k?
<g/>
200	#num#	k4
m.	m.	k?
Odrazem	odraz	k1gInSc7
ve	v	k7c6
vodě	voda	k1gFnSc6
a	a	k8xC
znásobenou	znásobený	k2eAgFnSc7d1
velikostí	velikost	k1gFnSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
na	na	k7c4
lidské	lidský	k2eAgNnSc4d1
oko	oko	k1gNnSc4
<g/>
,	,	kIx,
uvnitř	uvnitř	k7c2
kterého	který	k3yIgMnSc2,k3yQgMnSc2,k3yRgMnSc2
kupole	kupole	k1gFnSc1
kina	kino	k1gNnSc2
IMAX	IMAX	kA
představuje	představovat	k5eAaImIp3nS
zorničku	zornička	k1gFnSc4
chráněnou	chráněný	k2eAgFnSc4d1
monumentálním	monumentální	k2eAgFnPc3d1
„	„	k?
<g/>
víčkem	víčko	k1gNnSc7
<g/>
“	“	k?
z	z	k7c2
ocele	ocel	k1gFnSc2
a	a	k8xC
skla	sklo	k1gNnSc2
o	o	k7c6
průměru	průměr	k1gInSc6
32	#num#	k4
m	m	kA
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
podle	podle	k7c2
potřeby	potřeba	k1gFnSc2
otvírá	otvírat	k5eAaImIp3nS
a	a	k8xC
zavírá	zavírat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kině	kino	k1gNnSc6
jsou	být	k5eAaImIp3nP
promítány	promítat	k5eAaImNgInP
zejména	zejména	k9
filmy	film	k1gInPc4
s	s	k7c7
astronomickou	astronomický	k2eAgFnSc7d1
<g/>
,	,	kIx,
historickou	historický	k2eAgFnSc7d1
a	a	k8xC
biologickou	biologický	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
'	'	kIx"
<g/>
Hemisfè	Hemisfè	k1gMnSc1
má	mít	k5eAaImIp3nS
také	také	k9
vynikající	vynikající	k2eAgFnSc4d1
akustiku	akustika	k1gFnSc4
<g/>
,	,	kIx,
dva	dva	k4xCgMnPc1
lidé	člověk	k1gMnPc1
stojící	stojící	k2eAgMnPc1d1
u	u	k7c2
opačných	opačný	k2eAgInPc2d1
pilířů	pilíř	k1gInPc2
„	„	k?
<g/>
oka	oko	k1gNnSc2
<g/>
“	“	k?
mohou	moct	k5eAaImIp3nP
bez	bez	k7c2
problému	problém	k1gInSc2
komunikovat	komunikovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
prince	princ	k1gMnSc2
Filipa	Filip	k1gMnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
ukončila	ukončit	k5eAaPmAgFnS
linii	linie	k1gFnSc4
směřující	směřující	k2eAgFnSc4d1
od	od	k7c2
východu	východ	k1gInSc2
k	k	k7c3
západu	západ	k1gInSc3
budova	budova	k1gFnSc1
Vědeckého	vědecký	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
prince	princ	k1gMnSc2
Filipa	Filip	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
velká	velký	k2eAgFnSc1d1
obdélníková	obdélníkový	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
dlouhá	dlouhý	k2eAgFnSc1d1
214	#num#	k4
m	m	kA
je	být	k5eAaImIp3nS
utvářená	utvářený	k2eAgFnSc1d1
modulovým	modulový	k2eAgNnSc7d1
opakováním	opakování	k1gNnSc7
příčného	příčný	k2eAgInSc2d1
řezu	řez	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jih	jih	k1gInSc4
se	se	k3xPyFc4
obrací	obracet	k5eAaImIp3nS
plastickou	plastický	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
z	z	k7c2
bílého	bílý	k2eAgInSc2d1
betonu	beton	k1gInSc2
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
co	co	k9
prosklená	prosklený	k2eAgFnSc1d1
severní	severní	k2eAgFnSc1d1
fasáda	fasáda	k1gFnSc1
<g/>
,	,	kIx,
připomínající	připomínající	k2eAgInSc1d1
zmrzlý	zmrzlý	k2eAgInSc4d1
vodopád	vodopád	k1gInSc4
<g/>
,	,	kIx,
vytvořený	vytvořený	k2eAgInSc4d1
ze	z	k7c2
4000	#num#	k4
skleněných	skleněný	k2eAgFnPc2d1
tabulí	tabule	k1gFnPc2
<g/>
,	,	kIx,
odhaluje	odhalovat	k5eAaImIp3nS
pět	pět	k4xCc1
obrovských	obrovský	k2eAgInPc2d1
betónových	betónový	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
uvnitř	uvnitř	k6eAd1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
jako	jako	k9
stromy	strom	k1gInPc1
podepírají	podepírat	k5eAaImIp3nP
schodiště	schodiště	k1gNnSc4
a	a	k8xC
výtahy	výtah	k1gInPc4
do	do	k7c2
vyšších	vysoký	k2eAgFnPc2d2
podlaží	podlaží	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
úhlu	úhel	k1gInSc2
pohledů	pohled	k1gInPc2
vyvolává	vyvolávat	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
budova	budova	k1gFnSc1
různé	různý	k2eAgFnPc4d1
protichůdné	protichůdný	k2eAgFnPc4d1
představy	představa	k1gFnPc4
<g/>
;	;	kIx,
minulost	minulost	k1gFnSc1
vnímaná	vnímaný	k2eAgFnSc1d1
skrzeva	skrzeva	k1gFnSc1
kostru	kostra	k1gFnSc4
obřího	obří	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
a	a	k8xC
přítomnost	přítomnost	k1gFnSc1
zhmotněná	zhmotněný	k2eAgFnSc1d1
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
zvlněné	zvlněný	k2eAgFnSc6d1
fasádě	fasáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
dlouhý	dlouhý	k2eAgInSc1d1
výstavní	výstavní	k2eAgInSc1d1
sál	sál	k1gInSc1
věnovaný	věnovaný	k2eAgInSc1d1
vývoji	vývoj	k1gInSc6
života	život	k1gInSc2
a	a	k8xC
posledním	poslední	k2eAgInPc3d1
poznatkům	poznatek	k1gInPc3
na	na	k7c6
poli	pole	k1gNnSc6
vědy	věda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
expozice	expozice	k1gFnSc1
je	být	k5eAaImIp3nS
založená	založený	k2eAgFnSc1d1
na	na	k7c6
interaktivním	interaktivní	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převrací	převracet	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
tradiční	tradiční	k2eAgNnSc1d1
didaktické	didaktický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
výstavy	výstava	k1gFnSc2
<g/>
,	,	kIx,
spojuje	spojovat	k5eAaImIp3nS
vzdělání	vzdělání	k1gNnSc1
se	s	k7c7
zábavou	zábava	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krédo	Krédo	k1gNnSc1
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
ředitele	ředitel	k1gMnSc2
Manuela	Manuel	k1gMnSc2
Toharia	Toharium	k1gNnSc2
zní	znět	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
přísně	přísně	k6eAd1
zakázané	zakázaný	k2eAgNnSc1d1
nedotýkat	dotýkat	k5eNaImF
se	se	k3xPyFc4
vystavených	vystavený	k2eAgInPc2d1
exponátů	exponát	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgNnPc1
podlaží	podlaží	k1gNnPc1
představují	představovat	k5eAaImIp3nP
tři	tři	k4xCgNnPc4
velká	velký	k2eAgNnPc4d1
témata	téma	k1gNnPc4
<g/>
,	,	kIx,
kterých	který	k3yIgNnPc2,k3yRgNnPc2,k3yQgNnPc2
se	se	k3xPyFc4
divák	divák	k1gMnSc1
zúčastní	zúčastnit	k5eAaPmIp3nS
podle	podle	k7c2
doporučených	doporučený	k2eAgFnPc2d1
tras	trasa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Expozice	expozice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
42	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
taktéž	taktéž	k?
sbírky	sbírka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
muzeu	muzeum	k1gNnSc6
přenechal	přenechat	k5eAaPmAgInS
Severo	Severa	k1gMnSc5
Ochoa	Ochoa	k1gMnSc1
<g/>
,	,	kIx,
nositel	nositel	k1gMnSc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
medicínu	medicína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Umbraculo	Umbracout	k5eAaPmAgNnS
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS
rok	rok	k1gInSc4
po	po	k7c6
dokončení	dokončení	k1gNnSc6
Vědeckého	vědecký	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
prince	princ	k1gMnSc2
Filipa	Filip	k1gMnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
situované	situovaný	k2eAgInPc1d1
rovnoběžně	rovnoběžně	k6eAd1
s	s	k7c7
muzeem	muzeum	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
něco	něco	k3yInSc4
výš	vysoce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
návštěvníkům	návštěvník	k1gMnPc3
umožňuje	umožňovat	k5eAaImIp3nS
výhled	výhled	k1gInSc4
na	na	k7c4
celý	celý	k2eAgInSc4d1
komplex	komplex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstrukce	konstrukce	k1gFnSc1
vytvořená	vytvořený	k2eAgFnSc1d1
opakováním	opakování	k1gNnSc7
55	#num#	k4
bílých	bílý	k2eAgInPc2d1
kovových	kovový	k2eAgInPc2d1
oblouků	oblouk	k1gInPc2
<g/>
,	,	kIx,
vysokých	vysoký	k2eAgInPc2d1
18	#num#	k4
m	m	kA
<g/>
,	,	kIx,
o	o	k7c6
celkové	celkový	k2eAgFnSc6d1
délce	délka	k1gFnSc6
320	#num#	k4
m	m	kA
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgFnSc7d1
interpretací	interpretace	k1gFnSc7
zimní	zimní	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
uvnitř	uvnitř	k7c2
ukrývá	ukrývat	k5eAaImIp3nS
na	na	k7c4
50	#num#	k4
rozličných	rozličný	k2eAgInPc2d1
druhů	druh	k1gInPc2
květin	květina	k1gFnPc2
typických	typický	k2eAgFnPc2d1
pro	pro	k7c4
okolí	okolí	k1gNnSc4
Valencie	Valencie	k1gFnSc2
a	a	k8xC
taktéž	taktéž	k?
několik	několik	k4yIc4
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbené	oblíbený	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
mezi	mezi	k7c4
návštěvníky	návštěvník	k1gMnPc4
kaleidoskopické	kaleidoskopický	k2eAgInPc1d1
efekty	efekt	k1gInPc1
této	tento	k3xDgFnSc2
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
galerie	galerie	k1gFnSc1
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
představující	představující	k2eAgFnSc2d1
sochy	socha	k1gFnSc2
současných	současný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podzemním	podzemní	k2eAgNnSc6d1
podlaží	podlaží	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
kryté	krytý	k2eAgNnSc4d1
parkoviště	parkoviště	k1gNnSc4
pro	pro	k7c4
auta	auto	k1gNnPc4
a	a	k8xC
autobusy	autobus	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Palác	palác	k1gInSc1
umění	umění	k1gNnSc2
</s>
<s>
Palác	palác	k1gInSc1
umění	umění	k1gNnSc1
je	být	k5eAaImIp3nS
posledním	poslední	k2eAgInSc7d1
dílem	díl	k1gInSc7
uzavírajícím	uzavírající	k2eAgInSc7d1
komplex	komplex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostně	slavnostně	k6eAd1
ho	on	k3xPp3gMnSc4
otevřeli	otevřít	k5eAaPmAgMnP
13	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
s	s	k7c7
unikátní	unikátní	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
svého	svůj	k3xOyFgInSc2
druhu	druh	k1gInSc2
a	a	k8xC
s	s	k7c7
vybavením	vybavení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
jí	on	k3xPp3gFnSc7
umožňuje	umožňovat	k5eAaImIp3nS
uvést	uvést	k5eAaPmF
jakékoliv	jakýkoliv	k3yIgNnSc4
dílo	dílo	k1gNnSc4
z	z	k7c2
oblasti	oblast	k1gFnSc2
opery	opera	k1gFnSc2
a	a	k8xC
velkých	velký	k2eAgNnPc2d1
hudebních	hudební	k2eAgNnPc2d1
představení	představení	k1gNnPc2
<g/>
,	,	kIx,
výrazně	výrazně	k6eAd1
přispívá	přispívat	k5eAaImIp3nS
k	k	k7c3
posílení	posílení	k1gNnSc3
kulturního	kulturní	k2eAgInSc2d1
významu	význam	k1gInSc2
Valencie	Valencie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Honosná	honosný	k2eAgFnSc1d1
střešní	střešní	k2eAgFnSc1d1
konstrukce	konstrukce	k1gFnSc1
dlouhá	dlouhý	k2eAgFnSc1d1
230	#num#	k4
m	m	kA
a	a	k8xC
vysoká	vysoká	k1gFnSc1
70	#num#	k4
m	m	kA
ukrývá	ukrývat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
symetrické	symetrický	k2eAgFnPc4d1
betonové	betonový	k2eAgFnPc4d1
skořepiny	skořepina	k1gFnPc4
s	s	k7c7
auditóriem	auditórium	k1gNnSc7
pro	pro	k7c4
1300	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezení	sezení	k1gNnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
libovolně	libovolně	k6eAd1
upravit	upravit	k5eAaPmF
pro	pro	k7c4
uspořádání	uspořádání	k1gNnSc4
opery	opera	k1gFnSc2
<g/>
,	,	kIx,
koncertů	koncert	k1gInPc2
či	či	k8xC
baletu	balet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
sál	sál	k1gInSc1
se	s	k7c7
400	#num#	k4
místy	místo	k1gNnPc7
pro	pro	k7c4
komorní	komorní	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
divadlo	divadlo	k1gNnSc4
a	a	k8xC
další	další	k2eAgNnSc4d1
představení	představení	k1gNnSc4
a	a	k8xC
nakonec	nakonec	k6eAd1
ještě	ještě	k6eAd1
krytý	krytý	k2eAgInSc1d1
sál	sál	k1gInSc1
pod	pod	k7c7
širým	širý	k2eAgNnSc7d1
nebem	nebe	k1gNnSc7
s	s	k7c7
kapacitou	kapacita	k1gFnSc7
2000	#num#	k4
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nabízí	nabízet	k5eAaImIp3nS
úchvatný	úchvatný	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
celý	celý	k2eAgInSc4d1
komplex	komplex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mesto	Mesto	k1gNnSc4
umení	umeeň	k1gFnPc2
a	a	k8xC
vied	vieda	k1gFnPc2
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Valeria	Valerium	k1gNnPc1
Manferto	Manferta	k1gFnSc5
De	De	k?
Fabianis	Fabianis	k1gFnPc1
<g/>
:	:	kIx,
Moderní	moderní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
Rebo	Rebo	k1gNnSc1
<g/>
,	,	kIx,
Čestlice	Čestlice	k1gFnSc1
2006	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7234	#num#	k4
<g/>
-	-	kIx~
<g/>
636	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
140	#num#	k4
<g/>
–	–	k?
<g/>
151	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Město	město	k1gNnSc1
umění	umění	k1gNnPc2
a	a	k8xC
věd	věda	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Umění	umění	k1gNnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
1095155286637687180003	#num#	k4
</s>
