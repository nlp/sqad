<s>
Město	město	k1gNnSc1
umění	umění	k1gNnPc2
a	a	k8xC
věd	věda	k1gFnPc2
(	(	kIx(
<g/>
katalánsky	katalánsky	k6eAd1
Ciutat	Ciutat	k1gMnSc2
de	de	k?
les	les	k1gInSc1
Arts	Arts	k1gInSc1
i	i	k8xC
les	les	k1gInSc1
Ciè	Ciè	k1gFnPc2
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
Ciudad	Ciudad	k1gInSc1
de	de	k?
las	laso	k1gNnPc2
Artes	Artes	k1gInSc1
y	y	k?
las	laso	k1gNnPc2
Ciencias	Ciencias	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
reprezentativní	reprezentativní	k2eAgNnSc1d1
národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
Valencii	Valencie	k1gFnSc6
<g/>
,	,	kIx,
vybudované	vybudovaný	k2eAgFnPc1d1
v	v	k7c6
letech	let	k1gInPc6
1991	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
.	.	kIx.
</s>