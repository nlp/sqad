<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Proxima	Proxima	k1gFnSc1	Proxima
b	b	k?	b
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
extrasolární	extrasolární	k2eAgFnSc1d1	extrasolární
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
trojhvězdy	trojhvězda	k1gFnSc2	trojhvězda
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
obíhající	obíhající	k2eAgFnSc1d1	obíhající
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
červeného	červený	k2eAgMnSc2d1	červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnPc1d3	nejbližší
hvězdy	hvězda	k1gFnPc1	hvězda
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
