<p>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
Proxima	Proxima	k1gFnSc1	Proxima
b	b	k?	b
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
extrasolární	extrasolární	k2eAgFnSc1d1	extrasolární
planeta	planeta	k1gFnSc1	planeta
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
trojhvězdy	trojhvězda	k1gFnSc2	trojhvězda
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
obíhající	obíhající	k2eAgFnSc1d1	obíhající
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
červeného	červený	k2eAgMnSc2d1	červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
Proximy	Proxima	k1gFnSc2	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnPc1d3	nejbližší
hvězdy	hvězda	k1gFnPc1	hvězda
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
4,2	[number]	k4	4,2
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
daleko	daleko	k6eAd1	daleko
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kentaura	kentaur	k1gMnSc2	kentaur
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
prokázanou	prokázaný	k2eAgFnSc4d1	prokázaná
exoplanetu	exoplanet	k1gInSc2	exoplanet
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
obyvatelnost	obyvatelnost	k1gFnSc1	obyvatelnost
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
prozkoumána	prozkoumán	k2eAgFnSc1d1	prozkoumána
<g/>
.	.	kIx.	.
<g/>
Objev	objevit	k5eAaPmRp2nS	objevit
planety	planeta	k1gFnSc2	planeta
oznámila	oznámit	k5eAaPmAgFnS	oznámit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
Evropská	evropský	k2eAgFnSc1d1	Evropská
jižní	jižní	k2eAgFnSc1d1	jižní
observatoř	observatoř	k1gFnSc1	observatoř
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumníci	výzkumník	k1gMnPc1	výzkumník
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
relativní	relativní	k2eAgFnSc3d1	relativní
blízkosti	blízkost	k1gFnSc3	blízkost
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
vhodná	vhodný	k2eAgFnSc1d1	vhodná
pro	pro	k7c4	pro
případný	případný	k2eAgInSc4d1	případný
robotický	robotický	k2eAgInSc4d1	robotický
průzkum	průzkum	k1gInSc4	průzkum
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
Breakthrough	Breakthrough	k1gMnSc1	Breakthrough
Starshot	Starshot	k1gMnSc1	Starshot
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
blízkých	blízký	k2eAgNnPc6d1	blízké
staletích	staletí	k1gNnPc6	staletí
<g/>
.	.	kIx.	.
<g/>
Planeta	planeta	k1gFnSc1	planeta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
metodou	metoda	k1gFnSc7	metoda
měření	měření	k1gNnPc2	měření
radiálních	radiální	k2eAgFnPc2d1	radiální
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
periodické	periodický	k2eAgInPc1d1	periodický
Dopplerovské	Dopplerovský	k2eAgInPc1d1	Dopplerovský
posuny	posun	k1gInPc1	posun
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
existenci	existence	k1gFnSc4	existence
obíhajícího	obíhající	k2eAgInSc2d1	obíhající
objektu	objekt	k1gInSc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
měření	měření	k1gNnPc2	měření
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
radiální	radiální	k2eAgFnSc1d1	radiální
rychlost	rychlost	k1gFnSc1	rychlost
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
amplitudou	amplituda	k1gFnSc7	amplituda
asi	asi	k9	asi
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc1	průměr
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
sklon	sklon	k1gInSc1	sklon
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
planety	planeta	k1gFnSc2	planeta
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgInS	být
změřen	změřen	k2eAgInSc1d1	změřen
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
limit	limit	k1gInSc1	limit
hmotnosti	hmotnost	k1gFnSc2	hmotnost
je	být	k5eAaImIp3nS	být
1,27	[number]	k4	1,27
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
planeta	planeta	k1gFnSc1	planeta
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
Dopplerovský	Dopplerovský	k2eAgInSc1d1	Dopplerovský
posun	posun	k1gInSc1	posun
maximální	maximální	k2eAgInSc1d1	maximální
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
změříme	změřit	k5eAaPmIp1nP	změřit
sklon	sklon	k1gInSc4	sklon
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
přesněji	přesně	k6eAd2	přesně
hmotnost	hmotnost	k1gFnSc4	hmotnost
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
sklon	sklon	k1gInSc1	sklon
dráhy	dráha	k1gFnSc2	dráha
znamená	znamenat	k5eAaImIp3nS	znamenat
větší	veliký	k2eAgFnSc1d2	veliký
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
analyzovaných	analyzovaný	k2eAgNnPc2d1	analyzované
dat	datum	k1gNnPc2	datum
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
na	na	k7c4	na
hmotnost	hmotnost	k1gFnSc4	hmotnost
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
3	[number]	k4	3
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	můj	k3xOp1gFnSc1	můj
planeta	planeta	k1gFnSc1	planeta
skalnaté	skalnatý	k2eAgNnSc4d1	skalnaté
jádro	jádro	k1gNnSc4	jádro
a	a	k8xC	a
hustotu	hustota	k1gFnSc4	hustota
podobnou	podobný	k2eAgFnSc4d1	podobná
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
nejméně	málo	k6eAd3	málo
1,1	[number]	k4	1,1
násobek	násobek	k1gInSc4	násobek
průměru	průměr	k1gInSc2	průměr
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	můj	k3xOp1gFnSc1	můj
planeta	planeta	k1gFnSc1	planeta
menší	malý	k2eAgFnSc4d2	menší
hustotu	hustota	k1gFnSc4	hustota
nebo	nebo	k8xC	nebo
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
minimální	minimální	k2eAgFnSc4d1	minimální
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
větší	veliký	k2eAgInSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
teplotu	teplota	k1gFnSc4	teplota
zhruba	zhruba	k6eAd1	zhruba
234	[number]	k4	234
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
mínus	mínus	k6eAd1	mínus
39	[number]	k4	39
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
obyvatelné	obyvatelný	k2eAgFnSc6d1	obyvatelná
zóně	zóna	k1gFnSc6	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
hvězda	hvězda	k1gFnSc1	hvězda
===	===	k?	===
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
červeného	červený	k2eAgMnSc2d1	červený
trpaslíka	trpaslík	k1gMnSc2	trpaslík
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
M	M	kA	M
s	s	k7c7	s
názvem	název	k1gInSc7	název
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centauri	k1gNnSc2	Centauri
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
0,12	[number]	k4	0,12
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
0,14	[number]	k4	0,14
poloměru	poloměr	k1gInSc2	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
3040	[number]	k4	3040
Kelvinů	kelvin	k1gInPc2	kelvin
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
stará	starý	k2eAgFnSc1d1	stará
4,85	[number]	k4	4,85
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
staré	staré	k1gNnSc4	staré
4,6	[number]	k4	4,6
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
5778	[number]	k4	5778
Kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
rotace	rotace	k1gFnSc1	rotace
Proximy	Proxima	k1gFnSc2	Proxima
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
83	[number]	k4	83
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc4	její
svítivost	svítivost	k1gFnSc1	svítivost
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
0,0015	[number]	k4	0,0015
svítivosti	svítivost	k1gFnSc2	svítivost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Proxima	Proxima	k1gFnSc1	Proxima
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc4d1	vysoká
metalicitu	metalicita	k1gFnSc4	metalicita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
u	u	k7c2	u
hvězd	hvězda	k1gFnPc2	hvězda
nízké	nízký	k2eAgFnSc2d1	nízká
hmotnosti	hmotnost	k1gFnSc2	hmotnost
není	být	k5eNaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
metalicita	metalicita	k1gFnSc1	metalicita
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
u	u	k7c2	u
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
<g/>
Magnituda	Magnituda	k1gFnSc1	Magnituda
hvězdy	hvězda	k1gFnSc2	hvězda
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
11,13	[number]	k4	11,13
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Zemi	zem	k1gFnSc4	zem
nejbližší	blízký	k2eAgFnSc4d3	nejbližší
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
vidět	vidět	k5eAaImF	vidět
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
její	její	k3xOp3gFnSc3	její
nízké	nízký	k2eAgFnSc3d1	nízká
svítivosti	svítivost	k1gFnSc3	svítivost
<g/>
.	.	kIx.	.
<g/>
Proxima	Proxima	k1gFnSc1	Proxima
je	být	k5eAaImIp3nS	být
proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
občas	občas	k6eAd1	občas
prochází	procházet	k5eAaImIp3nS	procházet
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
změnami	změna	k1gFnPc7	změna
jasu	jas	k1gInSc2	jas
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
magnetické	magnetický	k2eAgFnSc3d1	magnetická
aktivitě	aktivita	k1gFnSc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
aktivita	aktivita	k1gFnSc1	aktivita
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vytvořit	vytvořit	k5eAaPmF	vytvořit
silnou	silný	k2eAgFnSc4d1	silná
magnetickou	magnetický	k2eAgFnSc4d1	magnetická
bouři	bouře	k1gFnSc4	bouře
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
silně	silně	k6eAd1	silně
ozářit	ozářit	k5eAaPmF	ozářit
povrch	povrch	k1gInSc4	povrch
exoplanety	exoplanet	k1gInPc7	exoplanet
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tato	tento	k3xDgFnSc1	tento
nemá	mít	k5eNaImIp3nS	mít
atmosféru	atmosféra	k1gFnSc4	atmosféra
nebo	nebo	k8xC	nebo
silné	silný	k2eAgNnSc4d1	silné
magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
===	===	k?	===
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
oběhnutí	oběhnutí	k1gNnSc1	oběhnutí
Proxima	Proxim	k1gMnSc2	Proxim
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
trvá	trvat	k5eAaImIp3nS	trvat
11,186	[number]	k4	11,186
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
poloosa	poloosa	k1gFnSc1	poloosa
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
0,05	[number]	k4	0,05
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
jednu	jeden	k4xCgFnSc4	jeden
dvacetinu	dvacetina	k1gFnSc4	dvacetina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
Země	zem	k1gFnSc2	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
planeta	planeta	k1gFnSc1	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
Merkur	Merkur	k1gInSc1	Merkur
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
poloosu	poloosa	k1gFnSc4	poloosa
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
0,39	[number]	k4	0,39
astronomické	astronomický	k2eAgFnSc2d1	astronomická
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Planeta	planeta	k1gFnSc1	planeta
obdrží	obdržet	k5eAaPmIp3nS	obdržet
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
hvězdy	hvězda	k1gFnSc2	hvězda
asi	asi	k9	asi
65	[number]	k4	65
%	%	kIx~	%
energetického	energetický	k2eAgInSc2d1	energetický
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dostává	dostávat	k5eAaImIp3nS	dostávat
Země	zem	k1gFnPc4	zem
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
mnohem	mnohem	k6eAd1	mnohem
bližší	blízký	k2eAgFnSc3d2	bližší
dráze	dráha	k1gFnSc3	dráha
také	také	k9	také
příjme	příjem	k1gInSc5	příjem
400	[number]	k4	400
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelnost	obyvatelnost	k1gFnSc4	obyvatelnost
==	==	k?	==
</s>
</p>
<p>
<s>
Obyvatelnost	obyvatelnost	k1gFnSc1	obyvatelnost
planety	planeta	k1gFnSc2	planeta
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
nebyla	být	k5eNaImAgFnS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
<g/>
.	.	kIx.	.
</s>
<s>
Exoplaneta	Exoplaneta	k1gFnSc1	Exoplaneta
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
nacházet	nacházet	k5eAaImF	nacházet
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
hvězdy	hvězda	k1gFnSc2	hvězda
se	s	k7c7	s
správnými	správný	k2eAgFnPc7d1	správná
podmínkami	podmínka	k1gFnPc7	podmínka
umožňující	umožňující	k2eAgFnSc4d1	umožňující
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
hvězda	hvězda	k1gFnSc1	hvězda
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centaur	k1gFnSc2	Centaur
bude	být	k5eAaImBp3nS	být
jako	jako	k9	jako
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
posloupnosti	posloupnost	k1gFnSc6	posloupnost
asi	asi	k9	asi
4	[number]	k4	4
biliony	bilion	k4xCgInPc7	bilion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
320	[number]	k4	320
krát	krát	k6eAd1	krát
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
<g/>
Planeta	planeta	k1gFnSc1	planeta
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k6eAd1	blízko
své	svůj	k3xOyFgFnSc3	svůj
hvězdě	hvězda	k1gFnSc3	hvězda
a	a	k8xC	a
pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
navíc	navíc	k6eAd1	navíc
excentricitu	excentricita	k1gFnSc4	excentricita
blízkou	blízký	k2eAgFnSc4d1	blízká
nule	nula	k1gFnSc3	nula
<g/>
,	,	kIx,	,
mohlo	moct	k5eAaImAgNnS	moct
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vést	vést	k5eAaImF	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
vázané	vázaný	k2eAgFnSc2d1	vázaná
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
polokoule	polokoule	k1gFnSc1	polokoule
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
neustále	neustále	k6eAd1	neustále
mířila	mířit	k5eAaImAgFnS	mířit
ke	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
trvale	trvale	k6eAd1	trvale
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
určili	určit	k5eAaPmAgMnP	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
obyvatelnost	obyvatelnost	k1gFnSc4	obyvatelnost
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
extrémními	extrémní	k2eAgFnPc7d1	extrémní
oblastmi	oblast	k1gFnPc7	oblast
<g/>
,	,	kIx,	,
označené	označený	k2eAgInPc1d1	označený
jako	jako	k8xC	jako
terminátory	terminátor	k1gInPc1	terminátor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
nacházet	nacházet	k5eAaImF	nacházet
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
<g/>
Výstřednost	výstřednost	k1gFnSc1	výstřednost
dráhy	dráha	k1gFnSc2	dráha
planety	planeta	k1gFnSc2	planeta
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
<g/>
,	,	kIx,	,
víme	vědět	k5eAaImIp1nP	vědět
jen	jen	k9	jen
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
0,35	[number]	k4	0,35
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
dost	dost	k6eAd1	dost
na	na	k7c4	na
zachycení	zachycení	k1gNnSc4	zachycení
do	do	k7c2	do
rezonance	rezonance	k1gFnSc2	rezonance
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
jižní	jižní	k2eAgFnSc1d1	jižní
observatoř	observatoř	k1gFnSc1	observatoř
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
podmínky	podmínka	k1gFnPc4	podmínka
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
teplotou	teplota	k1gFnSc7	teplota
podobnou	podobný	k2eAgFnSc7d1	podobná
jako	jako	k8xS	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
obyvatelná	obyvatelný	k2eAgFnSc1d1	obyvatelná
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
planeta	planeta	k1gFnSc1	planeta
dostatečně	dostatečně	k6eAd1	dostatečně
hustotu	hustota	k1gFnSc4	hustota
atmosféru	atmosféra	k1gFnSc4	atmosféra
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
tepla	teplo	k1gNnSc2	teplo
mezi	mezi	k7c7	mezi
přivrácenou	přivrácený	k2eAgFnSc7d1	přivrácená
a	a	k8xC	a
odvrácenou	odvrácený	k2eAgFnSc7d1	odvrácená
polokoulí	polokoule	k1gFnSc7	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Výpočty	výpočet	k1gInPc1	výpočet
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
ozáření	ozáření	k1gNnSc3	ozáření
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
asi	asi	k9	asi
200	[number]	k4	200
milionech	milion	k4xCgInPc6	milion
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
planety	planeta	k1gFnSc2	planeta
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
ekvivalentu	ekvivalent	k1gInSc2	ekvivalent
jednoho	jeden	k4xCgInSc2	jeden
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Kapalná	kapalný	k2eAgFnSc1d1	kapalná
voda	voda	k1gFnSc1	voda
by	by	kYmCp3nS	by
proto	proto	k6eAd1	proto
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
nejvíce	nejvíce	k6eAd1	nejvíce
osluněných	osluněný	k2eAgFnPc6d1	osluněná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c6	na
polokouli	polokoule	k1gFnSc6	polokoule
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
ke	k	k7c3	k
hvězdě	hvězda	k1gFnSc3	hvězda
nebo	nebo	k8xC	nebo
v	v	k7c6	v
tropickém	tropický	k2eAgInSc6d1	tropický
pásu	pás	k1gInSc6	pás
<g/>
.	.	kIx.	.
</s>
<s>
Planeta	planeta	k1gFnSc1	planeta
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
několika	několik	k4yIc2	několik
teleskopů	teleskop	k1gInPc2	teleskop
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
prozradit	prozradit	k5eAaPmF	prozradit
více	hodně	k6eAd2	hodně
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
složení	složení	k1gNnSc6	složení
a	a	k8xC	a
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nějakou	nějaký	k3yIgFnSc4	nějaký
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objev	k1gInSc4	objev
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
náznaky	náznak	k1gInPc1	náznak
existence	existence	k1gFnSc2	existence
této	tento	k3xDgFnSc6	tento
exoplanety	exoplanet	k1gInPc4	exoplanet
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Mikko	Mikko	k1gNnSc4	Mikko
Tuomi	Tuo	k1gFnPc7	Tuo
v	v	k7c6	v
archivních	archivní	k2eAgNnPc6d1	archivní
datech	datum	k1gNnPc6	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
potvrzení	potvrzení	k1gNnSc4	potvrzení
pozorování	pozorování	k1gNnSc2	pozorování
spustila	spustit	k5eAaPmAgFnS	spustit
Evropská	evropský	k2eAgFnSc1d1	Evropská
jižní	jižní	k2eAgFnSc1d1	jižní
observatoř	observatoř	k1gFnSc1	observatoř
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
projekt	projekt	k1gInSc1	projekt
Pale	pal	k1gInSc5	pal
Red	Red	k1gFnSc3	Red
Dot	Dot	k1gFnPc3	Dot
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
bledě	bledě	k6eAd1	bledě
rudá	rudý	k2eAgFnSc1d1	rudá
tečka	tečka	k1gFnSc1	tečka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
slavnou	slavný	k2eAgFnSc4d1	slavná
fotografii	fotografia	k1gFnSc4	fotografia
Bledě	bledě	k6eAd1	bledě
modrá	modrý	k2eAgFnSc1d1	modrá
tečka	tečka	k1gFnSc1	tečka
pořízenou	pořízený	k2eAgFnSc4d1	pořízená
z	z	k7c2	z
Voyageru	Voyager	k1gInSc2	Voyager
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
planety	planeta	k1gFnSc2	planeta
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
objev	objev	k1gInSc1	objev
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
publikován	publikovat	k5eAaBmNgInS	publikovat
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
bylo	být	k5eAaImAgNnS	být
prováděno	provádět	k5eAaImNgNnS	provádět
3,6	[number]	k4	3,6
metrovým	metrový	k2eAgInSc7d1	metrový
teleskopem	teleskop	k1gInSc7	teleskop
v	v	k7c6	v
observatoři	observatoř	k1gFnSc6	observatoř
La	la	k1gNnSc2	la
Silla	Sillo	k1gNnSc2	Sillo
a	a	k8xC	a
osmimetrovým	osmimetrový	k2eAgNnSc7d1	osmimetrové
Very	Very	k1gInPc7	Very
Large	Large	k1gNnSc2	Large
Telescope	Telescop	k1gMnSc5	Telescop
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
falešné	falešný	k2eAgFnSc2d1	falešná
detekce	detekce	k1gFnSc2	detekce
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
desetina	desetina	k1gFnSc1	desetina
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucí	budoucí	k2eAgNnPc1d1	budoucí
pozorování	pozorování	k1gNnPc1	pozorování
==	==	k?	==
</s>
</p>
<p>
<s>
Přímé	přímý	k2eAgNnSc1d1	přímé
zobrazení	zobrazení	k1gNnSc1	zobrazení
exoplanety	exoplanet	k1gInPc4	exoplanet
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
možné	možný	k2eAgFnPc4d1	možná
pomoci	pomoc	k1gFnPc4	pomoc
Evropského	evropský	k2eAgInSc2d1	evropský
extrémně	extrémně	k6eAd1	extrémně
velkého	velký	k2eAgInSc2d1	velký
dalekohledu	dalekohled	k1gInSc2	dalekohled
(	(	kIx(	(
<g/>
E-ELT	E-ELT	k1gFnSc1	E-ELT
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
mohl	moct	k5eAaImAgInS	moct
Kosmický	kosmický	k2eAgInSc4d1	kosmický
dalekohled	dalekohled	k1gInSc4	dalekohled
Jamese	Jamese	k1gFnSc2	Jamese
Webba	Webba	k1gFnSc1	Webba
získat	získat	k5eAaPmF	získat
teplotní	teplotní	k2eAgInSc4d1	teplotní
profil	profil	k1gInSc4	profil
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
Proxima	Proxima	k1gFnSc1	Proxima
b	b	k?	b
skutečně	skutečně	k6eAd1	skutečně
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Teplotní	teplotní	k2eAgInPc1d1	teplotní
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
denní	denní	k2eAgFnSc7d1	denní
a	a	k8xC	a
noční	noční	k2eAgFnSc7d1	noční
stranou	strana	k1gFnSc7	strana
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
existence	existence	k1gFnSc2	existence
planety	planeta	k1gFnSc2	planeta
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
atmosféra	atmosféra	k1gFnSc1	atmosféra
by	by	kYmCp3nS	by
část	část	k1gFnSc1	část
tepla	teplo	k1gNnSc2	teplo
distribuovala	distribuovat	k5eAaBmAgFnS	distribuovat
mezi	mezi	k7c7	mezi
denní	denní	k2eAgFnSc7d1	denní
a	a	k8xC	a
noční	noční	k2eAgFnSc7d1	noční
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc1d1	velký
astronomické	astronomický	k2eAgInPc1d1	astronomický
přístroje	přístroj	k1gInPc1	přístroj
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
také	také	k9	také
pozorovat	pozorovat	k5eAaImF	pozorovat
světlo	světlo	k1gNnSc4	světlo
hvězdy	hvězda	k1gFnSc2	hvězda
odražené	odražený	k2eAgFnSc2d1	odražená
planetou	planeta	k1gFnSc7	planeta
a	a	k8xC	a
zjistit	zjistit	k5eAaPmF	zjistit
albedo	albedo	k1gNnSc4	albedo
a	a	k8xC	a
sklon	sklon	k1gInSc4	sklon
dráhy	dráha	k1gFnSc2	dráha
vůči	vůči	k7c3	vůči
nám.	nám.	k?	nám.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Proxima	Proximum	k1gNnSc2	Proximum
Centauri	Centaur	k1gFnSc2	Centaur
b	b	k?	b
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
