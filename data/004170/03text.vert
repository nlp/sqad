<s>
Díky	dík	k1gInPc1	dík
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
nové	nový	k2eAgNnSc4d1	nové
ráno	ráno	k1gNnSc4	ráno
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
komedie	komedie	k1gFnSc1	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
natočená	natočený	k2eAgFnSc1d1	natočená
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Haliny	Halina	k1gFnSc2	Halina
Pawlovské	Pawlovský	k2eAgFnSc2d1	Pawlovská
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
hrdinkou	hrdinka	k1gFnSc7	hrdinka
je	být	k5eAaImIp3nS	být
dospívající	dospívající	k2eAgFnSc1d1	dospívající
dívka	dívka	k1gFnSc1	dívka
Olga	Olga	k1gFnSc1	Olga
Hakunděková	Hakunděková	k1gFnSc1	Hakunděková
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
normalizace	normalizace	k1gFnSc2	normalizace
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Olga	Olga	k1gFnSc1	Olga
dospívá	dospívat	k5eAaImIp3nS	dospívat
v	v	k7c4	v
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
milostné	milostný	k2eAgNnSc4d1	milostné
zklamání	zklamání	k1gNnSc4	zklamání
a	a	k8xC	a
bojuje	bojovat	k5eAaImIp3nS	bojovat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
nástrahami	nástraha	k1gFnPc7	nástraha
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
rozdělený	rozdělený	k2eAgMnSc1d1	rozdělený
do	do	k7c2	do
několika	několik	k4yIc2	několik
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navazujících	navazující	k2eAgFnPc2d1	navazující
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každá	každý	k3xTgFnSc1	každý
sleduje	sledovat	k5eAaImIp3nS	sledovat
určitou	určitý	k2eAgFnSc4d1	určitá
část	část	k1gFnSc4	část
Olžina	Olžin	k2eAgInSc2d1	Olžin
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
<g/>
:	:	kIx,	:
Nejlepší	dobrý	k2eAgInSc1d3	nejlepší
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
(	(	kIx(	(
<g/>
Ivana	Ivana	k1gFnSc1	Ivana
Chýlková	Chýlková	k1gFnSc1	Chýlková
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scénář	scénář	k1gInSc1	scénář
<g/>
;	;	kIx,	;
2	[number]	k4	2
nominace	nominace	k1gFnSc2	nominace
<g/>
:	:	kIx,	:
herec	herec	k1gMnSc1	herec
-	-	kIx~	-
Franciszek	Franciszek	k1gInSc1	Franciszek
Pieczka	Pieczka	k1gFnSc1	Pieczka
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
-	-	kIx~	-
Barbora	Barbora	k1gFnSc1	Barbora
Hrzánová	Hrzánová	k1gFnSc1	Hrzánová
Díky	dík	k1gInPc4	dík
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
nové	nový	k2eAgNnSc4d1	nové
ráno	ráno	k1gNnSc4	ráno
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Díky	dík	k1gInPc4	dík
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
nové	nový	k2eAgNnSc4d1	nové
ráno	ráno	k1gNnSc4	ráno
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
