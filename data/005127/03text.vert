<s>
Fluktuace	fluktuace	k1gFnSc1	fluktuace
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
fluctuare	fluctuar	k1gMnSc5	fluctuar
<g/>
,	,	kIx,	,
houpat	houpat	k5eAaImF	houpat
se	se	k3xPyFc4	se
na	na	k7c6	na
vlnách	vlna	k1gFnPc6	vlna
<g/>
,	,	kIx,	,
pohybovat	pohybovat	k5eAaImF	pohybovat
sem	sem	k6eAd1	sem
a	a	k8xC	a
tam	tam	k6eAd1	tam
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
nepravidelný	pravidelný	k2eNgInSc1d1	nepravidelný
a	a	k8xC	a
nesoustavný	soustavný	k2eNgInSc1d1	nesoustavný
pohyb	pohyb	k1gInSc1	pohyb
"	"	kIx"	"
<g/>
sem	sem	k6eAd1	sem
a	a	k8xC	a
tam	tam	k6eAd1	tam
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
smyslu	smysl	k1gInSc6	smysl
pro	pro	k7c4	pro
nepravidelné	pravidelný	k2eNgFnPc4d1	nepravidelná
změny	změna	k1gFnPc4	změna
nějaké	nějaký	k3yIgFnSc2	nějaký
veličiny	veličina	k1gFnSc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Fluktuace	fluktuace	k1gFnSc1	fluktuace
znamená	znamenat	k5eAaImIp3nS	znamenat
velikost	velikost	k1gFnSc4	velikost
nahodilých	nahodilý	k2eAgFnPc2d1	nahodilá
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
nebo	nebo	k8xC	nebo
určité	určitý	k2eAgFnSc2d1	určitá
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nelze	lze	k6eNd1	lze
příčinně	příčinně	k6eAd1	příčinně
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
jakýsi	jakýsi	k3yIgInSc4	jakýsi
"	"	kIx"	"
<g/>
statistický	statistický	k2eAgInSc4d1	statistický
šum	šum	k1gInSc4	šum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fluktuace	fluktuace	k1gFnPc1	fluktuace
nemají	mít	k5eNaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
jako	jako	k9	jako
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
omezují	omezovat	k5eAaImIp3nP	omezovat
však	však	k9	však
možnou	možný	k2eAgFnSc4d1	možná
přesnost	přesnost	k1gFnSc4	přesnost
měření	měření	k1gNnSc2	měření
fluktuující	fluktuující	k2eAgFnSc2d1	fluktuující
veličiny	veličina	k1gFnSc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Fluktuace	fluktuace	k1gFnPc4	fluktuace
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
počtu	počet	k1gInSc2	počet
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
instituci	instituce	k1gFnSc6	instituce
<g/>
,	,	kIx,	,
organizaci	organizace	k1gFnSc6	organizace
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
v	v	k7c6	v
absolutních	absolutní	k2eAgInPc6d1	absolutní
počtech	počet	k1gInPc6	počet
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	s	k7c7	s
<g/>
:	:	kIx,	:
fluktuace	fluktuace	k1gFnSc1	fluktuace
přirozená	přirozený	k2eAgFnSc1d1	přirozená
<g/>
,	,	kIx,	,
daná	daný	k2eAgFnSc1d1	daná
například	například	k6eAd1	například
stárnutím	stárnutí	k1gNnSc7	stárnutí
a	a	k8xC	a
úmrtností	úmrtnost	k1gFnSc7	úmrtnost
<g/>
,	,	kIx,	,
fluktuace	fluktuace	k1gFnSc1	fluktuace
instituční	instituční	k2eAgFnSc1d1	instituční
<g/>
,	,	kIx,	,
daná	daný	k2eAgFnSc1d1	daná
povahou	povaha	k1gFnSc7	povaha
instituce	instituce	k1gFnSc1	instituce
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
v	v	k7c6	v
pětitřídní	pětitřídní	k2eAgFnSc6d1	pětitřídní
škole	škola	k1gFnSc6	škola
bude	být	k5eAaImBp3nS	být
instituční	instituční	k2eAgFnSc1d1	instituční
fluktuace	fluktuace	k1gFnSc1	fluktuace
žáků	žák	k1gMnPc2	žák
kolem	kolem	k7c2	kolem
20	[number]	k4	20
<g/>
%	%	kIx~	%
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
třída	třída	k1gFnSc1	třída
z	z	k7c2	z
pěti	pět	k4xCc2	pět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fluktuace	fluktuace	k1gFnSc1	fluktuace
individuální	individuální	k2eAgFnSc1d1	individuální
<g/>
,	,	kIx,	,
vznikající	vznikající	k2eAgNnSc1d1	vznikající
z	z	k7c2	z
osobních	osobní	k2eAgNnPc2d1	osobní
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Fluktuace	fluktuace	k1gFnPc4	fluktuace
znamená	znamenat	k5eAaImIp3nS	znamenat
nahodilý	nahodilý	k2eAgInSc1d1	nahodilý
pohyb	pohyb	k1gInSc1	pohyb
cen	cena	k1gFnPc2	cena
komodit	komodita	k1gFnPc2	komodita
<g/>
,	,	kIx,	,
akcií	akcie	k1gFnPc2	akcie
nebo	nebo	k8xC	nebo
měn	měna	k1gFnPc2	měna
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
delším	dlouhý	k2eAgInSc6d2	delší
časovém	časový	k2eAgInSc6d1	časový
intervalu	interval	k1gInSc6	interval
lze	lze	k6eAd1	lze
z	z	k7c2	z
chaotické	chaotický	k2eAgFnSc2d1	chaotická
křivky	křivka	k1gFnSc2	křivka
fluktuací	fluktuace	k1gFnPc2	fluktuace
vyčíst	vyčíst	k5eAaPmF	vyčíst
případné	případný	k2eAgInPc4d1	případný
trendy	trend	k1gInPc4	trend
a	a	k8xC	a
dlouhodobé	dlouhodobý	k2eAgFnPc4d1	dlouhodobá
změny	změna	k1gFnPc4	změna
<g/>
.	.	kIx.	.
</s>
