<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
</s>
<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Systematický	systematický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Boron	Boron	k1gMnSc1
trioxide	trioxid	k1gInSc5
</s>
<s>
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Bortrioxid	Bortrioxid	k1gInSc1
</s>
<s>
Sumární	sumární	k2eAgInSc1d1
vzorec	vzorec	k1gInSc1
</s>
<s>
B2O3	B2O3	kA
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Bezbarvá	bezbarvý	k2eAgFnSc1d1
nebo	nebo	k8xC
bílá	bílý	k2eAgFnSc1d1
amorfní	amorfní	k2eAgFnSc1d1
nebo	nebo	k8xC
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
1303-86-2	1303-86-2	k4
</s>
<s>
EC-no	EC-no	k1gNnSc1
(	(	kIx(
<g/>
EINECS	EINECS	kA
<g/>
/	/	kIx~
<g/>
ELINCS	ELINCS	kA
<g/>
/	/	kIx~
<g/>
NLP	NLP	kA
<g/>
)	)	kIx)
</s>
<s>
215-125-8	215-125-8	k4
</s>
<s>
Indexové	indexový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
005-008-00-8	005-008-00-8	k4
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Molární	molární	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
69,62	69,62	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
448,8	448,8	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
1	#num#	k4
860	#num#	k4
°	°	k?
<g/>
C	C	kA
</s>
<s>
Teplota	teplota	k1gFnSc1
skelného	skelný	k2eAgInSc2d1
přechodu	přechod	k1gInSc2
</s>
<s>
450	#num#	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
2,46	2,46	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
<g/>
br	br	k0
/	/	kIx~
<g/>
>	>	kIx)
1,812	1,812	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
cm	cm	kA
<g/>
3	#num#	k4
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Dynamický	dynamický	k2eAgInSc1d1
viskozitní	viskozitní	k2eAgInSc1d1
koeficient	koeficient	k1gInSc1
</s>
<s>
5	#num#	k4
020	#num#	k4
cP	cP	k?
(	(	kIx(
<g/>
1	#num#	k4
137	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
3	#num#	k4
840	#num#	k4
cP	cP	k?
(	(	kIx(
<g/>
1	#num#	k4
217	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
1	#num#	k4
870	#num#	k4
cP	cP	k?
(	(	kIx(
<g/>
1	#num#	k4
417	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
918	#num#	k4
cP	cP	k?
(	(	kIx(
<g/>
1	#num#	k4
617	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Index	index	k1gInSc1
lomu	lom	k1gInSc2
</s>
<s>
1,61	1,61	k4
</s>
<s>
Rozpustnost	rozpustnost	k1gFnSc1
ve	v	k7c6
vodě	voda	k1gFnSc6
</s>
<s>
1,1	1,1	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
ml	ml	kA
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
,	,	kIx,
0	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
15,7	15,7	k4
g	g	kA
<g/>
/	/	kIx~
<g/>
100	#num#	k4
ml	ml	kA
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
,	,	kIx,
100	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
permitivita	permitivita	k1gFnSc1
ε	ε	k1gFnPc2
</s>
<s>
3,2	3,2	k4
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Povrchové	povrchový	k2eAgNnSc1d1
napětí	napětí	k1gNnSc1
</s>
<s>
97,1	97,1	k4
mN	mN	k?
<g/>
/	/	kIx~
<g/>
m	m	kA
(	(	kIx(
<g/>
700	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
90,1	90,1	k4
mN	mN	k?
<g/>
/	/	kIx~
<g/>
m	m	kA
(	(	kIx(
<g/>
1	#num#	k4
000	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
79,4	79,4	k4
mN	mN	k?
<g/>
/	/	kIx~
<g/>
m	m	kA
(	(	kIx(
<g/>
1	#num#	k4
200	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
72,4	72,4	k4
mN	mN	k?
<g/>
/	/	kIx~
<g/>
m	m	kA
(	(	kIx(
<g/>
1	#num#	k4
400	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Struktura	struktura	k1gFnSc1
</s>
<s>
Krystalová	krystalový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
</s>
<s>
šesterečná	šesterečný	k2eAgFnSc1d1
amorfní	amorfní	k2eAgFnSc1d1
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Hrana	hrana	k1gFnSc1
krystalové	krystalový	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
</s>
<s>
a	a	k8xC
<g/>
=	=	kIx~
<g/>
433	#num#	k4
pm	pm	k?
<g/>
,	,	kIx,
c	c	k0
<g/>
=	=	kIx~
<g/>
839,2	839,2	k4
pm	pm	k?
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
entalpie	entalpie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
1	#num#	k4
273,4	273,4	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
-1	-1	k4
255	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Entalpie	entalpie	k1gFnSc1
tání	tání	k1gNnSc3
Δ	Δ	k5eAaPmF
</s>
<s>
330	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Entalpie	entalpie	k1gFnSc1
varu	var	k1gInSc2
Δ	Δ	k1gNnPc2
</s>
<s>
5	#num#	k4
114	#num#	k4
J	J	kA
<g/>
/	/	kIx~
<g/>
g	g	kA
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
molární	molární	k2eAgFnSc1d1
entropie	entropie	k1gFnSc1
S	s	k7c7
<g/>
°	°	k?
</s>
<s>
54	#num#	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
77,9	77,9	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
mol	mol	k1gInSc1
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1
slučovací	slučovací	k2eAgFnSc1d1
Gibbsova	Gibbsův	k2eAgFnSc1d1
energie	energie	k1gFnSc1
Δ	Δ	k1gMnSc1
<g/>
°	°	k?
</s>
<s>
−	−	k?
194,3	194,3	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gInSc1
−	−	k?
183	#num#	k4
kJ	kJ	k?
<g/>
/	/	kIx~
<g/>
mol	mol	k1gMnSc1
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Izobarické	izobarický	k2eAgNnSc1d1
měrné	měrný	k2eAgNnSc1d1
teplo	teplo	k1gNnSc1
cp	cp	k?
</s>
<s>
0,904	0,904	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
0,878	0,878	k4
JK	JK	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
g	g	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
(	(	kIx(
<g/>
modifikace	modifikace	k1gFnSc1
β	β	k?
<g/>
)	)	kIx)
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1
</s>
<s>
GHS08	GHS08	k4
</s>
<s>
H-věty	H-věta	k1gFnPc1
</s>
<s>
H360FD	H360FD	k4
</s>
<s>
Toxický	toxický	k2eAgMnSc1d1
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
</s>
<s>
R-věty	R-věta	k1gFnPc1
</s>
<s>
R60	R60	k4
R61	R61	k1gFnSc1
</s>
<s>
S-věty	S-věta	k1gFnPc1
</s>
<s>
S45	S45	k4
S53	S53	k1gFnSc1
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
bezbarvá	bezbarvý	k2eAgFnSc1d1
nebo	nebo	k8xC
bílá	bílý	k2eAgFnSc1d1
amorfní	amorfní	k2eAgFnSc1d1
nebo	nebo	k8xC
krystalická	krystalický	k2eAgFnSc1d1
pevná	pevný	k2eAgFnSc1d1
látka	látka	k1gFnSc1
bez	bez	k7c2
zápachu	zápach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Příprava	příprava	k1gFnSc1
</s>
<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
se	se	k3xPyFc4
sice	sice	k8xC
dá	dát	k5eAaPmIp3nS
připravit	připravit	k5eAaPmF
spalováním	spalování	k1gNnSc7
elementárního	elementární	k2eAgInSc2d1
bóru	bór	k1gInSc2
v	v	k7c6
proudu	proud	k1gInSc6
kyslíku	kyslík	k1gInSc2
</s>
<s>
4	#num#	k4
B	B	kA
+	+	kIx~
3	#num#	k4
O2	O2	k1gFnSc2
→	→	k?
2	#num#	k4
B	B	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
ale	ale	k8xC
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
většinou	většinou	k6eAd1
připravuje	připravovat	k5eAaImIp3nS
žíháním	žíhání	k1gNnSc7
čisté	čistý	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
borité	boritý	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
tak	tak	k6eAd1
ztrácí	ztrácet	k5eAaImIp3nS
vodu	voda	k1gFnSc4
a	a	k8xC
mění	měnit	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
anhydrid	anhydrid	k1gInSc4
</s>
<s>
2	#num#	k4
H3BO3	H3BO3	k1gMnSc1
→	→	k?
B2O3	B2O3	k1gMnSc1
+	+	kIx~
3	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
</s>
<s>
Vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
V	v	k7c6
pevném	pevný	k2eAgNnSc6d1
skupenství	skupenství	k1gNnSc6
je	být	k5eAaImIp3nS
oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
polymerní	polymerní	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
tvořená	tvořený	k2eAgNnPc4d1
vzájemně	vzájemně	k6eAd1
propojenou	propojený	k2eAgFnSc7d1
sítí	síť	k1gFnSc7
kovalentně	kovalentně	k6eAd1
vázaných	vázaný	k2eAgInPc2d1
atomů	atom	k1gInPc2
boru	bor	k1gInSc2
a	a	k8xC
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
amorfní	amorfní	k2eAgNnSc1d1
(	(	kIx(
<g/>
sklovité	sklovitý	k2eAgNnSc1d1
<g/>
)	)	kIx)
modifikace	modifikace	k1gFnSc1
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
strukturní	strukturní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
šestičlenný	šestičlenný	k2eAgInSc1d1
oxoboranový	oxoboranový	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
horní	horní	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
v	v	k7c6
infoboxu	infobox	k1gInSc6
<g/>
)	)	kIx)
tvaru	tvar	k1gInSc2
rovinného	rovinný	k2eAgInSc2d1
šestiúhelníku	šestiúhelník	k1gInSc2
<g/>
.	.	kIx.
krystalická	krystalický	k2eAgFnSc1d1
forma	forma	k1gFnSc1
krystaluje	krystalovat	k5eAaImIp3nS
v	v	k7c6
trojklonné	trojklonný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
(	(	kIx(
<g/>
prostorová	prostorový	k2eAgFnSc1d1
grupa	grupa	k1gFnSc1
symetrie	symetrie	k1gFnSc2
P	P	kA
<g/>
31	#num#	k4
<g/>
,	,	kIx,
elementární	elementární	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
a	a	k8xC
=	=	kIx~
43,36	43,36	k4
pm	pm	k?
<g/>
,	,	kIx,
c	c	k0
=	=	kIx~
83,40	83,40	k4
pm	pm	k?
<g/>
)	)	kIx)
<g/>
;	;	kIx,
elementární	elementární	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
krystalové	krystalový	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
obsahuje	obsahovat	k5eAaImIp3nS
šest	šest	k4xCc1
atomů	atom	k1gInPc2
bóru	bór	k1gInSc2
a	a	k8xC
devět	devět	k4xCc1
atomů	atom	k1gInPc2
kyslíku	kyslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
plynné	plynný	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
je	být	k5eAaImIp3nS
sloučenina	sloučenina	k1gFnSc1
pravděpodobně	pravděpodobně	k6eAd1
tvořena	tvořit	k5eAaImNgFnS
molekulami	molekula	k1gFnPc7
B4O6	B4O6	k1gFnSc7
s	s	k7c7
tricyklickým	tricyklický	k2eAgNnSc7d1
uspořádáním	uspořádání	k1gNnSc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
spodní	spodní	k2eAgInPc4d1
vzorec	vzorec	k1gInSc4
v	v	k7c6
infoboxu	infobox	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Látka	látka	k1gFnSc1
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
vodě	voda	k1gFnSc6
se	se	k3xPyFc4
snadno	snadno	k6eAd1
rozpouští	rozpouštět	k5eAaImIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
kyseliny	kyselina	k1gFnSc2
borité	boritý	k2eAgFnSc2d1
</s>
<s>
B2O3	B2O3	k4
+	+	kIx~
3	#num#	k4
H2O	H2O	k1gFnSc2
→	→	k?
2	#num#	k4
H	H	kA
<g/>
3	#num#	k4
<g/>
BO	BO	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
oxidu	oxid	k1gInSc2
boritého	boritý	k2eAgInSc2d1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
připravit	připravit	k5eAaPmF
elementární	elementární	k2eAgInSc4d1
bór	bór	k1gInSc4
redukcí	redukce	k1gFnPc2
některými	některý	k3yIgInPc7
kovy	kov	k1gInPc7
<g/>
,	,	kIx,
např.	např.	kA
hořčíkem	hořčík	k1gInSc7
</s>
<s>
B2O3	B2O3	k4
+	+	kIx~
3	#num#	k4
Mg	mg	kA
→	→	k?
2	#num#	k4
B	B	kA
+	+	kIx~
3	#num#	k4
MgO	MgO	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podobně	podobně	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
redukce	redukce	k1gFnSc1
na	na	k7c4
elementární	elementární	k2eAgInSc4d1
bór	bór	k1gInSc4
provést	provést	k5eAaPmF
sodíkem	sodík	k1gInSc7
<g/>
,	,	kIx,
draslíkem	draslík	k1gInSc7
nebo	nebo	k8xC
hliníkem	hliník	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redukcí	redukce	k1gFnPc2
uhlíkem	uhlík	k1gInSc7
se	se	k3xPyFc4
čistý	čistý	k2eAgInSc1d1
bór	bór	k1gInSc1
připravit	připravit	k5eAaPmF
nedá	dát	k5eNaPmIp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
vyredukovaný	vyredukovaný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
se	se	k3xPyFc4
s	s	k7c7
uhlíkem	uhlík	k1gInSc7
okamžitě	okamžitě	k6eAd1
slučuje	slučovat	k5eAaImIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
karbidu	karbid	k1gInSc2
boru	bor	k1gInSc2
</s>
<s>
2	#num#	k4
B2O3	B2O3	k1gFnSc1
+	+	kIx~
7	#num#	k4
C	C	kA
→	→	k?
B4C	B4C	k1gMnSc1
+	+	kIx~
6	#num#	k4
CO	co	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zahřívá	zahřívat	k5eAaImIp3nS
<g/>
-li	-li	k?
se	se	k3xPyFc4
oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
s	s	k7c7
uhlíkem	uhlík	k1gInSc7
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
chlóru	chlór	k1gInSc2
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
chlorid	chlorid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
<g/>
:	:	kIx,
</s>
<s>
B2O3	B2O3	k4
+	+	kIx~
3	#num#	k4
C	C	kA
+	+	kIx~
3	#num#	k4
Cl	Cl	k1gFnSc1
<g/>
2	#num#	k4
→	→	k?
2	#num#	k4
BCl	BCl	k1gFnSc1
<g/>
3	#num#	k4
+	+	kIx~
3	#num#	k4
CO	co	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Působením	působení	k1gNnSc7
fluorovodíku	fluorovodík	k1gInSc2
na	na	k7c4
oxid	oxid	k1gInSc4
boritý	boritý	k2eAgInSc1d1
vzniká	vznikat	k5eAaImIp3nS
fluorid	fluorid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
</s>
<s>
B2O3	B2O3	k4
+	+	kIx~
6	#num#	k4
HF	HF	kA
→	→	k?
2	#num#	k4
BF3	BF3	k1gFnPc2
+	+	kIx~
3	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
,	,	kIx,
</s>
<s>
případně	případně	k6eAd1
při	při	k7c6
nadbytku	nadbytek	k1gInSc6
fluorovodíku	fluorovodík	k1gInSc2
vzniká	vznikat	k5eAaImIp3nS
kyselina	kyselina	k1gFnSc1
tetrafluoroboritá	tetrafluoroboritý	k2eAgFnSc1d1
</s>
<s>
B2O3	B2O3	k4
+	+	kIx~
8	#num#	k4
HF	HF	kA
→	→	k?
2	#num#	k4
HBF4	HBF4	k1gFnPc2
+	+	kIx~
3	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
má	mít	k5eAaImIp3nS
široké	široký	k2eAgNnSc4d1
využití	využití	k1gNnSc4
<g/>
,	,	kIx,
zejména	zejména	k9
jako	jako	k9
</s>
<s>
tavidlo	tavidlo	k1gNnSc1
při	při	k7c6
výrobě	výroba	k1gFnSc6
skla	sklo	k1gNnSc2
<g/>
;	;	kIx,
</s>
<s>
součást	součást	k1gFnSc1
směsí	směs	k1gFnPc2
pro	pro	k7c4
přípravu	příprava	k1gFnSc4
glazur	glazura	k1gFnPc2
porcelánových	porcelánový	k2eAgInPc2d1
a	a	k8xC
keramických	keramický	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
;	;	kIx,
</s>
<s>
surovina	surovina	k1gFnSc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
boru	bor	k1gInSc2
<g/>
,	,	kIx,
karbidu	karbid	k1gInSc2
boru	bor	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
chemická	chemický	k2eAgFnSc1d1
surovina	surovina	k1gFnSc1
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
dalších	další	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
bóru	bór	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
aditivum	aditivum	k1gNnSc1
při	při	k7c6
výrobě	výroba	k1gFnSc6
materiálu	materiál	k1gInSc2
pro	pro	k7c4
optická	optický	k2eAgNnPc4d1
vlákna	vlákno	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Fyziologické	fyziologický	k2eAgNnSc1d1
působení	působení	k1gNnSc1
</s>
<s>
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
není	být	k5eNaImIp3nS
vysloveně	vysloveně	k6eAd1
jedovatý	jedovatý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
vodou	voda	k1gFnSc7
reaguje	reagovat	k5eAaBmIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
slabé	slabý	k2eAgFnSc2d1
kyseliny	kyselina	k1gFnSc2
borité	boritý	k2eAgFnSc2d1
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
však	však	k9
ve	v	k7c6
větších	veliký	k2eAgFnPc6d2
dávkách	dávka	k1gFnPc6
způsobit	způsobit	k5eAaPmF
podráždění	podráždění	k1gNnSc3
sliznic	sliznice	k1gFnPc2
nebo	nebo	k8xC
očí	oko	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
požití	požití	k1gNnSc6
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
množství	množství	k1gNnSc6
může	moct	k5eAaImIp3nS
vyvolat	vyvolat	k5eAaPmF
nevolnost	nevolnost	k1gFnSc1
<g/>
,	,	kIx,
žaludeční	žaludeční	k2eAgFnPc1d1
bolesti	bolest	k1gFnPc1
<g/>
,	,	kIx,
zvracení	zvracení	k1gNnPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
průjem	průjem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA
<g/>
,	,	kIx,
JIŘÍ	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŠTULÍK	štulík	k1gInSc1
<g/>
,	,	kIx,
KAREL	Karel	k1gMnSc1
<g/>
;	;	kIx,
JULÁK	JULÁK	kA
<g/>
,	,	kIx,
ALOIS	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chemické	chemický	k2eAgFnPc4d1
a	a	k8xC
analytické	analytický	k2eAgFnPc4d1
tabulky	tabulka	k1gFnPc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7169	#num#	k4
<g/>
-	-	kIx~
<g/>
855	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Oxidy	oxid	k1gInPc1
s	s	k7c7
prvkem	prvek	k1gInSc7
v	v	k7c6
oxidačním	oxidační	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc4
hlinitý	hlinitý	k2eAgInSc4d1
(	(	kIx(
<g/>
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
antimonitý	antimonitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sb	sb	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
arsenitý	arsenitý	k2eAgInSc1d1
(	(	kIx(
<g/>
As	as	k1gInSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
bismutitý	bismutitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Bi	Bi	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
boritý	boritý	k2eAgInSc1d1
(	(	kIx(
<g/>
B	B	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
ceritý	ceritý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ce	Ce	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
chromitý	chromitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Cr	cr	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
dusitý	dusitý	k2eAgInSc1d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
N	N	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
erbitý	erbitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Er	Er	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
gadolinitý	gadolinitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Gd	Gd	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
gallitý	gallitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ga	Ga	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
holmitý	holmitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ho	on	k3xPp3gNnSc2
<g/>
2	#num#	k4
<g/>
O	o	k7c6
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
inditý	inditý	k2eAgInSc1d1
(	(	kIx(
<g/>
In	In	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
železitý	železitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Fe	Fe	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
lanthanitý	lanthanitý	k2eAgInSc1d1
(	(	kIx(
<g/>
La	la	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
lutecitý	lutecitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Lu	Lu	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
měditý	měditý	k2eAgInSc1d1
(	(	kIx(
<g/>
Cu	Cu	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
niklitý	niklitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ni	on	k3xPp3gFnSc4
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Oxid	oxid	k1gInSc1
fosforitý	fosforitý	k2eAgInSc1d1
(	(	kIx(
<g/>
P	P	kA
<g/>
4	#num#	k4
<g/>
O	o	k7c4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
promethitý	promethitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Pm	Pm	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
rhoditý	rhoditý	k2eAgInSc1d1
(	(	kIx(
<g/>
Rh	Rh	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
samaritý	samaritý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sm	Sm	k1gFnSc1
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
skanditý	skanditý	k2eAgInSc1d1
(	(	kIx(
<g/>
Sc	Sc	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
stříbrno-stříbřitý	stříbrno-stříbřitý	k2eAgInSc1d1
(	(	kIx(
<g/>
AgO	aga	k1gMnSc5
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
stříbřitý	stříbřitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ag	Ag	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
terbitý	terbitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Tb	Tb	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
thallitý	thallitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Tl	Tl	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
thulitý	thulitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Tm	Tm	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
titanitý	titanitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Ti	ten	k3xDgMnPc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Oxid	oxid	k1gInSc1
wolframitý	wolframitý	k2eAgInSc1d1
(	(	kIx(
<g/>
W	W	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
vanaditý	vanaditý	k2eAgInSc1d1
(	(	kIx(
<g/>
V	v	k7c4
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
ytterbitý	ytterbitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Yb	Yb	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
yttritý	yttritý	k2eAgInSc1d1
(	(	kIx(
<g/>
Y	Y	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
zlatitý	zlatitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Au	au	k0
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
chloritý	chloritý	k2eAgInSc1d1
(	(	kIx(
<g/>
Cl	Cl	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
americitý	americitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Am	Am	k1gFnSc1
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Též	též	k9
také	také	k9
<g/>
:	:	kIx,
Oxid	oxid	k1gInSc1
železnato-železitý	železnato-železitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Fe	Fe	k1gFnSc1
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oxid	oxid	k1gInSc1
praseodymito-praseodymičitý	praseodymito-praseodymičitý	k2eAgInSc1d1
(	(	kIx(
<g/>
Pr	pr	k0
<g/>
6	#num#	k4
<g/>
O	o	k7c4
<g/>
11	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4146385-7	4146385-7	k4
</s>
