<s>
Cristiano	Cristiano	k1gFnSc1
Ronaldo	Ronaldo	k1gFnSc1*xF
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
2018	#num#	k4
v	v	k7c6
zápase	zápas	k1gInSc6
Portugalska	Portugalsko	k1gNnSc2
proti	proti	k7c3
Španělsku	Španělsko	k1gNnSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
dosáhl	dosáhnout	k5eAaPmAgInS
hattrickOsobní	hattrickOsobní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
dos	dos	k?
Santos	Santos	k1gMnSc1
Aveiro	Aveiro	k1gNnSc1
Ronaldo	Ronaldo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1985	#num#	k4
(	(	kIx(
<g/>
36	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Funchal	Funchat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
Výška	výška	k1gFnSc1
</s>
<s>
1,87	1,87	k4
m	m	kA
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
84	#num#	k4
kg	kg	kA
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
Chris	Chris	k1gFnSc1
<g/>
,	,	kIx,
CR	cr	k0
<g/>
7	#num#	k4
<g/>
,	,	kIx,
Ronny	Ronen	k2eAgFnPc4d1
Klubové	klubový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Juventus	Juventus	k1gInSc4
Číslo	číslo	k1gNnSc1
dresu	dres	k1gInSc2
</s>
<s>
7	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
Útočník	útočník	k1gMnSc1
Mládežnické	mládežnický	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
1992	#num#	k4
<g/>
–	–	k?
<g/>
19951995	#num#	k4
<g/>
–	–	k?
<g/>
19971997	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
</s>
<s>
Andorinha	Andorinha	k1gMnSc1
Nacional	Nacional	k1gFnSc2
Sporting	Sporting	k1gInSc1
CP	CP	kA
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
20022002	#num#	k4
<g/>
–	–	k?
<g/>
20032003	#num#	k4
<g/>
–	–	k?
<g/>
20092009	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
2018	#num#	k4
<g/>
–	–	k?
Sporting	Sporting	k1gInSc1
CP	CP	kA
B	B	kA
Sporting	Sporting	k1gInSc4
CP	CP	kA
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Juventus	Juventus	k1gInSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2000	#num#	k4
<g/>
(	(	kIx(
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
25000	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
19600	#num#	k4
<g/>
(	(	kIx(
<g/>
84	#num#	k4
<g/>
)	)	kIx)
<g/>
2920	#num#	k4
<g/>
(	(	kIx(
<g/>
311	#num#	k4
<g/>
)	)	kIx)
0	#num#	k4
<g/>
8700	#num#	k4
<g/>
(	(	kIx(
<g/>
75	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
20012001	#num#	k4
<g/>
–	–	k?
<g/>
200220032002	#num#	k4
<g/>
–	–	k?
<g/>
200320042003	#num#	k4
<g/>
–	–	k?
Portugalsko	Portugalsko	k1gNnSc1
U15	U15	k1gFnSc1
Portugalsko	Portugalsko	k1gNnSc1
U17	U17	k1gFnSc2
Portugalsko	Portugalsko	k1gNnSc1
U20	U20	k1gFnSc2
Portugalsko	Portugalsko	k1gNnSc1
U21	U21	k1gFnSc2
Portugalsko	Portugalsko	k1gNnSc1
U23	U23	k1gFnPc2
Portugalsko	Portugalsko	k1gNnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
9000	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7000	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5000	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
10000	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3000	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
1700	#num#	k4
<g/>
(	(	kIx(
<g/>
102	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
ME	ME	kA
2004	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
ME	ME	kA
2012	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
ME	ME	kA
2016	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Liga	liga	k1gFnSc1
národů	národ	k1gInPc2
UEFA	UEFA	kA
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
</s>
<s>
2014	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
</s>
<s>
2008	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
anglická	anglický	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
španělská	španělský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
italská	italský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
2020	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Irina	Irina	k1gFnSc1
Shayk	Shayka	k1gFnPc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
Georgina	Georgina	k1gFnSc1
Rodríguez	Rodrígueza	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2016	#num#	k4
<g/>
)	)	kIx)
Rodiče	rodič	k1gMnPc1
</s>
<s>
José	José	k1gNnSc1
Dinis	Dinis	k1gFnSc1
Aveiro	Aveiro	k1gNnSc1
a	a	k8xC
Maria	Maria	k1gFnSc1
Dolores	Dolores	k1gInSc1
dos	dos	k?
Santos	Santos	k1gInSc1
Viveiros	Viveirosa	k1gFnPc2
da	da	k?
Aveiro	Aveiro	k1gNnSc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Elma	Elma	k1gMnSc1
dos	dos	k?
Santos	Santos	k1gMnSc1
Aveiro	Aveiro	k1gNnSc1
<g/>
,	,	kIx,
Hugo	Hugo	k1gMnSc1
dos	dos	k?
Santos	Santos	k1gInSc1
Aveiro	Aveiro	k1gNnSc1
a	a	k8xC
Kátia	Kátia	k1gFnSc1
Aveiro	Aveiro	k1gNnSc4
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnSc3
<g/>
)	)	kIx)
<g/>
José	Josá	k1gFnSc2
Nunes	Nunes	k1gInSc1
de	de	k?
Viveiros	Viveirosa	k1gFnPc2
(	(	kIx(
<g/>
dědeček	dědeček	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Matilde	Matild	k1gInSc5
dos	dos	k?
Santos	Santosa	k1gFnPc2
da	da	k?
Viveiros	Viveirosa	k1gFnPc2
(	(	kIx(
<g/>
babička	babička	k1gFnSc1
<g/>
)	)	kIx)
<g/>
Humberto	Humberta	k1gFnSc5
Cirilo	Cirila	k1gFnSc5
Aveiro	Aveiro	k1gNnSc1
(	(	kIx(
<g/>
dědeček	dědeček	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Filomena	Filomen	k2eAgFnSc1d1
da	da	k?
Aveiro	Aveiro	k1gNnSc1
(	(	kIx(
<g/>
babička	babička	k1gFnSc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
<g/>
*	*	kIx~
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
za	za	k7c4
klub	klub	k1gInSc4
aktuální	aktuální	k2eAgInSc4d1
k	k	k7c3
17	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
**	**	k?
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
za	za	k7c4
reprezentaci	reprezentace	k1gFnSc4
aktuální	aktuální	k2eAgFnSc4d1
k	k	k7c3
17	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
dos	dos	k?
Santos	Santos	k1gInSc1
Aveiro	Aveiro	k1gNnSc1
(	(	kIx(
<g/>
výslovnost	výslovnost	k1gFnSc1
kɾ	kɾ	k5eAaImIp1nS,k5eAaPmIp1nS
ʁ	ʁ	k6eAd1
<g/>
,	,	kIx,
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1985	#num#	k4
Funchal	Funchal	k1gFnPc2
<g/>
,	,	kIx,
Madeira	Madeira	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
portugalský	portugalský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
a	a	k8xC
reprezentant	reprezentant	k1gMnSc1
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
Evropy	Evropa	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
a	a	k8xC
vítěz	vítěz	k1gMnSc1
premiérové	premiérový	k2eAgFnSc2d1
Ligy	liga	k1gFnSc2
národů	národ	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
započal	započnout	k5eAaPmAgInS
ve	v	k7c6
Sportingu	Sporting	k1gInSc6
Lisabon	Lisabon	k1gInSc1
<g/>
,	,	kIx,
mezi	mezi	k7c7
roky	rok	k1gInPc7
2003	#num#	k4
až	až	k9
2009	#num#	k4
hrál	hrát	k5eAaImAgInS
za	za	k7c4
anglický	anglický	k2eAgInSc4d1
Manchester	Manchester	k1gInSc4
United	United	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odtud	odtud	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
španělského	španělský	k2eAgInSc2d1
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
za	za	k7c4
onehdy	onehdy	k6eAd1
rekordní	rekordní	k2eAgFnSc4d1
částku	částka	k1gFnSc4
80	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
nastupuje	nastupovat	k5eAaImIp3nS
za	za	k7c4
italský	italský	k2eAgInSc4d1
klub	klub	k1gInSc4
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
kam	kam	k6eAd1
přestoupil	přestoupit	k5eAaPmAgMnS
po	po	k7c6
devíti	devět	k4xCc6
letech	léto	k1gNnPc6
v	v	k7c6
Realu	Real	k1gInSc6
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
komplexního	komplexní	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
považovaného	považovaný	k2eAgMnSc4d1
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejlepších	dobrý	k2eAgMnPc2d3
fotbalistů	fotbalista	k1gMnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
kariéry	kariéra	k1gFnSc2
vybojoval	vybojovat	k5eAaPmAgInS
33	#num#	k4
klubových	klubový	k2eAgFnPc2d1
trofejí	trofej	k1gFnPc2
(	(	kIx(
<g/>
platné	platný	k2eAgInPc4d1
k	k	k7c3
lednu	leden	k1gInSc3
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Získal	získat	k5eAaPmAgInS
ocenění	ocenění	k1gNnSc4
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
)	)	kIx)
i	i	k8xC
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
FIFA	FIFA	kA
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
rok	rok	k1gInSc4
2009	#num#	k4
vyhrál	vyhrát	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
Ference	Ferenc	k1gMnSc2
Puskáse	Puskás	k1gMnSc2*xF
o	o	k7c4
nejhezčí	hezký	k2eAgInSc4d3
gól	gól	k1gInSc4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
prosince	prosinec	k1gInSc2
2020	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
750	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kariéře	kariéra	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
jej	on	k3xPp3gInSc4
historicky	historicky	k6eAd1
řadí	řadit	k5eAaImIp3nP
na	na	k7c4
čtvrté	čtvrtý	k4xOgNnSc4
místo	místo	k1gNnSc4
podle	podle	k7c2
Soccer	Soccra	k1gFnPc2
Statistics	Statisticsa	k1gFnPc2
Foundation	Foundation	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
své	svůj	k3xOyFgFnSc2
profesionální	profesionální	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Ronaldo	Ronaldo	k1gNnSc4
odehrál	odehrát	k5eAaPmAgInS
více	hodně	k6eAd2
než	než	k8xS
1000	#num#	k4
soutěžních	soutěžní	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
fotbalistou	fotbalista	k1gMnSc7
s	s	k7c7
pěti	pět	k4xCc7
triumfy	triumf	k1gInPc7
v	v	k7c6
novodobé	novodobý	k2eAgFnSc6d1
éře	éra	k1gFnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2021	#num#	k4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
této	tento	k3xDgFnSc2
evropské	evropský	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
se	se	k3xPyFc4
134	#num#	k4
góly	gól	k1gInPc7
(	(	kIx(
<g/>
135	#num#	k4
góly	gól	k1gInPc4
<g/>
,	,	kIx,
počítají	počítat	k5eAaImIp3nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
kvalifikační	kvalifikační	k2eAgNnPc1d1
předkola	předkolo	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmkrát	sedmkrát	k6eAd1
byl	být	k5eAaImAgInS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
v	v	k7c6
jednom	jeden	k4xCgInSc6
ročníku	ročník	k1gInSc6
Lize	liga	k1gFnSc3
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
jednom	jeden	k4xCgInSc6
případě	případ	k1gInSc6
byl	být	k5eAaImAgInS
na	na	k7c6
děleném	dělený	k2eAgMnSc6d1
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
se	s	k7c7
dvěma	dva	k4xCgMnPc7
dalšími	další	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
v	v	k7c6
přátelském	přátelský	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Kamerunu	Kamerun	k1gInSc3
zaznamenal	zaznamenat	k5eAaPmAgMnS
2	#num#	k4
góly	gól	k1gInPc4
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
se	s	k7c7
49	#num#	k4
brankami	branka	k1gFnPc7
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
portugalské	portugalský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
(	(	kIx(
<g/>
překonal	překonat	k5eAaPmAgMnS
Pauletu	Pauleta	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Společně	společně	k6eAd1
s	s	k7c7
Francouzem	Francouz	k1gMnSc7
Michelem	Michel	k1gMnSc7
Platinim	Platinima	k1gFnPc2
drží	držet	k5eAaImIp3nS
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
devíti	devět	k4xCc2
nastřílených	nastřílený	k2eAgFnPc2d1
branek	branka	k1gFnPc2
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
Evropanem	Evropan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dokázal	dokázat	k5eAaPmAgInS
nastřílet	nastřílet	k5eAaPmF
100	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
gólů	gól	k1gInPc2
a	a	k8xC
jediným	jediný	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
s	s	k7c7
více	hodně	k6eAd2
góly	gól	k1gInPc4
na	na	k7c6
reprezentační	reprezentační	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
zůstává	zůstávat	k5eAaImIp3nS
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2021	#num#	k4
bývalý	bývalý	k2eAgMnSc1d1
íránský	íránský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Ali	Ali	k1gMnSc1
Daeí	Daeí	k1gMnSc1
se	s	k7c7
109	#num#	k4
trefami	trefa	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americký	americký	k2eAgInSc1d1
týdeník	týdeník	k1gInSc1
Time	Tim	k1gInSc2
jej	on	k3xPp3gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
zařadil	zařadit	k5eAaPmAgInS
na	na	k7c4
seznam	seznam	k1gInSc4
stovky	stovka	k1gFnSc2
nejvlivnějších	vlivný	k2eAgFnPc2d3
světových	světový	k2eAgFnPc2d1
osobností	osobnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
třetím	třetí	k4xOgMnSc7
sportovcem	sportovec	k1gMnSc7
a	a	k8xC
prvním	první	k4xOgMnSc7
fotbalistou	fotbalista	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kariéře	kariéra	k1gFnSc6
vydělal	vydělat	k5eAaPmAgInS
v	v	k7c6
přepočtu	přepočet	k1gInSc6
přes	přes	k7c4
miliardu	miliarda	k4xCgFnSc4
amerických	americký	k2eAgInPc2d1
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klubová	klubový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
C.	C.	kA
Ronaldo	Ronaldo	k1gNnSc1
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
United	United	k1gMnSc1
</s>
<s>
Sporting	Sporting	k1gInSc1
CP	CP	kA
</s>
<s>
Memorabilie	memorabilie	k1gFnPc1
Ronalda	Ronaldo	k1gNnSc2
v	v	k7c6
muzeu	muzeum	k1gNnSc6
Sportingu	Sporting	k1gInSc2
</s>
<s>
Když	když	k8xS
bylo	být	k5eAaImAgNnS
Cristianovi	Cristianův	k2eAgMnPc1d1
16	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
manažer	manažer	k1gInSc1
tohoto	tento	k3xDgInSc2
týmu	tým	k1gInSc2
jej	on	k3xPp3gNnSc2
z	z	k7c2
mládežnického	mládežnický	k2eAgInSc2d1
týmu	tým	k1gInSc2
Sportingu	Sporting	k1gInSc2
převzal	převzít	k5eAaPmAgMnS
do	do	k7c2
lepších	dobrý	k2eAgInPc2d2
týmů	tým	k1gInPc2
Sportingu	Sporting	k1gInSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
byl	být	k5eAaImAgInS
ohromen	ohromen	k2eAgInSc1d1
Ronaldovým	Ronaldův	k2eAgNnSc7d1
driblováním	driblování	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Hned	hned	k6eAd1
poté	poté	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
hrál	hrát	k5eAaImAgMnS
za	za	k7c4
týmy	tým	k1gInPc4
U	u	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
16	#num#	k4
<g/>
,	,	kIx,
U	u	k7c2
<g/>
17	#num#	k4
<g/>
,	,	kIx,
U	u	k7c2
<g/>
18	#num#	k4
<g/>
,	,	kIx,
za	za	k7c7
B-tým	B-té	k1gNnSc7
a	a	k8xC
také	také	k9
A-tým	A-tí	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
si	se	k3xPyFc3
dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
zahrál	zahrát	k5eAaPmAgMnS
první	první	k4xOgInSc4
zápas	zápas	k1gInSc4
v	v	k7c4
Primeira	Primeir	k1gMnSc4
Lize	liga	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
týmu	tým	k1gInSc3
Moreirense	Moreirens	k1gMnSc2
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
tehdy	tehdy	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
dvakrát	dvakrát	k6eAd1
skórovat	skórovat	k5eAaBmF
a	a	k8xC
značně	značně	k6eAd1
tak	tak	k6eAd1
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
vítězství	vítězství	k1gNnSc3
Sportingu	Sporting	k1gInSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
průběhu	průběh	k1gInSc6
sezóny	sezóna	k1gFnSc2
2002	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
3	#num#	k4
jej	on	k3xPp3gMnSc4
představitelé	představitel	k1gMnPc1
Sportingu	Sporting	k1gInSc2
nabídli	nabídnout	k5eAaPmAgMnP
tehdejšímu	tehdejší	k2eAgMnSc3d1
manažerovi	manažer	k1gMnSc3
Liverpoolu	Liverpool	k1gInSc2
Gérardu	Gérard	k1gMnSc3
Houllierovi	Houllier	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Zájem	zájem	k1gInSc1
měl	mít	k5eAaImAgMnS
i	i	k9
manažer	manažer	k1gMnSc1
Arsenalu	Arsenal	k1gInSc2
Arsè	Arsè	k1gInSc5
Wenger	Wenger	k1gInSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
chtěl	chtít	k5eAaImAgMnS
do	do	k7c2
klubu	klub	k1gInSc2
získat	získat	k5eAaPmF
nějakého	nějaký	k3yIgMnSc4
křídelního	křídelní	k2eAgMnSc4d1
útočníka	útočník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Ronaldem	Ronaldo	k1gNnSc7
se	se	k3xPyFc4
dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2002	#num#	k4
setkal	setkat	k5eAaPmAgMnS
na	na	k7c6
tréninku	trénink	k1gInSc6
Arsenalu	Arsenal	k1gInSc2
<g/>
,	,	kIx,
aby	aby	k9
případný	případný	k2eAgInSc4d1
přestup	přestup	k1gInSc4
prodiskutovali	prodiskutovat	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
přestupu	přestup	k1gInSc6
se	se	k3xPyFc4
však	však	k9
kvůli	kvůli	k7c3
určitým	určitý	k2eAgFnPc3d1
neshodám	neshoda	k1gFnPc3
nedohodli	dohodnout	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
Sporting	Sporting	k1gInSc1
v	v	k7c6
srpnu	srpen	k1gInSc6
2003	#num#	k4
porazil	porazit	k5eAaPmAgInS
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
při	při	k7c6
slavnostním	slavnostní	k2eAgNnSc6d1
otevření	otevření	k1gNnSc6
stadionu	stadion	k1gInSc2
Estádio	Estádio	k1gNnSc4
José	Josý	k2eAgFnSc2d1
Alvalade	Alvalad	k1gInSc5
skórem	skóre	k1gNnSc7
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
chtěl	chtít	k5eAaImAgMnS
manažer	manažer	k1gMnSc1
Manchesteru	Manchester	k1gInSc2
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
Ronalda	Ronalda	k1gMnSc1
do	do	k7c2
svého	svůj	k3xOyFgInSc2
týmu	tým	k1gInSc2
co	co	k9
nejrychleji	rychle	k6eAd3
a	a	k8xC
byl	být	k5eAaImAgMnS
rozhodnut	rozhodnout	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
získá	získat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
chtěli	chtít	k5eAaImAgMnP
v	v	k7c4
United	United	k1gInSc4
s	s	k7c7
Ronaldem	Ronald	k1gInSc7
jen	jen	k6eAd1
podepsat	podepsat	k5eAaPmF
smlouvu	smlouva	k1gFnSc4
a	a	k8xC
poté	poté	k6eAd1
jej	on	k3xPp3gMnSc4
na	na	k7c4
rok	rok	k1gInSc4
do	do	k7c2
Sportingu	Sporting	k1gInSc2
půjčit	půjčit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
na	na	k7c4
tým	tým	k1gInSc4
Ronaldo	Ronaldo	k1gNnSc4
velice	velice	k6eAd1
zapůsobil	zapůsobit	k5eAaPmAgMnS
<g/>
,	,	kIx,
hráči	hráč	k1gMnPc1
Manchesteru	Manchester	k1gInSc2
na	na	k7c4
Fergusona	Ferguson	k1gMnSc4
zatlačili	zatlačit	k5eAaPmAgMnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
co	co	k9
nejrychleji	rychle	k6eAd3
smlouvu	smlouva	k1gFnSc4
s	s	k7c7
Ronaldem	Ronald	k1gInSc7
podepsal	podepsat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferguson	Ferguson	k1gMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
se	s	k7c7
Sportingem	Sporting	k1gInSc7
a	a	k8xC
Ronalda	Ronalda	k1gMnSc1
podepsal	podepsat	k5eAaPmAgMnS
za	za	k7c2
tehdy	tehdy	k6eAd1
rekordních	rekordní	k2eAgInPc2d1
12,24	12,24	k4
millionů	million	k1gInPc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
Ronaldovi	Ronald	k1gMnSc6
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nezajímavějších	zajímavý	k2eNgMnPc2d2
mladých	mladý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
kdy	kdy	k6eAd1
viděl	vidět	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Deset	deset	k4xCc4
let	léto	k1gNnPc2
po	po	k7c6
svém	svůj	k3xOyFgInSc6
odchodu	odchod	k1gInSc6
z	z	k7c2
klubu	klub	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
v	v	k7c6
dubnu	duben	k1gInSc6
2013	#num#	k4
Ronalda	Ronalda	k1gMnSc1
Sporting	Sporting	k1gInSc4
vyznamenal	vyznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
uznal	uznat	k5eAaPmAgMnS
za	za	k7c4
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
.	.	kIx.
člena	člen	k1gMnSc2
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
</s>
<s>
2003	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
Vývoj	vývoj	k1gInSc1
a	a	k8xC
průlom	průlom	k1gInSc1
</s>
<s>
Byl	být	k5eAaImAgInS
úplně	úplně	k6eAd1
prvním	první	k4xOgMnSc7
portugalským	portugalský	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
za	za	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gMnSc1
hrál	hrát	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Přestupní	přestupní	k2eAgInSc1d1
poplatek	poplatek	k1gInSc1
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
12,24	12,24	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
jej	on	k3xPp3gMnSc4
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
nejdražším	drahý	k2eAgMnSc7d3
teenagerem	teenager	k1gMnSc7
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
fotbalové	fotbalový	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
I	i	k9
přesto	přesto	k8xC
žádal	žádat	k5eAaImAgMnS
číslo	číslo	k1gNnSc4
dresu	dres	k1gInSc2
28	#num#	k4
<g/>
,	,	kIx,
stejné	stejný	k2eAgFnPc4d1
jako	jako	k8xC,k8xS
měl	mít	k5eAaImAgMnS
ve	v	k7c6
Sportingu	Sporting	k1gInSc6
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
dostal	dostat	k5eAaPmAgMnS
dres	dres	k1gInSc4
s	s	k7c7
číslem	číslo	k1gNnSc7
7	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
v	v	k7c4
United	United	k1gInSc4
hráli	hrát	k5eAaImAgMnP
například	například	k6eAd1
George	George	k1gNnSc4
Best	Besta	k1gFnPc2
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
Cantona	Cantona	k1gFnSc1
and	and	k?
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Nošení	nošení	k1gNnSc2
dresu	dres	k1gInSc2
s	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
číslem	číslo	k1gNnSc7
7	#num#	k4
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
pro	pro	k7c4
Ronalda	Ronaldo	k1gNnSc2
bonusovým	bonusový	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
motivace	motivace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Klíčovým	klíčový	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vývoje	vývoj	k1gInSc2
během	během	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
angažmá	angažmá	k1gNnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
být	být	k5eAaImF
jeho	jeho	k3xOp3gMnSc1
manažer	manažer	k1gMnSc1
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
později	pozdě	k6eAd2
řekl	říct	k5eAaPmAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
He	he	k0
<g/>
'	'	kIx"
<g/>
s	s	k7c7
been	been	k1gInSc1
my	my	k3xPp1nPc1
father	fathra	k1gFnPc2
in	in	k?
sport	sport	k1gInSc1
<g/>
,	,	kIx,
one	one	k?
of	of	k?
the	the	k?
most	most	k1gInSc1
important	important	k1gMnSc1
and	and	k?
influential	influentiat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
factors	factors	k1gInSc4
in	in	k?
my	my	k3xPp1nPc1
career	carerat	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
hrající	hrající	k2eAgNnSc1d1
z	z	k7c2
Manchester	Manchester	k1gInSc1
United	United	k1gInSc1
proti	proti	k7c3
Chelsea	Chelsea	k1gMnSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
<g/>
6	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1
debut	debut	k1gInSc1
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gMnSc2
si	se	k3xPyFc3
odčinil	odčinit	k5eAaPmAgMnS
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2003	#num#	k4
v	v	k7c6
domácím	domácí	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Bolton	Bolton	k1gInSc1
Wanderers	Wanderers	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
United	United	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
tehdy	tehdy	k6eAd1
hrál	hrát	k5eAaImAgInS
až	až	k6eAd1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
minuty	minuta	k1gFnPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nahradil	nahradit	k5eAaPmAgInS
Nickyho	Nicky	k1gMnSc4
Butta	Butt	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Svým	svůj	k3xOyFgInSc7
výkonem	výkon	k1gInSc7
si	se	k3xPyFc3
zasloužil	zasloužit	k5eAaPmAgMnS
chválu	chvála	k1gFnSc4
od	od	k7c2
George	Georg	k1gMnSc2
Besta	Best	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
Ronaldův	Ronaldův	k2eAgInSc1d1
debut	debut	k1gInSc1
označil	označit	k5eAaPmAgInS
za	za	k7c4
„	„	k?
<g/>
nepochybně	pochybně	k6eNd1
nejúžasnější	úžasný	k2eAgInSc1d3
debut	debut	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jaký	jaký	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
kdy	kdy	k6eAd1
viděl	vidět	k5eAaImAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
gól	gól	k1gInSc4
za	za	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2003	#num#	k4
volným	volný	k2eAgInSc7d1
kopem	kop	k1gInSc7
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
Portsmouth	Portsmouth	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
United	United	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
vyhráli	vyhrát	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
sezóny	sezóna	k1gFnSc2
přišly	přijít	k5eAaPmAgInP
další	další	k2eAgInPc1d1
tři	tři	k4xCgInPc1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
poslední	poslední	k2eAgMnSc1d1
v	v	k7c4
den	den	k1gInSc4
ukončení	ukončení	k1gNnSc2
sezóny	sezóna	k1gFnSc2
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
týmu	tým	k1gInSc3
Aston	Aston	k1gMnSc1
Villa	Villa	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
také	také	k6eAd1
v	v	k7c6
tomto	tento	k3xDgInSc6
zápase	zápas	k1gInSc6
obdržel	obdržet	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
červenou	červený	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
v	v	k7c6
Anglii	Anglie	k1gFnSc6
zakončil	zakončit	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
prvním	první	k4xOgInSc7
gólem	gól	k1gInSc7
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
Millwall	Millwall	k1gMnSc1
ve	v	k7c6
květnovém	květnový	k2eAgNnSc6d1
finále	finále	k1gNnSc6
Poháru	pohár	k1gInSc2
FA	fa	kA
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
Ronaldova	Ronaldův	k2eAgFnSc1d1
první	první	k4xOgFnSc1
trofej	trofej	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
odehrál	odehrát	k5eAaPmAgInS
své	svůj	k3xOyFgMnPc4
dva	dva	k4xCgInPc4
z	z	k7c2
nejlepších	dobrý	k2eAgInPc2d3
zápasů	zápas	k1gInPc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
:	:	kIx,
v	v	k7c6
prvním	první	k4xOgInSc6
dal	dát	k5eAaPmAgInS
proti	proti	k7c3
Aston	Aston	k1gInSc1
Ville	Ville	k1gNnSc4
jeden	jeden	k4xCgInSc1
gól	gól	k1gInSc1
a	a	k8xC
jednou	jeden	k4xCgFnSc7
asistoval	asistovat	k5eAaImAgMnS
<g/>
,	,	kIx,
v	v	k7c6
druhém	druhý	k4xOgMnSc6
pak	pak	k6eAd1
dvakrát	dvakrát	k6eAd1
proti	proti	k7c3
Arsenalu	Arsenal	k1gInSc3
skóroval	skórovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
40	#num#	k4
<g/>
]	]	kIx)
Hrál	hrát	k5eAaImAgMnS
celých	celý	k2eAgFnPc2d1
120	#num#	k4
minut	minuta	k1gFnPc2
rozhodujícího	rozhodující	k2eAgNnSc2d1
utkání	utkání	k1gNnSc2
proti	proti	k7c3
Arsenalu	Arsenal	k1gInSc3
ve	v	k7c6
finále	finále	k1gNnSc6
FA	fa	k1gNnSc2
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
skončil	skončit	k5eAaPmAgInS
bezbrankovou	bezbrankový	k2eAgFnSc7d1
remízou	remíza	k1gFnSc7
a	a	k8xC
vsítil	vsítit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
pokus	pokus	k1gInSc4
v	v	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
však	však	k8xC
United	United	k1gInSc4
prohráli	prohrát	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
vstřelil	vstřelit	k5eAaPmAgMnS
jubilejní	jubilejní	k2eAgInSc4d1
gól	gól	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
číslo	číslo	k1gNnSc1
1	#num#	k4
000	#num#	k4
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
jediný	jediný	k2eAgInSc1d1
úspěšný	úspěšný	k2eAgInSc1d1
pokus	pokus	k1gInSc1
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
prohře	prohra	k1gFnSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
proti	proti	k7c3
Middlesbrough	Middlesbrough	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
sezóny	sezóna	k1gFnSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
podepsal	podepsat	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tu	tu	k6eAd1
předchozí	předchozí	k2eAgFnSc1d1
prodloužila	prodloužit	k5eAaPmAgFnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
tj.	tj.	kA
do	do	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
trofej	trofej	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
vítězství	vítězství	k1gNnSc1
Manchesteru	Manchester	k1gInSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Wiganem	Wigan	k1gInSc7
Athletic	Athletice	k1gFnPc2
ve	v	k7c4
Football	Football	k1gInSc4
League	League	k1gInSc1
Cup	cup	k1gInSc1
si	se	k3xPyFc3
zasloužil	zasloužit	k5eAaPmAgInS
vsítění	vsítění	k1gNnSc3
třetího	třetí	k4xOgInSc2
gólu	gól	k1gInSc2
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
třetí	třetí	k4xOgFnSc2
sezóny	sezóna	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
se	se	k3xPyFc4
zapletl	zaplést	k5eAaPmAgMnS
do	do	k7c2
několika	několik	k4yIc2
konfliktů	konflikt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
fanoušky	fanoušek	k1gMnPc4
týmu	tým	k1gInSc2
Benfica	Benfica	k1gMnSc1
„	„	k?
<g/>
zdvihnul	zdvihnout	k5eAaPmAgMnS
prostředník	prostředník	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
dostal	dostat	k5eAaPmAgMnS
od	od	k7c2
federace	federace	k1gFnSc2
UEFA	UEFA	kA
distanc	distanc	k?
po	po	k7c4
dobu	doba	k1gFnSc4
jednoho	jeden	k4xCgInSc2
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
v	v	k7c6
Manchesterském	manchesterský	k2eAgNnSc6d1
derby	derby	k1gNnSc6
(	(	kIx(
<g/>
United	United	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
prohrál	prohrát	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
kopl	kopnout	k5eAaPmAgMnS
do	do	k7c2
hráče	hráč	k1gMnSc2
Manchester	Manchester	k1gInSc4
City	city	k1gNnSc2
Andyho	Andy	k1gMnSc2
Colea	Coleus	k1gMnSc2
–	–	k?
bývalého	bývalý	k2eAgMnSc2d1
hráče	hráč	k1gMnSc2
United	United	k1gInSc1
–	–	k?
byl	být	k5eAaImAgInS
po	po	k7c6
červené	červený	k2eAgFnSc6d1
kartě	karta	k1gFnSc6
poslán	poslat	k5eAaPmNgMnS
ze	z	k7c2
hřiště	hřiště	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
se	se	k3xPyFc4
pohádal	pohádat	k5eAaPmAgMnS
se	s	k7c7
spoluhráčem	spoluhráč	k1gMnSc7
útočníkem	útočník	k1gMnSc7
Ruudem	Ruud	k1gMnSc7
van	vana	k1gFnPc2
Nistelrooyem	Nistelrooyem	k1gInSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgMnSc3
vadil	vadit	k5eAaImAgInS
Ronaldův	Ronaldův	k2eAgInSc4d1
styl	styl	k1gInSc4
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yIgNnSc7,k3yRgNnSc7
se	se	k3xPyFc4
podle	podle	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
něj	on	k3xPp3gMnSc4
předváděl	předvádět	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
2006	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc1
zapojil	zapojit	k5eAaPmAgInS
do	do	k7c2
incidentu	incident	k1gInSc2
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgNnSc6,k3yQgNnSc6,k3yIgNnSc6
jeho	jeho	k3xOp3gMnSc1
klubový	klubový	k2eAgMnSc1d1
spoluhráč	spoluhráč	k1gMnSc1
Wayne	Wayn	k1gInSc5
Rooney	Roonea	k1gFnSc2
obdržel	obdržet	k5eAaPmAgInS
červenou	červený	k2eAgFnSc4d1
kartu	karta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Ronaldo	Ronaldo	k1gNnSc4
veřejně	veřejně	k6eAd1
požádal	požádat	k5eAaPmAgMnS
o	o	k7c4
přestup	přestup	k1gInSc4
a	a	k8xC
bědoval	bědovat	k5eAaImAgMnS
na	na	k7c4
nedostatek	nedostatek	k1gInSc4
podpory	podpora	k1gFnSc2
zástupců	zástupce	k1gMnPc2
klubu	klub	k1gInSc2
v	v	k7c6
tomto	tento	k3xDgInSc6
sporu	spor	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Zástupci	zástupce	k1gMnPc1
United	United	k1gMnSc1
však	však	k9
tuto	tento	k3xDgFnSc4
možnost	možnost	k1gFnSc4
zamítli	zamítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInPc4
problémy	problém	k1gInPc4
s	s	k7c7
Rooneym	Rooneymum	k1gNnPc2
vedly	vést	k5eAaImAgFnP
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgMnS
během	během	k7c2
sezóny	sezóna	k1gFnSc2
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
vypískán	vypískán	k2eAgInSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
tato	tento	k3xDgFnSc1
sezóna	sezóna	k1gFnSc1
vyjevila	vyjevit	k5eAaPmAgFnS
jako	jako	k9
jeho	jeho	k3xOp3gFnSc1
přelomová	přelomový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
překonal	překonat	k5eAaPmAgMnS
hranici	hranice	k1gFnSc4
20	#num#	k4
gólů	gól	k1gInPc2
a	a	k8xC
byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gFnSc1
první	první	k4xOgFnSc1
sezóna	sezóna	k1gFnSc1
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
jeho	jeho	k3xOp3gMnSc1
tým	tým	k1gInSc4
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
na	na	k7c6
těchto	tento	k3xDgInPc6
úspěších	úspěch	k1gInPc6
měl	mít	k5eAaImAgMnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
individuální	individuální	k2eAgInSc4d1
trénink	trénink	k1gInSc4
s	s	k7c7
trenérem	trenér	k1gMnSc7
René	René	k1gMnSc1
Meulensteenem	Meulensteen	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
jej	on	k3xPp3gMnSc4
naučil	naučit	k5eAaPmAgMnS
být	být	k5eAaImF
ještě	ještě	k9
více	hodně	k6eAd2
nepředvídatelným	předvídatelný	k2eNgMnPc3d1
<g/>
,	,	kIx,
pomohl	pomoct	k5eAaPmAgInS
zlepšit	zlepšit	k5eAaPmF
Ronaldovu	Ronaldův	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
a	a	k8xC
Ronaldo	Ronaldo	k1gNnSc4
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
co	co	k9
nejvíce	hodně	k6eAd3,k6eAd1
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
příležitost	příležitost	k1gFnSc4
vsítit	vsítit	k5eAaPmF
branku	branka	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
aby	aby	kYmCp3nS
dával	dávat	k5eAaImAgInS
esteticky	esteticky	k6eAd1
krásné	krásný	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
byl	být	k5eAaImAgInS
již	již	k9
tehdy	tehdy	k6eAd1
znám	znám	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
třech	tři	k4xCgInPc6
prosincových	prosincový	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
po	po	k7c6
sobě	sebe	k3xPyFc6
dal	dát	k5eAaPmAgMnS
po	po	k7c6
dvou	dva	k4xCgInPc6
gólech	gól	k1gInPc6
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
proti	proti	k7c3
Aston	Aston	k1gInSc1
Ville	Villa	k1gFnSc3
(	(	kIx(
<g/>
díky	díky	k7c3
této	tento	k3xDgFnSc3
výhře	výhra	k1gFnSc3
se	se	k3xPyFc4
United	United	k1gMnSc1
chopilo	chopit	k5eAaPmAgNnS
první	první	k4xOgFnPc4
příčky	příčka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
proti	proti	k7c3
Wiganu	Wigan	k1gInSc3
a	a	k8xC
nakonec	nakonec	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
proti	proti	k7c3
Readingu	Reading	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
a	a	k8xC
prosinci	prosinec	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hráčem	hráč	k1gMnSc7
měsíce	měsíc	k1gInSc2
Premier	Premier	k1gInSc4
League	League	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
třetím	třetí	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
toto	tento	k3xDgNnSc4
ocenění	ocenění	k1gNnSc4
dokázal	dokázat	k5eAaPmAgMnS
získat	získat	k5eAaPmF
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
po	po	k7c6
sobě	se	k3xPyFc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
Kolektivní	kolektivní	k2eAgNnSc1d1
and	and	k?
individuální	individuální	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
během	během	k7c2
zápasu	zápas	k1gInSc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
Premier	Premiero	k1gNnPc2
League	League	k1gFnPc2
</s>
<s>
Ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
své	svůj	k3xOyFgInPc4
první	první	k4xOgInPc4
góly	gól	k1gInPc4
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
třicátém	třicátý	k4xOgInSc6
zápase	zápas	k1gInSc6
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
dvakrát	dvakrát	k6eAd1
strefil	strefit	k5eAaPmAgMnS
a	a	k8xC
pomohl	pomoct	k5eAaPmAgMnS
vítězství	vítězství	k1gNnSc4
United	United	k1gInSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
proti	proti	k7c3
AS	as	k1gNnSc3
Řím	Řím	k1gInSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
prvním	první	k4xOgInSc6
zápase	zápas	k1gInSc6
semifinále	semifinále	k1gNnSc2
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
strefil	strefit	k5eAaPmAgInS
hned	hned	k6eAd1
ve	v	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
minutě	minuta	k1gFnSc6
proti	proti	k7c3
AC	AC	kA
Milán	Milán	k1gInSc1
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pro	pro	k7c4
United	United	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
v	v	k7c6
odvetném	odvetný	k2eAgInSc6d1
zápase	zápas	k1gInSc6
United	United	k1gMnSc1
prohrál	prohrát	k5eAaPmAgMnS
0	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
United	United	k1gMnSc1
také	také	k9
pomohl	pomoct	k5eAaPmAgMnS
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
květnového	květnový	k2eAgNnSc2d1
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
FA	fa	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
však	však	k9
United	United	k1gInSc1
podlehl	podlehnout	k5eAaPmAgInS
Chelsea	Chelse	k1gInSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
jediný	jediný	k2eAgMnSc1d1
gól	gól	k1gInSc4
Manchesterského	manchesterský	k2eAgNnSc2d1
derby	derby	k1gNnSc2
ve	v	k7c6
finále	finále	k1gNnSc6
Premier	Premira	k1gFnPc2
League	League	k1gFnSc1
a	a	k8xC
Manchester	Manchester	k1gInSc1
tak	tak	k6eAd1
po	po	k7c6
čtyřech	čtyři	k4xCgNnPc6
letech	léto	k1gNnPc6
dosáhl	dosáhnout	k5eAaPmAgInS
na	na	k7c4
titul	titul	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
také	také	k9
Ronaldův	Ronaldův	k2eAgInSc1d1
50	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
za	za	k7c4
United	United	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
své	svůj	k3xOyFgInPc4
výkony	výkon	k1gInPc4
v	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
získal	získat	k5eAaPmAgInS
vícero	vícero	k1gNnSc4
individuálních	individuální	k2eAgFnPc2d1
ocenění	ocenění	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
hráčem	hráč	k1gMnSc7
roku	rok	k1gInSc2
PFA	PFA	kA
<g/>
,	,	kIx,
fanouškovským	fanouškovský	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
roku	rok	k1gInSc2
PFA	PFA	kA
<g/>
,	,	kIx,
mladým	mladý	k2eAgMnSc7d1
hráčem	hráč	k1gMnSc7
roku	rok	k1gInSc2
PFA	PFA	kA
a	a	k8xC
také	také	k9
hráčem	hráč	k1gMnSc7
roku	rok	k1gInSc2
FWA	FWA	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
získal	získat	k5eAaPmAgMnS
všechna	všechen	k3xTgNnPc4
tato	tento	k3xDgNnPc4
ocenění	ocenění	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc1
plat	plat	k1gInSc1
zvedl	zvednout	k5eAaPmAgInS
na	na	k7c4
120	#num#	k4
000	#num#	k4
liber	libra	k1gFnPc2
týdně	týdně	k6eAd1
(	(	kIx(
<g/>
31	#num#	k4
milionů	milion	k4xCgInPc2
celkově	celkově	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
kvůli	kvůli	k7c3
pětiročního	pětiroční	k2eAgNnSc2d1
prodloužení	prodloužení	k1gNnSc3
smlouvy	smlouva	k1gFnSc2
s	s	k7c7
United	United	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2007	#num#	k4
byl	být	k5eAaImAgMnS
druhý	druhý	k4xOgMnSc1
za	za	k7c7
fotbalistou	fotbalista	k1gMnSc7
týmu	tým	k1gInSc2
AC	AC	kA
Milán	Milán	k1gInSc1
jménem	jméno	k1gNnSc7
Kaká	kakat	k5eAaImIp3nS
v	v	k7c6
anketě	anketa	k1gFnSc6
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
(	(	kIx(
<g/>
Ballon	Ballon	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Také	také	k6eAd1
byl	být	k5eAaImAgInS
za	za	k7c4
Kaká	kakat	k5eAaImIp3nS
a	a	k8xC
Messim	Messim	k1gMnSc1
třetí	třetí	k4xOgMnSc1
v	v	k7c6
anketě	anketa	k1gFnSc6
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
skóroval	skórovat	k5eAaBmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
a	a	k8xC
také	také	k9
jediný	jediný	k2eAgInSc4d1
hattrick	hattrick	k1gInSc4
za	za	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gInSc4
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
Newcastlu	Newcastl	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
United	United	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	on	k3xPp3gMnPc4
posunulo	posunout	k5eAaPmAgNnS
na	na	k7c4
čelo	čelo	k1gNnSc4
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
hrál	hrát	k5eAaImAgMnS
první	první	k4xOgInSc4
zápas	zápas	k1gInSc4
za	za	k7c4
United	United	k1gInSc4
jako	jako	k9
kapitán	kapitán	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
Boltonu	Bolton	k1gInSc3
vstřelil	vstřelit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
jediné	jediný	k2eAgInPc4d1
góly	gól	k1gInPc4
zápasu	zápas	k1gInSc2
a	a	k8xC
United	United	k1gMnSc1
tak	tak	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Druhý	druhý	k4xOgInSc1
z	z	k7c2
těchto	tento	k3xDgInPc2
gólů	gól	k1gInPc2
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc7
33	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
sezóně	sezóna	k1gFnSc6
a	a	k8xC
překonal	překonat	k5eAaPmAgMnS
tak	tak	k6eAd1
32	#num#	k4
gólů	gól	k1gInPc2
George	Georg	k1gMnSc2
Besta	Best	k1gMnSc2
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
zvýšil	zvýšit	k5eAaPmAgMnS
hranici	hranice	k1gFnSc4
klubového	klubový	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
pro	pro	k7c4
nejvíce	hodně	k6eAd3,k6eAd1
gólů	gól	k1gInPc2
hráče	hráč	k1gMnPc4
na	na	k7c4
pozici	pozice	k1gFnSc4
záložníka	záložník	k1gMnSc2
za	za	k7c4
jednu	jeden	k4xCgFnSc4
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gInSc1
31	#num#	k4
<g/>
.	.	kIx.
ligových	ligový	k2eAgInPc2d1
gólů	gól	k1gInPc2
mu	on	k3xPp3gMnSc3
přineslo	přinést	k5eAaPmAgNnS
Zlatou	zlatý	k2eAgFnSc4d1
kopačku	kopačka	k1gFnSc4
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
střelce	střelec	k1gMnSc4
Premier	Premier	k1gMnSc1
League	Leagu	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
i	i	k9
evropskou	evropský	k2eAgFnSc4d1
Zlatou	zlatý	k2eAgFnSc4d1
kopačku	kopačka	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgNnSc7
křídlem	křídlo	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
toto	tento	k3xDgNnSc4
ocenění	ocenění	k1gNnSc4
získal	získat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
již	již	k9
druhou	druhý	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
v	v	k7c6
řadě	řada	k1gFnSc6
získal	získat	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
hráče	hráč	k1gMnSc2
roku	rok	k1gInSc2
Premier	Premiero	k1gNnPc2
League	Leagu	k1gInSc2
a	a	k8xC
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
FWA	FWA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
vyřazovací	vyřazovací	k2eAgFnSc6d1
části	část	k1gFnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
vstřelil	vstřelit	k5eAaPmAgMnS
rozhodující	rozhodující	k2eAgInSc4d1
gól	gól	k1gInSc4
proti	proti	k7c3
Lyonu	Lyon	k1gInSc3
<g/>
,	,	kIx,
kterým	který	k3yRgInPc3,k3yIgInPc3,k3yQgInPc3
pomohl	pomoct	k5eAaPmAgInS
United	United	k1gInSc1
dostat	dostat	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
(	(	kIx(
<g/>
konečný	konečný	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
proti	proti	k7c3
Lyonu	Lyon	k1gInSc3
byl	být	k5eAaImAgInS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
pak	pak	k6eAd1
dal	dát	k5eAaPmAgInS
gól	gól	k1gInSc4
hlavičkou	hlavička	k1gFnSc7
a	a	k8xC
pomohl	pomoct	k5eAaPmAgInS
tak	tak	k9
k	k	k7c3
celkové	celkový	k2eAgFnSc3d1
výhře	výhra	k1gFnSc3
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Římem	Řím	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
United	United	k1gInSc1
se	se	k3xPyFc4
dostali	dostat	k5eAaPmAgMnP
až	až	k9
do	do	k7c2
finále	finále	k1gNnSc2
proti	proti	k7c3
Chelsea	Chelse	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
hrálo	hrát	k5eAaImAgNnS
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
úvodní	úvodní	k2eAgInSc4d1
gól	gól	k1gInSc4
zápasu	zápas	k1gInSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
v	v	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
svůj	svůj	k3xOyFgInSc4
pokus	pokus	k1gInSc4
neproměnil	proměnit	k5eNaPmAgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
United	United	k1gInSc1
ale	ale	k8xC
i	i	k9
tak	tak	k9
finále	finále	k1gNnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Jelikož	jelikož	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
ročníku	ročník	k1gInSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
nastřílel	nastřílet	k5eAaPmAgInS
nejvíce	hodně	k6eAd3,k6eAd1
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
obdržel	obdržet	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
Klubový	klubový	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
podle	podle	k7c2
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
sezóny	sezóna	k1gFnSc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
soutěžích	soutěž	k1gFnPc6
celkově	celkově	k6eAd1
42	#num#	k4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
tato	tento	k3xDgFnSc1
sezóna	sezóna	k1gFnSc1
byla	být	k5eAaImAgFnS
za	za	k7c4
dobu	doba	k1gFnSc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
působení	působení	k1gNnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
z	z	k7c2
hlediska	hledisko	k1gNnSc2
gólů	gól	k1gInPc2
nejlepší	dobrý	k2eAgMnPc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
na	na	k7c6
začátku	začátek	k1gInSc6
sezóny	sezóna	k1gFnSc2
úmyslně	úmyslně	k6eAd1
narazil	narazit	k5eAaPmAgMnS
hlavou	hlava	k1gFnSc7
do	do	k7c2
hráče	hráč	k1gMnSc2
Portsmouthu	Portsmouth	k1gInSc2
měl	mít	k5eAaImAgInS
třízápasový	třízápasový	k2eAgInSc1d1
distanc	distanc	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deníku	deník	k1gInSc2
Daily	Daila	k1gFnSc2
Mail	mail	k1gInSc1
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ponaučil	ponaučit	k5eAaPmAgInS
a	a	k8xC
že	že	k8xS
jej	on	k3xPp3gMnSc4
již	již	k6eAd1
žádný	žádný	k3yNgMnSc1
hráč	hráč	k1gMnSc1
nevyprovokuje	vyprovokovat	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
šířily	šířit	k5eAaImAgFnP
spekulace	spekulace	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c4
Ronalda	Ronald	k1gMnSc4
má	mít	k5eAaImIp3nS
zájem	zájem	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
podali	podat	k5eAaPmAgMnP
zástupci	zástupce	k1gMnPc1
United	United	k1gMnSc1
řídícímu	řídící	k2eAgMnSc3d1
orgánu	orgán	k1gMnSc3
FIFA	FIFA	kA
stížnost	stížnost	k1gFnSc4
kvůli	kvůli	k7c3
neoprávněné	oprávněný	k2eNgFnSc3d1
<g />
.	.	kIx.
</s>
<s hack="1">
manipulaci	manipulace	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
FIFA	FIFA	kA
jakkoliv	jakkoliv	k6eAd1
odmítla	odmítnout	k5eAaPmAgFnS
konat	konat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Prezident	prezident	k1gMnSc1
FIFA	FIFA	kA
Sepp	Sepp	k1gMnSc1
Blatter	Blatter	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
hráč	hráč	k1gMnSc1
měl	mít	k5eAaImAgMnS
dovoleno	dovolit	k5eAaPmNgNnS
odejít	odejít	k5eAaPmF
ze	z	k7c2
svého	svůj	k3xOyFgInSc2
klubu	klub	k1gInSc2
a	a	k8xC
tuto	tento	k3xDgFnSc4
situaci	situace	k1gFnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c2
„	„	k?
<g/>
moderní	moderní	k2eAgNnSc1d1
otroctví	otroctví	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
I	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
Ronaldo	Ronaldo	k1gNnSc1
veřejně	veřejně	k6eAd1
s	s	k7c7
Blatterem	Blatter	k1gMnSc7
souhlasil	souhlasit	k5eAaImAgMnS
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
zůstal	zůstat	k5eAaPmAgInS
v	v	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gInSc4
ještě	ještě	k9
další	další	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
Poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
za	za	k7c4
Manchester	Manchester	k1gInSc4
United	United	k1gMnSc1
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
v	v	k7c6
zápasu	zápas	k1gInSc6
proti	proti	k7c3
Liverpoolu	Liverpool	k1gInSc3
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
poslední	poslední	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
United	United	k1gInSc4
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
sezónou	sezóna	k1gFnSc7
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
podstoupil	podstoupit	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
operaci	operace	k1gFnSc4
kotníku	kotník	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
jej	on	k3xPp3gMnSc4
mimo	mimo	k7c4
zápasy	zápas	k1gInPc4
udržela	udržet	k5eAaPmAgFnS
po	po	k7c4
dobu	doba	k1gFnSc4
10	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
návratu	návrat	k1gInSc6
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc6
vstřelil	vstřelit	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
Stoke	Stoke	k1gNnSc3
City	City	k1gFnSc2
svůj	svůj	k3xOyFgInSc4
stý	stý	k4xOgInSc4
gól	gól	k1gInSc4
ve	v	k7c6
všech	všecek	k3xTgFnPc6
soutěžích	soutěž	k1gFnPc6
za	za	k7c4
Manchester	Manchester	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
zároveň	zároveň	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
skórovat	skórovat	k5eAaBmF
proti	proti	k7c3
všem	všecek	k3xTgMnPc3
19	#num#	k4
týmům	tým	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
hrály	hrát	k5eAaImAgFnP
v	v	k7c4
Premier	Premier	k1gInSc4
League	Leagu	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
konci	konec	k1gInSc3
roku	rok	k1gInSc2
pomohl	pomoct	k5eAaPmAgInS
United	United	k1gInSc1
vyhrát	vyhrát	k5eAaPmF
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
2008	#num#	k4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
finále	finále	k1gNnSc6
asistoval	asistovat	k5eAaImAgInS
gól	gól	k1gInSc4
proti	proti	k7c3
LDU	LDU	kA
Quito	Quito	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1968	#num#	k4
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
jej	on	k3xPp3gMnSc4
získal	získat	k5eAaPmAgMnS
George	Georg	k1gMnSc2
Best	Best	k1gMnSc1
<g/>
)	)	kIx)
prvním	první	k4xOgMnSc6
hráčem	hráč	k1gMnSc7
United	United	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
získal	získat	k5eAaPmAgInS
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Také	také	k6eAd1
byl	být	k5eAaImAgMnS
druhým	druhý	k4xOgMnSc7
fotbalistou	fotbalista	k1gMnSc7
z	z	k7c2
Portugalska	Portugalsko	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
vítězný	vítězný	k2eAgInSc1d1
gól	gól	k1gInSc1
v	v	k7c6
odvetném	odvetný	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Portu	porto	k1gNnSc3
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
ze	z	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
36	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
mu	on	k3xPp3gMnSc3
přinesl	přinést	k5eAaPmAgInS
cenu	cena	k1gFnSc4
Ference	Ferenc	k1gMnSc2
Puskáse	Puskáse	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
udělována	udělován	k2eAgFnSc1d1
autorovi	autor	k1gMnSc3
nejhezčího	hezký	k2eAgInSc2d3
gólu	gól	k1gInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dubnu	duben	k1gInSc6
2009	#num#	k4
pak	pak	k6eAd1
tento	tento	k3xDgInSc4
gól	gól	k1gInSc4
<g />
.	.	kIx.
</s>
<s hack="1">
označil	označit	k5eAaPmAgMnS
za	za	k7c4
nejlepší	dobrý	k2eAgInSc4d3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
kdy	kdy	k6eAd1
vsítil	vsítit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Tým	tým	k1gInSc1
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
finále	finále	k1gNnSc2
hraného	hraný	k2eAgNnSc2d1
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
však	však	k9
prohrál	prohrát	k5eAaPmAgInS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Anglie	Anglie	k1gFnSc2
odešel	odejít	k5eAaPmAgMnS
se	s	k7c7
třemi	tři	k4xCgNnPc7
vítězstvími	vítězství	k1gNnPc7
Premier	Premier	k1gInSc1
League	Leagu	k1gInPc1
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dvěma	dva	k4xCgNnPc7
vítězstvími	vítězství	k1gNnPc7
v	v	k7c6
anglickém	anglický	k2eAgInSc6d1
poháru	pohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
poslední	poslední	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
zde	zde	k6eAd1
zakončil	zakončit	k5eAaPmAgMnS
s	s	k7c7
26	#num#	k4
góly	gól	k1gInPc7
ve	v	k7c6
všech	všecek	k3xTgFnPc6
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
o	o	k7c4
16	#num#	k4
gólů	gól	k1gInPc2
méně	málo	k6eAd2
než	než	k8xS
v	v	k7c6
roce	rok	k1gInSc6
předešlém	předešlý	k2eAgInSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Naposledy	naposledy	k6eAd1
za	za	k7c4
United	United	k1gInSc4
skóroval	skórovat	k5eAaBmAgMnS
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
Manchesterském	manchesterský	k2eAgNnSc6d1
derby	derby	k1gNnSc6
na	na	k7c6
půdě	půda	k1gFnSc6
United	United	k1gMnSc1
proměnil	proměnit	k5eAaPmAgMnS
volný	volný	k2eAgInSc4d1
kop	kop	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
svou	svůj	k3xOyFgFnSc4
celou	celý	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
v	v	k7c4
United	United	k1gInSc4
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
292	#num#	k4
zápasech	zápas	k1gInPc6
<g/>
,	,	kIx,
dal	dát	k5eAaPmAgMnS
118	#num#	k4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
69	#num#	k4
<g/>
krát	krát	k6eAd1
asistoval	asistovat	k5eAaImAgMnS
<g/>
,	,	kIx,
na	na	k7c4
konto	konto	k1gNnSc4
si	se	k3xPyFc3
připsal	připsat	k5eAaPmAgInS
38	#num#	k4
žlutých	žlutý	k2eAgFnPc2d1
a	a	k8xC
dvě	dva	k4xCgFnPc4
červené	červená	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
</s>
<s>
V	v	k7c6
létě	léto	k1gNnSc6
2009	#num#	k4
přestoupil	přestoupit	k5eAaPmAgMnS
za	za	k7c4
80	#num#	k4
milionů	milion	k4xCgInPc2
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
94	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
)	)	kIx)
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
což	což	k9
ho	on	k3xPp3gMnSc4
do	do	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
činilo	činit	k5eAaImAgNnS
nejdražším	drahý	k2eAgInSc7d3
hráčem	hráč	k1gMnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
2013	#num#	k4
ho	on	k3xPp3gNnSc4
předčil	předčít	k5eAaPmAgMnS,k5eAaBmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
spoluhráč	spoluhráč	k1gMnSc1
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
Madridu	Madrid	k1gInSc2
za	za	k7c4
100	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
držitelem	držitel	k1gMnSc7
Zlaté	zlatý	k2eAgFnSc2d1
kopačky	kopačka	k1gFnSc2
za	za	k7c2
sezóny	sezóna	k1gFnSc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
United	United	k1gMnSc1
nastřílel	nastřílet	k5eAaPmAgMnS
za	za	k7c4
sezónu	sezóna	k1gFnSc4
31	#num#	k4
ligových	ligový	k2eAgFnPc2d1
branek	branka	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
40	#num#	k4
góly	gól	k1gInPc7
zajistil	zajistit	k5eAaPmAgMnS
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
La	la	k1gNnSc6
Lize	liga	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Získal	získat	k5eAaPmAgInS
i	i	k9
cenu	cena	k1gFnSc4
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
za	za	k7c4
rok	rok	k1gInSc4
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
2016	#num#	k4
a	a	k8xC
v	v	k7c6
letech	léto	k1gNnPc6
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Odchod	odchod	k1gInSc1
Raúla	Raúlo	k1gNnSc2
uvolnil	uvolnit	k5eAaPmAgInS
pro	pro	k7c4
Ronalda	Ronald	k1gMnSc4
dres	dres	k1gInSc4
číslo	číslo	k1gNnSc1
7	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
nosil	nosit	k5eAaImAgInS
již	již	k6eAd1
v	v	k7c6
Manchesteru	Manchester	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Při	při	k7c6
výhře	výhra	k1gFnSc6
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
nad	nad	k7c7
Racing	Racing	k1gInSc1
de	de	k?
Santander	Santander	k1gInSc1
dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
v	v	k7c6
kariéře	kariéra	k1gFnSc6
čtyřikrát	čtyřikrát	k6eAd1
střelecky	střelecky	k6eAd1
prosadil	prosadit	k5eAaPmAgMnS
<g/>
,	,	kIx,
čímmž	čímmž	k6eAd1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
udržel	udržet	k5eAaPmAgInS
domácí	domácí	k2eAgFnSc4d1
vítěznou	vítězný	k2eAgFnSc4d1
vlnu	vlna	k1gFnSc4
a	a	k8xC
i	i	k9
ligovou	ligový	k2eAgFnSc4d1
neporazitelnost	neporazitelnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
dalším	další	k2eAgInSc7d1
hattrickem	hattrick	k1gInSc7
překonal	překonat	k5eAaPmAgMnS
Athletic	Athletice	k1gFnPc2
Bilbao	Bilbao	k6eAd1
při	při	k7c6
výhře	výhra	k1gFnSc6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
do	do	k7c2
El	Ela	k1gFnPc2
Clásica	Clásic	k1gInSc2
vstupoval	vstupovat	k5eAaImAgInS
jako	jako	k9
nejlepší	dobrý	k2eAgMnSc1d3
střelec	střelec	k1gMnSc1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
(	(	kIx(
<g/>
15	#num#	k4
gólů	gól	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mužstvo	mužstvo	k1gNnSc1
vedené	vedený	k2eAgFnSc2d1
José	Josá	k1gFnSc2
Mourinhem	Mourinh	k1gInSc7
ale	ale	k9
v	v	k7c6
Katalánsku	Katalánsko	k1gNnSc6
prohrálo	prohrát	k5eAaPmAgNnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2011	#num#	k4
se	se	k3xPyFc4
navzdory	navzdory	k7c3
formě	forma	k1gFnSc3
nevměstnal	vměstnat	k5eNaPmAgInS
do	do	k7c2
finálové	finálový	k2eAgFnSc2d1
trojice	trojice	k1gFnSc2
nominované	nominovaný	k2eAgFnSc2d1
na	na	k7c4
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třemi	tři	k4xCgInPc7
góly	gól	k1gInPc7
v	v	k7c6
jednom	jeden	k4xCgInSc6
utkání	utkání	k1gNnSc6
se	se	k3xPyFc4
prezentoval	prezentovat	k5eAaBmAgMnS
rovněž	rovněž	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
prosince	prosinec	k1gInSc2
ve	v	k7c6
Španělském	španělský	k2eAgInSc6d1
poháru	pohár	k1gInSc6
proti	proti	k7c3
Levante	Levant	k1gMnSc5
(	(	kIx(
<g/>
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
začátku	začátek	k1gInSc6
ledna	leden	k1gInSc2
proti	proti	k7c3
Villarrealu	Villarreal	k1gInSc3
(	(	kIx(
<g/>
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
proti	proti	k7c3
Málaze	Málaha	k1gFnSc3
(	(	kIx(
<g/>
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
začátku	začátek	k1gInSc6
března	březen	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
dubnem	duben	k1gInSc7
a	a	k8xC
květnem	květen	k1gInSc7
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
čtyři	čtyři	k4xCgNnPc4
střetnutí	střetnutí	k1gNnPc2
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
kromě	kromě	k7c2
ligové	ligový	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
narazil	narazit	k5eAaPmAgMnS
také	také	k9
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
a	a	k8xC
též	též	k9
ve	v	k7c6
finále	finále	k1gNnSc6
Copa	Cop	k1gInSc2
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
vzájemný	vzájemný	k2eAgInSc4d1
střet	střet	k1gInSc4
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
po	po	k7c6
ligové	ligový	k2eAgFnSc6d1
remíze	remíza	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
předtím	předtím	k6eAd1
v	v	k7c6
Madridu	Madrid	k1gInSc6
rozhodl	rozhodnout	k5eAaPmAgInS
Ronaldo	Ronaldo	k1gNnSc1
vítězným	vítězný	k2eAgInSc7d1
gólem	gól	k1gInSc7
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
finále	finále	k1gNnSc4
Španělského	španělský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
ukončil	ukončit	k5eAaPmAgMnS
tříleté	tříletý	k2eAgNnSc4d1
čekání	čekání	k1gNnSc4
Realu	Real	k1gInSc2
na	na	k7c4
trofej	trofej	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
tyto	tento	k3xDgInPc4
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
do	do	k7c2
sítě	síť	k1gFnSc2
Barcelony	Barcelona	k1gFnSc2
ale	ale	k8xC
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
nenavázal	navázat	k5eNaPmAgInS
a	a	k8xC
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
po	po	k7c6
výsledcích	výsledek	k1gInPc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
doma	doma	k6eAd1
a	a	k8xC
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
venku	venku	k6eAd1
vypadl	vypadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Poté	poté	k6eAd1
co	co	k9
v	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
38	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
proti	proti	k7c3
již	již	k9
sestupující	sestupující	k2eAgFnSc3d1
Almeríi	Almerí	k1gFnSc3
dvakrát	dvakrát	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
při	při	k7c6
výhře	výhra	k1gFnSc6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
zaregistroval	zaregistrovat	k5eAaPmAgMnS
nejgólovější	gólový	k2eAgFnSc4d3
sezónu	sezóna	k1gFnSc4
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanovil	stanovit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
španělské	španělský	k2eAgNnSc1d1
La	la	k1gNnSc1
Ligy	liga	k1gFnSc2
svými	svůj	k3xOyFgInPc7
40	#num#	k4
góly	gól	k1gInPc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
překonal	překonat	k5eAaPmAgMnS
38	#num#	k4
gólů	gól	k1gInPc2
Huga	Hugo	k1gMnSc2
Sáncheze	Sáncheze	k1gFnSc2
ze	z	k7c2
sezóny	sezóna	k1gFnSc2
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
a	a	k8xC
i	i	k9
Telma	Telma	k1gNnSc1
Zarry	Zarra	k1gFnSc2
ze	z	k7c2
sezóny	sezóna	k1gFnSc2
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
proti	proti	k7c3
Ajaxu	Ajax	k1gInSc3
ve	v	k7c6
druhém	druhý	k4xOgInSc6
skupinovém	skupinový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
hraném	hraný	k2eAgNnSc6d1
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
(	(	kIx(
<g/>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
doma	doma	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
srpna	srpen	k1gInSc2
zahájil	zahájit	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
svoji	svůj	k3xOyFgFnSc4
třetí	třetí	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
hattrickem	hattrick	k1gInSc7
do	do	k7c2
sítě	síť	k1gFnSc2
Realu	Real	k1gInSc2
Zaragoza	Zaragoza	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
doma	doma	k6eAd1
prohrála	prohrát	k5eAaPmAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
počin	počin	k1gInSc1
zopakoval	zopakovat	k5eAaPmAgInS
proti	proti	k7c3
Rayu	Rayum	k1gNnSc3
Vallecano	Vallecana	k1gFnSc5
a	a	k8xC
Málaze	Málaha	k1gFnSc6
<g/>
,	,	kIx,
oba	dva	k4xCgInPc1
tyto	tento	k3xDgInPc1
zápasy	zápas	k1gInPc4
skončil	skončit	k5eAaPmAgInS
též	též	k9
vítězně	vítězně	k6eAd1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
doma	doma	k6eAd1
a	a	k8xC
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
venku	venku	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2011	#num#	k4
ještě	ještě	k6eAd1
slavil	slavit	k5eAaImAgInS
hattrick	hattrick	k1gInSc1
proti	proti	k7c3
Osasuně	Osasuna	k1gFnSc3
(	(	kIx(
<g/>
domácí	domácí	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Seville	Sevilla	k1gFnSc6
(	(	kIx(
<g/>
venkovní	venkovní	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
kalendářní	kalendářní	k2eAgInSc4d1
rok	rok	k1gInSc4
2011	#num#	k4
tak	tak	k6eAd1
vstřelil	vstřelit	k5eAaPmAgMnS
rekordních	rekordní	k2eAgFnPc2d1
60	#num#	k4
gólů	gól	k1gInPc2
v	v	k7c6
60	#num#	k4
zápasech	zápas	k1gInPc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ovšem	ovšem	k9
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
opět	opět	k6eAd1
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
v	v	k7c6
lednu	leden	k1gInSc6
získal	získat	k5eAaPmAgMnS
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
Levante	Levant	k1gMnSc5
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
svým	svůj	k3xOyFgInSc7
dalším	další	k2eAgInSc7d1
hattrickem	hattrick	k1gInSc7
a	a	k8xC
navýšit	navýšit	k5eAaPmF
tak	tak	k9
náskok	náskok	k1gInSc4
na	na	k7c4
Barcelonu	Barcelona	k1gFnSc4
na	na	k7c4
10	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
v	v	k7c6
zápase	zápas	k1gInSc6
proti	proti	k7c3
San	San	k1gMnSc3
Sebastianu	Sebastian	k1gMnSc3
překonal	překonat	k5eAaPmAgMnS
v	v	k7c6
dresu	dres	k1gInSc6
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc4
klubový	klubový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
v	v	k7c6
rychlosti	rychlost	k1gFnSc6
s	s	k7c7
jakou	jaký	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
dosáhl	dosáhnout	k5eAaPmAgInS
100	#num#	k4
ligových	ligový	k2eAgFnPc2d1
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačilo	stačit	k5eAaBmAgNnS
mu	on	k3xPp3gMnSc3
na	na	k7c4
to	ten	k3xDgNnSc4
pouhých	pouhý	k2eAgNnPc2d1
92	#num#	k4
ligových	ligový	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosavadní	dosavadní	k2eAgMnSc1d1
rekordman	rekordman	k1gMnSc1
Ferenc	Ferenc	k1gMnSc1
Puskás	Puskása	k1gFnPc2
na	na	k7c4
to	ten	k3xDgNnSc4
potřeboval	potřebovat	k5eAaImAgMnS
105	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
půdě	půda	k1gFnSc6
rivala	rival	k1gMnSc2
Atlétika	Atlétikum	k1gNnSc2
Madrid	Madrid	k1gInSc1
dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
znovu	znovu	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
třikrát	třikrát	k6eAd1
a	a	k8xC
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
tak	tak	k8xS,k8xC
po	po	k7c6
výhře	výhra	k1gFnSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
nadále	nadále	k6eAd1
držel	držet	k5eAaImAgInS
čtyřbodový	čtyřbodový	k2eAgInSc4d1
náskok	náskok	k1gInSc4
v	v	k7c6
ligové	ligový	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
května	květen	k1gInSc2
skončila	skončit	k5eAaPmAgFnS
ligová	ligový	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
ukončil	ukončit	k5eAaPmAgInS
nadvládu	nadvláda	k1gFnSc4
Barcelony	Barcelona	k1gFnSc2
a	a	k8xC
s	s	k7c7
devítibodovým	devítibodový	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
na	na	k7c4
katalánský	katalánský	k2eAgInSc4d1
klub	klub	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugalský	portugalský	k2eAgMnSc1d1
kouč	kouč	k1gMnSc1
José	José	k1gNnSc2
Mourinho	Mourin	k1gMnSc4
stanovil	stanovit	k5eAaPmAgInS
s	s	k7c7
pomocí	pomoc	k1gFnSc7
Ronalda	Ronaldo	k1gNnSc2
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
španělské	španělský	k2eAgNnSc1d1
La	la	k1gNnSc1
Ligy	liga	k1gFnSc2
ziskem	zisk	k1gInSc7
100	#num#	k4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
rekordního	rekordní	k2eAgInSc2d1
počtu	počet	k1gInSc2
32	#num#	k4
výher	výhra	k1gFnPc2
a	a	k8xC
vstřelením	vstřelení	k1gNnSc7
121	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotný	samotný	k2eAgInSc4d1
Ronaldo	Ronaldo	k1gNnSc1
vylepšil	vylepšit	k5eAaPmAgInS
vlastní	vlastní	k2eAgInSc1d1
gólový	gólový	k2eAgInSc1d1
počin	počin	k1gInSc1
z	z	k7c2
předchozí	předchozí	k2eAgFnSc2d1
ligové	ligový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
o	o	k7c4
čtyři	čtyři	k4xCgInPc4
góly	gól	k1gInPc4
<g/>
,	,	kIx,
se	s	k7c7
46	#num#	k4
góly	gól	k1gInPc7
ale	ale	k8xC
na	na	k7c4
trofej	trofej	k1gFnSc4
Pichichi	Pichich	k1gFnSc2
nedosáhl	dosáhnout	k5eNaPmAgInS
<g/>
,	,	kIx,
neboť	neboť	k8xC
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
gólů	gól	k1gInPc2
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
ovšem	ovšem	k9
prvním	první	k4xOgMnSc7
fotbalistou	fotbalista	k1gMnSc7
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
jediné	jediný	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
dal	dát	k5eAaPmAgMnS
alespoň	alespoň	k9
jeden	jeden	k4xCgInSc4
gól	gól	k1gInSc4
všem	všecek	k3xTgNnPc3
19	#num#	k4
soupeřům	soupeř	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
srpnu	srpen	k1gInSc6
zasadil	zasadit	k5eAaPmAgMnS
o	o	k7c4
zisk	zisk	k1gInSc4
Španělského	španělský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
jednou	jednou	k6eAd1
gólově	gólově	k6eAd1
prosadil	prosadit	k5eAaPmAgMnS
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
v	v	k7c6
obou	dva	k4xCgInPc6
dvou	dva	k4xCgInPc6
zápasech	zápas	k1gInPc6
o	o	k7c6
tuto	tento	k3xDgFnSc4
trofej	trofej	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Záhy	záhy	k6eAd1
si	se	k3xPyFc3
média	médium	k1gNnSc2
povšimla	povšimnout	k5eAaPmAgFnS
Ronaldovy	Ronaldův	k2eAgFnPc4d1
nespokojenosti	nespokojenost	k1gFnPc4
v	v	k7c6
klubu	klub	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
portugalský	portugalský	k2eAgMnSc1d1
forvard	forvard	k1gMnSc1
neslavil	slavit	k5eNaImAgMnS
své	svůj	k3xOyFgInPc4
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
do	do	k7c2
sítě	síť	k1gFnSc2
Granady	Granada	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
podnítilo	podnítit	k5eAaPmAgNnS
spekulace	spekulace	k1gFnPc4
<g/>
,	,	kIx,
zdali	zdali	k8xS
se	se	k3xPyFc4
hráč	hráč	k1gMnSc1
nechce	chtít	k5eNaImIp3nS
vrátit	vrátit	k5eAaPmF
do	do	k7c2
Manchesteru	Manchester	k1gInSc2
United	United	k1gInSc1
či	či	k8xC
zdali	zdali	k8xS
jej	on	k3xPp3gMnSc4
nevyvedlo	vyvést	k5eNaPmAgNnS
z	z	k7c2
míry	míra	k1gFnSc2
nezískání	nezískání	k1gNnSc2
ocenění	ocenění	k1gNnSc2
UEFA	UEFA	kA
pro	pro	k7c4
fotbalistu	fotbalista	k1gMnSc4
roku	rok	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
obdržel	obdržet	k5eAaPmAgInS
Andrés	Andrés	k1gInSc1
Iniesta	Iniesta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
pomezí	pomezí	k1gNnSc6
září	září	k1gNnSc2
a	a	k8xC
října	říjen	k1gInSc2
vsítil	vsítit	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
kariéře	kariéra	k1gFnSc6
dvakrát	dvakrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
hattrick	hattrick	k1gInSc4
<g/>
,	,	kIx,
nejprve	nejprve	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
září	září	k1gNnSc2
Deportivu	Deportiv	k1gInSc2
La	la	k1gNnSc2
Coruñ	Coruñ	k1gInSc2
v	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
při	při	k7c6
domácí	domácí	k2eAgFnSc6d1
výhře	výhra	k1gFnSc6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
posléze	posléze	k6eAd1
na	na	k7c6
půdě	půda	k1gFnSc6
Ajaxu	Ajax	k1gInSc2
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
při	při	k7c6
výhře	výhra	k1gFnSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
v	v	k7c6
této	tento	k3xDgFnSc6
skupině	skupina	k1gFnSc6
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
narazil	narazit	k5eAaPmAgMnS
na	na	k7c6
Borussii	Borussie	k1gFnSc6
Dortmund	Dortmund	k1gInSc1
a	a	k8xC
Manchester	Manchester	k1gInSc1
City	City	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
druhém	druhý	k4xOgInSc6
utkání	utkání	k1gNnSc6
měl	mít	k5eAaImAgMnS
ovšem	ovšem	k9
dvě	dva	k4xCgFnPc4
výhry	výhra	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Navzdory	navzdory	k7c3
výkonům	výkon	k1gInPc3
za	za	k7c4
rok	rok	k1gInSc4
2012	#num#	k4
putoval	putovat	k5eAaImAgInS
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFA	FIFA	kA
do	do	k7c2
rukou	ruka	k1gFnPc2
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
kariérního	kariérní	k2eAgMnSc2d1
rivala	rival	k1gMnSc2
Lionela	Lionel	k1gMnSc2
Messiho	Messi	k1gMnSc2
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
Ronaldo	Ronaldo	k1gNnSc4
skončil	skončit	k5eAaPmAgMnS
druhý	druhý	k4xOgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
ledna	leden	k1gInSc2
vedl	vést	k5eAaImAgMnS
mužstvo	mužstvo	k1gNnSc4
jako	jako	k8xC,k8xS
kapitán	kapitán	k1gMnSc1
navzdory	navzdory	k7c3
přítomnosti	přítomnost	k1gFnSc3
Ikera	Iker	k1gMnSc2
Casillase	Casillasa	k1gFnSc6
na	na	k7c6
hřišti	hřiště	k1gNnSc6
a	a	k8xC
dvěma	dva	k4xCgInPc7
góly	gól	k1gInPc7
pomohl	pomoct	k5eAaPmAgMnS
zdolat	zdolat	k5eAaPmF
Real	Real	k1gInSc4
Sociedad	Sociedad	k1gInSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
ledna	leden	k1gInSc2
pak	pak	k6eAd1
nejprve	nejprve	k6eAd1
třikrát	třikrát	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
proti	proti	k7c3
Getafe	Getaf	k1gInSc5
při	při	k7c6
výhře	výhra	k1gFnSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
z	z	k7c2
gólů	gól	k1gInPc2
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gInSc7
300	#num#	k4
<g/>
.	.	kIx.
gólem	gól	k1gInSc7
na	na	k7c6
klubové	klubový	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
v	v	k7c6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
navlékl	navléknout	k5eAaPmAgInS
kapitánskou	kapitánský	k2eAgFnSc4d1
pásku	páska	k1gFnSc4
také	také	k9
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
<g/>
,	,	kIx,
první	první	k4xOgInSc4
domácí	domácí	k2eAgInSc4d1
zápas	zápas	k1gInSc4
domácího	domácí	k2eAgInSc2d1
poháru	pohár	k1gInSc2
Copa	Copa	k1gMnSc1
del	del	k?
Rey	Rea	k1gFnSc2
ale	ale	k8xC
skončil	skončit	k5eAaPmAgInS
nerozhodně	rozhodně	k6eNd1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prvním	první	k4xOgInSc6
zápase	zápas	k1gInSc6
Realu	Real	k1gInSc2
v	v	k7c6
semifinále	semifinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2013	#num#	k4
proti	proti	k7c3
Borussii	Borussie	k1gFnSc3
Dortmund	Dortmund	k1gInSc1
vstřelil	vstřelit	k5eAaPmAgMnS
jeden	jeden	k4xCgInSc4
gól	gól	k1gInSc4
(	(	kIx(
<g/>
vyrovnával	vyrovnávat	k5eAaImAgInS
na	na	k7c6
průběžných	průběžný	k2eAgNnPc6d1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gInPc7
jubilejní	jubilejní	k2eAgInSc4d1
padesátý	padesátý	k4xOgInSc4
gól	gól	k1gInSc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
dvanáctý	dvanáctý	k4xOgInSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
ročníku	ročník	k1gInSc6
LM	LM	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
Real	Real	k1gInSc1
nakonec	nakonec	k6eAd1
odjížděl	odjíždět	k5eAaImAgInS
domů	domů	k6eAd1
s	s	k7c7
porážkou	porážka	k1gFnSc7
1	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
a	a	k8xC
nepříjemnou	příjemný	k2eNgFnSc4d1
pozicí	pozice	k1gFnSc7
pro	pro	k7c4
odvetu	odveta	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Výhra	výhra	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
v	v	k7c6
odvetě	odveta	k1gFnSc6
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
na	na	k7c4
postup	postup	k1gInSc4
nestačila	stačit	k5eNaBmAgFnS
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc4
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
spoluhráči	spoluhráč	k1gMnPc7
rozloučit	rozloučit	k5eAaPmF
s	s	k7c7
vidinou	vidina	k1gFnSc7
zisku	zisk	k1gInSc2
poháru	pohár	k1gInSc2
pro	pro	k7c4
vítěze	vítěz	k1gMnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
v	v	k7c6
tomto	tento	k3xDgInSc6
zápase	zápas	k1gInSc6
gól	gól	k1gInSc1
nevstřelil	vstřelit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc4
dal	dát	k5eAaPmAgInS
300	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
ve	v	k7c6
svém	své	k1gNnSc6
197	#num#	k4
<g/>
.	.	kIx.
zápase	zápas	k1gInSc6
za	za	k7c4
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
v	v	k7c6
lize	liga	k1gFnSc6
proti	proti	k7c3
Málaze	Málaha	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
Realu	Real	k1gInSc6
podlehla	podlehnout	k5eAaPmAgNnP
po	po	k7c6
výsledku	výsledek	k1gInSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
v	v	k7c6
září	září	k1gNnSc6
2013	#num#	k4
v	v	k7c6
derby	derby	k1gNnSc6
proti	proti	k7c3
Atlétiku	Atlétikum	k1gNnSc3
Madrid	Madrid	k1gInSc1
</s>
<s>
Počínaje	počínaje	k7c7
sezónou	sezóna	k1gFnSc7
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
se	se	k3xPyFc4
Ronaldo	Ronalda	k1gFnSc5
stal	stát	k5eAaPmAgInS
součástí	součást	k1gFnSc7
hvězdné	hvězdný	k2eAgFnSc2d1
útočné	útočný	k2eAgFnSc2d1
trojice	trojice	k1gFnSc2
s	s	k7c7
Karimem	Karim	k1gMnSc7
Benzemou	Benzema	k1gMnSc7
a	a	k8xC
nově	nově	k6eAd1
příchozím	příchozí	k1gMnSc7
Garethem	Gareth	k1gInSc7
Balem	bal	k1gInSc7
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
média	médium	k1gNnPc1
označovala	označovat	k5eAaImAgNnP
zkráceně	zkráceně	k6eAd1
„	„	k?
<g/>
BBC	BBC	kA
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
září	září	k1gNnSc2
Ronaldo	Ronaldo	k1gNnSc1
odehrál	odehrát	k5eAaPmAgMnS
úvodní	úvodní	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
skupiny	skupina	k1gFnSc2
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
a	a	k8xC
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Galatasaray	Galatasaraa	k1gFnSc2
se	se	k3xPyFc4
blýskl	blýsknout	k5eAaPmAgMnS
hattrickem	hattrick	k1gInSc7
při	při	k7c6
výhře	výhra	k1gFnSc6
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Během	během	k7c2
těchto	tento	k3xDgInPc2
dní	den	k1gInPc2
se	se	k3xPyFc4
dohodl	dohodnout	k5eAaPmAgInS
s	s	k7c7
klubem	klub	k1gInSc7
na	na	k7c4
prodloužení	prodloužení	k1gNnSc4
smlouvy	smlouva	k1gFnSc2
na	na	k7c4
dalších	další	k2eAgNnPc2d1
pět	pět	k4xCc4
let	léto	k1gNnPc2
a	a	k8xC
stal	stát	k5eAaPmAgInS
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
vydělávajícím	vydělávající	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
předběhl	předběhnout	k5eAaPmAgInS
Zlatana	Zlatan	k1gMnSc4
Ibrahimoviće	Ibrahimović	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
v	v	k7c6
utkání	utkání	k1gNnSc6
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
italskému	italský	k2eAgInSc3d1
Juventusu	Juventus	k1gInSc3
<g/>
,	,	kIx,
zařídil	zařídit	k5eAaPmAgInS
tak	tak	k9
výhru	výhra	k1gFnSc4
Realu	Real	k1gInSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc4
gól	gól	k1gInSc4
vstřelil	vstřelit	k5eAaPmAgMnS
z	z	k7c2
pokutového	pokutový	k2eAgInSc2d1
kopu	kop	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
Realem	Real	k1gInSc7
postoupil	postoupit	k5eAaPmAgMnS
z	z	k7c2
prvního	první	k4xOgNnSc2
místa	místo	k1gNnSc2
v	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
B	B	kA
do	do	k7c2
vyřazovacích	vyřazovací	k2eAgInPc2d1
bojů	boj	k1gInPc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
skupiny	skupina	k1gFnSc2
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
gól	gól	k1gInSc4
proti	proti	k7c3
dánskému	dánský	k2eAgInSc3d1
týmu	tým	k1gInSc3
FC	FC	kA
Kodaň	Kodaň	k1gFnSc4
a	a	k8xC
přispěl	přispět	k5eAaPmAgInS
tak	tak	k6eAd1
k	k	k7c3
vítězství	vítězství	k1gNnSc3
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
to	ten	k3xDgNnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
devátá	devátý	k4xOgFnSc1
branka	branka	k1gFnSc1
v	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vytvořil	vytvořit	k5eAaPmAgMnS
nový	nový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
LM	LM	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc4
zaznamenal	zaznamenat	k5eAaPmAgMnS
tři	tři	k4xCgInPc4
góly	gól	k1gInPc4
a	a	k8xC
tedy	tedy	k9
další	další	k2eAgInSc4d1
hattrick	hattrick	k1gInSc4
proti	proti	k7c3
Realu	Real	k1gInSc3
Sociedad	Sociedad	k1gInSc1
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
ligové	ligový	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
při	při	k7c6
výhře	výhra	k1gFnSc6
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
na	na	k7c6
začátku	začátek	k1gInSc6
listopadu	listopad	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
ocenění	ocenění	k1gNnSc4
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFA	FIFA	kA
za	za	k7c4
výkony	výkon	k1gInPc4
v	v	k7c6
minulém	minulý	k2eAgInSc6d1
roce	rok	k1gInSc6
a	a	k8xC
ukončil	ukončit	k5eAaPmAgInS
tak	tak	k9
sérii	série	k1gFnSc4
Lionela	Lionel	k1gMnSc2
Messiho	Messi	k1gMnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
ocenění	ocenění	k1gNnSc4
obdržel	obdržet	k5eAaPmAgMnS
čtyřikrát	čtyřikrát	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
zůstal	zůstat	k5eAaPmAgMnS
společně	společně	k6eAd1
s	s	k7c7
dalším	další	k2eAgMnSc7d1
finalistou	finalista	k1gMnSc7
Franckem	Francek	k1gMnSc7
Ribérym	Ribérym	k1gInSc4
až	až	k9
za	za	k7c7
Ronaldem	Ronaldo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2013	#num#	k4
jednou	jednou	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
v	v	k7c6
ligovém	ligový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
s	s	k7c7
Valencií	Valencie	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gInSc7
18	#num#	k4
<g/>
.	.	kIx.
trefa	trefa	k1gFnSc1
v	v	k7c6
ligovém	ligový	k2eAgInSc6d1
ročníku	ročník	k1gInSc6
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tabulce	tabulka	k1gFnSc6
střelců	střelec	k1gMnPc2
jej	on	k3xPp3gInSc4
však	však	k9
o	o	k7c4
gól	gól	k1gInSc4
přeskočil	přeskočit	k5eAaPmAgMnS
Diega	Dieg	k1gMnSc4
Costu	Costa	k1gMnSc4
z	z	k7c2
Atlética	Atlétic	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
ročníku	ročník	k1gInSc2
se	se	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc4
stal	stát	k5eAaPmAgMnS
s	s	k7c7
31	#num#	k4
vstřelenými	vstřelený	k2eAgInPc7d1
góly	gól	k1gInPc7
nejlepším	dobrý	k2eAgMnSc7d3
kanonýrem	kanonýr	k1gMnSc7
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
Ligový	ligový	k2eAgInSc1d1
titul	titul	k1gInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
sice	sice	k8xC
nezískal	získat	k5eNaPmAgInS
(	(	kIx(
<g/>
vyhrálo	vyhrát	k5eAaPmAgNnS
jej	on	k3xPp3gNnSc4
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
vítězem	vítěz	k1gMnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
po	po	k7c6
finálové	finálový	k2eAgFnSc6d1
výhře	výhra	k1gFnSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
po	po	k7c6
prodloužení	prodloužení	k1gNnSc6
v	v	k7c6
derby	derby	k1gNnSc6
právě	právě	k6eAd1
nad	nad	k7c7
Atléticem	Atlétic	k1gMnSc7
Madrid	Madrid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
vítězství	vítězství	k1gNnSc3
přispěl	přispět	k5eAaPmAgMnS
jednou	jeden	k4xCgFnSc7
brankou	branka	k1gFnSc7
a	a	k8xC
se	s	k7c7
17	#num#	k4
góly	gól	k1gInPc7
(	(	kIx(
<g/>
rekord	rekord	k1gInSc1
soutěže	soutěž	k1gFnSc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
také	také	k9
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
a	a	k8xC
obhájil	obhájit	k5eAaPmAgMnS
tak	tak	k9
své	svůj	k3xOyFgNnSc4
prvenství	prvenství	k1gNnSc4
z	z	k7c2
předchozího	předchozí	k2eAgInSc2d1
ročníku	ročník	k1gInSc2
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
střelců	střelec	k1gMnPc2
LM	LM	kA
třikrát	třikrát	k6eAd1
<g/>
,	,	kIx,
ještě	ještě	k9
v	v	k7c6
sezoně	sezona	k1gFnSc6
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
zároveň	zároveň	k6eAd1
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
skóroval	skórovat	k5eAaBmAgMnS
ve	v	k7c4
finále	finále	k1gNnSc4
za	za	k7c4
dva	dva	k4xCgInPc4
různé	různý	k2eAgInPc4d1
vítězné	vítězný	k2eAgInPc4d1
týmy	tým	k1gInPc4
LM	LM	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
zahájil	zahájit	k5eAaPmAgMnS
novou	nový	k2eAgFnSc4d1
soutěžní	soutěžní	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
Superpohárem	superpohár	k1gInSc7
UEFA	UEFA	kA
proti	proti	k7c3
Seville	Sevilla	k1gFnSc3
a	a	k8xC
dvěma	dva	k4xCgInPc7
góly	gól	k1gInPc7
zařídil	zařídit	k5eAaPmAgMnS
výhru	výhra	k1gFnSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
zisk	zisk	k1gInSc1
trofeje	trofej	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
byl	být	k5eAaImAgInS
v	v	k7c6
základní	základní	k2eAgFnSc6d1
sestavě	sestava	k1gFnSc6
Realu	Real	k1gInSc2
proti	proti	k7c3
Atlétiku	Atlétik	k1gMnSc3
Madrid	Madrid	k1gInSc1
v	v	k7c6
prvním	první	k4xOgInSc6
zápase	zápas	k1gInSc6
o	o	k7c4
Španělský	španělský	k2eAgInSc4d1
superpohár	superpohár	k1gInSc4
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
zranění	zranění	k1gNnSc3
jej	on	k3xPp3gMnSc4
ale	ale	k8xC
trenér	trenér	k1gMnSc1
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotti	k1gNnSc4
o	o	k7c6
poločasové	poločasový	k2eAgFnSc6d1
přestávce	přestávka	k1gFnSc6
vystřídal	vystřídat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domácí	domácí	k2eAgInSc1d1
zápas	zápas	k1gInSc1
skončil	skončit	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
u	u	k7c2
Ronalda	Ronaldo	k1gNnSc2
se	se	k3xPyFc4
ukázal	ukázat	k5eAaPmAgInS
být	být	k5eAaImF
problémem	problém	k1gInSc7
levý	levý	k2eAgInSc4d1
stehenní	stehenní	k2eAgInSc4d1
sval	sval	k1gInSc4
a	a	k8xC
nikoliv	nikoliv	k9
záda	záda	k1gNnPc4
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yQgMnPc7,k3yRgMnPc7,k3yIgMnPc7
laboroval	laborovat	k5eAaImAgMnS
v	v	k7c6
průběhu	průběh	k1gInSc6
léta	léto	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Atlétiko	Atlétika	k1gFnSc5
druhý	druhý	k4xOgInSc4
srpnový	srpnový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
zvládlo	zvládnout	k5eAaPmAgNnS
a	a	k8xC
vyhrálo	vyhrát	k5eAaPmAgNnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc4
odehrál	odehrát	k5eAaPmAgMnS
až	až	k9
druhý	druhý	k4xOgInSc4
poločas	poločas	k1gInSc4
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
ale	ale	k8xC
soupeře	soupeř	k1gMnSc4
neohrozil	ohrozit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nový	nový	k2eAgInSc4d1
ročník	ročník	k1gInSc4
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
začal	začít	k5eAaPmAgInS
fantasticky	fantasticky	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
vstřelil	vstřelit	k5eAaPmAgMnS
ve	v	k7c6
3	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
4	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
dohromady	dohromady	k6eAd1
7	#num#	k4
gólů	gól	k1gInPc2
(	(	kIx(
<g/>
celkově	celkově	k6eAd1
měl	mít	k5eAaImAgInS
za	za	k7c4
4	#num#	k4
kola	kolo	k1gNnSc2
vstřeleno	vstřelit	k5eAaPmNgNnS
9	#num#	k4
branek	branka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
také	také	k9
se	se	k3xPyFc4
zapsal	zapsat	k5eAaPmAgMnS
do	do	k7c2
listiny	listina	k1gFnSc2
10	#num#	k4
nejlepších	dobrý	k2eAgMnPc2d3
střelců	střelec	k1gMnPc2
Primera	primera	k1gFnSc1
División	División	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
2014	#num#	k4
překonal	překonat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Alfreda	Alfred	k1gMnSc2
di	di	k?
Stéfana	Stéfan	k1gMnSc2
a	a	k8xC
Telma	Telm	k1gMnSc2
Zarry	Zarra	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
vstřelil	vstřelit	k5eAaPmAgMnS
za	za	k7c4
celé	celý	k2eAgNnSc4d1
své	svůj	k3xOyFgNnSc4
působení	působení	k1gNnSc4
v	v	k7c6
La	la	k1gNnSc6
Lize	liga	k1gFnSc6
rekordních	rekordní	k2eAgInPc2d1
23	#num#	k4
hattricků	hattrick	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2015	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
poprvé	poprvé	k6eAd1
v	v	k7c6
kariéře	kariéra	k1gFnSc6
5	#num#	k4
gólů	gól	k1gInPc2
v	v	k7c6
ligovém	ligový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
proti	proti	k7c3
týmu	tým	k1gInSc3
Granada	Granada	k1gFnSc1
CF	CF	kA
při	při	k7c6
výhře	výhra	k1gFnSc6
9	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldův	Ronaldův	k2eAgInSc1d1
gólový	gólový	k2eAgInSc1d1
„	„	k?
<g/>
účet	účet	k1gInSc1
<g/>
“	“	k?
za	za	k7c4
sezónu	sezóna	k1gFnSc4
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
čísla	číslo	k1gNnSc2
61	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
jeho	jeho	k3xOp3gInSc7
osobním	osobní	k2eAgInSc7d1
rekordem	rekord	k1gInSc7
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
slaví	slavit	k5eAaImIp3nP
gól	gól	k1gInSc4
za	za	k7c4
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
</s>
<s>
V	v	k7c6
utkání	utkání	k1gNnSc6
La	la	k1gNnPc2
Ligy	liga	k1gFnSc2
venku	venku	k6eAd1
proti	proti	k7c3
Espanyolu	Espanyol	k1gInSc3
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
se	se	k3xPyFc4
Ronaldo	Ronalda	k1gFnSc5
stal	stát	k5eAaPmAgInS
autorem	autor	k1gMnSc7
pěti	pět	k4xCc2
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
konečná	konečný	k2eAgFnSc1d1
výhra	výhra	k1gFnSc1
činila	činit	k5eAaImAgFnS
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
překonal	překonat	k5eAaPmAgMnS
Raúla	Raúla	k1gMnSc1
jakožto	jakožto	k8xS
nejlepšího	dobrý	k2eAgMnSc4d3
střelce	střelec	k1gMnSc4
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
na	na	k7c6
kontě	konto	k1gNnSc6
měl	mít	k5eAaImAgInS
nyní	nyní	k6eAd1
229	#num#	k4
gólů	gól	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pár	pár	k4xCyI
dní	den	k1gInPc2
na	na	k7c4
to	ten	k3xDgNnSc4
se	se	k3xPyFc4
hattrickem	hattrick	k1gInSc7
postaral	postarat	k5eAaPmAgMnS
o	o	k7c4
výhru	výhra	k1gFnSc4
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
nad	nad	k7c7
Šachtarem	Šachtar	k1gInSc7
Doněck	Doněck	k1gInSc1
v	v	k7c6
úvodním	úvodní	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
a	a	k8xC
s	s	k7c7
80	#num#	k4
góly	gól	k1gInPc7
čněl	čnět	k5eAaImAgMnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
tabulky	tabulka	k1gFnSc2
střelců	střelec	k1gMnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
utkání	utkání	k1gNnSc6
evropské	evropský	k2eAgFnSc2d1
scény	scéna	k1gFnSc2
30	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
proti	proti	k7c3
švédskému	švédský	k2eAgMnSc3d1
Malmö	Malmö	k1gMnSc3
vyhrál	vyhrát	k5eAaPmAgMnS
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
venku	venek	k1gInSc2
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc4
mezitím	mezitím	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
500	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
v	v	k7c6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
navázal	navázat	k5eAaPmAgMnS
na	na	k7c4
jeden	jeden	k4xCgInSc4
gól	gól	k1gInSc4
proti	proti	k7c3
Malmö	Malmö	k1gFnSc3
a	a	k8xC
v	v	k7c6
utkání	utkání	k1gNnSc6
v	v	k7c6
Madridu	Madrid	k1gInSc6
vstřelil	vstřelit	k5eAaPmAgMnS
tomuto	tento	k3xDgMnSc3
soupeři	soupeř	k1gMnSc3
čtyři	čtyři	k4xCgInPc4
góly	gól	k1gInPc4
při	při	k7c6
výhře	výhra	k1gFnSc6
8	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtým	čtvrtá	k1gFnPc3
gólem	gól	k1gInSc7
navýšil	navýšit	k5eAaPmAgInS
počet	počet	k1gInSc1
gólů	gól	k1gInPc2
ve	v	k7c6
skupinové	skupinový	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
v	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
na	na	k7c4
11	#num#	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nikdo	nikdo	k3yNnSc1
nevstřelil	vstřelit	k5eNaPmAgMnS
ani	ani	k8xC
deset	deset	k4xCc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldovy	Ronaldův	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
góly	gól	k1gInPc4
při	při	k7c6
ligové	ligový	k2eAgFnSc6d1
výhře	výhra	k1gFnSc6
7	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
nad	nad	k7c7
Celtou	celta	k1gFnSc7
Vigo	Vigo	k1gNnSc1
5	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
Portugalce	Portugalka	k1gFnSc6
posunuli	posunout	k5eAaPmAgMnP
na	na	k7c4
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
před	před	k7c4
Telma	Telma	k1gNnSc4
Zarru	Zarr	k1gInSc2
v	v	k7c6
historické	historický	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
střelců	střelec	k1gMnPc2
španělské	španělský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
před	před	k7c7
ním	on	k3xPp3gMnSc7
zůstal	zůstat	k5eAaPmAgInS
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
z	z	k7c2
Barcelony	Barcelona	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
čtvrtfinálový	čtvrtfinálový	k2eAgInSc1d1
zápas	zápas	k1gInSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
v	v	k7c6
Německu	Německo	k1gNnSc6
proti	proti	k7c3
Wolfsburgu	Wolfsburg	k1gInSc3
skončil	skončit	k5eAaPmAgInS
prohrou	prohra	k1gFnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc4
ale	ale	k8xC
domácí	domácí	k2eAgFnSc4d1
odvetu	odveta	k1gFnSc4
ovládl	ovládnout	k5eAaPmAgInS
třemi	tři	k4xCgInPc7
góly	gól	k1gInPc7
a	a	k8xC
jeho	jeho	k3xOp3gInPc4
hattrick	hattrick	k1gInSc1
v	v	k7c6
konečném	konečný	k2eAgInSc6d1
součtu	součet	k1gInSc6
po	po	k7c6
výsledku	výsledek	k1gInSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
vyústil	vyústit	k5eAaPmAgInS
v	v	k7c4
postup	postup	k1gInSc4
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spolu	spolu	k6eAd1
s	s	k7c7
Realem	Real	k1gInSc7
dokráčel	dokráčet	k5eAaPmAgMnS
do	do	k7c2
finále	finále	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
reprízou	repríza	k1gFnSc7
toho	ten	k3xDgNnSc2
z	z	k7c2
před	před	k7c7
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
soupeřem	soupeř	k1gMnSc7
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
opět	opět	k6eAd1
Atlético	Atlético	k6eAd1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
během	během	k7c2
finále	finále	k1gNnSc2
podal	podat	k5eAaPmAgMnS
později	pozdě	k6eAd2
kritizovaný	kritizovaný	k2eAgInSc4d1
výkon	výkon	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
při	při	k7c6
penaltovém	penaltový	k2eAgInSc6d1
rozstřelu	rozstřel	k1gInSc6
nezaváhal	zaváhat	k5eNaPmAgMnS
a	a	k8xC
rozhodl	rozhodnout	k5eAaPmAgMnS
o	o	k7c4
vítěztví	vítěztví	k1gNnSc4
<g/>
,	,	kIx,
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
již	již	k6eAd1
třetího	třetí	k4xOgNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popáté	popáté	k4xO
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
tento	tento	k3xDgInSc4
rok	rok	k1gInSc4
počtvrté	počtvrté	k4xO
za	za	k7c7
sebou	se	k3xPyFc7
a	a	k8xC
tentokráte	tentokráte	k?
ovládl	ovládnout	k5eAaPmAgMnS
žebříček	žebříček	k1gInSc4
se	s	k7c7
16	#num#	k4
góly	gól	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pošesté	pošesté	k4xO
za	za	k7c4
sebou	se	k3xPyFc7
vstřelil	vstřelit	k5eAaPmAgMnS
za	za	k7c4
sezónu	sezóna	k1gFnSc4
50	#num#	k4
a	a	k8xC
více	hodně	k6eAd2
gólů	gól	k1gInPc2
a	a	k8xC
ačkoli	ačkoli	k8xS
31	#num#	k4
<g/>
letý	letý	k2eAgInSc4d1
<g/>
,	,	kIx,
odehrál	odehrát	k5eAaPmAgMnS
z	z	k7c2
mužstva	mužstvo	k1gNnSc2
nejvíce	hodně	k6eAd3,k6eAd1
minut	minuta	k1gFnPc2
za	za	k7c4
celou	celý	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Ronaldovo	Ronaldův	k2eAgNnSc4d1
zranění	zranění	k1gNnSc4
z	z	k7c2
vítězného	vítězný	k2eAgNnSc2d1
finále	finále	k1gNnSc2
evropského	evropský	k2eAgInSc2d1
šampionátu	šampionát	k1gInSc2
jej	on	k3xPp3gNnSc4
připravilo	připravit	k5eAaPmAgNnS
o	o	k7c6
první	první	k4xOgFnSc6
trojici	trojice	k1gFnSc6
zápasů	zápas	k1gInPc2
nové	nový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
včetně	včetně	k7c2
toho	ten	k3xDgInSc2
o	o	k7c4
Superpohár	superpohár	k1gInSc4
UEFA	UEFA	kA
proti	proti	k7c3
Seville	Sevilla	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
září	září	k1gNnSc2
pomohl	pomoct	k5eAaPmAgInS
k	k	k7c3
obratu	obrat	k1gInSc3
domácího	domácí	k2eAgInSc2d1
zápasu	zápas	k1gInSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
proti	proti	k7c3
Sportingu	Sporting	k1gInSc3
z	z	k7c2
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
na	na	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
89	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
totiž	totiž	k8xC
skóroval	skórovat	k5eAaBmAgMnS
z	z	k7c2
přímého	přímý	k2eAgInSc2d1
kopu	kop	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
nastaveném	nastavený	k2eAgInSc6d1
čase	čas	k1gInSc6
pak	pak	k6eAd1
obrat	obrat	k1gInSc1
dokonal	dokonat	k5eAaPmAgInS
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
gól	gól	k1gInSc1
proti	proti	k7c3
svému	svůj	k3xOyFgInSc3
mateřskému	mateřský	k2eAgInSc3d1
klubu	klub	k1gInSc3
neoslavil	oslavit	k5eNaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
listopadu	listopad	k1gInSc2
prodloužil	prodloužit	k5eAaPmAgMnS
smlouvu	smlouva	k1gFnSc4
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gInSc1
hattrick	hattrick	k1gInSc1
na	na	k7c6
půdě	půda	k1gFnSc6
Atlética	Atlétic	k1gInSc2
Madrid	Madrid	k1gInSc1
dnes	dnes	k6eAd1
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
znamenal	znamenat	k5eAaImAgInS
výhru	výhra	k1gFnSc4
Realu	Real	k1gInSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Ronaldo	Ronaldo	k1gNnSc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
historicky	historicky	k6eAd1
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
madridského	madridský	k2eAgNnSc2d1
derby	derby	k1gNnSc2
s	s	k7c7
18	#num#	k4
góly	gól	k1gInPc7
–	–	k?
překonal	překonat	k5eAaPmAgInS
Alfreda	Alfred	k1gMnSc4
Di	Di	k1gMnSc4
Stéfana	Stéfan	k1gMnSc4
se	se	k3xPyFc4
17	#num#	k4
góly	gól	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
se	se	k3xPyFc4
představil	představit	k5eAaPmAgMnS
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
FIFA	FIFA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
proti	proti	k7c3
celku	celek	k1gInSc2
Club	club	k1gInSc1
América	América	k1gFnSc1
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
vstřelil	vstřelit	k5eAaPmAgMnS
500	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
klubové	klubový	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Real	Real	k1gInSc1
přes	přes	k7c4
mexického	mexický	k2eAgMnSc4d1
soupeře	soupeř	k1gMnSc4
přešel	přejít	k5eAaPmAgInS
a	a	k8xC
ve	v	k7c6
finále	finále	k1gNnSc6
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
zdolal	zdolat	k5eAaPmAgMnS
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
japonský	japonský	k2eAgInSc1d1
celek	celek	k1gInSc1
Kašima	Kašimum	k1gNnSc2
Antlers	Antlersa	k1gFnPc2
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
Ronaldo	Ronaldo	k1gNnSc4
vstřelil	vstřelit	k5eAaPmAgMnS
hattrick	hattrick	k1gInSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
mužem	muž	k1gMnSc7
utkání	utkání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následně	následně	k6eAd1
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
nejlepším	dobrý	k2eAgMnSc7d3
fotbalistou	fotbalista	k1gMnSc7
turnaje	turnaj	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
opanoval	opanovat	k5eAaPmAgInS
potřetí	potřetí	k4xO
v	v	k7c6
kariéře	kariéra	k1gFnSc6
(	(	kIx(
<g/>
2008	#num#	k4
s	s	k7c7
United	United	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
s	s	k7c7
Realem	Real	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2017	#num#	k4
obdržel	obdržet	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
FIFA	FIFA	kA
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
měsíci	měsíc	k1gInSc6
z	z	k7c2
penalty	penalta	k1gFnSc2
vstřelil	vstřelit	k5eAaPmAgMnS
gól	gól	k1gInSc4
Seville	Sevilla	k1gFnSc3
a	a	k8xC
srovnal	srovnat	k5eAaPmAgMnS
rekord	rekord	k1gInSc4
někdejšího	někdejší	k2eAgMnSc2d1
útočníka	útočník	k1gMnSc2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
Huga	Hugo	k1gMnSc2
Sáncheze	Sáncheze	k1gFnSc2
v	v	k7c6
počtu	počet	k1gInSc6
proměněných	proměněný	k2eAgFnPc2d1
penalt	penalta	k1gFnPc2
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
první	první	k4xOgFnSc6
lize	liga	k1gFnSc6
–	–	k?
56	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
ale	ale	k8xC
prohrál	prohrát	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
přišel	přijít	k5eAaPmAgMnS
o	o	k7c4
svoji	svůj	k3xOyFgFnSc4
sérii	série	k1gFnSc4
bez	bez	k7c2
porážky	porážka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnovém	dubnový	k2eAgNnSc6d1
čtvrtfinále	čtvrtfinále	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Bayernu	Bayern	k1gInSc2
Mnichov	Mnichov	k1gInSc1
otočil	otočit	k5eAaPmAgInS
výsledek	výsledek	k1gInSc1
utkání	utkání	k1gNnPc2
na	na	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
svými	svůj	k3xOyFgMnPc7
dvěma	dva	k4xCgInPc7
góly	gól	k1gInPc7
ve	v	k7c6
druhém	druhý	k4xOgInSc6
poločase	poločas	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
odveta	odveta	k1gFnSc1
si	se	k3xPyFc3
po	po	k7c6
výsledku	výsledek	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
z	z	k7c2
pohledu	pohled	k1gInSc2
Realu	Real	k1gInSc2
vyžádala	vyžádat	k5eAaPmAgFnS
prodloužení	prodloužení	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
něm	on	k3xPp3gNnSc6
však	však	k9
Ronaldo	Ronaldo	k1gNnSc4
zkompletoval	zkompletovat	k5eAaPmAgMnS
hattrick	hattrick	k1gInSc4
a	a	k8xC
celkově	celkově	k6eAd1
pěti	pět	k4xCc7
góly	gól	k1gInPc7
ve	v	k7c6
dvojzápase	dvojzápas	k1gInSc6
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c6
postupu	postup	k1gInSc6
do	do	k7c2
semifinále	semifinále	k1gNnSc2
po	po	k7c6
celkovém	celkový	k2eAgInSc6d1
výsledku	výsledek	k1gInSc6
6	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
fotbalista	fotbalista	k1gMnSc1
tak	tak	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgMnS
stovky	stovka	k1gFnPc4
gólů	gól	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Začátkem	začátkem	k7c2
května	květen	k1gInSc2
vstřelil	vstřelit	k5eAaPmAgMnS
v	v	k7c6
domácím	domácí	k2eAgNnSc6d1
semifinále	semifinále	k1gNnSc6
hattrick	hattrick	k1gInSc1
Atlétiku	Atlétikum	k1gNnSc6
Madrid	Madrid	k1gInSc1
a	a	k8xC
přiblížil	přiblížit	k5eAaPmAgInS
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
finálovému	finálový	k2eAgInSc3d1
zápasu	zápas	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
La	la	k1gNnSc1
Lize	liga	k1gFnSc6
vstřelil	vstřelit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
do	do	k7c2
sítě	síť	k1gFnSc2
Celty	celta	k1gFnSc2
Vigo	Vigo	k6eAd1
a	a	k8xC
překonal	překonat	k5eAaPmAgInS
46	#num#	k4
let	léto	k1gNnPc2
starý	starý	k2eAgInSc1d1
rekord	rekord	k1gInSc1
Jimmyho	Jimmy	k1gMnSc2
Greavese	Greavese	k1gFnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vévodil	vévodit	k5eAaImAgMnS
střelcům	střelec	k1gMnPc3
pěti	pět	k4xCc2
největších	veliký	k2eAgFnPc2d3
evropských	evropský	k2eAgFnPc2d1
ligových	ligový	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
s	s	k7c7
366	#num#	k4
góly	gól	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
finále	finále	k1gNnSc6
dorovnal	dorovnat	k5eAaPmAgInS
další	další	k2eAgInSc1d1
počin	počin	k1gInSc1
Alfreda	Alfred	k1gMnSc2
Di	Di	k1gMnSc2
Stéfana	Stéfan	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
druhým	druhý	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
skórujícím	skórující	k2eAgNnSc6d1
ve	v	k7c6
třech	tři	k4xCgInPc6
finálových	finálový	k2eAgInPc6d1
duelech	duel	k1gInPc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
minulosti	minulost	k1gFnSc6
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvěma	dva	k4xCgFnPc7
góly	gól	k1gInPc4
Juventusu	Juventus	k1gInSc3
pomohl	pomoct	k5eAaPmAgInS
zvítězit	zvítězit	k5eAaPmF
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
jako	jako	k9
první	první	k4xOgInSc1
klub	klub	k1gInSc1
novodobé	novodobý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
obhájil	obhájit	k5eAaPmAgInS
prvenství	prvenství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
navíc	navíc	k6eAd1
zaznamenal	zaznamenat	k5eAaPmAgInS
600	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc1
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
–	–	k?
klubové	klubový	k2eAgNnSc1d1
a	a	k8xC
i	i	k9
reprezentační	reprezentační	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
srpna	srpen	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc4
v	v	k7c6
roli	role	k1gFnSc6
střídajícího	střídající	k2eAgInSc2d1
žolíka	žolík	k1gInSc2
zasadil	zasadit	k5eAaPmAgInS
jedním	jeden	k4xCgInSc7
gólem	gól	k1gInSc7
o	o	k7c4
výhru	výhra	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
nad	nad	k7c7
Barcelonou	Barcelona	k1gFnSc7
ve	v	k7c6
Španělském	španělský	k2eAgInSc6d1
superpoháru	superpohár	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslava	oslava	k1gFnSc1
gólu	gól	k1gInSc2
svlečením	svlečení	k1gNnSc7
dresu	dres	k1gInSc2
si	se	k3xPyFc3
vysloužila	vysloužit	k5eAaPmAgFnS
karetní	karetní	k2eAgInSc4d1
trest	trest	k1gInSc4
od	od	k7c2
rozhodčího	rozhodčí	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
později	pozdě	k6eAd2
Ronalda	Ronaldo	k1gNnSc2
po	po	k7c6
druhé	druhý	k4xOgFnSc6
žluté	žlutý	k2eAgFnSc6d1
kartě	karta	k1gFnSc6
za	za	k7c4
simulování	simulování	k1gNnSc4
v	v	k7c6
pokutovém	pokutový	k2eAgNnSc6d1
území	území	k1gNnSc6
soupeře	soupeř	k1gMnSc2
vyloučil	vyloučit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
179	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
zápase	zápas	k1gInSc6
uspěl	uspět	k5eAaPmAgMnS
„	„	k?
<g/>
Bílý	bílý	k2eAgInSc4d1
balet	balet	k1gInSc4
<g/>
“	“	k?
i	i	k9
bez	bez	k7c2
něho	on	k3xPp3gMnSc2
a	a	k8xC
získal	získat	k5eAaPmAgInS
tak	tak	k9
superpohár	superpohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2017	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
podruhé	podruhé	k6eAd1
za	za	k7c7
sebou	se	k3xPyFc7
cenu	cena	k1gFnSc4
pro	pro	k7c4
Nejlepšího	dobrý	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
podle	podle	k7c2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
180	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
měl	mít	k5eAaImAgInS
na	na	k7c6
kontě	konto	k1gNnSc6
jen	jen	k6eAd1
dva	dva	k4xCgInPc4
gólové	gólový	k2eAgInPc4d1
zásahy	zásah	k1gInPc4
ve	v	k7c6
španělské	španělský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
zato	zato	k6eAd1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
jich	on	k3xPp3gMnPc2
měl	mít	k5eAaImAgInS
po	po	k7c6
skupinové	skupinový	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
devět	devět	k4xCc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
skórovat	skórovat	k5eAaBmF
ve	v	k7c6
všech	všecek	k3xTgInPc6
šesti	šest	k4xCc6
skupinových	skupinový	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
to	ten	k3xDgNnSc1
podařilo	podařit	k5eAaPmAgNnS
i	i	k9
proti	proti	k7c3
Borussii	Borussie	k1gFnSc3
Dortmund	Dortmund	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Následně	následně	k6eAd1
obdržel	obdržet	k5eAaPmAgMnS
další	další	k2eAgNnSc4d1
individuální	individuální	k2eAgNnSc4d1
ocenění	ocenění	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jej	on	k3xPp3gInSc4
získal	získat	k5eAaPmAgMnS
popáté	popáté	k4xO
<g/>
,	,	kIx,
dorovnal	dorovnat	k5eAaPmAgInS
počet	počet	k1gInSc1
těchto	tento	k3xDgNnPc2
ocenění	ocenění	k1gNnPc2
držený	držený	k2eAgInSc1d1
Lionelem	Lionel	k1gMnSc7
Messim	Messim	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
182	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
posléze	posléze	k6eAd1
16	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
prosadil	prosadit	k5eAaPmAgMnS
z	z	k7c2
přímého	přímý	k2eAgInSc2d1
kopu	kop	k1gInSc2
ve	v	k7c6
finále	finále	k1gNnSc6
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
klubů	klub	k1gInPc2
FIFA	FIFA	kA
proti	proti	k7c3
Grê	Grê	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
skončilo	skončit	k5eAaPmAgNnS
výhrou	výhra	k1gFnSc7
Realu	Real	k1gInSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
obhajobou	obhajoba	k1gFnSc7
této	tento	k3xDgFnSc2
soutěže	soutěž	k1gFnSc2
a	a	k8xC
pátou	pátá	k1gFnSc4
trofejí	trofej	k1gFnPc2
za	za	k7c4
kalendářní	kalendářní	k2eAgInSc4d1
rok	rok	k1gInSc4
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
183	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třetí	třetí	k4xOgInSc1
březnový	březnový	k2eAgInSc1d1
den	den	k1gInSc1
roku	rok	k1gInSc2
2018	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgMnSc1
300	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
ve	v	k7c4
španělské	španělský	k2eAgNnSc4d1
La	la	k1gNnSc4
Lize	liga	k1gFnSc3
do	do	k7c2
sítě	síť	k1gFnSc2
Getafe	Getaf	k1gMnSc5
<g/>
,	,	kIx,
na	na	k7c4
což	což	k3yRnSc1,k3yQnSc1
mu	on	k3xPp3gMnSc3
stačilo	stačit	k5eAaBmAgNnS
286	#num#	k4
zápasů	zápas	k1gInPc2
–	–	k?
nikdo	nikdo	k3yNnSc1
jiný	jiný	k1gMnSc1
této	tento	k3xDgFnSc2
mety	meta	k1gFnSc2
nedosáhl	dosáhnout	k5eNaPmAgInS
tak	tak	k6eAd1
rychle	rychle	k6eAd1
jako	jako	k8xS,k8xC
on	on	k3xPp3gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
184	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
pak	pak	k6eAd1
Real	Real	k1gInSc1
porazil	porazit	k5eAaPmAgInS
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
Gironu	Giron	k1gInSc2
6	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
,	,	kIx,
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
Ronaldo	Ronaldo	k1gNnSc1
čtyřmi	čtyři	k4xCgNnPc7
góly	gól	k1gInPc4
a	a	k8xC
zaznamenal	zaznamenat	k5eAaPmAgInS
50	#num#	k4
<g/>
.	.	kIx.
hattrick	hattrick	k1gInSc1
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
185	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
čtvrtfinálovém	čtvrtfinálový	k2eAgNnSc6d1
březnovém	březnový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
na	na	k7c6
hřišti	hřiště	k1gNnSc6
Juventusu	Juventus	k1gInSc2
dvěma	dva	k4xCgInPc7
góly	gól	k1gInPc7
podpořil	podpořit	k5eAaPmAgMnS
týmovou	týmový	k2eAgFnSc4d1
výhru	výhra	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozornost	pozornost	k1gFnSc1
si	se	k3xPyFc3
získal	získat	k5eAaPmAgMnS
hlavně	hlavně	k9
druhým	druhý	k4xOgInSc7
z	z	k7c2
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
předvedl	předvést	k5eAaPmAgMnS
„	„	k?
<g/>
nůžkami	nůžky	k1gFnPc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soupeřův	soupeřův	k2eAgMnSc1d1
stoper	stoper	k1gMnSc1
Andrea	Andrea	k1gFnSc1
Barzagli	Barzagle	k1gFnSc6
tento	tento	k3xDgInSc4
gól	gól	k1gInSc4
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
„	„	k?
<g/>
gólem	gól	k1gInSc7
z	z	k7c2
Playstationu	Playstation	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diváci	divák	k1gMnPc1
v	v	k7c6
turínském	turínský	k2eAgNnSc6d1
hledišti	hlediště	k1gNnSc6
mu	on	k3xPp3gMnSc3
uznale	uznale	k6eAd1
zatleskali	zatleskat	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Real	Real	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
dubnové	dubnový	k2eAgFnSc2d1
odvety	odveta	k1gFnSc2
přiblížil	přiblížit	k5eAaPmAgMnS
vlastnímu	vlastní	k2eAgNnSc3d1
vyřazení	vyřazení	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
Ronaldův	Ronaldův	k2eAgInSc4d1
gól	gól	k1gInSc4
z	z	k7c2
penalty	penalta	k1gFnSc2
v	v	k7c6
98	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
nastavení	nastavení	k1gNnSc4
znamenal	znamenat	k5eAaImAgInS
postup	postup	k1gInSc1
do	do	k7c2
semifinále	semifinále	k1gNnSc2
po	po	k7c6
celkovém	celkový	k2eAgInSc6d1
výsledku	výsledek	k1gInSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Juventusu	Juventus	k1gInSc3
se	se	k3xPyFc4
Ronaldo	Ronalda	k1gFnSc5
prosadil	prosadit	k5eAaPmAgMnS
podesáté	podesáté	k4xO
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
kariéře	kariéra	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
rekordem	rekord	k1gInSc7
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
ve	v	k7c6
finále	finále	k1gNnSc6
porazil	porazit	k5eAaPmAgInS
Liverpool	Liverpool	k1gInSc1
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
prvenství	prvenství	k1gNnSc4
obhájil	obhájit	k5eAaPmAgMnS
a	a	k8xC
získal	získat	k5eAaPmAgMnS
potřetí	potřetí	k4xO
v	v	k7c6
řadě	řada	k1gFnSc6
za	za	k7c7
sebou	se	k3xPyFc7
a	a	k8xC
Ronaldo	Ronaldo	k1gNnSc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
novodobé	novodobý	k2eAgFnSc2d1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
s	s	k7c7
pěti	pět	k4xCc7
triumfy	triumf	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
188	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Juventus	Juventus	k1gInSc1
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
v	v	k7c6
Juventusu	Juventus	k1gInSc6
<g/>
,	,	kIx,
2019	#num#	k4
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
posilou	posila	k1gFnSc7
italského	italský	k2eAgInSc2d1
Juventusu	Juventus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
za	za	k7c7
33	#num#	k4
<g/>
letého	letý	k2eAgMnSc4d1
útočníka	útočník	k1gMnSc4
zaplatil	zaplatit	k5eAaPmAgMnS
částku	částka	k1gFnSc4
88,3	88,3	k4
milionu	milion	k4xCgInSc2
liber	libra	k1gFnPc2
(	(	kIx(
<g/>
neboli	neboli	k8xC
100	#num#	k4
milionů	milion	k4xCgInPc2
eur	euro	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
s	s	k7c7
hráčem	hráč	k1gMnSc7
podepsal	podepsat	k5eAaPmAgInS
čtyřletý	čtyřletý	k2eAgInSc1d1
kontrakt	kontrakt	k1gInSc1
s	s	k7c7
týdenním	týdenní	k2eAgInSc7d1
platem	plat	k1gInSc7
500	#num#	k4
tisíc	tisíc	k4xCgInPc2
liber	libra	k1gFnPc2
týdně	týdně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
189	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldův	Ronaldův	k2eAgInSc1d1
přestup	přestup	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
čtvrtým	čtvrtý	k4xOgInSc7
nejdražším	drahý	k2eAgInSc7d3
transferem	transfer	k1gInSc7
v	v	k7c6
dějinách	dějiny	k1gFnPc6
fotbalu	fotbal	k1gInSc2
a	a	k8xC
nejdražším	drahý	k2eAgInSc7d3
transferem	transfer	k1gInSc7
fotbalisty	fotbalista	k1gMnSc2
staršího	starší	k1gMnSc2
30	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
190	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc1
zapsal	zapsat	k5eAaPmAgInS
do	do	k7c2
dějin	dějiny	k1gFnPc2
jakožto	jakožto	k8xS
nejdražší	drahý	k2eAgInSc4d3
nákup	nákup	k1gInSc4
italského	italský	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
191	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
soutěžním	soutěžní	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
v	v	k7c6
novém	nový	k2eAgInSc6d1
týmu	tým	k1gInSc6
poprvé	poprvé	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
dne	den	k1gInSc2
16	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
v	v	k7c6
lize	liga	k1gFnSc6
proti	proti	k7c3
Sassuolu	Sassuol	k1gInSc3
a	a	k8xC
rovnou	rovnou	k6eAd1
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
přispěním	přispění	k1gNnSc7
vyhrál	vyhrát	k5eAaPmAgInS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
192	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhým	druhý	k4xOgInSc7
gólem	gól	k1gInSc7
se	se	k3xPyFc4
navíc	navíc	k6eAd1
zapsal	zapsat	k5eAaPmAgMnS
do	do	k7c2
historie	historie	k1gFnSc2
jako	jako	k8xS,k8xC
hráč	hráč	k1gMnSc1
se	s	k7c7
400	#num#	k4
góly	gól	k1gInPc7
v	v	k7c6
nejvyšších	vysoký	k2eAgFnPc6d3
ligových	ligový	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
naposledy	naposledy	k6eAd1
zapsal	zapsat	k5eAaPmAgMnS
Uwe	Uwe	k1gMnSc1
Seeler	Seeler	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
době	doba	k1gFnSc6
Ronaldovy	Ronaldův	k2eAgFnSc2d1
trefy	trefa	k1gFnSc2
už	už	k6eAd1
81	#num#	k4
<g/>
letý	letý	k2eAgMnSc1d1
muž	muž	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
193	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
s	s	k7c7
týmem	tým	k1gInSc7
odcestoval	odcestovat	k5eAaPmAgMnS
do	do	k7c2
Valencie	Valencie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
poprvé	poprvé	k6eAd1
zahrál	zahrát	k5eAaPmAgInS
Ligu	liga	k1gFnSc4
mistrů	mistr	k1gMnPc2
ve	v	k7c6
dresu	dres	k1gInSc6
turínského	turínský	k2eAgInSc2d1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
29	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
měl	mít	k5eAaImAgMnS
podle	podle	k7c2
rozhodčího	rozhodčí	k1gMnSc2
Felixe	Felix	k1gMnSc2
Brycha	Brych	k1gMnSc2
zatahat	zatahat	k5eAaPmF
za	za	k7c4
vlasy	vlas	k1gInPc4
obránce	obránce	k1gMnSc1
Valencie	Valencie	k1gFnSc2
Jeisona	Jeisona	k1gFnSc1
Murilla	Murilla	k1gFnSc1
a	a	k8xC
za	za	k7c4
tento	tento	k3xDgInSc4
údajný	údajný	k2eAgInSc4d1
přestupek	přestupek	k1gInSc4
byl	být	k5eAaImAgInS
vyloučen	vyloučit	k5eAaPmNgInS
<g/>
,	,	kIx,
vůbec	vůbec	k9
poprvé	poprvé	k6eAd1
během	během	k7c2
154	#num#	k4
zápasů	zápas	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
góly	gól	k1gInPc1
Miralema	Miralem	k1gMnSc2
Pjaniće	Pjanić	k1gMnSc2
z	z	k7c2
penalty	penalta	k1gFnSc2
přesto	přesto	k6eAd1
přisoudily	přisoudit	k5eAaPmAgInP
tři	tři	k4xCgInPc1
body	bod	k1gInPc1
Juventusu	Juventus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
194	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
předposledním	předposlední	k2eAgNnSc6d1
pátém	pátý	k4xOgNnSc6
utkání	utkání	k1gNnPc4
proti	proti	k7c3
Valencii	Valencie	k1gFnSc3
vstřelil	vstřelit	k5eAaPmAgMnS
jediný	jediný	k2eAgMnSc1d1
gól	gól	k1gInSc4
domácího	domácí	k2eAgNnSc2d1
klání	klání	k1gNnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
vůbec	vůbec	k9
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
se	se	k3xPyFc4
stovkou	stovka	k1gFnSc7
vyhraných	vyhraný	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
195	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Fiorentině	Fiorentina	k1gFnSc3
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
na	na	k7c6
její	její	k3xOp3gFnSc6
půdě	půda	k1gFnSc6
zpečetil	zpečetit	k5eAaPmAgInS
vítězství	vítězství	k1gNnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
gólem	gól	k1gInSc7
z	z	k7c2
penalty	penalta	k1gFnSc2
v	v	k7c6
79	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaznamenal	zaznamenat	k5eAaPmAgMnS
tak	tak	k9
10	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc1
ve	v	k7c6
14	#num#	k4
utkáních	utkání	k1gNnPc6
Serie	serie	k1gFnSc2
A	a	k8xC
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
se	se	k3xPyFc4
hráči	hráč	k1gMnPc7
Juventusu	Juventus	k1gInSc2
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
premiérové	premiérový	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
zdařilo	zdařit	k5eAaPmAgNnS
naposledy	naposledy	k6eAd1
Johnu	John	k1gMnSc3
Charlesovi	Charles	k1gMnSc3
z	z	k7c2
Walesu	Wales	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1957	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
196	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
prosince	prosinec	k1gInSc2
nedosáhl	dosáhnout	k5eNaPmAgInS
na	na	k7c4
ocenění	ocenění	k1gNnSc4
Ballon	Ballon	k1gInSc4
d	d	k?
<g/>
’	’	k?
<g/>
Or	Or	k1gFnSc2
(	(	kIx(
<g/>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
skončil	skončit	k5eAaPmAgMnS
za	za	k7c7
bývalým	bývalý	k2eAgMnSc7d1
spoluhráčem	spoluhráč	k1gMnSc7
z	z	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
,	,	kIx,
středopolařem	středopolař	k1gMnSc7
Lukou	Luka	k1gMnSc7
Modrićem	Modrić	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
anketě	anketa	k1gFnSc6
již	již	k6eAd1
pošesté	pošesté	k4xO
umístil	umístit	k5eAaPmAgMnS
jako	jako	k9
druhý	druhý	k4xOgInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
197	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c7
Modrićem	Modrić	k1gInSc7
skončil	skončit	k5eAaPmAgInS
druhý	druhý	k4xOgInSc1
také	také	k9
v	v	k7c6
srpnu	srpen	k1gInSc6
při	při	k7c6
vyhlašování	vyhlašování	k1gNnSc6
ceny	cena	k1gFnSc2
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
evropského	evropský	k2eAgMnSc4d1
hráče	hráč	k1gMnSc4
podle	podle	k7c2
UEFA	UEFA	kA
za	za	k7c4
sezónu	sezóna	k1gFnSc4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
198	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k8xC
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc4
opět	opět	k6eAd1
Modrić	Modrić	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
jej	on	k3xPp3gMnSc4
v	v	k7c6
září	září	k1gNnSc6
připravil	připravit	k5eAaPmAgInS
o	o	k7c4
ocenění	ocenění	k1gNnSc4
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
fotbalistu	fotbalista	k1gMnSc4
podle	podle	k7c2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
199	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
AC	AC	kA
Milán	Milán	k1gInSc1
16	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
vybojoval	vybojovat	k5eAaPmAgInS
první	první	k4xOgFnSc4
trofej	trofej	k1gFnSc4
v	v	k7c6
Turíně	Turín	k1gInSc6
jediným	jediný	k2eAgInSc7d1
gólem	gól	k1gInSc7
utkání	utkání	k1gNnSc2
italského	italský	k2eAgInSc2d1
superpoháru	superpohár	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
200	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc4
dále	daleko	k6eAd2
skóroval	skórovat	k5eAaBmAgInS
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
v	v	k7c6
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Sassuolu	Sassuol	k1gInSc3
při	při	k7c6
výhře	výhra	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podeváté	podeváté	k4xO
v	v	k7c6
řadě	řada	k1gFnSc6
tak	tak	k6eAd1
dal	dát	k5eAaPmAgMnS
gól	gól	k1gInSc4
ve	v	k7c6
venkovním	venkovní	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
naposledy	naposledy	k6eAd1
podařilo	podařit	k5eAaPmAgNnS
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
útočníkovi	útočník	k1gMnSc3
Giuseppemu	Giuseppem	k1gMnSc3
Signoriovi	Signorius	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
201	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
zápas	zápas	k1gInSc1
osmifinále	osmifinále	k1gNnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
s	s	k7c7
Atléticem	Atlétic	k1gMnSc7
Madrid	Madrid	k1gInSc4
Juventus	Juventus	k1gInSc4
venku	venku	k6eAd1
prohrál	prohrát	k5eAaPmAgInS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
odvetě	odveta	k1gFnSc6
v	v	k7c6
Turíně	Turín	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
na	na	k7c4
sebe	sebe	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc4
upozornil	upozornit	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
sám	sám	k3xTgMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
góly	gól	k1gInPc4
toho	ten	k3xDgInSc2
večera	večer	k1gInSc2
(	(	kIx(
<g/>
dva	dva	k4xCgInPc1
góly	gól	k1gInPc1
hlavou	hlava	k1gFnSc7
a	a	k8xC
proměněná	proměněný	k2eAgFnSc1d1
penalta	penalta	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
postup	postup	k1gInSc4
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
202	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Ajaxu	Ajax	k1gInSc3
v	v	k7c6
další	další	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
zaznamenal	zaznamenat	k5eAaPmAgInS
další	další	k2eAgInSc4d1
gól	gól	k1gInSc4
hlavou	hlava	k1gFnSc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
125	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dorovnal	dorovnat	k5eAaPmAgMnS
Zlatana	Zlatan	k1gMnSc4
Ibrahimoviće	Ibrahimović	k1gMnSc4
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgMnSc7
byl	být	k5eAaImAgInS
po	po	k7c6
tomto	tento	k3xDgInSc6
zápase	zápas	k1gInSc6
historicky	historicky	k6eAd1
nejlepším	dobrý	k2eAgMnSc7d3
střelce	střelka	k1gFnSc3
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
v	v	k7c6
její	její	k3xOp3gFnSc6
novodobé	novodobý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
203	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Domácí	domácí	k2eAgFnSc4d1
odvetu	odveta	k1gFnSc4
Juventus	Juventus	k1gInSc1
nezvládl	zvládnout	k5eNaPmAgInS
a	a	k8xC
po	po	k7c6
porážce	porážka	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
vypadl	vypadnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
otevřel	otevřít	k5eAaPmAgMnS
skóre	skóre	k1gNnSc4
tohoto	tento	k3xDgNnSc2
klání	klání	k1gNnSc2
v	v	k7c6
první	první	k4xOgFnSc6
půli	půle	k1gFnSc6
<g/>
,	,	kIx,
Nizozemci	Nizozemec	k1gMnPc1
dvěma	dva	k4xCgFnPc7
góly	gól	k1gInPc4
zápas	zápas	k1gInSc4
otočili	otočit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
204	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Utkání	utkání	k1gNnSc1
33	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
Serie	serie	k1gFnSc2
A	a	k8xC
dne	den	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
mezi	mezi	k7c7
domácím	domácí	k2eAgInSc7d1
Juventusem	Juventus	k1gInSc7
a	a	k8xC
hostující	hostující	k2eAgFnSc7d1
Fiorentinou	Fiorentina	k1gFnSc7
rozhodlo	rozhodnout	k5eAaPmAgNnS
o	o	k7c6
osmém	osmý	k4xOgInSc6
titulu	titul	k1gInSc6
pro	pro	k7c4
Juventus	Juventus	k1gInSc4
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fiorentina	Fiorentina	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Ronaldův	Ronaldův	k2eAgInSc1d1
centr	centr	k1gInSc1
si	se	k3xPyFc3
do	do	k7c2
vlastní	vlastní	k2eAgFnSc2d1
brány	brána	k1gFnSc2
srazil	srazit	k5eAaPmAgMnS
obránce	obránce	k1gMnSc1
German	German	k1gMnSc1
Pezzella	Pezzella	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
205	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
derby	derby	k1gNnSc6
proti	proti	k7c3
Interu	Intero	k1gNnSc3
Milán	Milán	k1gInSc1
následující	následující	k2eAgInSc4d1
týden	týden	k1gInSc4
vstřelil	vstřelit	k5eAaPmAgMnS
600	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc4
klubové	klubový	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
a	a	k8xC
dopomohl	dopomoct	k5eAaPmAgMnS
k	k	k7c3
remíze	remíza	k1gFnSc3
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
venku	venku	k6eAd1
v	v	k7c6
Miláně	Milán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
206	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
května	květen	k1gInSc2
obdržel	obdržet	k5eAaPmAgMnS
ocenění	ocenění	k1gNnSc4
pro	pro	k7c4
nejužitečnějšího	užitečný	k2eAgMnSc4d3
hráče	hráč	k1gMnSc4
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
si	se	k3xPyFc3
zasloužil	zasloužit	k5eAaPmAgMnS
22	#num#	k4
góly	gól	k1gInPc4
a	a	k8xC
11	#num#	k4
asistencemi	asistence	k1gFnPc7
napříč	napříč	k6eAd1
33	#num#	k4
zápasy	zápas	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
207	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stejně	stejně	k6eAd1
nebo	nebo	k8xC
více	hodně	k6eAd2
gólů	gól	k1gInPc2
než	než	k8xS
on	on	k3xPp3gMnSc1
v	v	k7c6
italské	italský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
vstřelili	vstřelit	k5eAaPmAgMnP
jen	jen	k9
Capocannoniere	Capocannonier	k1gInSc5
Fabio	Fabio	k6eAd1
Quagliarella	Quagliarell	k1gMnSc2
(	(	kIx(
<g/>
26	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Duván	Duván	k1gInSc1
Zapata	Zapata	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Krzysztof	Krzysztof	k1gMnSc1
Piątek	Piątek	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
208	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc4
získal	získat	k5eAaPmAgMnS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
svůj	svůj	k3xOyFgInSc4
šestý	šestý	k4xOgInSc4
ligový	ligový	k2eAgInSc4d1
titul	titul	k1gInSc4
v	v	k7c6
kariéře	kariéra	k1gFnSc6
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	s	k7c7
prvním	první	k4xOgMnSc7
hráčem	hráč	k1gMnSc7
<g/>
,	,	kIx,
kterému	který	k3yQgMnSc3,k3yIgMnSc3,k3yRgMnSc3
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
zdařilo	zdařit	k5eAaPmAgNnS
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
Španělsku	Španělsko	k1gNnSc6
a	a	k8xC
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
209	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
kole	kolo	k1gNnSc6
ligové	ligový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
na	na	k7c6
konci	konec	k1gInSc6
srpna	srpen	k1gInSc2
Juventus	Juventus	k1gInSc1
přivítal	přivítat	k5eAaPmAgInS
Neapol	Neapol	k1gFnSc4
a	a	k8xC
v	v	k7c6
62	#num#	k4
<g/>
.	.	kIx.
minutě	minuta	k1gFnSc6
vedl	vést	k5eAaImAgMnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
co	co	k9
Ronaldo	Ronaldo	k1gNnSc1
svým	svůj	k3xOyFgMnPc3
prvním	první	k4xOgInSc7
gólem	gól	k1gInSc7
sezóny	sezóna	k1gFnSc2
navýšil	navýšit	k5eAaPmAgInS
vedení	vedení	k1gNnSc4
po	po	k7c6
centru	centrum	k1gNnSc6
Douglase	Douglas	k1gInSc6
Costy	Costa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupeř	soupeř	k1gMnSc1
se	se	k3xPyFc4
vzmohl	vzmoct	k5eAaPmAgMnS
na	na	k7c4
vyrovnání	vyrovnání	k1gNnSc4
skóre	skóre	k1gNnSc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
vlastním	vlastní	k2eAgInSc6d1
gólu	gól	k1gInSc6
Kalidoua	Kalidouus	k1gMnSc2
Koulibalyho	Koulibaly	k1gMnSc2
nakonec	nakonec	k6eAd1
prohrál	prohrát	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
210	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
září	září	k1gNnSc2
byl	být	k5eAaImAgMnS
vyhlášen	vyhlášen	k2eAgMnSc1d1
vítěz	vítěz	k1gMnSc1
ceny	cena	k1gFnSc2
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
FIFA	FIFA	kA
a	a	k8xC
oceněným	oceněný	k2eAgMnPc3d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
skončil	skončit	k5eAaPmAgInS
třetí	třetí	k4xOgFnSc4
za	za	k7c2
Messim	Messima	k1gFnPc2
a	a	k8xC
van	van	k1gInSc1
Dijkem	Dijek	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
211	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgNnSc6
skupinovém	skupinový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2019	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
třetí	třetí	k4xOgInSc4
závěrečný	závěrečný	k2eAgInSc4d1
gól	gól	k1gInSc4
do	do	k7c2
sítě	síť	k1gFnSc2
Leverkusenu	Leverkusen	k2eAgFnSc4d1
při	při	k7c6
výhře	výhra	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
tímto	tento	k3xDgInSc7
vyhrál	vyhrát	k5eAaPmAgInS
105	#num#	k4
<g/>
.	.	kIx.
utkání	utkání	k1gNnSc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
v	v	k7c6
kariéře	kariéra	k1gFnSc6
a	a	k8xC
překonal	překonat	k5eAaPmAgMnS
Ikera	Iker	k1gMnSc4
Casillase	Casillasa	k1gFnSc6
<g/>
,	,	kIx,
předchozího	předchozí	k2eAgMnSc2d1
držitele	držitel	k1gMnSc2
rekordu	rekord	k1gInSc2
v	v	k7c6
počtu	počet	k1gInSc6
vítězných	vítězný	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
(	(	kIx(
<g/>
104	#num#	k4
utkání	utkání	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zároveň	zároveň	k6eAd1
dal	dát	k5eAaPmAgMnS
gól	gól	k1gInSc4
ve	v	k7c4
14	#num#	k4
po	po	k7c6
sobě	se	k3xPyFc3
jdoucích	jdoucí	k2eAgFnPc6d1
sezónách	sezóna	k1gFnPc6
v	v	k7c6
této	tento	k3xDgFnSc6
evropské	evropský	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
<g/>
,	,	kIx,
jedinými	jediný	k2eAgMnPc7d1
dalšími	další	k2eAgMnPc7d1
fotbalisty	fotbalista	k1gMnPc7
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
povedlo	povést	k5eAaPmAgNnS
jsou	být	k5eAaImIp3nP
Raúl	Raúl	k1gMnSc1
a	a	k8xC
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
skupině	skupina	k1gFnSc6
proti	proti	k7c3
Lokomotivu	lokomotiva	k1gFnSc4
Moskva	Moskva	k1gFnSc1
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
na	na	k7c6
venkovní	venkovní	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Moskvě	Moskva	k1gFnSc6
odehrál	odehrát	k5eAaPmAgInS
své	své	k1gNnSc4
174	#num#	k4
<g/>
.	.	kIx.
utkání	utkání	k1gNnSc6
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
a	a	k8xC
dorovnal	dorovnat	k5eAaPmAgMnS
tak	tak	k6eAd1
Paola	Paol	k1gMnSc4
Maldiniho	Maldini	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
utkání	utkání	k1gNnPc4
odehrál	odehrát	k5eAaPmAgMnS
jen	jen	k6eAd1
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
188	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
213	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Sampdorii	Sampdorie	k1gFnSc3
Janov	Janov	k1gInSc1
18	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
se	se	k3xPyFc4
gólově	gólově	k6eAd1
prosadil	prosadit	k5eAaPmAgInS
hlavou	hlava	k1gFnSc7
po	po	k7c6
centru	centrum	k1gNnSc6
Alexe	Alex	k1gMnSc5
Sandra	Sandra	k1gFnSc1
a	a	k8xC
tento	tento	k3xDgInSc4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vítězný	vítězný	k2eAgInSc4d1
gól	gól	k1gInSc4
pomohl	pomoct	k5eAaPmAgInS
k	k	k7c3
výhře	výhra	k1gFnSc3
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdiv	obdiv	k1gInSc4
si	se	k3xPyFc3
získal	získat	k5eAaPmAgMnS
pro	pro	k7c4
neobvyklý	obvyklý	k2eNgInSc4d1
výskok	výskok	k1gInSc4
do	do	k7c2
výšky	výška	k1gFnSc2
2,56	2,56	k4
metru	metr	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
tomuto	tento	k3xDgInSc3
gólu	gól	k1gInSc3
předcházel	předcházet	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldova	Ronaldův	k2eAgFnSc1d1
hlava	hlava	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
výše	výše	k1gFnSc1
než	než	k8xS
je	být	k5eAaImIp3nS
úroveň	úroveň	k1gFnSc4
břevna	břevno	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
pas	pas	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
úrovni	úroveň	k1gFnSc6
hlavy	hlava	k1gFnSc2
na	na	k7c6
zemi	zem	k1gFnSc6
stojících	stojící	k2eAgMnPc2d1
protihráčů	protihráč	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soupeřův	soupeřův	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Claudio	Claudio	k1gMnSc1
Ranieri	Raniere	k1gFnSc4
poznamenal	poznamenat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
podobné	podobný	k2eAgInPc1d1
výskoky	výskok	k1gInPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
vidění	vidění	k1gNnSc3
v	v	k7c6
NBA	NBA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
214	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Domácí	domácí	k2eAgMnPc1d1
diváci	divák	k1gMnPc1
v	v	k7c6
Turíně	Turín	k1gInSc6
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
byli	být	k5eAaImAgMnP
svědky	svědek	k1gMnPc4
premiérového	premiérový	k2eAgInSc2d1
Ronaldova	Ronaldův	k2eAgInSc2d1
hattricku	hattrick	k1gInSc2
v	v	k7c6
italské	italský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
proti	proti	k7c3
Cagliari	Cagliar	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
čtyři	čtyři	k4xCgInPc1
góly	gól	k1gInPc1
padly	padnout	k5eAaImAgInP,k5eAaPmAgInP
ve	v	k7c6
druhém	druhý	k4xOgInSc6
poločase	poločas	k1gInSc6
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
druhý	druhý	k4xOgInSc1
si	se	k3xPyFc3
Ronaldo	Ronaldo	k1gNnSc4
připsal	připsat	k5eAaPmAgMnS
proměněnou	proměněný	k2eAgFnSc7d1
penaltou	penalta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
svým	svůj	k3xOyFgMnSc7
posledním	poslední	k2eAgInSc7d1
gólem	gól	k1gInSc7
na	na	k7c6
konečných	konečný	k2eAgInPc6d1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
přidal	přidat	k5eAaPmAgMnS
ještě	ještě	k9
asistenci	asistence	k1gFnSc4
na	na	k7c4
gól	gól	k1gInSc4
Gonzala	Gonzala	k1gMnSc2
Higuaína	Higuaín	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Alexisovi	Alexis	k1gMnSc6
Sánchezovi	Sánchez	k1gMnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
dalším	další	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zaznamenal	zaznamenat	k5eAaPmAgMnS
hattrick	hattrick	k1gInSc4
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
<g/>
,	,	kIx,
španělské	španělský	k2eAgFnSc6d1
i	i	k8xC
italské	italský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
215	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Fiorentině	Fiorentina	k1gFnSc3
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
skóroval	skórovat	k5eAaBmAgMnS
v	v	k7c6
devátém	devátý	k4xOgInSc6
ligovém	ligový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
rovnalo	rovnat	k5eAaImAgNnS
klubovému	klubový	k2eAgInSc3d1
rekordu	rekord	k1gInSc3
Davida	David	k1gMnSc2
Trézégueta	Trézéguet	k1gMnSc2
<g/>
,	,	kIx,
dřívější	dřívější	k2eAgFnSc6d1
ikoně	ikona	k1gFnSc6
Juventusu	Juventus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgMnSc3
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
povedlo	povést	k5eAaPmAgNnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
216	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
sice	sice	k8xC
v	v	k7c6
dalším	další	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
rekord	rekord	k1gInSc4
překonal	překonat	k5eAaPmAgMnS
a	a	k8xC
skóroval	skórovat	k5eAaBmAgMnS
i	i	k9
proti	proti	k7c3
Hellasu	Hellas	k1gInSc2
Verona	Verona	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
Juventus	Juventus	k1gInSc1
prohrál	prohrát	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
217	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rekordní	rekordní	k2eAgFnSc1d1
řada	řada	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
11	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
proti	proti	k7c3
klubu	klub	k1gInSc3
S.	S.	kA
<g/>
P.	P.	kA
<g/>
A.L.	A.L.	k1gFnPc2
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc1
tak	tak	k8xS,k8xC
jedním	jeden	k4xCgInSc7
gólem	gól	k1gInSc7
podpořil	podpořit	k5eAaPmAgMnS
venkovní	venkovní	k2eAgFnSc4d1
výhru	výhra	k1gFnSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
šlo	jít	k5eAaImAgNnS
o	o	k7c4
jeho	jeho	k3xOp3gNnPc2
1000	#num#	k4
<g/>
.	.	kIx.
utkání	utkání	k1gNnSc6
profesionální	profesionální	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgInSc2
vstřelil	vstřelit	k5eAaPmAgMnS
21	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc1
této	tento	k3xDgFnSc2
ligové	ligový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
v	v	k7c6
21	#num#	k4
zápasech	zápas	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Série	série	k1gFnSc1
11	#num#	k4
ligových	ligový	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
s	s	k7c7
gólem	gól	k1gInSc7
na	na	k7c6
kontě	konto	k1gNnSc6
znamenala	znamenat	k5eAaImAgFnS
dorovnání	dorovnání	k1gNnSc4
rekordu	rekord	k1gInSc2
v	v	k7c6
italské	italský	k2eAgFnSc6d1
lize	liga	k1gFnSc6
dvojice	dvojice	k1gFnSc1
Gabriel	Gabriela	k1gFnPc2
Batistuta	Batistut	k2eAgFnSc1d1
a	a	k8xC
Fabio	Fabio	k6eAd1
Quagliarella	Quagliarella	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
odehrál	odehrát	k5eAaPmAgMnS
finále	finále	k1gNnSc4
domácího	domácí	k2eAgInSc2d1
poháru	pohár	k1gInSc2
proti	proti	k7c3
Neapoli	Neapol	k1gFnSc3
<g/>
,	,	kIx,
na	na	k7c4
brankáře	brankář	k1gMnSc4
Alexe	Alex	k1gMnSc5
Mereta	Meret	k1gMnSc4
ale	ale	k9
nevyzrál	vyzrát	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
120	#num#	k4
minut	minuta	k1gFnPc2
prodlužovaného	prodlužovaný	k2eAgInSc2d1
střetu	střet	k1gInSc2
nepadl	padnout	k5eNaPmAgInS
gól	gól	k1gInSc4
a	a	k8xC
na	na	k7c4
řadu	řada	k1gFnSc4
přišel	přijít	k5eAaPmAgInS
penaltový	penaltový	k2eAgInSc1d1
rozstřel	rozstřel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gInSc6
uspěli	uspět	k5eAaPmAgMnP
hráči	hráč	k1gMnPc1
Neapole	Neapol	k1gFnSc2
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
218	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
pět	pět	k4xCc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
dal	dát	k5eAaPmAgMnS
v	v	k7c6
prvním	první	k4xOgInSc6
poločase	poločas	k1gInSc6
z	z	k7c2
penalty	penalty	k1gNnSc2
gól	gól	k1gInSc1
do	do	k7c2
sítě	síť	k1gFnSc2
Bologni	Bologna	k1gFnSc3
a	a	k8xC
přispěl	přispět	k5eAaPmAgMnS
k	k	k7c3
výhře	výhra	k1gFnSc3
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
svým	svůj	k3xOyFgInSc7
22	#num#	k4
ligovým	ligový	k2eAgInSc7d1
gólem	gól	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
nejgólovějším	gólový	k2eAgMnSc7d3
Portugalcem	Portugalec	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
Serie	serie	k1gFnSc2
A	A	kA
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
překonal	překonat	k5eAaPmAgMnS
Rui	Rui	k1gMnSc3
Costu	Cost	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
219	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Juventus	Juventus	k1gInSc1
se	se	k3xPyFc4
držel	držet	k5eAaImAgInS
na	na	k7c6
špici	špice	k1gFnSc6
peletonu	peleton	k1gInSc2
před	před	k7c7
Laziem	Lazium	k1gNnSc7
i	i	k9
díky	díky	k7c3
venkovní	venkovní	k2eAgFnSc3d1
výhře	výhra	k1gFnSc3
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
nad	nad	k7c7
Janovem	Janov	k1gInSc7
<g/>
,	,	kIx,
již	již	k6eAd1
Ronaldo	Ronaldo	k1gNnSc1
podpořil	podpořit	k5eAaPmAgInS
přesným	přesný	k2eAgInSc7d1
zásahem	zásah	k1gInSc7
z	z	k7c2
dalekosáhlé	dalekosáhlý	k2eAgFnSc2d1
střely	střela	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
220	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soutěžním	soutěžní	k2eAgInSc7d1
gólem	gól	k1gInSc7
číslo	číslo	k1gNnSc1
746	#num#	k4
dotáhl	dotáhnout	k5eAaPmAgMnS
Ference	Ferenc	k1gMnSc4
Puskáse	Puskás	k1gMnSc4
na	na	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
místě	místo	k1gNnSc6
nejlepších	dobrý	k2eAgMnPc2d3
střelců	střelec	k1gMnPc2
fotbalových	fotbalový	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
221	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Třetí	třetí	k4xOgFnSc4
sezónu	sezóna	k1gFnSc4
v	v	k7c6
Juventusu	Juventus	k1gInSc6
zahájil	zahájit	k5eAaPmAgMnS
pod	pod	k7c7
novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Andreou	Andrea	k1gFnSc7
Pirlem	Pirl	k1gInSc7
záříjovým	záříjový	k2eAgInSc7d1
zápasem	zápas	k1gInSc7
proti	proti	k7c3
Sampdorii	Sampdorie	k1gFnSc3
Janov	Janov	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
vstřelil	vstřelit	k5eAaPmAgMnS
třetí	třetí	k4xOgInSc4
gól	gól	k1gInSc4
turínského	turínský	k2eAgInSc2d1
týmu	tým	k1gInSc2
při	při	k7c6
výhře	výhra	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
222	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
prodělal	prodělat	k5eAaPmAgMnS
nákazu	nákaza	k1gFnSc4
koronavirem	koronavirem	k6eAd1
a	a	k8xC
tak	tak	k6eAd1
musel	muset	k5eAaImAgMnS
vynechat	vynechat	k5eAaPmF
čtyři	čtyři	k4xCgInPc4
soutěžní	soutěžní	k2eAgInPc4d1
duely	duel	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
223	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
proti	proti	k7c3
Dynamu	dynamo	k1gNnSc3
Kyjev	Kyjev	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
dal	dát	k5eAaPmAgInS
750	#num#	k4
<g/>
.	.	kIx.
gól	gól	k1gInSc1
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
při	při	k7c6
výhře	výhra	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k8xC
proti	proti	k7c3
týmu	tým	k1gInSc3
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
dal	dát	k5eAaPmAgInS
svůj	svůj	k3xOyFgInSc4
první	první	k4xOgInSc4
gól	gól	k1gInSc4
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
13	#num#	k4
let	léto	k1gNnPc2
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
224	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
se	se	k3xPyFc4
dvakrát	dvakrát	k6eAd1
prosadil	prosadit	k5eAaPmAgMnS
z	z	k7c2
pokutového	pokutový	k2eAgInSc2d1
kopu	kop	k1gInSc2
v	v	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
duelu	duel	k1gInSc6
základní	základní	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
a	a	k8xC
to	ten	k3xDgNnSc4
na	na	k7c6
slavném	slavný	k2eAgInSc6d1
Camp	camp	k1gInSc1
Nou	Nou	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc1
jeho	jeho	k3xOp3gInPc4
první	první	k4xOgInPc4
dva	dva	k4xCgInPc4
góly	gól	k1gInPc4
v	v	k7c6
kariéře	kariéra	k1gFnSc6
proti	proti	k7c3
Barceloně	Barcelona	k1gFnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
milionářské	milionářský	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
225	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
hřišti	hřiště	k1gNnSc6
Janova	Janov	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
zaokrouhlil	zaokrouhlit	k5eAaPmAgInS
počet	počet	k1gInSc1
startů	start	k1gInPc2
za	za	k7c4
turínský	turínský	k2eAgInSc4d1
klub	klub	k1gInSc4
na	na	k7c4
stovku	stovka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
226	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dvakrát	dvakrát	k6eAd1
skóroval	skórovat	k5eAaBmAgMnS
z	z	k7c2
penalty	penalta	k1gFnSc2
a	a	k8xC
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
227	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
rozhodl	rozhodnout	k5eAaPmAgInS
o	o	k7c6
výsledku	výsledek	k1gInSc6
venkovního	venkovní	k2eAgInSc2d1
prvního	první	k4xOgNnSc2
zápasu	zápas	k1gInSc2
semifinále	semifinále	k1gNnSc4
italského	italský	k2eAgInSc2d1
poháru	pohár	k1gInSc2
Coppa	Coppa	k1gFnSc1
Italia	Italia	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
dvěma	dva	k4xCgInPc7
góly	gól	k1gInPc7
pomohl	pomoct	k5eAaPmAgMnS
porazit	porazit	k5eAaPmF
rivala	rival	k1gMnSc2
Inter	Inter	k1gInSc4
Milán	Milán	k1gInSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
228	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Spezii	Spezie	k1gFnSc3
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
odehrál	odehrát	k5eAaPmAgMnS
600	#num#	k4
<g/>
.	.	kIx.
ligový	ligový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
kariéry	kariéra	k1gFnSc2
a	a	k8xC
třetím	třetí	k4xOgInSc7
gólem	gól	k1gInSc7
zápasu	zápas	k1gInSc2
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosáhl	dosáhnout	k5eAaPmAgMnS
20	#num#	k4
gólů	gól	k1gInPc2
za	za	k7c4
21	#num#	k4
zápasů	zápas	k1gInPc2
Serie	serie	k1gFnSc2
A	A	kA
a	a	k8xC
podvanácté	podvanácté	k4xO
v	v	k7c6
řadě	řada	k1gFnSc6
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
ligové	ligový	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
podařilo	podařit	k5eAaPmAgNnS
přes	přes	k7c4
tuto	tento	k3xDgFnSc4
20	#num#	k4
gólovou	gólový	k2eAgFnSc4d1
metu	meta	k1gFnSc4
dostat	dostat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
229	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ačkoliv	ačkoliv	k8xS
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
Juventus	Juventus	k1gInSc1
domácí	domácí	k2eAgInSc1d1
odvetu	odveta	k1gFnSc4
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
proti	proti	k7c3
Portu	port	k1gInSc3
vyhrál	vyhrát	k5eAaPmAgMnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
(	(	kIx(
<g/>
venku	venku	k6eAd1
prohrál	prohrát	k5eAaPmAgInS
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vyřazen	vyřadit	k5eAaPmNgMnS
vlivem	vlivem	k7c2
pravidla	pravidlo	k1gNnSc2
o	o	k7c6
venkovních	venkovní	k2eAgInPc6d1
gólech	gól	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Italský	italský	k2eAgInSc1d1
deník	deník	k1gInSc1
Gazzetta	Gazzetto	k1gNnSc2
dello	dello	k1gNnSc1
Sport	sport	k1gInSc1
jej	on	k3xPp3gMnSc4
označil	označit	k5eAaPmAgInS
za	za	k7c4
zklamání	zklamání	k1gNnSc4
a	a	k8xC
také	také	k9
z	z	k7c2
dalších	další	k2eAgNnPc2d1
míst	místo	k1gNnPc2
se	se	k3xPyFc4
na	na	k7c4
Ronalda	Ronald	k1gMnSc4
snesla	snést	k5eAaPmAgFnS
kritika	kritika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
230	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
231	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedle	vedle	k7c2
kritiky	kritika	k1gFnSc2
výkonu	výkon	k1gInSc2
včetně	včetně	k7c2
chybného	chybný	k2eAgInSc2d1
výskoku	výskok	k1gInSc2
ve	v	k7c6
zdi	zeď	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
soupeři	soupeř	k1gMnSc3
skórovat	skórovat	k5eAaBmF
z	z	k7c2
přímého	přímý	k2eAgInSc2d1
kopu	kop	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc1
prvně	prvně	k?
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
gólově	gólově	k6eAd1
neprosadil	prosadit	k5eNaPmAgMnS
ve	v	k7c6
vyřazovacích	vyřazovací	k2eAgInPc6d1
bojích	boj	k1gInPc6
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
232	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
kritiku	kritika	k1gFnSc4
zareagoval	zareagovat	k5eAaPmAgMnS
hattrickem	hattrick	k1gInSc7
(	(	kIx(
<g/>
v	v	k7c6
kariéře	kariéra	k1gFnSc6
nově	nově	k6eAd1
celkem	celkem	k6eAd1
57	#num#	k4
<g/>
)	)	kIx)
proti	proti	k7c3
Cagliari	Cagliar	k1gInSc3
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k9
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
ledna	leden	k1gInSc2
2020	#num#	k4
vstřelil	vstřelit	k5eAaPmAgInS
jediný	jediný	k2eAgInSc1d1
předchozí	předchozí	k2eAgInSc1d1
hattrick	hattrick	k1gInSc1
za	za	k7c4
Juventus	Juventus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
233	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
</s>
<s>
V	v	k7c6
portugalské	portugalský	k2eAgFnSc6d1
reprezentaci	reprezentace	k1gFnSc6
debutoval	debutovat	k5eAaBmAgMnS
v	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2003	#num#	k4
v	v	k7c6
přátelském	přátelský	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Kazachstánu	Kazachstán	k1gInSc3
(	(	kIx(
<g/>
výhra	výhra	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
234	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
234	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
235	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2013	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
hattrick	hattrick	k1gInSc4
v	v	k7c6
odvetném	odvetný	k2eAgNnSc6d1
barážovém	barážový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
proti	proti	k7c3
domácímu	domácí	k2eAgInSc3d1
Švédsku	Švédsko	k1gNnSc6
a	a	k8xC
zařídil	zařídit	k5eAaPmAgMnS
tak	tak	k6eAd1
výhru	výhra	k1gFnSc4
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
a	a	k8xC
postup	postup	k1gInSc1
Portugalska	Portugalsko	k1gNnSc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2014	#num#	k4
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dotáhl	dotáhnout	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
v	v	k7c6
počtu	počet	k1gInSc6
reprezentačních	reprezentační	k2eAgFnPc2d1
branek	branka	k1gFnPc2
(	(	kIx(
<g/>
47	#num#	k4
<g/>
)	)	kIx)
na	na	k7c4
krajana	krajan	k1gMnSc4
Pauletu	Paulet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
zdatným	zdatný	k2eAgMnSc7d1
protivníkem	protivník	k1gMnSc7
byl	být	k5eAaImAgInS
v	v	k7c6
dresu	dres	k1gInSc6
Švédska	Švédsko	k1gNnSc2
Zlatan	Zlatan	k1gInSc4
Ibrahimović	Ibrahimović	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
skóroval	skórovat	k5eAaBmAgMnS
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Média	médium	k1gNnPc1
totiž	totiž	k8xC
střetnutí	střetnutí	k1gNnPc1
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
evropských	evropský	k2eAgInPc2d1
týmů	tým	k1gInPc2
popisovala	popisovat	k5eAaImAgFnS
jako	jako	k9
souboj	souboj	k1gInSc4
mezi	mezi	k7c7
Ronaldem	Ronald	k1gMnSc7
a	a	k8xC
Ibrahimovićem	Ibrahimović	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
dříve	dříve	k6eAd2
vstřelil	vstřelit	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
v	v	k7c6
prvním	první	k4xOgInSc6
utkání	utkání	k1gNnSc6
baráže	baráž	k1gFnSc2
jediný	jediný	k2eAgInSc4d1
a	a	k8xC
tudíž	tudíž	k8xC
vítězný	vítězný	k2eAgInSc4d1
gól	gól	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
236	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
EURO	euro	k1gNnSc1
2004	#num#	k4
</s>
<s>
Na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2004	#num#	k4
vstřelil	vstřelit	k5eAaPmAgInS
jediný	jediný	k2eAgInSc1d1
gól	gól	k1gInSc1
Portugalska	Portugalsko	k1gNnSc2
v	v	k7c6
prvním	první	k4xOgNnSc6
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Řecku	Řecko	k1gNnSc3
(	(	kIx(
<g/>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dal	dát	k5eAaPmAgMnS
i	i	k9
první	první	k4xOgInSc4
gól	gól	k1gInSc4
svého	svůj	k3xOyFgInSc2
týmu	tým	k1gInSc2
v	v	k7c6
semifinálovém	semifinálový	k2eAgNnSc6d1
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Nizozemsku	Nizozemsko	k1gNnSc3
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nakonec	nakonec	k6eAd1
Portugalsko	Portugalsko	k1gNnSc1
vyhrálo	vyhrát	k5eAaPmAgNnS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
finále	finále	k1gNnSc6
však	však	k9
jeho	jeho	k3xOp3gInSc1
tým	tým	k1gInSc1
opět	opět	k6eAd1
prohrál	prohrát	k5eAaPmAgInS
s	s	k7c7
Řeckem	Řecko	k1gNnSc7
(	(	kIx(
<g/>
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
2004	#num#	k4
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
se	se	k3xPyFc4
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
neukázalo	ukázat	k5eNaPmAgNnS
v	v	k7c6
nejlepším	dobrý	k2eAgNnSc6d3
světle	světlo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
lehce	lehko	k6eAd1
vypadající	vypadající	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
(	(	kIx(
<g/>
Irák	Irák	k1gInSc1
<g/>
,	,	kIx,
Maroko	Maroko	k1gNnSc1
<g/>
,	,	kIx,
Kostarika	Kostarika	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
Portugalsku	Portugalsko	k1gNnSc6
nepovedlo	povést	k5eNaPmAgNnS
postoupit	postoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
nejvíce	nejvíce	k6eAd1,k6eAd3
vytáhl	vytáhnout	k5eAaPmAgMnS
v	v	k7c6
utkání	utkání	k1gNnSc6
s	s	k7c7
Marokem	Maroko	k1gNnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vstřelil	vstřelit	k5eAaPmAgMnS
první	první	k4xOgFnSc4
branku	branka	k1gFnSc4
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
se	se	k3xPyFc4
představil	představit	k5eAaPmAgInS
ještě	ještě	k6eAd1
v	v	k7c6
utkání	utkání	k1gNnSc6
s	s	k7c7
Irákem	Irák	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
gólově	gólově	k6eAd1
neprosadil	prosadit	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
2006	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
<g/>
,	,	kIx,
už	už	k6eAd1
jako	jako	k8xS,k8xC
jeden	jeden	k4xCgInSc1
z	z	k7c2
tahounů	tahoun	k1gInPc2
mužstva	mužstvo	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dokázal	dokázat	k5eAaPmAgMnS
prosadit	prosadit	k5eAaPmF
i	i	k9
na	na	k7c6
tomto	tento	k3xDgInSc6
šampionátu	šampionát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
v	v	k7c6
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Íránu	Írán	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc4
sice	sice	k8xC
jeho	jeho	k3xOp3gNnSc4
jediný	jediný	k2eAgInSc1d1
gól	gól	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
to	ten	k3xDgNnSc1
ho	on	k3xPp3gMnSc4
nemuselo	muset	k5eNaImAgNnS
mrzet	mrzet	k5eAaImF
<g/>
,	,	kIx,
protože	protože	k8xS
dovedl	dovést	k5eAaPmAgMnS
Portugalsko	Portugalsko	k1gNnSc4
až	až	k6eAd1
do	do	k7c2
semifinále	semifinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
podlehli	podlehnout	k5eAaPmAgMnP
Francii	Francie	k1gFnSc4
i	i	k9
s	s	k7c7
legendárním	legendární	k2eAgMnSc7d1
Zidanem	Zidan	k1gMnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
utkání	utkání	k1gNnSc6
o	o	k7c4
bronz	bronz	k1gInSc4
hraném	hraný	k2eAgInSc6d1
na	na	k7c6
stadionu	stadion	k1gInSc6
Mercedes-Benz	Mercedes-Benza	k1gFnPc2
Arena	Aren	k1gInSc2
nastoupili	nastoupit	k5eAaPmAgMnP
Portugalci	Portugalec	k1gMnPc1
proti	proti	k7c3
Němcům	Němec	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
zápase	zápas	k1gInSc6
trápil	trápit	k5eAaImAgMnS
a	a	k8xC
s	s	k7c7
ním	on	k3xPp3gMnSc7
i	i	k9
celý	celý	k2eAgInSc4d1
portugalský	portugalský	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zápase	zápas	k1gInSc6
nakonec	nakonec	k6eAd1
Portugalci	Portugalec	k1gMnPc1
podlehli	podlehnout	k5eAaPmAgMnP
Němcům	Němec	k1gMnPc3
1	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
EURO	euro	k1gNnSc1
2008	#num#	k4
</s>
<s>
První	první	k4xOgInSc1
zápas	zápas	k1gInSc1
skupiny	skupina	k1gFnSc2
„	„	k?
<g/>
A	a	k8xC
<g/>
“	“	k?
proti	proti	k7c3
Turecku	Turecko	k1gNnSc3
skončil	skončit	k5eAaPmAgInS
pro	pro	k7c4
Portugalsko	Portugalsko	k1gNnSc4
vítězně	vítězně	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
237	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
zápase	zápas	k1gInSc6
hraném	hraný	k2eAgMnSc6d1
znovu	znovu	k6eAd1
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
se	se	k3xPyFc4
Ronaldo	Ronaldo	k1gNnSc1
postavil	postavit	k5eAaPmAgMnS
mužstvu	mužstvo	k1gNnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
u	u	k7c2
všech	všecek	k3xTgInPc2
třech	tři	k4xCgInPc2
gólů	gól	k1gInPc2
svého	svůj	k3xOyFgInSc2
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
vyhrálo	vyhrát	k5eAaPmAgNnS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
u	u	k7c2
gólové	gólový	k2eAgFnSc2d1
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
otevřel	otevřít	k5eAaPmAgMnS
skóre	skóre	k1gNnSc4
Deco	Deco	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
mu	on	k3xPp3gMnSc3
později	pozdě	k6eAd2
nahrával	nahrávat	k5eAaImAgMnS
na	na	k7c4
gól	gól	k1gInSc4
na	na	k7c4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěru	závěr	k1gInSc6
Ronaldo	Ronaldo	k1gNnSc4
ještě	ještě	k9
asistoval	asistovat	k5eAaImAgMnS
na	na	k7c4
třetí	třetí	k4xOgInSc4
gól	gól	k1gInSc4
Quaresmovi	Quaresm	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
238	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jistota	jistota	k1gFnSc1
postupu	postup	k1gInSc2
přiměla	přimět	k5eAaPmAgFnS
trenéra	trenér	k1gMnSc4
Scolariho	Scolari	k1gMnSc4
nechat	nechat	k5eAaPmF
odpočinout	odpočinout	k5eAaPmF
některé	některý	k3yIgFnPc1
hvězdy	hvězda	k1gFnPc1
včetně	včetně	k7c2
Ronalda	Ronaldo	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
proto	proto	k8xC
do	do	k7c2
třetího	třetí	k4xOgInSc2
zápasu	zápas	k1gInSc2
skupiny	skupina	k1gFnSc2
proti	proti	k7c3
pořadateli	pořadatel	k1gMnSc3
Švýcarsku	Švýcarsko	k1gNnSc6
(	(	kIx(
<g/>
prohra	prohra	k1gFnSc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
)	)	kIx)
nezasáhl	zasáhnout	k5eNaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
239	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
s	s	k7c7
Německem	Německo	k1gNnSc7
prohrávali	prohrávat	k5eAaImAgMnP
Portugalci	Portugalec	k1gMnPc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
dokázali	dokázat	k5eAaPmAgMnP
však	však	k9
snížit	snížit	k5eAaPmF
po	po	k7c6
gólu	gól	k1gInSc6
kapitána	kapitán	k1gMnSc2
Nuno	Nuno	k1gMnSc1
Gomese	Gomese	k1gFnPc4
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
předcházela	předcházet	k5eAaImAgFnS
Ronaldova	Ronaldův	k2eAgFnSc1d1
střela	střela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
prohře	prohra	k1gFnSc6
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
se	se	k3xPyFc4
ale	ale	k9
s	s	k7c7
turnajem	turnaj	k1gInSc7
rozloučili	rozloučit	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
240	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
2010	#num#	k4
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
představil	představit	k5eAaPmAgInS
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
pořádané	pořádaný	k2eAgNnSc1d1
Jihoafrickou	jihoafrický	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
Portugalsko	Portugalsko	k1gNnSc1
nalosováno	nalosovat	k5eAaImNgNnS,k5eAaBmNgNnS,k5eAaPmNgNnS
do	do	k7c2
„	„	k?
<g/>
skupiny	skupina	k1gFnSc2
smrti	smrt	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svěřenci	svěřenec	k1gMnPc1
trenéra	trenér	k1gMnSc2
Carlose	Carlosa	k1gFnSc3
Queiroze	Queiroz	k1gInSc5
uhráli	uhrát	k5eAaPmAgMnP
v	v	k7c6
prvním	první	k4xOgInSc6
utkání	utkání	k1gNnSc6
bod	bod	k1gInSc1
po	po	k7c6
remíze	remíza	k1gFnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
s	s	k7c7
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mužem	muž	k1gMnSc7
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
241	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgNnSc6
utkání	utkání	k1gNnSc6
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
7	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
proti	proti	k7c3
Severní	severní	k2eAgFnSc3d1
Koreji	Korea	k1gFnSc3
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
asistoval	asistovat	k5eAaImAgMnS
gólu	gól	k1gInSc2
Tiaga	Tiag	k1gMnSc2
Mendese	Mendese	k1gFnSc2
na	na	k7c4
4	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
sám	sám	k3xTgInSc1
zvyšoval	zvyšovat	k5eAaImAgInS
na	na	k7c4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
242	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Brazílii	Brazílie	k1gFnSc3
ve	v	k7c6
třetím	třetí	k4xOgNnSc6
utkání	utkání	k1gNnSc6
skupiny	skupina	k1gFnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
po	po	k7c6
remíze	remíza	k1gFnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
postoupilo	postoupit	k5eAaPmAgNnS
do	do	k7c2
osmifinále	osmifinále	k1gNnSc2
se	s	k7c7
Španělskem	Španělsko	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
243	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Španělsku	Španělsko	k1gNnSc3
se	se	k3xPyFc4
však	však	k9
Ronaldo	Ronaldo	k1gNnSc1
ani	ani	k8xC
nikdo	nikdo	k3yNnSc1
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
spoluhráčů	spoluhráč	k1gMnPc2
neprosadil	prosadit	k5eNaPmAgMnS
a	a	k8xC
Portugalsko	Portugalsko	k1gNnSc4
tak	tak	k9
turnaj	turnaj	k1gInSc4
po	po	k7c6
porážce	porážka	k1gFnSc6
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
s	s	k7c7
budoucím	budoucí	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
opustilo	opustit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
244	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
EURO	euro	k1gNnSc1
2012	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
EURO	euro	k1gNnSc1
2012	#num#	k4
v	v	k7c6
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Německu	Německo	k1gNnSc3
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
turnaji	turnaj	k1gInSc6
Euro	euro	k1gNnSc1
2012	#num#	k4
pořádané	pořádaný	k2eAgFnSc6d1
Polskem	Polsko	k1gNnSc7
a	a	k8xC
Ukrajinou	Ukrajina	k1gFnSc7
nalosováno	nalosován	k2eAgNnSc1d1
do	do	k7c2
tzv.	tzv.	kA
skupiny	skupina	k1gFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
vyšel	vyjít	k5eAaPmAgInS
v	v	k7c6
úvodních	úvodní	k2eAgInPc6d1
dvou	dva	k4xCgInPc6
duelech	duel	k1gInPc6
střelecky	střelecky	k6eAd1
naprázdno	naprázdno	k6eAd1
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
tým	tým	k1gInSc1
nejprve	nejprve	k6eAd1
prohrál	prohrát	k5eAaPmAgInS
s	s	k7c7
Německem	Německo	k1gNnSc7
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
posléze	posléze	k6eAd1
vyhrál	vyhrát	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
nad	nad	k7c7
Dánskem	Dánsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalý	bývalý	k2eAgMnSc1d1
anglický	anglický	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
Chris	Chris	k1gFnPc2
Waddle	Waddle	k1gMnSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
pro	pro	k7c4
BBC	BBC	kA
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Ronaldo	Ronaldo	k1gNnSc1
prožívá	prožívat	k5eAaImIp3nS
nepovedený	povedený	k2eNgInSc4d1
turnaj	turnaj	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
245	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
utkání	utkání	k1gNnSc6
proti	proti	k7c3
Nizozemsku	Nizozemsko	k1gNnSc3
ale	ale	k8xC
stvrdil	stvrdit	k5eAaPmAgMnS
postup	postup	k1gInSc4
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
dvěma	dva	k4xCgFnPc7
góly	gól	k1gInPc4
a	a	k8xC
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
246	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
Česku	Česko	k1gNnSc3
ve	v	k7c6
vyřazovacích	vyřazovací	k2eAgInPc6d1
bojích	boj	k1gInPc6
nejprve	nejprve	k6eAd1
trefil	trefit	k5eAaPmAgMnS
tyč	tyč	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
závěru	závěr	k1gInSc6
základní	základní	k2eAgFnSc2d1
hrací	hrací	k2eAgFnSc2d1
doby	doba	k1gFnSc2
pak	pak	k6eAd1
vstřelil	vstřelit	k5eAaPmAgMnS
jediný	jediný	k2eAgInSc4d1
gól	gól	k1gInSc4
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
247	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Semifinálový	semifinálový	k2eAgInSc1d1
střet	střet	k1gInSc1
se	s	k7c7
Španělskem	Španělsko	k1gNnSc7
skončil	skončit	k5eAaPmAgInS
nerozhodně	rozhodně	k6eNd1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
a	a	k8xC
nastal	nastat	k5eAaPmAgInS
penaltový	penaltový	k2eAgInSc4d1
rozstřel	rozstřel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
kopat	kopat	k5eAaImF
až	až	k9
pátou	pátý	k4xOgFnSc4
penaltu	penalta	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
toho	ten	k3xDgNnSc2
rozhodnutí	rozhodnutí	k1gNnSc2
jej	on	k3xPp3gMnSc4
o	o	k7c6
kopání	kopání	k1gNnSc6
možné	možný	k2eAgFnSc2d1
rozhodující	rozhodující	k2eAgFnSc2d1
penalty	penalta	k1gFnSc2
připravilo	připravit	k5eAaPmAgNnS
<g/>
,	,	kIx,
když	když	k8xS
jeho	jeho	k3xOp3gMnPc1
spoluhráči	spoluhráč	k1gMnPc1
dvě	dva	k4xCgFnPc1
penalty	penalta	k1gFnPc1
nedaly	dát	k5eNaPmAgFnP
a	a	k8xC
Španělé	Španěl	k1gMnPc1
postoupili	postoupit	k5eAaPmAgMnP
po	po	k7c6
výsledku	výsledek	k1gInSc6
4	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
Ronaldova	Ronaldův	k2eAgFnSc1d1
penalta	penalta	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
řadu	řada	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
248	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Se	s	k7c7
třemi	tři	k4xCgFnPc7
vstřelenými	vstřelený	k2eAgFnPc7d1
brankami	branka	k1gFnPc7
byl	být	k5eAaImAgMnS
v	v	k7c6
šestici	šestice	k1gFnSc6
nejlepších	dobrý	k2eAgMnPc2d3
střelců	střelec	k1gMnPc2
EURA	euro	k1gNnSc2
2012	#num#	k4
(	(	kIx(
<g/>
mimo	mimo	k7c4
něj	on	k3xPp3gNnSc4
ještě	ještě	k6eAd1
Mario	Mario	k1gMnSc1
Gómez	Gómez	k1gMnSc1
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Balotelli	Balotelle	k1gFnSc4
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
Mandžukić	Mandžukić	k1gMnSc1
<g/>
,	,	kIx,
Alan	Alan	k1gMnSc1
Dzagojev	Dzagojev	k1gMnSc1
<g/>
,	,	kIx,
Fernando	Fernanda	k1gFnSc5
Torres	Torresa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
2014	#num#	k4
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc4
zahájil	zahájit	k5eAaPmAgInS
turnaj	turnaj	k1gInSc1
úvodním	úvodní	k2eAgNnSc7d1
utkáním	utkání	k1gNnSc7
proti	proti	k7c3
Německu	Německo	k1gNnSc3
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
však	však	k9
Portugalci	Portugalec	k1gMnPc1
prohráli	prohrát	k5eAaPmAgMnP
0	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
byl	být	k5eAaImAgInS
zastíněn	zastíněn	k2eAgInSc1d1
soupeřovou	soupeřův	k2eAgFnSc7d1
ofenzivní	ofenzivní	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
a	a	k8xC
autorem	autor	k1gMnSc7
hattricku	hattrick	k1gInSc2
Thomasem	Thomas	k1gMnSc7
Müllerem	Müller	k1gMnSc7
rovněž	rovněž	k9
v	v	k7c6
dalších	další	k2eAgFnPc6d1
statistikách	statistika	k1gFnPc6
a	a	k8xC
nebezpečím	bezpečit	k5eNaImIp1nS
pro	pro	k7c4
soupeře	soupeř	k1gMnSc4
byl	být	k5eAaImAgMnS
zejména	zejména	k9
na	na	k7c6
začátku	začátek	k1gInSc6
a	a	k8xC
na	na	k7c6
konci	konec	k1gInSc6
utkání	utkání	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jej	on	k3xPp3gMnSc4
vychytal	vychytat	k5eAaPmAgMnS
brankář	brankář	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Neuer	Neuer	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
249	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
250	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Proti	proti	k7c3
USA	USA	kA
se	se	k3xPyFc4
Portugalci	Portugalec	k1gMnPc1
přiblížili	přiblížit	k5eAaPmAgMnP
vyřazení	vyřazení	k1gNnSc4
<g/>
,	,	kIx,
tým	tým	k1gInSc4
trenéra	trenér	k1gMnSc2
Paula	Paul	k1gMnSc2
Benta	Bent	k1gMnSc2
ale	ale	k8xC
zachránil	zachránit	k5eAaPmAgMnS
remízu	remíza	k1gFnSc4
2	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
v	v	k7c6
nastaveném	nastavený	k2eAgInSc6d1
čase	čas	k1gInSc6
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
po	po	k7c6
Ronaldově	Ronaldův	k2eAgNnSc6d1
centru	centrum	k1gNnSc6
prosadil	prosadit	k5eAaPmAgMnS
Silvestre	Silvestr	k1gInSc5
Varela	Varel	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
251	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
třetím	třetí	k4xOgNnSc6
utkání	utkání	k1gNnSc6
potřebovalo	potřebovat	k5eAaImAgNnS
mužstvo	mužstvo	k1gNnSc1
vyhrát	vyhrát	k5eAaPmF
proti	proti	k7c3
Ghaně	Ghana	k1gFnSc3
a	a	k8xC
věřit	věřit	k5eAaImF
ve	v	k7c4
výhru	výhra	k1gFnSc4
Německa	Německo	k1gNnSc2
nad	nad	k7c7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
vstřelil	vstřelit	k5eAaPmAgInS
vítězný	vítězný	k2eAgInSc1d1
gól	gól	k1gInSc1
na	na	k7c6
konečných	konečný	k2eAgInPc6d1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Portugalsko	Portugalsko	k1gNnSc1
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
vyřazeno	vyřazen	k2eAgNnSc1d1
a	a	k8xC
sám	sám	k3xTgInSc1
Ronaldo	Ronaldo	k1gNnSc4
tedy	tedy	k8xC
skončil	skončit	k5eAaPmAgInS
svoji	svůj	k3xOyFgFnSc4
úspěšnou	úspěšný	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
už	už	k6eAd1
ve	v	k7c6
skupině	skupina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
252	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
EURO	euro	k1gNnSc1
2016	#num#	k4
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
evropského	evropský	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
EURO	euro	k1gNnSc4
2016	#num#	k4
proti	proti	k7c3
Polsku	Polsko	k1gNnSc3
</s>
<s>
Trenér	trenér	k1gMnSc1
portugalského	portugalský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
týmu	tým	k1gInSc2
Fernando	Fernanda	k1gFnSc5
Santos	Santos	k1gMnSc1
jej	on	k3xPp3gMnSc4
zařadil	zařadit	k5eAaPmAgMnS
do	do	k7c2
závěrečné	závěrečný	k2eAgFnSc2d1
23	#num#	k4
<g/>
členné	členný	k2eAgFnSc2d1
nominace	nominace	k1gFnSc2
na	na	k7c4
EURO	euro	k1gNnSc4
2016	#num#	k4
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
253	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
finále	finále	k1gNnSc6
šampionátu	šampionát	k1gInSc2
proti	proti	k7c3
domácí	domácí	k2eAgFnSc3d1
Francii	Francie	k1gFnSc3
utrpěl	utrpět	k5eAaPmAgMnS
v	v	k7c6
prvním	první	k4xOgInSc6
poločase	poločas	k1gInSc6
zranění	zranění	k1gNnSc2
nohy	noha	k1gFnSc2
po	po	k7c6
střetu	střet	k1gInSc6
s	s	k7c7
Payetem	Payet	k1gMnSc7
a	a	k8xC
Portugalci	Portugalec	k1gMnPc1
se	se	k3xPyFc4
museli	muset	k5eAaImAgMnP
po	po	k7c4
zbytek	zbytek	k1gInSc4
zápasu	zápas	k1gInSc2
obejít	obejít	k5eAaPmF
bez	bez	k7c2
své	svůj	k3xOyFgFnSc2
největší	veliký	k2eAgFnSc2d3
hvězdy	hvězda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
dokázali	dokázat	k5eAaPmAgMnP
po	po	k7c6
vítězném	vítězný	k2eAgInSc6d1
gólu	gól	k1gInSc6
Édera	Éder	k1gInSc2
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
získat	získat	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
premiérový	premiérový	k2eAgInSc4d1
titul	titul	k1gInSc4
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
254	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
EURU	euro	k1gNnSc6
2016	#num#	k4
vstřelil	vstřelit	k5eAaPmAgMnS
3	#num#	k4
góly	gól	k1gInPc4
<g/>
,	,	kIx,
celkem	celkem	k6eAd1
jich	on	k3xPp3gFnPc2
má	mít	k5eAaImIp3nS
z	z	k7c2
evropských	evropský	k2eAgInPc2d1
šampionátu	šampionát	k1gInSc2
na	na	k7c6
kontě	konto	k1gNnSc6
9	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vyrovnal	vyrovnat	k5eAaBmAgMnS,k5eAaPmAgMnS
rekord	rekord	k1gInSc4
Francouze	Francouz	k1gMnSc2
Michela	Michel	k1gMnSc2
Platiniho	Platini	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
2018	#num#	k4
</s>
<s>
Trenér	trenér	k1gMnSc1
portugalského	portugalský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
týmu	tým	k1gInSc2
Fernando	Fernanda	k1gFnSc5
Santos	Santos	k1gMnSc1
jej	on	k3xPp3gMnSc4
zařadil	zařadit	k5eAaPmAgMnS
do	do	k7c2
závěrečné	závěrečný	k2eAgFnSc2d1
23	#num#	k4
<g/>
členné	členný	k2eAgFnSc2d1
nominace	nominace	k1gFnSc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2018	#num#	k4
v	v	k7c6
Rusku	Rusko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
zápase	zápas	k1gInSc6
se	s	k7c7
Španělskem	Španělsko	k1gNnSc7
výrazně	výrazně	k6eAd1
pomohl	pomoct	k5eAaPmAgInS
k	k	k7c3
remíze	remíza	k1gFnSc3
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
hattrickem	hattrick	k1gInSc7
a	a	k8xC
konečně	konečně	k6eAd1
dal	dát	k5eAaPmAgMnS
více	hodně	k6eAd2
než	než	k8xS
gól	gól	k1gInSc4
na	na	k7c6
jednom	jeden	k4xCgNnSc6
Mistrovství	mistrovství	k1gNnSc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
podstatě	podstata	k1gFnSc6
dal	dát	k5eAaPmAgMnS
tolik	tolik	k4xDc4,k4yIc4
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
za	za	k7c4
minulé	minulý	k2eAgInPc4d1
tři	tři	k4xCgInPc4
mistrovství	mistrovství	k1gNnSc6
dohromady	dohromady	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
zápase	zápas	k1gInSc6
s	s	k7c7
Marokem	Maroko	k1gNnSc7
dal	dát	k5eAaPmAgMnS
hlavou	hlava	k1gFnSc7
jediný	jediný	k2eAgInSc4d1
gól	gól	k1gInSc4
tohoto	tento	k3xDgInSc2
střetu	střet	k1gInSc2
a	a	k8xC
překonal	překonat	k5eAaPmAgMnS
Ference	Ferenc	k1gMnSc4
Puskáse	Puskás	k1gMnSc4
v	v	k7c6
roli	role	k1gFnSc6
nejlepšího	dobrý	k2eAgMnSc2d3
reprezentačního	reprezentační	k2eAgMnSc2d1
střelce	střelec	k1gMnSc2
Evropy	Evropa	k1gFnSc2
(	(	kIx(
<g/>
85	#num#	k4
gólů	gól	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
255	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
skupinovém	skupinový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Íránu	Írán	k1gInSc3
neproměnil	proměnit	k5eNaPmAgMnS
penaltu	penalta	k1gFnSc4
<g/>
,	,	kIx,
gól	gól	k1gInSc4
Quaresmy	Quaresma	k1gFnSc2
ale	ale	k8xC
zařídil	zařídit	k5eAaPmAgMnS
remízu	remíza	k1gFnSc4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
a	a	k8xC
postup	postup	k1gInSc1
z	z	k7c2
druhého	druhý	k4xOgNnSc2
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
256	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Konečnou	Konečná	k1gFnSc7
bylo	být	k5eAaImAgNnS
prohrané	prohraný	k2eAgNnSc1d1
osmifinálové	osmifinálový	k2eAgNnSc1d1
utkání	utkání	k1gNnSc1
proti	proti	k7c3
Uruguayi	Uruguay	k1gFnSc3
po	po	k7c6
výsledku	výsledek	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
257	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
července	červenec	k1gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
hlasování	hlasování	k1gNnSc6
fanoušků	fanoušek	k1gMnPc2
z	z	k7c2
webu	web	k1gInSc2
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
zařazen	zařadit	k5eAaPmNgInS
do	do	k7c2
turnajového	turnajový	k2eAgInSc2d1
„	„	k?
<g/>
Týmu	tým	k1gInSc2
snů	sen	k1gInPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
258	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Liga	liga	k1gFnSc1
národů	národ	k1gInPc2
2019	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
se	se	k3xPyFc4
Ronaldo	Ronalda	k1gFnSc5
zúčastnil	zúčastnit	k5eAaPmAgMnS
premiérového	premiérový	k2eAgInSc2d1
finálového	finálový	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
Ligy	liga	k1gFnSc2
národů	národ	k1gInPc2
UEFA	UEFA	kA
a	a	k8xC
třemi	tři	k4xCgInPc7
góly	gól	k1gInPc7
pomohl	pomoct	k5eAaPmAgMnS
překonat	překonat	k5eAaPmF
semifinálového	semifinálový	k2eAgMnSc4d1
soupeře	soupeř	k1gMnSc4
<g/>
,	,	kIx,
Švýcarsko	Švýcarsko	k1gNnSc4
při	při	k7c6
výhře	výhra	k1gFnSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gólově	gólově	k6eAd1
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgMnS
na	na	k7c6
svém	svůj	k3xOyFgInSc6
desátém	desátý	k4xOgInSc6
reprezentačním	reprezentační	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
vůbec	vůbec	k9
prvním	první	k4xOgMnSc6
bylo	být	k5eAaImAgNnS
Euro	euro	k1gNnSc1
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
259	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
finále	finále	k1gNnSc6
hraném	hraný	k2eAgNnSc6d1
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
na	na	k7c6
portugalském	portugalský	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
Estádio	Estádio	k6eAd1
do	do	k7c2
Dragã	Dragã	k6eAd1
s	s	k7c7
týmem	tým	k1gInSc7
uspěl	uspět	k5eAaPmAgInS
a	a	k8xC
po	po	k7c6
výhře	výhra	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
proti	proti	k7c3
Nizozemsku	Nizozemsko	k1gNnSc3
slavil	slavit	k5eAaImAgInS
triumf	triumf	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
260	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
EURO	euro	k1gNnSc1
2021	#num#	k4
</s>
<s>
Proti	proti	k7c3
Litvě	Litva	k1gFnSc3
10	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
se	se	k3xPyFc4
zaskvěl	zaskvět	k5eAaPmAgInS
čtyřmi	čtyři	k4xCgNnPc7
góly	gól	k1gInPc4
a	a	k8xC
pomohl	pomoct	k5eAaPmAgInS
vyhrát	vyhrát	k5eAaPmF
evropské	evropský	k2eAgNnSc4d1
kvalifikační	kvalifikační	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
5	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgInSc7
Ronaldo	Ronaldo	k1gNnSc4
zaznamenal	zaznamenat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
osmý	osmý	k4xOgInSc4
hattrick	hattrick	k1gInSc4
v	v	k7c6
reprezentaci	reprezentace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
261	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Profil	profil	k1gInSc1
fotbalisty	fotbalista	k1gMnSc2
</s>
<s>
Ve	v	k7c6
Sportingu	Sporting	k1gInSc6
a	a	k8xC
Manchesteru	Manchester	k1gInSc2
United	United	k1gMnSc1
byl	být	k5eAaImAgMnS
Ronaldo	Ronaldo	k1gNnSc4
hubeným	hubený	k2eAgNnSc7d1
šlachovitým	šlachovitý	k2eAgNnSc7d1
křídlem	křídlo	k1gNnSc7
schopným	schopný	k2eAgNnSc7d1
zahrát	zahrát	k5eAaPmF
nalevo	nalevo	k6eAd1
i	i	k9
napravo	napravo	k6eAd1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
v	v	k7c6
postupu	postup	k1gInSc6
kariéry	kariéra	k1gFnSc2
proměnil	proměnit	k5eAaPmAgMnS
ve	v	k7c4
všestranného	všestranný	k2eAgMnSc4d1
útočníka	útočník	k1gMnSc4
dobývající	dobývající	k2eAgNnSc1d1
gólové	gólový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
přibývajícím	přibývající	k2eAgInSc7d1
věkem	věk	k1gInSc7
se	se	k3xPyFc4
posléze	posléze	k6eAd1
zaměřil	zaměřit	k5eAaPmAgMnS
na	na	k7c4
roli	role	k1gFnSc4
více	hodně	k6eAd2
vpředu	vpředu	k6eAd1
<g/>
,	,	kIx,
na	na	k7c6
hrotu	hrot	k1gInSc6
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
262	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
jej	on	k3xPp3gMnSc4
německý	německý	k2eAgInSc1d1
týdeník	týdeník	k1gInSc1
Der	drát	k5eAaImRp2nS
Spiegel	Spiegel	k1gInSc4
po	po	k7c6
své	svůj	k3xOyFgFnSc6
studii	studie	k1gFnSc6
označil	označit	k5eAaPmAgMnS
za	za	k7c4
nejrychlejšího	rychlý	k2eAgMnSc4d3
fotbalistu	fotbalista	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dokáže	dokázat	k5eAaPmIp3nS
dosáhnout	dosáhnout	k5eAaPmF
rychlosti	rychlost	k1gFnSc3
v	v	k7c6
průměru	průměr	k1gInSc6
33,6	33,6	k4
kilometru	kilometr	k1gInSc2
v	v	k7c6
hodině	hodina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
byl	být	k5eAaImAgInS
týdeníkem	týdeník	k1gInSc7
označen	označit	k5eAaPmNgInS
prvním	první	k4xOgMnSc6
na	na	k7c4
úkor	úkor	k1gInSc4
fotbalistů	fotbalista	k1gMnPc2
jako	jako	k8xC,k8xS
Arjen	Arjna	k1gFnPc2
Robben	Robbna	k1gFnPc2
<g/>
,	,	kIx,
Theo	Thea	k1gFnSc5
Walcott	Walcott	k1gInSc1
<g/>
,	,	kIx,
Wayne	Wayn	k1gInSc5
Rooney	Roonea	k1gFnPc1
nebo	nebo	k8xC
Robin	robin	k2eAgInSc1d1
van	van	k1gInSc1
Persie	Persie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
263	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gInPc4
fotbalové	fotbalový	k2eAgInPc4d1
atributy	atribut	k1gInPc4
patří	patřit	k5eAaImIp3nS
atletičnost	atletičnost	k1gFnSc1
<g/>
,	,	kIx,
rychlost	rychlost	k1gFnSc1
<g/>
,	,	kIx,
driblink	driblink	k1gInSc1
a	a	k8xC
zakončování	zakončování	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ronaldo	Ronaldo	k1gNnSc1
je	být	k5eAaImIp3nS
častým	častý	k2eAgMnSc7d1
exekutorem	exekutor	k1gMnSc7
přímých	přímý	k2eAgInPc2d1
kopů	kop	k1gInPc2
<g/>
,	,	kIx,
při	při	k7c6
zahrávání	zahrávání	k1gNnSc6
proslul	proslout	k5eAaPmAgMnS
typickou	typický	k2eAgFnSc7d1
počáteční	počáteční	k2eAgFnSc7d1
pózou	póza	k1gFnSc7
s	s	k7c7
rozkročenými	rozkročený	k2eAgFnPc7d1
nohami	noha	k1gFnPc7
a	a	k8xC
rucema	rucema	k1gFnSc1
v	v	k7c4
bok	bok	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mimo	mimo	k7c4
hřiště	hřiště	k1gNnSc4
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojována	ozdrojován	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
žije	žít	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
v	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Španělska	Španělsko	k1gNnSc2
v	v	k7c6
Madridu	Madrid	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Madridu	Madrid	k1gInSc6
si	se	k3xPyFc3
pronajímá	pronajímat	k5eAaImIp3nS
vily	vila	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
žije	žít	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
otcem	otec	k1gMnSc7
a	a	k8xC
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
Twitter	Twitter	k1gInSc4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
toto	tento	k3xDgNnSc4
<g/>
:	:	kIx,
„	„	k?
<g/>
S	s	k7c7
velkou	velký	k2eAgFnSc7d1
radostí	radost	k1gFnSc7
oznamuji	oznamovat	k5eAaImIp1nS
<g/>
,	,	kIx,
že	že	k8xS
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nedávno	nedávno	k6eAd1
otcem	otec	k1gMnSc7
syna	syn	k1gMnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Podle	podle	k7c2
portugalského	portugalský	k2eAgInSc2d1
listu	list	k1gInSc2
Correiro	Correiro	k1gNnSc4
da	da	k?
manha	manha	k1gFnSc1
je	být	k5eAaImIp3nS
matkou	matka	k1gFnSc7
dítěte	dítě	k1gNnSc2
žena	žena	k1gFnSc1
ze	z	k7c2
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgMnPc4
chlapce	chlapec	k1gMnPc4
porodila	porodit	k5eAaPmAgFnS
začátkem	začátkem	k7c2
června	červen	k1gInSc2
<g/>
.	.	kIx.
„	„	k?
<g/>
Dohodl	dohodnout	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
se	se	k3xPyFc4
s	s	k7c7
matkou	matka	k1gFnSc7
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
chce	chtít	k5eAaImIp3nS
zůstat	zůstat	k5eAaPmF
v	v	k7c6
anonymitě	anonymita	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
syn	syn	k1gMnSc1
bude	být	k5eAaImBp3nS
svěřen	svěřen	k2eAgMnSc1d1
pouze	pouze	k6eAd1
do	do	k7c2
mé	můj	k3xOp1gFnSc2
péče	péče	k1gFnSc2
<g/>
,	,	kIx,
<g/>
“	“	k?
dodala	dodat	k5eAaPmAgFnS
hvězda	hvězda	k1gFnSc1
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
tedy	tedy	k8xC
doma	doma	k6eAd1
vychovává	vychovávat	k5eAaImIp3nS
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
pojmenoval	pojmenovat	k5eAaPmAgMnS
po	po	k7c6
sobě	se	k3xPyFc3
–	–	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
junior	junior	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ronaldova	Ronaldův	k2eAgFnSc1d1
bývalá	bývalý	k2eAgFnSc1d1
přítelkyně	přítelkyně	k1gFnSc1
byla	být	k5eAaImAgFnS
ruská	ruský	k2eAgFnSc1d1
top	topit	k5eAaImRp2nS
modelka	modelka	k1gFnSc1
Irina	Irino	k1gNnSc2
Shayk	Shayka	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yQgFnSc7,k3yRgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
prostřednictvím	prostřednictvím	k7c2
kampaně	kampaň	k1gFnSc2
Armani	Arman	k1gMnPc1
Exchange	Exchange	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
s	s	k7c7
ní	on	k3xPp3gFnSc7
tvořil	tvořit	k5eAaImAgInS
pár	pár	k1gInSc1
od	od	k7c2
května	květen	k1gInSc2
roku	rok	k1gInSc2
2010	#num#	k4
až	až	k9
do	do	k7c2
ledna	leden	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
264	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
a	a	k8xC
ocenění	ocenění	k1gNnSc1
</s>
<s>
Individuální	individuální	k2eAgFnSc1d1
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
Premier	Premier	k1gMnSc1
League	League	k1gFnSc1
podle	podle	k7c2
PFA	PFA	kA
–	–	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
[	[	kIx(
<g/>
265	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
Premier	Premier	k1gMnSc1
League	League	k1gFnSc1
podle	podle	k7c2
PFA	PFA	kA
–	–	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
265	#num#	k4
<g/>
]	]	kIx)
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
266	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hráč	hráč	k1gMnSc1
roku	rok	k1gInSc2
podle	podle	k7c2
FWA	FWA	kA
–	–	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
267	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cena	cena	k1gFnSc1
Ference	Ferenc	k1gMnSc2
Puskáse	Puskáse	k1gFnSc2
–	–	k?
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
FIFA	FIFA	kA
–	–	k?
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
268	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
/	/	kIx~
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
FIFA	FIFA	kA
–	–	k?
2008	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
269	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
270	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
271	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
kopačka	kopačka	k1gFnSc1
–	–	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
[	[	kIx(
<g/>
272	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trofeo	Trofeo	k1gMnSc1
Pichichi	Pichich	k1gFnSc2
–	–	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
[	[	kIx(
<g/>
273	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
sezóny	sezóna	k1gFnSc2
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
–	–	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
[	[	kIx(
<g/>
274	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
útočník	útočník	k1gMnSc1
sezóny	sezóna	k1gFnSc2
La	la	k1gNnSc2
Ligy	liga	k1gFnSc2
–	–	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
[	[	kIx(
<g/>
274	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejužitečnější	užitečný	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
Serie	serie	k1gFnSc2
A	A	kA
–	–	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
[	[	kIx(
<g/>
207	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tým	tým	k1gInSc1
snů	sen	k1gInPc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
258	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyznamenání	vyznamenání	k1gNnSc1
</s>
<s>
Stát	stát	k1gInSc1
<g/>
/	/	kIx~
<g/>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Stuha	stuha	k1gFnSc1
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Datum	datum	k1gNnSc1
udělení	udělení	k1gNnSc2
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
Portugalsko	Portugalsko	k1gNnSc1
<g/>
[	[	kIx(
<g/>
275	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velkodůstojník	Velkodůstojník	k1gInSc1
Řádu	řád	k1gInSc2
prince	princ	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
</s>
<s>
Důstojník	důstojník	k1gMnSc1
Řádu	řád	k1gInSc2
prince	princ	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2004	#num#	k4
</s>
<s>
Komandér	komandér	k1gMnSc1
Řádu	řád	k1gInSc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Braganzů	Braganz	k1gMnPc2
</s>
<s>
Řád	řád	k1gInSc1
Naší	náš	k3xOp1gFnSc2
milé	milý	k2eAgFnSc2d1
Paní	paní	k1gFnSc2
z	z	k7c2
Villa	Villo	k1gNnSc2
Vicosy	Vicosa	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
–	–	k?
Ve	v	k7c6
sportu	sport	k1gInSc6
byl	být	k5eAaImAgMnS
mým	můj	k3xOp1gMnSc7
otcem	otec	k1gMnSc7
<g/>
,	,	kIx,
jedním	jeden	k4xCgMnSc7
z	z	k7c2
nejdůležitějších	důležitý	k2eAgInPc2d3
a	a	k8xC
nejvlinějších	vliný	k2eAgInPc2d3
faktorů	faktor	k1gInPc2
v	v	k7c6
mé	můj	k3xOp1gFnSc6
kariéře	kariéra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
lednu	leden	k1gInSc6
roku	rok	k1gInSc2
2021	#num#	k4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
přestup	přestup	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
překonán	překonat	k5eAaPmNgInS
v	v	k7c6
deseti	deset	k4xCc6
případech	případ	k1gInPc6
devíti	devět	k4xCc3
fotbalisty	fotbalista	k1gMnPc4
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc4
samotný	samotný	k2eAgMnSc1d1
překonal	překonat	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
přestup	přestup	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
přestupem	přestup	k1gInSc7
do	do	k7c2
Juventusu	Juventus	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
fotbale	fotbal	k1gInSc6
děleným	dělený	k2eAgMnSc7d1
9	#num#	k4
<g/>
.	.	kIx.
nejdražším	drahý	k2eAgInSc7d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestup	přestup	k1gInSc1
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
je	být	k5eAaImIp3nS
11	#num#	k4
<g/>
.	.	kIx.
nejdražším	drahý	k2eAgMnPc3d3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
FIFA	FIFA	kA
Club	club	k1gInSc1
World	World	k1gInSc1
Cup	cup	k1gInSc4
UAE	UAE	kA
2017	#num#	k4
<g/>
:	:	kIx,
List	list	k1gInSc1
of	of	k?
players	players	k1gInSc1
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
CF	CF	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
,	,	kIx,
2017-12-16	2017-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Soccerway	Soccerwaa	k1gFnSc2
-	-	kIx~
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Ronaldo	Ronaldo	k1gNnSc1
completes	completesa	k1gFnPc2
£	£	k?
<g/>
80	#num#	k4
<g/>
m	m	kA
Real	Real	k1gInSc4
move	mov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-07-01	2009-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
star	star	k1gFnSc2
is	is	k?
best	best	k2eAgInSc4d1
player	player	k1gInSc4
in	in	k?
history	histor	k1gInPc4
-	-	kIx~
Zinedine	Zinedin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-08	2017-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MILLAR	MILLAR	kA
<g/>
,	,	kIx,
Colin	Colin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CRISTIANO	CRISTIANO	kA
RONALDO	RONALDO	kA
<g/>
:	:	kIx,
THE	THE	kA
SINGULAR	SINGULAR	kA
GENIUS	genius	k1gMnSc1
OF	OF	kA
A	a	k8xC
FOOTBALLER	FOOTBALLER	kA
LIKE	LIKE	kA
NO	no	k9
OTHER	OTHER	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
These	these	k1gFnSc1
Football	Footballa	k1gFnPc2
Times	Timesa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-21	2017-08-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
WOO	WOO	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gMnSc1
Football	Football	k1gMnSc1
<g/>
:	:	kIx,
Ranking	Ranking	k1gInSc1
the	the	k?
Top	topit	k5eAaImRp2nS
10	#num#	k4
Long	Long	k1gMnSc1
Free	Fre	k1gInSc2
Kick	Kick	k1gMnSc1
Specialists	Specialistsa	k1gFnPc2
of	of	k?
All-Time	All-Tim	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-05-03	2011-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SWAN	SWAN	kA
<g/>
,	,	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
Messi	Messe	k1gFnSc3
<g/>
,	,	kIx,
Dani	daň	k1gFnSc3
Alves	Alvesa	k1gFnPc2
<g/>
:	:	kIx,
Who	Who	k1gFnSc1
has	hasit	k5eAaImRp2nS
won	won	k1gInSc4
most	most	k1gInSc4
trophies	trophiesa	k1gFnPc2
in	in	k?
football	footbalnout	k5eAaPmAgMnS
history	histor	k1gInPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
givemesport	givemesport	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-01-21	2021-01-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
.	.	kIx.
puskas	puskas	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FIFA	FIFA	kA
Puskas	Puskas	k1gMnSc1
Award	Award	k1gMnSc1
<g/>
,	,	kIx,
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
CHURCH	CHURCH	kA
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
nets	nets	k1gInSc1
750	#num#	k4
<g/>
th	th	k?
career	career	k1gMnSc1
goal	goal	k1gMnSc1
and	and	k?
edges	edges	k1gMnSc1
closer	closer	k1gMnSc1
to	ten	k3xDgNnSc4
all-time	all-timat	k5eAaPmIp3nS
record	record	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-03	2020-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
SPAL	spát	k5eAaImAgMnS
1-2	1-2	k4
Juventus	Juventus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-22	2020-02-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
first	first	k1gMnSc1
to	ten	k3xDgNnSc1
win	win	k?
five	fivat	k5eAaPmIp3nS
Champions	Champions	k1gInSc1
League	Leagu	k1gFnSc2
titles	titlesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-26	2018-05-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Champions	Champions	k1gInSc1
League	Leagu	k1gFnPc4
all-time	all-timat	k5eAaPmIp3nS
top	topit	k5eAaImRp2nS
scorers	scorers	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-09	2020-12-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
je	být	k5eAaImIp3nS
najlepším	najlepší	k2eAgInSc7d1
strelcom	strelcom	k1gInSc1
Portugalska	Portugalsko	k1gNnSc2
<g/>
,	,	kIx,
prekonal	prekonat	k5eAaPmAgMnS,k5eAaImAgMnS
Pauletu	Pauleta	k1gFnSc4
<g/>
,	,	kIx,
SME	SME	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
5	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
zabral	zabrat	k5eAaPmAgMnS
a	a	k8xC
vyrovnal	vyrovnat	k5eAaBmAgMnS,k5eAaPmAgMnS
Platiniho	Platini	k1gMnSc2
<g/>
:	:	kIx,
K	k	k7c3
naplneniu	naplnenium	k1gNnSc3
sna	sen	k1gInSc2
chýba	chýba	k1gMnSc1
iba	iba	k6eAd1
krok	krok	k1gInSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Profutbal	Profutbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
102	#num#	k4
Portugal	portugal	k1gInSc1
goals	goals	k1gInSc4
<g/>
:	:	kIx,
Europe	Europ	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
top	topit	k5eAaImRp2nS
international	internationat	k5eAaImAgMnS,k5eAaPmAgMnS
scorer	scorer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-12	2020-11-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pelé	Pelé	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Time	Time	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-04-23	2014-04-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SETTIMI	SETTIMI	kA
<g/>
,	,	kIx,
Christina	Christina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
$	$	kIx~
<g/>
105	#num#	k4
Million	Million	k1gInSc1
Year	Yeara	k1gFnPc2
Tops	Tops	k1gInSc4
Messi	Messe	k1gFnSc4
And	Anda	k1gFnPc2
Crowns	Crownsa	k1gFnPc2
Him	Him	k1gFnSc2
Soccer	Soccer	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
First	First	k1gMnSc1
Billion-Dollar	Billion-Dollar	k1gMnSc1
Man	Man	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes	forbes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-04	2020-06-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Boloni	Boloňa	k1gFnSc6
îș	îș	k6eAd1
aminteș	aminteș	k5eAaPmIp2nP
de	de	k?
debutul	debutul	k1gInSc1
lui	lui	k?
Ronaldo	Ronaldo	k1gNnSc1
la	la	k1gNnSc2
Sporting	Sporting	k1gInSc4
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Nu	nu	k9
reuș	reuș	k1gInSc1
să	să	k?
îl	îl	k?
opresc	opresc	k1gInSc1
să	să	k?
facă	facă	k?
driblinguri	driblingur	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
DigiSport	DigiSport	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.manchestereveningnews.co.uk	www.manchestereveningnews.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.uefau17.com	www.uefau17.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
uefau	uefau	k6eAd1
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
uk	uk	k?
<g/>
.	.	kIx.
<g/>
eurosport	eurosport	k1gInSc1
<g/>
.	.	kIx.
<g/>
yahoo	yahoo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurosport	eurosport	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
bleacherreport	bleacherreport	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sporting	Sporting	k1gInSc1
Clube	club	k1gInSc5
de	de	k?
Portugal	portugal	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PORTUGUESE	PORTUGUESE	kA
MANCHESTER	Manchester	k1gInSc4
UNITED	UNITED	kA
PLAYERS	PLAYERS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
United	United	k1gMnSc1
fork	fork	k1gMnSc1
out	out	k?
record	record	k1gInSc1
£	£	k?
<g/>
12	#num#	k4
<g/>
m	m	kA
fee	fee	k?
to	ten	k3xDgNnSc4
land	land	k6eAd1
teenager	teenager	k1gMnSc1
<g/>
.	.	kIx.
www.telegraph.co.uk	www.telegraph.co.uk	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
307	#num#	k4
<g/>
-	-	kIx~
<g/>
1235	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2003-08-13	2003-08-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.mirror.co.uk	www.mirror.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
sabotagetimes	sabotagetimes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RIDLEY	RIDLEY	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
good	good	k1gInSc1
and	and	k?
bad	bad	k?
day	day	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Observer	Observer	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2004	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
261	#num#	k4
<g/>
-	-	kIx~
<g/>
3077	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.manchestereveningnews.co.uk	www.manchestereveningnews.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.telegraph.co.uk	www.telegraph.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.telegraph.co.uk	www.telegraph.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.inthenews.co.uk	www.inthenews.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
inthenews	inthenews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
<g/>
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
<g/>
ledna	leden	k1gInSc2
2010	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
<g/>
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
<g/>
srpna	srpen	k1gInSc2
2014	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2014	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
<g/>
května	květen	k1gInSc2
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.worldsoccer.com	www.worldsoccer.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.timesonline.co.uk	www.timesonline.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
www.uefa.com	www.uefa.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.fifa.com	www.fifa.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Man	Man	k1gMnSc1
Utd	Utd	k1gMnSc1
6	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
Newcastle	Newcastle	k1gFnSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Sport	sport	k1gInSc1
<g/>
,	,	kIx,
2008-01-12	2008-01-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.timesonline.co.uk	www.timesonline.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.uefa.com	www.uefa.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://web.archive.org/web/20140903180058/http://www.telegraph.co.uk/sport/football/2293527/Cristiano-Ronaldo-puts-Man-Utd-into-quarters.html	https://web.archive.org/web/20140903180058/http://www.telegraph.co.uk/sport/football/2293527/Cristiano-Ronaldo-puts-Man-Utd-into-quarters.html	k1gInSc1
<g/>
↑	↑	k?
www.independent.co.uk	www.independent.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.telegraph.co.uk	www.telegraph.co.uk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
UEFA	UEFA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.espnfc.com	www.espnfc.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.uefa.com	www.uefa.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Stat	Stat	k1gMnSc1
Attack	Attack	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
United	United	k1gMnSc1
Review	Review	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manchester	Manchester	k1gInSc1
United	United	k1gInSc4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
65	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.fifa.com	www.fifa.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www.theguardian.com	www.theguardian.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Premier	Premier	k1gInSc1
League	League	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.transfermarkt.com/cristiano-ronaldo/leistungsdaten/spieler/8198/saison/2008/plus/1	https://www.transfermarkt.com/cristiano-ronaldo/leistungsdaten/spieler/8198/saison/2008/plus/1	k4
<g/>
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
www	www	k?
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
skysports	skysportsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://www.transfermarkt.com/cristiano-ronaldo/leistungsdatenverein/spieler/8198	https://www.transfermarkt.com/cristiano-ronaldo/leistungsdatenverein/spieler/8198	k4
<g/>
↑	↑	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
<g/>
:	:	kIx,
The	The	k1gFnSc2
history	histor	k1gInPc4
of	of	k?
the	the	k?
world	world	k6eAd1
transfer	transfer	k1gInSc4
record	recorda	k1gFnPc2
<g/>
,	,	kIx,
BBC	BBC	kA
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc1d1
2	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DOYLE	DOYLE	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
100	#num#	k4
most	most	k1gInSc1
expensive	expensiev	k1gFnSc2
football	footbalnout	k5eAaPmAgInS
transfers	transfers	k1gInSc1
of	of	k?
all	all	k?
time	time	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-04	2021-03-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
European	European	k1gInSc1
Golden	Goldna	k1gFnPc2
Shoe	Sho	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Golden	Goldno	k1gNnPc2
Boot	Boot	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Soulier	Soulier	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
Awards	Awards	k1gInSc1
<g/>
,	,	kIx,
RSSSF	RSSSF	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
pro	pro	k7c4
nejlepšího	dobrý	k2eAgMnSc4d3
fotbalistu	fotbalista	k1gMnSc4
světa	svět	k1gInSc2
získal	získat	k5eAaPmAgMnS
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
Fotbal	fotbal	k1gInSc1
<g/>
.	.	kIx.
sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
ihned	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Nejlepší	dobrý	k2eAgMnSc1d3
fotbalista	fotbalista	k1gMnSc1
není	být	k5eNaImIp3nS
mistr	mistr	k1gMnSc1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
získal	získat	k5eAaPmAgInS
opět	opět	k6eAd1
Ronaldo	Ronaldo	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-01-12	2015-01-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
IRISH	IRISH	kA
<g/>
,	,	kIx,
Ollie	Ollie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CRISTIANO	CRISTIANO	kA
RONALDO	RONALDO	kA
TAKES	TAKES	kA
RAUL	RAUL	kA
<g/>
’	’	k?
<g/>
S	s	k7c7
NO	no	k9
<g/>
.7	.7	k4
SHIRT	SHIRT	kA
AT	AT	kA
REAL	Real	k1gInSc1
MADRID	Madrid	k1gInSc1
<g/>
,	,	kIx,
BENZEMA	BENZEMA	kA
MOVES	MOVES	kA
TO	ten	k3xDgNnSc4
NO	no	k9
<g/>
.9	.9	k4
<g/>
,	,	kIx,
XABI	XABI	kA
ALONSO	ALONSO	kA
TAKES	TAKES	kA
NO	no	k9
<g/>
.14	.14	k4
<g/>
.	.	kIx.
whoateallthepies	whoateallthepiesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
tv	tv	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-08-03	2010-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
scores	scores	k1gInSc1
four	four	k1gMnSc1
in	in	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
victory	victor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-10-23	2010-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
matches	matches	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
treble	treble	k6eAd1
as	as	k9
Real	Real	k1gInSc1
and	and	k?
Barca	Barca	k1gFnSc1
go	go	k?
goal	goal	k1gInSc4
crazy	craza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-11-20	2010-11-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
FIFA	FIFA	kA
za	za	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
získal	získat	k5eAaPmAgMnS
Argentinec	Argentinec	k1gMnSc1
Messi	Messe	k1gFnSc4
<g/>
.	.	kIx.
irozhlas	irozhlas	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-01-10	2011-01-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rampant	Rampant	k1gInSc1
Madrid	Madrid	k1gInSc1
hit	hit	k1gInSc1
eight	eight	k1gInSc1
past	past	k1gFnSc1
Levante	Levant	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-12-22	2010-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SYMCOX	SYMCOX	kA
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
La	la	k1gNnSc1
Liga	liga	k1gFnSc1
-	-	kIx~
Ronaldo	Ronaldo	k1gNnSc1
hat-trick	hat-trick	k6eAd1
sees	sees	k6eAd1
Real	Real	k1gInSc4
past	past	k1gFnSc1
Villarreal	Villarreal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yahoo	Yahoo	k1gNnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
/	/	kIx~
Eurosport	eurosport	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-01-09	2011-01-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOWE	LOWE	kA
<g/>
,	,	kIx,
Sid	Sid	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
No	no	k9
redemption	redemption	k1gInSc1
for	forum	k1gNnPc2
Manuel	Manuel	k1gMnSc1
Pellegrini	Pellegrin	k2eAgMnPc1d1
on	on	k3xPp3gMnSc1
a	a	k8xC
grim	grim	k6eAd1
return	return	k1gNnSc4
to	ten	k3xDgNnSc4
the	the	k?
Bernabéu	Bernabé	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-03-04	2011-03-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Madrid	Madrid	k1gInSc1
clinch	clinch	k1gInSc1
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gFnSc1
Sports	Sportsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-04-21	2011-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
sets	sets	k1gInSc1
new	new	k?
Primera	primera	k1gFnSc1
Liga	liga	k1gFnSc1
scoring	scoring	k1gInSc4
record	record	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-05-21	2011-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
scores	scores	k1gMnSc1
three	threat	k5eAaPmIp3nS
as	as	k9
Madrid	Madrid	k1gInSc1
hit	hit	k1gInSc1
Zaragoza	Zaragoza	k1gFnSc1
for	forum	k1gNnPc2
six	six	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-08-28	2011-08-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Hat	hat	k0
tricks	tricks	k1gInSc1
fuel	fuel	k1gMnSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Real	Real	k1gInSc1
to	ten	k3xDgNnSc4
romps	romps	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-09-24	2011-09-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
grabs	grabs	k6eAd1
hat-trick	hat-trick	k6eAd1
as	as	k9
Real	Real	k1gInSc1
go	go	k?
top	topit	k5eAaImRp2nS
in	in	k?
Spain	Spaina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-10-22	2011-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Gabe	Gabe	k1gFnSc1
Lezra	Lezra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
7-1	7-1	k4
Osasuna	Osasuna	k1gFnSc1
Match	Matcha	k1gFnPc2
Recap	Recap	k1gMnSc1
<g/>
:	:	kIx,
Madrid	Madrid	k1gInSc1
Score	Scor	k1gInSc5
7	#num#	k4
In	In	k1gFnSc7
Display	Displaa	k1gFnSc2
For	forum	k1gNnPc2
Asian	Asian	k1gMnSc1
Markets	Markets	k1gInSc1
<g/>
.	.	kIx.
managingmadrid	managingmadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-11-07	2011-11-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCOTT	SCOTT	kA
<g/>
,	,	kIx,
Charlie	Charlie	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sevilla	Sevilla	k1gFnSc1
2-6	2-6	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
hits	hits	k1gInSc1
hat	hat	k0
trick	trick	k1gMnSc1
and	and	k?
Callejon	Callejon	k1gMnSc1
<g/>
,	,	kIx,
Di	Di	k1gMnSc1
Maria	Mario	k1gMnSc2
and	and	k?
Altintop	Altintop	k1gInSc1
also	also	k6eAd1
strike	strikat	k5eAaPmIp3nS
as	as	k9
Jose	Jose	k1gInSc1
Mourinho	Mourin	k1gMnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
men	men	k?
thrash	thrash	k1gInSc1
Andalusians	Andalusiansa	k1gFnPc2
to	ten	k3xDgNnSc4
move	move	k6eAd1
three	threat	k5eAaPmIp3nS
points	points	k6eAd1
clear	clear	k1gInSc1
of	of	k?
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-12-17	2011-12-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HAYWARD	HAYWARD	kA
<g/>
,	,	kIx,
Ben	Ben	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
outscores	outscores	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
in	in	k?
2011	#num#	k4
-	-	kIx~
but	but	k?
the	the	k?
Argentine	Argentin	k1gInSc5
has	hasit	k5eAaImRp2nS
the	the	k?
last	last	k2eAgMnSc1d1
laugh	laugh	k1gMnSc1
with	with	k1gMnSc1
five	fivat	k5eAaPmIp3nS
trophies	trophies	k1gInSc4
to	ten	k3xDgNnSc1
one	one	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2011-12-25	2011-12-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Messi	Messe	k1gFnSc4
vows	vows	k6eAd1
to	ten	k3xDgNnSc4
share	shar	k1gInSc5
Ballon	Ballon	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gMnSc1
with	with	k1gMnSc1
Xavi	Xavi	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-01-09	2012-01-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
sixth	sixth	k1gMnSc1
hat-trick	hat-trick	k1gMnSc1
of	of	k?
the	the	k?
season	season	k1gInSc1
sees	sees	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
open	open	k1gInSc1
10	#num#	k4
<g/>
-point	-pointa	k1gFnPc2
lead	lead	k6eAd1
over	over	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
in	in	k?
La	la	k1gNnSc1
Liga	liga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegrapha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-02-13	2012-02-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HÄRING	HÄRING	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
sprint	sprint	k1gInSc1
mezi	mezi	k7c4
legendy	legenda	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2012	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
60	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
37	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příloha	příloha	k1gFnSc1
Eurofotbal	Eurofotbal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1210	#num#	k4
<g/>
-	-	kIx~
<g/>
8383	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
scores	scores	k1gMnSc1
hat-trick	hat-trick	k1gMnSc1
as	as	k9
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
defeat	defeat	k1gInSc1
Atletico	Atletico	k1gNnSc1
4-1	4-1	k4
to	ten	k3xDgNnSc1
maintain	maintain	k2eAgInSc1d1
four-point	four-point	k1gInSc1
cushion	cushion	k1gInSc1
over	over	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Daily	Daila	k1gFnSc2
Telegraph	Telegrapha	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-04-12	2012-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Marcus	Marcus	k1gMnSc1
Kwesi	Kwese	k1gFnSc3
O	O	kA
<g/>
'	'	kIx"
<g/>
Mard	Mard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jose	Jos	k1gMnSc4
Mourinho	Mourin	k1gMnSc4
Boasts	Boastsa	k1gFnPc2
as	as	k9
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
and	and	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
Smash	Smasha	k1gFnPc2
Records	Recordsa	k1gFnPc2
en	en	k?
Route	Rout	k1gInSc5
to	ten	k3xDgNnSc4
La	la	k1gNnSc4
Liga	liga	k1gFnSc1
Title	titla	k1gFnSc3
<g/>
.	.	kIx.
nesn	nesn	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-05-15	2012-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
3-2	3-2	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-23	2012-08-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOWE	LOWE	kA
<g/>
,	,	kIx,
Sid	Sid	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
beat	beat	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
to	ten	k3xDgNnSc1
win	win	k?
Spain	Spain	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Super	super	k1gInSc7
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-30	2012-08-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TALINTYRE	TALINTYRE	kA
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
:	:	kIx,
Could	Could	k1gInSc1
Superstar	superstar	k1gFnSc2
Be	Be	k1gFnSc2
"	"	kIx"
<g/>
Sad	sad	k1gInSc1
<g/>
"	"	kIx"
at	at	k?
Father	Fathra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Death	Deatha	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-09-04	2012-09-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TYNAN	TYNAN	kA
<g/>
,	,	kIx,
Gordon	Gordon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Champions	Champions	k1gInSc1
League	Leagu	k1gFnSc2
round-up	round-up	k1gMnSc1
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
grabs	grabsa	k1gFnPc2
another	anothra	k1gFnPc2
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
hat-trick	hat-trick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-10-04	2012-10-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Messi	Messe	k1gFnSc3
crowned	crowned	k1gMnSc1
world	world	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
best	besta	k1gFnPc2
for	forum	k1gNnPc2
record	record	k1gMnSc1
fourth	fourth	k1gMnSc1
time	timat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-08	2013-01-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Iker	Iker	k1gInSc1
Casillas	Casillas	k1gMnSc1
rejects	rejectsa	k1gFnPc2
captain	captain	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
armband	armbanda	k1gFnPc2
gesture	gestur	k1gMnSc5
from	from	k1gInSc1
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metro	metro	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-07	2013-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
scores	scores	k1gInSc1
300	#num#	k4
<g/>
th	th	k?
club	club	k1gInSc1
goal	goal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Independent	independent	k1gMnSc1
Online	Onlin	k1gInSc5
(	(	kIx(
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-27	2013-01-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LOWE	LOWE	kA
<g/>
,	,	kIx,
Sid	Sid	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
claw	claw	k?
back	backa	k1gFnPc2
Barcelona	Barcelona	k1gFnSc1
thanks	thanks	k6eAd1
to	ten	k3xDgNnSc4
Raphaël	Raphaël	k1gInSc1
Varane	varan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
late	lat	k1gInSc5
header	headra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Guardin	Guardina	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-01-30	2013-01-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lewandowski	Lewandowski	k1gNnSc4
nieRealny	nieRealna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cztery	Czter	k1gInPc7
gole	gol	k1gMnSc2
Polaka	Polak	k1gMnSc2
w	w	k?
półfinale	półfinale	k6eAd1
Ligi	Lige	k1gFnSc4
Mistrzów	Mistrzów	k1gFnSc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Gazeta	gazeta	k1gFnSc1
Wyborcza	Wyborcza	k1gFnSc1
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Borussia	Borussia	k1gFnSc1
Dortmund	Dortmund	k1gInSc1
-	-	kIx~
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
4	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
reaches	reaches	k1gInSc1
200	#num#	k4
goals	goalsa	k1gFnPc2
for	forum	k1gNnPc2
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
with	with	k1gInSc1
Malaga	Malaga	k1gFnSc1
strike	strike	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-05-08	2013-05-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
nets	nets	k1gInSc1
hat-trick	hat-trick	k1gMnSc1
in	in	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
rout	routa	k1gFnPc2
of	of	k?
Real	Real	k1gInSc1
Sociedad	Sociedad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-11-09	2013-11-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Galatasaray	Galatasaraa	k1gFnSc2
1-6	1-6	k4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
:	:	kIx,
Ronaldo	Ronaldo	k1gNnSc1
hat	hat	k0
trick	trick	k1gInSc4
torpedoes	torpedoes	k1gInSc4
Turks	Turksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-17	2013-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WALLACE	WALLACE	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
agrees	agrees	k1gInSc1
£	£	k?
<g/>
76	#num#	k4
<g/>
m	m	kA
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
deal	deanout	k5eAaPmAgInS
to	ten	k3xDgNnSc4
end	end	k?
hopes	hopes	k1gInSc1
of	of	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
return	return	k1gMnSc1
-	-	kIx~
and	and	k?
insists	insists	k1gInSc1
he	he	k0
has	hasit	k5eAaImRp2nS
'	'	kIx"
<g/>
no	no	k9
issue	issue	k1gFnSc1
<g/>
'	'	kIx"
with	with	k1gMnSc1
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-09-16	2013-09-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
si	se	k3xPyFc3
poradil	poradit	k5eAaPmAgInS
s	s	k7c7
Juventusem	Juventus	k1gInSc7
<g/>
,	,	kIx,
Ibrahimovic	Ibrahimovice	k1gFnPc2
se	se	k3xPyFc4
blýskl	blýsknout	k5eAaPmAgInS
čtyřmi	čtyři	k4xCgInPc7
góly	gól	k1gInPc7
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
24	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
V	v	k7c6
Istanbulu	Istanbul	k1gInSc6
se	se	k3xPyFc4
duel	duel	k1gInSc1
Ligy	liga	k1gFnSc2
mistrů	mistr	k1gMnPc2
nedohrál	dohrát	k5eNaPmAgMnS
<g/>
,	,	kIx,
Anderlecht	Anderlecht	k1gMnSc1
měl	mít	k5eAaImAgMnS
tři	tři	k4xCgInPc4
vyloučené	vyloučený	k2eAgInPc4d1
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
11	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
HOMEWOOD	HOMEWOOD	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emotional	Emotional	k1gFnSc6
Ronaldo	Ronaldo	k1gNnSc1
ends	ends	k6eAd1
Messi	Messe	k1gFnSc3
run	runa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-01-14	2014-01-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Barcelona	Barcelona	k1gFnSc1
otočila	otočit	k5eAaPmAgFnS
zápas	zápas	k1gInSc4
díky	díky	k7c3
Pedrovi	Pedr	k1gMnSc3
<g/>
,	,	kIx,
Atlético	Atlético	k6eAd1
a	a	k8xC
Real	Real	k1gInSc1
vyhrály	vyhrát	k5eAaPmAgInP
těsně	těsně	k6eAd1
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
23	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Spanish	Spanish	k1gMnSc1
La	la	k1gNnSc2
Liga	liga	k1gFnSc1
Stats	Statsa	k1gFnPc2
<g/>
:	:	kIx,
Top	topit	k5eAaImRp2nS
Goal	Goal	k1gInSc1
Scorers	Scorers	k1gInSc4
–	–	k?
2013	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Entertainment	Entertainment	k1gInSc1
and	and	k?
Sports	Sports	k1gInSc1
Programming	Programming	k1gInSc1
Network	network	k1gInSc1
(	(	kIx(
<g/>
ESPN	ESPN	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Record-breaking	Record-breaking	k1gInSc1
Ronaldo	Ronaldo	k1gNnSc1
takes	takes	k1gInSc1
scoring	scoring	k1gInSc1
honours	honours	k1gInSc1
<g/>
,	,	kIx,
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Real	Real	k1gInSc1
slaví	slavit	k5eAaImIp3nS
desátý	desátý	k4xOgInSc1
triumf	triumf	k1gInSc1
v	v	k7c6
Lize	liga	k1gFnSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
Atlético	Atlético	k6eAd1
porazil	porazit	k5eAaPmAgMnS
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
↑	↑	k?
HART	HART	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
fires	firesa	k1gFnPc2
Madrid	Madrid	k1gInSc1
to	ten	k3xDgNnSc4
Super	super	k6eAd1
Cup	cup	k1gInSc1
glory	glora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-12	2014-08-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BLANCHETTE	BLANCHETTE	kA
<g/>
,	,	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
Injury	Injura	k1gFnPc1
<g/>
:	:	kIx,
Updates	Updates	k1gMnSc1
on	on	k3xPp3gMnSc1
Real	Real	k1gInSc4
Madrid	Madrid	k1gInSc1
Star	Star	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Status	status	k1gInSc1
and	and	k?
Return	Return	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-20	2014-08-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TANSEY	TANSEY	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atletico	Atletico	k1gNnSc1
Madrid	Madrid	k1gInSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
:	:	kIx,
Winners	Winners	k1gInSc1
and	and	k?
Losers	Losers	k1gInSc1
from	from	k1gMnSc1
Spanish	Spanish	k1gMnSc1
Super	super	k2eAgInSc4d1
Cup	cup	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-23	2014-08-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pan	Pan	k1gMnSc1
Jedinečný	jedinečný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
čtyři	čtyři	k4xCgInPc4
dny	den	k1gInPc4
sedm	sedm	k4xCc4
gólů	gól	k1gInPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yInSc1,k3yRnSc1,k3yQnSc1
zastaví	zastavit	k5eAaPmIp3nS
Cristiana	Cristian	k1gMnSc4
Ronalda	Ronald	k1gMnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-09-24	2014-09-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
breaks	breaks	k6eAd1
La	la	k1gNnSc4
Liga	liga	k1gFnSc1
hat	hat	k0
trick	tricka	k1gFnPc2
record	recordo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
FC	FC	kA
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
A.S	A.S	k1gFnSc1
<g/>
,	,	kIx,
Petit	petit	k1gInSc1
Press	Press	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
rozdrvil	rozdrvit	k5eAaPmAgInS,k5eAaImAgInS
Granadu	Granada	k1gFnSc4
9	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc4
strelil	strelit	k5eAaPmAgMnS,k5eAaImAgMnS
päť	päť	k?
gólov	gólov	k1gInSc4
<g/>
.	.	kIx.
sport	sport	k1gInSc4
<g/>
.	.	kIx.
<g/>
sme	sme	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
scores	scores	k1gMnSc1
hat	hat	k0
trick	trick	k1gInSc1
to	ten	k3xDgNnSc1
end	end	k?
year	year	k1gInSc1
with	with	k1gInSc1
personal-best	personal-best	k1gFnSc1
61	#num#	k4
goals	goalsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-05-23	2015-05-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
fires	fires	k1gMnSc1
five	fivat	k5eAaPmIp3nS
to	ten	k3xDgNnSc4
become	becom	k1gInSc5
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
record	record	k6eAd1
La	la	k1gNnPc7
Liga	liga	k1gFnSc1
scorer	scorer	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WALKER	WALKER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
reaches	reachesa	k1gFnPc2
80	#num#	k4
goals	goalsa	k1gFnPc2
as	as	k9
Madrid	Madrid	k1gInSc1
fell	fell	k1gMnSc1
Shakhtar	Shakhtar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-15	2015-09-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
forward	forward	k1gInSc1
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
scores	scores	k1gInSc1
500	#num#	k4
<g/>
th	th	k?
career	career	k1gMnSc1
goal	goal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-09-30	2015-09-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WALKER	WALKER	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
rampant	rampanta	k1gFnPc2
as	as	k9
Madrid	Madrid	k1gInSc1
put	puta	k1gFnPc2
eight	eight	k1gInSc4
past	past	k1gFnSc4
Malmö	Malmö	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-12-08	2015-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
7	#num#	k4
Celta	celta	k1gFnSc1
Vigo	Vigo	k1gNnSc1
1	#num#	k4
<g/>
:	:	kIx,
Ronaldo	Ronaldo	k1gNnSc1
hits	hitsa	k1gFnPc2
four	foura	k1gFnPc2
to	ten	k3xDgNnSc1
beat	beat	k1gInSc4
Zarra	Zarr	k1gMnSc2
mark	mark	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Four	Four	k1gMnSc1
Four	Four	k1gMnSc1
Two	Two	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-03-05	2016-03-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JOHNSTON	JOHNSTON	kA
<g/>
,	,	kIx,
Neil	Neil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
3-0	3-0	k4
VfL	VfL	k1gMnPc2
Wolfsburg	Wolfsburg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-04-12	2016-04-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
STEVENS	STEVENS	kA
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
brands	brands	k6eAd1
his	his	k1gNnSc7
critics	criticsa	k1gFnPc2
'	'	kIx"
<g/>
jealous	jealous	k1gMnSc1
<g/>
'	'	kIx"
after	after	k1gMnSc1
third	third	k1gMnSc1
Champions	Champions	k1gInSc4
League	League	k1gNnSc2
triumph	triumpha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-05-30	2016-05-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
takes	takesa	k1gFnPc2
scoring	scoring	k1gInSc1
prize	prize	k6eAd1
again	again	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-05-28	2016-05-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HANAGUDU	HANAGUDU	kA
<g/>
,	,	kIx,
Ashwin	Ashwin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
to	ten	k3xDgNnSc1
miss	miss	k1gFnPc1
the	the	k?
first	first	k1gMnSc1
three	thre	k1gFnSc2
games	games	k1gMnSc1
of	of	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
season	season	k1gInSc1
due	due	k?
to	ten	k3xDgNnSc1
knee	knee	k1gNnSc1
injury	injura	k1gFnSc2
-	-	kIx~
reports	reports	k1gInSc1
<g/>
.	.	kIx.
sportskeeda	sportskeeda	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-17-19	2016-17-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CRISTIANO	CRISTIANO	kA
RONALDO	RONALDO	kA
<g/>
:	:	kIx,
I	I	kA
COULDN	COULDN	kA
<g/>
’	’	k?
<g/>
T	T	kA
CELEBRATE	CELEBRATE	kA
GOAL	GOAL	kA
AGAINST	AGAINST	kA
SPORTING	SPORTING	kA
<g/>
,	,	kIx,
THEY	Thea	k1gFnPc1
MADE	MADE	kA
ME	ME	kA
WHO	WHO	kA
I	i	k8xC
AM	AM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eurosport	eurosport	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-09-15	2016-09-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
signed	signed	k1gInSc1
his	his	k1gNnSc4
contract	contract	k5eAaPmF
extension	extension	k1gInSc4
with	with	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
realmadrid	realmadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-11-07	2016-11-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gMnSc5
<g/>
,	,	kIx,
all-time	all-tiet	k5eAaPmRp1nP,k5eAaImRp1nP,k5eAaBmRp1nP
top	topit	k5eAaImRp2nS
scorer	scorer	k1gInSc1
in	in	k?
the	the	k?
derby	derby	k1gNnSc1
against	against	k1gMnSc1
Atlético	Atlético	k1gMnSc1
<g/>
.	.	kIx.
realmadrid	realmadrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-11-20	2016-11-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
forward	forward	k1gInSc1
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
scores	scores	k1gInSc1
500	#num#	k4
<g/>
th	th	k?
club	club	k1gInSc1
goal	goal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-15	2016-12-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Madrid	Madrid	k1gInSc1
see	see	k?
off	off	k?
spirited	spirited	k1gMnSc1
Kashima	Kashima	k1gNnSc1
in	in	k?
electric	electrice	k1gInPc2
extra	extra	k2eAgFnSc2d1
time	tim	k1gFnSc2
final	finat	k5eAaPmAgMnS,k5eAaBmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-18	2016-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
Modric	Modric	k1gMnSc1
and	and	k?
Shibasaki	Shibasak	k1gFnSc2
sweep	sweep	k1gMnSc1
awards	awards	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-18	2016-12-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
world	world	k1gMnSc1
at	at	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
feet	feeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-01-10	2017-01-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
is	is	k?
now	now	k?
level	level	k1gInSc1
with	with	k1gMnSc1
Hugo	Hugo	k1gMnSc1
Sanchez	Sanchez	k1gMnSc1
<g/>
…	…	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tribuna	tribuna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-01-16	2017-01-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
makes	makes	k1gMnSc1
history	histor	k1gInPc4
with	with	k1gInSc1
100	#num#	k4
<g/>
th	th	k?
European	European	k1gMnSc1
goal	goal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-04-18	2017-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
record-breaking	record-breaking	k1gInSc1
double	double	k2eAgInSc4d1
gives	gives	k1gInSc4
Madrid	Madrid	k1gInSc1
win	win	k?
at	at	k?
Bayern	Bayern	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-04-18	2017-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HANSON	HANSON	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
scores	scores	k1gInSc1
100	#num#	k4
<g/>
th	th	k?
Champions	Champions	k1gInSc1
League	Leagu	k1gFnSc2
goal	goala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-04-18	2017-04-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROSTANCE	ROSTANCE	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FT	FT	kA
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
3-0	3-0	k4
Atletico	Atletico	k6eAd1
Madrid	Madrid	k1gInSc1
-	-	kIx~
Ronaldo	Ronaldo	k1gNnSc1
hat-trick	hat-tricka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-02	2017-05-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SMITH	SMITH	kA
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
smashes	smashes	k1gInSc4
Jimmy	Jimma	k1gFnSc2
Greaves	Greaves	k1gInSc1
<g/>
'	'	kIx"
all-time	all-timat	k5eAaPmIp3nS
goal	goal	k1gMnSc1
record	record	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sky	Sky	k1gFnSc1
Sports	Sportsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-04-21	2020-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
nets	nets	k1gInSc1
600	#num#	k4
<g/>
th	th	k?
goal	goal	k1gInSc1
<g/>
,	,	kIx,
boasts	boasts	k1gInSc1
'	'	kIx"
<g/>
I	i	k9
was	was	k?
very	very	k1gInPc1
good	good	k1gInSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-06-03	2017-06-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
scores	scores	k1gMnSc1
and	and	k?
is	is	k?
sent	sent	k1gInSc1
off	off	k?
in	in	k?
win	win	k?
over	over	k1gInSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-08-13	2017-08-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Best	Best	k1gMnSc1
FIFA	FIFA	kA
Men	Men	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Player	Playra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-23	2017-10-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
3-2	3-2	k4
Borussia	Borussium	k1gNnSc2
Dortmund	Dortmund	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-06	2017-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ballon	Ballon	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gFnSc1
2017	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
beats	beats	k6eAd1
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
to	ten	k3xDgNnSc1
win	win	k?
fifth	fifth	k1gInSc1
award	award	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-07	2017-12-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
1-0	1-0	k4
Grê	Grê	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-12-16	2017-12-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CORRIGAN	CORRIGAN	kA
<g/>
,	,	kIx,
Dermot	Dermot	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
becomes	becomes	k1gMnSc1
fastest	fastest	k1gMnSc1
to	ten	k3xDgNnSc4
score	scor	k1gInSc5
300	#num#	k4
<g/>
th	th	k?
goal	goal	k1gMnSc1
in	in	k?
La	la	k1gNnSc2
Liga	liga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-04	2018-03-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EMONS	EMONS	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
6-3	6-3	k4
Girona	Giron	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-03-18	2018-03-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
NEVIN	nevina	k1gFnPc2
<g/>
,	,	kIx,
Pat	pata	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
bicycle	bicycl	k1gMnSc4
kick	kick	k6eAd1
<g/>
:	:	kIx,
Night	Night	k2eAgInSc1d1
Juventus	Juventus	k1gInSc1
Stadium	stadium	k1gNnSc1
rose	rosa	k1gFnSc3
to	ten	k3xDgNnSc4
applaud	applaud	k6eAd1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
forward	forward	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-03	2018-04-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
edges	edges	k1gInSc4
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
past	pasta	k1gFnPc2
Juventus	Juventus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-11	2018-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Madrid	Madrid	k1gInSc1
beat	beat	k1gInSc1
Liverpool	Liverpool	k1gInSc1
to	ten	k3xDgNnSc1
complete	complést	k5eAaPmIp3nS
hat-trick	hat-trick	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-26	2018-05-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROMANO	Romano	k1gMnSc1
<g/>
,	,	kIx,
Fabrizio	Fabrizio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
joining	joining	k1gInSc1
Juventus	Juventus	k1gInSc1
in	in	k?
€	€	k?
<g/>
100	#num#	k4
<g/>
m	m	kA
deal	deal	k1gInSc1
from	from	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-10	2018-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
to	ten	k3xDgNnSc1
Juventus	Juventus	k1gInSc1
<g/>
:	:	kIx,
Is	Is	k1gFnSc1
£	£	k?
<g/>
99	#num#	k4
<g/>
m	m	kA
signing	signing	k1gInSc1
a	a	k8xC
good	good	k1gInSc1
deal	deala	k1gFnPc2
for	forum	k1gNnPc2
Italian	Italiany	k1gInPc2
champions	champions	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-10	2018-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
breaks	breaks	k6eAd1
transfer	transfer	k1gInSc4
record	recorda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Football-italia	Football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-10	2018-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
ends	ends	k6eAd1
Juventus	Juventus	k1gInSc4
drought	drought	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Football-italia	Football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-16	2018-09-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Byla	být	k5eAaImAgFnS
to	ten	k3xDgNnSc4
jen	jen	k9
otázka	otázka	k1gFnSc1
času	čas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
zase	zase	k9
jásá	jásat	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
ve	v	k7c6
společnosti	společnost	k1gFnSc6
Bicana	Bicana	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-17	2018-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Valencia	Valencia	k1gFnSc1
0-2	0-2	k4
Juventus	Juventus	k1gInSc1
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
sent	sent	k1gMnSc1
off	off	k?
but	but	k?
Italians	Italians	k1gInSc1
still	still	k1gMnSc1
win	win	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-19	2018-09-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BURTON	BURTON	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
first	first	k1gMnSc1
to	ten	k3xDgNnSc1
100	#num#	k4
Champions	Champions	k1gInSc4
League	Leagu	k1gFnSc2
wins	wins	k6eAd1
as	as	k9
he	he	k0
chases	chases	k1gMnSc1
down	downa	k1gFnPc2
sixth	sixth	k1gMnSc1
crown	crown	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-27	2018-11-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fiorentina	Fiorentina	k1gFnSc1
0-3	0-3	k4
Juventus	Juventus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-01	2018-12-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAS	DAS	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Luka	luka	k1gNnPc1
Modric	Modric	k1gMnSc1
Wins	Winsa	k1gFnPc2
Ballon	Ballon	k1gInSc4
d	d	k?
<g/>
’	’	k?
<g/>
Or	Or	k1gFnSc1
<g/>
,	,	kIx,
Ending	Ending	k1gInSc1
Decade	Decad	k1gInSc5
of	of	k?
Ronaldo	Ronaldo	k1gNnSc4
and	and	k?
Messi	Messe	k1gFnSc3
Triumphs	Triumphs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
NY	NY	kA
Times	Timesa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-03	2018-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc4
wins	winsa	k1gFnPc2
UEFA	UEFA	kA
Men	Men	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Player	Player	k1gInSc1
of	of	k?
the	the	k?
Year	Year	k1gMnSc1
award	award	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-30	2018-08-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
W.	W.	kA
Allen	allen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Best	Best	k1gMnSc1
FIFA	FIFA	kA
Football	Football	k1gInSc1
Awards	Awards	k1gInSc1
2018	#num#	k4
<g/>
:	:	kIx,
Modric	Modrice	k1gFnPc2
wins	wins	k1gInSc1
Best	Best	k1gMnSc1
Player	Player	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AS	as	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-24	2018-09-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BURNTON	BURNTON	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
1-0	1-0	k4
Milan	Milana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-01-16	2019-01-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
draws	draws	k6eAd1
level	level	k1gInSc1
with	witha	k1gFnPc2
Mbappe	Mbapp	k1gInSc5
and	and	k?
is	is	k?
three	threat	k5eAaPmIp3nS
off	off	k?
Messi	Messe	k1gFnSc4
in	in	k?
Golden	Goldna	k1gFnPc2
Shoe	Shoe	k1gFnSc1
race	race	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marca	Marca	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-10	2019-02-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
hat-trick	hat-trick	k1gMnSc1
against	against	k1gMnSc1
Atlético	Atlético	k1gMnSc1
fires	firesa	k1gFnPc2
Juventus	Juventus	k1gInSc1
into	into	k1gMnSc1
last	last	k1gMnSc1
eight	eight	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-03-12	2019-03-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Champions	Championsa	k1gFnPc2
League	League	k1gNnSc1
record	record	k1gMnSc1
-	-	kIx~
can	can	k?
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
match	matcha	k1gFnPc2
it	it	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-11	2019-04-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HENSON	HENSON	kA
<g/>
,	,	kIx,
Mike	Mike	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
1-2	1-2	k4
Ajax	Ajax	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-16	2019-04-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DUDKO	DUDKO	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
,	,	kIx,
Juventus	Juventus	k1gInSc1
Clinch	clinch	k1gInSc1
2019	#num#	k4
Serie	serie	k1gFnSc2
A	a	k8xC
Title	titla	k1gFnSc6
Via	via	k7c4
Fiorentina	Fiorentin	k2eAgMnSc4d1
Own	Own	k1gMnSc4
Goal	Goal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleacher	Bleachra	k1gFnPc2
Report	report	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-20	2019-04-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Inter	Inter	k1gMnSc1
Milan	Milan	k1gMnSc1
1-1	1-1	k4
Juventus	Juventus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-27	2019-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Ronaldo	Ronaldo	k1gNnSc1
MVP	MVP	kA
in	in	k?
Serie	serie	k1gFnSc1
A	a	k8xC
Awards	Awards	k1gInSc1
<g/>
.	.	kIx.
football-italia	football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-05-18	2019-05-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SERIE	serie	k1gFnSc1
A	a	k8xC
TOP	topit	k5eAaImRp2nS
SCORERS	SCORERS	kA
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
www.football-italia.net	www.football-italia.net	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DAMPF	DAMPF	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
becomes	becomes	k1gMnSc1
first	first	k1gMnSc1
player	player	k1gMnSc1
to	ten	k3xDgNnSc4
win	win	k?
Europe	Europ	k1gInSc5
<g/>
’	’	k?
<g/>
s	s	k7c7
top	topit	k5eAaImRp2nS
3	#num#	k4
leagues	leaguesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
AP	ap	kA
News	Newsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-04-22	2019-04-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BEGLEY	BEGLEY	kA
<g/>
,	,	kIx,
Emlyn	Emlyn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
edge	edg	k1gFnSc2
Napoli	Napole	k1gFnSc3
in	in	k?
seven-goal	seven-goal	k1gInSc1
Serie	serie	k1gFnSc1
A	a	k8xC
thriller	thriller	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-31	2019-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Messi	Messe	k1gFnSc4
<g/>
,	,	kIx,
Rapinoe	Rapinoe	k1gFnSc1
crowned	crowned	k1gMnSc1
The	The	k1gMnSc1
Best	Best	k1gMnSc1
in	in	k?
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-09-23	2019-09-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
JACOT	JACOT	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
Smashes	Smashesa	k1gFnPc2
Champions	Championsa	k1gFnPc2
League	Leagu	k1gFnSc2
Record	Record	k1gInSc1
During	During	k1gInSc1
Juventus	Juventus	k1gInSc1
<g/>
'	'	kIx"
Win	Win	k1gMnSc1
Over	Over	k1gMnSc1
Bayer	Bayer	k1gMnSc1
Leverkusen	Leverkusen	k2eAgMnSc1d1
<g/>
.	.	kIx.
90	#num#	k4
<g/>
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-10-02	2019-10-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HEYES	HEYES	kA
<g/>
,	,	kIx,
Apollo	Apollo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc1
matches	matches	k1gMnSc1
Maldini	Maldin	k2eAgMnPc1d1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
174	#num#	k4
appearances	appearances	k1gMnSc1
in	in	k?
UEFA	UEFA	kA
club	club	k1gInSc1
competitions	competitions	k1gInSc1
<g/>
.	.	kIx.
calciomercato	calciomercato	k6eAd1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-11-06	2019-11-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
:	:	kIx,
Juventus	Juventus	k1gInSc1
forward	forwarda	k1gFnPc2
defies	defies	k1gMnSc1
gravity	gravita	k1gFnSc2
with	with	k1gMnSc1
jump	jump	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-19	2019-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Juventus	Juventus	k1gInSc1
4-0	4-0	k4
Cagliari	Cagliar	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-06	2020-01-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Juve	Juve	k1gInSc1
<g/>
,	,	kIx,
Ronaldo	Ronaldo	k1gNnSc1
come	come	k1gFnPc2
Trezeguet	Trezegueta	k1gFnPc2
<g/>
:	:	kIx,
in	in	k?
gol	gol	k?
per	pero	k1gNnPc2
la	la	k1gNnPc2
nona	non	k2eAgFnSc1d1
volta	volta	k1gFnSc1
di	di	k?
fila	fila	k1gFnSc1
in	in	k?
Serie	serie	k1gFnSc2
A.	A.	kA
corrieredellosport	corrieredellosport	k1gInSc1
<g/>
.	.	kIx.
<g/>
it	it	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-02	2020-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WRIGHT	WRIGHT	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amazing	Amazing	k1gInSc1
giant	giant	k1gInSc4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
figure	figur	k1gMnSc5
towers	towersit	k5eAaPmRp2nS
over	over	k1gMnSc1
town	town	k1gMnSc1
at	at	k?
Italian	Italian	k1gMnSc1
carnival	carnivat	k5eAaBmAgMnS,k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-02-10	2020-02-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Coppa	Coppa	k1gFnSc1
<g/>
:	:	kIx,
Napoli	Napole	k1gFnSc4
beat	beat	k1gInSc1
Juventus	Juventus	k1gInSc4
on	on	k3xPp3gMnSc1
penalties	penalties	k1gMnSc1
<g/>
.	.	kIx.
football-italia	football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-17	2020-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
overtakes	overtakes	k1gMnSc1
Rui	Rui	k1gMnSc1
Costa	Costa	k1gMnSc1
to	ten	k3xDgNnSc4
become	becom	k1gInSc5
Serie	serie	k1gFnPc4
A	A	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
top	topit	k5eAaImRp2nS
Portuguese	Portuguese	k1gFnSc1
scorer	scorer	k1gMnSc1
<g/>
.	.	kIx.
yahoo	yahoo	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
sports	sports	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-23	2020-06-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Genoa	Genoa	k1gMnSc1
1-3	1-3	k4
Juventus	Juventus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-30	2020-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MCMAHON	MCMAHON	kA
<g/>
,	,	kIx,
Bobby	Bobb	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc4
And	Anda	k1gFnPc2
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
Score	Scor	k1gInSc5
Milestone	Mileston	k1gInSc5
Goals	Goals	k1gInSc4
On	on	k3xPp3gMnSc1
The	The	k1gMnSc1
Same	Sam	k1gMnSc5
Day	Day	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes	forbes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-06-30	2020-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pirlo	Pirlo	k1gNnSc1
po	po	k7c6
vítězném	vítězný	k2eAgInSc6d1
debutu	debut	k1gInSc6
v	v	k7c6
Juve	Juve	k1gFnSc6
řekl	říct	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
víc	hodně	k6eAd2
šetřit	šetřit	k5eAaImF
Ronalda	Ronalda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-09-21	2020-09-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
se	se	k3xPyFc4
po	po	k7c6
19	#num#	k4
dnech	den	k1gInPc6
vyléčil	vyléčit	k5eAaPmAgInS
z	z	k7c2
koronaviru	koronavir	k1gInSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-10-30	2020-10-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
scores	scores	k1gInSc1
750	#num#	k4
<g/>
th	th	k?
career	career	k1gMnSc1
goal	goal	k1gMnSc1
in	in	k?
Juventus	Juventus	k1gInSc1
<g/>
'	'	kIx"
Champions	Champions	k1gInSc1
League	Leagu	k1gInSc2
victory	victor	k1gInPc1
over	over	k1gMnSc1
Dynamo	dynamo	k1gNnSc1
Kiev	Kiev	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-02	2020-12-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Prochází	procházet	k5eAaImIp3nS
těžkým	těžký	k2eAgNnSc7d1
obdobím	období	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
pořád	pořád	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
sport	sport	k1gInSc1
-	-	kIx~
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
to	ten	k3xDgNnSc1
make	makat	k5eAaPmIp3nS
100	#num#	k4
<g/>
th	th	k?
Juve	Juve	k1gInSc1
appearance	appearance	k1gFnSc1
<g/>
.	.	kIx.
football-italia	football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-13	2020-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
scores	scores	k1gMnSc1
two	two	k?
penalties	penalties	k1gMnSc1
in	in	k?
Juventus	Juventus	k1gInSc1
win	win	k?
at	at	k?
Genoa	Genoum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-12-13	2020-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
strikes	strikes	k1gMnSc1
twice	twice	k1gMnSc1
to	ten	k3xDgNnSc4
give	givat	k5eAaPmIp3nS
Juventus	Juventus	k1gInSc1
edge	edg	k1gFnSc2
over	over	k1gMnSc1
Inter	Inter	k1gMnSc1
in	in	k?
Coppa	Copp	k1gMnSc4
Italia	Italius	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-02-02	2021-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
reaches	reaches	k1gInSc4
scoring	scoring	k1gInSc1
landmark	landmark	k1gInSc1
during	during	k1gInSc1
his	his	k1gNnSc1
600	#num#	k4
<g/>
th	th	k?
league	leaguat	k5eAaPmIp3nS
game	game	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2021-03-05	2021-03-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MACHADO	MACHADO	kA
<g/>
,	,	kIx,
Carlos	Carlos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juventus	Juventus	k1gInSc1
3	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
Porto	porto	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
UEFA	UEFA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-09	2021-03-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
kritiku	kritika	k1gFnSc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
nezvládá	zvládat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
místních	místní	k2eAgNnPc2d1
médií	médium	k1gNnPc2
zvažuje	zvažovat	k5eAaImIp3nS
i	i	k9
návrat	návrat	k1gInSc4
do	do	k7c2
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-12	2021-03-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Goalless	Goalless	k1gInSc1
CR7	CR7	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
first	first	k1gMnSc1
time	tim	k1gFnSc2
in	in	k?
16	#num#	k4
years	years	k1gInSc1
<g/>
.	.	kIx.
football-italia	football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-10	2021-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc4
hat-tricks	hat-tricks	k6eAd1
inspired	inspired	k1gMnSc1
by	by	kYmCp3nS
Cagliari	Cagliar	k1gMnPc5
<g/>
.	.	kIx.
football-italia	football-italia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-03-14	2021-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc4
<g/>
,	,	kIx,
eu-football	eu-football	k1gMnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gMnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
–	–	k?
profil	profil	k1gInSc1
<g/>
,	,	kIx,
životopis	životopis	k1gInSc1
<g/>
,	,	kIx,
statistiky	statistika	k1gFnPc1
<g/>
,	,	kIx,
Real-Madrid	Real-Madrid	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.real-madrid.cz	www.real-madrid.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
v	v	k7c6
úžasných	úžasný	k2eAgInPc6d1
dostizích	dostih	k1gInPc6
udolal	udolat	k5eAaPmAgInS
Ibrahimovice	Ibrahimovice	k1gFnPc4
<g/>
,	,	kIx,
na	na	k7c6
MS	MS	kA
je	být	k5eAaImIp3nS
i	i	k9
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
HART	HART	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scolari	Scolari	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Portugal	portugal	k1gInSc1
too	too	k?
strong	strong	k1gInSc1
for	forum	k1gNnPc2
Turkey	Turkea	k1gFnSc2
in	in	k?
EURO	euro	k1gNnSc1
2008	#num#	k4
Group	Group	k1gInSc4
A.	A.	kA
UEFA	UEFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-07	2008-06-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WALLACE	WALLACE	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ronaldo	Ronaldo	k1gNnSc1
shines	shinesa	k1gFnPc2
as	as	k9
Portugal	portugal	k1gInSc4
turn	turna	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
style	styl	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
Ireland	Ireland	k1gInSc1
<g/>
/	/	kIx~
<g/>
Irsko	Irsko	k1gNnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-12	2008-06-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Turks	Turks	k1gInSc1
scrape	scrapat	k5eAaPmIp3nS
through	through	k1gInSc1
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
bow	bow	k?
out	out	k?
in	in	k?
style	styl	k1gInSc5
<g/>
.	.	kIx.
france	franec	k1gInSc2
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-15	2008-06-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Germany	German	k1gInPc1
Beat	beat	k1gInSc1
Portugal	portugal	k1gInSc1
3-2	3-2	k4
to	ten	k3xDgNnSc1
Progress	Progress	k1gInSc1
to	ten	k3xDgNnSc4
Semi-Finals	Semi-Finals	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deutsche	Deutsche	k1gFnSc1
Welle	Welle	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-19	2008-06-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WILSON	WILSON	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Cup	cup	k1gInSc1
2010	#num#	k4
<g/>
:	:	kIx,
Ivory	Ivora	k1gFnSc2
Coast	Coast	k1gInSc1
and	and	k?
Portugal	portugal	k1gInSc1
play	play	k0
out	out	k?
cagey	cagey	k1gInPc1
stalemate	stalemat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-15	2010-06-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FORTUNE	FORTUNE	kA
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
WORLD	WORLD	kA
CUP	cup	k1gInSc4
2010	#num#	k4
<g/>
:	:	kIx,
Portugal	portugal	k1gInSc4
7	#num#	k4
North	North	k1gInSc1
Korea	Korea	k1gFnSc1
0	#num#	k4
-	-	kIx~
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
finally	finall	k1gInPc4
ends	ends	k6eAd1
goal	goanout	k5eAaPmAgInS
drought	drought	k1gInSc1
in	in	k?
red	red	k?
hot	hot	k0
rout	routa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daily	Dail	k1gInPc4
Mail	mail	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-21	2010-06-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Cup	cup	k1gInSc1
2010	#num#	k4
<g/>
:	:	kIx,
Brazil	Brazil	k1gFnSc1
and	and	k?
Portugal	portugal	k1gInSc1
both	both	k1gMnSc1
progress	progressa	k1gFnPc2
but	but	k?
fail	fail	k1gMnSc1
to	ten	k3xDgNnSc1
inspire	inspir	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-25	2010-06-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Subhankar	Subhankar	k1gMnSc1
Mondal	Mondal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Cup	cup	k1gInSc1
2010	#num#	k4
<g/>
:	:	kIx,
Spain	Spain	k1gMnSc1
1-0	1-0	k4
Portugal	portugal	k1gInSc1
-	-	kIx~
David	David	k1gMnSc1
Villa	Villa	k1gMnSc1
breaks	breaksa	k1gFnPc2
down	down	k1gMnSc1
Portuguese	Portuguese	k1gFnSc2
resistance	resistance	k1gFnSc1
to	ten	k3xDgNnSc4
send	send	k6eAd1
European	European	k1gMnSc1
champions	champions	k6eAd1
into	into	k6eAd1
quarter-finals	quarter-finals	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-29	2010-06-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BEVAN	BEVAN	kA
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
2012	#num#	k4
<g/>
:	:	kIx,
Who	Who	k1gFnSc6
will	willa	k1gFnPc2
survive	surviev	k1gFnSc2
the	the	k?
Group	Group	k1gMnSc1
of	of	k?
Death	Death	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-06-16	2012-06-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
2012	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
downs	downsa	k1gFnPc2
a	a	k8xC
double	double	k2eAgNnSc4d1
to	ten	k3xDgNnSc4
join	join	k1gNnSc4
the	the	k?
party	party	k1gFnSc1
spirit	spirit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-06-17	2012-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česko	Česko	k1gNnSc1
-	-	kIx~
Portugalsko	Portugalsko	k1gNnSc1
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
,	,	kIx,
cestu	cesta	k1gFnSc4
k	k	k7c3
medaili	medaile	k1gFnSc3
v	v	k7c6
závěru	závěr	k1gInSc6
uťal	utít	k5eAaPmAgMnS
Ronaldo	Ronaldo	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-06-21	2012-06-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
2012	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
save-it-for-later	save-it-for-later	k1gMnSc1
approach	approach	k1gMnSc1
hurts	hurts	k6eAd1
Portugal	portugal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-06-27	2012-06-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ORNSTEIN	ORNSTEIN	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Cup	cup	k1gInSc1
2014	#num#	k4
<g/>
:	:	kIx,
Thomas	Thomas	k1gMnSc1
Muller	Muller	k1gMnSc1
outshines	outshines	k1gMnSc1
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-06-17	2014-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SHEEN	SHEEN	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
vs	vs	k?
Thomas	Thomas	k1gMnSc1
Muller	Muller	k1gMnSc1
<g/>
:	:	kIx,
How	How	k1gMnSc1
did	did	k?
Portugal	portugal	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
talisman	talisman	k1gInSc1
perform	perform	k1gInSc1
in	in	k?
comparison	comparison	k1gInSc1
to	ten	k3xDgNnSc1
Germany	German	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
hat-trick	hat-trick	k6eAd1
hero	hero	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-06-17	2014-06-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KRISHNAN	KRISHNAN	kA
<g/>
,	,	kIx,
Joe	Joe	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
World	World	k1gInSc1
Cup	cup	k1gInSc1
2014	#num#	k4
<g/>
:	:	kIx,
Portugal	portugal	k1gInSc1
are	ar	k1gInSc5
an	an	k?
'	'	kIx"
<g/>
average	average	k1gFnSc1
<g/>
'	'	kIx"
team	team	k1gInSc1
who	who	k?
could	could	k1gInSc1
never	never	k1gInSc1
win	win	k?
the	the	k?
World	World	k1gInSc1
Cup	cup	k1gInSc1
<g/>
,	,	kIx,
says	says	k6eAd1
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-06-23	2014-06-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
upstaged	upstaged	k1gInSc4
by	by	kYmCp3nS
Lionel	Lionel	k1gMnSc1
Messi	Messe	k1gFnSc3
once	oncat	k5eAaPmIp3nS
again	again	k2eAgInSc1d1
at	at	k?
World	World	k1gInSc1
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc4
Daily	Daila	k1gFnSc2
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-06-26	2014-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
EURO	euro	k1gNnSc1
2016	#num#	k4
<g/>
,	,	kIx,
SME	SME	k?
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Oslavy	oslava	k1gFnPc4
na	na	k7c6
domácej	domácet	k5eAaPmRp2nS,k5eAaImRp2nS
pôde	pôde	k1gInSc1
sa	sa	k?
nekonajú	nekonajú	k?
<g/>
,	,	kIx,
Portugalci	Portugalec	k1gMnPc1
majstrami	majstra	k1gFnPc7
Európy	Európ	k1gInPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Profutbal	Profutbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
sk	sk	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
BROWN	BROWN	kA
<g/>
,	,	kIx,
Luke	Luke	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
goal	goal	k1gInSc1
<g/>
,	,	kIx,
Portugal	portugal	k1gInSc1
vs	vs	k?
Morocco	Morocco	k1gMnSc1
World	World	k1gMnSc1
Cup	cup	k1gInSc4
2018	#num#	k4
<g/>
:	:	kIx,
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
forward	forwarda	k1gFnPc2
breaks	breaks	k1gInSc1
record	record	k1gMnSc1
with	with	k1gMnSc1
header	header	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-20	2018-06-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HENRY	Henry	k1gMnSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iran	Iran	k1gNnSc1
1	#num#	k4
–	–	k?
1	#num#	k4
Portugal	portugal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-25	2018-06-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ROSE	Ros	k1gMnSc2
<g/>
,	,	kIx,
Gary	Gara	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uruguay	Uruguay	k1gFnSc1
2	#num#	k4
–	–	k?
1	#num#	k4
Portugal	portugal	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-06-30	2018-06-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Fan	Fana	k1gFnPc2
Dream	Dream	k1gInSc4
Team	team	k1gInSc1
and	and	k?
prize	prize	k1gFnSc2
winners	winnersa	k1gFnPc2
revealed	revealed	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-23	2018-07-23	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SANDFORD	SANDFORD	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
scored	scored	k1gInSc1
in	in	k?
his	his	k1gNnSc1
10	#num#	k4
<g/>
th	th	k?
international	internationat	k5eAaPmAgInS,k5eAaImAgInS
tournament	tournament	k1gInSc1
for	forum	k1gNnPc2
Portugal	portugal	k1gInSc4
with	with	k1gInSc4
Nations	Nationsa	k1gFnPc2
League	Leagu	k1gInSc2
hat-trick	hat-trick	k6eAd1
against	against	k5eAaPmF
Switzerland	Switzerland	k1gInSc4
<g/>
.	.	kIx.
talkSPORT	talkSPORT	k?
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-06	2019-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TAYLOR	TAYLOR	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portugal	portugal	k1gInSc1
win	win	k?
Nations	Nations	k1gInSc1
League	League	k1gFnSc1
as	as	k9
Gonçalo	Gonçala	k1gFnSc5
Guedes	Guedes	k1gInSc1
does	doesa	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
Netherlands	Netherlandsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-06-09	2019-06-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
scores	scores	k1gMnSc1
four	four	k1gMnSc1
as	as	k9
Portugal	portugal	k1gInSc4
beat	beat	k1gInSc1
Lithuania	Lithuanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-09-10	2019-09-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Profil	profil	k1gInSc1
na	na	k7c6
Transfermarktu	Transfermarkt	k1gInSc6
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
↑	↑	k?
Cyrus	Cyrus	k1gMnSc1
C.	C.	kA
Malek	Malek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Fastest	Fastest	k1gMnSc1
In	In	k1gMnSc1
Football	Football	k1gMnSc1
-	-	kIx~
German	German	k1gMnSc1
Study	stud	k1gInPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goal	Goal	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-12-11	2009-12-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Smutek	smutek	k1gInSc1
po	po	k7c6
debaklu	debakl	k1gInSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kdepak	kdepak	k6eAd1
<g/>
,	,	kIx,
hráči	hráč	k1gMnPc1
Realu	Real	k1gInSc2
Madrid	Madrid	k1gInSc1
do	do	k7c2
rána	ráno	k1gNnSc2
oslavovali	oslavovat	k5eAaImAgMnP
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
20151	#num#	k4
2	#num#	k4
Ronaldo	Ronaldo	k1gNnSc1
secures	securesa	k1gFnPc2
PFA	PFA	kA
awards	awards	k6eAd1
double	double	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-04-27	2007-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ronaldo	Ronaldo	k1gNnSc1
named	named	k1gMnSc1
player	player	k1gMnSc1
of	of	k?
the	the	k?
year	year	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-04-27	2008-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Previous	Previous	k1gInSc1
Winners	Winners	k1gInSc1
–	–	k?
Footballer	Footballer	k1gInSc1
of	of	k?
the	the	k?
year	year	k1gInSc1
<g/>
.	.	kIx.
footballwriters	footballwriters	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FIFA	FIFA	kA
2008	#num#	k4
World	Worldo	k1gNnPc2
Player	Playra	k1gFnPc2
of	of	k?
the	the	k?
Year	Year	k1gInSc1
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soccerlens	Soccerlens	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-01-13	2009-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
wins	wins	k6eAd1
Ballon	Ballon	k1gInSc1
d	d	k?
<g/>
’	’	k?
<g/>
Or	Or	k1gFnSc1
for	forum	k1gNnPc2
the	the	k?
fourth	fourth	k1gMnSc1
time	timat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-12	2016-12-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
European	European	k1gMnSc1
Footballer	Footballer	k1gMnSc1
of	of	k?
the	the	k?
Year	Year	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Ballon	Ballon	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Or	Or	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rec	Rec	k1gFnSc1
<g/>
.	.	kIx.
<g/>
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
Soccer	Soccer	k1gInSc1
Statistics	Statistics	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
RSSSF	RSSSF	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-05	2019-12-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
José	Josá	k1gFnSc2
Luis	Luisa	k1gFnPc2
Pierrend	Pierrend	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FIFA	FIFA	kA
Awards	Awardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rec	Rec	k1gFnSc1
<g/>
.	.	kIx.
<g/>
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
Soccer	Soccer	k1gInSc1
Statistics	Statistics	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
RSSSF	RSSSF	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-01-28	2016-01-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Award	Award	k1gInSc1
winners	winners	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeana	k1gFnPc2
Sports	Sportsa	k1gFnPc2
Media	medium	k1gNnSc2
(	(	kIx(
<g/>
ESM	ESM	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Spain	Spain	k1gInSc1
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Topscorers	Topscorers	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Pichichi	Pichichi	k1gNnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rec	Rec	k1gFnSc1
<g/>
.	.	kIx.
<g/>
Sport	sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
Soccer	Soccer	k1gInSc1
Statistics	Statistics	k1gInSc1
Foundation	Foundation	k1gInSc1
(	(	kIx(
<g/>
RSSSF	RSSSF	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2020-09-29	2020-09-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
RIGG	RIGG	kA
<g/>
,	,	kIx,
Nicholas	Nicholas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atletico	Atletico	k1gNnSc1
Madrid	Madrid	k1gInSc1
snubbed	snubbed	k1gMnSc1
at	at	k?
La	la	k1gNnSc2
Liga	liga	k1gFnSc1
awards	awards	k1gInSc1
despite	despit	k1gInSc5
winning	winning	k1gInSc4
league	league	k1gFnSc3
ahead	ahead	k1gInSc1
of	of	k?
Spanish	Spanish	k1gInSc1
giants	giants	k1gInSc1
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
and	and	k?
Barcelona	Barcelona	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Independent	independent	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-10-28	2014-10-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ENTIDADES	ENTIDADES	kA
NACIONAIS	NACIONAIS	kA
AGRACIADAS	AGRACIADAS	kA
COM	COM	kA
ORDENS	ORDENS	kA
PORTUGUESAS	PORTUGUESAS	kA
-	-	kIx~
Página	Página	k1gMnSc1
Oficial	Oficial	k1gMnSc1
das	das	k?
Ordens	Ordens	k1gInSc1
Honoríficas	Honoríficas	k1gMnSc1
Portuguesas	Portuguesas	k1gMnSc1
<g/>
.	.	kIx.
www.ordens.presidencia.pt	www.ordens.presidencia.pt	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
Transfermarkt	Transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
www.madrista.cz	www.madrista.cz	k1gInSc1
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
Životopis	životopis	k1gInSc1
a	a	k8xC
profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Bilybalet	Bilybalet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
Topforward	Topforward	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Statistika	statistika	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
FIFA	FIFA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
national-football-teams	national-football-teams	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
</s>
<s>
Medailisté	medailista	k1gMnPc1
-	-	kIx~
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2004	#num#	k4
Řecko	Řecko	k1gNnSc4
</s>
<s>
Antonios	Antonios	k1gMnSc1
Nikopolidis	Nikopolidis	k1gFnSc2
•	•	k?
Kostas	Kostas	k1gMnSc1
Chalkias	Chalkias	k1gMnSc1
•	•	k?
Fanis	Fanis	k1gFnSc1
Katergiannakis	Katergiannakis	k1gFnSc2
•	•	k?
Giourkas	Giourkas	k1gMnSc1
Seitaridis	Seitaridis	k1gFnSc2
•	•	k?
Stylianos	Stylianos	k1gMnSc1
Venetidis	Venetidis	k1gFnSc2
•	•	k?
Nikos	Nikos	k1gMnSc1
Dabizas	Dabizas	k1gMnSc1
•	•	k?
Traianos	Traianos	k1gMnSc1
Dellas	Dellas	k1gMnSc1
•	•	k?
Angelos	Angelos	k1gInSc1
Basinas	Basinas	k1gInSc1
•	•	k?
Theodoros	Theodorosa	k1gFnPc2
Zagorakis	Zagorakis	k1gFnSc1
•	•	k?
Stelios	Stelios	k1gMnSc1
Giannakopoulos	Giannakopoulos	k1gMnSc1
•	•	k?
Angelos	Angelos	k1gMnSc1
Charisteas	Charisteas	k1gMnSc1
•	•	k?
Vassilios	Vassilios	k1gMnSc1
Tsiartas	Tsiartas	k1gMnSc1
•	•	k?
Demis	Demis	k1gInSc1
Nikolaidis	Nikolaidis	k1gInSc1
•	•	k?
Takis	Takis	k1gInSc1
Fyssas	Fyssas	k1gInSc1
•	•	k?
Zisis	Zisis	k1gInSc1
Vryzas	Vryzas	k1gInSc1
•	•	k?
Pantelis	Pantelis	k1gInSc1
Kafes	Kafes	k1gMnSc1
•	•	k?
Georgios	Georgios	k1gMnSc1
Georgiadis	Georgiadis	k1gFnSc2
•	•	k?
Giannis	Giannis	k1gInSc1
Goumas	Goumas	k1gInSc1
•	•	k?
Michalis	Michalis	k1gInSc1
Kapsis	Kapsis	k1gFnSc2
•	•	k?
Jorgos	Jorgos	k1gMnSc1
Karagunis	Karagunis	k1gFnSc2
•	•	k?
Konstantinos	Konstantinos	k1gMnSc1
Katsuranis	Katsuranis	k1gFnSc2
•	•	k?
Dimitrios	Dimitrios	k1gMnSc1
Papadopoulos	Papadopoulos	k1gMnSc1
•	•	k?
Vassilis	Vassilis	k1gFnSc1
Lakis	Lakis	k1gFnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Otto	Otto	k1gMnSc1
Rehhagel	Rehhagel	k1gMnSc1
Portugalsko	Portugalsko	k1gNnSc4
</s>
<s>
Ricardo	Ricardo	k1gNnSc1
Pereira	Pereir	k1gInSc2
•	•	k?
Paulo	Paula	k1gFnSc5
Ferreira	Ferreira	k1gMnSc1
•	•	k?
Rui	Rui	k1gMnSc1
Jorge	Jorg	k1gFnSc2
•	•	k?
Jorge	Jorg	k1gFnSc2
Andrade	Andrad	k1gInSc5
•	•	k?
Fernando	Fernanda	k1gFnSc5
Couto	Couto	k1gNnSc1
•	•	k?
Costinha	Costinha	k1gMnSc1
•	•	k?
Luís	Luís	k1gInSc1
Figo	Figo	k1gMnSc1
•	•	k?
Petit	petit	k1gInSc1
•	•	k?
Pauleta	Pauleta	k1gFnSc1
•	•	k?
Rui	Rui	k1gMnSc1
Costa	Costa	k1gMnSc1
•	•	k?
Simã	Simã	k1gMnSc1
Sabrosa	Sabrosa	k1gFnSc1
•	•	k?
Quim	Quim	k1gMnSc1
•	•	k?
Miguel	Miguel	k1gMnSc1
Monteiro	Monteiro	k1gNnSc1
•	•	k?
Nuno	Nuna	k1gMnSc5
Valente	Valent	k1gMnSc5
•	•	k?
Beto	Beto	k6eAd1
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Maniche	Manich	k1gInSc2
•	•	k?
Tiago	Tiago	k1gMnSc1
Mendes	Mendes	k1gMnSc1
•	•	k?
Deco	Deco	k1gMnSc1
•	•	k?
Nuno	Nuno	k1gMnSc1
Gomes	Gomes	k1gMnSc1
•	•	k?
José	Josá	k1gFnSc2
Moreira	Moreira	k1gMnSc1
•	•	k?
Hélder	Hélder	k1gMnSc1
Postiga	Postig	k1gMnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Luiz	Luiz	k1gMnSc1
Felipe	Felip	k1gInSc5
Scolari	Scolar	k1gFnSc2
Česko	Česko	k1gNnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Čech	Čech	k1gMnSc1
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Blažek	Blažek	k1gMnSc1
•	•	k?
Antonín	Antonín	k1gMnSc1
Kinský	Kinský	k1gMnSc1
•	•	k?
Zdeněk	Zdeněk	k1gMnSc1
Grygera	Grygero	k1gNnSc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Mareš	Mareš	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Galásek	Galásek	k1gMnSc1
•	•	k?
René	René	k1gMnSc1
Bolf	Bolf	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Jankulovski	Jankulovsk	k1gFnSc2
•	•	k?
Vladimír	Vladimír	k1gMnSc1
Šmicer	Šmicer	k1gMnSc1
•	•	k?
Karel	Karel	k1gMnSc1
Poborský	Poborský	k2eAgMnSc1d1
•	•	k?
Jan	Jan	k1gMnSc1
Koller	Koller	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Rosický	rosický	k2eAgMnSc1d1
•	•	k?
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
•	•	k?
Vratislav	Vratislav	k1gFnSc1
Lokvenc	Lokvenc	k1gFnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Jiránek	Jiránek	k1gMnSc1
•	•	k?
Štěpán	Štěpán	k1gMnSc1
Vachoušek	Vachoušek	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Hübschman	Hübschman	k1gMnSc1
•	•	k?
Marek	Marek	k1gMnSc1
Heinz	Heinz	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Týce	Týc	k1gFnSc2
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Plašil	plašit	k5eAaImAgMnS
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Ujfaluši	Ujfaluch	k1gMnPc1
•	•	k?
David	David	k1gMnSc1
Rozehnal	Rozehnal	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Brückner	Brückner	k1gMnSc1
Nizozemsko	Nizozemsko	k1gNnSc4
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnSc2
•	•	k?
Sander	Sandra	k1gFnPc2
Westerveld	Westerveld	k1gMnSc1
•	•	k?
Ronald	Ronald	k1gMnSc1
Waterreus	Waterreus	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Reiziger	Reiziger	k1gMnSc1
•	•	k?
Jaap	Jaap	k1gMnSc1
Stam	Stam	k1gMnSc1
•	•	k?
Wilfred	Wilfred	k1gMnSc1
Bouma	Bouma	k1gNnSc1
•	•	k?
Giovanni	Giovaneň	k1gFnSc3
van	vana	k1gFnPc2
Bronckhorst	Bronckhorst	k1gMnSc1
•	•	k?
Phillip	Phillip	k1gMnSc1
Cocu	Cocus	k1gInSc2
•	•	k?
Andy	Anda	k1gFnSc2
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Meijde	Meijd	k1gInSc5
•	•	k?
Edgar	Edgar	k1gMnSc1
Davids	Davidsa	k1gFnPc2
•	•	k?
Patrick	Patrick	k1gMnSc1
Kluivert	Kluivert	k1gMnSc1
•	•	k?
Ruud	Ruud	k1gInSc1
van	vana	k1gFnPc2
Nistelrooy	Nistelrooa	k1gFnSc2
•	•	k?
Rafael	Rafael	k1gMnSc1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Vaart	Vaart	k1gInSc1
•	•	k?
Roy	Roy	k1gMnSc1
Makaay	Makaaa	k1gFnSc2
•	•	k?
Wesley	Weslea	k1gFnSc2
Sneijder	Sneijder	k1gMnSc1
•	•	k?
Frank	Frank	k1gMnSc1
de	de	k?
Boer	Boer	k1gMnSc1
•	•	k?
Marc	Marc	k1gInSc1
Overmars	Overmars	k1gInSc1
•	•	k?
Pierre	Pierr	k1gInSc5
van	vana	k1gFnPc2
Hooijdonk	Hooijdonko	k1gNnPc2
•	•	k?
Johnny	Johnna	k1gMnSc2
Heitinga	Heiting	k1gMnSc2
•	•	k?
Arjen	Arjen	k1gInSc1
Robben	Robbna	k1gFnPc2
•	•	k?
Clarence	Clarenec	k1gInSc2
Seedorf	Seedorf	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Bosvelt	Bosvelt	k1gMnSc1
•	•	k?
Boudewijn	Boudewijn	k1gInSc1
Zenden	Zendna	k1gFnPc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Dick	Dick	k1gInSc1
Advocaat	Advocaat	k2eAgInSc1d1
</s>
<s>
Medailisté	medailista	k1gMnPc1
–	–	k?
Mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2012	#num#	k4
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillasa	k1gFnPc2
•	•	k?
Pepe	Pepe	k1gFnPc2
Reina	Rein	k2eAgNnSc2d1
•	•	k?
Raúl	Raúl	k1gInSc1
Albiol	Albiol	k1gInSc1
•	•	k?
Gerard	Gerard	k1gInSc1
Piqué	Piquá	k1gFnSc2
•	•	k?
Javi	Jav	k1gFnSc2
Martínez	Martínez	k1gMnSc1
•	•	k?
Juanfran	Juanfran	k1gInSc1
•	•	k?
Andrés	Andrés	k1gInSc1
Iniesta	Iniesta	k1gFnSc1
•	•	k?
Pedro	Pedro	k1gNnSc1
Rodríguez	Rodríguez	k1gMnSc1
Ledesma	Ledesm	k1gMnSc2
•	•	k?
Xavi	Xav	k1gFnSc2
•	•	k?
Fernando	Fernanda	k1gFnSc5
Torres	Torres	k1gInSc4
•	•	k?
Francesc	Francesc	k1gInSc1
Fà	Fà	k1gInSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Negredo	Negredo	k1gNnSc4
•	•	k?
Víctor	Víctor	k1gInSc1
Valdés	Valdés	k1gInSc1
•	•	k?
Juan	Juan	k1gMnSc1
Manuel	Manuel	k1gMnSc1
Mata	mást	k5eAaImSgInS
•	•	k?
Xabi	Xab	k1gMnPc1
Alonso	Alonsa	k1gFnSc5
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Sergio	Sergio	k6eAd1
Busquets	Busquets	k1gInSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloum	k1gNnPc1
•	•	k?
Jordi	Jord	k1gMnPc1
Alba	album	k1gNnSc2
•	•	k?
Fernando	Fernanda	k1gFnSc5
Llorente	Llorent	k1gInSc5
•	•	k?
Santi	Santi	k1gNnSc1
Cazorla	Cazorla	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Silva	Silva	k1gFnSc1
•	•	k?
Jesús	Jesús	k1gInSc1
Navas	Navas	k1gInSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Vicente	Vicent	k1gInSc5
del	del	k?
Bosque	Bosqu	k1gMnPc4
Itálie	Itálie	k1gFnSc2
</s>
<s>
Gianluigi	Gianluigi	k6eAd1
Buffon	Buffon	k1gInSc1
•	•	k?
Morgan	morgan	k1gInSc1
De	De	k?
Sanctis	Sanctis	k1gFnSc2
•	•	k?
Christian	Christian	k1gMnSc1
Maggio	Maggio	k1gMnSc1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
•	•	k?
Angelo	Angela	k1gFnSc5
Ogbonna	Ogbonno	k1gNnSc2
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Federico	Federico	k1gMnSc1
Balzaretti	Balzaretť	k1gFnSc2
•	•	k?
Ignazio	Ignazia	k1gMnSc5
Abate	Abat	k1gMnSc5
•	•	k?
Claudio	Claudio	k1gMnSc1
Marchisio	Marchisio	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Balotelli	Balotell	k1gMnSc3
•	•	k?
Antonio	Antonio	k1gMnSc1
Cassano	Cassana	k1gFnSc5
•	•	k?
Antonio	Antonio	k1gMnSc1
Di	Di	k1gMnSc1
Natale	Natal	k1gInSc5
•	•	k?
Salvatore	Salvator	k1gMnSc5
Sirigu	Sirig	k1gMnSc5
•	•	k?
Emanuele	Emanuela	k1gFnSc3
Giaccherini	Giaccherin	k2eAgMnPc1d1
•	•	k?
Andrea	Andrea	k1gFnSc1
Barzagli	Barzagle	k1gFnSc4
•	•	k?
Daniele	Daniela	k1gFnSc3
De	De	k?
Rossi	Rosse	k1gFnSc3
•	•	k?
Fabio	Fabio	k1gMnSc1
Borini	Borin	k1gMnPc1
•	•	k?
Riccardo	Riccarda	k1gFnSc5
Montolivo	Montolivo	k1gNnSc1
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnSc3
•	•	k?
Sebastian	Sebastian	k1gMnSc1
Giovinco	Giovinco	k1gMnSc1
•	•	k?
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc1
•	•	k?
Alessandro	Alessandra	k1gFnSc5
Diamanti	Diamant	k1gMnPc1
•	•	k?
Antonio	Antonio	k1gMnSc5
Nocerino	Nocerina	k1gMnSc5
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Cesare	Cesar	k1gMnSc5
Prandelli	Prandell	k1gMnSc5
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Eduardo	Eduardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
Beto	Beto	k1gMnSc1
•	•	k?
Bruno	Bruno	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
Pepe	Pep	k1gFnSc2
•	•	k?
Miguel	Miguel	k1gMnSc1
Veloso	Velosa	k1gFnSc5
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Custódio	Custódio	k1gMnSc1
Castro	Castro	k1gNnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Joã	Joã	k1gMnSc1
Moutinho	Moutin	k1gMnSc2
•	•	k?
Hugo	Hugo	k1gMnSc1
Almeida	Almeid	k1gMnSc2
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Quaresma	Quaresmum	k1gNnSc2
•	•	k?
Nelson	Nelson	k1gMnSc1
Oliveira	Oliveira	k1gMnSc1
•	•	k?
Rui	Rui	k1gMnSc1
Patrício	Patrício	k1gMnSc1
•	•	k?
Ricardo	Ricardo	k1gNnSc4
Costa	Cost	k1gInSc2
•	•	k?
Rolando	Rolanda	k1gFnSc5
•	•	k?
Rúben	Rúben	k2eAgMnSc1d1
Micael	Micael	k1gMnSc1
•	•	k?
Raul	Raul	k1gMnSc1
Meireles	Meireles	k1gMnSc1
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Silvestre	Silvestr	k1gInSc5
Varela	Varel	k1gMnSc4
•	•	k?
Miguel	Miguel	k1gMnSc1
Lopes	Lopes	k1gMnSc1
•	•	k?
Hugo	Hugo	k1gMnSc1
Viana	Vian	k1gInSc2
•	•	k?
Joã	Joã	k1gMnSc1
Pereira	Pereira	k1gMnSc1
•	•	k?
Hélder	Hélder	k1gMnSc1
Postiga	Postig	k1gMnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Paulo	Paula	k1gFnSc5
Bento	Bento	k1gNnSc4
Německo	Německo	k1gNnSc1
</s>
<s>
Manuel	Manuel	k1gMnSc1
Neuer	Neuer	k1gMnSc1
•	•	k?
Tim	Tim	k?
Wiese	Wiese	k1gFnSc2
•	•	k?
Ron-Robert	Ron-Robert	k1gMnSc1
Zieler	Zieler	k1gMnSc1
•	•	k?
İ	İ	k1gFnSc2
Gündoğ	Gündoğ	k1gMnSc1
•	•	k?
Marcel	Marcel	k1gMnSc1
Schmelzer	Schmelzer	k1gMnSc1
•	•	k?
Benedikt	Benedikt	k1gMnSc1
Höwedes	Höwedes	k1gMnSc1
•	•	k?
Mats	Mats	k1gInSc1
Hummels	Hummels	k1gInSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Bastian	Bastian	k1gMnSc1
Schweinsteiger	Schweinsteiger	k1gMnSc1
•	•	k?
Mesut	Mesut	k1gMnSc1
Özil	Özil	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Schürrle	Schürrle	k1gFnSc2
•	•	k?
Lukas	Lukas	k1gMnSc1
Podolski	Podolsk	k1gFnSc2
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Klose	Klose	k1gFnSc2
•	•	k?
Thomas	Thomas	k1gMnSc1
Müller	Müller	k1gMnSc1
•	•	k?
Holger	Holger	k1gMnSc1
Badstuber	Badstuber	k1gMnSc1
•	•	k?
Lars	Lars	k1gInSc1
Bender	Bender	k1gMnSc1
•	•	k?
Philipp	Philipp	k1gMnSc1
Lahm	Lahm	k1gMnSc1
•	•	k?
Per	pero	k1gNnPc2
Mertesacker	Mertesackero	k1gNnPc2
•	•	k?
Toni	Toni	k1gFnSc2
Kroos	Kroos	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Götze	Götze	k1gFnSc2
•	•	k?
Jérôme	Jérôm	k1gInSc5
Boateng	Boateng	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Reus	Reusa	k1gFnPc2
•	•	k?
Mario	Mario	k1gMnSc1
Gómez	Gómez	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Joachim	Joachim	k1gMnSc1
Löw	Löw	k1gMnSc1
</s>
<s>
Medailisté	medailista	k1gMnPc1
–	–	k?
Mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2016	#num#	k4
Portugalsko	Portugalsko	k1gNnSc1
</s>
<s>
Rui	Rui	k?
Patrício	Patrício	k6eAd1
•	•	k?
Anthony	Anthon	k1gInPc1
Lopes	Lopes	k1gInSc1
•	•	k?
Eduardo	Eduardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
Bruno	Bruno	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
Pepe	Pep	k1gFnSc2
•	•	k?
José	Josá	k1gFnSc2
Fonte	font	k1gInSc5
•	•	k?
Raphaël	Raphaël	k1gMnSc1
Guerreiro	Guerreiro	k1gNnSc1
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
Eliseu	Eliseus	k1gInSc2
•	•	k?
Cédric	Cédric	k1gMnSc1
Soares	Soares	k1gMnSc1
•	•	k?
Vieirinha	Vieirinha	k1gMnSc1
•	•	k?
Joã	Joã	k1gMnSc1
Moutinho	Moutin	k1gMnSc2
•	•	k?
Joã	Joã	k1gMnSc1
Mário	Mário	k1gMnSc1
•	•	k?
Danilo	danit	k5eAaImAgNnS
Pereira	Pereir	k1gMnSc4
•	•	k?
William	William	k1gInSc4
Carvalho	Carval	k1gMnSc2
•	•	k?
André	André	k1gMnSc1
Gomes	Gomes	k1gMnSc1
•	•	k?
Renato	Renata	k1gFnSc5
Sanches	Sanchesa	k1gFnPc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Silva	Silva	k1gFnSc1
•	•	k?
Rafa	Raf	k2eAgFnSc1d1
Silva	Silva	k1gFnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
–	–	k?
•	•	k?
Éder	Éder	k1gInSc1
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Quaresma	Quaresm	k1gMnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Fernando	Fernanda	k1gFnSc5
Santos	Santosa	k1gFnPc2
Francie	Francie	k1gFnSc2
</s>
<s>
Hugo	Hugo	k1gMnSc1
Lloris	Lloris	k1gFnSc2
–	–	k?
•	•	k?
Christophe	Christophe	k1gInSc1
Jallet	Jallet	k1gInSc1
•	•	k?
Patrice	patrice	k1gFnSc2
Evra	Evra	k1gMnSc1
•	•	k?
Adil	Adil	k1gMnSc1
Rami	Ram	k1gFnSc2
•	•	k?
N	N	kA
<g/>
'	'	kIx"
<g/>
Golo	Golo	k6eAd1
Kanté	Kantý	k2eAgFnSc2d1
•	•	k?
Yohan	Yohan	k1gMnSc1
Cabaye	Cabay	k1gFnSc2
•	•	k?
Antoine	Antoin	k1gInSc5
Griezmann	Griezmanno	k1gNnPc2
•	•	k?
Dimitri	Dimitr	k1gFnSc2
Payet	Payet	k1gMnSc1
•	•	k?
Olivier	Olivier	k1gMnSc1
Giroud	Giroud	k1gMnSc1
•	•	k?
André-Pierre	André-Pierr	k1gInSc5
Gignac	Gignac	k1gInSc4
•	•	k?
Anthony	Anthona	k1gFnSc2
Martial	Martial	k1gMnSc1
•	•	k?
Morgan	morgan	k1gMnSc1
Schneiderlin	Schneiderlin	k2eAgMnSc1d1
•	•	k?
Eliaquim	Eliaquim	k1gMnSc1
Mangala	Mangal	k1gMnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Paul	Paul	k1gMnSc1
Pogba	Pogba	k1gMnSc1
•	•	k?
Steve	Steve	k1gMnSc1
Mandanda	Mandando	k1gNnSc2
•	•	k?
Lucas	Lucas	k1gMnSc1
Digne	Dign	k1gInSc5
•	•	k?
Moussa	Moussa	k1gFnSc1
Sissoko	Sissoko	k1gNnSc1
•	•	k?
Bacary	Bacara	k1gFnSc2
Sagna	Sagn	k1gMnSc2
•	•	k?
Kingsley	Kingslea	k1gFnSc2
Coman	Coman	k1gMnSc1
•	•	k?
Laurent	Laurent	k1gMnSc1
Koscielny	Koscielna	k1gFnSc2
•	•	k?
Samuel	Samuel	k1gMnSc1
Umtiti	Umtit	k1gMnPc1
•	•	k?
Benoît	Benoît	k2eAgMnSc1d1
Costil	Costil	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Didier	Didier	k1gMnSc1
Deschamps	Deschampsa	k1gFnPc2
Wales	Wales	k1gInSc1
</s>
<s>
Wayne	Waynout	k5eAaImIp3nS,k5eAaPmIp3nS
Hennessey	Hennessea	k1gFnPc4
•	•	k?
Chris	Chris	k1gFnSc2
Gunter	Gunter	k1gMnSc1
•	•	k?
Neil	Neil	k1gMnSc1
Taylor	Taylor	k1gMnSc1
•	•	k?
Ben	Ben	k1gInSc1
Davies	Davies	k1gMnSc1
•	•	k?
James	James	k1gMnSc1
Chester	Chester	k1gMnSc1
•	•	k?
Ashley	Ashley	k1gInPc1
Williams	Williams	k1gInSc1
–	–	k?
•	•	k?
Joe	Joe	k1gMnSc1
Allen	Allen	k1gMnSc1
•	•	k?
Andy	Anda	k1gFnSc2
King	King	k1gMnSc1
•	•	k?
Hal	halo	k1gNnPc2
Robson-Kanu	Robson-Kan	k1gInSc2
•	•	k?
Aaron	Aaron	k1gMnSc1
Ramsey	Ramsea	k1gFnSc2
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Owain	Owain	k2eAgMnSc1d1
Fôn	Fôn	k1gMnSc1
Williams	Williamsa	k1gFnPc2
•	•	k?
George	Georg	k1gInSc2
Christopher	Christophra	k1gFnPc2
Williams	Williamsa	k1gFnPc2
•	•	k?
David	David	k1gMnSc1
Edwards	Edwards	k1gInSc1
•	•	k?
Jazz	jazz	k1gInSc1
Richards	Richards	k1gInSc1
•	•	k?
Joe	Joe	k1gFnSc1
Ledley	Ledlea	k1gFnSc2
•	•	k?
David	David	k1gMnSc1
Cotterill	Cotterill	k1gMnSc1
•	•	k?
Sam	Sam	k1gMnSc1
Vokes	Vokes	k1gMnSc1
•	•	k?
James	James	k1gMnSc1
Collins	Collinsa	k1gFnPc2
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Peter	Peter	k1gMnSc1
Williams	Williamsa	k1gFnPc2
•	•	k?
Danny	Danna	k1gFnSc2
Ward	Ward	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Vaughan	Vaughan	k1gMnSc1
•	•	k?
Simon	Simon	k1gMnSc1
Church	Church	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Chris	Chris	k1gFnSc1
Coleman	Coleman	k1gMnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
Manuel	Manuel	k1gMnSc1
Neuer	Neuer	k1gMnSc1
•	•	k?
Shkodran	Shkodran	k1gInSc1
Mustafi	Mustaf	k1gFnSc2
•	•	k?
Jonas	Jonas	k1gMnSc1
Hector	Hector	k1gMnSc1
•	•	k?
Benedikt	Benedikt	k1gMnSc1
Höwedes	Höwedes	k1gMnSc1
•	•	k?
Mats	Mats	k1gInSc1
Hummels	Hummels	k1gInSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Bastian	Bastian	k1gMnSc1
Schweinsteiger	Schweinsteiger	k1gMnSc1
–	–	k?
•	•	k?
Mesut	Mesut	k2eAgMnSc1d1
Özil	Özil	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Schürrle	Schürrle	k1gFnSc2
•	•	k?
Lukas	Lukas	k1gMnSc1
Podolski	Podolsk	k1gFnSc2
•	•	k?
Julian	Julian	k1gMnSc1
Draxler	Draxler	k1gMnSc1
•	•	k?
Bernd	Bernd	k1gMnSc1
Leno	Lena	k1gFnSc5
•	•	k?
Thomas	Thomas	k1gMnSc1
Müller	Müller	k1gMnSc1
•	•	k?
Emre	Emre	k1gInSc1
Can	Can	k1gMnSc1
•	•	k?
Julian	Julian	k1gMnSc1
Weigl	Weigl	k1gMnSc1
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Tah	tah	k1gInSc1
•	•	k?
Jérôme	Jérôm	k1gInSc5
Boateng	Boateng	k1gMnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Götze	Götze	k1gFnSc2
•	•	k?
Leroy	Leroa	k1gFnSc2
Sané	Saná	k1gFnSc2
•	•	k?
Joshua	Joshua	k1gMnSc1
Kimmich	Kimmich	k1gMnSc1
•	•	k?
Marc-André	Marc-Andrý	k2eAgNnSc1d1
ter	ter	k?
Stegen	Stegen	k1gInSc1
•	•	k?
Mario	Mario	k1gMnSc1
Gómez	Gómez	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Joachim	Joachim	k1gMnSc1
Löw	Löw	k1gMnSc1
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
Evropy	Evropa	k1gFnSc2
2016	#num#	k4
</s>
<s>
1	#num#	k4
Rui	Rui	k1gMnSc1
Patrício	Patrício	k1gMnSc1
•	•	k?
2	#num#	k4
Bruno	Bruno	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
3	#num#	k4
Pepe	Pepe	k1gFnSc1
•	•	k?
4	#num#	k4
José	José	k1gNnSc2
Fonte	font	k1gInSc5
•	•	k?
5	#num#	k4
Raphaël	Raphaëla	k1gFnPc2
Guerreiro	Guerreiro	k1gNnSc1
•	•	k?
6	#num#	k4
Ricardo	Ricardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
7	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
•	•	k?
8	#num#	k4
Joã	Joã	k6eAd1
Moutinho	Moutin	k1gMnSc2
•	•	k?
9	#num#	k4
Éderzito	Éderzita	k1gFnSc5
António	António	k1gMnSc1
Macedo	Macedo	k1gNnSc1
Lopes	Lopes	k1gMnSc1
•	•	k?
10	#num#	k4
Joao	Joao	k6eAd1
Mário	Mário	k6eAd1
•	•	k?
11	#num#	k4
Vieirinha	Vieirinha	k1gFnSc1
•	•	k?
12	#num#	k4
Anthony	Anthona	k1gFnSc2
Lopes	Lopesa	k1gFnPc2
•	•	k?
13	#num#	k4
Danilo	danit	k5eAaImAgNnS
Pereira	Pereir	k1gMnSc4
•	•	k?
14	#num#	k4
William	William	k1gInSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
15	#num#	k4
André	André	k1gMnPc1
Gomes	Gomesa	k1gFnPc2
•	•	k?
16	#num#	k4
Renato	Renata	k1gFnSc5
Sanches	Sanches	k1gInSc4
•	•	k?
17	#num#	k4
Nani	Nan	k1gFnSc2
•	•	k?
18	#num#	k4
Rafa	Raf	k2eAgFnSc1d1
Silva	Silva	k1gFnSc1
•	•	k?
19	#num#	k4
Eliseu	Elise	k2eAgFnSc4d1
•	•	k?
20	#num#	k4
Ricardo	Ricardo	k1gNnSc4
Quaresma	Quaresmum	k1gNnSc2
•	•	k?
21	#num#	k4
Cédric	Cédrice	k1gInPc2
Soares	Soares	k1gInSc4
•	•	k?
22	#num#	k4
Eduardo	Eduardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
23	#num#	k4
Adrien	Adriena	k1gFnPc2
Silva	Silva	k1gFnSc1
•	•	k?
Trenér	trenér	k1gMnSc1
Fernando	Fernanda	k1gFnSc5
Santos	Santos	k1gInSc1
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
2018	#num#	k4
</s>
<s>
1	#num#	k4
Rui	Rui	k1gMnSc1
Patrício	Patrício	k1gMnSc1
•	•	k?
12	#num#	k4
Anthony	Anthona	k1gFnSc2
Lopes	Lopesa	k1gFnPc2
•	•	k?
22	#num#	k4
Beto	Beto	k1gNnSc4
•	•	k?
2	#num#	k4
Bruno	Bruno	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
3	#num#	k4
Pepe	Pepe	k1gFnSc1
•	•	k?
5	#num#	k4
Raphaël	Raphaëla	k1gFnPc2
Guerreiro	Guerreiro	k1gNnSc1
•	•	k?
6	#num#	k4
José	José	k1gNnSc2
Fonte	font	k1gInSc5
•	•	k?
13	#num#	k4
Rubén	Rubén	k1gInSc1
Dias	Dias	k1gInSc4
•	•	k?
15	#num#	k4
Ricardo	Ricardo	k1gNnSc4
Pereira	Pereiro	k1gNnSc2
•	•	k?
19	#num#	k4
Mario	Mario	k1gMnSc1
Rui	Rui	k1gMnSc1
•	•	k?
21	#num#	k4
Soares	Soaresa	k1gFnPc2
Cédric	Cédrice	k1gFnPc2
•	•	k?
4	#num#	k4
Manuel	Manuela	k1gFnPc2
Fernandes	Fernandesa	k1gFnPc2
•	•	k?
8	#num#	k4
Joã	Joã	k6eAd1
Moutinho	Moutin	k1gMnSc2
•	•	k?
10	#num#	k4
Joao	Joao	k6eAd1
Mário	Mário	k6eAd1
•	•	k?
11	#num#	k4
Bernardo	Bernardo	k1gNnSc4
Silva	Silva	k1gFnSc1
•	•	k?
14	#num#	k4
William	William	k1gInSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
16	#num#	k4
Bruno	Bruno	k1gMnSc1
Fernandes	Fernandes	k1gMnSc1
•	•	k?
23	#num#	k4
Adrien	Adriena	k1gFnPc2
Silva	Silva	k1gFnSc1
•	•	k?
7	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
-	-	kIx~
•	•	k?
9	#num#	k4
André	André	k1gMnSc1
Silva	Silva	k1gFnSc1
•	•	k?
17	#num#	k4
Goncalo	Goncalo	k1gFnSc2
Guedes	Guedesa	k1gFnPc2
•	•	k?
18	#num#	k4
Gelson	Gelson	k1gInSc1
Martins	Martins	k1gInSc4
•	•	k?
20	#num#	k4
Ricardo	Ricardo	k1gNnSc4
Quaresma	Quaresmum	k1gNnSc2
•	•	k?
Trenér	trenér	k1gMnSc1
Fernando	Fernanda	k1gFnSc5
Santos	Santos	k1gInSc1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gMnSc3
Hráči	hráč	k1gMnPc1
</s>
<s>
Wes	Wes	k?
Brown	Brown	k1gMnSc1
•	•	k?
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
–	–	k?
•	•	k?
Nemanja	Nemanja	k1gMnSc1
Vidić	Vidić	k1gMnSc1
•	•	k?
Patrice	patrice	k1gFnSc2
Evra	Evra	k1gMnSc1
•	•	k?
Owen	Owen	k1gMnSc1
Hargreaves	Hargreaves	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Carrick	Carrick	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
•	•	k?
Wayne	Wayn	k1gInSc5
Rooney	Rooneum	k1gNnPc7
•	•	k?
Carlos	Carlos	k1gMnSc1
Tévez	Tévez	k1gMnSc1
•	•	k?
Tomasz	Tomasz	k1gMnSc1
Kuszczak	Kuszczak	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Shea	Shea	k1gMnSc1
•	•	k?
Mikaël	Mikaël	k1gMnSc1
Silvestre	Silvestr	k1gInSc5
•	•	k?
Anderson	Anderson	k1gMnSc1
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Darren	Darrna	k1gFnPc2
Fletcher	Fletchra	k1gFnPc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Sir	sir	k1gMnSc1
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
–	–	k?
•	•	k?
Diego	Diego	k1gMnSc1
López	López	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Raphaël	Raphaël	k1gMnSc1
Varane	varan	k1gMnSc5
•	•	k?
Fábio	Fábio	k6eAd1
Coentrã	Coentrã	k6eAd1
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Karim	Karim	k1gInSc1
Benzema	Benzema	k1gFnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Pepe	Pep	k1gInSc2
•	•	k?
Marcelo	Marcela	k1gFnSc5
•	•	k?
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloa	k1gMnSc1
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Asier	Asier	k1gMnSc1
Illarramendi	Illarramend	k1gMnPc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
–	–	k?
•	•	k?
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Pepe	Pepe	k1gInSc1
•	•	k?
Marcelo	Marcela	k1gFnSc5
•	•	k?
Casemiro	Casemiro	k1gNnSc4
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Karim	Karim	k1gInSc1
Benzema	Benzema	k1gFnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Nacho	Nac	k1gMnSc2
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
James	James	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Jesé	Jesá	k1gFnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
–	–	k?
•	•	k?
Raphaël	Raphaël	k1gInSc1
Varane	varan	k1gMnSc5
•	•	k?
Marcelo	Marcela	k1gFnSc5
–	–	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Casemiro	Casemiro	k1gNnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
•	•	k?
Karim	Karim	k1gMnSc1
Benzema	Benzemum	k1gNnSc2
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Nacho	Nac	k1gMnSc2
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Pepe	Pepe	k1gNnSc1
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
•	•	k?
James	James	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
•	•	k?
Mariano	Mariana	k1gFnSc5
Hlavní	hlavní	k2eAgFnSc7d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Marcelo	Marcela	k1gFnSc5
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
–	–	k?
•	•	k?
Raphaël	Raphaël	k1gInSc1
Varane	varan	k1gMnSc5
•	•	k?
Nacho	Nac	k1gMnSc2
•	•	k?
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Theo	Thea	k1gFnSc5
Hernández	Hernández	k1gMnSc1
•	•	k?
Achraf	Achraf	k1gMnSc1
Hakimi	Haki	k1gFnPc7
•	•	k?
Jesús	Jesús	k1gInSc1
Vallejo	Vallejo	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
•	•	k?
Casemiro	Casemiro	k1gNnSc4
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
•	•	k?
Dani	daň	k1gFnSc3
Ceballos	Ceballos	k1gMnSc1
•	•	k?
Marcos	Marcos	k1gMnSc1
Llorente	Llorent	k1gInSc5
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
•	•	k?
Karim	Karim	k1gMnSc1
Benzema	Benzemum	k1gNnSc2
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Borja	Borja	k1gMnSc1
Mayoral	Mayoral	k1gFnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
2008	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnSc2
•	•	k?
Tomasz	Tomasz	k1gMnSc1
Kuszczak	Kuszczak	k1gMnSc1
•	•	k?
Ben	Ben	k1gInSc1
Amos	Amos	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Gary	Gar	k2eAgFnPc1d1
Neville	Neville	k1gFnPc1
•	•	k?
Patrice	patrice	k1gFnSc2
Evra	Evra	k1gMnSc1
•	•	k?
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Anderson	Anderson	k1gMnSc1
•	•	k?
Dimitar	Dimitar	k1gInSc1
Berbatov	Berbatov	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
Rooney	Rooney	k1gInPc1
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Pak	pak	k6eAd1
Či-song	Či-song	k1gMnSc1
•	•	k?
Nemanja	Nemanja	k1gMnSc1
Vidić	Vidić	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Carrick	Carrick	k1gMnSc1
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
•	•	k?
Danny	Danna	k1gFnSc2
Welbeck	Welbeck	k1gMnSc1
•	•	k?
Rafael	Rafael	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Shea	Shea	k1gMnSc1
•	•	k?
Jonny	Jonna	k1gFnSc2
Evans	Evansa	k1gFnPc2
•	•	k?
Darren	Darrna	k1gFnPc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
Darron	Darron	k1gMnSc1
Gibson	Gibson	k1gMnSc1
•	•	k?
Carlos	Carlos	k1gMnSc1
Tevez	Tevez	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
2014	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
•	•	k?
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Fernando	Fernanda	k1gFnSc5
Pacheco	Pacheco	k1gNnSc5
Hráči	hráč	k1gMnPc1
</s>
<s>
Raphaël	Raphaëlit	k5eAaPmRp2nS
Varane	varan	k1gMnSc5
•	•	k?
Pepe	Pep	k1gFnSc2
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedir	k1gMnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Karim	Karim	k1gMnSc1
Benzema	Benzemum	k1gNnSc2
•	•	k?
James	James	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Marcelo	Marcela	k1gFnSc5
•	•	k?
Javier	Javier	k1gMnSc1
Hernández	Hernández	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloum	k1gNnPc1
•	•	k?
Nacho	Nac	k1gMnSc2
•	•	k?
Jesé	Jesá	k1gFnSc2
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Asier	Asier	k1gMnSc1
Illarramendi	Illarramend	k1gMnPc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Medrán	Medrán	k2eAgMnSc1d1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
2016	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
•	•	k?
Rubén	Rubén	k1gInSc1
Yáñ	Yáñ	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Pepe	Pep	k1gFnSc2
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Raphaël	Raphaël	k1gMnSc1
Varane	varan	k1gMnSc5
•	•	k?
Nacho	Nac	k1gMnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Karim	Karim	k1gMnSc1
Benzema	Benzemum	k1gNnSc2
•	•	k?
James	James	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Marcelo	Marcela	k1gFnSc5
•	•	k?
Casemiro	Casemiro	k1gNnSc4
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
•	•	k?
Mariano	Mariana	k1gFnSc5
Díaz	Díaz	k1gInSc1
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Danilo	danit	k5eAaImAgNnS
Hlavní	hlavní	k2eAgNnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
2017	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
•	•	k?
Moha	moct	k5eAaImSgInS
Ramos	Ramos	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Raphaël	Raphaël	k1gMnSc1
Varane	varan	k1gMnSc5
•	•	k?
Nacho	Nac	k1gMnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Karim	Karim	k1gMnSc1
Benzema	Benzema	k1gNnSc1
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Marcelo	Marcela	k1gFnSc5
•	•	k?
Casemiro	Casemiro	k1gNnSc4
•	•	k?
Theo	Thea	k1gFnSc5
Hernández	Hernández	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
•	•	k?
Marcos	Marcos	k1gMnSc1
Llorente	Llorent	k1gInSc5
•	•	k?
Achraf	Achraf	k1gInSc1
Hakimi	Haki	k1gFnPc7
•	•	k?
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
•	•	k?
Borja	Borja	k1gMnSc1
Mayoral	Mayoral	k1gMnSc1
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
•	•	k?
Dani	daň	k1gFnSc3
Ceballos	Ceballos	k1gInSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
2014	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
•	•	k?
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Pepe	Pep	k1gFnSc2
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
James	James	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
Karim	Karim	k1gInSc1
Benzema	Benzema	k1gFnSc1
•	•	k?
Raphaël	Raphaël	k1gInSc1
Varane	varan	k1gMnSc5
•	•	k?
Marcelo	Marcela	k1gFnSc5
Vieira	Vieir	k1gMnSc2
•	•	k?
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloa	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Asier	Asier	k1gMnSc1
Illarramendi	Illarramend	k1gMnPc1
Hlavní	hlavní	k2eAgFnSc7d1
trenér	trenér	k1gMnSc1
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
</s>
<s>
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
2017	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Raphaël	Raphaël	k1gMnSc1
Varane	varan	k1gMnSc5
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Marcelo	Marcela	k1gFnSc5
Vieira	Vieir	k1gMnSc2
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Casemiro	Casemiro	k1gNnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Karim	Karim	k1gInSc4
Benzema	Benzem	k1gMnSc2
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Nacho	Nac	k1gMnSc4
Fernández	Fernández	k1gInSc1
•	•	k?
Theo	Thea	k1gFnSc5
Hernández	Hernández	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
•	•	k?
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
–	–	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Szczęsny	Szczęsna	k1gFnSc2
•	•	k?
Mattia	Mattia	k1gFnSc1
Perin	Perina	k1gFnPc2
•	•	k?
Carlo	Carlo	k1gNnSc4
Pinsoglio	Pinsoglio	k6eAd1
Hráči	hráč	k1gMnPc1
</s>
<s>
Alex	Alex	k1gMnSc1
Sandro	Sandra	k1gFnSc5
•	•	k?
Andrea	Andrea	k1gFnSc1
Barzagli	Barzagle	k1gFnSc4
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnSc3
•	•	k?
Martín	Martín	k1gMnSc1
Cáceres	Cáceres	k1gMnSc1
•	•	k?
Joã	Joã	k1gMnSc1
Cancelo	Cancelo	k1gMnSc1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
•	•	k?
Mattia	Mattium	k1gNnSc2
De	De	k?
Sciglio	Sciglio	k1gMnSc1
•	•	k?
Paolo	Paolo	k1gNnSc4
Gozzi	Gozze	k1gFnSc3
•	•	k?
Daniele	Daniela	k1gFnSc3
Rugani	Rugaň	k1gFnSc3
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Spinazzola	Spinazzola	k1gFnSc1
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Bentancur	Bentancur	k1gMnSc1
•	•	k?
Emre	Emre	k1gInSc1
Can	Can	k1gMnSc1
•	•	k?
Grigoris	Grigoris	k1gInSc1
Kastanos	Kastanos	k1gInSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedir	k1gMnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Hans	Hans	k1gMnSc1
Nicolussi	Nicoluss	k1gMnSc3
•	•	k?
Matheus	Matheus	k1gMnSc1
Pereira	Pereira	k1gMnSc1
•	•	k?
Miralem	Miral	k1gMnSc7
Pjanić	Pjanić	k1gMnSc7
•	•	k?
Manolo	Manola	k1gFnSc5
Portanova	Portanův	k2eAgInSc2d1
•	•	k?
Federico	Federico	k1gNnSc1
Bernardeschi	Bernardesch	k1gFnSc2
•	•	k?
Douglas	Douglas	k1gMnSc1
Costa	Costa	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Cuadrado	Cuadrada	k1gFnSc5
•	•	k?
Paulo	Paula	k1gFnSc5
Dybala	Dybal	k1gMnSc2
•	•	k?
Moise	Moise	k1gFnSc2
Kean	Kean	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Mandžukić	Mandžukić	k1gMnSc1
•	•	k?
Stephy	Stepha	k1gFnSc2
Mavididi	Mavidid	k1gMnPc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Massimiliano	Massimiliana	k1gFnSc5
Allegri	Allegr	k1gFnSc5
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
–	–	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Szczęsny	Szczęsna	k1gFnSc2
•	•	k?
Gianluigi	Gianluigi	k1gNnPc2
Buffon	Buffon	k1gInSc1
•	•	k?
Carlo	Carla	k1gMnSc5
Pinsoglio	Pinsoglia	k1gMnSc5
Hráči	hráč	k1gMnSc5
</s>
<s>
Mattia	Mattia	k1gFnSc1
De	De	k?
Sciglio	Sciglio	k6eAd1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
•	•	k?
Matthijs	Matthijsa	k1gFnPc2
de	de	k?
Ligt	Ligt	k1gMnSc1
•	•	k?
Alex	Alex	k1gMnSc1
Sandro	Sandra	k1gFnSc5
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnPc1
•	•	k?
Daniele	Daniela	k1gFnSc3
Rugani	Rugaň	k1gFnSc3
•	•	k?
Merih	Merih	k1gInSc1
Demiral	Demiral	k1gFnSc1
•	•	k?
Miralem	Miral	k1gInSc7
Pjanić	Pjanić	k1gMnSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Aaron	Aaron	k1gMnSc1
Ramsey	Ramsea	k1gFnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Bentancur	Bentancur	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
•	•	k?
Paulo	Paula	k1gFnSc5
Dybala	Dybal	k1gMnSc4
•	•	k?
Douglas	Douglas	k1gMnSc1
Costa	Costa	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Cuadrado	Cuadrada	k1gFnSc5
•	•	k?
Gonzalo	Gonzalo	k1gMnSc1
Higuaín	Higuaín	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Bernardeschi	Bernardesch	k1gFnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Massimiliano	Massimiliana	k1gFnSc5
Allegri	Allegr	k1gFnSc5
</s>
<s>
Supercoppa	Supercoppa	k1gFnSc1
italiana	italiana	k1gFnSc1
2018	#num#	k4
–	–	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Szczęsny	Szczęsna	k1gFnSc2
•	•	k?
Carlo	Carlo	k1gNnSc1
Pinsoglio	Pinsoglio	k1gMnSc1
•	•	k?
Mattia	Mattia	k1gFnSc1
Perin	Perin	k1gMnSc1
•	•	k?
Mattia	Mattius	k1gMnSc4
Del	Del	k1gMnSc4
Favero	Favero	k1gNnSc4
Hráči	hráč	k1gMnPc1
</s>
<s>
Joã	Joã	k1gMnSc1
Cancelo	Cancelo	k1gMnSc1
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnPc1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
•	•	k?
Alex	Alex	k1gMnSc1
Sandro	Sandra	k1gFnSc5
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Bentancur	Bentancur	k1gMnSc1
•	•	k?
Miralem	Miral	k1gMnSc7
Pjanić	Pjanić	k1gFnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Paulo	Paula	k1gFnSc5
Dybala	Dybal	k1gMnSc4
•	•	k?
Douglas	Douglas	k1gMnSc1
Costa	Costa	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
Mattia	Mattium	k1gNnSc2
De	De	k?
Sciglio	Sciglio	k1gMnSc1
•	•	k?
Daniele	Daniela	k1gFnSc3
Rugani	Rugaň	k1gFnSc3
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Spinazzola	Spinazzola	k1gFnSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khediro	k1gNnPc1
•	•	k?
Emre	Emr	k1gMnSc2
Can	Can	k1gMnSc2
•	•	k?
Moise	Moise	k1gFnSc2
Kean	Kean	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Bernardeschi	Bernardesch	k1gFnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Massimiliano	Massimiliana	k1gFnSc5
Allegri	Allegr	k1gFnSc5
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnSc2
•	•	k?
Tomasz	Tomasz	k1gMnSc1
Kuszczak	Kuszczak	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Rio	Rio	k?
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Shea	She	k2eAgMnSc4d1
•	•	k?
Nemanja	Nemanjus	k1gMnSc4
Vidić	Vidić	k1gFnSc2
•	•	k?
Patrice	patrice	k1gFnSc2
Evra	Evrus	k1gMnSc2
•	•	k?
Gary	Gara	k1gMnSc2
Neville	Nevill	k1gMnSc2
•	•	k?
Wes	Wes	k1gMnSc1
Brown	Brown	k1gMnSc1
•	•	k?
Gabriel	Gabriel	k1gMnSc1
Heinze	Heinze	k1gFnSc2
•	•	k?
Mikaël	Mikaël	k1gMnSc1
Silvestre	Silvestr	k1gInSc5
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Carrick	Carrick	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Darren	Darrna	k1gFnPc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
Kieran	Kieran	k1gInSc1
Richardson	Richardson	k1gInSc1
•	•	k?
Pak	pak	k6eAd1
Či-song	Či-song	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
Rooney	Rooneum	k1gNnPc7
•	•	k?
Louis	Louis	k1gMnSc1
Saha	Saha	k1gMnSc1
•	•	k?
Ole	Ola	k1gFnSc3
Gunnar	Gunnar	k1gMnSc1
Solskjæ	Solskjæ	k1gMnSc1
•	•	k?
Alan	Alan	k1gMnSc1
Smith	Smith	k1gMnSc1
•	•	k?
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnSc2
•	•	k?
Tomasz	Tomasz	k1gMnSc1
Kuszczak	Kuszczak	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Wes	Wes	k?
Brown	Brown	k1gMnSc1
•	•	k?
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Patrice	patrice	k1gFnSc2
Evra	Evra	k1gMnSc1
•	•	k?
Nemanja	Nemanja	k1gMnSc1
Vidić	Vidić	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Shea	Shea	k1gMnSc1
•	•	k?
Gerard	Gerard	k1gMnSc1
Piqué	Piquá	k1gFnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Michael	Michael	k1gMnSc1
Carrick	Carrick	k1gMnSc1
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
•	•	k?
Anderson	Anderson	k1gMnSc1
•	•	k?
Owen	Owen	k1gMnSc1
Hargreaves	Hargreaves	k1gMnSc1
•	•	k?
Darren	Darrno	k1gNnPc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
Pak	pak	k8xC
Či-song	Či-song	k1gMnSc1
•	•	k?
Carlos	Carlos	k1gMnSc1
Tévez	Tévez	k1gMnSc1
•	•	k?
Wayne	Wayn	k1gInSc5
Rooney	Rooneum	k1gNnPc7
•	•	k?
Louis	Louis	k1gMnSc1
Saha	Saha	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Premier	Premier	k1gInSc1
League	League	k1gNnSc1
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnPc2
Hráči	hráč	k1gMnPc1
</s>
<s>
Nemanja	Nemanja	k1gMnSc1
Vidić	Vidić	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Shea	She	k2eAgFnSc1d1
•	•	k?
Patrice	patrice	k1gFnSc1
Evra	Evr	k1gInSc2
•	•	k?
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Jonny	Jonna	k1gFnSc2
Evans	Evansa	k1gFnPc2
•	•	k?
Rafael	Rafael	k1gMnSc1
•	•	k?
Gary	Gara	k1gFnSc2
Neville	Neville	k1gFnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Carrick	Carrick	k1gMnSc1
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Darren	Darrna	k1gFnPc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
Pak	pak	k8xC
Či-song	Či-song	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
•	•	k?
Anderson	Anderson	k1gMnSc1
<g/>
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Dimitar	Dimitar	k1gInSc1
Berbatov	Berbatov	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
Rooney	Rooneum	k1gNnPc7
•	•	k?
Carlos	Carlos	k1gMnSc1
Tévez	Tévez	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Community	Communit	k1gInPc1
Shield	Shield	k1gInSc1
2003	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Tim	Tim	k?
Howard	Howard	k1gMnSc1
•	•	k?
Roy	Roy	k1gMnSc1
Carroll	Carroll	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Phil	Phil	k1gMnSc1
Neville	Neville	k1gFnSc2
•	•	k?
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Mikaël	Mikaël	k1gMnSc1
Silvestre	Silvestr	k1gInSc5
•	•	k?
Quinton	Quinton	k1gInSc1
Fortune	Fortun	k1gInSc5
•	•	k?
Ole	Ola	k1gFnSc3
Gunnar	Gunnar	k1gMnSc1
Solskjæ	Solskjæ	k1gMnSc1
•	•	k?
Roy	Roy	k1gMnSc1
Keane	Kean	k1gInSc5
•	•	k?
Nicky	nicka	k1gFnSc2
Butt	Butt	k1gMnSc1
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Paul	Paul	k1gMnSc1
Scholes	Scholes	k1gMnSc1
•	•	k?
Ruud	Ruud	k1gInSc1
van	vana	k1gFnPc2
Nistelrooy	Nistelrooa	k1gFnSc2
•	•	k?
John	John	k1gMnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Shea	She	k2eAgFnSc1d1
•	•	k?
Eric	Eric	k1gFnSc1
Djemba-Djemba	Djemba-Djemba	k1gFnSc1
•	•	k?
Kieran	Kieran	k1gInSc1
Richardson	Richardson	k1gInSc1
•	•	k?
Darren	Darrna	k1gFnPc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
David	David	k1gMnSc1
Bellion	Bellion	k1gInSc1
•	•	k?
Diego	Diego	k6eAd1
Forlán	Forlán	k2eAgMnSc1d1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Community	Communit	k1gInPc1
Shield	Shield	k1gInSc1
2007	#num#	k4
–	–	k?
Manchester	Manchester	k1gInSc1
United	United	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Edwin	Edwin	k2eAgInSc1d1
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Sar	Sar	k1gFnSc2
•	•	k?
Tomasz	Tomasz	k1gMnSc1
Kuszczak	Kuszczak	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Wes	Wes	k?
Brown	Brown	k1gMnSc1
•	•	k?
Rio	Rio	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
•	•	k?
Nemanja	Nemanja	k1gMnSc1
Vidić	Vidić	k1gMnSc1
•	•	k?
Mikaël	Mikaël	k1gMnSc1
Silvestre	Silvestr	k1gInSc5
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
John	John	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Shea	Shea	k1gMnSc1
•	•	k?
Michael	Michael	k1gMnSc1
Carrick	Carrick	k1gMnSc1
•	•	k?
Patrice	patrice	k1gFnSc2
Evra	Evra	k1gMnSc1
•	•	k?
Ryan	Ryan	k1gInSc1
Giggs	Giggs	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
Rooney	Rooneum	k1gNnPc7
•	•	k?
Gerard	Gerard	k1gMnSc1
Piqué	Piquá	k1gFnSc2
•	•	k?
Phil	Phil	k1gMnSc1
Bardsley	Bardslea	k1gFnSc2
•	•	k?
Nani	Nan	k1gFnSc2
•	•	k?
Darren	Darrna	k1gFnPc2
Fletcher	Fletchra	k1gFnPc2
•	•	k?
Lee	Lea	k1gFnSc3
Martin	Martin	k1gMnSc1
•	•	k?
Chris	Chris	k1gFnSc2
Eagles	Eaglesa	k1gFnPc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Alex	Alex	k1gMnSc1
Ferguson	Ferguson	k1gMnSc1
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
•	•	k?
Antonio	Antonio	k1gMnSc1
Adán	Adán	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Marcelo	Marcela	k1gFnSc5
Vieira	Vieiro	k1gNnPc1
•	•	k?
Pepe	Pep	k1gMnSc2
•	•	k?
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloa	k1gMnSc1
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Raúl	Raúl	k1gInSc1
Albiol	Albiol	k1gInSc1
•	•	k?
Raphaël	Raphaël	k1gInSc1
Varane	varan	k1gMnSc5
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
Xabi	Xab	k1gFnSc2
Alonso	Alonsa	k1gFnSc5
•	•	k?
Mesut	Mesut	k2eAgMnSc1d1
Özil	Özil	k1gMnSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gFnSc1
•	•	k?
Kaká	kakat	k5eAaImIp3nS
•	•	k?
José	José	k1gNnSc1
Callejón	Callejón	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Esteban	Esteban	k1gMnSc1
Granero	Granero	k1gNnSc1
•	•	k?
Lassana	Lassan	k1gMnSc2
Diarra	Diarr	k1gMnSc2
•	•	k?
Hamit	Hamit	k2eAgInSc4d1
Altı	Altı	k1gInSc4
•	•	k?
Nuri	Nur	k1gFnSc2
Şahin	Şahin	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Gonzalo	Gonzalo	k1gMnSc1
Higuaín	Higuaín	k1gMnSc1
•	•	k?
Karim	Karim	k1gMnSc1
Benzema	Benzema	k1gNnSc1
•	•	k?
Jesé	Jesé	k1gNnPc2
Rodríguez	Rodríguez	k1gInSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
José	José	k1gNnSc1
Mourinho	Mourin	k1gMnSc2
</s>
<s>
Primera	primera	k1gFnSc1
División	División	k1gInSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Keylor	Keylor	k1gMnSc1
Navas	Navas	k1gMnSc1
•	•	k?
Kiko	Kiko	k1gMnSc1
Casilla	Casilla	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Marcelo	Marcela	k1gFnSc5
Vieira	Vieira	k1gMnSc1
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Nacho	Nac	k1gMnSc4
Fernández	Fernández	k1gMnSc1
•	•	k?
Raphaël	Raphaël	k1gMnSc1
Varane	varan	k1gMnSc5
•	•	k?
Daniel	Daniel	k1gMnSc1
Carvajal	Carvajal	k1gMnSc1
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Pepe	Pepe	k1gNnSc1
•	•	k?
Fábio	Fábio	k1gMnSc1
Coentrã	Coentrã	k1gMnSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Tejero	Tejero	k1gNnSc1
•	•	k?
Isco	Isco	k1gMnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Mateo	Mateo	k1gMnSc1
Kovačić	Kovačić	k1gMnSc1
•	•	k?
Casemiro	Casemiro	k1gNnSc4
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Marco	Marco	k1gMnSc1
Asensio	Asensio	k1gMnSc1
•	•	k?
James	James	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Vázquez	Vázquez	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
Karim	Karim	k1gInSc1
Benzema	Benzema	k1gFnSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
•	•	k?
Gareth	Gareth	k1gMnSc1
Bale	bal	k1gInSc5
•	•	k?
Mariano	Mariana	k1gFnSc5
Hlavní	hlavní	k2eAgFnSc7d1
trenér	trenér	k1gMnSc1
</s>
<s>
Zinédine	Zinédin	k1gMnSc5
Zidane	Zidan	k1gMnSc5
</s>
<s>
Copa	Copa	k1gFnSc1
del	del	k?
Rey	Rea	k1gFnSc2
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloa	k1gMnSc1
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Carvalho	Carval	k1gMnSc2
•	•	k?
Marcelo	Marcela	k1gFnSc5
Vieira	Vieir	k1gMnSc2
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khediro	k1gNnPc1
•	•	k?
Pepe	Pep	k1gMnSc2
•	•	k?
Xabi	Xab	k1gFnSc2
Alonso	Alonsa	k1gFnSc5
•	•	k?
Mesut	Mesut	k2eAgMnSc1d1
Özil	Özil	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Emmanuel	Emmanuel	k1gMnSc1
Adebayor	Adebayor	k1gMnSc1
•	•	k?
Esteban	Esteban	k1gMnSc1
Granero	Granero	k1gNnSc1
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Garay	Garaa	k1gFnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
José	José	k1gNnSc1
Mourinho	Mourin	k1gMnSc2
</s>
<s>
Supercopa	Supercopa	k1gFnSc1
de	de	k?
Españ	Españ	k1gFnSc1
2012	#num#	k4
–	–	k?
Real	Real	k1gInSc1
Madrid	Madrid	k1gInSc1
Brankář	brankář	k1gMnSc1
</s>
<s>
Iker	Iker	k1gMnSc1
Casillas	Casillas	k1gMnSc1
•	•	k?
Antonio	Antonio	k1gMnSc1
Adán	Adán	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Álvaro	Álvara	k1gFnSc5
Arbeloa	Arbeloum	k1gNnPc1
•	•	k?
Pepe	Pep	k1gMnSc2
•	•	k?
Sergio	Sergio	k1gMnSc1
Ramos	Ramos	k1gMnSc1
•	•	k?
Marcelo	Marcela	k1gFnSc5
Vieira	Vieir	k1gMnSc2
•	•	k?
Xabi	Xab	k1gFnSc2
Alonso	Alonsa	k1gFnSc5
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
Mesut	Mesut	k2eAgMnSc1d1
Özil	Özil	k1gMnSc1
•	•	k?
Gonzalo	Gonzalo	k1gMnSc1
Higuaín	Higuaín	k1gMnSc1
•	•	k?
Raúl	Raúl	k1gInSc1
Albiol	Albiol	k1gInSc1
•	•	k?
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gMnSc2
•	•	k?
Lassana	Lassan	k1gMnSc2
Diarra	Diarr	k1gMnSc2
•	•	k?
Nacho	Nac	k1gMnSc2
Fernández	Fernández	k1gInSc1
•	•	k?
José	Josá	k1gFnSc2
Callejón	Callejón	k1gMnSc1
•	•	k?
Karim	Karim	k1gInSc1
Benzema	Benzemum	k1gNnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
José	José	k1gNnSc1
Mourinho	Mourin	k1gMnSc2
</s>
<s>
Supercoppa	Supercoppa	k1gFnSc1
italiana	italiana	k1gFnSc1
2020	#num#	k4
–	–	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Szczęsny	Szczęsna	k1gFnSc2
•	•	k?
Carlo	Carlo	k1gNnSc1
Pinsoglio	Pinsoglio	k1gMnSc1
•	•	k?
Gianluigi	Gianluigi	k1gNnPc2
Buffon	Buffon	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Juan	Juan	k1gMnSc1
Cuadrado	Cuadrada	k1gFnSc5
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnPc1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
(	(	kIx(
<g/>
c	c	k0
<g/>
)	)	kIx)
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Weston	Weston	k1gInSc4
McKennie	McKennie	k1gFnSc2
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Bentancur	Bentancur	k1gMnSc1
•	•	k?
Arthur	Arthur	k1gMnSc1
Melo	Melo	k?
•	•	k?
Federico	Federico	k6eAd1
Chiesa	Chiesa	k1gFnSc1
•	•	k?
Dejan	Dejan	k1gInSc1
Kulusevski	Kulusevsk	k1gFnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
•	•	k?
Alessandro	Alessandra	k1gFnSc5
Di	Di	k1gMnPc7
Pardo	Pardo	k1gNnSc1
•	•	k?
Radu	rad	k1gInSc2
Drăguș	Drăguș	k1gMnSc1
•	•	k?
Gianluca	Gianluca	k1gMnSc1
Frabotta	Frabotta	k1gMnSc1
•	•	k?
Aaron	Aaron	k1gMnSc1
Ramsey	Ramsea	k1gFnSc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Bernardeschi	Bernardesch	k1gFnSc2
•	•	k?
Nicolò	Nicolò	k1gFnSc2
Fagioli	Fagiole	k1gFnSc3
•	•	k?
Filippo	Filippa	k1gFnSc5
Ranocchia	Ranocchium	k1gNnPc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc1
</s>
<s>
Držitelé	držitel	k1gMnPc1
ocenění	ocenění	k1gNnSc2
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
</s>
<s>
1956	#num#	k4
Stanley	Stanlea	k1gFnSc2
Matthews	Matthewsa	k1gFnPc2
•	•	k?
1957	#num#	k4
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
•	•	k?
1958	#num#	k4
Raymond	Raymonda	k1gFnPc2
Kopa	kopa	k1gFnSc1
•	•	k?
1959	#num#	k4
Alfredo	Alfredo	k1gNnSc4
Di	Di	k1gFnSc2
Stéfano	Stéfana	k1gFnSc5
•	•	k?
1960	#num#	k4
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
•	•	k?
1961	#num#	k4
Omar	Omar	k1gInSc1
Sívori	Sívor	k1gFnSc2
•	•	k?
1962	#num#	k4
Josef	Josef	k1gMnSc1
Masopust	Masopust	k1gMnSc1
•	•	k?
1963	#num#	k4
Lev	lev	k1gInSc1
Jašin	Jašin	k1gInSc4
•	•	k?
1964	#num#	k4
Denis	Denisa	k1gFnPc2
Law	Law	k1gFnPc2
•	•	k?
1965	#num#	k4
Eusébio	Eusébio	k1gNnSc4
•	•	k?
1966	#num#	k4
Bobby	Bobba	k1gFnSc2
Charlton	Charlton	k1gInSc1
•	•	k?
1967	#num#	k4
Flórián	Flórián	k1gMnSc1
Albert	Albert	k1gMnSc1
•	•	k?
1968	#num#	k4
George	Georg	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Best	Best	k1gInSc1
•	•	k?
1969	#num#	k4
Gianni	Gianeň	k1gFnSc6
Rivera	Rivera	k1gFnSc1
•	•	k?
1970	#num#	k4
Gerd	Gerd	k1gInSc1
Müller	Müller	k1gInSc4
•	•	k?
1971	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1972	#num#	k4
Franz	Franz	k1gInSc1
Beckenbauer	Beckenbauer	k1gInSc4
•	•	k?
1973	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1974	#num#	k4
Johan	Johana	k1gFnPc2
Cruijff	Cruijff	k1gInSc4
•	•	k?
1975	#num#	k4
Oleg	Oleg	k1gMnSc1
Blochin	Blochin	k1gMnSc1
•	•	k?
1976	#num#	k4
Franz	Franz	k1gInSc1
Beckenbauer	Beckenbauer	k1gInSc4
•	•	k?
1977	#num#	k4
Allan	Allan	k1gMnSc1
Simonsen	Simonsen	k1gInSc1
•	•	k?
1978	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Keegan	Keegan	k1gInSc4
•	•	k?
1979	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Keegan	Keegan	k1gInSc4
•	•	k?
1980	#num#	k4
Karl-Heinz	Karl-Heinz	k1gInSc1
Rummenigge	Rummenigg	k1gFnSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1981	#num#	k4
Karl-Heinz	Karl-Heinza	k1gFnPc2
Rummenigge	Rummenigge	k1gFnPc2
•	•	k?
1982	#num#	k4
Paolo	Paolo	k1gNnSc4
Rossi	Rosse	k1gFnSc3
•	•	k?
1983	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1984	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1985	#num#	k4
Michel	Michlo	k1gNnPc2
Platini	Platin	k2eAgMnPc1d1
•	•	k?
1986	#num#	k4
Ihor	Ihor	k1gInSc1
Belanov	Belanov	k1gInSc4
•	•	k?
1987	#num#	k4
Ruud	Ruud	k1gInSc1
Gullit	Gullit	k1gInSc4
•	•	k?
1988	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1989	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1990	#num#	k4
Lothar	Lothar	k1gMnSc1
Matthäus	Matthäus	k1gMnSc1
•	•	k?
1991	#num#	k4
Jean-Pierre	Jean-Pierr	k1gInSc5
Papin	Papin	k1gMnSc1
•	•	k?
1992	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1993	#num#	k4
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gNnSc4
•	•	k?
1994	#num#	k4
Christo	Christa	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc4
•	•	k?
1995	#num#	k4
George	George	k1gInSc1
Weah	Weah	k1gInSc4
•	•	k?
1996	#num#	k4
Matthias	Matthias	k1gInSc1
Sammer	Sammer	k1gInSc4
•	•	k?
1997	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
1999	#num#	k4
Rivaldo	Rivaldo	k1gNnSc4
•	•	k?
2000	#num#	k4
Luís	Luísa	k1gFnPc2
Figo	Figo	k6eAd1
•	•	k?
2001	#num#	k4
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
•	•	k?
2002	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2003	#num#	k4
Pavel	Pavel	k1gMnSc1
Nedvěd	Nedvěd	k1gMnSc1
•	•	k?
2004	#num#	k4
Andrij	Andrij	k1gFnSc1
Ševčenko	Ševčenka	k1gFnSc5
•	•	k?
2005	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2006	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Fabio	Fabio	k1gNnSc1
Cannavaro	Cannavara	k1gFnSc5
•	•	k?
2007	#num#	k4
Kaká	kakat	k5eAaImIp3nS
•	•	k?
2008	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2010	#num#	k4
•	•	k?
2011	#num#	k4
•	•	k?
2012	#num#	k4
•	•	k?
2013	#num#	k4
•	•	k?
2014	#num#	k4
•	•	k?
2015	#num#	k4
•	•	k?
2016	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
Luka	luka	k1gNnPc1
Modrić	Modrić	k1gFnSc2
•	•	k?
2019	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2020	#num#	k4
neuděleno	udělen	k2eNgNnSc1d1
V	v	k7c6
letech	let	k1gInPc6
2010	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
sloučeno	sloučit	k5eAaPmNgNnS
do	do	k7c2
ankety	anketa	k1gFnSc2
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFAUmístění	FIFAUmístění	k1gNnSc2
českých	český	k2eAgMnPc2d1
fotbalistů	fotbalista	k1gMnPc2
v	v	k7c6
anketě	anketa	k1gFnSc6
Zlatý	zlatý	k2eAgInSc4d1
míč	míč	k1gInSc4
</s>
<s>
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
FIFA	FIFA	kA
<g/>
)	)	kIx)
podle	podle	k7c2
roků	rok	k1gInPc2
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
FIFA	FIFA	kA
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
Lothar	Lothar	k1gMnSc1
Matthäus	Matthäus	k1gMnSc1
•	•	k?
1992	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1993	#num#	k4
Roberto	Roberta	k1gFnSc5
Baggio	Baggio	k1gNnSc4
•	•	k?
1994	#num#	k4
Romário	Romário	k1gNnSc4
•	•	k?
1995	#num#	k4
George	George	k1gInSc1
Weah	Weah	k1gInSc4
•	•	k?
1996	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1997	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
1999	#num#	k4
Rivaldo	Rivaldo	k1gNnSc4
•	•	k?
2000	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
2001	#num#	k4
Luís	Luísa	k1gFnPc2
Figo	Figo	k6eAd1
•	•	k?
2002	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2003	#num#	k4
Zinédine	Zinédin	k1gInSc5
Zidane	Zidan	k1gMnSc5
•	•	k?
2004	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2005	#num#	k4
Ronaldinho	Ronaldin	k1gMnSc4
•	•	k?
2006	#num#	k4
Fabio	Fabio	k6eAd1
Cannavaro	Cannavara	k1gFnSc5
•	•	k?
2007	#num#	k4
Kaká	kakat	k5eAaImIp3nS
•	•	k?
2008	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc4
Zlatý	zlatý	k2eAgInSc1d1
míč	míč	k1gInSc1
FIFA	FIFA	kA
</s>
<s>
2010	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
Nejlepší	dobrý	k2eAgMnSc1d3
hráč	hráč	k1gMnSc1
FIFA	FIFA	kA
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Luka	luka	k1gNnPc4
Modrić	Modrić	k1gFnSc2
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Lionel	Lionel	k1gInSc4
Messi	Messe	k1gFnSc4
•	•	k?
2020	#num#	k4
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Lewandowski	Lewandowsk	k1gFnSc2
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
kopačka	kopačka	k1gFnSc1
</s>
<s>
1968	#num#	k4
Eusébio	Eusébio	k6eAd1
•	•	k?
1969	#num#	k4
Petar	Petar	k1gInSc1
Žekov	Žekov	k1gInSc4
•	•	k?
1970	#num#	k4
Gerd	Gerd	k1gInSc1
Müller	Müller	k1gInSc4
•	•	k?
1971	#num#	k4
Josip	Josip	k1gInSc1
Skoblar	Skoblar	k1gInSc4
•	•	k?
1972	#num#	k4
Gerd	Gerd	k1gInSc1
Müller	Müller	k1gInSc4
•	•	k?
1973	#num#	k4
Eusébio	Eusébio	k1gNnSc4
•	•	k?
1974	#num#	k4
Héctor	Héctor	k1gInSc1
Yazalde	Yazald	k1gInSc5
•	•	k?
1975	#num#	k4
Dudu	Duda	k1gMnSc4
Georgescu	Georgesca	k1gMnSc4
•	•	k?
1976	#num#	k4
Sotiris	Sotiris	k1gInSc1
Kaiafas	Kaiafas	k1gInSc4
•	•	k?
1977	#num#	k4
Dudu	Duda	k1gMnSc4
Georgescu	Georgesca	k1gMnSc4
•	•	k?
1978	#num#	k4
Hans	hansa	k1gFnPc2
Krankl	Krankl	k1gFnPc2
•	•	k?
1979	#num#	k4
Kees	Kees	k1gInSc1
Kist	Kist	k1gInSc4
•	•	k?
1980	#num#	k4
Erwin	Erwin	k1gMnSc1
Vandenbergh	Vandenbergh	k1gMnSc1
•	•	k?
1981	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Georgi	Georgi	k1gNnSc7
Slavkov	Slavkov	k1gInSc1
•	•	k?
1982	#num#	k4
Wim	Wim	k1gFnSc2
Kieft	Kiefta	k1gFnPc2
•	•	k?
1983	#num#	k4
Fernando	Fernanda	k1gFnSc5
Gomes	Gomes	k1gInSc4
•	•	k?
1984	#num#	k4
Ian	Ian	k1gFnSc2
Rush	Rusha	k1gFnPc2
•	•	k?
1985	#num#	k4
Fernando	Fernanda	k1gFnSc5
Gomes	Gomes	k1gInSc4
•	•	k?
1986	#num#	k4
Marco	Marco	k1gNnSc1
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
•	•	k?
1987	#num#	k4
Rodion	Rodion	k1gInSc1
Cămătaru	Cămătar	k1gInSc2
<g/>
/	/	kIx~
<g/>
Toni	Toni	k1gFnSc1
Polster	Polster	k1gInSc1
•	•	k?
1988	#num#	k4
Tanju	Tanju	k1gFnPc2
Çolak	Çolak	k1gInSc4
•	•	k?
1989	#num#	k4
Dorin	Dorin	k1gInSc1
Mateuț	Mateuț	k1gFnSc2
•	•	k?
1990	#num#	k4
Hugo	Hugo	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
a	a	k8xC
Christo	Christa	k1gMnSc5
Stoičkov	Stoičkov	k1gInSc4
•	•	k?
1991	#num#	k4
Darko	Darko	k1gNnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
Pančev	Pančev	k1gFnSc1
•	•	k?
1997	#num#	k4
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
Nikos	Nikos	k1gInSc1
Machlas	Machlas	k1gInSc4
•	•	k?
1999	#num#	k4
Mário	Mário	k6eAd1
Jardel	Jardlo	k1gNnPc2
•	•	k?
2000	#num#	k4
Kevin	Kevin	k2eAgInSc4d1
Phillips	Phillips	k1gInSc4
•	•	k?
2001	#num#	k4
Henrik	Henrik	k1gMnSc1
Larsson	Larsson	k1gMnSc1
•	•	k?
2002	#num#	k4
Mário	Mário	k6eAd1
Jardel	Jardlo	k1gNnPc2
•	•	k?
2003	#num#	k4
Roy	Roy	k1gMnSc1
Makaay	Makaaa	k1gFnSc2
•	•	k?
2004	#num#	k4
Thierry	Thierra	k1gFnSc2
Henry	henry	k1gInSc2
•	•	k?
2005	#num#	k4
Diego	Diego	k6eAd1
Forlán	Forlán	k2eAgInSc1d1
a	a	k8xC
Thierry	Thierra	k1gFnPc1
Henry	henry	k1gInSc1
•	•	k?
2006	#num#	k4
Luca	Luc	k2eAgFnSc1d1
Toni	Toni	k1gFnSc1
•	•	k?
2007	#num#	k4
Francesco	Francesco	k1gNnSc4
Totti	Totť	k1gFnSc2
•	•	k?
2008	#num#	k4
Cristiano	Cristiana	k1gFnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
Diego	Diego	k6eAd1
Forlán	Forlán	k2eAgInSc1d1
•	•	k?
2010	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2011	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2012	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2013	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2014	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc1
/	/	kIx~
Luis	Luisa	k1gFnPc2
Suárez	Suárez	k1gInSc1
•	•	k?
2015	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2016	#num#	k4
Luis	Luisa	k1gFnPc2
Suárez	Suáreza	k1gFnPc2
•	•	k?
2017	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2018	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2019	#num#	k4
Lionel	Lionela	k1gFnPc2
Messi	Messe	k1gFnSc4
•	•	k?
2020	#num#	k4
Ciro	Ciro	k6eAd1
Immobile	Immobil	k1gInSc5
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
Premier	Premira	k1gFnPc2
League	League	k1gNnSc2
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
:	:	kIx,
Teddy	Tedda	k1gFnSc2
Sheringham	Sheringham	k1gInSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
:	:	kIx,
Andrew	Andrew	k1gFnSc6
Cole	cola	k1gFnSc6
(	(	kIx(
<g/>
34	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
:	:	kIx,
Alan	Alan	k1gMnSc1
Shearer	Shearer	k1gMnSc1
(	(	kIx(
<g/>
34	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Alan	Alan	k1gMnSc1
Shearer	Shearer	k1gMnSc1
(	(	kIx(
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
:	:	kIx,
Alan	Alan	k1gMnSc1
Shearer	Shearer	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
:	:	kIx,
Dion	Diona	k1gFnPc2
Dublin	Dublin	k1gInSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
a	a	k8xC
Chris	Chris	k1gFnSc1
Sutton	Sutton	k1gInSc1
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Jimmy	Jimma	k1gFnSc2
Floyd	Floyd	k1gMnSc1
Hasselbaink	Hasselbaink	k1gInSc1
a	a	k8xC
Michael	Michael	k1gMnSc1
Owen	Owen	k1gMnSc1
a	a	k8xC
Dwight	Dwight	k1gMnSc1
Yorke	York	k1gFnSc2
(	(	kIx(
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
:	:	kIx,
Kevin	Kevin	k2eAgInSc1d1
Phillips	Phillips	k1gInSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
:	:	kIx,
Jimmy	Jimma	k1gFnSc2
Floyd	Floyd	k1gInSc1
Hasselbaink	Hasselbaink	k1gInSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	Henry	k1gMnSc1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
:	:	kIx,
Ruud	Ruud	k1gInSc1
van	vana	k1gFnPc2
Nistelrooy	Nistelrooa	k1gFnSc2
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	henry	k1gInSc2
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	Henry	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	Henry	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
:	:	kIx,
Didier	Didier	k1gMnSc1
Drogba	Drogba	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
(	(	kIx(
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
:	:	kIx,
Nicolas	Nicolas	k1gMnSc1
Anelka	Anelka	k1gMnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
:	:	kIx,
Didier	Didier	k1gMnSc1
Drogba	Drogba	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
:	:	kIx,
Dimitar	Dimitara	k1gFnPc2
Berbatov	Berbatov	k1gInSc1
a	a	k8xC
Carlos	Carlos	k1gMnSc1
Tévez	Tévez	k1gMnSc1
(	(	kIx(
<g/>
20	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
:	:	kIx,
Robin	robin	k2eAgInSc1d1
van	van	k1gInSc1
Persie	Persie	k1gFnSc2
(	(	kIx(
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
<g/>
:	:	kIx,
Robin	robin	k2eAgInSc1d1
van	van	k1gInSc1
Persie	Persie	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Suárez	Suárez	k1gInSc1
(	(	kIx(
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
<g/>
:	:	kIx,
Sergio	Sergio	k6eAd1
Agüero	Agüero	k1gNnSc1
(	(	kIx(
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
:	:	kIx,
Harry	Harra	k1gFnSc2
Kane	kanout	k5eAaImIp3nS
(	(	kIx(
<g/>
25	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2016	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
:	:	kIx,
Harry	Harra	k1gFnSc2
Kane	kanout	k5eAaImIp3nS
(	(	kIx(
<g/>
29	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
:	:	kIx,
Mohamed	Mohamed	k1gMnSc1
Salah	Salah	k1gMnSc1
(	(	kIx(
<g/>
32	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
:	:	kIx,
Pierre-Emerick	Pierre-Emerick	k1gMnSc1
Aubameyang	Aubameyang	k1gMnSc1
a	a	k8xC
Sadio	Sadio	k1gMnSc1
Mané	Maná	k1gFnSc2
a	a	k8xC
Mohamed	Mohamed	k1gMnSc1
Salah	Salah	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
:	:	kIx,
Jamie	Jamie	k1gFnSc2
Vardy	Varda	k1gFnSc2
(	(	kIx(
<g/>
23	#num#	k4
<g/>
)	)	kIx)
Výše	vysoce	k6eAd2
jsou	být	k5eAaImIp3nP
uvedeni	uveden	k2eAgMnPc1d1
střelci	střelec	k1gMnPc1
od	od	k7c2
roku	rok	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
ustanoven	ustanoven	k2eAgInSc4d1
nový	nový	k2eAgInSc4d1
formát	formát	k1gInSc4
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závorce	závorka	k1gFnSc6
je	být	k5eAaImIp3nS
počet	počet	k1gInSc1
gólů	gól	k1gInPc2
nejlepších	dobrý	k2eAgMnPc2d3
střelců	střelec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
španělské	španělský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
1929	#num#	k4
<g/>
:	:	kIx,
Bienzobas	Bienzobas	k1gInSc1
</s>
<s>
1930	#num#	k4
<g/>
:	:	kIx,
Gorostiza	Gorostiza	k1gFnSc1
</s>
<s>
1931	#num#	k4
<g/>
:	:	kIx,
Bata	Bat	k1gInSc2
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
Bata	Bat	k1gInSc2
</s>
<s>
1933	#num#	k4
<g/>
:	:	kIx,
Olivares	Olivares	k1gInSc1
</s>
<s>
1934	#num#	k4
<g/>
:	:	kIx,
Lángara	Lángara	k1gFnSc1
</s>
<s>
1935	#num#	k4
<g/>
:	:	kIx,
Lángara	Lángara	k1gFnSc1
</s>
<s>
1936	#num#	k4
<g/>
:	:	kIx,
Lángara	Lángara	k1gFnSc1
</s>
<s>
1940	#num#	k4
<g/>
:	:	kIx,
Unamuno	Unamuna	k1gFnSc5
</s>
<s>
1941	#num#	k4
<g/>
:	:	kIx,
Pruden	Prudna	k1gFnPc2
</s>
<s>
1942	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Suárez	Suárez	k1gMnSc1
</s>
<s>
1943	#num#	k4
<g/>
:	:	kIx,
Martín	Martín	k1gInSc1
</s>
<s>
1944	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Suárez	Suárez	k1gMnSc1
</s>
<s>
1945	#num#	k4
<g/>
:	:	kIx,
Zarra	Zarr	k1gInSc2
</s>
<s>
1946	#num#	k4
<g/>
:	:	kIx,
Zarra	Zarr	k1gInSc2
</s>
<s>
1947	#num#	k4
<g/>
:	:	kIx,
Zarra	Zarr	k1gInSc2
</s>
<s>
1948	#num#	k4
<g/>
:	:	kIx,
Pahiñ	Pahiñ	k6eAd1
</s>
<s>
1949	#num#	k4
<g/>
:	:	kIx,
César	César	k1gMnSc1
</s>
<s>
1950	#num#	k4
<g/>
:	:	kIx,
Zarra	Zarr	k1gInSc2
</s>
<s>
1951	#num#	k4
<g/>
:	:	kIx,
Zarra	Zarr	k1gInSc2
</s>
<s>
1952	#num#	k4
<g/>
:	:	kIx,
Pahiñ	Pahiñ	k6eAd1
</s>
<s>
1953	#num#	k4
<g/>
:	:	kIx,
Zarra	Zarr	k1gInSc2
</s>
<s>
1954	#num#	k4
<g/>
:	:	kIx,
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1955	#num#	k4
<g/>
:	:	kIx,
Arza	Arz	k1gInSc2
</s>
<s>
1956	#num#	k4
<g/>
:	:	kIx,
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1957	#num#	k4
<g/>
:	:	kIx,
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1958	#num#	k4
<g/>
:	:	kIx,
Badenes	Badenes	k1gMnSc1
<g/>
,	,	kIx,
Di	Di	k1gMnSc1
Stéfano	Stéfana	k1gFnSc5
&	&	k?
Ricardo	Ricarda	k1gFnSc5
</s>
<s>
1959	#num#	k4
<g/>
:	:	kIx,
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1960	#num#	k4
<g/>
:	:	kIx,
Puskás	Puskás	k1gInSc1
</s>
<s>
1961	#num#	k4
<g/>
:	:	kIx,
Puskás	Puskás	k1gInSc1
</s>
<s>
1962	#num#	k4
<g/>
:	:	kIx,
Seminario	Seminario	k6eAd1
</s>
<s>
1963	#num#	k4
<g/>
:	:	kIx,
Puskás	Puskás	k1gInSc1
</s>
<s>
1964	#num#	k4
<g/>
:	:	kIx,
Puskás	Puskás	k1gInSc1
</s>
<s>
1965	#num#	k4
<g/>
:	:	kIx,
Ré	ré	k1gNnSc2
</s>
<s>
1966	#num#	k4
<g/>
:	:	kIx,
Aragonés	Aragonés	k1gInSc1
</s>
<s>
1967	#num#	k4
<g/>
:	:	kIx,
Waldo	Waldo	k1gNnSc1
</s>
<s>
1968	#num#	k4
<g/>
:	:	kIx,
Uriarte	Uriart	k1gInSc5
</s>
<s>
1969	#num#	k4
<g/>
:	:	kIx,
Amancio	Amancio	k6eAd1
&	&	k?
Gárate	Gárat	k1gMnSc5
</s>
<s>
1970	#num#	k4
<g/>
:	:	kIx,
Amancio	Amancio	k1gNnSc1
<g/>
,	,	kIx,
Aragonés	Aragonés	k1gInSc1
&	&	k?
Gárate	Gárat	k1gMnSc5
</s>
<s>
1971	#num#	k4
<g/>
:	:	kIx,
Gárate	Gárat	k1gInSc5
&	&	k?
Rexach	Rexacha	k1gFnPc2
</s>
<s>
1972	#num#	k4
<g/>
:	:	kIx,
Porta	porta	k1gFnSc1
</s>
<s>
1973	#num#	k4
<g/>
:	:	kIx,
Marianín	Marianín	k1gInSc1
</s>
<s>
1974	#num#	k4
<g/>
:	:	kIx,
Quini	Quieň	k1gFnSc6
</s>
<s>
1975	#num#	k4
<g/>
:	:	kIx,
Carlos	Carlos	k1gMnSc1
</s>
<s>
1976	#num#	k4
<g/>
:	:	kIx,
Quini	Quieň	k1gFnSc6
</s>
<s>
1977	#num#	k4
<g/>
:	:	kIx,
Kempes	Kempes	k1gInSc1
</s>
<s>
1978	#num#	k4
<g/>
:	:	kIx,
Kempes	Kempes	k1gInSc1
</s>
<s>
1979	#num#	k4
<g/>
:	:	kIx,
Krankl	Krankl	k1gFnSc1
</s>
<s>
1980	#num#	k4
<g/>
:	:	kIx,
Quini	Quieň	k1gFnSc6
</s>
<s>
1981	#num#	k4
<g/>
:	:	kIx,
Quini	Quieň	k1gFnSc6
</s>
<s>
1982	#num#	k4
<g/>
:	:	kIx,
Quini	Quieň	k1gFnSc6
</s>
<s>
1983	#num#	k4
<g/>
:	:	kIx,
Rincón	Rincón	k1gInSc1
</s>
<s>
1984	#num#	k4
<g/>
:	:	kIx,
Da	Da	k1gFnSc1
Silva	Silva	k1gFnSc1
&	&	k?
Juanito	Juanit	k2eAgNnSc4d1
</s>
<s>
1985	#num#	k4
<g/>
:	:	kIx,
Sánchez	Sánchez	k1gInSc1
</s>
<s>
1986	#num#	k4
<g/>
:	:	kIx,
Sánchez	Sánchez	k1gInSc1
</s>
<s>
1987	#num#	k4
<g/>
:	:	kIx,
Sánchez	Sánchez	k1gInSc1
</s>
<s>
1988	#num#	k4
<g/>
:	:	kIx,
Sánchez	Sánchez	k1gInSc1
</s>
<s>
1989	#num#	k4
<g/>
:	:	kIx,
Baltazar	Baltazar	k1gMnSc1
</s>
<s>
1990	#num#	k4
<g/>
:	:	kIx,
Sánchez	Sánchez	k1gInSc1
</s>
<s>
1991	#num#	k4
<g/>
:	:	kIx,
Butragueñ	Butragueñ	k6eAd1
</s>
<s>
1992	#num#	k4
<g/>
:	:	kIx,
Manolo	Manola	k1gFnSc5
</s>
<s>
1993	#num#	k4
<g/>
:	:	kIx,
Bebeto	Bebeto	k1gNnSc1
</s>
<s>
1994	#num#	k4
<g/>
:	:	kIx,
Romário	Romário	k6eAd1
</s>
<s>
1995	#num#	k4
<g/>
:	:	kIx,
Zamorano	Zamorana	k1gFnSc5
</s>
<s>
1996	#num#	k4
<g/>
:	:	kIx,
Pizzi	Pizze	k1gFnSc6
</s>
<s>
1997	#num#	k4
<g/>
:	:	kIx,
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
1998	#num#	k4
<g/>
:	:	kIx,
Vieri	Vier	k1gFnSc2
</s>
<s>
1999	#num#	k4
<g/>
:	:	kIx,
Raúl	Raúl	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
:	:	kIx,
Salva	salva	k1gFnSc1
</s>
<s>
2001	#num#	k4
<g/>
:	:	kIx,
Raúl	Raúl	k1gInSc1
</s>
<s>
2002	#num#	k4
<g/>
:	:	kIx,
Tristán	Tristán	k1gInSc1
</s>
<s>
2003	#num#	k4
<g/>
:	:	kIx,
Makaay	Makaaa	k1gFnSc2
</s>
<s>
2004	#num#	k4
<g/>
:	:	kIx,
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
2005	#num#	k4
<g/>
:	:	kIx,
Forlán	Forlán	k2eAgMnSc1d1
&	&	k?
Eto	Eto	k1gMnSc1
<g/>
'	'	kIx"
<g/>
o	o	k7c6
</s>
<s>
2006	#num#	k4
<g/>
:	:	kIx,
Eto	Eto	k1gFnSc2
<g/>
'	'	kIx"
<g/>
o	o	k7c6
</s>
<s>
2007	#num#	k4
<g/>
:	:	kIx,
Van	van	k1gInSc1
Nistelrooy	Nistelrooa	k1gFnSc2
</s>
<s>
2008	#num#	k4
<g/>
:	:	kIx,
Güiza	Güiza	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
:	:	kIx,
Forlán	Forlán	k1gInSc1
</s>
<s>
2010	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
2011	#num#	k4
<g/>
:	:	kIx,
C.	C.	kA
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
2012	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
2013	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
2014	#num#	k4
<g/>
:	:	kIx,
C.	C.	kA
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
2015	#num#	k4
<g/>
:	:	kIx,
C.	C.	kA
Ronaldo	Ronaldo	k1gNnSc1
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
L.	L.	kA
Suárez	Suárez	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
2018	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
2019	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
2020	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc6
</s>
<s>
Držitelé	držitel	k1gMnPc1
ceny	cena	k1gFnSc2
Bravo	bravo	k6eAd1
</s>
<s>
1978	#num#	k4
<g/>
:	:	kIx,
Case	Cas	k1gInSc2
•	•	k?
1979	#num#	k4
<g/>
:	:	kIx,
Birtles	Birtles	k1gInSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
H.	H.	kA
Müller	Müller	k1gInSc1
•	•	k?
1981	#num#	k4
<g/>
:	:	kIx,
Wark	Wark	k1gInSc1
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
Shaw	Shaw	k1gFnSc1
•	•	k?
1983	#num#	k4
<g/>
:	:	kIx,
Bonini	bonin	k2eAgMnPc1d1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Righetti	Righetti	k1gNnSc4
•	•	k?
1985	#num#	k4
<g/>
:	:	kIx,
Butragueñ	Butragueñ	k1gNnSc4
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Butragueñ	Butragueñ	k1gNnSc4
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
van	vana	k1gFnPc2
Basten	Bastno	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Ohana	Ohan	k1gInSc2
•	•	k?
1989	#num#	k4
<g/>
:	:	kIx,
Maldini	Maldin	k2eAgMnPc1d1
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Baggio	Baggio	k1gNnSc4
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Prosinečki	Prosinečki	k1gNnSc4
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Guardiola	Guardiola	k1gFnSc1
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
Giggs	Giggs	k1gInSc1
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Panucci	Panucec	k1gInSc6
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Kluivert	Kluivert	k1gInSc1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Del	Del	k1gFnSc1
Piero	Piero	k1gNnSc1
•	•	k?
1997	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
Buffon	Buffon	k1gInSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Casillas	Casillas	k1gInSc1
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Hargreaves	Hargreaves	k1gInSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Metzelder	Metzelder	k1gInSc1
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Rooney	Roonea	k1gFnSc2
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
C.	C.	kA
Ronaldo	Ronaldo	k1gNnSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Robben	Robbna	k1gFnPc2
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Fà	Fà	k1gInSc1
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Messi	Messe	k1gFnSc4
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Benzema	Benzema	k1gFnSc1
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Busquets	Busquets	k1gInSc1
•	•	k?
2010	#num#	k4
<g/>
:	:	kIx,
T.	T.	kA
Müller	Müller	k1gInSc1
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Hazard	hazard	k1gInSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Verratti	Verratti	k1gNnSc4
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Isco	Isco	k1gNnSc4
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Pogba	Pogba	k1gFnSc1
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Berardi	Berard	k1gMnPc1
</s>
<s>
Fotbalista	fotbalista	k1gMnSc1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
Anglie	Anglie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
<g/>
:	:	kIx,
Stanley	Stanlea	k1gFnSc2
Matthews	Matthews	k1gInSc1
•	•	k?
1949	#num#	k4
<g/>
:	:	kIx,
Johnny	Johnen	k2eAgInPc1d1
Carey	Carey	k1gInPc1
•	•	k?
1950	#num#	k4
<g/>
:	:	kIx,
Joe	Joe	k1gFnSc1
Mercer	Mercer	k1gInSc1
•	•	k?
1951	#num#	k4
<g/>
:	:	kIx,
Harry	Harra	k1gFnSc2
Johnston	Johnston	k1gInSc1
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
Billy	Bill	k1gMnPc4
Wright	Wrighta	k1gFnPc2
•	•	k?
1953	#num#	k4
<g/>
:	:	kIx,
Nat	Nat	k1gFnSc1
Lofthouse	Lofthouse	k1gFnSc2
•	•	k?
1954	#num#	k4
<g/>
:	:	kIx,
Tom	Tom	k1gMnSc1
Finney	Finnea	k1gFnSc2
•	•	k?
1955	#num#	k4
<g/>
:	:	kIx,
Don	Don	k1gMnSc1
Revie	Revie	k1gFnSc2
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Bert	Berta	k1gFnPc2
Trautmann	Trautmanna	k1gFnPc2
•	•	k?
1957	#num#	k4
<g/>
:	:	kIx,
Tom	Tom	k1gMnSc1
Finney	Finnea	k1gFnSc2
•	•	k?
1958	#num#	k4
<g/>
:	:	kIx,
Danny	Danna	k1gFnSc2
Blanchflower	Blanchflower	k1gInSc1
•	•	k?
1959	#num#	k4
<g/>
:	:	kIx,
Syd	Syd	k1gFnSc1
Owen	Owen	k1gInSc1
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Bill	Bill	k1gMnSc1
Slater	Slatra	k1gFnPc2
•	•	k?
1961	#num#	k4
<g/>
:	:	kIx,
Danny	Danna	k1gFnSc2
Blanchflower	Blanchflower	k1gInSc1
•	•	k?
1962	#num#	k4
<g/>
:	:	kIx,
Jimmy	Jimma	k1gFnSc2
Adamson	Adamson	k1gInSc1
•	•	k?
1963	#num#	k4
<g/>
:	:	kIx,
Stanley	Stanlea	k1gFnSc2
Matthews	Matthews	k1gInSc1
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Bobby	Bobba	k1gFnSc2
Moore	Moor	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1965	#num#	k4
<g/>
:	:	kIx,
Bobby	Bobba	k1gFnSc2
Collins	Collins	k1gInSc1
•	•	k?
1966	#num#	k4
<g/>
:	:	kIx,
Bobby	Bobba	k1gFnSc2
Charlton	Charlton	k1gInSc1
•	•	k?
1967	#num#	k4
<g/>
:	:	kIx,
Jack	Jack	k1gInSc1
Charlton	Charlton	k1gInSc1
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
George	Georg	k1gInSc2
Best	Best	k1gInSc1
•	•	k?
1969	#num#	k4
<g/>
:	:	kIx,
Tony	Tony	k1gMnSc1
Book	Book	k1gMnSc1
a	a	k8xC
Dave	Dav	k1gInSc5
Mackay	Mackay	k1gInPc7
•	•	k?
1970	#num#	k4
<g/>
:	:	kIx,
Billy	Bill	k1gMnPc4
Bremner	Bremnra	k1gFnPc2
•	•	k?
1971	#num#	k4
<g/>
:	:	kIx,
Frank	Frank	k1gMnSc1
McLintock	McLintocka	k1gFnPc2
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Gordon	Gordon	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Banks	Banks	k1gInSc1
<g/>
]	]	kIx)
•	•	k?
1973	#num#	k4
<g/>
:	:	kIx,
Pat	pat	k1gInSc1
Jennings	Jennings	k1gInSc1
•	•	k?
1974	#num#	k4
<g/>
:	:	kIx,
Ian	Ian	k1gFnSc1
Callaghan	Callaghan	k1gInSc1
•	•	k?
1975	#num#	k4
<g/>
:	:	kIx,
Alan	alan	k1gInSc1
Mullery	Muller	k1gInPc1
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Kevin	Kevin	k2eAgInSc1d1
Keegan	Keegan	k1gInSc1
•	•	k?
1977	#num#	k4
<g/>
:	:	kIx,
Emlyn	Emlyn	k1gInSc1
Hughes	Hughes	k1gInSc1
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Kenny	Kenna	k1gFnSc2
Burns	Burns	k1gInSc1
•	•	k?
1979	#num#	k4
<g/>
:	:	kIx,
Kenny	Kenna	k1gFnSc2
Dalglish	Dalglish	k1gInSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Terry	Terra	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
McDermott	McDermott	k1gInSc1
•	•	k?
1981	#num#	k4
<g/>
:	:	kIx,
Frans	Frans	k1gMnSc1
Thijssen	Thijssna	k1gFnPc2
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
Steve	Steve	k1gMnSc1
Perryman	Perryman	k1gMnSc1
•	•	k?
1983	#num#	k4
<g/>
:	:	kIx,
Kenny	Kenna	k1gFnSc2
Dalglish	Dalglish	k1gInSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Ian	Ian	k1gFnSc1
Rush	Rush	k1gInSc1
•	•	k?
1985	#num#	k4
<g/>
:	:	kIx,
Neville	Nevill	k1gInSc6
Southall	Southall	k1gInSc4
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Gary	Gara	k1gFnSc2
Lineker	Lineker	k1gInSc1
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
Clive	Cliev	k1gFnSc2
Allen	allen	k1gInSc1
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Barnes	Barnes	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
1989	#num#	k4
<g/>
:	:	kIx,
Steve	Steve	k1gMnSc1
Nicol	Nicola	k1gFnPc2
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Barnes	Barnesa	k1gFnPc2
•	•	k?
1991	#num#	k4
<g/>
:	:	kIx,
Gordon	Gordon	k1gInSc1
Strachan	Strachan	k1gInSc1
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Gary	Gara	k1gFnSc2
Lineker	Lineker	k1gInSc1
•	•	k?
1993	#num#	k4
<g/>
:	:	kIx,
Chris	Chris	k1gFnSc2
Waddle	Waddle	k1gFnSc2
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Alan	Alan	k1gMnSc1
Shearer	Shearra	k1gFnPc2
•	•	k?
1995	#num#	k4
<g/>
:	:	kIx,
Jürgen	Jürgen	k1gInSc1
Klinsmann	Klinsmann	k1gInSc1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Eric	Eric	k1gFnSc1
Cantona	Cantona	k1gFnSc1
•	•	k?
1997	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
:	:	kIx,
Gianfranco	Gianfranco	k1gNnSc1
Zola	Zol	k1gInSc2
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Dennis	Dennis	k1gFnSc1
Bergkamp	Bergkamp	k1gInSc1
•	•	k?
1999	#num#	k4
<g/>
:	:	kIx,
David	David	k1gMnSc1
Ginola	Ginola	k1gFnSc1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Roy	Roy	k1gMnSc1
Keane	Kean	k1gInSc5
•	•	k?
2001	#num#	k4
<g/>
:	:	kIx,
Teddy	Tedda	k1gFnSc2
Sheringham	Sheringham	k1gInSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Pirè	Pirè	k1gFnPc2
•	•	k?
2003	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	henry	k1gInSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	henry	k1gInSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Frank	Frank	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Lampard	Lampard	k1gInSc1
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Thierry	Thierra	k1gFnSc2
Henry	henry	k1gInSc1
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Steven	Steven	k2eAgInSc1d1
Gerrard	Gerrard	k1gInSc1
•	•	k?
2010	#num#	k4
<g/>
:	:	kIx,
Wayne	Wayn	k1gInSc5
Rooney	Rooney	k1gInPc7
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Scott	Scott	k2eAgInSc1d1
Parker	Parker	k1gInSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Robin	robin	k2eAgInSc1d1
van	van	k1gInSc1
Persie	Persie	k1gFnSc2
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Gareth	Gareth	k1gInSc1
Bale	bal	k1gInSc5
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Luis	Luisa	k1gFnPc2
Suárez	Suárez	k1gInSc1
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Eden	Eden	k1gInSc1
Hazard	hazard	k1gInSc1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Jamie	Jamie	k1gFnSc1
Vardy	Varda	k1gFnSc2
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
N	N	kA
<g/>
'	'	kIx"
<g/>
Golo	Golo	k6eAd1
Kanté	Kantý	k2eAgFnSc2d1
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Mohamed	Mohamed	k1gMnSc1
Salah	Salaha	k1gFnPc2
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Raheem	Raheus	k1gMnSc7
Sterling	sterling	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
sportovec	sportovec	k1gMnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
1958	#num#	k4
Zdzisław	Zdzisław	k1gMnSc1
Krzyszkowiak	Krzyszkowiak	k1gMnSc1
•	•	k?
1959	#num#	k4
Vasilij	Vasilij	k1gFnPc2
Kuzněcov	Kuzněcov	k1gInSc4
•	•	k?
1960	#num#	k4
Jurij	Jurij	k1gFnPc2
Vlasov	Vlasov	k1gInSc4
•	•	k?
1961	#num#	k4
Valerij	Valerij	k1gFnSc2
Brumel	Brumela	k1gFnPc2
•	•	k?
1962	#num#	k4
Valerij	Valerij	k1gFnSc2
Brumel	Brumela	k1gFnPc2
•	•	k?
1963	#num#	k4
Valerij	Valerij	k1gFnSc2
Brumel	Brumela	k1gFnPc2
•	•	k?
1964	#num#	k4
Lidija	Lidij	k2eAgFnSc1d1
Skoblikovová	Skoblikovová	k1gFnSc1
•	•	k?
1965	#num#	k4
Michel	Michel	k1gInSc1
Jazy	Jaza	k1gFnSc2
•	•	k?
1966	#num#	k4
Irena	Irena	k1gFnSc1
Szewińská	Szewińská	k1gFnSc1
•	•	k?
1967	#num#	k4
Jean-Claude	Jean-Claud	k1gInSc5
Killy	Kill	k1gInPc7
•	•	k?
1968	#num#	k4
Jean-Claude	Jean-Claud	k1gInSc5
Killy	Kill	k1gInPc7
•	•	k?
1969	#num#	k4
Eddy	Edda	k1gFnSc2
Merckx	Merckx	k1gInSc1
•	•	k?
1970	#num#	k4
Eddy	Edda	k1gFnSc2
Merckx	Merckx	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1971	#num#	k4
Juha	Juha	k1gFnSc1
Väätäinen	Väätäinna	k1gFnPc2
•	•	k?
1972	#num#	k4
Lasse	Lasse	k1gFnSc2
Virén	Virén	k1gInSc1
•	•	k?
1973	#num#	k4
Kornelia	Kornelia	k1gFnSc1
Enderová	Enderová	k1gFnSc1
•	•	k?
1974	#num#	k4
Irena	Irena	k1gFnSc1
Szewińská	Szewińská	k1gFnSc1
•	•	k?
1975	#num#	k4
Kornelia	Kornelia	k1gFnSc1
Enderová	Enderová	k1gFnSc1
•	•	k?
1976	#num#	k4
Nadia	Nadius	k1gMnSc2
Comaneciová	Comaneciový	k2eAgFnSc1d1
•	•	k?
1977	#num#	k4
Rosemarie	Rosemarie	k1gFnSc1
Ackermannová	Ackermannová	k1gFnSc1
•	•	k?
1978	#num#	k4
Vladimir	Vladimir	k1gInSc1
Jaščenko	Jaščenka	k1gFnSc5
•	•	k?
1979	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Coe	Coe	k1gMnSc1
•	•	k?
1980	#num#	k4
Vladimir	Vladimir	k1gInSc1
Salnikov	Salnikov	k1gInSc4
•	•	k?
1981	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Coe	Coe	k1gMnSc1
•	•	k?
1982	#num#	k4
Daley	Dalea	k1gFnSc2
Thompson	Thompsona	k1gFnPc2
•	•	k?
1983	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Jarmila	Jarmila	k1gFnSc1
Kratochvílová	Kratochvílová	k1gFnSc1
•	•	k?
1984	#num#	k4
Michael	Michael	k1gMnSc1
Gross	Gross	k1gMnSc1
•	•	k?
1985	#num#	k4
Sergej	Sergej	k1gMnSc1
Bubka	Bubek	k1gInSc2
•	•	k?
1986	#num#	k4
Heike	Heike	k1gFnSc1
Drechslerová	Drechslerová	k1gFnSc1
•	•	k?
1987	#num#	k4
Stephen	Stephen	k2eAgInSc4d1
Roche	Roche	k1gInSc4
•	•	k?
1988	#num#	k4
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
•	•	k?
1989	#num#	k4
Steffi	Steffi	k1gFnSc1
Grafová	Grafová	k1gFnSc1
•	•	k?
1990	#num#	k4
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
•	•	k?
1991	#num#	k4
Katrin	Katrin	k1gInSc1
Krabbeová	Krabbeová	k1gFnSc1
•	•	k?
1992	#num#	k4
Nigel	Nigel	k1gInSc1
Mansell	Mansell	k1gInSc4
•	•	k?
1993	#num#	k4
Linford	Linford	k1gInSc1
Christie	Christie	k1gFnSc2
•	•	k?
1994	#num#	k4
Johann	Johanno	k1gNnPc2
Olav	Olava	k1gFnPc2
Koss	Kossa	k1gFnPc2
•	•	k?
1995	#num#	k4
Jonathan	Jonathan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Edwards	Edwards	k1gInSc1
•	•	k?
1996	#num#	k4
Světlana	Světlana	k1gFnSc1
Mastěrkovová	Mastěrkovová	k1gFnSc1
•	•	k?
1997	#num#	k4
Martina	Martin	k1gMnSc2
Hingisová	Hingisový	k2eAgFnSc1d1
•	•	k?
1998	#num#	k4
Mika	Mik	k1gMnSc2
Häkkinen	Häkkinen	k1gInSc4
•	•	k?
1999	#num#	k4
Gabriela	Gabriela	k1gFnSc1
Szabóová	Szabóová	k1gFnSc1
•	•	k?
2000	#num#	k4
Inge	Inge	k1gFnSc2
de	de	k?
Bruijnová	Bruijnová	k1gFnSc1
•	•	k?
2001	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2002	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2003	#num#	k4
Michael	Michaela	k1gFnPc2
Schumacher	Schumachra	k1gFnPc2
•	•	k?
2004	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
•	•	k?
2005	#num#	k4
Roger	Rogero	k1gNnPc2
Federer	Federra	k1gFnPc2
a	a	k8xC
Jelena	Jelena	k1gFnSc1
Isinbajevová	Isinbajevová	k1gFnSc1
•	•	k?
2006	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
2007	#num#	k4
Roger	Rogra	k1gFnPc2
Federer	Federra	k1gFnPc2
•	•	k?
2008	#num#	k4
Rafael	Rafaela	k1gFnPc2
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
2009	#num#	k4
Roger	Roger	k1gInSc1
Federer	Federer	k1gInSc4
•	•	k?
2010	#num#	k4
Rafael	Rafaela	k1gFnPc2
Nadal	nadat	k5eAaPmAgMnS
•	•	k?
2011	#num#	k4
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2012	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2013	#num#	k4
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
•	•	k?
2014	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2015	#num#	k4
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2016	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2017	#num#	k4
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronaldo	k1gNnSc4
•	•	k?
2018	#num#	k4
Novak	Novak	k1gInSc1
Djoković	Djoković	k1gFnSc2
•	•	k?
2019	#num#	k4
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc4
•	•	k?
2020	#num#	k4
Robert	Robert	k1gMnSc1
Lewandowski	Lewandowske	k1gFnSc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
161225	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
140422390	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1460	#num#	k4
7485	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2007130960	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
42170021	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2007130960	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Portugalsko	Portugalsko	k1gNnSc1
</s>
