<p>
<s>
Park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
nebo	nebo	k8xC	nebo
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
udržovaná	udržovaný	k2eAgFnSc1d1	udržovaná
zeleň	zeleň	k1gFnSc1	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
často	často	k6eAd1	často
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
některé	některý	k3yIgFnPc4	některý
významnější	významný	k2eAgFnPc4d2	významnější
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zámky	zámek	k1gInPc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Údržba	údržba	k1gFnSc1	údržba
veřejného	veřejný	k2eAgInSc2d1	veřejný
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
typicky	typicky	k6eAd1	typicky
financována	financován	k2eAgFnSc1d1	financována
městem	město	k1gNnSc7	město
nebo	nebo	k8xC	nebo
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
park	park	k1gInSc1	park
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
majitelem	majitel	k1gMnSc7	majitel
přilehlé	přilehlý	k2eAgFnSc2d1	přilehlá
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
estetická	estetický	k2eAgFnSc1d1	estetická
a	a	k8xC	a
relaxační	relaxační	k2eAgFnSc1d1	relaxační
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
do	do	k7c2	do
parku	park	k1gInSc2	park
chodí	chodit	k5eAaImIp3nP	chodit
procházet	procházet	k5eAaImF	procházet
nebo	nebo	k8xC	nebo
jen	jen	k6eAd1	jen
posedět	posedět	k5eAaPmF	posedět
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
si	se	k3xPyFc3	se
v	v	k7c6	v
parcích	park	k1gInPc6	park
hrávají	hrávat	k5eAaImIp3nP	hrávat
<g/>
.	.	kIx.	.
</s>
<s>
Park	park	k1gInSc1	park
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
upraveného	upravený	k2eAgNnSc2d1	upravené
prostranství	prostranství	k1gNnSc2	prostranství
se	se	k3xPyFc4	se
zelení	zelený	k2eAgMnPc1d1	zelený
mohou	moct	k5eAaImIp3nP	moct
vlastnit	vlastnit	k5eAaImF	vlastnit
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
soukromé	soukromý	k2eAgFnPc4d1	soukromá
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
soukromé	soukromý	k2eAgFnPc4d1	soukromá
i	i	k8xC	i
veřejné	veřejný	k2eAgFnPc4d1	veřejná
organizace	organizace	k1gFnPc4	organizace
za	za	k7c7	za
různým	různý	k2eAgInSc7d1	různý
účelem	účel	k1gInSc7	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
parky	park	k1gInPc1	park
jsou	být	k5eAaImIp3nP	být
upraveny	upravit	k5eAaPmNgInP	upravit
podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
sadovnické	sadovnický	k2eAgFnSc2d1	Sadovnická
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
stromy	strom	k1gInPc7	strom
<g/>
,	,	kIx,	,
cestami	cesta	k1gFnPc7	cesta
a	a	k8xC	a
záhony	záhon	k1gInPc7	záhon
s	s	k7c7	s
květinami	květina	k1gFnPc7	květina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k8xC	i
vodními	vodní	k2eAgFnPc7d1	vodní
plochami	plocha	k1gFnPc7	plocha
(	(	kIx(	(
<g/>
jezírky	jezírko	k1gNnPc7	jezírko
<g/>
,	,	kIx,	,
rybníky	rybník	k1gInPc1	rybník
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
potoky	potok	k1gInPc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
bývají	bývat	k5eAaImIp3nP	bývat
dekorativní	dekorativní	k2eAgFnPc1d1	dekorativní
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
fontány	fontána	k1gFnPc1	fontána
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sochy	socha	k1gFnPc1	socha
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
Městské	městský	k2eAgInPc1d1	městský
parky	park	k1gInPc1	park
mohou	moct	k5eAaImIp3nP	moct
snižovat	snižovat	k5eAaImF	snižovat
teplotu	teplota	k1gFnSc4	teplota
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chráněné	chráněný	k2eAgNnSc4d1	chráněné
území	území	k1gNnSc4	území
==	==	k?	==
</s>
</p>
<p>
<s>
Státem	stát	k1gInSc7	stát
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
–	–	k?	–
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
území	území	k1gNnSc4	území
vyhlášené	vyhlášený	k2eAgNnSc4d1	vyhlášené
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
území	území	k1gNnSc6	území
rostou	růst	k5eAaImIp3nP	růst
vzácné	vzácný	k2eAgFnPc4d1	vzácná
rostliny	rostlina	k1gFnPc4	rostlina
nebo	nebo	k8xC	nebo
žijí	žít	k5eAaImIp3nP	žít
ohrožená	ohrožený	k2eAgNnPc4d1	ohrožené
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typem	typ	k1gInSc7	typ
chráněného	chráněný	k2eAgNnSc2d1	chráněné
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
národním	národní	k2eAgInSc6d1	národní
parku	park	k1gInSc6	park
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zcela	zcela	k6eAd1	zcela
zakázán	zakázat	k5eAaPmNgInS	zakázat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
stavění	stavění	k1gNnSc1	stavění
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInSc1d1	přírodní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
geopark	geopark	k1gInSc1	geopark
<g/>
,	,	kIx,	,
lesnický	lesnický	k2eAgInSc1d1	lesnický
park	park	k1gInSc1	park
==	==	k?	==
</s>
</p>
<p>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
typ	typ	k1gInSc1	typ
<g/>
,	,	kIx,	,
podléhající	podléhající	k2eAgFnSc3d1	podléhající
nejmírnější	mírný	k2eAgFnSc3d3	nejmírnější
ochraně	ochrana	k1gFnSc3	ochrana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nazývá	nazývat	k5eAaImIp3nS	nazývat
přírodní	přírodní	k2eAgInSc4d1	přírodní
park	park	k1gInSc4	park
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
podobná	podobný	k2eAgFnSc1d1	podobná
území	území	k1gNnSc6	území
nazývala	nazývat	k5eAaImAgFnS	nazývat
klidová	klidový	k2eAgFnSc1d1	klidová
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Geopark	Geopark	k1gInSc1	Geopark
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
cenné	cenný	k2eAgNnSc1d1	cenné
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
místními	místní	k2eAgMnPc7d1	místní
obyvateli	obyvatel	k1gMnPc7	obyvatel
a	a	k8xC	a
organizacemi	organizace	k1gFnPc7	organizace
rozvíjeny	rozvíjen	k2eAgFnPc1d1	rozvíjena
aktivity	aktivita	k1gFnPc1	aktivita
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
rozvoje	rozvoj	k1gInSc2	rozvoj
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
návštěvnosti	návštěvnost	k1gFnSc2	návštěvnost
a	a	k8xC	a
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
<g/>
.	.	kIx.	.
</s>
<s>
Lesnický	lesnický	k2eAgInSc1d1	lesnický
park	park	k1gInSc1	park
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
objekt	objekt	k1gInSc1	objekt
lesního	lesní	k2eAgNnSc2d1	lesní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
trvale	trvale	k6eAd1	trvale
udržitelně	udržitelně	k6eAd1	udržitelně
hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
podle	podle	k7c2	podle
zásad	zásada	k1gFnPc2	zásada
lesnického	lesnický	k2eAgInSc2d1	lesnický
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dendrologie	dendrologie	k1gFnSc1	dendrologie
</s>
</p>
<p>
<s>
Lesopark	lesopark	k1gInSc1	lesopark
</s>
</p>
<p>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Zahradnictví	zahradnictví	k1gNnSc1	zahradnictví
</s>
</p>
<p>
<s>
Zahradní	zahradní	k2eAgFnSc1d1	zahradní
architektura	architektura	k1gFnSc1	architektura
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
park	park	k1gInSc1	park
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
park	park	k1gInSc1	park
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Park	park	k1gInSc1	park
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
