<s>
Sophiina	Sophiin	k2eAgFnSc1d1	Sophiina
volba	volba	k1gFnSc1	volba
–	–	k?	–
romantické	romantický	k2eAgNnSc4d1	romantické
drama	drama	k1gNnSc4	drama
režiséra	režisér	k1gMnSc2	režisér
Alana	Alan	k1gMnSc2	Alan
J.	J.	kA	J.
Pakuly	Pakula	k1gFnSc2	Pakula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
Meryl	Meryl	k1gInSc4	Meryl
Streepová	Streepová	k1gFnSc1	Streepová
(	(	kIx(	(
<g/>
za	za	k7c4	za
roli	role	k1gFnSc4	role
Sophie	Sophie	k1gFnSc2	Sophie
získala	získat	k5eAaPmAgFnS	získat
Oscara	Oscara	k1gFnSc1	Oscara
a	a	k8xC	a
Zlatý	zlatý	k2eAgInSc1d1	zlatý
glóbus	glóbus	k1gInSc1	glóbus
<g/>
)	)	kIx)	)
,	,	kIx,	,
Kevin	Kevin	k1gMnSc1	Kevin
Kline	klinout	k5eAaImIp3nS	klinout
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
MacNicol	MacNicola	k1gFnPc2	MacNicola
<g/>
.	.	kIx.	.
</s>
