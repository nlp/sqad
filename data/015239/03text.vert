<s>
Rošických	Rošická	k1gFnPc2
</s>
<s>
Rošických	Rošická	k1gFnPc2
</s>
<s>
Mariánské	mariánský	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
na	na	k7c6
konci	konec	k1gInSc6
uliceUmístění	uliceUmístění	k1gNnSc2
Město	město	k1gNnSc4
</s>
<s>
Praha	Praha	k1gFnSc1
Městská	městský	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
1	#num#	k4
Čtvrť	čtvrť	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Strana	strana	k1gFnSc1
Poloha	poloha	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
46,79	46,79	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
13,36	13,36	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Začíná	začínat	k5eAaImIp3nS
na	na	k7c4
</s>
<s>
Újezd	Újezd	k1gInSc1
Končí	končit	k5eAaImIp3nS
na	na	k7c4
</s>
<s>
Mariánské	mariánský	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
Další	další	k2eAgInPc4d1
údaje	údaj	k1gInPc4
PSČ	PSČ	kA
</s>
<s>
118	#num#	k4
00	#num#	k4
Kód	kód	k1gInSc1
ulice	ulice	k1gFnSc2
</s>
<s>
467880	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ulice	ulice	k1gFnSc1
Rošických	Rošická	k1gFnPc2
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
je	být	k5eAaImIp3nS
jednosměrná	jednosměrný	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
směrující	směrující	k2eAgFnSc1d1
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
Újezdu	Újezd	k1gInSc2
severně	severně	k6eAd1
po	po	k7c4
zbytek	zbytek	k1gInSc4
městských	městský	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
pokračují	pokračovat	k5eAaImIp3nP
ke	k	k7c3
Hladové	hladový	k2eAgFnSc3d1
zdi	zeď	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazvána	nazván	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c4
počest	počest	k1gFnSc4
českých	český	k2eAgMnPc2d1
vlastenců	vlastenec	k1gMnPc2
Jaroslava	Jaroslav	k1gMnSc2
Rošického	Rošický	k2eAgMnSc2d1
a	a	k8xC
Evžena	Evžen	k1gMnSc2
Rošického	Rošický	k2eAgMnSc2d1
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
byl	být	k5eAaImAgInS
nazván	nazvat	k5eAaPmNgInS,k5eAaBmNgInS
i	i	k9
Memoriál	memoriál	k1gInSc1
Evžena	Evžen	k1gMnSc2
Rošického	Rošický	k2eAgMnSc2d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
stadion	stadion	k1gInSc1
Evžena	Evžen	k1gMnSc2
Rošického	Rošický	k2eAgMnSc2d1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
čísle	číslo	k1gNnSc6
8	#num#	k4
má	mít	k5eAaImIp3nS
pobočku	pobočka	k1gFnSc4
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
grafická	grafický	k2eAgFnSc1d1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
grafická	grafický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Budovy	budova	k1gFnPc1
<g/>
,	,	kIx,
firmy	firma	k1gFnPc1
a	a	k8xC
instituce	instituce	k1gFnPc1
</s>
<s>
restaurant	restaurant	k1gInSc1
Atelier	atelier	k1gNnSc2
-	-	kIx~
Rošických	Rošická	k1gFnPc2
4	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
odborná	odborný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
grafická	grafický	k2eAgFnSc1d1
a	a	k8xC
Střední	střední	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
škola	škola	k1gFnSc1
grafická	grafický	k2eAgFnSc1d1
<g/>
,	,	kIx,
pobočka	pobočka	k1gFnSc1
-	-	kIx~
Rošických	Rošický	k2eAgInPc2d1
8	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.halonoviny.cz/articles/view/30435803	http://www.halonoviny.cz/articles/view/30435803	k4
<g/>
↑	↑	k?
https://www.lidovky.cz/pohnute-osudy-atlet-evzen-rosicky-byl-popraven-za-heydrichiady-pww-/lide.aspx?c=A160202_152304_lide_ELE	https://www.lidovky.cz/pohnute-osudy-atlet-evzen-rosicky-byl-popraven-za-heydrichiady-pww-/lide.aspx?c=A160202_152304_lide_ELE	k4
<g/>
↑	↑	k?
http://atelier-restaurant.cz	http://atelier-restaurant.cz	k1gMnSc1
<g/>
↑	↑	k?
http://docplayer.cz/6506938-Gabriel-fragner-funkeho-zaci-na-statni-graficke-skole-v-praze-teoreticka-diplomova-prace.html	http://docplayer.cz/6506938-Gabriel-fragner-funkeho-zaci-na-statni-graficke-skole-v-praze-teoreticka-diplomova-prace.html	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rošických	Rošická	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
</s>
