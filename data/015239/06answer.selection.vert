<s>
Ulice	ulice	k1gFnSc1
Rošických	Rošická	k1gFnPc2
na	na	k7c6
Malé	Malé	k2eAgFnSc6d1
Straně	strana	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
je	být	k5eAaImIp3nS
jednosměrná	jednosměrný	k2eAgFnSc1d1
ulice	ulice	k1gFnSc1
směrující	směrující	k2eAgFnSc1d1
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
části	část	k1gFnSc6
Újezdu	Újezd	k1gInSc2
severně	severně	k6eAd1
po	po	k7c4
zbytek	zbytek	k1gInSc4
městských	městský	k2eAgFnPc2d1
hradeb	hradba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
pokračují	pokračovat	k5eAaImIp3nP
ke	k	k7c3
Hladové	hladový	k2eAgFnSc3d1
zdi	zeď	k1gFnSc3
<g/>
.	.	kIx.
</s>