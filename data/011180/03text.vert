<p>
<s>
Lichenologie	lichenologie	k1gFnSc1	lichenologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
lišejnících	lišejník	k1gInPc6	lišejník
-	-	kIx~	-
symbióze	symbióza	k1gFnSc3	symbióza
houby	houba	k1gFnSc2	houba
(	(	kIx(	(
<g/>
mykobiont	mykobiont	k1gInSc1	mykobiont
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
fykobiont	fykobiont	k1gInSc1	fykobiont
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Logicky	logicky	k6eAd1	logicky
tedy	tedy	k9	tedy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hraniční	hraniční	k2eAgInSc4d1	hraniční
obor	obor	k1gInSc4	obor
mezi	mezi	k7c7	mezi
mykologií	mykologie	k1gFnSc7	mykologie
a	a	k8xC	a
botanikou	botanika	k1gFnSc7	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
lišejníky	lišejník	k1gInPc1	lišejník
jsou	být	k5eAaImIp3nP	být
lichenologové	lichenolog	k1gMnPc1	lichenolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
taxonomii	taxonomie	k1gFnSc6	taxonomie
lišejníků	lišejník	k1gInPc2	lišejník
zevrubně	zevrubně	k6eAd1	zevrubně
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
švédský	švédský	k2eAgMnSc1d1	švédský
botanik	botanik	k1gMnSc1	botanik
Erik	Erik	k1gMnSc1	Erik
Acharius	Acharius	k1gMnSc1	Acharius
(	(	kIx(	(
<g/>
1757	[number]	k4	1757
<g/>
-	-	kIx~	-
<g/>
1819	[number]	k4	1819
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
lichenologie	lichenologie	k1gFnSc2	lichenologie
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
studentem	student	k1gMnSc7	student
Carla	Carl	k1gMnSc2	Carl
Linného	Linný	k1gMnSc2	Linný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
Achariovým	Achariový	k2eAgFnPc3d1	Achariový
vědeckým	vědecký	k2eAgFnPc3d1	vědecká
pracím	práce	k1gFnPc3	práce
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lichenographiae	Lichenographia	k1gInPc1	Lichenographia
Suecia	Suecia	k1gFnSc1	Suecia
prodromus	prodromus	k1gInSc1	prodromus
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Methodus	Methodus	k1gInSc1	Methodus
lichenum	lichenum	k1gInSc1	lichenum
(	(	kIx(	(
<g/>
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lichenographia	Lichenographia	k1gFnSc1	Lichenographia
universalis	universalis	k1gFnSc1	universalis
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Synopsis	synopsis	k1gFnSc1	synopsis
methodica	methodica	k1gMnSc1	methodica
lichenum	lichenum	k1gInSc1	lichenum
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
<g/>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
lichenology	lichenolog	k1gMnPc4	lichenolog
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Edward	Edward	k1gMnSc1	Edward
Tuckerman	Tuckerman	k1gMnSc1	Tuckerman
či	či	k8xC	či
Konstantin	Konstantin	k1gMnSc1	Konstantin
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Merežkovskij	Merežkovskij	k1gMnSc1	Merežkovskij
<g/>
.	.	kIx.	.
</s>
<s>
Symbiotickou	symbiotický	k2eAgFnSc4d1	symbiotická
povahu	povaha	k1gFnSc4	povaha
lišejníků	lišejník	k1gInPc2	lišejník
objevil	objevit	k5eAaPmAgInS	objevit
roku	rok	k1gInSc2	rok
1867	[number]	k4	1867
Švýcar	Švýcar	k1gMnSc1	Švýcar
Simon	Simon	k1gMnSc1	Simon
Schwendener	Schwendener	k1gMnSc1	Schwendener
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
lichenology	lichenolog	k1gMnPc4	lichenolog
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
