<s>
Lichenologie	lichenologie	k1gFnSc1	lichenologie
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
lišejnících	lišejník	k1gInPc6	lišejník
-	-	kIx~	-
symbióze	symbióza	k1gFnSc3	symbióza
houby	houba	k1gFnSc2	houba
(	(	kIx(	(
<g/>
mykobiont	mykobiont	k1gInSc1	mykobiont
<g/>
)	)	kIx)	)
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
(	(	kIx(	(
<g/>
fykobiont	fykobiont	k1gInSc1	fykobiont
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
