<s>
Morava	Morava	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
historickém	historický	k2eAgNnSc6d1
území	území	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Morava	Morava	k1gFnSc1
Mikulov	Mikulov	k1gInSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
zeleně	zeleně	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
moravské	moravský	k2eAgFnPc1d1
enklávy	enkláva	k1gFnPc1
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
(	(	kIx(
<g/>
červeně	červeně	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
hranicích	hranice	k1gFnPc6
z	z	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
na	na	k7c6
mapě	mapa	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
město	město	k1gNnSc1
</s>
<s>
historická	historický	k2eAgNnPc1d1
hlavní	hlavní	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
a	a	k8xC
Olomouc	Olomouc	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
region	region	k1gInSc1
<g/>
,	,	kIx,
historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
22	#num#	k4
348,87	348,87	k4
km²	km²	k?
v	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
<g/>
,	,	kIx,
bez	bez	k7c2
moravských	moravský	k2eAgFnPc2d1
enkláv	enkláva	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
poblíž	poblíž	k7c2
vrcholu	vrchol	k1gInSc2
Pradědu	praděd	k1gMnSc6
(	(	kIx(
<g/>
1491	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
(	(	kIx(
<g/>
moravština	moravština	k1gFnSc1
<g/>
)	)	kIx)
Národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Češi	Čech	k1gMnPc1
<g/>
,	,	kIx,
Moravané	Moravan	k1gMnPc1
<g/>
,	,	kIx,
Slezané	Slezan	k1gMnPc1
<g/>
,	,	kIx,
Slováci	Slovák	k1gMnPc1
aj.	aj.	kA
Náboženství	náboženství	k1gNnSc3
</s>
<s>
křesťanství	křesťanství	k1gNnSc1
aj.	aj.	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Morava	Morava	k1gFnSc1
je	být	k5eAaImIp3nS
region	region	k1gInSc4
na	na	k7c6
východě	východ	k1gInSc6
území	území	k1gNnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
a	a	k8xC
prvního	první	k4xOgNnSc2
období	období	k1gNnSc2
dějin	dějiny	k1gFnPc2
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
současnosti	současnost	k1gFnSc2
si	se	k3xPyFc3
zachovala	zachovat	k5eAaPmAgFnS
rozmanitou	rozmanitý	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
tradice	tradice	k1gFnPc4
a	a	k8xC
nářečí	nářečí	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
jméno	jméno	k1gNnSc4
je	být	k5eAaImIp3nS
předslovanského	předslovanský	k2eAgInSc2d1
původu	původ	k1gInSc2
s	s	k7c7
významem	význam	k1gInSc7
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
močál	močál	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
západě	západ	k1gInSc6
Morava	Morava	k1gFnSc1
hraničí	hraničit	k5eAaImIp3nS
s	s	k7c7
Čechami	Čechy	k1gFnPc7
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
polským	polský	k2eAgNnSc7d1
Kladskem	Kladsko	k1gNnSc7
a	a	k8xC
s	s	k7c7
českým	český	k2eAgNnSc7d1
Slezskem	Slezsko	k1gNnSc7
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
se	s	k7c7
Slovenskem	Slovensko	k1gNnSc7
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
Dolními	dolní	k2eAgInPc7d1
Rakousy	Rakousy	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgNnSc7d3
městem	město	k1gNnSc7
Moravy	Morava	k1gFnSc2
je	být	k5eAaImIp3nS
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinými	jiný	k2eAgNnPc7d1
významnými	významný	k2eAgNnPc7d1
sídly	sídlo	k1gNnPc7
na	na	k7c6
historickém	historický	k2eAgNnSc6d1
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
vedle	vedle	k7c2
Brna	Brno	k1gNnSc2
druhé	druhý	k4xOgNnSc4
historické	historický	k2eAgNnSc4d1
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
sídlo	sídlo	k1gNnSc1
olomoucké	olomoucký	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Znojmo	Znojmo	k1gNnSc1
(	(	kIx(
<g/>
historické	historický	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
jihozápadní	jihozápadní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
Kroměříž	Kroměříž	k1gFnSc1
a	a	k8xC
větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
Jihlavy	Jihlava	k1gFnSc2
a	a	k8xC
část	část	k1gFnSc1
Ostravy	Ostrava	k1gFnSc2
(	(	kIx(
<g/>
obě	dva	k4xCgNnPc4
města	město	k1gNnPc4
leží	ležet	k5eAaImIp3nP
na	na	k7c6
bývalých	bývalý	k2eAgFnPc6d1
zemských	zemský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Většinu	většina	k1gFnSc4
území	území	k1gNnSc2
Moravy	Morava	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
pahorkatiny	pahorkatina	k1gFnPc1
a	a	k8xC
vrchoviny	vrchovina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc4d3
místo	místo	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
poblíž	poblíž	k7c2
vrcholu	vrchol	k1gInSc2
hory	hora	k1gFnSc2
Praděd	praděd	k1gMnSc1
(	(	kIx(
<g/>
1491	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Hrubý	hrubý	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgMnPc3d1
významným	významný	k2eAgMnPc3d1
pohořím	pohoří	k1gNnSc7
patří	patřit	k5eAaImIp3nS
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
,	,	kIx,
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
Beskydy	Beskyd	k1gInPc1
a	a	k8xC
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
Čech	Čechy	k1gFnPc2
je	být	k5eAaImIp3nS
Morava	Morava	k1gFnSc1
oddělena	oddělen	k2eAgFnSc1d1
Českomoravskou	českomoravský	k2eAgFnSc7d1
vrchovinou	vrchovina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitějšími	důležitý	k2eAgFnPc7d3
řekami	řeka	k1gFnPc7
jsou	být	k5eAaImIp3nP
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
Dyje	Dyje	k1gFnSc1
<g/>
,	,	kIx,
Svratka	Svratka	k1gFnSc1
<g/>
,	,	kIx,
Bečva	Bečva	k1gFnSc1
a	a	k8xC
Jihlava	Jihlava	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
protékají	protékat	k5eAaImIp3nP
rozsáhlými	rozsáhlý	k2eAgFnPc7d1
nížinnými	nížinný	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
střední	střední	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
jimiž	jenž	k3xRgFnPc7
jsou	být	k5eAaImIp3nP
Dyjsko-svratecký	dyjsko-svratecký	k2eAgInSc1d1
úval	úval	k1gInSc1
<g/>
,	,	kIx,
Dolnomoravský	dolnomoravský	k2eAgInSc1d1
úval	úval	k1gInSc1
a	a	k8xC
Hornomoravský	hornomoravský	k2eAgInSc1d1
úval	úval	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
příchodem	příchod	k1gInSc7
Slovanů	Slovan	k1gMnPc2
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
žily	žít	k5eAaImAgFnP
na	na	k7c6
Moravě	Morava	k1gFnSc6
keltské	keltský	k2eAgFnSc6d1
a	a	k8xC
později	pozdě	k6eAd2
germánské	germánský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
833	#num#	k4
až	až	k9
906	#num#	k4
<g/>
/	/	kIx~
<g/>
907	#num#	k4
tvořila	tvořit	k5eAaImAgFnS
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
Moravy	Morava	k1gFnSc2
spolu	spolu	k6eAd1
s	s	k7c7
přilehlými	přilehlý	k2eAgNnPc7d1
územími	území	k1gNnPc7
dnešního	dnešní	k2eAgNnSc2d1
Slovenska	Slovensko	k1gNnSc2
a	a	k8xC
Rakouska	Rakousko	k1gNnSc2
středisko	středisko	k1gNnSc1
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
vazalem	vazal	k1gMnSc7
Franské	franský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
panování	panování	k1gNnSc4
knížete	kníže	k1gMnSc2
Rostislava	Rostislav	k1gMnSc2
(	(	kIx(
<g/>
846	#num#	k4
<g/>
–	–	k?
<g/>
870	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
stala	stát	k5eAaPmAgFnS
kolébkou	kolébka	k1gFnSc7
slovanské	slovanský	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
křesťanství	křesťanství	k1gNnSc2
<g/>
,	,	kIx,
začátkem	začátkem	k7c2
10	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Morava	Morava	k1gFnSc1
pak	pak	k6eAd1
zřejmě	zřejmě	k6eAd1
byla	být	k5eAaImAgFnS
krátce	krátce	k6eAd1
součástí	součást	k1gFnSc7
Polska	Polsko	k1gNnSc2
či	či	k8xC
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1019	#num#	k4
ji	on	k3xPp3gFnSc4
získal	získat	k5eAaPmAgMnS
český	český	k2eAgMnSc1d1
přemyslovský	přemyslovský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Oldřich	Oldřich	k1gMnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
až	až	k9
dodnes	dodnes	k6eAd1
zůstávají	zůstávat	k5eAaImIp3nP
politické	politický	k2eAgInPc4d1
osudy	osud	k1gInPc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Čech	Čechy	k1gFnPc2
spojeny	spojit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
11	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
konce	konec	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
Morava	Morava	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
údělná	údělný	k2eAgNnPc4d1
knížectví	knížectví	k1gNnPc4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
držení	držení	k1gNnSc6
členů	člen	k1gMnPc2
rodu	rod	k1gInSc2
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1061	#num#	k4
dědičném	dědičný	k2eAgNnSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
moravské	moravský	k2eAgInPc4d1
úděly	úděl	k1gInPc4
sjednotil	sjednotit	k5eAaPmAgInS
v	v	k7c4
Moravské	moravský	k2eAgNnSc4d1
markrabství	markrabství	k1gNnSc4
pozdější	pozdní	k2eAgMnSc1d2
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Státoprávní	státoprávní	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
upřesnil	upřesnit	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
listinou	listina	k1gFnSc7
ze	z	k7c2
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1348	#num#	k4
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
stanovil	stanovit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
země	zem	k1gFnPc1
řízené	řízený	k2eAgFnPc1d1
českým	český	k2eAgMnPc3d1
králem	král	k1gMnSc7
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gNnPc7
vlastními	vlastní	k2eAgNnPc7d1
lény	léno	k1gNnPc7
<g/>
,	,	kIx,
a	a	k8xC
nikoliv	nikoliv	k9
lény	léno	k1gNnPc7
Svaté	svatá	k1gFnSc2
říše	říš	k1gFnSc2
římské	římský	k2eAgFnPc4d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
Moravu	Morava	k1gFnSc4
určil	určit	k5eAaPmAgMnS
jako	jako	k8xS,k8xC
jednu	jeden	k4xCgFnSc4
ze	z	k7c2
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1867	#num#	k4
až	až	k9
1918	#num#	k4
země	zem	k1gFnPc4
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnPc1d1
spadaly	spadat	k5eAaPmAgFnP,k5eAaImAgFnP
pod	pod	k7c4
Předlitavsko	Předlitavsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
českém	český	k2eAgNnSc6d1
národním	národní	k2eAgNnSc6d1
obrození	obrození	k1gNnSc6
a	a	k8xC
následujícím	následující	k2eAgInSc6d1
boji	boj	k1gInSc6
o	o	k7c4
české	český	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
sebeurčení	sebeurčení	k1gNnSc4
se	se	k3xPyFc4
prosadila	prosadit	k5eAaPmAgFnS
myšlenka	myšlenka	k1gFnSc1
jednotného	jednotný	k2eAgInSc2d1
a	a	k8xC
jediný	jediný	k2eAgInSc1d1
spisovný	spisovný	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
používajícího	používající	k2eAgInSc2d1
„	„	k?
<g/>
národu	národ	k1gInSc2
českého	český	k2eAgInSc2d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
v	v	k7c6
Moravě	Morava	k1gFnSc6
<g/>
“	“	k?
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
to	ten	k3xDgNnSc4
formuloval	formulovat	k5eAaImAgInS
z	z	k7c2
Moravy	Morava	k1gFnSc2
pocházející	pocházející	k2eAgMnPc1d1
obrozenec	obrozenec	k1gMnSc1
František	František	k1gMnSc1
Palacký	Palacký	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
zánikem	zánik	k1gInSc7
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
zaniklo	zaniknout	k5eAaPmAgNnS
i	i	k9
markrabství	markrabství	k1gNnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
Morava	Morava	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
jako	jako	k9
země	země	k1gFnSc1
Moravská	moravský	k2eAgFnSc1d1
i	i	k9
nadále	nadále	k6eAd1
správním	správní	k2eAgInSc7d1
celkem	celek	k1gInSc7
v	v	k7c6
rámci	rámec	k1gInSc6
nově	nově	k6eAd1
vzniklého	vzniklý	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1928	#num#	k4
byla	být	k5eAaImAgFnS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
československou	československý	k2eAgFnSc7d1
částí	část	k1gFnSc7
Slezska	Slezsko	k1gNnSc2
do	do	k7c2
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
existovala	existovat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
se	se	k3xPyFc4
území	území	k1gNnSc1
Československa	Československo	k1gNnSc2
rozdělilo	rozdělit	k5eAaPmAgNnS
na	na	k7c4
kraje	kraj	k1gInPc4
a	a	k8xC
Morava	Morava	k1gFnSc1
jako	jako	k8xC,k8xS
správní	správní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
vnímána	vnímat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
svébytný	svébytný	k2eAgInSc4d1
kulturní	kulturní	k2eAgInSc4d1
celek	celek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
poválečném	poválečný	k2eAgNnSc6d1
vysídlení	vysídlení	k1gNnSc6
Moravanů	Moravan	k1gMnPc2
německé	německý	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
v	v	k7c6
některých	některý	k3yIgFnPc6
částech	část	k1gFnPc6
Moravy	Morava	k1gFnSc2
převažovali	převažovat	k5eAaImAgMnP
<g/>
,	,	kIx,
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
obyvatelstvo	obyvatelstvo	k1gNnSc1
většinově	většinově	k6eAd1
mluvící	mluvící	k2eAgMnSc1d1
českými	český	k2eAgFnPc7d1
nářečími	nářečí	k1gNnPc7
a	a	k8xC
hlásící	hlásící	k2eAgFnSc1d1
se	se	k3xPyFc4
k	k	k7c3
české	český	k2eAgFnSc3d1
nebo	nebo	k8xC
méně	málo	k6eAd2
často	často	k6eAd1
k	k	k7c3
moravské	moravský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravané	Moravan	k1gMnPc1
se	se	k3xPyFc4
etnograficky	etnograficky	k6eAd1
a	a	k8xC
lingvisticky	lingvisticky	k6eAd1
dělí	dělit	k5eAaImIp3nS
do	do	k7c2
tradičních	tradiční	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgMnPc3
patří	patřit	k5eAaImIp3nS
Haná	Haná	k1gFnSc1
<g/>
,	,	kIx,
Valašsko	Valašsko	k1gNnSc1
<g/>
,	,	kIx,
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
Horácko	Horácko	k1gNnSc1
a	a	k8xC
Lašsko	Lašsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Území	území	k1gNnSc1
</s>
<s>
S	s	k7c7
historickým	historický	k2eAgNnSc7d1
územím	území	k1gNnSc7
Moravy	Morava	k1gFnSc2
hraničí	hraničit	k5eAaImIp3nS
na	na	k7c6
západě	západ	k1gInSc6
Čechy	Čechy	k1gFnPc1
(	(	kIx(
<g/>
420	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
rakouská	rakouský	k2eAgFnSc1d1
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
Dolní	dolní	k2eAgInPc1d1
Rakousy	Rakousy	k1gInPc7
(	(	kIx(
<g/>
201,24	201,24	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
pak	pak	k6eAd1
slovenský	slovenský	k2eAgInSc1d1
Trnavský	trnavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
slovenský	slovenský	k2eAgInSc1d1
Trenčínský	trenčínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
severovýchodě	severovýchod	k1gInSc6
slovenský	slovenský	k2eAgInSc1d1
Žilinský	žilinský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
a	a	k8xC
České	český	k2eAgNnSc1d1
Slezsko	Slezsko	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
severu	sever	k1gInSc6
také	také	k9
České	český	k2eAgNnSc1d1
Slezsko	Slezsko	k1gNnSc1
a	a	k8xC
na	na	k7c6
severozápadě	severozápad	k1gInSc6
pak	pak	k6eAd1
území	území	k1gNnSc1
Polska	Polsko	k1gNnSc2
–	–	k?
konkrétně	konkrétně	k6eAd1
Kladsko	Kladsko	k1gNnSc1
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
součást	součást	k1gFnSc4
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
dnes	dnes	k6eAd1
část	část	k1gFnSc1
Dolnoslezského	dolnoslezský	k2eAgNnSc2d1
vojvodství	vojvodství	k1gNnSc2
(	(	kIx(
<g/>
20	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdejší	někdejší	k2eAgFnPc1d1
moravské	moravský	k2eAgFnPc1d1
enklávy	enkláva	k1gFnPc1
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
polským	polský	k2eAgNnSc7d1
Opolským	opolský	k2eAgNnSc7d1
vojvodstvím	vojvodství	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
leží	ležet	k5eAaImIp3nS
téměř	téměř	k6eAd1
celé	celý	k2eAgNnSc1d1
v	v	k7c6
polské	polský	k2eAgFnSc6d1
části	část	k1gFnSc6
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Morava	Morava	k1gFnSc1
měla	mít	k5eAaImAgFnS
po	po	k7c6
staletí	staletí	k1gNnSc6
stálé	stálý	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
měnily	měnit	k5eAaImAgFnP
spíše	spíše	k9
ojediněle	ojediněle	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
k	k	k7c3
Moravě	Morava	k1gFnSc3
připojeny	připojit	k5eAaPmNgFnP
na	na	k7c6
základě	základ	k1gInSc6
Saint-germainské	Saint-germainský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
oblasti	oblast	k1gFnSc2
Valticka	Valticko	k1gNnSc2
a	a	k8xC
Dyjského	dyjský	k2eAgInSc2d1
trojúhelníku	trojúhelník	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
tzv.	tzv.	kA
česká	český	k2eAgFnSc1d1
část	část	k1gFnSc1
Dolních	dolní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
Moravy	Morava	k1gFnSc2
v	v	k7c6
rozsahu	rozsah	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
)	)	kIx)
rozděleno	rozdělit	k5eAaPmNgNnS
mezi	mezi	k7c7
kraje	kraj	k1gInSc2
Jihomoravský	jihomoravský	k2eAgInSc1d1
(	(	kIx(
<g/>
celý	celý	k2eAgInSc1d1
kraj	kraj	k1gInSc1
až	až	k9
na	na	k7c4
českou	český	k2eAgFnSc4d1
ves	ves	k1gFnSc4
Jobova	Jobův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
a	a	k8xC
výše	vysoce	k6eAd2
zmíněná	zmíněný	k2eAgNnPc1d1
dolnorakouská	dolnorakouský	k2eAgNnPc1d1
území	území	k1gNnPc1
na	na	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Moravskoslezský	moravskoslezský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Osoblažsko	Osoblažsko	k1gNnSc1
<g/>
,	,	kIx,
okolí	okolí	k1gNnSc1
Rýmařova	rýmařův	k2eAgNnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Nového	Nového	k2eAgInSc2d1
Jičína	Jičín	k1gInSc2
a	a	k8xC
Frenštátu	Frenštát	k1gInSc2
pod	pod	k7c7
Radhoštěm	Radhošť	k1gInSc7
<g/>
;	;	kIx,
dále	daleko	k6eAd2
část	část	k1gFnSc1
Ostravy	Ostrava	k1gFnSc2
a	a	k8xC
Místek	Místek	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
téměř	téměř	k6eAd1
celého	celý	k2eAgInSc2d1
okresu	okres	k1gInSc2
Jeseník	Jeseník	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Vysočinu	vysočina	k1gFnSc4
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
severní	severní	k2eAgFnSc2d1
a	a	k8xC
západní	západní	k2eAgFnSc2d1
části	část	k1gFnSc2
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Zlínský	zlínský	k2eAgMnSc1d1
(	(	kIx(
<g/>
celý	celý	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jihočeský	jihočeský	k2eAgMnSc1d1
(	(	kIx(
<g/>
okolí	okolí	k1gNnSc1
Dačic	Dačice	k1gFnPc2
a	a	k8xC
Slavonic	Slavonice	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Pardubický	pardubický	k2eAgInSc1d1
(	(	kIx(
<g/>
zejména	zejména	k9
okolí	okolí	k1gNnSc1
Svitav	Svitava	k1gFnPc2
a	a	k8xC
Moravské	moravský	k2eAgFnSc2d1
Třebové	Třebová	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
část	část	k1gFnSc4
podhůří	podhůří	k1gNnSc2
Králického	králický	k2eAgInSc2d1
Sněžníku	Sněžník	k1gInSc2
–	–	k?
okolí	okolí	k1gNnSc2
Červené	Červené	k2eAgFnSc2d1
Vody	voda	k1gFnSc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
Lanškrounska	Lanškrounsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
historickém	historický	k2eAgNnSc6d1
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
leží	ležet	k5eAaImIp3nS
i	i	k9
osada	osada	k1gFnSc1
U	u	k7c2
Sabotů	Sabot	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
některými	některý	k3yIgInPc7
dalšími	další	k2eAgInPc7d1
neobydlenými	obydlený	k2eNgInPc7d1
pozemky	pozemek	k1gInPc7
<g/>
,	,	kIx,
oddělena	oddělen	k2eAgMnSc4d1
od	od	k7c2
zbytku	zbytek	k1gInSc2
Moravy	Morava	k1gFnSc2
úpravou	úprava	k1gFnSc7
česko-slovenské	česko-slovenský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1997	#num#	k4
patří	patřit	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
osada	osada	k1gFnSc1
Slovensku	Slovensko	k1gNnSc6
a	a	k8xC
od	od	k7c2
září	září	k1gNnSc2
1998	#num#	k4
nese	nést	k5eAaImIp3nS
jako	jako	k9
místní	místní	k2eAgFnSc4d1
část	část	k1gFnSc4
obce	obec	k1gFnSc2
Vrbovce	vrbovka	k1gFnSc3
název	název	k1gInSc4
Šance	šance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
Moravy	Morava	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Země	země	k1gFnSc1
v	v	k7c6
Rakousku-Uhersku	Rakousku-Uhersko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1914	#num#	k4
<g/>
(	(	kIx(
<g/>
č.	č.	k?
9	#num#	k4
<g/>
:	:	kIx,
Morava	Morava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Země	země	k1gFnSc1
první	první	k4xOgFnSc2
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
do	do	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
</s>
<s>
Uspořádání	uspořádání	k1gNnSc1
moderních	moderní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
krajů	kraj	k1gInPc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
historickým	historický	k2eAgFnPc3d1
zemím	zem	k1gFnPc3
Koruny	koruna	k1gFnSc2
české	český	k2eAgMnPc4d1
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
moravsko-slezské	moravsko-slezský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
</s>
<s>
Významná	významný	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
<g/>
,	,	kIx,
dominanta	dominanta	k1gFnSc1
Brna	Brno	k1gNnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hrad	hrad	k1gInSc1
Špilberk	Špilberk	k1gInSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
bývalé	bývalý	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
moravských	moravský	k2eAgNnPc2d1
markrabat	markrabě	k1gNnPc2
</s>
<s>
Krystalizační	krystalizační	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
moravského	moravský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
prvních	první	k4xOgMnPc2
Mojmírovců	Mojmírovec	k1gMnPc2
v	v	k7c6
době	doba	k1gFnSc6
tzv.	tzv.	kA
"	"	kIx"
<g/>
hrubého	hrubý	k2eAgNnSc2d1
křesťanství	křesťanství	k1gNnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tedy	tedy	k9
především	především	k9
pro	pro	k7c4
nobilitu	nobilita	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
hlavní	hlavní	k2eAgNnSc4d1
slovo	slovo	k1gNnSc4
měl	mít	k5eAaImAgInS
pasovský	pasovský	k2eAgInSc1d1
episkopát	episkopát	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
nacházelo	nacházet	k5eAaImAgNnS
nejspíš	nejspíš	k9
v	v	k7c6
lokalitě	lokalita	k1gFnSc6
Valy	Vala	k1gMnSc2
u	u	k7c2
Mikulčic	Mikulčice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Sídlo	sídlo	k1gNnSc4
jak	jak	k6eAd1
velkomoravského	velkomoravský	k2eAgMnSc2d1
vládce	vládce	k1gMnSc2
tak	tak	k8xC,k8xS
arcibiskupa	arcibiskup	k1gMnSc2
diecéze	diecéze	k1gFnSc2
moravské	moravský	k2eAgFnSc2d1
v	v	k7c6
raném	raný	k2eAgInSc6d1
středověku	středověk	k1gInSc6
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
Veligrad	Veligrad	k1gInSc1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
místě	místo	k1gNnSc6
Starého	Starého	k2eAgNnSc2d1
Města	město	k1gNnSc2
a	a	k8xC
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejdůležitějšími	důležitý	k2eAgInPc7d3
centry	centr	k1gInPc7
Moravy	Morava	k1gFnSc2
vrcholného	vrcholný	k2eAgInSc2d1
středověku	středověk	k1gInSc2
byly	být	k5eAaImAgFnP
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
a	a	k8xC
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
sídelní	sídelní	k2eAgNnPc4d1
města	město	k1gNnPc4
moravských	moravský	k2eAgInPc2d1
údělů	úděl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
ve	v	k7c6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
Morava	Morava	k1gFnSc1
řízena	řídit	k5eAaImNgFnS
z	z	k7c2
Olomouce	Olomouc	k1gFnSc2
a	a	k8xC
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biskupství	biskupství	k1gNnSc1
sídlilo	sídlit	k5eAaImAgNnS
od	od	k7c2
roku	rok	k1gInSc2
1063	#num#	k4
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
bývá	bývat	k5eAaImIp3nS
proto	proto	k8xC
považována	považován	k2eAgFnSc1d1
za	za	k7c4
kulturní	kulturní	k2eAgFnSc4d1
a	a	k8xC
duchovní	duchovní	k2eAgFnSc4d1
metropoli	metropole	k1gFnSc4
tehdejší	tehdejší	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
a	a	k8xC
zemský	zemský	k2eAgInSc1d1
soud	soud	k1gInSc1
střídavě	střídavě	k6eAd1
zasedaly	zasedat	k5eAaImAgInP
v	v	k7c6
obou	dva	k4xCgNnPc6
městech	město	k1gNnPc6
<g/>
,	,	kIx,
a	a	k8xC
kvůli	kvůli	k7c3
tomu	ten	k3xDgInSc3
byly	být	k5eAaImAgFnP
i	i	k9
pravidelně	pravidelně	k6eAd1
přemisťovány	přemisťován	k2eAgFnPc4d1
moravské	moravský	k2eAgFnPc4d1
zemské	zemský	k2eAgFnPc4d1
desky	deska	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
Jana	Jan	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
ve	v	k7c4
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
Brno	Brno	k1gNnSc1
navíc	navíc	k6eAd1
stalo	stát	k5eAaPmAgNnS
trvalým	trvalý	k2eAgInSc7d1
sídlem	sídlo	k1gNnSc7
tehdejších	tehdejší	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
moravských	moravský	k2eAgNnPc2d1
markrabat	markrabě	k1gNnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
sídlem	sídlo	k1gNnSc7
byl	být	k5eAaImAgInS
hrad	hrad	k1gInSc1
Špilberk	Špilberk	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brno	Brno	k1gNnSc1
bylo	být	k5eAaImAgNnS
hlavním	hlavní	k2eAgInSc7d1
centrem	centr	k1gInSc7
Moravy	Morava	k1gFnSc2
především	především	k6eAd1
za	za	k7c2
vlády	vláda	k1gFnSc2
Jošta	Jošto	k1gNnSc2
Moravského	moravský	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
lze	lze	k6eAd1
mluvit	mluvit	k5eAaImF
o	o	k7c6
vyrovnaném	vyrovnaný	k2eAgInSc6d1
významu	význam	k1gInSc6
obou	dva	k4xCgInPc2
měst	město	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
poloviny	polovina	k1gFnSc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1573	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
založena	založit	k5eAaPmNgFnS
první	první	k4xOgFnSc4
a	a	k8xC
na	na	k7c4
několik	několik	k4yIc4
dalších	další	k2eAgNnPc2d1
století	století	k1gNnPc2
i	i	k8xC
jediná	jediný	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
krátký	krátký	k2eAgInSc4d1
čas	čas	k1gInSc4
přesunuta	přesunout	k5eAaPmNgFnS
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1636	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zřízení	zřízení	k1gNnSc3
královského	královský	k2eAgInSc2d1
tribunálu	tribunál	k1gInSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
významného	významný	k2eAgInSc2d1
zeměpanského	zeměpanský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
s	s	k7c7
rozsáhlými	rozsáhlý	k2eAgFnPc7d1
správními	správní	k2eAgFnPc7d1
a	a	k8xC
soudními	soudní	k2eAgFnPc7d1
pravomocemi	pravomoc	k1gFnPc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
poté	poté	k6eAd1
na	na	k7c4
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
sídlil	sídlit	k5eAaImAgInS
i	i	k9
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Brna	Brno	k1gNnSc2
se	se	k3xPyFc4
definitivně	definitivně	k6eAd1
přestěhovaly	přestěhovat	k5eAaPmAgFnP
celé	celá	k1gFnPc1
zemské	zemský	k2eAgFnSc2d1
desky	deska	k1gFnSc2
(	(	kIx(
<g/>
předtím	předtím	k6eAd1
přibližně	přibližně	k6eAd1
polovina	polovina	k1gFnSc1
vedena	veden	k2eAgFnSc1d1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
a	a	k8xC
druhá	druhý	k4xOgFnSc1
polovina	polovina	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
)	)	kIx)
i	i	k9
s	s	k7c7
královským	královský	k2eAgInSc7d1
tribunálem	tribunál	k1gInSc7
a	a	k8xC
bylo	být	k5eAaImAgNnS
ukončeno	ukončit	k5eAaPmNgNnS
střídavé	střídavý	k2eAgNnSc1d1
zasedání	zasedání	k1gNnSc1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
a	a	k8xC
soudu	soud	k1gInSc2
až	až	k9
na	na	k7c4
příkaz	příkaz	k1gInSc4
markraběte	markrabě	k1gMnSc2
a	a	k8xC
císaře	císař	k1gMnSc2
Ferdinanda	Ferdinand	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
za	za	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1641	#num#	k4
až	až	k9
1642	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Navíc	navíc	k6eAd1
roku	rok	k1gInSc2
1642	#num#	k4
se	se	k3xPyFc4
Olomouc	Olomouc	k1gFnSc1
po	po	k7c6
40	#num#	k4
dnech	den	k1gInPc6
obléhání	obléhání	k1gNnSc6
vzdala	vzdát	k5eAaPmAgFnS
Švédům	Švéd	k1gMnPc3
a	a	k8xC
byla	být	k5eAaImAgFnS
válkou	válka	k1gFnSc7
značně	značně	k6eAd1
zdevastována	zdevastován	k2eAgFnSc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Brno	Brno	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k9
fakticky	fakticky	k6eAd1
stalo	stát	k5eAaPmAgNnS
nedůležitějším	důležitý	k2eNgNnSc7d2
městem	město	k1gNnSc7
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1749	#num#	k4
byl	být	k5eAaImAgMnS
v	v	k7c6
Brně	Brno	k1gNnSc6
založen	založit	k5eAaPmNgInS
moravský	moravský	k2eAgInSc1d1
soudní	soudní	k2eAgInSc1d1
a	a	k8xC
politický	politický	k2eAgInSc1d1
senát	senát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěž	soutěž	k1gFnSc1
obou	dva	k4xCgNnPc2
měst	město	k1gNnPc2
ovšem	ovšem	k9
stále	stále	k6eAd1
pokračovala	pokračovat	k5eAaImAgFnS
a	a	k8xC
Olomouc	Olomouc	k1gFnSc1
několikrát	několikrát	k6eAd1
žádala	žádat	k5eAaImAgFnS
navrácení	navrácení	k1gNnSc4
svého	svůj	k3xOyFgNnSc2
původního	původní	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
navrácení	navrácení	k1gNnSc2
královského	královský	k2eAgInSc2d1
tribunálu	tribunál	k1gInSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
zřízen	zřídit	k5eAaPmNgInS
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
sídlil	sídlit	k5eAaImAgInS
jen	jen	k9
krátkou	krátký	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
královna	královna	k1gFnSc1
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
odražení	odražení	k1gNnSc6
pruské	pruský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
udělila	udělit	k5eAaPmAgFnS
Olomouci	Olomouc	k1gFnSc6
formální	formální	k2eAgInSc4d1
titul	titul	k1gInSc4
„	„	k?
<g/>
Královské	královský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Dlouhý	dlouhý	k2eAgInSc1d1
spor	spor	k1gInSc1
obou	dva	k4xCgNnPc2
měst	město	k1gNnPc2
o	o	k7c4
přednostní	přednostní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
byl	být	k5eAaImAgMnS
ukončen	ukončit	k5eAaPmNgMnS
Josefem	Josef	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
roce	rok	k1gInSc6
1782	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
panovník	panovník	k1gMnSc1
přiznal	přiznat	k5eAaPmAgMnS
Brnu	Brno	k1gNnSc6
nárok	nárok	k1gInSc1
být	být	k5eAaImF
definitivně	definitivně	k6eAd1
jediným	jediný	k2eAgNnSc7d1
ohniskem	ohnisko	k1gNnSc7
politické	politický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
roku	rok	k1gInSc3
1849	#num#	k4
potvrzeno	potvrdit	k5eAaPmNgNnS
Moravskou	moravský	k2eAgFnSc7d1
zemskou	zemský	k2eAgFnSc7d1
ústavou	ústava	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
Brno	Brno	k1gNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
hlavní	hlavní	k2eAgNnSc1d1
městem	město	k1gNnSc7
země	zem	k1gFnSc2
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
zemské	zemský	k2eAgNnSc1d1
územní	územní	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
zrušeno	zrušit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Města	město	k1gNnPc1
s	s	k7c7
nejvyšším	vysoký	k2eAgInSc7d3
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
měst	město	k1gNnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
Horní	horní	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
s	s	k7c7
radnicí	radnice	k1gFnSc7
a	a	k8xC
sloupem	sloup	k1gInSc7
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
</s>
<s>
Deseti	deset	k4xCc7
největšími	veliký	k2eAgNnPc7d3
městy	město	k1gNnPc7
Moravy	Morava	k1gFnSc2
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
jsou	být	k5eAaImIp3nP
(	(	kIx(
<g/>
podle	podle	k7c2
stavu	stav	k1gInSc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Brno	Brno	k1gNnSc1
–	–	k?
380	#num#	k4
681	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
–	–	k?
289	#num#	k4
128	#num#	k4
(	(	kIx(
<g/>
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
historické	historický	k2eAgFnSc2d1
moravsko-slezské	moravsko-slezský	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
části	část	k1gFnSc3
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
a	a	k8xC
jiné	jiný	k2eAgFnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
–	–	k?
100	#num#	k4
523	#num#	k4
</s>
<s>
Zlín	Zlín	k1gInSc1
–	–	k?
74	#num#	k4
997	#num#	k4
</s>
<s>
Frýdek-Místek	Frýdek-Místek	k1gInSc1
–	–	k?
55	#num#	k4
931	#num#	k4
(	(	kIx(
<g/>
rozkládá	rozkládat	k5eAaImIp3nS
se	se	k3xPyFc4
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
historické	historický	k2eAgFnSc2d1
moravsko-slezské	moravsko-slezský	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Místek	Místek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
–	–	k?
50	#num#	k4
845	#num#	k4
(	(	kIx(
<g/>
původně	původně	k6eAd1
ležící	ležící	k2eAgFnSc1d1
jen	jen	k9
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
rozkládající	rozkládající	k2eAgMnSc1d1
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
historické	historický	k2eAgFnSc2d1
moravsko-české	moravsko-český	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Prostějov	Prostějov	k1gInSc1
–	–	k?
43	#num#	k4
680	#num#	k4
</s>
<s>
Přerov	Přerov	k1gInSc1
–	–	k?
43	#num#	k4
186	#num#	k4
</s>
<s>
Třebíč	Třebíč	k1gFnSc1
–	–	k?
35	#num#	k4
691	#num#	k4
</s>
<s>
Znojmo	Znojmo	k1gNnSc1
–	–	k?
33	#num#	k4
780	#num#	k4
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1
významná	významný	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgInPc4d1
historicky	historicky	k6eAd1
významná	významný	k2eAgNnPc4d1
města	město	k1gNnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Slavkov	Slavkov	k1gInSc1
jako	jako	k8xS,k8xC
dějiště	dějiště	k1gNnSc1
bitvy	bitva	k1gFnSc2
u	u	k7c2
Slavkova	Slavkov	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Kroměříž	Kroměříž	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
„	„	k?
<g/>
hanácké	hanácký	k2eAgFnPc1d1
Atény	Atény	k1gFnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
zasedal	zasedat	k5eAaImAgInS
ústavodárný	ústavodárný	k2eAgInSc1d1
říšský	říšský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
a	a	k8xC
město	město	k1gNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
na	na	k7c4
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
nejvýznamnějším	významný	k2eAgNnSc7d3
místem	místo	k1gNnSc7
rakouské	rakouský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
centrum	centrum	k1gNnSc1
Moravského	moravský	k2eAgNnSc2d1
Slovácka	Slovácko	k1gNnSc2
s	s	k7c7
významnou	významný	k2eAgFnSc7d1
velkomoravskou	velkomoravský	k2eAgFnSc7d1
archeologickou	archeologický	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Novém	nový	k2eAgInSc6d1
Jičíně	Jičín	k1gInSc6
roku	rok	k1gInSc2
1790	#num#	k4
skonal	skonat	k5eAaPmAgMnS
Ernst	Ernst	k1gMnSc1
Gideon	Gideon	k1gMnSc1
von	von	k1gInSc4
Laudon	Laudon	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
největších	veliký	k2eAgMnPc2d3
vojevůdců	vojevůdce	k1gMnPc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
moravských	moravský	k2eAgNnPc6d1
městech	město	k1gNnPc6
je	být	k5eAaImIp3nS
také	také	k9
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
významných	významný	k2eAgFnPc2d1
památek	památka	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
řady	řada	k1gFnSc2
památek	památka	k1gFnPc2
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc3
UNESCO	Unesco	k1gNnSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Kroměříž	Kroměříž	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Telč	Telč	k1gFnSc1
<g/>
,	,	kIx,
Třebíč	Třebíč	k1gFnSc1
<g/>
,	,	kIx,
Valtice	Valtice	k1gFnPc1
<g/>
,	,	kIx,
Žďár	Žďár	k1gInSc1
nad	nad	k7c7
Sázavou	Sázava	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
</s>
<s>
Povrch	povrch	k1gInSc1
Moravy	Morava	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
pahorkatiny	pahorkatina	k1gFnSc2
<g/>
,	,	kIx,
vrchoviny	vrchovina	k1gFnSc2
a	a	k8xC
ne	ne	k9
příliš	příliš	k6eAd1
vysoká	vysoká	k1gFnSc1
pohoří	pohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Čechami	Čechy	k1gFnPc7
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
Českomoravská	českomoravský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
je	být	k5eAaImIp3nS
i	i	k9
masiv	masiv	k1gInSc1
Žďárských	Žďárských	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východně	východně	k6eAd1
od	od	k7c2
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
nachází	nacházet	k5eAaImIp3nS
Drahanská	Drahanský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
s	s	k7c7
Moravským	moravský	k2eAgInSc7d1
krasem	kras	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
Moravy	Morava	k1gFnSc2
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
Slezska	Slezsko	k1gNnSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
rozkládá	rozkládat	k5eAaImIp3nS
pohoří	pohoří	k1gNnSc4
Hrubý	hrubý	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
s	s	k7c7
nejvyššími	vysoký	k2eAgFnPc7d3
moravskými	moravský	k2eAgFnPc7d1
horami	hora	k1gFnPc7
Pradědem	praděd	k1gMnSc7
(	(	kIx(
<g/>
1492	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Vysokou	vysoký	k2eAgFnSc7d1
holí	hole	k1gFnSc7
(	(	kIx(
<g/>
1465	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
se	se	k3xPyFc4
na	na	k7c6
hranici	hranice	k1gFnSc6
s	s	k7c7
Čechami	Čechy	k1gFnPc7
a	a	k8xC
Kladskem	Kladsko	k1gNnSc7
rozkládá	rozkládat	k5eAaImIp3nS
Králický	králický	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
Hrubého	Hrubého	k2eAgInSc2d1
Jeseníku	Jeseník	k1gInSc2
se	se	k3xPyFc4
zvedá	zvedat	k5eAaImIp3nS
Nízký	nízký	k2eAgInSc1d1
Jeseník	Jeseník	k1gInSc1
(	(	kIx(
<g/>
včetně	včetně	k7c2
Oderských	oderský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
a	a	k8xC
střední	střední	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
podél	podél	k7c2
řek	řeka	k1gFnPc2
rozkládá	rozkládat	k5eAaImIp3nS
několik	několik	k4yIc1
nížinatých	nížinatý	k2eAgInPc2d1
úvalů	úval	k1gInPc2
a	a	k8xC
„	„	k?
<g/>
bran	brána	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
oddělují	oddělovat	k5eAaImIp3nP
výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgNnPc1d1
pohoří	pohoří	k1gNnPc1
od	od	k7c2
pohoří	pohoří	k1gNnPc2
na	na	k7c6
východě	východ	k1gInSc6
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
jimiž	jenž	k3xRgInPc7
jsou	být	k5eAaImIp3nP
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
<g/>
,	,	kIx,
Javorníky	Javorník	k1gInPc1
<g/>
,	,	kIx,
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
Beskydy	Beskyd	k1gInPc1
<g/>
,	,	kIx,
Vsetínské	vsetínský	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
<g/>
,	,	kIx,
a	a	k8xC
západněji	západně	k6eAd2
položené	položený	k2eAgInPc1d1
Chřiby	Chřiby	k1gInPc1
a	a	k8xC
pahorkatina	pahorkatina	k1gFnSc1
Ždánický	ždánický	k2eAgInSc4d1
les	les	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
celou	celý	k2eAgFnSc7d1
zemí	zem	k1gFnPc2
protéká	protékat	k5eAaImIp3nS
od	od	k7c2
severu	sever	k1gInSc2
k	k	k7c3
jihu	jih	k1gInSc3
<g/>
,	,	kIx,
leží	ležet	k5eAaImIp3nS
Hornomoravský	hornomoravský	k2eAgInSc4d1
úval	úval	k1gInSc4
a	a	k8xC
Dolnomoravský	dolnomoravský	k2eAgInSc4d1
úval	úval	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Hornomoravský	hornomoravský	k2eAgInSc4d1
úval	úval	k1gInSc4
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
Moravská	moravský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
pak	pak	k6eAd1
Vyškovská	vyškovský	k2eAgFnSc1d1
brána	brána	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
níž	nízce	k6eAd2
na	na	k7c6
jihu	jih	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
Dyjsko-svratecký	dyjsko-svratecký	k2eAgInSc1d1
úval	úval	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
Mikulova	Mikulov	k1gInSc2
se	se	k3xPyFc4
zvedají	zvedat	k5eAaImIp3nP
Pavlovské	pavlovský	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
řece	řeka	k1gFnSc6
Moravě	Morava	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
nejnižší	nízký	k2eAgInSc1d3
bod	bod	k1gInSc1
celé	celý	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
148	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
na	na	k7c6
soutoku	soutok	k1gInSc6
s	s	k7c7
Dyjí	Dyje	k1gFnSc7
v	v	k7c6
nejjižnějším	jižní	k2eAgInSc6d3
výběžku	výběžek	k1gInSc6
na	na	k7c6
trojmezí	trojmezí	k1gNnSc6
se	s	k7c7
Slovenskem	Slovensko	k1gNnSc7
a	a	k8xC
Rakouskem	Rakousko	k1gNnSc7
(	(	kIx(
<g/>
Dolními	dolní	k2eAgInPc7d1
Rakousy	Rakousy	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
území	území	k1gNnSc2
Moravy	Morava	k1gFnSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
do	do	k7c2
povodí	povodí	k1gNnSc2
Dunaje	Dunaj	k1gInSc2
a	a	k8xC
úmoří	úmoří	k1gNnSc2
Černého	černé	k1gNnSc2
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
do	do	k7c2
povodí	povodí	k1gNnSc2
Odry	Odra	k1gFnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
úmoří	úmoří	k1gNnSc4
Baltu	Balt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdelší	dlouhý	k2eAgFnSc7d3
moravskou	moravský	k2eAgFnSc7d1
řekou	řeka	k1gFnSc7
je	být	k5eAaImIp3nS
právě	právě	k9
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
354	#num#	k4
km	km	kA
<g/>
;	;	kIx,
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
270	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
důležitým	důležitý	k2eAgFnPc3d1
řekám	řeka	k1gFnPc3
patří	patřit	k5eAaImIp3nS
Bečva	Bečva	k1gFnSc1
<g/>
,	,	kIx,
Dyje	Dyje	k1gFnSc1
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
resp.	resp.	kA
Dolních	dolní	k2eAgInPc6d1
Rakousích	Rakousy	k1gInPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jevišovka	Jevišovka	k1gFnSc1
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
<g/>
,	,	kIx,
Odra	Odra	k1gFnSc1
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Oslava	oslava	k1gFnSc1
<g/>
,	,	kIx,
Ostravice	Ostravice	k1gFnSc1
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Svitava	Svitava	k1gFnSc1
a	a	k8xC
Svratka	Svratka	k1gFnSc1
(	(	kIx(
<g/>
částečně	částečně	k6eAd1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Lidový	lidový	k2eAgInSc1d1
tanec	tanec	k1gInSc1
během	během	k7c2
jízdy	jízda	k1gFnSc2
králů	král	k1gMnPc2
ve	v	k7c6
Vlčnově	Vlčnov	k1gInSc6
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Moravu	Morava	k1gFnSc4
lze	lze	k6eAd1
kulturně	kulturně	k6eAd1
rozdělit	rozdělit	k5eAaPmF
na	na	k7c4
několik	několik	k4yIc4
národopisných	národopisný	k2eAgInPc2d1
regionů	region	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
lišit	lišit	k5eAaImF
svým	svůj	k3xOyFgNnSc7
nářečím	nářečí	k1gNnSc7
<g/>
,	,	kIx,
lidovými	lidový	k2eAgInPc7d1
zvyky	zvyk	k1gInPc7
<g/>
,	,	kIx,
kroji	kroj	k1gInPc7
apod.	apod.	kA
</s>
<s>
Ve	v	k7c6
samém	samý	k3xTgInSc6
středu	střed	k1gInSc6
Moravy	Morava	k1gFnSc2
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
Haná	Haná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
typická	typický	k2eAgFnSc1d1
rozlehlá	rozlehlý	k2eAgFnSc1d1
pole	pole	k1gFnSc1
v	v	k7c6
úrodných	úrodný	k2eAgInPc6d1
úvalech	úval	k1gInPc6
řek	řeka	k1gFnPc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
Bečvy	Bečva	k1gFnSc2
a	a	k8xC
Hané	Haná	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
poslední	poslední	k2eAgFnSc1d1
dala	dát	k5eAaPmAgFnS
regionu	region	k1gInSc2
i	i	k8xC
jeho	jeho	k3xOp3gNnSc4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haná	Haná	k1gFnSc1
je	být	k5eAaImIp3nS
také	také	k9
centrem	centr	k1gMnSc7
skupiny	skupina	k1gFnSc2
středomoravských	středomoravský	k2eAgNnPc2d1
nářečí	nářečí	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
dále	daleko	k6eAd2
například	například	k6eAd1
na	na	k7c6
Horácku	Horácko	k1gNnSc6
<g/>
,	,	kIx,
národopisném	národopisný	k2eAgInSc6d1
regionu	region	k1gInSc6
na	na	k7c6
západě	západ	k1gInSc6
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastní	vlastní	k2eAgInSc4d1
Horácko	Horácko	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
přirozenou	přirozený	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
českou	český	k2eAgFnSc7d1
a	a	k8xC
moravskou	moravský	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
a	a	k8xC
zaujímá	zaujímat	k5eAaImIp3nS
vpodstatě	vpodstatě	k9
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
se	se	k3xPyFc4
k	k	k7c3
němu	on	k3xPp3gMnSc3
řádí	řádit	k5eAaImIp3nP
i	i	k9
tzv.	tzv.	kA
Podhorácko	Podhorácko	k1gNnSc4
<g/>
,	,	kIx,
mající	mající	k2eAgInPc4d1
již	již	k9
prvky	prvek	k1gInPc4
bližší	blízký	k2eAgInPc4d2
jihu	jih	k1gInSc3
Moravy	Morava	k1gFnSc2
a	a	k8xC
Brněnsku	Brněnsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Východ	východ	k1gInSc1
Moravy	Morava	k1gFnSc2
je	být	k5eAaImIp3nS
reprezentován	reprezentován	k2eAgInSc1d1
převážně	převážně	k6eAd1
Slováckem	Slovácko	k1gNnSc7
<g/>
,	,	kIx,
regionem	region	k1gInSc7
s	s	k7c7
mnoha	mnoho	k4c7
přechodnými	přechodný	k2eAgInPc7d1
prvky	prvek	k1gInPc7
moravsko-slovenskými	moravsko-slovenský	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
oblast	oblast	k1gFnSc4
s	s	k7c7
mnoha	mnoho	k4c7
subregiony	subregion	k1gInPc7
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
různorodou	různorodý	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
a	a	k8xC
zachovalými	zachovalý	k2eAgInPc7d1
místními	místní	k2eAgInPc7d1
zvyky	zvyk	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
nářečí	nářečí	k1gNnPc4
užívá	užívat	k5eAaImIp3nS
i	i	k9
severnější	severní	k2eAgNnSc1d2
Valašsko	Valašsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
oblast	oblast	k1gFnSc4
osídlenou	osídlený	k2eAgFnSc4d1
kočovníky	kočovník	k1gMnPc4
z	z	k7c2
rumunského	rumunský	k2eAgNnSc2d1
Valašska	Valašsko	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
byli	být	k5eAaImAgMnP
postupně	postupně	k6eAd1
asimilováni	asimilovat	k5eAaBmNgMnP
slovanským	slovanský	k2eAgNnSc7d1
obyvatelstvem	obyvatelstvo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
pomezí	pomezí	k1gNnSc6
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
nevelké	velký	k2eNgNnSc1d1
Lašsko	Lašsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pro	pro	k7c4
ně	on	k3xPp3gNnSc4
typické	typický	k2eAgNnSc4d1
zajímavé	zajímavý	k2eAgNnSc4d1
jazykové	jazykový	k2eAgNnSc4d1
míšení	míšení	k1gNnSc4
moravských	moravský	k2eAgInPc2d1
dialektů	dialekt	k1gInPc2
a	a	k8xC
slezštiny	slezština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdejší	zdejší	k2eAgInSc1d1
folklór	folklór	k1gInSc1
inspiroval	inspirovat	k5eAaBmAgInS
v	v	k7c6
tvorbě	tvorba	k1gFnSc6
zdejšího	zdejší	k2eAgMnSc2d1
rodáka	rodák	k1gMnSc2
Leoše	Leoš	k1gMnSc2
Janáčka	Janáček	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Moravské	moravský	k2eAgNnSc1d1
víno	víno	k1gNnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Těžba	těžba	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1
ekonomická	ekonomický	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
Moravy	Morava	k1gFnSc2
jsou	být	k5eAaImIp3nP
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
a	a	k8xC
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
všech	všecek	k3xTgInPc6
čtyřech	čtyři	k4xCgInPc6
případech	případ	k1gInPc6
se	se	k3xPyFc4
dnes	dnes	k6eAd1
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
města	město	k1gNnPc4
s	s	k7c7
velkým	velký	k2eAgInSc7d1
počtem	počet	k1gInSc7
vysokoškolských	vysokoškolský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
a	a	k8xC
bohatou	bohatý	k2eAgFnSc7d1
minulostí	minulost	k1gFnSc7
ve	v	k7c6
zpracovatelském	zpracovatelský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
bylo	být	k5eAaImAgNnS
dříve	dříve	k6eAd2
známé	známá	k1gFnSc2
svým	svůj	k3xOyFgInSc7
rozvinutým	rozvinutý	k2eAgInSc7d1
textilním	textilní	k2eAgInSc7d1
průmyslem	průmysl	k1gInSc7
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
Ostravy	Ostrava	k1gFnSc2
šlo	jít	k5eAaImAgNnS
o	o	k7c4
silné	silný	k2eAgNnSc4d1
průmyslové	průmyslový	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
spíše	spíše	k9
na	na	k7c4
činnost	činnost	k1gFnSc4
ve	v	k7c6
vědě	věda	k1gFnSc6
<g/>
,	,	kIx,
výzkumu	výzkum	k1gInSc6
a	a	k8xC
vývoji	vývoj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
okolo	okolo	k7c2
Ostravy	Ostrava	k1gFnSc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
probíhala	probíhat	k5eAaImAgFnS
intenzivní	intenzivní	k2eAgFnSc1d1
těžba	těžba	k1gFnSc1
uhlí	uhlí	k1gNnSc2
až	až	k6eAd1
do	do	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
vybudovat	vybudovat	k5eAaPmF
masivní	masivní	k2eAgFnSc4d1
aglomeraci	aglomerace	k1gFnSc4
orientovanou	orientovaný	k2eAgFnSc4d1
rovněž	rovněž	k6eAd1
na	na	k7c4
zpracovatelský	zpracovatelský	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
město	město	k1gNnSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
snaží	snažit	k5eAaImIp3nP
přeorientovat	přeorientovat	k5eAaPmF
na	na	k7c4
jiná	jiný	k2eAgNnPc4d1
odvětví	odvětví	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
Moravy	Morava	k1gFnSc2
okolo	okolo	k7c2
Hodonína	Hodonín	k1gInSc2
a	a	k8xC
Břeclavi	Břeclav	k1gFnSc2
se	se	k3xPyFc4
těží	těžet	k5eAaImIp3nS
ropa	ropa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgNnSc1d1
zemědělství	zemědělství	k1gNnSc1
je	být	k5eAaImIp3nS
známé	známý	k1gMnPc4
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
produkci	produkce	k1gFnSc4
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
okolo	okolo	k7c2
96	#num#	k4
%	%	kIx~
vinic	vinice	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Demografie	demografie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Moravané	Moravan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručná	stručný	k2eAgFnSc1d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
Moravanech	Moravan	k1gMnPc6
pochází	pocházet	k5eAaImIp3nP
z	z	k7c2
Letopisů	letopis	k1gInPc2
království	království	k1gNnSc2
Franků	Franky	k1gInPc2
k	k	k7c3
roku	rok	k1gInSc3
822	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Mnoho	mnoho	k6eAd1
obyvatel	obyvatel	k1gMnSc1
Moravy	Morava	k1gFnSc2
dříve	dříve	k6eAd2
mluvilo	mluvit	k5eAaImAgNnS
německy	německy	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
většina	většina	k1gFnSc1
Němců	Němec	k1gMnPc2
byla	být	k5eAaImAgFnS
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
deportována	deportovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Německé	německý	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
převládalo	převládat	k5eAaImAgNnS
v	v	k7c6
Jeseníkách	Jeseníky	k1gInPc6
a	a	k8xC
ve	v	k7c6
větších	veliký	k2eAgNnPc6d2
městech	město	k1gNnPc6
jako	jako	k8xC,k8xS
Brno	Brno	k1gNnSc1
a	a	k8xC
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
obyvatelstvem	obyvatelstvo	k1gNnSc7
Čech	Čechy	k1gFnPc2
jsou	být	k5eAaImIp3nP
Moravané	Moravan	k1gMnPc1
výrazně	výrazně	k6eAd1
častěji	často	k6eAd2
věřící	věřící	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
Moravy	Morava	k1gFnSc2
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
k	k	k7c3
české	český	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
<g/>
,	,	kIx,
další	další	k2eAgFnSc1d1
část	část	k1gFnSc1
k	k	k7c3
moravské	moravský	k2eAgFnSc3d1
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
k	k	k7c3
moravské	moravský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
v	v	k7c6
Česku	Česko	k1gNnSc6
přihlásilo	přihlásit	k5eAaPmAgNnS
celkem	celkem	k6eAd1
630	#num#	k4
897	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spisovným	spisovný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
Moravanů	Moravan	k1gMnPc2
je	být	k5eAaImIp3nS
již	již	k6eAd1
od	od	k7c2
národního	národní	k2eAgNnSc2d1
obrození	obrození	k1gNnSc2
čeština	čeština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
moravských	moravský	k2eAgInPc6d1
regionech	region	k1gInPc6
se	se	k3xPyFc4
však	však	k9
stále	stále	k6eAd1
udržují	udržovat	k5eAaImIp3nP
místní	místní	k2eAgInPc4d1
různorodé	různorodý	k2eAgInPc4d1
dialekty	dialekt	k1gInPc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
označované	označovaný	k2eAgFnPc1d1
hromadně	hromadně	k6eAd1
jako	jako	k9
moravština	moravština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
té	ten	k3xDgFnSc3
se	se	k3xPyFc4
jako	jako	k9
k	k	k7c3
mateřskému	mateřský	k2eAgInSc3d1
jazyku	jazyk	k1gInSc3
přihlásilo	přihlásit	k5eAaPmAgNnS
při	při	k7c6
sčítání	sčítání	k1gNnSc6
lidu	lid	k1gInSc2
108	#num#	k4
462	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
Moravě	Morava	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc1
etnografických	etnografický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
úzce	úzko	k6eAd1
spjatých	spjatý	k2eAgFnPc2d1
s	s	k7c7
krajem	kraj	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
žijí	žít	k5eAaImIp3nP
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
lišily	lišit	k5eAaImAgFnP
některými	některý	k3yIgInPc7
kulturními	kulturní	k2eAgInPc7d1
rysy	rys	k1gInPc7
a	a	k8xC
dialektem	dialekt	k1gInSc7
<g/>
,	,	kIx,
například	například	k6eAd1
Horáci	Horák	k1gMnPc1
<g/>
,	,	kIx,
Hanáci	Hanák	k1gMnPc1
<g/>
,	,	kIx,
Moravští	moravský	k2eAgMnPc1d1
Slováci	Slovák	k1gMnPc1
<g/>
,	,	kIx,
Podlužáci	Podlužák	k1gMnPc1
<g/>
,	,	kIx,
Valaši	Valach	k1gMnPc1
<g/>
,	,	kIx,
Laši	Lach	k1gMnPc1
nebo	nebo	k8xC
Moravští	moravský	k2eAgMnPc1d1
Chorvati	Chorvat	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc4
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Slovanské	slovanský	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3
historicky	historicky	k6eAd1
doloženým	doložený	k2eAgNnSc7d1
etnikem	etnikum	k1gNnSc7
byly	být	k5eAaImAgFnP
keltské	keltský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
Volků-Tektoságů	Volků-Tektoság	k1gInPc2
<g/>
,	,	kIx,
vystřídané	vystřídaný	k2eAgFnSc6d1
na	na	k7c6
přelomu	přelom	k1gInSc6
letopočtu	letopočet	k1gInSc2
germánskými	germánský	k2eAgMnPc7d1
Kvády	Kvád	k1gMnPc7
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
sem	sem	k6eAd1
zasahoval	zasahovat	k5eAaImAgInS
vliv	vliv	k1gInSc1
Marobudovy	Marobudův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
osídlily	osídlit	k5eAaPmAgInP
Moravu	Morava	k1gFnSc4
slovanské	slovanský	k2eAgInPc1d1
kmeny	kmen	k1gInPc1
<g/>
:	:	kIx,
Holasici	Holasik	k1gMnPc1
na	na	k7c6
Opavsku	Opavsko	k1gNnSc6
(	(	kIx(
<g/>
jejichž	jejichž	k3xOyRp3gFnSc1
existence	existence	k1gFnSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
značně	značně	k6eAd1
nejistá	jistý	k2eNgFnSc1d1
<g/>
)	)	kIx)
a	a	k8xC
Moravané	Moravan	k1gMnPc1
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
zmiňováni	zmiňován	k2eAgMnPc1d1
až	až	k9
roku	rok	k1gInSc2
822	#num#	k4
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
souhrnný	souhrnný	k2eAgInSc4d1
název	název	k1gInSc4
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
obyvatele	obyvatel	k1gMnPc4
jednoho	jeden	k4xCgInSc2
kmene	kmen	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
si	se	k3xPyFc3
v	v	k7c6
8	#num#	k4
<g/>
.	.	kIx.
či	či	k8xC
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
podmanil	podmanit	k5eAaPmAgMnS
ostatní	ostatní	k2eAgInPc4d1
slovanské	slovanský	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
žijící	žijící	k2eAgInPc4d1
v	v	k7c6
tomto	tento	k3xDgInSc6
regionu	region	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
žijící	žijící	k2eAgMnPc1d1
zejména	zejména	k9
kolem	kolem	k7c2
středního	střední	k2eAgInSc2d1
a	a	k8xC
dolního	dolní	k2eAgInSc2d1
toku	tok	k1gInSc2
řeky	řeka	k1gFnSc2
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
až	až	k9
k	k	k7c3
soutoku	soutok	k1gInSc3
s	s	k7c7
řekou	řeka	k1gFnSc7
Dunaj	Dunaj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Velkomoravská	velkomoravský	k2eAgFnSc1d1
epocha	epocha	k1gFnSc1
</s>
<s>
Přibližný	přibližný	k2eAgInSc1d1
teritoriální	teritoriální	k2eAgInSc1d1
rozsah	rozsah	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
v	v	k7c6
době	doba	k1gFnSc6
jejího	její	k3xOp3gInSc2
největšího	veliký	k2eAgInSc2d3
územního	územní	k2eAgInSc2d1
rozmachu	rozmach	k1gInSc2
</s>
<s>
Velkomoravská	velkomoravský	k2eAgFnSc1d1
archeologická	archeologický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
v	v	k7c6
Sadech	sad	k1gInPc6
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
tvořila	tvořit	k5eAaImAgFnS
Morava	Morava	k1gFnSc1
součást	součást	k1gFnSc1
Sámovy	Sámův	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
sjednocením	sjednocení	k1gNnSc7
slovanských	slovanský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
v	v	k7c6
boji	boj	k1gInSc6
proti	proti	k7c3
Avarům	Avar	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
první	první	k4xOgFnSc6
čtvrtině	čtvrtina	k1gFnSc6
9	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
dolnomoravských	dolnomoravský	k2eAgInPc6d1
úvalech	úval	k1gInPc6
začal	začít	k5eAaPmAgMnS
formovat	formovat	k5eAaImF
raně-středověký	raně-středověký	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
útvar	útvar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
knížata	kníže	k1gNnPc4
<g/>
,	,	kIx,
v	v	k7c6
novověku	novověk	k1gInSc6
nazvaná	nazvaný	k2eAgFnSc1d1
Mojmírovci	Mojmírovec	k1gMnPc7
<g/>
,	,	kIx,
připojila	připojit	k5eAaPmAgFnS
r.	r.	kA
833	#num#	k4
k	k	k7c3
sídelnímu	sídelní	k2eAgNnSc3d1
jádru	jádro	k1gNnSc3
i	i	k8xC
zemi	zem	k1gFnSc3
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
Karpat	Karpaty	k1gInPc2
–	–	k?
Nitranské	nitranský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
(	(	kIx(
<g/>
jihozápadní	jihozápadní	k2eAgNnSc1d1
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
Morava	Morava	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
centrem	centr	k1gInSc7
prvního	první	k4xOgInSc2
raně	raně	k6eAd1
feudálního	feudální	k2eAgInSc2d1
státu	stát	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
–	–	k?
Velkomoravské	velkomoravský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
Velkomoravskou	velkomoravský	k2eAgFnSc7d1
říší	říš	k1gFnSc7
a	a	k8xC
Byzancí	Byzanc	k1gFnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
863	#num#	k4
uzavřena	uzavřen	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Císař	Císař	k1gMnSc1
Michael	Michael	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
vyšel	vyjít	k5eAaPmAgMnS
vstříc	vstříc	k7c3
žádosti	žádost	k1gFnSc3
knížete	kníže	k1gMnSc2
Rostislava	Rostislav	k1gMnSc2
a	a	k8xC
vyslal	vyslat	k5eAaPmAgMnS
na	na	k7c4
Moravu	Morava	k1gFnSc4
misii	misie	k1gFnSc4
vedenou	vedený	k2eAgFnSc4d1
„	„	k?
<g/>
soluňskými	soluňský	k2eAgMnPc7d1
bratry	bratr	k1gMnPc7
<g/>
“	“	k?
Konstantinem	Konstantin	k1gMnSc7
a	a	k8xC
Metodějem	Metoděj	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
zásluhou	zásluha	k1gFnSc7
se	se	k3xPyFc4
Velkomoravská	velkomoravský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
stala	stát	k5eAaPmAgFnS
kolébkou	kolébka	k1gFnSc7
slovanské	slovanský	k2eAgFnSc2d1
vzdělanosti	vzdělanost	k1gFnSc2
a	a	k8xC
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
tu	tu	k6eAd1
šířit	šířit	k5eAaImF
východní	východní	k2eAgFnSc4d1
větev	větev	k1gFnSc4
křesťanství	křesťanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgMnSc1
z	z	k7c2
věrozvěstů	věrozvěst	k1gMnPc2
se	se	k3xPyFc4
o	o	k7c4
něco	něco	k3yInSc4
později	pozdě	k6eAd2
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
moravsko-panonským	moravsko-panonský	k2eAgMnSc7d1
arcibiskupem	arcibiskup	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
byla	být	k5eAaImAgFnS
politicky	politicky	k6eAd1
a	a	k8xC
vojensky	vojensky	k6eAd1
silným	silný	k2eAgInSc7d1
středoevropským	středoevropský	k2eAgInSc7d1
státem	stát	k1gInSc7
s	s	k7c7
mocnými	mocný	k2eAgMnPc7d1
panovníky	panovník	k1gMnPc7
v	v	k7c6
čele	čelo	k1gNnSc6
(	(	kIx(
<g/>
nejslavnějším	slavný	k2eAgMnPc3d3
byl	být	k5eAaImAgMnS
Svatopluk	Svatopluk	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
dobu	doba	k1gFnSc4
udivující	udivující	k2eAgFnSc7d1
hmotnou	hmotný	k2eAgFnSc7d1
kulturou	kultura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prameny	pramen	k1gInPc1
se	se	k3xPyFc4
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
zmiňují	zmiňovat	k5eAaImIp3nP
o	o	k7c6
velikosti	velikost	k1gFnSc6
a	a	k8xC
výstavnosti	výstavnost	k1gFnSc6
Svatoplukova	Svatoplukův	k2eAgNnSc2d1
města	město	k1gNnSc2
(	(	kIx(
<g/>
neznámého	známý	k2eNgInSc2d1
jménem	jméno	k1gNnSc7
<g/>
,	,	kIx,
snad	snad	k9
Veligrad	Veligrad	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
bohatství	bohatství	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
země	zem	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
nakonec	nakonec	k6eAd1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
oslabená	oslabený	k2eAgFnSc1d1
vnitřními	vnitřní	k2eAgInPc7d1
dynastickými	dynastický	k2eAgInPc7d1
rozpory	rozpor	k1gInPc7
<g/>
,	,	kIx,
nevydržela	vydržet	k5eNaPmAgFnS
trvalý	trvalý	k2eAgInSc4d1
tlak	tlak	k1gInSc4
Franků	Frank	k1gMnPc2
a	a	k8xC
zejména	zejména	k9
nájezdy	nájezd	k1gInPc1
nového	nový	k2eAgMnSc2d1
protivníka	protivník	k1gMnSc2
z	z	k7c2
východu	východ	k1gInSc2
<g/>
:	:	kIx,
kočovných	kočovný	k2eAgInPc2d1
Maďarů	maďar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
zániku	zánik	k1gInSc6
říše	říš	k1gFnSc2
v	v	k7c6
letech	léto	k1gNnPc6
906	#num#	k4
<g/>
/	/	kIx~
<g/>
907	#num#	k4
existuje	existovat	k5eAaImIp3nS
pro	pro	k7c4
následující	následující	k2eAgNnSc4d1
století	století	k1gNnSc4
jen	jen	k9
minimum	minimum	k1gNnSc4
písemných	písemný	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
teritorium	teritorium	k1gNnSc4
nynější	nynější	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
pod	pod	k7c4
vliv	vliv	k1gInSc4
Uher	uher	k1gInSc1
popř.	popř.	kA
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
zbytky	zbytek	k1gInPc1
původní	původní	k2eAgFnSc2d1
světské	světský	k2eAgFnSc2d1
a	a	k8xC
církevní	církevní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
byly	být	k5eAaImAgInP
patrně	patrně	k6eAd1
zachovány	zachovat	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s>
Středověk	středověk	k1gInSc1
</s>
<s>
Pod	pod	k7c7
vládou	vláda	k1gFnSc7
Přemyslovců	Přemyslovec	k1gMnPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
−	−	k?
<g/>
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Dóm	dóm	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Václava	Václav	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
</s>
<s>
Město	město	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
jedno	jeden	k4xCgNnSc4
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
sídel	sídlo	k1gNnPc2
Moravy	Morava	k1gFnSc2
</s>
<s>
Teprve	teprve	k6eAd1
až	až	k9
po	po	k7c6
několikanásobné	několikanásobný	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
Uhrů	Uher	k1gMnPc2
v	v	k7c6
letech	let	k1gInPc6
955	#num#	k4
a	a	k8xC
965	#num#	k4
získal	získat	k5eAaPmAgInS
severní	severní	k2eAgFnSc4d1
polovinu	polovina	k1gFnSc4
Moravy	Morava	k1gFnSc2
postupně	postupně	k6eAd1
pod	pod	k7c4
své	svůj	k3xOyFgNnSc4
panství	panství	k1gNnSc4
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Boleslav	Boleslav	k1gMnSc1
I.	I.	kA
Hranice	hranice	k1gFnSc1
Moravy	Morava	k1gFnSc2
byly	být	k5eAaImAgInP
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
značně	značně	k6eAd1
odlišné	odlišný	k2eAgNnSc1d1
od	od	k7c2
jejich	jejich	k3xOp3gInSc2
současného	současný	k2eAgInSc2d1
průběhu	průběh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
Přemyslovci	Přemyslovec	k1gMnSc3
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
ale	ale	k8xC
patrně	patrně	k6eAd1
neovládli	ovládnout	k5eNaPmAgMnP
<g/>
,	,	kIx,
zasahovala	zasahovat	k5eAaImAgFnS
hranice	hranice	k1gFnSc1
slovanského	slovanský	k2eAgNnSc2d1
osídlení	osídlení	k1gNnSc2
jižněji	jižně	k6eAd2
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
až	až	k9
k	k	k7c3
Dunaji	Dunaj	k1gInSc3
<g/>
;	;	kIx,
na	na	k7c6
východě	východ	k1gInSc6
vedla	vést	k5eAaImAgFnS
po	po	k7c6
západním	západní	k2eAgNnSc6d1
úpatí	úpatí	k1gNnSc6
Bílých	bílý	k2eAgInPc2d1
Karpat	Karpaty	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
nebyla	být	k5eNaImAgFnS
dlouho	dlouho	k6eAd1
zřejmá	zřejmý	k2eAgFnSc1d1
<g/>
,	,	kIx,
probíhala	probíhat	k5eAaImAgFnS
zhruba	zhruba	k6eAd1
hlubokými	hluboký	k2eAgInPc7d1
hvozdy	hvozd	k1gInPc7
na	na	k7c6
dnešním	dnešní	k2eAgNnSc6d1
jižním	jižní	k2eAgNnSc6d1
pomezí	pomezí	k1gNnSc6
Opavska	Opavsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1003	#num#	k4
se	se	k3xPyFc4
zmocnila	zmocnit	k5eAaPmAgFnS
celé	celá	k1gFnPc4
Moravy	Morava	k1gFnSc2
polská	polský	k2eAgNnPc4d1
vojska	vojsko	k1gNnPc4
Boleslava	Boleslav	k1gMnSc2
Chrabrého	chrabrý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
kdy	kdy	k6eAd1
Polsko	Polsko	k1gNnSc1
Moravu	Morava	k1gFnSc4
podrželo	podržet	k5eAaPmAgNnS
není	být	k5eNaImIp3nS
přesně	přesně	k6eAd1
známo	znám	k2eAgNnSc1d1
–	–	k?
její	její	k3xOp3gNnSc1
opětovné	opětovný	k2eAgNnSc1d1
ovládnutí	ovládnutí	k1gNnSc1
(	(	kIx(
<g/>
až	až	k9
po	po	k7c4
Bílé	bílý	k2eAgInPc4d1
Karpaty	Karpaty	k1gInPc4
<g/>
)	)	kIx)
Přemyslovcem	Přemyslovec	k1gMnSc7
Oldřichem	Oldřich	k1gMnSc7
se	se	k3xPyFc4
klade	klást	k5eAaImIp3nS
do	do	k7c2
roku	rok	k1gInSc2
1019	#num#	k4
nebo	nebo	k8xC
do	do	k7c2
r.	r.	kA
1029	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
na	na	k7c6
přelomu	přelom	k1gInSc6
11	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
však	však	k9
patřila	patřit	k5eAaImAgFnS
část	část	k1gFnSc1
Moravy	Morava	k1gFnSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
Uherského	uherský	k2eAgNnSc2d1
Hradiště	Hradiště	k1gNnSc2
<g/>
,	,	kIx,
Uherského	uherský	k2eAgInSc2d1
Brodu	Brod	k1gInSc2
a	a	k8xC
Strážnice	Strážnice	k1gFnSc2
k	k	k7c3
Uhrám	Uhry	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oldřich	Oldřich	k1gMnSc1
dal	dát	k5eAaPmAgMnS
zemi	zem	k1gFnSc4
do	do	k7c2
správy	správa	k1gFnSc2
svému	svůj	k1gMnSc3
synu	syn	k1gMnSc3
Břetislavovi	Břetislav	k1gMnSc3
I.	I.	kA
</s>
<s>
Břetislav	Břetislav	k1gMnSc1
I.	I.	kA
založil	založit	k5eAaPmAgMnS
či	či	k8xC
obnovil	obnovit	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
hradů	hrad	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
tři	tři	k4xCgInPc1
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
a	a	k8xC
Znojmo	Znojmo	k1gNnSc1
–	–	k?
se	se	k3xPyFc4
později	pozdě	k6eAd2
rozrostly	rozrůst	k5eAaPmAgInP
v	v	k7c4
důležitá	důležitý	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
hrady	hrad	k1gInPc4
také	také	k9
na	na	k7c6
samém	samý	k3xTgInSc6
sklonku	sklonek	k1gInSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
(	(	kIx(
<g/>
1055	#num#	k4
<g/>
)	)	kIx)
ustanovil	ustanovit	k5eAaPmAgMnS
centry	centr	k1gInPc4
nových	nový	k2eAgFnPc2d1
správních	správní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
údělů	úděl	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
kterých	který	k3yQgInPc6,k3yIgInPc6,k3yRgInPc6
vládli	vládnout	k5eAaImAgMnP
mladší	mladý	k2eAgMnPc1d2
synové	syn	k1gMnPc1
pražských	pražský	k2eAgNnPc2d1
knížat	kníže	k1gNnPc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
brněnském	brněnský	k2eAgInSc6d1
a	a	k8xC
znojemském	znojemský	k2eAgInSc6d1
údělu	úděl	k1gInSc6
<g/>
,	,	kIx,
samostatné	samostatný	k2eAgFnPc1d1
boční	boční	k2eAgFnPc1d1
linie	linie	k1gFnPc1
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Břetislav	Břetislav	k1gMnSc1
také	také	k9
stál	stát	k5eAaImAgMnS
u	u	k7c2
počátků	počátek	k1gInPc2
moravského	moravský	k2eAgNnSc2d1
mincovnictví	mincovnictví	k1gNnSc2
–	–	k?
ještě	ještě	k9
za	za	k7c2
života	život	k1gInSc2
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
začal	začít	k5eAaPmAgMnS
na	na	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
hradě	hrad	k1gInSc6
razit	razit	k5eAaImF
denáry	denár	k1gInPc4
a	a	k8xC
založil	založit	k5eAaPmAgMnS
zde	zde	k6eAd1
nejstarší	starý	k2eAgFnSc4d3
mincovnu	mincovna	k1gFnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc1
podtrhla	podtrhnout	k5eAaPmAgFnS
přednostní	přednostní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
Olomouce	Olomouc	k1gFnSc2
vůči	vůči	k7c3
ostatním	ostatní	k2eAgInPc3d1
údělům	úděl	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Nástupce	nástupce	k1gMnSc1
Břetislavův	Břetislavův	k2eAgMnSc1d1
na	na	k7c6
pražském	pražský	k2eAgInSc6d1
stolci	stolec	k1gInSc6
<g/>
,	,	kIx,
nejstarší	starý	k2eAgMnSc1d3
syn	syn	k1gMnSc1
Spytihněv	Spytihněv	k1gFnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
Moravu	Morava	k1gFnSc4
uspořádat	uspořádat	k5eAaPmF
jinak	jinak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sesadil	sesadit	k5eAaPmAgMnS
všechny	všechen	k3xTgInPc4
tři	tři	k4xCgInPc4
své	svůj	k3xOyFgMnPc4
bratry	bratr	k1gMnPc4
<g/>
/	/	kIx~
<g/>
údělníky	údělník	k1gMnPc4
(	(	kIx(
<g/>
olomoucký	olomoucký	k2eAgMnSc1d1
Vratislav	Vratislav	k1gMnSc1
–	–	k?
pozdější	pozdní	k2eAgMnSc1d2
král	král	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
–	–	k?
musel	muset	k5eAaImAgMnS
dokonce	dokonce	k9
uprchnout	uprchnout	k5eAaPmF
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dosazené	dosazený	k2eAgNnSc1d1
Břetislavem	Břetislav	k1gMnSc7
<g/>
;	;	kIx,
uvěznil	uvěznit	k5eAaPmAgInS
300	#num#	k4
moravských	moravský	k2eAgInPc2d1
předáků	předák	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
vyvlastnil	vyvlastnit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInSc4
majetek	majetek	k1gInSc4
(	(	kIx(
<g/>
„	„	k?
<g/>
koně	kůň	k1gMnSc4
i	i	k8xC
jejich	jejich	k3xOp3gNnSc4
zbroj	zbroj	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c4
jejich	jejich	k3xOp3gNnPc4
místa	místo	k1gNnPc4
dosadil	dosadit	k5eAaPmAgMnS
své	svůj	k3xOyFgMnPc4
lidi	člověk	k1gMnPc4
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
však	však	k9
nový	nový	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Vratislav	Vratislav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
systém	systém	k1gInSc1
údělů	úděl	k1gInPc2
opět	opět	k6eAd1
obnovil	obnovit	k5eAaPmAgInS
(	(	kIx(
<g/>
tzv.	tzv.	kA
druhé	druhý	k4xOgNnSc1
dělení	dělení	k1gNnSc1
Moravy	Morava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
ty	ten	k3xDgFnPc1
pak	pak	k6eAd1
existovaly	existovat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
sjednocení	sjednocení	k1gNnSc2
země	zem	k1gFnSc2
v	v	k7c6
r.	r.	kA
1182	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Zvláštní	zvláštní	k2eAgNnSc1d1
postavení	postavení	k1gNnSc1
mělo	mít	k5eAaImAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
Moravy	Morava	k1gFnSc2
olomoucké	olomoucký	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
<g/>
,	,	kIx,
založené	založený	k2eAgNnSc1d1
Vratislavem	Vratislav	k1gMnSc7
roku	rok	k1gInSc2
1063	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomoucký	olomoucký	k2eAgInSc1d1
biskup	biskup	k1gInSc1
býval	bývat	k5eAaImAgInS
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
pražského	pražský	k2eAgNnSc2d1
<g/>
)	)	kIx)
doslova	doslova	k6eAd1
pravou	pravý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
knížat	kníže	k1gMnPc2wR
a	a	k8xC
králů	král	k1gMnPc2
českých	český	k2eAgMnPc2d1
<g/>
,	,	kIx,
mocným	mocný	k2eAgInSc7d1
činitelem	činitel	k1gInSc7
v	v	k7c6
zemi	zem	k1gFnSc6
(	(	kIx(
<g/>
jediná	jediný	k2eAgFnSc1d1
skutečná	skutečný	k2eAgFnSc1d1
lenní	lenní	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
zemích	zem	k1gFnPc6
vznikla	vzniknout	k5eAaPmAgFnS
právě	právě	k6eAd1
na	na	k7c6
statcích	statek	k1gInPc6
a	a	k8xC
panstvích	panství	k1gNnPc6
biskupů	biskup	k1gMnPc2
z	z	k7c2
Olomouce	Olomouc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
</s>
<s>
Na	na	k7c6
konci	konec	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
moravské	moravský	k2eAgInPc1d1
úděly	úděl	k1gInPc1
sjednotil	sjednotit	k5eAaPmAgMnS
během	během	k7c2
krize	krize	k1gFnSc2
uvnitř	uvnitř	k7c2
přemyslovského	přemyslovský	k2eAgInSc2d1
rodu	rod	k1gInSc2
pozdější	pozdní	k2eAgMnSc1d2
český	český	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
sjednocení	sjednocení	k1gNnSc2
země	zem	k1gFnSc2
dokončil	dokončit	k5eAaPmAgMnS
markrabě	markrabě	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Břetislav	Břetislav	k1gMnSc1
a	a	k8xC
tak	tak	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
Moravské	moravský	k2eAgNnSc1d1
markrabství	markrabství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Loděnice	loděnice	k1gFnSc2
na	na	k7c6
Moravskokrumlovsku	Moravskokrumlovsko	k1gNnSc6
si	se	k3xPyFc3
Konrád	Konrád	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ota	Ota	k1gMnSc1
knínskou	knínská	k1gFnSc4
dohodou	dohoda	k1gFnSc7
s	s	k7c7
knížetem	kníže	k1gMnSc7
Bedřichem	Bedřich	k1gMnSc7
podržel	podržet	k5eAaPmAgInS
markraběcí	markraběcí	k2eAgInSc1d1
titul	titul	k1gInSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
uznal	uznat	k5eAaPmAgMnS
vrchní	vrchní	k2eAgFnSc4d1
autoritu	autorita	k1gFnSc4
českého	český	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
stal	stát	k5eAaPmAgMnS
českým	český	k2eAgMnSc7d1
knížetem	kníže	k1gMnSc7
<g/>
,	,	kIx,
přestal	přestat	k5eAaPmAgInS
markraběcí	markraběcí	k2eAgInSc4d1
titul	titul	k1gInSc4
užívat	užívat	k5eAaImF
<g/>
,	,	kIx,
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
listin	listina	k1gFnPc2
se	se	k3xPyFc4
dokonce	dokonce	k9
představil	představit	k5eAaPmAgInS
jako	jako	k9
„	„	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
kníže	kníže	k1gMnSc1
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
kdysi	kdysi	k6eAd1
markrabě	markrabě	k1gMnSc1
moravský	moravský	k2eAgMnSc1d1
<g/>
“	“	k?
(	(	kIx(
<g/>
Boemorum	Boemorum	k1gInSc1
dux	dux	k?
<g/>
,	,	kIx,
quondam	quondam	k6eAd1
marchio	marchio	k1gMnSc1
Morauie	Morauie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
dal	dát	k5eAaPmAgMnS
najevo	najevo	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
hodnost	hodnost	k1gFnSc4
českého	český	k2eAgNnSc2d1
knížete	kníže	k1gNnSc2wR
v	v	k7c6
sobě	sebe	k3xPyFc6
zahrnuje	zahrnovat	k5eAaImIp3nS
i	i	k9
panování	panování	k1gNnSc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Morava	Morava	k1gFnSc1
jako	jako	k8xS,k8xC
jednotná	jednotný	k2eAgFnSc1d1
země	země	k1gFnSc1
nicméně	nicméně	k8xC
nezanikla	zaniknout	k5eNaPmAgFnS
<g/>
,	,	kIx,
její	její	k3xOp3gInPc4
vztahy	vztah	k1gInPc4
k	k	k7c3
Čechám	Čechy	k1gFnPc3
však	však	k9
byly	být	k5eAaImAgInP
formálně	formálně	k6eAd1
neujasněné	ujasněný	k2eNgInPc1d1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1348	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
z	z	k7c2
Moravy	Morava	k1gFnSc2
učinil	učinit	k5eAaImAgMnS,k5eAaPmAgMnS
léno	léno	k1gNnSc4
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mongolský	mongolský	k2eAgInSc4d1
vpád	vpád	k1gInSc4
na	na	k7c4
Moravu	Morava	k1gFnSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
roku	rok	k1gInSc2
1241	#num#	k4
za	za	k7c2
vlády	vláda	k1gFnSc2
krále	král	k1gMnSc2
Václava	Václav	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
zpustošil	zpustošit	k5eAaPmAgInS
Opavsko	Opavsko	k1gNnSc4
a	a	k8xC
jablunkovským	jablunkovský	k2eAgInSc7d1
průsmykem	průsmyk	k1gInSc7
se	se	k3xPyFc4
přehnal	přehnat	k5eAaPmAgMnS
do	do	k7c2
Uher	Uhry	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Moravu	Morava	k1gFnSc4
plenili	plenit	k5eAaImAgMnP
také	také	k9
kočovní	kočovní	k2eAgMnPc1d1
Kumáni	Kumán	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
bojovali	bojovat	k5eAaImAgMnP
v	v	k7c6
uherském	uherský	k2eAgNnSc6d1
vojsku	vojsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počínaje	počínaje	k7c7
rokem	rok	k1gInSc7
1269	#num#	k4
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
z	z	k7c2
Moravy	Morava	k1gFnSc2
vydělovat	vydělovat	k5eAaImF
Opavské	opavský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
vytvořené	vytvořený	k2eAgNnSc1d1
Přemyslem	Přemysl	k1gMnSc7
Otakarem	Otakar	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
pro	pro	k7c4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
nemanželského	manželský	k2eNgMnSc4d1
syna	syn	k1gMnSc4
<g/>
,	,	kIx,
knížete	kníže	k1gMnSc2
Mikuláše	Mikuláš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Markrabství	markrabství	k1gNnSc1
mělo	mít	k5eAaImAgNnS
počátkem	počátek	k1gInSc7
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
své	svůj	k3xOyFgMnPc4
zemské	zemský	k2eAgMnPc4d1
úředníky	úředník	k1gMnPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
nejstarším	starý	k2eAgMnPc3d3
a	a	k8xC
nejpřednějším	přední	k2eAgMnPc3d3
byl	být	k5eAaImAgMnS
zemský	zemský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
(	(	kIx(
<g/>
poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1298	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemská	zemský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
,	,	kIx,
reprezentovaná	reprezentovaný	k2eAgFnSc1d1
sněmem	sněm	k1gInSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
na	na	k7c6
Moravě	Morava	k1gFnSc6
formovat	formovat	k5eAaImF
až	až	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
od	od	k7c2
vývoje	vývoj	k1gInSc2
Čech	Čechy	k1gFnPc2
odlišila	odlišit	k5eAaPmAgFnS
i	i	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
déle	dlouho	k6eAd2
vydržel	vydržet	k5eAaPmAgInS
institut	institut	k1gInSc1
krajských	krajský	k2eAgMnPc2d1
soudů	soud	k1gInPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
cúd	cúda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
svatováclavské	svatováclavský	k2eAgFnSc2d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
−	−	k?
<g/>
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Moravský	moravský	k2eAgMnSc1d1
markrabě	markrabě	k1gMnSc1
a	a	k8xC
římsko-německý	římsko-německý	k2eAgMnSc1d1
král	král	k1gMnSc1
Jošt	Jošt	k1gMnSc1
Moravský	moravský	k2eAgMnSc1d1
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
jedno	jeden	k4xCgNnSc1
z	z	k7c2
významných	významný	k2eAgNnPc2d1
pohřebišť	pohřebiště	k1gNnPc2
moravských	moravský	k2eAgNnPc2d1
markrabat	markrabě	k1gNnPc2
</s>
<s>
Štramberk	Štramberk	k1gInSc1
<g/>
,	,	kIx,
občas	občas	k6eAd1
označovaný	označovaný	k2eAgInSc1d1
za	za	k7c7
„	„	k?
<g/>
Moravský	moravský	k2eAgInSc4d1
Betlém	Betlém	k1gInSc4
<g/>
“	“	k?
</s>
<s>
Jan	Jan	k1gMnSc1
Lucemburský	lucemburský	k2eAgMnSc1d1
<g/>
,	,	kIx,
nový	nový	k2eAgMnSc1d1
markrabě	markrabě	k1gMnSc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
král	král	k1gMnSc1
Čech	Čech	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
vydat	vydat	k5eAaPmF
moravské	moravský	k2eAgFnSc3d1
a	a	k8xC
české	český	k2eAgFnSc3d1
šlechtě	šlechta	k1gFnSc3
tzv.	tzv.	kA
inaugurační	inaugurační	k2eAgInPc1d1
diplomy	diplom	k1gInPc1
neboli	neboli	k8xC
volební	volební	k2eAgFnPc1d1
kapitulace	kapitulace	k1gFnPc1
(	(	kIx(
<g/>
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
byl	být	k5eAaImAgInS
diplom	diplom	k1gInSc1
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
červnu	červen	k1gInSc6
1311	#num#	k4
<g/>
,	,	kIx,
půl	půl	k1xP
roku	rok	k1gInSc2
po	po	k7c6
tom	ten	k3xDgNnSc6
českém	český	k2eAgNnSc6d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
potvrdil	potvrdit	k5eAaPmAgMnS
všem	všecek	k3xTgMnPc3
knížatům	kníže	k1gMnPc3wR
<g />
.	.	kIx.
</s>
<s hack="1">
království	království	k1gNnSc1
našeho	náš	k3xOp1gInSc2
českého	český	k2eAgInSc2d1
a	a	k8xC
Moravě	Morava	k1gFnSc6
jejich	jejich	k3xOp3gInPc4
požadavky	požadavek	k1gInPc4
(	(	kIx(
<g/>
byť	byť	k8xS
v	v	k7c6
okleštěné	okleštěný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
)	)	kIx)
–	–	k?
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
že	že	k8xS
„	„	k?
<g/>
...	...	k?
<g/>
nikdy	nikdy	k6eAd1
nesvěříme	svěřit	k5eNaPmIp1nP
nikomu	nikdo	k3yNnSc3
jinému	jiný	k2eAgMnSc3d1
než	než	k8xS
Moravanu	Moravan	k1gMnSc3
na	na	k7c6
Moravě	Morava	k1gFnSc6
nějaký	nějaký	k3yIgInSc1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
spojený	spojený	k2eAgInSc1d1
s	s	k7c7
důchody	důchod	k1gInPc7
<g/>
...	...	k?
<g/>
“	“	k?
<g/>
;	;	kIx,
dále	daleko	k6eAd2
<g/>
,	,	kIx,
že	že	k8xS
zemskou	zemský	k2eAgFnSc4d1
berni	berni	k?
bude	být	k5eAaImBp3nS
král	král	k1gMnSc1
vybírat	vybírat	k5eAaImF
jen	jen	k9
ve	v	k7c6
zvláštních	zvláštní	k2eAgInPc6d1
případech	případ	k1gInPc6
a	a	k8xC
že	že	k9
nebude	být	k5eNaImBp3nS
šlechtu	šlechta	k1gFnSc4
nutit	nutit	k5eAaImF
k	k	k7c3
vojenským	vojenský	k2eAgInPc3d1
tažením	tažení	k1gNnSc7
za	za	k7c4
hranice	hranice	k1gFnPc4
země	zem	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
šlechtici	šlechtic	k1gMnPc1
měli	mít	k5eAaImAgMnP
pouze	pouze	k6eAd1
povinnost	povinnost	k1gFnSc4
zemské	zemský	k2eAgFnSc2d1
hotovosti	hotovost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
listiny	listina	k1gFnPc1
upevnily	upevnit	k5eAaPmAgFnP
politickou	politický	k2eAgFnSc4d1
samosprávu	samospráva	k1gFnSc4
Moravy	Morava	k1gFnSc2
ve	v	k7c6
středověku	středověk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Čechami	Čechy	k1gFnPc7
spojovala	spojovat	k5eAaImAgFnS
Moravu	Morava	k1gFnSc4
osoba	osoba	k1gFnSc1
společného	společný	k2eAgMnSc2d1
panovníka	panovník	k1gMnSc2
a	a	k8xC
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
úřady	úřada	k1gMnSc2
(	(	kIx(
<g/>
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
kancelář	kancelář	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
generální	generální	k2eAgFnSc1d1
sněmy	sněm	k1gInPc4
<g/>
,	,	kIx,
společné	společný	k2eAgNnSc4d1
obyvatelské	obyvatelský	k2eAgNnSc4d1
právo	právo	k1gNnSc4
šlechty	šlechta	k1gFnSc2
(	(	kIx(
<g/>
inkolát	inkolát	k1gInSc1
–	–	k?
šlechta	šlechta	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
i	i	k8xC
česká	český	k2eAgFnSc1d1
v	v	k7c6
něm	on	k3xPp3gMnSc6
měly	mít	k5eAaImAgFnP
přednost	přednost	k1gFnSc4
před	před	k7c7
šlechtou	šlechta	k1gFnSc7
z	z	k7c2
ostatních	ostatní	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
Koruny	koruna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
povinnost	povinnost	k1gFnSc4
společné	společný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
jednotná	jednotný	k2eAgFnSc1d1
církevní	církevní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
(	(	kIx(
<g/>
olomoucký	olomoucký	k2eAgMnSc1d1
a	a	k8xC
litomyšlský	litomyšlský	k2eAgMnSc1d1
biskup	biskup	k1gMnSc1
podléhali	podléhat	k5eAaImAgMnP
pražskému	pražský	k2eAgMnSc3d1
arcibiskupovi	arcibiskup	k1gMnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
soudní	soudní	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
(	(	kIx(
<g/>
mincmistrovský	mincmistrovský	k2eAgInSc4d1
<g/>
,	,	kIx,
komorní	komorní	k2eAgInSc4d1
<g/>
,	,	kIx,
dvorský	dvorský	k2eAgInSc4d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
apelační	apelační	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
městské	městský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
měly	mít	k5eAaImAgInP
obě	dva	k4xCgFnPc4
země	zem	k1gFnPc4
jednoho	jeden	k4xCgInSc2
společného	společný	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
úředníka	úředník	k1gMnSc2
<g/>
:	:	kIx,
nejvyšší	vysoký	k2eAgMnSc1d3
maršálek	maršálek	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
nejdůležitější	důležitý	k2eAgMnSc1d3
úředník	úředník	k1gMnSc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
i	i	k8xC
2	#num#	k4
<g/>
.	.	kIx.
úředníkem	úředník	k1gMnSc7
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1348	#num#	k4
český	český	k2eAgMnSc1d1
král	král	k1gMnSc1
a	a	k8xC
římský	římský	k2eAgMnSc1d1
císař	císař	k1gMnSc1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
učinil	učinit	k5eAaPmAgInS,k5eAaImAgInS
konec	konec	k1gInSc1
nejasnému	jasný	k2eNgInSc3d1
vztahu	vztah	k1gInSc3
Moravy	Morava	k1gFnSc2
k	k	k7c3
Čechám	Čechy	k1gFnPc3
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
utvoření	utvoření	k1gNnSc2
soustátí	soustátí	k1gNnSc2
zemí	zem	k1gFnSc7
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
učinil	učinit	k5eAaPmAgInS,k5eAaImAgInS
Moravu	Morava	k1gFnSc4
lénem	léno	k1gNnSc7
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
Morava	Morava	k1gFnSc1
nadále	nadále	k6eAd1
neměla	mít	k5eNaImAgFnS
být	být	k5eAaImF
zcizována	zcizovat	k5eAaImNgFnS
z	z	k7c2
moci	moc	k1gFnSc2
českých	český	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markraběti	markrabě	k1gMnSc3
patřila	patřit	k5eAaImAgFnS
vláda	vláda	k1gFnSc1
nad	nad	k7c7
částí	část	k1gFnSc7
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
hlavně	hlavně	k9
na	na	k7c6
jihu	jih	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhou	druhý	k4xOgFnSc4
část	část	k1gFnSc4
země	zem	k1gFnSc2
ovládal	ovládat	k5eAaImAgMnS
biskup	biskup	k1gMnSc1
olomoucký	olomoucký	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Opavský	opavský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
pak	pak	k6eAd1
byl	být	k5eAaImAgMnS
z	z	k7c2
Moravy	Morava	k1gFnSc2
vydělen	vydělen	k2eAgMnSc1d1
a	a	k8xC
měl	mít	k5eAaImAgInS
dokonce	dokonce	k9
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
markraběte	markrabě	k1gMnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
i	i	k9
právo	právo	k1gNnSc4
horního	horní	k2eAgInSc2d1
a	a	k8xC
mincovního	mincovní	k2eAgInSc2d1
regálu	regál	k1gInSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
též	též	k9
zreformoval	zreformovat	k5eAaPmAgMnS
moravské	moravský	k2eAgNnSc4d1
soudnictví	soudnictví	k1gNnSc4
a	a	k8xC
do	do	k7c2
Brna	Brno	k1gNnSc2
a	a	k8xC
Olomouce	Olomouc	k1gFnSc2
byly	být	k5eAaImAgFnP
převedeny	převést	k5eAaPmNgFnP
zemské	zemský	k2eAgFnPc1d1
desky	deska	k1gFnPc1
(	(	kIx(
<g/>
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
střídavě	střídavě	k6eAd1
zasedal	zasedat	k5eAaImAgInS
v	v	k7c6
obou	dva	k4xCgNnPc6
městech	město	k1gNnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
přelomu	přelom	k1gInSc6
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
probíhaly	probíhat	k5eAaImAgFnP
markraběcí	markraběcí	k2eAgFnPc1d1
války	válka	k1gFnPc1
mezi	mezi	k7c7
bratry	bratr	k1gMnPc7
Joštem	Jošt	k1gMnSc7
a	a	k8xC
Prokopem	Prokop	k1gMnSc7
<g/>
,	,	kIx,
které	který	k3yRgFnSc3,k3yIgFnSc3,k3yQgFnSc3
zemi	zem	k1gFnSc3
dosti	dosti	k6eAd1
zpustošily	zpustošit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
vláda	vláda	k1gFnSc1
markrabat	markrabě	k1gNnPc2
Karla	Karel	k1gMnSc2
<g/>
,	,	kIx,
Jana	Jan	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
a	a	k8xC
Jošta	Jošt	k1gMnSc2
bývá	bývat	k5eAaImIp3nS
též	též	k9
označována	označovat	k5eAaImNgFnS
za	za	k7c4
období	období	k1gNnSc4
velkého	velký	k2eAgInSc2d1
rozkvětu	rozkvět	k1gInSc2
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
<g/>
,	,	kIx,
Jošt	Jošt	k1gInSc1
se	se	k3xPyFc4
krátce	krátce	k6eAd1
před	před	k7c7
svojí	svůj	k3xOyFgFnSc7
smrtí	smrt	k1gFnSc7
dokonce	dokonce	k9
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
Svaté	svatý	k2eAgFnSc2d1
říše	říš	k1gFnSc2
římské	římský	k2eAgFnSc2d1
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
jeho	jeho	k3xOp3gMnSc1
strýc	strýc	k1gMnSc1
Karel	Karel	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Moravu	Morava	k1gFnSc4
nezasáhly	zasáhnout	k5eNaPmAgFnP
příliš	příliš	k6eAd1
významně	významně	k6eAd1
husitské	husitský	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
moravská	moravský	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
sice	sice	k8xC
nejprve	nejprve	k6eAd1
na	na	k7c6
čáslavském	čáslavský	k2eAgInSc6d1
sněmu	sněm	k1gInSc6
souhlasila	souhlasit	k5eAaImAgFnS
s	s	k7c7
husitským	husitský	k2eAgInSc7d1
programem	program	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc4
většina	většina	k1gFnSc1
pánů	pan	k1gMnPc2
zřekla	zřeknout	k5eAaPmAgFnS
a	a	k8xC
přijala	přijmout	k5eAaPmAgFnS
v	v	k7c6
listopadu	listopad	k1gInSc6
1421	#num#	k4
Zikmunda	Zikmund	k1gMnSc4
Lucemburského	lucemburský	k2eAgMnSc2d1
za	za	k7c4
krále	král	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
země	země	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
po	po	k7c6
jejich	jejich	k3xOp3gNnSc6
skončení	skončení	k1gNnSc6
rozdělila	rozdělit	k5eAaPmAgFnS
nábožensky	nábožensky	k6eAd1
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
Čechy	Čechy	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Husitské	husitský	k2eAgFnSc2d1
války	válka	k1gFnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jiří	Jiří	k1gMnSc1
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
vydal	vydat	k5eAaPmAgInS
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1464	#num#	k4
„	„	k?
<g/>
na	na	k7c4
poníženou	ponížený	k2eAgFnSc4d1
prosbu	prosba	k1gFnSc4
baronů	baron	k1gMnPc2
<g/>
,	,	kIx,
šlechticů	šlechtic	k1gMnPc2
a	a	k8xC
obyvatel	obyvatel	k1gMnPc2
tohoto	tento	k3xDgInSc2
markrabství	markrabství	k1gNnPc1
<g/>
“	“	k?
slavnostní	slavnostní	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
proklamoval	proklamovat	k5eAaBmAgMnS
nerozlučitelné	rozlučitelný	k2eNgNnSc4d1
spojení	spojení	k1gNnSc4
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
jako	jako	k8xS,k8xC
dvou	dva	k4xCgFnPc2
rovnoprávných	rovnoprávný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Markrabství	markrabství	k1gNnSc1
přestalo	přestat	k5eAaPmAgNnS
být	být	k5eAaImF
chápáno	chápat	k5eAaImNgNnS
jako	jako	k8xS,k8xC
české	český	k2eAgNnSc1d1
léno	léno	k1gNnSc1
a	a	k8xC
nemělo	mít	k5eNaImAgNnS
být	být	k5eAaImF
už	už	k9
nikdy	nikdy	k6eAd1
nikomu	nikdo	k3yNnSc3
postoupeno	postoupen	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
válek	válka	k1gFnPc2
s	s	k7c7
Matyášem	Matyáš	k1gMnSc7
Korvínem	Korvín	k1gMnSc7
a	a	k8xC
následného	následný	k2eAgInSc2d1
chaosu	chaos	k1gInSc2
a	a	k8xC
rozdělené	rozdělený	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
v	v	k7c6
zemích	zem	k1gFnPc6
Koruny	koruna	k1gFnSc2
české	český	k2eAgNnSc1d1
(	(	kIx(
<g/>
Morava	Morava	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1469	#num#	k4
<g/>
/	/	kIx~
<g/>
1471	#num#	k4
<g/>
–	–	k?
<g/>
1490	#num#	k4
spadala	spadat	k5eAaPmAgFnS,k5eAaImAgFnS
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
se	s	k7c7
Slezskem	Slezsko	k1gNnSc7
a	a	k8xC
obojí	oboj	k1gFnSc7
Lužicí	Lužice	k1gFnSc7
<g/>
,	,	kIx,
pod	pod	k7c4
vládu	vláda	k1gFnSc4
uherského	uherský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Matyáše	Matyáš	k1gMnSc2
Korvína	Korvín	k1gMnSc2
<g/>
)	)	kIx)
však	však	k9
listina	listina	k1gFnSc1
nebyla	být	k5eNaImAgFnS
fakticky	fakticky	k6eAd1
naplněna	naplnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Raný	raný	k2eAgInSc1d1
novověk	novověk	k1gInSc1
</s>
<s>
Památková	památkový	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
v	v	k7c6
Moravské	moravský	k2eAgFnSc6d1
Třebové	Třebová	k1gFnSc6
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1535	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
Zřízení	zřízení	k1gNnSc1
zemské	zemský	k2eAgNnSc1d1
markrabství	markrabství	k1gNnSc1
moravského	moravský	k2eAgMnSc2d1
a	a	k8xC
vznikly	vzniknout	k5eAaPmAgInP
tak	tak	k9
kraje	kraj	k1gInPc1
novojičínský	novojičínský	k2eAgInSc1d1
<g/>
,	,	kIx,
olomoucký	olomoucký	k2eAgMnSc1d1
<g/>
,	,	kIx,
brněnský	brněnský	k2eAgMnSc1d1
a	a	k8xC
hradišťský	hradišťský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
nejvyšším	vysoký	k2eAgInSc7d3
zemským	zemský	k2eAgInSc7d1
orgánem	orgán	k1gInSc7
moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
právo	právo	k1gNnSc4
povolovat	povolovat	k5eAaImF
berně	berně	k6eAd1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgInS
samostatnou	samostatný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
pokud	pokud	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
zemská	zemský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
a	a	k8xC
zemské	zemský	k2eAgNnSc4d1
zákonodárství	zákonodárství	k1gNnSc4
a	a	k8xC
střežil	střežit	k5eAaImAgMnS
moravské	moravský	k2eAgFnSc2d1
svobody	svoboda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
země	zem	k1gFnSc2
stál	stát	k5eAaImAgMnS
královský	královský	k2eAgMnSc1d1
zemský	zemský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
úředníky	úředník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zahraničí	zahraničí	k1gNnSc6
byla	být	k5eAaImAgFnS
Morava	Morava	k1gFnSc1
chápána	chápat	k5eAaImNgFnS
téměř	téměř	k6eAd1
jako	jako	k8xC,k8xS
samostatná	samostatný	k2eAgFnSc1d1
stavovská	stavovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Morava	Morava	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
přístupu	přístup	k1gInSc6
k	k	k7c3
ostatním	ostatní	k2eAgFnPc3d1
náboženstvím	náboženství	k1gNnSc7
tolerantnější	tolerantní	k2eAgFnSc2d2
než	než	k8xS
Čechy	Čech	k1gMnPc4
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c4
jih	jih	k1gInSc4
země	zem	k1gFnSc2
přivedlo	přivést	k5eAaPmAgNnS
několik	několik	k4yIc1
specifických	specifický	k2eAgFnPc2d1
etnických	etnický	k2eAgFnPc2d1
a	a	k8xC
náboženských	náboženský	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
rozvinuly	rozvinout	k5eAaPmAgFnP
své	své	k1gNnSc4
komunity	komunita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
členů	člen	k1gMnPc2
Jednoty	jednota	k1gFnSc2
bratrské	bratrský	k2eAgFnSc2d1
to	ten	k3xDgNnSc1
byli	být	k5eAaImAgMnP
ze	z	k7c2
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
vyhnaní	vyhnaný	k2eAgMnPc1d1
novokřtěnci	novokřtěnec	k1gMnPc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Habáni	habán	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
početná	početný	k2eAgFnSc1d1
židovská	židovský	k2eAgFnSc1d1
diaspora	diaspora	k1gFnSc1
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Mikulově	Mikulov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Počátkem	počátkem	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
Karla	Karel	k1gMnSc2
st.	st.	kA
ze	z	k7c2
Žerotína	Žerotín	k1gInSc2
se	se	k3xPyFc4
Moravané	Moravan	k1gMnPc1
poprvé	poprvé	k6eAd1
a	a	k8xC
naposledy	naposledy	k6eAd1
odtrhli	odtrhnout	k5eAaPmAgMnP
a	a	k8xC
dočasně	dočasně	k6eAd1
se	se	k3xPyFc4
odklonili	odklonit	k5eAaPmAgMnP
nikoliv	nikoliv	k9
od	od	k7c2
Koruny	koruna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
krále	král	k1gMnSc2
(	(	kIx(
<g/>
markrabího	markrabí	k1gMnSc2
<g/>
)	)	kIx)
Rudolfa	Rudolf	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
konfederace	konfederace	k1gFnSc1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
Uher	Uhry	k1gFnPc2
a	a	k8xC
Rakous	Rakousy	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
čele	čelo	k1gNnSc6
stál	stát	k5eAaImAgMnS
uherský	uherský	k2eAgMnSc1d1
král	král	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
Habsburský	habsburský	k2eAgInSc4d1
přijatý	přijatý	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1608	#num#	k4
za	za	k7c2
moravského	moravský	k2eAgMnSc2d1
markrabího	markrabí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matyáš	Matyáš	k1gMnSc1
tehdy	tehdy	k6eAd1
přiznal	přiznat	k5eAaPmAgMnS
moravským	moravský	k2eAgNnSc7d1
stavům	stav	k1gInPc3
právo	právo	k1gNnSc1
zákonodárné	zákonodárný	k2eAgInPc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
bez	bez	k7c2
souhlasu	souhlas	k1gInSc2
panovníka-markrabího	panovníka-markrabí	k2eAgInSc2d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
právo	právo	k1gNnSc4
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
vypovídat	vypovídat	k5eAaImF,k5eAaPmF
válku	válka	k1gFnSc4
a	a	k8xC
uzavírat	uzavírat	k5eAaPmF,k5eAaImF
mír	mír	k1gInSc4
a	a	k8xC
bránit	bránit	k5eAaImF
zemské	zemský	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
i	i	k8xC
zbraní	zbraň	k1gFnSc7
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
je	on	k3xPp3gNnSc4
panovník	panovník	k1gMnSc1
porušoval	porušovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
iniciativa	iniciativa	k1gFnSc1
byla	být	k5eAaImAgFnS
výsledkem	výsledek	k1gInSc7
rozhořčení	rozhořčení	k1gNnSc1
nad	nad	k7c7
postupem	postup	k1gInSc7
českých	český	k2eAgInPc2d1
stavů	stav	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gInSc7
postojem	postoj	k1gInSc7
v	v	k7c6
minulosti	minulost	k1gFnSc6
k	k	k7c3
Moravě	Morava	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
Karlem	Karel	k1gMnSc7
starším	starší	k1gMnSc7
ze	z	k7c2
Žerotína	Žerotín	k1gInSc2
a	a	k8xC
Františkem	František	k1gMnSc7
z	z	k7c2
Ditrichštejna	Ditrichštejn	k1gInSc2
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
odmítla	odmítnout	k5eAaPmAgFnS
připojit	připojit	k5eAaPmF
k	k	k7c3
českému	český	k2eAgNnSc3d1
stavovskému	stavovský	k2eAgNnSc3d1
povstání	povstání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojila	spojit	k5eAaPmAgFnS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1619	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
zemi	zem	k1gFnSc6
proběhl	proběhnout	k5eAaPmAgMnS
s	s	k7c7
pomocí	pomoc	k1gFnSc7
českého	český	k2eAgNnSc2d1
stavovského	stavovský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
převrat	převrat	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Bitvy	bitva	k1gFnPc1
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1620	#num#	k4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
na	na	k7c4
čtyři	čtyři	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
mužů	muž	k1gMnPc2
moravského	moravský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
o	o	k7c4
žoldnéře	žoldnéř	k1gMnPc4
z	z	k7c2
německých	německý	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
najaté	najatý	k2eAgInPc4d1
za	za	k7c4
peníze	peníz	k1gInPc4
moravských	moravský	k2eAgInPc2d1
stavů	stav	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
vedl	vést	k5eAaImAgMnS
Jindřich	Jindřich	k1gMnSc1
z	z	k7c2
Thurnu	Thurn	k1gInSc2
mladší	mladý	k2eAgMnSc1d2
a	a	k8xC
Jindřich	Jindřich	k1gMnSc1
Šlik	šlika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pobělohorské	pobělohorský	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Brna	Brno	k1gNnSc2
Švédy	švéda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1645	#num#	k4
</s>
<s>
Panství	panství	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
pobělohorské	pobělohorský	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1628	#num#	k4
vydáno	vydat	k5eAaPmNgNnS
na	na	k7c6
sněmu	sněm	k1gInSc6
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
Obnovené	obnovený	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
zemské	zemský	k2eAgNnSc4d1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
zůstalo	zůstat	k5eAaPmAgNnS
<g/>
,	,	kIx,
s	s	k7c7
drobnými	drobný	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
,	,	kIx,
v	v	k7c6
platnosti	platnost	k1gFnSc6
až	až	k9
do	do	k7c2
r.	r.	kA
1848	#num#	k4
a	a	k8xC
zabezpečovalo	zabezpečovat	k5eAaImAgNnS
Habsburkům	Habsburk	k1gMnPc3
dědičné	dědičný	k2eAgNnSc4d1
právo	právo	k1gNnSc4
na	na	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znamenalo	znamenat	k5eAaImAgNnS
tehdy	tehdy	k6eAd1
konec	konec	k1gInSc4
stavovských	stavovský	k2eAgFnPc2d1
svobod	svoboda	k1gFnPc2
a	a	k8xC
na	na	k7c4
tehdejší	tehdejší	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
značné	značný	k2eAgFnSc2d1
náboženské	náboženský	k2eAgFnSc2d1
tolerance	tolerance	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
nastupujícího	nastupující	k2eAgInSc2d1
absolutismu	absolutismus	k1gInSc2
Ferdinand	Ferdinand	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
také	také	k9
zřídil	zřídit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
úřad	úřad	k1gInSc1
–	–	k?
moravský	moravský	k2eAgInSc1d1
královský	královský	k2eAgInSc1d1
tribunál	tribunál	k1gInSc1
–	–	k?
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
nahradil	nahradit	k5eAaPmAgInS
dosavadní	dosavadní	k2eAgFnSc3d1
instituci	instituce	k1gFnSc3
zemského	zemský	k2eAgMnSc2d1
hejtmana	hejtman	k1gMnSc2
(	(	kIx(
<g/>
jakožto	jakožto	k8xS
jednotlivce	jednotlivec	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tribunál	tribunál	k1gInSc1
<g/>
,	,	kIx,
ustanovený	ustanovený	k2eAgInSc1d1
v	v	k7c6
prosinci	prosinec	k1gInSc6
r.	r.	kA
1636	#num#	k4
<g/>
,	,	kIx,
sídlil	sídlit	k5eAaImAgInS
zpočátku	zpočátku	k6eAd1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
r.	r.	kA
1642	#num#	k4
byl	být	k5eAaImAgMnS
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
se	s	k7c7
zemskými	zemský	k2eAgFnPc7d1
deskami	deska	k1gFnPc7
a	a	k8xC
soudem	soud	k1gInSc7
<g/>
)	)	kIx)
přemístěn	přemístěn	k2eAgMnSc1d1
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1642	#num#	k4
<g/>
–	–	k?
<g/>
1650	#num#	k4
byla	být	k5eAaImAgFnS
Morava	Morava	k1gFnSc1
okupována	okupovat	k5eAaBmNgFnS
švédskými	švédský	k2eAgFnPc7d1
vojsky	vojsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1663	#num#	k4
podnikli	podniknout	k5eAaPmAgMnP
krymští	krymský	k2eAgMnPc1d1
Tataři	Tatar	k1gMnPc1
společně	společně	k6eAd1
s	s	k7c7
Osmanskými	osmanský	k2eAgMnPc7d1
Turky	Turek	k1gMnPc4
několik	několik	k4yIc1
ničivých	ničivý	k2eAgInPc2d1
vpádů	vpád	k1gInPc2
na	na	k7c4
Moravu	Morava	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
bylo	být	k5eAaImAgNnS
odvlečeno	odvléct	k5eAaPmNgNnS
do	do	k7c2
otroctví	otroctví	k1gNnSc2
na	na	k7c4
12	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
především	především	k9
mladých	mladý	k2eAgFnPc2d1
dívek	dívka	k1gFnPc2
a	a	k8xC
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
„	„	k?
<g/>
Čoskoro	Čoskora	k1gFnSc5
sa	sa	k?
začalo	začít	k5eAaPmAgNnS
ohavné	ohavný	k2eAgFnPc4d1
neslýchané	slýchaný	k2eNgFnPc4d1
prznenie	prznenie	k1gFnPc4
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
znásilňovanie	znásilňovanie	k1gFnSc1
žien	žiena	k1gFnPc2
a	a	k8xC
panien	panina	k1gFnPc2
<g/>
,	,	kIx,
<g/>
“	“	k?
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
očitý	očitý	k2eAgMnSc1d1
svědek	svědek	k1gMnSc1
<g/>
,	,	kIx,
evangelický	evangelický	k2eAgMnSc1d1
farář	farář	k1gMnSc1
Štefan	Štefan	k1gMnSc1
Pilárik	Pilárik	k1gMnSc1
<g/>
,	,	kIx,
o	o	k7c6
ženách	žena	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
unesli	unést	k5eAaPmAgMnP
Tataři	Tatar	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Východní	východní	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
postihly	postihnout	k5eAaPmAgInP
vpády	vpád	k1gInPc1
uherských	uherský	k2eAgMnPc2d1
rebelů	rebel	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
zúčastnili	zúčastnit	k5eAaPmAgMnP
protihabsburských	protihabsburský	k2eAgNnPc2d1
stavovských	stavovský	k2eAgNnPc2d1
povstání	povstání	k1gNnPc2
v	v	k7c6
Uhrách	Uhry	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Územně-správní	Územně-správní	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
Karla	Karel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
z	z	k7c2
roku	rok	k1gInSc2
1714	#num#	k4
ustanovila	ustanovit	k5eAaPmAgFnS
(	(	kIx(
<g/>
už	už	k9
stálé	stálý	k2eAgFnSc6d1
<g/>
)	)	kIx)
kraje	kraj	k1gInPc1
<g/>
:	:	kIx,
Brněnský	brněnský	k2eAgInSc1d1
<g/>
,	,	kIx,
Jihlavský	jihlavský	k2eAgInSc1d1
<g/>
,	,	kIx,
Znojemský	znojemský	k2eAgInSc1d1
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
<g/>
,	,	kIx,
Přerovský	přerovský	k2eAgMnSc1d1
a	a	k8xC
Hradišťský	hradišťský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
pronikavými	pronikavý	k2eAgFnPc7d1
reformami	reforma	k1gFnPc7
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
(	(	kIx(
<g/>
zřízení	zřízení	k1gNnSc1
tzv.	tzv.	kA
Directoria	Directorium	k1gNnSc2
in	in	k?
publicis	publicis	k1gInSc1
et	et	k?
cameralibus	cameralibus	k1gInSc1
<g/>
)	)	kIx)
přestalo	přestat	k5eAaPmAgNnS
roce	rok	k1gInSc6
1749	#num#	k4
moravské	moravský	k2eAgFnSc3d1
markrabství	markrabství	k1gNnPc2
(	(	kIx(
<g/>
jakož	jakož	k8xC
i	i	k9
ostatní	ostatní	k2eAgFnPc4d1
země	zem	k1gFnPc4
Koruny	koruna	k1gFnSc2
české	český	k2eAgFnSc2d1
<g/>
)	)	kIx)
de	de	k?
facto	fact	k2eAgNnSc1d1
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ztratilo	ztratit	k5eAaPmAgNnS
i	i	k9
to	ten	k3xDgNnSc1
málo	málo	k1gNnSc1
pravomocí	pravomoc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
v	v	k7c6
pobělohorském	pobělohorský	k2eAgNnSc6d1
období	období	k1gNnSc6
vídeňský	vídeňský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
zemi	zem	k1gFnSc3
byl	být	k5eAaImAgInS
ochoten	ochoten	k2eAgInSc1d1
ponechat	ponechat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1763	#num#	k4
sice	sice	k8xC
panovnice	panovnice	k1gFnPc4
nespokojeným	spokojený	k2eNgInPc3d1
stavům	stav	k1gInPc3
částečně	částečně	k6eAd1
ustoupila	ustoupit	k5eAaPmAgFnS
(	(	kIx(
<g/>
ustavením	ustavení	k1gNnSc7
královského	královský	k2eAgNnSc2d1
moravského	moravský	k2eAgNnSc2d1
zemského	zemský	k2eAgNnSc2d1
gubernia	gubernium	k1gNnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
zemským	zemský	k2eAgMnSc7d1
hejtmanem	hejtman	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
zároveň	zároveň	k6eAd1
předsedou	předseda	k1gMnSc7
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
a	a	k8xC
zemského	zemský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ovšem	ovšem	k9
zemským	zemský	k2eAgMnPc3d1
úředníkům	úředník	k1gMnPc3
byly	být	k5eAaImAgFnP
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
úředníků	úředník	k1gMnPc2
<g/>
,	,	kIx,
dosazovaných	dosazovaný	k2eAgFnPc2d1
z	z	k7c2
centra	centrum	k1gNnSc2
do	do	k7c2
všech	všecek	k3xTgInPc2
nižších	nízký	k2eAgInPc2d2
a	a	k8xC
středních	střední	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
a	a	k8xC
do	do	k7c2
značné	značný	k2eAgFnSc2d1
části	část	k1gFnSc2
úřadů	úřad	k1gInPc2
zemských	zemský	k2eAgInPc2d1
<g/>
)	)	kIx)
jejich	jejich	k3xOp3gFnPc1
pravomoci	pravomoc	k1gFnPc1
stále	stále	k6eAd1
oklešťovány	oklešťován	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Radikální	radikální	k2eAgMnSc1d1
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
pak	pak	k6eAd1
zemské	zemský	k2eAgInPc4d1
úřady	úřad	k1gInPc4
proměnil	proměnit	k5eAaPmAgMnS
v	v	k7c6
čistě	čistě	k6eAd1
zeměpanské	zeměpanský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
a	a	k8xC
zemští	zemský	k2eAgMnPc1d1
(	(	kIx(
<g/>
stavovští	stavovský	k2eAgMnPc1d1
<g/>
)	)	kIx)
úředníci	úředník	k1gMnPc1
měli	mít	k5eAaImAgMnP
pouze	pouze	k6eAd1
čestné	čestný	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1758	#num#	k4
<g/>
,	,	kIx,
za	za	k7c2
sedmileté	sedmiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
vpadl	vpadnout	k5eAaPmAgMnS
na	na	k7c4
Moravu	Morava	k1gFnSc4
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ovšem	ovšem	k9
kvůli	kvůli	k7c3
potížím	potíž	k1gFnPc3
se	s	k7c7
zásobováním	zásobování	k1gNnSc7
<g/>
,	,	kIx,
úspěšné	úspěšný	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
Olomouce	Olomouc	k1gFnSc2
a	a	k8xC
prohrané	prohraný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Domašova	Domašův	k2eAgMnSc2d1
byl	být	k5eAaImAgMnS
nucen	nutit	k5eAaImNgMnS
se	se	k3xPyFc4
bez	bez	k7c2
úspěchu	úspěch	k1gInSc2
stáhnout	stáhnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
zkušenosti	zkušenost	k1gFnSc6
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
vybudovat	vybudovat	k5eAaPmF
z	z	k7c2
Olomouce	Olomouc	k1gFnSc2
nedobytnou	dobytný	k2eNgFnSc4d1
pevnost	pevnost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
chránit	chránit	k5eAaImF
severní	severní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1777	#num#	k4
bylo	být	k5eAaImAgNnS
olomoucké	olomoucký	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
povýšeno	povýšen	k2eAgNnSc1d1
na	na	k7c6
arcibiskupství	arcibiskupství	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
Brně	Brno	k1gNnSc6
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
biskupství	biskupství	k1gNnSc1
<g/>
,	,	kIx,
namísto	namísto	k7c2
proponovaného	proponovaný	k2eAgNnSc2d1
biskupství	biskupství	k1gNnSc2
v	v	k7c6
Opavě	Opava	k1gFnSc6
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
generální	generální	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1782	#num#	k4
<g/>
–	–	k?
<g/>
1783	#num#	k4
Josef	Josefa	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
sloučil	sloučit	k5eAaPmAgInS
státní	státní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Rakouského	rakouský	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
v	v	k7c4
Zemi	zem	k1gFnSc4
moravskoslezskou	moravskoslezský	k2eAgFnSc4d1
<g/>
,	,	kIx,
spravovanou	spravovaný	k2eAgFnSc4d1
moravskoslezským	moravskoslezský	k2eAgNnSc7d1
guberniem	gubernium	k1gNnSc7
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
stavovské	stavovský	k2eAgFnPc1d1
instituce	instituce	k1gFnPc1
obou	dva	k4xCgFnPc2
zemí	zem	k1gFnPc2
zůstaly	zůstat	k5eAaPmAgInP
zachovány	zachovat	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
nadále	nadále	k6eAd1
byla	být	k5eAaImAgFnS
Morava	Morava	k1gFnSc1
rozčleněna	rozčlenit	k5eAaPmNgFnS
na	na	k7c4
6	#num#	k4
krajů	kraj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
zároveň	zároveň	k6eAd1
roku	rok	k1gInSc2
1783	#num#	k4
začlenil	začlenit	k5eAaPmAgInS
území	území	k1gNnSc4
tzv.	tzv.	kA
moravských	moravský	k2eAgFnPc2d1
enkláv	enkláva	k1gFnPc2
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
pod	pod	k7c4
Opavský	opavský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
slezských	slezský	k2eAgInPc2d1
krajů	kraj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
nadále	nadále	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
tato	tento	k3xDgNnPc4
území	území	k1gNnPc4
spravována	spravován	k2eAgNnPc4d1
dle	dle	k7c2
moravských	moravský	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Císař	Císař	k1gMnSc1
Leopold	Leopold	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
svým	svůj	k3xOyFgInSc7
reskriptem	reskript	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
1790	#num#	k4
svolal	svolat	k5eAaPmAgInS
a	a	k8xC
obnovil	obnovit	k5eAaPmAgInS
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
(	(	kIx(
<g/>
čímž	což	k3yRnSc7,k3yQnSc7
vlastně	vlastně	k9
rušil	rušit	k5eAaImAgMnS
předchozí	předchozí	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
svého	svůj	k3xOyFgMnSc2
bratra	bratr	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněmu	sněm	k1gInSc2
předsedal	předsedat	k5eAaImAgMnS
sněmovní	sněmovní	k2eAgMnSc1d1
direktor	direktor	k1gMnSc1
a	a	k8xC
moravskoslezský	moravskoslezský	k2eAgMnSc1d1
gubernátor	gubernátor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskému	moravský	k2eAgInSc3d1
zemskému	zemský	k2eAgInSc3d1
sněmu	sněm	k1gInSc3
byla	být	k5eAaImAgFnS
navrácena	navrácen	k2eAgFnSc1d1
moc	moc	k1gFnSc1
zákonodárná	zákonodárný	k2eAgFnSc1d1
a	a	k8xC
vládní	vládní	k2eAgFnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
moc	moc	k6eAd1
výkonnou	výkonný	k2eAgFnSc4d1
si	se	k3xPyFc3
podržely	podržet	k5eAaPmAgInP
zeměpanské	zeměpanský	k2eAgInPc1d1
úřady	úřad	k1gInPc1
panovníka	panovník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Novověk	novověk	k1gInSc1
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1850	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
</s>
<s>
Schoberova	Schoberův	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1888	#num#	k4
</s>
<s>
Mapa	mapa	k1gFnSc1
administrativního	administrativní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
1897	#num#	k4
</s>
<s>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1850	#num#	k4
bylo	být	k5eAaImAgNnS
zrušeno	zrušit	k5eAaPmNgNnS
moravskoslezské	moravskoslezský	k2eAgNnSc1d1
gubernium	gubernium	k1gNnSc1
a	a	k8xC
Morava	Morava	k1gFnSc1
se	se	k3xPyFc4
Slezskem	Slezsko	k1gNnSc7
byly	být	k5eAaImAgFnP
opět	opět	k5eAaPmF
plně	plně	k6eAd1
samostatnými	samostatný	k2eAgFnPc7d1
korunními	korunní	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
dvou	dva	k4xCgNnPc2
zemských	zemský	k2eAgNnPc2d1
místodržitelství	místodržitelství	k1gNnPc2
<g/>
:	:	kIx,
moravského	moravský	k2eAgInSc2d1
a	a	k8xC
slezského	slezský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
centralizace	centralizace	k1gFnSc2
se	se	k3xPyFc4
Moravské	moravský	k2eAgNnSc1d1
markrabství	markrabství	k1gNnSc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
jiné	jiný	k2eAgFnPc1d1
země	zem	k1gFnPc1
mocnářství	mocnářství	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
korunní	korunní	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
mocnáře	mocnář	k1gMnSc2
a	a	k8xC
provincií	provincie	k1gFnSc7
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1867	#num#	k4
až	až	k9
1918	#num#	k4
země	zem	k1gFnPc4
Koruny	koruna	k1gFnSc2
české	český	k2eAgInPc1d1
spadaly	spadat	k5eAaPmAgInP,k5eAaImAgInP
pod	pod	k7c4
tzv.	tzv.	kA
Předlitavsko	Předlitavsko	k1gNnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
stávajících	stávající	k2eAgFnPc2d1
moravských	moravský	k2eAgFnPc2d1
enkláv	enkláva	k1gFnPc2
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
nadále	nadále	k6eAd1
tvořilo	tvořit	k5eAaImAgNnS
(	(	kIx(
<g/>
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
)	)	kIx)
specifické	specifický	k2eAgNnSc1d1
správní	správní	k2eAgNnSc1d1
území	území	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
bylo	být	k5eAaImAgNnS
spravováno	spravovat	k5eAaImNgNnS
slezskými	slezský	k2eAgInPc7d1
orgány	orgán	k1gInPc7
v	v	k7c6
rámci	rámec	k1gInSc6
nově	nově	k6eAd1
vzniklých	vzniklý	k2eAgInPc2d1
slezských	slezský	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
platily	platit	k5eAaImAgInP
zde	zde	k6eAd1
moravské	moravský	k2eAgInPc1d1
zemské	zemský	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
,	,	kIx,
daně	daň	k1gFnPc1
odtud	odtud	k6eAd1
byly	být	k5eAaImAgFnP
odváděné	odváděný	k2eAgInPc1d1
na	na	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
rovněž	rovněž	k9
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
moravských	moravský	k2eAgInPc2d1
volebních	volební	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
správní	správní	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
vznikly	vzniknout	k5eAaPmAgFnP
na	na	k7c6
Moravě	Morava	k1gFnSc6
dva	dva	k4xCgInPc1
kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
Brněnský	brněnský	k2eAgInSc1d1
s	s	k7c7
dvanácti	dvanáct	k4xCc7
okresy	okres	k1gInPc7
a	a	k8xC
statutárním	statutární	k2eAgNnSc7d1
městem	město	k1gNnSc7
Brnem	Brno	k1gNnSc7
a	a	k8xC
Olomoucký	olomoucký	k2eAgInSc1d1
s	s	k7c7
třinácti	třináct	k4xCc7
okresy	okres	k1gInPc7
a	a	k8xC
statutárním	statutární	k2eAgNnSc7d1
městem	město	k1gNnSc7
Olomoucí	Olomouc	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rozsáhlých	rozsáhlý	k2eAgInPc6d1
okresech	okres	k1gInPc6
existovaly	existovat	k5eAaImAgFnP
expozitury	expozitura	k1gFnPc1
okresních	okresní	k2eAgNnPc2d1
hejtmanství	hejtmanství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc4
následující	následující	k2eAgNnSc4d1
<g/>
:	:	kIx,
Břeclav	Břeclav	k1gFnSc4
–	–	k?
expozitura	expozitura	k1gFnSc1
okresního	okresní	k2eAgNnSc2d1
hejtmanství	hejtmanství	k1gNnSc2
v	v	k7c6
Hustopečích	Hustopeč	k1gFnPc6
<g/>
;	;	kIx,
Moravské	moravský	k2eAgInPc4d1
Budějovice	Budějovice	k1gInPc4
–	–	k?
expozitura	expozitura	k1gFnSc1
okresního	okresní	k2eAgNnSc2d1
hejtmanství	hejtmanství	k1gNnSc2
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
<g/>
;	;	kIx,
Staré	Staré	k2eAgNnSc1d1
Město	město	k1gNnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
–	–	k?
expozitura	expozitura	k1gFnSc1
okresního	okresní	k2eAgNnSc2d1
hejtmanství	hejtmanství	k1gNnSc2
v	v	k7c6
Šumperku	Šumperk	k1gInSc6
<g/>
;	;	kIx,
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
–	–	k?
expozitura	expozitura	k1gFnSc1
okresního	okresní	k2eAgNnSc2d1
hejtmanství	hejtmanství	k1gNnSc2
v	v	k7c6
Místku	místko	k1gNnSc6
<g/>
;	;	kIx,
Strážnice	Strážnice	k1gFnSc1
–	–	k?
expozitura	expozitura	k1gFnSc1
okresního	okresní	k2eAgNnSc2d1
hejtmanství	hejtmanství	k1gNnSc2
v	v	k7c6
Uherském	uherský	k2eAgNnSc6d1
Hradišti	Hradiště	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
1861	#num#	k4
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
tzv.	tzv.	kA
Únorová	únorový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
obsahovala	obsahovat	k5eAaImAgFnS
soubor	soubor	k1gInSc4
zemských	zemský	k2eAgNnPc2d1
zřízení	zřízení	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působnost	působnost	k1gFnSc1
nového	nový	k2eAgInSc2d1
Zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
a	a	k8xC
Moravského	moravský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
byla	být	k5eAaImAgFnS
taxativně	taxativně	k6eAd1
vymezena	vymezit	k5eAaPmNgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
zákonodárné	zákonodárný	k2eAgFnSc2d1
a	a	k8xC
správní	správní	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
zemského	zemský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
stál	stát	k5eAaImAgInS
opět	opět	k6eAd1
moravský	moravský	k2eAgMnSc1d1
zemský	zemský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výbor	výbor	k1gInSc1
měl	mít	k5eAaImAgInS
výkonnou	výkonný	k2eAgFnSc4d1
a	a	k8xC
samosprávnou	samosprávný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemský	zemský	k2eAgInSc1d1
místodržitel	místodržitel	k1gMnSc1
nebyl	být	k5eNaImAgInS
však	však	k9
odpovědný	odpovědný	k2eAgInSc1d1
zemskému	zemský	k2eAgInSc3d1
sněmu	sněm	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
byly	být	k5eAaImAgInP
kraje	kraj	k1gInPc1
zrušeny	zrušit	k5eAaPmNgInP
a	a	k8xC
nahradilo	nahradit	k5eAaPmAgNnS
je	on	k3xPp3gNnSc4
76	#num#	k4
„	„	k?
<g/>
smíšených	smíšený	k2eAgInPc2d1
<g/>
“	“	k?
okresů	okres	k1gInPc2
a	a	k8xC
dvě	dva	k4xCgNnPc4
statutární	statutární	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
a	a	k8xC
Olomouc	Olomouc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1865	#num#	k4
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
federalizace	federalizace	k1gFnSc2
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
byla	být	k5eAaImAgFnS
Únorová	únorový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
vídeňská	vídeňský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
zahájila	zahájit	k5eAaPmAgFnS
přímé	přímý	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
se	s	k7c7
zemskými	zemský	k2eAgInPc7d1
sněmy	sněm	k1gInPc7
včetně	včetně	k7c2
sněmu	sněm	k1gInSc2
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
Prosincová	prosincový	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
nesená	nesený	k2eAgFnSc1d1
liberálním	liberální	k2eAgMnSc7d1
duchem	duch	k1gMnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
bez	bez	k7c2
federalistického	federalistický	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
zemským	zemský	k2eAgMnSc7d1
hejtmanem	hejtman	k1gMnSc7
měl	mít	k5eAaImAgMnS
151	#num#	k4
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
<g/>
,	,	kIx,
jemuž	jenž	k3xRgInSc3
nadále	nadále	k6eAd1
předsedal	předsedat	k5eAaImAgMnS
zemský	zemský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
referátech	referát	k1gInPc6
staral	starat	k5eAaImAgInS
o	o	k7c4
zemskou	zemský	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
zemský	zemský	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
podniky	podnik	k1gInPc4
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
a	a	k8xC
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
zdravotnictví	zdravotnictví	k1gNnSc4
<g/>
,	,	kIx,
bezpečnost	bezpečnost	k1gFnSc4
<g/>
,	,	kIx,
vojenské	vojenský	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
vědu	věda	k1gFnSc4
<g/>
,	,	kIx,
literaturu	literatura	k1gFnSc4
a	a	k8xC
umění	umění	k1gNnSc4
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
<g/>
,	,	kIx,
dopravu	doprava	k1gFnSc4
a	a	k8xC
zemské	zemský	k2eAgFnSc2d1
daně	daň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
další	další	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
došlo	dojít	k5eAaPmAgNnS
opět	opět	k6eAd1
k	k	k7c3
reformě	reforma	k1gFnSc3
okresů	okres	k1gInPc2
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
se	se	k3xPyFc4
snížil	snížit	k5eAaPmAgInS
na	na	k7c4
30	#num#	k4
a	a	k8xC
doplnilo	doplnit	k5eAaPmAgNnS
je	on	k3xPp3gMnPc4
6	#num#	k4
statutárních	statutární	k2eAgNnPc2d1
měst	město	k1gNnPc2
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
Jihlava	Jihlava	k1gFnSc1
<g/>
,	,	kIx,
Znojmo	Znojmo	k1gNnSc1
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
a	a	k8xC
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
vznikly	vzniknout	k5eAaPmAgFnP
další	další	k2eAgInPc4d1
4	#num#	k4
okresy	okres	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
uspořádání	uspořádání	k1gNnSc1
platilo	platit	k5eAaImAgNnS
s	s	k7c7
malými	malý	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
ještě	ještě	k9
v	v	k7c6
První	první	k4xOgFnSc6
Československé	československý	k2eAgFnSc6d1
republice	republika	k1gFnSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Území	území	k1gNnSc1
okresů	okres	k1gInPc2
se	se	k3xPyFc4
dále	daleko	k6eAd2
členilo	členit	k5eAaImAgNnS
na	na	k7c4
soudní	soudní	k2eAgInPc4d1
okresy	okres	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
byly	být	k5eAaImAgFnP
rozděleny	rozdělit	k5eAaPmNgFnP
mezi	mezi	k7c4
dva	dva	k4xCgInPc4
i	i	k8xC
více	hodně	k6eAd2
politických	politický	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správu	správa	k1gFnSc4
smíšených	smíšený	k2eAgFnPc2d1
a	a	k8xC
později	pozdě	k6eAd2
politických	politický	k2eAgInPc2d1
okresů	okres	k1gInPc2
řídila	řídit	k5eAaImAgFnS
okresní	okresní	k2eAgNnSc4d1
hejtmanství	hejtmanství	k1gNnSc4
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
hejtmanem	hejtman	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1918	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
</s>
<s>
Katedrála	katedrála	k1gFnSc1
Božského	božský	k2eAgMnSc2d1
Spasitele	spasitel	k1gMnSc2
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
</s>
<s>
Roku	rok	k1gInSc2
1918	#num#	k4
se	se	k3xPyFc4
Morava	Morava	k1gFnSc1
jako	jako	k8xC,k8xS
země	země	k1gFnSc1
Moravská	moravský	k2eAgFnSc1d1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
29	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
vyhlásili	vyhlásit	k5eAaPmAgMnP
němečtí	německý	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
v	v	k7c6
pohraničí	pohraničí	k1gNnSc6
autonomní	autonomní	k2eAgInPc1d1
celky	celek	k1gInPc1
požadující	požadující	k2eAgFnSc4d1
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Československu	Československo	k1gNnSc6
a	a	k8xC
připojení	připojení	k1gNnSc4
k	k	k7c3
Německému	německý	k2eAgNnSc3d1
Rakousku	Rakousko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
území	území	k1gNnSc4
Moravy	Morava	k1gFnSc2
zasahovaly	zasahovat	k5eAaImAgInP
tyto	tento	k3xDgInPc1
celky	celek	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Sudetsko	Sudetsko	k1gNnSc1
(	(	kIx(
<g/>
Sudetenland	Sudetenland	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
severovýchod	severovýchod	k1gInSc1
Čech	Čechy	k1gFnPc2
včetně	včetně	k7c2
Orlických	orlický	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
severozápad	severozápad	k1gInSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
západ	západ	k1gInSc1
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Opavě	Opava	k1gFnSc6
</s>
<s>
Německá	německý	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
Deutschsüdmähren	Deutschsüdmährno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jih	jih	k1gInSc4
Moravy	Morava	k1gFnSc2
s	s	k7c7
centrem	centrum	k1gNnSc7
ve	v	k7c6
Znojmě	Znojmo	k1gNnSc6
–	–	k?
představitelé	představitel	k1gMnPc1
tohoto	tento	k3xDgInSc2
celku	celek	k1gInSc2
deklarovali	deklarovat	k5eAaBmAgMnP
spojitost	spojitost	k1gFnSc4
s	s	k7c7
Dolními	dolní	k2eAgInPc7d1
Rakousy	Rakousy	k1gInPc7
</s>
<s>
Československo	Československo	k1gNnSc1
obnovilo	obnovit	k5eAaPmAgNnS
územní	územní	k2eAgFnSc4d1
integritu	integrita	k1gFnSc4
s	s	k7c7
těmito	tento	k3xDgInPc7
celky	celek	k1gInPc4
bleskovou	bleskový	k2eAgFnSc7d1
vojenskou	vojenský	k2eAgFnSc7d1
akcí	akce	k1gFnSc7
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
byly	být	k5eAaImAgFnP
během	během	k7c2
prosince	prosinec	k1gInSc2
1918	#num#	k4
prakticky	prakticky	k6eAd1
bez	bez	k7c2
odporu	odpor	k1gInSc2
obsazeny	obsadit	k5eAaPmNgInP
a	a	k8xC
přestaly	přestat	k5eAaPmAgInP
existovat	existovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Se	s	k7c7
vznikem	vznik	k1gInSc7
Československa	Československo	k1gNnSc2
zanikla	zaniknout	k5eAaPmAgFnS
podle	podle	k7c2
ústavy	ústava	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
autonomie	autonomie	k1gFnSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
,	,	kIx,
Moravě	Morava	k1gFnSc6
však	však	k9
zůstaly	zůstat	k5eAaPmAgInP
zemské	zemský	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tatáž	týž	k3xTgFnSc1
ústava	ústava	k1gFnSc1
také	také	k6eAd1
předpokládala	předpokládat	k5eAaImAgFnS
nové	nový	k2eAgNnSc4d1
administrativní	administrativní	k2eAgNnSc4d1
členění	členění	k1gNnSc4
státu	stát	k1gInSc2
na	na	k7c4
menší	malý	k2eAgFnPc4d2
župy	župa	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
by	by	kYmCp3nP
nahradily	nahradit	k5eAaPmAgFnP
stávající	stávající	k2eAgNnSc4d1
členění	členění	k1gNnSc4
na	na	k7c4
země	zem	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Župy	župa	k1gFnSc2
pak	pak	k6eAd1
zřídil	zřídit	k5eAaPmAgInS
zákon	zákon	k1gInSc1
č.	č.	k?
126	#num#	k4
<g/>
/	/	kIx~
<g/>
1920	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
„	„	k?
<g/>
o	o	k7c6
zřízení	zřízení	k1gNnSc6
župních	župní	k2eAgInPc2d1
a	a	k8xC
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
v	v	k7c6
republice	republika	k1gFnSc6
Československé	československý	k2eAgFnSc2d1
<g/>
“	“	k?
ze	z	k7c2
dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
Moravy	Morava	k1gFnSc2
jejich	jejich	k3xOp3gFnSc2
hranice	hranice	k1gFnSc2
nerespektovaly	respektovat	k5eNaImAgFnP
stávající	stávající	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
moravská	moravský	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
s	s	k7c7
Čechami	Čechy	k1gFnPc7
i	i	k8xC
Slovenskem	Slovensko	k1gNnSc7
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
nadále	nadále	k6eAd1
respektována	respektován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Župní	župní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc6
však	však	k9
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
silná	silný	k2eAgNnPc1d1
hlavně	hlavně	k6eAd1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
zejména	zejména	k9
národní	národní	k2eAgFnPc1d1
demokracie	demokracie	k1gFnPc1
odmítaly	odmítat	k5eAaImAgFnP
a	a	k8xC
také	také	k6eAd1
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
byli	být	k5eAaImAgMnP
jejich	jejich	k3xOp3gMnPc1
moravští	moravský	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
slezských	slezský	k2eAgFnPc2d1
a	a	k8xC
slovenských	slovenský	k2eAgFnPc2d1
<g/>
)	)	kIx)
proti	proti	k7c3
tomuto	tento	k3xDgNnSc3
uspořádání	uspořádání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
poukazoval	poukazovat	k5eAaImAgInS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
Morava	Morava	k1gFnSc1
jako	jako	k8xC,k8xS
historický	historický	k2eAgInSc1d1
útvar	útvar	k1gInSc1
se	s	k7c7
staletou	staletý	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
zanikla	zaniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Neuskutečněný	uskutečněný	k2eNgInSc1d1
návrh	návrh	k1gInSc1
na	na	k7c4
rozdělení	rozdělení	k1gNnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
na	na	k7c4
župy	župa	k1gFnPc4
z	z	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
</s>
<s>
Země	země	k1gFnSc1
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
(	(	kIx(
<g/>
vyznačena	vyznačen	k2eAgFnSc1d1
zeleně	zeleň	k1gFnPc1
<g/>
,	,	kIx,
žlutě	žlutě	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
<g/>
)	)	kIx)
na	na	k7c6
mapě	mapa	k1gFnSc6
dnešního	dnešní	k2eAgNnSc2d1
Česka	Česko	k1gNnSc2
</s>
<s>
Definitivní	definitivní	k2eAgFnSc1d1
ústava	ústava	k1gFnSc1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
)	)	kIx)
rozhodla	rozhodnout	k5eAaPmAgFnS
proti	proti	k7c3
zemskému	zemský	k2eAgNnSc3d1
zřízení	zřízení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonodárnou	zákonodárný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
svěřila	svěřit	k5eAaPmAgFnS
dvoukomorovému	dvoukomorový	k2eAgNnSc3d1
Národnímu	národní	k2eAgNnSc3d1
shromáždění	shromáždění	k1gNnSc3
Republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
<g/>
,	,	kIx,
složenému	složený	k2eAgInSc3d1
z	z	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
a	a	k8xC
senátu	senát	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
potvrdila	potvrdit	k5eAaPmAgFnS
neexistenci	neexistence	k1gFnSc4
zemských	zemský	k2eAgInPc2d1
sněmů	sněm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
Moravy	Morava	k1gFnSc2
nadále	nadále	k6eAd1
stál	stát	k5eAaImAgInS
zemský	zemský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
a	a	k8xC
Zemská	zemský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
politická	politický	k2eAgFnSc1d1
(	(	kIx(
<g/>
zemský	zemský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
jejíž	jejíž	k3xOyRp3gFnSc6
čele	čelo	k1gNnSc6
stál	stát	k5eAaImAgMnS
prezident	prezident	k1gMnSc1
Zemské	zemský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
politické	politický	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1920	#num#	k4
byla	být	k5eAaImAgFnS
k	k	k7c3
ČSR	ČSR	kA
připojena	připojen	k2eAgFnSc1d1
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
Dolních	dolní	k2eAgInPc2d1
Rakous	Rakousy	k1gInPc2
(	(	kIx(
<g/>
Valticko	Valticko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
moravských	moravský	k2eAgInPc2d1
politických	politický	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1925	#num#	k4
byla	být	k5eAaImAgFnS
osada	osada	k1gFnSc1
Nedvězíčko	Nedvězíčko	k1gNnSc4
podle	podle	k7c2
vládního	vládní	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
č.	č.	k?
315	#num#	k4
<g/>
/	/	kIx~
<g/>
1924	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
převedena	převést	k5eAaPmNgFnS
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1925	#num#	k4
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
k	k	k7c3
Československu	Československo	k1gNnSc3
a	a	k8xC
zároveň	zároveň	k6eAd1
k	k	k7c3
Moravě	Morava	k1gFnSc6
připojen	připojit	k5eAaPmNgInS
Dyjský	dyjský	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
Valticka	Valticko	k1gNnSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1926	#num#	k4
bylo	být	k5eAaImAgNnS
na	na	k7c4
úkor	úkor	k1gInSc4
tehdejší	tehdejší	k2eAgFnSc2d1
dolnorakouské	dolnorakouský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
Kleinschweinbarth	Kleinschweinbarth	k1gMnSc1
rozšířeno	rozšířen	k2eAgNnSc4d1
území	území	k1gNnSc4
Mikulova	Mikulov	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
vzrůstající	vzrůstající	k2eAgInSc4d1
odpor	odpor	k1gInSc4
byla	být	k5eAaImAgFnS
realizace	realizace	k1gFnSc1
župního	župní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
v	v	k7c6
české	český	k2eAgFnSc6d1
části	část	k1gFnSc6
republiky	republika	k1gFnSc2
stále	stále	k6eAd1
odkládána	odkládán	k2eAgFnSc1d1
<g/>
,	,	kIx,
až	až	k9
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
roku	rok	k1gInSc2
1927	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
vlně	vlna	k1gFnSc6
protestů	protest	k1gInPc2
a	a	k8xC
petičních	petiční	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
,	,	kIx,
opuštěna	opuštěn	k2eAgFnSc1d1
myšlenka	myšlenka	k1gFnSc1
nahradit	nahradit	k5eAaPmF
historické	historický	k2eAgFnPc4d1
země	zem	k1gFnPc4
župami	župa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákonem	zákon	k1gInSc7
ze	z	k7c2
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1927	#num#	k4
č.	č.	k?
125	#num#	k4
<g/>
/	/	kIx~
<g/>
1927	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
o	o	k7c4
organisaci	organisace	k1gFnSc4
politické	politický	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
pak	pak	k6eAd1
s	s	k7c7
účinností	účinnost	k1gFnSc7
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1928	#num#	k4
stala	stát	k5eAaPmAgFnS
Morava	Morava	k1gFnSc1
částí	část	k1gFnPc2
nově	nově	k6eAd1
zřízené	zřízený	k2eAgFnPc1d1
země	zem	k1gFnPc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
samosprávných	samosprávný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yRgFnSc6,k3yIgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
území	území	k1gNnSc1
Československa	Československo	k1gNnSc2
dělilo	dělit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
pro	pro	k7c4
vznik	vznik	k1gInSc4
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
byla	být	k5eAaImAgNnP
nejen	nejen	k6eAd1
relativně	relativně	k6eAd1
malá	malý	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
snaha	snaha	k1gFnSc1
omezit	omezit	k5eAaPmF
politický	politický	k2eAgInSc4d1
vliv	vliv	k1gInSc4
zdejších	zdejší	k2eAgMnPc2d1
sudetských	sudetský	k2eAgMnPc2d1
Němců	Němec	k1gMnPc2
a	a	k8xC
těšínských	těšínský	k2eAgMnPc2d1
Poláků	Polák	k1gMnPc2
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
župního	župní	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
by	by	kYmCp3nS
totiž	totiž	k9
v	v	k7c6
obou	dva	k4xCgFnPc6
slezských	slezský	k2eAgFnPc6d1
župách	župa	k1gFnPc6
měli	mít	k5eAaImAgMnP
Němci	Němec	k1gMnPc1
naprostou	naprostý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Centrem	centr	k1gMnSc7
nově	nově	k6eAd1
zřízené	zřízený	k2eAgFnSc2d1
Země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
sídlil	sídlit	k5eAaImAgInS
Zemský	zemský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
pro	pro	k7c4
zemi	zem	k1gFnSc4
Moravskoslezskou	moravskoslezský	k2eAgFnSc4d1
a	a	k8xC
šedesátičlenné	šedesátičlenný	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
zastupitelstvo	zastupitelstvo	k1gNnSc1
v	v	k7c6
čele	čelo	k1gNnSc6
se	s	k7c7
zemským	zemský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
stál	stát	k5eAaImAgMnS
zároveň	zároveň	k6eAd1
v	v	k7c6
čele	čelo	k1gNnSc6
dvanáctičlenného	dvanáctičlenný	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
článkem	článek	k1gInSc7
územní	územní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
byla	být	k5eAaImAgFnS
zemská	zemský	k2eAgFnSc1d1
samospráva	samospráva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	s	k7c7
vznikem	vznik	k1gInSc7
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
došlo	dojít	k5eAaPmAgNnS
podle	podle	k7c2
vládního	vládní	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1928	#num#	k4
č.	č.	k?
174	#num#	k4
<g/>
/	/	kIx~
<g/>
1928	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
jímž	jenž	k3xRgInSc7
se	se	k3xPyFc4
v	v	k7c6
zemi	zem	k1gFnSc6
České	český	k2eAgFnSc6d1
a	a	k8xC
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
určují	určovat	k5eAaImIp3nP
obvody	obvod	k1gInPc1
a	a	k8xC
sídla	sídlo	k1gNnSc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
a	a	k8xC
některá	některý	k3yIgNnPc1
města	město	k1gNnPc1
se	se	k3xPyFc4
zvláštním	zvláštní	k2eAgInSc7d1
statutem	statut	k1gInSc7
podrobují	podrobovat	k5eAaImIp3nP
všeobecně	všeobecně	k6eAd1
platným	platný	k2eAgNnSc7d1
obecním	obecní	k2eAgNnSc7d1
zřízením	zřízení	k1gNnSc7
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
území	území	k1gNnSc6
bývalé	bývalý	k2eAgFnSc2d1
země	zem	k1gFnSc2
Moravské	moravský	k2eAgFnSc2d1
také	také	k6eAd1
k	k	k7c3
úpravě	úprava	k1gFnSc3
správního	správní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
v	v	k7c6
podobě	podoba	k1gFnSc6
odebrání	odebrání	k1gNnSc2
postavení	postavení	k1gNnSc2
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Jihlavě	Jihlava	k1gFnSc6
<g/>
,	,	kIx,
Kroměříži	Kroměříž	k1gFnSc6
<g/>
,	,	kIx,
Uherskému	uherský	k2eAgNnSc3d1
Hradišti	Hradiště	k1gNnSc3
a	a	k8xC
Znojmu	Znojmo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc2
těchto	tento	k3xDgNnPc2
měst	město	k1gNnPc2
byla	být	k5eAaImAgFnS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
politickými	politický	k2eAgInPc7d1
okresy	okres	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
je	být	k5eAaImIp3nS
dosud	dosud	k6eAd1
obklopovaly	obklopovat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zániku	zánik	k1gInSc3
dosavadních	dosavadní	k2eAgFnPc2d1
Moravských	moravský	k2eAgFnPc2d1
enkláv	enkláva	k1gFnPc2
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
jakožto	jakožto	k8xS
specifického	specifický	k2eAgNnSc2d1
správního	správní	k2eAgNnSc2d1
území	území	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
bývalé	bývalý	k2eAgFnSc2d1
země	zem	k1gFnSc2
Moravské	moravský	k2eAgNnSc1d1
tak	tak	k9
nyní	nyní	k6eAd1
existovalo	existovat	k5eAaImAgNnS
36	#num#	k4
politických	politický	k2eAgInPc2d1
okresů	okres	k1gInPc2
(	(	kIx(
<g/>
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
říjnu	říjen	k1gInSc6
1935	#num#	k4
pak	pak	k6eAd1
podle	podle	k7c2
vládního	vládní	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
č.	č.	k?
104	#num#	k4
<g/>
/	/	kIx~
<g/>
1935	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
se	se	k3xPyFc4
v	v	k7c6
zemích	zem	k1gFnPc6
České	český	k2eAgFnSc2d1
a	a	k8xC
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
provádějí	provádět	k5eAaImIp3nP
změny	změna	k1gFnPc1
obvodů	obvod	k1gInPc2
některých	některý	k3yIgInPc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
přibyl	přibýt	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
okres	okres	k1gInSc1
Zlín	Zlín	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
statutární	statutární	k2eAgNnPc1d1
města	město	k1gNnPc1
Brno	Brno	k1gNnSc1
a	a	k8xC
Olomouc	Olomouc	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mnichovskou	mnichovský	k2eAgFnSc7d1
dohodou	dohoda	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1938	#num#	k4
byly	být	k5eAaImAgFnP
z	z	k7c2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
země	zem	k1gFnSc2
vytrženy	vytržen	k2eAgFnPc4d1
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
převážně	převážně	k6eAd1
německojazyčné	německojazyčný	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
na	na	k7c6
severu	sever	k1gInSc6
(	(	kIx(
<g/>
většina	většina	k1gFnSc1
území	území	k1gNnSc2
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
německé	německý	k2eAgFnSc2d1
župy	župa	k1gFnSc2
Sudetenland	Sudetenlanda	k1gFnPc2
<g/>
,	,	kIx,
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
–	–	k?
Hlučínsko	Hlučínsko	k1gNnSc1
–	–	k?
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
pruské	pruský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
Slezsko	Slezsko	k1gNnSc1
<g/>
;	;	kIx,
část	část	k1gFnSc1
území	území	k1gNnSc2
na	na	k7c6
Těšínsku	Těšínsko	k1gNnSc6
připojilo	připojit	k5eAaPmAgNnS
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
po	po	k7c6
jehož	jehož	k3xOyRp3gNnSc6
obsazení	obsazení	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
rovněž	rovněž	k9
připojeno	připojit	k5eAaPmNgNnS
k	k	k7c3
pruské	pruský	k2eAgFnSc3d1
provincii	provincie	k1gFnSc3
Slezsko	Slezsko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
jihu	jih	k1gInSc2
(	(	kIx(
<g/>
tato	tento	k3xDgFnSc1
oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
Sudetoněmeckých	sudetoněmecký	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
15	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1939	#num#	k4
do	do	k7c2
Zemského	zemský	k2eAgNnSc2d1
hejtmanství	hejtmanství	k1gNnSc2
Dolní	dolní	k2eAgNnSc1d1
Podunají	Podunají	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1939	#num#	k4
přeměněno	přeměnit	k5eAaPmNgNnS
v	v	k7c4
říšskou	říšský	k2eAgFnSc4d1
župu	župa	k1gFnSc4
Dolní	dolní	k2eAgNnSc1d1
Podunají	Podunají	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Správní	správní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Země	zem	k1gFnSc2
moravskoslezské	moravskoslezský	k2eAgFnSc2d1
1938	#num#	k4
</s>
<s>
Německá	německý	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
byl	být	k5eAaImAgInS
zbytek	zbytek	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
obsazen	obsazen	k2eAgInSc1d1
nacistickým	nacistický	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
začleněn	začlenit	k5eAaPmNgInS
do	do	k7c2
Protektorátu	protektorát	k1gInSc2
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
byl	být	k5eAaImAgInS
až	až	k9
do	do	k7c2
osvobození	osvobození	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
(	(	kIx(
<g/>
tedy	tedy	k8xC
ještě	ještě	k9
za	za	k7c2
formálně	formálně	k6eAd1
nezávislého	závislý	k2eNgNnSc2d1
Česko-Slovenska	Česko-Slovensko	k1gNnSc2
<g/>
)	)	kIx)
započala	započnout	k5eAaPmAgFnS
na	na	k7c6
Moravě	Morava	k1gFnSc6
výstavba	výstavba	k1gFnSc1
německé	německý	k2eAgFnSc2d1
exteritoriální	exteritoriální	k2eAgFnSc2d1
dálnice	dálnice	k1gFnSc2
Vídeň	Vídeň	k1gFnSc1
<g/>
–	–	k?
Vratislav	Vratislav	k1gFnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
měla	mít	k5eAaImAgFnS
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
napomoci	napomoct	k5eAaPmF
rychlejší	rychlý	k2eAgFnSc4d2
germanizaci	germanizace	k1gFnSc4
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1940	#num#	k4
protektorátní	protektorátní	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
svým	svůj	k3xOyFgNnSc7
nařízením	nařízení	k1gNnSc7
č.	č.	k?
388	#num#	k4
<g/>
/	/	kIx~
<g/>
1940	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
o	o	k7c6
některých	některý	k3yIgFnPc6
změnách	změna	k1gFnPc6
obvodů	obvod	k1gInPc2
zemských	zemský	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
připojila	připojit	k5eAaPmAgFnS
k	k	k7c3
zemi	zem	k1gFnSc3
Moravské	moravský	k2eAgFnSc2d1
některé	některý	k3yIgFnPc4
okrajové	okrajový	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
východních	východní	k2eAgFnPc2d1
a	a	k8xC
jihovýchodních	jihovýchodní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Českomoravské	českomoravský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
stejnému	stejný	k2eAgNnSc3d1
datu	datum	k1gNnSc3
vstoupilo	vstoupit	k5eAaPmAgNnS
v	v	k7c4
platnost	platnost	k1gFnSc4
i	i	k8xC
související	související	k2eAgNnSc4d1
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
ze	z	k7c2
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1940	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
389	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
upravují	upravovat	k5eAaImIp3nP
obvody	obvod	k1gInPc1
a	a	k8xC
sídla	sídlo	k1gNnPc1
některých	některý	k3yIgInPc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
jímž	jenž	k3xRgNnSc7
se	se	k3xPyFc4
měnilo	měnit	k5eAaImAgNnS
vymezení	vymezení	k1gNnSc1
několika	několik	k4yIc2
politických	politický	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
jiné	jiný	k2eAgInPc1d1
politické	politický	k2eAgInPc1d1
okresy	okres	k1gInPc1
rušily	rušit	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
pro	pro	k7c4
tyto	tento	k3xDgFnPc4
změny	změna	k1gFnPc4
byl	být	k5eAaImAgInS
především	především	k6eAd1
tlak	tlak	k1gInSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
německého	německý	k2eAgInSc2d1
ostrůvku	ostrůvek	k1gInSc2
na	na	k7c6
Jihlavsku	Jihlavsko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
Moravě	Morava	k1gFnSc3
tak	tak	k9
byly	být	k5eAaImAgFnP
připojeny	připojit	k5eAaPmNgInP
soudní	soudní	k2eAgInPc1d1
okresy	okres	k1gInPc1
Polná	Polná	k1gFnSc1
<g/>
,	,	kIx,
Štoky	štok	k1gInPc1
(	(	kIx(
<g/>
k	k	k7c3
němuž	jenž	k3xRgNnSc3
byla	být	k5eAaImAgFnS
připojena	připojen	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
soudního	soudní	k2eAgInSc2d1
okresu	okres	k1gInSc2
Německý	německý	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
byly	být	k5eAaImAgInP
začleněny	začlenit	k5eAaPmNgInP
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Jihlava	Jihlava	k1gFnSc1
<g/>
;	;	kIx,
soudní	soudní	k2eAgInSc1d1
okres	okres	k1gInSc1
Přibyslav	Přibyslava	k1gFnPc2
(	(	kIx(
<g/>
k	k	k7c3
němuž	jenž	k3xRgNnSc3
byla	být	k5eAaImAgFnS
připojena	připojen	k2eAgFnSc1d1
i	i	k8xC
severovýchodní	severovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
soudního	soudní	k2eAgInSc2d1
okresu	okres	k1gInSc2
Německý	německý	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
začleněn	začlenit	k5eAaPmNgInS
do	do	k7c2
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Nové	Nové	k2eAgNnSc1d1
Město	město	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
;	;	kIx,
protektorátní	protektorátní	k2eAgFnSc1d1
část	část	k1gFnSc1
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
začleněn	začleněn	k2eAgMnSc1d1
k	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
nově	nově	k6eAd1
zřízenému	zřízený	k2eAgNnSc3d1
politickému	politický	k2eAgNnSc3d1
okresu	okres	k1gInSc2
Telč	Telč	k1gFnSc1
(	(	kIx(
<g/>
okresní	okresní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
sem	sem	k6eAd1
byl	být	k5eAaImAgInS
přeložen	přeložit	k5eAaPmNgInS
z	z	k7c2
Dačic	Dačice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
sice	sice	k8xC
ještě	ještě	k6eAd1
náležely	náležet	k5eAaImAgInP
k	k	k7c3
protektorátu	protektorát	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
byly	být	k5eAaImAgFnP
těsně	těsně	k6eAd1
u	u	k7c2
jeho	jeho	k3xOp3gFnPc2
hranic	hranice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgFnSc1d1
část	část	k1gFnSc1
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Polička	Polička	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Moravy	Morava	k1gFnSc2
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
soudního	soudní	k2eAgInSc2d1
okresu	okres	k1gInSc2
Kunštát	Kunštát	k1gInSc1
v	v	k7c6
politickém	politický	k2eAgInSc6d1
okrese	okres	k1gInSc6
Boskovice	Boskovice	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
dubna	duben	k1gInSc2
1942	#num#	k4
byly	být	k5eAaImAgFnP
práce	práce	k1gFnPc1
na	na	k7c6
výše	vysoce	k6eAd2
zmíněné	zmíněný	k2eAgFnSc6d1
exteritoriální	exteritoriální	k2eAgFnSc6d1
dálnici	dálnice	k1gFnSc6
přerušeny	přerušen	k2eAgMnPc4d1
a	a	k8xC
již	již	k6eAd1
nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
nepokračovalo	pokračovat	k5eNaImAgNnS
v	v	k7c6
její	její	k3xOp3gFnSc6
výstavbě	výstavba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
protektorátu	protektorát	k1gInSc2
prováděli	provádět	k5eAaImAgMnP
nacisté	nacista	k1gMnPc1
na	na	k7c4
území	území	k1gNnSc4
Moravy	Morava	k1gFnSc2
organizovaný	organizovaný	k2eAgInSc1d1
útisk	útisk	k1gInSc1
a	a	k8xC
terorizování	terorizování	k1gNnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
po	po	k7c6
obsazení	obsazení	k1gNnSc6
zbytku	zbytek	k1gInSc2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
byly	být	k5eAaImAgFnP
vypáleny	vypálen	k2eAgFnPc1d1
například	například	k6eAd1
synagogy	synagoga	k1gFnPc4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
Jihlavě	Jihlava	k1gFnSc6
a	a	k8xC
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
uzavření	uzavření	k1gNnSc6
českých	český	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
koncem	koncem	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
byla	být	k5eAaImAgFnS
řada	řada	k1gFnSc1
vysokoškolských	vysokoškolský	k2eAgFnPc2d1
budov	budova	k1gFnPc2
a	a	k8xC
kolejí	kolej	k1gFnPc2
přeměněna	přeměnit	k5eAaPmNgFnS
na	na	k7c4
věznice	věznice	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
Kounicovy	Kounicův	k2eAgFnPc1d1
koleje	kolej	k1gFnPc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
úřadovny	úřadovna	k1gFnSc2
gestapa	gestapo	k1gNnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
budova	budova	k1gFnSc1
Právnické	právnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
okupace	okupace	k1gFnSc2
se	se	k3xPyFc4
též	též	k9
nově	nově	k6eAd1
objevil	objevit	k5eAaPmAgInS
problém	problém	k1gInSc1
moravského	moravský	k2eAgInSc2d1
separatismu	separatismus	k1gInSc2
a	a	k8xC
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
hranice	hranice	k1gFnSc2
naopak	naopak	k6eAd1
snaha	snaha	k1gFnSc1
sousedního	sousední	k2eAgInSc2d1
Slovenského	slovenský	k2eAgInSc2d1
štátu	štát	k1gInSc2
těchto	tento	k3xDgInPc2
jevů	jev	k1gInPc2
využít	využít	k5eAaPmF
<g/>
,	,	kIx,
rozšířit	rozšířit	k5eAaPmF
slovenské	slovenský	k2eAgNnSc4d1
území	území	k1gNnSc4
o	o	k7c4
podstatnou	podstatný	k2eAgFnSc4d1
část	část	k1gFnSc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
vytvořit	vytvořit	k5eAaPmF
tak	tak	k9
„	„	k?
<g/>
Velké	velký	k2eAgNnSc1d1
Slovensko	Slovensko	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravsko-slovenská	moravsko-slovenský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
vyjádřila	vyjádřit	k5eAaPmAgFnS
již	již	k6eAd1
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1939	#num#	k4
přání	přání	k1gNnPc2
připojit	připojit	k5eAaPmF
Slovácko	Slovácko	k1gNnSc1
k	k	k7c3
právě	právě	k9
vzniklému	vzniklý	k2eAgNnSc3d1
Slovensku	Slovensko	k1gNnSc3
<g/>
:	:	kIx,
„	„	k?
<g/>
Moravští	moravský	k2eAgMnPc1d1
Slováci	Slovák	k1gMnPc1
z	z	k7c2
kraje	kraj	k1gInSc2
hodonínského	hodonínský	k2eAgInSc2d1
<g/>
,	,	kIx,
strážnického	strážnický	k2eAgInSc2d1
<g/>
,	,	kIx,
kyjovského	kyjovský	k2eAgInSc2d1
<g/>
,	,	kIx,
hradišťského	hradišťský	k2eAgInSc2d1
i	i	k8xC
uherskobrodského	uherskobrodský	k2eAgInSc2d1
se	se	k3xPyFc4
počítají	počítat	k5eAaImIp3nP
ke	k	k7c3
slovenskému	slovenský	k2eAgInSc3d1
národu	národ	k1gInSc3
a	a	k8xC
vítají	vítat	k5eAaImIp3nP
vytvoření	vytvoření	k1gNnSc4
samostatné	samostatný	k2eAgFnSc2d1
suverénní	suverénní	k2eAgFnSc2d1
Slovenské	slovenský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc4
suverenitu	suverenita	k1gFnSc4
uznávají	uznávat	k5eAaImIp3nP
i	i	k9
nad	nad	k7c7
krajem	kraj	k1gInSc7
moravských	moravský	k2eAgMnPc2d1
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
spolek	spolek	k1gInSc1
Národopisná	národopisný	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
podporoval	podporovat	k5eAaImAgMnS
tyto	tento	k3xDgInPc4
záměry	záměr	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
1940	#num#	k4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
slovenský	slovenský	k2eAgMnSc1d1
premiér	premiér	k1gMnSc1
Vojtech	Vojta	k1gMnPc6
Tuka	Tukum	k1gNnPc1
memorandum	memorandum	k1gNnSc4
Adolfu	Adolf	k1gMnSc3
Hitlerovi	Hitler	k1gMnSc3
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
na	na	k7c6
hranicích	hranice	k1gFnPc6
protektorátu	protektorát	k1gInSc2
žije	žít	k5eAaImIp3nS
půl	půl	k1xP
milionu	milion	k4xCgInSc2
moravských	moravský	k2eAgMnPc2d1
Slováků	Slovák	k1gMnPc2
bez	bez	k7c2
menšinových	menšinový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gNnPc2
žádá	žádat	k5eAaImIp3nS
připojení	připojení	k1gNnSc4
ke	k	k7c3
Slovensku	Slovensko	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
Slovensku	Slovensko	k1gNnSc3
se	se	k3xPyFc4
podle	podle	k7c2
memoranda	memorandum	k1gNnSc2
měla	mít	k5eAaImAgFnS
připojit	připojit	k5eAaPmF
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
užší	úzký	k2eAgFnSc6d2
variantě	varianta	k1gFnSc6
<g/>
:	:	kIx,
oblast	oblast	k1gFnSc1
s	s	k7c7
městy	město	k1gNnPc7
Hodonín	Hodonín	k1gInSc1
<g/>
,	,	kIx,
Kyjov	Kyjov	k1gInSc1
<g/>
,	,	kIx,
Strážnice	Strážnice	k1gFnSc1
<g/>
,	,	kIx,
Veselí	veselí	k1gNnSc1
nad	nad	k7c7
Moravou	Morava	k1gFnSc7
<g/>
,	,	kIx,
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Otrokovice	Otrokovice	k1gFnPc1
<g/>
,	,	kIx,
Zlín	Zlín	k1gInSc1
<g/>
,	,	kIx,
Hranice	hranice	k1gFnSc1
<g/>
,	,	kIx,
Valašské	valašský	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
<g/>
,	,	kIx,
Vsetín	Vsetín	k1gInSc1
a	a	k8xC
Bylnice	Bylnice	k1gFnSc1
</s>
<s>
v	v	k7c6
širší	široký	k2eAgFnSc6d2
variantě	varianta	k1gFnSc6
<g/>
:	:	kIx,
oblast	oblast	k1gFnSc1
vymezená	vymezený	k2eAgFnSc1d1
trasou	trasa	k1gFnSc7
Valtice-Poštorná-Chorvatská	Valtice-Poštorná-Chorvatský	k2eAgFnSc1d1
Nová	nový	k2eAgFnSc1d1
Ves-Podivín-Šakvice	Ves-Podivín-Šakvice	k1gFnSc1
<g/>
,	,	kIx,
následně	následně	k6eAd1
shodně	shodně	k6eAd1
s	s	k7c7
první	první	k4xOgFnSc7
mapou	mapa	k1gFnSc7
až	až	k6eAd1
k	k	k7c3
městu	město	k1gNnSc3
Kroměříž	Kroměříž	k1gFnSc1
a	a	k8xC
odtud	odtud	k6eAd1
přes	přes	k7c4
Holešov-Bystřici	Holešov-Bystřice	k1gFnSc4
pod	pod	k7c4
Hostýnem-Teplici	Hostýnem-Teplice	k1gFnSc4
nad	nad	k7c7
Bečvou-Spálov-Odry	Bečvou-Spálov-Odr	k1gInPc7
</s>
<s>
Němci	Němec	k1gMnPc1
nereagovali	reagovat	k5eNaBmAgMnP
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
sepsali	sepsat	k5eAaPmAgMnP
slovenští	slovenský	k2eAgMnPc1d1
politici	politik	k1gMnPc1
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
žádost	žádost	k1gFnSc4
podruhé	podruhé	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc1
tyto	tento	k3xDgInPc1
pokusy	pokus	k1gInPc1
byly	být	k5eAaImAgInP
ukončeny	ukončen	k2eAgInPc4d1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
červnu	červen	k1gInSc6
1941	#num#	k4
K.	K.	kA
H.	H.	kA
Frank	Frank	k1gMnSc1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gMnSc6
zejména	zejména	k9
Hitler	Hitler	k1gMnSc1
postavili	postavit	k5eAaPmAgMnP
ostře	ostro	k6eAd1
proti	proti	k7c3
–	–	k?
především	především	k9
z	z	k7c2
důvodu	důvod	k1gInSc2
zachování	zachování	k1gNnSc4
zbrojní	zbrojní	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
a	a	k8xC
také	také	k9
kvůli	kvůli	k7c3
germanizačním	germanizační	k2eAgInPc3d1
plánům	plán	k1gInPc3
nechtěla	chtít	k5eNaImAgFnS
Německá	německý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
svému	svůj	k3xOyFgMnSc3
slovenskému	slovenský	k2eAgMnSc3d1
vazalovi	vazal	k1gMnSc3
v	v	k7c6
této	tento	k3xDgFnSc6
záležitosti	záležitost	k1gFnSc6
ustoupit	ustoupit	k5eAaPmF
a	a	k8xC
moravské	moravský	k2eAgNnSc4d1
území	území	k1gNnSc4
mu	on	k3xPp3gMnSc3
předat	předat	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
</s>
<s>
Po	po	k7c6
osvobození	osvobození	k1gNnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
obnovení	obnovení	k1gNnSc3
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
a	a	k8xC
jejího	její	k3xOp3gNnSc2
původního	původní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
na	na	k7c4
politické	politický	k2eAgInPc4d1
okresy	okres	k1gInPc4
ke	k	k7c3
stavu	stav	k1gInSc3
ze	z	k7c2
září	září	k1gNnSc2
1938	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odchylkou	odchylka	k1gFnSc7
od	od	k7c2
původního	původní	k2eAgInSc2d1
stavu	stav	k1gInSc2
bylo	být	k5eAaImAgNnS
nové	nový	k2eAgNnSc4d1
statutární	statutární	k2eAgNnSc4d1
město	město	k1gNnSc4
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1946	#num#	k4
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzniklé	vzniklý	k2eAgFnSc3d1
jako	jako	k8xS,k8xC
městský	městský	k2eAgInSc1d1
okres	okres	k1gInSc1
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
,	,	kIx,
dále	daleko	k6eAd2
uznání	uznání	k1gNnSc4
protektorátní	protektorátní	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
provedeného	provedený	k2eAgNnSc2d1
začlenění	začlenění	k1gNnSc2
téměř	téměř	k6eAd1
celého	celý	k2eAgInSc2d1
soudního	soudní	k2eAgInSc2d1
okresu	okres	k1gInSc2
Frýdku	Frýdek	k1gInSc2
a	a	k8xC
nepatrné	patrný	k2eNgFnPc1d1,k2eAgFnPc1d1
části	část	k1gFnPc1
soudního	soudní	k2eAgInSc2d1
okresu	okres	k1gInSc2
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
do	do	k7c2
politického	politický	k2eAgInSc2d1
okresu	okres	k1gInSc2
Místek	Místek	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
městysu	městys	k1gInSc2
Líšně	Líšeň	k1gFnSc2
ke	k	k7c3
statutárnímu	statutární	k2eAgNnSc3d1
městu	město	k1gNnSc3
Brnu	Brno	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
obnovení	obnovení	k1gNnSc1
postavení	postavení	k1gNnSc2
statutárního	statutární	k2eAgNnSc2d1
města	město	k1gNnSc2
Opavě	Opava	k1gFnSc6
spojené	spojený	k2eAgInPc1d1
s	s	k7c7
připojením	připojení	k1gNnSc7
3	#num#	k4
sousedních	sousední	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
k	k	k7c3
městu	město	k1gNnSc3
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
1946	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
požadavek	požadavek	k1gInSc4
obnovení	obnovení	k1gNnSc2
slezské	slezský	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
Slezské	slezský	k2eAgFnSc2d1
expozitury	expozitura	k1gFnSc2
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnPc4d1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
,	,	kIx,
do	do	k7c2
níž	jenž	k3xRgFnSc2
byly	být	k5eAaImAgFnP
vedle	vedle	k7c2
území	území	k1gNnSc2
původního	původní	k2eAgNnSc2d1
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
začleněny	začlenit	k5eAaPmNgInP
také	také	k9
původně	původně	k6eAd1
moravské	moravský	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
okresy	okres	k1gInPc4
Nový	nový	k2eAgInSc1d1
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
a	a	k8xC
Místek	Místek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
proběhly	proběhnout	k5eAaPmAgFnP
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1946	#num#	k4
<g/>
,	,	kIx,
získali	získat	k5eAaPmAgMnP
komunisté	komunista	k1gMnPc1
vliv	vliv	k1gInSc4
také	také	k9
v	v	k7c6
zemi	zem	k1gFnSc6
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
se	se	k3xPyFc4
promítly	promítnout	k5eAaPmAgInP
i	i	k9
do	do	k7c2
vedení	vedení	k1gNnSc2
a	a	k8xC
činnosti	činnost	k1gFnSc2
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Zemského	zemský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
zemi	zem	k1gFnSc4
Moravskoslezskou	moravskoslezský	k2eAgFnSc4d1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
novým	nový	k2eAgMnSc7d1
předsedou	předseda	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1946	#num#	k4
člen	člen	k1gInSc1
KSČ	KSČ	kA
a	a	k8xC
šéfredaktor	šéfredaktor	k1gMnSc1
jejího	její	k3xOp3gInSc2
moravského	moravský	k2eAgInSc2d1
deníku	deník	k1gInSc2
Rovnost	rovnost	k1gFnSc1
František	František	k1gMnSc1
Píšek	Píšek	k1gMnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
zemský	zemský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
podstatně	podstatně	k6eAd1
poslušnějším	poslušný	k2eAgInSc7d2
nástrojem	nástroj	k1gInSc7
převážně	převážně	k6eAd1
komunistické	komunistický	k2eAgFnSc2d1
československé	československý	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
po	po	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
</s>
<s>
Náměstí	náměstí	k1gNnSc1
Svobody	svoboda	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
</s>
<s>
Po	po	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
začal	začít	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
pracovat	pracovat	k5eAaImF
na	na	k7c6
správní	správní	k2eAgFnSc6d1
reorganizaci	reorganizace	k1gFnSc6
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledkem	výsledek	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
kromě	kromě	k7c2
jiného	jiné	k1gNnSc2
<g/>
,	,	kIx,
zrušení	zrušení	k1gNnSc1
zemského	zemský	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
a	a	k8xC
rozčlenění	rozčlenění	k1gNnSc2
republiky	republika	k1gFnSc2
na	na	k7c4
kraje	kraj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobu	podoba	k1gFnSc4
krajského	krajský	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
schválila	schválit	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
republiky	republika	k1gFnSc2
v	v	k7c6
listopadu	listopad	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodla	rozhodnout	k5eAaPmAgFnS
o	o	k7c6
zřízení	zřízení	k1gNnSc6
13	#num#	k4
krajů	kraj	k1gInPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
šesti	šest	k4xCc2
krajů	kraj	k1gInPc2
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc4d1
shromáždění	shromáždění	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
změny	změna	k1gFnSc2
odsouhlasilo	odsouhlasit	k5eAaPmAgNnS
a	a	k8xC
zavedlo	zavést	k5eAaPmAgNnS
úplnou	úplný	k2eAgFnSc4d1
centralizaci	centralizace	k1gFnSc4
správního	správní	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1948	#num#	k4
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Československa	Československo	k1gNnSc2
rozděleno	rozdělit	k5eAaPmNgNnS
mezi	mezi	k7c4
nově	nově	k6eAd1
vzniklé	vzniklý	k2eAgInPc4d1
kraje	kraj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nerespektovaly	respektovat	k5eNaImAgFnP
dřívější	dřívější	k2eAgFnPc4d1
historické	historický	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývalá	bývalý	k2eAgFnSc1d1
země	země	k1gFnSc1
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
kraje	kraj	k1gInPc4
Brněnský	brněnský	k2eAgInSc1d1
<g/>
,	,	kIx,
Gottwaldovský	gottwaldovský	k2eAgInSc1d1
<g/>
,	,	kIx,
Jihlavský	jihlavský	k2eAgInSc1d1
(	(	kIx(
<g/>
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zasahoval	zasahovat	k5eAaImAgInS
hluboko	hluboko	k6eAd1
do	do	k7c2
Čech	Čechy	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
a	a	k8xC
Ostravský	ostravský	k2eAgMnSc1d1
<g/>
;	;	kIx,
několik	několik	k4yIc1
dříve	dříve	k6eAd2
moravských	moravský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
kraje	kraj	k1gInSc2
Českobudějovického	českobudějovický	k2eAgInSc2d1
a	a	k8xC
Pardubického	pardubický	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1960	#num#	k4
Národní	národní	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
schválilo	schválit	k5eAaPmAgNnS
zákon	zákon	k1gInSc4
o	o	k7c6
reorganizaci	reorganizace	k1gFnSc6
územní	územní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
se	se	k3xPyFc4
rušilo	rušit	k5eAaImAgNnS
členění	členění	k1gNnSc1
platné	platný	k2eAgNnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstoupil	vstoupit	k5eAaPmAgMnS
v	v	k7c4
platnost	platnost	k1gFnSc4
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1960	#num#	k4
a	a	k8xC
výrazně	výrazně	k6eAd1
se	se	k3xPyFc4
jím	on	k3xPp3gMnSc7
snižoval	snižovat	k5eAaImAgMnS
počet	počet	k1gInSc4
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
ani	ani	k8xC
nové	nový	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
nekopírovalo	kopírovat	k5eNaImAgNnS
historické	historický	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
této	tento	k3xDgFnSc2
reformy	reforma	k1gFnSc2
byla	být	k5eAaImAgFnS
převážná	převážný	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
někdejší	někdejší	k2eAgFnSc2d1
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
přerozdělena	přerozdělit	k5eAaPmNgNnP
mezi	mezi	k7c4
kraje	kraj	k1gInPc4
Jihomoravský	jihomoravský	k2eAgMnSc1d1
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
i	i	k9
osada	osada	k1gFnSc1
Jobova	Jobův	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
<g/>
,	,	kIx,
historicky	historicky	k6eAd1
patřící	patřící	k2eAgMnSc1d1
k	k	k7c3
Čechám	Čechy	k1gFnPc3
<g/>
)	)	kIx)
a	a	k8xC
Severomoravský	severomoravský	k2eAgMnSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
území	území	k1gNnSc1
na	na	k7c6
severozápadě	severozápad	k1gInSc6
Moravy	Morava	k1gFnSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
Svitav	Svitava	k1gFnPc2
a	a	k8xC
Moravské	moravský	k2eAgFnSc2d1
Třebové	Třebová	k1gFnSc2
bylo	být	k5eAaImAgNnS
začleněno	začlenit	k5eAaPmNgNnS
do	do	k7c2
kraje	kraj	k1gInSc2
Východočeského	východočeský	k2eAgInSc2d1
(	(	kIx(
<g/>
roku	rok	k1gInSc2
2000	#num#	k4
bylo	být	k5eAaImAgNnS
začleněno	začlenit	k5eAaPmNgNnS
do	do	k7c2
kraje	kraj	k1gInSc2
Pardubického	pardubický	k2eAgInSc2d1
<g/>
)	)	kIx)
a	a	k8xC
území	území	k1gNnSc1
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
Dačicko	Dačicko	k1gNnSc1
<g/>
)	)	kIx)
ke	k	k7c3
kraji	kraj	k1gInSc3
Jihočeskému	jihočeský	k2eAgInSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
katastrálních	katastrální	k2eAgFnPc2d1
území	území	k1gNnPc2
Vesce	Vesce	k1gFnSc2
u	u	k7c2
Počátek	počátek	k1gInSc1
<g/>
,	,	kIx,
Prostý	prostý	k2eAgInSc1d1
a	a	k8xC
Horní	horní	k2eAgInSc1d1
Vilímeč	Vilímeč	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
nyní	nyní	k6eAd1
náležejí	náležet	k5eAaImIp3nP
k	k	k7c3
českému	český	k2eAgNnSc3d1
městu	město	k1gNnSc3
Počátky	počátek	k1gInPc4
v	v	k7c6
kraji	kraj	k1gInSc6
Vysočina	vysočina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
toto	tento	k3xDgNnSc1
území	území	k1gNnSc1
po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
přičleněno	přičlenit	k5eAaPmNgNnS
ke	k	k7c3
kraji	kraj	k1gInSc3
Budějovickému	budějovický	k2eAgInSc3d1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Jihočeskému	jihočeský	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
byl	být	k5eAaImAgMnS
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
poslanců	poslanec	k1gMnPc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
nově	nova	k1gFnSc3
založené	založený	k2eAgFnSc2d1
Společnosti	společnost	k1gFnSc2
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
Slezsko	Slezsko	k1gNnSc4
podán	podat	k5eAaPmNgInS
„	„	k?
<g/>
Návrh	návrh	k1gInSc1
na	na	k7c4
státoprávní	státoprávní	k2eAgNnSc4d1
a	a	k8xC
územní	územní	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
Československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
měla	mít	k5eAaImAgFnS
stát	stát	k5eAaImF,k5eAaPmF
trojdílnou	trojdílný	k2eAgFnSc7d1
federací	federace	k1gFnSc7
tří	tři	k4xCgInPc2
rovnoprávných	rovnoprávný	k2eAgInPc2d1
státoprávních	státoprávní	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
se	s	k7c7
Slezskem	Slezsko	k1gNnSc7
a	a	k8xC
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc4
však	však	k8xC
přijat	přijat	k2eAgInSc1d1
nebyl	být	k5eNaImAgInS
a	a	k8xC
stát	stát	k1gInSc1
byl	být	k5eAaImAgInS
uspořádán	uspořádat	k5eAaPmNgInS
federativně	federativně	k6eAd1
na	na	k7c6
národnostním	národnostní	k2eAgInSc6d1
principu	princip	k1gInSc6
dvou	dva	k4xCgInPc2
hlavních	hlavní	k2eAgInPc2d1
národů	národ	k1gInPc2
Čechů	Čech	k1gMnPc2
a	a	k8xC
Slováků	Slovák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentem	parlament	k1gInSc7
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
Česká	český	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
již	jenž	k3xRgFnSc4
tvořili	tvořit	k5eAaImAgMnP
poslanci	poslanec	k1gMnPc1
z	z	k7c2
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
Moravy	Morava	k1gFnSc2
i	i	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Federální	federální	k2eAgNnSc1d1
shromáždění	shromáždění	k1gNnSc1
dne	den	k1gInSc2
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1990	#num#	k4
přijalo	přijmout	k5eAaPmAgNnS
usnesení	usnesení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
odsoudilo	odsoudit	k5eAaPmAgNnS
zrušení	zrušení	k1gNnSc4
Země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
a	a	k8xC
vyslovilo	vyslovit	k5eAaPmAgNnS
v	v	k7c6
něm	on	k3xPp3gNnSc6
pevné	pevný	k2eAgNnSc1d1
přesvědčení	přesvědčení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
nové	nový	k2eAgNnSc1d1
ústavní	ústavní	k2eAgNnSc1d1
uspořádání	uspořádání	k1gNnSc1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
vzejde	vzejít	k5eAaPmIp3nS
z	z	k7c2
jednání	jednání	k1gNnSc2
svobodně	svobodně	k6eAd1
zvolených	zvolený	k2eAgInPc2d1
zákonodárných	zákonodárný	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
,	,	kIx,
tuto	tento	k3xDgFnSc4
nespravedlnost	nespravedlnost	k1gFnSc4
napraví	napravit	k5eAaPmIp3nS
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Poslanci	poslanec	k1gMnPc1
moravských	moravský	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
zvolení	zvolení	k1gNnSc4
v	v	k7c6
prvních	první	k4xOgFnPc6
svobodných	svobodný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
předložili	předložit	k5eAaPmAgMnP
návrh	návrh	k1gInSc4
zákona	zákon	k1gInSc2
na	na	k7c4
obnovení	obnovení	k1gNnSc4
zemského	zemský	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsednictvo	předsednictvo	k1gNnSc1
ČNR	ČNR	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
mělo	mít	k5eAaImAgNnS
převahu	převaha	k1gFnSc4
Občanské	občanský	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
<g/>
,	,	kIx,
jej	on	k3xPp3gNnSc4
však	však	k9
ve	v	k7c6
výborech	výbor	k1gInPc6
zamítlo	zamítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
počátkem	počátkem	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
pak	pak	k9
proběhly	proběhnout	k5eAaPmAgFnP
ještě	ještě	k9
další	další	k2eAgFnPc1d1
snahy	snaha	k1gFnPc1
o	o	k7c4
obnovu	obnova	k1gFnSc4
Moravy	Morava	k1gFnSc2
jako	jako	k8xC,k8xS
autonomní	autonomní	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
však	však	k9
vyšly	vyjít	k5eAaPmAgFnP
naprázdno	naprázdno	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
1992	#num#	k4
předložila	předložit	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
Petra	Petr	k1gMnSc2
Pitharta	Pithart	k1gMnSc2
(	(	kIx(
<g/>
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
návrh	návrh	k1gInSc1
na	na	k7c4
zemské	zemský	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jak	jak	k6eAd1
říká	říkat	k5eAaImIp3nS
politik	politik	k1gMnSc1
po	po	k7c6
letech	léto	k1gNnPc6
<g/>
:	:	kIx,
Zklamal	zklamat	k5eAaPmAgInS
mě	já	k3xPp1nSc2
rozpad	rozpad	k1gInSc1
společného	společný	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
zklamala	zklamat	k5eAaPmAgFnS
mě	já	k3xPp1nSc4
prosazená	prosazený	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
územně	územně	k6eAd1
správního	správní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moje	můj	k3xOp1gInPc4
vláda	vláda	k1gFnSc1
předložila	předložit	k5eAaPmAgFnS
na	na	k7c6
jaře	jaro	k1gNnSc6
1992	#num#	k4
návrh	návrh	k1gInSc1
na	na	k7c4
zemské	zemský	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
se	s	k7c7
zachováním	zachování	k1gNnSc7
bývalých	bývalý	k2eAgInPc2d1
okresů	okres	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
ani	ani	k8xC
neprojednával	projednávat	k5eNaImAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
blížil	blížit	k5eAaImAgInS
rozpad	rozpad	k1gInSc1
federace	federace	k1gFnSc2
a	a	k8xC
byly	být	k5eAaImAgFnP
obavy	obava	k1gFnPc1
z	z	k7c2
„	„	k?
<g/>
nového	nový	k2eAgInSc2d1
dualismu	dualismus	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Československo	Československo	k1gNnSc1
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
1992	#num#	k4
skutečně	skutečně	k6eAd1
zaniklo	zaniknout	k5eAaPmAgNnS
a	a	k8xC
roku	rok	k1gInSc3
1997	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zatím	zatím	k6eAd1
poslední	poslední	k2eAgFnSc3d1
úpravě	úprava	k1gFnSc3
česko-slovenské	česko-slovenský	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
některé	některý	k3yIgInPc1
moravské	moravský	k2eAgInPc1d1
pozemky	pozemek	k1gInPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
osady	osada	k1gFnSc2
U	u	k7c2
Sabotů	Sabot	k1gInPc2
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgFnP
součástí	součást	k1gFnSc7
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2000	#num#	k4
vstoupil	vstoupit	k5eAaPmAgInS
v	v	k7c4
platnost	platnost	k1gFnSc4
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vytvořil	vytvořit	k5eAaPmAgInS
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
14	#num#	k4
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
územně	územně	k6eAd1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
rovněž	rovněž	k9
nerespektují	respektovat	k5eNaImIp3nP
historické	historický	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
množství	množství	k1gNnSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
sdružení	sdružení	k1gNnSc2
a	a	k8xC
spolků	spolek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
dohromady	dohromady	k6eAd1
vytvářejí	vytvářet	k5eAaImIp3nP
tzv.	tzv.	kA
moravské	moravský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
o	o	k7c4
obnovu	obnova	k1gFnSc4
samosprávy	samospráva	k1gFnSc2
Moravy	Morava	k1gFnSc2
a	a	k8xC
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
významné	významný	k2eAgNnSc1d1
Hnutí	hnutí	k1gNnSc1
za	za	k7c4
samosprávnou	samosprávný	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
–	–	k?
Společnost	společnost	k1gFnSc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
Slezsko	Slezsko	k1gNnSc4
(	(	kIx(
<g/>
HSD-SMS	HSD-SMS	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
v	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
získalo	získat	k5eAaPmAgNnS
několik	několik	k4yIc1
mandátů	mandát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politický	politický	k2eAgInSc1d1
význam	význam	k1gInSc1
moravistických	moravistický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
však	však	k9
již	již	k6eAd1
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc1
upadl	upadnout	k5eAaPmAgInS
a	a	k8xC
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
je	být	k5eAaImIp3nS
marginální	marginální	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
aktivity	aktivita	k1gFnPc4
HSD-SMS	HSD-SMS	k1gFnPc2
se	se	k3xPyFc4
pokusila	pokusit	k5eAaPmAgFnS
navázat	navázat	k5eAaPmF
strana	strana	k1gFnSc1
Moravané	Moravan	k1gMnPc1
<g/>
,	,	kIx,
od	od	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
oddělilo	oddělit	k5eAaPmAgNnS
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
hnutí	hnutí	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Součástí	součást	k1gFnSc7
moravského	moravský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
je	být	k5eAaImIp3nS
také	také	k9
např.	např.	kA
kulturní	kulturní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Moravská	moravský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
Moravy	Morava	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Moravská	moravský	k2eAgFnSc1d1
orlice	orlice	k1gFnSc1
<g/>
,	,	kIx,
Moravské	moravský	k2eAgFnSc2d1
zemské	zemský	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
a	a	k8xC
Moravská	moravský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Znakem	znak	k1gInSc7
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
byla	být	k5eAaImAgFnS
od	od	k7c2
druhé	druhý	k4xOgFnSc2
pol.	pol.	k?
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
červenostříbrně	červenostříbrně	k6eAd1
šachovaná	šachovaný	k2eAgFnSc1d1
orlice	orlice	k1gFnSc1
se	s	k7c7
zlatou	zlatý	k2eAgFnSc7d1
zbrojí	zbroj	k1gFnSc7
a	a	k8xC
korunkou	korunka	k1gFnSc7
položená	položený	k2eAgFnSc1d1
na	na	k7c4
modrý	modrý	k2eAgInSc4d1
štít	štít	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
erbovního	erbovní	k2eAgInSc2d1
listu	list	k1gInSc2
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
z	z	k7c2
r.	r.	kA
1462	#num#	k4
<g/>
,	,	kIx,
vydaného	vydaný	k2eAgInSc2d1
na	na	k7c4
znak	znak	k1gInSc4
vděčnosti	vděčnost	k1gFnSc2
za	za	k7c4
vojenskou	vojenský	k2eAgFnSc4d1
pomoc	pomoc	k1gFnSc4
moravského	moravský	k2eAgMnSc2d1
markraběte	markrabě	k1gMnSc2
a	a	k8xC
českého	český	k2eAgMnSc2d1
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Kunštátu	Kunštát	k1gInSc2
a	a	k8xC
Poděbrad	Poděbrady	k1gInPc2
byl	být	k5eAaImAgInS
znak	znak	k1gInSc1
na	na	k7c4
žádost	žádost	k1gFnSc4
moravského	moravský	k2eAgMnSc2d1
zemského	zemský	k2eAgMnSc2d1
hejtmana	hejtman	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
z	z	k7c2
Lipé	Lipá	k1gFnSc2
polepšen	polepšen	k2eAgInSc4d1
změnou	změna	k1gFnSc7
stříbrného	stříbrný	k2eAgNnSc2d1
šachování	šachování	k1gNnSc2
na	na	k7c4
zlaté	zlatý	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
král	král	k1gMnSc1
Friedrichovo	Friedrichův	k2eAgNnSc4d1
privilegium	privilegium	k1gNnSc4
nikdy	nikdy	k6eAd1
nepotvrdil	potvrdit	k5eNaPmAgMnS
a	a	k8xC
v	v	k7c6
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
zlatá	zlatý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
neprosadila	prosadit	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Privilegium	privilegium	k1gNnSc1
nemělo	mít	k5eNaImAgNnS
ani	ani	k8xC
před	před	k7c7
ani	ani	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1628	#num#	k4
na	na	k7c4
moravský	moravský	k2eAgInSc4d1
znak	znak	k1gInSc4
žádný	žádný	k3yNgInSc4
účinek	účinek	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dokazují	dokazovat	k5eAaImIp3nP
všechny	všechen	k3xTgInPc4
vydané	vydaný	k2eAgInPc4d1
sněmovní	sněmovní	k2eAgInPc4d1
artikuly	artikul	k1gInPc4
do	do	k7c2
roku	rok	k1gInSc2
1838	#num#	k4
a	a	k8xC
nebo	nebo	k8xC
zemské	zemský	k2eAgInPc1d1
řády	řád	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1545	#num#	k4
<g/>
,	,	kIx,
1562	#num#	k4
<g/>
,	,	kIx,
1604	#num#	k4
a	a	k8xC
1628	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1628	#num#	k4
byl	být	k5eAaImAgInS
Ferdinandem	Ferdinand	k1gMnSc7
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
snahou	snaha	k1gFnSc7
o	o	k7c4
definitivní	definitivní	k2eAgNnSc4d1
ustavení	ustavení	k1gNnSc4
habsburského	habsburský	k2eAgInSc2d1
rodu	rod	k1gInSc2
jako	jako	k8xS,k8xC
dědičného	dědičný	k2eAgMnSc4d1
pána	pán	k1gMnSc4
země	zem	k1gFnSc2
erbovní	erbovní	k2eAgInSc1d1
list	list	k1gInSc1
potvrzen	potvrdit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc1
souboru	soubor	k1gInSc2
důležitějších	důležitý	k2eAgNnPc2d2
privilegií	privilegium	k1gNnPc2
<g/>
,	,	kIx,
potvrzených	potvrzená	k1gFnPc2
jako	jako	k8xC,k8xS
celek	celek	k1gInSc4
<g/>
,	,	kIx,
neporušujících	porušující	k2eNgFnPc2d1
především	především	k9
pobělohorské	pobělohorský	k2eAgNnSc1d1
Obnovené	obnovený	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
zemské	zemský	k2eAgNnSc1d1
<g/>
,	,	kIx,
bez	bez	k7c2
zkoumání	zkoumání	k1gNnSc2
jednotlivostí	jednotlivost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Františkem	František	k1gMnSc7
Píchou	Pícha	k1gMnSc7
je	být	k5eAaImIp3nS
význam	význam	k1gInSc4
císařských	císařský	k2eAgInPc2d1
dekretů	dekret	k1gInPc2
ve	v	k7c6
vztahu	vztah	k1gInSc6
ke	k	k7c3
znaku	znak	k1gInSc3
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
1752	#num#	k4
znak	znak	k1gInSc1
habsburských	habsburský	k2eAgFnPc2d1
dědičných	dědičný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
1766	#num#	k4
znak	znak	k1gInSc1
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
1804	#num#	k4
znak	znak	k1gInSc1
rakouského	rakouský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
1806	#num#	k4
znak	znak	k1gInSc1
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
otázce	otázka	k1gFnSc3
<g/>
,	,	kIx,
pro	pro	k7c4
koho	kdo	k3yInSc4,k3yQnSc4,k3yRnSc4
a	a	k8xC
za	za	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
účelem	účel	k1gInSc7
byla	být	k5eAaImAgFnS
Fridrichova	Fridrichův	k2eAgFnSc1d1
listina	listina	k1gFnSc1
vydána	vydán	k2eAgFnSc1d1
<g/>
,	,	kIx,
hodnocen	hodnocen	k2eAgInSc1d1
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
pokud	pokud	k8xS
by	by	kYmCp3nS
změna	změna	k1gFnSc1
barev	barva	k1gFnPc2
moravské	moravský	k2eAgFnSc2d1
orlice	orlice	k1gFnSc2
listinou	listina	k1gFnSc7
Fridricha	Fridrich	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
byla	být	k5eAaImAgFnS
od	od	k7c2
počátku	počátek	k1gInSc2
chápána	chápán	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
změna	změna	k1gFnSc1
státního	státní	k2eAgInSc2d1
znaku	znak	k1gInSc2
<g/>
,	,	kIx,
pak	pak	k8xC
všechny	všechen	k3xTgInPc1
tyto	tento	k3xDgInPc1
císařské	císařský	k2eAgInPc1d1
dekrety	dekret	k1gInPc1
by	by	kYmCp3nP
představovaly	představovat	k5eAaImAgInP
akty	akt	k1gInPc1
rušící	rušící	k2eAgInPc1d1
ustanovení	ustanovení	k1gNnSc4
této	tento	k3xDgFnSc2
erbovní	erbovní	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
<g/>
“	“	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
změnou	změna	k1gFnSc7
barevnosti	barevnost	k1gFnSc2
šachování	šachování	k1gNnSc2
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1915	#num#	k4
na	na	k7c4
červeno	červeno	k1gNnSc4
zlaté	zlatá	k1gFnSc2
byl	být	k5eAaImAgMnS
velký	velký	k2eAgInSc4d1
znak	znak	k1gInSc4
Rakouského	rakouský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
naposledy	naposledy	k6eAd1
upraven	upravit	k5eAaPmNgInS
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1836	#num#	k4
dekretem	dekret	k1gInSc7
Ferdinanda	Ferdinand	k1gMnSc2
V.	V.	kA
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Znakem	znak	k1gInSc7
země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
znak	znak	k1gInSc1
zeměpána	zeměpán	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
země	země	k1gFnSc1
je	být	k5eAaImIp3nS
takto	takto	k6eAd1
prezentována	prezentovat	k5eAaBmNgFnS
i	i	k9
navenek	navenek	k6eAd1
(	(	kIx(
<g/>
mezinárodně	mezinárodně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpánem	zeměpán	k1gMnSc7
Moravy	Morava	k1gFnSc2
nebyli	být	k5eNaImAgMnP
stavové	stavový	k2eAgFnPc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
markrabě	markrabě	k1gMnSc1
(	(	kIx(
<g/>
panovník	panovník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doloženě	doloženě	k6eAd1
byly	být	k5eAaImAgInP
zvláště	zvláště	k6eAd1
od	od	k7c2
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
používány	používat	k5eAaImNgFnP
obě	dva	k4xCgFnPc1
barevné	barevný	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
ve	v	k7c6
zcela	zcela	k6eAd1
jiném	jiný	k2eAgInSc6d1
právním	právní	k2eAgInSc6d1
významu	význam	k1gInSc6
před	před	k7c7
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
(	(	kIx(
<g/>
ústavních	ústavní	k2eAgFnPc2d1
a	a	k8xC
politických	politický	k2eAgFnPc2d1
změn	změna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Moravský	moravský	k2eAgInSc1d1
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1848	#num#	k4
(	(	kIx(
<g/>
„	„	k?
<g/>
selský	selský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
usnesl	usnést	k5eAaPmAgInS
na	na	k7c6
používání	používání	k1gNnSc6
zlatočerveného	zlatočervený	k2eAgNnSc2d1
šachování	šachování	k1gNnSc2
v	v	k7c6
zemském	zemský	k2eAgInSc6d1
znaku	znak	k1gInSc6
<g/>
,	,	kIx,
k	k	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
se	se	k3xPyFc4
vyjádřilo	vyjádřit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1849	#num#	k4
a	a	k8xC
následně	následně	k6eAd1
kvůli	kvůli	k7c3
žádosti	žádost	k1gFnSc3
o	o	k7c4
jednoznačné	jednoznačný	k2eAgNnSc4d1
stanovisko	stanovisko	k1gNnSc4
ve	v	k7c6
věci	věc	k1gFnSc6
zemských	zemský	k2eAgFnPc2d1
barev	barva	k1gFnPc2
i	i	k9
v	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
Ministerstvo	ministerstvo	k1gNnSc4
vnitra	vnitro	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
potvrdilo	potvrdit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nebrání	bránit	k5eNaImIp3nS
jeho	jeho	k3xOp3gNnSc1
používání	používání	k1gNnSc1
ještě	ještě	k9
před	před	k7c7
novou	nový	k2eAgFnSc7d1
úpravou	úprava	k1gFnSc7
rakouského	rakouský	k2eAgInSc2d1
státního	státní	k2eAgInSc2d1
znaku	znak	k1gInSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
definitivně	definitivně	k6eAd1
nerozhodne	rozhodnout	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
velkém	velký	k2eAgInSc6d1
a	a	k8xC
středním	střední	k2eAgInSc6d1
habsburském	habsburský	k2eAgInSc6d1
znaku	znak	k1gInSc6
však	však	k9
bylo	být	k5eAaImAgNnS
stabilně	stabilně	k6eAd1
používáno	používán	k2eAgNnSc4d1
původní	původní	k2eAgNnSc4d1
stříbrnočervené	stříbrnočervený	k2eAgNnSc4d1
šachování	šachování	k1gNnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
1915	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
středním	střední	k2eAgInSc6d1
znaku	znak	k1gInSc6
změněno	změněn	k2eAgNnSc1d1
na	na	k7c4
zlatočervené	zlatočervený	k2eAgMnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1918	#num#	k4
se	se	k3xPyFc4
přešlo	přejít	k5eAaPmAgNnS
na	na	k7c4
výlučné	výlučný	k2eAgNnSc4d1
používání	používání	k1gNnSc4
původní	původní	k2eAgFnSc2d1
stříbrnočervené	stříbrnočervený	k2eAgFnSc2d1
moravské	moravský	k2eAgFnSc2d1
orlice	orlice	k1gFnSc2
v	v	k7c6
modrém	modrý	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
tradičních	tradiční	k2eAgFnPc2d1
slovanských	slovanský	k2eAgFnPc2d1
barev	barva	k1gFnPc2
–	–	k?
červené	červená	k1gFnSc2
<g/>
,	,	kIx,
bílé	bílý	k2eAgFnSc2d1
a	a	k8xC
modré	modrý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
právního	právní	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
bylo	být	k5eAaImAgNnS
pak	pak	k6eAd1
stříbrnočervené	stříbrnočervený	k2eAgNnSc1d1
šachování	šachování	k1gNnSc1
v	v	k7c6
moravském	moravský	k2eAgInSc6d1
znaku	znak	k1gInSc6
výslovně	výslovně	k6eAd1
potvrzeno	potvrdit	k5eAaPmNgNnS
zákonem	zákon	k1gInSc7
č.	č.	k?
252	#num#	k4
<g/>
/	/	kIx~
<g/>
1920	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stříbrnočervená	Stříbrnočervený	k2eAgFnSc1d1
moravská	moravský	k2eAgFnSc1d1
orlice	orlice	k1gFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
součástí	součást	k1gFnSc7
velkého	velký	k2eAgInSc2d1
státního	státní	k2eAgInSc2d1
znaku	znak	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
znaků	znak	k1gInPc2
krajů	kraj	k1gInPc2
Jihomoravského	jihomoravský	k2eAgInSc2d1
<g/>
,	,	kIx,
Olomouckého	olomoucký	k2eAgInSc2d1
<g/>
,	,	kIx,
Zlínského	zlínský	k2eAgInSc2d1
<g/>
,	,	kIx,
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
<g/>
,	,	kIx,
Pardubického	pardubický	k2eAgMnSc2d1
a	a	k8xC
Vysočiny	vysočina	k1gFnPc4
(	(	kIx(
<g/>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
zahrnuje	zahrnovat	k5eAaImIp3nS
část	část	k1gFnSc4
historického	historický	k2eAgInSc2d1
mor	mor	k1gInSc1
<g/>
.	.	kIx.
území	území	k1gNnSc1
<g/>
,	,	kIx,
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
znaku	znak	k1gInSc6
orlici	orlice	k1gFnSc4
nemá	mít	k5eNaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
kromě	kromě	k7c2
stříbrnočervené	stříbrnočervený	k2eAgFnSc2d1
orlice	orlice	k1gFnSc2
užívá	užívat	k5eAaImIp3nS
ve	v	k7c6
čtvrceném	čtvrcený	k2eAgInSc6d1
znaku	znak	k1gInSc6
uděleném	udělený	k2eAgInSc6d1
roku	rok	k1gInSc3
2004	#num#	k4
také	také	k9
zlatočervenou	zlatočervený	k2eAgFnSc4d1
variantu	varianta	k1gFnSc4
moravské	moravský	k2eAgFnSc2d1
orlice	orlice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
ve	v	k7c6
čtvrtém	čtvrtý	k4xOgNnSc6
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3
dochované	dochovaný	k2eAgNnSc1d1
barevné	barevný	k2eAgNnSc1d1
vyobrazení	vyobrazení	k1gNnSc1
znaku	znak	k1gInSc2
moravského	moravský	k2eAgMnSc4d1
markraběte	markrabě	k1gMnSc4
(	(	kIx(
<g/>
Moravy	Morava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
freska	freska	k1gFnSc1
v	v	k7c6
sále	sál	k1gInSc6
hradu	hrad	k1gInSc2
Gozzoburg	Gozzoburg	k1gMnSc1
v	v	k7c6
Kremži	Kremže	k1gFnSc6
z	z	k7c2
počátku	počátek	k1gInSc2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
markraběte	markrabě	k1gMnSc2
a	a	k8xC
markrabství	markrabství	k1gNnSc6
(	(	kIx(
<g/>
do	do	k7c2
1915	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
Moravy	Morava	k1gFnSc2
</s>
<s>
Znak	znak	k1gInSc1
markraběte	markrabě	k1gMnSc2
a	a	k8xC
markrabství	markrabství	k1gNnSc6
(	(	kIx(
<g/>
do	do	k7c2
1915	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tj.	tj.	kA
Moravy	Morava	k1gFnSc2
</s>
<s>
Znak	znak	k1gInSc1
markraběte	markrabě	k1gMnSc2
a	a	k8xC
markrabství	markrabství	k1gNnSc6
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
ve	v	k7c6
znaku	znak	k1gInSc6
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Úplný	úplný	k2eAgInSc1d1
znak	znak	k1gInSc1
moravského	moravský	k2eAgMnSc2d1
markraběte	markrabě	k1gMnSc2
v	v	k7c6
kresbě	kresba	k1gFnSc6
heraldika	heraldik	k1gMnSc2
Huga	Hugo	k1gMnSc2
Gerharda	Gerhard	k1gMnSc2
Ströhla	Ströhla	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoretická	teoretický	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
markraběcího	markraběcí	k2eAgInSc2d1
znaku	znak	k1gInSc2
po	po	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
Moravského	moravský	k2eAgNnSc2d1
markrabství	markrabství	k1gNnSc2
v	v	k7c6
kresbě	kresba	k1gFnSc6
Huga	Hugo	k1gMnSc2
Gerharda	Gerhard	k1gMnSc2
Ströhla	Ströhla	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
republice	republika	k1gFnSc6
Československé	československý	k2eAgFnSc2d1
II	II	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Země	země	k1gFnSc1
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
vnitra	vnitro	k1gNnSc2
a	a	k8xC
St.	st.	kA
úřad	úřad	k1gInSc4
statistický	statistický	k2eAgInSc1d1
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
↑	↑	k?
VÁLKA	Válka	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Esej	esej	k1gFnSc1
o	o	k7c6
Moravě	Morava	k1gFnSc6
<g/>
:	:	kIx,
Morava	Morava	k1gFnSc1
je	být	k5eAaImIp3nS
ideálním	ideální	k2eAgInSc7d1
typem	typ	k1gInSc7
historické	historický	k2eAgFnSc2d1
země	zem	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkumné	výzkumný	k2eAgFnSc2d1
středisko	středisko	k1gNnSc4
pro	pro	k7c4
dějiny	dějiny	k1gFnPc4
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
:	:	kIx,
prameny	pramen	k1gInPc1
<g/>
,	,	kIx,
země	zem	k1gFnPc1
<g/>
,	,	kIx,
kultura	kultura	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SVITÁK	SVITÁK	kA
<g/>
,	,	kIx,
Zbyněk	Zbyněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úvod	úvod	k1gInSc1
do	do	k7c2
historické	historický	k2eAgFnSc2d1
topografie	topografie	k1gFnSc2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
:	:	kIx,
Územní	územní	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
75	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠRÁMEK	Šrámek	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
,	,	kIx,
MAJTÁN	MAJTÁN	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
,	,	kIx,
Lutterer	Lutterer	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
:	:	kIx,
Zeměpisná	zeměpisný	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
Československa	Československo	k1gNnSc2
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
202	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ČAPKA	Čapka	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
František	František	k1gMnSc1
<g/>
:	:	kIx,
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
edice	edice	k1gFnSc1
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
186	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
132	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HAVLÍK	Havlík	k1gMnSc1
<g/>
,	,	kIx,
LUBOMÍR	Lubomír	k1gMnSc1
EMIL	Emil	k1gMnSc1
<g/>
,	,	kIx,
1925	#num#	k4
<g/>
-	-	kIx~
<g/>
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kronika	kronika	k1gFnSc1
o	o	k7c6
Velké	velký	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dotisk	dotisk	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
dopl	dopl	k1gMnSc1
<g/>
.	.	kIx.
a	a	k8xC
upraveného	upravený	k2eAgMnSc2d1
vyd	vyd	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jota	jota	k1gFnSc1
339	#num#	k4
s.	s.	k?
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
39586115	#num#	k4
↑	↑	k?
KRZEMIENSKA	KRZEMIENSKA	kA
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wann	Wann	k1gInSc1
erfolgte	erfolgte	k5eAaPmIp2nP
der	drát	k5eAaImRp2nS
Anschluss	Anschluss	k1gInSc1
Mährens	Mährens	k1gInSc1
an	an	k?
den	den	k1gInSc4
böhmischen	böhmischen	k2eAgInSc4d1
Staat	Staat	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historica	Historic	k1gInSc2
XIX	XIX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1980	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
195	#num#	k4
<g/>
-	-	kIx~
<g/>
243	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
,	,	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgNnSc1d1
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
<g />
.	.	kIx.
</s>
<s hack="1">
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
196	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
347	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgNnSc1d1
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
196	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
348.1	348.1	k4
2	#num#	k4
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgNnSc1d1
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
196	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
318	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MEZNÍK	mezník	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
:	:	kIx,
Lucemburská	lucemburský	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
1310	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1423	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
363	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
53	#num#	k4
<g/>
,	,	kIx,
<g/>
↑	↑	k?
ČAPKA	Čapka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
,	,	kIx,
Morava	Morava	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
edice	edice	k1gFnSc1
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
186	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
120	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
:	:	kIx,
Kladsko	Kladsko	k1gNnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
edice	edice	k1gFnSc1
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
států	stát	k1gInPc2
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
340	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
157	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slované-stopy	Slované-stopa	k1gFnSc2
předků	předek	k1gInPc2
<g/>
,	,	kIx,
O	o	k7c6
Moravě	Morava	k1gFnSc6
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
10	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
.	.	kIx.
první	první	k4xOgFnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
218	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
216	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
GALUŠKA	GALUŠKA	kA
<g/>
,	,	kIx,
Luděk	Luděk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slované-stopy	Slované-stopa	k1gFnSc2
předků	předek	k1gInPc2
<g/>
,	,	kIx,
O	o	k7c6
Moravě	Morava	k1gFnSc6
v	v	k7c6
6	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
10	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
.	.	kIx.
první	první	k4xOgFnSc2
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Moravské	moravský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7028	#num#	k4
<g/>
-	-	kIx~
<g/>
218	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
134	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.pokladymoravy.cz/?page_id=361	http://www.pokladymoravy.cz/?page_id=361	k4
<g/>
↑	↑	k?
http://www.slovane.cz/view.php?cisloclanku=2011030003	http://www.slovane.cz/view.php?cisloclanku=2011030003	k4
<g/>
↑	↑	k?
http://www.hlasmoravy.eu/2011/02/popis-veligradu.html%5B%5D	http://www.hlasmoravy.eu/2011/02/popis-veligradu.html%5B%5D	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
boiohaemum	boiohaemum	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
arcibiskupství	arcibiskupství	k1gNnSc2
|	|	kIx~
Arcidiecéze	arcidiecéze	k1gFnSc1
olomoucká	olomoucký	k2eAgFnSc1d1
<g/>
.	.	kIx.
www.ado.cz	www.ado.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
hradu	hrad	k1gInSc2
—	—	k?
Špilberk	Špilberk	k1gInSc1
<g/>
,	,	kIx,
brněnský	brněnský	k2eAgInSc1d1
hrad	hrad	k1gInSc1
<g/>
,	,	kIx,
sídlo	sídlo	k1gNnSc4
Muzea	muzeum	k1gNnSc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
↑	↑	k?
Přemístění	přemístění	k1gNnSc2
zemského	zemský	k2eAgInSc2d1
soudu	soud	k1gInSc2
s	s	k7c7
královským	královský	k2eAgInSc7d1
tribunálem	tribunál	k1gInSc7
a	a	k8xC
zemskými	zemský	k2eAgFnPc7d1
deskami	deska	k1gFnPc7
do	do	k7c2
Brna	Brno	k1gNnSc2
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Historie	historie	k1gFnSc1
města	město	k1gNnSc2
«	«	k?
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
«	«	k?
Olomouc	Olomouc	k1gFnSc1
Tourism	Tourism	k1gInSc1
<g/>
↑	↑	k?
Města	město	k1gNnSc2
–	–	k?
Olomouc	Olomouc	k1gFnSc1
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
↑	↑	k?
Historie	historie	k1gFnSc1
–	–	k?
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
www.brno-jihomoravsky-kraj.cz	www.brno-jihomoravsky-kraj.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
MORAVY-BRNO	MORAVY-BRNO	k1gFnPc2
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc4
a	a	k8xC
okolí	okolí	k1gNnSc4
<g/>
↑	↑	k?
prof.	prof.	kA
František	františek	k1gInSc4
Mezihorák	Mezihorák	k1gInSc4
Vzkřísíme	vzkřísit	k5eAaPmIp1nP
"	"	kIx"
<g/>
Hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Olomouc	Olomouc	k1gFnSc4
<g/>
"	"	kIx"
<g/>
?	?	kIx.
</s>
<s desamb="1">
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
cyklistický	cyklistický	k2eAgMnSc1d1
komentátor	komentátor	k1gMnSc1
Robert	Robert	k1gMnSc1
Bakalář	bakalář	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
<g/>
71	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Na	na	k7c6
kole	kolo	k1gNnSc6
do	do	k7c2
nebe	nebe	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tema	tema	k?
<g/>
.	.	kIx.
<g/>
novinky	novinka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
§	§	k?
6	#num#	k4
císařského	císařský	k2eAgInSc2d1
patentu	patent	k1gInSc2
ze	z	k7c2
dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1849	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
18	#num#	k4
<g/>
/	/	kIx~
<g/>
1850	#num#	k4
ř.	ř.	k?
z.	z.	k?
<g/>
,	,	kIx,
Ústava	ústava	k1gFnSc1
zemská	zemský	k2eAgFnSc1d1
pro	pro	k7c4
markrabství	markrabství	k1gNnSc4
Moravské	moravský	k2eAgFnSc2d1
<g/>
↑	↑	k?
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2019	.2019	k4
<g/>
.	.	kIx.
www.czso.cz	www.czso.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Letopisy	letopis	k1gInPc4
království	království	k1gNnSc2
Franků	Franky	k1gInPc2
k	k	k7c3
roku	rok	k1gInSc3
822	#num#	k4
<g/>
↑	↑	k?
SČÍTÁNÍ	sčítání	k1gNnSc4
LIDU	lid	k1gInSc2
2011	#num#	k4
–	–	k?
K	k	k7c3
moravské	moravský	k2eAgFnSc3d1
národnosti	národnost	k1gFnSc3
se	se	k3xPyFc4
přihlásilo	přihlásit	k5eAaPmAgNnS
630	#num#	k4
897	#num#	k4
lidí	člověk	k1gMnPc2
–	–	k?
Moravská	moravský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
obec	obec	k1gFnSc1
–	–	k?
Za	za	k7c4
Moravu	Morava	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Národnostní	národnostní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ČSÚ	ČSÚ	kA
<g/>
,	,	kIx,
30.6	30.6	k4
<g/>
.2014	.2014	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
500	#num#	k4
let	léto	k1gNnPc2
od	od	k7c2
příchodu	příchod	k1gInSc2
na	na	k7c4
jižní	jižní	k2eAgFnSc4d1
Moravu	Morava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charváti	Charvát	k1gMnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vinaři	vinař	k1gMnPc1
milující	milující	k2eAgFnSc4d1
fazolovou	fazolový	k2eAgFnSc4d1
polévku	polévka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brněnský	brněnský	k2eAgInSc1d1
deník	deník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Cizinec	cizinec	k1gMnSc1
vůdcem	vůdce	k1gMnSc7
Slovanů	Slovan	k1gInPc2
<g/>
:	:	kIx,
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
byl	být	k5eAaImAgMnS
a	a	k8xC
odkud	odkud	k6eAd1
vlastně	vlastně	k9
přišel	přijít	k5eAaPmAgMnS
kupec	kupec	k1gMnSc1
Sámo	Sámo	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
perský	perský	k2eAgMnSc1d1
kupec	kupec	k1gMnSc1
Abú	abú	k1gMnSc1
Omar	Omar	k1gMnSc1
ibn	ibn	k?
Rusta	Rusta	k1gMnSc1
to	ten	k3xDgNnSc4
zaznamenal	zaznamenat	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Kniha	kniha	k1gFnSc1
vzácných	vzácný	k2eAgInPc2d1
drahokamů	drahokam	k1gInPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
článek	článek	k1gInSc1
J.	J.	kA
Galatíka	Galatík	k1gMnSc2
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
ibn	ibn	k?
Rusta	Rust	k1gInSc2
o	o	k7c6
králi	král	k1gMnSc6
Svatoplukovi	Svatopluk	k1gMnSc6
a	a	k8xC
jeho	jeho	k3xOp3gNnSc6
městě	město	k1gNnSc6
na	na	k7c6
stránkách	stránka	k1gFnPc6
Slované	Slovan	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Termín	termín	k1gInSc1
je	být	k5eAaImIp3nS
novodobého	novodobý	k2eAgInSc2d1
původu	původ	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
vymyslel	vymyslet	k5eAaPmAgMnS
ho	on	k3xPp3gMnSc4
a	a	k8xC
uvedl	uvést	k5eAaPmAgMnS
ve	v	k7c4
známost	známost	k1gFnSc4
až	až	k9
František	František	k1gMnSc1
Palacký	Palacký	k1gMnSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
dobový	dobový	k2eAgInSc1d1
latinský	latinský	k2eAgInSc1d1
název	název	k1gInSc1
těchto	tento	k3xDgInPc2
celků	celek	k1gInPc2
byl	být	k5eAaImAgInS
partes	partes	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
díly	díl	k1gInPc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Vedou	vést	k5eAaImIp3nP
se	se	k3xPyFc4
spory	spor	k1gInPc1
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
starou	starý	k2eAgFnSc4d1
<g/>
,	,	kIx,
původní	původní	k2eAgFnSc4d1
moravskou	moravský	k2eAgFnSc4d1
nobilitu	nobilita	k1gFnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
přeživší	přeživší	k2eAgFnSc1d1
ještě	ještě	k9
z	z	k7c2
dob	doba	k1gFnPc2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
zastával	zastávat	k5eAaImAgMnS
zejména	zejména	k9
L.	L.	kA
E.	E.	kA
Havlík	Havlík	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
zda	zda	k8xS
to	ten	k3xDgNnSc4
byli	být	k5eAaImAgMnP
beneficiáři	beneficiář	k1gMnPc1
a	a	k8xC
hradští	hradský	k2eAgMnPc1d1
úředníci	úředník	k1gMnPc1
přišlí	přišlý	k2eAgMnPc1d1
na	na	k7c4
Moravu	Morava	k1gFnSc4
z	z	k7c2
Čech	Čechy	k1gFnPc2
spolu	spolu	k6eAd1
s	s	k7c7
Břetislavem	Břetislav	k1gMnSc7
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŽEMLIČKA	Žemlička	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgFnSc6d1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
196	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
319	#num#	k4
a	a	k8xC
320	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŽEMLIČKA	Žemlička	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
Čechy	Čechy	k1gFnPc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgNnSc1d1
(	(	kIx(
<g/>
1034	#num#	k4
<g/>
–	–	k?
<g/>
1198	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
196	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
323	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sága	sága	k1gFnSc1
moravských	moravský	k2eAgMnPc2d1
Přemyslovců	Přemyslovec	k1gMnPc2
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2006	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
25	#num#	k4
<g/>
;	;	kIx,
Jaroslav	Jaroslav	k1gMnSc1
Mezník	mezník	k1gInSc1
<g/>
:	:	kIx,
Lucemburská	lucemburský	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1999	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
52	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
brána	brána	k1gFnSc1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tatarský	tatarský	k2eAgInSc4d1
mýtus	mýtus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Blansko	Blansko	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VÁLKA	Válka	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Dějiny	dějiny	k1gFnPc1
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
reformace	reformace	k1gFnSc2
<g/>
,	,	kIx,
renesance	renesance	k1gFnSc2
a	a	k8xC
baroka	baroko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
:	:	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
128	#num#	k4
<g/>
–	–	k?
<g/>
129	#num#	k4
<g/>
↑	↑	k?
Lánové	lánový	k2eAgInPc1d1
rejstříky	rejstřík	k1gInPc1
(	(	kIx(
<g/>
1656	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.ilcik.cz	www.ilcik.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Štefan	Štefan	k1gMnSc1
Pilárik	Pilárik	k1gMnSc1
<g/>
:	:	kIx,
Turcico-Tartarica	Turcico-Tartarica	k1gMnSc1
crudelitas	crudelitas	k1gMnSc1
(	(	kIx(
<g/>
Turecko-tatárska	Turecko-tatárska	k1gFnSc1
ukrutnosť	ukrutnostit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Kuruci	kuruc	k1gMnPc7
povraždili	povraždit	k5eAaPmAgMnP
osmdesát	osmdesát	k4xCc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdoničtí	Tvrdonický	k2eAgMnPc1d1
odhalili	odhalit	k5eAaPmAgMnP
pomník	pomník	k1gInSc4
tragédii	tragédie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Turci	Turek	k1gMnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
č.	č.	k?
315	#num#	k4
<g/>
/	/	kIx~
<g/>
1924	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
↑	↑	k?
Statistický	statistický	k2eAgInSc1d1
lexikon	lexikon	k1gInSc1
obcí	obec	k1gFnPc2
v	v	k7c6
republice	republika	k1gFnSc6
Československé	československý	k2eAgFnSc2d1
-	-	kIx~
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Země	země	k1gFnSc1
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Změny	změna	k1gFnSc2
v	v	k7c6
rozloze	rozloha	k1gFnSc6
obcí	obec	k1gFnPc2
a	a	k8xC
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
149	#num#	k4
a	a	k8xC
151	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
zákon	zákon	k1gInSc1
ze	z	k7c2
dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1927	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
125	#num#	k4
<g/>
/	/	kIx~
<g/>
1927	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
o	o	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
organisaci	organisace	k1gFnSc4
politické	politický	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
“	“	k?
<g/>
↑	↑	k?
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
ze	z	k7c2
dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1928	#num#	k4
č.	č.	k?
174	#num#	k4
<g/>
/	/	kIx~
<g/>
1928	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
jímž	jenž	k3xRgInSc7
se	se	k3xPyFc4
v	v	k7c6
zemi	zem	k1gFnSc6
České	český	k2eAgFnSc6d1
a	a	k8xC
Moravskoslezské	moravskoslezský	k2eAgInPc1d1
určují	určovat	k5eAaImIp3nP
obvody	obvod	k1gInPc1
a	a	k8xC
sídla	sídlo	k1gNnSc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
a	a	k8xC
některá	některý	k3yIgNnPc4
města	město	k1gNnPc4
se	s	k7c7
zvláštním	zvláštní	k2eAgNnSc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
statutem	statut	k1gInSc7
podrobují	podrobovat	k5eAaImIp3nP
všeobecně	všeobecně	k6eAd1
platným	platný	k2eAgNnSc7d1
obecním	obecní	k2eAgNnSc7d1
zřízením	zřízení	k1gNnSc7
<g/>
“	“	k?
<g/>
↑	↑	k?
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
č.	č.	k?
104	#num#	k4
<g/>
/	/	kIx~
<g/>
1935	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
kterým	který	k3yQgFnPc3,k3yRgFnPc3,k3yIgFnPc3
se	se	k3xPyFc4
v	v	k7c6
zemích	zem	k1gFnPc6
České	český	k2eAgFnSc2d1
a	a	k8xC
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
provádějí	provádět	k5eAaImIp3nP
změny	změna	k1gFnPc1
obvodů	obvod	k1gInPc2
některých	některý	k3yIgInPc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
“	“	k?
<g/>
↑	↑	k?
svým	svůj	k3xOyFgNnSc7
nařízením	nařízení	k1gNnSc7
č.	č.	k?
388	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
1940	#num#	k4
„	„	k?
<g/>
O	o	k7c6
některých	některý	k3yIgFnPc6
změnách	změna	k1gFnPc6
obvodů	obvod	k1gInPc2
zemských	zemský	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
“	“	k?
<g/>
↑	↑	k?
vládní	vládní	k2eAgNnSc4d1
nařízení	nařízení	k1gNnSc4
ze	z	k7c2
dne	den	k1gInSc2
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1940	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
389	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
upravují	upravovat	k5eAaImIp3nP
obvody	obvod	k1gInPc1
a	a	k8xC
sídla	sídlo	k1gNnPc1
některých	některý	k3yIgInPc2
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
“	“	k?
<g/>
↑	↑	k?
Dějiny	dějiny	k1gFnPc4
Moravy	Morava	k1gFnSc2
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7275	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Život	život	k1gInSc1
v	v	k7c6
protektorátu	protektorát	k1gInSc6
<g/>
,	,	kIx,
s.	s.	k?
185	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
galerie	galerie	k1gFnSc2
dobových	dobový	k2eAgInPc2d1
dokumentů	dokument	k1gInPc2
<g/>
↑	↑	k?
Slovensko	Slovensko	k1gNnSc1
toužilo	toužit	k5eAaImAgNnS
zabrat	zabrat	k5eAaPmF
Zlín	Zlín	k1gInSc4
i	i	k8xC
Vsetín	Vsetín	k1gInSc4
<g/>
↑	↑	k?
Slováci	Slovák	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
získat	získat	k5eAaPmF
Slovácko	Slovácko	k1gNnSc1
<g/>
,	,	kIx,
myšlenka	myšlenka	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
z	z	k7c2
Moravy	Morava	k1gFnSc2
<g/>
↑	↑	k?
Slováci	Slovák	k1gMnPc1
chtěli	chtít	k5eAaImAgMnP
za	za	k7c2
války	válka	k1gFnSc2
připojit	připojit	k5eAaPmF
kus	kus	k1gInSc4
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
Hitler	Hitler	k1gMnSc1
to	ten	k3xDgNnSc4
zatrhl	zatrhnout	k5eAaPmAgMnS
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
http://is.muni.cz/th/145986/ff_b/Vytisk-Opava1945-48.pdf	http://is.muni.cz/th/145986/ff_b/Vytisk-Opava1945-48.pdf	k1gInSc1
<g/>
↑	↑	k?
http://psp.cz/eknih/1986fs/slsn/usneseni/u0212.htm	http://psp.cz/eknih/1986fs/slsn/usneseni/u0212.htm	k1gInSc1
<g/>
↑	↑	k?
Sněmovní	sněmovní	k2eAgInSc1d1
tisk	tisk	k1gInSc1
619	#num#	k4
-	-	kIx~
Návrh	návrh	k1gInSc1
skupiny	skupina	k1gFnSc2
poslanců	poslanec	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
na	na	k7c6
vydání	vydání	k1gNnSc6
zákona	zákon	k1gInSc2
ČNR	ČNR	kA
ze	z	k7c2
dne	den	k1gInSc2
<g/>
........	........	k?
1992	#num#	k4
o	o	k7c6
zemské	zemský	k2eAgFnSc6d1
samosprávě	samospráva	k1gFnSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
ČR	ČR	kA
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sněmovní	sněmovní	k2eAgInSc4d1
tisk	tisk	k1gInSc4
619	#num#	k4
-	-	kIx~
Důvodová	důvodový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
ČR	ČR	kA
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sněmovní	sněmovní	k2eAgInSc4d1
tisk	tisk	k1gInSc4
642	#num#	k4
-	-	kIx~
Návrh	návrh	k1gInSc1
skupiny	skupina	k1gFnSc2
poslanců	poslanec	k1gMnPc2
České	český	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
na	na	k7c6
vydání	vydání	k1gNnSc6
zákona	zákon	k1gInSc2
ČNR	ČNR	kA
ze	z	k7c2
dne	den	k1gInSc2
<g/>
........	........	k?
1992	#num#	k4
o	o	k7c6
zemské	zemský	k2eAgFnSc6d1
samosprávě	samospráva	k1gFnSc6
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
ČR	ČR	kA
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Sněmovní	sněmovní	k2eAgInSc4d1
tisk	tisk	k1gInSc4
642	#num#	k4
-	-	kIx~
Důvodová	důvodový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
ČR	ČR	kA
<g/>
]	]	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Petr	Petr	k1gMnSc1
Pithart	Pitharta	k1gFnPc2
tvrdě	tvrdě	k6eAd1
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
tu	tu	k6eAd1
mnoho	mnoho	k4c4
tupých	tupý	k2eAgFnPc2d1
ovcí	ovce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volíme	volit	k5eAaImIp1nP
pořád	pořád	k6eAd1
stejně	stejně	k6eAd1
a	a	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
divíme	divit	k5eAaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parlamentnilisty	Parlamentnilista	k1gMnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-07-23	2012-07-23	k4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012-07-23	2012-07-23	k4
6	#num#	k4
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
Moravě	Morava	k1gFnSc6
nemá	mít	k5eNaImIp3nS
rozhodovat	rozhodovat	k5eAaImF
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgNnPc2d1
hnutí	hnutí	k1gNnPc2
chce	chtít	k5eAaImIp3nS
zrušit	zrušit	k5eAaPmF
kraje	kraj	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-04-27	2018-04-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symboly	symbol	k1gInPc1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vláda	vláda	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
42	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87041	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
VOJTÍŠEK	Vojtíšek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naše	náš	k3xOp1gInPc4
státní	státní	k2eAgInSc4d1
znaky	znak	k1gInPc4
(	(	kIx(
<g/>
staré	starý	k2eAgInPc4d1
a	a	k8xC
nynější	nynější	k2eAgInPc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
s.	s.	k?
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PÍCHA	Pícha	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
550	#num#	k4
letům	let	k1gInPc3
erbovní	erbovní	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
císaře	císař	k1gMnSc2
Fridricha	Fridrich	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
pro	pro	k7c4
moravské	moravský	k2eAgInPc4d1
stavy	stav	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Genealogické	genealogický	k2eAgFnPc4d1
a	a	k8xC
heraldické	heraldický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
2012	#num#	k4
:	:	kIx,
Moravská	moravský	k2eAgFnSc1d1
genealogická	genealogický	k2eAgFnSc1d1
a	a	k8xC
heraldická	heraldický	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
o.	o.	k?
s.	s.	k?
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
17	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
60	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
862	#num#	k4
<g/>
-	-	kIx~
<g/>
8963	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
č.	č.	k?
78	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wappen	Wappen	k2eAgInSc1d1
und	und	k?
Titeln	Titeln	k1gInSc1
Seiner	Seinra	k1gFnPc2
kaiserlichen	kaiserlichen	k2eAgMnSc1d1
königlichen	königlichen	k2eAgMnSc1d1
Apostolischen	Apostolischen	k2eAgMnSc1d1
Majestät	Majestät	k2eAgMnSc1d1
Ferdinand	Ferdinand	k1gMnSc1
des	des	k1gNnSc2
Ersten	Erstno	k1gNnPc2
<g/>
,	,	kIx,
Kaisers	Kaisers	k1gInSc1
von	von	k1gInSc1
Oesterreich	Oesterreich	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Sbírka	sbírka	k1gFnSc1
zákonů	zákon	k1gInPc2
a	a	k8xC
nařízení	nařízení	k1gNnSc2
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
a	a	k8xC
Slezsko	Slezsko	k1gNnSc4
<g/>
:	:	kIx,
Sammlung	Sammlung	k1gInSc1
der	drát	k5eAaImRp2nS
politischen	politischen	k1gInSc1
Gesetze	Gesetze	k1gFnSc1
und	und	k?
Verordnungen	Verordnungen	k1gInSc1
für	für	k?
Mähren	Mährna	k1gFnPc2
und	und	k?
Schlesien	Schlesina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herausgegeben	Herausgegeben	k2eAgInSc1d1
auf	auf	k?
allerhöchsten	allerhöchsten	k2eAgMnSc1d1
Befehl	Befehl	k1gMnSc1
<g/>
,	,	kIx,
unter	unter	k1gMnSc1
Aufsicht	Aufsicht	k1gMnSc1
des	des	k1gNnPc2
k.	k.	k?
k.	k.	k?
mähr	mähr	k1gMnSc1
<g/>
.	.	kIx.
schles	schles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guberniums	Guberniumsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Achtzehnter	Achtzehntra	k1gFnPc2
Band	banda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Enthält	Enthält	k2eAgInSc1d1
die	die	k?
Verordnungen	Verordnungen	k1gInSc1
vom	vom	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jänner	Jänner	k1gInSc1
bis	bis	k?
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
December	December	k1gInSc1
1836	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1837	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
517	#num#	k4
<g/>
-	-	kIx~
<g/>
540	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SOMMARUGA	SOMMARUGA	kA
<g/>
,	,	kIx,
FREIHERR	FREIHERR	kA
VON	von	k1gInSc1
<g/>
,	,	kIx,
Oscar	Oscar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jahrbuch	Jahrbuch	k1gInSc1
der	drát	k5eAaImRp2nS
k.	k.	k?
k.	k.	k?
heraldischen	heraldischen	k2eAgMnSc1d1
Gesellschaft	Gesellschaft	k1gMnSc1
Adler	Adler	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1875	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Das	Das	k1gFnSc1
Wappen	Wappen	k2eAgInSc4d1
der	drát	k5eAaImRp2nS
Markgrafschaft	Markgrafschafta	k1gFnPc2
Mähren	Mährna	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
145	#num#	k4
<g/>
-	-	kIx~
<g/>
146	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jednání	jednání	k1gNnSc2
úřadů	úřad	k1gInPc2
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1880	#num#	k4
<g/>
,	,	kIx,
Vídeň	Vídeň	k1gFnSc1
-	-	kIx~
odpověď	odpověď	k1gFnSc1
moravskému	moravský	k2eAgMnSc3d1
místodržiteli	místodržitel	k1gMnSc3
ohledně	ohledně	k7c2
barevnosti	barevnost	k1gFnSc2
moravské	moravský	k2eAgFnSc2d1
orlice	orlice	k1gFnSc2
a	a	k8xC
moravských	moravský	k2eAgFnPc2d1
zemských	zemský	k2eAgFnPc2d1
barev	barva	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Část	část	k1gFnSc1
CLV	CLV	kA
Reichsgesetzblatt	Reichsgesetzblattum	k1gNnPc2
für	für	k?
die	die	k?
im	im	k?
Reichsrate	Reichsrat	k1gInSc5
vertretenen	vertretenen	k2eAgMnSc1d1
Königreiche	Königreiche	k1gFnSc7
und	und	k?
Länder	Ländra	k1gFnPc2
<g/>
,	,	kIx,
pod	pod	k7c7
č.	č.	k?
327	#num#	k4
a	a	k8xC
328	#num#	k4
<g/>
.	.	kIx.
327	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kundmachung	Kundmachung	k1gMnSc1
<g/>
,	,	kIx,
betreffend	betreffend	k1gMnSc1
die	die	k?
Festsetzung	Festsetzung	k1gMnSc1
und	und	k?
Beschreibung	Beschreibung	k1gInSc1
des	des	k1gNnSc1
Wappens	Wappens	k1gInSc1
der	drát	k5eAaImRp2nS
österreichischen	österreichischen	k1gInSc4
Länder	Länder	k1gInSc4
<g/>
.	.	kIx.
-	-	kIx~
328	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kundmachung	Kundmachung	k1gMnSc1
<g/>
,	,	kIx,
betreffend	betreffend	k1gMnSc1
das	das	k?
für	für	k?
den	den	k1gInSc1
Gebrauch	Gebrauch	k1gInSc1
bei	bei	k?
den	den	k1gInSc4
gemeinsamen	gemeinsamen	k2eAgInSc4d1
Einrichtungen	Einrichtungen	k1gInSc4
der	drát	k5eAaImRp2nS
österreichisch-ungarischen	österreichisch-ungarischna	k1gFnPc2
Monarchie	monarchie	k1gFnPc1
bestimmte	bestimmit	k5eAaPmRp2nP
Wappen	Wappen	k2eAgInSc4d1
<g/>
..	..	k?
Wien	Wien	k1gInSc4
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Festsetzung	Festsetzung	k1gInSc1
des	des	k1gNnSc1
Wappens	Wappens	k1gInSc1
der	drát	k5eAaImRp2nS
österreichischen	österreichischen	k1gInSc1
Länder	Länder	k1gInSc4
sowie	sowie	k1gFnSc2
des	des	k1gNnSc7
Wappens	Wappensa	k1gFnPc2
der	drát	k5eAaImRp2nS
österreichisch-ungarischen	österreichisch-ungarischna	k1gFnPc2
Monarchie	monarchie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úřední	úřední	k2eAgFnSc1d1
část	část	k1gFnSc1
Wiener	Wiener	k1gMnSc1
Zeitung	Zeitung	k1gMnSc1
z	z	k7c2
12	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1915	#num#	k4
č.	č.	k?
236	#num#	k4
a	a	k8xC
z	z	k7c2
6	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
1915	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
257	#num#	k4
<g/>
.	.	kIx.
anno	anno	k6eAd1
<g/>
.	.	kIx.
<g/>
onb	onb	k?
<g/>
.	.	kIx.
<g/>
ac	ac	k?
<g/>
.	.	kIx.
<g/>
at	at	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wien	Wien	k1gNnSc1
<g/>
:	:	kIx,
12	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1915	#num#	k4
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SEDLÁČEK	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
,	,	kIx,
Gregorovičová	Gregorovičová	k1gFnSc1
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
panovnická	panovnický	k2eAgFnSc1d1
a	a	k8xC
státní	státní	k2eAgFnSc1d1
symbolika	symbolika	k1gFnSc1
<g/>
:	:	kIx,
vývoj	vývoj	k1gInSc1
od	od	k7c2
středověku	středověk	k1gInSc2
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
:	:	kIx,
výstava	výstava	k1gFnSc1
Státního	státní	k2eAgInSc2d1
ústředního	ústřední	k2eAgInSc2d1
archivu	archiv	k1gInSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
-	-	kIx~
Archivní	archivní	k2eAgInSc1d1
areál	areál	k1gInSc1
Chodovec	Chodovec	k1gInSc1
28	#num#	k4
<g/>
.	.	kIx.
září-	září-	k?
<g/>
31	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
(	(	kIx(
<g/>
Czech	Czech	k1gInSc1
Sovereigns	Sovereigns	k1gInSc1
<g/>
´	´	k?
and	and	k?
State	status	k1gInSc5
<g/>
´	´	k?
<g/>
s	s	k7c7
Symbols	Symbolsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Their	Theira	k1gFnPc2
Evolution	Evolution	k1gInSc1
from	from	k1gMnSc1
the	the	k?
Middle	Middle	k1gFnSc2
Ages	Agesa	k1gFnPc2
up	up	k?
to	ten	k3xDgNnSc1
the	the	k?
Present	Present	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgInSc1d1
ústřední	ústřední	k2eAgInSc1d1
archiv	archiv	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
19	#num#	k4
<g/>
-	-	kIx~
<g/>
126	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČAPKA	Čapka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
LIBRI	LIBRI	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
159	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Stručná	stručný	k2eAgFnSc1d1
historie	historie	k1gFnSc1
států	stát	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
186	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ČEREŠŇÁK	ČEREŠŇÁK	kA
<g/>
,	,	kIx,
Bedřich	Bedřich	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
dějin	dějiny	k1gFnPc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I.	I.	kA
Od	od	k7c2
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
do	do	k7c2
husitské	husitský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
159	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
186	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dialektický	dialektický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
moravský	moravský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
pro	pro	k7c4
vědy	věda	k1gFnPc4
<g/>
,	,	kIx,
slovesnost	slovesnost	k1gFnSc1
a	a	k8xC
umění	umění	k1gNnSc1
<g/>
,	,	kIx,
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemská	zemský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
a	a	k8xC
státní	státní	k2eAgFnPc1d1
finance	finance	k1gFnPc1
Moravy	Morava	k1gFnSc2
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
Universita	universita	k1gFnSc1
<g/>
,	,	kIx,
filosofická	filosofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
220	#num#	k4
s.	s.	k?
</s>
<s>
DAVID	David	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravian	Moravian	k1gMnSc1
estatism	estatism	k1gMnSc1
and	and	k?
provincial	provinciat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
councils	councils	k1gInSc4
in	in	k?
the	the	k?
second	second	k1gInSc1
half	halfa	k1gFnPc2
of	of	k?
the	the	k?
17	#num#	k4
<g/>
th	th	k?
century	centura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Foglia	Foglia	k1gFnSc1
historia	historium	k1gNnSc2
Bohemica	Bohemic	k1gInSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
54	#num#	k4
s.	s.	k?
</s>
<s>
STOPĚRUTNÍK	STOPĚRUTNÍK	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravská	moravský	k2eAgFnSc1d1
zem	zem	k1gFnSc1
<g/>
,	,	kIx,
moravská	moravský	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Nový	nový	k2eAgInSc1d1
lid	lid	k1gInSc1
<g/>
,	,	kIx,
1923	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PLESKALOVÁ	PLESKALOVÁ	kA
<g/>
,	,	kIx,
J.	J.	kA
Tvoření	tvoření	k1gNnSc1
pomístních	pomístní	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinočany	Jinočan	k1gMnPc7
<g/>
:	:	kIx,
H	H	kA
&	&	k?
H	H	kA
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FLIP	FLIP	kA
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Secesní	secesní	k2eAgInPc1d1
chrámy	chrám	k1gInPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Barister	Barister	k1gMnSc1
<g/>
&	&	k?
<g/>
Principal	Principal	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
266	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86598	#num#	k4
<g/>
-	-	kIx~
<g/>
63	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FOLTÝN	Foltýn	k1gMnSc1
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
klášterů	klášter	k1gInPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
878	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KADLEC	Kadlec	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
ústavních	ústavní	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Všehrd	Všehrd	k1gMnSc1
<g/>
,	,	kIx,
1926	#num#	k4
<g/>
.	.	kIx.
99	#num#	k4
s.	s.	k?
</s>
<s>
KALUS	kalus	k1gInSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzea	muzeum	k1gNnPc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
Profil	profil	k1gInSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
240	#num#	k4
s.	s.	k?
</s>
<s>
KLENOVSKÝ	KLENOVSKÝ	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovské	židovský	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Era	Era	k1gFnSc1
-	-	kIx~
vydavatelství	vydavatelství	k1gNnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
220	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86517	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
KOHOUT	kohout	k1gInSc1
A	a	k9
KOL	kol	k6eAd1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
a	a	k8xC
Slezsko	Slezsko	k1gNnSc1
-	-	kIx~
Architektura	architektura	k1gFnSc1
XX	XX	kA
<g/>
.	.	kIx.
století	století	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Zlatý	zlatý	k2eAgInSc1d1
řez	řez	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
332	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902810	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOLEJKA	kolejka	k1gFnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předindustriální	Předindustriální	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
Moravy	Morava	k1gFnSc2
jako	jako	k8xC,k8xS
přírodní	přírodní	k2eAgNnSc4d1
a	a	k8xC
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Košice	Košice	k1gInPc4
<g/>
:	:	kIx,
UPJŠpress	UPJŠpressa	k1gFnPc2
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
8152	#num#	k4
<g/>
-	-	kIx~
<g/>
646	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KONEČNÝ	Konečný	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vitruvius	Vitruvius	k1gMnSc1
Moravicus	Moravicus	k1gMnSc1
-	-	kIx~
Neoklasicistní	neoklasicistní	k2eAgFnSc1d1
aristokratická	aristokratický	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
454	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87231	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOUDELKA	Koudelka	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znaky	znak	k1gInPc1
a	a	k8xC
vlajky	vlajka	k1gFnPc1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právo	právo	k1gNnSc1
a	a	k8xC
bezpečnost	bezpečnost	k1gFnSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
25	#num#	k4
<g/>
-	-	kIx~
<g/>
50	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
2336	#num#	k4
<g/>
-	-	kIx~
<g/>
5323	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOUDELKA	Koudelka	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravský	moravský	k2eAgInSc1d1
znak	znak	k1gInSc1
a	a	k8xC
vlajka	vlajka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdenekkoudelka	Zdenekkoudelka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2013-08-11	2013-08-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
KRSEK	krsek	k1gMnSc1
<g/>
,	,	kIx,
Ivo	Ivo	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
baroka	baroko	k1gNnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
736	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
540	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MALÍŘ	malíř	k1gMnSc1
A	a	k9
KOL	kol	k6eAd1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografický	biografický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
poslanců	poslanec	k1gMnPc2
moravského	moravský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
v	v	k7c6
letech	let	k1gInPc6
1861	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
887	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7325	#num#	k4
<g/>
-	-	kIx~
<g/>
272	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MALÍŘ	malíř	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
ŘEPA	Řepa	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Morava	Morava	k1gFnSc1
na	na	k7c6
cestě	cesta	k1gFnSc6
k	k	k7c3
občanské	občanský	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
:	:	kIx,
Historický	historický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
335	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7275	#num#	k4
<g/>
-	-	kIx~
<g/>
105	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MEZNÍK	mezník	k1gInSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucemburská	lucemburský	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
1310	#num#	k4
<g/>
-	-	kIx~
<g/>
1423	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
562	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
363	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravští	moravský	k2eAgMnPc1d1
Židé	Žid	k1gMnPc1
v	v	k7c6
době	doba	k1gFnSc6
emancipace	emancipace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
360	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
307	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MLČÁK	MLČÁK	kA
<g/>
,	,	kIx,
Leoš	Leoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvony	zvon	k1gInPc4
na	na	k7c6
Moravě	Morava	k1gFnSc6
a	a	k8xC
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Kaligram	Kaligram	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
295	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
904847	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
;	;	kIx,
PLAČEK	Plaček	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaniklé	zaniklý	k2eAgInPc1d1
hrady	hrad	k1gInPc1
<g/>
,	,	kIx,
zámky	zámek	k1gInPc1
a	a	k8xC
tvrze	tvrz	k1gFnPc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
Slezska	Slezsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
243	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MUSIL	Musil	k1gMnSc1
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
ledové	ledový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
MUNI	MUNI	k?
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
232	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
6364	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
PLAČEK	Plaček	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrovaná	ilustrovaný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
moravských	moravský	k2eAgInPc2d1
hradů	hrad	k1gInPc2
<g/>
,	,	kIx,
zámků	zámek	k1gInPc2
a	a	k8xC
tvrzí	tvrz	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
757	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7277	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
46	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
SEJBAL	SEJBAL	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgNnSc1d1
mincovnictví	mincovnictví	k1gNnSc1
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Aleš	Aleš	k1gMnSc1
Čeněk	Čeněk	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
330	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7380	#num#	k4
<g/>
-	-	kIx~
<g/>
208	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
STILLER	STILLER	kA
<g/>
,	,	kIx,
Adolph	Adolph	k1gMnSc1
<g/>
;	;	kIx,
SAPÁK	SAPÁK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
TEMPL	templ	k1gInSc1
<g/>
,	,	kIx,
Stephan	Stephan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
-	-	kIx~
Stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
Cesty	cesta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salzburg	Salzburg	k1gInSc1
<g/>
/	/	kIx~
<g/>
Wien	Wien	k1gInSc1
<g/>
:	:	kIx,
Müry	Müra	k1gFnSc2
Salzmann	Salzmann	k1gMnSc1
Verlag	Verlag	k1gMnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
188	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
99014	#num#	k4
<g/>
-	-	kIx~
<g/>
102	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠTĚPÁN	Štěpán	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravský	moravský	k2eAgMnSc1d1
markrabě	markrabě	k1gMnSc1
Jošt	Jošt	k1gMnSc1
(	(	kIx(
<g/>
1354	#num#	k4
<g/>
-	-	kIx~
<g/>
1411	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Matice	matice	k1gFnSc1
moravská	moravský	k2eAgFnSc1d1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
828	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86488	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
reprint	reprint	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
TEJRAL	TEJRAL	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
na	na	k7c6
sklonku	sklonek	k1gInSc6
antiky	antika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
.	.	kIx.
256	#num#	k4
s.	s.	k?
</s>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA
<g/>
,	,	kIx,
Dušan	Dušan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
Velké	velký	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
384	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VÁLKA	Válka	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díl	díl	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
Morava	Morava	k1gFnSc1
reformace	reformace	k1gFnSc2
<g/>
,	,	kIx,
renesance	renesance	k1gFnSc2
a	a	k8xC
baroka	baroko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Muzejní	muzejní	k2eAgFnSc1d1
a	a	k8xC
vlastivědná	vlastivědný	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
275	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85048	#num#	k4
<g/>
-	-	kIx~
<g/>
62	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
WIHODA	WIHODA	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morava	Morava	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
knížecí	knížecí	k2eAgFnSc1d1
906	#num#	k4
<g/>
–	–	k?
<g/>
1197	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
464	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
563	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZATLOUKAL	Zatloukal	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
;	;	kIx,
KOLLMANN	KOLLMANN	kA
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravské	moravský	k2eAgFnPc4d1
lázně	lázeň	k1gFnPc4
v	v	k7c6
proměnách	proměna	k1gFnPc6
dvou	dva	k4xCgInPc2
století	století	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Krajské	krajský	k2eAgNnSc1d1
vlastivědné	vlastivědný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
1987	#num#	k4
<g/>
.	.	kIx.
139	#num#	k4
s.	s.	k?
</s>
<s>
ZEMEK	zemek	k1gMnSc1
<g/>
,	,	kIx,
Metoděj	Metoděj	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravsko-uherská	moravsko-uherský	k2eAgFnSc1d1
hranice	hranice	k1gFnSc1
v	v	k7c6
10	#num#	k4
<g/>
.	.	kIx.
až	až	k9
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Musejní	musejní	k2eAgInSc4d1
spolek	spolek	k1gInSc4
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
127	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1
církevní	církevní	k2eAgFnSc1d1
provincie	provincie	k1gFnSc1
</s>
<s>
Moravská	moravský	k2eAgNnPc1d1
platidla	platidlo	k1gNnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
vládců	vládce	k1gMnPc2
Moravy	Morava	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Morava	Morava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Morava	Morava	k1gFnSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Morava	Morava	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Morava	Morava	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Mapa	mapa	k1gFnSc1
Moravy	Morava	k1gFnSc2
a	a	k8xC
Českého	český	k2eAgNnSc2d1
Slezska	Slezsko	k1gNnSc2
ze	z	k7c2
druhého	druhý	k4xOgNnSc2
vojenského	vojenský	k2eAgNnSc2d1
mapování	mapování	k1gNnSc2
</s>
<s>
Detailní	detailní	k2eAgInSc4d1
přehled	přehled	k1gInSc4
průběhu	průběh	k1gInSc2
moravsko-české	moravsko-český	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
na	na	k7c6
současných	současný	k2eAgFnPc6d1
topografických	topografický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
měřítka	měřítko	k1gNnPc4
1	#num#	k4
<g/>
:	:	kIx,
<g/>
50	#num#	k4
000	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Historické	historický	k2eAgFnSc2d1
české	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
</s>
<s>
Čechy	Čechy	k1gFnPc1
</s>
<s>
Morava	Morava	k1gFnSc1
</s>
<s>
Slezsko	Slezsko	k1gNnSc1
</s>
<s>
Československo	Československo	k1gNnSc1
–	–	k?
administrativní	administrativní	k2eAgNnSc4d1
dělení	dělení	k1gNnSc4
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
přechodné	přechodný	k2eAgNnSc1d1
období	období	k1gNnSc1
1918	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
</s>
<s>
Česko	Česko	k1gNnSc1
</s>
<s>
země	země	k1gFnSc1
1918	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
•	•	k?
Moravská	moravský	k2eAgFnSc1d1
•	•	k?
Slezská	Slezská	k1gFnSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
</s>
<s>
župy	župa	k1gFnPc1
1918	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
</s>
<s>
Bratislavská	bratislavský	k2eAgFnSc1d1
•	•	k?
Komárenská	komárenský	k2eAgFnSc1d1
•	•	k?
Nitranská	nitranský	k2eAgFnSc1d1
•	•	k?
Trenčínská	trenčínský	k2eAgFnSc1d1
•	•	k?
Tekovská	Tekovská	k1gFnSc1
•	•	k?
Turčanská	Turčanský	k2eAgFnSc1d1
•	•	k?
Oravská	oravský	k2eAgFnSc1d1
•	•	k?
Liptovská	liptovský	k2eAgFnSc1d1
•	•	k?
Zvolenská	zvolenský	k2eAgFnSc1d1
•	•	k?
Hontská	Hontský	k2eAgFnSc1d1
•	•	k?
Novohradská	novohradský	k2eAgFnSc1d1
•	•	k?
Gemersko-malohontská	Gemersko-malohontský	k2eAgFnSc1d1
•	•	k?
Spišská	spišský	k2eAgFnSc1d1
•	•	k?
Abauj-turňanská	Abauj-turňanský	k2eAgFnSc1d1
župa	župa	k1gFnSc1
•	•	k?
Šarišská	šarišský	k2eAgFnSc1d1
•	•	k?
Zemplínskámunicipální	Zemplínskámunicipální	k2eAgMnPc1d1
města	město	k1gNnSc2
na	na	k7c6
úrovni	úroveň	k1gFnSc6
žup	župa	k1gFnPc2
<g/>
:	:	kIx,
Banská	banský	k2eAgFnSc1d1
Štiavnica	Štiavnica	k1gFnSc1
a	a	k8xC
Banská	banský	k2eAgFnSc1d1
Belá	Belá	k1gFnSc1
•	•	k?
Bratislava	Bratislava	k1gFnSc1
•	•	k?
Komárno	Komárno	k1gNnSc1
•	•	k?
Košice	Košice	k1gInPc1
župy	župa	k1gFnSc2
1923	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
</s>
<s>
Bratislavská	bratislavský	k2eAgFnSc1d1
•	•	k?
Nitranská	nitranský	k2eAgFnSc1d1
•	•	k?
Povážská	povážský	k2eAgFnSc1d1
•	•	k?
Zvolenská	zvolenský	k2eAgFnSc1d1
•	•	k?
Podtatranská	podtatranský	k2eAgFnSc1d1
•	•	k?
Košická	košický	k2eAgFnSc1d1
</s>
<s>
Podkarpatská	podkarpatský	k2eAgFnSc1d1
Rus	Rus	k1gFnSc1
</s>
<s>
župy	župa	k1gFnPc1
1919	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
</s>
<s>
Berežská	Berežský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marmarošská	marmarošský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1920	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mukačevská	Mukačevský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1921	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Podkarpatoruská	podkarpatoruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Užhorodská	užhorodský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
země	země	k1gFnSc1
1928	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
•	•	k?
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
(	(	kIx(
<g/>
Slezská	slezský	k2eAgFnSc1d1
expozitura	expozitura	k1gFnSc1
země	zem	k1gFnSc2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
•	•	k?
Podkarpatoruská	podkarpatoruský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
kraje	kraj	k1gInSc2
1948	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
</s>
<s>
české	český	k2eAgFnPc1d1
</s>
<s>
Pražský	pražský	k2eAgInSc1d1
•	•	k?
Českobudějovický	českobudějovický	k2eAgInSc1d1
•	•	k?
Plzeňský	plzeňský	k2eAgInSc1d1
•	•	k?
Karlovarský	karlovarský	k2eAgInSc1d1
•	•	k?
Ústecký	ústecký	k2eAgInSc1d1
•	•	k?
Liberecký	liberecký	k2eAgInSc1d1
•	•	k?
Hradecký	hradecký	k2eAgInSc1d1
•	•	k?
Pardubický	pardubický	k2eAgInSc1d1
•	•	k?
Jihlavský	jihlavský	k2eAgInSc1d1
•	•	k?
Brněnský	brněnský	k2eAgInSc1d1
•	•	k?
Olomoucký	olomoucký	k2eAgInSc1d1
•	•	k?
Gottwaldovský	gottwaldovský	k2eAgInSc1d1
•	•	k?
Ostravský	ostravský	k2eAgInSc1d1
slovenské	slovenský	k2eAgFnSc3d1
</s>
<s>
Bratislavský	bratislavský	k2eAgInSc1d1
•	•	k?
Nitranský	nitranský	k2eAgInSc1d1
•	•	k?
Banskobystrický	banskobystrický	k2eAgInSc1d1
•	•	k?
Žilinský	žilinský	k2eAgInSc1d1
•	•	k?
Košický	košický	k2eAgInSc1d1
•	•	k?
Prešovský	prešovský	k2eAgInSc1d1
</s>
<s>
kraje	kraj	k1gInPc1
1960	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
české	český	k2eAgFnPc1d1
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
•	•	k?
Jihočeský	jihočeský	k2eAgInSc1d1
•	•	k?
Západočeský	západočeský	k2eAgInSc1d1
•	•	k?
Severočeský	severočeský	k2eAgInSc1d1
•	•	k?
Východočeský	východočeský	k2eAgInSc1d1
•	•	k?
Jihomoravský	jihomoravský	k2eAgInSc1d1
•	•	k?
Severomoravskýsamostatná	Severomoravskýsamostatný	k2eAgNnPc1d1
města	město	k1gNnPc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
krajů	kraj	k1gInPc2
<g/>
:	:	kIx,
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Praha	Praha	k1gFnSc1
•	•	k?
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
slovenské	slovenský	k2eAgFnSc2d1
</s>
<s>
Západoslovenský	západoslovenský	k2eAgInSc1d1
•	•	k?
Středoslovenský	středoslovenský	k2eAgInSc1d1
•	•	k?
Východoslovenský	východoslovenský	k2eAgInSc1d1
(	(	kIx(
<g/>
slovenské	slovenský	k2eAgInPc1d1
kraje	kraj	k1gInPc1
zrušeny	zrušen	k2eAgInPc1d1
1969	#num#	k4
<g/>
–	–	k?
<g/>
1970	#num#	k4
a	a	k8xC
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
samostatné	samostatný	k2eAgNnSc1d1
město	město	k1gNnSc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
kraje	kraj	k1gInSc2
<g/>
:	:	kIx,
Bratislava	Bratislava	k1gFnSc1
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
;	;	kIx,
od	od	k7c2
1969	#num#	k4
hlavné	hlavný	k2eAgNnSc1d1
mesto	mesto	k1gNnSc1
SSR	SSR	kA
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
1990	#num#	k4
hlavné	hlavný	k2eAgNnSc4d1
mesto	mesto	k1gNnSc4
SR	SR	kA
Bratislava	Bratislava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
republiky	republika	k1gFnPc1
1969	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
ČSR	ČSR	kA
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1
socialistická	socialistický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
SSR	SSR	kA
<g/>
,	,	kIx,
1969	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slovenská	slovenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
SR	SR	kA
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
130070	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4074432-2	4074432-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81033397	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
131280017	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81033397	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Morava	Morava	k1gFnSc1
</s>
