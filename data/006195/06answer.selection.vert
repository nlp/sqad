<s>
Lisabonská	lisabonský	k2eAgFnSc1d1	Lisabonská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
platná	platný	k2eAgFnSc1d1	platná
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Evropská	evropský	k2eAgFnSc1d1	Evropská
rada	rada	k1gFnSc1	rada
volí	volit	k5eAaImIp3nS	volit
svého	svůj	k3xOyFgMnSc4	svůj
stálého	stálý	k2eAgMnSc4d1	stálý
předsedu	předseda	k1gMnSc4	předseda
kvalifikovanou	kvalifikovaný	k2eAgFnSc7d1	kvalifikovaná
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
a	a	k8xC	a
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
možností	možnost	k1gFnSc7	možnost
znovuzvolení	znovuzvolení	k1gNnSc2	znovuzvolení
<g/>
.	.	kIx.	.
</s>
