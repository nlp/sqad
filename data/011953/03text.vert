<p>
<s>
Malhostovice	Malhostovice	k1gFnPc1	Malhostovice
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
poblíž	poblíž	k7c2	poblíž
Tišnova	Tišnov	k1gInSc2	Tišnov
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
Boskovické	boskovický	k2eAgFnSc2d1	boskovická
brázdy	brázda	k1gFnSc2	brázda
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
967	[number]	k4	967
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samotných	samotný	k2eAgFnPc2d1	samotná
Malhostovic	Malhostovice	k1gFnPc2	Malhostovice
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnPc2	součást
obce	obec	k1gFnSc2	obec
také	také	k9	také
vesnice	vesnice	k1gFnSc2	vesnice
Nuzířov	Nuzířov	k1gInSc1	Nuzířov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1320	[number]	k4	1320
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
bývalý	bývalý	k2eAgInSc1d1	bývalý
vojenský	vojenský	k2eAgInSc1d1	vojenský
výcvikový	výcvikový	k2eAgInSc1d1	výcvikový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pozorovací	pozorovací	k2eAgFnSc1d1	pozorovací
věž	věž	k1gFnSc1	věž
byla	být	k5eAaImAgFnS	být
přebudována	přebudovat	k5eAaPmNgFnS	přebudovat
na	na	k7c4	na
rozhlednu	rozhledna	k1gFnSc4	rozhledna
Zlobice	Zlobice	k1gFnSc2	Zlobice
a	a	k8xC	a
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
pasou	pást	k5eAaImIp3nP	pást
ovce	ovce	k1gFnSc2	ovce
a	a	k8xC	a
kozy	koza	k1gFnSc2	koza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Filiální	filiální	k2eAgInSc1d1	filiální
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodáci	rodák	k1gMnPc5	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Podborský	Podborský	k2eAgMnSc1d1	Podborský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
archeolog	archeolog	k1gMnSc1	archeolog
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
farnost	farnost	k1gFnSc1	farnost
Drásov	Drásov	k1gInSc1	Drásov
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Malhostovice	Malhostovice	k1gFnSc2	Malhostovice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Malhostovice	Malhostovice	k1gFnSc2	Malhostovice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnSc2	obec
Malhostovice	Malhostovice	k1gFnSc2	Malhostovice
</s>
</p>
<p>
<s>
Sokol	Sokol	k1gMnSc1	Sokol
Malhostovice	Malhostovice	k1gFnSc2	Malhostovice
</s>
</p>
