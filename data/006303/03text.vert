<s>
ČT24	ČT24	k4	ČT24
je	být	k5eAaImIp3nS	být
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
kanál	kanál	k1gInSc4	kanál
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
věnuje	věnovat	k5eAaPmIp3nS	věnovat
převážně	převážně	k6eAd1	převážně
zpravodajství	zpravodajství	k1gNnSc1	zpravodajství
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetového	internetový	k2eAgNnSc2d1	internetové
a	a	k8xC	a
digitálního	digitální	k2eAgNnSc2d1	digitální
satelitního	satelitní	k2eAgNnSc2d1	satelitní
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Křišťálová	křišťálový	k2eAgFnSc1d1	Křišťálová
Lupa	lupa	k1gFnSc1	lupa
4	[number]	k4	4
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
média	médium	k1gNnSc2	médium
-	-	kIx~	-
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
<g/>
.	.	kIx.	.
</s>
<s>
Internetový	internetový	k2eAgInSc1d1	internetový
portál	portál	k1gInSc1	portál
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
se	se	k3xPyFc4	se
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
společností	společnost	k1gFnPc2	společnost
Mediasearch	Mediasearcha	k1gFnPc2	Mediasearcha
o	o	k7c4	o
'	'	kIx"	'
<g/>
nejdůvěryhodnější	důvěryhodný	k2eAgInSc4d3	nejdůvěryhodnější
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
server	server	k1gInSc4	server
<g/>
'	'	kIx"	'
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Web	web	k1gInSc1	web
ČT24	ČT24	k1gFnSc2	ČT24
byl	být	k5eAaImAgInS	být
vysoce	vysoce	k6eAd1	vysoce
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
kritériích	kritérion	k1gNnPc6	kritérion
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
'	'	kIx"	'
<g/>
obsah	obsah	k1gInSc1	obsah
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
přehlednost	přehlednost	k1gFnSc1	přehlednost
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
množství	množství	k1gNnSc1	množství
reklam	reklama	k1gFnPc2	reklama
<g/>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
<g/>
první	první	k4xOgInSc1	první
dojem	dojem	k1gInSc1	dojem
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
ČT24	ČT24	k1gFnSc2	ČT24
je	být	k5eAaImIp3nS	být
též	též	k9	též
nejvyužívanější	využívaný	k2eAgFnPc4d3	nejvyužívanější
aplikace	aplikace	k1gFnPc4	aplikace
pro	pro	k7c4	pro
sledování	sledování	k1gNnSc4	sledování
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
mobilu	mobil	k1gInSc2	mobil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
vysílá	vysílat	k5eAaImIp3nS	vysílat
ČT24	ČT24	k1gMnSc1	ČT24
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
pořady	pořad	k1gInPc1	pořad
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
programu	program	k1gInSc6	program
jsou	být	k5eAaImIp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
v	v	k7c6	v
širokoúhlém	širokoúhlý	k2eAgInSc6d1	širokoúhlý
formátu	formát	k1gInSc6	formát
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
a	a	k8xC	a
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
vysílá	vysílat	k5eAaImIp3nS	vysílat
stanice	stanice	k1gFnSc1	stanice
aktuální	aktuální	k2eAgFnSc2d1	aktuální
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
intervalu	interval	k1gInSc6	interval
30	[number]	k4	30
popřípadě	popřípadě	k6eAd1	popřípadě
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
vysílá	vysílat	k5eAaImIp3nS	vysílat
10-20	[number]	k4	10-20
minutovou	minutový	k2eAgFnSc4d1	minutová
relaci	relace	k1gFnSc4	relace
Zprávy	zpráva	k1gFnPc1	zpráva
včetně	včetně	k7c2	včetně
krátké	krátký	k2eAgFnSc2d1	krátká
předpovědi	předpověď	k1gFnSc2	předpověď
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgFnPc1	čtyři
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
šest	šest	k4xCc1	šest
relací	relace	k1gFnPc2	relace
je	být	k5eAaImIp3nS	být
třicetiminutových	třicetiminutový	k2eAgInPc2d1	třicetiminutový
a	a	k8xC	a
nesou	nést	k5eAaImIp3nP	nést
názvy	název	k1gInPc1	název
Zprávy	zpráva	k1gFnSc2	zpráva
ve	v	k7c6	v
12	[number]	k4	12
(	(	kIx(	(
<g/>
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zprávy	zpráva	k1gFnPc1	zpráva
v	v	k7c6	v
16	[number]	k4	16
(	(	kIx(	(
<g/>
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Události	událost	k1gFnPc1	událost
(	(	kIx(	(
<g/>
v	v	k7c6	v
19	[number]	k4	19
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zprávy	zpráva	k1gFnPc1	zpráva
ve	v	k7c6	v
23	[number]	k4	23
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
ve	v	k7c4	v
všední	všední	k2eAgInPc4d1	všední
dny	den	k1gInPc4	den
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pořadu	pořad	k1gInSc2	pořad
Studio	studio	k1gNnSc1	studio
6	[number]	k4	6
i	i	k8xC	i
Zprávy	zpráva	k1gFnPc1	zpráva
v	v	k7c6	v
10	[number]	k4	10
(	(	kIx(	(
<g/>
v	v	k7c6	v
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
i	i	k9	i
sportovní	sportovní	k2eAgNnPc1d1	sportovní
zpravodajství	zpravodajství	k1gNnPc1	zpravodajství
a	a	k8xC	a
předpověď	předpověď	k1gFnSc1	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
hlavní	hlavní	k2eAgFnSc1d1	hlavní
relace	relace	k1gFnSc1	relace
od	od	k7c2	od
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
následují	následovat	k5eAaImIp3nP	následovat
samostatné	samostatný	k2eAgFnPc1d1	samostatná
Branky	branka	k1gFnPc1	branka
<g/>
,	,	kIx,	,
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
vteřiny	vteřina	k1gFnPc1	vteřina
(	(	kIx(	(
<g/>
sportovní	sportovní	k2eAgFnSc1d1	sportovní
<g/>
)	)	kIx)	)
a	a	k8xC	a
Předpověď	předpověď	k1gFnSc1	předpověď
počasí	počasí	k1gNnSc2	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
všední	všední	k2eAgInPc4d1	všední
dny	den	k1gInPc4	den
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
22.00	[number]	k4	22.00
hod	hod	k1gInSc1	hod
vysílán	vysílán	k2eAgInSc1d1	vysílán
zpravodajský	zpravodajský	k2eAgInSc1d1	zpravodajský
pořad	pořad	k1gInSc1	pořad
Události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInPc1	komentář
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
zobrazovány	zobrazovat	k5eAaImNgInP	zobrazovat
nepřetržitě	přetržitě	k6eNd1	přetržitě
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
informačního	informační	k2eAgInSc2d1	informační
proužku	proužek	k1gInSc2	proužek
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
obrazu	obraz	k1gInSc2	obraz
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ticker	ticker	k1gInSc1	ticker
nebo	nebo	k8xC	nebo
crawl	crawl	k1gInSc1	crawl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kromě	kromě	k7c2	kromě
aktuálních	aktuální	k2eAgFnPc2d1	aktuální
zpráv	zpráva	k1gFnPc2	zpráva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
časový	časový	k2eAgInSc4d1	časový
údaj	údaj	k1gInSc4	údaj
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
HH	HH	kA	HH
<g/>
:	:	kIx,	:
<g/>
MM	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
ČT24	ČT24	k4	ČT24
vysílá	vysílat	k5eAaImIp3nS	vysílat
kromě	kromě	k7c2	kromě
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
např.	např.	kA	např.
pořady	pořad	k1gInPc4	pořad
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
a	a	k8xC	a
Objektiv	objektiv	k1gInSc1	objektiv
obsahující	obsahující	k2eAgFnSc2d1	obsahující
reportáže	reportáž	k1gFnSc2	reportáž
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
pořad	pořad	k1gInSc1	pořad
o	o	k7c6	o
životě	život	k1gInSc6	život
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
Retro	retro	k1gNnSc2	retro
<g/>
,	,	kIx,	,
publicistický	publicistický	k2eAgInSc1d1	publicistický
pořad	pořad	k1gInSc1	pořad
168	[number]	k4	168
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
magazín	magazín	k1gInSc1	magazín
o	o	k7c4	o
počasí	počasí	k1gNnSc4	počasí
Turbulence	turbulence	k1gFnSc2	turbulence
<g/>
,	,	kIx,	,
zdravotnický	zdravotnický	k2eAgInSc4d1	zdravotnický
a	a	k8xC	a
lifestylový	lifestylový	k2eAgInSc4d1	lifestylový
magazín	magazín	k1gInSc4	magazín
Tep	tep	k1gInSc1	tep
24	[number]	k4	24
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pořady	pořad	k1gInPc1	pořad
jsou	být	k5eAaImIp3nP	být
vysílány	vysílat	k5eAaImNgInP	vysílat
souběžně	souběžně	k6eAd1	souběžně
i	i	k8xC	i
na	na	k7c6	na
programu	program	k1gInSc6	program
ČT1	ČT1	k1gFnSc2	ČT1
(	(	kIx(	(
<g/>
Zprávy	zpráva	k1gFnPc1	zpráva
ve	v	k7c4	v
12	[number]	k4	12
<g/>
,	,	kIx,	,
Události	událost	k1gFnPc1	událost
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
diskusního	diskusní	k2eAgInSc2d1	diskusní
pořadu	pořad	k1gInSc2	pořad
Otázky	otázka	k1gFnSc2	otázka
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ČT24	ČT24	k4	ČT24
také	také	k6eAd1	také
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c6	na
náhlé	náhlý	k2eAgFnSc6d1	náhlá
důležité	důležitý	k2eAgFnSc6d1	důležitá
události	událost	k1gFnSc6	událost
mimořádným	mimořádný	k2eAgNnSc7d1	mimořádné
vysíláním	vysílání	k1gNnSc7	vysílání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zahájit	zahájit	k5eAaPmF	zahájit
během	během	k7c2	během
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Sledovanost	sledovanost	k1gFnSc1	sledovanost
ČT24	ČT24	k1gFnSc2	ČT24
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
činila	činit	k5eAaImAgFnS	činit
ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
15	[number]	k4	15
<g/>
+	+	kIx~	+
3,40	[number]	k4	3,40
<g/>
%	%	kIx~	%
celodenně	celodenně	k6eAd1	celodenně
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
pak	pak	k6eAd1	pak
2,09	[number]	k4	2,09
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
skupině	skupina	k1gFnSc6	skupina
15-54	[number]	k4	15-54
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
celodenní	celodenní	k2eAgFnSc1d1	celodenní
sledovanost	sledovanost	k1gFnSc1	sledovanost
2,04	[number]	k4	2,04
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
1,24	[number]	k4	1,24
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
sledovanost	sledovanost	k1gFnSc1	sledovanost
ČT24	ČT24	k1gFnSc1	ČT24
3,99	[number]	k4	3,99
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
činilo	činit	k5eAaImAgNnS	činit
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
veřejnoprávní	veřejnoprávní	k2eAgInSc4d1	veřejnoprávní
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
televizní	televizní	k2eAgInSc4d1	televizní
kanál	kanál	k1gInSc4	kanál
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
začal	začít	k5eAaPmAgInS	začít
přinášet	přinášet	k5eAaImF	přinášet
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
www.ct24.cz	www.ct24.cza	k1gFnPc2	www.ct24.cza
a	a	k8xC	a
též	též	k9	též
digitálním	digitální	k2eAgNnSc7d1	digitální
vysíláním	vysílání	k1gNnSc7	vysílání
současně	současně	k6eAd1	současně
přes	přes	k7c4	přes
družice	družice	k1gFnPc4	družice
Astra	astra	k1gFnSc1	astra
1	[number]	k4	1
a	a	k8xC	a
3A	[number]	k4	3A
a	a	k8xC	a
Eurobird	Eurobird	k1gInSc1	Eurobird
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
vysílala	vysílat	k5eAaImAgFnS	vysílat
v	v	k7c6	v
řádném	řádný	k2eAgNnSc6d1	řádné
pozemním	pozemní	k2eAgNnSc6d1	pozemní
digitálním	digitální	k2eAgNnSc6d1	digitální
vysílání	vysílání	k1gNnSc6	vysílání
v	v	k7c6	v
přechodném	přechodný	k2eAgInSc6d1	přechodný
multiplexu	multiplex	k1gInSc6	multiplex
A.	A.	kA	A.
Ten	ten	k3xDgInSc1	ten
od	od	k7c2	od
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
vysílá	vysílat	k5eAaImIp3nS	vysílat
pouze	pouze	k6eAd1	pouze
programy	program	k1gInPc1	program
ČT	ČT	kA	ČT
a	a	k8xC	a
ČRo	ČRo	k1gFnSc1	ČRo
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
Multiplex	multiplex	k1gInSc1	multiplex
1	[number]	k4	1
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
<g/>
%	%	kIx~	%
pokrytí	pokrytí	k1gNnSc1	pokrytí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c4	na
orbitální	orbitální	k2eAgFnSc4d1	orbitální
pozici	pozice	k1gFnSc4	pozice
23,5	[number]	k4	23,5
<g/>
°	°	k?	°
východně	východně	k6eAd1	východně
přes	přes	k7c4	přes
družici	družice	k1gFnSc4	družice
Astra	astra	k1gFnSc1	astra
3A	[number]	k4	3A
v	v	k7c6	v
paketu	paket	k1gInSc6	paket
Skylink	Skylink	k1gInSc1	Skylink
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
0,8	[number]	k4	0,8
<g/>
°	°	k?	°
západně	západně	k6eAd1	západně
družici	družice	k1gFnSc4	družice
na	na	k7c4	na
Thor	Thor	k1gInSc4	Thor
6	[number]	k4	6
v	v	k7c6	v
paketu	paket	k1gInSc6	paket
UPC	UPC	kA	UPC
Direct	Direct	k1gInSc1	Direct
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
orbitální	orbitální	k2eAgFnSc4d1	orbitální
pozici	pozice	k1gFnSc4	pozice
1	[number]	k4	1
<g/>
°	°	k?	°
západně	západně	k6eAd1	západně
na	na	k7c6	na
satelitu	satelit	k1gInSc6	satelit
Intelsat	Intelsat	k1gFnSc2	Intelsat
10-02	[number]	k4	10-02
v	v	k7c6	v
paketu	paket	k1gInSc6	paket
Digi	Dig	k1gFnSc2	Dig
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
také	také	k9	také
přenášejí	přenášet	k5eAaImIp3nP	přenášet
některé	některý	k3yIgFnPc1	některý
kabelové	kabelový	k2eAgFnPc1d1	kabelová
sítě	síť	k1gFnPc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
#	#	kIx~	#
<g/>
Kritika	kritika	k1gFnSc1	kritika
<g/>
.	.	kIx.	.
</s>
