<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
šachová	šachový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
příp	příp	kA
<g/>
.	.	kIx.
Mezinárodní	mezinárodní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
šachu	šach	k1gInSc2
(	(	kIx(
<g/>
FIDE	FIDE	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Fédération	Fédération	k1gInSc1
Internationale	Internationale	k1gFnSc2
des	des	k1gNnSc2
Échecs	Échecs	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
sdružující	sdružující	k2eAgFnSc2d1
jednotlivé	jednotlivý	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
šachové	šachový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
<g/>
.	.	kIx.
</s>
