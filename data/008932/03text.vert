<p>
<s>
Walter	Walter	k1gMnSc1	Walter
Elias	Elias	k1gMnSc1	Elias
"	"	kIx"	"
<g/>
Walt	Walt	k1gMnSc1	Walt
<g/>
"	"	kIx"	"
Disney	Disney	k1gInPc1	Disney
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1901	[number]	k4	1901
<g/>
,	,	kIx,	,
Hermosa	Hermosa	k1gFnSc1	Hermosa
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gInSc1	Illinois
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Burbank	Burbank	k1gInSc1	Burbank
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
dabér	dabér	k1gMnSc1	dabér
<g/>
,	,	kIx,	,
animátor	animátor	k1gMnSc1	animátor
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
filantrop	filantrop	k1gMnSc1	filantrop
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
zábavní	zábavní	k2eAgInSc1d1	zábavní
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Royem	Roy	k1gMnSc7	Roy
O.	O.	kA	O.
Disneyem	Disneyem	k1gInSc1	Disneyem
<g/>
)	)	kIx)	)
firmy	firma	k1gFnPc1	firma
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productionsa	k1gFnPc2	Productionsa
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgMnPc2d3	nejznámější
filmových	filmový	k2eAgMnPc2d1	filmový
producentů	producent	k1gMnPc2	producent
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
The	The	k1gMnSc1	The
Walt	Walt	k1gMnSc1	Walt
Disney	Disne	k2eAgFnPc4d1	Disne
Company	Compana	k1gFnPc4	Compana
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
roční	roční	k2eAgInSc4d1	roční
obrat	obrat	k1gInSc4	obrat
asi	asi	k9	asi
35	[number]	k4	35
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znám	znám	k2eAgMnSc1d1	znám
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
bavič	bavič	k1gMnSc1	bavič
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
také	také	k9	také
jako	jako	k8xS	jako
inovátor	inovátor	k1gMnSc1	inovátor
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
a	a	k8xC	a
zábavních	zábavní	k2eAgInPc2d1	zábavní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
společnosti	společnost	k1gFnSc2	společnost
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
řadu	řada	k1gFnSc4	řada
světově	světově	k6eAd1	světově
známých	známý	k2eAgFnPc2d1	známá
postaviček	postavička	k1gFnPc2	postavička
včetně	včetně	k7c2	včetně
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
Disney	Disnea	k1gFnPc4	Disnea
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
i	i	k9	i
vlastní	vlastní	k2eAgInSc4d1	vlastní
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
26	[number]	k4	26
Oskarů	Oskar	k1gMnPc2	Oskar
z	z	k7c2	z
59	[number]	k4	59
nominací	nominace	k1gFnPc2	nominace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rekordních	rekordní	k2eAgInPc2d1	rekordní
čtyř	čtyři	k4xCgMnPc2	čtyři
Oskarů	Oskar	k1gMnPc2	Oskar
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
získal	získat	k5eAaPmAgMnS	získat
více	hodně	k6eAd2	hodně
cen	cena	k1gFnPc2	cena
a	a	k8xC	a
nominací	nominace	k1gFnPc2	nominace
než	než	k8xS	než
kdokoli	kdokoli	k3yInSc1	kdokoli
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
obdržel	obdržet	k5eAaPmAgMnS	obdržet
sedm	sedm	k4xCc4	sedm
ocenění	ocenění	k1gNnPc2	ocenění
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
zábavní	zábavní	k2eAgInPc1d1	zábavní
parky	park	k1gInPc1	park
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
(	(	kIx(	(
<g/>
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
,	,	kIx,	,
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	World	k1gMnSc1	World
Resort	resort	k1gInSc1	resort
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
(	(	kIx(	(
<g/>
Tokyo	Tokyo	k1gMnSc1	Tokyo
Disney	Disnea	k1gFnSc2	Disnea
Resort	resort	k1gInSc1	resort
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
(	(	kIx(	(
<g/>
Disneyland	Disneyland	k1gInSc1	Disneyland
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
a	a	k8xC	a
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
Disneyland	Disneyland	k1gInSc1	Disneyland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
plic	plíce	k1gFnPc2	plíce
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Burbanku	Burbanka	k1gFnSc4	Burbanka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
:	:	kIx,	:
Začátky	začátek	k1gInPc1	začátek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1901	[number]	k4	1901
Eliasu	Elias	k1gInSc2	Elias
Disneyovi	Disneya	k1gMnSc6	Disneya
a	a	k8xC	a
Floře	Flora	k1gFnSc6	Flora
Call	Calla	k1gFnPc2	Calla
Disneyové	Disneyové	k2eAgFnPc2d1	Disneyové
v	v	k7c6	v
Hermose	Hermosa	k1gFnSc6	Hermosa
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
na	na	k7c4	na
2156	[number]	k4	2156
N.	N.	kA	N.
Tripp	Tripp	k1gInSc4	Tripp
Ave	ave	k1gNnSc2	ave
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
předkové	předek	k1gMnPc1	předek
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
z	z	k7c2	z
irského	irský	k2eAgInSc2d1	irský
Gowranu	Gowran	k1gInSc2	Gowran
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Kilkenny	Kilkenna	k1gFnSc2	Kilkenna
<g/>
.	.	kIx.	.
</s>
<s>
Arundel	Arundlo	k1gNnPc2	Arundlo
Elias	Elias	k1gMnSc1	Elias
Disney	Disnea	k1gFnSc2	Disnea
<g/>
,	,	kIx,	,
praděd	praděd	k1gMnSc1	praděd
Walta	Walta	k1gMnSc1	Walta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Kilkenny	Kilkenn	k1gInPc4	Kilkenn
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
potomek	potomek	k1gMnSc1	potomek
francouze	francouze	k1gFnSc2	francouze
Roberta	Robert	k1gMnSc2	Robert
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Isignyho	Isigny	k1gMnSc2	Isigny
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1066	[number]	k4	1066
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
společně	společně	k6eAd1	společně
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
I.	I.	kA	I.
Dobyvatelem	dobyvatel	k1gMnSc7	dobyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Isigny	Isigna	k1gMnSc2	Isigna
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
změněno	změnit	k5eAaPmNgNnS	změnit
na	na	k7c4	na
Disney	Disnea	k1gFnPc4	Disnea
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
Norton	Norton	k1gInSc1	Norton
Disney	Disnea	k1gFnSc2	Disnea
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
Lincoln	Lincoln	k1gMnSc1	Lincoln
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Elias	Eliasa	k1gFnPc2	Eliasa
Disney	Disnea	k1gFnSc2	Disnea
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
z	z	k7c2	z
kanadského	kanadský	k2eAgMnSc2d1	kanadský
Huron	Huron	k1gInSc1	Huron
County	Counta	k1gFnSc2	Counta
do	do	k7c2	do
USA	USA	kA	USA
roku	rok	k1gInSc2	rok
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
nalezl	naleznout	k5eAaPmAgInS	naleznout
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
rodičům	rodič	k1gMnPc3	rodič
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Ellis	Ellis	k1gFnSc2	Ellis
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
železnici	železnice	k1gFnSc4	železnice
Union	union	k1gInSc1	union
Pacific	Pacific	k1gMnSc1	Pacific
Railroad	Railroad	k1gInSc1	Railroad
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1888	[number]	k4	1888
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgMnS	vzít
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
Floru	Flora	k1gFnSc4	Flora
Callovou	Callová	k1gFnSc4	Callová
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Acron	Acrona	k1gFnPc2	Acrona
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
Eliasův	Eliasův	k2eAgMnSc1d1	Eliasův
bratr	bratr	k1gMnSc1	bratr
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
často	často	k6eAd1	často
musel	muset	k5eAaImAgMnS	muset
Eliasovi	Elias	k1gMnSc3	Elias
finančně	finančně	k6eAd1	finančně
pomáhat	pomáhat	k5eAaImF	pomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Waltovi	Waltův	k2eAgMnPc1d1	Waltův
čtyři	čtyři	k4xCgMnPc1	čtyři
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Elias	Elias	k1gInSc1	Elias
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
na	na	k7c4	na
farmu	farma	k1gFnSc4	farma
v	v	k7c6	v
Marceline	Marcelin	k1gInSc5	Marcelin
v	v	k7c6	v
Missouri	Missouri	k1gFnSc2	Missouri
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
právě	právě	k9	právě
koupil	koupit	k5eAaPmAgMnS	koupit
Eliasův	Eliasův	k2eAgMnSc1d1	Eliasův
bratr	bratr	k1gMnSc1	bratr
Roy	Roy	k1gMnSc1	Roy
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
u	u	k7c2	u
Walta	Walt	k1gInSc2	Walt
projevila	projevit	k5eAaPmAgFnS	projevit
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
kreslení	kreslení	k1gNnSc3	kreslení
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
sousedů	soused	k1gMnPc2	soused
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
na	na	k7c6	na
penzi	penze	k1gFnSc6	penze
jménem	jméno	k1gNnSc7	jméno
"	"	kIx"	"
<g/>
Doc	doc	kA	doc
<g/>
"	"	kIx"	"
Sherwood	Sherwood	k1gInSc1	Sherwood
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
za	za	k7c4	za
kresbu	kresba	k1gFnSc4	kresba
svého	svůj	k3xOyFgMnSc2	svůj
koně	kůň	k1gMnSc2	kůň
Ruperta	Rupert	k1gMnSc2	Rupert
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
projevil	projevit	k5eAaPmAgInS	projevit
Waltův	Waltův	k2eAgInSc1d1	Waltův
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
vlaky	vlak	k1gInPc4	vlak
<g/>
,	,	kIx,	,
městem	město	k1gNnSc7	město
totiž	totiž	k9	totiž
prochází	procházet	k5eAaImIp3nS	procházet
Atchison	Atchison	k1gInSc1	Atchison
<g/>
,	,	kIx,	,
Topeka	Topeko	k1gNnPc1	Topeko
and	and	k?	and
Santa	Sant	k1gMnSc2	Sant
Fe	Fe	k1gMnSc2	Fe
Railway	Railwaa	k1gMnSc2	Railwaa
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
železnic	železnice	k1gFnPc2	železnice
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
často	často	k6eAd1	často
dával	dávat	k5eAaImAgMnS	dávat
ucho	ucho	k1gNnSc4	ucho
na	na	k7c4	na
koleje	kolej	k1gFnPc4	kolej
a	a	k8xC	a
vyhlížel	vyhlížet	k5eAaImAgInS	vyhlížet
vlak	vlak	k1gInSc1	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
rozběhl	rozběhnout	k5eAaPmAgMnS	rozběhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
našel	najít	k5eAaPmAgMnS	najít
svého	svůj	k3xOyFgMnSc4	svůj
strýce	strýc	k1gMnSc4	strýc
<g/>
,	,	kIx,	,
strojvůdce	strojvůdce	k1gMnSc4	strojvůdce
Michaela	Michael	k1gMnSc4	Michael
Martina	Martin	k1gMnSc4	Martin
<g/>
.	.	kIx.	.
<g/>
Disneyovi	Disneyův	k2eAgMnPc1d1	Disneyův
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
Marceline	Marcelin	k1gInSc5	Marcelin
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc4	City
Zde	zde	k6eAd1	zde
Walt	Walt	k1gInSc1	Walt
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mladší	mladý	k2eAgFnSc7d2	mladší
sestrou	sestra	k1gFnSc7	sestra
Ruth	Ruth	k1gFnSc2	Ruth
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
Benton	Benton	k1gInSc1	Benton
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkal	potkat	k5eAaPmAgInS	potkat
Waltera	Walter	k1gMnSc4	Walter
Pfeifferta	Pfeiffert	k1gMnSc4	Pfeiffert
<g/>
.	.	kIx.	.
</s>
<s>
Pfeiffertovi	Pfeiffertův	k2eAgMnPc1d1	Pfeiffertův
byli	být	k5eAaImAgMnP	být
divadelní	divadelní	k2eAgMnPc1d1	divadelní
herci	herec	k1gMnPc1	herec
a	a	k8xC	a
Walta	Walta	k1gFnSc1	Walta
zasvětili	zasvětit	k5eAaPmAgMnP	zasvětit
do	do	k7c2	do
světa	svět	k1gInSc2	svět
varieté	varieté	k1gNnSc2	varieté
a	a	k8xC	a
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
trávil	trávit	k5eAaImAgMnS	trávit
více	hodně	k6eAd2	hodně
času	čas	k1gInSc2	čas
u	u	k7c2	u
Pfeiffertových	Pfeiffertová	k1gFnPc2	Pfeiffertová
než	než	k8xS	než
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
času	čas	k1gInSc2	čas
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
sobotní	sobotní	k2eAgInPc4d1	sobotní
kurzy	kurz	k1gInPc4	kurz
v	v	k7c6	v
uměleckém	umělecký	k2eAgInSc6d1	umělecký
institutu	institut	k1gInSc6	institut
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
žili	žít	k5eAaImAgMnP	žít
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
Walt	Walt	k1gInSc1	Walt
a	a	k8xC	a
Ruth	Ruth	k1gFnSc1	Ruth
také	také	k9	také
pravidelně	pravidelně	k6eAd1	pravidelně
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
Electric	Electrice	k1gFnPc2	Electrice
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
15	[number]	k4	15
bloků	blok	k1gInPc2	blok
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
domova	domov	k1gInSc2	domov
(	(	kIx(	(
<g/>
Walt	Walt	k1gMnSc1	Walt
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabavní	zabavní	k2eAgInSc1d1	zabavní
park	park	k1gInSc1	park
měl	mít	k5eAaImAgInS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
design	design	k1gInSc4	design
Disneylandu	Disneyland	k1gInSc2	Disneyland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
jeho	jeho	k3xOp3gFnSc4	jeho
otec	otec	k1gMnSc1	otec
získal	získat	k5eAaPmAgMnS	získat
akcie	akcie	k1gFnPc4	akcie
chicagské	chicagský	k2eAgFnSc2d1	Chicagská
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
želé	želé	k1gNnSc4	želé
O-Zell	O-Zella	k1gFnPc2	O-Zella
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
opět	opět	k6eAd1	opět
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
prvního	první	k4xOgInSc2	první
ročníku	ročník	k1gInSc2	ročník
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
McKinley	McKinlea	k1gFnSc2	McKinlea
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
večerní	večerní	k2eAgInPc4d1	večerní
kurzy	kurz	k1gInPc4	kurz
Institutu	institut	k1gInSc2	institut
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
i	i	k9	i
karikaturistou	karikaturista	k1gMnSc7	karikaturista
školních	školní	k2eAgFnPc2d1	školní
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
kresby	kresba	k1gFnPc1	kresba
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zuřila	zuřit	k5eAaImAgFnS	zuřit
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
vlastenecké	vlastenecký	k2eAgFnPc1d1	vlastenecká
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
Walt	Walt	k1gInSc1	Walt
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
šestnácti	šestnáct	k4xCc6	šestnáct
letech	let	k1gInPc6	let
a	a	k8xC	a
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ho	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
pro	pro	k7c4	pro
nízký	nízký	k2eAgInSc4d1	nízký
věk	věk	k1gInSc4	věk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
odmítnutí	odmítnutí	k1gNnSc6	odmítnutí
armádou	armáda	k1gFnSc7	armáda
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
červenému	červený	k2eAgInSc3d1	červený
kříži	kříž	k1gInSc3	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
vyslán	vyslán	k2eAgInSc1d1	vyslán
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
až	až	k9	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
řídil	řídit	k5eAaImAgMnS	řídit
sanitku	sanitka	k1gFnSc4	sanitka
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
najde	najít	k5eAaPmIp3nS	najít
jinou	jiný	k2eAgFnSc4d1	jiná
práci	práce	k1gFnSc4	práce
než	než	k8xS	než
v	v	k7c6	v
chicagské	chicagský	k2eAgFnSc6d1	Chicagská
továrně	továrna	k1gFnSc6	továrna
O-Zell	O-Zell	k1gMnSc1	O-Zell
<g/>
,,	,,	k?	,,
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
domova	domov	k1gInSc2	domov
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc3	City
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
umělecká	umělecký	k2eAgFnSc1d1	umělecká
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
stát	stát	k5eAaImF	stát
hercem	herec	k1gMnSc7	herec
nebo	nebo	k8xC	nebo
ilustrátorem	ilustrátor	k1gMnSc7	ilustrátor
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
kariéru	kariéra	k1gFnSc4	kariéra
v	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
kreslit	kreslit	k5eAaImF	kreslit
politické	politický	k2eAgFnPc4d1	politická
karikatury	karikatura	k1gFnPc4	karikatura
nebo	nebo	k8xC	nebo
komiksy	komiks	k1gInPc4	komiks
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gNnSc4	on
však	však	k9	však
nikdo	nikdo	k3yNnSc1	nikdo
nechtěl	chtít	k5eNaImAgMnS	chtít
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
jako	jako	k9	jako
umělce	umělec	k1gMnPc4	umělec
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
jako	jako	k9	jako
řidiče	řidič	k1gMnSc4	řidič
sanitky	sanitka	k1gFnSc2	sanitka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Waltův	Waltův	k2eAgMnSc1d1	Waltův
bratr	bratr	k1gMnSc1	bratr
Roy	Roy	k1gMnSc1	Roy
<g/>
,	,	kIx,	,
pracující	pracující	k2eAgMnSc1d1	pracující
v	v	k7c6	v
bance	banka	k1gFnSc6	banka
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zajistí	zajistit	k5eAaPmIp3nS	zajistit
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Pesmen-Rubin	Pesmen-Rubina	k1gFnPc2	Pesmen-Rubina
Art	Art	k1gFnSc2	Art
Studio	studio	k1gNnSc1	studio
přes	přes	k7c4	přes
svého	svůj	k3xOyFgMnSc4	svůj
kolegu	kolega	k1gMnSc4	kolega
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Pesmen-Rubin	Pesmen-Rubin	k2eAgMnSc1d1	Pesmen-Rubin
Art	Art	k1gMnSc1	Art
Studio	studio	k1gNnSc4	studio
kreslil	kreslit	k5eAaImAgMnS	kreslit
reklamy	reklama	k1gFnPc4	reklama
pro	pro	k7c4	pro
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
magazíny	magazín	k1gInPc4	magazín
a	a	k8xC	a
filmová	filmový	k2eAgNnPc4d1	filmové
divadla	divadlo	k1gNnPc4	divadlo
(	(	kIx(	(
<g/>
předchůdce	předchůdce	k1gMnSc2	předchůdce
kin	kino	k1gNnPc2	kino
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
karikaturistou	karikaturista	k1gMnSc7	karikaturista
jménem	jméno	k1gNnSc7	jméno
Ub	Ub	k1gFnSc2	Ub
Iwerks	Iwerksa	k1gFnPc2	Iwerksa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jejich	jejich	k3xOp3gFnSc1	jejich
smlouva	smlouva	k1gFnSc1	smlouva
se	se	k3xPyFc4	se
studiem	studio	k1gNnSc7	studio
vypršela	vypršet	k5eAaPmAgFnS	vypršet
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1920	[number]	k4	1920
společně	společně	k6eAd1	společně
s	s	k7c7	s
Iwerksem	Iwerks	k1gMnSc7	Iwerks
založili	založit	k5eAaPmAgMnP	založit
společnost	společnost	k1gFnSc4	společnost
Iwerks-Disney	Iwerks-Disnea	k1gFnSc2	Iwerks-Disnea
Commercial	Commercial	k1gInSc1	Commercial
Artists	Artists	k1gInSc1	Artists
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
podnik	podnik	k1gInSc1	podnik
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vydělal	vydělat	k5eAaPmAgInS	vydělat
více	hodně	k6eAd2	hodně
peněz	peníze	k1gInPc2	peníze
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Kansas	Kansas	k1gInSc4	Kansas
City	city	k1gNnSc2	city
Film	film	k1gInSc1	film
Ad	ad	k7c4	ad
Company	Compan	k1gMnPc4	Compan
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
společnosti	společnost	k1gFnSc3	společnost
připojil	připojit	k5eAaPmAgInS	připojit
i	i	k9	i
Iwerks	Iwerks	k1gInSc1	Iwerks
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
sám	sám	k3xTgMnSc1	sám
řízení	řízení	k1gNnSc6	řízení
podniku	podnik	k1gInSc3	podnik
nezvládl	zvládnout	k5eNaPmAgInS	zvládnout
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
Film	film	k1gInSc1	film
Ad	ad	k7c4	ad
Company	Compana	k1gFnPc4	Compana
Disney	Disnea	k1gFnSc2	Disnea
kreslil	kreslit	k5eAaImAgMnS	kreslit
reklamy	reklama	k1gFnPc4	reklama
pomocí	pomocí	k7c2	pomocí
techniky	technika	k1gFnSc2	technika
výstřižkové	výstřižkový	k2eAgFnSc2d1	výstřižková
animace	animace	k1gFnSc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zajímat	zajímat	k5eAaImF	zajímat
o	o	k7c4	o
animaci	animace	k1gFnSc4	animace
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
animátorem	animátor	k1gMnSc7	animátor
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
vlastníka	vlastník	k1gMnSc2	vlastník
společnosti	společnost	k1gFnSc2	společnost
Ad	ad	k7c4	ad
Company	Compana	k1gFnPc4	Compana
<g/>
,	,	kIx,	,
A.	A.	kA	A.
V.	V.	kA	V.
Caugera	Caugero	k1gNnPc4	Caugero
<g/>
,	,	kIx,	,
o	o	k7c6	o
zapůjčení	zapůjčení	k1gNnSc6	zapůjčení
firemní	firemní	k2eAgFnSc2d1	firemní
kamery	kamera	k1gFnSc2	kamera
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
mohl	moct	k5eAaImAgMnS	moct
experimentovat	experimentovat	k5eAaImF	experimentovat
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
přečtení	přečtení	k1gNnSc6	přečtení
knihy	kniha	k1gFnSc2	kniha
Animated	Animated	k1gMnSc1	Animated
Cartoons	Cartoons	k1gInSc1	Cartoons
<g/>
:	:	kIx,	:
How	How	k1gFnSc1	How
They	Thea	k1gFnSc2	Thea
Are	ar	k1gInSc5	ar
Made	Made	k1gFnSc5	Made
<g/>
,	,	kIx,	,
Their	Their	k1gMnSc1	Their
Origin	Origin	k1gMnSc1	Origin
and	and	k?	and
Development	Development	k1gMnSc1	Development
od	od	k7c2	od
Edwina	Edwino	k1gNnSc2	Edwino
G.	G.	kA	G.
Lutze	Lutze	k1gFnSc2	Lutze
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
názoru	názor	k1gInSc3	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
animace	animace	k1gFnSc1	animace
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
lepší	dobrý	k2eAgFnSc1d2	lepší
budoucnost	budoucnost	k1gFnSc1	budoucnost
než	než	k8xS	než
výstřižková	výstřižkový	k2eAgFnSc1d1	výstřižková
animace	animace	k1gFnSc1	animace
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dělal	dělat	k5eAaImAgMnS	dělat
pro	pro	k7c4	pro
Caugera	Cauger	k1gMnSc4	Cauger
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
založit	založit	k5eAaPmF	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
animační	animační	k2eAgFnSc4d1	animační
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
bývalého	bývalý	k2eAgMnSc4d1	bývalý
spolupracovníka	spolupracovník	k1gMnSc4	spolupracovník
z	z	k7c2	z
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc3	City
Film	film	k1gInSc1	film
Ad	ad	k7c4	ad
Company	Compan	k1gMnPc4	Compan
<g/>
,	,	kIx,	,
Freda	Fred	k1gMnSc4	Fred
Harmana	Harman	k1gMnSc4	Harman
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
prvního	první	k4xOgMnSc2	první
zaměstnance	zaměstnanec	k1gMnSc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gMnSc1	Walt
a	a	k8xC	a
Harman	Harman	k1gMnSc1	Harman
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Frankem	Frank	k1gMnSc7	Frank
L.	L.	kA	L.
Newmanem	Newman	k1gMnSc7	Newman
<g/>
,	,	kIx,	,
majitelem	majitel	k1gMnSc7	majitel
místního	místní	k2eAgNnSc2d1	místní
kina	kino	k1gNnSc2	kino
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
nejproslulejším	proslulý	k2eAgMnSc7d3	nejproslulejší
zábavním	zábavní	k2eAgMnSc7d1	zábavní
podnikatelem	podnikatel	k1gMnSc7	podnikatel
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
promítat	promítat	k5eAaImF	promítat
své	svůj	k3xOyFgInPc4	svůj
kreslené	kreslený	k2eAgInPc4d1	kreslený
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nazvali	nazvat	k5eAaBmAgMnP	nazvat
Laugh-O-Grams	Laugh-O-Grams	k1gInSc4	Laugh-O-Grams
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
kině	kino	k1gNnSc6	kino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
podnikání	podnikání	k1gNnSc2	podnikání
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Newman	Newman	k1gMnSc1	Newman
Laugh-O-Grams	Laugh-O-Grams	k1gInSc1	Laugh-O-Grams
získaly	získat	k5eAaPmAgFnP	získat
jeho	jeho	k3xOp3gFnPc1	jeho
projekce	projekce	k1gFnPc1	projekce
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
velkou	velký	k2eAgFnSc4d1	velká
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
finančnímu	finanční	k2eAgInSc3d1	finanční
úspěchu	úspěch	k1gInSc3	úspěch
si	se	k3xPyFc3	se
Disney	Disney	k1gInPc4	Disney
mohl	moct	k5eAaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
založit	založit	k5eAaPmF	založit
vlastní	vlastní	k2eAgNnSc4d1	vlastní
studio	studio	k1gNnSc4	studio
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgInPc4d1	pojmenovaný
Laugh-O-Gram	Laugh-O-Gram	k1gInSc4	Laugh-O-Gram
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
a	a	k8xC	a
najmout	najmout	k5eAaPmF	najmout
další	další	k2eAgMnPc4d1	další
animátory	animátor	k1gMnPc4	animátor
včetně	včetně	k7c2	včetně
bratra	bratr	k1gMnSc2	bratr
Freda	Fred	k1gMnSc2	Fred
Harmana	Harman	k1gMnSc2	Harman
Hugha	Hugh	k1gMnSc2	Hugh
<g/>
,	,	kIx,	,
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Isigna	Isign	k1gMnSc2	Isign
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
blízkého	blízký	k2eAgMnSc2d1	blízký
přítele	přítel	k1gMnSc2	přítel
Ubbeho	Ubbe	k1gMnSc2	Ubbe
Iwerkse	Iwerks	k1gMnSc2	Iwerks
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tím	ten	k3xDgNnSc7	ten
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
výdaje	výdaj	k1gInPc4	výdaj
na	na	k7c4	na
platy	plat	k1gInPc4	plat
a	a	k8xC	a
Disneyovi	Disneya	k1gMnSc3	Disneya
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zvládnout	zvládnout	k5eAaPmF	zvládnout
financování	financování	k1gNnSc4	financování
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
se	se	k3xPyFc4	se
předlužilo	předlužit	k5eAaPmAgNnS	předlužit
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
zbankrotovalo	zbankrotovat	k5eAaPmAgNnS	zbankrotovat
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zkrachování	zkrachování	k1gNnSc4	zkrachování
dalšího	další	k2eAgInSc2d1	další
pokusu	pokus	k1gInSc2	pokus
o	o	k7c4	o
vlastní	vlastní	k2eAgNnSc4d1	vlastní
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Disney	Disnea	k1gFnSc2	Disnea
odhodlal	odhodlat	k5eAaPmAgMnS	odhodlat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pokusí	pokusit	k5eAaPmIp3nS	pokusit
prorazit	prorazit	k5eAaPmF	prorazit
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
silně	silně	k6eAd1	silně
konkurenčním	konkurenční	k2eAgInSc6d1	konkurenční
filmovém	filmový	k2eAgInSc6d1	filmový
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
<g/>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Royem	Roy	k1gMnSc7	Roy
našetřil	našetřit	k5eAaPmAgMnS	našetřit
dostatek	dostatek	k1gInSc4	dostatek
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
vybudovat	vybudovat	k5eAaPmF	vybudovat
nové	nový	k2eAgNnSc4d1	nové
animační	animační	k2eAgNnSc4d1	animační
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
však	však	k9	však
najít	najít	k5eAaPmF	najít
distributora	distributor	k1gMnSc4	distributor
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
animovaný	animovaný	k2eAgInSc4d1	animovaný
seriál	seriál	k1gInSc4	seriál
Alice	Alice	k1gFnSc1	Alice
Comedies	Comedies	k1gInSc1	Comedies
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
začali	začít	k5eAaPmAgMnP	začít
pracovat	pracovat	k5eAaImF	pracovat
již	již	k6eAd1	již
v	v	k7c4	v
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
ho	on	k3xPp3gMnSc4	on
neuveřejnili	uveřejnit	k5eNaPmAgMnP	uveřejnit
<g/>
.	.	kIx.	.
</s>
<s>
Poslali	poslat	k5eAaPmAgMnP	poslat
tedy	tedy	k9	tedy
nedokončenou	dokončený	k2eNgFnSc4d1	nedokončená
ukázku	ukázka	k1gFnSc4	ukázka
newyorské	newyorský	k2eAgFnSc3d1	newyorská
distributorce	distributorka	k1gFnSc3	distributorka
Margaret	Margareta	k1gFnPc2	Margareta
J.	J.	kA	J.
Winklerové	Winklerová	k1gFnSc2	Winklerová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jim	on	k3xPp3gMnPc3	on
okamžitě	okamžitě	k6eAd1	okamžitě
odepsala	odepsat	k5eAaPmAgFnS	odepsat
<g/>
.	.	kIx.	.
</s>
<s>
Winklerová	Winklerová	k1gFnSc1	Winklerová
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
dohody	dohoda	k1gFnSc2	dohoda
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
velmi	velmi	k6eAd1	velmi
nadšena	nadchnout	k5eAaPmNgFnS	nadchnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
krátké	krátký	k2eAgFnSc2d1	krátká
animace	animace	k1gFnSc2	animace
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
Alence	Alenka	k1gFnSc6	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
ji	on	k3xPp3gFnSc4	on
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
<g/>
.	.	kIx.	.
<g/>
Virginia	Virginium	k1gNnPc4	Virginium
Davisová	Davisový	k2eAgNnPc4d1	Davisové
<g/>
,	,	kIx,	,
představitelka	představitelka	k1gFnSc1	představitelka
Alenky	Alenka	k1gFnSc2	Alenka
v	v	k7c6	v
živě	živě	k6eAd1	živě
točených	točený	k2eAgFnPc6d1	točená
scénách	scéna	k1gFnPc6	scéna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gFnSc2	jeho
žádosti	žádost	k1gFnSc2	žádost
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
z	z	k7c2	z
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc4	City
do	do	k7c2	do
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
Iwerks	Iwerks	k1gInSc1	Iwerks
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
začátek	začátek	k1gInSc1	začátek
studia	studio	k1gNnSc2	studio
Disney	Disnea	k1gFnSc2	Disnea
Brothers	Brothers	k1gInSc1	Brothers
<g/>
'	'	kIx"	'
Studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
<s>
Sídlilo	sídlit	k5eAaImAgNnS	sídlit
na	na	k7c4	na
Hyperion	Hyperion	k1gInSc4	Hyperion
Avenue	avenue	k1gFnSc2	avenue
v	v	k7c6	v
losangeleské	losangeleský	k2eAgFnSc6d1	losangeleská
části	část	k1gFnSc6	část
Silver	Silvra	k1gFnPc2	Silvra
Lake	Lak	k1gFnSc2	Lak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
najal	najmout	k5eAaPmAgMnS	najmout
mladou	mladý	k2eAgFnSc4d1	mladá
slečnu	slečna	k1gFnSc4	slečna
jménem	jméno	k1gNnSc7	jméno
Lillian	Lillian	k1gInSc1	Lillian
Boundsová	Boundsový	k2eAgFnSc1d1	Boundsový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kreslila	kreslit	k5eAaImAgFnS	kreslit
inkoustem	inkoust	k1gInSc7	inkoust
na	na	k7c4	na
celuloid	celuloid	k1gInSc4	celuloid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
známosti	známost	k1gFnSc6	známost
se	se	k3xPyFc4	se
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
seriál	seriál	k1gInSc1	seriál
Alice	Alice	k1gFnSc2	Alice
Comedies	Comedies	k1gMnSc1	Comedies
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
vystřídaly	vystřídat	k5eAaPmAgInP	vystřídat
Dawn	Dawn	k1gInSc4	Dawn
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Dayová	Dayová	k1gFnSc1	Dayová
a	a	k8xC	a
Margie	Margie	k1gFnSc1	Margie
Gayová	Gayová	k1gFnSc1	Gayová
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
i	i	k9	i
Lois	Lois	k1gInSc4	Lois
Hardwicková	Hardwicková	k1gFnSc1	Hardwicková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
seriálu	seriál	k1gInSc2	seriál
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
se	se	k3xPyFc4	se
autoři	autor	k1gMnPc1	autor
zaměřili	zaměřit	k5eAaPmAgMnP	zaměřit
více	hodně	k6eAd2	hodně
na	na	k7c4	na
animované	animovaný	k2eAgFnPc4d1	animovaná
postavičky	postavička	k1gFnPc4	postavička
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
kocoura	kocour	k1gMnSc4	kocour
Julia	Julius	k1gMnSc4	Julius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
připomínal	připomínat	k5eAaImAgMnS	připomínat
kocoura	kocour	k1gMnSc4	kocour
Felixe	Felix	k1gMnSc4	Felix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
si	se	k3xPyFc3	se
Charles	Charles	k1gMnSc1	Charles
Mintz	Mintz	k1gMnSc1	Mintz
vzal	vzít	k5eAaPmAgMnS	vzít
Margaret	Margareta	k1gFnPc2	Margareta
Winklerovou	Winklerová	k1gFnSc4	Winklerová
a	a	k8xC	a
převzal	převzít	k5eAaPmAgMnS	převzít
vedení	vedení	k1gNnSc4	vedení
jejího	její	k3xOp3gInSc2	její
obchodu	obchod	k1gInSc2	obchod
včetně	včetně	k7c2	včetně
objednávek	objednávka	k1gFnPc2	objednávka
animovaných	animovaný	k2eAgInPc2d1	animovaný
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
distribuovány	distribuovat	k5eAaBmNgInP	distribuovat
společností	společnost	k1gFnSc7	společnost
Universal	Universal	k1gFnSc2	Universal
Pictures	Picturesa	k1gFnPc2	Picturesa
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
seriál	seriál	k1gInSc1	seriál
Králík	Králík	k1gMnSc1	Králík
Oswald	Oswald	k1gMnSc1	Oswald
(	(	kIx(	(
<g/>
Oswald	Oswald	k1gMnSc1	Oswald
the	the	k?	the
Lucky	Lucka	k1gFnSc2	Lucka
Rabbit	Rabbit	k1gFnSc2	Rabbit
<g/>
)	)	kIx)	)
okamžitě	okamžitě	k6eAd1	okamžitě
uspěl	uspět	k5eAaPmAgMnS	uspět
a	a	k8xC	a
Oswald	Oswald	k1gMnSc1	Oswald
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
i	i	k8xC	i
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Iwerks	Iwerks	k1gInSc4	Iwerks
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
populární	populární	k2eAgFnSc7d1	populární
animovanou	animovaný	k2eAgFnSc7d1	animovaná
postavičkou	postavička	k1gFnSc7	postavička
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
bratří	bratr	k1gMnPc2	bratr
Disneyů	Disney	k1gInPc2	Disney
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
si	se	k3xPyFc3	se
mohli	moct	k5eAaImAgMnP	moct
najmout	najmout	k5eAaPmF	najmout
opět	opět	k6eAd1	opět
Harmana	Harman	k1gMnSc4	Harman
<g/>
,	,	kIx,	,
Isinga	Ising	k1gMnSc4	Ising
<g/>
,	,	kIx,	,
Carmana	Carman	k1gMnSc2	Carman
Maxwella	Maxwell	k1gMnSc2	Maxwell
a	a	k8xC	a
Frize	Frize	k1gFnSc1	Frize
Frelenga	Frelenga	k1gFnSc1	Frelenga
z	z	k7c2	z
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc4	City
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1928	[number]	k4	1928
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
za	za	k7c7	za
Mintzem	Mintz	k1gInSc7	Mintz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sjednal	sjednat	k5eAaPmAgMnS	sjednat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
však	však	k9	však
šokován	šokovat	k5eAaBmNgMnS	šokovat
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Mintz	Mintz	k1gInSc1	Mintz
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
naopak	naopak	k6eAd1	naopak
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
snímek	snímek	k1gInSc4	snímek
snížit	snížit	k5eAaPmF	snížit
a	a	k8xC	a
že	že	k8xS	že
si	se	k3xPyFc3	se
smluvně	smluvně	k6eAd1	smluvně
zajistil	zajistit	k5eAaPmAgMnS	zajistit
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gMnPc2	jeho
hlavních	hlavní	k2eAgMnPc2d1	hlavní
animátorů	animátor	k1gMnPc2	animátor
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Harmana	Harman	k1gMnSc2	Harman
<g/>
,	,	kIx,	,
Isigna	Isign	k1gMnSc2	Isign
<g/>
,	,	kIx,	,
Maxwella	Maxwell	k1gMnSc2	Maxwell
a	a	k8xC	a
Frelenga	Freleng	k1gMnSc2	Freleng
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Iwerkse	Iwerks	k1gMnSc2	Iwerks
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
opustit	opustit	k5eAaPmF	opustit
Disneye	Disneye	k1gFnSc4	Disneye
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
Disney	Disne	k1gMnPc4	Disne
nepřijme	přijmout	k5eNaPmIp3nS	přijmout
jeho	jeho	k3xOp3gInSc1	jeho
plán	plán	k1gInSc1	plán
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
rozpočtů	rozpočet	k1gInPc2	rozpočet
<g/>
,	,	kIx,	,
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Universal	Universal	k1gFnSc1	Universal
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
Oswalda	Oswaldo	k1gNnPc4	Oswaldo
<g/>
,	,	kIx,	,
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
proto	proto	k8xC	proto
točit	točit	k5eAaImF	točit
filmy	film	k1gInPc4	film
i	i	k9	i
bez	bez	k7c2	bez
Disneye	Disney	k1gFnSc2	Disney
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
Mintzovou	Mintzová	k1gFnSc4	Mintzová
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
ztratil	ztratit	k5eAaPmAgMnS	ztratit
tak	tak	k9	tak
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgMnPc2	svůj
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
citelnou	citelný	k2eAgFnSc4d1	citelná
ztrátu	ztráta	k1gFnSc4	ztráta
kvalitních	kvalitní	k2eAgMnPc2d1	kvalitní
animátorů	animátor	k1gMnPc2	animátor
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k2eAgFnPc4d1	Disne
Company	Compana	k1gFnPc4	Compana
zabralo	zabrat	k5eAaPmAgNnS	zabrat
celých	celý	k2eAgNnPc2d1	celé
78	[number]	k4	78
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
získala	získat	k5eAaPmAgFnS	získat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
Oswalda	Oswaldo	k1gNnPc4	Oswaldo
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
práva	právo	k1gNnSc2	právo
získala	získat	k5eAaPmAgFnS	získat
od	od	k7c2	od
NBC	NBC	kA	NBC
Universal	Universal	k1gFnSc2	Universal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
komentátora	komentátor	k1gMnSc4	komentátor
ABC	ABC	kA	ABC
sports	sportsa	k1gFnPc2	sportsa
Ala	ala	k1gFnSc1	ala
Michaelse	Michaelse	k1gFnSc1	Michaelse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mickey	Micke	k2eAgMnPc4d1	Micke
Mouse	Mous	k1gMnPc4	Mous
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
ztratil	ztratit	k5eAaPmAgInS	ztratit
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
Oswalda	Oswaldo	k1gNnPc4	Oswaldo
<g/>
,	,	kIx,	,
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
vymyslet	vymyslet	k5eAaPmF	vymyslet
novou	nový	k2eAgFnSc4d1	nová
postavičku	postavička	k1gFnSc4	postavička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
ho	on	k3xPp3gInSc4	on
nahradila	nahradit	k5eAaPmAgFnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Napadla	napadnout	k5eAaPmAgFnS	napadnout
ho	on	k3xPp3gNnSc4	on
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jednu	jeden	k4xCgFnSc4	jeden
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
jako	jako	k8xS	jako
domácího	domácí	k2eAgMnSc4d1	domácí
mazlíčka	mazlíček	k1gMnSc4	mazlíček
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
Kansas	Kansas	k1gInSc4	Kansas
City	City	k1gFnSc2	City
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Ub	Ub	k?	Ub
Iwerks	Iwerks	k1gInSc1	Iwerks
přepracoval	přepracovat	k5eAaPmAgInS	přepracovat
Waltovy	Waltův	k2eAgInPc4d1	Waltův
náčrty	náčrt	k1gInPc4	náčrt
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
lépe	dobře	k6eAd2	dobře
animovatelné	animovatelný	k2eAgFnPc1d1	animovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
i	i	k9	i
osobnost	osobnost	k1gFnSc4	osobnost
Mickeyeho	Mickeye	k1gMnSc2	Mickeye
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
samotný	samotný	k2eAgMnSc1d1	samotný
Disney	Disney	k1gInPc4	Disney
<g/>
.	.	kIx.	.
</s>
<s>
Slovy	slovo	k1gNnPc7	slovo
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
studia	studio	k1gNnSc2	studio
"	"	kIx"	"
<g/>
Ub	Ub	k1gMnSc1	Ub
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
podobu	podoba	k1gFnSc4	podoba
Mickeyeho	Mickeye	k1gMnSc2	Mickeye
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Walt	Walt	k1gMnSc1	Walt
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Myš	myš	k1gFnSc1	myš
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
měla	mít	k5eAaImAgFnS	mít
jmenovat	jmenovat	k5eAaImF	jmenovat
Mortimer	Mortimer	k1gInSc4	Mortimer
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
Lillian	Lillian	k1gInSc4	Lillian
Disneyová	Disneyová	k1gFnSc1	Disneyová
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Mickey	Micke	k2eAgFnPc4d1	Micke
Mouse	Mouse	k1gFnPc4	Mouse
(	(	kIx(	(
<g/>
Myšák	myšák	k1gMnSc1	myšák
Mickey	Mickea	k1gFnSc2	Mickea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
se	se	k3xPyFc4	se
k	k	k7c3	k
myšákovi	myšák	k1gMnSc3	myšák
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
nehodilo	hodit	k5eNaPmAgNnS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Mortimer	Mortimer	k1gInSc1	Mortimer
s	s	k7c7	s
brooklynským	brooklynský	k2eAgInSc7d1	brooklynský
přízvukem	přízvuk	k1gInSc7	přízvuk
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgInS	stát
hlavní	hlavní	k2eAgFnSc2d1	hlavní
rivalem	rival	k1gMnSc7	rival
Mickeyeho	Mickeye	k1gMnSc2	Mickeye
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
Minnie	Minnie	k1gFnPc4	Minnie
Mouse	Mouse	k1gFnSc2	Mouse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
s	s	k7c7	s
Mickeyem	Mickey	k1gMnSc7	Mickey
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Plane	planout	k5eAaImIp3nS	planout
Crazy	Craz	k1gInPc4	Craz
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
předešlé	předešlý	k2eAgInPc4d1	předešlý
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěšném	úspěšný	k2eNgNnSc6d1	neúspěšné
hledání	hledání	k1gNnSc6	hledání
distributora	distributor	k1gMnSc2	distributor
pro	pro	k7c4	pro
Plane	planout	k5eAaImIp3nS	planout
Crazy	Craz	k1gInPc4	Craz
a	a	k8xC	a
následující	následující	k2eAgInSc4d1	následující
druhý	druhý	k4xOgInSc4	druhý
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Gallopin	Gallopin	k1gMnSc1	Gallopin
<g/>
'	'	kIx"	'
Gaucho	Gaucha	k1gMnSc5	Gaucha
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Walt	Walt	k1gMnSc1	Walt
animaci	animace	k1gFnSc4	animace
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
Parník	parník	k1gInSc4	parník
Willie	Willie	k1gFnSc1	Willie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
se	s	k7c7	s
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Nabídky	nabídka	k1gFnPc4	nabídka
se	se	k3xPyFc4	se
chytil	chytit	k5eAaPmAgInS	chytit
podnikatel	podnikatel	k1gMnSc1	podnikatel
Pat	pata	k1gFnPc2	pata
Powers	Powers	k1gInSc1	Powers
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
distribuci	distribuce	k1gFnSc4	distribuce
i	i	k9	i
Cinephone	Cinephon	k1gInSc5	Cinephon
<g/>
,	,	kIx,	,
proces	proces	k1gInSc4	proces
synchronizace	synchronizace	k1gFnSc2	synchronizace
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Parník	parník	k1gInSc1	parník
Willie	Willie	k1gFnSc2	Willie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
okamžitě	okamžitě	k6eAd1	okamžitě
populárním	populární	k2eAgMnSc6d1	populární
a	a	k8xC	a
Plane	planout	k5eAaImIp3nS	planout
Crazy	Craz	k1gInPc4	Craz
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Galloping	Galloping	k1gInSc1	Galloping
Gaucho	Gaucha	k1gFnSc5	Gaucha
i	i	k9	i
další	další	k2eAgFnPc4d1	další
pozdější	pozdní	k2eAgFnPc4d2	pozdější
animace	animace	k1gFnPc4	animace
s	s	k7c7	s
Mickeyem	Mickey	k1gInSc7	Mickey
už	už	k6eAd1	už
vycházely	vycházet	k5eAaImAgFnP	vycházet
jedině	jedině	k6eAd1	jedině
se	s	k7c7	s
zvukovým	zvukový	k2eAgInSc7d1	zvukový
doprovodem	doprovod	k1gInSc7	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Mickey	Micke	k2eAgFnPc1d1	Micke
Mouse	Mouse	k1gFnPc1	Mouse
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
daboval	dabovat	k5eAaBmAgInS	dabovat
samotný	samotný	k2eAgInSc1d1	samotný
Disney	Disneum	k1gNnPc7	Disneum
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
animovananá	animovananý	k2eAgFnSc1d1	animovananý
osobnost	osobnost	k1gFnSc1	osobnost
brzy	brzy	k6eAd1	brzy
zastínila	zastínit	k5eAaPmAgFnS	zastínit
i	i	k9	i
kocoura	kocour	k1gMnSc4	kocour
Felixe	Felix	k1gMnSc4	Felix
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
světově	světově	k6eAd1	světově
nejpopulárnější	populární	k2eAgFnSc7d3	nejpopulárnější
postavičkou	postavička	k1gFnSc7	postavička
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
popularita	popularita	k1gFnSc1	popularita
rapidně	rapidně	k6eAd1	rapidně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
především	především	k6eAd1	především
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgNnPc4	první
ocenění	ocenění	k1gNnPc4	ocenění
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
obdržel	obdržet	k5eAaPmAgMnS	obdržet
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
seriál	seriál	k1gInSc4	seriál
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
převeden	převést	k5eAaPmNgInS	převést
z	z	k7c2	z
černobílého	černobílý	k2eAgInSc2d1	černobílý
na	na	k7c4	na
barevný	barevný	k2eAgInSc4d1	barevný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
později	pozdě	k6eAd2	pozdě
přibyly	přibýt	k5eAaPmAgFnP	přibýt
i	i	k9	i
postavy	postava	k1gFnPc1	postava
jako	jako	k8xS	jako
Kačer	kačer	k1gMnSc1	kačer
Donald	Donald	k1gMnSc1	Donald
<g/>
,	,	kIx,	,
Goofy	Goof	k1gInPc1	Goof
nebo	nebo	k8xC	nebo
Pluto	Pluto	k1gNnSc1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Pluto	Pluto	k1gMnSc1	Pluto
a	a	k8xC	a
Donald	Donald	k1gMnSc1	Donald
téměř	téměř	k6eAd1	téměř
okamžitě	okamžitě	k6eAd1	okamžitě
odstartovali	odstartovat	k5eAaPmAgMnP	odstartovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
seriál	seriál	k1gInSc4	seriál
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
svůj	svůj	k3xOyFgInSc4	svůj
seriál	seriál	k1gInSc4	seriál
získal	získat	k5eAaPmAgMnS	získat
i	i	k8xC	i
Goofy	Goof	k1gInPc4	Goof
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgNnSc1	první
těhotenství	těhotenství	k1gNnSc1	těhotenství
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rodině	rodina	k1gFnSc6	rodina
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
Lillian	Lilliana	k1gFnPc2	Lilliana
potratila	potratit	k5eAaPmAgFnS	potratit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
podruhé	podruhé	k6eAd1	podruhé
<g/>
,	,	kIx,	,
narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1933	[number]	k4	1933
dcera	dcera	k1gFnSc1	dcera
Diane	Dian	k1gInSc5	Dian
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
adoptovali	adoptovat	k5eAaPmAgMnP	adoptovat
Sharon	Sharon	k1gInSc4	Sharon
Mae	Mae	k1gFnSc2	Mae
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
:	:	kIx,	:
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
animace	animace	k1gFnSc2	animace
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dvě	dva	k4xCgFnPc4	dva
animované	animovaný	k2eAgFnPc4d1	animovaná
série	série	k1gFnPc4	série
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
naplánovat	naplánovat	k5eAaBmF	naplánovat
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nový	nový	k2eAgInSc1d1	nový
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
Pepek	Pepek	k1gMnSc1	Pepek
námořník	námořník	k1gMnSc1	námořník
Maxe	Max	k1gMnSc2	Max
Fleischera	Fleischer	k1gMnSc2	Fleischer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
stal	stát	k5eAaPmAgMnS	stát
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
populárnějším	populární	k2eAgMnPc3d2	populárnější
než	než	k8xS	než
Mickey	Micke	k2eAgMnPc4d1	Micke
Mouse	Mous	k1gMnPc4	Mous
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
však	však	k9	však
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Mickeyeho	Mickeye	k1gMnSc4	Mickeye
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
popularity	popularita	k1gFnSc2	popularita
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
animátor	animátor	k1gMnSc1	animátor
Fred	Fred	k1gMnSc1	Fred
Moore	Moor	k1gInSc5	Moor
vybarvil	vybarvit	k5eAaPmAgMnS	vybarvit
a	a	k8xC	a
upravil	upravit	k5eAaPmAgMnS	upravit
jeho	jeho	k3xOp3gInSc4	jeho
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
studia	studio	k1gNnSc2	studio
dozvědělo	dozvědět	k5eAaPmAgNnS	dozvědět
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
plánu	plán	k1gInSc6	plán
natočit	natočit	k5eAaBmF	natočit
animovaný	animovaný	k2eAgInSc4d1	animovaný
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
o	o	k7c6	o
Sněhurce	Sněhurka	k1gFnSc6	Sněhurka
<g/>
,	,	kIx,	,
označilo	označit	k5eAaPmAgNnS	označit
projekt	projekt	k1gInSc4	projekt
za	za	k7c7	za
"	"	kIx"	"
<g/>
Disneyeho	Disneye	k1gMnSc2	Disneye
hloupost	hloupost	k1gFnSc1	hloupost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zničí	zničit	k5eAaPmIp3nS	zničit
jeho	jeho	k3xOp3gFnSc4	jeho
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Lillian	Lillian	k1gMnSc1	Lillian
i	i	k8xC	i
bratr	bratr	k1gMnSc1	bratr
Roy	Roy	k1gMnSc1	Roy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
snažili	snažit	k5eAaImAgMnP	snažit
vymluvit	vymluvit	k5eAaPmF	vymluvit
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
však	však	k9	však
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
plánování	plánování	k1gNnSc6	plánování
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnal	zaměstnat	k5eAaPmAgMnS	zaměstnat
profesora	profesor	k1gMnSc4	profesor
Dona	Don	k1gMnSc4	Don
Grahama	Graham	k1gMnSc4	Graham
z	z	k7c2	z
Chouinard	Chouinarda	k1gFnPc2	Chouinarda
Art	Art	k1gFnSc2	Art
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
s	s	k7c7	s
tréninkem	trénink	k1gInSc7	trénink
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
také	také	k9	také
s	s	k7c7	s
animací	animace	k1gFnSc7	animace
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgInS	využít
speciální	speciální	k2eAgInPc4d1	speciální
efekty	efekt	k1gInPc4	efekt
a	a	k8xC	a
nové	nový	k2eAgFnPc4d1	nová
techniky	technika	k1gFnPc4	technika
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
víceplánová	víceplánový	k2eAgFnSc1d1	víceplánový
kamera	kamera	k1gFnSc1	kamera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
zkvalitnění	zkvalitnění	k1gNnSc3	zkvalitnění
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
schopným	schopný	k2eAgInPc3d1	schopný
vyprodukovat	vyprodukovat	k5eAaPmF	vyprodukovat
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
později	pozdě	k6eAd2	pozdě
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
natáčela	natáčet	k5eAaImAgFnS	natáčet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došly	dojít	k5eAaPmAgInP	dojít
studiu	studio	k1gNnSc6	studio
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
dokončit	dokončit	k5eAaPmF	dokončit
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
ukázal	ukázat	k5eAaPmAgMnS	ukázat
část	část	k1gFnSc4	část
Sněhurky	Sněhurka	k1gFnSc2	Sněhurka
úředníkům	úředník	k1gMnPc3	úředník
banky	banka	k1gFnPc1	banka
Bank	bank	k1gInSc1	bank
of	of	k?	of
America	Americ	k1gInSc2	Americ
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
úvěr	úvěr	k1gInSc4	úvěr
na	na	k7c4	na
dokončení	dokončení	k1gNnSc4	dokončení
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
Carthay	Carthaa	k1gMnSc2	Carthaa
Circle	Circl	k1gMnSc2	Circl
Theater	Theater	k1gMnSc1	Theater
a	a	k8xC	a
diváci	divák	k1gMnPc1	divák
tleskali	tleskat	k5eAaImAgMnP	tleskat
ve	v	k7c4	v
stoje	stoj	k1gInPc4	stoj
<g/>
.	.	kIx.	.
</s>
<s>
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prvním	první	k4xOgInSc7	první
animovaným	animovaný	k2eAgInSc7d1	animovaný
celovečerním	celovečerní	k2eAgInSc7d1	celovečerní
filmem	film	k1gInSc7	film
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
použila	použít	k5eAaPmAgFnS	použít
techniku	technika	k1gFnSc4	technika
značky	značka	k1gFnSc2	značka
Technicolor	Technicolora	k1gFnPc2	Technicolora
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
a	a	k8xC	a
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
verzi	verze	k1gFnSc6	verze
vydělal	vydělat	k5eAaPmAgMnS	vydělat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
(	(	kIx(	(
<g/>
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
výjimečné	výjimečný	k2eAgNnSc4d1	výjimečné
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
:	:	kIx,	:
osm	osm	k4xCc4	osm
oscarových	oscarový	k2eAgFnPc2d1	oscarová
sošek	soška	k1gFnPc2	soška
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jednu	jeden	k4xCgFnSc4	jeden
v	v	k7c4	v
plné	plný	k2eAgFnPc4d1	plná
velikosti	velikost	k1gFnPc4	velikost
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
malých	malý	k2eAgFnPc2d1	malá
<g/>
,	,	kIx,	,
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
dovolila	dovolit	k5eAaPmAgFnS	dovolit
vybudovat	vybudovat	k5eAaPmF	vybudovat
nový	nový	k2eAgInSc4d1	nový
kampus	kampus	k1gInSc4	kampus
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Studios	Studios	k?	Studios
v	v	k7c4	v
Burbanku	Burbanka	k1gFnSc4	Burbanka
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
nebyl	být	k5eNaImAgInS	být
jen	jen	k9	jen
jeho	jeho	k3xOp3gInSc7	jeho
vlastním	vlastní	k2eAgInSc7d1	vlastní
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uvedl	uvést	k5eAaPmAgMnS	uvést
jeho	jeho	k3xOp3gNnSc4	jeho
studio	studio	k1gNnSc4	studio
do	do	k7c2	do
éry	éra	k1gFnSc2	éra
později	pozdě	k6eAd2	pozdě
označované	označovaný	k2eAgFnPc4d1	označovaná
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
animace	animace	k1gFnSc2	animace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
filmy	film	k1gInPc1	film
jako	jako	k8xC	jako
Pinocchio	Pinocchio	k1gNnSc1	Pinocchio
<g/>
,	,	kIx,	,
Fantasia	Fantasia	k1gFnSc1	Fantasia
<g/>
,	,	kIx,	,
Bambi	Bambi	k1gNnSc1	Bambi
<g/>
,	,	kIx,	,
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
a	a	k8xC	a
Petr	Petr	k1gMnSc1	Petr
Pan	Pan	k1gMnSc1	Pan
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
také	také	k9	také
vznikaly	vznikat	k5eAaImAgInP	vznikat
nové	nový	k2eAgInPc1d1	nový
díly	díl	k1gInPc1	díl
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
<g/>
,	,	kIx,	,
Kačera	kačer	k1gMnSc2	kačer
Donalda	Donald	k1gMnSc2	Donald
<g/>
,	,	kIx,	,
Goofyho	Goofy	k1gMnSc2	Goofy
a	a	k8xC	a
Pluta	Pluto	k1gMnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
:	:	kIx,	:
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
animátorů	animátor	k1gMnPc2	animátor
vyslán	vyslat	k5eAaPmNgInS	vyslat
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
oficiální	oficiální	k2eAgFnSc2d1	oficiální
politiky	politika	k1gFnSc2	politika
dobrého	dobrý	k2eAgNnSc2d1	dobré
sousedství	sousedství	k1gNnSc2	sousedství
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
zároveň	zároveň	k6eAd1	zároveň
zajistila	zajistit	k5eAaPmAgFnS	zajistit
financování	financování	k1gNnSc3	financování
výsledného	výsledný	k2eAgInSc2d1	výsledný
filmu	film	k1gInSc2	film
Saludos	Saludos	k1gMnSc1	Saludos
amigos	amigos	k1gMnSc1	amigos
<g/>
.	.	kIx.	.
<g/>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
filmu	film	k1gInSc2	film
Dumbo	Dumba	k1gFnSc5	Dumba
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
nasmlouvala	nasmlouvat	k5eAaPmAgFnS	nasmlouvat
většinu	většina	k1gFnSc4	většina
kapacity	kapacita	k1gFnSc2	kapacita
jeho	jeho	k3xOp3gNnSc2	jeho
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
tvořit	tvořit	k5eAaImF	tvořit
instruktážní	instruktážní	k2eAgInPc4d1	instruktážní
filmy	film	k1gInPc4	film
a	a	k8xC	a
díla	dílo	k1gNnSc2	dílo
zvyšující	zvyšující	k2eAgNnSc4d1	zvyšující
bojové	bojový	k2eAgNnSc4d1	bojové
odhodlání	odhodlání	k1gNnSc4	odhodlání
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
tak	tak	k9	tak
například	například	k6eAd1	například
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
Der	drát	k5eAaImRp2nS	drát
Fuehrer	Fuehrer	k1gInSc1	Fuehrer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Face	Face	k1gNnSc7	Face
s	s	k7c7	s
Kačerem	kačer	k1gMnSc7	kačer
Donaldem	Donald	k1gMnSc7	Donald
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
nebo	nebo	k8xC	nebo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
snímek	snímek	k1gInSc4	snímek
Victory	Victor	k1gMnPc4	Victor
Through	Through	k1gMnSc1	Through
Air	Air	k1gMnSc1	Air
Power	Power	k1gMnSc1	Power
<g/>
.	.	kIx.	.
</s>
<s>
Válečné	válečný	k2eAgInPc1d1	válečný
filmy	film	k1gInPc1	film
však	však	k9	však
nepřinášely	přinášet	k5eNaImAgInP	přinášet
zisk	zisk	k1gInSc4	zisk
a	a	k8xC	a
také	také	k9	také
uvedení	uvedení	k1gNnSc1	uvedení
pohádky	pohádka	k1gFnSc2	pohádka
Bambi	Bamb	k1gFnSc2	Bamb
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
nesplnilo	splnit	k5eNaPmAgNnS	splnit
finanční	finanční	k2eAgNnSc1d1	finanční
očekávání	očekávání	k1gNnSc1	očekávání
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
upravená	upravený	k2eAgFnSc1d1	upravená
verze	verze	k1gFnSc1	verze
Sněhurky	Sněhurka	k1gFnSc2	Sněhurka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
disneyovská	disneyovský	k2eAgFnSc1d1	disneyovská
tradice	tradice	k1gFnSc1	tradice
opakovaného	opakovaný	k2eAgNnSc2d1	opakované
uvádění	uvádění	k1gNnSc2	uvádění
filmů	film	k1gInPc2	film
po	po	k7c6	po
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Three	Three	k1gInSc1	Three
Caballeros	Caballerosa	k1gFnPc2	Caballerosa
byl	být	k5eAaImAgInS	být
poslední	poslední	k2eAgInSc1d1	poslední
animovaný	animovaný	k2eAgInSc1d1	animovaný
celovečerní	celovečerní	k2eAgInSc1d1	celovečerní
film	film	k1gInSc1	film
z	z	k7c2	z
období	období	k1gNnSc2	období
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
William	William	k1gInSc1	William
Benton	Benton	k1gInSc1	Benton
<g/>
,	,	kIx,	,
senátor	senátor	k1gMnSc1	senátor
a	a	k8xC	a
vydavatel	vydavatel	k1gMnSc1	vydavatel
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
Encyclopæ	Encyclopæ	k1gMnSc1	Encyclopæ
Britannica	Britannica	k1gMnSc1	Britannica
<g/>
,	,	kIx,	,
marně	marně	k6eAd1	marně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
šesti	šest	k4xCc2	šest
až	až	k9	až
dvanácti	dvanáct	k4xCc2	dvanáct
naučných	naučný	k2eAgInPc2d1	naučný
filmů	film	k1gInPc2	film
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
také	také	k9	také
požádán	požádat	k5eAaPmNgInS	požádat
americkým	americký	k2eAgInSc7d1	americký
Úřadem	úřad	k1gInSc7	úřad
pro	pro	k7c4	pro
meziamerické	meziamerický	k2eAgFnPc4d1	meziamerický
záležitosti	záležitost	k1gFnPc4	záležitost
(	(	kIx(	(
<g/>
Office	Office	kA	Office
of	of	k?	of
Inter-American	Inter-American	k1gInSc1	Inter-American
Affairs	Affairs	k1gInSc1	Affairs
<g/>
,	,	kIx,	,
OIAA	OIAA	kA	OIAA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
natočil	natočit	k5eAaBmAgMnS	natočit
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
film	film	k1gInSc4	film
o	o	k7c6	o
Amazonii	Amazonie	k1gFnSc6	Amazonie
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
krátký	krátký	k2eAgInSc1d1	krátký
animovaný	animovaný	k2eAgInSc1d1	animovaný
snímek	snímek	k1gInSc1	snímek
s	s	k7c7	s
názvem	název	k1gInSc7	název
The	The	k1gFnPc2	The
Amazon	amazona	k1gFnPc2	amazona
Awakens	Awakens	k1gInSc1	Awakens
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1955	[number]	k4	1955
<g/>
:	:	kIx,	:
Poválečné	poválečný	k2eAgNnSc1d1	poválečné
období	období	k1gNnSc1	období
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
jeho	jeho	k3xOp3gNnSc1	jeho
studio	studio	k1gNnSc1	studio
natočilo	natočit	k5eAaBmAgNnS	natočit
několik	několik	k4yIc1	několik
levých	levý	k2eAgFnPc2d1	levá
filmových	filmový	k2eAgFnPc2d1	filmová
antologií	antologie	k1gFnPc2	antologie
<g/>
,	,	kIx,	,
sestavených	sestavený	k2eAgFnPc2d1	sestavená
z	z	k7c2	z
krátkých	krátký	k2eAgInPc2d1	krátký
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Make	Make	k1gNnSc1	Make
Mine	minout	k5eAaImIp3nS	minout
Music	Musice	k1gFnPc2	Musice
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Melody	Melod	k1gInPc1	Melod
Time	Tim	k1gInSc2	Tim
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fun	Fun	k1gFnSc1	Fun
and	and	k?	and
Fancy	Fanc	k2eAgFnPc4d1	Fanc
Free	Fre	k1gFnPc4	Fre
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
a	a	k8xC	a
The	The	k1gMnSc1	The
Adventures	Adventures	k1gMnSc1	Adventures
of	of	k?	of
Ichabod	Ichabod	k1gInSc1	Ichabod
and	and	k?	and
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Toad	Toad	k1gInSc1	Toad
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
jen	jen	k9	jen
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
i	i	k9	i
do	do	k7c2	do
natáčení	natáčení	k1gNnSc2	natáčení
celovečerních	celovečerní	k2eAgNnPc2d1	celovečerní
dramat	drama	k1gNnPc2	drama
<g/>
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Song	song	k1gInSc1	song
of	of	k?	of
the	the	k?	the
South	South	k1gInSc1	South
nebo	nebo	k8xC	nebo
So	So	kA	So
Dear	Dear	k1gInSc1	Dear
to	ten	k3xDgNnSc1	ten
My	my	k3xPp1nPc1	my
Heart	Hearta	k1gFnPc2	Hearta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
animované	animovaný	k2eAgFnSc2d1	animovaná
i	i	k8xC	i
hrané	hraný	k2eAgFnSc2d1	hraná
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
také	také	k9	také
postupně	postupně	k6eAd1	postupně
klesala	klesat	k5eAaImAgFnS	klesat
popularita	popularita	k1gFnSc1	popularita
Mickeyeho	Mickeye	k1gMnSc2	Mickeye
Mouse	Mous	k1gMnSc2	Mous
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
studio	studio	k1gNnSc1	studio
vzpamatovalo	vzpamatovat	k5eAaPmAgNnS	vzpamatovat
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohlo	moct	k5eAaImAgNnS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
Alenka	Alenka	k1gFnSc1	Alenka
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
divů	div	k1gInPc2	div
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
za	za	k7c2	za
války	válka	k1gFnSc2	válka
odložilo	odložit	k5eAaPmAgNnS	odložit
do	do	k7c2	do
šuplíku	šuplíku	k?	šuplíku
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
začalo	začít	k5eAaPmAgNnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
Popelce	Popelka	k1gFnSc6	Popelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gInSc7	jeho
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
premiéry	premiéra	k1gFnSc2	premiéra
Sněhurky	Sněhurka	k1gFnSc2	Sněhurka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
True-Life	True-Lif	k1gInSc5	True-Lif
Adventures	Adventures	k1gInSc1	Adventures
začal	začít	k5eAaPmAgInS	začít
točit	točit	k5eAaImF	točit
seriál	seriál	k1gInSc1	seriál
filmů	film	k1gInPc2	film
o	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
úspěch	úspěch	k1gInSc4	úspěch
celovečerních	celovečerní	k2eAgInPc2d1	celovečerní
filmů	film	k1gInPc2	film
však	však	k9	však
popularita	popularita	k1gFnSc1	popularita
disneyovské	disneyovský	k2eAgFnSc2d1	disneyovská
produkce	produkce	k1gFnSc2	produkce
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
firma	firma	k1gFnSc1	firma
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
studio	studio	k1gNnSc4	studio
Leon	Leona	k1gFnPc2	Leona
Schlesinger	Schlesingra	k1gFnPc2	Schlesingra
Productions	Productions	k1gInSc1	Productions
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
animovaným	animovaný	k2eAgMnSc7d1	animovaný
zajíčkem	zajíček	k1gMnSc7	zajíček
zvaným	zvaný	k2eAgInSc7d1	zvaný
Bugs	Bugsa	k1gFnPc2	Bugsa
Bunny	Bunen	k2eAgFnPc4d1	Bunen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
stalo	stát	k5eAaPmAgNnS	stát
nejpopulárnějším	populární	k2eAgNnSc7d3	nejpopulárnější
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rostla	růst	k5eAaImAgFnS	růst
popularita	popularita	k1gFnSc1	popularita
Bugse	Bugs	k1gMnSc2	Bugs
Bunnyho	Bunny	k1gMnSc2	Bunny
<g/>
,	,	kIx,	,
rostla	růst	k5eAaImAgFnS	růst
i	i	k9	i
popularita	popularita	k1gFnSc1	popularita
Kačera	kačer	k1gMnSc2	kačer
Donalda	Donald	k1gMnSc2	Donald
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
hlavní	hlavní	k2eAgFnSc7d1	hlavní
Disneyovou	Disneyový	k2eAgFnSc7d1	Disneyový
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
natočil	natočit	k5eAaBmAgMnS	natočit
několik	několik	k4yIc4	několik
vzdělávacích	vzdělávací	k2eAgInPc2d1	vzdělávací
filmů	film	k1gInPc2	film
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
(	(	kIx(	(
<g/>
Man	Man	k1gMnSc1	Man
in	in	k?	in
Space	Space	k1gMnSc1	Space
<g/>
,	,	kIx,	,
Man	Man	k1gMnSc1	Man
and	and	k?	and
the	the	k?	the
Moon	Moon	k1gInSc1	Moon
a	a	k8xC	a
Mars	Mars	k1gInSc1	Mars
and	and	k?	and
Beyond	Beyond	k1gInSc1	Beyond
<g/>
)	)	kIx)	)
společně	společně	k6eAd1	společně
s	s	k7c7	s
návrhářem	návrhář	k1gMnSc7	návrhář
raket	raketa	k1gFnPc2	raketa
Wernherem	Wernher	k1gMnSc7	Wernher
von	von	k1gInSc1	von
Braunem	Braun	k1gMnSc7	Braun
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracoval	pracovat	k5eAaImAgInS	pracovat
pro	pro	k7c4	pro
NASA	NASA	kA	NASA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
horlivého	horlivý	k2eAgMnSc4d1	horlivý
antikomunistu	antikomunista	k1gMnSc4	antikomunista
<g/>
,	,	kIx,	,
zakladatele	zakladatel	k1gMnSc2	zakladatel
antikomunistické	antikomunistický	k2eAgFnSc2d1	antikomunistická
organizace	organizace	k1gFnSc2	organizace
Motion	Motion	k1gInSc1	Motion
Picture	Pictur	k1gMnSc5	Pictur
Alliance	Allianec	k1gInPc4	Allianec
for	forum	k1gNnPc2	forum
the	the	k?	the
Preservation	Preservation	k1gInSc1	Preservation
of	of	k?	of
American	American	k1gInSc1	American
Ideals	Ideals	k1gInSc1	Ideals
(	(	kIx(	(
<g/>
Filmařská	filmařský	k2eAgFnSc1d1	filmařská
aliance	aliance	k1gFnSc1	aliance
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
amerických	americký	k2eAgInPc2d1	americký
ideálů	ideál	k1gInPc2	ideál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
v	v	k7c6	v
období	období	k1gNnSc6	období
mccarthistické	mccarthistický	k2eAgFnSc2d1	mccarthistický
antikomunistické	antikomunistický	k2eAgFnSc2d1	antikomunistická
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
předstoupil	předstoupit	k5eAaPmAgMnS	předstoupit
před	před	k7c4	před
kongresový	kongresový	k2eAgInSc4d1	kongresový
Výbor	výbor	k1gInSc4	výbor
pro	pro	k7c4	pro
neamerickou	americký	k2eNgFnSc4d1	neamerická
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
HUAC	HUAC	kA	HUAC
<g/>
)	)	kIx)	)
a	a	k8xC	a
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Herberta	Herbert	k1gMnSc4	Herbert
Sorrella	Sorrell	k1gMnSc4	Sorrell
<g/>
,	,	kIx,	,
Davida	David	k1gMnSc4	David
Hilbermana	Hilberman	k1gMnSc4	Hilberman
a	a	k8xC	a
Williama	William	k1gMnSc4	William
Pomeranceho	Pomerance	k1gMnSc4	Pomerance
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnPc4d1	přední
animátory	animátor	k1gMnPc4	animátor
a	a	k8xC	a
odboráře	odborář	k1gMnPc4	odborář
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
komunistické	komunistický	k2eAgInPc1d1	komunistický
agitátory	agitátor	k1gInPc1	agitátor
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
obvinění	obviněný	k2eAgMnPc1d1	obviněný
popřeli	popřít	k5eAaPmAgMnP	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
obvinil	obvinit	k5eAaPmAgMnS	obvinit
filmařské	filmařský	k2eAgInPc4d1	filmařský
odbory	odbor	k1gInPc4	odbor
Screen	Screen	k2eAgInSc4d1	Screen
Actors	Actors	k1gInSc4	Actors
Guild	Guild	k1gMnSc1	Guild
(	(	kIx(	(
<g/>
SAG	SAG	kA	SAG
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
komunistické	komunistický	k2eAgFnSc2d1	komunistická
snahy	snaha	k1gFnSc2	snaha
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
:	:	kIx,	:
Vznik	vznik	k1gInSc1	vznik
Disneylandu	Disneyland	k1gInSc2	Disneyland
a	a	k8xC	a
růst	růst	k1gInSc1	růst
podnikání	podnikání	k1gNnSc2	podnikání
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
čtyřicátých	čtyřicátý	k4xOgNnPc2	čtyřicátý
let	léto	k1gNnPc2	léto
během	během	k7c2	během
obchodní	obchodní	k2eAgFnSc2d1	obchodní
cesty	cesta	k1gFnSc2	cesta
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
náčrt	náčrt	k1gInSc1	náčrt
zábavního	zábavní	k2eAgInSc2d1	zábavní
parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nP	by
jeho	jeho	k3xOp3gMnPc3	jeho
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
mohli	moct	k5eAaImAgMnP	moct
trávit	trávit	k5eAaImF	trávit
čas	čas	k1gInSc4	čas
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
myšlenku	myšlenka	k1gFnSc4	myšlenka
přišel	přijít	k5eAaPmAgMnS	přijít
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
zábavního	zábavní	k2eAgInSc2d1	zábavní
parku	park	k1gInSc2	park
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Fairyland	Fairylanda	k1gFnPc2	Fairylanda
v	v	k7c6	v
Oaklandu	Oaklando	k1gNnSc6	Oaklando
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
pozemek	pozemek	k1gInSc4	pozemek
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
originální	originální	k2eAgInPc1d1	originální
nápady	nápad	k1gInPc1	nápad
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
svůj	svůj	k3xOyFgInSc4	svůj
nápad	nápad	k1gInSc4	nápad
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
<g/>
,	,	kIx,	,
však	však	k9	však
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgInSc4d1	budoucí
Disneyland	Disneyland	k1gInSc4	Disneyland
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
potřebu	potřeba	k1gFnSc4	potřeba
většího	veliký	k2eAgNnSc2d2	veliký
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Strávil	strávit	k5eAaPmAgMnS	strávit
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
přemýšlením	přemýšlení	k1gNnSc7	přemýšlení
nad	nad	k7c7	nad
stavbou	stavba	k1gFnSc7	stavba
parku	park	k1gInSc2	park
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
novou	nový	k2eAgFnSc4d1	nová
dceřinou	dceřiný	k2eAgFnSc4d1	dceřiná
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Imagineering	Imagineering	k1gInSc1	Imagineering
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
za	za	k7c4	za
plánování	plánování	k1gNnSc4	plánování
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
malá	malý	k2eAgFnSc1d1	malá
skupinka	skupinka	k1gFnSc1	skupinka
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
inženýry	inženýr	k1gMnPc4	inženýr
a	a	k8xC	a
projektanty	projektant	k1gMnPc4	projektant
<g/>
,	,	kIx,	,
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
rozvojovému	rozvojový	k2eAgInSc3d1	rozvojový
projektu	projekt	k1gInSc3	projekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
plánování	plánování	k1gNnSc6	plánování
Disneylandu	Disneyland	k1gInSc2	Disneyland
řekl	říct	k5eAaPmAgMnS	říct
Herbertu	Herbert	k1gMnSc3	Herbert
Rymanovi	Ryman	k1gMnSc3	Ryman
<g/>
,	,	kIx,	,
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
projektantů	projektant	k1gMnPc2	projektant
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Herbie	Herbius	k1gMnSc5	Herbius
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jen	jen	k9	jen
chci	chtít	k5eAaImIp1nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
vypadalo	vypadat	k5eAaImAgNnS	vypadat
jako	jako	k9	jako
nikde	nikde	k6eAd1	nikde
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
obklopené	obklopený	k2eAgFnPc4d1	obklopená
železnicí	železnice	k1gFnSc7	železnice
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Disneyland	Disneyland	k1gInSc1	Disneyland
oficiálně	oficiálně	k6eAd1	oficiálně
otevřel	otevřít	k5eAaPmAgInS	otevřít
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc4	den
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
zábavním	zábavní	k2eAgInSc6d1	zábavní
parku	park	k1gInSc6	park
konala	konat	k5eAaImAgFnS	konat
předpremiéra	předpremiéra	k1gFnSc1	předpremiéra
pro	pro	k7c4	pro
tisíce	tisíc	k4xCgInPc4	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
přenášená	přenášený	k2eAgFnSc1d1	přenášená
televizí	televize	k1gFnSc7	televize
<g/>
.	.	kIx.	.
</s>
<s>
Přišli	přijít	k5eAaPmAgMnP	přijít
i	i	k9	i
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Cummings	Cummings	k1gInSc1	Cummings
<g/>
,	,	kIx,	,
Art	Art	k1gMnSc1	Art
Linkletter	Linkletter	k1gMnSc1	Linkletter
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
Anaheimu	Anaheim	k1gInSc2	Anaheim
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
zde	zde	k6eAd1	zde
přednesl	přednést	k5eAaPmAgMnS	přednést
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
věnující	věnující	k2eAgFnSc1d1	věnující
řeč	řeč	k1gFnSc1	řeč
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Firma	firma	k1gFnSc1	firma
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Productions	Productionsa	k1gFnPc2	Productionsa
vedle	vedle	k7c2	vedle
Disneylandu	Disneyland	k1gInSc2	Disneyland
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
i	i	k9	i
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
filmových	filmový	k2eAgNnPc6d1	filmové
dílech	dílo	k1gNnPc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
první	první	k4xOgInSc1	první
film	film	k1gInSc1	film
se	s	k7c7	s
živými	živý	k2eAgMnPc7d1	živý
herci	herec	k1gMnPc7	herec
Ostrov	ostrov	k1gInSc4	ostrov
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgInP	následovat
20	[number]	k4	20
000	[number]	k4	000
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Yeller	Yeller	k1gInSc1	Yeller
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chundelatý	chundelatý	k2eAgMnSc1d1	chundelatý
pes	pes	k1gMnSc1	pes
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pollyanna	Pollyanna	k1gFnSc1	Pollyanna
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Absent-Minded	Absent-Minded	k1gMnSc1	Absent-Minded
Professor	Professor	k1gMnSc1	Professor
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Parent	Parent	k1gMnSc1	Parent
Trap	trap	k1gInSc1	trap
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
Walt	Waltum	k1gNnPc2	Waltum
Disney	Disnea	k1gFnSc2	Disnea
Studio	studio	k1gNnSc1	studio
premiérovalo	premiérovat	k5eAaBmAgNnS	premiérovat
televizní	televizní	k2eAgInSc4d1	televizní
speciál	speciál	k1gInSc4	speciál
One	One	k1gMnSc1	One
Hour	Hour	k1gMnSc1	Hour
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studio	studio	k1gNnSc1	studio
se	se	k3xPyFc4	se
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
již	již	k6eAd1	již
přestával	přestávat	k5eAaImAgMnS	přestávat
řídit	řídit	k5eAaImF	řídit
svůj	svůj	k3xOyFgInSc4	svůj
tým	tým	k1gInSc4	tým
animátorů	animátor	k1gMnPc2	animátor
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
během	během	k7c2	během
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
velmi	velmi	k6eAd1	velmi
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
pohádek	pohádka	k1gFnPc2	pohádka
včetně	včetně	k7c2	včetně
filmů	film	k1gInPc2	film
jako	jako	k8xC	jako
Lady	lady	k1gFnPc2	lady
a	a	k8xC	a
Tramp	tramp	k1gInSc1	tramp
<g/>
,	,	kIx,	,
Šípková	šípkový	k2eAgFnSc1d1	šípková
Růženka	Růženka	k1gFnSc1	Růženka
<g/>
,	,	kIx,	,
101	[number]	k4	101
dalmatinů	dalmatin	k1gMnPc2	dalmatin
nebo	nebo	k8xC	nebo
Meč	meč	k1gInSc1	meč
v	v	k7c6	v
kameni	kámen	k1gInSc6	kámen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
také	také	k9	také
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
divizi	divize	k1gFnSc4	divize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
zabývat	zabývat	k5eAaImF	zabývat
hudební	hudební	k2eAgFnSc7d1	hudební
produkcí	produkce	k1gFnSc7	produkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
inspirován	inspirován	k2eAgInSc1d1	inspirován
veleúspěšným	veleúspěšný	k2eAgInSc7d1	veleúspěšný
singlem	singl	k1gInSc7	singl
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Ballad	Ballad	k1gInSc1	Ballad
of	of	k?	of
Davy	Dav	k1gInPc1	Dav
Crockett	Crockett	k1gInSc1	Crockett
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
a	a	k8xC	a
distribuční	distribuční	k2eAgFnSc4d1	distribuční
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
Disneyland	Disneyland	k1gInSc1	Disneyland
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1966	[number]	k4	1966
<g/>
:	:	kIx,	:
Závěr	závěr	k1gInSc1	závěr
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
velké	velký	k2eAgFnSc3d1	velká
popularitě	popularita	k1gFnSc3	popularita
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Production	Production	k1gInSc1	Production
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
mezi	mezi	k7c4	mezi
přední	přední	k2eAgMnPc4d1	přední
světové	světový	k2eAgMnPc4d1	světový
filmové	filmový	k2eAgMnPc4d1	filmový
producenty	producent	k1gMnPc4	producent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desítce	desítka	k1gFnSc6	desítka
let	let	k1gInSc4	let
vyjednávání	vyjednávání	k1gNnSc2	vyjednávání
konečně	konečně	k6eAd1	konečně
získal	získat	k5eAaPmAgInS	získat
práva	právo	k1gNnSc2	právo
zfimovat	zfimovat	k5eAaImF	zfimovat
knihu	kniha	k1gFnSc4	kniha
P.	P.	kA	P.
L.	L.	kA	L.
Traversové	Traversová	k1gFnSc2	Traversová
o	o	k7c6	o
kouzelné	kouzelný	k2eAgFnSc6d1	kouzelná
chůvě	chůva	k1gFnSc6	chůva
Mary	Mary	k1gFnSc2	Mary
Poppinsové	Poppinsová	k1gFnSc2	Poppinsová
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc7	jeho
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
šedesátých	šedesátý	k4xOgInPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vystavil	vystavit	k5eAaPmAgInS	vystavit
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
svých	svůj	k3xOyFgInPc2	svůj
exponátů	exponát	k1gInPc2	exponát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
později	pozdě	k6eAd2	pozdě
ozdobily	ozdobit	k5eAaPmAgInP	ozdobit
Disneyland	Disneyland	k1gInSc4	Disneyland
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stavěl	stavět	k5eAaImAgMnS	stavět
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jeho	jeho	k3xOp3gNnSc1	jeho
studio	studio	k1gNnSc1	studio
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
konkurovat	konkurovat	k5eAaImF	konkurovat
studiu	studio	k1gNnSc3	studio
Hanna-Barbera	Hanna-Barbero	k1gNnSc2	Hanna-Barbero
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
televizních	televizní	k2eAgInPc2d1	televizní
animovaných	animovaný	k2eAgInPc2d1	animovaný
seriálů	seriál	k1gInPc2	seriál
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
celovečerní	celovečerní	k2eAgInPc4d1	celovečerní
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
oznámil	oznámit	k5eAaPmAgInS	oznámit
záměr	záměr	k1gInSc1	záměr
vybudovat	vybudovat	k5eAaPmF	vybudovat
další	další	k2eAgInSc4d1	další
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
Orlanda	Orlando	k1gNnSc2	Orlando
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
nazývat	nazývat	k5eAaImF	nazývat
Disney	Disnea	k1gFnPc4	Disnea
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
<s>
Disney	Disne	k2eAgInPc1d1	Disne
World	World	k1gInSc1	World
měl	mít	k5eAaImAgInS	mít
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
komplikovanější	komplikovaný	k2eAgFnSc4d2	komplikovanější
verzi	verze	k1gFnSc4	verze
Disneylandu	Disneyland	k1gInSc2	Disneyland
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Magic	Magice	k1gInPc2	Magice
Kingdom	Kingdom	k1gInSc4	Kingdom
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
zde	zde	k6eAd1	zde
být	být	k5eAaImF	být
také	také	k9	také
několik	několik	k4yIc4	několik
golfových	golfový	k2eAgNnPc2d1	golfové
hřišť	hřiště	k1gNnPc2	hřiště
a	a	k8xC	a
rekreačních	rekreační	k2eAgInPc2d1	rekreační
hotelů	hotel	k1gInPc2	hotel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Srdcem	srdce	k1gNnSc7	srdce
<g/>
"	"	kIx"	"
Disney	Disnea	k1gFnSc2	Disnea
Worldu	World	k1gInSc2	World
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k1gInSc1	stát
tzv.	tzv.	kA	tzv.
Experimental	Experimental	k1gMnSc1	Experimental
Prototype	prototyp	k1gInSc5	prototyp
Community	Communit	k2eAgMnPc4d1	Communit
of	of	k?	of
Tomorrow	Tomorrow	k1gFnSc4	Tomorrow
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
EPCOT	EPCOT	kA	EPCOT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
plánoval	plánovat	k5eAaImAgMnS	plánovat
podstoupit	podstoupit	k5eAaPmF	podstoupit
operaci	operace	k1gFnSc4	operace
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
starého	starý	k2eAgNnSc2d1	staré
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
si	se	k3xPyFc3	se
způsobil	způsobit	k5eAaPmAgMnS	způsobit
během	během	k7c2	během
hraní	hraní	k1gNnSc2	hraní
póla	pólo	k1gNnSc2	pólo
v	v	k7c4	v
Riviera	Riviero	k1gNnPc4	Riviero
Clubu	club	k1gInSc2	club
v	v	k7c6	v
Hollywoodu	Hollywood	k1gInSc6	Hollywood
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
objevili	objevit	k5eAaPmAgMnP	objevit
lékaři	lékař	k1gMnPc1	lékař
nemocnice	nemocnice	k1gFnSc2	nemocnice
Providence	providence	k1gFnSc2	providence
Saint	Sainto	k1gNnPc2	Sainto
Joseph	Joseph	k1gMnSc1	Joseph
Medical	Medical	k1gFnSc2	Medical
Center	centrum	k1gNnPc2	centrum
na	na	k7c6	na
předoperačních	předoperační	k2eAgInPc6d1	předoperační
rentgenových	rentgenový	k2eAgInPc6d1	rentgenový
snímcích	snímek	k1gInPc6	snímek
nádor	nádor	k1gInSc1	nádor
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
plíci	plíce	k1gFnSc6	plíce
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
Disney	Disnea	k1gFnSc2	Disnea
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
biopsii	biopsie	k1gFnSc4	biopsie
nádoru	nádor	k1gInSc2	nádor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
<g/>
.	.	kIx.	.
</s>
<s>
Nádor	nádor	k1gInSc1	nádor
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
levé	levý	k2eAgFnSc2d1	levá
plíce	plíce	k1gFnSc2	plíce
a	a	k8xC	a
po	po	k7c6	po
odstranění	odstranění	k1gNnSc6	odstranění
plíce	plíce	k1gFnSc2	plíce
lékaři	lékař	k1gMnPc1	lékař
Disneye	Disneye	k1gFnPc2	Disneye
informovali	informovat	k5eAaBmAgMnP	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
zbývá	zbývat	k5eAaImIp3nS	zbývat
jen	jen	k9	jen
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
až	až	k6eAd1	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
několika	několik	k4yIc2	několik
chemoterapií	chemoterapie	k1gFnPc2	chemoterapie
strávil	strávit	k5eAaPmAgInS	strávit
krátký	krátký	k2eAgInSc1d1	krátký
čas	čas	k1gInSc1	čas
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
v	v	k7c4	v
Palm	Palm	k1gInSc4	Palm
Springs	Springsa	k1gFnPc2	Springsa
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Přivolanou	přivolaný	k2eAgFnSc7d1	přivolaná
sanitkou	sanitka	k1gFnSc7	sanitka
byl	být	k5eAaImAgMnS	být
odvezen	odvézt	k5eAaPmNgMnS	odvézt
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
St.	st.	kA	st.
Joseph	Joseph	k1gInSc4	Joseph
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
v	v	k7c4	v
půl	půl	k6eAd1	půl
desáté	desátý	k4xOgNnSc4	desátý
dopoledne	dopoledne	k1gNnSc4	dopoledne
<g/>
,	,	kIx,	,
deset	deset	k4xCc4	deset
dnů	den	k1gInPc2	den
po	po	k7c4	po
svých	svůj	k3xOyFgNnPc2	svůj
65	[number]	k4	65
<g/>
.	.	kIx.	.
narozeninách	narozeniny	k1gFnPc6	narozeniny
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
selhání	selhání	k1gNnSc4	selhání
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
způsobené	způsobený	k2eAgNnSc1d1	způsobené
rakovinou	rakovina	k1gFnSc7	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
údajně	údajně	k6eAd1	údajně
napsal	napsat	k5eAaPmAgInS	napsat
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jméno	jméno	k1gNnSc1	jméno
herce	herec	k1gMnSc2	herec
Kurta	Kurt	k1gMnSc2	Kurt
Russella	Russell	k1gMnSc2	Russell
<g/>
;	;	kIx,	;
význam	význam	k1gInSc1	význam
zápisu	zápis	k1gInSc2	zápis
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
záhadou	záhada	k1gFnSc7	záhada
i	i	k9	i
pro	pro	k7c4	pro
samotného	samotný	k2eAgMnSc4d1	samotný
Russella	Russell	k1gMnSc4	Russell
<g/>
.	.	kIx.	.
<g/>
Zpopelněn	zpopelněn	k2eAgMnSc1d1	zpopelněn
byl	být	k5eAaImAgMnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
a	a	k8xC	a
popel	popel	k1gInSc1	popel
byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Forest	Forest	k1gFnSc1	Forest
Lawn	Lawn	k1gInSc1	Lawn
Memorial	Memorial	k1gInSc1	Memorial
Park	park	k1gInSc1	park
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Waltův	Waltův	k2eAgMnSc1d1	Waltův
starší	starý	k2eAgMnSc1d2	starší
bratr	bratr	k1gMnSc1	bratr
Roy	Roy	k1gMnSc1	Roy
Disney	Disney	k1gInPc4	Disney
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
realizaci	realizace	k1gFnSc6	realizace
projektu	projekt	k1gInSc2	projekt
Florida	Florida	k1gFnSc1	Florida
a	a	k8xC	a
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	World	k1gInSc4	World
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
svůj	svůj	k3xOyFgInSc4	svůj
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Kniha	kniha	k1gFnSc1	kniha
džunglí	džungle	k1gFnSc7	džungle
<g/>
,	,	kIx,	,
Winnie	Winnie	k1gFnSc1	Winnie
the	the	k?	the
Pooh	Pooh	k1gInSc1	Pooh
and	and	k?	and
the	the	k?	the
Blustery	Bluster	k1gInPc1	Bluster
Day	Day	k1gFnSc1	Day
(	(	kIx(	(
<g/>
Medvídek	medvídek	k1gMnSc1	medvídek
Pú	Pú	k1gMnSc1	Pú
a	a	k8xC	a
Bouřlivý	bouřlivý	k2eAgInSc1d1	bouřlivý
den	den	k1gInSc1	den
<g/>
)	)	kIx)	)
a	a	k8xC	a
hudební	hudební	k2eAgFnSc2d1	hudební
komedie	komedie	k1gFnSc2	komedie
The	The	k1gMnSc1	The
Happiest	Happiest	k1gMnSc1	Happiest
Millionaire	Millionair	k1gInSc5	Millionair
(	(	kIx(	(
<g/>
Nejšťastnější	šťastný	k2eAgMnSc1d3	nejšťastnější
milionář	milionář	k1gMnSc1	milionář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
Robert	Robert	k1gMnSc1	Robert
B.	B.	kA	B.
Sherman	Sherman	k1gMnSc1	Sherman
vzpomínal	vzpomínat	k5eAaImAgMnS	vzpomínat
na	na	k7c4	na
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gMnSc4	on
viděl	vidět	k5eAaImAgMnS	vidět
naposledy	naposledy	k6eAd1	naposledy
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Dlouho	dlouho	k6eAd1	dlouho
kolovala	kolovat	k5eAaImAgFnS	kolovat
fáma	fáma	k1gFnSc1	fáma
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
kryoprezervaci	kryoprezervace	k1gFnSc6	kryoprezervace
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc1	jeho
zmrzlé	zmrzlý	k2eAgNnSc1d1	zmrzlé
tělo	tělo	k1gNnSc1	tělo
prý	prý	k9	prý
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
Disneylandu	Disneyland	k1gInSc6	Disneyland
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
první	první	k4xOgFnSc1	první
známá	známý	k2eAgFnSc1d1	známá
kryoprezervace	kryoprezervace	k1gFnSc1	kryoprezervace
lidské	lidský	k2eAgFnSc2d1	lidská
mrtvoly	mrtvola	k1gFnSc2	mrtvola
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
až	až	k9	až
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc3	jeho
bratr	bratr	k1gMnSc1	bratr
Roy	Roy	k1gMnSc1	Roy
Disney	Disney	k1gInPc4	Disney
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
převzal	převzít	k5eAaPmAgInS	převzít
vedení	vedení	k1gNnSc4	vedení
Walt	Waltum	k1gNnPc2	Waltum
Disney	Disnea	k1gFnSc2	Disnea
Production	Production	k1gInSc1	Production
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
rodinných	rodinný	k2eAgInPc2d1	rodinný
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
se	se	k3xPyFc4	se
rodiny	rodina	k1gFnPc1	rodina
Walta	Walt	k1gMnSc2	Walt
a	a	k8xC	a
Roye	Roy	k1gMnSc2	Roy
setkaly	setkat	k5eAaPmAgInP	setkat
v	v	k7c6	v
Magic	Magice	k1gInPc2	Magice
Kingdom	Kingdom	k1gInSc1	Kingdom
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
otevřely	otevřít	k5eAaPmAgFnP	otevřít
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
Resort	resort	k1gInSc1	resort
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Roy	Roy	k1gMnSc1	Roy
pronesl	pronést	k5eAaPmAgMnS	pronést
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
řeč	řeč	k1gFnSc4	řeč
<g/>
,	,	kIx,	,
požádal	požádat	k5eAaPmAgMnS	požádat
Lillian	Lillian	k1gMnSc1	Lillian
Disneyovou	Disneyová	k1gFnSc4	Disneyová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
připojila	připojit	k5eAaPmAgFnS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
<g/>
,	,	kIx,	,
zeptal	zeptat	k5eAaPmAgMnS	zeptat
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lilly	Lill	k1gInPc1	Lill
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
jsi	být	k5eAaImIp2nS	být
znala	znát	k5eAaImAgFnS	znát
ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
naděje	naděje	k1gFnPc4	naděje
Walta	Walt	k1gMnSc2	Walt
jako	jako	k8xC	jako
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
o	o	k7c4	o
Walt	Walt	k1gInSc4	Walt
Disney	Disnea	k1gFnSc2	Disnea
World	Worlda	k1gFnPc2	Worlda
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
ho	on	k3xPp3gInSc4	on
Walt	Walt	k1gInSc4	Walt
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Roy	Roy	k1gMnSc1	Roy
umřel	umřít	k5eAaPmAgMnS	umřít
na	na	k7c4	na
mozkovou	mozkový	k2eAgFnSc4d1	mozková
mrtvici	mrtvice	k1gFnSc4	mrtvice
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Disneylandu	Disneyland	k1gInSc6	Disneyland
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
vánoční	vánoční	k2eAgInSc1d1	vánoční
průvod	průvod	k1gInSc1	průvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
filmová	filmový	k2eAgNnPc1d1	filmové
studia	studio	k1gNnPc1	studio
a	a	k8xC	a
zábavní	zábavní	k2eAgInPc1d1	zábavní
parky	park	k1gInPc1	park
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
staly	stát	k5eAaPmAgFnP	stát
multimiliardovou	multimiliardův	k2eAgFnSc7d1	multimiliardův
televizní	televizní	k2eAgFnSc7d1	televizní
<g/>
,	,	kIx,	,
filmovou	filmový	k2eAgFnSc7d1	filmová
a	a	k8xC	a
multimediální	multimediální	k2eAgFnSc7d1	multimediální
korporací	korporace	k1gFnSc7	korporace
<g/>
,	,	kIx,	,
nesoucí	nesoucí	k2eAgFnSc7d1	nesoucí
stále	stále	k6eAd1	stále
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Company	Compana	k1gFnSc2	Compana
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
pět	pět	k4xCc1	pět
rekreačních	rekreační	k2eAgNnPc2d1	rekreační
středisek	středisko	k1gNnPc2	středisko
<g/>
,	,	kIx,	,
jedenáct	jedenáct	k4xCc1	jedenáct
zábavních	zábavní	k2eAgInPc2d1	zábavní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
vodní	vodní	k2eAgInPc1d1	vodní
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
třicet	třicet	k4xCc1	třicet
devět	devět	k4xCc1	devět
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
osm	osm	k4xCc4	osm
filmových	filmový	k2eAgFnPc2d1	filmová
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
nahrávacích	nahrávací	k2eAgFnPc2d1	nahrávací
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
jedenáct	jedenáct	k4xCc4	jedenáct
kabelových	kabelový	k2eAgFnPc2d1	kabelová
televizních	televizní	k2eAgFnPc2d1	televizní
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
pozemní	pozemní	k2eAgFnSc1d1	pozemní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
společnost	společnost	k1gFnSc1	společnost
přiznala	přiznat	k5eAaPmAgFnS	přiznat
roční	roční	k2eAgInPc4d1	roční
příjmy	příjem	k1gInPc4	příjem
více	hodně	k6eAd2	hodně
než	než	k8xS	než
35	[number]	k4	35
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
otevřeno	otevřen	k2eAgNnSc4d1	otevřeno
Rodinné	rodinný	k2eAgNnSc4d1	rodinné
muzeum	muzeum	k1gNnSc4	muzeum
Walta	Walt	k1gMnSc2	Walt
Disneyeho	Disneye	k1gMnSc2	Disneye
(	(	kIx(	(
<g/>
The	The	k1gMnSc2	The
Walt	Walt	k1gMnSc1	Walt
Disney	Disnea	k1gMnSc2	Disnea
Family	Famila	k1gFnSc2	Famila
Museum	museum	k1gNnSc1	museum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
tisíce	tisíc	k4xCgInPc4	tisíc
předmětů	předmět	k1gInPc2	předmět
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgNnPc2	všecek
248	[number]	k4	248
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
získal	získat	k5eAaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
šířit	šířit	k5eAaImF	šířit
zvěsti	zvěst	k1gFnPc4	zvěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
antisemita	antisemita	k1gMnSc1	antisemita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
životopisec	životopisec	k1gMnSc1	životopisec
Neal	Neal	k1gMnSc1	Neal
Gabler	Gabler	k1gMnSc1	Gabler
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
spisovatel	spisovatel	k1gMnSc1	spisovatel
získal	získat	k5eAaPmAgMnS	získat
neomezený	omezený	k2eNgInSc4d1	neomezený
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
jeho	jeho	k3xOp3gInPc3	jeho
archivům	archiv	k1gInPc3	archiv
<g/>
,	,	kIx,	,
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
došel	dojít	k5eAaPmAgMnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
antisemitismu	antisemitismus	k1gInSc6	antisemitismus
neexistuje	existovat	k5eNaImIp3nS	existovat
důkaz	důkaz	k1gInSc1	důkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Disneyovo	Disneyův	k2eAgNnSc1d1	Disneyovo
museum	museum	k1gNnSc1	museum
připouští	připouštět	k5eAaImIp3nS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
složité	složitý	k2eAgInPc4d1	složitý
vztahy	vztah	k1gInPc4	vztah
<g/>
"	"	kIx"	"
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
Židy	Žid	k1gMnPc7	Žid
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
raných	raný	k2eAgInPc6d1	raný
filmech	film	k1gInPc6	film
z	z	k7c2	z
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
použil	použít	k5eAaPmAgInS	použít
dobové	dobový	k2eAgInPc4d1	dobový
etnické	etnický	k2eAgInPc4d1	etnický
stereotypy	stereotyp	k1gInPc4	stereotyp
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Tři	tři	k4xCgNnPc4	tři
malá	malý	k2eAgNnPc4d1	malé
prasátka	prasátko	k1gNnPc4	prasátko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
zaměstnával	zaměstnávat	k5eAaImAgMnS	zaměstnávat
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
mužem	muž	k1gMnSc7	muž
roku	rok	k1gInSc2	rok
židovskou	židovský	k2eAgFnSc7d1	židovská
organizací	organizace	k1gFnSc7	organizace
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
nai	nai	k?	nai
B	B	kA	B
<g/>
'	'	kIx"	'
<g/>
rith	rith	k1gMnSc1	rith
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ceny	cena	k1gFnPc1	cena
Akademie	akademie	k1gFnSc2	akademie
==	==	k?	==
</s>
</p>
<p>
<s>
Dodnes	dodnes	k6eAd1	dodnes
drží	držet	k5eAaImIp3nP	držet
rekord	rekord	k1gInSc4	rekord
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
nominací	nominace	k1gFnPc2	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
(	(	kIx(	(
<g/>
59	[number]	k4	59
<g/>
)	)	kIx)	)
i	i	k8xC	i
počet	počet	k1gInSc1	počet
udělených	udělený	k2eAgInPc2d1	udělený
Oscarů	Oscar	k1gInPc2	Oscar
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
čtyři	čtyři	k4xCgInPc4	čtyři
čestné	čestný	k2eAgInPc4d1	čestný
Oscary	Oscar	k1gInPc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
Cenu	cena	k1gFnSc4	cena
Akademie	akademie	k1gFnSc2	akademie
obdržel	obdržet	k5eAaPmAgMnS	obdržet
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Flowers	Flowers	k1gInSc1	Flowers
and	and	k?	and
Trees	Trees	k1gInSc1	Trees
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
–	–	k?	–
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
vytvoření	vytvoření	k1gNnSc4	vytvoření
Mickey	Mickea	k1gFnSc2	Mickea
Mouse	Mouse	k1gFnSc2	Mouse
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Tři	tři	k4xCgNnPc4	tři
malá	malý	k2eAgNnPc4d1	malé
prasátka	prasátko	k1gNnPc4	prasátko
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Želva	želva	k1gFnSc1	želva
a	a	k8xC	a
zajíc	zajíc	k1gMnSc1	zajíc
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Three	Three	k1gInSc4	Three
Orphan	Orphana	k1gFnPc2	Orphana
Kittens	Kittensa	k1gFnPc2	Kittensa
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
The	The	k1gFnSc4	The
Country	country	k2eAgFnPc2d1	country
Cousin	cousina	k1gFnPc2	cousina
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
mlýn	mlýn	k1gInSc1	mlýn
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Býk	býk	k1gMnSc1	býk
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
(	(	kIx(	(
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
–	–	k?	–
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
film	film	k1gInSc4	film
Sněhurka	Sněhurka	k1gFnSc1	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc1	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
Sněhurku	Sněhurka	k1gFnSc4	Sněhurka
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
významnou	významný	k2eAgFnSc4d1	významná
inovaci	inovace	k1gFnSc4	inovace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
okouzlila	okouzlit	k5eAaPmAgFnS	okouzlit
miliony	milion	k4xCgInPc7	milion
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
propagaci	propagace	k1gFnSc4	propagace
skvělé	skvělý	k2eAgFnSc2d1	skvělá
oblasti	oblast	k1gFnSc2	oblast
zábavy	zábava	k1gFnSc2	zábava
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cena	cena	k1gFnSc1	cena
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
soška	soška	k1gFnSc1	soška
a	a	k8xC	a
sedm	sedm	k4xCc4	sedm
miniaturních	miniaturní	k2eAgFnPc2d1	miniaturní
sošek	soška	k1gFnPc2	soška
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Ošklivé	ošklivý	k2eAgNnSc4d1	ošklivé
káčátko	káčátko	k1gNnSc4	káčátko
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1941	[number]	k4	1941
–	–	k?	–
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
za	za	k7c4	za
film	film	k1gInSc4	film
Fantazie	fantazie	k1gFnSc2	fantazie
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Williamem	William	k1gInSc7	William
E.	E.	kA	E.
Garityem	Garityem	k1gInSc1	Garityem
a	a	k8xC	a
J.	J.	kA	J.
N.	N.	kA	N.
A.	A.	kA	A.
Hawkinsem	Hawkins	k1gInSc7	Hawkins
<g/>
.	.	kIx.	.
</s>
<s>
Citace	citace	k1gFnSc1	citace
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Za	za	k7c4	za
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
využívání	využívání	k1gNnSc2	využívání
zvuku	zvuk	k1gInSc2	zvuk
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
produkci	produkce	k1gFnSc6	produkce
Fantazie	fantazie	k1gFnSc2	fantazie
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Dej	dát	k5eAaPmRp2nS	dát
pac	pac	k1gInSc1	pac
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1943	[number]	k4	1943
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Der	drát	k5eAaImRp2nS	drát
Fuehrer	Fuehrer	k1gInSc1	Fuehrer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Face	Face	k1gFnSc7	Face
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Seal	Seal	k1gInSc1	Seal
Island	Island	k1gInSc1	Island
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
–	–	k?	–
Irving	Irving	k1gInSc1	Irving
G.	G.	kA	G.
Thalberg	Thalberg	k1gInSc1	Thalberg
Memorial	Memorial	k1gInSc1	Memorial
Award	Award	k1gInSc4	Award
(	(	kIx(	(
<g/>
čestné	čestný	k2eAgNnSc4d1	čestné
uznání	uznání	k1gNnSc4	uznání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Beaver	Beaver	k1gInSc4	Beaver
Valley	Vallea	k1gFnSc2	Vallea
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Nature	Natur	k1gMnSc5	Natur
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Half	halfa	k1gFnPc2	halfa
Acre	Acre	k1gFnSc7	Acre
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Water	Water	k1gInSc1	Water
Birds	Birds	k1gInSc1	Birds
(	(	kIx(	(
<g/>
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Žijící	žijící	k2eAgFnSc4d1	žijící
poušť	poušť	k1gFnSc4	poušť
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
dokumentární	dokumentární	k2eAgMnSc1d1	dokumentární
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Alaskan	Alaskan	k1gMnSc1	Alaskan
Eskimo	Eskima	k1gFnSc5	Eskima
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Toot	Toot	k2eAgMnSc1d1	Toot
Whistle	Whistle	k1gMnSc1	Whistle
Plunk	Plunk	k1gMnSc1	Plunk
and	and	k?	and
Boom	boom	k1gInSc1	boom
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Bear	Bear	k1gInSc4	Bear
Country	country	k2eAgInPc2d1	country
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Živoucí	živoucí	k2eAgFnSc4d1	živoucí
poušť	poušť	k1gFnSc4	poušť
2	[number]	k4	2
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
–	–	k?	–
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
a	a	k8xC	a
krátkometrážní	krátkometrážní	k2eAgInSc4d1	krátkometrážní
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
Men	Men	k1gFnSc1	Men
Against	Against	k1gFnSc1	Against
the	the	k?	the
Arctic	Arctice	k1gFnPc2	Arctice
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
–	–	k?	–
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Winnie	Winnie	k1gFnSc1	Winnie
the	the	k?	the
Pooh	Pooh	k1gInSc1	Pooh
and	and	k?	and
the	the	k?	the
Blustery	Bluster	k1gInPc1	Bluster
Day	Day	k1gFnSc1	Day
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc4d1	další
pocty	pocta	k1gFnPc4	pocta
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
chodníku	chodník	k1gInSc6	chodník
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Anaheimu	Anaheim	k1gInSc6	Anaheim
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
udělena	udělen	k2eAgNnPc4d1	uděleno
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
významný	významný	k2eAgInSc4d1	významný
příspěvek	příspěvek	k1gInSc4	příspěvek
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
za	za	k7c4	za
Disneyland	Disneyland	k1gInSc4	Disneyland
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
součástí	součást	k1gFnSc7	součást
Disneyland	Disneyland	k1gInSc1	Disneyland
Resort	resort	k1gInSc1	resort
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
u	u	k7c2	u
vchodu	vchod	k1gInSc2	vchod
do	do	k7c2	do
Disneylandu	Disneyland	k1gInSc2	Disneyland
na	na	k7c4	na
Harbor	Harbor	k1gInSc4	Harbor
Boulevard	Boulevarda	k1gFnPc2	Boulevarda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
hvězdy	hvězda	k1gFnPc4	hvězda
na	na	k7c6	na
Hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
za	za	k7c4	za
filmy	film	k1gInPc4	film
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
za	za	k7c4	za
televizi	televize	k1gFnSc4	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
obdržel	obdržet	k5eAaPmAgInS	obdržet
Řád	řád	k1gInSc1	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
,	,	kIx,	,
28	[number]	k4	28
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1968	[number]	k4	1968
pak	pak	k6eAd1	pak
v	v	k7c4	v
USA	USA	kA	USA
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
získal	získat	k5eAaPmAgMnS	získat
medaili	medaile	k1gFnSc4	medaile
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
za	za	k7c4	za
vytvoření	vytvoření	k1gNnSc4	vytvoření
Mickey	Mickea	k1gMnSc2	Mickea
Mouse	Mous	k1gMnSc2	Mous
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1964	[number]	k4	1964
přijal	přijmout	k5eAaPmAgMnS	přijmout
Prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
medaili	medaile	k1gFnSc4	medaile
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
ho	on	k3xPp3gNnSc4	on
guvernér	guvernér	k1gMnSc1	guvernér
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
Arnold	Arnold	k1gMnSc1	Arnold
Schwarzenegger	Schwarzenegger	k1gInSc1	Schwarzenegger
a	a	k8xC	a
první	první	k4xOgFnSc1	první
dáma	dáma	k1gFnSc1	dáma
Maria	Maria	k1gFnSc1	Maria
Shriverová	Shriverová	k1gFnSc1	Shriverová
uvedli	uvést	k5eAaPmAgMnP	uvést
do	do	k7c2	do
Kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
planetka	planetka	k1gFnSc1	planetka
4017	[number]	k4	4017
Disneya	Disney	k1gInSc2	Disney
<g/>
,	,	kIx,	,
objevena	objevit	k5eAaPmNgFnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
astronomkou	astronomka	k1gFnSc7	astronomka
Ljudmilou	Ljudmila	k1gFnSc7	Ljudmila
Karačkinou	Karačkina	k1gFnSc7	Karačkina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
byla	být	k5eAaImAgFnS	být
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
otevřena	otevřen	k2eAgFnSc1d1	otevřena
koncertní	koncertní	k2eAgFnSc1d1	koncertní
hala	hala	k1gFnSc1	hala
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
Concert	Concert	k1gMnSc1	Concert
Hall	Hall	k1gMnSc1	Hall
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
začala	začít	k5eAaPmAgFnS	začít
televize	televize	k1gFnSc1	televize
HBO	HBO	kA	HBO
natáčet	natáčet	k5eAaImF	natáčet
jeho	on	k3xPp3gInSc4	on
životopisný	životopisný	k2eAgInSc4d1	životopisný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevysílal	vysílat	k5eNaImAgMnS	vysílat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gFnSc2	Disnea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Walt	Walta	k1gFnPc2	Walta
Disney	Disnea	k1gMnSc2	Disnea
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Disney	Disney	k1gInPc1	Disney
na	na	k7c4	na
tcm	tcm	k?	tcm
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Disney	Disney	k1gInPc1	Disney
na	na	k7c4	na
worldcat	worldcat	k5eAaPmF	worldcat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
The	The	k?	The
Hollywood	Hollywood	k1gInSc1	Hollywood
Blacklist	Blacklist	k1gMnSc1	Blacklist
<g/>
.	.	kIx.	.
www.npr.org	www.npr.org	k1gMnSc1	www.npr.org
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Archivováno	archivovat	k5eAaBmNgNnS	archivovat
11	[number]	k4	11
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
na	na	k7c4	na
Wayback	Wayback	k1gInSc4	Wayback
Machine	Machin	k1gInSc5	Machin
Diskuse	diskuse	k1gFnSc1	diskuse
Disneyeho	Disneye	k1gMnSc2	Disneye
postoje	postoj	k1gInSc2	postoj
k	k	k7c3	k
odborům	odbor	k1gInPc3	odbor
a	a	k8xC	a
komunismu	komunismus	k1gInSc3	komunismus
</s>
</p>
<p>
<s>
Walt	Walt	k1gInSc1	Walt
Disney	Disnea	k1gFnSc2	Disnea
Family	Famila	k1gFnSc2	Famila
Museum	museum	k1gNnSc1	museum
</s>
</p>
<p>
<s>
Neal	Neal	k1gMnSc1	Neal
Gabler	Gabler	k1gMnSc1	Gabler
<g/>
,	,	kIx,	,
Inside	Insid	k1gInSc5	Insid
Walt	Walt	k1gInSc4	Walt
Disney	Disne	k2eAgMnPc4d1	Disne
</s>
</p>
<p>
<s>
Chodník	chodník	k1gInSc1	chodník
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Anaheimu	Anaheim	k1gInSc6	Anaheim
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Disney	Disney	k1gInPc1	Disney
na	na	k7c4	na
findagrave	findagravat	k5eAaPmIp3nS	findagravat
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Fantastické	fantastický	k2eAgNnSc1d1	fantastické
cestování	cestování	k1gNnSc1	cestování
Disneyeho	Disneye	k1gMnSc2	Disneye
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Walta	Walt	k1gMnSc2	Walt
Disneyeho	Disneye	k1gMnSc2	Disneye
</s>
</p>
