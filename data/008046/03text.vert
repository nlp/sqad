<s>
"	"	kIx"	"
<g/>
Křížovka	křížovka	k1gFnSc1	křížovka
je	být	k5eAaImIp3nS	být
luštitelská	luštitelský	k2eAgFnSc1d1	luštitelská
úloha	úloha	k1gFnSc1	úloha
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
křižování	křižování	k1gNnSc6	křižování
výrazů	výraz	k1gInPc2	výraz
vpisovaných	vpisovaný	k2eAgInPc2d1	vpisovaný
do	do	k7c2	do
obrazce	obrazec	k1gInSc2	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
řešení	řešení	k1gNnSc1	řešení
křížovky	křížovka	k1gFnSc2	křížovka
je	být	k5eAaImIp3nS	být
úplně	úplně	k6eAd1	úplně
a	a	k8xC	a
správně	správně	k6eAd1	správně
vyplnit	vyplnit	k5eAaPmF	vyplnit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vytvořit	vytvořit	k5eAaPmF	vytvořit
obrazec	obrazec	k1gInSc4	obrazec
křížovky	křížovka	k1gFnSc2	křížovka
a	a	k8xC	a
nalézt	nalézt	k5eAaPmF	nalézt
tajenku	tajenka	k1gFnSc4	tajenka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
Takto	takto	k6eAd1	takto
definuje	definovat	k5eAaBmIp3nS	definovat
křížovku	křížovka	k1gFnSc4	křížovka
Svaz	svaz	k1gInSc1	svaz
českých	český	k2eAgMnPc2d1	český
hádankářů	hádankář	k1gMnPc2	hádankář
a	a	k8xC	a
křížovkářů	křížovkář	k1gMnPc2	křížovkář
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
"	"	kIx"	"
<g/>
Směrnicích	směrnice	k1gFnPc6	směrnice
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
křížovek	křížovka	k1gFnPc2	křížovka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Základními	základní	k2eAgInPc7d1	základní
prvky	prvek	k1gInPc7	prvek
křížovky	křížovka	k1gFnSc2	křížovka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
název	název	k1gInSc1	název
křížovky	křížovka	k1gFnSc2	křížovka
<g/>
,	,	kIx,	,
legenda	legenda	k1gFnSc1	legenda
<g/>
,	,	kIx,	,
obrazec	obrazec	k1gInSc1	obrazec
<g/>
,	,	kIx,	,
vpisované	vpisovaný	k2eAgInPc1d1	vpisovaný
výrazy	výraz	k1gInPc1	výraz
a	a	k8xC	a
tajenka	tajenka	k1gFnSc1	tajenka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
křížovky	křížovka	k1gFnSc2	křížovka
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
druhu	druh	k1gInSc2	druh
křížovky	křížovka	k1gFnSc2	křížovka
<g/>
,	,	kIx,	,
legenda	legenda	k1gFnSc1	legenda
a	a	k8xC	a
obrazec	obrazec	k1gInSc1	obrazec
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
součástmi	součást	k1gFnPc7	součást
zadání	zadání	k1gNnSc2	zadání
křížovky	křížovka	k1gFnSc2	křížovka
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnSc7	součást
křížovky	křížovka	k1gFnSc2	křížovka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
uvedená	uvedený	k2eAgFnSc1d1	uvedená
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
hádanky	hádanka	k1gFnSc2	hádanka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyluštit	vyluštit	k5eAaPmF	vyluštit
tajenku	tajenka	k1gFnSc4	tajenka
<g/>
.	.	kIx.	.
</s>
<s>
Tajenka	tajenka	k1gFnSc1	tajenka
je	být	k5eAaImIp3nS	být
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
v	v	k7c6	v
políčkách	políčko	k1gNnPc6	políčko
označených	označený	k2eAgFnPc2d1	označená
v	v	k7c6	v
legendě	legenda	k1gFnSc6	legenda
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
obrazci	obrazec	k1gInSc6	obrazec
<g/>
.	.	kIx.	.
</s>
<s>
Tajenka	tajenka	k1gFnSc1	tajenka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
textu	text	k1gInSc6	text
skryta	skryt	k2eAgFnSc1d1	skryta
podle	podle	k7c2	podle
nějakého	nějaký	k3yIgNnSc2	nějaký
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
diagonále	diagonála	k1gFnSc6	diagonála
<g/>
.	.	kIx.	.
</s>
<s>
Tajenka	tajenka	k1gFnSc1	tajenka
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nějaký	nějaký	k3yIgInSc4	nějaký
citát	citát	k1gInSc4	citát
nebo	nebo	k8xC	nebo
vtip	vtip	k1gInSc4	vtip
eventuálně	eventuálně	k6eAd1	eventuálně
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
text	text	k1gInSc1	text
souvisejícího	související	k2eAgInSc2d1	související
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
i	i	k8xC	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
variant	varianta	k1gFnPc2	varianta
těchto	tento	k3xDgFnPc2	tento
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
křížovky	křížovka	k1gFnPc4	křížovka
<g/>
:	:	kIx,	:
S	s	k7c7	s
úplným	úplný	k2eAgNnSc7d1	úplné
křižováním	křižování	k1gNnSc7	křižování
-	-	kIx~	-
všechna	všechen	k3xTgNnPc1	všechen
políčka	políčko	k1gNnPc4	políčko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
výrazů	výraz	k1gInPc2	výraz
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
S	s	k7c7	s
neúplným	úplný	k2eNgNnSc7d1	neúplné
křižováním	křižování	k1gNnSc7	křižování
-	-	kIx~	-
některá	některý	k3yIgNnPc1	některý
políčka	políčko	k1gNnPc4	políčko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
znaky	znak	k1gInPc1	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
výrazů	výraz	k1gInPc2	výraz
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
Koncem	koncem	k7c2	koncem
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
soletí	soletí	k1gNnSc2	soletí
se	se	k3xPyFc4	se
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
různé	různý	k2eAgFnSc2d1	různá
slovní	slovní	k2eAgFnSc2d1	slovní
hříčky	hříčka	k1gFnSc2	hříčka
a	a	k8xC	a
hádanky	hádanka	k1gFnPc1	hádanka
často	často	k6eAd1	často
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
nějakým	nějaký	k3yIgNnSc7	nějaký
grafickým	grafický	k2eAgNnSc7d1	grafické
uspořádáním	uspořádání	k1gNnSc7	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
hříčky	hříčka	k1gFnPc1	hříčka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
slova	slovo	k1gNnPc1	slovo
zapisovaly	zapisovat	k5eAaImAgFnP	zapisovat
křížem	kříž	k1gInSc7	kříž
-	-	kIx~	-
vodorovně	vodorovně	k6eAd1	vodorovně
a	a	k8xC	a
svisle	svisle	k6eAd1	svisle
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
opravdová	opravdový	k2eAgFnSc1d1	opravdová
křížovka	křížovka	k1gFnSc1	křížovka
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1913	[number]	k4	1913
v	v	k7c6	v
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
<s>
Křížovku	křížovka	k1gFnSc4	křížovka
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
jako	jako	k9	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
v	v	k7c6	v
klubovně	klubovna	k1gFnSc6	klubovna
<g/>
,	,	kIx,	,
místnosti	místnost	k1gFnPc1	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
nakreslit	nakreslit	k5eAaPmF	nakreslit
čtverečkovanou	čtverečkovaný	k2eAgFnSc4d1	čtverečkovaná
síť	síť	k1gFnSc4	síť
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
3	[number]	k4	3
a	a	k8xC	a
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vepsat	vepsat	k5eAaPmF	vepsat
slova	slovo	k1gNnPc1	slovo
o	o	k7c6	o
třech	tři	k4xCgNnPc6	tři
písmenech	písmeno	k1gNnPc6	písmeno
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měla	mít	k5eAaImAgFnS	mít
smysl	smysl	k1gInSc4	smysl
vodorovně	vodorovně	k6eAd1	vodorovně
i	i	k9	i
svisle	svisle	k6eAd1	svisle
<g/>
.	.	kIx.	.
</s>
<s>
Tajenku	tajenka	k1gFnSc4	tajenka
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vymýšlet	vymýšlet	k5eAaImF	vymýšlet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
dětí	dítě	k1gFnPc2	dítě
lze	lze	k6eAd1	lze
zadat	zadat	k5eAaPmF	zadat
síť	síť	k1gFnSc4	síť
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
5	[number]	k4	5
<g/>
x	x	k?	x
<g/>
5	[number]	k4	5
Lze	lze	k6eAd1	lze
v	v	k7c6	v
luštění	luštění	k1gNnSc6	luštění
křížovek	křížovka	k1gFnPc2	křížovka
soutěžit	soutěžit	k5eAaImF	soutěžit
<g/>
.	.	kIx.	.
</s>
<s>
Zkopírovat	zkopírovat	k5eAaPmF	zkopírovat
a	a	k8xC	a
namnožit	namnožit	k5eAaPmF	namnožit
stejnou	stejný	k2eAgFnSc4d1	stejná
křížovku	křížovka	k1gFnSc4	křížovka
a	a	k8xC	a
dětem	dítě	k1gFnPc3	dítě
dát	dát	k5eAaPmF	dát
úkol	úkol	k1gInSc4	úkol
ji	on	k3xPp3gFnSc4	on
vyluštit	vyluštit	k5eAaPmF	vyluštit
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yRnSc1	kdo
dříve	dříve	k6eAd2	dříve
úkol	úkol	k1gInSc4	úkol
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
