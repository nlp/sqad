<s>
Luciferiny	luciferin	k1gInPc1	luciferin
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
lucifer	lucifer	k1gMnSc1	lucifer
=	=	kIx~	=
"	"	kIx"	"
<g/>
světlonoš	světlonoš	k1gMnSc1	světlonoš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
lux	lux	k1gInSc1	lux
=	=	kIx~	=
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
fero	fero	k1gNnSc1	fero
=	=	kIx~	=
nesu	nést	k5eAaImIp1nS	nést
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
skupina	skupina	k1gFnSc1	skupina
biologických	biologický	k2eAgInPc2d1	biologický
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
emitovat	emitovat	k5eAaBmF	emitovat
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
světla	světlo	k1gNnSc2	světlo
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
biochemická	biochemický	k2eAgFnSc1d1	biochemická
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
bioluminiscence	bioluminiscence	k1gFnSc1	bioluminiscence
<g/>
.	.	kIx.	.
</s>
