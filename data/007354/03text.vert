<s>
Luciferiny	luciferin	k1gInPc1	luciferin
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
lucifer	lucifer	k1gMnSc1	lucifer
=	=	kIx~	=
"	"	kIx"	"
<g/>
světlonoš	světlonoš	k1gMnSc1	světlonoš
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
lux	lux	k1gInSc1	lux
=	=	kIx~	=
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
fero	fero	k1gNnSc1	fero
=	=	kIx~	=
nesu	nést	k5eAaImIp1nS	nést
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
skupina	skupina	k1gFnSc1	skupina
biologických	biologický	k2eAgInPc2d1	biologický
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
emitovat	emitovat	k5eAaBmF	emitovat
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
světla	světlo	k1gNnSc2	světlo
<g/>
;	;	kIx,	;
tato	tento	k3xDgFnSc1	tento
biochemická	biochemický	k2eAgFnSc1d1	biochemická
reakce	reakce	k1gFnSc1	reakce
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
bioluminiscence	bioluminiscence	k1gFnSc1	bioluminiscence
<g/>
.	.	kIx.	.
</s>
<s>
Luciferiny	luciferin	k1gInPc4	luciferin
jsou	být	k5eAaImIp3nP	být
malomolekulové	malomolekulový	k2eAgInPc4d1	malomolekulový
substráty	substrát	k1gInPc4	substrát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
enzymu	enzym	k1gInSc2	enzym
luciferázy	luciferáza	k1gFnSc2	luciferáza
oxidují	oxidovat	k5eAaBmIp3nP	oxidovat
na	na	k7c4	na
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
oxyluciferin	oxyluciferin	k1gInSc4	oxyluciferin
a	a	k8xC	a
energii	energie	k1gFnSc4	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
luciferinu	luciferin	k1gInSc3	luciferin
koresponduje	korespondovat	k5eAaImIp3nS	korespondovat
jistý	jistý	k2eAgInSc4d1	jistý
enzym	enzym	k1gInSc4	enzym
luciferáza	luciferáza	k1gFnSc1	luciferáza
<g/>
.	.	kIx.	.
</s>
<s>
Luciferinem	luciferin	k1gInSc7	luciferin
disponují	disponovat	k5eAaBmIp3nP	disponovat
<g/>
:	:	kIx,	:
světluškovití	světluškovitý	k2eAgMnPc1d1	světluškovitý
(	(	kIx(	(
<g/>
Lampyridae	Lampyridae	k1gNnSc7	Lampyridae
<g/>
)	)	kIx)	)
některé	některý	k3yIgFnPc1	některý
medúzy	medúza	k1gFnPc1	medúza
(	(	kIx(	(
<g/>
rod	rod	k1gInSc1	rod
Aequorea	Aequorea	k1gFnSc1	Aequorea
<g/>
)	)	kIx)	)
někteří	některý	k3yIgMnPc1	některý
plži	plž	k1gMnPc1	plž
-	-	kIx~	-
konkrétně	konkrétně	k6eAd1	konkrétně
Latia	Latium	k1gNnPc4	Latium
neritoides	neritoidesa	k1gFnPc2	neritoidesa
obrněnka	obrněnka	k1gFnSc1	obrněnka
rodu	rod	k1gInSc2	rod
svítilka	svítilka	k1gFnSc1	svítilka
některé	některý	k3yIgFnSc2	některý
bakterie	bakterie	k1gFnSc2	bakterie
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Luciferin	luciferin	k1gInSc1	luciferin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
www.lifesci.ucsb.edu/~biolum/chem/detail1.html	www.lifesci.ucsb.edu/~biolum/chem/detail1.htmnout	k5eAaPmAgMnS	www.lifesci.ucsb.edu/~biolum/chem/detail1.htmnout
</s>
