<s>
Internetový	internetový	k2eAgInSc1d1
časopis	časopis	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
online	onlinout	k5eAaPmIp3nS
magazín	magazín	k1gInSc4
nebo	nebo	k8xC
zkráceně	zkráceně	k6eAd1
e-zin	e-zin	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
„	„	k?
<g/>
elektronického	elektronický	k2eAgInSc2d1
magazínu	magazín	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
časopis	časopis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
čtenářům	čtenář	k1gMnPc3
on-line	on-lin	k1gInSc5
prostřednictvím	prostřednictví	k1gNnSc7
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
</s>