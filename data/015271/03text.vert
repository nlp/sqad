<s>
Internetový	internetový	k2eAgInSc1d1
časopis	časopis	k1gInSc1
</s>
<s>
Internetový	internetový	k2eAgInSc1d1
časopis	časopis	k1gInSc1
<g/>
,	,	kIx,
též	též	k9
online	onlinout	k5eAaPmIp3nS
magazín	magazín	k1gInSc4
nebo	nebo	k8xC
zkráceně	zkráceně	k6eAd1
e-zin	e-zin	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
„	„	k?
<g/>
elektronického	elektronický	k2eAgInSc2d1
magazínu	magazín	k1gInSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
časopis	časopis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
dostupný	dostupný	k2eAgInSc1d1
čtenářům	čtenář	k1gMnPc3
on-line	on-lin	k1gInSc5
prostřednictvím	prostřednictví	k1gNnSc7
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
a	a	k8xC
obsah	obsah	k1gInSc1
</s>
<s>
Internetové	internetový	k2eAgInPc1d1
časopisy	časopis	k1gInPc1
vznikly	vzniknout	k5eAaPmAgInP
spojením	spojení	k1gNnSc7
tištěných	tištěný	k2eAgNnPc2d1
a	a	k8xC
internetových	internetový	k2eAgNnPc2d1
medií	medium	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
rozdílem	rozdíl	k1gInSc7
oproti	oproti	k7c3
klasickým	klasický	k2eAgInPc3d1
tištěným	tištěný	k2eAgInPc3d1
časopisům	časopis	k1gInPc3
je	být	k5eAaImIp3nS
interaktivnost	interaktivnost	k1gFnSc4
a	a	k8xC
takřka	takřka	k6eAd1
neomezený	omezený	k2eNgInSc4d1
obsah	obsah	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
limitován	limitován	k2eAgInSc1d1
pouze	pouze	k6eAd1
datovou	datový	k2eAgFnSc7d1
kapacitou	kapacita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prohlížení	prohlížení	k1gNnSc3
internetových	internetový	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
připojení	připojení	k1gNnSc4
k	k	k7c3
internetu	internet	k1gInSc3
a	a	k8xC
často	často	k6eAd1
také	také	k9
aktuální	aktuální	k2eAgFnSc1d1
verze	verze	k1gFnSc1
flash	flasha	k1gFnPc2
playeru	playrat	k5eAaPmIp1nS
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
obsahové	obsahový	k2eAgFnSc2d1
stánky	stánka	k1gFnSc2
týče	týkat	k5eAaImIp3nS
<g/>
,	,	kIx,
nabízí	nabízet	k5eAaImIp3nS
možnosti	možnost	k1gFnPc1
využití	využití	k1gNnSc2
vizuálních	vizuální	k2eAgInPc2d1
i	i	k8xC
audiovizuálních	audiovizuální	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
dovoluje	dovolovat	k5eAaImIp3nS
tudíž	tudíž	k8xC
články	článek	k1gInPc4
prokládat	prokládat	k5eAaImF
různými	různý	k2eAgInPc7d1
videi	vide	k1gInPc7
<g/>
,	,	kIx,
zvukovými	zvukový	k2eAgInPc7d1
soubory	soubor	k1gInPc7
nebo	nebo	k8xC
aplikacemi	aplikace	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtenáři	čtenář	k1gMnPc7
je	být	k5eAaImIp3nS
tak	tak	k9
možné	možný	k2eAgNnSc1d1
poskytnout	poskytnout	k5eAaPmF
nejen	nejen	k6eAd1
textovou	textový	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
např.	např.	kA
k	k	k7c3
recenzi	recenze	k1gFnSc3
nového	nový	k2eAgInSc2d1
automobilu	automobil	k1gInSc2
připojit	připojit	k5eAaPmF
i	i	k9
videozáznam	videozáznam	k1gInSc4
z	z	k7c2
testovací	testovací	k2eAgFnSc2d1
jízdy	jízda	k1gFnSc2
<g/>
,	,	kIx,
popř.	popř.	kA
záznam	záznam	k1gInSc4
zvuku	zvuk	k1gInSc2
motoru	motor	k1gInSc2
pří	pře	k1gFnPc2
zvýšených	zvýšený	k2eAgFnPc6d1
otáčkách	otáčka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Žurnál	žurnál	k1gInSc1
</s>
<s>
Magazín	magazín	k1gInSc1
</s>
<s>
Noviny	novina	k1gFnPc1
</s>
<s>
Bulletin	bulletin	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
