<s>
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Ostravice	Ostravice	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
17,92	17,92	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
53,58	53,58	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
100962	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
v	v	k7c6
Ostravici	Ostravice	k1gFnSc6
spravuje	spravovat	k5eAaImIp3nS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Památník	památník	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
v	v	k7c6
Opavě	Opava	k1gFnSc6
<g/>
,	,	kIx,
Slezské	slezský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
instalovaný	instalovaný	k2eAgInSc4d1
objekt	objekt	k1gInSc4
<g/>
,	,	kIx,
u	u	k7c2
nějž	jenž	k3xRgNnSc2
je	být	k5eAaImIp3nS
důraz	důraz	k1gInSc1
kladen	klást	k5eAaImNgInS
na	na	k7c4
zachování	zachování	k1gNnSc4
autentické	autentický	k2eAgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
srubu	srubat	k5eAaPmIp1nS
z	z	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
něm	on	k3xPp3gNnSc6
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
pobýval	pobývat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2004	#num#	k4
je	být	k5eAaImIp3nS
srub	srub	k1gInSc1
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
-	-	kIx~
interiér	interiér	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
1958	#num#	k4
<g/>
,	,	kIx,
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Vladimír	Vladimír	k1gMnSc1
Vašek	Vašek	k1gMnSc1
<g/>
)	)	kIx)
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
představitelům	představitel	k1gMnPc3
české	český	k2eAgFnSc2d1
poezie	poezie	k1gFnSc2
přelomu	přelom	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
literatury	literatura	k1gFnSc2
vstoupil	vstoupit	k5eAaPmAgMnS
svou	svůj	k3xOyFgFnSc7
jedinou	jediný	k2eAgFnSc7d1
sbírkou	sbírka	k1gFnSc7
Slezské	slezský	k2eAgFnSc2d1
písně	píseň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimečné	výjimečný	k2eAgInPc1d1
verše	verš	k1gInPc1
nemají	mít	k5eNaImIp3nP
svým	svůj	k3xOyFgInSc7
stylem	styl	k1gInSc7
v	v	k7c6
české	český	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
obdobu	obdoba	k1gFnSc4
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
Bezručův	Bezručův	k2eAgInSc1d1
exaltovaný	exaltovaný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
tvorby	tvorba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
je	být	k5eAaImIp3nS
literárními	literární	k2eAgMnPc7d1
historiky	historik	k1gMnPc7
řazen	řadit	k5eAaImNgMnS
do	do	k7c2
generace	generace	k1gFnSc2
tzv.	tzv.	kA
anarchistických	anarchistický	k2eAgMnPc2d1
buřičů	buřič	k1gMnPc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gMnPc4
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
ovlivněno	ovlivnit	k5eAaPmNgNnS
symbolismem	symbolismus	k1gInSc7
a	a	k8xC
Českou	český	k2eAgFnSc7d1
modernou	moderna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Fojtství	fojtství	k1gNnSc1
ve	v	k7c6
Starých	Starých	k2eAgInPc6d1
Hamrech	Hamry	k1gInPc6
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
Ostravice	Ostravice	k1gFnSc1
<g/>
)	)	kIx)
Petr	Petr	k1gMnSc1
Bezruč	Bezruč	k1gMnSc1
zakoupil	zakoupit	k5eAaPmAgMnS
společně	společně	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
<g/>
,	,	kIx,
básníkem	básník	k1gMnSc7
Otakarem	Otakar	k1gMnSc7
Bystřinou	bystřina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezručovi	Bezruč	k1gMnSc3
se	se	k3xPyFc4
místo	místo	k1gNnSc1
líbilo	líbit	k5eAaImAgNnS
hlavně	hlavně	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
nedaleko	nedaleko	k7c2
domu	dům	k1gInSc2
tekla	téct	k5eAaImAgFnS
Ostravice	Ostravice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezruč	Bezruč	k1gMnSc1
miloval	milovat	k5eAaImAgMnS
vodu	voda	k1gFnSc4
a	a	k8xC
nikdy	nikdy	k6eAd1
neodolal	odolat	k5eNaPmAgMnS
příležitosti	příležitost	k1gFnPc4
se	se	k3xPyFc4
v	v	k7c6
ledové	ledový	k2eAgFnSc6d1
řece	řeka	k1gFnSc6
vykoupat	vykoupat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Básníci	básník	k1gMnPc1
si	se	k3xPyFc3
vybudovali	vybudovat	k5eAaPmAgMnP
ze	z	k7c2
starého	starý	k2eAgNnSc2d1
fojtství	fojtství	k1gNnSc2
letovisko	letovisko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
Bystřinově	bystřinově	k6eAd1
smrti	smrt	k1gFnSc2
však	však	k9
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozdělení	rozdělení	k1gNnSc3
společného	společný	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petru	Petr	k1gMnSc3
Bezručovi	Bezruč	k1gMnSc3
připadla	připadnout	k5eAaPmAgFnS
stodola	stodola	k1gFnSc1
někdejšího	někdejší	k2eAgNnSc2d1
fojtství	fojtství	k1gNnSc2
<g/>
,	,	kIx,
část	část	k1gFnSc1
zahrady	zahrada	k1gFnSc2
<g/>
,	,	kIx,
paseka	paseka	k1gFnSc1
a	a	k8xC
les	les	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pomocí	pomoc	k1gFnSc7
přátel	přítel	k1gMnPc2
nechal	nechat	k5eAaPmAgMnS
na	na	k7c6
počátku	počátek	k1gInSc6
30	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přebudovat	přebudovat	k5eAaPmF
stodolu	stodola	k1gFnSc4
dle	dle	k7c2
vlastního	vlastní	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
na	na	k7c4
obytný	obytný	k2eAgInSc4d1
srub	srub	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zde	zde	k6eAd1
stojí	stát	k5eAaImIp3nS
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gFnSc2
-	-	kIx~
interiér	interiér	k1gInSc1
2	#num#	k4
</s>
<s>
Současná	současný	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
</s>
<s>
Krajem	krajem	k6eAd1
Petra	Petra	k1gFnSc1
Bezruče	Bezruč	k1gFnSc2
</s>
<s>
Expozice	expozice	k1gFnSc1
srubu	srubat	k5eAaPmIp1nS
Petra	Petra	k1gFnSc1
Bezruče	Bezruč	k1gFnSc2
prezentuje	prezentovat	k5eAaBmIp3nS
autentické	autentický	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
letního	letní	k2eAgNnSc2d1
obydlí	obydlí	k1gNnSc2
básníka	básník	k1gMnSc2
<g/>
,	,	kIx,
dotýká	dotýkat	k5eAaImIp3nS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInPc2
životních	životní	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
a	a	k8xC
kraje	kraj	k1gInSc2
Beskyd	Beskydy	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
prohlídku	prohlídka	k1gFnSc4
obohatí	obohatit	k5eAaPmIp3nS
tištěný	tištěný	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
k	k	k7c3
zapůjčení	zapůjčení	k1gNnSc3
při	při	k7c6
koupi	koupě	k1gFnSc6
vstupenek	vstupenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc4
Bezruče	Bezruč	k1gInSc2
na	na	k7c4
Ostravici	Ostravice	k1gFnSc4
získalo	získat	k5eAaPmAgNnS
Slezské	slezský	k2eAgNnSc1d1
zemské	zemský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
1958	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
dědictví	dědictví	k1gNnSc2
po	po	k7c6
básníku	básník	k1gMnSc6
Petru	Petr	k1gMnSc6
Bezručovi	Bezruč	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obytná	obytný	k2eAgFnSc1d1
část	část	k1gFnSc1
srubu	srub	k1gInSc2
byla	být	k5eAaImAgFnS
ponechána	ponechat	k5eAaPmNgFnS
v	v	k7c6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
i	i	k9
se	s	k7c7
zařízením	zařízení	k1gNnSc7
interiéru	interiér	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
si	se	k3xPyFc3
Bezruč	Bezruč	k1gMnSc1
sám	sám	k3xTgMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přilehlém	přilehlý	k2eAgInSc6d1
dřevníku	dřevník	k1gInSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
zbudována	zbudován	k2eAgFnSc1d1
expozice	expozice	k1gFnSc1
s	s	k7c7
názvem	název	k1gInSc7
Krajem	krajem	k6eAd1
Petra	Petra	k1gFnSc1
Bezruče	Bezruč	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návštěvník	návštěvník	k1gMnSc1
srubu	srub	k1gInSc2
zde	zde	k6eAd1
nalezne	naleznout	k5eAaPmIp3nS,k5eAaBmIp3nS
nejen	nejen	k6eAd1
autentické	autentický	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
letního	letní	k2eAgInSc2d1
příbytku	příbytek	k1gInSc2
básníka	básník	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nutně	nutně	k6eAd1
chodí	chodit	k5eAaImIp3nP
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
stopách	stopa	k1gFnPc6
<g/>
,	,	kIx,
dotýká	dotýkat	k5eAaImIp3nS
se	se	k3xPyFc4
životních	životní	k2eAgInPc2d1
příběhů	příběh	k1gInPc2
básníka	básník	k1gMnSc2
a	a	k8xC
především	především	k6eAd1
může	moct	k5eAaImIp3nS
poznat	poznat	k5eAaPmF
prostředí	prostředí	k1gNnSc4
kraje	kraj	k1gInSc2
pod	pod	k7c7
Beskydem	Beskyd	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
osud	osud	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
minulosti	minulost	k1gFnSc6
tak	tak	k6eAd1
svízelný	svízelný	k2eAgMnSc1d1
<g/>
,	,	kIx,
zachytil	zachytit	k5eAaPmAgMnS
básník	básník	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
stěžejním	stěžejní	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
–	–	k?
ve	v	k7c6
Slezských	slezský	k2eAgFnPc6d1
písních	píseň	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ústřední	ústřední	k2eAgInSc4d1
seznam	seznam	k1gInSc4
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Identifikátor	identifikátor	k1gInSc1
záznamu	záznam	k1gInSc2
164299601	#num#	k4
:	:	kIx,
Jiná	jiný	k2eAgFnSc1d1
obytná	obytný	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
–	–	k?
Srub	srub	k1gInSc4
Petra	Petr	k1gMnSc2
Bezruče	Bezruč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památkový	památkový	k2eAgInSc1d1
katalog	katalog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hledat	hledat	k5eAaImF
dokumenty	dokument	k1gInPc4
v	v	k7c6
Metainformačním	Metainformační	k2eAgInSc6d1
systému	systém	k1gInSc6
NPÚ	NPÚ	kA
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Srub	srub	k1gInSc1
Petra	Petr	k1gMnSc4
Bezruče	Bezruč	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Stránky	stránka	k1gFnPc1
muzea	muzeum	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
4869147312829137970002	#num#	k4
</s>
