<p>
<s>
Stuha	stuha	k1gFnSc1	stuha
či	či	k8xC	či
stužka	stužka	k1gFnSc1	stužka
je	být	k5eAaImIp3nS	být
úzká	úzký	k2eAgFnSc1d1	úzká
plošná	plošný	k2eAgFnSc1d1	plošná
textilie	textilie	k1gFnSc1	textilie
s	s	k7c7	s
pevnými	pevný	k2eAgInPc7d1	pevný
kraji	kraj	k1gInPc7	kraj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Definice	definice	k1gFnPc1	definice
stuhy	stuha	k1gFnPc1	stuha
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
definicí	definice	k1gFnPc2	definice
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
označení	označení	k1gNnSc1	označení
stuha	stuha	k1gFnSc1	stuha
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
plošné	plošný	k2eAgFnPc1d1	plošná
textilie	textilie	k1gFnPc1	textilie
v	v	k7c6	v
šířce	šířka	k1gFnSc6	šířka
do	do	k7c2	do
cca	cca	kA	cca
50	[number]	k4	50
cm	cm	kA	cm
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
prýmků	prýmek	k1gInPc2	prýmek
<g/>
.	.	kIx.	.
</s>
<s>
Prýmek	prýmek	k1gInSc1	prýmek
se	se	k3xPyFc4	se
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
stuhy	stuha	k1gFnSc2	stuha
jen	jen	k9	jen
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nevyrábí	vyrábět	k5eNaImIp3nS	vyrábět
na	na	k7c6	na
tkacích	tkací	k2eAgInPc6d1	tkací
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
stuha	stuha	k1gFnSc1	stuha
nedá	dát	k5eNaPmIp3nS	dát
přímo	přímo	k6eAd1	přímo
překládat	překládat	k5eAaImF	překládat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
nepoužívá	používat	k5eNaImIp3nS	používat
jako	jako	k8xC	jako
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
může	moct	k5eAaImIp3nS	moct
stuha	stuha	k1gFnSc1	stuha
znamenat	znamenat	k5eAaImF	znamenat
tape	tape	k1gFnSc4	tape
<g/>
,	,	kIx,	,
ribbon	ribbon	k1gInSc4	ribbon
<g/>
,	,	kIx,	,
belt	belt	k1gInSc4	belt
nebo	nebo	k8xC	nebo
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
Litze	Litze	k1gFnSc2	Litze
<g/>
,	,	kIx,	,
Gurt	Gurt	k?	Gurt
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
výroby	výroba	k1gFnSc2	výroba
úzkých	úzký	k2eAgFnPc2d1	úzká
textilií	textilie	k1gFnPc2	textilie
není	být	k5eNaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Nigérie	Nigérie	k1gFnSc2	Nigérie
a	a	k8xC	a
Kamerunu	Kamerun	k1gInSc2	Kamerun
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
tkaly	tkát	k5eAaImAgFnP	tkát
stuhy	stuha	k1gFnPc1	stuha
na	na	k7c6	na
stavech	stav	k1gInPc6	stav
"	"	kIx"	"
<g/>
ground	ground	k1gMnSc1	ground
loom	loom	k1gMnSc1	loom
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
raphia	raphia	k1gFnSc1	raphia
loom	loom	k1gMnSc1	loom
<g/>
"	"	kIx"	"
už	už	k6eAd1	už
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
Nástroje	nástroj	k1gInSc2	nástroj
podobné	podobný	k2eAgFnSc2d1	podobná
konstrukce	konstrukce	k1gFnSc2	konstrukce
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
používaly	používat	k5eAaImAgInP	používat
ještě	ještě	k6eAd1	ještě
i	i	k9	i
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
řemeslné	řemeslný	k2eAgFnSc6d1	řemeslná
výrobě	výroba	k1gFnSc6	výroba
stuh	stuha	k1gFnPc2	stuha
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
St.	st.	kA	st.
<g/>
Etienne	Etienn	k1gInSc5	Etienn
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
rok	rok	k1gInSc4	rok
1549	[number]	k4	1549
jako	jako	k8xC	jako
počátek	počátek	k1gInSc1	počátek
tkaní	tkaní	k1gNnSc1	tkaní
stuh	stuha	k1gFnPc2	stuha
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
NDR	NDR	kA	NDR
zabývalo	zabývat	k5eAaImAgNnS	zabývat
výrobou	výroba	k1gFnSc7	výroba
asi	asi	k9	asi
3500	[number]	k4	3500
ročních	roční	k2eAgFnPc2d1	roční
tun	tuna	k1gFnPc2	tuna
úzkých	úzký	k2eAgFnPc2d1	úzká
textilií	textilie	k1gFnPc2	textilie
6000	[number]	k4	6000
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c6	na
3600	[number]	k4	3600
tkacích	tkací	k2eAgInPc6d1	tkací
strojích	stroj	k1gInPc6	stroj
(	(	kIx(	(
<g/>
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
rozsah	rozsah	k1gInSc1	rozsah
výroby	výroba	k1gFnSc2	výroba
asi	asi	k9	asi
poloviční	poloviční	k2eAgFnSc1d1	poloviční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
stuh	stuha	k1gFnPc2	stuha
známá	známý	k2eAgFnSc1d1	známá
od	od	k7c2	od
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
zbylo	zbýt	k5eAaPmAgNnS	zbýt
po	po	k7c6	po
značném	značný	k2eAgNnSc6d1	značné
snížení	snížení	k1gNnSc6	snížení
kapacit	kapacita	k1gFnPc2	kapacita
asi	asi	k9	asi
4-5	[number]	k4	4-5
výrobců	výrobce	k1gMnPc2	výrobce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zaměstnávali	zaměstnávat	k5eAaImAgMnP	zaměstnávat
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
Šluknovsku	Šluknovsko	k1gNnSc6	Šluknovsko
a	a	k8xC	a
v	v	k7c6	v
Krnově	Krnov	k1gInSc6	Krnov
<g/>
)	)	kIx)	)
cca	cca	kA	cca
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
vyráběném	vyráběný	k2eAgNnSc6d1	vyráběné
množství	množství	k1gNnSc6	množství
stuh	stuha	k1gFnPc2	stuha
nejsou	být	k5eNaImIp3nP	být
zveřejňovány	zveřejňován	k2eAgInPc1d1	zveřejňován
ani	ani	k8xC	ani
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
ani	ani	k8xC	ani
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stuhové	stuhový	k2eAgInPc1d1	stuhový
tkací	tkací	k2eAgInPc1d1	tkací
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
výnosu	výnos	k1gInSc6	výnos
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
textilních	textilní	k2eAgInPc2d1	textilní
strojů	stroj	k1gInPc2	stroj
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	s	k7c7	s
400	[number]	k4	400
miliony	milion	k4xCgInPc4	milion
USD	USD	kA	USD
cca	cca	kA	cca
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
stuh	stuha	k1gFnPc2	stuha
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
tkaním	tkaní	k1gNnSc7	tkaní
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
pletením	pletení	k1gNnSc7	pletení
<g/>
,	,	kIx,	,
splétáním	splétání	k1gNnSc7	splétání
nebo	nebo	k8xC	nebo
lepením	lepení	k1gNnSc7	lepení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
příze	příz	k1gFnPc1	příz
z	z	k7c2	z
nekonečných	konečný	k2eNgFnPc2d1	nekonečná
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
z	z	k7c2	z
přírodního	přírodní	k2eAgNnSc2d1	přírodní
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
technické	technický	k2eAgInPc4d1	technický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
příze	příz	k1gFnPc4	příz
ze	z	k7c2	z
skleněných	skleněný	k2eAgNnPc2d1	skleněné
a	a	k8xC	a
uhlíkových	uhlíkový	k2eAgNnPc2d1	uhlíkové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
i	i	k8xC	i
směsi	směs	k1gFnPc4	směs
filamentů	filament	k1gInPc2	filament
s	s	k7c7	s
dráty	drát	k1gInPc7	drát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
stuh	stuha	k1gFnPc2	stuha
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
elastomerů	elastomer	k1gInPc2	elastomer
<g/>
,	,	kIx,	,
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
účelům	účel	k1gInPc3	účel
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
příze	příz	k1gFnPc1	příz
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tkané	tkaný	k2eAgFnPc1d1	tkaná
stuhy	stuha	k1gFnPc1	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
Tkané	tkaný	k2eAgFnPc1d1	tkaná
stuhy	stuha	k1gFnPc1	stuha
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nP	vyrábět
na	na	k7c6	na
člunkových	člunkový	k2eAgInPc6d1	člunkový
nebo	nebo	k8xC	nebo
jehlových	jehlový	k2eAgInPc6d1	jehlový
stavech	stav	k1gInPc6	stav
a	a	k8xC	a
také	také	k9	také
ručně	ručně	k6eAd1	ručně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výroba	výroba	k1gFnSc1	výroba
na	na	k7c6	na
člunkových	člunkový	k2eAgInPc6d1	člunkový
strojích	stroj	k1gInPc6	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Ploché	plochý	k2eAgFnPc1d1	plochá
stuhy	stuha	k1gFnPc1	stuha
se	se	k3xPyFc4	se
tkají	tkát	k5eAaImIp3nP	tkát
většinou	většinou	k6eAd1	většinou
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
člunkem	člunek	k1gInSc7	člunek
při	při	k7c6	při
max	max	kA	max
<g/>
.	.	kIx.	.
300	[number]	k4	300
prohozech	prohoz	k1gInPc6	prohoz
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
extrémní	extrémní	k2eAgFnSc6d1	extrémní
hustotě	hustota	k1gFnSc6	hustota
až	až	k9	až
220	[number]	k4	220
útků	útek	k1gInPc2	útek
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc4	stroj
s	s	k7c7	s
žakárovým	žakárový	k2eAgNnSc7d1	žakárové
ústrojím	ústrojí	k1gNnSc7	ústrojí
mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
mít	mít	k5eAaImF	mít
i	i	k9	i
osm	osm	k4xCc4	osm
soustav	soustava	k1gFnPc2	soustava
k	k	k7c3	k
zanášení	zanášení	k1gNnSc3	zanášení
útku	útek	k1gInSc2	útek
(	(	kIx(	(
<g/>
na	na	k7c6	na
snímku	snímek	k1gInSc6	snímek
vpravo	vpravo	k6eAd1	vpravo
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
takového	takový	k3xDgInSc2	takový
stroje	stroj	k1gInSc2	stroj
z	z	k7c2	z
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
stroji	stroj	k1gInSc6	stroj
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
1-6	[number]	k4	1-6
(	(	kIx(	(
<g/>
i	i	k8xC	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
stuh	stuha	k1gFnPc2	stuha
v	v	k7c6	v
šíři	šíř	k1gFnSc6	šíř
0,5	[number]	k4	0,5
až	až	k8xS	až
cca	cca	kA	cca
30	[number]	k4	30
cm	cm	kA	cm
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Kraje	kraj	k1gInPc1	kraj
tkaniny	tkanina	k1gFnSc2	tkanina
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
výrobků	výrobek	k1gInPc2	výrobek
zpevňovat	zpevňovat	k5eAaImF	zpevňovat
tzv.	tzv.	kA	tzv.
dutou	dutý	k2eAgFnSc7d1	dutá
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
dutinných	dutinný	k2eAgFnPc2d1	dutinný
(	(	kIx(	(
<g/>
hadicovitých	hadicovitý	k2eAgFnPc2d1	hadicovitý
<g/>
)	)	kIx)	)
stuh	stuha	k1gFnPc2	stuha
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
stroje	stroj	k1gInPc1	stroj
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
osnovami	osnova	k1gFnPc7	osnova
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
systémy	systém	k1gInPc1	systém
zanášení	zanášení	k1gNnSc2	zanášení
útku	útka	k1gFnSc4	útka
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Útky	útek	k1gInPc1	útek
jsou	být	k5eAaImIp3nP	být
zanášeny	zanášet	k5eAaImNgInP	zanášet
střídavě	střídavě	k6eAd1	střídavě
do	do	k7c2	do
horní	horní	k2eAgFnSc2d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc2d1	dolní
osnovy	osnova	k1gFnSc2	osnova
a	a	k8xC	a
tak	tak	k6eAd1	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
bezešvá	bezešvý	k2eAgFnSc1d1	bezešvá
hadice	hadice	k1gFnSc1	hadice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
nových	nový	k2eAgFnPc2d1	nová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
technických	technický	k2eAgFnPc2d1	technická
stuh	stuha	k1gFnPc2	stuha
(	(	kIx(	(
<g/>
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
cévní	cévní	k2eAgFnPc1d1	cévní
chirurgie	chirurgie	k1gFnPc1	chirurgie
<g/>
,	,	kIx,	,
stuhy	stuha	k1gFnPc1	stuha
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
Y	Y	kA	Y
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
možné	možný	k2eAgNnSc1d1	možné
použití	použití	k1gNnSc1	použití
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
také	také	k9	také
3D	[number]	k4	3D
tkaniny	tkanina	k1gFnPc4	tkanina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výroba	výroba	k1gFnSc1	výroba
na	na	k7c6	na
jehlových	jehlový	k2eAgInPc6d1	jehlový
strojích	stroj	k1gInPc6	stroj
===	===	k?	===
</s>
</p>
<p>
<s>
Jehlové	jehlový	k2eAgInPc1d1	jehlový
stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
měřítku	měřítko	k1gNnSc6	měřítko
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Útek	útek	k1gInSc1	útek
se	se	k3xPyFc4	se
zanáší	zanášet	k5eAaImIp3nS	zanášet
do	do	k7c2	do
osnovy	osnova	k1gFnSc2	osnova
jednou	jednou	k6eAd1	jednou
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgFnPc7	dva
jehlami	jehla	k1gFnPc7	jehla
rychlostí	rychlost	k1gFnSc7	rychlost
asi	asi	k9	asi
do	do	k7c2	do
1300	[number]	k4	1300
prohozů	prohoz	k1gInPc2	prohoz
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kraje	kraj	k1gInPc1	kraj
stuhy	stuha	k1gFnSc2	stuha
se	se	k3xPyFc4	se
zpevňují	zpevňovat	k5eAaImIp3nP	zpevňovat
zpravidla	zpravidla	k6eAd1	zpravidla
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
vzájemným	vzájemný	k2eAgNnSc7d1	vzájemné
propletením	propletení	k1gNnSc7	propletení
dvou	dva	k4xCgInPc2	dva
útků	útek	k1gInPc2	útek
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
nebo	nebo	k8xC	nebo
s	s	k7c7	s
vaznou	vazný	k2eAgFnSc7d1	vazná
nití	nit	k1gFnSc7	nit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
jazýčkové	jazýčkový	k2eAgFnSc2d1	jazýčková
jehly	jehla	k1gFnSc2	jehla
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc4	stroj
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
úzce	úzko	k6eAd1	úzko
specializované	specializovaný	k2eAgInPc1d1	specializovaný
pro	pro	k7c4	pro
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
stuh	stuha	k1gFnPc2	stuha
(	(	kIx(	(
<g/>
elastické	elastický	k2eAgFnPc1d1	elastická
stuhy	stuha	k1gFnPc1	stuha
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgInPc1d1	těžký
popruhy	popruh	k1gInPc1	popruh
<g/>
,	,	kIx,	,
suché	suchý	k2eAgInPc1d1	suchý
zipy	zip	k1gInPc1	zip
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
|	|	kIx~	|
<g/>
Stuhy	stuha	k1gFnPc1	stuha
se	se	k3xPyFc4	se
tkají	tkát	k5eAaImIp3nP	tkát
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
základních	základní	k2eAgFnPc6d1	základní
vazbách	vazba	k1gFnPc6	vazba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgMnPc2	který
často	často	k6eAd1	často
nazývají	nazývat	k5eAaImIp3nP	nazývat
i	i	k9	i
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
styku	styk	k1gInSc6	styk
(	(	kIx(	(
<g/>
plátnovka	plátnovka	k1gFnSc1	plátnovka
<g/>
,	,	kIx,	,
keprovka	keprovka	k1gFnSc1	keprovka
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ručně	ručně	k6eAd1	ručně
tkané	tkaný	k2eAgFnPc1d1	tkaná
stuhy	stuha	k1gFnPc1	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
se	s	k7c7	s
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
třemi	tři	k4xCgFnPc7	tři
známými	známý	k2eAgFnPc7d1	známá
technikami	technika	k1gFnPc7	technika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
hřebene	hřeben	k1gInSc2	hřeben
(	(	kIx(	(
<g/>
paprsku	paprsek	k1gInSc2	paprsek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tkaním	tkaní	k1gNnSc7	tkaní
na	na	k7c6	na
karetkách	karetka	k1gFnPc6	karetka
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
šikmou	šikma	k1gFnSc7	šikma
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řezané	řezaný	k2eAgFnPc1d1	řezaná
stuhy	stuha	k1gFnPc1	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
Laciné	laciný	k2eAgFnPc1d1	laciná
stuhy	stuha	k1gFnPc1	stuha
a	a	k8xC	a
štítky	štítek	k1gInPc1	štítek
se	se	k3xPyFc4	se
také	také	k9	také
zhotovují	zhotovovat	k5eAaImIp3nP	zhotovovat
řezáním	řezání	k1gNnSc7	řezání
široké	široký	k2eAgFnSc2d1	široká
tkaniny	tkanina	k1gFnSc2	tkanina
ze	z	k7c2	z
syntetických	syntetický	k2eAgInPc2d1	syntetický
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
osnovy	osnova	k1gFnSc2	osnova
<g/>
)	)	kIx)	)
na	na	k7c4	na
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
řezání	řezání	k1gNnSc2	řezání
se	se	k3xPyFc4	se
nataví	natavit	k5eAaPmIp3nS	natavit
útkové	útkový	k2eAgFnSc2d1	útková
niti	nit	k1gFnSc2	nit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zabrání	zabránit	k5eAaPmIp3nS	zabránit
párání	párání	k1gNnSc4	párání
krajů	kraj	k1gInPc2	kraj
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stroje	stroj	k1gInPc4	stroj
s	s	k7c7	s
jehlovým	jehlový	k2eAgInSc7d1	jehlový
prohozem	prohoz	k1gInSc7	prohoz
útku	útek	k1gInSc2	útek
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
výkonu	výkon	k1gInSc3	výkon
do	do	k7c2	do
1000	[number]	k4	1000
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc1	stroj
s	s	k7c7	s
pneumatickým	pneumatický	k2eAgInSc7d1	pneumatický
prohozem	prohoz	k1gInSc7	prohoz
až	až	k9	až
1140	[number]	k4	1140
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
</s>
</p>
<p>
<s>
===	===	k?	===
Pletené	pletený	k2eAgFnPc1d1	pletená
stuhy	stuha	k1gFnPc1	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
na	na	k7c6	na
osnovních	osnovní	k2eAgInPc6d1	osnovní
stávcích	stávec	k1gInPc6	stávec
<g/>
,	,	kIx,	,
na	na	k7c6	na
zátažných	zátažný	k2eAgInPc6d1	zátažný
strojích	stroj	k1gInPc6	stroj
nebo	nebo	k8xC	nebo
i	i	k9	i
"	"	kIx"	"
<g/>
háčkují	háčkovat	k5eAaImIp3nP	háčkovat
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
na	na	k7c6	na
galonových	galonův	k2eAgInPc6d1	galonův
stávcích	stávec	k1gInPc6	stávec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Splétané	splétaný	k2eAgFnPc1d1	splétaná
stuhy	stuha	k1gFnPc1	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vyrábět	vyrábět	k5eAaImF	vyrábět
na	na	k7c6	na
jemných	jemný	k2eAgInPc6d1	jemný
splétacích	splétací	k2eAgInPc6d1	splétací
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lepené	lepený	k2eAgFnPc1d1	lepená
stuhy	stuha	k1gFnPc1	stuha
===	===	k?	===
</s>
</p>
<p>
<s>
Lepením	lepení	k1gNnSc7	lepení
nití	nit	k1gFnSc7	nit
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
podklad	podklad	k1gInSc4	podklad
a	a	k8xC	a
rozdělením	rozdělení	k1gNnSc7	rozdělení
na	na	k7c4	na
pásky	páska	k1gFnPc4	páska
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rovněž	rovněž	k9	rovněž
zhotovovat	zhotovovat	k5eAaImF	zhotovovat
levné	levný	k2eAgFnPc1d1	levná
stuhy	stuha	k1gFnPc1	stuha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
výrobků	výrobek	k1gInPc2	výrobek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
podle	podle	k7c2	podle
vzhledu	vzhled	k1gInSc2	vzhled
===	===	k?	===
</s>
</p>
<p>
<s>
paspulky	paspulka	k1gFnPc4	paspulka
–	–	k?	–
stuhy	stuha	k1gFnPc4	stuha
se	s	k7c7	s
zesíleným	zesílený	k2eAgInSc7d1	zesílený
okrajem	okraj	k1gInSc7	okraj
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
výplnkovou	výplnkův	k2eAgFnSc7d1	výplnkův
nití	nit	k1gFnSc7	nit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lámové	láma	k1gMnPc1	láma
stuhy	stuha	k1gFnSc2	stuha
–	–	k?	–
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
pokovovaných	pokovovaný	k2eAgFnPc2d1	pokovovaná
nití	nit	k1gFnPc2	nit
nebo	nebo	k8xC	nebo
pásků	pásek	k1gInPc2	pásek
zvaných	zvaný	k2eAgInPc2d1	zvaný
lamé	lamé	k1gNnSc4	lamé
</s>
</p>
<p>
<s>
piketky	piketka	k1gFnPc1	piketka
–	–	k?	–
se	s	k7c7	s
zoubkováním	zoubkování	k1gNnSc7	zoubkování
krajů	kraj	k1gInPc2	kraj
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
jemnými	jemný	k2eAgInPc7d1	jemný
smyčkami	smyčka	k1gFnPc7	smyčka
</s>
</p>
<p>
<s>
sametky	sametka	k1gFnPc1	sametka
–	–	k?	–
se	s	k7c7	s
sametovým	sametový	k2eAgInSc7d1	sametový
povrchem	povrch	k1gInSc7	povrch
na	na	k7c6	na
lícní	lícní	k2eAgFnSc6d1	lícní
straně	strana	k1gFnSc6	strana
</s>
</p>
<p>
<s>
dutinné	dutinný	k2eAgFnPc1d1	dutinný
stuhy	stuha	k1gFnPc1	stuha
–	–	k?	–
hadicovitý	hadicovitý	k2eAgInSc4d1	hadicovitý
tvar	tvar	k1gInSc4	tvar
</s>
</p>
<p>
<s>
pruženky	pruženka	k1gFnPc1	pruženka
–	–	k?	–
stuhy	stuha	k1gFnSc2	stuha
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
elastanů	elastan	k1gInPc2	elastan
</s>
</p>
<p>
<s>
===	===	k?	===
podle	podle	k7c2	podle
použití	použití	k1gNnSc2	použití
===	===	k?	===
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
oděvní	oděvní	k2eAgInPc4d1	oděvní
a	a	k8xC	a
obuvnické	obuvnický	k2eAgInPc4d1	obuvnický
účely	účel	k1gInPc4	účel
–	–	k?	–
zdrhovadla	zdrhovadlo	k1gNnSc2	zdrhovadlo
<g/>
,	,	kIx,	,
lemovky	lemovka	k1gFnSc2	lemovka
<g/>
,	,	kIx,	,
pasovky	pasovka	k1gFnPc1	pasovka
<g/>
,	,	kIx,	,
ramínkovky	ramínkovka	k1gFnPc1	ramínkovka
<g/>
,	,	kIx,	,
poutkovky	poutkovka	k1gFnPc1	poutkovka
<g/>
,	,	kIx,	,
chránidlovky	chránidlovka	k1gFnPc1	chránidlovka
<g/>
,	,	kIx,	,
zdobící	zdobící	k2eAgFnPc1d1	zdobící
stuhy	stuha	k1gFnPc1	stuha
</s>
</p>
<p>
<s>
jako	jako	k8xS	jako
doplňky	doplněk	k1gInPc4	doplněk
k	k	k7c3	k
bytovým	bytový	k2eAgFnPc3d1	bytová
textiliím	textilie	k1gFnPc3	textilie
–	–	k?	–
kobercovky	kobercovka	k1gFnPc1	kobercovka
<g/>
,	,	kIx,	,
záclonovky	záclonovka	k1gFnPc1	záclonovka
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
stuhy	stuha	k1gFnPc1	stuha
k	k	k7c3	k
technickým	technický	k2eAgNnPc3d1	technické
a	a	k8xC	a
zdravotním	zdravotní	k2eAgInPc3d1	zdravotní
účelům	účel	k1gInPc3	účel
–	–	k?	–
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
suché	suchý	k2eAgInPc1d1	suchý
zipy	zip	k1gInPc1	zip
<g/>
,	,	kIx,	,
knoty	knot	k1gInPc1	knot
<g/>
,	,	kIx,	,
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
obinadla	obinadlo	k1gNnPc1	obinadlo
<g/>
,	,	kIx,	,
izolace	izolace	k1gFnSc1	izolace
a	a	k8xC	a
těsnění	těsnění	k1gNnSc1	těsnění
</s>
</p>
<p>
<s>
popruhy	popruha	k1gFnPc1	popruha
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
rovněž	rovněž	k9	rovněž
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
stuhové	stuhový	k2eAgNnSc4d1	stuhový
zboží	zboží	k1gNnSc4	zboží
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Heinz	Heinz	k1gMnSc1	Heinz
Hennig	Hennig	k1gMnSc1	Hennig
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Gewebetechnik	Gewebetechnik	k1gMnSc1	Gewebetechnik
<g/>
,	,	kIx,	,
Fachbuchverlag	Fachbuchverlag	k1gMnSc1	Fachbuchverlag
<g/>
,	,	kIx,	,
Lipsko	Lipsko	k1gNnSc1	Lipsko
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
420-442	[number]	k4	420-442
</s>
</p>
<p>
<s>
Hana	Hana	k1gFnSc1	Hana
Srbová	Srbová	k1gFnSc1	Srbová
<g/>
:	:	kIx,	:
Nové	Nové	k2eAgInPc1d1	Nové
textilní	textilní	k2eAgInPc1d1	textilní
výrobky	výrobek	k1gInPc1	výrobek
společnosti	společnost	k1gFnSc2	společnost
STAP	STAP	kA	STAP
(	(	kIx(	(
<g/>
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stuha	stuha	k1gFnSc1	stuha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
