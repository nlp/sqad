<s>
Strany	strana	k1gFnPc1	strana
mají	mít	k5eAaImIp3nP	mít
poměr	poměr	k1gInSc4	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
žluto-oranžové	žlutoranžový	k2eAgInPc1d1	žluto-oranžový
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
