<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Mali	Mali	k1gNnSc2	Mali
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
mají	mít	k5eAaImIp3nP	mít
poměr	poměr	k1gInSc4	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
třemi	tři	k4xCgInPc7	tři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
žluto-oranžové	žlutoranžový	k2eAgInPc1d1	žluto-oranžový
(	(	kIx(	(
<g/>
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
)	)	kIx)	)
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
mají	mít	k5eAaImIp3nP	mít
panafrický	panafrický	k2eAgInSc4d1	panafrický
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
zelená	zelený	k2eAgFnSc1d1	zelená
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
přírodu	příroda	k1gFnSc4	příroda
a	a	k8xC	a
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
žlutá	žlutat	k5eAaImIp3nS	žlutat
slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
červená	červenat	k5eAaImIp3nS	červenat
odhodlání	odhodlání	k1gNnSc4	odhodlání
a	a	k8xC	a
boj	boj	k1gInSc4	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
verze	verze	k1gFnSc1	verze
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
prostředním	prostřední	k2eAgInSc6d1	prostřední
pruhu	pruh	k1gInSc6	pruh
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
stylizovanou	stylizovaný	k2eAgFnSc4d1	stylizovaná
postavu	postava	k1gFnSc4	postava
(	(	kIx(	(
<g/>
kanaga	kanaga	k1gFnSc1	kanaga
<g/>
)	)	kIx)	)
s	s	k7c7	s
rukama	ruka	k1gFnPc7	ruka
vztyčenýma	vztyčený	k2eAgFnPc7d1	vztyčená
vzhůru	vzhůru	k6eAd1	vzhůru
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
90	[number]	k4	90
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Mali	Mali	k1gNnSc2	Mali
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
islám	islám	k1gInSc1	islám
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
ve	v	k7c6	v
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
zobrazovat	zobrazovat	k5eAaImF	zobrazovat
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
nejnovější	nový	k2eAgFnSc6d3	nejnovější
verzi	verze	k1gFnSc6	verze
postava	postava	k1gFnSc1	postava
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Flag	flaga	k1gFnPc2	flaga
of	of	k?	of
Mali	Mali	k1gNnSc6	Mali
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Mali	Mali	k1gNnSc2	Mali
</s>
</p>
<p>
<s>
Malijská	malijský	k2eAgFnSc1d1	malijská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlajka	vlajka	k1gFnSc1	vlajka
Mali	Mali	k1gNnPc2	Mali
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
