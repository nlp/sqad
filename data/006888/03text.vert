<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
6	[number]	k4	6
byla	být	k5eAaImAgFnS	být
jednohlasně	jednohlasně	k6eAd1	jednohlasně
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
uvedla	uvést	k5eAaPmAgFnS	uvést
data	datum	k1gNnSc2	datum
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
posoudí	posoudit	k5eAaPmIp3nS	posoudit
nové	nový	k2eAgMnPc4d1	nový
uchazeče	uchazeč	k1gMnPc4	uchazeč
o	o	k7c4	o
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
byla	být	k5eAaImAgFnS	být
pozměněna	pozměněn	k2eAgFnSc1d1	pozměněna
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
kvůli	kvůli	k7c3	kvůli
odkladu	odklad	k1gInSc2	odklad
data	datum	k1gNnSc2	datum
zahájení	zahájení	k1gNnSc2	zahájení
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
prvního	první	k4xOgNnSc2	první
zasedání	zasedání	k1gNnSc6	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
v	v	k7c6	v
originální	originální	k2eAgFnSc6d1	originální
rezoluci	rezoluce	k1gFnSc6	rezoluce
byly	být	k5eAaImAgFnP	být
proto	proto	k8xC	proto
posunuta	posunout	k5eAaPmNgFnS	posunout
zpět	zpět	k6eAd1	zpět
o	o	k7c4	o
tolik	tolik	k4yIc4	tolik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
kolik	kolik	k9	kolik
činil	činit	k5eAaImAgInS	činit
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
původním	původní	k2eAgNnSc7d1	původní
a	a	k8xC	a
odloženým	odložený	k2eAgNnSc7d1	odložené
datem	datum	k1gNnSc7	datum
zasedání	zasedání	k1gNnSc2	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Security	Securita	k1gFnSc2	Securita
Council	Council	k1gInSc1	Council
Resolution	Resolution	k1gInSc1	Resolution
6	[number]	k4	6
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Security	Securita	k1gFnSc2	Securita
Council	Council	k1gInSc1	Council
Resolution	Resolution	k1gInSc1	Resolution
6	[number]	k4	6
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
