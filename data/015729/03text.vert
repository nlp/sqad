<s>
Guitar	Guitar	k1gInSc1
Tluppa	Tlupp	k1gMnSc2
</s>
<s>
Skupina	skupina	k1gFnSc1
v	v	k7c6
červnu	červen	k1gInSc6
2014	#num#	k4
</s>
<s>
Guitar	Guitar	k1gInSc1
Tluppa	Tluppa	k1gFnSc1
je	být	k5eAaImIp3nS
českolipská	českolipský	k2eAgFnSc1d1
country	country	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
založená	založený	k2eAgFnSc1d1
v	v	k7c6
České	český	k2eAgFnSc6d1
Lípě	lípa	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
velký	velký	k2eAgInSc4d1
podíl	podíl	k1gInSc4
za	za	k7c4
obnovení	obnovení	k1gNnSc4
country	country	k2eAgInPc2d1
festivalů	festival	k1gInPc2
v	v	k7c6
Holanech	Holan	k1gMnPc6
na	na	k7c6
Českolipsku	Českolipsko	k1gNnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Členové	člen	k1gMnPc1
skupiny	skupina	k1gFnSc2
</s>
<s>
Vláďa	Vláďa	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
–	–	k?
vedoucí	vedoucí	k1gMnSc1
</s>
<s>
Jarin	Jarin	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
</s>
<s>
Honza	Honza	k1gMnSc1
Špikla	Špikla	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Janda	Janda	k1gMnSc1
</s>
<s>
Tom	Tom	k1gMnSc1
Nosál	nosál	k1gMnSc1
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1
členové	člen	k1gMnPc1
skupiny	skupina	k1gFnSc2
</s>
<s>
Láďa	Láďa	k1gFnSc1
Merc	Merc	k1gFnSc1
</s>
<s>
Míra	Míra	k1gFnSc1
Myška	myška	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kraus	Kraus	k1gMnSc1
(	(	kIx(
<g/>
zvukař	zvukař	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
byla	být	k5eAaImAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
těch	ten	k3xDgFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
obnovily	obnovit	k5eAaPmAgFnP
country	country	k2eAgInSc4d1
festival	festival	k1gInSc4
v	v	k7c6
Holanech	Holan	k1gMnPc6
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Česká	český	k2eAgFnSc1d1
Lípa	lípa	k1gFnSc1
<g/>
)	)	kIx)
pod	pod	k7c7
novým	nový	k2eAgInSc7d1
názvem	název	k1gInSc7
Holanský	Holanský	k2eAgMnSc1d1
pulec	pulec	k1gMnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
Holanský	Holanský	k2eAgMnSc1d1
kapr	kapr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vystupuje	vystupovat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
jiných	jiný	k2eAgFnPc6d1
scénách	scéna	k1gFnPc6
<g/>
,	,	kIx,
např.	např.	kA
Českolipské	českolipský	k2eAgFnSc2d1
slavnosti	slavnost	k1gFnSc2
2014	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jarin	Jarin	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Holanský	Holanský	k2eAgMnSc1d1
pulec	pulec	k1gMnSc1
<g/>
.	.	kIx.
i-noviny	i-novina	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-06-29	2014-06-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Petra	Petra	k1gFnSc1
Hámová	Hámová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnosti	slavnost	k1gFnSc2
v	v	k7c6
České	český	k2eAgFnSc6d1
Lípě	lípa	k1gFnSc6
<g/>
:	:	kIx,
Spousta	spousta	k1gFnSc1
zábavy	zábava	k1gFnSc2
i	i	k8xC
uzavírky	uzavírka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českolipský	českolipský	k2eAgInSc1d1
deník	deník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-06-27	2014-06-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Guitar	Guitara	k1gFnPc2
Tluppa	Tluppa	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
skupiny	skupina	k1gFnSc2
</s>
<s>
Skupina	skupina	k1gFnSc1
na	na	k7c6
Holanském	Holanský	k2eAgMnSc6d1
kapru	kapr	k1gMnSc6
</s>
<s>
Záznam	záznam	k1gInSc1
vystoupení	vystoupení	k1gNnSc1
2012	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
</s>
