<s>
Irena	Irena	k1gFnSc1	Irena
je	být	k5eAaImIp3nS	být
ženské	ženský	k2eAgNnSc4d1	ženské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
řeckém	řecký	k2eAgNnSc6d1	řecké
slově	slovo	k1gNnSc6	slovo
eirinis	eirinis	k1gFnSc2	eirinis
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
mír	mír	k1gInSc1	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mužským	mužský	k2eAgInSc7d1	mužský
protějškem	protějšek	k1gInSc7	protějšek
Ireny	Irena	k1gFnSc2	Irena
je	být	k5eAaImIp3nS	být
zřídka	zřídka	k6eAd1	zřídka
používané	používaný	k2eAgNnSc4d1	používané
jméno	jméno	k1gNnSc4	jméno
Ireneus	Ireneus	k1gMnSc1	Ireneus
nebo	nebo	k8xC	nebo
Irenej	Irenej	k1gMnSc1	Irenej
<g/>
.	.	kIx.	.
</s>
<s>
Čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ailin	Ailin	k1gInSc1	Ailin
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Yileina	Yileina	k1gFnSc1	Yileina
<g/>
"	"	kIx"	"
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Irene	Iren	k1gMnSc5	Iren
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Eirene	Eiren	k1gInSc5	Eiren
<g/>
"	"	kIx"	"
Irene	Iren	k1gMnSc5	Iren
<g/>
,	,	kIx,	,
Iren	Irena	k1gFnPc2	Irena
<g/>
,	,	kIx,	,
Irén	Iréna	k1gFnPc2	Iréna
<g/>
,	,	kIx,	,
Irini	Irieň	k1gFnSc6	Irieň
<g/>
,	,	kIx,	,
Ira	Ir	k1gMnSc4	Ir
<g/>
,	,	kIx,	,
Irka	Irka	k1gFnSc1	Irka
<g/>
,	,	kIx,	,
Irenka	Irenka	k1gFnSc1	Irenka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Rena	Ren	k2eAgFnSc1d1	Rena
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
ženskými	ženský	k2eAgNnPc7d1	ženské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
-	-	kIx~	-
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgFnPc7d1	žijící
ženami	žena	k1gFnPc7	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
-0,1	-0,1	k4	-0,1
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Eiréné	Eiréné	k1gNnSc1	Eiréné
-	-	kIx~	-
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
míru	mír	k1gInSc2	mír
a	a	k8xC	a
jara	jaro	k1gNnSc2	jaro
Svatá	svatý	k2eAgFnSc1d1	svatá
Irena	Irena	k1gFnSc1	Irena
Irena	Irena	k1gFnSc1	Irena
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
-	-	kIx~	-
císařovna	císařovna	k1gFnSc1	císařovna
východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
Irena	Irena	k1gFnSc1	Irena
Dukaina	Dukaina	k1gFnSc1	Dukaina
-	-	kIx~	-
žena	žena	k1gFnSc1	žena
byzantského	byzantský	k2eAgMnSc2d1	byzantský
císaře	císař	k1gMnSc2	císař
Alexia	Alexium	k1gNnSc2	Alexium
I.	I.	kA	I.
Komnena	Komnen	k2eAgFnSc1d1	Komnen
Irena	Irena	k1gFnSc1	Irena
Švábská	švábský	k2eAgFnSc1d1	Švábská
-	-	kIx~	-
německá	německý	k2eAgFnSc1d1	německá
královna	královna	k1gFnSc1	královna
Irene	Iren	k1gInSc5	Iren
Adler	Adler	k1gMnSc1	Adler
-	-	kIx~	-
láska	láska	k1gFnSc1	láska
Sherlocka	Sherlocka	k1gFnSc1	Sherlocka
Holmese	Holmese	k1gFnSc1	Holmese
Irene	Iren	k1gInSc5	Iren
Bedard	Bedard	k1gInSc1	Bedard
-	-	kIx~	-
severoamerická	severoamerický	k2eAgFnSc1d1	severoamerická
indiánská	indiánský	k2eAgFnSc1d1	indiánská
herečka	herečka	k1gFnSc1	herečka
Irena	Irena	k1gFnSc1	Irena
Budweiserová	Budweiserová	k1gFnSc1	Budweiserová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Irene	Iren	k1gInSc5	Iren
Cara	car	k1gMnSc2	car
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Irena	Irena	k1gFnSc1	Irena
Dousková	Dousková	k1gFnSc1	Dousková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Irini	Irieň	k1gFnSc3	Irieň
Charalambidu	Charalambid	k1gInSc2	Charalambid
-	-	kIx~	-
česko-řecká	česko-řecký	k2eAgFnSc1d1	česko-řecká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Irena	Irena	k1gFnSc1	Irena
Chřibková	Chřibkový	k2eAgFnSc1d1	Chřibková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
varhanice	varhanice	k1gFnSc1	varhanice
Eirini	Eirin	k2eAgMnPc1d1	Eirin
Kavarnou	Kavarná	k1gFnSc4	Kavarná
-	-	kIx~	-
řecká	řecký	k2eAgFnSc1d1	řecká
plavkyně	plavkyně	k1gFnSc1	plavkyně
Irena	Irena	k1gFnSc1	Irena
Kačírková	Kačírková	k1gFnSc1	Kačírková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
Irena	Irena	k1gFnSc1	Irena
Kosíková	Kosíková	k1gFnSc1	Kosíková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
varhanice	varhanice	k1gFnSc1	varhanice
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
Eirene	Eiren	k1gInSc5	Eiren
Koroulakis	Koroulakis	k1gFnPc1	Koroulakis
-	-	kIx~	-
řecká	řecký	k2eAgFnSc1d1	řecká
lekařka	lekařka	k1gFnSc1	lekařka
medicíny	medicína	k1gFnSc2	medicína
žijících	žijící	k2eAgMnPc2d1	žijící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
Irena	Irena	k1gFnSc1	Irena
Obermannová	Obermannový	k2eAgFnSc1d1	Obermannová
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Irena	Irena	k1gFnSc1	Irena
Pavlásková	Pavlásková	k1gFnSc1	Pavlásková
-	-	kIx~	-
česká	český	k2eAgFnSc1d1	Česká
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
scenáristka	scenáristka	k1gFnSc1	scenáristka
Irena	Irena	k1gFnSc1	Irena
Uherská	uherský	k2eAgFnSc1d1	uherská
-	-	kIx~	-
byzantská	byzantský	k2eAgFnSc1d1	byzantská
císařovna	císařovna	k1gFnSc1	císařovna
Eirene	Eiren	k1gInSc5	Eiren
Williams	Williams	k1gInSc1	Williams
-	-	kIx~	-
americká	americký	k2eAgFnSc1d1	americká
genetička	genetička	k1gFnSc1	genetička
a	a	k8xC	a
lekařka	lekařka	k1gFnSc1	lekařka
Ireen	Irena	k1gFnPc2	Irena
Wüst	Wüsta	k1gFnPc2	Wüsta
-	-	kIx~	-
novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
rychlobruslařka	rychlobruslařka	k1gFnSc1	rychlobruslařka
Irena	Irena	k1gFnSc1	Irena
tyrkysová	tyrkysový	k2eAgFnSc1d1	tyrkysová
-	-	kIx~	-
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Irenovitých	Irenovitý	k2eAgFnPc2d1	Irenovitý
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Irena	Irena	k1gFnSc1	Irena
<g/>
"	"	kIx"	"
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Irena	Irena	k1gFnSc1	Irena
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
