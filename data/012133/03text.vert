<p>
<s>
Prix	Prix	k1gInSc1	Prix
Bohemia	bohemia	k1gFnSc1	bohemia
Radio	radio	k1gNnSc4	radio
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěž	soutěž	k1gFnSc1	soutěž
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
pořadů	pořad	k1gInPc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
33	[number]	k4	33
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
v	v	k7c6	v
jarním	jarní	k2eAgInSc6d1	jarní
termínu	termín	k1gInSc6	termín
<g/>
.	.	kIx.	.
</s>
<s>
Čestnou	čestný	k2eAgFnSc7d1	čestná
prezidentkou	prezidentka	k1gFnSc7	prezidentka
festivalu	festival	k1gInSc2	festival
je	být	k5eAaImIp3nS	být
herečka	herečka	k1gFnSc1	herečka
Hana	Hana	k1gFnSc1	Hana
Maciuchová	Maciuchová	k1gFnSc1	Maciuchová
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
soutěž	soutěž	k1gFnSc1	soutěž
pořádá	pořádat	k5eAaImIp3nS	pořádat
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
snahou	snaha	k1gFnSc7	snaha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
zapojily	zapojit	k5eAaPmAgInP	zapojit
pořady	pořad	k1gInPc1	pořad
vyrobené	vyrobený	k2eAgInPc1d1	vyrobený
partnerskými	partnerský	k2eAgFnPc7d1	partnerská
organizacemi	organizace	k1gFnPc7	organizace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Visegrádské	visegrádský	k2eAgFnSc2d1	Visegrádská
čtyřky	čtyřka	k1gFnSc2	čtyřka
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Rozhlas	rozhlas	k1gInSc1	rozhlas
a	a	k8xC	a
televize	televize	k1gFnSc1	televize
Slovenska	Slovensko	k1gNnSc2	Slovensko
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Polským	polský	k2eAgInSc7d1	polský
rozhlasem	rozhlas	k1gInSc7	rozhlas
a	a	k8xC	a
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
MTVA	MTVA	kA	MTVA
<g/>
.	.	kIx.	.
</s>
<s>
Pořádání	pořádání	k1gNnSc1	pořádání
festivalu	festival	k1gInSc2	festival
proto	proto	k8xC	proto
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
visegrádský	visegrádský	k2eAgInSc1d1	visegrádský
fond	fond	k1gInSc1	fond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Prix	Prix	k1gInSc1	Prix
Bohemia	bohemia	k1gFnSc1	bohemia
Radio	radio	k1gNnSc4	radio
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
