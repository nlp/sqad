<s>
Haitská	haitský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Haiti	Haiti	k1gNnSc2
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
dva	dva	k4xCgInPc1
vodorovné	vodorovný	k2eAgInPc1d1
pruhy	pruh	k1gInPc1
<g/>
:	:	kIx,
červený	červený	k2eAgInSc1d1
a	a	k8xC
modrý	modrý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajka	vlajka	k1gFnSc1
navazuje	navazovat	k5eAaImIp3nS
na	na	k7c4
červeno-modrou	červeno-modrý	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1803	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
během	během	k7c2
protifrancouzského	protifrancouzský	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
odstraněn	odstraněn	k2eAgInSc4d1
bílý	bílý	k2eAgInSc4d1
pruh	pruh	k1gInSc4
z	z	k7c2
francouzské	francouzský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
bílém	bílé	k1gNnSc6
obdélníku	obdélník	k1gInSc2
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
má	mít	k5eAaImIp3nS
poměr	poměr	k1gInSc4
stran	strana	k1gFnPc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
varianta	varianta	k1gFnSc1
s	s	k7c7
větším	veliký	k2eAgInSc7d2
bílým	bílý	k2eAgInSc7d1
obdélníkem	obdélník	k1gInSc7
(	(	kIx(
<g/>
i	i	k9
znakem	znak	k1gInSc7
<g/>
)	)	kIx)
o	o	k7c6
poměru	poměr	k1gInSc6
3	#num#	k4
<g/>
:	:	kIx,
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modrá	modrý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
černochy	černoch	k1gMnPc4
a	a	k8xC
mulaty	mulat	k1gMnPc4
<g/>
,	,	kIx,
červená	červenat	k5eAaImIp3nS
krev	krev	k1gFnSc4
prolitou	prolitý	k2eAgFnSc4d1
v	v	k7c6
bojích	boj	k1gInPc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
Berlíně	Berlín	k1gInSc6
zjistilo	zjistit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
Haiti	Haiti	k1gNnSc1
a	a	k8xC
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
používají	používat	k5eAaImIp3nP
stejnou	stejný	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
byla	být	k5eAaImAgFnS
na	na	k7c4
lichtenštejnskou	lichtenštejnský	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
přidána	přidán	k2eAgFnSc1d1
24	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1937	#num#	k4
do	do	k7c2
horního	horní	k2eAgInSc2d1
žerďového	žerďový	k2eAgInSc2d1
rohu	roh	k1gInSc2
zlatá	zlatý	k2eAgFnSc1d1
knížecí	knížecí	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozměry	rozměra	k1gFnPc1
haitské	haitský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ostrov	ostrov	k1gInSc1
Hispaniola	Hispaniola	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gFnSc6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
se	se	k3xPyFc4
dnešní	dnešní	k2eAgFnSc1d1
Republika	republika	k1gFnSc1
Haiti	Haiti	k1gNnSc4
rozkládá	rozkládat	k5eAaImIp3nS
(	(	kIx(
<g/>
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
ostrova	ostrov	k1gInSc2
zabírá	zabírat	k5eAaImIp3nS
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
objevil	objevit	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1492	#num#	k4
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc1d1
zdroj	zdroj	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
5	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
své	svůj	k3xOyFgFnSc6
první	první	k4xOgFnSc6
výpravě	výprava	k1gFnSc6
janovský	janovský	k2eAgMnSc1d1
mořeplavec	mořeplavec	k1gMnSc1
ve	v	k7c6
službách	služba	k1gFnPc6
Španělska	Španělsko	k1gNnSc2
Kryštof	Kryštof	k1gMnSc1
Kolumbus	Kolumbus	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostrov	ostrov	k1gInSc1
se	se	k3xPyFc4
následně	následně	k6eAd1
stal	stát	k5eAaPmAgMnS
základnou	základna	k1gFnSc7
pro	pro	k7c4
další	další	k2eAgInPc4d1
objevy	objev	k1gInPc4
a	a	k8xC
španělskou	španělský	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátků	počátek	k1gInPc2
koloniálního	koloniální	k2eAgNnSc2d1
období	období	k1gNnSc2
ovládali	ovládat	k5eAaImAgMnP
Španělé	Španěl	k1gMnPc1
celý	celý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
většinu	většina	k1gFnSc4
Karibiku	Karibik	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
začaly	začít	k5eAaPmAgFnP
v	v	k7c6
regionu	region	k1gInSc6
prosazovat	prosazovat	k5eAaImF
další	další	k2eAgFnPc4d1
evropské	evropský	k2eAgFnPc4d1
mocnosti	mocnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1
bukanýři	bukanýr	k1gMnPc1
osídlili	osídlit	k5eAaPmAgMnP
roku	rok	k1gInSc2
1625	#num#	k4
nejprve	nejprve	k6eAd1
ostrov	ostrov	k1gInSc4
Tortuga	Tortuga	k1gFnSc1
(	(	kIx(
<g/>
severně	severně	k6eAd1
od	od	k7c2
Hispanioly	Hispaniola	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
následně	následně	k6eAd1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
usazovat	usazovat	k5eAaImF
i	i	k9
na	na	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
kolonie	kolonie	k1gFnSc1
na	na	k7c6
Tortuze	Tortuha	k1gFnSc6
byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
ustanovena	ustanoven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1659	#num#	k4
výnosem	výnos	k1gInSc7
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Španělsko	Španělsko	k1gNnSc1
uznalo	uznat	k5eAaPmAgNnS
francouzskou	francouzský	k2eAgFnSc4d1
svrchovanost	svrchovanost	k1gFnSc4
nad	nad	k7c7
západní	západní	k2eAgFnSc7d1
částí	část	k1gFnSc7
Hispanioly	Hispaniola	k1gFnSc2
roku	rok	k1gInSc2
1697	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
podepsána	podepsán	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
z	z	k7c2
Rijswijku	Rijswijka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
tak	tak	k9
kolonie	kolonie	k1gFnSc1
Saint-Domingue	Saint-Domingue	k1gFnSc1
<g/>
,	,	kIx,
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
ostrova	ostrov	k1gInSc2
pod	pod	k7c7
názvem	název	k1gInSc7
Santo	Sant	k2eAgNnSc4d1
Domingo	Domingo	k1gNnSc4
ovládali	ovládat	k5eAaImAgMnP
nadále	nadále	k6eAd1
Španělé	Španěl	k1gMnPc1
(	(	kIx(
<g/>
Santo	Santo	k1gNnSc1
Domingo	Domingo	k6eAd1
je	být	k5eAaImIp3nS
i	i	k9
současný	současný	k2eAgInSc1d1
název	název	k1gInSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Dominikánské	dominikánský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původní	původní	k2eAgMnPc1d1
indiáni	indián	k1gMnPc1
z	z	k7c2
kmene	kmen	k1gInSc2
Taínů	Taín	k1gMnPc2
(	(	kIx(
<g/>
patřící	patřící	k2eAgFnSc1d1
do	do	k7c2
skupiny	skupina	k1gFnSc2
indiánských	indiánský	k2eAgNnPc2d1
etnik	etnikum	k1gNnPc2
označovaných	označovaný	k2eAgNnPc2d1
jako	jako	k9
Aravakové	Aravakový	k2eAgFnSc6d1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
vyhubeni	vyhuben	k2eAgMnPc1d1
<g/>
,	,	kIx,
na	na	k7c6
čemž	což	k3yQnSc6,k3yRnSc6
se	se	k3xPyFc4
podílelo	podílet	k5eAaImAgNnS
vyvražďování	vyvražďování	k1gNnSc3
<g/>
,	,	kIx,
zavlečené	zavlečený	k2eAgFnSc3d1
nemoci	nemoc	k1gFnSc3
(	(	kIx(
<g/>
např.	např.	kA
neštovice	neštovice	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
otrocké	otrocký	k2eAgFnPc4d1
práce	práce	k1gFnPc4
na	na	k7c6
plantážích	plantáž	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k6eAd1
vyhubených	vyhubený	k2eAgMnPc2d1
indiánů	indián	k1gMnPc2
byli	být	k5eAaImAgMnP
na	na	k7c4
práci	práce	k1gFnSc4
na	na	k7c6
polích	pole	k1gNnPc6
přiváženi	přivážen	k2eAgMnPc1d1
otroci	otrok	k1gMnPc1
z	z	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časté	častý	k2eAgFnPc1d1
vzpoury	vzpoura	k1gFnPc1
proti	proti	k7c3
krutému	krutý	k2eAgNnSc3d1
zacházení	zacházení	k1gNnSc3
vedly	vést	k5eAaImAgFnP
k	k	k7c3
postupnému	postupný	k2eAgInSc3d1
boji	boj	k1gInSc3
za	za	k7c4
svobodu	svoboda	k1gFnSc4
a	a	k8xC
nezávislost	nezávislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
měla	mít	k5eAaImAgFnS
vliv	vliv	k1gInSc4
i	i	k9
na	na	k7c4
dění	dění	k1gNnSc4
v	v	k7c6
západní	západní	k2eAgFnSc6d1
francouzské	francouzský	k2eAgFnSc6d1
části	část	k1gFnSc6
ostrova	ostrov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1791	#num#	k4
zahájili	zahájit	k5eAaPmAgMnP
otroci	otrok	k1gMnPc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Toussainta	Toussaint	k1gInSc2
Lovertura	Lovertura	k1gFnSc1
tzv.	tzv.	kA
Haitskou	haitský	k2eAgFnSc4d1
revoluci	revoluce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Basilejská	basilejský	k2eAgFnSc1d1
smlouva	smlouva	k1gFnSc1
přiřkla	přiřknout	k5eAaPmAgFnS
celý	celý	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1798	#num#	k4
na	na	k7c6
něm	on	k3xPp3gNnSc6
vlála	vlát	k5eAaImAgFnS
pouze	pouze	k6eAd1
francouzská	francouzský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
byl	být	k5eAaImAgInS
Loverture	Lovertur	k1gMnSc5
na	na	k7c4
příkaz	příkaz	k1gInSc4
Napoleona	Napoleon	k1gMnSc4
na	na	k7c6
jednání	jednání	k1gNnSc6
s	s	k7c7
Francouzi	Francouz	k1gMnPc7
(	(	kIx(
<g/>
kam	kam	k6eAd1
byl	být	k5eAaImAgMnS
pozván	pozvat	k5eAaPmNgMnS
a	a	k8xC
dostal	dostat	k5eAaPmAgMnS
záruku	záruka	k1gFnSc4
bezpečí	bezpečí	k1gNnSc2
<g/>
)	)	kIx)
zatčen	zatknout	k5eAaPmNgMnS
a	a	k8xC
převezen	převézt	k5eAaPmNgMnS
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vězení	vězení	k1gNnSc6
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
velení	velení	k1gNnSc1
povstalců	povstalec	k1gMnPc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
generál	generál	k1gMnSc1
Jean-Jacques	Jean-Jacques	k1gMnSc1
Dessalines	Dessalines	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
první	první	k4xOgFnSc2
haitské	haitský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sjezdu	sjezd	k1gInSc6
vzbouřenců	vzbouřenec	k1gMnPc2
ve	v	k7c6
dnech	den	k1gInPc6
15	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1803	#num#	k4
z	z	k7c2
francouzské	francouzský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
vypáral	vypárat	k5eAaPmAgInS
bílý	bílý	k2eAgInSc1d1
pruh	pruh	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
připomínal	připomínat	k5eAaImAgInS
bílé	bílé	k1gNnSc4
otrokáře	otrokář	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
kmotřenka	kmotřenka	k1gFnSc1
Catharine	Catharin	k1gInSc5
Flonová	Flonová	k1gFnSc1
ušila	ušít	k5eAaPmAgFnS
ze	z	k7c2
zbytků	zbytek	k1gInPc2
první	první	k4xOgFnSc4
haitskou	haitský	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
modro-červenými	modro-červený	k2eAgInPc7d1
svislými	svislý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1803	#num#	k4
byla	být	k5eAaImAgFnS
vlajka	vlajka	k1gFnSc1
poprvé	poprvé	k6eAd1
vztyčena	vztyčen	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Později	pozdě	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
období	období	k1gNnSc6
bojů	boj	k1gInPc2
užívalo	užívat	k5eAaImAgNnS
i	i	k9
několik	několik	k4yIc4
variant	varianta	k1gFnPc2
modro-červených	modro-červený	k2eAgFnPc2d1
vlajek	vlajka	k1gFnPc2
s	s	k7c7
bílým	bílý	k2eAgNnSc7d1
heslem	heslo	k1gNnSc7
LIBERTÉ	LIBERTÉ	kA
OU	ou	k0
LA	la	k1gNnPc1
MORT	MORT	kA
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Svobodu	svoboda	k1gFnSc4
nebo	nebo	k8xC
smrt	smrt	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
francouzské	francouzský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1803	#num#	k4
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Vertiè	Vertiè	k1gFnPc2
byla	být	k5eAaImAgFnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1804	#num#	k4
vyhlášena	vyhlášen	k2eAgFnSc1d1
haitská	haitský	k2eAgFnSc1d1
nezávislost	nezávislost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálním	generální	k2eAgMnSc7d1
guvernérem	guvernér	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Dessalines	Dessalines	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	s	k7c7
8	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1804	#num#	k4
prohlásil	prohlásit	k5eAaPmAgMnS
císařem	císař	k1gMnSc7
Jakubem	Jakub	k1gMnSc7
I.	I.	kA
Haitským	haitský	k2eAgNnSc7d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
černý	černý	k2eAgInSc1d1
Napoleon	napoleon	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státní	státní	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
zůstala	zůstat	k5eAaPmAgFnS
modro-červená	modro-červený	k2eAgFnSc1d1
bikolóra	bikolóra	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1805	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c4
počest	počest	k1gFnSc4
černého	černý	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
vlajka	vlajka	k1gFnSc1
změněna	změnit	k5eAaPmNgFnS
výměnou	výměna	k1gFnSc7
modrého	modré	k1gNnSc2
za	za	k7c4
černý	černý	k2eAgInSc4d1
pruh	pruh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
zavraždění	zavraždění	k1gNnSc6
císaře	císař	k1gMnSc2
Jakuba	Jakub	k1gMnSc2
I.	I.	kA
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1806	#num#	k4
vypukl	vypuknout	k5eAaPmAgInS
boj	boj	k1gInSc1
mezi	mezi	k7c4
černochy	černoch	k1gMnPc4
a	a	k8xC
mulaty	mulat	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
byl	být	k5eAaImAgInS
vznik	vznik	k1gInSc1
dvou	dva	k4xCgInPc2
útvarů	útvar	k1gInPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
mulati	mulat	k1gMnPc1
<g/>
)	)	kIx)
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
západě	západ	k1gInSc6
</s>
<s>
Haitský	haitský	k2eAgInSc1d1
stát	stát	k1gInSc1
(	(	kIx(
<g/>
černoši	černoch	k1gMnPc1
<g/>
)	)	kIx)
na	na	k7c6
severu	sever	k1gInSc6
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1807	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
Haitské	haitský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Alexandre	Alexandr	k1gInSc5
Sabè	Sabè	k1gInSc1
Pétion	Pétion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajkou	vlajka	k1gFnSc7
republiky	republika	k1gFnSc2
byla	být	k5eAaImAgFnS
původní	původní	k2eAgFnSc1d1
modro-červená	modro-červený	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
s	s	k7c7
pruhy	pruh	k1gInPc7
svislými	svislý	k2eAgInPc7d1
i	i	k8xC
vodorovnými	vodorovný	k2eAgInPc7d1
(	(	kIx(
<g/>
ty	ten	k3xDgFnPc1
převládaly	převládat	k5eAaImAgFnP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pétion	Pétion	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1807	#num#	k4
navrhl	navrhnout	k5eAaPmAgInS
a	a	k8xC
poté	poté	k6eAd1
i	i	k9
zavedl	zavést	k5eAaPmAgMnS
první	první	k4xOgInSc4
haitský	haitský	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
se	se	k3xPyFc4
po	po	k7c6
úpravách	úprava	k1gFnPc6
užívá	užívat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1807	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prezidentem	prezident	k1gMnSc7
Haitského	haitský	k2eAgInSc2d1
státu	stát	k1gInSc2
Henri	Henr	k1gFnSc2
Christophe	Christoph	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
Haitského	haitský	k2eAgNnSc2d1
království	království	k1gNnSc2
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1811	#num#	k4
králem	král	k1gMnSc7
Henrim	Henrim	k1gInSc4
I.	I.	kA
Státní	státní	k2eAgMnSc1d1
vlajkou	vlajka	k1gFnSc7
republiky	republika	k1gFnSc2
i	i	k8xC
království	království	k1gNnSc2
byla	být	k5eAaImAgFnS
černo-červená	černo-červený	k2eAgFnSc1d1
bikolóra	bikolóra	k1gFnSc1
se	s	k7c7
svislými	svislý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Henri	Henr	k1gFnSc2
I.	I.	kA
užíval	užívat	k5eAaImAgInS
osobní	osobní	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
tvořenou	tvořený	k2eAgFnSc7d1
státní	státní	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
doplněnou	doplněná	k1gFnSc4
o	o	k7c4
královský	královský	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1809	#num#	k4
Španělé	Španěl	k1gMnPc1
za	za	k7c2
podpory	podpora	k1gFnSc2
Britů	Brit	k1gMnPc2
opět	opět	k6eAd1
obsadili	obsadit	k5eAaPmAgMnP
východní	východní	k2eAgFnSc4d1
část	část	k1gFnSc4
Hispanioly	Hispaniola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1820	#num#	k4
byly	být	k5eAaImAgInP
oba	dva	k4xCgInPc1
haitské	haitský	k2eAgInPc1d1
státy	stát	k1gInPc1
sloučeny	sloučen	k2eAgInPc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
Haitská	haitský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1821	#num#	k4
byla	být	k5eAaImAgFnS
Španěly	Španěly	k1gInPc4
na	na	k7c6
východě	východ	k1gInSc6
vyhlášena	vyhlásit	k5eAaPmNgFnS
nezávislost	nezávislost	k1gFnSc1
pod	pod	k7c7
názvem	název	k1gInSc7
Kolumbijská	kolumbijský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Santo	Sant	k2eAgNnSc1d1
Domingo	Domingo	k1gNnSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1822	#num#	k4
byla	být	k5eAaImAgFnS
tato	tento	k3xDgFnSc1
republika	republika	k1gFnSc1
anektována	anektovat	k5eAaBmNgFnS
Haitskou	haitský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
a	a	k8xC
celý	celý	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
tak	tak	k9
znovu	znovu	k6eAd1
sloučen	sloučen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
republika	republika	k1gFnSc1
užívala	užívat	k5eAaImAgFnS
modro-červenou	modro-červený	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
z	z	k7c2
roku	rok	k1gInSc2
1807	#num#	k4
<g/>
,	,	kIx,
již	již	k6eAd1
výhradně	výhradně	k6eAd1
s	s	k7c7
vodorovnými	vodorovný	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1844	#num#	k4
se	se	k3xPyFc4
východ	východ	k1gInSc1
ostrova	ostrov	k1gInSc2
definitivně	definitivně	k6eAd1
odtrhl	odtrhnout	k5eAaPmAgInS
od	od	k7c2
Haiti	Haiti	k1gNnSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
začala	začít	k5eAaPmAgFnS
užívat	užívat	k5eAaImF
svou	svůj	k3xOyFgFnSc4
vlajku	vlajka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
na	na	k7c4
haitskou	haitský	k2eAgFnSc4d1
státní	státní	k2eAgFnSc4d1
vlajku	vlajka	k1gFnSc4
přidán	přidán	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
znak	znak	k1gInSc4
v	v	k7c6
bílém	bílé	k1gNnSc6
obdélníku	obdélník	k1gInSc2
(	(	kIx(
<g/>
shodná	shodný	k2eAgFnSc1d1
se	s	k7c7
současnou	současný	k2eAgFnSc7d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1849	#num#	k4
bylo	být	k5eAaImAgNnS
obnoveno	obnoven	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
(	(	kIx(
<g/>
Faustin	Faustin	k1gInSc1
I.	I.	kA
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
středu	střed	k1gInSc2
vlajky	vlajka	k1gFnSc2
byl	být	k5eAaImAgInS
umístěn	umístit	k5eAaPmNgInS
velký	velký	k2eAgInSc1d1
státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Haitského	haitský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1859	#num#	k4
bylo	být	k5eAaImAgNnS
císařství	císařství	k1gNnSc1
zrušeno	zrušit	k5eAaPmNgNnS
a	a	k8xC
obnovena	obnoven	k2eAgFnSc1d1
Haitská	haitský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajkou	vlajka	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vlajka	vlajka	k1gFnSc1
užívaná	užívaný	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1844	#num#	k4
<g/>
–	–	k?
<g/>
1849	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
1964	#num#	k4
uspořádal	uspořádat	k5eAaPmAgMnS
haitský	haitský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
François	François	k1gFnSc2
Duvalier	Duvalier	k1gMnSc1
(	(	kIx(
<g/>
ve	v	k7c6
funkci	funkce	k1gFnSc6
v	v	k7c6
letech	let	k1gInPc6
1957	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
lidové	lidový	k2eAgNnSc1d1
referendum	referendum	k1gNnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
si	se	k3xPyFc3
zajistil	zajistit	k5eAaPmAgMnS
doživotní	doživotní	k2eAgInSc4d1
mandát	mandát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
prosadil	prosadit	k5eAaPmAgMnS
i	i	k9
změnu	změna	k1gFnSc4
vlajky	vlajka	k1gFnSc2
na	na	k7c4
již	již	k6eAd1
užívanou	užívaný	k2eAgFnSc4d1
černo-červenou	černo-červený	k2eAgFnSc4d1
bikolóru	bikolóra	k1gFnSc4
se	s	k7c7
svislými	svislý	k2eAgInPc7d1
pruhy	pruh	k1gInPc7
z	z	k7c2
roku	rok	k1gInSc2
1805	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
středu	střed	k1gInSc2
vlajky	vlajka	k1gFnSc2
byl	být	k5eAaImAgInS
ale	ale	k8xC
umístěn	umístit	k5eAaPmNgInS
bílý	bílý	k2eAgInSc1d1
obdélník	obdélník	k1gInSc1
s	s	k7c7
pozměněným	pozměněný	k2eAgInSc7d1
státním	státní	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlajka	vlajka	k1gFnSc1
o	o	k7c6
poměru	poměr	k1gInSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
byla	být	k5eAaImAgFnS
oficiálně	oficiálně	k6eAd1
zavedena	zaveden	k2eAgFnSc1d1
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černá	černý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
symbolizovala	symbolizovat	k5eAaImAgFnS
černošské	černošský	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
a	a	k8xC
africké	africký	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
<g/>
,	,	kIx,
červená	červenat	k5eAaImIp3nS
boj	boj	k1gInSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
mulaty	mulat	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
haitského	haitský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
byla	být	k5eAaImAgFnS
stejná	stejný	k2eAgNnPc1d1
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
žlutě	žlutě	k6eAd1
olemovaná	olemovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Znak	znak	k1gInSc1
byl	být	k5eAaImAgInS
(	(	kIx(
<g/>
možná	možná	k9
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
)	)	kIx)
o	o	k7c4
trochu	trocha	k1gFnSc4
větší	veliký	k2eAgFnSc1d2
než	než	k8xS
na	na	k7c6
státní	státní	k2eAgFnSc6d1
vlajce	vlajka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Prozatímní	prozatímní	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
po	po	k7c6
útěku	útěk	k1gInSc6
prezidenta	prezident	k1gMnSc2
Jean-Clauda	Jean-Claud	k1gMnSc2
Duvaliera	Duvalier	k1gMnSc2
(	(	kIx(
<g/>
syna	syn	k1gMnSc2
předchozího	předchozí	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
převzal	převzít	k5eAaPmAgMnS
úřad	úřad	k1gInSc4
po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
únoru	únor	k1gInSc6
1986	#num#	k4
již	již	k9
7	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
oznámila	oznámit	k5eAaPmAgFnS
návrat	návrat	k1gInSc4
k	k	k7c3
dřívějším	dřívější	k2eAgInPc3d1
symbolům	symbol	k1gInPc3
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
prezident	prezident	k1gMnSc1
Henri	Henri	k1gNnSc2
Namphy	Nampha	k1gFnSc2
podepsal	podepsat	k5eAaPmAgMnS
příslušný	příslušný	k2eAgInSc4d1
dekret	dekret	k1gInSc4
a	a	k8xC
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1987	#num#	k4
byly	být	k5eAaImAgInP
symboly	symbol	k1gInPc1
ústavou	ústava	k1gFnSc7
potvrzeny	potvrzen	k2eAgFnPc1d1
a	a	k8xC
zůstaly	zůstat	k5eAaPmAgFnP
v	v	k7c6
platnosti	platnost	k1gFnSc6
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Saint-Domingue	Saint-Domingue	k1gNnSc2
<g/>
/	/	kIx~
<g/>
Ludvíka	Ludvík	k1gMnSc4
XIV	XIV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1625	#num#	k4
<g/>
–	–	k?
<g/>
1791	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Saint-Domingue	Saint-Domingue	k1gFnSc1
(	(	kIx(
<g/>
1791	#num#	k4
<g/>
–	–	k?
<g/>
1798	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
Haiti	Haiti	k1gNnSc2
(	(	kIx(
<g/>
1798	#num#	k4
<g/>
–	–	k?
<g/>
1803	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
<g/>
–	–	k?
<g/>
1805	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
.	.	kIx.
varianta	varianta	k1gFnSc1
vlajky	vlajka	k1gFnSc2
haitské	haitský	k2eAgFnSc2d1
rep	rep	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1807	#num#	k4
<g/>
-	-	kIx~
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
haitských	haitský	k2eAgMnPc2d1
povstalců	povstalec	k1gMnPc2
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
1805	#num#	k4
<g/>
–	–	k?
<g/>
1806	#num#	k4
<g/>
)	)	kIx)
<g/>
Vlajka	vlajka	k1gFnSc1
Haitského	haitský	k2eAgInSc2d1
státu	stát	k1gInSc2
(	(	kIx(
<g/>
1807	#num#	k4
<g/>
–	–	k?
<g/>
1811	#num#	k4
<g/>
)	)	kIx)
<g/>
Vlajka	vlajka	k1gFnSc1
haitského	haitský	k2eAgInSc2d1
královstvi	královstev	k1gFnSc6
(	(	kIx(
<g/>
1811	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
krále	král	k1gMnSc2
Henriho	Henri	k1gMnSc2
I.	I.	kA
(	(	kIx(
<g/>
1811	#num#	k4
<g/>
–	–	k?
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
asi	asi	k9
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
varianta	varianta	k1gFnSc1
vlajky	vlajka	k1gFnSc2
haitské	haitský	k2eAgFnSc2d1
rep	rep	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1807	#num#	k4
<g/>
-	-	kIx~
<g/>
1844	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Haitského	haitský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
(	(	kIx(
<g/>
1849	#num#	k4
<g/>
–	–	k?
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
3	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
haitského	haitský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.crwflags.com/fotw/flags/ht.html	http://www.crwflags.com/fotw/flags/ht.htmla	k1gFnPc2
Haitská	haitský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Flags	Flags	k1gInSc4
Of	Of	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
http://vexilologie.cz/vexilolo/noxx.php?n=35	http://vexilologie.cz/vexilolo/noxx.php?n=35	k4
Zpravodaj	zpravodaj	k1gMnSc1
Vexilolog	Vexilolog	k1gMnSc1
č.	č.	k?
35	#num#	k4
<g/>
,	,	kIx,
leden	leden	k1gInSc1
2010	#num#	k4
<g/>
↑	↑	k?
http://www.crwflags.com/fotw/flags/li_hist.html	http://www.crwflags.com/fotw/flags/li_hist.html	k1gMnSc1
Historie	historie	k1gFnSc2
lichtenštejnské	lichtenštejnský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
<g/>
↑	↑	k?
http://www.crwflags.com/fotw/flags/ht_pres.html	http://www.crwflags.com/fotw/flags/ht_pres.html	k1gInSc1
Vlajka	vlajka	k1gFnSc1
haitského	haitský	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
na	na	k7c4
Flags	Flags	k1gInSc4
Of	Of	k1gMnSc1
The	The	k1gMnSc1
World	World	k1gMnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Haiti	Haiti	k1gNnSc2
</s>
<s>
Haitská	haitský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Haiti	Haiti	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Haitská	haitský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vlajky	vlajka	k1gFnSc2
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gMnSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Bolívie	Bolívie	k1gFnSc2
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Haiti	Haiti	k1gNnSc4
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
Kostarika	Kostarika	k1gFnSc1
•	•	k?
Kuba	Kuba	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Panama	panama	k2eAgFnSc1d1
•	•	k?
Paraguay	Paraguay	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Salvador	Salvador	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgNnSc4d1
•	•	k?
Surinam	Surinam	k1gInSc4
•	•	k?
Svatá	svatat	k5eAaImIp3nS
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
•	•	k?
Uruguay	Uruguay	k1gFnSc1
•	•	k?
Venezuela	Venezuela	k1gFnSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azámořská	azámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Americké	americký	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Anguilla	Anguilla	k1gMnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Aruba	Aruba	k1gMnSc1
(	(	kIx(
<g/>
NLD	NLD	kA
<g/>
)	)	kIx)
•	•	k?
Bermudy	Bermudy	k1gFnPc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Britské	britský	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Curaçao	curaçao	k1gNnSc4
(	(	kIx(
<g/>
NLD	NLD	kA
<g/>
)	)	kIx)
•	•	k?
Falklandy	Falklanda	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Francouzská	francouzský	k2eAgFnSc1d1
Guyana	Guyana	k1gFnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Grónsko	Grónsko	k1gNnSc4
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Guadeloupe	Guadeloupe	k1gMnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Georgie	Georgie	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgInPc1d1
Sandwichovy	Sandwichův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Karibské	karibský	k2eAgNnSc1d1
Nizozemsko	Nizozemsko	k1gNnSc1
(	(	kIx(
<g/>
NLD	NLD	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Martinik	Martinik	k1gMnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Montserrat	Montserrat	k1gMnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Portoriko	Portoriko	k1gNnSc4
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
•	•	k?
Saint	Saint	k1gInSc1
Pierre	Pierr	k1gInSc5
a	a	k8xC
Miquelon	Miquelon	k1gInSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Svatý	svatý	k1gMnSc1
Bartoloměj	Bartoloměj	k1gMnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Svatý	svatý	k1gMnSc1
Martin	Martin	k1gMnSc1
(	(	kIx(
<g/>
FRA	FRA	kA
<g/>
)	)	kIx)
•	•	k?
Svatý	svatý	k1gMnSc1
Martin	Martin	k1gMnSc1
(	(	kIx(
<g/>
NLD	NLD	kA
<g/>
)	)	kIx)
•	•	k?
Turks	Turks	k1gInSc1
a	a	k8xC
Caicos	Caicos	k1gInSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
</s>
