<s>
Intersexuál	Intersexuál	k1gMnSc1
</s>
<s>
Intersexuál	intersexuál	k1gMnSc1
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnPc1
vnitřní	vnitřní	k2eAgInPc4d1
nebo	nebo	k8xC
vnější	vnější	k2eAgInPc4d1
pohlavní	pohlavní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
jsou	být	k5eAaImIp3nP
odlišné	odlišný	k2eAgInPc1d1
od	od	k7c2
typicky	typicky	k6eAd1
mužských	mužský	k2eAgFnPc2d1
či	či	k8xC
ženských	ženský	k2eAgFnPc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgMnSc1
jedinec	jedinec	k1gMnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
navenek	navenek	k6eAd1
pohlavní	pohlavní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
jednoho	jeden	k4xCgNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
jeho	jeho	k3xOp3gInPc4
vnitřní	vnitřní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
pohlaví	pohlaví	k1gNnSc3
opačného	opačný	k2eAgNnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
o	o	k7c4
jedince	jedinec	k1gMnPc4
s	s	k7c7
biologickou	biologický	k2eAgFnSc7d1
kombinací	kombinace	k1gFnSc7
mužských	mužský	k2eAgInPc2d1
a	a	k8xC
ženských	ženský	k2eAgInPc2d1
pohlavních	pohlavní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Moderní	moderní	k2eAgFnSc1d1
medicína	medicína	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
setkává	setkávat	k5eAaImIp3nS
s	s	k7c7
případem	případ	k1gInSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
určení	určení	k1gNnSc1
genitálií	genitálie	k1gFnPc2
není	být	k5eNaImIp3nS
jednoznačné	jednoznačný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přesnější	přesný	k2eAgNnSc4d2
definování	definování	k1gNnSc4
intersexuality	intersexualita	k1gFnSc2
existují	existovat	k5eAaImIp3nP
4	#num#	k4
jednotlivé	jednotlivý	k2eAgFnPc1d1
podoby	podoba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Podoby	podoba	k1gFnPc1
intersexuality	intersexualita	k1gFnSc2
</s>
<s>
Intersexuálové	Intersexuál	k1gMnPc1
jsou	být	k5eAaImIp3nP
pomocí	pomocí	k7c2
této	tento	k3xDgFnSc2
klasifikace	klasifikace	k1gFnSc2
rozděleni	rozdělit	k5eAaPmNgMnP
do	do	k7c2
několika	několik	k4yIc2
kategorií	kategorie	k1gFnPc2
podle	podle	k7c2
typu	typ	k1gInSc2
a	a	k8xC
počtu	počet	k1gInSc2
chromozomů	chromozom	k1gInPc2
jedince	jedinec	k1gMnSc2
a	a	k8xC
také	také	k9
podle	podle	k7c2
zastoupení	zastoupení	k1gNnSc2
jednoho	jeden	k4xCgMnSc2
nebo	nebo	k8xC
obou	dva	k4xCgInPc2
typů	typ	k1gInPc2
gonád	gonáda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
XX	XX	kA
<g/>
,46	,46	k4
intersexuál	intersexuát	k5eAaPmAgInS
</s>
<s>
XX	XX	kA
<g/>
,46	,46	k4
intersexuál	intersexuál	k1gInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k9
ženský	ženský	k2eAgInSc1d1
pseudohermafrodit	pseudohermafrodit	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinci	jedinec	k1gMnPc7
v	v	k7c6
této	tento	k3xDgFnSc6
skupině	skupina	k1gFnSc6
mají	mít	k5eAaImIp3nP
v	v	k7c6
buňce	buňka	k1gFnSc6
soubor	soubor	k1gInSc1
chromozomů	chromozom	k1gInPc2
XX	XX	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
genetickými	genetický	k2eAgFnPc7d1
ženami	žena	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
mají	mít	k5eAaImIp3nP
vyvinutou	vyvinutý	k2eAgFnSc4d1
dělohu	děloha	k1gFnSc4
i	i	k8xC
vaječníky	vaječník	k1gInPc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
jejich	jejich	k3xOp3gFnPc1
genitálie	genitálie	k1gFnPc1
jsou	být	k5eAaImIp3nP
nejednoznačné	jednoznačný	k2eNgFnPc1d1
nebo	nebo	k8xC
převážně	převážně	k6eAd1
mužské	mužský	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
XY	XY	kA
<g/>
,46	,46	k4
intersexuál	intersexuát	k5eAaPmAgInS
</s>
<s>
XY	XY	kA
<g/>
,46	,46	k4
intersexuál	intersexuála	k1gFnPc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
označován	označován	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
mužský	mužský	k2eAgMnSc1d1
pseudohermafrodit	pseudohermafrodit	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
karyotyp	karyotyp	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
chromozomů	chromozom	k1gInPc2
XY	XY	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
tedy	tedy	k9
genetickými	genetický	k2eAgMnPc7d1
muži	muž	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
vnější	vnější	k2eAgFnPc4d1
genitálie	genitálie	k1gFnPc4
jsou	být	k5eAaImIp3nP
buďto	buďto	k8xC
čistě	čistě	k6eAd1
ženské	ženský	k2eAgFnSc2d1
nebo	nebo	k8xC
nejednoznačné	jednoznačný	k2eNgFnSc2d1
–	–	k?
v	v	k7c6
určitém	určitý	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
mezi	mezi	k7c7
mužskými	mužský	k2eAgInPc7d1
a	a	k8xC
ženskými	ženský	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ojediněle	ojediněle	k6eAd1
může	moct	k5eAaImIp3nS
nastat	nastat	k5eAaPmF
případ	případ	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
varlata	varle	k1gNnPc1
jedince	jedinko	k6eAd1
nejsou	být	k5eNaImIp3nP
dostatečně	dostatečně	k6eAd1
vyvinutá	vyvinutý	k2eAgNnPc1d1
nebo	nebo	k8xC
nejsou	být	k5eNaImIp3nP
přítomna	přítomno	k1gNnPc4
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ovotestikulátní	Ovotestikulátní	k2eAgInSc1d1
intersexuál	intersexuál	k1gInSc1
</s>
<s>
Ovotestikulátní	Ovotestikulátní	k2eAgInSc1d1
intersexuál	intersexuál	k1gInSc1
byl	být	k5eAaImAgInS
dříve	dříve	k6eAd2
známý	známý	k2eAgInSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
pravý	pravý	k2eAgMnSc1d1
hermafrodit	hermafrodit	k1gMnSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
pohlavní	pohlavní	k2eAgFnPc1d1
žlázy	žláza	k1gFnPc1
nejsou	být	k5eNaImIp3nP
jednotné	jednotný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
vnější	vnější	k2eAgFnPc1d1
genitálie	genitálie	k1gFnPc1
mohou	moct	k5eAaImIp3nP
vypadat	vypadat	k5eAaPmF,k5eAaImF
typicky	typicky	k6eAd1
žensky	žensky	k6eAd1
<g/>
,	,	kIx,
typicky	typicky	k6eAd1
mužsky	mužsky	k6eAd1
nebo	nebo	k8xC
také	také	k9
nejednoznačně	jednoznačně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinec	jedinec	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
liší	lišit	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
kombinaci	kombinace	k1gFnSc4
jednoho	jeden	k4xCgNnSc2
varlete	varle	k1gNnSc2
a	a	k8xC
jednoho	jeden	k4xCgInSc2
vaječníku	vaječník	k1gInSc2
nebo	nebo	k8xC
jeho	jeho	k3xOp3gFnSc2
pohlavní	pohlavní	k2eAgFnSc2d1
žlázy	žláza	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
jedno	jeden	k4xCgNnSc1
varle	varle	k1gNnSc1
a	a	k8xC
tzv.	tzv.	kA
ovotestis	ovotestis	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
kombinace	kombinace	k1gFnSc1
varlete	varle	k1gNnSc2
a	a	k8xC
vaječníku	vaječník	k1gInSc2
<g/>
,	,	kIx,
popř.	popř.	kA
jeden	jeden	k4xCgInSc4
vaječník	vaječník	k1gInSc4
a	a	k8xC
ovotestis	ovotestis	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Komplexní	komplexní	k2eAgInSc1d1
intersexuál	intersexuál	k1gInSc1
</s>
<s>
Komplexní	komplexní	k2eAgInSc1d1
intersexuál	intersexuál	k1gInSc1
bývá	bývat	k5eAaImIp3nS
také	také	k9
označován	označovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
chromozomální	chromozomální	k2eAgInSc1d1
intersexuál	intersexuál	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jedinec	jedinec	k1gMnSc1
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
karyotyp	karyotyp	k1gInSc1
XX	XX	kA
ani	ani	k8xC
XY	XY	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
komplexních	komplexní	k2eAgInPc2d1
intersexuálů	intersexuál	k1gInPc2
nastává	nastávat	k5eAaImIp3nS
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
má	mít	k5eAaImIp3nS
jedinec	jedinec	k1gMnSc1
o	o	k7c4
jeden	jeden	k4xCgInSc4
chromozom	chromozom	k1gInSc4
navíc	navíc	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
XXY	XXY	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
naopak	naopak	k6eAd1
o	o	k7c4
jeden	jeden	k4xCgInSc4
méně	málo	k6eAd2
(	(	kIx(
<g/>
X	X	kA
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skupina	skupina	k1gFnSc1
intersexuálů	intersexuál	k1gMnPc2
se	se	k3xPyFc4
od	od	k7c2
předchozích	předchozí	k2eAgNnPc2d1
tří	tři	k4xCgNnPc2
liší	lišit	k5eAaImIp3nP
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
není	být	k5eNaImIp3nS
rozpor	rozpor	k1gInSc1
mezi	mezi	k7c7
vnitřními	vnitřní	k2eAgInPc7d1
a	a	k8xC
vnějšími	vnější	k2eAgInPc7d1
pohlavními	pohlavní	k2eAgInPc7d1
orgány	orgán	k1gInPc7
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
nadbytečný	nadbytečný	k2eAgInSc1d1
či	či	k8xC
chybějící	chybějící	k2eAgInSc1d1
chromozom	chromozom	k1gInSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
vliv	vliv	k1gInSc4
na	na	k7c4
hladinu	hladina	k1gFnSc4
hormonů	hormon	k1gInPc2
a	a	k8xC
pohlavní	pohlavní	k2eAgInSc4d1
vývin	vývin	k1gInSc4
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
ojedinělém	ojedinělý	k2eAgInSc6d1
případě	případ	k1gInSc6
nastává	nastávat	k5eAaImIp3nS
situace	situace	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
člověk	člověk	k1gMnSc1
klasifikovaný	klasifikovaný	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
komplexní	komplexní	k2eAgMnSc1d1
intersexuál	intersexuál	k1gMnSc1
rodí	rodit	k5eAaImIp3nS
s	s	k7c7
různými	různý	k2eAgInPc7d1
chromozomy	chromozom	k1gInPc7
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
buňkách	buňka	k1gFnPc6
v	v	k7c6
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Medicína	medicína	k1gFnSc1
a	a	k8xC
intersexualita	intersexualita	k1gFnSc1
</s>
<s>
Záznamy	záznam	k1gInPc1
o	o	k7c6
intersexuálech	intersexuál	k1gInPc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
už	už	k9
mnoho	mnoho	k4c1
klasifikací	klasifikace	k1gFnPc2
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
se	se	k3xPyFc4
však	však	k9
neustále	neustále	k6eAd1
s	s	k7c7
postupující	postupující	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dřívějších	dřívější	k2eAgNnPc6d1
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
obvyklé	obvyklý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
doktoři	doktor	k1gMnPc1
pacientu	pacient	k1gMnSc6
intersexuálovi	intersexuál	k1gMnSc6
po	po	k7c6
provedení	provedení	k1gNnSc6
operace	operace	k1gFnSc2
zatajili	zatajit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gFnSc4
pravou	pravý	k2eAgFnSc4d1
intersexuální	intersexuální	k2eAgFnSc4d1
identitu	identita	k1gFnSc4
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
nechtěli	chtít	k5eNaImAgMnP
vystavovat	vystavovat	k5eAaImF
přílišnému	přílišný	k2eAgInSc3d1
stresu	stres	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
jev	jev	k1gInSc1
je	být	k5eAaImIp3nS
patrný	patrný	k2eAgInSc1d1
i	i	k9
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
existuje	existovat	k5eAaImIp3nS
skupina	skupina	k1gFnSc1
doktorů	doktor	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
podporují	podporovat	k5eAaImIp3nP
myšlenku	myšlenka	k1gFnSc4
utajení	utajení	k1gNnSc2
intersexuality	intersexualita	k1gFnSc2
před	před	k7c7
pacientem	pacient	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
narození	narození	k1gNnSc6
dítěte	dítě	k1gNnSc2
jsou	být	k5eAaImIp3nP
lékaři	lékař	k1gMnPc1
oprávněni	oprávnit	k5eAaPmNgMnP
posoudit	posoudit	k5eAaPmF
jaké	jaký	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
pohlaví	pohlaví	k1gNnSc4
je	být	k5eAaImIp3nS
pro	pro	k7c4
novorozeně	novorozeně	k1gNnSc4
nejvhodnější	vhodný	k2eAgNnSc4d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
potaz	potaz	k1gInSc4
berou	brát	k5eAaImIp3nP
jak	jak	k6eAd1
typ	typ	k1gInSc4
pohlavních	pohlavní	k2eAgFnPc2d1
žláz	žláza	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
vzhled	vzhled	k1gInSc4
vnějších	vnější	k2eAgInPc2d1
pohlavních	pohlavní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Častěji	často	k6eAd2
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
s	s	k7c7
operací	operace	k1gFnSc7
zkrácení	zkrácení	k1gNnSc2
klitorisu	klitoris	k1gInSc2
a	a	k8xC
umělému	umělý	k2eAgNnSc3d1
vytvoření	vytvoření	k1gNnSc3
vagíny	vagína	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
pro	pro	k7c4
lékaře	lékař	k1gMnSc4
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jednodušší	jednoduchý	k2eAgNnSc1d2
než	než	k8xS
vytvořit	vytvořit	k5eAaPmF
funkční	funkční	k2eAgInSc4d1
penis	penis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
operace	operace	k1gFnSc1
však	však	k9
často	často	k6eAd1
nejsou	být	k5eNaImIp3nP
pouze	pouze	k6eAd1
jednorázovou	jednorázový	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
jich	on	k3xPp3gMnPc2
potřeba	potřeba	k6eAd1
víc	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
lékaři	lékař	k1gMnPc1
stále	stále	k6eAd1
zastávají	zastávat	k5eAaImIp3nP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
operace	operace	k1gFnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
měla	mít	k5eAaImAgNnP
uskutečnit	uskutečnit	k5eAaPmF
co	co	k9
nejdříve	dříve	k6eAd3
po	po	k7c6
narození	narození	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgMnS
jedinec	jedinec	k1gMnSc1
„	„	k?
<g/>
normální	normální	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
postup	postup	k1gInSc4
je	být	k5eAaImIp3nS
ale	ale	k8xC
dle	dle	k7c2
ostatních	ostatní	k2eAgInPc2d1
neadekvátní	adekvátní	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohlavní	pohlavní	k2eAgFnSc1d1
identita	identita	k1gFnSc1
člověka	člověk	k1gMnSc2
se	se	k3xPyFc4
neprojevuje	projevovat	k5eNaImIp3nS
od	od	k7c2
narození	narození	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
může	moct	k5eAaImIp3nS
po	po	k7c6
zákroku	zákrok	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
chirurgové	chirurg	k1gMnPc1
vytvoří	vytvořit	k5eAaPmIp3nP
orgán	orgán	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
nepřísluší	příslušet	k5eNaImIp3nS
skutečnému	skutečný	k2eAgNnSc3d1
pohlaví	pohlaví	k1gNnSc3
<g/>
,	,	kIx,
docházet	docházet	k5eAaImF
u	u	k7c2
jedince	jedinec	k1gMnSc2
k	k	k7c3
depresi	deprese	k1gFnSc3
<g/>
,	,	kIx,
úzkosti	úzkost	k1gFnSc3
a	a	k8xC
dokonce	dokonce	k9
k	k	k7c3
sebevražedným	sebevražedný	k2eAgFnPc3d1
tendencím	tendence	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
ojedinělých	ojedinělý	k2eAgInPc6d1
případech	případ	k1gInPc6
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
ohrožen	ohrožen	k2eAgInSc1d1
život	život	k1gInSc1
dítěte	dítě	k1gNnSc2
<g/>
,	,	kIx,
operace	operace	k1gFnSc2
potřebná	potřebný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
však	však	k9
nijak	nijak	k6eAd1
nezahrnuje	zahrnovat	k5eNaImIp3nS
úpravu	úprava	k1gFnSc4
především	především	k9
pohlavních	pohlavní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
a	a	k8xC
intersexualita	intersexualita	k1gFnSc1
</s>
<s>
Postoj	postoj	k1gInSc1
k	k	k7c3
této	tento	k3xDgFnSc3
otázce	otázka	k1gFnSc3
se	se	k3xPyFc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
nadále	nadále	k6eAd1
vyvíjí	vyvíjet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastějším	častý	k2eAgInSc7d3
problémem	problém	k1gInSc7
společnosti	společnost	k1gFnSc2
je	být	k5eAaImIp3nS
předčasné	předčasný	k2eAgNnSc1d1
odsouzení	odsouzení	k1gNnSc1
takového	takový	k3xDgMnSc2
jedince	jedinec	k1gMnSc2
na	na	k7c6
základě	základ	k1gInSc6
neznalosti	neznalost	k1gFnSc2
daného	daný	k2eAgNnSc2d1
tématu	téma	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
potřeba	potřeba	k6eAd1
přidělit	přidělit	k5eAaPmF
člověku	člověk	k1gMnSc3
pohlaví	pohlaví	k1gNnPc4
vychází	vycházet	k5eAaImIp3nS
především	především	k9
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedinec	jedinec	k1gMnSc1
potřebuje	potřebovat	k5eAaImIp3nS
socializovat	socializovat	k5eAaBmF
a	a	k8xC
začlenit	začlenit	k5eAaPmF
se	se	k3xPyFc4
do	do	k7c2
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nutnost	nutnost	k1gFnSc4
vyplněného	vyplněný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
na	na	k7c6
rodném	rodný	k2eAgInSc6d1
listu	list	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c6
formulářích	formulář	k1gInPc6
či	či	k8xC
přidělení	přidělení	k1gNnSc6
jména	jméno	k1gNnSc2
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
ale	ale	k8xC
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
osoba	osoba	k1gFnSc1
s	s	k7c7
intersexuální	intersexuální	k2eAgFnSc7d1
identitou	identita	k1gFnSc7
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
necítí	cítit	k5eNaImIp3nS
ani	ani	k8xC
ženou	žena	k1gFnSc7
ani	ani	k8xC
mužem	muž	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgInPc6
případech	případ	k1gInPc6
hovoříme	hovořit	k5eAaImIp1nP
o	o	k7c6
„	„	k?
<g/>
třetím	třetí	k4xOgMnSc6
pohlaví	pohlaví	k1gNnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
o	o	k7c6
pohlaví	pohlaví	k1gNnSc6
neurčeném	určený	k2eNgNnSc6d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Psychologický	psychologický	k2eAgInSc4d1
dopad	dopad	k1gInSc4
</s>
<s>
Zákrok	zákrok	k1gInSc1
upravující	upravující	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
a	a	k8xC
funkci	funkce	k1gFnSc4
pohlavních	pohlavní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
tématem	téma	k1gNnSc7
diskuzí	diskuze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
novým	nový	k2eAgNnSc7d1
tisíciletím	tisíciletí	k1gNnSc7
přišel	přijít	k5eAaPmAgMnS
i	i	k9
nový	nový	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
odlišnosti	odlišnost	k1gFnPc4
jednotlivých	jednotlivý	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
a	a	k8xC
jedinci	jedinec	k1gMnPc1
postiženi	postihnout	k5eAaPmNgMnP
tímto	tento	k3xDgInSc7
jevem	jev	k1gInSc7
začali	začít	k5eAaPmAgMnP
o	o	k7c6
intersexualitě	intersexualita	k1gFnSc6
a	a	k8xC
operacích	operace	k1gFnPc6
otevřeně	otevřeně	k6eAd1
hovořit	hovořit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
k	k	k7c3
běžné	běžný	k2eAgFnSc3d1
praxi	praxe	k1gFnSc3
v	v	k7c6
dřívějších	dřívější	k2eAgNnPc6d1
letech	léto	k1gNnPc6
patřilo	patřit	k5eAaImAgNnS
neinformovat	informovat	k5eNaBmF
dítě	dítě	k1gNnSc1
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
odlišnosti	odlišnost	k1gFnSc6
a	a	k8xC
místo	místo	k7c2
toho	ten	k3xDgNnSc2
rychle	rychle	k6eAd1
udělat	udělat	k5eAaPmF
zákrok	zákrok	k1gInSc1
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
intersexuálů	intersexuál	k1gInPc2
uvedlo	uvést	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
nich	on	k3xPp3gFnPc6
toto	tento	k3xDgNnSc4
zacházení	zacházení	k1gNnSc1
zanechalo	zanechat	k5eAaPmAgNnS
trauma	trauma	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jim	on	k3xPp3gMnPc3
brání	bránit	k5eAaImIp3nS
v	v	k7c6
normálním	normální	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popisují	popisovat	k5eAaImIp3nP
pocity	pocit	k1gInPc1
úzkosti	úzkost	k1gFnSc2
<g/>
,	,	kIx,
strachu	strach	k1gInSc2
<g/>
,	,	kIx,
vzteku	vztek	k1gInSc2
a	a	k8xC
ponížení	ponížení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Morgan	morgan	k1gMnSc1
Holmes	Holmes	k1gMnSc1
<g/>
,	,	kIx,
socioložka	socioložka	k1gFnSc1
a	a	k8xC
intersexuálka	intersexuálka	k1gFnSc1
<g/>
,	,	kIx,
vzpomíná	vzpomínat	k5eAaImIp3nS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
s	s	k7c7
ní	on	k3xPp3gFnSc7
bylo	být	k5eAaImAgNnS
zacházeno	zacházen	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
s	s	k7c7
majetkem	majetek	k1gInSc7
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhlas	souhlas	k1gInSc1
otce	otec	k1gMnSc4
stačil	stačit	k5eAaBmAgInS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
operace	operace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
měla	mít	k5eAaImAgFnS
udělat	udělat	k5eAaPmF
„	„	k?
<g/>
normálního	normální	k2eAgMnSc4d1
<g/>
“	“	k?
jedince	jedinec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
toho	ten	k3xDgInSc2
ale	ale	k9
až	až	k6eAd1
do	do	k7c2
dospělosti	dospělost	k1gFnSc2
cítila	cítit	k5eAaImAgFnS
odpor	odpor	k1gInSc4
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
upravenému	upravený	k2eAgNnSc3d1
tělu	tělo	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
udávají	udávat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
tělesná	tělesný	k2eAgFnSc1d1
odlišnost	odlišnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nebyla	být	k5eNaImAgFnS
operativně	operativně	k6eAd1
upravena	upravit	k5eAaPmNgFnS
<g/>
,	,	kIx,
způsobuje	způsobovat	k5eAaImIp3nS
stejné	stejný	k2eAgInPc4d1
pocity	pocit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
intersexualita	intersexualita	k1gFnSc1
nezpůsobovala	způsobovat	k5eNaImAgFnS
depresi	deprese	k1gFnSc4
pouze	pouze	k6eAd1
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jejich	jejich	k3xOp3gFnSc3
rodině	rodina	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
styděla	stydět	k5eAaImAgFnS
za	za	k7c4
svého	svůj	k3xOyFgMnSc4
potomka	potomek	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Je	být	k5eAaImIp3nS
tedy	tedy	k9
těžké	těžký	k2eAgNnSc1d1
určit	určit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
zákrok	zákrok	k1gInSc4
vhodné	vhodný	k2eAgNnSc1d1
provést	provést	k5eAaPmF
v	v	k7c6
útlém	útlý	k2eAgInSc6d1
věku	věk	k1gInSc6
nebo	nebo	k8xC
až	až	k6eAd1
později	pozdě	k6eAd2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
dítě	dítě	k1gNnSc1
připraveno	připravit	k5eAaPmNgNnS
rozhodnout	rozhodnout	k5eAaPmF
se	se	k3xPyFc4
samo	sám	k3xTgNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomoci	pomoct	k5eAaPmF
by	by	kYmCp3nS
ale	ale	k9
mohlo	moct	k5eAaImAgNnS
oficiální	oficiální	k2eAgNnSc1d1
uznávání	uznávání	k1gNnSc1
třetího	třetí	k4xOgNnSc2
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
osoba	osoba	k1gFnSc1
nemusela	muset	k5eNaImAgFnS
cítit	cítit	k5eAaImF
pod	pod	k7c7
tlakem	tlak	k1gInSc7
vybrat	vybrat	k5eAaPmF
si	se	k3xPyFc3
jedno	jeden	k4xCgNnSc4
nebo	nebo	k8xC
druhé	druhý	k4xOgNnSc4
pohlaví	pohlaví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
zemím	zem	k1gFnPc3
uznávajícím	uznávající	k2eAgFnPc3d1
třetí	třetí	k4xOgNnSc1
pohlaví	pohlaví	k1gNnPc2
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
nebo	nebo	k8xC
Německo	Německo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Intersex	Intersex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Nations	Nations	k1gInSc4
<g/>
,	,	kIx,
Office	Office	kA
of	of	k?
the	the	k?
High	High	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
for	forum	k1gNnPc2
Human	Humana	k1gFnPc2
Rights	Rightsa	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://unfe.org/system/unfe-65-Intersex_Factsheet_ENGLISH.pdf	https://unfe.org/system/unfe-65-Intersex_Factsheet_ENGLISH.pdf	k1gInSc1
<g/>
↑	↑	k?
Velký	velký	k2eAgInSc1d1
sociologický	sociologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Karolinum	Karolinum	k1gNnSc1
<g/>
,	,	kIx,
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
8071843113	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Intersexualita	Intersexualita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
lékařský	lékařský	k2eAgInSc1d1
slovník	slovník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maxford	Maxforda	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://lekarske.slovniky.cz/pojem/intersexualita	http://lekarske.slovniky.cz/pojem/intersexualita	k1gFnSc1
<g/>
↑	↑	k?
MONEY	MONEY	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
a	a	k8xC
Anke	Anke	k1gInSc1
A.	A.	kA
EHRHARDT	EHRHARDT	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Man	Man	k1gMnSc1
</s>
<s>
&	&	k?
Woman	Woman	k1gMnSc1
Boy	boy	k1gMnSc1
&	&	k?
Girl	girl	k1gFnSc1
<g/>
:	:	kIx,
Differentiation	Differentiation	k1gInSc1
and	and	k?
dimorphism	dimorphism	k1gInSc1
of	of	k?
gender	gender	k1gInSc1
identity	identita	k1gFnSc2
</s>
<s>
from	from	k6eAd1
conception	conception	k1gInSc1
to	ten	k3xDgNnSc1
maturity	maturita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
<g/>
:	:	kIx,
The	The	k1gFnSc1
Johns	Johns	k1gInSc4
Hopkins	Hopkins	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8018	#num#	k4
<g/>
-	-	kIx~
<g/>
1405	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Intersexuál	Intersexuál	k1gMnSc1
<g/>
:	:	kIx,
XX	XX	kA
<g/>
,46	,46	k4
Intersexuál	Intersexuála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intersexuál	Intersexuál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
WordPress	WordPress	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://intersexual.sk/podoby-intersexuality/xx46-intersexual/	http://intersexual.sk/podoby-intersexuality/xx46-intersexual/	k4
Archivováno	archivován	k2eAgNnSc4d1
28	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Intersexuál	Intersexuál	k1gMnSc1
<g/>
:	:	kIx,
XY	XY	kA
<g/>
,46	,46	k4
Intersexuál	Intersexuála	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intersexuál	Intersexuál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
WordPress	WordPress	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://intersexual.sk/podoby-intersexuality/xy46-intersexual/	http://intersexual.sk/podoby-intersexuality/xy46-intersexual/	k4
Archivováno	archivován	k2eAgNnSc4d1
28	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Intersexuál	Intersexuál	k1gInSc1
<g/>
:	:	kIx,
Ovotestikulárny	Ovotestikulárna	k1gFnPc1
Intersexuál	Intersexuál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intersexuál	Intersexuál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
WordPress	WordPress	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://intersexual.sk/podoby-intersexuality/ovotestikularny-intersexual/	http://intersexual.sk/podoby-intersexuality/ovotestikularny-intersexual/	k?
Archivováno	archivován	k2eAgNnSc1d1
27	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Intersexuál	Intersexuál	k1gMnSc1
<g/>
:	:	kIx,
Komplexný	Komplexný	k1gMnSc1
Intersexuál	Intersexuál	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Intersexuál	Intersexuál	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
WordPress	WordPress	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://intersexual.sk/podoby-intersexuality/komplexny-intersexual/	http://intersexual.sk/podoby-intersexuality/komplexny-intersexual/	k?
Archivováno	archivován	k2eAgNnSc1d1
28	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
1	#num#	k4
2	#num#	k4
Is	Is	k1gFnPc2
Frowing	Frowing	k1gInSc4
up	up	k?
in	in	k?
Silence	silenka	k1gFnSc3
Better	Better	k1gMnSc1
Than	Than	k1gMnSc1
Growing	Growing	k1gInSc1
up	up	k?
Different	Different	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Intersex	Intersex	k1gInSc1
Society	societa	k1gFnSc2
of	of	k?
North	North	k1gMnSc1
America	America	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
North	North	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
ISNA	ISNA	kA
<g/>
,	,	kIx,
1993	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
www.isna.org	www.isna.org	k1gMnSc1
<g/>
↑	↑	k?
FAUSTO-STERLING	FAUSTO-STERLING	k1gMnSc4
<g/>
,	,	kIx,
Anne	Ann	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sexing	Sexing	k1gInSc4
the	the	k?
Body	bod	k1gInPc4
<g/>
:	:	kIx,
Gender	Gender	k1gMnSc1
Politics	Politicsa	k1gFnPc2
and	and	k?
the	the	k?
Construction	Construction	k1gInSc1
of	of	k?
sexuality	sexualita	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Basic	Basic	kA
Books	Books	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0-465-07713-71	0-465-07713-71	k4
2	#num#	k4
3	#num#	k4
Creighton	Creighton	k1gInSc1
<g/>
,	,	kIx,
Sarah	Sarah	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Lih-Mei	Lih-Me	k1gFnSc2
Liao	Liao	k1gMnSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
.	.	kIx.
“	“	k?
<g/>
Changing	Changing	k1gInSc1
Attitudes	Attitudesa	k1gFnPc2
To	to	k9
Sex	sex	k1gInSc1
Assignment	Assignment	k1gMnSc1
In	In	k1gFnPc2
Intersex	Intersex	k1gInSc4
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bju	Bju	k?
International	International	k1gMnSc1
2004	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
7	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
j	j	k?
<g/>
.1464	.1464	k4
<g/>
-	-	kIx~
<g/>
410	#num#	k4
<g/>
X	X	kA
<g/>
.2003	.2003	k4
<g/>
.04694	.04694	k4
<g/>
.	.	kIx.
<g/>
x.	x.	k?
<g/>
↑	↑	k?
Intersexion	Intersexion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
svět	svět	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Člověk	člověk	k1gMnSc1
v	v	k7c6
tísni	tíseň	k1gFnSc6
<g/>
,	,	kIx,
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
http://www.jedensvet.cz/2013/filmy-a-z/22242-intersexion	http://www.jedensvet.cz/2013/filmy-a-z/22242-intersexion	k1gInSc1
<g/>
↑	↑	k?
Intersex	Intersex	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gInSc1
Nations	Nations	k1gInSc4
<g/>
,	,	kIx,
Office	Office	kA
of	of	k?
the	the	k?
High	High	k1gMnSc1
Commissioner	Commissioner	k1gMnSc1
for	forum	k1gNnPc2
Human	Humana	k1gFnPc2
Rights	Rightsa	k1gFnPc2
<g/>
,	,	kIx,
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
z	z	k7c2
<g/>
:	:	kIx,
https://unfe.org/system/unfe-65-Intersex_Factsheet_ENGLISH.pdf	https://unfe.org/system/unfe-65-Intersex_Factsheet_ENGLISH.pdf	k1gMnSc1
<g/>
↑	↑	k?
Herdt	Herdt	k1gMnSc1
<g/>
,	,	kIx,
Gilbert	Gilbert	k1gMnSc1
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Third	Third	k1gInSc1
Sex	sex	k1gInSc4
<g/>
,	,	kIx,
Third	Third	k1gMnSc1
Gender	Gender	k1gMnSc1
<g/>
:	:	kIx,
Beyond	Beyond	k1gMnSc1
Sexual	Sexual	k1gMnSc1
Dimorphism	Dimorphism	k1gMnSc1
In	In	k1gMnSc1
Culture	Cultur	k1gMnSc5
And	Anda	k1gFnPc2
History	Histor	k1gInPc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
rd	rd	k?
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Zone	Zone	k1gNnSc1
Books	Booksa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Patel	patela	k1gFnPc2
<g/>
,	,	kIx,
Amisha	Amisha	k1gFnSc1
R.	R.	kA
2010	#num#	k4
<g/>
.	.	kIx.
“	“	k?
<g/>
India	indium	k1gNnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Hijras	Hijrasa	k1gFnPc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Case	Cas	k1gFnSc2
For	forum	k1gNnPc2
Transgender	Transgender	k1gMnSc1
Rights	Rightsa	k1gFnPc2
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
George	Georg	k1gInSc2
Washington	Washington	k1gInSc1
International	International	k1gMnSc2
Law	Law	k1gMnSc2
Review	Review	k1gMnSc2
2010	#num#	k4
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
28	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.230	10.230	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2928520	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dow	Dow	k1gMnSc1
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
“	“	k?
<g/>
Neither	Neithra	k1gFnPc2
A	A	kA
Man	Man	k1gMnSc1
Nor	Nor	k1gMnSc1
Woman	Woman	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
The	The	k1gFnSc2
Sydney	Sydney	k1gNnSc2
Morning	Morning	k1gInSc1
Herald.	Herald.	k1gFnSc4
http://www.smh.com.au/nsw/neither-man-nor-woman-20100626-zaye.html.	http://www.smh.com.au/nsw/neither-man-nor-woman-20100626-zaye.html.	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4027484-6	4027484-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85060401	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85060401	#num#	k4
</s>
