<s>
Párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
jsou	být	k5eAaImIp3nP
nukleové	nukleový	k2eAgFnPc1d1
báze	báze	k1gFnPc1
(	(	kIx(
<g/>
ať	ať	k9
v	v	k7c6
DNA	DNA	kA
či	či	k8xC
v	v	k7c6
RNA	RNA	kA
<g/>
)	)	kIx)
navzájem	navzájem	k6eAd1
pospojovány	pospojován	k2eAgInPc1d1
pomocí	pomocí	k7c2
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
<g/>
.	.	kIx.
</s>