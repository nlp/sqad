<s desamb="1">
V	v	k7c6
typickém	typický	k2eAgInSc6d1
případě	případ	k1gInSc6
se	se	k3xPyFc4
párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
základních	základní	k2eAgNnPc2d1
watson-crickovských	watson-crickovský	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
komplementarity	komplementarita	k1gFnSc2
<g/>
,	,	kIx,
tzn.	tzn.	kA
báze	báze	k1gFnSc1
adenin	adenin	k1gInSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
páruje	párovat	k5eAaImIp3nS
s	s	k7c7
thyminem	thymin	k1gInSc7
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
či	či	k8xC
s	s	k7c7
uracilem	uracil	k1gInSc7
v	v	k7c6
dsRNA	dsRNA	k?
<g/>
]	]	kIx)
a	a	k8xC
báze	báze	k1gFnSc1
guanin	guanin	k1gInSc1
(	(	kIx(
<g/>
G	G	kA
<g/>
)	)	kIx)
páruje	párovat	k5eAaImIp3nS
s	s	k7c7
cytosinem	cytosino	k1gNnSc7
<g/>
.	.	kIx.
</s>
