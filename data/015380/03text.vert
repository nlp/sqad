<s>
Párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
</s>
<s>
Dva	dva	k4xCgInPc4
základní	základní	k2eAgInPc4d1
komplementární	komplementární	k2eAgInPc4d1
páry	pár	k1gInPc4
</s>
<s>
Párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
jsou	být	k5eAaImIp3nP
nukleové	nukleový	k2eAgFnPc1d1
báze	báze	k1gFnPc1
(	(	kIx(
<g/>
ať	ať	k9
v	v	k7c6
DNA	DNA	kA
či	či	k8xC
v	v	k7c6
RNA	RNA	kA
<g/>
)	)	kIx)
navzájem	navzájem	k6eAd1
pospojovány	pospojován	k2eAgInPc1d1
pomocí	pomocí	k7c2
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
typickém	typický	k2eAgInSc6d1
případě	případ	k1gInSc6
se	se	k3xPyFc4
párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
základních	základní	k2eAgNnPc2d1
watson-crickovských	watson-crickovský	k2eAgNnPc2d1
pravidel	pravidlo	k1gNnPc2
komplementarity	komplementarita	k1gFnSc2
<g/>
,	,	kIx,
tzn.	tzn.	kA
báze	báze	k1gFnSc1
adenin	adenin	k1gInSc1
(	(	kIx(
<g/>
A	A	kA
<g/>
)	)	kIx)
páruje	párovat	k5eAaImIp3nS
s	s	k7c7
thyminem	thymin	k1gInSc7
(	(	kIx(
<g/>
T	T	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
či	či	k8xC
s	s	k7c7
uracilem	uracil	k1gInSc7
v	v	k7c6
dsRNA	dsRNA	k?
<g/>
]	]	kIx)
a	a	k8xC
báze	báze	k1gFnSc1
guanin	guanin	k1gInSc1
(	(	kIx(
<g/>
G	G	kA
<g/>
)	)	kIx)
páruje	párovat	k5eAaImIp3nS
s	s	k7c7
cytosinem	cytosino	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<s>
</s>
Existují	existovat	k5eAaImIp3nP
však	však	k9
i	i	k9
alternativní	alternativní	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
párování	párování	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
umožňují	umožňovat	k5eAaImIp3nP
vznik	vznik	k1gInSc4
některých	některý	k3yIgFnPc2
méně	málo	k6eAd2
obvyklých	obvyklý	k2eAgFnPc2d1
situací	situace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
komplementární	komplementární	k2eAgInSc4d1
pár	pár	k4xCyI
bází	báze	k1gFnPc2
(	(	kIx(
<g/>
base	basa	k1gFnSc3
pair	pair	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
bp	bp	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Watson-crickovské	Watson-crickovský	k2eAgNnSc1d1
párování	párování	k1gNnSc1
</s>
<s>
Dvěma	dva	k4xCgInPc7
základními	základní	k2eAgInPc7d1
páry	pár	k1gInPc7
v	v	k7c6
typické	typický	k2eAgFnSc6d1
dvouvláknové	dvouvláknový	k2eAgFnSc6d1
DNA	DNA	kA
je	být	k5eAaImIp3nS
AT	AT	kA
pár	pár	k4xCyI
(	(	kIx(
<g/>
adenin	adenin	k1gInSc1
+	+	kIx~
thymin	thymin	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
GC	GC	kA
pár	pár	k4xCyI
(	(	kIx(
<g/>
guanin	guanin	k1gInSc1
+	+	kIx~
cytosin	cytosin	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
dvouvláknové	dvouvláknový	k2eAgFnSc6d1
RNA	RNA	kA
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
GC	GC	kA
pár	pár	k4xCyI
rovněž	rovněž	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
druhým	druhý	k4xOgInSc7
základním	základní	k2eAgInSc7d1
párem	pár	k1gInSc7
je	být	k5eAaImIp3nS
AU	au	k0
pár	pár	k4xCyI
(	(	kIx(
<g/>
adenin	adenin	k1gInSc1
<g/>
+	+	kIx~
<g/>
uracil	uracil	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
komplementární	komplementární	k2eAgInPc4d1
páry	pár	k1gInPc4
platí	platit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
purin	purin	k1gInSc1
(	(	kIx(
<g/>
A	A	kA
či	či	k8xC
G	G	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
je	být	k5eAaImIp3nS
pyrimidin	pyrimidin	k1gInSc1
(	(	kIx(
<g/>
C	C	kA
<g/>
,	,	kIx,
T	T	kA
<g/>
,	,	kIx,
U	U	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jakékoliv	jakýkoliv	k3yIgFnSc2
jiné	jiný	k2eAgFnSc2d1
kombinace	kombinace	k1gFnSc2
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
by	by	kYmCp3nP
silně	silně	k6eAd1
deformovaly	deformovat	k5eAaImAgInP
dvoušroubovici	dvoušroubovice	k1gFnSc4
B-DNA	B-DNA	k1gMnPc2
a	a	k8xC
proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
živých	živý	k2eAgInPc6d1
organismech	organismus	k1gInPc6
téměř	téměř	k6eAd1
nevyskytují	vyskytovat	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
GC	GC	kA
páru	pár	k1gInSc6
jsou	být	k5eAaImIp3nP
mezi	mezi	k7c7
guaninem	guanin	k1gInSc7
a	a	k8xC
cytosinem	cytosin	k1gInSc7
tři	tři	k4xCgInPc4
vodíkové	vodíkový	k2eAgInPc4d1
můstky	můstek	k1gInPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
AT	AT	kA
páru	pár	k1gInSc6
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
dva	dva	k4xCgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
učebnicích	učebnice	k1gFnPc6
se	se	k3xPyFc4
běžně	běžně	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
přítomnost	přítomnost	k1gFnSc1
tří	tři	k4xCgFnPc2
vodíkových	vodíkový	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
mezi	mezi	k7c7
G	G	kA
a	a	k8xC
C	C	kA
je	být	k5eAaImIp3nS
důvodem	důvod	k1gInSc7
vyšší	vysoký	k2eAgFnSc2d2
stability	stabilita	k1gFnSc2
oblastí	oblast	k1gFnPc2
DNA	DNA	kA
bohatých	bohatý	k2eAgInPc2d1
na	na	k7c4
GC	GC	kA
páry	pára	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
pravou	pravý	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
tohoto	tento	k3xDgInSc2
jevu	jev	k1gInSc2
silné	silný	k2eAgFnPc1d1
patrové	patrový	k2eAgFnPc1d1
interakce	interakce	k1gFnPc1
mezi	mezi	k7c7
nad	nad	k7c7
sebou	se	k3xPyFc7
umístěným	umístěný	k2eAgInSc7d1
guaninovou	guaninový	k2eAgFnSc7d1
a	a	k8xC
cytosinovou	cytosinový	k2eAgFnSc7d1
nukleovou	nukleový	k2eAgFnSc7d1
bází	báze	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chargaffova	Chargaffův	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
říkají	říkat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
DNA	DNA	kA
je	být	k5eAaImIp3nS
stejný	stejný	k2eAgInSc4d1
počet	počet	k1gInSc4
adeninových	adeninový	k2eAgInPc2d1
a	a	k8xC
thyminových	thyminův	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
(	(	kIx(
<g/>
A	A	kA
=	=	kIx~
T	T	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
samé	samý	k3xTgFnPc1
platí	platit	k5eAaImIp3nP
pro	pro	k7c4
guanin	guanin	k1gInSc4
s	s	k7c7
cytosinem	cytosin	k1gMnSc7
(	(	kIx(
<g/>
G	G	kA
=	=	kIx~
C	C	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
nic	nic	k3yNnSc1
neříká	říkat	k5eNaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
poměru	poměr	k1gInSc6
jsou	být	k5eAaImIp3nP
GC	GC	kA
a	a	k8xC
AT	AT	kA
páry	pára	k1gFnSc2
v	v	k7c6
molekule	molekula	k1gFnSc6
DNA	dno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	s	k7c7
tzv.	tzv.	kA
obsah	obsah	k1gInSc1
GC	GC	kA
(	(	kIx(
<g/>
GC	GC	kA
content	content	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
zastoupení	zastoupení	k1gNnPc4
GC	GC	kA
párů	pár	k1gInPc2
v	v	k7c4
DNA	DNA	kA
<g/>
,	,	kIx,
pohybuje	pohybovat	k5eAaImIp3nS
u	u	k7c2
bakterií	bakterie	k1gFnPc2
od	od	k7c2
25	#num#	k4
do	do	k7c2
75	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
u	u	k7c2
savců	savec	k1gMnPc2
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
39-46	39-46	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alternativní	alternativní	k2eAgNnSc1d1
párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
</s>
<s>
G	G	kA
kvartet	kvartet	k1gInSc1
<g/>
,	,	kIx,
ukázka	ukázka	k1gFnSc1
alternativního	alternativní	k2eAgNnSc2d1
párování	párování	k1gNnSc2
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
dalších	další	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
pomocí	pomocí	k7c2
vodíkových	vodíkový	k2eAgInPc2d1
můstků	můstek	k1gInPc2
spárovat	spárovat	k5eAaPmF,k5eAaImF
báze	báze	k1gFnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
atomů	atom	k1gInPc2
schopných	schopný	k2eAgMnPc2d1
podílet	podílet	k5eAaImF
se	se	k3xPyFc4
na	na	k7c6
vzniku	vznik	k1gInSc6
vodíkových	vodíkový	k2eAgFnPc2d1
vazeb	vazba	k1gFnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
molekulách	molekula	k1gFnPc6
purinů	purin	k1gInPc2
i	i	k9
pyrimidinů	pyrimidin	k1gInPc2
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samostatnou	samostatný	k2eAgFnSc7d1
kapitolou	kapitola	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
hoogsteenovské	hoogsteenovský	k2eAgNnSc1d1
párování	párování	k1gNnSc1
pojmenované	pojmenovaný	k2eAgNnSc1d1
podle	podle	k7c2
Karsta	Karst	k1gMnSc2
Hoogsteena	Hoogsteen	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jako	jako	k9
první	první	k4xOgMnSc1
popsal	popsat	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
vazeb	vazba	k1gFnPc2
mezi	mezi	k7c7
adeninem	adenin	k1gInSc7
a	a	k8xC
thyminem	thymin	k1gInSc7
<g/>
,	,	kIx,
hoogsteenovské	hoogsteenovský	k2eAgInPc4d1
páry	pár	k1gInPc4
vznikají	vznikat	k5eAaImIp3nP
buď	buď	k8xC
mezi	mezi	k7c7
NH2	NH2	k1gMnPc7
skupinou	skupina	k1gFnSc7
adeninu	adenin	k1gInSc2
a	a	k8xC
ketoskupinou	ketoskupina	k1gFnSc7
thyminu	thymin	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
mezi	mezi	k7c4
N7	N7	k1gFnSc4
dusíkem	dusík	k1gInSc7
adeninu	adenin	k1gInSc2
a	a	k8xC
vodíkem	vodík	k1gInSc7
na	na	k7c4
C1	C1	k1gFnSc4
thyminu	thymin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
mezi	mezi	k7c7
guaninem	guanin	k1gInSc7
a	a	k8xC
cytosinem	cytosin	k1gInSc7
může	moct	k5eAaImIp3nS
vznikat	vznikat	k5eAaImF
alternativní	alternativní	k2eAgNnSc1d1
párování	párování	k1gNnSc1
mezi	mezi	k7c7
ketoskupinou	ketoskupina	k1gFnSc7
na	na	k7c6
šesté	šestý	k4xOgFnSc6
pozici	pozice	k1gFnSc6
guaninu	guanin	k1gInSc2
a	a	k8xC
aminoskupinou	aminoskupina	k1gFnSc7
na	na	k7c6
čtvrtém	čtvrtý	k4xOgInSc6
uhlíku	uhlík	k1gInSc6
cytosinu	cytosina	k1gFnSc4
<g/>
,	,	kIx,
případně	případně	k6eAd1
mezi	mezi	k7c7
dusíkem	dusík	k1gInSc7
na	na	k7c6
sedmé	sedmý	k4xOgFnSc6
pozici	pozice	k1gFnSc6
guaninu	guanin	k1gInSc2
a	a	k8xC
protonovaným	protonovaný	k2eAgInSc7d1
dusíkem	dusík	k1gInSc7
na	na	k7c4
pozici	pozice	k1gFnSc4
N1	N1	k1gFnSc4
cytosinu	cytosina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hoogsteenovské	Hoogsteenovský	k2eAgNnSc1d1
párování	párování	k1gNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
vznik	vznik	k1gInSc4
tzv.	tzv.	kA
trojvláknové	trojvláknový	k2eAgFnSc2d1
DNA	DNA	kA
(	(	kIx(
<g/>
tripl-helixu	tripl-helix	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
čtyřmi	čtyři	k4xCgInPc7
do	do	k7c2
čtverce	čtverec	k1gInSc2
uspořádanými	uspořádaný	k2eAgInPc7d1
guaniny	guanin	k1gInPc7
dokonce	dokonce	k9
může	moct	k5eAaImIp3nS
vzniknout	vzniknout	k5eAaPmF
tetraplex	tetraplex	k1gInSc1
<g/>
,	,	kIx,
tzv.	tzv.	kA
G-kvartet	G-kvartet	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
wobble	wobble	k6eAd1
párování	párování	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
úsporné	úsporný	k2eAgNnSc1d1
rozeznávání	rozeznávání	k1gNnSc1
kodonů	kodon	k1gMnPc2
pomocí	pomocí	k7c2
tRNA	trnout	k5eAaImSgInS
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
wobble	wobble	k6eAd1
párování	párování	k1gNnPc2
může	moct	k5eAaImIp3nS
například	například	k6eAd1
guanin	guanin	k1gInSc4
vytvářet	vytvářet	k5eAaImF
vazbu	vazba	k1gFnSc4
s	s	k7c7
uracilem	uracil	k1gInSc7
<g/>
;	;	kIx,
někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
rekrutován	rekrutován	k2eAgMnSc1d1
inosin	inosin	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
obecné	obecný	k2eAgFnSc2d1
vazebné	vazebný	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
vázat	vázat	k5eAaImF
se	se	k3xPyFc4
na	na	k7c4
C	C	kA
<g/>
,	,	kIx,
A	A	kA
a	a	k8xC
U.	U.	kA
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nepřirozené	přirozený	k2eNgInPc1d1
páry	pár	k1gInPc1
bází	báze	k1gFnPc2
</s>
<s>
Na	na	k7c4
tuto	tento	k3xDgFnSc4
kapitolu	kapitola	k1gFnSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Nepřirozené	přirozený	k2eNgFnSc2d1
páry	pára	k1gFnSc2
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vědcům	vědec	k1gMnPc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
syntetizovat	syntetizovat	k5eAaImF
již	již	k9
mnoho	mnoho	k4c1
kandidátů	kandidát	k1gMnPc2
na	na	k7c4
nepřirozené	přirozený	k2eNgFnPc4d1
nukleové	nukleový	k2eAgFnPc4d1
báze	báze	k1gFnPc4
<g/>
,	,	kIx,
jen	jen	k9
naprostá	naprostý	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
však	však	k9
skutečně	skutečně	k6eAd1
replikovatelná	replikovatelný	k2eAgFnSc1d1
DNA	dno	k1gNnSc2
polymerázami	polymeráza	k1gFnPc7
a	a	k8xC
ještě	ještě	k6eAd1
menší	malý	k2eAgInSc1d2
počet	počet	k1gInSc1
umožňuje	umožňovat	k5eAaImIp3nS
transkripci	transkripce	k1gFnSc4
do	do	k7c2
RNA	RNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
u	u	k7c2
jediného	jediný	k2eAgInSc2d1
umělého	umělý	k2eAgInSc2d1
páru	pár	k1gInSc2
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
byla	být	k5eAaImAgFnS
dosud	dosud	k6eAd1
prokázána	prokázat	k5eAaPmNgFnS
in	in	k?
vivo	vivo	k6eAd1
funkční	funkční	k2eAgFnPc1d1
ekvivalence	ekvivalence	k1gFnPc1
s	s	k7c7
přirozenými	přirozený	k2eAgInPc7d1
páry	pár	k1gInPc7
(	(	kIx(
<g/>
cytosin-guanin	cytosin-guanina	k1gFnPc2
<g/>
,	,	kIx,
adenin-thymin	adenin-thymin	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
r.	r.	kA
1989	#num#	k4
se	se	k3xPyFc4
švýcarskému	švýcarský	k2eAgMnSc3d1
biochemikovi	biochemik	k1gMnSc3
Stevenu	Steven	k2eAgFnSc4d1
Bennerovi	Bennerův	k2eAgMnPc1d1
a	a	k8xC
jeho	jeho	k3xOp3gInSc3
týmu	tým	k1gInSc3
podařilo	podařit	k5eAaPmAgNnS
implementovat	implementovat	k5eAaImF
do	do	k7c2
struktury	struktura	k1gFnSc2
DNA	DNA	kA
modifikované	modifikovaný	k2eAgFnSc2d1
nukleosidy	nukleosida	k1gFnSc2
iso-cytidin	iso-cytidin	k1gInSc1
a	a	k8xC
iso-guanosin	iso-guanosin	k1gInSc1
párující	párující	k2eAgFnSc2d1
se	se	k3xPyFc4
vodíkovými	vodíkový	k2eAgInPc7d1
můstky	můstek	k1gInPc7
odlišně	odlišně	k6eAd1
od	od	k7c2
C-	C-	k1gFnSc2
<g/>
G.	G.	kA
Nové	Nové	k2eAgInPc1d1
úseky	úsek	k1gInPc1
byly	být	k5eAaImAgInP
in	in	k?
vitro	vitro	k1gNnSc4
schopné	schopný	k2eAgFnSc2d1
replikace	replikace	k1gFnSc2
a	a	k8xC
transkripce	transkripce	k1gFnSc2
do	do	k7c2
RNA	RNA	kA
(	(	kIx(
<g/>
přiřazování	přiřazování	k1gNnSc1
komplementárních	komplementární	k2eAgInPc2d1
deoxyribonukleosidů	deoxyribonukleosid	k1gInPc2
i	i	k8xC
ribonukleosidů	ribonukleosid	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
publikoval	publikovat	k5eAaBmAgInS
objev	objev	k1gInSc1
dalších	další	k2eAgMnPc2d1
pozměněných	pozměněný	k2eAgMnPc2d1
nukleosidů	nukleosid	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
uměle	uměle	k6eAd1
vytvořeného	vytvořený	k2eAgInSc2d1
"	"	kIx"
<g/>
κ	κ	k?
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
3	#num#	k4
<g/>
-β	-β	k?
<g/>
(	(	kIx(
<g/>
2,6	2,6	k4
<g/>
-diaminopyrimidin	-diaminopyrimidin	k1gInSc1
<g/>
))	))	k?
<g/>
,	,	kIx,
párujícího	párující	k2eAgMnSc2d1
se	se	k3xPyFc4
buďto	buďto	k8xC
s	s	k7c7
přirozeným	přirozený	k2eAgInSc7d1
xanthosinem	xanthosin	k1gInSc7
(	(	kIx(
<g/>
X	X	kA
<g/>
)	)	kIx)
(	(	kIx(
<g/>
má	mít	k5eAaImIp3nS
však	však	k9
nestálý	stálý	k2eNgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
deoxyribonukleotid	deoxyribonukleotid	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
uměle	uměle	k6eAd1
vytvořeným	vytvořený	k2eAgInSc7d1
"	"	kIx"
<g/>
π	π	k?
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
3	#num#	k4
<g/>
-β	-β	k?
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
-methyl-pyrazolo	-methyl-pyrazola	k1gFnSc5
<g/>
[	[	kIx(
<g/>
4,3	4,3	k4
<g/>
-d	-d	k?
<g/>
]	]	kIx)
<g/>
pyrimidin-	pyrimidin-	k?
<g/>
5,7	5,7	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
H	H	kA
<g/>
,6	,6	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
-dion	-dion	k1gMnSc1
<g/>
))	))	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
r.	r.	kA
2002	#num#	k4
vyvinul	vyvinout	k5eAaPmAgMnS
japonský	japonský	k2eAgMnSc1d1
biochemik	biochemik	k1gMnSc1
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
týmem	tým	k1gInSc7
další	další	k2eAgInSc1d1
nepřirozený	přirozený	k2eNgInSc1d1
pár	pár	k1gInSc1
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
založených	založený	k2eAgFnPc2d1
na	na	k7c6
purinu	purin	k1gInSc6
a	a	k8xC
pyridinu	pyridin	k1gInSc6
<g/>
,	,	kIx,
fungující	fungující	k2eAgNnSc1d1
in	in	k?
vitro	vitro	k1gNnSc1
při	při	k7c6
transkripci	transkripce	k1gFnSc6
a	a	k8xC
translaci	translace	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
"	"	kIx"
<g/>
s	s	k7c7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
2	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
-thienyl	-thienyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
purin	purin	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
y	y	k?
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
pyridin-	pyridin-	k?
<g/>
2	#num#	k4
<g/>
-on	-on	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
pak	pak	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
další	další	k2eAgMnPc1d1
takový	takový	k3xDgInSc4
pár	pár	k1gInSc4
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Ds	Ds	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
7	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
-thienyl	-thienyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
imidazo	imidaza	k1gFnSc5
<g/>
[	[	kIx(
<g/>
4,5	4,5	k4
<g/>
-b	-b	k?
<g/>
]	]	kIx)
<g/>
pyridin	pyridin	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
Pa	pa	k0
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
pyrrol-	pyrrol-	k?
<g/>
2	#num#	k4
<g/>
-karbaldehyd	-karbaldehyd	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
r.	r.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
2009	#num#	k4
objevili	objevit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
vhodnější	vhodný	k2eAgNnSc1d2
chování	chování	k1gNnSc1
má	mít	k5eAaImIp3nS
pár	pár	k4xCyI
"	"	kIx"
<g/>
Ds	Ds	k1gFnSc2
<g/>
"	"	kIx"
s	s	k7c7
nově	nově	k6eAd1
vytvořenou	vytvořený	k2eAgFnSc7d1
bází	báze	k1gFnSc7
"	"	kIx"
<g/>
Px	Px	k1gFnSc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
4	#num#	k4
<g/>
-	-	kIx~
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
-aminohexanamido	-aminohexanamida	k1gFnSc5
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
-propynyl	-propynyl	k1gInSc1
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-nitropyrrol	-nitropyrrola	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Skupina	skupina	k1gFnSc1
amerických	americký	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
vedených	vedený	k2eAgFnPc2d1
Floydem	Floyd	k1gMnSc7
Romesbergem	Romesberg	k1gMnSc7
v	v	k7c6
r.	r.	kA
2008	#num#	k4
vytvořila	vytvořit	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
dvojici	dvojice	k1gFnSc4
párujících	párující	k2eAgMnPc2d1
se	se	k3xPyFc4
deoxynukleosidů	deoxynukleosid	k1gMnPc2
"	"	kIx"
<g/>
d	d	k?
<g/>
5	#num#	k4
<g/>
SICS	SICS	kA
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
2	#num#	k4
<g/>
-	-	kIx~
<g/>
((	((	k?
<g/>
2	#num#	k4
<g/>
R	R	kA
<g/>
,4	,4	k4
<g/>
R	R	kA
<g/>
,5	,5	k4
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
-tetrahydro-	-tetrahydro-	k?
<g/>
4	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-hydroxy-	-hydroxy-	k?
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
hydroxymethyl	hydroxymethyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
furan-	furan-	k?
<g/>
2	#num#	k4
<g/>
-yl	-yl	k?
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-methylisochinolin-	-methylisochinolin-	k?
<g/>
1	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
-thion	-thion	k1gInSc1
<g/>
;	;	kIx,
v	v	k7c6
popularizačních	popularizační	k2eAgInPc6d1
článcích	článek	k1gInPc6
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k9
"	"	kIx"
<g/>
Y	Y	kA
<g/>
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
dNaM	dNaM	k?
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
1,4	1,4	k4
<g/>
-anhydro-	-anhydro-	k?
<g/>
2	#num#	k4
<g/>
-deoxy-	-deoxy-	k?
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
-methoxy-	-methoxy-	k?
<g/>
2	#num#	k4
<g/>
-naftyl	-naftyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-D-erythro-pentitol	-D-erythro-pentitol	k1gInSc1
<g/>
;	;	kIx,
v	v	k7c6
popularizačních	popularizační	k2eAgInPc6d1
článcích	článek	k1gInPc6
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k9
"	"	kIx"
<g/>
X	X	kA
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
–	–	k?
pozor	pozor	k1gInSc1
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
xanthosin	xanthosin	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
již	již	k6eAd1
nejsou	být	k5eNaImIp3nP
odvozeny	odvodit	k5eAaPmNgFnP
z	z	k7c2
purinu	purin	k1gInSc2
a	a	k8xC
pyrimidinu	pyrimidin	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
obě	dva	k4xCgFnPc1
obsahují	obsahovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
kondenzované	kondenzovaný	k2eAgInPc1d1
aromatické	aromatický	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
r.	r.	kA
2012	#num#	k4
tým	tým	k1gInSc1
vedený	vedený	k2eAgInSc1d1
Denisem	Denis	k1gInSc7
Malyshevem	Malyshev	k1gInSc7
publikoval	publikovat	k5eAaBmAgMnS
funkčnost	funkčnost	k1gFnSc4
těchto	tento	k3xDgFnPc2
bází	báze	k1gFnPc2
při	při	k7c6
replikaci	replikace	k1gFnSc6
in	in	k?
vitro	vitro	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
r.	r.	kA
2014	#num#	k4
pak	pak	k6eAd1
úspěšné	úspěšný	k2eAgNnSc4d1
uplatnění	uplatnění	k1gNnSc4
in	in	k?
vivo	vivo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
přidali	přidat	k5eAaPmAgMnP
do	do	k7c2
buňky	buňka	k1gFnSc2
bakterie	bakterie	k1gFnSc2
Escherichia	Escherichius	k1gMnSc2
coli	col	k1gFnSc2
plazmid	plazmida	k1gFnPc2
s	s	k7c7
novými	nový	k2eAgFnPc7d1
bázemi	báze	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakterie	bakterie	k1gFnSc1
s	s	k7c7
plazmidem	plazmid	k1gInSc7
se	se	k3xPyFc4
rozmnožovaly	rozmnožovat	k5eAaImAgFnP
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
by	by	kYmCp3nP
byly	být	k5eAaImAgFnP
úplně	úplně	k6eAd1
obyčejné	obyčejný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uměle	uměle	k6eAd1
syntetizované	syntetizovaný	k2eAgFnSc2d1
báze	báze	k1gFnSc2
<g/>
,	,	kIx,
nutné	nutný	k2eAgFnPc1d1
pro	pro	k7c4
toto	tento	k3xDgNnSc4
množení	množení	k1gNnSc4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
dodávány	dodávat	k5eAaImNgFnP
z	z	k7c2
vnějšího	vnější	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gFnSc4
syntézu	syntéza	k1gFnSc4
buňka	buňka	k1gFnSc1
neumí	umět	k5eNaImIp3nS
<g/>
)	)	kIx)
pomocí	pomocí	k7c2
přenosového	přenosový	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
vypůjčeného	vypůjčený	k2eAgInSc2d1
z	z	k7c2
řas	řasa	k1gFnPc2
–	–	k?
gen	gen	k1gInSc1
pro	pro	k7c4
jeho	jeho	k3xOp3gFnSc4
tvorbu	tvorba	k1gFnSc4
byl	být	k5eAaImAgInS
vložen	vložit	k5eAaPmNgInS
do	do	k7c2
genomu	genom	k1gInSc2
bakterie	bakterie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
pár	pár	k1gInSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
ukázal	ukázat	k5eAaPmAgInS
plně	plně	k6eAd1
funkčně	funkčně	k6eAd1
ekvivalentní	ekvivalentní	k2eAgFnSc1d1
přirozeným	přirozený	k2eAgInSc7d1
párům	pár	k1gInPc3
nukleových	nukleový	k2eAgFnPc2d1
bází	báze	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
dalšímu	další	k2eAgInSc3d1
výzkumu	výzkum	k1gInSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
vyvinout	vyvinout	k5eAaPmF
nový	nový	k2eAgInSc4d1
transportér	transportér	k1gInSc4
syntetických	syntetický	k2eAgFnPc2d1
bází	báze	k1gFnPc2
přes	přes	k7c4
membránu	membrána	k1gFnSc4
a	a	k8xC
po	po	k7c6
chemické	chemický	k2eAgFnSc6d1
optimalizaci	optimalizace	k1gFnSc6
umělého	umělý	k2eAgInSc2d1
páru	pár	k1gInSc2
bází	báze	k1gFnPc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
udržet	udržet	k5eAaPmF
polosyntetický	polosyntetický	k2eAgInSc4d1
genom	genom	k1gInSc4
bakterie	bakterie	k1gFnSc2
Escherichia	Escherichium	k1gNnSc2
coli	coli	k6eAd1
životaschopný	životaschopný	k2eAgMnSc1d1
a	a	k8xC
stabilní	stabilní	k2eAgMnSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
schopný	schopný	k2eAgInSc1d1
přetrvat	přetrvat	k5eAaPmF
minimálně	minimálně	k6eAd1
60	#num#	k4
buněčných	buněčný	k2eAgNnPc2d1
dělení	dělení	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
publikovaly	publikovat	k5eAaBmAgInP
dva	dva	k4xCgInPc1
týmy	tým	k1gInPc1
syntetických	syntetický	k2eAgMnPc2d1
biologů	biolog	k1gMnPc2
výsledky	výsledek	k1gInPc4
prací	práce	k1gFnPc2
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
genetické	genetický	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
o	o	k7c4
další	další	k2eAgInSc4d1
pár	pár	k4xCyI
bází	báze	k1gFnPc2
–	–	k?
"	"	kIx"
<g/>
Z	z	k7c2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
5	#num#	k4
<g/>
-nitro-	-nitro-	k?
<g/>
2	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
-pyridon	-pyridon	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
P	P	kA
<g/>
"	"	kIx"
(	(	kIx(
<g/>
chemicky	chemicky	k6eAd1
2	#num#	k4
<g/>
-aminoimidazo	-aminoimidaza	k1gFnSc5
<g/>
[	[	kIx(
<g/>
1,2	1,2	k4
<g/>
-a	-a	k?
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
,5	,5	k4
<g/>
-triazin-	-triazin-	k?
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
on	on	k3xPp3gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
průkazu	průkaz	k1gInSc2
in	in	k?
vivo	vivo	k6eAd1
jejich	jejich	k3xOp3gFnPc1
rovnocennosti	rovnocennost	k1gFnPc1
a	a	k8xC
kombinovatelnosti	kombinovatelnost	k1gFnPc1
s	s	k7c7
přirozenými	přirozený	k2eAgInPc7d1
páry	pár	k1gInPc7
<g />
.	.	kIx.
</s>
<s hack="1">
bází	báze	k1gFnPc2
v	v	k7c6
řetězci	řetězec	k1gInSc6
DNA	DNA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Pokračující	pokračující	k2eAgFnSc2d1
výzkumné	výzkumný	k2eAgFnSc2d1
práce	práce	k1gFnSc2
vyústily	vyústit	k5eAaPmAgFnP
ve	v	k7c6
vytvoření	vytvoření	k1gNnSc6
tzv.	tzv.	kA
"	"	kIx"
<g/>
hachimoji	hachimoj	k1gInSc6
<g/>
"	"	kIx"
DNA	DNA	kA
a	a	k8xC
RNA	RNA	kA
(	(	kIx(
<g/>
z	z	k7c2
japonských	japonský	k2eAgFnPc2d1
slov	slovo	k1gNnPc2
hachi	hach	k1gFnSc2
–	–	k?
osm	osm	k4xCc4
a	a	k8xC
moji	můj	k3xOp1gMnPc1
–	–	k?
písmeno	písmeno	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
obsahujících	obsahující	k2eAgFnPc6d1
<g />
.	.	kIx.
</s>
<s hack="1">
8	#num#	k4
různých	různý	k2eAgFnPc2d1
bází	báze	k1gFnPc2
vzájemně	vzájemně	k6eAd1
se	se	k3xPyFc4
párujících	párující	k2eAgFnPc2d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
zachovávají	zachovávat	k5eAaImIp3nP
geometrické	geometrický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
přirozené	přirozený	k2eAgInPc1d1
DNA	DNA	kA
(	(	kIx(
<g/>
2	#num#	k4
páry	pára	k1gFnSc2
bází	báze	k1gFnPc2
jsou	být	k5eAaImIp3nP
přitom	přitom	k6eAd1
přirozené	přirozený	k2eAgInPc1d1
<g/>
,	,	kIx,
2	#num#	k4
páry	pár	k1gInPc1
umělé	umělý	k2eAgInPc1d1
<g/>
:	:	kIx,
výše	vysoce	k6eAd2
zmíněný	zmíněný	k2eAgInSc1d1
pár	pár	k1gInSc1
Z	z	k7c2
<g/>
·	·	k?
<g/>
P	P	kA
a	a	k8xC
nový	nový	k2eAgInSc1d1
pár	pár	k1gInSc1
S	s	k7c7
<g/>
·	·	k?
<g/>
B	B	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
S	s	k7c7
je	být	k5eAaImIp3nS
methylcytosin	methylcytosin	k2eAgInSc1d1
<g/>
,	,	kIx,
B	B	kA
izoguanin	izoguanin	k2eAgInSc1d1
<g/>
)	)	kIx)
a	a	k8xC
hachimoji	hachimoj	k1gInSc3
DNA	dno	k1gNnSc2
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
schopna	schopen	k2eAgFnSc1d1
jednoznačného	jednoznačný	k2eAgInSc2d1
přepisu	přepis	k1gInSc2
do	do	k7c2
hachimoji	hachimoj	k1gInPc7
RNA	RNA	kA
(	(	kIx(
<g/>
v	v	k7c6
níž	jenž	k3xRgFnSc6
S	s	k7c7
je	být	k5eAaImIp3nS
izocytosin	izocytosin	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objev	objev	k1gInSc4
těchto	tento	k3xDgFnPc2
uměle	uměle	k6eAd1
rozšířených	rozšířený	k2eAgFnPc2d1
nukleových	nukleový	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
byl	být	k5eAaImAgInS
publikován	publikovat	k5eAaBmNgInS
v	v	k7c6
r.	r.	kA
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
v	v	k7c6
hachimoji	hachimoj	k1gInSc6
DNA	dno	k1gNnSc2
</s>
<s>
Párování	párování	k1gNnSc1
bází	báze	k1gFnPc2
v	v	k7c6
hachimoji	hachimoj	k1gInSc6
RNA	RNA	kA
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
nukleotidů	nukleotid	k1gInPc2
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
u	u	k7c2
hachimoji	hachimoj	k1gInPc7
DNA	DNA	kA
(	(	kIx(
<g/>
vedle	vedle	k7c2
přirozených	přirozený	k2eAgInPc2d1
A	A	kA
<g/>
,	,	kIx,
T	T	kA
<g/>
,	,	kIx,
C	C	kA
<g/>
,	,	kIx,
G	G	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
dS	dS	k?
(	(	kIx(
<g/>
z	z	k7c2
deoxyS	deoxyS	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
3	#num#	k4
<g/>
-methyl-	-methyl-	k?
<g/>
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k?
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
-deoxyribofuranosyl	-deoxyribofuranosyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-pyrimidin-	-pyrimidin-	k?
<g/>
2	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
dB	db	kA
<g/>
:	:	kIx,
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k?
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
-deoxyribofuranosyl	-deoxyribofuranosyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-hydroxy-	-hydroxy-	k?
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
hydroxymethyl	hydroxymethyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-oxolan-	-oxolan-	k?
<g/>
2	#num#	k4
<g/>
-yl	-yl	k?
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
H-purin-	H-purin-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
dZ	dZ	k?
<g/>
:	:	kIx,
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k?
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
-deoxyribofuranosyl	-deoxyribofuranosyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-nitro-	-nitro-	k?
<g/>
1	#num#	k4
<g/>
H-pyridin-	H-pyridin-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
dP	dP	k?
<g/>
:	:	kIx,
2	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k?
<g/>
2	#num#	k4
<g/>
'	'	kIx"
<g/>
-deoxyribofuranosyl	-deoxyribofuranosyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-imidazo-	-imidazo-	k?
<g/>
[	[	kIx(
<g/>
1,2	1,2	k4
<g/>
a	a	k8xC
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
,5	,5	k4
<g/>
-triazin-	-triazin-	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
u	u	k7c2
hachimoji	hachimoj	k1gInPc7
RNA	RNA	kA
(	(	kIx(
<g/>
vedle	vedle	k7c2
přirozených	přirozený	k2eAgInPc2d1
A	A	kA
<g/>
,	,	kIx,
U	U	kA
<g/>
,	,	kIx,
C	C	kA
<g/>
,	,	kIx,
G	G	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
S	s	k7c7
(	(	kIx(
<g/>
též	též	k6eAd1
rS	rS	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k1gInSc1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
-pyrimidinon	-pyrimidinon	k1gMnSc1
</s>
<s>
B	B	kA
<g/>
:	:	kIx,
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k1gInSc1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-hydroxy-	-hydroxy-	k?
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
hydroxymethyl	hydroxymethyl	k1gInSc1
<g/>
)	)	kIx)
<g/>
-oxolan-	-oxolan-	k?
<g/>
2	#num#	k4
<g/>
-yl	-yl	k?
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
H-purin-	H-purin-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
Z	z	k7c2
<g/>
:	:	kIx,
6	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k1gInSc1
<g/>
)	)	kIx)
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-nitro-	-nitro-	k?
<g/>
1	#num#	k4
<g/>
H-pyridin-	H-pyridin-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
P	P	kA
<g/>
:	:	kIx,
2	#num#	k4
<g/>
-amino-	-amino-	k?
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
'	'	kIx"
<g/>
-β	-β	k1gInSc1
<g/>
)	)	kIx)
<g/>
-imidazo-	-imidazo-	k?
<g/>
[	[	kIx(
<g/>
1,2	1,2	k4
<g/>
a	a	k8xC
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
1,3	1,3	k4
<g/>
,5	,5	k4
<g/>
-triazin-	-triazin-	k?
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
H	H	kA
<g/>
]	]	kIx)
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-on	-on	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Oxford	Oxford	k1gInSc1
dictionary	dictionara	k1gFnSc2
of	of	k?
biochemistry	biochemistr	k1gMnPc4
and	and	k?
molecular	molecular	k1gInSc1
biology	biolog	k1gMnPc4
<g/>
;	;	kIx,
revised	revised	k1gInSc1
edition	edition	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
R.	R.	kA
Cammack	Cammack	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
university	universita	k1gFnSc2
press	pressa	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
852917	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
VOET	VOET	kA
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
<g/>
;	;	kIx,
VOET	VOET	kA
<g/>
,	,	kIx,
Judith	Judith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biochemie	biochemie	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Victoria	Victorium	k1gNnSc2
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85605	#num#	k4
<g/>
-	-	kIx~
<g/>
44	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Robert	Robert	k1gMnSc1
K.	K.	kA
Murray	Murray	k1gInPc1
<g/>
;	;	kIx,
Daryl	Daryl	k1gInSc1
K.	K.	kA
Granner	Granner	k1gInSc1
<g/>
;	;	kIx,
Joe	Joe	k1gFnSc1
C.	C.	kA
Davis	Davis	k1gFnSc1
<g/>
;	;	kIx,
Peter	Peter	k1gMnSc1
A.	A.	kA
Mayes	Mayes	k1gMnSc1
<g/>
;	;	kIx,
Victor	Victor	k1gMnSc1
W.	W.	kA
Rodwell	Rodwell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harper	Harper	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Illustrated	Illustrated	k1gMnSc1
Biochemistry	Biochemistr	k1gMnPc4
<g/>
;	;	kIx,
twenty-sixth	twenty-sixth	k1gInSc1
edition	edition	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
138901	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
YAKOVCHUK	YAKOVCHUK	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
PROTOZANOVA	PROTOZANOVA	kA
<g/>
,	,	kIx,
E.	E.	kA
<g/>
;	;	kIx,
FRANK-KAMENETSKII	FRANK-KAMENETSKII	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
D.	D.	kA
Base-stacking	Base-stacking	k1gInSc1
and	and	k?
base-pairing	base-pairing	k1gInSc1
contributions	contributions	k6eAd1
into	into	k6eAd1
thermal	thermal	k1gMnSc1
stability	stabilita	k1gFnSc2
of	of	k?
the	the	k?
DNA	DNA	kA
double	double	k2eAgInSc4d1
helix	helix	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nucleic	Nucleice	k1gInPc2
Acids	Acidsa	k1gFnPc2
Res	Res	k1gFnSc7
<g/>
..	..	k?
2006	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
34	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
564	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1362	#num#	k4
<g/>
-	-	kIx~
<g/>
4962	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
NELSON	Nelson	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
COX	COX	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
M.	M.	kA
Lehninger	Lehninger	k1gMnSc1
principles	principles	k1gMnSc1
of	of	k?
biochemistry	biochemistr	k1gMnPc7
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.	W.	kA
H.	H.	kA
Freeman	Freeman	k1gMnSc1
and	and	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7167	#num#	k4
<g/>
-	-	kIx~
<g/>
7108	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
LODISH	LODISH	kA
<g/>
,	,	kIx,
Harvey	Harvea	k1gFnSc2
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
..	..	k?
Molecular	Moleculara	k1gFnPc2
Cell	cello	k1gNnPc2
Biology	biolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
W.H.	W.H.	k1gMnSc1
Freedman	Freedman	k1gMnSc1
and	and	k?
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7167	#num#	k4
<g/>
-	-	kIx~
<g/>
4366	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SWITZER	SWITZER	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
MORONEY	MORONEY	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
E.	E.	kA
<g/>
;	;	kIx,
BENNER	BENNER	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgMnSc1d1
A.	A.	kA
Enzymatic	Enzymatice	k1gFnPc2
incorporation	incorporation	k1gInSc4
of	of	k?
a	a	k8xC
new	new	k?
base	basa	k1gFnSc3
pair	pair	k1gMnSc1
into	into	k1gMnSc1
DNA	DNA	kA
and	and	k?
RNA	RNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
8322	#num#	k4
<g/>
-	-	kIx~
<g/>
8323	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
111	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
8322	#num#	k4
<g/>
-	-	kIx~
<g/>
8323	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1520	#num#	k4
<g/>
-	-	kIx~
<g/>
5126	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
ja	ja	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
203	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
67	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PICCIRILLI	PICCIRILLI	kA
<g/>
,	,	kIx,
Joseph	Joseph	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
KRAUCH	KRAUCH	kA
<g/>
,	,	kIx,
Tilman	Tilman	k1gMnSc1
<g/>
;	;	kIx,
MORONEY	MORONEY	kA
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
E.	E.	kA
<g/>
,	,	kIx,
BENNER	BENNER	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgMnSc1d1
A.	A.	kA
Enzymatic	Enzymatice	k1gFnPc2
incorporation	incorporation	k1gInSc4
of	of	k?
a	a	k8xC
new	new	k?
base	basa	k1gFnSc3
pair	pair	k1gMnSc1
into	into	k1gMnSc1
DNA	DNA	kA
and	and	k?
RNA	RNA	kA
extends	extends	k1gInSc1
the	the	k?
genetic	genetice	k1gFnPc2
alphabet	alphabet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
33	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc4
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
343	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6253	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
33	#num#	k4
<g/>
-	-	kIx~
<g/>
37	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1476	#num#	k4
<g/>
-	-	kIx~
<g/>
4687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
343033	#num#	k4
<g/>
a	a	k8xC
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
1688644	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HIRAO	HIRAO	kA
<g/>
,	,	kIx,
Ichiro	Ichiro	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
unnatural	unnaturat	k5eAaPmAgMnS,k5eAaImAgMnS
base	basa	k1gFnSc3
pair	pair	k1gMnSc1
for	forum	k1gNnPc2
incorporating	incorporating	k1gInSc1
amino	amino	k1gNnSc4
acid	acid	k6eAd1
analogs	analogs	k6eAd1
into	into	k6eAd1
proteins	proteins	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
-	-	kIx~
<g/>
182	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Biotechnology	biotechnolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
20	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
177	#num#	k4
<g/>
-	-	kIx~
<g/>
182	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1546	#num#	k4
<g/>
-	-	kIx~
<g/>
1696	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nbt	nbt	k?
<g/>
0	#num#	k4
<g/>
202	#num#	k4
<g/>
-	-	kIx~
<g/>
177	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
11821864	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HIRAO	HIRAO	kA
<g/>
,	,	kIx,
Ichiro	Ichiro	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gMnSc1
unnatural	unnaturat	k5eAaPmAgMnS,k5eAaImAgMnS
hydrophobic	hydrophobic	k1gMnSc1
base	basa	k1gFnSc3
pair	pair	k1gMnSc1
system	syst	k1gInSc7
<g/>
:	:	kIx,
site-specific	site-specifice	k1gFnPc2
incorporation	incorporation	k1gInSc1
of	of	k?
nucleotide	nucleotid	k1gInSc5
analogs	analogs	k6eAd1
into	into	k1gMnSc1
DNA	DNA	kA
and	and	k?
RNA	RNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
729	#num#	k4
<g/>
-	-	kIx~
<g/>
735	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Methods	Methods	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
3	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
729	#num#	k4
<g/>
-	-	kIx~
<g/>
735	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1548	#num#	k4
<g/>
-	-	kIx~
<g/>
7105	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nmeth	nmeth	k1gInSc1
<g/>
915	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
16929319	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KIMOTO	KIMOTO	kA
<g/>
,	,	kIx,
Michiko	Michika	k1gFnSc5
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generation	Generation	k1gInSc1
of	of	k?
high-affinity	high-affinita	k1gFnSc2
DNA	DNA	kA
aptamers	aptamers	k1gInSc1
using	using	k1gInSc1
an	an	k?
expanded	expanded	k1gInSc1
genetic	genetice	k1gFnPc2
alphabet	alphabeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
453	#num#	k4
<g/>
-	-	kIx~
<g/>
457	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Biotechnology	biotechnolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
31	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
453	#num#	k4
<g/>
-	-	kIx~
<g/>
457	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1548	#num#	k4
<g/>
-	-	kIx~
<g/>
7105	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nbt	nbt	k?
<g/>
.2556	.2556	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
23563318	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
PAZDERA	Pazdera	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
stabilní	stabilní	k2eAgInSc4d1
polosyntetický	polosyntetický	k2eAgInSc4d1
organismus	organismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MALYSHEV	MALYSHEV	kA
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
A.	A.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Efficient	Efficient	k1gMnSc1
and	and	k?
sequence-independent	sequence-independent	k1gMnSc1
replication	replication	k1gInSc1
of	of	k?
DNA	DNA	kA
containing	containing	k1gInSc1
a	a	k8xC
third	third	k1gInSc1
base	bas	k1gInSc6
pair	pair	k1gMnSc1
establishes	establishes	k1gMnSc1
a	a	k8xC
functional	functionat	k5eAaPmAgMnS,k5eAaImAgMnS
six-letter	six-letter	k1gMnSc1
genetic	genetice	k1gFnPc2
alphabet	alphabet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12005	#num#	k4
<g/>
–	–	k?
<g/>
12010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
National	National	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gMnSc1
USA	USA	kA
(	(	kIx(
<g/>
PNAS	PNAS	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
109	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12005	#num#	k4
<g/>
–	–	k?
<g/>
12010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1091	#num#	k4
<g/>
-	-	kIx~
<g/>
6490	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
pnas	pnas	k6eAd1
<g/>
.1205176109	.1205176109	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
22773812	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MALYSHEV	MALYSHEV	kA
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
A.	A.	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	A	kA
semi-synthetic	semi-synthetice	k1gFnPc2
organism	organism	k1gMnSc1
with	with	k1gMnSc1
an	an	k?
expanded	expanded	k1gInSc1
genetic	genetice	k1gFnPc2
alphabet	alphabeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
385	#num#	k4
<g/>
–	–	k?
<g/>
388	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
509	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
7500	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
385	#num#	k4
<g/>
–	–	k?
<g/>
388	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1476	#num#	k4
<g/>
-	-	kIx~
<g/>
4687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nature	natur	k1gMnSc5
<g/>
13314	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
24805238	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JOHN	John	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
:	:	kIx,
Vědci	vědec	k1gMnPc1
stvořili	stvořit	k5eAaPmAgMnP
bakterii	bakterie	k1gFnSc4
s	s	k7c7
novým	nový	k2eAgInSc7d1
genetickým	genetický	k2eAgInSc7d1
kódem	kód	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
Yorke	Yorke	k1gFnSc1
<g/>
;	;	kIx,
LAMB	LAMB	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
M.	M.	kA
<g/>
;	;	kIx,
FELDMAN	FELDMAN	kA
<g/>
,	,	kIx,
Aaron	Aaron	k1gMnSc1
W.	W.	kA
<g/>
;	;	kIx,
ZHOU	ZHOU	kA
<g/>
,	,	kIx,
Anne	Anne	k1gFnSc7
Xiaozhou	Xiaozha	k1gMnSc7
<g/>
;	;	kIx,
LAVERGNE	LAVERGNE	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
<g/>
;	;	kIx,
LI	li	k9
<g/>
,	,	kIx,
Lingjun	Lingjun	k1gMnSc1
<g/>
;	;	kIx,
ROMESBERG	ROMESBERG	kA
<g/>
,	,	kIx,
Floyd	Floyd	k1gMnSc1
E.	E.	kA
A	A	kA
semisynthetic	semisynthetice	k1gFnPc2
organism	organism	k1gMnSc1
engineered	engineered	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
stable	stable	k6eAd1
expansion	expansion	k1gInSc1
of	of	k?
the	the	k?
genetic	genetice	k1gFnPc2
alphabet	alphabet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
National	National	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gMnSc1
USA	USA	kA
(	(	kIx(
<g/>
PNAS	PNAS	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Online	Onlin	k1gInSc5
před	před	k7c7
tiskem	tisk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1091	#num#	k4
<g/>
-	-	kIx~
<g/>
6490	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.107	10.107	k4
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
pnas	pnas	k6eAd1
<g/>
.1616443114	.1616443114	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gFnSc2
Scripps	Scrippsa	k1gFnPc2
Research	Research	k1gMnSc1
Institute	institut	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientists	Scientists	k1gInSc1
create	creat	k1gInSc5
first	first	k1gFnSc1
stable	stable	k6eAd1
semisynthetic	semisynthetice	k1gFnPc2
organism	organism	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popularizační	popularizační	k2eAgInSc1d1
článek	článek	k1gInSc1
k	k	k7c3
předchozí	předchozí	k2eAgFnSc3d1
referenci	reference	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOUSER	houser	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
stabilní	stabilní	k2eAgInSc4d1
polosyntetický	polosyntetický	k2eAgInSc4d1
organismus	organismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SCIENCEmag	SCIENCEmag	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitemedia	Nitemedium	k1gNnSc2
s.	s.	k?
<g/>
r.	r.	kA
<g/>
o.	o.	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GEORGIADIS	GEORGIADIS	kA
<g/>
,	,	kIx,
Millie	Millie	k1gFnPc1
M.	M.	kA
<g/>
;	;	kIx,
SINGH	SINGH	kA
<g/>
,	,	kIx,
Isha	Isha	k1gFnSc1
<g/>
;	;	kIx,
KELLETT	KELLETT	kA
<g/>
,	,	kIx,
Whitney	Whitney	k1gInPc7
F.	F.	kA
<g/>
,	,	kIx,
HOSHIKA	HOSHIKA	kA
<g/>
,	,	kIx,
Shuichi	Shuichi	k1gNnSc1
<g/>
;	;	kIx,
BENNER	BENNER	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgMnSc1d1
A.	A.	kA
<g/>
;	;	kIx,
RICHARDS	RICHARDS	kA
<g/>
,	,	kIx,
Nigel	Nigel	k1gMnSc1
G.	G.	kA
J.	J.	kA
Structural	Structural	k1gMnSc1
Basis	Basis	k?
for	forum	k1gNnPc2
a	a	k8xC
Six	Six	k1gFnPc2
Nucleotide	Nucleotid	k1gInSc5
Genetic	Genetice	k1gFnPc2
Alphabet	Alphabeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6947	#num#	k4
<g/>
–	–	k?
<g/>
6955	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
137	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6947	#num#	k4
<g/>
–	–	k?
<g/>
6955	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1520	#num#	k4
<g/>
-	-	kIx~
<g/>
5126	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
jacs	jacs	k6eAd1
<g/>
.5	.5	k4
<g/>
b	b	k?
<g/>
0	#num#	k4
<g/>
3482	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
Liqin	Liqin	k1gMnSc1
<g/>
;	;	kIx,
YANG	YANG	kA
<g/>
,	,	kIx,
Zunyi	Zunyi	k1gNnSc1
<g/>
;	;	kIx,
SEFAH	SEFAH	kA
<g/>
,	,	kIx,
Kwame	Kwam	k1gMnSc5
<g/>
,	,	kIx,
BRADLEY	BRADLEY	kA
<g/>
,	,	kIx,
Kevin	Kevin	k1gMnSc1
M.	M.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
HOSHIKA	HOSHIKA	kA
<g/>
,	,	kIx,
Shuichi	Shuichi	k1gNnSc1
<g/>
;	;	kIx,
KIM	KIM	kA
<g/>
,	,	kIx,
Myong-Jung	Myong-Jung	k1gMnSc1
<g/>
;	;	kIx,
KIM	KIM	kA
<g/>
,	,	kIx,
Hyo-Joong	Hyo-Joong	k1gMnSc1
<g/>
;	;	kIx,
ZHU	ZHU	kA
<g/>
,	,	kIx,
Guizhi	Guizhi	k1gNnSc1
<g/>
;	;	kIx,
JIMÉNEZ	JIMÉNEZ	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
;	;	kIx,
CANSIZ	CANSIZ	kA
<g/>
,	,	kIx,
Sena	sena	k1gFnSc1
<g/>
;	;	kIx,
TENG	TENG	kA
<g/>
,	,	kIx,
I-Ting	I-Ting	k1gInSc1
<g/>
;	;	kIx,
CHAMPANHAC	CHAMPANHAC	kA
<g/>
,	,	kIx,
Carole	Carole	k1gFnSc1
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
McLENDON	McLENDON	k1gFnPc2
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
LIU	LIU	kA
<g/>
,	,	kIx,
Chen	Chen	k1gMnSc1
<g/>
;	;	kIx,
ZHANG	ZHANG	kA
<g/>
,	,	kIx,
Wen	Wen	k1gFnSc1
<g/>
;	;	kIx,
GERLOFF	GERLOFF	kA
<g/>
,	,	kIx,
Dietlind	Dietlind	k1gMnSc1
L.	L.	kA
<g/>
;	;	kIx,
HUANG	HUANG	kA
<g/>
,	,	kIx,
Zhen	Zhen	k1gMnSc1
<g/>
;	;	kIx,
TAN	TAN	kA
<g/>
,	,	kIx,
Weihong	Weihong	k1gMnSc1
<g/>
;	;	kIx,
BENNER	BENNER	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
A.	A.	kA
Evolution	Evolution	k1gInSc1
of	of	k?
Functional	Functional	k1gMnSc5
Six-Nucleotide	Six-Nucleotid	k1gMnSc5
DNA	DNA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
6734	#num#	k4
<g/>
–	–	k?
<g/>
6737	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
American	American	k1gInSc4
Chemical	Chemical	k1gMnSc2
Society	societa	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
137	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
21	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6734	#num#	k4
<g/>
–	–	k?
<g/>
6737	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1520	#num#	k4
<g/>
-	-	kIx~
<g/>
5126	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
jacs	jacs	k6eAd1
<g/>
.5	.5	k4
<g/>
b	b	k?
<g/>
0	#num#	k4
<g/>
2251	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kar	kar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
vytvořili	vytvořit	k5eAaPmAgMnP
„	„	k?
<g/>
mimozemskou	mimozemský	k2eAgFnSc4d1
<g/>
“	“	k?
DNA	dno	k1gNnSc2
<g/>
,	,	kIx,
místo	místo	k7c2
čtyř	čtyři	k4xCgNnPc2
písmen	písmeno	k1gNnPc2
jich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
osm	osm	k4xCc1
<g/>
.	.	kIx.
ct	ct	k?
<g/>
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
BROWN	BROWN	kA
<g/>
,	,	kIx,
Dwayne	Dwayn	k1gInSc5
<g/>
;	;	kIx,
LANDAU	LANDAU	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Research	Research	k1gMnSc1
creates	creates	k1gMnSc1
DNA-like	DNA-lik	k1gMnSc2
molecule	molecule	k1gFnSc2
to	ten	k3xDgNnSc1
aid	aid	k?
search	search	k1gInSc1
for	forum	k1gNnPc2
alien	alien	k2eAgInSc1d1
life	life	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Phys	Phys	k1gInSc1
<g/>
.	.	kIx.
<g/>
Org	Org	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HOSHIKA	HOSHIKA	kA
<g/>
,	,	kIx,
Shuichi	Shuichi	k1gNnSc1
<g/>
;	;	kIx,
LEAL	LEAL	kA
<g/>
,	,	kIx,
Nicole	Nicole	k1gFnPc1
A.	A.	kA
<g/>
;	;	kIx,
KIM	KIM	kA
<g/>
,	,	kIx,
Myong-Jung	Myong-Jung	k1gMnSc1
<g/>
;	;	kIx,
KIM	KIM	kA
<g/>
,	,	kIx,
Myong-Sang	Myong-Sang	k1gMnSc1
<g/>
;	;	kIx,
KARALKAR	KARALKAR	kA
<g/>
,	,	kIx,
Nilesh	Nilesh	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
KIM	KIM	kA
<g/>
,	,	kIx,
Hyo-Joong	Hyo-Joong	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hachimoji	Hachimoj	k1gInSc6
DNA	dno	k1gNnSc2
and	and	k?
RNA	RNA	kA
<g/>
:	:	kIx,
A	A	kA
genetic	genetice	k1gFnPc2
system	syst	k1gInSc7
with	with	k1gInSc1
eight	eighta	k1gFnPc2
building	building	k1gInSc1
blocks	blocks	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Supplement	Supplement	k1gMnSc1
<g/>
:	:	kIx,
Materials	Materials	k1gInSc1
and	and	k?
Methods	Methods	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gInSc1
Association	Association	k1gInSc4
for	forum	k1gNnPc2
the	the	k?
Advancement	Advancement	k1gMnSc1
of	of	k?
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
363	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6429	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1095	#num#	k4
<g/>
-	-	kIx~
<g/>
9203	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
science	scienec	k1gInSc2
<g/>
.	.	kIx.
<g/>
aat	aat	k?
<g/>
0	#num#	k4
<g/>
971	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PMID	PMID	kA
30792304	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
