<s>
The	The	k?	The
Who	Who	k1gFnSc1	Who
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
rocková	rockový	k2eAgFnSc1d1	rocková
kapela	kapela	k1gFnSc1	kapela
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgMnSc7d3	veliký
konkurentem	konkurent	k1gMnSc7	konkurent
Beatles	Beatles	k1gFnPc2	Beatles
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
popové	popový	k2eAgFnSc2d1	popová
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
a	a	k8xC	a
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
zejména	zejména	k9	zejména
písní	píseň	k1gFnSc7	píseň
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc1	Generation
<g/>
"	"	kIx"	"
a	a	k8xC	a
první	první	k4xOgFnSc7	první
rockovou	rockový	k2eAgFnSc7d1	rocková
operou	opera	k1gFnSc7	opera
Tommy	Tomma	k1gFnSc2	Tomma
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
žebříčku	žebříček	k1gInSc2	žebříček
časopisu	časopis	k1gInSc2	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
se	se	k3xPyFc4	se
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc1	Generation
<g/>
"	"	kIx"	"
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
z	z	k7c2	z
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
proslavili	proslavit	k5eAaPmAgMnP	proslavit
ničením	ničení	k1gNnSc7	ničení
nástrojů	nástroj	k1gInPc2	nástroj
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
se	se	k3xPyFc4	se
v	v	k7c4	v
londýnské	londýnský	k2eAgNnSc4d1	Londýnské
Acton	Acton	k1gNnSc4	Acton
County	Counta	k1gFnSc2	Counta
Grammar	Grammara	k1gFnPc2	Grammara
School	Schoola	k1gFnPc2	Schoola
seznámili	seznámit	k5eAaPmAgMnP	seznámit
Pete	Pete	k1gNnSc4	Pete
Townshend	Townshend	k1gMnSc1	Townshend
a	a	k8xC	a
John	John	k1gMnSc1	John
Entwistle	Entwistle	k1gFnSc2	Entwistle
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
kapelu	kapela	k1gFnSc4	kapela
The	The	k1gFnSc3	The
Confederates	Confederatesa	k1gFnPc2	Confederatesa
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
banjo	banjo	k1gNnSc4	banjo
a	a	k8xC	a
Entwistle	Entwistle	k1gFnSc4	Entwistle
na	na	k7c4	na
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Entwistle	Entwistle	k1gMnSc1	Entwistle
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Daltreym	Daltreym	k1gInSc4	Daltreym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kapele	kapela	k1gFnSc6	kapela
The	The	k1gMnSc1	The
Detours	Detoursa	k1gFnPc2	Detoursa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgInS	založit
rok	rok	k1gInSc1	rok
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pár	pár	k4xCyI	pár
týdnech	týden	k1gInPc6	týden
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
připojil	připojit	k5eAaPmAgMnS	připojit
i	i	k9	i
Townshend	Townshend	k1gMnSc1	Townshend
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
měla	mít	k5eAaImAgFnS	mít
pět	pět	k4xCc4	pět
členů	člen	k1gMnPc2	člen
-	-	kIx~	-
Colin	Colin	k1gMnSc1	Colin
Dawson	Dawson	k1gMnSc1	Dawson
zpíval	zpívat	k5eAaImAgMnS	zpívat
<g/>
,	,	kIx,	,
Daltrey	Daltrey	k1gInPc7	Daltrey
hrál	hrát	k5eAaImAgInS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
Entwistle	Entwistle	k1gFnSc4	Entwistle
na	na	k7c4	na
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
Townshend	Townshend	k1gInSc4	Townshend
na	na	k7c4	na
rytmickou	rytmický	k2eAgFnSc4d1	rytmická
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
Doug	Doug	k1gInSc4	Doug
Sandom	Sandom	k1gInSc1	Sandom
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Dawsona	Dawson	k1gMnSc2	Dawson
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Daltrey	Daltrea	k1gFnSc2	Daltrea
hlavním	hlavní	k2eAgMnSc7d1	hlavní
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
Townshend	Townshend	k1gMnSc1	Townshend
výhradním	výhradní	k2eAgMnSc7d1	výhradní
kytaristou	kytarista	k1gMnSc7	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
nového	nový	k2eAgMnSc4d1	nový
bubeníka	bubeník	k1gMnSc4	bubeník
a	a	k8xC	a
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skladby	skladba	k1gFnPc4	skladba
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
The	The	k1gFnSc2	The
Beatles	beatles	k1gMnPc2	beatles
nebo	nebo	k8xC	nebo
The	The	k1gMnPc2	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
opustil	opustit	k5eAaPmAgInS	opustit
Sandom	Sandom	k1gInSc1	Sandom
kapelu	kapela	k1gFnSc4	kapela
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
na	na	k7c4	na
zbývající	zbývající	k2eAgInPc4d1	zbývající
naplánované	naplánovaný	k2eAgInPc4d1	naplánovaný
koncerty	koncert	k1gInPc4	koncert
najala	najmout	k5eAaPmAgFnS	najmout
provizorního	provizorní	k2eAgMnSc4d1	provizorní
bubeníka	bubeník	k1gMnSc4	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
oslovil	oslovit	k5eAaPmAgMnS	oslovit
Daltreyho	Daltrey	k1gMnSc2	Daltrey
Keith	Keith	k1gMnSc1	Keith
Moon	Moon	k1gMnSc1	Moon
a	a	k8xC	a
řekl	říct	k5eAaPmAgMnS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Slyšel	slyšet	k5eAaImAgMnS	slyšet
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hledáte	hledat	k5eAaImIp2nP	hledat
nového	nový	k2eAgMnSc4d1	nový
bubeníka	bubeník	k1gMnSc4	bubeník
<g/>
.	.	kIx.	.
</s>
<s>
No	no	k9	no
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
máte	mít	k5eAaImIp2nP	mít
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
hned	hned	k6eAd1	hned
na	na	k7c6	na
onom	onen	k3xDgInSc6	onen
koncertě	koncert	k1gInSc6	koncert
byla	být	k5eAaImAgFnS	být
dána	dán	k2eAgFnSc1d1	dána
příležitost	příležitost	k1gFnSc1	příležitost
zahrát	zahrát	k5eAaPmF	zahrát
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
,	,	kIx,	,
Moon	Moon	k1gMnSc1	Moon
svůj	svůj	k3xOyFgInSc4	svůj
výrok	výrok	k1gInSc4	výrok
obhájil	obhájit	k5eAaPmAgMnS	obhájit
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
ho	on	k3xPp3gMnSc4	on
přijala	přijmout	k5eAaPmAgFnS	přijmout
mezi	mezi	k7c4	mezi
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1964	[number]	k4	1964
slyšel	slyšet	k5eAaImAgMnS	slyšet
Entwistle	Entwistle	k1gFnSc4	Entwistle
o	o	k7c6	o
kapele	kapela	k1gFnSc6	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
The	The	k1gFnSc4	The
Detours	Detoursa	k1gFnPc2	Detoursa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Townshendova	Townshendův	k2eAgMnSc2d1	Townshendův
spolubydlícího	spolubydlící	k2eAgMnSc2d1	spolubydlící
Richarda	Richard	k1gMnSc2	Richard
Barnese	Barnese	k1gFnSc2	Barnese
si	se	k3xPyFc3	se
změnili	změnit	k5eAaPmAgMnP	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
The	The	k1gFnSc4	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
1964	[number]	k4	1964
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Petea	Pete	k1gInSc2	Pete
Meadena	Meadeno	k1gNnSc2	Meadeno
změnili	změnit	k5eAaPmAgMnP	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
The	The	k1gFnSc4	The
High	Higha	k1gFnPc2	Higha
Numbers	Numbersa	k1gFnPc2	Numbersa
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Zoot	Zoot	k1gInSc1	Zoot
Suit	suita	k1gFnPc2	suita
<g/>
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
<g/>
I	i	k9	i
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
the	the	k?	the
Face	Face	k1gInSc1	Face
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
názvu	název	k1gInSc3	název
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Meaden	Meadna	k1gFnPc2	Meadna
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
manažery	manažer	k1gMnPc4	manažer
Kitem	kit	k1gInSc7	kit
Lambertem	Lambert	k1gInSc7	Lambert
a	a	k8xC	a
Chrisem	Chris	k1gInSc7	Chris
Stampem	Stamp	k1gInSc7	Stamp
<g/>
.	.	kIx.	.
</s>
<s>
Měli	mít	k5eAaImAgMnP	mít
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
místních	místní	k2eAgInPc2d1	místní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
se	se	k3xPyFc4	se
nějak	nějak	k6eAd1	nějak
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
spousty	spousta	k1gFnSc2	spousta
dalších	další	k2eAgFnPc2d1	další
ambiciózních	ambiciózní	k2eAgFnPc2d1	ambiciózní
londýnských	londýnský	k2eAgFnPc2d1	londýnská
kapel	kapela	k1gFnPc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgNnSc2	jeden
vystoupení	vystoupení	k1gNnSc2	vystoupení
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1964	[number]	k4	1964
Townshend	Townshenda	k1gFnPc2	Townshenda
omylem	omylem	k6eAd1	omylem
zlomil	zlomit	k5eAaPmAgMnS	zlomit
krk	krk	k1gInSc4	krk
kytary	kytara	k1gFnSc2	kytara
o	o	k7c4	o
nízký	nízký	k2eAgInSc4d1	nízký
strop	strop	k1gInSc4	strop
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
rozmlátil	rozmlátit	k5eAaPmAgInS	rozmlátit
nástroj	nástroj	k1gInSc4	nástroj
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Vzal	vzít	k5eAaPmAgMnS	vzít
si	se	k3xPyFc3	se
další	další	k2eAgFnSc4d1	další
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
koncertě	koncert	k1gInSc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
došly	dojít	k5eAaPmAgInP	dojít
Townshendovi	Townshendův	k2eAgMnPc1d1	Townshendův
kytary	kytara	k1gFnSc2	kytara
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
převrhl	převrhnout	k5eAaPmAgMnS	převrhnout
zesilovače	zesilovač	k1gInPc4	zesilovač
Marshall	Marshalla	k1gFnPc2	Marshalla
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
Moon	Moon	k1gMnSc1	Moon
nezůstal	zůstat	k5eNaPmAgMnS	zůstat
pozadu	pozadu	k6eAd1	pozadu
<g/>
,	,	kIx,	,
zničil	zničit	k5eAaPmAgMnS	zničit
svoje	svůj	k3xOyFgMnPc4	svůj
bicí	bicí	k2eAgMnPc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
incident	incident	k1gInSc1	incident
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
stal	stát	k5eAaPmAgMnS	stát
jendím	jendit	k5eAaImIp1nS	jendit
z	z	k7c2	z
"	"	kIx"	"
<g/>
50	[number]	k4	50
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
změnily	změnit	k5eAaPmAgFnP	změnit
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
roll	rollum	k1gNnPc2	rollum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
skladatelem	skladatel	k1gMnSc7	skladatel
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgFnSc7d1	tvůrčí
silou	síla	k1gFnSc7	síla
Townshend	Townshenda	k1gFnPc2	Townshenda
<g/>
.	.	kIx.	.
</s>
<s>
Entwistle	Entwistle	k6eAd1	Entwistle
<g/>
,	,	kIx,	,
Daltrey	Daltrea	k1gFnPc4	Daltrea
a	a	k8xC	a
Moon	Moon	k1gInSc4	Moon
také	také	k9	také
přispěli	přispět	k5eAaPmAgMnP	přispět
několika	několik	k4yIc7	několik
písněmi	píseň	k1gFnPc7	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
Shel	Shel	k1gMnSc1	Shel
Talmy	Talma	k1gFnSc2	Talma
byl	být	k5eAaImAgMnS	být
producentem	producent	k1gMnSc7	producent
několika	několik	k4yIc2	několik
nadějných	nadějný	k2eAgFnPc2d1	nadějná
nových	nový	k2eAgFnPc2d1	nová
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
vyprodukaval	vyprodukavat	k5eAaImAgInS	vyprodukavat
jejich	jejich	k3xOp3gInSc4	jejich
první	první	k4xOgInSc4	první
šlágr	šlágr	k1gInSc4	šlágr
"	"	kIx"	"
<g/>
I	i	k9	i
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Explain	Explain	k1gInSc1	Explain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
ovlivněný	ovlivněný	k2eAgMnSc1d1	ovlivněný
hudbou	hudba	k1gFnSc7	hudba
The	The	k1gFnSc2	The
Kinks	Kinks	k1gInSc1	Kinks
(	(	kIx(	(
<g/>
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
producentem	producent	k1gMnSc7	producent
byl	být	k5eAaImAgMnS	být
také	také	k9	také
Talmy	Talma	k1gFnSc2	Talma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
10	[number]	k4	10
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
následoval	následovat	k5eAaImAgInS	následovat
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Anyway	Anywaa	k1gFnPc1	Anywaa
<g/>
,	,	kIx,	,
Anyhow	Anyhow	k1gFnPc1	Anyhow
<g/>
,	,	kIx,	,
Anywhere	Anywher	k1gMnSc5	Anywher
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
skladatelsky	skladatelsky	k6eAd1	skladatelsky
podíleli	podílet	k5eAaImAgMnP	podílet
Townshend	Townshend	k1gInSc4	Townshend
s	s	k7c7	s
Daltreym	Daltreymum	k1gNnPc2	Daltreymum
<g/>
.	.	kIx.	.
</s>
<s>
Debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc4	Generation
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
vydané	vydaný	k2eAgFnPc4d1	vydaná
jako	jako	k8xC	jako
The	The	k1gFnPc4	The
Who	Who	k1gFnSc2	Who
Sings	Singsa	k1gFnPc2	Singsa
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc4	Generation
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Kids	Kidsa	k1gFnPc2	Kidsa
Are	ar	k1gInSc5	ar
Alright	Alrightum	k1gNnPc2	Alrightum
<g/>
"	"	kIx"	"
a	a	k8xC	a
titulní	titulní	k2eAgFnSc4d1	titulní
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc1	Generation
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
písní	píseň	k1gFnPc2	píseň
se	s	k7c7	s
sólem	sólo	k1gNnSc7	sólo
na	na	k7c4	na
basovou	basový	k2eAgFnSc4d1	basová
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
další	další	k2eAgInPc1d1	další
hity	hit	k1gInPc1	hit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Substitute	substitut	k1gInSc5	substitut
<g/>
"	"	kIx"	"
o	o	k7c6	o
mladíkovi	mladík	k1gMnSc6	mladík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
cítí	cítit	k5eAaImIp3nS	cítit
jako	jako	k9	jako
podvodník	podvodník	k1gMnSc1	podvodník
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
a	a	k8xC	a
Boy	boy	k1gMnSc1	boy
<g/>
"	"	kIx"	"
o	o	k7c6	o
chlapci	chlapec	k1gMnSc6	chlapec
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc2	který
oblékají	oblékat	k5eAaImIp3nP	oblékat
jako	jako	k8xS	jako
děvče	děvče	k1gNnSc4	děvče
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Happy	Happ	k1gInPc4	Happ
Jack	Jack	k1gInSc1	Jack
<g/>
"	"	kIx"	"
o	o	k7c6	o
mentálně	mentálně	k6eAd1	mentálně
narušeném	narušený	k2eAgInSc6d1	narušený
mladíkovi	mladík	k1gMnSc3	mladík
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pictures	Pictures	k1gInSc1	Pictures
Of	Of	k1gFnSc2	Of
Lily	lít	k5eAaImAgInP	lít
<g/>
"	"	kIx"	"
o	o	k7c6	o
mladíkovi	mladík	k1gMnSc6	mladík
upnutém	upnutý	k2eAgMnSc6d1	upnutý
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
plakát	plakát	k1gInSc4	plakát
spoře	sporo	k6eAd1	sporo
oděné	oděný	k2eAgFnSc2d1	oděná
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Rané	raný	k2eAgInPc1d1	raný
singly	singl	k1gInPc1	singl
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
napsané	napsaný	k2eAgInPc1d1	napsaný
Townshendem	Townshend	k1gInSc7	Townshend
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
motivy	motiv	k1gInPc1	motiv
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
napětí	napětí	k1gNnSc2	napětí
a	a	k8xC	a
silných	silný	k2eAgFnPc2d1	silná
obav	obava	k1gFnPc2	obava
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
měla	mít	k5eAaImAgFnS	mít
kapela	kapela	k1gFnSc1	kapela
se	s	k7c7	s
singly	singl	k1gInPc7	singl
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
Townshend	Townshend	k1gMnSc1	Townshend
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gNnPc4	jejich
alba	album	k1gNnPc4	album
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
sbírka	sbírka	k1gFnSc1	sbírka
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
album	album	k1gNnSc1	album
A	a	k8xC	a
Quick	Quick	k1gMnSc1	Quick
One	One	k1gMnSc1	One
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Happy	Happa	k1gFnSc2	Happa
Jack	Jack	k1gMnSc1	Jack
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
zmiňovali	zmiňovat	k5eAaImAgMnP	zmiňovat
jako	jako	k9	jako
o	o	k7c6	o
mini-opeře	minipera	k1gFnSc6	mini-opera
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
A	A	kA	A
Quick	Quick	k1gMnSc1	Quick
One	One	k1gMnSc1	One
následoval	následovat	k5eAaImAgInS	následovat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Pictues	Pictues	k1gInSc1	Pictues
Of	Of	k1gFnSc2	Of
Lily	lít	k5eAaImAgInP	lít
<g/>
"	"	kIx"	"
a	a	k8xC	a
konceptuální	konceptuální	k2eAgNnSc1d1	konceptuální
album	album	k1gNnSc1	album
The	The	k1gFnSc2	The
Who	Who	k1gFnPc2	Who
Sell	Sell	k1gMnSc1	Sell
Out	Out	k1gMnSc1	Out
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
pirátských	pirátský	k2eAgNnPc2d1	pirátské
rádií	rádio	k1gNnPc2	rádio
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
zábavné	zábavný	k2eAgFnPc4d1	zábavná
znělky	znělka	k1gFnPc4	znělka
a	a	k8xC	a
reklamy	reklama	k1gFnPc4	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
také	také	k9	také
například	například	k6eAd1	například
malou	malý	k2eAgFnSc4d1	malá
rockovou	rockový	k2eAgFnSc4d1	rocková
operu	opera	k1gFnSc4	opera
"	"	kIx"	"
<g/>
Rael	Rael	k1gInSc4	Rael
<g/>
"	"	kIx"	"
a	a	k8xC	a
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
I	i	k8xC	i
Can	Can	k1gFnSc3	Can
See	See	k1gFnSc3	See
for	forum	k1gNnPc2	forum
Miles	Miles	k1gInSc1	Miles
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
také	také	k9	také
rozbili	rozbít	k5eAaPmAgMnP	rozbít
své	svůj	k3xOyFgInPc4	svůj
nástroje	nástroj	k1gInPc4	nástroj
na	na	k7c4	na
Monterey	Monterey	k1gInPc4	Monterey
Pop	pop	k1gMnSc1	pop
Festival	festival	k1gInSc1	festival
a	a	k8xC	a
toto	tento	k3xDgNnSc4	tento
jednání	jednání	k1gNnSc4	jednání
zopakovali	zopakovat	k5eAaPmAgMnP	zopakovat
i	i	k9	i
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
pořadu	pořad	k1gInSc6	pořad
The	The	k1gFnSc1	The
Smothers	Smothers	k1gInSc1	Smothers
Brothers	Brothers	k1gInSc1	Brothers
Comedy	Comeda	k1gMnSc2	Comeda
Hour	Hour	k1gInSc4	Hour
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nechal	nechat	k5eAaPmAgMnS	nechat
Moon	Moon	k1gMnSc1	Moon
explodovat	explodovat	k5eAaBmF	explodovat
svoje	svůj	k3xOyFgNnSc4	svůj
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Podplatil	podplatit	k5eAaPmAgMnS	podplatit
technika	technik	k1gMnSc2	technik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
naplnil	naplnit	k5eAaPmAgInS	naplnit
bicí	bicí	k2eAgFnSc4d1	bicí
soupravu	souprava	k1gFnSc4	souprava
nadměrným	nadměrný	k2eAgNnSc7d1	nadměrné
množstvím	množství	k1gNnSc7	množství
výbušnin	výbušnina	k1gFnPc2	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
exploze	exploze	k1gFnSc1	exploze
byla	být	k5eAaImAgFnS	být
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdokoliv	kdokoliv	k3yInSc1	kdokoliv
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Moona	Moon	k1gInSc2	Moon
samotného	samotný	k2eAgInSc2d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
během	během	k7c2	během
natáčení	natáčení	k1gNnSc2	natáčení
dokumentárního	dokumentární	k2eAgInSc2d1	dokumentární
filmu	film	k1gInSc2	film
The	The	k1gFnSc2	The
Kids	Kidsa	k1gFnPc2	Kidsa
Are	ar	k1gInSc5	ar
Alright	Alright	k1gMnSc1	Alright
<g/>
,	,	kIx,	,
Townshend	Townshend	k1gMnSc1	Townshend
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gNnSc3	on
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
způsobila	způsobit	k5eAaPmAgFnS	způsobit
tinnitus	tinnitus	k1gInSc4	tinnitus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byli	být	k5eAaImAgMnP	být
The	The	k1gMnSc4	The
Who	Who	k1gMnSc4	Who
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
Schaefer	Schaefer	k1gMnSc1	Schaefer
Music	Musice	k1gFnPc2	Musice
Festivalu	festival	k1gInSc3	festival
v	v	k7c6	v
Central	Central	k1gFnSc6	Central
Parku	park	k1gInSc2	park
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Magic	Magice	k1gFnPc2	Magice
Bus	bus	k1gInSc1	bus
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	se	k3xPyFc4	se
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
mini-operou	minipera	k1gFnSc7	mini-opera
"	"	kIx"	"
<g/>
A	a	k9	a
Quick	Quick	k1gInSc1	Quick
One	One	k1gMnSc2	One
While	Whil	k1gMnSc2	Whil
He	he	k0	he
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Away	Away	k1gInPc7	Away
<g/>
"	"	kIx"	"
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
natáčení	natáčený	k2eAgMnPc1d1	natáčený
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gMnSc1	Stones
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gMnSc1	Roll
Circus	Circus	k1gMnSc1	Circus
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
také	také	k9	také
Townshend	Townshend	k1gMnSc1	Townshend
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
rozhovor	rozhovor	k1gInSc4	rozhovor
pro	pro	k7c4	pro
Rolling	Rolling	k1gInSc4	Rolling
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
standardně	standardně	k6eAd1	standardně
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
rockové	rockový	k2eAgFnSc6d1	rocková
opeře	opera	k1gFnSc6	opera
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
Tommyho	Tommy	k1gMnSc2	Tommy
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
dílo	dílo	k1gNnSc4	dílo
uváděné	uváděný	k2eAgNnSc4d1	uváděné
jako	jako	k8xS	jako
rocková	rockový	k2eAgFnSc1d1	rocková
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
mezník	mezník	k1gInSc1	mezník
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
na	na	k7c4	na
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
Townshendovu	Townshendův	k2eAgFnSc4d1	Townshendův
skladatelskou	skladatelský	k2eAgFnSc4d1	skladatelská
činnost	činnost	k1gFnSc4	činnost
Meher	Mehra	k1gFnPc2	Mehra
Baba	baba	k1gFnSc1	baba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
Tommyho	Tommy	k1gMnSc4	Tommy
avatár	avatár	k1gMnSc1	avatár
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
Tommy	Tomma	k1gFnSc2	Tomma
stal	stát	k5eAaPmAgInS	stát
velkým	velký	k2eAgInSc7d1	velký
hitem	hit	k1gInSc7	hit
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
rok	rok	k1gInSc4	rok
hráli	hrát	k5eAaImAgMnP	hrát
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
na	na	k7c6	na
Woodstocku	Woodstock	k1gInSc6	Woodstock
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trvali	trvat	k5eAaImAgMnP	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gFnPc3	on
bylo	být	k5eAaImAgNnS	být
zaplaceno	zaplatit	k5eAaPmNgNnS	zaplatit
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
půjdou	jít	k5eAaImIp3nP	jít
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
a	a	k8xC	a
zahrají	zahrát	k5eAaPmIp3nP	zahrát
většinu	většina	k1gFnSc4	většina
Tommyho	Tommy	k1gMnSc2	Tommy
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jejich	jejich	k3xOp3gNnSc2	jejich
vystoupení	vystoupení	k1gNnSc2	vystoupení
se	se	k3xPyFc4	se
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
dostal	dostat	k5eAaPmAgMnS	dostat
Abbie	Abbie	k1gFnPc1	Abbie
Hoffman	Hoffman	k1gMnSc1	Hoffman
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
představitel	představitel	k1gMnSc1	představitel
Yippies	Yippies	k1gMnSc1	Yippies
<g/>
,	,	kIx,	,
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
koncertu	koncert	k1gInSc2	koncert
Michael	Michael	k1gMnSc1	Michael
Lang	Lang	k1gMnSc1	Lang
<g/>
.	.	kIx.	.
</s>
<s>
Hoffman	Hoffman	k1gMnSc1	Hoffman
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
LSD	LSD	kA	LSD
<g/>
.	.	kIx.	.
</s>
<s>
Vyskočil	Vyskočil	k1gMnSc1	Vyskočil
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
<g/>
,	,	kIx,	,
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
se	se	k3xPyFc4	se
mikrofonu	mikrofon	k1gInSc2	mikrofon
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
obeznámit	obeznámit	k5eAaPmF	obeznámit
veřejnost	veřejnost	k1gFnSc4	veřejnost
s	s	k7c7	s
případem	případ	k1gInSc7	případ
Johna	John	k1gMnSc2	John
Sinclaira	Sinclair	k1gMnSc2	Sinclair
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
deseti	deset	k4xCc7	deset
letům	léto	k1gNnPc3	léto
vězení	vězení	k1gNnSc2	vězení
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
agentovi	agent	k1gMnSc3	agent
protidrogového	protidrogový	k2eAgNnSc2d1	protidrogové
oddělení	oddělení	k1gNnSc2	oddělení
v	v	k7c6	v
utajení	utajení	k1gNnSc6	utajení
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
dvě	dva	k4xCgFnPc4	dva
cigarety	cigareta	k1gFnPc4	cigareta
marihuany	marihuana	k1gFnSc2	marihuana
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
ho	on	k3xPp3gMnSc4	on
praštil	praštit	k5eAaPmAgMnS	praštit
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
kytarou	kytara	k1gFnSc7	kytara
a	a	k8xC	a
Hoffman	Hoffman	k1gMnSc1	Hoffman
spadl	spadnout	k5eAaPmAgMnS	spadnout
z	z	k7c2	z
pódia	pódium	k1gNnSc2	pódium
a	a	k8xC	a
zmizel	zmizet	k5eAaPmAgMnS	zmizet
v	v	k7c6	v
davu	dav	k1gInSc6	dav
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1970	[number]	k4	1970
kapela	kapela	k1gFnSc1	kapela
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
BBC	BBC	kA	BBC
Pop	pop	k1gMnSc1	pop
Go	Go	k1gMnSc1	Go
To	to	k9	to
Sixties	Sixties	k1gInSc4	Sixties
<g/>
,	,	kIx,	,
živě	živě	k6eAd1	živě
vysílaném	vysílaný	k2eAgInSc6d1	vysílaný
na	na	k7c4	na
BBC	BBC	kA	BBC
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahrála	zahrát	k5eAaPmAgFnS	zahrát
"	"	kIx"	"
<g/>
I	i	k8xC	i
Can	Can	k1gFnSc3	Can
See	See	k1gFnSc3	See
For	forum	k1gNnPc2	forum
Miles	Miles	k1gInSc1	Miles
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
nahrála	nahrát	k5eAaPmAgFnS	nahrát
Live	Live	k1gFnSc1	Live
at	at	k?	at
Leeds	Leedsa	k1gFnPc2	Leedsa
<g/>
,	,	kIx,	,
mnoha	mnoho	k4c3	mnoho
kritiky	kritika	k1gFnSc2	kritika
označované	označovaný	k2eAgFnPc1d1	označovaná
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
živé	živý	k2eAgNnSc4d1	živé
rockové	rockový	k2eAgNnSc4d1	rockové
album	album	k1gNnSc4	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Leedsu	Leeds	k1gInSc6	Leeds
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
Tommymu	Tommym	k1gInSc3	Tommym
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
stali	stát	k5eAaPmAgMnP	stát
prvním	první	k4xOgMnSc6	první
rockovým	rockový	k2eAgMnSc7d1	rockový
interpretem	interpret	k1gMnSc7	interpret
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
Metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
opeře	opera	k1gFnSc6	opera
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
vydali	vydat	k5eAaPmAgMnP	vydat
hit	hit	k1gInSc4	hit
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Seeker	Seeker	k1gMnSc1	Seeker
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1971	[number]	k4	1971
začala	začít	k5eAaPmAgFnS	začít
kapela	kapela	k1gFnSc1	kapela
nahrávat	nahrávat	k5eAaImF	nahrávat
dostupný	dostupný	k2eAgInSc4d1	dostupný
materiál	materiál	k1gInSc4	materiál
k	k	k7c3	k
Lifehousu	Lifehous	k1gInSc3	Lifehous
<g/>
,	,	kIx,	,
Townshendově	Townshendův	k2eAgFnSc3d1	Townshendův
rockové	rockový	k2eAgFnSc3d1	rocková
opeře	opera	k1gFnSc3	opera
<g/>
,	,	kIx,	,
s	s	k7c7	s
Kitem	kit	k1gInSc7	kit
Lambertem	Lambert	k1gMnSc7	Lambert
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
nahrávání	nahrávání	k1gNnSc1	nahrávání
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
s	s	k7c7	s
Glynem	Glyn	k1gMnSc7	Glyn
Johnsem	Johns	k1gMnSc7	Johns
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
nahraného	nahraný	k2eAgInSc2d1	nahraný
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
nesouvisející	související	k2eNgFnSc1d1	nesouvisející
Entwistlova	Entwistlův	k2eAgFnSc1d1	Entwistlův
skladba	skladba	k1gFnSc1	skladba
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
jako	jako	k9	jako
tradiční	tradiční	k2eAgNnSc4d1	tradiční
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Who	Who	k1gFnPc2	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Next	Nexta	k1gFnPc2	Nexta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kritiků	kritik	k1gMnPc2	kritik
a	a	k8xC	a
fanoušků	fanoušek	k1gMnPc2	fanoušek
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejúspěšnější	úspěšný	k2eAgNnSc4d3	nejúspěšnější
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
americké	americký	k2eAgFnSc2d1	americká
popové	popový	k2eAgFnSc2d1	popová
hitparády	hitparáda	k1gFnSc2	hitparáda
a	a	k8xC	a
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rile	k2eAgInPc4d1	Rile
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Get	Get	k1gMnSc1	Get
Fooled	Fooled	k1gMnSc1	Fooled
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
příkladů	příklad	k1gInPc2	příklad
užití	užití	k1gNnSc2	užití
syntetizátoru	syntetizátor	k1gInSc2	syntetizátor
v	v	k7c6	v
rockové	rockový	k2eAgFnSc6d1	rocková
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Syntezátory	syntezátor	k1gInPc1	syntezátor
ale	ale	k9	ale
můžeme	moct	k5eAaImIp1nP	moct
slyšet	slyšet	k5eAaImF	slyšet
i	i	k9	i
na	na	k7c6	na
jiných	jiný	k2eAgFnPc6d1	jiná
skladbách	skladba	k1gFnPc6	skladba
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Bargain	Bargain	k1gInSc1	Bargain
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Going	Going	k1gInSc1	Going
Mobile	mobile	k1gNnSc2	mobile
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Song	song	k1gInSc1	song
Is	Is	k1gMnSc1	Is
Over	Over	k1gMnSc1	Over
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
vydali	vydat	k5eAaPmAgMnP	vydat
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
hit	hit	k1gInSc1	hit
"	"	kIx"	"
<g/>
Lets	Lets	k1gInSc1	Lets
See	See	k1gMnSc1	See
Action	Action	k1gInSc1	Action
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
vydali	vydat	k5eAaPmAgMnP	vydat
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Join	Join	k1gInSc1	Join
Together	Togethra	k1gFnPc2	Togethra
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Relay	Relaa	k1gFnSc2	Relaa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Who	Who	k1gFnSc6	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Next	Next	k1gInSc1	Next
následovala	následovat	k5eAaImAgFnS	následovat
Quadrophenia	Quadrophenium	k1gNnSc2	Quadrophenium
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
dokončená	dokončený	k2eAgFnSc1d1	dokončená
dvojdisková	dvojdiskový	k2eAgFnSc1d1	dvojdisková
rocková	rockový	k2eAgFnSc1d1	rocková
opera	opera	k1gFnSc1	opera
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
chlapci	chlapec	k1gMnSc6	chlapec
Jimmym	Jimmym	k1gInSc4	Jimmym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zjistit	zjistit	k5eAaPmF	zjistit
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
identitu	identita	k1gFnSc4	identita
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc3	jeho
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
příběhu	příběh	k1gInSc2	příběh
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
potyčky	potyčka	k1gFnPc1	potyčka
mezi	mezi	k7c4	mezi
Mods	Mods	k1gInSc4	Mods
a	a	k8xC	a
Rockers	Rockers	k1gInSc1	Rockers
začátkem	začátkem	k7c2	začátkem
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
vydali	vydat	k5eAaPmAgMnP	vydat
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
album	album	k1gNnSc1	album
Odds	Oddsa	k1gFnPc2	Oddsa
&	&	k?	&
Sods	Sodsa	k1gFnPc2	Sodsa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
několik	několik	k4yIc1	několik
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
předčasně	předčasně	k6eAd1	předčasně
ukončeného	ukončený	k2eAgInSc2d1	ukončený
projektu	projekt	k1gInSc2	projekt
Lifehouse	Lifehouse	k1gFnSc2	Lifehouse
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
album	album	k1gNnSc1	album
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Who	Who	k1gFnSc2	Who
by	by	kYmCp3nS	by
Numbers	Numbersa	k1gFnPc2	Numbersa
<g/>
,	,	kIx,	,
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
introspektivní	introspektivní	k2eAgFnPc4d1	introspektivní
skladby	skladba	k1gFnPc4	skladba
odlehčené	odlehčený	k2eAgFnPc4d1	odlehčená
hitem	hit	k1gInSc7	hit
"	"	kIx"	"
<g/>
Squeeze	Squeeze	k1gFnSc1	Squeeze
Box	box	k1gInSc1	box
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
považovali	považovat	k5eAaImAgMnP	považovat
By	by	k9	by
Numbers	Numbers	k1gInSc4	Numbers
za	za	k7c4	za
Townshendův	Townshendův	k2eAgInSc4d1	Townshendův
dopis	dopis	k1gInSc4	dopis
na	na	k7c4	na
rozloučenou	rozloučená	k1gFnSc4	rozloučená
<g/>
.	.	kIx.	.
</s>
<s>
Filmová	filmový	k2eAgFnSc1d1	filmová
verze	verze	k1gFnSc1	verze
Tommyho	Tommy	k1gMnSc2	Tommy
vydaná	vydaný	k2eAgNnPc1d1	vydané
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Kenem	Ken	k1gMnSc7	Ken
Russellem	Russell	k1gMnSc7	Russell
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
Daltrey	Daltrea	k1gFnSc2	Daltrea
a	a	k8xC	a
Townshendovi	Townshendův	k2eAgMnPc1d1	Townshendův
vynesla	vynést	k5eAaPmAgFnS	vynést
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Academy	Academa	k1gFnPc4	Academa
Award	Awardo	k1gNnPc2	Awardo
za	za	k7c4	za
původní	původní	k2eAgFnSc4d1	původní
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1975	[number]	k4	1975
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
The	The	k1gFnSc4	The
Who	Who	k1gFnSc2	Who
rekord	rekord	k1gInSc4	rekord
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
koncert	koncert	k1gInSc4	koncert
konaný	konaný	k2eAgInSc1d1	konaný
pod	pod	k7c7	pod
střechou	střecha	k1gFnSc7	střecha
v	v	k7c4	v
Pointiac	Pointiac	k1gFnSc4	Pointiac
Silverdome	Silverdom	k1gInSc5	Silverdom
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
75	[number]	k4	75
962	[number]	k4	962
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1976	[number]	k4	1976
zahráli	zahrát	k5eAaPmAgMnP	zahrát
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
minimálně	minimálně	k6eAd1	minimálně
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
zapsán	zapsat	k5eAaPmNgMnS	zapsat
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
jako	jako	k8xS	jako
nejhlasitější	hlasitý	k2eAgInSc1d3	nejhlasitější
koncert	koncert	k1gInSc1	koncert
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
hlasitostí	hlasitost	k1gFnSc7	hlasitost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
<g/>
dB	db	kA	db
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1978	[number]	k4	1978
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
Who	Who	k1gFnSc1	Who
Are	ar	k1gInSc5	ar
You	You	k1gFnPc2	You
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgInS	být
zastíněn	zastínit	k5eAaPmNgInS	zastínit
Moonovou	Moonový	k2eAgFnSc7d1	Moonový
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Moon	Moon	k1gMnSc1	Moon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
po	po	k7c6	po
nadměrné	nadměrný	k2eAgFnSc6d1	nadměrná
dávce	dávka	k1gFnSc6	dávka
Heminevrinu	Heminevrin	k1gInSc2	Heminevrin
-	-	kIx~	-
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
předepsán	předepsat	k5eAaPmNgInS	předepsat
pro	pro	k7c4	pro
potlačení	potlačení	k1gNnSc4	potlačení
abstinenčních	abstinenční	k2eAgInPc2d1	abstinenční
příznaků	příznak	k1gInPc2	příznak
alkoholu	alkohol	k1gInSc2	alkohol
-	-	kIx~	-
pár	pár	k4xCyI	pár
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
večírku	večírek	k1gInSc6	večírek
u	u	k7c2	u
Paula	Paul	k1gMnSc2	Paul
McCartneyho	McCartney	k1gMnSc2	McCartney
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
Kenney	Kennea	k1gFnSc2	Kennea
Jones	Jones	k1gInSc1	Jones
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
z	z	k7c2	z
kapel	kapela	k1gFnPc2	kapela
The	The	k1gFnSc2	The
Small	Small	k1gMnSc1	Small
Faces	Faces	k1gMnSc1	Faces
a	a	k8xC	a
The	The	k1gMnSc1	The
Faces	Faces	k1gMnSc1	Faces
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1979	[number]	k4	1979
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
vrátila	vrátit	k5eAaPmAgFnS	vrátit
na	na	k7c4	na
pódium	pódium	k1gNnSc1	pódium
koncertem	koncert	k1gInSc7	koncert
v	v	k7c6	v
Rainbow	Rainbow	k1gFnSc6	Rainbow
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
následovaly	následovat	k5eAaImAgFnP	následovat
vystoupení	vystoupení	k1gNnPc4	vystoupení
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Wembley	Wemblea	k1gFnSc2	Wemblea
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
v	v	k7c6	v
Západním	západní	k2eAgNnSc6d1	západní
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c4	v
Capitol	Capitol	k1gInSc4	Capitol
Theatre	Theatr	k1gInSc5	Theatr
v	v	k7c6	v
Passaic	Passaice	k1gInPc2	Passaice
v	v	k7c6	v
New	New	k1gFnSc6	New
Jersey	Jersea	k1gFnSc2	Jersea
a	a	k8xC	a
pět	pět	k4xCc4	pět
koncertů	koncert	k1gInPc2	koncert
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
v	v	k7c4	v
New	New	k1gFnPc4	New
York	York	k1gInSc4	York
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vydali	vydat	k5eAaPmAgMnP	vydat
The	The	k1gMnPc1	The
Who	Who	k1gFnPc2	Who
také	také	k9	také
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
The	The	k1gFnPc2	The
Kids	Kidsa	k1gFnPc2	Kidsa
Are	ar	k1gInSc5	ar
Alright	Alright	k1gInSc4	Alright
a	a	k8xC	a
filmovou	filmový	k2eAgFnSc4d1	filmová
verzi	verze	k1gFnSc4	verze
Quadrophenie	Quadrophenie	k1gFnSc2	Quadrophenie
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgFnSc2d2	pozdější
kasovní	kasovní	k2eAgInSc4d1	kasovní
trhák	trhák	k1gInSc4	trhák
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
se	s	k7c7	s
The	The	k1gMnSc7	The
Who	Who	k1gMnSc7	Who
stali	stát	k5eAaPmAgMnP	stát
třetí	třetí	k4xOgNnSc4	třetí
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
po	po	k7c6	po
The	The	k1gFnSc6	The
Beatles	Beatles	k1gFnPc2	Beatles
a	a	k8xC	a
The	The	k1gFnPc2	The
Band	banda	k1gFnPc2	banda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
magazínu	magazín	k1gInSc2	magazín
Time	Tim	k1gInSc2	Tim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
na	na	k7c4	na
menší	malý	k2eAgNnSc4d2	menší
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poznamenáno	poznamenat	k5eAaPmNgNnS	poznamenat
tragédií	tragédie	k1gFnSc7	tragédie
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1979	[number]	k4	1979
v	v	k7c4	v
Riverfront	Riverfront	k1gInSc4	Riverfront
Coliseum	Coliseum	k1gNnSc4	Coliseum
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
v	v	k7c6	v
davové	davový	k2eAgFnSc6d1	davová
tlačenici	tlačenice	k1gFnSc6	tlačenice
11	[number]	k4	11
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
26	[number]	k4	26
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
místa	místo	k1gNnPc1	místo
v	v	k7c6	v
hledišti	hlediště	k1gNnSc6	hlediště
nebyla	být	k5eNaImAgFnS	být
rezervovatelná	rezervovatelný	k2eAgFnSc1d1	rezervovatelný
-	-	kIx~	-
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
haly	hala	k1gFnSc2	hala
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
získají	získat	k5eAaPmIp3nP	získat
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
spousta	spousta	k1gFnSc1	spousta
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
čekali	čekat	k5eAaImAgMnP	čekat
venku	venku	k6eAd1	venku
<g/>
,	,	kIx,	,
omylem	omylem	k6eAd1	omylem
pokládala	pokládat	k5eAaImAgFnS	pokládat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
zkoušku	zkouška	k1gFnSc4	zkouška
kapely	kapela	k1gFnSc2	kapela
za	za	k7c4	za
skutečný	skutečný	k2eAgInSc4d1	skutečný
začátek	začátek	k1gInSc4	začátek
koncertu	koncert	k1gInSc2	koncert
a	a	k8xC	a
pokusila	pokusit	k5eAaPmAgFnS	pokusit
se	se	k3xPyFc4	se
natlačit	natlačit	k5eAaBmF	natlačit
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
jen	jen	k9	jen
část	část	k1gFnSc1	část
vchodu	vchod	k1gInSc2	vchod
<g/>
,	,	kIx,	,
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zúžené	zúžený	k2eAgNnSc1d1	zúžené
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
dostat	dostat	k5eAaPmF	dostat
tisíce	tisíc	k4xCgInPc1	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
ušlapáni	ušlapat	k5eAaPmNgMnP	ušlapat
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nebyla	být	k5eNaImAgFnS	být
informována	informován	k2eAgFnSc1d1	informována
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
koncert	koncert	k1gInSc1	koncert
neskončil	skončit	k5eNaPmAgInS	skončit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
čelní	čelní	k2eAgMnPc1d1	čelní
představitelé	představitel	k1gMnPc1	představitel
města	město	k1gNnSc2	město
obávali	obávat	k5eAaImAgMnP	obávat
dalších	další	k2eAgInPc2d1	další
davových	davový	k2eAgInPc2d1	davový
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgInS	být
koncert	koncert	k1gInSc1	koncert
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
hluboce	hluboko	k6eAd1	hluboko
otřesena	otřesen	k2eAgFnSc1d1	otřesena
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
incidentu	incident	k1gInSc6	incident
dozvěděla	dozvědět	k5eAaPmAgFnS	dozvědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
si	se	k3xPyFc3	se
na	na	k7c6	na
příštích	příští	k2eAgInPc6d1	příští
koncertech	koncert	k1gInPc6	koncert
výpomoc	výpomoc	k1gFnSc4	výpomoc
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
večer	večer	k1gInSc4	večer
v	v	k7c6	v
Buffalu	Buffal	k1gInSc6	Buffal
Daltrey	Daltrea	k1gFnSc2	Daltrea
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapela	kapela	k1gFnSc1	kapela
"	"	kIx"	"
<g/>
minulou	minulý	k2eAgFnSc4d1	minulá
noc	noc	k1gFnSc4	noc
ztratila	ztratit	k5eAaPmAgFnS	ztratit
část	část	k1gFnSc1	část
svojí	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
tenhle	tenhle	k3xDgInSc4	tenhle
koncert	koncert	k1gInSc4	koncert
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
vydala	vydat	k5eAaPmAgFnS	vydat
s	s	k7c7	s
Jonesem	Jones	k1gMnSc7	Jones
za	za	k7c7	za
bicími	bicí	k2eAgFnPc7d1	bicí
dvě	dva	k4xCgNnPc1	dva
studiová	studiový	k2eAgNnPc1d1	studiové
alba	album	k1gNnPc1	album
<g/>
,	,	kIx,	,
Face	Fac	k1gInPc1	Fac
Dances	Dancesa	k1gFnPc2	Dancesa
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
a	a	k8xC	a
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hard	Harda	k1gFnPc2	Harda
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
obě	dva	k4xCgNnPc1	dva
alba	album	k1gNnPc1	album
prodávala	prodávat	k5eAaImAgNnP	prodávat
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
It	It	k1gFnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hard	Harda	k1gFnPc2	Harda
získalo	získat	k5eAaPmAgNnS	získat
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
pětihvězdičkové	pětihvězdičkový	k2eAgNnSc4d1	pětihvězdičkové
hodnocení	hodnocení	k1gNnPc4	hodnocení
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
fanoušci	fanoušek	k1gMnPc1	fanoušek
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
nový	nový	k2eAgInSc4d1	nový
zvuk	zvuk	k1gInSc4	zvuk
kapely	kapela	k1gFnSc2	kapela
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Townshendův	Townshendův	k2eAgInSc1d1	Townshendův
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
-	-	kIx~	-
jeho	jeho	k3xOp3gNnSc1	jeho
manželství	manželství	k1gNnSc1	manželství
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
kvůli	kvůli	k7c3	kvůli
neustálým	neustálý	k2eAgNnPc3d1	neustálé
turné	turné	k1gNnPc3	turné
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
nadměrně	nadměrně	k6eAd1	nadměrně
pít	pít	k5eAaImF	pít
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
závislým	závislý	k2eAgMnSc7d1	závislý
na	na	k7c6	na
heroinu	heroin	k1gInSc6	heroin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
předchozímu	předchozí	k2eAgInSc3d1	předchozí
protidrogovému	protidrogový	k2eAgInSc3d1	protidrogový
postoji	postoj	k1gInSc3	postoj
šokovalo	šokovat	k5eAaBmAgNnS	šokovat
i	i	k9	i
jeho	jeho	k3xOp3gMnPc4	jeho
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
se	se	k3xPyFc4	se
uzdravil	uzdravit	k5eAaPmAgInS	uzdravit
a	a	k8xC	a
The	The	k1gMnPc1	The
Who	Who	k1gMnSc4	Who
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
na	na	k7c4	na
své	své	k1gNnSc4	své
"	"	kIx"	"
<g/>
rozlučkové	rozlučkový	k2eAgNnSc4d1	rozlučkové
<g/>
"	"	kIx"	"
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
dva	dva	k4xCgInPc4	dva
koncerty	koncert	k1gInPc4	koncert
v	v	k7c4	v
Shea	She	k2eAgNnPc4d1	She
Stadium	stadium	k1gNnSc4	stadium
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
ještě	ještě	k6eAd1	ještě
jednou	jednou	k6eAd1	jednou
vyjet	vyjet	k5eAaPmF	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
z	z	k7c2	z
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
stane	stanout	k5eAaPmIp3nS	stanout
studiová	studiový	k2eAgFnSc1d1	studiová
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
nejvýdělečnější	výdělečný	k2eAgNnSc1d3	nejvýdělečnější
turné	turné	k1gNnSc1	turné
roku	rok	k1gInSc2	rok
s	s	k7c7	s
vyprodanými	vyprodaný	k2eAgNnPc7d1	vyprodané
hledišti	hlediště	k1gNnPc7	hlediště
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
strávil	strávit	k5eAaPmAgMnS	strávit
část	část	k1gFnSc4	část
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
snahou	snaha	k1gFnSc7	snaha
napsat	napsat	k5eAaBmF	napsat
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
stále	stále	k6eAd1	stále
dlužil	dlužit	k5eAaImAgMnS	dlužit
Warner	Warner	k1gMnSc1	Warner
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
</s>
<s>
Records	Records	k1gInSc1	Records
díky	díky	k7c3	díky
smlouvě	smlouva	k1gFnSc3	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
Townshend	Townshend	k1gMnSc1	Townshend
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c4	za
neschopného	schopný	k2eNgMnSc4d1	neschopný
vytvořit	vytvořit	k5eAaPmF	vytvořit
materiál	materiál	k1gInSc4	materiál
vhodný	vhodný	k2eAgInSc4d1	vhodný
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Who	Who	k1gFnSc2	Who
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
oznámil	oznámit	k5eAaPmAgMnS	oznámit
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
kapely	kapela	k1gFnSc2	kapela
a	a	k8xC	a
Daltreymu	Daltreym	k1gInSc2	Daltreym
<g/>
,	,	kIx,	,
Entwistleovi	Entwistleus	k1gMnSc3	Entwistleus
a	a	k8xC	a
Jonesovi	Jones	k1gMnSc3	Jones
popřál	popřát	k5eAaPmAgMnS	popřát
všechno	všechen	k3xTgNnSc1	všechen
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
sólové	sólový	k2eAgInPc4d1	sólový
projekty	projekt	k1gInPc4	projekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
White	Whit	k1gInSc5	Whit
City	city	k1gNnSc1	city
<g/>
:	:	kIx,	:
A	a	k9	a
Novel	novela	k1gFnPc2	novela
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Iron	iron	k1gInSc1	iron
Man	Man	k1gMnSc1	Man
a	a	k8xC	a
Psychoderelict	Psychoderelict	k1gMnSc1	Psychoderelict
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1985	[number]	k4	1985
se	se	k3xPyFc4	se
The	The	k1gFnSc1	The
Who	Who	k1gMnPc2	Who
-	-	kIx~	-
včetně	včetně	k7c2	včetně
Kenneyho	Kenney	k1gMnSc2	Kenney
Jonese	Jonese	k1gFnSc2	Jonese
-	-	kIx~	-
znovu	znovu	k6eAd1	znovu
sešli	sejít	k5eAaPmAgMnP	sejít
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Live	Live	k1gFnPc2	Live
Aid	Aida	k1gFnPc2	Aida
Boba	Bob	k1gMnSc2	Bob
Geldofa	Geldof	k1gMnSc2	Geldof
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Wembley	Wemblea	k1gFnSc2	Wemblea
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Přenosový	přenosový	k2eAgInSc4d1	přenosový
vůz	vůz	k1gInSc4	vůz
BBC	BBC	kA	BBC
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc1	Generation
<g/>
"	"	kIx"	"
pojistky	pojistka	k1gFnPc1	pojistka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
kompletní	kompletní	k2eAgFnSc4d1	kompletní
ztrátu	ztráta	k1gFnSc4	ztráta
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kapela	kapela	k1gFnSc1	kapela
hrála	hrát	k5eAaImAgFnS	hrát
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
videozáznamu	videozáznam	k1gInSc2	videozáznam
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc1	Generation
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Pinball	Pinball	k1gMnSc1	Pinball
Wizard	Wizard	k1gMnSc1	Wizard
<g/>
"	"	kIx"	"
nebyla	být	k5eNaImAgFnS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvuková	zvukový	k2eAgFnSc1d1	zvuková
stopa	stopa	k1gFnSc1	stopa
"	"	kIx"	"
<g/>
Pinball	Pinball	k1gMnSc1	Pinball
Wizard	Wizard	k1gMnSc1	Wizard
<g/>
"	"	kIx"	"
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
písní	píseň	k1gFnPc2	píseň
byla	být	k5eAaImAgFnS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
rádiem	rádius	k1gInSc7	rádius
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
s	s	k7c7	s
"	"	kIx"	"
<g/>
Love	lov	k1gInSc5	lov
Reign	Reign	k1gNnSc4	Reign
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
er	er	k?	er
Me	Me	k1gFnSc1	Me
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Wont	Wont	k2eAgMnSc1d1	Wont
Get	Get	k1gMnSc1	Get
Fooled	Fooled	k1gMnSc1	Fooled
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
na	na	k7c4	na
BRIT	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc1	Awards
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
kapela	kapela	k1gFnSc1	kapela
oceněna	ocenit	k5eAaPmNgFnS	ocenit
British	Britisha	k1gFnPc2	Britisha
Phonographic	Phonographice	k1gFnPc2	Phonographice
Industry	Industra	k1gFnSc2	Industra
Cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
(	(	kIx(	(
<g/>
Lifetime	Lifetim	k1gInSc5	Lifetim
Achievement	Achievement	k1gMnSc1	Achievement
Award	Award	k1gInSc1	Award
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Who	Who	k1gMnSc1	Who
na	na	k7c6	na
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
zahráli	zahrát	k5eAaPmAgMnP	zahrát
krátký	krátký	k2eAgInSc4d1	krátký
soubor	soubor	k1gInSc4	soubor
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
k	k	k7c3	k
příležitosti	příležitost	k1gFnSc3	příležitost
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
The	The	k1gFnSc2	The
Kids	Kidsa	k1gFnPc2	Kidsa
Are	ar	k1gInSc5	ar
Alright	Alrightum	k1gNnPc2	Alrightum
na	na	k7c4	na
reunion	reunion	k1gInSc4	reunion
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
převážně	převážně	k6eAd1	převážně
skladby	skladba	k1gFnSc2	skladba
z	z	k7c2	z
Tommyho	Tommy	k1gMnSc2	Tommy
<g/>
.	.	kIx.	.
</s>
<s>
Simon	Simon	k1gMnSc1	Simon
Phillips	Phillips	k1gInSc4	Phillips
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
,	,	kIx,	,
Steve	Steve	k1gMnSc1	Steve
"	"	kIx"	"
<g/>
Boltz	Boltz	k1gMnSc1	Boltz
<g/>
"	"	kIx"	"
Bolton	Bolton	k1gInSc1	Bolton
hlavní	hlavní	k2eAgFnSc4d1	hlavní
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
Townshend	Townshend	k1gMnSc1	Townshend
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
akustickou	akustický	k2eAgFnSc4d1	akustická
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zminimalizoval	zminimalizovat	k5eAaImAgMnS	zminimalizovat
poškození	poškození	k1gNnSc4	poškození
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
byly	být	k5eAaImAgInP	být
koncerty	koncert	k1gInPc1	koncert
vyprodané	vyprodaný	k2eAgInPc1d1	vyprodaný
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
čtyř	čtyři	k4xCgNnPc2	čtyři
vystoupení	vystoupení	k1gNnPc2	vystoupení
v	v	k7c4	v
Giants	Giants	k1gInSc4	Giants
Stadium	stadium	k1gNnSc1	stadium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gInPc4	jejich
dva	dva	k4xCgInPc4	dva
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
Sullivan	Sullivan	k1gMnSc1	Sullivan
Stadium	stadium	k1gNnSc4	stadium
ve	v	k7c4	v
Foxboro	Foxbora	k1gFnSc5	Foxbora
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
100	[number]	k4	100
000	[number]	k4	000
lístků	lístek	k1gInPc2	lístek
za	za	k7c4	za
méně	málo	k6eAd2	málo
než	než	k8xS	než
8	[number]	k4	8
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
překonali	překonat	k5eAaPmAgMnP	překonat
předchozí	předchozí	k2eAgInPc4d1	předchozí
rekordy	rekord	k1gInPc4	rekord
U2	U2	k1gFnSc2	U2
a	a	k8xC	a
Davida	David	k1gMnSc2	David
Bowieho	Bowie	k1gMnSc2	Bowie
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
bylo	být	k5eAaImAgNnS	být
prodáno	prodat	k5eAaPmNgNnS	prodat
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
miliony	milion	k4xCgInPc4	milion
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
vydali	vydat	k5eAaPmAgMnP	vydat
dvojdiskové	dvojdiskový	k2eAgNnSc4d1	dvojdiskové
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
Join	Joina	k1gFnPc2	Joina
Together	Togethra	k1gFnPc2	Togethra
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
jak	jak	k6eAd1	jak
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
uvedeni	uveden	k2eAgMnPc1d1	uveden
U2	U2	k1gMnPc1	U2
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
Bono	bona	k1gFnSc5	bona
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
jsou	být	k5eAaImIp3nP	být
našimi	náš	k3xOp1gInPc7	náš
vzory	vzor	k1gInPc4	vzor
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
kterákoliv	kterýkoliv	k3yIgFnSc1	kterýkoliv
jiná	jiný	k2eAgFnSc1d1	jiná
kapela	kapela	k1gFnSc1	kapela
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
nahráli	nahrát	k5eAaBmAgMnP	nahrát
The	The	k1gMnPc1	The
Who	Who	k1gMnSc1	Who
cover	cover	k1gMnSc1	cover
"	"	kIx"	"
<g/>
Saturday	Saturday	k1gInPc1	Saturday
Night	Nighta	k1gFnPc2	Nighta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Alright	Alright	k1gInSc1	Alright
for	forum	k1gNnPc2	forum
Fighting	Fighting	k1gInSc1	Fighting
<g/>
"	"	kIx"	"
od	od	k7c2	od
Eltona	Elton	k1gMnSc4	Elton
Johna	John	k1gMnSc2	John
pro	pro	k7c4	pro
tribute	tribut	k1gInSc5	tribut
album	album	k1gNnSc1	album
Two	Two	k1gFnPc2	Two
Rooms	Roomsa	k1gFnPc2	Roomsa
<g/>
:	:	kIx,	:
Celebrating	Celebrating	k1gInSc1	Celebrating
the	the	k?	the
Songs	Songs	k1gInSc1	Songs
of	of	k?	of
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
&	&	k?	&
Bernie	Bernie	k1gFnSc2	Bernie
Taupin	Taupina	k1gFnPc2	Taupina
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
naposledy	naposledy	k6eAd1	naposledy
<g/>
,	,	kIx,	,
co	co	k9	co
nahrávali	nahrávat	k5eAaImAgMnP	nahrávat
nějakou	nějaký	k3yIgFnSc4	nějaký
novou	nový	k2eAgFnSc4d1	nová
studiovou	studiový	k2eAgFnSc4d1	studiová
nahrávku	nahrávka	k1gFnSc4	nahrávka
s	s	k7c7	s
Entwistlem	Entwistl	k1gInSc7	Entwistl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
oslavil	oslavit	k5eAaPmAgMnS	oslavit
Daltrey	Daltre	k2eAgFnPc4d1	Daltre
padesáté	padesátý	k4xOgFnPc4	padesátý
narozeniny	narozeniny	k1gFnPc4	narozeniny
dvěma	dva	k4xCgInPc7	dva
koncerty	koncert	k1gInPc7	koncert
v	v	k7c4	v
Carnegie	Carnegie	k1gFnPc4	Carnegie
Hall	Halla	k1gFnPc2	Halla
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
jako	jako	k9	jako
hosté	host	k1gMnPc1	host
Entwistle	Entwistle	k1gMnSc1	Entwistle
s	s	k7c7	s
Townshendem	Townshend	k1gMnSc7	Townshend
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
přítomni	přítomen	k2eAgMnPc1d1	přítomen
<g/>
,	,	kIx,	,
na	na	k7c6	na
pódiu	pódium	k1gNnSc6	pódium
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
společně	společně	k6eAd1	společně
až	až	k6eAd1	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
koncertu	koncert	k1gInSc2	koncert
při	pře	k1gFnSc3	pře
"	"	kIx"	"
<g/>
Join	Join	k1gNnSc1	Join
Together	Togethra	k1gFnPc2	Togethra
<g/>
"	"	kIx"	"
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
hosty	host	k1gMnPc7	host
<g/>
.	.	kIx.	.
</s>
<s>
Daltrey	Daltrea	k1gFnPc1	Daltrea
ten	ten	k3xDgInSc4	ten
rok	rok	k1gInSc4	rok
vyjel	vyjet	k5eAaPmAgMnS	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
Entwistlem	Entwistlo	k1gNnSc7	Entwistlo
<g/>
,	,	kIx,	,
s	s	k7c7	s
klávesistou	klávesista	k1gMnSc7	klávesista
Johnem	John	k1gMnSc7	John
"	"	kIx"	"
<g/>
Rabbitem	Rabbit	k1gInSc7	Rabbit
<g/>
"	"	kIx"	"
Bundrickem	Bundrick	k1gInSc7	Bundrick
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
Zakem	Zak	k1gMnSc7	Zak
Starkeym	Starkeym	k1gInSc4	Starkeym
a	a	k8xC	a
Simonem	Simon	k1gMnSc7	Simon
Townshendem	Townshend	k1gMnSc7	Townshend
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
místo	místo	k7c2	místo
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Townshend	Townshend	k1gInSc4	Townshend
<g/>
,	,	kIx,	,
Entwistle	Entwistle	k1gMnPc4	Entwistle
<g/>
,	,	kIx,	,
Daltrey	Daltre	k1gMnPc4	Daltre
<g/>
,	,	kIx,	,
Starkey	Starke	k2eAgMnPc4d1	Starke
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
hosté	host	k1gMnPc1	host
v	v	k7c6	v
Hyde	Hyde	k1gNnSc6	Hyde
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahráli	zahrát	k5eAaPmAgMnP	zahrát
Quadrophenii	Quadrophenie	k1gFnSc4	Quadrophenie
<g/>
.	.	kIx.	.
</s>
<s>
Komentář	komentář	k1gInSc1	komentář
k	k	k7c3	k
vystoupení	vystoupení	k1gNnSc3	vystoupení
četl	číst	k5eAaImAgMnS	číst
Phil	Phil	k1gMnSc1	Phil
Daniels	Daniels	k1gInSc4	Daniels
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
hrál	hrát	k5eAaImAgMnS	hrát
Moda	Mod	k2eAgMnSc4d1	Mod
Jimmyho	Jimmy	k1gMnSc4	Jimmy
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
technickým	technický	k2eAgInPc3d1	technický
problémům	problém	k1gInPc3	problém
byl	být	k5eAaImAgInS	být
koncert	koncert	k1gInSc1	koncert
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
a	a	k8xC	a
následovala	následovat	k5eAaImAgFnS	následovat
jej	on	k3xPp3gNnSc4	on
šestidenní	šestidenní	k2eAgFnSc1d1	šestidenní
štace	štace	k1gFnSc1	štace
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
hrál	hrát	k5eAaImAgMnS	hrát
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
akustickou	akustický	k2eAgFnSc4d1	akustická
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
nebyly	být	k5eNaImAgInP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
jako	jako	k8xS	jako
akce	akce	k1gFnPc1	akce
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
Quadrophenie	Quadrophenie	k1gFnSc2	Quadrophenie
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c6	v
turné	turné	k1gNnSc6	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
a	a	k8xC	a
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gMnSc1	Townshend
hrál	hrát	k5eAaImAgMnS	hrát
většinou	většina	k1gFnSc7	většina
na	na	k7c4	na
akustickou	akustický	k2eAgFnSc4d1	akustická
kytaru	kytara	k1gFnSc4	kytara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
při	při	k7c6	při
vybraných	vybraný	k2eAgFnPc6d1	vybraná
skladbách	skladba	k1gFnPc6	skladba
i	i	k9	i
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zařadila	zařadit	k5eAaPmAgFnS	zařadit
VH1	VH1	k1gFnSc1	VH1
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
mezi	mezi	k7c7	mezi
100	[number]	k4	100
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
umělců	umělec	k1gMnPc2	umělec
rock	rock	k1gInSc4	rock
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
rollu	roll	k1gInSc3	roll
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
hráli	hrát	k5eAaImAgMnP	hrát
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
poprvé	poprvé	k6eAd1	poprvé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
jako	jako	k9	jako
pětičlenní	pětičlenný	k2eAgMnPc1d1	pětičlenný
s	s	k7c7	s
klávesistou	klávesista	k1gMnSc7	klávesista
Bundrickem	Bundricko	k1gNnSc7	Bundricko
a	a	k8xC	a
bubeníkem	bubeník	k1gMnSc7	bubeník
Starkeym	Starkeymum	k1gNnPc2	Starkeymum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
koncert	koncert	k1gInSc1	koncert
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1999	[number]	k4	1999
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
v	v	k7c6	v
MGM	MGM	kA	MGM
Grand	grand	k1gMnSc1	grand
Garden	Gardno	k1gNnPc2	Gardno
Arena	Arena	k1gFnSc1	Arena
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
koncertovali	koncertovat	k5eAaImAgMnP	koncertovat
akusticky	akusticky	k6eAd1	akusticky
na	na	k7c6	na
Bridge	Bridge	k1gFnSc6	Bridge
School	Schoola	k1gFnPc2	Schoola
Benefit	Benefita	k1gFnPc2	Benefita
Neila	Neil	k1gMnSc2	Neil
Younga	Young	k1gMnSc2	Young
v	v	k7c6	v
Shoreline	Shorelin	k1gInSc5	Shorelin
Amphitheatre	Amphitheatr	k1gInSc5	Amphitheatr
v	v	k7c4	v
Mountain	Mountain	k1gInSc4	Mountain
View	View	k1gFnSc2	View
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
zahráli	zahrát	k5eAaPmAgMnP	zahrát
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c4	v
House	house	k1gNnSc4	house
of	of	k?	of
Blues	blues	k1gNnSc2	blues
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
benefiční	benefiční	k2eAgInSc4d1	benefiční
koncert	koncert	k1gInSc4	koncert
pro	pro	k7c4	pro
Maryville	Maryville	k1gNnSc4	Maryville
Academy	Academa	k1gFnSc2	Academa
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
vánočním	vánoční	k2eAgInSc6d1	vánoční
charitativním	charitativní	k2eAgInSc6d1	charitativní
koncertě	koncert	k1gInSc6	koncert
v	v	k7c4	v
Shepherd	Shepherd	k1gInSc4	Shepherd
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Bush	Bush	k1gMnSc1	Bush
Empire	empir	k1gInSc5	empir
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
první	první	k4xOgInPc1	první
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc2	který
hrál	hrát	k5eAaImAgMnS	hrát
Townshend	Townshend	k1gMnSc1	Townshend
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1	vystoupení
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc1	Vegas
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
vysílané	vysílaný	k2eAgNnSc1d1	vysílané
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
na	na	k7c4	na
DVD	DVD	kA	DVD
The	The	k1gMnSc1	The
Vegas	Vegas	k1gMnSc1	Vegas
Job	Job	k1gMnSc1	Job
<g/>
.	.	kIx.	.
</s>
<s>
Recenze	recenze	k1gFnSc1	recenze
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
koncerty	koncert	k1gInPc4	koncert
byly	být	k5eAaImAgInP	být
kladné	kladný	k2eAgInPc1d1	kladný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgInSc6d1	úspěšný
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
charitativním	charitativní	k2eAgInSc7d1	charitativní
koncertem	koncert	k1gInSc7	koncert
pro	pro	k7c4	pro
Robin	robin	k2eAgInSc4d1	robin
Hood	Hood	k1gInSc4	Hood
Foundation	Foundation	k1gInSc4	Foundation
v	v	k7c4	v
Jacob	Jacoba	k1gFnPc2	Jacoba
K.	K.	kA	K.
Javits	Javits	k1gInSc1	Javits
Convention	Convention	k1gInSc1	Convention
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
charitativním	charitativní	k2eAgInSc7d1	charitativní
koncertem	koncert	k1gInSc7	koncert
pro	pro	k7c4	pro
Teenage	Teenage	k1gInSc4	Teenage
Cancer	Cancer	k1gInSc1	Cancer
Trust	trust	k1gInSc1	trust
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zařadila	zařadit	k5eAaPmAgFnS	zařadit
VH1	VH1	k1gFnSc1	VH1
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
na	na	k7c4	na
osmé	osmý	k4xOgNnSc4	osmý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
100	[number]	k4	100
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
umělců	umělec	k1gMnPc2	umělec
hard	hard	k6eAd1	hard
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
hrála	hrát	k5eAaImAgFnS	hrát
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Zakem	Zak	k1gMnSc7	Zak
Starkeym	Starkeym	k1gInSc4	Starkeym
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
The	The	k1gMnSc1	The
Concert	Concert	k1gMnSc1	Concert
for	forum	k1gNnPc2	forum
New	New	k1gMnSc3	New
York	York	k1gInSc1	York
City	city	k1gNnSc2	city
v	v	k7c4	v
Madison	Madison	k1gInSc4	Madison
Square	square	k1gInSc4	square
Garden	Gardna	k1gFnPc2	Gardna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zahráli	zahrát	k5eAaPmAgMnP	zahrát
"	"	kIx"	"
<g/>
Who	Who	k1gFnSc1	Who
Are	ar	k1gInSc5	ar
You	You	k1gMnSc5	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Baba	baba	k1gFnSc1	baba
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Riley	Rile	k2eAgInPc4d1	Rile
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Behind	Behind	k1gInSc1	Behind
Blue	Blue	k1gNnSc1	Blue
Eyes	Eyes	k1gInSc1	Eyes
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Won	won	k1gInSc1	won
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Get	Get	k1gMnSc1	Get
Fooled	Fooled	k1gMnSc1	Fooled
Again	Again	k1gMnSc1	Again
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
rodinám	rodina	k1gFnPc3	rodina
hasičů	hasič	k1gMnPc2	hasič
a	a	k8xC	a
policistů	policista	k1gMnPc2	policista
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
ve	v	k7c6	v
Světovém	světový	k2eAgNnSc6d1	světové
obchodním	obchodní	k2eAgNnSc6d1	obchodní
centru	centrum	k1gNnSc6	centrum
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Who	Who	k1gMnPc7	Who
byli	být	k5eAaImAgMnP	být
také	také	k9	také
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
oceněni	ocenit	k5eAaPmNgMnP	ocenit
cenou	cena	k1gFnSc7	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
zahráli	zahrát	k5eAaPmAgMnP	zahrát
The	The	k1gMnPc1	The
Who	Who	k1gMnSc1	Who
pět	pět	k4xCc4	pět
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
;	;	kIx,	;
27	[number]	k4	27
<g/>
.	.	kIx.	.
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
v	v	k7c6	v
Portsmouthu	Portsmouth	k1gInSc6	Portsmouth
<g/>
,	,	kIx,	,
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
ve	v	k7c6	v
Watfordu	Watfordo	k1gNnSc6	Watfordo
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
pro	pro	k7c4	pro
Teenage	Teenage	k1gInSc4	Teenage
Cancer	Cancer	k1gInSc1	Cancer
Trust	trust	k1gInSc1	trust
v	v	k7c4	v
Royal	Royal	k1gInSc4	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
poslední	poslední	k2eAgInPc1d1	poslední
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgInPc2	který
hrál	hrát	k5eAaImAgMnS	hrát
Entwistle	Entwistle	k1gMnSc1	Entwistle
s	s	k7c7	s
The	The	k1gMnSc7	The
Who	Who	k1gMnSc7	Who
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
jejich	jejich	k3xOp3gNnSc2	jejich
amerického	americký	k2eAgNnSc2d1	americké
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Entwistle	Entwistle	k1gFnSc4	Entwistle
v	v	k7c4	v
Hard	Hard	k1gInSc4	Hard
Rock	rock	k1gInSc1	rock
Hotelu	hotel	k1gInSc2	hotel
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegas	k1gInSc4	Vegas
nalezen	nalezen	k2eAgMnSc1d1	nalezen
mrtvý	mrtvý	k1gMnSc1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
byl	být	k5eAaImAgInS	být
infarkt	infarkt	k1gInSc1	infarkt
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
také	také	k9	také
kokain	kokain	k1gInSc4	kokain
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
otálení	otálení	k1gNnSc6	otálení
a	a	k8xC	a
dvou	dva	k4xCgInPc6	dva
zrušených	zrušený	k2eAgInPc6d1	zrušený
koncertech	koncert	k1gInPc6	koncert
začalo	začít	k5eAaPmAgNnS	začít
turné	turné	k1gNnSc4	turné
v	v	k7c4	v
Hollywood	Hollywood	k1gInSc4	Hollywood
Bowl	Bowlum	k1gNnPc2	Bowlum
s	s	k7c7	s
basistou	basista	k1gMnSc7	basista
Pinem	pin	k1gInSc7	pin
Palladinem	Palladin	k1gInSc7	Palladin
jako	jako	k8xC	jako
Entwistleova	Entwistleův	k2eAgNnPc1d1	Entwistleův
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
stálá	stálý	k2eAgFnSc1d1	stálá
<g/>
)	)	kIx)	)
náhrada	náhrada	k1gFnSc1	náhrada
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
koncertů	koncert	k1gInPc2	koncert
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
vydána	vydán	k2eAgFnSc1d1	vydána
na	na	k7c6	na
CD	CD	kA	CD
jako	jako	k8xC	jako
Encore	Encor	k1gInSc5	Encor
Series	Series	k1gInSc4	Series
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
časopis	časopis	k1gInSc1	časopis
Q	Q	kA	Q
označil	označit	k5eAaPmAgInS	označit
The	The	k1gFnSc7	The
Who	Who	k1gMnPc2	Who
jako	jako	k8xS	jako
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
"	"	kIx"	"
<g/>
50	[number]	k4	50
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
musíte	muset	k5eAaImIp2nP	muset
slyšet	slyšet	k5eAaImF	slyšet
<g/>
,	,	kIx,	,
než	než	k8xS	než
zemřete	zemřít	k5eAaPmIp2nP	zemřít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2003	[number]	k4	2003
se	se	k3xPyFc4	se
sedm	sedm	k4xCc1	sedm
alb	alba	k1gFnPc2	alba
od	od	k7c2	od
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
umístilo	umístit	k5eAaPmAgNnS	umístit
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
magazínu	magazín	k1gInSc2	magazín
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
500	[number]	k4	500
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
jakýkoliv	jakýkoliv	k3yIgMnSc1	jakýkoliv
jiný	jiný	k2eAgMnSc1d1	jiný
umělec	umělec	k1gMnSc1	umělec
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
The	The	k1gFnSc1	The
Beatles	Beatles	k1gFnSc1	Beatles
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
,	,	kIx,	,
Boba	Bob	k1gMnSc2	Bob
Dylana	Dylan	k1gMnSc2	Dylan
a	a	k8xC	a
Bruce	Bruce	k1gMnSc2	Bruce
Springsteena	Springsteen	k1gMnSc2	Springsteen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vydali	vydat	k5eAaPmAgMnP	vydat
The	The	k1gFnSc4	The
Who	Who	k1gFnPc2	Who
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Red	Red	k1gMnPc2	Red
Wine	Win	k1gFnSc2	Win
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Real	Real	k1gInSc1	Real
Good	Gooda	k1gFnPc2	Gooda
Looking	Looking	k1gInSc1	Looking
Boy	boy	k1gMnSc1	boy
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
basisty	basista	k1gMnPc7	basista
Pinem	pin	k1gInSc7	pin
Palladinem	Palladin	k1gInSc7	Palladin
a	a	k8xC	a
Gregem	Greg	k1gInSc7	Greg
Lakem	lak	k1gInSc7	lak
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
sbírky	sbírka	k1gFnSc2	sbírka
singlů	singl	k1gInPc2	singl
Then	Then	k1gInSc4	Then
and	and	k?	and
Now	Now	k1gFnSc2	Now
a	a	k8xC	a
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
18	[number]	k4	18
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
na	na	k7c6	na
CD	CD	kA	CD
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
Encore	Encor	k1gInSc5	Encor
Series	Series	k1gInSc4	Series
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
byla	být	k5eAaImAgFnS	být
také	také	k6eAd1	také
hlavní	hlavní	k2eAgFnSc7d1	hlavní
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c4	na
Isle	Isl	k1gMnPc4	Isl
of	of	k?	of
Wight	Wight	k2eAgInSc1d1	Wight
Festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
také	také	k9	také
časopis	časopis	k1gInSc1	časopis
Rolling	Rolling	k1gInSc1	Rolling
Stone	ston	k1gInSc5	ston
zařadil	zařadit	k5eAaPmAgMnS	zařadit
The	The	k1gFnPc2	The
Who	Who	k1gFnSc2	Who
na	na	k7c4	na
29	[number]	k4	29
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
seznam	seznam	k1gInSc4	seznam
100	[number]	k4	100
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
umělců	umělec	k1gMnPc2	umělec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Who	Who	k1gMnPc1	Who
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
na	na	k7c4	na
jaře	jaro	k6eAd1	jaro
2005	[number]	k4	2005
vyjde	vyjít	k5eAaPmIp3nS	vyjít
jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
po	po	k7c6	po
23	[number]	k4	23
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
předběžně	předběžně	k6eAd1	předběžně
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
WHO	WHO	kA	WHO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Townshend	Townshend	k1gInSc1	Townshend
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
na	na	k7c6	na
albu	album	k1gNnSc6	album
a	a	k8xC	a
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
blogu	blog	k1gInSc6	blog
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
novelu	novela	k1gFnSc4	novela
The	The	k1gMnSc1	The
Boy	boy	k1gMnSc1	boy
Who	Who	k1gMnSc1	Who
Heard	Heard	k1gMnSc1	Heard
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgNnP	vyvinout
mini-opera	minipero	k1gNnPc1	mini-opero
Wire	Wir	k1gMnSc2	Wir
&	&	k?	&
Glass	Glass	k1gInSc1	Glass
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jádro	jádro	k1gNnSc4	jádro
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
řádné	řádný	k2eAgFnSc2d1	řádná
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Townshend	Townshend	k1gInSc1	Townshend
představil	představit	k5eAaPmAgInS	představit
na	na	k7c4	na
Vassar	Vassar	k1gInSc4	Vassar
College	Colleg	k1gFnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2005	[number]	k4	2005
hráli	hrát	k5eAaImAgMnP	hrát
The	The	k1gMnSc4	The
Who	Who	k1gMnSc4	Who
na	na	k7c6	na
londýnské	londýnský	k2eAgFnSc6d1	londýnská
části	část	k1gFnSc6	část
koncertu	koncert	k1gInSc2	koncert
Live	Liv	k1gInSc2	Liv
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Who	Who	k1gMnPc7	Who
byli	být	k5eAaImAgMnP	být
také	také	k9	také
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
Britské	britský	k2eAgFnSc2d1	britská
hudební	hudební	k2eAgFnSc2d1	hudební
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
(	(	kIx(	(
<g/>
UK	UK	kA	UK
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
of	of	k?	of
Fame	Fame	k1gInSc1	Fame
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byli	být	k5eAaImAgMnP	být
The	The	k1gMnSc4	The
Who	Who	k1gMnSc4	Who
prvními	první	k4xOgMnPc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
Lifetime	Lifetim	k1gInSc5	Lifetim
Achievement	Achievement	k1gInSc1	Achievement
Award	Award	k1gInSc1	Award
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Vodafone	Vodafon	k1gInSc5	Vodafon
music	music	k1gMnSc1	music
awards	awardsit	k5eAaPmRp2nS	awardsit
<g/>
.	.	kIx.	.
</s>
<s>
Endless	Endless	k6eAd1	Endless
Wire	Wire	k1gNnSc1	Wire
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
první	první	k4xOgNnSc4	první
řádné	řádný	k2eAgNnSc4d1	řádné
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
od	od	k7c2	od
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hard	Hard	k1gInSc1	Hard
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
první	první	k4xOgMnSc1	první
mini-operu	miniprat	k5eAaPmIp1nS	mini-oprat
od	od	k7c2	od
"	"	kIx"	"
<g/>
Rael	Raela	k1gFnPc2	Raela
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Who	Who	k1gMnSc1	Who
Sell	Sell	k1gMnSc1	Sell
Out	Out	k1gMnSc1	Out
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahráli	zahrát	k5eAaPmAgMnP	zahrát
The	The	k1gMnPc1	The
Who	Who	k1gFnSc3	Who
část	část	k1gFnSc4	část
mini-opery	minipera	k1gFnSc2	mini-opera
a	a	k8xC	a
několik	několik	k4yIc4	několik
písní	píseň	k1gFnPc2	píseň
živě	živě	k6eAd1	živě
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
BBC	BBC	kA	BBC
Electric	Electrice	k1gFnPc2	Electrice
Proms	Promsa	k1gFnPc2	Promsa
v	v	k7c6	v
Roundhouse	Roundhous	k1gInSc6	Roundhous
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
alba	album	k1gNnSc2	album
(	(	kIx(	(
<g/>
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
podporu	podpora	k1gFnSc4	podpora
<g/>
)	)	kIx)	)
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
na	na	k7c6	na
CD	CD	kA	CD
a	a	k8xC	a
DVD	DVD	kA	DVD
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
Encore	Encor	k1gInSc5	Encor
Series	Seriesa	k1gFnPc2	Seriesa
2006	[number]	k4	2006
and	and	k?	and
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Starkeymu	Starkeym	k1gInSc3	Starkeym
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnut	k2eAgNnSc1d1	nabídnuto
oficiální	oficiální	k2eAgNnSc1d1	oficiální
členství	členství	k1gNnSc1	členství
v	v	k7c6	v
Oasis	Oasis	k1gFnSc6	Oasis
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2006	[number]	k4	2006
a	a	k8xC	a
v	v	k7c6	v
The	The	k1gFnSc6	The
Who	Who	k1gFnSc2	Who
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
raději	rád	k6eAd2	rád
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
kapely	kapela	k1gFnPc4	kapela
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
byli	být	k5eAaImAgMnP	být
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
programu	program	k1gInSc6	program
festivalu	festival	k1gInSc2	festival
Glastonbury	Glastonbura	k1gFnSc2	Glastonbura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
dokumentární	dokumentární	k2eAgInSc1d1	dokumentární
film	film	k1gInSc1	film
Amazing	Amazing	k1gInSc4	Amazing
Journey	Journea	k1gFnSc2	Journea
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
záběry	záběr	k1gInPc4	záběr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgInP	být
v	v	k7c6	v
dřívějších	dřívější	k2eAgInPc6d1	dřívější
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
filmového	filmový	k2eAgInSc2d1	filmový
záznamu	záznam	k1gInSc2	záznam
vystoupení	vystoupení	k1gNnSc2	vystoupení
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Leeds	Leeds	k1gInSc4	Leeds
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
a	a	k8xC	a
vystoupení	vystoupení	k1gNnSc4	vystoupení
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
The	The	k1gMnPc2	The
High	High	k1gInSc1	High
Numbers	Numbersa	k1gFnPc2	Numbersa
v	v	k7c4	v
Railway	Railwaa	k1gFnPc4	Railwaa
Hotelu	hotel	k1gInSc2	hotel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Amazing	Amazing	k1gInSc1	Amazing
Journey	Journea	k1gFnSc2	Journea
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Grammy	Gramm	k1gMnPc4	Gramm
Award	Awarda	k1gFnPc2	Awarda
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Who	Who	k1gMnSc1	Who
byli	být	k5eAaImAgMnP	být
vyznamenáni	vyznamenat	k5eAaPmNgMnP	vyznamenat
na	na	k7c6	na
VH1	VH1	k1gFnSc6	VH1
Rock	rock	k1gInSc1	rock
Honors	Honorsa	k1gFnPc2	Honorsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
koncertu	koncert	k1gInSc2	koncert
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
12	[number]	k4	12
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
následovalo	následovat	k5eAaImAgNnS	následovat
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
týden	týden	k1gInSc4	týden
vyšla	vyjít	k5eAaPmAgFnS	vyjít
best-of	bestf	k1gInSc4	best-of
kolekce	kolekce	k1gFnSc2	kolekce
12	[number]	k4	12
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
hru	hra	k1gFnSc4	hra
Rock	rock	k1gInSc1	rock
Band	banda	k1gFnPc2	banda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
E3	E3	k1gFnSc2	E3
Media	medium	k1gNnSc2	medium
and	and	k?	and
Business	business	k1gInSc1	business
Summit	summit	k1gInSc1	summit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
na	na	k7c4	na
Rock	rock	k1gInSc4	rock
Band	band	k1gInSc4	band
party	parta	k1gFnSc2	parta
v	v	k7c4	v
Orpheum	Orpheum	k1gNnSc4	Orpheum
Theater	Theatra	k1gFnPc2	Theatra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
vyjeli	vyjet	k5eAaPmAgMnP	vyjet
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nějž	jenž	k3xRgInSc2	jenž
navštívili	navštívit	k5eAaPmAgMnP	navštívit
čtyři	čtyři	k4xCgNnPc1	čtyři
japonská	japonský	k2eAgNnPc1d1	Japonské
a	a	k8xC	a
devět	devět	k4xCc4	devět
severoamerických	severoamerický	k2eAgNnPc2d1	severoamerické
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
byli	být	k5eAaImAgMnP	být
The	The	k1gMnPc1	The
Who	Who	k1gFnSc2	Who
oceněni	oceněn	k2eAgMnPc1d1	oceněn
na	na	k7c4	na
Kennedy	Kenned	k1gMnPc4	Kenned
Center	centrum	k1gNnPc2	centrum
Honors	Honorsa	k1gFnPc2	Honorsa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
byli	být	k5eAaImAgMnP	být
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
Townshend	Townshenda	k1gFnPc2	Townshenda
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
muzikálu	muzikál	k1gInSc6	muzikál
nazvaném	nazvaný	k2eAgInSc6d1	nazvaný
Floss	Floss	k1gInSc1	Floss
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
stárnoucím	stárnoucí	k2eAgMnSc6d1	stárnoucí
rockerovi	rocker	k1gMnSc6	rocker
známém	známý	k1gMnSc6	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Walter	Walter	k1gMnSc1	Walter
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
skladby	skladba	k1gFnPc4	skladba
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
muzikálu	muzikál	k1gInSc2	muzikál
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
na	na	k7c6	na
budoucím	budoucí	k2eAgNnSc6d1	budoucí
albu	album	k1gNnSc6	album
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Who	Who	k1gFnSc1	Who
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
dělala	dělat	k5eAaImAgFnS	dělat
vlastní	vlastní	k2eAgInPc4d1	vlastní
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
film	film	k1gInSc4	film
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
byl	být	k5eAaImAgMnS	být
Tommy	Tomma	k1gFnPc4	Tomma
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
inspirovaný	inspirovaný	k2eAgMnSc1d1	inspirovaný
jejich	jejich	k3xOp3gFnSc7	jejich
stejnojmennou	stejnojmenný	k2eAgFnSc7d1	stejnojmenná
rockovou	rockový	k2eAgFnSc7d1	rocková
operou	opera	k1gFnSc7	opera
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
</s>
<s>
Objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k4c1	mnoho
hvězd	hvězda	k1gFnPc2	hvězda
jako	jako	k8xC	jako
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Oliver	Oliver	k1gMnSc1	Oliver
Reed	Reed	k1gMnSc1	Reed
a	a	k8xC	a
Ann	Ann	k1gMnSc1	Ann
Margret	Margret	k1gMnSc1	Margret
<g/>
.	.	kIx.	.
</s>
<s>
Daltrey	Daltre	k1gMnPc4	Daltre
hrál	hrát	k5eAaImAgInS	hrát
"	"	kIx"	"
<g/>
hluchého	hluchý	k2eAgMnSc4d1	hluchý
<g/>
,	,	kIx,	,
němého	němý	k2eAgMnSc4d1	němý
a	a	k8xC	a
slepého	slepý	k2eAgMnSc4d1	slepý
chlapce	chlapec	k1gMnSc4	chlapec
<g/>
"	"	kIx"	"
a	a	k8xC	a
Keith	Keith	k1gMnSc1	Keith
Moon	Moon	k1gMnSc1	Moon
zlého	zlý	k2eAgMnSc2d1	zlý
pedofilního	pedofilní	k2eAgMnSc2d1	pedofilní
strýčka	strýček	k1gMnSc2	strýček
Ernieho	Ernie	k1gMnSc2	Ernie
<g/>
.	.	kIx.	.
</s>
<s>
Quadrophenia	Quadrophenium	k1gNnPc1	Quadrophenium
byla	být	k5eAaImAgNnP	být
jejich	jejich	k3xOp3gInSc4	jejich
druhým	druhý	k4xOgInSc7	druhý
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
inspirovaným	inspirovaný	k2eAgInSc7d1	inspirovaný
jejich	jejich	k3xOp3gFnSc7	jejich
rockovou	rockový	k2eAgFnSc7d1	rocková
oprerou	oprera	k1gFnSc7	oprera
téhož	týž	k3xTgNnSc2	týž
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
protagonistou	protagonista	k1gMnSc7	protagonista
je	být	k5eAaImIp3nS	být
Jimmy	Jimmo	k1gNnPc7	Jimmo
(	(	kIx(	(
<g/>
Phil	Phil	k1gInSc1	Phil
Daniels	Daniels	k1gInSc1	Daniels
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
Mod	Mod	k1gFnSc4	Mod
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
amfetaminy	amfetamin	k1gInPc4	amfetamin
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
má	mít	k5eAaImIp3nS	mít
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
také	také	k6eAd1	také
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
končí	končit	k5eAaImIp3nS	končit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
opouští	opouštět	k5eAaImIp3nS	opouštět
subkulturu	subkultura	k1gFnSc4	subkultura
Mods	Modsa	k1gFnPc2	Modsa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
film	film	k1gInSc1	film
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nejlepší	dobrý	k2eAgNnPc4d3	nejlepší
živá	živý	k2eAgNnPc4d1	živé
vystoupení	vystoupení	k1gNnPc4	vystoupení
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
film	film	k1gInSc1	film
The	The	k1gFnSc2	The
Who	Who	k1gFnPc2	Who
<g/>
,	,	kIx,	,
McVicar	McVicara	k1gFnPc2	McVicara
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
skutečných	skutečný	k2eAgFnPc6d1	skutečná
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gInSc1	Roger
Daltrey	Daltrea	k1gFnSc2	Daltrea
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
hraje	hrát	k5eAaImIp3nS	hrát
bankovního	bankovní	k2eAgMnSc4d1	bankovní
lupiče	lupič	k1gMnSc4	lupič
Johna	John	k1gMnSc4	John
McVicara	McVicar	k1gMnSc4	McVicar
a	a	k8xC	a
Adam	Adam	k1gMnSc1	Adam
Faith	Faitha	k1gFnPc2	Faitha
jeho	jeho	k3xOp3gMnSc2	jeho
kamaráda	kamarád	k1gMnSc2	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Soundtrack	soundtrack	k1gInSc1	soundtrack
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
Daltreyho	Daltreyha	k1gFnSc5	Daltreyha
sólovým	sólový	k2eAgNnSc7d1	sólové
albem	album	k1gNnSc7	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
členové	člen	k1gMnPc1	člen
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
.	.	kIx.	.
</s>
<s>
Současní	současný	k2eAgMnPc1d1	současný
členové	člen	k1gMnPc1	člen
Roger	Rogra	k1gFnPc2	Rogra
Daltrey	Daltrea	k1gFnSc2	Daltrea
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnSc1	perkuse
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
Pete	Pete	k1gFnSc1	Pete
Townshend	Townshend	k1gMnSc1	Townshend
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
syntezátory	syntezátor	k1gInPc1	syntezátor
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
současnost	současnost	k1gFnSc1	současnost
<g/>
)	)	kIx)	)
Dřívější	dřívější	k2eAgMnPc1d1	dřívější
členové	člen	k1gMnPc1	člen
John	John	k1gMnSc1	John
Entwistle	Entwistle	k1gMnSc1	Entwistle
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgInSc4d1	lesní
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
klávesy	klávesa	k1gFnPc4	klávesa
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Doug	Doug	k1gInSc1	Doug
Sandom	Sandom	k1gInSc1	Sandom
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
Keith	Keith	k1gInSc1	Keith
Moon	Moon	k1gNnSc1	Moon
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
-	-	kIx~	-
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Kenney	Kennea	k1gFnSc2	Kennea
Jones	Jones	k1gMnSc1	Jones
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Studiová	studiový	k2eAgFnSc1d1	studiová
alba	alba	k1gFnSc1	alba
My	my	k3xPp1nPc1	my
Generation	Generation	k1gInSc1	Generation
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
A	a	k8xC	a
Quick	Quick	k1gMnSc1	Quick
One	One	k1gMnSc1	One
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
Sell	Sell	k1gMnSc1	Sell
Out	Out	k1gMnSc1	Out
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Tommy	Tomma	k1gFnSc2	Tomma
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Who	Who	k1gFnSc2	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Next	Nexta	k1gFnPc2	Nexta
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Quadrophenia	Quadrophenium	k1gNnSc2	Quadrophenium
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
<g />
.	.	kIx.	.
</s>
<s>
Who	Who	k?	Who
by	by	kYmCp3nS	by
Numbers	Numbers	k1gInSc1	Numbers
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Who	Who	k1gFnSc1	Who
Are	ar	k1gInSc5	ar
You	You	k1gMnSc6	You
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Face	Fac	k1gInSc2	Fac
Dances	Dances	k1gInSc1	Dances
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Hard	Harda	k1gFnPc2	Harda
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Endless	Endless	k1gInSc1	Endless
Wire	Wir	k1gInSc2	Wir
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gMnSc2	The
Who	Who	k1gMnSc2	Who
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
The	The	k1gFnSc1	The
Who	Who	k1gFnSc1	Who
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
The	The	k1gFnSc6	The
Who	Who	k1gMnSc2	Who
The	The	k1gMnSc2	The
Who	Who	k1gMnSc2	Who
Information	Information	k1gInSc1	Information
Center	centrum	k1gNnPc2	centrum
Official	Official	k1gInSc1	Official
Website	Websit	k1gInSc5	Websit
</s>
