<s>
Černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
nebo	nebo	k8xC	nebo
dávivý	dávivý	k2eAgInSc1d1	dávivý
kašel	kašel	k1gInSc1	kašel
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
pertussis	pertussis	k1gInSc1	pertussis
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
někdy	někdy	k6eAd1	někdy
pertuse	pertuse	k1gFnSc1	pertuse
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
bakterie	bakterie	k1gFnSc1	bakterie
Bordetella	Bordetella	k1gFnSc1	Bordetella
pertussis	pertussis	k1gFnSc1	pertussis
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
kapénkovou	kapénkový	k2eAgFnSc7d1	kapénková
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
dráždivý	dráždivý	k2eAgInSc1d1	dráždivý
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
dávivý	dávivý	k2eAgMnSc1d1	dávivý
a	a	k8xC	a
při	při	k7c6	při
nadechování	nadechování	k1gNnSc6	nadechování
může	moct	k5eAaImIp3nS	moct
znít	znít	k5eAaImF	znít
jako	jako	k8xC	jako
kokrhání	kokrhání	k1gNnSc4	kokrhání
kohouta	kohout	k1gMnSc2	kohout
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
vykašlávání	vykašlávání	k1gNnSc1	vykašlávání
hlenu	hlen	k1gInSc2	hlen
<g/>
.	.	kIx.	.
</s>
<s>
Tělesná	tělesný	k2eAgFnSc1d1	tělesná
teplota	teplota	k1gFnSc1	teplota
bývá	bývat	k5eAaImIp3nS	bývat
značně	značně	k6eAd1	značně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
až	až	k9	až
kolem	kolem	k7c2	kolem
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Onemocnění	onemocnění	k1gNnSc1	onemocnění
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
podle	podle	k7c2	podle
příznaků	příznak	k1gInPc2	příznak
kašle	kašel	k1gInSc2	kašel
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
kultivace	kultivace	k1gFnSc2	kultivace
vykašlaného	vykašlaný	k2eAgInSc2d1	vykašlaný
hlenu	hlen	k1gInSc2	hlen
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
obsažena	obsažen	k2eAgFnSc1d1	obsažena
bakterie	bakterie	k1gFnSc1	bakterie
Bordetella	Bordetella	k1gFnSc1	Bordetella
pertussis	pertussis	k1gFnSc1	pertussis
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
širokospektrální	širokospektrální	k2eAgNnSc4d1	širokospektrální
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
černému	černý	k2eAgInSc3d1	černý
kašli	kašel	k1gInSc3	kašel
povinné	povinný	k2eAgFnSc2d1	povinná
a	a	k8xC	a
významně	významně	k6eAd1	významně
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
jeho	jeho	k3xOp3gInSc2	jeho
výskytu	výskyt	k1gInSc2	výskyt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
probíhá	probíhat	k5eAaImIp3nS	probíhat
celoplošné	celoplošný	k2eAgNnSc1d1	celoplošné
očkování	očkování	k1gNnSc1	očkování
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Očkovací	očkovací	k2eAgFnPc1d1	očkovací
látky	látka	k1gFnPc1	látka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pouze	pouze	k6eAd1	pouze
složky	složka	k1gFnSc2	složka
b.	b.	k?	b.
černého	černý	k2eAgInSc2d1	černý
kašle	kašel	k1gInSc2	kašel
nutné	nutný	k2eAgNnSc1d1	nutné
pro	pro	k7c4	pro
imunizaci	imunizace	k1gFnSc4	imunizace
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
snášeny	snášen	k2eAgFnPc1d1	snášena
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	s	k7c7	s
<g/>
:	:	kIx,	:
hexavakcína	hexavakcína	k1gFnSc1	hexavakcína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určen	k2eAgFnSc1d1	určena
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
nejmenší	malý	k2eAgMnPc4d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
dávkách	dávka	k1gFnPc6	dávka
a	a	k8xC	a
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
šesti	šest	k4xCc3	šest
nemocem	nemoc	k1gFnPc3	nemoc
(	(	kIx(	(
<g/>
záškrt	záškrt	k1gInSc1	záškrt
<g/>
,	,	kIx,	,
tetanus	tetanus	k1gInSc1	tetanus
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
<g/>
,	,	kIx,	,
hepatitida	hepatitida	k1gFnSc1	hepatitida
B	B	kA	B
<g/>
,	,	kIx,	,
dětská	dětský	k2eAgFnSc1d1	dětská
obrna	obrna	k1gFnSc1	obrna
a	a	k8xC	a
proti	proti	k7c3	proti
bakterii	bakterie	k1gFnSc3	bakterie
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenzae	k6eAd1	influenzae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dostupná	dostupný	k2eAgFnSc1d1	dostupná
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
Infanrix	Infanrix	k1gInSc1	Infanrix
Hexa	Hexus	k1gMnSc2	Hexus
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
GlaxoSmithKline	GlaxoSmithKlin	k1gInSc5	GlaxoSmithKlin
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
Hexacima	Hexacima	k1gFnSc1	Hexacima
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Sanofi	Sanof	k1gFnSc2	Sanof
Pasteur	Pasteur	k1gMnSc1	Pasteur
<g/>
.	.	kIx.	.
trivakcína	trivakcína	k1gFnSc1	trivakcína
<g/>
,	,	kIx,	,
chránící	chránící	k2eAgFnSc1d1	chránící
proti	proti	k7c3	proti
černému	černý	k2eAgInSc3d1	černý
kašli	kašel	k1gInSc3	kašel
<g/>
,	,	kIx,	,
záškrtu	záškrt	k1gInSc3	záškrt
a	a	k8xC	a
tetanu	tetan	k1gInSc3	tetan
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
těchto	tento	k3xDgNnPc2	tento
opatření	opatření	k1gNnPc2	opatření
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
četnost	četnost	k1gFnSc4	četnost
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
přibývá	přibývat	k5eAaImIp3nS	přibývat
nemocných	mocný	k2eNgInPc2d1	mocný
černým	černý	k2eAgInSc7d1	černý
kašlem	kašel	k1gInSc7	kašel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
10	[number]	k4	10
měsíců	měsíc	k1gInPc2	měsíc
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
asi	asi	k9	asi
880	[number]	k4	880
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podobně	podobně	k6eAd1	podobně
vysoký	vysoký	k2eAgInSc1d1	vysoký
výskyt	výskyt	k1gInSc1	výskyt
byl	být	k5eAaImAgInS	být
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Černý	černý	k2eAgInSc4d1	černý
kašel	kašel	k1gInSc4	kašel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Vakciny	Vakcin	k1gInPc1	Vakcin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
proti	proti	k7c3	proti
onemocněním	onemocnění	k1gNnSc7	onemocnění
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
ČR	ČR	kA	ČR
Černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
-	-	kIx~	-
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
černém	černý	k2eAgInSc6d1	černý
kašli	kašel	k1gInSc6	kašel
</s>
