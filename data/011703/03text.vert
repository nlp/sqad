<p>
<s>
Luk	luk	k1gInSc1	luk
je	být	k5eAaImIp3nS	být
mechanická	mechanický	k2eAgFnSc1d1	mechanická
střelná	střelný	k2eAgFnSc1d1	střelná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
pružného	pružný	k2eAgNnSc2d1	pružné
lučiště	lučiště	k1gNnSc2	lučiště
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
dřevěného	dřevěný	k2eAgInSc2d1	dřevěný
<g/>
,	,	kIx,	,
a	a	k8xC	a
nepružné	pružný	k2eNgFnPc1d1	nepružná
tětivy	tětiva	k1gFnPc1	tětiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střílí	střílet	k5eAaImIp3nS	střílet
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
dřevěnými	dřevěný	k2eAgFnPc7d1	dřevěná
nebo	nebo	k8xC	nebo
bambusovými	bambusový	k2eAgFnPc7d1	bambusová
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
běžnějšími	běžný	k2eAgInPc7d2	běžnější
karbonovými	karbonový	k2eAgInPc7d1	karbonový
nebo	nebo	k8xC	nebo
duralovými	duralový	k2eAgInPc7d1	duralový
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
opeření	opeření	k1gNnSc2	opeření
<g/>
,	,	kIx,	,
zlepšující	zlepšující	k2eAgFnSc1d1	zlepšující
výrazně	výrazně	k6eAd1	výrazně
jejich	jejich	k3xOp3gFnPc4	jejich
letové	letový	k2eAgFnPc4d1	letová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc1d1	jiný
používaný	používaný	k2eAgInSc1d1	používaný
výraz	výraz	k1gInSc1	výraz
je	on	k3xPp3gNnPc4	on
kormidla	kormidlo	k1gNnPc4	kormidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
ostrý	ostrý	k2eAgInSc1d1	ostrý
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nejčastěji	často	k6eAd3	často
kovový	kovový	k2eAgInSc1d1	kovový
hrot	hrot	k1gInSc1	hrot
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
se	se	k3xPyFc4	se
ke	k	k7c3	k
zpevnění	zpevnění	k1gNnSc3	zpevnění
konce	konec	k1gInSc2	konec
šípu	šíp	k1gInSc2	šíp
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
opálení	opálení	k1gNnSc1	opálení
v	v	k7c6	v
ohni	oheň	k1gInSc6	oheň
nebo	nebo	k8xC	nebo
kamenné	kamenný	k2eAgInPc1d1	kamenný
hroty	hrot	k1gInPc1	hrot
(	(	kIx(	(
<g/>
pazourek	pazourek	k1gInSc1	pazourek
<g/>
,	,	kIx,	,
obsidián	obsidián	k1gInSc1	obsidián
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dřeva	dřevo	k1gNnSc2	dřevo
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
těla	tělo	k1gNnSc2	tělo
šípů	šíp	k1gInPc2	šíp
používají	používat	k5eAaImIp3nP	používat
duralové	duralový	k2eAgFnPc1d1	duralová
trubky	trubka	k1gFnPc1	trubka
<g/>
,	,	kIx,	,
trubky	trubka	k1gFnPc1	trubka
z	z	k7c2	z
karbonového	karbonový	k2eAgInSc2d1	karbonový
laminátu	laminát	k1gInSc2	laminát
a	a	k8xC	a
nebo	nebo	k8xC	nebo
kombinace	kombinace	k1gFnSc1	kombinace
těchto	tento	k3xDgInPc2	tento
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střelba	střelba	k1gFnSc1	střelba
z	z	k7c2	z
luku	luk	k1gInSc2	luk
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luk	luk	k1gInSc1	luk
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
přírodními	přírodní	k2eAgInPc7d1	přírodní
národy	národ	k1gInPc7	národ
dodnes	dodnes	k6eAd1	dodnes
používán	používán	k2eAgMnSc1d1	používán
k	k	k7c3	k
rozdělávání	rozdělávání	k1gNnSc3	rozdělávání
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
pohánění	pohánění	k1gNnSc4	pohánění
vrtáku	vrták	k1gInSc2	vrták
při	při	k7c6	při
vrtání	vrtání	k1gNnSc6	vrtání
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
pohonu	pohon	k1gInSc2	pohon
obrobku	obrobek	k1gInSc2	obrobek
při	při	k7c6	při
soustružení	soustružení	k1gNnSc6	soustružení
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
traxlování	traxlování	k1gNnSc2	traxlování
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
strunný	strunný	k2eAgInSc4d1	strunný
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
rozhodnete	rozhodnout	k5eAaPmIp2nP	rozhodnout
si	se	k3xPyFc3	se
pořídit	pořídit	k5eAaPmF	pořídit
sportovní	sportovní	k2eAgInSc4d1	sportovní
luk	luk	k1gInSc4	luk
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
byste	by	kYmCp2nP	by
být	být	k5eAaImF	být
schopni	schopen	k2eAgMnPc1d1	schopen
ho	on	k3xPp3gNnSc4	on
11	[number]	k4	11
<g/>
x	x	k?	x
naplno	naplno	k6eAd1	naplno
natáhnout	natáhnout	k5eAaPmF	natáhnout
se	s	k7c7	s
sedmisekundovou	sedmisekundový	k2eAgFnSc7d1	sedmisekundový
výdrží	výdrž	k1gFnSc7	výdrž
v	v	k7c6	v
napětí	napětí	k1gNnSc6	napětí
a	a	k8xC	a
dvousekundovým	dvousekundový	k2eAgInSc7d1	dvousekundový
odpočinkem	odpočinek	k1gInSc7	odpočinek
při	při	k7c6	při
uvolnění	uvolnění	k1gNnSc6	uvolnění
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
nedokážete	dokázat	k5eNaPmIp2nP	dokázat
<g/>
,	,	kIx,	,
zkuste	zkusit	k5eAaPmRp2nP	zkusit
slabší	slabý	k2eAgInSc4d2	slabší
luk	luk	k1gInSc4	luk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
máte	mít	k5eAaImIp2nP	mít
rezervu	rezerva	k1gFnSc4	rezerva
<g/>
,	,	kIx,	,
můžete	moct	k5eAaImIp2nP	moct
zkusit	zkusit	k5eAaPmF	zkusit
silnější	silný	k2eAgFnPc1d2	silnější
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc1	smysl
se	se	k3xPyFc4	se
s	s	k7c7	s
nataženým	natažený	k2eAgInSc7d1	natažený
lukem	luk	k1gInSc7	luk
třást	třást	k5eAaImF	třást
<g/>
,	,	kIx,	,
nedokážete	dokázat	k5eNaPmIp2nP	dokázat
zamířit	zamířit	k5eAaPmF	zamířit
<g/>
.	.	kIx.	.
</s>
<s>
Nátahová	Nátahový	k2eAgFnSc1d1	Nátahový
síla	síla	k1gFnSc1	síla
moderních	moderní	k2eAgInPc2d1	moderní
luků	luk	k1gInPc2	luk
bývá	bývat	k5eAaImIp3nS	bývat
vyznačena	vyznačen	k2eAgFnSc1d1	vyznačena
na	na	k7c6	na
ramenech	rameno	k1gNnPc6	rameno
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tradičně	tradičně	k6eAd1	tradičně
v	v	k7c6	v
librách	libra	k1gFnPc6	libra
(	(	kIx(	(
<g/>
1	[number]	k4	1
Lb	Lb	k1gFnPc2	Lb
=	=	kIx~	=
0,45	[number]	k4	0,45
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
nátahu	nátah	k1gInSc2	nátah
pak	pak	k6eAd1	pak
v	v	k7c6	v
palcích	palec	k1gInPc6	palec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
luku	luk	k1gInSc2	luk
==	==	k?	==
</s>
</p>
<p>
<s>
Jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
luk	luk	k1gInSc1	luk
<g/>
:	:	kIx,	:
lučiště	lučiště	k1gNnSc1	lučiště
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
<g/>
,	,	kIx,	,
po	po	k7c6	po
naložení	naložení	k1gNnSc6	naložení
tětivy	tětiva	k1gFnSc2	tětiva
obloukovitě	obloukovitě	k6eAd1	obloukovitě
prohnutý	prohnutý	k2eAgInSc1d1	prohnutý
kus	kus	k1gInSc1	kus
dřeva	dřevo	k1gNnSc2	dřevo
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
bambus	bambus	k1gInSc1	bambus
<g/>
,	,	kIx,	,
šlachy	šlacha	k1gFnPc1	šlacha
<g/>
,	,	kIx,	,
rohovina	rohovina	k1gFnSc1	rohovina
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
laminát	laminát	k1gInSc1	laminát
a	a	k8xC	a
plasty	plast	k1gInPc1	plast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
atlatlu	atlatl	k1gInSc6	atlatl
nejstarší	starý	k2eAgMnSc1d3	nejstarší
a	a	k8xC	a
historicky	historicky	k6eAd1	historicky
i	i	k9	i
geograficky	geograficky	k6eAd1	geograficky
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
dalekonosná	dalekonosný	k2eAgFnSc1d1	dalekonosná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
luky	luk	k1gInPc4	luk
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
a	a	k8xC	a
krátké	krátký	k2eAgFnSc6d1	krátká
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
materiálu	materiál	k1gInSc2	materiál
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
má	mít	k5eAaImIp3nS	mít
luk	luk	k1gInSc4	luk
na	na	k7c6	na
průřezu	průřez	k1gInSc6	průřez
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
kruhový	kruhový	k2eAgInSc4d1	kruhový
<g/>
,	,	kIx,	,
eliptický	eliptický	k2eAgInSc4d1	eliptický
<g/>
,	,	kIx,	,
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
D	D	kA	D
<g/>
,	,	kIx,	,
trojúhelníkový	trojúhelníkový	k2eAgInSc1d1	trojúhelníkový
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
zploštělý	zploštělý	k2eAgInSc1d1	zploštělý
<g/>
.	.	kIx.	.
</s>
<s>
Materiálem	materiál	k1gInSc7	materiál
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nejčastěji	často	k6eAd3	často
dřevo	dřevo	k1gNnSc1	dřevo
tisu	tis	k1gInSc2	tis
<g/>
,	,	kIx,	,
či	či	k8xC	či
jilmu	jilma	k1gFnSc4	jilma
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
bambus	bambus	k1gInSc4	bambus
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
palmové	palmový	k2eAgNnSc4d1	palmové
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
maklura	maklura	k1gFnSc1	maklura
oranžová	oranžový	k2eAgFnSc1d1	oranžová
(	(	kIx(	(
<g/>
Osage	Osage	k1gFnSc1	Osage
Orange	Orang	k1gFnSc2	Orang
<g/>
,	,	kIx,	,
příbuzné	příbuzný	k2eAgFnSc3d1	příbuzná
moruši	moruše	k1gFnSc3	moruše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hickory	hickor	k1gInPc1	hickor
(	(	kIx(	(
<g/>
ořechovec	ořechovec	k1gInSc1	ořechovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tis	tis	k1gInSc1	tis
<g/>
,	,	kIx,	,
akát	akát	k1gInSc1	akát
<g/>
,	,	kIx,	,
jasan	jasan	k1gInSc1	jasan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
luk	luk	k1gInSc1	luk
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
longbow	longbow	k?	longbow
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
dosti	dosti	k6eAd1	dosti
nepřesně	přesně	k6eNd1	přesně
vymezený	vymezený	k2eAgInSc1d1	vymezený
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
přibližně	přibližně	k6eAd1	přibližně
lze	lze	k6eAd1	lze
takto	takto	k6eAd1	takto
nazvat	nazvat	k5eAaPmF	nazvat
luk	luk	k1gInSc4	luk
délky	délka	k1gFnSc2	délka
lidské	lidský	k2eAgFnSc2d1	lidská
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
nazýván	nazývat	k5eAaImNgInS	nazývat
zejména	zejména	k9	zejména
tradiční	tradiční	k2eAgInSc1d1	tradiční
anglický	anglický	k2eAgInSc1d1	anglický
longbow	longbow	k?	longbow
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc2	jehož
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
ramen	rameno	k1gNnPc2	rameno
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
stabilizační	stabilizační	k2eAgInSc1d1	stabilizační
prvek	prvek	k1gInSc1	prvek
pro	pro	k7c4	pro
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
má	mít	k5eAaImIp3nS	mít
menší	malý	k2eAgInSc1d2	menší
"	"	kIx"	"
<g/>
stacking	stacking	k1gInSc1	stacking
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnSc1d2	menší
strmost	strmost	k1gFnSc1	strmost
náběhu	náběh	k1gInSc2	náběh
síly	síla	k1gFnSc2	síla
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
nátahu	nátah	k1gInSc2	nátah
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
menšího	malý	k2eAgInSc2d2	menší
úhlu	úhel	k1gInSc2	úhel
konců	konec	k1gInPc2	konec
tětivy	tětiva	k1gFnSc2	tětiva
a	a	k8xC	a
ramen	rameno	k1gNnPc2	rameno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ramena	rameno	k1gNnPc1	rameno
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
luku	luk	k1gInSc2	luk
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
krátkému	krátký	k2eAgInSc3d1	krátký
luku	luk	k1gInSc3	luk
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
stresována	stresován	k2eAgFnSc1d1	stresována
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyrobit	vyrobit	k5eAaPmF	vyrobit
efektivní	efektivní	k2eAgInSc4d1	efektivní
<g/>
,	,	kIx,	,
přesný	přesný	k2eAgInSc4d1	přesný
a	a	k8xC	a
trvanlivý	trvanlivý	k2eAgInSc4d1	trvanlivý
luk	luk	k1gInSc4	luk
<g/>
,	,	kIx,	,
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
délkou	délka	k1gFnSc7	délka
nátahu	nátah	k1gInSc2	nátah
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
selfbow	selfbow	k?	selfbow
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
rychleji	rychle	k6eAd2	rychle
a	a	k8xC	a
podstatně	podstatně	k6eAd1	podstatně
jednodušeji	jednoduše	k6eAd2	jednoduše
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zajímavost	zajímavost	k1gFnSc4	zajímavost
<g/>
:	:	kIx,	:
anglický	anglický	k2eAgInSc1d1	anglický
středověký	středověký	k2eAgInSc1d1	středověký
válečný	válečný	k2eAgInSc1d1	válečný
luk	luk	k1gInSc1	luk
(	(	kIx(	(
<g/>
warbow	warbow	k?	warbow
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
sílu	síla	k1gFnSc4	síla
120-170	[number]	k4	120-170
liber	libra	k1gFnPc2	libra
v	v	k7c6	v
nátahu	nátah	k1gInSc6	nátah
30-32	[number]	k4	30-32
palců	palec	k1gInPc2	palec
<g/>
,	,	kIx,	,
při	při	k7c6	při
délce	délka	k1gFnSc6	délka
luku	luk	k1gInSc2	luk
76-82	[number]	k4	76-82
palců	palec	k1gInPc2	palec
<g/>
!	!	kIx.	!
</s>
<s>
Vyrobit	vyrobit	k5eAaPmF	vyrobit
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
takto	takto	k6eAd1	takto
silných	silný	k2eAgFnPc2d1	silná
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
spolehlivých	spolehlivý	k2eAgInPc2d1	spolehlivý
luků	luk	k1gInPc2	luk
pro	pro	k7c4	pro
masy	masa	k1gFnPc4	masa
vojáků	voják	k1gMnPc2	voják
bylo	být	k5eAaImAgNnS	být
nejsnadnější	snadný	k2eAgInSc4d3	nejsnazší
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
tisového	tisový	k2eAgNnSc2d1	Tisové
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Recurve	Recurvat	k5eAaPmIp3nS	Recurvat
luk	louka	k1gFnPc2	louka
<g/>
:	:	kIx,	:
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
prohnutím	prohnutí	k1gNnSc7	prohnutí
konců	konec	k1gInPc2	konec
ramen	rameno	k1gNnPc2	rameno
dopředu	dopříst	k5eAaPmIp1nS	dopříst
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
terči	terč	k1gFnSc3	terč
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
kladka	kladka	k1gFnSc1	kladka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nerovnoměrný	rovnoměrný	k2eNgInSc4d1	nerovnoměrný
nárůst	nárůst	k1gInSc4	nárůst
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
nátahu	nátah	k1gInSc2	nátah
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
reflexní	reflexní	k2eAgNnSc1d1	reflexní
(	(	kIx(	(
<g/>
zvratný	zvratný	k2eAgMnSc1d1	zvratný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celá	celý	k2eAgNnPc1d1	celé
ramena	rameno	k1gNnPc1	rameno
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
klidovém	klidový	k2eAgInSc6d1	klidový
stavu	stav	k1gInSc6	stav
bez	bez	k7c2	bez
napnuté	napnutý	k2eAgFnSc2d1	napnutá
tětivy	tětiva	k1gFnSc2	tětiva
prohnuta	prohnout	k5eAaPmNgFnS	prohnout
vpřed	vpřed	k6eAd1	vpřed
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
terči	terč	k1gInSc3	terč
<g/>
,	,	kIx,	,
tím	ten	k3xDgInSc7	ten
je	být	k5eAaImIp3nS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
vysokého	vysoký	k2eAgNnSc2d1	vysoké
předpětí	předpětí	k1gNnSc2	předpětí
(	(	kIx(	(
<g/>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
recurve	recurvat	k5eAaPmIp3nS	recurvat
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
pouze	pouze	k6eAd1	pouze
konců	konec	k1gInPc2	konec
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
reflex	reflex	k1gInSc1	reflex
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
celého	celý	k2eAgNnSc2d1	celé
ramena	rameno	k1gNnSc2	rameno
<g/>
!	!	kIx.	!
</s>
<s>
Zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
luk	luk	k1gInSc1	luk
reflexní	reflexní	k2eAgInSc1d1	reflexní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
lze	lze	k6eAd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
až	až	k9	až
po	po	k7c6	po
sejmutí	sejmutí	k1gNnSc6	sejmutí
tětivy	tětiva	k1gFnSc2	tětiva
<g/>
,	,	kIx,	,
čeština	čeština	k1gFnSc1	čeština
nemá	mít	k5eNaImIp3nS	mít
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
recurve	recurev	k1gFnPc4	recurev
<g/>
"	"	kIx"	"
relevantní	relevantní	k2eAgInSc4d1	relevantní
jednoslovný	jednoslovný	k2eAgInSc4d1	jednoslovný
výraz	výraz	k1gInSc4	výraz
často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
zaměňovány	zaměňován	k2eAgInPc1d1	zaměňován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Recurve	Recurev	k1gFnPc1	Recurev
konce	konec	k1gInSc2	konec
byly	být	k5eAaImAgFnP	být
vynalezeny	vynaleznout	k5eAaPmNgInP	vynaleznout
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
zkrácení	zkrácení	k1gNnSc1	zkrácení
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
k	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
efektivity	efektivita	k1gFnSc2	efektivita
krátkého	krátký	k2eAgInSc2d1	krátký
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Zahnuté	zahnutý	k2eAgInPc1d1	zahnutý
konce	konec	k1gInPc1	konec
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
docílit	docílit	k5eAaPmF	docílit
srovnatelné	srovnatelný	k2eAgFnPc4d1	srovnatelná
efektivity	efektivita	k1gFnPc4	efektivita
a	a	k8xC	a
stejné	stejný	k2eAgFnPc4d1	stejná
délky	délka	k1gFnPc4	délka
nátahu	nátah	k1gInSc2	nátah
jako	jako	k8xC	jako
u	u	k7c2	u
delšího	dlouhý	k2eAgInSc2d2	delší
rovného	rovný	k2eAgInSc2d1	rovný
luku	luk	k1gInSc2	luk
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
síle	síla	k1gFnSc6	síla
nátahu	nátah	k1gInSc2	nátah
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
snížení	snížení	k1gNnSc3	snížení
"	"	kIx"	"
<g/>
stackingu	stacking	k1gInSc2	stacking
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vysvětleno	vysvětlen	k2eAgNnSc1d1	vysvětleno
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
a	a	k8xC	a
větším	veliký	k2eAgNnSc7d2	veliký
zrychlením	zrychlení	k1gNnSc7	zrychlení
<g/>
,	,	kIx,	,
když	když	k8xS	když
šíp	šíp	k1gInSc1	šíp
opouští	opouštět	k5eAaImIp3nS	opouštět
luk	luk	k1gInSc4	luk
na	na	k7c6	na
konci	konec	k1gInSc6	konec
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
luku	luk	k1gInSc2	luk
"	"	kIx"	"
<g/>
recurve	recurvat	k5eAaPmIp3nS	recurvat
<g/>
"	"	kIx"	"
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
konců	konec	k1gInPc2	konec
ramen	rameno	k1gNnPc2	rameno
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	on	k3xPp3gFnPc4	on
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
"	"	kIx"	"
<g/>
reflex	reflex	k1gInSc1	reflex
<g/>
"	"	kIx"	"
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
efektivitu	efektivita	k1gFnSc4	efektivita
u	u	k7c2	u
krátkého	krátký	k2eAgInSc2d1	krátký
i	i	k8xC	i
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
luku	luk	k1gInSc2	luk
zvýšením	zvýšení	k1gNnSc7	zvýšení
předpětí	předpětí	k1gNnPc2	předpětí
tětivy	tětiva	k1gFnSc2	tětiva
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
opět	opět	k6eAd1	opět
zrychlí	zrychlit	k5eAaPmIp3nS	zrychlit
šíp	šíp	k1gInSc4	šíp
na	na	k7c6	na
konci	konec	k1gInSc6	konec
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
<g/>
Lučiště	lučiště	k1gNnSc1	lučiště
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
symetrické	symetrický	k2eAgInPc4d1	symetrický
(	(	kIx(	(
<g/>
luky	luk	k1gInPc4	luk
Hunů	Hun	k1gMnPc2	Hun
<g/>
,	,	kIx,	,
Mongolů	Mongol	k1gMnPc2	Mongol
<g/>
,	,	kIx,	,
Arabů	Arab	k1gMnPc2	Arab
<g/>
,	,	kIx,	,
Peršanů	Peršan	k1gMnPc2	Peršan
či	či	k8xC	či
Turků	Turek	k1gMnPc2	Turek
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
asymetrické	asymetrický	k2eAgNnSc1d1	asymetrické
(	(	kIx(	(
<g/>
japonské	japonský	k2eAgInPc1d1	japonský
luky	luk	k1gInPc1	luk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asijské	asijský	k2eAgInPc1d1	asijský
reflexní	reflexní	k2eAgInPc1d1	reflexní
luky	luk	k1gInPc1	luk
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
"	"	kIx"	"
<g/>
kompozitní	kompozitní	k2eAgInSc1d1	kompozitní
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
vrstev	vrstva	k1gFnPc2	vrstva
šlach	šlacha	k1gFnPc2	šlacha
<g/>
,	,	kIx,	,
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
rohoviny	rohovina	k1gFnSc2	rohovina
<g/>
,	,	kIx,	,
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
pevně	pevně	k6eAd1	pevně
slepených	slepený	k2eAgFnPc2d1	slepená
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
reflexní	reflexní	k2eAgInPc1d1	reflexní
zvratné	zvratný	k2eAgInPc1d1	zvratný
luky	luk	k1gInPc1	luk
mají	mít	k5eAaImIp3nP	mít
výhodný	výhodný	k2eAgInSc4d1	výhodný
poměr	poměr	k1gInSc4	poměr
délky	délka	k1gFnSc2	délka
luku	luk	k1gInSc2	luk
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
jako	jako	k9	jako
takové	takový	k3xDgFnPc1	takový
byly	být	k5eAaImAgFnP	být
cíleně	cíleně	k6eAd1	cíleně
vynalezeny	vynalezen	k2eAgFnPc1d1	vynalezena
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
z	z	k7c2	z
koňského	koňský	k2eAgInSc2d1	koňský
hřbetu	hřbet	k1gInSc2	hřbet
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
silným	silný	k2eAgInSc7d1	silný
lukem	luk	k1gInSc7	luk
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
středověkých	středověký	k2eAgMnPc2d1	středověký
kompozitních	kompozitní	k2eAgMnPc2d1	kompozitní
luků	luk	k1gInPc2	luk
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gNnSc7	jejich
citlivost	citlivost	k1gFnSc4	citlivost
na	na	k7c4	na
vlhko	vlhko	k1gNnSc4	vlhko
(	(	kIx(	(
<g/>
slábly	slábnout	k5eAaImAgFnP	slábnout
a	a	k8xC	a
rozlepovaly	rozlepovat	k5eAaImAgFnP	rozlepovat
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
byly	být	k5eAaImAgInP	být
proto	proto	k6eAd1	proto
ukládány	ukládat	k5eAaImNgInP	ukládat
do	do	k7c2	do
pouzder	pouzdro	k1gNnPc2	pouzdro
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
byly	být	k5eAaImAgInP	být
takové	takový	k3xDgInPc1	takový
luky	luk	k1gInPc1	luk
před	před	k7c7	před
použitím	použití	k1gNnSc7	použití
i	i	k8xC	i
nahřívány	nahříván	k2eAgMnPc4d1	nahříván
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kladkový	kladkový	k2eAgInSc1d1	kladkový
luk	luk	k1gInSc1	luk
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
compound	compound	k1gInSc1	compound
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgInSc4d1	moderní
druh	druh	k1gInSc4	druh
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
při	při	k7c6	při
sportovní	sportovní	k2eAgFnSc6d1	sportovní
lukostřelbě	lukostřelba	k1gFnSc6	lukostřelba
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
k	k	k7c3	k
lovu	lov	k1gInSc2	lov
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
lučiště	lučiště	k1gNnSc2	lučiště
jsou	být	k5eAaImIp3nP	být
nesymetrické	symetrický	k2eNgFnPc1d1	nesymetrická
kladky	kladka	k1gFnPc1	kladka
tvořící	tvořící	k2eAgFnPc1d1	tvořící
kladkostroj	kladkostroj	k1gInSc4	kladkostroj
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
napínací	napínací	k2eAgFnSc4d1	napínací
sílu	síla	k1gFnSc4	síla
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
nátahu	nátah	k1gInSc2	nátah
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
nárůst	nárůst	k1gInSc1	nárůst
síly	síla	k1gFnSc2	síla
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgFnSc4d3	nejmenší
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nátahu	nátah	k1gInSc2	nátah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
dolet	dolet	k1gInSc4	dolet
šípu	šíp	k1gInSc2	šíp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
snížení	snížení	k1gNnSc3	snížení
napínací	napínací	k2eAgFnSc2d1	napínací
síly	síla	k1gFnSc2	síla
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nátahu	nátah	k1gInSc2	nátah
a	a	k8xC	a
menšímu	malý	k2eAgInSc3d2	menší
zpětnému	zpětný	k2eAgInSc3d1	zpětný
rázu	ráz	k1gInSc3	ráz
<g/>
,	,	kIx,	,
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
míření	míření	k1gNnSc4	míření
a	a	k8xC	a
přesnost	přesnost	k1gFnSc4	přesnost
střelby	střelba	k1gFnSc2	střelba
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kladkovým	kladkový	k2eAgInSc7d1	kladkový
lukem	luk	k1gInSc7	luk
si	se	k3xPyFc3	se
lukostřelec	lukostřelec	k1gMnSc1	lukostřelec
odbude	odbýt	k5eAaPmIp3nS	odbýt
největší	veliký	k2eAgFnSc4d3	veliký
námahu	námaha	k1gFnSc4	námaha
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
nátahu	nátah	k1gInSc2	nátah
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
nátahu	nátah	k1gInSc2	nátah
již	již	k6eAd1	již
necítí	cítit	k5eNaImIp3nS	cítit
v	v	k7c6	v
prstech	prst	k1gInPc6	prst
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
zamíření	zamíření	k1gNnSc4	zamíření
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
luku	luk	k1gInSc2	luk
(	(	kIx(	(
<g/>
madlo	madlo	k1gNnSc1	madlo
<g/>
,	,	kIx,	,
střed	střed	k1gInSc1	střed
luku	luk	k1gInSc2	luk
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
nejčastěji	často	k6eAd3	často
duralová	duralový	k2eAgFnSc1d1	duralová
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
plastová	plastový	k2eAgFnSc1d1	plastová
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc1	rameno
bývají	bývat	k5eAaImIp3nP	bývat
vyrobena	vyrobit	k5eAaPmNgNnP	vyrobit
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
nebo	nebo	k8xC	nebo
karbonových	karbonový	k2eAgInPc2d1	karbonový
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Kladkové	kladkový	k2eAgInPc1d1	kladkový
luky	luk	k1gInPc1	luk
bývají	bývat	k5eAaImIp3nP	bývat
opatřeny	opatřen	k2eAgInPc1d1	opatřen
poměrně	poměrně	k6eAd1	poměrně
sofistikovanými	sofistikovaný	k2eAgNnPc7d1	sofistikované
mířidly	mířidlo	k1gNnPc7	mířidlo
(	(	kIx(	(
<g/>
optika	optika	k1gFnSc1	optika
<g/>
,	,	kIx,	,
vodováha	vodováha	k1gFnSc1	vodováha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
bývají	bývat	k5eAaImIp3nP	bývat
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
modernímu	moderní	k2eAgInSc3d1	moderní
lovu	lov	k1gInSc3	lov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
lov	lov	k1gInSc1	lov
lukem	luk	k1gInSc7	luk
proti	proti	k7c3	proti
mysliveckému	myslivecký	k2eAgNnSc3d1	Myslivecké
právu	právo	k1gNnSc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
lovu	lov	k1gInSc2	lov
se	se	k3xPyFc4	se
kladkové	kladkový	k2eAgInPc1d1	kladkový
luky	luk	k1gInPc1	luk
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
sportovní	sportovní	k2eAgFnSc3d1	sportovní
střelbě	střelba	k1gFnSc3	střelba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
pořádáno	pořádat	k5eAaImNgNnS	pořádat
i	i	k9	i
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Peletový	Peletový	k2eAgInSc1d1	Peletový
luk	luk	k1gInSc1	luk
<g/>
:	:	kIx,	:
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc1	dva
tětivy	tětiva	k1gFnPc1	tětiva
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgFnPc7	jenž
je	být	k5eAaImIp3nS	být
upevněn	upevněn	k2eAgInSc1d1	upevněn
váček	váček	k1gInSc1	váček
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
vkládají	vkládat	k5eAaImIp3nP	vkládat
střely	střela	k1gFnPc1	střela
<g/>
.	.	kIx.	.
</s>
<s>
Nestřílí	střílet	k5eNaImIp3nS	střílet
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kamínky	kamínek	k1gInPc4	kamínek
nebo	nebo	k8xC	nebo
olověné	olověný	k2eAgFnPc4d1	olověná
kuličky	kulička	k1gFnPc4	kulička
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
slabší	slabý	k2eAgFnSc1d2	slabší
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
drobné	drobný	k2eAgFnSc2d1	drobná
zvěře	zvěř	k1gFnSc2	zvěř
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
princip	princip	k1gInSc1	princip
používal	používat	k5eAaImAgInS	používat
u	u	k7c2	u
tzv.	tzv.	kA	tzv.
balistru	balistr	k1gInSc2	balistr
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnPc4d1	malá
kuše	kuše	k1gFnPc4	kuše
s	s	k7c7	s
obvykle	obvykle	k6eAd1	obvykle
ocelovými	ocelový	k2eAgNnPc7d1	ocelové
lučišti	lučiště	k1gNnPc7	lučiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šíp	šíp	k1gInSc4	šíp
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc1	každý
šíp	šíp	k1gInSc1	šíp
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
dříku	dřík	k1gInSc2	dřík
(	(	kIx(	(
<g/>
ratiště	ratiště	k1gNnSc2	ratiště
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrotu	hrot	k1gInSc2	hrot
a	a	k8xC	a
hlaviště	hlaviště	k1gNnSc2	hlaviště
(	(	kIx(	(
<g/>
končíku	končík	k1gInSc2	končík
<g/>
)	)	kIx)	)
s	s	k7c7	s
vodícím	vodící	k2eAgInSc7d1	vodící
žlábkem	žlábek	k1gInSc7	žlábek
pro	pro	k7c4	pro
tětivu	tětiva	k1gFnSc4	tětiva
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
opatřen	opatřit	k5eAaPmNgInS	opatřit
opeřením	opeření	k1gNnSc7	opeření
(	(	kIx(	(
<g/>
křidélky	křidélko	k1gNnPc7	křidélko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
šíp	šíp	k1gInSc1	šíp
stabilizují	stabilizovat	k5eAaBmIp3nP	stabilizovat
a	a	k8xC	a
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
jeho	jeho	k3xOp3gFnPc4	jeho
letové	letový	k2eAgFnPc4d1	letová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Šípy	šíp	k1gInPc1	šíp
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
především	především	k9	především
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
podle	podle	k7c2	podle
tvaru	tvar	k1gInSc2	tvar
hrotu	hrot	k1gInSc2	hrot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
šípy	šíp	k1gInPc1	šíp
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
zpravidla	zpravidla	k6eAd1	zpravidla
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
tenké	tenký	k2eAgInPc4d1	tenký
<g/>
,	,	kIx,	,
rovné	rovný	k2eAgInPc4d1	rovný
pruty	prut	k1gInPc4	prut
např.	např.	kA	např.
šípku	šípek	k1gInSc2	šípek
<g/>
,	,	kIx,	,
angreštu	angrešt	k1gInSc2	angrešt
či	či	k8xC	či
kaliny	kalina	k1gFnSc2	kalina
<g/>
,	,	kIx,	,
svídy	svída	k1gFnSc2	svída
<g/>
,	,	kIx,	,
rákosu	rákos	k1gInSc2	rákos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
štípané	štípaný	k2eAgInPc1d1	štípaný
z	z	k7c2	z
rovnoletého	rovnoletý	k2eAgNnSc2d1	rovnoletý
dřeva	dřevo	k1gNnSc2	dřevo
jasanu	jasan	k1gInSc2	jasan
<g/>
,	,	kIx,	,
topolu	topol	k1gInSc2	topol
<g/>
,	,	kIx,	,
smrku	smrk	k1gInSc2	smrk
<g/>
,	,	kIx,	,
jedle	jedle	k1gFnPc1	jedle
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
bambus	bambus	k1gInSc4	bambus
nebo	nebo	k8xC	nebo
rákos	rákos	k1gInSc4	rákos
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
zpravidla	zpravidla	k6eAd1	zpravidla
bambus	bambus	k1gInSc4	bambus
<g/>
,	,	kIx,	,
u	u	k7c2	u
Inuitů	Inuita	k1gMnPc2	Inuita
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
z	z	k7c2	z
nedostatku	nedostatek	k1gInSc2	nedostatek
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
materiálů	materiál	k1gInPc2	materiál
někdy	někdy	k6eAd1	někdy
i	i	k9	i
z	z	k7c2	z
kosti	kost	k1gFnSc2	kost
nebo	nebo	k8xC	nebo
mrožího	mroží	k2eAgInSc2d1	mroží
klu	kel	k1gInSc2	kel
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
šípu	šíp	k1gInSc2	šíp
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
300	[number]	k4	300
cm	cm	kA	cm
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šípy	šíp	k1gInPc1	šíp
delší	dlouhý	k2eAgFnSc2d2	delší
než	než	k8xS	než
100	[number]	k4	100
cm	cm	kA	cm
používají	používat	k5eAaImIp3nP	používat
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
poněkud	poněkud	k6eAd1	poněkud
kratší	krátký	k2eAgFnSc1d2	kratší
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
asi	asi	k9	asi
90	[number]	k4	90
cm	cm	kA	cm
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
šípy	šíp	k1gInPc1	šíp
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
se	se	k3xPyFc4	se
fyzickými	fyzický	k2eAgFnPc7d1	fyzická
předpoklady	předpoklad	k1gInPc4	předpoklad
lučištníka	lučištník	k1gMnSc2	lučištník
(	(	kIx(	(
<g/>
k	k	k7c3	k
změření	změření	k1gNnSc3	změření
sevřete	sevřít	k5eAaPmIp2nP	sevřít
šíp	šíp	k1gInSc1	šíp
opřený	opřený	k2eAgInSc1d1	opřený
v	v	k7c6	v
jugulární	jugulární	k2eAgFnSc6d1	jugulární
jamce	jamka	k1gFnSc6	jamka
mezi	mezi	k7c7	mezi
dlaněmi	dlaň	k1gFnPc7	dlaň
předpažených	předpažený	k2eAgFnPc2d1	předpažená
rukou	ruka	k1gFnPc2	ruka
s	s	k7c7	s
napjatými	napjatý	k2eAgInPc7d1	napjatý
prsty	prst	k1gInPc7	prst
a	a	k8xC	a
mezi	mezi	k7c4	mezi
prostředníky	prostředník	k1gMnPc4	prostředník
by	by	kYmCp3nP	by
vám	vy	k3xPp2nPc3	vy
měl	mít	k5eAaImAgInS	mít
vyčnívat	vyčnívat	k5eAaImF	vyčnívat
jen	jen	k9	jen
hrot	hrot	k1gInSc4	hrot
šípu	šíp	k1gInSc2	šíp
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
šípu	šíp	k1gInSc2	šíp
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
části	část	k1gFnSc2	část
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
loveckých	lovecký	k2eAgInPc2d1	lovecký
<g/>
,	,	kIx,	,
výměnný	výměnný	k2eAgInSc1d1	výměnný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
krátké	krátký	k2eAgInPc4d1	krátký
šípy	šíp	k1gInPc4	šíp
používali	používat	k5eAaImAgMnP	používat
i	i	k9	i
severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
indiáni	indián	k1gMnPc1	indián
na	na	k7c6	na
Velkých	velký	k2eAgFnPc6d1	velká
Pláních	pláň	k1gFnPc6	pláň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
existuje	existovat	k5eAaImIp3nS	existovat
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
vystřeluje	vystřelovat	k5eAaImIp3nS	vystřelovat
šíp	šíp	k1gInSc1	šíp
kratší	krátký	k2eAgInSc1d2	kratší
než	než	k8xS	než
délka	délka	k1gFnSc1	délka
nátahu	nátah	k1gInSc2	nátah
vložený	vložený	k2eAgInSc1d1	vložený
do	do	k7c2	do
(	(	kIx(	(
<g/>
bambusového	bambusový	k2eAgNnSc2d1	bambusové
<g/>
)	)	kIx)	)
korýtka	korýtko	k1gNnSc2	korýtko
<g/>
.	.	kIx.	.
<g/>
Opeření	opeření	k1gNnSc1	opeření
šípu	šíp	k1gInSc2	šíp
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
olep	olepit	k5eAaPmRp2nS	olepit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
ptačích	ptačí	k2eAgNnPc2d1	ptačí
per	pero	k1gNnPc2	pero
<g/>
,	,	kIx,	,
u	u	k7c2	u
moderních	moderní	k2eAgInPc2d1	moderní
loveckých	lovecký	k2eAgInPc2d1	lovecký
a	a	k8xC	a
sportovních	sportovní	k2eAgInPc2d1	sportovní
šípů	šíp	k1gInPc2	šíp
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
z	z	k7c2	z
plátků	plátek	k1gInPc2	plátek
celuloidu	celuloid	k1gInSc2	celuloid
<g/>
,	,	kIx,	,
gumy	guma	k1gFnSc2	guma
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Křidélka	křidélko	k1gNnPc1	křidélko
bývají	bývat	k5eAaImIp3nP	bývat
většinou	většinou	k6eAd1	většinou
tři	tři	k4xCgFnPc4	tři
(	(	kIx(	(
<g/>
severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
indiáni	indián	k1gMnPc1	indián
<g/>
,	,	kIx,	,
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgMnPc1	čtyři
(	(	kIx(	(
<g/>
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
dvě	dva	k4xCgFnPc4	dva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nouzové	nouzový	k2eAgFnPc1d1	nouzová
<g/>
,	,	kIx,	,
či	či	k8xC	či
dětské	dětský	k2eAgInPc1d1	dětský
šípy	šíp	k1gInPc1	šíp
indiánů	indián	k1gMnPc2	indián
kmene	kmen	k1gInSc2	kmen
Cherokee	Cherokee	k1gFnPc2	Cherokee
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
spirálovitě	spirálovitě	k6eAd1	spirálovitě
vinuté	vinutý	k2eAgNnSc1d1	vinuté
opeření	opeření	k1gNnSc1	opeření
šípů	šíp	k1gInPc2	šíp
některých	některý	k3yIgInPc2	některý
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
uvádí	uvádět	k5eAaImIp3nS	uvádět
šíp	šíp	k1gInSc1	šíp
do	do	k7c2	do
rotace	rotace	k1gFnSc2	rotace
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
vývrt	vývrt	k1gInSc4	vývrt
v	v	k7c6	v
hlavni	hlaveň	k1gFnSc6	hlaveň
kulovnice	kulovnice	k1gFnSc2	kulovnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
vyrovnávána	vyrovnáván	k2eAgFnSc1d1	vyrovnávána
případná	případný	k2eAgFnSc1d1	případná
nesymetrie	nesymetrie	k1gFnSc1	nesymetrie
šípu	šíp	k1gInSc2	šíp
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
váhová	váhový	k2eAgFnSc1d1	váhová
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
tvarová	tvarový	k2eAgFnSc1d1	tvarová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Opeření	opeření	k1gNnSc1	opeření
šípu	šíp	k1gInSc2	šíp
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
letové	letový	k2eAgFnPc4d1	letová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
i	i	k9	i
chybět	chybět	k5eAaImF	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
domorodí	domorodý	k2eAgMnPc1d1	domorodý
Afričané	Afričan	k1gMnPc1	Afričan
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Sanové	Sanová	k1gFnPc1	Sanová
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
šípy	šíp	k1gInPc4	šíp
bez	bez	k7c2	bez
opeření	opeření	k1gNnSc2	opeření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hroty	hrot	k1gInPc1	hrot
šípů	šíp	k1gInPc2	šíp
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
zvířecích	zvířecí	k2eAgInPc2d1	zvířecí
drápů	dráp	k1gInPc2	dráp
a	a	k8xC	a
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
tvrzeného	tvrzený	k2eAgNnSc2d1	tvrzené
nad	nad	k7c7	nad
ohněm	oheň	k1gInSc7	oheň
nebo	nebo	k8xC	nebo
ostře	ostro	k6eAd1	ostro
seříznutého	seříznutý	k2eAgInSc2d1	seříznutý
bambusu	bambus	k1gInSc2	bambus
<g/>
,	,	kIx,	,
nejostřejší	ostrý	k2eAgFnSc1d3	nejostřejší
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ze	z	k7c2	z
štípaného	štípaný	k2eAgInSc2d1	štípaný
kamene	kámen	k1gInSc2	kámen
(	(	kIx(	(
<g/>
pazourek	pazourek	k1gInSc1	pazourek
<g/>
,	,	kIx,	,
obsidián	obsidián	k1gInSc1	obsidián
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
převládly	převládnout	k5eAaPmAgInP	převládnout
kovy	kov	k1gInPc1	kov
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bronz	bronz	k1gInSc4	bronz
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
ocel	ocel	k1gFnSc4	ocel
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
duralu	dural	k1gInSc2	dural
<g/>
,	,	kIx,	,
nerezové	rezový	k2eNgFnSc2d1	nerezová
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
titanu	titan	k1gInSc2	titan
či	či	k8xC	či
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgInSc1d3	nejčastější
tvar	tvar	k1gInSc1	tvar
hrotu	hrot	k1gInSc2	hrot
loveckého	lovecký	k2eAgInSc2d1	lovecký
šípu	šíp	k1gInSc2	šíp
je	být	k5eAaImIp3nS	být
trojúhelníkový	trojúhelníkový	k2eAgMnSc1d1	trojúhelníkový
<g/>
,	,	kIx,	,
s	s	k7c7	s
jedním	jeden	k4xCgNnSc7	jeden
nebo	nebo	k8xC	nebo
dvěma	dva	k4xCgInPc7	dva
protihroty	protihrot	k1gInPc7	protihrot
<g/>
,	,	kIx,	,
znesnadňující	znesnadňující	k2eAgFnSc1d1	znesnadňující
jeho	jeho	k3xOp3gNnSc4	jeho
vytažení	vytažení	k1gNnSc4	vytažení
z	z	k7c2	z
rány	rána	k1gFnSc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
hroty	hrot	k1gInPc4	hrot
kuželovité	kuželovitý	k2eAgInPc4d1	kuželovitý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ogivální	ogivální	k2eAgNnSc1d1	ogivální
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
u	u	k7c2	u
sportovních	sportovní	k2eAgInPc2d1	sportovní
šípů	šíp	k1gInPc2	šíp
<g/>
)	)	kIx)	)
a	a	k8xC	a
listovité	listovitý	k2eAgFnPc1d1	listovitá
(	(	kIx(	(
<g/>
kopinaté	kopinatý	k2eAgFnPc1d1	kopinatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
méně	málo	k6eAd2	málo
obvyklým	obvyklý	k2eAgInPc3d1	obvyklý
tvarům	tvar	k1gInPc3	tvar
patří	patřit	k5eAaImIp3nS	patřit
hroty	hrot	k1gInPc1	hrot
zubaté	zubatý	k2eAgInPc1d1	zubatý
k	k	k7c3	k
trhání	trhání	k1gNnSc3	trhání
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
půlměsíčité	půlměsíčitý	k2eAgFnSc2d1	půlměsíčitý
(	(	kIx(	(
<g/>
známé	známá	k1gFnSc2	známá
např.	např.	kA	např.
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
šípy	šíp	k1gInPc1	šíp
s	s	k7c7	s
více	hodně	k6eAd2	hodně
ozubenými	ozubený	k2eAgInPc7d1	ozubený
hroty	hrot	k1gInPc7	hrot
na	na	k7c4	na
lov	lov	k1gInSc4	lov
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
mořských	mořský	k2eAgMnPc2d1	mořský
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
šípy	šíp	k1gInPc1	šíp
s	s	k7c7	s
tupým	tupý	k2eAgInSc7d1	tupý
hrotem	hrot	k1gInSc7	hrot
<g/>
,	,	kIx,	,
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
a	a	k8xC	a
hry	hra	k1gFnPc4	hra
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
srážení	srážení	k1gNnSc4	srážení
ovoce	ovoce	k1gNnSc1	ovoce
nebo	nebo	k8xC	nebo
chytání	chytání	k1gNnSc1	chytání
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vystřelený	vystřelený	k2eAgInSc1d1	vystřelený
šíp	šíp	k1gInSc1	šíp
pouze	pouze	k6eAd1	pouze
omráčí	omráčit	k5eAaPmIp3nS	omráčit
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
<g/>
,	,	kIx,	,
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
některé	některý	k3yIgInPc1	některý
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
používali	používat	k5eAaImAgMnP	používat
hvízdající	hvízdající	k2eAgInPc4d1	hvízdající
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
signalizaci	signalizace	k1gFnSc3	signalizace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
zastrašení	zastrašení	k1gNnSc3	zastrašení
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
i	i	k8xC	i
Americe	Amerika	k1gFnSc6	Amerika
používaly	používat	k5eAaImAgInP	používat
také	také	k9	také
zápalné	zápalný	k2eAgInPc1d1	zápalný
šípy	šíp	k1gInPc1	šíp
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
dřík	dřík	k1gInSc1	dřík
byl	být	k5eAaImAgInS	být
obtočen	obtočit	k5eAaPmNgInS	obtočit
koudelí	koudel	k1gFnSc7	koudel
nebo	nebo	k8xC	nebo
přízí	příz	k1gFnSc7	příz
<g/>
,	,	kIx,	,
napuštěnou	napuštěný	k2eAgFnSc4d1	napuštěná
hořlavinami	hořlavina	k1gFnPc7	hořlavina
a	a	k8xC	a
před	před	k7c7	před
výstřelem	výstřel	k1gInSc7	výstřel
zapálen	zapálit	k5eAaPmNgInS	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
otrávené	otrávený	k2eAgInPc4d1	otrávený
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Použití	použití	k1gNnSc1	použití
šípových	šípový	k2eAgInPc2d1	šípový
jedů	jed	k1gInPc2	jed
bylo	být	k5eAaImAgNnS	být
samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
foukačkou	foukačka	k1gFnSc7	foukačka
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
a	a	k8xC	a
Jihovýchodní	jihovýchodní	k2eAgFnSc3d1	jihovýchodní
Asii	Asie	k1gFnSc3	Asie
<g/>
,	,	kIx,	,
šípové	šípový	k2eAgInPc1d1	šípový
jedy	jed	k1gInPc1	jed
se	se	k3xPyFc4	se
však	však	k9	však
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
i	i	k9	i
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
lukem	luk	k1gInSc7	luk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro	pro	k7c4	pro
lov	lov	k1gInSc4	lov
slonů	slon	k1gMnPc2	slon
<g/>
,	,	kIx,	,
nosorožců	nosorožec	k1gMnPc2	nosorožec
<g/>
,	,	kIx,	,
goril	gorila	k1gFnPc2	gorila
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
nebezpečných	bezpečný	k2eNgNnPc2d1	nebezpečné
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc3d1	jižní
Americe	Amerika	k1gFnSc3	Amerika
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
používali	používat	k5eAaImAgMnP	používat
např.	např.	kA	např.
Slované	Slovan	k1gMnPc1	Slovan
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
loveckým	lovecký	k2eAgInPc3d1	lovecký
i	i	k8xC	i
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kalená	kalený	k2eAgFnSc1d1	kalená
střela	střela	k1gFnSc1	střela
<g/>
"	"	kIx"	"
v	v	k7c6	v
ruských	ruský	k2eAgFnPc6d1	ruská
bohatýrských	bohatýrský	k2eAgFnPc6d1	bohatýrská
bylinách	bylina	k1gFnPc6	bylina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šípy	šíp	k1gInPc1	šíp
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
střelcem	střelec	k1gMnSc7	střelec
nošeny	nosit	k5eAaImNgInP	nosit
v	v	k7c6	v
toulci	toulec	k1gInSc6	toulec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
ke	k	k7c3	k
sportovním	sportovní	k2eAgInPc3d1	sportovní
a	a	k8xC	a
loveckým	lovecký	k2eAgInPc3d1	lovecký
účelům	účel	k1gInPc3	účel
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
či	či	k8xC	či
USA	USA	kA	USA
šípy	šíp	k1gInPc4	šíp
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
<g/>
,	,	kIx,	,
laminátové	laminátový	k2eAgInPc4d1	laminátový
<g/>
,	,	kIx,	,
duralové	duralový	k2eAgInPc4d1	duralový
<g/>
,	,	kIx,	,
karbonové	karbonový	k2eAgInPc4d1	karbonový
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
z	z	k7c2	z
kombinovaných	kombinovaný	k2eAgInPc2d1	kombinovaný
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
šípy	šíp	k1gInPc1	šíp
mají	mít	k5eAaImIp3nP	mít
ratiště	ratiště	k1gNnSc4	ratiště
z	z	k7c2	z
trubky	trubka	k1gFnSc2	trubka
různé	různý	k2eAgFnSc2d1	různá
tvrdosti	tvrdost	k1gFnSc2	tvrdost
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
spin	spin	k1gInSc1	spin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíp	Šíp	k1gMnSc1	Šíp
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
<g/>
,	,	kIx,	,
čím	co	k3yQnSc7	co
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
spin	spin	k1gInSc4	spin
<g/>
.	.	kIx.	.
</s>
<s>
Spin	spin	k1gInSc1	spin
je	být	k5eAaImIp3nS	být
přepočten	přepočíst	k5eAaPmNgInS	přepočíst
z	z	k7c2	z
délky	délka	k1gFnSc2	délka
příčného	příčný	k2eAgInSc2d1	příčný
průhybu	průhyb	k1gInSc2	průhyb
při	při	k7c6	při
určené	určený	k2eAgFnSc6d1	určená
příčné	příčný	k2eAgFnSc6d1	příčná
zátěži	zátěž	k1gFnSc6	zátěž
mezi	mezi	k7c7	mezi
podstavci	podstavec	k1gInPc7	podstavec
o	o	k7c4	o
určené	určený	k2eAgFnPc4d1	určená
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
-	-	kIx~	-
čím	co	k3yQnSc7	co
je	být	k5eAaImIp3nS	být
průhyb	průhyb	k1gInSc1	průhyb
větší	veliký	k2eAgInSc1d2	veliký
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
spin	spin	k1gInSc4	spin
menší	malý	k2eAgInSc4d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Spin	spin	k1gInSc1	spin
šípu	šíp	k1gInSc2	šíp
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
odpovídat	odpovídat	k5eAaImF	odpovídat
síle	síla	k1gFnSc3	síla
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
kvůli	kvůli	k7c3	kvůli
přesností	přesnost	k1gFnSc7	přesnost
střelby	střelba	k1gFnSc2	střelba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dosti	dosti	k6eAd1	dosti
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
vystřelit	vystřelit	k5eAaPmF	vystřelit
příliš	příliš	k6eAd1	příliš
měkký	měkký	k2eAgInSc1d1	měkký
šíp	šíp	k1gInSc1	šíp
příliš	příliš	k6eAd1	příliš
silným	silný	k2eAgInSc7d1	silný
lukem	luk	k1gInSc7	luk
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
zlomit	zlomit	k5eAaPmF	zlomit
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
než	než	k8xS	než
se	se	k3xPyFc4	se
vzdálí	vzdálit	k5eAaPmIp3nS	vzdálit
ruce	ruka	k1gFnPc4	ruka
která	který	k3yQgFnSc1	který
luk	luk	k1gInSc4	luk
drží	držet	k5eAaImIp3nS	držet
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
I	i	k9	i
pro	pro	k7c4	pro
základní	základní	k2eAgFnSc4d1	základní
sportovní	sportovní	k2eAgFnSc4d1	sportovní
střelbu	střelba	k1gFnSc4	střelba
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používají	používat	k5eAaImIp3nP	používat
běžně	běžně	k6eAd1	běžně
trubky	trubka	k1gFnPc1	trubka
z	z	k7c2	z
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
laminátu	laminát	k1gInSc2	laminát
-	-	kIx~	-
karbon	karbon	k1gInSc1	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
drží	držet	k5eAaImIp3nS	držet
dobře	dobře	k6eAd1	dobře
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
lehký	lehký	k2eAgInSc1d1	lehký
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
ovšem	ovšem	k9	ovšem
zlomit	zlomit	k5eAaPmF	zlomit
a	a	k8xC	a
vlákna	vlákno	k1gNnPc1	vlákno
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nepříjemná	příjemný	k2eNgNnPc1d1	nepříjemné
zranění	zranění	k1gNnPc1	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
sportovní	sportovní	k2eAgMnPc1d1	sportovní
střelci	střelec	k1gMnPc1	střelec
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
duralu	dural	k1gInSc2	dural
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kombinovaným	kombinovaný	k2eAgInPc3d1	kombinovaný
materiálům	materiál	k1gInPc3	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
hliník	hliník	k1gInSc1	hliník
ohne	ohnout	k5eAaPmIp3nS	ohnout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
šíp	šíp	k1gInSc4	šíp
srovnat	srovnat	k5eAaPmF	srovnat
do	do	k7c2	do
rozumné	rozumný	k2eAgFnSc2d1	rozumná
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trubce	trubka	k1gFnSc6	trubka
je	být	k5eAaImIp3nS	být
vpředu	vpředu	k6eAd1	vpředu
buď	buď	k8xC	buď
hrot	hrot	k1gInSc1	hrot
přímo	přímo	k6eAd1	přímo
vlepen	vlepen	k2eAgInSc1d1	vlepen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
použit	použit	k2eAgInSc1d1	použit
tzv.	tzv.	kA	tzv.
insert	insert	k1gInSc1	insert
s	s	k7c7	s
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
závitem	závit	k1gInSc7	závit
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
hrot	hrot	k1gInSc1	hrot
našroubován	našroubován	k2eAgInSc1d1	našroubován
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
snadno	snadno	k6eAd1	snadno
vyměnit	vyměnit	k5eAaPmF	vyměnit
<g/>
.	.	kIx.	.
</s>
<s>
Váha	váha	k1gFnSc1	váha
hrotů	hrot	k1gInPc2	hrot
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
podle	podle	k7c2	podle
střelecké	střelecký	k2eAgFnSc2d1	střelecká
tradice	tradice	k1gFnSc2	tradice
v	v	k7c6	v
grainech	grain	k1gInPc6	grain
(	(	kIx(	(
<g/>
1	[number]	k4	1
gr	gr	k?	gr
<g/>
.	.	kIx.	.
=	=	kIx~	=
0,06	[number]	k4	0,06
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
váha	váha	k1gFnSc1	váha
je	být	k5eAaImIp3nS	být
70	[number]	k4	70
až	až	k9	až
150	[number]	k4	150
gr	gr	k?	gr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačném	opačný	k2eAgInSc6d1	opačný
konci	konec	k1gInSc6	konec
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
trubky	trubka	k1gFnSc2	trubka
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
vlepen	vlepen	k2eAgInSc4d1	vlepen
končík	končík	k1gInSc4	končík
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pin	pin	k1gInSc1	pin
končíku	končík	k1gInSc2	končík
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
se	se	k3xPyFc4	se
končík	končík	k1gInSc4	končík
nasazuje	nasazovat	k5eAaImIp3nS	nasazovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
zadní	zadní	k2eAgFnSc4d1	zadní
část	část	k1gFnSc4	část
trubky	trubka	k1gFnSc2	trubka
se	se	k3xPyFc4	se
v	v	k7c6	v
nastavitelném	nastavitelný	k2eAgInSc6d1	nastavitelný
přípravku	přípravek	k1gInSc6	přípravek
lepí	lepit	k5eAaImIp3nS	lepit
olep	olepit	k5eAaPmRp2nS	olepit
(	(	kIx(	(
<g/>
křidélka	křidélko	k1gNnPc1	křidélko
<g/>
,	,	kIx,	,
opeření	opeření	k1gNnSc1	opeření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ceny	cena	k1gFnPc1	cena
šípů	šíp	k1gInPc2	šíp
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
cca	cca	kA	cca
sto	sto	k4xCgNnSc4	sto
korun	koruna	k1gFnPc2	koruna
až	až	k6eAd1	až
do	do	k7c2	do
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
šípů	šíp	k1gInPc2	šíp
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
ode	ode	k7c2	ode
dna	dno	k1gNnSc2	dno
zářezu	zářez	k1gInSc3	zářez
končíku	končík	k1gInSc2	končík
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
trubky	trubka	k1gFnSc2	trubka
<g/>
,	,	kIx,	,
hrot	hrot	k1gInSc1	hrot
se	se	k3xPyFc4	se
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
výměnnosti	výměnnost	k1gFnSc2	výměnnost
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konkretního	konkretní	k2eAgMnSc4d1	konkretní
střelce	střelec	k1gMnSc4	střelec
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
konec	konec	k1gInSc1	konec
šípu	šíp	k1gInSc2	šíp
právě	právě	k6eAd1	právě
vyčuhoval	vyčuhovat	k5eAaImAgMnS	vyčuhovat
mezi	mezi	k7c7	mezi
napjatými	napjatý	k2eAgInPc7d1	napjatý
prsty	prst	k1gInPc7	prst
rukou	ruka	k1gFnPc2	ruka
to	ten	k3xDgNnSc1	ten
tak	tak	k8xS	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
konec	konec	k1gInSc4	konec
šípu	šíp	k1gInSc2	šíp
opřete	opřít	k5eAaPmIp2nP	opřít
v	v	k7c6	v
jugulární	jugulární	k2eAgFnSc6d1	jugulární
jamce	jamka	k1gFnSc6	jamka
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
klíčními	klíční	k2eAgFnPc7d1	klíční
kostmi	kost	k1gFnPc7	kost
pod	pod	k7c7	pod
krkem	krk	k1gInSc7	krk
<g/>
)	)	kIx)	)
a	a	k8xC	a
šíp	šíp	k1gInSc4	šíp
sevřete	sevřít	k5eAaPmIp2nP	sevřít
v	v	k7c6	v
předpažených	předpažený	k2eAgFnPc6d1	předpažená
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výstřel	výstřel	k1gInSc1	výstřel
z	z	k7c2	z
luku	luk	k1gInSc2	luk
==	==	k?	==
</s>
</p>
<p>
<s>
Luk	luk	k1gInSc1	luk
se	se	k3xPyFc4	se
natahuje	natahovat	k5eAaImIp3nS	natahovat
naplno	naplno	k6eAd1	naplno
vždy	vždy	k6eAd1	vždy
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
nátah	nátah	k1gInSc4	nátah
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
olympijského	olympijský	k2eAgInSc2d1	olympijský
luku	luk	k1gInSc2	luk
používají	používat	k5eAaImIp3nP	používat
závodníci	závodník	k1gMnPc1	závodník
tzv.	tzv.	kA	tzv.
klapačku	klapačka	k1gFnSc4	klapačka
indikující	indikující	k2eAgFnSc2d1	indikující
natažení	natažení	k1gNnSc4	natažení
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
nátah	nátah	k1gInSc4	nátah
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
stejná	stejný	k2eAgFnSc1d1	stejná
metná	metný	k2eAgFnSc1d1	metná
rychlost	rychlost	k1gFnSc1	rychlost
při	při	k7c6	při
každém	každý	k3xTgInSc6	každý
výstřelu	výstřel	k1gInSc6	výstřel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
sportovních	sportovní	k2eAgInPc2d1	sportovní
luků	luk	k1gInPc2	luk
je	být	k5eAaImIp3nS	být
tětiva	tětiva	k1gFnSc1	tětiva
napínána	napínat	k5eAaImNgFnS	napínat
třemi	tři	k4xCgInPc7	tři
prsty	prst	k1gInPc7	prst
chráněnými	chráněný	k2eAgFnPc7d1	chráněná
tzv.	tzv.	kA	tzv.
zástěrkou	zástěrka	k1gFnSc7	zástěrka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rukavicí	rukavice	k1gFnSc7	rukavice
a	a	k8xC	a
ruka	ruka	k1gFnSc1	ruka
drží	držet	k5eAaImIp3nS	držet
tětivu	tětiva	k1gFnSc4	tětiva
dlaní	dlaň	k1gFnPc2	dlaň
k	k	k7c3	k
tváři	tvář	k1gFnSc3	tvář
v	v	k7c6	v
takzvaném	takzvaný	k2eAgInSc6d1	takzvaný
kotvícím	kotvící	k2eAgInSc6d1	kotvící
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kladkových	kladkový	k2eAgInPc2d1	kladkový
luků	luk	k1gInPc2	luk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vypouštěč	vypouštěč	k1gMnSc1	vypouštěč
a	a	k8xC	a
tah	tah	k1gInSc1	tah
tětivy	tětiva	k1gFnSc2	tětiva
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
rozložen	rozložit	k5eAaPmNgInS	rozložit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přenášen	přenášen	k2eAgInSc1d1	přenášen
závěsem	závěs	k1gInSc7	závěs
na	na	k7c4	na
zápěstí	zápěstí	k1gNnSc4	zápěstí
<g/>
.	.	kIx.	.
</s>
<s>
Ruka	ruka	k1gFnSc1	ruka
je	být	k5eAaImIp3nS	být
otočena	otočit	k5eAaPmNgFnS	otočit
hřbetem	hřbet	k1gInSc7	hřbet
k	k	k7c3	k
tváři	tvář	k1gFnSc3	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Tětiva	tětiva	k1gFnSc1	tětiva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
držena	držen	k2eAgFnSc1d1	držena
vypouštěčem	vypouštěč	k1gMnSc7	vypouštěč
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
prsty	prst	k1gInPc4	prst
a	a	k8xC	a
vypouští	vypouštět	k5eAaImIp3nS	vypouštět
se	se	k3xPyFc4	se
mechanismem	mechanismus	k1gInSc7	mechanismus
připomínajícím	připomínající	k2eAgInSc7d1	připomínající
spoušť	spoušť	k1gFnSc4	spoušť
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mongolského	mongolský	k2eAgNnSc2d1	mongolské
držení	držení	k1gNnSc2	držení
je	být	k5eAaImIp3nS	být
tětiva	tětiva	k1gFnSc1	tětiva
zachycena	zachytit	k5eAaPmNgFnS	zachytit
palcem	palec	k1gInSc7	palec
opatřeným	opatřený	k2eAgInSc7d1	opatřený
lučištnickým	lučištnický	k2eAgInSc7d1	lučištnický
prstenem	prsten	k1gInSc7	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
spuštění	spuštění	k1gNnSc6	spuštění
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
tětiva	tětiva	k1gFnSc1	tětiva
šíp	šíp	k1gInSc4	šíp
do	do	k7c2	do
oblouku	oblouk	k1gInSc2	oblouk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
okolo	okolo	k7c2	okolo
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
<g/>
Šíp	šíp	k1gInSc1	šíp
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vypuštěn	vypustit	k5eAaPmNgMnS	vypustit
(	(	kIx(	(
<g/>
u	u	k7c2	u
sportovních	sportovní	k2eAgInPc2d1	sportovní
luků	luk	k1gInPc2	luk
a	a	k8xC	a
u	u	k7c2	u
praváka	pravák	k1gMnSc2	pravák
<g/>
)	)	kIx)	)
vlevo	vlevo	k6eAd1	vlevo
od	od	k7c2	od
záměrné	záměrná	k1gFnSc2	záměrná
mírně	mírně	k6eAd1	mírně
bokem	bokem	k6eAd1	bokem
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
dalším	další	k2eAgInSc6d1	další
letu	let	k1gInSc6	let
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
srovnání	srovnání	k1gNnSc3	srovnání
(	(	kIx(	(
<g/>
lučištnický	lučištnický	k2eAgInSc1d1	lučištnický
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíp	Šíp	k1gMnSc1	Šíp
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
za	za	k7c2	za
letu	let	k1gInSc2	let
chvěje	chvěj	k1gInSc2	chvěj
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
šíp	šíp	k1gInSc1	šíp
příliš	příliš	k6eAd1	příliš
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
(	(	kIx(	(
<g/>
neohebný	ohebný	k2eNgMnSc1d1	neohebný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
veden	vést	k5eAaImNgMnS	vést
okolo	okolo	k7c2	okolo
luku	luk	k1gInSc2	luk
a	a	k8xC	a
"	"	kIx"	"
<g/>
odskakuje	odskakovat	k5eAaImIp3nS	odskakovat
<g/>
"	"	kIx"	"
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
měkký	měkký	k2eAgInSc1d1	měkký
šíp	šíp	k1gInSc1	šíp
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
omotává	omotávat	k5eAaImIp3nS	omotávat
<g/>
"	"	kIx"	"
okolo	okolo	k7c2	okolo
luku	luk	k1gInSc2	luk
a	a	k8xC	a
drhne	drhnout	k5eAaImIp3nS	drhnout
<g/>
,	,	kIx,	,
chová	chovat	k5eAaImIp3nS	chovat
nepředvídatelně	předvídatelně	k6eNd1	předvídatelně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
zlomí	zlomit	k5eAaPmIp3nS	zlomit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pravěk	pravěk	k1gInSc4	pravěk
===	===	k?	===
</s>
</p>
<p>
<s>
Vynález	vynález	k1gInSc1	vynález
luku	luk	k1gInSc2	luk
a	a	k8xC	a
šípu	šíp	k1gInSc2	šíp
spadá	spadat	k5eAaPmIp3nS	spadat
již	již	k6eAd1	již
do	do	k7c2	do
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
tak	tak	k9	tak
stalo	stát	k5eAaPmAgNnS	stát
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
období	období	k1gNnSc6	období
před	před	k7c7	před
40	[number]	k4	40
000	[number]	k4	000
-	-	kIx~	-
50	[number]	k4	50
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
vědecké	vědecký	k2eAgFnPc1d1	vědecká
hypotézy	hypotéza	k1gFnPc1	hypotéza
však	však	k9	však
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vynález	vynález	k1gInSc1	vynález
luku	luk	k1gInSc2	luk
není	být	k5eNaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
20	[number]	k4	20
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
kamenných	kamenný	k2eAgInPc2d1	kamenný
či	či	k8xC	či
kostěných	kostěný	k2eAgInPc2d1	kostěný
hrotů	hrot	k1gInPc2	hrot
totiž	totiž	k9	totiž
často	často	k6eAd1	často
nelze	lze	k6eNd1	lze
přesně	přesně	k6eAd1	přesně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
šíp	šíp	k1gInSc4	šíp
nebo	nebo	k8xC	nebo
lehký	lehký	k2eAgInSc4d1	lehký
oštěp	oštěp	k1gInSc4	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
objev	objev	k1gInSc1	objev
luku	luk	k1gInSc2	luk
a	a	k8xC	a
šípu	šíp	k1gInSc2	šíp
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgInPc3d3	nejvýznamnější
vynálezům	vynález	k1gInPc3	vynález
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
do	do	k7c2	do
významu	význam	k1gInSc2	význam
a	a	k8xC	a
stáří	stáří	k1gNnSc2	stáří
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
řazen	řadit	k5eAaImNgInS	řadit
na	na	k7c4	na
přední	přední	k2eAgNnSc4d1	přední
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c4	za
objev	objev	k1gInSc4	objev
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
používání	používání	k1gNnSc2	používání
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
kola	kola	k1gFnSc1	kola
případně	případně	k6eAd1	případně
provazu	provaz	k1gInSc2	provaz
bez	bez	k7c2	bez
kterého	který	k3yIgInSc2	který
byste	by	kYmCp2nP	by
luk	louka	k1gFnPc2	louka
nezhotovili	zhotovit	k5eNaPmAgMnP	zhotovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
C.	C.	kA	C.
W.	W.	kA	W.
Ceram	Ceram	k1gInSc4	Ceram
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
První	první	k4xOgMnSc1	první
Američan	Američan	k1gMnSc1	Američan
<g/>
"	"	kIx"	"
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentu	kontinent	k1gInSc6	kontinent
se	se	k3xPyFc4	se
znalost	znalost	k1gFnSc1	znalost
luku	luk	k1gInSc2	luk
a	a	k8xC	a
šípu	šíp	k1gInSc2	šíp
objevila	objevit	k5eAaPmAgFnS	objevit
až	až	k9	až
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
asi	asi	k9	asi
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Inuitů	Inuita	k1gMnPc2	Inuita
a	a	k8xC	a
Aleutů	Aleut	k1gMnPc2	Aleut
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
První	první	k4xOgMnPc1	první
Američané	Američan	k1gMnPc1	Američan
<g/>
"	"	kIx"	"
totiž	totiž	k9	totiž
přišli	přijít	k5eAaPmAgMnP	přijít
přes	přes	k7c4	přes
Beringův	Beringův	k2eAgInSc4d1	Beringův
průliv	průliv	k1gInSc4	průliv
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
již	již	k6eAd1	již
před	před	k7c7	před
cca	cca	kA	cca
40	[number]	k4	40
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
luk	luk	k1gInSc1	luk
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Asii	Asie	k1gFnSc6	Asie
ještě	ještě	k6eAd1	ještě
používán	používat	k5eAaImNgInS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
oštěp	oštěp	k1gInSc1	oštěp
<g/>
,	,	kIx,	,
vrhaný	vrhaný	k2eAgInSc1d1	vrhaný
pomocí	pomocí	k7c2	pomocí
vrhacího	vrhací	k2eAgNnSc2d1	vrhací
prkénka	prkénko	k1gNnSc2	prkénko
atlatlu	atlatl	k1gInSc2	atlatl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
luky	luk	k1gInPc7	luk
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
před	před	k7c7	před
10	[number]	k4	10
000	[number]	k4	000
-	-	kIx~	-
20	[number]	k4	20
000	[number]	k4	000
lety	let	k1gInPc7	let
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
migrační	migrační	k2eAgFnSc7d1	migrační
vlnou	vlna	k1gFnSc7	vlna
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nálezech	nález	k1gInPc6	nález
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
již	již	k6eAd1	již
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
drobné	drobný	k2eAgInPc4d1	drobný
pazourkové	pazourkový	k2eAgInPc4d1	pazourkový
i	i	k8xC	i
kostěné	kostěný	k2eAgInPc4d1	kostěný
hroty	hrot	k1gInPc4	hrot
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
tvarově	tvarově	k6eAd1	tvarově
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
dřívějších	dřívější	k2eAgInPc2d1	dřívější
hrotů	hrot	k1gInPc2	hrot
oštěpů	oštěp	k1gInPc2	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jeskynních	jeskynní	k2eAgFnPc2d1	jeskynní
maleb	malba	k1gFnPc2	malba
lze	lze	k6eAd1	lze
usuzovat	usuzovat	k5eAaImF	usuzovat
i	i	k9	i
na	na	k7c4	na
hroty	hrot	k1gInPc4	hrot
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
tvrzené	tvrzený	k2eAgInPc1d1	tvrzený
nad	nad	k7c7	nad
ohněm	oheň	k1gInSc7	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezené	omezený	k2eAgFnSc3d1	omezená
trvanlivosti	trvanlivost	k1gFnSc3	trvanlivost
dřeva	dřevo	k1gNnSc2	dřevo
je	být	k5eAaImIp3nS	být
však	však	k9	však
nelze	lze	k6eNd1	lze
dokumentovat	dokumentovat	k5eAaBmF	dokumentovat
archeologicky	archeologicky	k6eAd1	archeologicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
přínos	přínos	k1gInSc1	přínos
luku	luk	k1gInSc2	luk
pro	pro	k7c4	pro
prehistorické	prehistorický	k2eAgMnPc4d1	prehistorický
lovce	lovec	k1gMnPc4	lovec
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
možnosti	možnost	k1gFnPc4	možnost
překonat	překonat	k5eAaPmF	překonat
únikovou	únikový	k2eAgFnSc4d1	úniková
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kopytníků	kopytník	k1gMnPc2	kopytník
<g/>
.	.	kIx.	.
</s>
<s>
Šíp	Šíp	k1gMnSc1	Šíp
nemusel	muset	k5eNaImAgMnS	muset
zvíře	zvíře	k1gNnSc4	zvíře
usmrtit	usmrtit	k5eAaPmF	usmrtit
ihned	ihned	k6eAd1	ihned
<g/>
,	,	kIx,	,
stačilo	stačit	k5eAaBmAgNnS	stačit
jej	on	k3xPp3gMnSc4	on
zranit	zranit	k5eAaPmF	zranit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
snáz	snadno	k6eAd2	snadno
dostihnout	dostihnout	k5eAaPmF	dostihnout
a	a	k8xC	a
dobít	dobít	k5eAaPmF	dobít
např.	např.	kA	např.
oštěpem	oštěp	k1gInSc7	oštěp
nebo	nebo	k8xC	nebo
kyjem	kyj	k1gInSc7	kyj
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stopovat	stopovat	k5eAaImF	stopovat
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
uštvat	uštvat	k5eAaPmF	uštvat
do	do	k7c2	do
úhynu	úhyn	k1gInSc2	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
skalní	skalní	k2eAgFnPc1d1	skalní
a	a	k8xC	a
jeskynní	jeskynní	k2eAgFnPc1d1	jeskynní
malby	malba	k1gFnPc1	malba
na	na	k7c6	na
území	území	k1gNnSc6	území
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Hlinková	hlinkový	k2eAgFnSc1d1	hlinková
malba	malba	k1gFnSc1	malba
paleolitického	paleolitický	k2eAgMnSc2d1	paleolitický
umělce	umělec	k1gMnSc2	umělec
v	v	k7c6	v
jeskyni	jeskyně	k1gFnSc6	jeskyně
Cueva	Cuev	k1gMnSc2	Cuev
de	de	k?	de
los	los	k1gMnSc1	los
Caballos	Caballos	k1gMnSc1	Caballos
u	u	k7c2	u
Albocareru	Albocarer	k1gInSc2	Albocarer
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
"	"	kIx"	"
<g/>
Hon	hon	k1gInSc1	hon
na	na	k7c4	na
jeleny	jelena	k1gFnPc4	jelena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
způsob	způsob	k1gInSc1	způsob
lovi	lovi	k6eAd1	lovi
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
lukostřelbu	lukostřelba	k1gFnSc4	lukostřelba
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
dosahovaly	dosahovat	k5eAaImAgFnP	dosahovat
délky	délka	k1gFnPc1	délka
asi	asi	k9	asi
200	[number]	k4	200
cm	cm	kA	cm
<g/>
,	,	kIx,	,
šípy	šíp	k1gInPc7	šíp
měřily	měřit	k5eAaImAgFnP	měřit
asi	asi	k9	asi
100	[number]	k4	100
cm	cm	kA	cm
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
měly	mít	k5eAaImAgInP	mít
hroty	hrot	k1gInPc1	hrot
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
ostře	ostro	k6eAd1	ostro
seříznutého	seříznutý	k2eAgNnSc2d1	seříznuté
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
dostřel	dostřel	k1gInSc1	dostřel
byl	být	k5eAaImAgInS	být
dost	dost	k6eAd1	dost
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
lovci	lovec	k1gMnPc1	lovec
nepotřebovali	potřebovat	k5eNaImAgMnP	potřebovat
velký	velký	k2eAgInSc4d1	velký
dostřel	dostřel	k1gInSc4	dostřel
ani	ani	k8xC	ani
přesnost	přesnost	k1gFnSc4	přesnost
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc4	kořist
často	často	k6eAd1	často
obstoupili	obstoupit	k5eAaPmAgMnP	obstoupit
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
ohně	oheň	k1gInSc2	oheň
zahnali	zahnat	k5eAaPmAgMnP	zahnat
do	do	k7c2	do
přirozené	přirozený	k2eAgFnSc2d1	přirozená
pasti	past	k1gFnSc2	past
nebo	nebo	k8xC	nebo
uměle	uměle	k6eAd1	uměle
vykopané	vykopaný	k2eAgFnPc4d1	vykopaná
jámy	jáma	k1gFnPc4	jáma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
usmrtili	usmrtit	k5eAaPmAgMnP	usmrtit
<g/>
,	,	kIx,	,
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
oštěpy	oštěp	k1gInPc4	oštěp
a	a	k8xC	a
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
lovci	lovec	k1gMnPc1	lovec
usmrtili	usmrtit	k5eAaPmAgMnP	usmrtit
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
než	než	k8xS	než
mohli	moct	k5eAaImAgMnP	moct
spotřebovat	spotřebovat	k5eAaPmF	spotřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
vybíjeli	vybíjet	k5eAaImAgMnP	vybíjet
celá	celý	k2eAgNnPc4d1	celé
stáda	stádo	k1gNnPc4	stádo
např.	např.	kA	např.
bizonů	bizon	k1gMnPc2	bizon
<g/>
,	,	kIx,	,
divokých	divoký	k2eAgMnPc2d1	divoký
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
sobů	sob	k1gMnPc2	sob
nebo	nebo	k8xC	nebo
jelenů	jelen	k1gMnPc2	jelen
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
zahnali	zahnat	k5eAaPmAgMnP	zahnat
pomocí	pomocí	k7c2	pomocí
ohně	oheň	k1gInSc2	oheň
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
nebo	nebo	k8xC	nebo
na	na	k7c4	na
kraj	kraj	k1gInSc4	kraj
skalního	skalní	k2eAgInSc2d1	skalní
útesu	útes	k1gInSc2	útes
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
lovu	lov	k1gInSc2	lov
mohl	moct	k5eAaImAgInS	moct
podle	podle	k7c2	podle
vědců	vědec	k1gMnPc2	vědec
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
vyhubení	vyhubení	k1gNnSc3	vyhubení
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
zbraně	zbraň	k1gFnPc1	zbraň
zdokonaleny	zdokonalit	k5eAaPmNgFnP	zdokonalit
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
bezohledné	bezohledný	k2eAgInPc1d1	bezohledný
způsoby	způsob	k1gInPc1	způsob
lovu	lov	k1gInSc2	lov
se	se	k3xPyFc4	se
již	již	k6eAd1	již
neprováděly	provádět	k5eNaImAgFnP	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
paradoxem	paradoxon	k1gNnSc7	paradoxon
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yInSc7	co
byly	být	k5eAaImAgInP	být
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
lovu	lov	k1gInSc2	lov
dokonalejší	dokonalý	k2eAgNnSc1d2	dokonalejší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
méně	málo	k6eAd2	málo
zvířat	zvíře	k1gNnPc2	zvíře
lovci	lovec	k1gMnPc1	lovec
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
hypotéz	hypotéza	k1gFnPc2	hypotéza
<g/>
,	,	kIx,	,
snažících	snažící	k2eAgFnPc2d1	snažící
se	se	k3xPyFc4	se
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vynález	vynález	k1gInSc4	vynález
luku	luk	k1gInSc2	luk
pomocí	pomocí	k7c2	pomocí
aplikace	aplikace	k1gFnSc2	aplikace
náhodného	náhodný	k2eAgInSc2d1	náhodný
objevu	objev	k1gInSc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
údajů	údaj	k1gInPc2	údaj
byl	být	k5eAaImAgInS	být
luk	luk	k1gInSc1	luk
nejprve	nejprve	k6eAd1	nejprve
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
rozdělávání	rozdělávání	k1gNnSc3	rozdělávání
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
vrtání	vrtání	k1gNnSc2	vrtání
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
hudební	hudební	k2eAgInSc4d1	hudební
nástroj	nástroj	k1gInSc4	nástroj
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
náhodou	náhodou	k6eAd1	náhodou
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
síla	síla	k1gFnSc1	síla
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
i	i	k9	i
k	k	k7c3	k
vymrštění	vymrštění	k1gNnSc3	vymrštění
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
dobře	dobře	k6eAd1	dobře
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
vynálezu	vynález	k1gInSc3	vynález
luku	luk	k1gInSc2	luk
došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
všimli	všimnout	k5eAaPmAgMnP	všimnout
síly	síla	k1gFnPc4	síla
ohnutého	ohnutý	k2eAgMnSc2d1	ohnutý
a	a	k8xC	a
náhle	náhle	k6eAd1	náhle
narovnaného	narovnaný	k2eAgInSc2d1	narovnaný
stromku	stromek	k1gInSc2	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc4	spojení
dvou	dva	k4xCgInPc2	dva
konců	konec	k1gInPc2	konec
pružného	pružný	k2eAgNnSc2d1	pružné
dřeva	dřevo	k1gNnSc2	dřevo
napnutým	napnutý	k2eAgNnSc7d1	napnuté
vláknem	vlákno	k1gNnSc7	vlákno
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
byl	být	k5eAaImAgInS	být
přiložen	přiložen	k2eAgInSc1d1	přiložen
lehký	lehký	k2eAgInSc1d1	lehký
oštěp	oštěp	k1gInSc1	oštěp
<g/>
,	,	kIx,	,
jej	on	k3xPp3gNnSc4	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vymrštit	vymrštit	k5eAaPmF	vymrštit
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
přesností	přesnost	k1gFnSc7	přesnost
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
používaný	používaný	k2eAgInSc1d1	používaný
vrhač	vrhač	k1gInSc1	vrhač
atlatl	atlatnout	k5eAaPmAgInS	atlatnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Starověk	starověk	k1gInSc4	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2	[number]	k4	2
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
pozdního	pozdní	k2eAgInSc2d1	pozdní
neolitu	neolit	k1gInSc2	neolit
a	a	k8xC	a
počátků	počátek	k1gInPc2	počátek
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnPc4d1	bronzová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
výroba	výroba	k1gFnSc1	výroba
luků	luk	k1gInPc2	luk
a	a	k8xC	a
šípů	šíp	k1gInPc2	šíp
velmi	velmi	k6eAd1	velmi
zdokonalila	zdokonalit	k5eAaPmAgFnS	zdokonalit
<g/>
.	.	kIx.	.
</s>
<s>
Přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zejména	zejména	k9	zejména
objev	objev	k1gInSc1	objev
metalurgie	metalurgie	k1gFnSc2	metalurgie
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc4d1	umožňující
výrobu	výroba	k1gFnSc4	výroba
měděných	měděný	k2eAgFnPc2d1	měděná
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
bronzových	bronzový	k2eAgInPc2d1	bronzový
hrotů	hrot	k1gInPc2	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
a	a	k8xC	a
šíp	šíp	k1gInSc1	šíp
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nástroji	nástroj	k1gInPc7	nástroj
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgInPc1d1	hrající
prvořadou	prvořadý	k2eAgFnSc4d1	prvořadá
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Lučištníci	lučištník	k1gMnPc1	lučištník
byli	být	k5eAaImAgMnP	být
pokládáni	pokládat	k5eAaImNgMnP	pokládat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
starověkých	starověký	k2eAgFnPc2d1	starověká
civilizací	civilizace	k1gFnPc2	civilizace
za	za	k7c4	za
elitní	elitní	k2eAgFnSc4d1	elitní
složku	složka	k1gFnSc4	složka
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
luk	luk	k1gInSc1	luk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
symbolem	symbol	k1gInSc7	symbol
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
největší	veliký	k2eAgNnSc1d3	veliký
procento	procento	k1gNnSc1	procento
padlých	padlý	k1gMnPc2	padlý
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
celé	celý	k2eAgFnSc2d1	celá
historie	historie	k1gFnSc2	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
právě	právě	k9	právě
lučištníci	lučištník	k1gMnPc1	lučištník
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Blízký	blízký	k2eAgInSc4d1	blízký
východ	východ	k1gInSc4	východ
====	====	k?	====
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc4d3	nejstarší
obrazové	obrazový	k2eAgInPc4d1	obrazový
a	a	k8xC	a
písemné	písemný	k2eAgInPc4d1	písemný
doklady	doklad	k1gInPc4	doklad
o	o	k7c4	o
použití	použití	k1gNnSc4	použití
luku	luk	k1gInSc2	luk
ve	v	k7c6	v
válkách	válka	k1gFnPc6	válka
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
a	a	k8xC	a
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
Sumerové	Sumer	k1gMnPc1	Sumer
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ještě	ještě	k6eAd1	ještě
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
oštěpům	oštěp	k1gInPc3	oštěp
<g/>
,	,	kIx,	,
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
<g/>
,	,	kIx,	,
Asyřané	Asyřan	k1gMnPc1	Asyřan
<g/>
,	,	kIx,	,
Chetité	Chetita	k1gMnPc1	Chetita
a	a	k8xC	a
Churrité	Churrita	k1gMnPc1	Churrita
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
již	již	k6eAd1	již
stavěli	stavět	k5eAaImAgMnP	stavět
válečnou	válečný	k2eAgFnSc4d1	válečná
taktiku	taktika	k1gFnSc4	taktika
především	především	k9	především
na	na	k7c4	na
používání	používání	k1gNnSc4	používání
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Lučištníci	lučištník	k1gMnPc1	lučištník
bojovali	bojovat	k5eAaImAgMnP	bojovat
buďto	buďto	k8xC	buďto
jako	jako	k9	jako
pěšáci	pěšák	k1gMnPc1	pěšák
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
vozech	vůz	k1gInPc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Asyrští	asyrský	k2eAgMnPc1d1	asyrský
a	a	k8xC	a
babylónští	babylónský	k2eAgMnPc1d1	babylónský
lučištníci	lučištník	k1gMnPc1	lučištník
stříleli	střílet	k5eAaImAgMnP	střílet
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
jízdě	jízda	k1gFnSc6	jízda
z	z	k7c2	z
dvoukolových	dvoukolový	k2eAgInPc2d1	dvoukolový
válečných	válečný	k2eAgInPc2d1	válečný
vozů	vůz	k1gInPc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Šípy	Šíp	k1gMnPc4	Šíp
ukládali	ukládat	k5eAaImAgMnP	ukládat
po	po	k7c6	po
dvaceti	dvacet	k4xCc6	dvacet
kusech	kus	k1gInPc6	kus
do	do	k7c2	do
toulců	toulec	k1gInPc2	toulec
<g/>
,	,	kIx,	,
zavěšených	zavěšený	k2eAgNnPc2d1	zavěšené
řemením	řemení	k1gNnSc7	řemení
na	na	k7c6	na
rameni	rameno	k1gNnSc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
a	a	k8xC	a
šípy	šíp	k1gInPc1	šíp
králů	král	k1gMnPc2	král
a	a	k8xC	a
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
byly	být	k5eAaImAgFnP	být
bohatě	bohatě	k6eAd1	bohatě
zdobeny	zdobit	k5eAaImNgInP	zdobit
slonovinou	slonovina	k1gFnSc7	slonovina
<g/>
,	,	kIx,	,
zlatem	zlato	k1gNnSc7	zlato
a	a	k8xC	a
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Jízdní	jízdní	k2eAgMnPc1d1	jízdní
lučištníci	lučištník	k1gMnPc1	lučištník
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
Churritů	Churrit	k1gInPc2	Churrit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mezopotámské	Mezopotámský	k2eAgInPc1d1	Mezopotámský
luky	luk	k1gInPc1	luk
měly	mít	k5eAaImAgInP	mít
podle	podle	k7c2	podle
obrazových	obrazový	k2eAgInPc2d1	obrazový
dokladů	doklad	k1gInPc2	doklad
často	často	k6eAd1	často
nezvykle	zvykle	k6eNd1	zvykle
trojúhelníkový	trojúhelníkový	k2eAgInSc4d1	trojúhelníkový
tvar	tvar	k1gInSc4	tvar
lučiště	lučiště	k1gNnSc2	lučiště
<g/>
.	.	kIx.	.
</s>
<s>
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
a	a	k8xC	a
Asyřané	Asyřan	k1gMnPc1	Asyřan
používali	používat	k5eAaImAgMnP	používat
luky	luk	k1gInPc4	luk
také	také	k9	také
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
.	.	kIx.	.
</s>
<s>
Lovili	lovit	k5eAaImAgMnP	lovit
jimi	on	k3xPp3gInPc7	on
nejen	nejen	k6eAd1	nejen
gazely	gazela	k1gFnPc1	gazela
a	a	k8xC	a
divoké	divoký	k2eAgMnPc4d1	divoký
osly	osel	k1gMnPc4	osel
<g/>
,	,	kIx,	,
i	i	k8xC	i
tak	tak	k6eAd1	tak
silná	silný	k2eAgNnPc1d1	silné
a	a	k8xC	a
nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
lvy	lev	k1gMnPc4	lev
a	a	k8xC	a
divoké	divoký	k2eAgMnPc4d1	divoký
tury	tur	k1gMnPc4	tur
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
reliéfy	reliéf	k1gInPc1	reliéf
z	z	k7c2	z
paláce	palác	k1gInSc2	palác
asyrského	asyrský	k2eAgMnSc2d1	asyrský
krále	král	k1gMnSc2	král
Aššurbanipala	Aššurbanipal	k1gMnSc2	Aššurbanipal
(	(	kIx(	(
<g/>
668	[number]	k4	668
–	–	k?	–
630	[number]	k4	630
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zobrazující	zobrazující	k2eAgMnSc1d1	zobrazující
panovníka	panovník	k1gMnSc2	panovník
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
zvěře	zvěř	k1gFnSc2	zvěř
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
používali	používat	k5eAaImAgMnP	používat
luk	luk	k1gInSc4	luk
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
i	i	k8xC	i
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
egyptští	egyptský	k2eAgMnPc1d1	egyptský
lučištníci	lučištník	k1gMnPc1	lučištník
bojovali	bojovat	k5eAaImAgMnP	bojovat
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
vozech	vůz	k1gInPc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Pěší	pěší	k2eAgMnPc4d1	pěší
lučištníky	lučištník	k1gMnPc4	lučištník
v	v	k7c6	v
egyptském	egyptský	k2eAgNnSc6d1	egyptské
vojsku	vojsko	k1gNnSc6	vojsko
reprezentovali	reprezentovat	k5eAaImAgMnP	reprezentovat
především	především	k9	především
Núbijci	Núbijce	k1gMnPc1	Núbijce
a	a	k8xC	a
Libyjci	Libyjec	k1gMnPc1	Libyjec
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
lučištníkem	lučištník	k1gMnSc7	lučištník
byl	být	k5eAaImAgMnS	být
faraon	faraon	k1gMnSc1	faraon
Amenhotep	Amenhotep	k1gMnSc1	Amenhotep
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
podle	podle	k7c2	podle
písemných	písemný	k2eAgInPc2d1	písemný
záznamů	záznam	k1gInPc2	záznam
střílel	střílet	k5eAaImAgMnS	střílet
šípy	šíp	k1gInPc4	šíp
skrz	skrz	k7c4	skrz
silné	silný	k2eAgFnPc4d1	silná
měděné	měděný	k2eAgFnPc4d1	měděná
terče	terč	k1gFnPc4	terč
z	z	k7c2	z
rychle	rychle	k6eAd1	rychle
jedoucího	jedoucí	k2eAgInSc2d1	jedoucí
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Ramesse	Ramesse	k1gFnSc1	Ramesse
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
vynikající	vynikající	k2eAgMnSc1d1	vynikající
lučištník	lučištník	k1gMnSc1	lučištník
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
zobrazeních	zobrazení	k1gNnPc6	zobrazení
lukem	luk	k1gInSc7	luk
a	a	k8xC	a
šípy	šíp	k1gInPc1	šíp
poráží	porážet	k5eAaImIp3nP	porážet
své	svůj	k3xOyFgMnPc4	svůj
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Egyptské	egyptský	k2eAgInPc1d1	egyptský
luky	luk	k1gInPc1	luk
měly	mít	k5eAaImAgInP	mít
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
<g/>
,	,	kIx,	,
obloukovitý	obloukovitý	k2eAgInSc4d1	obloukovitý
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
poměrně	poměrně	k6eAd1	poměrně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Písemným	písemný	k2eAgInSc7d1	písemný
dokladem	doklad	k1gInSc7	doklad
o	o	k7c6	o
používání	používání	k1gNnSc6	používání
luku	luk	k1gInSc2	luk
je	být	k5eAaImIp3nS	být
i	i	k9	i
biblický	biblický	k2eAgInSc1d1	biblický
Starý	starý	k2eAgInSc1d1	starý
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
můžeme	moct	k5eAaImIp1nP	moct
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
číst	číst	k5eAaImF	číst
o	o	k7c6	o
luku	luk	k1gInSc6	luk
<g/>
,	,	kIx,	,
o	o	k7c6	o
střelbě	střelba	k1gFnSc6	střelba
z	z	k7c2	z
luku	luk	k1gInSc2	luk
a	a	k8xC	a
o	o	k7c6	o
šípech	šíp	k1gInPc6	šíp
a	a	k8xC	a
toulcích	toulec	k1gInPc6	toulec
<g/>
,	,	kIx,	,
známým	známý	k2eAgMnSc7d1	známý
biblickým	biblický	k2eAgMnSc7d1	biblický
lučišníkem	lučišník	k1gMnSc7	lučišník
byl	být	k5eAaImAgMnS	být
Jónatán	Jónatán	k1gMnSc1	Jónatán
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Saula	Saul	k1gMnSc2	Saul
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hrdina	hrdina	k1gMnSc1	hrdina
Jozue	Jozue	k1gMnSc1	Jozue
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obávanými	obávaný	k2eAgMnPc7d1	obávaný
lučištníky	lučištník	k1gMnPc7	lučištník
byli	být	k5eAaImAgMnP	být
Médové	Méd	k1gMnPc1	Méd
a	a	k8xC	a
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
podle	podle	k7c2	podle
Hérodota	Hérodot	k1gMnSc2	Hérodot
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
"	"	kIx"	"
<g/>
svými	svůj	k3xOyFgInPc7	svůj
šípy	šíp	k1gInPc7	šíp
zastiňovali	zastiňovat	k5eAaImAgMnP	zastiňovat
Slunce	slunce	k1gNnSc2	slunce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Persii	Persie	k1gFnSc6	Persie
byla	být	k5eAaImAgFnS	být
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
národním	národní	k2eAgInSc7d1	národní
sportem	sport	k1gInSc7	sport
a	a	k8xC	a
luk	luk	k1gInSc1	luk
byl	být	k5eAaImAgInS	být
atributem	atribut	k1gInSc7	atribut
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
reprezentujícím	reprezentující	k2eAgMnPc3d1	reprezentující
jeho	jeho	k3xOp3gFnSc4	jeho
úlohu	úloha	k1gFnSc4	úloha
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
zlu	zlo	k1gNnSc3	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
krále	král	k1gMnSc2	král
Dáreia	Dáreium	k1gNnSc2	Dáreium
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
panovník	panovník	k1gMnSc1	panovník
zobrazen	zobrazit	k5eAaPmNgMnS	zobrazit
jako	jako	k9	jako
lučištník	lučištník	k1gMnSc1	lučištník
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Kambýsés	Kambýsésa	k1gFnPc2	Kambýsésa
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
tak	tak	k6eAd1	tak
dobrý	dobrý	k2eAgMnSc1d1	dobrý
lučištník	lučištník	k1gMnSc1	lučištník
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zastřelit	zastřelit	k5eAaPmF	zastřelit
člověka	člověk	k1gMnSc4	člověk
přesnou	přesný	k2eAgFnSc7d1	přesná
ranou	rána	k1gFnSc7	rána
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
i	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
opilý	opilý	k2eAgInSc1d1	opilý
<g/>
.	.	kIx.	.
</s>
<s>
Perské	perský	k2eAgInPc1d1	perský
luky	luk	k1gInPc1	luk
byly	být	k5eAaImAgInP	být
podle	podle	k7c2	podle
obrazového	obrazový	k2eAgInSc2d1	obrazový
materiálu	materiál	k1gInSc2	materiál
reflexní	reflexní	k2eAgNnSc1d1	reflexní
(	(	kIx(	(
<g/>
dvakrát	dvakrát	k6eAd1	dvakrát
prohnuté	prohnutý	k2eAgNnSc1d1	prohnuté
<g/>
)	)	kIx)	)
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
.	.	kIx.	.
</s>
<s>
Perští	perský	k2eAgMnPc1d1	perský
lučištníci	lučištník	k1gMnPc1	lučištník
bojovali	bojovat	k5eAaImAgMnP	bojovat
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xC	jako
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
zbroj	zbroj	k1gFnSc4	zbroj
(	(	kIx(	(
<g/>
nanejvýš	nanejvýš	k6eAd1	nanejvýš
přilbu	přilba	k1gFnSc4	přilba
<g/>
)	)	kIx)	)
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
velké	velký	k2eAgInPc4d1	velký
obdélníkové	obdélníkový	k2eAgInPc4d1	obdélníkový
štíty	štít	k1gInPc4	štít
z	z	k7c2	z
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
desek	deska	k1gFnPc2	deska
nebo	nebo	k8xC	nebo
proutí	proutí	k1gNnSc2	proutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rovněž	rovněž	k9	rovněž
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Indii	Indie	k1gFnSc6	Indie
má	můj	k3xOp1gFnSc1	můj
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
dávnou	dávný	k2eAgFnSc4d1	dávná
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Véd	véda	k1gFnPc2	véda
byli	být	k5eAaImAgMnP	být
indoevropští	indoevropský	k2eAgMnPc1d1	indoevropský
Árjové	Árjus	k1gMnPc1	Árjus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
obsadili	obsadit	k5eAaPmAgMnP	obsadit
severní	severní	k2eAgFnSc4d1	severní
Indii	Indie	k1gFnSc4	Indie
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
lučištníci	lučištník	k1gMnPc1	lučištník
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
vozech	vůz	k1gInPc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k1gNnSc4	málo
mladší	mladý	k2eAgFnSc2d2	mladší
(	(	kIx(	(
<g/>
asi	asi	k9	asi
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
učebnice	učebnice	k1gFnSc1	učebnice
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
Dhanuršástra	Dhanuršástra	k1gFnSc1	Dhanuršástra
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgInPc4d1	velký
indické	indický	k2eAgInPc4d1	indický
eposy	epos	k1gInPc4	epos
Rámájana	Rámájana	k1gFnSc1	Rámájana
a	a	k8xC	a
Mahábhárata	Mahábhárata	k1gFnSc1	Mahábhárata
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgInPc1d1	plný
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
lucích	luk	k1gInPc6	luk
a	a	k8xC	a
šípech	šíp	k1gInPc6	šíp
<g/>
,	,	kIx,	,
střeleckých	střelecký	k2eAgFnPc6d1	střelecká
soutěžích	soutěž	k1gFnPc6	soutěž
a	a	k8xC	a
bitvách	bitva	k1gFnPc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Hrdinové	Hrdinová	k1gFnPc1	Hrdinová
těchto	tento	k3xDgInPc2	tento
eposů	epos	k1gInPc2	epos
je	být	k5eAaImIp3nS	být
Ráma	Ráma	k1gMnSc1	Ráma
a	a	k8xC	a
Ardžuna	Ardžuna	k1gFnSc1	Ardžuna
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
nadlidské	nadlidský	k2eAgFnPc4d1	nadlidská
schopnosti	schopnost	k1gFnPc4	schopnost
ilustrují	ilustrovat	k5eAaBmIp3nP	ilustrovat
eposy	epos	k1gInPc1	epos
právě	právě	k6eAd1	právě
dokonalým	dokonalý	k2eAgNnSc7d1	dokonalé
ovládáním	ovládání	k1gNnSc7	ovládání
luku	luk	k1gInSc2	luk
a	a	k8xC	a
šípů	šíp	k1gInPc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Indické	indický	k2eAgInPc1d1	indický
luky	luk	k1gInPc1	luk
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
z	z	k7c2	z
bambusu	bambus	k1gInSc2	bambus
nebo	nebo	k8xC	nebo
z	z	k7c2	z
palmového	palmový	k2eAgNnSc2d1	palmové
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
značně	značně	k6eAd1	značně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
180	[number]	k4	180
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Indičtí	indický	k2eAgMnPc1d1	indický
lučištníci	lučištník	k1gMnPc1	lučištník
bojovali	bojovat	k5eAaImAgMnP	bojovat
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
vozech	vůz	k1gInPc6	vůz
nebo	nebo	k8xC	nebo
slonech	slon	k1gMnPc6	slon
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
jako	jako	k8xS	jako
pěchota	pěchota	k1gFnSc1	pěchota
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
se	se	k3xPyFc4	se
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
o	o	k7c6	o
mnoha	mnoho	k4c6	mnoho
tvarech	tvar	k1gInPc6	tvar
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zdobených	zdobený	k2eAgFnPc2d1	zdobená
zlatem	zlato	k1gNnSc7	zlato
nebo	nebo	k8xC	nebo
drahokamy	drahokam	k1gInPc7	drahokam
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
luky	luk	k1gInPc1	luk
<g/>
,	,	kIx,	,
toulce	toulec	k1gInPc1	toulec
<g/>
,	,	kIx,	,
zbroj	zbroj	k1gFnSc4	zbroj
nebo	nebo	k8xC	nebo
koňské	koňský	k2eAgInPc4d1	koňský
postroje	postroj	k1gInPc4	postroj
jejich	jejich	k3xOp3gMnPc2	jejich
majitelů	majitel	k1gMnPc2	majitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
starověkých	starověký	k2eAgNnPc2d1	starověké
etnik	etnikum	k1gNnPc2	etnikum
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
luk	luk	k1gInSc4	luk
posvátným	posvátný	k2eAgInSc7d1	posvátný
symbolem	symbol	k1gInSc7	symbol
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
atributem	atribut	k1gInSc7	atribut
božstev	božstvo	k1gNnPc2	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
bůh	bůh	k1gMnSc1	bůh
Asyřanů	Asyřan	k1gMnPc2	Asyřan
Aššur	Aššur	k1gMnSc1	Aššur
<g/>
,	,	kIx,	,
pán	pán	k1gMnSc1	pán
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zpodobňován	zpodobňovat	k5eAaImNgMnS	zpodobňovat
jako	jako	k9	jako
válečník	válečník	k1gMnSc1	válečník
stojící	stojící	k2eAgMnSc1d1	stojící
uprostřed	uprostřed	k7c2	uprostřed
rozzářeného	rozzářený	k2eAgNnSc2d1	rozzářené
Slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
v	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Bohyně	bohyně	k1gFnSc1	bohyně
Ištar	Ištara	k1gFnPc2	Ištara
byla	být	k5eAaImAgFnS	být
znázorňována	znázorňovat	k5eAaImNgFnS	znázorňovat
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
připraveným	připravený	k2eAgInSc7d1	připravený
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
<g/>
,	,	kIx,	,
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
šípy	šíp	k1gInPc7	šíp
u	u	k7c2	u
tětivy	tětiva	k1gFnSc2	tětiva
<g/>
.	.	kIx.	.
</s>
<s>
Vzadu	vzadu	k6eAd1	vzadu
na	na	k7c6	na
rameni	rameno	k1gNnSc6	rameno
má	mít	k5eAaImIp3nS	mít
zavěšen	zavěšen	k2eAgInSc1d1	zavěšen
toulec	toulec	k1gInSc1	toulec
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
bohyni	bohyně	k1gFnSc4	bohyně
uctívali	uctívat	k5eAaImAgMnP	uctívat
též	též	k9	též
Babylóňané	Babylóňan	k1gMnPc1	Babylóňan
<g/>
.	.	kIx.	.
</s>
<s>
Babylónský	babylónský	k2eAgMnSc1d1	babylónský
bůh	bůh	k1gMnSc1	bůh
války	válka	k1gFnSc2	válka
a	a	k8xC	a
lovu	lov	k1gInSc2	lov
Ninurta	Ninurt	k1gMnSc2	Ninurt
(	(	kIx(	(
<g/>
zkomolením	zkomolení	k1gNnSc7	zkomolení
jeho	on	k3xPp3gNnSc2	on
jména	jméno	k1gNnSc2	jméno
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
biblický	biblický	k2eAgMnSc1d1	biblický
Nimrod	Nimrod	k1gMnSc1	Nimrod
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
rovněž	rovněž	k9	rovněž
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
a	a	k8xC	a
mečem	meč	k1gInSc7	meč
<g/>
.	.	kIx.	.
</s>
<s>
Indický	indický	k2eAgMnSc1d1	indický
bůh	bůh	k1gMnSc1	bůh
Ráma	Ráma	k1gMnSc1	Ráma
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
legendární	legendární	k2eAgMnSc1d1	legendární
princ	princ	k1gMnSc1	princ
<g/>
,	,	kIx,	,
pokládaný	pokládaný	k2eAgMnSc1d1	pokládaný
za	za	k7c4	za
vtělení	vtělení	k1gNnSc4	vtělení
boha	bůh	k1gMnSc2	bůh
Višnu	Višn	k1gInSc2	Višn
<g/>
)	)	kIx)	)
bývá	bývat	k5eAaImIp3nS	bývat
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
jako	jako	k8xC	jako
lučištník	lučištník	k1gMnSc1	lučištník
<g/>
,	,	kIx,	,
také	také	k9	také
bohyně	bohyně	k1gFnSc1	bohyně
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mateřství	mateřství	k1gNnSc2	mateřství
a	a	k8xC	a
ochrany	ochrana	k1gFnSc2	ochrana
Durga	Durga	k1gFnSc1	Durga
drží	držet	k5eAaImIp3nS	držet
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
osmi	osm	k4xCc6	osm
pažích	paže	k1gFnPc6	paže
kromě	kromě	k7c2	kromě
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
štítů	štít	k1gInPc2	štít
a	a	k8xC	a
mečů	meč	k1gInPc2	meč
také	také	k9	také
luk	luk	k1gInSc4	luk
a	a	k8xC	a
šíp	šíp	k1gInSc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Indický	indický	k2eAgMnSc1d1	indický
bůh	bůh	k1gMnSc1	bůh
lásky	láska	k1gFnSc2	láska
Káma	Káma	k1gMnSc1	Káma
podle	podle	k7c2	podle
Véd	véda	k1gFnPc2	véda
a	a	k8xC	a
Purán	Purán	k1gInSc1	Purán
používá	používat	k5eAaImIp3nS	používat
květinové	květinový	k2eAgInPc4d1	květinový
šípy	šíp	k1gInPc4	šíp
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
luk	luk	k1gInSc1	luk
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
tětivy	tětiva	k1gFnSc2	tětiva
má	mít	k5eAaImIp3nS	mít
roj	roj	k1gInSc1	roj
včel	včela	k1gFnPc2	včela
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
připomíná	připomínat	k5eAaImIp3nS	připomínat
antického	antický	k2eAgMnSc4d1	antický
Eróta	Erós	k1gMnSc4	Erós
a	a	k8xC	a
Kupida	Kupid	k1gMnSc4	Kupid
s	s	k7c7	s
jeho	jeho	k3xOp3gNnPc7	jeho
šípy	šíp	k1gInPc4	šíp
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Antika	antika	k1gFnSc1	antika
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
antické	antický	k2eAgMnPc4d1	antický
Řeky	Řek	k1gMnPc4	Řek
byla	být	k5eAaImAgFnS	být
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
hlavně	hlavně	k6eAd1	hlavně
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
sportem	sport	k1gInSc7	sport
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
disciplín	disciplína	k1gFnPc2	disciplína
starověkých	starověký	k2eAgFnPc2d1	starověká
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
se	se	k3xPyFc4	se
v	v	k7c6	v
antice	antika	k1gFnSc6	antika
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
rovněž	rovněž	k9	rovněž
při	při	k7c6	při
lovu	lov	k1gInSc6	lov
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
lovecké	lovecký	k2eAgInPc4d1	lovecký
spisy	spis	k1gInPc4	spis
Xenofóna	Xenofón	k1gMnSc4	Xenofón
či	či	k8xC	či
Flavia	Flavius	k1gMnSc4	Flavius
Arriána	Arrián	k1gMnSc4	Arrián
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
dávali	dávat	k5eAaImAgMnP	dávat
Řekové	Řek	k1gMnPc1	Řek
přednost	přednost	k1gFnSc4	přednost
boji	boj	k1gInSc6	boj
muže	muž	k1gMnSc2	muž
proti	proti	k7c3	proti
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInPc7	jejich
hlavními	hlavní	k2eAgInPc7d1	hlavní
zbraněmi	zbraň	k1gFnPc7	zbraň
staly	stát	k5eAaPmAgInP	stát
meče	meč	k1gInPc4	meč
a	a	k8xC	a
kopí	kopí	k1gNnPc4	kopí
<g/>
.	.	kIx.	.
</s>
<s>
Lehkoodění	Lehkooděný	k2eAgMnPc1d1	Lehkooděný
lučištníci	lučištník	k1gMnPc1	lučištník
a	a	k8xC	a
prakovníci	prakovník	k1gMnPc1	prakovník
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
pouze	pouze	k6eAd1	pouze
podporovat	podporovat	k5eAaImF	podporovat
útok	útok	k1gInSc4	útok
falangy	falanga	k1gFnSc2	falanga
těžkooděných	těžkooděný	k2eAgInPc2d1	těžkooděný
hoplítů	hoplít	k1gInPc2	hoplít
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
Spartě	Sparta	k1gFnSc6	Sparta
se	se	k3xPyFc4	se
lučištníci	lučištník	k1gMnPc1	lučištník
a	a	k8xC	a
prakovníci	prakovník	k1gMnPc1	prakovník
rekrutovali	rekrutovat	k5eAaImAgMnP	rekrutovat
z	z	k7c2	z
vrstvy	vrstva	k1gFnSc2	vrstva
poloprávných	poloprávný	k2eAgMnPc2d1	poloprávný
heilótů	heilót	k1gMnPc2	heilót
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hoplíty	hoplíta	k1gFnPc4	hoplíta
byli	být	k5eAaImAgMnP	být
svobodní	svobodný	k2eAgMnPc1d1	svobodný
občané	občan	k1gMnPc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
v	v	k7c6	v
řeckých	řecký	k2eAgFnPc6d1	řecká
a	a	k8xC	a
makedonských	makedonský	k2eAgFnPc6d1	makedonská
armádách	armáda	k1gFnPc6	armáda
objevovali	objevovat	k5eAaImAgMnP	objevovat
námezdní	námezdní	k2eAgMnPc1d1	námezdní
lučištníci	lučištník	k1gMnPc1	lučištník
maloasijského	maloasijský	k2eAgInSc2d1	maloasijský
<g/>
,	,	kIx,	,
thráckého	thrácký	k2eAgInSc2d1	thrácký
a	a	k8xC	a
skythského	skythský	k2eAgInSc2d1	skythský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
lukostřelbě	lukostřelba	k1gFnSc3	lukostřelba
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
příslušníci	příslušník	k1gMnPc1	příslušník
pravidelného	pravidelný	k2eAgNnSc2d1	pravidelné
vojska	vojsko	k1gNnSc2	vojsko
bojovali	bojovat	k5eAaImAgMnP	bojovat
kopími	kopí	k1gNnPc7	kopí
<g/>
,	,	kIx,	,
oštěpy	oštěp	k1gInPc7	oštěp
<g/>
,	,	kIx,	,
meči	meč	k1gInPc7	meč
a	a	k8xC	a
sekerami	sekera	k1gFnPc7	sekera
<g/>
,	,	kIx,	,
numidští	numidský	k2eAgMnPc1d1	numidský
a	a	k8xC	a
hispánští	hispánský	k2eAgMnPc1d1	hispánský
žoldnéři	žoldnér	k1gMnPc1	žoldnér
byli	být	k5eAaImAgMnP	být
ozbrojení	ozbrojení	k1gNnSc4	ozbrojení
luky	luk	k1gInPc4	luk
a	a	k8xC	a
praky	prak	k1gInPc4	prak
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
Římané	Říman	k1gMnPc1	Říman
vyzbrojovali	vyzbrojovat	k5eAaImAgMnP	vyzbrojovat
luky	luk	k1gInPc4	luk
především	především	k9	především
auxiliarie	auxiliarie	k1gFnSc1	auxiliarie
(	(	kIx(	(
<g/>
pomocné	pomocný	k2eAgInPc1d1	pomocný
oddíly	oddíl	k1gInPc1	oddíl
barbarského	barbarský	k2eAgInSc2d1	barbarský
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
legionáři	legionář	k1gMnPc1	legionář
bojovali	bojovat	k5eAaImAgMnP	bojovat
oštěpy	oštěp	k1gInPc4	oštěp
a	a	k8xC	a
meči	meč	k1gInPc7	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
pro	pro	k7c4	pro
Kelty	Kelt	k1gMnPc4	Kelt
a	a	k8xC	a
především	především	k9	především
Germány	Germán	k1gMnPc4	Germán
měla	mít	k5eAaImAgFnS	mít
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
Germány	Germán	k1gMnPc4	Germán
vedle	vedle	k7c2	vedle
sekery	sekera	k1gFnSc2	sekera
a	a	k8xC	a
meče	meč	k1gInSc2	meč
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Pramenů	pramen	k1gInPc2	pramen
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
používání	používání	k1gNnSc4	používání
je	být	k5eAaImIp3nS	být
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
vojenství	vojenství	k1gNnSc6	vojenství
se	se	k3xPyFc4	se
luk	luk	k1gInSc1	luk
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
a	a	k8xC	a
římské	římský	k2eAgFnSc3d1	římská
mytologii	mytologie	k1gFnSc3	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
okřídlený	okřídlený	k2eAgMnSc1d1	okřídlený
řecký	řecký	k2eAgMnSc1d1	řecký
bůh	bůh	k1gMnSc1	bůh
lásky	láska	k1gFnSc2	láska
Eros	Eros	k1gMnSc1	Eros
či	či	k8xC	či
římský	římský	k2eAgMnSc1d1	římský
Amor	Amor	k1gMnSc1	Amor
(	(	kIx(	(
<g/>
Kupido	Kupida	k1gFnSc5	Kupida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
ranit	ranit	k5eAaPmF	ranit
svým	svůj	k3xOyFgInSc7	svůj
šípem	šíp	k1gInSc7	šíp
ty	ten	k3xDgFnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dosud	dosud	k6eAd1	dosud
nepoznali	poznat	k5eNaPmAgMnP	poznat
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
řecká	řecký	k2eAgFnSc1d1	řecká
bohyně	bohyně	k1gFnSc1	bohyně
lovu	lov	k1gInSc2	lov
Artemis	Artemis	k1gFnSc1	Artemis
(	(	kIx(	(
<g/>
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
mytologii	mytologie	k1gFnSc6	mytologie
Diana	Diana	k1gFnSc1	Diana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zobrazována	zobrazovat	k5eAaImNgFnS	zobrazovat
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
atributem	atribut	k1gInSc7	atribut
jejího	její	k3xOp3gMnSc2	její
bratra	bratr	k1gMnSc2	bratr
Apollóna	Apollón	k1gMnSc2	Apollón
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
bývá	bývat	k5eAaImIp3nS	bývat
častěji	často	k6eAd2	často
zobrazován	zobrazován	k2eAgInSc1d1	zobrazován
s	s	k7c7	s
lyrou	lyra	k1gFnSc7	lyra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
známého	známý	k2eAgInSc2d1	známý
mýtu	mýtus	k1gInSc2	mýtus
oba	dva	k4xCgMnPc1	dva
božští	božský	k2eAgMnPc1d1	božský
sourozenci	sourozenec	k1gMnPc1	sourozenec
svými	svůj	k3xOyFgInPc7	svůj
šípy	šíp	k1gInPc7	šíp
usmrtili	usmrtit	k5eAaPmAgMnP	usmrtit
sedm	sedm	k4xCc4	sedm
synů	syn	k1gMnPc2	syn
a	a	k8xC	a
dcer	dcera	k1gFnPc2	dcera
pyšné	pyšný	k2eAgFnSc2d1	pyšná
královny	královna	k1gFnSc2	královna
Nioby	Nioba	k1gFnSc2	Nioba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
potrestali	potrestat	k5eAaPmAgMnP	potrestat
její	její	k3xOp3gNnSc4	její
rouhání	rouhání	k1gNnSc4	rouhání
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
řecký	řecký	k2eAgMnSc1d1	řecký
hrdina	hrdina	k1gMnSc1	hrdina
Héraklés	Héraklésa	k1gFnPc2	Héraklésa
používal	používat	k5eAaImAgMnS	používat
smrtící	smrtící	k2eAgInPc4d1	smrtící
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
otrávené	otrávený	k2eAgNnSc1d1	otrávené
jedovatou	jedovatý	k2eAgFnSc7d1	jedovatá
krví	krev	k1gFnSc7	krev
příšery	příšera	k1gFnSc2	příšera
Hydry	hydra	k1gFnSc2	hydra
<g/>
.	.	kIx.	.
</s>
<s>
Usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
např.	např.	kA	např.
orla	orel	k1gMnSc4	orel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
přilétal	přilétat	k5eAaImAgInS	přilétat
klovat	klovat	k5eAaImF	klovat
játra	játra	k1gNnPc4	játra
titánu	titán	k1gMnSc3	titán
Prométheovi	Prométheus	k1gMnSc3	Prométheus
přikovanému	přikovaný	k2eAgMnSc3d1	přikovaný
ke	k	k7c3	k
skále	skála	k1gFnSc3	skála
<g/>
.	.	kIx.	.
</s>
<s>
Héraklés	Héraklés	k6eAd1	Héraklés
však	však	k9	však
svými	svůj	k3xOyFgInPc7	svůj
šípy	šíp	k1gInPc7	šíp
také	také	k6eAd1	také
nešťastnou	šťastný	k2eNgFnSc7d1	nešťastná
náhodou	náhoda	k1gFnSc7	náhoda
smrtelně	smrtelně	k6eAd1	smrtelně
zranil	zranit	k5eAaPmAgMnS	zranit
moudrého	moudrý	k2eAgMnSc4d1	moudrý
kentaura	kentaur	k1gMnSc4	kentaur
Cheiróna	Cheirón	k1gMnSc4	Cheirón
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
verzí	verze	k1gFnPc2	verze
Héraklés	Héraklésa	k1gFnPc2	Héraklésa
nakonec	nakonec	k6eAd1	nakonec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gMnPc3	jeho
manželka	manželka	k1gFnSc1	manželka
Déianeira	Déianeira	k1gFnSc1	Déianeira
otrávila	otrávit	k5eAaPmAgFnS	otrávit
krví	krev	k1gFnSc7	krev
kentaura	kentaur	k1gMnSc2	kentaur
Nesseha	Nesseh	k1gMnSc2	Nesseh
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
Héraklés	Héraklés	k1gInSc1	Héraklés
zabil	zabít	k5eAaPmAgInS	zabít
otráveným	otrávený	k2eAgInSc7d1	otrávený
šípem	šíp	k1gInSc7	šíp
(	(	kIx(	(
<g/>
jed	jed	k1gInSc1	jed
se	se	k3xPyFc4	se
smísil	smísit	k5eAaPmAgInS	smísit
s	s	k7c7	s
kentauří	kentauří	k2eAgFnSc7d1	kentauří
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
Héraklovi	Hérakles	k1gMnSc3	Hérakles
takové	takový	k3xDgNnSc1	takový
bolesti	bolest	k1gFnPc1	bolest
<g/>
,	,	kIx,	,
že	že	k8xS	že
raději	rád	k6eAd2	rád
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrdina	Hrdina	k1gMnSc1	Hrdina
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
Achilles	Achilles	k1gMnSc1	Achilles
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
trójským	trójský	k2eAgMnSc7d1	trójský
princem	princ	k1gMnSc7	princ
Paridem	Paris	k1gMnSc7	Paris
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jej	on	k3xPp3gMnSc4	on
střelil	střelit	k5eAaPmAgMnS	střelit
šípem	šíp	k1gInSc7	šíp
do	do	k7c2	do
jediného	jediný	k2eAgNnSc2d1	jediné
zranitelného	zranitelný	k2eAgNnSc2d1	zranitelné
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
patě	pata	k1gFnSc6	pata
<g/>
.	.	kIx.	.
</s>
<s>
Parida	Paris	k1gMnSc4	Paris
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
šípem	šíp	k1gInSc7	šíp
chromý	chromý	k2eAgMnSc1d1	chromý
řecký	řecký	k2eAgMnSc1d1	řecký
válečník	válečník	k1gMnSc1	válečník
Filoktétés	Filoktétésa	k1gFnPc2	Filoktétésa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
z	z	k7c2	z
homérských	homérský	k2eAgMnPc2d1	homérský
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
Odysseus	Odysseus	k1gInSc4	Odysseus
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
obhajovat	obhajovat	k5eAaImF	obhajovat
svoji	svůj	k3xOyFgFnSc4	svůj
královskou	královský	k2eAgFnSc4d1	královská
i	i	k8xC	i
manželskou	manželský	k2eAgFnSc4d1	manželská
pozici	pozice	k1gFnSc4	pozice
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Ithaka	Ithaka	k1gFnSc1	Ithaka
v	v	k7c6	v
lukostřelecké	lukostřelecký	k2eAgFnSc6d1	lukostřelecká
soutěži	soutěž	k1gFnSc6	soutěž
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
šípy	šíp	k1gInPc4	šíp
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
zrádné	zrádný	k2eAgMnPc4d1	zrádný
nápadníky	nápadník	k1gMnPc4	nápadník
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Penelopy	Penelopa	k1gFnSc2	Penelopa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Stepní	stepní	k2eAgMnPc1d1	stepní
nomádi	nomád	k1gMnPc1	nomád
====	====	k?	====
</s>
</p>
<p>
<s>
Skythové	Skyth	k1gMnPc1	Skyth
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
příbuzní	příbuzný	k1gMnPc1	příbuzný
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Massagetové	Massaget	k1gMnPc1	Massaget
<g/>
,	,	kIx,	,
Sarmaté	Sarmat	k1gMnPc1	Sarmat
a	a	k8xC	a
Alani	Alan	k1gMnPc1	Alan
obývali	obývat	k5eAaImAgMnP	obývat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
stepní	stepní	k2eAgFnPc4d1	stepní
oblasti	oblast	k1gFnPc4	oblast
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
od	od	k7c2	od
Karpat	Karpaty	k1gInPc2	Karpaty
až	až	k9	až
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
íránská	íránský	k2eAgNnPc4d1	íránské
etnika	etnikum	k1gNnPc4	etnikum
<g/>
,	,	kIx,	,
spřízněná	spřízněný	k2eAgNnPc4d1	spřízněné
s	s	k7c7	s
dnešními	dnešní	k2eAgMnPc7d1	dnešní
Osety	Oset	k1gMnPc7	Oset
a	a	k8xC	a
Jaghnóbci	Jaghnóbce	k1gMnPc7	Jaghnóbce
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
těchto	tento	k3xDgInPc2	tento
nomádských	nomádský	k2eAgInPc2d1	nomádský
kmenů	kmen	k1gInPc2	kmen
prosluli	proslout	k5eAaPmAgMnP	proslout
jako	jako	k9	jako
stateční	statečný	k2eAgMnPc1d1	statečný
a	a	k8xC	a
krutí	krutý	k2eAgMnPc1d1	krutý
válečníci	válečník	k1gMnPc1	válečník
<g/>
,	,	kIx,	,
znepokojující	znepokojující	k2eAgMnPc1d1	znepokojující
svými	svůj	k3xOyFgMnPc7	svůj
nájezdy	nájezd	k1gInPc4	nájezd
sousední	sousední	k2eAgInPc4d1	sousední
usedlé	usedlý	k2eAgInPc4d1	usedlý
národy	národ	k1gInPc4	národ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Thráky	Thrák	k1gMnPc4	Thrák
<g/>
,	,	kIx,	,
Řeky	Řek	k1gMnPc4	Řek
<g/>
,	,	kIx,	,
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
Peršany	Peršan	k1gMnPc4	Peršan
i	i	k8xC	i
Indy	Ind	k1gMnPc4	Ind
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
armádách	armáda	k1gFnPc6	armáda
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
však	však	k9	však
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
oddíly	oddíl	k1gInPc1	oddíl
skythsých	skythsý	k2eAgMnPc2d1	skythsý
a	a	k8xC	a
sarmatských	sarmatský	k2eAgMnPc2d1	sarmatský
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
Skythů	Skyth	k1gMnPc2	Skyth
<g/>
,	,	kIx,	,
Sarmatů	Sarmat	k1gMnPc2	Sarmat
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
sousedů	soused	k1gMnPc2	soused
byl	být	k5eAaImAgInS	být
krátký	krátký	k2eAgInSc1d1	krátký
reflexní	reflexní	k2eAgInSc1d1	reflexní
luk	luk	k1gInSc1	luk
<g/>
,	,	kIx,	,
zhotovený	zhotovený	k2eAgInSc1d1	zhotovený
z	z	k7c2	z
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
obaleného	obalený	k2eAgNnSc2d1	obalené
vrstvami	vrstva	k1gFnPc7	vrstva
šlach	šlacha	k1gFnPc2	šlacha
a	a	k8xC	a
rohoviny	rohovina	k1gFnSc2	rohovina
<g/>
.	.	kIx.	.
</s>
<s>
Bojovali	bojovat	k5eAaImAgMnP	bojovat
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xC	jako
jízda	jízda	k1gFnSc1	jízda
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
schopností	schopnost	k1gFnSc7	schopnost
střílet	střílet	k5eAaImF	střílet
z	z	k7c2	z
luku	luk	k1gInSc2	luk
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
trysku	trysk	k1gInSc6	trysk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
jízdy	jízda	k1gFnSc2	jízda
<g/>
,	,	kIx,	,
udivovali	udivovat	k5eAaImAgMnP	udivovat
své	svůj	k3xOyFgMnPc4	svůj
protivníky	protivník	k1gMnPc4	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
typ	typ	k1gInSc1	typ
luku	luk	k1gInSc2	luk
a	a	k8xC	a
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
taktiku	taktika	k1gFnSc4	taktika
používali	používat	k5eAaImAgMnP	používat
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
starověku	starověk	k1gInSc2	starověk
rovněž	rovněž	k9	rovněž
Hunové	Hun	k1gMnPc1	Hun
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
však	však	k9	však
kromě	kromě	k7c2	kromě
luků	luk	k1gInPc2	luk
v	v	k7c6	v
boji	boj	k1gInSc6	boj
hojně	hojně	k6eAd1	hojně
používali	používat	k5eAaImAgMnP	používat
také	také	k9	také
kopí	kopí	k1gNnSc4	kopí
a	a	k8xC	a
lasa	laso	k1gNnPc4	laso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Asie	Asie	k1gFnSc2	Asie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
obávanými	obávaný	k2eAgMnPc7d1	obávaný
lučištníky	lučištník	k1gMnPc7	lučištník
Alani	Alaň	k1gMnSc5	Alaň
a	a	k8xC	a
zejména	zejména	k9	zejména
Hunové	Hun	k1gMnPc1	Hun
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
jízdní	jízdní	k2eAgMnPc1d1	jízdní
lučištníci	lučištník	k1gMnPc1	lučištník
udivovali	udivovat	k5eAaImAgMnP	udivovat
Římany	Říman	k1gMnPc4	Říman
svou	svůj	k3xOyFgFnSc7	svůj
zručností	zručnost	k1gFnSc7	zručnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
schopností	schopnost	k1gFnPc2	schopnost
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
střílet	střílet	k5eAaImF	střílet
dozadu	dozadu	k6eAd1	dozadu
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
jízdy	jízda	k1gFnSc2	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
Hunů	Hun	k1gMnPc2	Hun
byly	být	k5eAaImAgInP	být
reflexní	reflexní	k2eAgInPc1d1	reflexní
<g/>
,	,	kIx,	,
lepené	lepený	k2eAgInPc1d1	lepený
z	z	k7c2	z
vrstev	vrstva	k1gFnPc2	vrstva
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
dýh	dýha	k1gFnPc2	dýha
<g/>
,	,	kIx,	,
šlach	šlacha	k1gFnPc2	šlacha
a	a	k8xC	a
rohoviny	rohovina	k1gFnSc2	rohovina
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgNnSc4d1	podobné
pozdějším	pozdní	k2eAgInPc3d2	pozdější
lukům	luk	k1gInPc3	luk
Mongolů	mongol	k1gInPc2	mongol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
užívání	užívání	k1gNnSc6	užívání
luků	luk	k1gInPc2	luk
vynikali	vynikat	k5eAaImAgMnP	vynikat
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
Peršané	Peršan	k1gMnPc1	Peršan
a	a	k8xC	a
Turci	Turek	k1gMnPc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
armádách	armáda	k1gFnPc6	armáda
hrály	hrát	k5eAaImAgFnP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
oddíly	oddíl	k1gInPc1	oddíl
jízdních	jízdní	k2eAgMnPc2d1	jízdní
i	i	k8xC	i
pěších	pěší	k2eAgMnPc2d1	pěší
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
.	.	kIx.	.
obáváni	obávat	k5eAaImNgMnP	obávat
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
jízdní	jízdní	k2eAgMnPc1d1	jízdní
lučištníci	lučištník	k1gMnPc1	lučištník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
boji	boj	k1gInSc6	boj
objížděli	objíždět	k5eAaImAgMnP	objíždět
nepřátele	nepřítel	k1gMnPc4	nepřítel
na	na	k7c6	na
hbitých	hbitý	k2eAgMnPc6d1	hbitý
konících	koník	k1gMnPc6	koník
a	a	k8xC	a
stříleli	střílet	k5eAaImAgMnP	střílet
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
z	z	k7c2	z
krátkých	krátká	k1gFnPc2	krátká
reflexních	reflexní	k2eAgInPc2d1	reflexní
luků	luk	k1gInPc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
taktiku	taktika	k1gFnSc4	taktika
používali	používat	k5eAaImAgMnP	používat
později	pozdě	k6eAd2	pozdě
osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
Mongolové	Mongol	k1gMnPc1	Mongol
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pečeněžští	pečeněžský	k2eAgMnPc1d1	pečeněžský
<g/>
,	,	kIx,	,
uherští	uherský	k2eAgMnPc1d1	uherský
a	a	k8xC	a
kumánští	kumánský	k2eAgMnPc1d1	kumánský
jezdci	jezdec	k1gMnPc1	jezdec
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Evropští	evropský	k2eAgMnPc1d1	evropský
rytíři	rytíř	k1gMnPc1	rytíř
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
a	a	k8xC	a
reconquisty	reconquist	k1gInPc4	reconquist
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
velmi	velmi	k6eAd1	velmi
obávali	obávat	k5eAaImAgMnP	obávat
muslimských	muslimský	k2eAgMnPc2d1	muslimský
lučištníků	lučištník	k1gMnPc2	lučištník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
šípy	šíp	k1gInPc1	šíp
pronikaly	pronikat	k5eAaImAgInP	pronikat
i	i	k9	i
kroužkovou	kroužkový	k2eAgFnSc7d1	kroužková
zbrojí	zbroj	k1gFnSc7	zbroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
bojištích	bojiště	k1gNnPc6	bojiště
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc2d1	celá
Asie	Asie	k1gFnSc2	Asie
si	se	k3xPyFc3	se
ve	v	k7c4	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vydobyli	vydobýt	k5eAaPmAgMnP	vydobýt
pověst	pověst	k1gFnSc4	pověst
neporazitelnosti	neporazitelnost	k1gFnSc2	neporazitelnost
mongolští	mongolský	k2eAgMnPc1d1	mongolský
jezdci	jezdec	k1gInSc3	jezdec
Čingischána	Čingischán	k1gMnSc2	Čingischán
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
nástupců	nástupce	k1gMnPc2	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jezdecké	jezdecký	k2eAgInPc1d1	jezdecký
reflexní	reflexní	k2eAgInPc1d1	reflexní
luky	luk	k1gInPc1	luk
Seldžuků	Seldžuk	k1gMnPc2	Seldžuk
<g/>
,	,	kIx,	,
Osmanů	Osman	k1gMnPc2	Osman
i	i	k8xC	i
Mongolů	Mongol	k1gMnPc2	Mongol
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
symetrickým	symetrický	k2eAgNnSc7d1	symetrické
dvojitým	dvojitý	k2eAgNnSc7d1	dvojité
prohnutím	prohnutí	k1gNnSc7	prohnutí
lučiště	lučiště	k1gNnSc2	lučiště
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
slepovány	slepovat	k5eAaImNgInP	slepovat
z	z	k7c2	z
několika	několik	k4yIc2	několik
vrstev	vrstva	k1gFnPc2	vrstva
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
dýh	dýha	k1gFnPc2	dýha
(	(	kIx(	(
<g/>
jádro	jádro	k1gNnSc1	jádro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
šlach	šlacha	k1gFnPc2	šlacha
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnSc1d1	přední
strana	strana	k1gFnSc1	strana
lučiště	lučiště	k1gNnSc2	lučiště
<g/>
)	)	kIx)	)
a	a	k8xC	a
rohoviny	rohovina	k1gFnSc2	rohovina
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgFnSc1d1	zadní
strana	strana	k1gFnSc1	strana
lučiště	lučiště	k1gNnSc2	lučiště
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
luk	luk	k1gInSc1	luk
bývá	bývat	k5eAaImIp3nS	bývat
potažen	potažen	k2eAgInSc4d1	potažen
kůží	kůže	k1gFnSc7	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
ohybem	ohyb	k1gInSc7	ohyb
špiček	špička	k1gFnPc2	špička
luku	luk	k1gInSc2	luk
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
kostěné	kostěný	k2eAgFnPc1d1	kostěná
kobylky	kobylka	k1gFnPc1	kobylka
pro	pro	k7c4	pro
zachycení	zachycení	k1gNnSc4	zachycení
tětivy	tětiva	k1gFnSc2	tětiva
a	a	k8xC	a
jejímu	její	k3xOp3gMnSc3	její
držení	držení	k1gNnSc1	držení
na	na	k7c6	na
středu	střed	k1gInSc6	střed
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
bývá	bývat	k5eAaImIp3nS	bývat
zdoben	zdobit	k5eAaImNgInS	zdobit
ornamenty	ornament	k1gInPc7	ornament
a	a	k8xC	a
pestře	pestro	k6eAd1	pestro
barven	barven	k2eAgInSc1d1	barven
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
luku	luk	k1gInSc2	luk
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
12	[number]	k4	12
do	do	k7c2	do
50	[number]	k4	50
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
výjimečnou	výjimečný	k2eAgFnSc7d1	výjimečná
pružností	pružnost	k1gFnSc7	pružnost
a	a	k8xC	a
dalekým	daleký	k2eAgInSc7d1	daleký
dostřelem	dostřel	k1gInSc7	dostřel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vypnutém	vypnutý	k2eAgInSc6d1	vypnutý
stavu	stav	k1gInSc6	stav
má	mít	k5eAaImIp3nS	mít
reflexní	reflexní	k2eAgInSc4d1	reflexní
luk	luk	k1gInSc4	luk
tvar	tvar	k1gInSc4	tvar
téměř	téměř	k6eAd1	téměř
kruhového	kruhový	k2eAgInSc2d1	kruhový
oblouku	oblouk	k1gInSc2	oblouk
<g/>
,	,	kIx,	,
při	při	k7c6	při
napínání	napínání	k1gNnSc6	napínání
se	se	k3xPyFc4	se
oblouk	oblouk	k1gInSc1	oblouk
luku	luk	k1gInSc2	luk
ohne	ohnout	k5eAaPmIp3nS	ohnout
"	"	kIx"	"
<g/>
naruby	naruby	k6eAd1	naruby
<g/>
"	"	kIx"	"
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
symetrický	symetrický	k2eAgInSc1d1	symetrický
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
prohnutý	prohnutý	k2eAgInSc4d1	prohnutý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Mongolské	mongolský	k2eAgInPc1d1	mongolský
luky	luk	k1gInPc1	luk
jsou	být	k5eAaImIp3nP	být
krátké	krátká	k1gFnPc1	krátká
<g/>
,	,	kIx,	,
v	v	k7c6	v
napnutém	napnutý	k2eAgInSc6d1	napnutý
stavu	stav	k1gInSc6	stav
měří	měřit	k5eAaImIp3nS	měřit
jen	jen	k9	jen
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
170	[number]	k4	170
cm	cm	kA	cm
a	a	k8xC	a
jezdci	jezdec	k1gMnPc1	jezdec
je	on	k3xPp3gMnPc4	on
nosili	nosit	k5eAaImAgMnP	nosit
u	u	k7c2	u
pasu	pas	k1gInSc2	pas
<g/>
,	,	kIx,	,
buďto	buďto	k8xC	buďto
v	v	k7c6	v
toulci	toulec	k1gInSc6	toulec
společně	společně	k6eAd1	společně
se	s	k7c7	s
šípy	šíp	k1gInPc7	šíp
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
zvláštním	zvláštní	k2eAgNnSc6d1	zvláštní
koženém	kožený	k2eAgNnSc6d1	kožené
pouzdře	pouzdro	k1gNnSc6	pouzdro
zvaném	zvaný	k2eAgNnSc6d1	zvané
sahajdak	sahajdak	k1gInSc1	sahajdak
<g/>
.	.	kIx.	.
</s>
<s>
Šípy	šíp	k1gInPc1	šíp
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
kolem	kolem	k7c2	kolem
jednoho	jeden	k4xCgInSc2	jeden
metru	metr	k1gInSc2	metr
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
váhu	váha	k1gFnSc4	váha
100	[number]	k4	100
až	až	k9	až
120	[number]	k4	120
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
opeření	opeření	k1gNnSc1	opeření
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
orlího	orlí	k2eAgNnSc2d1	orlí
peří	peří	k1gNnSc2	peří
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
jej	on	k3xPp3gMnSc4	on
čtyři	čtyři	k4xCgNnPc4	čtyři
křidélka	křidélko	k1gNnPc4	křidélko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
(	(	kIx(	(
<g/>
asi	asi	k9	asi
třetina	třetina	k1gFnSc1	třetina
délky	délka	k1gFnSc2	délka
šípu	šíp	k1gInSc2	šíp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
asijských	asijský	k2eAgInPc2d1	asijský
národů	národ	k1gInPc2	národ
napíná	napínat	k5eAaImIp3nS	napínat
tětivu	tětiva	k1gFnSc4	tětiva
pomocí	pomocí	k7c2	pomocí
palce	palec	k1gInSc2	palec
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nP	činit
pomocí	pomocí	k7c2	pomocí
tří	tři	k4xCgInPc2	tři
prstů	prst	k1gInPc2	prst
(	(	kIx(	(
<g/>
tří	tři	k4xCgInPc2	tři
prstů	prst	k1gInPc2	prst
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
lukostřelbě	lukostřelba	k1gFnSc6	lukostřelba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
a	a	k8xC	a
ochranu	ochrana	k1gFnSc4	ochrana
palce	palec	k1gInSc2	palec
proto	proto	k8xC	proto
používají	používat	k5eAaImIp3nP	používat
lukostřelecký	lukostřelecký	k2eAgInSc4d1	lukostřelecký
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Navléká	navlékat	k5eAaImIp3nS	navlékat
se	se	k3xPyFc4	se
na	na	k7c4	na
palec	palec	k1gInSc4	palec
ruky	ruka	k1gFnSc2	ruka
napínající	napínající	k2eAgFnSc4d1	napínající
tětivu	tětiva	k1gFnSc4	tětiva
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
jazyk	jazyk	k1gInSc4	jazyk
chránící	chránící	k2eAgInSc4d1	chránící
bříško	bříško	k1gNnSc4	bříško
palce	palec	k1gInSc2	palec
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
tětivy	tětiva	k1gFnSc2	tětiva
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
napínání	napínání	k1gNnSc2	napínání
tětivy	tětiva	k1gFnSc2	tětiva
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
i	i	k9	i
držení	držení	k1gNnSc1	držení
šípu	šíp	k1gInSc2	šíp
u	u	k7c2	u
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
asijští	asijský	k2eAgMnPc1d1	asijský
lučištníci	lučištník	k1gMnPc1	lučištník
přikládají	přikládat	k5eAaImIp3nP	přikládat
šíp	šíp	k1gInSc4	šíp
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c4	na
palec	palec	k1gInSc4	palec
<g/>
.	.	kIx.	.
</s>
<s>
Mířících	mířící	k2eAgNnPc2d1	mířící
znamének	znaménko	k1gNnPc2	znaménko
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
střílí	střílet	k5eAaImIp3nS	střílet
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
odhadem	odhad	k1gInSc7	odhad
dráhy	dráha	k1gFnSc2	dráha
letu	let	k1gInSc2	let
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mongolové	Mongol	k1gMnPc1	Mongol
si	se	k3xPyFc3	se
lukostřelby	lukostřelba	k1gFnPc1	lukostřelba
cení	cenit	k5eAaImIp3nP	cenit
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
pokládají	pokládat	k5eAaImIp3nP	pokládat
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
národní	národní	k2eAgInSc4d1	národní
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
staré	starý	k2eAgNnSc4d1	staré
umění	umění	k1gNnSc4	umění
výroby	výroba	k1gFnSc2	výroba
luků	luk	k1gInPc2	luk
však	však	k9	však
již	již	k6eAd1	již
ovládá	ovládat	k5eAaImIp3nS	ovládat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Turnaje	turnaj	k1gInPc1	turnaj
v	v	k7c6	v
lukostřelbě	lukostřelba	k1gFnSc6	lukostřelba
jsou	být	k5eAaImIp3nP	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zápasem	zápas	k1gInSc7	zápas
a	a	k8xC	a
dostihy	dostih	k1gInPc1	dostih
součástí	součást	k1gFnPc2	součást
mongolského	mongolský	k2eAgInSc2d1	mongolský
národního	národní	k2eAgInSc2d1	národní
svátku	svátek	k1gInSc2	svátek
nádam	nádam	k6eAd1	nádam
<g/>
.	.	kIx.	.
</s>
<s>
Střílí	střílet	k5eAaImIp3nS	střílet
se	se	k3xPyFc4	se
na	na	k7c4	na
40	[number]	k4	40
kroků	krok	k1gInPc2	krok
do	do	k7c2	do
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
špalíků	špalík	k1gInPc2	špalík
nebo	nebo	k8xC	nebo
velkých	velký	k2eAgNnPc2d1	velké
klubek	klubko	k1gNnPc2	klubko
z	z	k7c2	z
kožených	kožený	k2eAgInPc2d1	kožený
řemínků	řemínek	k1gInPc2	řemínek
<g/>
,	,	kIx,	,
ležících	ležící	k2eAgFnPc2d1	ležící
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
disciplínou	disciplína	k1gFnSc7	disciplína
je	být	k5eAaImIp3nS	být
střelba	střelba	k1gFnSc1	střelba
na	na	k7c4	na
co	co	k3yInSc4	co
největší	veliký	k2eAgFnSc1d3	veliký
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
(	(	kIx(	(
<g/>
něco	něco	k3yInSc1	něco
jako	jako	k8xS	jako
vrh	vrh	k1gInSc4	vrh
oštěpem	oštěp	k1gInSc7	oštěp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc6	Bhútán
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Sibiře	Sibiř	k1gFnSc2	Sibiř
je	být	k5eAaImIp3nS	být
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
dodnes	dodnes	k6eAd1	dodnes
národním	národní	k2eAgInSc7d1	národní
sportem	sport	k1gInSc7	sport
a	a	k8xC	a
dbá	dbát	k5eAaImIp3nS	dbát
se	se	k3xPyFc4	se
na	na	k7c4	na
udržování	udržování	k1gNnSc4	udržování
její	její	k3xOp3gFnSc2	její
tradice	tradice	k1gFnSc2	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
má	mít	k5eAaImIp3nS	mít
dávné	dávný	k2eAgFnSc2d1	dávná
tradice	tradice	k1gFnSc2	tradice
také	také	k9	také
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
čínských	čínský	k2eAgInPc2d1	čínský
mýtů	mýtus	k1gInPc2	mýtus
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
lučištníku	lučištník	k1gMnSc6	lučištník
Jaovi	Jaoev	k1gFnSc6	Jaoev
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sestřelil	sestřelit	k5eAaPmAgMnS	sestřelit
dvě	dva	k4xCgNnPc1	dva
nadbytečná	nadbytečný	k2eAgNnPc1d1	nadbytečné
slunce	slunce	k1gNnPc1	slunce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
přílišným	přílišný	k2eAgInSc7d1	přílišný
žárem	žár	k1gInSc7	žár
spalovala	spalovat	k5eAaImAgFnS	spalovat
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Luk	luk	k1gInSc1	luk
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Číně	Čína	k1gFnSc6	Čína
zbraní	zbraň	k1gFnPc2	zbraň
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
bojovníků	bojovník	k1gMnPc2	bojovník
na	na	k7c6	na
válečných	válečný	k2eAgInPc6d1	válečný
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
složil	složit	k5eAaPmAgMnS	složit
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
i	i	k9	i
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
vytlačován	vytlačován	k2eAgInSc4d1	vytlačován
kuší	kuše	k1gFnSc7	kuše
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
rozkvět	rozkvět	k1gInSc4	rozkvět
doznala	doznat	k5eAaPmAgFnS	doznat
čínská	čínský	k2eAgFnSc1d1	čínská
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
až	až	k9	až
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Mongolů	Mongol	k1gMnPc2	Mongol
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Jüan	jüan	k1gInSc1	jüan
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
̈	̈	k?	̈
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověkém	středověký	k2eAgNnSc6d1	středověké
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
luky	luk	k1gInPc1	luk
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
z	z	k7c2	z
bambusových	bambusový	k2eAgInPc2d1	bambusový
stromků	stromek	k1gInPc2	stromek
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgInPc1d1	japonský
luky	luk	k1gInPc1	luk
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
až	až	k9	až
220	[number]	k4	220
cm	cm	kA	cm
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
asymetrickým	asymetrický	k2eAgInSc7d1	asymetrický
tvarem	tvar	k1gInSc7	tvar
lučiště	lučiště	k1gNnSc2	lučiště
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
lučiště	lučiště	k1gNnSc1	lučiště
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
část	část	k1gFnSc4	část
spodní	spodní	k2eAgFnSc2d1	spodní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
asi	asi	k9	asi
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
:	:	kIx,	:
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Šípy	šíp	k1gInPc1	šíp
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
od	od	k7c2	od
svislé	svislý	k2eAgFnSc2d1	svislá
osy	osa	k1gFnSc2	osa
těla	tělo	k1gNnSc2	tělo
po	po	k7c6	po
délce	délka	k1gFnSc6	délka
natažené	natažený	k2eAgFnSc2d1	natažená
paže	paže	k1gFnSc2	paže
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
ramen	rameno	k1gNnPc2	rameno
<g/>
,	,	kIx,	,
asi	asi	k9	asi
5	[number]	k4	5
cm	cm	kA	cm
přes	přes	k7c4	přes
délku	délka	k1gFnSc4	délka
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Střílí	střílet	k5eAaImIp3nS	střílet
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
instinktivně	instinktivně	k6eAd1	instinktivně
<g/>
.	.	kIx.	.
</s>
<s>
Japonské	japonský	k2eAgNnSc1d1	Japonské
umění	umění	k1gNnSc1	umění
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kjúdó	kjúdó	k?	kjúdó
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
duchovní	duchovní	k2eAgInSc4d1	duchovní
přesah	přesah	k1gInSc4	přesah
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
úzce	úzko	k6eAd1	úzko
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
zenovým	zenový	k2eAgInSc7d1	zenový
buddhismem	buddhismus	k1gInSc7	buddhismus
a	a	k8xC	a
uměním	umění	k1gNnSc7	umění
meditace	meditace	k1gFnSc2	meditace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Afrika	Afrika	k1gFnSc1	Afrika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
měla	mít	k5eAaImAgFnS	mít
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
starých	starý	k2eAgMnPc2d1	starý
Egypťanů	Egypťan	k1gMnPc2	Egypťan
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
středověku	středověk	k1gInSc2	středověk
byla	být	k5eAaImAgFnS	být
ještě	ještě	k9	ještě
zdokonalena	zdokonalit	k5eAaPmNgFnS	zdokonalit
Araby	Arab	k1gMnPc7	Arab
a	a	k8xC	a
Berbery	Berber	k1gMnPc7	Berber
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
požívají	požívat	k5eAaImIp3nP	požívat
luky	luk	k1gInPc4	luk
a	a	k8xC	a
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
podobající	podobající	k2eAgInPc4d1	podobající
se	se	k3xPyFc4	se
lukům	luk	k1gInPc3	luk
z	z	k7c2	z
období	období	k1gNnSc2	období
paleolitu	paleolit	k1gInSc2	paleolit
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
afrických	africký	k2eAgMnPc2d1	africký
lovců	lovec	k1gMnPc2	lovec
vypadaly	vypadat	k5eAaPmAgFnP	vypadat
ještě	ještě	k6eAd1	ještě
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
takřka	takřka	k6eAd1	takřka
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
<g/>
,	,	kIx,	,
když	když	k8xS	když
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
o	o	k7c6	o
Núbijcích	Núbijce	k1gMnPc6	Núbijce
<g/>
,	,	kIx,	,
bojujících	bojující	k2eAgMnPc2d1	bojující
ve	v	k7c6	v
vojsku	vojsko	k1gNnSc6	vojsko
perského	perský	k2eAgMnSc2d1	perský
krále	král	k1gMnSc2	král
Xerxa	Xerxes	k1gMnSc2	Xerxes
při	při	k7c6	při
jeho	jeho	k3xOp3gNnSc6	jeho
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
ohnutých	ohnutý	k2eAgInPc2d1	ohnutý
stromků	stromek	k1gInPc2	stromek
nebo	nebo	k8xC	nebo
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
bez	bez	k7c2	bez
zářezů	zářez	k1gInPc2	zářez
pro	pro	k7c4	pro
tětivu	tětiva	k1gFnSc4	tětiva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zhotovuje	zhotovovat	k5eAaImIp3nS	zhotovovat
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
lýka	lýko	k1gNnSc2	lýko
<g/>
,	,	kIx,	,
sušených	sušený	k2eAgNnPc2d1	sušené
střívek	střívko	k1gNnPc2	střívko
nebo	nebo	k8xC	nebo
provázků	provázek	k1gInPc2	provázek
a	a	k8xC	a
připevňuje	připevňovat	k5eAaImIp3nS	připevňovat
se	se	k3xPyFc4	se
pouhým	pouhý	k2eAgNnSc7d1	pouhé
přivázáním	přivázání	k1gNnSc7	přivázání
<g/>
.	.	kIx.	.
</s>
<s>
Šípy	šíp	k1gInPc1	šíp
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
jim	on	k3xPp3gMnPc3	on
chybí	chybět	k5eAaImIp3nS	chybět
opeření	opeření	k1gNnSc4	opeření
a	a	k8xC	a
často	často	k6eAd1	často
mívají	mívat	k5eAaImIp3nP	mívat
otrávené	otrávený	k2eAgInPc4d1	otrávený
hroty	hrot	k1gInPc4	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
právě	právě	k6eAd1	právě
účinné	účinný	k2eAgInPc1d1	účinný
šípové	šípový	k2eAgInPc1d1	šípový
jedy	jed	k1gInPc1	jed
způsobily	způsobit	k5eAaPmAgInP	způsobit
ustrnutí	ustrnutí	k1gNnSc4	ustrnutí
africké	africký	k2eAgFnSc2d1	africká
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
jednoduchých	jednoduchý	k2eAgFnPc6d1	jednoduchá
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc4	luk
a	a	k8xC	a
otrávené	otrávený	k2eAgInPc4d1	otrávený
šípy	šíp	k1gInPc4	šíp
používají	používat	k5eAaImIp3nP	používat
afričtí	africký	k2eAgMnPc1d1	africký
pytláci	pytlák	k1gMnPc1	pytlák
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
především	především	k9	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
pušce	puška	k1gFnSc3	puška
tiché	tichý	k2eAgFnSc3d1	tichá
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
ukrýt	ukrýt	k5eAaPmF	ukrýt
<g/>
.	.	kIx.	.
</s>
<s>
Afričané	Afričan	k1gMnPc1	Afričan
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
šípových	šípový	k2eAgInPc2d1	šípový
jedů	jed	k1gInPc2	jed
mléčných	mléčný	k2eAgFnPc2d1	mléčná
šťáv	šťáva	k1gFnPc2	šťáva
získaných	získaný	k2eAgInPc2d1	získaný
z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
Apocantheum	Apocantheum	k1gNnSc4	Apocantheum
frisiorum	frisiorum	k1gNnSc1	frisiorum
<g/>
,	,	kIx,	,
pryšců	pryšec	k1gInPc2	pryšec
rodu	rod	k1gInSc2	rod
Euphorbia	Euphorbius	k1gMnSc2	Euphorbius
nebo	nebo	k8xC	nebo
sukulentní	sukulentní	k2eAgFnSc2d1	sukulentní
rostliny	rostlina	k1gFnSc2	rostlina
Adenium	Adenium	k1gNnSc1	Adenium
obesum	obesum	k1gInSc1	obesum
<g/>
,	,	kIx,	,
pěstované	pěstovaný	k2eAgNnSc1d1	pěstované
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
růžové	růžový	k2eAgInPc4d1	růžový
květy	květ	k1gInPc4	květ
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Sanové	Sanus	k1gMnPc1	Sanus
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
jed	jed	k1gInSc4	jed
i	i	k9	i
z	z	k7c2	z
housenek	housenka	k1gFnPc2	housenka
některých	některý	k3yIgMnPc2	některý
motýlů	motýl	k1gMnPc2	motýl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
slouží	sloužit	k5eAaImIp3nS	sloužit
luky	luk	k1gInPc4	luk
a	a	k8xC	a
šípy	šíp	k1gInPc4	šíp
téměř	téměř	k6eAd1	téměř
výlučně	výlučně	k6eAd1	výlučně
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
válečnou	válečný	k2eAgFnSc4d1	válečná
zbraň	zbraň	k1gFnSc4	zbraň
preferovali	preferovat	k5eAaImAgMnP	preferovat
afričtí	africký	k2eAgMnPc1d1	africký
domorodci	domorodec	k1gMnPc1	domorodec
vždy	vždy	k6eAd1	vždy
oštěp	oštěp	k1gInSc4	oštěp
či	či	k8xC	či
kopí	kopí	k1gNnSc4	kopí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Indiáni	Indián	k1gMnPc1	Indián
====	====	k?	====
</s>
</p>
<p>
<s>
Také	také	k9	také
severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
Indiáni	Indián	k1gMnPc1	Indián
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
dobrými	dobrý	k2eAgMnPc7d1	dobrý
střelci	střelec	k1gMnPc7	střelec
a	a	k8xC	a
výrobci	výrobce	k1gMnPc1	výrobce
luků	luk	k1gInPc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
luků	luk	k1gInPc2	luk
byla	být	k5eAaImAgFnS	být
až	až	k9	až
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zhotovovány	zhotovovat	k5eAaImNgFnP	zhotovovat
z	z	k7c2	z
tisu	tis	k1gInSc2	tis
<g/>
,	,	kIx,	,
hicori	hicor	k1gFnSc2	hicor
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
z	z	k7c2	z
cedru	cedr	k1gInSc2	cedr
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgInPc4d3	nejdelší
luky	luk	k1gInPc4	luk
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
indiáni	indián	k1gMnPc1	indián
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Seminolové	Seminol	k1gMnPc1	Seminol
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
kmeny	kmen	k1gInPc1	kmen
z	z	k7c2	z
plání	pláň	k1gFnPc2	pláň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Siouxové	Siouxový	k2eAgNnSc1d1	Siouxové
a	a	k8xC	a
Komančové	Komanč	k1gMnPc1	Komanč
měli	mít	k5eAaImAgMnP	mít
luky	luk	k1gInPc4	luk
kratší	krátký	k2eAgInPc4d2	kratší
<g/>
,	,	kIx,	,
uzpůsobené	uzpůsobený	k2eAgInPc4d1	uzpůsobený
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
z	z	k7c2	z
koňského	koňský	k2eAgNnSc2d1	koňské
sedla	sedlo	k1gNnSc2	sedlo
<g/>
.	.	kIx.	.
</s>
<s>
Indiáni	Indián	k1gMnPc1	Indián
znali	znát	k5eAaImAgMnP	znát
také	také	k9	také
reflexní	reflexní	k2eAgInPc4d1	reflexní
luky	luk	k1gInPc4	luk
z	z	k7c2	z
rohů	roh	k1gInPc2	roh
bizona	bizon	k1gMnSc2	bizon
či	či	k8xC	či
ovce	ovce	k1gFnPc4	ovce
tlustorohé	tlustorohý	k2eAgFnPc4d1	tlustorohý
<g/>
,	,	kIx,	,
jako	jako	k9	jako
zhotovovali	zhotovovat	k5eAaImAgMnP	zhotovovat
např.	např.	kA	např.
Siouxové	Siouxový	k2eAgInPc1d1	Siouxový
nebo	nebo	k8xC	nebo
luky	luk	k1gInPc1	luk
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
vyztuženého	vyztužený	k2eAgInSc2d1	vyztužený
šlachami	šlacha	k1gFnPc7	šlacha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
dodávaly	dodávat	k5eAaImAgFnP	dodávat
luku	luk	k1gInSc2	luk
větší	veliký	k2eAgFnSc4d2	veliký
pružnost	pružnost	k1gFnSc4	pružnost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
Apačů	Apač	k1gMnPc2	Apač
a	a	k8xC	a
Navahů	Navah	k1gMnPc2	Navah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgFnPc1d1	přesná
<g/>
,	,	kIx,	,
neměly	mít	k5eNaImAgFnP	mít
však	však	k9	však
velký	velký	k2eAgInSc4d1	velký
dostřel	dostřel	k1gInSc4	dostřel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luk	luk	k1gInSc1	luk
indiánů	indián	k1gMnPc2	indián
Janomamů	Janomam	k1gInPc2	Janomam
z	z	k7c2	z
orinockých	orinocký	k2eAgInPc2d1	orinocký
pralesů	prales	k1gInPc2	prales
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
–	–	k?	–
asi	asi	k9	asi
2	[number]	k4	2
metry	metr	k1gInPc4	metr
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
šípy	šíp	k1gInPc4	šíp
s	s	k7c7	s
otráveným	otrávený	k2eAgInSc7d1	otrávený
hrotem	hrot	k1gInSc7	hrot
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
amazonští	amazonský	k2eAgMnPc1d1	amazonský
indiáni	indián	k1gMnPc1	indián
lukem	luk	k1gInSc7	luk
dosud	dosud	k6eAd1	dosud
loví	lovit	k5eAaImIp3nP	lovit
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
opice	opice	k1gFnPc4	opice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Znají	znát	k5eAaImIp3nP	znát
i	i	k9	i
šípy	šíp	k1gInPc1	šíp
zápalné	zápalný	k2eAgInPc1d1	zápalný
a	a	k8xC	a
hvízdající	hvízdající	k2eAgInPc1d1	hvízdající
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tupé	tupý	k2eAgInPc4d1	tupý
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
lze	lze	k6eAd1	lze
kořist	kořist	k1gFnSc4	kořist
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
ptáky	pták	k1gMnPc4	pták
<g/>
)	)	kIx)	)
jen	jen	k9	jen
omráčit	omráčit	k5eAaPmF	omráčit
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejkrásnější	krásný	k2eAgInPc1d3	nejkrásnější
luky	luk	k1gInPc1	luk
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
ozdobené	ozdobený	k2eAgInPc4d1	ozdobený
složitými	složitý	k2eAgInPc7d1	složitý
obrazci	obrazec	k1gInPc7	obrazec
z	z	k7c2	z
barveného	barvený	k2eAgNnSc2d1	barvené
lýka	lýko	k1gNnSc2	lýko
a	a	k8xC	a
bavlněné	bavlněný	k2eAgFnSc2d1	bavlněná
příze	příz	k1gFnSc2	příz
zhotovovali	zhotovovat	k5eAaImAgMnP	zhotovovat
indiáni	indián	k1gMnPc1	indián
Šipivové	Šipiv	k1gMnPc1	Šipiv
z	z	k7c2	z
peruánské	peruánský	k2eAgFnSc2d1	peruánská
Amazonie	Amazonie	k1gFnSc2	Amazonie
<g/>
.	.	kIx.	.
</s>
<s>
Šamani	šaman	k1gMnPc1	šaman
amazonských	amazonský	k2eAgInPc2d1	amazonský
kmenů	kmen	k1gInPc2	kmen
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
rituálním	rituální	k2eAgInPc3d1	rituální
účelům	účel	k1gInPc3	účel
maličké	maličká	k1gFnSc2	maličká
luky	luk	k1gInPc4	luk
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgNnSc1d1	připomínající
spíš	spíš	k9	spíš
dětské	dětský	k2eAgFnPc4d1	dětská
hračky	hračka	k1gFnPc4	hračka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
luk	luk	k1gInSc4	luk
a	a	k8xC	a
šípy	šíp	k1gInPc4	šíp
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
naší	náš	k3xOp1gFnSc3	náš
představě	představa	k1gFnSc3	představa
indiána	indián	k1gMnSc2	indián
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
především	především	k9	především
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
luk	luk	k1gInSc1	luk
nikdy	nikdy	k6eAd1	nikdy
nepoužívaly	používat	k5eNaImAgFnP	používat
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
vrhači	vrhač	k1gInSc6	vrhač
oštěpů	oštěp	k1gInPc2	oštěp
(	(	kIx(	(
<g/>
atlaltl	atlaltnout	k5eAaPmAgInS	atlaltnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
praku	prak	k1gInSc3	prak
či	či	k8xC	či
foukačce	foukačka	k1gFnSc3	foukačka
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
např.	např.	kA	např.
Kofánové	Kofán	k1gMnPc1	Kofán
<g/>
,	,	kIx,	,
Chívarové	Chívar	k1gMnPc1	Chívar
<g/>
,	,	kIx,	,
Huitotové	Huitot	k1gMnPc1	Huitot
či	či	k8xC	či
Jaguové	Jaguus	k1gMnPc1	Jaguus
<g/>
.	.	kIx.	.
</s>
<s>
Andští	andský	k2eAgMnPc1d1	andský
indiáni	indián	k1gMnPc1	indián
včetně	včetně	k7c2	včetně
Močiků	Močik	k1gMnPc2	Močik
a	a	k8xC	a
Inků	Ink	k1gMnPc2	Ink
luk	louka	k1gFnPc2	louka
sice	sice	k8xC	sice
znali	znát	k5eAaImAgMnP	znát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dávali	dávat	k5eAaImAgMnP	dávat
přednost	přednost	k1gFnSc4	přednost
praku	prak	k1gInSc2	prak
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
k	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Aztékové	Azték	k1gMnPc1	Azték
zase	zase	k9	zase
v	v	k7c6	v
boji	boj	k1gInSc6	boj
upřednostňovali	upřednostňovat	k5eAaImAgMnP	upřednostňovat
vrhač	vrhač	k1gInSc4	vrhač
oštěpů	oštěp	k1gInPc2	oštěp
(	(	kIx(	(
<g/>
atlatl	atlatnout	k5eAaPmAgInS	atlatnout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc4	slovo
atlatl	atlatnout	k5eAaPmAgInS	atlatnout
přitom	přitom	k6eAd1	přitom
pochází	pocházet	k5eAaImIp3nS	pocházet
právě	právě	k9	právě
z	z	k7c2	z
aztéckého	aztécký	k2eAgInSc2d1	aztécký
jazyka	jazyk	k1gInSc2	jazyk
náhuatl	náhuatnout	k5eAaPmAgInS	náhuatnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luky	luk	k1gInPc1	luk
a	a	k8xC	a
šípy	šíp	k1gInPc1	šíp
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
používány	používat	k5eAaImNgFnP	používat
až	až	k9	až
v	v	k7c4	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Předtím	předtím	k6eAd1	předtím
používali	používat	k5eAaImAgMnP	používat
předkové	předek	k1gMnPc1	předek
indiánů	indián	k1gMnPc2	indián
pouze	pouze	k6eAd1	pouze
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
vrhač	vrhač	k1gInSc4	vrhač
oštěpů	oštěp	k1gInPc2	oštěp
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
atlatl	atlatnout	k5eAaPmAgMnS	atlatnout
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vymršťovaly	vymršťovat	k5eAaImAgInP	vymršťovat
kratší	krátký	k2eAgInPc1d2	kratší
oštěpy	oštěp	k1gInPc1	oštěp
a	a	k8xC	a
kopí	kopit	k5eAaImIp3nP	kopit
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
účinností	účinnost	k1gFnSc7	účinnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
kdyby	kdyby	kYmCp3nS	kdyby
se	se	k3xPyFc4	se
vrhala	vrhat	k5eAaImAgFnS	vrhat
běžným	běžný	k2eAgInSc7d1	běžný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
atlatlu	atlatl	k1gInSc6	atlatl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
po	po	k7c6	po
technické	technický	k2eAgFnSc6d1	technická
stránce	stránka	k1gFnSc6	stránka
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrhací	vrhací	k2eAgFnSc1d1	vrhací
síla	síla	k1gFnSc1	síla
nepůsobí	působit	k5eNaImIp3nS	působit
v	v	k7c6	v
těžišti	těžiště	k1gNnSc6	těžiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
konci	konec	k1gInSc6	konec
vrhaného	vrhaný	k2eAgNnSc2d1	vrhané
kopí	kopí	k1gNnSc2	kopí
nebo	nebo	k8xC	nebo
oštěpu	oštěp	k1gInSc2	oštěp
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
i	i	k9	i
u	u	k7c2	u
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
u	u	k7c2	u
šípu	šíp	k1gInSc2	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
vumera	vumera	k1gFnSc1	vumera
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
woomera	woomera	k1gFnSc1	woomera
<g/>
)	)	kIx)	)
používají	používat	k5eAaImIp3nP	používat
vrhač	vrhač	k1gInSc4	vrhač
oštěpů	oštěp	k1gInPc2	oštěp
také	také	k6eAd1	také
australší	australší	k2eAgMnPc1d1	australší
Aboriginci	Aboriginec	k1gMnPc1	Aboriginec
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
luk	luk	k1gInSc1	luk
nikdy	nikdy	k6eAd1	nikdy
nerozšířil	rozšířit	k5eNaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Středověká	středověký	k2eAgFnSc1d1	středověká
Evropa	Evropa	k1gFnSc1	Evropa
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
středověk	středověk	k1gInSc4	středověk
<g/>
,	,	kIx,	,
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
Stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
až	až	k6eAd1	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
luk	louka	k1gFnPc2	louka
důležitou	důležitý	k2eAgFnSc4d1	důležitá
loveckou	lovecký	k2eAgFnSc4d1	lovecká
a	a	k8xC	a
válečnou	válečný	k2eAgFnSc4d1	válečná
zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
začal	začít	k5eAaPmAgMnS	začít
být	být	k5eAaImF	být
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentě	kontinent	k1gInSc6	kontinent
vytlačován	vytlačován	k2eAgInSc4d1	vytlačován
kuší	kuše	k1gFnSc7	kuše
a	a	k8xC	a
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
palnými	palný	k2eAgFnPc7d1	palná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Vynikajícími	vynikající	k2eAgMnPc7d1	vynikající
lučištníky	lučištník	k1gMnPc7	lučištník
byli	být	k5eAaImAgMnP	být
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
používající	používající	k2eAgMnPc1d1	používající
podle	podle	k7c2	podle
byzantských	byzantský	k2eAgInPc2d1	byzantský
pramenů	pramen	k1gInPc2	pramen
dokonce	dokonce	k9	dokonce
otrávené	otrávený	k2eAgInPc4d1	otrávený
šípy	šíp	k1gInPc4	šíp
<g/>
,	,	kIx,	,
keltští	keltský	k2eAgMnPc1d1	keltský
Velšané	Velšan	k1gMnPc1	Velšan
a	a	k8xC	a
Skoti	Skot	k1gMnPc1	Skot
i	i	k8xC	i
germánští	germánský	k2eAgMnPc1d1	germánský
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
Dánové	Dán	k1gMnPc1	Dán
a	a	k8xC	a
Vikingové	Viking	k1gMnPc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
Velšanů	Velšan	k1gMnPc2	Velšan
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
tisu	tis	k1gInSc2	tis
<g/>
,	,	kIx,	,
luky	luk	k1gInPc1	luk
Sasů	Sas	k1gMnPc2	Sas
byly	být	k5eAaImAgInP	být
zhotoveny	zhotovit	k5eAaPmNgInP	zhotovit
z	z	k7c2	z
jilmu	jilm	k1gInSc2	jilm
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
až	až	k9	až
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
mimořádně	mimořádně	k6eAd1	mimořádně
silné	silný	k2eAgFnPc1d1	silná
a	a	k8xC	a
průbojné	průbojný	k2eAgFnPc1d1	průbojná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
kratší	krátký	k2eAgFnSc6d2	kratší
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
předchůdce	předchůdce	k1gMnPc4	předchůdce
pověstných	pověstný	k2eAgInPc2d1	pověstný
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
luků	luk	k1gInPc2	luk
Angličanů	Angličan	k1gMnPc2	Angličan
ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
etnonymum	etnonymum	k1gNnSc1	etnonymum
Skotů	Skot	k1gMnPc2	Skot
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
od	od	k7c2	od
staroanglického	staroanglický	k2eAgMnSc2d1	staroanglický
"	"	kIx"	"
<g/>
sciot	sciot	k1gInSc1	sciot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
lukostřelec	lukostřelec	k1gMnSc1	lukostřelec
<g/>
.	.	kIx.	.
</s>
<s>
Výborně	výborně	k6eAd1	výborně
vycvičené	vycvičený	k2eAgInPc1d1	vycvičený
oddíly	oddíl	k1gInPc1	oddíl
lučištníků	lučištník	k1gMnPc2	lučištník
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
Normané	Norman	k1gMnPc1	Norman
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Hastingsu	Hastings	k1gInSc2	Hastings
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
kromě	kromě	k7c2	kromě
normanské	normanský	k2eAgFnSc2d1	normanská
těžké	těžký	k2eAgFnSc2d1	těžká
jízdy	jízda	k1gFnSc2	jízda
také	také	k9	také
lučištníci	lučištník	k1gMnPc1	lučištník
vévody	vévoda	k1gMnSc2	vévoda
Viléma	Vilém	k1gMnSc2	Vilém
Dobyvatele	dobyvatel	k1gMnSc2	dobyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luk	luk	k1gInSc1	luk
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
středověké	středověký	k2eAgFnSc6d1	středověká
Evropě	Evropa	k1gFnSc6	Evropa
pokládán	pokládat	k5eAaImNgMnS	pokládat
za	za	k7c4	za
méně	málo	k6eAd2	málo
ušlechtilou	ušlechtilý	k2eAgFnSc4d1	ušlechtilá
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
než	než	k8xS	než
kopí	kopit	k5eAaImIp3nS	kopit
nebo	nebo	k8xC	nebo
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jím	on	k3xPp3gMnSc7	on
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zabíjet	zabíjet	k5eAaImF	zabíjet
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Výstřel	výstřel	k1gInSc1	výstřel
šípu	šíp	k1gInSc2	šíp
ukončil	ukončit	k5eAaPmAgInS	ukončit
život	život	k1gInSc1	život
řady	řada	k1gFnSc2	řada
významných	významný	k2eAgMnPc2d1	významný
válečníků	válečník	k1gMnPc2	válečník
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Oddíl	oddíl	k1gInSc1	oddíl
franckého	francký	k2eAgMnSc2d1	francký
markraběte	markrabě	k1gMnSc2	markrabě
Rolanda	Rolando	k1gNnSc2	Rolando
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
Pyrenejí	Pyreneje	k1gFnPc2	Pyreneje
zničen	zničit	k5eAaPmNgInS	zničit
útokem	útok	k1gInSc7	útok
Basků	Bask	k1gMnPc2	Bask
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Franky	Franky	k1gInPc4	Franky
<g/>
,	,	kIx,	,
táhnoucí	táhnoucí	k2eAgNnSc1d1	táhnoucí
soutěskou	soutěska	k1gFnSc7	soutěska
<g/>
,	,	kIx,	,
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
skal	skála	k1gFnPc2	skála
zabíjeli	zabíjet	k5eAaImAgMnP	zabíjet
šípy	šíp	k1gInPc4	šíp
a	a	k8xC	a
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Richard	Richard	k1gMnSc1	Richard
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnSc4	srdce
zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnPc2	zranění
šípem	šíp	k1gInSc7	šíp
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
jednoho	jeden	k4xCgInSc2	jeden
hradu	hrad	k1gInSc2	hrad
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
kladenou	kladený	k2eAgFnSc7d1	kladená
otázkou	otázka	k1gFnSc7	otázka
bývá	bývat	k5eAaImIp3nS	bývat
bojová	bojový	k2eAgFnSc1d1	bojová
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
luky	luk	k1gInPc1	luk
používaly	používat	k5eAaImAgInP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
to	ten	k3xDgNnSc1	ten
bývalo	bývat	k5eAaImAgNnS	bývat
na	na	k7c4	na
asi	asi	k9	asi
180	[number]	k4	180
až	až	k9	až
210	[number]	k4	210
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
salvě	salva	k1gFnSc6	salva
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgInPc1d1	Dlouhé
luky	luk	k1gInPc1	luk
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
až	až	k9	až
o	o	k7c4	o
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
dál	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
dostřelily	dostřelit	k5eAaPmAgFnP	dostřelit
kuše	kuše	k1gFnPc1	kuše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Crésy	Crésa	k1gFnSc2	Crésa
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
střeleckou	střelecký	k2eAgFnSc7d1	střelecká
silou	síla	k1gFnSc7	síla
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
straně	strana	k1gFnSc6	strana
najatí	najatý	k2eAgMnPc1d1	najatý
benátští	benátský	k2eAgMnPc1d1	benátský
střelci	střelec	k1gMnPc1	střelec
z	z	k7c2	z
kuší	kuše	k1gFnPc2	kuše
s	s	k7c7	s
dosahem	dosah	k1gInSc7	dosah
asi	asi	k9	asi
240	[number]	k4	240
až	až	k9	až
250	[number]	k4	250
m.	m.	k?	m.
Díky	díky	k7c3	díky
zvlhlým	zvlhlý	k2eAgFnPc3d1	zvlhlá
tětivám	tětiva	k1gFnPc3	tětiva
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
kuše	kuše	k1gFnPc1	kuše
střílely	střílet	k5eAaImAgFnP	střílet
o	o	k7c4	o
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
méně	málo	k6eAd2	málo
a	a	k8xC	a
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
přestříleni	přestřílet	k5eAaPmNgMnP	přestřílet
anglickými	anglický	k2eAgMnPc7d1	anglický
lukostřelci	lukostřelec	k1gMnPc7	lukostřelec
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
luky	luk	k1gInPc7	luk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
benátské	benátský	k2eAgInPc4d1	benátský
oddíly	oddíl	k1gInPc4	oddíl
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Syn	syn	k1gMnSc1	syn
čingischánova	čingischánův	k2eAgMnSc2d1	čingischánův
bratra	bratr	k1gMnSc2	bratr
jménem	jméno	k1gNnSc7	jméno
Yesüngge	Yesüngg	k1gFnSc2	Yesüngg
dokázal	dokázat	k5eAaPmAgInS	dokázat
na	na	k7c6	na
mongolské	mongolský	k2eAgFnSc6d1	mongolská
národní	národní	k2eAgFnSc6d1	národní
slavnosti	slavnost	k1gFnSc6	slavnost
vyslat	vyslat	k5eAaPmF	vyslat
šíp	šíp	k1gInSc4	šíp
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
536	[number]	k4	536
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
historicky	historicky	k6eAd1	historicky
uznávaný	uznávaný	k2eAgInSc1d1	uznávaný
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
středověký	středověký	k2eAgInSc1d1	středověký
mongolský	mongolský	k2eAgInSc1d1	mongolský
rekord	rekord	k1gInSc1	rekord
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
wikipedie	wikipedie	k1gFnSc1	wikipedie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novější	nový	k2eAgFnSc6d2	novější
historii	historie	k1gFnSc6	historie
je	být	k5eAaImIp3nS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
mnoho	mnoho	k4c1	mnoho
pokusů	pokus	k1gInPc2	pokus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tuto	tento	k3xDgFnSc4	tento
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
překonaly	překonat	k5eAaPmAgFnP	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
soudobého	soudobý	k2eAgInSc2d1	soudobý
luku	luk	k1gInSc2	luk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hranice	hranice	k1gFnSc1	hranice
dostřelu	dostřel	k1gInSc2	dostřel
někde	někde	k6eAd1	někde
na	na	k7c6	na
dvojnásobku	dvojnásobek	k1gInSc6	dvojnásobek
uvedené	uvedený	k2eAgFnSc2d1	uvedená
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Luk	luk	k1gInSc1	luk
býval	bývat	k5eAaImAgInS	bývat
spojován	spojován	k2eAgMnSc1d1	spojován
spíše	spíše	k9	spíše
s	s	k7c7	s
nižšími	nízký	k2eAgFnPc7d2	nižší
vrstvami	vrstva	k1gFnPc7	vrstva
než	než	k8xS	než
s	s	k7c7	s
rytíři	rytíř	k1gMnPc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Legendárním	legendární	k2eAgMnSc7d1	legendární
lučištníkem	lučištník	k1gMnSc7	lučištník
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
anglický	anglický	k2eAgMnSc1d1	anglický
zbojník	zbojník	k1gMnSc1	zbojník
Robin	Robina	k1gFnPc2	Robina
Hood	Hood	k1gMnSc1	Hood
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
obdivován	obdivován	k2eAgMnSc1d1	obdivován
i	i	k8xC	i
svými	svůj	k3xOyFgMnPc7	svůj
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
domovem	domov	k1gInSc7	domov
byl	být	k5eAaImAgMnS	být
Sherwoodský	Sherwoodský	k2eAgInSc4d1	Sherwoodský
les	les	k1gInSc4	les
a	a	k8xC	a
přežil	přežít	k5eAaPmAgMnS	přežít
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vládu	vláda	k1gFnSc4	vláda
králů	král	k1gMnPc2	král
Jindřicha	Jindřich	k1gMnSc4	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Richarda	Richard	k1gMnSc2	Richard
Lví	lví	k2eAgNnSc4d1	lví
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Bezzemka	bezzemek	k1gMnSc2	bezzemek
a	a	k8xC	a
Jindřicha	Jindřich	k1gMnSc2	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opatství	opatství	k1gNnSc6	opatství
Kirklerském	Kirklerský	k2eAgNnSc6d1	Kirklerský
je	být	k5eAaImIp3nS	být
dosud	dosud	k6eAd1	dosud
hrob	hrob	k1gInSc1	hrob
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
pravé	pravý	k2eAgNnSc1d1	pravé
jméno	jméno	k1gNnSc1	jméno
tohoto	tento	k3xDgMnSc2	tento
zbojníka	zbojník	k1gMnSc2	zbojník
-	-	kIx~	-
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Huntingtonu	Huntington	k1gInSc2	Huntington
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
vystřelil	vystřelit	k5eAaPmAgMnS	vystřelit
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
poslední	poslední	k2eAgInSc4d1	poslední
šíp	šíp	k1gInSc4	šíp
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
tento	tento	k3xDgInSc4	tento
šíp	šíp	k1gInSc4	šíp
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Walter	Walter	k1gMnSc1	Walter
Scott	Scott	k1gMnSc1	Scott
popisuje	popisovat	k5eAaImIp3nS	popisovat
Robinovu	Robinův	k2eAgFnSc4d1	Robinova
zručnost	zručnost	k1gFnSc4	zručnost
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
v	v	k7c6	v
románu	román	k1gInSc6	román
Ivanhoe	Ivanho	k1gFnSc2	Ivanho
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
většina	většina	k1gFnSc1	většina
odborníků	odborník	k1gMnPc2	odborník
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Robin	robin	k2eAgInSc1d1	robin
Hood	Hood	k1gInSc1	Hood
nebyl	být	k5eNaImAgInS	být
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
historickou	historický	k2eAgFnSc7d1	historická
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíš	spíš	k9	spíš
mýtickou	mýtický	k2eAgFnSc7d1	mýtická
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
mající	mající	k2eAgFnSc7d1	mající
za	za	k7c4	za
předlohu	předloha	k1gFnSc4	předloha
několik	několik	k4yIc4	několik
historických	historický	k2eAgFnPc2d1	historická
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
používali	používat	k5eAaImAgMnP	používat
luku	luk	k1gInSc2	luk
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
armády	armáda	k1gFnPc1	armáda
evropského	evropský	k2eAgInSc2d1	evropský
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
pěší	pěší	k2eAgMnPc1d1	pěší
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
vyzbrojení	vyzbrojení	k1gNnSc1	vyzbrojení
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
luky	luk	k1gInPc7	luk
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
proti	proti	k7c3	proti
Skotům	Skot	k1gMnPc3	Skot
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
stoleté	stoletý	k2eAgFnSc2d1	stoletá
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Francouzům	Francouz	k1gMnPc3	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
příkladům	příklad	k1gInPc3	příklad
patří	patřit	k5eAaImIp3nS	patřit
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
Azincourtu	Azincourt	k1gInSc2	Azincourt
<g/>
.	.	kIx.	.
</s>
<s>
Lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
plnili	plnit	k5eAaImAgMnP	plnit
úkol	úkol	k1gInSc4	úkol
"	"	kIx"	"
<g/>
střelecké	střelecký	k2eAgFnSc2d1	střelecká
přípravy	příprava	k1gFnSc2	příprava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
měli	mít	k5eAaImAgMnP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
rozvrátit	rozvrátit	k5eAaPmF	rozvrátit
šiky	šik	k1gInPc4	šik
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Salvám	salva	k1gFnPc3	salva
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
padajících	padající	k2eAgInPc2d1	padající
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
průrazností	průraznost	k1gFnSc7	průraznost
a	a	k8xC	a
často	často	k6eAd1	často
za	za	k7c4	za
ochranné	ochranný	k2eAgInPc4d1	ochranný
štíty	štít	k1gInPc4	štít
<g/>
,	,	kIx,	,
odolal	odolat	k5eAaPmAgMnS	odolat
málokdo	málokdo	k3yInSc1	málokdo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kronikáře	kronikář	k1gMnSc2	kronikář
Froissarta	Froissart	k1gMnSc2	Froissart
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
lučištník	lučištník	k1gMnSc1	lučištník
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
30	[number]	k4	30
kroků	krok	k1gInPc2	krok
schopen	schopen	k2eAgMnSc1d1	schopen
prostřelit	prostřelit	k5eAaPmF	prostřelit
stehno	stehno	k1gNnSc4	stehno
rytíře	rytíř	k1gMnSc2	rytíř
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgNnSc1d1	chráněné
plátovou	plátový	k2eAgFnSc7d1	plátová
zbrojí	zbroj	k1gFnSc7	zbroj
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
usmrtit	usmrtit	k5eAaPmF	usmrtit
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
rytíř	rytíř	k1gMnSc1	rytíř
seděl	sedět	k5eAaImAgMnS	sedět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byla	být	k5eAaImAgFnS	být
také	také	k9	také
vydána	vydat	k5eAaPmNgFnS	vydat
první	první	k4xOgFnSc1	první
známá	známý	k2eAgFnSc1d1	známá
kniha	kniha	k1gFnSc1	kniha
popisující	popisující	k2eAgFnSc1d1	popisující
způsob	způsob	k1gInSc4	způsob
výcviku	výcvik	k1gInSc2	výcvik
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
z	z	k7c2	z
luku	luk	k1gInSc2	luk
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytištěna	vytisknout	k5eAaPmNgFnS	vytisknout
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
Roger	Rogero	k1gNnPc2	Rogero
Ashamov	Ashamov	k1gInSc1	Ashamov
byl	být	k5eAaImAgInS	být
odměněn	odměnit	k5eAaPmNgInS	odměnit
roční	roční	k2eAgFnSc7d1	roční
penzí	penze	k1gFnSc7	penze
10	[number]	k4	10
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
rozkazu	rozkaz	k1gInSc2	rozkaz
panovníka	panovník	k1gMnSc2	panovník
povinností	povinnost	k1gFnSc7	povinnost
každého	každý	k3xTgMnSc2	každý
občana	občan	k1gMnSc2	občan
cvičit	cvičit	k5eAaImF	cvičit
se	se	k3xPyFc4	se
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
lukem	luk	k1gInSc7	luk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většina	většina	k1gFnSc1	většina
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
používala	používat	k5eAaImAgFnS	používat
např.	např.	kA	např.
při	při	k7c6	při
námořních	námořní	k2eAgFnPc6d1	námořní
bitvách	bitva	k1gFnPc6	bitva
<g/>
,	,	kIx,	,
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
na	na	k7c4	na
kratší	krátký	k2eAgFnSc4d2	kratší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
pistole	pistol	k1gFnSc2	pistol
<g/>
,	,	kIx,	,
bojovali	bojovat	k5eAaImAgMnP	bojovat
angličtí	anglický	k2eAgMnPc1d1	anglický
námořníci	námořník	k1gMnPc1	námořník
luky	luk	k1gInPc4	luk
a	a	k8xC	a
šípy	šíp	k1gInPc4	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
byl	být	k5eAaImAgInS	být
luk	luk	k1gInSc1	luk
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
účelům	účel	k1gInPc3	účel
za	za	k7c2	za
anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
přežil	přežít	k5eAaPmAgMnS	přežít
dokonce	dokonce	k9	dokonce
až	až	k9	až
do	do	k7c2	do
jakobitského	jakobitský	k2eAgNnSc2d1	jakobitské
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
lučištníci	lučištník	k1gMnPc1	lučištník
bojovali	bojovat	k5eAaImAgMnP	bojovat
ještě	ještě	k6eAd1	ještě
u	u	k7c2	u
Cullodenu	Culloden	k1gInSc2	Culloden
roku	rok	k1gInSc2	rok
1746	[number]	k4	1746
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
používali	používat	k5eAaImAgMnP	používat
luk	luk	k1gInSc4	luk
také	také	k9	také
polští	polský	k2eAgMnPc1d1	polský
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
v	v	k7c6	v
Sienkiewiczově	Sienkiewiczův	k2eAgInSc6d1	Sienkiewiczův
románu	román	k1gInSc6	román
Pan	Pan	k1gMnSc1	Pan
Wolodyjowski	Wolodyjowsk	k1gFnSc2	Wolodyjowsk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
říká	říkat	k5eAaImIp3nS	říkat
starý	starý	k2eAgMnSc1d1	starý
voják	voják	k1gMnSc1	voják
Muszalski	Muszalsk	k1gFnSc2	Muszalsk
<g/>
,	,	kIx,	,
že	že	k8xS	že
luk	luk	k1gInSc1	luk
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
pistole	pistole	k1gFnSc1	pistole
a	a	k8xC	a
střílí	střílet	k5eAaImIp3nS	střílet
lukem	luk	k1gInSc7	luk
Turky	Turek	k1gMnPc4	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
přežili	přežít	k5eAaPmAgMnP	přežít
lučištníci	lučištník	k1gMnPc1	lučištník
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
vojsku	vojsko	k1gNnSc6	vojsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bojovali	bojovat	k5eAaImAgMnP	bojovat
ještě	ještě	k9	ještě
v	v	k7c6	v
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jízdní	jízdní	k2eAgInPc4d1	jízdní
oddíly	oddíl	k1gInPc4	oddíl
Baškirů	Baškir	k1gMnPc2	Baškir
a	a	k8xC	a
Kalmyků	Kalmyk	k1gMnPc2	Kalmyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cechy	cech	k1gInPc1	cech
lukařů	lukař	k1gMnPc2	lukař
byly	být	k5eAaImAgInP	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
velmi	velmi	k6eAd1	velmi
početné	početný	k2eAgMnPc4d1	početný
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
dovednost	dovednost	k1gFnSc4	dovednost
ve	v	k7c4	v
zhotovování	zhotovování	k1gNnSc4	zhotovování
luků	luk	k1gInPc2	luk
a	a	k8xC	a
šípů	šíp	k1gInPc2	šíp
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
váženými	vážený	k2eAgMnPc7d1	vážený
řemeslníky	řemeslník	k1gMnPc7	řemeslník
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
doložen	doložit	k5eAaPmNgInS	doložit
i	i	k9	i
počátek	počátek	k1gInSc4	počátek
lukostřeleckého	lukostřelecký	k2eAgInSc2d1	lukostřelecký
sportu	sport	k1gInSc2	sport
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
ptačí	ptačí	k2eAgFnSc2d1	ptačí
střelby	střelba	k1gFnSc2	střelba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
události	událost	k1gFnPc1	událost
se	se	k3xPyFc4	se
pořádaly	pořádat	k5eAaImAgFnP	pořádat
o	o	k7c6	o
Letnicích	letnice	k1gFnPc6	letnice
a	a	k8xC	a
střelci	střelec	k1gMnPc1	střelec
při	při	k7c6	při
nich	on	k3xPp3gInPc6	on
sestřelovali	sestřelovat	k5eAaImAgMnP	sestřelovat
dřevěnou	dřevěný	k2eAgFnSc4d1	dřevěná
figurku	figurka	k1gFnSc4	figurka
ptáka	pták	k1gMnSc2	pták
s	s	k7c7	s
roztaženými	roztažený	k2eAgNnPc7d1	roztažené
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yInSc1	kdo
srazil	srazit	k5eAaPmAgMnS	srazit
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
"	"	kIx"	"
<g/>
pravým	pravý	k2eAgMnSc7d1	pravý
pobočníkem	pobočník	k1gMnSc7	pobočník
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
levé	levý	k2eAgNnSc1d1	levé
křídlo	křídlo	k1gNnSc1	křídlo
"	"	kIx"	"
<g/>
levým	levý	k2eAgMnSc7d1	levý
pobočníkem	pobočník	k1gMnSc7	pobočník
<g/>
"	"	kIx"	"
a	a	k8xC	a
střelec	střelec	k1gMnSc1	střelec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
zbytek	zbytek	k1gInSc4	zbytek
ptáka	pták	k1gMnSc2	pták
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
"	"	kIx"	"
<g/>
ptačím	ptačí	k2eAgMnSc7d1	ptačí
králem	král	k1gMnSc7	král
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
králem	král	k1gMnSc7	král
střelců	střelec	k1gMnPc2	střelec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Výhry	výhra	k1gFnPc1	výhra
byly	být	k5eAaImAgFnP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
i	i	k9	i
s	s	k7c7	s
hmotnými	hmotný	k2eAgFnPc7d1	hmotná
cenami	cena	k1gFnPc7	cena
<g/>
,	,	kIx,	,
např.	např.	kA	např.
voly	vůl	k1gMnPc7	vůl
nebo	nebo	k8xC	nebo
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
sport	sport	k1gInSc1	sport
doložen	doložit	k5eAaPmNgInS	doložit
ve	v	k7c6	v
Flandrech	Flandry	k1gInPc6	Flandry
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
i	i	k8xC	i
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnPc1d1	římská
<g/>
,	,	kIx,	,
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	s	k7c7	s
"	"	kIx"	"
<g/>
střelba	střelba	k1gFnSc1	střelba
ku	k	k7c3	k
ptáku	pták	k1gMnSc3	pták
<g/>
"	"	kIx"	"
pořádala	pořádat	k5eAaImAgFnS	pořádat
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
střelecké	střelecký	k2eAgFnPc1d1	střelecká
soutěže	soutěž	k1gFnPc1	soutěž
přežily	přežít	k5eAaPmAgFnP	přežít
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
místo	místo	k7c2	místo
luků	luk	k1gInPc2	luk
používaly	používat	k5eAaImAgFnP	používat
kuše	kuše	k1gFnPc1	kuše
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
pušky	puška	k1gFnSc2	puška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Novověk	novověk	k1gInSc4	novověk
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
éra	éra	k1gFnSc1	éra
luku	luk	k1gInSc2	luk
jako	jako	k8xC	jako
válečné	válečný	k2eAgFnPc1d1	válečná
a	a	k8xC	a
lovecké	lovecký	k2eAgFnPc1d1	lovecká
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
jej	on	k3xPp3gMnSc4	on
vytlačily	vytlačit	k5eAaPmAgFnP	vytlačit
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
výzbroje	výzbroj	k1gFnSc2	výzbroj
palné	palný	k2eAgFnSc2d1	palná
zbraně	zbraň	k1gFnSc2	zbraň
také	také	k9	také
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
Persii	Persie	k1gFnSc6	Persie
a	a	k8xC	a
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
přestal	přestat	k5eAaPmAgMnS	přestat
být	být	k5eAaImF	být
používám	používat	k5eAaImIp1nS	používat
také	také	k9	také
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Arábii	Arábie	k1gFnSc6	Arábie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
používali	používat	k5eAaImAgMnP	používat
luk	luk	k1gInSc4	luk
severoameričtí	severoamerický	k2eAgMnPc1d1	severoamerický
a	a	k8xC	a
patagonští	patagonský	k2eAgMnPc1d1	patagonský
indiáni	indián	k1gMnPc1	indián
<g/>
,	,	kIx,	,
živou	živý	k2eAgFnSc7d1	živá
zbraní	zbraň	k1gFnSc7	zbraň
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
luk	luk	k1gInSc1	luk
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradice	tradice	k1gFnSc1	tradice
moderní	moderní	k2eAgFnSc2d1	moderní
sportovní	sportovní	k2eAgFnSc2d1	sportovní
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
romantismem	romantismus	k1gInSc7	romantismus
a	a	k8xC	a
obnovou	obnova	k1gFnSc7	obnova
zájmu	zájem	k1gInSc2	zájem
o	o	k7c4	o
středověk	středověk	k1gInSc4	středověk
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
sportovní	sportovní	k2eAgFnSc1d1	sportovní
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
šířila	šířit	k5eAaImAgFnS	šířit
šířila	šířit	k5eAaImAgFnS	šířit
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
kontinent	kontinent	k1gInSc4	kontinent
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Francie	Francie	k1gFnSc2	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1828	[number]	k4	1828
první	první	k4xOgInSc1	první
klub	klub	k1gInSc1	klub
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
v	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prestižním	prestižní	k2eAgInSc7d1	prestižní
a	a	k8xC	a
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
dokonce	dokonce	k9	dokonce
olympijským	olympijský	k2eAgInSc7d1	olympijský
sportem	sport	k1gInSc7	sport
<g/>
.	.	kIx.	.
</s>
<s>
Střílí	střílet	k5eAaImIp3nS	střílet
se	se	k3xPyFc4	se
sportovním	sportovní	k2eAgInSc7d1	sportovní
zvratným	zvratný	k2eAgInSc7d1	zvratný
reflexním	reflexní	k2eAgInSc7d1	reflexní
kompozitním	kompozitní	k2eAgInSc7d1	kompozitní
lukem	luk	k1gInSc7	luk
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
olympijský	olympijský	k2eAgInSc1d1	olympijský
luk	luk	k1gInSc1	luk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
lovecká	lovecký	k2eAgFnSc1d1	lovecká
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
USA	USA	kA	USA
až	až	k9	až
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Její	její	k3xOp3gFnSc4	její
historie	historie	k1gFnSc1	historie
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
americký	americký	k2eAgMnSc1d1	americký
lékař	lékař	k1gMnSc1	lékař
Saxton	Saxton	k1gInSc4	Saxton
Pope	pop	k1gInSc5	pop
léčil	léčit	k5eAaImAgMnS	léčit
nemocného	nemocný	k2eAgMnSc4d1	nemocný
indiána	indián	k1gMnSc4	indián
Ishiho	Ishi	k1gMnSc4	Ishi
z	z	k7c2	z
kalifornského	kalifornský	k2eAgInSc2d1	kalifornský
kmene	kmen	k1gInSc2	kmen
Yani	Yan	k1gFnSc2	Yan
<g/>
.	.	kIx.	.
</s>
<s>
Pope	pop	k1gMnSc5	pop
se	se	k3xPyFc4	se
od	od	k7c2	od
indiánského	indiánský	k2eAgMnSc2d1	indiánský
lovce	lovec	k1gMnSc2	lovec
naučil	naučit	k5eAaPmAgMnS	naučit
používat	používat	k5eAaImF	používat
tradiční	tradiční	k2eAgInSc4d1	tradiční
luk	luk	k1gInSc4	luk
a	a	k8xC	a
lovit	lovit	k5eAaImF	lovit
jím	jíst	k5eAaImIp1nS	jíst
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
Lov	lov	k1gInSc1	lov
lukem	luk	k1gInSc7	luk
a	a	k8xC	a
šípem	šíp	k1gInSc7	šíp
<g/>
"	"	kIx"	"
a	a	k8xC	a
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
o	o	k7c6	o
lovu	lov	k1gInSc6	lov
lukem	luk	k1gInSc7	luk
natočeny	natočit	k5eAaBmNgFnP	natočit
<g/>
,	,	kIx,	,
vzbudily	vzbudit	k5eAaPmAgFnP	vzbudit
opět	opět	k6eAd1	opět
zájem	zájem	k1gInSc4	zájem
americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
o	o	k7c4	o
lukostřelbu	lukostřelba	k1gFnSc4	lukostřelba
<g/>
.	.	kIx.	.
</s>
<s>
Lovecká	lovecký	k2eAgFnSc1d1	lovecká
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
USA	USA	kA	USA
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
a	a	k8xC	a
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
i	i	k9	i
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
tak	tak	k6eAd1	tak
pozitivně	pozitivně	k6eAd1	pozitivně
přijímána	přijímat	k5eAaImNgFnS	přijímat
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Ochránci	ochránce	k1gMnPc1	ochránce
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
myslivci	myslivec	k1gMnPc1	myslivec
i	i	k8xC	i
zoologové	zoolog	k1gMnPc1	zoolog
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nešetrný	šetrný	k2eNgInSc4d1	nešetrný
a	a	k8xC	a
nehumánní	humánní	k2eNgInSc4d1	nehumánní
způsob	způsob	k1gInSc4	způsob
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
zvěř	zvěř	k1gFnSc1	zvěř
málokdy	málokdy	k6eAd1	málokdy
usmrcena	usmrcen	k2eAgFnSc1d1	usmrcena
prvním	první	k4xOgInSc7	první
šípem	šíp	k1gInSc7	šíp
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
trýzní	trýznit	k5eAaImIp3nP	trýznit
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
šípy	šíp	k1gInPc4	šíp
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
a	a	k8xC	a
kuše	kuše	k1gFnPc1	kuše
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
tichost	tichost	k1gFnSc4	tichost
také	také	k9	také
často	často	k6eAd1	často
zneužívány	zneužíván	k2eAgMnPc4d1	zneužíván
pytláky	pytlák	k1gMnPc4	pytlák
k	k	k7c3	k
ilegálnímu	ilegální	k2eAgInSc3d1	ilegální
odstřelu	odstřel	k1gInSc3	odstřel
zvěře	zvěř	k1gFnSc2	zvěř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
České	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
území	území	k1gNnSc1	území
obývaly	obývat	k5eAaImAgInP	obývat
v	v	k7c6	v
pradávnu	pradávno	k1gNnSc6	pradávno
různé	různý	k2eAgInPc1d1	různý
kmeny	kmen	k1gInPc1	kmen
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
kulturní	kulturní	k2eAgFnSc7d1	kulturní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgInPc4	tento
kulturní	kulturní	k2eAgInPc4d1	kulturní
kmeny	kmen	k1gInPc4	kmen
patřili	patřit	k5eAaImAgMnP	patřit
především	především	k9	především
Keltové	Kelt	k1gMnPc1	Kelt
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
od	od	k7c2	od
západu	západ	k1gInSc2	západ
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Obývali	obývat	k5eAaImAgMnP	obývat
naše	náš	k3xOp1gInPc4	náš
kraje	kraj	k1gInPc4	kraj
až	až	k9	až
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Drávu	Dráv	k1gInSc2	Dráv
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
používání	používání	k1gNnSc3	používání
luku	luk	k1gInSc2	luk
těmito	tento	k3xDgFnPc7	tento
lidmi	člověk	k1gMnPc7	člověk
řadou	řada	k1gFnSc7	řada
nálezů	nález	k1gInPc2	nález
hrotů	hrot	k1gInPc2	hrot
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
pískovcových	pískovcový	k2eAgInPc2d1	pískovcový
rovnačů	rovnač	k1gInPc2	rovnač
tětiv	tětiva	k1gFnPc2	tětiva
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběny	vyráběn	k2eAgInPc1d1	vyráběn
byly	být	k5eAaImAgInP	být
luky	luk	k1gInPc1	luk
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
<g/>
,	,	kIx,	,
z	z	k7c2	z
javorového	javorový	k2eAgNnSc2d1	javorové
nebo	nebo	k8xC	nebo
tisového	tisový	k2eAgNnSc2d1	Tisové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
rovnačů	rovnač	k1gInPc2	rovnač
tětiv	tětiva	k1gFnPc2	tětiva
byly	být	k5eAaImAgInP	být
také	také	k9	také
nalezeny	nalezen	k2eAgInPc1d1	nalezen
chrániče	chránič	k1gInPc1	chránič
na	na	k7c4	na
ruku	ruka	k1gFnSc4	ruka
z	z	k7c2	z
břidlice	břidlice	k1gFnSc2	břidlice
nebo	nebo	k8xC	nebo
buližníku	buližník	k1gInSc2	buližník
<g/>
.	.	kIx.	.
</s>
<s>
Keltové	Kelt	k1gMnPc1	Kelt
na	na	k7c6	na
našem	náš	k3xOp1gNnSc6	náš
území	území	k1gNnSc6	území
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
největšího	veliký	k2eAgInSc2d3	veliký
rozkvětu	rozkvět	k1gInSc2	rozkvět
i	i	k8xC	i
pádu	pád	k1gInSc2	pád
Asyřanů	Asyřan	k1gMnPc2	Asyřan
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
značná	značný	k2eAgFnSc1d1	značná
časová	časový	k2eAgFnSc1d1	časová
mezera	mezera	k1gFnSc1	mezera
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
spadá	spadat	k5eAaImIp3nS	spadat
slovanské	slovanský	k2eAgNnSc4d1	slovanské
osídlení	osídlení	k1gNnSc4	osídlení
našeho	náš	k3xOp1gNnSc2	náš
území	území	k1gNnSc2	území
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Luky	luk	k1gInPc1	luk
a	a	k8xC	a
šípy	šíp	k1gInPc1	šíp
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
jistě	jistě	k6eAd1	jistě
používaly	používat	k5eAaImAgFnP	používat
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
luk	luk	k1gInSc4	luk
a	a	k8xC	a
šíp	šíp	k1gInSc4	šíp
nutnou	nutný	k2eAgFnSc7d1	nutná
výstrojí	výstroj	k1gFnSc7	výstroj
vojenských	vojenský	k2eAgInPc2d1	vojenský
sborů	sbor	k1gInPc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
početné	početný	k2eAgInPc1d1	početný
lučištnické	lučištnický	k2eAgInPc1d1	lučištnický
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
zdatnost	zdatnost	k1gFnSc1	zdatnost
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
byli	být	k5eAaImAgMnP	být
vybíráni	vybírat	k5eAaImNgMnP	vybírat
do	do	k7c2	do
královských	královský	k2eAgInPc2d1	královský
vojenských	vojenský	k2eAgInPc2d1	vojenský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
panování	panování	k1gNnSc4	panování
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
dostali	dostat	k5eAaPmAgMnP	dostat
pražští	pražský	k2eAgMnPc1d1	pražský
lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
svoji	svůj	k3xOyFgFnSc4	svůj
střelnici	střelnice	k1gFnSc4	střelnice
pod	pod	k7c7	pod
hradbami	hradba	k1gFnPc7	hradba
u	u	k7c2	u
nynější	nynější	k2eAgFnSc2d1	nynější
Prašné	prašný	k2eAgFnSc2d1	prašná
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
holdovali	holdovat	k5eAaImAgMnP	holdovat
střelbě	střelba	k1gFnSc6	střelba
z	z	k7c2	z
luku	luk	k1gInSc2	luk
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
(	(	kIx(	(
<g/>
padl	padnout	k5eAaPmAgMnS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Angličané	Angličan	k1gMnPc1	Angličan
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
díky	díky	k7c3	díky
lepší	dobrý	k2eAgFnSc3d2	lepší
střelbě	střelba	k1gFnSc3	střelba
svých	svůj	k3xOyFgMnPc2	svůj
lukostřelců	lukostřelec	k1gMnPc2	lukostřelec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladislavové	Vladislav	k1gMnPc1	Vladislav
i	i	k9	i
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
cvičením	cvičení	k1gNnPc3	cvičení
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lukostřelci	lukostřelec	k1gMnPc1	lukostřelec
dostávali	dostávat	k5eAaImAgMnP	dostávat
značnou	značný	k2eAgFnSc4d1	značná
přímou	přímý	k2eAgFnSc4d1	přímá
podporu	podpora	k1gFnSc4	podpora
od	od	k7c2	od
samotného	samotný	k2eAgMnSc2d1	samotný
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
i	i	k9	i
od	od	k7c2	od
pražských	pražský	k2eAgMnPc2d1	pražský
městských	městský	k2eAgMnPc2d1	městský
konšelů	konšel	k1gMnPc2	konšel
a	a	k8xC	a
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
českých	český	k2eAgNnPc2d1	české
a	a	k8xC	a
moravských	moravský	k2eAgNnPc2d1	Moravské
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
pořádány	pořádán	k2eAgFnPc1d1	pořádána
střelecké	střelecký	k2eAgFnPc1d1	střelecká
soutěže	soutěž	k1gFnPc1	soutěž
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
byly	být	k5eAaImAgFnP	být
lukostřelecké	lukostřelecký	k2eAgFnPc1d1	lukostřelecká
slavnosti	slavnost	k1gFnPc1	slavnost
o	o	k7c6	o
letnicích	letnice	k1gFnPc6	letnice
<g/>
.	.	kIx.	.
</s>
<s>
Lukostřelbě	lukostřelba	k1gFnSc3	lukostřelba
se	se	k3xPyFc4	se
věnovaly	věnovat	k5eAaPmAgFnP	věnovat
všechny	všechen	k3xTgFnPc1	všechen
vrstvy	vrstva	k1gFnPc1	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgNnPc1d1	české
a	a	k8xC	a
moravská	moravský	k2eAgNnPc1d1	Moravské
města	město	k1gNnPc1	město
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
soutěžila	soutěžit	k5eAaImAgFnS	soutěžit
v	v	k7c6	v
lukostřelbě	lukostřelba	k1gFnSc6	lukostřelba
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
lukostřeleckým	lukostřelecký	k2eAgInSc7d1	lukostřelecký
sborem	sbor	k1gInSc7	sbor
byli	být	k5eAaImAgMnP	být
pražští	pražský	k2eAgMnPc1d1	pražský
lučištníci	lučištník	k1gMnPc1	lučištník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
a	a	k8xC	a
popularita	popularita	k1gFnSc1	popularita
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
šly	jít	k5eAaImAgInP	jít
tak	tak	k6eAd1	tak
daleko	daleko	k6eAd1	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
střílelo	střílet	k5eAaImAgNnS	střílet
po	po	k7c6	po
všech	všecek	k3xTgFnPc6	všecek
pražských	pražský	k2eAgFnPc6d1	Pražská
zahradách	zahrada	k1gFnPc6	zahrada
a	a	k8xC	a
lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
zábavou	zábava	k1gFnSc7	zábava
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgFnP	být
lukostřelcům	lukostřelec	k1gMnPc3	lukostřelec
trvale	trvale	k6eAd1	trvale
přiděleny	přidělen	k2eAgInPc4d1	přidělen
říční	říční	k2eAgInPc4d1	říční
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
častých	častý	k2eAgNnPc2d1	časté
zranění	zranění	k1gNnPc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
nynější	nynější	k2eAgFnSc1d1	nynější
Štvanice	Štvanice	k1gFnSc1	Štvanice
a	a	k8xC	a
Střelecký	střelecký	k2eAgInSc1d1	střelecký
ostrov	ostrov	k1gInSc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Tradici	tradice	k1gFnSc4	tradice
lukostřelby	lukostřelba	k1gFnSc2	lukostřelba
na	na	k7c6	na
Střeleckém	střelecký	k2eAgInSc6d1	střelecký
ostrově	ostrov	k1gInSc6	ostrov
udržuje	udržovat	k5eAaImIp3nS	udržovat
alespoň	alespoň	k9	alespoň
během	během	k7c2	během
zimních	zimní	k2eAgInPc2d1	zimní
tréninků	trénink	k1gInPc2	trénink
lukostřelecký	lukostřelecký	k2eAgInSc1d1	lukostřelecký
oddíl	oddíl	k1gInSc1	oddíl
SK	Sk	kA	Sk
Start	start	k1gInSc1	start
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
husité	husita	k1gMnPc1	husita
měli	mít	k5eAaImAgMnP	mít
početné	početný	k2eAgInPc4d1	početný
lučištnické	lučištnický	k2eAgInPc4d1	lučištnický
oddíly	oddíl	k1gInPc4	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
husitských	husitský	k2eAgFnPc6d1	husitská
dobách	doba	k1gFnPc6	doba
ale	ale	k8xC	ale
již	již	k6eAd1	již
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
současnému	současný	k2eAgNnSc3d1	současné
používání	používání	k1gNnSc3	používání
luků	luk	k1gInPc2	luk
<g/>
,	,	kIx,	,
kuší	kuše	k1gFnPc2	kuše
a	a	k8xC	a
počínajících	počínající	k2eAgFnPc2d1	počínající
lehčích	lehký	k2eAgFnPc2d2	lehčí
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
hákovnice	hákovnice	k1gFnPc1	hákovnice
a	a	k8xC	a
píšťaly	píšťala	k1gFnPc1	píšťala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pobělohorské	pobělohorský	k2eAgNnSc1d1	pobělohorské
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
lučištnické	lučištnický	k2eAgInPc4d1	lučištnický
sbory	sbor	k1gInPc4	sbor
<g/>
,	,	kIx,	,
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
i	i	k9	i
početní	početní	k2eAgMnPc1d1	početní
výrobci	výrobce	k1gMnPc1	výrobce
luků	luk	k1gInPc2	luk
<g/>
,	,	kIx,	,
šípů	šíp	k1gInPc2	šíp
a	a	k8xC	a
toulců	toulec	k1gInPc2	toulec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
zaměřovat	zaměřovat	k5eAaImF	zaměřovat
především	především	k9	především
na	na	k7c4	na
palné	palný	k2eAgFnPc4d1	palná
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
lukostřelcích	lukostřelec	k1gMnPc6	lukostřelec
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Jiráskově	Jiráskův	k2eAgFnSc6d1	Jiráskova
F.	F.	kA	F.
L.	L.	kA	L.
Věkovi	Věkův	k2eAgMnPc1d1	Věkův
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
přehlídce	přehlídka	k1gFnSc6	přehlídka
vojska	vojsko	k1gNnSc2	vojsko
generála	generál	k1gMnSc2	generál
Suvorova	Suvorův	k2eAgInSc2d1	Suvorův
na	na	k7c6	na
Karlínském	karlínský	k2eAgNnSc6d1	Karlínské
poli	pole	k1gNnSc6	pole
před	před	k7c7	před
tažením	tažení	k1gNnSc7	tažení
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byly	být	k5eAaImAgInP	být
obdivovány	obdivován	k2eAgInPc4d1	obdivován
útvary	útvar	k1gInPc4	útvar
Tatarů	Tatar	k1gMnPc2	Tatar
s	s	k7c7	s
luky	luk	k1gInPc7	luk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
pak	pak	k6eAd1	pak
luk	luk	k1gInSc4	luk
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
úplně	úplně	k6eAd1	úplně
kuši	kuše	k1gFnSc3	kuše
a	a	k8xC	a
palným	palný	k2eAgFnPc3d1	palná
zbraním	zbraň	k1gFnPc3	zbraň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Lukostřelba	lukostřelba	k1gFnSc1	lukostřelba
</s>
</p>
<p>
<s>
Kuše	kuše	k1gFnSc1	kuše
</s>
</p>
<p>
<s>
Šíp	Šíp	k1gMnSc1	Šíp
</s>
</p>
<p>
<s>
Lukostřelecký	lukostřelecký	k2eAgInSc1d1	lukostřelecký
paradox	paradox	k1gInSc1	paradox
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
luk	louka	k1gFnPc2	louka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
luk	louka	k1gFnPc2	louka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
