<p>
<s>
Fukóza	Fukóza	k1gFnSc1	Fukóza
(	(	kIx(	(
<g/>
v	v	k7c6	v
chemickém	chemický	k2eAgNnSc6d1	chemické
názvosloví	názvosloví	k1gNnSc6	názvosloví
fukosa	fukosa	k1gFnSc1	fukosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
monosacharid	monosacharid	k1gInSc1	monosacharid
se	s	k7c7	s
šesti	šest	k4xCc7	šest
uhlíky	uhlík	k1gInPc7	uhlík
(	(	kIx(	(
<g/>
hexóza	hexóza	k1gFnSc1	hexóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
aldehydovou	aldehydův	k2eAgFnSc7d1	aldehydův
skupinou	skupina	k1gFnSc7	skupina
(	(	kIx(	(
<g/>
aldóza	aldóza	k1gFnSc1	aldóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
zkratka	zkratka	k1gFnSc1	zkratka
je	být	k5eAaImIp3nS	být
Fuc	Fuc	k1gFnSc4	Fuc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
naprosté	naprostý	k2eAgFnSc2d1	naprostá
většiny	většina	k1gFnSc2	většina
sacharidů	sacharid	k1gInPc2	sacharid
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
D	D	kA	D
konfiguraci	konfigurace	k1gFnSc6	konfigurace
(	(	kIx(	(
<g/>
D	D	kA	D
sacharidy	sacharid	k1gInPc4	sacharid
neboli	neboli	k8xC	neboli
D	D	kA	D
cukry	cukr	k1gInPc1	cukr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
fukóza	fukóza	k1gFnSc1	fukóza
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
přirozeně	přirozeně	k6eAd1	přirozeně
v	v	k7c6	v
L	L	kA	L
konfiguraci	konfigurace	k1gFnSc6	konfigurace
<g/>
.	.	kIx.	.
</s>
<s>
Fukóza	Fukóza	k1gFnSc1	Fukóza
není	být	k5eNaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
hexózou	hexóza	k1gFnSc7	hexóza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
odvozený	odvozený	k2eAgInSc4d1	odvozený
cukr	cukr	k1gInSc4	cukr
–	–	k?	–
derivát	derivát	k1gInSc1	derivát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
tzv.	tzv.	kA	tzv.
deoxysacharidů	deoxysacharid	k1gInPc2	deoxysacharid
(	(	kIx(	(
<g/>
sacharidů	sacharid	k1gInPc2	sacharid
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
chybí	chybit	k5eAaPmIp3nS	chybit
jedna	jeden	k4xCgFnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
hydroxylových	hydroxylový	k2eAgFnPc2d1	hydroxylová
skupin	skupina	k1gFnPc2	skupina
–	–	k?	–
příkladem	příklad	k1gInSc7	příklad
dalších	další	k2eAgInPc2d1	další
významných	významný	k2eAgInPc2d1	významný
deoxycukrů	deoxycukr	k1gInPc2	deoxycukr
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
L-rhamnóza	Lhamnóza	k1gFnSc1	L-rhamnóza
nebo	nebo	k8xC	nebo
D-deoxyribóza	Deoxyribóza	k1gFnSc1	D-deoxyribóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fukózu	Fukóza	k1gFnSc4	Fukóza
lze	lze	k6eAd1	lze
také	také	k9	také
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
6	[number]	k4	6
<g/>
-deoxy-L-galaktózu	eoxy-Lalaktóza	k1gFnSc4	-deoxy-L-galaktóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
==	==	k?	==
</s>
</p>
<p>
<s>
Fukóza	Fukóza	k1gFnSc1	Fukóza
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
volná	volný	k2eAgFnSc1d1	volná
prakticky	prakticky	k6eAd1	prakticky
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
polysacharidů	polysacharid	k1gInPc2	polysacharid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fukoidan	fukoidany	k1gInPc2	fukoidany
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
navázané	navázaný	k2eAgInPc1d1	navázaný
na	na	k7c6	na
proteinech	protein	k1gInPc6	protein
(	(	kIx(	(
<g/>
glykoproteiny	glykoprotein	k2eAgInPc1d1	glykoprotein
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
N-glykosidovou	Nlykosidový	k2eAgFnSc7d1	N-glykosidový
vazbou	vazba	k1gFnSc7	vazba
tak	tak	k6eAd1	tak
i	i	k9	i
O-glykosidovou	Olykosidový	k2eAgFnSc7d1	O-glykosidový
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
oligosacharidech	oligosacharid	k1gInPc6	oligosacharid
je	být	k5eAaImIp3nS	být
fukóza	fukóza	k1gFnSc1	fukóza
vždy	vždy	k6eAd1	vždy
navázaná	navázaný	k2eAgFnSc1d1	navázaná
O-glykosidovou	Olykosidový	k2eAgFnSc7d1	O-glykosidový
vazbou	vazba	k1gFnSc7	vazba
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
sacharid	sacharid	k1gInSc4	sacharid
–	–	k?	–
nejčastěji	často	k6eAd3	často
galaktózu	galaktóza	k1gFnSc4	galaktóza
(	(	kIx(	(
<g/>
Gal	Gal	k1gMnSc1	Gal
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
N-acetylglukosamin	Ncetylglukosamin	k2eAgInSc1d1	N-acetylglukosamin
(	(	kIx(	(
<g/>
GlcNAc	GlcNAc	k1gInSc1	GlcNAc
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vzácněji	vzácně	k6eAd2	vzácně
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
protein	protein	k1gInSc4	protein
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přes	přes	k7c4	přes
hydroxylové	hydroxylový	k2eAgFnPc4d1	hydroxylová
skupiny	skupina	k1gFnPc4	skupina
postranních	postranní	k2eAgInPc2d1	postranní
řetězců	řetězec	k1gInPc2	řetězec
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
serinu	serinout	k5eAaPmIp1nS	serinout
nebo	nebo	k8xC	nebo
threoninu	threonina	k1gFnSc4	threonina
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Syntéza	syntéza	k1gFnSc1	syntéza
a	a	k8xC	a
zapojení	zapojení	k1gNnSc1	zapojení
do	do	k7c2	do
metabolismu	metabolismus	k1gInSc2	metabolismus
==	==	k?	==
</s>
</p>
<p>
<s>
Fukóza	Fukóza	k1gFnSc1	Fukóza
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgInPc1d1	ostatní
monosacharidy	monosacharid	k1gInPc1	monosacharid
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
v	v	k7c6	v
metabolismu	metabolismus	k1gInSc6	metabolismus
samotná	samotný	k2eAgFnSc1d1	samotná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navázaná	navázaný	k2eAgFnSc1d1	navázaná
na	na	k7c4	na
přenašeč	přenašeč	k1gInSc4	přenašeč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
fukózy	fukóza	k1gFnSc2	fukóza
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
GDP	GDP	kA	GDP
(	(	kIx(	(
<g/>
guanosindifosfát	guanosindifosfát	k1gMnSc1	guanosindifosfát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
GDP-fukóza	GDPukóza	k1gFnSc1	GDP-fukóza
je	být	k5eAaImIp3nS	být
syntetizovaná	syntetizovaný	k2eAgFnSc1d1	syntetizovaná
jednak	jednak	k8xC	jednak
z	z	k7c2	z
GTP	GTP	kA	GTP
(	(	kIx(	(
<g/>
guanosintrifosfát	guanosintrifosfát	k1gMnSc1	guanosintrifosfát
<g/>
)	)	kIx)	)
a	a	k8xC	a
volné	volný	k2eAgFnPc4d1	volná
fukózy	fukóza	k1gFnPc4	fukóza
(	(	kIx(	(
<g/>
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
zdroj	zdroj	k1gInSc4	zdroj
je	být	k5eAaImIp3nS	být
přijatá	přijatý	k2eAgFnSc1d1	přijatá
potrava	potrava	k1gFnSc1	potrava
nebo	nebo	k8xC	nebo
štěpení	štěpení	k1gNnSc1	štěpení
vlastních	vlastní	k2eAgInPc2d1	vlastní
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
syntéza	syntéza	k1gFnSc1	syntéza
vycházet	vycházet	k5eAaImF	vycházet
z	z	k7c2	z
GDP-manózy	GDPanóza	k1gFnSc2	GDP-manóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
zdroje	zdroj	k1gInPc1	zdroj
fukózy	fukóza	k1gFnSc2	fukóza
==	==	k?	==
</s>
</p>
<p>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
zdrojem	zdroj	k1gInSc7	zdroj
L-fukózy	Lukóza	k1gFnSc2	L-fukóza
jsou	být	k5eAaImIp3nP	být
fukoidany	fukoidan	k1gInPc1	fukoidan
–	–	k?	–
polysacharidy	polysacharid	k1gInPc1	polysacharid
tvořené	tvořený	k2eAgInPc1d1	tvořený
zčásti	zčásti	k6eAd1	zčásti
fukózou	fukóza	k1gFnSc7	fukóza
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
míře	míra	k1gFnSc6	míra
sulfatovanou	sulfatovaný	k2eAgFnSc7d1	sulfatovaný
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
s	s	k7c7	s
navázanými	navázaný	k2eAgInPc7d1	navázaný
zbytky	zbytek	k1gInPc7	zbytek
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgInPc1d1	sírový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
produkované	produkovaný	k2eAgInPc1d1	produkovaný
některými	některý	k3yIgFnPc7	některý
hnědými	hnědý	k2eAgFnPc7d1	hnědá
řasami	řasa	k1gFnPc7	řasa
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
jejich	jejich	k3xOp3gFnPc2	jejich
buněčných	buněčný	k2eAgFnPc2d1	buněčná
stěn	stěna	k1gFnPc2	stěna
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
celulózou	celulóza	k1gFnSc7	celulóza
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Fucus	Fucus	k1gMnSc1	Fucus
vesiculosus	vesiculosus	k1gMnSc1	vesiculosus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
mohou	moct	k5eAaImIp3nP	moct
tvořit	tvořit	k5eAaImF	tvořit
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
sušených	sušený	k2eAgFnPc2d1	sušená
buněčných	buněčný	k2eAgFnPc2d1	buněčná
stěn	stěna	k1gFnPc2	stěna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
funkcí	funkce	k1gFnPc2	funkce
fukosy	fukosa	k1gFnSc2	fukosa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Zánět	zánět	k1gInSc4	zánět
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
zánětu	zánět	k1gInSc2	zánět
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
složení	složení	k1gNnSc2	složení
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
buňky	buňka	k1gFnPc1	buňka
tvořící	tvořící	k2eAgFnPc1d1	tvořící
stěny	stěna	k1gFnPc1	stěna
krevních	krevní	k2eAgFnPc2d1	krevní
kapilár	kapilára	k1gFnPc2	kapilára
tzv.	tzv.	kA	tzv.
endotheliální	endotheliální	k2eAgFnPc4d1	endotheliální
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
krvinky	krvinka	k1gFnPc1	krvinka
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
speciální	speciální	k2eAgInPc4d1	speciální
oligosacharidy	oligosacharid	k1gInPc4	oligosacharid
receptory	receptor	k1gInPc4	receptor
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
selektiny	selektina	k1gFnSc2	selektina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
je	on	k3xPp3gMnPc4	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
a	a	k8xC	a
navázat	navázat	k5eAaPmF	navázat
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tímto	tento	k3xDgInSc7	tento
mechanismem	mechanismus	k1gInSc7	mechanismus
se	se	k3xPyFc4	se
bíle	bíle	k6eAd1	bíle
krvinky	krvinka	k1gFnPc1	krvinka
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
na	na	k7c4	na
stěny	stěna	k1gFnPc4	stěna
krevních	krevní	k2eAgFnPc2d1	krevní
kapilár	kapilára	k1gFnPc2	kapilára
a	a	k8xC	a
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
se	se	k3xPyFc4	se
místě	místo	k1gNnSc6	místo
zánětu	zánět	k1gInSc3	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Fukóza	Fukóza	k1gFnSc1	Fukóza
je	být	k5eAaImIp3nS	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
těchto	tento	k3xDgInPc2	tento
oligosacharidů	oligosacharid	k1gInPc2	oligosacharid
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
endotheliální	endotheliální	k2eAgFnPc1d1	endotheliální
buňky	buňka	k1gFnPc1	buňka
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zánětu	zánět	k1gInSc2	zánět
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
a	a	k8xC	a
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krevní	krevní	k2eAgFnSc2d1	krevní
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Biochemickou	biochemický	k2eAgFnSc7d1	biochemická
podstatou	podstata	k1gFnSc7	podstata
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
jsou	být	k5eAaImIp3nP	být
oligosacharidy	oligosacharid	k1gInPc1	oligosacharid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
(	(	kIx(	(
<g/>
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
buněk	buňka	k1gFnPc2	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
sacharidů	sacharid	k1gInPc2	sacharid
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
i	i	k9	i
0	[number]	k4	0
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
H	H	kA	H
<g/>
))	))	k?	))
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
navázanou	navázaný	k2eAgFnSc4d1	navázaná
fukózu	fukóza	k1gFnSc4	fukóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
Brooks	Brooksa	k1gFnPc2	Brooksa
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
V.	V.	kA	V.
Dwek	Dwek	k1gMnSc1	Dwek
<g/>
,	,	kIx,	,
U.	U.	kA	U.
Schumacher	Schumachra	k1gFnPc2	Schumachra
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Functional	Functional	k1gMnSc1	Functional
and	and	k?	and
Molecular	Molecular	k1gMnSc1	Molecular
Glykobiology	Glykobiolog	k1gMnPc7	Glykobiolog
<g/>
"	"	kIx"	"
1	[number]	k4	1
<g/>
st	st	kA	st
edition	edition	k1gInSc1	edition
<g/>
,	,	kIx,	,
BIOS	BIOS	kA	BIOS
Scientific	Scientifice	k1gFnPc2	Scientifice
Publishers	Publishersa	k1gFnPc2	Publishersa
Limited	limited	k2eAgFnSc1d1	limited
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1-85996-022-7	[number]	k4	1-85996-022-7
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
galaktóza	galaktóza	k1gFnSc1	galaktóza
</s>
</p>
<p>
<s>
manóza	manóza	k1gFnSc1	manóza
</s>
</p>
<p>
<s>
monosacharidy	monosacharid	k1gInPc1	monosacharid
</s>
</p>
<p>
<s>
glykoproteiny	glykoproteina	k1gFnPc1	glykoproteina
</s>
</p>
<p>
<s>
lektiny	lektina	k1gFnPc1	lektina
</s>
</p>
