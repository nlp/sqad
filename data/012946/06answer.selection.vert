<s>
Fukóza	Fukóza	k1gFnSc1	Fukóza
(	(	kIx(	(
<g/>
v	v	k7c6	v
chemickém	chemický	k2eAgNnSc6d1	chemické
názvosloví	názvosloví	k1gNnSc6	názvosloví
fukosa	fukosa	k1gFnSc1	fukosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
monosacharid	monosacharid	k1gInSc1	monosacharid
se	s	k7c7	s
šesti	šest	k4xCc7	šest
uhlíky	uhlík	k1gInPc7	uhlík
(	(	kIx(	(
<g/>
hexóza	hexóza	k1gFnSc1	hexóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
aldehydovou	aldehydův	k2eAgFnSc7d1	aldehydův
skupinou	skupina	k1gFnSc7	skupina
(	(	kIx(	(
<g/>
aldóza	aldóza	k1gFnSc1	aldóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
