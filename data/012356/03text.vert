<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgMnSc1d1	severní
(	(	kIx(	(
<g/>
Bucephala	Bucephal	k1gMnSc2	Bucephal
clangula	clangul	k1gMnSc2	clangul
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velká	velký	k2eAgFnSc1d1	velká
potápivá	potápivý	k2eAgFnSc1d1	potápivá
kachna	kachna	k1gFnSc1	kachna
<g/>
,	,	kIx,	,
hnízdící	hnízdící	k2eAgFnSc1d1	hnízdící
ve	v	k7c6	v
stromových	stromový	k2eAgFnPc6d1	stromová
dutinách	dutina	k1gFnPc6	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k8xS	jako
kachnička	kachnička	k1gFnSc1	kachnička
mandarínská	mandarínský	k2eAgFnSc1d1	mandarínská
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
48	[number]	k4	48
cm	cm	kA	cm
<g/>
,	,	kIx,	,
váží	vážit	k5eAaImIp3nS	vážit
600	[number]	k4	600
<g/>
–	–	k?	–
<g/>
1300	[number]	k4	1300
g	g	kA	g
a	a	k8xC	a
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měří	měřit	k5eAaImIp3nP	měřit
kolem	kolem	k7c2	kolem
77	[number]	k4	77
<g/>
–	–	k?	–
<g/>
83	[number]	k4	83
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
hoholové	hohol	k1gMnPc1	hohol
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
svérázný	svérázný	k2eAgInSc4d1	svérázný
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
rozpoznatelní	rozpoznatelný	k2eAgMnPc1d1	rozpoznatelný
podle	podle	k7c2	podle
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
tvaru	tvar	k1gInSc2	tvar
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
tmavou	tmavý	k2eAgFnSc4d1	tmavá
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
jasně	jasně	k6eAd1	jasně
zelenavým	zelenavý	k2eAgInSc7d1	zelenavý
odstínem	odstín	k1gInSc7	odstín
<g/>
,	,	kIx,	,
výraznou	výrazný	k2eAgFnSc4d1	výrazná
bílou	bílý	k2eAgFnSc4d1	bílá
skvrnu	skvrna	k1gFnSc4	skvrna
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
<g/>
,	,	kIx,	,
tmavý	tmavý	k2eAgInSc1d1	tmavý
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
černý	černý	k2eAgInSc1d1	černý
hřbet	hřbet	k1gInSc4	hřbet
a	a	k8xC	a
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
příčné	příčný	k2eAgNnSc4d1	příčné
černobílé	černobílý	k2eAgNnSc4d1	černobílé
pruhování	pruhování	k1gNnSc4	pruhování
na	na	k7c6	na
bocích	bok	k1gInPc6	bok
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgNnSc4d1	světlé
břicho	břicho	k1gNnSc4	břicho
<g/>
,	,	kIx,	,
hruď	hruď	k1gFnSc4	hruď
a	a	k8xC	a
hrdlo	hrdlo	k1gNnSc4	hrdlo
a	a	k8xC	a
oranžové	oranžový	k2eAgFnPc4d1	oranžová
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
celé	celý	k2eAgFnPc1d1	celá
hnědé	hnědý	k2eAgFnPc1d1	hnědá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
černožlutě	černožlutě	k6eAd1	černožlutě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgMnSc1d1	severní
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
u	u	k7c2	u
větších	veliký	k2eAgFnPc2d2	veliký
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
obklopenými	obklopený	k2eAgInPc7d1	obklopený
stromovým	stromový	k2eAgInSc7d1	stromový
porostem	porost	k1gInSc7	porost
zejména	zejména	k9	zejména
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
a	a	k8xC	a
při	při	k7c6	při
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
každým	každý	k3xTgInSc7	každý
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
stahuje	stahovat	k5eAaImIp3nS	stahovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
mořskému	mořský	k2eAgNnSc3d1	mořské
pobřeží	pobřeží	k1gNnSc3	pobřeží
a	a	k8xC	a
na	na	k7c4	na
větší	veliký	k2eAgFnPc4d2	veliký
nezamrzající	zamrzající	k2eNgFnPc4d1	nezamrzající
vnitrozemské	vnitrozemský	k2eAgFnPc4d1	vnitrozemská
vodní	vodní	k2eAgFnPc4d1	vodní
plochy	plocha	k1gFnPc4	plocha
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
mírnějším	mírný	k2eAgNnSc7d2	mírnější
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
vnitrozemských	vnitrozemský	k2eAgInPc6d1	vnitrozemský
státech	stát	k1gInPc6	stát
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ročně	ročně	k6eAd1	ročně
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
párů	pár	k1gInPc2	pár
a	a	k8xC	a
přezimuje	přezimovat	k5eAaBmIp3nS	přezimovat
zhruba	zhruba	k6eAd1	zhruba
500	[number]	k4	500
<g/>
–	–	k?	–
<g/>
1000	[number]	k4	1000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgMnSc1d1	severní
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
hnízdní	hnízdní	k2eAgNnSc4d1	hnízdní
období	období	k1gNnSc4	období
zdržuje	zdržovat	k5eAaImIp3nS	zdržovat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
rozvolněných	rozvolněný	k2eAgNnPc6d1	rozvolněné
hejnech	hejno	k1gNnPc6	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
potápí	potápět	k5eAaImIp3nS	potápět
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
složku	složka	k1gFnSc4	složka
jeho	jeho	k3xOp3gFnSc2	jeho
potravy	potrava	k1gFnSc2	potrava
tvoří	tvořit	k5eAaImIp3nP	tvořit
korýši	korýš	k1gMnPc1	korýš
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc1	měkkýš
a	a	k8xC	a
drobné	drobný	k2eAgFnPc1d1	drobná
rybky	rybka	k1gFnPc1	rybka
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
požírá	požírat	k5eAaImIp3nS	požírat
i	i	k9	i
jikry	jikra	k1gFnPc1	jikra
a	a	k8xC	a
vodní	vodní	k2eAgFnPc1d1	vodní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
kořistí	kořist	k1gFnSc7	kořist
především	především	k9	především
orlů	orel	k1gMnPc2	orel
<g/>
,	,	kIx,	,
sov	sova	k1gFnPc2	sova
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
velkých	velký	k2eAgMnPc2d1	velký
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnPc1	vejce
a	a	k8xC	a
mláďata	mládě	k1gNnPc1	mládě
padají	padat	k5eAaImIp3nP	padat
často	často	k6eAd1	často
za	za	k7c4	za
kořist	kořist	k1gFnSc4	kořist
medvědům	medvěd	k1gMnPc3	medvěd
<g/>
,	,	kIx,	,
lasicím	lasice	k1gFnPc3	lasice
<g/>
,	,	kIx,	,
norkům	norek	k1gMnPc3	norek
nebo	nebo	k8xC	nebo
mývalům	mýval	k1gMnPc3	mýval
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
v	v	k7c6	v
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
ozývá	ozývat	k5eAaImIp3nS	ozývat
zejména	zejména	k9	zejména
pronikavým	pronikavý	k2eAgInSc7d1	pronikavý
"	"	kIx"	"
<g/>
knirrr	knirrr	k1gInSc1	knirrr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
hrčivým	hrčivý	k2eAgMnPc3d1	hrčivý
"	"	kIx"	"
<g/>
garrr-gra	garrrra	k6eAd1	garrr-gra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
je	být	k5eAaImIp3nS	být
též	též	k9	též
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
<g/>
,	,	kIx,	,
zvonivý	zvonivý	k2eAgInSc1d1	zvonivý
svist	svist	k1gInSc1	svist
letek	letka	k1gFnPc2	letka
<g/>
,	,	kIx,	,
nápadný	nápadný	k2eAgInSc4d1	nápadný
zejména	zejména	k9	zejména
u	u	k7c2	u
samců	samec	k1gMnPc2	samec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
svist	svist	k1gInSc1	svist
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
popisován	popisovat	k5eAaImNgInS	popisovat
v	v	k7c6	v
jakutském	jakutský	k2eAgInSc6d1	jakutský
eposu	epos	k1gInSc6	epos
Oloncho	Oloncha	k1gMnSc5	Oloncha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
opuštěné	opuštěný	k2eAgFnPc4d1	opuštěná
dutiny	dutina	k1gFnPc4	dutina
datlů	datel	k1gMnPc2	datel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahnízdí	zahnízdit	k5eAaPmIp3nS	zahnízdit
i	i	k9	i
v	v	k7c6	v
přiměřeně	přiměřeně	k6eAd1	přiměřeně
velké	velký	k2eAgFnSc3d1	velká
budce	budka	k1gFnSc3	budka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
průměrně	průměrně	k6eAd1	průměrně
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
nazelenalých	nazelenalý	k2eAgMnPc2d1	nazelenalý
<g/>
,	,	kIx,	,
48,3	[number]	k4	48,3
×	×	k?	×
59,3	[number]	k4	59,3
mm	mm	kA	mm
velkých	velká	k1gFnPc2	velká
a	a	k8xC	a
přibližně	přibližně	k6eAd1	přibližně
64	[number]	k4	64
g	g	kA	g
těžkých	těžký	k2eAgFnPc2d1	těžká
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
dnech	den	k1gInPc6	den
samec	samec	k1gMnSc1	samec
samici	samice	k1gFnSc3	samice
opouští	opouštět	k5eAaImIp3nS	opouštět
a	a	k8xC	a
na	na	k7c6	na
vejcích	vejce	k1gNnPc6	vejce
sedí	sedit	k5eAaImIp3nS	sedit
proto	proto	k6eAd1	proto
sama	sám	k3xTgFnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
průměrné	průměrný	k2eAgNnSc1d1	průměrné
inkubační	inkubační	k2eAgNnSc1d1	inkubační
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc1	hnízdo
opouští	opouštět	k5eAaImIp3nS	opouštět
již	již	k6eAd1	již
po	po	k7c6	po
24	[number]	k4	24
<g/>
–	–	k?	–
<g/>
36	[number]	k4	36
hodinách	hodina	k1gFnPc6	hodina
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
přitom	přitom	k6eAd1	přitom
musejí	muset	k5eAaImIp3nP	muset
seskočit	seskočit	k5eAaPmF	seskočit
ze	z	k7c2	z
značné	značný	k2eAgFnSc2d1	značná
výšky	výška	k1gFnSc2	výška
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vzletná	vzletný	k2eAgFnSc1d1	vzletná
kolem	kolem	k6eAd1	kolem
55	[number]	k4	55
<g/>
–	–	k?	–
<g/>
65	[number]	k4	65
dnu	dna	k1gFnSc4	dna
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Sibiři	Sibiř	k1gFnSc6	Sibiř
vybírána	vybírán	k2eAgNnPc1d1	vybíráno
z	z	k7c2	z
hnízd	hnízdo	k1gNnPc2	hnízdo
ke	k	k7c3	k
kuchyňským	kuchyňský	k2eAgInPc3d1	kuchyňský
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgMnSc1d1	severní
se	se	k3xPyFc4	se
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
areálu	areál	k1gInSc6	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
2	[number]	k4	2
poddruzích	poddruh	k1gInPc6	poddruh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
eurosibiřský	eurosibiřský	k2eAgMnSc1d1	eurosibiřský
(	(	kIx(	(
<g/>
B.	B.	kA	B.
c.	c.	k?	c.
clangula	clangula	k1gFnSc1	clangula
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
americký	americký	k2eAgMnSc1d1	americký
(	(	kIx(	(
<g/>
B.	B.	kA	B.
c.	c.	k?	c.
americana	americana	k1gFnSc1	americana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Common	Common	k1gInSc4	Common
Goldeneye	Goldeney	k1gFnSc2	Goldeney
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Schellente	Schellent	k1gInSc5	Schellent
na	na	k7c6	na
německé	německý	k2eAgFnSc3d1	německá
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dungel	Dungel	k1gInSc1	Dungel
J.	J.	kA	J.
<g/>
,	,	kIx,	,
Hudec	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
K.	K.	kA	K.
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
ptáků	pták	k1gMnPc2	pták
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
;	;	kIx,	;
str	str	kA	str
<g/>
.	.	kIx.	.
52	[number]	k4	52
<g/>
.	.	kIx.	.
</s>
<s>
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-0927-2	[number]	k4	978-80-200-0927-2
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgMnSc1d1	severní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
hohol	hohol	k1gMnSc1	hohol
severní	severní	k2eAgMnSc1d1	severní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Bucephala	Bucephal	k1gMnSc2	Bucephal
clangula	clangul	k1gMnSc2	clangul
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gMnSc1	BioLib
</s>
</p>
<p>
<s>
Cornell	Cornell	k1gMnSc1	Cornell
Lab	Lab	k1gMnSc1	Lab
of	of	k?	of
Ornithology	Ornitholog	k1gMnPc7	Ornitholog
</s>
</p>
<p>
<s>
Hohol	hohol	k1gMnSc1	hohol
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Ohrada	ohrada	k1gFnSc1	ohrada
</s>
</p>
