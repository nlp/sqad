<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgFnSc3d1
přírodní	přírodní	k2eAgFnSc3d1
rezervaceKaňon	rezervaceKaňon	k1gNnSc1
LabeIUCN	LabeIUCN	k1gMnPc1
kategorie	kategorie	k1gFnSc2
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Kaňon	kaňon	k1gInSc1
LabeZákladní	LabeZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
127	#num#	k4
<g/>
–	–	k?
<g/>
432	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
443,27	443,27	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Děčín	Děčín	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Loubí	loubí	k1gNnSc2
u	u	k7c2
Děčína	Děčín	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hřensko	Hřensko	k1gNnSc1
<g/>
,	,	kIx,
Labská	labský	k2eAgFnSc1d1
Stráň	stráň	k1gFnSc1
<g/>
,	,	kIx,
Ludvíkovice	Ludvíkovice	k1gFnSc1
<g/>
,	,	kIx,
Růžová	růžový	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
3,65	3,65	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
0,51	0,51	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
5608	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
je	být	k5eAaImIp3nS
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
<g/>
,	,	kIx,
vyhlášená	vyhlášený	k2eAgFnSc1d1
Ministerstvem	ministerstvo	k1gNnSc7
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
ČR	ČR	kA
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Ústeckém	ústecký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
mezi	mezi	k7c7
Děčínem	Děčín	k1gInSc7
a	a	k8xC
Hřenskem	Hřensko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Předmět	předmět	k1gInSc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Důvodem	důvod	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
přirozené	přirozený	k2eAgInPc1d1
lesní	lesní	k2eAgInPc1d1
porosty	porost	k1gInPc1
na	na	k7c6
pravém	pravý	k2eAgInSc6d1
svahu	svah	k1gInSc6
kaňonu	kaňon	k1gInSc6
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc1
tvořené	tvořený	k2eAgNnSc1d1
především	především	k9
acidofilními	acidofilní	k2eAgFnPc7d1
a	a	k8xC
květnatými	květnatý	k2eAgFnPc7d1
bučinami	bučina	k1gFnPc7
<g/>
,	,	kIx,
suťovými	suťový	k2eAgInPc7d1
lesy	les	k1gInPc7
a	a	k8xC
bory	bor	k1gInPc7
a	a	k8xC
tvořícími	tvořící	k2eAgInPc7d1
biotop	biotop	k1gInSc4
vzácných	vzácný	k2eAgInPc2d1
a	a	k8xC
ohrožených	ohrožený	k2eAgInPc2d1
druhů	druh	k1gInPc2
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
;	;	kIx,
geomorfologicky	geomorfologicky	k6eAd1
ojedinělý	ojedinělý	k2eAgInSc1d1
útvar	útvar	k1gInSc1
pravého	pravý	k2eAgInSc2d1
svahu	svah	k1gInSc2
kaňonu	kaňon	k1gInSc2
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
tvořeného	tvořený	k2eAgInSc2d1
formami	forma	k1gFnPc7
pseudokrasového	pseudokrasový	k2eAgInSc2d1
reliéfu	reliéf	k1gInSc2
v	v	k7c6
kvádrových	kvádrový	k2eAgInPc6d1
pískovcích	pískovec	k1gInPc6
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
vyskytujícími	vyskytující	k2eAgFnPc7d1
se	se	k3xPyFc4
zde	zde	k6eAd1
především	především	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
skalních	skalní	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
<g/>
,	,	kIx,
věží	věž	k1gFnPc2
a	a	k8xC
složitě	složitě	k6eAd1
členěných	členěný	k2eAgInPc2d1
hřbetů	hřbet	k1gInPc2
<g/>
,	,	kIx,
suťových	suťový	k2eAgFnPc2d1
polí	pole	k1gFnPc2
<g/>
,	,	kIx,
soutěsek	soutěska	k1gFnPc2
a	a	k8xC
jeskyní	jeskyně	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
specifickými	specifický	k2eAgNnPc7d1
rostlinnými	rostlinný	k2eAgNnPc7d1
a	a	k8xC
živočišnými	živočišný	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Péčí	péče	k1gFnPc2
o	o	k7c6
území	území	k1gNnSc6
je	být	k5eAaImIp3nS
pověřena	pověřen	k2eAgFnSc1d1
správa	správa	k1gFnSc1
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
(	(	kIx(
<g/>
od	od	k7c2
1972	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
území	území	k1gNnSc6
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
a	a	k8xC
Děčínské	děčínský	k2eAgFnSc2d1
vrchoviny	vrchovina	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
je	být	k5eAaImIp3nS
nejcennější	cenný	k2eAgFnSc7d3
částí	část	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejmohutnější	mohutný	k2eAgInSc4d3
pískovcový	pískovcový	k2eAgInSc4d1
kaňon	kaňon	k1gInSc4
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
vytvořený	vytvořený	k2eAgInSc1d1
erozní	erozní	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
řeky	řeka	k1gFnSc2
Labe	Labe	k1gNnSc2
v	v	k7c6
křídových	křídový	k2eAgInPc6d1
sedimentech	sediment	k1gInPc6
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
do	do	k7c2
hloubky	hloubka	k1gFnSc2
300	#num#	k4
m	m	kA
(	(	kIx(
<g/>
ve	v	k7c6
spodních	spodní	k2eAgFnPc6d1
částech	část	k1gFnPc6
je	být	k5eAaImIp3nS
odkryto	odkryt	k2eAgNnSc1d1
krystalinické	krystalinický	k2eAgNnSc1d1
podloží	podloží	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
hlavně	hlavně	k9
o	o	k7c6
území	území	k1gNnSc6
lesních	lesní	k2eAgInPc2d1
pozemků	pozemek	k1gInPc2
(	(	kIx(
<g/>
479	#num#	k4
hektarů	hektar	k1gInPc2
z	z	k7c2
celkové	celkový	k2eAgFnSc2d1
rozlohy	rozloha	k1gFnSc2
480	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
pomoří	pomoří	k1gNnSc3
Severního	severní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
povodí	povodí	k1gNnSc1
(	(	kIx(
<g/>
I.	I.	kA
řádu	řád	k1gInSc2
<g/>
)	)	kIx)
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
Chráněná	chráněný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
přirozené	přirozený	k2eAgFnSc2d1
akumulace	akumulace	k1gFnSc2
vod	voda	k1gFnPc2
Severočeská	severočeský	k2eAgFnSc1d1
křída	křída	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejkrásnější	krásný	k2eAgInSc1d3
pohled	pohled	k1gInSc1
na	na	k7c4
celé	celý	k2eAgNnSc4d1
území	území	k1gNnSc4
je	být	k5eAaImIp3nS
zejména	zejména	k9
z	z	k7c2
vyhlídek	vyhlídka	k1gFnPc2
Belvedér	belvedér	k1gInSc1
a	a	k8xC
Růžový	růžový	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
patřil	patřit	k5eAaImAgInS
kaňon	kaňon	k1gInSc1
k	k	k7c3
území	území	k1gNnSc3
„	„	k?
<g/>
Hvozdu	hvozd	k1gInSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
měl	mít	k5eAaImAgInS
primárně	primárně	k6eAd1
obranný	obranný	k2eAgInSc1d1
charakter	charakter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžba	těžba	k1gFnSc1
dřeva	dřevo	k1gNnSc2
zde	zde	k6eAd1
probíhala	probíhat	k5eAaImAgFnS
postupně	postupně	k6eAd1
od	od	k7c2
okrajových	okrajový	k2eAgInPc2d1
pozemků	pozemek	k1gInPc2
až	až	k9
po	po	k7c4
lesy	les	k1gInPc4
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitá	důležitý	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
vždy	vždy	k6eAd1
plavba	plavba	k1gFnSc1
dřeva	dřevo	k1gNnSc2
po	po	k7c6
Labi	Labe	k1gNnSc6
(	(	kIx(
<g/>
zmínky	zmínka	k1gFnPc4
už	už	k6eAd1
z	z	k7c2
roku	rok	k1gInSc2
1057	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
jeho	jeho	k3xOp3gInPc6
přítocích	přítok	k1gInPc6
(	(	kIx(
<g/>
Suchá	suchý	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
až	až	k9
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
častý	častý	k2eAgInSc1d1
byl	být	k5eAaImAgInS
i	i	k9
vývoz	vývoz	k1gInSc1
do	do	k7c2
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
projevila	projevit	k5eAaPmAgFnS
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
devastace	devastace	k1gFnSc1
některých	některý	k3yIgFnPc2
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
omezení	omezení	k1gNnSc3
těžby	těžba	k1gFnSc2
i	i	k8xC
vývozu	vývoz	k1gInSc2
císařským	císařský	k2eAgInSc7d1
patentem	patent	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
začalo	začít	k5eAaPmAgNnS
s	s	k7c7
umělým	umělý	k2eAgNnSc7d1
zalesňováním	zalesňování	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
ruku	ruka	k1gFnSc4
v	v	k7c6
ruce	ruka	k1gFnSc6
s	s	k7c7
budováním	budování	k1gNnSc7
železnice	železnice	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c4
které	který	k3yRgInPc4,k3yQgInPc4,k3yIgInPc4
byla	být	k5eAaImAgFnS
sice	sice	k8xC
spotřeba	spotřeba	k1gFnSc1
dřeva	dřevo	k1gNnSc2
značná	značný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
masivnímu	masivní	k2eAgInSc3d1
dovozu	dovoz	k1gInSc3
uhlí	uhlí	k1gNnSc2
následná	následný	k2eAgFnSc1d1
potřeba	potřeba	k1gFnSc1
těžby	těžba	k1gFnSc2
dřeva	dřevo	k1gNnSc2
výrazně	výrazně	k6eAd1
klesla	klesnout	k5eAaPmAgFnS
<g/>
)	)	kIx)
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
zlepšení	zlepšení	k1gNnSc3
poměrů	poměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
vyhlášení	vyhlášení	k1gNnSc2
NPR	NPR	kA
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
CHKO	CHKO	kA
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
<g/>
,	,	kIx,
při	při	k7c6
čemž	což	k3yRnSc6,k3yQnSc6
péče	péče	k1gFnPc4
spočívala	spočívat	k5eAaImAgFnS
hlavně	hlavně	k9
v	v	k7c6
mícení	mícení	k1gNnSc6
invazních	invazní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
sanaci	sanace	k1gFnSc4
erozních	erozní	k2eAgFnPc2d1
rýh	rýha	k1gFnPc2
a	a	k8xC
usměrňování	usměrňování	k1gNnSc2
turistiky	turistika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
vyhlášení	vyhlášení	k1gNnSc3
území	území	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1
poměry	poměr	k1gInPc1
</s>
<s>
Geomorfologie	geomorfologie	k1gFnSc1
a	a	k8xC
geologie	geologie	k1gFnSc1
</s>
<s>
Labe	Labe	k1gNnSc4
nad	nad	k7c7
Dolním	dolní	k2eAgInSc7d1
Žlebem	žleb	k1gInSc7
</s>
<s>
Labe	Labe	k1gNnSc4
z	z	k7c2
Labské	labský	k2eAgFnSc2d1
vyhlídky	vyhlídka	k1gFnSc2
</s>
<s>
Z	z	k7c2
geomorfologického	geomorfologický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
patří	patřit	k5eAaImIp3nS
území	území	k1gNnSc4
NPR	NPR	kA
k	k	k7c3
Děčínské	děčínský	k2eAgFnSc3d1
vrchovině	vrchovina	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
členitou	členitý	k2eAgFnSc4d1
vrchovinu	vrchovina	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
ze	z	k7c2
třech	tři	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
Sněžnická	Sněžnický	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
(	(	kIx(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
Vysoký	vysoký	k2eAgInSc1d1
Sněžník	Sněžník	k1gInSc1
<g/>
,	,	kIx,
723	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Růžovská	Růžovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
(	(	kIx(
<g/>
Růžovský	Růžovský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
619	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
a	a	k8xC
Jetřichovické	Jetřichovický	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
velké	velký	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
v	v	k7c6
nadmořských	nadmořský	k2eAgFnPc6d1
výškách	výška	k1gFnPc6
se	se	k3xPyFc4
nejedná	jednat	k5eNaImIp3nS
o	o	k7c6
pohoří	pohoří	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
erozivní	erozivní	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
v	v	k7c6
kvádrových	kvádrový	k2eAgInPc6d1
pískovcích	pískovec	k1gInPc6
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
s	s	k7c7
podložním	podložní	k2eAgNnSc7d1
krystalinikem	krystalinikum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skalní	skalní	k2eAgFnPc4d1
stěny	stěna	k1gFnPc4
měří	měřit	k5eAaImIp3nS
až	až	k9
70	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
ostatní	ostatní	k2eAgInPc1d1
skalní	skalní	k2eAgInPc1d1
útvary	útvar	k1gInPc1
jako	jako	k8xS,k8xC
římsy	říms	k1gInPc1
<g/>
,	,	kIx,
věže	věž	k1gFnPc1
či	či	k8xC
převisy	převis	k1gInPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
hojné	hojný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
oblasti	oblast	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
pseudokrasové	pseudokrasové	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
–	–	k?
z	z	k7c2
nejznámějších	známý	k2eAgNnPc2d3
např.	např.	kA
Pytlácká	pytlácký	k2eAgFnSc1d1
sluj	sluj	k1gFnSc1
<g/>
,	,	kIx,
Loupežnická	loupežnický	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
a	a	k8xC
Medvědí	medvědí	k2eAgFnSc1d1
sluj	sluj	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klima	klima	k1gNnSc1
</s>
<s>
Skalní	skalní	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
Růžový	růžový	k2eAgInSc4d1
hřeben	hřeben	k1gInSc4
v	v	k7c6
zimě	zima	k1gFnSc6
</s>
<s>
Skalní	skalní	k2eAgFnSc1d1
vyhlídka	vyhlídka	k1gFnSc1
Růžový	růžový	k2eAgInSc1d1
hřeben	hřeben	k1gInSc1
v	v	k7c6
létě	léto	k1gNnSc6
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
mírně	mírně	k6eAd1
teplou	teplý	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
<g/>
,	,	kIx,
s	s	k7c7
průměrnými	průměrný	k2eAgFnPc7d1
teplotami	teplota	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
v	v	k7c6
lednu	leden	k1gInSc6
okolo	okolo	k7c2
−	−	k?
až	až	k8xS
−	−	k?
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
v	v	k7c6
dubnu	duben	k1gInSc6
<g/>
:	:	kIx,
6	#num#	k4
až	až	k9
7	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
červenci	červenec	k1gInSc6
<g/>
:	:	kIx,
17	#num#	k4
až	až	k9
18	#num#	k4
°	°	k?
<g/>
C	C	kA
a	a	k8xC
říjnu	říjen	k1gInSc6
<g/>
:	:	kIx,
7	#num#	k4
až	až	k9
8	#num#	k4
°	°	k?
<g/>
C.	C.	kA
Častá	častý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
teplotní	teplotní	k2eAgFnSc1d1
inverze	inverze	k1gFnSc1
v	v	k7c6
údolích	údolí	k1gNnPc6
či	či	k8xC
mikroklima	mikroklima	k1gNnSc4
pokleslin	pokleslina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Flóra	Flóra	k1gFnSc1
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
je	být	k5eAaImIp3nS
lesnatá	lesnatý	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
na	na	k7c6
chudém	chudý	k2eAgInSc6d1
pískovcovém	pískovcový	k2eAgInSc6d1
podkladu	podklad	k1gInSc6
s	s	k7c7
převahou	převaha	k1gFnSc7
jehličnanů	jehličnan	k1gInPc2
(	(	kIx(
<g/>
až	až	k9
65	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
přirozený	přirozený	k2eAgInSc4d1
poměr	poměr	k1gInSc4
listnatých	listnatý	k2eAgInPc2d1
a	a	k8xC
jehličnatých	jehličnatý	k2eAgInPc2d1
lesů	les	k1gInPc2
opačný	opačný	k2eAgMnSc1d1
<g/>
,	,	kIx,
obdělávaný	obdělávaný	k2eAgMnSc1d1
jen	jen	k9
na	na	k7c6
Růžovské	Růžovský	k2eAgFnSc6d1
tabuli	tabule	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jehličnatých	jehličnatý	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
je	být	k5eAaImIp3nS
nejhojnější	hojný	k2eAgInSc1d3
smrk	smrk	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
(	(	kIx(
<g/>
Picea	Picea	k1gMnSc1
abies	abies	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
31	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
borovice	borovice	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Pinus	Pinus	k1gMnSc1
sylvestris	sylvestris	k1gFnSc2
<g/>
)	)	kIx)
–	–	k?
29	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
z	z	k7c2
nepůvodních	původní	k2eNgInPc2d1
modřín	modřín	k1gInSc4
(	(	kIx(
<g/>
Larix	Larix	k1gInSc4
<g/>
)	)	kIx)
–	–	k?
3	#num#	k4
%	%	kIx~
a	a	k8xC
borovice	borovice	k1gFnSc1
vejmutovka	vejmutovka	k1gFnSc1
(	(	kIx(
<g/>
Pinus	Pinus	k1gMnSc1
strobus	strobus	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
1	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
téměř	téměř	k6eAd1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
jedle	jedle	k6eAd1
bělokorá	bělokorý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Abies	Abies	k1gInSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc1
přirozený	přirozený	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
sahal	sahat	k5eAaImAgInS
až	až	k9
k	k	k7c3
6	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
listnatých	listnatý	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
je	být	k5eAaImIp3nS
nejčastější	častý	k2eAgInSc4d3
buk	buk	k1gInSc4
lesní	lesní	k2eAgInSc4d1
(	(	kIx(
<g/>
Fagus	Fagus	k1gInSc4
sylvatica	sylvatic	k1gInSc2
<g/>
)	)	kIx)
–	–	k?
20	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
bříza	bříza	k1gFnSc1
bradavičnatá	bradavičnatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Betula	Betula	k1gFnSc1
pendula	pendula	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
9	#num#	k4
%	%	kIx~
a	a	k8xC
dub	dub	k1gInSc1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
4	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedinečná	jedinečný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
vzniklá	vzniklý	k2eAgFnSc1d1
kombinací	kombinace	k1gFnSc7
pískovcového	pískovcový	k2eAgMnSc2d1
a	a	k8xC
říčního	říční	k2eAgInSc2d1
fenoménu	fenomén	k1gInSc2
umožňuje	umožňovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
značnou	značný	k2eAgFnSc4d1
druhovou	druhový	k2eAgFnSc4d1
rozmanitost	rozmanitost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
podpořená	podpořený	k2eAgFnSc1d1
i	i	k8xC
střetem	střet	k1gInSc7
porostů	porost	k1gInPc2
typických	typický	k2eAgInPc2d1
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
nadmořské	nadmořský	k2eAgFnPc4d1
výšky	výška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlivem	vlivem	k7c2
oceánského	oceánský	k2eAgNnSc2d1
klimatu	klima	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
rozvinula	rozvinout	k5eAaPmAgFnS
i	i	k9
ojedinělá	ojedinělý	k2eAgFnSc1d1
mykoflora	mykoflora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
plošinách	plošina	k1gFnPc6
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
borové	borový	k2eAgFnPc1d1
doubravy	doubrava	k1gFnPc1
<g/>
,	,	kIx,
na	na	k7c6
hranách	hrana	k1gFnPc6
skalních	skalní	k2eAgFnPc2d1
stěn	stěna	k1gFnPc2
boreokontinentální	boreokontinentální	k2eAgFnSc1d1
bory	bor	k1gInPc7
a	a	k8xC
nejrozsáhlejším	rozsáhlý	k2eAgInSc7d3
biotopem	biotop	k1gInSc7
jsou	být	k5eAaImIp3nP
acidofilní	acidofilní	k2eAgFnPc4d1
bukové	bukový	k2eAgFnPc4d1
bučiny	bučina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ohroženým	ohrožený	k2eAgMnPc3d1
druhům	druh	k1gMnPc3
zde	zde	k6eAd1
patří	patřit	k5eAaImIp3nS
jedle	jedle	k6eAd1
bělokorá	bělokorý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Abies	Abies	k1gInSc1
alba	album	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
udatna	udatna	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
(	(	kIx(
<g/>
Aruncus	Aruncus	k1gMnSc1
vulgaris	vulgaris	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
rojovník	rojovník	k1gInSc1
bahenní	bahenní	k2eAgInSc1d1
(	(	kIx(
<g/>
Ledum	Ledum	k1gInSc1
palustre	palustr	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fauna	fauna	k1gFnSc1
</s>
<s>
Z	z	k7c2
Evropského	evropský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
významný	významný	k2eAgInSc4d1
biokoridor	biokoridor	k1gInSc4
<g/>
:	:	kIx,
propojení	propojení	k1gNnSc4
se	s	k7c7
Severoněmeckou	severoněmecký	k2eAgFnSc7d1
nížinou	nížina	k1gFnSc7
<g/>
,	,	kIx,
až	až	k9
k	k	k7c3
atlantickým	atlantický	k2eAgFnPc3d1
oblastem	oblast	k1gFnPc3
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
umožňuje	umožňovat	k5eAaImIp3nS
jak	jak	k6eAd1
tahy	tah	k1gInPc4
lososů	losos	k1gMnPc2
na	na	k7c4
naše	náš	k3xOp1gNnSc4
území	území	k1gNnSc4
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
návrat	návrat	k1gInSc1
původních	původní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
znovuobnovení	znovuobnovení	k1gNnSc4
výskytu	výskyt	k1gInSc2
bobra	bobr	k1gMnSc2
evropského	evropský	k2eAgMnSc2d1
(	(	kIx(
<g/>
Castor	Castor	k1gInSc1
fiber	fiber	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
malou	malý	k2eAgFnSc4d1
nadmořskou	nadmořský	k2eAgFnSc4d1
výšku	výška	k1gFnSc4
(	(	kIx(
<g/>
130	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
též	též	k9
důležitý	důležitý	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
horských	horský	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
preferujících	preferující	k2eAgFnPc2d1
oblasti	oblast	k1gFnSc3
s	s	k7c7
teplotní	teplotní	k2eAgFnSc7d1
inverzí	inverze	k1gFnSc7
v	v	k7c6
údolích	údolí	k1gNnPc6
bočních	boční	k2eAgFnPc2d1
přítoků	přítok	k1gInPc2
Labe	Labe	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
pozůstatek	pozůstatek	k1gInSc1
poslední	poslední	k2eAgFnSc2d1
doby	doba	k1gFnSc2
ledové	ledový	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modelovým	modelový	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nosatec	nosatec	k1gMnSc1
Plintus	Plintus	k1gInSc4
soroeensis	soroeensis	k1gFnSc2
původem	původ	k1gInSc7
z	z	k7c2
Karpat	Karpaty	k1gInPc2
či	či	k8xC
koník	koník	k1gMnSc1
Troglophilus	Troglophilus	k1gMnSc1
neglectus	neglectus	k1gMnSc1
z	z	k7c2
mediteránu	mediterán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
role	role	k1gFnSc1
náleží	náležet	k5eAaImIp3nS
zvláštním	zvláštní	k2eAgFnPc3d1
dlouhodobým	dlouhodobý	k2eAgFnPc3d1
trofickým	trofický	k2eAgFnPc3d1
vazbám	vazba	k1gFnPc3
–	–	k?
např.	např.	kA
mezi	mezi	k7c7
smrkem	smrk	k1gInSc7
ztepilým	ztepilý	k2eAgInSc7d1
a	a	k8xC
hmyzem	hmyz	k1gInSc7
<g/>
,	,	kIx,
hlavně	hlavně	k9
nosatcem	nosatec	k1gMnSc7
Acalles	Acalles	k1gInSc4
boehmei	boehme	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
populace	populace	k1gFnPc1
žijí	žít	k5eAaImIp3nP
ve	v	k7c6
ztrouchnivělém	ztrouchnivělý	k2eAgNnSc6d1
smrkovém	smrkový	k2eAgNnSc6d1
dřevu	dřevo	k1gNnSc6
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
důležité	důležitý	k2eAgNnSc1d1
ponechávat	ponechávat	k5eAaImF
v	v	k7c6
lesích	les	k1gInPc6
spadlé	spadlý	k2eAgInPc1d1
stromy	strom	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeskyních	jeskyně	k1gFnPc6
žijí	žít	k5eAaImIp3nP
netopýři	netopýr	k1gMnPc1
a	a	k8xC
koník	koník	k1gMnSc1
jeskynní	jeskynní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Troglophilus	Troglophilus	k1gMnSc1
neglectus	neglectus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
kriticky	kriticky	k6eAd1
ohrožené	ohrožený	k2eAgInPc4d1
druhy	druh	k1gInPc4
z	z	k7c2
Červené	Červené	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
sokol	sokol	k1gMnSc1
stěhovavý	stěhovavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Falco	Falco	k1gMnSc1
peregrinus	peregrinus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
orlovec	orlovec	k1gInSc1
říční	říční	k2eAgInSc1d1
(	(	kIx(
<g/>
Pandion	Pandion	k1gInSc1
haliaetus	haliaetus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
orel	orel	k1gMnSc1
mořský	mořský	k2eAgMnSc1d1
(	(	kIx(
<g/>
Haliaeetus	Haliaeetus	k1gMnSc1
alnbiciolla	alnbiciolla	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vrápenec	vrápenec	k1gMnSc1
malý	malý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Rhinolophus	Rhinolophus	k1gInSc1
hipposideros	hipposiderosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
raroh	raroh	k1gMnSc1
velký	velký	k2eAgMnSc1d1
(	(	kIx(
<g/>
Falco	Falco	k1gMnSc1
cherrug	cherrug	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
plch	plch	k1gMnSc1
zahradní	zahradní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Eliomys	Eliomys	k1gInSc1
quercinus	quercinus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
skokan	skokan	k1gMnSc1
skřehotavý	skřehotavý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Rana	Rana	k1gFnSc1
ridibunda	ridibunda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koník	koník	k1gMnSc1
jeskynní	jeskynní	k2eAgMnSc1d1
(	(	kIx(
<g/>
Troglophilus	Troglophilus	k1gMnSc1
neglectus	neglectus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ochrana	ochrana	k1gFnSc1
a	a	k8xC
budoucí	budoucí	k2eAgInPc1d1
plány	plán	k1gInPc1
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
vymezení	vymezení	k1gNnSc1
bezzásahové	bezzásahový	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
území	území	k1gNnSc1
ponechá	ponechat	k5eAaPmIp3nS
samovolnému	samovolný	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
<g/>
,	,	kIx,
a	a	k8xC
postupně	postupně	k6eAd1
posouvání	posouvání	k1gNnSc1
celého	celý	k2eAgNnSc2d1
území	území	k1gNnSc2
k	k	k7c3
tomuto	tento	k3xDgInSc3
trendu	trend	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provádění	provádění	k1gNnSc1
jen	jen	k6eAd1
nutné	nutný	k2eAgFnSc2d1
těžby	těžba	k1gFnSc2
(	(	kIx(
<g/>
kácení	kácení	k1gNnSc1
pro	pro	k7c4
člověka	člověk	k1gMnSc4
nebezpečných	bezpečný	k2eNgFnPc2d1
dřevin	dřevina	k1gFnPc2
v	v	k7c6
okrajových	okrajový	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
mimovegetačním	mimovegetační	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zabránilo	zabránit	k5eAaPmAgNnS
ohrožení	ohrožení	k1gNnSc3
rozmnožování	rozmnožování	k1gNnSc2
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
neodstraňování	neodstraňování	k1gNnSc1
trouchnivějícího	trouchnivějící	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
diverzity	diverzita	k1gFnSc2
minimálním	minimální	k2eAgNnSc7d1
hospodářským	hospodářský	k2eAgNnSc7d1
využíváním	využívání	k1gNnSc7
krajiny	krajina	k1gFnSc2
+	+	kIx~
důraz	důraz	k1gInSc1
na	na	k7c4
přirozenou	přirozený	k2eAgFnSc4d1
skladbu	skladba	k1gFnSc4
s	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
potlačením	potlačení	k1gNnSc7
nepůvodních	původní	k2eNgInPc2d1
druhů	druh	k1gInPc2
jako	jako	k8xS,k8xC
borovice	borovice	k1gFnSc1
vejmutovka	vejmutovka	k1gFnSc1
(	(	kIx(
<g/>
Pinus	Pinus	k1gMnSc1
strobus	strobus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
modřín	modřín	k1gInSc1
opadavý	opadavý	k2eAgInSc1d1
(	(	kIx(
<g/>
Larix	Larix	k1gInSc1
decidua	decidu	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dub	dub	k1gInSc1
červený	červený	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gInSc1
rubra	rubrum	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
netýkavka	netýkavka	k1gFnSc1
žlaznatá	žlaznatý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Impatiens	Impatiens	k1gInSc1
glandulifera	glandulifero	k1gNnSc2
<g/>
)	)	kIx)
a	a	k8xC
trnovník	trnovník	k1gInSc1
akát	akát	k1gInSc1
(	(	kIx(
<g/>
Robinia	Robinium	k1gNnSc2
pseudoacacia	pseudoacacium	k1gNnSc2
<g/>
,	,	kIx,
syn	syn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robinia	Robinium	k1gNnSc2
acacia	acacium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regulace	regulace	k1gFnSc1
srnčí	srnčí	k2eAgFnSc2d1
a	a	k8xC
černé	černý	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bránění	bránění	k1gNnSc1
vzniku	vznik	k1gInSc2
černých	černý	k2eAgFnPc2d1
skládek	skládka	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
silnice	silnice	k1gFnSc2
z	z	k7c2
Děčína	Děčín	k1gInSc2
do	do	k7c2
Hřenska	Hřensko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turistika	turistika	k1gFnSc1
<g/>
,	,	kIx,
rekreace	rekreace	k1gFnSc1
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
je	být	k5eAaImIp3nS
z	z	k7c2
turistického	turistický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
méně	málo	k6eAd2
navštěvovaným	navštěvovaný	k2eAgNnSc7d1
územím	území	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc7d3
vyhlídkou	vyhlídka	k1gFnSc7
je	být	k5eAaImIp3nS
uměle	uměle	k6eAd1
vytvořená	vytvořený	k2eAgFnSc1d1
skalní	skalní	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
Belvedér	belvedér	k1gInSc4
u	u	k7c2
Labské	labský	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirozené	přirozený	k2eAgNnSc4d1
výhledové	výhledový	k2eAgNnSc4d1
místo	místo	k1gNnSc4
na	na	k7c4
kaňon	kaňon	k1gInSc4
<g/>
,	,	kIx,
Labe	Labe	k1gNnSc4
<g/>
,	,	kIx,
horolezce	horolezec	k1gMnPc4
a	a	k8xC
mnohé	mnohé	k1gNnSc4
další	další	k2eAgNnSc4d1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přímo	přímo	k6eAd1
na	na	k7c4
hranici	hranice	k1gFnSc4
rezervace	rezervace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větší	veliký	k2eAgInSc4d2
význam	význam	k1gInSc4
má	mít	k5eAaImIp3nS
horolezectví	horolezectví	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
velmi	velmi	k6eAd1
tradiční	tradiční	k2eAgFnSc7d1
záležitostí	záležitost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
tu	tu	k6eAd1
1760	#num#	k4
cest	cesta	k1gFnPc2
a	a	k8xC
jde	jít	k5eAaImIp3nS
o	o	k7c4
nejzatíženější	zatížený	k2eAgFnSc4d3
oblast	oblast	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
Labských	labský	k2eAgInPc2d1
pískovců	pískovec	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
je	být	k5eAaImIp3nS
zákaz	zákaz	k1gInSc1
horolezectví	horolezectví	k1gNnSc2
na	na	k7c6
malém	malý	k2eAgInSc6d1
úseku	úsek	k1gInSc6
z	z	k7c2
důvodu	důvod	k1gInSc2
hrozby	hrozba	k1gFnSc2
padání	padání	k1gNnSc2
skal	skála	k1gFnPc2
<g/>
,	,	kIx,
roce	rok	k1gInSc6
2010	#num#	k4
spolu	spolu	k6eAd1
s	s	k7c7
vyhlášením	vyhlášení	k1gNnSc7
NPR	NPR	kA
začal	začít	k5eAaPmAgMnS
platit	platit	k5eAaImF
celoplošný	celoplošný	k2eAgInSc4d1
zákaz	zákaz	k1gInSc4
provozování	provozování	k1gNnSc2
horolezecké	horolezecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c6
krátké	krátký	k2eAgFnSc6d1
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
území	území	k1gNnSc4
povolena	povolen	k2eAgFnSc1d1
výjimka	výjimka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
horolezectví	horolezectví	k1gNnSc1
povoleno	povolen	k2eAgNnSc1d1
s	s	k7c7
určitými	určitý	k2eAgNnPc7d1
omezeními	omezení	k1gNnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
zákaz	zákaz	k1gInSc1
používání	používání	k1gNnSc2
magnesia	magnesium	k1gNnSc2
<g/>
,	,	kIx,
vklíněnců	vklíněnec	k1gMnPc2
k	k	k7c3
jištění	jištění	k1gNnSc3
<g/>
,	,	kIx,
popisování	popisování	k1gNnSc3
skal	skála	k1gFnPc2
či	či	k8xC
rušení	rušení	k1gNnSc1
hnízdících	hnízdící	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
Kaňon	kaňon	k1gInSc4
Labe	Labe	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
GLOCKNER	GLOCKNER	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fyzickogeografické	fyzickogeografický	k2eAgInPc1d1
a	a	k8xC
geologické	geologický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
okresu	okres	k1gInSc2
Děčín	Děčín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děčín	Děčín	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
191	#num#	k4
s.	s.	k?
1	#num#	k4
2	#num#	k4
KOŠŤÁL	Košťál	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
SMÍŠKOVÁ	Smíšková	k1gFnSc1
<g/>
,	,	kIx,
H.	H.	kA
Přírodou	příroda	k1gFnSc7
Děčínska	Děčínsko	k1gNnSc2
*	*	kIx~
<g/>
I.	I.	kA
Děčín	Děčín	k1gInSc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Děčíně	Děčín	k1gInSc6
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
282	#num#	k4
s.	s.	k?
1	#num#	k4
2	#num#	k4
3	#num#	k4
Plán	plán	k1gInSc1
péče	péče	k1gFnSc2
o	o	k7c4
NPR	NPR	kA
Kaňon	kaňon	k1gInSc4
Labe	Labe	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
Životního	životní	k2eAgNnSc2d1
Prostředí	prostředí	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
GLOCKNER	GLOCKNER	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fyzickogeografické	fyzickogeografický	k2eAgInPc1d1
a	a	k8xC
geologické	geologický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
okresu	okres	k1gInSc2
Děčín	Děčín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děčín	Děčín	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
191	#num#	k4
s.	s.	k?
</s>
<s>
KOŠŤÁL	Košťál	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
SMÍŠKOVÁ	Smíšková	k1gFnSc1
<g/>
,	,	kIx,
H.	H.	kA
Přírodou	příroda	k1gFnSc7
Děčínska	Děčínsko	k1gNnSc2
*	*	kIx~
<g/>
I.	I.	kA
Děčín	Děčín	k1gInSc1
<g/>
:	:	kIx,
Okresní	okresní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
Děčíně	Děčín	k1gInSc6
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
282	#num#	k4
s.	s.	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
národních	národní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Děčín	Děčín	k1gInSc1
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
České	český	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Kaňon	kaňon	k1gInSc1
Labe	Labe	k1gNnSc2
•	•	k?
Růžovský	Růžovský	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Březinské	Březinský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Pravčická	Pravčická	k1gFnSc1
brána	brána	k1gFnSc1
•	•	k?
Zlatý	zlatý	k2eAgInSc4d1
vrch	vrch	k1gInSc4
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Arba	arba	k1gFnSc1
•	•	k?
Bohyňská	Bohyňský	k2eAgFnSc1d1
lada	lado	k1gNnSc2
•	•	k?
Čabel	Čabel	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Jílového	Jílové	k1gNnSc2
•	•	k?
Kamenná	kamenný	k2eAgFnSc1d1
hůra	hůra	k1gFnSc1
•	•	k?
Maiberg	Maiberg	k1gInSc1
•	•	k?
Marschnerova	Marschnerův	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Pavlínino	Pavlínin	k2eAgNnSc4d1
údolí	údolí	k1gNnSc4
•	•	k?
Pekelský	pekelský	k2eAgInSc1d1
důl	důl	k1gInSc1
•	•	k?
Spravedlnost	spravedlnost	k1gFnSc1
•	•	k?
Studený	studený	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Světlík	světlík	k1gInSc1
•	•	k?
Vápenka	vápenka	k1gFnSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Vrabinec	Vrabinec	k1gInSc1
•	•	k?
Za	za	k7c7
pilou	pila	k1gFnSc7
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bobří	bobří	k2eAgFnSc1d1
soutěska	soutěska	k1gFnSc1
•	•	k?
Hofberg	Hofberg	k1gInSc1
•	•	k?
Jeskyně	jeskyně	k1gFnSc1
pod	pod	k7c7
Sněžníkem	Sněžník	k1gInSc7
•	•	k?
Jílovské	jílovský	k2eAgInPc4d1
tisy	tis	k1gInPc4
•	•	k?
Kytlice	kytlice	k1gFnSc2
•	•	k?
Líska	líska	k1gFnSc1
•	•	k?
Louka	louka	k1gFnSc1
u	u	k7c2
Brodských	Brodská	k1gFnPc2
•	•	k?
Meandry	meandr	k1gInPc1
Chřibské	chřibský	k2eAgFnSc2d1
Kamenice	Kamenice	k1gFnSc2
•	•	k?
Nebočadský	Nebočadský	k2eAgInSc1d1
luh	luh	k1gInSc1
•	•	k?
Noldenteich	Noldenteich	k1gInSc1
•	•	k?
Pod	pod	k7c7
lesem	les	k1gInSc7
•	•	k?
Rybník	rybník	k1gInSc1
u	u	k7c2
Králova	Králův	k2eAgInSc2d1
mlýna	mlýn	k1gInSc2
•	•	k?
Sojčí	sojčí	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
Oleška	Olešek	k1gMnSc2
•	•	k?
Stříbrný	stříbrný	k2eAgInSc4d1
roh	roh	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
