<s>
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděný	uváděný	k2eAgInSc1d1	uváděný
i	i	k9	i
jako	jako	k9	jako
Apollón	Apollón	k1gMnSc1	Apollón
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
,	,	kIx,	,
Potrestání	potrestání	k1gNnSc1	potrestání
Marsya	Marsya	k1gMnSc1	Marsya
<g/>
,	,	kIx,	,
Apollo	Apollo	k1gMnSc1	Apollo
trestá	trestat	k5eAaImIp3nS	trestat
Marsya	Marsyum	k1gNnSc2	Marsyum
nebo	nebo	k8xC	nebo
Stahování	stahování	k1gNnSc2	stahování
Marsya	Marsy	k1gInSc2	Marsy
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc4	obraz
italského	italský	k2eAgInSc2d1	italský
pozdně	pozdně	k6eAd1	pozdně
renesančního	renesanční	k2eAgMnSc2d1	renesanční
umělce	umělec	k1gMnSc2	umělec
Tiziana	Tizian	k1gMnSc2	Tizian
<g/>
.	.	kIx.	.
</s>
