<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
rakouskými	rakouský	k2eAgFnPc7d1	rakouská
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Solnohradskem	Solnohradsko	k1gNnSc7	Solnohradsko
a	a	k8xC	a
Horními	horní	k2eAgInPc7d1	horní
Rakousy	Rakousy	k1gInPc7	Rakousy
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
rakouskými	rakouský	k2eAgFnPc7d1	rakouská
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
<g/>
,	,	kIx,	,
Vorarlberskem	Vorarlbersko	k1gNnSc7	Vorarlbersko
a	a	k8xC	a
Solnohradskem	Solnohradsko	k1gNnSc7	Solnohradsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Bádenskem-Württemberskem	Bádenskem-Württembersek	k1gInSc7	Bádenskem-Württembersek
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
německými	německý	k2eAgFnPc7d1	německá
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Hesenskem	Hesensko	k1gNnSc7	Hesensko
<g/>
,	,	kIx,	,
Svobodným	svobodný	k2eAgInSc7d1	svobodný
státem	stát	k1gInSc7	stát
Durynsko	Durynsko	k1gNnSc4	Durynsko
a	a	k8xC	a
Svobodným	svobodný	k2eAgInSc7d1	svobodný
státem	stát	k1gInSc7	stát
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
.	.	kIx.	.
</s>
