<s>
Vzácně	vzácně	k6eAd1	vzácně
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
přílišné	přílišný	k2eAgFnSc3d1	přílišná
produkci	produkce	k1gFnSc3	produkce
melaninu	melanin	k1gInSc2	melanin
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
melanismus	melanismus	k1gInSc1	melanismus
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
melanin	melanin	k1gInSc1	melanin
je	být	k5eAaImIp3nS	být
produkován	produkovat	k5eAaImNgInS	produkovat
v	v	k7c6	v
nižším	nízký	k2eAgNnSc6d2	nižší
množství	množství	k1gNnSc6	množství
(	(	kIx(	(
<g/>
albinismus	albinismus	k1gInSc1	albinismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
