<p>
<s>
Stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
zdroj	zdroj	k1gInSc1	zdroj
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
o	o	k7c6	o
požadovaném	požadovaný	k2eAgNnSc6d1	požadované
napětí	napětí	k1gNnSc6	napětí
do	do	k7c2	do
elektrického	elektrický	k2eAgInSc2d1	elektrický
obvodu	obvod	k1gInSc2	obvod
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ji	on	k3xPp3gFnSc4	on
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgInSc1d1	ideální
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
zdroj	zdroj	k1gInSc1	zdroj
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
konstantní	konstantní	k2eAgNnSc1d1	konstantní
napětí	napětí	k1gNnSc1	napětí
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c6	na
odebíraném	odebíraný	k2eAgInSc6d1	odebíraný
proudu	proud	k1gInSc6	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnSc2	konstrukce
==	==	k?	==
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgInSc1d1	lineární
stabilizovaný	stabilizovaný	k2eAgInSc1d1	stabilizovaný
zdroj	zdroj	k1gInSc1	zdroj
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
4	[number]	k4	4
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Transformátor	transformátor	k1gInSc1	transformátor
<g/>
,	,	kIx,	,
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
<g/>
,	,	kIx,	,
filtr	filtr	k1gInSc1	filtr
a	a	k8xC	a
stabilizátor	stabilizátor	k1gInSc1	stabilizátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Transformátor	transformátor	k1gInSc4	transformátor
===	===	k?	===
</s>
</p>
<p>
<s>
Úkolem	úkol	k1gInSc7	úkol
transformátoru	transformátor	k1gInSc2	transformátor
ve	v	k7c6	v
zdroji	zdroj	k1gInSc6	zdroj
je	být	k5eAaImIp3nS	být
snížit	snížit	k5eAaPmF	snížit
střídavé	střídavý	k2eAgNnSc4d1	střídavé
napětí	napětí	k1gNnSc4	napětí
z	z	k7c2	z
rozvodné	rozvodný	k2eAgFnSc2d1	rozvodná
sítě	síť	k1gFnSc2	síť
na	na	k7c4	na
nižší	nízký	k2eAgNnSc4d2	nižší
<g/>
,	,	kIx,	,
bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ale	ale	k9	ale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgFnSc4d2	vyšší
než	než	k8xS	než
napětí	napětí	k1gNnSc6	napětí
které	který	k3yIgInPc4	který
požadujeme	požadovat	k5eAaImIp1nP	požadovat
na	na	k7c6	na
výstupu	výstup	k1gInSc6	výstup
zdroje	zdroj	k1gInSc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
primární	primární	k2eAgNnSc4d1	primární
vinutí	vinutí	k1gNnSc4	vinutí
transformátoru	transformátor	k1gInSc2	transformátor
je	být	k5eAaImIp3nS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
síťové	síťový	k2eAgNnSc1d1	síťové
napětí	napětí	k1gNnSc1	napětí
a	a	k8xC	a
na	na	k7c4	na
sekundární	sekundární	k2eAgNnSc4d1	sekundární
vinutí	vinutí	k1gNnSc4	vinutí
je	být	k5eAaImIp3nS	být
připojen	připojen	k2eAgInSc1d1	připojen
další	další	k2eAgInSc1d1	další
blok	blok	k1gInSc1	blok
zdroje	zdroj	k1gInPc1	zdroj
–	–	k?	–
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
napětí	napětí	k1gNnSc1	napětí
na	na	k7c6	na
sekundárním	sekundární	k2eAgNnSc6d1	sekundární
vinutí	vinutí	k1gNnSc6	vinutí
transformátoru	transformátor	k1gInSc2	transformátor
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
střídavé	střídavý	k2eAgNnSc1d1	střídavé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Usměrňovač	usměrňovač	k1gInSc4	usměrňovač
===	===	k?	===
</s>
</p>
<p>
<s>
Usměrňovač	usměrňovač	k1gInSc1	usměrňovač
mění	měnit	k5eAaImIp3nS	měnit
střídavé	střídavý	k2eAgNnSc4d1	střídavé
napětí	napětí	k1gNnSc4	napětí
na	na	k7c4	na
stejnosměrné	stejnosměrný	k2eAgFnPc4d1	stejnosměrná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používáme	používat	k5eAaImIp1nP	používat
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
druhy	druh	k1gInPc4	druh
usměrňovačů	usměrňovač	k1gInPc2	usměrňovač
<g/>
:	:	kIx,	:
Jednocestný	jednocestný	k2eAgInSc4d1	jednocestný
a	a	k8xC	a
dvoucestný	dvoucestný	k2eAgInSc4d1	dvoucestný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednocestný	jednocestný	k2eAgInSc1d1	jednocestný
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
jednou	jednou	k9	jednou
diodou	dioda	k1gFnSc7	dioda
zapojenou	zapojený	k2eAgFnSc7d1	zapojená
v	v	k7c6	v
propustném	propustný	k2eAgInSc6d1	propustný
směru	směr	k1gInSc6	směr
sériově	sériově	k6eAd1	sériově
k	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
z	z	k7c2	z
transformátoru	transformátor	k1gInSc2	transformátor
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
usměrňovačem	usměrňovač	k1gInSc7	usměrňovač
bude	být	k5eAaImBp3nS	být
pouze	pouze	k6eAd1	pouze
půlvlna	půlvlna	k1gFnSc1	půlvlna
v	v	k7c6	v
kladných	kladný	k2eAgFnPc6d1	kladná
hodnotách	hodnota	k1gFnPc6	hodnota
napětí	napětí	k1gNnSc2	napětí
stále	stále	k6eAd1	stále
ve	v	k7c6	v
frekvenci	frekvence	k1gFnSc6	frekvence
50	[number]	k4	50
<g/>
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
střídá	střídat	k5eAaImIp3nS	střídat
se	se	k3xPyFc4	se
doba	doba	k1gFnSc1	doba
jedné	jeden	k4xCgFnSc2	jeden
půlvlny	půlvlna	k1gFnSc2	půlvlna
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
napětí	napětí	k1gNnSc1	napětí
je	být	k5eAaImIp3nS	být
nulové	nulový	k2eAgNnSc1d1	nulové
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
krajních	krajní	k2eAgInPc6d1	krajní
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nám	my	k3xPp1nPc3	my
jde	jít	k5eAaImIp3nS	jít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
rychlé	rychlý	k2eAgNnSc4d1	rychlé
zrušení	zrušení	k1gNnSc4	zrušení
střídání	střídání	k1gNnSc2	střídání
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvoucestný	dvoucestný	k2eAgInSc1d1	dvoucestný
usměrňovač	usměrňovač	k1gInSc1	usměrňovač
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
diodami	dioda	k1gFnPc7	dioda
zapojenými	zapojený	k2eAgFnPc7d1	zapojená
tak	tak	k8xC	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
propouštěly	propouštět	k5eAaImAgFnP	propouštět
kladné	kladný	k2eAgFnPc1d1	kladná
půlvlny	půlvlna	k1gFnPc1	půlvlna
a	a	k8xC	a
záporné	záporný	k2eAgFnPc1d1	záporná
půlvlny	půlvlna	k1gFnPc1	půlvlna
otáčely	otáčet	k5eAaImAgFnP	otáčet
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
usměrňovačem	usměrňovač	k1gInSc7	usměrňovač
bude	být	k5eAaImBp3nS	být
stejnosměrné	stejnosměrný	k2eAgNnSc1d1	stejnosměrné
pulzující	pulzující	k2eAgNnSc1d1	pulzující
napětí	napětí	k1gNnSc1	napětí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filtr	filtr	k1gInSc4	filtr
===	===	k?	===
</s>
</p>
<p>
<s>
Filtr	filtr	k1gInSc1	filtr
je	být	k5eAaImIp3nS	být
elektronické	elektronický	k2eAgNnSc4d1	elektronické
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
stejnosměrné	stejnosměrný	k2eAgNnSc4d1	stejnosměrné
napětí	napětí	k1gNnSc4	napětí
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mělo	mít	k5eAaImAgNnS	mít
co	co	k3yRnSc1	co
nejmenší	malý	k2eAgNnSc1d3	nejmenší
zvlnění	zvlnění	k1gNnSc1	zvlnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgInSc1d3	nejjednodušší
filtr	filtr	k1gInSc1	filtr
je	být	k5eAaImIp3nS	být
kondenzátor	kondenzátor	k1gInSc4	kondenzátor
připojený	připojený	k2eAgInSc4d1	připojený
paralelně	paralelně	k6eAd1	paralelně
k	k	k7c3	k
výstupům	výstup	k1gInPc3	výstup
usměrňovače	usměrňovač	k1gInSc2	usměrňovač
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
amplitudy	amplituda	k1gFnSc2	amplituda
první	první	k4xOgFnSc2	první
kladné	kladný	k2eAgFnSc2d1	kladná
půlvlny	půlvlna	k1gFnSc2	půlvlna
nabije	nabít	k5eAaBmIp3nS	nabít
na	na	k7c4	na
napětí	napětí	k1gNnSc4	napětí
rovné	rovný	k2eAgFnSc2d1	rovná
amplitudě	amplituda	k1gFnSc3	amplituda
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vybíjí	vybíjet	k5eAaImIp3nS	vybíjet
<g/>
,	,	kIx,	,
až	až	k8xS	až
k	k	k7c3	k
příchodu	příchod	k1gInSc3	příchod
další	další	k2eAgFnSc2d1	další
kladné	kladný	k2eAgFnSc2d1	kladná
půlvlny	půlvlna	k1gFnSc2	půlvlna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
nabije	nabít	k5eAaBmIp3nS	nabít
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
amplitudu	amplituda	k1gFnSc4	amplituda
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
cyklus	cyklus	k1gInSc1	cyklus
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
větší	veliký	k2eAgFnSc1d2	veliký
kapacita	kapacita	k1gFnSc1	kapacita
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
tím	ten	k3xDgNnSc7	ten
vyrovnanější	vyrovnaný	k2eAgFnSc1d2	vyrovnanější
průběh	průběh	k1gInSc4	průběh
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
filtry	filtr	k1gInPc4	filtr
z	z	k7c2	z
kombinací	kombinace	k1gFnPc2	kombinace
rezistoru	rezistor	k1gInSc2	rezistor
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kondenzátoru	kondenzátor	k1gInSc2	kondenzátor
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
cívky	cívka	k1gFnSc2	cívka
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
například	například	k6eAd1	například
filtry	filtr	k1gInPc1	filtr
LC	LC	kA	LC
<g/>
,	,	kIx,	,
RC	RC	kA	RC
<g/>
,	,	kIx,	,
RLC	RLC	kA	RLC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Stabilizátor	stabilizátor	k1gInSc4	stabilizátor
===	===	k?	===
</s>
</p>
<p>
<s>
Stabilizátor	stabilizátor	k1gMnSc1	stabilizátor
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžeme	moct	k5eAaImIp1nP	moct
měnit	měnit	k5eAaImF	měnit
odebíraný	odebíraný	k2eAgInSc4d1	odebíraný
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napětí	napětí	k1gNnSc1	napětí
zůstane	zůstat	k5eAaPmIp3nS	zůstat
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
stabilizátor	stabilizátor	k1gInSc4	stabilizátor
můžeme	moct	k5eAaImIp1nP	moct
použít	použít	k5eAaPmF	použít
zenerovu	zenerův	k2eAgFnSc4d1	zenerova
diodu	dioda	k1gFnSc4	dioda
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
použít	použít	k5eAaPmF	použít
stabilizátory	stabilizátor	k1gInPc4	stabilizátor
integrované	integrovaný	k2eAgNnSc1d1	integrované
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
spínaných	spínaný	k2eAgInPc2d1	spínaný
zdrojů	zdroj	k1gInPc2	zdroj
neruší	rušit	k5eNaImIp3nS	rušit
síťové	síťový	k2eAgNnSc4d1	síťové
napětí	napětí	k1gNnSc4	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
konstrukčně	konstrukčně	k6eAd1	konstrukčně
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Výkonné	výkonný	k2eAgInPc1d1	výkonný
zdroje	zdroj	k1gInPc1	zdroj
jsou	být	k5eAaImIp3nP	být
rozměrné	rozměrný	k2eAgInPc1d1	rozměrný
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgInPc1d1	těžký
<g/>
,	,	kIx,	,
drahé	drahý	k2eAgInPc1d1	drahý
a	a	k8xC	a
méně	málo	k6eAd2	málo
účinné	účinný	k2eAgFnSc2d1	účinná
oproti	oproti	k7c3	oproti
spínaným	spínaný	k2eAgInPc3d1	spínaný
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Měnič	měnič	k1gMnSc1	měnič
</s>
</p>
<p>
<s>
Spínaný	spínaný	k2eAgInSc1d1	spínaný
zdroj	zdroj	k1gInSc1	zdroj
</s>
</p>
<p>
<s>
Elektrický	elektrický	k2eAgInSc1d1	elektrický
zdroj	zdroj	k1gInSc1	zdroj
</s>
</p>
