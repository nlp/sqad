<s>
Demokraté	demokrat	k1gMnPc1
66	#num#	k4
</s>
<s>
Demokraté	demokrat	k1gMnPc1
66	#num#	k4
<g/>
Democraten	Democratno	k1gNnPc2
66	#num#	k4
Zkratka	zkratka	k1gFnSc1
</s>
<s>
D66	D66	k4
Datum	datum	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1966	#num#	k4
Předsedkyně	předsedkyně	k1gFnSc1
</s>
<s>
Anne-Marie	Anne-Marie	k1gFnSc1
Spieringsová	Spieringsová	k1gFnSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Hans	Hans	k1gMnSc1
van	vana	k1gFnPc2
Mierlo	Mierlo	k1gNnSc1
a	a	k8xC
Hans	Hans	k1gMnSc1
Gruijters	Gruijters	k1gInSc1
(	(	kIx(
<g/>
politicus	politicus	k1gInSc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Partijbureau	Partijburea	k2eAgFnSc4d1
D66	D66	k1gFnSc4
Hoge	Hog	k1gFnSc2
Nieuwstraat	Nieuwstraat	k2eAgInSc4d1
30	#num#	k4
<g/>
Haag	Haag	k1gInSc4
Ideologie	ideologie	k1gFnSc2
</s>
<s>
sociální	sociální	k2eAgInSc1d1
liberalismus	liberalismus	k1gInSc1
proevropanismus	proevropanismus	k1gInSc1
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
střed	střed	k1gInSc1
Mezinárodní	mezinárodní	k2eAgInSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Liberální	liberální	k2eAgFnSc1d1
internacionála	internacionála	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Aliance	aliance	k1gFnSc1
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Renew	Renew	k?
Europe	Europ	k1gMnSc5
Mládežnická	mládežnický	k2eAgNnPc4d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
27	#num#	k4
121	#num#	k4
Barvy	barva	k1gFnPc1
</s>
<s>
zelená	zelenat	k5eAaImIp3nS
Oficiální	oficiální	k2eAgNnSc1d1
web	web	k1gInSc4
</s>
<s>
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Senát	senát	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1
reprezentantů	reprezentant	k1gMnPc2
<g/>
:	:	kIx,
</s>
<s>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
150	#num#	k4
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
</s>
<s>
Democraten	Democraten	k2eAgMnSc1d1
66	#num#	k4
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
D66	D66	k1gMnPc1
(	(	kIx(
<g/>
česky	česky	k6eAd1
Demokraté	demokrat	k1gMnPc1
66	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nizozemská	nizozemský	k2eAgFnSc1d1
sociálně	sociálně	k6eAd1
liberální	liberální	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založen	k2eAgFnSc1d1
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1966	#num#	k4
a	a	k8xC
patří	patřit	k5eAaImIp3nS
k	k	k7c3
aktivním	aktivní	k2eAgMnPc3d1
členům	člen	k1gMnPc3
evropské	evropský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Aliance	aliance	k1gFnSc2
liberálů	liberál	k1gMnPc2
a	a	k8xC
demokratů	demokrat	k1gMnPc2
pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
a	a	k8xC
Liberální	liberální	k2eAgFnPc4d1
internacionály	internacionála	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Demokraté	demokrat	k1gMnPc1
66	#num#	k4
byli	být	k5eAaImAgMnP
v	v	k7c6
minulosti	minulost	k1gFnSc6
součástí	součást	k1gFnPc2
vládních	vládní	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
<g/>
,	,	kIx,
V	v	k7c6
posledních	poslední	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
obdrželi	obdržet	k5eAaPmAgMnP
12,2	12,2	k4
<g/>
%	%	kIx~
hlasů	hlas	k1gInPc2
a	a	k8xC
19	#num#	k4
křesel	křeslo	k1gNnPc2
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
senátu	senát	k1gInSc6
mají	mít	k5eAaImIp3nP
po	po	k7c6
posledních	poslední	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
7	#num#	k4
senátorů	senátor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
strana	strana	k1gFnSc1
získala	získat	k5eAaPmAgFnS
dva	dva	k4xCgInPc4
europoslance	europoslanec	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
o	o	k7c4
dva	dva	k4xCgMnPc4
méně	málo	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
ve	v	k7c6
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
je	být	k5eAaImIp3nS
zelená	zelený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Demokraté	demokrat	k1gMnPc1
66	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
<g/>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
D66	D66	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81114436	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
216614488	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81114436	#num#	k4
</s>
