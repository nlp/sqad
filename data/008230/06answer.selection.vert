<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
De	De	k?	De
sterrennacht	sterrennacht	k1gInSc1	sterrennacht
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
od	od	k7c2	od
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
postimpresionisty	postimpresionista	k1gMnSc2	postimpresionista
Vincenta	Vincent	k1gMnSc2	Vincent
van	vana	k1gFnPc2	vana
Gogha	Gogh	k1gMnSc2	Gogh
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
převládají	převládat	k5eAaImIp3nP	převládat
barvy	barva	k1gFnPc4	barva
noci	noc	k1gFnPc4	noc
<g/>
.	.	kIx.	.
</s>
