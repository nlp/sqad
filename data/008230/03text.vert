<p>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
De	De	k?	De
sterrennacht	sterrennacht	k1gInSc1	sterrennacht
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
od	od	k7c2	od
nizozemského	nizozemský	k2eAgMnSc2d1	nizozemský
postimpresionisty	postimpresionista	k1gMnSc2	postimpresionista
Vincenta	Vincent	k1gMnSc2	Vincent
van	vana	k1gFnPc2	vana
Gogha	Gogh	k1gMnSc2	Gogh
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgMnSc6	jenž
převládají	převládat	k5eAaImIp3nP	převládat
barvy	barva	k1gFnPc4	barva
noci	noc	k1gFnPc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Olejomalbu	olejomalba	k1gFnSc4	olejomalba
namaloval	namalovat	k5eAaPmAgMnS	namalovat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnSc1	kompozice
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
pokojového	pokojový	k2eAgNnSc2d1	pokojové
okna	okno	k1gNnSc2	okno
sanatoria	sanatorium	k1gNnSc2	sanatorium
v	v	k7c6	v
jihofrancouzském	jihofrancouzský	k2eAgNnSc6d1	jihofrancouzské
městě	město	k1gNnSc6	město
Saint-Rémy-de-Provence	Saint-Rémye-Provence	k1gFnSc2	Saint-Rémy-de-Provence
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
obraz	obraz	k1gInSc1	obraz
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
noční	noční	k2eAgInSc4d1	noční
výjev	výjev	k1gInSc4	výjev
<g/>
,	,	kIx,	,
umělec	umělec	k1gMnSc1	umělec
jej	on	k3xPp3gMnSc4	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
po	po	k7c6	po
paměti	paměť	k1gFnSc6	paměť
během	během	k7c2	během
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
součástí	součást	k1gFnSc7	součást
stálé	stálý	k2eAgFnSc2d1	stálá
kolekce	kolekce	k1gFnSc2	kolekce
newyorského	newyorský	k2eAgNnSc2d1	newyorské
Muzea	muzeum	k1gNnSc2	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zahrnuto	zahrnout	k5eAaPmNgNnS	zahrnout
do	do	k7c2	do
odkazu	odkaz	k1gInSc2	odkaz
Lillie	Lillie	k1gFnSc2	Lillie
P.	P.	kA	P.
Blissové	Blissová	k1gFnSc2	Blissová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obraz	obraz	k1gInSc1	obraz
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
malby	malba	k1gFnSc2	malba
Van	vana	k1gFnPc2	vana
Gogha	Gogha	k1gMnSc1	Gogha
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
uměleckém	umělecký	k2eAgInSc6d1	umělecký
vývoji	vývoj	k1gInSc6	vývoj
představuje	představovat	k5eAaImIp3nS	představovat
konečný	konečný	k2eAgInSc1d1	konečný
příklon	příklon	k1gInSc1	příklon
k	k	k7c3	k
výraznějšímu	výrazný	k2eAgNnSc3d2	výraznější
imaginativnímu	imaginativní	k2eAgNnSc3d1	imaginativní
osvobození	osvobození	k1gNnSc3	osvobození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
dubna	duben	k1gInSc2	duben
1888	[number]	k4	1888
směřovaném	směřovaný	k2eAgInSc6d1	směřovaný
příteli	přítel	k1gMnSc3	přítel
Émilu	Émil	k1gMnSc3	Émil
Bernardovi	Bernard	k1gMnSc3	Bernard
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Van	van	k1gInSc4	van
Gogh	Gogh	k1gMnSc1	Gogh
touhu	touha	k1gFnSc4	touha
namalovat	namalovat	k5eAaPmF	namalovat
noční	noční	k2eAgFnSc4d1	noční
oblohu	obloha	k1gFnSc4	obloha
a	a	k8xC	a
vznesl	vznést	k5eAaPmAgMnS	vznést
dotaz	dotaz	k1gInSc4	dotaz
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
realizovat	realizovat	k5eAaBmF	realizovat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
to	ten	k3xDgNnSc4	ten
činili	činit	k5eAaImAgMnP	činit
impresionisté	impresionista	k1gMnPc1	impresionista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
před	před	k7c7	před
prosincovým	prosincový	k2eAgNnSc7d1	prosincové
zhroucením	zhroucení	k1gNnSc7	zhroucení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c6	v
hospitalizaci	hospitalizace	k1gFnSc6	hospitalizace
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
<g/>
,	,	kIx,	,
namaloval	namalovat	k5eAaPmAgMnS	namalovat
obraz	obraz	k1gInSc4	obraz
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
nad	nad	k7c7	nad
Rhônou	Rhôna	k1gFnSc7	Rhôna
(	(	kIx(	(
<g/>
malba	malba	k1gFnSc1	malba
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
když	když	k8xS	když
maloval	malovat	k5eAaImAgMnS	malovat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
pod	pod	k7c7	pod
plynovou	plynový	k2eAgFnSc7d1	plynová
lampou	lampa	k1gFnSc7	lampa
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k3xOyFgMnSc3	svůj
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
Theovi	Thea	k1gMnSc3	Thea
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1889	[number]	k4	1889
se	se	k3xPyFc4	se
Van	van	k1gInSc1	van
Gogh	Gogha	k1gFnPc2	Gogha
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
uchýlit	uchýlit	k5eAaPmF	uchýlit
do	do	k7c2	do
sanatoria	sanatorium	k1gNnSc2	sanatorium
v	v	k7c6	v
provensálském	provensálský	k2eAgInSc6d1	provensálský
Saint-Rémy	Saint-Rém	k1gInPc4	Saint-Rém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
naplněný	naplněný	k2eAgInSc1d1	naplněný
prací	práce	k1gFnSc7	práce
byl	být	k5eAaImAgInS	být
plodný	plodný	k2eAgInSc1d1	plodný
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
opakovaným	opakovaný	k2eAgFnPc3d1	opakovaná
atakám	ataka	k1gFnPc3	ataka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenaly	znamenat	k5eAaImAgFnP	znamenat
neschopnost	neschopnost	k1gFnSc1	neschopnost
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
sanatoria	sanatorium	k1gNnSc2	sanatorium
jej	on	k3xPp3gMnSc4	on
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
obrazu	obraz	k1gInSc2	obraz
namalovaného	namalovaný	k2eAgInSc2d1	namalovaný
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozí	předchozí	k2eAgFnSc2d1	předchozí
práce	práce	k1gFnSc2	práce
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
noci	noc	k1gFnSc2	noc
nad	nad	k7c7	nad
Rhônou	Rhôna	k1gFnSc7	Rhôna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
noční	noční	k2eAgFnSc1d1	noční
scenérie	scenérie	k1gFnSc1	scenérie
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
za	za	k7c2	za
denního	denní	k2eAgNnSc2d1	denní
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nočních	noční	k2eAgFnPc2d1	noční
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
září	září	k1gNnSc2	září
1889	[number]	k4	1889
<g/>
,	,	kIx,	,
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
krizi	krize	k1gFnSc6	krize
trvající	trvající	k2eAgFnSc6d1	trvající
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
do	do	k7c2	do
konce	konec	k1gInSc2	konec
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
noc	noc	k1gFnSc4	noc
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
do	do	k7c2	do
příští	příští	k2eAgFnSc2d1	příští
série	série	k1gFnSc2	série
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odešle	odešle	k6eAd1	odešle
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratru	bratr	k1gMnSc3	bratr
Theu	Thea	k1gFnSc4	Thea
van	van	k1gInSc1	van
Goghovi	Gogh	k1gMnSc3	Gogh
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
snížil	snížit	k5eAaPmAgInS	snížit
přepravní	přepravní	k2eAgInPc4d1	přepravní
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
vyřadil	vyřadit	k5eAaPmAgMnS	vyřadit
ze	z	k7c2	z
zásilky	zásilka	k1gFnSc2	zásilka
tři	tři	k4xCgNnPc1	tři
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
nakonec	nakonec	k6eAd1	nakonec
odeslal	odeslat	k5eAaPmAgMnS	odeslat
až	až	k9	až
v	v	k7c6	v
další	další	k2eAgFnSc6d1	další
várce	várka	k1gFnSc6	várka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Theo	Thea	k1gFnSc5	Thea
po	po	k7c4	po
doručení	doručení	k1gNnSc4	doručení
nákladu	náklad	k1gInSc2	náklad
okamžitě	okamžitě	k6eAd1	okamžitě
neodpovídal	odpovídat	k5eNaImAgMnS	odpovídat
<g/>
,	,	kIx,	,
dotázal	dotázat	k5eAaPmAgMnS	dotázat
se	se	k3xPyFc4	se
Vincent	Vincent	k1gMnSc1	Vincent
bratra	bratr	k1gMnSc2	bratr
na	na	k7c4	na
obrazy	obraz	k1gInPc4	obraz
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
až	až	k9	až
následně	následně	k6eAd1	následně
obdržel	obdržet	k5eAaPmAgInS	obdržet
jeho	on	k3xPp3gInSc4	on
komentář	komentář	k1gInSc4	komentář
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
poslední	poslední	k2eAgFnSc3d1	poslední
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Námět	námět	k1gInSc1	námět
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
pozici	pozice	k1gFnSc6	pozice
je	být	k5eAaImIp3nS	být
zachycena	zachycen	k2eAgFnSc1d1	zachycena
obec	obec	k1gFnSc1	obec
a	a	k8xC	a
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
vířící	vířící	k2eAgNnSc1d1	vířící
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Pohled	pohled	k1gInSc1	pohled
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
sanatoria	sanatorium	k1gNnSc2	sanatorium
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
severu	sever	k1gInSc3	sever
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pravé	pravý	k2eAgFnSc2d1	pravá
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
zasazen	zasazen	k2eAgInSc1d1	zasazen
masiv	masiv	k1gInSc1	masiv
Alpilles	Alpillesa	k1gFnPc2	Alpillesa
(	(	kIx(	(
<g/>
Alpiček	Alpička	k1gFnPc2	Alpička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
obrazu	obraz	k1gInSc6	obraz
existuje	existovat	k5eAaImIp3nS	existovat
drobný	drobný	k2eAgInSc1d1	drobný
nesoulad	nesoulad	k1gInSc1	nesoulad
mezi	mezi	k7c7	mezi
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
scenérií	scenérie	k1gFnSc7	scenérie
masivu	masiv	k1gInSc2	masiv
a	a	k8xC	a
navazujícími	navazující	k2eAgInPc7d1	navazující
kopci	kopec	k1gInPc7	kopec
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
pocházely	pocházet	k5eAaImAgInP	pocházet
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
části	část	k1gFnSc2	část
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
položené	položený	k2eAgFnPc1d1	položená
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
sanatoria	sanatorium	k1gNnSc2	sanatorium
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
části	část	k1gFnSc2	část
kompozice	kompozice	k1gFnSc2	kompozice
byl	být	k5eAaImAgInS	být
přidán	přidán	k2eAgInSc1d1	přidán
cypřišový	cypřišový	k2eAgInSc1d1	cypřišový
strom	strom	k1gInSc1	strom
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Gogh	Gogha	k1gFnPc2	Gogha
již	již	k6eAd1	již
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c4	v
Arles	Arles	k1gInSc4	Arles
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c6	na
obrazu	obraz	k1gInSc6	obraz
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
nad	nad	k7c7	nad
Rhônou	Rhôna	k1gFnSc7	Rhôna
pozici	pozice	k1gFnSc4	pozice
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Velké	velký	k2eAgFnSc2d1	velká
medvědice	medvědice	k1gFnSc2	medvědice
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Komentáře	komentář	k1gInPc1	komentář
==	==	k?	==
</s>
</p>
<p>
<s>
Britský	britský	k2eAgMnSc1d1	britský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Simon	Simon	k1gMnSc1	Simon
Singh	Singh	k1gMnSc1	Singh
<g/>
,	,	kIx,	,
píšící	píšící	k2eAgMnSc1d1	píšící
zejména	zejména	k9	zejména
o	o	k7c6	o
matematických	matematický	k2eAgNnPc6d1	matematické
tématech	téma	k1gNnPc6	téma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Big	Big	k1gMnSc1	Big
Bang	Bang	k1gMnSc1	Bang
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
třesk	třesk	k1gInSc1	třesk
<g/>
)	)	kIx)	)
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
pozoruhodné	pozoruhodný	k2eAgFnPc4d1	pozoruhodná
podobnosti	podobnost	k1gFnPc4	podobnost
ke	k	k7c3	k
skice	skica	k1gFnSc3	skica
Whirlpool	Whirlpool	k1gInSc1	Whirlpool
Galaxy	Galax	k1gInPc1	Galax
(	(	kIx(	(
<g/>
Vířící	vířící	k2eAgFnPc1d1	vířící
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
načrtnuté	načrtnutý	k2eAgFnSc2d1	načrtnutá
britským	britský	k2eAgMnSc7d1	britský
hvězdářem	hvězdář	k1gMnSc7	hvězdář
lordem	lord	k1gMnSc7	lord
Williamem	William	k1gInSc7	William
Parsonsem	Parsons	k1gMnSc7	Parsons
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
postavil	postavit	k5eAaPmAgMnS	postavit
několik	několik	k4yIc1	několik
dalekohledů	dalekohled	k1gInPc2	dalekohled
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yRgInSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
skicu	skica	k1gFnSc4	skica
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
čtyřicet	čtyřicet	k4xCc4	čtyřicet
čtyři	čtyři	k4xCgInPc4	čtyři
let	léto	k1gNnPc2	léto
před	před	k7c7	před
van	van	k1gInSc1	van
Goghem	Gogh	k1gInSc7	Gogh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kresba	kresba	k1gFnSc1	kresba
byla	být	k5eAaImAgFnS	být
podrobena	podrobit	k5eAaPmNgFnS	podrobit
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
astronomickou	astronomický	k2eAgFnSc7d1	astronomická
fotografií	fotografia	k1gFnSc7	fotografia
hvězdy	hvězda	k1gFnSc2	hvězda
nesoucí	nesoucí	k2eAgNnSc4d1	nesoucí
jméno	jméno	k1gNnSc4	jméno
V838	V838	k1gFnSc2	V838
Monocerotis	Monocerotis	k1gFnSc2	Monocerotis
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zachytil	zachytit	k5eAaPmAgInS	zachytit
Hubbleův	Hubbleův	k2eAgInSc1d1	Hubbleův
vesmírný	vesmírný	k2eAgInSc1d1	vesmírný
dalekohled	dalekohled	k1gInSc1	dalekohled
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Oblaka	oblaka	k1gNnPc1	oblaka
plynu	plyn	k1gInSc2	plyn
obkružující	obkružující	k2eAgFnSc4d1	obkružující
hvězdu	hvězda	k1gFnSc4	hvězda
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
vířícím	vířící	k2eAgInPc3d1	vířící
proudům	proud	k1gInPc3	proud
použitým	použitý	k2eAgInSc7d1	použitý
van	van	k1gInSc1	van
Goghem	Gogh	k1gInSc7	Gogh
na	na	k7c6	na
malbě	malba	k1gFnSc6	malba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc2	umění
Joachim	Joachim	k1gMnSc1	Joachim
Pissarro	Pissarro	k1gNnSc4	Pissarro
označil	označit	k5eAaPmAgMnS	označit
Hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
noc	noc	k1gFnSc4	noc
za	za	k7c4	za
příklad	příklad	k1gInSc4	příklad
autorovy	autorův	k2eAgFnSc2d1	autorova
fascinace	fascinace	k1gFnSc2	fascinace
obrazem	obraz	k1gInSc7	obraz
noční	noční	k2eAgFnSc2d1	noční
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gMnSc2	The
Starry	Starra	k1gMnSc2	Starra
Night	Night	k1gMnSc1	Night
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BOIME	BOIME	kA	BOIME
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Gogh	Gogha	k1gFnPc2	Gogha
<g/>
:	:	kIx,	:
Sternennacht	Sternennacht	k1gInSc4	Sternennacht
<g/>
:	:	kIx,	:
Kunstgeschichte	Kunstgeschicht	k1gMnSc5	Kunstgeschicht
im	im	k?	im
Detail	detail	k1gInSc1	detail
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Systhema	Systhema	k1gNnSc1	Systhema
Verlag	Verlaga	k1gFnPc2	Verlaga
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
634	[number]	k4	634
<g/>
-	-	kIx~	-
<g/>
23015	[number]	k4	23015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
OCLC	OCLC	kA	OCLC
80645605	[number]	k4	80645605
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
vydalo	vydat	k5eAaPmAgNnS	vydat
Voyager	Voyager	k1gInSc4	Voyager
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
NY	NY	kA	NY
<g/>
,	,	kIx,	,
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
<g/>
:	:	kIx,	:
Vincent	Vincent	k1gMnSc1	Vincent
Van	vana	k1gFnPc2	vana
Gogh	Gogh	k1gMnSc1	Gogh
<g/>
:	:	kIx,	:
Starry	Starra	k1gFnPc1	Starra
night	nightum	k1gNnPc2	nightum
<g/>
,	,	kIx,	,
a	a	k8xC	a
history	histor	k1gInPc1	histor
of	of	k?	of
matter	matter	k1gInSc1	matter
<g/>
,	,	kIx,	,
a	a	k8xC	a
matter	matter	k1gInSc4	matter
of	of	k?	of
history	histor	k1gInPc4	histor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
noc	noc	k1gFnSc1	noc
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Muzea	muzeum	k1gNnSc2	muzeum
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
</s>
</p>
