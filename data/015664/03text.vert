<s>
Šťastné	Šťastné	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
matematickém	matematický	k2eAgInSc6d1
pojmu	pojem	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Šťastná	šťastný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
–	–	k?
film	film	k1gInSc4
Nory	Nora	k1gFnSc2
Ephron	Ephron	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Šťastné	Šťastné	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
happy	happ	k1gInPc4
number	numbero	k1gNnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
definováno	definovat	k5eAaBmNgNnS
následujícím	následující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
:	:	kIx,
vezme	vzít	k5eAaPmIp3nS
se	se	k3xPyFc4
libovolné	libovolný	k2eAgFnSc2d1
kladné	kladný	k2eAgFnSc2d1
celé	celá	k1gFnSc2
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
nahradí	nahradit	k5eAaPmIp3nS
se	se	k3xPyFc4
součtem	součet	k1gInSc7
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
svých	svůj	k3xOyFgFnPc2
číslic	číslice	k1gFnPc2
a	a	k8xC
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
nedojde	dojít	k5eNaPmIp3nS
k	k	k7c3
číslu	číslo	k1gNnSc3
jedna	jeden	k4xCgFnSc1
(	(	kIx(
<g/>
kde	kde	k6eAd1
se	se	k3xPyFc4
proces	proces	k1gInSc1
zastaví	zastavit	k5eAaPmIp3nS
<g/>
)	)	kIx)
nebo	nebo	k8xC
dokud	dokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
posloupnosti	posloupnost	k1gFnSc6
neobjeví	objevit	k5eNaPmIp3nS
některé	některý	k3yIgNnSc1
číslo	číslo	k1gNnSc1
dvakrát	dvakrát	k6eAd1
(	(	kIx(
<g/>
posloupnost	posloupnost	k1gFnSc1
se	se	k3xPyFc4
zacyklí	zacyklit	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
skončí	skončit	k5eAaPmIp3nS
jedničkou	jednička	k1gFnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
šťastná	šťastný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnSc1d1
pak	pak	k6eAd1
nešťastná	šťastný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Formálněji	formálně	k6eAd2
řečeno	říct	k5eAaPmNgNnS
<g/>
:	:	kIx,
mějme	mít	k5eAaImRp1nP
číslo	číslo	k1gNnSc4
</s>
<s>
n	n	k0
</s>
<s>
=	=	kIx~
</s>
<s>
n	n	k0
</s>
<s>
0	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
=	=	kIx~
<g/>
n_	n_	k?
<g/>
{	{	kIx(
<g/>
0	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
a	a	k8xC
definujme	definovat	k5eAaBmRp1nP
posloupnost	posloupnost	k1gFnSc4
</s>
<s>
n	n	k0
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
</s>
<s>
n	n	k0
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
...	...	k?
kde	kde	k6eAd1
</s>
<s>
n	n	k0
</s>
<s>
i	i	k9
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
součet	součet	k1gInSc1
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
čísel	číslo	k1gNnPc2
vyjádřených	vyjádřený	k2eAgNnPc2d1
číslicemi	číslice	k1gFnPc7
čísla	číslo	k1gNnPc4
</s>
<s>
n	n	k0
</s>
<s>
i	i	k9
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}}	}}	k?
</s>
<s>
Poté	poté	k6eAd1
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
šťastné	šťastný	k2eAgNnSc1d1
právě	právě	k9
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
když	když	k8xS
existuje	existovat	k5eAaImIp3nS
i	i	k9
takové	takový	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
</s>
<s>
n	n	k0
</s>
<s>
i	i	k9
</s>
<s>
=	=	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n_	n_	k?
<g/>
{	{	kIx(
<g/>
i	i	k9
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
nějaké	nějaký	k3yIgNnSc4
číslo	číslo	k1gNnSc4
šťastné	šťastný	k2eAgNnSc4d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
také	také	k9
všechny	všechen	k3xTgMnPc4
členy	člen	k1gMnPc4
jemu	on	k3xPp3gInSc3
příslušné	příslušný	k2eAgFnPc4d1
posloupnosti	posloupnost	k1gFnPc4
jsou	být	k5eAaImIp3nP
také	také	k9
šťastnými	šťastný	k2eAgMnPc7d1
čísly	číslo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Příklad	příklad	k1gInSc1
</s>
<s>
Například	například	k6eAd1
7	#num#	k4
je	být	k5eAaImIp3nS
šťastné	šťastný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
a	a	k8xC
přísluší	příslušet	k5eAaImIp3nS
mu	on	k3xPp3gMnSc3
tato	tento	k3xDgFnSc1
posloupnost	posloupnost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
72	#num#	k4
=	=	kIx~
49	#num#	k4
</s>
<s>
42	#num#	k4
+	+	kIx~
92	#num#	k4
=	=	kIx~
97	#num#	k4
</s>
<s>
92	#num#	k4
+	+	kIx~
72	#num#	k4
=	=	kIx~
130	#num#	k4
</s>
<s>
12	#num#	k4
+	+	kIx~
32	#num#	k4
+	+	kIx~
02	#num#	k4
=	=	kIx~
10	#num#	k4
</s>
<s>
12	#num#	k4
+	+	kIx~
02	#num#	k4
=	=	kIx~
1	#num#	k4
</s>
<s>
číslo	číslo	k1gNnSc1
1663	#num#	k4
je	být	k5eAaImIp3nS
také	také	k9
šťastné	šťastný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
12	#num#	k4
+	+	kIx~
62	#num#	k4
+	+	kIx~
62	#num#	k4
+	+	kIx~
32	#num#	k4
=	=	kIx~
82	#num#	k4
</s>
<s>
82	#num#	k4
+	+	kIx~
22	#num#	k4
=	=	kIx~
68	#num#	k4
</s>
<s>
62	#num#	k4
+	+	kIx~
82	#num#	k4
=	=	kIx~
100	#num#	k4
</s>
<s>
12	#num#	k4
+	+	kIx~
02	#num#	k4
+	+	kIx~
02	#num#	k4
=	=	kIx~
1	#num#	k4
</s>
<s>
i	i	k8xC
číslo	číslo	k1gNnSc1
13	#num#	k4
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
pokládané	pokládaný	k2eAgInPc1d1
za	za	k7c4
nešťastné	šťastný	k2eNgInPc4d1
(	(	kIx(
<g/>
například	například	k6eAd1
triskaidekafobiky	triskaidekafobika	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dle	dle	k7c2
této	tento	k3xDgFnSc2
definice	definice	k1gFnSc2
šťastné	šťastný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
12	#num#	k4
+	+	kIx~
32	#num#	k4
=	=	kIx~
10	#num#	k4
</s>
<s>
12	#num#	k4
+	+	kIx~
02	#num#	k4
=	=	kIx~
1	#num#	k4
</s>
<s>
Chování	chování	k1gNnSc1
posloupnosti	posloupnost	k1gFnSc2
</s>
<s>
Když	když	k8xS
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
není	být	k5eNaImIp3nS
šťastné	šťastný	k2eAgNnSc1d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gFnSc1
posloupnost	posloupnost	k1gFnSc1
nedostane	dostat	k5eNaPmIp3nS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
zacyklí	zacyklit	k5eAaPmIp3nS
(	(	kIx(
<g/>
například	například	k6eAd1
pro	pro	k7c4
číslo	číslo	k1gNnSc4
4	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
4	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
,	,	kIx,
37	#num#	k4
<g/>
,	,	kIx,
58	#num#	k4
<g/>
,	,	kIx,
89	#num#	k4
<g/>
,	,	kIx,
145	#num#	k4
<g/>
,	,	kIx,
42	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
4	#num#	k4
<g/>
,	,	kIx,
...	...	k?
</s>
<s>
Pokud	pokud	k8xS
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
má	mít	k5eAaImIp3nS
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
číslic	číslice	k1gFnPc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
součet	součet	k1gInSc1
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
jimi	on	k3xPp3gMnPc7
vyjádřených	vyjádřený	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
nejvýše	vysoce	k6eAd3,k6eAd1
</s>
<s>
81	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
81	#num#	k4
<g/>
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
(	(	kIx(
<g/>
to	ten	k3xDgNnSc1
nastane	nastat	k5eAaPmIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
číslice	číslice	k1gFnPc4
devítky	devítka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
</s>
<s>
m	m	kA
</s>
<s>
=	=	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
m	m	kA
<g/>
=	=	kIx~
<g/>
4	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
více	hodně	k6eAd2
je	být	k5eAaImIp3nS
</s>
<s>
n	n	k0
</s>
<s>
≥	≥	k?
</s>
<s>
10	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
>	>	kIx)
</s>
<s>
81	#num#	k4
</s>
<s>
m	m	kA
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
\	\	kIx~
<g/>
geq	geq	k?
10	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
m-	m-	k?
<g/>
1	#num#	k4
<g/>
}	}	kIx)
<g/>
>	>	kIx)
<g/>
81	#num#	k4
<g/>
m	m	kA
<g/>
}	}	kIx)
</s>
<s>
tedy	tedy	k9
každé	každý	k3xTgNnSc1
číslo	číslo	k1gNnSc1
větší	veliký	k2eAgFnSc2d2
než	než	k8xS
1000	#num#	k4
se	se	k3xPyFc4
definovaným	definovaný	k2eAgInSc7d1
postupem	postup	k1gInSc7
zmenšuje	zmenšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
číslech	číslo	k1gNnPc6
menších	malý	k2eAgFnPc2d2
než	než	k8xS
1000	#num#	k4
je	být	k5eAaImIp3nS
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
součet	součet	k1gInSc1
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
jeho	jeho	k3xOp3gFnPc2
cifer	cifra	k1gFnPc2
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
<g/>
,	,	kIx,
999	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
dá	dát	k5eAaPmIp3nS
výsledek	výsledek	k1gInSc4
3	#num#	k4
krát	krát	k6eAd1
81	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
243	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rozmezí	rozmezí	k1gNnSc6
100	#num#	k4
až	až	k9
243	#num#	k4
největší	veliký	k2eAgFnSc4d3
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
163	#num#	k4
<g/>
,	,	kIx,
dává	dávat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
199	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rozmezí	rozmezí	k1gNnSc6
100	#num#	k4
až	až	k9
163	#num#	k4
největší	veliký	k2eAgFnSc4d3
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
107	#num#	k4
<g/>
,	,	kIx,
dává	dávat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
159	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rozmezí	rozmezí	k1gNnSc6
100	#num#	k4
až	až	k9
107	#num#	k4
největší	veliký	k2eAgFnSc4d3
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
50	#num#	k4
<g/>
,	,	kIx,
dává	dávat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
107	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
čísel	číslo	k1gNnPc2
v	v	k7c6
intervalech	interval	k1gInPc6
[	[	kIx(
<g/>
244,999	244,999	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
[	[	kIx(
<g/>
164,243	164,243	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
[	[	kIx(
<g/>
108,163	108,163	k4
<g/>
]	]	kIx)
a	a	k8xC
[	[	kIx(
<g/>
100,107	100,107	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
každé	každý	k3xTgNnSc1
číslo	číslo	k1gNnSc1
větší	veliký	k2eAgFnSc2d2
než	než	k8xS
99	#num#	k4
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
procesem	proces	k1gInSc7
rychle	rychle	k6eAd1
zmenšuje	zmenšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tedy	tedy	k8xC
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
s	s	k7c7
kterým	který	k3yQgNnSc7,k3yRgNnSc7,k3yIgNnSc7
číslem	číslo	k1gNnSc7
se	se	k3xPyFc4
začne	začít	k5eAaPmIp3nS
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
vznikne	vzniknout	k5eAaPmIp3nS
číslo	číslo	k1gNnSc4
menší	malý	k2eAgInPc4d2
než	než	k8xS
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgNnSc1
číslo	číslo	k1gNnSc1
z	z	k7c2
intervalu	interval	k1gInSc2
[	[	kIx(
<g/>
1,99	1,99	k4
<g/>
]	]	kIx)
je	být	k5eAaImIp3nS
buď	buď	k8xC
šťastné	šťastný	k2eAgNnSc1d1
nebo	nebo	k8xC
se	se	k3xPyFc4
zacyklí	zacyklit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Šťastná	šťastný	k2eAgNnPc1d1
prvočísla	prvočíslo	k1gNnPc1
</s>
<s>
Šťastné	Šťastné	k2eAgNnSc1d1
prvočíslo	prvočíslo	k1gNnSc1
je	být	k5eAaImIp3nS
takové	takový	k3xDgNnSc4
šťastné	šťastný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
prvočíslem	prvočíslo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šťastná	Šťastná	k1gFnSc1
prvočísla	prvočíslo	k1gNnSc2
menší	malý	k2eAgFnSc2d2
než	než	k8xS
500	#num#	k4
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
7	#num#	k4
<g/>
,	,	kIx,
13	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
,	,	kIx,
79	#num#	k4
<g/>
,	,	kIx,
97	#num#	k4
<g/>
,	,	kIx,
103	#num#	k4
<g/>
,	,	kIx,
109	#num#	k4
<g/>
,	,	kIx,
139	#num#	k4
<g/>
,	,	kIx,
167	#num#	k4
<g/>
,	,	kIx,
193	#num#	k4
<g/>
,	,	kIx,
239	#num#	k4
<g/>
,	,	kIx,
263	#num#	k4
<g/>
,	,	kIx,
293	#num#	k4
<g/>
,	,	kIx,
313	#num#	k4
<g/>
,	,	kIx,
331	#num#	k4
<g/>
,	,	kIx,
367	#num#	k4
<g/>
,	,	kIx,
379	#num#	k4
<g/>
,	,	kIx,
383	#num#	k4
<g/>
,	,	kIx,
397	#num#	k4
<g/>
,	,	kIx,
409	#num#	k4
<g/>
,	,	kIx,
487	#num#	k4
(	(	kIx(
<g/>
Sekvence	sekvence	k1gFnSc1
A035497	A035497	k1gFnSc2
v	v	k7c6
OEIS	OEIS	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Všechna	všechen	k3xTgNnPc1
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
i	i	k9
všechna	všechen	k3xTgNnPc1
prvočísla	prvočíslo	k1gNnPc1
tvaru	tvar	k1gInSc2
</s>
<s>
10	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
3	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
10	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
a	a	k8xC
</s>
<s>
10	#num#	k4
</s>
<s>
n	n	k0
</s>
<s>
+	+	kIx~
</s>
<s>
9	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
10	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
n	n	k0
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
9	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
pro	pro	k7c4
n	n	k0
větší	veliký	k2eAgFnSc4d2
než	než	k8xS
0	#num#	k4
jsou	být	k5eAaImIp3nP
šťastná	šťastný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
tak	tak	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
<g/>
:	:	kIx,
</s>
<s>
Všechna	všechen	k3xTgNnPc1
tato	tento	k3xDgNnPc1
čísla	číslo	k1gNnPc1
mají	mít	k5eAaImIp3nP
nejméně	málo	k6eAd3
2	#num#	k4
číslice	číslice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc7
číslicí	číslice	k1gFnSc7
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1
číslicí	číslice	k1gFnSc7
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
3	#num#	k4
nebo	nebo	k8xC
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Všechny	všechen	k3xTgFnPc1
další	další	k2eAgFnPc1d1
číslice	číslice	k1gFnPc1
jsou	být	k5eAaImIp3nP
0	#num#	k4
(	(	kIx(
<g/>
jejich	jejich	k3xOp3gFnSc1
druhá	druhý	k4xOgFnSc1
mocnina	mocnina	k1gFnSc1
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
0	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
součet	součet	k1gInSc1
nijak	nijak	k6eAd1
neovlivní	ovlivnit	k5eNaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1
při	při	k7c6
přidání	přidání	k1gNnSc6
3	#num#	k4
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
12	#num#	k4
+	+	kIx~
32	#num#	k4
=	=	kIx~
10	#num#	k4
→	→	k?
12	#num#	k4
=	=	kIx~
1	#num#	k4
</s>
<s>
Posloupnost	posloupnost	k1gFnSc1
při	při	k7c6
přidání	přidání	k1gNnSc6
9	#num#	k4
je	být	k5eAaImIp3nS
<g/>
:	:	kIx,
12	#num#	k4
+	+	kIx~
92	#num#	k4
=	=	kIx~
82	#num#	k4
→	→	k?
64	#num#	k4
+	+	kIx~
4	#num#	k4
=	=	kIx~
68	#num#	k4
→	→	k?
100	#num#	k4
→	→	k?
1	#num#	k4
</s>
<s>
Palindromické	Palindromický	k2eAgNnSc1d1
prvočíslo	prvočíslo	k1gNnSc1
10150006	#num#	k4
+	+	kIx~
7426247	#num#	k4
<g/>
×	×	k?
<g/>
1075000	#num#	k4
+	+	kIx~
1	#num#	k4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
má	mít	k5eAaImIp3nS
150007	#num#	k4
číslic	číslice	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
šťastné	šťastný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
mnoho	mnoho	k4c1
nul	nula	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
součet	součet	k1gInSc1
neovlivňují	ovlivňovat	k5eNaImIp3nP
a	a	k8xC
zbylá	zbylý	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
dávají	dávat	k5eAaImIp3nP
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
2	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
7	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+	+	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
176	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
1	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
+	+	kIx~
<g/>
6	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
4	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
7	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
176	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
šťastné	šťastný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
prvočíslo	prvočíslo	k1gNnSc1
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
Paulem	Paul	k1gMnSc7
Joblingem	Jobling	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Šťastná	šťastný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
v	v	k7c6
jiné	jiný	k2eAgFnSc6d1
než	než	k8xS
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
</s>
<s>
Definice	definice	k1gFnSc1
šťastných	šťastný	k2eAgNnPc2d1
čísel	číslo	k1gNnPc2
je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
desítkové	desítkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuuto	Tuuto	k1gNnSc1
definici	definice	k1gFnSc4
lze	lze	k6eAd1
rozšířit	rozšířit	k5eAaPmF
na	na	k7c4
ostatní	ostatní	k1gNnSc4
číselné	číselný	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vyznačení	vyznačení	k1gNnSc3
čísel	číslo	k1gNnPc2
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
soustavách	soustava	k1gFnPc6
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
používat	používat	k5eAaImF
číslo	číslo	k1gNnSc4
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
dolním	dolní	k2eAgInSc6d1
indexu	index	k1gInSc6
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
představuje	představovat	k5eAaImIp3nS
zvolenou	zvolený	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
</s>
<s>
100	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
100	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
představuje	představovat	k5eAaImIp3nS
číslo	číslo	k1gNnSc1
4	#num#	k4
ve	v	k7c6
dvojkové	dvojkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
každé	každý	k3xTgFnSc6
číselné	číselný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
existují	existovat	k5eAaImIp3nP
šťastná	šťastný	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
čísla	číslo	k1gNnPc4
</s>
<s>
1	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
10	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
100	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
1000	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
,	,	kIx,
</s>
<s>
</s>
<s>
</s>
<s>
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
1	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
,10	,10	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
,100	,100	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
,1000	,1000	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}	}	kIx)
<g/>
,	,	kIx,
<g/>
...	...	k?
<g/>
}	}	kIx)
</s>
<s>
jsou	být	k5eAaImIp3nP
šťastná	šťastný	k2eAgFnSc1d1
pro	pro	k7c4
jakoukoliv	jakýkoliv	k3yIgFnSc4
číselnou	číselný	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
</s>
<s>
Ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
důvodu	důvod	k1gInSc2
jako	jako	k8xC,k8xS
výše	vysoce	k6eAd2
se	se	k3xPyFc4
lz	lz	k?
přesvědčit	přesvědčit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
každé	každý	k3xTgNnSc4
nešťastné	šťastný	k2eNgNnSc4d1
číslo	číslo	k1gNnSc4
v	v	k7c6
číselné	číselný	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
vede	vést	k5eAaImIp3nS
k	k	k7c3
zacyklení	zacyklení	k1gNnSc3
v	v	k7c6
číslech	číslo	k1gNnPc6
menších	malý	k2eAgFnPc2d2
než	než	k8xS
</s>
<s>
1000	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
1000	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}}	}}	k?
</s>
<s>
Využije	využít	k5eAaPmIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
</s>
<s>
n	n	k0
</s>
<s>
<	<	kIx(
</s>
<s>
1000	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
<	<	kIx(
<g/>
1000	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
pak	pak	k6eAd1
součet	součet	k1gInSc1
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
čísel	číslo	k1gNnPc2
vyjádřených	vyjádřený	k2eAgNnPc2d1
číslicemi	číslice	k1gFnPc7
čísla	číslo	k1gNnPc4
</s>
<s>
n	n	k0
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
n	n	k0
<g/>
}	}	kIx)
</s>
<s>
v	v	k7c6
soustavě	soustava	k1gFnSc6
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
}	}	kIx)
</s>
<s>
je	být	k5eAaImIp3nS
menší	malý	k2eAgInSc1d2
nebo	nebo	k8xC
roven	roven	k2eAgInSc1d1
</s>
<s>
3	#num#	k4
</s>
<s>
(	(	kIx(
</s>
<s>
b	b	k?
</s>
<s>
−	−	k?
</s>
<s>
1	#num#	k4
</s>
<s>
)	)	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
3	#num#	k4
<g/>
(	(	kIx(
<g/>
b-	b-	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}}	}}	k?
</s>
<s>
</s>
<s>
Lze	lze	k6eAd1
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
výraz	výraz	k1gInSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
menší	malý	k2eAgFnSc1d2
než	než	k8xS
</s>
<s>
b	b	k?
</s>
<s>
3	#num#	k4
</s>
<s>
=	=	kIx~
</s>
<s>
1000	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
b	b	k?
<g/>
^	^	kIx~
<g/>
{	{	kIx(
<g/>
3	#num#	k4
<g/>
}	}	kIx)
<g/>
=	=	kIx~
<g/>
1000	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}}	}}	k?
</s>
<s>
Z	z	k7c2
toho	ten	k3xDgNnSc2
lze	lze	k6eAd1
usoudit	usoudit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
jakmile	jakmile	k8xS
se	se	k3xPyFc4
posloupnost	posloupnost	k1gFnSc1
dostane	dostat	k5eAaPmIp3nS
do	do	k7c2
čísla	číslo	k1gNnSc2
menšího	malý	k2eAgInSc2d2
než	než	k8xS
</s>
<s>
1000	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
1000	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}}	}}	k?
</s>
<s>
,	,	kIx,
zůstane	zůstat	k5eAaPmIp3nS
v	v	k7c6
tomto	tento	k3xDgNnSc6
rozmezí	rozmezí	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
musí	muset	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
zacyklit	zacyklit	k5eAaPmF
(	(	kIx(
<g/>
neboť	neboť	k8xC
čísel	číslo	k1gNnPc2
menších	malý	k2eAgMnPc2d2
než	než	k8xS
</s>
<s>
1000	#num#	k4
</s>
<s>
b	b	k?
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
1000	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
b	b	k?
<g/>
}}	}}	k?
</s>
<s>
je	být	k5eAaImIp3nS
jen	jen	k9
konečně	konečně	k6eAd1
mnoho	mnoho	k6eAd1
<g/>
)	)	kIx)
či	či	k8xC
se	se	k3xPyFc4
dostat	dostat	k5eAaPmF
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
dvojkové	dvojkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
jsou	být	k5eAaImIp3nP
všechna	všechen	k3xTgNnPc1
čísla	číslo	k1gNnPc1
šťastná	šťastný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechna	všechen	k3xTgNnPc1
čísla	číslo	k1gNnPc1
ve	v	k7c6
dvojkové	dvojkový	k2eAgFnSc6d1
soustavě	soustava	k1gFnSc6
menší	malý	k2eAgFnSc4d2
než	než	k8xS
10002	#num#	k4
jsou	být	k5eAaImIp3nP
totiž	totiž	k9
šťastná	šťastný	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
111	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
11	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
111	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
11	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
10	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
110	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
110	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
10	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
101	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
1	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
101	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
10	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
1	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
100	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
→	→	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
100	#num#	k4
<g/>
_	_	kIx~
<g/>
{	{	kIx(
<g/>
2	#num#	k4
<g/>
}	}	kIx)
<g/>
\	\	kIx~
<g/>
rightarrow	rightarrow	k?
1	#num#	k4
<g/>
.	.	kIx.
<g/>
}	}	kIx)
</s>
<s>
Dvojková	dvojkový	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
je	být	k5eAaImIp3nS
tedy	tedy	k9
šťastná	šťastný	k2eAgFnSc1d1
číselná	číselný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnSc1d1
takovou	takový	k3xDgFnSc7
soustavou	soustava	k1gFnSc7
je	být	k5eAaImIp3nS
soustava	soustava	k1gFnSc1
čtyřková	čtyřková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Happy	Happa	k1gFnSc2
number	numbra	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Šťastné	Šťastné	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
v	v	k7c6
encyklopedii	encyklopedie	k1gFnSc6
MathWorld	MathWorld	k1gMnSc1
Happy	Happa	k1gFnSc2
Number	Number	k1gMnSc1
--	--	k?
from	from	k1gInSc1
Wolfram	wolfram	k1gInSc1
MathWorld	MathWorld	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
OEIS	OEIS	kA
<g/>
:	:	kIx,
A007770	A007770	k1gMnSc1
–	–	k?
Happy	Happa	k1gFnSc2
numbers	numbersa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
