<s>
Šťastné	Šťastné	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
happy	happ	k1gInPc4
number	numbero	k1gNnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
matematice	matematika	k1gFnSc6
definováno	definovat	k5eAaBmNgNnS
následujícím	následující	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
:	:	kIx,
vezme	vzít	k5eAaPmIp3nS
se	se	k3xPyFc4
libovolné	libovolný	k2eAgFnSc2d1
kladné	kladný	k2eAgFnSc2d1
celé	celá	k1gFnSc2
číslo	číslo	k1gNnSc1
<g/>
,	,	kIx,
nahradí	nahradit	k5eAaPmIp3nS
se	se	k3xPyFc4
součtem	součet	k1gInSc7
druhých	druhý	k4xOgFnPc2
mocnin	mocnina	k1gFnPc2
svých	svůj	k3xOyFgFnPc2
číslic	číslice	k1gFnPc2
a	a	k8xC
tento	tento	k3xDgInSc1
proces	proces	k1gInSc1
se	se	k3xPyFc4
opakuje	opakovat	k5eAaImIp3nS
<g/>
,	,	kIx,
dokud	dokud	k8xS
se	se	k3xPyFc4
nedojde	dojít	k5eNaPmIp3nS
k	k	k7c3
číslu	číslo	k1gNnSc3
jedna	jeden	k4xCgFnSc1
(	(	kIx(
<g/>
kde	kde	k6eAd1
se	se	k3xPyFc4
proces	proces	k1gInSc1
zastaví	zastavit	k5eAaPmIp3nS
<g/>
)	)	kIx)
nebo	nebo	k8xC
dokud	dokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
posloupnosti	posloupnost	k1gFnSc6
neobjeví	objevit	k5eNaPmIp3nS
některé	některý	k3yIgNnSc1
číslo	číslo	k1gNnSc1
dvakrát	dvakrát	k6eAd1
(	(	kIx(
<g/>
posloupnost	posloupnost	k1gFnSc1
se	se	k3xPyFc4
zacyklí	zacyklit	k5eAaPmIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>