<s>
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
postavení	postavení	k1gNnSc1	postavení
je	být	k5eAaImIp3nS	být
upraveno	upravit	k5eAaPmNgNnS	upravit
v	v	k7c6	v
Ústavě	ústava	k1gFnSc6	ústava
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
přijaté	přijatý	k2eAgFnSc2d1	přijatá
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Senátu	senát	k1gInSc6	senát
zasedá	zasedat	k5eAaImIp3nS	zasedat
81	[number]	k4	81
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
šesti	šest	k4xCc2	šest
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
je	být	k5eAaImIp3nS	být
nerozpustitelný	rozpustitelný	k2eNgInSc1d1	nerozpustitelný
<g/>
,	,	kIx,	,
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
volí	volit	k5eAaImIp3nS	volit
třetina	třetina	k1gFnSc1	třetina
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
Senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
59	[number]	k4	59
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
areál	areál	k1gInSc1	areál
Valdštejnského	valdštejnský	k2eAgInSc2d1	valdštejnský
paláce	palác	k1gInSc2	palác
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
jízdárnou	jízdárna	k1gFnSc7	jízdárna
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Kolovratským	Kolovratský	k2eAgInSc7d1	Kolovratský
palácem	palác	k1gInSc7	palác
a	a	k8xC	a
Malým	malý	k2eAgInSc7d1	malý
Fürstenberským	Fürstenberský	k2eAgInSc7d1	Fürstenberský
palácem	palác	k1gInSc7	palác
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc7d1	horní
komorou	komora	k1gFnSc7	komora
zákonodárného	zákonodárný	k2eAgInSc2d1	zákonodárný
sboru	sbor	k1gInSc2	sbor
byla	být	k5eAaImAgFnS	být
Panská	panský	k2eAgFnSc1d1	Panská
sněmovna	sněmovna	k1gFnSc1	sněmovna
rakousko-uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
Říšské	říšský	k2eAgFnSc2d1	říšská
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dědičnými	dědičný	k2eAgInPc7d1	dědičný
členy	člen	k1gInPc7	člen
(	(	kIx(	(
<g/>
např.	např.	kA	např.
církevními	církevní	k2eAgMnPc7d1	církevní
hodnostáři	hodnostář	k1gMnPc7	hodnostář
<g/>
)	)	kIx)	)
a	a	k8xC	a
členy	člen	k1gInPc7	člen
jmenovanými	jmenovaný	k2eAgInPc7d1	jmenovaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
byl	být	k5eAaImAgInS	být
reformou	reforma	k1gFnSc7	reforma
omezen	omezit	k5eAaPmNgInS	omezit
počet	počet	k1gInSc1	počet
doživotně	doživotně	k6eAd1	doživotně
jmenovaných	jmenovaný	k2eAgInPc2d1	jmenovaný
členů	člen	k1gInPc2	člen
a	a	k8xC	a
stanoven	stanovit	k5eAaPmNgInS	stanovit
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
mezi	mezi	k7c7	mezi
150	[number]	k4	150
a	a	k8xC	a
170	[number]	k4	170
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dědičných	dědičný	k2eAgInPc2d1	dědičný
českých	český	k2eAgInPc2d1	český
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
Panské	panský	k2eAgFnSc2d1	Panská
sněmovny	sněmovna	k1gFnSc2	sněmovna
jmenována	jmenován	k2eAgFnSc1d1	jmenována
řada	řada	k1gFnSc1	řada
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zasloužily	zasloužit	k5eAaPmAgFnP	zasloužit
o	o	k7c4	o
společnost	společnost	k1gFnSc4	společnost
či	či	k8xC	či
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Čech	Čech	k1gMnSc1	Čech
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
.	.	kIx.	.
</s>
<s>
Prvorepublikovou	prvorepublikový	k2eAgFnSc7d1	prvorepubliková
horní	horní	k2eAgFnSc7d1	horní
komorou	komora	k1gFnSc7	komora
byl	být	k5eAaImAgInS	být
Senát	senát	k1gInSc1	senát
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgFnS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
přijata	přijat	k2eAgFnSc1d1	přijata
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jak	jak	k6eAd1	jak
v	v	k7c6	v
rakousko-uherské	rakouskoherský	k2eAgFnSc6d1	rakousko-uherská
tradici	tradice	k1gFnSc6	tradice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
USA	USA	kA	USA
předjímala	předjímat	k5eAaImAgFnS	předjímat
dvoukomorový	dvoukomorový	k2eAgInSc4d1	dvoukomorový
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Skládal	skládat	k5eAaImAgInS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
s	s	k7c7	s
třemi	tři	k4xCgNnPc7	tři
sty	sto	k4xCgNnPc7	sto
členy	člen	k1gMnPc7	člen
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
sto	sto	k4xCgNnSc4	sto
padesáti	padesát	k4xCc2	padesát
členy	člen	k1gInPc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
volilo	volit	k5eAaImAgNnS	volit
poměrným	poměrný	k2eAgInSc7d1	poměrný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
na	na	k7c4	na
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
na	na	k7c4	na
8	[number]	k4	8
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
Sněmovny	sněmovna	k1gFnSc2	sněmovna
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaPmF	stát
občan	občan	k1gMnSc1	občan
starší	starší	k1gMnSc1	starší
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
věku	věk	k1gInSc2	věk
45	[number]	k4	45
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
nutné	nutný	k2eAgFnSc6d1	nutná
délce	délka	k1gFnSc6	délka
státního	státní	k2eAgNnSc2d1	státní
občanství	občanství	k1gNnSc2	občanství
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
předčasným	předčasný	k2eAgInPc3d1	předčasný
termínům	termín	k1gInPc3	termín
následující	následující	k2eAgFnSc2d1	následující
volby	volba	k1gFnSc2	volba
probíhaly	probíhat	k5eAaImAgFnP	probíhat
současně	současně	k6eAd1	současně
s	s	k7c7	s
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
shodným	shodný	k2eAgInSc7d1	shodný
poměrným	poměrný	k2eAgInSc7d1	poměrný
systémem	systém	k1gInSc7	systém
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
<g/>
,	,	kIx,	,
že	že	k8xS	že
stranické	stranický	k2eAgNnSc1d1	stranické
složení	složení	k1gNnSc1	složení
Senátu	senát	k1gInSc2	senát
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
kopírovalo	kopírovat	k5eAaImAgNnS	kopírovat
složení	složení	k1gNnSc1	složení
dolní	dolní	k2eAgFnSc2d1	dolní
komory	komora	k1gFnSc2	komora
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
proklamovaná	proklamovaný	k2eAgFnSc1d1	proklamovaná
funkce	funkce	k1gFnSc1	funkce
"	"	kIx"	"
<g/>
pojistky	pojistka	k1gFnPc1	pojistka
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
iluzorní	iluzorní	k2eAgFnSc1d1	iluzorní
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
komory	komora	k1gFnPc1	komora
byly	být	k5eAaImAgFnP	být
prezidentem	prezident	k1gMnSc7	prezident
definitivně	definitivně	k6eAd1	definitivně
rozpuštěny	rozpuštěn	k2eAgMnPc4d1	rozpuštěn
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
okupaci	okupace	k1gFnSc6	okupace
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
republice	republika	k1gFnSc6	republika
nebyla	být	k5eNaImAgFnS	být
instituce	instituce	k1gFnSc1	instituce
senátu	senát	k1gInSc2	senát
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
určitý	určitý	k2eAgInSc4d1	určitý
charakter	charakter	k1gInSc4	charakter
druhé	druhý	k4xOgFnSc2	druhý
komory	komora	k1gFnSc2	komora
měla	mít	k5eAaImAgFnS	mít
slovenská	slovenský	k2eAgFnSc1d1	slovenská
frakce	frakce	k1gFnSc1	frakce
poslanců	poslanec	k1gMnPc2	poslanec
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
však	však	k9	však
trval	trvat	k5eAaImAgInS	trvat
jen	jen	k9	jen
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Květnová	květnový	k2eAgFnSc1d1	květnová
ústava	ústava	k1gFnSc1	ústava
zavedla	zavést	k5eAaPmAgFnS	zavést
striktně	striktně	k6eAd1	striktně
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
zachována	zachován	k2eAgFnSc1d1	zachována
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
usnesení	usnesení	k1gNnSc3	usnesení
zůstala	zůstat	k5eAaPmAgFnS	zůstat
omezená	omezený	k2eAgFnSc1d1	omezená
teritoriálně	teritoriálně	k6eAd1	teritoriálně
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
)	)	kIx)	)
i	i	k9	i
tematicky	tematicky	k6eAd1	tematicky
<g/>
,	,	kIx,	,
a	a	k8xC	a
nově	nově	k6eAd1	nově
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
i	i	k8xC	i
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
federativním	federativní	k2eAgNnSc6d1	federativní
Československu	Československo	k1gNnSc6	Československo
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
existovalo	existovat	k5eAaImAgNnS	existovat
dvoukomorové	dvoukomorový	k2eAgNnSc1d1	dvoukomorové
Federální	federální	k2eAgNnSc1d1	federální
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
143	[number]	k4	143
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
FS	FS	kA	FS
skládalo	skládat	k5eAaImAgNnS	skládat
ze	z	k7c2	z
Sněmovny	sněmovna	k1gFnSc2	sněmovna
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Povahu	povaha	k1gFnSc4	povaha
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
na	na	k7c6	na
regionálním	regionální	k2eAgInSc6d1	regionální
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
Sněmovna	sněmovna	k1gFnSc1	sněmovna
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
150	[number]	k4	150
poslanců	poslanec	k1gMnPc2	poslanec
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
bylo	být	k5eAaImAgNnS	být
75	[number]	k4	75
voleno	volit	k5eAaImNgNnS	volit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
75	[number]	k4	75
ve	v	k7c6	v
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
socialistické	socialistický	k2eAgFnSc6d1	socialistická
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
Sněmovna	sněmovna	k1gFnSc1	sněmovna
národů	národ	k1gInPc2	národ
volená	volený	k2eAgFnSc1d1	volená
řádnými	řádný	k2eAgFnPc7d1	řádná
volbami	volba	k1gFnPc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
Poslance	poslanec	k1gMnSc4	poslanec
za	za	k7c2	za
SSR	SSR	kA	SSR
zvolila	zvolit	k5eAaPmAgFnS	zvolit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
poslance	poslanec	k1gMnPc4	poslanec
za	za	k7c4	za
ČSR	ČSR	kA	ČSR
zvolila	zvolit	k5eAaPmAgFnS	zvolit
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
řad	řada	k1gFnPc2	řada
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
77	[number]	k4	77
<g/>
/	/	kIx~	/
<g/>
1968	[number]	k4	1968
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
přípravě	příprava	k1gFnSc6	příprava
federativního	federativní	k2eAgNnSc2d1	federativní
uspořádání	uspořádání	k1gNnSc2	uspořádání
Československé	československý	k2eAgFnSc2d1	Československá
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
řádné	řádný	k2eAgFnPc1d1	řádná
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
až	až	k9	až
26	[number]	k4	26
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
27.11	[number]	k4	27.11
<g/>
.1971	.1971	k4	.1971
<g/>
;	;	kIx,	;
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
každých	každý	k3xTgInPc2	každý
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
poslanců	poslanec	k1gMnPc2	poslanec
vyměněna	vyměněn	k2eAgFnSc1d1	vyměněna
kooptací	kooptace	k1gFnSc7	kooptace
namísto	namísto	k7c2	namísto
řádných	řádný	k2eAgFnPc2d1	řádná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Postavení	postavení	k1gNnSc1	postavení
Sněmovny	sněmovna	k1gFnSc2	sněmovna
národů	národ	k1gInPc2	národ
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
ČSSR	ČSSR	kA	ČSSR
bylo	být	k5eAaImAgNnS	být
silnější	silný	k2eAgInSc4d2	silnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
postavení	postavení	k1gNnSc1	postavení
Senátu	senát	k1gInSc2	senát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prvorepublikového	prvorepublikový	k2eAgInSc2d1	prvorepublikový
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
sněmovny	sněmovna	k1gFnPc1	sněmovna
FS	FS	kA	FS
si	se	k3xPyFc3	se
byly	být	k5eAaImAgInP	být
rovnocenné	rovnocenný	k2eAgInPc1d1	rovnocenný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
platnému	platný	k2eAgNnSc3d1	platné
usnesení	usnesení	k1gNnSc3	usnesení
a	a	k8xC	a
přijetí	přijetí	k1gNnSc3	přijetí
zákona	zákon	k1gInSc2	zákon
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
souhlasu	souhlas	k1gInSc2	souhlas
kvalifikované	kvalifikovaný	k2eAgFnSc2d1	kvalifikovaná
většiny	většina	k1gFnSc2	většina
poslanců	poslanec	k1gMnPc2	poslanec
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
sněmovnách	sněmovna	k1gFnPc6	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
platil	platit	k5eAaImAgInS	platit
zákaz	zákaz	k1gInSc1	zákaz
majorizace	majorizace	k1gFnSc2	majorizace
při	při	k7c6	při
společném	společný	k2eAgNnSc6d1	společné
hlasování	hlasování	k1gNnSc6	hlasování
hlasování	hlasování	k1gNnSc1	hlasování
obou	dva	k4xCgFnPc2	dva
sněmoven	sněmovna	k1gFnPc2	sněmovna
o	o	k7c6	o
řadě	řada	k1gFnSc6	řada
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
vládě	vláda	k1gFnSc6	vláda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
menšina	menšina	k1gFnSc1	menšina
slovenských	slovenský	k2eAgMnPc2d1	slovenský
poslanců	poslanec	k1gMnPc2	poslanec
nesměla	smět	k5eNaImAgFnS	smět
být	být	k5eAaImF	být
přehlasována	přehlasovat	k5eAaBmNgFnS	přehlasovat
českou	český	k2eAgFnSc7d1	Česká
většinou	většina	k1gFnSc7	většina
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
sporné	sporný	k2eAgNnSc4d1	sporné
usnesení	usnesení	k1gNnSc4	usnesení
zablokovat	zablokovat	k5eAaPmF	zablokovat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
obě	dva	k4xCgFnPc1	dva
sněmovny	sněmovna	k1gFnPc1	sněmovna
hlasovaly	hlasovat	k5eAaImAgFnP	hlasovat
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
mohly	moct	k5eAaImAgFnP	moct
se	se	k3xPyFc4	se
usnést	usnést	k5eAaPmF	usnést
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
dohodovacím	dohodovací	k2eAgNnSc6d1	dohodovací
řízení	řízení	k1gNnSc1	řízení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
dohodovací	dohodovací	k2eAgInSc1d1	dohodovací
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
tvořený	tvořený	k2eAgInSc4d1	tvořený
zpravidla	zpravidla	k6eAd1	zpravidla
20	[number]	k4	20
poslanci	poslanec	k1gMnPc7	poslanec
(	(	kIx(	(
<g/>
po	po	k7c6	po
deseti	deset	k4xCc6	deset
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
sněmovnu	sněmovna	k1gFnSc4	sněmovna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
ani	ani	k8xC	ani
dohodovací	dohodovací	k2eAgInSc1d1	dohodovací
výbor	výbor	k1gInSc1	výbor
nedošel	dojít	k5eNaPmAgInS	dojít
ke	k	k7c3	k
shodě	shoda	k1gFnSc3	shoda
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
prezident	prezident	k1gMnSc1	prezident
Federální	federální	k2eAgNnSc4d1	federální
shromáždění	shromáždění	k1gNnSc4	shromáždění
rozpustit	rozpustit	k5eAaPmF	rozpustit
a	a	k8xC	a
vypsat	vypsat	k5eAaPmF	vypsat
nové	nový	k2eAgFnPc4d1	nová
volby	volba	k1gFnPc4	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
normalizace	normalizace	k1gFnSc2	normalizace
však	však	k8xC	však
takových	takový	k3xDgFnPc2	takový
případ	případ	k1gInSc1	případ
neshody	neshoda	k1gFnSc2	neshoda
prakticky	prakticky	k6eAd1	prakticky
nastat	nastat	k5eAaPmF	nastat
nemohl	moct	k5eNaImAgMnS	moct
a	a	k8xC	a
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
nedošlo	dojít	k5eNaPmAgNnS	dojít
ani	ani	k8xC	ani
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
celé	celý	k2eAgNnSc4d1	celé
Federální	federální	k2eAgNnSc4d1	federální
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
,	,	kIx,	,
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
jednokomorovou	jednokomorový	k2eAgFnSc4d1	jednokomorová
Českou	český	k2eAgFnSc4d1	Česká
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rozdělení	rozdělení	k1gNnSc6	rozdělení
federace	federace	k1gFnSc2	federace
vyvstal	vyvstat	k5eAaPmAgInS	vyvstat
však	však	k9	však
problém	problém	k1gInSc1	problém
s	s	k7c7	s
umístěním	umístění	k1gNnSc7	umístění
českých	český	k2eAgMnPc2d1	český
poslanců	poslanec	k1gMnPc2	poslanec
zanikajícího	zanikající	k2eAgNnSc2d1	zanikající
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Ústava	ústava	k1gFnSc1	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
účinná	účinný	k2eAgFnSc1d1	účinná
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zakotvila	zakotvit	k5eAaPmAgFnS	zakotvit
dvoukomorový	dvoukomorový	k2eAgInSc4d1	dvoukomorový
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc4d1	složený
z	z	k7c2	z
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
a	a	k8xC	a
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zřízení	zřízení	k1gNnSc1	zřízení
senátu	senát	k1gInSc2	senát
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
diskusích	diskuse	k1gFnPc6	diskuse
silně	silně	k6eAd1	silně
zpochybňováno	zpochybňovat	k5eAaImNgNnS	zpochybňovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
vzácné	vzácný	k2eAgFnPc1d1	vzácná
nebyly	být	k5eNaImAgFnP	být
ani	ani	k9	ani
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
vypuštění	vypuštění	k1gNnSc4	vypuštění
z	z	k7c2	z
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Do	do	k7c2	do
doby	doba	k1gFnSc2	doba
jeho	jeho	k3xOp3gNnPc2	jeho
ustavení	ustavení	k1gNnPc2	ustavení
bylo	být	k5eAaImAgNnS	být
zamýšleno	zamýšlen	k2eAgNnSc1d1	zamýšleno
zřídit	zřídit	k5eAaPmF	zřídit
Prozatímní	prozatímní	k2eAgInSc4d1	prozatímní
Senát	senát	k1gInSc4	senát
(	(	kIx(	(
<g/>
čl	čl	kA	čl
<g/>
.	.	kIx.	.
106	[number]	k4	106
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
Ústavy	ústava	k1gFnSc2	ústava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
nakonec	nakonec	k6eAd1	nakonec
uzákoněn	uzákoněn	k2eAgMnSc1d1	uzákoněn
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnPc1	první
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
až	až	k9	až
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgFnPc4	první
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
obvodů	obvod	k1gInPc2	obvod
volila	volit	k5eAaImAgFnS	volit
senátora	senátor	k1gMnSc4	senátor
na	na	k7c4	na
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
zvolila	zvolit	k5eAaPmAgFnS	zvolit
senátora	senátor	k1gMnSc2	senátor
se	s	k7c7	s
čtyřletým	čtyřletý	k2eAgInSc7d1	čtyřletý
mandátem	mandát	k1gInSc7	mandát
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
volila	volit	k5eAaImAgFnS	volit
senátora	senátor	k1gMnSc2	senátor
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
předepsaným	předepsaný	k2eAgInSc7d1	předepsaný
šestiletým	šestiletý	k2eAgInSc7d1	šestiletý
mandátem	mandát	k1gInSc7	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Zkrácením	zkrácení	k1gNnSc7	zkrácení
prvních	první	k4xOgInPc2	první
mandátů	mandát	k1gInPc2	mandát
u	u	k7c2	u
vybraných	vybraný	k2eAgInPc2d1	vybraný
senátních	senátní	k2eAgInPc2d1	senátní
obvodů	obvod	k1gInPc2	obvod
byla	být	k5eAaImAgFnS	být
naplněna	naplnit	k5eAaPmNgFnS	naplnit
ústavní	ústavní	k2eAgFnSc1d1	ústavní
klauzule	klauzule	k1gFnSc1	klauzule
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
konají	konat	k5eAaImIp3nP	konat
každé	každý	k3xTgNnSc4	každý
2	[number]	k4	2
roky	rok	k1gInPc4	rok
a	a	k8xC	a
volí	volit	k5eAaImIp3nP	volit
se	s	k7c7	s
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
senátu	senát	k1gInSc2	senát
(	(	kIx(	(
<g/>
samotná	samotný	k2eAgFnSc1d1	samotná
délka	délka	k1gFnSc1	délka
mandátu	mandát	k1gInSc2	mandát
je	být	k5eAaImIp3nS	být
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc4	první
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ustavující	ustavující	k2eAgFnSc2d1	ustavující
schůze	schůze	k1gFnSc2	schůze
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
vedení	vedení	k1gNnSc1	vedení
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Petr	Petr	k1gMnSc1	Petr
Pithart	Pithart	k1gInSc4	Pithart
z	z	k7c2	z
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
kandidáti	kandidát	k1gMnPc1	kandidát
vládní	vládní	k2eAgFnSc2d1	vládní
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
32	[number]	k4	32
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
společně	společně	k6eAd1	společně
se	s	k7c7	s
senátorským	senátorský	k2eAgInSc7d1	senátorský
klubem	klub	k1gInSc7	klub
KDU-ČSL	KDU-ČSL	k1gFnPc2	KDU-ČSL
(	(	kIx(	(
<g/>
13	[number]	k4	13
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
a	a	k8xC	a
klubem	klub	k1gInSc7	klub
ODA	ODA	kA	ODA
(	(	kIx(	(
<g/>
7	[number]	k4	7
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
komoře	komora	k1gFnSc6	komora
parlamentu	parlament	k1gInSc2	parlament
ústavní	ústavní	k2eAgFnSc4d1	ústavní
většinu	většina	k1gFnSc4	většina
52	[number]	k4	52
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
provládní	provládní	k2eAgFnSc1d1	provládní
koalice	koalice	k1gFnSc1	koalice
vydržela	vydržet	k5eAaPmAgFnS	vydržet
až	až	k9	až
do	do	k7c2	do
pádu	pád	k1gInSc2	pád
vlády	vláda	k1gFnSc2	vláda
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1997	[number]	k4	1997
a	a	k8xC	a
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Senát	senát	k1gInSc1	senát
podporoval	podporovat	k5eAaImAgInS	podporovat
úřednickou	úřednický	k2eAgFnSc4d1	úřednická
vládu	vláda	k1gFnSc4	vláda
Josefa	Josef	k1gMnSc4	Josef
Tošovského	Tošovský	k1gMnSc4	Tošovský
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
předčasných	předčasný	k2eAgFnPc2d1	předčasná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sněmovních	sněmovní	k2eAgFnPc6d1	sněmovní
volbách	volba	k1gFnPc6	volba
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
dvě	dva	k4xCgFnPc1	dva
nejsilnější	silný	k2eAgFnPc1d3	nejsilnější
parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
strany	strana	k1gFnPc1	strana
(	(	kIx(	(
<g/>
ODS	ODS	kA	ODS
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
Opoziční	opoziční	k2eAgFnSc4d1	opoziční
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dávala	dávat	k5eAaImAgFnS	dávat
nejen	nejen	k6eAd1	nejen
možnost	možnost	k1gFnSc4	možnost
oběma	dva	k4xCgFnPc3	dva
politickým	politický	k2eAgFnPc3d1	politická
stranám	strana	k1gFnPc3	strana
měnit	měnit	k5eAaImF	měnit
ústavu	ústava	k1gFnSc4	ústava
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
měly	mít	k5eAaImAgFnP	mít
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
ústavní	ústavní	k2eAgFnSc4d1	ústavní
většinu	většina	k1gFnSc4	většina
jak	jak	k8xC	jak
v	v	k7c6	v
PS	PS	kA	PS
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
vládní	vládní	k2eAgFnSc1d1	vládní
sociální	sociální	k2eAgFnSc1d1	sociální
demokracie	demokracie	k1gFnSc1	demokracie
podpořila	podpořit	k5eAaPmAgFnS	podpořit
kandidáta	kandidát	k1gMnSc4	kandidát
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
Libuše	Libuše	k1gFnSc1	Libuše
Benešová	Benešová	k1gFnSc1	Benešová
<g/>
)	)	kIx)	)
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
politická	politický	k2eAgFnSc1d1	politická
konstelace	konstelace	k1gFnSc1	konstelace
však	však	k9	však
vydržela	vydržet	k5eAaPmAgFnS	vydržet
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
r.	r.	kA	r.
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
většinu	většina	k1gFnSc4	většina
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
obsadili	obsadit	k5eAaPmAgMnP	obsadit
členové	člen	k1gMnPc1	člen
Čtyřkoalice	Čtyřkoalice	k1gFnSc2	Čtyřkoalice
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
malých	malý	k2eAgFnPc2d1	malá
stran	strana	k1gFnPc2	strana
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
i	i	k9	i
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
r.	r.	kA	r.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Čtyřkoalice	Čtyřkoalice	k1gFnSc1	Čtyřkoalice
opět	opět	k6eAd1	opět
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
však	však	k9	však
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
vládní	vládní	k2eAgInSc4d1	vládní
subjekt	subjekt	k1gInSc4	subjekt
a	a	k8xC	a
Senát	senát	k1gInSc4	senát
tak	tak	k6eAd1	tak
dál	daleko	k6eAd2	daleko
podporoval	podporovat	k5eAaImAgInS	podporovat
Špidlův	Špidlův	k2eAgInSc1d1	Špidlův
kabinet	kabinet	k1gInSc1	kabinet
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
vedená	vedený	k2eAgFnSc1d1	vedená
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
M.	M.	kA	M.
Topolánek	Topolánek	k1gInSc1	Topolánek
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Senátem	senát	k1gInSc7	senát
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
r.	r.	kA	r.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ČSSD	ČSSD	kA	ČSSD
vedená	vedený	k2eAgFnSc1d1	vedená
premiérem	premiér	k1gMnSc7	premiér
S.	S.	kA	S.
Grossem	Gross	k1gMnSc7	Gross
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
velkou	velká	k1gFnSc4	velká
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
měla	mít	k5eAaImAgFnS	mít
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
pouhých	pouhý	k2eAgMnPc2d1	pouhý
7	[number]	k4	7
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
ODS	ODS	kA	ODS
se	se	k3xPyFc4	se
opakoval	opakovat	k5eAaImAgInS	opakovat
i	i	k9	i
v	v	k7c6	v
supervolebním	supervolební	k2eAgInSc6d1	supervolební
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
M.	M.	kA	M.
Topolánek	Topolánka	k1gFnPc2	Topolánka
jako	jako	k8xS	jako
premiér	premiér	k1gMnSc1	premiér
obhájil	obhájit	k5eAaPmAgMnS	obhájit
post	post	k1gInSc4	post
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
strany	strana	k1gFnSc2	strana
i	i	k9	i
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
r.	r.	kA	r.
2006	[number]	k4	2006
mají	mít	k5eAaImIp3nP	mít
prvenství	prvenství	k1gNnSc4	prvenství
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
získala	získat	k5eAaPmAgFnS	získat
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
počet	počet	k1gInSc1	počet
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
disponovat	disponovat	k5eAaBmF	disponovat
většinou	většinou	k6eAd1	většinou
senátních	senátní	k2eAgInPc2d1	senátní
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
l.	l.	k?	l.
2006	[number]	k4	2006
-	-	kIx~	-
2008	[number]	k4	2008
měla	mít	k5eAaImAgFnS	mít
41	[number]	k4	41
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
polovinu	polovina	k1gFnSc4	polovina
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
ztratila	ztratit	k5eAaPmAgFnS	ztratit
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
r.	r.	kA	r.
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc2	koalice
vedená	vedený	k2eAgFnSc1d1	vedená
ODS	ODS	kA	ODS
si	se	k3xPyFc3	se
však	však	k9	však
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
udržela	udržet	k5eAaPmAgFnS	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dokázala	dokázat	k5eAaPmAgFnS	dokázat
ČSSD	ČSSD	kA	ČSSD
zopakovat	zopakovat	k5eAaPmF	zopakovat
takový	takový	k3xDgInSc4	takový
výsledek	výsledek	k1gInSc4	výsledek
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
měla	mít	k5eAaImAgFnS	mít
v	v	k7c6	v
r.	r.	kA	r.
2006	[number]	k4	2006
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
výsledné	výsledný	k2eAgNnSc1d1	výsledné
obsazení	obsazení	k1gNnSc1	obsazení
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
jedinou	jediný	k2eAgFnSc4d1	jediná
polit	polit	k2eAgMnSc1d1	polit
<g/>
.	.	kIx.	.
stranou	stranou	k6eAd1	stranou
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
opoziční	opoziční	k2eAgFnSc2d1	opoziční
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
ČSSD	ČSSD	kA	ČSSD
se	se	k3xPyFc4	se
opakoval	opakovat	k5eAaImAgInS	opakovat
i	i	k9	i
v	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
dále	daleko	k6eAd2	daleko
posílila	posílit	k5eAaPmAgFnS	posílit
na	na	k7c4	na
celkových	celkový	k2eAgInPc2d1	celkový
46	[number]	k4	46
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
senátní	senátní	k2eAgFnPc1d1	senátní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
r.	r.	kA	r.
2014	[number]	k4	2014
znamenaly	znamenat	k5eAaImAgFnP	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
nadpoloviční	nadpoloviční	k2eAgFnSc2d1	nadpoloviční
většiny	většina	k1gFnSc2	většina
pro	pro	k7c4	pro
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gInSc1	jejich
senátorský	senátorský	k2eAgInSc1d1	senátorský
klub	klub	k1gInSc1	klub
měl	mít	k5eAaImAgInS	mít
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
35	[number]	k4	35
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotná	samotný	k2eAgFnSc1d1	samotná
vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc1	koalice
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gMnPc1	KDU-ČSL
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
udržela	udržet	k5eAaPmAgFnS	udržet
pohodlnou	pohodlný	k2eAgFnSc4d1	pohodlná
většinu	většina	k1gFnSc4	většina
(	(	kIx(	(
<g/>
49	[number]	k4	49
senátorů	senátor	k1gMnPc2	senátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Senátní	senátní	k2eAgFnPc1d1	senátní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
znamenaly	znamenat	k5eAaImAgFnP	znamenat
úspěch	úspěch	k1gInSc4	úspěch
především	především	k6eAd1	především
pro	pro	k7c4	pro
KDU-ČSL	KDU-ČSL	k1gMnPc4	KDU-ČSL
či	či	k8xC	či
kandidáty	kandidát	k1gMnPc4	kandidát
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
KDU-ČSL	KDU-ČSL	k1gFnSc1	KDU-ČSL
podporovala	podporovat	k5eAaImAgFnS	podporovat
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zisk	zisk	k1gInSc4	zisk
9	[number]	k4	9
mandátů	mandát	k1gInPc2	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
propadly	propadnout	k5eAaPmAgInP	propadnout
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc1	koalice
udržela	udržet	k5eAaPmAgFnS	udržet
většinu	většina	k1gFnSc4	většina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
opozičních	opoziční	k2eAgFnPc2d1	opoziční
stran	strana	k1gFnPc2	strana
získaly	získat	k5eAaPmAgFnP	získat
senátory	senátor	k1gMnPc4	senátor
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
3	[number]	k4	3
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
STAN	stan	k1gInSc1	stan
(	(	kIx(	(
<g/>
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
a	a	k8xC	a
STAN	stan	k1gInSc4	stan
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
2	[number]	k4	2
mandáty	mandát	k1gInPc4	mandát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žádného	žádný	k1gMnSc2	žádný
senátora	senátor	k1gMnSc2	senátor
nezískala	získat	k5eNaPmAgFnS	získat
KSČM	KSČM	kA	KSČM
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
menších	malý	k2eAgFnPc2d2	menší
stran	strana	k1gFnPc2	strana
úspěch	úspěch	k1gInSc4	úspěch
slavily	slavit	k5eAaImAgInP	slavit
také	také	k9	také
Severočeši	Severočech	k1gMnPc1	Severočech
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
,	,	kIx,	,
OPAT	opat	k1gMnSc1	opat
<g/>
,	,	kIx,	,
SLK	SLK	kA	SLK
či	či	k8xC	či
"	"	kIx"	"
<g/>
OSN	OSN	kA	OSN
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnějším	početní	k2eAgInSc7d3	nejpočetnější
senátorským	senátorský	k2eAgInSc7d1	senátorský
klubem	klub	k1gInSc7	klub
zůstal	zůstat	k5eAaPmAgInS	zůstat
Senátorský	senátorský	k2eAgInSc1d1	senátorský
klub	klub	k1gInSc1	klub
ČSSD	ČSSD	kA	ČSSD
(	(	kIx(	(
<g/>
25	[number]	k4	25
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
Senátorský	senátorský	k2eAgInSc4d1	senátorský
klub	klub	k1gInSc4	klub
KDU-ČSL	KDU-ČSL	k1gMnSc1	KDU-ČSL
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
(	(	kIx(	(
<g/>
16	[number]	k4	16
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následují	následovat	k5eAaImIp3nP	následovat
Klub	klub	k1gInSc4	klub
Starostové	Starostová	k1gFnSc2	Starostová
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
(	(	kIx(	(
<g/>
11	[number]	k4	11
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Senátorský	senátorský	k2eAgInSc1d1	senátorský
klub	klub	k1gInSc1	klub
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
10	[number]	k4	10
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Senátorský	senátorský	k2eAgInSc1d1	senátorský
klub	klub	k1gInSc1	klub
ANO	ano	k9	ano
(	(	kIx(	(
<g/>
7	[number]	k4	7
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
a	a	k8xC	a
Klub	klub	k1gInSc1	klub
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
senátorů	senátor	k1gMnPc2	senátor
(	(	kIx(	(
<g/>
5	[number]	k4	5
členů	člen	k1gInPc2	člen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
obhajovalo	obhajovat	k5eAaImAgNnS	obhajovat
svůj	svůj	k3xOyFgInSc4	svůj
mandát	mandát	k1gInSc4	mandát
19	[number]	k4	19
senátorů	senátor	k1gMnPc2	senátor
(	(	kIx(	(
<g/>
volilo	volit	k5eAaImAgNnS	volit
se	se	k3xPyFc4	se
celkem	celkem	k6eAd1	celkem
ve	v	k7c6	v
27	[number]	k4	27
obvodech	obvod	k1gInPc6	obvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uspělo	uspět	k5eAaPmAgNnS	uspět
10	[number]	k4	10
senátorů	senátor	k1gMnPc2	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
4	[number]	k4	4
-	-	kIx~	-
Most	most	k1gInSc1	most
však	však	k9	však
později	pozdě	k6eAd2	pozdě
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
se	se	k3xPyFc4	se
opakovat	opakovat	k5eAaImF	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
projednává	projednávat	k5eAaImIp3nS	projednávat
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
postoupila	postoupit	k5eAaPmAgFnS	postoupit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
může	moct	k5eAaImIp3nS	moct
<g/>
:	:	kIx,	:
schválit	schválit	k5eAaPmF	schválit
-	-	kIx~	-
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
postoupen	postoupit	k5eAaPmNgInS	postoupit
prezidentovi	prezident	k1gMnSc3	prezident
ČR	ČR	kA	ČR
zamítnout	zamítnout	k5eAaPmF	zamítnout
-	-	kIx~	-
návrh	návrh	k1gInSc1	návrh
je	být	k5eAaImIp3nS	být
vrácen	vrátit	k5eAaPmNgInS	vrátit
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
Senát	senát	k1gInSc4	senát
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
nadpoloviční	nadpoloviční	k2eAgFnSc7d1	nadpoloviční
většinou	většina	k1gFnSc7	většina
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Přehlasování	přehlasování	k1gNnSc1	přehlasování
poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ústavního	ústavní	k2eAgInSc2d1	ústavní
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
volebních	volební	k2eAgInPc2d1	volební
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
styku	styk	k1gInSc6	styk
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
jednacího	jednací	k2eAgInSc2d1	jednací
řádu	řád	k1gInSc2	řád
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
vrátit	vrátit	k5eAaPmF	vrátit
s	s	k7c7	s
pozměňovacími	pozměňovací	k2eAgInPc7d1	pozměňovací
návrhy	návrh	k1gInPc7	návrh
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
-	-	kIx~	-
ta	ten	k3xDgFnSc1	ten
o	o	k7c6	o
zákonu	zákon	k1gInSc6	zákon
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
navrženém	navržený	k2eAgNnSc6d1	navržené
Senátem	senát	k1gInSc7	senát
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
<g />
.	.	kIx.	.
</s>
<s>
není	být	k5eNaImIp3nS	být
přijato	přijmout	k5eAaPmNgNnS	přijmout
prostou	prostý	k2eAgFnSc7d1	prostá
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
hlasuje	hlasovat	k5eAaImIp3nS	hlasovat
znovu	znovu	k6eAd1	znovu
o	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
znění	znění	k1gNnSc6	znění
navrženém	navržený	k2eAgNnSc6d1	navržené
Poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
případně	případně	k6eAd1	případně
schválen	schválit	k5eAaPmNgInS	schválit
nadpoloviční	nadpoloviční	k2eAgFnSc7d1	nadpoloviční
většinou	většina	k1gFnSc7	většina
hlasů	hlas	k1gInPc2	hlas
všech	všecek	k3xTgMnPc2	všecek
poslanců	poslanec	k1gMnPc2	poslanec
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
vůli	vůle	k1gFnSc4	vůle
se	se	k3xPyFc4	se
návrhem	návrh	k1gInSc7	návrh
nezabývat	zabývat	k5eNaImF	zabývat
-	-	kIx~	-
tímto	tento	k3xDgNnSc7	tento
usnesením	usnesení	k1gNnSc7	usnesení
je	být	k5eAaImIp3nS	být
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
přijat	přijat	k2eAgMnSc1d1	přijat
nevyjádřit	vyjádřit	k5eNaPmF	vyjádřit
se	se	k3xPyFc4	se
-	-	kIx~	-
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Senát	senát	k1gInSc1	senát
do	do	k7c2	do
30	[number]	k4	30
dnů	den	k1gInPc2	den
k	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
zákona	zákon	k1gInSc2	zákon
nevyjádří	vyjádřit	k5eNaPmIp3nS	vyjádřit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
přijat	přijat	k2eAgInSc1d1	přijat
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
má	mít	k5eAaImIp3nS	mít
a	a	k8xC	a
opakovaně	opakovaně	k6eAd1	opakovaně
využívá	využívat	k5eAaPmIp3nS	využívat
právo	právo	k1gNnSc4	právo
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
iniciativy	iniciativa	k1gFnSc2	iniciativa
<g/>
,	,	kIx,	,
t.	t.	k?	t.
j.	j.	k?	j.
může	moct	k5eAaImIp3nS	moct
poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
podávat	podávat	k5eAaImF	podávat
vlastní	vlastní	k2eAgInPc4d1	vlastní
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
přijímá	přijímat	k5eAaImIp3nS	přijímat
v	v	k7c6	v
naléhavých	naléhavý	k2eAgFnPc6d1	naléhavá
záležitostech	záležitost	k1gFnPc6	záležitost
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
zákonů	zákon	k1gInPc2	zákon
zákonná	zákonný	k2eAgNnPc4d1	zákonné
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
může	moct	k5eAaImIp3nS	moct
navrhovat	navrhovat	k5eAaImF	navrhovat
jen	jen	k9	jen
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
která	který	k3yRgFnSc1	který
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schválena	schválit	k5eAaPmNgFnS	schválit
na	na	k7c6	na
první	první	k4xOgFnSc6	první
schůzi	schůze	k1gFnSc6	schůze
nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgFnSc2d1	zvolená
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
dále	daleko	k6eAd2	daleko
pozbývají	pozbývat	k5eAaImIp3nP	pozbývat
platnosti	platnost	k1gFnSc3	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Zákonná	zákonný	k2eAgNnPc4d1	zákonné
opatření	opatření	k1gNnPc4	opatření
nelze	lze	k6eNd1	lze
přijímat	přijímat	k5eAaImF	přijímat
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
Ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
,	,	kIx,	,
státního	státní	k2eAgInSc2d1	státní
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
účtu	účet	k1gInSc2	účet
<g/>
,	,	kIx,	,
volebního	volební	k2eAgInSc2d1	volební
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
termín	termín	k1gInSc1	termín
voleb	volba	k1gFnPc2	volba
prezidenta	prezident	k1gMnSc2	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
může	moct	k5eAaImIp3nS	moct
podat	podat	k5eAaPmF	podat
k	k	k7c3	k
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
soudu	soud	k1gInSc3	soud
na	na	k7c4	na
prezidenta	prezident	k1gMnSc2	prezident
žalobu	žaloba	k1gFnSc4	žaloba
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
jmenováním	jmenování	k1gNnSc7	jmenování
soudců	soudce	k1gMnPc2	soudce
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
vydává	vydávat	k5eAaPmIp3nS	vydávat
usnesení	usnesení	k1gNnSc1	usnesení
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
nemůže	moct	k5eNaImIp3nS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
ze	z	k7c2	z
závažných	závažný	k2eAgInPc2d1	závažný
důvodů	důvod	k1gInPc2	důvod
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Senátu	senát	k1gInSc2	senát
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
předsedu	předseda	k1gMnSc4	předseda
a	a	k8xC	a
inspektory	inspektor	k1gMnPc4	inspektor
Úřadu	úřad	k1gInSc2	úřad
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
osobních	osobní	k2eAgInPc2d1	osobní
údajů	údaj	k1gInPc2	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
též	též	k6eAd1	též
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
dva	dva	k4xCgMnPc4	dva
kandidáty	kandidát	k1gMnPc4	kandidát
na	na	k7c4	na
Veřejného	veřejný	k2eAgInSc2d1	veřejný
ochránce	ochránce	k1gMnSc1	ochránce
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
tajným	tajný	k2eAgNnSc7d1	tajné
hlasováním	hlasování	k1gNnSc7	hlasování
na	na	k7c6	na
základě	základ	k1gInSc6	základ
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
<g/>
,	,	kIx,	,
rovného	rovný	k2eAgNnSc2d1	rovné
a	a	k8xC	a
přímého	přímý	k2eAgNnSc2d1	přímé
volebního	volební	k2eAgNnSc2d1	volební
práva	právo	k1gNnSc2	právo
podle	podle	k7c2	podle
dvoukolového	dvoukolový	k2eAgInSc2d1	dvoukolový
většinového	většinový	k2eAgInSc2d1	většinový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zákonem	zákon	k1gInSc7	zákon
stanovených	stanovený	k2eAgInPc6d1	stanovený
lhůtách	lhůta	k1gFnPc6	lhůta
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInPc1d1	volební
obvody	obvod	k1gInPc1	obvod
jsou	být	k5eAaImIp3nP	být
jednomandátové	jednomandátový	k2eAgInPc1d1	jednomandátový
a	a	k8xC	a
vymezené	vymezený	k2eAgInPc1d1	vymezený
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
81	[number]	k4	81
obvodech	obvod	k1gInPc6	obvod
(	(	kIx(	(
<g/>
každé	každý	k3xTgFnSc6	každý
2	[number]	k4	2
roky	rok	k1gInPc4	rok
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
třetině	třetina	k1gFnSc6	třetina
obvodů	obvod	k1gInPc2	obvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
každý	každý	k3xTgInSc4	každý
obvod	obvod	k1gInSc4	obvod
je	být	k5eAaImIp3nS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
kandidát	kandidát	k1gMnSc1	kandidát
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
volby	volba	k1gFnSc2	volba
získal	získat	k5eAaPmAgInS	získat
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
nezískal	získat	k5eNaPmAgInS	získat
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
<g/>
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
postupují	postupovat	k5eAaImIp3nP	postupovat
2	[number]	k4	2
kandidáti	kandidát	k1gMnPc1	kandidát
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
do	do	k7c2	do
kola	kolo	k1gNnSc2	kolo
druhého	druhý	k4xOgMnSc2	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
senátorem	senátor	k1gMnSc7	senátor
kandidát	kandidát	k1gMnSc1	kandidát
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
počtem	počet	k1gInSc7	počet
získaných	získaný	k2eAgInPc2d1	získaný
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
uvolnění	uvolnění	k1gNnSc2	uvolnění
nebo	nebo	k8xC	nebo
neobsazení	neobsazení	k1gNnSc2	neobsazení
některého	některý	k3yIgNnSc2	některý
místa	místo	k1gNnSc2	místo
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
pro	pro	k7c4	pro
příslušný	příslušný	k2eAgInSc4d1	příslušný
obvod	obvod	k1gInSc4	obvod
doplňovací	doplňovací	k2eAgFnSc2d1	doplňovací
volby	volba	k1gFnSc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Kandidátovi	kandidátův	k2eAgMnPc1d1	kandidátův
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c4	v
první	první	k4xOgInSc4	první
den	den	k1gInSc4	den
voleb	volba	k1gFnPc2	volba
nejméně	málo	k6eAd3	málo
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
15,38	[number]	k4	15,38
%	%	kIx~	%
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
po	po	k7c6	po
44,59	[number]	k4	44,59
%	%	kIx~	%
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Senátní	senátní	k2eAgFnPc1d1	senátní
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
sudých	sudý	k2eAgInPc6d1	sudý
letech	let	k1gInPc6	let
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
každých	každý	k3xTgFnPc2	každý
druhých	druhý	k4xOgFnPc2	druhý
voleb	volba	k1gFnPc2	volba
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
společně	společně	k6eAd1	společně
s	s	k7c7	s
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
ostatních	ostatní	k2eAgFnPc2d1	ostatní
voleb	volba	k1gFnPc2	volba
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
společně	společně	k6eAd1	společně
s	s	k7c7	s
volbami	volba	k1gFnPc7	volba
do	do	k7c2	do
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
krajů	kraj	k1gInPc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
členů	člen	k1gMnPc2	člen
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
toto	tento	k3xDgNnSc4	tento
složení	složení	k1gNnSc4	složení
<g/>
:	:	kIx,	:
Mandáty	mandát	k1gInPc1	mandát
senátorů	senátor	k1gMnPc2	senátor
Mandáty	mandát	k1gInPc4	mandát
senátorů	senátor	k1gMnPc2	senátor
pro	pro	k7c4	pro
11	[number]	k4	11
<g/>
.	.	kIx.	.
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
2018	[number]	k4	2018
vzešly	vzejít	k5eAaPmAgFnP	vzejít
z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
třetiny	třetina	k1gFnSc2	třetina
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
a	a	k8xC	a
2016	[number]	k4	2016
a	a	k8xC	a
z	z	k7c2	z
doplňovacích	doplňovací	k2eAgFnPc2d1	doplňovací
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Senát	senát	k1gInSc1	senát
navenek	navenek	k6eAd1	navenek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
pravomoci	pravomoc	k1gFnPc4	pravomoc
patří	patřit	k5eAaImIp3nP	patřit
svolávání	svolávání	k1gNnSc3	svolávání
<g/>
,	,	kIx,	,
zahajování	zahajování	k1gNnSc3	zahajování
<g/>
,	,	kIx,	,
přerušování	přerušování	k1gNnSc3	přerušování
a	a	k8xC	a
ukončování	ukončování	k1gNnSc3	ukončování
schůze	schůze	k1gFnSc2	schůze
Senátu	senát	k1gInSc2	senát
<g/>
;	;	kIx,	;
podepisování	podepisování	k1gNnSc2	podepisování
usnesení	usnesení	k1gNnSc2	usnesení
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
podepisuje	podepisovat	k5eAaImIp3nS	podepisovat
zákonná	zákonný	k2eAgNnPc4d1	zákonné
opatření	opatření	k1gNnPc4	opatření
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
neobsazené	obsazený	k2eNgFnSc2d1	neobsazená
funkce	funkce	k1gFnSc2	funkce
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
současného	současný	k2eAgNnSc2d1	současné
rozpuštění	rozpuštění	k1gNnSc2	rozpuštění
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
část	část	k1gFnSc1	část
pravomocí	pravomoc	k1gFnSc7	pravomoc
prezidenta	prezident	k1gMnSc2	prezident
svěřenou	svěřený	k2eAgFnSc7d1	svěřená
předsedovi	předseda	k1gMnSc3	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
Senátem	senát	k1gInSc7	senát
vždy	vždy	k6eAd1	vždy
po	po	k7c6	po
nových	nový	k2eAgFnPc6d1	nová
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
třetiny	třetina	k1gFnSc2	třetina
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Disponuje	disponovat	k5eAaBmIp3nS	disponovat
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
klíčů	klíč	k1gInPc2	klíč
od	od	k7c2	od
dveří	dveře	k1gFnPc2	dveře
do	do	k7c2	do
Korunní	korunní	k2eAgFnSc2d1	korunní
komory	komora	k1gFnSc2	komora
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
české	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
předsedou	předseda	k1gMnSc7	předseda
Senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
Milan	Milan	k1gMnSc1	Milan
Štěch	Štěch	k1gMnSc1	Štěch
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
9	[number]	k4	9
<g/>
.	.	kIx.	.
volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
Senátu	senát	k1gInSc2	senát
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
pro	pro	k7c4	pro
10	[number]	k4	10
<g/>
.	.	kIx.	.
volební	volební	k2eAgNnPc4d1	volební
období	období	k1gNnPc4	období
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Místopředsedové	místopředseda	k1gMnPc1	místopředseda
Senátu	senát	k1gInSc2	senát
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
předsedu	předseda	k1gMnSc4	předseda
Senátu	senát	k1gInSc2	senát
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
jím	on	k3xPp3gInSc7	on
určeném	určený	k2eAgInSc6d1	určený
a	a	k8xC	a
střídají	střídat	k5eAaImIp3nP	střídat
se	se	k3xPyFc4	se
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
schůze	schůze	k1gFnSc2	schůze
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Ústava	ústava	k1gFnSc1	ústava
ani	ani	k8xC	ani
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
107	[number]	k4	107
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
jednacím	jednací	k2eAgInSc6d1	jednací
řádu	řád	k1gInSc6	řád
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
takovou	takový	k3xDgFnSc4	takový
funkci	funkce	k1gFnSc4	funkce
výslovně	výslovně	k6eAd1	výslovně
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
např.	např.	kA	např.
od	od	k7c2	od
Federálního	federální	k2eAgNnSc2d1	federální
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
)	)	kIx)	)
neznají	neznat	k5eAaImIp3nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Nejdéle	dlouho	k6eAd3	dlouho
působícím	působící	k2eAgMnSc7d1	působící
senátorem	senátor	k1gMnSc7	senátor
je	být	k5eAaImIp3nS	být
Milan	Milan	k1gMnSc1	Milan
Štěch	Štěch	k1gMnSc1	Štěch
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
obvodě	obvod	k1gInSc6	obvod
č.	č.	k?	č.
15	[number]	k4	15
-	-	kIx~	-
Pelhřimov	Pelhřimov	k1gInSc1	Pelhřimov
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
a	a	k8xC	a
senátorský	senátorský	k2eAgInSc1d1	senátorský
mandát	mandát	k1gInSc1	mandát
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
v	v	k7c6	v
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
a	a	k8xC	a
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
2016	[number]	k4	2016
byli	být	k5eAaImAgMnP	být
senátory	senátor	k1gMnPc4	senátor
také	také	k9	také
Pavel	Pavel	k1gMnSc1	Pavel
Eybert	Eybert	k1gMnSc1	Eybert
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
13	[number]	k4	13
-	-	kIx~	-
Tábor	Tábor	k1gInSc1	Tábor
<g/>
)	)	kIx)	)
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
Sobotka	Sobotka	k1gMnSc1	Sobotka
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
(	(	kIx(	(
<g/>
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
34	[number]	k4	34
-	-	kIx~	-
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
již	již	k6eAd1	již
nekandidovali	kandidovat	k5eNaImAgMnP	kandidovat
<g/>
.	.	kIx.	.
</s>
<s>
Sídlem	sídlo	k1gNnSc7	sídlo
Senátu	senát	k1gInSc2	senát
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
59	[number]	k4	59
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
areál	areál	k1gInSc1	areál
Valdštejnského	valdštejnský	k2eAgInSc2d1	valdštejnský
paláce	palác	k1gInSc2	palác
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
Valdštejnskou	valdštejnský	k2eAgFnSc7d1	Valdštejnská
jízdárnou	jízdárna	k1gFnSc7	jízdárna
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Kolovratský	Kolovratský	k2eAgInSc1d1	Kolovratský
palác	palác	k1gInSc1	palác
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Fürstenberský	Fürstenberský	k2eAgInSc1d1	Fürstenberský
palác	palác	k1gInSc1	palác
<g/>
.	.	kIx.	.
</s>
<s>
Jednací	jednací	k2eAgInSc1d1	jednací
sál	sál	k1gInSc1	sál
Senátu	senát	k1gInSc2	senát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
bývalých	bývalý	k2eAgFnPc2d1	bývalá
Valdštejnových	Valdštejnův	k2eAgFnPc2d1	Valdštejnova
koníren	konírna	k1gFnPc2	konírna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
historické	historický	k2eAgFnPc1d1	historická
prostory	prostora	k1gFnPc1	prostora
slouží	sloužit	k5eAaImIp3nP	sloužit
senátorům	senátor	k1gMnPc3	senátor
pro	pro	k7c4	pro
jednání	jednání	k1gNnPc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Interiéry	interiér	k1gInPc1	interiér
Valdštejnského	valdštejnský	k2eAgInSc2d1	valdštejnský
paláce	palác	k1gInSc2	palác
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
přístupné	přístupný	k2eAgInPc1d1	přístupný
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
16	[number]	k4	16
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
déle	dlouho	k6eAd2	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
objektu	objekt	k1gInSc2	objekt
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
nádvoří	nádvoří	k1gNnSc6	nádvoří
Valdštejnského	valdštejnský	k2eAgInSc2d1	valdštejnský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
přístupné	přístupný	k2eAgNnSc1d1	přístupné
z	z	k7c2	z
Valdštejnského	valdštejnský	k2eAgNnSc2d1	Valdštejnské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejnská	valdštejnský	k2eAgFnSc1d1	Valdštejnská
zahrada	zahrada	k1gFnSc1	zahrada
je	být	k5eAaImIp3nS	být
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
od	od	k7c2	od
10	[number]	k4	10
do	do	k7c2	do
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
státních	státní	k2eAgInPc2d1	státní
svátků	svátek	k1gInPc2	svátek
je	být	k5eAaImIp3nS	být
slavnostně	slavnostně	k6eAd1	slavnostně
osvětlena	osvětlit	k5eAaPmNgFnS	osvětlit
a	a	k8xC	a
otevřena	otevřít	k5eAaPmNgFnS	otevřít
až	až	k6eAd1	až
do	do	k7c2	do
22	[number]	k4	22
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sala	sala	k6eAd1	sala
terreně	terreně	k6eAd1	terreně
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
koncerty	koncert	k1gInPc4	koncert
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Seznam	seznam	k1gInSc1	seznam
členů	člen	k1gMnPc2	člen
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Parlament	parlament	k1gInSc1	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Prozatímní	prozatímní	k2eAgInSc1d1	prozatímní
Senát	senát	k1gInSc1	senát
Seznam	seznam	k1gInSc1	seznam
sněmů	sněm	k1gInPc2	sněm
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
Panská	panský	k2eAgFnSc1d1	Panská
sněmovna	sněmovna	k1gFnSc1	sněmovna
Senát	senát	k1gInSc1	senát
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Senát	senát	k1gInSc1	senát
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
</s>
