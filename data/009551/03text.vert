<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Pukanec	pukanec	k1gInSc1	pukanec
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Košice	Košice	k1gInPc4	Košice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
evangelický	evangelický	k2eAgMnSc1d1	evangelický
kněz	kněz	k1gMnSc1	kněz
<g/>
,	,	kIx,	,
pronásledovaný	pronásledovaný	k2eAgMnSc1d1	pronásledovaný
komunistickým	komunistický	k2eAgInSc7d1	komunistický
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
byl	být	k5eAaImAgMnS	být
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
uvězněn	uvězněn	k2eAgMnSc1d1	uvězněn
na	na	k7c4	na
21	[number]	k4	21
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
Valdicích	Valdice	k1gFnPc6	Valdice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
místního	místní	k2eAgMnSc2d1	místní
kloboučníka	kloboučník	k1gMnSc2	kloboučník
jako	jako	k8xS	jako
nejmladší	mladý	k2eAgFnSc2d3	nejmladší
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
rodišti	rodiště	k1gNnSc6	rodiště
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
evangelickém	evangelický	k2eAgNnSc6d1	evangelické
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Citlivé	citlivý	k2eAgNnSc1d1	citlivé
vnímání	vnímání	k1gNnSc1	vnímání
života	život	k1gInSc2	život
ho	on	k3xPp3gNnSc4	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
zasvětit	zasvětit	k5eAaPmF	zasvětit
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
službě	služba	k1gFnSc6	služba
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zápasili	zápasit	k5eAaImAgMnP	zápasit
s	s	k7c7	s
těžkými	těžký	k2eAgInPc7d1	těžký
<g/>
,	,	kIx,	,
nejednou	jednou	k6eNd1	jednou
existenčními	existenční	k2eAgInPc7d1	existenční
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
po	po	k7c6	po
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
zvolil	zvolit	k5eAaPmAgMnS	zvolit
studium	studium	k1gNnSc4	studium
evangelické	evangelický	k2eAgFnSc2d1	evangelická
teologie	teologie	k1gFnSc2	teologie
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
nakrátko	nakrátko	k6eAd1	nakrátko
stal	stát	k5eAaPmAgInS	stát
evangelickým	evangelický	k2eAgMnSc7d1	evangelický
kaplanem	kaplan	k1gMnSc7	kaplan
v	v	k7c6	v
Brezně	Brezna	k1gFnSc6	Brezna
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
kaplan	kaplan	k1gMnSc1	kaplan
při	při	k7c6	při
seniorovi	senior	k1gMnSc6	senior
Bakossovi	Bakoss	k1gMnSc6	Bakoss
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
zastihlo	zastihnout	k5eAaPmAgNnS	zastihnout
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
spojené	spojený	k2eAgInPc1d1	spojený
smutné	smutný	k2eAgInPc1d1	smutný
osudy	osud	k1gInPc1	osud
mnoha	mnoho	k4c2	mnoho
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
pomoci	pomoct	k5eAaPmF	pomoct
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Riskantní	riskantní	k2eAgFnSc2d1	riskantní
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
křtění	křtění	k1gNnSc1	křtění
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
zachránili	zachránit	k5eAaPmAgMnP	zachránit
před	před	k7c7	před
transportem	transport	k1gInSc7	transport
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
farář	farář	k1gMnSc1	farář
pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
uvězněným	uvězněný	k2eAgNnSc7d1	uvězněné
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
lidem	člověk	k1gMnPc3	člověk
v	v	k7c6	v
době	doba	k1gFnSc6	doba
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
mládeži	mládež	k1gFnSc3	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
si	se	k3xPyFc3	se
získával	získávat	k5eAaImAgMnS	získávat
svou	svůj	k3xOyFgFnSc7	svůj
srdečností	srdečnost	k1gFnSc7	srdečnost
a	a	k8xC	a
otevřeností	otevřenost	k1gFnSc7	otevřenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
farářem	farář	k1gMnSc7	farář
do	do	k7c2	do
gemerské	gemerský	k2eAgFnSc2d1	gemerský
obce	obec	k1gFnSc2	obec
Sirk	Sirka	k1gFnPc2	Sirka
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
první	první	k4xOgFnPc4	první
negativní	negativní	k2eAgFnPc4d1	negativní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
zajistila	zajistit	k5eAaPmAgFnS	zajistit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
kázání	kázání	k1gNnSc6	kázání
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
vděčnost	vděčnost	k1gFnSc4	vděčnost
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
obce	obec	k1gFnSc2	obec
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
ne	ne	k9	ne
sovětské	sovětský	k2eAgFnSc3d1	sovětská
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1949	[number]	k4	1949
ho	on	k3xPp3gMnSc4	on
zajistily	zajistit	k5eAaPmAgInP	zajistit
orgány	orgán	k1gInPc1	orgán
VB	VB	kA	VB
<g/>
,	,	kIx,	,
držely	držet	k5eAaImAgFnP	držet
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
v	v	k7c6	v
Revúci	Revúec	k1gInSc6	Revúec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
soudu	soud	k1gInSc6	soud
v	v	k7c6	v
Rimavské	rimavský	k2eAgFnSc6d1	Rimavská
Sobotě	sobota	k1gFnSc6	sobota
byl	být	k5eAaImAgInS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
zproštěn	zproštěn	k2eAgMnSc1d1	zproštěn
obžaloby	obžaloba	k1gFnSc2	obžaloba
a	a	k8xC	a
propuštěn	propustit	k5eAaPmNgMnS	propustit
na	na	k7c4	na
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1949	[number]	k4	1949
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
odvedli	odvést	k5eAaPmAgMnP	odvést
do	do	k7c2	do
tábora	tábor	k1gInSc2	tábor
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Novákách	Novákách	k?	Novákách
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
záležitost	záležitost	k1gFnSc1	záležitost
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
zajištěním	zajištění	k1gNnSc7	zajištění
orgány	orgán	k1gInPc7	orgán
VB	VB	kA	VB
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1949	[number]	k4	1949
<g/>
;	;	kIx,	;
na	na	k7c6	na
službách	služba	k1gFnPc6	služba
Božích	boží	k2eAgFnPc6d1	boží
poděkoval	poděkovat	k5eAaPmAgMnS	poděkovat
Pánu	pán	k1gMnSc3	pán
Bohu	bůh	k1gMnSc3	bůh
za	za	k7c4	za
dar	dar	k1gInSc4	dar
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
nepoděkoval	poděkovat	k5eNaPmAgInS	poděkovat
se	se	k3xPyFc4	se
za	za	k7c2	za
osvobození	osvobození	k1gNnSc2	osvobození
Sovětské	sovětský	k2eAgFnSc6d1	sovětská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
okolnost	okolnost	k1gFnSc1	okolnost
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
odchod	odchod	k1gInSc4	odchod
ze	z	k7c2	z
Sirku	sirka	k1gFnSc4	sirka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
gemerské	gemerský	k2eAgFnSc2d1	gemerský
obce	obec	k1gFnSc2	obec
Vlachovo	Vlachův	k2eAgNnSc1d1	Vlachovo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
řádně	řádně	k6eAd1	řádně
zvoleným	zvolený	k2eAgMnSc7d1	zvolený
farářem	farář	k1gMnSc7	farář
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vlachově	Vlachův	k2eAgFnSc6d1	Vlachova
pocítil	pocítit	k5eAaPmAgMnS	pocítit
omezující	omezující	k2eAgNnSc4d1	omezující
působení	působení	k1gNnSc4	působení
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
vůči	vůči	k7c3	vůči
církvi	církev	k1gFnSc3	církev
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nastalo	nastat	k5eAaPmAgNnS	nastat
období	období	k1gNnSc4	období
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
prosazování	prosazování	k1gNnSc2	prosazování
ateistické	ateistický	k2eAgFnSc2d1	ateistická
ideologie	ideologie	k1gFnSc2	ideologie
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
KSČ	KSČ	kA	KSČ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
vychovat	vychovat	k5eAaPmF	vychovat
pokrokově	pokrokově	k6eAd1	pokrokově
smýšlejícího	smýšlející	k2eAgMnSc2d1	smýšlející
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
nezatíženého	zatížený	k2eNgMnSc2d1	nezatížený
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
s	s	k7c7	s
ideologickým	ideologický	k2eAgInSc7d1	ideologický
režimem	režim	k1gInSc7	režim
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
stupňovaly	stupňovat	k5eAaImAgFnP	stupňovat
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
skupiny	skupina	k1gFnSc2	skupina
KVAP	kvap	k1gInSc1	kvap
<g/>
.	.	kIx.	.
</s>
<s>
Skupinu	skupina	k1gFnSc4	skupina
KVAP	kvap	k1gInSc4	kvap
tvořili	tvořit	k5eAaImAgMnP	tvořit
čtyři	čtyři	k4xCgMnPc1	čtyři
faráři	farář	k1gMnPc1	farář
z	z	k7c2	z
Gemera	Gemero	k1gNnSc2	Gemero
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Bedřich	Bedřich	k1gMnSc1	Bedřich
Kasanický	Kasanický	k2eAgMnSc1d1	Kasanický
z	z	k7c2	z
Dobšiné	Dobšiná	k1gFnSc2	Dobšiná
<g/>
,	,	kIx,	,
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
z	z	k7c2	z
Vlachova	Vlachův	k2eAgInSc2d1	Vlachův
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Antal	Antal	k1gMnSc1	Antal
z	z	k7c2	z
Betliara	Betliar	k1gMnSc2	Betliar
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Paulův	Paulův	k2eAgMnSc1d1	Paulův
z	z	k7c2	z
Gemerské	Gemerský	k2eAgFnPc1d1	Gemerská
Polomy	polom	k1gInPc4	polom
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
faráři	farář	k1gMnPc1	farář
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
setkávali	setkávat	k5eAaImAgMnP	setkávat
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
svých	svůj	k3xOyFgFnPc2	svůj
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gNnPc1	jejich
setkání	setkání	k1gNnPc1	setkání
změnila	změnit	k5eAaPmAgNnP	změnit
i	i	k9	i
na	na	k7c4	na
místa	místo	k1gNnPc4	místo
teologického	teologický	k2eAgInSc2d1	teologický
a	a	k8xC	a
duchovního	duchovní	k2eAgInSc2d1	duchovní
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
vypracovat	vypracovat	k5eAaPmF	vypracovat
společný	společný	k2eAgInSc4d1	společný
soubor	soubor	k1gInSc4	soubor
kázání	kázání	k1gNnSc2	kázání
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
však	však	k9	však
VB	VB	kA	VB
jejich	jejich	k3xOp3gNnSc4	jejich
setkání	setkání	k1gNnSc4	setkání
vyhodnotila	vyhodnotit	k5eAaPmAgFnS	vyhodnotit
jako	jako	k9	jako
protistátní	protistátní	k2eAgFnSc1d1	protistátní
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
KVAP	kvap	k1gInSc1	kvap
byli	být	k5eAaImAgMnP	být
obviňováni	obviňovat	k5eAaImNgMnP	obviňovat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
ohrozit	ohrozit	k5eAaPmF	ohrozit
socialistický	socialistický	k2eAgInSc4d1	socialistický
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byli	být	k5eAaImAgMnP	být
tito	tento	k3xDgMnPc1	tento
čtyři	čtyři	k4xCgMnPc1	čtyři
faráři	farář	k1gMnPc1	farář
obžalováni	obžalovat	k5eAaPmNgMnP	obžalovat
a	a	k8xC	a
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
během	během	k7c2	během
výslechu	výslech	k1gInSc2	výslech
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
přiznali	přiznat	k5eAaPmAgMnP	přiznat
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
soudní	soudní	k2eAgFnPc1d1	soudní
spory	spora	k1gFnPc1	spora
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
trvaly	trvat	k5eAaImAgInP	trvat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
byl	být	k5eAaImAgInS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
na	na	k7c4	na
20	[number]	k4	20
měsíců	měsíc	k1gInPc2	měsíc
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
nepodmíněně	podmíněně	k6eNd1	podmíněně
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gNnSc3	on
a	a	k8xC	a
Janu	Jan	k1gMnSc3	Jan
Antalovi	Antal	k1gMnSc3	Antal
soud	soud	k1gInSc1	soud
zároveň	zároveň	k6eAd1	zároveň
udělil	udělit	k5eAaPmAgInS	udělit
zákaz	zákaz	k1gInSc1	zákaz
vykonávat	vykonávat	k5eAaImF	vykonávat
povolání	povolání	k1gNnSc4	povolání
duchovního	duchovní	k1gMnSc2	duchovní
a	a	k8xC	a
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
náboženské	náboženský	k2eAgFnSc2d1	náboženská
funkce	funkce	k1gFnSc2	funkce
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
všech	všecek	k3xTgMnPc2	všecek
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
propadly	propadnout	k5eAaPmAgFnP	propadnout
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
státu	stát	k1gInSc2	stát
všechny	všechen	k3xTgMnPc4	všechen
do	do	k7c2	do
úschovy	úschova	k1gFnSc2	úschova
vzaté	vzatý	k2eAgFnSc2d1	vzatá
písemnosti	písemnost	k1gFnSc2	písemnost
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
<g/>
,	,	kIx,	,
kázání	kázání	k1gNnSc6	kázání
<g/>
,	,	kIx,	,
knihy	kniha	k1gFnPc4	kniha
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Samuela	Samuel	k1gMnSc2	Samuel
Velebného	velebný	k2eAgMnSc2d1	velebný
<g/>
,	,	kIx,	,
Josefa	Josef	k1gMnSc2	Josef
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Kasanického	Kasanický	k2eAgMnSc2d1	Kasanický
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Antala	Antal	k1gMnSc2	Antal
zasedal	zasedat	k5eAaImAgInS	zasedat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
základě	základ	k1gInSc6	základ
odvolání	odvolání	k1gNnSc2	odvolání
krajského	krajský	k2eAgMnSc2d1	krajský
státního	státní	k2eAgMnSc2d1	státní
zástupce	zástupce	k1gMnSc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
dostali	dostat	k5eAaPmAgMnP	dostat
přísnější	přísný	k2eAgInSc4d2	přísnější
a	a	k8xC	a
definitivní	definitivní	k2eAgInSc4d1	definitivní
tresty	trest	k1gInPc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
dostal	dostat	k5eAaPmAgMnS	dostat
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
odnětí	odnětí	k1gNnSc4	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
zákazu	zákaz	k1gInSc2	zákaz
vykonávat	vykonávat	k5eAaImF	vykonávat
farářskou	farářský	k2eAgFnSc4d1	farářská
činnost	činnost	k1gFnSc4	činnost
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nezměněna	změněn	k2eNgFnSc1d1	nezměněna
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzení	odsouzený	k1gMnPc1	odsouzený
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
ve	v	k7c6	v
známé	známý	k2eAgFnSc6d1	známá
věznici	věznice	k1gFnSc6	věznice
ve	v	k7c6	v
Valdicích	Valdice	k1gFnPc6	Valdice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
komunistický	komunistický	k2eAgInSc4d1	komunistický
režim	režim	k1gInSc4	režim
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
koncentroval	koncentrovat	k5eAaBmAgInS	koncentrovat
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
odsouzené	odsouzená	k1gFnSc2	odsouzená
na	na	k7c6	na
principu	princip	k1gInSc6	princip
náboženské	náboženský	k2eAgFnSc2d1	náboženská
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
při	při	k7c6	při
broušení	broušení	k1gNnSc6	broušení
českého	český	k2eAgInSc2d1	český
křišťálu	křišťál	k1gInSc2	křišťál
a	a	k8xC	a
jiných	jiný	k2eAgFnPc6d1	jiná
pracích	práce	k1gFnPc6	práce
přežil	přežít	k5eAaPmAgMnS	přežít
21	[number]	k4	21
měsíců	měsíc	k1gInPc2	měsíc
až	až	k6eAd1	až
do	do	k7c2	do
propuštění	propuštění	k1gNnSc2	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
nízká	nízký	k2eAgFnSc1d1	nízká
sazba	sazba	k1gFnSc1	sazba
trestů	trest	k1gInPc2	trest
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
praxí	praxe	k1gFnSc7	praxe
odpuštění	odpuštění	k1gNnSc2	odpuštění
poloviny	polovina	k1gFnSc2	polovina
trestů	trest	k1gInPc2	trest
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dobrého	dobrý	k2eAgNnSc2d1	dobré
chování	chování	k1gNnSc2	chování
vězně	vězně	k6eAd1	vězně
napomohly	napomoct	k5eAaPmAgFnP	napomoct
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obviněné	obviněný	k1gMnPc4	obviněný
propustili	propustit	k5eAaPmAgMnP	propustit
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Samuela	Samuel	k1gMnSc2	Samuel
Velebného	velebný	k2eAgInSc2d1	velebný
to	ten	k3xDgNnSc4	ten
bylo	být	k5eAaImAgNnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Podmíněné	podmíněný	k2eAgNnSc1d1	podmíněné
propuštění	propuštění	k1gNnSc1	propuštění
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgInSc4d1	velebný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
nesměl	smět	k5eNaImAgMnS	smět
vykonávat	vykonávat	k5eAaImF	vykonávat
povolání	povolání	k1gNnSc4	povolání
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
připočteme	připočíst	k5eAaPmIp1nP	připočíst
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zákazu	zákaz	k1gInSc2	zákaz
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
i	i	k9	i
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
byl	být	k5eAaImAgMnS	být
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
zaměstnán	zaměstnán	k2eAgMnSc1d1	zaměstnán
jako	jako	k8xC	jako
stavební	stavební	k2eAgMnSc1d1	stavební
dělník	dělník	k1gMnSc1	dělník
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Stavoindustria	Stavoindustrium	k1gNnSc2	Stavoindustrium
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
budoval	budovat	k5eAaImAgMnS	budovat
provoz	provoz	k1gInSc4	provoz
chemického	chemický	k2eAgInSc2d1	chemický
závodu	závod	k1gInSc2	závod
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
Hnúšti	Hnúšti	k1gFnSc6	Hnúšti
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Přiměřeně	přiměřeně	k6eAd1	přiměřeně
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zapojil	zapojit	k5eAaPmAgInS	zapojit
i	i	k9	i
do	do	k7c2	do
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
církevním	církevní	k2eAgInSc6d1	církevní
sboru	sbor	k1gInSc6	sbor
Klenovec	Klenovec	k1gMnSc1	Klenovec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k9	jako
nedodržení	nedodržení	k1gNnSc1	nedodržení
podmínky	podmínka	k1gFnSc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
totiž	totiž	k9	totiž
jako	jako	k9	jako
kantor	kantor	k1gMnSc1	kantor
na	na	k7c6	na
službách	služba	k1gFnPc6	služba
Božích	boží	k2eAgFnPc2d1	boží
uprostřed	uprostřed	k7c2	uprostřed
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
i	i	k9	i
večerní	večerní	k2eAgInSc4d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
prosince	prosinec	k1gInSc2	prosinec
1966	[number]	k4	1966
dostal	dostat	k5eAaPmAgMnS	dostat
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgInSc4d1	velebný
státní	státní	k2eAgInSc4d1	státní
souhlas	souhlas	k1gInSc4	souhlas
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
farářského	farářský	k2eAgNnSc2d1	farářské
povolání	povolání	k1gNnSc2	povolání
a	a	k8xC	a
GBU	GBU	kA	GBU
mu	on	k3xPp3gMnSc3	on
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
Vyšné	vyšný	k2eAgFnSc6d1	Vyšná
Kamenici	Kamenice	k1gFnSc6	Kamenice
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
nadále	nadále	k6eAd1	nadále
po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
občas	občas	k6eAd1	občas
přicházel	přicházet	k5eAaImAgMnS	přicházet
agent	agent	k1gMnSc1	agent
StB	StB	k1gMnSc1	StB
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInPc1d1	soudní
procesy	proces	k1gInPc1	proces
s	s	k7c7	s
faráři	farář	k1gMnPc7	farář
se	se	k3xPyFc4	se
už	už	k6eAd1	už
však	však	k9	však
nekonaly	konat	k5eNaImAgFnP	konat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1968	[number]	k4	1968
požádal	požádat	k5eAaPmAgInS	požádat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
o	o	k7c4	o
rehabilitaci	rehabilitace	k1gFnSc4	rehabilitace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
nedočkal	dočkat	k5eNaPmAgMnS	dočkat
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
žádost	žádost	k1gFnSc4	žádost
na	na	k7c4	na
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
ten	ten	k3xDgMnSc1	ten
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
zamítl	zamítnout	k5eAaPmAgMnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
aktivní	aktivní	k2eAgFnSc4d1	aktivní
službu	služba	k1gFnSc4	služba
ukončil	ukončit	k5eAaPmAgMnS	ukončit
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
poslední	poslední	k2eAgFnSc4d1	poslední
listopadovou	listopadový	k2eAgFnSc4d1	listopadová
neděli	neděle	k1gFnSc4	neděle
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
a	a	k8xC	a
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
se	se	k3xPyFc4	se
s	s	k7c7	s
Vyšnou	vyšný	k2eAgFnSc7d1	Vyšná
Kamenicí	Kamenice	k1gFnSc7	Kamenice
po	po	k7c6	po
21	[number]	k4	21
letech	léto	k1gNnPc6	léto
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
roky	rok	k1gInPc4	rok
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
dostal	dostat	k5eAaPmAgMnS	dostat
oficiální	oficiální	k2eAgNnSc4d1	oficiální
poděkování	poděkování	k1gNnSc4	poděkování
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
evangelického	evangelický	k2eAgMnSc2d1	evangelický
biskupa	biskup	k1gMnSc2	biskup
Uhorskaia	Uhorskaia	k1gFnSc1	Uhorskaia
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
statečně	statečně	k6eAd1	statečně
trpěl	trpět	k5eAaImAgMnS	trpět
pro	pro	k7c4	pro
Pána	pán	k1gMnSc4	pán
Boha	bůh	k1gMnSc4	bůh
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
církev	církev	k1gFnSc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Velebný	velebný	k2eAgMnSc1d1	velebný
zemřel	zemřít	k5eAaPmAgMnS	zemřít
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
dovršením	dovršení	k1gNnSc7	dovršení
73	[number]	k4	73
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Kdo	kdo	k3yQnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
</s>
</p>
<p>
<s>
Evanjelici	Evanjelik	k1gMnPc1	Evanjelik
v	v	k7c6	v
dejinách	dejina	k1gFnPc6	dejina
slovenskej	slovenskej	k?	slovenskej
kultúry	kultúra	k1gFnSc2	kultúra
</s>
</p>
<p>
<s>
J.G.	J.G.	k?	J.G.
Farár	Farár	k1gMnSc1	Farár
Juráš	Juráš	k1gMnSc1	Juráš
a	a	k8xC	a
"	"	kIx"	"
<g/>
KVAP	kvap	k1gInSc1	kvap
<g/>
"	"	kIx"	"
ve	v	k7c6	v
Východoslovenských	východoslovenský	k2eAgFnPc6d1	Východoslovenská
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
augusta	august	k1gMnSc4	august
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
ANTALOVÁ	Antalová	k1gFnSc1	Antalová
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prednej	prednat	k5eAaImRp2nS	prednat
strany	strana	k1gFnSc2	strana
mreží	mreží	k1gNnSc1	mreží
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Matica	Matica	k1gFnSc1	Matica
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7090	[number]	k4	7090
<g/>
-	-	kIx~	-
<g/>
890	[number]	k4	890
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Froňková	Froňková	k1gFnSc1	Froňková
<g/>
,	,	kIx,	,
Viola	Viola	k1gFnSc1	Viola
<g/>
.	.	kIx.	.
</s>
<s>
SAMUEL	Samuel	k1gMnSc1	Samuel
VELEBNÝ-	VELEBNÝ-	k1gMnSc1	VELEBNÝ-
obhajca	obhajca	k1gMnSc1	obhajca
Kristovej	Kristovej	k?	Kristovej
pravdy	pravda	k1gFnSc2	pravda
v	v	k7c4	v
Evanjelický	Evanjelický	k2eAgInSc4d1	Evanjelický
Posol	posolit	k5eAaPmRp2nS	posolit
Spod	spod	k6eAd1	spod
Tatier	Tatira	k1gFnPc2	Tatira
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.18	.18	k4	.18
</s>
</p>
<p>
<s>
Froňková	Froňková	k1gFnSc1	Froňková
<g/>
,	,	kIx,	,
Viola	Viola	k1gFnSc1	Viola
<g/>
.	.	kIx.	.
</s>
<s>
Pre	Pre	k?	Pre
všetkých	všetká	k1gFnPc2	všetká
v	v	k7c4	v
rodine	rodinout	k5eAaPmIp3nS	rodinout
bol	bol	k1gInSc1	bol
láskavým	láskavý	k2eAgInSc7d1	láskavý
a	a	k8xC	a
vzácnym	vzácnym	k?	vzácnym
človekom	človekom	k1gInSc1	človekom
v	v	k7c4	v
Evanjelický	Evanjelický	k2eAgInSc4d1	Evanjelický
Posol	posolit	k5eAaPmRp2nS	posolit
Spod	spod	k6eAd1	spod
Tatier	Tatira	k1gFnPc2	Tatira
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.19	.19	k4	.19
</s>
</p>
<p>
<s>
Havíra	Havír	k1gMnSc4	Havír
<g/>
,	,	kIx,	,
Ján	Ján	k1gMnSc1	Ján
<g/>
.	.	kIx.	.
</s>
<s>
Novodobé	novodobý	k2eAgFnPc1d1	novodobá
dejinné	dejinný	k2eAgFnPc1d1	dejinný
križovatky	križovatka	k1gFnPc1	križovatka
našej	našej	k1gInSc1	našej
cirkvi	cirkev	k1gFnSc6	cirkev
v	v	k7c4	v
Evanjelický	Evanjelický	k2eAgInSc4d1	Evanjelický
Posol	posolit	k5eAaPmRp2nS	posolit
Spod	spod	k1gInSc4	spod
Tatier	Tatier	k1gMnSc1	Tatier
<g/>
,	,	kIx,	,
č.	č.	k?	č.
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.44	.44	k4	.44
</s>
</p>
<p>
<s>
Zajden	Zajdno	k1gNnPc2	Zajdno
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
DVE	DVE	kA	DVE
SMUTNÉ	Smutné	k2eAgFnSc1d1	Smutné
výročia	výročia	k1gFnSc1	výročia
(	(	kIx(	(
<g/>
I.	I.	kA	I.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evangelický	evangelický	k2eAgInSc1d1	evangelický
Posel	posít	k5eAaPmAgInS	posít
Spod	spod	k1gInSc1	spod
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.8	.8	k4	.8
</s>
</p>
<p>
<s>
Zajden	Zajdno	k1gNnPc2	Zajdno
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
DVE	DVE	kA	DVE
SMUTNÉ	Smutné	k2eAgFnSc1d1	Smutné
výročia	výročia	k1gFnSc1	výročia
(	(	kIx(	(
<g/>
II	II	kA	II
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evangelický	evangelický	k2eAgInSc1d1	evangelický
Posel	posít	k5eAaPmAgInS	posít
Spod	spod	k1gInSc1	spod
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
23	[number]	k4	23
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.8	.8	k4	.8
</s>
</p>
<p>
<s>
Zajden	Zajdno	k1gNnPc2	Zajdno
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
DVE	DVE	kA	DVE
SMUTNÉ	Smutné	k2eAgFnSc1d1	Smutné
výročia	výročia	k1gFnSc1	výročia
(	(	kIx(	(
<g/>
III	III	kA	III
<g/>
.	.	kIx.	.
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evangelický	evangelický	k2eAgInSc1d1	evangelický
Posel	posít	k5eAaPmAgInS	posít
Spod	spod	k1gInSc1	spod
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
květen	květen	k1gInSc4	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.8	.8	k4	.8
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
https://web.archive.org/web/20170112075238/http://banskabystrica.kniznice.net/katalog/clpr54.htm	[url]	k6eAd1	https://web.archive.org/web/20170112075238/http://banskabystrica.kniznice.net/katalog/clpr54.htm
</s>
</p>
