<s>
V	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejvíce	hodně	k6eAd3	hodně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc1d1	moderní
standardní	standardní	k2eAgFnSc1d1	standardní
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
hovorovým	hovorový	k2eAgInSc7d1	hovorový
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zřídka	zřídka	k6eAd1	zřídka
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k1gNnPc7	další
hojně	hojně	k6eAd1	hojně
používanými	používaný	k2eAgMnPc7d1	používaný
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
jazyky	jazyk	k1gInPc7	jazyk
se	se	k3xPyFc4	se
především	především	k6eAd1	především
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
turismu	turismus	k1gInSc2	turismus
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Arabština	arabština	k1gFnSc1	arabština
se	se	k3xPyFc4	se
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
jazykem	jazyk	k1gInSc7	jazyk
Egypťanů	Egypťan	k1gMnPc2	Egypťan
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
variant	varianta	k1gFnPc2	varianta
arabštiny	arabština	k1gFnSc2	arabština
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
egyptská	egyptský	k2eAgFnSc1d1	egyptská
nejvíce	hodně	k6eAd3	hodně
srozumitelnou	srozumitelný	k2eAgFnSc4d1	srozumitelná
pro	pro	k7c4	pro
ostatní	ostatní	k1gNnSc4	ostatní
arabsky	arabsky	k6eAd1	arabsky
mluvící	mluvící	k2eAgFnSc2d1	mluvící
země	zem	k1gFnSc2	zem
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
Středního	střední	k2eAgInSc2d1	střední
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobným	pravděpodobný	k2eAgInSc7d1	pravděpodobný
důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
vliv	vliv	k1gInSc1	vliv
egyptské	egyptský	k2eAgFnSc2d1	egyptská
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
arabských	arabský	k2eAgFnPc6d1	arabská
zemích	zem	k1gFnPc6	zem
velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Arabská	arabský	k2eAgFnSc1d1	arabská
beduínská	beduínský	k2eAgFnSc1d1	beduínská
menšina	menšina	k1gFnSc1	menšina
žijící	žijící	k2eAgFnSc1d1	žijící
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
Sinajském	sinajský	k2eAgInSc6d1	sinajský
poloostrově	poloostrov	k1gInSc6	poloostrov
hovoří	hovořit	k5eAaImIp3nS	hovořit
beduínskou	beduínský	k2eAgFnSc7d1	beduínská
arabštinou	arabština	k1gFnSc7	arabština
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
súdánská	súdánský	k2eAgFnSc1d1	súdánská
menšina	menšina	k1gFnSc1	menšina
hovoří	hovořit	k5eAaImIp3nS	hovořit
súdánskou	súdánský	k2eAgFnSc7d1	súdánská
arabštinou	arabština	k1gFnSc7	arabština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
údolí	údolí	k1gNnSc2	údolí
Nilu	Nil	k1gInSc2	Nil
v	v	k7c6	v
Horním	horní	k2eAgInSc6d1	horní
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
měst	město	k1gNnPc2	město
Kom	kdo	k3yInSc6	kdo
Ombo	Ombo	k1gNnSc1	Ombo
a	a	k8xC	a
Asuánu	Asuán	k1gInSc2	Asuán
<g/>
,	,	kIx,	,
hovoří	hovořit	k5eAaImIp3nS	hovořit
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
núbijskými	núbijský	k2eAgInPc7d1	núbijský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
jazyky	jazyk	k1gInPc1	jazyk
Nobbin	Nobbina	k1gFnPc2	Nobbina
a	a	k8xC	a
Kenuzi-Dongola	Kenuzi-Dongola	k1gFnSc1	Kenuzi-Dongola
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Súdánu	Súdán	k1gInSc2	Súdán
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Egyptština	egyptština	k1gFnSc1	egyptština
a	a	k8xC	a
Koptština	koptština	k1gFnSc1	koptština
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
jazyky	jazyk	k1gInPc7	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yIgNnPc7	který
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Egypta	Egypt	k1gInSc2	Egypt
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
starověká	starověký	k2eAgFnSc1d1	starověká
egyptština	egyptština	k1gFnSc1	egyptština
a	a	k8xC	a
koptština	koptština	k1gFnSc1	koptština
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jazyky	jazyk	k1gInPc1	jazyk
tvořily	tvořit	k5eAaImAgInP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
větev	větev	k1gFnSc4	větev
mezi	mezi	k7c7	mezi
rodinami	rodina	k1gFnPc7	rodina
afroasijských	afroasijský	k2eAgMnPc2d1	afroasijský
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Egyptský	egyptský	k2eAgInSc1d1	egyptský
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
psaných	psaný	k2eAgInPc2d1	psaný
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
z	z	k7c2	z
hieroglyfických	hieroglyfický	k2eAgInPc2d1	hieroglyfický
zápisů	zápis	k1gInPc2	zápis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
ve	v	k7c6	v
staroegyptských	staroegyptský	k2eAgInPc6d1	staroegyptský
chrámech	chrám	k1gInPc6	chrám
a	a	k8xC	a
na	na	k7c6	na
svitcích	svitek	k1gInPc6	svitek
papyru	papyr	k1gInSc2	papyr
<g/>
.	.	kIx.	.
</s>
<s>
Koptština	koptština	k1gFnSc1	koptština
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
existující	existující	k2eAgMnSc1d1	existující
potomek	potomek	k1gMnSc1	potomek
staroegyptštiny	staroegyptština	k1gFnSc2	staroegyptština
a	a	k8xC	a
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
liturgický	liturgický	k2eAgInSc1d1	liturgický
jazyk	jazyk	k1gInSc1	jazyk
koptské	koptský	k2eAgFnSc2d1	koptská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Egypta	Egypt	k1gInSc2	Egypt
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
starořecký	starořecký	k2eAgInSc4d1	starořecký
dialekt	dialekt	k1gInSc4	dialekt
koiné	koiné	k1gFnSc2	koiné
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
především	především	k6eAd1	především
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaImNgInS	využívat
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
filozofických	filozofický	k2eAgFnPc6d1	filozofická
a	a	k8xC	a
vědeckých	vědecký	k2eAgNnPc6d1	vědecké
odvětvích	odvětví	k1gNnPc6	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
77	[number]	k4	77
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Arabské	arabský	k2eAgFnSc2d1	arabská
pouště	poušť	k1gFnSc2	poušť
a	a	k8xC	a
pobřeží	pobřeží	k1gNnSc2	pobřeží
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
mluví	mluvit	k5eAaImIp3nS	mluvit
jazykem	jazyk	k1gInSc7	jazyk
bedža	bedžum	k1gNnSc2	bedžum
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Káhiry	Káhira	k1gFnSc2	Káhira
a	a	k8xC	a
v	v	k7c6	v
Luxoru	Luxor	k1gInSc6	Luxor
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jazyk	jazyk	k1gInSc1	jazyk
domari	domar	k1gFnSc2	domar
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
indoárijský	indoárijský	k2eAgInSc1d1	indoárijský
jazyk	jazyk	k1gInSc1	jazyk
podobný	podobný	k2eAgInSc1d1	podobný
romštině	romština	k1gFnSc3	romština
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
dialektem	dialekt	k1gInSc7	dialekt
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
234	[number]	k4	234
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oáze	oáza	k1gFnSc6	oáza
Síwa	Síw	k1gInSc2	Síw
na	na	k7c6	na
západě	západ	k1gInSc6	západ
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
berberské	berberský	k2eAgInPc1d1	berberský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
zde	zde	k6eAd1	zde
mluví	mluvit	k5eAaImIp3nS	mluvit
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Languages	Languages	k1gInSc1	Languages
of	of	k?	of
Egypt	Egypt	k1gInSc1	Egypt
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
