<s>
Cha-cha-cha	Chahaha	k1gFnSc1	Cha-cha-cha
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ča-ča-ča	ča-ča-ča	k6eAd1	ča-ča-ča
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Cha-cha	Chaha	k1gFnSc1	Cha-cha
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
čača	čača	k1gFnSc1	čača
nebo	nebo	k8xC	nebo
ča-ča	ča-ča	k1gNnSc1	ča-ča
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
svižný	svižný	k2eAgInSc1d1	svižný
latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
tanec	tanec	k1gInSc1	tanec
pocházející	pocházející	k2eAgInSc1d1	pocházející
z	z	k7c2	z
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Rytmus	rytmus	k1gInSc1	rytmus
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
kubánského	kubánský	k2eAgInSc2d1	kubánský
tance	tanec	k1gInSc2	tanec
Danzon	Danzon	k1gNnSc1	Danzon
synkopováním	synkopování	k1gNnSc7	synkopování
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Cha-cha-cha	Chahaha	k1gMnSc1	Cha-cha-cha
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
zjednodušené	zjednodušený	k2eAgFnSc6d1	zjednodušená
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
také	také	k9	také
v	v	k7c6	v
tanečních	taneční	k2eAgInPc6d1	taneční
kurzech	kurz	k1gInPc6	kurz
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
i	i	k8xC	i
většina	většina	k1gFnSc1	většina
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
figur	figura	k1gFnPc2	figura
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
partnery	partner	k1gMnPc4	partner
(	(	kIx(	(
<g/>
zrcadlově	zrcadlově	k6eAd1	zrcadlově
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
tanec	tanec	k1gInSc4	tanec
cha-cha-cha	chahaha	k1gFnSc1	cha-cha-cha
přeměna	přeměna	k1gFnSc1	přeměna
na	na	k7c4	na
synkopovanou	synkopovaný	k2eAgFnSc4d1	synkopovaná
dobu	doba	k1gFnSc4	doba
4	[number]	k4	4
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
v	v	k7c6	v
rytmizaci	rytmizace	k1gFnSc6	rytmizace
4	[number]	k4	4
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
zní	znět	k5eAaImIp3nS	znět
výrazný	výrazný	k2eAgInSc1d1	výrazný
zvuk	zvuk	k1gInSc1	zvuk
kubánského	kubánský	k2eAgInSc2d1	kubánský
hudebního	hudební	k2eAgInSc2d1	hudební
nástroje	nástroj	k1gInSc2	nástroj
giro	giro	k1gMnSc1	giro
<g/>
.	.	kIx.	.
</s>
<s>
Cha-Cha	Cha-Cha	k1gFnSc1	Cha-Cha
se	se	k3xPyFc4	se
tancuje	tancovat	k5eAaImIp3nS	tancovat
jak	jak	k6eAd1	jak
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
latinskou	latinský	k2eAgFnSc4d1	Latinská
hudbu	hudba	k1gFnSc4	hudba
nebo	nebo	k8xC	nebo
na	na	k7c6	na
latin	latina	k1gFnPc2	latina
rock	rock	k1gInSc1	rock
či	či	k8xC	či
latin	latina	k1gFnPc2	latina
pop	pop	k1gMnSc1	pop
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
taktu	takt	k1gInSc6	takt
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
tří	tři	k4xCgInPc2	tři
pomalých	pomalý	k2eAgInPc2d1	pomalý
kroků	krok	k1gInPc2	krok
a	a	k8xC	a
dvou	dva	k4xCgInPc2	dva
rychlých	rychlý	k2eAgInPc2d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
cha-cha	chaha	k1gFnSc1	cha-cha
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
podle	podle	k7c2	podle
zvuku	zvuk	k1gInSc2	zvuk
zvonku	zvonek	k1gInSc2	zvonek
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
hrávalo	hrávat	k5eAaImAgNnS	hrávat
při	při	k7c6	při
tradičních	tradiční	k2eAgInPc6d1	tradiční
tancích	tanec	k1gInPc6	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Melodie	melodie	k1gFnSc1	melodie
cha-chi	chahi	k1gNnSc2	cha-chi
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
především	především	k9	především
z	z	k7c2	z
mamba	mamb	k1gMnSc2	mamb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
mambo	mamba	k1gFnSc5	mamba
v	v	k7c6	v
USA	USA	kA	USA
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnSc1d1	populární
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
náročné	náročný	k2eAgNnSc1d1	náročné
kvůli	kvůli	k7c3	kvůli
svojí	svůj	k3xOyFgFnSc3	svůj
rychlosti	rychlost	k1gFnSc3	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
hrát	hrát	k5eAaImF	hrát
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
cha-cha	chaha	k1gFnSc1	cha-cha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
hudebník	hudebník	k1gMnSc1	hudebník
Enrique	Enriqu	k1gInSc2	Enriqu
Jorrín	Jorrín	k1gInSc1	Jorrín
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
orchestr	orchestr	k1gInSc1	orchestr
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
havanské	havanský	k2eAgFnSc6d1	Havanská
kavárně	kavárna	k1gFnSc6	kavárna
Prado	Prado	k1gNnSc1	Prado
di	di	k?	di
Neptune	Neptun	k1gMnSc5	Neptun
<g/>
,	,	kIx,	,
představil	představit	k5eAaPmAgInS	představit
tento	tento	k3xDgInSc1	tento
zpomalený	zpomalený	k2eAgInSc1d1	zpomalený
rytmus	rytmus	k1gInSc1	rytmus
pod	pod	k7c7	pod
dnešním	dnešní	k2eAgInSc7d1	dnešní
názvem	název	k1gInSc7	název
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
melodie	melodie	k1gFnPc1	melodie
La	la	k1gNnSc2	la
Engañ	Engañ	k1gFnPc2	Engañ
a	a	k8xC	a
Silver	Silvra	k1gFnPc2	Silvra
Star	star	k1gFnSc2	star
staly	stát	k5eAaPmAgInP	stát
hity	hit	k1gInPc1	hit
<g/>
.	.	kIx.	.
</s>
<s>
Figury	figura	k1gFnPc1	figura
podle	podle	k7c2	podle
ČSTS	ČSTS	kA	ČSTS
<g/>
:	:	kIx,	:
Základní	základní	k2eAgInSc1d1	základní
pohyb	pohyb	k1gInSc1	pohyb
(	(	kIx(	(
<g/>
basic	basic	k1gMnSc1	basic
Movement	Movement	k1gMnSc1	Movement
<g/>
)	)	kIx)	)
zavřený	zavřený	k2eAgMnSc1d1	zavřený
(	(	kIx(	(
<g/>
Close	Close	k1gFnPc1	Close
<g/>
)	)	kIx)	)
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
In	In	k1gMnSc1	In
Place	plac	k1gInSc6	plac
<g/>
)	)	kIx)	)
stranou	stranou	k6eAd1	stranou
(	(	kIx(	(
<g/>
Side	Side	k1gInSc1	Side
Basic	Basic	kA	Basic
<g/>
)	)	kIx)	)
otevřený	otevřený	k2eAgInSc4d1	otevřený
(	(	kIx(	(
<g/>
Open	Open	k1gInSc4	Open
Basic	Basic	kA	Basic
<g/>
)	)	kIx)	)
Rytmický	rytmický	k2eAgInSc1d1	rytmický
krok	krok	k1gInSc1	krok
(	(	kIx(	(
<g/>
Time	Time	k1gInSc1	Time
Step	step	k1gInSc1	step
<g/>
)	)	kIx)	)
Otáčky	otáčka	k1gFnSc2	otáčka
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
Spot	spot	k1gInSc1	spot
Turns	Turns	k1gInSc1	Turns
<g/>
)	)	kIx)	)
Podtáčky	Podtáček	k1gInPc1	Podtáček
pod	pod	k7c7	pod
rukou	ruka	k1gFnSc7	ruka
(	(	kIx(	(
<g/>
Under	Under	k1gInSc1	Under
Arm	Arm	k1gFnSc2	Arm
Turns	Turnsa	k1gFnPc2	Turnsa
<g/>
)	)	kIx)	)
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
Ruka	ruka	k1gFnSc1	ruka
k	k	k7c3	k
ruce	ruka	k1gFnSc3	ruka
(	(	kIx(	(
<g/>
Hand	Hand	k1gMnSc1	Hand
to	ten	k3xDgNnSc4	ten
Hand	Hand	k1gMnSc1	Hand
<g/>
)	)	kIx)	)
Tři	tři	k4xCgFnPc1	tři
čači	čača	k1gFnPc1	čača
(	(	kIx(	(
<g/>
Three	Thre	k1gFnPc1	Thre
Cha	cha	k0	cha
Cha	cha	k0	cha
Chas	chasa	k1gFnPc2	chasa
<g/>
)	)	kIx)	)
Křížený	křížený	k2eAgInSc1d1	křížený
základní	základní	k2eAgInSc1d1	základní
krok	krok	k1gInSc1	krok
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Cross	Cross	k1gInSc1	Cross
Basic	Basic	kA	Basic
<g/>
)	)	kIx)	)
Vějíř	vějíř	k1gInSc4	vějíř
(	(	kIx(	(
<g/>
Fan	Fana	k1gFnPc2	Fana
<g/>
)	)	kIx)	)
Alemana	Aleman	k1gMnSc2	Aleman
(	(	kIx(	(
<g/>
Alemana	Aleman	k1gMnSc2	Aleman
<g/>
)	)	kIx)	)
Hokejka	hokejka	k1gFnSc1	hokejka
(	(	kIx(	(
<g/>
Hockey	Hockey	k1gInPc1	Hockey
Stick	Sticka	k1gFnPc2	Sticka
<g/>
)	)	kIx)	)
Káča	Káča	k1gFnSc1	Káča
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
Natural	Natural	k?	Natural
Top	topit	k5eAaImRp2nS	topit
<g/>
)	)	kIx)	)
Otevření	otevřený	k2eAgMnPc1d1	otevřený
ven	ven	k6eAd1	ven
vpravo	vpravo	k6eAd1	vpravo
(	(	kIx(	(
<g/>
Opening	Opening	k1gInSc1	Opening
Out	Out	k1gFnSc2	Out
to	ten	k3xDgNnSc1	ten
Right	Right	k2eAgMnSc1d1	Right
<g/>
)	)	kIx)	)
Švihy	švih	k1gInPc1	švih
kyčlí	kyčel	k1gFnPc2	kyčel
(	(	kIx(	(
<g/>
Hip	hip	k0	hip
Twists	Twistsa	k1gFnPc2	Twistsa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
zavřený	zavřený	k2eAgInSc4d1	zavřený
(	(	kIx(	(
<g/>
Close	Close	k1gFnSc2	Close
<g/>
)	)	kIx)	)
otevřený	otevřený	k2eAgInSc4d1	otevřený
(	(	kIx(	(
<g/>
Open	Open	k1gInSc4	Open
<g/>
)	)	kIx)	)
pokročilý	pokročilý	k2eAgInSc4d1	pokročilý
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc4	Advanced
<g/>
)	)	kIx)	)
spirálový	spirálový	k2eAgMnSc1d1	spirálový
(	(	kIx(	(
<g/>
Spiral	Spiral	k1gFnPc1	Spiral
<g/>
)	)	kIx)	)
Kubánská	kubánský	k2eAgNnPc1d1	kubánské
přerušení	přerušení	k1gNnPc1	přerušení
(	(	kIx(	(
<g/>
Cuban	Cuban	k1gInSc1	Cuban
Breaks	Breaks	k1gInSc1	Breaks
<g/>
)	)	kIx)	)
Rameno	rameno	k1gNnSc1	rameno
k	k	k7c3	k
rameni	rameno	k1gNnSc3	rameno
(	(	kIx(	(
<g/>
Shoulder	Shoulder	k1gInSc4	Shoulder
to	ten	k3xDgNnSc4	ten
Shoulder	Shoulder	k1gMnSc1	Shoulder
<g/>
)	)	kIx)	)
Turecký	turecký	k2eAgInSc1d1	turecký
ručník	ručník	k1gInSc1	ručník
(	(	kIx(	(
<g/>
Turkish	Turkish	k1gMnSc1	Turkish
Towel	Towel	k1gMnSc1	Towel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Metody	metoda	k1gFnPc1	metoda
změny	změna	k1gFnSc2	změna
chodidla	chodidlo	k1gNnSc2	chodidlo
(	(	kIx(	(
<g/>
Methods	Methods	k1gInSc1	Methods
od	od	k7c2	od
Changing	Changing	k1gInSc4	Changing
Feet	Feet	k1gInSc1	Feet
<g/>
)	)	kIx)	)
Káča	Káča	k1gFnSc1	Káča
vlevo	vlevo	k6eAd1	vlevo
(	(	kIx(	(
<g/>
reverse	reverse	k1gFnSc1	reverse
Turn	Turn	k1gMnSc1	Turn
<g/>
)	)	kIx)	)
Spirála	spirála	k1gFnSc1	spirála
(	(	kIx(	(
<g/>
Spiral	Spiral	k1gFnSc1	Spiral
<g/>
)	)	kIx)	)
Laso	laso	k1gNnSc1	laso
(	(	kIx(	(
<g/>
Rope	Rope	k1gInSc1	Rope
Spinning	Spinning	k1gInSc1	Spinning
<g/>
)	)	kIx)	)
Miláček	miláček	k1gMnSc1	miláček
(	(	kIx(	(
<g/>
Sweetheart	Sweethearta	k1gFnPc2	Sweethearta
<g/>
)	)	kIx)	)
Následuj	následovat	k5eAaImRp2nS	následovat
mě	já	k3xPp1nSc2	já
(	(	kIx(	(
<g/>
Follow	Follow	k1gMnSc1	Follow
My	my	k3xPp1nPc1	my
Leader	leader	k1gMnSc1	leader
<g/>
)	)	kIx)	)
Kadeř	kadeř	k1gFnSc1	kadeř
(	(	kIx(	(
<g/>
Curl	Curl	k1gInSc1	Curl
<g/>
)	)	kIx)	)
Aida	Aida	k1gFnSc1	Aida
(	(	kIx(	(
<g/>
Aida	Aida	k1gFnSc1	Aida
<g/>
)	)	kIx)	)
Otevření	otevření	k1gNnSc1	otevření
ven	ven	k6eAd1	ven
z	z	k7c2	z
káči	káča	k1gFnSc2	káča
vlevo	vlevo	k6eAd1	vlevo
(	(	kIx(	(
<g/>
Opening	Opening	k1gInSc1	Opening
Out	Out	k1gFnSc2	Out
from	from	k6eAd1	from
Reverse	reverse	k1gFnSc1	reverse
Top	topit	k5eAaImRp2nS	topit
<g/>
)	)	kIx)	)
Guapacha	Guapacha	k1gMnSc1	Guapacha
</s>
