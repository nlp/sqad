<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
(	(	kIx(
<g/>
Prostějov	Prostějov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Sloh	sloha	k1gFnPc2
</s>
<s>
secese	secese	k1gFnSc1
Architekti	architekt	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
Tranquillini	Tranquillin	k2eAgMnPc1d1
(	(	kIx(
<g/>
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Novák	Novák	k1gMnSc1
(	(	kIx(
<g/>
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Bohuslav	Bohuslav	k1gMnSc1
Fuchs	Fuchs	k1gMnSc1
(	(	kIx(
<g/>
rekonstrukce	rekonstrukce	k1gFnSc1
<g/>
)	)	kIx)
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Vojáčkovo	Vojáčkův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
218	#num#	k4
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
796	#num#	k4
01	#num#	k4
Prostějov	Prostějov	k1gInSc1
<g/>
,	,	kIx,
Prostějov	Prostějov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Vojáčkovo	Vojáčkův	k2eAgNnSc1d1
nám.	nám.	k?
Souřadnice	souřadnice	k1gFnPc4
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
24,85	24,85	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
52,71	52,71	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
13874	#num#	k4
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5717	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
je	být	k5eAaImIp3nS
secesní	secesní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1905	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
podle	podle	k7c2
plánů	plán	k1gInPc2
architekta	architekt	k1gMnSc2
Jana	Jan	k1gMnSc2
Kotěry	Kotěra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vedení	vedení	k1gNnSc1
města	město	k1gNnSc2
Prostějova	Prostějov	k1gInSc2
uvažovalo	uvažovat	k5eAaImAgNnS
od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
o	o	k7c6
stavbě	stavba	k1gFnSc6
společenského	společenský	k2eAgNnSc2d1
a	a	k8xC
kulturního	kulturní	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
dílo	dílo	k1gNnSc1
bylo	být	k5eAaImAgNnS
dovedeno	dovést	k5eAaPmNgNnS
do	do	k7c2
konce	konec	k1gInSc2
<g/>
,	,	kIx,
přispěl	přispět	k5eAaPmAgMnS
významný	významný	k2eAgInSc4d1
finanční	finanční	k2eAgInSc4d1
odkaz	odkaz	k1gInSc4
zesnulého	zesnulý	k2eAgMnSc2d1
starosty	starosta	k1gMnSc2
Karla	Karel	k1gMnSc2
Vojáčka	Vojáček	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
byl	být	k5eAaImAgMnS
osloven	osloven	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
působil	působit	k5eAaImAgMnS
jako	jako	k9
profesor	profesor	k1gMnSc1
na	na	k7c6
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1905	#num#	k4
předložil	předložit	k5eAaPmAgMnS
Kotěra	Kotěra	k1gFnSc1
první	první	k4xOgInPc4
plány	plán	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
schválena	schválit	k5eAaPmNgFnS
a	a	k8xC
započata	započnout	k5eAaPmNgFnS
v	v	k7c6
květnu	květen	k1gInSc6
1906	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
1907	#num#	k4
byl	být	k5eAaImAgInS
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
slavnostně	slavnostně	k6eAd1
otevřený	otevřený	k2eAgInSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
obsahoval	obsahovat	k5eAaImAgInS
divadelní	divadelní	k2eAgInPc4d1
prostory	prostor	k1gInPc4
<g/>
,	,	kIx,
přednáškové	přednáškový	k2eAgInPc4d1
sály	sál	k1gInPc4
<g/>
,	,	kIx,
restauraci	restaurace	k1gFnSc4
a	a	k8xC
kavárnu	kavárna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
výzdobě	výzdoba	k1gFnSc6
se	se	k3xPyFc4
podílela	podílet	k5eAaImAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
významných	významný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
mj.	mj.	kA
Stanislav	Stanislav	k1gMnSc1
Sucharda	Sucharda	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
Bohumil	Bohumil	k1gMnSc1
Kafka	Kafka	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kysela	Kysela	k1gMnSc1
nebo	nebo	k8xC
Jan	Jan	k1gMnSc1
Preisler	Preisler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Stavbu	stavba	k1gFnSc4
doplňuje	doplňovat	k5eAaImIp3nS
pomník	pomník	k1gInSc1
manželů	manžel	k1gMnPc2
Vojáčkových	Vojáčkových	k2eAgMnPc2d1
z	z	k7c2
roku	rok	k1gInSc2
1910	#num#	k4
na	na	k7c6
prostranství	prostranství	k1gNnSc6
před	před	k7c7
východním	východní	k2eAgNnSc7d1
průčelím	průčelí	k1gNnSc7
<g/>
,	,	kIx,
dnešním	dnešní	k2eAgNnSc6d1
Vojáčkově	Vojáčkův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
;	;	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
rovněž	rovněž	k9
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
s	s	k7c7
využitím	využití	k1gNnSc7
reliéfů	reliéf	k1gInPc2
Bohumila	Bohumil	k1gMnSc2
Kafky	Kafka	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
necitlivě	citlivě	k6eNd1
přestavěn	přestavět	k5eAaPmNgInS
brněnským	brněnský	k2eAgMnSc7d1
architektem	architekt	k1gMnSc7
Emilem	Emil	k1gMnSc7
Tranquillinim	Tranquillinim	k1gInSc4
<g/>
,	,	kIx,
profesorem	profesor	k1gMnSc7
na	na	k7c6
Německé	německý	k2eAgFnSc6d1
vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
technické	technický	k2eAgFnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
této	tento	k3xDgFnSc2
přestavby	přestavba	k1gFnSc2
byla	být	k5eAaImAgFnS
odstraněna	odstranit	k5eAaPmNgFnS
většina	většina	k1gFnSc1
secesních	secesní	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
léty	léto	k1gNnPc7
1956	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
první	první	k4xOgFnSc1
vlna	vlna	k1gFnSc1
rekonstrukce	rekonstrukce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
byly	být	k5eAaImAgFnP
započaty	započnout	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
Program	program	k1gInSc1
záchrany	záchrana	k1gFnSc2
kulturní	kulturní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
Národního	národní	k2eAgInSc2d1
domu	dům	k1gInSc2
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnPc1d1
změny	změna	k1gFnPc1
proběhly	proběhnout	k5eAaPmAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
2006	#num#	k4
a	a	k8xC
2007	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
dokončeny	dokončit	k5eAaPmNgFnP
opravy	oprava	k1gFnPc1
restaurace	restaurace	k1gFnSc2
a	a	k8xC
divadla	divadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
provozovatelem	provozovatel	k1gMnSc7
Národního	národní	k2eAgInSc2d1
domu	dům	k1gInSc2
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
o.	o.	k?
p.	p.	k?
s.	s.	k?
Ročně	ročně	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
koná	konat	k5eAaImIp3nS
250	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
kulturních	kulturní	k2eAgFnPc2d1
a	a	k8xC
společenských	společenský	k2eAgFnPc2d1
akcí	akce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
národní	národní	k2eAgFnSc7d1
kulturní	kulturní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Panorama	panorama	k1gNnSc1
</s>
<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
průčelí	průčelí	k1gNnSc4
do	do	k7c2
Vojáčkova	Vojáčkův	k2eAgNnSc2d1
náměstí	náměstí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
vlevo	vlevo	k6eAd1
část	část	k1gFnSc1
pomníku	pomník	k1gInSc2
manželům	manžel	k1gMnPc3
Vojáčkovým	Vojáčkův	k2eAgMnSc7d1
od	od	k7c2
stejných	stejný	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CHYTIL	Chytil	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
Prostějova	Prostějov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
110	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
O	o	k7c6
Národním	národní	k2eAgInSc6d1
domě	dům	k1gInSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
21	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BARTKOVÁ	BARTKOVÁ	kA
<g/>
,	,	kIx,
Hana	Hana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
sto	sto	k4xCgNnSc4
lety	léto	k1gNnPc7
byly	být	k5eAaImAgInP
položeny	položen	k2eAgInPc1d1
základy	základ	k1gInPc1
ke	k	k7c3
stavbě	stavba	k1gFnSc3
Národního	národní	k2eAgInSc2d1
domu	dům	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostějovský	prostějovský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
16	#num#	k4
<g/>
(	(	kIx(
<g/>
24	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostějovský	prostějovský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
138	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1801	#num#	k4
<g/>
-	-	kIx~
<g/>
9781	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DOLÍVKA	dolívka	k1gFnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Kotěra	Kotěra	k1gFnSc1
udělal	udělat	k5eAaPmAgInS
z	z	k7c2
Národního	národní	k2eAgInSc2d1
domu	dům	k1gInSc2
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
secesní	secesní	k2eAgInSc4d1
skvost	skvost	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostějovský	prostějovský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
1606	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ELIÁŠ	Eliáš	k1gMnSc1
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
výročí	výročí	k1gNnSc3
Národního	národní	k2eAgInSc2d1
domu	dům	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc6
"	"	kIx"
<g/>
oživlé	oživlý	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostějovský	prostějovský	k2eAgInSc1d1
týden	týden	k1gInSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CHYTIL	Chytil	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavné	slavný	k2eAgFnPc4d1
stavby	stavba	k1gFnPc4
Prostějova	Prostějov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
s.	s.	k?
110	#num#	k4
<g/>
–	–	k?
<g/>
115	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
KOUDELKA	Koudelka	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Perlou	perla	k1gFnSc7
prostějovské	prostějovský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
je	být	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hanácký	hanácký	k2eAgInSc1d1
rok	rok	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
126	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87091	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MAREK	Marek	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
stavby	stavba	k1gFnSc2
Národního	národní	k2eAgInSc2d1
domu	dům	k1gInSc2
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rosice	Rosice	k1gFnPc1
u	u	k7c2
Brna	Brno	k1gNnSc2
<g/>
:	:	kIx,
Gloria	Gloria	k1gFnSc1
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
128	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86760	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ROHÁČKOVÁ	ROHÁČKOVÁ	kA
<g/>
,	,	kIx,
Dagmar	Dagmar	k1gFnSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
1907	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyd	Vyd	k1gFnSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostějov	Prostějov	k1gInSc1
<g/>
:	:	kIx,
Město	město	k1gNnSc1
Prostějov	Prostějov	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
399	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
7247	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
dům	dům	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
–	–	k?
oficiální	oficiální	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
</s>
<s>
O	o	k7c6
Národním	národní	k2eAgInSc6d1
domě	dům	k1gInSc6
–	–	k?
Městské	městský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2016122287	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
55147663095660552127	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2016122287	#num#	k4
</s>
