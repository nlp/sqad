<s>
Národní	národní	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
Prostějově	Prostějov	k1gInSc6
je	být	k5eAaImIp3nS
secesní	secesní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
<g/>
,	,	kIx,
postavená	postavený	k2eAgFnSc1d1
v	v	k7c6
letech	let	k1gInPc6
1905	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
podle	podle	k7c2
plánů	plán	k1gInPc2
architekta	architekt	k1gMnSc2
Jana	Jan	k1gMnSc2
Kotěry	Kotěra	k1gFnSc2
<g/>
.	.	kIx.
</s>