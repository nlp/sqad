<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DJC	DJC	kA	DJC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pražské	pražský	k2eAgNnSc4d1	Pražské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgInSc2	jenž
repertoáru	repertoár	k1gInSc2	repertoár
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgFnPc1d3	nejznámější
hry	hra	k1gFnPc1	hra
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
fiktivního	fiktivní	k2eAgMnSc2d1	fiktivní
českého	český	k2eAgMnSc2d1	český
génia	génius	k1gMnSc2	génius
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
