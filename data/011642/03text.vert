<p>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
DJC	DJC	kA	DJC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pražské	pražský	k2eAgNnSc4d1	Pražské
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgInSc2	jenž
repertoáru	repertoár	k1gInSc2	repertoár
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgFnPc1d3	nejznámější
hry	hra	k1gFnPc1	hra
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
fiktivního	fiktivní	k2eAgMnSc2d1	fiktivní
českého	český	k2eAgMnSc2d1	český
génia	génius	k1gMnSc2	génius
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
hrají	hrát	k5eAaImIp3nP	hrát
pouze	pouze	k6eAd1	pouze
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
divadla	divadlo	k1gNnSc2	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
Prvotním	prvotní	k2eAgMnSc7d1	prvotní
autorem	autor	k1gMnSc7	autor
nápadu	nápad	k1gInSc2	nápad
založit	založit	k5eAaPmF	založit
toto	tento	k3xDgNnSc4	tento
divadlo	divadlo	k1gNnSc4	divadlo
byl	být	k5eAaImAgMnS	být
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
seznámil	seznámit	k5eAaPmAgInS	seznámit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
myšlenkou	myšlenka	k1gFnSc7	myšlenka
Miloně	Miloň	k1gFnSc2	Miloň
Čepelku	čepelka	k1gFnSc4	čepelka
<g/>
,	,	kIx,	,
Ladislava	Ladislav	k1gMnSc2	Ladislav
Smoljaka	Smoljak	k1gMnSc2	Smoljak
a	a	k8xC	a
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Svěráka	Svěrák	k1gMnSc2	Svěrák
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
rané	raný	k2eAgFnSc6d1	raná
fázi	fáze	k1gFnSc6	fáze
přizváni	přizvat	k5eAaPmNgMnP	přizvat
ještě	ještě	k9	ještě
Karel	Karel	k1gMnSc1	Karel
Velebný	velebný	k2eAgMnSc1d1	velebný
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Trtílek	Trtílek	k1gMnSc1	Trtílek
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
Unger	Unger	k1gMnSc1	Unger
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
Philippová	Philippová	k1gFnSc1	Philippová
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
pořadu	pořad	k1gInSc6	pořad
Nealkoholická	alkoholický	k2eNgFnSc1d1	nealkoholická
vinárna	vinárna	k1gFnSc1	vinárna
U	u	k7c2	u
Pavouka	pavouk	k1gMnSc2	pavouk
oznámen	oznámen	k2eAgInSc4d1	oznámen
doktorem	doktor	k1gMnSc7	doktor
Evženem	Evžen	k1gMnSc7	Evžen
Hedvábným	hedvábný	k2eAgMnSc7d1	hedvábný
(	(	kIx(	(
<g/>
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Karel	Karel	k1gMnSc1	Karel
Velebný	velebný	k2eAgMnSc1d1	velebný
<g/>
)	)	kIx)	)
nález	nález	k1gInSc1	nález
pozůstalosti	pozůstalost	k1gFnSc2	pozůstalost
doposud	doposud	k6eAd1	doposud
neznámého	známý	k2eNgMnSc2d1	neznámý
českého	český	k2eAgMnSc2d1	český
génia	génius	k1gMnSc2	génius
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
plánu	plán	k1gInSc2	plán
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
při	při	k7c6	při
prémiéře	prémiéra	k1gFnSc6	prémiéra
nového	nový	k2eAgNnSc2d1	nové
divadla	divadlo	k1gNnSc2	divadlo
uvedeny	uvést	k5eAaPmNgFnP	uvést
dvě	dva	k4xCgFnPc1	dva
jednoaktové	jednoaktový	k2eAgFnPc1d1	jednoaktová
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jednu	jeden	k4xCgFnSc4	jeden
měl	mít	k5eAaImAgMnS	mít
napsat	napsat	k5eAaBmF	napsat
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
Šebánkova	Šebánkův	k2eAgFnSc1d1	Šebánkův
hra	hra	k1gFnSc1	hra
Domácí	domácí	k2eAgFnSc1d1	domácí
zabíjačka	zabíjačka	k1gFnSc1	zabíjačka
nebyla	být	k5eNaImAgFnS	být
včas	včas	k6eAd1	včas
dokončena	dokončit	k5eAaPmNgFnS	dokončit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
plán	plán	k1gInSc4	plán
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
místo	místo	k1gNnSc4	místo
vyplnil	vyplnit	k5eAaPmAgInS	vyplnit
seminář	seminář	k1gInSc1	seminář
o	o	k7c6	o
Cimrmanově	Cimrmanův	k2eAgInSc6d1	Cimrmanův
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tuto	tento	k3xDgFnSc4	tento
formu	forma	k1gFnSc4	forma
(	(	kIx(	(
<g/>
seminář	seminář	k1gInSc1	seminář
+	+	kIx~	+
hra	hra	k1gFnSc1	hra
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
udržela	udržet	k5eAaPmAgFnS	udržet
představení	představení	k1gNnSc4	představení
DJC	DJC	kA	DJC
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
tedy	tedy	k9	tedy
byla	být	k5eAaImAgFnS	být
uvedena	uveden	k2eAgFnSc1d1	uvedena
hra	hra	k1gFnSc1	hra
Akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
nejprve	nejprve	k6eAd1	nejprve
zkušební	zkušební	k2eAgFnSc4d1	zkušební
prémiéru	prémiéra	k1gFnSc4	prémiéra
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
známé	známý	k2eAgFnPc4d1	známá
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
v	v	k7c6	v
Malostranské	malostranský	k2eAgFnSc6d1	Malostranská
besedě	beseda	k1gFnSc6	beseda
představena	představit	k5eAaPmNgFnS	představit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
mělo	mít	k5eAaImAgNnS	mít
prémiéru	prémiéra	k1gFnSc4	prémiéra
Vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
ztráty	ztráta	k1gFnSc2	ztráta
třídní	třídní	k2eAgFnSc2d1	třídní
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
i	i	k9	i
Domácí	domácí	k2eAgFnSc1d1	domácí
zabíjačka	zabíjačka	k1gFnSc1	zabíjačka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
stažena	stažen	k2eAgFnSc1d1	stažena
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
když	když	k8xS	když
divadlo	divadlo	k1gNnSc1	divadlo
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Philippovou	Philippový	k2eAgFnSc7d1	Philippový
<g/>
)	)	kIx)	)
po	po	k7c6	po
uměleckých	umělecký	k2eAgFnPc6d1	umělecká
neshodách	neshoda	k1gFnPc6	neshoda
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInPc1d1	následující
roky	rok	k1gInPc1	rok
působení	působení	k1gNnSc2	působení
divadla	divadlo	k1gNnSc2	divadlo
se	se	k3xPyFc4	se
nesly	nést	k5eAaImAgFnP	nést
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
neshod	neshoda	k1gFnPc2	neshoda
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
aparátem	aparát	k1gInSc7	aparát
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
i	i	k9	i
film	film	k1gInSc1	film
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
divadlo	divadlo	k1gNnSc1	divadlo
muselo	muset	k5eAaImAgNnS	muset
několikrát	několikrát	k6eAd1	několikrát
přesouvat	přesouvat	k5eAaImF	přesouvat
<g/>
,	,	kIx,	,
hrát	hrát	k5eAaImF	hrát
na	na	k7c6	na
alternativních	alternativní	k2eAgFnPc6d1	alternativní
scénách	scéna	k1gFnPc6	scéna
v	v	k7c6	v
provizorních	provizorní	k2eAgNnPc6d1	provizorní
prostředích	prostředí	k1gNnPc6	prostředí
<g/>
,	,	kIx,	,
měnit	měnit	k5eAaImF	měnit
obsah	obsah	k1gInSc4	obsah
her	hra	k1gFnPc2	hra
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
sexuolog	sexuolog	k1gMnSc1	sexuolog
Pepa	Pepa	k1gMnSc1	Pepa
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Akt	akt	k1gInSc1	akt
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
příslušník	příslušník	k1gMnSc1	příslušník
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
dokonce	dokonce	k9	dokonce
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
účinkovat	účinkovat	k5eAaImF	účinkovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgMnS	mít
prémiéru	prémiér	k1gMnSc3	prémiér
film	film	k1gInSc4	film
Rozpuštěný	rozpuštěný	k2eAgInSc4d1	rozpuštěný
a	a	k8xC	a
vypuštěný	vypuštěný	k2eAgInSc4d1	vypuštěný
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
hry	hra	k1gFnSc2	hra
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salónním	salónní	k2eAgInSc6d1	salónní
coupé	coupý	k2eAgNnSc1d1	coupé
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvolnění	uvolnění	k1gNnSc1	uvolnění
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
přineslo	přinést	k5eAaPmAgNnS	přinést
nejen	nejen	k6eAd1	nejen
možnost	možnost	k1gFnSc4	možnost
uvést	uvést	k5eAaPmF	uvést
hru	hra	k1gFnSc4	hra
Blaník	Blaník	k1gInSc1	Blaník
(	(	kIx(	(
<g/>
o	o	k7c4	o
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
nevěřilo	věřit	k5eNaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
projde	projít	k5eAaPmIp3nS	projít
přes	přes	k7c4	přes
schvalovací	schvalovací	k2eAgFnSc4d1	schvalovací
komisi	komise	k1gFnSc4	komise
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rozmach	rozmach	k1gInSc1	rozmach
popularity	popularita	k1gFnSc2	popularita
osobnosti	osobnost	k1gFnSc2	osobnost
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenovány	pojmenován	k2eAgInPc1d1	pojmenován
nejen	nejen	k6eAd1	nejen
ulice	ulice	k1gFnPc1	ulice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
planetka	planetka	k1gFnSc1	planetka
objevená	objevený	k2eAgFnSc1d1	objevená
astronomem	astronom	k1gMnSc7	astronom
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Moravcem	Moravec	k1gMnSc7	Moravec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1995	[number]	k4	1995
dostává	dostávat	k5eAaImIp3nS	dostávat
hra	hra	k1gFnSc1	hra
Záskok	záskok	k1gInSc4	záskok
Cenu	cena	k1gFnSc4	cena
Alfréda	Alfréd	k1gMnSc2	Alfréd
Radoka	Radoek	k1gMnSc2	Radoek
jako	jako	k8xC	jako
hra	hra	k1gFnSc1	hra
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Působiště	působiště	k1gNnSc1	působiště
divadla	divadlo	k1gNnSc2	divadlo
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
zkušebnímu	zkušební	k2eAgInSc3d1	zkušební
představení	představení	k1gNnSc4	představení
došlo	dojít	k5eAaPmAgNnS	dojít
19	[number]	k4	19
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
v	v	k7c6	v
Malostranské	malostranský	k2eAgFnSc6d1	Malostranská
besedě	beseda	k1gFnSc6	beseda
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
známé	známý	k2eAgInPc4d1	známý
<g/>
)	)	kIx)	)
a	a	k8xC	a
oficiálně	oficiálně	k6eAd1	oficiálně
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
–	–	k?	–
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hru	hra	k1gFnSc4	hra
Akt	akta	k1gNnPc2	akta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1972	[number]	k4	1972
se	se	k3xPyFc4	se
DJC	DJC	kA	DJC
nastěhovalo	nastěhovat	k5eAaPmAgNnS	nastěhovat
a	a	k8xC	a
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
Redutě	reduta	k1gFnSc6	reduta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1974	[number]	k4	1974
DJC	DJC	kA	DJC
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
bez	bez	k7c2	bez
kulis	kulisa	k1gFnPc2	kulisa
<g/>
,	,	kIx,	,
kostýmů	kostým	k1gInPc2	kostým
a	a	k8xC	a
rekvizit	rekvizita	k1gFnPc2	rekvizita
na	na	k7c6	na
alternativních	alternativní	k2eAgFnPc6d1	alternativní
scénách	scéna	k1gFnPc6	scéna
–	–	k?	–
strahovská	strahovský	k2eAgFnSc1d1	Strahovská
Sedmička	sedmička	k1gFnSc1	sedmička
<g/>
,	,	kIx,	,
kolej	kolej	k1gFnSc1	kolej
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
Klub	klub	k1gInSc1	klub
chemiků	chemik	k1gMnPc2	chemik
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Řeznické	řeznická	k1gFnSc6	řeznická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
1975	[number]	k4	1975
divadlo	divadlo	k1gNnSc1	divadlo
působilo	působit	k5eAaImAgNnS	působit
v	v	k7c6	v
Branické	branický	k2eAgFnSc6d1	Branická
ulici	ulice	k1gFnSc6	ulice
č.	č.	k?	č.
41	[number]	k4	41
na	na	k7c6	na
Praze	Praha	k1gFnSc6	Praha
4	[number]	k4	4
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
Intermezzu	intermezzo	k1gNnSc6	intermezzo
v	v	k7c6	v
kulturním	kulturní	k2eAgInSc6d1	kulturní
domě	dům	k1gInSc6	dům
Novodvorská	novodvorský	k2eAgFnSc1d1	Novodvorská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
působilo	působit	k5eAaImAgNnS	působit
Divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Solidarita	solidarita	k1gFnSc1	solidarita
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Strašnické	strašnický	k2eAgNnSc1d1	Strašnické
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
Žižkovské	žižkovský	k2eAgNnSc4d1	Žižkovské
divadlo	divadlo	k1gNnSc4	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
příspěvková	příspěvkový	k2eAgFnSc1d1	příspěvková
organizace	organizace	k1gFnSc1	organizace
sídlí	sídlet	k5eAaImIp3nS	sídlet
Štítného	štítný	k2eAgMnSc2d1	štítný
520	[number]	k4	520
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
130	[number]	k4	130
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
3	[number]	k4	3
–	–	k?	–
Žižkov	Žižkov	k1gInSc1	Žižkov
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Žižkovské	žižkovský	k2eAgNnSc1d1	Žižkovské
divadlo	divadlo	k1gNnSc1	divadlo
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hry	hra	k1gFnPc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Akt	akt	k1gInSc1	akt
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
</s>
</p>
<p>
<s>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
ztráty	ztráta	k1gFnSc2	ztráta
třídní	třídní	k2eAgFnSc2d1	třídní
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
</s>
</p>
<p>
<s>
Domácí	domácí	k2eAgFnSc1d1	domácí
zabijačka	zabijačka	k1gFnSc1	zabijačka
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
vypuštěna	vypustit	k5eAaPmNgFnS	vypustit
z	z	k7c2	z
repertoáru	repertoár	k1gInSc2	repertoár
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
autora	autor	k1gMnSc2	autor
z	z	k7c2	z
divadla	divadlo	k1gNnSc2	divadlo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hospoda	Hospoda	k?	Hospoda
Na	na	k7c6	na
mýtince	mýtinka	k1gFnSc6	mýtinka
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salonním	salonní	k2eAgInSc6d1	salonní
coupé	coupý	k2eAgInPc1d1	coupý
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
</s>
</p>
<p>
<s>
Němý	němý	k2eAgMnSc1d1	němý
Bobeš	Bobeš	k1gMnSc1	Bobeš
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Cimrman	Cimrman	k1gMnSc1	Cimrman
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Posel	posel	k1gMnSc1	posel
z	z	k7c2	z
Liptákova	Liptákův	k2eAgNnSc2d1	Liptákův
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Lijavec	lijavec	k1gInSc1	lijavec
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
Blaník	Blaník	k1gInSc1	Blaník
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
</s>
</p>
<p>
<s>
Záskok	záskok	k1gInSc1	záskok
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Švestka	Švestka	k1gMnSc1	Švestka
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
nebe	nebe	k1gNnSc1	nebe
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
==	==	k?	==
Herecký	herecký	k2eAgInSc1d1	herecký
soubor	soubor	k1gInSc1	soubor
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Miloň	Milonit	k5eAaPmRp2nS	Milonit
Čepelka	čepelka	k1gFnSc1	čepelka
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Smoljak	Smoljak	k1gMnSc1	Smoljak
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Svěrák	Svěrák	k1gMnSc1	Svěrák
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Velebný	velebný	k2eAgMnSc1d1	velebný
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Evžen	Evžen	k1gMnSc1	Evžen
Hedvábný	hedvábný	k2eAgMnSc1d1	hedvábný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Trtílek	Trtílek	k1gMnSc1	Trtílek
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Unger	Unger	k1gMnSc1	Unger
1966	[number]	k4	1966
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Podaný	podaný	k2eAgMnSc1d1	podaný
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Stivín	Stivín	k1gMnSc1	Stivín
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Štědrý	štědrý	k2eAgMnSc1d1	štědrý
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Vít	Vít	k1gMnSc1	Vít
Mach	Mach	k1gMnSc1	Mach
1967	[number]	k4	1967
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Vondruška	Vondruška	k1gMnSc1	Vondruška
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vozáb	Vozáb	k1gMnSc1	Vozáb
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Klusák	Klusák	k1gMnSc1	Klusák
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Weigel	Weigel	k1gMnSc1	Weigel
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Petiška	Petišek	k1gInSc2	Petišek
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
–	–	k?	–
uměleckým	umělecký	k2eAgNnSc7d1	umělecké
příjmením	příjmení	k1gNnSc7	příjmení
Vinant	Vinanta	k1gFnPc2	Vinanta
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
Eduarda	Eduard	k1gMnSc2	Eduard
Petišky	Petiška	k1gFnSc2	Petiška
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Kotek	Kotek	k1gMnSc1	Kotek
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Menzel	Menzel	k1gMnSc1	Menzel
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Koudelka	Koudelka	k1gMnSc1	Koudelka
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Hraběta	Hraběta	k1gMnSc1	Hraběta
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Bořivoj	Bořivoj	k1gMnSc1	Bořivoj
Penc	Penc	k1gInSc4	Penc
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
</s>
</p>
<p>
<s>
Genadij	Genadít	k5eAaPmRp2nS	Genadít
Rumlena	Rumlena	k1gFnSc1	Rumlena
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Šimon	Šimon	k1gMnSc1	Šimon
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Reidinger	Reidinger	k1gMnSc1	Reidinger
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Weigel	Weigel	k1gMnSc1	Weigel
1979	[number]	k4	1979
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Bárta	Bárta	k1gMnSc1	Bárta
1978	[number]	k4	1978
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Svoboda	Svoboda	k1gMnSc1	Svoboda
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
dodnes	dodnes	k6eAd1	dodnes
</s>
</p>
<p>
<s>
Andrej	Andrej	k1gMnSc1	Andrej
Krob	Krob	k1gMnSc1	Krob
do	do	k7c2	do
1975	[number]	k4	1975
<g/>
;	;	kIx,	;
2013	[number]	k4	2013
-	-	kIx~	-
dodnesNěkteří	dodnesNěkterý	k2eAgMnPc1d1	dodnesNěkterý
herci	herec	k1gMnPc1	herec
jako	jako	k8xS	jako
Petr	Petr	k1gMnSc1	Petr
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kašpar	Kašpar	k1gMnSc1	Kašpar
nebo	nebo	k8xC	nebo
Genadij	Genadij	k1gMnSc1	Genadij
Rumlena	Rumlena	k1gFnSc1	Rumlena
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
dřív	dříve	k6eAd2	dříve
jako	jako	k8xS	jako
kulisáci	kulisák	k1gMnPc1	kulisák
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
jako	jako	k8xC	jako
hosté	host	k1gMnPc1	host
účinkují	účinkovat	k5eAaImIp3nP	účinkovat
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kotek	Kotek	k1gMnSc1	Kotek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Čepelka	čepelka	k1gFnSc1	čepelka
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
Vražda	vražda	k1gFnSc1	vražda
v	v	k7c6	v
salónním	salónní	k2eAgMnSc6d1	salónní
coupé	coupý	k2eAgFnPc4d1	coupý
<g/>
;	;	kIx,	;
akce	akce	k1gFnPc4	akce
"	"	kIx"	"
<g/>
Synové	syn	k1gMnPc1	syn
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
otcům	otec	k1gMnPc3	otec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
Miroslav	Miroslav	k1gMnSc1	Miroslav
Táborský	Táborský	k1gMnSc1	Táborský
(	(	kIx(	(
<g/>
Dlouhý	Dlouhý	k1gMnSc1	Dlouhý
<g/>
,	,	kIx,	,
Široký	Široký	k1gMnSc1	Široký
a	a	k8xC	a
Krátkozraký	krátkozraký	k2eAgMnSc1d1	krátkozraký
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
</s>
</p>
<p>
<s>
Nejistá	jistý	k2eNgFnSc1d1	nejistá
sezóna	sezóna	k1gFnSc1	sezóna
</s>
</p>
<p>
<s>
Jára	Jára	k1gMnSc1	Jára
Cimrman	Cimrman	k1gMnSc1	Cimrman
ležící	ležící	k2eAgMnSc1d1	ležící
<g/>
,	,	kIx,	,
spící	spící	k2eAgMnSc1d1	spící
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
www.zdjc.cz	www.zdjc.cz	k1gInSc1	www.zdjc.cz
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
<p>
<s>
www.cimrman.at	www.cimrman.at	k1gInSc1	www.cimrman.at
–	–	k?	–
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
souboru	soubor	k1gInSc2	soubor
</s>
</p>
<p>
<s>
TACE	TACE	kA	TACE
–	–	k?	–
heslo	heslo	k1gNnSc1	heslo
v	v	k7c6	v
databázi	databáze	k1gFnSc6	databáze
TACE	TACE	kA	TACE
</s>
</p>
<p>
<s>
http://www.divadlo-jary-cimrmana-fanklub.estranky.cz	[url]	k1gInSc1	http://www.divadlo-jary-cimrmana-fanklub.estranky.cz
–	–	k?	–
fanklub	fanklub	k1gInSc1	fanklub
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
