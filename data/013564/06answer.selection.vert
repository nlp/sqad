<s desamb="1">
Přes	přes	k7c4
léto	léto	k1gNnSc4
má	mít	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
srst	srst	k1gFnSc4
mírně	mírně	k6eAd1
rezavo-červený	rezavo-červený	k2eAgInSc1d1
odstín	odstín	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
koncem	konec	k1gInSc7
roku	rok	k1gInSc2
však	však	k9
narůstá	narůstat	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
zimní	zimní	k2eAgFnSc1d1
srst	srst	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
zbarvení	zbarvení	k1gNnSc1
znatelně	znatelně	k6eAd1
tmavne	tmavnout	k5eAaImIp3nS
(	(	kIx(
<g/>
srnčí	srnčí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
přebarvuje	přebarvovat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>