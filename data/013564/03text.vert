<s>
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
srna	srna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Srna	srna	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
srnec	srnec	k1gMnSc1
obecný	obecný	k2eAgInSc4d1
Stupeň	stupeň	k1gInSc4
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
savci	savec	k1gMnPc1
(	(	kIx(
<g/>
Mammalia	Mammalia	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
sudokopytníci	sudokopytník	k1gMnPc1
(	(	kIx(
<g/>
Artiodactyla	Artiodactyla	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
jelenovití	jelenovití	k1gMnPc1
(	(	kIx(
<g/>
Cervidae	Cervidae	k1gInSc1
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
srnec	srnec	k1gMnSc1
(	(	kIx(
<g/>
Capreolus	Capreolus	k1gMnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Capreolus	Capreolus	k1gMnSc1
capreolus	capreolus	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Linné	Linné	k1gNnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
mapka	mapka	k1gFnSc1
s	s	k7c7
vyznačeným	vyznačený	k2eAgInSc7d1
areálem	areál	k1gInSc7
rozšíření	rozšíření	k1gNnSc4
srnce	srnec	k1gMnSc4
obecného	obecný	k2eAgInSc2d1
</s>
<s>
mapka	mapka	k1gFnSc1
s	s	k7c7
vyznačeným	vyznačený	k2eAgInSc7d1
areálem	areál	k1gInSc7
rozšíření	rozšíření	k1gNnSc4
srnce	srnec	k1gMnSc4
obecného	obecný	k2eAgInSc2d1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Capreolus	Capreolus	k1gMnSc1
capreolus	capreolus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hojně	hojně	k6eAd1
rozšířený	rozšířený	k2eAgMnSc1d1
sudokopytník	sudokopytník	k1gMnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
jelenovití	jelenovití	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
evropských	evropský	k2eAgInPc2d1
druhů	druh	k1gInPc2
jelenovitých	jelenovitý	k2eAgInPc2d1
je	být	k5eAaImIp3nS
nejmenší	malý	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
srnce	srnec	k1gMnSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
srna	srna	k1gFnSc1
(	(	kIx(
<g/>
řidčeji	řídce	k6eAd2
srnka	srnka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mládě	mládě	k1gNnSc1
pak	pak	k8xC
srnče	srnče	k1gNnSc1
(	(	kIx(
<g/>
řidčeji	řídce	k6eAd2
srně	srně	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
vedle	vedle	k7c2
prasete	prase	k1gNnSc2
divokého	divoký	k2eAgNnSc2d1
o	o	k7c4
nejrozšířenější	rozšířený	k2eAgFnSc4d3
spárkatou	spárkatý	k2eAgFnSc4d1
zvěř	zvěř	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Srna	srna	k1gFnSc1
a	a	k8xC
srnec	srnec	k1gMnSc1
v	v	k7c6
běhu	běh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
malým	malý	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
své	svůj	k3xOyFgFnSc2
čeledi	čeleď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosahuje	dosahovat	k5eAaImIp3nS
hmotnosti	hmotnost	k1gFnSc2
mezi	mezi	k7c7
15	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
kg	kg	kA
a	a	k8xC
v	v	k7c6
kohoutku	kohoutek	k1gInSc6
měří	měřit	k5eAaImIp3nS
65	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ocas	ocas	k1gInSc1
(	(	kIx(
<g/>
kelka	kelka	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
krátký	krátký	k2eAgMnSc1d1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
a	a	k8xC
sotva	sotva	k6eAd1
viditelný	viditelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přes	přes	k7c4
léto	léto	k1gNnSc4
má	mít	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
srst	srst	k1gFnSc4
mírně	mírně	k6eAd1
rezavo-červený	rezavo-červený	k2eAgInSc1d1
odstín	odstín	k1gInSc1
<g/>
,	,	kIx,
s	s	k7c7
koncem	konec	k1gInSc7
roku	rok	k1gInSc2
však	však	k9
narůstá	narůstat	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
zimní	zimní	k2eAgFnSc1d1
srst	srst	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
zbarvení	zbarvení	k1gNnSc1
znatelně	znatelně	k6eAd1
tmavne	tmavnout	k5eAaImIp3nS
(	(	kIx(
<g/>
srnčí	srnčí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
přebarvuje	přebarvovat	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
mají	mít	k5eAaImIp3nP
relativně	relativně	k6eAd1
krátké	krátký	k2eAgInPc4d1
parůžky	parůžek	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mohou	moct	k5eAaImIp3nP
u	u	k7c2
jedinců	jedinec	k1gMnPc2
v	v	k7c6
dobrých	dobrý	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
dorůstat	dorůstat	k5eAaImF
až	až	k9
do	do	k7c2
délky	délka	k1gFnSc2
25	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
října	říjen	k1gInSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
shazují	shazovat	k5eAaImIp3nP
samci	samec	k1gInSc3
své	svůj	k3xOyFgInPc4
parůžky	parůžek	k1gInPc4
a	a	k8xC
na	na	k7c6
jaře	jaro	k1gNnSc6
příštího	příští	k2eAgInSc2d1
roku	rok	k1gInSc2
jim	on	k3xPp3gMnPc3
narůstají	narůstat	k5eAaImIp3nP
nové	nový	k2eAgFnPc1d1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
zpočátku	zpočátku	k6eAd1
pokryté	pokrytý	k2eAgFnPc1d1
jakousi	jakýsi	k3yIgFnSc7
sametovou	sametový	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
lýčí	lýčí	k1gNnSc1
a	a	k8xC
vyživuje	vyživovat	k5eAaImIp3nS
parůžek	parůžek	k1gInSc4
při	při	k7c6
vývoji	vývoj	k1gInSc6
a	a	k8xC
růstu	růst	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dokončení	dokončení	k1gNnSc6
vývoje	vývoj	k1gInSc2
parůžků	parůžek	k1gInPc2
je	být	k5eAaImIp3nS
srnec	srnec	k1gMnSc1
vytlouká	vytloukat	k5eAaImIp3nS
–	–	k?
zbavuje	zbavovat	k5eAaImIp3nS
parůžky	parůžek	k1gInPc4
odumřelého	odumřelý	k2eAgNnSc2d1
lýčí	lýčí	k1gNnSc2
otíráním	otírání	k1gNnSc7
o	o	k7c4
slabé	slabý	k2eAgFnPc4d1
kmínky	kmínka	k1gFnPc4
stromů	strom	k1gInPc2
nebo	nebo	k8xC
keřů	keř	k1gInPc2
(	(	kIx(
<g/>
podle	podle	k7c2
druhu	druh	k1gInSc2
stromu	strom	k1gInSc2
nebo	nebo	k8xC
keře	keř	k1gInSc2
se	se	k3xPyFc4
odvíjí	odvíjet	k5eAaImIp3nS
odstín	odstín	k1gInSc1
parůžků	parůžek	k1gInPc2
např.	např.	kA
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
Moravě	Morava	k1gFnSc6
mají	mít	k5eAaImIp3nP
srnci	srnec	k1gMnPc1
světlé	světlý	k2eAgInPc4d1
parůžky	parůžek	k1gInPc1
a	a	k8xC
v	v	k7c6
Beskydech	Beskyd	k1gInPc6
nebo	nebo	k8xC
ve	v	k7c6
výše	vysoce	k6eAd2
položených	položený	k2eAgInPc6d1
lesích	les	k1gInPc6
mají	mít	k5eAaImIp3nP
srnci	srnec	k1gMnPc1
obvykle	obvykle	k6eAd1
parůžky	parůžek	k1gInPc4
tmavší	tmavý	k2eAgInPc4d2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srnčí	srnčí	k1gNnSc1
paroží	paroží	k1gNnSc3
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
jednoduše	jednoduše	k6eAd1
zašpičatělé	zašpičatělý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
naopak	naopak	k6eAd1
4	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
výsad	výsada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takže	takže	k8xS
to	ten	k3xDgNnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
špičák	špičák	k1gMnSc1
<g/>
,	,	kIx,
vidlák	vidlák	k1gMnSc1
<g/>
,	,	kIx,
šesterák	šesterák	k1gMnSc1
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
osmerák	osmerák	k1gMnSc1
(	(	kIx(
<g/>
čtyři	čtyři	k4xCgFnPc1
výsady	výsada	k1gFnPc1
na	na	k7c6
jednom	jeden	k4xCgInSc6
parůžku	parůžek	k1gInSc6
<g/>
)	)	kIx)
nebo	nebo	k8xC
raritní	raritní	k2eAgMnSc1d1
srnec	srnec	k1gMnSc1
s	s	k7c7
různě	různě	k6eAd1
nepravidelně	pravidelně	k6eNd1
deformovaným	deformovaný	k2eAgNnSc7d1
a	a	k8xC
členitým	členitý	k2eAgNnSc7d1
parožím	paroží	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srnec	Srnec	k1gMnSc1
své	svůj	k3xOyFgInPc4
parůžky	parůžek	k1gInPc4
shazuje	shazovat	k5eAaImIp3nS
v	v	k7c6
listopadu	listopad	k1gInSc6
a	a	k8xC
prosinci	prosinec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
poranění	poranění	k1gNnSc6
pučnic	pučnice	k1gFnPc2
parůžků	parůžek	k1gInPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
však	však	k9
i	i	k9
v	v	k7c6
důsledku	důsledek	k1gInSc6
poranění	poranění	k1gNnSc1
varlat	varle	k1gNnPc2
<g/>
(	(	kIx(
<g/>
ráže	ráže	k1gFnSc2
<g/>
)	)	kIx)
vznikají	vznikat	k5eAaImIp3nP
v	v	k7c6
paroží	paroží	k1gNnSc6
srnčí	srnčí	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
abnormality	abnormalita	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
parukáči	parukáč	k1gMnPc1
s	s	k7c7
houbovitým	houbovitý	k2eAgNnSc7d1
parožím	paroží	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
zůstává	zůstávat	k5eAaImIp3nS
trvale	trvale	k6eAd1
v	v	k7c6
lýčí	lýčí	k1gNnSc6
a	a	k8xC
vývrtkáči	vývrtkáč	k1gInSc6
s	s	k7c7
parožím	paroží	k1gNnSc7
ve	v	k7c6
tvaru	tvar	k1gInSc6
vývrtky	vývrtka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Barevné	barevný	k2eAgFnPc4d1
odchylky	odchylka	k1gFnPc4
</s>
<s>
Velmi	velmi	k6eAd1
vzácně	vzácně	k6eAd1
lze	lze	k6eAd1
spatřit	spatřit	k5eAaPmF
srnčí	srnčí	k2eAgInPc4d1
albíny	albín	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
své	svůj	k3xOyFgFnSc2
vady	vada	k1gFnSc2
často	často	k6eAd1
trpí	trpět	k5eAaImIp3nS
i	i	k9
špatným	špatný	k2eAgInSc7d1
zrakem	zrak	k1gInSc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
světloplaší	světloplachý	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
myslivci	myslivec	k1gMnPc7
se	se	k3xPyFc4
traduje	tradovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zastřelit	zastřelit	k5eAaPmF
srnu-albína	srnu-albín	k1gMnSc4
přináší	přinášet	k5eAaImIp3nS
smůlu	smůla	k1gFnSc4
<g/>
,	,	kIx,
respektive	respektive	k9
<g/>
,	,	kIx,
že	že	k8xS
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
zastřelí	zastřelit	k5eAaPmIp3nS
srnu-albína	srnu-albína	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
roka	rok	k1gInSc2
zemře	zemřít	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednu	jeden	k4xCgFnSc4
srnu-albína	srnu-albín	k1gInSc2
chová	chovat	k5eAaImIp3nS
Zoo	zoo	k1gFnSc1
Děčín	Děčín	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
žije	žít	k5eAaImIp3nS
v	v	k7c6
lesích	les	k1gInPc6
<g/>
,	,	kIx,
za	za	k7c2
soumraku	soumrak	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
nejaktivnější	aktivní	k2eAgNnSc1d3
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
často	často	k6eAd1
navštěvuje	navštěvovat	k5eAaImIp3nS
zemědělskou	zemědělský	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
a	a	k8xC
často	často	k6eAd1
proniká	pronikat	k5eAaImIp3nS
až	až	k9
na	na	k7c4
okraje	okraj	k1gInPc4
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obývá	obývat	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
celou	celý	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
velkém	velký	k2eAgInSc6d1
počtu	počet	k1gInSc6
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
pouze	pouze	k6eAd1
na	na	k7c6
severu	sever	k1gInSc6
Skandinávie	Skandinávie	k1gFnSc2
<g/>
,	,	kIx,
zasahuje	zasahovat	k5eAaImIp3nS
také	také	k9
až	až	k9
k	k	k7c3
pobřeží	pobřeží	k1gNnSc3
Kaspického	kaspický	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
na	na	k7c6
území	území	k1gNnSc6
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
nejhojnějšího	hojný	k2eAgMnSc4d3
sudokopytníka	sudokopytník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
početnost	početnost	k1gFnSc1
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
198	#num#	k4
000	#num#	k4
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
již	již	k6eAd1
na	na	k7c4
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
000	#num#	k4
jedinců	jedinec	k1gMnPc2
(	(	kIx(
<g/>
nejvíce	nejvíce	k6eAd1,k6eAd3
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lov	lov	k1gInSc1
</s>
<s>
Srnčí	srnčí	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Česku	Česko	k1gNnSc6
nejvíce	hodně	k6eAd3,k6eAd1
loveným	lovený	k2eAgMnSc7d1
druhem	druh	k1gMnSc7
srstnaté	srstnatý	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roční	roční	k2eAgInSc1d1
odstřel	odstřel	k1gInSc1
činí	činit	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
110	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srnci	Srnec	k1gMnPc1
se	se	k3xPyFc4
loví	lovit	k5eAaImIp3nP
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
srny	srna	k1gFnPc4
a	a	k8xC
srnčata	srnče	k1gNnPc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
do	do	k7c2
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lov	lov	k1gInSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
osamělým	osamělý	k2eAgNnSc7d1
čekáním	čekání	k1gNnSc7
<g/>
,	,	kIx,
šoulačkou	šoulačka	k1gFnSc7
či	či	k8xC
vábením	vábení	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
kulovnicí	kulovnice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
se	se	k3xPyFc4
srnčí	srnčí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
střílela	střílet	k5eAaImAgFnS
i	i	k9
broky	brok	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
není	být	k5eNaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
z	z	k7c2
etických	etický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
povoleno	povolit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
středověku	středověk	k1gInSc6
a	a	k8xC
raném	raný	k2eAgInSc6d1
novověku	novověk	k1gInSc6
se	se	k3xPyFc4
srnčí	srnčí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
chytala	chytat	k5eAaImAgFnS
do	do	k7c2
tenat	tenata	k1gNnPc2
nebo	nebo	k8xC
se	se	k3xPyFc4
štvala	štvát	k5eAaImAgFnS
psy	pes	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavý	zajímavý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
lov	lov	k1gInSc1
vábením	vábení	k1gNnSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
zejména	zejména	k9
v	v	k7c6
období	období	k1gNnSc6
říje	říje	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lovec	lovec	k1gMnSc1
při	při	k7c6
něm	on	k3xPp3gInSc6
pomocí	pomocí	k7c2
vábničky	vábnička	k1gFnSc2
nebo	nebo	k8xC
lístku	lístek	k1gInSc2
trávy	tráva	k1gFnSc2
napodobuje	napodobovat	k5eAaImIp3nS
pískání	pískání	k1gNnSc1
říjné	říjný	k2eAgFnSc2d1
srny	srna	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
srnce	srnec	k1gMnPc4
přivábil	přivábit	k5eAaPmAgMnS
na	na	k7c4
dostřel	dostřel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ekologie	ekologie	k1gFnSc1
a	a	k8xC
rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Srnče	srnče	k1gNnSc1
</s>
<s>
Srnčí	srnčí	k1gNnSc1
se	se	k3xPyFc4
živí	živit	k5eAaImIp3nS
zejména	zejména	k9
nejrůznějšími	různý	k2eAgFnPc7d3
bylinami	bylina	k1gFnPc7
<g/>
,	,	kIx,
občas	občas	k6eAd1
požírá	požírat	k5eAaImIp3nS
také	také	k9
různé	různý	k2eAgInPc4d1
plody	plod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláště	zvláště	k6eAd1
v	v	k7c6
oblibě	obliba	k1gFnSc6
má	mít	k5eAaImIp3nS
přitom	přitom	k6eAd1
mladé	mladý	k2eAgInPc4d1
výhonky	výhonek	k1gInPc4
trav	tráva	k1gFnPc2
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
vlhkosti	vlhkost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
plachý	plachý	k2eAgMnSc1d1
a	a	k8xC
při	při	k7c6
ohrožení	ohrožení	k1gNnSc6
se	se	k3xPyFc4
často	často	k6eAd1
ozývá	ozývat	k5eAaImIp3nS
bekáním	bekání	k1gNnSc7
<g/>
,	,	kIx,
tedy	tedy	k8xC
zvukem	zvuk	k1gInSc7
podobným	podobný	k2eAgInSc7d1
psímu	psí	k2eAgNnSc3d1
štěknutí	štěknutí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srnčí	srnčí	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
žila	žít	k5eAaImAgFnS
původně	původně	k6eAd1
samotářsky	samotářsky	k6eAd1
ve	v	k7c6
světlých	světlý	k2eAgInPc6d1
lesích	les	k1gInPc6
a	a	k8xC
lesostepní	lesostepní	k2eAgFnSc6d1
krajině	krajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
osídlila	osídlit	k5eAaPmAgFnS
kulturní	kulturní	k2eAgFnSc4d1
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
sdružuje	sdružovat	k5eAaImIp3nS
do	do	k7c2
početných	početný	k2eAgNnPc2d1
stád	stádo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
však	však	k9
nemají	mít	k5eNaImIp3nP
přísnou	přísný	k2eAgFnSc4d1
sociální	sociální	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
nebezpečí	nebezpečí	k1gNnSc4
srnčí	srnčí	k2eAgNnPc4d1
varuje	varovat	k5eAaImIp3nS
bílou	bílý	k2eAgFnSc7d1
skvrnou	skvrna	k1gFnSc7
na	na	k7c6
zadku	zadek	k1gInSc6
-	-	kIx~
obřitkem	obřitek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
tvaru	tvar	k1gInSc2
obřitku	obřitek	k1gInSc2
lze	lze	k6eAd1
rozlišit	rozlišit	k5eAaPmF
pohlaví	pohlaví	k1gNnSc4
srnčí	srnčí	k2eAgFnSc2d1
zvěře	zvěř	k1gFnSc2
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
když	když	k8xS
srnci	srnec	k1gMnPc1
nemají	mít	k5eNaImIp3nP
parůžky	parůžek	k1gInPc4
<g/>
,	,	kIx,
srnec	srnec	k1gMnSc1
má	mít	k5eAaImIp3nS
obřitek	obřitek	k1gInSc1
oválný	oválný	k2eAgInSc1d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
srn	srna	k1gFnPc2
je	být	k5eAaImIp3nS
srdčitého	srdčitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Samice	samice	k1gFnPc1
pohlavně	pohlavně	k6eAd1
dospívají	dospívat	k5eAaImIp3nP
ve	v	k7c6
věku	věk	k1gInSc6
16	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
jsou	být	k5eAaImIp3nP
polygamní	polygamní	k2eAgMnPc1d1
a	a	k8xC
v	v	k7c6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
začínají	začínat	k5eAaImIp3nP
bojovat	bojovat	k5eAaImF
o	o	k7c4
přízeň	přízeň	k1gFnSc4
samic	samice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
listopadu	listopad	k1gInSc2
se	se	k3xPyFc4
zárodek	zárodek	k1gInSc1
prakticky	prakticky	k6eAd1
nevyvíjí	vyvíjet	k5eNaImIp3nS
(	(	kIx(
<g/>
latentní	latentní	k2eAgFnSc1d1
březost	březost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
se	se	k3xPyFc4
v	v	k7c6
listopadu	listopad	k1gInSc6
neoplozené	oplozený	k2eNgFnSc2d1
srny	srna	k1gFnSc2
páří	pářit	k5eAaImIp3nS
s	s	k7c7
mladými	mladý	k2eAgMnPc7d1
srnci	srnec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nebyli	být	k5eNaImAgMnP
připuštěni	připustit	k5eAaPmNgMnP
k	k	k7c3
říji	říje	k1gFnSc3
v	v	k7c6
obvyklém	obvyklý	k2eAgInSc6d1
čase	čas	k1gInSc6
staršími	starý	k2eAgInPc7d2
a	a	k8xC
silnějšími	silný	k2eAgInPc7d2
samci	samec	k1gInPc7
<g/>
,	,	kIx,
zárodek	zárodek	k1gInSc1
se	se	k3xPyFc4
poté	poté	k6eAd1
vyvíjí	vyvíjet	k5eAaImIp3nS
normálně	normálně	k6eAd1
–	–	k?
toto	tento	k3xDgNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
až	až	k8xS
červnu	červen	k1gInSc6
rodí	rodit	k5eAaImIp3nS
srna	srna	k1gFnSc1
obvykle	obvykle	k6eAd1
jedno	jeden	k4xCgNnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
dvě	dva	k4xCgNnPc4
hnědá	hnědý	k2eAgNnPc4d1
<g/>
,	,	kIx,
bíle	bíle	k6eAd1
skvrnitá	skvrnitý	k2eAgNnPc4d1
mláďata	mládě	k1gNnPc4
(	(	kIx(
<g/>
srnčata	srnče	k1gNnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Srnčata	srnče	k1gNnPc1
zůstávají	zůstávat	k5eAaImIp3nP
několik	několik	k4yIc4
prvních	první	k4xOgInPc2
dnů	den	k1gInPc2
nehybně	hybně	k6eNd1
skryta	skrýt	k5eAaPmNgFnS
v	v	k7c6
husté	hustý	k2eAgFnSc6d1
vegetaci	vegetace	k1gFnSc6
<g/>
,	,	kIx,
kam	kam	k6eAd1
je	on	k3xPp3gInPc4
chodí	chodit	k5eAaImIp3nS
matka	matka	k1gFnSc1
několikrát	několikrát	k6eAd1
denně	denně	k6eAd1
krmit	krmit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgNnSc6
období	období	k1gNnSc6
jich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
mnoho	mnoho	k6eAd1
zahubeno	zahuben	k2eAgNnSc1d1
sekačkami	sekačka	k1gFnPc7
<g/>
,	,	kIx,
myslivci	myslivec	k1gMnPc1
se	se	k3xPyFc4
tomuto	tento	k3xDgInSc3
snaží	snažit	k5eAaImIp3nS
zabránit	zabránit	k5eAaPmF
spoluprací	spolupráce	k1gFnSc7
se	s	k7c7
zemědělci	zemědělec	k1gMnPc7
–	–	k?
procházením	procházení	k1gNnSc7
luk	louka	k1gFnPc2
před	před	k7c7
senosečí	senoseč	k1gFnSc7
<g/>
,	,	kIx,
nalezená	nalezený	k2eAgNnPc1d1
srnčata	srnče	k1gNnPc1
odnáší	odnášet	k5eAaImIp3nP
do	do	k7c2
křovin	křovina	k1gFnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
na	na	k7c6
nich	on	k3xPp3gFnPc6
nezůstal	zůstat	k5eNaPmAgInS
lidský	lidský	k2eAgInSc1d1
pach	pach	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
samice	samice	k1gFnSc1
zaregistruje	zaregistrovat	k5eAaPmIp3nS
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
blízkosti	blízkost	k1gFnSc6
člověka	člověk	k1gMnSc2
nebo	nebo	k8xC
nějakého	nějaký	k3yIgMnSc2
dravce	dravec	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
často	často	k6eAd1
stává	stávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
svá	svůj	k3xOyFgNnPc4
mláďata	mládě	k1gNnPc4
opustí	opustit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srnčata	srnče	k1gNnPc4
s	s	k7c7
matkou	matka	k1gFnSc7
zůstávají	zůstávat	k5eAaImIp3nP
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
až	až	k9
do	do	k7c2
příštího	příští	k2eAgInSc2d1
porodu	porod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nepoučení	poučený	k2eNgMnPc1d1
lidé	člověk	k1gMnPc1
ročně	ročně	k6eAd1
v	v	k7c6
lesích	lese	k1gFnPc6
seberou	sebrat	k5eAaPmIp3nP
mnoho	mnoho	k4c4
srnčat	srnče	k1gNnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
je	on	k3xPp3gInPc4
pokládají	pokládat	k5eAaImIp3nP
za	za	k7c4
opuštěná	opuštěný	k2eAgNnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
rychle	rychle	k6eAd1
nedostane	dostat	k5eNaPmIp3nS
odborné	odborný	k2eAgFnPc4d1
péče	péče	k1gFnPc4
<g/>
,	,	kIx,
srnčata	srnče	k1gNnPc4
obvykle	obvykle	k6eAd1
uhynou	uhynout	k5eAaPmIp3nP
<g/>
,	,	kIx,
protože	protože	k8xS
jim	on	k3xPp3gMnPc3
kravské	kravský	k2eAgNnSc1d1
mléko	mléko	k1gNnSc1
působí	působit	k5eAaImIp3nS
těžké	těžký	k2eAgFnPc4d1
zažívací	zažívací	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vhodnější	vhodný	k2eAgFnSc7d2
potravou	potrava	k1gFnSc7
by	by	kYmCp3nS
bylo	být	k5eAaImAgNnS
mléko	mléko	k1gNnSc1
kozí	kozí	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
odchované	odchovaný	k2eAgFnPc1d1
srny	srna	k1gFnPc1
v	v	k7c6
zajetí	zajetí	k1gNnSc6
zkrotnou	zkrotnout	k5eAaPmIp3nP
<g/>
,	,	kIx,
srnci	srnec	k1gMnPc1
samčího	samčí	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
se	se	k3xPyFc4
po	po	k7c6
dosažení	dosažení	k1gNnSc6
dospělosti	dospělost	k1gFnSc2
stávají	stávat	k5eAaImIp3nP
agresívními	agresívní	k2eAgFnPc7d1
a	a	k8xC
mohou	moct	k5eAaImIp3nP
vážně	vážně	k6eAd1
zranit	zranit	k5eAaPmF
i	i	k9
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
volné	volný	k2eAgFnSc6d1
přírodě	příroda	k1gFnSc6
se	se	k3xPyFc4
dožívá	dožívat	k5eAaImIp3nS
srnec	srnec	k1gMnSc1
obvykle	obvykle	k6eAd1
10	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
vzácněji	vzácně	k6eAd2
i	i	k9
více	hodně	k6eAd2
–	–	k?
asi	asi	k9
15	#num#	k4
nebo	nebo	k8xC
17	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Přirozeným	přirozený	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
je	být	k5eAaImIp3nS
liška	liška	k1gFnSc1
<g/>
,	,	kIx,
vlk	vlk	k1gMnSc1
nebo	nebo	k8xC
rys	rys	k1gMnSc1
ostrovid	ostrovid	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Roe	Roe	k1gMnSc1
Deer	Deer	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Vzácnou	vzácný	k2eAgFnSc4d1
srnu	srna	k1gFnSc4
albína	albín	k1gMnSc2
pokousali	pokousat	k5eAaPmAgMnP
psi	pes	k1gMnPc1
<g/>
,	,	kIx,
pomohli	pomoct	k5eAaPmAgMnP
jí	on	k3xPp3gFnSc3
v	v	k7c6
zoo	zoo	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
V	v	k7c6
lesích	les	k1gInPc6
je	být	k5eAaImIp3nS
historicky	historicky	k6eAd1
nejvíce	hodně	k6eAd3,k6eAd1
srn	srna	k1gFnPc2
|	|	kIx~
Domov	domov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-01-05	2007-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
JACQUES	Jacques	k1gMnSc1
<g/>
,	,	kIx,
Kristi	Kristi	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Capreolus	Capreolus	k1gMnSc1
capreolus	capreolus	k1gMnSc1
(	(	kIx(
<g/>
western	western	k1gInSc1
roe	roe	k?
deer	deer	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Animal	animal	k1gMnSc1
Diversity	Diversit	k1gInPc4
Web	web	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Roe	Roe	k1gMnSc1
Deer	Deer	k1gMnSc1
(	(	kIx(
<g/>
Capreolus	Capreolus	k1gMnSc1
capreolus	capreolus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
WILD	WILD	kA
Jaeger	Jaegra	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-04-15	2013-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
srnec	srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
srnec	srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
srnec	srnec	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Taxon	taxon	k1gInSc1
Capreolus	Capreolus	k1gInSc1
capreolus	capreolus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Srnec	Srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Příroda	příroda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Biolib	Biolib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Lovná	lovný	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Pernatá	pernatý	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
</s>
<s>
bažant	bažant	k1gMnSc1
královský	královský	k2eAgMnSc1d1
<g/>
*	*	kIx~
•	•	k?
bažant	bažant	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
•	•	k?
hrdlička	hrdlička	k1gFnSc1
zahradní	zahradní	k2eAgFnSc1d1
•	•	k?
holub	holub	k1gMnSc1
hřivnáč	hřivnáč	k1gMnSc1
•	•	k?
husa	husa	k1gFnSc1
běločelá	běločelý	k2eAgFnSc1d1
•	•	k?
husa	husa	k1gFnSc1
polní	polní	k2eAgFnSc1d1
•	•	k?
husa	husa	k1gFnSc1
velká	velký	k2eAgFnSc1d1
•	•	k?
kachna	kachna	k1gFnSc1
divoká	divoký	k2eAgFnSc1d1
•	•	k?
krocan	krocan	k1gMnSc1
divoký	divoký	k2eAgMnSc1d1
<g/>
*	*	kIx~
•	•	k?
lyska	lyska	k1gFnSc1
černá	černý	k2eAgFnSc1d1
•	•	k?
perlička	perlička	k1gFnSc1
kropenatá	kropenatý	k2eAgFnSc1d1
<g/>
*	*	kIx~
•	•	k?
polák	polák	k1gInSc1
chocholačka	chocholačka	k1gFnSc1
•	•	k?
polák	polák	k1gInSc1
velký	velký	k2eAgInSc1d1
•	•	k?
straka	straka	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
•	•	k?
vrána	vrána	k1gFnSc1
obecná	obecný	k2eAgFnSc1d1
Srstnatá	srstnatý	k2eAgFnSc1d1
zvěř	zvěř	k1gFnSc1
</s>
<s>
daněk	daněk	k1gMnSc1
skvrnitý	skvrnitý	k2eAgMnSc1d1
•	•	k?
jelen	jelen	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
•	•	k?
jelenec	jelenec	k1gMnSc1
běloocasý	běloocasý	k2eAgMnSc1d1
<g/>
*	*	kIx~
•	•	k?
jezevec	jezevec	k1gMnSc1
lesní	lesní	k2eAgMnSc1d1
•	•	k?
kamzík	kamzík	k1gMnSc1
horský	horský	k2eAgMnSc1d1
•	•	k?
koza	koza	k1gFnSc1
bezoárová	bezoárový	k2eAgFnSc1d1
<g/>
*	*	kIx~
•	•	k?
králík	králík	k1gMnSc1
divoký	divoký	k2eAgMnSc1d1
•	•	k?
kuna	kuna	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
•	•	k?
kuna	kuna	k1gFnSc1
skalní	skalní	k2eAgFnSc1d1
•	•	k?
liška	liška	k1gFnSc1
obecná	obecná	k1gFnSc1
•	•	k?
muflon	muflon	k1gMnSc1
•	•	k?
ondatra	ondatra	k1gFnSc1
pižmová	pižmový	k2eAgFnSc1d1
•	•	k?
prase	prase	k1gNnSc4
divoké	divoký	k2eAgFnSc2d1
•	•	k?
sika	sika	k1gMnSc1
Dybowského	Dybowský	k1gMnSc2
<g/>
*	*	kIx~
•	•	k?
sika	sika	k1gMnSc1
japonský	japonský	k2eAgMnSc1d1
<g/>
*	*	kIx~
•	•	k?
srnec	srnec	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
•	•	k?
zajíc	zajíc	k1gMnSc1
polní	polní	k2eAgMnSc1d1
*	*	kIx~
Druh	druh	k1gInSc1
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
nepůvodní	původní	k2eNgInSc4d1
<g/>
,	,	kIx,
volně	volně	k6eAd1
se	se	k3xPyFc4
nevyskytující	vyskytující	k2eNgMnSc1d1
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
jen	jen	k9
ve	v	k7c6
velmi	velmi	k6eAd1
malém	malý	k2eAgInSc6d1
počtu	počet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Živočichové	živočich	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4049077-4	4049077-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85114827	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85114827	#num#	k4
</s>
