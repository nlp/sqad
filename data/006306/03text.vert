<s>
ČT1	ČT1	k4	ČT1
(	(	kIx(	(
<g/>
též	též	k9	též
jednička	jednička	k1gFnSc1	jednička
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
kanál	kanál	k1gInSc4	kanál
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
ČT1	ČT1	k4	ČT1
představuje	představovat	k5eAaImIp3nS	představovat
mainstreamový	mainstreamový	k2eAgInSc1d1	mainstreamový
program	program	k1gInSc1	program
s	s	k7c7	s
programovým	programový	k2eAgNnSc7d1	programové
schématem	schéma	k1gNnSc7	schéma
zaměřeným	zaměřený	k2eAgNnSc7d1	zaměřené
na	na	k7c6	na
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
vysílá	vysílat	k5eAaImIp3nS	vysílat
české	český	k2eAgInPc4d1	český
i	i	k8xC	i
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
filmy	film	k1gInPc4	film
a	a	k8xC	a
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
<g/>
,	,	kIx,	,
zábavné	zábavný	k2eAgInPc4d1	zábavný
a	a	k8xC	a
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
pořady	pořad	k1gInPc4	pořad
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
pořadů	pořad	k1gInPc2	pořad
tří	tři	k4xCgNnPc2	tři
televizních	televizní	k2eAgNnPc2d1	televizní
studií	studio	k1gNnPc2	studio
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
jako	jako	k9	jako
ČST	ČST	kA	ČST
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
Svátek	svátek	k1gInSc4	svátek
práce	práce	k1gFnSc2	práce
<g/>
)	)	kIx)	)
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
vysílání	vysílání	k1gNnSc1	vysílání
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1954	[number]	k4	1954
(	(	kIx(	(
<g/>
výročí	výročí	k1gNnSc2	výročí
"	"	kIx"	"
<g/>
Vítězného	vítězný	k2eAgInSc2d1	vítězný
února	únor	k1gInSc2	únor
dle	dle	k7c2	dle
socialistického	socialistický	k2eAgInSc2d1	socialistický
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
stanice	stanice	k1gFnSc2	stanice
na	na	k7c4	na
ČST	ČST	kA	ČST
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
vysílá	vysílat	k5eAaImIp3nS	vysílat
barevně	barevně	k6eAd1	barevně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
stanice	stanice	k1gFnSc1	stanice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federalizace	federalizace	k1gFnSc2	federalizace
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
Československa	Československo	k1gNnSc2	Československo
byla	být	k5eAaImAgFnS	být
F1	F1	k1gFnSc1	F1
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
ČT2	ČT2	k1gFnPc4	ČT2
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
STV2	STV2	k1gFnSc4	STV2
pro	pro	k7c4	pro
Slovenskou	slovenský	k2eAgFnSc4d1	slovenská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
došlo	dojít	k5eAaPmAgNnS	dojít
též	též	k6eAd1	též
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
frekvencí	frekvence	k1gFnPc2	frekvence
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
stanice	stanice	k1gFnPc1	stanice
vysílaly	vysílat	k5eAaImAgFnP	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
existující	existující	k2eAgFnSc2d1	existující
stanice	stanice	k1gFnSc2	stanice
ČT3	ČT3	k1gFnPc2	ČT3
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
frekvenci	frekvence	k1gFnSc4	frekvence
nyní	nyní	k6eAd1	nyní
vysílala	vysílat	k5eAaImAgFnS	vysílat
ČT	ČT	kA	ČT
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
frekvenci	frekvence	k1gFnSc6	frekvence
ČT2	ČT2	k1gFnSc1	ČT2
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
nově	nově	k6eAd1	nově
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
stanice	stanice	k1gFnSc1	stanice
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
stanice	stanice	k1gFnSc1	stanice
ČT2	ČT2	k1gFnSc2	ČT2
tak	tak	k6eAd1	tak
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c4	na
původně	původně	k6eAd1	původně
třetím	třetí	k4xOgMnSc6	třetí
federálním	federální	k2eAgInSc6d1	federální
okruhu	okruh	k1gInSc6	okruh
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
stanice	stanice	k1gFnSc1	stanice
ČT1	ČT1	k1gFnSc2	ČT1
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c6	na
původním	původní	k2eAgInSc6d1	původní
druhém	druhý	k4xOgInSc6	druhý
kanálu	kanál	k1gInSc6	kanál
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Studio	studio	k1gNnSc1	studio
6	[number]	k4	6
-	-	kIx~	-
Tříhodinové	tříhodinový	k2eAgNnSc1d1	tříhodinové
zpravodajské	zpravodajský	k2eAgNnSc1d1	zpravodajské
studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
všední	všednět	k5eAaImIp3nS	všednět
den	den	k1gInSc4	den
v	v	k7c6	v
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
-	-	kIx~	-
moderátoři	moderátor	k1gMnPc1	moderátor
s	s	k7c7	s
hosty	host	k1gMnPc7	host
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dnešek	dnešek	k1gInSc1	dnešek
přinese	přinést	k5eAaPmIp3nS	přinést
nového	nový	k2eAgMnSc4d1	nový
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
sport	sport	k1gInSc1	sport
vč.	vč.	k?	vč.
počasí	počasí	k1gNnSc2	počasí
a	a	k8xC	a
dopravy	doprava	k1gFnSc2	doprava
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Zprávy	zpráva	k1gFnPc1	zpráva
ve	v	k7c6	v
12	[number]	k4	12
-	-	kIx~	-
Zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
relace	relace	k1gFnSc1	relace
včetně	včetně	k7c2	včetně
sportovních	sportovní	k2eAgFnPc2d1	sportovní
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
počasí	počasí	k1gNnSc1	počasí
-	-	kIx~	-
každý	každý	k3xTgInSc4	každý
všední	všední	k2eAgInSc4d1	všední
den	den	k1gInSc4	den
ve	v	k7c6	v
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
na	na	k7c6	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Zprávy	zpráva	k1gFnPc1	zpráva
ve	v	k7c6	v
13	[number]	k4	13
-	-	kIx~	-
Zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
relace	relace	k1gFnSc1	relace
včetně	včetně	k7c2	včetně
sportovních	sportovní	k2eAgFnPc2d1	sportovní
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
počasí	počasí	k1gNnSc2	počasí
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
ve	v	k7c4	v
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
na	na	k7c6	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Události	událost	k1gFnPc1	událost
v	v	k7c6	v
regionech	region	k1gInPc6	region
-	-	kIx~	-
souhrn	souhrn	k1gInSc1	souhrn
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
krajů	kraj	k1gInPc2	kraj
ČR	ČR	kA	ČR
-	-	kIx~	-
vysílány	vysílán	k2eAgFnPc1d1	vysílána
také	také	k9	také
Události	událost	k1gFnPc1	událost
v	v	k7c6	v
regionech	region	k1gInPc6	region
plus	plus	k1gNnSc1	plus
(	(	kIx(	(
<g/>
také	také	k9	také
na	na	k7c4	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Události	událost	k1gFnPc1	událost
-	-	kIx~	-
hlavní	hlavní	k2eAgFnSc1d1	hlavní
relace	relace	k1gFnSc1	relace
<g />
.	.	kIx.	.
</s>
<s>
včetně	včetně	k7c2	včetně
Počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
(	(	kIx(	(
<g/>
také	také	k9	také
na	na	k7c4	na
ČT	ČT	kA	ČT
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
Branky	branka	k1gFnPc1	branka
<g/>
,	,	kIx,	,
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
vteřiny	vteřina	k1gFnPc1	vteřina
-	-	kIx~	-
sportovní	sportovní	k2eAgFnSc1d1	sportovní
relace	relace	k1gFnSc1	relace
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
den	den	k1gInSc1	den
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
Černé	Černé	k2eAgFnSc2d1	Černé
ovce	ovce	k1gFnSc2	ovce
-	-	kIx~	-
relace	relace	k1gFnSc1	relace
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
odhalování	odhalování	k1gNnSc4	odhalování
<g />
.	.	kIx.	.
</s>
<s>
podvodníků	podvodník	k1gMnPc2	podvodník
Reportéři	reportér	k1gMnPc1	reportér
ČT	ČT	kA	ČT
-	-	kIx~	-
reportáže	reportáž	k1gFnPc1	reportáž
z	z	k7c2	z
ČR	ČR	kA	ČR
i	i	k9	i
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Máte	mít	k5eAaImIp2nP	mít
slovo	slovo	k1gNnSc1	slovo
-	-	kIx~	-
diskuzní	diskuzní	k2eAgInSc1d1	diskuzní
pořad	pořad	k1gInSc1	pořad
s	s	k7c7	s
moderátorkou	moderátorka	k1gFnSc7	moderátorka
M.	M.	kA	M.
Jílkovou	Jílková	k1gFnSc7	Jílková
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zapojit	zapojit	k5eAaPmF	zapojit
i	i	k9	i
diváci	divák	k1gMnPc1	divák
pomocí	pomoc	k1gFnPc2	pomoc
SMS	SMS	kA	SMS
Otázky	otázka	k1gFnPc1	otázka
Václava	Václav	k1gMnSc2	Václav
Moravce	Moravec	k1gMnSc2	Moravec
-	-	kIx~	-
dizkuzní	dizkuzní	k2eAgInSc1d1	dizkuzní
pořad	pořad	k1gInSc1	pořad
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
Žižkovské	žižkovský	k2eAgFnSc2d1	Žižkovská
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
moderuje	moderovat	k5eAaBmIp3nS	moderovat
V.	V.	kA	V.
Moravec	Moravec	k1gMnSc1	Moravec
Na	na	k7c6	na
stopě	stopa	k1gFnSc6	stopa
-	-	kIx~	-
po	po	k7c6	po
kom	kdo	k3yInSc6	kdo
<g />
.	.	kIx.	.
</s>
<s>
policie	policie	k1gFnSc1	policie
pátrá	pátrat	k5eAaImIp3nS	pátrat
168	[number]	k4	168
hodin	hodina	k1gFnPc2	hodina
-	-	kIx~	-
nedělník	nedělník	k1gInSc1	nedělník
<g/>
,	,	kIx,	,
shrnutí	shrnutí	k1gNnSc1	shrnutí
informací	informace	k1gFnPc2	informace
z	z	k7c2	z
uplynulého	uplynulý	k2eAgInSc2d1	uplynulý
týdne	týden	k1gInSc2	týden
Gejzír	gejzír	k1gInSc1	gejzír
-	-	kIx~	-
moderní	moderní	k2eAgInSc1d1	moderní
magazín	magazín	k1gInSc1	magazín
s	s	k7c7	s
pěticí	pětice	k1gFnSc7	pětice
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
reportáží	reportáž	k1gFnPc2	reportáž
Z	z	k7c2	z
metropole	metropol	k1gFnSc2	metropol
-	-	kIx~	-
reportáže	reportáž	k1gFnPc4	reportáž
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
tipy	tip	k1gInPc4	tip
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Praha	Praha	k1gFnSc1	Praha
Polopatě	Polopata	k1gFnSc3	Polopata
-	-	kIx~	-
hobby	hobby	k1gNnSc4	hobby
magazín	magazín	k1gInSc4	magazín
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
domům	dům	k1gInPc3	dům
<g/>
,	,	kIx,	,
bytům	byt	k1gInPc3	byt
a	a	k8xC	a
zahradám	zahrada	k1gFnPc3	zahrada
Hobby	hobby	k1gNnSc2	hobby
naší	náš	k3xOp1gFnSc2	náš
doby	doba	k1gFnSc2	doba
-	-	kIx~	-
magazín	magazín	k1gInSc1	magazín
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
co	co	k3yInSc4	co
rádi	rád	k2eAgMnPc1d1	rád
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
a	a	k8xC	a
zahradničí	zahradničit	k5eAaImIp3nS	zahradničit
DoktorKA	doktorka	k1gFnSc1	doktorka
-	-	kIx~	-
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
zajímavosti	zajímavost	k1gFnPc4	zajímavost
o	o	k7c6	o
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
v	v	k7c6	v
podání	podání	k1gNnSc6	podání
doktorky	doktorka	k1gFnSc2	doktorka
M.	M.	kA	M.
Cajthamlové	Cajthamlové	k2eAgFnSc1d1	Cajthamlové
Kočka	kočka	k1gFnSc1	kočka
není	být	k5eNaImIp3nS	být
pes	pes	k1gMnSc1	pes
-	-	kIx~	-
cyklus	cyklus	k1gInSc1	cyklus
věnující	věnující	k2eAgInSc1d1	věnující
se	s	k7c7	s
převýchově	převýchova	k1gFnSc6	převýchova
neposlušným	poslušný	k2eNgMnPc3d1	neposlušný
psům	pes	k1gMnPc3	pes
a	a	k8xC	a
kočkám	kočka	k1gFnPc3	kočka
Kluci	kluk	k1gMnPc1	kluk
v	v	k7c6	v
akci	akce	k1gFnSc6	akce
-	-	kIx~	-
magazín	magazín	k1gInSc1	magazín
o	o	k7c4	o
vaření	vaření	k1gNnSc4	vaření
a	a	k8xC	a
kuchyni	kuchyně	k1gFnSc4	kuchyně
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
z	z	k7c2	z
regionů	region	k1gInPc2	region
Kalendárium	kalendárium	k1gNnSc1	kalendárium
-	-	kIx~	-
magazín	magazín	k1gInSc1	magazín
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
Toulavá	toulavý	k2eAgFnSc1d1	Toulavá
kamera	kamera	k1gFnSc1	kamera
-	-	kIx~	-
tipy	tip	k1gInPc1	tip
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
a	a	k8xC	a
výlety	výlet	k1gInPc4	výlet
po	po	k7c6	po
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Objektiv	objektiv	k1gInSc1	objektiv
-	-	kIx~	-
magazín	magazín	k1gInSc1	magazín
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
zajímavostech	zajímavost	k1gFnPc6	zajímavost
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
bylinky	bylinka	k1gFnSc2	bylinka
-	-	kIx~	-
pořad	pořad	k1gInSc1	pořad
o	o	k7c6	o
pěstování	pěstování	k1gNnSc6	pěstování
a	a	k8xC	a
zajímavostech	zajímavost	k1gFnPc6	zajímavost
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
bylinek	bylinka	k1gFnPc2	bylinka
AZ	AZ	kA	AZ
-	-	kIx~	-
kvíz	kvíz	k1gInSc1	kvíz
-	-	kIx~	-
divácky	divácky	k6eAd1	divácky
populární	populární	k2eAgInSc1d1	populární
televizní	televizní	k2eAgInSc1d1	televizní
kvíz	kvíz	k1gInSc1	kvíz
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc1	můj
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
pořad	pořad	k1gInSc1	pořad
o	o	k7c6	o
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Sama	sám	k3xTgFnSc1	sám
doma	doma	k6eAd1	doma
-	-	kIx~	-
odpolední	odpolední	k2eAgNnSc1d1	odpolední
studio	studio	k1gNnSc1	studio
věnující	věnující	k2eAgNnSc1d1	věnující
se	s	k7c7	s
ženským	ženský	k2eAgNnSc7d1	ženské
tématům	téma	k1gNnPc3	téma
Pošta	pošta	k1gFnSc1	pošta
pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
-	-	kIx~	-
pořad	pořad	k1gInSc1	pořad
odhalující	odhalující	k2eAgInPc1d1	odhalující
osudy	osud	k1gInPc1	osud
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
moderuje	moderovat	k5eAaBmIp3nS	moderovat
E.	E.	kA	E.
Janečková	Janečková	k1gFnSc1	Janečková
Všechnopárty	Všechnopárta	k1gFnSc2	Všechnopárta
-	-	kIx~	-
talkshow	talkshow	k?	talkshow
K.	K.	kA	K.
Šípa	Šíp	k1gMnSc2	Šíp
Zázraky	zázrak	k1gInPc1	zázrak
přírody	příroda	k1gFnSc2	příroda
Tajemství	tajemství	k1gNnSc2	tajemství
těla	tělo	k1gNnSc2	tělo
ČT1	ČT1	k1gFnSc2	ČT1
vysílá	vysílat	k5eAaImIp3nS	vysílat
opakovaně	opakovaně	k6eAd1	opakovaně
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
seriály	seriál	k1gInPc4	seriál
<g/>
,	,	kIx,	,
pohádky	pohádka	k1gFnPc4	pohádka
a	a	k8xC	a
televizní	televizní	k2eAgFnPc4d1	televizní
hry	hra	k1gFnPc4	hra
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Televariété	Televariéta	k1gMnPc1	Televariéta
<g/>
,	,	kIx,	,
Panoptikum	panoptikum	k1gNnSc1	panoptikum
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
ČT1	ČT1	k4	ČT1
pravidelně	pravidelně	k6eAd1	pravidelně
vysílá	vysílat	k5eAaImIp3nS	vysílat
oceňované	oceňovaný	k2eAgInPc4d1	oceňovaný
české	český	k2eAgInPc4d1	český
a	a	k8xC	a
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
seriálů	seriál	k1gInPc2	seriál
výsílá	výsílat	k5eAaImIp3nS	výsílat
např.	např.	kA	např.
Labyrint	labyrint	k1gInSc1	labyrint
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
pod	pod	k7c7	pod
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Rapl	Rapl	k?	Rapl
<g/>
,	,	kIx,	,
Případy	případ	k1gInPc1	případ
1	[number]	k4	1
<g/>
.	.	kIx.	.
oddělení	oddělení	k1gNnSc6	oddělení
<g/>
,	,	kIx,	,
Doktor	doktor	k1gMnSc1	doktor
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Četníci	četník	k1gMnPc1	četník
z	z	k7c2	z
Luhačovic	Luhačovice	k1gFnPc2	Luhačovice
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
např.	např.	kA	např.
Hercule	Hercule	k1gFnPc1	Hercule
Poirot	Poirot	k1gMnSc1	Poirot
<g/>
,	,	kIx,	,
Místo	místo	k7c2	místo
činu	čin	k1gInSc2	čin
<g/>
:	:	kIx,	:
Schimanski	Schimansk	k1gFnSc2	Schimansk
<g/>
,	,	kIx,	,
Mladý	mladý	k2eAgMnSc1d1	mladý
Montalbano	Montalbana	k1gFnSc5	Montalbana
<g/>
,	,	kIx,	,
Soudkyně	soudkyně	k1gFnSc1	soudkyně
Amy	Amy	k1gFnSc1	Amy
nebo	nebo	k8xC	nebo
Taggart	Taggart	k1gInSc1	Taggart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
ČR	ČR	kA	ČR
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
veřejnoprávního	veřejnoprávní	k2eAgInSc2d1	veřejnoprávní
multiplexu	multiplex	k1gInSc2	multiplex
1	[number]	k4	1
v	v	k7c6	v
pozemním	pozemní	k2eAgNnSc6d1	pozemní
digitálním	digitální	k2eAgNnSc6d1	digitální
vysílání	vysílání	k1gNnSc6	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
Analogové	analogový	k2eAgNnSc1d1	analogové
vysílání	vysílání	k1gNnSc1	vysílání
ČT1	ČT1	k1gFnSc2	ČT1
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ČT1	ČT1	k1gFnSc6	ČT1
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
must	must	k1gMnSc1	must
carry	carra	k1gMnSc2	carra
<g/>
"	"	kIx"	"
povinnost	povinnost	k1gFnSc1	povinnost
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
satelitní	satelitní	k2eAgInSc1d1	satelitní
<g/>
,	,	kIx,	,
kabelové	kabelový	k2eAgFnSc2d1	kabelová
a	a	k8xC	a
IPTV	IPTV	kA	IPTV
operátory	operátor	k1gInPc1	operátor
(	(	kIx(	(
<g/>
musí	muset	k5eAaImIp3nP	muset
ji	on	k3xPp3gFnSc4	on
obsahovat	obsahovat	k5eAaImF	obsahovat
základní	základní	k2eAgFnSc1d1	základní
nabídka	nabídka	k1gFnSc1	nabídka
programů	program	k1gInPc2	program
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednička	jednička	k1gFnSc1	jednička
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
kanál	kanál	k1gInSc4	kanál
ČT1	ČT1	k1gFnSc2	ČT1
HD	HD	kA	HD
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
obrazu	obraz	k1gInSc2	obraz
1080	[number]	k4	1080
<g/>
i.	i.	k?	i.
Tento	tento	k3xDgInSc4	tento
kanál	kanál	k1gInSc4	kanál
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pozemním	pozemní	k2eAgNnSc6d1	pozemní
vysílání	vysílání	k1gNnSc6	vysílání
možno	možno	k6eAd1	možno
přijímat	přijímat	k5eAaImF	přijímat
přes	přes	k7c4	přes
Regionální	regionální	k2eAgFnSc4d1	regionální
síť	síť	k1gFnSc4	síť
7	[number]	k4	7
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
přes	přes	k7c4	přes
DVB-S	DVB-S	k1gMnSc4	DVB-S
<g/>
,	,	kIx,	,
DVB-C	DVB-C	k1gMnSc4	DVB-C
a	a	k8xC	a
IPTV	IPTV	kA	IPTV
<g/>
.	.	kIx.	.
</s>
<s>
ČT1	ČT1	k4	ČT1
HD	HD	kA	HD
vysílá	vysílat	k5eAaImIp3nS	vysílat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nahradil	nahradit	k5eAaPmAgInS	nahradit
kanál	kanál	k1gInSc1	kanál
ČT	ČT	kA	ČT
HD	HD	kA	HD
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
názvu	název	k1gInSc2	název
týkala	týkat	k5eAaImAgNnP	týkat
hlavně	hlavně	k9	hlavně
obsahu	obsah	k1gInSc2	obsah
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
,	,	kIx,	,
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
data	datum	k1gNnSc2	datum
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c4	na
ČT	ČT	kA	ČT
HD	HD	kA	HD
ke	k	k7c3	k
střídání	střídání	k1gNnSc3	střídání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
února	únor	k1gInSc2	únor
bylo	být	k5eAaImAgNnS	být
vysílání	vysílání	k1gNnSc1	vysílání
ČT1	ČT1	k1gFnSc2	ČT1
HD	HD	kA	HD
možné	možný	k2eAgFnPc1d1	možná
zachytit	zachytit	k5eAaPmF	zachytit
také	také	k9	také
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Multiplexu	multiplex	k1gInSc2	multiplex
4	[number]	k4	4
společnosti	společnost	k1gFnSc2	společnost
Digital	Digital	kA	Digital
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
zkušební	zkušební	k2eAgNnSc4d1	zkušební
vysílání	vysílání	k1gNnSc4	vysílání
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
<g/>
.	.	kIx.	.
</s>
