<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
SvatáMatka	SvatáMatka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Terezie	Terezie	k1gFnSc1
z	z	k7c2
Kalkaty	Kalkata	k1gFnSc2
<g/>
)	)	kIx)
<g/>
Generální	generální	k2eAgFnSc1d1
představená	představená	k1gFnSc1
kongregaceMisionářek	kongregaceMisionářka	k1gFnPc2
milosrdenství	milosrdenství	k1gNnPc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgNnPc1d1
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Kalkata	Kalkata	k1gFnSc1
Období	období	k1gNnSc1
služby	služba	k1gFnSc2
</s>
<s>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
Nástupkyně	nástupkyně	k1gFnSc1
</s>
<s>
Nirmala	Nirmat	k5eAaImAgFnS,k5eAaPmAgFnS
Joshi	Joshi	k1gNnSc7
<g/>
,	,	kIx,
M.	M.	kA
<g/>
C.	C.	kA
Zasvěcený	zasvěcený	k2eAgInSc4d1
život	život	k1gInSc4
Institut	institut	k1gInSc1
</s>
<s>
Kongregace	kongregace	k1gFnSc1
Loretánských	loretánský	k2eAgFnPc2d1
sester	sestra	k1gFnPc2
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
Misionářky	misionářka	k1gFnPc1
milosrdenství	milosrdenství	k1gNnSc2
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
Postulát	postulát	k1gInSc1
</s>
<s>
1928	#num#	k4
Noviciát	noviciát	k1gInSc1
</s>
<s>
1929	#num#	k4
Sliby	slib	k1gInPc1
</s>
<s>
dočasné	dočasný	k2eAgNnSc1d1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1931	#num#	k4
doživotní	doživotní	k2eAgFnSc4d1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1937	#num#	k4
Změna	změna	k1gFnSc1
</s>
<s>
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
po	po	k7c6
dohodě	dohoda	k1gFnSc6
opustila	opustit	k5eAaPmAgFnS
loretánský	loretánský	k2eAgInSc4d1
klášter	klášter	k1gInSc4
<g/>
;	;	kIx,
dále	daleko	k6eAd2
se	se	k3xPyFc4
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
chudým	chudý	k2eAgMnSc7d1
<g/>
;	;	kIx,
od	od	k7c2
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1950	#num#	k4
představená	představená	k1gFnSc1
kongregace	kongregace	k1gFnSc2
Misionářek	misionářka	k1gFnPc2
milosrdenství	milosrdenství	k1gNnSc2
Osobní	osobní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Rodné	rodný	k2eAgInPc1d1
jméno	jméno	k1gNnSc4
</s>
<s>
Agnesë	Agnesë	k?
Gonxhe	Gonxhe	k1gInSc1
Bojaxhiu	Bojaxhium	k1gNnSc6
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1910	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Skopje	Skopje	k1gFnSc1
<g/>
,	,	kIx,
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Osmanská	osmanský	k2eAgFnSc1d1
říše	říš	k1gFnSc2
Datum	datum	k1gNnSc4
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1997	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
87	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Kalkata	Kalkata	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Kalkata	Kalkata	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
otomanská	otomanský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
–	–	k?
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
srbská	srbský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
–	–	k?
<g/>
1915	#num#	k4
<g/>
)	)	kIx)
<g/>
bulharská	bulharský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1915	#num#	k4
<g/>
–	–	k?
<g/>
1918	#num#	k4
<g/>
)	)	kIx)
<g/>
jugoslávská	jugoslávský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
<g/>
indická	indický	k2eAgFnSc1d1
(	(	kIx(
<g/>
1948	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
Známý	známý	k2eAgInSc1d1
díky	díky	k7c3
</s>
<s>
zakladatelka	zakladatelka	k1gFnSc1
kongregace	kongregace	k1gFnSc2
Misionářek	misionářka	k1gFnPc2
milosrdenství	milosrdenství	k1gNnSc4
Řády	řád	k1gInPc4
a	a	k8xC
ocenění	ocenění	k1gNnSc4
</s>
<s>
Templetonova	Templetonův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
Nobelova	Nobelův	k2eAgFnSc1d1
cena	cena	k1gFnSc1
míru	mír	k1gInSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
Britský	britský	k2eAgInSc1d1
Řád	řád	k1gInSc1
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
Nejvyšší	vysoký	k2eAgNnSc1d3
indické	indický	k2eAgNnSc1d1
civilní	civilní	k2eAgNnSc1d1
vyznamenání	vyznamenání	k1gNnSc1
Bharat	Bharat	k1gInSc1
Ratna	Ratna	k1gFnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
Svatořečení	svatořečení	k1gNnSc1
Beatifikace	beatifikace	k1gFnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
Řím	Řím	k1gInSc1
beatifikoval	beatifikovat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanonizace	kanonizace	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2016	#num#	k4
Řím	Řím	k1gInSc1
kanonizoval	kanonizovat	k5eAaBmAgMnS
František	František	k1gMnSc1
Svátek	Svátek	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nP
Atributy	atribut	k1gInPc4
</s>
<s>
bílé	bílý	k2eAgNnSc1d1
sárí	sárí	k1gNnSc1
s	s	k7c7
modrým	modrý	k2eAgInSc7d1
lemem	lem	k1gInSc7
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
citáty	citát	k1gInPc7
na	na	k7c6
Wikicitátech	Wikicitát	k1gInPc6
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
(	(	kIx(
<g/>
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Agnesë	Agnesë	k1gFnSc2
Gonxhe	Gonxhe	k1gNnSc3
Bojaxhiu	Bojaxhium	k1gNnSc3
[	[	kIx(
<g/>
gondže	gondzat	k5eAaPmIp3nS
bojadžiu	bojadžius	k1gMnSc3
<g/>
]	]	kIx)
<g/>
;	;	kIx,
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1910	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1997	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
humanitární	humanitární	k2eAgFnSc1d1
pracovnice	pracovnice	k1gFnSc1
a	a	k8xC
řeholnice	řeholnice	k1gFnSc1
albánského	albánský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
působila	působit	k5eAaImAgFnS
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
smrti	smrt	k1gFnSc6
kanonizována	kanonizován	k2eAgFnSc1d1
římskokatolickou	římskokatolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zakladatelkou	zakladatelka	k1gFnSc7
a	a	k8xC
první	první	k4xOgFnSc7
představenou	představená	k1gFnSc7
řádu	řád	k1gInSc2
Misionářek	misionářka	k1gFnPc2
lásky	láska	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
své	svůj	k3xOyFgNnSc4
humanitární	humanitární	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
získala	získat	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
ocenění	ocenění	k1gNnSc2
<g/>
:	:	kIx,
Templetonovy	Templetonův	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
první	první	k4xOgFnSc1
nositelka	nositelka	k1gFnSc1
vůbec	vůbec	k9
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
míru	mír	k1gInSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
britského	britský	k2eAgInSc2d1
Řádu	řád	k1gInSc2
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
nejvyššího	vysoký	k2eAgNnSc2d3
indického	indický	k2eAgNnSc2d1
civilního	civilní	k2eAgNnSc2d1
vyznamenání	vyznamenání	k1gNnSc2
Bharat	Bharat	k1gMnSc1
Ratna	Ratn	k1gMnSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blahoslavenou	blahoslavený	k2eAgFnSc7d1
byla	být	k5eAaImAgNnP
prohlášena	prohlášen	k2eAgFnSc1d1
roku	rok	k1gInSc2
2003	#num#	k4
a	a	k8xC
svatořečena	svatořečen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS
se	s	k7c7
26	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
roku	rok	k1gInSc2
1910	#num#	k4
v	v	k7c6
albánské	albánský	k2eAgFnSc6d1
rodině	rodina	k1gFnSc6
jako	jako	k8xC,k8xS
nejmladší	mladý	k2eAgFnSc1d3
dcera	dcera	k1gFnSc1
úspěšného	úspěšný	k2eAgMnSc2d1
obchodníka	obchodník	k1gMnSc2
a	a	k8xC
farmaceuta	farmaceut	k1gMnSc2
Nikola	Nikola	k1gFnSc1
Bojaxhiu	Bojaxhium	k1gNnSc3
a	a	k8xC
Drane	Dran	k1gInSc5
Bojaxhiu	Bojaxhius	k1gMnSc3
(	(	kIx(
<g/>
Barnaj	Barnaj	k1gMnSc1
rodné	rodný	k2eAgNnSc4d1
příjmení	příjmení	k1gNnSc4
<g/>
)	)	kIx)
ve	v	k7c6
městě	město	k1gNnSc6
Shkupi	Shkup	k1gFnSc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc2d1
Skopje	Skopje	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rodiště	rodiště	k1gNnSc1
tehdy	tehdy	k6eAd1
náleželo	náležet	k5eAaImAgNnS
v	v	k7c6
Osmanské	osmanský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
<g/>
,	,	kIx,
pak	pak	k6eAd1
Srbsku	Srbsko	k1gNnSc3
<g/>
,	,	kIx,
Jugoslávii	Jugoslávie	k1gFnSc3
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Severní	severní	k2eAgFnSc4d1
Makedonii	Makedonie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokřtěna	pokřtěn	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
jménem	jméno	k1gNnSc7
Agnese	Agnese	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
doma	doma	k6eAd1
jí	on	k3xPp3gFnSc3
říkali	říkat	k5eAaImAgMnP
Gonxha	Gonxha	k1gFnSc1
(	(	kIx(
<g/>
poupátko	poupátko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Otec	otec	k1gMnSc1
jí	jíst	k5eAaImIp3nS
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
jí	on	k3xPp3gFnSc3
bylo	být	k5eAaImAgNnS
osm	osm	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
dvanácti	dvanáct	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
pro	pro	k7c4
řeholní	řeholní	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
i	i	k9
první	první	k4xOgFnSc7
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
změnily	změnit	k5eAaPmAgInP
politickou	politický	k2eAgFnSc4d1
mapu	mapa	k1gFnSc4
světa	svět	k1gInSc2
a	a	k8xC
Skopje	Skopje	k1gFnSc2
připadlo	připadnout	k5eAaPmAgNnS
Srbsku	Srbsko	k1gNnSc3
<g/>
,	,	kIx,
po	po	k7c6
roce	rok	k1gInSc6
1918	#num#	k4
nově	nově	k6eAd1
vzniklému	vzniklý	k2eAgNnSc3d1
Království	království	k1gNnSc3
SHS	SHS	kA
<g/>
,	,	kIx,
od	od	k7c2
r.	r.	kA
1929	#num#	k4
Království	království	k1gNnPc2
Jugoslávie	Jugoslávie	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
události	událost	k1gFnPc1
rozptýlily	rozptýlit	k5eAaPmAgFnP
rodinu	rodina	k1gFnSc4
Bojaxhiuových	Bojaxhiuových	k2eAgFnSc4d1
po	po	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agnese	Agnese	k1gFnSc1
odešla	odejít	k5eAaPmAgFnS
do	do	k7c2
Irska	Irsko	k1gNnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
do	do	k7c2
kongregace	kongregace	k1gFnSc2
Loretánských	loretánský	k2eAgFnPc2d1
sester	sestra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
řeholní	řeholní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
bylo	být	k5eAaImAgNnS
aktivní	aktivní	k2eAgFnSc7d1
hlavně	hlavně	k6eAd1
v	v	k7c6
Indii	Indie	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
Agnesu	Agnesa	k1gFnSc4
přitahovalo	přitahovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
krátké	krátký	k2eAgFnSc6d1
přípravě	příprava	k1gFnSc6
v	v	k7c6
Dublinu	Dublin	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
Agnese	Agnese	k1gFnSc1
odjela	odjet	k5eAaPmAgFnS
do	do	k7c2
Indie	Indie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Dárdžilingu	Dárdžiling	k1gInSc6
složila	složit	k5eAaPmAgFnS
slib	slib	k1gInSc4
novicky	novicka	k1gFnSc2
a	a	k8xC
přijala	přijmout	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
misionářské	misionářský	k2eAgFnSc6d1
dívčí	dívčí	k2eAgFnSc6d1
škole	škola	k1gFnSc6
vyučovala	vyučovat	k5eAaImAgFnS
zeměpis	zeměpis	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nadání	nadání	k1gNnSc4
a	a	k8xC
organizační	organizační	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
bylo	být	k5eAaImAgNnS
Tereze	Tereza	k1gFnSc3
svěřeno	svěřen	k2eAgNnSc1d1
vedení	vedení	k1gNnSc1
Vyšší	vysoký	k2eAgFnSc2d2
misijní	misijní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
St.	st.	kA
Mary	Mary	k1gFnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
High	High	k1gMnSc1
School	School	k1gInSc1
v	v	k7c6
Kalkatě	Kalkata	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Založení	založení	k1gNnSc1
řádu	řád	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
sestra	sestra	k1gFnSc1
Tereza	Tereza	k1gFnSc1
po	po	k7c6
dohodě	dohoda	k1gFnSc6
s	s	k7c7
představenými	představená	k1gFnPc7
opustila	opustit	k5eAaPmAgFnS
loretánský	loretánský	k2eAgInSc4d1
klášter	klášter	k1gInSc4
a	a	k8xC
začala	začít	k5eAaPmAgFnS
se	se	k3xPyFc4
věnovat	věnovat	k5eAaPmF,k5eAaImF
nejchudším	chudý	k2eAgFnPc3d3
a	a	k8xC
umírajícím	umírající	k2eAgFnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
ji	on	k3xPp3gFnSc4
obklopil	obklopit	k5eAaPmAgInS
dostatek	dostatek	k1gInSc1
spolupracovnic	spolupracovnice	k1gFnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
mohla	moct	k5eAaImAgFnS
založit	založit	k5eAaPmF
kalkatskou	kalkatský	k2eAgFnSc4d1
kongregaci	kongregace	k1gFnSc4
Misionářek	misionářka	k1gFnPc2
milosrdenství	milosrdenství	k1gNnSc2
(	(	kIx(
<g/>
Misionářky	misionářka	k1gFnSc2
lásky	láska	k1gFnSc2
<g/>
,	,	kIx,
kongregace	kongregace	k1gFnSc1
Milosrdenství	milosrdenství	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Svatý	svatý	k2eAgInSc1d1
stolec	stolec	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1950	#num#	k4
uznal	uznat	k5eAaPmAgInS
nové	nový	k2eAgNnSc4d1
řeholní	řeholní	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
řídící	řídící	k2eAgFnSc4d1
se	s	k7c7
františkánskými	františkánský	k2eAgInPc7d1
principy	princip	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Tereza	Tereza	k1gFnSc1
zakládala	zakládat	k5eAaImAgFnS
útulky	útulek	k1gInPc4
pro	pro	k7c4
umírající	umírající	k1gFnPc4
<g/>
,	,	kIx,
sirotky	sirotka	k1gFnPc4
a	a	k8xC
opuštěné	opuštěný	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budovala	budovat	k5eAaImAgFnS
nemocnice	nemocnice	k1gFnPc4
a	a	k8xC
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
několika	několik	k4yIc2
roků	rok	k1gInPc2
vyvinula	vyvinout	k5eAaPmAgFnS
obrovské	obrovský	k2eAgNnSc4d1
úsilí	úsilí	k1gNnSc4
a	a	k8xC
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
položit	položit	k5eAaPmF
základy	základ	k1gInPc1
velkému	velký	k2eAgNnSc3d1
humanitárnímu	humanitární	k2eAgNnSc3d1
dílu	dílo	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
sklonku	sklonek	k1gInSc6
života	život	k1gInSc2
absolvovala	absolvovat	k5eAaPmAgFnS
řadu	řada	k1gFnSc4
cest	cesta	k1gFnPc2
po	po	k7c6
světě	svět	k1gInSc6
a	a	k8xC
využívala	využívat	k5eAaImAgFnS,k5eAaPmAgFnS
své	svůj	k3xOyFgFnPc4
popularity	popularita	k1gFnPc4
a	a	k8xC
možností	možnost	k1gFnSc7
setkávat	setkávat	k5eAaImF
se	se	k3xPyFc4
s	s	k7c7
významnými	významný	k2eAgFnPc7d1
osobnostmi	osobnost	k1gFnPc7
k	k	k7c3
podpoře	podpora	k1gFnSc3
sociálních	sociální	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
své	svůj	k3xOyFgFnSc2
kongregace	kongregace	k1gFnSc2
a	a	k8xC
šíření	šíření	k1gNnSc2
křesťanských	křesťanský	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítavý	odmítavý	k2eAgInSc4d1
postoj	postoj	k1gInSc4
k	k	k7c3
potratům	potrat	k1gInPc3
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
podstatnou	podstatný	k2eAgFnSc7d1
částí	část	k1gFnSc7
řeči	řeč	k1gFnSc2
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
udělení	udělení	k1gNnSc2
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potraty	potrat	k1gInPc4
označila	označit	k5eAaPmAgNnP
za	za	k7c4
„	„	k?
<g/>
největšího	veliký	k2eAgMnSc2d3
ničitele	ničitel	k1gMnSc2
míru	mír	k1gInSc2
současnosti	současnost	k1gFnSc2
<g/>
“	“	k?
a	a	k8xC
prohlásila	prohlásit	k5eAaPmAgFnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Může	moct	k5eAaImIp3nS
<g/>
-li	-li	k?
matka	matka	k1gFnSc1
zabít	zabít	k5eAaPmF
své	svůj	k3xOyFgNnSc4
dítě	dítě	k1gNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
brání	bránit	k5eAaImIp3nP
mně	já	k3xPp1nSc3
zabít	zabít	k5eAaPmF
vás	vy	k3xPp2nPc4
a	a	k8xC
vám	vy	k3xPp2nPc3
zabít	zabít	k5eAaPmF
mě	já	k3xPp1nSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
začalo	začít	k5eAaPmAgNnS
říkat	říkat	k5eAaImF
<g/>
,	,	kIx,
navštívila	navštívit	k5eAaPmAgFnS
ve	v	k7c6
světě	svět	k1gInSc6
mnoho	mnoho	k4c4
míst	místo	k1gNnPc2
lidského	lidský	k2eAgNnSc2d1
utrpení	utrpení	k1gNnSc2
<g/>
:	:	kIx,
osadu	osada	k1gFnSc4
s	s	k7c7
přesídlenci	přesídlenec	k1gMnPc7
po	po	k7c6
černobylské	černobylský	k2eAgFnSc6d1
havárii	havárie	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
obleženém	obležený	k2eAgInSc6d1
Bejrútu	Bejrút	k1gInSc6
vyhledávala	vyhledávat	k5eAaImAgFnS
děti	dítě	k1gFnPc4
ohrožené	ohrožený	k2eAgInPc4d1
válkou	válka	k1gFnSc7
–	–	k?
v	v	k7c6
rozvalinách	rozvalina	k1gFnPc6
města	město	k1gNnSc2
jich	on	k3xPp3gMnPc2
našla	najít	k5eAaPmAgFnS
na	na	k7c4
šedesát	šedesát	k4xCc4
zmrzačených	zmrzačený	k2eAgFnPc2d1
a	a	k8xC
nemocných	nemocný	k2eAgFnPc2d1,k2eNgFnPc2d1
a	a	k8xC
všechny	všechen	k3xTgInPc1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
podařilo	podařit	k5eAaPmAgNnS
evakuovat	evakuovat	k5eAaBmF
do	do	k7c2
bezpečí	bezpečí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
otázku	otázka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
šedesát	šedesát	k4xCc1
dětí	dítě	k1gFnPc2
není	být	k5eNaImIp3nS
málo	málo	k1gNnSc1
<g/>
,	,	kIx,
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
odpověděla	odpovědět	k5eAaPmAgFnS
<g/>
:	:	kIx,
Mě	já	k3xPp1nSc4
nezajímá	zajímat	k5eNaImIp3nS
dav	dav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdybych	kdyby	kYmCp1nS
viděla	vidět	k5eAaImAgFnS
davy	dav	k1gInPc4
ubožáků	ubožák	k1gMnPc2
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
bych	by	kYmCp1nS
nemohla	moct	k5eNaImAgFnS
svoji	svůj	k3xOyFgFnSc4
práci	práce	k1gFnSc4
začít	začít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímá	zajímat	k5eAaImIp3nS
mě	já	k3xPp1nSc4
jen	jen	k9
můj	můj	k3xOp1gMnSc1
bližní	bližní	k1gMnSc1
<g/>
,	,	kIx,
jednotlivec	jednotlivec	k1gMnSc1
<g/>
,	,	kIx,
tomu	ten	k3xDgNnSc3
mohu	moct	k5eAaImIp1nS
pomoci	pomoct	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
pomáhám	pomáhat	k5eAaImIp1nS
<g/>
…	…	k?
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Ohlas	ohlas	k1gInSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
při	při	k7c6
oslavách	oslava	k1gFnPc6
čtyřicátého	čtyřicátý	k4xOgNnSc2
výročí	výročí	k1gNnPc2
založení	založení	k1gNnSc2
OSN	OSN	kA
měl	mít	k5eAaImAgMnS
premiéru	premiéra	k1gFnSc4
dokumentární	dokumentární	k2eAgInSc4d1
film	film	k1gInSc4
s	s	k7c7
prostým	prostý	k2eAgInSc7d1
názvem	název	k1gInSc7
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorky	autorka	k1gFnPc4
filmu	film	k1gInSc2
<g/>
,	,	kIx,
známé	známý	k2eAgFnSc2d1
americké	americký	k2eAgFnSc2d1
dokumentaristky	dokumentaristka	k1gFnSc2
Ann	Ann	k1gFnSc2
a	a	k8xC
Janette	Janett	k1gInSc5
Petrioovy	Petrioův	k2eAgFnPc1d1
(	(	kIx(
<g/>
slovem	slovem	k6eAd1
dokument	dokument	k1gInSc4
doprovází	doprovázet	k5eAaImIp3nS
David	David	k1gMnSc1
Attenborough	Attenborough	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zachytily	zachytit	k5eAaPmAgInP
práci	práce	k1gFnSc4
sester	sestra	k1gFnPc2
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
na	na	k7c6
nejzbídačenějších	zbídačený	k2eAgNnPc6d3
místech	místo	k1gNnPc6
naší	náš	k3xOp1gFnSc2
planety	planeta	k1gFnSc2
<g/>
:	:	kIx,
Etiopii	Etiopie	k1gFnSc4
vysílenou	vysílený	k2eAgFnSc4d1
hladomorem	hladomor	k1gInSc7
<g/>
,	,	kIx,
Guatemalu	Guatemala	k1gFnSc4
po	po	k7c6
ničivém	ničivý	k2eAgNnSc6d1
zemětřesení	zemětřesení	k1gNnSc6
<g/>
,	,	kIx,
otřesné	otřesný	k2eAgInPc1d1
brlohy	brloh	k1gInPc1
na	na	k7c6
okrajích	okraj	k1gInPc6
zářivých	zářivý	k2eAgFnPc2d1
velkoměst	velkoměsto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
albánské	albánský	k2eAgFnSc2d1
Tirany	Tirana	k1gFnSc2
se	se	k3xPyFc4
po	po	k7c6
Matce	matka	k1gFnSc6
Tereze	Tereza	k1gFnSc6
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
(	(	kIx(
<g/>
ležící	ležící	k2eAgFnSc1d1
v	v	k7c6
nedaleké	daleký	k2eNgFnSc6d1
obci	obec	k1gFnSc6
Rinas	Rinas	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
byl	být	k5eAaImAgInS
natočen	natočit	k5eAaBmNgInS
film	film	k1gInSc1
s	s	k7c7
provokativním	provokativní	k2eAgInSc7d1
názvem	název	k1gInSc7
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
:	:	kIx,
Anděl	Anděla	k1gFnPc2
pekla	peklo	k1gNnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
byla	být	k5eAaImAgFnS
řeholnice	řeholnice	k1gFnSc1
obviněna	obvinit	k5eAaPmNgFnS
ze	z	k7c2
solidarity	solidarita	k1gFnSc2
s	s	k7c7
diktátorskými	diktátorský	k2eAgInPc7d1
režimy	režim	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umožnila	umožnit	k5eAaPmAgFnS
jim	on	k3xPp3gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
za	za	k7c4
příspěvky	příspěvek	k1gInPc4
na	na	k7c4
její	její	k3xOp3gNnSc4
útulky	útulek	k1gInPc1
hřály	hřát	k5eAaImAgInP
v	v	k7c6
její	její	k3xOp3gFnSc6
popularitě	popularita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
filmu	film	k1gInSc6
následovala	následovat	k5eAaImAgFnS
knižní	knižní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Christophera	Christophera	k1gFnSc1
Hitchense	Hitchense	k1gFnSc1
"	"	kIx"
<g/>
The	The	k1gFnSc1
Missionary	Missionara	k1gFnSc2
Position	Position	k1gInSc1
<g/>
:	:	kIx,
Mother	Mothra	k1gFnPc2
Teresa	Teresa	k1gFnSc1
In	In	k1gFnSc1
Theory	Theora	k1gFnSc2
And	Anda	k1gFnPc2
Practice	Practice	k1gFnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
autor	autor	k1gMnSc1
kritizoval	kritizovat	k5eAaImAgMnS
nedostatečnou	dostatečný	k2eNgFnSc4d1
lékařskou	lékařský	k2eAgFnSc4d1
péči	péče	k1gFnSc4
o	o	k7c4
nemocné	nemocný	k1gMnPc4
a	a	k8xC
umírající	umírající	k1gFnPc4
v	v	k7c6
domech	dům	k1gInPc6
kongregace	kongregace	k1gFnSc1
milosrdenství	milosrdenství	k1gNnPc1
<g/>
,	,	kIx,
údajné	údajný	k2eAgFnPc1d1
snahy	snaha	k1gFnPc1
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
získávat	získávat	k5eAaImF
své	svůj	k3xOyFgMnPc4
pacienty	pacient	k1gMnPc4
pro	pro	k7c4
křesťanskou	křesťanský	k2eAgFnSc4d1
víru	víra	k1gFnSc4
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc1
odmítání	odmítání	k1gNnSc3
potratů	potrat	k1gInPc2
a	a	k8xC
antikoncepce	antikoncepce	k1gFnSc2
a	a	k8xC
již	již	k6eAd1
zmíněné	zmíněný	k2eAgInPc4d1
styky	styk	k1gInPc4
s	s	k7c7
diktátorskými	diktátorský	k2eAgInPc7d1
režimy	režim	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Mnoha	mnoho	k4c3
lidem	člověk	k1gMnPc3
vadilo	vadit	k5eAaImAgNnS
její	její	k3xOp3gNnSc1
odmítání	odmítání	k1gNnSc1
potratů	potrat	k1gInPc2
a	a	k8xC
antikoncepce	antikoncepce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kritikové	kritik	k1gMnPc1
ji	on	k3xPp3gFnSc4
označovali	označovat	k5eAaImAgMnP
za	za	k7c4
fanatickou	fanatický	k2eAgFnSc4d1
antifeministku	antifeministka	k1gFnSc4
<g/>
,	,	kIx,
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
kvůli	kvůli	k7c3
výroku	výrok	k1gInSc3
z	z	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
nikdy	nikdy	k6eAd1
nesvěřila	svěřit	k5eNaPmAgFnS
dítě	dítě	k1gNnSc4
k	k	k7c3
adopci	adopce	k1gFnSc3
matce	matka	k1gFnSc3
užívající	užívající	k2eAgFnSc4d1
antikoncepci	antikoncepce	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
„	„	k?
<g/>
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
důkaz	důkaz	k1gInSc1
její	její	k3xOp3gFnSc2
neschopnosti	neschopnost	k1gFnSc2
milovat	milovat	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
knize	kniha	k1gFnSc6
poukázáno	poukázat	k5eAaPmNgNnS
na	na	k7c6
její	její	k3xOp3gNnSc4
údajné	údajný	k2eAgNnSc4d1
pokrytectví	pokrytectví	k1gNnSc4
<g/>
:	:	kIx,
zatímco	zatímco	k8xS
prý	prý	k9
svým	svůj	k3xOyFgMnPc3
pacientům	pacient	k1gMnPc3
odbornou	odborný	k2eAgFnSc4d1
lékařskou	lékařský	k2eAgFnSc4d1
péči	péče	k1gFnSc4
nedopřávala	dopřávat	k5eNaImAgFnS
<g/>
,	,	kIx,
jen	jen	k8xS
jim	on	k3xPp3gInPc3
ulehčovala	ulehčovat	k5eAaImAgFnS
umírání	umírání	k1gNnSc4
s	s	k7c7
poukazem	poukaz	k1gInSc7
na	na	k7c4
milost	milost	k1gFnSc4
boží	božit	k5eAaImIp3nS
(	(	kIx(
<g/>
kniha	kniha	k1gFnSc1
dokumentuje	dokumentovat	k5eAaBmIp3nS
případy	případ	k1gInPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
objektivně	objektivně	k6eAd1
léčitelní	léčitelný	k2eAgMnPc1d1
pacienti	pacient	k1gMnPc1
nebyli	být	k5eNaImAgMnP
odesílání	odesílání	k1gNnSc4
do	do	k7c2
nemocnic	nemocnice	k1gFnPc2
k	k	k7c3
léčbě	léčba	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
byli	být	k5eAaImAgMnP
ponecháni	ponechán	k2eAgMnPc1d1
zemřít	zemřít	k5eAaPmF
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sama	sám	k3xTgFnSc1
se	se	k3xPyFc4
léčila	léčit	k5eAaImAgFnS
na	na	k7c6
drahých	drahý	k2eAgFnPc6d1
klinikách	klinika	k1gFnPc6
západního	západní	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zřejmě	zřejmě	k6eAd1
největší	veliký	k2eAgFnSc4d3
kontroverzi	kontroverze	k1gFnSc4
okolo	okolo	k7c2
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
rozpoutala	rozpoutat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
skupina	skupina	k1gFnSc1
kanadských	kanadský	k2eAgMnPc2d1
lékařů	lékař	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
Misionářek	misionářka	k1gFnPc2
milosrdenství	milosrdenství	k1gNnPc2
odhalila	odhalit	k5eAaPmAgFnS
otřesné	otřesný	k2eAgFnPc4d1
hygienické	hygienický	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
nedostatek	nedostatek	k1gInSc4
jídla	jídlo	k1gNnSc2
a	a	k8xC
mizivé	mizivý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
utišujících	utišující	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
byl	být	k5eAaImAgInS
specifický	specifický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
na	na	k7c4
utrpení	utrpení	k1gNnSc4
a	a	k8xC
smrt	smrt	k1gFnSc4
<g/>
.	.	kIx.
„	„	k?
<g/>
Je	být	k5eAaImIp3nS
krásné	krásný	k2eAgNnSc1d1
vidět	vidět	k5eAaImF
chudé	chudý	k2eAgMnPc4d1
a	a	k8xC
nemocné	nemocný	k1gMnPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
trpně	trpně	k6eAd1
přijímají	přijímat	k5eAaImIp3nP
svůj	svůj	k3xOyFgInSc4
úděl	úděl	k1gInSc4
a	a	k8xC
trpí	trpět	k5eAaImIp3nS
jako	jako	k9
Kristus	Kristus	k1gMnSc1
o	o	k7c6
pašijích	pašije	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svět	svět	k1gInSc1
je	být	k5eAaImIp3nS
jejich	jejich	k3xOp3gNnSc7
utrpením	utrpení	k1gNnSc7
bohatší	bohatý	k2eAgFnSc2d2
<g/>
,	,	kIx,
<g/>
“	“	k?
řekla	říct	k5eAaPmAgFnS
matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
při	při	k7c6
rozhovoru	rozhovor	k1gInSc6
britskému	britský	k2eAgMnSc3d1
novináři	novinář	k1gMnSc3
Christopheru	Christopher	k1gMnSc3
Hitchensovi	Hitchens	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitchens	Hitchens	k1gInSc4
také	také	k9
upozornil	upozornit	k5eAaPmAgMnS
na	na	k7c6
praxi	praxe	k1gFnSc6
křtu	křest	k1gInSc2
bezmocných	bezmocný	k2eAgMnPc2d1
umírajících	umírající	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
řádové	řádový	k2eAgFnPc1d1
sestry	sestra	k1gFnPc1
na	na	k7c4
její	její	k3xOp3gInSc4
pokyn	pokyn	k1gInSc4
běžně	běžně	k6eAd1
praktikovaly	praktikovat	k5eAaImAgInP
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
podle	podle	k7c2
církevních	církevní	k2eAgInPc2d1
řádů	řád	k1gInPc2
je	být	k5eAaImIp3nS
ke	k	k7c3
křtu	křest	k1gInSc2
třeba	třeba	k6eAd1
informovaného	informovaný	k2eAgInSc2d1
souhlasu	souhlas	k1gInSc2
křtěnce	křtěnec	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
smrti	smrt	k1gFnSc6
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
vyšly	vyjít	k5eAaPmAgInP
knižně	knižně	k6eAd1
její	její	k3xOp3gInPc1
deníky	deník	k1gInPc1
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
vyplynulo	vyplynout	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
nedlouho	dlouho	k6eNd1
po	po	k7c6
povolání	povolání	k1gNnSc6
do	do	k7c2
služby	služba	k1gFnSc2
ztratila	ztratit	k5eAaPmAgFnS
jistotu	jistota	k1gFnSc4
víry	víra	k1gFnSc2
a	a	k8xC
Kristu	Krista	k1gFnSc4
sloužila	sloužit	k5eAaImAgFnS
dlouhá	dlouhý	k2eAgFnSc1d1
desetiletí	desetiletí	k1gNnPc2
navzdory	navzdory	k7c3
hlubokým	hluboký	k2eAgFnPc3d1
pochybnostem	pochybnost	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
a	a	k8xC
Československo	Československo	k1gNnSc1
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
navštívila	navštívit	k5eAaPmAgFnS
socialistické	socialistický	k2eAgNnSc4d1
Československo	Československo	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
na	na	k7c4
pozvání	pozvání	k1gNnSc4
kardinála	kardinál	k1gMnSc2
Františka	František	k1gMnSc2
Tomáška	Tomášek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
navštívila	navštívit	k5eAaPmAgFnS
Prahu	Praha	k1gFnSc4
a	a	k8xC
Brno	Brno	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
pozvání	pozvání	k1gNnSc4
prezidenta	prezident	k1gMnSc2
Václava	Václav	k1gMnSc2
Havla	Havel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
získala	získat	k5eAaPmAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
6	#num#	k4
v	v	k7c6
ulici	ulice	k1gFnSc6
Na	na	k7c6
Zátorce	Zátorka	k1gFnSc6
jednopatrovou	jednopatrový	k2eAgFnSc4d1
vilu	vila	k1gFnSc4
pro	pro	k7c4
zřízení	zřízení	k1gNnSc4
útulku	útulek	k1gInSc2
své	svůj	k3xOyFgFnSc2
kongregace	kongregace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgFnSc1d1
návštěva	návštěva	k1gFnSc1
tehdy	tehdy	k6eAd1
už	už	k6eAd1
federativního	federativní	k2eAgNnSc2d1
Československa	Československo	k1gNnSc2
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
její	její	k3xOp3gFnPc1
sestry	sestra	k1gFnPc1
založily	založit	k5eAaPmAgFnP
útulek	útulek	k1gInSc4
v	v	k7c6
Bratislavě-Rači	Bratislavě-Rač	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Závěr	závěr	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Při	při	k7c6
návštěvě	návštěva	k1gFnSc6
papeže	papež	k1gMnSc2
Jana	Jan	k1gMnSc2
Pavla	Pavel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
v	v	k7c6
Římě	Řím	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
dostala	dostat	k5eAaPmAgFnS
srdeční	srdeční	k2eAgInSc4d1
infarkt	infarkt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhém	druhý	k4xOgInSc6
infarktu	infarkt	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
jí	on	k3xPp3gFnSc2
byl	být	k5eAaImAgInS
voperován	voperován	k2eAgInSc1d1
kardiostimulátor	kardiostimulátor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
při	při	k7c6
návštěvě	návštěva	k1gFnSc6
Mexika	Mexiko	k1gNnSc2
dostala	dostat	k5eAaPmAgFnS
zápal	zápal	k1gInSc4
plic	plíce	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zhoršil	zhoršit	k5eAaPmAgInS
její	její	k3xOp3gFnPc4
srdeční	srdeční	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navrhla	navrhnout	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
rezignaci	rezignace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ovšem	ovšem	k9
nebyla	být	k5eNaImAgFnS
řádem	řád	k1gInSc7
přijata	přijmout	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
si	se	k3xPyFc3
zlomila	zlomit	k5eAaPmAgFnS
klíční	klíční	k2eAgFnSc1d1
kost	kost	k1gFnSc1
a	a	k8xC
záchvat	záchvat	k1gInSc1
malárie	malárie	k1gFnSc2
zhoršil	zhoršit	k5eAaPmAgInS
její	její	k3xOp3gFnPc4
srdeční	srdeční	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arcibiskup	arcibiskup	k1gMnSc1
z	z	k7c2
Kalkaty	Kalkata	k1gFnSc2
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
Sebastina	Sebastin	k1gMnSc2
D	D	kA
<g/>
'	'	kIx"
<g/>
Souza	Souz	k1gMnSc2
<g/>
,	,	kIx,
z	z	k7c2
ní	on	k3xPp3gFnSc2
nechal	nechat	k5eAaPmAgMnS
vyhnat	vyhnat	k5eAaPmF
ďábla	ďábel	k1gMnSc4
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
k	k	k7c3
tomu	ten	k3xDgNnSc3
navíc	navíc	k6eAd1
v	v	k7c6
prestižní	prestižní	k2eAgFnSc6d1
nemocnici	nemocnice	k1gFnSc6
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
podstoupila	podstoupit	k5eAaPmAgFnS
operaci	operace	k1gFnSc4
srdce	srdce	k1gNnSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1997	#num#	k4
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
,	,	kIx,
těžce	těžce	k6eAd1
nemocná	nemocný	k2eAgFnSc1d1,k2eNgFnSc1d1
a	a	k8xC
unavená	unavený	k2eAgFnSc1d1
stářím	stáří	k1gNnSc7
<g/>
,	,	kIx,
předala	předat	k5eAaPmAgFnS
vedení	vedení	k1gNnSc4
řádu	řád	k1gInSc6
své	svůj	k3xOyFgFnSc3
zástupkyni	zástupkyně	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
dvouměsíční	dvouměsíční	k2eAgFnSc6d1
poradě	porada	k1gFnSc6
nakonec	nakonec	k6eAd1
sestry	sestra	k1gFnPc1
generální	generální	k2eAgFnPc1d1
kapituly	kapitula	k1gFnPc1
řádu	řád	k1gInSc2
zvolily	zvolit	k5eAaPmAgFnP
za	za	k7c4
novou	nový	k2eAgFnSc4d1
představenou	představená	k1gFnSc4
řádu	řád	k1gInSc2
třiapadesátiletou	třiapadesátiletý	k2eAgFnSc4d1
sestru	sestra	k1gFnSc4
Nirmalu	Nirmal	k1gInSc2
<g/>
,	,	kIx,
konvertitku	konvertitka	k1gFnSc4
z	z	k7c2
hinduismu	hinduismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Blahořečení	blahořečení	k1gNnSc1
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Indii	Indie	k1gFnSc6
byl	být	k5eAaImAgInS
vyhlášen	vyhlásit	k5eAaPmNgInS
státní	státní	k2eAgInSc1d1
smutek	smutek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tělo	tělo	k1gNnSc1
zesnulé	zesnulá	k1gFnSc2
bylo	být	k5eAaImAgNnS
vystaveno	vystaven	k2eAgNnSc1d1
v	v	k7c6
kalkatském	kalkatský	k2eAgInSc6d1
kostele	kostel	k1gInSc6
sv.	sv.	kA
Tomáše	Tomáš	k1gMnSc2
–	–	k?
rozloučit	rozloučit	k5eAaPmF
se	se	k3xPyFc4
přišly	přijít	k5eAaPmAgInP
statisíce	statisíce	k1gInPc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
její	její	k3xOp3gFnSc2
smrti	smrt	k1gFnSc2
měl	mít	k5eAaImAgInS
její	její	k3xOp3gInSc1
řád	řád	k1gInSc1
přes	přes	k7c4
4	#num#	k4
000	#num#	k4
sester	sestra	k1gFnPc2
v	v	k7c6
610	#num#	k4
misiích	misie	k1gFnPc6
(	(	kIx(
<g/>
123	#num#	k4
zemí	zem	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c4
sobotu	sobota	k1gFnSc4
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
byla	být	k5eAaImAgFnS
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
pohřbena	pohřben	k2eAgFnSc1d1
–	–	k?
osm	osm	k4xCc1
indických	indický	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
vyneslo	vynést	k5eAaPmAgNnS
z	z	k7c2
chrámu	chrám	k1gInSc2
její	její	k3xOp3gNnSc4
tělo	tělo	k1gNnSc4
zabalené	zabalený	k2eAgNnSc4d1
do	do	k7c2
státní	státní	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
a	a	k8xC
položilo	položit	k5eAaPmAgNnS
na	na	k7c4
dělovou	dělový	k2eAgFnSc4d1
lafetu	lafeta	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
před	před	k7c7
padesáti	padesát	k4xCc7
lety	léto	k1gNnPc7
spočívalo	spočívat	k5eAaImAgNnS
tělo	tělo	k1gNnSc1
mrtvého	mrtvý	k1gMnSc2
Mahátmy	Mahátma	k1gFnSc2
Gándhího	Gándhí	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohřebního	pohřební	k2eAgInSc2d1
obřadu	obřad	k1gInSc2
se	se	k3xPyFc4
účastnilo	účastnit	k5eAaImAgNnS
na	na	k7c4
dvacet	dvacet	k4xCc4
tisíc	tisíc	k4xCgInPc2
hostů	host	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1999	#num#	k4
–	–	k?
V	v	k7c6
Kalkatě	Kalkata	k1gFnSc6
se	se	k3xPyFc4
na	na	k7c6
základě	základ	k1gInSc6
zvláštní	zvláštní	k2eAgFnSc2d1
dispenze	dispenz	k1gFnSc2
Jana	Jan	k1gMnSc2
Pavla	Pavel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
zahájil	zahájit	k5eAaPmAgInS
proces	proces	k1gInSc1
blahořečení	blahořečení	k1gNnSc2
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2001	#num#	k4
–	–	k?
Diecézní	diecézní	k2eAgInSc1d1
proces	proces	k1gInSc1
byl	být	k5eAaImAgInS
uzavřen	uzavřít	k5eAaPmNgInS
a	a	k8xC
materiály	materiál	k1gInPc4
odeslány	odeslán	k2eAgInPc4d1
do	do	k7c2
Říma	Řím	k1gInSc2
k	k	k7c3
přezkoumání	přezkoumání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2002	#num#	k4
–	–	k?
papež	papež	k1gMnSc1
Jan	Jan	k1gMnSc1
Pavel	Pavel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
vydal	vydat	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
dekrety	dekret	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
se	se	k3xPyFc4
oficiálně	oficiálně	k6eAd1
uznávají	uznávat	k5eAaImIp3nP
heroické	heroický	k2eAgFnPc1d1
ctnosti	ctnost	k1gFnPc1
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
a	a	k8xC
také	také	k9
zázrak	zázrak	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
základě	základ	k1gInSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c4
blahoslavenou	blahoslavený	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
nevysvětlitelně	vysvětlitelně	k6eNd1
náhlé	náhlý	k2eAgNnSc4d1
uzdravení	uzdravení	k1gNnSc4
mladé	mladý	k2eAgFnSc2d1
indické	indický	k2eAgFnSc2d1
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
vyznavačky	vyznavačka	k1gFnSc2
animistického	animistický	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2003	#num#	k4
v	v	k7c6
průběhu	průběh	k1gInSc6
Světového	světový	k2eAgInSc2d1
misijního	misijní	k2eAgInSc2d1
dne	den	k1gInSc2
byla	být	k5eAaImAgFnS
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
slavnostně	slavnostně	k6eAd1
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c4
blahoslavenou	blahoslavený	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Svatořečení	svatořečení	k1gNnSc1
</s>
<s>
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
uznal	uznat	k5eAaPmAgMnS
papež	papež	k1gMnSc1
František	František	k1gMnSc1
druhý	druhý	k4xOgInSc1
zázrak	zázrak	k1gInSc1
na	na	k7c4
její	její	k3xOp3gFnSc4
přímluvu	přímluva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
zázrak	zázrak	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
proběhl	proběhnout	k5eAaPmAgInS
před	před	k7c7
7	#num#	k4
lety	let	k1gInPc7
<g/>
,	,	kIx,
šlo	jít	k5eAaImAgNnS
o	o	k7c6
uzdravení	uzdravení	k1gNnSc6
pacienta	pacient	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
trpěl	trpět	k5eAaImAgMnS
zánětem	zánět	k1gInSc7
mozku	mozek	k1gInSc2
virového	virový	k2eAgInSc2d1
původu	původ	k1gInSc2
v	v	k7c6
brazilském	brazilský	k2eAgInSc6d1
Santosu	Santosa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
podepsal	podepsat	k5eAaPmAgMnS
papež	papež	k1gMnSc1
František	František	k1gMnSc1
dekret	dekret	k1gInSc4
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
svatořečení	svatořečení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
byla	být	k5eAaImAgFnS
ve	v	k7c6
Vatikánu	Vatikán	k1gInSc6
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c7
svatou	svatá	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Malé	Malé	k2eAgFnSc2d1
věci	věc	k1gFnSc2
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
láskou	láska	k1gFnSc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
byla	být	k5eAaImAgFnS
svatořečena	svatořečit	k5eAaBmNgFnS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
vira	vira	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://catholica.cz/?id=4363	http://catholica.cz/?id=4363	k4
<g/>
↑	↑	k?
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Řeč	řeč	k1gFnSc1
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
udělení	udělení	k1gNnSc2
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nobelprize	Nobelprize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
[	[	kIx(
<g/>
https://www.tirana-airport.com/	https://www.tirana-airport.com/	k?
Mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
Tirana	Tirana	k1gFnSc1
↑	↑	k?
http://www.youtube.com/watch?v=9WQ0i3nCx601	http://www.youtube.com/watch?v=9WQ0i3nCx601	k4
2	#num#	k4
3	#num#	k4
Pochyby	pochyba	k1gFnSc2
kolem	kolem	k7c2
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
trvají	trvat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://encyklopedie.brna.cz/home-mmb/?acc=profil_udalosti&	http://encyklopedie.brna.cz/home-mmb/?acc=profil_udalosti&	k?
<g/>
↑	↑	k?
Bindra	Bindra	k1gFnSc1
<g/>
,	,	kIx,
Satinder	Satinder	k1gInSc1
(	(	kIx(
<g/>
7	#num#	k4
September	September	k1gInSc1
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Archbishop	Archbishop	k1gInSc4
<g/>
:	:	kIx,
Mother	Mothra	k1gFnPc2
Teresa	Teresa	k1gFnSc1
underwent	underwent	k1gInSc1
exorcism	exorcism	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
Retrieved	Retrieved	k1gInSc4
30	#num#	k4
May	May	k1gMnSc1
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Irish	Irish	k1gMnSc1
Independent	independent	k1gMnSc1
http://www.independent.ie/unsorted/features/easter-the-church-and-the-same-party-line-42461.html	http://www.independent.ie/unsorted/features/easter-the-church-and-the-same-party-line-42461.html	k1gMnSc1
<g/>
↑	↑	k?
Otevřena	otevřen	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
ke	k	k7c3
kanonizaci	kanonizace	k1gFnSc3
bl.	bl.	k?
Matky	matka	k1gFnSc2
Terezy	Tereza	k1gFnSc2
z	z	k7c2
Kalkaty	Kalkata	k1gFnSc2
<g/>
↑	↑	k?
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
měla	mít	k5eAaImAgFnS
a	a	k8xC
má	mít	k5eAaImIp3nS
i	i	k8xC
odpůrce	odpůrce	k1gMnPc4
proti	proti	k7c3
svatořečení	svatořečení	k1gNnSc3
<g/>
,	,	kIx,
potvrzuje	potvrzovat	k5eAaImIp3nS
historik	historik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
-	-	kIx~
souhrnné	souhrnný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
<g/>
,	,	kIx,
texty	text	k1gInPc1
<g/>
,	,	kIx,
zamyšlení	zamyšlení	k1gNnPc1
<g/>
,	,	kIx,
citáty	citát	k1gInPc1
<g/>
,	,	kIx,
životopis	životopis	k1gInSc1
</s>
<s>
Stránky	stránka	k1gFnPc1
o	o	k7c6
Matce	matka	k1gFnSc6
Tereze	Tereza	k1gFnSc6
v	v	k7c6
češtině	čeština	k1gFnSc6
</s>
<s>
Citáty	citát	k1gInPc1
</s>
<s>
Citáty	citát	k1gInPc1
E-mamut	E-mamut	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nositelé	nositel	k1gMnPc1
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
mír	mír	k1gInSc4
</s>
<s>
Betty	Betty	k1gFnSc1
Williamsová	Williamsová	k1gFnSc1
/	/	kIx~
Mairead	Mairead	k1gInSc1
Maguireová	Maguireová	k1gFnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc2
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Anvar	Anvar	k1gMnSc1
as-Sádát	as-Sádát	k1gMnSc1
/	/	kIx~
Menachem	Menach	k1gMnSc7
Begin	Begin	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Adolfo	Adolfo	k1gMnSc1
Pérez	Pérez	k1gMnSc1
Esquivel	Esquivel	k1gMnSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Vysoký	vysoký	k2eAgMnSc1d1
komisař	komisař	k1gMnSc1
OSN	OSN	kA
pro	pro	k7c4
uprchlíky	uprchlík	k1gMnPc4
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Alva	Alv	k2eAgFnSc1d1
Myrdalová	Myrdalová	k1gFnSc1
/	/	kIx~
Alfonso	Alfonso	k1gMnSc1
García	García	k1gMnSc1
Robles	Robles	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lech	Lech	k1gMnSc1
Wałęsa	Wałęs	k1gMnSc2
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Desmond	Desmond	k1gInSc1
Tutu	tut	k1gInSc2
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lékaři	lékař	k1gMnPc1
proti	proti	k7c3
jaderné	jaderný	k2eAgFnSc3d1
válce	válka	k1gFnSc3
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Elie	Elie	k1gFnSc1
Wiesel	Wiesela	k1gFnPc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Óscar	Óscar	k1gMnSc1
Arias	Arias	k1gMnSc1
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Mírové	mírový	k2eAgFnPc1d1
síly	síla	k1gFnPc1
OSN	OSN	kA
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Tändzin	Tändzin	k1gMnSc1
Gjamccho	Gjamccha	k1gFnSc5
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
dalajláma	dalajláma	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Aun	Aun	k?
Schan	Schan	k1gMnSc1
Su	Su	k?
Ťij	Ťij	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Rigoberta	Rigoberta	k1gFnSc1
Menchú	Menchú	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Nelson	Nelson	k1gMnSc1
Mandela	Mandel	k1gMnSc2
/	/	kIx~
F.	F.	kA
<g/>
W.	W.	kA
de	de	k?
Klerk	Klerk	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Jásir	Jásir	k1gMnSc1
Arafat	Arafat	k1gMnSc1
/	/	kIx~
Šimon	Šimon	k1gMnSc1
Peres	Peres	k1gMnSc1
/	/	kIx~
Jicchak	Jicchak	k1gMnSc1
Rabin	Rabin	k1gMnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Pugwashské	pugwashský	k2eAgFnPc1d1
konference	konference	k1gFnPc1
/	/	kIx~
Józef	Józef	k1gInSc1
Rotblat	Rotble	k1gNnPc2
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Carlos	Carlos	k1gMnSc1
Belo	Bela	k1gFnSc5
/	/	kIx~
José	José	k1gNnSc1
Ramos-Horta	Ramos-Horta	k1gFnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
kampaň	kampaň	k1gFnSc1
za	za	k7c4
zákaz	zákaz	k1gInSc4
nášlapných	nášlapný	k2eAgFnPc2d1
min	mina	k1gFnPc2
/	/	kIx~
Jody	jod	k1gInPc1
Williams	Williamsa	k1gFnPc2
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
John	John	k1gMnSc1
Hume	Hum	k1gFnSc2
/	/	kIx~
David	David	k1gMnSc1
Trimble	Trimble	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Lékaři	lékař	k1gMnPc1
bez	bez	k7c2
hranic	hranice	k1gFnPc2
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
</s>
<s>
Kim	Kim	k?
Te-džung	Te-džung	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1901	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
•	•	k?
1926	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
•	•	k?
1951	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
</s>
<s>
Držitelé	držitel	k1gMnPc1
Templetonovy	Templetonův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
</s>
<s>
Matka	matka	k1gFnSc1
Tereza	Tereza	k1gFnSc1
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Roger	Roger	k1gMnSc1
Schütz	Schütz	k1gMnSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sarvepalli	Sarvepalle	k1gFnSc4
Rádhakrišnan	Rádhakrišnany	k1gInPc2
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Léon-Joseph	Léon-Joseph	k1gInSc1
Suenens	Suenens	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chiara	Chiara	k1gFnSc1
Lubichová	Lubichová	k1gFnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Thomas	Thomas	k1gMnSc1
Torrance	Torranec	k1gMnSc2
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nikkjó	Nikkjó	k1gFnSc1
Niwano	Niwana	k1gFnSc5
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ralph	Ralph	k1gInSc1
Wendell	Wendell	k1gMnSc1
Burhoe	Burhoe	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cicely	Cicel	k1gInPc1
Saundersová	Saundersová	k1gFnSc1
(	(	kIx(
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Billy	Bill	k1gMnPc4
Graham	Graham	k1gMnSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Solženicyn	Solženicyn	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Bourdeaux	Bourdeaux	k1gInSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alister	Alister	k1gInSc1
Hardy	Harda	k1gFnSc2
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1985	#num#	k4
<g/>
)	)	kIx)
•	•	k?
James	James	k1gMnSc1
McCord	McCord	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Stanley	Stanle	k2eAgFnPc4d1
Jaki	Jak	k1gFnPc4
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Inamullah	Inamullah	k1gMnSc1
Khan	Khan	k1gMnSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carl	Carl	k1gMnSc1
Friedrich	Friedrich	k1gMnSc1
von	von	k1gInSc4
Weizsäcker	Weizsäcker	k1gMnSc1
<g/>
,	,	kIx,
George	George	k1gFnSc1
MacLeod	MacLeoda	k1gFnPc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Baba	baba	k1gFnSc1
Amte	Amte	k?
<g/>
,	,	kIx,
L.	L.	kA
Charles	Charles	k1gMnSc1
Birch	Birch	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Immanuel	Immanuel	k1gInSc1
Jakobovits	Jakobovits	k1gInSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Kjóngdžik	Kjóngdžik	k1gInSc1
Han	Hana	k1gFnPc2
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charles	Charles	k1gMnSc1
Colson	Colson	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michael	Michael	k1gMnSc1
Novak	Novak	k1gMnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Paul	Paul	k1gMnSc1
Davies	Davies	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bill	Bill	k1gMnSc1
Bright	Bright	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Pándurang	Pándurang	k1gInSc1
Šástrí	Šástrí	k1gFnSc2
Áthavalé	Áthavalý	k2eAgFnSc2d1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sigmund	Sigmund	k1gMnSc1
Sternberg	Sternberg	k1gMnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ian	Ian	k1gMnSc1
Barbour	Barbour	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Freeman	Freeman	k1gMnSc1
Dyson	Dyson	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Arthur	Arthur	k1gMnSc1
Peacocke	Peacock	k1gMnSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Polkinghorne	Polkinghorn	k1gInSc5
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Holmes	Holmes	k1gInSc1
Rolston	Rolston	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
•	•	k?
George	George	k1gInSc1
Ellis	Ellis	k1gFnSc1
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charles	Charles	k1gMnSc1
Hard	Hard	k1gMnSc1
Townes	Townes	k1gMnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
D.	D.	kA
Barrow	Barrow	k1gMnSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Charles	Charles	k1gMnSc1
Taylor	Taylor	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Michał	Michał	k1gMnSc1
Heller	Heller	k1gMnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Bernard	Bernard	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Espagnat	Espagnat	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Francisco	Francisco	k1gMnSc1
J.	J.	kA
Ayala	Ayala	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gInSc1
Rees	Rees	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tändzin	Tändzin	k1gInSc1
Gjamccho	Gjamccha	k1gFnSc5
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Desmond	Desmond	k1gInSc1
Tutu	tut	k1gInSc2
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Halík	Halík	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jean	Jean	k1gMnSc1
Vanier	Vanier	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Sacks	Sacks	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Alvin	Alvin	k1gInSc1
Plantinga	Plantinga	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Abdalláh	Abdalláh	k1gInSc1
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Marcelo	Marcela	k1gFnSc5
Gleiser	Gleiser	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Francis	Francis	k1gInSc1
Collins	Collins	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19981001824	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118642707	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2144	#num#	k4
1401	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79144708	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
95161232	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79144708	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
