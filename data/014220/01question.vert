<s>
Pod	pod	k7c7
jakým	jaký	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
jménem	jméno	k1gNnSc7
byla	být	k5eAaImAgFnS
známá	známý	k2eAgFnSc1d1
humanitární	humanitární	k2eAgFnSc1d1
pracovnice	pracovnice	k1gFnSc1
a	a	k8xC
řeholnice	řeholnice	k1gFnSc1
albánského	albánský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
navštívila	navštívit	k5eAaPmAgFnS
Československo	Československo	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
?	?	kIx.
</s>