<s>
Loket	loket	k1gInSc1	loket
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Elbogen	Elbogen	k1gInSc1	Elbogen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Ohří	Ohře	k1gFnSc7	Ohře
v	v	k7c6	v
chráněné	chráněný	k2eAgFnSc6d1	chráněná
krajinné	krajinný	k2eAgFnSc6d1	krajinná
oblasti	oblast	k1gFnSc6	oblast
Slavkovský	slavkovský	k2eAgInSc4d1	slavkovský
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Goticko-románský	gotickoománský	k2eAgInSc1d1	goticko-románský
hrad	hrad	k1gInSc1	hrad
Loket	loket	k1gInSc1	loket
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
staré	starý	k2eAgNnSc4d1	staré
slovanské	slovanský	k2eAgNnSc4d1	slovanské
hradiště	hradiště	k1gNnSc4	hradiště
zvané	zvaný	k2eAgNnSc4d1	zvané
starý	starý	k2eAgInSc4d1	starý
Loket	loket	k1gInSc4	loket
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
postavení	postavení	k1gNnSc6	postavení
oporou	opora	k1gFnSc7	opora
králi	král	k1gMnSc6	král
Václavovi	Václav	k1gMnSc3	Václav
I.	I.	kA	I.
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gMnSc3	jeho
synu	syn	k1gMnSc3	syn
Přemyslovi	Přemysl	k1gMnSc3	Přemysl
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
však	však	k9	však
Přemysl	Přemysl	k1gMnSc1	Přemysl
stal	stát	k5eAaPmAgMnS	stát
sám	sám	k3xTgMnSc1	sám
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgMnS	zřídit
kolem	kolem	k7c2	kolem
Lokte	loket	k1gInSc2	loket
léna	léno	k1gNnSc2	léno
<g/>
,	,	kIx,	,
o	o	k7c4	o
která	který	k3yRgNnPc4	který
se	s	k7c7	s
staral	starat	k5eAaImAgMnS	starat
jeho	jeho	k3xOp3gMnSc4	jeho
zástupce	zástupce	k1gMnSc4	zástupce
loketský	loketský	k2eAgMnSc1d1	loketský
purkrabí	purkrabí	k1gMnSc1	purkrabí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
léna	léno	k1gNnPc1	léno
propůjčoval	propůjčovat	k5eAaImAgInS	propůjčovat
německým	německý	k2eAgMnPc3d1	německý
pánům	pan	k1gMnPc3	pan
a	a	k8xC	a
rytířům	rytíř	k1gMnPc3	rytíř
za	za	k7c4	za
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
privilegia	privilegium	k1gNnSc2	privilegium
vydaného	vydaný	k2eAgNnSc2d1	vydané
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1341	[number]	k4	1341
měli	mít	k5eAaImAgMnP	mít
leníci	leník	k1gMnPc1	leník
tyto	tento	k3xDgInPc1	tento
statky	statek	k1gInPc1	statek
v	v	k7c6	v
dědičném	dědičný	k2eAgNnSc6d1	dědičné
držení	držení	k1gNnSc6	držení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
práva	právo	k1gNnSc2	právo
odkazovat	odkazovat	k5eAaImF	odkazovat
je	on	k3xPp3gNnSc4	on
nečlenům	nečlen	k1gMnPc3	nečlen
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
byl	být	k5eAaImAgMnS	být
purkrabím	purkrabí	k1gMnPc3	purkrabí
Půta	Půta	k1gMnSc1	Půta
z	z	k7c2	z
Ilburka	Ilburek	k1gMnSc2	Ilburek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
řídil	řídit	k5eAaImAgInS	řídit
obranu	obrana	k1gFnSc4	obrana
celého	celý	k2eAgInSc2d1	celý
kraje	kraj	k1gInSc2	kraj
proti	proti	k7c3	proti
husitům	husita	k1gMnPc3	husita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1421,1427	[number]	k4	1421,1427
a	a	k8xC	a
1429	[number]	k4	1429
se	se	k3xPyFc4	se
husité	husita	k1gMnPc1	husita
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
hrad	hrad	k1gInSc4	hrad
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1434	[number]	k4	1434
zastavil	zastavit	k5eAaPmAgMnS	zastavit
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
hrad	hrad	k1gInSc4	hrad
Loket	loket	k1gInSc4	loket
s	s	k7c7	s
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
Andělskou	andělský	k2eAgFnSc7d1	Andělská
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
Ostrov	ostrov	k1gInSc1	ostrov
a	a	k8xC	a
panství	panství	k1gNnSc1	panství
hroznětínské	hroznětínský	k2eAgNnSc1d1	hroznětínský
<g/>
,	,	kIx,	,
falknovské	falknovský	k2eAgFnPc1d1	falknovský
a	a	k8xC	a
hartenberské	hartenberský	k2eAgFnPc1d1	hartenberský
svému	svůj	k3xOyFgMnSc3	svůj
kancléři	kancléř	k1gMnSc3	kancléř
Kašparu	Kašpar	k1gMnSc3	Kašpar
Šlikovi	Šliek	k1gMnSc3	Šliek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Kašparova	Kašparův	k2eAgMnSc2d1	Kašparův
bratra	bratr	k1gMnSc2	bratr
a	a	k8xC	a
dědice	dědic	k1gMnSc2	dědic
Matese	Mates	k1gMnSc2	Mates
roku	rok	k1gInSc2	rok
1487	[number]	k4	1487
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dvojímu	dvojí	k4xRgInSc3	dvojí
dělení	dělení	k1gNnSc3	dělení
šlikovských	šlikovský	k2eAgNnPc2d1	šlikovský
panství	panství	k1gNnPc2	panství
mezi	mezi	k7c4	mezi
Matesovy	Matesův	k2eAgMnPc4d1	Matesův
syny	syn	k1gMnPc4	syn
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
<g/>
,	,	kIx,	,
Kašpara	Kašpar	k1gMnSc4	Kašpar
a	a	k8xC	a
Jeronýma	Jeroným	k1gMnSc4	Jeroným
1487	[number]	k4	1487
a	a	k8xC	a
1489	[number]	k4	1489
<g/>
.	.	kIx.	.
</s>
<s>
Mocenský	mocenský	k2eAgInSc1d1	mocenský
vzestup	vzestup	k1gInSc1	vzestup
a	a	k8xC	a
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
evangelickými	evangelický	k2eAgInPc7d1	evangelický
stavy	stav	k1gInPc7	stav
přivedly	přivést	k5eAaPmAgFnP	přivést
Šliky	šlika	k1gFnPc4	šlika
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
šmalkaldské	šmalkaldský	k2eAgFnSc6d1	šmalkaldská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1546	[number]	k4	1546
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
prohráli	prohrát	k5eAaPmAgMnP	prohrát
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
panství	panství	k1gNnSc4	panství
(	(	kIx(	(
<g/>
předně	předně	k6eAd1	předně
Loket	loket	k1gInSc1	loket
a	a	k8xC	a
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
)	)	kIx)	)
jim	on	k3xPp3gMnPc3	on
král	král	k1gMnSc1	král
zkonfiskoval	zkonfiskovat	k5eAaPmAgInS	zkonfiskovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1551	[number]	k4	1551
propůjčil	propůjčit	k5eAaPmAgMnS	propůjčit
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
Habsburský	habsburský	k2eAgInSc4d1	habsburský
Loket	loket	k1gInSc4	loket
s	s	k7c7	s
purkrabstvím	purkrabství	k1gNnSc7	purkrabství
pánům	pan	k1gMnPc3	pan
z	z	k7c2	z
Plavna	Plavno	k1gNnSc2	Plavno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
špatné	špatný	k2eAgNnSc4d1	špatné
hospodaření	hospodaření	k1gNnSc4	hospodaření
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
r.	r.	kA	r.
1562	[number]	k4	1562
Loket	loket	k1gInSc1	loket
odebral	odebrat	k5eAaPmAgInS	odebrat
a	a	k8xC	a
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
ho	on	k3xPp3gMnSc4	on
loketským	loketský	k2eAgMnSc7d1	loketský
měšťanům	měšťan	k1gMnPc3	měšťan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
spiknutí	spiknutí	k1gNnSc4	spiknutí
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
na	na	k7c6	na
Lokti	loket	k1gInSc6	loket
vězněn	vězněn	k2eAgMnSc1d1	vězněn
Jiří	Jiří	k1gMnSc1	Jiří
Popel	popel	k1gInSc1	popel
z	z	k7c2	z
Lobkovic	Lobkovice	k1gInPc2	Lobkovice
který	který	k3yQgInSc4	který
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1613	[number]	k4	1613
zemřel	zemřít	k5eAaPmAgMnS	zemřít
(	(	kIx(	(
<g/>
druhé	druhý	k4xOgFnSc6	druhý
nejčastěji	často	k6eAd3	často
uváděné	uváděný	k2eAgNnSc4d1	uváděné
datum	datum	k1gNnSc4	datum
je	být	k5eAaImIp3nS	být
1607	[number]	k4	1607
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohromy	pohroma	k1gFnPc1	pohroma
postihly	postihnout	k5eAaPmAgFnP	postihnout
město	město	k1gNnSc4	město
i	i	k8xC	i
hrad	hrad	k1gInSc4	hrad
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Evangeličtí	evangelický	k2eAgMnPc1d1	evangelický
stavové	stavový	k2eAgNnSc4d1	stavové
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
mansfeldské	mansfeldský	k2eAgFnSc2d1	mansfeldský
posádky	posádka	k1gFnSc2	posádka
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
rytíře	rytíř	k1gMnSc2	rytíř
Globnara	Globnar	k1gMnSc2	Globnar
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
obléhání	obléhání	k1gNnSc6	obléhání
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
1621	[number]	k4	1621
<g/>
)	)	kIx)	)
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
valdštejnským	valdštejnský	k2eAgNnPc3d1	Valdštejnské
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1631	[number]	k4	1631
<g/>
–	–	k?	–
<g/>
1632	[number]	k4	1632
se	se	k3xPyFc4	se
evangeličtí	evangelický	k2eAgMnPc1d1	evangelický
emigranti	emigrant	k1gMnPc1	emigrant
s	s	k7c7	s
rytířem	rytíř	k1gMnSc7	rytíř
Globnarem	Globnar	k1gMnSc7	Globnar
načas	načas	k6eAd1	načas
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
museli	muset	k5eAaImAgMnP	muset
definitivně	definitivně	k6eAd1	definitivně
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Loket	loket	k1gInSc4	loket
Švédové	Švédová	k1gFnSc2	Švédová
a	a	k8xC	a
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
i	i	k9	i
hrad	hrad	k1gInSc1	hrad
Loket	loket	k1gInSc1	loket
začaly	začít	k5eAaPmAgInP	začít
po	po	k7c6	po
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
upadat	upadat	k5eAaPmF	upadat
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1714	[number]	k4	1714
byl	být	k5eAaImAgInS	být
Loketský	loketský	k2eAgInSc1d1	loketský
kraj	kraj	k1gInSc1	kraj
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Žatecku	Žatecko	k1gNnSc3	Žatecko
a	a	k8xC	a
obnoven	obnovit	k5eAaPmNgMnS	obnovit
byl	být	k5eAaImAgMnS	být
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1751	[number]	k4	1751
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
známá	známý	k2eAgFnSc1d1	známá
přestavba	přestavba	k1gFnSc1	přestavba
hradu	hrad	k1gInSc2	hrad
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
krále	král	k1gMnSc4	král
Václava	Václav	k1gMnSc2	Václav
I.	I.	kA	I.
Další	další	k1gNnPc4	další
stavební	stavební	k2eAgFnSc2d1	stavební
úpravy	úprava	k1gFnSc2	úprava
a	a	k8xC	a
přístavba	přístavba	k1gFnSc1	přístavba
byly	být	k5eAaImAgFnP	být
prováděny	provádět	k5eAaImNgFnP	provádět
za	za	k7c2	za
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
hrad	hrad	k1gInSc1	hrad
často	často	k6eAd1	často
využíval	využívat	k5eAaPmAgInS	využívat
jako	jako	k9	jako
dočasné	dočasný	k2eAgNnSc4d1	dočasné
sídlo	sídlo	k1gNnSc4	sídlo
královského	královský	k2eAgInSc2d1	královský
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
hradem	hrad	k1gInSc7	hrad
bylo	být	k5eAaImAgNnS	být
opevněno	opevněn	k2eAgNnSc1d1	opevněno
i	i	k8xC	i
město	město	k1gNnSc1	město
Loket	loket	k1gInSc1	loket
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
částem	část	k1gFnPc3	část
hradu	hrad	k1gInSc2	hrad
patří	patřit	k5eAaImIp3nP	patřit
markrabský	markrabský	k2eAgInSc4d1	markrabský
dům	dům	k1gInSc4	dům
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hradní	hradní	k2eAgFnSc1d1	hradní
věž	věž	k1gFnSc1	věž
(	(	kIx(	(
<g/>
konec	konec	k1gInSc1	konec
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavebních	stavební	k2eAgFnPc6d1	stavební
úpravách	úprava	k1gFnPc6	úprava
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
nalezeny	nalezen	k2eAgInPc1d1	nalezen
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
románské	románský	k2eAgFnSc2d1	románská
rotundy	rotunda	k1gFnSc2	rotunda
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejíchž	jejíž	k3xOyRp3gFnPc2	jejíž
zdí	zeď	k1gFnPc2	zeď
bylo	být	k5eAaImAgNnS	být
vestavěno	vestavět	k5eAaPmNgNnS	vestavět
později	pozdě	k6eAd2	pozdě
točité	točitý	k2eAgNnSc1d1	točité
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
velká	velký	k2eAgFnSc1d1	velká
přestavba	přestavba	k1gFnSc1	přestavba
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
hradu	hrad	k1gInSc2	hrad
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
šlikovské	šlikovský	k2eAgNnSc1d1	šlikovský
(	(	kIx(	(
<g/>
1434	[number]	k4	1434
<g/>
–	–	k?	–
<g/>
1547	[number]	k4	1547
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
přebudovat	přebudovat	k5eAaPmF	přebudovat
starý	starý	k2eAgInSc4d1	starý
gotický	gotický	k2eAgInSc4d1	gotický
hrad	hrad	k1gInSc4	hrad
na	na	k7c4	na
pohodlnější	pohodlný	k2eAgNnSc4d2	pohodlnější
rodové	rodový	k2eAgNnSc4d1	rodové
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
velké	velký	k2eAgInPc1d1	velký
sálové	sálový	k2eAgInPc1d1	sálový
prostory	prostor	k1gInPc1	prostor
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
křídle	křídlo	k1gNnSc6	křídlo
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
palác	palác	k1gInSc1	palác
s	s	k7c7	s
gotickým	gotický	k2eAgInSc7d1	gotický
arkýřem	arkýř	k1gInSc7	arkýř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
cenné	cenný	k2eAgFnPc4d1	cenná
gotické	gotický	k2eAgFnPc4d1	gotická
fresky	freska	k1gFnPc4	freska
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
kaple	kaple	k1gFnSc1	kaple
a	a	k8xC	a
hejtmanský	hejtmanský	k2eAgInSc1d1	hejtmanský
dům	dům	k1gInSc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvorské	dvorský	k2eAgFnSc6d1	dvorská
straně	strana	k1gFnSc6	strana
byla	být	k5eAaImAgFnS	být
studna	studna	k1gFnSc1	studna
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
rozváděla	rozvádět	k5eAaImAgFnS	rozvádět
štolou	štola	k1gFnSc7	štola
po	po	k7c6	po
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
bourání	bourání	k1gNnSc4	bourání
částí	část	k1gFnPc2	část
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
snadno	snadno	k6eAd1	snadno
opravit	opravit	k5eAaPmF	opravit
<g/>
,	,	kIx,	,
např.	např.	kA	např.
paláce	palác	k1gInPc1	palác
byly	být	k5eAaImAgInP	být
sníženy	snížit	k5eAaPmNgInP	snížit
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
patro	patro	k1gNnSc4	patro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1792	[number]	k4	1792
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
část	část	k1gFnSc1	část
hradu	hrad	k1gInSc2	hrad
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
na	na	k7c4	na
státní	státní	k2eAgNnSc4d1	státní
vězení	vězení	k1gNnSc4	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Lokti	loket	k1gInSc6	loket
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
pozůstatek	pozůstatek	k1gInSc1	pozůstatek
největšího	veliký	k2eAgInSc2d3	veliký
meteoritu	meteorit	k1gInSc2	meteorit
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
na	na	k7c6	na
území	území	k1gNnSc6	území
nynější	nynější	k2eAgFnSc2d1	nynější
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Purkrabí	purkrabí	k1gMnSc1	purkrabí
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dopadnout	dopadnout	k5eAaPmF	dopadnout
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
hradu	hrad	k1gInSc2	hrad
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
1422	[number]	k4	1422
a	a	k8xC	a
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
původní	původní	k2eAgFnSc2d1	původní
podoby	podoba	k1gFnSc2	podoba
se	se	k3xPyFc4	se
již	již	k6eAd1	již
značně	značně	k6eAd1	značně
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
–	–	k?	–
údajně	údajně	k6eAd1	údajně
vážil	vážit	k5eAaImAgInS	vážit
kolem	kolem	k7c2	kolem
107	[number]	k4	107
kg	kg	kA	kg
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xC	jako
koňská	koňský	k2eAgFnSc1d1	koňská
hlava	hlava	k1gFnSc1	hlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
byl	být	k5eAaImAgInS	být
rozřezán	rozřezán	k2eAgInSc1d1	rozřezán
<g/>
;	;	kIx,	;
největší	veliký	k2eAgInSc1d3	veliký
kus	kus	k1gInSc1	kus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
váží	vážit	k5eAaImIp3nS	vážit
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
kg	kg	kA	kg
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgMnS	uložit
Přírodovědeckém	přírodovědecký	k2eAgNnSc6d1	Přírodovědecké
muzeu	muzeum	k1gNnSc6	muzeum
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
