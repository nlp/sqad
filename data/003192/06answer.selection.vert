<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
především	především	k9	především
dva	dva	k4xCgInPc4	dva
tělesné	tělesný	k2eAgInPc4d1	tělesný
pohyby	pohyb	k1gInPc4	pohyb
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
tanec	tanec	k1gInSc4	tanec
<g/>
:	:	kIx,	:
metlení	metlení	k1gNnSc1	metlení
a	a	k8xC	a
šťouchání	šťouchání	k?	šťouchání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
jednak	jednak	k8xC	jednak
výrazem	výraz	k1gInSc7	výraz
porozumění	porozumění	k1gNnSc2	porozumění
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
rytmickým	rytmický	k2eAgNnSc7d1	rytmické
gestem	gesto	k1gNnSc7	gesto
<g/>
.	.	kIx.	.
</s>
