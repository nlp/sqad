<s>
Soluň	Soluň	k1gFnSc1
</s>
<s>
Soluň	Soluň	k1gFnSc1
Θ	Θ	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
40	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
22	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
0	#num#	k4
až	až	k6eAd1
20	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Řecko	Řecko	k1gNnSc1
Řecko	Řecko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Střední	střední	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
Prefektura	prefektura	k1gFnSc1
</s>
<s>
Thessaloníki	Thessaloníki	k6eAd1
</s>
<s>
Soluň	Soluň	k1gFnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
455,62	455,62	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
315	#num#	k4
196	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
216,5	216,5	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Světové	světový	k2eAgFnSc2d1
dědictví	dědictví	k1gNnSc6
UNESCO	Unesco	k1gNnSc1
Název	název	k1gInSc4
lokality	lokalita	k1gFnSc2
</s>
<s>
raněkřesťanské	raněkřesťanský	k2eAgFnPc1d1
a	a	k8xC
byzantské	byzantský	k2eAgFnPc1d1
památky	památka	k1gFnPc1
Soluně	Soluň	k1gFnSc2
Typ	typa	k1gFnPc2
</s>
<s>
kulturní	kulturní	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
Kritérium	kritérium	k1gNnSc1
</s>
<s>
i	i	k9
<g/>
,	,	kIx,
ii	ii	k?
<g/>
,	,	kIx,
iv	iv	k?
Odkaz	odkaz	k1gInSc1
</s>
<s>
456	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Zařazení	zařazení	k1gNnSc1
</s>
<s>
1988	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc2
<g/>
)	)	kIx)
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Konstantinos	Konstantinos	k1gMnSc1
Zervas	Zervas	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
2019	#num#	k4
<g/>
)	)	kIx)
Vznik	vznik	k1gInSc1
</s>
<s>
315	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.thessaloniki.gr	www.thessaloniki.gr	k1gInSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
2310	#num#	k4
PSČ	PSČ	kA
</s>
<s>
530	#num#	k4
<g/>
–	–	k?
<g/>
539	#num#	k4
<g/>
,	,	kIx,
54015	#num#	k4
<g/>
–	–	k?
<g/>
54655	#num#	k4
a	a	k8xC
56404	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
MM	mm	kA
<g/>
,	,	kIx,
NA	na	k7c6
<g/>
,	,	kIx,
NB	NB	kA
<g/>
,	,	kIx,
NE	Ne	kA
<g/>
,	,	kIx,
NH	NH	kA
<g/>
,	,	kIx,
NI	on	k3xPp3gFnSc4
<g/>
,	,	kIx,
NK	NK	kA
<g/>
,	,	kIx,
NM	NM	kA
<g/>
,	,	kIx,
NN	NN	kA
<g/>
,	,	kIx,
NO	no	k9
<g/>
,	,	kIx,
NP	NP	kA
<g/>
,	,	kIx,
NT	NT	kA
<g/>
,	,	kIx,
NY	NY	kA
<g/>
,	,	kIx,
NX	NX	kA
a	a	k8xC
NZ	NZ	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Soluň	Soluň	k1gFnSc1
(	(	kIx(
<g/>
Tesalonika	Tesalonika	k1gFnSc1
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
Θ	Θ	k?
<g/>
,	,	kIx,
přepisováno	přepisován	k2eAgNnSc4d1
jako	jako	k8xS,k8xC
Thessaloniki	Thessaloniki	k1gNnSc4
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
Saloniki	Salonik	k1gMnPc1
z	z	k7c2
tureckého	turecký	k2eAgMnSc2d1
Selânik	Selânik	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
neoficiální	neoficiální	k2eAgFnSc7d1,k2eNgFnSc7d1
metropolí	metropol	k1gFnSc7
řecké	řecký	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
<g/>
,	,	kIx,
správní	správní	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
oblasti	oblast	k1gFnSc2
Střední	střední	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
prefektury	prefektura	k1gFnSc2
Soluň	Soluň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
předměstími	předměstí	k1gNnPc7
zde	zde	k6eAd1
žije	žít	k5eAaImIp3nS
přes	přes	k7c4
jeden	jeden	k4xCgInSc4
milion	milion	k4xCgInSc1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
druhým	druhý	k4xOgNnSc7
největším	veliký	k2eAgNnSc7d3
řeckým	řecký	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
staveb	stavba	k1gFnPc2
ve	v	k7c6
městě	město	k1gNnSc6
z	z	k7c2
období	období	k1gNnSc2
raného	raný	k2eAgNnSc2d1
křesťanství	křesťanství	k1gNnSc2
a	a	k8xC
Byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
zapsáno	zapsat	k5eAaPmNgNnS
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
</s>
<s>
Soluň	Soluň	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
Balkánském	balkánský	k2eAgInSc6d1
poloostrově	poloostrov	k1gInSc6
v	v	k7c6
kraji	kraj	k1gInSc6
Makedonie	Makedonie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západě	západ	k1gInSc6
poloostrova	poloostrov	k1gInSc2
Chalkidiki	Chalkidik	k1gFnSc2
v	v	k7c6
Soluňském	soluňský	k2eAgInSc6d1
zálivu	záliv	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
deltě	delta	k1gFnSc6
řek	řeka	k1gFnPc2
Vardar	Vardara	k1gFnPc2
a	a	k8xC
Aliákmon	Aliákmona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
malé	malý	k2eAgFnSc6d1
nížině	nížina	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
obklopena	obklopen	k2eAgFnSc1d1
horami	hora	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
je	být	k5eAaImIp3nS
přirozeně	přirozeně	k6eAd1
chránily	chránit	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hluboký	hluboký	k2eAgInSc1d1
záliv	záliv	k1gInSc1
poskytoval	poskytovat	k5eAaImAgInS
výborné	výborný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
přístav	přístav	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
mělo	mít	k5eAaImAgNnS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
poloze	poloha	k1gFnSc3
velký	velký	k2eAgInSc1d1
strategický	strategický	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Období	období	k1gNnSc1
Řecka	Řecko	k1gNnSc2
a	a	k8xC
Říma	Řím	k1gInSc2
</s>
<s>
Antické	antický	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
</s>
<s>
Město	město	k1gNnSc1
založil	založit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
312	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Kassandros	Kassandros	k1gMnSc1
<g/>
,	,	kIx,
král	král	k1gMnSc1
Makedonský	makedonský	k2eAgMnSc1d1
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
dnes	dnes	k6eAd1
historického	historický	k2eAgNnSc2d1
města	město	k1gNnSc2
Therma	Thermum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojmenoval	pojmenovat	k5eAaPmAgMnS
jej	on	k3xPp3gNnSc4
po	po	k7c6
své	svůj	k3xOyFgFnSc6
manželce	manželka	k1gFnSc6
Thessalonice	Thessalonice	k1gFnSc2
<g/>
,	,	kIx,
sestře	sestra	k1gFnSc3
Alexandra	Alexandr	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
rychle	rychle	k6eAd1
rostlo	růst	k5eAaImAgNnS
<g/>
,	,	kIx,
v	v	k7c6
druhém	druhý	k4xOgNnSc6
století	století	k1gNnSc6
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
již	již	k6eAd1
byly	být	k5eAaImAgFnP
vybudovány	vybudován	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soluň	Soluň	k1gFnSc1
byla	být	k5eAaImAgFnS
centrem	centr	k1gMnSc7
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
autonomní	autonomní	k2eAgFnSc2d1
makedonské	makedonský	k2eAgFnSc2d1
provincie	provincie	k1gFnSc2
se	se	k3xPyFc4
svým	svůj	k3xOyFgInSc7
parlamentem	parlament	k1gInSc7
(	(	kIx(
<g/>
Ε	Ε	k?
τ	τ	k?
Δ	Δ	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
Makedonské	makedonský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
146	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
se	se	k3xPyFc4
město	město	k1gNnSc1
stalo	stát	k5eAaPmAgNnS
součástí	součást	k1gFnSc7
Římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
důležité	důležitý	k2eAgFnPc4d1
spojnici	spojnice	k1gFnSc4
mezi	mezi	k7c7
Římem	Řím	k1gInSc7
<g/>
,	,	kIx,
Dyrrhaciem	Dyrrhacium	k1gNnSc7
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc2d1
Dračí	dračí	k2eAgFnSc2d1
v	v	k7c6
Albánii	Albánie	k1gFnSc6
<g/>
)	)	kIx)
a	a	k8xC
Konstantinopolí	Konstantinopolí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k6eAd1
mu	on	k3xPp3gMnSc3
zůstaly	zůstat	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
výsady	výsada	k1gFnPc1
z	z	k7c2
dob	doba	k1gFnPc2
Makedonského	makedonský	k2eAgNnSc2d1
království	království	k1gNnSc2
–	–	k?
Soluň	Soluň	k1gFnSc4
totiž	totiž	k9
stále	stále	k6eAd1
byla	být	k5eAaImAgFnS
správním	správní	k2eAgNnSc7d1
městem	město	k1gNnSc7
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
římských	římský	k2eAgFnPc2d1
částí	část	k1gFnPc2
Makedonie	Makedonie	k1gFnSc2
<g/>
,	,	kIx,
pod	pod	k7c7
správou	správa	k1gFnSc7
prétora	prétor	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
sídlil	sídlit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
také	také	k9
přístav	přístav	k1gInSc1
(	(	kIx(
<g/>
Σ	Σ	k?
Λ	Λ	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc1
části	část	k1gFnPc1
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgFnP
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
55	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
vybudována	vybudován	k2eAgFnSc1d1
Akropolis	Akropolis	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
město	město	k1gNnSc4
chránit	chránit	k5eAaImF
před	před	k7c7
Thráckými	thrácký	k2eAgInPc7d1
nájezdy	nájezd	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
nedlouho	dlouho	k6eNd1
předtím	předtím	k6eAd1
město	město	k1gNnSc4
vážně	vážně	k6eAd1
poškodily	poškodit	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
časech	čas	k1gInPc6
po	po	k7c6
přelomu	přelom	k1gInSc6
letopočtu	letopočet	k1gInSc2
tu	tu	k6eAd1
existovala	existovat	k5eAaImAgFnS
početná	početný	k2eAgFnSc1d1
židovská	židovský	k2eAgFnSc1d1
komunita	komunita	k1gFnSc1
a	a	k8xC
začali	začít	k5eAaPmAgMnP
se	se	k3xPyFc4
tu	tu	k6eAd1
objevovat	objevovat	k5eAaImF
i	i	k9
první	první	k4xOgMnPc1
křesťané	křesťan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
misii	misie	k1gFnSc6
svatý	svatý	k2eAgMnSc1d1
Pavel	Pavel	k1gMnSc1
zde	zde	k6eAd1
založil	založit	k5eAaPmAgMnS
první	první	k4xOgInSc4
církevní	církevní	k2eAgInSc4d1
sbor	sbor	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
zanedlouho	zanedlouho	k6eAd1
musel	muset	k5eAaImAgMnS
město	město	k1gNnSc4
opustit	opustit	k5eAaPmF
<g/>
,	,	kIx,
právě	právě	k6eAd1
kvůli	kvůli	k7c3
rozepřím	rozepře	k1gFnPc3
s	s	k7c7
početnou	početný	k2eAgFnSc7d1
již	již	k6eAd1
zmíněnou	zmíněný	k2eAgFnSc7d1
židovskou	židovský	k2eAgFnSc7d1
komunitou	komunita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
roku	rok	k1gInSc2
306	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Soluň	Soluň	k1gFnSc1
svého	svůj	k3xOyFgMnSc2
patrona	patron	k1gMnSc2
<g/>
,	,	kIx,
svatého	svatý	k1gMnSc2
Demetria	Demetrium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Byzantské	byzantský	k2eAgNnSc1d1
a	a	k8xC
osmanské	osmanský	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Freska	freska	k1gFnSc1
z	z	k7c2
8	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
v	v	k7c6
kostele	kostel	k1gInSc6
Hagios	Hagios	k1gMnSc1
Demetrios	Demetrios	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příchod	příchod	k1gInSc1
císaře	císař	k1gMnSc2
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
Justiniána	Justinián	k1gMnSc4
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
do	do	k7c2
města	město	k1gNnSc2
</s>
<s>
Po	po	k7c6
rozdělení	rozdělení	k1gNnSc6
římské	římský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
na	na	k7c4
východní	východní	k2eAgInSc4d1
a	a	k8xC
západní	západní	k2eAgInSc4d1
význam	význam	k1gInSc4
Soluně	Soluň	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
největších	veliký	k2eAgNnPc2d3
východořímských	východořímský	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
,	,	kIx,
vzrostl	vzrůst	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
konce	konec	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byl	být	k5eAaImAgMnS
však	však	k9
Balkán	Balkán	k1gInSc4
vystaven	vystavit	k5eAaPmNgMnS
ničivým	ničivý	k2eAgInPc3d1
barbarským	barbarský	k2eAgInPc3d1
nájezdům	nájezd	k1gInPc3
(	(	kIx(
<g/>
Slované	Slovan	k1gMnPc1
<g/>
,	,	kIx,
Avaři	Avar	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
620	#num#	k4
postihlo	postihnout	k5eAaPmAgNnS
město	město	k1gNnSc1
zničující	zničující	k2eAgNnSc1d1
zemětřesení	zemětřesení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
zdevastovalo	zdevastovat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
budov	budova	k1gFnPc2
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
veřejných	veřejný	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
586	#num#	k4
oblehli	oblehnout	k5eAaPmAgMnP
Avaři	Avar	k1gMnPc1
zřejmě	zřejmě	k6eAd1
společně	společně	k6eAd1
se	s	k7c7
Slovany	Slovan	k1gMnPc7
Soluň	Soluň	k1gFnSc1
poprvé	poprvé	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
město	město	k1gNnSc1
bylo	být	k5eAaImAgNnS
chráněno	chránit	k5eAaImNgNnS
masívním	masívní	k2eAgNnSc7d1
opevněním	opevnění	k1gNnSc7
a	a	k8xC
náporu	nápor	k1gInSc2
útočníků	útočník	k1gMnPc2
odolalo	odolat	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obležení	obležení	k1gNnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
ještě	ještě	k6eAd1
několikrát	několikrát	k6eAd1
opakovalo	opakovat	k5eAaImAgNnS
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
675	#num#	k4
<g/>
–	–	k?
<g/>
681	#num#	k4
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
Slované	Slovan	k1gMnPc1
neúspěšně	úspěšně	k6eNd1
pokusili	pokusit	k5eAaPmAgMnP
dobýt	dobýt	k5eAaPmF
celkem	celkem	k6eAd1
čtyřikrát	čtyřikrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
již	již	k6eAd1
Soluň	Soluň	k1gFnSc1
druhým	druhý	k4xOgNnSc7
největším	veliký	k2eAgNnSc7d3
městem	město	k1gNnSc7
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
po	po	k7c6
ztrátě	ztráta	k1gFnSc6
Antiochie	Antiochie	k1gFnSc2
a	a	k8xC
Alexandrie	Alexandrie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
904	#num#	k4
vyplundrovali	vyplundrovat	k5eAaPmAgMnP
město	město	k1gNnSc4
Arabové	Arab	k1gMnPc1
<g/>
,	,	kIx,
odvezli	odvézt	k5eAaPmAgMnP
z	z	k7c2
něj	on	k3xPp3gMnSc2
bohatou	bohatý	k2eAgFnSc4d1
kořist	kořist	k1gFnSc4
a	a	k8xC
odvlekli	odvléct	k5eAaPmAgMnP
na	na	k7c4
třicet	třicet	k4xCc4
tisíc	tisíc	k4xCgInPc2
zajatců	zajatec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Byzantská	byzantský	k2eAgFnSc1d1
Soluň	Soluň	k1gFnSc1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
stala	stát	k5eAaPmAgFnS
prosperujícím	prosperující	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
,	,	kIx,
významnou	významný	k2eAgFnSc7d1
obchodní	obchodní	k2eAgFnSc7d1
křižovatkou	křižovatka	k1gFnSc7
i	i	k8xC
centrem	centrum	k1gNnSc7
tehdejší	tehdejší	k2eAgFnSc2d1
učenosti	učenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1185	#num#	k4
byla	být	k5eAaImAgFnS
dobyta	dobyt	k2eAgFnSc1d1
jihoitalskými	jihoitalský	k2eAgMnPc7d1
Normany	Norman	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
pokoušeli	pokoušet	k5eAaImAgMnP
zmocnit	zmocnit	k5eAaPmF
byzantských	byzantský	k2eAgFnPc2d1
držav	država	k1gFnPc2
na	na	k7c6
Balkáně	Balkán	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
latinského	latinský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1204	#num#	k4
se	se	k3xPyFc4
Soluně	Soluň	k1gFnSc2
a	a	k8xC
části	část	k1gFnSc2
Makedonie	Makedonie	k1gFnSc2
a	a	k8xC
Thessálie	Thessálie	k1gFnSc2
zmocnil	zmocnit	k5eAaPmAgMnS
protikandidát	protikandidát	k1gMnSc1
Balduina	Balduin	k2eAgInSc2d1
Flanderského	flanderský	k2eAgInSc2d1
na	na	k7c4
císařský	císařský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
Bonifác	Bonifác	k1gMnSc1
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
a	a	k8xC
založil	založit	k5eAaPmAgMnS
zde	zde	k6eAd1
soluňské	soluňský	k2eAgNnSc4d1
království	království	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
existovalo	existovat	k5eAaImAgNnS
pouze	pouze	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1224	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ho	on	k3xPp3gMnSc4
na	na	k7c4
synovi	syn	k1gMnSc3
Bonifáce	Bonifác	k1gMnSc2
z	z	k7c2
Montferratu	Montferrat	k1gInSc2
Demetriovi	Demetriův	k2eAgMnPc1d1
dobyl	dobýt	k5eAaPmAgInS
epeirský	epeirský	k2eAgMnSc1d1
despota	despota	k1gMnSc1
Theodoros	Theodorosa	k1gFnPc2
Angelos	Angelos	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
byzantských	byzantský	k2eAgMnPc2d1
exilových	exilový	k2eAgMnPc2d1
vládců	vládce	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
se	se	k3xPyFc4
korunovat	korunovat	k5eAaBmF
císařem	císař	k1gMnSc7
Římanů	Říman	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
Soluň	Soluň	k1gFnSc1
oddělila	oddělit	k5eAaPmAgFnS
od	od	k7c2
epeirského	epeirský	k2eAgInSc2d1
despotátu	despotát	k1gInSc2
a	a	k8xC
roku	rok	k1gInSc2
1246	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
nikájského	nikájský	k2eAgNnSc2d1
císařství	císařství	k1gNnSc2
a	a	k8xC
po	po	k7c6
roce	rok	k1gInSc6
1261	#num#	k4
obnovené	obnovený	k2eAgFnSc2d1
byzantské	byzantský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
vypuklo	vypuknout	k5eAaPmAgNnS
v	v	k7c6
letech	let	k1gInPc6
1342	#num#	k4
<g/>
–	–	k?
<g/>
1349	#num#	k4
tzv.	tzv.	kA
povstání	povstání	k1gNnSc2
zélótů	zélóta	k1gMnPc2
<g/>
,	,	kIx,
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
sociální	sociální	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
,	,	kIx,
vyvolané	vyvolaný	k2eAgInPc1d1
složitými	složitý	k2eAgInPc7d1
poměry	poměr	k1gInPc7
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
Byzanci	Byzanc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
byzantské	byzantský	k2eAgNnSc1d1
území	území	k1gNnSc1
silně	silně	k6eAd1
okleštěno	okleštit	k5eAaPmNgNnS
osmanskými	osmanský	k2eAgInPc7d1
Turky	turek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soluň	Soluň	k1gFnSc1
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mála	málo	k4c2
držav	država	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
císaři	císař	k1gMnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k9
roku	rok	k1gInSc2
1387	#num#	k4
a	a	k8xC
znovu	znovu	k6eAd1
1384	#num#	k4
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
dočasně	dočasně	k6eAd1
zmocnili	zmocnit	k5eAaPmAgMnP
Osmani	Osman	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1430	#num#	k4
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
definitivně	definitivně	k6eAd1
dobyto	dobýt	k5eAaPmNgNnS
osmanským	osmanský	k2eAgInSc7d1
sultánem	sultán	k1gMnSc7
Muradem	Murad	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
přejmenováno	přejmenovat	k5eAaPmNgNnS
na	na	k7c4
Selânik	Selânik	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
vydrželo	vydržet	k5eAaPmAgNnS
v	v	k7c6
tureckých	turecký	k2eAgFnPc6d1
rukou	ruka	k1gFnPc6
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
–	–	k?
téměř	téměř	k6eAd1
500	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
byl	být	k5eAaImAgInS
vystavěn	vystavěn	k2eAgInSc1d1
moderní	moderní	k2eAgInSc1d1
přístav	přístav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zde	zde	k6eAd1
také	také	k9
centrum	centrum	k1gNnSc1
hnutí	hnutí	k1gNnSc2
Mladoturků	mladoturek	k1gMnPc2
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1881	#num#	k4
narodil	narodit	k5eAaPmAgMnS
pozdější	pozdní	k2eAgMnSc1d2
prezident	prezident	k1gMnSc1
Turecké	turecký	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
Mustafa	Mustaf	k1gMnSc4
Kemal	Kemal	k1gMnSc1
Atatürk	Atatürk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
byla	být	k5eAaImAgFnS
Soluň	Soluň	k1gFnSc1
značně	značně	k6eAd1
multikulturním	multikulturní	k2eAgNnSc7d1
městem	město	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jak	jak	k8xC,k8xS
uvádí	uvádět	k5eAaImIp3nS
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
,	,	kIx,
žilo	žít	k5eAaImAgNnS
zde	zde	k6eAd1
v	v	k7c6
r.	r.	kA
1900	#num#	k4
téměř	téměř	k6eAd1
118	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
na	na	k7c4
55	#num#	k4
000	#num#	k4
Židů	Žid	k1gMnPc2
<g/>
,	,	kIx,
26	#num#	k4
000	#num#	k4
Turků	turek	k1gInPc2
<g/>
,	,	kIx,
16	#num#	k4
000	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Řeků	Řek	k1gMnPc2
<g/>
,	,	kIx,
10	#num#	k4
000	#num#	k4
Bulharů	Bulhar	k1gMnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
2	#num#	k4
500	#num#	k4
Romů	Rom	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
Albánci	Albánec	k1gMnPc1
<g/>
,	,	kIx,
Srbové	Srb	k1gMnPc1
<g/>
,	,	kIx,
Francouzi	Francouz	k1gMnPc1
<g/>
,	,	kIx,
Italové	Ital	k1gMnPc1
aj.	aj.	kA
Podíl	podíl	k1gInSc1
židovské	židovský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
v	v	k7c6
Soluni	Soluň	k1gFnSc6
byl	být	k5eAaImAgInS
vysoký	vysoký	k2eAgInSc1d1
po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1842	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
51	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
pol.	pol.	k?
60	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
56	#num#	k4
%	%	kIx~
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
49	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
s	s	k7c7
nástupem	nástup	k1gInSc7
druhé	druhý	k4xOgFnSc2
balkánské	balkánský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
respektive	respektive	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
klesl	klesnout	k5eAaPmAgInS
podíl	podíl	k1gInSc1
Židů	Žid	k1gMnPc2
pod	pod	k7c4
40	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
ovšem	ovšem	k9
pouze	pouze	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
nárůstu	nárůst	k1gInSc2
nežidovského	židovský	k2eNgInSc2d1
(	(	kIx(
<g/>
především	především	k6eAd1
řeckého	řecký	k2eAgNnSc2d1
<g/>
)	)	kIx)
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnou	skutečný	k2eAgFnSc7d1
ranou	rána	k1gFnSc7
byla	být	k5eAaImAgFnS
až	až	k9
druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
po	po	k7c4
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
se	se	k3xPyFc4
podíl	podíl	k1gInSc1
židovského	židovský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
radikálně	radikálně	k6eAd1
snížil	snížit	k5eAaPmAgInS
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
už	už	k6eAd1
jen	jen	k9
1000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
hlásících	hlásící	k2eAgMnPc2d1
se	se	k3xPyFc4
k	k	k7c3
židovství	židovství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Přístaviště	přístaviště	k1gNnSc1
v	v	k7c6
Soluni	Soluň	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
</s>
<s>
V	v	k7c6
první	první	k4xOgFnSc6
balkánské	balkánský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
roku	rok	k1gInSc2
1912	#num#	k4
byla	být	k5eAaImAgFnS
Soluň	Soluň	k1gFnSc1
obsazena	obsadit	k5eAaPmNgFnS
řeckými	řecký	k2eAgNnPc7d1
vojsky	vojsko	k1gNnPc7
<g/>
,	,	kIx,
po	po	k7c6
vypuknutí	vypuknutí	k1gNnSc6
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zde	zde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
k	k	k7c3
vylodění	vylodění	k1gNnSc3
jednotek	jednotka	k1gFnPc2
Dohody	dohoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
podepsáním	podepsání	k1gNnSc7
míru	mír	k1gInSc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1917	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
celá	celá	k1gFnSc1
zničena	zničit	k5eAaPmNgFnS
požárem	požár	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc4
příčina	příčina	k1gFnSc1
není	být	k5eNaImIp3nS
dodnes	dodnes	k6eAd1
objasněna	objasnit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Ernest	Ernest	k1gMnSc1
Hebrard	Hebrard	k1gMnSc1
Soluň	Soluň	k1gFnSc4
přestavěl	přestavět	k5eAaPmAgMnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
jejího	její	k3xOp3gMnSc2
orientálního	orientální	k2eAgMnSc2d1
<g/>
,	,	kIx,
tureckého	turecký	k2eAgInSc2d1
rázu	ráz	k1gInSc2
a	a	k8xC
jejímu	její	k3xOp3gInSc3
architektonickému	architektonický	k2eAgInSc3d1
návratu	návrat	k1gInSc3
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
kromě	kromě	k7c2
této	tento	k3xDgFnSc2
obnovy	obnova	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
ohromnému	ohromný	k2eAgNnSc3d1
stěhování	stěhování	k1gNnSc3
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
také	také	k9
říkalo	říkat	k5eAaImAgNnS
„	„	k?
<g/>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
uprchlíků	uprchlík	k1gMnPc2
<g/>
“	“	k?
<g/>
;	;	kIx,
byli	být	k5eAaImAgMnP
sem	sem	k6eAd1
přesídleni	přesídlen	k2eAgMnPc1d1
Řekové	Řek	k1gMnPc1
ze	z	k7c2
západní	západní	k2eAgFnSc2d1
Anatolie	Anatolie	k1gFnSc2
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Židé	Žid	k1gMnPc1
naopak	naopak	k6eAd1
odešli	odejít	k5eAaPmAgMnP
do	do	k7c2
Palestiny	Palestina	k1gFnSc2
nebo	nebo	k8xC
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
vzniku	vznik	k1gInSc6
Jugoslávie	Jugoslávie	k1gFnSc2
tu	tu	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
Jugoslávské	jugoslávský	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
svobodného	svobodný	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
výsledek	výsledek	k1gInSc1
jejích	její	k3xOp3gFnPc2
snah	snaha	k1gFnPc2
o	o	k7c6
anexi	anexe	k1gFnSc6
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byla	být	k5eAaImAgFnS
Soluň	Soluň	k1gFnSc1
v	v	k7c6
letech	let	k1gInPc6
1941	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
obsazena	obsadit	k5eAaPmNgNnP
německými	německý	k2eAgFnPc7d1
vojsky	vojsky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okupace	okupace	k1gFnSc1
zničila	zničit	k5eAaPmAgFnS
poslední	poslední	k2eAgInPc4d1
zbytky	zbytek	k1gInPc4
židovské	židovský	k2eAgFnSc2d1
komunity	komunita	k1gFnSc2
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
jen	jen	k9
necelých	celý	k2eNgInPc2d1
1000	#num#	k4
židů	žid	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
bylo	být	k5eAaImAgNnS
město	město	k1gNnSc1
rychle	rychle	k6eAd1
obnoveno	obnovit	k5eAaPmNgNnS
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
je	být	k5eAaImIp3nS
ale	ale	k9
opět	opět	k6eAd1
poškodilo	poškodit	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1997	#num#	k4
byla	být	k5eAaImAgFnS
Soluň	Soluň	k1gFnSc1
vyhlášena	vyhlásit	k5eAaPmNgFnS
Evropským	evropský	k2eAgNnSc7d1
městem	město	k1gNnSc7
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
V	v	k7c6
Soluni	Soluň	k1gFnSc6
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
ocel	ocel	k1gFnSc1
<g/>
,	,	kIx,
chemické	chemický	k2eAgInPc1d1
výrobky	výrobek	k1gInPc1
a	a	k8xC
stavební	stavební	k2eAgInPc1d1
materiály	materiál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Díky	díky	k7c3
přístavu	přístav	k1gInSc3
a	a	k8xC
svojí	svůj	k3xOyFgFnSc3
poloze	poloha	k1gFnSc3
je	být	k5eAaImIp3nS
její	její	k3xOp3gInSc1
význam	význam	k1gInSc1
veliký	veliký	k2eAgInSc1d1
pro	pro	k7c4
celou	celý	k2eAgFnSc4d1
jihovýchodní	jihovýchodní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
hlavně	hlavně	k9
pro	pro	k7c4
země	zem	k1gFnPc4
bývalé	bývalý	k2eAgFnSc2d1
Jugoslávie	Jugoslávie	k1gFnSc2
a	a	k8xC
Bulharsko	Bulharsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
byla	být	k5eAaImAgFnS
zprovozněna	zprovoznit	k5eAaPmNgFnS
první	první	k4xOgFnSc1
dálnice	dálnice	k1gFnSc1
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
velká	velký	k2eAgFnSc1d1
dálniční	dálniční	k2eAgFnSc1d1
křižovatka	křižovatka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Soluni	Soluň	k1gFnSc6
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgNnSc4d1
letiště	letiště	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yQgNnSc4,k3yIgNnSc4
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
obsluhuje	obsluhovat	k5eAaImIp3nS
i	i	k9
pravidelnou	pravidelný	k2eAgFnSc4d1
linku	linka	k1gFnSc4
z	z	k7c2
<g/>
/	/	kIx~
<g/>
do	do	k7c2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začalo	začít	k5eAaPmAgNnS
se	se	k3xPyFc4
stavět	stavět	k5eAaImF
i	i	k9
metro	metro	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
městské	městský	k2eAgFnSc3d1
dopravě	doprava	k1gFnSc3
velmi	velmi	k6eAd1
pomůže	pomoct	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Ve	v	k7c6
městě	město	k1gNnSc6
sídlí	sídlet	k5eAaImIp3nS
multisportovní	multisportovní	k2eAgNnSc1d1
(	(	kIx(
<g/>
především	především	k9
fotbalové	fotbalový	k2eAgInPc4d1
a	a	k8xC
basketbalové	basketbalový	k2eAgInPc4d1
<g/>
)	)	kIx)
kluby	klub	k1gInPc4
PAOK	PAOK	kA
<g/>
,	,	kIx,
Aris	Arisa	k1gFnPc2
a	a	k8xC
Iraklis	Iraklis	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Alexandrie	Alexandrie	k1gFnSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
od	od	k7c2
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1993	#num#	k4
</s>
<s>
Boloňa	Boloňa	k1gFnSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1984	#num#	k4
</s>
<s>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
,	,	kIx,
23	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1986	#num#	k4
</s>
<s>
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1988	#num#	k4
</s>
<s>
Constanţa	Constanţa	k1gFnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1988	#num#	k4
</s>
<s>
Hartford	Hartford	k1gMnSc1
<g/>
,	,	kIx,
Connecticut	Connecticut	k1gMnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1962	#num#	k4
</s>
<s>
Kalkata	Kalkata	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2005	#num#	k4
</s>
<s>
Korçë	Korçë	k?
<g/>
,	,	kIx,
Albánie	Albánie	k1gFnSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2005	#num#	k4
</s>
<s>
Lipsko	Lipsko	k1gNnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1984	#num#	k4
</s>
<s>
Limassol	Limassol	k1gInSc1
<g/>
,	,	kIx,
Kypr	Kypr	k1gInSc1
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1984	#num#	k4
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1984	#num#	k4
</s>
<s>
Nice	Nice	k1gFnSc1
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
<g/>
,	,	kIx,
20	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1992	#num#	k4
</s>
<s>
Plovdiv	Plovdiv	k1gInSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
1984	#num#	k4
</s>
<s>
San	San	k?
Francisco	Francisco	k1gNnSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1990	#num#	k4
</s>
<s>
Tel	tel	kA
Aviv	Aviv	k1gInSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1994	#num#	k4
</s>
<s>
Tchien-ťin	Tchien-ťin	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2002	#num#	k4
</s>
<s>
Tung-kuan	Tung-kuan	k1gInSc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
</s>
<s>
Bergen	Bergen	k1gInSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2008	#num#	k4
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Rotunda	rotunda	k1gFnSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
</s>
<s>
Rotunda	rotunda	k1gFnSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
<g/>
,	,	kIx,
interiér	interiér	k1gInSc1
</s>
<s>
Chrám	chrám	k1gInSc1
Acheiropoiétos	Acheiropoiétosa	k1gFnPc2
</s>
<s>
Chrám	chrám	k1gInSc1
Acheiropoiétos	Acheiropoiétos	k1gInSc1
<g/>
,	,	kIx,
interiér	interiér	k1gInSc1
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1
věž	věž	k1gFnSc1
</s>
<s>
Lázně	lázeň	k1gFnPc1
Bey	Bey	k1gFnSc2
Hamam	Hamam	k1gInSc4
</s>
<s>
Galeriův	Galeriův	k2eAgInSc1d1
triumfální	triumfální	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
</s>
<s>
Reliéfy	reliéf	k1gInPc1
</s>
<s>
Chrám	chrám	k1gInSc4
Dvanácti	dvanáct	k4xCc2
apoštolů	apoštol	k1gMnPc2
</s>
<s>
Chrám	chrám	k1gInSc1
Panagia	Panagium	k1gNnSc2
Chalkeon	Chalkeona	k1gFnPc2
</s>
<s>
Chrám	chrám	k1gInSc1
Panagia	Panagium	k1gNnSc2
Chalkeon	Chalkeona	k1gFnPc2
<g/>
,	,	kIx,
freska	freska	k1gFnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
)	)	kIx)
</s>
<s>
Středověké	středověký	k2eAgFnPc1d1
hradby	hradba	k1gFnPc1
</s>
<s>
Královské	královský	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
KARČIĆ	KARČIĆ	kA
<g/>
,	,	kIx,
Hamza	Hamza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnPc2
a	a	k8xC
Balkan	Balkana	k1gFnPc2
city	city	k1gFnSc2
is	is	k?
important	important	k1gMnSc1
for	forum	k1gNnPc2
understanding	understanding	k1gInSc1
Turkish	Turkisha	k1gFnPc2
history	histor	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Daily	Daila	k1gFnSc2
Sabah	Sabaha	k1gFnPc2
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.solunbg.org/en/	https://www.solunbg.org/en/	k?
<g/>
↑	↑	k?
ŠEBA	ŠEBA	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Malá	malý	k2eAgFnSc1d1
dohoda	dohoda	k1gFnSc1
v	v	k7c6
politice	politika	k1gFnSc6
světové	světový	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Melantrich	Melantrich	k1gMnSc1
<g/>
,	,	kIx,
1936	#num#	k4
<g/>
.	.	kIx.
652	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
s.	s.	k?
517	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
nové	nový	k2eAgNnSc4d1
doby	doba	k1gFnPc4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc4
Soluň	Soluň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
11	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
111	#num#	k4
</s>
<s>
Thessaloniki	Thessaloniki	k6eAd1
and	and	k?
the	the	k?
Bulgarians	Bulgarians	k1gInSc1
<g/>
.	.	kIx.
<g/>
History	Histor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Memory	Memor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Present	Present	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Soluň	Soluň	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
solun	solun	k1gNnSc1
Photo	Phot	k2eAgNnSc1d1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Řecko	Řecko	k1gNnSc1
–	–	k?
Ε	Ε	k?
–	–	k?
(	(	kIx(
<g/>
GR	GR	kA
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
Π	Π	k?
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Attika	Attika	k1gFnSc1
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Athény	Athéna	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Epirus	Epirus	k1gInSc1
(	(	kIx(
<g/>
Ioánnina	Ioánnina	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jónské	jónský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
Korfu	Korfu	k1gNnSc1
–	–	k?
Kerkyra	Kerkyr	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Jižní	jižní	k2eAgFnSc3d1
Egeis	Egeis	k1gFnSc3
(	(	kIx(
<g/>
Ermupoli	Ermupole	k1gFnSc3
<g/>
)	)	kIx)
•	•	k?
Kréta	Kréta	k1gFnSc1
(	(	kIx(
<g/>
Heráklion	Heráklion	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Peloponés	Peloponés	k1gInSc4
(	(	kIx(
<g/>
Tripoli	Tripole	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Severní	severní	k2eAgFnSc1d1
Egeis	Egeis	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Mytiléna	Mytiléna	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Střední	střední	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
(	(	kIx(
<g/>
Soluň	Soluň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Střední	střední	k2eAgNnSc1d1
Řecko	Řecko	k1gNnSc1
(	(	kIx(
<g/>
Lamia	Lamia	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Thesálie	Thesálie	k1gFnSc1
(	(	kIx(
<g/>
Larisa	Larisa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Východní	východní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
a	a	k8xC
Thrákie	Thrákie	k1gFnSc1
(	(	kIx(
<g/>
Komotiní	Komotiní	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Západní	západní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
(	(	kIx(
<g/>
Kozani	Kozan	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Západní	západní	k2eAgNnSc4d1
Řecko	Řecko	k1gNnSc4
(	(	kIx(
<g/>
Patra	patro	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1
status	status	k1gInSc1
<g/>
:	:	kIx,
Athos	Athos	k1gInSc1
(	(	kIx(
<g/>
Karyés	Karyés	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Řecké	řecký	k2eAgFnPc1d1
památky	památka	k1gFnPc1
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Akropolis	Akropolis	k1gFnSc1
v	v	k7c6
Athénách	Athéna	k1gFnPc6
•	•	k?
hora	hora	k1gFnSc1
Athos	Athos	k1gInSc1
•	•	k?
Apollónův	Apollónův	k2eAgInSc1d1
chrám	chrám	k1gInSc1
v	v	k7c4
Bassai	Bassa	k1gFnPc4
•	•	k?
Delfy	Delfy	k1gFnPc4
•	•	k?
Délos	Délos	k1gInSc1
•	•	k?
Epidauros	Epidaurosa	k1gFnPc2
•	•	k?
kláštery	klášter	k1gInPc1
Dafnion	Dafnion	k1gInSc1
<g/>
,	,	kIx,
Hossios	Hossios	k1gMnSc1
Loukas	Loukas	k1gMnSc1
a	a	k8xC
Nea	Nea	k1gMnSc1
Moni	Moni	k?
na	na	k7c6
Chiu	Chius	k1gInSc6
•	•	k?
Korfu	Korfu	k1gNnSc2
<g/>
,	,	kIx,
historické	historický	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
•	•	k?
Meteora	Meteor	k1gMnSc2
•	•	k?
Mykény	Mykény	k1gFnPc4
a	a	k8xC
Tíryns	Tíryns	k1gInSc4
•	•	k?
Mystras	Mystras	k1gInSc1
•	•	k?
Olympie	Olympia	k1gFnSc2
•	•	k?
Patmos	Patmos	k1gInSc1
•	•	k?
Rhodos	Rhodos	k1gInSc1
•	•	k?
Héraion	Héraion	k1gInSc1
a	a	k8xC
Pythagoreion	Pythagoreion	k1gInSc1
na	na	k7c4
Samu	sám	k3xTgFnSc4
•	•	k?
Soluň	Soluň	k1gFnSc4
•	•	k?
Vergina	Vergina	k1gFnSc1
•	•	k?
Filippi	Filipp	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
130890	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4051394-4	4051394-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80056641	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
312797568	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80056641	#num#	k4
</s>
