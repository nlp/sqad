<p>
<s>
Ovce	ovce	k1gFnSc1	ovce
tlustorohá	tlustorohat	k5eAaImIp3nS	tlustorohat
(	(	kIx(	(
<g/>
Ovis	Ovis	k1gInSc1	Ovis
canadensis	canadensis	k1gInSc1	canadensis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
druhů	druh	k1gInPc2	druh
divokých	divoký	k2eAgFnPc2d1	divoká
severoamerických	severoamerický	k2eAgFnPc2d1	severoamerická
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměr	k1gInPc4	rozměr
==	==	k?	==
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
<g/>
:	:	kIx,	:
samci	samec	k1gInSc6	samec
–	–	k?	–
106	[number]	k4	106
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
ocasu	ocas	k1gInSc2	ocas
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
samci	samec	k1gInSc3	samec
–	–	k?	–
160	[number]	k4	160
<g/>
–	–	k?	–
<g/>
180	[number]	k4	180
cm	cm	kA	cm
</s>
</p>
<p>
<s>
samice	samice	k1gFnSc1	samice
–	–	k?	–
150	[number]	k4	150
cmHmotnost	cmHmotnost	k1gFnSc1	cmHmotnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
samci	samec	k1gInSc3	samec
–	–	k?	–
119	[number]	k4	119
<g/>
–	–	k?	–
<g/>
140	[number]	k4	140
kg	kg	kA	kg
</s>
</p>
<p>
<s>
samice	samice	k1gFnSc1	samice
–	–	k?	–
53	[number]	k4	53
<g/>
–	–	k?	–
<g/>
91	[number]	k4	91
kg	kg	kA	kg
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
a	a	k8xC	a
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
získává	získávat	k5eAaImIp3nS	získávat
světlejší	světlý	k2eAgInSc1d2	světlejší
odstín	odstín	k1gInSc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Beranům	Beran	k1gMnPc3	Beran
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
mohutné	mohutný	k2eAgInPc4d1	mohutný
zatočené	zatočený	k2eAgInPc4d1	zatočený
rohy	roh	k1gInPc4	roh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
14	[number]	k4	14
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Rohy	roh	k1gInPc1	roh
samic	samice	k1gFnPc2	samice
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
a	a	k8xC	a
méně	málo	k6eAd2	málo
zahnuté	zahnutý	k2eAgNnSc1d1	zahnuté
<g/>
.	.	kIx.	.
</s>
<s>
Ovce	ovce	k1gFnSc1	ovce
tlustorohá	tlustorohý	k2eAgFnSc1d1	tlustorohý
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
životu	život	k1gInSc2	život
ve	v	k7c6	v
skalnatém	skalnatý	k2eAgInSc6d1	skalnatý
terénu	terén	k1gInSc6	terén
<g/>
,	,	kIx,	,
navzdory	navzdory	k7c3	navzdory
velké	velký	k2eAgFnSc3d1	velká
hmotnosti	hmotnost	k1gFnSc3	hmotnost
hlavy	hlava	k1gFnSc2	hlava
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
i	i	k8xC	i
plavat	plavat	k5eAaImF	plavat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ovis	Ovis	k6eAd1	Ovis
canadensis	canadensis	k1gFnSc6	canadensis
nelsoni	nelsoň	k1gFnSc6	nelsoň
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
ostatním	ostatní	k2eAgInPc3d1	ostatní
poddruhům	poddruh	k1gInPc3	poddruh
drobnější	drobný	k2eAgFnSc1d2	drobnější
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
rohy	roh	k1gInPc1	roh
jsou	být	k5eAaImIp3nP	být
plošší	plochý	k2eAgInPc1d2	plošší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Ovis	Ovis	k6eAd1	Ovis
canadensis	canadensis	k1gFnSc1	canadensis
canadensis	canadensis	k1gFnSc1	canadensis
a	a	k8xC	a
Ovis	Ovis	k1gInSc1	Ovis
canadensis	canadensis	k1gFnSc2	canadensis
sierrae	sierrae	k6eAd1	sierrae
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
Skalnatých	skalnatý	k2eAgFnPc6d1	skalnatá
horách	hora	k1gFnPc6	hora
od	od	k7c2	od
jižní	jižní	k2eAgFnSc2d1	jižní
Kanady	Kanada	k1gFnSc2	Kanada
až	až	k9	až
po	po	k7c4	po
Colorado	Colorado	k1gNnSc4	Colorado
<g/>
.	.	kIx.	.
</s>
<s>
Domovem	domov	k1gInSc7	domov
Ovis	Ovisa	k1gFnPc2	Ovisa
canadensis	canadensis	k1gFnPc2	canadensis
cremnobates	cremnobates	k1gInSc1	cremnobates
<g/>
,	,	kIx,	,
Ovis	Ovis	k1gInSc1	Ovis
canadensis	canadensis	k1gFnSc1	canadensis
mexicana	mexicana	k1gFnSc1	mexicana
a	a	k8xC	a
Ovis	Ovis	k1gInSc1	Ovis
canadensis	canadensis	k1gFnSc2	canadensis
nelsoni	nelsoň	k1gFnSc3	nelsoň
jsou	být	k5eAaImIp3nP	být
suché	suchý	k2eAgFnSc2d1	suchá
oblasti	oblast	k1gFnSc2	oblast
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
Nevady	Nevada	k1gFnSc2	Nevada
<g/>
,	,	kIx,	,
Utahu	Utah	k1gInSc2	Utah
<g/>
,	,	kIx,	,
východního	východní	k2eAgNnSc2d1	východní
Colorada	Colorado	k1gNnSc2	Colorado
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
Arizony	Arizona	k1gFnSc2	Arizona
<g/>
,	,	kIx,	,
jižního	jižní	k2eAgNnSc2d1	jižní
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
západního	západní	k2eAgInSc2d1	západní
Texasu	Texas	k1gInSc2	Texas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Jídelníček	jídelníček	k1gInSc1	jídelníček
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
hlavně	hlavně	k6eAd1	hlavně
travinami	travina	k1gFnPc7	travina
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
listím	listí	k1gNnSc7	listí
<g/>
.	.	kIx.	.
</s>
<s>
Pouštní	pouštní	k2eAgInPc1d1	pouštní
poddruhy	poddruh	k1gInPc1	poddruh
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
suchomilnými	suchomilný	k2eAgFnPc7d1	suchomilná
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
páření	páření	k1gNnSc3	páření
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
spolu	spolu	k6eAd1	spolu
berani	beran	k1gMnPc1	beran
svádějí	svádět	k5eAaImIp3nP	svádět
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
souboje	souboj	k1gInPc4	souboj
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
narážejí	narážet	k5eAaPmIp3nP	narážet
čely	čelo	k1gNnPc7	čelo
<g/>
.	.	kIx.	.
</s>
<s>
Údery	úder	k1gInPc1	úder
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc1d1	silná
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
zaslechnout	zaslechnout	k5eAaPmF	zaslechnout
až	až	k9	až
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
1,5	[number]	k4	1,5
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
samci	samec	k1gMnPc1	samec
spolu	spolu	k6eAd1	spolu
zápasí	zápasit	k5eAaImIp3nP	zápasit
i	i	k9	i
dvacet	dvacet	k4xCc4	dvacet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
souboj	souboj	k1gInSc1	souboj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
25,5	[number]	k4	25,5
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
třinácti	třináct	k4xCc2	třináct
až	až	k9	až
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
berani	beran	k1gMnPc1	beran
stávají	stávat	k5eAaImIp3nP	stávat
neplodnými	plodný	k2eNgFnPc7d1	neplodná
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
říje	říje	k1gFnSc2	říje
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
mimo	mimo	k7c4	mimo
místa	místo	k1gNnPc4	místo
páření	páření	k1gNnSc2	páření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Ovce	ovce	k1gFnPc1	ovce
tlustorohé	tlustorohý	k2eAgFnPc1d1	tlustorohý
tvoří	tvořit	k5eAaImIp3nP	tvořit
oddělená	oddělený	k2eAgNnPc1d1	oddělené
stáda	stádo	k1gNnPc1	stádo
samců	samec	k1gInPc2	samec
a	a	k8xC	a
samic	samice	k1gFnPc2	samice
čítající	čítající	k2eAgFnSc1d1	čítající
asi	asi	k9	asi
deset	deset	k4xCc1	deset
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
výjimečně	výjimečně	k6eAd1	výjimečně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
celého	celý	k2eAgInSc2d1	celý
dne	den	k1gInSc2	den
pasou	pást	k5eAaImIp3nP	pást
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
skal	skála	k1gFnPc2	skála
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc2	ohrožení
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
1	[number]	k4	1
800	[number]	k4	800
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
600	[number]	k4	600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
stahují	stahovat	k5eAaImIp3nP	stahovat
do	do	k7c2	do
níže	nízce	k6eAd2	nízce
položených	položený	k2eAgFnPc2d1	položená
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
750	[number]	k4	750
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snáze	snadno	k6eAd2	snadno
mohly	moct	k5eAaImAgFnP	moct
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
potravě	potrava	k1gFnSc3	potrava
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vyhrabávají	vyhrabávat	k5eAaImIp3nP	vyhrabávat
ze	z	k7c2	z
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgInPc2	tento
přesunů	přesun	k1gInPc2	přesun
mohou	moct	k5eAaImIp3nP	moct
urazit	urazit	k5eAaPmF	urazit
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
až	až	k9	až
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
říje	říje	k1gFnSc2	říje
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
po	po	k7c4	po
narození	narození	k1gNnSc4	narození
mláďat	mládě	k1gNnPc2	mládě
(	(	kIx(	(
<g/>
květen	květen	k1gInSc1	květen
a	a	k8xC	a
červen	červen	k1gInSc1	červen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
s	s	k7c7	s
matkami	matka	k1gFnPc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
opouštějí	opouštět	k5eAaImIp3nP	opouštět
mladí	mladý	k2eAgMnPc1d1	mladý
samci	samec	k1gMnPc1	samec
samičí	samičí	k2eAgFnSc2d1	samičí
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
připojují	připojovat	k5eAaImIp3nP	připojovat
se	se	k3xPyFc4	se
ke	k	k7c3	k
stádům	stádo	k1gNnPc3	stádo
beranů	beran	k1gMnPc2	beran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ovce	ovce	k1gFnSc2	ovce
tlustorohá	tlustorohat	k5eAaPmIp3nS	tlustorohat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Ovce	ovce	k1gFnSc1	ovce
tlustorohá	tlustorohat	k5eAaPmIp3nS	tlustorohat
na	na	k7c4	na
BioLibu	BioLiba	k1gFnSc4	BioLiba
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
</s>
</p>
