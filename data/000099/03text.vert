<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
posledním	poslední	k2eAgInSc7d1	poslední
dnem	den	k1gInSc7	den
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
vánoční	vánoční	k2eAgInPc4d1	vánoční
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
den	den	k1gInSc4	den
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
začínal	začínat	k5eAaImAgInS	začínat
nejčastěji	často	k6eAd3	často
právě	právě	k9	právě
na	na	k7c4	na
slavnost	slavnost	k1gFnSc4	slavnost
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Štědrým	štědrý	k2eAgInSc7d1	štědrý
dnem	den	k1gInSc7	den
je	být	k5eAaImIp3nS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
množství	množství	k1gNnSc1	množství
předkřesťanských	předkřesťanský	k2eAgInPc2d1	předkřesťanský
i	i	k8xC	i
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
vánočních	vánoční	k2eAgInPc2d1	vánoční
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
pověr	pověra	k1gFnPc2	pověra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
štědrovečerní	štědrovečerní	k2eAgFnSc1d1	štědrovečerní
večeře	večeře	k1gFnSc1	večeře
a	a	k8xC	a
rozsvěcování	rozsvěcování	k1gNnSc1	rozsvěcování
vánočního	vánoční	k2eAgInSc2d1	vánoční
stromečku	stromeček	k1gInSc2	stromeček
nebo	nebo	k8xC	nebo
pověra	pověra	k1gFnSc1	pověra
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
během	během	k7c2	během
dne	den	k1gInSc2	den
postí	postit	k5eAaImIp3nS	postit
<g/>
,	,	kIx,	,
uvidí	uvidět	k5eAaPmIp3nS	uvidět
večer	večer	k6eAd1	večer
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
prasátko	prasátko	k1gNnSc4	prasátko
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
chybný	chybný	k2eAgInSc1d1	chybný
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
Štědrý	štědrý	k2eAgInSc4d1	štědrý
večer	večer	k1gInSc4	večer
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
předvečer	předvečer	k1gInSc4	předvečer
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
židé	žid	k1gMnPc1	žid
počítají	počítat	k5eAaImIp3nP	počítat
počátek	počátek	k1gInSc4	počátek
dne	den	k1gInSc2	den
od	od	k7c2	od
západu	západ	k1gInSc2	západ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
od	od	k7c2	od
půlnoci	půlnoc	k1gFnSc2	půlnoc
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
narození	narození	k1gNnSc1	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
židovského	židovský	k2eAgInSc2d1	židovský
národa	národ	k1gInSc2	národ
a	a	k8xC	a
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
slaveno	slavit	k5eAaImNgNnS	slavit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dne	den	k1gInSc2	den
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
po	po	k7c6	po
západu	západ	k1gInSc6	západ
slunce	slunce	k1gNnSc1	slunce
24.12	[number]	k4	24.12
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
jako	jako	k8xS	jako
sváteční	sváteční	k2eAgInSc1d1	sváteční
den	den	k1gInSc1	den
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
vlastně	vlastně	k9	vlastně
omylem	omyl	k1gInSc7	omyl
poslance	poslanec	k1gMnSc2	poslanec
Ivana	Ivan	k1gMnSc2	Ivan
Fišery	Fišer	k1gMnPc7	Fišer
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
toto	tento	k3xDgNnSc4	tento
neuvědomil	uvědomit	k5eNaPmAgMnS	uvědomit
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
24.12	[number]	k4	24.12
<g/>
.	.	kIx.	.
jako	jako	k8xC	jako
státem	stát	k1gInSc7	stát
uznaný	uznaný	k2eAgInSc4d1	uznaný
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mají	mít	k5eAaImIp3nP	mít
Češi	Čech	k1gMnPc1	Čech
a	a	k8xC	a
Slováci	Slovák	k1gMnPc1	Slovák
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
jediní	jediný	k2eAgMnPc1d1	jediný
na	na	k7c6	na
světě	svět	k1gInSc6	svět
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
státem	stát	k1gInSc7	stát
uznané	uznaný	k2eAgNnSc4d1	uznané
volno	volno	k1gNnSc4	volno
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
i	i	k9	i
některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
církevní	církevní	k2eAgInPc1d1	církevní
svátky	svátek	k1gInPc1	svátek
slaví	slavit	k5eAaImIp3nP	slavit
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc1d1	štědrý
den	den	k1gInSc1	den
začínal	začínat	k5eAaImAgInS	začínat
poslední	poslední	k2eAgFnSc7d1	poslední
adventní	adventní	k2eAgFnSc7d1	adventní
jitřní	jitřní	k2eAgFnSc7d1	jitřní
mší	mše	k1gFnSc7	mše
(	(	kIx(	(
<g/>
roráty	roráty	k1gInPc4	roráty
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
požehnány	požehnán	k2eAgFnPc1d1	požehnána
ozdoby	ozdoba	k1gFnPc1	ozdoba
z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
zdobila	zdobit	k5eAaImAgFnS	zdobit
domácnost	domácnost	k1gFnSc1	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Katolický	katolický	k2eAgInSc1d1	katolický
liturgický	liturgický	k2eAgInSc1d1	liturgický
kalendář	kalendář	k1gInSc1	kalendář
dříve	dříve	k6eAd2	dříve
předepisoval	předepisovat	k5eAaImAgInS	předepisovat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
přísný	přísný	k2eAgInSc4d1	přísný
půst	půst	k1gInSc4	půst
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
den	den	k1gInSc4	den
buďto	buďto	k8xC	buďto
vůbec	vůbec	k9	vůbec
nejedlo	jíst	k5eNaImAgNnS	jíst
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
podával	podávat	k5eAaImAgMnS	podávat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
sporý	sporý	k2eAgInSc4d1	sporý
oběd	oběd	k1gInSc4	oběd
postního	postní	k2eAgInSc2d1	postní
charakteru	charakter	k1gInSc2	charakter
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
zvyků	zvyk	k1gInPc2	zvyk
kraje	kraj	k1gInSc2	kraj
či	či	k8xC	či
rodiny	rodina	k1gFnSc2	rodina
hubník	hubník	k1gInSc1	hubník
<g/>
,	,	kIx,	,
kuba	kuba	k1gNnSc1	kuba
<g/>
,	,	kIx,	,
muzika	muzika	k1gFnSc1	muzika
či	či	k8xC	či
jen	jen	k9	jen
nějaká	nějaký	k3yIgFnSc1	nějaký
řídká	řídký	k2eAgFnSc1d1	řídká
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dětem	dítě	k1gFnPc3	dítě
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
vydrží	vydržet	k5eAaPmIp3nS	vydržet
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
nejíst	jíst	k5eNaImF	jíst
<g/>
,	,	kIx,	,
uvidí	uvidět	k5eAaPmIp3nS	uvidět
večer	večer	k6eAd1	večer
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
prasátko	prasátko	k1gNnSc4	prasátko
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
dnešní	dnešní	k2eAgInSc1d1	dnešní
kalendář	kalendář	k1gInSc1	kalendář
tento	tento	k3xDgInSc4	tento
půst	půst	k1gInSc4	půst
již	již	k6eAd1	již
neukládá	ukládat	k5eNaImIp3nS	ukládat
<g/>
,	,	kIx,	,
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
katolických	katolický	k2eAgFnPc2d1	katolická
rodin	rodina	k1gFnPc2	rodina
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
na	na	k7c6	na
zvykové	zvykový	k2eAgFnSc6d1	zvyková
či	či	k8xC	či
dobrovolné	dobrovolný	k2eAgFnSc6d1	dobrovolná
bázi	báze	k1gFnSc6	báze
<g/>
.	.	kIx.	.
</s>
<s>
Vánoční	vánoční	k2eAgInSc1d1	vánoční
stromek	stromek	k1gInSc1	stromek
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
měšťanských	měšťanský	k2eAgFnPc6d1	měšťanská
rodinách	rodina	k1gFnPc6	rodina
začal	začít	k5eAaPmAgInS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
až	až	k9	až
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Stromeček	stromeček	k1gInSc1	stromeček
se	se	k3xPyFc4	se
zdobil	zdobit	k5eAaImAgInS	zdobit
cukrovím	cukroví	k1gNnSc7	cukroví
<g/>
,	,	kIx,	,
výrobky	výrobek	k1gInPc4	výrobek
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
perníku	perník	k1gInSc2	perník
<g/>
,	,	kIx,	,
pečiva	pečivo	k1gNnSc2	pečivo
a	a	k8xC	a
ovocem	ovoce	k1gNnSc7	ovoce
především	především	k6eAd1	především
jablky	jablko	k1gNnPc7	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Skleněné	skleněný	k2eAgFnPc1d1	skleněná
ozdoby	ozdoba	k1gFnPc1	ozdoba
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
až	až	k9	až
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
mohli	moct	k5eAaImAgMnP	moct
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
dovolit	dovolit	k5eAaPmF	dovolit
pouze	pouze	k6eAd1	pouze
bohaté	bohatý	k2eAgFnPc1d1	bohatá
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
jako	jako	k8xC	jako
takový	takový	k3xDgInSc4	takový
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k6eAd1	ještě
do	do	k7c2	do
adventu	advent	k1gInSc2	advent
<g/>
,	,	kIx,	,
s	s	k7c7	s
vyjitím	vyjití	k1gNnSc7	vyjití
první	první	k4xOgFnSc2	první
večerní	večerní	k2eAgFnSc2d1	večerní
hvězdy	hvězda	k1gFnSc2	hvězda
začínají	začínat	k5eAaImIp3nP	začínat
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
večeři	večeře	k1gFnSc6	večeře
bylo	být	k5eAaImAgNnS	být
zvykem	zvyk	k1gInSc7	zvyk
zpívat	zpívat	k5eAaImF	zpívat
koledy	koleda	k1gFnPc4	koleda
a	a	k8xC	a
případně	případně	k6eAd1	případně
rozdávat	rozdávat	k5eAaImF	rozdávat
dárky	dárek	k1gInPc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Nadělování	nadělování	k1gNnSc1	nadělování
dárků	dárek	k1gInPc2	dárek
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
<g/>
,	,	kIx,	,
památkou	památka	k1gFnSc7	památka
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
téměř	téměř	k6eAd1	téměř
úplně	úplně	k6eAd1	úplně
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
"	"	kIx"	"
<g/>
naděluje	nadělovat	k5eAaImIp3nS	nadělovat
dárky	dárek	k1gInPc4	dárek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nazýván	nazýván	k2eAgMnSc1d1	nazýván
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
víra	víra	k1gFnSc1	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
člověk	člověk	k1gMnSc1	člověk
dostává	dostávat	k5eAaImIp3nS	dostávat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
Boha	bůh	k1gMnSc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
totiž	totiž	k9	totiž
zbožní	zbožní	k2eAgMnPc1d1	zbožní
rodiče	rodič	k1gMnPc1	rodič
odpovídali	odpovídat	k5eAaImAgMnP	odpovídat
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
děkovaly	děkovat	k5eAaImAgFnP	děkovat
za	za	k7c4	za
dárky	dárek	k1gInPc4	dárek
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
poděkují	poděkovat	k5eAaPmIp3nP	poděkovat
Ježíši	Ježíš	k1gMnPc7	Ježíš
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgNnSc3	který
má	mít	k5eAaImIp3nS	mít
rodina	rodina	k1gFnSc1	rodina
vše	všechen	k3xTgNnSc4	všechen
co	co	k9	co
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Štědrému	štědrý	k2eAgInSc3d1	štědrý
večeru	večer	k1gInSc3	večer
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
také	také	k9	také
řada	řada	k1gFnSc1	řada
tradičních	tradiční	k2eAgInPc2d1	tradiční
obyčejů	obyčej	k1gInPc2	obyčej
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
naši	náš	k3xOp1gMnPc1	náš
předkové	předek	k1gMnPc1	předek
snažili	snažit	k5eAaImAgMnP	snažit
odhadnout	odhadnout	k5eAaPmF	odhadnout
průběh	průběh	k1gInSc4	průběh
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
zvyklosti	zvyklost	k1gFnPc4	zvyklost
patří	patřit	k5eAaImIp3nP	patřit
schování	schování	k1gNnSc2	schování
kapří	kapří	k2eAgFnSc2d1	kapří
šupiny	šupina	k1gFnSc2	šupina
pod	pod	k7c4	pod
talíř	talíř	k1gInSc4	talíř
<g/>
,	,	kIx,	,
pouštění	pouštění	k1gNnSc4	pouštění
lodiček	lodička	k1gFnPc2	lodička
<g/>
,	,	kIx,	,
rozkrajování	rozkrajování	k1gNnSc2	rozkrajování
jablka	jablko	k1gNnSc2	jablko
<g/>
,	,	kIx,	,
házení	házení	k1gNnSc2	házení
pantoflem	pantoflem	k?	pantoflem
<g/>
,	,	kIx,	,
klepání	klepání	k1gNnSc6	klepání
střevícem	střevíc	k1gInSc7	střevíc
na	na	k7c4	na
kurník	kurník	k1gInSc4	kurník
<g/>
,	,	kIx,	,
barborky	barborka	k1gFnPc4	barborka
<g/>
,	,	kIx,	,
lití	lití	k1gNnSc4	lití
olova	olovo	k1gNnSc2	olovo
apod.	apod.	kA	apod.
V	v	k7c6	v
katolické	katolický	k2eAgFnSc6d1	katolická
církvi	církev	k1gFnSc6	církev
večer	večer	k6eAd1	večer
Štědrého	štědrý	k2eAgInSc2d1	štědrý
dne	den	k1gInSc2	den
představuje	představovat	k5eAaImIp3nS	představovat
již	již	k6eAd1	již
začátek	začátek	k1gInSc4	začátek
oslavy	oslava	k1gFnSc2	oslava
Narození	narození	k1gNnSc2	narození
Páně	páně	k2eAgNnSc2d1	páně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vigilie	vigilie	k1gFnSc2	vigilie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
den	den	k1gInSc1	den
tradičně	tradičně	k6eAd1	tradičně
končí	končit	k5eAaImIp3nS	končit
půlnoční	půlnoční	k2eAgFnSc1d1	půlnoční
mše	mše	k1gFnSc1	mše
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
začíná	začínat	k5eAaImIp3nS	začínat
samotná	samotný	k2eAgFnSc1d1	samotná
církevní	církevní	k2eAgFnSc1d1	církevní
oslava	oslava	k1gFnSc1	oslava
narození	narození	k1gNnSc2	narození
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Hlas	hlas	k1gInSc1	hlas
půlnočních	půlnoční	k2eAgInPc2d1	půlnoční
zvonů	zvon	k1gInPc2	zvon
má	mít	k5eAaImIp3nS	mít
prý	prý	k9	prý
kouzelnou	kouzelný	k2eAgFnSc4d1	kouzelná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
hlasem	hlas	k1gInSc7	hlas
se	se	k3xPyFc4	se
o	o	k7c6	o
štědrovečerní	štědrovečerní	k2eAgFnSc6d1	štědrovečerní
noci	noc	k1gFnSc6	noc
otevírají	otevírat	k5eAaImIp3nP	otevírat
poklady	poklad	k1gInPc4	poklad
a	a	k8xC	a
probouzejí	probouzet	k5eAaImIp3nP	probouzet
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
viz	vidět	k5eAaImRp2nS	vidět
Pověsti	pověst	k1gFnSc2	pověst
o	o	k7c6	o
zvonech	zvon	k1gInPc6	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Večeře	večeře	k1gFnSc1	večeře
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
tradičně	tradičně	k6eAd1	tradičně
začíná	začínat	k5eAaImIp3nS	začínat
rybí	rybí	k2eAgFnSc7d1	rybí
polévkou	polévka	k1gFnSc7	polévka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
jídlo	jídlo	k1gNnSc1	jídlo
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
obvykle	obvykle	k6eAd1	obvykle
kapr	kapr	k1gMnSc1	kapr
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
ryby	ryba	k1gFnPc4	ryba
nejedí	jíst	k5eNaImIp3nP	jíst
<g/>
)	)	kIx)	)
vinná	vinný	k2eAgFnSc1d1	vinná
klobása	klobása	k1gFnSc1	klobása
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
se	s	k7c7	s
štědrovečerní	štědrovečerní	k2eAgFnSc7d1	štědrovečerní
večeří	večeře	k1gFnSc7	večeře
se	se	k3xPyFc4	se
pojí	pojit	k5eAaImIp3nS	pojit
krajově	krajově	k6eAd1	krajově
mnoho	mnoho	k4c1	mnoho
obyčejů	obyčej	k1gInPc2	obyčej
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
přežívají	přežívat	k5eAaImIp3nP	přežívat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
večeře	večeře	k1gFnSc1	večeře
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
rodinným	rodinný	k2eAgNnSc7d1	rodinné
společným	společný	k2eAgNnSc7d1	společné
jídlem	jídlo	k1gNnSc7	jídlo
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kapr	kapr	k1gMnSc1	kapr
coby	coby	k?	coby
hlavní	hlavní	k2eAgNnSc1d1	hlavní
večerní	večerní	k2eAgNnSc1d1	večerní
jídlo	jídlo	k1gNnSc1	jídlo
však	však	k9	však
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nemá	mít	k5eNaImIp3nS	mít
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Štědrý	štědrý	k2eAgInSc1d1	štědrý
den	den	k1gInSc1	den
zůstával	zůstávat	k5eAaImAgInS	zůstávat
postním	postní	k2eAgInSc7d1	postní
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
i	i	k9	i
charakter	charakter	k1gInSc1	charakter
jídla	jídlo	k1gNnSc2	jídlo
byl	být	k5eAaImAgInS	být
postní	postní	k2eAgInSc1d1	postní
<g/>
;	;	kIx,	;
za	za	k7c4	za
maso	maso	k1gNnSc4	maso
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nepokládalo	pokládat	k5eNaImAgNnS	pokládat
maso	maso	k1gNnSc1	maso
rybí	rybí	k2eAgNnSc1d1	rybí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dražší	drahý	k2eAgInPc1d2	dražší
než	než	k8xS	než
ostatní	ostatní	k2eAgInPc1d1	ostatní
druhy	druh	k1gInPc1	druh
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
většina	většina	k1gFnSc1	většina
domácností	domácnost	k1gFnPc2	domácnost
musela	muset	k5eAaImAgFnS	muset
dávat	dávat	k5eAaImF	dávat
přednost	přednost	k1gFnSc4	přednost
levnějším	levný	k2eAgFnPc3d2	levnější
alternativám	alternativa	k1gFnPc3	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Nejedl	jíst	k5eNaImAgMnS	jíst
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
kapr	kapr	k1gMnSc1	kapr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
i	i	k8xC	i
lín	lín	k1gMnSc1	lín
<g/>
,	,	kIx,	,
sumec	sumec	k1gMnSc1	sumec
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
sladkovodní	sladkovodní	k2eAgFnPc4d1	sladkovodní
ryby	ryba	k1gFnPc4	ryba
<g/>
;	;	kIx,	;
obvykle	obvykle	k6eAd1	obvykle
byly	být	k5eAaImAgInP	být
připravovány	připravovat	k5eAaImNgInP	připravovat
nasladko	nasladka	k1gFnSc5	nasladka
<g/>
.	.	kIx.	.
</s>
<s>
Kapr	kapr	k1gMnSc1	kapr
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
běžným	běžný	k2eAgNnSc7d1	běžné
štědrovečerním	štědrovečerní	k2eAgNnSc7d1	štědrovečerní
jídlem	jídlo	k1gNnSc7	jídlo
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jej	on	k3xPp3gInSc4	on
cenová	cenový	k2eAgFnSc1d1	cenová
regulace	regulace	k1gFnSc1	regulace
učinila	učinit	k5eAaImAgFnS	učinit
dostupným	dostupný	k2eAgInSc7d1	dostupný
pro	pro	k7c4	pro
široké	široký	k2eAgFnPc4d1	široká
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Klasickým	klasický	k2eAgInSc7d1	klasický
způsobem	způsob	k1gInSc7	způsob
přípravy	příprava	k1gFnSc2	příprava
-	-	kIx~	-
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
recept	recept	k1gInSc1	recept
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
kapr	kapr	k1gMnSc1	kapr
načerno	načerno	k6eAd1	načerno
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
připravoval	připravovat	k5eAaImAgMnS	připravovat
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
a	a	k8xC	a
pekl	péct	k5eAaImAgMnS	péct
se	se	k3xPyFc4	se
v	v	k7c6	v
omáčce	omáčka	k1gFnSc6	omáčka
z	z	k7c2	z
mandlí	mandle	k1gFnPc2	mandle
<g/>
,	,	kIx,	,
rozinek	rozinka	k1gFnPc2	rozinka
<g/>
,	,	kIx,	,
perníku	perník	k1gInSc2	perník
<g/>
,	,	kIx,	,
povidel	povidla	k1gNnPc2	povidla
<g/>
,	,	kIx,	,
ořechů	ořech	k1gInPc2	ořech
a	a	k8xC	a
sladkého	sladký	k2eAgNnSc2d1	sladké
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Podával	podávat	k5eAaImAgMnS	podávat
se	se	k3xPyFc4	se
se	s	k7c7	s
šiškami	šiška	k1gFnPc7	šiška
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
s	s	k7c7	s
knedlíky	knedlík	k1gInPc7	knedlík
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
recept	recept	k1gInSc1	recept
však	však	k9	však
postupně	postupně	k6eAd1	postupně
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
složité	složitý	k2eAgFnSc3d1	složitá
přípravě	příprava	k1gFnSc3	příprava
zcela	zcela	k6eAd1	zcela
z	z	k7c2	z
jídelníčku	jídelníček	k1gInSc2	jídelníček
zmizel	zmizet	k5eAaPmAgMnS	zmizet
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
kuchyně	kuchyně	k1gFnSc2	kuchyně
však	však	k9	však
z	z	k7c2	z
rakouské	rakouský	k2eAgFnSc2d1	rakouská
kuchyně	kuchyně	k1gFnSc2	kuchyně
přišel	přijít	k5eAaPmAgMnS	přijít
kapr	kapr	k1gMnSc1	kapr
smažený	smažený	k2eAgMnSc1d1	smažený
<g/>
,	,	kIx,	,
na	na	k7c4	na
něhož	jenž	k3xRgMnSc4	jenž
nacházíme	nacházet	k5eAaImIp1nP	nacházet
recept	recept	k1gInSc4	recept
v	v	k7c6	v
Domácí	domácí	k2eAgFnSc6d1	domácí
kuchařce	kuchařka	k1gFnSc6	kuchařka
od	od	k7c2	od
Magdalény	Magdaléna	k1gFnSc2	Magdaléna
Dobromily	Dobromila	k1gFnSc2	Dobromila
Rettigové	Rettigový	k2eAgFnSc2d1	Rettigová
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kapru	kapr	k1gMnSc3	kapr
se	se	k3xPyFc4	se
jako	jako	k9	jako
příloha	příloha	k1gFnSc1	příloha
podává	podávat	k5eAaImIp3nS	podávat
bramborový	bramborový	k2eAgInSc4d1	bramborový
salát	salát	k1gInSc4	salát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovšem	ovšem	k9	ovšem
taktéž	taktéž	k?	taktéž
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kuchyni	kuchyně	k1gFnSc6	kuchyně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Rodinné	rodinný	k2eAgInPc1d1	rodinný
recepty	recept	k1gInPc1	recept
na	na	k7c4	na
salát	salát	k1gInSc4	salát
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
;	;	kIx,	;
základ	základ	k1gInSc1	základ
tvoří	tvořit	k5eAaImIp3nS	tvořit
brambory	brambor	k1gInPc4	brambor
<g/>
,	,	kIx,	,
petržel	petržel	k1gFnSc1	petržel
<g/>
,	,	kIx,	,
celer	celer	k1gInSc1	celer
<g/>
,	,	kIx,	,
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
hrášek	hrášek	k1gInSc1	hrášek
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnSc1	cibule
<g/>
,	,	kIx,	,
kyselé	kyselý	k2eAgInPc4d1	kyselý
okurky	okurek	k1gInPc4	okurek
spojené	spojený	k2eAgInPc4d1	spojený
majonézou	majonéza	k1gFnSc7	majonéza
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
přidávat	přidávat	k5eAaImF	přidávat
též	též	k9	též
např.	např.	kA	např.
vejce	vejce	k1gNnSc4	vejce
<g/>
,	,	kIx,	,
salám	salám	k1gInSc1	salám
<g/>
,	,	kIx,	,
šunka	šunka	k1gFnSc1	šunka
či	či	k8xC	či
sýr	sýr	k1gInSc1	sýr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předválečných	předválečný	k2eAgFnPc6d1	předválečná
kuchařkách	kuchařka	k1gFnPc6	kuchařka
nenalezneme	naleznout	k5eNaPmIp1nP	naleznout
na	na	k7c4	na
bramborový	bramborový	k2eAgInSc4d1	bramborový
salát	salát	k1gInSc4	salát
recept	recept	k1gInSc4	recept
<g/>
,	,	kIx,	,
odborníci	odborník	k1gMnPc1	odborník
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kuchyni	kuchyně	k1gFnSc6	kuchyně
objevuje	objevovat	k5eAaImIp3nS	objevovat
až	až	k9	až
někdy	někdy	k6eAd1	někdy
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
tkví	tkvět	k5eAaImIp3nS	tkvět
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
ruské	ruský	k2eAgFnSc6d1	ruská
kuchyni	kuchyně	k1gFnSc6	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
obvykle	obvykle	k6eAd1	obvykle
vysílají	vysílat	k5eAaImIp3nP	vysílat
pohádky	pohádka	k1gFnPc4	pohádka
(	(	kIx(	(
<g/>
Tři	tři	k4xCgInPc1	tři
oříšky	oříšek	k1gInPc1	oříšek
pro	pro	k7c4	pro
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
Zlatovláska	zlatovláska	k1gFnSc1	zlatovláska
<g/>
,	,	kIx,	,
O	o	k7c6	o
princezně	princezna	k1gFnSc6	princezna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ráčkovala	ráčkovat	k5eAaImAgFnS	ráčkovat
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
se	s	k7c7	s
zlatou	zlatý	k2eAgFnSc7d1	zlatá
hvězdou	hvězda	k1gFnSc7	hvězda
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
Pyšná	pyšný	k2eAgFnSc1d1	pyšná
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
Šíleně	šíleně	k6eAd1	šíleně
smutná	smutný	k2eAgFnSc1d1	smutná
princezna	princezna	k1gFnSc1	princezna
<g/>
,	,	kIx,	,
S	s	k7c7	s
čerty	čert	k1gMnPc7	čert
nejsou	být	k5eNaImIp3nP	být
žerty	žert	k1gInPc1	žert
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
staré	starý	k2eAgFnPc1d1	stará
české	český	k2eAgFnPc1d1	Česká
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
komedie	komedie	k1gFnPc1	komedie
a	a	k8xC	a
muzikály	muzikál	k1gInPc1	muzikál
(	(	kIx(	(
<g/>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
..	..	k?	..
<g/>
,	,	kIx,	,
Sám	sám	k3xTgMnSc1	sám
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
Přednosta	přednosta	k1gMnSc1	přednosta
stanice	stanice	k1gFnSc2	stanice
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
s	s	k7c7	s
Vlastou	Vlasta	k1gMnSc7	Vlasta
Burianem	Burian	k1gMnSc7	Burian
<g/>
,	,	kIx,	,
Četník	četník	k1gMnSc1	četník
ze	z	k7c2	z
Saint	Sainta	k1gFnPc2	Sainta
Tropez	Tropeza	k1gFnPc2	Tropeza
a	a	k8xC	a
další	další	k2eAgFnSc1d1	další
s	s	k7c7	s
Louisem	Louis	k1gMnSc7	Louis
de	de	k?	de
Funè	Funè	k1gMnSc7	Funè
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
nosí	nosit	k5eAaImIp3nP	nosit
dárky	dárek	k1gInPc4	dárek
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
částech	část	k1gFnPc6	část
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Ježíšek	Ježíšek	k1gMnSc1	Ježíšek
(	(	kIx(	(
<g/>
Christkind	Christkind	k1gMnSc1	Christkind
<g/>
,	,	kIx,	,
Jesulein	Jesulein	k1gMnSc1	Jesulein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
zvyklost	zvyklost	k1gFnSc1	zvyklost
panuje	panovat	k5eAaImIp3nS	panovat
i	i	k9	i
v	v	k7c6	v
Horním	horní	k2eAgNnSc6d1	horní
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
,	,	kIx,	,
kulturně	kulturně	k6eAd1	kulturně
původně	původně	k6eAd1	původně
německém	německý	k2eAgInSc6d1	německý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dárky	dárek	k1gInPc4	dárek
přináší	přinášet	k5eAaImIp3nS	přinášet
Děťátko	děťátko	k1gNnSc1	děťátko
(	(	kIx(	(
<g/>
Dzieciątko	Dzieciątka	k1gMnSc5	Dzieciątka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
polských	polský	k2eAgInPc6d1	polský
regionech	region	k1gInPc6	region
dárky	dárek	k1gInPc4	dárek
přináší	přinášet	k5eAaImIp3nS	přinášet
vždy	vždy	k6eAd1	vždy
někdo	někdo	k3yInSc1	někdo
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Děťátka	děťátko	k1gNnSc2	děťátko
(	(	kIx(	(
<g/>
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Gwiazdka	Gwiazdka	k1gFnSc1	Gwiazdka
(	(	kIx(	(
<g/>
Hvězdička	hvězdička	k1gFnSc1	hvězdička
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgFnSc1	první
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
(	(	kIx(	(
<g/>
św	św	k?	św
<g/>
.	.	kIx.	.
</s>
<s>
Mikołaj	Mikołaj	k1gFnSc1	Mikołaj
<g/>
,	,	kIx,	,
Mazovsko	Mazovsko	k1gNnSc1	Mazovsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andílek	andílek	k1gMnSc1	andílek
(	(	kIx(	(
<g/>
Aniołek	Aniołek	k1gInSc1	Aniołek
<g/>
,	,	kIx,	,
Malopolsko	Malopolsko	k1gNnSc1	Malopolsko
<g/>
,	,	kIx,	,
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gwiazdor	Gwiazdor	k1gMnSc1	Gwiazdor
(	(	kIx(	(
<g/>
stařec	stařec	k1gMnSc1	stařec
s	s	k7c7	s
holí	hole	k1gFnSc7	hole
<g/>
,	,	kIx,	,
Velkopolsko	Velkopolsko	k1gNnSc1	Velkopolsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
vozí	vozit	k5eAaImIp3nP	vozit
dárky	dárek	k1gInPc7	dárek
Santa	Santa	k1gMnSc1	Santa
Claus	Claus	k1gMnSc1	Claus
<g/>
.	.	kIx.	.
</s>
<s>
Obdarovaní	obdarovaný	k2eAgMnPc1d1	obdarovaný
je	on	k3xPp3gNnSc4	on
najdou	najít	k5eAaPmIp3nP	najít
ráno	ráno	k6eAd1	ráno
25	[number]	k4	25
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Létá	létat	k5eAaImIp3nS	létat
vzduchem	vzduch	k1gInSc7	vzduch
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tažené	tažený	k2eAgInPc1d1	tažený
kouzelnými	kouzelný	k2eAgMnPc7d1	kouzelný
soby	sob	k1gMnPc7	sob
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
pomalu	pomalu	k6eAd1	pomalu
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Tradice	tradice	k1gFnSc1	tradice
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
i	i	k9	i
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
nebo	nebo	k8xC	nebo
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
i	i	k9	i
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nosí	nosit	k5eAaImIp3nS	nosit
dárky	dárek	k1gInPc4	dárek
Pè	Pè	k1gFnSc2	Pè
Noël	Noëla	k1gFnPc2	Noëla
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
nosí	nosit	k5eAaImIp3nP	nosit
dárky	dárek	k1gInPc4	dárek
Tři	tři	k4xCgMnPc4	tři
králové	král	k1gMnPc1	král
(	(	kIx(	(
<g/>
Los	los	k1gMnSc1	los
Reyes	Reyes	k1gMnSc1	Reyes
Magos	Magos	k1gMnSc1	Magos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
se	se	k3xPyFc4	se
nenaděluje	nadělovat	k5eNaImIp3nS	nadělovat
na	na	k7c4	na
Štědrý	štědrý	k2eAgInSc4d1	štědrý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
přináší	přinášet	k5eAaImIp3nS	přinášet
dárky	dárek	k1gInPc4	dárek
Děda	děda	k1gMnSc1	děda
Mráz	Mráz	k1gMnSc1	Mráz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
Sněhurky	Sněhurka	k1gFnSc2	Sněhurka
na	na	k7c6	na
saních	saně	k1gFnPc6	saně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
děti	dítě	k1gFnPc1	dítě
Joulupukki	Joulupukki	k1gNnSc2	Joulupukki
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
oděná	oděný	k2eAgFnSc1d1	oděná
do	do	k7c2	do
kozlí	kozlí	k2eAgFnSc2d1	kozlí
kůže	kůže	k1gFnSc2	kůže
bydlící	bydlící	k2eAgMnPc1d1	bydlící
v	v	k7c6	v
Laponsku	Laponsko	k1gNnSc6	Laponsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
připomíná	připomínat	k5eAaImIp3nS	připomínat
českého	český	k2eAgMnSc4d1	český
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
tradice	tradice	k1gFnSc1	tradice
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
-	-	kIx~	-
zde	zde	k6eAd1	zde
naděluje	nadělovat	k5eAaImIp3nS	nadělovat
dárky	dárek	k1gInPc4	dárek
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
hodná	hodný	k2eAgFnSc1d1	hodná
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
Befana	Befana	k1gFnSc1	Befana
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
ale	ale	k8xC	ale
dárky	dárek	k1gInPc7	dárek
rozdává	rozdávat	k5eAaImIp3nS	rozdávat
až	až	k9	až
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
i	i	k9	i
zde	zde	k6eAd1	zde
stále	stále	k6eAd1	stále
víc	hodně	k6eAd2	hodně
projevuje	projevovat	k5eAaImIp3nS	projevovat
vliv	vliv	k1gInSc1	vliv
Santy	Santa	k1gFnSc2	Santa
Clause	Clause	k1gFnSc2	Clause
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
nosí	nosit	k5eAaImIp3nP	nosit
dárky	dárek	k1gInPc4	dárek
skřítci	skřítek	k1gMnPc1	skřítek
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
zde	zde	k6eAd1	zde
říkají	říkat	k5eAaImIp3nP	říkat
nisser	nisser	k1gInSc4	nisser
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
dárky	dárek	k1gInPc4	dárek
naděluje	nadělovat	k5eAaImIp3nS	nadělovat
Sinterklaas	Sinterklaas	k1gMnSc1	Sinterklaas
nebo	nebo	k8xC	nebo
Saint	Saint	k1gMnSc1	Saint
Nikolas	Nikolas	k1gMnSc1	Nikolas
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
jsou	být	k5eAaImIp3nP	být
dárky	dárek	k1gInPc1	dárek
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
obdobnou	obdobný	k2eAgFnSc4d1	obdobná
Santu	Santa	k1gFnSc4	Santa
Clausovi	Clausův	k2eAgMnPc1d1	Clausův
-	-	kIx~	-
Babbo	Babba	k1gFnSc5	Babba
Natale	Natal	k1gInSc5	Natal
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
nosí	nosit	k5eAaImIp3nP	nosit
dárky	dárek	k1gInPc1	dárek
přihrblý	přihrblý	k2eAgMnSc1d1	přihrblý
dědeček	dědeček	k1gMnSc1	dědeček
Jultomten	Jultomten	k2eAgMnSc1d1	Jultomten
(	(	kIx(	(
<g/>
Vánoční	vánoční	k2eAgMnSc1d1	vánoční
muž	muž	k1gMnSc1	muž
<g/>
)	)	kIx)	)
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
vánočních	vánoční	k2eAgMnPc2d1	vánoční
skřítků	skřítek	k1gMnPc2	skřítek
Julnissarů	Julnissar	k1gInPc2	Julnissar
<g/>
.	.	kIx.	.
</s>
