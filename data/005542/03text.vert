<s>
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
je	být	k5eAaImIp3nS	být
záliv	záliv	k1gInSc4	záliv
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
pokračování	pokračování	k1gNnSc4	pokračování
Ománského	ománský	k2eAgInSc2d1	ománský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yQgFnPc3	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
Hormuzským	Hormuzský	k2eAgInSc7d1	Hormuzský
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Arabským	arabský	k2eAgInSc7d1	arabský
poloostrovem	poloostrov	k1gInSc7	poloostrov
a	a	k8xC	a
Íránem	Írán	k1gInSc7	Írán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ústupu	ústup	k1gInSc2	ústup
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
záliv	záliv	k1gInSc1	záliv
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
2000	[number]	k4	2000
let	léto	k1gNnPc2	léto
znatelně	znatelně	k6eAd1	znatelně
zkrátil	zkrátit	k5eAaPmAgInS	zkrátit
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
starověké	starověký	k2eAgNnSc1d1	starověké
město	město	k1gNnSc1	město
Ur	Ur	k1gInSc1	Ur
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
leží	ležet	k5eAaImIp3nS	ležet
jeho	jeho	k3xOp3gInPc1	jeho
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
Iráku	Irák	k1gInSc2	Irák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
referoval	referovat	k5eAaBmAgMnS	referovat
například	například	k6eAd1	například
Sir	sir	k1gMnSc1	sir
Arnold	Arnold	k1gMnSc1	Arnold
Wilson	Wilson	k1gMnSc1	Wilson
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
vydané	vydaný	k2eAgFnSc6d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
:	:	kIx,	:
Řada	řada	k1gFnSc1	řada
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
jej	on	k3xPp3gMnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Arabský	arabský	k2eAgInSc4d1	arabský
záliv	záliv	k1gInSc4	záliv
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
Záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
ale	ale	k8xC	ale
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c4	na
pojmenování	pojmenování	k1gNnSc4	pojmenování
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
vášnivě	vášnivě	k6eAd1	vášnivě
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
případů	případ	k1gInPc2	případ
vyhostil	vyhostit	k5eAaPmAgInS	vyhostit
řeckého	řecký	k2eAgMnSc4d1	řecký
stewarda	steward	k1gMnSc4	steward
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
nazval	nazvat	k5eAaPmAgInS	nazvat
Arabský	arabský	k2eAgInSc1d1	arabský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiném	jiné	k1gNnSc6	jiné
pak	pak	k6eAd1	pak
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
žalobou	žaloba	k1gFnSc7	žaloba
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gFnSc2	Google
kvůli	kvůli	k7c3	kvůli
nepoužívání	nepoužívání	k1gNnSc3	nepoužívání
názvu	název	k1gInSc2	název
Perský	perský	k2eAgInSc4d1	perský
záliv	záliv	k1gInSc4	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
239	[number]	k4	239
000	[number]	k4	000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
délku	délka	k1gFnSc4	délka
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
a	a	k8xC	a
šířku	šířka	k1gFnSc4	šířka
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
380	[number]	k4	380
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
však	však	k9	však
malé	malý	k2eAgFnPc1d1	malá
hloubky	hloubka	k1gFnPc1	hloubka
do	do	k7c2	do
50	[number]	k4	50
m	m	kA	m
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
102	[number]	k4	102
m.	m.	k?	m.
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
33	[number]	k4	33
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
slanost	slanost	k1gFnSc1	slanost
37	[number]	k4	37
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
horké	horký	k2eAgNnSc1d1	horké
a	a	k8xC	a
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
,	,	kIx,	,
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
zde	zde	k6eAd1	zde
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
prakticky	prakticky	k6eAd1	prakticky
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hodnot	hodnota	k1gFnPc2	hodnota
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
jako	jako	k8xS	jako
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ahváz	Ahváza	k1gFnPc2	Ahváza
či	či	k8xC	či
Abú	abú	k1gMnPc2	abú
Zabí	Zabí	k2eAgFnSc2d1	Zabí
teploty	teplota	k1gFnSc2	teplota
šplhají	šplhat	k5eAaImIp3nP	šplhat
až	až	k6eAd1	až
k	k	k7c3	k
53	[number]	k4	53
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
mnohdy	mnohdy	k6eAd1	mnohdy
neklesají	klesat	k5eNaImIp3nP	klesat
pod	pod	k7c7	pod
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
teplejší	teplý	k2eAgFnPc1d2	teplejší
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ománu	Omán	k1gInSc2	Omán
či	či	k8xC	či
SAE	SAE	kA	SAE
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
průměrná	průměrný	k2eAgFnSc1d1	průměrná
<g />
.	.	kIx.	.
</s>
<s>
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
na	na	k7c6	na
severu	sever	k1gInSc6	sever
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kuvajtu	Kuvajt	k1gInSc2	Kuvajt
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
zimní	zimní	k2eAgInSc1d1	zimní
průměr	průměr	k1gInSc1	průměr
13	[number]	k4	13
°	°	k?	°
<g/>
C.	C.	kA	C.
Místní	místní	k2eAgNnSc1d1	místní
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vlhkostí	vlhkost	k1gFnSc7	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
často	často	k6eAd1	často
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
50	[number]	k4	50
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
pocitově	pocitově	k6eAd1	pocitově
ještě	ještě	k6eAd1	ještě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
je	být	k5eAaImIp3nS	být
také	také	k9	také
největším	veliký	k2eAgNnSc7d3	veliký
světovým	světový	k2eAgNnSc7d1	světové
nalezištěm	naleziště	k1gNnSc7	naleziště
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Ropná	ropný	k2eAgNnPc1d1	ropné
naleziště	naleziště	k1gNnPc1	naleziště
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
asi	asi	k9	asi
v	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
počtu	počet	k1gInSc6	počet
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
Kuvajtem	Kuvajt	k1gInSc7	Kuvajt
<g/>
,	,	kIx,	,
Irákem	Irák	k1gInSc7	Irák
a	a	k8xC	a
Íránem	Írán	k1gInSc7	Írán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
se	se	k3xPyFc4	se
zase	zase	k9	zase
dováží	dovážet	k5eAaImIp3nS	dovážet
ropa	ropa	k1gFnSc1	ropa
vytěžená	vytěžený	k2eAgFnSc1d1	vytěžená
v	v	k7c6	v
obřím	obří	k2eAgNnSc6d1	obří
poli	pole	k1gNnSc6	pole
Ghawar	Ghawara	k1gFnPc2	Ghawara
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
největších	veliký	k2eAgNnPc2d3	veliký
překladišť	překladiště	k1gNnPc2	překladiště
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Ras	ras	k1gMnSc1	ras
Tanuře	Tanura	k1gFnSc6	Tanura
<g/>
,	,	kIx,	,
situované	situovaný	k2eAgFnSc6d1	situovaná
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
od	od	k7c2	od
sousedního	sousední	k2eAgInSc2d1	sousední
Bahrajnu	Bahrajn	k1gInSc2	Bahrajn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Kiš	kiš	k0	kiš
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
světových	světový	k2eAgFnPc2d1	světová
burz	burza	k1gFnPc2	burza
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
Íránská	íránský	k2eAgFnSc1d1	íránská
ropná	ropný	k2eAgFnSc1d1	ropná
burza	burza	k1gFnSc1	burza
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
ropy	ropa	k1gFnSc2	ropa
je	být	k5eAaImIp3nS	být
dováženo	dovážit	k5eAaPmNgNnS	dovážit
z	z	k7c2	z
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
v	v	k7c6	v
regionu	region	k1gInSc6	region
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
tamní	tamní	k2eAgInSc4d1	tamní
vývoz	vývoz	k1gInSc4	vývoz
ropy	ropa	k1gFnSc2	ropa
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
armáda	armáda	k1gFnSc1	armáda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Náhlé	náhlý	k2eAgNnSc1d1	náhlé
bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
vakuum	vakuum	k1gNnSc1	vakuum
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
íránský	íránský	k2eAgMnSc1d1	íránský
šáh	šáh	k1gMnSc1	šáh
Páhlaví	Páhlavý	k2eAgMnPc1d1	Páhlavý
stával	stávat	k5eAaImAgInS	stávat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
méně	málo	k6eAd2	málo
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
chtělo	chtít	k5eAaImAgNnS	chtít
vyplnit	vyplnit	k5eAaPmF	vyplnit
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Nixon	Nixon	k1gMnSc1	Nixon
<g/>
.	.	kIx.	.
</s>
<s>
Zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
stálou	stálý	k2eAgFnSc4d1	stálá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
přítomnost	přítomnost	k1gFnSc4	přítomnost
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Jimmy	Jimma	k1gFnSc2	Jimma
Carter	Carter	k1gInSc1	Carter
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1980	[number]	k4	1980
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
za	za	k7c4	za
americkou	americký	k2eAgFnSc4d1	americká
zónu	zóna	k1gFnSc4	zóna
vlivu	vliv	k1gInSc2	vliv
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
každý	každý	k3xTgInSc1	každý
pokus	pokus	k1gInSc1	pokus
vnější	vnější	k2eAgFnSc2d1	vnější
moci	moc	k1gFnSc2	moc
získat	získat	k5eAaPmF	získat
vliv	vliv	k1gInSc4	vliv
nad	nad	k7c7	nad
Perským	perský	k2eAgInSc7d1	perský
zálivem	záliv	k1gInSc7	záliv
bude	být	k5eAaImBp3nS	být
[	[	kIx(	[
<g/>
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
]	]	kIx)	]
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
zásadní	zásadní	k2eAgInSc1d1	zásadní
útok	útok	k1gInSc1	útok
na	na	k7c4	na
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc4d1	důležitý
zájmy	zájem	k1gInPc4	zájem
USA	USA	kA	USA
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
<g />
.	.	kIx.	.
</s>
<s>
bude	být	k5eAaImBp3nS	být
odražen	odražen	k2eAgMnSc1d1	odražen
všemi	všecek	k3xTgInPc7	všecek
dostupnými	dostupný	k2eAgInPc7d1	dostupný
prostředky	prostředek	k1gInPc7	prostředek
včetně	včetně	k7c2	včetně
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
invazi	invaze	k1gFnSc4	invaze
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
během	během	k7c2	během
pozdní	pozdní	k2eAgFnSc2d1	pozdní
Studené	Studené	k2eAgFnSc2d1	Studené
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Carterovy	Carterův	k2eAgFnSc2d1	Carterova
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
budou	být	k5eAaImBp3nP	být
více	hodně	k6eAd2	hodně
využívat	využívat	k5eAaPmF	využívat
své	svůj	k3xOyFgFnPc4	svůj
vojenské	vojenský	k2eAgFnPc4d1	vojenská
síly	síla	k1gFnPc4	síla
k	k	k7c3	k
hájení	hájení	k1gNnSc3	hájení
svých	svůj	k3xOyFgInPc2	svůj
národních	národní	k2eAgInPc2d1	národní
zájmů	zájem	k1gInPc2	zájem
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
vojenská	vojenský	k2eAgFnSc1d1	vojenská
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
navýšila	navýšit	k5eAaPmAgFnS	navýšit
během	během	k7c2	během
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Irákem	Irák	k1gInSc7	Irák
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
91	[number]	k4	91
<g/>
,	,	kIx,	,
a	a	k8xC	a
čítá	čítat	k5eAaImIp3nS	čítat
letadlové	letadlový	k2eAgFnPc4d1	letadlová
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
stíhačky	stíhačka	k1gFnPc4	stíhačka
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
daňové	daňový	k2eAgFnPc1d1	daňová
poplatníky	poplatník	k1gMnPc4	poplatník
tento	tento	k3xDgInSc1	tento
vojenský	vojenský	k2eAgInSc1d1	vojenský
stav	stav	k1gInSc1	stav
stojí	stát	k5eAaImIp3nS	stát
60	[number]	k4	60
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
ročně	ročně	k6eAd1	ročně
<g/>
;	;	kIx,	;
pramení	pramenit	k5eAaImIp3nS	pramenit
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
ale	ale	k9	ale
i	i	k9	i
nesporná	sporný	k2eNgFnSc1d1	nesporná
strategická	strategický	k2eAgFnSc1d1	strategická
kontrola	kontrola	k1gFnSc1	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
deníku	deník	k1gInSc2	deník
Times	Times	k1gInSc4	Times
z	z	k7c2	z
konce	konec	k1gInSc2	konec
května	květen	k1gInSc2	květen
2010	[number]	k4	2010
umístí	umístit	k5eAaPmIp3nS	umístit
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
Izrael	Izrael	k1gInSc4	Izrael
do	do	k7c2	do
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
k	k	k7c3	k
íránskému	íránský	k2eAgNnSc3d1	íránské
pobřeží	pobřeží	k1gNnSc3	pobřeží
tři	tři	k4xCgFnPc4	tři
ponorky	ponorka	k1gFnPc1	ponorka
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
vypustit	vypustit	k5eAaPmF	vypustit
nukleární	nukleární	k2eAgFnSc4d1	nukleární
střelu	střela	k1gFnSc4	střela
i	i	k8xC	i
vysílat	vysílat	k5eAaImF	vysílat
špiony	špion	k1gMnPc4	špion
na	na	k7c4	na
íránské	íránský	k2eAgNnSc4d1	íránské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
