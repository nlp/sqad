<s>
Perský	perský	k2eAgInSc1d1	perský
záliv	záliv	k1gInSc1	záliv
je	být	k5eAaImIp3nS	být
záliv	záliv	k1gInSc4	záliv
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
pokračování	pokračování	k1gNnSc4	pokračování
Ománského	ománský	k2eAgInSc2d1	ománský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yIgFnPc3	který
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
Hormuzským	Hormuzský	k2eAgInSc7d1	Hormuzský
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
