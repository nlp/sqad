<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
DNA	dna	k1gFnSc1	dna
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
deoxyribonucleic	deoxyribonucleice	k1gInPc2	deoxyribonucleice
acid	acido	k1gNnPc2	acido
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
DNK	DNK	kA	DNK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
všech	všecek	k3xTgInPc2	všecek
organismů	organismus	k1gInPc2	organismus
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
některých	některý	k3yIgMnPc2	některý
nebuněčných	buněčný	k2eNgMnPc2d1	buněčný
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
hraje	hrát	k5eAaImIp3nS	hrát
tuto	tento	k3xDgFnSc4	tento
úlohu	úloha	k1gFnSc4	úloha
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
např.	např.	kA	např.
RNA	RNA	kA	RNA
viry	vir	k1gInPc1	vir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
