<s>
Námět	námět	k1gInSc1
<g/>
:	:	kIx,
Adolf	Adolf	k1gMnSc1
Branald	Branald	k1gMnSc1
Scénář	scénář	k1gInSc1
<g/>
:	:	kIx,
Vladimír	Vladimír	k1gMnSc1
Bor	bor	k1gInSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Kachyňa	Kachyňa	k1gMnSc1
Hudba	hudba	k1gFnSc1
<g/>
:	:	kIx,
Luboš	Luboš	k1gMnSc1
Fišer	Fišer	k1gMnSc1
Kamera	kamera	k1gFnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Čuřík	Čuřík	k1gMnSc1
Režie	režie	k1gFnSc1
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Kachyňa	Kachyňa	k1gMnSc1
Hrají	hrát	k5eAaImIp3nP
<g/>
:	:	kIx,
Jiřina	Jiřina	k1gFnSc1
Jirásková	Jirásková	k1gFnSc1
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
Mihulová	Mihulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Husák	Husák	k1gMnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
Vízner	Vízner	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Vetchý	vetchý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Růžička	Růžička	k1gMnSc1
<g/>
,	,	kIx,
Otto	Otto	k1gMnSc1
Lackovič	Lackovič	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Žák	Žák	k1gMnSc1
Další	další	k2eAgInPc4d1
údaje	údaj	k1gInPc4
<g/>
:	:	kIx,
barevný	barevný	k2eAgInSc4d1
<g/>
,	,	kIx,
85	[number]	k4
min	mina	k1gFnPc2
<g/>
,	,	kIx,
hořká	hořký	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
Výroba	výroba	k1gFnSc1
<g/>
:	:	kIx,
ČSSR	ČSSR	kA
<g/>
,	,	kIx,
Filmové	filmový	k2eAgNnSc1d1
studio	studio	k1gNnSc1
Barrandov	Barrandov	k1gInSc1
<g/>
,	,	kIx,
1983	[number]	k4
Sestřičky	sestřička	k1gFnSc2
v	v	k7c6
Česko-Slovenské	českolovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc3d1
databázi	databáze	k1gFnSc3
Sestřičky	sestřička	k1gFnSc2
na	na	k7c6
Kinoboxu	Kinobox	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>