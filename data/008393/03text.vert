<p>
<s>
Panova	Panův	k2eAgFnSc1d1	Panova
flétna	flétna	k1gFnSc1	flétna
nebo	nebo	k8xC	nebo
také	také	k9	také
syrinx	syrinx	k1gInSc1	syrinx
je	být	k5eAaImIp3nS	být
dechový	dechový	k2eAgInSc1d1	dechový
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
-	-	kIx~	-
aerofon	aerofon	k1gInSc1	aerofon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
rákosových	rákosový	k2eAgFnPc2d1	rákosová
<g/>
,	,	kIx,	,
třtinových	třtinový	k2eAgFnPc2d1	třtinová
<g/>
,	,	kIx,	,
bambusových	bambusový	k2eAgFnPc2d1	bambusová
či	či	k8xC	či
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
píšťalek	píšťalka	k1gFnPc2	píšťalka
sestavených	sestavený	k2eAgInPc2d1	sestavený
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
řady	řada	k1gFnSc2	řada
<g/>
.	.	kIx.	.
</s>
<s>
Zvuky	zvuk	k1gInPc1	zvuk
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
nástroje	nástroj	k1gInSc2	nástroj
se	se	k3xPyFc4	se
vyluzují	vyluzovat	k5eAaImIp3nP	vyluzovat
pomocí	pomocí	k7c2	pomocí
foukání	foukání	k1gNnSc2	foukání
na	na	k7c4	na
okraje	okraj	k1gInPc4	okraj
otvorů	otvor	k1gInPc2	otvor
píšťalek	píšťalka	k1gFnPc2	píšťalka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
hrával	hrávat	k5eAaImAgMnS	hrávat
na	na	k7c4	na
flétnu	flétna	k1gFnSc4	flétna
bůh	bůh	k1gMnSc1	bůh
pastýřů	pastýř	k1gMnPc2	pastýř
a	a	k8xC	a
stád	stádo	k1gNnPc2	stádo
Pan	Pan	k1gMnSc1	Pan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panovy	Panův	k2eAgFnPc1d1	Panova
flétny	flétna	k1gFnPc1	flétna
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
z	z	k7c2	z
bambusu	bambus	k1gInSc2	bambus
nebo	nebo	k8xC	nebo
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
ani	ani	k8xC	ani
flétna	flétna	k1gFnSc1	flétna
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
kovu	kov	k1gInSc2	kov
nebo	nebo	k8xC	nebo
plastu	plast	k1gInSc2	plast
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
jihoamerické	jihoamerický	k2eAgFnSc6d1	jihoamerická
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dvě	dva	k4xCgFnPc4	dva
flétny	flétna	k1gFnPc4	flétna
vzájemně	vzájemně	k6eAd1	vzájemně
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
jedné	jeden	k4xCgFnSc2	jeden
melodie	melodie	k1gFnSc2	melodie
(	(	kIx(	(
<g/>
hoketus	hoketus	k1gInSc1	hoketus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Panova	Panův	k2eAgFnSc1d1	Panova
flétna	flétna	k1gFnSc1	flétna
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
původ	původ	k1gInSc1	původ
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
samých	samý	k3xTgInPc2	samý
počátků	počátek	k1gInPc2	počátek
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
byla	být	k5eAaImAgFnS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
rákosu	rákos	k1gInSc2	rákos
<g/>
,	,	kIx,	,
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
bambusu	bambus	k1gInSc2	bambus
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Panova	Panův	k2eAgFnSc1d1	Panova
<g/>
"	"	kIx"	"
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
z	z	k7c2	z
tamější	tamější	k2eAgFnSc2d1	tamější
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
Arkádii	Arkádie	k1gFnSc6	Arkádie
jistý	jistý	k2eAgMnSc1d1	jistý
bůh	bůh	k1gMnSc1	bůh
Pan	Pan	k1gMnSc1	Pan
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc1	ochránce
pastýřů	pastýř	k1gMnPc2	pastýř
a	a	k8xC	a
stád	stádo	k1gNnPc2	stádo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
krásné	krásný	k2eAgFnSc2d1	krásná
nymfy	nymfa	k1gFnSc2	nymfa
Syrinx	syrinx	k1gInSc1	syrinx
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Pan	Pan	k1gMnSc1	Pan
byl	být	k5eAaImAgMnS	být
napůl	napůl	k6eAd1	napůl
člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
kozel	kozel	k1gMnSc1	kozel
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
divu	diva	k1gFnSc4	diva
<g/>
,	,	kIx,	,
že	že	k8xS	že
sličná	sličný	k2eAgFnSc1d1	sličná
nymfa	nymfa	k1gFnSc1	nymfa
jeho	jeho	k3xOp3gFnSc4	jeho
lásku	láska	k1gFnSc4	láska
neopětovala	opětovat	k5eNaImAgFnS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Panův	Panův	k2eAgInSc1d1	Panův
zájem	zájem	k1gInSc1	zájem
stal	stát	k5eAaPmAgInS	stát
pro	pro	k7c4	pro
nymfu	nymfa	k1gFnSc4	nymfa
nesnesitelný	snesitelný	k2eNgMnSc1d1	nesnesitelný
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
před	před	k7c7	před
ním	on	k3xPp3gInSc7	on
prchat	prchat	k5eAaImF	prchat
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
stále	stále	k6eAd1	stále
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Syrinx	syrinx	k1gInSc4	syrinx
už	už	k6eAd1	už
neměla	mít	k5eNaImAgFnS	mít
kam	kam	k6eAd1	kam
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
žádost	žádost	k1gFnSc4	žádost
nechala	nechat	k5eAaPmAgFnS	nechat
od	od	k7c2	od
vládce	vládce	k1gMnSc2	vládce
všech	všecek	k3xTgMnPc2	všecek
bohů	bůh	k1gMnPc2	bůh
Dia	Dia	k1gFnSc4	Dia
proměnit	proměnit	k5eAaPmF	proměnit
v	v	k7c6	v
rákosí	rákosí	k1gNnSc6	rákosí
<g/>
.	.	kIx.	.
</s>
<s>
Panova	Panův	k2eAgFnSc1d1	Panova
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Syrinx	syrinx	k1gInSc4	syrinx
byla	být	k5eAaImAgFnS	být
však	však	k9	však
natolik	natolik	k6eAd1	natolik
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
z	z	k7c2	z
onoho	onen	k3xDgNnSc2	onen
rákosí	rákosí	k1gNnSc2	rákosí
udělal	udělat	k5eAaPmAgMnS	udělat
píšťalu	píšťala	k1gFnSc4	píšťala
a	a	k8xC	a
po	po	k7c6	po
večerech	večer	k1gInPc6	večer
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
vyhrával	vyhrávat	k5eAaImAgMnS	vyhrávat
smutné	smutný	k2eAgFnPc4d1	smutná
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
bambusu	bambus	k1gInSc2	bambus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
ze	z	k7c2	z
skla	sklo	k1gNnSc2	sklo
nebo	nebo	k8xC	nebo
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
chybou	chyba	k1gFnSc7	chyba
hovořit	hovořit	k5eAaImF	hovořit
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
souvislostech	souvislost	k1gFnPc6	souvislost
o	o	k7c6	o
píšťalách	píšťala	k1gFnPc6	píšťala
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
pouhé	pouhý	k2eAgFnPc4d1	pouhá
rourky	rourka	k1gFnPc4	rourka
(	(	kIx(	(
<g/>
rezonátory	rezonátor	k1gInPc4	rezonátor
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
podstatných	podstatný	k2eAgInPc2d1	podstatný
znaků	znak	k1gInPc2	znak
(	(	kIx(	(
<g/>
retné	retný	k2eAgFnPc4d1	retný
<g/>
)	)	kIx)	)
píšťaly	píšťala	k1gFnPc4	píšťala
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
beze	beze	k7c2	beze
rtů	ret	k1gInPc2	ret
a	a	k8xC	a
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
je	být	k5eAaImIp3nS	být
tu	ten	k3xDgFnSc4	ten
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
příčnou	příčný	k2eAgFnSc7d1	příčná
flétnou	flétna	k1gFnSc7	flétna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
Panovy	Panův	k2eAgFnSc2d1	Panova
flétny	flétna	k1gFnSc2	flétna
firma	firma	k1gFnSc1	firma
Panex	Panex	k1gInSc1	Panex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
