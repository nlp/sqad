<p>
<s>
Avram	Avram	k6eAd1	Avram
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1928	[number]	k4	1928
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
kognitivní	kognitivní	k2eAgMnSc1d1	kognitivní
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
lingvista	lingvista	k1gMnSc1	lingvista
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
<g/>
,	,	kIx,	,
společenský	společenský	k2eAgMnSc1d1	společenský
kritik	kritik	k1gMnSc1	kritik
a	a	k8xC	a
aktivista	aktivista	k1gMnSc1	aktivista
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc4	tvůrce
tzv.	tzv.	kA	tzv.
Chomského	Chomský	k2eAgMnSc4d1	Chomský
hierarchické	hierarchický	k2eAgNnSc1d1	hierarchické
klasifikace	klasifikace	k1gFnPc4	klasifikace
formálních	formální	k2eAgInPc2d1	formální
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
emeritní	emeritní	k2eAgMnSc1d1	emeritní
profesor	profesor	k1gMnSc1	profesor
lingvistiky	lingvistika	k1gFnSc2	lingvistika
na	na	k7c6	na
Massachusettském	massachusettský	k2eAgInSc6d1	massachusettský
technologickém	technologický	k2eAgInSc6d1	technologický
institutu	institut	k1gInSc6	institut
(	(	kIx(	(
<g/>
MIT	MIT	kA	MIT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
levicově	levicově	k6eAd1	levicově
orientovaný	orientovaný	k2eAgMnSc1d1	orientovaný
politický	politický	k2eAgMnSc1d1	politický
aktivista	aktivista	k1gMnSc1	aktivista
-	-	kIx~	-
anarchista	anarchista	k1gMnSc1	anarchista
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgInSc7	svůj
kritickým	kritický	k2eAgInSc7d1	kritický
vztahem	vztah	k1gInSc7	vztah
ke	k	k7c3	k
globalizaci	globalizace	k1gFnSc3	globalizace
a	a	k8xC	a
jejím	její	k3xOp3gInPc3	její
dopadům	dopad	k1gInPc3	dopad
<g/>
,	,	kIx,	,
k	k	k7c3	k
válečným	válečný	k2eAgInPc3d1	válečný
konfliktům	konflikt	k1gInPc3	konflikt
<g/>
,	,	kIx,	,
k	k	k7c3	k
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
politice	politika	k1gFnSc3	politika
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Izraele	Izrael	k1gInSc2	Izrael
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Top	topit	k5eAaImRp2nS	topit
global	globat	k5eAaImAgInS	globat
intellectuals	intellectuals	k1gInSc1	intellectuals
of	of	k?	of
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
internetového	internetový	k2eAgNnSc2d1	internetové
hlasování	hlasování	k1gNnSc2	hlasování
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
Prospect	Prospecta	k1gFnPc2	Prospecta
magazine	magazinout	k5eAaPmIp3nS	magazinout
a	a	k8xC	a
Foreign	Foreign	k1gMnSc1	Foreign
policy	polica	k1gFnSc2	polica
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nejvlivnějším	vlivný	k2eAgMnSc7d3	nejvlivnější
globálním	globální	k2eAgMnSc7d1	globální
intelektuálem	intelektuál	k1gMnSc7	intelektuál
<g/>
.	.	kIx.	.
<g/>
Chomsky	Chomsky	k1gMnSc1	Chomsky
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
teorie	teorie	k1gFnSc2	teorie
generativní	generativní	k2eAgFnPc1d1	generativní
gramatiky	gramatika	k1gFnPc1	gramatika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
některými	některý	k3yIgFnPc7	některý
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c4	za
největší	veliký	k2eAgInSc4d3	veliký
přínos	přínos	k1gInSc4	přínos
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
teoretické	teoretický	k2eAgFnSc2d1	teoretická
lingvistiky	lingvistika	k1gFnSc2	lingvistika
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
též	též	k9	též
filozofii	filozofie	k1gFnSc4	filozofie
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Herman	Herman	k1gMnSc1	Herman
<g/>
,	,	kIx,	,
Fodor	Fodor	k1gMnSc1	Fodor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
pomohl	pomoct	k5eAaPmAgInS	pomoct
podnítit	podnítit	k5eAaPmF	podnítit
kognitivní	kognitivní	k2eAgFnSc4d1	kognitivní
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
psychologii	psychologie	k1gFnSc6	psychologie
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
revizi	revize	k1gFnSc3	revize
Skinnerova	Skinnerův	k2eAgNnSc2d1	Skinnerovo
verbálního	verbální	k2eAgNnSc2d1	verbální
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgMnPc4	který
zpochybnil	zpochybnit	k5eAaPmAgInS	zpochybnit
behavioristický	behavioristický	k2eAgInSc1d1	behavioristický
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
převažující	převažující	k2eAgFnSc6d1	převažující
v	v	k7c4	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
naturalistický	naturalistický	k2eAgInSc1d1	naturalistický
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
jazyka	jazyk	k1gInSc2	jazyk
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
filozofii	filozofie	k1gFnSc4	filozofie
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
(	(	kIx(	(
<g/>
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
publikaci	publikace	k1gFnSc6	publikace
modelu	model	k1gInSc2	model
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
–	–	k?	–
klasifikaci	klasifikace	k1gFnSc3	klasifikace
formálních	formální	k2eAgInPc2d1	formální
jazyků	jazyk	k1gInPc2	jazyk
dle	dle	k7c2	dle
jejich	jejich	k3xOp3gFnSc2	jejich
generativní	generativní	k2eAgFnSc2d1	generativní
schopnosti	schopnost	k1gFnSc2	schopnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
Chomského	Chomský	k2eAgNnSc2d1	Chomský
je	být	k5eAaImIp3nS	být
vývoj	vývoj	k1gInSc1	vývoj
řeči	řeč	k1gFnSc2	řeč
vymezen	vymezit	k5eAaPmNgInS	vymezit
geneticky	geneticky	k6eAd1	geneticky
–	–	k?	–
došel	dojít	k5eAaPmAgInS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
jakási	jakýsi	k3yIgFnSc1	jakýsi
univerzální	univerzální	k2eAgFnSc1d1	univerzální
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
naučená	naučený	k2eAgFnSc1d1	naučená
během	během	k7c2	během
života	život	k1gInSc2	život
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
se	s	k7c7	s
základní	základní	k2eAgFnSc7d1	základní
gramatickou	gramatický	k2eAgFnSc7d1	gramatická
šablonou	šablona	k1gFnSc7	šablona
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
snadno	snadno	k6eAd1	snadno
zapadne	zapadnout	k5eAaPmIp3nS	zapadnout
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
světový	světový	k2eAgInSc4d1	světový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
teorie	teorie	k1gFnPc1	teorie
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
napadány	napadán	k2eAgFnPc1d1	napadána
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
postrádají	postrádat	k5eAaImIp3nP	postrádat
vědecké	vědecký	k2eAgInPc4d1	vědecký
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
Indexu	index	k1gInSc2	index
humanitních	humanitní	k2eAgFnPc2d1	humanitní
citací	citace	k1gFnPc2	citace
(	(	kIx(	(
<g/>
Arts	Arts	k1gInSc1	Arts
and	and	k?	and
Humanities	Humanities	k1gInSc1	Humanities
Citation	Citation	k1gInSc1	Citation
Index	index	k1gInSc4	index
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
Chomsky	Chomsky	k1gFnSc7	Chomsky
jako	jako	k9	jako
pramen	pramen	k1gInSc1	pramen
citován	citován	k2eAgInSc1d1	citován
častěji	často	k6eAd2	často
než	než	k8xS	než
jakýkoliv	jakýkoliv	k3yIgMnSc1	jakýkoliv
jiný	jiný	k2eAgMnSc1d1	jiný
žijící	žijící	k2eAgMnSc1d1	žijící
badatel	badatel	k1gMnSc1	badatel
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
osmý	osmý	k4xOgMnSc1	osmý
nejcitovanější	citovaný	k2eAgMnSc1d3	nejcitovanější
vědec	vědec	k1gMnSc1	vědec
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
kritiky	kritika	k1gFnSc2	kritika
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ještě	ještě	k6eAd1	ještě
známějším	známý	k2eAgInSc7d2	známější
–	–	k?	–
hlavně	hlavně	k6eAd1	hlavně
mezinárodně	mezinárodně	k6eAd1	mezinárodně
–	–	k?	–
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
mediální	mediální	k2eAgFnSc3d1	mediální
kritice	kritika	k1gFnSc3	kritika
a	a	k8xC	a
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
osobnost	osobnost	k1gFnSc4	osobnost
levého	levý	k2eAgNnSc2d1	levé
křídla	křídlo	k1gNnSc2	křídlo
politiky	politika	k1gFnSc2	politika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
znám	znám	k2eAgInSc1d1	znám
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
politickou	politický	k2eAgFnSc4d1	politická
aktivitu	aktivita	k1gFnSc4	aktivita
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
USA	USA	kA	USA
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
popsání	popsání	k1gNnSc4	popsání
modelu	model	k1gInSc2	model
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
East	East	k1gInSc4	East
Oak	Oak	k1gMnSc2	Oak
Lane	Lan	k1gMnSc2	Lan
poblíž	poblíž	k7c2	poblíž
Philadelphie	Philadelphia	k1gFnSc2	Philadelphia
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
hebrejského	hebrejský	k2eAgMnSc2d1	hebrejský
vědce	vědec	k1gMnSc2	vědec
a	a	k8xC	a
účastníka	účastník	k1gMnSc2	účastník
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
Williama	Williama	k1gFnSc1	Williama
Chomského	Chomský	k2eAgInSc2d1	Chomský
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
-	-	kIx~	-
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Elsie	Elsie	k1gFnSc1	Elsie
Chomsky	Chomsky	k1gFnSc1	Chomsky
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Simonofska	Simonofska	k1gFnSc1	Simonofska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
nynějšího	nynější	k2eAgNnSc2d1	nynější
Běloruska	Bělorusko	k1gNnSc2	Bělorusko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
mluvila	mluvit	k5eAaImAgFnS	mluvit
"	"	kIx"	"
<g/>
běžnou	běžný	k2eAgFnSc7d1	běžná
newyorskou	newyorský	k2eAgFnSc7d1	newyorská
angličtinou	angličtina	k1gFnSc7	angličtina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
mateřským	mateřský	k2eAgMnSc7d1	mateřský
jazykem	jazyk	k1gMnSc7	jazyk
byla	být	k5eAaImAgFnS	být
jidiš	jidiš	k6eAd1	jidiš
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jak	jak	k8xS	jak
Chomsky	Chomsky	k1gMnSc1	Chomsky
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
rodině	rodina	k1gFnSc6	rodina
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
jazykem	jazyk	k1gInSc7	jazyk
nehovořilo	hovořit	k5eNaImAgNnS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
jako	jako	k8xC	jako
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
jakémsi	jakýsi	k3yIgNnSc6	jakýsi
"	"	kIx"	"
<g/>
židovském	židovský	k2eAgNnSc6d1	Židovské
ghettu	ghetto	k1gNnSc6	ghetto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
rozděleném	rozdělený	k2eAgMnSc6d1	rozdělený
mezi	mezi	k7c4	mezi
jidiš	jidiš	k1gNnSc4	jidiš
a	a	k8xC	a
hebrejskou	hebrejský	k2eAgFnSc4d1	hebrejská
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
přikláněla	přiklánět	k5eAaImAgFnS	přiklánět
spíše	spíše	k9	spíše
k	k	k7c3	k
té	ten	k3xDgFnSc3	ten
hebrejské	hebrejský	k2eAgFnSc3d1	hebrejská
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
v	v	k7c6	v
hebrejské	hebrejský	k2eAgFnSc6d1	hebrejská
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
sám	sám	k3xTgMnSc1	sám
popisuje	popisovat	k5eAaImIp3nS	popisovat
napětí	napětí	k1gNnSc4	napětí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
sám	sám	k3xTgMnSc1	sám
zažil	zažít	k5eAaPmAgInS	zažít
s	s	k7c7	s
irskými	irský	k2eAgMnPc7d1	irský
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
antisemitisty	antisemitista	k1gMnPc7	antisemitista
v	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nerad	nerad	k2eAgMnSc1d1	nerad
to	ten	k3xDgNnSc4	ten
říkám	říkat	k5eAaImIp1nS	říkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
jsem	být	k5eAaImIp1nS	být
s	s	k7c7	s
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
vnitřním	vnitřní	k2eAgInSc7d1	vnitřní
strachem	strach	k1gInSc7	strach
z	z	k7c2	z
katolíků	katolík	k1gMnPc2	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Věděl	vědět	k5eAaImAgMnS	vědět
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
neracionální	racionální	k2eNgMnSc1d1	neracionální
a	a	k8xC	a
překonal	překonat	k5eAaPmAgMnS	překonat
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc4	ten
prostě	prostě	k9	prostě
zkušenost	zkušenost	k1gFnSc1	zkušenost
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
pamatuje	pamatovat	k5eAaImIp3nS	pamatovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
článek	článek	k1gInSc4	článek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
přibližně	přibližně	k6eAd1	přibližně
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
o	o	k7c6	o
hrozbě	hrozba	k1gFnSc6	hrozba
šíření	šíření	k1gNnSc2	šíření
fašismu	fašismus	k1gInSc2	fašismus
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Barcelony	Barcelona	k1gFnSc2	Barcelona
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dvanácti	dvanáct	k4xCc2	dvanáct
nebo	nebo	k8xC	nebo
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ztotožnil	ztotožnit	k5eAaPmAgInS	ztotožnit
ještě	ještě	k9	ještě
plněji	plně	k6eAd2	plně
s	s	k7c7	s
anarchistickou	anarchistický	k2eAgFnSc7d1	anarchistická
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
(	(	kIx(	(
<g/>
Central	Central	k1gFnSc1	Central
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
of	of	k?	of
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
filozofii	filozofie	k1gFnSc4	filozofie
a	a	k8xC	a
lingvistiku	lingvistika	k1gFnSc4	lingvistika
na	na	k7c6	na
Pensylvánské	pensylvánský	k2eAgFnSc6d1	Pensylvánská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
od	od	k7c2	od
filozofů	filozof	k1gMnPc2	filozof
C.	C.	kA	C.
W.	W.	kA	W.
Churchmana	Churchman	k1gMnSc4	Churchman
a	a	k8xC	a
N.	N.	kA	N.
Goodmana	Goodman	k1gMnSc2	Goodman
a	a	k8xC	a
lingvisty	lingvista	k1gMnSc2	lingvista
Z.	Z.	kA	Z.
Harrise	Harrise	k1gFnSc2	Harrise
<g/>
.	.	kIx.	.
</s>
<s>
Harrisovo	Harrisův	k2eAgNnSc1d1	Harrisovo
učení	učení	k1gNnSc1	učení
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
jeho	on	k3xPp3gInSc4	on
objev	objev	k1gInSc4	objev
transformací	transformace	k1gFnPc2	transformace
jako	jako	k8xS	jako
matematické	matematický	k2eAgFnSc2d1	matematická
analýzy	analýza	k1gFnSc2	analýza
struktury	struktura	k1gFnSc2	struktura
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
mapování	mapování	k1gNnSc1	mapování
od	od	k7c2	od
jedné	jeden	k4xCgFnSc2	jeden
podmnožiny	podmnožina	k1gFnSc2	podmnožina
k	k	k7c3	k
jiné	jiná	k1gFnSc3	jiná
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
vět	věta	k1gFnPc2	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
následně	následně	k6eAd1	následně
vyložil	vyložit	k5eAaPmAgMnS	vyložit
toto	tento	k3xDgNnSc4	tento
učení	učení	k1gNnSc4	učení
jako	jako	k8xC	jako
operace	operace	k1gFnPc4	operace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
bezkontextové	bezkontextový	k2eAgFnSc2d1	bezkontextová
gramatiky	gramatika	k1gFnSc2	gramatika
(	(	kIx(	(
<g/>
odvozené	odvozený	k2eAgInPc1d1	odvozený
od	od	k7c2	od
post-produkčních	postrodukční	k2eAgInPc2d1	post-produkční
systémů	systém	k1gInPc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Harrisovy	Harrisův	k2eAgInPc1d1	Harrisův
politické	politický	k2eAgInPc1d1	politický
názory	názor	k1gInPc1	názor
rovněž	rovněž	k9	rovněž
přispěly	přispět	k5eAaPmAgInP	přispět
k	k	k7c3	k
utváření	utváření	k1gNnSc3	utváření
Chomského	Chomský	k2eAgInSc2d1	Chomský
názorů	názor	k1gInPc2	názor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Chomsky	Chomsky	k1gMnSc1	Chomsky
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
lingvistkou	lingvistka	k1gFnSc7	lingvistka
Carol	Carol	k1gInSc1	Carol
Schatzovou	Schatzová	k1gFnSc4	Schatzová
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
:	:	kIx,	:
Avivu	Aviv	k1gInSc2	Aviv
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Diane	Dian	k1gMnSc5	Dian
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
syna	syn	k1gMnSc2	syn
Harryho	Harry	k1gMnSc2	Harry
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chomskému	Chomský	k2eAgMnSc3d1	Chomský
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc4d1	udělen
titul	titul	k1gInSc4	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
z	z	k7c2	z
lingvistiky	lingvistika	k1gFnSc2	lingvistika
na	na	k7c6	na
Pensylvánské	pensylvánský	k2eAgFnSc6d1	Pensylvánská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
svého	svůj	k3xOyFgInSc2	svůj
doktorského	doktorský	k2eAgInSc2d1	doktorský
výzkumu	výzkum	k1gInSc2	výzkum
vedl	vést	k5eAaImAgInS	vést
během	během	k7c2	během
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
disertační	disertační	k2eAgFnSc6d1	disertační
práci	práce	k1gFnSc6	práce
začal	začít	k5eAaPmAgInS	začít
tvořit	tvořit	k5eAaImF	tvořit
některé	některý	k3yIgFnPc4	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
lingvistických	lingvistický	k2eAgFnPc2d1	lingvistická
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Syntaktické	syntaktický	k2eAgFnSc2d1	syntaktická
struktury	struktura	k1gFnSc2	struktura
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Syntactic	Syntactice	k1gFnPc2	Syntactice
Structures	Structuresa	k1gFnPc2	Structuresa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
nejznámější	známý	k2eAgFnSc1d3	nejznámější
lingvistickou	lingvistický	k2eAgFnSc7d1	lingvistická
knihou	kniha	k1gFnSc7	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mladý	mladý	k2eAgMnSc1d1	mladý
Chomsky	Chomsky	k1gMnSc1	Chomsky
==	==	k?	==
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
na	na	k7c6	na
Massachusettském	massachusettský	k2eAgInSc6d1	massachusettský
technologickém	technologický	k2eAgInSc6d1	technologický
institutu	institut	k1gInSc6	institut
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
řádným	řádný	k2eAgMnSc7d1	řádný
profesorem	profesor	k1gMnSc7	profesor
Katedry	katedra	k1gFnSc2	katedra
moderních	moderní	k2eAgInPc2d1	moderní
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
lingvistiky	lingvistika	k1gFnSc2	lingvistika
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Katedra	katedra	k1gFnSc1	katedra
lingvistiky	lingvistika	k1gFnSc2	lingvistika
a	a	k8xC	a
filozofie	filozofie	k1gFnSc2	filozofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgMnS	být
držitelem	držitel	k1gMnSc7	držitel
profesury	profesura	k1gFnSc2	profesura
Ferrari	Ferrari	k1gMnSc1	Ferrari
P.	P.	kA	P.
Ward	Ward	k1gMnSc1	Ward
Professorship	Professorship	k1gMnSc1	Professorship
z	z	k7c2	z
moderních	moderní	k2eAgInPc2d1	moderní
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
institutu	institut	k1gInSc2	institut
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
Chomsky	Chomsky	k1gFnSc1	Chomsky
učil	učit	k5eAaImAgInS	učit
na	na	k7c6	na
MIT	MIT	kA	MIT
souvisle	souvisle	k6eAd1	souvisle
52	[number]	k4	52
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
odpůrců	odpůrce	k1gMnPc2	odpůrce
Vietnamské	vietnamský	k2eAgFnSc2d1	vietnamská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
protiválečné	protiválečný	k2eAgInPc1d1	protiválečný
pocity	pocit	k1gInPc1	pocit
byly	být	k5eAaImAgInP	být
ještě	ještě	k9	ještě
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vydání	vydání	k1gNnSc3	vydání
své	svůj	k3xOyFgFnSc2	svůj
eseje	esej	k1gFnSc2	esej
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Responsibility	Responsibilita	k1gFnSc2	Responsibilita
of	of	k?	of
Intellectuals	Intellectuals	k1gInSc1	Intellectuals
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
)	)	kIx)	)
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
magazínu	magazín	k1gInSc6	magazín
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
Review	Review	k1gMnSc1	Review
of	of	k?	of
Books	Books	k1gInSc1	Books
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
American	American	k1gMnSc1	American
Power	Power	k1gMnSc1	Power
and	and	k?	and
the	the	k?	the
New	New	k1gFnSc2	New
Mandarins	Mandarinsa	k1gFnPc2	Mandarinsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
sbírkou	sbírka	k1gFnSc7	sbírka
esejí	esej	k1gFnPc2	esej
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
by	by	kYmCp3nP	by
pomohl	pomoct	k5eAaPmAgInS	pomoct
jistý	jistý	k2eAgInSc4d1	jistý
druh	druh	k1gInSc4	druh
denacifikace	denacifikace	k1gFnSc2	denacifikace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Staly	stát	k5eAaPmAgInP	stát
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
"	"	kIx"	"
<g/>
nejagresivnější	agresivní	k2eAgFnSc7d3	nejagresivnější
mocností	mocnost	k1gFnSc7	mocnost
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
největším	veliký	k2eAgNnSc7d3	veliký
ohrožením	ohrožení	k1gNnSc7	ohrožení
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
národního	národní	k2eAgNnSc2d1	národní
sebeurčení	sebeurčení	k1gNnSc2	sebeurčení
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
případě	případ	k1gInSc6	případ
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
protiválečné	protiválečný	k2eAgFnSc6d1	protiválečná
demonstraci	demonstrace	k1gFnSc6	demonstrace
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
zatčen	zatčen	k2eAgInSc4d1	zatčen
policií	policie	k1gFnSc7	policie
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
držen	držen	k2eAgMnSc1d1	držen
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dalekosáhlá	dalekosáhlý	k2eAgFnSc1d1	dalekosáhlá
kritika	kritika	k1gFnSc1	kritika
americké	americký	k2eAgFnSc2d1	americká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
oprávněnosti	oprávněnost	k1gFnSc2	oprávněnost
moci	moc	k1gFnSc2	moc
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
udělala	udělat	k5eAaPmAgFnS	udělat
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgNnPc1d1	tradiční
americká	americký	k2eAgNnPc1d1	americké
media	medium	k1gNnPc1	medium
velice	velice	k6eAd1	velice
stranila	stranit	k5eAaImAgNnP	stranit
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
své	svůj	k3xOyFgFnPc4	svůj
obavy	obava	k1gFnPc4	obava
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc4	jaký
příští	příští	k2eAgInSc4d1	příští
režim	režim	k1gInSc4	režim
může	moct	k5eAaImIp3nS	moct
uchvátit	uchvátit	k5eAaPmF	uchvátit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
–	–	k?	–
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
kritiku	kritika	k1gFnSc4	kritika
americké	americký	k2eAgFnSc2d1	americká
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
bylo	být	k5eAaImAgNnS	být
Chomskému	Chomský	k2eAgInSc3d1	Chomský
často	často	k6eAd1	často
vyhrožováno	vyhrožován	k2eAgNnSc4d1	vyhrožováno
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
cílů	cíl	k1gInPc2	cíl
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
Theodorem	Theodor	k1gMnSc7	Theodor
Kaczynskim	Kaczynskim	k1gInSc4	Kaczynskim
<g/>
,	,	kIx,	,
známým	známý	k2eAgInPc3d1	známý
též	též	k9	též
jako	jako	k9	jako
Unabomber	Unabomber	k1gMnSc1	Unabomber
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Kaczynski	Kaczynsk	k1gFnSc2	Kaczynsk
byl	být	k5eAaImAgInS	být
hrozbou	hrozba	k1gFnSc7	hrozba
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
Chomsky	Chomsky	k1gFnSc4	Chomsky
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
veškerou	veškerý	k3xTgFnSc4	veškerý
poštu	pošta	k1gFnSc4	pošta
na	na	k7c4	na
výbušniny	výbušnina	k1gFnPc4	výbušnina
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
tajně	tajně	k6eAd1	tajně
střežen	stříct	k5eAaPmNgInS	stříct
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
univerzitní	univerzitní	k2eAgFnSc6d1	univerzitní
půdě	půda	k1gFnSc6	půda
MIT	MIT	kA	MIT
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
s	s	k7c7	s
policejní	policejní	k2eAgFnSc7d1	policejní
ochranou	ochrana	k1gFnSc7	ochrana
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Lexingtonu	Lexington	k1gInSc6	Lexington
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
cestuje	cestovat	k5eAaImIp3nS	cestovat
a	a	k8xC	a
přednáší	přednášet	k5eAaImIp3nS	přednášet
o	o	k7c6	o
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přínos	přínos	k1gInSc1	přínos
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
lingvistiky	lingvistika	k1gFnSc2	lingvistika
==	==	k?	==
</s>
</p>
<p>
<s>
Syntaktické	syntaktický	k2eAgFnPc1d1	syntaktická
struktury	struktura	k1gFnPc1	struktura
byly	být	k5eAaImAgFnP	být
výtahem	výtah	k1gInSc7	výtah
jeho	jeho	k3xOp3gFnSc2	jeho
knihy	kniha	k1gFnSc2	kniha
Logická	logický	k2eAgFnSc1d1	logická
struktura	struktura	k1gFnSc1	struktura
teorie	teorie	k1gFnSc2	teorie
lingvistiky	lingvistika	k1gFnSc2	lingvistika
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Logical	Logicat	k5eAaPmAgMnS	Logicat
Structure	Structur	k1gMnSc5	Structur
of	of	k?	of
Linguistic	Linguistice	k1gFnPc2	Linguistice
Theory	Theora	k1gFnSc2	Theora
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
představuje	představovat	k5eAaImIp3nS	představovat
transformační	transformační	k2eAgFnSc4d1	transformační
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
mají	mít	k5eAaImIp3nP	mít
výroky	výrok	k1gInPc1	výrok
(	(	kIx(	(
<g/>
sekvence	sekvence	k1gFnSc1	sekvence
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
nějakou	nějaký	k3yIgFnSc4	nějaký
syntax	syntax	k1gFnSc4	syntax
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
popsána	popsat	k5eAaPmNgFnS	popsat
formální	formální	k2eAgFnSc7d1	formální
gramatikou	gramatika	k1gFnSc7	gramatika
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
bezkontextovou	bezkontextový	k2eAgFnSc4d1	bezkontextová
gramatiku	gramatika	k1gFnSc4	gramatika
rozšířenou	rozšířený	k2eAgFnSc4d1	rozšířená
o	o	k7c4	o
transformační	transformační	k2eAgFnPc4d1	transformační
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
znalost	znalost	k1gFnSc4	znalost
základní	základní	k2eAgFnSc2d1	základní
gramatické	gramatický	k2eAgFnSc2d1	gramatická
struktury	struktura	k1gFnSc2	struktura
společnou	společný	k2eAgFnSc4d1	společná
všem	všecek	k3xTgMnPc3	všecek
lidským	lidský	k2eAgMnPc3d1	lidský
jazykům	jazyk	k1gMnPc3	jazyk
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
setkají	setkat	k5eAaPmIp3nP	setkat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
určitého	určitý	k2eAgInSc2d1	určitý
omezeného	omezený	k2eAgInSc2d1	omezený
druhu	druh	k1gInSc2	druh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrozená	vrozený	k2eAgFnSc1d1	vrozená
znalost	znalost	k1gFnSc1	znalost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
univerzální	univerzální	k2eAgFnSc1d1	univerzální
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoření	tvoření	k1gNnSc1	tvoření
na	na	k7c6	na
základě	základ	k1gInSc6	základ
znalosti	znalost	k1gFnSc2	znalost
jazyka	jazyk	k1gInSc2	jazyk
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
formální	formální	k2eAgFnSc2d1	formální
gramatiky	gramatika	k1gFnSc2	gramatika
je	být	k5eAaImIp3nS	být
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
"	"	kIx"	"
<g/>
produktivitu	produktivita	k1gFnSc4	produktivita
<g/>
"	"	kIx"	"
jazyka	jazyk	k1gInSc2	jazyk
<g/>
:	:	kIx,	:
s	s	k7c7	s
omezenou	omezený	k2eAgFnSc7d1	omezená
množinou	množina	k1gFnSc7	množina
gramatických	gramatický	k2eAgNnPc2d1	gramatické
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
konečnou	konečný	k2eAgFnSc7d1	konečná
množinou	množina	k1gFnSc7	množina
pojmů	pojem	k1gInPc2	pojem
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
schopni	schopen	k2eAgMnPc1d1	schopen
utvořit	utvořit	k5eAaPmF	utvořit
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
množství	množství	k1gNnSc4	množství
vět	věta	k1gFnPc2	věta
včetně	včetně	k7c2	včetně
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
předtím	předtím	k6eAd1	předtím
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
vždy	vždy	k6eAd1	vždy
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
závazek	závazek	k1gInSc4	závazek
Páninimu	Páninim	k1gInSc2	Páninim
za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
moderní	moderní	k2eAgInSc4d1	moderní
náhled	náhled	k1gInSc4	náhled
na	na	k7c4	na
explicitní	explicitní	k2eAgFnSc4d1	explicitní
generativní	generativní	k2eAgFnSc4d1	generativní
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přístup	přístup	k1gInSc1	přístup
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
&	&	k?	&
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozvedený	rozvedený	k2eAgInSc1d1	rozvedený
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
Pisa	Pisa	k1gFnSc1	Pisa
1979	[number]	k4	1979
Lectures	Lecturesa	k1gFnPc2	Lecturesa
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
publikované	publikovaný	k2eAgNnSc4d1	publikované
jako	jako	k9	jako
Lecutres	Lecutres	k1gInSc4	Lecutres
on	on	k3xPp3gMnSc1	on
Government	Government	k1gMnSc1	Government
and	and	k?	and
Binding	Binding	k1gInSc1	Binding
(	(	kIx(	(
<g/>
LGB	LGB	kA	LGB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
o	o	k7c6	o
univerzální	univerzální	k2eAgFnSc6d1	univerzální
gramatice	gramatika	k1gFnSc6	gramatika
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
gramatické	gramatický	k2eAgInPc1d1	gramatický
principy	princip	k1gInPc1	princip
společné	společný	k2eAgFnSc2d1	společná
všem	všecek	k3xTgMnPc3	všecek
jazykům	jazyk	k1gMnPc3	jazyk
jsou	být	k5eAaImIp3nP	být
vrozené	vrozený	k2eAgInPc1d1	vrozený
a	a	k8xC	a
neměnné	neměnný	k2eAgInPc1d1	neměnný
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgInPc7d1	světový
jazyky	jazyk	k1gInPc7	jazyk
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
podle	podle	k7c2	podle
nastavení	nastavení	k1gNnSc2	nastavení
parametrů	parametr	k1gInPc2	parametr
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pro-drop	prorop	k1gInSc1	pro-drop
parametr	parametr	k1gInSc1	parametr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
explicitní	explicitní	k2eAgInSc1d1	explicitní
podmět	podmět	k1gInSc1	podmět
vždy	vždy	k6eAd1	vždy
vyžadován	vyžadován	k2eAgInSc1d1	vyžadován
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zda	zda	k8xS	zda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vypuštěn	vypuštěn	k2eAgMnSc1d1	vypuštěn
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
přirovnávány	přirovnáván	k2eAgFnPc1d1	přirovnávána
k	k	k7c3	k
přepínačům	přepínač	k1gInPc3	přepínač
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
pravidla	pravidlo	k1gNnPc1	pravidlo
a	a	k8xC	a
parametry	parametr	k1gInPc1	parametr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
principles	principles	k1gInSc1	principles
and	and	k?	and
parameters	parameters	k1gInSc1	parameters
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
pojetí	pojetí	k1gNnSc6	pojetí
si	se	k3xPyFc3	se
dítě	dítě	k1gNnSc1	dítě
učící	učící	k2eAgMnSc1d1	učící
se	se	k3xPyFc4	se
jazyk	jazyk	k1gInSc1	jazyk
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
osvojit	osvojit	k5eAaPmF	osvojit
pouze	pouze	k6eAd1	pouze
lexikální	lexikální	k2eAgFnPc1d1	lexikální
položky	položka	k1gFnPc1	položka
(	(	kIx(	(
<g/>
slova	slovo	k1gNnPc1	slovo
<g/>
,	,	kIx,	,
gramatické	gramatický	k2eAgInPc1d1	gramatický
morfémy	morfém	k1gInPc1	morfém
a	a	k8xC	a
idiomy	idiom	k1gInPc1	idiom
<g/>
)	)	kIx)	)
a	a	k8xC	a
určit	určit	k5eAaPmF	určit
vhodné	vhodný	k2eAgNnSc4d1	vhodné
nastavení	nastavení	k1gNnSc4	nastavení
parametrů	parametr	k1gInPc2	parametr
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
na	na	k7c6	na
základě	základ	k1gInSc6	základ
několika	několik	k4yIc2	několik
příkladů	příklad	k1gInPc2	příklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tempo	tempo	k1gNnSc1	tempo
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
děti	dítě	k1gFnPc1	dítě
učí	učit	k5eAaImIp3nP	učit
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
je	on	k3xPp3gInPc4	on
nevysvětlitelně	vysvětlitelně	k6eNd1	vysvětlitelně
rychlé	rychlý	k2eAgInPc4d1	rychlý
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
možné	možný	k2eAgNnSc1d1	možné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
děti	dítě	k1gFnPc1	dítě
měly	mít	k5eAaImAgFnP	mít
vrozenou	vrozený	k2eAgFnSc4d1	vrozená
schopnost	schopnost	k1gFnSc4	schopnost
učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
tezi	teze	k1gFnSc4	teze
hovoří	hovořit	k5eAaImIp3nS	hovořit
i	i	k9	i
následující	následující	k2eAgNnPc4d1	následující
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
učí	učit	k5eAaImIp3nS	učit
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
stejnými	stejná	k1gFnPc7	stejná
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
učení	učení	k1gNnSc2	učení
dělají	dělat	k5eAaImIp3nP	dělat
stejně	stejně	k6eAd1	stejně
charakteristické	charakteristický	k2eAgFnPc4d1	charakteristická
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
jiné	jiný	k2eAgInPc1d1	jiný
zdánlivě	zdánlivě	k6eAd1	zdánlivě
logické	logický	k2eAgInPc1d1	logický
druhy	druh	k1gInPc1	druh
chyb	chyba	k1gFnPc2	chyba
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
během	během	k7c2	během
učení	učení	k1gNnSc2	učení
neobjeví	objevit	k5eNaPmIp3nP	objevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
Minimalistickém	minimalistický	k2eAgInSc6d1	minimalistický
programu	program	k1gInSc6	program
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
ponechal	ponechat	k5eAaPmAgInS	ponechat
jádro	jádro	k1gNnSc4	jádro
konceptu	koncept	k1gInSc2	koncept
"	"	kIx"	"
<g/>
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
parametrů	parametr	k1gInPc2	parametr
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Chomsky	Chomsky	k1gFnSc1	Chomsky
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
o	o	k7c4	o
významnější	významný	k2eAgNnPc4d2	významnější
přepracování	přepracování	k1gNnPc4	přepracování
lingvistického	lingvistický	k2eAgInSc2d1	lingvistický
aparátu	aparát	k1gInSc2	aparát
v	v	k7c6	v
LGB	LGB	kA	LGB
modelu	model	k1gInSc6	model
oddělením	oddělení	k1gNnSc7	oddělení
všech	všecek	k3xTgInPc2	všecek
jeho	jeho	k3xOp3gInPc2	jeho
prvků	prvek	k1gInPc2	prvek
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těch	ten	k3xDgMnPc2	ten
nejnutnějších	nutný	k2eAgMnPc2d3	nejnutnější
<g/>
.	.	kIx.	.
</s>
<s>
Zastává	zastávat	k5eAaImIp3nS	zastávat
zde	zde	k6eAd1	zde
obecný	obecný	k2eAgInSc4d1	obecný
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
lidské	lidský	k2eAgFnSc2d1	lidská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
vlohy	vloha	k1gFnSc2	vloha
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
princip	princip	k1gInSc4	princip
hospodárnosti	hospodárnost	k1gFnSc2	hospodárnost
a	a	k8xC	a
optimálního	optimální	k2eAgInSc2d1	optimální
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
k	k	k7c3	k
derivačnímu	derivační	k2eAgInSc3d1	derivační
přístupu	přístup	k1gInSc3	přístup
ke	k	k7c3	k
generaci	generace	k1gFnSc3	generace
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
velkou	velký	k2eAgFnSc7d1	velká
měrou	míra	k1gFnSc7wR	míra
reprezentativního	reprezentativní	k2eAgInSc2d1	reprezentativní
přístupu	přístup	k1gInSc2	přístup
klasických	klasický	k2eAgInPc2d1	klasický
"	"	kIx"	"
<g/>
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
parametrů	parametr	k1gInPc2	parametr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
myšlenky	myšlenka	k1gFnPc1	myšlenka
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
badatele	badatel	k1gMnPc4	badatel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zkoumání	zkoumání	k1gNnSc2	zkoumání
osvojování	osvojování	k1gNnSc2	osvojování
si	se	k3xPyFc3	se
jazyka	jazyk	k1gInSc2	jazyk
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dnes	dnes	k6eAd1	dnes
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
nepodporují	podporovat	k5eNaImIp3nP	podporovat
Chomského	Chomský	k2eAgMnSc4d1	Chomský
teorie	teorie	k1gFnPc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
redukují	redukovat	k5eAaBmIp3nP	redukovat
jazyk	jazyk	k1gInSc4	jazyk
na	na	k7c4	na
instanci	instance	k1gFnSc4	instance
obecných	obecný	k2eAgInPc2d1	obecný
zpracovávajících	zpracovávající	k2eAgInPc2d1	zpracovávající
mechanismů	mechanismus	k1gInPc2	mechanismus
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
dále	daleko	k6eAd2	daleko
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
neomezené	omezený	k2eNgNnSc1d1	neomezené
rozšíření	rozšíření	k1gNnSc1	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
jako	jako	k8xS	jako
je	on	k3xPp3gNnSc4	on
angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jedině	jedině	k6eAd1	jedině
rekurzivní	rekurzivní	k2eAgFnSc7d1	rekurzivní
metodou	metoda	k1gFnSc7	metoda
vkládání	vkládání	k1gNnSc2	vkládání
vět	věta	k1gFnPc2	věta
do	do	k7c2	do
vět	věta	k1gFnPc2	věta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Generativní	generativní	k2eAgFnSc1d1	generativní
gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
analýzy	analýza	k1gFnPc1	analýza
syntaxe	syntax	k1gFnPc1	syntax
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
generativní	generativní	k2eAgFnSc1d1	generativní
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vysoce	vysoce	k6eAd1	vysoce
abstraktní	abstraktní	k2eAgFnPc1d1	abstraktní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
bedlivém	bedlivý	k2eAgInSc6d1	bedlivý
průzkumu	průzkum	k1gInSc6	průzkum
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
gramatickými	gramatický	k2eAgFnPc7d1	gramatická
a	a	k8xC	a
negramatickými	gramatický	k2eNgFnPc7d1	gramatický
konstrukcemi	konstrukce	k1gFnPc7	konstrukce
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc4	takový
gramatické	gramatický	k2eAgInPc4d1	gramatický
úsudky	úsudek	k1gInPc4	úsudek
o	o	k7c6	o
jazyku	jazyk	k1gInSc6	jazyk
může	moct	k5eAaImIp3nS	moct
úspěšně	úspěšně	k6eAd1	úspěšně
provést	provést	k5eAaPmF	provést
pouze	pouze	k6eAd1	pouze
rodilý	rodilý	k2eAgMnSc1d1	rodilý
mluvčí	mluvčí	k1gMnSc1	mluvčí
a	a	k8xC	a
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
pragmatických	pragmatický	k2eAgInPc2d1	pragmatický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
lingvisté	lingvista	k1gMnPc1	lingvista
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
mateřské	mateřský	k2eAgInPc4d1	mateřský
jazyky	jazyk	k1gInPc4	jazyk
nebo	nebo	k8xC	nebo
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dokonale	dokonale	k6eAd1	dokonale
ovládají	ovládat	k5eAaImIp3nP	ovládat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dostáváme	dostávat	k5eAaImIp1nP	dostávat
k	k	k7c3	k
angličtině	angličtina	k1gFnSc3	angličtina
<g/>
,	,	kIx,	,
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
nizozemštině	nizozemština	k1gFnSc6	nizozemština
<g/>
,	,	kIx,	,
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
japonštině	japonština	k1gFnSc3	japonština
nebo	nebo	k8xC	nebo
k	k	k7c3	k
jazykům	jazyk	k1gMnPc3	jazyk
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nP	hovořit
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
syntaxi	syntax	k1gFnSc3	syntax
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
generativní	generativní	k2eAgFnSc1d1	generativní
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
studuje	studovat	k5eAaImIp3nS	studovat
gramatiku	gramatika	k1gFnSc4	gramatika
jako	jako	k8xC	jako
podstatu	podstata	k1gFnSc4	podstata
znalosti	znalost	k1gFnSc2	znalost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mají	mít	k5eAaImIp3nP	mít
uživatelé	uživatel	k1gMnPc1	uživatel
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
Chomsky	Chomsky	k1gFnSc2	Chomsky
trvá	trvat	k5eAaImIp3nS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
této	tento	k3xDgFnSc2	tento
znalosti	znalost	k1gFnSc2	znalost
je	být	k5eAaImIp3nS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
děti	dítě	k1gFnPc1	dítě
se	se	k3xPyFc4	se
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
naučit	naučit	k5eAaPmF	naučit
jen	jen	k9	jen
omezené	omezený	k2eAgFnPc4d1	omezená
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jejich	jejich	k3xOp3gInSc2	jejich
mateřského	mateřský	k2eAgInSc2d1	mateřský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vrozená	vrozený	k2eAgFnSc1d1	vrozená
podstata	podstata	k1gFnSc1	podstata
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
znalosti	znalost	k1gFnSc2	znalost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
univerzální	univerzální	k2eAgFnSc1d1	univerzální
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Chomského	Chomský	k2eAgInSc2d1	Chomský
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
nejsilnějším	silný	k2eAgInSc7d3	nejsilnější
důkazem	důkaz	k1gInSc7	důkaz
existence	existence	k1gFnSc2	existence
univerzální	univerzální	k2eAgFnSc2d1	univerzální
gramatiky	gramatika	k1gFnSc2	gramatika
jednoduše	jednoduše	k6eAd1	jednoduše
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
děti	dítě	k1gFnPc4	dítě
úspěšně	úspěšně	k6eAd1	úspěšně
osvojí	osvojit	k5eAaPmIp3nP	osvojit
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
jazyk	jazyk	k1gInSc4	jazyk
za	za	k7c4	za
tak	tak	k6eAd1	tak
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lingvistická	lingvistický	k2eAgNnPc4d1	lingvistické
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgInPc3	který
mají	mít	k5eAaImIp3nP	mít
děti	dítě	k1gFnPc4	dítě
přístup	přístup	k1gInSc4	přístup
<g/>
,	,	kIx,	,
vůbec	vůbec	k9	vůbec
nedokládají	dokládat	k5eNaImIp3nP	dokládat
bohatou	bohatý	k2eAgFnSc4d1	bohatá
lingvistickou	lingvistický	k2eAgFnSc4d1	lingvistická
znalost	znalost	k1gFnSc4	znalost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
(	(	kIx(	(
<g/>
argument	argument	k1gInSc1	argument
nedostatku	nedostatek	k1gInSc2	nedostatek
podnětů	podnět	k1gInPc2	podnět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
teorie	teorie	k1gFnPc1	teorie
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
populární	populární	k2eAgFnPc1d1	populární
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
nebyly	být	k5eNaImAgInP	být
oproštěny	oprostit	k5eAaPmNgInP	oprostit
od	od	k7c2	od
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
přichází	přicházet	k5eAaImIp3nS	přicházet
z	z	k7c2	z
několika	několik	k4yIc2	několik
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Lingvisté	lingvista	k1gMnPc1	lingvista
dle	dle	k7c2	dle
Chomského	Chomský	k2eAgMnSc2d1	Chomský
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
intuici	intuice	k1gFnSc4	intuice
rodilých	rodilý	k2eAgMnPc2d1	rodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
při	při	k7c6	při
posuzování	posuzování	k1gNnSc6	posuzování
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
věty	věta	k1gFnPc1	věta
jejich	jejich	k3xOp3gInSc2	jejich
jazyka	jazyk	k1gInSc2	jazyk
jsou	být	k5eAaImIp3nP	být
správně	správně	k6eAd1	správně
utvořené	utvořený	k2eAgFnPc1d1	utvořená
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
jednak	jednak	k8xC	jednak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
obecných	obecný	k2eAgNnPc2d1	obecné
metodických	metodický	k2eAgNnPc2d1	metodické
východisek	východisko	k1gNnPc2	východisko
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
přehnanému	přehnaný	k2eAgInSc3d1	přehnaný
důrazu	důraz	k1gInSc3	důraz
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
získaly	získat	k5eAaPmAgInP	získat
alespoň	alespoň	k9	alespoň
trochu	trochu	k6eAd1	trochu
pozornosti	pozornost	k1gFnSc2	pozornost
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
o	o	k7c6	o
generativní	generativní	k2eAgFnSc6d1	generativní
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někteří	některý	k3yIgMnPc1	některý
kritici	kritik	k1gMnPc1	kritik
stejně	stejně	k6eAd1	stejně
vnímají	vnímat	k5eAaImIp3nP	vnímat
přehnaný	přehnaný	k2eAgInSc4d1	přehnaný
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
angličtinu	angličtina	k1gFnSc4	angličtina
a	a	k8xC	a
tendenci	tendence	k1gFnSc4	tendence
založit	založit	k5eAaPmF	založit
tvrzení	tvrzení	k1gNnSc4	tvrzení
o	o	k7c6	o
univerzální	univerzální	k2eAgFnSc6d1	univerzální
gramatice	gramatika	k1gFnSc6	gramatika
na	na	k7c6	na
příliš	příliš	k6eAd1	příliš
malém	malý	k2eAgInSc6d1	malý
vzorku	vzorek	k1gInSc6	vzorek
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
psychologové	psycholog	k1gMnPc1	psycholog
a	a	k8xC	a
psycholingvisté	psycholingvista	k1gMnPc1	psycholingvista
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
schvalují	schvalovat	k5eAaImIp3nP	schvalovat
Chomského	Chomský	k2eAgMnSc2d1	Chomský
celkový	celkový	k2eAgInSc4d1	celkový
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lingvistika	lingvistika	k1gFnSc1	lingvistika
dle	dle	k7c2	dle
Chomského	Chomský	k2eAgNnSc2d1	Chomský
nevěnuje	věnovat	k5eNaImIp3nS	věnovat
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
pozornost	pozornost	k1gFnSc4	pozornost
experimentálním	experimentální	k2eAgNnPc3d1	experimentální
datům	datum	k1gNnPc3	datum
ze	z	k7c2	z
zpracování	zpracování	k1gNnSc2	zpracování
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
následně	následně	k6eAd1	následně
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
teorie	teorie	k1gFnPc1	teorie
nejsou	být	k5eNaImIp3nP	být
psychologicky	psychologicky	k6eAd1	psychologicky
přijatelné	přijatelný	k2eAgFnPc1d1	přijatelná
<g/>
.	.	kIx.	.
</s>
<s>
Radikálnější	radikální	k2eAgMnPc1d2	radikálnější
kritici	kritik	k1gMnPc1	kritik
pochybují	pochybovat	k5eAaImIp3nP	pochybovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnSc4	vysvětlení
učení	učení	k1gNnSc2	učení
se	se	k3xPyFc4	se
dětí	dítě	k1gFnPc2	dítě
jazyku	jazyk	k1gInSc3	jazyk
nutná	nutný	k2eAgFnSc1d1	nutná
univerzální	univerzální	k2eAgFnSc1d1	univerzální
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
domain-general	domainenerat	k5eAaImAgInS	domain-generat
učící	učící	k2eAgInPc4d1	učící
mechanismy	mechanismus	k1gInPc4	mechanismus
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgMnSc1d1	kulturní
antropolog	antropolog	k1gMnSc1	antropolog
a	a	k8xC	a
lingvista	lingvista	k1gMnSc1	lingvista
Daniel	Daniel	k1gMnSc1	Daniel
Everett	Everett	k1gMnSc1	Everett
z	z	k7c2	z
Illinois	Illinois	k1gFnSc2	Illinois
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
lidí	člověk	k1gMnPc2	člověk
Piraha	Pirah	k1gMnSc2	Pirah
v	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
deštném	deštný	k2eAgInSc6d1	deštný
pralese	prales	k1gInSc6	prales
Brazílie	Brazílie	k1gFnSc2	Brazílie
odporuje	odporovat	k5eAaImIp3nS	odporovat
Chomského	Chomský	k2eAgMnSc4d1	Chomský
teoriím	teorie	k1gFnPc3	teorie
o	o	k7c6	o
univerzální	univerzální	k2eAgFnSc6d1	univerzální
gramatice	gramatika	k1gFnSc6	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Everett	Everett	k1gMnSc1	Everett
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
Piraha	Pirah	k1gMnSc2	Pirah
nepoužívají	používat	k5eNaImIp3nP	používat
rekurzi	rekurze	k1gFnSc4	rekurze
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgFnPc2d1	klíčová
vlastností	vlastnost	k1gFnPc2	vlastnost
univerzální	univerzální	k2eAgFnSc2d1	univerzální
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
Piraha	Pirah	k1gMnSc2	Pirah
nemají	mít	k5eNaImIp3nP	mít
pevná	pevný	k2eAgNnPc4d1	pevné
slova	slovo	k1gNnPc4	slovo
pro	pro	k7c4	pro
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
neužívají	užívat	k5eNaImIp3nP	užívat
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nP	mluvit
jednoduchými	jednoduchý	k2eAgInPc7d1	jednoduchý
fonémy	foném	k1gInPc7	foném
<g/>
,	,	kIx,	,
a	a	k8xC	a
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
prozódii	prozódie	k1gFnSc6	prozódie
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
jeho	on	k3xPp3gNnSc2	on
tvrzení	tvrzení	k1gNnSc2	tvrzení
sama	sám	k3xTgFnSc1	sám
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
David	David	k1gMnSc1	David
Pesetsky	Pesetsky	k1gMnSc1	Pesetsky
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Nevis	viset	k5eNaImRp2nS	viset
a	a	k8xC	a
Vlineme	vlinout	k5eAaPmIp1nP	vlinout
Rodrigues	Rodrigues	k1gInSc4	Rodrigues
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
článku	článek	k1gInSc6	článek
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
z	z	k7c2	z
Everettových	Everettová	k1gFnPc2	Everettová
tvrzení	tvrzení	k1gNnSc2	tvrzení
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vážné	vážný	k2eAgInPc1d1	vážný
nedostatky	nedostatek	k1gInPc1	nedostatek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Steven	Steven	k2eAgMnSc1d1	Steven
T.	T.	kA	T.
Piantadosi	Piantadose	k1gFnSc4	Piantadose
se	s	k7c7	s
spoluautory	spoluautor	k1gMnPc7	spoluautor
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazyk	jazyk	k1gInSc1	jazyk
lidí	člověk	k1gMnPc2	člověk
Piraha	Piraha	k1gFnSc1	Piraha
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
rekurzi	rekurze	k1gFnSc4	rekurze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velmi	velmi	k6eAd1	velmi
málo	málo	k1gNnSc1	málo
rekurze	rekurze	k1gFnSc2	rekurze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
<g/>
.	.	kIx.	.
<g/>
Geoffrey	Geoffrey	k1gInPc4	Geoffrey
Sampson	Sampsona	k1gFnPc2	Sampsona
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Sussex	Sussex	k1gInSc1	Sussex
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teorie	teorie	k1gFnSc1	teorie
univerzální	univerzální	k2eAgFnSc2d1	univerzální
gramatiky	gramatika	k1gFnSc2	gramatika
není	být	k5eNaImIp3nS	být
falzifikovatelná	falzifikovatelný	k2eAgFnSc1d1	falzifikovatelná
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pseudovědu	pseudověda	k1gFnSc4	pseudověda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
==	==	k?	==
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
rovněž	rovněž	k9	rovněž
studiem	studio	k1gNnSc7	studio
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
formálních	formální	k2eAgInPc2d1	formální
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
výzkumem	výzkum	k1gInSc7	výzkum
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tyto	tento	k3xDgFnPc1	tento
mohou	moct	k5eAaImIp3nP	moct
či	či	k8xC	či
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
schopny	schopen	k2eAgFnPc1d1	schopna
zachytit	zachytit	k5eAaPmF	zachytit
klíčové	klíčový	k2eAgFnPc4d1	klíčová
vlastnosti	vlastnost	k1gFnPc4	vlastnost
lidského	lidský	k2eAgInSc2d1	lidský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
dělí	dělit	k5eAaImIp3nP	dělit
formální	formální	k2eAgFnPc4d1	formální
gramatiky	gramatika	k1gFnPc4	gramatika
do	do	k7c2	do
tříd	třída	k1gFnPc2	třída
a	a	k8xC	a
skupin	skupina	k1gFnPc2	skupina
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
vyjadřovací	vyjadřovací	k2eAgFnSc7d1	vyjadřovací
silou	síla	k1gFnSc7	síla
–	–	k?	–
každá	každý	k3xTgFnSc1	každý
následná	následný	k2eAgFnSc1d1	následná
třída	třída	k1gFnSc1	třída
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
generovat	generovat	k5eAaImF	generovat
širší	široký	k2eAgFnSc4d2	širší
skupinu	skupina	k1gFnSc4	skupina
formálních	formální	k2eAgInPc2d1	formální
jazyků	jazyk	k1gInPc2	jazyk
než	než	k8xS	než
předchozí	předchozí	k2eAgFnSc1d1	předchozí
třída	třída	k1gFnSc1	třída
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
modelování	modelování	k1gNnSc1	modelování
některých	některý	k3yIgInPc2	některý
aspektů	aspekt	k1gInPc2	aspekt
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
komplexnější	komplexní	k2eAgFnSc4d2	komplexnější
formální	formální	k2eAgFnSc4d1	formální
gramatiku	gramatika	k1gFnSc4	gramatika
(	(	kIx(	(
<g/>
měřeno	měřit	k5eAaImNgNnS	měřit
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchií	hierarchie	k1gFnSc7	hierarchie
<g/>
)	)	kIx)	)
než	než	k8xS	než
modelování	modelování	k1gNnSc1	modelování
jiných	jiný	k1gMnPc2	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zatímco	zatímco	k8xS	zatímco
regulární	regulární	k2eAgInSc1d1	regulární
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
silný	silný	k2eAgInSc1d1	silný
k	k	k7c3	k
modelování	modelování	k1gNnSc3	modelování
morfologie	morfologie	k1gFnSc2	morfologie
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
už	už	k6eAd1	už
schopen	schopen	k2eAgMnSc1d1	schopen
modelovat	modelovat	k5eAaImF	modelovat
její	její	k3xOp3gFnSc4	její
syntax	syntax	k1gFnSc4	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
konstrukci	konstrukce	k1gFnSc6	konstrukce
kompilátorů	kompilátor	k1gInPc2	kompilátor
a	a	k8xC	a
teorií	teorie	k1gFnPc2	teorie
automatů	automat	k1gInPc2	automat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chomského	Chomský	k2eAgInSc2d1	Chomský
politické	politický	k2eAgInPc4d1	politický
názory	názor	k1gInPc4	názor
==	==	k?	==
</s>
</p>
<p>
<s>
Chomský	Chomský	k2eAgInSc1d1	Chomský
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
reputaci	reputace	k1gFnSc4	reputace
politického	politický	k2eAgMnSc2d1	politický
disidenta	disident	k1gMnSc2	disident
<g/>
,	,	kIx,	,
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
Západní	západní	k2eAgFnSc3d1	západní
mocnosti	mocnost	k1gFnSc3	mocnost
<g/>
,	,	kIx,	,
propagandu	propaganda	k1gFnSc4	propaganda
a	a	k8xC	a
Západem	západ	k1gInSc7	západ
vedené	vedený	k2eAgFnSc2d1	vedená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k8xC	jako
státem	stát	k1gInSc7	stát
sponzorovaný	sponzorovaný	k2eAgInSc1d1	sponzorovaný
terorismus	terorismus	k1gInSc1	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
příznivce	příznivec	k1gMnPc4	příznivec
anarchosyndikalismu	anarchosyndikalismus	k1gInSc2	anarchosyndikalismus
a	a	k8xC	a
libertariánského	libertariánský	k2eAgInSc2d1	libertariánský
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
odborů	odbor	k1gInPc2	odbor
Industrial	Industrial	k1gMnSc1	Industrial
Workers	Workersa	k1gFnPc2	Workersa
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
(	(	kIx(	(
<g/>
Průmysloví	průmyslový	k2eAgMnPc1d1	průmyslový
dělníci	dělník	k1gMnPc1	dělník
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejcitovanějším	citovaný	k2eAgMnPc3d3	nejcitovanější
a	a	k8xC	a
nejvlivnějším	vlivný	k2eAgMnPc3d3	nejvlivnější
intelektuálům	intelektuál	k1gMnPc3	intelektuál
dneška	dnešek	k1gInSc2	dnešek
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
aktivně	aktivně	k6eAd1	aktivně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
ve	v	k7c6	v
Vietnamu	Vietnam	k1gInSc6	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
svého	svůj	k3xOyFgInSc2	svůj
protiválečného	protiválečný	k2eAgInSc2d1	protiválečný
aktivismu	aktivismus	k1gInSc2	aktivismus
byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
"	"	kIx"	"
<g/>
seznam	seznam	k1gInSc4	seznam
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
"	"	kIx"	"
amerického	americký	k2eAgMnSc4d1	americký
prezidenta	prezident	k1gMnSc4	prezident
Richarda	Richard	k1gMnSc4	Richard
Nixona	Nixon	k1gMnSc4	Nixon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mediální	mediální	k2eAgFnSc1d1	mediální
propaganda	propaganda	k1gFnSc1	propaganda
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
===	===	k?	===
</s>
</p>
<p>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hermanem	Herman	k1gMnSc7	Herman
vydali	vydat	k5eAaPmAgMnP	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Consent	Consent	k1gMnSc1	Consent
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Political	Political	k1gFnSc2	Political
Economy	Econom	k1gInPc4	Econom
of	of	k?	of
the	the	k?	the
Mass	Mass	k1gInSc1	Mass
Media	medium	k1gNnSc2	medium
<g/>
,	,	kIx,	,
pojednávající	pojednávající	k2eAgFnSc2d1	pojednávající
o	o	k7c6	o
tvorbě	tvorba	k1gFnSc6	tvorba
společenského	společenský	k2eAgInSc2d1	společenský
souhlasu	souhlas	k1gInSc2	souhlas
s	s	k7c7	s
establishmentem	establishment	k1gInSc7	establishment
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
inspirování	inspirování	k1gNnSc2	inspirování
neomarxismem	neomarxismus	k1gInSc7	neomarxismus
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vzešlou	vzešlý	k2eAgFnSc7d1	vzešlá
politickou	politický	k2eAgFnSc7d1	politická
ekonomií	ekonomie	k1gFnSc7	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
publikaci	publikace	k1gFnSc6	publikace
popsali	popsat	k5eAaPmAgMnP	popsat
pět	pět	k4xCc4	pět
filtrů	filtr	k1gInPc2	filtr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
výslednou	výsledný	k2eAgFnSc4d1	výsledná
podobu	podoba	k1gFnSc4	podoba
mediálních	mediální	k2eAgInPc2d1	mediální
obsahů	obsah	k1gInPc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
mediální	mediální	k2eAgInSc4d1	mediální
model	model	k1gInSc4	model
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
orientace	orientace	k1gFnSc1	orientace
na	na	k7c4	na
zisk	zisk	k1gInSc4	zisk
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
reklamy	reklama	k1gFnSc2	reklama
a	a	k8xC	a
inzerce	inzerce	k1gFnSc2	inzerce
</s>
</p>
<p>
<s>
Vliv	vliv	k1gInSc1	vliv
informačních	informační	k2eAgInPc2d1	informační
zdrojů	zdroj	k1gInPc2	zdroj
</s>
</p>
<p>
<s>
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
nátlak	nátlak	k1gInSc1	nátlak
na	na	k7c4	na
médium	médium	k1gNnSc4	médium
</s>
</p>
<p>
<s>
Ideologie	ideologie	k1gFnSc1	ideologie
</s>
</p>
<p>
<s>
===	===	k?	===
Chomsky	Chomsky	k1gFnPc4	Chomsky
a	a	k8xC	a
východoevropský	východoevropský	k2eAgInSc4d1	východoevropský
disent	disent	k1gInSc4	disent
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
Chomsky	Chomsky	k1gMnSc1	Chomsky
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
americkou	americký	k2eAgFnSc4d1	americká
veřejnost	veřejnost	k1gFnSc4	veřejnost
k	k	k7c3	k
protestům	protest	k1gInPc3	protest
proti	proti	k7c3	proti
útlaku	útlak	k1gInSc3	útlak
československých	československý	k2eAgMnPc2d1	československý
vědců	vědec	k1gMnPc2	vědec
normalizačními	normalizační	k2eAgInPc7d1	normalizační
úřady	úřad	k1gInPc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
podepsal	podepsat	k5eAaPmAgMnS	podepsat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Pelikánem	Pelikán	k1gMnSc7	Pelikán
<g/>
,	,	kIx,	,
Andrejem	Andrej	k1gMnSc7	Andrej
Sacharovem	Sacharov	k1gInSc7	Sacharov
<g/>
,	,	kIx,	,
Pavlem	Pavel	k1gMnSc7	Pavel
Litvinovem	Litvinov	k1gInSc7	Litvinov
výzvu	výzva	k1gFnSc4	výzva
k	k	k7c3	k
propuštění	propuštění	k1gNnSc3	propuštění
československých	československý	k2eAgMnPc2d1	československý
politických	politický	k2eAgMnPc2d1	politický
vězňů	vězeň	k1gMnPc2	vězeň
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Šabaty	Šabata	k1gMnSc2	Šabata
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Müllera	Müller	k1gMnSc2	Müller
<g/>
.	.	kIx.	.
</s>
<s>
Protestoval	protestovat	k5eAaBmAgMnS	protestovat
i	i	k9	i
proti	proti	k7c3	proti
rozsudku	rozsudek	k1gInSc3	rozsudek
nad	nad	k7c7	nad
polskými	polský	k2eAgMnPc7d1	polský
disidenty	disident	k1gMnPc7	disident
Jackem	Jacek	k1gMnSc7	Jacek
Czaputowiczem	Czaputowicz	k1gMnSc7	Czaputowicz
a	a	k8xC	a
Piotrem	Piotr	k1gMnSc7	Piotr
Niemczykem	Niemczyk	k1gInSc7	Niemczyk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
podepsal	podepsat	k5eAaPmAgInS	podepsat
prohlášení	prohlášení	k1gNnSc4	prohlášení
proti	proti	k7c3	proti
perzekuci	perzekuce	k1gFnSc3	perzekuce
československých	československý	k2eAgMnPc2d1	československý
chartistů	chartista	k1gMnPc2	chartista
<g/>
;	;	kIx,	;
Ivana	Ivan	k1gMnSc2	Ivan
"	"	kIx"	"
<g/>
Magora	magor	k1gMnSc2	magor
<g/>
"	"	kIx"	"
Jirouse	Jirouse	k1gFnSc1	Jirouse
<g/>
,	,	kIx,	,
Jiřího	Jiří	k1gMnSc2	Jiří
Tichého	Tichý	k1gMnSc2	Tichý
<g/>
,	,	kIx,	,
Hany	Hana	k1gFnSc2	Hana
Marvanové	Marvanová	k1gFnSc2	Marvanová
<g/>
,	,	kIx,	,
Tomáše	Tomáš	k1gMnSc2	Tomáš
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
,	,	kIx,	,
Petra	Petr	k1gMnSc2	Petr
Cibulky	Cibulka	k1gMnSc2	Cibulka
nebo	nebo	k8xC	nebo
Jiřího	Jiří	k1gMnSc2	Jiří
Štencla	Štencl	k1gMnSc2	Štencl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
například	například	k6eAd1	například
anglický	anglický	k2eAgMnSc1d1	anglický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Harold	Harold	k1gMnSc1	Harold
Pinter	Pinter	k1gMnSc1	Pinter
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Chomsky	Chomsky	k1gFnSc7	Chomsky
vůči	vůči	k7c3	vůči
východoevropskému	východoevropský	k2eAgInSc3d1	východoevropský
disentu	disent	k1gInSc3	disent
kritický	kritický	k2eAgInSc1d1	kritický
postoj	postoj	k1gInSc1	postoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
A2	A2	k1gFnSc2	A2
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
Chomsky	Chomsky	k1gMnSc1	Chomsky
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
byl	být	k5eAaImAgMnS	být
statečný	statečný	k2eAgMnSc1d1	statečný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c2	za
práva	právo	k1gNnSc2	právo
svých	svůj	k3xOyFgMnPc2	svůj
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
a	a	k8xC	a
nebál	bát	k5eNaImAgMnS	bát
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jít	jít	k5eAaImF	jít
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
však	však	k9	však
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Havlovo	Havlův	k2eAgNnSc4d1	Havlovo
přehlížení	přehlížení	k1gNnSc4	přehlížení
porušování	porušování	k1gNnSc2	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
měl	mít	k5eAaImAgMnS	mít
společné	společný	k2eAgNnSc4d1	společné
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
východoevropskými	východoevropský	k2eAgMnPc7d1	východoevropský
disidenty	disident	k1gMnPc7	disident
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Chomského	Chomský	k2eAgNnSc2d1	Chomský
tento	tento	k3xDgInSc4	tento
postoj	postoj	k1gInSc1	postoj
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
východoevropské	východoevropský	k2eAgMnPc4d1	východoevropský
disidenty	disident	k1gMnPc4	disident
protežovaly	protežovat	k5eAaImAgFnP	protežovat
západní	západní	k2eAgFnPc1d1	západní
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
,	,	kIx,	,
s	s	k7c7	s
jichž	jenž	k3xRgFnPc2	jenž
pomocí	pomoc	k1gFnPc2	pomoc
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
dodalo	dodat	k5eAaPmAgNnS	dodat
pocit	pocit	k1gInSc4	pocit
vlastní	vlastní	k2eAgFnSc2d1	vlastní
výjimečnosti	výjimečnost	k1gFnSc2	výjimečnost
<g/>
.	.	kIx.	.
</s>
<s>
Utrpení	utrpení	k1gNnSc1	utrpení
východoevropských	východoevropský	k2eAgInPc2d1	východoevropský
disentů	disent	k1gInPc2	disent
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
neznámými	známý	k2eNgMnPc7d1	neznámý
latinskoamerickými	latinskoamerický	k2eAgMnPc7d1	latinskoamerický
disidenty	disident	k1gMnPc7	disident
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
hrozila	hrozit	k5eAaImAgFnS	hrozit
likvidace	likvidace	k1gFnSc1	likvidace
eskadrami	eskadra	k1gFnPc7	eskadra
smrti	smrt	k1gFnSc3	smrt
podporovanými	podporovaný	k2eAgFnPc7d1	podporovaná
USA	USA	kA	USA
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nijak	nijak	k6eAd1	nijak
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
východoevropský	východoevropský	k2eAgInSc4d1	východoevropský
disent	disent	k1gInSc4	disent
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
Chomského	Chomský	k2eAgInSc2d1	Chomský
utvrdil	utvrdit	k5eAaPmAgInS	utvrdit
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
"	"	kIx"	"
<g/>
jediní	jediný	k2eAgMnPc1d1	jediný
utlačovaní	utlačovaný	k1gMnPc1	utlačovaný
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgMnSc7d1	velký
kritikem	kritik	k1gMnSc7	kritik
Operace	operace	k1gFnSc2	operace
Kondor	kondor	k1gMnSc1	kondor
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
pravicové	pravicový	k2eAgInPc4d1	pravicový
diktátorské	diktátorský	k2eAgInPc4d1	diktátorský
režimy	režim	k1gInPc4	režim
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc6	Chile
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
Latinské	latinský	k2eAgFnSc2d1	Latinská
Ameriky	Amerika	k1gFnSc2	Amerika
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
CIA	CIA	kA	CIA
pronásledovaly	pronásledovat	k5eAaImAgInP	pronásledovat
veškeré	veškerý	k3xTgNnSc4	veškerý
skutečné	skutečný	k2eAgNnSc4d1	skutečné
i	i	k9	i
domnělé	domnělý	k2eAgMnPc4d1	domnělý
nepřátele	nepřítel	k1gMnPc4	nepřítel
svých	svůj	k3xOyFgMnPc2	svůj
režimů	režim	k1gInPc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
perzekvovaných	perzekvovaný	k2eAgMnPc2d1	perzekvovaný
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
téměř	téměř	k6eAd1	téměř
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
80	[number]	k4	80
000	[number]	k4	000
popravených	popravený	k2eAgMnPc2d1	popravený
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
zmizelých	zmizelý	k2eAgMnPc2d1	zmizelý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Chomsky	Chomsky	k1gFnSc1	Chomsky
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
projevem	projev	k1gInSc7	projev
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
kongresu	kongres	k1gInSc6	kongres
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
chválil	chválit	k5eAaImAgMnS	chválit
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
jako	jako	k8xC	jako
obránce	obránce	k1gMnPc4	obránce
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
však	však	k9	však
Reaganovou	Reaganův	k2eAgFnSc7d1	Reaganova
administrativou	administrativa	k1gFnSc7	administrativa
podporovaná	podporovaný	k2eAgFnSc1d1	podporovaná
vláda	vláda	k1gFnSc1	vláda
Salvadoru	Salvador	k1gInSc2	Salvador
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
zavraždit	zavraždit	k5eAaPmF	zavraždit
šest	šest	k4xCc4	šest
k	k	k7c3	k
vládě	vláda	k1gFnSc3	vláda
kritických	kritický	k2eAgInPc2d1	kritický
jezuitských	jezuitský	k2eAgInPc2d1	jezuitský
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc3	jejich
spolupracovnici	spolupracovnice	k1gFnSc3	spolupracovnice
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
dceru	dcera	k1gFnSc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
případů	případ	k1gInPc2	případ
vražd	vražda	k1gFnPc2	vražda
disidentů	disident	k1gMnPc2	disident
během	během	k7c2	během
Salvadorské	salvadorský	k2eAgFnSc2d1	Salvadorská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
česká	český	k2eAgFnSc1d1	Česká
média	médium	k1gNnSc2	médium
konfrontovala	konfrontovat	k5eAaBmAgFnS	konfrontovat
s	s	k7c7	s
Chomského	Chomský	k2eAgInSc2d1	Chomský
názory	názor	k1gInPc1	názor
bývalého	bývalý	k2eAgMnSc2d1	bývalý
disidenta	disident	k1gMnSc2	disident
a	a	k8xC	a
konzervativního	konzervativní	k2eAgMnSc4d1	konzervativní
politika	politik	k1gMnSc4	politik
Alexandra	Alexandr	k1gMnSc4	Alexandr
Vondru	Vondra	k1gMnSc4	Vondra
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
ně	on	k3xPp3gNnPc4	on
reagoval	reagovat	k5eAaBmAgMnS	reagovat
argumentací	argumentace	k1gFnSc7	argumentace
ad	ad	k7c4	ad
hominem	homino	k1gNnSc7	homino
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tu	tu	k6eAd1	tu
<g />
.	.	kIx.	.
</s>
<s>
lidé	člověk	k1gMnPc1	člověk
jako	jako	k8xS	jako
Havel	Havel	k1gMnSc1	Havel
seděli	sedět	k5eAaImAgMnP	sedět
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
v	v	k7c6	v
komunistickém	komunistický	k2eAgNnSc6d1	komunistické
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
z	z	k7c2	z
bostonských	bostonský	k2eAgFnPc2d1	Bostonská
kaváren	kavárna	k1gFnPc2	kavárna
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
genocidu	genocida	k1gFnSc4	genocida
Pol	pola	k1gFnPc2	pola
Pota	Pot	k1gInSc2	Pot
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Chomského	Chomský	k2eAgMnSc4d1	Chomský
z	z	k7c2	z
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
a	a	k8xC	a
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
naslouchání	naslouchání	k1gNnSc1	naslouchání
"	"	kIx"	"
<g/>
kecům	kec	k1gInPc3	kec
takových	takový	k3xDgMnPc2	takový
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
povede	povést	k5eAaPmIp3nS	povést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
a	a	k8xC	a
gulagů	gulag	k1gInPc2	gulag
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
argumentaci	argumentace	k1gFnSc3	argumentace
ad	ad	k7c4	ad
hominem	homin	k1gMnSc7	homin
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
také	také	k9	také
psychiatr	psychiatr	k1gMnSc1	psychiatr
Cyril	Cyril	k1gMnSc1	Cyril
Höschl	Höschl	k1gMnSc1	Höschl
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
disident	disident	k1gMnSc1	disident
a	a	k8xC	a
liberální	liberální	k2eAgMnSc1d1	liberální
politik	politik	k1gMnSc1	politik
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
český	český	k2eAgInSc4d1	český
disent	disent	k1gInSc4	disent
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
nějak	nějak	k6eAd1	nějak
zvlášť	zvlášť	k6eAd1	zvlášť
trpěl	trpět	k5eAaImAgMnS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozsah	rozsah	k1gInSc1	rozsah
brutality	brutalita	k1gFnSc2	brutalita
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
režimů	režim	k1gInPc2	režim
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
prakticky	prakticky	k6eAd1	prakticky
neznámý	známý	k2eNgMnSc1d1	neznámý
a	a	k8xC	a
připustil	připustit	k5eAaPmAgMnS	připustit
podporu	podpora	k1gFnSc4	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
západních	západní	k2eAgNnPc2d1	západní
medií	medium	k1gNnPc2	medium
a	a	k8xC	a
ochránců	ochránce	k1gMnPc2	ochránce
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
Jiří	Jiří	k1gMnSc1	Jiří
Pehe	Pehe	k1gFnSc4	Pehe
označil	označit	k5eAaPmAgMnS	označit
celý	celý	k2eAgInSc4d1	celý
spor	spor	k1gInSc4	spor
za	za	k7c4	za
trapný	trapný	k2eAgInSc4d1	trapný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
zjednodušují	zjednodušovat	k5eAaImIp3nP	zjednodušovat
<g/>
.	.	kIx.	.
</s>
<s>
Předně	předně	k6eAd1	předně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
Chomsky	Chomsky	k1gFnSc1	Chomsky
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
hodnocení	hodnocení	k1gNnSc6	hodnocení
opomíjí	opomíjet	k5eAaImIp3nS	opomíjet
stalinistický	stalinistický	k2eAgInSc1d1	stalinistický
teror	teror	k1gInSc1	teror
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Porovnávání	porovnávání	k1gNnSc4	porovnávání
útlaku	útlak	k1gInSc2	útlak
disentu	disent	k1gInSc2	disent
v	v	k7c6	v
sovětském	sovětský	k2eAgInSc6d1	sovětský
bloku	blok	k1gInSc6	blok
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
brutalitou	brutalita	k1gFnSc7	brutalita
některých	některý	k3yIgInPc2	některý
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
režimů	režim	k1gInPc2	režim
považuje	považovat	k5eAaImIp3nS	považovat
přinejmenším	přinejmenším	k6eAd1	přinejmenším
za	za	k7c4	za
ošidné	ošidný	k2eAgNnSc4d1	ošidné
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Jan	Jan	k1gMnSc1	Jan
Ruml	Ruml	k1gMnSc1	Ruml
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
českoslovenští	československý	k2eAgMnPc1d1	československý
disidenti	disident	k1gMnPc1	disident
cítili	cítit	k5eAaImAgMnP	cítit
výjimeční	výjimeční	k2eAgMnPc1d1	výjimeční
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
jediní	jediný	k2eAgMnPc1d1	jediný
utlačovaní	utlačovaný	k1gMnPc1	utlačovaný
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kavan	Kavan	k1gMnSc1	Kavan
odsoudil	odsoudit	k5eAaPmAgMnS	odsoudit
sprosté	sprostý	k2eAgInPc4d1	sprostý
útoky	útok	k1gInPc4	útok
českých	český	k2eAgMnPc2d1	český
novinářů	novinář	k1gMnPc2	novinář
a	a	k8xC	a
komentátorů	komentátor	k1gMnPc2	komentátor
<g/>
.	.	kIx.	.
</s>
<s>
Připomněl	připomnět	k5eAaPmAgMnS	připomnět
Chomského	Chomský	k2eAgInSc2d1	Chomský
podporu	podpor	k1gInSc2	podpor
českého	český	k2eAgInSc2d1	český
disentu	disent	k1gInSc2	disent
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Husákovy	Husákův	k2eAgFnSc2d1	Husákova
normalizace	normalizace	k1gFnSc2	normalizace
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
nařčení	nařčení	k1gNnSc4	nařčení
z	z	k7c2	z
antiamerikanismu	antiamerikanismus	k1gInSc2	antiamerikanismus
a	a	k8xC	a
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mu	on	k3xPp3gMnSc3	on
Chomsky	Chomsky	k1gMnPc1	Chomsky
v	v	k7c6	v
soukromém	soukromý	k2eAgInSc6d1	soukromý
rozhovoru	rozhovor	k1gInSc6	rozhovor
sdělil	sdělit	k5eAaPmAgMnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
sionistou	sionista	k1gMnSc7	sionista
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
nebrání	bránit	k5eNaImIp3nS	bránit
kritizovat	kritizovat	k5eAaImF	kritizovat
izraelskou	izraelský	k2eAgFnSc4d1	izraelská
politiku	politika	k1gFnSc4	politika
vůči	vůči	k7c3	vůči
Palestincům	Palestinec	k1gMnPc3	Palestinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
–	–	k?	–
Perspektivy	perspektiva	k1gFnSc2	perspektiva
moci	moct	k5eAaImF	moct
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Tajnosti	tajnost	k1gFnSc2	tajnost
<g/>
,	,	kIx,	,
lži	lež	k1gFnSc2	lež
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
–	–	k?	–
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
moc	moc	k1gFnSc1	moc
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
–	–	k?	–
Hegemonie	hegemonie	k1gFnSc2	hegemonie
nebo	nebo	k8xC	nebo
přežití	přežití	k1gNnSc2	přežití
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
–	–	k?	–
Moc	moc	k1gFnSc4	moc
a	a	k8xC	a
teror	teror	k1gInSc4	teror
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
–	–	k?	–
Intervence	intervence	k1gFnPc1	intervence
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Hnutí	hnutí	k1gNnSc2	hnutí
Occupy	Occupa	k1gFnSc2	Occupa
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905309	[number]	k4	905309
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
Broken	Broken	k2eAgInSc1d1	Broken
Books	Books	k1gInSc1	Books
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Západní	západní	k2eAgInSc1d1	západní
terorismus	terorismus	k1gInSc1	terorismus
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
905309	[number]	k4	905309
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
Broken	Broken	k2eAgInSc1d1	Broken
Books	Books	k1gInSc1	Books
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
–	–	k?	–
Disident	disident	k1gMnSc1	disident
Západu	západ	k1gInSc2	západ
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978-80-246-2629-1	[number]	k4	978-80-246-2629-1
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
–	–	k?	–
Conversation	Conversation	k1gInSc1	Conversation
on	on	k3xPp3gMnSc1	on
Palestine	Palestin	k1gInSc5	Palestin
(	(	kIx(	(
<g/>
Plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
Broken	Broken	k2eAgInSc1d1	Broken
Books	Books	k1gInSc1	Books
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Democracy	Democraca	k1gFnPc1	Democraca
in	in	k?	in
Europe	Europ	k1gInSc5	Europ
Movement	Movement	k1gMnSc1	Movement
2025	[number]	k4	2025
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
TOMEK	Tomek	k1gMnSc1	Tomek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
SLAČÁLEK	SLAČÁLEK	kA	SLAČÁLEK
<g/>
,	,	kIx,	,
Ondřej	Ondřej	k1gMnSc1	Ondřej
<g/>
.	.	kIx.	.
</s>
<s>
Anarchismus	anarchismus	k1gInSc1	anarchismus
:	:	kIx,	:
svoboda	svoboda	k1gFnSc1	svoboda
proti	proti	k7c3	proti
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
781	[number]	k4	781
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
přirozenost	přirozenost	k1gFnSc1	přirozenost
proti	proti	k7c3	proti
nelegitimní	legitimní	k2eNgFnSc3d1	nelegitimní
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
s.	s.	k?	s.
523	[number]	k4	523
<g/>
-	-	kIx~	-
<g/>
529	[number]	k4	529
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOX	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc1	Jerem
<g/>
.	.	kIx.	.
</s>
<s>
Chomsky	Chomsky	k6eAd1	Chomsky
a	a	k8xC	a
globalizace	globalizace	k1gFnSc1	globalizace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
85	[number]	k4	85
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7254	[number]	k4	7254
<g/>
-	-	kIx~	-
<g/>
367	[number]	k4	367
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Noam	Noama	k1gFnPc2	Noama
Chomsky	Chomsky	k1gFnSc2	Chomsky
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Noam	Noama	k1gFnPc2	Noama
Chomsky	Chomsky	k1gFnPc2	Chomsky
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Noam	Noam	k1gMnSc1	Noam
Chomsky	Chomsky	k1gMnSc1	Chomsky
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
webová	webový	k2eAgFnSc1d1	webová
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Videozáznam	videozáznam	k1gInSc1	videozáznam
z	z	k7c2	z
přednášky	přednáška	k1gFnSc2	přednáška
Chomského	Chomský	k2eAgMnSc2d1	Chomský
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Knižní	knižní	k2eAgNnSc1d1	knižní
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
vydávající	vydávající	k2eAgInPc1d1	vydávající
překlady	překlad	k1gInPc1	překlad
Chomskeho	Chomske	k1gMnSc2	Chomske
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
České	český	k2eAgInPc1d1	český
a	a	k8xC	a
slovenské	slovenský	k2eAgInPc1d1	slovenský
překlady	překlad	k1gInPc1	překlad
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
listech	list	k1gInPc6	list
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Noam	Noa	k1gNnSc7	Noa
Chomsky	Chomsky	k1gFnSc2	Chomsky
o	o	k7c6	o
anarchismu	anarchismus	k1gInSc6	anarchismus
<g/>
,	,	kIx,	,
marxismu	marxismus	k1gInSc2	marxismus
a	a	k8xC	a
naději	naděje	k1gFnSc4	naděje
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
Nová	nový	k2eAgFnSc1d1	nová
válka	válka	k1gFnSc1	válka
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
,	,	kIx,	,
výňatek	výňatek	k1gInSc4	výňatek
z	z	k7c2	z
Chomskyho	Chomsky	k1gMnSc2	Chomsky
přednášky	přednáška	k1gFnSc2	přednáška
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Noam	Noa	k1gNnSc7	Noa
Chomsky	Chomsky	k1gFnSc2	Chomsky
–	–	k?	–
Anarchy	Anarcha	k1gFnSc2	Anarcha
Archives	Archivesa	k1gFnPc2	Archivesa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rozhovor	rozhovor	k1gInSc1	rozhovor
z	z	k7c2	z
Chomským	Chomský	k2eAgNnPc3d1	Chomský
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Hyde	Hyde	k1gNnSc2	Hyde
Park	park	k1gInSc1	park
Civilizace	civilizace	k1gFnSc2	civilizace
programu	program	k1gInSc2	program
ČT24	ČT24	k1gFnSc2	ČT24
</s>
</p>
<p>
<s>
===	===	k?	===
Audiovizuální	audiovizuální	k2eAgInPc1d1	audiovizuální
dokumenty	dokument	k1gInPc1	dokument
===	===	k?	===
</s>
</p>
<p>
<s>
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Consent	Consent	k1gInSc1	Consent
<g/>
:	:	kIx,	:
Noam	Noam	k1gInSc1	Noam
Chomsky	Chomsky	k1gFnSc2	Chomsky
and	and	k?	and
The	The	k1gFnSc2	The
Media	medium	k1gNnSc2	medium
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
167	[number]	k4	167
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Mark	Mark	k1gMnSc1	Mark
Achbar	Achbar	k1gMnSc1	Achbar
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Wintonick	Wintonick	k1gMnSc1	Wintonick
</s>
</p>
