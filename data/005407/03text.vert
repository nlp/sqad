<p>
<s>
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
přepisu	přepis	k1gInSc6	přepis
Čung-chua	Čunghua	k1gFnSc1	Čung-chua
žen-min	ženin	k1gInSc1	žen-min
kung-che-kuo	kungheuo	k6eAd1	kung-che-kuo
<g/>
,	,	kIx,	,
pchin-jinem	pchinin	k1gInSc7	pchin-jin
Zhō	Zhō	k1gFnSc2	Zhō
rénmín	rénmín	k1gInSc1	rénmín
gò	gò	k?	gò
<g/>
,	,	kIx,	,
znaky	znak	k1gInPc4	znak
zjednodušené	zjednodušený	k2eAgFnSc2d1	zjednodušená
中	中	k?	中
<g/>
,	,	kIx,	,
zkratka	zkratka	k1gFnSc1	zkratka
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
mezinárodně	mezinárodně	k6eAd1	mezinárodně
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
ležící	ležící	k2eAgInSc1d1	ležící
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
9,6	[number]	k4	9,6
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
rychle	rychle	k6eAd1	rychle
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
ekonomice	ekonomika	k1gFnSc3	ekonomika
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc3d1	vojenská
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
spojenému	spojený	k2eAgInSc3d1	spojený
vzrůstajícímu	vzrůstající	k2eAgInSc3d1	vzrůstající
vlivu	vliv	k1gInSc3	vliv
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
stala	stát	k5eAaPmAgFnS	stát
novou	nový	k2eAgFnSc7d1	nová
supervelmocí	supervelmoc	k1gFnSc7	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
je	být	k5eAaImIp3nS	být
ČLR	ČLR	kA	ČLR
stálým	stálý	k2eAgInSc7d1	stálý
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
postavení	postavení	k1gNnSc1	postavení
odepřeno	odepřít	k5eAaPmNgNnS	odepřít
Čínské	čínský	k2eAgFnSc6d1	čínská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
vedena	vést	k5eAaImNgFnS	vést
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
již	již	k6eAd1	již
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
svoji	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
bývalé	bývalý	k2eAgFnSc2d1	bývalá
cizí	cizí	k2eAgFnSc2d1	cizí
kolonie	kolonie	k1gFnSc2	kolonie
Hongkong	Hongkong	k1gInSc1	Hongkong
a	a	k8xC	a
Macao	Macao	k1gNnSc1	Macao
<g/>
.	.	kIx.	.
</s>
<s>
Vznáší	vznášet	k5eAaImIp3nS	vznášet
nárok	nárok	k1gInSc4	nárok
i	i	k9	i
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgNnPc2d1	další
menších	malý	k2eAgNnPc2d2	menší
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
spravovány	spravovat	k5eAaImNgInP	spravovat
vládou	vláda	k1gFnSc7	vláda
Čínské	čínský	k2eAgFnPc1d1	čínská
republiky	republika	k1gFnPc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Prehistorie	prehistorie	k1gFnSc2	prehistorie
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc1d1	původní
osídlení	osídlení	k1gNnSc1	osídlení
Číny	Čína	k1gFnSc2	Čína
náleží	náležet	k5eAaImIp3nS	náležet
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnSc4d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
Sinanthropua	Sinanthropu	k1gInSc2	Sinanthropu
Pekinensis	Pekinensis	k1gFnSc2	Pekinensis
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
osídlení	osídlení	k1gNnSc4	osídlení
již	již	k6eAd1	již
před	před	k7c4	před
půl	půl	k1xP	půl
miliónem	milión	k4xCgInSc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vlády	vláda	k1gFnPc1	vláda
dynastií	dynastie	k1gFnSc7	dynastie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Sia	Sia	k1gFnSc2	Sia
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgFnSc4	první
čínskou	čínský	k2eAgFnSc4d1	čínská
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
dynastii	dynastie	k1gFnSc4	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Šang	Šang	k1gMnSc1	Šang
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
historicky	historicky	k6eAd1	historicky
doložená	doložený	k2eAgFnSc1d1	doložená
čínská	čínský	k2eAgFnSc1d1	čínská
dynastie	dynastie	k1gFnSc1	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Vládla	vládnout	k5eAaImAgFnS	vládnout
v	v	k7c6	v
17.	[number]	k4	17.
až	až	k9	až
16.	[number]	k4	16.
století	století	k1gNnPc2	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
používány	používat	k5eAaImNgFnP	používat
písmo	písmo	k1gNnSc4	písmo
a	a	k8xC	a
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
<g/>
Dynastie	dynastie	k1gFnSc1	dynastie
Čou	Čou	k1gFnSc2	Čou
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
v	v	k7c6	v
11.	[number]	k4	11.
až	až	k9	až
8.	[number]	k4	8.
století	století	k1gNnPc2	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
již	již	k6eAd1	již
ovládala	ovládat	k5eAaImAgFnS	ovládat
a	a	k8xC	a
spravovala	spravovat	k5eAaImAgFnS	spravovat
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
území	území	k1gNnSc4	území
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Jang-c	Jang	k1gInSc1	Jang-c
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
ťiang	ťiang	k1gInSc4	ťiang
až	až	k9	až
po	po	k7c4	po
Velkou	velký	k2eAgFnSc4d1	velká
čínskou	čínský	k2eAgFnSc4d1	čínská
zeď	zeď	k1gFnSc4	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Čouské	Čouský	k2eAgNnSc1d1	Čouský
období	období	k1gNnSc1	období
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Západní	západní	k2eAgFnPc4d1	západní
a	a	k8xC	a
Východní	východní	k2eAgFnPc4d1	východní
Čou	Čou	k1gFnPc4	Čou
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Východního	východní	k2eAgMnSc2d1	východní
Čou	Čou	k1gMnSc2	Čou
se	se	k3xPyFc4	se
vládnoucí	vládnoucí	k2eAgMnSc1d1	vládnoucí
král	král	k1gMnSc1	král
přesunul	přesunout	k5eAaPmAgMnS	přesunout
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
moderního	moderní	k2eAgInSc2d1	moderní
Si-anu	Sin	k1gInSc2	Si-an
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
moc	moc	k1gFnSc1	moc
čouského	čouský	k1gMnSc2	čouský
krále	král	k1gMnSc2	král
zeslábla	zeslábnout	k5eAaPmAgFnS	zeslábnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8.	[number]	k4	8.
až	až	k9	až
5.	[number]	k4	5.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
následovalo	následovat	k5eAaImAgNnS	následovat
období	období	k1gNnSc1	období
Jar	Jara	k1gFnPc2	Jara
a	a	k8xC	a
podzimů	podzim	k1gInPc2	podzim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
vládci	vládce	k1gMnPc1	vládce
údělných	údělný	k2eAgInPc2d1	údělný
států	stát	k1gInPc2	stát
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
zemi	zem	k1gFnSc4	zem
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
Válčících	válčící	k2eAgInPc2d1	válčící
států	stát	k1gInPc2	stát
utkalo	utkat	k5eAaPmAgNnS	utkat
několik	několik	k4yIc1	několik
silných	silný	k2eAgInPc2d1	silný
států	stát	k1gInPc2	stát
v	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Čínou	Čína	k1gFnSc7	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
dynastie	dynastie	k1gFnSc1	dynastie
Čchin	Čchin	k1gInSc1	Čchin
<g/>
,	,	kIx,	,
ovládající	ovládající	k2eAgInSc1d1	ovládající
region	region	k1gInSc1	region
kolem	kolem	k7c2	kolem
původní	původní	k2eAgFnSc2d1	původní
čouské	čouský	k2eAgFnSc2d1	čouský
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Čchin	Čchin	k1gInSc1	Čchin
přinesla	přinést	k5eAaPmAgFnS	přinést
sjednocení	sjednocení	k1gNnSc4	sjednocení
území	území	k1gNnPc2	území
pod	pod	k7c4	pod
silně	silně	k6eAd1	silně
centralizovanou	centralizovaný	k2eAgFnSc4d1	centralizovaná
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
220	[number]	k4	220
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
postaven	postavit	k5eAaPmNgInS	postavit
zavodňovací	zavodňovací	k2eAgInSc1d1	zavodňovací
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
reforma	reforma	k1gFnSc1	reforma
písma	písmo	k1gNnSc2	písmo
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
sjednoceny	sjednocen	k2eAgFnPc4d1	sjednocena
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
<g/>
.	.	kIx.	.
<g/>
Ze	z	k7c2	z
zmatků	zmatek	k1gInPc2	zmatek
následujících	následující	k2eAgInPc2d1	následující
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
říše	říš	k1gFnSc2	říš
Čchin	Čchina	k1gFnPc2	Čchina
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
říše	říše	k1gFnSc1	říše
Chan	Chana	k1gFnPc2	Chana
<g/>
,	,	kIx,	,
když	když	k8xS	když
Liou	Lioa	k1gFnSc4	Lioa
Pang	Panga	k1gFnPc2	Panga
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
protičchingských	protičchingský	k2eAgMnPc2d1	protičchingský
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
opětovně	opětovně	k6eAd1	opětovně
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
roku	rok	k1gInSc2	rok
202	[number]	k4	202
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Chan	Chana	k1gFnPc2	Chana
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgFnPc2d3	nejmocnější
dynastií	dynastie	k1gFnPc2	dynastie
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
čínské	čínský	k2eAgFnSc6d1	čínská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
220	[number]	k4	220
až	až	k8xS	až
206	[number]	k4	206
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Technické	technický	k2eAgFnPc1d1	technická
znalosti	znalost	k1gFnPc1	znalost
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
používání	používání	k1gNnSc4	používání
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
porcelánu	porcelán	k1gInSc2	porcelán
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Chan	Chana	k1gFnPc2	Chana
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
předchozí	předchozí	k2eAgFnSc4d1	předchozí
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
expanzi	expanze	k1gFnSc4	expanze
dynastie	dynastie	k1gFnSc2	dynastie
Čchin	Čchina	k1gFnPc2	Čchina
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
kontaktům	kontakt	k1gInPc3	kontakt
se	se	k3xPyFc4	se
Západem	západ	k1gInSc7	západ
<g/>
,	,	kIx,	,
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
1.	[number]	k4	1.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
se	se	k3xPyFc4	se
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
budhismus	budhismus	k1gInSc1	budhismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Tří	tři	k4xCgFnPc2	tři
říší	říš	k1gFnPc2	říš
(	(	kIx(	(
<g/>
220-265	[number]	k4	220-265
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
nejprve	nejprve	k6eAd1	nejprve
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
státy	stát	k1gInPc4	stát
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
roztříštěnost	roztříštěnost	k1gFnSc1	roztříštěnost
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
vymezeno	vymezit	k5eAaPmNgNnS	vymezit
rokem	rok	k1gInSc7	rok
220	[number]	k4	220
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
oficiálně	oficiálně	k6eAd1	oficiálně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
stát	stát	k1gInSc1	stát
Wej	Wej	k1gFnSc2	Wej
<g/>
,	,	kIx,	,
a	a	k8xC	a
rokem	rok	k1gInSc7	rok
280	[number]	k4	280
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
říše	říše	k1gFnSc1	říše
Ťin	Ťin	k1gFnSc2	Ťin
dobyla	dobýt	k5eAaPmAgFnS	dobýt
stát	stát	k5eAaPmF	stát
Wu	Wu	k1gFnSc1	Wu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Suej	Suej	k1gInSc1	Suej
<g/>
,	,	kIx,	,
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
6.	[number]	k4	6.
a	a	k8xC	a
7.	[number]	k4	7.
století	století	k1gNnPc2	století
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
sjednocení	sjednocení	k1gNnSc3	sjednocení
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
následující	následující	k2eAgFnSc2d1	následující
dynastie	dynastie	k1gFnSc2	dynastie
Tchang	Tchanga	k1gFnPc2	Tchanga
<g/>
,	,	kIx,	,
618	[number]	k4	618
až	až	k8xS	až
907	[number]	k4	907
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
<g/>
"	"	kIx"	"
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
stala	stát	k5eAaPmAgFnS	stát
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
státem	stát	k1gInSc7	stát
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vynalezen	vynalezen	k2eAgInSc1d1	vynalezen
knihtisk	knihtisk	k1gInSc1	knihtisk
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
Čína	Čína	k1gFnSc1	Čína
navázala	navázat	k5eAaPmAgFnS	navázat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
zeměmi	zem	k1gFnPc7	zem
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
907	[number]	k4	907
až	až	k9	až
960	[number]	k4	960
<g/>
,	,	kIx,	,
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
období	období	k1gNnSc1	období
Pěti	pět	k4xCc2	pět
dynastií	dynastie	k1gFnPc2	dynastie
a	a	k8xC	a
deseti	deset	k4xCc2	deset
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
válkám	válka	k1gFnPc3	válka
a	a	k8xC	a
fragmentaci	fragmentace	k1gFnSc3	fragmentace
ovládaných	ovládaný	k2eAgNnPc2d1	ovládané
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
vláda	vláda	k1gFnSc1	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Sung	Sunga	k1gFnPc2	Sunga
(	(	kIx(	(
<g/>
960-1279	[number]	k4	960-1279
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
naopak	naopak	k6eAd1	naopak
obdobím	období	k1gNnSc7	období
velkých	velký	k2eAgInPc2d1	velký
hospodářských	hospodářský	k2eAgInPc2d1	hospodářský
a	a	k8xC	a
kulturních	kulturní	k2eAgInPc2d1	kulturní
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
manufaktur	manufaktura	k1gFnPc2	manufaktura
<g/>
,	,	kIx,	,
směny	směna	k1gFnSc2	směna
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
významnému	významný	k2eAgInSc3d1	významný
rozvoji	rozvoj	k1gInSc3	rozvoj
peněžního	peněžní	k2eAgNnSc2d1	peněžní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
věd	věda	k1gFnPc2	věda
i	i	k8xC	i
farmakologie	farmakologie	k1gFnSc2	farmakologie
<g/>
.	.	kIx.	.
<g/>
Vládu	vláda	k1gFnSc4	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Sung	Sung	k1gInSc1	Sung
ukončily	ukončit	k5eAaPmAgInP	ukončit
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
čtvrtině	čtvrtina	k1gFnSc6	čtvrtina
13.	[number]	k4	13.
století	století	k1gNnSc2	století
nájezdy	nájezd	k1gInPc7	nájezd
Mongolů	mongol	k1gInPc2	mongol
<g/>
.	.	kIx.	.
</s>
<s>
Mongolové	Mongol	k1gMnPc1	Mongol
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
celou	celý	k2eAgFnSc4d1	celá
Čínu	Čína	k1gFnSc4	Čína
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
říše	říše	k1gFnSc1	říše
sahala	sahat	k5eAaImAgFnS	sahat
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
Asii	Asie	k1gFnSc4	Asie
až	až	k9	až
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
z	z	k7c2	z
vládců	vládce	k1gMnPc2	vládce
mongolské	mongolský	k2eAgFnSc2d1	mongolská
dynastie	dynastie	k1gFnSc2	dynastie
Jüan	jüan	k1gInSc1	jüan
byl	být	k5eAaImAgMnS	být
chán	chán	k1gMnSc1	chán
Kublaj	Kublaj	k1gMnSc1	Kublaj
(	(	kIx(	(
<g/>
známé	známá	k1gFnSc6	známá
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc4	jeho
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
benátským	benátský	k2eAgMnSc7d1	benátský
cestovatelem	cestovatel	k1gMnSc7	cestovatel
Markem	Marek	k1gMnSc7	Marek
Polem	polem	k6eAd1	polem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Krutovláda	krutovláda	k1gFnSc1	krutovláda
Mongolů	Mongol	k1gMnPc2	Mongol
vyvolávala	vyvolávat	k5eAaImAgFnS	vyvolávat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Ming	k1gInSc1	Ming
(	(	kIx(	(
<g/>
1368-1644	[number]	k4	1368-1644
<g/>
)	)	kIx)	)
byli	být	k5eAaImAgMnP	být
Mongolové	Mongol	k1gMnPc1	Mongol
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Minga	k1gFnPc2	Minga
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Peking	Peking	k1gInSc1	Peking
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
námořních	námořní	k2eAgInPc2d1	námořní
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
výprav	výprava	k1gFnPc2	výprava
<g/>
,	,	kIx,	,
stavbě	stavba	k1gFnSc3	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
zpracování	zpracování	k1gNnSc4	zpracování
železa	železo	k1gNnSc2	železo
nebo	nebo	k8xC	nebo
rozvoji	rozvoj	k1gInSc6	rozvoj
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16.	[number]	k4	16.
století	století	k1gNnSc2	století
začali	začít	k5eAaPmAgMnP	začít
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
pronikat	pronikat	k5eAaImF	pronikat
portugalští	portugalský	k2eAgMnPc1d1	portugalský
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
misionáři	misionář	k1gMnPc1	misionář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
vliv	vliv	k1gInSc1	vliv
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
výrazně	výrazně	k6eAd1	výrazně
omezen	omezit	k5eAaPmNgInS	omezit
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
města	město	k1gNnSc2	město
a	a	k8xC	a
přistavu	přistav	k1gInSc6	přistav
Macao	Macao	k1gNnSc1	Macao
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgFnSc7d1	poslední
dynastií	dynastie	k1gFnSc7	dynastie
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
byla	být	k5eAaImAgFnS	být
mandžuská	mandžuský	k2eAgFnSc1d1	mandžuská
dynastie	dynastie	k1gFnSc1	dynastie
Čching	Čching	k1gInSc1	Čching
<g/>
.	.	kIx.	.
</s>
<s>
Vládla	vládnout	k5eAaImAgFnS	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1644	[number]	k4	1644
až	až	k9	až
1911	[number]	k4	1911
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
císaři	císař	k1gMnPc1	císař
této	tento	k3xDgFnSc2	tento
dynastie	dynastie	k1gFnSc2	dynastie
obnovili	obnovit	k5eAaPmAgMnP	obnovit
moc	moc	k6eAd1	moc
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
říše	říš	k1gFnSc2	říš
jako	jako	k9	jako
bývaly	bývat	k5eAaImAgFnP	bývat
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Tchang	Tchanga	k1gFnPc2	Tchanga
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
říše	říše	k1gFnSc1	říše
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
a	a	k8xC	a
Tibet	Tibet	k1gInSc1	Tibet
<g/>
.	.	kIx.	.
</s>
<s>
Císaři	císař	k1gMnPc1	císař
dynastie	dynastie	k1gFnSc2	dynastie
Čhing	Čhinga	k1gFnPc2	Čhinga
podporovali	podporovat	k5eAaImAgMnP	podporovat
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
období	období	k1gNnSc1	období
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
životní	životní	k2eAgFnSc4d1	životní
úroveň	úroveň	k1gFnSc4	úroveň
obyvatel	obyvatel	k1gMnPc2	obyvatel
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zakončeno	zakončen	k2eAgNnSc1d1	zakončeno
tzv.	tzv.	kA	tzv.
Opiovou	opiový	k2eAgFnSc7d1	opiová
válkou	válka	k1gFnSc7	válka
(	(	kIx(	(
<g/>
1839-1842	[number]	k4	1839-1842
<g/>
)	)	kIx)	)
s	s	k7c7	s
Angličany	Angličan	k1gMnPc7	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
musela	muset	k5eAaImAgFnS	muset
Čína	Čína	k1gFnSc1	Čína
postoupit	postoupit	k5eAaPmF	postoupit
Spojenému	spojený	k2eAgNnSc3d1	spojené
království	království	k1gNnSc3	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Irska	Irsko	k1gNnSc2	Irsko
město	město	k1gNnSc1	město
Hongkong	Hongkong	k1gInSc1	Hongkong
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
britskou	britský	k2eAgFnSc7d1	britská
korunní	korunní	k2eAgFnSc7d1	korunní
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
další	další	k2eAgInPc1d1	další
dvě	dva	k4xCgFnPc1	dva
opiové	opiový	k2eAgFnPc1d1	opiová
války	válka	k1gFnPc1	válka
a	a	k8xC	a
pokles	pokles	k1gInSc1	pokles
životní	životní	k2eAgFnSc2d1	životní
úrovně	úroveň	k1gFnSc2	úroveň
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19.	[number]	k4	19.
století	století	k1gNnPc2	století
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vojenského	vojenský	k2eAgInSc2d1	vojenský
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
protimandžuských	protimandžuský	k2eAgNnPc6d1	protimandžuský
povstáních	povstání	k1gNnPc6	povstání
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
dynastie	dynastie	k1gFnSc2	dynastie
Čching	Čching	k1gInSc4	Čching
nucena	nucen	k2eAgFnSc1d1	nucena
abdikovat	abdikovat	k5eAaBmF	abdikovat
a	a	k8xC	a
29.	[number]	k4	29.
prosince	prosinec	k1gInSc2	prosinec
1911	[number]	k4	1911
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Čínsko-japonská	čínskoaponský	k2eAgFnSc1d1	čínsko-japonská
válka	válka	k1gFnSc1	válka
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Mukdenského	Mukdenský	k2eAgInSc2d1	Mukdenský
Incidentu	incident	k1gInSc2	incident
obsadila	obsadit	k5eAaPmAgFnS	obsadit
japonská	japonský	k2eAgFnSc1d1	japonská
armáda	armáda	k1gFnSc1	armáda
Mandžusko	Mandžusko	k1gNnSc1	Mandžusko
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
zde	zde	k6eAd1	zde
loutkový	loutkový	k2eAgInSc4d1	loutkový
stát	stát	k1gInSc4	stát
Mandžukuo	Mandžukuo	k1gMnSc1	Mandžukuo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
odrazovým	odrazový	k2eAgInSc7d1	odrazový
můstkem	můstek	k1gInSc7	můstek
k	k	k7c3	k
japonské	japonský	k2eAgFnSc3d1	japonská
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
ostatní	ostatní	k2eAgFnSc2d1	ostatní
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Incident	incident	k1gInSc1	incident
na	na	k7c6	na
mostě	most	k1gInSc6	most
Marca	Marc	k2eAgFnSc1d1	Marca
Pola	pola	k1gFnSc1	pola
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
uměle	uměle	k6eAd1	uměle
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
Japonci	Japonec	k1gMnSc3	Japonec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
záminkou	záminka	k1gFnSc7	záminka
k	k	k7c3	k
rozpoutání	rozpoutání	k1gNnSc3	rozpoutání
8	[number]	k4	8
let	léto	k1gNnPc2	léto
trvající	trvající	k2eAgMnSc1d1	trvající
druhé	druhý	k4xOgFnSc2	druhý
čínsko-japonské	čínskoaponský	k2eAgFnSc2d1	čínsko-japonská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
2.	[number]	k4	2.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
dopouštěla	dopouštět	k5eAaImAgFnS	dopouštět
mnoha	mnoho	k4c2	mnoho
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
vešel	vejít	k5eAaPmAgInS	vejít
ve	v	k7c4	v
známost	známost	k1gFnSc4	známost
Nankingský	nankingský	k2eAgInSc4d1	nankingský
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
japonští	japonský	k2eAgMnPc1d1	japonský
vojáci	voják	k1gMnPc1	voják
zmasakrovali	zmasakrovat	k5eAaPmAgMnP	zmasakrovat
na	na	k7c4	na
300 000	[number]	k4	300 000
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
válečných	válečný	k2eAgMnPc2d1	válečný
zajatců	zajatec	k1gMnPc2	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
japonská	japonský	k2eAgFnSc1d1	japonská
jednotka	jednotka	k1gFnSc1	jednotka
731	[number]	k4	731
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
Mandžusku	Mandžusko	k1gNnSc6	Mandžusko
<g/>
,	,	kIx,	,
prováděla	provádět	k5eAaImAgFnS	provádět
pokusy	pokus	k1gInPc4	pokus
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Japonci	Japonec	k1gMnPc1	Japonec
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
testovali	testovat	k5eAaImAgMnP	testovat
biologické	biologický	k2eAgFnPc4d1	biologická
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
antrax	antrax	k1gInSc1	antrax
nebo	nebo	k8xC	nebo
dýmějový	dýmějový	k2eAgInSc1d1	dýmějový
mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
nakazily	nakazit	k5eAaPmAgFnP	nakazit
statisíce	statisíce	k1gInPc4	statisíce
Číňanů	Číňan	k1gMnPc2	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
si	se	k3xPyFc3	se
druhá	druhý	k4xOgFnSc1	druhý
čínsko-japonská	čínskoaponský	k2eAgFnSc1d1	čínsko-japonská
válka	válka	k1gFnSc1	válka
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
přes	přes	k7c4	přes
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Čínská	čínský	k2eAgFnSc1d1	čínská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
znovu	znovu	k6eAd1	znovu
propukla	propuknout	k5eAaPmAgFnS	propuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
Komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
Číny	Čína	k1gFnSc2	Čína
(	(	kIx(	(
<g/>
Kungčchantang	Kungčchantang	k1gInSc1	Kungčchantang
<g/>
)	)	kIx)	)
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
vládnoucími	vládnoucí	k2eAgMnPc7d1	vládnoucí
nacionalisty	nacionalista	k1gMnPc7	nacionalista
(	(	kIx(	(
<g/>
Kuomintang	Kuomintang	k1gInSc1	Kuomintang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
vítězstvím	vítězství	k1gNnSc7	vítězství
komunistů	komunista	k1gMnPc2	komunista
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
převzali	převzít	k5eAaPmAgMnP	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
celou	celý	k2eAgFnSc7d1	celá
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
Čínou	Čína	k1gFnSc7	Čína
včetně	včetně	k7c2	včetně
ostrova	ostrov	k1gInSc2	ostrov
Chaj-nan	Chajany	k1gInPc2	Chaj-nany
<g/>
.	.	kIx.	.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1	příslušník
Kuomintangu	Kuomintang	k1gInSc2	Kuomintang
byli	být	k5eAaImAgMnP	být
donuceni	donucen	k2eAgMnPc1d1	donucen
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
a	a	k8xC	a
několik	několik	k4yIc1	několik
menších	malý	k2eAgInPc2d2	menší
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
1.	[number]	k4	1.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
na	na	k7c6	na
pekingském	pekingský	k2eAgNnSc6d1	pekingské
náměstí	náměstí	k1gNnSc6	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Číňané	Číňan	k1gMnPc1	Číňan
povstali	povstat	k5eAaPmAgMnP	povstat
<g/>
"	"	kIx"	"
Čínskou	čínský	k2eAgFnSc4d1	čínská
lidovou	lidový	k2eAgFnSc4d1	lidová
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Vláda	vláda	k1gFnSc1	vláda
Mao	Mao	k1gFnSc1	Mao
Ce-tunga	Ceunga	k1gFnSc1	Ce-tunga
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
vlády	vláda	k1gFnSc2	vláda
během	během	k7c2	během
života	život	k1gInSc2	život
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
dodnes	dodnes	k6eAd1	dodnes
vzbuzuje	vzbuzovat	k5eAaImIp3nS	vzbuzovat
řadu	řada	k1gFnSc4	řada
kontroverzí	kontroverze	k1gFnPc2	kontroverze
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
nesmiřitelné	smiřitelný	k2eNgFnSc3d1	nesmiřitelná
politice	politika	k1gFnSc3	politika
vůči	vůči	k7c3	vůči
odpůrcům	odpůrce	k1gMnPc3	odpůrce
<g/>
,	,	kIx,	,
mnohým	mnohý	k2eAgMnPc3d1	mnohý
omylům	omyl	k1gInPc3	omyl
a	a	k8xC	a
až	až	k9	až
zločinným	zločinný	k2eAgInPc3d1	zločinný
důsledkům	důsledek	k1gInPc3	důsledek
na	na	k7c6	na
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
způsobených	způsobený	k2eAgFnPc2d1	způsobená
mj.	mj.	kA	mj.
fanatickým	fanatický	k2eAgInSc7d1	fanatický
vykládáním	vykládání	k1gNnSc7	vykládání
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
<g/>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
okamžitě	okamžitě	k6eAd1	okamžitě
uzákonila	uzákonit	k5eAaPmAgFnS	uzákonit
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
znárodnila	znárodnit	k5eAaPmAgFnS	znárodnit
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
započala	započnout	k5eAaPmAgFnS	započnout
s	s	k7c7	s
vyhlašováním	vyhlašování	k1gNnSc7	vyhlašování
kampaní	kampaň	k1gFnPc2	kampaň
(	(	kIx(	(
<g/>
programy	program	k1gInPc1	program
gramotnosti	gramotnost	k1gFnSc2	gramotnost
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc1	očkování
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
kvality	kvalita	k1gFnSc2	kvalita
zemědělství	zemědělství	k1gNnSc2	zemědělství
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
ČLR	ČLR	kA	ČLR
také	také	k9	také
formálně	formálně	k6eAd1	formálně
anektovala	anektovat	k5eAaBmAgFnS	anektovat
Východní	východní	k2eAgInSc4d1	východní
Turkestán	Turkestán	k1gInSc4	Turkestán
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
Sin-ťiang	Sin-ťianga	k1gFnPc2	Sin-ťianga
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
i	i	k9	i
Tibet	Tibet	k1gInSc1	Tibet
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Tibetská	tibetský	k2eAgFnSc1d1	tibetská
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dokončila	dokončit	k5eAaPmAgFnS	dokončit
opětovné	opětovný	k2eAgNnSc4d1	opětovné
sjednocení	sjednocení	k1gNnSc4	sjednocení
území	území	k1gNnSc2	území
připojením	připojení	k1gNnSc7	připojení
všech	všecek	k3xTgInPc2	všecek
separatistických	separatistický	k2eAgInPc2d1	separatistický
států	stát	k1gInPc2	stát
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
ČLR	ČLR	kA	ČLR
se	se	k3xPyFc4	se
také	také	k9	také
zapojila	zapojit	k5eAaPmAgNnP	zapojit
do	do	k7c2	do
korejské	korejský	k2eAgFnSc2d1	Korejská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
komunistického	komunistický	k2eAgInSc2d1	komunistický
bloku	blok	k1gInSc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
pod	pod	k7c7	pod
heslem	heslo	k1gNnSc7	heslo
"	"	kIx"	"
<g/>
Ať	ať	k8xS	ať
rozkvétá	rozkvétat	k5eAaImIp3nS	rozkvétat
sto	sto	k4xCgNnSc4	sto
květů	květ	k1gInPc2	květ
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
soupeří	soupeřit	k5eAaImIp3nS	soupeřit
sto	sto	k4xCgNnSc1	sto
škol	škola	k1gFnPc2	škola
učení	učení	k1gNnSc2	učení
<g/>
"	"	kIx"	"
kampaň	kampaň	k1gFnSc1	kampaň
Sta	sto	k4xCgNnPc1	sto
květů	květ	k1gInPc2	květ
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
rámci	rámec	k1gInSc6	rámec
byli	být	k5eAaImAgMnP	být
intelektuálové	intelektuál	k1gMnPc1	intelektuál
vyzváni	vyzvat	k5eAaPmNgMnP	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dali	dát	k5eAaPmAgMnP	dát
kritikou	kritika	k1gFnSc7	kritika
podněty	podnět	k1gInPc4	podnět
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
zlepšení	zlepšení	k1gNnSc4	zlepšení
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
ale	ale	k9	ale
kampaň	kampaň	k1gFnSc1	kampaň
postupně	postupně	k6eAd1	postupně
zvrhla	zvrhnout	k5eAaPmAgFnS	zvrhnout
v	v	k7c4	v
kritiku	kritika	k1gFnSc4	kritika
vládního	vládní	k2eAgInSc2d1	vládní
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
kampaň	kampaň	k1gFnSc4	kampaň
proti	proti	k7c3	proti
pravičákům	pravičák	k1gMnPc3	pravičák
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
statisíce	statisíce	k1gInPc4	statisíce
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
nebo	nebo	k8xC	nebo
k	k	k7c3	k
převýchově	převýchova	k1gFnSc3	převýchova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pokusu	pokus	k1gInSc6	pokus
dohnat	dohnat	k5eAaPmF	dohnat
a	a	k8xC	a
předehnat	předehnat	k5eAaPmF	předehnat
Západ	západ	k1gInSc4	západ
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlášen	k2eAgInSc4d1	vyhlášen
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgInSc4d1	velký
skok	skok	k1gInSc4	skok
vpřed	vpřed	k6eAd1	vpřed
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
ale	ale	k9	ale
skončil	skončit	k5eAaPmAgInS	skončit
katastrofou	katastrofa	k1gFnSc7	katastrofa
–	–	k?	–
na	na	k7c4	na
hladomor	hladomor	k1gInSc4	hladomor
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
dokonce	dokonce	k9	dokonce
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
z	z	k7c2	z
čela	čelo	k1gNnSc2	čelo
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
odstoupil	odstoupit	k5eAaPmAgInS	odstoupit
Mao	Mao	k1gFnSc4	Mao
Ce-tung	Ceunga	k1gFnPc2	Ce-tunga
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
udržoval	udržovat	k5eAaImAgInS	udržovat
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Liou	Lioa	k1gMnSc4	Lioa
Šao-čchi	Šao-čch	k1gFnSc2	Šao-čch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ideologické	ideologický	k2eAgFnSc2d1	ideologická
roztržky	roztržka	k1gFnSc2	roztržka
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
ČLR	ČLR	kA	ČLR
odkázána	odkázat	k5eAaPmNgFnS	odkázat
pouze	pouze	k6eAd1	pouze
sama	sám	k3xTgFnSc1	sám
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
jaderná	jaderný	k2eAgFnSc1d1	jaderná
zkouška	zkouška	k1gFnSc1	zkouška
a	a	k8xC	a
ČLR	ČLR	kA	ČLR
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
jaderné	jaderný	k2eAgFnPc4d1	jaderná
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
revoluce	revoluce	k1gFnPc1	revoluce
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gInSc1	Ce-tung
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
mezi	mezi	k7c7	mezi
rolníky	rolník	k1gMnPc7	rolník
přes	přes	k7c4	přes
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
reformu	reforma	k1gFnSc4	reforma
a	a	k8xC	a
také	také	k9	také
díky	díky	k7c3	díky
popravám	poprava	k1gFnPc3	poprava
mezi	mezi	k7c7	mezi
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
hospodářů	hospodář	k1gMnPc2	hospodář
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
populace	populace	k1gFnSc1	populace
se	se	k3xPyFc4	se
z	z	k7c2	z
550	[number]	k4	550
milionů	milion	k4xCgInPc2	milion
téměř	téměř	k6eAd1	téměř
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
Mao	Mao	k1gFnSc7	Mao
Ce-tungův	Ceungův	k2eAgInSc1d1	Ce-tungův
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
projekt	projekt	k1gInSc1	projekt
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Velký	velký	k2eAgInSc1d1	velký
skok	skok	k1gInSc1	skok
vpřed	vpřed	k6eAd1	vpřed
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1958	[number]	k4	1958
a	a	k8xC	a
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
okolo	okolo	k7c2	okolo
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
úmrtí	úmrtí	k1gNnPc2	úmrtí
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
následkem	následkem	k7c2	následkem
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Velkou	velký	k2eAgFnSc4d1	velká
proletářskou	proletářský	k2eAgFnSc4d1	proletářská
kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
myšlence	myšlenka	k1gFnSc6	myšlenka
nepřetržité	přetržitý	k2eNgFnSc2d1	nepřetržitá
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
masové	masový	k2eAgFnSc2d1	masová
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vymýtit	vymýtit	k5eAaPmF	vymýtit
kapitalismus	kapitalismus	k1gInSc4	kapitalismus
uvnitř	uvnitř	k7c2	uvnitř
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
Maovým	Maový	k2eAgInSc7d1	Maový
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
revoluce	revoluce	k1gFnSc2	revoluce
však	však	k9	však
patrně	patrně	k6eAd1	patrně
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
vedení	vedení	k1gNnSc6	vedení
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgInS	chtít
zbavit	zbavit	k5eAaPmF	zbavit
svých	svůj	k3xOyFgMnPc2	svůj
ideologických	ideologický	k2eAgMnPc2d1	ideologický
odpůrců	odpůrce	k1gMnPc2	odpůrce
a	a	k8xC	a
osobních	osobní	k2eAgMnPc2d1	osobní
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
proti	proti	k7c3	proti
čtyřem	čtyři	k4xCgInPc3	čtyři
přežitkům	přežitek	k1gInPc3	přežitek
–	–	k?	–
starému	starý	k2eAgNnSc3d1	staré
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
obyčejům	obyčej	k1gInPc3	obyčej
a	a	k8xC	a
návykům	návyk	k1gInPc3	návyk
<g/>
.	.	kIx.	.
</s>
<s>
Hnací	hnací	k2eAgFnSc7d1	hnací
silou	síla	k1gFnSc7	síla
Maovy	Maova	k1gFnSc2	Maova
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
mládež	mládež	k1gFnSc1	mládež
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
tzv.	tzv.	kA	tzv.
rudé	rudý	k2eAgFnSc2d1	rudá
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oběťmi	oběť	k1gFnPc7	oběť
kampaně	kampaň	k1gFnSc2	kampaň
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
Liou	Lioum	k1gNnSc3	Lioum
Šao-čchi	Šao-čchi	k1gNnSc2	Šao-čchi
a	a	k8xC	a
Teng	Teng	k1gMnSc1	Teng
Siao-pching	Siaoching	k1gInSc1	Siao-pching
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
diplomaté	diplomat	k1gMnPc1	diplomat
a	a	k8xC	a
veškeré	veškerý	k3xTgFnSc2	veškerý
autority	autorita	k1gFnSc2	autorita
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obrovským	obrovský	k2eAgFnPc3d1	obrovská
škodám	škoda	k1gFnPc3	škoda
na	na	k7c6	na
kulturním	kulturní	k2eAgNnSc6d1	kulturní
dědictví	dědictví	k1gNnSc6	dědictví
(	(	kIx(	(
<g/>
byly	být	k5eAaImAgFnP	být
páleny	pálen	k2eAgFnPc1d1	pálena
knihy	kniha	k1gFnPc1	kniha
<g/>
,	,	kIx,	,
vypleněna	vypleněn	k2eAgNnPc1d1	vypleněno
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
ničeny	ničen	k2eAgFnPc1d1	ničena
historické	historický	k2eAgFnPc1d1	historická
budovy	budova	k1gFnPc1	budova
<g/>
)	)	kIx)	)
a	a	k8xC	a
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
zmítala	zmítat	k5eAaImAgFnS	zmítat
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Kampaň	kampaň	k1gFnSc1	kampaň
trvala	trvat	k5eAaImAgFnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInPc1	její
výsledky	výsledek	k1gInPc1	výsledek
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
do	do	k7c2	do
Maovy	Maova	k1gFnSc2	Maova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
po	po	k7c6	po
31	[number]	k4	31
měsících	měsíc	k1gInPc6	měsíc
intenzivního	intenzivní	k2eAgNnSc2d1	intenzivní
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
,	,	kIx,	,
ověřování	ověřování	k1gNnSc2	ověřování
a	a	k8xC	a
přehodnocování	přehodnocování	k1gNnSc2	přehodnocování
Ústředním	ústřední	k2eAgInSc7d1	ústřední
výborem	výbor	k1gInSc7	výbor
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
počty	počet	k1gInPc4	počet
vztahující	vztahující	k2eAgInPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
kulturní	kulturní	k2eAgFnSc3d1	kulturní
revoluci	revoluce	k1gFnSc3	revoluce
tyto	tento	k3xDgMnPc4	tento
<g/>
:	:	kIx,	:
4,2	[number]	k4	4,2
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zatčeno	zatknout	k5eAaPmNgNnS	zatknout
a	a	k8xC	a
vyšetřováno	vyšetřovat	k5eAaImNgNnS	vyšetřovat
<g/>
,	,	kIx,	,
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
nepřirozenou	přirozený	k2eNgFnSc7d1	nepřirozená
smrtí	smrt	k1gFnSc7	smrt
<g/>
,	,	kIx,	,
135 000	[number]	k4	135 000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
kontrarevolucionáře	kontrarevolucionář	k1gMnPc4	kontrarevolucionář
a	a	k8xC	a
popraveno	popraven	k2eAgNnSc4d1	popraveno
<g/>
,	,	kIx,	,
237 000	[number]	k4	237 000
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
<g/>
,	,	kIx,	,
7,03	[number]	k4	7,03
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
nebo	nebo	k8xC	nebo
zmrzačeno	zmrzačit	k5eAaPmNgNnS	zmrzačit
při	při	k7c6	při
ozbrojených	ozbrojený	k2eAgInPc6d1	ozbrojený
útocích	útok	k1gInPc6	útok
a	a	k8xC	a
71 200	[number]	k4	71 200
rodin	rodina	k1gFnPc2	rodina
bylo	být	k5eAaImAgNnS	být
rozbito	rozbít	k5eAaPmNgNnS	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
Statistiky	statistika	k1gFnPc1	statistika
sestavené	sestavený	k2eAgFnPc1d1	sestavená
ze	z	k7c2	z
zdrojů	zdroj	k1gInPc2	zdroj
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
krajů	kraj	k1gInPc2	kraj
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
revoluce	revoluce	k1gFnSc2	revoluce
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
7,73	[number]	k4	7,73
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
nepřirozenou	přirozený	k2eNgFnSc7d1	nepřirozená
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
<g/>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mao	Mao	k1gMnSc1	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
smrt	smrt	k1gFnSc4	smrt
40	[number]	k4	40
až	až	k9	až
70	[number]	k4	70
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
nucených	nucený	k2eAgFnPc2d1	nucená
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
poprav	poprava	k1gFnPc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
50.	[number]	k4	50.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgMnS	být
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
čínské	čínský	k2eAgFnSc6d1	čínská
vesnici	vesnice	k1gFnSc6	vesnice
určen	určit	k5eAaPmNgMnS	určit
pro	pro	k7c4	pro
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
popravu	poprava	k1gFnSc4	poprava
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgMnSc1	jeden
statkář	statkář	k1gMnSc1	statkář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obvykle	obvykle	k6eAd1	obvykle
několik	několik	k4yIc1	několik
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
odhadem	odhad	k1gInSc7	odhad
2	[number]	k4	2
až	až	k8xS	až
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Liberalizace	liberalizace	k1gFnSc2	liberalizace
ekonomiky	ekonomika	k1gFnSc2	ekonomika
====	====	k?	====
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
revoluce	revoluce	k1gFnSc1	revoluce
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
smrtí	smrt	k1gFnSc7	smrt
Mao	Mao	k1gMnSc2	Mao
Ce-tunga	Ceung	k1gMnSc2	Ce-tung
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následném	následný	k2eAgInSc6d1	následný
boji	boj	k1gInSc6	boj
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
neuspěla	uspět	k5eNaPmAgFnS	uspět
tzv.	tzv.	kA	tzv.
banda	banda	k1gFnSc1	banda
čtyř	čtyři	k4xCgMnPc2	čtyři
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
kulturní	kulturní	k2eAgFnSc4d1	kulturní
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
obviněna	obvinit	k5eAaPmNgFnS	obvinit
z	z	k7c2	z
uvedení	uvedení	k1gNnSc2	uvedení
země	zem	k1gFnSc2	zem
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
odsouzeni	odsouzet	k5eAaImNgMnP	odsouzet
k	k	k7c3	k
vysokým	vysoký	k2eAgInPc3d1	vysoký
trestům	trest	k1gInPc3	trest
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
proreformní	proreformní	k2eAgNnSc1d1	proreformní
křídlo	křídlo	k1gNnSc1	křídlo
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
reprezentované	reprezentovaný	k2eAgFnSc2d1	reprezentovaná
Teng	Teng	k1gMnSc1	Teng
Siao-pchingem	Siaoching	k1gInSc7	Siao-pching
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
postupně	postupně	k6eAd1	postupně
převzal	převzít	k5eAaPmAgInS	převzít
veškerou	veškerý	k3xTgFnSc4	veškerý
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oteplování	oteplování	k1gNnSc3	oteplování
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
mnohé	mnohý	k2eAgFnPc1d1	mnohá
reformy	reforma	k1gFnPc1	reforma
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
oblasti	oblast	k1gFnSc6	oblast
země	zem	k1gFnSc2	zem
postupně	postupně	k6eAd1	postupně
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
více	hodně	k6eAd2	hodně
Západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
Zřízeny	zřízen	k2eAgFnPc1d1	zřízena
byly	být	k5eAaImAgFnP	být
například	například	k6eAd1	například
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
zóny	zóna	k1gFnPc1	zóna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
podnikání	podnikání	k1gNnSc1	podnikání
západním	západní	k2eAgMnSc7d1	západní
investorům	investor	k1gMnPc3	investor
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
zavedena	zaveden	k2eAgFnSc1d1	zavedena
politika	politika	k1gFnSc1	politika
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
přinesla	přinést	k5eAaPmAgFnS	přinést
mnohé	mnohý	k2eAgInPc4d1	mnohý
negativní	negativní	k2eAgInPc4d1	negativní
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nepoměru	nepoměr	k1gInSc2	nepoměr
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
<g/>
,	,	kIx,	,
stárnutí	stárnutí	k1gNnSc1	stárnutí
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
úbytku	úbytek	k1gInSc3	úbytek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=====	=====	k?	=====
Studentské	studentský	k2eAgInPc1d1	studentský
protesty	protest	k1gInPc1	protest
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Tchien-an-men	Tchiennen	k1gInSc1	Tchien-an-men
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
narůstající	narůstající	k2eAgFnSc6d1	narůstající
nespokojenosti	nespokojenost	k1gFnSc6	nespokojenost
k	k	k7c3	k
protestům	protest	k1gInPc3	protest
studentů	student	k1gMnPc2	student
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
KS	ks	kA	ks
Číny	Čína	k1gFnSc2	Čína
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Nebeského	nebeský	k2eAgInSc2d1	nebeský
klidu	klid	k1gInSc2	klid
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
tvrdě	tvrdě	k6eAd1	tvrdě
potlačeny	potlačit	k5eAaPmNgFnP	potlačit
zásahem	zásah	k1gInSc7	zásah
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
uvrhly	uvrhnout	k5eAaPmAgFnP	uvrhnout
ČLR	ČLR	kA	ČLR
do	do	k7c2	do
přechodné	přechodný	k2eAgFnSc2d1	přechodná
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
demokratizaci	demokratizace	k1gFnSc3	demokratizace
a	a	k8xC	a
zavedení	zavedení	k1gNnSc3	zavedení
pluralitního	pluralitní	k2eAgInSc2d1	pluralitní
systému	systém	k1gInSc2	systém
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
komunistické	komunistický	k2eAgNnSc1d1	komunistické
vedení	vedení	k1gNnSc1	vedení
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
však	však	k9	však
generačně	generačně	k6eAd1	generačně
obměnilo	obměnit	k5eAaPmAgNnS	obměnit
a	a	k8xC	a
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
nadále	nadále	k6eAd1	nadále
v	v	k7c6	v
hospodářských	hospodářský	k2eAgFnPc6d1	hospodářská
reformách	reforma	k1gFnPc6	reforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
90.	[number]	k4	90.
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
1.	[number]	k4	1.
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
byl	být	k5eAaImAgMnS	být
k	k	k7c3	k
Čínské	čínský	k2eAgFnSc3d1	čínská
lidové	lidový	k2eAgFnSc3d1	lidová
republice	republika	k1gFnSc3	republika
připojen	připojen	k2eAgInSc1d1	připojen
Hong	Hong	k1gInSc1	Hong
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
"	"	kIx"	"
<g/>
jeden	jeden	k4xCgInSc1	jeden
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
systémy	systém	k1gInPc1	systém
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
20.	[number]	k4	20.
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
stejnými	stejný	k2eAgFnPc7d1	stejná
podmínkami	podmínka	k1gFnPc7	podmínka
připojeno	připojit	k5eAaPmNgNnS	připojit
Macao	Macao	k1gNnSc1	Macao
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
výdajích	výdaj	k1gInPc6	výdaj
na	na	k7c4	na
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
Čína	Čína	k1gFnSc1	Čína
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
za	za	k7c4	za
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
začala	začít	k5eAaPmAgFnS	začít
modernizovat	modernizovat	k5eAaBmF	modernizovat
všechny	všechen	k3xTgFnPc4	všechen
složky	složka	k1gFnPc4	složka
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
administrativy	administrativa	k1gFnSc2	administrativa
Chu	Chu	k1gFnSc1	Chu
Ťin-tchaa	Ťinchaa	k1gFnSc1	Ťin-tchaa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vládla	vládnout	k5eAaImAgFnS	vládnout
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
řešeny	řešen	k2eAgInPc4d1	řešen
problémy	problém	k1gInPc4	problém
zejména	zejména	k9	zejména
v	v	k7c6	v
sociální	sociální	k2eAgFnSc6d1	sociální
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
koncept	koncept	k1gInSc4	koncept
s	s	k7c7	s
názvem	název	k1gInSc7	název
Harmonická	harmonický	k2eAgFnSc1d1	harmonická
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slibuje	slibovat	k5eAaImIp3nS	slibovat
fúzi	fúze	k1gFnSc4	fúze
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
podporu	podpora	k1gFnSc4	podpora
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
prvky	prvek	k1gInPc4	prvek
nového	nový	k2eAgNnSc2d1	nové
konfuciánství	konfuciánství	k1gNnSc2	konfuciánství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
administrativa	administrativa	k1gFnSc1	administrativa
byla	být	k5eAaImAgFnS	být
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ČLR	ČLR	kA	ČLR
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
otevřeně	otevřeně	k6eAd1	otevřeně
podporovat	podporovat	k5eAaImF	podporovat
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
buddhismus	buddhismus	k1gInSc1	buddhismus
a	a	k8xC	a
taoismus	taoismus	k1gInSc1	taoismus
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
tradiční	tradiční	k2eAgFnSc4d1	tradiční
součást	součást	k1gFnSc4	součást
čínské	čínský	k2eAgFnSc2d1	čínská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
ovšem	ovšem	k9	ovšem
označují	označovat	k5eAaImIp3nP	označovat
aktivitu	aktivita	k1gFnSc4	aktivita
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
za	za	k7c4	za
další	další	k2eAgInSc4d1	další
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
namalování	namalování	k1gNnSc4	namalování
falešného	falešný	k2eAgInSc2d1	falešný
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neustále	neustále	k6eAd1	neustále
probíhají	probíhat	k5eAaImIp3nP	probíhat
masové	masový	k2eAgFnPc4d1	masová
represe	represe	k1gFnPc4	represe
<g/>
,	,	kIx,	,
cenzura	cenzura	k1gFnSc1	cenzura
informací	informace	k1gFnPc2	informace
a	a	k8xC	a
zabíjení	zabíjení	k1gNnSc2	zabíjení
jako	jako	k9	jako
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Pokračují	pokračovat	k5eAaImIp3nP	pokračovat
také	také	k9	také
restrikce	restrikce	k1gFnPc1	restrikce
proti	proti	k7c3	proti
určitým	určitý	k2eAgFnPc3d1	určitá
náboženským	náboženský	k2eAgFnPc3d1	náboženská
skupinám	skupina	k1gFnPc3	skupina
proti	proti	k7c3	proti
nimž	jenž	k3xRgMnPc3	jenž
trvá	trvat	k5eAaImIp3nS	trvat
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
"	"	kIx"	"
<g/>
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
zlým	zlý	k2eAgInPc3d1	zlý
kultům	kult	k1gInPc3	kult
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Strana	strana	k1gFnSc1	strana
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
zasažena	zasažen	k2eAgNnPc1d1	zasaženo
jsou	být	k5eAaImIp3nP	být
veškerá	veškerý	k3xTgNnPc1	veškerý
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
nepodřizují	podřizovat	k5eNaImIp3nP	podřizovat
Straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
křesťané	křesťan	k1gMnPc1	křesťan
loajální	loajální	k2eAgFnSc2d1	loajální
papeži	papež	k1gMnSc3	papež
<g/>
,	,	kIx,	,
buddhisté	buddhista	k1gMnPc1	buddhista
loajální	loajální	k2eAgFnSc2d1	loajální
dalajlámovi	dalajláma	k1gMnSc3	dalajláma
<g/>
,	,	kIx,	,
Fa-lun-kung	Faunung	k1gInSc1	Fa-lun-kung
loajální	loajální	k2eAgInSc1d1	loajální
k	k	k7c3	k
zakladateli	zakladatel	k1gMnSc6	zakladatel
Li	li	k8xS	li
Chung-č	Chung-č	k1gInSc4	Chung-č
<g/>
'	'	kIx"	'
<g/>
ovi	ovi	k?	ovi
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
také	také	k9	také
sporná	sporný	k2eAgFnSc1d1	sporná
otázka	otázka	k1gFnSc1	otázka
Tibetu	Tibet	k1gInSc2	Tibet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
Tibetu	Tibet	k1gInSc6	Tibet
krvavě	krvavě	k6eAd1	krvavě
potlačeny	potlačen	k2eAgFnPc1d1	potlačena
údajné	údajný	k2eAgFnPc1d1	údajná
protičínské	protičínský	k2eAgFnPc1d1	protičínská
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
zdroje	zdroj	k1gInPc1	zdroj
poukazovaly	poukazovat	k5eAaImAgInP	poukazovat
na	na	k7c4	na
angažování	angažování	k1gNnSc4	angažování
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
vyprovokování	vyprovokování	k1gNnSc6	vyprovokování
a	a	k8xC	a
řízení	řízení	k1gNnSc6	řízení
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Exilový	exilový	k2eAgMnSc1d1	exilový
tibetský	tibetský	k2eAgMnSc1d1	tibetský
duchovní	duchovní	k1gMnSc1	duchovní
vůdce	vůdce	k1gMnSc1	vůdce
dalajláma	dalajláma	k1gMnSc1	dalajláma
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzpoura	vzpoura	k1gFnSc1	vzpoura
ve	v	k7c6	v
Lhase	Lhasa	k1gFnSc6	Lhasa
byla	být	k5eAaImAgFnS	být
zinscenována	zinscenovat	k5eAaPmNgFnS	zinscenovat
čínskou	čínský	k2eAgFnSc7d1	čínská
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
<g/>
Režimem	režim	k1gInSc7	režim
propagovaná	propagovaný	k2eAgFnSc1d1	propagovaná
"	"	kIx"	"
<g/>
harmonická	harmonický	k2eAgFnSc1d1	harmonická
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
skutečným	skutečný	k2eAgFnPc3d1	skutečná
událostem	událost	k1gFnPc3	událost
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
chápána	chápat	k5eAaImNgFnS	chápat
rovněž	rovněž	k6eAd1	rovněž
jako	jako	k8xC	jako
pouhý	pouhý	k2eAgInSc4d1	pouhý
politický	politický	k2eAgInSc4d1	politický
manévr	manévr	k1gInSc4	manévr
na	na	k7c4	na
okrášlení	okrášlení	k1gNnSc4	okrášlení
obrazu	obraz	k1gInSc2	obraz
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgMnSc1d1	čínský
právník	právník	k1gMnSc1	právník
Kao	Kao	k1gFnSc2	Kao
Č	Č	kA	Č
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
šeng	šeng	k1gInSc1	šeng
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
nezvěstný	zvěstný	k2eNgMnSc1d1	nezvěstný
<g/>
,	,	kIx,	,
prodělal	prodělat	k5eAaPmAgMnS	prodělat
mučení	mučení	k1gNnSc4	mučení
čínskou	čínský	k2eAgFnSc7d1	čínská
tajnou	tajný	k2eAgFnSc7d1	tajná
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
napsal	napsat	k5eAaBmAgMnS	napsat
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
USA	USA	kA	USA
svoji	svůj	k3xOyFgFnSc4	svůj
výpověď	výpověď	k1gFnSc4	výpověď
<g/>
.	.	kIx.	.
</s>
<s>
Právníci	právník	k1gMnPc1	právník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hájí	hájit	k5eAaImIp3nP	hájit
vládou	vláda	k1gFnSc7	vláda
pronásledované	pronásledovaný	k2eAgFnSc2d1	pronásledovaná
duchovní	duchovní	k2eAgFnSc2d1	duchovní
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
hnutí	hnutí	k1gNnSc2	hnutí
Fa-lun-kung	Faununga	k1gFnPc2	Fa-lun-kunga
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
sami	sám	k3xTgMnPc1	sám
pronásledováni	pronásledován	k2eAgMnPc1d1	pronásledován
<g/>
.	.	kIx.	.
<g/>
Nepopiratelný	popiratelný	k2eNgInSc1d1	nepopiratelný
velký	velký	k2eAgInSc1d1	velký
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
růst	růst	k1gInSc1	růst
Číny	Čína	k1gFnSc2	Čína
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
znalců	znalec	k1gMnPc2	znalec
vykoupen	vykoupen	k2eAgMnSc1d1	vykoupen
devastací	devastace	k1gFnSc7	devastace
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
pošlapáváním	pošlapávání	k1gNnSc7	pošlapávání
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
demokracie	demokracie	k1gFnSc1	demokracie
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
svoboda	svoboda	k1gFnSc1	svoboda
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
informací	informace	k1gFnPc2	informace
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
fakticky	fakticky	k6eAd1	fakticky
povolována	povolován	k2eAgFnSc1d1	povolována
a	a	k8xC	a
ani	ani	k8xC	ani
umožněna	umožněn	k2eAgFnSc1d1	umožněna
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čínských	čínský	k2eAgInPc6d1	čínský
zákonech	zákon	k1gInPc6	zákon
ustavena	ustavit	k5eAaPmNgFnS	ustavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
území	území	k1gNnSc2	území
Číny	Čína	k1gFnSc2	Čína
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pouště	poušť	k1gFnPc4	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
při	při	k7c6	při
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
severu	sever	k1gInSc2	sever
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Chej-lung-ťiang	Chejung-ťiang	k1gInSc1	Chej-lung-ťiang
(	(	kIx(	(
<g/>
Amur	Amur	k1gInSc1	Amur
<g/>
)	)	kIx)	)
na	na	k7c4	na
53	[number]	k4	53
<g/>
°	°	k?	°
s	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
š	š	k?	š
<g/>
.	.	kIx.	.
k	k	k7c3	k
nejjižnějšímu	jižní	k2eAgInSc3d3	nejjižnější
cípu	cíp	k1gInSc3	cíp
ostrovů	ostrov	k1gInPc2	ostrov
Nan-ša	Nan-š	k1gInSc2	Nan-š
(	(	kIx(	(
<g/>
Spratlyho	Spratlyha	k1gFnSc5	Spratlyha
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
na	na	k7c4	na
18	[number]	k4	18
<g/>
°	°	k?	°
s	s	k7c7	s
<g/>
.	.	kIx.	.
<g/>
š	š	k?	š
<g/>
.	.	kIx.	.
</s>
<s>
Spojnice	spojnice	k1gFnSc1	spojnice
měří	měřit	k5eAaImIp3nS	měřit
kolem	kolem	k7c2	kolem
5 500	[number]	k4	5 500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Pamíru	Pamír	k1gInSc2	Pamír
na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
71	[number]	k4	71
<g/>
°	°	k?	°
v	v	k7c6	v
<g/>
.	.	kIx.	.
<g/>
d	d	k?	d
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
po	po	k7c4	po
soutok	soutok	k1gInSc4	soutok
řek	řeka	k1gFnPc2	řeka
Chej-lung-ťiang	Chejung-ťianga	k1gFnPc2	Chej-lung-ťianga
a	a	k8xC	a
Ussuri	Ussuri	k1gNnPc2	Ussuri
(	(	kIx(	(
<g/>
135	[number]	k4	135
<g/>
°	°	k?	°
v	v	k7c6	v
<g/>
.	.	kIx.	.
<g/>
d	d	k?	d
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
spojnice	spojnice	k1gFnSc1	spojnice
5 200	[number]	k4	5 200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Časový	časový	k2eAgInSc1d1	časový
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgFnPc4	čtyři
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
jednotný	jednotný	k2eAgInSc1d1	jednotný
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČLR	ČLR	kA	ČLR
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
14	[number]	k4	14
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
suchozemskou	suchozemský	k2eAgFnSc4d1	suchozemská
hranici	hranice	k1gFnSc4	hranice
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
22 800	[number]	k4	22 800
km	km	kA	km
a	a	k8xC	a
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgNnSc7d2	veliký
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
)	)	kIx)	)
nejvíce	nejvíce	k6eAd1	nejvíce
sousedů	soused	k1gMnPc2	soused
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
jimi	on	k3xPp3gFnPc7	on
jsou	být	k5eAaImIp3nP	být
Indie	Indie	k1gFnPc1	Indie
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc1	hranice
3 380	[number]	k4	3 380
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pákistán	Pákistán	k1gInSc1	Pákistán
(	(	kIx(	(
<g/>
523	[number]	k4	523
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
(	(	kIx(	(
<g/>
76	[number]	k4	76
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
(	(	kIx(	(
<g/>
414	[number]	k4	414
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
(	(	kIx(	(
<g/>
858	[number]	k4	858
km	km	kA	km
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
1	[number]	k4	1
533	[number]	k4	533
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
3	[number]	k4	3
645	[number]	k4	645
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
(	(	kIx(	(
<g/>
4	[number]	k4	4
677	[number]	k4	677
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
(	(	kIx(	(
<g/>
1	[number]	k4	1
416	[number]	k4	416
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
(	(	kIx(	(
<g/>
1	[number]	k4	1
281	[number]	k4	281
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
(	(	kIx(	(
<g/>
423	[number]	k4	423
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Myanmar	Myanmar	k1gMnSc1	Myanmar
(	(	kIx(	(
<g/>
2	[number]	k4	2
185	[number]	k4	185
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bhútán	Bhútán	k1gInSc1	Bhútán
(	(	kIx(	(
<g/>
470	[number]	k4	470
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nepál	Nepál	k1gInSc1	Nepál
(	(	kIx(	(
<g/>
1	[number]	k4	1
236	[number]	k4	236
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
moře	moře	k1gNnSc1	moře
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Čínu	Čína	k1gFnSc4	Čína
od	od	k7c2	od
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Čínské	čínský	k2eAgFnPc1d1	čínská
republiky	republika	k1gFnPc1	republika
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
Filipín	Filipíny	k1gFnPc2	Filipíny
<g/>
,	,	kIx,	,
Bruneje	Brunej	k1gInSc2	Brunej
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc2	Malajsie
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc2	Indonésie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČLR	ČLR	kA	ČLR
vede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
pozemní	pozemní	k2eAgFnSc4d1	pozemní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
spor	spor	k1gInSc1	spor
o	o	k7c6	o
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Tádžikistánem	Tádžikistán	k1gInSc7	Tádžikistán
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
KLDR	KLDR	kA	KLDR
není	být	k5eNaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
<g/>
.	.	kIx.	.
</s>
<s>
Pohraniční	pohraniční	k2eAgInPc1d1	pohraniční
spory	spor	k1gInPc1	spor
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
byly	být	k5eAaImAgFnP	být
urovnány	urovnán	k2eAgFnPc1d1	urovnána
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004.	[number]	k4	2004.
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
omývají	omývat	k5eAaImIp3nP	omývat
čínské	čínský	k2eAgNnSc1d1	čínské
území	území	k1gNnSc1	území
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
Pochajský	Pochajský	k2eAgInSc1d1	Pochajský
záliv	záliv	k1gInSc1	záliv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
čínské	čínský	k2eAgNnSc4d1	čínské
kontinentální	kontinentální	k2eAgNnSc4d1	kontinentální
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Žluté	žlutý	k2eAgNnSc4d1	žluté
<g/>
,	,	kIx,	,
Východočínské	východočínský	k2eAgNnSc4d1	Východočínské
a	a	k8xC	a
Jihočínské	jihočínský	k2eAgNnSc4d1	Jihočínské
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
jsou	být	k5eAaImIp3nP	být
okrajovými	okrajový	k2eAgFnPc7d1	okrajová
moři	moře	k1gNnSc6	moře
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teritoriálních	teritoriální	k2eAgFnPc6d1	teritoriální
vodách	voda	k1gFnPc6	voda
Číny	Čína	k1gFnSc2	Čína
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
4,73	[number]	k4	4,73
milionu	milion	k4xCgInSc2	milion
km2	km2	k4	km2
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c4	na
5 400	[number]	k4	5 400
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
36 000	[number]	k4	36 000
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
(	(	kIx(	(
<g/>
ovládán	ovládán	k2eAgInSc1d1	ovládán
je	být	k5eAaImIp3nS	být
však	však	k9	však
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc1d1	následovaný
ostrovem	ostrov	k1gInSc7	ostrov
Chaj-nan	Chajan	k1gMnSc1	Chaj-nan
(	(	kIx(	(
<g/>
34	[number]	k4	34
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýchodnějšími	východní	k2eAgInPc7d3	nejvýchodnější
čínskými	čínský	k2eAgInPc7d1	čínský
ostrovy	ostrov	k1gInPc7	ostrov
je	být	k5eAaImIp3nS	být
souostroví	souostroví	k1gNnSc1	souostroví
Tiao-jü	Tiaoü	k1gFnSc2	Tiao-jü
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvýchodněji	východně	k6eAd3	východně
leží	ležet	k5eAaImIp3nS	ležet
ostrov	ostrov	k1gInSc4	ostrov
Čch	Čch	k1gFnSc2	Čch
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
wej	wej	k?	wej
<g/>
;	;	kIx,	;
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
pod	pod	k7c7	pod
námořní	námořní	k2eAgFnSc7d1	námořní
kontrolou	kontrola	k1gFnSc7	kontrola
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
a	a	k8xC	a
skalních	skalní	k2eAgInPc2d1	skalní
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označován	k2eAgInPc1d1	označován
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
ostrovy	ostrov	k1gInPc1	ostrov
Jihočínského	jihočínský	k2eAgNnSc2d1	Jihočínské
moře	moře	k1gNnSc2	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
patří	patřit	k5eAaImIp3nS	patřit
hlavně	hlavně	k6eAd1	hlavně
do	do	k7c2	do
čtyř	čtyři	k4xCgNnPc2	čtyři
větších	veliký	k2eAgNnPc2d2	veliký
souostroví	souostroví	k1gNnPc2	souostroví
<g/>
:	:	kIx,	:
Tung-ša	Tung-ša	k1gFnSc1	Tung-ša
(	(	kIx(	(
<g/>
Východní	východní	k2eAgInPc1d1	východní
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Si-ša	Si-ša	k1gFnSc1	Si-ša
(	(	kIx(	(
<g/>
Západní	západní	k2eAgInPc1d1	západní
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Čung-ša	Čung-ša	k1gFnSc1	Čung-ša
(	(	kIx(	(
<g/>
Střední	střední	k2eAgInPc1d1	střední
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nan-ša	Nan-ša	k1gFnSc1	Nan-ša
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgInPc1d1	jižní
písky	písek	k1gInPc1	písek
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
vedou	vést	k5eAaImIp3nP	vést
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
územní	územní	k2eAgInPc1d1	územní
spory	spor	k1gInPc1	spor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČLR	ČLR	kA	ČLR
je	být	k5eAaImIp3nS	být
zaangažována	zaangažován	k2eAgFnSc1d1	zaangažován
ve	v	k7c6	v
spletitém	spletitý	k2eAgInSc6d1	spletitý
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
ostrovy	ostrov	k1gInPc4	ostrov
Nan-ša	Nan-šus	k1gMnSc2	Nan-šus
(	(	kIx(	(
<g/>
Spratlyho	Spratly	k1gMnSc2	Spratly
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
s	s	k7c7	s
Malajsií	Malajsie	k1gFnSc7	Malajsie
<g/>
,	,	kIx,	,
Filipínami	Filipíny	k1gFnPc7	Filipíny
<g/>
,	,	kIx,	,
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
Vietnamem	Vietnam	k1gInSc7	Vietnam
a	a	k8xC	a
Brunejí	Brunej	k1gFnSc7	Brunej
a	a	k8xC	a
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c6	o
souostroví	souostroví	k1gNnSc6	souostroví
Tiao-jü-tao	Tiaoüao	k1gMnSc1	Tiao-jü-tao
(	(	kIx(	(
<g/>
Senkaku	Senkak	k1gInSc2	Senkak
<g/>
)	)	kIx)	)
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
a	a	k8xC	a
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vede	vést	k5eAaImIp3nS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
námořní	námořní	k2eAgFnSc4d1	námořní
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Vietnamem	Vietnam	k1gInSc7	Vietnam
v	v	k7c6	v
Tonkinském	tonkinský	k2eAgInSc6d1	tonkinský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
Si-ša	Si-šum	k1gNnSc2	Si-šum
(	(	kIx(	(
<g/>
Paracelské	Paracelský	k2eAgInPc1d1	Paracelský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obsazené	obsazený	k2eAgInPc1d1	obsazený
Čínskou	čínský	k2eAgFnSc7d1	čínská
lidovou	lidový	k2eAgFnSc7d1	lidová
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nárokovány	nárokován	k2eAgFnPc1d1	nárokován
Vietnamem	Vietnam	k1gInSc7	Vietnam
a	a	k8xC	a
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
i	i	k9	i
okolo	okolo	k7c2	okolo
souostroví	souostroví	k1gNnSc2	souostroví
Tung-ša	Tung-š	k1gInSc2	Tung-š
(	(	kIx(	(
<g/>
Prataské	Prataský	k2eAgInPc1d1	Prataský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
Čung-ša	Čung-ša	k1gFnSc1	Čung-ša
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
Číně	Čína	k1gFnSc6	Čína
bývají	bývat	k5eAaImIp3nP	bývat
studené	studený	k2eAgFnPc1d1	studená
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
horká	horký	k2eAgNnPc4d1	horké
léta	léto	k1gNnPc4	léto
s	s	k7c7	s
přiměřeným	přiměřený	k2eAgNnSc7d1	přiměřené
množstvím	množství	k1gNnSc7	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
oblasti	oblast	k1gFnSc6	oblast
střední	střední	k2eAgFnSc2d1	střední
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgFnPc4d1	mírná
zimy	zima	k1gFnPc4	zima
a	a	k8xC	a
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
je	být	k5eAaImIp3nS	být
vlhké	vlhký	k2eAgNnSc4d1	vlhké
subtropické	subtropický	k2eAgNnSc4d1	subtropické
podnebí	podnebí	k1gNnSc4	podnebí
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc1	podnebí
velmi	velmi	k6eAd1	velmi
drsné	drsný	k2eAgNnSc1d1	drsné
<g/>
.	.	kIx.	.
</s>
<s>
Lhasa	Lhasa	k1gFnSc1	Lhasa
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
studené	studený	k2eAgFnSc2d1	studená
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
málo	málo	k4c4	málo
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
vícestupňový	vícestupňový	k2eAgInSc4d1	vícestupňový
správní	správní	k2eAgInSc4d1	správní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
úrovní	úroveň	k1gFnSc7	úroveň
stojí	stát	k5eAaImIp3nS	stát
provincie	provincie	k1gFnSc1	provincie
(	(	kIx(	(
<g/>
省	省	k?	省
<g/>
,	,	kIx,	,
shěng	shěng	k1gMnSc1	shěng
<g/>
,	,	kIx,	,
šeng	šeng	k1gMnSc1	šeng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ČLR	ČLR	kA	ČLR
jich	on	k3xPp3gMnPc2	on
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
22	[number]	k4	22
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc1	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
je	být	k5eAaImIp3nS	být
vládou	vláda	k1gFnSc7	vláda
ČLR	ČLR	kA	ČLR
sice	sice	k8xC	sice
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
další	další	k2eAgFnSc4d1	další
provincii	provincie	k1gFnSc4	provincie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
nad	nad	k7c7	nad
ním	on	k3xPp3gMnSc7	on
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
provincií	provincie	k1gFnPc2	provincie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
pět	pět	k4xCc4	pět
autonomních	autonomní	k2eAgFnPc2d1	autonomní
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
自	自	k?	自
<g/>
,	,	kIx,	,
zì	zì	k?	zì
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
č	č	k0	č
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
čchü	čchü	k?	čchü
<g/>
)	)	kIx)	)
zahrnujících	zahrnující	k2eAgNnPc2d1	zahrnující
území	území	k1gNnPc2	území
obydlené	obydlený	k2eAgFnPc1d1	obydlená
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
příslušníky	příslušník	k1gMnPc7	příslušník
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
ústřední	ústřední	k2eAgInPc4d1	ústřední
vládou	vláda	k1gFnSc7	vláda
přímo	přímo	k6eAd1	přímo
spravovaná	spravovaný	k2eAgNnPc1d1	spravované
města	město	k1gNnPc1	město
(	(	kIx(	(
<g/>
直	直	k?	直
<g/>
,	,	kIx,	,
zhíxiáshì	zhíxiáshì	k?	zhíxiáshì
<g/>
,	,	kIx,	,
č	č	k0	č
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
sia-š	sia-š	k5eAaPmIp2nS	sia-š
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
správní	správní	k2eAgFnPc1d1	správní
oblasti	oblast	k1gFnPc1	oblast
(	(	kIx(	(
<g/>
特	特	k?	特
<g/>
,	,	kIx,	,
tè	tè	k?	tè
xíngzhè	xíngzhè	k?	xíngzhè
<g/>
,	,	kIx,	,
tche-pie	tcheie	k1gFnSc1	tche-pie
sing-čeng-čchü	sing-čeng-čchü	k?	sing-čeng-čchü
<g/>
)	)	kIx)	)
těšící	těšící	k2eAgFnSc3d1	těšící
se	se	k3xPyFc4	se
velké	velký	k2eAgFnSc3d1	velká
míře	míra	k1gFnSc3	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
provincie	provincie	k1gFnPc1	provincie
a	a	k8xC	a
autonomní	autonomní	k2eAgFnPc1d1	autonomní
oblasti	oblast	k1gFnPc1	oblast
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
na	na	k7c4	na
autonomní	autonomní	k2eAgFnPc4d1	autonomní
prefektury	prefektura	k1gFnPc4	prefektura
<g/>
,	,	kIx,	,
okresy	okres	k1gInPc4	okres
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgInPc4d1	autonomní
okresy	okres	k1gInPc4	okres
a	a	k8xC	a
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgMnPc4	ten
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
města	město	k1gNnPc4	město
a	a	k8xC	a
obce	obec	k1gFnPc4	obec
včetně	včetně	k7c2	včetně
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
etnických	etnický	k2eAgFnPc2d1	etnická
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
provincií	provincie	k1gFnPc2	provincie
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
je	být	k5eAaImIp3nS	být
totalitní	totalitní	k2eAgInSc4d1	totalitní
režim	režim	k1gInSc4	režim
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
čele	čelo	k1gNnSc6	čelo
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
pět	pět	k4xCc4	pět
"	"	kIx"	"
<g/>
generací	generace	k1gFnPc2	generace
vůdců	vůdce	k1gMnPc2	vůdce
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
válečná	válečný	k2eAgFnSc1d1	válečná
generace	generace	k1gFnSc1	generace
</s>
</p>
<p>
<s>
Mao	Mao	k?	Mao
Ce-tung	Ceung	k1gMnSc1	Ce-tung
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
1954	[number]	k4	1954
<g/>
–	–	k?	–
<g/>
1959	[number]	k4	1959
</s>
</p>
<p>
<s>
Čou	Čou	k?	Čou
En-laj	Enaj	k1gMnSc1	En-laj
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1949	[number]	k4	1949
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Ču	Ču	k?	Ču
Te	Te	k1gMnSc1	Te
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
stálého	stálý	k2eAgInSc2d1	stálý
výboru	výbor	k1gInSc2	výbor
Všečínského	všečínský	k2eAgNnSc2d1	Všečínské
shromáždění	shromáždění	k1gNnSc2	shromáždění
lidových	lidový	k2eAgMnPc2d1	lidový
zástupců	zástupce	k1gMnPc2	zástupce
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Liou	Lio	k2eAgFnSc4d1	Lio
Šao-čchi	Šao-čche	k1gFnSc4	Šao-čche
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
1959	[number]	k4	1959
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
válečná	válečný	k2eAgFnSc1d1	válečná
generace	generace	k1gFnSc1	generace
</s>
</p>
<p>
<s>
Teng	Teng	k1gInSc1	Teng
Siao-pching	Siaoching	k1gInSc1	Siao-pching
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vojenské	vojenský	k2eAgFnSc2d1	vojenská
komise	komise	k1gFnSc2	komise
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Maův	Maův	k1gMnSc1	Maův
nástupce	nástupce	k1gMnSc1	nástupce
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgMnS	provést
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
reformy	reforma	k1gFnSc2	reforma
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgMnS	otevřít
Čínu	Čína	k1gFnSc4	Čína
zahraničnímu	zahraniční	k2eAgInSc3d1	zahraniční
kapitálu	kapitál	k1gInSc3	kapitál
</s>
</p>
<p>
<s>
Chua	Chua	k1gMnSc1	Chua
Kuo-feng	Kuoeng	k1gMnSc1	Kuo-feng
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
Chu	Chu	k?	Chu
Jao-Pang	Jao-Pang	k1gMnSc1	Jao-Pang
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
</s>
</p>
<p>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
vzešlá	vzešlý	k2eAgFnSc1d1	vzešlá
z	z	k7c2	z
ředitelů	ředitel	k1gMnPc2	ředitel
státních	státní	k2eAgInPc2d1	státní
podniků	podnik	k1gInPc2	podnik
vzdělaných	vzdělaný	k2eAgInPc2d1	vzdělaný
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čao	čao	k0	čao
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
jang	jang	k1gMnSc1	jang
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1983	[number]	k4	1983
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Ťiang	Ťiang	k1gMnSc1	Ťiang
Ce-min	Cein	k2eAgMnSc1d1	Ce-min
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Li	li	k8xS	li
Pcheng	Pcheng	k1gMnSc1	Pcheng
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Ču	Ču	k?	Ču
Žung-ťi	Žung-ťi	k1gNnSc1	Žung-ťi
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
Čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
generace	generace	k1gFnSc1	generace
<g/>
,	,	kIx,	,
technokratická	technokratický	k2eAgFnSc1d1	technokratická
</s>
</p>
<p>
<s>
Chu	Chu	k?	Chu
Ťin-tchao	Ťinchao	k1gMnSc1	Ťin-tchao
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Wen	Wen	k?	Wen
Ťia-pao	Ťiaao	k1gMnSc1	Ťia-pao
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
od	od	k7c2	od
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Pátá	pátý	k4xOgFnSc1	pátý
generace	generace	k1gFnSc1	generace
</s>
</p>
<p>
<s>
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
<g/>
,	,	kIx,	,
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
strany	strana	k1gFnSc2	strana
od	od	k7c2	od
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
od	od	k7c2	od
2013	[number]	k4	2013
</s>
</p>
<p>
<s>
Li	li	k8xS	li
Kche-čchiang	Kche-čchiang	k1gMnSc1	Kche-čchiang
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
od	od	k7c2	od
2013	[number]	k4	2013
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
ČLR	ČLR	kA	ČLR
má	mít	k5eAaImIp3nS	mít
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
175	[number]	k4	175
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
ve	v	k7c6	v
162	[number]	k4	162
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
legitimita	legitimita	k1gFnSc1	legitimita
je	být	k5eAaImIp3nS	být
zpochybňována	zpochybňovat	k5eAaImNgFnS	zpochybňovat
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
několika	několik	k4yIc7	několik
dalšími	další	k2eAgFnPc7d1	další
zeměmi	zem	k1gFnPc7	zem
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
a	a	k8xC	a
nejlidnatější	lidnatý	k2eAgInSc4d3	nejlidnatější
stát	stát	k1gInSc4	stát
s	s	k7c7	s
omezeným	omezený	k2eAgNnSc7d1	omezené
uznáním	uznání	k1gNnSc7	uznání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ČLR	ČLR	kA	ČLR
Čínskou	čínský	k2eAgFnSc4d1	čínská
republiku	republika	k1gFnSc4	republika
jako	jako	k8xS	jako
výhradního	výhradní	k2eAgMnSc4d1	výhradní
zástupce	zástupce	k1gMnSc4	zástupce
Číny	Čína	k1gFnSc2	Čína
v	v	k7c6	v
OSN	OSN	kA	OSN
a	a	k8xC	a
jako	jako	k8xS	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
stálých	stálý	k2eAgMnPc2d1	stálý
členů	člen	k1gMnPc2	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
je	být	k5eAaImIp3nS	být
také	také	k9	také
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
a	a	k8xC	a
lídr	lídr	k1gMnSc1	lídr
hnutí	hnutí	k1gNnSc2	hnutí
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
stále	stále	k6eAd1	stále
sebe	sebe	k3xPyFc4	sebe
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c2	za
obhájce	obhájce	k1gMnSc2	obhájce
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
Jihoafrickou	jihoafrický	k2eAgFnSc7d1	Jihoafrická
republikou	republika	k1gFnSc7	republika
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
členem	člen	k1gInSc7	člen
BRICS	BRICS	kA	BRICS
-	-	kIx~	-
skupiny	skupina	k1gFnSc2	skupina
rozvíjejících	rozvíjející	k2eAgFnPc2d1	rozvíjející
se	se	k3xPyFc4	se
hlavních	hlavní	k2eAgFnPc2d1	hlavní
ekonomik	ekonomika	k1gFnPc2	ekonomika
a	a	k8xC	a
hostila	hostit	k5eAaImAgFnS	hostit
třetí	třetí	k4xOgInSc4	třetí
oficiální	oficiální	k2eAgInSc4d1	oficiální
summit	summit	k1gInSc4	summit
v	v	k7c4	v
San-ja	Sanum	k1gNnPc4	San-jum
na	na	k7c4	na
Chaj-nanu	Chajana	k1gFnSc4	Chaj-nana
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
výkladem	výklad	k1gInSc7	výklad
politiky	politika	k1gFnSc2	politika
jedné	jeden	k4xCgFnSc2	jeden
Číny	Čína	k1gFnSc2	Čína
Peking	Peking	k1gInSc1	Peking
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
takovou	takový	k3xDgFnSc4	takový
podmínku	podmínka	k1gFnSc4	podmínka
k	k	k7c3	k
navázání	navázání	k1gNnSc3	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhá	druhý	k4xOgFnSc1	druhý
země	země	k1gFnSc1	země
uznává	uznávat	k5eAaImIp3nS	uznávat
jeho	on	k3xPp3gInSc4	on
požadavek	požadavek	k1gInSc4	požadavek
na	na	k7c4	na
Tchaj-wan	Tchajan	k1gInSc4	Tchaj-wan
a	a	k8xC	a
zpřetrhává	zpřetrhávat	k5eAaImIp3nS	zpřetrhávat
oficiální	oficiální	k2eAgFnPc4d1	oficiální
diplomatické	diplomatický	k2eAgFnPc4d1	diplomatická
vazby	vazba	k1gFnPc4	vazba
s	s	k7c7	s
Čínskou	čínský	k2eAgFnSc7d1	čínská
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
představitelé	představitel	k1gMnPc1	představitel
protestovali	protestovat	k5eAaBmAgMnP	protestovat
při	při	k7c6	při
mnoha	mnoho	k4c6	mnoho
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
když	když	k8xS	když
zahraniční	zahraniční	k2eAgFnPc1d1	zahraniční
země	zem	k1gFnPc1	zem
učinily	učinit	k5eAaPmAgFnP	učinit
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
pokusy	pokus	k1gInPc4	pokus
o	o	k7c6	o
sblížení	sblížení	k1gNnSc6	sblížení
s	s	k7c7	s
Tchaj-wanem	Tchajan	k1gInSc7	Tchaj-wan
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
prodeje	prodej	k1gInSc2	prodej
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
současné	současný	k2eAgFnSc2d1	současná
čínské	čínský	k2eAgFnSc2d1	čínská
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
politiky	politika	k1gFnSc2	politika
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
politice	politika	k1gFnSc6	politika
premiéra	premiér	k1gMnSc2	premiér
Čou	Čou	k1gMnSc2	Čou
En-laje	Enaje	k1gMnSc2	En-laje
-	-	kIx~	-
Panča	Pančus	k1gMnSc2	Pančus
šíla	šíl	k1gMnSc2	šíl
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
poháněna	pohánět	k5eAaImNgFnS	pohánět
pojmem	pojem	k1gInSc7	pojem
"	"	kIx"	"
<g/>
harmonie	harmonie	k1gFnSc2	harmonie
bez	bez	k7c2	bez
uniformity	uniformita	k1gFnSc2	uniformita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podporuje	podporovat	k5eAaImIp3nS	podporovat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c4	mezi
státy	stát	k1gInPc4	stát
navzdory	navzdory	k7c3	navzdory
ideologickým	ideologický	k2eAgInPc3d1	ideologický
rozdílům	rozdíl	k1gInPc3	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
politika	politika	k1gFnSc1	politika
zřejmě	zřejmě	k6eAd1	zřejmě
vedla	vést	k5eAaImAgFnS	vést
Čínu	Čína	k1gFnSc4	Čína
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
západními	západní	k2eAgInPc7d1	západní
státy	stát	k1gInPc7	stát
označovné	označovný	k2eAgFnSc2d1	označovný
jako	jako	k8xC	jako
darebácké	darebácký	k2eAgInPc1d1	darebácký
a	a	k8xC	a
represivní	represivní	k2eAgInPc1d1	represivní
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Zimbabwe	Zimbabwe	k1gNnSc1	Zimbabwe
<g/>
,	,	kIx,	,
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
má	mít	k5eAaImIp3nS	mít
úzké	úzký	k2eAgInPc4d1	úzký
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
a	a	k8xC	a
vojenské	vojenský	k2eAgInPc4d1	vojenský
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
často	často	k6eAd1	často
hlasují	hlasovat	k5eAaImIp3nP	hlasovat
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
jednotně	jednotně	k6eAd1	jednotně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Územní	územní	k2eAgInPc1d1	územní
spory	spor	k1gInPc1	spor
====	====	k?	====
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
čínské	čínský	k2eAgFnSc6d1	čínská
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
si	se	k3xPyFc3	se
ČLR	ČLR	kA	ČLR
nárokuje	nárokovat	k5eAaImIp3nS	nárokovat
území	území	k1gNnSc4	území
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
oddělené	oddělený	k2eAgFnSc2d1	oddělená
politické	politický	k2eAgFnSc2d1	politická
entity	entita	k1gFnSc2	entita
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
uzemí	uzemit	k5eAaPmIp3nS	uzemit
jako	jako	k8xS	jako
provincii	provincie	k1gFnSc4	provincie
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Ťin-men	Ťinen	k1gInSc4	Ťin-men
a	a	k8xC	a
Ma-cu	Maa	k1gFnSc4	Ma-ca
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
provincie	provincie	k1gFnSc2	provincie
Fu-ťien	Fu-ťien	k1gInSc1	Fu-ťien
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
Jihočínském	jihočínský	k2eAgNnSc6d1	Jihočínské
moři	moře	k1gNnSc6	moře
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Čínské	čínský	k2eAgFnSc2d1	čínská
republiky	republika	k1gFnSc2	republika
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
provincie	provincie	k1gFnSc2	provincie
Chaj-nan	Chajana	k1gFnPc2	Chaj-nana
a	a	k8xC	a
provincie	provincie	k1gFnSc2	provincie
Kuang-tung	Kuangunga	k1gFnPc2	Kuang-tunga
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tvrzení	tvrzení	k1gNnPc1	tvrzení
jsou	být	k5eAaImIp3nP	být
kontroverzní	kontroverzní	k2eAgNnPc1d1	kontroverzní
kvůli	kvůli	k7c3	kvůli
komplikovaným	komplikovaný	k2eAgInPc3d1	komplikovaný
vztahům	vztah	k1gInPc3	vztah
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
úžiny	úžina	k1gFnSc2	úžina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ČLR	ČLR	kA	ČLR
operuje	operovat	k5eAaImIp3nS	operovat
s	s	k7c7	s
politikou	politika	k1gFnSc7	politika
jedné	jeden	k4xCgFnSc2	jeden
Číny	Čína	k1gFnSc2	Čína
jako	jako	k9	jako
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
principů	princip	k1gInPc2	princip
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
Tchaj-wanu	Tchajan	k1gInSc2	Tchaj-wan
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
zapojena	zapojen	k2eAgFnSc1d1	zapojena
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
územních	územní	k2eAgInPc2d1	územní
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
devadesátých	devadesátý	k4xOgInPc2	devadesátý
lét	lét	k?	lét
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
zapojená	zapojený	k2eAgFnSc1d1	zapojená
do	do	k7c2	do
jednání	jednání	k1gNnSc2	jednání
vyřešit	vyřešit	k5eAaPmF	vyřešit
své	svůj	k3xOyFgFnPc4	svůj
sporné	sporný	k2eAgFnPc4d1	sporná
pozemní	pozemní	k2eAgFnPc4d1	pozemní
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
nedefinované	definovaný	k2eNgFnPc4d1	nedefinovaná
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Bhútánem	Bhútán	k1gInSc7	Bhútán
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
je	být	k5eAaImIp3nS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
mnohostranných	mnohostranný	k2eAgInPc2d1	mnohostranný
sporů	spor	k1gInPc2	spor
o	o	k7c6	o
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
několika	několik	k4yIc2	několik
malých	malý	k2eAgInPc2d1	malý
ostrovů	ostrov	k1gInPc2	ostrov
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
Číny	Čína	k1gFnSc2	Čína
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ostrovy	ostrov	k1gInPc1	ostrov
Senkaku	Senkak	k1gInSc2	Senkak
a	a	k8xC	a
mělčina	mělčina	k1gFnSc1	mělčina
Scarborough	Scarborougha	k1gFnPc2	Scarborougha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
2,3	[number]	k4	2,3
milióny	milión	k4xCgInPc7	milión
aktivními	aktivní	k2eAgInPc7d1	aktivní
vojáky	voják	k1gMnPc4	voják
je	být	k5eAaImIp3nS	být
čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
PLA	PLA	kA	PLA
<g/>
)	)	kIx)	)
největší	veliký	k2eAgFnSc7d3	veliký
stálou	stálý	k2eAgFnSc7d1	stálá
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Řízena	řízen	k2eAgFnSc1d1	řízena
je	být	k5eAaImIp3nS	být
ústřední	ústřední	k2eAgFnSc7d1	ústřední
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
komisí	komise	k1gFnSc7	komise
(	(	kIx(	(
<g/>
CMC	CMC	kA	CMC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
KLDR	KLDR	kA	KLDR
má	mít	k5eAaImIp3nS	mít
Čína	Čína	k1gFnSc1	Čína
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
rezervistů	rezervista	k1gMnPc2	rezervista
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
pozemních	pozemní	k2eAgFnPc2d1	pozemní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
PLAGF	PLAGF	kA	PLAGF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
(	(	kIx(	(
<g/>
PLAN	plan	k1gInSc1	plan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
letectva	letectvo	k1gNnSc2	letectvo
(	(	kIx(	(
<g/>
PLAAF	PLAAF	kA	PLAAF
<g/>
)	)	kIx)	)
a	a	k8xC	a
raketového	raketový	k2eAgNnSc2d1	raketové
vojska	vojsko	k1gNnSc2	vojsko
(	(	kIx(	(
<g/>
PLARF	PLARF	kA	PLARF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
činil	činit	k5eAaImAgInS	činit
čínský	čínský	k2eAgInSc1d1	čínský
vojenský	vojenský	k2eAgInSc1d1	vojenský
rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2017 151,5	[number]	k4	2017 151,5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
druhý	druhý	k4xOgMnSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
vojenský	vojenský	k2eAgInSc1d1	vojenský
rozpočet	rozpočet	k1gInSc1	rozpočet
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
poměr	poměr	k1gInSc1	poměr
vojenských	vojenský	k2eAgInPc2d1	vojenský
výdajů	výdaj	k1gInPc2	výdaj
-	-	kIx~	-
HDP	HDP	kA	HDP
s	s	k7c7	s
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
světovým	světový	k2eAgInSc7d1	světový
průměrem	průměr	k1gInSc7	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
instituce	instituce	k1gFnPc1	instituce
-	-	kIx~	-
včetně	včetně	k7c2	včetně
SIPRI	SIPRI	kA	SIPRI
a	a	k8xC	a
amerického	americký	k2eAgInSc2d1	americký
úřadu	úřad	k1gInSc2	úřad
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čína	Čína	k1gFnSc1	Čína
svou	svůj	k3xOyFgFnSc4	svůj
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
úroveň	úroveň	k1gFnSc4	úroveň
vojenských	vojenský	k2eAgInPc2d1	vojenský
výdajů	výdaj	k1gInPc2	výdaj
nehlásí	hlásit	k5eNaImIp3nP	hlásit
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgInSc1d2	vyšší
než	než	k8xS	než
oficiální	oficiální	k2eAgInSc1d1	oficiální
rozpočet	rozpočet	k1gInSc1	rozpočet
<g/>
.	.	kIx.	.
<g/>
Čína	Čína	k1gFnSc1	Čína
také	také	k9	také
disponuje	disponovat	k5eAaBmIp3nS	disponovat
jadernými	jaderný	k2eAgFnPc7d1	jaderná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
oblastní	oblastní	k2eAgFnSc4d1	oblastní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
potenciální	potenciální	k2eAgFnSc4d1	potenciální
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
supervelmoc	supervelmoc	k1gFnSc4	supervelmoc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
má	mít	k5eAaImIp3nS	mít
Čína	Čína	k1gFnSc1	Čína
ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
mezi	mezi	k7c7	mezi
50	[number]	k4	50
až	až	k9	až
75	[number]	k4	75
kusy	kus	k1gInPc4	kus
jaderných	jaderný	k2eAgFnPc2d1	jaderná
mezikontinentálních	mezikontinentální	k2eAgFnPc2d1	mezikontinentální
balistických	balistický	k2eAgFnPc2d1	balistická
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
balistických	balistický	k2eAgFnPc2d1	balistická
raket	raketa	k1gFnPc2	raketa
krátkého	krátký	k2eAgInSc2d1	krátký
dosahu	dosah	k1gInSc2	dosah
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
čtyřmi	čtyři	k4xCgMnPc7	čtyři
stálými	stálý	k2eAgMnPc7d1	stálý
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
má	mít	k5eAaImIp3nS	mít
Čína	Čína	k1gFnSc1	Čína
relativně	relativně	k6eAd1	relativně
omezené	omezený	k2eAgFnSc2d1	omezená
možnosti	možnost	k1gFnSc2	možnost
projekce	projekce	k1gFnSc2	projekce
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Čínská	čínský	k2eAgFnSc1d1	čínská
špionáž	špionáž	k1gFnSc1	špionáž
==	==	k?	==
</s>
</p>
<p>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
zájem	zájem	k1gInSc1	zájem
se	se	k3xPyFc4	se
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
zejména	zejména	k9	zejména
na	na	k7c4	na
strategické	strategický	k2eAgInPc4d1	strategický
sektory	sektor	k1gInPc4	sektor
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
energetika	energetika	k1gFnSc1	energetika
<g/>
,	,	kIx,	,
telekomunikace	telekomunikace	k1gFnPc1	telekomunikace
<g/>
,	,	kIx,	,
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
logistika	logistika	k1gFnSc1	logistika
<g/>
,	,	kIx,	,
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
a	a	k8xC	a
špičkové	špičkový	k2eAgFnPc1d1	špičková
technologie	technologie	k1gFnPc1	technologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
špionáž	špionáž	k1gFnSc1	špionáž
===	===	k?	===
</s>
</p>
<p>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
celonárodní	celonárodní	k2eAgFnSc6d1	celonárodní
špionáži	špionáž	k1gFnSc6	špionáž
<g/>
,	,	kIx,	,
přijatý	přijatý	k2eAgInSc1d1	přijatý
Všelidovým	všelidový	k2eAgInSc7d1	všelidový
kongresem	kongres	k1gInSc7	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
všem	všecek	k3xTgMnPc3	všecek
čínským	čínský	k2eAgMnPc3d1	čínský
jednotlivcům	jednotlivec	k1gMnPc3	jednotlivec
i	i	k8xC	i
firmám	firma	k1gFnPc3	firma
v	v	k7c6	v
případě	případ	k1gInSc6	případ
státního	státní	k2eAgInSc2d1	státní
zájmu	zájem	k1gInSc2	zájem
provádět	provádět	k5eAaImF	provádět
špionáž	špionáž	k1gFnSc4	špionáž
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Huawei	Huawe	k1gInSc3	Huawe
China	China	k1gFnSc1	China
zavedla	zavést	k5eAaPmAgFnS	zavést
formální	formální	k2eAgFnSc4d1	formální
politiku	politika	k1gFnSc4	politika
bonusového	bonusový	k2eAgInSc2d1	bonusový
programu	program	k1gInSc2	program
odměňujícího	odměňující	k2eAgMnSc4d1	odměňující
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
konkurenci	konkurence	k1gFnSc3	konkurence
tajné	tajný	k2eAgFnSc2d1	tajná
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
bylo	být	k5eAaImAgNnS	být
nařízeno	nařízen	k2eAgNnSc1d1	nařízeno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tajné	tajný	k2eAgFnPc4d1	tajná
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
cizích	cizí	k2eAgFnPc2d1	cizí
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
ukládali	ukládat	k5eAaImAgMnP	ukládat
na	na	k7c4	na
interní	interní	k2eAgFnSc4d1	interní
firemní	firemní	k2eAgFnSc4d1	firemní
stránku	stránka	k1gFnSc4	stránka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zvláště	zvláště	k6eAd1	zvláště
citlivých	citlivý	k2eAgFnPc2d1	citlivá
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
poslali	poslat	k5eAaPmAgMnP	poslat
zašifrovaný	zašifrovaný	k2eAgInSc4d1	zašifrovaný
email	email	k1gInSc4	email
do	do	k7c2	do
speciální	speciální	k2eAgFnSc2d1	speciální
emailové	emailový	k2eAgFnSc2d1	emailová
schránky	schránka	k1gFnSc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Směrnice	směrnice	k1gFnSc1	směrnice
zdůrazňovala	zdůrazňovat	k5eAaImAgFnS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgMnSc1	žádný
ze	z	k7c2	z
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
nebude	být	k5eNaImBp3nS	být
potrestán	potrestat	k5eAaPmNgMnS	potrestat
za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
provedena	provést	k5eAaPmNgFnS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skandálu	skandál	k1gInSc6	skandál
v	v	k7c6	v
USA	USA	kA	USA
upozornilo	upozornit	k5eAaPmAgNnS	upozornit
vedení	vedení	k1gNnSc1	vedení
pracovníky	pracovník	k1gMnPc7	pracovník
Huawei	Huawe	k1gFnSc2	Huawe
USA	USA	kA	USA
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
podobné	podobný	k2eAgFnSc2d1	podobná
činnosti	činnost	k1gFnSc2	činnost
nejsou	být	k5eNaImIp3nP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
emailové	emailový	k2eAgFnSc2d1	emailová
komunikace	komunikace	k1gFnSc2	komunikace
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
amerických	americký	k2eAgInPc2d1	americký
úřadů	úřad	k1gInPc2	úřad
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
regionech	region	k1gInPc6	region
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
naprosto	naprosto	k6eAd1	naprosto
běžná	běžný	k2eAgFnSc1d1	běžná
součást	součást	k1gFnSc1	součást
pracovní	pracovní	k2eAgFnSc2d1	pracovní
náplně	náplň	k1gFnSc2	náplň
<g/>
.	.	kIx.	.
<g/>
Deník	deník	k1gInSc1	deník
Frankfurter	Frankfurtra	k1gFnPc2	Frankfurtra
Algemeine	Algemein	k1gInSc5	Algemein
Zeitung	Zeitung	k1gMnSc1	Zeitung
popsal	popsat	k5eAaPmAgMnS	popsat
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
zkušenosti	zkušenost	k1gFnPc4	zkušenost
německých	německý	k2eAgMnPc2d1	německý
manažerů	manažer	k1gMnPc2	manažer
<g/>
,	,	kIx,	,
bankéřů	bankéř	k1gMnPc2	bankéř
a	a	k8xC	a
právníků	právník	k1gMnPc2	právník
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
je	on	k3xPp3gMnPc4	on
odposlouchávají	odposlouchávat	k5eAaImIp3nP	odposlouchávat
v	v	k7c6	v
restauracích	restaurace	k1gFnPc6	restaurace
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
telefony	telefon	k1gInPc4	telefon
nebo	nebo	k8xC	nebo
stahují	stahovat	k5eAaImIp3nP	stahovat
data	datum	k1gNnPc4	datum
rovnou	rovnou	k6eAd1	rovnou
z	z	k7c2	z
jejich	jejich	k3xOp3gNnPc2	jejich
zařízení	zařízení	k1gNnPc2	zařízení
odcizených	odcizený	k2eAgNnPc2d1	odcizené
z	z	k7c2	z
hotelového	hotelový	k2eAgInSc2d1	hotelový
trezoru	trezor	k1gInSc2	trezor
<g/>
.	.	kIx.	.
</s>
<s>
Přísná	přísný	k2eAgNnPc1d1	přísné
bezpečnostní	bezpečnostní	k2eAgNnPc1d1	bezpečnostní
pravidla	pravidlo	k1gNnPc1	pravidlo
platí	platit	k5eAaImIp3nP	platit
i	i	k9	i
pro	pro	k7c4	pro
průmyslníky	průmyslník	k1gMnPc4	průmyslník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
vládním	vládní	k2eAgInSc6d1	vládní
speciálu	speciál	k1gInSc6	speciál
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
kancléřku	kancléřka	k1gFnSc4	kancléřka
Merkelovou	Merkelová	k1gFnSc4	Merkelová
<g/>
.	.	kIx.	.
</s>
<s>
Chytré	chytrý	k2eAgInPc1d1	chytrý
telefony	telefon	k1gInPc1	telefon
nechávají	nechávat	k5eAaImIp3nP	nechávat
doma	doma	k6eAd1	doma
a	a	k8xC	a
i	i	k9	i
řadoví	řadový	k2eAgMnPc1d1	řadový
úředníci	úředník	k1gMnPc1	úředník
z	z	k7c2	z
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
a	a	k8xC	a
zahraničí	zahraničí	k1gNnSc2	zahraničí
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
používají	používat	k5eAaImIp3nP	používat
ty	ten	k3xDgInPc1	ten
nejlacinější	laciný	k2eAgInPc1d3	nejlacinější
tlačítkové	tlačítkový	k2eAgInPc1d1	tlačítkový
přístroje	přístroj	k1gInPc1	přístroj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
vyhazují	vyhazovat	k5eAaImIp3nP	vyhazovat
do	do	k7c2	do
elektrošrotu	elektrošrot	k1gInSc2	elektrošrot
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
ČLR	ČLR	kA	ČLR
je	být	k5eAaImIp3nS	být
praxe	praxe	k1gFnSc1	praxe
"	"	kIx"	"
<g/>
domácí	domácí	k2eAgInSc1d1	domácí
med	med	k1gInSc1	med
z	z	k7c2	z
cizích	cizí	k2eAgFnPc2d1	cizí
kytek	kytka	k1gFnPc2	kytka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Picking	Picking	k1gInSc1	Picking
flowers	flowers	k1gInSc1	flowers
in	in	k?	in
foreign	foreigna	k1gFnPc2	foreigna
lands	lands	k6eAd1	lands
to	ten	k3xDgNnSc4	ten
make	make	k1gNnSc4	make
honey	honea	k1gFnSc2	honea
in	in	k?	in
China	China	k1gFnSc1	China
<g/>
)	)	kIx)	)
realizována	realizovat	k5eAaBmNgFnS	realizovat
poskytováním	poskytování	k1gNnSc7	poskytování
státních	státní	k2eAgNnPc2d1	státní
stipendií	stipendium	k1gNnPc2	stipendium
pro	pro	k7c4	pro
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
pobyty	pobyt	k1gInPc4	pobyt
studentů	student	k1gMnPc2	student
a	a	k8xC	a
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
pedagogů	pedagog	k1gMnPc2	pedagog
na	na	k7c6	na
prestižních	prestižní	k2eAgFnPc6d1	prestižní
západních	západní	k2eAgFnPc6d1	západní
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vysílá	vysílat	k5eAaImIp3nS	vysílat
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
přímo	přímo	k6eAd1	přímo
čínská	čínský	k2eAgFnSc1d1	čínská
National	National	k1gFnSc1	National
University	universita	k1gFnSc2	universita
of	of	k?	of
Defense	defense	k1gFnSc2	defense
Technology	technolog	k1gMnPc4	technolog
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
vysocí	vysoký	k2eAgMnPc1d1	vysoký
důstojníci	důstojník	k1gMnPc1	důstojník
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
skrytou	skrytý	k2eAgFnSc7d1	skrytá
identitou	identita	k1gFnSc7	identita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životopise	životopis	k1gInSc6	životopis
uvádějí	uvádět	k5eAaImIp3nP	uvádět
čínské	čínský	k2eAgFnPc1d1	čínská
výzkumné	výzkumný	k2eAgFnPc1d1	výzkumná
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neexistují	existovat	k5eNaImIp3nP	existovat
nebo	nebo	k8xC	nebo
instituce	instituce	k1gFnPc1	instituce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
zastírá	zastírat	k5eAaImIp3nS	zastírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vojenské	vojenský	k2eAgNnSc4d1	vojenské
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Changsha	Changsha	k1gMnSc1	Changsha
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc4	technolog
je	on	k3xPp3gInPc4	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Vědecko-technologická	vědeckoechnologický	k2eAgFnSc1d1	vědecko-technologická
univerzita	univerzita	k1gFnSc1	univerzita
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
Lidově	lidově	k6eAd1	lidově
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
armády	armáda	k1gFnSc2	armáda
nebo	nebo	k8xC	nebo
Xi	Xi	k1gFnSc2	Xi
<g/>
'	'	kIx"	'
<g/>
an	an	k?	an
Institute	institut	k1gInSc5	institut
of	of	k?	of
High	High	k1gInSc1	High
Technology	technolog	k1gMnPc4	technolog
je	on	k3xPp3gInPc4	on
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
Univerzita	univerzita	k1gFnSc1	univerzita
vojenského	vojenský	k2eAgNnSc2d1	vojenské
raketového	raketový	k2eAgNnSc2d1	raketové
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
univerzity	univerzita	k1gFnPc1	univerzita
si	se	k3xPyFc3	se
často	často	k6eAd1	často
ani	ani	k8xC	ani
nejsou	být	k5eNaImIp3nP	být
vědomy	vědom	k2eAgFnPc1d1	vědoma
<g/>
,	,	kIx,	,
že	že	k8xS	že
čínští	čínský	k2eAgMnPc1d1	čínský
vědci	vědec	k1gMnPc1	vědec
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
armádní	armádní	k2eAgMnSc1d1	armádní
experti	expert	k1gMnPc1	expert
a	a	k8xC	a
nechávají	nechávat	k5eAaImIp3nP	nechávat
celou	celý	k2eAgFnSc4d1	celá
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
prověrky	prověrka	k1gFnPc4	prověrka
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
zájmu	zájem	k1gInSc2	zájem
patří	patřit	k5eAaImIp3nS	patřit
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
výzkumu	výzkum	k1gInSc6	výzkum
hypersonických	hypersonický	k2eAgFnPc2d1	hypersonická
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
navigační	navigační	k2eAgFnSc1d1	navigační
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
kryptografie	kryptografie	k1gFnSc1	kryptografie
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgNnPc4d1	autonomní
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Špionáž	špionáž	k1gFnSc1	špionáž
ČLR	ČLR	kA	ČLR
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
informační	informační	k2eAgFnSc2d1	informační
služby	služba	k1gFnSc2	služba
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
intenzita	intenzita	k1gFnSc1	intenzita
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
činnosti	činnost	k1gFnSc2	činnost
čínských	čínský	k2eAgMnPc2d1	čínský
zpravodajců	zpravodajce	k1gMnPc2	zpravodajce
působících	působící	k2eAgMnPc2d1	působící
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pod	pod	k7c7	pod
diplomatickým	diplomatický	k2eAgNnSc7d1	diplomatické
krytím	krytí	k1gNnSc7	krytí
a	a	k8xC	a
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
činnost	činnost	k1gFnSc1	činnost
proti	proti	k7c3	proti
českým	český	k2eAgInPc3d1	český
cílům	cíl	k1gInPc3	cíl
vedená	vedený	k2eAgFnSc1d1	vedená
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
vystavení	vystavení	k1gNnSc2	vystavení
českých	český	k2eAgMnPc2d1	český
občanů	občan	k1gMnPc2	občan
čínskému	čínský	k2eAgInSc3d1	čínský
špionážnímu	špionážní	k2eAgInSc3d1	špionážní
zájmu	zájem	k1gInSc3	zájem
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
.	.	kIx.	.
</s>
<s>
Špionážní	špionážní	k2eAgFnSc2d1	špionážní
aktivity	aktivita	k1gFnSc2	aktivita
vedené	vedený	k2eAgFnSc2d1	vedená
proti	proti	k7c3	proti
českým	český	k2eAgInPc3d1	český
cílům	cíl	k1gInPc3	cíl
a	a	k8xC	a
zájmům	zájem	k1gInPc3	zájem
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
segmentů	segment	k1gInPc2	segment
<g/>
:	:	kIx,	:
Narušení	narušení	k1gNnSc1	narušení
jednotné	jednotný	k2eAgFnSc2d1	jednotná
politiky	politika	k1gFnSc2	politika
EU	EU	kA	EU
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
českých	český	k2eAgFnPc2d1	Česká
entit	entita	k1gFnPc2	entita
<g/>
;	;	kIx,	;
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
činnost	činnost	k1gFnSc1	činnost
zacílená	zacílený	k2eAgFnSc1d1	zacílená
na	na	k7c4	na
české	český	k2eAgInPc4d1	český
silové	silový	k2eAgInPc4d1	silový
resorty	resort	k1gInPc4	resort
a	a	k8xC	a
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
vědecko-technická	vědeckoechnický	k2eAgFnSc1d1	vědecko-technická
špionáž	špionáž	k1gFnSc1	špionáž
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
mise	mise	k1gFnSc1	mise
také	také	k9	také
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
schopnost	schopnost	k1gFnSc4	schopnost
čínské	čínský	k2eAgFnSc2d1	čínská
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
a	a	k8xC	a
ovládat	ovládat	k5eAaImF	ovládat
čínskou	čínský	k2eAgFnSc4d1	čínská
krajanskou	krajanský	k2eAgFnSc4d1	krajanská
komunitu	komunita	k1gFnSc4	komunita
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
BIS	BIS	kA	BIS
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
znepokojující	znepokojující	k2eAgInSc4d1	znepokojující
vývoj	vývoj	k1gInSc4	vývoj
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
čínských	čínský	k2eAgFnPc2d1	čínská
aktivit	aktivita	k1gFnPc2	aktivita
(	(	kIx(	(
<g/>
politických	politický	k2eAgFnPc2d1	politická
<g/>
,	,	kIx,	,
špionážních	špionážní	k2eAgFnPc2d1	špionážní
<g/>
,	,	kIx,	,
legislativních	legislativní	k2eAgFnPc2d1	legislativní
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
jako	jako	k9	jako
komplex	komplex	k1gInSc1	komplex
představují	představovat	k5eAaImIp3nP	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
a	a	k8xC	a
vědecko-technické	vědeckoechnický	k2eAgFnSc2d1	vědecko-technická
rozvědky	rozvědka	k1gFnSc2	rozvědka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
===	===	k?	===
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
ekonomickým	ekonomický	k2eAgFnPc3d1	ekonomická
reformám	reforma	k1gFnPc3	reforma
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
začal	začít	k5eAaPmAgInS	začít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
prosazovat	prosazovat	k5eAaImF	prosazovat
Teng	Teng	k1gInSc4	Teng
Siao-pching	Siaoching	k1gInSc1	Siao-pching
a	a	k8xC	a
na	na	k7c4	na
které	který	k3yRgInPc4	který
navázali	navázat	k5eAaPmAgMnP	navázat
jeho	jeho	k3xOp3gMnPc1	jeho
nástupci	nástupce	k1gMnPc1	nástupce
v	v	k7c6	v
90.	[number]	k4	90.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
ČLR	ČLR	kA	ČLR
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90.	[number]	k4	90.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
nastartován	nastartován	k2eAgInSc4d1	nastartován
rychlý	rychlý	k2eAgInSc4d1	rychlý
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
země	zem	k1gFnSc2	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
rostla	růst	k5eAaImAgFnS	růst
tempem	tempo	k1gNnSc7	tempo
11,9	[number]	k4	11,9
%	%	kIx~	%
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgMnSc6d3	nejvyšší
za	za	k7c4	za
13	[number]	k4	13
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
tak	tak	k6eAd1	tak
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
cifry	cifra	k1gFnSc2	cifra
24,953	[number]	k4	24,953
biliónů	bilión	k4xCgInPc2	bilión
CNY	CNY	kA	CNY
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
3,428	[number]	k4	3,428
biliónů	bilión	k4xCgInPc2	bilión
USD	USD	kA	USD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
dvouciferný	dvouciferný	k2eAgInSc1d1	dvouciferný
růst	růst	k1gInSc1	růst
způsobil	způsobit	k5eAaPmAgInS	způsobit
zvýšení	zvýšení	k1gNnSc4	zvýšení
inflace	inflace	k1gFnSc2	inflace
na	na	k7c4	na
4,8	[number]	k4	4,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
necelým	celý	k2eNgInPc3d1	necelý
2	[number]	k4	2
%	%	kIx~	%
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
inflace	inflace	k1gFnSc1	inflace
dokonce	dokonce	k9	dokonce
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
až	až	k9	až
na	na	k7c4	na
8,7	[number]	k4	8,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jedenáctileté	jedenáctiletý	k2eAgNnSc4d1	jedenáctileté
maximum	maximum	k1gNnSc4	maximum
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
vývoz	vývoz	k1gInSc1	vývoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
o	o	k7c4	o
27,2	[number]	k4	27,2
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
dovoz	dovoz	k1gInSc1	dovoz
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
narostl	narůst	k5eAaPmAgInS	narůst
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Saldo	saldo	k1gNnSc1	saldo
obchodní	obchodní	k2eAgFnSc2d1	obchodní
bilance	bilance	k1gFnSc2	bilance
tak	tak	k6eAd1	tak
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
přebytku	přebytek	k1gInSc2	přebytek
177,5	[number]	k4	177,5
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
100	[number]	k4	100
mld	mld	k?	mld
<g/>
.	.	kIx.	.
</s>
<s>
USD	USD	kA	USD
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
ekonomika	ekonomika	k1gFnSc1	ekonomika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
britskou	britský	k2eAgFnSc7d1	britská
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
německou	německý	k2eAgFnSc4d1	německá
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
s	s	k7c7	s
objemem	objem	k1gInSc7	objem
HDP	HDP	kA	HDP
2,49	[number]	k4	2,49
bilionu	bilion	k4xCgInSc2	bilion
€	€	k?	€
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
tehdy	tehdy	k6eAd1	tehdy
na	na	k7c4	na
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
Japonsko	Japonsko	k1gNnSc4	Japonsko
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
státní	státní	k2eAgFnSc7d1	státní
ekonomikou	ekonomika	k1gFnSc7	ekonomika
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
co	co	k9	co
do	do	k7c2	do
objemu	objem	k1gInSc2	objem
obchodovaného	obchodovaný	k2eAgNnSc2d1	obchodované
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2014	[number]	k4	2014
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc4d1	měnový
fond	fond	k1gInSc4	fond
ekonomiku	ekonomika	k1gFnSc4	ekonomika
Číny	Čína	k1gFnSc2	Čína
za	za	k7c4	za
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
ukazateli	ukazatel	k1gInSc6	ukazatel
HDP	HDP	kA	HDP
dle	dle	k7c2	dle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgMnPc1	tři
čínští	čínský	k2eAgMnPc1d1	čínský
internetoví	internetový	k2eAgMnPc1d1	internetový
"	"	kIx"	"
<g/>
giganti	gigant	k1gMnPc1	gigant
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Baidu	Baido	k1gNnSc6	Baido
<g/>
,	,	kIx,	,
Tencent	Tencent	k1gMnSc1	Tencent
a	a	k8xC	a
Alibaba	Alibaba	k1gMnSc1	Alibaba
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
technologické	technologický	k2eAgInPc4d1	technologický
konglomeráty	konglomerát	k1gInPc4	konglomerát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
výrobci	výrobce	k1gMnPc1	výrobce
spotřební	spotřební	k2eAgFnSc2d1	spotřební
elektroniky	elektronika	k1gFnSc2	elektronika
jako	jako	k8xC	jako
Lenovo	Lenovo	k1gNnSc1	Lenovo
<g/>
,	,	kIx,	,
Huawei	Huawei	k1gNnSc1	Huawei
<g/>
,	,	kIx,	,
ZTE	ZTE	kA	ZTE
<g/>
,	,	kIx,	,
Xiaomi	Xiao	k1gFnPc7	Xiao
<g/>
,	,	kIx,	,
Oppo	Oppo	k1gNnSc4	Oppo
a	a	k8xC	a
Meizu	Meiza	k1gFnSc4	Meiza
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetika	k1gFnSc1	energetika
===	===	k?	===
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
energetika	energetika	k1gFnSc1	energetika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
dostupných	dostupný	k2eAgInPc6d1	dostupný
typech	typ	k1gInPc6	typ
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
především	především	k9	především
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
elektrárna	elektrárna	k1gFnSc1	elektrárna
Tři	tři	k4xCgInPc4	tři
soutěsky	soutěsk	k1gInPc1	soutěsk
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ze	z	k7c2	z
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgFnSc2d3	veliký
elektrárnou	elektrárna	k1gFnSc7	elektrárna
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
vodní	vodní	k2eAgInPc1d1	vodní
<g/>
)	)	kIx)	)
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
velmi	velmi	k6eAd1	velmi
ambiciozní	ambiciozní	k2eAgInSc1d1	ambiciozní
projekt	projekt	k1gInSc1	projekt
výstavby	výstavba	k1gFnSc2	výstavba
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
výkon	výkon	k1gInSc4	výkon
zařízení	zařízení	k1gNnSc2	zařízení
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
dvojnásobný	dvojnásobný	k2eAgInSc1d1	dvojnásobný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výstavbě	výstavba	k1gFnSc6	výstavba
jaderných	jaderný	k2eAgInPc2d1	jaderný
reaktorů	reaktor	k1gInPc2	reaktor
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
pracují	pracovat	k5eAaImIp3nP	pracovat
jak	jak	k6eAd1	jak
francouzské	francouzský	k2eAgFnPc1d1	francouzská
<g/>
,	,	kIx,	,
tak	tak	k9	tak
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
Fukušimě	Fukušima	k1gFnSc6	Fukušima
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
dočasně	dočasně	k6eAd1	dočasně
pozastaveno	pozastaven	k2eAgNnSc4d1	pozastaveno
vydávání	vydávání	k1gNnSc4	vydávání
povolení	povolení	k1gNnSc2	povolení
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
nových	nový	k2eAgFnPc2d1	nová
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
provedeny	proveden	k2eAgFnPc1d1	provedena
prověrky	prověrka	k1gFnPc1	prověrka
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
stávajících	stávající	k2eAgFnPc6d1	stávající
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Čína	Čína	k1gFnSc1	Čína
hodlá	hodlat	k5eAaImIp3nS	hodlat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
projaderné	projaderný	k2eAgFnSc6d1	projaderná
politice	politika	k1gFnSc6	politika
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Nová	nový	k2eAgFnSc1d1	nová
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
oznámil	oznámit	k5eAaPmAgMnS	oznámit
čínský	čínský	k2eAgMnSc1d1	čínský
prezident	prezident	k1gMnSc1	prezident
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
záměr	záměr	k1gInSc4	záměr
vybudování	vybudování	k1gNnSc1	vybudování
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgFnPc1d1	Nové
hedvábné	hedvábný	k2eAgFnPc1d1	hedvábná
stezky	stezka	k1gFnPc1	stezka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
vhodného	vhodný	k2eAgNnSc2d1	vhodné
zboží	zboží	k1gNnSc2	zboží
v	v	k7c6	v
kontejnerech	kontejner	k1gInPc6	kontejner
mají	mít	k5eAaImIp3nP	mít
přitom	přitom	k6eAd1	přitom
být	být	k5eAaImF	být
využity	využít	k5eAaPmNgFnP	využít
již	již	k6eAd1	již
existující	existující	k2eAgFnPc1d1	existující
železniční	železniční	k2eAgFnPc1d1	železniční
trati	trať	k1gFnPc1	trať
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
samotné	samotný	k2eAgFnSc6d1	samotná
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Transsibiřská	transsibiřský	k2eAgFnSc1d1	Transsibiřská
magistrála	magistrála	k1gFnSc1	magistrála
a	a	k8xC	a
tratě	trať	k1gFnPc1	trať
přes	přes	k7c4	přes
evropské	evropský	k2eAgNnSc4d1	Evropské
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
Bělorusko	Bělorusko	k1gNnSc4	Bělorusko
<g/>
,	,	kIx,	,
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
Duisburg	Duisburg	k1gInSc1	Duisburg
důležitým	důležitý	k2eAgNnSc7d1	důležité
překladištěm	překladiště	k1gNnSc7	překladiště
u	u	k7c2	u
splavné	splavný	k2eAgFnSc2d1	splavná
řeky	řeka	k1gFnSc2	řeka
Rýn	rýna	k1gFnPc2	rýna
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
hedvábnou	hedvábný	k2eAgFnSc4d1	hedvábná
stezku	stezka	k1gFnSc4	stezka
napojeny	napojen	k2eAgFnPc4d1	napojena
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
15.	[number]	k4	15.
února	únor	k1gInSc2	únor
2016	[number]	k4	2016
přijel	přijet	k5eAaPmAgMnS	přijet
první	první	k4xOgInSc4	první
vlak	vlak	k1gInSc4	vlak
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
čínské	čínský	k2eAgFnSc2d1	čínská
provincie	provincie	k1gFnSc2	provincie
Če-ťiang	Če-ťianga	k1gFnPc2	Če-ťianga
do	do	k7c2	do
Teheránu	Teherán	k1gInSc2	Teherán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
vypraven	vypravit	k5eAaPmNgInS	vypravit
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
až	až	k9	až
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
celou	celý	k2eAgFnSc4d1	celá
trasu	trasa	k1gFnSc4	trasa
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
12 000	[number]	k4	12 000
km	km	kA	km
ujel	ujet	k5eAaPmAgMnS	ujet
za	za	k7c4	za
plánovaných	plánovaný	k2eAgInPc2d1	plánovaný
18	[number]	k4	18
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Zboží	zboží	k1gNnSc1	zboží
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
ovšem	ovšem	k9	ovšem
několikrát	několikrát	k6eAd1	několikrát
překládáno	překládán	k2eAgNnSc1d1	překládáno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kvůli	kvůli	k7c3	kvůli
rozdílným	rozdílný	k2eAgInPc3d1	rozdílný
rozchodům	rozchod	k1gInPc3	rozchod
kolejí	kolej	k1gFnPc2	kolej
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
např.	např.	kA	např.
mezi	mezi	k7c7	mezi
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
měněny	měnit	k5eAaImNgFnP	měnit
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
i	i	k8xC	i
vagóny	vagón	k1gInPc1	vagón
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
napojení	napojení	k1gNnSc4	napojení
Rakouska	Rakousko	k1gNnSc2	Rakousko
přes	přes	k7c4	přes
Slovensko	Slovensko	k1gNnSc4	Slovensko
na	na	k7c4	na
čínský	čínský	k2eAgInSc4d1	čínský
projekt	projekt	k1gInSc4	projekt
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
nová	nový	k2eAgFnSc1d1	nová
asi	asi	k9	asi
450	[number]	k4	450
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
širokorozchodná	širokorozchodný	k2eAgFnSc1d1	širokorozchodná
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Košic	Košice	k1gInPc2	Košice
(	(	kIx(	(
<g/>
kam	kam	k6eAd1	kam
již	již	k9	již
taková	takový	k3xDgFnSc1	takový
trať	trať	k1gFnSc1	trať
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Užhorodu	Užhorod	k1gInSc2	Užhorod
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
Bratislavu	Bratislava	k1gFnSc4	Bratislava
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
mají	mít	k5eAaImIp3nP	mít
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
spolkové	spolkový	k2eAgFnSc2d1	spolková
dráhy	dráha	k1gFnSc2	dráha
velký	velký	k2eAgInSc1d1	velký
zájem	zájem	k1gInSc1	zájem
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
mu	on	k3xPp3gInSc3	on
však	však	k9	však
brání	bránit	k5eAaImIp3nS	bránit
válka	válka	k1gFnSc1	válka
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
<g/>
Tzv.	tzv.	kA	tzv.
Nová	nový	k2eAgFnSc1d1	nová
hedvábná	hedvábný	k2eAgFnSc1d1	hedvábná
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Jeden	jeden	k4xCgInSc1	jeden
pás	pás	k1gInSc1	pás
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
One	One	k1gMnSc1	One
Belt	Belt	k1gMnSc1	Belt
<g/>
,	,	kIx,	,
One	One	k1gMnSc1	One
Road	Road	k1gMnSc1	Road
<g/>
,	,	kIx,	,
zkr.	zkr.	kA	zkr.
OBOR	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
též	též	k9	též
měkčím	měkký	k2eAgNnSc7d2	měkčí
vyjádřením	vyjádření	k1gNnSc7	vyjádření
Belt	Belt	k1gMnSc1	Belt
and	and	k?	and
Road	Road	k1gMnSc1	Road
Iniciative	Iniciativ	k1gInSc5	Iniciativ
<g/>
,	,	kIx,	,
BRI	BRI	kA	BRI
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
oficiálně	oficiálně	k6eAd1	oficiálně
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
dopravních	dopravní	k2eAgInPc2d1	dopravní
koridorů	koridor	k1gInPc2	koridor
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
propojí	propojit	k5eAaPmIp3nS	propojit
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
čínských	čínský	k2eAgFnPc2d1	čínská
velmocenských	velmocenský	k2eAgFnPc2d1	velmocenská
ambicí	ambice	k1gFnPc2	ambice
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
významným	významný	k2eAgInSc7d1	významný
strategickým	strategický	k2eAgInSc7d1	strategický
prvkem	prvek	k1gInSc7	prvek
a	a	k8xC	a
začátkem	začátek	k1gInSc7	začátek
čínské	čínský	k2eAgFnSc2d1	čínská
cesty	cesta	k1gFnSc2	cesta
globalizace	globalizace	k1gFnSc2	globalizace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
mocenského	mocenský	k2eAgInSc2d1	mocenský
boje	boj	k1gInSc2	boj
s	s	k7c7	s
USA	USA	kA	USA
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
omezit	omezit	k5eAaPmF	omezit
vliv	vliv	k1gInSc4	vliv
Japonska	Japonsko	k1gNnSc2	Japonsko
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Indočíně	Indočína	k1gFnSc6	Indočína
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
investice	investice	k1gFnPc4	investice
navázat	navázat	k5eAaPmF	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Příslib	příslib	k1gInSc1	příslib
čínských	čínský	k2eAgFnPc2d1	čínská
investic	investice	k1gFnPc2	investice
je	být	k5eAaImIp3nS	být
vykládán	vykládat	k5eAaImNgInS	vykládat
jako	jako	k8xS	jako
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
nástroj	nástroj	k1gInSc1	nástroj
Číny	Čína	k1gFnSc2	Čína
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
spojence	spojenec	k1gMnPc4	spojenec
pro	pro	k7c4	pro
přiznání	přiznání	k1gNnSc4	přiznání
statutu	statut	k1gInSc2	statut
tržní	tržní	k2eAgFnSc2d1	tržní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
bruselských	bruselský	k2eAgMnPc2d1	bruselský
diplomatů	diplomat	k1gMnPc2	diplomat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
důležitým	důležitý	k2eAgInSc7d1	důležitý
zájmem	zájem	k1gInSc7	zájem
čínské	čínský	k2eAgFnSc2d1	čínská
agendy	agenda	k1gFnSc2	agenda
v	v	k7c6	v
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Peking	Peking	k1gInSc1	Peking
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
také	také	k9	také
přes	přes	k7c4	přes
Východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
iniciativy	iniciativa	k1gFnSc2	iniciativa
16	[number]	k4	16
+	+	kIx~	+
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
vyjednávání	vyjednávání	k1gNnPc4	vyjednávání
otevřená	otevřený	k2eAgNnPc4d1	otevřené
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
multilaterální	multilaterální	k2eAgFnSc6d1	multilaterální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pouze	pouze	k6eAd1	pouze
formou	forma	k1gFnSc7	forma
bilaterálních	bilaterální	k2eAgInPc2d1	bilaterální
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
užší	úzký	k2eAgFnSc3d2	užší
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
ČLR	ČLR	kA	ČLR
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
ekonomického	ekonomický	k2eAgMnSc4d1	ekonomický
rivala	rival	k1gMnSc4	rival
usilujícího	usilující	k2eAgMnSc4d1	usilující
o	o	k7c6	o
vůdčí	vůdčí	k2eAgFnSc6d1	vůdčí
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
zavádění	zavádění	k1gNnSc6	zavádění
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
systémového	systémový	k2eAgMnSc2d1	systémový
rivala	rival	k1gMnSc2	rival
v	v	k7c6	v
prosazování	prosazování	k1gNnSc6	prosazování
alternativních	alternativní	k2eAgInPc2d1	alternativní
modelů	model	k1gInPc2	model
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Z	z	k7c2	z
interních	interní	k2eAgInPc2d1	interní
čínských	čínský	k2eAgInPc2d1	čínský
dokumentů	dokument	k1gInPc2	dokument
lze	lze	k6eAd1	lze
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
mezi	mezi	k7c4	mezi
běžné	běžný	k2eAgFnPc4d1	běžná
obyvatele	obyvatel	k1gMnSc2	obyvatel
západních	západní	k2eAgFnPc2d1	západní
zemí	zem	k1gFnPc2	zem
čínská	čínský	k2eAgFnSc1d1	čínská
externí	externí	k2eAgFnSc1d1	externí
propaganda	propaganda	k1gFnSc1	propaganda
nepronikla	proniknout	k5eNaPmAgFnS	proniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
tamějšími	tamější	k2eAgMnPc7d1	tamější
politiky	politik	k1gMnPc7	politik
dezinterpretována	dezinterpretován	k2eAgFnSc1d1	dezinterpretován
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
čínská	čínský	k2eAgFnSc1d1	čínská
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
korigovat	korigovat	k5eAaBmF	korigovat
náš	náš	k3xOp1gInSc4	náš
přístup	přístup	k1gInSc4	přístup
a	a	k8xC	a
zaměřit	zaměřit	k5eAaPmF	zaměřit
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
vyspělými	vyspělý	k2eAgInPc7d1	vyspělý
státy	stát	k1gInPc7	stát
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Ameriky	Amerika	k1gFnSc2	Amerika
představují	představovat	k5eAaImIp3nP	představovat
skupinu	skupina	k1gFnSc4	skupina
nejvhodnější	vhodný	k2eAgFnSc4d3	nejvhodnější
k	k	k7c3	k
"	"	kIx"	"
<g/>
indoktrinaci	indoktrinace	k1gFnSc3	indoktrinace
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
doslova	doslova	k6eAd1	doslova
<g/>
:	:	kIx,	:
k	k	k7c3	k
vymývání	vymývání	k1gNnSc3	vymývání
mozků	mozek	k1gInPc2	mozek
a	a	k8xC	a
získávání	získávání	k1gNnSc4	získávání
srdcí	srdce	k1gNnPc2	srdce
[	[	kIx(	[
<g/>
si	se	k3xPyFc3	se
nao	nao	k?	nao
jing	jinga	k1gFnPc2	jinga
sin	sin	kA	sin
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
rozvojové	rozvojový	k2eAgFnPc1d1	rozvojová
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tato	tento	k3xDgFnSc1	tento
čínská	čínský	k2eAgFnSc1d1	čínská
strategie	strategie	k1gFnSc1	strategie
Hedvábné	hedvábný	k2eAgFnSc2d1	hedvábná
stezky	stezka	k1gFnSc2	stezka
má	mít	k5eAaImIp3nS	mít
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
korupčních	korupční	k2eAgInPc2d1	korupční
skandálů	skandál	k1gInPc2	skandál
a	a	k8xC	a
astronomického	astronomický	k2eAgNnSc2d1	astronomické
zadlužení	zadlužení	k1gNnSc2	zadlužení
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
přistoupily	přistoupit	k5eAaPmAgInP	přistoupit
na	na	k7c4	na
nabídky	nabídka	k1gFnPc4	nabídka
čínských	čínský	k2eAgFnPc2d1	čínská
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
přibližně	přibližně	k6eAd1	přibližně
1 370 536 875	[number]	k4	1 370 536 875
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
16,60	[number]	k4	16,60
<g/>
%	%	kIx~	%
populace	populace	k1gFnPc4	populace
bylo	být	k5eAaImAgNnS	být
14	[number]	k4	14
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
mladších	mladý	k2eAgMnPc2d2	mladší
<g/>
,	,	kIx,	,
70,14	[number]	k4	70,14
<g/>
%	%	kIx~	%
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
až	až	k9	až
59	[number]	k4	59
let	léto	k1gNnPc2	léto
a	a	k8xC	a
13,26	[number]	k4	13,26
<g/>
%	%	kIx~	%
bylo	být	k5eAaImAgNnS	být
starších	starý	k2eAgFnPc2d2	starší
než	než	k8xS	než
60	[number]	k4	60
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Míra	Míra	k1gFnSc1	Míra
populačního	populační	k2eAgInSc2d1	populační
růstu	růst	k1gInSc2	růst
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
0,46	[number]	k4	0,46
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Navzdory	navzdory	k7c3	navzdory
politice	politika	k1gFnSc3	politika
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ČLR	ČLR	kA	ČLR
zavedla	zavést	k5eAaPmAgFnS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Číny	Čína	k1gFnSc2	Čína
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
550	[number]	k4	550
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
ČLR	ČLR	kA	ČLR
na	na	k7c4	na
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnPc4	miliarda
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
<g/>
Čína	Čína	k1gFnSc1	Čína
tvořila	tvořit	k5eAaImAgFnS	tvořit
většinu	většina	k1gFnSc4	většina
chudých	chudý	k2eAgInPc2d1	chudý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
Čína	Čína	k1gFnSc1	Čína
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
světové	světový	k2eAgFnSc2d1	světová
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Etnické	etnický	k2eAgFnSc2d1	etnická
skupiny	skupina	k1gFnSc2	skupina
===	===	k?	===
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
oficiálně	oficiálně	k6eAd1	oficiálně
uznává	uznávat	k5eAaImIp3nS	uznávat
56	[number]	k4	56
různých	různý	k2eAgFnPc2d1	různá
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
představují	představovat	k5eAaImIp3nP	představovat
Chanové	Chan	k1gMnPc1	Chan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
91,51	[number]	k4	91,51
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
</s>
</p>
<p>
<s>
populace	populace	k1gFnPc1	populace
<g/>
.	.	kIx.	.
</s>
<s>
Chanové	Chanová	k1gFnPc1	Chanová
-	-	kIx~	-
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
světová	světový	k2eAgFnSc1d1	světová
etnická	etnický	k2eAgFnSc1d1	etnická
skupina	skupina	k1gFnSc1	skupina
-	-	kIx~	-
převyšují	převyšovat	k5eAaImIp3nP	převyšovat
ostatní	ostatní	k2eAgFnPc1d1	ostatní
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
provincii	provincie	k1gFnSc6	provincie
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Tibetu	Tibet	k1gInSc2	Tibet
a	a	k8xC	a
Sin-ťiangu	Sin-ťiang	k1gInSc2	Sin-ťiang
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
tvoří	tvořit	k5eAaImIp3nP	tvořit
etnické	etnický	k2eAgFnPc1d1	etnická
menšiny	menšina	k1gFnPc1	menšina
asi	asi	k9	asi
8,49	[number]	k4	8,49
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
sčítáním	sčítání	k1gNnSc7	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
populace	populace	k1gFnSc1	populace
Chanů	Chan	k1gMnPc2	Chan
o	o	k7c4	o
66 537 177	[number]	k4	66 537 177
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
o	o	k7c4	o
5,74	[number]	k4	5,74
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
populace	populace	k1gFnSc1	populace
55	[number]	k4	55
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
7 362 627	[number]	k4	7 362 627
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
i	i	k9	i
6,92	[number]	k4	6,92
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
z	z	k7c2	z
ruku	ruka	k1gFnSc4	ruka
2010	[number]	k4	2010
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
celkem	celek	k1gInSc7	celek
593 832	[number]	k4	593 832
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
žijících	žijící	k2eAgMnPc2d1	žijící
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
skupiny	skupina	k1gFnPc1	skupina
pocházely	pocházet	k5eAaImAgFnP	pocházet
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Koreje	Korea	k1gFnSc2	Korea
(	(	kIx(	(
<g/>
120	[number]	k4	120
750	[number]	k4	750
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
71	[number]	k4	71
493	[number]	k4	493
<g/>
)	)	kIx)	)
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
(	(	kIx(	(
<g/>
66	[number]	k4	66
159	[number]	k4	159
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
etnické	etnický	k2eAgFnPc4d1	etnická
menšiny	menšina	k1gFnPc4	menšina
(	(	kIx(	(
<g/>
údaje	údaj	k1gInPc4	údaj
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
Čuangové	Čuangový	k2eAgNnSc1d1	Čuangový
(	(	kIx(	(
<g/>
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mandžuové	Mandžu	k1gMnPc1	Mandžu
(	(	kIx(	(
<g/>
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ujgurové	Ujgur	k1gMnPc1	Ujgur
(	(	kIx(	(
<g/>
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chuejové	Chuej	k1gMnPc1	Chuej
(	(	kIx(	(
<g/>
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miaové	Miaové	k2eAgFnSc1d1	Miaové
(	(	kIx(	(
<g/>
8	[number]	k4	8
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Iové	Iové	k1gNnSc1	Iové
(	(	kIx(	(
<g/>
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tujia	Tujia	k1gFnSc1	Tujia
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
<g/>
75	[number]	k4	75
milionu	milion	k4xCgInSc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mongolové	Mongol	k1gMnPc1	Mongol
(	(	kIx(	(
<g/>
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tibeťané	Tibeťan	k1gMnPc1	Tibeťan
(	(	kIx(	(
<g/>
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Puejové	Puej	k1gMnPc1	Puej
(	(	kIx(	(
<g/>
3	[number]	k4	3
miliony	milion	k4xCgInPc4	milion
<g/>
)	)	kIx)	)
a	a	k8xC	a
Korejci	Korejec	k1gMnPc1	Korejec
(	(	kIx(	(
<g/>
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jazyky	jazyk	k1gInPc1	jazyk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
292	[number]	k4	292
aktivních	aktivní	k2eAgInPc2d1	aktivní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanější	používaný	k2eAgNnSc1d3	nejpoužívanější
náleží	náležet	k5eAaImIp3nS	náležet
do	do	k7c2	do
sinotibetských	sinotibetský	k2eAgInPc2d1	sinotibetský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
mandarínštinu	mandarínština	k1gFnSc4	mandarínština
(	(	kIx(	(
<g/>
hovoří	hovořit	k5eAaImIp3nS	hovořit
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
70	[number]	k4	70
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
varianty	varianta	k1gFnPc1	varianta
čínštiny	čínština	k1gFnSc2	čínština
<g/>
:	:	kIx,	:
Jüe	Jüe	k1gFnSc1	Jüe
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
kantonštiny	kantonština	k1gFnSc2	kantonština
a	a	k8xC	a
tchajšanštiny	tchajšanština	k1gFnSc2	tchajšanština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
wu	wu	k?	wu
<g/>
,	,	kIx,	,
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
xiang	xianga	k1gFnPc2	xianga
<g/>
,	,	kIx,	,
gan	gan	k?	gan
a	a	k8xC	a
hakka	hakek	k1gInSc2	hakek
<g/>
.	.	kIx.	.
</s>
<s>
Tibetobarmské	Tibetobarmský	k2eAgInPc1d1	Tibetobarmský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tibetštiny	tibetština	k1gFnSc2	tibetština
<g/>
,	,	kIx,	,
qiangu	qiang	k1gInSc2	qiang
<g/>
,	,	kIx,	,
nakhi	nakh	k1gFnSc2	nakh
a	a	k8xC	a
nuosu	nuos	k1gInSc2	nuos
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tibetské	tibetský	k2eAgFnSc2d1	tibetská
náhorní	náhorní	k2eAgFnSc2d1	náhorní
plošiny	plošina	k1gFnSc2	plošina
a	a	k8xC	a
jünnansko-kuejčouské	jünnanskouejčouský	k2eAgFnSc2d1	jünnansko-kuejčouský
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
jazyky	jazyk	k1gInPc7	jazyk
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Číně	Čína	k1gFnSc6	Čína
(	(	kIx(	(
<g/>
čuangština	čuangština	k1gFnSc1	čuangština
<g/>
,	,	kIx,	,
thajština	thajština	k1gFnSc1	thajština
a	a	k8xC	a
kam-sujské	kamujský	k2eAgInPc1d1	kam-sujský
jazyky	jazyk	k1gInPc1	jazyk
z	z	k7c2	z
tajsko-kadajských	tajskoadajský	k2eAgInPc2d1	tajsko-kadajský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
hmong-mienské	hmongienský	k2eAgInPc1d1	hmong-mienský
jazyky	jazyk	k1gInPc1	jazyk
a	a	k8xC	a
wa	wa	k?	wa
z	z	k7c2	z
austroasijské	austroasijský	k2eAgFnSc2d1	austroasijský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napříč	napříč	k7c7	napříč
severovýchodní	severovýchodní	k2eAgFnSc7d1	severovýchodní
a	a	k8xC	a
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
Čínou	Čína	k1gFnSc7	Čína
používají	používat	k5eAaImIp3nP	používat
zdejší	zdejší	k2eAgFnPc1d1	zdejší
etnické	etnický	k2eAgFnPc1d1	etnická
skupiny	skupina	k1gFnPc1	skupina
altajské	altajský	k2eAgFnPc1d1	Altajská
jazyky	jazyk	k1gInPc4	jazyk
včetně	včetně	k7c2	včetně
mandžuštiny	mandžuština	k1gFnSc2	mandžuština
<g/>
,	,	kIx,	,
mongolštiny	mongolština	k1gFnSc2	mongolština
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
turkické	turkický	k2eAgInPc1d1	turkický
jazyky	jazyk	k1gInPc1	jazyk
<g/>
:	:	kIx,	:
ujgurštinu	ujgurština	k1gFnSc4	ujgurština
<g/>
,	,	kIx,	,
kazaštinu	kazaština	k1gFnSc4	kazaština
<g/>
,	,	kIx,	,
kyrgyzštinu	kyrgyzština	k1gFnSc4	kyrgyzština
<g/>
,	,	kIx,	,
salarštinu	salarština	k1gFnSc4	salarština
a	a	k8xC	a
západní	západní	k2eAgFnSc4d1	západní
jugurštinu	jugurština	k1gFnSc4	jugurština
<g/>
.	.	kIx.	.
</s>
<s>
Korejština	korejština	k1gFnSc1	korejština
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
podél	podél	k7c2	podél
hranice	hranice	k1gFnSc2	hranice
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Koreou	Korea	k1gFnSc7	Korea
<g/>
.	.	kIx.	.
</s>
<s>
Saríqólština	Saríqólština	k1gFnSc1	Saríqólština
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
Tádžiků	Tádžik	k1gMnPc2	Tádžik
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Sin-ťiangu	Sin-ťiang	k1gInSc2	Sin-ťiang
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
indoevropský	indoevropský	k2eAgInSc4d1	indoevropský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tchajwanští	tchajwanský	k2eAgMnPc1d1	tchajwanský
domorodci	domorodec	k1gMnPc1	domorodec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
malé	malý	k2eAgFnSc2d1	malá
populace	populace	k1gFnSc2	populace
v	v	k7c6	v
pevninské	pevninský	k2eAgFnSc6d1	pevninská
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
austronéské	austronéský	k2eAgInPc1d1	austronéský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
<g/>
Standardní	standardní	k2eAgFnSc1d1	standardní
čínština	čínština	k1gFnSc1	čínština
<g/>
,	,	kIx,	,
varianta	varianta	k1gFnSc1	varianta
mandarínštiny	mandarínština	k1gFnSc2	mandarínština
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
pekingském	pekingský	k2eAgInSc6d1	pekingský
dialektu	dialekt	k1gInSc6	dialekt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oficiální	oficiální	k2eAgInSc4d1	oficiální
národní	národní	k2eAgInSc4d1	národní
jazyk	jazyk	k1gInSc4	jazyk
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
lingua	lingu	k2eAgFnSc1d1	lingua
franca	franca	k1gFnSc1	franca
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
různého	různý	k2eAgInSc2d1	různý
lingvistického	lingvistický	k2eAgInSc2d1	lingvistický
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
<g/>
Čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
jako	jako	k9	jako
písmo	písmo	k1gNnSc4	písmo
pro	pro	k7c4	pro
sinické	sinický	k2eAgInPc4d1	sinický
jazyky	jazyk	k1gInPc4	jazyk
používají	používat	k5eAaImIp3nP	používat
po	po	k7c6	po
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
mluvčím	mluvčit	k5eAaImIp1nS	mluvčit
vzájemně	vzájemně	k6eAd1	vzájemně
nesrozumitelných	srozumitelný	k2eNgInPc2d1	nesrozumitelný
čínských	čínský	k2eAgInPc2d1	čínský
jazyků	jazyk	k1gInPc2	jazyk
komunikovat	komunikovat	k5eAaImF	komunikovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
psaní	psaní	k1gNnSc2	psaní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vláda	vláda	k1gFnSc1	vláda
představila	představit	k5eAaPmAgFnS	představit
zjednodušené	zjednodušený	k2eAgInPc4d1	zjednodušený
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
pevninské	pevninský	k2eAgFnSc6d1	pevninská
Číně	Čína	k1gFnSc6	Čína
nahradily	nahradit	k5eAaPmAgInP	nahradit
starší	starý	k2eAgInPc1d2	starší
tradiční	tradiční	k2eAgInPc1d1	tradiční
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Čínské	čínský	k2eAgInPc1d1	čínský
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
romanizované	romanizovaný	k2eAgInPc1d1	romanizovaný
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pchin-jinu	pchinin	k1gInSc2	pchin-jin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Vláda	vláda	k1gFnSc1	vláda
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
oficiálně	oficiálně	k6eAd1	oficiálně
zastává	zastávat	k5eAaImIp3nS	zastávat
státní	státní	k2eAgInSc1d1	státní
ateismus	ateismus	k1gInSc1	ateismus
a	a	k8xC	a
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
vede	vést	k5eAaImIp3nS	vést
antireligistické	antireligistický	k2eAgFnSc2d1	antireligistický
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náboženské	náboženský	k2eAgFnSc6d1	náboženská
záležitosti	záležitost	k1gFnSc6	záležitost
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgFnPc4d1	náboženská
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Náboženská	náboženský	k2eAgFnSc1d1	náboženská
svoboda	svoboda	k1gFnSc1	svoboda
je	být	k5eAaImIp3nS	být
zaručena	zaručit	k5eAaPmNgFnS	zaručit
čínskou	čínský	k2eAgFnSc7d1	čínská
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
náboženské	náboženský	k2eAgFnPc1d1	náboženská
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nemají	mít	k5eNaImIp3nP	mít
oficiální	oficiální	k2eAgNnSc4d1	oficiální
schválení	schválení	k1gNnSc4	schválení
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
předmětem	předmět	k1gInSc7	předmět
státní	státní	k2eAgFnSc2d1	státní
perzekuce	perzekuce	k1gFnSc2	perzekuce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
byla	být	k5eAaImAgFnS	být
čínská	čínský	k2eAgFnSc1d1	čínská
civilizace	civilizace	k1gFnSc1	civilizace
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
různými	různý	k2eAgFnPc7d1	různá
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
hnutími	hnutí	k1gNnPc7	hnutí
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Tři	tři	k4xCgMnPc4	tři
učení	učený	k2eAgMnPc1d1	učený
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
konfuciánství	konfuciánství	k1gNnSc2	konfuciánství
<g/>
,	,	kIx,	,
taoismu	taoismus	k1gInSc2	taoismus
a	a	k8xC	a
buddhismu	buddhismus	k1gInSc2	buddhismus
(	(	kIx(	(
<g/>
čínský	čínský	k2eAgInSc1d1	čínský
buddhismus	buddhismus	k1gInSc1	buddhismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
formování	formování	k1gNnSc6	formování
čínské	čínský	k2eAgFnSc2d1	čínská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
obohatily	obohatit	k5eAaPmAgInP	obohatit
teologický	teologický	k2eAgInSc4d1	teologický
a	a	k8xC	a
duchovní	duchovní	k2eAgInSc4d1	duchovní
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
období	období	k1gNnSc2	období
Šangu	Šang	k1gInSc2	Šang
a	a	k8xC	a
dynastie	dynastie	k1gFnSc2	dynastie
Čou	Čou	k1gFnSc2	Čou
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
je	být	k5eAaImIp3nS	být
zemí	zem	k1gFnSc7	zem
s	s	k7c7	s
kulturní	kulturní	k2eAgFnSc7d1	kulturní
tradicí	tradice	k1gFnSc7	tradice
starou	stará	k1gFnSc4	stará
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
svou	svůj	k3xOyFgFnSc4	svůj
kulturní	kulturní	k2eAgFnSc4d1	kulturní
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
tak	tak	k9	tak
tradici	tradice	k1gFnSc4	tradice
literární	literární	k2eAgFnSc4d1	literární
a	a	k8xC	a
hudební	hudební	k2eAgFnSc4d1	hudební
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
otevřená	otevřený	k2eAgFnSc1d1	otevřená
kulturním	kulturní	k2eAgNnPc3d1	kulturní
vlivům	vliv	k1gInPc3	vliv
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Za	za	k7c4	za
první	první	k4xOgFnSc4	první
velkou	velký	k2eAgFnSc4d1	velká
básnickou	básnický	k2eAgFnSc4d1	básnická
osobnost	osobnost	k1gFnSc4	osobnost
Číny	Čína	k1gFnSc2	Čína
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
Čchü	Čchü	k1gFnSc7	Čchü
Jüan	jüan	k1gInSc1	jüan
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
3.	[number]	k4	3.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
Tzv.	tzv.	kA	tzv.
popisné	popisný	k2eAgFnPc4d1	popisná
básně	báseň	k1gFnPc4	báseň
skládal	skládat	k5eAaImAgMnS	skládat
v	v	k7c4	v
1.	[number]	k4	1.
století	století	k1gNnPc2	století
Pan	Pan	k1gMnSc1	Pan
Ku	k	k7c3	k
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4.	[number]	k4	4.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
básník	básník	k1gMnSc1	básník
Tchao	Tchao	k1gMnSc1	Tchao
Jüan-ming	Jüaning	k1gInSc4	Jüan-ming
<g/>
.	.	kIx.	.
</s>
<s>
Cchao	Cchao	k1gMnSc1	Cchao
Cchao	Cchao	k1gMnSc1	Cchao
založil	založit	k5eAaPmAgMnS	založit
nový	nový	k2eAgInSc4d1	nový
styl	styl	k1gInSc4	styl
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
ťien-an	ťienn	k1gInSc1	ťien-an
<g/>
,	,	kIx,	,
významným	významný	k2eAgMnSc7d1	významný
básníkem	básník	k1gMnSc7	básník
byl	být	k5eAaImAgMnS	být
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Cchao	Cchao	k6eAd1	Cchao
Pchi	Pch	k1gFnSc2	Pch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8.	[number]	k4	8.
století	století	k1gNnSc2	století
zazářil	zazářit	k5eAaPmAgMnS	zazářit
Tu	tu	k6eAd1	tu
Fu	fu	k0	fu
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
Li	li	k8xS	li
Po	po	k7c6	po
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejpopulárnějším	populární	k2eAgMnSc7d3	nejpopulárnější
čínským	čínský	k2eAgMnSc7d1	čínský
autorem	autor	k1gMnSc7	autor
až	až	k8xS	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Li	li	k8xS	li
Poa	Poa	k1gMnPc4	Poa
a	a	k8xC	a
Tu	ten	k3xDgFnSc4	ten
Fua	Fua	k1gFnSc4	Fua
navazoval	navazovat	k5eAaImAgInS	navazovat
Po	po	k7c6	po
Ťü-i	Ťü-	k1gInSc6	Ťü-
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jim	on	k3xPp3gMnPc3	on
vyčítal	vyčítat	k5eAaImAgInS	vyčítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
utíkaly	utíkat	k5eAaImAgFnP	utíkat
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
k	k	k7c3	k
řekám	řeka	k1gFnPc3	řeka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
8.	[number]	k4	8.
a	a	k8xC	a
9.	[number]	k4	9.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
zlatý	zlatý	k2eAgInSc4d1	zlatý
věk	věk	k1gInSc4	věk
čínské	čínský	k2eAgFnSc2d1	čínská
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
pozdním	pozdní	k2eAgMnPc3d1	pozdní
představitelům	představitel	k1gMnPc3	představitel
patřil	patřit	k5eAaImAgInS	patřit
Tu	tu	k6eAd1	tu
Mu	on	k3xPp3gNnSc3	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
11.	[number]	k4	11.
století	století	k1gNnPc2	století
proslula	proslout	k5eAaPmAgFnS	proslout
básnířka	básnířka	k1gFnSc1	básnířka
Li	li	k8xS	li
Čching-čao	Čching-čao	k1gNnSc1	Čching-čao
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíhož	jejíž	k3xOyRp3gNnSc2	jejíž
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
však	však	k9	však
zachovala	zachovat	k5eAaPmAgFnS	zachovat
jen	jen	k9	jen
jediná	jediný	k2eAgFnSc1d1	jediná
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
průzračného	průzračný	k2eAgInSc2d1	průzračný
nefritu	nefrit	k1gInSc2	nefrit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
8.	[number]	k4	8.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
érou	éra	k1gFnSc7	éra
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
čínská	čínský	k2eAgFnSc1d1	čínská
próza	próza	k1gFnSc1	próza
<g/>
,	,	kIx,	,
za	za	k7c4	za
její	její	k3xOp3gFnPc4	její
zakladatelské	zakladatelský	k2eAgFnPc4d1	zakladatelská
postavy	postava	k1gFnPc4	postava
jsou	být	k5eAaImIp3nP	být
označováni	označován	k2eAgMnPc1d1	označován
Chan	Chan	k1gMnSc1	Chan
Jü	Jü	k1gFnSc2	Jü
a	a	k8xC	a
Liou	Lious	k1gInSc2	Lious
Cung-jüan	Cungüany	k1gInPc2	Cung-jüany
<g/>
.	.	kIx.	.
</s>
<s>
Významným	významný	k2eAgMnSc7d1	významný
byl	být	k5eAaImAgMnS	být
však	však	k9	však
již	již	k6eAd1	již
cestopis	cestopis	k1gInSc1	cestopis
Zápisky	zápiska	k1gFnSc2	zápiska
o	o	k7c6	o
buddhistických	buddhistický	k2eAgFnPc6d1	buddhistická
zemích	zem	k1gFnPc6	zem
autora	autor	k1gMnSc2	autor
4.	[number]	k4	4.
století	století	k1gNnSc2	století
Fa-siena	Faieno	k1gNnSc2	Fa-sieno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
11.	[number]	k4	11.
století	století	k1gNnSc6	století
prosluly	proslout	k5eAaPmAgFnP	proslout
svými	svůj	k3xOyFgMnPc7	svůj
eseji	esej	k1gInSc6	esej
Su	Su	k?	Su
Š	Š	kA	Š
<g/>
'	'	kIx"	'
či	či	k8xC	či
Ou-jang	Ouang	k1gInSc4	Ou-jang
Siou	Sious	k1gInSc2	Sious
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
od	od	k7c2	od
jezerního	jezerní	k2eAgInSc2d1	jezerní
břehu	břeh	k1gInSc2	břeh
z	z	k7c2	z
konce	konec	k1gInSc2	konec
14.	[number]	k4	14.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
napsali	napsat	k5eAaPmAgMnP	napsat
Š	Š	kA	Š
<g/>
'	'	kIx"	'
Naj-an	Najn	k1gMnSc1	Naj-an
a	a	k8xC	a
Luo	Luo	k1gMnSc1	Luo
Kuan-čung	Kuan-čung	k1gMnSc1	Kuan-čung
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
prvním	první	k4xOgMnSc6	první
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
čtyř	čtyři	k4xCgInPc2	čtyři
klasických	klasický	k2eAgInPc2d1	klasický
čínských	čínský	k2eAgInPc2d1	čínský
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
jsou	být	k5eAaImIp3nP	být
Příběhy	příběh	k1gInPc1	příběh
Tří	tři	k4xCgFnPc2	tři
říší	říš	k1gFnPc2	říš
Luo	Luo	k1gMnSc2	Luo
Kuan-čunga	Kuan-čung	k1gMnSc2	Kuan-čung
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
ze	z	k7c2	z
14.	[number]	k4	14.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Süan-cang	Süanang	k1gMnSc1	Süan-cang
proslul	proslout	k5eAaPmAgMnS	proslout
svým	svůj	k3xOyFgInSc7	svůj
cestopisem	cestopis	k1gInSc7	cestopis
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
později	pozdě	k6eAd2	pozdě
posloužila	posloužit	k5eAaPmAgFnS	posloužit
jako	jako	k9	jako
inspirace	inspirace	k1gFnSc1	inspirace
pro	pro	k7c4	pro
román	román	k1gInSc4	román
Putování	putování	k1gNnSc2	putování
na	na	k7c4	na
západ	západ	k1gInSc4	západ
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgInSc1	třetí
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
klasických	klasický	k2eAgInPc2d1	klasický
románů	román	k1gInPc2	román
<g/>
)	)	kIx)	)
spisovatele	spisovatel	k1gMnSc2	spisovatel
Wu	Wu	k1gMnSc2	Wu
Čcheng-ena	Čchengn	k1gMnSc2	Čcheng-en
<g/>
,	,	kIx,	,
nejvýznamnějšího	významný	k2eAgMnSc4d3	nejvýznamnější
autora	autor	k1gMnSc4	autor
mingského	mingského	k2eAgNnSc1d1	mingského
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
průkopníkům	průkopník	k1gMnPc3	průkopník
moderního	moderní	k2eAgNnSc2d1	moderní
divadla	divadlo	k1gNnSc2	divadlo
17.	[number]	k4	17.
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgMnS	patřit
Li	li	k9	li
Jü	Jü	k1gMnSc1	Jü
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18.	[number]	k4	18.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
Cchao	Cchao	k1gMnSc1	Cchao
Süe-čchin	Süe-čchin	k1gMnSc1	Süe-čchin
románem	román	k1gInSc7	román
Sen	sen	k1gInSc1	sen
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
domě	dům	k1gInSc6	dům
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgMnSc7d1	poslední
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
klasických	klasický	k2eAgInPc2d1	klasický
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
prvním	první	k4xOgFnPc3	první
na	na	k7c4	na
západ	západ	k1gInSc1	západ
proniknuvším	proniknuvší	k2eAgMnPc3d1	proniknuvší
autorům	autor	k1gMnPc3	autor
patřil	patřit	k5eAaImAgInS	patřit
Lin	Lina	k1gFnPc2	Lina
Jü-tchang	Jüchanga	k1gFnPc2	Jü-tchanga
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
čínských	čínský	k2eAgMnPc2d1	čínský
spisovatelů	spisovatel	k1gMnPc2	spisovatel
20.	[number]	k4	20.
století	století	k1gNnPc2	století
jsou	být	k5eAaImIp3nP	být
označováni	označován	k2eAgMnPc1d1	označován
Lu	Lu	k1gMnPc1	Lu
Sün	Sün	k1gMnSc1	Sün
a	a	k8xC	a
Lao	Lao	k1gMnSc1	Lao
Še	Še	k1gMnSc1	Še
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
známého	známý	k2eAgInSc2d1	známý
románu	román	k1gInSc2	román
Rikša	rikša	k1gMnSc1	rikša
<g/>
.	.	kIx.	.
</s>
<s>
Románem	román	k1gInSc7	román
Rodina	rodina	k1gFnSc1	rodina
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Pa	Pa	kA	Pa
Ťin	Ťin	k1gFnSc2	Ťin
<g/>
.	.	kIx.	.
</s>
<s>
Feminismus	feminismus	k1gInSc1	feminismus
do	do	k7c2	do
čínské	čínský	k2eAgFnSc2d1	čínská
literatury	literatura	k1gFnSc2	literatura
vnesla	vnést	k5eAaPmAgFnS	vnést
Čchiou	Čchiý	k2eAgFnSc4d1	Čchiý
Ťin	Ťin	k1gFnSc4	Ťin
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
komunistickým	komunistický	k2eAgMnPc3d1	komunistický
autorům	autor	k1gMnPc3	autor
patřil	patřit	k5eAaImAgInS	patřit
Kuo	Kuo	k1gMnSc1	Kuo
Mo-žo	Mo-žo	k1gMnSc1	Mo-žo
či	či	k8xC	či
Mao	Mao	k1gMnSc1	Mao
Tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Kao	Kao	k?	Kao
Sing-ťien	Sing-ťien	k1gInSc1	Sing-ťien
obdržel	obdržet	k5eAaPmAgInS	obdržet
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
Mo	Mo	k1gFnSc4	Mo
Jen	jen	k8xS	jen
stejné	stejný	k2eAgNnSc4d1	stejné
ocenění	ocenění	k1gNnSc4	ocenění
převzal	převzít	k5eAaPmAgMnS	převzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012.	[number]	k4	2012.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Proslulým	proslulý	k2eAgInSc7d1	proslulý
starověkým	starověký	k2eAgInSc7d1	starověký
artefaktem	artefakt	k1gInSc7	artefakt
je	být	k5eAaImIp3nS	být
Terakotová	terakotový	k2eAgFnSc1d1	Terakotová
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
soubor	soubor	k1gInSc1	soubor
6000	[number]	k4	6000
soch	socha	k1gFnPc2	socha
zakopaných	zakopaný	k2eAgFnPc2d1	zakopaná
v	v	k7c6	v
pohřebním	pohřební	k2eAgInSc6d1	pohřební
komplexu	komplex	k1gInSc6	komplex
poblíž	poblíž	k7c2	poblíž
čínského	čínský	k2eAgNnSc2d1	čínské
města	město	k1gNnSc2	město
Si-an	Sin	k1gInSc1	Si-an
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
29.	[number]	k4	29.
března	březen	k1gInSc2	březen
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c4	v
3.	[number]	k4	3.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
hrobku	hrobka	k1gFnSc4	hrobka
sjednotitele	sjednotitel	k1gMnSc2	sjednotitel
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
císaře	císař	k1gMnSc4	císař
Čchin	Čchina	k1gFnPc2	Čchina
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
chuang-tiho	chuangize	k6eAd1	chuang-tize
<g/>
.	.	kIx.	.
<g/>
Klasikem	klasik	k1gMnSc7	klasik
čínské	čínský	k2eAgFnSc2d1	čínská
kaligrafie	kaligrafie	k1gFnSc2	kaligrafie
a	a	k8xC	a
malířství	malířství	k1gNnSc2	malířství
byl	být	k5eAaImAgInS	být
Mi	já	k3xPp1nSc3	já
Fu	fu	k0	fu
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
svitkové	svitkový	k2eAgInPc1d1	svitkový
obrazy	obraz	k1gInPc1	obraz
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgInP	dochovat
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
autora	autor	k1gMnSc2	autor
4.	[number]	k4	4.
století	století	k1gNnPc1	století
Ku	k	k7c3	k
Kchaj-č	Kchaj-č	k1gInSc1	Kchaj-č
<g/>
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
čínského	čínský	k2eAgNnSc2d1	čínské
krajinářství	krajinářství	k1gNnSc2	krajinářství
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
8.	[number]	k4	8.
století	století	k1gNnPc2	století
Wang	Wanga	k1gFnPc2	Wanga
Wej	Wej	k1gMnPc3	Wej
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
12.	[number]	k4	12.
století	století	k1gNnSc6	století
se	s	k7c7	s
známým	známý	k2eAgMnSc7d1	známý
malířem	malíř	k1gMnSc7	malíř
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
Chuej-cung	Chuejung	k1gMnSc1	Chuej-cung
(	(	kIx(	(
<g/>
Sung	Sung	k1gMnSc1	Sung
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17.	[number]	k4	17.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
od	od	k7c2	od
tradičních	tradiční	k2eAgInPc2d1	tradiční
vzorů	vzor	k1gInPc2	vzor
počal	počnout	k5eAaPmAgMnS	počnout
vzdalovat	vzdalovat	k5eAaImF	vzdalovat
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
tchao	tchao	k1gMnSc1	tchao
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
moderním	moderní	k2eAgMnPc3d1	moderní
malířům	malíř	k1gMnPc3	malíř
patří	patřit	k5eAaImIp3nS	patřit
Čchi	Čche	k1gFnSc4	Čche
Paj-š	Paj-š	k1gInSc1	Paj-š
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejuznávanějším	uznávaný	k2eAgMnPc3d3	nejuznávanější
současným	současný	k2eAgMnPc3d1	současný
výtvarným	výtvarný	k2eAgMnPc3d1	výtvarný
umělcům	umělec	k1gMnPc3	umělec
Aj	aj	kA	aj
Wej-wej	Wejej	k1gInSc1	Wej-wej
<g/>
.	.	kIx.	.
<g/>
Architekt	architekt	k1gMnSc1	architekt
Wang	Wang	k1gMnSc1	Wang
Šu	Šu	k1gMnSc1	Šu
je	být	k5eAaImIp3nS	být
nositel	nositel	k1gMnSc1	nositel
prestižní	prestižní	k2eAgFnSc2d1	prestižní
architektonické	architektonický	k2eAgFnSc2d1	architektonická
Pritzkerovy	Pritzkerův	k2eAgFnSc2d1	Pritzkerova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
čínského	čínský	k2eAgInSc2d1	čínský
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
i	i	k9	i
jiný	jiný	k2eAgMnSc1d1	jiný
držitel	držitel	k1gMnSc1	držitel
této	tento	k3xDgFnSc2	tento
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
Američan	Američan	k1gMnSc1	Američan
I.	I.	kA	I.
M.	M.	kA	M.
Pei	Pei	k1gMnSc1	Pei
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
známé	známý	k2eAgFnSc2d1	známá
Skleněné	skleněný	k2eAgFnSc2d1	skleněná
pyramidy	pyramida	k1gFnSc2	pyramida
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
Louvre	Louvre	k1gInSc1	Louvre
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
čínským	čínský	k2eAgMnPc3d1	čínský
filmařům	filmař	k1gMnPc3	filmař
patří	patřit	k5eAaImIp3nP	patřit
režisér	režisér	k1gMnSc1	režisér
Čang	Čang	k1gMnSc1	Čang
I-mou	Ia	k1gMnSc7	I-ma
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
mj.	mj.	kA	mj.
hlavním	hlavní	k2eAgMnSc7d1	hlavní
režisérem	režisér	k1gMnSc7	režisér
úvodního	úvodní	k2eAgInSc2d1	úvodní
a	a	k8xC	a
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
ceremoniálu	ceremoniál	k1gInSc2	ceremoniál
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
generačním	generační	k2eAgMnPc3d1	generační
souputníkům	souputník	k1gMnPc3	souputník
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pátá	pátý	k4xOgFnSc1	pátý
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
Čchen	Čchen	k2eAgInSc4d1	Čchen
Kchaj-ke	Kchaje	k1gInSc4	Kchaj-ke
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
prosadili	prosadit	k5eAaPmAgMnP	prosadit
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
John	John	k1gMnSc1	John
Woo	Woo	k1gMnSc1	Woo
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
herečky	herečka	k1gFnPc1	herečka
Joan	Joana	k1gFnPc2	Joana
Chen	Chena	k1gFnPc2	Chena
<g/>
,	,	kIx,	,
Li	li	k8xS	li
Ping-ping	Pinging	k1gInSc1	Ping-ping
nebo	nebo	k8xC	nebo
Bai	Bai	k1gMnSc1	Bai
Ling	Ling	k1gMnSc1	Ling
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejpopulárnějším	populární	k2eAgFnPc3d3	nejpopulárnější
herečkám	herečka	k1gFnPc3	herečka
Čao	čao	k0	čao
Wej	Wej	k1gMnSc5	Wej
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Krvavé	krvavý	k2eAgNnSc4d1	krvavé
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Čang	Čang	k1gInSc1	Čang
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
i	i	k9	i
známá	známý	k2eAgFnSc1d1	známá
z	z	k7c2	z
filmu	film	k1gInSc2	film
Gejša	gejša	k1gFnSc1	gejša
či	či	k8xC	či
Fan	Fana	k1gFnPc2	Fana
Ping-ping	Pinging	k1gInSc1	Ping-ping
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
je	být	k5eAaImIp3nS	být
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
můžeme	moct	k5eAaImIp1nP	moct
rovněž	rovněž	k9	rovněž
přičítat	přičítat	k5eAaImF	přičítat
k	k	k7c3	k
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
<s>
Proslulé	proslulý	k2eAgInPc1d1	proslulý
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
hongkongské	hongkongský	k2eAgInPc1d1	hongkongský
akční	akční	k2eAgInPc1d1	akční
filmy	film	k1gInPc1	film
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc7d3	veliký
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
tento	tento	k3xDgMnSc1	tento
průmysl	průmysl	k1gInSc4	průmysl
zplodil	zplodit	k5eAaPmAgInS	zplodit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
Jackie	Jackie	k1gFnSc1	Jackie
Chan	Chana	k1gFnPc2	Chana
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnPc6	jehož
stopách	stopa	k1gFnPc6	stopa
šel	jít	k5eAaImAgInS	jít
Jet	jet	k5eAaImNgInS	jet
Li	li	k9	li
(	(	kIx(	(
<g/>
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Pekingu	Peking	k1gInSc2	Peking
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
občan	občan	k1gMnSc1	občan
Singapuru	Singapur	k1gInSc2	Singapur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čang	Čang	k1gMnSc1	Čang
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
lin	lin	k?	lin
jako	jako	k8xC	jako
pvní	pvní	k2eAgFnSc1d1	pvní
Číňanka	Číňanka	k1gFnSc1	Číňanka
uspěla	uspět	k5eAaPmAgFnS	uspět
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
soutěžích	soutěž	k1gFnPc6	soutěž
krásy	krása	k1gFnSc2	krása
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
stala	stát	k5eAaPmAgFnS	stát
Miss	miss	k1gFnSc1	miss
World	Worlda	k1gFnPc2	Worlda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
velkých	velký	k2eAgNnPc6d1	velké
čínských	čínský	k2eAgNnPc6d1	čínské
městech	město	k1gNnPc6	město
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
velmi	velmi	k6eAd1	velmi
moderní	moderní	k2eAgNnPc1d1	moderní
operní	operní	k2eAgNnPc1d1	operní
divadla	divadlo	k1gNnPc1	divadlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
jsou	být	k5eAaImIp3nP	být
uváděny	uváděn	k2eAgFnPc4d1	uváděna
jak	jak	k8xS	jak
tradiční	tradiční	k2eAgFnPc4d1	tradiční
čínské	čínský	k2eAgFnPc4d1	čínská
opery	opera	k1gFnPc4	opera
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
opery	opera	k1gFnSc2	opera
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
světového	světový	k2eAgInSc2d1	světový
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
evropské	evropský	k2eAgFnSc2d1	Evropská
opery	opera	k1gFnSc2	opera
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
čínská	čínský	k2eAgFnSc1d1	čínská
opera	opera	k1gFnSc1	opera
v	v	k7c6	v
lidových	lidový	k2eAgFnPc6d1	lidová
vrstvách	vrstva	k1gFnPc6	vrstva
a	a	k8xC	a
uměním	umění	k1gNnSc7	umění
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
společnosti	společnost	k1gFnSc2	společnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
teprve	teprve	k6eAd1	teprve
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vrcholnou	vrcholný	k2eAgFnSc4d1	vrcholná
éru	éra	k1gFnSc4	éra
bývá	bývat	k5eAaImIp3nS	bývat
označováno	označovat	k5eAaImNgNnS	označovat
13.	[number]	k4	13.
století	století	k1gNnPc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
okolo	okolo	k7c2	okolo
stovky	stovka	k1gFnSc2	stovka
žánrů	žánr	k1gInPc2	žánr
čínské	čínský	k2eAgFnSc2d1	čínská
opery	opera	k1gFnSc2	opera
<g/>
,	,	kIx,	,
odlišených	odlišený	k2eAgFnPc2d1	odlišená
především	především	k9	především
regionálně	regionálně	k6eAd1	regionálně
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Pekingská	pekingský	k2eAgFnSc1d1	Pekingská
opera	opera	k1gFnSc1	opera
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
představení	představení	k1gNnSc2	představení
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
prakticky	prakticky	k6eAd1	prakticky
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
před	před	k7c7	před
17.	[number]	k4	17.
stoletím	století	k1gNnSc7	století
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
tance	tanec	k1gInSc2	tanec
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
často	často	k6eAd1	často
i	i	k9	i
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
<g/>
,	,	kIx,	,
akrobacii	akrobacie	k1gFnSc6	akrobacie
<g/>
,	,	kIx,	,
typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
výrazné	výrazný	k2eAgFnPc1d1	výrazná
masky	maska	k1gFnPc1	maska
a	a	k8xC	a
líčení	líčení	k1gNnSc1	líčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
revoluce	revoluce	k1gFnSc1	revoluce
byla	být	k5eAaImAgFnS	být
čínská	čínský	k2eAgFnSc1d1	čínská
opera	opera	k1gFnSc1	opera
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
od	od	k7c2	od
konce	konec	k1gInSc2	konec
70.	[number]	k4	70.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Číňané	Číňan	k1gMnPc1	Číňan
snaží	snažit	k5eAaImIp3nP	snažit
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
oživit	oživit	k5eAaPmF	oživit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
působí	působit	k5eAaImIp3nS	působit
několik	několik	k4yIc1	několik
symfonických	symfonický	k2eAgInPc2d1	symfonický
orchestrů	orchestr	k1gInPc2	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
je	být	k5eAaImIp3nS	být
Šanghajský	šanghajský	k2eAgInSc1d1	šanghajský
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
(	(	kIx(	(
<g/>
Shanghai	Shangha	k1gFnPc1	Shangha
Symphony	Symphona	k1gFnSc2	Symphona
Orchestra	orchestra	k1gFnSc1	orchestra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
výhradně	výhradně	k6eAd1	výhradně
z	z	k7c2	z
čínských	čínský	k2eAgMnPc2d1	čínský
hudebníků	hudebník	k1gMnPc2	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
vysílaly	vysílat	k5eAaImAgFnP	vysílat
evropské	evropský	k2eAgFnPc1d1	Evropská
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Arte	Arte	k1gFnSc1	Arte
<g/>
)	)	kIx)	)
vystoupení	vystoupení	k1gNnSc1	vystoupení
tohoto	tento	k3xDgInSc2	tento
orchestru	orchestr	k1gInSc2	orchestr
–	–	k?	–
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
jeho	on	k3xPp3gMnSc2	on
šéfdirigenta	šéfdirigent	k1gMnSc2	šéfdirigent
Ju	ju	k0	ju
Longa	Longa	k1gFnSc1	Longa
–	–	k?	–
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
města	město	k1gNnSc2	město
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
programu	program	k1gInSc6	program
byl	být	k5eAaImAgInS	být
mj.	mj.	kA	mj.
2.	[number]	k4	2.
klavírní	klavírní	k2eAgInSc1d1	klavírní
koncert	koncert	k1gInSc1	koncert
Sergeje	Sergej	k1gMnSc2	Sergej
Rachmaninova	Rachmaninův	k2eAgMnSc2d1	Rachmaninův
<g/>
.	.	kIx.	.
</s>
<s>
Sólistou	sólista	k1gMnSc7	sólista
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
pianista	pianista	k1gMnSc1	pianista
Daniil	Daniil	k1gMnSc1	Daniil
Trifonov	Trifonovo	k1gNnPc2	Trifonovo
<g/>
.	.	kIx.	.
</s>
<s>
Čínským	čínský	k2eAgMnSc7d1	čínský
občanem	občan	k1gMnSc7	občan
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgMnSc1d1	jiný
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
klavírista	klavírista	k1gMnSc1	klavírista
<g/>
,	,	kIx,	,
Lang	Lang	k1gMnSc1	Lang
Lang	Lang	k1gMnSc1	Lang
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
hudebním	hudební	k2eAgMnPc3d1	hudební
skladatelům	skladatel	k1gMnPc3	skladatel
současnosti	současnost	k1gFnSc2	současnost
patří	patřit	k5eAaImIp3nS	patřit
Tchan	Tchan	k1gInSc4	Tchan
Tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
soudobou	soudobý	k2eAgFnSc4d1	soudobá
vážnou	vážný	k2eAgFnSc4d1	vážná
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
více	hodně	k6eAd2	hodně
proslul	proslout	k5eAaPmAgMnS	proslout
hudbou	hudba	k1gFnSc7	hudba
filmovou	filmový	k2eAgFnSc7d1	filmová
(	(	kIx(	(
<g/>
Tygr	tygr	k1gMnSc1	tygr
a	a	k8xC	a
drak	drak	k1gMnSc1	drak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Yo-Yo	Yo-Yo	k1gMnSc1	Yo-Yo
Ma	Ma	k1gMnSc1	Ma
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgMnSc7d1	významný
violoncelistou	violoncelista	k1gMnSc7	violoncelista
<g/>
.	.	kIx.	.
</s>
<s>
Legendární	legendární	k2eAgFnSc7d1	legendární
čínskou	čínský	k2eAgFnSc7d1	čínská
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
byla	být	k5eAaImAgFnS	být
Čou	Čou	k1gFnSc1	Čou
Süan	Süana	k1gFnPc2	Süana
<g/>
,	,	kIx,	,
k	k	k7c3	k
současným	současný	k2eAgFnPc3d1	současná
popovým	popový	k2eAgFnPc3d1	popová
hvězdám	hvězda	k1gFnPc3	hvězda
(	(	kIx(	(
<g/>
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
Cantopopu	Cantopop	k1gInSc6	Cantopop
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
Leslie	Leslie	k1gFnSc1	Leslie
Cheung	Cheunga	k1gFnPc2	Cheunga
nebo	nebo	k8xC	nebo
Faye	Faye	k1gFnPc2	Faye
Wong	Wonga	k1gFnPc2	Wonga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Světově	světově	k6eAd1	světově
významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
čínská	čínský	k2eAgFnSc1d1	čínská
kuchyně	kuchyně	k1gFnSc1	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ceněna	cenit	k5eAaImNgFnS	cenit
téměř	téměř	k6eAd1	téměř
všude	všude	k6eAd1	všude
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zastoupena	zastoupit	k5eAaPmNgFnS	zastoupit
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
velkoměstech	velkoměsto	k1gNnPc6	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
vedené	vedený	k2eAgFnSc2d1	vedená
čínskými	čínský	k2eAgMnPc7d1	čínský
majiteli	majitel	k1gMnPc7	majitel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
pracují	pracovat	k5eAaImIp3nP	pracovat
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
čínští	čínský	k2eAgMnPc1d1	čínský
kuchaři	kuchař	k1gMnPc1	kuchař
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
turistických	turistický	k2eAgFnPc6d1	turistická
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
však	však	k9	však
i	i	k9	i
mimo	mimo	k7c4	mimo
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Podávají	podávat	k5eAaImIp3nP	podávat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
pokrmy	pokrm	k1gInPc4	pokrm
připravované	připravovaný	k2eAgNnSc1d1	připravované
podle	podle	k7c2	podle
tradičních	tradiční	k2eAgFnPc2d1	tradiční
metod	metoda	k1gFnPc2	metoda
pěti	pět	k4xCc2	pět
známých	známý	k2eAgFnPc2d1	známá
regionálních	regionální	k2eAgFnPc2d1	regionální
kuchyní	kuchyně	k1gFnPc2	kuchyně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typickými	typický	k2eAgFnPc7d1	typická
surovinami	surovina	k1gFnPc7	surovina
čínské	čínský	k2eAgFnSc2d1	čínská
kuchyně	kuchyně	k1gFnSc2	kuchyně
jsou	být	k5eAaImIp3nP	být
bambusové	bambusový	k2eAgInPc1d1	bambusový
výhonky	výhonek	k1gInPc1	výhonek
<g/>
,	,	kIx,	,
houba	houba	k1gFnSc1	houba
Ucho	ucho	k1gNnSc1	ucho
Jidášovo	Jidášův	k2eAgNnSc1d1	Jidášovo
<g/>
,	,	kIx,	,
houba	houba	k1gFnSc1	houba
šitake	šitake	k1gFnSc1	šitake
<g/>
,	,	kIx,	,
pasta	pasta	k1gFnSc1	pasta
z	z	k7c2	z
černých	černý	k2eAgInPc2d1	černý
bobů	bob	k1gInPc2	bob
<g/>
,	,	kIx,	,
pepř	pepř	k1gInSc1	pepř
sečuánský	sečuánský	k2eAgInSc1d1	sečuánský
<g/>
,	,	kIx,	,
sójová	sójový	k2eAgFnSc1d1	sójová
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
,	,	kIx,	,
tofu	tofu	k1gNnSc1	tofu
<g/>
,	,	kIx,	,
šalotka	šalotka	k1gFnSc1	šalotka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pálenek	pálenka	k1gFnPc2	pálenka
rýžové	rýžový	k2eAgNnSc1d1	rýžové
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
z	z	k7c2	z
koření	koření	k1gNnSc2	koření
zázvor	zázvor	k1gInSc1	zázvor
<g/>
.	.	kIx.	.
</s>
<s>
Známými	známý	k2eAgInPc7d1	známý
pokrmy	pokrm	k1gInPc7	pokrm
jsou	být	k5eAaImIp3nP	být
Kuřecí	kuřecí	k2eAgFnSc1d1	kuřecí
kung-pao	kungao	k6eAd1	kung-pao
<g/>
,	,	kIx,	,
Pekingská	pekingský	k2eAgFnSc1d1	Pekingská
kachna	kachna	k1gFnSc1	kachna
<g/>
,	,	kIx,	,
jarní	jarní	k2eAgFnSc2d1	jarní
závitky	závitka	k1gFnSc2	závitka
či	či	k8xC	či
Šuej-ču-žou	Šuej-ču-žou	k1gFnSc2	Šuej-ču-žou
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
velké	velký	k2eAgInPc1d1	velký
regionální	regionální	k2eAgInPc1d1	regionální
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
regionálním	regionální	k2eAgFnPc3d1	regionální
kuchyním	kuchyně	k1gFnPc3	kuchyně
patří	patřit	k5eAaImIp3nS	patřit
kantonská	kantonský	k2eAgFnSc1d1	kantonská
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
užívající	užívající	k2eAgFnSc1d1	užívající
pánev	pánev	k1gFnSc1	pánev
wok	wok	k?	wok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pekingská	pekingský	k2eAgFnSc1d1	Pekingská
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
úprava	úprava	k1gFnSc1	úprava
v	v	k7c6	v
páře	pára	k1gFnSc6	pára
či	či	k8xC	či
grilování	grilování	k1gNnSc6	grilování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sečuánská	sečuánský	k2eAgFnSc1d1	sečuánská
(	(	kIx(	(
<g/>
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejpikantnějším	pikantní	k2eAgFnPc3d3	nejpikantnější
<g/>
)	)	kIx)	)
či	či	k8xC	či
čchaošanská	čchaošanský	k2eAgFnSc1d1	čchaošanský
(	(	kIx(	(
<g/>
hodně	hodně	k6eAd1	hodně
vegetariánská	vegetariánský	k2eAgNnPc1d1	vegetariánské
<g/>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
tofu	tofu	k1gNnSc6	tofu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
estetickou	estetický	k2eAgFnSc4d1	estetická
úpravu	úprava	k1gFnSc4	úprava
pokrmů	pokrm	k1gInPc2	pokrm
a	a	k8xC	a
stolování	stolování	k1gNnSc2	stolování
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jídelní	jídelní	k2eAgFnSc2d1	jídelní
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
,	,	kIx,	,
polévky	polévka	k1gFnSc2	polévka
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
lžící	lžíce	k1gFnSc7	lžíce
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
porcelánovou	porcelánový	k2eAgFnSc4d1	porcelánová
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
pokrmy	pokrm	k1gInPc1	pokrm
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pekingská	pekingský	k2eAgFnSc1d1	Pekingská
kachna	kachna	k1gFnSc1	kachna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
jedí	jíst	k5eAaImIp3nP	jíst
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
pokrmy	pokrm	k1gInPc1	pokrm
se	se	k3xPyFc4	se
servírují	servírovat	k5eAaBmIp3nP	servírovat
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
ze	z	k7c2	z
stolujících	stolující	k2eAgMnPc2d1	stolující
má	mít	k5eAaImIp3nS	mít
před	před	k7c7	před
sebou	se	k3xPyFc7	se
misku	miska	k1gFnSc4	miska
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
si	se	k3xPyFc3	se
nabírá	nabírat	k5eAaImIp3nS	nabírat
z	z	k7c2	z
pokrmů	pokrm	k1gInPc2	pokrm
na	na	k7c6	na
stole	stol	k1gInSc6	stol
<g/>
.	.	kIx.	.
</s>
<s>
Přednostně	přednostně	k6eAd1	přednostně
jedí	jíst	k5eAaImIp3nP	jíst
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
muži	muž	k1gMnPc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
těžký	těžký	k2eAgInSc4d1	těžký
prohřešek	prohřešek	k1gInSc4	prohřešek
proti	proti	k7c3	proti
etiketě	etiketa	k1gFnSc3	etiketa
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
někdo	někdo	k3yInSc1	někdo
zapíchne	zapíchnout	k5eAaPmIp3nS	zapíchnout
hůlky	hůlka	k1gFnPc4	hůlka
do	do	k7c2	do
zbytků	zbytek	k1gInPc2	zbytek
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
misce	miska	k1gFnSc6	miska
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jídla	jídlo	k1gNnSc2	jídlo
pijí	pít	k5eAaImIp3nP	pít
Číňané	Číňan	k1gMnPc1	Číňan
nejraději	rád	k6eAd3	rád
zelený	zelený	k2eAgInSc4d1	zelený
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Lékař	lékař	k1gMnSc1	lékař
Chua	Chua	k1gMnSc1	Chua
Tchuo	Tchuo	k1gMnSc1	Tchuo
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
již	již	k6eAd1	již
v	v	k7c6	v
1.	[number]	k4	1.
století	století	k1gNnSc6	století
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
užil	užít	k5eAaPmAgMnS	užít
v	v	k7c4	v
lékařství	lékařství	k1gNnSc4	lékařství
anestézii	anestézie	k1gFnSc4	anestézie
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
matematického	matematický	k2eAgNnSc2d1	matematické
myšlení	myšlení	k1gNnSc2	myšlení
stavěl	stavět	k5eAaImAgMnS	stavět
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
Liou	Lious	k1gInSc2	Lious
Chuej	Chuej	k1gInSc4	Chuej
ve	v	k7c4	v
3.	[number]	k4	3.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Proslulým	proslulý	k2eAgMnSc7d1	proslulý
všestranným	všestranný	k2eAgMnSc7d1	všestranný
učencem	učenec	k1gMnSc7	učenec
starověku	starověk	k1gInSc2	starověk
byl	být	k5eAaImAgMnS	být
Šen	Šen	k1gMnSc1	Šen
Kua	Kua	k1gMnSc1	Kua
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
práci	práce	k1gFnSc4	práce
Meng-si	Menge	k1gFnSc4	Meng-se
pi-tchan	pichana	k1gFnPc2	pi-tchana
(	(	kIx(	(
<g/>
Eseje	esej	k1gFnSc2	esej
víru	vír	k1gInSc2	vír
snů	sen	k1gInPc2	sen
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1088	[number]	k4	1088
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
princip	princip	k1gInSc1	princip
magnetické	magnetický	k2eAgFnSc2d1	magnetická
jehly	jehla	k1gFnSc2	jehla
kompasu	kompas	k1gInSc2	kompas
a	a	k8xC	a
naznačil	naznačit	k5eAaPmAgMnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
využita	využít	k5eAaPmNgFnS	využít
pro	pro	k7c4	pro
navigaci	navigace	k1gFnSc4	navigace
(	(	kIx(	(
<g/>
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1187	[number]	k4	1187
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
promýšlení	promýšlení	k1gNnSc6	promýšlení
kompasu	kompas	k1gInSc2	kompas
definoval	definovat	k5eAaBmAgInS	definovat
též	též	k9	též
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
magnetickým	magnetický	k2eAgInSc7d1	magnetický
a	a	k8xC	a
skutečným	skutečný	k2eAgInSc7d1	skutečný
severním	severní	k2eAgInSc7d1	severní
pólem	pól	k1gInSc7	pól
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
400	[number]	k4	400
let	léto	k1gNnPc2	léto
před	před	k7c7	před
evropskou	evropský	k2eAgFnSc7d1	Evropská
vědou	věda	k1gFnSc7	věda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
starověkými	starověký	k2eAgMnPc7d1	starověký
astronomy	astronom	k1gMnPc7	astronom
byli	být	k5eAaImAgMnP	být
Cu	Cu	k1gMnSc1	Cu
Čchung-č	Čchung-č	k1gMnSc1	Čchung-č
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
Čang	Čang	k1gMnSc1	Čang
Cheng	Cheng	k1gMnSc1	Cheng
<g/>
.	.	kIx.	.
</s>
<s>
Su	Su	k?	Su
Sung	Sung	k1gInSc1	Sung
v	v	k7c4	v
11.	[number]	k4	11.
století	století	k1gNnSc2	století
sestavil	sestavit	k5eAaPmAgMnS	sestavit
mapu	mapa	k1gFnSc4	mapa
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejstarší	starý	k2eAgFnSc1d3	nejstarší
takovou	takový	k3xDgFnSc7	takový
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
tištěnou	tištěný	k2eAgFnSc7d1	tištěná
mapou	mapa	k1gFnSc7	mapa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16.	[number]	k4	16.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgMnS	být
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
styčným	styčný	k2eAgInSc7d1	styčný
bodem	bod	k1gInSc7	bod
tradiční	tradiční	k2eAgFnSc2d1	tradiční
čínské	čínský	k2eAgFnSc2d1	čínská
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
a	a	k8xC	a
vznikající	vznikající	k2eAgFnSc2d1	vznikající
evropské	evropský	k2eAgFnSc2d1	Evropská
vědy	věda	k1gFnSc2	věda
Sü	Sü	k1gFnSc2	Sü
Kuang-čchi	Kuang-čch	k1gFnSc2	Kuang-čch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
poznání	poznání	k1gNnSc2	poznání
čínské	čínský	k2eAgFnSc2d1	čínská
přírodovědy	přírodověda	k1gFnSc2	přírodověda
a	a	k8xC	a
medicíny	medicína	k1gFnSc2	medicína
sepsal	sepsat	k5eAaPmAgInS	sepsat
a	a	k8xC	a
uchoval	uchovat	k5eAaPmAgInS	uchovat
Li	li	k9	li
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
čen	čen	k?	čen
<g/>
.	.	kIx.	.
<g/>
Nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
čínskou	čínský	k2eAgFnSc7d1	čínská
vědkyní	vědkyně	k1gFnSc7	vědkyně
je	být	k5eAaImIp3nS	být
fyzička	fyzička	k1gFnSc1	fyzička
Chien-Shiung	Chien-Shiung	k1gMnSc1	Chien-Shiung
Wu	Wu	k1gMnSc1	Wu
<g/>
.	.	kIx.	.
</s>
<s>
Čchien	Čchien	k2eAgInSc1d1	Čchien
Süe-sen	Süeen	k1gInSc1	Süe-sen
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
raketové	raketový	k2eAgFnSc2d1	raketová
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
čínského	čínský	k2eAgInSc2d1	čínský
kosmického	kosmický	k2eAgInSc2d1	kosmický
programu	program	k1gInSc2	program
a	a	k8xC	a
hlavním	hlavní	k2eAgMnSc7d1	hlavní
konstruktérem	konstruktér	k1gMnSc7	konstruktér
čínských	čínský	k2eAgFnPc2d1	čínská
kosmických	kosmický	k2eAgFnPc2d1	kosmická
raket	raketa	k1gFnPc2	raketa
Dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
pochod	pochod	k1gInSc1	pochod
(	(	kIx(	(
<g/>
Čchang	Čchang	k1gMnSc1	Čchang
Čeng	Čeng	k1gMnSc1	Čeng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
Jang	Janga	k1gFnPc2	Janga
Čen-ning	Čening	k1gInSc4	Čen-ning
a	a	k8xC	a
Li	li	k9	li
Čeng-tao	Čengao	k6eAd1	Čeng-tao
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgNnSc4d1	stejné
ocenění	ocenění	k1gNnSc4	ocenění
získali	získat	k5eAaPmAgMnP	získat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
čínští	čínský	k2eAgMnPc1d1	čínský
emigranti	emigrant	k1gMnPc1	emigrant
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Cchuej	Cchuej	k1gInSc4	Cchuej
Čchi	Čch	k1gFnSc2	Čch
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
Charles	Charles	k1gMnSc1	Charles
Kuen	Kuen	k1gMnSc1	Kuen
Kao	Kao	k1gMnSc1	Kao
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
naturalizovaný	naturalizovaný	k2eAgMnSc1d1	naturalizovaný
Američan	Američan	k1gMnSc1	Američan
Čchiou	Čchiá	k1gFnSc4	Čchiá
Čcheng-tung	Čchengung	k1gInSc1	Čcheng-tung
získal	získat	k5eAaPmAgInS	získat
prestižní	prestižní	k2eAgFnSc4d1	prestižní
matematickou	matematický	k2eAgFnSc4d1	matematická
Fieldsovu	Fieldsův	k2eAgFnSc4d1	Fieldsova
medaili	medaile	k1gFnSc4	medaile
<g/>
,	,	kIx,	,
informatik	informatik	k1gMnSc1	informatik
Andrew	Andrew	k1gMnSc1	Andrew
Jao	Jao	k1gMnSc1	Jao
zase	zase	k9	zase
Turingovu	Turingův	k2eAgFnSc4d1	Turingova
cenu	cena	k1gFnSc4	cena
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemička	chemička	k1gFnSc1	chemička
Tchu	Tch	k2eAgFnSc4d1	Tch
Jou-jou	Jouá	k1gFnSc4	Jou-já
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
nebo	nebo	k8xC	nebo
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Nejprestižnější	prestižní	k2eAgNnSc4d3	nejprestižnější
vědecké	vědecký	k2eAgNnSc4d1	vědecké
ocenění	ocenění	k1gNnSc4	ocenění
převzala	převzít	k5eAaPmAgFnS	převzít
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
občanka	občanka	k1gFnSc1	občanka
ČLR	ČLR	kA	ČLR
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
izolovala	izolovat	k5eAaBmAgFnS	izolovat
hořčin	hořčina	k1gFnPc2	hořčina
artemisinin	artemisinin	k2eAgInSc1d1	artemisinin
z	z	k7c2	z
pelyňku	pelyněk	k1gInSc2	pelyněk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
využila	využít	k5eAaPmAgFnS	využít
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
malárie	malárie	k1gFnSc2	malárie
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
terapie	terapie	k1gFnSc1	terapie
snížila	snížit	k5eAaPmAgFnS	snížit
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
nemoci	nemoc	k1gFnSc2	nemoc
o	o	k7c4	o
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
tak	tak	k6eAd1	tak
zachránila	zachránit	k5eAaPmAgFnS	zachránit
stovky	stovka	k1gFnPc4	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
odborníkem	odborník	k1gMnSc7	odborník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
potravinářství	potravinářství	k1gNnSc2	potravinářství
je	být	k5eAaImIp3nS	být
Da-Wen	Da-Wen	k1gInSc1	Da-Wen
Sun	Sun	kA	Sun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
čínským	čínský	k2eAgMnSc7d1	čínský
filozofem	filozof	k1gMnSc7	filozof
je	být	k5eAaImIp3nS	být
Konfucius	Konfucius	k1gMnSc1	Konfucius
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
učení	učení	k1gNnSc4	učení
čínskou	čínský	k2eAgFnSc4d1	čínská
společnost	společnost	k1gFnSc4	společnost
hluboce	hluboko	k6eAd1	hluboko
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
a	a	k8xC	a
formovalo	formovat	k5eAaImAgNnS	formovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
oponentem	oponent	k1gMnSc7	oponent
byl	být	k5eAaImAgMnS	být
Mocius	Mocius	k1gMnSc1	Mocius
<g/>
,	,	kIx,	,
konfucianismus	konfucianismus	k1gInSc1	konfucianismus
však	však	k9	však
převládl	převládnout	k5eAaPmAgInS	převládnout
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Konfuciovým	Konfuciový	k2eAgMnPc3d1	Konfuciový
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
žákům	žák	k1gMnPc3	žák
patřil	patřit	k5eAaImAgInS	patřit
Mencius	Mencius	k1gInSc1	Mencius
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
pragmatickým	pragmatický	k2eAgMnSc7d1	pragmatický
oponentem	oponent	k1gMnSc7	oponent
byl	být	k5eAaImAgMnS	být
Sün-c	Sün	k1gFnSc4	Sün-c
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Materialistické	materialistický	k2eAgInPc1d1	materialistický
prvky	prvek	k1gInPc1	prvek
do	do	k7c2	do
konfuciánství	konfuciánství	k1gNnSc2	konfuciánství
vnášel	vnášet	k5eAaImAgMnS	vnášet
Wang	Wang	k1gMnSc1	Wang
Čchung	Čchung	k1gMnSc1	Čchung
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tvůrcům	tvůrce	k1gMnPc3	tvůrce
neokonfucianismu	neokonfucianismus	k1gInSc2	neokonfucianismus
v	v	k7c4	v
15.	[number]	k4	15.
století	století	k1gNnSc2	století
patřil	patřit	k5eAaImAgInS	patřit
Wang	Wang	k1gInSc1	Wang
Jang-ming	Janging	k1gInSc1	Jang-ming
<g/>
.	.	kIx.	.
</s>
<s>
Završitelem	Završitel	k1gMnSc7	Završitel
neokonfuciánské	okonfuciánský	k2eNgFnSc2d1	okonfuciánský
scholastiky	scholastika	k1gFnSc2	scholastika
byl	být	k5eAaImAgMnS	být
Ču	Ču	k1gMnSc1	Ču
Si	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
legismu	legismus	k1gInSc2	legismus
patřil	patřit	k5eAaImAgMnS	patřit
Chan	Chan	k1gMnSc1	Chan
Fej	Fej	k1gMnSc1	Fej
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
klíčovým	klíčový	k2eAgMnPc3d1	klíčový
představitelům	představitel	k1gMnPc3	představitel
Li	li	k8xS	li
S	s	k7c7	s
<g/>
'	'	kIx"	'
a	a	k8xC	a
Šang	Šang	k1gMnSc1	Šang
Jang	Jang	k1gMnSc1	Jang
<g/>
.	.	kIx.	.
</s>
<s>
Představitelem	představitel	k1gMnSc7	představitel
konfucianismu	konfucianismus	k1gInSc2	konfucianismus
20.	[number]	k4	20.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hledal	hledat	k5eAaImAgInS	hledat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
moderním	moderní	k2eAgInSc7d1	moderní
světem	svět	k1gInSc7	svět
a	a	k8xC	a
evropskou	evropský	k2eAgFnSc7d1	Evropská
kulturou	kultura	k1gFnSc7	kultura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Kchang	Kchang	k1gMnSc1	Kchang
Jou-wej	Jouej	k1gInSc4	Jou-wej
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gMnPc3	jeho
souputníkům	souputník	k1gMnPc3	souputník
v	v	k7c6	v
"	"	kIx"	"
<g/>
europeizačních	europeizační	k2eAgFnPc6d1	europeizační
snahách	snaha	k1gFnPc6	snaha
<g/>
"	"	kIx"	"
patřil	patřit	k5eAaImAgInS	patřit
Jen	jen	k9	jen
Fu	fu	k0	fu
nebo	nebo	k8xC	nebo
Liang	Liang	k1gMnSc1	Liang
Čchi-čchao	Čchi-čchao	k1gMnSc1	Čchi-čchao
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
6.	[number]	k4	6.
století	století	k1gNnSc6	století
přišla	přijít	k5eAaPmAgFnS	přijít
myšlenková	myšlenkový	k2eAgFnSc1d1	myšlenková
revoluce	revoluce	k1gFnSc1	revoluce
s	s	k7c7	s
dílem	dílo	k1gNnSc7	dílo
záhadného	záhadný	k2eAgMnSc2d1	záhadný
autora	autor	k1gMnSc2	autor
známého	známý	k2eAgMnSc2d1	známý
jako	jako	k8xC	jako
Lao-c	Lao	k1gInSc1	Lao-c
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
učení	učení	k1gNnSc1	učení
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
taoismus	taoismus	k1gInSc1	taoismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Laovým	Laový	k2eAgMnPc3d1	Laový
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
následovníkům	následovník	k1gMnPc3	následovník
patřil	patřit	k5eAaImAgInS	patřit
Čuang-c	Čuang	k1gInSc1	Čuang-c
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pronikání	pronikání	k1gNnSc4	pronikání
buddhismu	buddhismus	k1gInSc2	buddhismus
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
měl	mít	k5eAaImAgInS	mít
zásadní	zásadní	k2eAgInSc1d1	zásadní
podíl	podíl	k1gInSc1	podíl
Kumáradžíva	Kumáradžíva	k1gFnSc1	Kumáradžíva
<g/>
.	.	kIx.	.
<g/>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
čínské	čínský	k2eAgFnSc2d1	čínská
historiografie	historiografie	k1gFnSc2	historiografie
byl	být	k5eAaImAgInS	být
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
ma	ma	k?	ma
Čchien	Čchien	k1gInSc1	Čchien
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
rozvoji	rozvoj	k1gInSc6	rozvoj
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
<g/>
:	:	kIx,	:
Pan	Pan	k1gMnSc1	Pan
Čao	čao	k0	čao
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamějším	významý	k2eAgMnPc3d3	nejvýznamější
moderním	moderní	k2eAgMnPc3d1	moderní
historikům	historik	k1gMnPc3	historik
patřili	patřit	k5eAaImAgMnP	patřit
Chuang	Chuang	k1gMnSc1	Chuang
Sien-fan	Sienan	k1gMnSc1	Sien-fan
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
zakladatelem	zakladatel	k1gMnSc7	zakladatel
čínské	čínský	k2eAgFnSc2d1	čínská
antropologie	antropologie	k1gFnSc2	antropologie
<g/>
,	,	kIx,	,
či	či	k8xC	či
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
ma	ma	k?	ma
Kuang	Kuang	k1gInSc1	Kuang
<g/>
.	.	kIx.	.
</s>
<s>
Vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
6.	[number]	k4	6.
století	století	k1gNnPc2	století
př.	př.	kA	př.
n.	n.	k?	n.
l.	l.	k?	l.
Sun-c	Sun	k1gInSc1	Sun-c
<g/>
'	'	kIx"	'
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
legendárního	legendární	k2eAgInSc2d1	legendární
traktátu	traktát	k1gInSc2	traktát
Umění	umění	k1gNnSc1	umění
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
strategii	strategie	k1gFnSc6	strategie
a	a	k8xC	a
taktice	taktika	k1gFnSc6	taktika
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
čínské	čínský	k2eAgFnSc2d1	čínská
hláskové	hláskový	k2eAgFnSc2d1	hlásková
abecedy	abeceda	k1gFnSc2	abeceda
pchin-jin	pchinin	k1gInSc1	pchin-jin
byl	být	k5eAaImAgMnS	být
jazykovědec	jazykovědec	k1gMnSc1	jazykovědec
Čou	Čou	k1gMnSc1	Čou
Jou-kuang	Jouuang	k1gMnSc1	Jou-kuang
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
velkou	velký	k2eAgFnSc4d1	velká
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
reformu	reforma	k1gFnSc4	reforma
(	(	kIx(	(
<g/>
paj-chua	pajhua	k6eAd1	paj-chua
<g/>
)	)	kIx)	)
ve	v	k7c4	v
20.	[number]	k4	20.
století	století	k1gNnSc2	století
usiloval	usilovat	k5eAaImAgMnS	usilovat
Chu	Chu	k1gMnSc1	Chu
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Od	od	k7c2	od
80.	[number]	k4	80.
let	léto	k1gNnPc2	léto
20.	[number]	k4	20.
století	století	k1gNnSc2	století
zažívá	zažívat	k5eAaImIp3nS	zažívat
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
obrovský	obrovský	k2eAgInSc1d1	obrovský
rozmach	rozmach	k1gInSc1	rozmach
sport	sport	k1gInSc1	sport
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
státní	státní	k2eAgFnSc4d1	státní
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
prvně	prvně	k?	prvně
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
letních	letní	k2eAgFnPc2d1	letní
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
první	první	k4xOgFnSc7	první
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
hvězdou	hvězda	k1gFnSc7	hvězda
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
stal	stát	k5eAaPmAgMnS	stát
gymnasta	gymnasta	k1gMnSc1	gymnasta
Li	li	k8xS	li
Ning	Ning	k1gMnSc1	Ning
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ČLR	ČLR	kA	ČLR
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
237	[number]	k4	237
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
(	(	kIx(	(
<g/>
k	k	k7c3	k
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
a	a	k8xC	a
dala	dát	k5eAaPmAgFnS	dát
světu	svět	k1gInSc3	svět
řadu	řad	k1gInSc2	řad
dalších	další	k2eAgMnPc2d1	další
úspěšných	úspěšný	k2eAgMnPc2d1	úspěšný
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
:	:	kIx,	:
K	k	k7c3	k
největším	veliký	k2eAgMnSc7d3	veliký
čínských	čínský	k2eAgMnPc2d1	čínský
sportovním	sportovní	k2eAgFnPc3d1	sportovní
hvězdám	hvězda	k1gFnPc3	hvězda
patří	patřit	k5eAaImIp3nS	patřit
tenistka	tenistka	k1gFnSc1	tenistka
Li	li	k8xS	li
Na	na	k7c6	na
<g/>
,	,	kIx,	,
vítězka	vítězka	k1gFnSc1	vítězka
Australian	Australian	k1gMnSc1	Australian
Open	Open	k1gMnSc1	Open
2014	[number]	k4	2014
a	a	k8xC	a
Roland	Roland	k1gInSc4	Roland
Garros	Garrosa	k1gFnPc2	Garrosa
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
byla	být	k5eAaImAgFnS	být
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
Pcheng	Pcheng	k1gMnSc1	Pcheng
Šuaj	Šuaj	k1gMnSc1	Šuaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
lize	liga	k1gFnSc6	liga
NBA	NBA	kA	NBA
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
baskebalista	baskebalista	k1gMnSc1	baskebalista
Jao	Jao	k1gMnSc1	Jao
Ming	Ming	k1gMnSc1	Ming
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
svou	svůj	k3xOyFgFnSc7	svůj
výškou	výška	k1gFnSc7	výška
(	(	kIx(	(
<g/>
229	[number]	k4	229
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skokanka	skokanka	k1gFnSc1	skokanka
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
Wu	Wu	k1gFnSc1	Wu
Min-sia	Minia	k1gFnSc1	Min-sia
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
legendou	legenda	k1gFnSc7	legenda
své	svůj	k3xOyFgFnSc2	svůj
disciplíny	disciplína	k1gFnSc2	disciplína
a	a	k8xC	a
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
pět	pět	k4xCc4	pět
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
o	o	k7c4	o
trochu	trochu	k6eAd1	trochu
méně	málo	k6eAd2	málo
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
její	její	k3xOp3gFnSc2	její
kolegyně	kolegyně	k1gFnSc2	kolegyně
Fu	fu	k0	fu
Ming-sia	Mingium	k1gNnPc1	Ming-sium
<g/>
,	,	kIx,	,
Čchen	Čchen	k2eAgInSc1d1	Čchen
Žuo-lin	Žuoin	k1gInSc1	Žuo-lin
a	a	k8xC	a
Kuo	Kuo	k1gFnSc1	Kuo
Ťing-ťing	Ťing-ťing	k1gInSc1	Ťing-ťing
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
ve	v	k7c6	v
skocích	skok	k1gInPc6	skok
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
získala	získat	k5eAaPmAgFnS	získat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2018	[number]	k4	2018
již	již	k9	již
40	[number]	k4	40
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
její	její	k3xOp3gFnSc1	její
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Li	li	k8xS	li
Ningových	Ningův	k2eAgFnPc6d1	Ningův
stopách	stopa	k1gFnPc6	stopa
šel	jít	k5eAaImAgMnS	jít
především	především	k9	především
gymnasta	gymnasta	k1gMnSc1	gymnasta
Cou	Cou	k1gMnSc1	Cou
Kchaj	Kchaj	k1gMnSc1	Kchaj
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
pěti	pět	k4xCc2	pět
olympijských	olympijský	k2eAgFnPc2d1	olympijská
zlatých	zlatá	k1gFnPc2	zlatá
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
Li	li	k8xS	li
Siao-pcheng	Siaocheng	k1gInSc1	Siao-pcheng
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
tradičním	tradiční	k2eAgMnPc3d1	tradiční
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
stolní	stolní	k2eAgInSc1d1	stolní
tenis	tenis	k1gInSc1	tenis
<g/>
:	:	kIx,	:
Ma	Ma	k1gFnSc1	Ma
Lung	Lunga	k1gFnPc2	Lunga
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
olympijská	olympijský	k2eAgNnPc4d1	Olympijské
zlata	zlato	k1gNnPc4	zlato
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
desetinásobným	desetinásobný	k2eAgMnSc7d1	desetinásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgFnSc1d1	stolní
tenistka	tenistka	k1gFnSc1	tenistka
Teng	Teng	k1gInSc4	Teng
Ja-pching	Jaching	k1gInSc1	Ja-pching
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
OH	OH	kA	OH
dokonce	dokonce	k9	dokonce
čtyři	čtyři	k4xCgNnPc4	čtyři
zlata	zlato	k1gNnPc4	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
ženami	žena	k1gFnPc7	žena
dominovala	dominovat	k5eAaImAgFnS	dominovat
rovněž	rovněž	k9	rovněž
Wang	Wang	k1gInSc4	Wang
Nan	Nan	k1gFnSc2	Nan
či	či	k8xC	či
Čang	Čang	k1gMnSc1	Čang
I-ning	Iing	k1gInSc1	I-ning
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
Číňanem	Číňan	k1gMnSc7	Číňan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zkompletoval	zkompletovat	k5eAaPmAgMnS	zkompletovat
ping-pongový	pingongový	k2eAgInSc4d1	ping-pongový
grandslam	grandslam	k1gInSc4	grandslam
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vítěz	vítěz	k1gMnSc1	vítěz
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Liou	Lioa	k1gFnSc4	Lioa
Kuo-liang	Kuoianga	k1gFnPc2	Kuo-lianga
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
Kchung	Kchung	k1gMnSc1	Kchung
Ling-chuej	Linghuej	k1gInSc1	Ling-chuej
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetím	třetí	k4xOgMnSc6	třetí
Čang	Čang	k1gInSc1	Čang
Ťi-kche	Ťich	k1gInPc1	Ťi-kch
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
jen	jen	k6eAd1	jen
Švédovi	Švéd	k1gMnSc3	Švéd
Jan-Ove	Jan-Oev	k1gFnSc2	Jan-Oev
Waldnerovi	Waldnerův	k2eAgMnPc1d1	Waldnerův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
jsou	být	k5eAaImIp3nP	být
Číňané	Číňan	k1gMnPc1	Číňan
v	v	k7c6	v
badmintonu	badminton	k1gInSc6	badminton
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Lin	Lina	k1gFnPc2	Lina
Tan	Tan	k1gFnSc3	Tan
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
mužský	mužský	k2eAgMnSc1d1	mužský
badmintonista	badmintonista	k1gMnSc1	badmintonista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
v	v	k7c6	v
individuální	individuální	k2eAgFnSc6d1	individuální
disciplíně	disciplína	k1gFnSc6	disciplína
dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
badmintonové	badmintonový	k2eAgFnSc2d1	badmintonová
dvouhry	dvouhra	k1gFnSc2	dvouhra
je	být	k5eAaImIp3nS	být
Fu	fu	k0	fu
Chaj-feng	Chajeng	k1gInSc4	Chaj-feng
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ping-pongu	pingong	k1gInSc6	ping-pong
a	a	k8xC	a
badmintonu	badminton	k1gInSc6	badminton
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
Číňané	Číňan	k1gMnPc1	Číňan
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
historických	historický	k2eAgFnPc2d1	historická
tabulek	tabulka	k1gFnPc2	tabulka
olympijských	olympijský	k2eAgMnPc2d1	olympijský
medailistů	medailista	k1gMnPc2	medailista
<g/>
.	.	kIx.	.
</s>
<s>
Plavec	plavec	k1gMnSc1	plavec
Sun	Sun	kA	Sun
Jang	Jang	k1gMnSc1	Jang
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
olympijské	olympijský	k2eAgFnPc4d1	olympijská
zlaté	zlatý	k2eAgFnPc4d1	zlatá
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
,	,	kIx,	,
v	v	k7c6	v
ženském	ženský	k2eAgNnSc6d1	ženské
plavání	plavání	k1gNnSc6	plavání
brala	brát	k5eAaImAgFnS	brát
dvě	dva	k4xCgFnPc4	dva
olympijské	olympijský	k2eAgFnPc4d1	olympijská
zlaté	zlatý	k2eAgFnPc4d1	zlatá
Jie	Jie	k1gFnPc4	Jie
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
wen	wen	k?	wen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgMnPc3d3	nejúspěšnější
atletům	atlet	k1gMnPc3	atlet
patří	patřit	k5eAaImIp3nS	patřit
běžkyně	běžkyně	k1gFnSc1	běžkyně
Wang	Wang	k1gInSc1	Wang
Ťün-sia	Ťünia	k1gFnSc1	Ťün-sia
a	a	k8xC	a
Sing	Sing	k1gInSc1	Sing
Chuej-na	Chuejo	k1gNnSc2	Chuej-no
<g/>
,	,	kIx,	,
překážkář	překážkář	k1gMnSc1	překážkář
Liou	Lious	k1gInSc2	Lious
Siang	Siang	k1gMnSc1	Siang
či	či	k8xC	či
chodec	chodec	k1gMnSc1	chodec
Čchen	Čchna	k1gFnPc2	Čchna
Ting	Ting	k1gMnSc1	Ting
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šachu	šach	k1gInSc6	šach
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
září	září	k1gNnSc2	září
Chou	Chous	k1gInSc2	Chous
I-fan	Iana	k1gFnPc2	I-fana
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
předchůdkyní	předchůdkyně	k1gFnSc7	předchůdkyně
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
90.	[number]	k4	90.
letech	let	k1gInPc6	let
Sie	Sie	k1gFnSc2	Sie
Ťün	Ťün	k1gMnPc1	Ťün
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc4	čaj
Čchao	Čchao	k6eAd1	Čchao
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
házenkářkou	házenkářka	k1gFnSc7	házenkářka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
volejbalové	volejbalový	k2eAgFnSc2d1	volejbalová
federace	federace	k1gFnSc2	federace
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
Lang	Lang	k1gMnSc1	Lang
Pching	Pching	k1gInSc1	Pching
<g/>
.	.	kIx.	.
<g/>
Zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
nebyly	být	k5eNaImAgFnP	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
dlouho	dlouho	k6eAd1	dlouho
příliš	příliš	k6eAd1	příliš
populární	populární	k2eAgMnSc1d1	populární
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
však	však	k9	však
Číňané	Číňan	k1gMnPc1	Číňan
vrhli	vrhnout	k5eAaImAgMnP	vrhnout
na	na	k7c4	na
rychlobruslení	rychlobruslení	k1gNnSc4	rychlobruslení
a	a	k8xC	a
zejména	zejména	k9	zejména
v	v	k7c6	v
disciplíně	disciplína	k1gFnSc6	disciplína
zvané	zvaný	k2eAgInPc1d1	zvaný
short-track	shortrack	k6eAd1	short-track
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
mimořádných	mimořádný	k2eAgInPc2d1	mimořádný
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Wang	Wang	k1gInSc1	Wang
Meng	Menga	k1gFnPc2	Menga
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgNnPc4	čtyři
olympijská	olympijský	k2eAgNnPc4d1	Olympijské
zlata	zlato	k1gNnPc4	zlato
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
kolegyně	kolegyně	k1gFnSc1	kolegyně
Čou	Čou	k1gMnSc1	Čou
Jang	Jang	k1gMnSc1	Jang
tři	tři	k4xCgFnPc4	tři
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
"	"	kIx"	"
<g/>
zimními	zimní	k2eAgFnPc7d1	zimní
<g/>
"	"	kIx"	"
hvězdami	hvězda	k1gFnPc7	hvězda
čínského	čínský	k2eAgInSc2d1	čínský
sportu	sport	k1gInSc2	sport
jsou	být	k5eAaImIp3nP	být
manželé	manžel	k1gMnPc1	manžel
Šen	Šen	k1gMnSc1	Šen
Süe	Süe	k1gMnSc1	Süe
a	a	k8xC	a
Čao	čao	k0	čao
Chung-po	Chunga	k1gFnSc5	Chung-pa
<g/>
,	,	kIx,	,
olympijští	olympijský	k2eAgMnPc1d1	olympijský
vítězové	vítěz	k1gMnPc1	vítěz
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
krasobruslařských	krasobruslařský	k2eAgFnPc2d1	krasobruslařská
sportovních	sportovní	k2eAgFnPc2d1	sportovní
dvojic	dvojice	k1gFnPc2	dvojice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
asijských	asijský	k2eAgFnPc6d1	asijská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
mají	mít	k5eAaImIp3nP	mít
bojová	bojový	k2eAgNnPc1d1	bojové
umění	umění	k1gNnPc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
své	svůj	k3xOyFgFnPc4	svůj
techniky	technika	k1gFnPc4	technika
pod	pod	k7c4	pod
značku	značka	k1gFnSc4	značka
wu-šu	wuat	k5eAaPmIp1nS	wu-sat
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgMnPc3d1	známý
učitelům	učitel	k1gMnPc3	učitel
patřil	patřit	k5eAaImAgInS	patřit
například	například	k6eAd1	například
Yip	Yip	k1gFnSc4	Yip
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
k	k	k7c3	k
jehož	jehož	k3xOyRp3gMnPc3	jehož
žákům	žák	k1gMnPc3	žák
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
Bruce	Bruec	k1gInPc4	Bruec
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
rodilý	rodilý	k2eAgMnSc1d1	rodilý
Američan	Američan	k1gMnSc1	Američan
s	s	k7c7	s
čínskými	čínský	k2eAgInPc7d1	čínský
kořeny	kořen	k1gInPc7	kořen
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
USA	USA	kA	USA
akčními	akční	k2eAgInPc7d1	akční
filmy	film	k1gInPc7	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
čínská	čínský	k2eAgFnSc1d1	čínská
komunita	komunita	k1gFnSc1	komunita
žije	žít	k5eAaImIp3nS	žít
tradičně	tradičně	k6eAd1	tradičně
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
slavným	slavný	k2eAgMnPc3d1	slavný
americkým	americký	k2eAgMnPc3d1	americký
sportovcům	sportovec	k1gMnPc3	sportovec
čínského	čínský	k2eAgInSc2d1	čínský
původu	původ	k1gInSc2	původ
patří	patřit	k5eAaImIp3nS	patřit
tenista	tenista	k1gMnSc1	tenista
Michael	Michael	k1gMnSc1	Michael
Chang	Chang	k1gMnSc1	Chang
(	(	kIx(	(
<g/>
vítěž	vítěž	k1gFnSc1	vítěž
Paříže	Paříž	k1gFnSc2	Paříž
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basketbalista	basketbalista	k1gMnSc1	basketbalista
Jeremy	Jerema	k1gFnSc2	Jerema
Lin	Lina	k1gFnPc2	Lina
<g/>
,	,	kIx,	,
plavec	plavec	k1gMnSc1	plavec
Nathan	Nathan	k1gMnSc1	Nathan
Adrian	Adrian	k1gMnSc1	Adrian
nebo	nebo	k8xC	nebo
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
Michelle	Michelle	k1gFnSc1	Michelle
Kwanová	Kwanová	k1gFnSc1	Kwanová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
zase	zase	k9	zase
narodil	narodit	k5eAaPmAgMnS	narodit
Patrick	Patrick	k1gMnSc1	Patrick
Chan	Chan	k1gMnSc1	Chan
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
krasobruslení	krasobruslení	k1gNnSc6	krasobruslení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínským	čínský	k2eAgInSc7d1	čínský
vynálezem	vynález	k1gInSc7	vynález
je	být	k5eAaImIp3nS	být
desková	deskový	k2eAgFnSc1d1	desková
hra	hra	k1gFnSc1	hra
go	go	k?	go
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
go	go	k?	go
20.	[number]	k4	20.
století	století	k1gNnSc2	století
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgMnSc1d1	označován
čínský	čínský	k2eAgMnSc1d1	čínský
rodák	rodák	k1gMnSc1	rodák
Go	Go	k1gFnSc2	Go
Seigen	Seigen	k1gInSc1	Seigen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
Čína	Čína	k1gFnSc1	Čína
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
uspořádala	uspořádat	k5eAaPmAgFnS	uspořádat
letní	letní	k2eAgFnSc1d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc4d1	stejné
město	město	k1gNnSc4	město
bude	být	k5eAaImBp3nS	být
roku	rok	k1gInSc2	rok
2022	[number]	k4	2022
hostit	hostit	k5eAaImF	hostit
i	i	k9	i
hry	hra	k1gFnPc4	hra
zimní	zimní	k2eAgFnPc4d1	zimní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
skoku	skok	k1gInSc6	skok
vpřed	vpřed	k6eAd1	vpřed
Čína	Čína	k1gFnSc1	Čína
dokonale	dokonale	k6eAd1	dokonale
napodobila	napodobit	k5eAaPmAgFnS	napodobit
industrializaci	industrializace	k1gFnSc4	industrializace
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
-	-	kIx~	-
se	se	k3xPyFc4	se
všemi	všecek	k3xTgInPc7	všecek
jejími	její	k3xOp3gFnPc7	její
chybami	chyba	k1gFnPc7	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
vzestup	vzestup	k1gInSc1	vzestup
byl	být	k5eAaImAgInS	být
doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
rychlým	rychlý	k2eAgNnSc7d1	rychlé
budováním	budování	k1gNnSc7	budování
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
čelí	čelit	k5eAaImIp3nS	čelit
Čína	Čína	k1gFnSc1	Čína
vážným	vážný	k2eAgInPc3d1	vážný
environmentálním	environmentální	k2eAgInPc3d1	environmentální
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
180	[number]	k4	180
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
Čína	Čína	k1gFnSc1	Čína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
109.	[number]	k4	109.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
Environmental	Environmental	k1gMnSc1	Environmental
Performance	performance	k1gFnSc2	performance
Index	index	k1gInSc1	index
(	(	kIx(	(
<g/>
Index	index	k1gInSc1	index
výkonnosti	výkonnost	k1gFnSc2	výkonnost
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ekologická	ekologický	k2eAgFnSc1d1	ekologická
stopa	stopa	k1gFnSc1	stopa
Číny	Čína	k1gFnSc2	Čína
se	se	k3xPyFc4	se
od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažnějším	závažný	k2eAgInSc7d3	nejzávažnější
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
znečištění	znečištění	k1gNnSc4	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
způsobené	způsobený	k2eAgNnSc1d1	způsobené
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
zničující	zničující	k2eAgMnSc1d1	zničující
<g/>
:	:	kIx,	:
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
poprvé	poprvé	k6eAd1	poprvé
Čína	Čína	k1gFnSc1	Čína
předstihla	předstihnout	k5eAaPmAgFnS	předstihnout
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
v	v	k7c6	v
celkových	celkový	k2eAgFnPc6d1	celková
emisích	emise	k1gFnPc6	emise
CO2	CO2	k1gFnSc2	CO2
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hodnoty	hodnota	k1gFnPc4	hodnota
8,1	[number]	k4	8,1
miliardy	miliarda	k4xCgFnSc2	miliarda
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
několika	několik	k4yIc6	několik
městech	město	k1gNnPc6	město
severní	severní	k2eAgFnSc2d1	severní
Číny	Čína	k1gFnSc2	Čína
naměřeno	naměřit	k5eAaBmNgNnS	naměřit
rekordních	rekordní	k2eAgInPc2d1	rekordní
více	hodně	k6eAd2	hodně
než	než	k8xS	než
800	[number]	k4	800
mikrogramů	mikrogram	k1gInPc2	mikrogram
pevných	pevný	k2eAgFnPc2d1	pevná
prachových	prachový	k2eAgFnPc2d1	prachová
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
polétavého	polétavý	k2eAgInSc2d1	polétavý
prachu	prach	k1gInSc2	prach
<g/>
)	)	kIx)	)
na	na	k7c4	na
metr	metr	k1gInSc4	metr
krychlový	krychlový	k2eAgInSc4d1	krychlový
-	-	kIx~	-
30	[number]	k4	30
násobek	násobek	k1gInSc4	násobek
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
neškodnou	škodný	k2eNgFnSc4d1	neškodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
důsledky	důsledek	k1gInPc4	důsledek
škod	škoda	k1gFnPc2	škoda
na	na	k7c6	na
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
nešlo	jít	k5eNaImAgNnS	jít
přehlížet	přehlížet	k5eAaImF	přehlížet
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
otázka	otázka	k1gFnSc1	otázka
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
priorit	priorita	k1gFnPc2	priorita
čínské	čínský	k2eAgFnSc2d1	čínská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
rozvoj	rozvoj	k1gInSc1	rozvoj
šetrný	šetrný	k2eAgInSc1d1	šetrný
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
snadný	snadný	k2eAgInSc4d1	snadný
úkol	úkol	k1gInSc4	úkol
<g/>
,	,	kIx,	,
uznává	uznávat	k5eAaImIp3nS	uznávat
mj.	mj.	kA	mj.
<g/>
i	i	k8xC	i
Světový	světový	k2eAgInSc1d1	světový
fond	fond	k1gInSc1	fond
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
přírody	příroda	k1gFnSc2	příroda
(	(	kIx(	(
<g/>
WWF	WWF	kA	WWF
<g/>
)	)	kIx)	)
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
musí	muset	k5eAaImIp3nS	muset
uživit	uživit	k5eAaPmF	uživit
téměř	téměř	k6eAd1	téměř
jednu	jeden	k4xCgFnSc4	jeden
pětinu	pětina	k1gFnSc4	pětina
světové	světový	k2eAgFnSc2d1	světová
populace	populace	k1gFnSc2	populace
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
omezenými	omezený	k2eAgInPc7d1	omezený
zdroji	zdroj	k1gInPc7	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
9	[number]	k4	9
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
<g/>
%	%	kIx~	%
světových	světový	k2eAgInPc2d1	světový
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
čínských	čínský	k2eAgFnPc6d1	čínská
provinciích	provincie	k1gFnPc6	provincie
klesají	klesat	k5eAaImIp3nP	klesat
hladiny	hladina	k1gFnPc1	hladina
podzemních	podzemní	k2eAgFnPc2d1	podzemní
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
zmizí	zmizet	k5eAaPmIp3nS	zmizet
asi	asi	k9	asi
30	[number]	k4	30
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
stoupá	stoupat	k5eAaImIp3nS	stoupat
eroze	eroze	k1gFnSc1	eroze
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
desertifikace	desertifikace	k1gFnSc2	desertifikace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
vegetace	vegetace	k1gFnSc2	vegetace
<g/>
,	,	kIx,	,
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
WWF	WWF	kA	WWF
je	být	k5eAaImIp3nS	být
však	však	k9	však
"	"	kIx"	"
<g/>
ekologická	ekologický	k2eAgFnSc1d1	ekologická
stopa	stopa	k1gFnSc1	stopa
<g/>
"	"	kIx"	"
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
1,4	[number]	k4	1,4
miliardy	miliarda	k4xCgFnSc2	miliarda
Číňanů	Číňan	k1gMnPc2	Číňan
skromná	skromný	k2eAgFnSc1d1	skromná
-	-	kIx~	-
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
činila	činit	k5eAaImAgFnS	činit
roční	roční	k2eAgFnSc1d1	roční
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
elektrické	elektrický	k2eAgFnSc6d1	elektrická
energii	energie	k1gFnSc6	energie
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
3 927,05	[number]	k4	3 927,05
kWh	kwh	kA	kwh
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
7 035,49	[number]	k4	7 035,49
kWh	kwh	kA	kwh
nebo	nebo	k8xC	nebo
emise	emise	k1gFnSc1	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgMnSc2d1	uhličitý
na	na	k7c4	na
obyvatele	obyvatel	k1gMnSc4	obyvatel
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
7,55	[number]	k4	7,55
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
8,89	[number]	k4	8,89
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zároveň	zároveň	k6eAd1	zároveň
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
ekologických	ekologický	k2eAgFnPc2d1	ekologická
organizací	organizace	k1gFnPc2	organizace
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
částech	část	k1gFnPc6	část
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
založených	založený	k2eAgNnPc2d1	založené
na	na	k7c6	na
iniciativě	iniciativa	k1gFnSc6	iniciativa
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
mj.	mj.	kA	mj.
"	"	kIx"	"
<g/>
Přátelé	přítel	k1gMnPc1	přítel
přírody	příroda	k1gFnSc2	příroda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Yunnan	Yunnan	k1gInSc4	Yunnan
Green	Green	k2eAgInSc4d1	Green
Watersheds	Watersheds	k1gInSc4	Watersheds
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Green	Green	k2eAgInSc1d1	Green
Earth	Earth	k1gInSc1	Earth
Volunteers	Volunteersa	k1gFnPc2	Volunteersa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
domácích	domácí	k2eAgMnPc2d1	domácí
aktivistů	aktivista	k1gMnPc2	aktivista
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
zastoupení	zastoupení	k1gNnSc2	zastoupení
i	i	k8xC	i
různé	různý	k2eAgFnSc2d1	různá
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
nevládní	vládní	k2eNgFnSc2d1	nevládní
organizace	organizace	k1gFnSc2	organizace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
environmentální	environmentální	k2eAgInPc1d1	environmentální
projekty	projekt	k1gInPc1	projekt
jsou	být	k5eAaImIp3nP	být
financovány	financovat	k5eAaBmNgInP	financovat
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
tak	tak	k8xC	tak
např.	např.	kA	např.
zřízením	zřízení	k1gNnSc7	zřízení
"	"	kIx"	"
<g/>
Velké	velký	k2eAgFnPc1d1	velká
zelené	zelený	k2eAgFnPc1d1	zelená
zdi	zeď	k1gFnPc1	zeď
<g/>
"	"	kIx"	"
realizuje	realizovat	k5eAaBmIp3nS	realizovat
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
projekt	projekt	k1gInSc1	projekt
v	v	k7c6	v
zalesňování	zalesňování	k1gNnSc6	zalesňování
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2050	[number]	k4	2050
osazena	osadit	k5eAaPmNgFnS	osadit
původními	původní	k2eAgInPc7d1	původní
druhy	druh	k1gInPc7	druh
stromů	strom	k1gInPc2	strom
plocha	plocha	k1gFnSc1	plocha
350 000	[number]	k4	350 000
km2	km2	k4	km2
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
vytvořit	vytvořit	k5eAaPmF	vytvořit
zelený	zelený	k2eAgInSc1d1	zelený
pás	pás	k1gInSc1	pás
mezi	mezi	k7c7	mezi
suchým	suchý	k2eAgInSc7d1	suchý
severem	sever	k1gInSc7	sever
a	a	k8xC	a
úrodným	úrodný	k2eAgInSc7d1	úrodný
jihem	jih	k1gInSc7	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
přijala	přijmout	k5eAaPmAgFnS	přijmout
čínská	čínský	k2eAgFnSc1d1	čínská
vláda	vláda	k1gFnSc1	vláda
další	další	k2eAgNnSc1d1	další
opatření	opatření	k1gNnSc1	opatření
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c6	na
snížení	snížení	k1gNnSc6	snížení
emisí	emise	k1gFnPc2	emise
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
HDP	HDP	kA	HDP
o	o	k7c4	o
33,8	[number]	k4	33,8
<g/>
%	%	kIx~	%
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030.	[number]	k4	2030.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
zákaz	zákaz	k1gInSc1	zákaz
výstavby	výstavba	k1gFnSc2	výstavba
nových	nový	k2eAgFnPc2d1	nová
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
emisní	emisní	k2eAgFnSc1d1	emisní
norma	norma	k1gFnSc1	norma
pro	pro	k7c4	pro
výfukové	výfukový	k2eAgInPc4d1	výfukový
plyny	plyn	k1gInPc4	plyn
Euro	euro	k1gNnSc1	euro
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
podíl	podíl	k1gInSc1	podíl
uhlí	uhlí	k1gNnSc2	uhlí
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
spotřebě	spotřeba	k1gFnSc6	spotřeba
energie	energie	k1gFnSc2	energie
snížit	snížit	k5eAaPmF	snížit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2030	[number]	k4	2030
z	z	k7c2	z
66,6	[number]	k4	66,6
na	na	k7c4	na
méně	málo	k6eAd2	málo
než	než	k8xS	než
45	[number]	k4	45
<g/>
%	%	kIx~	%
a	a	k8xC	a
podíl	podíl	k1gInSc4	podíl
obnovitelných	obnovitelný	k2eAgFnPc2d1	obnovitelná
energií	energie	k1gFnPc2	energie
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
období	období	k1gNnSc6	období
zvýšit	zvýšit	k5eAaPmF	zvýšit
na	na	k7c6	na
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
opatření	opatření	k1gNnPc2	opatření
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
znečištění	znečištění	k1gNnSc2	znečištění
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
také	také	k9	také
významné	významný	k2eAgNnSc1d1	významné
snížení	snížení	k1gNnSc1	snížení
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
uhelných	uhelný	k2eAgInPc2d1	uhelný
dolů	dol	k1gInPc2	dol
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
roční	roční	k2eAgFnSc7d1	roční
kapacitou	kapacita	k1gFnSc7	kapacita
těžby	těžba	k1gFnSc2	těžba
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
uzavřeny	uzavřen	k2eAgInPc4d1	uzavřen
nadbytečné	nadbytečný	k2eAgInPc4d1	nadbytečný
doly	dol	k1gInPc4	dol
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
500	[number]	k4	500
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
ratifikovala	ratifikovat	k5eAaBmAgFnS	ratifikovat
Kjótský	Kjótský	k2eAgInSc4d1	Kjótský
protokol	protokol	k1gInSc4	protokol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
a	a	k8xC	a
Pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
klimatu	klima	k1gNnSc6	klima
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
Číny	Čína	k1gFnSc2	Čína
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc1	stát
světovým	světový	k2eAgMnSc7d1	světový
lídrem	lídr	k1gMnSc7	lídr
v	v	k7c6	v
ochraně	ochrana	k1gFnSc6	ochrana
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
hlavní	hlavní	k2eAgNnPc1d1	hlavní
čínská	čínský	k2eAgNnPc1d1	čínské
města	město	k1gNnPc1	město
bývají	bývat	k5eAaImIp3nP	bývat
nadále	nadále	k6eAd1	nadále
pravidelně	pravidelně	k6eAd1	pravidelně
zasahována	zasahovat	k5eAaImNgFnS	zasahovat
silným	silný	k2eAgInSc7d1	silný
smogem	smog	k1gInSc7	smog
<g/>
,	,	kIx,	,
zátěž	zátěž	k1gFnSc4	zátěž
znečištěním	znečištění	k1gNnSc7	znečištění
rok	rok	k1gInSc4	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
znečištění	znečištění	k1gNnSc1	znečištění
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předchozím	předchozí	k2eAgInSc7d1	předchozí
rokem	rok	k1gInSc7	rok
o	o	k7c4	o
53,8	[number]	k4	53,8
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
muselo	muset	k5eAaImAgNnS	muset
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Číně	Čína	k1gFnSc6	Čína
ukončit	ukončit	k5eAaPmF	ukončit
činnost	činnost	k1gFnSc4	činnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
176 000	[number]	k4	176 000
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
44 000	[number]	k4	44 000
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesplňovaly	splňovat	k5eNaImAgInP	splňovat
stanovené	stanovený	k2eAgInPc1d1	stanovený
emisní	emisní	k2eAgInPc1d1	emisní
cíle	cíl	k1gInPc1	cíl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
zmizelo	zmizet	k5eAaPmAgNnS	zmizet
v	v	k7c6	v
uhelném	uhelný	k2eAgInSc6d1	uhelný
průmyslu	průmysl	k1gInSc6	průmysl
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
dalších	další	k2eAgFnPc2d1	další
500 000	[number]	k4	500 000
v	v	k7c6	v
ocelářském	ocelářský	k2eAgInSc6d1	ocelářský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
investuje	investovat	k5eAaBmIp3nS	investovat
nemalé	malý	k2eNgInPc4d1	nemalý
prostředky	prostředek	k1gInPc4	prostředek
do	do	k7c2	do
nefosilních	fosilní	k2eNgInPc2d1	fosilní
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
uplynulých	uplynulý	k2eAgInPc6d1	uplynulý
letech	let	k1gInPc6	let
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
postaveno	postavit	k5eAaPmNgNnS	postavit
více	hodně	k6eAd2	hodně
solárních	solární	k2eAgMnPc2d1	solární
a	a	k8xC	a
větrných	větrný	k2eAgFnPc2d1	větrná
turbín	turbína	k1gFnPc2	turbína
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
světa	svět	k1gInSc2	svět
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
čínská	čínský	k2eAgFnSc1d1	čínská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
další	další	k2eAgFnSc1d1	další
investice	investice	k1gFnSc1	investice
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
přibližně	přibližně	k6eAd1	přibližně
360	[number]	k4	360
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
pro	pro	k7c4	pro
větrnou	větrný	k2eAgFnSc4d1	větrná
energii	energie	k1gFnSc4	energie
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
by	by	kYmCp3nS	by
instalovaná	instalovaný	k2eAgFnSc1d1	instalovaná
kapacita	kapacita	k1gFnSc1	kapacita
měla	mít	k5eAaImAgFnS	mít
vzrůst	vzrůst	k1gInSc4	vzrůst
ze	z	k7c2	z
151	[number]	k4	151
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
205	[number]	k4	205
gigawattů	gigawatt	k1gInPc2	gigawatt
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
solární	solární	k2eAgFnSc4d1	solární
energii	energie	k1gFnSc4	energie
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
plán	plán	k1gInSc1	plán
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
její	její	k3xOp3gNnSc1	její
ztrojnásobení	ztrojnásobení	k1gNnSc1	ztrojnásobení
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
příklad	příklad	k1gInSc1	příklad
zároveň	zároveň	k6eAd1	zároveň
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energetický	energetický	k2eAgInSc1d1	energetický
přechod	přechod	k1gInSc1	přechod
přináší	přinášet	k5eAaImIp3nS	přinášet
i	i	k9	i
ekonomické	ekonomický	k2eAgFnPc4d1	ekonomická
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3,5	[number]	k4	3,5
milionu	milion	k4xCgInSc2	milion
Číňanů	Číňan	k1gMnPc2	Číňan
již	již	k6eAd1	již
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
odvětví	odvětví	k1gNnSc6	odvětví
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020.	[number]	k4	2020.
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
vláda	vláda	k1gFnSc1	vláda
masivně	masivně	k6eAd1	masivně
podporuje	podporovat	k5eAaImIp3nS	podporovat
dotacemi	dotace	k1gFnPc7	dotace
nástup	nástup	k1gInSc1	nástup
elektromobilů	elektromobil	k1gInPc2	elektromobil
a	a	k8xC	a
kvótami	kvóta	k1gFnPc7	kvóta
a	a	k8xC	a
omezeními	omezení	k1gNnPc7	omezení
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
vytlačit	vytlačit	k5eAaPmF	vytlačit
spalovací	spalovací	k2eAgInPc4d1	spalovací
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šesti	šest	k4xCc6	šest
velkých	velký	k2eAgInPc6d1	velký
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
postiženy	postihnout	k5eAaPmNgFnP	postihnout
znečištěním	znečištění	k1gNnSc7	znečištění
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ročně	ročně	k6eAd1	ročně
(	(	kIx(	(
<g/>
losem	los	k1gInSc7	los
nebo	nebo	k8xC	nebo
aukcemi	aukce	k1gFnPc7	aukce
<g/>
)	)	kIx)	)
povoleno	povolit	k5eAaPmNgNnS	povolit
pouze	pouze	k6eAd1	pouze
650 000	[number]	k4	650 000
registrací	registrace	k1gFnPc2	registrace
nových	nový	k2eAgInPc2d1	nový
osobních	osobní	k2eAgInPc2d1	osobní
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
vyhrazena	vyhradit	k5eAaPmNgFnS	vyhradit
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
registraci	registrace	k1gFnSc4	registrace
elektrických	elektrický	k2eAgNnPc2d1	elektrické
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Elektromobily	elektromobila	k1gFnPc1	elektromobila
nabízejí	nabízet	k5eAaImIp3nP	nabízet
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
další	další	k2eAgFnPc4d1	další
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
mohou	moct	k5eAaImIp3nP	moct
jezdit	jezdit	k5eAaImF	jezdit
sedm	sedm	k4xCc4	sedm
dní	den	k1gInPc2	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
automobily	automobil	k1gInPc1	automobil
se	s	k7c7	s
spalovacími	spalovací	k2eAgInPc7d1	spalovací
motory	motor	k1gInPc7	motor
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
posledním	poslední	k2eAgNnSc6d1	poslední
čísle	číslo	k1gNnSc6	číslo
SPZ	SPZ	kA	SPZ
<g/>
,	,	kIx,	,
zákaz	zákaz	k1gInSc1	zákaz
jízdy	jízda	k1gFnSc2	jízda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nákupu	nákup	k1gInSc6	nákup
elektrického	elektrický	k2eAgNnSc2d1	elektrické
nebo	nebo	k8xC	nebo
hybridního	hybridní	k2eAgNnSc2d1	hybridní
elektrického	elektrický	k2eAgNnSc2d1	elektrické
vozidla	vozidlo	k1gNnSc2	vozidlo
hradí	hradit	k5eAaImIp3nS	hradit
centrální	centrální	k2eAgFnSc1d1	centrální
vláda	vláda	k1gFnSc1	vláda
státní	státní	k2eAgFnSc4d1	státní
prémii	prémie	k1gFnSc4	prémie
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
4 600	[number]	k4	4 600
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
musí	muset	k5eAaImIp3nP	muset
všechny	všechen	k3xTgFnPc4	všechen
domácí	domácí	k2eAgFnPc4d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
automobilky	automobilka	k1gFnPc4	automobilka
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
10	[number]	k4	10
<g/>
%	%	kIx~	%
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2020 12	[number]	k4	2020 12
<g/>
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
elektromobilů	elektromobil	k1gInPc2	elektromobil
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
celkové	celkový	k2eAgFnSc6d1	celková
produkci	produkce	k1gFnSc6	produkce
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
této	tento	k3xDgFnSc2	tento
kvóty	kvóta	k1gFnSc2	kvóta
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
značný	značný	k2eAgInSc1d1	značný
tlak	tlak	k1gInSc1	tlak
na	na	k7c4	na
výrobce	výrobce	k1gMnPc4	výrobce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Volkswagen	volkswagen	k1gInSc4	volkswagen
<g/>
,	,	kIx,	,
BMW	BMW	kA	BMW
a	a	k8xC	a
Daimler	Daimler	k1gInSc1	Daimler
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgInSc7d3	veliký
jednotným	jednotný	k2eAgInSc7d1	jednotný
trhem	trh	k1gInSc7	trh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
indexu	index	k1gInSc2	index
elektrických	elektrický	k2eAgNnPc2d1	elektrické
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
vyvinutým	vyvinutý	k2eAgNnSc7d1	vyvinuté
společností	společnost	k1gFnSc7	společnost
McKinsey	McKinsea	k1gFnSc2	McKinsea
&	&	k?	&
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
"	"	kIx"	"
<g/>
utíká	utíkat	k5eAaImIp3nS	utíkat
svým	svůj	k3xOyFgMnPc3	svůj
konkurentům	konkurent	k1gMnPc3	konkurent
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgNnPc1d1	elektrické
vozidla	vozidlo	k1gNnPc1	vozidlo
prodaná	prodaný	k2eAgNnPc1d1	prodané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
336 000	[number]	k4	336 000
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
171 000	[number]	k4	171 000
užitkových	užitkový	k2eAgNnPc2d1	užitkové
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
115 000	[number]	k4	115 000
autobusů	autobus	k1gInPc2	autobus
na	na	k7c4	na
baterie	baterie	k1gFnPc4	baterie
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
zemí	zem	k1gFnPc2	zem
s	s	k7c7	s
nejrozmanitějšími	rozmanitý	k2eAgInPc7d3	nejrozmanitější
modely	model	k1gInPc7	model
<g/>
;	;	kIx,	;
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
měli	mít	k5eAaImAgMnP	mít
čínští	čínský	k2eAgMnPc1d1	čínský
zájemci	zájemce	k1gMnPc1	zájemce
o	o	k7c6	o
koupi	koupě	k1gFnSc6	koupě
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
mezi	mezi	k7c7	mezi
téměř	téměř	k6eAd1	téměř
60	[number]	k4	60
různými	různý	k2eAgInPc7d1	různý
elektromodely	elektromodel	k1gInPc7	elektromodel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Volksrepublik_China	Volksrepublik_China	k1gFnSc1	Volksrepublik_China
<g/>
#	#	kIx~	#
<g/>
Ökologie	Ökologie	k1gFnSc1	Ökologie
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
83,3	[number]	k4	83,3
%	%	kIx~	%
čínských	čínský	k2eAgFnPc2d1	čínská
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
až	až	k9	až
49	[number]	k4	49
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vdaných	vdaný	k2eAgFnPc2d1	vdaná
či	či	k8xC	či
zadaných	zadaný	k2eAgFnPc2d1	zadaná
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
moderní	moderní	k2eAgFnSc2d1	moderní
metody	metoda	k1gFnSc2	metoda
antikoncepce	antikoncepce	k1gFnSc2	antikoncepce
a	a	k8xC	a
tímto	tento	k3xDgInSc7	tento
podílem	podíl	k1gInSc7	podíl
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
Čína	Čína	k1gFnSc1	Čína
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Číňané	Číňan	k1gMnPc1	Číňan
preferují	preferovat	k5eAaImIp3nP	preferovat
mužské	mužský	k2eAgMnPc4d1	mužský
potomky	potomek	k1gMnPc4	potomek
a	a	k8xC	a
dívky	dívka	k1gFnPc4	dívka
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
stávají	stávat	k5eAaImIp3nP	stávat
ještě	ještě	k9	ještě
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
narozením	narození	k1gNnSc7	narození
oběťmi	oběť	k1gFnPc7	oběť
uměle	uměle	k6eAd1	uměle
vyvolaných	vyvolaný	k2eAgInPc2d1	vyvolaný
potratů	potrat	k1gInPc2	potrat
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
přispěla	přispět	k5eAaPmAgFnS	přispět
i	i	k8xC	i
politika	politika	k1gFnSc1	politika
jednoho	jeden	k4xCgNnSc2	jeden
dítěte	dítě	k1gNnSc2	dítě
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
omezit	omezit	k5eAaPmF	omezit
růst	růst	k1gInSc4	růst
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
potratů	potrat	k1gInPc2	potrat
a	a	k8xC	a
zabíjení	zabíjení	k1gNnPc2	zabíjení
novorozeňat	novorozeně	k1gNnPc2	novorozeně
odhadem	odhad	k1gInSc7	odhad
o	o	k7c4	o
34	[number]	k4	34
milionů	milion	k4xCgInPc2	milion
více	hodně	k6eAd2	hodně
mužů	muž	k1gMnPc2	muž
než	než	k8xS	než
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
čínskou	čínský	k2eAgFnSc4d1	čínská
vládu	vláda	k1gFnSc4	vláda
pracuje	pracovat	k5eAaImIp3nS	pracovat
50 000	[number]	k4	50 000
až	až	k9	až
100 000	[number]	k4	100 000
hackerů	hacker	k1gMnPc2	hacker
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
odhaleno	odhalen	k2eAgNnSc1d1	odhaleno
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
největším	veliký	k2eAgInSc6d3	veliký
kyberútoku	kyberútok	k1gInSc6	kyberútok
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
historii	historie	k1gFnSc6	historie
čínští	čínský	k2eAgMnPc1d1	čínský
hackeři	hacker	k1gMnPc1	hacker
ukradli	ukradnout	k5eAaPmAgMnP	ukradnout
osobní	osobní	k2eAgInPc4d1	osobní
údaje	údaj	k1gInPc4	údaj
21,5	[number]	k4	21,5
milionu	milion	k4xCgInSc2	milion
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
amerických	americký	k2eAgInPc2d1	americký
federálních	federální	k2eAgInPc2d1	federální
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
existuje	existovat	k5eAaImIp3nS	existovat
velice	velice	k6eAd1	velice
přísná	přísný	k2eAgFnSc1d1	přísná
cenzura	cenzura	k1gFnSc1	cenzura
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
státních	státní	k2eAgMnPc2d1	státní
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
monitoruje	monitorovat	k5eAaImIp3nS	monitorovat
<g/>
,	,	kIx,	,
filtruje	filtrovat	k5eAaImIp3nS	filtrovat
a	a	k8xC	a
blokuje	blokovat	k5eAaImIp3nS	blokovat
obsah	obsah	k1gInSc4	obsah
čínského	čínský	k2eAgInSc2d1	čínský
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Čínští	čínský	k2eAgMnPc1d1	čínský
uživatelé	uživatel	k1gMnPc1	uživatel
se	se	k3xPyFc4	se
nedostanou	dostat	k5eNaPmIp3nP	dostat
na	na	k7c4	na
Twitter	Twitter	k1gInSc4	Twitter
<g/>
,	,	kIx,	,
Google	Google	k1gInSc4	Google
nebo	nebo	k8xC	nebo
Facebook	Facebook	k1gInSc4	Facebook
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
internetová	internetový	k2eAgFnSc1d1	internetová
společnost	společnost	k1gFnSc1	společnost
Facebook	Facebook	k1gInSc1	Facebook
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
cenzorský	cenzorský	k2eAgInSc4d1	cenzorský
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
čínský	čínský	k2eAgInSc4d1	čínský
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínský	čínský	k2eAgMnSc1d1	čínský
prezident	prezident	k1gMnSc1	prezident
Si	se	k3xPyFc3	se
Ťin-pching	Ťinching	k1gInSc4	Ťin-pching
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
spustil	spustit	k5eAaPmAgInS	spustit
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
protikorupční	protikorupční	k2eAgFnSc4d1	protikorupční
kampaň	kampaň	k1gFnSc4	kampaň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
potrestání	potrestání	k1gNnSc3	potrestání
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300 000	[number]	k4	300 000
stranických	stranický	k2eAgMnPc2d1	stranický
funkcionářů	funkcionář	k1gMnPc2	funkcionář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
vlastní	vlastní	k2eAgMnSc1d1	vlastní
švagr	švagr	k1gMnSc1	švagr
identifikován	identifikován	k2eAgMnSc1d1	identifikován
v	v	k7c6	v
uniklých	uniklý	k2eAgInPc6d1	uniklý
Panamských	panamský	k2eAgInPc6d1	panamský
dokumentech	dokument	k1gInPc6	dokument
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
detailní	detailní	k2eAgFnPc4d1	detailní
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
daňových	daňový	k2eAgInPc6d1	daňový
únicích	únik	k1gInPc6	únik
a	a	k8xC	a
praní	praní	k1gNnSc1	praní
špinavých	špinavý	k2eAgInPc2d1	špinavý
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Veškerá	veškerý	k3xTgNnPc4	veškerý
odhalení	odhalení	k1gNnPc4	odhalení
Panamských	panamský	k2eAgInPc2d1	panamský
dokumentů	dokument	k1gInPc2	dokument
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
rodiny	rodina	k1gFnPc4	rodina
Si	se	k3xPyFc3	se
Ťin-pchinga	Ťinchinga	k1gFnSc1	Ťin-pchinga
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
čínském	čínský	k2eAgInSc6d1	čínský
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
cenzurována	cenzurován	k2eAgFnSc1d1	cenzurována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
společnost	společnost	k1gFnSc1	společnost
CEFC	CEFC	kA	CEFC
China	China	k1gFnSc1	China
Energy	Energ	k1gInPc4	Energ
Company	Compana	k1gFnSc2	Compana
Limited	limited	k2eAgMnPc2d1	limited
získala	získat	k5eAaPmAgFnS	získat
četná	četný	k2eAgNnPc4d1	četné
aktiva	aktivum	k1gNnPc4	aktivum
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
většinu	většina	k1gFnSc4	většina
v	v	k7c4	v
Pivovary	pivovar	k1gInPc4	pivovar
Lobkowicz	Lobkowicz	k1gMnSc1	Lobkowicz
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
60	[number]	k4	60
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
ve	v	k7c6	v
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
klubu	klub	k1gInSc6	klub
SK	Sk	kA	Sk
Slavia	Slavia	k1gFnSc1	Slavia
Praha	Praha	k1gFnSc1	Praha
nebo	nebo	k8xC	nebo
49	[number]	k4	49
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
mediální	mediální	k2eAgFnSc6d1	mediální
společnosti	společnost	k1gFnSc6	společnost
Empresa	Empresa	k1gFnSc1	Empresa
Media	medium	k1gNnPc1	medium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
nebo	nebo	k8xC	nebo
vydává	vydávat	k5eAaImIp3nS	vydávat
týdeník	týdeník	k1gInSc1	týdeník
Týden	týden	k1gInSc1	týden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
médiích	médium	k1gNnPc6	médium
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
napojení	napojení	k1gNnSc6	napojení
firmy	firma	k1gFnSc2	firma
CEFC	CEFC	kA	CEFC
na	na	k7c4	na
čínskou	čínský	k2eAgFnSc4d1	čínská
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
rozvědku	rozvědka	k1gFnSc4	rozvědka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
Čína	Čína	k1gFnSc1	Čína
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
americké	americký	k2eAgInPc4d1	americký
státní	státní	k2eAgInPc4d1	státní
dluhopisy	dluhopis	k1gInPc4	dluhopis
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
1,244	[number]	k4	1,244
bilionu	bilion	k4xCgInSc2	bilion
USD	USD	kA	USD
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
největším	veliký	k2eAgMnSc7d3	veliký
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
věřitelem	věřitel	k1gMnSc7	věřitel
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
začala	začít	k5eAaPmAgFnS	začít
čínská	čínský	k2eAgFnSc1d1	čínská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Ujgurské	Ujgurský	k2eAgFnSc6d1	Ujgurská
autonomní	autonomní	k2eAgFnSc6d1	autonomní
oblasti	oblast	k1gFnSc6	oblast
Sin-ťiang	Sin-ťianga	k1gFnPc2	Sin-ťianga
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
budovat	budovat	k5eAaImF	budovat
převýchovné	převýchovný	k2eAgInPc4d1	převýchovný
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgMnPc6	který
internovala	internovat	k5eAaBmAgFnS	internovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc4	milion
muslimských	muslimský	k2eAgInPc2d1	muslimský
Ujgurů	Ujgur	k1gInPc2	Ujgur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
plánuje	plánovat	k5eAaImIp3nS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
spustí	spustit	k5eAaPmIp3nS	spustit
systém	systém	k1gInSc4	systém
sociálního	sociální	k2eAgInSc2d1	sociální
kreditu	kredit	k1gInSc2	kredit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgInS	označit
za	za	k7c4	za
nejpropracovanější	propracovaný	k2eAgInSc4d3	nejpropracovanější
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BECKER	BECKER	kA	BECKER
<g/>
,	,	kIx,	,
Jasper	Jasper	k1gInSc1	Jasper
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jasper	Jasper	k1gMnSc1	Jasper
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7257-729-8	[number]	k4	80-7257-729-8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FAIRBANK	FAIRBANK	kA	FAIRBANK
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
K.	K.	kA	K.
Dějiny	dějiny	k1gFnPc1	dějiny
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7422-007-4	[number]	k4	978-80-7422-007-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FISHMAN	FISHMAN	kA	FISHMAN
<g/>
,	,	kIx,	,
Ted	Ted	k1gMnSc1	Ted
C.	C.	kA	C.
China	China	k1gFnSc1	China
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
Čína	Čína	k1gFnSc1	Čína
drtí	drtit	k5eAaImIp3nS	drtit
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Alfa	alfa	k1gFnSc1	alfa
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86851-44-3	[number]	k4	80-86851-44-3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LIŠČÁK	LIŠČÁK	kA	LIŠČÁK
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7277-109-4	[number]	k4	80-7277-109-4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOJTA	Vojta	k1gMnSc1	Vojta
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Čínský	čínský	k2eAgInSc1d1	čínský
svět	svět	k1gInSc1	svět
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
porozumět	porozumět	k5eAaPmF	porozumět
současné	současný	k2eAgFnSc3d1	současná
Číně	Čína	k1gFnSc3	Čína
<g/>
,	,	kIx,	,
čínskému	čínský	k2eAgNnSc3d1	čínské
chování	chování	k1gNnSc3	chování
a	a	k8xC	a
myšlení	myšlení	k1gNnSc3	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Pixl-e	Pixl	k1gNnSc1	Pixl-e
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-905021-0-9	[number]	k4	978-80-905021-0-9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLIMEŠ	Klimeš	k1gMnSc1	Klimeš
<g/>
,	,	kIx,	,
<g/>
Ondřej	Ondřej	k1gMnSc1	Ondřej
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
diplomacie	diplomacie	k1gFnSc1	diplomacie
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
regionální	regionální	k2eAgFnPc4d1	regionální
variace	variace	k1gFnPc4	variace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-200-2962-1	[number]	k4	978-80-200-2962-1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
Čínu	Čína	k1gFnSc4	Čína
</s>
</p>
<p>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
</s>
</p>
<p>
<s>
Čínština	čínština	k1gFnSc1	čínština
</s>
</p>
<p>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
čínské	čínský	k2eAgFnSc2d1	čínská
historie	historie	k1gFnSc2	historie
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
skok	skok	k1gInSc1	skok
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Čína	Čína	k1gFnSc1	Čína
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Čína	Čína	k1gFnSc1	Čína
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Čína	Čína	k1gFnSc1	Čína
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
před	před	k7c7	před
30	[number]	k4	30
lety	léto	k1gNnPc7	léto
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
<g/>
:	:	kIx,	:
od	od	k7c2	od
hladomoru	hladomor	k1gInSc2	hladomor
k	k	k7c3	k
ekonomickému	ekonomický	k2eAgInSc3d1	ekonomický
zázraku	zázrak	k1gInSc3	zázrak
</s>
</p>
<p>
<s>
China	China	k1gFnSc1	China
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
China	China	k1gFnSc1	China
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
vyhráváme	vyhrávat	k5eAaImIp1nP	vyhrávat
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
tvrdí	tvrdý	k2eAgMnPc1d1	tvrdý
čínský	čínský	k2eAgMnSc1d1	čínský
disident	disident	k1gMnSc1	disident
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
of	of	k?	of
Origin	Origin	k2eAgInSc1d1	Origin
Information	Information	k1gInSc1	Information
Report	report	k1gInSc1	report
-	-	kIx~	-
China	China	k1gFnSc1	China
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
UK	UK	kA	UK
Border	Border	k1gMnSc1	Border
Agency	Agenca	k1gMnSc2	Agenca
<g/>
,	,	kIx,	,
2011-08-24	[number]	k4	2011-08-24
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-12-10	[number]	k4	2011-12-10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011-12-08	[number]	k4	2011-12-08
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
East	East	k1gMnSc1	East
Asian	Asian	k1gMnSc1	Asian
and	and	k?	and
Pacific	Pacific	k1gMnSc1	Pacific
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
China	China	k1gFnSc1	China
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.S.	U.S.	k?	U.S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2010-08-05	[number]	k4	2010-08-05
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
China	China	k1gFnSc1	China
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-03	[number]	k4	2011-08-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
China	China	k1gFnSc1	China
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2006-08-25	[number]	k4	2006-08-25
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo.cz	Businessinfo.cz	k1gMnSc1	Businessinfo.cz
<g/>
,	,	kIx,	,
2011-04-01	[number]	k4	2011-04-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2013-05-11	[number]	k4	2013-05-11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CHAN	CHAN	kA	CHAN
<g/>
,	,	kIx,	,
Hoklam	Hoklam	k1gInSc1	Hoklam
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
China	China	k1gFnSc1	China
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
