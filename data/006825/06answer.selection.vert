<s>
První	první	k4xOgFnSc1	první
čajovna	čajovna	k1gFnSc1	čajovna
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
objevila	objevit	k5eAaPmAgFnS	objevit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cestovatel	cestovatel	k1gMnSc1	cestovatel
Joe	Joe	k1gMnSc1	Joe
Hloucha	Hlouch	k1gMnSc4	Hlouch
založil	založit	k5eAaPmAgMnS	založit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
výstavišti	výstaviště	k1gNnSc6	výstaviště
japonskou	japonský	k2eAgFnSc4d1	japonská
čajovnu	čajovna	k1gFnSc4	čajovna
<g/>
.	.	kIx.	.
</s>
