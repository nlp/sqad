<s>
Čajovna	čajovna	k1gFnSc1	čajovna
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc4d1	veřejné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
chodí	chodit	k5eAaImIp3nP	chodit
pít	pít	k5eAaImF	pít
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
domácí	domácí	k2eAgFnPc4d1	domácí
místnosti	místnost	k1gFnPc4	místnost
nebo	nebo	k8xC	nebo
domky	domek	k1gInPc1	domek
určené	určený	k2eAgInPc1d1	určený
k	k	k7c3	k
pití	pití	k1gNnSc3	pití
čaje	čaj	k1gInSc2	čaj
například	například	k6eAd1	například
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
čajovna	čajovna	k1gFnSc1	čajovna
se	se	k3xPyFc4	se
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
objevila	objevit	k5eAaPmAgFnS	objevit
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
cestovatel	cestovatel	k1gMnSc1	cestovatel
Joe	Joe	k1gMnSc1	Joe
Hloucha	Hlouch	k1gMnSc4	Hlouch
založil	založit	k5eAaPmAgMnS	založit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
na	na	k7c6	na
pražském	pražský	k2eAgNnSc6d1	Pražské
výstavišti	výstaviště	k1gNnSc6	výstaviště
japonskou	japonský	k2eAgFnSc4d1	japonská
čajovnu	čajovna	k1gFnSc4	čajovna
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozmach	rozmach	k1gInSc1	rozmach
čajoven	čajovna	k1gFnPc2	čajovna
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k6eAd1	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
vznikají	vznikat	k5eAaImIp3nP	vznikat
první	první	k4xOgFnPc1	první
čajovny	čajovna	k1gFnPc1	čajovna
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
většinou	většinou	k6eAd1	většinou
tichá	tichý	k2eAgNnPc4d1	tiché
místa	místo	k1gNnPc4	místo
s	s	k7c7	s
tlumeným	tlumený	k2eAgNnSc7d1	tlumené
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
nejčastěji	často	k6eAd3	často
meditativní	meditativní	k2eAgFnSc2d1	meditativní
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
New	New	k1gFnSc2	New
Age	Age	k1gFnSc2	Age
nebo	nebo	k8xC	nebo
world	world	k1gMnSc1	world
music	music	k1gMnSc1	music
pijí	pít	k5eAaImIp3nP	pít
nejen	nejen	k6eAd1	nejen
čaje	čaj	k1gInPc4	čaj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
jihoamerické	jihoamerický	k2eAgNnSc1d1	jihoamerické
maté	maté	k1gNnSc1	maté
nebo	nebo	k8xC	nebo
africký	africký	k2eAgInSc1d1	africký
rooibos	rooibos	k1gInSc1	rooibos
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
čajovny	čajovna	k1gFnPc1	čajovna
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
vždy	vždy	k6eAd1	vždy
navodit	navodit	k5eAaPmF	navodit
pocit	pocit	k1gInSc4	pocit
intimního	intimní	k2eAgNnSc2d1	intimní
a	a	k8xC	a
zabydleného	zabydlený	k2eAgNnSc2d1	zabydlené
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
pro	pro	k7c4	pro
návštěvníky	návštěvník	k1gMnPc4	návštěvník
"	"	kIx"	"
<g/>
obývák	obývák	k1gInSc1	obývák
někde	někde	k6eAd1	někde
jinde	jinde	k6eAd1	jinde
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
<g/>
.	.	kIx.	.
</s>
<s>
Odráží	odrážet	k5eAaImIp3nS	odrážet
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
kupříkladu	kupříkladu	k6eAd1	kupříkladu
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yQgInSc7	jaký
je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
osvětlení	osvětlení	k1gNnSc1	osvětlení
–	–	k?	–
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
velice	velice	k6eAd1	velice
jemné	jemný	k2eAgFnPc1d1	jemná
nebo	nebo	k8xC	nebo
tlumené	tlumený	k2eAgFnPc1d1	tlumená
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
i	i	k9	i
s	s	k7c7	s
vnějším	vnější	k2eAgNnSc7d1	vnější
světlem	světlo	k1gNnSc7	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k4c1	málo
čajoven	čajovna	k1gFnPc2	čajovna
má	mít	k5eAaImIp3nS	mít
velká	velký	k2eAgNnPc4d1	velké
prosklená	prosklený	k2eAgNnPc4d1	prosklené
okna	okno	k1gNnPc4	okno
bez	bez	k7c2	bez
záclon	záclona	k1gFnPc2	záclona
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
čajovnách	čajovna	k1gFnPc6	čajovna
se	se	k3xPyFc4	se
často	často	k6eAd1	často
podávají	podávat	k5eAaImIp3nP	podávat
malá	malý	k2eAgNnPc4d1	malé
jídla	jídlo	k1gNnPc4	jídlo
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nekuřácké	kuřácký	k2eNgInPc4d1	nekuřácký
prostory	prostor	k1gInPc4	prostor
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vodních	vodní	k2eAgFnPc2d1	vodní
dýmek	dýmka	k1gFnPc2	dýmka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
čajoven	čajovna	k1gFnPc2	čajovna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
koncentrace	koncentrace	k1gFnSc1	koncentrace
tak	tak	k9	tak
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
se	se	k3xPyFc4	se
v	v	k7c6	v
čajovnách	čajovna	k1gFnPc6	čajovna
odjakživa	odjakživa	k6eAd1	odjakživa
odehrával	odehrávat	k5eAaImAgInS	odehrávat
veškerý	veškerý	k3xTgInSc4	veškerý
společenský	společenský	k2eAgInSc4d1	společenský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Čajovna	čajovna	k1gFnSc1	čajovna
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
jakýmsi	jakýsi	k3yIgNnSc7	jakýsi
zrcadlem	zrcadlo	k1gNnSc7	zrcadlo
čínské	čínský	k2eAgFnSc2d1	čínská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
čínských	čínský	k2eAgFnPc6d1	čínská
čajovnách	čajovna	k1gFnPc6	čajovna
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
čajových	čajový	k2eAgFnPc6d1	čajová
teráskách	teráska	k1gFnPc6	teráska
(	(	kIx(	(
<g/>
čajovny	čajovna	k1gFnPc1	čajovna
nebyly	být	k5eNaImAgFnP	být
situovány	situovat	k5eAaBmNgFnP	situovat
do	do	k7c2	do
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
místností	místnost	k1gFnPc2	místnost
nebo	nebo	k8xC	nebo
sklepů	sklep	k1gInPc2	sklep
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bychom	by	kYmCp1nP	by
marně	marně	k6eAd1	marně
hledali	hledat	k5eAaImAgMnP	hledat
oázu	oáza	k1gFnSc4	oáza
klidu	klid	k1gInSc2	klid
pro	pro	k7c4	pro
meditaci	meditace	k1gFnSc4	meditace
<g/>
.	.	kIx.	.
</s>
<s>
Hromadně	hromadně	k6eAd1	hromadně
a	a	k8xC	a
hlasitě	hlasitě	k6eAd1	hlasitě
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
debatovalo	debatovat	k5eAaImAgNnS	debatovat
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgNnPc1	jaký
jen	jen	k9	jen
Číňany	Číňan	k1gMnPc4	Číňan
napadla	napadnout	k5eAaPmAgFnS	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
za	za	k7c2	za
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
všech	všecek	k3xTgFnPc2	všecek
veřejných	veřejný	k2eAgFnPc2d1	veřejná
(	(	kIx(	(
<g/>
mnohdy	mnohdy	k6eAd1	mnohdy
i	i	k9	i
soukromých	soukromý	k2eAgMnPc2d1	soukromý
<g/>
)	)	kIx)	)
místech	místo	k1gNnPc6	místo
trestné	trestný	k2eAgNnSc1d1	trestné
mluvit	mluvit	k5eAaImF	mluvit
o	o	k7c6	o
"	"	kIx"	"
<g/>
věcech	věc	k1gFnPc6	věc
státu	stát	k1gInSc2	stát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nesly	nést	k5eAaImAgFnP	nést
čajovnami	čajovna	k1gFnPc7	čajovna
ostré	ostrý	k2eAgFnSc2d1	ostrá
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
centrem	centrum	k1gNnSc7	centrum
konání	konání	k1gNnSc2	konání
různých	různý	k2eAgNnPc2d1	různé
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
slavných	slavný	k2eAgFnPc2d1	slavná
čajových	čajový	k2eAgFnPc2d1	čajová
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
,	,	kIx,	,
utkání	utkání	k1gNnSc1	utkání
řečníků	řečník	k1gMnPc2	řečník
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
však	však	k9	však
začaly	začít	k5eAaPmAgInP	začít
zvyky	zvyk	k1gInPc1	zvyk
ubírat	ubírat	k5eAaImF	ubírat
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
otevřela	otevřít	k5eAaPmAgFnS	otevřít
západním	západní	k2eAgInPc3d1	západní
vlivům	vliv	k1gInPc3	vliv
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
částečný	částečný	k2eAgInSc4d1	částečný
útěk	útěk	k1gInSc4	útěk
od	od	k7c2	od
čajoven	čajovna	k1gFnPc2	čajovna
k	k	k7c3	k
hospůdkám	hospůdka	k1gFnPc3	hospůdka
<g/>
,	,	kIx,	,
kavárnám	kavárna	k1gFnPc3	kavárna
a	a	k8xC	a
hernám	herna	k1gFnPc3	herna
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
čajovny	čajovna	k1gFnPc4	čajovna
centrem	centr	k1gInSc7	centr
veškerého	veškerý	k3xTgNnSc2	veškerý
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
obliba	obliba	k1gFnSc1	obliba
sice	sice	k8xC	sice
oproti	oproti	k7c3	oproti
dřívějším	dřívější	k2eAgInPc3d1	dřívější
časům	čas	k1gInPc3	čas
klesla	klesnout	k5eAaPmAgFnS	klesnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
přece	přece	k9	přece
si	se	k3xPyFc3	se
dodnes	dodnes	k6eAd1	dodnes
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
své	své	k1gNnSc4	své
typické	typický	k2eAgNnSc4d1	typické
kouzlo	kouzlo	k1gNnSc4	kouzlo
a	a	k8xC	a
vytříbené	vytříbený	k2eAgInPc1d1	vytříbený
způsoby	způsob	k1gInPc1	způsob
přípravy	příprava	k1gFnSc2	příprava
čajů	čaj	k1gInPc2	čaj
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čajovna	čajovna	k1gFnSc1	čajovna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čajovna	čajovna	k1gFnSc1	čajovna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
e-cajovna	eajovna	k1gFnSc1	e-cajovna
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
Internetový	internetový	k2eAgInSc4d1	internetový
portál	portál	k1gInSc4	portál
o	o	k7c6	o
čaji	čaj	k1gInSc6	čaj
a	a	k8xC	a
čajovnách	čajovna	k1gFnPc6	čajovna
cajik	cajikum	k1gNnPc2	cajikum
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Databáze	databáze	k1gFnPc1	databáze
čajoven	čajovna	k1gFnPc2	čajovna
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
Slovensku	Slovensko	k1gNnSc6	Slovensko
icajove-more	icajoveor	k1gInSc5	icajove-mor
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Čajový	čajový	k2eAgInSc4d1	čajový
magazín	magazín	k1gInSc4	magazín
s	s	k7c7	s
mapou	mapa	k1gFnSc7	mapa
čajoven	čajovna	k1gFnPc2	čajovna
i	i	k8xC	i
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
čajem	čaj	k1gInSc7	čaj
zen-buddhismus	zenuddhismus	k1gInSc1	zen-buddhismus
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
/	/	kIx~	/
<g/>
cajovny	cajovna	k1gFnSc2	cajovna
–	–	k?	–
Doplňovaný	doplňovaný	k2eAgInSc4d1	doplňovaný
seznam	seznam	k1gInSc4	seznam
čajoven	čajovna	k1gFnPc2	čajovna
</s>
