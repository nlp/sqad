<s>
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
(	(	kIx(	(
<g/>
také	také	k9	také
olomoucké	olomoucký	k2eAgInPc4d1	olomoucký
syrečky	syreček	k1gInPc4	syreček
nebo	nebo	k8xC	nebo
tvargle	tvargle	k1gFnPc4	tvargle
z	z	k7c2	z
něm.	něm.	k?	něm.
Olmützer	Olmützer	k1gInSc1	Olmützer
Quargel	Quargel	k1gInSc1	Quargel
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
moravským	moravský	k2eAgInSc7d1	moravský
zrajícím	zrající	k2eAgInSc7d1	zrající
sýrem	sýr	k1gInSc7	sýr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
odtučněného	odtučněný	k2eAgNnSc2d1	odtučněné
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
