<s>
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
(	(	kIx(	(
<g/>
také	také	k9	také
olomoucké	olomoucký	k2eAgInPc4d1	olomoucký
syrečky	syreček	k1gInPc4	syreček
nebo	nebo	k8xC	nebo
tvargle	tvargle	k1gFnPc4	tvargle
z	z	k7c2	z
něm.	něm.	k?	něm.
Olmützer	Olmützer	k1gInSc1	Olmützer
Quargel	Quargel	k1gInSc1	Quargel
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
moravským	moravský	k2eAgInSc7d1	moravský
zrajícím	zrající	k2eAgInSc7d1	zrající
sýrem	sýr	k1gInSc7	sýr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
odtučněného	odtučněný	k2eAgNnSc2d1	odtučněné
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
soustředila	soustředit	k5eAaPmAgNnP	soustředit
do	do	k7c2	do
regionu	region	k1gInSc2	region
Haná	Haná	k1gFnSc1	Haná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
spojeno	spojen	k2eAgNnSc1d1	spojeno
město	město	k1gNnSc1	město
Loštice	Loštice	k1gFnSc2	Loštice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvarůžky	tvarůžek	k1gInPc4	tvarůžek
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
firma	firma	k1gFnSc1	firma
A.	A.	kA	A.
W.	W.	kA	W.
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
roku	rok	k1gInSc2	rok
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
součástí	součást	k1gFnPc2	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
pak	pak	k6eAd1	pak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
díky	díky	k7c3	díky
trhům	trh	k1gInPc3	trh
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
prodávaly	prodávat	k5eAaImAgInP	prodávat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
se	se	k3xPyFc4	se
však	však	k9	však
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
vesnicích	vesnice	k1gFnPc6	vesnice
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
selské	selský	k2eAgInPc4d1	selský
tvarůžky	tvarůžek	k1gInPc4	tvarůžek
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
tvarůžky	tvarůžek	k1gInPc4	tvarůžek
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
tvarohu	tvaroh	k1gInSc6	tvaroh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
meziproduktem	meziprodukt	k1gInSc7	meziprodukt
výroby	výroba	k1gFnSc2	výroba
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc4d1	dnešní
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
Lošticích	Loštice	k1gFnPc6	Loštice
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Josef	Josef	k1gMnSc1	Josef
Wessels	Wessels	k1gInSc4	Wessels
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Aloisem	Alois	k1gMnSc7	Alois
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
se	se	k3xPyFc4	se
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
až	až	k9	až
do	do	k7c2	do
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
firma	firma	k1gFnSc1	firma
zaměstnávala	zaměstnávat	k5eAaImAgFnS	zaměstnávat
36	[number]	k4	36
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
největším	veliký	k2eAgMnSc7d3	veliký
výrobcem	výrobce	k1gMnSc7	výrobce
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
státu	stát	k1gInSc2	stát
postavena	postaven	k2eAgFnSc1d1	postavena
Mlékařská	mlékařský	k2eAgFnSc1d1	mlékařská
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
odborníky	odborník	k1gMnPc4	odborník
do	do	k7c2	do
mlékáren	mlékárna	k1gFnPc2	mlékárna
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
i	i	k9	i
výzkumným	výzkumný	k2eAgNnSc7d1	výzkumné
pracovištěm	pracoviště	k1gNnSc7	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
pořádaly	pořádat	k5eAaImAgInP	pořádat
i	i	k9	i
kurzy	kurz	k1gInPc1	kurz
pro	pro	k7c4	pro
tvarůžkářky	tvarůžkářka	k1gFnPc4	tvarůžkářka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
"	"	kIx"	"
<g/>
vzorová	vzorový	k2eAgFnSc1d1	vzorová
tvarůžkárna	tvarůžkárna	k1gFnSc1	tvarůžkárna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
výrobna	výrobna	k1gFnSc1	výrobna
znárodněna	znárodnit	k5eAaPmNgFnS	znárodnit
a	a	k8xC	a
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
firma	firma	k1gFnSc1	firma
A.W.	A.W.	k1gFnSc1	A.W.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
výroba	výroba	k1gFnSc1	výroba
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
klesala	klesat	k5eAaImAgFnS	klesat
<g/>
,	,	kIx,	,
do	do	k7c2	do
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
zejména	zejména	k6eAd1	zejména
objektů	objekt	k1gInPc2	objekt
tvarůžkárny	tvarůžkárna	k1gFnSc2	tvarůžkárna
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
neinvestovalo	investovat	k5eNaBmAgNnS	investovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
firma	firma	k1gFnSc1	firma
v	v	k7c6	v
restituci	restituce	k1gFnSc6	restituce
navrácena	navrácen	k2eAgFnSc1d1	navrácena
potomkům	potomek	k1gMnPc3	potomek
původních	původní	k2eAgMnPc2d1	původní
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ihned	ihned	k6eAd1	ihned
zahájili	zahájit	k5eAaPmAgMnP	zahájit
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
tak	tak	k8xC	tak
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
získaly	získat	k5eAaPmAgInP	získat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
chráněné	chráněný	k2eAgNnSc4d1	chráněné
zeměpisné	zeměpisný	k2eAgNnSc4d1	zeměpisné
označení	označení	k1gNnSc4	označení
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
má	můj	k3xOp1gFnSc1	můj
firma	firma	k1gFnSc1	firma
v	v	k7c6	v
Lošticích	Loštice	k1gFnPc6	Loštice
přes	přes	k7c4	přes
130	[number]	k4	130
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
tun	tuna	k1gFnPc2	tuna
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
jsou	být	k5eAaImIp3nP	být
zrajícím	zrající	k2eAgInSc7d1	zrající
sýrem	sýr	k1gInSc7	sýr
vyrobeným	vyrobený	k2eAgInSc7d1	vyrobený
z	z	k7c2	z
odstředěného	odstředěný	k2eAgNnSc2d1	odstředěné
mléka	mléko	k1gNnSc2	mléko
a	a	k8xC	a
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
sám	sám	k3xTgMnSc1	sám
výrobce	výrobce	k1gMnSc1	výrobce
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Tvarůžky	tvarůžek	k1gInPc1	tvarůžek
mají	mít	k5eAaImIp3nP	mít
ojedinělou	ojedinělý	k2eAgFnSc4d1	ojedinělá
pikantní	pikantní	k2eAgFnSc4d1	pikantní
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
typickou	typický	k2eAgFnSc4d1	typická
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
povrch	povrch	k1gInSc4	povrch
se	s	k7c7	s
zlatožlutým	zlatožlutý	k2eAgInSc7d1	zlatožlutý
mazem	maz	k1gInSc7	maz
a	a	k8xC	a
poloměkkou	poloměkký	k2eAgFnSc7d1	poloměkká
až	až	k8xS	až
měkkou	měkký	k2eAgFnSc7d1	měkká
konzistencí	konzistence	k1gFnSc7	konzistence
s	s	k7c7	s
patrným	patrný	k2eAgNnSc7d1	patrné
světlejším	světlý	k2eAgNnSc7d2	světlejší
jádrem	jádro	k1gNnSc7	jádro
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tradičně	tradičně	k6eAd1	tradičně
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
označovaly	označovat	k5eAaImAgFnP	označovat
jako	jako	k9	jako
pleskačky	pleskačka	k1gFnPc1	pleskačka
a	a	k8xC	a
využívaly	využívat	k5eAaPmAgFnP	využívat
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
klapaček	klapačka	k1gFnPc2	klapačka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dávaly	dávat	k5eAaImAgFnP	dávat
tvarůžkům	tvarůžek	k1gInPc3	tvarůžek
tvar	tvar	k1gInSc4	tvar
koleček	kolečko	k1gNnPc2	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
Princip	princip	k1gInSc1	princip
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
,	,	kIx,	,
jen	jen	k9	jen
ruční	ruční	k2eAgFnSc4d1	ruční
práci	práce	k1gFnSc4	práce
z	z	k7c2	z
části	část	k1gFnSc2	část
nahradily	nahradit	k5eAaPmAgInP	nahradit
moderní	moderní	k2eAgInPc1d1	moderní
stroje	stroj	k1gInPc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
výroby	výroba	k1gFnSc2	výroba
je	být	k5eAaImIp3nS	být
netučný	tučný	k2eNgInSc1d1	netučný
tvaroh	tvaroh	k1gInSc1	tvaroh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zraje	zrát	k5eAaImIp3nS	zrát
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
semele	semlít	k5eAaPmIp3nS	semlít
a	a	k8xC	a
vytvaruje	vytvarovat	k5eAaPmIp3nS	vytvarovat
do	do	k7c2	do
koleček	kolečko	k1gNnPc2	kolečko
<g/>
,	,	kIx,	,
věnečků	věneček	k1gInPc2	věneček
či	či	k8xC	či
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
nechávají	nechávat	k5eAaImIp3nP	nechávat
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
zrát	zrát	k5eAaImF	zrát
v	v	k7c6	v
sušárně	sušárna	k1gFnSc6	sušárna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jsou	být	k5eAaImIp3nP	být
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
promyty	promyt	k2eAgInPc1d1	promyt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
omytí	omytí	k1gNnSc3	omytí
kvasinek	kvasinka	k1gFnPc2	kvasinka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
začít	začít	k5eAaPmF	začít
působit	působit	k5eAaImF	působit
směs	směs	k1gFnSc4	směs
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sušících	sušící	k2eAgInPc6d1	sušící
roštech	rošt	k1gInPc6	rošt
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prozrání	prozrání	k1gNnSc3	prozrání
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roštů	rošt	k1gInPc2	rošt
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
sesbírány	sesbírán	k2eAgFnPc1d1	sesbírána
do	do	k7c2	do
beden	bedna	k1gFnPc2	bedna
a	a	k8xC	a
uloženy	uložit	k5eAaPmNgFnP	uložit
do	do	k7c2	do
chladící	chladící	k2eAgFnSc2d1	chladící
místnosti	místnost	k1gFnSc2	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc1	jeden
až	až	k9	až
dva	dva	k4xCgInPc1	dva
dny	den	k1gInPc1	den
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
<g/>
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
olomouckých	olomoucký	k2eAgInPc2d1	olomoucký
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
je	být	k5eAaImIp3nS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
původní	původní	k2eAgFnSc2d1	původní
výrobny	výrobna	k1gFnSc2	výrobna
v	v	k7c6	v
Lošticích	Loštice	k1gFnPc6	Loštice
a	a	k8xC	a
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
výroby	výroba	k1gFnSc2	výroba
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
prošlo	projít	k5eAaPmAgNnS	projít
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lošticích	Loštice	k1gFnPc6	Loštice
sídli	sídlet	k5eAaImRp2nS	sídlet
také	také	k9	také
podniková	podnikový	k2eAgFnSc1d1	podniková
prodejna	prodejna	k1gFnSc1	prodejna
a	a	k8xC	a
tvarůžková	tvarůžkový	k2eAgFnSc1d1	Tvarůžková
cukrárna	cukrárna	k1gFnSc1	cukrárna
<g/>
,	,	kIx,	,
nabízející	nabízející	k2eAgFnSc1d1	nabízející
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
Poštulkovy	Poštulkův	k2eAgInPc4d1	Poštulkův
tvarůžkové	tvarůžkový	k2eAgInPc4d1	tvarůžkový
moučníky	moučník	k1gInPc4	moučník
–	–	k?	–
slané	slaný	k2eAgFnPc4d1	slaná
tvarůžkové	tvarůžkový	k2eAgFnPc4d1	Tvarůžková
kremrole	kremrole	k1gFnPc4	kremrole
<g/>
,	,	kIx,	,
šátečky	šáteček	k1gInPc4	šáteček
či	či	k8xC	či
dortíky	dortík	k1gInPc4	dortík
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
speciality	specialita	k1gFnPc4	specialita
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
tvarůžkový	tvarůžkový	k2eAgInSc1d1	tvarůžkový
langoš	langoš	k1gInSc1	langoš
<g/>
,	,	kIx,	,
hot	hot	k0	hot
dog	doga	k1gFnPc2	doga
či	či	k8xC	či
bramboráček	bramboráček	k1gInSc1	bramboráček
<g/>
.	.	kIx.	.
</s>
