<p>
<s>
Pizza	pizza	k1gFnSc1	pizza
[	[	kIx(	[
<g/>
pica	pica	k1gFnSc1	pica
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
kruhový	kruhový	k2eAgInSc4d1	kruhový
plát	plát	k1gInSc4	plát
světlého	světlý	k2eAgNnSc2d1	světlé
chlebového	chlebový	k2eAgNnSc2d1	chlebové
těsta	těsto	k1gNnSc2	těsto
(	(	kIx(	(
<g/>
koláč	koláč	k1gInSc1	koláč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nahoře	nahoře	k6eAd1	nahoře
s	s	k7c7	s
náplní	náplň	k1gFnSc7	náplň
vyrobenou	vyrobený	k2eAgFnSc4d1	vyrobená
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
rajčat	rajče	k1gNnPc2	rajče
<g/>
,	,	kIx,	,
sýra	sýr	k1gInSc2	sýr
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
rozdílných	rozdílný	k2eAgFnPc2d1	rozdílná
ingrediencí	ingredience	k1gFnPc2	ingredience
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
pizzy	pizza	k1gFnSc2	pizza
<g/>
.	.	kIx.	.
</s>
<s>
Peče	péct	k5eAaImIp3nS	péct
se	se	k3xPyFc4	se
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
nebo	nebo	k8xC	nebo
troubě	trouba	k1gFnSc6	trouba
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
jídlem	jídlo	k1gNnSc7	jídlo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
se	se	k3xPyFc4	se
pizza	pizza	k1gFnSc1	pizza
připravuje	připravovat	k5eAaImIp3nS	připravovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pizzerie	pizzerie	k1gFnPc1	pizzerie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
pečením	pečení	k1gNnSc7	pečení
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
základních	základní	k2eAgFnPc2d1	základní
surovin	surovina	k1gFnPc2	surovina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ze	z	k7c2	z
středně	středně	k6eAd1	středně
tuhého	tuhý	k2eAgNnSc2d1	tuhé
těsta	těsto	k1gNnSc2	těsto
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
droždí	droždí	k1gNnSc2	droždí
</s>
</p>
<p>
<s>
rajčatového	rajčatový	k2eAgInSc2d1	rajčatový
základu	základ	k1gInSc2	základ
(	(	kIx(	(
<g/>
sugo	sugo	k6eAd1	sugo
di	di	k?	di
pomodoro	pomodora	k1gFnSc5	pomodora
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
mozzarellyDále	mozzarellyDále	k6eAd1	mozzarellyDále
lze	lze	k6eAd1	lze
přidat	přidat	k5eAaPmF	přidat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
suroviny	surovina	k1gFnPc4	surovina
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
pizzy	pizza	k1gFnSc2	pizza
<g/>
,	,	kIx,	,
např.	např.	kA	např.
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
sardinky	sardinka	k1gFnPc1	sardinka
<g/>
,	,	kIx,	,
žampiony	žampion	k1gInPc1	žampion
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnPc1	cibule
<g/>
,	,	kIx,	,
salám	salám	k1gInSc1	salám
<g/>
,	,	kIx,	,
ančovičky	ančovička	k1gFnPc1	ančovička
<g/>
,	,	kIx,	,
ananas	ananas	k1gInSc1	ananas
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
národních	národní	k2eAgFnPc6d1	národní
zvyklostech	zvyklost	k1gFnPc6	zvyklost
toho	ten	k3xDgInSc2	ten
kterého	který	k3yQgInSc2	který
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
v	v	k7c6	v
receptech	recept	k1gInPc6	recept
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
náhrady	náhrada	k1gFnPc1	náhrada
a	a	k8xC	a
obměny	obměna	k1gFnPc1	obměna
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kategorie	kategorie	k1gFnPc1	kategorie
==	==	k?	==
</s>
</p>
<p>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
pizza	pizza	k1gFnSc1	pizza
–	–	k?	–
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
pizzeriích	pizzerie	k1gFnPc6	pizzerie
</s>
</p>
<p>
<s>
Neapolská	neapolský	k2eAgFnSc1d1	neapolská
pizza	pizza	k1gFnSc1	pizza
–	–	k?	–
označována	označovat	k5eAaImNgFnS	označovat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
jako	jako	k8xC	jako
královna	královna	k1gFnSc1	královna
všech	všecek	k3xTgFnPc2	všecek
pizz	pizza	k1gFnPc2	pizza
</s>
</p>
<p>
<s>
Celozrnná	celozrnný	k2eAgFnSc1d1	celozrnná
pizza	pizza	k1gFnSc1	pizza
–	–	k?	–
těsto	těsto	k1gNnSc1	těsto
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
celozrnné	celozrnný	k2eAgFnSc2d1	celozrnná
mouky	mouka	k1gFnSc2	mouka
</s>
</p>
<p>
<s>
Bezlepková	bezlepkový	k2eAgFnSc1d1	bezlepková
pizza	pizza	k1gFnSc1	pizza
–	–	k?	–
těsto	těsto	k1gNnSc1	těsto
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
lepek	lepek	k1gInSc4	lepek
</s>
</p>
<p>
<s>
Raw	Raw	k?	Raw
pizza	pizza	k1gFnSc1	pizza
–	–	k?	–
těsto	těsto	k1gNnSc1	těsto
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
semínek	semínko	k1gNnPc2	semínko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pohanka	pohanka	k1gFnSc1	pohanka
<g/>
)	)	kIx)	)
a	a	k8xC	a
suší	sušit	k5eAaImIp3nS	sušit
se	se	k3xPyFc4	se
při	při	k7c6	při
40	[number]	k4	40
<g/>
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
Pizza	pizza	k1gFnSc1	pizza
pane	pan	k1gMnSc5	pan
–	–	k?	–
nezdobené	zdobený	k2eNgNnSc4d1	nezdobené
<g/>
,	,	kIx,	,
upečené	upečený	k2eAgNnSc4d1	upečené
těsto	těsto	k1gNnSc4	těsto
na	na	k7c4	na
pizzu	pizza	k1gFnSc4	pizza
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
jako	jako	k8xS	jako
přílohaPizzu	přílohaPizz	k1gInSc2	přílohaPizz
je	být	k5eAaImIp3nS	být
také	také	k9	také
možno	možno	k6eAd1	možno
zakoupit	zakoupit	k5eAaPmF	zakoupit
i	i	k9	i
předpečenou	předpečený	k2eAgFnSc4d1	předpečená
a	a	k8xC	a
zmraženou	zmražený	k2eAgFnSc4d1	zmražená
a	a	k8xC	a
poté	poté	k6eAd1	poté
ji	on	k3xPp3gFnSc4	on
rozmrazit	rozmrazit	k5eAaPmF	rozmrazit
a	a	k8xC	a
dopéct	dopéct	k5eAaPmF	dopéct
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
podoba	podoba	k1gFnSc1	podoba
pizzy	pizza	k1gFnSc2	pizza
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Italské	italský	k2eAgFnSc2d1	italská
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
její	její	k3xOp3gMnPc1	její
předchůdci	předchůdce	k1gMnPc1	předchůdce
pochází	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
(	(	kIx(	(
<g/>
hlavními	hlavní	k2eAgFnPc7d1	hlavní
ingrediencemi	ingredience	k1gFnPc7	ingredience
byly	být	k5eAaImAgInP	být
sýr	sýr	k1gInSc4	sýr
a	a	k8xC	a
olivy	oliva	k1gFnPc4	oliva
<g/>
)	)	kIx)	)
a	a	k8xC	a
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pizza	pizza	k1gFnSc1	pizza
freestyle	freestyl	k1gInSc5	freestyl
==	==	k?	==
</s>
</p>
<p>
<s>
Pizza	pizza	k1gFnSc1	pizza
freestyle	freestyl	k1gInSc5	freestyl
(	(	kIx(	(
<g/>
též	též	k9	též
pizza	pizza	k1gFnSc1	pizza
akrobacie	akrobacie	k1gFnSc1	akrobacie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgNnSc1d1	sportovní
odvětví	odvětví	k1gNnSc1	odvětví
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
hází	házet	k5eAaImIp3nS	házet
s	s	k7c7	s
těstem	těsto	k1gNnSc7	těsto
na	na	k7c4	na
pizzu	pizza	k1gFnSc4	pizza
nebo	nebo	k8xC	nebo
s	s	k7c7	s
gumovým	gumový	k2eAgInSc7d1	gumový
diskem	disk	k1gInSc7	disk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
pizza	pizza	k1gFnSc1	pizza
těsto	těsto	k1gNnSc4	těsto
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tzv.	tzv.	kA	tzv.
pizza	pizza	k1gFnSc1	pizza
show	show	k1gFnSc1	show
se	se	k3xPyFc4	se
předvádí	předvádět	k5eAaImIp3nS	předvádět
do	do	k7c2	do
rytmu	rytmus	k1gInSc2	rytmus
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Pizzař	Pizzař	k1gMnSc1	Pizzař
nechává	nechávat	k5eAaImIp3nS	nechávat
pizza	pizza	k1gFnSc1	pizza
těsto	těsto	k1gNnSc4	těsto
lítat	lítat	k5eAaImF	lítat
a	a	k8xC	a
točit	točit	k5eAaImF	točit
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
těsto	těsto	k1gNnSc1	těsto
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
svůj	svůj	k3xOyFgInSc4	svůj
průměr	průměr	k1gInSc4	průměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnPc1	metoda
pečení	pečení	k1gNnSc2	pečení
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
pizzy	pizza	k1gFnSc2	pizza
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
několik	několik	k4yIc1	několik
druhů	druh	k1gInPc2	druh
pecí	pec	k1gFnPc2	pec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pec	Pec	k1gFnSc1	Pec
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
–	–	k?	–
Pizza	pizza	k1gFnSc1	pizza
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
se	se	k3xPyFc4	se
peče	péct	k5eAaImIp3nS	péct
okolo	okolo	k7c2	okolo
tří	tři	k4xCgFnPc2	tři
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
pec	pec	k1gFnSc1	pec
se	se	k3xPyFc4	se
vytápí	vytápět	k5eAaImIp3nS	vytápět
na	na	k7c4	na
380-500	[number]	k4	380-500
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pec	Pec	k1gFnSc1	Pec
elektrická	elektrický	k2eAgFnSc1d1	elektrická
–	–	k?	–
Profesionální	profesionální	k2eAgFnSc2d1	profesionální
elektrické	elektrický	k2eAgFnSc2d1	elektrická
pece	pec	k1gFnSc2	pec
lze	lze	k6eAd1	lze
také	také	k9	také
vytopit	vytopit	k5eAaPmF	vytopit
přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
450	[number]	k4	450
°	°	k?	°
<g/>
C.	C.	kA	C.
Nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
těchto	tento	k3xDgFnPc2	tento
pecí	pec	k1gFnPc2	pec
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
ztrátě	ztráta	k1gFnSc6	ztráta
teploty	teplota	k1gFnSc2	teplota
při	při	k7c6	při
každém	každý	k3xTgNnSc6	každý
otevření	otevření	k1gNnSc6	otevření
dveří	dveře	k1gFnPc2	dveře
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pec	Pec	k1gFnSc1	Pec
plynová	plynový	k2eAgFnSc1d1	plynová
–	–	k?	–
(	(	kIx(	(
<g/>
Plyn	plyn	k1gInSc1	plyn
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
tryskami	tryska	k1gFnPc7	tryska
uvnitř	uvnitř	k7c2	uvnitř
oválné	oválný	k2eAgFnSc2d1	oválná
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Plynová	plynový	k2eAgFnSc1d1	plynová
pec	pec	k1gFnSc1	pec
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xS	jako
pec	pec	k1gFnSc4	pec
na	na	k7c4	na
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
pizzy	pizza	k1gFnSc2	pizza
==	==	k?	==
</s>
</p>
<p>
<s>
tradiční	tradiční	k2eAgFnPc1d1	tradiční
italské	italský	k2eAgFnPc1d1	italská
pizzyMargherita	pizzyMargherita	k1gFnSc1	pizzyMargherita
–	–	k?	–
rajčatový	rajčatový	k2eAgInSc1d1	rajčatový
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
mozzarella	mozzarella	k1gFnSc1	mozzarella
<g/>
,	,	kIx,	,
bazalka	bazalka	k1gFnSc1	bazalka
<g/>
,	,	kIx,	,
olivový	olivový	k2eAgInSc1d1	olivový
olej	olej	k1gInSc1	olej
</s>
</p>
<p>
<s>
Prosciutto	Prosciutto	k1gNnSc1	Prosciutto
-	-	kIx~	-
rajčatový	rajčatový	k2eAgInSc1d1	rajčatový
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
mozzarella	mozzarella	k1gFnSc1	mozzarella
<g/>
,	,	kIx,	,
šunka	šunka	k1gFnSc1	šunka
</s>
</p>
<p>
<s>
Prosciutto	Prosciutto	k1gNnSc1	Prosciutto
e	e	k0	e
Funghi	Fungh	k1gInSc3	Fungh
-	-	kIx~	-
rajčatový	rajčatový	k2eAgInSc1d1	rajčatový
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
mozzarella	mozzarella	k1gFnSc1	mozzarella
<g/>
,	,	kIx,	,
šunka	šunka	k1gFnSc1	šunka
<g/>
,	,	kIx,	,
žampióny	žampión	k1gInPc1	žampión
</s>
</p>
<p>
<s>
Capricciosa	Capricciosa	k1gFnSc1	Capricciosa
–	–	k?	–
rajčatový	rajčatový	k2eAgInSc1d1	rajčatový
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
mozzarella	mozzarella	k1gFnSc1	mozzarella
<g/>
,	,	kIx,	,
šunka	šunka	k1gFnSc1	šunka
<g/>
,	,	kIx,	,
žampióny	žampión	k1gInPc1	žampión
<g/>
,	,	kIx,	,
artyčoky	artyčok	k1gInPc1	artyčok
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
</s>
</p>
<p>
<s>
Marinara	Marinara	k1gFnSc1	Marinara
–	–	k?	–
rajčatový	rajčatový	k2eAgInSc1d1	rajčatový
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
česnek	česnek	k1gInSc1	česnek
nakrájený	nakrájený	k2eAgInSc1d1	nakrájený
na	na	k7c4	na
plátky	plátek	k1gInPc4	plátek
<g/>
,	,	kIx,	,
extra	extra	k6eAd1	extra
panenský	panenský	k2eAgInSc4d1	panenský
(	(	kIx(	(
<g/>
virgin	virgin	k2eAgInSc4d1	virgin
<g/>
)	)	kIx)	)
olivový	olivový	k2eAgInSc4d1	olivový
olej	olej	k1gInSc4	olej
<g/>
,	,	kIx,	,
bazalka	bazalka	k1gFnSc1	bazalka
<g/>
,	,	kIx,	,
mořská	mořský	k2eAgFnSc1d1	mořská
sůl	sůl	k1gFnSc1	sůl
</s>
</p>
<p>
<s>
Quattro	Quattro	k1gNnSc1	Quattro
Formaggi	Formagg	k1gFnSc2	Formagg
–	–	k?	–
pizza	pizza	k1gFnSc1	pizza
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
druhy	druh	k1gInPc7	druh
sýrů	sýr	k1gInPc2	sýr
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
jako	jako	k9	jako
kombinace	kombinace	k1gFnSc1	kombinace
výraznějších	výrazný	k2eAgInPc2d2	výraznější
tvrdších	tvrdý	k2eAgInPc2d2	tvrdší
a	a	k8xC	a
čerstvých	čerstvý	k2eAgInPc2d1	čerstvý
krémových	krémový	k2eAgInPc2d1	krémový
sýrů	sýr	k1gInPc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
mozzarella	mozzarella	k1gFnSc1	mozzarella
<g/>
,	,	kIx,	,
gorgonzola	gorgonzola	k1gFnSc1	gorgonzola
<g/>
,	,	kIx,	,
parmezán	parmezán	k1gInSc1	parmezán
a	a	k8xC	a
asiago	asiago	k1gNnSc1	asiago
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Quattro	Quattro	k6eAd1	Quattro
Stagioni	Stagion	k1gMnPc1	Stagion
–	–	k?	–
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Margherita	Margherita	k1gFnSc1	Margherita
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Prosciutto	Prosciutto	k1gNnSc1	Prosciutto
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Salámová	salámový	k2eAgFnSc1d1	salámová
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
Quattro	Quattro	k1gNnSc4	Quattro
Formagi	Formagi	k1gNnSc2	Formagi
</s>
</p>
<p>
<s>
Tonno	Tonno	k6eAd1	Tonno
–	–	k?	–
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
tuňák	tuňák	k1gMnSc1	tuňák
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnSc1	cibule
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
olejnetradiční	olejnetradiční	k2eAgInSc1d1	olejnetradiční
pizzyHavaj	pizzyHavaj	k1gInSc1	pizzyHavaj
–	–	k?	–
pizza	pizza	k1gFnSc1	pizza
se	s	k7c7	s
šunkou	šunka	k1gFnSc7	šunka
a	a	k8xC	a
ananasem	ananas	k1gInSc7	ananas
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jídlo	jídlo	k1gNnSc1	jídlo
obdobné	obdobný	k2eAgFnSc3d1	obdobná
dnešní	dnešní	k2eAgFnSc3d1	dnešní
pizze	pizza	k1gFnSc3	pizza
bylo	být	k5eAaImAgNnS	být
připravováno	připravovat	k5eAaImNgNnS	připravovat
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
koláče	koláč	k1gInPc1	koláč
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
pizzy	pizza	k1gFnSc2	pizza
připravovali	připravovat	k5eAaImAgMnP	připravovat
již	již	k6eAd1	již
starověcí	starověký	k2eAgMnPc1d1	starověký
egypťané	egypťan	k1gMnPc1	egypťan
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
plochých	plochý	k2eAgInPc2d1	plochý
koláčů	koláč	k1gInPc2	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
časem	časem	k6eAd1	časem
převzali	převzít	k5eAaPmAgMnP	převzít
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
pizza	pizza	k1gFnSc1	pizza
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
picea	piceum	k1gNnSc2	piceum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
neapolského	neapolský	k2eAgInSc2d1	neapolský
dialektu	dialekt	k1gInSc2	dialekt
italštiny	italština	k1gFnSc2	italština
<g/>
.	.	kIx.	.
</s>
<s>
Označovaly	označovat	k5eAaImAgFnP	označovat
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
pečené	pečený	k2eAgInPc4d1	pečený
placky	placek	k1gInPc4	placek
z	z	k7c2	z
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
kladly	klást	k5eAaImAgFnP	klást
další	další	k2eAgFnPc1d1	další
ingredience	ingredience	k1gFnPc1	ingredience
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikaly	vznikat	k5eAaImAgInP	vznikat
první	první	k4xOgFnPc4	první
pizzerie	pizzerie	k1gFnPc4	pizzerie
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
rozvoji	rozvoj	k1gInSc3	rozvoj
užívání	užívání	k1gNnSc2	užívání
rajčat	rajče	k1gNnPc2	rajče
v	v	k7c6	v
Italské	italský	k2eAgFnSc6d1	italská
gastronomii	gastronomie	k1gFnSc6	gastronomie
se	se	k3xPyFc4	se
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
začala	začít	k5eAaPmAgFnS	začít
pizza	pizza	k1gFnSc1	pizza
formovat	formovat	k5eAaImF	formovat
do	do	k7c2	do
moderní	moderní	k2eAgFnSc2d1	moderní
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
známe	znát	k5eAaImIp1nP	znát
teď	teď	k6eAd1	teď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
pizz	pizza	k1gFnPc2	pizza
-	-	kIx~	-
pizza	pizza	k1gFnSc1	pizza
Margherita	Margherita	k1gFnSc1	Margherita
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
v	v	k7c6	v
Neapoli	Neapol	k1gFnSc6	Neapol
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
předtím	předtím	k6eAd1	předtím
sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
Itálie	Itálie	k1gFnSc1	Itálie
chtěla	chtít	k5eAaImAgFnS	chtít
uctít	uctít	k5eAaPmF	uctít
královnu	královna	k1gFnSc4	královna
Margheritu	Margherit	k1gInSc2	Margherit
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ji	on	k3xPp3gFnSc4	on
navštívila	navštívit	k5eAaPmAgFnS	navštívit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
místní	místní	k2eAgMnSc1d1	místní
kuchař	kuchař	k1gMnSc1	kuchař
Rafael	Rafael	k1gMnSc1	Rafael
Esposito	Esposita	k1gFnSc5	Esposita
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
pizzu	pizza	k1gFnSc4	pizza
v	v	k7c6	v
italských	italský	k2eAgFnPc6d1	italská
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
červené	červený	k2eAgNnSc4d1	červené
(	(	kIx(	(
<g/>
rajče	rajče	k1gNnSc4	rajče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zelené	zelené	k1gNnSc1	zelené
(	(	kIx(	(
<g/>
bazalka	bazalka	k1gFnSc1	bazalka
<g/>
)	)	kIx)	)
a	a	k8xC	a
bílé	bílý	k2eAgFnPc1d1	bílá
(	(	kIx(	(
<g/>
sýr	sýr	k1gInSc1	sýr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
další	další	k2eAgInSc4d1	další
rozvoj	rozvoj	k1gInSc4	rozvoj
pizzy	pizza	k1gFnSc2	pizza
se	se	k3xPyFc4	se
postarali	postarat	k5eAaPmAgMnP	postarat
italští	italský	k2eAgMnPc1d1	italský
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
své	svůj	k3xOyFgFnSc2	svůj
technologie	technologie	k1gFnSc2	technologie
přinesli	přinést	k5eAaPmAgMnP	přinést
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Pizzu	pizza	k1gFnSc4	pizza
Margherita	Margherita	k1gFnSc1	Margherita
pekli	péct	k5eAaImAgMnP	péct
s	s	k7c7	s
mozarellou	mozarella	k1gMnSc7	mozarella
a	a	k8xC	a
oregánem	oregán	k1gMnSc7	oregán
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
ji	on	k3xPp3gFnSc4	on
začali	začít	k5eAaPmAgMnP	začít
připravovat	připravovat	k5eAaImF	připravovat
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
pánvi	pánev	k1gFnSc6	pánev
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
forma	forma	k1gFnSc1	forma
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
přizpůsobila	přizpůsobit	k5eAaPmAgNnP	přizpůsobit
americkému	americký	k2eAgMnSc3d1	americký
fast	fast	k2eAgInSc4d1	fast
food	food	k1gInSc4	food
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
americká	americký	k2eAgFnSc1d1	americká
pizza	pizza	k1gFnSc1	pizza
měla	mít	k5eAaImAgFnS	mít
název	název	k1gInSc4	název
Chicago-Style	Chicago-Styl	k1gInSc5	Chicago-Styl
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
prodávat	prodávat	k5eAaImF	prodávat
první	první	k4xOgFnPc1	první
předpřipravené	předpřipravený	k2eAgFnPc1d1	předpřipravená
pizza-těsto	pizzaěsto	k6eAd1	pizza-těsto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
až	až	k9	až
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
stala	stát	k5eAaPmAgFnS	stát
pizza	pizza	k1gFnSc1	pizza
standardním	standardní	k2eAgNnSc7d1	standardní
fast	fast	k2eAgInSc1d1	fast
food	food	k6eAd1	food
jídlem	jídlo	k1gNnSc7	jídlo
vedle	vedle	k7c2	vedle
hot	hot	k0	hot
dogu	doga	k1gFnSc4	doga
a	a	k8xC	a
jablečného	jablečný	k2eAgInSc2d1	jablečný
koláče	koláč	k1gInSc2	koláč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
přinesli	přinést	k5eAaPmAgMnP	přinést
bratři	bratr	k1gMnPc1	bratr
Celentanovi	Celentanův	k2eAgMnPc1d1	Celentanův
na	na	k7c4	na
trh	trh	k1gInSc4	trh
první	první	k4xOgFnSc4	první
zmrazenou	zmrazený	k2eAgFnSc4d1	zmrazená
pizzu	pizza	k1gFnSc4	pizza
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
napomohli	napomoct	k5eAaPmAgMnP	napomoct
masivnímu	masivní	k2eAgNnSc3d1	masivní
tažení	tažení	k1gNnSc3	tažení
mražených	mražený	k2eAgInPc2d1	mražený
polotovarů	polotovar	k1gInPc2	polotovar
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
se	se	k3xPyFc4	se
i	i	k9	i
její	její	k3xOp3gInSc1	její
sortiment	sortiment	k1gInSc1	sortiment
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
první	první	k4xOgInSc1	první
spotřebitelský	spotřebitelský	k2eAgInSc1d1	spotřebitelský
test	test	k1gInSc1	test
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
pizzy	pizza	k1gFnSc2	pizza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Certifikace	certifikace	k1gFnPc4	certifikace
Neapolské	neapolský	k2eAgFnSc2d1	neapolská
pizzy	pizza	k1gFnSc2	pizza
==	==	k?	==
</s>
</p>
<p>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
Neapolská	neapolský	k2eAgFnSc1d1	neapolská
pizza	pizza	k1gFnSc1	pizza
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zapsána	zapsat	k5eAaPmNgFnS	zapsat
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
do	do	k7c2	do
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
skupiny	skupina	k1gFnSc2	skupina
pokrmů	pokrm	k1gInPc2	pokrm
STG	STG	kA	STG
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zkratku	zkratka	k1gFnSc4	zkratka
"	"	kIx"	"
<g/>
specialità	specialità	k?	specialità
tradizionale	tradizionale	k6eAd1	tradizionale
garantita	garantit	k2eAgFnSc1d1	garantit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
zaručená	zaručený	k2eAgFnSc1d1	zaručená
tradiční	tradiční	k2eAgFnSc1d1	tradiční
specialita	specialita	k1gFnSc1	specialita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Recept	recept	k1gInSc1	recept
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
tradiční	tradiční	k2eAgFnSc4d1	tradiční
Neapolskou	neapolský	k2eAgFnSc4d1	neapolská
pizzu	pizza	k1gFnSc4	pizza
je	být	k5eAaImIp3nS	být
regulován	regulovat	k5eAaImNgInS	regulovat
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
jaký	jaký	k3yRgInSc4	jaký
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
pizza	pizza	k1gFnSc1	pizza
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
připravováno	připravován	k2eAgNnSc4d1	připravováno
těsto	těsto	k1gNnSc4	těsto
a	a	k8xC	a
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pravá	pravý	k2eAgFnSc1d1	pravá
Neapolská	neapolský	k2eAgFnSc1d1	neapolská
pizza	pizza	k1gFnSc1	pizza
konzumována	konzumován	k2eAgFnSc1d1	konzumována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pizza	pizza	k1gFnSc1	pizza
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Pizza	pizza	k1gFnSc1	pizza
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pizza	pizza	k1gFnSc1	pizza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
https://web.archive.org/web/20100505083503/http://www.spinningdough.cz/cs/uvodni-stranka/Recepty	[url]	k1gFnPc1	https://web.archive.org/web/20100505083503/http://www.spinningdough.cz/cs/uvodni-stranka/Recepty
</s>
</p>
<p>
<s>
Recepty	recept	k1gInPc1	recept
na	na	k7c4	na
pizzu	pizza	k1gFnSc4	pizza
</s>
</p>
<p>
<s>
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
pizze	pizza	k1gFnSc6	pizza
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
</s>
</p>
<p>
<s>
Katalog	katalog	k1gInSc1	katalog
pizzerií	pizzerie	k1gFnPc2	pizzerie
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
rozvozu	rozvoz	k1gInSc2	rozvoz
a	a	k8xC	a
nonstop	nonstop	k2eAgFnSc2d1	nonstop
objednávky	objednávka	k1gFnSc2	objednávka
</s>
</p>
