<p>
<s>
Peterhouse	Peterhouse	k1gFnSc1	Peterhouse
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
kolejí	kolej	k1gFnSc7	kolej
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1284	[number]	k4	1284
ji	on	k3xPp3gFnSc4	on
založil	založit	k5eAaPmAgMnS	založit
Hugo	Hugo	k1gMnSc1	Hugo
de	de	k?	de
Balsham	Balsham	k1gInSc1	Balsham
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
z	z	k7c2	z
Ely	Ela	k1gFnSc2	Ela
<g/>
.	.	kIx.	.
</s>
<s>
Počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
také	také	k9	také
o	o	k7c4	o
nejmenší	malý	k2eAgFnSc4d3	nejmenší
kolej	kolej	k1gFnSc4	kolej
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Peterhouse	Peterhouse	k1gFnSc1	Peterhouse
může	moct	k5eAaImIp3nS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
pěti	pět	k4xCc2	pět
nositeli	nositel	k1gMnSc3	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
kterými	který	k3yIgMnPc7	který
jsou	být	k5eAaImIp3nP	být
Sir	sir	k1gMnSc1	sir
John	John	k1gMnSc1	John
Kendrew	Kendrew	k1gMnSc1	Kendrew
<g/>
,	,	kIx,	,
Sir	sir	k1gMnSc1	sir
Aaron	Aaron	k1gMnSc1	Aaron
Klug	Kluga	k1gFnPc2	Kluga
<g/>
,	,	kIx,	,
Archer	Archra	k1gFnPc2	Archra
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Perutz	Perutz	k1gMnSc1	Perutz
a	a	k8xC	a
Michael	Michael	k1gMnSc1	Michael
Levitt	Levitt	k1gMnSc1	Levitt
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
osobností	osobnost	k1gFnSc7	osobnost
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
Peterhousem	Peterhous	k1gInSc7	Peterhous
ale	ale	k8xC	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
Lord	lord	k1gMnSc1	lord
Kelvin	kelvin	k1gInSc1	kelvin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
kolej	kolej	k1gFnSc1	kolej
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
vybavil	vybavit	k5eAaPmAgInS	vybavit
elektrickým	elektrický	k2eAgNnSc7d1	elektrické
osvětlením	osvětlení	k1gNnSc7	osvětlení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
