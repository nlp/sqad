<p>
<s>
Aeroflot	aeroflot	k1gInSc1	aeroflot
(	(	kIx(	(
<g/>
v	v	k7c6	v
ruštině	ruština	k1gFnSc6	ruština
<g/>
:	:	kIx,	:
О	О	k?	О
"	"	kIx"	"
<g/>
А	А	k?	А
–	–	k?	–
Р	Р	k?	Р
а	а	k?	а
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
ruská	ruský	k2eAgFnSc1d1	ruská
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
leteckou	letecký	k2eAgFnSc7d1	letecká
základnou	základna	k1gFnSc7	základna
na	na	k7c6	na
moskevském	moskevský	k2eAgNnSc6d1	moskevské
letišti	letiště	k1gNnSc6	letiště
Šeremeťjevo	Šeremeťjevo	k1gNnSc1	Šeremeťjevo
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
51	[number]	k4	51
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
ruský	ruský	k2eAgInSc1d1	ruský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
měla	mít	k5eAaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
přes	přes	k7c4	přes
30	[number]	k4	30
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Dceřinými	dceřin	k2eAgFnPc7d1	dceřina
leteckými	letecký	k2eAgFnPc7d1	letecká
společnostmi	společnost	k1gFnPc7	společnost
Aeroflotu	aeroflot	k1gInSc2	aeroflot
jsou	být	k5eAaImIp3nP	být
nízkonákladová	nízkonákladový	k2eAgNnPc1d1	nízkonákladové
Pobeda	Pobeda	k1gMnSc1	Pobeda
a	a	k8xC	a
Rossiya	Rossiya	k1gMnSc1	Rossiya
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgInSc7d1	současný
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
ředitelem	ředitel	k1gMnSc7	ředitel
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
Vitalij	Vitalij	k1gMnSc1	Vitalij
Saveljev	Saveljev	k1gMnSc1	Saveljev
<g/>
.	.	kIx.	.
</s>
<s>
Nahradil	nahradit	k5eAaPmAgMnS	nahradit
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
svého	svůj	k3xOyFgMnSc4	svůj
předchůdce	předchůdce	k1gMnSc4	předchůdce
Valerie	Valerie	k1gFnSc2	Valerie
Okulova	Okulův	k2eAgInSc2d1	Okulův
který	který	k3yQgMnSc1	který
funkci	funkce	k1gFnSc4	funkce
zastával	zastávat	k5eAaImAgMnS	zastávat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
SSSR	SSSR	kA	SSSR
byl	být	k5eAaImAgMnS	být
Aeroflot	aeroflot	k1gInSc4	aeroflot
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
IATA	IATA	kA	IATA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
regionalních	regionalní	k2eAgFnPc2d1	regionalní
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
speciální	speciální	k2eAgFnSc1d1	speciální
zbytková	zbytkový	k2eAgFnSc1d1	zbytková
společnost	společnost	k1gFnSc1	společnost
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
Aeroflot	aeroflot	k1gInSc1	aeroflot
ze	z	k7c2	z
státní	státní	k2eAgFnSc2d1	státní
firmy	firma	k1gFnSc2	firma
přeměněn	přeměněn	k2eAgInSc1d1	přeměněn
na	na	k7c4	na
akciovou	akciový	k2eAgFnSc4d1	akciová
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
bylo	být	k5eAaImAgNnS	být
49	[number]	k4	49
<g/>
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
prodáno	prodat	k5eAaPmNgNnS	prodat
zaměstnancům	zaměstnanec	k1gMnPc3	zaměstnanec
a	a	k8xC	a
dalším	další	k2eAgMnPc3d1	další
zájemcům	zájemce	k1gMnPc3	zájemce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Aeroflot	aeroflot	k1gInSc1	aeroflot
členem	člen	k1gInSc7	člen
letecké	letecký	k2eAgFnSc2d1	letecká
aliance	aliance	k1gFnSc2	aliance
SkyTeam	SkyTeam	k1gInSc1	SkyTeam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hodnocení	hodnocení	k1gNnSc1	hodnocení
==	==	k?	==
</s>
</p>
<p>
<s>
Aeroflot	aeroflot	k1gInSc1	aeroflot
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
ruská	ruský	k2eAgFnSc1d1	ruská
společnost	společnost	k1gFnSc1	společnost
získala	získat	k5eAaPmAgFnS	získat
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
rating	rating	k1gInSc4	rating
"	"	kIx"	"
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
"	"	kIx"	"
na	na	k7c4	na
kvalitu	kvalita	k1gFnSc4	kvalita
služeb	služba	k1gFnPc2	služba
Skytrax	Skytrax	k1gInSc4	Skytrax
<g/>
,	,	kIx,	,
a	a	k8xC	a
popáté	popáté	k4xO	popáté
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
prestižní	prestižní	k2eAgInSc1d1	prestižní
Skytrax	Skytrax	k1gInSc1	Skytrax
World	Worlda	k1gFnPc2	Worlda
Airline	Airlin	k1gInSc5	Airlin
Awards	Awardsa	k1gFnPc2	Awardsa
International	International	k1gMnSc1	International
Award	Award	k1gMnSc1	Award
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
uznán	uznat	k5eAaPmNgInS	uznat
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
agenturou	agentura	k1gFnSc7	agentura
Brand	Branda	k1gFnPc2	Branda
Finance	finance	k1gFnSc2	finance
jako	jako	k8xS	jako
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
značka	značka	k1gFnSc1	značka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
značka	značka	k1gFnSc1	značka
v	v	k7c6	v
leteckém	letecký	k2eAgInSc6d1	letecký
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Aeroflot	aeroflot	k1gInSc1	aeroflot
byl	být	k5eAaImAgInS	být
také	také	k9	také
uznán	uznat	k5eAaPmNgInS	uznat
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
podle	podle	k7c2	podle
verze	verze	k1gFnSc2	verze
největšího	veliký	k2eAgInSc2d3	veliký
světového	světový	k2eAgInSc2d1	světový
cestovního	cestovní	k2eAgInSc2d1	cestovní
webu	web	k1gInSc2	web
TripAdvisor	TripAdvisora	k1gFnPc2	TripAdvisora
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Destinace	destinace	k1gFnSc2	destinace
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
operoval	operovat	k5eAaImAgMnS	operovat
Aeroflot	aeroflot	k1gInSc4	aeroflot
lety	léto	k1gNnPc7	léto
do	do	k7c2	do
129	[number]	k4	129
destinací	destinace	k1gFnPc2	destinace
<g/>
/	/	kIx~	/
<g/>
letišť	letiště	k1gNnPc2	letiště
především	především	k9	především
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
létá	létat	k5eAaImIp3nS	létat
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pravidelně	pravidelně	k6eAd1	pravidelně
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
obsluhoval	obsluhovat	k5eAaImAgMnS	obsluhovat
také	také	k9	také
letiště	letiště	k1gNnSc2	letiště
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
Aeroflot	aeroflot	k1gInSc1	aeroflot
létá	létat	k5eAaImIp3nS	létat
pravidelně	pravidelně	k6eAd1	pravidelně
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
ruzyňské	ruzyňský	k2eAgNnSc4d1	ruzyňské
letiště	letiště	k1gNnSc4	letiště
již	již	k6eAd1	již
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
jeho	jeho	k3xOp3gInSc1	jeho
provoz	provoz	k1gInSc1	provoz
po	po	k7c6	po
Druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
linku	linka	k1gFnSc4	linka
provozoval	provozovat	k5eAaImAgMnS	provozovat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
spojení	spojení	k1gNnSc2	spojení
létala	létat	k5eAaImAgNnP	létat
letadla	letadlo	k1gNnPc1	letadlo
sovětské	sovětský	k2eAgFnSc2d1	sovětská
výroby	výroba	k1gFnSc2	výroba
jako	jako	k8xS	jako
Tupolev	Tupolev	k1gFnSc2	Tupolev
Tu-	Tu-	k1gFnSc2	Tu-
<g/>
154	[number]	k4	154
<g/>
,	,	kIx,	,
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
104	[number]	k4	104
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tu-	Tu-	k1gFnSc1	Tu-
<g/>
134	[number]	k4	134
<g/>
,	,	kIx,	,
Iljušin	iljušin	k1gInSc1	iljušin
Il-	Il-	k1gFnSc1	Il-
<g/>
62	[number]	k4	62
či	či	k8xC	či
Il-	Il-	k1gMnSc1	Il-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
se	se	k3xPyFc4	se
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
objevily	objevit	k5eAaPmAgInP	objevit
větší	veliký	k2eAgInPc1d2	veliký
letouny	letoun	k1gInPc1	letoun
Il-	Il-	k1gFnSc1	Il-
<g/>
86	[number]	k4	86
<g/>
,	,	kIx,	,
Il-	Il-	k1gFnSc1	Il-
<g/>
96	[number]	k4	96
nebo	nebo	k8xC	nebo
Boeing	boeing	k1gInSc4	boeing
767	[number]	k4	767
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
linku	linka	k1gFnSc4	linka
obsluhovaly	obsluhovat	k5eAaImAgInP	obsluhovat
Boeingy	boeing	k1gInPc1	boeing
737	[number]	k4	737
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
2018	[number]	k4	2018
létá	létat	k5eAaImIp3nS	létat
tato	tento	k3xDgFnSc1	tento
společnost	společnost	k1gFnSc1	společnost
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
šestkrát	šestkrát	k6eAd1	šestkrát
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lince	linka	k1gFnSc6	linka
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
nasazeny	nasazen	k2eAgInPc1d1	nasazen
Airbusy	airbus	k1gInPc1	airbus
A	A	kA	A
<g/>
320	[number]	k4	320
<g/>
,	,	kIx,	,
A321	A321	k1gFnSc1	A321
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
denně	denně	k6eAd1	denně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
ruský	ruský	k2eAgInSc1d1	ruský
stroj	stroj	k1gInSc1	stroj
Suchoj	Suchoj	k1gInSc1	Suchoj
Superjet	Superjet	k1gInSc1	Superjet
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
byly	být	k5eAaImAgFnP	být
frekvence	frekvence	k1gFnPc4	frekvence
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
denně	denně	k6eAd1	denně
na	na	k7c4	na
pět	pět	k4xCc4	pět
denně	denně	k6eAd1	denně
navýšeny	navýšit	k5eAaPmNgFnP	navýšit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flotila	flotila	k1gFnSc1	flotila
==	==	k?	==
</s>
</p>
<p>
<s>
Flotila	flotila	k1gFnSc1	flotila
Aeroflotu	aeroflot	k1gInSc2	aeroflot
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
čítala	čítat	k5eAaImAgFnS	čítat
251	[number]	k4	251
letounů	letoun	k1gInPc2	letoun
s	s	k7c7	s
průměrným	průměrný	k2eAgNnSc7d1	průměrné
stářím	stáří	k1gNnSc7	stáří
4,2	[number]	k4	4,2
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Letecké	letecký	k2eAgFnPc1d1	letecká
nehody	nehoda	k1gFnPc1	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
leteckých	letecký	k2eAgFnPc2d1	letecká
nehod	nehoda	k1gFnPc2	nehoda
není	být	k5eNaImIp3nS	být
kompletní	kompletní	k2eAgFnSc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
Aeroflot	aeroflot	k1gInSc1	aeroflot
141	[number]	k4	141
–	–	k?	–
havárie	havárie	k1gFnSc1	havárie
letounu	letoun	k1gInSc2	letoun
Tu-	Tu-	k1gFnSc2	Tu-
<g/>
154	[number]	k4	154
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
z	z	k7c2	z
100	[number]	k4	100
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
Aeroflot	aeroflot	k1gInSc1	aeroflot
892	[number]	k4	892
–	–	k?	–
havárie	havárie	k1gFnSc2	havárie
při	při	k7c6	při
neúspěšném	úspěšný	k2eNgInSc6d1	neúspěšný
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
přistání	přistání	k1gNnSc6	přistání
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
73	[number]	k4	73
z	z	k7c2	z
82	[number]	k4	82
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
typu	typ	k1gInSc2	typ
letadla	letadlo	k1gNnSc2	letadlo
přejeďte	přejet	k5eAaPmRp2nP	přejet
myší	myš	k1gFnSc7	myš
po	po	k7c6	po
obrázku	obrázek	k1gInSc6	obrázek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Aeroflot	aeroflot	k1gInSc1	aeroflot
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
</s>
</p>
