<s>
Herbert	Herbert	k1gInSc1	Herbert
Clark	Clark	k1gInSc1	Clark
Hoover	Hoover	k1gInSc1	Hoover
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1874	[number]	k4	1874
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
31	[number]	k4	31
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1929	[number]	k4	1929
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Syn	syn	k1gMnSc1	syn
kvakerů	kvaker	k1gMnPc2	kvaker
<g/>
,	,	kIx,	,
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Herbert	Herbert	k1gInSc1	Herbert
ještě	ještě	k6eAd1	ještě
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Jesse	Jess	k1gMnSc2	Jess
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
Hulda	Huldo	k1gNnSc2	Huldo
Minthorn	Minthorno	k1gNnPc2	Minthorno
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
rodinné	rodinný	k2eAgNnSc1d1	rodinné
příjmení	příjmení	k1gNnSc1	příjmení
neznělo	znět	k5eNaImAgNnS	znět
Hoover	Hoover	k1gInSc4	Hoover
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
Huber	Huber	k1gInSc1	Huber
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ministr	ministr	k1gMnSc1	ministr
obchodu	obchod	k1gInSc2	obchod
USA	USA	kA	USA
garantoval	garantovat	k5eAaBmAgMnS	garantovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Masarykem	Masaryk	k1gMnSc7	Masaryk
první	první	k4xOgInSc4	první
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
světový	světový	k2eAgInSc4d1	světový
kongres	kongres	k1gInSc4	kongres
o	o	k7c6	o
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
řízení	řízení	k1gNnSc6	řízení
<g/>
,	,	kIx,	,
konaný	konaný	k2eAgInSc1d1	konaný
v	v	k7c6	v
Pantheonu	Pantheon	k1gInSc6	Pantheon
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1924	[number]	k4	1924
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
PIMCO	PIMCO	kA	PIMCO
-	-	kIx~	-
First	First	k1gMnSc1	First
Prague	Pragu	k1gFnSc2	Pragu
International	International	k1gMnPc2	International
Management	management	k1gInSc1	management
Congress	Congressa	k1gFnPc2	Congressa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
západně	západně	k6eAd1	západně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
Velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Herbert	Herbert	k1gInSc1	Herbert
Hoover	Hoovero	k1gNnPc2	Hoovero
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Herbert	Herbert	k1gInSc1	Herbert
Hoover	Hoover	k1gInSc1	Hoover
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Claus	Claus	k1gMnSc1	Claus
Bernet	Bernet	k1gMnSc1	Bernet
<g/>
:	:	kIx,	:
Herbert	Herbert	k1gMnSc1	Herbert
Hoover	Hoover	k1gMnSc1	Hoover
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
BBKL	BBKL	kA	BBKL
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
644	[number]	k4	644
<g/>
-	-	kIx~	-
<g/>
653	[number]	k4	653
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
