<p>
<s>
Chilli	chilli	k1gNnSc1	chilli
paprička	paprička	k1gFnSc1	paprička
(	(	kIx(	(
<g/>
též	též	k9	též
čili	čili	k8xC	čili
paprička	paprička	k1gFnSc1	paprička
<g/>
,	,	kIx,	,
feferonka	feferonka	k1gFnSc1	feferonka
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
nesprávně	správně	k6eNd1	správně
užívá	užívat	k5eAaImIp3nS	užívat
název	název	k1gInSc4	název
kayenský	kayenský	k2eAgInSc4d1	kayenský
pepř	pepř	k1gInSc4	pepř
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
kajenský	kajenský	k2eAgInSc1d1	kajenský
pepř	pepř	k1gInSc1	pepř
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
především	především	k6eAd1	především
exotických	exotický	k2eAgInPc2d1	exotický
druhů	druh	k1gInPc2	druh
křovitých	křovitý	k2eAgFnPc2d1	křovitá
paprik	paprika	k1gFnPc2	paprika
<g/>
,	,	kIx,	,
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc2	Vietnam
<g/>
,	,	kIx,	,
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
i	i	k9	i
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
malé	malý	k2eAgFnPc4d1	malá
světlejší	světlý	k2eAgFnPc4d2	světlejší
papričky	paprička	k1gFnPc4	paprička
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
vůní	vůně	k1gFnSc7	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
feferonky	feferonka	k1gFnPc1	feferonka
jsou	být	k5eAaImIp3nP	být
ostré	ostrý	k2eAgFnPc1d1	ostrá
odrůdy	odrůda	k1gFnPc1	odrůda
zpravidla	zpravidla	k6eAd1	zpravidla
vytrvalých	vytrvalý	k2eAgFnPc2d1	vytrvalá
kořeninových	kořeninový	k2eAgFnPc2d1	kořeninová
paprik	paprika	k1gFnPc2	paprika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
pálivou	pálivý	k2eAgFnSc4d1	pálivá
chuť	chuť	k1gFnSc4	chuť
vděčí	vděčit	k5eAaImIp3nS	vděčit
látce	látka	k1gFnSc3	látka
kapsaicin	kapsaicin	k2eAgInSc4d1	kapsaicin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vitamíny	vitamín	k1gInPc4	vitamín
C	C	kA	C
a	a	k8xC	a
karoten	karoten	k1gInSc4	karoten
B.	B.	kA	B.
Podporují	podporovat	k5eAaImIp3nP	podporovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
dráždí	dráždit	k5eAaImIp3nS	dráždit
pohlavní	pohlavní	k2eAgNnSc4d1	pohlavní
a	a	k8xC	a
močové	močový	k2eAgNnSc4d1	močové
ústrojí	ústrojí	k1gNnSc4	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přiměřených	přiměřený	k2eAgFnPc6d1	přiměřená
dávkách	dávka	k1gFnPc6	dávka
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
žaludek	žaludek	k1gInSc4	žaludek
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
pepř	pepř	k1gInSc1	pepř
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
příznivý	příznivý	k2eAgInSc4d1	příznivý
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
zažívání	zažívání	k1gNnSc4	zažívání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kultury	kultura	k1gFnPc1	kultura
<g/>
,	,	kIx,	,
zvyklé	zvyklý	k2eAgNnSc1d1	zvyklé
na	na	k7c4	na
ostrá	ostrý	k2eAgNnPc4d1	ostré
jídla	jídlo	k1gNnPc4	jídlo
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
kořením	kořenit	k5eAaImIp1nS	kořenit
<g/>
,	,	kIx,	,
snáze	snadno	k6eAd2	snadno
snášejí	snášet	k5eAaImIp3nP	snášet
zamoření	zamoření	k1gNnPc1	zamoření
pokrmů	pokrm	k1gInPc2	pokrm
a	a	k8xC	a
prostředí	prostředí	k1gNnSc2	prostředí
kolem	kolem	k7c2	kolem
sebe	se	k3xPyFc2	se
různými	různý	k2eAgFnPc7d1	různá
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
těmi	ten	k3xDgInPc7	ten
vyvolávajícími	vyvolávající	k2eAgInPc7d1	vyvolávající
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Chilli	chilli	k1gNnSc1	chilli
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
změny	změna	k1gFnPc4	změna
ve	v	k7c4	v
střevní	střevní	k2eAgFnSc4d1	střevní
sliznici	sliznice	k1gFnSc4	sliznice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
potom	potom	k6eAd1	potom
leccos	leccos	k3yInSc4	leccos
snese	snést	k5eAaPmIp3nS	snést
<g/>
.	.	kIx.	.
</s>
<s>
Chili	Chile	k1gFnSc4	Chile
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
také	také	k9	také
za	za	k7c4	za
afrodisiakum	afrodisiakum	k1gNnSc4	afrodisiakum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
Chilli	chilli	k1gNnSc4	chilli
papričky	paprička	k1gFnSc2	paprička
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
Indiáni	Indián	k1gMnPc1	Indián
již	již	k6eAd1	již
v	v	k7c6	v
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k6eAd1	už
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
samosprašných	samosprašný	k2eAgFnPc2d1	samosprašná
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
začaly	začít	k5eAaPmAgFnP	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
je	být	k5eAaImIp3nS	být
evropští	evropský	k2eAgMnPc1d1	evropský
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
portugalští	portugalský	k2eAgMnPc1d1	portugalský
<g/>
)	)	kIx)	)
kupci	kupec	k1gMnPc1	kupec
rychle	rychle	k6eAd1	rychle
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
představovaly	představovat	k5eAaImAgFnP	představovat
cenné	cenný	k2eAgNnSc4d1	cenné
koření	koření	k1gNnSc4	koření
a	a	k8xC	a
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
pepř	pepř	k1gInSc4	pepř
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
obliby	obliba	k1gFnPc1	obliba
doznaly	doznat	k5eAaPmAgFnP	doznat
především	především	k6eAd1	především
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dnes	dnes	k6eAd1	dnes
představují	představovat	k5eAaImIp3nP	představovat
nedílnou	dílný	k2eNgFnSc4d1	nedílná
součást	součást	k1gFnSc4	součást
indické	indický	k2eAgFnSc2d1	indická
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Osmanskou	osmanský	k2eAgFnSc4d1	Osmanská
říši	říše	k1gFnSc4	říše
dostaly	dostat	k5eAaPmAgInP	dostat
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
Maďaři	Maďar	k1gMnPc1	Maďar
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
své	svůj	k3xOyFgNnSc4	svůj
národní	národní	k2eAgNnSc4d1	národní
koření	koření	k1gNnSc4	koření
–	–	k?	–
mletou	mletý	k2eAgFnSc4d1	mletá
papriku	paprika	k1gFnSc4	paprika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Chilli	chilli	k1gNnSc1	chilli
papričky	paprička	k1gFnSc2	paprička
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
kultivarů	kultivar	k1gInPc2	kultivar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
do	do	k7c2	do
následujících	následující	k2eAgInPc2d1	následující
druhů	druh	k1gInPc2	druh
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
paprika	paprika	k1gFnSc1	paprika
čínská	čínský	k2eAgFnSc1d1	čínská
(	(	kIx(	(
<g/>
Capsicum	Capsicum	k1gInSc1	Capsicum
chinense	chinense	k1gFnSc2	chinense
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
paprika	paprika	k1gFnSc1	paprika
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
(	(	kIx(	(
<g/>
Capsicum	Capsicum	k1gInSc1	Capsicum
pubescens	pubescens	k1gInSc1	pubescens
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
paprika	paprika	k1gFnSc1	paprika
křídlatá	křídlatý	k2eAgFnSc1d1	křídlatá
(	(	kIx(	(
<g/>
Capsicum	Capsicum	k1gInSc1	Capsicum
baccatum	baccatum	k1gNnSc1	baccatum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
paprika	paprika	k1gFnSc1	paprika
křovitá	křovitý	k2eAgFnSc1d1	křovitá
(	(	kIx(	(
<g/>
Capsicum	Capsicum	k1gInSc1	Capsicum
frutescens	frutescens	k1gInSc1	frutescens
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
paprika	paprika	k1gFnSc1	paprika
setá	setý	k2eAgFnSc1d1	setá
(	(	kIx(	(
<g/>
Capsicum	Capsicum	k1gInSc1	Capsicum
annuum	annuum	k1gNnSc1	annuum
<g/>
)	)	kIx)	)
<g/>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
kapie	kapie	k1gFnSc1	kapie
byla	být	k5eAaImAgFnS	být
vyšlechtěna	vyšlechtit	k5eAaPmNgFnS	vyšlechtit
jen	jen	k9	jen
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
Capsicum	Capsicum	k1gInSc1	Capsicum
annuum	annuum	k1gInSc1	annuum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Superchilli	Superchill	k1gMnPc1	Superchill
a	a	k8xC	a
nejpálivější	pálivý	k2eAgFnPc1d3	nejpálivější
papričky	paprička	k1gFnPc1	paprička
na	na	k7c6	na
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejpálivějších	pálivý	k2eAgFnPc2d3	nejpálivější
papriček	paprička	k1gFnPc2	paprička
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
na	na	k7c6	na
indickém	indický	k2eAgInSc6d1	indický
subkontinentu	subkontinent	k1gInSc6	subkontinent
<g/>
.	.	kIx.	.
</s>
<s>
Tamní	tamní	k2eAgNnSc1d1	tamní
chilli	chilli	k1gNnSc1	chilli
se	se	k3xPyFc4	se
honosí	honosit	k5eAaImIp3nS	honosit
příznačným	příznačný	k2eAgInSc7d1	příznačný
názvem	název	k1gInSc7	název
Bhut	Bhut	k1gMnSc1	Bhut
Jolokia	Jolokius	k1gMnSc2	Jolokius
(	(	kIx(	(
<g/>
Ghost	Ghost	k1gMnSc1	Ghost
Pepper	Pepper	k1gMnSc1	Pepper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
chilli	chilli	k1gNnSc1	chilli
démon	démon	k1gMnSc1	démon
<g/>
"	"	kIx"	"
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
donucovací	donucovací	k2eAgInSc1d1	donucovací
prostředek	prostředek	k1gInSc1	prostředek
místní	místní	k2eAgFnSc2d1	místní
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc2d1	vojenská
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
kouřových	kouřový	k2eAgInPc2d1	kouřový
granátů	granát	k1gInPc2	granát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nepotvrzených	potvrzený	k2eNgFnPc2d1	nepotvrzená
zvěstí	zvěst	k1gFnPc2	zvěst
prý	prý	k9	prý
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc3	úmrtí
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
démona	démon	k1gMnSc2	démon
<g/>
"	"	kIx"	"
omylem	omylem	k6eAd1	omylem
pozřel	pozřít	k5eAaPmAgMnS	pozřít
<g/>
.	.	kIx.	.
<g/>
Bhut	Bhut	k1gInSc4	Bhut
Jolokia	Jolokium	k1gNnSc2	Jolokium
byla	být	k5eAaImAgFnS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nejpálivější	pálivý	k2eAgFnSc4d3	nejpálivější
papričku	paprička	k1gFnSc4	paprička
na	na	k7c6	na
světě	svět	k1gInSc6	svět
až	až	k9	až
1	[number]	k4	1
041	[number]	k4	041
427	[number]	k4	427
jednotek	jednotka	k1gFnPc2	jednotka
Scovilleovy	Scovilleův	k2eAgFnSc2d1	Scovilleův
stupnice	stupnice	k1gFnSc2	stupnice
(	(	kIx(	(
<g/>
SHU	SHU	kA	SHU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
papričky	paprička	k1gFnPc1	paprička
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobku	dvojnásobek	k1gInSc2	dvojnásobek
této	tento	k3xDgFnSc2	tento
hodnoty	hodnota	k1gFnSc2	hodnota
a	a	k8xC	a
Bhut	Bhut	k1gInSc1	Bhut
Jolokia	Jolokium	k1gNnSc2	Jolokium
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
sedmou	sedma	k1gFnSc7	sedma
známou	známý	k2eAgFnSc7d1	známá
nejpálivější	pálivý	k2eAgFnSc7d3	nejpálivější
papričkou	paprička	k1gFnSc7	paprička
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kajenský	Kajenský	k2eAgInSc1d1	Kajenský
pepř	pepř	k1gInSc1	pepř
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
prášek	prášek	k1gInSc1	prášek
ze	z	k7c2	z
sušených	sušený	k2eAgInPc2d1	sušený
chili	chil	k1gMnPc7	chil
papriček	paprička	k1gFnPc2	paprička
různých	různý	k2eAgFnPc2d1	různá
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
žlutou	žlutý	k2eAgFnSc4d1	žlutá
až	až	k8xS	až
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
palčivou	palčivý	k2eAgFnSc4d1	palčivá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
nepřesný	přesný	k2eNgInSc1d1	nepřesný
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nesprávným	správný	k2eNgInSc7d1	nesprávný
překladem	překlad	k1gInSc7	překlad
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
"	"	kIx"	"
<g/>
pepper	pepper	k1gInSc1	pepper
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
jak	jak	k6eAd1	jak
pepř	pepř	k1gInSc4	pepř
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
papriku	paprika	k1gFnSc4	paprika
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
chilli	chilli	k1gNnSc2	chilli
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
kajenský	kajenský	k2eAgMnSc1d1	kajenský
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
počeštěným	počeštěný	k2eAgInSc7d1	počeštěný
názvem	název	k1gInSc7	název
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
,	,	kIx,	,
papričky	paprička	k1gFnSc2	paprička
Cayenne	Cayenn	k1gInSc5	Cayenn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Množení	množení	k1gNnSc4	množení
==	==	k?	==
</s>
</p>
<p>
<s>
Chilli	chilli	k1gNnSc1	chilli
papričky	paprička	k1gFnSc2	paprička
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
semínky	semínko	k1gNnPc7	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
jsou	být	k5eAaImIp3nP	být
kulatá	kulatý	k2eAgNnPc1d1	kulaté
<g/>
,	,	kIx,	,
zploštěná	zploštěný	k2eAgNnPc1d1	zploštěné
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
vrásčitá	vrásčitý	k2eAgFnSc1d1	vrásčitá
žluté	žlutý	k2eAgFnPc4d1	žlutá
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Časem	čas	k1gInSc7	čas
semínka	semínko	k1gNnSc2	semínko
postupně	postupně	k6eAd1	postupně
hnědnou	hnědnout	k5eAaImIp3nP	hnědnout
což	což	k3yQnSc4	což
může	moct	k5eAaImIp3nS	moct
ukazovat	ukazovat	k5eAaImF	ukazovat
nižší	nízký	k2eAgFnSc4d2	nižší
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
klíčivost	klíčivost	k1gFnSc4	klíčivost
semínek	semínko	k1gNnPc2	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
klíčivost	klíčivost	k1gFnSc4	klíčivost
kolem	kolem	k7c2	kolem
95	[number]	k4	95
<g/>
%	%	kIx~	%
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
klíčivost	klíčivost	k1gFnSc4	klíčivost
zachovat	zachovat	k5eAaPmF	zachovat
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
==	==	k?	==
</s>
</p>
<p>
<s>
Papričky	paprička	k1gFnPc1	paprička
lze	lze	k6eAd1	lze
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
pěstovat	pěstovat	k5eAaImF	pěstovat
i	i	k9	i
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
se	se	k3xPyFc4	se
však	však	k9	však
speciální	speciální	k2eAgInSc1d1	speciální
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
důležité	důležitý	k2eAgInPc4d1	důležitý
kroky	krok	k1gInPc4	krok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předpěstování	předpěstování	k1gNnSc1	předpěstování
–	–	k?	–
Začátkem	začátkem	k7c2	začátkem
nového	nový	k2eAgInSc2d1	nový
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
osivo	osivo	k1gNnSc1	osivo
v	v	k7c6	v
bytě	byt	k1gInSc6	byt
umístí	umístit	k5eAaPmIp3nS	umístit
do	do	k7c2	do
zeminy	zemina	k1gFnSc2	zemina
a	a	k8xC	a
vyčká	vyčkat	k5eAaPmIp3nS	vyčkat
se	se	k3xPyFc4	se
na	na	k7c4	na
vyklíčení	vyklíčení	k1gNnPc4	vyklíčení
a	a	k8xC	a
první	první	k4xOgInPc1	první
děložní	děložní	k2eAgInPc1d1	děložní
lístky	lístek	k1gInPc1	lístek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesazení	přesazení	k1gNnSc1	přesazení
–	–	k?	–
Po	po	k7c6	po
zesílení	zesílení	k1gNnSc6	zesílení
rostliny	rostlina	k1gFnSc2	rostlina
následuje	následovat	k5eAaImIp3nS	následovat
přesazení	přesazení	k1gNnSc1	přesazení
do	do	k7c2	do
menšího	malý	k2eAgInSc2d2	menší
samostatného	samostatný	k2eAgInSc2d1	samostatný
kořenáče	kořenáč	k1gInSc2	kořenáč
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
chilli	chilli	k1gNnSc6	chilli
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
příhodné	příhodný	k2eAgFnPc4d1	příhodná
venkovní	venkovní	k2eAgFnPc4d1	venkovní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsadba	výsadba	k1gFnSc1	výsadba
–	–	k?	–
S	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
prvních	první	k4xOgInPc2	první
jarních	jarní	k2eAgMnPc2d1	jarní
dní	den	k1gInPc2	den
s	s	k7c7	s
jistotou	jistota	k1gFnSc7	jistota
absence	absence	k1gFnSc2	absence
mrazivého	mrazivý	k2eAgNnSc2d1	mrazivé
rána	ráno	k1gNnSc2	ráno
lze	lze	k6eAd1	lze
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
finální	finální	k2eAgFnSc3d1	finální
výsadbě	výsadba	k1gFnSc3	výsadba
na	na	k7c4	na
kýžené	kýžený	k2eAgNnSc4d1	kýžené
místo	místo	k1gNnSc4	místo
do	do	k7c2	do
skleníku	skleník	k1gInSc2	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
dostatek	dostatek	k1gInSc4	dostatek
tepla	teplo	k1gNnSc2	teplo
i	i	k8xC	i
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
nenáročná	náročný	k2eNgFnSc1d1	nenáročná
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
na	na	k7c4	na
zálivku	zálivka	k1gFnSc4	zálivka
<g/>
.	.	kIx.	.
</s>
<s>
Potřebné	potřebný	k2eAgFnPc4d1	potřebná
živiny	živina	k1gFnPc4	živina
dodá	dodat	k5eAaPmIp3nS	dodat
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
hnojení	hnojení	k1gNnSc1	hnojení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vegetační	vegetační	k2eAgFnSc1d1	vegetační
doba	doba	k1gFnSc1	doba
–	–	k?	–
Chilli	chilli	k1gNnSc7	chilli
papričky	paprička	k1gFnSc2	paprička
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
pěstovány	pěstován	k2eAgFnPc1d1	pěstována
jako	jako	k8xS	jako
jednoleté	jednoletý	k2eAgFnPc1d1	jednoletá
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
zazimování	zazimování	k1gNnSc6	zazimování
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
mrazy	mráz	k1gInPc7	mráz
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
pěstovat	pěstovat	k5eAaImF	pěstovat
i	i	k9	i
jako	jako	k9	jako
víceleté	víceletý	k2eAgNnSc1d1	víceleté
<g/>
.	.	kIx.	.
</s>
<s>
Papriky	paprika	k1gFnPc1	paprika
jsou	být	k5eAaImIp3nP	být
stáleplodící	stáleplodící	k2eAgFnSc7d1	stáleplodící
<g/>
,	,	kIx,	,
výhodou	výhoda	k1gFnSc7	výhoda
víceletého	víceletý	k2eAgNnSc2d1	víceleté
pěstování	pěstování	k1gNnSc2	pěstování
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgFnSc1d2	bohatší
a	a	k8xC	a
časnější	časný	k2eAgFnSc1d2	časnější
úroda	úroda	k1gFnSc1	úroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Bhut	Bhut	k1gInSc1	Bhut
Jolokia	Jolokium	k1gNnSc2	Jolokium
</s>
</p>
<p>
<s>
Carolina	Carolina	k1gFnSc1	Carolina
Reaper	Reapra	k1gFnPc2	Reapra
</s>
</p>
<p>
<s>
Paprička	paprička	k1gFnSc1	paprička
habanero	habanera	k1gFnSc5	habanera
</s>
</p>
<p>
<s>
Paprička	paprička	k1gFnSc1	paprička
jalapeñ	jalapeñ	k?	jalapeñ
</s>
</p>
<p>
<s>
Pepper	Pepper	k1gMnSc1	Pepper
X	X	kA	X
</s>
</p>
<p>
<s>
Trinidad	Trinidad	k1gInSc1	Trinidad
Moruga	Morug	k1gMnSc2	Morug
Scorpion	Scorpion	k1gInSc1	Scorpion
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
chilli	chilli	k1gNnSc2	chilli
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Chile	Chile	k1gNnSc1	Chile
Head	Heada	k1gFnPc2	Heada
</s>
</p>
