<s>
OMV	OMV	kA	OMV
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Österreichische	Österreichische	k1gNnSc1	Österreichische
Mineralölverwaltung	Mineralölverwaltunga	k1gFnPc2	Mineralölverwaltunga
<g/>
,	,	kIx,	,
ÖMV	ÖMV	kA	ÖMV
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rakouský	rakouský	k2eAgInSc1d1	rakouský
koncern	koncern	k1gInSc1	koncern
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
především	především	k9	především
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
výrobu	výroba	k1gFnSc4	výroba
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
provozuje	provozovat	k5eAaImIp3nS	provozovat
síť	síť	k1gFnSc1	síť
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
stejnojmenných	stejnojmenný	k2eAgFnPc2d1	stejnojmenná
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
koupil	koupit	k5eAaPmAgMnS	koupit
čerpací	čerpací	k2eAgFnSc2d1	čerpací
stanice	stanice	k1gFnSc2	stanice
ARAL	ARAL	kA	ARAL
(	(	kIx(	(
<g/>
z	z	k7c2	z
ARALu	ARALus	k1gInSc2	ARALus
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
OMV	OMV	kA	OMV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
koupí	koupit	k5eAaPmIp3nP	koupit
se	se	k3xPyFc4	se
OMV	OMV	kA	OMV
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
trhu	trh	k1gInSc6	trh
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
společností	společnost	k1gFnPc2	společnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
13	[number]	k4	13
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
34.000	[number]	k4	34.000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
OMV	OMV	kA	OMV
je	být	k5eAaImIp3nS	být
veřejně	veřejně	k6eAd1	veřejně
obchodovanou	obchodovaný	k2eAgFnSc7d1	obchodovaná
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
OIAG	OIAG	kA	OIAG
(	(	kIx(	(
<g/>
31,5	[number]	k4	31,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
IPIC	IPIC	kA	IPIC
(	(	kIx(	(
<g/>
20,0	[number]	k4	20,0
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
Volně	volně	k6eAd1	volně
prodejné	prodejný	k2eAgFnPc1d1	prodejná
akcie	akcie	k1gFnPc1	akcie
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
(	(	kIx(	(
<g/>
48,3	[number]	k4	48,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
Vlastní	vlastní	k2eAgFnPc1d1	vlastní
akcie	akcie	k1gFnPc1	akcie
(	(	kIx(	(
<g/>
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
OMV	OMV	kA	OMV
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
i	i	k9	i
podíly	podíl	k1gInPc4	podíl
v	v	k7c6	v
několika	několik	k4yIc6	několik
ropných	ropný	k2eAgFnPc6d1	ropná
a	a	k8xC	a
petrochemických	petrochemický	k2eAgFnPc2d1	petrochemická
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
podíly	podíl	k1gInPc1	podíl
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Petrom	Petrom	k1gInSc1	Petrom
SA	SA	kA	SA
(	(	kIx(	(
<g/>
51	[number]	k4	51
%	%	kIx~	%
<g/>
)	)	kIx)	)
Borealis	Borealis	k1gInSc1	Borealis
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
S	s	k7c7	s
(	(	kIx(	(
<g/>
36	[number]	k4	36
%	%	kIx~	%
<g/>
)	)	kIx)	)
Agrolinz	Agrolinz	k1gInSc1	Agrolinz
Melamine	Melamin	k1gInSc5	Melamin
International	International	k1gMnSc1	International
(	(	kIx(	(
<g/>
AMI	AMI	kA	AMI
<g/>
)	)	kIx)	)
GmbH	GmbH	k1gMnSc1	GmbH
(	(	kIx(	(
<g/>
51	[number]	k4	51
%	%	kIx~	%
<g/>
)	)	kIx)	)
MOL	mol	k1gMnSc1	mol
Group	Group	k1gMnSc1	Group
(	(	kIx(	(
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
Bayernoil	Bayernoil	k1gMnSc1	Bayernoil
Raffineriegesellschaft	Raffineriegesellschaft	k1gMnSc1	Raffineriegesellschaft
<g />
.	.	kIx.	.
</s>
<s>
GmbH	GmbH	k?	GmbH
(	(	kIx(	(
<g/>
45	[number]	k4	45
%	%	kIx~	%
<g/>
)	)	kIx)	)
EconGas	EconGas	k1gMnSc1	EconGas
GmbH	GmbH	k1gMnSc1	GmbH
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
Petrol	Petrol	k1gInSc4	Petrol
Ofisi	Ofise	k1gFnSc4	Ofise
A.Ş.	A.Ş.	k1gFnSc2	A.Ş.
(	(	kIx(	(
<g/>
34	[number]	k4	34
%	%	kIx~	%
<g/>
)	)	kIx)	)
Nabucco	Nabucco	k1gMnSc1	Nabucco
Gas	Gas	k1gMnSc1	Gas
Pipeline	Pipelin	k1gInSc5	Pipelin
International	International	k1gFnSc7	International
GmbH	GmbH	k1gMnSc7	GmbH
(	(	kIx(	(
<g/>
16.67	[number]	k4	16.67
%	%	kIx~	%
<g/>
)	)	kIx)	)
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
působí	působit	k5eAaImIp3nP	působit
OMV	OMV	kA	OMV
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
provozuje	provozovat	k5eAaImIp3nS	provozovat
220	[number]	k4	220
čerpacích	čerpací	k2eAgFnPc2d1	čerpací
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
84	[number]	k4	84
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
dalších	další	k2eAgInPc2d1	další
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1.500	[number]	k4	1.500
na	na	k7c6	na
čerpacích	čerpací	k2eAgFnPc6d1	čerpací
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
OMV	OMV	kA	OMV
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
OMV	OMV	kA	OMV
</s>
