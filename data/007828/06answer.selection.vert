<s>
Vnímání	vnímání	k1gNnSc1	vnímání
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
polohou	poloha	k1gFnSc7	poloha
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
každého	každý	k3xTgNnSc2	každý
oka	oko	k1gNnSc2	oko
dopadá	dopadat	k5eAaImIp3nS	dopadat
mírně	mírně	k6eAd1	mírně
odlišný	odlišný	k2eAgInSc1d1	odlišný
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
skládá	skládat	k5eAaImIp3nS	skládat
prostorový	prostorový	k2eAgInSc1d1	prostorový
obraz	obraz	k1gInSc1	obraz
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
