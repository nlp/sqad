<s>
Václav	Václav	k1gMnSc1	Václav
Davídek	Davídek	k1gMnSc1	Davídek
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1913	[number]	k4	1913
Lipnice	Lipnice	k1gFnSc1	Lipnice
u	u	k7c2	u
Spáleného	spálený	k2eAgNnSc2d1	spálené
Poříčí	Poříčí	k1gNnSc2	Poříčí
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1993	[number]	k4	1993
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
archivář	archivář	k1gMnSc1	archivář
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
genealogických	genealogický	k2eAgInPc2d1	genealogický
a	a	k8xC	a
regionálních	regionální	k2eAgInPc2d1	regionální
spisů	spis	k1gInPc2	spis
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
a	a	k8xC	a
Spáleného	spálený	k2eAgNnSc2d1	spálené
Poříčí	Poříčí	k1gNnSc2	Poříčí
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgMnSc4	tento
archiváře	archivář	k1gMnSc4	archivář
a	a	k8xC	a
historika	historik	k1gMnSc4	historik
přivedl	přivést	k5eAaPmAgMnS	přivést
na	na	k7c4	na
Kladensko	Kladensko	k1gNnSc4	Kladensko
sňatek	sňatek	k1gInSc1	sňatek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
přiženil	přiženit	k5eAaPmAgMnS	přiženit
do	do	k7c2	do
Trněného	trněný	k2eAgNnSc2d1	trněný
Újezda	Újezdo	k1gNnSc2	Újezdo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
místní	místní	k2eAgFnSc2d1	místní
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Zákolany	Zákolana	k1gFnSc2	Zákolana
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
jako	jako	k9	jako
student	student	k1gMnSc1	student
psal	psát	k5eAaImAgMnS	psát
recenze	recenze	k1gFnPc4	recenze
na	na	k7c4	na
knihy	kniha	k1gFnPc4	kniha
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
regionální	regionální	k2eAgFnSc2d1	regionální
historie	historie	k1gFnSc2	historie
Kladenska	Kladensko	k1gNnSc2	Kladensko
například	například	k6eAd1	například
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Mottla	Mottl	k1gMnSc2	Mottl
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
,	,	kIx,	,
město	město	k1gNnSc1	město
a	a	k8xC	a
statek	statek	k1gInSc1	statek
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
kraji	kraj	k1gInSc6	kraj
nebo	nebo	k8xC	nebo
Bartůňkovy	Bartůňkův	k2eAgFnPc4d1	Bartůňkova
Náboženské	náboženský	k2eAgFnPc4d1	náboženská
dějiny	dějiny	k1gFnPc4	dějiny
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
ve	v	k7c6	v
Státním	státní	k2eAgInSc6d1	státní
zemědělském	zemědělský	k2eAgInSc6d1	zemědělský
archivu	archiv	k1gInSc6	archiv
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
pracovišť	pracoviště	k1gNnPc2	pracoviště
byl	být	k5eAaImAgMnS	být
i	i	k9	i
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Buštěhradě	Buštěhrad	k1gInSc6	Buštěhrad
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
rodištěm	rodiště	k1gNnSc7	rodiště
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
ho	on	k3xPp3gMnSc4	on
zákonitě	zákonitě	k6eAd1	zákonitě
přivedlo	přivést	k5eAaPmAgNnS	přivést
k	k	k7c3	k
hlubokému	hluboký	k2eAgInSc3d1	hluboký
zájmu	zájem	k1gInSc3	zájem
o	o	k7c4	o
Budeč	Budeč	k1gFnSc4	Budeč
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
psal	psát	k5eAaImAgMnS	psát
už	už	k6eAd1	už
od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
psaním	psaní	k1gNnSc7	psaní
knih	kniha	k1gFnPc2	kniha
o	o	k7c6	o
přemyslovském	přemyslovský	k2eAgNnSc6d1	přemyslovské
sídle	sídlo	k1gNnSc6	sídlo
Budeč	Budeč	k1gFnSc1	Budeč
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
kniha	kniha	k1gFnSc1	kniha
Co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
některých	některý	k3yIgMnPc2	některý
předních	přední	k2eAgMnPc2d1	přední
českých	český	k2eAgMnPc2d1	český
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Pozdrav	pozdrav	k1gInSc1	pozdrav
z	z	k7c2	z
Budče	Budeč	k1gFnSc2	Budeč
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1944	[number]	k4	1944
Vévodská	vévodský	k2eAgFnSc1d1	vévodská
Budeč	Budeč	k1gFnSc1	Budeč
<g/>
.	.	kIx.	.
1945	[number]	k4	1945
Sláva	Sláva	k1gMnSc1	Sláva
svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
Budče	Budeč	k1gFnSc2	Budeč
<g/>
.	.	kIx.	.
</s>
<s>
Poutník	poutník	k1gMnSc1	poutník
svatováclavský	svatováclavský	k2eAgMnSc1d1	svatováclavský
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
s.	s.	k?	s.
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
101	[number]	k4	101
<g/>
;	;	kIx,	;
Tisíciletý	tisíciletý	k2eAgInSc1d1	tisíciletý
demografický	demografický	k2eAgInSc1d1	demografický
vývoj	vývoj	k1gInSc1	vývoj
ve	v	k7c6	v
středočeských	středočeský	k2eAgFnPc6d1	Středočeská
farnostech	farnost	k1gFnPc6	farnost
Budeč	Budeč	k1gFnSc1	Budeč
a	a	k8xC	a
Koleč	Koleč	k1gInSc1	Koleč
<g/>
.	.	kIx.	.
</s>
<s>
Demografie	demografie	k1gFnSc1	demografie
II	II	kA	II
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
s.	s.	k?	s.
338	[number]	k4	338
<g/>
–	–	k?	–
<g/>
350	[number]	k4	350
<g/>
.	.	kIx.	.
</s>
<s>
Nález	nález	k1gInSc1	nález
mince	mince	k1gFnSc2	mince
a	a	k8xC	a
datování	datování	k1gNnSc2	datování
mariánského	mariánský	k2eAgInSc2d1	mariánský
kostelíka	kostelík	k1gInSc2	kostelík
na	na	k7c6	na
Budči	Budeč	k1gFnSc6	Budeč
<g/>
.	.	kIx.	.
</s>
<s>
Numismatické	numismatický	k2eAgInPc4d1	numismatický
listy	list	k1gInPc4	list
III	III	kA	III
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
s.	s.	k?	s.
48	[number]	k4	48
<g/>
;	;	kIx,	;
Co	co	k3yRnSc1	co
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
Prahou	Praha	k1gFnSc7	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
12	[number]	k4	12
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
D-Die	D-Die	k1gFnSc1	D-Die
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
105-215	[number]	k4	105-215
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
149	[number]	k4	149
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Davídek	Davídek	k1gMnSc1	Davídek
(	(	kIx(	(
<g/>
historik	historik	k1gMnSc1	historik
<g/>
)	)	kIx)	)
</s>
