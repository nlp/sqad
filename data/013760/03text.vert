<s>
Noble	Noble	k6eAd1
Automotive	Automotiv	k1gInSc5
</s>
<s>
Noble	Noble	k6eAd1
Automotive	Automotiv	k1gInSc5
LogoZákladní	LogoZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
Public	publicum	k1gNnPc2
limited	limited	k2eAgFnSc2d1
company	compana	k1gFnSc2
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1999	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Lee	Lea	k1gFnSc3
Noble	Noble	k1gFnSc2
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Leicester	Leicester	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Leicester	Leicester	k1gInSc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Charakteristika	charakteristikon	k1gNnSc2
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
Automobilový	automobilový	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
Automobily	automobil	k1gInPc1
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
Noblecars	Noblecars	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k6eAd1
Automotive	Automotiv	k1gInSc5
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
jednoduše	jednoduše	k6eAd1
jako	jako	k8xC,k8xS
Noble	Noble	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
sportovních	sportovní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Firmu	firma	k1gFnSc4
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Leedsu	Leeds	k1gInSc6
<g/>
,	,	kIx,
West	West	k2eAgInSc1d1
Yorkshire	Yorkshir	k1gInSc5
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
Lee	Lea	k1gFnSc3
Noble	Noble	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
cílem	cíl	k1gInSc7
byla	být	k5eAaImAgFnS
výroba	výroba	k1gFnSc1
velmi	velmi	k6eAd1
rychlých	rychlý	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
automobilů	automobil	k1gInPc2
s	s	k7c7
motorem	motor	k1gInSc7
umístěným	umístěný	k2eAgInSc7d1
uprostřed	uprostřed	k7c2
a	a	k8xC
pohonem	pohon	k1gInSc7
zadních	zadní	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lee	Lea	k1gFnSc6
Noble	Noble	k1gFnSc2
byl	být	k5eAaImAgMnS
hlavní	hlavní	k2eAgMnSc1d1
designer	designer	k1gMnSc1
a	a	k8xC
majitel	majitel	k1gMnSc1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
však	však	k9
v	v	k7c6
srpnu	srpen	k1gInSc6
2006	#num#	k4
prodal	prodat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vedení	vedení	k1gNnSc2
společnosti	společnost	k1gFnSc2
odstoupil	odstoupit	k5eAaPmAgMnS
v	v	k7c6
únoru	únor	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
oznámil	oznámit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
nový	nový	k2eAgInSc4d1
projekt	projekt	k1gInSc4
<g/>
,	,	kIx,
Fenix	Fenix	k1gInSc4
Automotive	Automotiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlo	sídlo	k1gNnSc1
Noblu	Nobl	k1gInSc2
se	se	k3xPyFc4
poté	poté	k6eAd1
přesunulo	přesunout	k5eAaPmAgNnS
do	do	k7c2
větších	veliký	k2eAgFnPc2d2
prostor	prostora	k1gFnPc2
Meridian	Meridiana	k1gFnPc2
Business	business	k1gInSc1
Parku	park	k1gInSc2
poblíž	poblíž	k7c2
Leicesteru	Leicester	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gInSc1
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
nízkoprodukční	nízkoprodukční	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
sportovních	sportovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
jehož	jehož	k3xOyRp3gMnPc4
starší	starý	k2eAgMnPc4d2
modely	model	k1gInPc4
patří	patřit	k5eAaImIp3nP
M12	M12	k1gMnPc1
GTO	GTO	kA
<g/>
,	,	kIx,
M12	M12	k1gFnSc1
GTO-	GTO-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
,	,	kIx,
M12	M12	k1gFnSc1
GTO-3R	GTO-3R	k1gFnSc1
a	a	k8xC
Noble	Noble	k1gFnSc1
M	M	kA
<g/>
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modely	model	k1gInPc1
M12	M12	k1gFnSc4
GTO-3R	GTO-3R	k1gFnPc1
a	a	k8xC
M400	M400	k1gFnPc1
mají	mít	k5eAaImIp3nP
stejné	stejný	k2eAgNnSc4d1
šasi	šasi	k1gNnSc4
a	a	k8xC
karoserii	karoserie	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
lehce	lehko	k6eAd1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
v	v	k7c6
motorech	motor	k1gInPc6
a	a	k8xC
odpružení	odpružení	k1gNnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
model	model	k1gInSc1
M15	M15	k1gFnSc2
je	být	k5eAaImIp3nS
již	již	k9
osazen	osadit	k5eAaPmNgInS
novým	nový	k2eAgInSc7d1
nosným	nosný	k2eAgInSc7d1
rámem	rám	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rámy	rám	k1gInPc4
a	a	k8xC
karoserie	karoserie	k1gFnPc4
si	se	k3xPyFc3
Noble	Noble	k1gFnSc1
nechává	nechávat	k5eAaImIp3nS
vyrábět	vyrábět	k5eAaImF
v	v	k7c6
Hi-Tech	Hi-T	k1gFnPc6
Automotive	Automotiv	k1gInSc5
v	v	k7c4
Port	port	k1gInSc4
Elizabeth	Elizabeth	k1gFnSc2
v	v	k7c6
Jihoafrické	jihoafrický	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakmile	jakmile	k8xS
je	být	k5eAaImIp3nS
karoserie	karoserie	k1gFnSc1
hotová	hotový	k2eAgFnSc1d1
<g/>
,	,	kIx,
odešle	odeslat	k5eAaPmIp3nS
se	se	k3xPyFc4
do	do	k7c2
továrny	továrna	k1gFnSc2
Noblu	Nobl	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
přidány	přidán	k2eAgInPc4d1
motor	motor	k1gInSc4
<g/>
,	,	kIx,
převodovka	převodovka	k1gFnSc1
<g/>
,	,	kIx,
ap.	ap.	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
přišel	přijít	k5eAaPmAgInS
Noble	Noble	k1gInSc1
s	s	k7c7
modelem	model	k1gInSc7
M	M	kA
<g/>
600	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zařadil	zařadit	k5eAaPmAgInS
Noble	Noble	k1gInSc4
mezi	mezi	k7c4
výrobce	výrobce	k1gMnPc4
supersportovních	supersportovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noblem	Noblo	k1gNnSc7
vyráběný	vyráběný	k2eAgInSc4d1
dvakrát	dvakrát	k6eAd1
přeplňovaný	přeplňovaný	k2eAgInSc4d1
vidlicový	vidlicový	k2eAgInSc4d1
osmiválec	osmiválec	k1gInSc4
o	o	k7c6
objemu	objem	k1gInSc6
4,4	4,4	k4
litru	litr	k1gInSc2
produkuje	produkovat	k5eAaImIp3nS
650	#num#	k4
koňských	koňský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
lehkou	lehký	k2eAgFnSc7d1
konstrukcí	konstrukce	k1gFnSc7
z	z	k7c2
uhlíkových	uhlíkový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
se	se	k3xPyFc4
Noble	Noble	k1gInSc1
M600	M600	k1gFnSc2
dostává	dostávat	k5eAaImIp3nS
do	do	k7c2
společnosti	společnost	k1gFnSc2
značek	značka	k1gFnPc2
jako	jako	k9
jsou	být	k5eAaImIp3nP
Ferrari	ferrari	k1gNnSc1
nebo	nebo	k8xC
Porsche	Porsche	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
zakázky	zakázka	k1gFnPc1
byly	být	k5eAaImAgFnP
zhotoveny	zhotovit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
se	s	k7c7
základní	základní	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
od	od	k7c2
200	#num#	k4
tisíc	tisíc	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
se	se	k3xPyFc4
exportovalo	exportovat	k5eAaBmAgNnS
pouze	pouze	k6eAd1
220	#num#	k4
kusů	kus	k1gInPc2
modelů	model	k1gInPc2
M12	M12	k1gMnSc7
GTO-3R	GTO-3R	k1gMnSc7
a	a	k8xC
M	M	kA
<g/>
400	#num#	k4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
jako	jako	k8xC,k8xS
jediné	jediný	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
k	k	k7c3
dostání	dostání	k1gNnSc3
na	na	k7c6
americkém	americký	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Práva	práv	k2eAgFnSc1d1
na	na	k7c4
prodej	prodej	k1gInSc4
těchto	tento	k3xDgInPc2
modelů	model	k1gInPc2
koupila	koupit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
společnost	společnost	k1gFnSc1
1G	1G	k4
Racing	Racing	k1gInSc1
z	z	k7c2
Ohia	Ohio	k1gNnSc2
a	a	k8xC
kvůli	kvůli	k7c3
vysoké	vysoký	k2eAgFnSc3d1
poptávce	poptávka	k1gFnSc3
po	po	k7c6
těchto	tento	k3xDgInPc6
vozech	vůz	k1gInPc6
začala	začít	k5eAaPmAgFnS
vyrábět	vyrábět	k5eAaImF
vlastní	vlastní	k2eAgFnPc4d1
kopie	kopie	k1gFnPc4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Rossion	Rossion	k1gInSc1
Q	Q	kA
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M10	M10	k1gFnSc1
(	(	kIx(
<g/>
1999	#num#	k4
-	-	kIx~
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Noble	Noble	k1gFnSc1
M10	M10	k1gMnPc2
je	být	k5eAaImIp3nS
dvoumístný	dvoumístný	k2eAgInSc4d1
kabriolet	kabriolet	k1gInSc4
s	s	k7c7
nepřeplňovaným	přeplňovaný	k2eNgInSc7d1
motorem	motor	k1gInSc7
o	o	k7c6
objemu	objem	k1gInSc6
2,5	2,5	k4
litru	litr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
představen	představit	k5eAaPmNgInS
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
již	již	k6eAd1
nevyrábí	vyrábět	k5eNaImIp3nS
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
model	model	k1gInSc4
M	M	kA
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modelů	model	k1gInPc2
M10	M10	k1gFnPc1
se	se	k3xPyFc4
vyrobilo	vyrobit	k5eAaPmAgNnS
velice	velice	k6eAd1
málo	málo	k4c1
<g/>
,	,	kIx,
protože	protože	k8xS
mnoho	mnoho	k4c1
zákazníků	zákazník	k1gMnPc2
změnilo	změnit	k5eAaPmAgNnS
objednávku	objednávka	k1gFnSc4
na	na	k7c4
model	model	k1gInSc4
M12	M12	k1gFnPc1
krátce	krátce	k6eAd1
po	po	k7c4
jeho	jeho	k3xOp3gNnSc4
oznámení	oznámení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noble	Noble	k1gFnSc1
M10	M10	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
a	a	k8xC
parametrech	parametr	k1gInPc6
podobný	podobný	k2eAgInSc4d1
Lotusu	Lotus	k1gInSc2
Elise	elise	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M12	M12	k1gFnSc1
(	(	kIx(
<g/>
2000	#num#	k4
-	-	kIx~
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
předchozí	předchozí	k2eAgInSc1d1
model	model	k1gInSc1
je	být	k5eAaImIp3nS
i	i	k9
Noble	Noble	k1gFnSc1
M12	M12	k1gFnPc2
dvoudveřový	dvoudveřový	k2eAgInSc1d1
dvoumístný	dvoumístný	k2eAgInSc1d1
sportovní	sportovní	k2eAgInSc1d1
vůz	vůz	k1gInSc1
<g/>
,	,	kIx,
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
však	však	k9
speciálně	speciálně	k6eAd1
upravený	upravený	k2eAgMnSc1d1
přeplňované	přeplňovaný	k2eAgInPc4d1
motory	motor	k1gInPc4
Ford	ford	k1gInSc1
Duratec	Duratec	k1gMnSc1
V6	V6	k1gMnSc1
a	a	k8xC
vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
variantách	varianta	k1gFnPc6
kupé	kupé	k1gNnSc2
a	a	k8xC
kabriolet	kabriolet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
M12	M12	k1gMnSc1
má	mít	k5eAaImIp3nS
ocelový	ocelový	k2eAgInSc4d1
vnitřní	vnitřní	k2eAgInSc4d1
rám	rám	k1gInSc4
<g/>
,	,	kIx,
ocelový	ocelový	k2eAgInSc4d1
skelet	skelet	k1gInSc4
a	a	k8xC
sklolaminátovou	sklolaminátový	k2eAgFnSc4d1
karoserii	karoserie	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
dělá	dělat	k5eAaImIp3nS
velmi	velmi	k6eAd1
lehký	lehký	k2eAgInSc1d1
a	a	k8xC
zároveň	zároveň	k6eAd1
tuhý	tuhý	k2eAgInSc4d1
vůz	vůz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
M12	M12	k1gFnSc1
svým	svůj	k3xOyFgInSc7
vzhledem	vzhled	k1gInSc7
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
pouze	pouze	k6eAd1
na	na	k7c4
závodní	závodní	k2eAgFnSc4d1
trať	trať	k1gFnSc4
<g/>
,	,	kIx,
poskytuje	poskytovat	k5eAaImIp3nS
slušné	slušný	k2eAgFnPc4d1
jízdní	jízdní	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
i	i	k9
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
M12	M12	k1gFnSc2
Coupe	coup	k1gInSc5
se	se	k3xPyFc4
vyráběla	vyrábět	k5eAaImAgNnP
ve	v	k7c6
3	#num#	k4
variantách	varianta	k1gFnPc6
<g/>
:	:	kIx,
</s>
<s>
ModelMotorVýkon	ModelMotorVýkon	k1gMnSc1
</s>
<s>
Noble	Noble	k1gFnSc1
M12	M12	k1gFnSc2
GTO	GTO	kA
<g/>
2.5	2.5	k4
<g/>
L	L	kA
bi-turbo	bi-turba	k1gFnSc5
<g/>
330	#num#	k4
bhp	bhp	k?
<g/>
/	/	kIx~
<g/>
231	#num#	k4
kW	kW	kA
</s>
<s>
Noble	Noble	k1gFnSc1
M12	M12	k1gFnSc1
GTO-	GTO-	k1gFnSc1
<g/>
33.0	33.0	k4
<g/>
L	L	kA
bi-turbo	bi-turba	k1gFnSc5
<g/>
365	#num#	k4
bhp	bhp	k?
<g/>
/	/	kIx~
<g/>
242	#num#	k4
kW	kW	kA
</s>
<s>
Noble	Noble	k1gFnSc1
M12	M12	k1gFnSc1
GTO-	GTO-	k1gFnSc1
<g/>
3	#num#	k4
<g/>
R	R	kA
<g/>
3.0	3.0	k4
<g/>
L	L	kA
bi-turbo	bi-turba	k1gFnSc5
<g/>
415	#num#	k4
bhp	bhp	k?
<g/>
/	/	kIx~
<g/>
262	#num#	k4
kW	kW	kA
</s>
<s>
Vrcholovým	vrcholový	k2eAgInSc7d1
modelem	model	k1gInSc7
M12	M12	k1gFnSc2
je	být	k5eAaImIp3nS
Noble	Noble	k1gInSc1
M	M	kA
<g/>
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M400	M400	k1gFnSc2
</s>
<s>
Noble	Noble	k1gFnSc1
M400	M400	k1gFnSc2
je	být	k5eAaImIp3nS
závodní	závodní	k2eAgNnSc1d1
provedení	provedení	k1gNnSc1
modelu	model	k1gInSc2
M	M	kA
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
motor	motor	k1gInSc1
disponuje	disponovat	k5eAaBmIp3nS
výkonem	výkon	k1gInSc7
425	#num#	k4
koňských	koňský	k2eAgFnPc2d1
sil	síla	k1gFnPc2
(	(	kIx(
<g/>
317	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
a	a	k8xC
Noble	Noble	k1gInSc4
udává	udávat	k5eAaImIp3nS
zrychlení	zrychlení	k1gNnSc1
z	z	k7c2
0-100	0-100	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
pod	pod	k7c7
4	#num#	k4
sekundy	sekund	k1gInPc7
a	a	k8xC
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Magazín	magazín	k1gInSc1
Car	car	k1gMnSc1
and	and	k?
Driver	driver	k1gInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
při	při	k7c6
testu	test	k1gInSc6
zrychlení	zrychlení	k1gNnSc1
0-100	0-100	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
hodnoty	hodnota	k1gFnPc4
3,3	3,3	k4
sekundy	sekunda	k1gFnSc2
a	a	k8xC
0-160	0-160	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
7,52	7,52	k4
sekundy	sekunda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjištěné	zjištěný	k2eAgFnPc4d1
boční	boční	k2eAgNnPc4d1
přetížení	přetížení	k1gNnPc4
dosahuje	dosahovat	k5eAaImIp3nS
až	až	k9
1,2	1,2	k4
<g/>
G.	G.	kA
</s>
<s>
Označení	označení	k1gNnSc1
M400	M400	k1gFnSc2
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
ze	z	k7c2
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
poměr	poměr	k1gInSc1
výkonu	výkon	k1gInSc2
vzhledem	vzhledem	k7c3
k	k	k7c3
váze	váha	k1gFnSc3
je	být	k5eAaImIp3nS
400	#num#	k4
koní	kůň	k1gMnPc2
na	na	k7c4
tunu	tuna	k1gFnSc4
(	(	kIx(
<g/>
298	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpodstatnější	podstatný	k2eAgInPc1d3
rozdíly	rozdíl	k1gInPc1
oproti	oproti	k7c3
modelu	model	k1gInSc2
M12	M12	k1gMnPc2
jsou	být	k5eAaImIp3nP
ražené	ražený	k2eAgInPc1d1
písty	píst	k1gInPc1
<g/>
,	,	kIx,
přední	přední	k2eAgMnSc1d1
příčný	příčný	k2eAgMnSc1d1
stabilizátor	stabilizátor	k1gMnSc1
<g/>
,	,	kIx,
tužší	tuhý	k2eAgNnPc1d2
odpružení	odpružení	k1gNnPc1
a	a	k8xC
tlumiče	tlumič	k1gInPc1
<g/>
,	,	kIx,
pneumatiky	pneumatika	k1gFnPc1
Pirelli	Pirelle	k1gFnSc4
P	P	kA
Zero	Zero	k1gNnSc1
<g/>
,	,	kIx,
jemnější	jemný	k2eAgFnSc1d2
převodovka	převodovka	k1gFnSc1
a	a	k8xC
užší	úzký	k2eAgInSc1d2
střední	střední	k2eAgInSc1d1
přístrojový	přístrojový	k2eAgInSc1d1
panel	panel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Model	model	k1gInSc1
M400	M400	k1gFnSc2
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
jako	jako	k8xS,k8xC
mimořádné	mimořádný	k2eAgNnSc1d1
závodní	závodní	k2eAgNnSc1d1
vozidlo	vozidlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
schopné	schopný	k2eAgNnSc1d1
poskytovat	poskytovat	k5eAaImF
příjemné	příjemný	k2eAgFnPc4d1
jízdní	jízdní	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M14	M14	k1gFnSc2
</s>
<s>
Noble	Noble	k1gInSc1
M14	M14	k1gFnSc2
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
představen	představit	k5eAaPmNgInS
na	na	k7c6
autosalonu	autosalon	k1gInSc6
British	British	k1gInSc1
Motor	motor	k1gInSc1
Show	show	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
a	a	k8xC
okamžitě	okamžitě	k6eAd1
vzbudil	vzbudit	k5eAaPmAgInS
zájem	zájem	k1gInSc4
motoristického	motoristický	k2eAgNnSc2d1
veřejnosti	veřejnost	k1gFnSc3
jako	jako	k8xC,k8xS
možný	možný	k2eAgMnSc1d1
soupeř	soupeř	k1gMnSc1
renomovaných	renomovaný	k2eAgInPc2d1
sportovních	sportovní	k2eAgInPc2d1
vozů	vůz	k1gInPc2
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
Porsche	Porsche	k1gNnSc4
911	#num#	k4
Turbo	turba	k1gFnSc5
a	a	k8xC
Ferrari	Ferrari	k1gMnSc1
F	F	kA
<g/>
430	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
vozu	vůz	k1gInSc2
je	být	k5eAaImIp3nS
lehce	lehko	k6eAd1
upravený	upravený	k2eAgInSc1d1
podvozek	podvozek	k1gInSc1
modelu	model	k1gInSc2
M	M	kA
<g/>
12	#num#	k4
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgInSc4
navazuje	navazovat	k5eAaImIp3nS
nová	nový	k2eAgFnSc1d1
karoserie	karoserie	k1gFnSc1
a	a	k8xC
vylepšený	vylepšený	k2eAgInSc1d1
interiér	interiér	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
představení	představení	k1gNnSc6
ale	ale	k8xC
Lee	Lea	k1gFnSc6
Noble	Noble	k1gFnSc2
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
M14	M14	k1gFnSc1
není	být	k5eNaImIp3nS
natolik	natolik	k6eAd1
rozdílný	rozdílný	k2eAgInSc1d1
od	od	k7c2
modelu	model	k1gInSc2
M	M	kA
<g/>
12	#num#	k4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
nárůst	nárůst	k1gInSc1
ceny	cena	k1gFnSc2
oprávněný	oprávněný	k2eAgInSc1d1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
přijal	přijmout	k5eAaPmAgMnS
několik	několik	k4yIc4
předobjednávek	předobjednávka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Noble	Noble	k1gFnSc1
namísto	namísto	k7c2
toho	ten	k3xDgInSc2
vyvinul	vyvinout	k5eAaPmAgInS
zbrusu	zbrusu	k6eAd1
nové	nový	k2eAgNnSc4d1
vozidlo	vozidlo	k1gNnSc4
<g/>
,	,	kIx,
Noble	Noble	k1gFnPc4
M	M	kA
<g/>
15	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
již	již	k6eAd1
více	hodně	k6eAd2
odlišovalo	odlišovat	k5eAaImAgNnS
od	od	k7c2
modelů	model	k1gInPc2
M12	M12	k1gFnSc2
a	a	k8xC
M	M	kA
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M15	M15	k1gFnSc2
</s>
<s>
Výrobou	výroba	k1gFnSc7
modelu	model	k1gInSc2
M15	M15	k1gFnPc2
se	se	k3xPyFc4
Noble	Noble	k1gInSc1
zaměřil	zaměřit	k5eAaPmAgInS
na	na	k7c4
daleko	daleko	k6eAd1
širší	široký	k2eAgInSc4d2
trh	trh	k1gInSc4
a	a	k8xC
začal	začít	k5eAaPmAgInS
soupeřit	soupeřit	k5eAaImF
přímo	přímo	k6eAd1
s	s	k7c7
vozy	vůz	k1gInPc7
Porsche	Porsche	k1gNnSc2
911	#num#	k4
Turbo	turba	k1gFnSc5
a	a	k8xC
Ferrari	Ferrari	k1gMnSc1
F	F	kA
<g/>
430	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
v	v	k7c6
Noblu	Nobl	k1gInSc6
objevily	objevit	k5eAaPmAgInP
doposud	doposud	k6eAd1
nepoužívané	používaný	k2eNgInPc4d1
prvky	prvek	k1gInPc4
<g/>
:	:	kIx,
satelitní	satelitní	k2eAgFnSc1d1
navigace	navigace	k1gFnSc1
<g/>
,	,	kIx,
kontrola	kontrola	k1gFnSc1
trakce	trakce	k1gFnSc1
<g/>
,	,	kIx,
elektronicky	elektronicky	k6eAd1
ovládané	ovládaný	k2eAgNnSc4d1
stahování	stahování	k1gNnSc4
oken	okno	k1gNnPc2
nebo	nebo	k8xC
ABS	ABS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k6eAd1
zvýšenému	zvýšený	k2eAgInSc3d1
komfortu	komfort	k1gInSc3
a	a	k8xC
využitelnosti	využitelnost	k1gFnSc3
v	v	k7c6
běžném	běžný	k2eAgInSc6d1
provozu	provoz	k1gInSc6
<g/>
,	,	kIx,
Noble	Noble	k1gFnSc1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
tiskovém	tiskový	k2eAgNnSc6d1
prohlášení	prohlášení	k1gNnSc6
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
M15	M15	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
být	být	k5eAaImF
na	na	k7c6
okruhu	okruh	k1gInSc6
výrazně	výrazně	k6eAd1
rychlejší	rychlý	k2eAgFnSc1d2
než	než	k8xS
M	M	kA
<g/>
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zrychlení	zrychlení	k1gNnSc1
vozu	vůz	k1gInSc2
z	z	k7c2
0-100	0-100	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
je	být	k5eAaImIp3nS
3,3	3,3	k4
sekundy	sekunda	k1gFnSc2
a	a	k8xC
maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
300	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Noble	Noble	k1gFnSc1
M15	M15	k1gFnSc2
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
zcela	zcela	k6eAd1
novém	nový	k2eAgInSc6d1
podvozku	podvozek	k1gInSc6
<g/>
,	,	kIx,
s	s	k7c7
podélně	podélně	k6eAd1
uloženým	uložený	k2eAgInSc7d1
motorem	motor	k1gInSc7
a	a	k8xC
převodovkou	převodovka	k1gFnSc7
vyráběnou	vyráběný	k2eAgFnSc7d1
na	na	k7c4
zakázku	zakázka	k1gFnSc4
firmo	firma	k1gFnSc5
Graziano	Graziana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojité	dvojitý	k2eAgNnSc1d1
vidlicové	vidlicový	k2eAgNnSc1d1
odpružení	odpružení	k1gNnSc1
je	být	k5eAaImIp3nS
vylepšením	vylepšení	k1gNnSc7
systému	systém	k1gInSc2
použitého	použitý	k2eAgInSc2d1
v	v	k7c6
M	M	kA
<g/>
400	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
podélnému	podélný	k2eAgNnSc3d1
uložení	uložení	k1gNnSc3
motoru	motor	k1gInSc2
dokázali	dokázat	k5eAaPmAgMnP
designeři	designer	k1gMnPc1
zlepšit	zlepšit	k5eAaPmF
přívod	přívod	k1gInSc1
chlazení	chlazení	k1gNnSc2
k	k	k7c3
motoru	motor	k1gInSc3
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
umožňuje	umožňovat	k5eAaImIp3nS
dvakrát	dvakrát	k6eAd1
přeplňovanému	přeplňovaný	k2eAgInSc3d1
vidlicovému	vidlicový	k2eAgInSc3d1
šestiválci	šestiválec	k1gInSc3
vyvinout	vyvinout	k5eAaPmF
výkon	výkon	k1gInSc4
455	#num#	k4
koní	kůň	k1gMnPc2
(	(	kIx(
<g/>
339	#num#	k4
<g/>
kW	kW	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motor	motor	k1gInSc1
je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
splňoval	splňovat	k5eAaImAgInS
současné	současný	k2eAgInPc4d1
emisní	emisní	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
a	a	k8xC
nový	nový	k2eAgInSc4d1
ocelovo-hliníkový	ocelovo-hliníkový	k2eAgInSc4d1
nosný	nosný	k2eAgInSc4d1
rám	rám	k1gInSc4
zaručuje	zaručovat	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc4d1
bezpečnost	bezpečnost	k1gFnSc4
potvrzenou	potvrzená	k1gFnSc4
mnoha	mnoho	k4c2
crash	crasha	k1gFnPc2
testy	testa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M15	M15	k1gMnPc2
je	být	k5eAaImIp3nS
prvním	první	k4xOgInSc7
vozem	vůz	k1gInSc7
této	tento	k3xDgFnSc2
značky	značka	k1gFnSc2
homologovaným	homologovaný	k2eAgInPc3d1
jak	jak	k8xS,k8xC
v	v	k7c6
Evropě	Evropa	k1gFnSc6
tak	tak	k9
i	i	k9
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
firmy	firma	k1gFnSc2
Lee	Lea	k1gFnSc6
Noble	Noble	k1gNnSc7
se	se	k3xPyFc4
k	k	k7c3
tomuto	tento	k3xDgInSc3
vozu	vůz	k1gInSc3
vyjádřil	vyjádřit	k5eAaPmAgMnS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
(	(	kIx(
<g/>
Noble	Noble	k1gNnSc1
<g/>
)	)	kIx)
M12	M12	k1gFnPc2
je	být	k5eAaImIp3nS
skvělé	skvělý	k2eAgNnSc1d1
auto	auto	k1gNnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
zaměřené	zaměřený	k2eAgNnSc1d1
jedním	jeden	k4xCgInSc7
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chtěl	chtít	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
vytvořit	vytvořit	k5eAaPmF
supersportovní	supersportovní	k2eAgInSc4d1
automobil	automobil	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
budou	být	k5eAaImBp3nP
moct	moct	k5eAaImF
majitelé	majitel	k1gMnPc1
využít	využít	k5eAaPmF
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Noble	Noble	k1gInSc4
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
udělat	udělat	k5eAaPmF
velký	velký	k2eAgInSc4d1
krok	krok	k1gInSc4
kupředu	kupředu	k6eAd1
po	po	k7c6
stránce	stránka	k1gFnSc6
vytříbenosti	vytříbenost	k1gFnSc2
<g/>
,	,	kIx,
praktičnosti	praktičnost	k1gFnSc2
a	a	k8xC
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
Noble	Noble	k1gFnSc1
M15	M15	k1gFnPc2
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
i	i	k9
v	v	k7c6
pořadu	pořad	k1gInSc6
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
testoval	testovat	k5eAaImAgMnS
Richard	Richard	k1gMnSc1
Hammond	Hammond	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
mnohem	mnohem	k6eAd1
rychlejší	rychlý	k2eAgMnSc1d2
na	na	k7c4
Top	topit	k5eAaImRp2nS
Gear	Geara	k1gFnPc2
okruhu	okruh	k1gInSc2
než	než	k8xS
starý	starý	k2eAgInSc4d1
Noble	Noble	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
Hammonda	Hammond	k1gMnSc2
zapříčiněno	zapříčinit	k5eAaPmNgNnS
novou	nový	k2eAgFnSc7d1
silnější	silný	k2eAgFnSc7d2
převodovkou	převodovka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
Noblu	Nobla	k1gFnSc4
přenášet	přenášet	k5eAaImF
více	hodně	k6eAd2
tahu	tah	k1gInSc2
a	a	k8xC
vyladit	vyladit	k5eAaPmF
totožný	totožný	k2eAgInSc4d1
motor	motor	k1gInSc4
na	na	k7c4
větší	veliký	k2eAgInSc4d2
výkon	výkon	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M600	M600	k1gFnSc2
</s>
<s>
Noble	Noble	k6eAd1
v	v	k7c6
současnosti	současnost	k1gFnSc6
vyrábí	vyrábět	k5eAaImIp3nS
nový	nový	k2eAgInSc1d1
model	model	k1gInSc1
M	M	kA
<g/>
600	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohání	pohánět	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
upravený	upravený	k2eAgInSc4d1
<g/>
,	,	kIx,
dvakrát	dvakrát	k6eAd1
přeplňovaný	přeplňovaný	k2eAgInSc1d1
vidlicový	vidlicový	k2eAgInSc1d1
osmiválec	osmiválec	k1gInSc1
Volvo	Volvo	k1gNnSc1
(	(	kIx(
<g/>
650	#num#	k4
koní	kůň	k1gMnPc2
<g/>
,	,	kIx,
485	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
výkon	výkon	k1gInSc1
je	být	k5eAaImIp3nS
přenášen	přenášet	k5eAaImNgInS
na	na	k7c4
zadní	zadní	k2eAgNnPc4d1
kola	kolo	k1gNnPc4
šestistupňovou	šestistupňový	k2eAgFnSc7d1
převodovkou	převodovka	k1gFnSc7
Yamaha	yamaha	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
disponuje	disponovat	k5eAaBmIp3nS
karoserií	karoserie	k1gFnPc2
z	z	k7c2
uhlíkových	uhlíkový	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
své	svůj	k3xOyFgFnSc6
váze	váha	k1gFnSc6
1300	#num#	k4
kg	kg	kA
dokáže	dokázat	k5eAaPmIp3nS
zrychlit	zrychlit	k5eAaPmF
z	z	k7c2
0-100	0-100	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
na	na	k7c4
3,5	3,5	k4
sekundy	sekunda	k1gFnSc2
a	a	k8xC
za	za	k7c4
další	další	k2eAgInPc4d1
4	#num#	k4
sekundy	sekund	k1gInPc4
dosáhne	dosáhnout	k5eAaPmIp3nS
rychlosti	rychlost	k1gFnSc2
160	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
V	v	k7c6
základním	základní	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
nenajdete	najít	k5eNaPmIp2nP
ve	v	k7c6
výbavě	výbava	k1gFnSc6
Noblu	Nobl	k1gInSc2
M600	M600	k1gFnSc2
ABS	ABS	kA
ani	ani	k8xC
kontrolu	kontrola	k1gFnSc4
trakce	trakce	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tudíž	tudíž	k8xC
vozem	vůz	k1gInSc7
pro	pro	k7c4
skutečně	skutečně	k6eAd1
zkušené	zkušený	k2eAgMnPc4d1
řidiče	řidič	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
tohoto	tento	k3xDgInSc2
supersportovního	supersportovní	k2eAgInSc2d1
vozu	vůz	k1gInSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
době	doba	k1gFnSc6
uvedení	uvedení	k1gNnSc2
na	na	k7c4
trh	trh	k1gInSc4
přibližně	přibližně	k6eAd1
200	#num#	k4
tisíc	tisíc	k4xCgInPc2
liber	libra	k1gFnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
počítá	počítat	k5eAaImIp3nS
s	s	k7c7
výrobou	výroba	k1gFnSc7
pouze	pouze	k6eAd1
50	#num#	k4
kusů	kus	k1gInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k6eAd1
také	také	k9
veřejně	veřejně	k6eAd1
testoval	testovat	k5eAaImAgMnS
model	model	k1gInSc4
M600	M600	k1gMnSc2
prototype	prototyp	k1gInSc5
2	#num#	k4
osazený	osazený	k2eAgInSc1d1
motorem	motor	k1gInSc7
s	s	k7c7
výkonem	výkon	k1gInSc7
sníženým	snížený	k2eAgInSc7d1
na	na	k7c4
500	#num#	k4
koní	kůň	k1gMnPc2
(	(	kIx(
<g/>
373	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
dosahoval	dosahovat	k5eAaImAgMnS
výsledků	výsledek	k1gInPc2
odpovídajících	odpovídající	k2eAgInPc2d1
vozů	vůz	k1gInPc2
Porsche	Porsche	k1gNnSc4
Carrera	Carrero	k1gNnSc2
GT	GT	kA
a	a	k8xC
Ferrari	Ferrari	k1gMnSc1
Enzo	Enzo	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Noble	Noble	k1gFnSc1
M600	M600	k1gFnPc2
se	se	k3xPyFc4
opět	opět	k6eAd1
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
pořadu	pořad	k1gInSc6
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gInSc4
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
páté	pátý	k4xOgFnSc6
epizodě	epizoda	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeremy	Jeremo	k1gNnPc7
Clarkson	Clarksona	k1gFnPc2
sice	sice	k8xC
lamentoval	lamentovat	k5eAaImAgMnS
nad	nad	k7c7
chudou	chudý	k2eAgFnSc7d1
výbavou	výbava	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
byl	být	k5eAaImAgInS
ohromen	ohromit	k5eAaPmNgInS
jeho	jeho	k3xOp3gInSc7
výkonem	výkon	k1gInSc7
a	a	k8xC
zrychlením	zrychlení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stig	Stig	k1gInSc1
s	s	k7c7
Noblem	Nobl	k1gMnSc7
M600	M600	k1gMnSc7
dosáhl	dosáhnout	k5eAaPmAgMnS
na	na	k7c4
Top	topit	k5eAaImRp2nS
Gear	Gear	k1gMnSc1
okruhu	okruh	k1gInSc2
času	čas	k1gInSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
17,7	17,7	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
i	i	k9
přes	přes	k7c4
relativně	relativně	k6eAd1
nepříznivé	příznivý	k2eNgFnPc4d1
povětrnostní	povětrnostní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
znamenalo	znamenat	k5eAaImAgNnS
osmý	osmý	k4xOgInSc4
nejrychlejší	rychlý	k2eAgInSc4d3
čas	čas	k1gInSc4
(	(	kIx(
<g/>
byl	být	k5eAaImAgInS
tedy	tedy	k9
rychlejší	rychlý	k2eAgMnSc1d2
než	než	k8xS
Bugatti	Bugatť	k1gFnPc1
EB	EB	kA
16,4	16,4	k4
Veyron	Veyron	k1gInSc4
nebo	nebo	k8xC
Pagani	Pagaň	k1gFnSc6
Zonda	Zonda	k1gFnSc1
Roadster	roadster	k1gInSc1
F	F	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
internetové	internetový	k2eAgFnSc2d1
portálu	portál	k1gInSc3
ATFULLCHAT	ATFULLCHAT	kA
není	být	k5eNaImIp3nS
vyloučeno	vyloučit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
mohl	moct	k5eAaImAgInS
M600	M600	k1gMnPc4
objevit	objevit	k5eAaPmF
i	i	k9
ve	v	k7c6
verzi	verze	k1gFnSc6
bez	bez	k7c2
pevné	pevný	k2eAgFnSc2d1
střechy	střecha	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
podle	podle	k7c2
vyjádření	vyjádření	k1gNnSc2
ředitele	ředitel	k1gMnSc2
Noblu	Nobl	k1gInSc2
Petera	Peter	k1gMnSc2
Boutwooda	Boutwood	k1gMnSc2
nejsou	být	k5eNaImIp3nP
v	v	k7c6
plánu	plán	k1gInSc6
žádné	žádný	k3yNgFnSc2
jiné	jiný	k2eAgFnSc2d1
karosářské	karosářský	k2eAgFnSc2d1
verze	verze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Noble	Noble	k1gFnSc2
Automotive	Automotiv	k1gInSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
