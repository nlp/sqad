<s>
Open	Open	k1gMnSc1
Systems	Systemsa	k1gFnPc2
Interconnection	Interconnection	k1gInSc4
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
propojení	propojení	k1gNnSc4
otevřených	otevřený	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Open	Opena	k1gFnPc2
Systems	Systemsa	k1gFnPc2
Interconnection	Interconnection	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Open	Open	k1gInSc1
Systems	Systems	k1gInSc1
Interconnection	Interconnection	k1gInSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
OSI	OSI	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
informatice	informatika	k1gFnSc6
snaha	snaha	k1gFnSc1
o	o	k7c6
standardizaci	standardizace	k1gFnSc6
komunikace	komunikace	k1gFnSc2
v	v	k7c6
počítačových	počítačový	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
(	(	kIx(
<g/>
propojení	propojení	k1gNnSc4
otevřených	otevřený	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
roku	rok	k1gInSc2
1977	#num#	k4
zahájila	zahájit	k5eAaPmAgFnS
organizace	organizace	k1gFnSc1
ISO	ISO	kA
společně	společně	k6eAd1
s	s	k7c7
ITU-	ITU-	k1gFnSc7
<g/>
T.	T.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7
hlavní	hlavní	k2eAgFnSc7d1
částí	část	k1gFnSc7
je	být	k5eAaImIp3nS
referenční	referenční	k2eAgInSc1d1
model	model	k1gInSc1
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
OSI	OSI	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
představoval	představovat	k5eAaImAgInS
podstatný	podstatný	k2eAgInSc4d1
pokrok	pokrok	k1gInSc4
v	v	k7c6
návrhu	návrh	k1gInSc6
komunikace	komunikace	k1gFnSc2
v	v	k7c6
počítačových	počítačový	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
rodina	rodina	k1gFnSc1
protokolů	protokol	k1gInPc2
TCP	TCP	kA
<g/>
/	/	kIx~
<g/>
IP	IP	kA
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
Internetu	Internet	k1gInSc6
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
jednodušší	jednoduchý	k2eAgInSc1d2
model	model	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Nicméně	nicméně	k8xC
pro	pro	k7c4
mnoho	mnoho	k4c4
lidí	člověk	k1gMnPc2
se	se	k3xPyFc4
zdál	zdát	k5eAaImAgInS
celý	celý	k2eAgInSc1d1
návrh	návrh	k1gInSc1
protokolů	protokol	k1gInPc2
příliš	příliš	k6eAd1
složitý	složitý	k2eAgInSc1d1
a	a	k8xC
těžko	těžko	k6eAd1
implementovatelný	implementovatelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
problémů	problém	k1gInPc2
byl	být	k5eAaImAgInS
přístup	přístup	k1gInSc1
návrhářů	návrhář	k1gMnPc2
protokolů	protokol	k1gInPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nepoužili	použít	k5eNaPmAgMnP
protokoly	protokol	k1gInPc4
již	již	k6eAd1
existující	existující	k2eAgInPc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
naplnili	naplnit	k5eAaPmAgMnP
model	model	k1gInSc4
svými	svůj	k3xOyFgFnPc7
nově	nově	k6eAd1
vytvořenými	vytvořený	k2eAgFnPc7d1
alternativami	alternativa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
byla	být	k5eAaImAgFnS
implementace	implementace	k1gFnPc4
tohoto	tento	k3xDgInSc2
modelu	model	k1gInSc2
značně	značně	k6eAd1
obtížná	obtížný	k2eAgFnSc1d1
a	a	k8xC
mnoho	mnoho	k4c1
výrobců	výrobce	k1gMnPc2
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc3
bránilo	bránit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
byly	být	k5eAaImAgInP
protokoly	protokol	k1gInPc1
schvalovány	schvalován	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
různé	různý	k2eAgFnPc1d1
strany	strana	k1gFnPc1
měly	mít	k5eAaImAgFnP
značně	značně	k6eAd1
rozdílné	rozdílný	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
mělo	mít	k5eAaImAgNnS
za	za	k7c4
následek	následek	k1gInSc4
značnou	značný	k2eAgFnSc4d1
složitost	složitost	k1gFnSc4
protokolů	protokol	k1gInPc2
a	a	k8xC
množství	množství	k1gNnSc4
nepovinných	povinný	k2eNgFnPc2d1
nadstaveb	nadstavba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
ani	ani	k8xC
požadavky	požadavek	k1gInPc1
administrativy	administrativa	k1gFnSc2
USA	USA	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
veškerý	veškerý	k3xTgInSc1
hardware	hardware	k1gInSc1
nakupovaný	nakupovaný	k2eAgInSc1d1
vládními	vládní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
podporoval	podporovat	k5eAaImAgMnS
OSI	OSI	kA
<g/>
,	,	kIx,
nedokázaly	dokázat	k5eNaPmAgFnP
prosadit	prosadit	k5eAaPmF
tento	tento	k3xDgInSc4
model	model	k1gInSc4
v	v	k7c6
komerční	komerční	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Model	model	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
celkem	celkem	k6eAd1
ze	z	k7c2
sedmi	sedm	k4xCc2
vrstev	vrstva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vrstva	vrstva	k1gFnSc1
1	#num#	k4
<g/>
:	:	kIx,
Fyzická	fyzický	k2eAgFnSc1d1
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
vrstvě	vrstva	k1gFnSc6
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
vlastní	vlastní	k2eAgInSc1d1
přenos	přenos	k1gInSc1
bitů	bit	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
elektrické	elektrický	k2eAgNnSc1d1
<g/>
,	,	kIx,
mechanické	mechanický	k2eAgFnPc4d1
a	a	k8xC
procedurální	procedurální	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
si	se	k3xPyFc3
pod	pod	k7c7
touto	tento	k3xDgFnSc7
vrstvou	vrstva	k1gFnSc7
lze	lze	k6eAd1
představit	představit	k5eAaPmF
kabeláž	kabeláž	k1gFnSc4
<g/>
,	,	kIx,
konektory	konektor	k1gInPc1
<g/>
,	,	kIx,
kódování	kódování	k1gNnSc1
signálu	signál	k1gInSc2
<g/>
...	...	k?
</s>
<s>
Vrstva	vrstva	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
Spojová	spojový	k2eAgFnSc1d1
</s>
<s>
Tato	tento	k3xDgFnSc1
vrstva	vrstva	k1gFnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
především	především	k9
logiku	logika	k1gFnSc4
a	a	k8xC
řízení	řízení	k1gNnSc4
přenosu	přenos	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
za	za	k7c4
paketizaci	paketizace	k1gFnSc4
dat	datum	k1gNnPc2
<g/>
,	,	kIx,
detekci	detekce	k1gFnSc3
chyb	chyba	k1gFnPc2
<g/>
,	,	kIx,
pravidla	pravidlo	k1gNnSc2
přístupu	přístup	k1gInSc2
k	k	k7c3
médiu	médium	k1gNnSc3
apod.	apod.	kA
</s>
<s>
Vrstva	vrstva	k1gFnSc1
3	#num#	k4
<g/>
:	:	kIx,
Síťová	síťový	k2eAgFnSc1d1
</s>
<s>
Vrstva	vrstva	k1gFnSc1
je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
za	za	k7c4
směrování	směrování	k1gNnSc4
(	(	kIx(
<g/>
hledá	hledat	k5eAaImIp3nS
nejlepší	dobrý	k2eAgFnSc4d3
cestu	cesta	k1gFnSc4
<g/>
,	,	kIx,
vyvažuje	vyvažovat	k5eAaImIp3nS
zátěž	zátěž	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obstarává	obstarávat	k5eAaImIp3nS
řízení	řízení	k1gNnSc4
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
účtování	účtování	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Vrstva	vrstva	k1gFnSc1
4	#num#	k4
<g/>
:	:	kIx,
Transportní	transportní	k2eAgFnSc2d1
</s>
<s>
Je	být	k5eAaImIp3nS
implementována	implementovat	k5eAaImNgFnS
přímo	přímo	k6eAd1
v	v	k7c6
koncovém	koncový	k2eAgNnSc6d1
zařízení	zařízení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přizpůsobuje	přizpůsobovat	k5eAaImIp3nS
vlastnosti	vlastnost	k1gFnPc4
sítě	síť	k1gFnSc2
(	(	kIx(
<g/>
předchozích	předchozí	k2eAgFnPc2d1
tří	tři	k4xCgFnPc2
vrstev	vrstva	k1gFnPc2
<g/>
)	)	kIx)
potřebám	potřeba	k1gFnPc3
aplikací	aplikace	k1gFnPc2
(	(	kIx(
<g/>
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
schopna	schopen	k2eAgFnSc1d1
rozlišovat	rozlišovat	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
bezchybný	bezchybný	k2eAgInSc4d1
kanál	kanál	k1gInSc4
se	s	k7c7
zachováním	zachování	k1gNnSc7
pořadí	pořadí	k1gNnSc2
a	a	k8xC
spravuje	spravovat	k5eAaImIp3nS
spojení	spojení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vrstva	vrstva	k1gFnSc1
5	#num#	k4
<g/>
:	:	kIx,
Relační	relační	k2eAgFnSc2d1
</s>
<s>
Byla	být	k5eAaImAgFnS
do	do	k7c2
protokolu	protokol	k1gInSc2
přidána	přidat	k5eAaPmNgFnS
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doplňuje	doplňovat	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
důležité	důležitý	k2eAgFnPc4d1
podrobnosti	podrobnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
přátelské	přátelský	k2eAgNnSc4d1
ukonční	ukonční	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
a	a	k8xC
řízení	řízení	k1gNnSc4
dialogů	dialog	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
poloduplex	poloduplex	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
synchronizační	synchronizační	k2eAgInPc1d1
body	bod	k1gInPc1
<g/>
,	,	kIx,
aktivity	aktivita	k1gFnPc1
<g/>
...	...	k?
</s>
<s>
Vrstva	vrstva	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
Prezentační	prezentační	k2eAgFnSc2d1
</s>
<s>
Sleduje	sledovat	k5eAaImIp3nS
význam	význam	k1gInSc4
přenášených	přenášený	k2eAgNnPc2d1
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
tedy	tedy	k9
jak	jak	k6eAd1
reprezentovat	reprezentovat	k5eAaImF
struktury	struktura	k1gFnPc4
a	a	k8xC
data	datum	k1gNnPc4
a	a	k8xC
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
přepravovat	přepravovat	k5eAaImF
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
za	za	k7c4
kódování	kódování	k1gNnSc4
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
UTF	UTF	kA
<g/>
,	,	kIx,
CP-	CP-	k1gFnSc1
<g/>
1250	#num#	k4
<g/>
,	,	kIx,
ASCII	ascii	kA
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
komprimaci	komprimace	k1gFnSc4
<g/>
,	,	kIx,
šifrování	šifrování	k1gNnSc4
<g/>
...	...	k?
</s>
<s>
Vrstva	vrstva	k1gFnSc1
7	#num#	k4
<g/>
:	:	kIx,
Aplikační	aplikační	k2eAgNnSc1d1
</s>
<s>
Naplňuje	naplňovat	k5eAaImIp3nS
význam	význam	k1gInSc4
protokolů	protokol	k1gInPc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
a	a	k8xC
aplikací	aplikace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajišťuje	zajišťovat	k5eAaImIp3nS
věci	věc	k1gFnPc4
jako	jako	k8xS,k8xC
přenos	přenos	k1gInSc4
souborů	soubor	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
FTP	FTP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vzdálený	vzdálený	k2eAgInSc1d1
přístup	přístup	k1gInSc1
<g/>
,	,	kIx,
elektronickou	elektronický	k2eAgFnSc4d1
poštu	pošta	k1gFnSc4
<g/>
...	...	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
OSI	OSI	kA
Reference	reference	k1gFnSc1
Model	model	k1gInSc1
<g/>
,	,	kIx,
Hubert	Hubert	k1gMnSc1
Zimmermann	Zimmermann	k1gMnSc1
<g/>
,	,	kIx,
IEEE	IEEE	kA
Transactions	Transactions	k1gInSc1
on	on	k3xPp3gMnSc1
Communications	Communications	k1gInSc4
<g/>
,	,	kIx,
Vol	vol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
COMM-	COMM-	k1gFnSc1
<g/>
28	#num#	k4
<g/>
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
p	p	k?
<g/>
425	#num#	k4
<g/>
,	,	kIx,
April	April	k1gInSc1
1980	#num#	k4
<g/>
.	.	kIx.
</s>
