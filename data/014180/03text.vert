<s>
Charlotte	Charlotit	k5eAaBmRp2nP,k5eAaImRp2nP,k5eAaPmRp2nP
Pothuis	Pothuis	k1gInSc4
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP
Pothuis	Pothuis	k1gFnSc4
Narození	narození	k1gNnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1867	#num#	k4
Londýn	Londýn	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Amsterdam	Amsterdam	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
rytkyně	rytkyně	k1gFnSc1
leptů	lept	k1gInPc2
<g/>
,	,	kIx,
malířka	malířka	k1gFnSc1
<g/>
,	,	kIx,
litografka	litografka	k1gFnSc1
a	a	k8xC
kreslířka	kreslířka	k1gFnSc1
Příbuzní	příbuzný	k2eAgMnPc1d1
</s>
<s>
Samuel	Samuel	k1gMnSc1
Pothuis	Pothuis	k1gFnSc2
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Charlotte	Charlotte	k5eAaPmIp2nP
Pothuis	Pothuis	k1gFnPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1867	#num#	k4
<g/>
,	,	kIx,
Londýn	Londýn	k1gInSc1
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
<g/>
,	,	kIx,
Amsterdam	Amsterdam	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
holandská	holandský	k2eAgFnSc1d1
malířka	malířka	k1gFnSc1
a	a	k8xC
fotografka	fotografka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Pothuis	Pothuis	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1867	#num#	k4
v	v	k7c6
Londýně	Londýn	k1gInSc6
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Studovala	studovat	k5eAaImAgFnS
na	na	k7c6
Dagtekenschool	Dagtekenschool	k1gFnSc6
voor	voor	k1gMnSc1
meisjes	meisjes	k1gMnSc1
v	v	k7c6
Amersterdamu	Amersterdam	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostala	dostat	k5eAaPmAgFnS
rovněž	rovněž	k9
školení	školení	k1gNnSc4
od	od	k7c2
Henriëtte	Henriëtt	k1gInSc5
Asscher	Asschra	k1gFnPc2
<g/>
,	,	kIx,
Meijer	Meijra	k1gFnPc2
de	de	k?
Haan	Haana	k1gFnPc2
<g/>
,	,	kIx,
Wilhelminy	Wilhelmin	k2eAgFnPc1d1
Cornelie	Cornelie	k1gFnPc1
Kerlenové	Kerlenová	k1gFnSc2
<g/>
,	,	kIx,
Bartola	Bartola	k1gFnSc1
Wilhelma	Wilhelmum	k1gNnSc2
van	vana	k1gFnPc2
Laara	Laar	k1gInSc2
a	a	k8xC
Jana	Jan	k1gMnSc2
Zürchera	Zürcher	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1891	#num#	k4
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c2
umělce	umělec	k1gMnSc2
Karla	Karel	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
Augusta	August	k1gMnSc2
Jana	Jan	k1gMnSc2
Booma	Boom	k1gMnSc2
(	(	kIx(
<g/>
1862	#num#	k4
<g/>
-	-	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
Vereeniging	Vereeniging	k1gInSc4
Sint	Sint	k2eAgInSc4d1
Lucas	Lucas	k1gInSc4
Amsterdam	Amsterdam	k1gInSc1
(	(	kIx(
<g/>
Amsterdamský	amsterdamský	k2eAgInSc1d1
umělecký	umělecký	k2eAgInSc1d1
spolek	spolek	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Lukáše	Lukáš	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
umělecké	umělecký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
Arti	Art	k1gFnSc2
et	et	k?
Amicitiae	Amicitia	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Díla	dílo	k1gNnSc2
Pothuisové	Pothuisový	k2eAgFnSc2d1
byla	být	k5eAaImAgFnS
zahrnuta	zahrnout	k5eAaPmNgFnS
do	do	k7c2
výstavy	výstava	k1gFnSc2
a	a	k8xC
prodeje	prodej	k1gInSc2
Onze	Onz	k1gFnSc2
Kunst	Kunst	k1gInSc1
van	van	k1gInSc1
Heden	Heden	k1gInSc4
(	(	kIx(
<g/>
Naše	náš	k3xOp1gNnSc1
umění	umění	k1gNnSc1
dneška	dnešek	k1gInSc2
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
v	v	k7c4
Rijksmuseum	Rijksmuseum	k1gNnSc4
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dames	Dames	k1gMnSc1
Sluijter	Sluijter	k1gMnSc1
&	&	k?
Boom	boom	k1gInSc1
<g/>
,	,	kIx,
reklama	reklama	k1gFnSc1
<g/>
,	,	kIx,
1896	#num#	k4
</s>
<s>
V	v	k7c6
červnu	červen	k1gInSc6
1896	#num#	k4
otevřela	otevřít	k5eAaPmAgFnS
fotografické	fotografický	k2eAgNnSc4d1
studio	studio	k1gNnSc4
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
kolegyní	kolegyně	k1gFnSc7
Annou	Anna	k1gFnSc7
Sluijter	Sluijter	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nazvali	nazvat	k5eAaPmAgMnP,k5eAaBmAgMnP
své	svůj	k3xOyFgNnSc4
studio	studio	k1gNnSc4
Dames	Dames	k1gMnSc1
Sluijter	Sluijter	k1gMnSc1
&	&	k?
Boom	boom	k1gInSc1
a	a	k8xC
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
jedno	jeden	k4xCgNnSc4
z	z	k7c2
prvních	první	k4xOgNnPc2
ženských	ženský	k2eAgNnPc2d1
fotografických	fotografický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Pothuis	Pothuis	k1gFnSc1
zemřela	zemřít	k5eAaPmAgFnS
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1945	#num#	k4
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
práce	práce	k1gFnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Městského	městský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
Amsterdam	Amsterdam	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Stilleven	Stilleven	k2eAgInSc1d1
met	met	k1gInSc1
dode	dodat	k5eAaPmIp3nS
eend	eend	k6eAd1
<g/>
,	,	kIx,
1917	#num#	k4
</s>
<s>
Im	Im	k?
Café	café	k1gNnSc1
</s>
<s>
Boerderij	Boerderít	k5eAaPmRp2nS
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Charlotte	Charlott	k1gInSc5
Pothuis	Pothuis	k1gFnPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Charlotte	Charlott	k1gMnSc5
Pothuis	Pothuis	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Pothuis	Pothuis	k1gFnPc2
<g/>
,	,	kIx,
Charlotte	Charlott	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Charlotte	Charlott	k1gInSc5
Pothuis	Pothuis	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
nizozemsky	nizozemsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Charlotte	Charlott	k1gInSc5
Boom-Pothuis	Boom-Pothuis	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Onze	Onze	k1gInSc1
kunst	kunst	k1gMnSc1
van	vana	k1gFnPc2
heden	hedna	k1gFnPc2
<g/>
,	,	kIx,
1939	#num#	k4
-	-	kIx~
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Anna	Anna	k1gFnSc1
Sluijter	Sluijter	k1gInSc1
en	en	k?
Charlotte	Charlott	k1gInSc5
Boom	boom	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Charlotte	Charlott	k1gInSc5
Boom-Pothuis	Boom-Pothuis	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Zomeravondstemming	Zomeravondstemming	k1gInSc4
-	-	kIx~
Charlotte	Charlott	k1gMnSc5
Boom-Pothuis	Boom-Pothuis	k1gFnPc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Charlotte	Charlott	k1gInSc5
Pothuis	Pothuis	k1gFnPc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
9169	#num#	k4
8661	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
285551854	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
