<p>
<s>
Ocas	ocas	k1gInSc1	ocas
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
cauda	caud	k1gMnSc2	caud
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
končetina	končetina	k1gFnSc1	končetina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
konci	konec	k1gInSc6	konec
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
pravý	pravý	k2eAgInSc4d1	pravý
ocas	ocas	k1gInSc4	ocas
je	být	k5eAaImIp3nS	být
ocas	ocas	k1gInSc1	ocas
strunatců	strunatec	k1gMnPc2	strunatec
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
nezasahuje	zasahovat	k5eNaImIp3nS	zasahovat
trávicí	trávicí	k2eAgFnSc1d1	trávicí
trubice	trubice	k1gFnSc1	trubice
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
celý	celý	k2eAgMnSc1d1	celý
až	až	k6eAd1	až
za	za	k7c7	za
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
postanální	postanální	k2eAgNnSc1d1	postanální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
však	však	k9	však
zakrnělý	zakrnělý	k2eAgMnSc1d1	zakrnělý
a	a	k8xC	a
zbývá	zbývat	k5eAaImIp3nS	zbývat
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
jen	jen	k6eAd1	jen
kostrč	kostrč	k1gFnSc4	kostrč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šířeji	Šířej	k1gInSc3	Šířej
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc1	termín
označuje	označovat	k5eAaImIp3nS	označovat
veškeré	veškerý	k3xTgInPc4	veškerý
ohebné	ohebný	k2eAgInPc4d1	ohebný
přívěsky	přívěsek	k1gInPc4	přívěsek
připojené	připojený	k2eAgInPc4d1	připojený
k	k	k7c3	k
trupu	trup	k1gInSc3	trup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Ocas	ocas	k1gInSc1	ocas
jako	jako	k8xC	jako
orgán	orgán	k1gInSc1	orgán
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
mnohým	mnohý	k2eAgMnPc3d1	mnohý
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
skladují	skladovat	k5eAaImIp3nP	skladovat
tukové	tukový	k2eAgFnPc1d1	tuková
zásoby	zásoba	k1gFnPc1	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kopytníků	kopytník	k1gInPc2	kopytník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
tur	tur	k1gMnSc1	tur
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odhánění	odhánění	k1gNnSc3	odhánění
obtížného	obtížný	k2eAgInSc2d1	obtížný
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bobrů	bobr	k1gMnPc2	bobr
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Chápani	chápan	k1gMnPc1	chápan
na	na	k7c6	na
ocase	ocas	k1gInSc6	ocas
mají	mít	k5eAaImIp3nP	mít
výběžek	výběžek	k1gInSc4	výběžek
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
ocasem	ocas	k1gInSc7	ocas
mohou	moct	k5eAaImIp3nP	moct
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zachytávat	zachytávat	k5eAaImF	zachytávat
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgMnPc1d1	další
savci	savec	k1gMnPc1	savec
žijícím	žijící	k2eAgFnPc3d1	žijící
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
využívají	využívat	k5eAaImIp3nP	využívat
ocasu	ocas	k1gInSc2	ocas
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
jako	jako	k8xS	jako
kormidlo	kormidlo	k1gNnSc1	kormidlo
při	při	k7c6	při
skákání	skákání	k1gNnSc6	skákání
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
na	na	k7c4	na
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Ocasy	ocas	k1gInPc1	ocas
veverek	veverka	k1gFnPc2	veverka
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
huňaté	huňatý	k2eAgFnPc1d1	huňatá
<g/>
,	,	kIx,	,
že	že	k8xS	že
zachytávají	zachytávat	k5eAaImIp3nP	zachytávat
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
skok	skok	k1gInSc4	skok
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
klokanů	klokan	k1gMnPc2	klokan
slouží	sloužit	k5eAaImIp3nS	sloužit
ocas	ocas	k1gInSc4	ocas
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
-	-	kIx~	-
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
jako	jako	k9	jako
pružina	pružina	k1gFnSc1	pružina
při	při	k7c6	při
skákání	skákání	k1gNnSc6	skákání
<g/>
.	.	kIx.	.
<g/>
Někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
jsou	být	k5eAaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
odvrhnout	odvrhnout	k5eAaPmF	odvrhnout
ocas	ocas	k1gInSc4	ocas
čili	čili	k8xC	čili
provést	provést	k5eAaPmF	provést
tzv.	tzv.	kA	tzv.
kaudální	kaudální	k2eAgFnSc4d1	kaudální
autotomii	autotomie	k1gFnSc4	autotomie
<g/>
.	.	kIx.	.
</s>
<s>
Odvrhnout	odvrhnout	k5eAaPmF	odvrhnout
ocas	ocas	k1gInSc4	ocas
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpeč	k1gFnSc7wB	nebezpeč
schopny	schopen	k2eAgInPc1d1	schopen
konkrétně	konkrétně	k6eAd1	konkrétně
mnozí	mnohý	k2eAgMnPc1d1	mnohý
ocasatí	ocasatý	k2eAgMnPc1d1	ocasatý
obojživelníci	obojživelník	k1gMnPc1	obojživelník
<g/>
,	,	kIx,	,
haterie	haterie	k1gFnPc1	haterie
novozélandská	novozélandský	k2eAgFnSc1d1	novozélandská
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
ještěři	ještěr	k1gMnPc1	ještěr
<g/>
,	,	kIx,	,
amfisbény	amfisbéna	k1gFnPc1	amfisbéna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
několik	několik	k4yIc1	několik
hadů	had	k1gMnPc2	had
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
někteří	některý	k3yIgMnPc1	některý
hlodavci	hlodavec	k1gMnPc1	hlodavec
<g/>
.	.	kIx.	.
<g/>
Pokud	pokud	k8xS	pokud
ocas	ocas	k1gInSc1	ocas
ztratil	ztratit	k5eAaPmAgInS	ztratit
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zakrňuje	zakrňovat	k5eAaImIp3nS	zakrňovat
(	(	kIx(	(
<g/>
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
rudiment	rudiment	k1gInSc1	rudiment
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
kostrč	kostrč	k1gFnSc1	kostrč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
lenochodi	lenochod	k1gMnPc1	lenochod
a	a	k8xC	a
koaly	koala	k1gFnPc1	koala
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neskáčou	skákat	k5eNaImIp3nP	skákat
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
korunách	koruna	k1gFnPc6	koruna
velice	velice	k6eAd1	velice
obezřetně	obezřetně	k6eAd1	obezřetně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ocas	ocas	k1gInSc1	ocas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
ocas	ocas	k1gInSc1	ocas
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
