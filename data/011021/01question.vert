<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
podlouhlá	podlouhlý	k2eAgFnSc1d1	podlouhlá
končetina	končetina	k1gFnSc1	končetina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c6	na
zadním	zadní	k2eAgInSc6d1	zadní
konci	konec	k1gInSc6	konec
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
<g/>
?	?	kIx.	?
</s>
