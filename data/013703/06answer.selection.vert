<s>
U	u	k7c2
rebarbory	rebarbora	k1gFnSc2
se	se	k3xPyFc4
konzumují	konzumovat	k5eAaBmIp3nP
pouze	pouze	k6eAd1
řapíky	řapík	k1gInPc1
listů	list	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tepelně	tepelně	k6eAd1
zpracované	zpracovaný	k2eAgInPc1d1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
syrové	syrový	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Připravují	připravovat	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
mimo	mimo	k6eAd1
jiné	jiný	k2eAgInPc4d1
moučníky	moučník	k1gInPc1
s	s	k7c7
tvarohem	tvaroh	k1gInSc7
<g/>
,	,	kIx,
náplně	náplň	k1gFnPc1
do	do	k7c2
knedlíků	knedlík	k1gMnPc2
<g/>
,	,	kIx,
ovocné	ovocný	k2eAgFnPc1d1
polévky	polévka	k1gFnPc1
<g/>
,	,	kIx,
kompoty	kompot	k1gInPc1
<g/>
,	,	kIx,
zavařeniny	zavařenina	k1gFnPc1
a	a	k8xC
koláče	koláč	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
vaření	vaření	k1gNnSc6
se	se	k3xPyFc4
nedoporučuje	doporučovat	k5eNaImIp3nS
používat	používat	k5eAaImF
hliníkové	hliníkový	k2eAgNnSc4d1
nádobí	nádobí	k1gNnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
hliník	hliník	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
obsaženou	obsažený	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
šťavelovou	šťavelový	k2eAgFnSc7d1
a	a	k8xC
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
zeleniny	zelenina	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
neprospívá	prospívat	k5eNaImIp3nS
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>