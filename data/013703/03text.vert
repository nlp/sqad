<s>
Reveň	reveň	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
</s>
<s>
Reveň	reveň	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
Sklizená	sklizený	k2eAgFnSc1d1
rebarbora	rebarbora	k1gFnSc1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Plantae	Plantae	k1gFnSc1
<g/>
)	)	kIx)
Podříše	podříše	k1gFnSc1
</s>
<s>
cévnaté	cévnatý	k2eAgFnPc1d1
rostliny	rostlina	k1gFnPc1
(	(	kIx(
<g/>
Tracheobionta	Tracheobionta	k1gFnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
krytosemenné	krytosemenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Magnoliophyta	Magnoliophyta	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
vyšší	vysoký	k2eAgFnPc1d2
dvouděložné	dvouděložná	k1gFnPc1
(	(	kIx(
<g/>
Rosopsida	Rosopsida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
hvozdíkotvaré	hvozdíkotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Caryophyllales	Caryophyllales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
rdesnovité	rdesnovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Polygonaceae	Polygonacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
reveň	reveň	k1gFnSc1
(	(	kIx(
<g/>
Rheum	Rheum	k1gNnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Rheum	Rheum	k1gInSc1
rhabarbarumL	rhabarbarumL	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1753	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Květenství	květenství	k1gNnSc1
reveně	reveň	k1gFnSc2
kadeřavé	kadeřavý	k2eAgFnSc2d1
</s>
<s>
Reveň	reveň	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Rheum	Rheum	k1gInSc1
rhabarbarum	rhabarbarum	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
rostlina	rostlina	k1gFnSc1
náležící	náležící	k2eAgFnSc1d1
do	do	k7c2
rodu	rod	k1gInSc2
reveň	reveň	k1gFnSc1
<g/>
,	,	kIx,
čeledi	čeleď	k1gFnPc1
rdesnovité	rdesnovitý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řapíky	řapík	k1gInPc1
listů	list	k1gInPc2
se	se	k3xPyFc4
užívají	užívat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
jménem	jméno	k1gNnSc7
rebarbora	rebarbora	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
názvu	název	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Reveň	reveň	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
také	také	k9
reveň	reveň	k1gFnSc1
vlnovitá	vlnovitý	k2eAgFnSc1d1
nebo	nebo	k8xC
reveň	reveň	k1gFnSc1
rebarbora	rebarbora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Statná	statný	k2eAgFnSc1d1
<g/>
,	,	kIx,
až	až	k9
2	#num#	k4
m	m	kA
vysoká	vysoký	k2eAgFnSc1d1
vytrvalá	vytrvalý	k2eAgFnSc1d1
rostlina	rostlina	k1gFnSc1
s	s	k7c7
přízemními	přízemní	k2eAgInPc7d1
<g/>
,	,	kIx,
50-60	50-60	k4
cm	cm	kA
dlouhými	dlouhý	k2eAgInPc7d1
listy	list	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čepele	čepel	k1gInSc2
listů	list	k1gInPc2
jsou	být	k5eAaImIp3nP
podlouhlé	podlouhlý	k2eAgFnPc1d1
<g/>
,	,	kIx,
vejčité	vejčitý	k2eAgFnPc1d1
a	a	k8xC
výrazně	výrazně	k6eAd1
zkrabacené	zkrabacený	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhý	dlouhý	k2eAgInSc1d1
řapík	řapík	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
líci	líc	k1gFnSc6
vrubovitý	vrubovitý	k2eAgInSc1d1
<g/>
,	,	kIx,
na	na	k7c6
rubu	rub	k1gInSc6
hladký	hladký	k2eAgMnSc1d1
a	a	k8xC
vydutý	vydutý	k2eAgMnSc1d1
<g/>
,	,	kIx,
nahoře	nahoře	k6eAd1
někdy	někdy	k6eAd1
bělavý	bělavý	k2eAgInSc1d1
<g/>
,	,	kIx,
jinak	jinak	k6eAd1
karmínově	karmínově	k6eAd1
červený	červený	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lodyha	lodyha	k1gFnSc1
má	mít	k5eAaImIp3nS
až	až	k9
5	#num#	k4
cm	cm	kA
v	v	k7c6
průměru	průměr	k1gInSc6
a	a	k8xC
nese	nést	k5eAaImIp3nS
bohaté	bohatý	k2eAgNnSc1d1
květenství	květenství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okvětí	okvětí	k1gNnSc1
o	o	k7c6
průměru	průměr	k1gInSc6
až	až	k9
2	#num#	k4
mm	mm	kA
je	být	k5eAaImIp3nS
žluté	žlutý	k2eAgFnPc4d1
<g/>
,	,	kIx,
šestičetné	šestičetný	k2eAgFnPc4d1
a	a	k8xC
opadávající	opadávající	k2eAgFnPc4d1
<g/>
,	,	kIx,
tyčinek	tyčinka	k1gFnPc2
je	být	k5eAaImIp3nS
9	#num#	k4
ve	v	k7c6
dvou	dva	k4xCgInPc6
kruzích	kruh	k1gInPc6
<g/>
,	,	kIx,
čnělky	čnělka	k1gFnPc1
zpravidla	zpravidla	k6eAd1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvete	kvést	k5eAaImIp3nS
v	v	k7c6
červnu	červen	k1gInSc6
a	a	k8xC
červenci	červenec	k1gInSc6
<g/>
,	,	kIx,
plod	plod	k1gInSc4
je	být	k5eAaImIp3nS
vejčitý	vejčitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
se	s	k7c7
2-4	2-4	k4
křídly	křídlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pochází	pocházet	k5eAaImIp3nS
ze	z	k7c2
Střední	střední	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
(	(	kIx(
<g/>
Mongolsko	Mongolsko	k1gNnSc1
<g/>
,	,	kIx,
Sibiř	Sibiř	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
dávno	dávno	k6eAd1
pěstována	pěstován	k2eAgFnSc1d1
i	i	k9
na	na	k7c6
polích	pole	k1gNnPc6
<g/>
,	,	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
většinou	většinou	k6eAd1
na	na	k7c6
zahrádkách	zahrádka	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Použití	použití	k1gNnSc1
</s>
<s>
U	u	k7c2
rebarbory	rebarbora	k1gFnSc2
se	se	k3xPyFc4
konzumují	konzumovat	k5eAaBmIp3nP
pouze	pouze	k6eAd1
řapíky	řapík	k1gInPc1
listů	list	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
tepelně	tepelně	k6eAd1
zpracované	zpracovaný	k2eAgInPc1d1
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
syrové	syrový	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Připravují	připravovat	k5eAaImIp3nP
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
mimo	mimo	k6eAd1
jiné	jiný	k2eAgInPc4d1
moučníky	moučník	k1gInPc1
s	s	k7c7
tvarohem	tvaroh	k1gInSc7
<g/>
,	,	kIx,
náplně	náplň	k1gFnPc1
do	do	k7c2
knedlíků	knedlík	k1gMnPc2
<g/>
,	,	kIx,
ovocné	ovocný	k2eAgFnPc1d1
polévky	polévka	k1gFnPc1
<g/>
,	,	kIx,
kompoty	kompot	k1gInPc1
<g/>
,	,	kIx,
zavařeniny	zavařenina	k1gFnPc1
a	a	k8xC
koláče	koláč	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
vaření	vaření	k1gNnSc6
se	se	k3xPyFc4
nedoporučuje	doporučovat	k5eNaImIp3nS
používat	používat	k5eAaImF
hliníkové	hliníkový	k2eAgNnSc4d1
nádobí	nádobí	k1gNnSc4
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
hliník	hliník	k1gInSc1
reaguje	reagovat	k5eAaBmIp3nS
s	s	k7c7
obsaženou	obsažený	k2eAgFnSc7d1
kyselinou	kyselina	k1gFnSc7
šťavelovou	šťavelový	k2eAgFnSc7d1
a	a	k8xC
přechází	přecházet	k5eAaImIp3nS
do	do	k7c2
zeleniny	zelenina	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
neprospívá	prospívat	k5eNaImIp3nS
zdraví	zdraví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Toxicita	toxicita	k1gFnSc1
</s>
<s>
Zelené	Zelené	k2eAgInPc1d1
listové	listový	k2eAgInPc1d1
čepele	čepel	k1gInPc1
rebarbory	rebarbora	k1gFnSc2
obsahují	obsahovat	k5eAaImIp3nP
jedovaté	jedovatý	k2eAgFnPc1d1
látky	látka	k1gFnPc1
včetně	včetně	k7c2
kyseliny	kyselina	k1gFnSc2
šťavelové	šťavelový	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
nefrotoxická	frotoxický	k2eNgFnSc1d1
a	a	k8xC
korozivní	korozivní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
popsány	popsán	k2eAgFnPc1d1
otravy	otrava	k1gFnPc1
lidí	člověk	k1gMnPc2
po	po	k7c6
požití	požití	k1gNnSc6
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smrtelné	smrtelný	k2eAgInPc1d1
následky	následek	k1gInPc1
konzumace	konzumace	k1gFnSc2
byly	být	k5eAaImAgInP
problémem	problém	k1gInSc7
zejména	zejména	k9
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgFnP
listy	lista	k1gFnPc1
doporučovány	doporučovat	k5eAaImNgFnP
ke	k	k7c3
konzumaci	konzumace	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
J.	J.	kA
Dostál	Dostál	k1gMnSc1
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Květena	květena	k1gFnSc1
ČSR	ČSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1950	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
490.1	490.1	k4
2	#num#	k4
MÜLLEROVÁ	MÜLLEROVÁ	kA
<g/>
,	,	kIx,
Eva	Eva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rebarbora	rebarbora	k1gFnSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
kulinářské	kulinářský	k2eAgFnSc2d1
magie	magie	k1gFnSc2
ingredience	ingredience	k1gFnSc2
lásky	láska	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2009-5-29	2009-5-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
rebarbora	rebarbora	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vareni	Varen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
čeština	čeština	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Robb	Robb	k1gInSc1
<g/>
,	,	kIx,
H.	H.	kA
F.	F.	kA
1919	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Death	Death	k1gMnSc1
from	from	k1gMnSc1
rhubarb	rhubarb	k1gMnSc1
leaves	leaves	k1gMnSc1
due	due	k?
to	ten	k3xDgNnSc1
oxalic	oxalice	k1gFnPc2
acid	acida	k1gFnPc2
poisoning	poisoning	k1gInSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
J.	J.	kA
Am	Am	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Med	med	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Assoc	Assoc	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
73	#num#	k4
<g/>
:	:	kIx,
627	#num#	k4
<g/>
-	-	kIx~
<g/>
628	#num#	k4
<g/>
↑	↑	k?
Cooper	Cooper	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
R.	R.	kA
<g/>
,	,	kIx,
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
A.	A.	kA
W.	W.	kA
1984	#num#	k4
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Poisonous	Poisonous	k1gMnSc1
plants	plantsa	k1gFnPc2
in	in	k?
Britain	Britain	k1gMnSc1
and	and	k?
their	their	k1gMnSc1
effects	effects	k6eAd1
on	on	k3xPp3gMnSc1
animals	animals	k6eAd1
and	and	k?
man	man	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Her	hra	k1gFnPc2
Majesty	Majest	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Stationery	Stationer	k1gMnPc7
Office	Office	kA
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
England	England	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
J.	J.	kA
Dostál	Dostál	k1gMnSc1
(	(	kIx(
<g/>
red	red	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Květena	květena	k1gFnSc1
ČSR	ČSR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1950	#num#	k4
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
490	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
reveň	reveň	k1gFnSc1
kadeřavá	kadeřavý	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
The	The	k1gMnSc1
Rhubarb	Rhubarb	k1gMnSc1
Compendium	Compendium	k1gNnSc4
</s>
<s>
Na	na	k7c6
slovníku	slovník	k1gInSc6
vaření	vaření	k1gNnSc1
</s>
<s>
Článek	článek	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Lidovky	Lidovky	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Zelenina	zelenina	k1gFnSc1
cibulová	cibulový	k2eAgFnSc1d1
</s>
<s>
cibule	cibule	k1gFnSc1
•	•	k?
česnek	česnek	k1gInSc1
•	•	k?
pažitka	pažitka	k1gFnSc1
•	•	k?
pórek	pórek	k1gInSc1
kořenová	kořenový	k2eAgFnSc5d1
</s>
<s>
celer	celer	k1gInSc1
•	•	k?
černý	černý	k2eAgInSc1d1
kořen	kořen	k1gInSc1
•	•	k?
křen	křen	k1gInSc1
•	•	k?
mrkev	mrkev	k1gFnSc1
•	•	k?
pastinák	pastinák	k1gInSc1
•	•	k?
petržel	petržel	k1gFnSc1
•	•	k?
ředkev	ředkev	k1gFnSc1
•	•	k?
ředkvička	ředkvička	k1gFnSc1
•	•	k?
řepa	řepa	k1gFnSc1
•	•	k?
tuřín	tuřín	k1gInSc1
•	•	k?
vodnice	vodnice	k1gFnSc1
hlíznatá	hlíznatý	k2eAgFnSc1d1
</s>
<s>
batáty	batáty	k1gInPc1
•	•	k?
brambory	brambora	k1gFnSc2
•	•	k?
maniok	maniok	k1gInSc1
•	•	k?
topinambur	topinambur	k1gInSc1
</s>
<s>
košťálová	košťálový	k2eAgFnSc1d1
</s>
<s>
artyčok	artyčok	k1gInSc1
•	•	k?
brokolice	brokolice	k1gFnSc2
•	•	k?
hlávková	hlávkový	k2eAgFnSc1d1
kapusta	kapusta	k1gFnSc1
•	•	k?
kedluben	kedluben	k1gInSc1
•	•	k?
květák	květák	k1gInSc1
•	•	k?
romanesco	romanesco	k6eAd1
•	•	k?
růžičková	růžičkový	k2eAgFnSc1d1
kapusta	kapusta	k1gFnSc1
•	•	k?
zelí	zelí	k1gNnSc1
(	(	kIx(
<g/>
čínské	čínský	k2eAgFnPc1d1
•	•	k?
hlávkové	hlávkový	k2eAgFnPc1d1
•	•	k?
pekingské	pekingský	k2eAgFnPc1d1
<g/>
)	)	kIx)
listová	listový	k2eAgFnSc1d1
</s>
<s>
celerová	celerový	k2eAgFnSc1d1
nať	nať	k1gFnSc1
•	•	k?
čekanka	čekanka	k1gFnSc1
•	•	k?
hlávkový	hlávkový	k2eAgInSc4d1
salát	salát	k1gInSc4
•	•	k?
chřest	chřest	k1gInSc1
•	•	k?
mangold	mangold	k1gInSc1
•	•	k?
petrželová	petrželový	k2eAgFnSc1d1
nať	nať	k1gFnSc1
•	•	k?
rebarbora	rebarbora	k1gFnSc1
•	•	k?
roketa	roket	k2eAgFnSc1d1
•	•	k?
řeřicha	řeřicha	k1gFnSc1
•	•	k?
římský	římský	k2eAgInSc1d1
salát	salát	k1gInSc1
•	•	k?
špenát	špenát	k1gInSc1
lusková	luskový	k2eAgNnPc5d1
</s>
<s>
bob	bob	k1gInSc1
•	•	k?
cizrna	cizrna	k1gFnSc1
•	•	k?
čočka	čočka	k1gFnSc1
•	•	k?
fazol	fazol	k1gInSc1
•	•	k?
hrách	hrách	k1gInSc1
•	•	k?
sója	sója	k1gFnSc1
plodová	plodový	k2eAgFnSc1d1
</s>
<s>
baklažán	baklažán	k1gInSc1
•	•	k?
cuketa	cuketa	k1gFnSc1
•	•	k?
dýně	dýně	k1gFnSc2
•	•	k?
kiwano	kiwana	k1gFnSc5
•	•	k?
meloun	meloun	k1gInSc1
•	•	k?
okurka	okurka	k1gFnSc1
•	•	k?
paprika	paprika	k1gMnSc1
•	•	k?
patizon	patizon	k1gMnSc1
•	•	k?
rajče	rajče	k1gNnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnPc1
|	|	kIx~
Rostliny	rostlina	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4177974-5	4177974-5	k4
</s>
