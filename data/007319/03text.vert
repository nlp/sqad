<s>
Oidipovský	oidipovský	k2eAgInSc1d1	oidipovský
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
Oidipův	Oidipův	k2eAgInSc1d1	Oidipův
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
psychoanalýze	psychoanalýza	k1gFnSc6	psychoanalýza
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
milostného	milostný	k2eAgInSc2d1	milostný
vztahu	vztah	k1gInSc2	vztah
syna	syn	k1gMnSc2	syn
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
Oidipovi	Oidipus	k1gMnSc6	Oidipus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
<g/>
,	,	kIx,	,
neznalý	znalý	k2eNgMnSc1d1	neznalý
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
(	(	kIx(	(
<g/>
krále	král	k1gMnSc4	král
Laia	Laius	k1gMnSc4	Laius
<g/>
,	,	kIx,	,
Laios	Laios	k1gMnSc1	Laios
ho	on	k3xPp3gMnSc4	on
přitom	přitom	k6eAd1	přitom
nechal	nechat	k5eAaPmAgMnS	nechat
pohodit	pohodit	k5eAaPmF	pohodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
věštba	věštba	k1gFnSc1	věštba
prorokovala	prorokovat	k5eAaImAgFnS	prorokovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
<g/>
)	)	kIx)	)
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
matkou	matka	k1gFnSc7	matka
(	(	kIx(	(
<g/>
Iokastou	Iokasta	k1gFnSc7	Iokasta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Problematiku	problematika	k1gFnSc4	problematika
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
a	a	k8xC	a
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
psychoanalytik	psychoanalytik	k1gMnSc1	psychoanalytik
Sigmund	Sigmund	k1gMnSc1	Sigmund
Freud	Freud	k1gMnSc1	Freud
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
analogický	analogický	k2eAgInSc4d1	analogický
vztah	vztah	k1gInSc4	vztah
dcery	dcera	k1gFnSc2	dcera
a	a	k8xC	a
otce	otec	k1gMnSc2	otec
se	se	k3xPyFc4	se
zřídka	zřídka	k6eAd1	zřídka
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
Elektřin	elektřina	k1gFnPc2	elektřina
komplex	komplex	k1gInSc1	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
zavedl	zavést	k5eAaPmAgMnS	zavést
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
patologie	patologie	k1gFnSc2	patologie
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
neuróz	neuróza	k1gFnPc2	neuróza
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
oidipovská	oidipovský	k2eAgFnSc1d1	oidipovská
a	a	k8xC	a
kastrační	kastrační	k2eAgFnSc1d1	kastrační
problematika	problematika	k1gFnSc1	problematika
stěžejní	stěžejní	k2eAgFnSc1d1	stěžejní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
momentů	moment	k1gInPc2	moment
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
oidipovská	oidipovský	k2eAgFnSc1d1	oidipovská
problematika	problematika	k1gFnSc1	problematika
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
normě	norma	k1gFnSc6	norma
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
dlí	dlít	k5eAaImIp3nS	dlít
v	v	k7c6	v
nevědomí	nevědomí	k1gNnSc6	nevědomí
a	a	k8xC	a
její	její	k3xOp3gMnSc1	její
nositel	nositel	k1gMnSc1	nositel
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
volba	volba	k1gFnSc1	volba
partnera	partner	k1gMnSc2	partner
<g/>
:	:	kIx,	:
zcela	zcela	k6eAd1	zcela
obecně	obecně	k6eAd1	obecně
Freud	Freud	k1gMnSc1	Freud
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
partnera	partner	k1gMnSc4	partner
si	se	k3xPyFc3	se
vybíráme	vybírat	k5eAaImIp1nP	vybírat
podle	podle	k7c2	podle
rodiče	rodič	k1gMnSc2	rodič
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
dědictví	dědictví	k1gNnSc6	dědictví
oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
je	být	k5eAaImIp3nS	být
problematická	problematický	k2eAgFnSc1d1	problematická
volba	volba	k1gFnSc1	volba
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
poškozený	poškozený	k1gMnSc1	poškozený
třetí	třetí	k4xOgMnSc1	třetí
<g/>
"	"	kIx"	"
a	a	k8xC	a
volba	volba	k1gFnSc1	volba
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
světice	světice	k1gFnSc1	světice
<g/>
"	"	kIx"	"
versus	versus	k7c1	versus
"	"	kIx"	"
<g/>
děvky	děvka	k1gFnPc4	děvka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
singulár	singulár	k1gInSc1	singulár
versus	versus	k7c1	versus
plurál	plurál	k1gInSc1	plurál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
cenná	cenný	k2eAgFnSc1d1	cenná
jen	jen	k9	jen
ta	ten	k3xDgFnSc1	ten
partnerka	partnerka	k1gFnSc1	partnerka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
partnera	partner	k1gMnSc4	partner
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
atakovat	atakovat	k5eAaBmF	atakovat
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
oidipovských	oidipovský	k2eAgNnPc2d1	oidipovské
přání	přání	k1gNnPc2	přání
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
cílem	cíl	k1gInSc7	cíl
zcela	zcela	k6eAd1	zcela
mimo	mimo	k7c4	mimo
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
)	)	kIx)	)
vztah	vztah	k1gInSc4	vztah
dvou	dva	k4xCgMnPc2	dva
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
získat	získat	k5eAaPmF	získat
oidipovský	oidipovský	k2eAgInSc4d1	oidipovský
triumf	triumf	k1gInSc4	triumf
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
podkladem	podklad	k1gInSc7	podklad
<g/>
,	,	kIx,	,
nevědomým	vědomý	k2eNgInSc7d1	nevědomý
zdrojem	zdroj	k1gInSc7	zdroj
příběhů	příběh	k1gInPc2	příběh
Dona	Don	k1gMnSc4	Don
Juana	Juan	k1gMnSc4	Juan
a	a	k8xC	a
Casanovy	Casanův	k2eAgMnPc4d1	Casanův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
svatý	svatý	k2eAgMnSc1d1	svatý
a	a	k8xC	a
nelze	lze	k6eNd1	lze
jej	on	k3xPp3gNnSc4	on
poskvrnit	poskvrnit	k5eAaPmF	poskvrnit
sexem	sex	k1gInSc7	sex
a	a	k8xC	a
spermatem	sperma	k1gNnSc7	sperma
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
k	k	k7c3	k
plození	plození	k1gNnSc3	plození
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
anonymní	anonymní	k2eAgInPc4d1	anonymní
vztahy	vztah	k1gInPc4	vztah
nebo	nebo	k8xC	nebo
milostné	milostný	k2eAgFnPc4d1	milostná
pletky	pletka	k1gFnPc4	pletka
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
ženami	žena	k1gFnPc7	žena
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
plné	plný	k2eAgNnSc4d1	plné
pestrých	pestrý	k2eAgFnPc2d1	pestrá
sexuálních	sexuální	k2eAgFnPc2d1	sexuální
praktik	praktika	k1gFnPc2	praktika
–	–	k?	–
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
prvek	prvek	k1gInSc1	prvek
idealizace	idealizace	k1gFnSc2	idealizace
světice	světice	k1gFnSc2	světice
<g/>
,	,	kIx,	,
přenesený	přenesený	k2eAgInSc4d1	přenesený
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
a	a	k8xC	a
devalvace	devalvace	k1gFnSc1	devalvace
žen	žena	k1gFnPc2	žena
obecně	obecně	k6eAd1	obecně
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
stav	stav	k1gInSc4	stav
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
kastrovanosti	kastrovanost	k1gFnPc1	kastrovanost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
absence	absence	k1gFnSc1	absence
penisu	penis	k1gInSc2	penis
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
prvek	prvek	k1gInSc1	prvek
devalvace	devalvace	k1gFnSc2	devalvace
sexu	sex	k1gInSc2	sex
jako	jako	k9	jako
análního	anální	k2eAgNnSc2d1	anální
fenomenu	fenomenout	k5eAaPmIp1nS	fenomenout
(	(	kIx(	(
<g/>
ne	ne	k9	ne
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
análního	anální	k2eAgInSc2d1	anální
koitu	koitus	k1gInSc2	koitus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
něčeho	něco	k3yInSc2	něco
špinavého	špinavý	k2eAgNnSc2d1	špinavé
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
slasti	slast	k1gFnSc2	slast
<g/>
,	,	kIx,	,
sex	sex	k1gInSc4	sex
jako	jako	k8xS	jako
výraz	výraz	k1gInSc4	výraz
hry	hra	k1gFnSc2	hra
s	s	k7c7	s
fekáliemi	fekálie	k1gFnPc7	fekálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
nemožnost	nemožnost	k1gFnSc4	nemožnost
předstihnout	předstihnout	k5eAaPmF	předstihnout
svého	svůj	k3xOyFgMnSc4	svůj
rodiče	rodič	k1gMnSc4	rodič
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc4d1	projevující
se	se	k3xPyFc4	se
jako	jako	k9	jako
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
uplatněné	uplatněný	k2eAgFnPc1d1	uplatněná
a	a	k8xC	a
využití	využití	k1gNnSc1	využití
svého	svůj	k3xOyFgNnSc2	svůj
nadání	nadání	k1gNnSc2	nadání
ve	v	k7c4	v
vlastní	vlastní	k2eAgInSc4d1	vlastní
prospěch	prospěch	k1gInSc4	prospěch
a	a	k8xC	a
odborný	odborný	k2eAgInSc4d1	odborný
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
klient	klient	k1gMnSc1	klient
vyzbrojí	vyzbrojit	k5eAaPmIp3nS	vyzbrojit
svými	svůj	k3xOyFgFnPc7	svůj
znalostmi	znalost	k1gFnPc7	znalost
a	a	k8xC	a
dovednostmi	dovednost	k1gFnPc7	dovednost
kolegy	kolega	k1gMnSc2	kolega
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sám	sám	k3xTgMnSc1	sám
není	být	k5eNaImIp3nS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
<g/>
,	,	kIx,	,
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
vědecké	vědecký	k2eAgFnPc4d1	vědecká
hodnosti	hodnost	k1gFnPc4	hodnost
–	–	k?	–
prototypem	prototyp	k1gInSc7	prototyp
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
suplující	suplující	k2eAgMnSc1d1	suplující
profesor	profesor	k1gMnSc1	profesor
biologie	biologie	k1gFnSc2	biologie
v	v	k7c6	v
Žákově	Žákův	k2eAgFnSc6d1	Žákova
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
hlubin	hlubina	k1gFnPc2	hlubina
študákovy	študákův	k2eAgFnSc2d1	študákův
duše	duše	k1gFnSc2	duše
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc4	jeho
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
sám	sám	k3xTgMnSc1	sám
neumí	umět	k5eNaImIp3nS	umět
prodat	prodat	k5eAaPmF	prodat
<g/>
,	,	kIx,	,
uplatnit	uplatnit	k5eAaPmF	uplatnit
<g/>
,	,	kIx,	,
využít	využít	k5eAaPmF	využít
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaPmIp3nP	využívat
jen	jen	k9	jen
jeho	jeho	k3xOp3gMnPc1	jeho
studenti	student	k1gMnPc1	student
–	–	k?	–
nicméně	nicméně	k8xC	nicméně
nakonec	nakonec	k6eAd1	nakonec
pod	pod	k7c7	pod
fingovaným	fingovaný	k2eAgInSc7d1	fingovaný
prodejem	prodej	k1gInSc7	prodej
sbírky	sbírka	k1gFnSc2	sbírka
přírodnin	přírodnina	k1gFnPc2	přírodnina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
členové	člen	k1gMnPc1	člen
zkušební	zkušební	k2eAgFnSc2d1	zkušební
komise	komise	k1gFnSc2	komise
nabízejí	nabízet	k5eAaImIp3nP	nabízet
udičky	udička	k1gFnPc1	udička
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
nesmyslných	smyslný	k2eNgFnPc2d1	nesmyslná
promluv	promluva	k1gFnPc2	promluva
o	o	k7c6	o
přírodninách	přírodnina	k1gFnPc6	přírodnina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
uvede	uvést	k5eAaPmIp3nS	uvést
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
profesorského	profesorský	k2eAgNnSc2d1	profesorské
místa	místo	k1gNnSc2	místo
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
nemohl	moct	k5eNaImAgMnS	moct
dost	dost	k6eAd1	dost
idealisovat	idealisovat	k5eAaBmF	idealisovat
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
nedostačivý	dostačivý	k2eNgMnSc1d1	nedostačivý
<g/>
,	,	kIx,	,
selhával	selhávat	k5eAaImAgMnS	selhávat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
syn	syn	k1gMnSc1	syn
nemohl	moct	k5eNaImAgMnS	moct
utvořit	utvořit	k5eAaPmF	utvořit
prožitek	prožitek	k1gInSc1	prožitek
oprávněné	oprávněný	k2eAgFnSc2d1	oprávněná
a	a	k8xC	a
přiměřené	přiměřený	k2eAgFnSc2d1	přiměřená
rivality	rivalita	k1gFnSc2	rivalita
<g/>
,	,	kIx,	,
nemohl	moct	k5eNaImAgMnS	moct
být	být	k5eAaImF	být
lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
nedostačivý	dostačivý	k2eNgMnSc1d1	nedostačivý
<g/>
,	,	kIx,	,
selhávající	selhávající	k2eAgMnSc1d1	selhávající
otec	otec	k1gMnSc1	otec
–	–	k?	–
uchránil	uchránit	k5eAaPmAgMnS	uchránit
se	se	k3xPyFc4	se
oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
triumfu	triumf	k1gInSc2	triumf
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
že	že	k8xS	že
setrvá	setrvat	k5eAaPmIp3nS	setrvat
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
nedostačivosti	nedostačivost	k1gFnSc2	nedostačivost
<g/>
,	,	kIx,	,
selhávání	selhávání	k1gNnSc1	selhávání
otce	otec	k1gMnSc2	otec
(	(	kIx(	(
<g/>
ať	ať	k9	ať
je	být	k5eAaImIp3nS	být
selhávání	selhávání	k1gNnSc1	selhávání
nebo	nebo	k8xC	nebo
nedostačivost	nedostačivost	k1gFnSc1	nedostačivost
reálná	reálný	k2eAgFnSc1d1	reálná
nebo	nebo	k8xC	nebo
domnělá	domnělý	k2eAgFnSc1d1	domnělá
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
obraz	obraz	k1gInSc4	obraz
v	v	k7c6	v
chlapcově	chlapcův	k2eAgFnSc6d1	chlapcova
mysli	mysl	k1gFnSc6	mysl
<g/>
,	,	kIx,	,
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
nevědomé	vědomý	k2eNgFnSc6d1	nevědomá
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oidipovská	oidipovský	k2eAgFnSc1d1	oidipovská
tematika	tematika	k1gFnSc1	tematika
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nevědomým	vědomý	k2eNgInSc7d1	nevědomý
zdrojem	zdroj	k1gInSc7	zdroj
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Dostojevského	Dostojevský	k2eAgInSc2d1	Dostojevský
románu	román	k1gInSc2	román
Bratří	bratřit	k5eAaImIp3nP	bratřit
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
(	(	kIx(	(
<g/>
téma	téma	k1gNnSc7	téma
otcovraždy	otcovražda	k1gFnSc2	otcovražda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shakespearova	Shakespearův	k2eAgMnSc2d1	Shakespearův
Hamleta	Hamlet	k1gMnSc2	Hamlet
(	(	kIx(	(
<g/>
Claudius	Claudius	k1gMnSc1	Claudius
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
Hamletova	Hamletův	k2eAgMnSc2d1	Hamletův
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
representuje	representovat	k5eAaImIp3nS	representovat
vražedně	vražedně	k6eAd1	vražedně
agresivní	agresivní	k2eAgFnPc4d1	agresivní
složky	složka	k1gFnPc4	složka
oidipovského	oidipovský	k2eAgInSc2d1	oidipovský
komplexu	komplex	k1gInSc2	komplex
Hamletova	Hamletův	k2eAgFnSc1d1	Hamletova
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
potom	potom	k8xC	potom
zrající	zrající	k2eAgMnSc1d1	zrající
Superego	Superego	k6eAd1	Superego
spějící	spějící	k2eAgMnSc1d1	spějící
k	k	k7c3	k
pomstě	pomsta	k1gFnSc3	pomsta
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
potrestání	potrestání	k1gNnSc4	potrestání
viny	vina	k1gFnSc2	vina
<g/>
,	,	kIx,	,
také	také	k9	také
proto	proto	k8xC	proto
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
dospělé	dospělý	k2eAgFnSc2d1	dospělá
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zatížen	zatížit	k5eAaPmNgInS	zatížit
vinou	vina	k1gFnSc7	vina
–	–	k?	–
Ofélie	Ofélie	k1gFnSc1	Ofélie
zešílí	zešílet	k5eAaPmIp3nS	zešílet
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
autor	autor	k1gMnSc1	autor
projekcí	projekce	k1gFnPc2	projekce
zbavil	zbavit	k5eAaPmAgMnS	zbavit
vražedně	vražedně	k6eAd1	vražedně
agresivních	agresivní	k2eAgFnPc2d1	agresivní
tendencí	tendence	k1gFnPc2	tendence
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
převzal	převzít	k5eAaPmAgMnS	převzít
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
a	a	k8xC	a
bratr	bratr	k1gMnSc1	bratr
zavražděného	zavražděný	k2eAgMnSc2d1	zavražděný
Claudius	Claudius	k1gMnSc1	Claudius
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
neunikne	uniknout	k5eNaPmIp3nS	uniknout
<g/>
:	:	kIx,	:
zavraždí	zavraždit	k5eAaPmIp3nS	zavraždit
otce	otec	k1gMnPc4	otec
Ofelie	Ofelie	k1gFnSc2	Ofelie
<g/>
,	,	kIx,	,
Polonia	polonium	k1gNnSc2	polonium
<g/>
.	.	kIx.	.
</s>
<s>
Claudius	Claudius	k1gInSc1	Claudius
ovšem	ovšem	k9	ovšem
representuje	representovat	k5eAaImIp3nS	representovat
zároveň	zároveň	k6eAd1	zároveň
naplnění	naplnění	k1gNnSc1	naplnění
Hamletových	Hamletův	k2eAgNnPc2d1	Hamletovo
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
projikovaných	projikovaný	k2eAgNnPc2d1	projikované
přání	přání	k1gNnPc2	přání
incestního	incestní	k2eAgInSc2d1	incestní
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
vezme	vzít	k5eAaPmIp3nS	vzít
si	se	k3xPyFc3	se
ženu	žena	k1gFnSc4	žena
zavražděného	zavražděný	k2eAgMnSc4d1	zavražděný
krále	král	k1gMnSc4	král
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oidipovský	oidipovský	k2eAgInSc1d1	oidipovský
triumf	triumf	k1gInSc1	triumf
je	být	k5eAaImIp3nS	být
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
syn	syn	k1gMnSc1	syn
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
mysli	mysl	k1gFnSc6	mysl
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
<g/>
,	,	kIx,	,
zničí	zničit	k5eAaPmIp3nS	zničit
otce	otec	k1gMnSc4	otec
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
matku	matka	k1gFnSc4	matka
<g/>
:	:	kIx,	:
zdůrazňujeme	zdůrazňovat	k5eAaImIp1nP	zdůrazňovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
mysli	mysl	k1gFnSc6	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
tomu	ten	k3xDgMnSc3	ten
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
otec	otec	k1gMnSc1	otec
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
/	/	kIx~	/
<g/>
musí	muset	k5eAaImIp3nS	muset
odejít	odejít	k5eAaPmF	odejít
<g/>
/	/	kIx~	/
<g/>
chce	chtít	k5eAaImIp3nS	chtít
odejít	odejít	k5eAaPmF	odejít
od	od	k7c2	od
matky	matka	k1gFnSc2	matka
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
pije	pít	k5eAaImIp3nS	pít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jinak	jinak	k6eAd1	jinak
submisivní	submisivní	k2eAgMnSc1d1	submisivní
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
formě	forma	k1gFnSc6	forma
nepřítomen	přítomen	k2eNgInSc1d1	nepřítomen
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
má	mít	k5eAaImIp3nS	mít
obecně	obecně	k6eAd1	obecně
význam	význam	k1gInSc4	význam
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
především	především	k6eAd1	především
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
chlapce	chlapec	k1gMnSc4	chlapec
nebo	nebo	k8xC	nebo
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
vysvobozuje	vysvobozovat	k5eAaImIp3nS	vysvobozovat
ze	z	k7c2	z
zajetí	zajetí	k1gNnSc2	zajetí
dyády	dyáda	k1gFnSc2	dyáda
(	(	kIx(	(
<g/>
vztahu	vztah	k1gInSc2	vztah
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
členech	člen	k1gInPc6	člen
<g/>
)	)	kIx)	)
dítě	dítě	k1gNnSc4	dítě
<g/>
/	/	kIx~	/
<g/>
matka	matka	k1gFnSc1	matka
(	(	kIx(	(
<g/>
vysvobozuje	vysvobozovat	k5eAaImIp3nS	vysvobozovat
i	i	k9	i
matku	matka	k1gFnSc4	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
otvírá	otvírat	k5eAaImIp3nS	otvírat
perspektivu	perspektiva	k1gFnSc4	perspektiva
–	–	k?	–
pokud	pokud	k8xS	pokud
<g />
.	.	kIx.	.
</s>
<s>
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
nabídka	nabídka	k1gFnSc1	nabídka
správně	správně	k6eAd1	správně
načasována	načasován	k2eAgFnSc1d1	načasována
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vývoj	vývoj	k1gInSc1	vývoj
dyády	dyáda	k1gFnSc2	dyáda
matka	matka	k1gFnSc1	matka
<g/>
/	/	kIx~	/
<g/>
dítě	dítě	k1gNnSc1	dítě
děl	dělo	k1gNnPc2	dělo
přiměřeným	přiměřený	k2eAgNnSc7d1	přiměřené
tempem	tempo	k1gNnSc7	tempo
(	(	kIx(	(
<g/>
selhání	selhání	k1gNnSc1	selhání
<g/>
:	:	kIx,	:
kupř	kupř	kA	kupř
<g/>
.	.	kIx.	.
viz	vidět	k5eAaImRp2nS	vidět
těsná	těsný	k2eAgFnSc1d1	těsná
vazba	vazba	k1gFnSc1	vazba
matky	matka	k1gFnSc2	matka
s	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
znázorněná	znázorněný	k2eAgFnSc1d1	znázorněná
K.	K.	kA	K.
J.	J.	kA	J.
Erbenem	Erben	k1gMnSc7	Erben
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Vodník	vodník	k1gMnSc1	vodník
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
muž	muž	k1gMnSc1	muž
vylíčen	vylíčit	k5eAaPmNgMnS	vylíčit
jako	jako	k9	jako
vrah	vrah	k1gMnSc1	vrah
<g/>
,	,	kIx,	,
volba	volba	k1gFnSc1	volba
dcery	dcera	k1gFnSc2	dcera
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedbá	dbát	k5eNaImIp3nS	dbát
varování	varování	k1gNnSc4	varování
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
puzení	puzení	k1gNnSc4	puzení
jít	jít	k5eAaImF	jít
za	za	k7c7	za
svým	svůj	k3xOyFgNnSc7	svůj
oidipským	oidipský	k2eAgNnSc7d1	oidipské
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
snad	snad	k9	snad
i	i	k9	i
dospělým	dospělý	k2eAgNnSc7d1	dospělé
přáním	přání	k1gNnSc7	přání
<g/>
,	,	kIx,	,
znehodnocena	znehodnocen	k2eAgFnSc1d1	znehodnocena
jeho	jeho	k3xOp3gFnSc7	jeho
zákeřností	zákeřnost	k1gFnSc7	zákeřnost
<g/>
.	.	kIx.	.
</s>
<s>
Dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
Vodníkova	vodníkův	k2eAgFnSc1d1	Vodníkova
manželka	manželka	k1gFnSc1	manželka
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
zradí	zradit	k5eAaPmIp3nP	zradit
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
málo	málo	k6eAd1	málo
pochopení	pochopení	k1gNnSc4	pochopení
pro	pro	k7c4	pro
zoufalství	zoufalství	k1gNnSc4	zoufalství
zrazeného	zrazený	k2eAgMnSc2d1	zrazený
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
málo	málo	k6eAd1	málo
pochopení	pochopení	k1gNnSc4	pochopení
pro	pro	k7c4	pro
zoufalství	zoufalství	k1gNnPc4	zoufalství
dyády	dyáda	k1gFnSc2	dyáda
matka	matka	k1gFnSc1	matka
<g/>
/	/	kIx~	/
<g/>
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
osoby	osoba	k1gFnPc4	osoba
zhoubně	zhoubně	k6eAd1	zhoubně
vázány	vázán	k2eAgFnPc4d1	vázána
–	–	k?	–
uchování	uchování	k1gNnSc3	uchování
dyády	dyáda	k1gFnSc2	dyáda
zaplatí	zaplatit	k5eAaPmIp3nS	zaplatit
nezmíněným	zmíněný	k2eNgNnSc7d1	nezmíněné
neštěstím	neštěstí	k1gNnSc7	neštěstí
Vodníka	vodník	k1gMnSc2	vodník
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
jejich	jejich	k3xOp3gNnSc2	jejich
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Elektřin	elektřina	k1gFnPc2	elektřina
komplex	komplex	k1gInSc4	komplex
Incest	incest	k1gInSc4	incest
</s>
