<s>
Šupinovka	šupinovka	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
</s>
<s>
Šupinovka	šupinovka	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
houby	houba	k1gFnPc1
(	(	kIx(
<g/>
Fungi	Fungi	k1gNnSc1
<g/>
)	)	kIx)
Oddělení	oddělení	k1gNnSc1
</s>
<s>
houby	houby	k6eAd1
stopkovýtrusné	stopkovýtrusný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Basidiomycota	Basidiomycota	k1gFnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
stopkovýtrusé	stopkovýtrusý	k2eAgFnPc1d1
(	(	kIx(
<g/>
basidiomycetes	basidiomycetes	k1gInSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
houby	houba	k1gFnPc1
rouškaté	rouškatý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Agaricomycetidae	Agaricomycetida	k1gFnPc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
lupenotvaré	lupenotvarý	k2eAgFnPc1d1
(	(	kIx(
<g/>
Agaricales	Agaricales	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
límcovkovité	límcovkovitý	k2eAgInPc4d1
(	(	kIx(
<g/>
Strophariaceae	Strophariacea	k1gInPc4
<g/>
)	)	kIx)
Rod	rod	k1gInSc4
</s>
<s>
šupinovka	šupinovka	k1gFnSc1
(	(	kIx(
<g/>
Gymnopilus	Gymnopilus	k1gInSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Gymnopilus	Gymnopilus	k1gMnSc1
spectabilis	spectabilis	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
Sing	Sing	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Areál	areál	k1gInSc1
rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Šupinovka	šupinovka	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
(	(	kIx(
<g/>
Gymnopilus	Gymnopilus	k1gMnSc1
spectabilis	spectabilis	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejedlá	jedlý	k2eNgFnSc1d1
houba	houba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
límcovkovitých	límcovkovitý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Roste	růst	k5eAaImIp3nS
v	v	k7c6
srpnu	srpen	k1gInSc6
až	až	k8xS
říjnu	říjen	k1gInSc6
nepříliš	příliš	k6eNd1
hojně	hojně	k6eAd1
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
trsnatě	trsnatě	k6eAd1
na	na	k7c6
bázi	báze	k1gFnSc6
kmenů	kmen	k1gInPc2
a	a	k8xC
pařezech	pařez	k1gInPc6
listnáčů	listnáč	k1gInPc2
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
dubů	dub	k1gInPc2
<g/>
,	,	kIx,
vzácně	vzácně	k6eAd1
i	i	k9
borovic	borovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Synonyma	synonymum	k1gNnPc1
</s>
<s>
Šupinovec	Šupinovec	k1gMnSc1
nádherný	nádherný	k2eAgMnSc1d1
</s>
<s>
Gymnopilus	Gymnopilus	k1gMnSc1
junonius	junonius	k1gMnSc1
(	(	kIx(
<g/>
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
P.	P.	kA
<g/>
D.	D.	kA
Orton	Orton	k1gMnSc1
</s>
<s>
Pholiota	Pholiota	k1gFnSc1
spectabilis	spectabilis	k1gMnSc1
(	(	kIx(
<g/>
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
Kummer	Kummer	k1gMnSc1
</s>
<s>
Agaricus	Agaricus	k1gMnSc1
junonius	junonius	k1gMnSc1
Fr.	Fr.	k1gMnSc1
</s>
<s>
Gymnopilus	Gymnopilus	k1gMnSc1
spectabilis	spectabilis	k1gMnSc1
var.	var.	k?
junonius	junonius	k1gMnSc1
(	(	kIx(
<g/>
Fr.	Fr.	k1gMnSc1
<g/>
)	)	kIx)
Kühner	Kühner	k1gMnSc1
&	&	k?
Romagn	Romagn	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pholiota	Pholiota	k1gFnSc1
grandis	grandis	k1gFnSc1
Rea	Rea	k1gFnSc1
</s>
<s>
Pholiota	Pholiota	k1gFnSc1
junonia	junonium	k1gNnSc2
(	(	kIx(
<g/>
Fr.	Fr.	k1gFnSc1
<g/>
)	)	kIx)
P.	P.	kA
Karst	Karst	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Pholiota	Pholiota	k1gFnSc1
spectabilis	spectabilis	k1gMnSc1
var.	var.	k?
junonia	junonium	k1gNnSc2
(	(	kIx(
<g/>
Fr.	Fr.	k1gFnSc1
<g/>
)	)	kIx)
J.E.	J.E.	k1gFnSc1
Lange	Lange	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ZICHA	Zich	k1gMnSc4
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gymnopilus	Gymnopilus	k1gMnSc1
junonius	junonius	k1gMnSc1
(	(	kIx(
<g/>
šupinovka	šupinovka	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2005-03-13	2005-03-13	k4
14	#num#	k4
<g/>
:	:	kIx,
<g/>
45	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
CET	ceta	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
šupinovka	šupinovka	k1gFnSc1
nádherná	nádherný	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Gymnopilus	Gymnopilus	k1gInSc1
junonius	junonius	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Biologie	biologie	k1gFnSc1
|	|	kIx~
Dřevo	dřevo	k1gNnSc1
a	a	k8xC
nábytek	nábytek	k1gInSc1
|	|	kIx~
Houby	houba	k1gFnPc5
</s>
