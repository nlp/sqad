<p>
<s>
Arlington	Arlington	k1gInSc1	Arlington
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Snohomish	Snohomisha	k1gFnPc2	Snohomisha
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
hraničí	hraničit	k5eAaImIp3nP	hraničit
s	s	k7c7	s
městem	město	k1gNnSc7	město
Marysville	Marysville	k1gFnSc2	Marysville
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žilo	žít	k5eAaImAgNnS	žít
17	[number]	k4	17
926	[number]	k4	926
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
naplánováno	naplánovat	k5eAaBmNgNnS	naplánovat
roku	rok	k1gInSc2	rok
1890	[number]	k4	1890
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
Lordu	lord	k1gMnSc6	lord
Henrym	Henry	k1gMnSc6	Henry
Arlingtonovi	Arlington	k1gMnSc6	Arlington
<g/>
,	,	kIx,	,
členovi	člen	k1gMnSc3	člen
anglické	anglický	k2eAgFnSc2d1	anglická
vlády	vláda	k1gFnSc2	vláda
pod	pod	k7c7	pod
Jiřím	Jiří	k1gMnSc7	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
nedaleko	daleko	k6eNd1	daleko
založeno	založit	k5eAaPmNgNnS	založit
Haller	Haller	k1gMnSc1	Haller
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
o	o	k7c4	o
což	což	k3yRnSc4	což
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
Theodore	Theodor	k1gMnSc5	Theodor
N.	N.	kA	N.
Haller	Haller	k1gInSc4	Haller
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodiče	rodič	k1gMnPc1	rodič
Henrietta	Henriett	k1gInSc2	Henriett
a	a	k8xC	a
Granville	Granville	k1gFnSc2	Granville
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
začlenění	začlenění	k1gNnSc6	začlenění
Arlingtonu	Arlington	k1gInSc2	Arlington
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1903	[number]	k4	1903
se	se	k3xPyFc4	se
Haller	Haller	k1gMnSc1	Haller
City	City	k1gFnSc2	City
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
mnoho	mnoho	k6eAd1	mnoho
historicky	historicky	k6eAd1	historicky
významných	významný	k2eAgFnPc2d1	významná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
Muzeum	muzeum	k1gNnSc1	muzeum
průkopníků	průkopník	k1gMnPc2	průkopník
ve	v	k7c4	v
Stillaguamish	Stillaguamish	k1gInSc4	Stillaguamish
Valley	Vallea	k1gFnSc2	Vallea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Demografie	demografie	k1gFnSc2	demografie
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
17	[number]	k4	17
926	[number]	k4	926
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
86	[number]	k4	86
%	%	kIx~	%
tvořili	tvořit	k5eAaImAgMnP	tvořit
běloši	běloch	k1gMnPc1	běloch
<g/>
,	,	kIx,	,
3	[number]	k4	3
%	%	kIx~	%
Asiaté	Asiat	k1gMnPc1	Asiat
a	a	k8xC	a
přes	přes	k7c4	přes
1	[number]	k4	1
%	%	kIx~	%
původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
<g/>
.	.	kIx.	.
10	[number]	k4	10
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomie	ekonomie	k1gFnSc2	ekonomie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dřevo	dřevo	k1gNnSc1	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
byl	být	k5eAaImAgInS	být
Arlington	Arlington	k1gInSc1	Arlington
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
těžbě	těžba	k1gFnSc6	těžba
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
dřevozpracujícím	dřevozpracující	k2eAgInSc6d1	dřevozpracující
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
šindelovým	šindelový	k2eAgFnPc3d1	Šindelová
továrnám	továrna	k1gFnPc3	továrna
bylo	být	k5eAaImAgNnS	být
kdysi	kdysi	k6eAd1	kdysi
město	město	k1gNnSc1	město
nazváno	nazván	k2eAgNnSc1d1	nazváno
šindelovým	šindelový	k2eAgNnSc7d1	Šindelové
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházely	nacházet	k5eAaImAgFnP	nacházet
četné	četný	k2eAgInPc4d1	četný
pilařské	pilařský	k2eAgInPc4d1	pilařský
závody	závod	k1gInPc4	závod
a	a	k8xC	a
tábory	tábor	k1gInPc4	tábor
na	na	k7c4	na
těžbu	těžba	k1gFnSc4	těžba
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Železnice	železnice	k1gFnSc1	železnice
===	===	k?	===
</s>
</p>
<p>
<s>
Kdysi	kdysi	k6eAd1	kdysi
dávno	dávno	k6eAd1	dávno
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
několik	několik	k4yIc4	několik
možností	možnost	k1gFnPc2	možnost
železničního	železniční	k2eAgNnSc2d1	železniční
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Nacházelo	nacházet	k5eAaImAgNnS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
koridoru	koridor	k1gInSc6	koridor
společnosti	společnost	k1gFnSc2	společnost
Northern	Northern	k1gMnSc1	Northern
Pacific	Pacific	k1gMnSc1	Pacific
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gMnSc4	on
spojoval	spojovat	k5eAaImAgMnS	spojovat
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
a	a	k8xC	a
městy	město	k1gNnPc7	město
Sedro-Woolley	Sedro-Woollea	k1gFnSc2	Sedro-Woollea
či	či	k8xC	či
Snohomish	Snohomisha	k1gFnPc2	Snohomisha
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgFnS	nacházet
vlečka	vlečka	k1gFnSc1	vlečka
vedoucí	vedoucí	k1gFnSc1	vedoucí
do	do	k7c2	do
Darringtonu	Darrington	k1gInSc2	Darrington
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
využívána	využívat	k5eAaImNgFnS	využívat
především	především	k9	především
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
úpadku	úpadek	k1gInSc3	úpadek
železnice	železnice	k1gFnSc1	železnice
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc4	dva
trasy	trasa	k1gFnPc4	trasa
po	po	k7c4	po
zakoupení	zakoupení	k1gNnSc4	zakoupení
společností	společnost	k1gFnPc2	společnost
BNSF	BNSF	kA	BNSF
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
opuštěny	opustit	k5eAaPmNgInP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
už	už	k6eAd1	už
město	město	k1gNnSc1	město
není	být	k5eNaImIp3nS	být
tolik	tolik	k6eAd1	tolik
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
železnici	železnice	k1gFnSc6	železnice
jako	jako	k9	jako
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
má	mít	k5eAaImIp3nS	mít
jedno	jeden	k4xCgNnSc1	jeden
spojení	spojení	k1gNnSc1	spojení
přes	přes	k7c4	přes
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
trať	trať	k1gFnSc4	trať
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
sítí	síť	k1gFnSc7	síť
BNSF	BNSF	kA	BNSF
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bohaté	bohatý	k2eAgFnSc6d1	bohatá
železniční	železniční	k2eAgFnSc6d1	železniční
historii	historie	k1gFnSc6	historie
města	město	k1gNnSc2	město
zbývá	zbývat	k5eAaImIp3nS	zbývat
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
opuštěné	opuštěný	k2eAgNnSc4d1	opuštěné
signalizační	signalizační	k2eAgNnSc4d1	signalizační
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Okres	okres	k1gInSc1	okres
Snohomish	Snohomish	k1gInSc1	Snohomish
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
snaží	snažit	k5eAaImIp3nP	snažit
o	o	k7c6	o
transformaci	transformace	k1gFnSc6	transformace
bývalých	bývalý	k2eAgFnPc2d1	bývalá
železničních	železniční	k2eAgFnPc2d1	železniční
tratí	trať	k1gFnPc2	trať
v	v	k7c6	v
turistické	turistický	k2eAgFnSc6d1	turistická
nebo	nebo	k8xC	nebo
cyklistické	cyklistický	k2eAgFnPc1d1	cyklistická
stezky	stezka	k1gFnPc1	stezka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letectví	letectví	k1gNnSc2	letectví
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
odsud	odsud	k6eAd1	odsud
nachází	nacházet	k5eAaImIp3nS	nacházet
5	[number]	k4	5
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
festivalu	festival	k1gInSc2	festival
Arlington	Arlington	k1gInSc4	Arlington
Fly-In	Fly-Ino	k1gNnPc2	Fly-Ino
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
stahuje	stahovat	k5eAaImIp3nS	stahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
letců	letec	k1gMnPc2	letec
z	z	k7c2	z
USA	USA	kA	USA
i	i	k8xC	i
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1959	[number]	k4	1959
spadl	spadnout	k5eAaPmAgInS	spadnout
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
do	do	k7c2	do
koryta	koryto	k1gNnSc2	koryto
řeky	řeka	k1gFnSc2	řeka
Stillaguamish	Stillaguamish	k1gInSc1	Stillaguamish
Boeing	boeing	k1gInSc1	boeing
707	[number]	k4	707
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
první	první	k4xOgMnSc1	první
z	z	k7c2	z
pěti	pět	k4xCc2	pět
letadel	letadlo	k1gNnPc2	letadlo
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
doručena	doručen	k2eAgFnSc1d1	doručena
společnosti	společnost	k1gFnPc1	společnost
Braniff	Braniff	k1gInSc1	Braniff
International	International	k1gFnSc2	International
Airways	Airwaysa	k1gFnPc2	Airwaysa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nouzovém	nouzový	k2eAgNnSc6d1	nouzové
přistání	přistání	k1gNnSc6	přistání
zahynul	zahynout	k5eAaPmAgMnS	zahynout
jak	jak	k8xC	jak
testovací	testovací	k2eAgMnSc1d1	testovací
pilot	pilot	k1gMnSc1	pilot
společnosti	společnost	k1gFnSc2	společnost
Boeing	boeing	k1gInSc1	boeing
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
kapitán	kapitán	k1gMnSc1	kapitán
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
Braniff	Braniff	k1gInSc1	Braniff
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pád	pád	k1gInSc4	pád
mohlo	moct	k5eAaImAgNnS	moct
stržení	stržení	k1gNnSc1	stržení
tří	tři	k4xCgFnPc2	tři
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
motorů	motor	k1gInPc2	motor
při	při	k7c6	při
tréninkovém	tréninkový	k2eAgInSc6d1	tréninkový
manévru	manévr	k1gInSc6	manévr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnebí	podnebí	k1gNnSc2	podnebí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Části	část	k1gFnSc3	část
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
<s>
Smokey	Smokey	k1gInPc4	Smokey
Point	pointa	k1gFnPc2	pointa
je	být	k5eAaImIp3nS	být
rušná	rušný	k2eAgFnSc1d1	rušná
rezidenční	rezidenční	k2eAgFnSc3d1	rezidenční
<g/>
,	,	kIx,	,
komerční	komerční	k2eAgFnSc3d1	komerční
i	i	k8xC	i
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
stala	stát	k5eAaPmAgFnS	stát
oficiálně	oficiálně	k6eAd1	oficiálně
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
Arlington	Arlington	k1gInSc1	Arlington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gleneagle	Gleneagle	k1gFnSc1	Gleneagle
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
čtvrť	čtvrť	k1gFnSc1	čtvrť
Arlingtonu	Arlington	k1gInSc2	Arlington
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
108	[number]	k4	108
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
místní	místní	k2eAgNnSc4d1	místní
golfové	golfový	k2eAgNnSc4d1	golfové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
Postavena	postaven	k2eAgFnSc1d1	postavena
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1987	[number]	k4	1987
a	a	k8xC	a
2002	[number]	k4	2002
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
ji	on	k3xPp3gFnSc4	on
obývá	obývat	k5eAaImIp3nS	obývat
30	[number]	k4	30
%	%	kIx~	%
populace	populace	k1gFnSc1	populace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
High	High	k1gMnSc1	High
Clover	Clover	k1gMnSc1	Clover
Park	park	k1gInSc4	park
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc1d1	další
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
162	[number]	k4	162
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
městského	městský	k2eAgNnSc2d1	Městské
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arlington	Arlington	k1gInSc1	Arlington
Heights	Heightsa	k1gFnPc2	Heightsa
je	být	k5eAaImIp3nS	být
rezidenční	rezidenční	k2eAgFnSc1d1	rezidenční
čtvrť	čtvrť	k1gFnSc1	čtvrť
východně	východně	k6eAd1	východně
od	od	k7c2	od
Arlingtonu	Arlington	k1gInSc2	Arlington
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
trojúhelníkové	trojúhelníkový	k2eAgFnSc6d1	trojúhelníková
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
z	z	k7c2	z
východu	východ	k1gInSc2	východ
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Kaskádové	kaskádový	k2eAgNnSc4d1	kaskádové
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
severní	severní	k2eAgNnSc1d1	severní
rameno	rameno	k1gNnSc1	rameno
řeky	řeka	k1gFnSc2	řeka
Stillaguamish	Stillaguamisha	k1gFnPc2	Stillaguamisha
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
jižním	jižní	k2eAgInSc6d1	jižní
ramenem	rameno	k1gNnSc7	rameno
této	tento	k3xDgFnSc2	tento
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Kaskád	kaskáda	k1gFnPc2	kaskáda
kousek	kousek	k6eAd1	kousek
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
čtvrtě	čtvrt	k1gFnSc2	čtvrt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Námořní	námořní	k2eAgFnSc1d1	námořní
rádiová	rádiový	k2eAgFnSc1d1	rádiová
stanice	stanice	k1gFnSc1	stanice
Jim	on	k3xPp3gMnPc3	on
Creek	Creky	k1gFnPc2	Creky
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
a	a	k8xC	a
s	s	k7c7	s
ponorkami	ponorka	k1gFnPc7	ponorka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c6	na
velice	velice	k6eAd1	velice
nízké	nízký	k2eAgFnSc6d1	nízká
frekvenci	frekvence	k1gFnSc6	frekvence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
vyřazená	vyřazený	k2eAgFnSc1d1	vyřazená
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
využívají	využívat	k5eAaPmIp3nP	využívat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
jen	jen	k9	jen
příslušníci	příslušník	k1gMnPc1	příslušník
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mimo	mimo	k7c4	mimo
samotné	samotný	k2eAgNnSc4d1	samotné
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnPc1	jeho
obyvatelé	obyvatel	k1gMnPc1	obyvatel
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
poštu	pošta	k1gFnSc4	pošta
jako	jako	k8xC	jako
Arlington	Arlington	k1gInSc4	Arlington
a	a	k8xC	a
tamní	tamní	k2eAgFnPc4d1	tamní
děti	dítě	k1gFnPc4	dítě
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
školy	škola	k1gFnPc1	škola
Arlingtonského	Arlingtonský	k2eAgInSc2d1	Arlingtonský
školního	školní	k2eAgInSc2d1	školní
okrsku	okrsek	k1gInSc2	okrsek
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
místa	místo	k1gNnPc1	místo
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
nabízí	nabízet	k5eAaImIp3nP	nabízet
výhledy	výhled	k1gInPc1	výhled
na	na	k7c4	na
Whitehorse	Whitehorse	k1gFnPc4	Whitehorse
Mountain	Mountaina	k1gFnPc2	Mountaina
a	a	k8xC	a
okolní	okolní	k2eAgFnSc2d1	okolní
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Arlington	Arlington	k1gInSc1	Arlington
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
