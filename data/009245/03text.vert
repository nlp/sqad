<p>
<s>
Řemeslo	řemeslo	k1gNnSc1	řemeslo
je	být	k5eAaImIp3nS	být
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
manuální	manuální	k2eAgFnSc2d1	manuální
dovednosti	dovednost	k1gFnSc2	dovednost
<g/>
,	,	kIx,	,
provozovaný	provozovaný	k2eAgMnSc1d1	provozovaný
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obživy	obživa	k1gFnSc2	obživa
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
vytváření	vytváření	k1gNnSc1	vytváření
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řemeslné	řemeslný	k2eAgFnPc4d1	řemeslná
práce	práce	k1gFnPc4	práce
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
vysoký	vysoký	k2eAgInSc1d1	vysoký
podíl	podíl	k1gInSc1	podíl
ruční	ruční	k2eAgFnSc2d1	ruční
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
spojený	spojený	k2eAgInSc4d1	spojený
s	s	k7c7	s
používáním	používání	k1gNnSc7	používání
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Řemeslné	řemeslný	k2eAgFnPc1d1	řemeslná
práce	práce	k1gFnPc1	práce
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
slova	slovo	k1gNnSc2	slovo
se	se	k3xPyFc4	se
v	v	k7c6	v
nejširším	široký	k2eAgInSc6d3	nejširší
smyslu	smysl	k1gInSc6	smysl
slova	slovo	k1gNnSc2	slovo
jedná	jednat	k5eAaImIp3nS	jednat
ale	ale	k8xC	ale
i	i	k9	i
o	o	k7c4	o
intelektuální	intelektuální	k2eAgFnPc4d1	intelektuální
dovednosti	dovednost	k1gFnPc4	dovednost
a	a	k8xC	a
osobní	osobní	k2eAgFnPc4d1	osobní
schopnosti	schopnost	k1gFnPc4	schopnost
doplněné	doplněná	k1gFnSc2	doplněná
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
nutně	nutně	k6eAd1	nutně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
manuální	manuální	k2eAgNnSc1d1	manuální
<g/>
)	)	kIx)	)
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
zručností	zručnost	k1gFnSc7	zručnost
<g/>
,	,	kIx,	,
odbornou	odborný	k2eAgFnSc7d1	odborná
erudicí	erudice	k1gFnSc7	erudice
a	a	k8xC	a
zkušeností	zkušenost	k1gFnSc7	zkušenost
resp.	resp.	kA	resp.
praxí	praxe	k1gFnSc7	praxe
daného	daný	k2eAgMnSc4d1	daný
jedince	jedinec	k1gMnSc4	jedinec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
umělecké	umělecký	k2eAgNnSc1d1	umělecké
řemeslo	řemeslo	k1gNnSc1	řemeslo
nemůže	moct	k5eNaImIp3nS	moct
vykonávat	vykonávat	k5eAaImF	vykonávat
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
i	i	k9	i
tvůrčí	tvůrčí	k2eAgFnSc4d1	tvůrčí
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
každému	každý	k3xTgNnSc3	každý
dány	dán	k2eAgInPc1d1	dán
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
vymyslet	vymyslet	k5eAaPmF	vymyslet
a	a	k8xC	a
předložit	předložit	k5eAaPmF	předložit
vlastní	vlastní	k2eAgInSc4d1	vlastní
originální	originální	k2eAgInSc4d1	originální
umělecký	umělecký	k2eAgInSc4d1	umělecký
návrh	návrh	k1gInSc4	návrh
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
rozvojem	rozvoj	k1gInSc7	rozvoj
a	a	k8xC	a
pokrokem	pokrok	k1gInSc7	pokrok
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
stoupají	stoupat	k5eAaImIp3nP	stoupat
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
úroveň	úroveň	k1gFnSc4	úroveň
řemeslníků	řemeslník	k1gMnPc2	řemeslník
a	a	k8xC	a
přibývá	přibývat	k5eAaImIp3nS	přibývat
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
středoškolské	středoškolský	k2eAgNnSc4d1	středoškolské
či	či	k8xC	či
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
samozřejmostí	samozřejmost	k1gFnPc2	samozřejmost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Řemesla	řemeslo	k1gNnPc1	řemeslo
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
postupující	postupující	k2eAgFnSc7d1	postupující
specializací	specializace	k1gFnSc7	specializace
lidských	lidský	k2eAgFnPc2d1	lidská
činností	činnost	k1gFnPc2	činnost
-	-	kIx~	-
některé	některý	k3yIgFnPc4	některý
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
vysokou	vysoký	k2eAgFnSc4d1	vysoká
odbornost	odbornost	k1gFnSc4	odbornost
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
zručnost	zručnost	k1gFnSc4	zručnost
postupně	postupně	k6eAd1	postupně
začali	začít	k5eAaPmAgMnP	začít
vykonávat	vykonávat	k5eAaImF	vykonávat
jedinci	jedinec	k1gMnSc3	jedinec
<g/>
,	,	kIx,	,
touto	tento	k3xDgFnSc7	tento
odborností	odbornost	k1gFnSc7	odbornost
či	či	k8xC	či
zručností	zručnost	k1gFnSc7	zručnost
vynikající	vynikající	k2eAgFnSc7d1	vynikající
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
(	(	kIx(	(
<g/>
daného	daný	k2eAgNnSc2d1	dané
<g/>
)	)	kIx)	)
řemesla	řemeslo	k1gNnSc2	řemeslo
byl	být	k5eAaImAgInS	být
nejprve	nejprve	k6eAd1	nejprve
zajišťován	zajišťovat	k5eAaImNgInS	zajišťovat
po	po	k7c6	po
dědické	dědický	k2eAgFnSc6d1	dědická
linii	linie	k1gFnSc6	linie
–	–	k?	–
předáváním	předávání	k1gNnSc7	předávání
zkušeností	zkušenost	k1gFnPc2	zkušenost
potomkům	potomek	k1gMnPc3	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
ustálil	ustálit	k5eAaPmAgInS	ustálit
proces	proces	k1gInSc1	proces
řemeslnické	řemeslnický	k2eAgFnSc2d1	řemeslnická
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
si	se	k3xPyFc3	se
mistři	mistr	k1gMnPc1	mistr
řemeslníci	řemeslník	k1gMnPc1	řemeslník
kromě	kromě	k7c2	kromě
členů	člen	k1gInPc2	člen
svých	svůj	k3xOyFgFnPc2	svůj
rodin	rodina	k1gFnPc2	rodina
vybírali	vybírat	k5eAaImAgMnP	vybírat
cizí	cizí	k2eAgMnPc4d1	cizí
učedníky	učedník	k1gMnPc4	učedník
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
za	za	k7c4	za
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
vyškolili	vyškolit	k5eAaPmAgMnP	vyškolit
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
absolventy	absolvent	k1gMnPc7	absolvent
bez	bez	k7c2	bez
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
tovaryše	tovaryš	k1gMnSc2	tovaryš
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zvyšovali	zvyšovat	k5eAaImAgMnP	zvyšovat
produktivitu	produktivita	k1gFnSc4	produktivita
jejich	jejich	k3xOp3gFnSc2	jejich
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
mistr	mistr	k1gMnSc1	mistr
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
činnostem	činnost	k1gFnPc3	činnost
<g/>
,	,	kIx,	,
vyžadujícím	vyžadující	k2eAgInSc7d1	vyžadující
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
učňové	učeň	k1gMnPc1	učeň
připravovali	připravovat	k5eAaImAgMnP	připravovat
materiály	materiál	k1gInPc4	materiál
a	a	k8xC	a
základní	základní	k2eAgFnPc4d1	základní
výrobní	výrobní	k2eAgFnPc4d1	výrobní
technologie	technologie	k1gFnPc4	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Tovaryši	tovaryš	k1gMnPc1	tovaryš
vykonávali	vykonávat	k5eAaImAgMnP	vykonávat
rutinní	rutinní	k2eAgFnSc4d1	rutinní
a	a	k8xC	a
snazší	snadný	k2eAgFnSc2d2	snazší
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
získávali	získávat	k5eAaImAgMnP	získávat
potřebné	potřebný	k2eAgFnPc4d1	potřebná
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
3-4	[number]	k4	3-4
roky	rok	k1gInPc4	rok
praxe	praxe	k1gFnSc1	praxe
mimo	mimo	k7c4	mimo
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
vyučili	vyučit	k5eAaPmAgMnP	vyučit
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
měst	město	k1gNnPc2	město
začaly	začít	k5eAaPmAgFnP	začít
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
vznikat	vznikat	k5eAaImF	vznikat
první	první	k4xOgFnSc1	první
profesní	profesní	k2eAgFnSc1d1	profesní
organizace	organizace	k1gFnSc1	organizace
řemeslníků	řemeslník	k1gMnPc2	řemeslník
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgMnSc1d1	sdružující
představitele	představitel	k1gMnSc2	představitel
daného	daný	k2eAgNnSc2d1	dané
řemesla	řemeslo	k1gNnSc2	řemeslo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
či	či	k8xC	či
regionu	region	k1gInSc6	region
–	–	k?	–
řemeslné	řemeslný	k2eAgInPc4d1	řemeslný
cechy	cech	k1gInPc4	cech
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
převzaly	převzít	k5eAaPmAgFnP	převzít
část	část	k1gFnSc1	část
úřední	úřední	k2eAgFnPc1d1	úřední
agendy	agenda	k1gFnPc1	agenda
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
zatěžující	zatěžující	k2eAgInPc4d1	zatěžující
členy	člen	k1gInPc4	člen
svých	svůj	k3xOyFgNnPc2	svůj
sdružení	sdružení	k1gNnPc2	sdružení
–	–	k?	–
cenovou	cenový	k2eAgFnSc4d1	cenová
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
kontrolní	kontrolní	k2eAgFnSc4d1	kontrolní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
organizačně	organizačně	k6eAd1	organizačně
technické	technický	k2eAgNnSc4d1	technické
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
<g/>
.	.	kIx.	.
</s>
<s>
Cechy	cech	k1gInPc1	cech
zavedly	zavést	k5eAaPmAgFnP	zavést
jednotné	jednotný	k2eAgFnPc1d1	jednotná
pracovní	pracovní	k2eAgFnPc1d1	pracovní
normy	norma	k1gFnPc1	norma
-	-	kIx~	-
cechovní	cechovní	k2eAgNnPc1d1	cechovní
pravidla	pravidlo	k1gNnPc1	pravidlo
(	(	kIx(	(
<g/>
pořádky	pořádek	k1gInPc1	pořádek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
závazné	závazný	k2eAgFnPc1d1	závazná
pro	pro	k7c4	pro
výkon	výkon	k1gInSc4	výkon
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
mistrovské	mistrovský	k2eAgInPc1d1	mistrovský
kusy	kus	k1gInPc1	kus
<g/>
,	,	kIx,	,
tovaryšské	tovaryšský	k2eAgFnPc1d1	tovaryšská
zkoušky	zkouška	k1gFnPc1	zkouška
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
i	i	k9	i
garantovaly	garantovat	k5eAaBmAgFnP	garantovat
minimální	minimální	k2eAgFnSc4d1	minimální
kvalitativní	kvalitativní	k2eAgFnSc4d1	kvalitativní
úroveň	úroveň	k1gFnSc4	úroveň
výkonu	výkon	k1gInSc2	výkon
řemesla	řemeslo	k1gNnSc2	řemeslo
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
nejstarší	starý	k2eAgNnSc4d3	nejstarší
cechovní	cechovní	k2eAgNnSc4d1	cechovní
sdružení	sdružení	k1gNnSc4	sdružení
jsou	být	k5eAaImIp3nP	být
známa	známo	k1gNnSc2	známo
ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
Florenice	Florenice	k1gFnSc1	Florenice
<g/>
,	,	kIx,	,
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
Pisa	Pisa	k1gFnSc1	Pisa
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
české	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
cechy	cech	k1gInPc1	cech
vznikaly	vznikat	k5eAaImAgFnP	vznikat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
1319	[number]	k4	1319
řezníci	řezník	k1gMnPc1	řezník
<g/>
,	,	kIx,	,
1324	[number]	k4	1324
zlatníci	zlatník	k1gMnPc5	zlatník
<g/>
,	,	kIx,	,
následovali	následovat	k5eAaImAgMnP	následovat
nožíři	nožíř	k1gMnPc1	nožíř
<g/>
,	,	kIx,	,
kováři	kovář	k1gMnPc1	kovář
<g/>
,	,	kIx,	,
zbrojíři	zbrojíř	k1gMnPc1	zbrojíř
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnPc1	krejčí
<g/>
,	,	kIx,	,
tkalci	tkadlec	k1gMnPc1	tkadlec
a	a	k8xC	a
soukeníci	soukeník	k1gMnPc1	soukeník
<g/>
,	,	kIx,	,
ševci	švec	k1gMnPc1	švec
<g/>
,	,	kIx,	,
vyšívači	vyšívač	k1gMnPc1	vyšívač
<g/>
,	,	kIx,	,
malíři	malíř	k1gMnPc1	malíř
a	a	k8xC	a
štítaři	štítař	k1gMnPc1	štítař
<g/>
,	,	kIx,	,
hrnčíři	hrnčíř	k1gMnPc1	hrnčíř
<g/>
,	,	kIx,	,
pasíři	pasíř	k1gMnPc1	pasíř
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
rozvojem	rozvoj	k1gInSc7	rozvoj
společnosti	společnost	k1gFnSc2	společnost
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc4	století
cechy	cech	k1gInPc1	cech
byly	být	k5eAaImAgInP	být
spíše	spíše	k9	spíše
na	na	k7c4	na
překážku	překážka	k1gFnSc4	překážka
<g/>
,	,	kIx,	,
snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
zakazovat	zakazovat	k5eAaImF	zakazovat
strojní	strojní	k2eAgFnSc4d1	strojní
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
svobodné	svobodný	k2eAgNnSc4d1	svobodné
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
postupně	postupně	k6eAd1	postupně
formou	forma	k1gFnSc7	forma
zákonů	zákon	k1gInPc2	zákon
zrušeny	zrušen	k2eAgFnPc1d1	zrušena
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
evropská	evropský	k2eAgFnSc1d1	Evropská
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaImF	upadat
<g/>
,	,	kIx,	,
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
podporu	podpora	k1gFnSc4	podpora
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
například	například	k6eAd1	například
obrodné	obrodný	k2eAgNnSc1d1	obrodné
hnutí	hnutí	k1gNnSc1	hnutí
Arts	Artsa	k1gFnPc2	Artsa
and	and	k?	and
Crafts	Craftsa	k1gFnPc2	Craftsa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ji	on	k3xPp3gFnSc4	on
začal	začít	k5eAaPmAgInS	začít
organizovat	organizovat	k5eAaBmF	organizovat
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Cechy	cech	k1gInPc1	cech
proměněné	proměněný	k2eAgInPc1d1	proměněný
spíše	spíše	k9	spíše
v	v	k7c4	v
profesní	profesní	k2eAgInPc4d1	profesní
spolky	spolek	k1gInPc4	spolek
a	a	k8xC	a
často	často	k6eAd1	často
označované	označovaný	k2eAgFnPc1d1	označovaná
termínem	termín	k1gInSc7	termín
grémium	grémium	k1gNnSc1	grémium
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
věnují	věnovat	k5eAaPmIp3nP	věnovat
jen	jen	k9	jen
samotnému	samotný	k2eAgNnSc3d1	samotné
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Současné	současný	k2eAgNnSc1d1	současné
platné	platný	k2eAgNnSc1d1	platné
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
následující	následující	k2eAgFnSc3d1	následující
řemeslné	řemeslný	k2eAgFnSc3d1	řemeslná
živnosti	živnost	k1gFnSc3	živnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Kovy	kov	k1gInPc1	kov
a	a	k8xC	a
kovové	kovový	k2eAgInPc1d1	kovový
výrobky	výrobek	k1gInPc1	výrobek
===	===	k?	===
</s>
</p>
<p>
<s>
Kovářství	kovářství	k1gNnSc1	kovářství
</s>
</p>
<p>
<s>
Podkovářství	podkovářství	k1gNnSc1	podkovářství
</s>
</p>
<p>
<s>
Zámečnictví	zámečnictví	k1gNnSc1	zámečnictví
</s>
</p>
<p>
<s>
Nástrojářství	Nástrojářství	k1gNnSc1	Nástrojářství
</s>
</p>
<p>
<s>
Kovoobráběčství	Kovoobráběčství	k1gNnSc1	Kovoobráběčství
</s>
</p>
<p>
<s>
Galvanizérství	galvanizérství	k1gNnSc1	galvanizérství
</s>
</p>
<p>
<s>
Smaltérství	Smaltérství	k1gNnSc1	Smaltérství
</s>
</p>
<p>
<s>
Slévárenství	slévárenství	k1gNnSc1	slévárenství
</s>
</p>
<p>
<s>
Modelářství	modelářství	k1gNnSc1	modelářství
</s>
</p>
<p>
<s>
===	===	k?	===
Motorové	motorový	k2eAgInPc1d1	motorový
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
===	===	k?	===
</s>
</p>
<p>
<s>
Opravy	oprava	k1gFnPc1	oprava
silničních	silniční	k2eAgNnPc2d1	silniční
vozidel	vozidlo	k1gNnPc2	vozidlo
</s>
</p>
<p>
<s>
Opravy	oprava	k1gFnPc1	oprava
karoserií	karoserie	k1gFnPc2	karoserie
</s>
</p>
<p>
<s>
Opravy	oprava	k1gFnPc1	oprava
ostatních	ostatní	k2eAgInPc2d1	ostatní
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
</s>
</p>
<p>
<s>
Opravy	oprava	k1gFnPc1	oprava
pracovních	pracovní	k2eAgInPc2d1	pracovní
strojů	stroj	k1gInPc2	stroj
</s>
</p>
<p>
<s>
===	===	k?	===
Zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
výrobky	výrobek	k1gInPc1	výrobek
a	a	k8xC	a
výrobky	výrobek	k1gInPc1	výrobek
jemné	jemný	k2eAgFnSc2d1	jemná
mechaniky	mechanika	k1gFnSc2	mechanika
===	===	k?	===
</s>
</p>
<p>
<s>
Hodinářství	hodinářství	k1gNnSc1	hodinářství
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
105	[number]	k4	105
<g/>
:	:	kIx,	:
Elektrické	elektrický	k2eAgInPc1d1	elektrický
stroje	stroj	k1gInPc1	stroj
a	a	k8xC	a
přístroje	přístroj	k1gInPc1	přístroj
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
instalace	instalace	k1gFnSc1	instalace
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
elektrických	elektrický	k2eAgInPc2d1	elektrický
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
přístrojů	přístroj	k1gInPc2	přístroj
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
instalace	instalace	k1gFnSc1	instalace
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
elektronických	elektronický	k2eAgNnPc2d1	elektronické
zařízení	zařízení	k1gNnPc2	zařízení
</s>
</p>
<p>
<s>
Montáž	montáž	k1gFnSc1	montáž
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc1	oprava
a	a	k8xC	a
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
chladicích	chladicí	k2eAgNnPc2d1	chladicí
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
tepelných	tepelný	k2eAgNnPc2d1	tepelné
čerpadel	čerpadlo	k1gNnPc2	čerpadlo
</s>
</p>
<p>
<s>
===	===	k?	===
Zpracování	zpracování	k1gNnSc1	zpracování
kameniva	kamenivo	k1gNnSc2	kamenivo
a	a	k8xC	a
zemin	zemina	k1gFnPc2	zemina
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
===	===	k?	===
</s>
</p>
<p>
<s>
Broušení	broušení	k1gNnSc1	broušení
a	a	k8xC	a
leptání	leptání	k1gNnSc1	leptání
skla	sklo	k1gNnSc2	sklo
</s>
</p>
<p>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
kamene	kámen	k1gInSc2	kámen
</s>
</p>
<p>
<s>
===	===	k?	===
Chemická	chemický	k2eAgFnSc1d1	chemická
výroba	výroba	k1gFnSc1	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
kosmetických	kosmetický	k2eAgInPc2d1	kosmetický
prostředků	prostředek	k1gInPc2	prostředek
</s>
</p>
<p>
<s>
===	===	k?	===
Potraviny	potravina	k1gFnPc1	potravina
a	a	k8xC	a
nápoje	nápoj	k1gInPc1	nápoj
===	===	k?	===
</s>
</p>
<p>
<s>
Řeznictví	řeznictví	k1gNnSc1	řeznictví
a	a	k8xC	a
uzenářství	uzenářství	k1gNnSc1	uzenářství
</s>
</p>
<p>
<s>
Mlynářství	mlynářství	k1gNnSc1	mlynářství
</s>
</p>
<p>
<s>
Pivovarnictví	pivovarnictví	k1gNnSc1	pivovarnictví
a	a	k8xC	a
sladovnictví	sladovnictví	k1gNnSc1	sladovnictví
</s>
</p>
<p>
<s>
Mlékárenství	mlékárenství	k1gNnSc1	mlékárenství
</s>
</p>
<p>
<s>
Pekařství	pekařství	k1gNnSc1	pekařství
<g/>
,	,	kIx,	,
cukrářství	cukrářství	k1gNnSc1	cukrářství
</s>
</p>
<p>
<s>
===	===	k?	===
Textilie	textilie	k1gFnPc4	textilie
a	a	k8xC	a
oděvy	oděv	k1gInPc4	oděv
===	===	k?	===
</s>
</p>
<p>
<s>
Barvení	barvení	k1gNnSc1	barvení
a	a	k8xC	a
chemická	chemický	k2eAgFnSc1d1	chemická
úprava	úprava	k1gFnSc1	úprava
textilií	textilie	k1gFnPc2	textilie
</s>
</p>
<p>
<s>
===	===	k?	===
Kůže	kůže	k1gFnPc4	kůže
<g/>
,	,	kIx,	,
kožené	kožený	k2eAgInPc4d1	kožený
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
pryžové	pryžový	k2eAgInPc4d1	pryžový
výrobky	výrobek	k1gInPc4	výrobek
a	a	k8xC	a
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
plastických	plastický	k2eAgFnPc2d1	plastická
hmot	hmota	k1gFnPc2	hmota
===	===	k?	===
</s>
</p>
<p>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
gumárenských	gumárenský	k2eAgFnPc2d1	gumárenská
směsí	směs	k1gFnPc2	směs
</s>
</p>
<p>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
kůží	kůže	k1gFnPc2	kůže
a	a	k8xC	a
kožešinOpravy	kožešinOprava	k1gFnSc2	kožešinOprava
usňové	usňový	k2eAgFnSc2d1	usňová
obuvi	obuv	k1gFnSc2	obuv
</s>
</p>
<p>
<s>
===	===	k?	===
Dřevařská	dřevařský	k2eAgFnSc1d1	dřevařská
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
výrobků	výrobek	k1gInPc2	výrobek
===	===	k?	===
</s>
</p>
<p>
<s>
Truhlářství	truhlářství	k1gNnSc1	truhlářství
</s>
</p>
<p>
<s>
Zlatnictví	zlatnictví	k1gNnSc1	zlatnictví
a	a	k8xC	a
klenotnictví	klenotnictví	k1gNnSc1	klenotnictví
</s>
</p>
<p>
<s>
Opravy	oprava	k1gFnPc1	oprava
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
</s>
</p>
<p>
<s>
===	===	k?	===
Papírenská	papírenský	k2eAgFnSc1d1	Papírenská
a	a	k8xC	a
polygrafická	polygrafický	k2eAgFnSc1d1	polygrafická
výroba	výroba	k1gFnSc1	výroba
===	===	k?	===
</s>
</p>
<p>
<s>
Polygrafická	polygrafický	k2eAgFnSc1d1	polygrafická
výroba	výroba	k1gFnSc1	výroba
</s>
</p>
<p>
<s>
===	===	k?	===
Stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Zednictví	zednictví	k1gNnSc1	zednictví
</s>
</p>
<p>
<s>
Tesařství	tesařství	k1gNnSc1	tesařství
</s>
</p>
<p>
<s>
Pokrývačství	pokrývačství	k1gNnSc1	pokrývačství
</s>
</p>
<p>
<s>
Klempířství	klempířství	k1gNnSc1	klempířství
</s>
</p>
<p>
<s>
Štukatérství	štukatérství	k1gNnSc1	štukatérství
</s>
</p>
<p>
<s>
Podlahářství	Podlahářství	k1gNnSc1	Podlahářství
</s>
</p>
<p>
<s>
Izolatérství	izolatérství	k1gNnSc1	izolatérství
</s>
</p>
<p>
<s>
Kominictví	kominictví	k1gNnSc1	kominictví
</s>
</p>
<p>
<s>
Vodoinstalatérství	Vodoinstalatérství	k1gNnSc1	Vodoinstalatérství
<g/>
,	,	kIx,	,
topenářství	topenářství	k1gNnSc1	topenářství
</s>
</p>
<p>
<s>
Montáž	montáž	k1gFnSc1	montáž
suchých	suchý	k2eAgFnPc2d1	suchá
staveb	stavba	k1gFnPc2	stavba
</s>
</p>
<p>
<s>
Kamnářství	kamnářství	k1gNnSc1	kamnářství
</s>
</p>
<p>
<s>
Malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
lakýrnictví	lakýrnictví	k1gNnSc1	lakýrnictví
a	a	k8xC	a
natěračství	natěračství	k1gNnSc1	natěračství
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
Čištění	čištění	k1gNnSc1	čištění
textilu	textil	k1gInSc2	textil
a	a	k8xC	a
oděvů	oděv	k1gInPc2	oděv
</s>
</p>
<p>
<s>
Fotografické	fotografický	k2eAgFnPc1d1	fotografická
služby	služba	k1gFnPc1	služba
</s>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
opravy	oprava	k1gFnPc1	oprava
ortopedické	ortopedický	k2eAgFnSc2d1	ortopedická
obuvi	obuv	k1gFnSc2	obuv
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
řemeslo	řemeslo	k1gNnSc4	řemeslo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
řemeslo	řemeslo	k1gNnSc4	řemeslo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Televizní	televizní	k2eAgInSc4d1	televizní
pořad	pořad	k1gInSc4	pořad
Devatero	devatero	k1gNnSc4	devatero
řemesel	řemeslo	k1gNnPc2	řemeslo
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
ČT	ČT	kA	ČT
</s>
</p>
