<p>
<s>
BK	BK	kA	BK
Kondoři	kondor	k1gMnPc1	kondor
Liberec	Liberec	k1gInSc4	Liberec
(	(	kIx(	(
<g/>
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
Kondoři	kondor	k1gMnPc1	kondor
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
přivést	přivést	k5eAaPmF	přivést
do	do	k7c2	do
Liberce	Liberec	k1gInSc2	Liberec
prvoligový	prvoligový	k2eAgInSc4d1	prvoligový
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
hned	hned	k6eAd1	hned
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
koupil	koupit	k5eAaPmAgInS	koupit
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
účast	účast	k1gFnSc4	účast
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
(	(	kIx(	(
<g/>
sezóna	sezóna	k1gFnSc1	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
postup	postup	k1gInSc1	postup
do	do	k7c2	do
NBL	NBL	kA	NBL
kde	kde	k6eAd1	kde
odehrál	odehrát	k5eAaPmAgInS	odehrát
3	[number]	k4	3
sezony	sezona	k1gFnPc4	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2018	[number]	k4	2018
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
působí	působit	k5eAaImIp3nS	působit
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
soutěži	soutěž	k1gFnSc6	soutěž
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
29.4	[number]	k4	29.4
<g/>
.2009	.2009	k4	.2009
byl	být	k5eAaImAgInS	být
liberecký	liberecký	k2eAgInSc1d1	liberecký
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
<g/>
)	)	kIx)	)
odkoupen	odkoupen	k2eAgMnSc1d1	odkoupen
brněnskými	brněnský	k2eAgMnPc7d1	brněnský
podnikateli	podnikatel	k1gMnPc7	podnikatel
Jiřím	Jiří	k1gMnSc7	Jiří
Hosem	Hos	k1gMnSc7	Hos
a	a	k8xC	a
Radkem	Radek	k1gMnSc7	Radek
Konečným	Konečný	k1gMnSc7	Konečný
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
licenci	licence	k1gFnSc4	licence
na	na	k7c6	na
NBL	NBL	kA	NBL
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
finančnímu	finanční	k2eAgInSc3d1	finanční
krachu	krach	k1gInSc3	krach
BC	BC	kA	BC
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
a	a	k8xC	a
sestupu	sestup	k1gInSc3	sestup
BBK	BBK	kA	BBK
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vytvořen	vytvořen	k2eAgInSc1d1	vytvořen
nový	nový	k2eAgInSc1d1	nový
klub	klub	k1gInSc1	klub
s	s	k7c7	s
názvem	název	k1gInSc7	název
Basketball	Basketball	k1gInSc1	Basketball
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
libereckého	liberecký	k2eAgInSc2d1	liberecký
klubu	klub	k1gInSc2	klub
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
basketbal	basketbal	k1gInSc4	basketbal
v	v	k7c6	v
regionu	region	k1gInSc6	region
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
návratnost	návratnost	k1gFnSc1	návratnost
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liberecké	liberecký	k2eAgFnSc3d1	liberecká
firmě	firma	k1gFnSc3	firma
MSV	MSV	kA	MSV
SYSTEMS	SYSTEMS	kA	SYSTEMS
CZ	CZ	kA	CZ
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o	o	k7c4	o
(	(	kIx(	(
<g/>
které	který	k3yQgNnSc4	který
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
podporuje	podporovat	k5eAaImIp3nS	podporovat
mládež	mládež	k1gFnSc1	mládež
Kondorů	kondor	k1gMnPc2	kondor
<g/>
)	)	kIx)	)
nebyl	být	k5eNaImAgInS	být
osud	osud	k1gInSc1	osud
týmu	tým	k1gInSc2	tým
lhostejný	lhostejný	k2eAgInSc1d1	lhostejný
<g/>
,	,	kIx,	,
sehnala	sehnat	k5eAaPmAgFnS	sehnat
finance	finance	k1gFnSc1	finance
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
sezonu	sezona	k1gFnSc4	sezona
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
a	a	k8xC	a
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
prvoligovou	prvoligový	k2eAgFnSc4d1	prvoligová
licenci	licence	k1gFnSc4	licence
od	od	k7c2	od
BK	BK	kA	BK
Jičín	Jičín	k1gInSc1	Jičín
<g/>
.	.	kIx.	.
</s>
<s>
Kondoři	kondor	k1gMnPc1	kondor
hrají	hrát	k5eAaImIp3nP	hrát
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
v	v	k7c6	v
liberecké	liberecký	k2eAgFnSc6d1	liberecká
hale	hala	k1gFnSc6	hala
Kort	Kort	k1gInSc1	Kort
(	(	kIx(	(
<g/>
Jeronýmova	Jeronýmův	k2eAgFnSc1d1	Jeronýmova
570	[number]	k4	570
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
,	,	kIx,	,
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
PSČ	PSČ	kA	PSČ
47001	[number]	k4	47001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
pro	pro	k7c4	pro
sezonu	sezona	k1gFnSc4	sezona
[	[	kIx(	[
<g/>
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
ale	ale	k8xC	ale
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
okleštěny	okleštit	k5eAaPmNgFnP	okleštit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tým	tým	k1gInSc4	tým
opustila	opustit	k5eAaPmAgFnS	opustit
většina	většina	k1gFnSc1	většina
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Liberečtí	liberecký	k2eAgMnPc1d1	liberecký
tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
vyřešili	vyřešit	k5eAaPmAgMnP	vyřešit
<g/>
,	,	kIx,	,
povolali	povolat	k5eAaPmAgMnP	povolat
hráče	hráč	k1gMnSc4	hráč
z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
kádr	kádr	k1gInSc4	kádr
doplnili	doplnit	k5eAaPmAgMnP	doplnit
odchovanci	odchovanec	k1gMnPc1	odchovanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
Kort	Kort	k1gInSc4	Kort
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
80	[number]	k4	80
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
ZČ	ZČ	kA	ZČ
-	-	kIx~	-
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc1d1	červené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc1d1	zelené
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgNnSc1d1	fialové
podbarvení	podbarvení	k1gNnSc1	podbarvení
-	-	kIx~	-
reorganizace	reorganizace	k1gFnSc1	reorganizace
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
skupiny	skupina	k1gFnSc2	skupina
či	či	k8xC	či
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
