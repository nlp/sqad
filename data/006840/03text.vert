<s>
ha-Kol	ha-Kol	k1gInSc1	ha-Kol
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ה	ה	k?	ה
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
Hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
hebrejský	hebrejský	k2eAgInSc1d1	hebrejský
psaný	psaný	k2eAgInSc1d1	psaný
deník	deník	k1gInSc1	deník
vycházející	vycházející	k2eAgInSc1d1	vycházející
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
deníku	deník	k1gInSc2	deník
stála	stát	k5eAaImAgFnS	stát
ultraortodoxní	ultraortodoxní	k2eAgFnSc1d1	ultraortodoxní
náboženská	náboženský	k2eAgFnSc1d1	náboženská
strana	strana	k1gFnSc1	strana
Agudat	Agudat	k1gFnSc1	Agudat
Jisra	Jisra	k1gFnSc1	Jisra
<g/>
'	'	kIx"	'
<g/>
el.	el.	k?	el.
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
zdroje	zdroj	k1gInSc2	zdroj
byl	být	k5eAaImAgInS	být
deník	deník	k1gInSc1	deník
provozován	provozovat	k5eAaImNgInS	provozovat
stranou	stranou	k6eAd1	stranou
Po	po	k7c4	po
<g/>
'	'	kIx"	'
<g/>
alej	alej	k1gFnSc4	alej
Agudat	Agudat	k1gMnSc2	Agudat
Jisra	Jisr	k1gMnSc2	Jisr
<g/>
'	'	kIx"	'
<g/>
el.	el.	k?	el.
Ta	ten	k3xDgFnSc1	ten
tehdy	tehdy	k6eAd1	tehdy
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
dobovým	dobový	k2eAgInSc7d1	dobový
trendem	trend	k1gInSc7	trend
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každá	každý	k3xTgFnSc1	každý
politická	politický	k2eAgFnSc1d1	politická
formace	formace	k1gFnSc1	formace
provozovala	provozovat	k5eAaImAgFnS	provozovat
svůj	svůj	k3xOyFgInSc4	svůj
stranický	stranický	k2eAgInSc4d1	stranický
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
vydávala	vydávat	k5eAaPmAgFnS	vydávat
dva	dva	k4xCgInPc4	dva
deníky	deník	k1gInPc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
ha-Kol	ha-Kola	k1gFnPc2	ha-Kola
<g/>
,	,	kIx,	,
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
Še	Še	k1gFnSc2	Še
<g/>
'	'	kIx"	'
<g/>
arim	arim	k1gMnSc1	arim
<g/>
.	.	kIx.	.
</s>
<s>
Ha-Kol	Ha-Kol	k1gInSc1	Ha-Kol
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
dle	dle	k7c2	dle
stavu	stav	k1gInSc2	stav
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1964	[number]	k4	1964
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
vydáván	vydávat	k5eAaImNgInS	vydávat
<g/>
.	.	kIx.	.
</s>
