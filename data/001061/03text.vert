<s>
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
Republika	republika	k1gFnSc1	republika
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Togem	Togo	k1gNnSc7	Togo
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Pobřežím	pobřeží	k1gNnSc7	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
jižní	jižní	k2eAgNnSc4d1	jižní
pobřeží	pobřeží	k1gNnSc4	pobřeží
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Guinejského	guinejský	k2eAgInSc2d1	guinejský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Accra	Accra	k1gFnSc1	Accra
<g/>
.	.	kIx.	.
</s>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
kolonizována	kolonizovat	k5eAaBmNgFnS	kolonizovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1470	[number]	k4	1470
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
usídlili	usídlit	k5eAaPmAgMnP	usídlit
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
postavili	postavit	k5eAaPmAgMnP	postavit
zde	zde	k6eAd1	zde
první	první	k4xOgFnSc7	první
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
mocná	mocný	k2eAgFnSc1d1	mocná
a	a	k8xC	a
bohatá	bohatý	k2eAgFnSc1d1	bohatá
Ašantská	Ašantský	k2eAgFnSc1d1	Ašantská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
třetí	třetí	k4xOgFnSc2	třetí
ašantsko-britské	ašantskoritský	k2eAgFnSc2d1	ašantsko-britský
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
britskému	britský	k2eAgInSc3d1	britský
imperialismu	imperialismus	k1gInSc3	imperialismus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
byla	být	k5eAaImAgFnS	být
Ghana	Ghana	k1gFnSc1	Ghana
državou	država	k1gFnSc7	država
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
první	první	k4xOgFnSc7	první
kolonií	kolonie	k1gFnSc7	kolonie
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Kwame	Kwam	k1gInSc5	Kwam
Nkrumah	Nkrumah	k1gInSc4	Nkrumah
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
panafrikanismu	panafrikanismus	k1gInSc2	panafrikanismus
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
mezivládní	mezivládní	k2eAgFnSc2d1	mezivládní
Organizace	organizace	k1gFnSc2	organizace
africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Africká	africký	k2eAgFnSc1d1	africká
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Nkrumah	Nkrumah	k1gMnSc1	Nkrumah
byl	být	k5eAaImAgMnS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
v	v	k7c6	v
ozbrojeném	ozbrojený	k2eAgInSc6d1	ozbrojený
státním	státní	k2eAgInSc6d1	státní
převratu	převrat	k1gInSc6	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
opustil	opustit	k5eAaPmAgMnS	opustit
cestu	cesta	k1gFnSc4	cesta
demokratického	demokratický	k2eAgNnSc2d1	demokratické
vládnutí	vládnutí	k1gNnSc2	vládnutí
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vzešlá	vzešlý	k2eAgFnSc1d1	vzešlá
z	z	k7c2	z
demokratických	demokratický	k2eAgFnPc2d1	demokratická
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
schopná	schopný	k2eAgFnSc1d1	schopná
čelit	čelit	k5eAaImF	čelit
narůstajícím	narůstající	k2eAgFnPc3d1	narůstající
hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
obtížím	obtíž	k1gFnPc3	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Edward	Edward	k1gMnSc1	Edward
Akufo-Addo	Akufo-Addo	k1gNnSc4	Akufo-Addo
jakož	jakož	k8xC	jakož
i	i	k9	i
vláda	vláda	k1gFnSc1	vláda
Kofiho	Kofi	k1gMnSc2	Kofi
Busii	Busius	k1gMnPc7	Busius
byli	být	k5eAaImAgMnP	být
svrhnuti	svrhnout	k5eAaPmNgMnP	svrhnout
nenásilným	násilný	k2eNgInSc7d1	nenásilný
převratem	převrat	k1gInSc7	převrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
pučů	puč	k1gInPc2	puč
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
začala	začít	k5eAaPmAgFnS	začít
vláda	vláda	k1gFnSc1	vláda
diktátora	diktátor	k1gMnSc2	diktátor
Jerryho	Jerry	k1gMnSc2	Jerry
Rawlingse	Rawlings	k1gMnSc2	Rawlings
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
platnost	platnost	k1gFnSc4	platnost
ústavy	ústav	k1gInPc4	ústav
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgMnS	zakázat
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
úpadku	úpadek	k1gInSc2	úpadek
stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInSc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
odešlo	odejít	k5eAaPmAgNnS	odejít
hledat	hledat	k5eAaImF	hledat
obživu	obživa	k1gFnSc4	obživa
do	do	k7c2	do
sousedních	sousední	k2eAgInPc2d1	sousední
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
Rawlings	Rawlings	k1gInSc4	Rawlings
k	k	k7c3	k
liberalizaci	liberalizace	k1gFnSc3	liberalizace
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
Rawlings	Rawlings	k1gInSc1	Rawlings
ve	v	k7c6	v
svobodných	svobodný	k2eAgFnPc6d1	svobodná
volbách	volba	k1gFnPc6	volba
zvolen	zvolit	k5eAaPmNgInS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
i	i	k9	i
podruhé	podruhé	k6eAd1	podruhé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
nedovolovala	dovolovat	k5eNaImAgFnS	dovolovat
třetí	třetí	k4xOgNnPc4	třetí
funkční	funkční	k2eAgNnPc4d1	funkční
období	období	k1gNnPc4	období
a	a	k8xC	a
proto	proto	k8xC	proto
Rawlingsova	Rawlingsův	k2eAgFnSc1d1	Rawlingsova
strana	strana	k1gFnSc1	strana
Národní	národní	k2eAgFnSc2d1	národní
demokratický	demokratický	k2eAgInSc4d1	demokratický
kongres	kongres	k1gInSc4	kongres
zvolila	zvolit	k5eAaPmAgFnS	zvolit
jako	jako	k9	jako
kandidáta	kandidát	k1gMnSc4	kandidát
dosavadního	dosavadní	k2eAgMnSc4d1	dosavadní
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
Johna	John	k1gMnSc4	John
Atta	Attus	k1gMnSc4	Attus
Millse	Mills	k1gMnSc4	Mills
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
jeho	jeho	k3xOp3gMnSc1	jeho
protikandidát	protikandidát	k1gMnSc1	protikandidát
John	John	k1gMnSc1	John
Kufuor	Kufuor	k1gMnSc1	Kufuor
z	z	k7c2	z
opoziční	opoziční	k2eAgFnSc2d1	opoziční
Nové	Nová	k1gFnSc2	Nová
vlastenecké	vlastenecký	k2eAgFnSc2d1	vlastenecká
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
jeho	jeho	k3xOp3gNnSc4	jeho
volební	volební	k2eAgNnSc4d1	volební
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vyšel	vyjít	k5eAaPmAgInS	vyjít
vítězně	vítězně	k6eAd1	vítězně
J.A.	J.A.	k1gFnSc4	J.A.
Mills	Millsa	k1gFnPc2	Millsa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
demokratizace	demokratizace	k1gFnSc2	demokratizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
tak	tak	k6eAd1	tak
došlo	dojít	k5eAaPmAgNnS	dojít
dvakrát	dvakrát	k6eAd1	dvakrát
k	k	k7c3	k
předání	předání	k1gNnSc3	předání
moci	moc	k1gFnSc2	moc
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
opozičními	opoziční	k2eAgFnPc7d1	opoziční
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
stabilitě	stabilita	k1gFnSc6	stabilita
demokracie	demokracie	k1gFnSc2	demokracie
v	v	k7c6	v
Ghaně	Ghana	k1gFnSc6	Ghana
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
převážně	převážně	k6eAd1	převážně
nížinnou	nížinný	k2eAgFnSc4d1	nížinná
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gNnPc2	její
hlavní	hlavní	k2eAgInSc1d1	hlavní
osu	osa	k1gFnSc4	osa
tvoří	tvořit	k5eAaImIp3nS	tvořit
řeka	řeka	k1gFnSc1	řeka
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
883	[number]	k4	883
m	m	kA	m
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Tožské	Tožský	k2eAgFnSc6d1	Tožský
vrchovině	vrchovina	k1gFnSc6	vrchovina
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Togem	Togo	k1gNnSc7	Togo
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc1	sever
země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
tropickým	tropický	k2eAgNnSc7d1	tropické
vnitrozemským	vnitrozemský	k2eAgNnSc7d1	vnitrozemské
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
jih	jih	k1gInSc1	jih
vlhkým	vlhký	k2eAgNnSc7d1	vlhké
rovníkovým	rovníkový	k2eAgNnSc7d1	rovníkové
klimatem	klima	k1gNnSc7	klima
<g/>
.	.	kIx.	.
</s>
<s>
Většině	většina	k1gFnSc3	většina
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
vévodí	vévodit	k5eAaImIp3nP	vévodit
savany	savana	k1gFnPc1	savana
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnPc1d1	jižní
části	část	k1gFnPc1	část
pak	pak	k6eAd1	pak
deštné	deštný	k2eAgInPc4d1	deštný
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
dvě	dva	k4xCgNnPc4	dva
období	období	k1gNnPc4	období
dešťů	dešť	k1gInPc2	dešť
(	(	kIx(	(
<g/>
květen-červen	květen-červen	k2eAgMnSc1d1	květen-červen
a	a	k8xC	a
srpen-září	srpenáří	k2eAgMnSc1d1	srpen-září
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
období	období	k1gNnSc1	období
dešťů	dešť	k1gInPc2	dešť
vrcholící	vrcholící	k2eAgFnSc2d1	vrcholící
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
únoru	únor	k1gInSc6	únor
přichází	přicházet	k5eAaImIp3nS	přicházet
horké	horký	k2eAgNnSc1d1	horké
vzdušné	vzdušný	k2eAgNnSc1d1	vzdušné
proudění	proudění	k1gNnSc1	proudění
ze	z	k7c2	z
Sahary	Sahara	k1gFnSc2	Sahara
zvané	zvaný	k2eAgNnSc1d1	zvané
harmatán	harmatán	k2eAgInSc1d1	harmatán
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
83	[number]	k4	83
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Ghany	Ghana	k1gFnSc2	Ghana
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
přehradních	přehradní	k2eAgFnPc2d1	přehradní
nádrží	nádrž	k1gFnPc2	nádrž
světa	svět	k1gInSc2	svět
-	-	kIx~	-
Jezero	jezero	k1gNnSc1	jezero
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
přehradní	přehradní	k2eAgFnSc2d1	přehradní
zdi	zeď	k1gFnSc2	zeď
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Akosombo	Akosomba	k1gFnSc5	Akosomba
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
520	[number]	k4	520
km	km	kA	km
severně	severně	k6eAd1	severně
k	k	k7c3	k
Yapei	Yape	k1gFnSc3	Yape
<g/>
.	.	kIx.	.
</s>
<s>
Ghanská	ghanský	k2eAgFnSc1d1	ghanská
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
oblastí	oblast	k1gFnPc2	oblast
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
jsou	být	k5eAaImIp3nP	být
uvedena	uveden	k2eAgNnPc1d1	uvedeno
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ashanti	Ashant	k1gMnPc1	Ashant
(	(	kIx(	(
<g/>
Kumasi	Kumas	k1gMnPc1	Kumas
<g/>
)	)	kIx)	)
Brong-Ahafo	Brong-Ahafo	k6eAd1	Brong-Ahafo
(	(	kIx(	(
<g/>
Sunyani	Sunyan	k1gMnPc1	Sunyan
<g/>
)	)	kIx)	)
Central	Central	k1gFnSc1	Central
Region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
Cape	capat	k5eAaImIp3nS	capat
Coast	Coast	k1gFnSc1	Coast
<g/>
)	)	kIx)	)
Eastern	Eastern	k1gInSc1	Eastern
Region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
Koforidua	Koforidua	k1gMnSc1	Koforidua
<g/>
)	)	kIx)	)
Greater	Greater	k1gMnSc1	Greater
Accra	Accra	k1gMnSc1	Accra
(	(	kIx(	(
<g/>
Akra	Akra	k1gMnSc1	Akra
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Northern	Northerna	k1gFnPc2	Northerna
Region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
Tamale	Tamala	k1gFnSc6	Tamala
<g/>
)	)	kIx)	)
Upper	Upper	k1gInSc1	Upper
East	East	k1gInSc1	East
(	(	kIx(	(
<g/>
Bolgatanga	Bolgatanga	k1gFnSc1	Bolgatanga
<g/>
)	)	kIx)	)
Upper	Upper	k1gMnSc1	Upper
West	West	k1gMnSc1	West
(	(	kIx(	(
<g/>
Wa	Wa	k1gMnSc1	Wa
<g/>
)	)	kIx)	)
Volta	Volta	k1gFnSc1	Volta
(	(	kIx(	(
<g/>
Ho	ho	k0	ho
<g/>
)	)	kIx)	)
Western	Western	kA	Western
Region	region	k1gInSc1	region
(	(	kIx(	(
<g/>
Sekondi	Sekond	k1gMnPc1	Sekond
<g/>
)	)	kIx)	)
Accra	Accro	k1gNnSc2	Accro
Kumasi	Kumas	k1gMnPc1	Kumas
Takoradi	Takorad	k1gMnPc1	Takorad
Tamale	Tamala	k1gFnSc3	Tamala
Cape	capat	k5eAaImIp3nS	capat
Coast	Coast	k1gFnSc1	Coast
Tema	Tema	k?	Tema
Ghana	Ghana	k1gFnSc1	Ghana
má	mít	k5eAaImIp3nS	mít
21	[number]	k4	21
029	[number]	k4	029
853	[number]	k4	853
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
úřední	úřední	k2eAgFnSc2d1	úřední
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
jazyky	jazyk	k1gInPc7	jazyk
a	a	k8xC	a
nářečími	nářečí	k1gNnPc7	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
Či	či	k8xC	či
(	(	kIx(	(
<g/>
Twi	Twi	k1gFnSc1	Twi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
hovoří	hovořit	k5eAaImIp3nP	hovořit
Ašantové	Ašantové	k?	Ašantové
soustředění	soustředění	k1gNnSc1	soustředění
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jazyk	jazyk	k1gInSc4	jazyk
z	z	k7c2	z
Akanské	Akanský	k2eAgFnSc2d1	Akanský
skupiny	skupina	k1gFnSc2	skupina
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Fanti	fantit	k5eAaImRp2nS	fantit
užívané	užívaný	k2eAgInPc1d1	užívaný
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Střediskem	středisko	k1gNnSc7	středisko
ašantské	ašantský	k2eAgFnSc2d1	ašantský
kultury	kultura	k1gFnSc2	kultura
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
Kumasi	Kumas	k1gMnPc1	Kumas
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
akanských	akanský	k2eAgInPc2d1	akanský
kmenů	kmen	k1gInPc2	kmen
(	(	kIx(	(
<g/>
44	[number]	k4	44
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
kmeny	kmen	k1gInPc7	kmen
Moshi-Dagomba	Moshi-Dagomb	k1gMnSc2	Moshi-Dagomb
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ewe	Ewe	k1gMnSc1	Ewe
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ga	Ga	k1gMnSc1	Ga
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
oba	dva	k4xCgMnPc1	dva
soustředěné	soustředěný	k2eAgFnSc6d1	soustředěná
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Běloši	běloch	k1gMnPc1	běloch
tvoří	tvořit	k5eAaImIp3nP	tvořit
asi	asi	k9	asi
0,2	[number]	k4	0,2
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Domorodá	domorodý	k2eAgNnPc1d1	domorodé
náboženství	náboženství	k1gNnPc1	náboženství
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
38	[number]	k4	38
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
následují	následovat	k5eAaImIp3nP	následovat
křesťané	křesťan	k1gMnPc1	křesťan
(	(	kIx(	(
<g/>
34	[number]	k4	34
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
muslimové	muslim	k1gMnPc1	muslim
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Muslimské	muslimský	k2eAgNnSc4d1	muslimské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
značné	značný	k2eAgFnSc3d1	značná
vnitřní	vnitřní	k2eAgFnSc3d1	vnitřní
migraci	migrace	k1gFnSc3	migrace
se	se	k3xPyFc4	se
řada	řada	k1gFnSc1	řada
muslimských	muslimský	k2eAgMnPc2d1	muslimský
obyvatel	obyvatel	k1gMnPc2	obyvatel
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
-	-	kIx~	-
například	například	k6eAd1	například
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
do	do	k7c2	do
zlatých	zlatý	k2eAgInPc2d1	zlatý
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Obuasi	Obuas	k1gMnPc1	Obuas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
západoafrické	západoafrický	k2eAgInPc4d1	západoafrický
poměry	poměr	k1gInPc4	poměr
má	mít	k5eAaImIp3nS	mít
Ghana	Ghana	k1gFnSc1	Ghana
bohaté	bohatý	k2eAgInPc4d1	bohatý
a	a	k8xC	a
různorodé	různorodý	k2eAgInPc4d1	různorodý
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
Ghana	Ghana	k1gFnSc1	Ghana
převážně	převážně	k6eAd1	převážně
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
země	země	k1gFnSc1	země
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
pracujících	pracující	k2eAgMnPc2d1	pracující
obyvatel	obyvatel	k1gMnPc2	obyvatel
zaměstnaných	zaměstnaný	k2eAgMnPc2d1	zaměstnaný
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tržní	tržní	k2eAgFnPc4d1	tržní
plodiny	plodina	k1gFnPc4	plodina
patří	patřit	k5eAaImIp3nP	patřit
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
kakaovými	kakaový	k2eAgInPc7d1	kakaový
polotovary	polotovar	k1gInPc7	polotovar
a	a	k8xC	a
tovary	tovar	k1gInPc1	tovar
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
vývozních	vývozní	k2eAgInPc2d1	vývozní
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
kokosové	kokosový	k2eAgInPc1d1	kokosový
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
palmové	palmový	k2eAgInPc1d1	palmový
produkty	produkt	k1gInPc1	produkt
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
máslovníku	máslovník	k1gInSc2	máslovník
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jedlý	jedlý	k2eAgInSc1d1	jedlý
tuk	tuk	k1gInSc1	tuk
<g/>
,	,	kIx,	,
a	a	k8xC	a
káva	káva	k1gFnSc1	káva
<g/>
.	.	kIx.	.
</s>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
také	také	k9	také
úspěšně	úspěšně	k6eAd1	úspěšně
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
méně	málo	k6eAd2	málo
tradiční	tradiční	k2eAgInPc4d1	tradiční
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
výrobky	výrobek	k1gInPc4	výrobek
-	-	kIx~	-
ananas	ananas	k1gInSc1	ananas
<g/>
,	,	kIx,	,
ledvinovníky	ledvinovník	k1gInPc1	ledvinovník
a	a	k8xC	a
pepř	pepř	k1gInSc1	pepř
<g/>
.	.	kIx.	.
</s>
<s>
Maniok	maniok	k1gInSc1	maniok
jedlý	jedlý	k2eAgInSc1d1	jedlý
<g/>
,	,	kIx,	,
sladké	sladký	k2eAgInPc1d1	sladký
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnPc1	kukuřice
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnPc1	rýže
<g/>
,	,	kIx,	,
arašídy	arašíd	k1gInPc1	arašíd
<g/>
,	,	kIx,	,
proso	proso	k1gNnSc1	proso
<g/>
,	,	kIx,	,
a	a	k8xC	a
čirok	čirok	k1gInSc1	čirok
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
potraviny	potravina	k1gFnPc4	potravina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ghaně	Ghana	k1gFnSc6	Ghana
se	se	k3xPyFc4	se
také	také	k9	také
těží	těžet	k5eAaImIp3nS	těžet
(	(	kIx(	(
<g/>
a	a	k8xC	a
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
hlavně	hlavně	k9	hlavně
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
diamanty	diamant	k1gInPc1	diamant
<g/>
,	,	kIx,	,
manganová	manganový	k2eAgFnSc1d1	manganová
ruda	ruda	k1gFnSc1	ruda
<g/>
,	,	kIx,	,
a	a	k8xC	a
bauxit	bauxit	k1gInSc1	bauxit
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
nedostatečně	dostatečně	k6eNd1	dostatečně
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
dopravní	dopravní	k2eAgFnSc4d1	dopravní
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
britskou	britský	k2eAgFnSc7d1	britská
koloniální	koloniální	k2eAgFnSc7d1	koloniální
správou	správa	k1gFnSc7	správa
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zastaralá	zastaralý	k2eAgFnSc1d1	zastaralá
<g/>
,	,	kIx,	,
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
linka	linka	k1gFnSc1	linka
spojující	spojující	k2eAgFnSc1d1	spojující
město	město	k1gNnSc4	město
Kumasi	Kumas	k1gMnPc1	Kumas
a	a	k8xC	a
významný	významný	k2eAgInSc1d1	významný
přístav	přístav	k1gInSc1	přístav
Takoradi	Takorad	k1gMnPc1	Takorad
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Temě	Tema	k1gFnSc6	Tema
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
zejména	zejména	k9	zejména
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
dálnice	dálnice	k1gFnSc1	dálnice
spojuje	spojovat	k5eAaImIp3nS	spojovat
Akru	akr	k1gInSc2	akr
a	a	k8xC	a
Temu	Temu	k?	Temu
<g/>
,	,	kIx,	,
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	se	k3xPyFc4	se
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
úseku	úsek	k1gInSc6	úsek
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
z	z	k7c2	z
Akry	akr	k1gInPc1	akr
do	do	k7c2	do
Kumasi	Kumas	k1gMnPc5	Kumas
<g/>
.	.	kIx.	.
</s>
<s>
Kotokovo	Kotokův	k2eAgNnSc1d1	Kotokův
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Akře	Akře	k1gFnSc6	Akře
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
významná	významný	k2eAgFnSc1d1	významná
spojnice	spojnice	k1gFnSc1	spojnice
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
městy	město	k1gNnPc7	město
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
i	i	k9	i
vnitrostátní	vnitrostátní	k2eAgInPc1d1	vnitrostátní
letecké	letecký	k2eAgInPc1d1	letecký
spoje	spoj	k1gInPc1	spoj
do	do	k7c2	do
Kumasi	Kumas	k1gMnPc1	Kumas
a	a	k8xC	a
Tamale	Tamala	k1gFnSc6	Tamala
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
pochází	pocházet	k5eAaImIp3nS	pocházet
70	[number]	k4	70
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
kakaa	kakao	k1gNnSc2	kakao
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,5	[number]	k4	1,5
miliónu	milión	k4xCgInSc2	milión
rodinných	rodinný	k2eAgNnPc2d1	rodinné
hospodářství	hospodářství	k1gNnSc2	hospodářství
ve	v	k7c6	v
státech	stát	k1gInPc6	stát
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
Kamerun	Kamerun	k1gInSc1	Kamerun
a	a	k8xC	a
Nigérie	Nigérie	k1gFnSc1	Nigérie
je	být	k5eAaImIp3nS	být
životně	životně	k6eAd1	životně
závislých	závislý	k2eAgFnPc2d1	závislá
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
kakaovníků	kakaovník	k1gInPc2	kakaovník
<g/>
.	.	kIx.	.
</s>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
kakaa	kakao	k1gNnSc2	kakao
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
lukrativní	lukrativní	k2eAgMnSc1d1	lukrativní
<g/>
,	,	kIx,	,
rolníci	rolník	k1gMnPc1	rolník
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
a	a	k8xC	a
najímají	najímat	k5eAaImIp3nP	najímat
si	se	k3xPyFc3	se
nejlevnější	levný	k2eAgFnSc4d3	nejlevnější
pracovní	pracovní	k2eAgFnSc4d1	pracovní
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
unášeni	unášet	k5eAaImNgMnP	unášet
z	z	k7c2	z
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
Interpolu	interpol	k1gInSc2	interpol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
především	především	k9	především
z	z	k7c2	z
Mali	Mali	k1gNnSc2	Mali
a	a	k8xC	a
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k1gNnSc1	Faso
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
nuceny	nutit	k5eAaImNgFnP	nutit
pracovat	pracovat	k5eAaImF	pracovat
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
pesticidy	pesticid	k1gInPc7	pesticid
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
zachází	zacházet	k5eAaImIp3nS	zacházet
s	s	k7c7	s
mačetami	mačeta	k1gFnPc7	mačeta
<g/>
,	,	kIx,	,
nosí	nosit	k5eAaImIp3nP	nosit
těžké	těžký	k2eAgInPc1d1	těžký
náklady	náklad	k1gInPc1	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
výzkumu	výzkum	k1gInSc2	výzkum
Payson	Payson	k1gNnSc1	Payson
Centra	centrum	k1gNnSc2	centrum
při	při	k7c6	při
Tulane	Tulan	k1gMnSc5	Tulan
University	universita	k1gFnSc2	universita
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
nuceno	nucen	k2eAgNnSc1d1	nuceno
pracovat	pracovat	k5eAaImF	pracovat
15	[number]	k4	15
procent	procento	k1gNnPc2	procento
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
polovina	polovina	k1gFnSc1	polovina
pracovníků	pracovník	k1gMnPc2	pracovník
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
byly	být	k5eAaImAgFnP	být
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
polovina	polovina	k1gFnSc1	polovina
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
zranila	zranit	k5eAaPmAgFnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Ghaně	Ghana	k1gFnSc6	Ghana
pracuje	pracovat	k5eAaImIp3nS	pracovat
téměř	téměř	k6eAd1	téměř
44	[number]	k4	44
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
5	[number]	k4	5
-	-	kIx~	-
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
USA	USA	kA	USA
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
kampaně	kampaň	k1gFnSc2	kampaň
mířící	mířící	k2eAgFnSc2d1	mířící
na	na	k7c6	na
velké	velká	k1gFnSc6	velká
zpracovatele	zpracovatel	k1gMnSc2	zpracovatel
kakaa	kakao	k1gNnSc2	kakao
(	(	kIx(	(
<g/>
Nestlé	Nestlý	k2eAgInPc4d1	Nestlý
<g/>
,	,	kIx,	,
Kraft	Kraft	k2eAgInSc4d1	Kraft
Foods	Foods	k1gInSc4	Foods
<g/>
,	,	kIx,	,
Hershey	Hershey	k1gInPc4	Hershey
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
výrobky	výrobek	k1gInPc1	výrobek
nevyužívaly	využívat	k5eNaImAgInP	využívat
kakao	kakao	k1gNnSc4	kakao
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
dětská	dětský	k2eAgFnSc1d1	dětská
práce	práce	k1gFnSc1	práce
a	a	k8xC	a
žádající	žádající	k2eAgInPc1d1	žádající
výrobky	výrobek	k1gInPc1	výrobek
fair	fair	k6eAd1	fair
trade	trade	k6eAd1	trade
<g/>
.	.	kIx.	.
</s>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
vzešlý	vzešlý	k2eAgMnSc1d1	vzešlý
ze	z	k7c2	z
všeobecných	všeobecný	k2eAgFnPc2d1	všeobecná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
zároveň	zároveň	k6eAd1	zároveň
řídí	řídit	k5eAaImIp3nS	řídit
moc	moc	k1gFnSc4	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Státní	státní	k2eAgFnSc7d1	státní
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
o	o	k7c6	o
201	[number]	k4	201
členech	člen	k1gInPc6	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
jeden	jeden	k4xCgMnSc1	jeden
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Poslanci	poslanec	k1gMnPc1	poslanec
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
politickými	politický	k2eAgMnPc7d1	politický
uskupeními	uskupení	k1gNnPc7	uskupení
jsou	být	k5eAaImIp3nP	být
Nová	nový	k2eAgFnSc1d1	nová
vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
NPP	NPP	kA	NPP
<g/>
)	)	kIx)	)
a	a	k8xC	a
Národní	národní	k2eAgInSc1d1	národní
demokratický	demokratický	k2eAgInSc1d1	demokratický
kongres	kongres	k1gInSc1	kongres
(	(	kIx(	(
<g/>
NDC	NDC	kA	NDC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nynějším	nynější	k2eAgMnSc7d1	nynější
prezidentem	prezident	k1gMnSc7	prezident
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Dramani	Draman	k1gMnPc1	Draman
MAHAMA	MAHAMA	k?	MAHAMA
(	(	kIx(	(
<g/>
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Johna	John	k1gMnSc4	John
Attu	Atta	k1gMnSc4	Atta
Millse	Mills	k1gMnSc4	Mills
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
úlohy	úloha	k1gFnSc2	úloha
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
rovněž	rovněž	k9	rovněž
nejvyšším	vysoký	k2eAgMnSc7d3	nejvyšší
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
viceprezidenta	viceprezident	k1gMnSc4	viceprezident
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezidentem	viceprezident	k1gMnSc7	viceprezident
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2001-2009	[number]	k4	2001-2009
byl	být	k5eAaImAgMnS	být
Alhaji	Alhaje	k1gFnSc4	Alhaje
Aliu	Alius	k1gInSc2	Alius
Mahama	Mahama	k?	Mahama
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	on	k3xPp3gInPc4	on
jím	jíst	k5eAaImIp1nS	jíst
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
Kwesi	Kwese	k1gFnSc3	Kwese
Bekoe	Beko	k1gFnSc2	Beko
AMISSAH-ARTHUR	AMISSAH-ARTHUR	k1gFnSc2	AMISSAH-ARTHUR
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
John	John	k1gMnSc1	John
Dramani	Draman	k1gMnPc1	Draman
Mahama	Mahama	k?	Mahama
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
poražen	porazit	k5eAaPmNgMnS	porazit
vůdcem	vůdce	k1gMnSc7	vůdce
opozice	opozice	k1gFnSc2	opozice
Nana	Nan	k1gInSc2	Nan
Akufo-Addo	Akufo-Addo	k1gNnSc1	Akufo-Addo
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
prezident	prezident	k1gMnSc1	prezident
Mahama	Mahama	k?	Mahama
porážku	porážka	k1gFnSc4	porážka
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Ghana	Ghana	k1gFnSc1	Ghana
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
ohledů	ohled	k1gInPc2	ohled
je	být	k5eAaImIp3nS	být
ghanský	ghanský	k2eAgInSc1d1	ghanský
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
odrazem	odraz	k1gInSc7	odraz
modelů	model	k1gInPc2	model
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
6.3	[number]	k4	6.3
<g/>
.1957	.1957	k4	.1957
<g/>
-	-	kIx~	-
<g/>
24.6	[number]	k4	24.6
<g/>
.1957	.1957	k4	.1957
-	-	kIx~	-
Sir	sir	k1gMnSc1	sir
Charles	Charles	k1gMnSc1	Charles
Noble	Noble	k1gInSc4	Noble
Arden-Clarke	Arden-Clark	k1gInSc2	Arden-Clark
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
britskou	britský	k2eAgFnSc4d1	britská
monarchii	monarchie	k1gFnSc4	monarchie
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
24.6	[number]	k4	24.6
<g/>
.1957	.1957	k4	.1957
<g/>
-	-	kIx~	-
<g/>
1.7	[number]	k4	1.7
<g/>
.1960	.1960	k4	.1960
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
William	William	k1gInSc1	William
Francis	Francis	k1gFnSc2	Francis
Hare	Har	k1gFnSc2	Har
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Listowel	Listowel	k1gMnSc1	Listowel
-	-	kIx~	-
generální	generální	k2eAgMnSc1d1	generální
guvernér	guvernér	k1gMnSc1	guvernér
reprezentující	reprezentující	k2eAgFnSc4d1	reprezentující
britskou	britský	k2eAgFnSc4d1	britská
monarchii	monarchie	k1gFnSc4	monarchie
jako	jako	k8xC	jako
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
1.7	[number]	k4	1.7
<g/>
.1960	.1960	k4	.1960
<g/>
-	-	kIx~	-
<g/>
24.2	[number]	k4	24.2
<g/>
.1966	.1966	k4	.1966
-	-	kIx~	-
Kofi	Kof	k1gMnSc5	Kof
Kwame	Kwam	k1gMnSc5	Kwam
Nkrumah	Nkrumah	k1gMnSc1	Nkrumah
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
CPP	CPP	kA	CPP
24.2	[number]	k4	24.2
<g/>
.1966	.1966	k4	.1966
<g/>
-	-	kIx~	-
<g/>
3.9	[number]	k4	3.9
<g/>
.1969	.1969	k4	.1969
-	-	kIx~	-
Joseph	Joseph	k1gMnSc1	Joseph
Arthur	Arthur	k1gMnSc1	Arthur
Ankrah	Ankrah	k1gMnSc1	Ankrah
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
2.4	[number]	k4	2.4
<g/>
.1969	.1969	k4	.1969
<g/>
;	;	kIx,	;
předseda	předseda	k1gMnSc1	předseda
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Akwasi	Akwas	k1gMnPc1	Akwas
Amankwaa	Amankwaus	k1gMnSc2	Amankwaus
Afrifa	Afrif	k1gMnSc2	Afrif
(	(	kIx(	(
<g/>
od	od	k7c2	od
2.4	[number]	k4	2.4
<g/>
.1969	.1969	k4	.1969
<g/>
;	;	kIx,	;
předseda	předseda	k1gMnSc1	předseda
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Willie	Willie	k1gFnSc2	Willie
Kofi	Kof	k1gFnSc2	Kof
Harlley	Harllea	k1gFnSc2	Harllea
(	(	kIx(	(
<g/>
bezp	bezp	k1gMnSc1	bezp
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Kotoka	Kotoka	k1gMnSc1	Kotoka
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
17.4	[number]	k4	17.4
<g/>
.1967	.1967	k4	.1967
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Albert	Albert	k1gMnSc1	Albert
Kwesi	Kwese	k1gFnSc4	Kwese
Ocran	Ocrana	k1gFnPc2	Ocrana
(	(	kIx(	(
<g/>
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Rada	rada	k1gFnSc1	rada
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
3.9	[number]	k4	3.9
<g/>
.1969	.1969	k4	.1969
<g/>
-	-	kIx~	-
<g/>
7.8	[number]	k4	7.8
<g/>
.1970	.1970	k4	.1970
-	-	kIx~	-
Akwasi	Akwas	k1gMnPc1	Akwas
Amankwaa	Amankwaus	k1gMnSc2	Amankwaus
Afrifa	Afrif	k1gMnSc2	Afrif
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Willie	Willie	k1gFnSc2	Willie
Kofi	Kof	k1gFnSc2	Kof
Harlley	Harllea	k1gFnSc2	Harllea
(	(	kIx(	(
<g/>
bezp	bezp	k1gInSc1	bezp
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Kwesi	Kwese	k1gFnSc4	Kwese
Ocran	Ocrana	k1gFnPc2	Ocrana
(	(	kIx(	(
<g/>
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
-	-	kIx~	-
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
komise	komise	k1gFnSc1	komise
7.8	[number]	k4	7.8
<g/>
.1970	.1970	k4	.1970
<g/>
-	-	kIx~	-
<g/>
31.8	[number]	k4	31.8
<g/>
.1970	.1970	k4	.1970
-	-	kIx~	-
Nii	Nii	k1gFnSc1	Nii
Amaa	Amaum	k1gNnSc2	Amaum
Ollennu	Ollenn	k1gInSc2	Ollenn
-	-	kIx~	-
úřadující	úřadující	k2eAgMnSc1d1	úřadující
prezident	prezident	k1gMnSc1	prezident
31.8	[number]	k4	31.8
<g/>
.1970	.1970	k4	.1970
<g/>
-	-	kIx~	-
<g/>
13.1	[number]	k4	13.1
<g/>
.1972	.1972	k4	.1972
-	-	kIx~	-
Edward	Edward	k1gMnSc1	Edward
A.	A.	kA	A.
Akufo-Addo	Akufo-Addo	k1gNnSc1	Akufo-Addo
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
bezp	bezp	k1gInSc1	bezp
<g/>
.	.	kIx.	.
13.1	[number]	k4	13.1
<g/>
.1972	.1972	k4	.1972
<g/>
-	-	kIx~	-
<g/>
9.10	[number]	k4	9.10
<g/>
.1975	.1975	k4	.1975
-	-	kIx~	-
Ignatius	Ignatius	k1gInSc4	Ignatius
Kutu	kut	k2eAgFnSc4d1	Kuta
Acheampong	Acheampong	k1gMnSc1	Acheampong
(	(	kIx(	(
<g/>
předseda	předseda	k1gMnSc1	předseda
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
J.	J.	kA	J.
H.	H.	kA	H.
Cobbina	Cobbina	k1gFnSc1	Cobbina
<g/>
,	,	kIx,	,
C.	C.	kA	C.
D.	D.	kA	D.
Benni	Benn	k1gMnPc1	Benn
<g/>
,	,	kIx,	,
Anthony	Anthona	k1gFnPc1	Anthona
Hugh	Hugh	k1gInSc1	Hugh
Selormey	Selormea	k1gFnSc2	Selormea
(	(	kIx(	(
<g/>
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kwame	Kwam	k1gInSc5	Kwam
R.	R.	kA	R.
M.	M.	kA	M.
Baah	Baah	k1gMnSc1	Baah
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kwame	Kwam	k1gInSc5	Kwam
B.	B.	kA	B.
Agbo	Agbo	k1gNnSc4	Agbo
(	(	kIx(	(
<g/>
voj.	voj.	k?	voj.
<g/>
)	)	kIx)	)
-	-	kIx~	-
Rada	rada	k1gFnSc1	rada
národní	národní	k2eAgFnSc2d1	národní
spásy	spása	k1gFnSc2	spása
9.10	[number]	k4	9.10
<g/>
.1975	.1975	k4	.1975
<g/>
-	-	kIx~	-
<g/>
5.7	[number]	k4	5.7
<g/>
.1978	.1978	k4	.1978
-	-	kIx~	-
Ignatius	Ignatius	k1gInSc4	Ignatius
Kutu	kut	k2eAgFnSc4d1	Kuta
Acheampong	Acheampong	k1gMnSc1	Acheampong
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
5.7	[number]	k4	5.7
<g/>
.1978	.1978	k4	.1978
<g/>
-	-	kIx~	-
<g/>
4.6	[number]	k4	4.6
<g/>
.1979	.1979	k4	.1979
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Frederick	Frederick	k1gMnSc1	Frederick
"	"	kIx"	"
<g/>
Fred	Fred	k1gMnSc1	Fred
<g/>
"	"	kIx"	"
William	William	k1gInSc1	William
Kwasi	Kwase	k1gFnSc4	Kwase
Akuffo	Akuffo	k1gNnSc1	Akuffo
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
vojenské	vojenský	k2eAgFnSc2d1	vojenská
rady	rada	k1gFnSc2	rada
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
4.6	[number]	k4	4.6
<g/>
.1979	.1979	k4	.1979
<g/>
-	-	kIx~	-
<g/>
24.9	[number]	k4	24.9
<g/>
.1979	.1979	k4	.1979
-	-	kIx~	-
Jerry	Jerra	k1gFnSc2	Jerra
John	John	k1gMnSc1	John
Rawlings	Rawlings	k1gInSc1	Rawlings
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Revoluční	revoluční	k2eAgFnSc2d1	revoluční
rady	rada	k1gFnSc2	rada
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
24.9	[number]	k4	24.9
<g/>
.1979	.1979	k4	.1979
<g/>
-	-	kIx~	-
<g/>
31.12	[number]	k4	31.12
<g/>
.1981	.1981	k4	.1981
-	-	kIx~	-
Hilla	Hill	k1gMnSc4	Hill
<g />
.	.	kIx.	.
</s>
<s>
Limann	Limann	k1gMnSc1	Limann
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
PNP	PNP	kA	PNP
31.12	[number]	k4	31.12
<g/>
.1981	.1981	k4	.1981
<g/>
-	-	kIx~	-
<g/>
7.1	[number]	k4	7.1
<g/>
.1993	.1993	k4	.1993
-	-	kIx~	-
Jerry	Jerra	k1gFnSc2	Jerra
John	John	k1gMnSc1	John
Rawlings	Rawlings	k1gInSc1	Rawlings
-	-	kIx~	-
předseda	předseda	k1gMnSc1	předseda
Prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
rady	rada	k1gFnSc2	rada
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
<g/>
;	;	kIx,	;
voj.	voj.	k?	voj.
7.1	[number]	k4	7.1
<g/>
.1993	.1993	k4	.1993
<g/>
-	-	kIx~	-
<g/>
7.1	[number]	k4	7.1
<g/>
.2001	.2001	k4	.2001
-	-	kIx~	-
Jerry	Jerra	k1gFnSc2	Jerra
John	John	k1gMnSc1	John
Rawlings	Rawlings	k1gInSc1	Rawlings
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NDC	NDC	kA	NDC
7.1	[number]	k4	7.1
<g/>
.2001	.2001	k4	.2001
<g/>
-	-	kIx~	-
<g/>
7.1	[number]	k4	7.1
<g/>
.2009	.2009	k4	.2009
-	-	kIx~	-
John	John	k1gMnSc1	John
Agyekum	Agyekum	k1gNnSc1	Agyekum
Kufuor	Kufuor	k1gMnSc1	Kufuor
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NPP	NPP	kA	NPP
7.1	[number]	k4	7.1
<g/>
.2009	.2009	k4	.2009
<g/>
-	-	kIx~	-
<g/>
24.7	[number]	k4	24.7
<g/>
.2012	.2012	k4	.2012
-	-	kIx~	-
John	John	k1gMnSc1	John
Evans	Evans	k1gInSc4	Evans
Atta	Att	k1gInSc2	Att
Mills	Mills	k1gInSc1	Mills
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NDC	NDC	kA	NDC
24.7	[number]	k4	24.7
<g/>
.2012	.2012	k4	.2012
<g/>
-současnost	oučasnost	k1gFnSc1	-současnost
-	-	kIx~	-
John	John	k1gMnSc1	John
Dramani	Draman	k1gMnPc1	Draman
Mahama	Mahama	k?	Mahama
-	-	kIx~	-
prezident	prezident	k1gMnSc1	prezident
<g/>
;	;	kIx,	;
NDC	NDC	kA	NDC
</s>
