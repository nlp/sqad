<s>
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1
liraTürk	liraTürk	k1gInSc1
lirası	lirası	k?
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
Turecké	turecký	k2eAgFnSc3d1
minceZemě	minceZema	k1gFnSc3
</s>
<s>
Turecko	Turecko	k1gNnSc4
TureckoSeverní	tureckoseverní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
TRY	TRY	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
25,2	25,2	k4
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
Symbol	symbol	k1gInSc1
</s>
<s>
Dílčí	dílčí	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
</s>
<s>
kuruş	kuruş	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
100	#num#	k4
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
kuruş	kuruş	k?
<g/>
,	,	kIx,
1	#num#	k4
lira	lira	k1gFnSc1
Bankovky	bankovka	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
je	být	k5eAaImIp3nS
měna	měna	k1gFnSc1
Turecké	turecký	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Turecka	Turecko	k1gNnSc2
tuto	tento	k3xDgFnSc4
měnu	měna	k1gFnSc4
používá	používat	k5eAaImIp3nS
i	i	k9
Severokyperská	severokyperský	k2eAgFnSc1d1
turecká	turecký	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
de	de	k?
facto	facto	k1gNnSc4
nezávislý	závislý	k2eNgInSc1d1
stát	stát	k1gInSc1
na	na	k7c6
ostrově	ostrov	k1gInSc6
Kypr	Kypr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
není	být	k5eNaImIp3nS
celosvětově	celosvětově	k6eAd1
uznán	uznat	k5eAaPmNgInS
žádným	žádný	k3yNgInSc7
státem	stát	k1gInSc7
kromě	kromě	k7c2
právě	právě	k9
Turecka	Turecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
liry	lira	k1gFnSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
„	„	k?
<g/>
kuruş	kuruş	k?
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
liry	lira	k1gFnSc2
je	být	k5eAaImIp3nS
TRY	TRY	kA
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
Lirou	lira	k1gFnSc7
se	se	k3xPyFc4
platilo	platit	k5eAaImAgNnS
ještě	ještě	k9
na	na	k7c6
území	území	k1gNnSc6
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
nahradila	nahradit	k5eAaPmAgFnS
měny	měna	k1gFnPc4
akçe	akçe	k1gNnSc2
a	a	k8xC
kuruş	kuruş	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachována	zachován	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
i	i	k9
po	po	k7c6
vyhlášení	vyhlášení	k1gNnSc6
republiky	republika	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
této	tento	k3xDgFnSc2
doby	doba	k1gFnSc2
až	až	k9
dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
každé	každý	k3xTgFnSc6
bankovce	bankovka	k1gFnSc6
vyobrazen	vyobrazen	k2eAgMnSc1d1
zakladatel	zakladatel	k1gMnSc1
moderního	moderní	k2eAgNnSc2d1
Turecka	Turecko	k1gNnSc2
<g/>
,	,	kIx,
Mustafa	Mustaf	k1gMnSc2
Kemal	Kemal	k1gMnSc1
Atatürk	Atatürk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
až	až	k9
do	do	k7c2
konce	konec	k1gInSc2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byly	být	k5eAaImAgInP
v	v	k7c6
oběhu	oběh	k1gInSc6
i	i	k9
bankovky	bankovka	k1gFnPc1
s	s	k7c7
portrétem	portrét	k1gInSc7
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
nástupce	nástupce	k1gMnSc2
<g/>
,	,	kIx,
prezidentem	prezident	k1gMnSc7
İ	İ	k1gMnSc7
İ	İ	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
proběhla	proběhnout	k5eAaPmAgFnS
inflace	inflace	k1gFnSc1
<g/>
;	;	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
za	za	k7c2
1	#num#	k4
USD	USD	kA
koupit	koupit	k5eAaPmF
9	#num#	k4
lir	lira	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
už	už	k6eAd1
1,65	1,65	k4
milionu	milion	k4xCgInSc2
lir	lira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiér	premiér	k1gMnSc1
země	země	k1gFnSc1
<g/>
,	,	kIx,
Recep	Recep	k1gMnSc1
Tayyip	Tayyip	k1gMnSc1
Erdoğ	Erdoğ	k1gMnSc1
toto	tento	k3xDgNnSc4
označil	označit	k5eAaPmAgMnS
za	za	k7c4
národní	národní	k2eAgFnSc4d1
ostudu	ostuda	k1gFnSc4
<g/>
,	,	kIx,
následně	následně	k6eAd1
parlament	parlament	k1gInSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
roku	rok	k1gInSc2
2003	#num#	k4
schválil	schválit	k5eAaPmAgInS
plán	plán	k1gInSc1
měnové	měnový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
<g/>
,	,	kIx,
ta	ten	k3xDgFnSc1
z	z	k7c2
bankovek	bankovka	k1gFnPc2
odstranila	odstranit	k5eAaPmAgFnS
6	#num#	k4
nul	nula	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Provedena	proveden	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Při	při	k7c6
měnové	měnový	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
se	se	k3xPyFc4
na	na	k7c6
začátku	začátek	k1gInSc6
roku	rok	k1gInSc2
2005	#num#	k4
do	do	k7c2
oběhu	oběh	k1gInSc2
dostala	dostat	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
série	série	k1gFnSc1
jak	jak	k6eAd1
mincí	mince	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
bankovek	bankovka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Současné	současný	k2eAgFnPc1d1
mince	mince	k1gFnPc1
jsou	být	k5eAaImIp3nP
raženy	razit	k5eAaImNgFnP
v	v	k7c6
hodnotách	hodnota	k1gFnPc6
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
kuruşů	kuruş	k1gMnPc2
a	a	k8xC
1	#num#	k4
lira	lira	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
bankovkách	bankovka	k1gFnPc6
8	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
tiskly	tisknout	k5eAaImAgFnP
mezi	mezi	k7c4
roky	rok	k1gInPc4
2005	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
nominální	nominální	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
zapsána	zapsat	k5eAaPmNgFnS
s	s	k7c7
názvem	název	k1gInSc7
Nová	nový	k2eAgFnSc1d1
turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
bankovky	bankovka	k1gFnPc1
měly	mít	k5eAaImAgFnP
nominální	nominální	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
a	a	k8xC
100	#num#	k4
lir	lira	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
oběhu	oběh	k1gInSc2
dostala	dostat	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
<g/>
,	,	kIx,
již	již	k6eAd1
9	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc1
bankovek	bankovka	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
je	být	k5eAaImIp3nS
nominální	nominální	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
zapsaná	zapsaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
<g/>
,	,	kIx,
slovo	slovo	k1gNnSc1
Nová	Nová	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gInPc6
již	již	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
nejnovější	nový	k2eAgFnPc1d3
bankovky	bankovka	k1gFnPc1
mají	mít	k5eAaImIp3nP
nominální	nominální	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
lir	lira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnSc2
8	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnPc1
zůstaly	zůstat	k5eAaPmAgFnP
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2009	#num#	k4
<g/>
,	,	kIx,
od	od	k7c2
začátku	začátek	k1gInSc2
2010	#num#	k4
jsou	být	k5eAaImIp3nP
směnitelné	směnitelný	k2eAgInPc1d1
u	u	k7c2
turecké	turecký	k2eAgFnSc2d1
centrální	centrální	k2eAgFnSc2d1
banky	banka	k1gFnSc2
za	za	k7c4
bankovky	bankovka	k1gFnPc4
9	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Bankovky	bankovka	k1gFnPc1
9	#num#	k4
<g/>
.	.	kIx.
série	série	k1gFnSc2
</s>
<s>
VyobrazeníHodnotaRozměryBarva	VyobrazeníHodnotaRozměryBarva	k6eAd1
</s>
<s>
AversRevers	AversRevers	k6eAd1
</s>
<s>
5	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
64	#num#	k4
×	×	k?
130	#num#	k4
mm	mm	kA
</s>
<s>
hnědá	hnědat	k5eAaImIp3nS
</s>
<s>
10	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
64	#num#	k4
×	×	k?
136	#num#	k4
mm	mm	kA
</s>
<s>
červená	červený	k2eAgFnSc1d1
</s>
<s>
20	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
68	#num#	k4
×	×	k?
142	#num#	k4
mm	mm	kA
</s>
<s>
zelená	zelený	k2eAgFnSc1d1
</s>
<s>
50	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
68	#num#	k4
×	×	k?
148	#num#	k4
mm	mm	kA
</s>
<s>
oranžová	oranžový	k2eAgFnSc1d1
</s>
<s>
100	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
72	#num#	k4
×	×	k?
154	#num#	k4
mm	mm	kA
</s>
<s>
modrá	modrý	k2eAgFnSc1d1
</s>
<s>
200	#num#	k4
lir	lira	k1gFnPc2
</s>
<s>
72	#num#	k4
×	×	k?
160	#num#	k4
mm	mm	kA
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Asie	Asie	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
měn	měna	k1gFnPc2
Evropy	Evropa	k1gFnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
jak	jak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdražování	zdražování	k1gNnPc1
podle	podle	k7c2
Erdogana	Erdogan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inflace	inflace	k1gFnSc1
v	v	k7c6
Turecku	Turecko	k1gNnSc6
překonala	překonat	k5eAaPmAgFnS
hranici	hranice	k1gFnSc4
25	#num#	k4
procent	procento	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2018-11-05	2018-11-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kypr	Kypr	k1gInSc1
a	a	k8xC
Malta	Malta	k1gFnSc1
se	se	k3xPyFc4
připojily	připojit	k5eAaPmAgInP
k	k	k7c3
eurozóně	eurozóna	k1gFnSc3
<g/>
.	.	kIx.
www.finance.cz	www.finance.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Turecko	Turecko	k1gNnSc1
umazalo	umazat	k5eAaPmAgNnS
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
měny	měna	k1gFnSc2
šest	šest	k4xCc4
nul	nula	k1gFnPc2
-	-	kIx~
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.novinky.cz	www.novinky.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Turecká	turecký	k2eAgFnSc1d1
centrální	centrální	k2eAgFnSc1d1
banka	banka	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
turecky	turecky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
vládní	vládní	k2eAgInSc1d1
plán	plán	k1gInSc1
měnové	měnový	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Bankovky	bankovka	k1gFnPc1
Turecka	Turecko	k1gNnSc2
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
od	od	k7c2
60	#num#	k4
<g/>
-	-	kIx~
<g/>
70	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
do	do	k7c2
současnosti	současnost	k1gFnSc2
</s>
<s>
Měny	měna	k1gFnPc4
Asie	Asie	k1gFnSc2
Severní	severní	k2eAgFnSc2d1
</s>
<s>
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
Střední	střední	k2eAgFnSc2d1
</s>
<s>
Kazachstánský	kazachstánský	k2eAgInSc1d1
tenge	tenge	k1gInSc1
•	•	k?
Kyrgyzský	kyrgyzský	k2eAgInSc1d1
som	soma	k1gFnPc2
•	•	k?
Tádžický	tádžický	k2eAgInSc1d1
somoni	somoň	k1gFnSc3
•	•	k?
Turkmenský	turkmenský	k2eAgMnSc1d1
manat	manat	k2eAgMnSc1d1
•	•	k?
Uzbecký	uzbecký	k2eAgInSc1d1
sum	suma	k1gFnPc2
Jihozápadní	jihozápadní	k2eAgNnPc5d1
</s>
<s>
Abchazský	abchazský	k2eAgInSc1d1
apsar	apsar	k1gInSc1
•	•	k?
Arménský	arménský	k2eAgInSc1d1
dram	drama	k1gFnPc2
•	•	k?
Ázerbájdžánský	ázerbájdžánský	k2eAgInSc1d1
manat	manat	k1gInSc1
•	•	k?
Bahrajnský	Bahrajnský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Irácký	irácký	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Íránský	íránský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Izraelský	izraelský	k2eAgInSc1d1
šekel	šekel	k1gInSc1
•	•	k?
Jemenský	jemenský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Jordánský	jordánský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Katarský	katarský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Kuvajtský	kuvajtský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Libanonská	libanonský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Ománský	ománský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Saúdský	saúdský	k2eAgInSc1d1
rijál	rijál	k1gInSc1
•	•	k?
Dirham	dirham	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
emirátů	emirát	k1gInPc2
•	•	k?
Syrská	syrský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Jižní	jižní	k2eAgFnSc2d1
</s>
<s>
Afghánský	afghánský	k2eAgInSc1d1
afghání	afghání	k1gNnSc6
•	•	k?
Bangladéšská	bangladéšský	k2eAgFnSc1d1
taka	taka	k1gFnSc1
•	•	k?
Bhútánský	bhútánský	k2eAgInSc1d1
ngultrum	ngultrum	k1gNnSc1
•	•	k?
Indická	indický	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Maledivská	maledivský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Nepálská	nepálský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Pákistánská	pákistánský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Šrílanská	Šrílanský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
</s>
<s>
Čínský	čínský	k2eAgInSc1d1
jüan	jüan	k1gInSc1
•	•	k?
Hongkongský	hongkongský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Japonský	japonský	k2eAgMnSc1d1
jen	jen	k9
•	•	k?
Jihokorejský	jihokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Macajská	macajský	k2eAgFnSc1d1
pataca	pataca	k1gFnSc1
•	•	k?
Mongolský	mongolský	k2eAgInSc1d1
tugrik	tugrik	k1gInSc1
•	•	k?
Tchajwanský	tchajwanský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1
</s>
<s>
Brunejský	brunejský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Filipínské	filipínský	k2eAgNnSc4d1
peso	peso	k1gNnSc4
•	•	k?
Indonéská	indonéský	k2eAgFnSc1d1
rupie	rupie	k1gFnSc1
•	•	k?
Kambodžský	kambodžský	k2eAgInSc1d1
riel	riel	k1gInSc1
•	•	k?
Laoský	laoský	k2eAgInSc1d1
kip	kip	k?
•	•	k?
Malajsijský	malajsijský	k2eAgInSc1d1
ringgit	ringgit	k1gInSc1
•	•	k?
Myanmarský	Myanmarský	k2eAgInSc1d1
kyat	kyat	k1gInSc1
•	•	k?
Severokorejský	severokorejský	k2eAgInSc1d1
won	won	k1gInSc1
•	•	k?
Singapurský	singapurský	k2eAgInSc1d1
dolar	dolar	k1gInSc1
•	•	k?
Thajský	thajský	k2eAgInSc1d1
baht	baht	k1gInSc1
•	•	k?
Vietnamský	vietnamský	k2eAgInSc1d1
dong	dong	k1gInSc1
•	•	k?
Východotimorské	Východotimorský	k2eAgFnPc4d1
centavové	centavový	k2eAgFnPc4d1
mince	mince	k1gFnPc4
</s>
<s>
Měny	měna	k1gFnPc1
Evropy	Evropa	k1gFnSc2
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
</s>
<s>
Bulharský	bulharský	k2eAgInSc1d1
lev	lev	k1gInSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Dánská	dánský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Chorvatská	chorvatský	k2eAgFnSc1d1
kuna	kuna	k1gFnSc1
•	•	k?
Euro	euro	k1gNnSc1
•	•	k?
Maďarský	maďarský	k2eAgInSc1d1
forint	forint	k1gInSc1
•	•	k?
Polský	polský	k2eAgInSc1d1
zlotý	zlotý	k1gInSc1
•	•	k?
Rumunský	rumunský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Švédská	švédský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
Východní	východní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Běloruský	běloruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Gruzínský	gruzínský	k2eAgInSc1d1
lari	lari	k1gInSc1
•	•	k?
Moldavský	moldavský	k2eAgInSc1d1
lei	lei	k1gInSc1
•	•	k?
Ruský	ruský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
•	•	k?
Ukrajinská	ukrajinský	k2eAgFnSc1d1
hřivna	hřivna	k1gFnSc1
•	•	k?
(	(	kIx(
<g/>
Podněsterský	podněsterský	k2eAgInSc1d1
rubl	rubl	k1gInSc1
<g/>
)	)	kIx)
Jižní	jižní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Albánský	albánský	k2eAgInSc1d1
lek	lek	k1gInSc1
•	•	k?
Bosenskohercegovská	bosenskohercegovský	k2eAgFnSc1d1
marka	marka	k1gFnSc1
•	•	k?
Makedonský	makedonský	k2eAgInSc1d1
denár	denár	k1gInSc1
•	•	k?
Srbský	srbský	k2eAgInSc1d1
dinár	dinár	k1gInSc1
•	•	k?
Turecká	turecký	k2eAgFnSc1d1
lira	lira	k1gFnSc1
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
šterlinků	šterlink	k1gInPc2
•	•	k?
Faerská	Faerský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Gibraltarská	gibraltarský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Guernseyská	Guernseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Islandská	islandský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Jerseyská	Jerseyský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Manská	manský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
•	•	k?
Norská	norský	k2eAgFnSc1d1
koruna	koruna	k1gFnSc1
•	•	k?
Švýcarský	švýcarský	k2eAgInSc4d1
frank	frank	k1gInSc4
</s>
