<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
:	:	kIx,	:
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
jasně	jasně	k6eAd1	jasně
červené	červený	k2eAgNnSc4d1	červené
peří	peří	k1gNnSc4	peří
po	po	k7c6	po
větší	veliký	k2eAgFnSc6d2	veliký
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
peří	peřit	k5eAaImIp3nP	peřit
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
až	až	k8xS	až
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
