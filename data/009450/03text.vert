<p>
<s>
Tangara	tangara	k1gFnSc1	tangara
šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
(	(	kIx(	(
<g/>
Piranga	Piranga	k1gFnSc1	Piranga
olivacea	olivacea	k1gFnSc1	olivacea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
kardinálovitý	kardinálovitý	k2eAgMnSc1d1	kardinálovitý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Piranga	Piranga	k1gFnSc1	Piranga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
tangara	tangara	k1gFnSc1	tangara
šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
řazena	řadit	k5eAaImNgFnS	řadit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
tangarovitých	tangarovitý	k2eAgFnPc2d1	tangarovitý
(	(	kIx(	(
<g/>
Thraupidae	Thraupidae	k1gFnPc2	Thraupidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
molekulárních	molekulární	k2eAgNnPc2d1	molekulární
dat	datum	k1gNnPc2	datum
ovšem	ovšem	k9	ovšem
plně	plně	k6eAd1	plně
podrporují	podrporovat	k5eAaBmIp3nP	podrporovat
zařazení	zařazení	k1gNnSc4	zařazení
tangary	tangara	k1gFnSc2	tangara
šarlatové	šarlatový	k2eAgFnSc2d1	šarlatová
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
rodu	rod	k1gInSc2	rod
Piranga	Pirang	k1gMnSc2	Pirang
<g/>
)	)	kIx)	)
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
kardinálovitých	kardinálovitý	k2eAgFnPc2d1	kardinálovitý
(	(	kIx(	(
<g/>
Cardinalidae	Cardinalidae	k1gFnPc2	Cardinalidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
východní	východní	k2eAgFnSc3d1	východní
části	část	k1gFnSc3	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přezimovává	přezimovávat	k5eAaImIp3nS	přezimovávat
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
IUCN	IUCN	kA	IUCN
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc4d1	dotčený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tangara	tangara	k1gFnSc1	tangara
šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nejmenší	malý	k2eAgFnSc1d3	nejmenší
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Piranga	Piranga	k1gFnSc1	Piranga
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
23,5	[number]	k4	23,5
až	až	k9	až
38	[number]	k4	38
g	g	kA	g
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
potravě	potrava	k1gFnSc6	potrava
a	a	k8xC	a
jejím	její	k3xOp3gNnSc6	její
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
má	mít	k5eAaImIp3nS	mít
od	od	k7c2	od
16	[number]	k4	16
do	do	k7c2	do
19	[number]	k4	19
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
25	[number]	k4	25
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgInSc1d1	pohlavní
dimorfismus	dimorfismus	k1gInSc1	dimorfismus
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
:	:	kIx,	:
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
jasně	jasně	k6eAd1	jasně
červené	červený	k2eAgNnSc4d1	červené
peří	peří	k1gNnSc4	peří
po	po	k7c6	po
větší	veliký	k2eAgFnSc6d2	veliký
části	část	k1gFnSc6	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
jen	jen	k9	jen
peří	peřit	k5eAaImIp3nP	peřit
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
hnědé	hnědý	k2eAgFnPc1d1	hnědá
až	až	k8xS	až
černé	černý	k2eAgFnPc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
nažloutlé	nažloutlý	k2eAgMnPc4d1	nažloutlý
na	na	k7c6	na
dolních	dolní	k2eAgFnPc6d1	dolní
partiích	partie	k1gFnPc6	partie
a	a	k8xC	a
horní	horní	k2eAgFnPc1d1	horní
partie	partie	k1gFnPc1	partie
mají	mít	k5eAaImIp3nP	mít
olivové	olivový	k2eAgFnPc1d1	olivová
až	až	k6eAd1	až
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědat	k5eAaImIp3nS	hnědat
až	až	k6eAd1	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
se	se	k3xPyFc4	se
ale	ale	k9	ale
samci	samec	k1gMnPc1	samec
velmi	velmi	k6eAd1	velmi
podobají	podobat	k5eAaImIp3nP	podobat
samičkám	samička	k1gFnPc3	samička
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
peří	peřit	k5eAaImIp3nP	peřit
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
těla	tělo	k1gNnSc2	tělo
totiž	totiž	k9	totiž
zesvětlá	zesvětlat	k5eAaPmIp3nS	zesvětlat
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
a	a	k8xC	a
ocase	ocas	k1gInSc6	ocas
ztmavne	ztmavnout	k5eAaPmIp3nS	ztmavnout
<g/>
.	.	kIx.	.
</s>
<s>
Ptáčata	ptáče	k1gNnPc1	ptáče
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
podobají	podobat	k5eAaImIp3nP	podobat
samicím	samice	k1gFnPc3	samice
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc2	jejich
pohlaví	pohlaví	k1gNnSc2	pohlaví
lze	lze	k6eAd1	lze
rozpoznat	rozpoznat	k5eAaPmF	rozpoznat
až	až	k9	až
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
přepeřování	přepeřování	k1gNnSc6	přepeřování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poněkud	poněkud	k6eAd1	poněkud
matoucí	matoucí	k2eAgMnSc1d1	matoucí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaPmF	zdát
latinský	latinský	k2eAgInSc4d1	latinský
název	název	k1gInSc4	název
olivacea	olivace	k1gInSc2	olivace
čili	čili	k8xC	čili
olivový	olivový	k2eAgInSc4d1	olivový
<g/>
/	/	kIx~	/
<g/>
á.	á.	k?	á.
Tato	tento	k3xDgFnSc1	tento
chyba	chyba	k6eAd1	chyba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
popsání	popsání	k1gNnSc6	popsání
tangary	tangara	k1gFnSc2	tangara
šarlatové	šarlatový	k2eAgFnSc2d1	šarlatová
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
popisována	popisován	k2eAgFnSc1d1	popisována
mladá	mladý	k2eAgFnSc1d1	mladá
samice	samice	k1gFnSc1	samice
<g/>
:	:	kIx,	:
ty	ten	k3xDgInPc1	ten
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zeleně	zeleň	k1gFnPc1	zeleň
olivové	olivový	k2eAgFnPc1d1	olivová
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
světlejší	světlý	k2eAgInSc1d2	světlejší
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
chyba	chyba	k1gFnSc1	chyba
později	pozdě	k6eAd2	pozdě
zaznamenána	zaznamenán	k2eAgFnSc1d1	zaznamenána
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
již	již	k6eAd1	již
zažitý	zažitý	k2eAgInSc1d1	zažitý
název	název	k1gInSc1	název
neupravoval	upravovat	k5eNaImAgInS	upravovat
a	a	k8xC	a
proto	proto	k8xC	proto
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
nezměněný	změněný	k2eNgInSc1d1	nezměněný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Tangara	tangara	k1gFnSc1	tangara
šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
zralými	zralý	k2eAgInPc7d1	zralý
plody	plod	k1gInPc7	plod
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
i	i	k9	i
citrusy	citrus	k1gInPc4	citrus
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
exotickým	exotický	k2eAgNnSc7d1	exotické
ovocem	ovoce	k1gNnSc7	ovoce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
najde	najít	k5eAaPmIp3nS	najít
u	u	k7c2	u
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
si	se	k3xPyFc3	se
ale	ale	k9	ale
vystačí	vystačit	k5eAaBmIp3nS	vystačit
s	s	k7c7	s
ostružinami	ostružina	k1gFnPc7	ostružina
<g/>
,	,	kIx,	,
malinami	malina	k1gFnPc7	malina
<g/>
,	,	kIx,	,
borůvkami	borůvka	k1gFnPc7	borůvka
nebo	nebo	k8xC	nebo
brusinkami	brusinka	k1gFnPc7	brusinka
<g/>
.	.	kIx.	.
</s>
<s>
Nevynechají	vynechat	k5eNaPmIp3nP	vynechat
ani	ani	k8xC	ani
příležitost	příležitost	k1gFnSc4	příležitost
za	za	k7c2	za
letu	let	k1gInSc2	let
chytit	chytit	k5eAaPmF	chytit
malý	malý	k2eAgInSc4d1	malý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
,	,	kIx,	,
specializují	specializovat	k5eAaBmIp3nP	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
mravence	mravenec	k1gMnPc4	mravenec
<g/>
,	,	kIx,	,
sršně	sršeň	k1gMnPc4	sršeň
<g/>
,	,	kIx,	,
vosy	vosa	k1gFnPc4	vosa
<g/>
,	,	kIx,	,
motýly	motýl	k1gMnPc4	motýl
nebo	nebo	k8xC	nebo
cikády	cikáda	k1gFnPc4	cikáda
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
ptákům	pták	k1gMnPc3	pták
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
rozlehlé	rozlehlý	k2eAgInPc4d1	rozlehlý
listnaté	listnatý	k2eAgInPc4d1	listnatý
lesy	les	k1gInPc4	les
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dubové	dubový	k2eAgFnSc2d1	dubová
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
minimálně	minimálně	k6eAd1	minimálně
10	[number]	k4	10
hektarů	hektar	k1gInPc2	hektar
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
tangary	tangara	k1gFnPc1	tangara
našly	najít	k5eAaPmAgFnP	najít
dostatek	dostatek	k1gInSc4	dostatek
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
je	on	k3xPp3gNnSc4	on
ale	ale	k8xC	ale
i	i	k8xC	i
v	v	k7c6	v
parcích	park	k1gInPc6	park
nebo	nebo	k8xC	nebo
na	na	k7c6	na
předměstích	předměstí	k1gNnPc6	předměstí
<g/>
.	.	kIx.	.
</s>
<s>
Přezimovávají	Přezimovávat	k5eAaPmIp3nP	Přezimovávat
v	v	k7c6	v
horských	horský	k2eAgInPc6d1	horský
lesích	les	k1gInPc6	les
v	v	k7c6	v
podhůří	podhůří	k1gNnSc6	podhůří
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
migrace	migrace	k1gFnSc1	migrace
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
a	a	k8xC	a
kolem	kolem	k7c2	kolem
říjnu	říjen	k1gInSc6	říjen
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
tangary	tangara	k1gFnSc2	tangara
stráví	strávit	k5eAaPmIp3nS	strávit
asi	asi	k9	asi
jen	jen	k9	jen
pět	pět	k4xCc4	pět
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
minimálně	minimálně	k6eAd1	minimálně
další	další	k2eAgInSc4d1	další
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Extrémně	extrémně	k6eAd1	extrémně
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
hnízdění	hnízdění	k1gNnSc2	hnízdění
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
května	květen	k1gInSc2	květen
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
samci	samec	k1gMnPc1	samec
jsou	být	k5eAaImIp3nP	být
již	již	k9	již
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
předem	předem	k6eAd1	předem
na	na	k7c6	na
zimovišti	zimoviště	k1gNnSc6	zimoviště
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
zpozdí	zpozdit	k5eAaPmIp3nS	zpozdit
i	i	k9	i
o	o	k7c4	o
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
námluvách	námluva	k1gFnPc6	námluva
pár	pár	k4xCyI	pár
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
postaví	postavit	k5eAaPmIp3nS	postavit
hnízdo	hnízdo	k1gNnSc1	hnízdo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zabere	zabrat	k5eAaPmIp3nS	zabrat
méně	málo	k6eAd2	málo
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Snůška	snůška	k1gFnSc1	snůška
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
samice	samice	k1gFnSc1	samice
snese	snést	k5eAaPmIp3nS	snést
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
,	,	kIx,	,
čítá	čítat	k5eAaImIp3nS	čítat
obvykle	obvykle	k6eAd1	obvykle
čtyři	čtyři	k4xCgNnPc1	čtyři
vejce	vejce	k1gNnPc1	vejce
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
až	až	k9	až
šest	šest	k4xCc1	šest
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechna	všechen	k3xTgFnSc1	všechen
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
oplodněná	oplodněný	k2eAgFnSc1d1	oplodněná
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
výjimečným	výjimečný	k2eAgInSc7d1	výjimečný
případem	případ	k1gInSc7	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
pěti	pět	k4xCc2	pět
vajec	vejce	k1gNnPc2	vejce
je	být	k5eAaImIp3nS	být
oplodněné	oplodněný	k2eAgNnSc1d1	oplodněné
pouze	pouze	k6eAd1	pouze
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vejce	vejce	k1gNnPc1	vejce
mají	mít	k5eAaImIp3nP	mít
světle	světle	k6eAd1	světle
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
s	s	k7c7	s
nazelenalým	nazelenalý	k2eAgInSc7d1	nazelenalý
odstínem	odstín	k1gInSc7	odstín
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
inkubaci	inkubace	k1gFnSc4	inkubace
provádí	provádět	k5eAaImIp3nS	provádět
samice	samice	k1gFnSc1	samice
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hmotnost	hmotnost	k1gFnSc1	hmotnost
právě	právě	k9	právě
vylíhnutého	vylíhnutý	k2eAgNnSc2d1	vylíhnuté
ptáčete	ptáče	k1gNnSc2	ptáče
tangary	tangara	k1gFnSc2	tangara
šarlatové	šarlatový	k2eAgFnSc2d1	šarlatová
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
4	[number]	k4	4
g.	g.	k?	g.
Za	za	k7c4	za
deset	deset	k4xCc4	deset
dní	den	k1gInPc2	den
pak	pak	k6eAd1	pak
mláďata	mládě	k1gNnPc4	mládě
svoji	svůj	k3xOyFgFnSc4	svůj
váhu	váha	k1gFnSc4	váha
navýší	navýšit	k5eAaPmIp3nS	navýšit
na	na	k7c4	na
20	[number]	k4	20
až	až	k9	až
22	[number]	k4	22
g	g	kA	g
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
70	[number]	k4	70
%	%	kIx~	%
váhy	váha	k1gFnSc2	váha
dospělého	dospělý	k1gMnSc4	dospělý
jedince	jedinec	k1gMnSc4	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Ptáčata	ptáče	k1gNnPc1	ptáče
poprvé	poprvé	k6eAd1	poprvé
opouštějí	opouštět	k5eAaImIp3nP	opouštět
hnízdo	hnízdo	k1gNnSc4	hnízdo
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
9	[number]	k4	9
až	až	k9	až
12	[number]	k4	12
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Létat	létat	k5eAaImF	létat
umí	umět	k5eAaImIp3nS	umět
již	již	k6eAd1	již
za	za	k7c4	za
pár	pár	k4xCyI	pár
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Scarlet	Scarlet	k1gInSc1	Scarlet
Tanager	tanagra	k1gFnPc2	tanagra
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tangara	tangara	k1gFnSc1	tangara
šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
tangara	tangara	k1gFnSc1	tangara
šarlatová	šarlatový	k2eAgFnSc1d1	šarlatová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Piranga	Pirang	k1gMnSc2	Pirang
olivacea	olivaceus	k1gMnSc2	olivaceus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
