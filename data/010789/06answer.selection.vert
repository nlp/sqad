<s>
Zirkon	zirkon	k1gInSc1	zirkon
(	(	kIx(	(
<g/>
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
ZrSiO	ZrSiO	k1gFnSc1	ZrSiO
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
křemičitan	křemičitan	k1gInSc4	křemičitan
zirkoničitý	zirkoničitý	k2eAgInSc4d1	zirkoničitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
