<p>
<s>
Zirkon	zirkon	k1gInSc1	zirkon
(	(	kIx(	(
<g/>
Werner	Werner	k1gMnSc1	Werner
<g/>
,	,	kIx,	,
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
ZrSiO	ZrSiO	k1gFnSc1	ZrSiO
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
křemičitan	křemičitan	k1gInSc4	křemičitan
zirkoničitý	zirkoničitý	k2eAgInSc4d1	zirkoničitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
čtverečný	čtverečný	k2eAgInSc4d1	čtverečný
minerál	minerál	k1gInSc4	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
perského	perský	k2eAgInSc2d1	perský
zargun	zargun	k1gNnSc1	zargun
–	–	k?	–
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
jako	jako	k9	jako
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
odrůd	odrůda	k1gFnPc2	odrůda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
==	==	k?	==
</s>
</p>
<p>
<s>
magmatický	magmatický	k2eAgInSc1d1	magmatický
–	–	k?	–
akcesorický	akcesorický	k2eAgInSc4d1	akcesorický
minerál	minerál	k1gInSc4	minerál
v	v	k7c6	v
kyselých	kyselý	k2eAgFnPc6d1	kyselá
vyvřelých	vyvřelý	k2eAgFnPc6d1	vyvřelá
horninách	hornina	k1gFnPc6	hornina
(	(	kIx(	(
<g/>
granity	granit	k1gInPc1	granit
<g/>
,	,	kIx,	,
diority	diorit	k1gInPc1	diorit
<g/>
,	,	kIx,	,
syenity	syenit	k1gInPc1	syenit
<g/>
,	,	kIx,	,
leukogranity	leukogranit	k1gInPc1	leukogranit
<g/>
,	,	kIx,	,
pegmatity	pegmatit	k1gInPc1	pegmatit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bazických	bazický	k2eAgFnPc6d1	bazická
a	a	k8xC	a
ultrabazických	ultrabazický	k2eAgFnPc6d1	ultrabazický
horninách	hornina	k1gFnPc6	hornina
vzácný	vzácný	k2eAgInSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
krystaly	krystal	k1gInPc1	krystal
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
pegmatitech	pegmatit	k1gInPc6	pegmatit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
metamorfní	metamorfní	k2eAgInSc1d1	metamorfní
–	–	k?	–
běžný	běžný	k2eAgInSc1d1	běžný
akcesorický	akcesorický	k2eAgInSc1d1	akcesorický
minerál	minerál	k1gInSc1	minerál
(	(	kIx(	(
<g/>
fylity	fylit	k1gInPc1	fylit
<g/>
,	,	kIx,	,
svory	svor	k1gInPc1	svor
<g/>
,	,	kIx,	,
ruly	rula	k1gFnPc1	rula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sedimentární	sedimentární	k2eAgInSc1d1	sedimentární
–	–	k?	–
odolný	odolný	k2eAgInSc1d1	odolný
vůči	vůči	k7c3	vůči
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
i	i	k8xC	i
chemickému	chemický	k2eAgNnSc3d1	chemické
zvětrávání	zvětrávání	k1gNnSc3	zvětrávání
<g/>
,	,	kIx,	,
typický	typický	k2eAgInSc1d1	typický
"	"	kIx"	"
<g/>
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
"	"	kIx"	"
minerál	minerál	k1gInSc1	minerál
úlomkovitých	úlomkovitý	k2eAgFnPc2d1	úlomkovitá
(	(	kIx(	(
<g/>
klastických	klastický	k2eAgFnPc2d1	klastická
<g/>
)	)	kIx)	)
usazenin	usazenina	k1gFnPc2	usazenina
pískovců	pískovec	k1gInPc2	pískovec
a	a	k8xC	a
drob	droba	k1gFnPc2	droba
<g/>
.	.	kIx.	.
<g/>
Zirkon	zirkon	k1gInSc1	zirkon
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
minerál	minerál	k1gInSc1	minerál
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
věku	věk	k1gInSc3	věk
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ne	ne	k9	ne
k	k	k7c3	k
lidskému	lidský	k2eAgNnSc3d1	lidské
poznání	poznání	k1gNnSc3	poznání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc1	stáří
vzorku	vzorek	k1gInSc2	vzorek
nalezeného	nalezený	k2eAgInSc2d1	nalezený
v	v	k7c4	v
Jack	Jack	k1gInSc4	Jack
Hills	Hillsa	k1gFnPc2	Hillsa
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
na	na	k7c4	na
4,4	[number]	k4	4,4
mld.	mld.	k?	mld.
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morfologie	morfologie	k1gFnSc2	morfologie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tabulkových	tabulkový	k2eAgInPc2d1	tabulkový
až	až	k8xS	až
prizmatických	prizmatický	k2eAgInPc2d1	prizmatický
krystalů	krystal	k1gInPc2	krystal
<g/>
,	,	kIx,	,
s	s	k7c7	s
čtvercovým	čtvercový	k2eAgInSc7d1	čtvercový
průřezem	průřez	k1gInSc7	průřez
<g/>
,	,	kIx,	,
ukončených	ukončený	k2eAgInPc2d1	ukončený
čtyřbokým	čtyřboký	k2eAgInSc7d1	čtyřboký
jehlanem	jehlan	k1gInSc7	jehlan
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
do	do	k7c2	do
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nepravidelných	pravidelný	k2eNgNnPc2d1	nepravidelné
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
masivní	masivní	k2eAgFnSc1d1	masivní
<g/>
.	.	kIx.	.
</s>
<s>
Dvojčatění	Dvojčatění	k1gNnSc1	Dvojčatění
podle	podle	k7c2	podle
{	{	kIx(	{
<g/>
101	[number]	k4	101
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Metamiktní	Metamiktní	k2eAgInSc1d1	Metamiktní
minerál	minerál	k1gInSc1	minerál
–	–	k?	–
radioaktivní	radioaktivní	k2eAgNnSc1d1	radioaktivní
záření	záření	k1gNnSc1	záření
obsaženého	obsažený	k2eAgNnSc2d1	obsažené
thoria	thorium	k1gNnSc2	thorium
a	a	k8xC	a
uranu	uran	k1gInSc2	uran
časem	časem	k6eAd1	časem
rozruší	rozrušit	k5eAaPmIp3nS	rozrušit
jeho	jeho	k3xOp3gFnSc4	jeho
krystalovou	krystalový	k2eAgFnSc4d1	krystalová
mřížku	mřížka	k1gFnSc4	mřížka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
7,5	[number]	k4	7,5
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
4,6	[number]	k4	4,6
<g/>
–	–	k?	–
<g/>
4,7	[number]	k4	4,7
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
štěpnost	štěpnost	k1gFnSc1	štěpnost
špatná	špatný	k2eAgFnSc1d1	špatná
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
{	{	kIx(	{
<g/>
110	[number]	k4	110
<g/>
}	}	kIx)	}
a	a	k8xC	a
{	{	kIx(	{
<g/>
111	[number]	k4	111
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
lom	lom	k1gInSc1	lom
lasturnatý	lasturnatý	k2eAgInSc1d1	lasturnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
,	,	kIx,	,
hnědá	hnědý	k2eAgFnSc1d1	hnědá
<g/>
,	,	kIx,	,
hnědočervená	hnědočervený	k2eAgFnSc1d1	hnědočervená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
vzácněji	vzácně	k6eAd2	vzácně
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
až	až	k6eAd1	až
diamantový	diamantový	k2eAgInSc1d1	diamantový
<g/>
,	,	kIx,	,
mastný	mastný	k2eAgInSc1d1	mastný
(	(	kIx(	(
<g/>
metamiktní	metamiktní	k2eAgInSc1d1	metamiktní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průhlednost	průhlednost	k1gFnSc4	průhlednost
<g/>
:	:	kIx,	:
průhledný	průhledný	k2eAgInSc1d1	průhledný
až	až	k8xS	až
opakní	opakní	k2eAgInSc1d1	opakní
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
hafnia	hafnium	k1gNnSc2	hafnium
(	(	kIx(	(
<g/>
1	[number]	k4	1
až	až	k9	až
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
prvků	prvek	k1gInPc2	prvek
vzácných	vzácný	k2eAgFnPc2d1	vzácná
zemin	zemina	k1gFnPc2	zemina
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
do	do	k7c2	do
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Zirkon	zirkon	k1gInSc1	zirkon
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
horninách	hornina	k1gFnPc6	hornina
hlavním	hlavní	k2eAgMnSc7d1	hlavní
nositelem	nositel	k1gMnSc7	nositel
přirozené	přirozený	k2eAgFnSc2d1	přirozená
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
Hyacint	hyacint	k1gInSc1	hyacint
-	-	kIx~	-
žlutočervený	žlutočervený	k2eAgInSc1d1	žlutočervený
<g/>
,	,	kIx,	,
červenohnědý	červenohnědý	k2eAgInSc1d1	červenohnědý
</s>
</p>
<p>
<s>
Jargon	Jargon	k1gNnSc1	Jargon
-	-	kIx~	-
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
<g/>
,	,	kIx,	,
bledě	bledě	k6eAd1	bledě
slámově	slámově	k6eAd1	slámově
žlutý	žlutý	k2eAgInSc1d1	žlutý
na	na	k7c4	na
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
</s>
</p>
<p>
<s>
Starlit	Starlit	k1gInSc1	Starlit
-	-	kIx~	-
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Pailinu	Pailin	k1gInSc6	Pailin
v	v	k7c6	v
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
.	.	kIx.	.
</s>
<s>
Žíháním	žíhání	k1gNnSc7	žíhání
lze	lze	k6eAd1	lze
změnit	změnit	k5eAaPmF	změnit
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	časem	k6eAd1	časem
opět	opět	k6eAd1	opět
vybledne	vyblednout	k5eAaPmIp3nS	vyblednout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Polymorfie	polymorfie	k1gFnSc2	polymorfie
==	==	k?	==
</s>
</p>
<p>
<s>
reidit	reidit	k5eAaPmF	reidit
–	–	k?	–
vysokotlaká	vysokotlaký	k2eAgFnSc1d1	vysokotlaká
varianta	varianta	k1gFnSc1	varianta
</s>
</p>
<p>
<s>
==	==	k?	==
Podobné	podobný	k2eAgInPc4d1	podobný
minerály	minerál	k1gInPc4	minerál
==	==	k?	==
</s>
</p>
<p>
<s>
granát	granát	k1gInSc1	granát
<g/>
,	,	kIx,	,
thorit	thorit	k1gInSc1	thorit
<g/>
,	,	kIx,	,
xenotim	xenotim	k1gInSc1	xenotim
</s>
</p>
<p>
<s>
==	==	k?	==
Parageneze	parageneze	k1gFnSc2	parageneze
==	==	k?	==
</s>
</p>
<p>
<s>
amfibol	amfibol	k1gInSc1	amfibol
<g/>
,	,	kIx,	,
biotit	biotit	k1gInSc1	biotit
<g/>
,	,	kIx,	,
granát	granát	k1gInSc1	granát
<g/>
,	,	kIx,	,
křemen	křemen	k1gInSc1	křemen
aj.	aj.	kA	aj.
</s>
</p>
<p>
<s>
==	==	k?	==
Získávání	získávání	k1gNnSc2	získávání
==	==	k?	==
</s>
</p>
<p>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
těžba	těžba	k1gFnSc1	těžba
z	z	k7c2	z
plážových	plážový	k2eAgInPc2d1	plážový
písků	písek	k1gInPc2	písek
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
zirkonia	zirkonium	k1gNnSc2	zirkonium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pěkně	pěkně	k6eAd1	pěkně
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
indexu	index	k1gInSc3	index
lomu	lom	k1gInSc2	lom
světla	světlo	k1gNnSc2	světlo
používají	používat	k5eAaImIp3nP	používat
ve	v	k7c6	v
šperkařství	šperkařství	k1gNnSc6	šperkařství
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
brousí	brousit	k5eAaImIp3nP	brousit
do	do	k7c2	do
oválných	oválný	k2eAgInPc2d1	oválný
nebo	nebo	k8xC	nebo
kulatých	kulatý	k2eAgInPc2d1	kulatý
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
diamantový	diamantový	k2eAgInSc1d1	diamantový
brus	brus	k1gInSc1	brus
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
nalezišť	naleziště	k1gNnPc2	naleziště
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
,	,	kIx,	,
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
a	a	k8xC	a
v	v	k7c6	v
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
průmyslově	průmyslově	k6eAd1	průmyslově
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
žáruvzdorných	žáruvzdorný	k2eAgFnPc2d1	žáruvzdorná
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
keramiky	keramika	k1gFnSc2	keramika
<g/>
,	,	kIx,	,
glazur	glazura	k1gFnPc2	glazura
<g/>
,	,	kIx,	,
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zirkoničitého	zirkoničitý	k2eAgInSc2d1	zirkoničitý
<g/>
.	.	kIx.	.
<g/>
Radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
zirkonu	zirkon	k1gInSc2	zirkon
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
určování	určování	k1gNnSc3	určování
stáří	stáří	k1gNnSc2	stáří
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Naleziště	naleziště	k1gNnSc2	naleziště
==	==	k?	==
</s>
</p>
<p>
<s>
Široce	široko	k6eAd1	široko
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
minerál	minerál	k1gInSc1	minerál
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
krystaly	krystal	k1gInPc1	krystal
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česko	Česko	k1gNnSc1	Česko
–	–	k?	–
např.	např.	kA	např.
Vlastějovice	Vlastějovice	k1gFnSc2	Vlastějovice
u	u	k7c2	u
Zruče	Zruč	k1gInSc2	Zruč
nad	nad	k7c7	nad
Sázavou	Sázava	k1gFnSc7	Sázava
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
pyropy	pyrop	k1gInPc7	pyrop
u	u	k7c2	u
Třebenic	Třebenice	k1gFnPc2	Třebenice
v	v	k7c6	v
Českém	český	k2eAgNnSc6d1	české
středohoří	středohoří	k1gNnSc6	středohoří
<g/>
,	,	kIx,	,
Krušné	krušný	k2eAgFnPc1d1	krušná
hory	hora	k1gFnPc1	hora
–	–	k?	–
Jáchymov	Jáchymov	k1gInSc1	Jáchymov
<g/>
,	,	kIx,	,
Podsedice	Podsedice	k1gFnSc1	Podsedice
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
např.	např.	kA	např.
Maršíkov	Maršíkov	k1gInSc1	Maršíkov
</s>
</p>
<p>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
–	–	k?	–
Banská	banský	k2eAgFnSc1d1	Banská
Štiavnica	Štiavnica	k1gFnSc1	Štiavnica
<g/>
,	,	kIx,	,
Brezno	Brezna	k1gFnSc5	Brezna
<g/>
,	,	kIx,	,
Gelnica	Gelnica	k1gMnSc1	Gelnica
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
Siebengebirge	Siebengebirg	k1gInSc2	Siebengebirg
<g/>
,	,	kIx,	,
Niedermendig	Niedermendig	k1gMnSc1	Niedermendig
<g/>
,	,	kIx,	,
Eifel	Eifel	k1gMnSc1	Eifel
<g/>
,	,	kIx,	,
Pfitsch	Pfitsch	k1gMnSc1	Pfitsch
</s>
</p>
<p>
<s>
Srí	Srí	k?	Srí
Lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
Barma	Barma	k1gFnSc1	Barma
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
–	–	k?	–
zirkon	zirkon	k1gInSc4	zirkon
drahokamové	drahokamový	k2eAgFnSc2d1	drahokamová
kvality	kvalita	k1gFnSc2	kvalita
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Zirkón	Zirkón	k1gInSc1	Zirkón
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dana	Dana	k1gFnSc1	Dana
<g/>
,	,	kIx,	,
E.	E.	kA	E.
S.	S.	kA	S.
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Dana	Dana	k1gFnSc1	Dana
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
system	syst	k1gInSc7	syst
of	of	k?	of
mineralogy	mineralog	k1gMnPc7	mineralog
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
th	th	k?	th
edition	edition	k1gInSc1	edition
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
482	[number]	k4	482
<g/>
–	–	k?	–
<g/>
488	[number]	k4	488
</s>
</p>
<p>
<s>
DUĎA	DUĎA	kA	DUĎA
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
;	;	kIx,	;
REJL	REJL	kA	REJL
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Minerály	minerál	k1gInPc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
Dušan	Dušan	k1gMnSc1	Dušan
Slivka	slivka	k1gFnSc1	slivka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
české	český	k2eAgFnSc2d1	Česká
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
AVENTINUM	AVENTINUM	kA	AVENTINUM
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
520	[number]	k4	520
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7151	[number]	k4	7151
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
30	[number]	k4	30
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Minerály	minerál	k1gInPc1	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
IKAR	IKAR	kA	IKAR
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7176-207-5	[number]	k4	80-7176-207-5
a	a	k8xC	a
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85830	[number]	k4	85830
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Křemičitany	křemičitan	k1gInPc1	křemičitan
<g/>
,	,	kIx,	,
s.	s.	k?	s.
202	[number]	k4	202
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zirkon	zirkon	k1gInSc1	zirkon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zirkon	zirkon	k1gInSc1	zirkon
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zirkon	zirkon	k1gInSc1	zirkon
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Zirkon	zirkon	k1gInSc1	zirkon
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Mineral	Mineral	k1gMnSc1	Mineral
data	datum	k1gNnSc2	datum
publishing	publishing	k1gInSc1	publishing
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Multimediální	multimediální	k2eAgInPc1d1	multimediální
studijní	studijní	k2eAgInPc1d1	studijní
texty	text	k1gInPc1	text
z	z	k7c2	z
mineralogie	mineralogie	k1gFnSc2	mineralogie
pro	pro	k7c4	pro
bakalářské	bakalářský	k2eAgNnSc4d1	bakalářské
studium	studium	k1gNnSc4	studium
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Zirkon	zirkon	k1gInSc1	zirkon
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
geologie	geologie	k1gFnSc2	geologie
<g/>
.	.	kIx.	.
<g/>
vsb	vsb	k?	vsb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
