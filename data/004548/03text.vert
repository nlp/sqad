<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
neboli	neboli	k8xC	neboli
Praha	Praha	k1gFnSc1	Praha
<g/>
/	/	kIx~	/
<g/>
Ruzyně	Ruzyně	k1gFnSc1	Ruzyně
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
PRG	PRG	kA	PRG
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
LKPR	LKPR	kA	LKPR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
umístěné	umístěný	k2eAgNnSc1d1	umístěné
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
,	,	kIx,	,
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
<g/>
,	,	kIx,	,
u	u	k7c2	u
Kněževsi	Kněževes	k1gFnSc2	Kněževes
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
i	i	k8xC	i
vnitrostátní	vnitrostátní	k2eAgInSc4d1	vnitrostátní
<g/>
,	,	kIx,	,
pravidelný	pravidelný	k2eAgInSc4d1	pravidelný
i	i	k8xC	i
nepravidelný	pravidelný	k2eNgInSc4d1	nepravidelný
letecký	letecký	k2eAgInSc4d1	letecký
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
České	český	k2eAgFnPc1d1	Česká
aerolinie	aerolinie	k1gFnPc1	aerolinie
<g/>
,	,	kIx,	,
Travel	Travel	k1gInSc1	Travel
Service	Service	k1gFnSc1	Service
<g/>
,	,	kIx,	,
SmartWings	SmartWings	k1gInSc1	SmartWings
<g/>
,	,	kIx,	,
Ryanair	Ryanair	k1gMnSc1	Ryanair
<g/>
,	,	kIx,	,
Wizz	Wizz	k1gMnSc1	Wizz
Air	Air	k1gMnSc1	Air
a	a	k8xC	a
slovenská	slovenský	k2eAgFnSc1d1	slovenská
nákladní	nákladní	k2eAgFnSc1d1	nákladní
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gMnSc1	Air
Cargo	Cargo	k6eAd1	Cargo
Global	globat	k5eAaImAgMnS	globat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
letní	letní	k2eAgFnSc4d1	letní
sezónně	sezónně	k6eAd1	sezónně
2017	[number]	k4	2017
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
létá	létat	k5eAaImIp3nS	létat
přes	přes	k7c4	přes
66	[number]	k4	66
dopravců	dopravce	k1gMnPc2	dopravce
do	do	k7c2	do
154	[number]	k4	154
destinací	destinace	k1gFnPc2	destinace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
a	a	k8xC	a
nejrušnější	rušný	k2eAgNnSc4d3	nejrušnější
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
řadilo	řadit	k5eAaImAgNnS	řadit
mezi	mezi	k7c4	mezi
40	[number]	k4	40
nejvytíženějších	vytížený	k2eAgNnPc2d3	nejvytíženější
letišť	letiště	k1gNnPc2	letiště
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
odbavilo	odbavit	k5eAaPmAgNnS	odbavit
rekordních	rekordní	k2eAgInPc2d1	rekordní
13	[number]	k4	13
milionů	milion	k4xCgInPc2	milion
cestujících	cestující	k1gMnPc2	cestující
při	při	k7c6	při
136	[number]	k4	136
tisících	tisíc	k4xCgInPc6	tisíc
vzletech	vzlet	k1gInPc6	vzlet
<g/>
/	/	kIx~	/
<g/>
přistání	přistání	k1gNnSc4	přistání
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
71	[number]	k4	71
tisících	tisící	k4xOgInPc2	tisící
tun	tuna	k1gFnPc2	tuna
přepraveného	přepravený	k2eAgInSc2d1	přepravený
nákladu	náklad	k1gInSc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
letiště	letiště	k1gNnSc2	letiště
je	být	k5eAaImIp3nS	být
akciová	akciový	k2eAgFnSc1d1	akciová
společnost	společnost	k1gFnSc1	společnost
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
100	[number]	k4	100
<g/>
%	%	kIx~	%
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
akciovou	akciový	k2eAgFnSc7d1	akciová
společností	společnost	k1gFnSc7	společnost
Český	český	k2eAgInSc1d1	český
Aeroholding	Aeroholding	k1gInSc1	Aeroholding
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
čtyřčlenné	čtyřčlenný	k2eAgNnSc1d1	čtyřčlenné
představenstvo	představenstvo	k1gNnSc1	představenstvo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
předsedou	předseda	k1gMnSc7	předseda
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Řehoř	Řehoř	k1gMnSc1	Řehoř
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Ruzyni	Ruzyně	k1gFnSc6	Ruzyně
bylo	být	k5eAaImAgNnS	být
nově	nově	k6eAd1	nově
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
původní	původní	k2eAgNnSc4d1	původní
hlavní	hlavní	k2eAgNnSc4d1	hlavní
pražské	pražský	k2eAgNnSc4d1	Pražské
letiště	letiště	k1gNnSc4	letiště
Kbely	Kbely	k1gInPc1	Kbely
založené	založený	k2eAgInPc1d1	založený
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
letiště	letiště	k1gNnSc1	letiště
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
letectví	letectví	k1gNnSc2	letectví
a	a	k8xC	a
proudových	proudový	k2eAgNnPc2d1	proudové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
podstatná	podstatný	k2eAgNnPc1d1	podstatné
rozšíření	rozšíření	k1gNnPc1	rozšíření
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
otevření	otevření	k1gNnSc1	otevření
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
výrazné	výrazný	k2eAgNnSc4d1	výrazné
navýšení	navýšení	k1gNnSc4	navýšení
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
na	na	k7c6	na
pláni	pláň	k1gFnSc6	pláň
zvané	zvaný	k2eAgFnPc1d1	zvaná
Dlouhá	Dlouhá	k1gFnSc1	Dlouhá
míle	míle	k1gFnSc1	míle
v	v	k7c6	v
letech	let	k1gInPc6	let
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1937	[number]	k4	1937
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
letiště	letiště	k1gNnSc2	letiště
vyprojektovaná	vyprojektovaný	k2eAgFnSc1d1	vyprojektovaná
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
Adolfem	Adolf	k1gMnSc7	Adolf
Benšem	Benš	k1gMnSc7	Benš
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
výstavě	výstava	k1gFnSc6	výstava
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
oceněna	ocenit	k5eAaPmNgFnS	ocenit
zlatou	zlatý	k2eAgFnSc7d1	zlatá
medailí	medaile	k1gFnSc7	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
budovy	budova	k1gFnPc4	budova
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Kamil	Kamil	k1gMnSc1	Kamil
Roškot	Roškot	k1gInSc4	Roškot
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
letiště	letiště	k1gNnSc2	letiště
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
v	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
přistál	přistát	k5eAaImAgInS	přistát
letoun	letoun	k1gInSc1	letoun
Douglas	Douglas	k1gInSc4	Douglas
DC-2	DC-2	k1gFnSc2	DC-2
Československé	československý	k2eAgFnSc2d1	Československá
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
letu	let	k1gInSc6	let
z	z	k7c2	z
Piešťan	Piešťany	k1gInPc2	Piešťany
přes	přes	k7c4	přes
Zlín	Zlín	k1gInSc4	Zlín
a	a	k8xC	a
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
letoun	letoun	k1gInSc1	letoun
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
se	se	k3xPyFc4	se
přemístila	přemístit	k5eAaPmAgFnS	přemístit
veškerá	veškerý	k3xTgFnSc1	veškerý
civilní	civilní	k2eAgFnSc1d1	civilní
letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
Praha-Kbely	Praha-Kbela	k1gFnSc2	Praha-Kbela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
vojenský	vojenský	k2eAgInSc1d1	vojenský
letecký	letecký	k2eAgInSc1d1	letecký
provoz	provoz	k1gInSc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Prudký	prudký	k2eAgInSc1d1	prudký
rozvoj	rozvoj	k1gInSc1	rozvoj
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
si	se	k3xPyFc3	se
vynutil	vynutit	k5eAaPmAgInS	vynutit
takřka	takřka	k6eAd1	takřka
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
rozšíření	rozšíření	k1gNnSc4	rozšíření
letiště	letiště	k1gNnSc2	letiště
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
80	[number]	k4	80
ha	ha	kA	ha
na	na	k7c4	na
čtyřnásobek	čtyřnásobek	k1gInSc4	čtyřnásobek
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1937	[number]	k4	1937
byly	být	k5eAaImAgFnP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
zpevněné	zpevněný	k2eAgInPc1d1	zpevněný
vzletové	vzletový	k2eAgInPc1d1	vzletový
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc2d1	přistávací
dráhy	dráha	k1gFnSc2	dráha
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
26	[number]	k4	26
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
500	[number]	k4	500
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
280	[number]	k4	280
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
pojezdová	pojezdový	k2eAgFnSc1d1	pojezdová
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
P	P	kA	P
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
VPD	VPD	kA	VPD
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
1000	[number]	k4	1000
m	m	kA	m
a	a	k8xC	a
17	[number]	k4	17
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
délky	délka	k1gFnSc2	délka
950	[number]	k4	950
m	m	kA	m
<g/>
,	,	kIx,	,
VPD	VPD	kA	VPD
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
1080	[number]	k4	1080
m	m	kA	m
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
VPD	VPD	kA	VPD
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
26	[number]	k4	26
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
na	na	k7c4	na
1300	[number]	k4	1300
m	m	kA	m
a	a	k8xC	a
VPD	VPD	kA	VPD
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
na	na	k7c4	na
1800	[number]	k4	1800
m.	m.	k?	m.
Křižovatka	křižovatka	k1gFnSc1	křižovatka
drah	draha	k1gNnPc2	draha
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
a	a	k8xC	a
17	[number]	k4	17
<g/>
/	/	kIx~	/
<g/>
35	[number]	k4	35
bývala	bývat	k5eAaImAgFnS	bývat
také	také	k9	také
nazývána	nazýván	k2eAgFnSc1d1	nazývána
"	"	kIx"	"
<g/>
Velké	velký	k2eAgNnSc1d1	velké
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
"	"	kIx"	"
–	–	k?	–
"	"	kIx"	"
<g/>
Big	Big	k1gFnSc1	Big
square	square	k1gInSc1	square
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
úprav	úprava	k1gFnPc2	úprava
se	se	k3xPyFc4	se
letiště	letiště	k1gNnSc1	letiště
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1947	[number]	k4	1947
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
doznalo	doznat	k5eAaPmAgNnS	doznat
rozšíření	rozšíření	k1gNnSc1	rozšíření
a	a	k8xC	a
vylepšení	vylepšení	k1gNnSc1	vylepšení
jeho	on	k3xPp3gInSc2	on
technické	technický	k2eAgNnSc1d1	technické
zázemí	zázemí	k1gNnSc1	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
pak	pak	k6eAd1	pak
přinesl	přinést	k5eAaPmAgInS	přinést
další	další	k2eAgFnPc4d1	další
úpravy	úprava	k1gFnPc4	úprava
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
proudových	proudový	k2eAgNnPc2d1	proudové
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
stavba	stavba	k1gFnSc1	stavba
paralelní	paralelní	k2eAgFnSc2d1	paralelní
dráhy	dráha	k1gFnSc2	dráha
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
záhy	záhy	k6eAd1	záhy
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1968	[number]	k4	1968
bylo	být	k5eAaImAgNnS	být
letiště	letiště	k1gNnSc1	letiště
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
o	o	k7c4	o
oblast	oblast	k1gFnSc4	oblast
označovanou	označovaný	k2eAgFnSc4d1	označovaná
dnes	dnes	k6eAd1	dnes
jako	jako	k8xS	jako
Sever	sever	k1gInSc1	sever
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
Nové	Nové	k2eAgNnSc1d1	Nové
letiště	letiště	k1gNnSc1	letiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
vypsána	vypsat	k5eAaPmNgFnS	vypsat
další	další	k2eAgFnSc1d1	další
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
a	a	k8xC	a
podle	podle	k7c2	podle
vítězného	vítězný	k2eAgInSc2d1	vítězný
návrhu	návrh	k1gInSc2	návrh
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
nová	nový	k2eAgFnSc1d1	nová
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
budova	budova	k1gFnSc1	budova
architektů	architekt	k1gMnPc2	architekt
Karla	Karel	k1gMnSc2	Karel
Filsaka	Filsak	k1gMnSc2	Filsak
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Bubeníčka	Bubeníček	k1gMnSc2	Bubeníček
<g/>
,	,	kIx,	,
sousedící	sousedící	k2eAgInSc1d1	sousedící
hangár	hangár	k1gInSc1	hangár
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
vzletových	vzletový	k2eAgFnPc2d1	vzletová
a	a	k8xC	a
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgFnPc1d3	nejdelší
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
3200	[number]	k4	3200
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
byla	být	k5eAaImAgFnS	být
plocha	plocha	k1gFnSc1	plocha
letiště	letiště	k1gNnSc2	letiště
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
na	na	k7c6	na
800	[number]	k4	800
ha	ha	kA	ha
(	(	kIx(	(
<g/>
což	což	k3yQnSc4	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
rozšíření	rozšíření	k1gNnSc1	rozšíření
území	území	k1gNnSc2	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
obec	obec	k1gFnSc4	obec
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
o	o	k7c6	o
části	část	k1gFnSc6	část
Kněževsi	Kněževes	k1gFnSc2	Kněževes
<g/>
,	,	kIx,	,
Přední	přední	k2eAgFnSc2d1	přední
Kopaniny	kopanina	k1gFnSc2	kopanina
a	a	k8xC	a
Hostivice	Hostivice	k1gFnSc2	Hostivice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
nového	nový	k2eAgNnSc2d1	nové
letiště	letiště	k1gNnSc2	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
byl	být	k5eAaImAgInS	být
vypracován	vypracovat	k5eAaPmNgInS	vypracovat
v	v	k7c6	v
letištním	letištní	k2eAgInSc6d1	letištní
atelieru	atelier	k1gInSc6	atelier
Vojenském	vojenský	k2eAgInSc6d1	vojenský
projektovém	projektový	k2eAgInSc6d1	projektový
ústavu	ústav	k1gInSc6	ústav
vedeném	vedený	k2eAgInSc6d1	vedený
ing.	ing.	kA	ing.
arch	arch	k1gInSc4	arch
<g/>
.	.	kIx.	.
pplk.	pplk.	kA	pplk.
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Conkem	Conek	k1gMnSc7	Conek
<g/>
,	,	kIx,	,
členem	člen	k1gMnSc7	člen
katedry	katedra	k1gFnSc2	katedra
letištních	letištní	k2eAgFnPc2d1	letištní
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
kandidátem	kandidát	k1gMnSc7	kandidát
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
hlavním	hlavní	k2eAgMnSc7d1	hlavní
inženýrem	inženýr	k1gMnSc7	inženýr
výstavby	výstavba	k1gFnSc2	výstavba
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1989	[number]	k4	1989
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
severní	severní	k2eAgFnSc2d1	severní
odbavovací	odbavovací	k2eAgFnSc2d1	odbavovací
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
byla	být	k5eAaImAgFnS	být
vypsána	vypsán	k2eAgFnSc1d1	vypsána
architektonická	architektonický	k2eAgFnSc1d1	architektonická
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
Petr	Petr	k1gMnSc1	Petr
Franta	Franta	k1gMnSc1	Franta
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Brix	Brix	k1gInSc1	Brix
<g/>
.	.	kIx.	.
</s>
<s>
Souběžně	souběžně	k6eAd1	souběžně
byly	být	k5eAaImAgFnP	být
rekonstruovány	rekonstruován	k2eAgFnPc1d1	rekonstruována
vzletové	vzletový	k2eAgFnPc1d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnPc1d1	přistávací
dráhy	dráha	k1gFnPc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgInS	být
vypracován	vypracován	k2eAgInSc1d1	vypracován
investiční	investiční	k2eAgInSc1d1	investiční
záměr	záměr	k1gInSc1	záměr
rozšíření	rozšíření	k1gNnSc2	rozšíření
odbavovacího	odbavovací	k2eAgInSc2d1	odbavovací
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Sever	sever	k1gInSc1	sever
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
uskutečňování	uskutečňování	k1gNnSc3	uskutečňování
se	se	k3xPyFc4	se
přikročilo	přikročit	k5eAaPmAgNnS	přikročit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
součástí	součást	k1gFnPc2	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
terminál	terminál	k1gInSc1	terminál
2	[number]	k4	2
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc4d1	otevřený
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
tým	tým	k1gInSc4	tým
architektů	architekt	k1gMnPc2	architekt
Nikodem	Nikod	k1gInSc7	Nikod
a	a	k8xC	a
Partner	partner	k1gMnSc1	partner
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
a	a	k8xC	a
Mansfeld	Mansfeld	k1gMnSc1	Mansfeld
IDC	IDC	kA	IDC
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
Z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
staveb	stavba	k1gFnPc2	stavba
budí	budit	k5eAaImIp3nS	budit
pozornost	pozornost	k1gFnSc4	pozornost
z	z	k7c2	z
architektonického	architektonický	k2eAgNnSc2d1	architektonické
hlediska	hledisko	k1gNnSc2	hledisko
rovněž	rovněž	k9	rovněž
budova	budova	k1gFnSc1	budova
energetického	energetický	k2eAgInSc2d1	energetický
dispečinku	dispečink	k1gInSc2	dispečink
od	od	k7c2	od
dvojice	dvojice	k1gFnSc2	dvojice
respektovaných	respektovaný	k2eAgMnPc2d1	respektovaný
architektů	architekt	k1gMnPc2	architekt
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Jirana	Jiran	k1gMnSc2	Jiran
a	a	k8xC	a
M.	M.	kA	M.
Kohouta	Kohout	k1gMnSc2	Kohout
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
federální	federální	k2eAgFnSc1d1	federální
prokuratura	prokuratura	k1gFnSc1	prokuratura
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
požádala	požádat	k5eAaPmAgFnS	požádat
české	český	k2eAgInPc4d1	český
úřady	úřad	k1gInPc4	úřad
o	o	k7c4	o
právní	právní	k2eAgFnSc4d1	právní
pomoc	pomoc	k1gFnSc4	pomoc
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
privatizace	privatizace	k1gFnSc1	privatizace
státem	stát	k1gInSc7	stát
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
společnosti	společnost	k1gFnSc2	společnost
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
letiště	letiště	k1gNnSc2	letiště
v	v	k7c6	v
Ruzyni	Ruzyně	k1gFnSc6	Ruzyně
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
prokuratura	prokuratura	k1gFnSc1	prokuratura
měla	mít	k5eAaImAgFnS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
informace	informace	k1gFnPc4	informace
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
policejních	policejní	k2eAgInPc2d1	policejní
spisů	spis	k1gInPc2	spis
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
letiště	letiště	k1gNnSc2	letiště
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
prosba	prosba	k1gFnSc1	prosba
měla	mít	k5eAaImAgFnS	mít
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
podezřením	podezření	k1gNnSc7	podezření
na	na	k7c4	na
legalizaci	legalizace	k1gFnSc4	legalizace
peněz	peníze	k1gInPc2	peníze
z	z	k7c2	z
nezákonné	zákonný	k2eNgFnSc2d1	nezákonná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
Ruzyňské	ruzyňský	k2eAgNnSc4d1	ruzyňské
letiště	letiště	k1gNnSc4	letiště
denně	denně	k6eAd1	denně
létat	létat	k5eAaImF	létat
největší	veliký	k2eAgInSc4d3	veliký
dopravní	dopravní	k2eAgInSc4d1	dopravní
letoun	letoun	k1gInSc4	letoun
světa	svět	k1gInSc2	svět
Airbus	airbus	k1gInSc4	airbus
A380	A380	k1gMnSc3	A380
letecké	letecký	k2eAgFnSc2d1	letecká
společnosti	společnost	k1gFnSc2	společnost
Emirates	Emiratesa	k1gFnPc2	Emiratesa
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
z	z	k7c2	z
Dubaje	Dubaj	k1gInSc2	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
letiště	letiště	k1gNnSc4	letiště
začaly	začít	k5eAaPmAgFnP	začít
létat	létat	k5eAaImF	létat
také	také	k9	také
nové	nový	k2eAgFnPc1d1	nová
tři	tři	k4xCgFnPc1	tři
čínské	čínský	k2eAgFnPc1d1	čínská
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
China	China	k1gFnSc1	China
Eastern	Eastern	k1gMnSc1	Eastern
(	(	kIx(	(
<g/>
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hainan	Hainan	k1gInSc1	Hainan
(	(	kIx(	(
<g/>
Peking	Peking	k1gInSc1	Peking
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sichuan	Sichuan	k1gMnSc1	Sichuan
Airlines	Airlines	k1gMnSc1	Airlines
(	(	kIx(	(
<g/>
Čcheng-tu	Čcheng	k1gInSc2	Čcheng-t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
linku	linka	k1gFnSc4	linka
Korean	Korean	k1gMnSc1	Korean
Air	Air	k1gMnSc1	Air
ze	z	k7c2	z
Soulu	Soul	k1gInSc2	Soul
nasazován	nasazován	k2eAgMnSc1d1	nasazován
nejdeší	jdešit	k5eNaPmIp3nS	jdešit
letoun	letoun	k1gInSc4	letoun
světa	svět	k1gInSc2	svět
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
nově	nově	k6eAd1	nově
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gFnSc1	Air
Canada	Canada	k1gFnSc1	Canada
Rouge	rouge	k1gFnSc1	rouge
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
nákladní	nákladní	k2eAgFnSc4d1	nákladní
linku	linka	k1gFnSc4	linka
také	také	k9	také
zahájila	zahájit	k5eAaPmAgFnS	zahájit
společnost	společnost	k1gFnSc1	společnost
Qatar	Qatar	k1gMnSc1	Qatar
Airways	Airways	k1gInSc1	Airways
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
anonym	anonym	k1gInSc1	anonym
telefonátem	telefonát	k1gInSc7	telefonát
nahlásil	nahlásit	k5eAaPmAgInS	nahlásit
bombu	bomba	k1gFnSc4	bomba
na	na	k7c6	na
Terminálu	terminál	k1gInSc6	terminál
2	[number]	k4	2
Letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
evakuována	evakuován	k2eAgFnSc1d1	evakuována
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
prohledali	prohledat	k5eAaPmAgMnP	prohledat
pyrotechnici	pyrotechnik	k1gMnPc1	pyrotechnik
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
žádnou	žádný	k3yNgFnSc4	žádný
výbušninu	výbušnina	k1gFnSc4	výbušnina
ani	ani	k8xC	ani
bombu	bomba	k1gFnSc4	bomba
nenašli	najít	k5eNaPmAgMnP	najít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
provoz	provoz	k1gInSc1	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2017	[number]	k4	2017
zde	zde	k6eAd1	zde
začaly	začít	k5eAaPmAgFnP	začít
platit	platit	k5eAaImF	platit
ETD	ETD	kA	ETD
namátkové	namátkový	k2eAgFnPc4d1	namátková
kontroly	kontrola	k1gFnPc4	kontrola
odbavených	odbavený	k2eAgNnPc2d1	odbavené
zavazadel	zavazadlo	k1gNnPc2	zavazadlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
na	na	k7c6	na
letištích	letiště	k1gNnPc6	letiště
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
či	či	k8xC	či
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
vybaveno	vybaven	k2eAgNnSc1d1	vybaveno
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
za	za	k7c2	za
viditelnosti	viditelnost	k1gFnSc2	viditelnost
(	(	kIx(	(
<g/>
VFR	VFR	kA	VFR
<g/>
)	)	kIx)	)
i	i	k9	i
podle	podle	k7c2	podle
přístrojů	přístroj	k1gInPc2	přístroj
(	(	kIx(	(
<g/>
IFR	IFR	kA	IFR
<g/>
)	)	kIx)	)
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nepřetržitý	přetržitý	k2eNgInSc4d1	nepřetržitý
provoz	provoz	k1gInSc4	provoz
ve	v	k7c6	v
dne	den	k1gInSc2	den
i	i	k8xC	i
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
koordinováno	koordinovat	k5eAaBmNgNnS	koordinovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EUROCONTROL	EUROCONTROL	kA	EUROCONTROL
v	v	k7c6	v
Bruselu	Brusel	k1gInSc6	Brusel
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
přidělovaných	přidělovaný	k2eAgInPc2d1	přidělovaný
časů	čas	k1gInPc2	čas
vzletů	vzlet	k1gInPc2	vzlet
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
slotů	slot	k1gInPc2	slot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dráhový	dráhový	k2eAgInSc1d1	dráhový
systém	systém	k1gInSc1	systém
sestává	sestávat	k5eAaImIp3nS	sestávat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
vzletových	vzletový	k2eAgFnPc2d1	vzletová
a	a	k8xC	a
přistávacích	přistávací	k2eAgFnPc2d1	přistávací
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
VPD	VPD	kA	VPD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
délka	délka	k1gFnSc1	délka
2120	[number]	k4	2120
m	m	kA	m
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
trvale	trvale	k6eAd1	trvale
pro	pro	k7c4	pro
vzlety	vzlet	k1gInPc4	vzlet
i	i	k8xC	i
přistání	přistání	k1gNnPc4	přistání
uzavřena	uzavřen	k2eAgNnPc4d1	uzavřeno
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
pojezdová	pojezdový	k2eAgFnSc1d1	pojezdová
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
pro	pro	k7c4	pro
parkování	parkování	k1gNnSc4	parkování
velkých	velký	k2eAgNnPc2d1	velké
letadel	letadlo	k1gNnPc2	letadlo
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
Antonov	Antonov	k1gInSc1	Antonov
An-	An-	k1gFnSc1	An-
<g/>
225	[number]	k4	225
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
VPD	VPD	kA	VPD
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
0	[number]	k4	0
<g/>
62	[number]	k4	62
<g/>
/	/	kIx~	/
<g/>
242	[number]	k4	242
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
severovýchod-jihozápad	severovýchodihozápad	k1gInSc1	severovýchod-jihozápad
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
3715	[number]	k4	3715
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
vybavená	vybavený	k2eAgNnPc4d1	vybavené
systémem	systém	k1gInSc7	systém
ILS	ILS	kA	ILS
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
VPD	VPD	kA	VPD
je	být	k5eAaImIp3nS	být
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
124	[number]	k4	124
<g/>
/	/	kIx~	/
<g/>
304	[number]	k4	304
<g/>
°	°	k?	°
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
severozápad-jihovýchod	severozápadihovýchod	k1gInSc1	severozápad-jihovýchod
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
3250	[number]	k4	3250
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
30	[number]	k4	30
vybavená	vybavený	k2eAgFnSc1d1	vybavená
systémem	systém	k1gInSc7	systém
ILS	ILS	kA	ILS
<g/>
.	.	kIx.	.
</s>
<s>
Dráha	dráha	k1gFnSc1	dráha
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1993	[number]	k4	1993
značena	značen	k2eAgFnSc1d1	značena
jako	jako	k8xS	jako
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
25	[number]	k4	25
a	a	k8xC	a
dráha	dráha	k1gFnSc1	dráha
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
do	do	k7c2	do
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
jako	jako	k9	jako
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změny	změna	k1gFnSc2	změna
magnetické	magnetický	k2eAgFnSc2d1	magnetická
deklinace	deklinace	k1gFnSc2	deklinace
byla	být	k5eAaImAgNnP	být
označení	označení	k1gNnPc1	označení
změněna	změněn	k2eAgNnPc1d1	změněno
<g/>
.	.	kIx.	.
</s>
<s>
Letištěm	letiště	k1gNnSc7	letiště
vedou	vést	k5eAaImIp3nP	vést
betonové	betonový	k2eAgFnPc1d1	betonová
a	a	k8xC	a
asfaltové	asfaltový	k2eAgFnPc1d1	asfaltová
pojezdové	pojezdový	k2eAgFnPc1d1	pojezdová
dráhy	dráha	k1gFnPc1	dráha
označené	označený	k2eAgFnPc1d1	označená
písmeny	písmeno	k1gNnPc7	písmeno
A	a	k9	a
<g/>
–	–	k?	–
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
K	k	k7c3	k
<g/>
–	–	k?	–
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
AA	AA	kA	AA
<g/>
,	,	kIx,	,
FF	ff	kA	ff
a	a	k8xC	a
RR	RR	kA	RR
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnSc1	jejich
šířka	šířka	k1gFnSc1	šířka
je	být	k5eAaImIp3nS	být
22,5	[number]	k4	22,5
m	m	kA	m
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
části	část	k1gFnSc2	část
dráhy	dráha	k1gFnSc2	dráha
<g />
.	.	kIx.	.
</s>
<s>
P	P	kA	P
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
široká	široký	k2eAgFnSc1d1	široká
40	[number]	k4	40
m.	m.	k?	m.
Na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgNnPc4	čtyři
vyhrazená	vyhrazený	k2eAgNnPc4d1	vyhrazené
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
přistání	přistání	k1gNnSc4	přistání
vrtulníků	vrtulník	k1gInPc2	vrtulník
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
heliporty	heliport	k1gInPc4	heliport
<g/>
)	)	kIx)	)
označené	označený	k2eAgInPc1d1	označený
H	H	kA	H
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
H	H	kA	H
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
TWY	TWY	kA	TWY
G	G	kA	G
<g/>
,	,	kIx,	,
P	P	kA	P
<g/>
,	,	kIx,	,
S	s	k7c7	s
a	a	k8xC	a
RR	RR	kA	RR
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
letiště	letiště	k1gNnSc2	letiště
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2	Praha-Ruzyně
je	být	k5eAaImIp3nS	být
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
koncová	koncový	k2eAgFnSc1d1	koncová
řízená	řízený	k2eAgFnSc1d1	řízená
oblast	oblast	k1gFnSc1	oblast
TMA	tma	k1gFnSc1	tma
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
Ruzyní	Ruzyně	k1gFnSc7	Ruzyně
se	se	k3xPyFc4	se
v	v	k7c6	v
nízkém	nízký	k2eAgInSc6d1	nízký
vzdušném	vzdušný	k2eAgInSc6d1	vzdušný
prostoru	prostor	k1gInSc6	prostor
kříží	křížit	k5eAaImIp3nP	křížit
letové	letový	k2eAgFnPc4d1	letová
cesty	cesta	k1gFnPc4	cesta
M	M	kA	M
<g/>
748	[number]	k4	748
<g/>
,	,	kIx,	,
W	W	kA	W
<g/>
32	[number]	k4	32
<g/>
,	,	kIx,	,
L984	L984	k1gFnSc1	L984
a	a	k8xC	a
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
vzdušném	vzdušný	k2eAgInSc6d1	vzdušný
prostoru	prostor	k1gInSc6	prostor
letové	letový	k2eAgFnSc2d1	letová
cesty	cesta	k1gFnSc2	cesta
UN871	UN871	k1gFnSc2	UN871
a	a	k8xC	a
UL	ul	kA	ul
<g/>
984	[number]	k4	984
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
jiných	jiný	k2eAgNnPc2d1	jiné
radionavigačních	radionavigační	k2eAgNnPc2d1	radionavigační
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
radiomaják	radiomaják	k1gInSc4	radiomaják
zařízení	zařízení	k1gNnSc2	zařízení
VOR	vor	k1gInSc1	vor
<g/>
/	/	kIx~	/
<g/>
DME	dmout	k5eAaImIp3nS	dmout
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
OKL	OKL	kA	OKL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vysílá	vysílat	k5eAaImIp3nS	vysílat
na	na	k7c6	na
frekvenci	frekvence	k1gFnSc6	frekvence
112,6	[number]	k4	112,6
MHz	Mhz	kA	Mhz
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
poblíž	poblíž	k7c2	poblíž
středu	střed	k1gInSc2	střed
dráhy	dráha	k1gFnSc2	dráha
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc4	letiště
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgFnPc1	tři
dvojice	dvojice	k1gFnPc1	dvojice
terminálů	terminál	k1gInPc2	terminál
rozdělené	rozdělený	k2eAgInPc1d1	rozdělený
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c4	na
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
(	(	kIx(	(
<g/>
severní	severní	k2eAgMnSc1d1	severní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nákladní	nákladní	k2eAgFnSc4d1	nákladní
dopravu	doprava	k1gFnSc4	doprava
(	(	kIx(	(
<g/>
cargo	cargo	k6eAd1	cargo
<g/>
)	)	kIx)	)
a	a	k8xC	a
individuální	individuální	k2eAgFnSc1d1	individuální
a	a	k8xC	a
speciální	speciální	k2eAgFnSc1d1	speciální
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xS	jako
Terminál	terminál	k1gInSc1	terminál
Sever	sever	k1gInSc1	sever
<g/>
.	.	kIx.	.
</s>
<s>
Terminál	terminál	k1gInSc1	terminál
T1	T1	k1gFnSc2	T1
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Sever	sever	k1gInSc1	sever
1	[number]	k4	1
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
letům	let	k1gInPc3	let
mimo	mimo	k7c4	mimo
Schengenský	schengenský	k2eAgInSc4d1	schengenský
prostor	prostor	k1gInSc4	prostor
Terminál	terminála	k1gFnPc2	terminála
T2	T2	k1gFnPc2	T2
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Sever	sever	k1gInSc1	sever
2	[number]	k4	2
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
lety	let	k1gInPc4	let
v	v	k7c6	v
Schengenském	schengenský	k2eAgInSc6d1	schengenský
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
2005	[number]	k4	2005
byl	být	k5eAaImAgInS	být
oficiálně	oficiálně	k6eAd1	oficiálně
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2005	[number]	k4	2005
částečně	částečně	k6eAd1	částečně
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
zpřístupněn	zpřístupněn	k2eAgMnSc1d1	zpřístupněn
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2006	[number]	k4	2006
plně	plně	k6eAd1	plně
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nástupních	nástupní	k2eAgInPc2d1	nástupní
mostů	most	k1gInPc2	most
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
ze	z	k7c2	z
17	[number]	k4	17
na	na	k7c4	na
27	[number]	k4	27
a	a	k8xC	a
kapacita	kapacita	k1gFnSc1	kapacita
letiště	letiště	k1gNnSc2	letiště
(	(	kIx(	(
<g/>
co	co	k9	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
odbavených	odbavený	k2eAgMnPc2d1	odbavený
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zvětšila	zvětšit	k5eAaPmAgFnS	zvětšit
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Investice	investice	k1gFnPc1	investice
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
terminálu	terminál	k1gInSc2	terminál
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
výše	vysoce	k6eAd2	vysoce
9	[number]	k4	9
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
severního	severní	k2eAgInSc2d1	severní
terminálu	terminál	k1gInSc2	terminál
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
ulice	ulice	k1gFnPc1	ulice
Aviatická	aviatický	k2eAgFnSc1d1	Aviatická
a	a	k8xC	a
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
estakáda	estakáda	k1gFnSc1	estakáda
mezi	mezi	k7c4	mezi
terminály	terminál	k1gInPc4	terminál
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
ulehčuje	ulehčovat	k5eAaImIp3nS	ulehčovat
automobilové	automobilový	k2eAgFnSc3d1	automobilová
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
terminálů	terminál	k1gInPc2	terminál
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
expresní	expresní	k2eAgNnPc1d1	expresní
parkoviště	parkoviště	k1gNnPc1	parkoviště
P1	P1	k1gFnSc2	P1
a	a	k8xC	a
P	P	kA	P
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
zastávky	zastávka	k1gFnPc1	zastávka
MHD	MHD	kA	MHD
a	a	k8xC	a
kyvadlové	kyvadlový	k2eAgFnSc2d1	kyvadlová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
stanoviště	stanoviště	k1gNnSc2	stanoviště
taxislužby	taxislužba	k1gFnSc2	taxislužba
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pak	pak	k6eAd1	pak
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
parkovišť	parkoviště	k1gNnPc2	parkoviště
a	a	k8xC	a
parkovacích	parkovací	k2eAgInPc2d1	parkovací
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
<g/>
,	,	kIx,	,
obchodně-administrativní	obchodnědministrativní	k2eAgFnPc1d1	obchodně-administrativní
budovy	budova	k1gFnPc1	budova
včetně	včetně	k7c2	včetně
správní	správní	k2eAgFnSc2d1	správní
budovy	budova	k1gFnSc2	budova
Českého	český	k2eAgInSc2d1	český
Aeoroholdingu	Aeoroholding	k1gInSc2	Aeoroholding
atd.	atd.	kA	atd.
U	u	k7c2	u
těchto	tento	k3xDgInPc2	tento
terminálů	terminál	k1gInPc2	terminál
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
odbavovací	odbavovací	k2eAgFnSc1d1	odbavovací
plocha	plocha	k1gFnSc1	plocha
(	(	kIx(	(
<g/>
Apron	Apron	k1gMnSc1	Apron
North	North	k1gMnSc1	North
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vybavená	vybavený	k2eAgFnSc1d1	vybavená
také	také	k9	také
17	[number]	k4	17
stáními	stání	k1gNnPc7	stání
s	s	k7c7	s
tunely	tunel	k1gInPc7	tunel
pro	pro	k7c4	pro
přímý	přímý	k2eAgInSc4d1	přímý
nástup	nástup	k1gInSc4	nástup
do	do	k7c2	do
letadla	letadlo	k1gNnSc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sousedství	sousedství	k1gNnSc6	sousedství
je	být	k5eAaImIp3nS	být
nákladní	nákladní	k2eAgInSc4d1	nákladní
terminál	terminál	k1gInSc4	terminál
s	s	k7c7	s
odbavovacími	odbavovací	k2eAgFnPc7d1	odbavovací
plochami	plocha	k1gFnPc7	plocha
Apron	Aprona	k1gFnPc2	Aprona
Cargo	Cargo	k1gNnSc1	Cargo
1	[number]	k4	1
a	a	k8xC	a
Apron	Apron	k1gInSc1	Apron
Cargo	Cargo	k6eAd1	Cargo
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
severního	severní	k2eAgInSc2d1	severní
terminálu	terminál	k1gInSc2	terminál
<g/>
,	,	kIx,	,
u	u	k7c2	u
ulice	ulice	k1gFnSc2	ulice
Laglerové	Laglerová	k1gFnSc2	Laglerová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
cargo	cargo	k6eAd1	cargo
zóna	zóna	k1gFnSc1	zóna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
dva	dva	k4xCgInPc4	dva
nákladní	nákladní	k2eAgInPc4d1	nákladní
terminály	terminál	k1gInPc4	terminál
<g/>
:	:	kIx,	:
Menzies	Menziesa	k1gFnPc2	Menziesa
Aviation	Aviation	k1gInSc1	Aviation
-	-	kIx~	-
Cargo	Cargo	k1gMnSc1	Cargo
2	[number]	k4	2
Skyport	Skyport	k1gInSc1	Skyport
-	-	kIx~	-
Cargo	Cargo	k1gNnSc1	Cargo
1	[number]	k4	1
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
menší	malý	k2eAgFnSc1d2	menší
budova	budova	k1gFnSc1	budova
oddělení	oddělení	k1gNnSc2	oddělení
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
veterinární	veterinární	k2eAgFnSc2d1	veterinární
kontroly	kontrola	k1gFnSc2	kontrola
Městské	městský	k2eAgFnSc2d1	městská
veterinární	veterinární	k2eAgFnSc2d1	veterinární
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
cargo	cargo	k1gMnSc1	cargo
zóny	zóna	k1gFnSc2	zóna
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
gastrocentrum	gastrocentrum	k1gNnSc1	gastrocentrum
GASTRO-HROCH	GASTRO-HROCH	k1gFnPc2	GASTRO-HROCH
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
letecký	letecký	k2eAgInSc4d1	letecký
i	i	k8xC	i
pozemní	pozemní	k2eAgInSc4d1	pozemní
catering	catering	k1gInSc4	catering
řadě	řada	k1gFnSc3	řada
významných	významný	k2eAgFnPc2d1	významná
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
starého	starý	k2eAgNnSc2d1	staré
letiště	letiště	k1gNnSc2	letiště
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
jako	jako	k9	jako
Terminál	terminál	k1gInSc1	terminál
Jih	jih	k1gInSc1	jih
Terminál	terminál	k1gInSc1	terminál
T3	T3	k1gFnSc1	T3
General	General	k1gMnSc1	General
Aviation	Aviation	k1gInSc1	Aviation
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Jih	jih	k1gInSc1	jih
2	[number]	k4	2
<g/>
)	)	kIx)	)
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
aerotaxi	aerotaxi	k1gNnSc4	aerotaxi
a	a	k8xC	a
charterové	charterový	k2eAgInPc4d1	charterový
lety	let	k1gInPc4	let
Terminál	terminála	k1gFnPc2	terminála
T4	T4	k1gFnPc2	T4
Military	Militara	k1gFnSc2	Militara
(	(	kIx(	(
<g/>
staré	starý	k2eAgNnSc1d1	staré
letiště	letiště	k1gNnSc1	letiště
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Jih	jih	k1gInSc1	jih
1	[number]	k4	1
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
VIP	VIP	kA	VIP
lety	let	k1gInPc4	let
(	(	kIx(	(
<g/>
Apron	Apron	k1gMnSc1	Apron
South	South	k1gMnSc1	South
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
leteckou	letecký	k2eAgFnSc4d1	letecká
taxislužbu	taxislužba	k1gFnSc4	taxislužba
(	(	kIx(	(
<g/>
Apron	Apron	k1gInSc1	Apron
Aviation	Aviation	k1gInSc1	Aviation
Service	Service	k1gFnSc1	Service
<g/>
)	)	kIx)	)
a	a	k8xC	a
lety	léto	k1gNnPc7	léto
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
letectví	letectví	k1gNnSc2	letectví
(	(	kIx(	(
<g/>
Apron	Apron	k1gInSc4	Apron
GA	GA	kA	GA
před	před	k7c7	před
hangárem	hangár	k1gInSc7	hangár
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgInSc6d1	letní
letovém	letový	k2eAgInSc6d1	letový
řádu	řád	k1gInSc6	řád
2017	[number]	k4	2017
od	od	k7c2	od
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
až	až	k9	až
do	do	k7c2	do
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
létá	létat	k5eAaImIp3nS	létat
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
pravidelně	pravidelně	k6eAd1	pravidelně
s	s	k7c7	s
67	[number]	k4	67
dopravců	dopravce	k1gMnPc2	dopravce
do	do	k7c2	do
155	[number]	k4	155
destinací	destinace	k1gFnPc2	destinace
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
10	[number]	k4	10
destinací	destinace	k1gFnPc2	destinace
(	(	kIx(	(
<g/>
Aarhus	Aarhus	k1gMnSc1	Aarhus
<g/>
,	,	kIx,	,
Almería	Almería	k1gMnSc1	Almería
<g/>
,	,	kIx,	,
Jerevan	Jerevan	k1gMnSc1	Jerevan
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
–	–	k?	–
<g/>
Southend	Southend	k1gMnSc1	Southend
<g/>
,	,	kIx,	,
Malmö	Malmö	k1gMnSc1	Malmö
<g/>
,	,	kIx,	,
Menorca	Menorca	k1gMnSc1	Menorca
<g/>
,	,	kIx,	,
Reykjavík	Reykjavík	k1gMnSc1	Reykjavík
<g/>
,	,	kIx,	,
Stavanger	Stavanger	k1gMnSc1	Stavanger
<g/>
,	,	kIx,	,
Trapani	Trapan	k1gMnPc1	Trapan
a	a	k8xC	a
Verona	Verona	k1gFnSc1	Verona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
přibyli	přibýt	k5eAaPmAgMnP	přibýt
dva	dva	k4xCgMnPc1	dva
noví	nový	k2eAgMnPc1d1	nový
dopravci	dopravce	k1gMnPc1	dopravce
Flybe	Flyb	k1gInSc5	Flyb
<g/>
,	,	kIx,	,
Georgian	Georgian	k1gInSc4	Georgian
Airways	Airwaysa	k1gFnPc2	Airwaysa
a	a	k8xC	a
Blue	Blue	k1gFnPc2	Blue
Air	Air	k1gFnSc2	Air
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
navýšení	navýšení	k1gNnSc3	navýšení
frekvencí	frekvence	k1gFnPc2	frekvence
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
20	[number]	k4	20
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
bude	být	k5eAaImBp3nS	být
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
11	[number]	k4	11
aerolinií	aerolinie	k1gFnPc2	aerolinie
dálkové	dálkový	k2eAgFnSc2d1	dálková
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
platnosti	platnost	k1gFnSc2	platnost
zimního	zimní	k2eAgInSc2d1	zimní
letového	letový	k2eAgInSc2d1	letový
řádu	řád	k1gInSc2	řád
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
<g/>
2017	[number]	k4	2017
působilo	působit	k5eAaImAgNnS	působit
na	na	k7c6	na
Letišti	letiště	k1gNnSc6	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
celkem	celkem	k6eAd1	celkem
58	[number]	k4	58
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
(	(	kIx(	(
<g/>
v	v	k7c6	v
sezónně	sezónně	k6eAd1	sezónně
15	[number]	k4	15
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
54	[number]	k4	54
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cestujícím	cestující	k1gMnPc3	cestující
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
spojení	spojení	k1gNnSc6	spojení
do	do	k7c2	do
105	[number]	k4	105
destinací	destinace	k1gFnPc2	destinace
ve	v	k7c6	v
39	[number]	k4	39
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
15	[number]	k4	15
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
bylo	být	k5eAaImAgNnS	být
97	[number]	k4	97
v	v	k7c6	v
38	[number]	k4	38
zemích	zem	k1gFnPc6	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopravci	dopravce	k1gMnPc7	dopravce
létající	létající	k2eAgInSc4d1	létající
pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c4	na
pražské	pražský	k2eAgNnSc4d1	Pražské
letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
:	:	kIx,	:
O	o	k7c6	o
konkrétních	konkrétní	k2eAgFnPc6d1	konkrétní
linkách	linka	k1gFnPc6	linka
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
více	hodně	k6eAd2	hodně
dozvědět	dozvědět	k5eAaPmF	dozvědět
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
společností	společnost	k1gFnPc2	společnost
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
některých	některý	k3yIgInPc6	některý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
nákladní	nákladní	k2eAgFnPc1d1	nákladní
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
létají	létat	k5eAaImIp3nP	létat
pravidelně	pravidelně	k6eAd1	pravidelně
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dřívějších	dřívější	k2eAgFnPc2d1	dřívější
destinací	destinace	k1gFnPc2	destinace
z	z	k7c2	z
Letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1937	[number]	k4	1937
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
letišti	letiště	k1gNnSc6	letiště
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
řada	řada	k1gFnSc1	řada
leteckých	letecký	k2eAgFnPc2d1	letecká
společností	společnost	k1gFnPc2	společnost
i	i	k8xC	i
destinací	destinace	k1gFnPc2	destinace
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
38	[number]	k4	38
<g/>
.	.	kIx.	.
nejrušnější	rušný	k2eAgNnPc1d3	nejrušnější
letiště	letiště	k1gNnPc1	letiště
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
přepravených	přepravený	k2eAgMnPc2d1	přepravený
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejrušnější	rušný	k2eAgNnSc4d3	nejrušnější
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
počtem	počet	k1gInSc7	počet
pasažérů	pasažér	k1gMnPc2	pasažér
z	z	k7c2	z
Letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2015	[number]	k4	2015
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Státy	stát	k1gInPc1	stát
s	s	k7c7	s
nějvětším	nějvětší	k2eAgInSc7d1	nějvětší
počtem	počet	k1gInSc7	počet
cestujících	cestující	k1gMnPc2	cestující
z	z	k7c2	z
Letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
Porovnání	porovnání	k1gNnSc1	porovnání
počtu	počet	k1gInSc2	počet
destinací	destinace	k1gFnPc2	destinace
a	a	k8xC	a
dopravců	dopravce	k1gMnPc2	dopravce
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c4	v
letní	letní	k2eAgNnSc4d1	letní
sezónně	sezónně	k6eAd1	sezónně
<g/>
:	:	kIx,	:
Plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
vzletové	vzletový	k2eAgFnSc2d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc2d1	přistávací
dráhy	dráha	k1gFnSc2	dráha
rovnoběžné	rovnoběžný	k2eAgFnPc4d1	rovnoběžná
s	s	k7c7	s
dráhou	dráha	k1gFnSc7	dráha
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
generální	generální	k2eAgFnSc4d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
České	český	k2eAgFnSc2d1	Česká
správy	správa	k1gFnSc2	správa
letišť	letiště	k1gNnPc2	letiště
Martin	Martin	k1gMnSc1	Martin
Kačur	Kačur	k1gMnSc1	Kačur
uváděl	uvádět	k5eAaImAgMnS	uvádět
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
začátek	začátek	k1gInSc4	začátek
stavby	stavba	k1gFnSc2	stavba
na	na	k7c4	na
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
a	a	k8xC	a
dokončení	dokončení	k1gNnSc4	dokončení
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
projekt	projekt	k1gInSc4	projekt
stavby	stavba	k1gFnSc2	stavba
teprve	teprve	k6eAd1	teprve
27	[number]	k4	27
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
od	od	k7c2	od
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
souhlasné	souhlasný	k2eAgNnSc4d1	souhlasné
stanovisko	stanovisko	k1gNnSc4	stanovisko
EIA	EIA	kA	EIA
(	(	kIx(	(
<g/>
posouzení	posouzení	k1gNnSc4	posouzení
vlivu	vliv	k1gInSc2	vliv
na	na	k7c4	na
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
žádost	žádost	k1gFnSc1	žádost
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
1	[number]	k4	1
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
k	k	k7c3	k
ní	on	k3xPp3gFnSc7	on
přes	přes	k7c4	přes
3600	[number]	k4	3600
vyjádření	vyjádření	k1gNnPc2	vyjádření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
71	[number]	k4	71
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
úplné	úplný	k2eAgNnSc1d1	úplné
zastavení	zastavení	k1gNnSc1	zastavení
leteckého	letecký	k2eAgInSc2d1	letecký
provozu	provoz	k1gInSc2	provoz
od	od	k7c2	od
24	[number]	k4	24
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
do	do	k7c2	do
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
dráha	dráha	k1gFnSc1	dráha
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
dráhou	dráha	k1gFnSc7	dráha
letiště	letiště	k1gNnSc2	letiště
a	a	k8xC	a
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
%	%	kIx~	%
provozu	provoz	k1gInSc2	provoz
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgFnSc1d1	vedlejší
dráha	dráha	k1gFnSc1	dráha
12	[number]	k4	12
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
využita	využít	k5eAaPmNgFnS	využít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
technických	technický	k2eAgInPc2d1	technický
či	či	k8xC	či
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
důvodů	důvod	k1gInPc2	důvod
možné	možný	k2eAgNnSc1d1	možné
používat	používat	k5eAaImF	používat
dráhu	dráha	k1gFnSc4	dráha
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
má	mít	k5eAaImIp3nS	mít
letiště	letiště	k1gNnSc4	letiště
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2	Praha-Ruzyně
dráhy	dráha	k1gFnSc2	dráha
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Hodinová	hodinový	k2eAgFnSc1d1	hodinová
kapacita	kapacita	k1gFnSc1	kapacita
této	tento	k3xDgFnSc2	tento
jediné	jediný	k2eAgFnSc2d1	jediná
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
46	[number]	k4	46
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
48	[number]	k4	48
<g/>
)	)	kIx)	)
pohybů	pohyb	k1gInPc2	pohyb
(	(	kIx(	(
<g/>
vzletů	vzlet	k1gInPc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnPc2	přistání
<g/>
)	)	kIx)	)
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
řadí	řadit	k5eAaImIp3nS	řadit
Ruzyni	Ruzyně	k1gFnSc4	Ruzyně
mezi	mezi	k7c4	mezi
absolutní	absolutní	k2eAgFnSc4d1	absolutní
evropskou	evropský	k2eAgFnSc4d1	Evropská
špičku	špička	k1gFnSc4	špička
co	co	k8xS	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
intenzity	intenzita	k1gFnSc2	intenzita
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
dráze	dráha	k1gFnSc6	dráha
(	(	kIx(	(
<g/>
absolutní	absolutní	k2eAgFnSc1d1	absolutní
jedničkou	jednička	k1gFnSc7	jednička
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
Londýn	Londýn	k1gInSc1	Londýn
<g/>
/	/	kIx~	/
<g/>
Gatwick	Gatwick	k1gInSc1	Gatwick
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
46	[number]	k4	46
–	–	k?	–
50	[number]	k4	50
pohybů	pohyb	k1gInPc2	pohyb
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
poměru	poměr	k1gInSc6	poměr
vzletů	vzlet	k1gInPc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc2	přistání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
však	však	k9	však
již	již	k9	již
provozu	provoz	k1gInSc2	provoz
nestačí	stačit	k5eNaBmIp3nS	stačit
a	a	k8xC	a
dráhový	dráhový	k2eAgInSc1d1	dráhový
systém	systém	k1gInSc1	systém
letiště	letiště	k1gNnSc2	letiště
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
přetížen	přetížen	k2eAgInSc4d1	přetížen
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
problému	problém	k1gInSc2	problém
je	být	k5eAaImIp3nS	být
výstavba	výstavba	k1gFnSc1	výstavba
nové	nový	k2eAgFnSc2d1	nová
paralelní	paralelní	k2eAgFnSc2d1	paralelní
dráhy	dráha	k1gFnSc2	dráha
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
L.	L.	kA	L.
Ta	ten	k3xDgFnSc1	ten
bude	být	k5eAaImBp3nS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
1525	[number]	k4	1525
metrů	metr	k1gInPc2	metr
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
stávající	stávající	k2eAgFnSc2d1	stávající
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
přeznačena	přeznačit	k5eAaPmNgFnS	přeznačit
na	na	k7c4	na
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
L	L	kA	L
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
<g/>
R	R	kA	R
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
dráha	dráha	k1gFnSc1	dráha
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
původní	původní	k2eAgFnSc2d1	původní
dráhy	dráha	k1gFnSc2	dráha
letiště	letiště	k1gNnSc1	letiště
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bude	být	k5eAaImBp3nS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
dráha	dráha	k1gFnSc1	dráha
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
rozměry	rozměr	k1gInPc4	rozměr
3550	[number]	k4	3550
×	×	k?	×
60	[number]	k4	60
m.	m.	k?	m.
Bude	být	k5eAaImBp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
jako	jako	k9	jako
přístrojová	přístrojový	k2eAgFnSc1d1	přístrojová
dráha	dráha	k1gFnSc1	dráha
pro	pro	k7c4	pro
přesné	přesný	k2eAgNnSc4d1	přesné
přiblížení	přiblížení	k1gNnSc4	přiblížení
zařízením	zařízení	k1gNnSc7	zařízení
ILS	ILS	kA	ILS
CAT	CAT	kA	CAT
IIIB	IIIB	kA	IIIB
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
směrech	směr	k1gInPc6	směr
(	(	kIx(	(
<g/>
ILS	ILS	kA	ILS
CAT	CAT	kA	CAT
IIIB	IIIB	kA	IIIB
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
např.	např.	kA	např.
automatické	automatický	k2eAgNnSc1d1	automatické
přistání	přistání	k1gNnSc1	přistání
při	při	k7c6	při
nulové	nulový	k2eAgFnSc6d1	nulová
dohlednosti	dohlednost	k1gFnSc6	dohlednost
až	až	k9	až
do	do	k7c2	do
dotyku	dotyk	k1gInSc2	dotyk
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Investice	investice	k1gFnPc1	investice
na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
paralelní	paralelní	k2eAgFnSc4d1	paralelní
dráhu	dráha	k1gFnSc4	dráha
bude	být	k5eAaImBp3nS	být
9	[number]	k4	9
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pozemky	pozemek	k1gInPc1	pozemek
pro	pro	k7c4	pro
paralelní	paralelní	k2eAgFnSc4d1	paralelní
dráhu	dráha	k1gFnSc4	dráha
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gFnSc4	jejich
dlouho	dlouho	k6eAd1	dlouho
známý	známý	k2eAgInSc4d1	známý
strategický	strategický	k2eAgInSc4d1	strategický
záměr	záměr	k1gInSc4	záměr
prodal	prodat	k5eAaPmAgMnS	prodat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
státní	státní	k2eAgInSc1d1	státní
Pozemkový	pozemkový	k2eAgInSc1d1	pozemkový
fond	fond	k1gInSc1	fond
soukromým	soukromý	k2eAgMnPc3d1	soukromý
vlastníkům	vlastník	k1gMnPc3	vlastník
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
ha	ha	kA	ha
těchto	tento	k3xDgInPc2	tento
pozemků	pozemek	k1gInPc2	pozemek
nakoupila	nakoupit	k5eAaPmAgFnS	nakoupit
investiční	investiční	k2eAgFnSc1d1	investiční
skupina	skupina	k1gFnSc1	skupina
Penta	Pent	k1gInSc2	Pent
Investments	Investmentsa	k1gFnPc2	Investmentsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
když	když	k8xS	když
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
lepší	dobrý	k2eAgFnSc4d2	lepší
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
než	než	k8xS	než
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
Kč	Kč	kA	Kč
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
Letiště	letiště	k1gNnSc1	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
tyto	tento	k3xDgInPc4	tento
pozemky	pozemek	k1gInPc4	pozemek
získalo	získat	k5eAaPmAgNnS	získat
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
klíčové	klíčový	k2eAgInPc4d1	klíčový
pro	pro	k7c4	pro
cenu	cena	k1gFnSc4	cena
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
připravováno	připravovat	k5eAaImNgNnS	připravovat
k	k	k7c3	k
privatizaci	privatizace	k1gFnSc3	privatizace
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
zisk	zisk	k1gInSc1	zisk
transakce	transakce	k1gFnSc2	transakce
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Penty	Penta	k1gFnSc2	Penta
měl	mít	k5eAaImAgMnS	mít
činit	činit	k5eAaImF	činit
údajně	údajně	k6eAd1	údajně
3	[number]	k4	3
až	až	k9	až
3,5	[number]	k4	3,5
mld	mld	k?	mld
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgFnSc4d1	čtvereční
pozemku	pozemka	k1gFnSc4	pozemka
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
přibližně	přibližně	k6eAd1	přibližně
5500	[number]	k4	5500
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
týdeníku	týdeník	k1gInSc2	týdeník
EURO	euro	k1gNnSc1	euro
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
důvěryhodné	důvěryhodný	k2eAgInPc4d1	důvěryhodný
zdroje	zdroj	k1gInPc4	zdroj
žádala	žádat	k5eAaImAgFnS	žádat
přitom	přitom	k6eAd1	přitom
skupina	skupina	k1gFnSc1	skupina
za	za	k7c4	za
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
deset	deset	k4xCc4	deset
až	až	k9	až
dvanáct	dvanáct	k4xCc4	dvanáct
tisíc	tisíc	k4xCgInSc4	tisíc
korun	koruna	k1gFnPc2	koruna
(	(	kIx(	(
<g/>
celkově	celkově	k6eAd1	celkově
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedm	sedm	k4xCc4	sedm
miliard	miliarda	k4xCgFnPc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolumajitel	spolumajitel	k1gMnSc1	spolumajitel
Penty	Penta	k1gMnSc2	Penta
Marek	Marek	k1gMnSc1	Marek
Dospiva	Dospiv	k1gMnSc2	Dospiv
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
metr	metr	k1gInSc4	metr
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
jako	jako	k8xC	jako
cenu	cena	k1gFnSc4	cena
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
výhodnou	výhodný	k2eAgFnSc4d1	výhodná
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Transakce	transakce	k1gFnSc1	transakce
nakonec	nakonec	k6eAd1	nakonec
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
ne	ne	k9	ne
jako	jako	k9	jako
odkup	odkup	k1gInSc4	odkup
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přes	přes	k7c4	přes
nákup	nákup	k1gInSc4	nákup
akcií	akcie	k1gFnPc2	akcie
společnosti	společnost	k1gFnSc2	společnost
Realitní	realitní	k2eAgFnSc1d1	realitní
developerská	developerský	k2eAgFnSc1d1	developerská
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
snížení	snížení	k1gNnSc1	snížení
odvodu	odvod	k1gInSc2	odvod
daní	daň	k1gFnPc2	daň
pro	pro	k7c4	pro
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
Parlament	parlament	k1gInSc1	parlament
ČR	ČR	kA	ČR
schválil	schválit	k5eAaPmAgInS	schválit
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
544	[number]	k4	544
<g/>
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
vzletové	vzletový	k2eAgFnSc2d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc2d1	přistávací
dráhy	dráha	k1gFnSc2	dráha
06R	[number]	k4	06R
-	-	kIx~	-
24L	[number]	k4	24L
letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
Ruzyně	Ruzyně	k1gFnSc1	Ruzyně
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
deklaroval	deklarovat	k5eAaBmAgInS	deklarovat
veřejný	veřejný	k2eAgInSc4d1	veřejný
zájem	zájem	k1gInSc4	zájem
paralelní	paralelní	k2eAgFnSc2d1	paralelní
dráhy	dráha	k1gFnSc2	dráha
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
vyvlastnění	vyvlastnění	k1gNnSc3	vyvlastnění
potřebných	potřebný	k2eAgInPc2d1	potřebný
pozemků	pozemek	k1gInPc2	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
ČR	ČR	kA	ČR
za	za	k7c4	za
protiústavní	protiústavní	k2eAgFnPc4d1	protiústavní
a	a	k8xC	a
zrušil	zrušit	k5eAaPmAgMnS	zrušit
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
vyhověl	vyhovět	k5eAaPmAgMnS	vyhovět
tak	tak	k6eAd1	tak
návrhu	návrh	k1gInSc2	návrh
17	[number]	k4	17
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
připojil	připojit	k5eAaPmAgMnS	připojit
další	další	k2eAgMnSc1d1	další
senátor	senátor	k1gMnSc1	senátor
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
výstavbě	výstavba	k1gFnSc3	výstavba
dráhy	dráha	k1gFnSc2	dráha
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
řada	řada	k1gFnSc1	řada
sdružení	sdružení	k1gNnSc2	sdružení
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
obávají	obávat	k5eAaImIp3nP	obávat
zhoršení	zhoršení	k1gNnSc4	zhoršení
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
např.	např.	kA	např.
o	o	k7c4	o
Občanské	občanský	k2eAgNnSc4d1	občanské
sdružení	sdružení	k1gNnSc4	sdružení
pro	pro	k7c4	pro
Nebušice	Nebušic	k1gMnPc4	Nebušic
<g/>
.	.	kIx.	.
</s>
<s>
Kritikům	kritik	k1gMnPc3	kritik
výstavby	výstavba	k1gFnSc2	výstavba
letiště	letiště	k1gNnSc2	letiště
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
úspěšně	úspěšně	k6eAd1	úspěšně
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
napadnout	napadnout	k5eAaPmF	napadnout
nezákonnost	nezákonnost	k1gFnSc4	nezákonnost
rozhodování	rozhodování	k1gNnSc2	rozhodování
orgánů	orgán	k1gInPc2	orgán
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Praha	Praha	k1gFnSc1	Praha
při	při	k7c6	při
projednávání	projednávání	k1gNnSc6	projednávání
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
neúspěšně	úspěšně	k6eNd1	úspěšně
rozsudek	rozsudek	k1gInSc4	rozsudek
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
napadnout	napadnout	k5eAaPmF	napadnout
stížností	stížnost	k1gFnSc7	stížnost
u	u	k7c2	u
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
ze	z	k7c2	z
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
stížnost	stížnost	k1gFnSc1	stížnost
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
tak	tak	k6eAd1	tak
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
nezákonnost	nezákonnost	k1gFnSc4	nezákonnost
postupu	postup	k1gInSc2	postup
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
stavby	stavba	k1gFnSc2	stavba
nové	nový	k2eAgFnSc2d1	nová
přistávací	přistávací	k2eAgFnSc2d1	přistávací
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
následně	následně	k6eAd1	následně
přijalo	přijmout	k5eAaPmAgNnS	přijmout
novou	nový	k2eAgFnSc4d1	nová
změnu	změna	k1gFnSc4	změna
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
napadena	napadnout	k5eAaPmNgFnS	napadnout
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
ten	ten	k3xDgInSc1	ten
podanou	podaný	k2eAgFnSc4d1	podaná
kasační	kasační	k2eAgFnSc4d1	kasační
stížnost	stížnost	k1gFnSc4	stížnost
zamítl	zamítnout	k5eAaPmAgInS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
musí	muset	k5eAaImIp3nS	muset
tímto	tento	k3xDgInSc7	tento
případem	případ	k1gInSc7	případ
znovu	znovu	k6eAd1	znovu
zabývat	zabývat	k5eAaImF	zabývat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
formalisticky	formalisticky	k6eAd1	formalisticky
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
takový	takový	k3xDgInSc1	takový
postup	postup	k1gInSc1	postup
neshledal	shledat	k5eNaPmAgInS	shledat
senát	senát	k1gInSc1	senát
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
jako	jako	k8xC	jako
ústavně	ústavně	k6eAd1	ústavně
souladný	souladný	k2eAgInSc1d1	souladný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Verdikt	verdikt	k1gInSc1	verdikt
soudu	soud	k1gInSc2	soud
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
projekt	projekt	k1gInSc4	projekt
dráhy	dráha	k1gFnSc2	dráha
bezprostřední	bezprostřední	k2eAgInSc4d1	bezprostřední
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1938	[number]	k4	1938
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Vozovna	vozovna	k1gFnSc1	vozovna
Vokovice	Vokovice	k1gFnSc1	Vokovice
–	–	k?	–
Letiště	letiště	k1gNnSc2	letiště
zavedena	zaveden	k2eAgFnSc1d1	zavedena
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
označená	označený	k2eAgFnSc1d1	označená
písmenem	písmeno	k1gNnSc7	písmeno
I.	I.	kA	I.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
byla	být	k5eAaImAgFnS	být
přečíslována	přečíslovat	k5eAaPmNgFnS	přečíslovat
na	na	k7c4	na
108	[number]	k4	108
<g/>
.	.	kIx.	.
</s>
<s>
Rychlíková	rychlíkový	k2eAgFnSc1d1	rychlíková
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
225	[number]	k4	225
<g/>
,	,	kIx,	,
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
poválečné	poválečný	k2eAgFnSc6d1	poválečná
době	doba	k1gFnSc6	doba
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
třech	tři	k4xCgFnPc2	tři
městských	městský	k2eAgFnPc2d1	městská
autobusových	autobusový	k2eAgFnPc2d1	autobusová
linek	linka	k1gFnPc2	linka
s	s	k7c7	s
jednoslužným	jednoslužný	k2eAgInSc7d1	jednoslužný
provozem	provoz	k1gInSc7	provoz
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
bez	bez	k7c2	bez
průvodčího	průvodčí	k1gMnSc2	průvodčí
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
zde	zde	k6eAd1	zde
platilo	platit	k5eAaImAgNnS	platit
dvojnásobné	dvojnásobný	k2eAgNnSc4d1	dvojnásobné
jízdné	jízdné	k1gNnSc4	jízdné
oproti	oproti	k7c3	oproti
běžným	běžný	k2eAgFnPc3d1	běžná
městským	městský	k2eAgFnPc3d1	městská
linkám	linka	k1gFnPc3	linka
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
vyjadřovala	vyjadřovat	k5eAaImAgFnS	vyjadřovat
i	i	k8xC	i
dvojka	dvojka	k1gFnSc1	dvojka
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
čísla	číslo	k1gNnSc2	číslo
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
linka	linka	k1gFnSc1	linka
přečíslována	přečíslovat	k5eAaPmNgFnS	přečíslovat
na	na	k7c4	na
běžnou	běžný	k2eAgFnSc4d1	běžná
linku	linka	k1gFnSc4	linka
119	[number]	k4	119
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
nebo	nebo	k8xC	nebo
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
možností	možnost	k1gFnPc2	možnost
<g/>
:	:	kIx,	:
Městské	městský	k2eAgFnSc2d1	městská
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
za	za	k7c4	za
běžný	běžný	k2eAgInSc4d1	běžný
přestupní	přestupní	k2eAgInSc4d1	přestupní
městský	městský	k2eAgInSc4d1	městský
tarif	tarif	k1gInSc4	tarif
pražské	pražský	k2eAgFnSc2d1	Pražská
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
provozované	provozovaný	k2eAgNnSc1d1	provozované
Dopravním	dopravní	k2eAgInSc7d1	dopravní
podnikem	podnik	k1gInSc7	podnik
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
přestupní	přestupní	k2eAgNnSc1d1	přestupní
jízdné	jízdné	k1gNnSc1	jízdné
na	na	k7c4	na
90	[number]	k4	90
minut	minuta	k1gFnPc2	minuta
činí	činit	k5eAaImIp3nS	činit
32	[number]	k4	32
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
dalších	další	k2eAgInPc2d1	další
16	[number]	k4	16
Kč	Kč	kA	Kč
se	se	k3xPyFc4	se
platí	platit	k5eAaImIp3nS	platit
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
zavazadlo	zavazadlo	k1gNnSc4	zavazadlo
<g/>
,	,	kIx,	,
při	při	k7c6	při
nákupu	nákup	k1gInSc6	nákup
jízdenky	jízdenka	k1gFnPc1	jízdenka
u	u	k7c2	u
řidiče	řidič	k1gMnSc2	řidič
je	být	k5eAaImIp3nS	být
jízdné	jízdné	k1gNnSc1	jízdné
dražší	drahý	k2eAgMnSc1d2	dražší
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
Kč	Kč	kA	Kč
<g/>
)	)	kIx)	)
a	a	k8xC	a
nelze	lze	k6eNd1	lze
koupit	koupit	k5eAaPmF	koupit
levnější	levný	k2eAgFnSc4d2	levnější
<g/>
,	,	kIx,	,
půlhodinovou	půlhodinový	k2eAgFnSc4d1	půlhodinová
jízdenku	jízdenka	k1gFnSc4	jízdenka
<g/>
.	.	kIx.	.
</s>
<s>
Zastávková	zastávkový	k2eAgFnSc1d1	zastávková
linka	linka	k1gFnSc1	linka
119	[number]	k4	119
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
linky	linka	k1gFnSc2	linka
metra	metro	k1gNnSc2	metro
A	a	k8xC	a
Nádraží	nádraží	k1gNnSc2	nádraží
Veleslavín	Veleslavín	k1gMnSc1	Veleslavín
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
jezdila	jezdit	k5eAaImAgFnS	jezdit
až	až	k9	až
ke	k	k7c3	k
stanici	stanice	k1gFnSc3	stanice
metra	metro	k1gNnSc2	metro
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
na	na	k7c4	na
metro	metro	k1gNnSc4	metro
dostat	dostat	k5eAaPmF	dostat
za	za	k7c4	za
cca	cca	kA	cca
35	[number]	k4	35
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
100	[number]	k4	100
na	na	k7c4	na
konečnou	konečná	k1gFnSc4	konečná
metra	metr	k1gMnSc2	metr
B	B	kA	B
Zličín	Zličín	k1gInSc1	Zličín
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
využívá	využívat	k5eAaImIp3nS	využívat
rychlostní	rychlostní	k2eAgInSc4d1	rychlostní
Pražský	pražský	k2eAgInSc4d1	pražský
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
na	na	k7c4	na
metro	metro	k1gNnSc4	metro
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
zastávková	zastávkový	k2eAgFnSc1d1	zastávková
linka	linka	k1gFnSc1	linka
910	[number]	k4	910
přes	přes	k7c4	přes
Liboc	Liboc	k1gFnSc4	Liboc
<g/>
,	,	kIx,	,
Břevnov	Břevnov	k1gInSc1	Břevnov
<g/>
,	,	kIx,	,
Smíchov	Smíchov	k1gInSc1	Smíchov
a	a	k8xC	a
centrum	centrum	k1gNnSc1	centrum
Prahy	Praha	k1gFnSc2	Praha
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Pankrác	Pankrác	k1gFnSc4	Pankrác
<g/>
,	,	kIx,	,
Krč	Krč	k1gFnSc4	Krč
a	a	k8xC	a
Modřany	Modřany	k1gInPc4	Modřany
<g/>
.	.	kIx.	.
</s>
<s>
Zastávka	zastávka	k1gFnSc1	zastávka
Karlovo	Karlův	k2eAgNnSc1d1	Karlovo
náměstí	náměstí	k1gNnSc1	náměstí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
poblíž	poblíž	k7c2	poblíž
nočního	noční	k2eAgInSc2d1	noční
centrálního	centrální	k2eAgInSc2d1	centrální
přestupního	přestupní	k2eAgInSc2d1	přestupní
uzlu	uzel	k1gInSc2	uzel
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
zastávka	zastávka	k1gFnSc1	zastávka
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgMnSc4d1	Pavlův
je	být	k5eAaImIp3nS	být
přestupním	přestupní	k2eAgInSc7d1	přestupní
bodem	bod	k1gInSc7	bod
na	na	k7c4	na
páteřní	páteřní	k2eAgFnPc4d1	páteřní
noční	noční	k2eAgFnPc4d1	noční
autobusové	autobusový	k2eAgFnPc4d1	autobusová
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jízdy	jízda	k1gFnSc2	jízda
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
s	s	k7c7	s
přestupem	přestup	k1gInSc7	přestup
na	na	k7c4	na
tramvaj	tramvaj	k1gFnSc4	tramvaj
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
50	[number]	k4	50
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
jezdila	jezdit	k5eAaImAgFnS	jezdit
noční	noční	k2eAgFnSc1d1	noční
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
jen	jen	k9	jen
k	k	k7c3	k
tramvajové	tramvajový	k2eAgFnSc3d1	tramvajová
konečné	konečná	k1gFnSc3	konečná
Divoká	divoký	k2eAgFnSc1d1	divoká
Šárka	Šárka	k1gFnSc1	Šárka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
navazovala	navazovat	k5eAaImAgFnS	navazovat
noční	noční	k2eAgFnSc1d1	noční
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
linka	linka	k1gFnSc1	linka
51	[number]	k4	51
<g/>
,	,	kIx,	,
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
linka	linka	k1gFnSc1	linka
510	[number]	k4	510
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
až	až	k9	až
na	na	k7c4	na
Sídliště	sídliště	k1gNnSc4	sídliště
Stodůlky	stodůlka	k1gFnSc2	stodůlka
(	(	kIx(	(
<g/>
v	v	k7c6	v
hodinovém	hodinový	k2eAgInSc6d1	hodinový
intervalu	interval	k1gInSc6	interval
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
místo	místo	k7c2	místo
Stodůlek	stodůlka	k1gFnPc2	stodůlka
odkloněna	odkloněn	k2eAgFnSc1d1	odkloněna
přes	přes	k7c4	přes
centrum	centrum	k1gNnSc4	centrum
do	do	k7c2	do
Modřan	Modřany	k1gInPc2	Modřany
(	(	kIx(	(
<g/>
Na	na	k7c6	na
Beránku	beránek	k1gInSc6	beránek
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
interval	interval	k1gInSc1	interval
byl	být	k5eAaImAgInS	být
zkrácen	zkrátit	k5eAaPmNgInS	zkrátit
na	na	k7c4	na
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
číslo	číslo	k1gNnSc4	číslo
je	být	k5eAaImIp3nS	být
910	[number]	k4	910
<g/>
.	.	kIx.	.
</s>
<s>
Zastávková	zastávkový	k2eAgFnSc1d1	zastávková
linka	linka	k1gFnSc1	linka
191	[number]	k4	191
přes	přes	k7c4	přes
Liboc	Liboc	k1gFnSc4	Liboc
a	a	k8xC	a
Břevnov	Břevnov	k1gInSc4	Břevnov
na	na	k7c4	na
Smíchov	Smíchov	k1gInSc4	Smíchov
(	(	kIx(	(
<g/>
Na	na	k7c4	na
Knížecí	knížecí	k2eAgNnSc4d1	knížecí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
Prahy	Praha	k1gFnSc2	Praha
denní	denní	k2eAgFnSc7d1	denní
obdobou	obdoba	k1gFnSc7	obdoba
noční	noční	k2eAgFnSc2d1	noční
linky	linka	k1gFnSc2	linka
910	[number]	k4	910
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
linka	linka	k1gFnSc1	linka
100	[number]	k4	100
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
2015	[number]	k4	2015
nesla	nést	k5eAaImAgFnS	nést
tato	tento	k3xDgFnSc1	tento
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
obdobná	obdobný	k2eAgFnSc1d1	obdobná
linka	linka	k1gFnSc1	linka
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
číslo	číslo	k1gNnSc1	číslo
179	[number]	k4	179
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2012	[number]	k4	2012
zajíždělo	zajíždět	k5eAaImAgNnS	zajíždět
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
též	též	k9	též
několik	několik	k4yIc4	několik
spojů	spoj	k1gInPc2	spoj
linky	linka	k1gFnSc2	linka
254	[number]	k4	254
<g/>
,	,	kIx,	,
z	z	k7c2	z
Dejvické	dejvický	k2eAgFnSc2d1	Dejvická
přes	přes	k7c4	přes
Nebušice	Nebušice	k1gFnPc4	Nebušice
a	a	k8xC	a
Přední	přední	k2eAgFnSc4d1	přední
Kopaninu	kopanina	k1gFnSc4	kopanina
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
mezi	mezi	k7c7	mezi
Přední	přední	k2eAgFnSc7d1	přední
Kopaninou	kopanina	k1gFnSc7	kopanina
a	a	k8xC	a
letištěm	letiště	k1gNnSc7	letiště
veřejná	veřejný	k2eAgFnSc1d1	veřejná
linková	linkový	k2eAgFnSc1d1	Linková
doprava	doprava	k1gFnSc1	doprava
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
Airport	Airport	k1gInSc4	Airport
Express	express	k1gInSc1	express
(	(	kIx(	(
<g/>
AE	AE	kA	AE
<g/>
,	,	kIx,	,
103120	[number]	k4	103120
<g/>
)	)	kIx)	)
do	do	k7c2	do
zastávky	zastávka	k1gFnSc2	zastávka
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
staví	stavit	k5eAaBmIp3nS	stavit
též	též	k9	též
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
u	u	k7c2	u
Masarykova	Masarykův	k2eAgNnSc2d1	Masarykovo
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
DPP	DPP	kA	DPP
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Českými	český	k2eAgFnPc7d1	Česká
drahami	draha	k1gFnPc7	draha
za	za	k7c4	za
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
nepřestupní	přestupní	k2eNgInSc4d1	nepřestupní
tarif	tarif	k1gInSc4	tarif
Pražské	pražský	k2eAgFnSc2d1	Pražská
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jízdenky	jízdenka	k1gFnSc2	jízdenka
zakoupené	zakoupený	k2eAgFnSc2d1	zakoupená
u	u	k7c2	u
řidiče	řidič	k1gInSc2	řidič
platí	platit	k5eAaImIp3nS	platit
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
lince	linka	k1gFnSc6	linka
i	i	k9	i
železniční	železniční	k2eAgFnPc1d1	železniční
jízdenky	jízdenka	k1gFnPc1	jízdenka
ČD	ČD	kA	ČD
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
železniční	železniční	k2eAgFnPc4d1	železniční
jízdenky	jízdenka	k1gFnPc4	jízdenka
vystavené	vystavený	k2eAgFnPc4d1	vystavená
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
"	"	kIx"	"
<g/>
Praha-letiště	Prahaetiště	k1gNnSc1	Praha-letiště
(	(	kIx(	(
<g/>
NAD	NAD	kA	NAD
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
Airport	Airport	k1gInSc1	Airport
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zvýhodněné	zvýhodněný	k2eAgFnPc1d1	zvýhodněná
jízdenky	jízdenka	k1gFnPc1	jízdenka
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
linku	linka	k1gFnSc4	linka
nabízí	nabízet	k5eAaImIp3nS	nabízet
svým	svůj	k3xOyFgMnPc3	svůj
zákazníkům	zákazník	k1gMnPc3	zákazník
i	i	k8xC	i
RegioJet	RegioJet	k1gMnSc1	RegioJet
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
jízdy	jízda	k1gFnSc2	jízda
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
33	[number]	k4	33
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
interval	interval	k1gInSc4	interval
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
jízdné	jízdné	k1gNnSc1	jízdné
činí	činit	k5eAaImIp3nS	činit
60	[number]	k4	60
Kč	Kč	kA	Kč
<g/>
,	,	kIx,	,
zavazadla	zavazadlo	k1gNnPc1	zavazadlo
se	se	k3xPyFc4	se
však	však	k9	však
přepravují	přepravovat	k5eAaImIp3nP	přepravovat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Minibusová	Minibusový	k2eAgFnSc1d1	Minibusová
městská	městský	k2eAgFnSc1d1	městská
linka	linka	k1gFnSc1	linka
103102	[number]	k4	103102
společnosti	společnost	k1gFnSc2	společnost
CEDAZ	CEDAZ	kA	CEDAZ
jezdí	jezdit	k5eAaImIp3nS	jezdit
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
V	v	k7c6	v
celnici	celnice	k1gFnSc6	celnice
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
náměstím	náměstí	k1gNnSc7	náměstí
Republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
Masarykovým	Masarykův	k2eAgNnSc7d1	Masarykovo
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
před	před	k7c4	před
kancelář	kancelář	k1gFnSc4	kancelář
ČSA	ČSA	kA	ČSA
<g/>
.	.	kIx.	.
</s>
<s>
Jízdné	jízdné	k1gNnSc1	jízdné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
činí	činit	k5eAaImIp3nS	činit
150	[number]	k4	150
Kč	Kč	kA	Kč
za	za	k7c4	za
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
standardní	standardní	k2eAgInSc4d1	standardní
počet	počet	k1gInSc4	počet
zavazadel	zavazadlo	k1gNnPc2	zavazadlo
se	se	k3xPyFc4	se
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nástupiště	nástupiště	k1gNnSc2	nástupiště
pod	pod	k7c7	pod
estakádou	estakáda	k1gFnSc7	estakáda
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
příměstské	příměstský	k2eAgFnPc1d1	příměstská
a	a	k8xC	a
meziměstské	meziměstský	k2eAgFnPc1d1	meziměstská
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
<g/>
:	:	kIx,	:
linka	linka	k1gFnSc1	linka
PID	PID	kA	PID
319	[number]	k4	319
dopravce	dopravce	k1gMnSc2	dopravce
ČSAD	ČSAD	kA	ČSAD
MHD	MHD	kA	MHD
Kladno	Kladno	k1gNnSc1	Kladno
do	do	k7c2	do
vesnic	vesnice	k1gFnPc2	vesnice
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
letiště	letiště	k1gNnSc2	letiště
(	(	kIx(	(
<g/>
Kněževes	Kněževes	k1gFnSc1	Kněževes
<g/>
,	,	kIx,	,
Dobrovíz	Dobrovíz	k1gInSc1	Dobrovíz
<g/>
,	,	kIx,	,
Hostouň	Hostouň	k1gFnSc1	Hostouň
<g/>
,	,	kIx,	,
Jeneč	Jeneč	k1gInSc1	Jeneč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
zde	zde	k6eAd1	zde
některé	některý	k3yIgInPc1	některý
spoje	spoj	k1gInPc1	spoj
linek	linka	k1gFnPc2	linka
SID	SID	kA	SID
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
Kladensko	Kladensko	k1gNnSc4	Kladensko
<g />
.	.	kIx.	.
</s>
<s>
provozovaných	provozovaný	k2eAgNnPc2d1	provozované
ČSAD	ČSAD	kA	ČSAD
MHD	MHD	kA	MHD
Kladno	Kladno	k1gNnSc4	Kladno
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
22	[number]	k4	22
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
23	[number]	k4	23
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
A	a	k9	a
<g/>
56	[number]	k4	56
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neintegrovaná	integrovaný	k2eNgFnSc1d1	neintegrovaná
linka	linka	k1gFnSc1	linka
220097	[number]	k4	220097
dopravce	dopravce	k1gMnSc1	dopravce
Exprescar	Exprescar	k1gMnSc1	Exprescar
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
neintegrovaná	integrovaný	k2eNgFnSc1d1	neintegrovaná
linka	linka	k1gFnSc1	linka
ČSAD	ČSAD	kA	ČSAD
Slaný	Slaný	k1gInSc1	Slaný
do	do	k7c2	do
Slaného	Slaný	k1gInSc2	Slaný
(	(	kIx(	(
<g/>
220059	[number]	k4	220059
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dálková	dálkový	k2eAgFnSc1d1	dálková
linka	linka	k1gFnSc1	linka
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
141103	[number]	k4	141103
na	na	k7c4	na
Karlovarsko	Karlovarsko	k1gNnSc4	Karlovarsko
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
dálkové	dálkový	k2eAgFnSc6d1	dálková
lince	linka	k1gFnSc6	linka
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
i	i	k9	i
frekvenci	frekvence	k1gFnSc4	frekvence
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
ÚAN	ÚAN	kA	ÚAN
Florenc	Florenc	k1gFnSc4	Florenc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
základní	základní	k2eAgNnSc4d1	základní
jízdné	jízdné	k1gNnSc4	jízdné
60	[number]	k4	60
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnSc2d1	městská
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
severní	severní	k2eAgInSc4d1	severní
terminál	terminál	k1gInSc4	terminál
při	při	k7c6	při
příjezdu	příjezd	k1gInSc6	příjezd
i	i	k8xC	i
při	při	k7c6	při
odjezdu	odjezd	k1gInSc6	odjezd
objíždějí	objíždět	k5eAaImIp3nP	objíždět
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
zastávky	zastávka	k1gFnPc1	zastávka
u	u	k7c2	u
terminálu	terminál	k1gInSc2	terminál
se	se	k3xPyFc4	se
jmenují	jmenovat	k5eAaImIp3nP	jmenovat
Terminál	terminál	k1gInSc1	terminál
1	[number]	k4	1
a	a	k8xC	a
Terminál	terminál	k1gInSc1	terminál
2	[number]	k4	2
<g/>
,	,	kIx,	,
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
otočce	otočka	k1gFnSc6	otočka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Letiště	letiště	k1gNnSc1	letiště
a	a	k8xC	a
nácestná	nácestný	k2eAgFnSc1d1	nácestná
zastávka	zastávka	k1gFnSc1	zastávka
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
otočky	otočka	k1gFnSc2	otočka
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Schengenská	schengenský	k2eAgFnSc1d1	Schengenská
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zastávkové	zastávkový	k2eAgFnPc1d1	zastávková
linky	linka	k1gFnPc1	linka
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
v	v	k7c6	v
zastávkách	zastávka	k1gFnPc6	zastávka
U	u	k7c2	u
Hangáru	hangár	k1gInSc2	hangár
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Padesátníku	padesátník	k1gInSc6	padesátník
<g/>
,	,	kIx,	,
Terminál	terminál	k1gInSc1	terminál
3	[number]	k4	3
a	a	k8xC	a
K	k	k7c3	k
Letišti	letiště	k1gNnSc3	letiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
kladenské	kladenský	k2eAgFnSc6d1	kladenská
výpadovce	výpadovka	k1gFnSc6	výpadovka
(	(	kIx(	(
<g/>
Lipská	lipský	k2eAgFnSc1d1	Lipská
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Evropská	evropský	k2eAgFnSc1d1	Evropská
<g/>
)	)	kIx)	)
u	u	k7c2	u
odbočení	odbočení	k1gNnSc2	odbočení
Aviatické	aviatický	k2eAgFnSc2d1	Aviatická
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
dálnici	dálnice	k1gFnSc6	dálnice
D	D	kA	D
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
zastávka	zastávka	k1gFnSc1	zastávka
"	"	kIx"	"
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Aviatická	aviatický	k2eAgFnSc1d1	Aviatická
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
linky	linka	k1gFnPc4	linka
a	a	k8xC	a
spoje	spoj	k1gInPc4	spoj
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
nezajížděly	zajíždět	k5eNaImAgFnP	zajíždět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
220100	[number]	k4	220100
ČSAD	ČSAD	kA	ČSAD
Slaný	Slaný	k1gInSc1	Slaný
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
220078	[number]	k4	220078
ČSAD	ČSAD	kA	ČSAD
Slaný	slaný	k2eAgInSc4d1	slaný
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
též	též	k9	též
ji	on	k3xPp3gFnSc4	on
užívaly	užívat	k5eAaImAgFnP	užívat
linky	linka	k1gFnPc1	linka
DPÚK	DPÚK	kA	DPÚK
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
570040	[number]	k4	570040
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
k	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
a	a	k8xC	a
pro	pro	k7c4	pro
linku	linka	k1gFnSc4	linka
560010	[number]	k4	560010
Louny	Louny	k1gInPc7	Louny
<g/>
–	–	k?	–
<g/>
Praha	Praha	k1gFnSc1	Praha
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
jako	jako	k8xC	jako
nepoužívaná	používaný	k2eNgFnSc1d1	nepoužívaná
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
z	z	k7c2	z
jízdního	jízdní	k2eAgInSc2d1	jízdní
řádu	řád	k1gInSc2	řád
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
umístění	umístění	k1gNnSc1	umístění
zastávky	zastávka	k1gFnSc2	zastávka
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
dotčeno	dotčen	k2eAgNnSc1d1	dotčeno
přestavbou	přestavba	k1gFnSc7	přestavba
mimoúrovňové	mimoúrovňový	k2eAgFnSc2d1	mimoúrovňová
křižovatky	křižovatka	k1gFnSc2	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
využít	využít	k5eAaPmF	využít
také	také	k9	také
vozidel	vozidlo	k1gNnPc2	vozidlo
taxislužby	taxislužba	k1gFnSc2	taxislužba
<g/>
.	.	kIx.	.
</s>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
taxislužby	taxislužba	k1gFnSc2	taxislužba
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
terminálů	terminál	k1gInPc2	terminál
využívají	využívat	k5eAaPmIp3nP	využívat
výhradně	výhradně	k6eAd1	výhradně
taxikářské	taxikářský	k2eAgFnPc1d1	taxikářská
společnosti	společnost	k1gFnPc1	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
provozovatelem	provozovatel	k1gMnSc7	provozovatel
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
taxislužby	taxislužba	k1gFnSc2	taxislužba
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
individuální	individuální	k2eAgFnSc4d1	individuální
poptávkovou	poptávkový	k2eAgFnSc4d1	poptávková
dopravu	doprava	k1gFnSc4	doprava
též	též	k9	též
vozidla	vozidlo	k1gNnPc4	vozidlo
smluvní	smluvní	k2eAgFnSc2d1	smluvní
dopravy	doprava	k1gFnSc2	doprava
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
FIX	fix	k1gInSc1	fix
<g/>
.	.	kIx.	.
</s>
<s>
Autobusové	autobusový	k2eAgNnSc1d1	autobusové
nádraží	nádraží	k1gNnSc1	nádraží
Florenc	Florenc	k1gFnSc1	Florenc
nabízí	nabízet	k5eAaImIp3nS	nabízet
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
letištěm	letiště	k1gNnSc7	letiště
smluvní	smluvní	k2eAgMnPc1d1	smluvní
transfery	transfer	k1gInPc4	transfer
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
informačním	informační	k2eAgNnSc6d1	informační
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
letiště	letiště	k1gNnSc2	letiště
kyvadlová	kyvadlový	k2eAgFnSc1d1	kyvadlová
minibusová	minibusový	k2eAgFnSc1d1	minibusová
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
parkovištím	parkoviště	k1gNnPc3	parkoviště
a	a	k8xC	a
poptávková	poptávkový	k2eAgFnSc1d1	poptávková
doprava	doprava	k1gFnSc1	doprava
k	k	k7c3	k
některým	některý	k3yIgMnPc3	některý
hotelům	hotel	k1gInPc3	hotel
stojícím	stojící	k2eAgInPc3d1	stojící
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
vede	vést	k5eAaImIp3nS	vést
od	od	k7c2	od
sjezdu	sjezd	k1gInSc2	sjezd
z	z	k7c2	z
dálnice	dálnice	k1gFnSc2	dálnice
D7	D7	k1gFnSc2	D7
(	(	kIx(	(
<g/>
Lipské	lipský	k2eAgFnPc1d1	Lipská
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
nedlouhá	dlouhý	k2eNgFnSc1d1	nedlouhá
Aviatická	aviatický	k2eAgFnSc1d1	Aviatická
ulice	ulice	k1gFnSc1	ulice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
oblast	oblast	k1gFnSc1	oblast
Nového	Nového	k2eAgNnSc2d1	Nového
letiště	letiště	k1gNnSc2	letiště
(	(	kIx(	(
<g/>
severního	severní	k2eAgInSc2d1	severní
terminálu	terminál	k1gInSc2	terminál
<g/>
)	)	kIx)	)
prochází	procházet	k5eAaImIp3nS	procházet
velkou	velký	k2eAgFnSc7d1	velká
smyčkou	smyčka	k1gFnSc7	smyčka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
starému	starý	k2eAgNnSc3d1	staré
letišti	letiště	k1gNnSc3	letiště
(	(	kIx(	(
<g/>
jižnímu	jižní	k2eAgInSc3d1	jižní
terminálu	terminál	k1gInSc3	terminál
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
ulice	ulice	k1gFnSc1	ulice
K	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
od	od	k7c2	od
mimoúrovňového	mimoúrovňový	k2eAgNnSc2d1	mimoúrovňové
křížení	křížení	k1gNnSc2	křížení
Evropské	evropský	k2eAgFnSc2d1	Evropská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
Pražského	pražský	k2eAgInSc2d1	pražský
okruhu	okruh	k1gInSc2	okruh
a	a	k8xC	a
dálnice	dálnice	k1gFnSc2	dálnice
D	D	kA	D
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přímé	přímý	k2eAgFnSc6d1	přímá
propojce	propojka	k1gFnSc6	propojka
mezi	mezi	k7c7	mezi
starým	starý	k2eAgNnSc7d1	staré
a	a	k8xC	a
novým	nový	k2eAgNnSc7d1	nové
letištěm	letiště	k1gNnSc7	letiště
je	být	k5eAaImIp3nS	být
veřejný	veřejný	k2eAgInSc1d1	veřejný
provoz	provoz	k1gInSc1	provoz
omezen	omezit	k5eAaPmNgInS	omezit
<g/>
,	,	kIx,	,
jezdí	jezdit	k5eAaImIp3nS	jezdit
tudy	tudy	k6eAd1	tudy
však	však	k9	však
městské	městský	k2eAgFnSc2d1	městská
a	a	k8xC	a
regionální	regionální	k2eAgFnSc2d1	regionální
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgInSc1d1	silniční
provoz	provoz	k1gInSc1	provoz
před	před	k7c7	před
terminály	terminál	k1gInPc7	terminál
je	být	k5eAaImIp3nS	být
přísně	přísně	k6eAd1	přísně
regulován	regulovat	k5eAaImNgInS	regulovat
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
řidiči	řidič	k1gMnPc1	řidič
linkových	linkový	k2eAgInPc2d1	linkový
autobusů	autobus	k1gInPc2	autobus
si	se	k3xPyFc3	se
musejí	muset	k5eAaImIp3nP	muset
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
jízdě	jízda	k1gFnSc6	jízda
čipovou	čipový	k2eAgFnSc7d1	čipová
kartou	karta	k1gFnSc7	karta
otevírat	otevírat	k5eAaImF	otevírat
vjezdovou	vjezdový	k2eAgFnSc4d1	vjezdová
a	a	k8xC	a
výjezdovou	výjezdový	k2eAgFnSc4d1	výjezdová
závoru	závora	k1gFnSc4	závora
<g/>
.	.	kIx.	.
</s>
<s>
Poplatková	poplatkový	k2eAgFnSc1d1	poplatková
politika	politika	k1gFnSc1	politika
letiště	letiště	k1gNnSc2	letiště
motivuje	motivovat	k5eAaBmIp3nS	motivovat
řidiče	řidič	k1gMnPc4	řidič
k	k	k7c3	k
co	co	k9	co
nejrychlejšímu	rychlý	k2eAgNnSc3d3	nejrychlejší
odbavení	odbavení	k1gNnSc3	odbavení
–	–	k?	–
na	na	k7c6	na
expresních	expresní	k2eAgNnPc6d1	expresní
parkovištích	parkoviště	k1gNnPc6	parkoviště
P1	P1	k1gMnPc1	P1
a	a	k8xC	a
P2	P2	k1gMnPc1	P2
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
terminálů	terminál	k1gInPc2	terminál
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
pobyt	pobyt	k1gInSc1	pobyt
vozidla	vozidlo	k1gNnSc2	vozidlo
do	do	k7c2	do
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nejméně	málo	k6eAd3	málo
za	za	k7c4	za
100	[number]	k4	100
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
celodenní	celodenní	k2eAgNnPc4d1	celodenní
parkování	parkování	k1gNnPc4	parkování
je	být	k5eAaImIp3nS	být
určen	určen	k2eAgInSc1d1	určen
parkovací	parkovací	k2eAgInSc1d1	parkovací
dům	dům	k1gInSc1	dům
PCComfort	PCComfort	k1gInSc1	PCComfort
a	a	k8xC	a
venkovní	venkovní	k2eAgNnPc1d1	venkovní
parkoviště	parkoviště	k1gNnPc1	parkoviště
PBEconomy	PBEconom	k1gInPc4	PBEconom
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
vícedenní	vícedenní	k2eAgNnSc4d1	vícedenní
parkování	parkování	k1gNnSc4	parkování
parkovací	parkovací	k2eAgInPc4d1	parkovací
domy	dům	k1gInPc4	dům
PDHoliday	PDHolidaa	k1gFnSc2	PDHolidaa
<g/>
,	,	kIx,	,
PASmart	PASmart	k1gInSc4	PASmart
i	i	k8xC	i
PCComfort	PCComfort	k1gInSc4	PCComfort
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
parkoviště	parkoviště	k1gNnPc1	parkoviště
patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
zdejším	zdejší	k2eAgInPc3d1	zdejší
hotelům	hotel	k1gInPc3	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
je	být	k5eAaImIp3nS	být
též	též	k9	též
půjčovna	půjčovna	k1gFnSc1	půjčovna
automobilů	automobil	k1gInPc2	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
oblast	oblast	k1gFnSc4	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
letiště	letiště	k1gNnSc2	letiště
vedla	vést	k5eAaImAgFnS	vést
významná	významný	k2eAgFnSc1d1	významná
stará	starý	k2eAgFnSc1d1	stará
Slánská	slánský	k2eAgFnSc1d1	slánská
silnice	silnice	k1gFnSc1	silnice
od	od	k7c2	od
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Středokluky	Středokluk	k1gMnPc4	Středokluk
<g/>
,	,	kIx,	,
Slaný	Slaný	k1gInSc1	Slaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
stopě	stopa	k1gFnSc6	stopa
vedou	vést	k5eAaImIp3nP	vést
dnešní	dnešní	k2eAgFnPc1d1	dnešní
ulice	ulice	k1gFnPc1	ulice
Drnovská	Drnovský	k2eAgFnSc1d1	Drnovská
a	a	k8xC	a
starší	starý	k2eAgFnSc1d2	starší
část	část	k1gFnSc1	část
ulice	ulice	k1gFnSc2	ulice
K	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
<g/>
,	,	kIx,	,
za	za	k7c7	za
letištěm	letiště	k1gNnSc7	letiště
pak	pak	k6eAd1	pak
v	v	k7c6	v
Kněževsi	Kněževes	k1gFnSc6	Kněževes
ulice	ulice	k1gFnSc2	ulice
Na	na	k7c6	na
Staré	Staré	k2eAgFnSc6d1	Staré
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
dálkové	dálkový	k2eAgNnSc4d1	dálkové
spojení	spojení	k1gNnSc4	spojení
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dnešní	dnešní	k2eAgFnSc1d1	dnešní
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
letiště	letiště	k1gNnSc4	letiště
obchází	obcházet	k5eAaImIp3nS	obcházet
východní	východní	k2eAgFnSc7d1	východní
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
letiště	letiště	k1gNnSc2	letiště
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Hostivice	Hostivice	k1gFnSc2	Hostivice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
zbytek	zbytek	k1gInSc1	zbytek
zrušeného	zrušený	k2eAgInSc2d1	zrušený
úseku	úsek	k1gInSc2	úsek
bývalé	bývalý	k2eAgFnSc2d1	bývalá
okresní	okresní	k2eAgFnSc2d1	okresní
silnice	silnice	k1gFnSc2	silnice
Sobín	Sobín	k1gMnSc1	Sobín
-	-	kIx~	-
Hostivice	Hostivice	k1gFnSc1	Hostivice
-	-	kIx~	-
Kněževes	Kněževes	k1gFnSc1	Kněževes
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
polní	polní	k2eAgFnSc1d1	polní
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
pokračování	pokračování	k1gNnSc1	pokračování
ulice	ulice	k1gFnSc2	ulice
K	k	k7c3	k
Višňovce	višňovka	k1gFnSc3	višňovka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
121	[number]	k4	121
původně	původně	k6eAd1	původně
vedla	vést	k5eAaImAgFnS	vést
jako	jako	k9	jako
pokračování	pokračování	k1gNnSc4	pokračování
dnešní	dnešní	k2eAgFnSc2d1	dnešní
větve	větev	k1gFnSc2	větev
trati	trať	k1gFnSc2	trať
122	[number]	k4	122
od	od	k7c2	od
Hostivice	Hostivice	k1gFnSc2	Hostivice
ke	k	k7c3	k
Kněževsi	Kněževes	k1gFnSc3	Kněževes
(	(	kIx(	(
<g/>
stanici	stanice	k1gFnSc6	stanice
Středokluky	Středokluk	k1gInPc4	Středokluk
<g/>
)	)	kIx)	)
přímo	přímo	k6eAd1	přímo
přes	přes	k7c4	přes
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
dnešního	dnešní	k2eAgNnSc2d1	dnešní
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
letiště	letiště	k1gNnSc2	letiště
však	však	k9	však
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
přeložena	přeložit	k5eAaPmNgFnS	přeložit
do	do	k7c2	do
trasy	trasa	k1gFnSc2	trasa
kolem	kolem	k7c2	kolem
vesnic	vesnice	k1gFnPc2	vesnice
Hostouň	Hostouň	k1gFnSc4	Hostouň
a	a	k8xC	a
Dobrovíz	Dobrovíz	k1gInSc4	Dobrovíz
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
původní	původní	k2eAgFnSc2d1	původní
větve	větev	k1gFnSc2	větev
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
od	od	k7c2	od
stanice	stanice	k1gFnSc2	stanice
Středokluky	Středokluk	k1gInPc4	Středokluk
však	však	k8xC	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
zachována	zachovat	k5eAaPmNgFnS	zachovat
jako	jako	k8xC	jako
železniční	železniční	k2eAgFnSc1d1	železniční
vlečka	vlečka	k1gFnSc1	vlečka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zásobování	zásobování	k1gNnSc3	zásobování
letiště	letiště	k1gNnSc1	letiště
leteckým	letecký	k2eAgNnSc7d1	letecké
palivem	palivo	k1gNnSc7	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
letiště	letiště	k1gNnSc1	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
–	–	k?	–
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spolu	spolu	k6eAd1	spolu
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
koncepce	koncepce	k1gFnPc1	koncepce
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
prodloužit	prodloužit	k5eAaPmF	prodloužit
linku	linka	k1gFnSc4	linka
metra	metro	k1gNnSc2	metro
A	a	k8xC	a
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
linku	linka	k1gFnSc4	linka
metra	metro	k1gNnSc2	metro
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
modernizovanou	modernizovaný	k2eAgFnSc4d1	modernizovaná
železniční	železniční	k2eAgFnSc4d1	železniční
dráhu	dráha	k1gFnSc4	dráha
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
označovanou	označovaný	k2eAgFnSc7d1	označovaná
za	za	k7c4	za
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
<g/>
,	,	kIx,	,
či	či	k8xC	či
její	její	k3xOp3gFnSc4	její
odbočku	odbočka	k1gFnSc4	odbočka
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
více	hodně	k6eAd2	hodně
variant	varianta	k1gFnPc2	varianta
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
investice	investice	k1gFnPc1	investice
byly	být	k5eAaImAgFnP	být
dlouho	dlouho	k6eAd1	dlouho
oddalovány	oddalován	k2eAgFnPc1d1	oddalována
<g/>
.	.	kIx.	.
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
schválilo	schválit	k5eAaPmAgNnS	schválit
projekt	projekt	k1gInSc4	projekt
železniční	železniční	k2eAgFnPc1d1	železniční
"	"	kIx"	"
<g/>
rychlodráhy	rychlodráha	k1gFnPc1	rychlodráha
<g/>
"	"	kIx"	"
společnosti	společnost	k1gFnPc1	společnost
PRaK	prak	k1gInSc4	prak
usnesením	usnesení	k1gNnSc7	usnesení
č.	č.	k?	č.
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
21	[number]	k4	21
ze	z	k7c2	z
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
zprovoznění	zprovoznění	k1gNnSc1	zprovoznění
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
plánováno	plánovat	k5eAaImNgNnS	plánovat
na	na	k7c6	na
roku	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Prodloužení	prodloužení	k1gNnSc1	prodloužení
metra	metro	k1gNnSc2	metro
A	a	k9	a
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Dejvická	dejvický	k2eAgFnSc1d1	Dejvická
nebo	nebo	k8xC	nebo
metra	metro	k1gNnSc2	metro
B	B	kA	B
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Zličín	Zličína	k1gFnPc2	Zličína
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hodnoceno	hodnocen	k2eAgNnSc1d1	hodnoceno
jako	jako	k8xS	jako
nevýhodné	výhodný	k2eNgNnSc1d1	nevýhodné
a	a	k8xC	a
jako	jako	k9	jako
nehospodárný	hospodárný	k2eNgInSc4d1	nehospodárný
souběh	souběh	k1gInSc4	souběh
dvou	dva	k4xCgInPc2	dva
páteřních	páteřní	k2eAgInPc2d1	páteřní
kolejových	kolejový	k2eAgInPc2d1	kolejový
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Česká	český	k2eAgFnSc1d1	Česká
správa	správa	k1gFnSc1	správa
letišť	letiště	k1gNnPc2	letiště
objednala	objednat	k5eAaPmAgFnS	objednat
vypracování	vypracování	k1gNnSc4	vypracování
projektu	projekt	k1gInSc2	projekt
úseku	úsek	k1gInSc2	úsek
z	z	k7c2	z
centra	centrum	k1gNnSc2	centrum
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodloužení	prodloužení	k1gNnSc1	prodloužení
do	do	k7c2	do
Kladna	Kladno	k1gNnSc2	Kladno
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
dodatečnou	dodatečný	k2eAgFnSc7d1	dodatečná
fází	fáze	k1gFnSc7	fáze
–	–	k?	–
takto	takto	k6eAd1	takto
pojímaly	pojímat	k5eAaImAgFnP	pojímat
priority	priorita	k1gFnPc1	priorita
i	i	k8xC	i
České	český	k2eAgFnPc1d1	Česká
dráhy	dráha	k1gFnPc1	dráha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
hovořilo	hovořit	k5eAaImAgNnS	hovořit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
relaci	relace	k1gFnSc6	relace
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
a	a	k8xC	a
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
mělo	mít	k5eAaImAgNnS	mít
platit	platit	k5eAaImF	platit
speciální	speciální	k2eAgNnSc1d1	speciální
vysoké	vysoký	k2eAgNnSc1d1	vysoké
jízdné	jízdné	k1gNnSc1	jízdné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
a	a	k8xC	a
vyvíjelo	vyvíjet	k5eAaImAgNnS	vyvíjet
aktivitu	aktivita	k1gFnSc4	aktivita
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
"	"	kIx"	"
<g/>
Chceme	chtít	k5eAaImIp1nP	chtít
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
nechceme	chtít	k5eNaImIp1nP	chtít
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přípravu	příprava	k1gFnSc4	příprava
zablokovat	zablokovat	k5eAaPmF	zablokovat
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
vydaly	vydat	k5eAaPmAgFnP	vydat
Středočeský	středočeský	k2eAgInSc4d1	středočeský
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
memorandum	memorandum	k1gNnSc1	memorandum
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
modernizaci	modernizace	k1gFnSc4	modernizace
železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
označily	označit	k5eAaPmAgFnP	označit
za	za	k7c4	za
prioritu	priorita	k1gFnSc4	priorita
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
projekt	projekt	k1gInSc1	projekt
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
doporučilo	doporučit	k5eAaPmAgNnS	doporučit
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
vládě	vláda	k1gFnSc3	vláda
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
programu	program	k1gInSc2	program
PPP	PPP	kA	PPP
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
projednávala	projednávat	k5eAaImAgFnS	projednávat
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
negativní	negativní	k2eAgNnSc1d1	negativní
stanovisko	stanovisko	k1gNnSc1	stanovisko
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
preferovala	preferovat	k5eAaImAgFnS	preferovat
metro	metro	k1gNnSc4	metro
před	před	k7c7	před
modernizací	modernizace	k1gFnSc7	modernizace
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
radní	radní	k1gMnSc1	radní
Radovan	Radovan	k1gMnSc1	Radovan
Šteiner	Šteiner	k1gMnSc1	Šteiner
i	i	k8xC	i
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
Jan	Jan	k1gMnSc1	Jan
Bürgermeister	Bürgermeister	k1gMnSc1	Bürgermeister
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
výstavba	výstavba	k1gFnSc1	výstavba
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
prodloužením	prodloužení	k1gNnSc7	prodloužení
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
Šteiner	Šteiner	k1gInSc1	Šteiner
však	však	k9	však
očekával	očekávat	k5eAaImAgInS	očekávat
alespoň	alespoň	k9	alespoň
50	[number]	k4	50
<g/>
%	%	kIx~	%
finanční	finanční	k2eAgInSc1d1	finanční
podíl	podíl	k1gInSc1	podíl
státu	stát	k1gInSc2	stát
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
nového	nový	k2eAgInSc2d1	nový
úseku	úsek	k1gInSc2	úsek
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
vzala	vzít	k5eAaPmAgFnS	vzít
návrh	návrh	k1gInSc4	návrh
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
na	na	k7c4	na
vědomí	vědomí	k1gNnSc4	vědomí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
projekt	projekt	k1gInSc4	projekt
nepodpořila	podpořit	k5eNaPmAgFnS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2005	[number]	k4	2005
město	město	k1gNnSc1	město
Praha	Praha	k1gFnSc1	Praha
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
prodloužena	prodloužen	k2eAgFnSc1d1	prodloužena
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
A	A	kA	A
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
vybudováním	vybudování	k1gNnSc7	vybudování
dopravního	dopravní	k2eAgInSc2d1	dopravní
terminálu	terminál	k1gInSc2	terminál
na	na	k7c6	na
Dlouhé	Dlouhé	k2eAgFnSc6d1	Dlouhé
míli	míle	k1gFnSc6	míle
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
možné	možný	k2eAgFnPc4d1	možná
přestup	přestup	k1gInSc4	přestup
z	z	k7c2	z
modernizované	modernizovaný	k2eAgFnSc2d1	modernizovaná
kladenské	kladenský	k2eAgFnSc2d1	kladenská
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
odkladům	odklad	k1gInPc3	odklad
výstavby	výstavba	k1gFnSc2	výstavba
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
trasy	trasa	k1gFnSc2	trasa
A	a	k8xC	a
i	i	k9	i
železniční	železniční	k2eAgFnPc4d1	železniční
tratě	trať	k1gFnPc4	trať
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
autobusový	autobusový	k2eAgInSc4d1	autobusový
terminál	terminál	k1gInSc4	terminál
a	a	k8xC	a
parkoviště	parkoviště	k1gNnSc4	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
budou	být	k5eAaImBp3nP	být
místo	místo	k7c2	místo
Dlouhé	Dlouhá	k1gFnSc2	Dlouhá
míle	míle	k1gFnSc2	míle
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
Nádraží	nádraží	k1gNnSc2	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
konceptu	koncept	k1gInSc6	koncept
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
zveřejněném	zveřejněný	k2eAgInSc6d1	zveřejněný
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
obě	dva	k4xCgFnPc1	dva
uvažované	uvažovaný	k2eAgFnPc1d1	uvažovaná
větve	větev	k1gFnPc1	větev
linky	linka	k1gFnSc2	linka
A	a	k9	a
Motol	Motol	k1gInSc1	Motol
–	–	k?	–
Letiště	letiště	k1gNnSc2	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
i	i	k8xC	i
Motol	Motol	k1gInSc1	Motol
–	–	k?	–
Zličín	Zličín	k1gInSc1	Zličín
vedeny	veden	k2eAgInPc1d1	veden
jako	jako	k8xS	jako
územní	územní	k2eAgFnSc1d1	územní
rezerva	rezerva	k1gFnSc1	rezerva
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
linka	linka	k1gFnSc1	linka
metra	metro	k1gNnSc2	metro
A	a	k8xC	a
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Nemocnice	nemocnice	k1gFnSc2	nemocnice
Motol	Motol	k1gInSc1	Motol
a	a	k8xC	a
městské	městský	k2eAgFnSc2d1	městská
i	i	k8xC	i
regionální	regionální	k2eAgFnSc2d1	regionální
autobusové	autobusový	k2eAgFnSc2d1	autobusová
linky	linka	k1gFnSc2	linka
od	od	k7c2	od
letiště	letiště	k1gNnSc2	letiště
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
terminálu	terminál	k1gInSc6	terminál
u	u	k7c2	u
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
Nádraží	nádraží	k1gNnSc1	nádraží
Veleslavín	Veleslavín	k1gInSc1	Veleslavín
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
modernizaci	modernizace	k1gFnSc6	modernizace
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
sváděn	sváděn	k2eAgInSc1d1	sváděn
politický	politický	k2eAgInSc1d1	politický
boj	boj	k1gInSc1	boj
zejména	zejména	k9	zejména
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
částí	část	k1gFnSc7	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
původně	původně	k6eAd1	původně
preferovala	preferovat	k5eAaImAgFnS	preferovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
bylo	být	k5eAaImAgNnS	být
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
této	tento	k3xDgFnSc2	tento
varianty	varianta	k1gFnSc2	varianta
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
z	z	k7c2	z
urbanistických	urbanistický	k2eAgInPc2d1	urbanistický
a	a	k8xC	a
hygienických	hygienický	k2eAgInPc2d1	hygienický
důvodů	důvod	k1gInPc2	důvod
byla	být	k5eAaImAgFnS	být
co	co	k9	co
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
část	část	k1gFnSc1	část
modernizované	modernizovaný	k2eAgFnSc2d1	modernizovaná
trati	trať	k1gFnSc2	trať
<g />
.	.	kIx.	.
</s>
<s>
přes	přes	k7c4	přes
obydlené	obydlený	k2eAgNnSc4d1	obydlené
území	území	k1gNnSc4	území
vedena	vést	k5eAaImNgFnS	vést
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
investoři	investor	k1gMnPc1	investor
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
prosazovat	prosazovat	k5eAaImF	prosazovat
levnější	levný	k2eAgFnPc1d2	levnější
varianty	varianta	k1gFnPc1	varianta
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
zastupitelstvo	zastupitelstvo	k1gNnSc4	zastupitelstvo
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
schválilo	schválit	k5eAaPmAgNnS	schválit
do	do	k7c2	do
konceptu	koncept	k1gInSc2	koncept
změny	změna	k1gFnSc2	změna
územního	územní	k2eAgInSc2d1	územní
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
téměř	téměř	k6eAd1	téměř
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
železniční	železniční	k2eAgFnSc7d1	železniční
rychlodráhou	rychlodráha	k1gFnSc7	rychlodráha
vedla	vést	k5eAaImAgFnS	vést
i	i	k9	i
trasa	trasa	k1gFnSc1	trasa
metra	metro	k1gNnSc2	metro
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
dokončení	dokončení	k1gNnSc2	dokončení
asi	asi	k9	asi
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
prodloužení	prodloužení	k1gNnSc3	prodloužení
metra	metro	k1gNnSc2	metro
byla	být	k5eAaImAgFnS	být
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tento	tento	k3xDgInSc4	tento
souběh	souběh	k1gInSc4	souběh
označila	označit	k5eAaPmAgFnS	označit
za	za	k7c4	za
plýtvání	plýtvání	k1gNnSc4	plýtvání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2007	[number]	k4	2007
Dopravní	dopravní	k2eAgFnSc1d1	dopravní
fakulta	fakulta	k1gFnSc1	fakulta
ČVUT	ČVUT	kA	ČVUT
dokončila	dokončit	k5eAaPmAgFnS	dokončit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zadání	zadání	k1gNnSc2	zadání
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
studii	studie	k1gFnSc4	studie
"	"	kIx"	"
<g/>
Posouzení	posouzení	k1gNnSc4	posouzení
variant	varianta	k1gFnPc2	varianta
železničního	železniční	k2eAgNnSc2d1	železniční
spojení	spojení	k1gNnSc2	spojení
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Letiště	letiště	k1gNnSc1	letiště
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2	Praha-Ruzyně
–	–	k?	–
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porovnala	porovnat	k5eAaPmAgFnS	porovnat
čtyři	čtyři	k4xCgFnPc4	čtyři
varianty	varianta	k1gFnPc4	varianta
řešení	řešení	k1gNnPc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
oznámila	oznámit	k5eAaPmAgFnS	oznámit
změnu	změna	k1gFnSc4	změna
stanoviska	stanovisko	k1gNnSc2	stanovisko
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
železniční	železniční	k2eAgFnSc4d1	železniční
rychlodráhu	rychlodráha	k1gFnSc4	rychlodráha
podporovat	podporovat	k5eAaImF	podporovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
studii	studie	k1gFnSc4	studie
provedenou	provedený	k2eAgFnSc4d1	provedená
ČVUT	ČVUT	kA	ČVUT
a	a	k8xC	a
na	na	k7c4	na
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
prodlouženo	prodloužit	k5eAaPmNgNnS	prodloužit
i	i	k9	i
metro	metro	k1gNnSc1	metro
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
úseky	úsek	k1gInPc1	úsek
budou	být	k5eAaImBp3nP	být
vedeny	vést	k5eAaImNgInP	vést
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
že	že	k8xS	že
trať	trať	k1gFnSc1	trať
bude	být	k5eAaImBp3nS	být
sloužit	sloužit	k5eAaImF	sloužit
jen	jen	k9	jen
osobní	osobní	k2eAgFnSc4d1	osobní
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
zelených	zelená	k1gFnPc2	zelená
však	však	k9	však
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
souběhu	souběh	k1gInSc3	souběh
a	a	k8xC	a
preferovala	preferovat	k5eAaImAgFnS	preferovat
před	před	k7c7	před
metrem	metr	k1gInSc7	metr
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
železnici	železnice	k1gFnSc4	železnice
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
vydali	vydat	k5eAaPmAgMnP	vydat
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
středočeský	středočeský	k2eAgMnSc1d1	středočeský
hejtman	hejtman	k1gMnSc1	hejtman
<g/>
,	,	kIx,	,
pražský	pražský	k2eAgMnSc1d1	pražský
primátor	primátor	k1gMnSc1	primátor
<g/>
,	,	kIx,	,
starostové	starosta	k1gMnPc1	starosta
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
(	(	kIx(	(
<g/>
starosta	starosta	k1gMnSc1	starosta
Prahy	Praha	k1gFnSc2	Praha
6	[number]	k4	6
podmíněně	podmíněně	k6eAd1	podmíněně
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
náměstek	náměstek	k1gMnSc1	náměstek
primátora	primátor	k1gMnSc2	primátor
města	město	k1gNnSc2	město
Kladna	Kladno	k1gNnSc2	Kladno
společné	společný	k2eAgNnSc4d1	společné
memorandum	memorandum	k1gNnSc4	memorandum
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Podpořili	podpořit	k5eAaPmAgMnP	podpořit
trať	trať	k1gFnSc4	trať
dvoukolejnou	dvoukolejný	k2eAgFnSc4d1	dvoukolejná
<g/>
,	,	kIx,	,
elektrizovanou	elektrizovaný	k2eAgFnSc4d1	elektrizovaná
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stopě	stopa	k1gFnSc6	stopa
Buštěhradské	buštěhradský	k2eAgFnSc2d1	Buštěhradská
dráhy	dráha	k1gFnSc2	dráha
s	s	k7c7	s
odbočkou	odbočka	k1gFnSc7	odbočka
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
stanicí	stanice	k1gFnSc7	stanice
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Realizována	realizován	k2eAgFnSc1d1	realizována
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
plně	plně	k6eAd1	plně
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
systému	systém	k1gInSc2	systém
Pražské	pražský	k2eAgFnSc2d1	Pražská
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
financování	financování	k1gNnSc2	financování
výstavby	výstavba	k1gFnSc2	výstavba
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
deklaraci	deklarace	k1gFnSc6	deklarace
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Modernizace	modernizace	k1gFnPc1	modernizace
trati	trať	k1gFnSc2	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Kladno	Kladno	k1gNnSc1	Kladno
s	s	k7c7	s
připojením	připojení	k1gNnSc7	připojení
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
objednala	objednat	k5eAaPmAgFnS	objednat
státní	státní	k2eAgFnSc1d1	státní
organizace	organizace	k1gFnSc1	organizace
Správa	správa	k1gFnSc1	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
přípravná	přípravný	k2eAgFnSc1d1	přípravná
dokumentace	dokumentace	k1gFnSc1	dokumentace
předložena	předložen	k2eAgFnSc1d1	předložena
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
projednání	projednání	k1gNnSc3	projednání
<g/>
,	,	kIx,	,
se	s	k7c7	s
zapracovanými	zapracovaný	k2eAgFnPc7d1	zapracovaná
připomínkami	připomínka	k1gFnPc7	připomínka
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
do	do	k7c2	do
konce	konec	k1gInSc2	konec
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2009	[number]	k4	2009
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
termín	termín	k1gInSc1	termín
2013	[number]	k4	2013
se	se	k3xPyFc4	se
nepodaří	podařit	k5eNaPmIp3nS	podařit
dodržet	dodržet	k5eAaPmF	dodržet
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
nevyřešenému	vyřešený	k2eNgNnSc3d1	nevyřešené
financování	financování	k1gNnSc3	financování
a	a	k8xC	a
územnímu	územní	k2eAgInSc3d1	územní
plánu	plán	k1gInSc3	plán
se	se	k3xPyFc4	se
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
odloží	odložit	k5eAaPmIp3nP	odložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2009	[number]	k4	2009
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
projekt	projekt	k1gInSc1	projekt
pozastavuje	pozastavovat	k5eAaImIp3nS	pozastavovat
kvůli	kvůli	k7c3	kvůli
napjaté	napjatý	k2eAgFnSc3d1	napjatá
situaci	situace	k1gFnSc3	situace
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
úsporným	úsporný	k2eAgNnSc7d1	úsporné
opatřením	opatření	k1gNnSc7	opatření
vlády	vláda	k1gFnSc2	vláda
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
Blesk	blesk	k1gInSc4	blesk
však	však	k9	však
přišel	přijít	k5eAaPmAgMnS	přijít
se	s	k7c7	s
spekulací	spekulace	k1gFnSc7	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavbu	stavba	k1gFnSc4	stavba
blokuje	blokovat	k5eAaImIp3nS	blokovat
sám	sám	k3xTgMnSc1	sám
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Gustáv	Gustáva	k1gFnPc2	Gustáva
Slamečka	Slamečko	k1gNnSc2	Slamečko
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydlí	bydlet	k5eAaImIp3nS	bydlet
<g/>
.	.	kIx.	.
</s>
<s>
Středočeský	středočeský	k2eAgMnSc1d1	středočeský
hejtman	hejtman	k1gMnSc1	hejtman
David	David	k1gMnSc1	David
Rath	Rath	k1gMnSc1	Rath
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zlevnit	zlevnit	k5eAaPmF	zlevnit
projekt	projekt	k1gInSc4	projekt
o	o	k7c4	o
deset	deset	k4xCc4	deset
miliard	miliarda	k4xCgFnPc2	miliarda
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
území	území	k1gNnSc6	území
Prahy	Praha	k1gFnSc2	Praha
6	[number]	k4	6
bude	být	k5eAaImBp3nS	být
upuštěno	upuštěn	k2eAgNnSc1d1	upuštěno
od	od	k7c2	od
hloubené	hloubený	k2eAgFnSc2d1	hloubená
varianty	varianta	k1gFnSc2	varianta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
odporovalo	odporovat	k5eAaImAgNnS	odporovat
požadavku	požadavek	k1gInSc2	požadavek
Prahy	Praha	k1gFnSc2	Praha
6	[number]	k4	6
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
média	médium	k1gNnPc1	médium
referovala	referovat	k5eAaBmAgNnP	referovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ministr	ministr	k1gMnSc1	ministr
dopravy	doprava	k1gFnSc2	doprava
Pavel	Pavel	k1gMnSc1	Pavel
Dobeš	Dobeš	k1gMnSc1	Dobeš
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
středočeský	středočeský	k2eAgMnSc1d1	středočeský
hejtman	hejtman	k1gMnSc1	hejtman
David	David	k1gMnSc1	David
Rath	Rath	k1gMnSc1	Rath
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
bude	být	k5eAaImBp3nS	být
postaven	postaven	k2eAgInSc1d1	postaven
úsek	úsek	k1gInSc1	úsek
z	z	k7c2	z
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
k	k	k7c3	k
letišti	letiště	k1gNnSc3	letiště
do	do	k7c2	do
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zprovozněn	zprovoznit	k5eAaPmNgInS	zprovoznit
současně	současně	k6eAd1	současně
s	s	k7c7	s
prodloužením	prodloužení	k1gNnSc7	prodloužení
tratě	trať	k1gFnSc2	trať
A	a	k8xC	a
metra	metro	k1gNnSc2	metro
z	z	k7c2	z
Dejvic	Dejvice	k1gFnPc2	Dejvice
přes	přes	k7c4	přes
Veleslavín	Veleslavín	k1gInSc4	Veleslavín
do	do	k7c2	do
Motola	Motola	k1gFnSc1	Motola
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dokončeno	dokončit	k5eAaPmNgNnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Správy	správa	k1gFnSc2	správa
železniční	železniční	k2eAgFnSc2d1	železniční
dopravní	dopravní	k2eAgFnSc2d1	dopravní
cesty	cesta	k1gFnSc2	cesta
Pavel	Pavel	k1gMnSc1	Pavel
Halla	Halla	k1gMnSc1	Halla
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
železniční	železniční	k2eAgInSc1d1	železniční
úsek	úsek	k1gInSc1	úsek
alespoň	alespoň	k9	alespoň
rozestavěn	rozestavěn	k2eAgMnSc1d1	rozestavěn
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
úseku	úsek	k1gInSc2	úsek
z	z	k7c2	z
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
na	na	k7c4	na
Masarykovo	Masarykův	k2eAgNnSc4d1	Masarykovo
nádraží	nádraží	k1gNnSc4	nádraží
byla	být	k5eAaImAgFnS	být
odložena	odložen	k2eAgFnSc1d1	odložena
na	na	k7c4	na
neurčito	neurčito	k1gNnSc4	neurčito
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
žádného	žádný	k3yNgInSc2	žádný
úseku	úsek	k1gInSc2	úsek
však	však	k9	však
zahájena	zahájen	k2eAgFnSc1d1	zahájena
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
uvažovanou	uvažovaný	k2eAgFnSc7d1	uvažovaná
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
napojení	napojení	k1gNnSc1	napojení
letiště	letiště	k1gNnSc2	letiště
na	na	k7c4	na
systém	systém	k1gInSc4	systém
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1	vysokorychlostní
tratí	trať	k1gFnPc2	trať
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nP	by
přes	přes	k7c4	přes
letiště	letiště	k1gNnSc4	letiště
procházela	procházet	k5eAaImAgFnS	procházet
trať	trať	k1gFnSc1	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
–	–	k?	–
Norimberk	Norimberk	k1gInSc1	Norimberk
<g/>
)	)	kIx)	)
a	a	k8xC	a
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
(	(	kIx(	(
<g/>
–	–	k?	–
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
rozpracovanosti	rozpracovanost	k1gFnSc2	rozpracovanost
těchto	tento	k3xDgFnPc2	tento
tratí	trať	k1gFnPc2	trať
ČR	ČR	kA	ČR
nelze	lze	k6eNd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
realizaci	realizace	k1gFnSc4	realizace
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
oslav	oslava	k1gFnPc2	oslava
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
40	[number]	k4	40
<g/>
.	.	kIx.	.
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
Ronalda	Ronald	k1gMnSc2	Ronald
Reagana	Reagan	k1gMnSc2	Reagan
<g/>
,	,	kIx,	,
napsala	napsat	k5eAaBmAgFnS	napsat
skupinka	skupinka	k1gFnSc1	skupinka
aktivistů	aktivista	k1gMnPc2	aktivista
ministrovi	ministr	k1gMnSc3	ministr
financí	finance	k1gFnSc7	finance
Miroslavu	Miroslava	k1gFnSc4	Miroslava
Kalouskovi	Kalousek	k1gMnSc3	Kalousek
otevřený	otevřený	k2eAgInSc4d1	otevřený
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
žádali	žádat	k5eAaImAgMnP	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
letiště	letiště	k1gNnSc1	letiště
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
po	po	k7c6	po
tomto	tento	k3xDgMnSc6	tento
americkém	americký	k2eAgMnSc6d1	americký
prezidentovi	prezident	k1gMnSc6	prezident
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
pád	pád	k1gInSc4	pád
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
a	a	k8xC	a
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
návrhem	návrh	k1gInSc7	návrh
stála	stát	k5eAaImAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
Opona	opona	k1gFnSc1	opona
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úmrtí	úmrtí	k1gNnSc6	úmrtí
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
Fero	Fero	k1gMnSc1	Fero
Fenič	Fenič	k1gInSc4	Fenič
prohlášení	prohlášení	k1gNnSc2	prohlášení
s	s	k7c7	s
výzvou	výzva	k1gFnSc7	výzva
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
letiště	letiště	k1gNnSc4	letiště
Praha-Ruzyně	Praha-Ruzyně	k1gFnSc2	Praha-Ruzyně
v	v	k7c6	v
Praze-Ruzyni	Praze-Ruzyně	k1gFnSc6	Praze-Ruzyně
na	na	k7c4	na
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
podle	podle	k7c2	podle
Feniče	Fenič	k1gInSc2	Fenič
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíše	spíše	k9	spíše
známou	známý	k2eAgFnSc4d1	známá
pražskou	pražský	k2eAgFnSc4d1	Pražská
věznici	věznice	k1gFnSc4	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Feniče	Fenič	k1gMnSc2	Fenič
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c6	o
přejmenování	přejmenování	k1gNnSc6	přejmenování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
pojmenování	pojmenování	k1gNnSc4	pojmenování
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
názvu	název	k1gInSc6	název
"	"	kIx"	"
<g/>
Letiště	letiště	k1gNnSc1	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
není	být	k5eNaImIp3nS	být
žádné	žádný	k3yNgNnSc1	žádný
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vzory	vzor	k1gInPc4	vzor
uvedl	uvést	k5eAaPmAgMnS	uvést
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgNnPc2d1	jiné
i	i	k8xC	i
letiště	letiště	k1gNnSc4	letiště
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaulla	Gaull	k1gMnSc2	Gaull
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Reklamní	reklamní	k2eAgFnSc1d1	reklamní
a	a	k8xC	a
marketingová	marketingový	k2eAgFnSc1d1	marketingová
agentura	agentura	k1gFnSc1	agentura
Graffitti	Graffitť	k1gFnSc2	Graffitť
Networks	Networksa	k1gFnPc2	Networksa
pro	pro	k7c4	pro
prezentaci	prezentace	k1gFnSc4	prezentace
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
návrhu	návrh	k1gInSc2	návrh
zřídila	zřídit	k5eAaPmAgFnS	zřídit
webovou	webový	k2eAgFnSc4d1	webová
stránku	stránka	k1gFnSc4	stránka
na	na	k7c6	na
doméně	doména	k1gFnSc6	doména
www.letiste-vaclava-havla.cz	www.letisteaclavaavla.cza	k1gFnPc2	www.letiste-vaclava-havla.cza
a	a	k8xC	a
www.letistevaclavahavla.cz	www.letistevaclavahavla.cza	k1gFnPc2	www.letistevaclavahavla.cza
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výzvě	výzva	k1gFnSc6	výzva
opakovaně	opakovaně	k6eAd1	opakovaně
referovala	referovat	k5eAaBmAgNnP	referovat
významná	významný	k2eAgNnPc1d1	významné
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
podepsali	podepsat	k5eAaPmAgMnP	podepsat
ji	on	k3xPp3gFnSc4	on
například	například	k6eAd1	například
Jiří	Jiří	k1gMnSc1	Jiří
Bartoška	Bartoška	k1gMnSc1	Bartoška
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Brabec	Brabec	k1gMnSc1	Brabec
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
Brdečková	Brdečková	k1gFnSc1	Brdečková
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Bursík	Bursík	k1gMnSc1	Bursík
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Čáslavská	Čáslavská	k1gFnSc1	Čáslavská
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Černý	Černý	k1gMnSc1	Černý
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
za	za	k7c4	za
10	[number]	k4	10
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
internetovou	internetový	k2eAgFnSc7d1	internetová
peticí	petice	k1gFnSc7	petice
objevilo	objevit	k5eAaPmAgNnS	objevit
asi	asi	k9	asi
70	[number]	k4	70
tisíc	tisíc	k4xCgInPc2	tisíc
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
podala	podat	k5eAaPmAgFnS	podat
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
žádost	žádost	k1gFnSc1	žádost
Úřadu	úřad	k1gInSc2	úřad
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
o	o	k7c6	o
registraci	registrace	k1gFnSc6	registrace
slovních	slovní	k2eAgFnPc2d1	slovní
ochranných	ochranný	k2eAgFnPc2d1	ochranná
známek	známka	k1gFnPc2	známka
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
"	"	kIx"	"
<g/>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
Letiště	letiště	k1gNnSc1	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Vaclav	Vaclav	k1gMnSc1	Vaclav
Havel	Havel	k1gMnSc1	Havel
Prague	Pragu	k1gFnSc2	Pragu
International	International	k1gMnSc1	International
Airport	Airport	k1gInSc1	Airport
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlová	Havlová	k1gFnSc1	Havlová
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
prezidenta	prezident	k1gMnSc2	prezident
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
dala	dát	k5eAaPmAgFnS	dát
vládě	vláda	k1gFnSc3	vláda
ČR	ČR	kA	ČR
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
souhlas	souhlas	k1gInSc1	souhlas
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
letiště	letiště	k1gNnSc1	letiště
může	moct	k5eAaImIp3nS	moct
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
po	po	k7c6	po
jejím	její	k3xOp3gMnSc6	její
zesnulém	zesnulý	k1gMnSc6	zesnulý
manželovi	manžel	k1gMnSc6	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
tak	tak	k6eAd1	tak
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
o	o	k7c6	o
přejmenování	přejmenování	k1gNnSc6	přejmenování
letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
se	se	k3xPyFc4	se
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
objevila	objevit	k5eAaPmAgFnS	objevit
též	též	k9	též
anonymní	anonymní	k2eAgFnPc4d1	anonymní
petice	petice	k1gFnPc4	petice
proti	proti	k7c3	proti
přejmenování	přejmenování	k1gNnSc3	přejmenování
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentní	parlamentní	k2eAgInPc1d1	parlamentní
listy	list	k1gInPc1	list
její	její	k3xOp3gMnPc4	její
autorství	autorství	k1gNnSc1	autorství
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
pondělí	pondělí	k1gNnSc6	pondělí
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
přiřkly	přiřknout	k5eAaPmAgFnP	přiřknout
miliardáři	miliardář	k1gMnSc3	miliardář
Petru	Petr	k1gMnSc3	Petr
Kellnerovi	Kellner	k1gMnSc3	Kellner
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
společnost	společnost	k1gFnSc1	společnost
však	však	k9	však
vzápětí	vzápětí	k6eAd1	vzápětí
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
akcionář	akcionář	k1gMnSc1	akcionář
PPF	PPF	kA	PPF
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
peticí	petice	k1gFnSc7	petice
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
a	a	k8xC	a
text	text	k1gInSc1	text
prohlášení	prohlášení	k1gNnSc2	prohlášení
žádným	žádný	k3yNgInSc7	žádný
způsobem	způsob	k1gInSc7	způsob
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejde	jít	k5eNaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
shodu	shoda	k1gFnSc4	shoda
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
podvrh	podvrh	k1gInSc4	podvrh
či	či	k8xC	či
provokaci	provokace	k1gFnSc4	provokace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
hrubým	hrubý	k2eAgInSc7d1	hrubý
způsobem	způsob	k1gInSc7	způsob
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
práv	právo	k1gNnPc2	právo
akcionáře	akcionář	k1gMnSc2	akcionář
PPF	PPF	kA	PPF
Petra	Petr	k1gMnSc2	Petr
Kellnera	Kellner	k1gMnSc2	Kellner
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
za	za	k7c7	za
4	[number]	k4	4
dny	den	k1gInPc7	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
objevilo	objevit	k5eAaPmAgNnS	objevit
asi	asi	k9	asi
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
článku	článek	k1gInSc2	článek
Britských	britský	k2eAgInPc2d1	britský
listů	list	k1gInPc2	list
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
autoritativního	autoritativní	k2eAgInSc2d1	autoritativní
režimu	režim	k1gInSc2	režim
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc4	její
autor	autor	k1gMnSc1	autor
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
odkázal	odkázat	k5eAaPmAgMnS	odkázat
na	na	k7c4	na
petici	petice	k1gFnSc4	petice
<g/>
,	,	kIx,	,
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
s	s	k7c7	s
něco	něco	k6eAd1	něco
přes	přes	k7c4	přes
19	[number]	k4	19
tisíci	tisíc	k4xCgInPc7	tisíc
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
stránky	stránka	k1gFnSc2	stránka
petice	petice	k1gFnSc2	petice
uváděly	uvádět	k5eAaImAgInP	uvádět
k	k	k7c3	k
autorovi	autor	k1gMnSc3	autor
<g/>
:	:	kIx,	:
Autor	autor	k1gMnSc1	autor
petice	petice	k1gFnSc2	petice
<g/>
:	:	kIx,	:
narozen	narozen	k2eAgMnSc1d1	narozen
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgInS	být
členem	člen	k1gInSc7	člen
pionýra	pionýr	k1gMnSc2	pionýr
SSM	SSM	kA	SSM
<g/>
,	,	kIx,	,
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
jiné	jiný	k2eAgFnPc1d1	jiná
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
v	v	k7c6	v
disentu	disent	k1gInSc6	disent
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odpůrcem	odpůrce	k1gMnSc7	odpůrce
všech	všecek	k3xTgInPc2	všecek
totalitních	totalitní	k2eAgMnPc2d1	totalitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
pravicových	pravicový	k2eAgMnPc2d1	pravicový
i	i	k8xC	i
levicových	levicový	k2eAgMnPc2d1	levicový
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svobodné	svobodný	k2eAgNnSc4d1	svobodné
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
malíř	malíř	k1gMnSc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
Petr	Petr	k1gMnSc1	Petr
Kellner	Kellner	k1gMnSc1	Kellner
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
Otevřený	otevřený	k2eAgInSc4d1	otevřený
dopis	dopis	k1gInSc4	dopis
vládě	vláda	k1gFnSc3	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ze	z	k7c2	z
30.1	[number]	k4	30.1
<g/>
.2012	.2012	k4	.2012
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
přimlouvá	přimlouvat	k5eAaImIp3nS	přimlouvat
"	"	kIx"	"
<g/>
při	při	k7c6	při
nejmenším	malý	k2eAgMnSc6d3	nejmenší
o	o	k7c4	o
určitou	určitý	k2eAgFnSc4d1	určitá
zdrženlivost	zdrženlivost	k1gFnSc4	zdrženlivost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
k	k	k7c3	k
památce	památka	k1gFnSc3	památka
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
exprezidenta	exprezident	k1gMnSc2	exprezident
pana	pan	k1gMnSc2	pan
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sám	sám	k3xTgMnSc1	sám
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
tvrdým	tvrdý	k2eAgInSc7d1	tvrdý
<g />
.	.	kIx.	.
</s>
<s>
kritikem	kritik	k1gMnSc7	kritik
a	a	k8xC	a
odpůrcem	odpůrce	k1gMnSc7	odpůrce
vytváření	vytváření	k1gNnSc2	vytváření
jakýchkoliv	jakýkoliv	k3yIgInPc2	jakýkoliv
kultů	kult	k1gInPc2	kult
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
"	"	kIx"	"
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nP	prosit
"	"	kIx"	"
<g/>
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvážila	zvážit	k5eAaPmAgFnS	zvážit
a	a	k8xC	a
přehodnotila	přehodnotit	k5eAaPmAgFnS	přehodnotit
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
velice	velice	k6eAd1	velice
špatný	špatný	k2eAgInSc1d1	špatný
signál	signál	k1gInSc1	signál
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
občanské	občanský	k2eAgFnSc3d1	občanská
společnosti	společnost	k1gFnSc3	společnost
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
důsledkem	důsledek	k1gInSc7	důsledek
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
precedens	precedens	k1gNnSc4	precedens
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
principy	princip	k1gInPc4	princip
rozhodování	rozhodování	k1gNnSc2	rozhodování
daleko	daleko	k6eAd1	daleko
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mocenské	mocenský	k2eAgInPc1d1	mocenský
aparáty	aparát	k1gInPc1	aparát
bývalého	bývalý	k2eAgInSc2d1	bývalý
režimu	režim	k1gInSc2	režim
určovaly	určovat	k5eAaImAgInP	určovat
autoritativním	autoritativní	k2eAgInSc7d1	autoritativní
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
obdivu	obdiv	k1gInSc2	obdiv
občanů	občan	k1gMnPc2	občan
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
pod	pod	k7c7	pod
peticí	petice	k1gFnSc7	petice
proti	proti	k7c3	proti
přejmenování	přejmenování	k1gNnSc3	přejmenování
stály	stát	k5eAaImAgInP	stát
podpisy	podpis	k1gInPc1	podpis
více	hodně	k6eAd2	hodně
než	než	k8xS	než
22	[number]	k4	22
tisíc	tisíc	k4xCgInSc4	tisíc
podporovatelů	podporovatel	k1gMnPc2	podporovatel
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
peticí	petice	k1gFnSc7	petice
Fera	Fero	k1gMnSc2	Fero
Feniče	Fenič	k1gMnSc2	Fenič
za	za	k7c4	za
přejmenování	přejmenování	k1gNnSc4	přejmenování
přes	přes	k7c4	přes
81	[number]	k4	81
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pochyby	pochyba	k1gFnPc4	pochyba
k	k	k7c3	k
přejmenování	přejmenování	k1gNnSc3	přejmenování
letiště	letiště	k1gNnSc2	letiště
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Havlův	Havlův	k2eAgMnSc1d1	Havlův
tajemník	tajemník	k1gMnSc1	tajemník
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hanzel	Hanzel	k1gMnSc1	Hanzel
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgNnSc2	jenž
letiště	letiště	k1gNnSc2	letiště
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
komerční	komerční	k2eAgInSc1d1	komerční
subjekt	subjekt	k1gInSc1	subjekt
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
Havel	Havel	k1gMnSc1	Havel
létání	létání	k1gNnSc2	létání
bytostně	bytostně	k6eAd1	bytostně
nesnášel	snášet	k5eNaImAgInS	snášet
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Havlovi	Havel	k1gMnSc3	Havel
pojmenována	pojmenován	k2eAgMnSc4d1	pojmenován
spíše	spíše	k9	spíše
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
nést	nést	k5eAaImF	nést
Havlův	Havlův	k2eAgInSc1d1	Havlův
duchovní	duchovní	k2eAgInSc1d1	duchovní
odkaz	odkaz	k1gInSc1	odkaz
–	–	k?	–
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
koncertní	koncertní	k2eAgInSc1d1	koncertní
sál	sál	k1gInSc1	sál
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
CVVM	CVVM	kA	CVVM
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
převažovali	převažovat	k5eAaImAgMnP	převažovat
kritici	kritik	k1gMnPc1	kritik
přejmenování	přejmenování	k1gNnPc2	přejmenování
nad	nad	k7c7	nad
příznivci	příznivec	k1gMnPc7	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
vláda	vláda	k1gFnSc1	vláda
odsouhlasila	odsouhlasit	k5eAaPmAgFnS	odsouhlasit
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
přejmenování	přejmenování	k1gNnSc4	přejmenování
Letiště	letiště	k1gNnSc2	letiště
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
na	na	k7c4	na
"	"	kIx"	"
<g/>
Letiště	letiště	k1gNnSc4	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
"	"	kIx"	"
<g/>
Prague	Prague	k1gInSc1	Prague
Airport	Airport	k1gInSc1	Airport
-	-	kIx~	-
Vaclav	Vaclav	k1gMnSc1	Vaclav
Havel	Havel	k1gMnSc1	Havel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
řada	řada	k1gFnSc1	řada
kritických	kritický	k2eAgInPc2d1	kritický
ohlasů	ohlas	k1gInPc2	ohlas
upozorňujících	upozorňující	k2eAgInPc2d1	upozorňující
na	na	k7c4	na
chybný	chybný	k2eAgInSc4d1	chybný
slovosled	slovosled	k1gInSc4	slovosled
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Pinkava	Pinkava	k?	Pinkava
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jana	Jan	k1gMnSc2	Jan
Křesadla	křesadlo	k1gNnSc2	křesadlo
<g/>
,	,	kIx,	,
upozornil	upozornit	k5eAaPmAgMnS	upozornit
redakci	redakce	k1gFnSc3	redakce
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
i	i	k8xC	i
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
slovosledu	slovosled	k1gInSc6	slovosled
název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
stal	stát	k5eAaPmAgMnS	stát
letištěm	letiště	k1gNnSc7	letiště
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
Pražské	pražský	k2eAgNnSc1d1	Pražské
letiště	letiště	k1gNnSc1	letiště
Vaclav	Vaclav	k1gMnSc1	Vaclav
Havel	Havel	k1gMnSc1	Havel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
se	se	k3xPyFc4	se
i	i	k9	i
nad	nad	k7c7	nad
chybějící	chybějící	k2eAgFnSc7d1	chybějící
čárkou	čárka	k1gFnSc7	čárka
v	v	k7c6	v
Havlově	Havlův	k2eAgNnSc6d1	Havlovo
jménu	jméno	k1gNnSc6	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
paskvil	paskvil	k1gInSc4	paskvil
<g/>
.	.	kIx.	.
</s>
<s>
Nesmyslnost	nesmyslnost	k1gFnSc1	nesmyslnost
schváleného	schválený	k2eAgInSc2d1	schválený
názvu	název	k1gInSc2	název
redakci	redakce	k1gFnSc4	redakce
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
překladatel	překladatel	k1gMnSc1	překladatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
František	František	k1gMnSc1	František
Fuka	Fuka	k1gMnSc1	Fuka
a	a	k8xC	a
Pavla	Pavla	k1gFnSc1	Pavla
Pohořálková	Pohořálkový	k2eAgFnSc1d1	Pohořálkový
z	z	k7c2	z
jazykové	jazykový	k2eAgFnSc2d1	jazyková
školy	škola	k1gFnSc2	škola
Glossa	Glossa	k1gFnSc1	Glossa
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
názvu	název	k1gInSc2	název
přišlo	přijít	k5eAaPmAgNnS	přijít
samo	sám	k3xTgNnSc1	sám
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
že	že	k8xS	že
na	na	k7c4	na
problém	problém	k1gInSc4	problém
nikdo	nikdo	k3yNnSc1	nikdo
dosud	dosud	k6eAd1	dosud
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
neupozornil	upozornit	k5eNaPmAgMnS	upozornit
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
letiště	letiště	k1gNnSc2	letiště
Eva	Eva	k1gFnSc1	Eva
Krejčí	Krejčí	k1gFnSc1	Krejčí
se	se	k3xPyFc4	se
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
název	název	k1gInSc4	název
hájit	hájit	k5eAaImF	hájit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Paris	Paris	k1gMnSc1	Paris
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
<g/>
"	"	kIx"	"
místo	místo	k1gNnSc1	místo
"	"	kIx"	"
<g/>
Paris	Paris	k1gMnSc1	Paris
–	–	k?	–
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gInSc1	Gaulle
Airport	Airport	k1gInSc1	Airport
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
definitivní	definitivní	k2eAgFnSc1d1	definitivní
verze	verze	k1gFnSc1	verze
nového	nový	k2eAgInSc2d1	nový
názvu	název	k1gInSc2	název
a	a	k8xC	a
nejvhodnější	vhodný	k2eAgInSc4d3	nejvhodnější
anglický	anglický	k2eAgInSc4d1	anglický
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
doladí	doladit	k5eAaPmIp3nS	doladit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
mluvčí	mluvčí	k1gFnSc1	mluvčí
letiště	letiště	k1gNnSc2	letiště
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
anglický	anglický	k2eAgInSc1d1	anglický
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
konzultaci	konzultace	k1gFnSc6	konzultace
s	s	k7c7	s
jazykovými	jazykový	k2eAgMnPc7d1	jazykový
experty	expert	k1gMnPc7	expert
přepracován	přepracovat	k5eAaPmNgMnS	přepracovat
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
"	"	kIx"	"
<g/>
Václav	Václav	k1gMnSc1	Václav
Havel	Havel	k1gMnSc1	Havel
Airport	Airport	k1gInSc4	Airport
Prague	Pragu	k1gInSc2	Pragu
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
přejmenovací	přejmenovací	k2eAgInSc1d1	přejmenovací
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Letiště	letiště	k1gNnSc2	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
Světového	světový	k2eAgInSc2d1	světový
dne	den	k1gInSc2	den
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
devět	devět	k4xCc1	devět
dní	den	k1gInPc2	den
před	před	k7c7	před
prvním	první	k4xOgNnSc7	první
výročím	výročí	k1gNnSc7	výročí
úmrtí	úmrť	k1gFnPc2	úmrť
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
Dagmar	Dagmar	k1gFnSc1	Dagmar
Havlové	Havlová	k1gFnSc2	Havlová
a	a	k8xC	a
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Karla	Karel	k1gMnSc2	Karel
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
v	v	k7c6	v
Terminálu	terminál	k1gInSc6	terminál
2	[number]	k4	2
odhalena	odhalen	k2eAgFnSc1d1	odhalena
tapiserie	tapiserie	k1gFnSc1	tapiserie
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
s	s	k7c7	s
názvem	název	k1gInSc7	název
Poslední	poslední	k2eAgFnSc1d1	poslední
audience	audience	k1gFnSc1	audience
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Petra	Petr	k1gMnSc2	Petr
Síse	Sís	k1gMnSc2	Sís
<g/>
;	;	kIx,	;
obrázek	obrázek	k1gInSc1	obrázek
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
použit	použít	k5eAaPmNgInS	použít
po	po	k7c6	po
Havlově	Havlův	k2eAgFnSc6d1	Havlova
smrti	smrt	k1gFnSc6	smrt
pro	pro	k7c4	pro
titulní	titulní	k2eAgFnSc4d1	titulní
stránku	stránka	k1gFnSc4	stránka
Hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc4	vznik
díla	dílo	k1gNnSc2	dílo
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
Bill	Bill	k1gMnSc1	Bill
Shipsey	Shipsea	k1gFnSc2	Shipsea
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
organizace	organizace	k1gFnSc2	organizace
Art	Art	k1gFnSc2	Art
for	forum	k1gNnPc2	forum
Amnesty	Amnest	k1gInPc7	Amnest
International	International	k1gMnPc2	International
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
ceně	cena	k1gFnSc6	cena
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
hudebníci	hudebník	k1gMnPc1	hudebník
Bono	bona	k1gFnSc5	bona
a	a	k8xC	a
The	The	k1gMnSc2	The
Edge	Edg	k1gMnSc2	Edg
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Gabriel	Gabriel	k1gMnSc1	Gabriel
<g/>
,	,	kIx,	,
Sting	Sting	k1gMnSc1	Sting
a	a	k8xC	a
Yoko	Yoko	k6eAd1	Yoko
Ono	onen	k3xDgNnSc1	onen
<g/>
.	.	kIx.	.
</s>
<s>
Nástěnný	nástěnný	k2eAgInSc1d1	nástěnný
tkaný	tkaný	k2eAgInSc1d1	tkaný
koberec	koberec	k1gInSc1	koberec
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
5	[number]	k4	5
<g/>
×	×	k?	×
<g/>
4,25	[number]	k4	4,25
metru	metr	k1gInSc2	metr
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
na	na	k7c6	na
tmavomodrém	tmavomodrý	k2eAgNnSc6d1	tmavomodré
pozadí	pozadí	k1gNnSc6	pozadí
hejno	hejno	k1gNnSc4	hejno
bílých	bílý	k2eAgMnPc2d1	bílý
ptáků	pták	k1gMnPc2	pták
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
člověka	člověk	k1gMnSc2	člověk
vznášejícího	vznášející	k2eAgMnSc2d1	vznášející
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
a	a	k8xC	a
siluetou	silueta	k1gFnSc7	silueta
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
umístěno	umístit	k5eAaPmNgNnS	umístit
poblíž	poblíž	k7c2	poblíž
obydlené	obydlený	k2eAgFnSc2d1	obydlená
zástavby	zástavba	k1gFnSc2	zástavba
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
kterou	který	k3yRgFnSc7	který
přelétají	přelétat	k5eAaImIp3nP	přelétat
přistávající	přistávající	k2eAgNnPc1d1	přistávající
a	a	k8xC	a
vzlétající	vzlétající	k2eAgNnPc1d1	vzlétající
letadla	letadlo	k1gNnPc1	letadlo
<g/>
,	,	kIx,	,
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
Letiště	letiště	k1gNnSc1	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
(	(	kIx(	(
<g/>
správce	správce	k1gMnSc1	správce
letiště	letiště	k1gNnSc2	letiště
<g/>
)	)	kIx)	)
některá	některý	k3yIgNnPc1	některý
opatření	opatření	k1gNnPc1	opatření
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgNnPc1d1	sestávající
jednak	jednak	k8xC	jednak
z	z	k7c2	z
omezujících	omezující	k2eAgNnPc2d1	omezující
pravidel	pravidlo	k1gNnPc2	pravidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
omezení	omezení	k1gNnSc1	omezení
vzletů	vzlet	k1gInPc2	vzlet
a	a	k8xC	a
přistání	přistání	k1gNnSc4	přistání
ve	v	k7c6	v
směrech	směr	k1gInPc6	směr
k	k	k7c3	k
<g />
.	.	kIx.	.
</s>
<s>
obydlené	obydlený	k2eAgFnPc1d1	obydlená
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
nočních	noční	k2eAgInPc2d1	noční
letů	let	k1gInPc2	let
<g/>
,	,	kIx,	,
nepřetržité	přetržitý	k2eNgNnSc1d1	nepřetržité
monitorování	monitorování	k1gNnSc1	monitorování
leteckého	letecký	k2eAgInSc2d1	letecký
hluku	hluk	k1gInSc2	hluk
<g/>
,	,	kIx,	,
vyhlašování	vyhlašování	k1gNnSc1	vyhlašování
hlukově	hlukově	k6eAd1	hlukově
chráněných	chráněný	k2eAgFnPc2d1	chráněná
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
letiště	letiště	k1gNnSc2	letiště
<g/>
,	,	kIx,	,
vyhlašování	vyhlašování	k1gNnSc1	vyhlašování
povinných	povinný	k2eAgInPc2d1	povinný
protihlukových	protihlukový	k2eAgInPc2d1	protihlukový
postupů	postup	k1gInPc2	postup
pro	pro	k7c4	pro
letadla	letadlo	k1gNnPc1	letadlo
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
uplatňováním	uplatňování	k1gNnSc7	uplatňování
poplatkové	poplatkový	k2eAgFnSc2d1	poplatková
(	(	kIx(	(
<g/>
penalizační	penalizační	k2eAgFnSc2d1	penalizační
<g/>
)	)	kIx)	)
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
způsobeném	způsobený	k2eAgInSc6d1	způsobený
leteckém	letecký	k2eAgInSc6d1	letecký
hluku	hluk	k1gInSc6	hluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
programu	program	k1gInSc2	program
Airport	Airport	k1gInSc1	Airport
Carbon	Carbon	k1gInSc1	Carbon
Accreditation	Accreditation	k1gInSc4	Accreditation
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
obdrželo	obdržet	k5eAaPmAgNnS	obdržet
certifikát	certifikát	k1gInSc4	certifikát
třetí	třetí	k4xOgFnSc2	třetí
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Snížilo	snížit	k5eAaPmAgNnS	snížit
tak	tak	k9	tak
opět	opět	k6eAd1	opět
oproti	oproti	k7c3	oproti
minulým	minulý	k2eAgFnPc3d1	minulá
roku	rok	k1gInSc2	rok
emise	emise	k1gFnSc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ruzyňské	ruzyňský	k2eAgNnSc1d1	ruzyňské
letiště	letiště	k1gNnSc1	letiště
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
v	v	k7c6	v
několika	několik	k4yIc6	několik
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gNnSc4	on
i	i	k8xC	i
Casino	Casino	k1gNnSc4	Casino
Royale	Royala	k1gFnSc3	Royala
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Ruzyně	Ruzyně	k1gFnSc1	Ruzyně
představuje	představovat	k5eAaImIp3nS	představovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Airbusem	airbus	k1gInSc7	airbus
A340-600	A340-600	k1gFnPc1	A340-600
společnosti	společnost	k1gFnSc2	společnost
Virgin	Virgina	k1gFnPc2	Virgina
Atlantic	Atlantice	k1gFnPc2	Atlantice
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Miami	Miami	k1gNnSc2	Miami
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jmenovat	jmenovat	k5eAaImF	jmenovat
např.	např.	kA	např.
Konec	konec	k1gInSc1	konec
agenta	agent	k1gMnSc2	agent
W4C	W4C	k1gMnSc2	W4C
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
psa	pes	k1gMnSc2	pes
pana	pan	k1gMnSc2	pan
Foustky	Foustka	k1gFnSc2	Foustka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
natočila	natočit	k5eAaBmAgFnS	natočit
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
dokument	dokument	k1gInSc1	dokument
Runway	runway	k1gFnSc1	runway
06-24	[number]	k4	06-24
o	o	k7c6	o
dráze	dráha	k1gFnSc6	dráha
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
24	[number]	k4	24
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
její	její	k3xOp3gFnSc4	její
generální	generální	k2eAgFnSc4d1	generální
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
u	u	k7c2	u
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
80	[number]	k4	80
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
letiště	letiště	k1gNnSc2	letiště
ČT	ČT	kA	ČT
natočila	natočit	k5eAaBmAgFnS	natočit
dokument	dokument	k1gInSc4	dokument
Příběh	příběh	k1gInSc1	příběh
letiště	letiště	k1gNnSc1	letiště
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popisuje	popisovat	k5eAaImIp3nS	popisovat
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
fungování	fungování	k1gNnSc4	fungování
zdejšího	zdejší	k2eAgInSc2d1	zdejší
leteckého	letecký	k2eAgInSc2d1	letecký
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Youtube	Youtub	k1gInSc5	Youtub
kanálu	kanál	k1gInSc6	kanál
letiště	letiště	k1gNnSc2	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
vychází	vycházet	k5eAaImIp3nS	vycházet
videa	video	k1gNnSc2	video
o	o	k7c6	o
provozu	provoz	k1gInSc6	provoz
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
<g/>
,	,	kIx,	,
praktických	praktický	k2eAgFnPc6d1	praktická
informacích	informace	k1gFnPc6	informace
a	a	k8xC	a
zdejších	zdejší	k2eAgNnPc6d1	zdejší
letadlech	letadlo	k1gNnPc6	letadlo
<g/>
.	.	kIx.	.
</s>
