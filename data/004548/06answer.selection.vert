<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
neboli	neboli	k8xC	neboli
Praha	Praha	k1gFnSc1	Praha
<g/>
/	/	kIx~	/
<g/>
Ruzyně	Ruzyně	k1gFnSc1	Ruzyně
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
PRG	PRG	kA	PRG
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
LKPR	LKPR	kA	LKPR
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
umístěné	umístěný	k2eAgNnSc1d1	umístěné
na	na	k7c6	na
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
okraji	okraj	k1gInSc6	okraj
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Praha	Praha	k1gFnSc1	Praha
6	[number]	k4	6
<g/>
,	,	kIx,	,
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Ruzyně	Ruzyně	k1gFnSc2	Ruzyně
<g/>
,	,	kIx,	,
u	u	k7c2	u
Kněževsi	Kněževes	k1gFnSc2	Kněževes
<g/>
.	.	kIx.	.
</s>
