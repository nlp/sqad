<s>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Jirásková	Jirásková	k1gFnSc1	Jirásková
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1931	[number]	k4	1931
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
ředitelka	ředitelka	k1gFnSc1	ředitelka
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
a	a	k8xC	a
partnerka	partnerka	k1gFnSc1	partnerka
českého	český	k2eAgNnSc2d1	české
filmového	filmový	k2eAgNnSc2d1	filmové
a	a	k8xC	a
televizního	televizní	k2eAgMnSc2d1	televizní
režiséra	režisér	k1gMnSc2	režisér
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
DAMU	DAMU	kA	DAMU
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přešla	přejít	k5eAaPmAgFnS	přejít
z	z	k7c2	z
řádového	řádový	k2eAgNnSc2d1	řádové
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
hrála	hrát	k5eAaImAgFnS	hrát
nejprve	nejprve	k6eAd1	nejprve
rok	rok	k1gInSc4	rok
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členkou	členka	k1gFnSc7	členka
pražského	pražský	k2eAgNnSc2d1	Pražské
Divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poté	poté	k6eAd1	poté
stále	stále	k6eAd1	stále
působila	působit	k5eAaImAgFnS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1990	[number]	k4	1990
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
také	také	k9	také
zastávala	zastávat	k5eAaImAgFnS	zastávat
funkci	funkce	k1gFnSc4	funkce
ředitelky	ředitelka	k1gFnSc2	ředitelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
jako	jako	k8xS	jako
členka	členka	k1gFnSc1	členka
vinohradské	vinohradský	k2eAgFnSc2d1	Vinohradská
scény	scéna	k1gFnSc2	scéna
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vykonstruovaného	vykonstruovaný	k2eAgInSc2d1	vykonstruovaný
procesu	proces	k1gInSc2	proces
svoji	svůj	k3xOyFgFnSc4	svůj
kolegyni	kolegyně	k1gFnSc4	kolegyně
Jiřinu	Jiřina	k1gFnSc4	Jiřina
Štěpničkovou	Štěpničková	k1gFnSc4	Štěpničková
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
vězněna	věznit	k5eAaImNgFnS	věznit
po	po	k7c4	po
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
se	se	k3xPyFc4	se
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
několikrát	několikrát	k6eAd1	několikrát
omluvila	omluvit	k5eAaPmAgFnS	omluvit
<g/>
:	:	kIx,	:
z	z	k7c2	z
mladické	mladický	k2eAgFnSc2d1	mladická
nerozvážnosti	nerozvážnost	k1gFnSc2	nerozvážnost
věřila	věřit	k5eAaImAgFnS	věřit
komunistické	komunistický	k2eAgNnSc4d1	komunistické
propagandě	propaganda	k1gFnSc3	propaganda
všechna	všechen	k3xTgNnPc4	všechen
obvinění	obvinění	k1gNnPc4	obvinění
tehdy	tehdy	k6eAd1	tehdy
odsouzené	odsouzený	k2eAgFnPc1d1	odsouzená
populární	populární	k2eAgFnPc1d1	populární
herečky	herečka	k1gFnPc1	herečka
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
pak	pak	k6eAd1	pak
čelila	čelit	k5eAaImAgFnS	čelit
komunistickým	komunistický	k2eAgFnPc3d1	komunistická
represím	represe	k1gFnPc3	represe
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
dostala	dostat	k5eAaPmAgFnS	dostat
první	první	k4xOgFnSc4	první
velkou	velká	k1gFnSc4	velká
příležitost	příležitost	k1gFnSc4	příležitost
ve	v	k7c6	v
špionážním	špionážní	k2eAgNnSc6d1	špionážní
dramatu	drama	k1gNnSc6	drama
Smyk	smyk	k1gInSc1	smyk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Brynych	Brynych	k1gMnSc1	Brynych
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
filmech	film	k1gInPc6	film
Každá	každý	k3xTgFnSc1	každý
koruna	koruna	k1gFnSc1	koruna
dobrá	dobrý	k2eAgFnSc1d1	dobrá
a	a	k8xC	a
Pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
objevil	objevit	k5eAaPmAgMnS	objevit
její	její	k3xOp3gInSc4	její
komediální	komediální	k2eAgInSc4d1	komediální
talent	talent	k1gInSc4	talent
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Podskalský	podskalský	k2eAgMnSc1d1	podskalský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
však	však	k9	však
nastala	nastat	k5eAaPmAgFnS	nastat
nucená	nucený	k2eAgFnSc1d1	nucená
přestávka	přestávka	k1gFnSc1	přestávka
před	před	k7c7	před
kamerou	kamera	k1gFnSc7	kamera
a	a	k8xC	a
na	na	k7c4	na
filmové	filmový	k2eAgNnSc4d1	filmové
plátno	plátno	k1gNnSc4	plátno
se	se	k3xPyFc4	se
vrátila	vrátit	k5eAaPmAgFnS	vrátit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Trhák	trhák	k1gInSc1	trhák
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
přes	přes	k7c4	přes
140	[number]	k4	140
filmových	filmový	k2eAgFnPc2d1	filmová
a	a	k8xC	a
televizních	televizní	k2eAgFnPc2d1	televizní
rolí	role	k1gFnPc2	role
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
dabingu	dabing	k1gInSc2	dabing
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
nevěnovala	věnovat	k5eNaImAgFnS	věnovat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
však	však	k9	však
stojí	stát	k5eAaImIp3nS	stát
dabing	dabing	k1gInSc4	dabing
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Taylor	Taylora	k1gFnPc2	Taylora
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
Virgínie	Virgínie	k1gFnSc1	Virgínie
Wolfové	Wolfové	k2eAgFnSc1d1	Wolfové
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
byl	být	k5eAaImAgMnS	být
herec	herec	k1gMnSc1	herec
Jiří	Jiří	k1gMnSc1	Jiří
Pleskot	Pleskot	k1gMnSc1	Pleskot
<g/>
,	,	kIx,	,
za	za	k7c2	za
kterého	který	k3yRgNnSc2	který
byla	být	k5eAaImAgFnS	být
vdaná	vdaný	k2eAgFnSc1d1	vdaná
jen	jen	k9	jen
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
žila	žít	k5eAaImAgFnS	žít
dvacet	dvacet	k4xCc4	dvacet
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
životním	životní	k2eAgMnSc7d1	životní
partnerem	partner	k1gMnSc7	partner
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Podskalským	podskalský	k2eAgMnSc7d1	podskalský
<g/>
.	.	kIx.	.
</s>
<s>
Neměla	mít	k5eNaImAgFnS	mít
žádné	žádný	k3yNgFnPc4	žádný
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
kmotrou	kmotra	k1gFnSc7	kmotra
mladšího	mladý	k2eAgMnSc4d2	mladší
syna	syn	k1gMnSc4	syn
herečky	herečka	k1gFnSc2	herečka
Simony	Simona	k1gFnSc2	Simona
Stašové	Stašová	k1gFnSc2	Stašová
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Staše	Staš	k1gMnSc2	Staš
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
rozvedení	rozvedení	k1gNnSc4	rozvedení
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
podruhé	podruhé	k6eAd1	podruhé
oženil	oženit	k5eAaPmAgMnS	oženit
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
její	její	k3xOp3gFnSc4	její
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
sestru	sestra	k1gFnSc4	sestra
Petru	Petra	k1gFnSc4	Petra
(	(	kIx(	(
<g/>
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
neteř	neteř	k1gFnSc1	neteř
Aneta	Aneta	k1gFnSc1	Aneta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
nestýkala	stýkat	k5eNaImAgFnS	stýkat
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
po	po	k7c6	po
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
nemoci	nemoc	k1gFnSc6	nemoc
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
81	[number]	k4	81
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
bytě	byt	k1gInSc6	byt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
byla	být	k5eAaImAgFnS	být
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
v	v	k7c6	v
jihočeských	jihočeský	k2eAgFnPc6d1	Jihočeská
Malenicích	Malenice	k1gFnPc6	Malenice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
oba	dva	k4xCgMnPc1	dva
její	její	k3xOp3gMnPc1	její
životní	životní	k2eAgMnPc1d1	životní
partneři	partner	k1gMnPc1	partner
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
Vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
Za	za	k7c4	za
vynikající	vynikající	k2eAgFnSc4d1	vynikající
práci	práce	k1gFnSc4	práce
1988	[number]	k4	1988
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
zasloužilou	zasloužilý	k2eAgFnSc7d1	zasloužilá
umělkyní	umělkyně	k1gFnSc7	umělkyně
1999	[number]	k4	1999
Cena	cena	k1gFnSc1	cena
Thálie	Thálie	k1gFnSc1	Thálie
2000	[number]	k4	2000
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
evropská	evropský	k2eAgFnSc1d1	Evropská
plaketa	plaketa	k1gFnSc1	plaketa
2006	[number]	k4	2006
Medaile	medaile	k1gFnSc1	medaile
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
stát	stát	k1gInSc4	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
2006	[number]	k4	2006
Cena	cena	k1gFnSc1	cena
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
české	český	k2eAgFnSc6d1	Česká
komedii	komedie	k1gFnSc3	komedie
2008	[number]	k4	2008
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Dvorany	dvorana	k1gFnSc2	dvorana
slávy	sláva	k1gFnSc2	sláva
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
anketě	anketa	k1gFnSc6	anketa
TýTý	TýTý	k1gFnSc1	TýTý
2008	[number]	k4	2008
1957	[number]	k4	1957
F.	F.	kA	F.
<g/>
Goodrichová	Goodrichový	k2eAgFnSc1d1	Goodrichový
<g/>
,	,	kIx,	,
A.	A.	kA	A.
<g/>
Hackett	Hackett	k1gInSc1	Hackett
<g/>
:	:	kIx,	:
Deník	deník	k1gInSc1	deník
Anny	Anna	k1gFnSc2	Anna
Frankové	Franková	k1gFnSc2	Franková
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Strejček	Strejček	k1gMnSc1	Strejček
1967	[number]	k4	1967
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
:	:	kIx,	:
August	August	k1gMnSc1	August
August	August	k1gMnSc1	August
<g/>
,	,	kIx,	,
august	august	k1gMnSc1	august
<g/>
,	,	kIx,	,
Lulu	lula	k1gFnSc4	lula
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dudek	Dudek	k1gMnSc1	Dudek
1973	[number]	k4	1973
William	William	k1gInSc1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
:	:	kIx,	:
Večer	večer	k6eAd1	večer
tříkrálový	tříkrálový	k2eAgInSc4d1	tříkrálový
nebo	nebo	k8xC	nebo
Cokoli	cokoli	k3yInSc4	cokoli
chcete	chtít	k5eAaImIp2nP	chtít
<g/>
,	,	kIx,	,
Olivie	Olivie	k1gFnSc1	Olivie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Stanislav	Stanislava	k1gFnPc2	Stanislava
Remunda	remunda	k1gFnSc1	remunda
1974	[number]	k4	1974
Robert	Robert	k1gMnSc1	Robert
Bolt	Bolt	k1gMnSc1	Bolt
<g/>
:	:	kIx,	:
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
královna	královna	k1gFnSc1	královna
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
Tudorovna	Tudorovna	k1gFnSc1	Tudorovna
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dudek	Dudek	k1gMnSc1	Dudek
1989	[number]	k4	1989
Vítězslav	Vítězslava	k1gFnPc2	Vítězslava
Hálek	hálka	k1gFnPc2	hálka
<g/>
:	:	kIx,	:
Záviš	Záviš	k1gMnSc1	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejno	k1gNnSc2	Falkenštejno
<g/>
,	,	kIx,	,
Královna	královna	k1gFnSc1	královna
Kunhuta	Kunhuta	k1gFnSc1	Kunhuta
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jan	Jan	k1gMnSc1	Jan
Kačer	kačer	k1gMnSc1	kačer
1996	[number]	k4	1996
Friedrich	Friedrich	k1gMnSc1	Friedrich
Dürrenmatt	Dürrenmatt	k1gMnSc1	Dürrenmatt
<g/>
:	:	kIx,	:
Návštěva	návštěva	k1gFnSc1	návštěva
staré	starý	k2eAgFnSc2d1	stará
dámy	dáma	k1gFnSc2	dáma
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Vladimír	Vladimír	k1gMnSc1	Vladimír
Strnisko	strnisko	k1gNnSc1	strnisko
j.	j.	k?	j.
h.	h.	k?	h.
2000	[number]	k4	2000
G.G.	G.G.	k1gMnSc2	G.G.
<g/>
Márquez	Márquez	k1gInSc4	Márquez
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
G.	G.	kA	G.
<g/>
Schwajda	Schwajda	k1gFnSc1	Schwajda
:	:	kIx,	:
Sto	sto	k4xCgNnSc4	sto
roků	rok	k1gInPc2	rok
samoty	samota	k1gFnSc2	samota
<g/>
,	,	kIx,	,
Úrsula	Úrsula	k1gFnSc1	Úrsula
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Novotný	Novotný	k1gMnSc1	Novotný
j.	j.	k?	j.
h.	h.	k?	h.
2003	[number]	k4	2003
Jiří	Jiří	k1gMnSc1	Jiří
Šotola	Šotola	k1gMnSc1	Šotola
<g/>
:	:	kIx,	:
Cesta	cesta	k1gFnSc1	cesta
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
Isabela	Isabela	k1gFnSc1	Isabela
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Novotný	Novotný	k1gMnSc1	Novotný
j.	j.	k?	j.
h.	h.	k?	h.
2003	[number]	k4	2003
J.	J.	kA	J.
C.	C.	kA	C.
Grumberg	Grumberg	k1gMnSc1	Grumberg
<g/>
:	:	kIx,	:
Krejčovský	krejčovský	k2eAgInSc1d1	krejčovský
salón	salón	k1gInSc1	salón
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Novotný	Novotný	k1gMnSc1	Novotný
j.	j.	k?	j.
h.	h.	k?	h.
1953	[number]	k4	1953
Po	po	k7c6	po
noci	noc	k1gFnSc6	noc
den	den	k1gInSc1	den
-	-	kIx~	-
Jiřinina	Jiřinin	k2eAgFnSc1d1	Jiřinina
kamarádka	kamarádka	k1gFnSc1	kamarádka
1960	[number]	k4	1960
Smyk	smyk	k1gInSc1	smyk
-	-	kIx~	-
Luisa	Luisa	k1gFnSc1	Luisa
1961	[number]	k4	1961
Každá	každý	k3xTgFnSc1	každý
koruna	koruna	k1gFnSc1	koruna
dobrá	dobrý	k2eAgFnSc1d1	dobrá
-	-	kIx~	-
učitelka	učitelka	k1gFnSc1	učitelka
hudby	hudba	k1gFnSc2	hudba
1961	[number]	k4	1961
Pohled	pohled	k1gInSc1	pohled
do	do	k7c2	do
očí	oko	k1gNnPc2	oko
-	-	kIx~	-
Olga	Olga	k1gFnSc1	Olga
Valentová	Valentová	k1gFnSc1	Valentová
1963	[number]	k4	1963
Einstein	Einstein	k1gMnSc1	Einstein
kontra	kontra	k2eAgFnSc2d1	kontra
Babinský	Babinský	k2eAgInSc4d1	Babinský
-	-	kIx~	-
Dáša	Dáša	k1gFnSc1	Dáša
1964	[number]	k4	1964
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
odvahu	odvaha	k1gFnSc4	odvaha
-	-	kIx~	-
Olina	Olina	k1gFnSc1	Olina
1964	[number]	k4	1964
Čintamani	čintamani	k1gNnSc2	čintamani
a	a	k8xC	a
podvodník	podvodník	k1gMnSc1	podvodník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
Příběh	příběh	k1gInSc1	příběh
sňatkového	sňatkový	k2eAgMnSc2d1	sňatkový
podvodníka	podvodník	k1gMnSc2	podvodník
-	-	kIx~	-
zákaznice	zákaznice	k1gFnSc1	zákaznice
1965	[number]	k4	1965
Bloudění	bloudění	k1gNnSc2	bloudění
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
1965	[number]	k4	1965
Třicet	třicet	k4xCc1	třicet
jedna	jeden	k4xCgFnSc1	jeden
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
-	-	kIx~	-
Věra	Věra	k1gFnSc1	Věra
1966	[number]	k4	1966
Dědeček	dědeček	k1gMnSc1	dědeček
<g/>
,	,	kIx,	,
Kylián	Kylián	k1gMnSc1	Kylián
a	a	k8xC	a
já	já	k3xPp1nSc1	já
-	-	kIx~	-
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Karpíšková	Karpíšková	k1gFnSc1	Karpíšková
1966	[number]	k4	1966
Hotel	hotel	k1gInSc1	hotel
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
-	-	kIx~	-
Marie	Marie	k1gFnSc1	Marie
1967	[number]	k4	1967
Dům	dům	k1gInSc1	dům
ztracených	ztracený	k2eAgFnPc2d1	ztracená
duší	duše	k1gFnPc2	duše
-	-	kIx~	-
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
1967	[number]	k4	1967
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
<g />
.	.	kIx.	.
</s>
<s>
Helenky	Helenka	k1gFnSc2	Helenka
-	-	kIx~	-
Viky	vika	k1gFnSc2	vika
1968	[number]	k4	1968
Naše	náš	k3xOp1gFnSc1	náš
bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
rodina	rodina	k1gFnSc1	rodina
-	-	kIx~	-
Solničková	Solničková	k1gFnSc1	Solničková
1969	[number]	k4	1969
Flirt	flirt	k1gInSc1	flirt
se	s	k7c7	s
slečnou	slečna	k1gFnSc7	slečna
Stříbrnou	stříbrná	k1gFnSc7	stříbrná
-	-	kIx~	-
Blumenfeldová	Blumenfeldová	k1gFnSc1	Blumenfeldová
1969	[number]	k4	1969
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
truchlivý	truchlivý	k2eAgMnSc1d1	truchlivý
bůh	bůh	k1gMnSc1	bůh
-	-	kIx~	-
Štenclová	Štenclová	k1gFnSc1	Štenclová
1969	[number]	k4	1969
Případ	případ	k1gInSc1	případ
pro	pro	k7c4	pro
začínajícího	začínající	k2eAgMnSc4d1	začínající
kata	kat	k1gMnSc4	kat
-	-	kIx~	-
Tadeášová	Tadeášová	k1gFnSc1	Tadeášová
1969	[number]	k4	1969
Světáci	Světák	k1gMnPc5	Světák
-	-	kIx~	-
Marcela	Marcela	k1gFnSc1	Marcela
1970	[number]	k4	1970
Ďábelské	ďábelský	k2eAgInPc1d1	ďábelský
líbánky	líbánky	k1gInPc1	líbánky
-	-	kIx~	-
Alžběta	Alžběta	k1gFnSc1	Alžběta
1970	[number]	k4	1970
Na	na	k7c6	na
kometě	kometa	k1gFnSc6	kometa
-	-	kIx~	-
Ester	ester	k1gInSc1	ester
1980	[number]	k4	1980
Julek	Julka	k1gFnPc2	Julka
-	-	kIx~	-
žena	žena	k1gFnSc1	žena
polykače	polykač	k1gMnSc2	polykač
<g />
.	.	kIx.	.
</s>
<s>
mečů	meč	k1gInPc2	meč
1980	[number]	k4	1980
Trhák	trhák	k1gInSc1	trhák
-	-	kIx~	-
skriptka	skriptka	k1gFnSc1	skriptka
1980	[number]	k4	1980
Útěky	útěk	k1gInPc7	útěk
domů	domů	k6eAd1	domů
-	-	kIx~	-
herečka	herečka	k1gFnSc1	herečka
1981	[number]	k4	1981
Křtiny	křtiny	k1gFnPc1	křtiny
-	-	kIx~	-
sekretářka	sekretářka	k1gFnSc1	sekretářka
Karolína	Karolína	k1gFnSc1	Karolína
1982	[number]	k4	1982
Jak	jak	k8xC	jak
svět	svět	k1gInSc1	svět
přichází	přicházet	k5eAaImIp3nS	přicházet
o	o	k7c4	o
básníky	básník	k1gMnPc4	básník
-	-	kIx~	-
ředitelka	ředitelka	k1gFnSc1	ředitelka
školy	škola	k1gFnSc2	škola
1982	[number]	k4	1982
Vinobraní	vinobraní	k1gNnSc2	vinobraní
-	-	kIx~	-
Albína	Albína	k1gFnSc1	Albína
Brousková	brouskový	k2eAgFnSc1d1	Brousková
1983	[number]	k4	1983
Bota	bota	k1gFnSc1	bota
jménem	jméno	k1gNnSc7	jméno
Melichar	Melichar	k1gMnSc1	Melichar
-	-	kIx~	-
Moutelíková	Moutelíková	k1gFnSc1	Moutelíková
1983	[number]	k4	1983
Katapult	katapulta	k1gFnPc2	katapulta
-	-	kIx~	-
Anna	Anna	k1gFnSc1	Anna
1983	[number]	k4	1983
Samorost	samorost	k1gInSc1	samorost
-	-	kIx~	-
Vlasta	Vlasta	k1gFnSc1	Vlasta
1983	[number]	k4	1983
Sestřičky	sestřička	k1gFnPc1	sestřička
-	-	kIx~	-
babi	babi	k?	babi
1983	[number]	k4	1983
Slunce	slunce	k1gNnSc2	slunce
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
jahody	jahoda	k1gFnPc1	jahoda
-	-	kIx~	-
řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
Hubičková	hubičkový	k2eAgFnSc1d1	Hubičková
1983	[number]	k4	1983
Egy	ego	k1gNnPc7	ego
kicsit	kicsit	k1gInSc4	kicsit
én	én	k?	én
<g/>
,	,	kIx,	,
egy	ego	k1gNnPc7	ego
kicsit	kicsit	k1gInSc1	kicsit
te	te	k?	te
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
Evy	Eva	k1gFnSc2	Eva
1984	[number]	k4	1984
Barrandovské	barrandovský	k2eAgNnSc1d1	Barrandovské
nokturno	nokturno	k1gNnSc1	nokturno
aneb	aneb	k?	aneb
jak	jak	k8xS	jak
film	film	k1gInSc4	film
zpíval	zpívat	k5eAaImAgMnS	zpívat
a	a	k8xC	a
tančil	tančit	k5eAaImAgMnS	tančit
-	-	kIx~	-
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
1986	[number]	k4	1986
Můj	můj	k3xOp1gMnSc1	můj
hříšný	hříšný	k2eAgMnSc1d1	hříšný
muž	muž	k1gMnSc1	muž
-	-	kIx~	-
Jeníková	Jeníková	k1gFnSc1	Jeníková
1986	[number]	k4	1986
Šiesta	Šiesta	k1gFnSc1	Šiesta
veta	veto	k1gNnSc2	veto
-	-	kIx~	-
Ema	Ema	k1gFnSc1	Ema
Goldpergerová	Goldpergerová	k1gFnSc1	Goldpergerová
1986	[number]	k4	1986
Veselé	Veselé	k2eAgFnPc1d1	Veselé
Vánoce	Vánoce	k1gFnPc1	Vánoce
přejí	přát	k5eAaImIp3nP	přát
chobotnice	chobotnice	k1gFnPc4	chobotnice
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
paní	paní	k1gFnSc1	paní
Krásná	krásný	k2eAgFnSc1d1	krásná
1987	[number]	k4	1987
Když	když	k8xS	když
v	v	k7c6	v
ráji	ráj	k1gInSc6	ráj
pršelo	pršet	k5eAaImAgNnS	pršet
-	-	kIx~	-
Dočkajka	Dočkajka	k1gFnSc1	Dočkajka
1988	[number]	k4	1988
Maria	Maria	k1gFnSc1	Maria
Stuarda	Stuarda	k1gFnSc1	Stuarda
1989	[number]	k4	1989
Dva	dva	k4xCgMnPc4	dva
lidi	člověk	k1gMnPc4	člověk
v	v	k7c4	v
zoo	zoo	k1gFnSc4	zoo
-	-	kIx~	-
Josefína	Josefína	k1gFnSc1	Josefína
1989	[number]	k4	1989
Muka	muka	k1gFnSc1	muka
obraznosti	obraznost	k1gFnSc2	obraznost
-	-	kIx~	-
Antlová	Antlová	k1gFnSc1	Antlová
1989	[number]	k4	1989
Příběh	příběh	k1gInSc1	příběh
'	'	kIx"	'
<g/>
88	[number]	k4	88
-	-	kIx~	-
Zdena	Zdena	k1gFnSc1	Zdena
1989	[number]	k4	1989
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc4	seno
a	a	k8xC	a
pár	pár	k4xCyI	pár
facek	facka	k1gFnPc2	facka
-	-	kIx~	-
řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
Hubičková	hubičkový	k2eAgFnSc1d1	Hubičková
1990	[number]	k4	1990
Motýlí	motýlí	k2eAgFnSc1d1	motýlí
čas	čas	k1gInSc1	čas
-	-	kIx~	-
ředitelka	ředitelka	k1gFnSc1	ředitelka
školy	škola	k1gFnSc2	škola
1991	[number]	k4	1991
<g />
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
erotika	erotika	k1gFnSc1	erotika
-	-	kIx~	-
řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
Hubičková	hubičkový	k2eAgFnSc1d1	Hubičková
1993	[number]	k4	1993
Jedna	jeden	k4xCgFnSc1	jeden
kočka	kočka	k1gFnSc1	kočka
za	za	k7c4	za
druhou	druhý	k4xOgFnSc4	druhý
-	-	kIx~	-
paní	paní	k1gFnSc4	paní
domácí	domácí	k2eAgFnSc4d1	domácí
1995	[number]	k4	1995
Fany	Fany	k1gFnSc4	Fany
-	-	kIx~	-
MUDr.	MUDr.	kA	MUDr.
Jiřina	Jiřina	k1gFnSc1	Jiřina
Venclovská	Venclovský	k2eAgFnSc1d1	Venclovská
1996	[number]	k4	1996
Let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Make	Make	k1gFnSc7	Make
an	an	k?	an
Opera	opera	k1gFnSc1	opera
2005	[number]	k4	2005
Anděl	Anděla	k1gFnPc2	Anděla
Páně	páně	k2eAgFnSc1d1	páně
-	-	kIx~	-
abatyše	abatyše	k1gFnSc1	abatyše
2005	[number]	k4	2005
Povodeň	povodeň	k1gFnSc1	povodeň
-	-	kIx~	-
Gabriela	Gabriela	k1gFnSc1	Gabriela
Kvapilová	Kvapilová	k1gFnSc1	Kvapilová
2006	[number]	k4	2006
Rafťáci	Rafťák	k1gMnPc5	Rafťák
-	-	kIx~	-
babička	babička	k1gFnSc1	babička
Dadyho	Dady	k1gMnSc4	Dady
2012	[number]	k4	2012
<g />
.	.	kIx.	.
</s>
<s>
Vrásky	vrásek	k1gInPc1	vrásek
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
-	-	kIx~	-
Jižná	jižný	k2eAgFnSc1d1	Jižná
2012	[number]	k4	2012
Školní	školní	k2eAgFnSc1d1	školní
výlet	výlet	k1gInSc1	výlet
-	-	kIx~	-
hraběnka	hraběnka	k1gFnSc1	hraběnka
Schwarzová	Schwarzová	k1gFnSc1	Schwarzová
2013	[number]	k4	2013
Donšajni	Donšajeň	k1gFnSc6	Donšajeň
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
Markéty	Markéta	k1gFnSc2	Markéta
Jiřina	Jiřina	k1gFnSc1	Jiřina
1962	[number]	k4	1962
Pozdní	pozdní	k2eAgFnSc1d1	pozdní
láska	láska	k1gFnSc1	láska
1962	[number]	k4	1962
Střevíčky	střevíček	k1gInPc4	střevíček
-	-	kIx~	-
Marta	Marta	k1gFnSc1	Marta
1968	[number]	k4	1968
Sňatky	sňatek	k1gInPc7	sňatek
z	z	k7c2	z
rozumu	rozum	k1gInSc2	rozum
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hana	Hana	k1gFnSc1	Hana
1970	[number]	k4	1970
Obavy	obava	k1gFnPc1	obava
komisaře	komisař	k1gMnSc2	komisař
Maigreta	Maigret	k1gMnSc2	Maigret
1982	[number]	k4	1982
O	o	k7c6	o
labuti	labuť	k1gFnSc6	labuť
1986	[number]	k4	1986
Zlá	zlá	k1gFnSc1	zlá
krev	krev	k1gFnSc1	krev
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
-	-	kIx~	-
Hana	Hana	k1gFnSc1	Hana
Bornová	Bornová	k1gFnSc1	Bornová
1986	[number]	k4	1986
O	o	k7c6	o
houbovém	houbový	k2eAgMnSc6d1	houbový
Kubovi	Kuba	k1gMnSc6	Kuba
a	a	k8xC	a
princi	princ	k1gMnSc6	princ
Jakubovi	Jakub	k1gMnSc6	Jakub
1987	[number]	k4	1987
Stačí	stačit	k5eAaBmIp3nS	stačit
stisknout	stisknout	k5eAaPmF	stisknout
-	-	kIx~	-
Macková	Macková	k1gFnSc1	Macková
1987	[number]	k4	1987
Poslední	poslední	k2eAgInPc1d1	poslední
leč	leč	k8xC	leč
Alfonse	Alfons	k1gMnSc4	Alfons
Karáska	Karásek	k1gMnSc4	Karásek
1987	[number]	k4	1987
Podivná	podivný	k2eAgFnSc1d1	podivná
nevěsta	nevěsta	k1gFnSc1	nevěsta
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
1989	[number]	k4	1989
Čeleď	čeleď	k1gFnSc1	čeleď
brouků	brouk	k1gMnPc2	brouk
finančníků	finančník	k1gMnPc2	finančník
-	-	kIx~	-
uklízečka	uklízečka	k1gFnSc1	uklízečka
1990	[number]	k4	1990
Dcera	dcera	k1gFnSc1	dcera
národa	národ	k1gInSc2	národ
1990	[number]	k4	1990
Radostný	radostný	k2eAgInSc1d1	radostný
život	život	k1gInSc1	život
posmrtný	posmrtný	k2eAgInSc1d1	posmrtný
1991	[number]	k4	1991
Přes	přes	k7c4	přes
padací	padací	k2eAgInPc4d1	padací
mosty	most	k1gInPc4	most
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
1991	[number]	k4	1991
Talisman	talisman	k1gInSc1	talisman
<g />
.	.	kIx.	.
</s>
<s>
1991	[number]	k4	1991
Koho	kdo	k3yQnSc4	kdo
ofoukne	ofouknout	k5eAaPmIp3nS	ofouknout
větříček	větříček	k1gInSc1	větříček
1991	[number]	k4	1991
Klaním	klanit	k5eAaImIp1nS	klanit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
měsíci	měsíc	k1gInSc6	měsíc
1991	[number]	k4	1991
Závrať	závrať	k1gFnSc1	závrať
1992	[number]	k4	1992
Kadeř	kadeř	k1gFnSc1	kadeř
královny	královna	k1gFnSc2	královna
Bereniké	Bereniký	k2eAgFnSc2d1	Bereniký
-	-	kIx~	-
Klió	Klió	k1gFnSc2	Klió
1993	[number]	k4	1993
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
hraběnka	hraběnka	k1gFnSc1	hraběnka
1994	[number]	k4	1994
Bylo	být	k5eAaImAgNnS	být
nás	my	k3xPp1nPc2	my
pět	pět	k4xCc1	pět
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
Fajstová	Fajstová	k1gFnSc1	Fajstová
1996	[number]	k4	1996
Život	život	k1gInSc1	život
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
babička	babička	k1gFnSc1	babička
Králová	Králová	k1gFnSc1	Králová
1996	[number]	k4	1996
<g />
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgMnSc1d1	pražský
písničkář	písničkář	k1gMnSc1	písničkář
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
vypravěčka	vypravěčka	k1gFnSc1	vypravěčka
2002	[number]	k4	2002
O	o	k7c6	o
ztracené	ztracený	k2eAgFnSc6d1	ztracená
lásce	láska	k1gFnSc6	láska
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
;	;	kIx,	;
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
díle	dílo	k1gNnSc6	dílo
<g/>
)	)	kIx)	)
-	-	kIx~	-
sudička	sudička	k1gFnSc1	sudička
2002	[number]	k4	2002
Kožené	kožený	k2eAgFnSc6d1	kožená
slunce	slunka	k1gFnSc6	slunka
-	-	kIx~	-
Zobalka	Zobalka	k1gFnSc1	Zobalka
2002	[number]	k4	2002
Vyvraždění	vyvraždění	k1gNnSc4	vyvraždění
rodiny	rodina	k1gFnSc2	rodina
Greenů	Green	k1gMnPc2	Green
-	-	kIx~	-
Paní	paní	k1gFnSc1	paní
Greenová	Greenová	k1gFnSc1	Greenová
2004	[number]	k4	2004
Povodeň	povodeň	k1gFnSc1	povodeň
-	-	kIx~	-
Gabriela	Gabriela	k1gFnSc1	Gabriela
2004	[number]	k4	2004
Pojišťovna	pojišťovna	k1gFnSc1	pojišťovna
štěstí	štěstí	k1gNnSc2	štěstí
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
Kekulová	Kekulový	k2eAgFnSc1d1	Kekulová
2005	[number]	k4	2005
Dobrá	dobrý	k2eAgFnSc1d1	dobrá
čtvrť	čtvrť	k1gFnSc1	čtvrť
(	(	kIx(	(
<g/>
TV	TV	kA	TV
seriál	seriál	k1gInSc1	seriál
<g/>
)	)	kIx)	)
-	-	kIx~	-
matka	matka	k1gFnSc1	matka
profesorky	profesorka	k1gFnSc2	profesorka
Svobodové	Svobodové	k2eAgFnSc1d1	Svobodové
2012	[number]	k4	2012
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
TV	TV	kA	TV
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1952	[number]	k4	1952
-	-	kIx~	-
Zázračný	zázračný	k2eAgInSc1d1	zázračný
prsten	prsten	k1gInSc1	prsten
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ljubava	Ljubava	k1gFnSc1	Ljubava
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
-	-	kIx~	-
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vražda	vražda	k1gFnSc1	vražda
<g/>
,	,	kIx,	,
řekla	říct	k5eAaPmAgFnS	říct
...	...	k?	...
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Muriel	Muriel	k1gMnSc1	Muriel
Pavlow	Pavlow	k1gMnSc1	Pavlow
(	(	kIx(	(
<g/>
Ema	Ema	k1gMnSc1	Ema
Ackenthorpe	Ackenthorp	k1gInSc5	Ackenthorp
<g/>
)	)	kIx)	)
1966	[number]	k4	1966
-	-	kIx~	-
Past	past	k1gFnSc1	past
na	na	k7c4	na
Popelku	Popelka	k1gFnSc4	Popelka
<g/>
,	,	kIx,	,
Dany	Dana	k1gFnPc4	Dana
Carrel	Carrela	k1gFnPc2	Carrela
(	(	kIx(	(
<g/>
Michè	Michè	k1gFnSc1	Michè
Isola	Isola	k1gFnSc1	Isola
/	/	kIx~	/
Dominique	Dominique	k1gFnSc1	Dominique
Loï	Loï	k1gMnSc1	Loï
<g/>
)	)	kIx)	)
1968	[number]	k4	1968
-	-	kIx~	-
Kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
Virgínie	Virgínie	k1gFnSc1	Virgínie
Wolfové	Wolfové	k2eAgFnSc1d1	Wolfové
<g/>
,	,	kIx,	,
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
Marta	Marta	k1gFnSc1	Marta
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
-	-	kIx~	-
Zločin	zločin	k1gInSc1	zločin
Davida	David	k1gMnSc2	David
Levinsteina	Levinstein	k1gMnSc2	Levinstein
<g/>
,	,	kIx,	,
Bérangè	Bérangè	k1gFnSc7	Bérangè
<g />
.	.	kIx.	.
</s>
<s>
Dautun	Dautun	k1gNnSc1	Dautun
(	(	kIx(	(
<g/>
Jacky	Jacka	k1gFnPc1	Jacka
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
-	-	kIx~	-
Trampoty	trampota	k1gFnPc1	trampota
s	s	k7c7	s
dědictvím	dědictví	k1gNnSc7	dědictví
<g/>
,	,	kIx,	,
Galina	Galina	k1gFnSc1	Galina
Polskich	Polskich	k1gMnSc1	Polskich
(	(	kIx(	(
<g/>
vedoucí	vedoucí	k1gMnSc1	vedoucí
<g/>
)	)	kIx)	)
1985	[number]	k4	1985
-	-	kIx~	-
Testament	testament	k1gInSc1	testament
profesora	profesor	k1gMnSc2	profesor
Dowella	Dowell	k1gMnSc2	Dowell
<g/>
,	,	kIx,	,
Valentina	Valentin	k1gMnSc2	Valentin
Titova	Titovo	k1gNnSc2	Titovo
(	(	kIx(	(
<g/>
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Loranová	Loranová	k1gFnSc1	Loranová
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
-	-	kIx~	-
Jediný	jediný	k2eAgMnSc1d1	jediný
svědek	svědek	k1gMnSc1	svědek
<g/>
,	,	kIx,	,
Renée	René	k1gMnPc4	René
Faure	Faur	k1gInSc5	Faur
(	(	kIx(	(
<g/>
Lambelainová	Lambelainová	k1gFnSc1	Lambelainová
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
-	-	kIx~	-
Chobotnice	chobotnice	k1gFnSc1	chobotnice
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Lassander	Lassander	k1gMnSc1	Lassander
(	(	kIx(	(
<g/>
Cannitova	Cannitův	k2eAgFnSc1d1	Cannitův
manželka	manželka	k1gFnSc1	manželka
<g/>
)	)	kIx)	)
</s>
