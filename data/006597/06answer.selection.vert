<s>
První	první	k4xOgFnSc7	první
transgenní	transgenní	k2eAgFnSc7d1	transgenní
rostlinou	rostlina	k1gFnSc7	rostlina
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
tabák	tabák	k1gInSc1	tabák
(	(	kIx(	(
<g/>
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
<g/>
)	)	kIx)	)
s	s	k7c7	s
resistencí	resistence	k1gFnSc7	resistence
k	k	k7c3	k
antibiotiku	antibiotikum	k1gNnSc3	antibiotikum
kanamycinu	kanamycin	k1gInSc2	kanamycin
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byly	být	k5eAaImAgInP	být
skupinou	skupina	k1gFnSc7	skupina
Rogera	Roger	k1gMnSc2	Roger
Beachyho	Beachy	k1gMnSc2	Beachy
(	(	kIx(	(
<g/>
Washingtonova	Washingtonův	k2eAgFnSc1d1	Washingtonova
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
)	)	kIx)	)
úspěšně	úspěšně	k6eAd1	úspěšně
provedeny	provést	k5eAaPmNgInP	provést
první	první	k4xOgInPc1	první
polní	polní	k2eAgInPc1d1	polní
pokusy	pokus	k1gInPc1	pokus
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
plodiny	plodina	k1gFnSc2	plodina
–	–	k?	–
rajčat	rajče	k1gNnPc2	rajče
odolných	odolný	k2eAgNnPc2d1	odolné
k	k	k7c3	k
viru	vir	k1gInSc3	vir
TMV	TMV	kA	TMV
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
mozaiky	mozaika	k1gFnSc2	mozaika
tabáku	tabák	k1gInSc2	tabák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
