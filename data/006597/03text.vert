<s>
Geneticky	geneticky	k6eAd1	geneticky
modifikovaný	modifikovaný	k2eAgInSc1d1	modifikovaný
organismus	organismus	k1gInSc1	organismus
(	(	kIx(	(
<g/>
GM	GM	kA	GM
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
GMO	GMO	kA	GMO
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
RNA	RNA	kA	RNA
u	u	k7c2	u
RNA	RNA	kA	RNA
virů	vir	k1gInPc2	vir
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
úmyslně	úmyslně	k6eAd1	úmyslně
změněn	změnit	k5eAaPmNgInS	změnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
rekombinací	rekombinace	k1gFnSc7	rekombinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
genetické	genetický	k2eAgFnPc4d1	genetická
modifikace	modifikace	k1gFnPc4	modifikace
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
diskuzí	diskuze	k1gFnPc2	diskuze
<g/>
.	.	kIx.	.
</s>
<s>
Zásahy	zásah	k1gInPc1	zásah
do	do	k7c2	do
genetického	genetický	k2eAgInSc2d1	genetický
materiálu	materiál	k1gInSc2	materiál
organismů	organismus	k1gInPc2	organismus
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
způsobů	způsob	k1gInPc2	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
jsou	být	k5eAaImIp3nP	být
nahodilé	nahodilý	k2eAgInPc4d1	nahodilý
zásahy	zásah	k1gInPc4	zásah
působením	působení	k1gNnSc7	působení
mutagenů	mutagen	k1gInPc2	mutagen
nebo	nebo	k8xC	nebo
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
například	například	k6eAd1	například
většina	většina	k1gFnSc1	většina
současných	současný	k2eAgFnPc2d1	současná
odrůd	odrůda	k1gFnPc2	odrůda
pšenice	pšenice	k1gFnSc2	pšenice
<g/>
,	,	kIx,	,
řepky	řepka	k1gFnSc2	řepka
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nahodilé	nahodilý	k2eAgInPc1d1	nahodilý
zásahy	zásah	k1gInPc1	zásah
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
genetickou	genetický	k2eAgFnSc4d1	genetická
modifikaci	modifikace	k1gFnSc4	modifikace
a	a	k8xC	a
nevztahují	vztahovat	k5eNaImIp3nP	vztahovat
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
regulace	regulace	k1gFnPc4	regulace
vyplývající	vyplývající	k2eAgFnPc4d1	vyplývající
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
nakládání	nakládání	k1gNnSc6	nakládání
s	s	k7c7	s
GMO	GMO	kA	GMO
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
78	[number]	k4	78
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
typem	typ	k1gInSc7	typ
jsou	být	k5eAaImIp3nP	být
cílené	cílený	k2eAgInPc1d1	cílený
zásahy	zásah	k1gInPc1	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Mutace	mutace	k1gFnPc1	mutace
jsou	být	k5eAaImIp3nP	být
získány	získat	k5eAaPmNgFnP	získat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
vneseme	vnést	k5eAaPmIp1nP	vnést
nebo	nebo	k8xC	nebo
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
cíleně	cíleně	k6eAd1	cíleně
deaktivujeme	deaktivovat	k5eAaImIp1nP	deaktivovat
nějaké	nějaký	k3yIgInPc4	nějaký
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
geny	gen	k1gInPc4	gen
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
bakterie	bakterie	k1gFnSc2	bakterie
Agrobacterium	Agrobacterium	k1gNnSc1	Agrobacterium
tumefaciens	tumefaciensa	k1gFnPc2	tumefaciensa
vnesen	vnést	k5eAaPmNgInS	vnést
gen	gen	k1gInSc1	gen
pro	pro	k7c4	pro
odolnost	odolnost	k1gFnSc4	odolnost
k	k	k7c3	k
herbicidům	herbicid	k1gInPc3	herbicid
nebo	nebo	k8xC	nebo
gen	gen	k1gInSc1	gen
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
insekticidů	insekticid	k1gInPc2	insekticid
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Bt-kukuřice	Btukuřice	k1gFnSc2	Bt-kukuřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
GMO	GMO	kA	GMO
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
byl	být	k5eAaImAgInS	být
metodami	metoda	k1gFnPc7	metoda
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
cíleně	cíleně	k6eAd1	cíleně
přenesen	přenesen	k2eAgInSc1d1	přenesen
gen	gen	k1gInSc1	gen
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
transgenní	transgenní	k2eAgInSc1d1	transgenní
organismus	organismus	k1gInSc1	organismus
a	a	k8xC	a
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
transgenoze	transgenoze	k6eAd1	transgenoze
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgFnPc4d1	genetická
modifikace	modifikace	k1gFnPc4	modifikace
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
jednobuněčných	jednobuněčný	k2eAgInPc2d1	jednobuněčný
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organismů	organismus	k1gInPc2	organismus
–	–	k?	–
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
například	například	k6eAd1	například
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
lidského	lidský	k2eAgInSc2d1	lidský
inzulínu	inzulín	k1gInSc2	inzulín
<g/>
.	.	kIx.	.
</s>
<s>
Přenáší	přenášet	k5eAaImIp3nS	přenášet
se	se	k3xPyFc4	se
rekombinantní	rekombinantní	k2eAgFnSc1d1	rekombinantní
DNA	dna	k1gFnSc1	dna
z	z	k7c2	z
beta-buněk	betauňka	k1gFnPc2	beta-buňka
Langerhansových	Langerhansův	k2eAgInPc2d1	Langerhansův
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
slinivky	slinivka	k1gFnSc2	slinivka
břišní	břišní	k2eAgFnSc6d1	břišní
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	coli	k1gNnPc2	coli
nebo	nebo	k8xC	nebo
Saccharomyces	Saccharomycesa	k1gFnPc2	Saccharomycesa
cerevisiae	cerevisiae	k1gFnPc2	cerevisiae
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
poté	poté	k6eAd1	poté
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
inzulín	inzulín	k1gInSc4	inzulín
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
geneticky	geneticky	k6eAd1	geneticky
modifikovaným	modifikovaný	k2eAgInSc7d1	modifikovaný
organismem	organismus	k1gInSc7	organismus
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
nazýváme	nazývat	k5eAaImIp1nP	nazývat
je	on	k3xPp3gFnPc4	on
většinou	většinou	k6eAd1	většinou
transgenní	transgenní	k2eAgFnPc4d1	transgenní
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
transgenní	transgenní	k2eAgFnSc7d1	transgenní
rostlinou	rostlina	k1gFnSc7	rostlina
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
tabák	tabák	k1gInSc1	tabák
(	(	kIx(	(
<g/>
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
<g/>
)	)	kIx)	)
s	s	k7c7	s
resistencí	resistence	k1gFnSc7	resistence
k	k	k7c3	k
antibiotiku	antibiotikum	k1gNnSc3	antibiotikum
kanamycinu	kanamycin	k1gInSc2	kanamycin
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byly	být	k5eAaImAgInP	být
skupinou	skupina	k1gFnSc7	skupina
Rogera	Roger	k1gMnSc2	Roger
Beachyho	Beachy	k1gMnSc2	Beachy
(	(	kIx(	(
<g/>
Washingtonova	Washingtonův	k2eAgFnSc1d1	Washingtonova
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
)	)	kIx)	)
úspěšně	úspěšně	k6eAd1	úspěšně
provedeny	provést	k5eAaPmNgInP	provést
první	první	k4xOgInPc1	první
polní	polní	k2eAgInPc1d1	polní
pokusy	pokus	k1gInPc1	pokus
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
plodiny	plodina	k1gFnSc2	plodina
–	–	k?	–
rajčat	rajče	k1gNnPc2	rajče
odolných	odolný	k2eAgNnPc2d1	odolné
k	k	k7c3	k
viru	vir	k1gInSc3	vir
TMV	TMV	kA	TMV
(	(	kIx(	(
<g/>
virus	virus	k1gInSc1	virus
mozaiky	mozaika	k1gFnSc2	mozaika
tabáku	tabák	k1gInSc2	tabák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Omezením	omezení	k1gNnSc7	omezení
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Agrobacterium	Agrobacterium	k1gNnSc1	Agrobacterium
infikuje	infikovat	k5eAaBmIp3nS	infikovat
převážně	převážně	k6eAd1	převážně
dvouděložné	dvouděložný	k2eAgFnPc4d1	dvouděložná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vypracování	vypracování	k1gNnSc1	vypracování
protokolů	protokol	k1gInPc2	protokol
pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
jednoděložných	jednoděložný	k2eAgFnPc2d1	jednoděložná
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
obilniny	obilnina	k1gFnSc2	obilnina
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
do	do	k7c2	do
nedávné	dávný	k2eNgFnSc2d1	nedávná
doby	doba	k1gFnSc2	doba
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
objevy	objev	k1gInPc1	objev
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
byly	být	k5eAaImAgFnP	být
učiněny	učinit	k5eAaImNgFnP	učinit
koncem	koncem	k7c2	koncem
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
zejména	zejména	k9	zejména
Jeff	Jeff	k1gMnSc1	Jeff
Schell	Schell	k1gMnSc1	Schell
a	a	k8xC	a
Marc	Marc	k1gFnSc1	Marc
Van	van	k1gInSc1	van
Montagu	Montag	k1gInSc2	Montag
(	(	kIx(	(
<g/>
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Ghentu	Ghent	k1gInSc6	Ghent
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mary-Dell	Mary-Dell	k1gInSc1	Mary-Dell
Chilton	Chilton	k1gInSc1	Chilton
(	(	kIx(	(
<g/>
Washingtonova	Washingtonův	k2eAgFnSc1d1	Washingtonova
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ernie	Ernie	k1gFnSc1	Ernie
Jaworski	Jaworsk	k1gFnSc2	Jaworsk
(	(	kIx(	(
<g/>
Monsanto	Monsanta	k1gFnSc5	Monsanta
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Louis	Louis	k1gMnSc1	Louis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podařilo	podařit	k5eAaPmAgNnS	podařit
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
upravit	upravit	k5eAaPmF	upravit
přirozené	přirozený	k2eAgFnPc4d1	přirozená
Ti-plazmidy	Tilazmida	k1gFnPc4	Ti-plazmida
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
namísto	namísto	k7c2	namísto
genů	gen	k1gInPc2	gen
vyvolávajících	vyvolávající	k2eAgInPc2d1	vyvolávající
rostlinné	rostlinný	k2eAgNnSc4d1	rostlinné
onemocnění	onemocnění	k1gNnSc4	onemocnění
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
vloženy	vložen	k2eAgInPc4d1	vložen
geny	gen	k1gInPc4	gen
propůjčující	propůjčující	k2eAgInPc4d1	propůjčující
rostlinám	rostlina	k1gFnPc3	rostlina
zemědělsky	zemědělsky	k6eAd1	zemědělsky
užitečné	užitečný	k2eAgFnPc4d1	užitečná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výhodným	výhodný	k2eAgFnPc3d1	výhodná
vlastnostem	vlastnost	k1gFnPc3	vlastnost
<g/>
,	,	kIx,	,
kterých	který	k3yRgFnPc2	který
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nP	docílit
genetickou	genetický	k2eAgFnSc7d1	genetická
modifikací	modifikace	k1gFnSc7	modifikace
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
škůdcům	škůdce	k1gMnPc3	škůdce
(	(	kIx(	(
<g/>
Bt	Bt	k1gFnSc1	Bt
toxin	toxin	k1gInSc1	toxin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
pesticidům	pesticid	k1gInPc3	pesticid
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgFnSc1d2	lepší
nutriční	nutriční	k2eAgFnPc4d1	nutriční
hodnoty	hodnota	k1gFnPc4	hodnota
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
nepříznivému	příznivý	k2eNgNnSc3d1	nepříznivé
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
transgenních	transgenní	k2eAgFnPc2d1	transgenní
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
nejčastěji	často	k6eAd3	často
dvě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
–	–	k?	–
transformace	transformace	k1gFnSc2	transformace
pomocí	pomocí	k7c2	pomocí
agrobakterií	agrobakterie	k1gFnPc2	agrobakterie
(	(	kIx(	(
<g/>
agroinfekce	agroinfekce	k1gFnSc1	agroinfekce
<g/>
)	)	kIx)	)
a	a	k8xC	a
biolistické	biolistický	k2eAgNnSc1d1	biolistický
"	"	kIx"	"
<g/>
nastřelení	nastřelení	k1gNnSc1	nastřelení
<g/>
"	"	kIx"	"
DNA	dno	k1gNnPc4	dno
do	do	k7c2	do
buněčného	buněčný	k2eAgNnSc2d1	buněčné
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přímý	přímý	k2eAgInSc1d1	přímý
přenos	přenos	k1gInSc1	přenos
DNA	dno	k1gNnSc2	dno
do	do	k7c2	do
protoplastů	protoplast	k1gInPc2	protoplast
<g/>
,	,	kIx,	,
první	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
většinou	většinou	k6eAd1	většinou
jednodušší	jednoduchý	k2eAgMnSc1d2	jednodušší
a	a	k8xC	a
účinnější	účinný	k2eAgMnSc1d2	účinnější
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
přímý	přímý	k2eAgInSc1d1	přímý
přenos	přenos	k1gInSc1	přenos
i	i	k8xC	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
metody	metoda	k1gFnPc1	metoda
používají	používat	k5eAaImIp3nP	používat
jen	jen	k9	jen
ve	v	k7c6	v
speciálních	speciální	k2eAgInPc6d1	speciální
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
známá	známý	k2eAgFnSc1d1	známá
kukuřice	kukuřice	k1gFnSc1	kukuřice
BT11	BT11	k1gFnSc1	BT11
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
Syngenta	Syngento	k1gNnSc2	Syngento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
vzato	vzít	k5eAaPmNgNnS	vzít
<g/>
,	,	kIx,	,
volbu	volba	k1gFnSc4	volba
metody	metoda	k1gFnSc2	metoda
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
mimo	mimo	k7c4	mimo
dostupnosti	dostupnost	k1gFnPc4	dostupnost
dané	daný	k2eAgFnSc2d1	daná
metody	metoda	k1gFnSc2	metoda
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
transformované	transformovaný	k2eAgFnSc2d1	transformovaná
rostliny	rostlina	k1gFnSc2	rostlina
hlavně	hlavně	k9	hlavně
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yIgInSc3	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
rostlina	rostlina	k1gFnSc1	rostlina
transformována	transformován	k2eAgFnSc1d1	transformována
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každá	každý	k3xTgFnSc1	každý
transgenní	transgenní	k2eAgFnSc1d1	transgenní
rostlina	rostlina	k1gFnSc1	rostlina
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
komerční	komerční	k2eAgNnSc4d1	komerční
pěstování	pěstování	k1gNnSc4	pěstování
jako	jako	k8xS	jako
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
plodina	plodina	k1gFnSc1	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
transgenních	transgenní	k2eAgFnPc2d1	transgenní
rostlin	rostlina	k1gFnPc2	rostlina
vzniká	vznikat	k5eAaImIp3nS	vznikat
jen	jen	k9	jen
pro	pro	k7c4	pro
základní	základní	k2eAgInSc4d1	základní
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
je	být	k5eAaImIp3nS	být
zlikvidována	zlikvidován	k2eAgFnSc1d1	zlikvidována
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
transgenní	transgenní	k2eAgInPc4d1	transgenní
rostlinné	rostlinný	k2eAgInPc4d1	rostlinný
modely	model	k1gInPc4	model
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
použity	použít	k5eAaPmNgInP	použít
složitější	složitý	k2eAgInPc1d2	složitější
druhy	druh	k1gInPc1	druh
transformace	transformace	k1gFnSc2	transformace
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gMnPc2	on
mělo	mít	k5eAaImAgNnS	mít
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
transformaci	transformace	k1gFnSc6	transformace
pomocí	pomocí	k7c2	pomocí
bakterie	bakterie	k1gFnSc2	bakterie
Agrobacterium	Agrobacterium	k1gNnSc1	Agrobacterium
tumefaciens	tumefaciens	k1gInSc1	tumefaciens
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
přirozené	přirozený	k2eAgFnPc4d1	přirozená
schopnosti	schopnost	k1gFnPc4	schopnost
této	tento	k3xDgFnSc2	tento
patogenní	patogenní	k2eAgFnSc2d1	patogenní
bakterie	bakterie	k1gFnSc2	bakterie
vnášet	vnášet	k5eAaImF	vnášet
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
geny	gen	k1gInPc4	gen
z	z	k7c2	z
takzvaného	takzvaný	k2eAgInSc2d1	takzvaný
Ti-plazmidu	Tilazmid	k1gInSc2	Ti-plazmid
do	do	k7c2	do
genomu	genom	k1gInSc2	genom
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgInSc7d1	přirozený
důsledkem	důsledek	k1gInSc7	důsledek
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
inzerce	inzerce	k1gFnSc2	inzerce
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
genu	gen	k1gInSc2	gen
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
cizorodé	cizorodý	k2eAgInPc1d1	cizorodý
geny	gen	k1gInPc1	gen
přinutí	přinutit	k5eAaPmIp3nP	přinutit
rostlinu	rostlina	k1gFnSc4	rostlina
samu	sám	k3xTgFnSc4	sám
vytvořit	vytvořit	k5eAaPmF	vytvořit
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
infekce	infekce	k1gFnSc2	infekce
nádor	nádor	k1gInSc4	nádor
a	a	k8xC	a
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
tam	tam	k6eAd1	tam
speciální	speciální	k2eAgFnPc4d1	speciální
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
bakterie	bakterie	k1gFnPc1	bakterie
živí	živit	k5eAaImIp3nP	živit
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
napadené	napadený	k2eAgFnPc1d1	napadená
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
přirozenými	přirozený	k2eAgFnPc7d1	přirozená
transgenními	transgenní	k2eAgFnPc7d1	transgenní
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
hlavní	hlavní	k2eAgFnSc1d1	hlavní
metoda	metoda	k1gFnSc1	metoda
používaná	používaný	k2eAgFnSc1d1	používaná
k	k	k7c3	k
transformaci	transformace	k1gFnSc3	transformace
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
biolistická	biolistický	k2eAgFnSc1d1	biolistický
<g/>
.	.	kIx.	.
</s>
<s>
Požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
DNA	dna	k1gFnSc1	dna
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
vysráží	vysrážet	k5eAaPmIp3nS	vysrážet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nepatrných	nepatrný	k2eAgFnPc2d1	nepatrná
částeček	částečka	k1gFnPc2	částečka
zlata	zlato	k1gNnSc2	zlato
nebo	nebo	k8xC	nebo
wolframu	wolfram	k1gInSc2	wolfram
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jiného	jiný	k2eAgInSc2d1	jiný
těžkého	těžký	k2eAgInSc2d1	těžký
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tzv.	tzv.	kA	tzv.
projektily	projektil	k1gInPc1	projektil
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pod	pod	k7c7	pod
vysokým	vysoký	k2eAgInSc7d1	vysoký
tlakem	tlak	k1gInSc7	tlak
helia	helium	k1gNnSc2	helium
"	"	kIx"	"
<g/>
nastřelí	nastřelit	k5eAaPmIp3nS	nastřelit
<g/>
"	"	kIx"	"
do	do	k7c2	do
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
tkáně	tkáň	k1gFnSc2	tkáň
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
procentu	procent	k1gInSc6	procent
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
zasaženo	zasažen	k2eAgNnSc1d1	zasaženo
jádro	jádro	k1gNnSc1	jádro
a	a	k8xC	a
ve	v	k7c6	v
zlomku	zlomek	k1gInSc6	zlomek
těchto	tento	k3xDgFnPc2	tento
"	"	kIx"	"
<g/>
šťastných	šťastný	k2eAgInPc2d1	šťastný
<g/>
"	"	kIx"	"
zásahů	zásah	k1gInPc2	zásah
se	se	k3xPyFc4	se
během	během	k7c2	během
oprav	oprava	k1gFnPc2	oprava
poškození	poškození	k1gNnSc2	poškození
způsobených	způsobený	k2eAgFnPc2d1	způsobená
zlatým	zlatý	k2eAgInSc7d1	zlatý
projektilem	projektil	k1gInSc7	projektil
cizorodá	cizorodý	k2eAgFnSc1d1	cizorodá
DNA	dna	k1gFnSc1	dna
spojí	spojit	k5eAaPmIp3nS	spojit
s	s	k7c7	s
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
genomem	genom	k1gInSc7	genom
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vlastní	vlastní	k2eAgFnSc2d1	vlastní
transformace	transformace	k1gFnSc2	transformace
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přenosu	přenos	k1gInSc2	přenos
cizorodé	cizorodý	k2eAgNnSc4d1	cizorodé
DNA	dna	k1gFnSc1	dna
do	do	k7c2	do
genomu	genom	k1gInSc2	genom
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgMnPc3d1	další
<g/>
,	,	kIx,	,
neméně	málo	k6eNd2	málo
důležitým	důležitý	k2eAgInSc7d1	důležitý
předpokladem	předpoklad	k1gInSc7	předpoklad
vzniku	vznik	k1gInSc2	vznik
transgenní	transgenní	k2eAgFnSc2d1	transgenní
rostliny	rostlina	k1gFnSc2	rostlina
regenerační	regenerační	k2eAgInSc4d1	regenerační
protokol	protokol	k1gInSc4	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
vypěstovat	vypěstovat	k5eAaPmF	vypěstovat
úplnou	úplný	k2eAgFnSc4d1	úplná
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
plodnou	plodný	k2eAgFnSc4d1	plodná
rostlinu	rostlina	k1gFnSc4	rostlina
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
úspěšně	úspěšně	k6eAd1	úspěšně
transformací	transformace	k1gFnSc7	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
rostliny	rostlina	k1gFnPc4	rostlina
je	být	k5eAaImIp3nS	být
regenerace	regenerace	k1gFnSc1	regenerace
z	z	k7c2	z
jediné	jediný	k2eAgFnSc2d1	jediná
buňky	buňka	k1gFnSc2	buňka
na	na	k7c4	na
dospělou	dospělý	k2eAgFnSc4d1	dospělá
rostlinu	rostlina	k1gFnSc4	rostlina
snadná	snadný	k2eAgFnSc1d1	snadná
(	(	kIx(	(
<g/>
huseníček	huseníčko	k1gNnPc2	huseníčko
rolní	rolní	k2eAgInPc1d1	rolní
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
některé	některý	k3yIgNnSc4	některý
(	(	kIx(	(
<g/>
maniok	maniok	k1gInSc1	maniok
<g/>
)	)	kIx)	)
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
či	či	k8xC	či
zatím	zatím	k6eAd1	zatím
zcela	zcela	k6eAd1	zcela
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc4	problém
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
tropických	tropický	k2eAgFnPc2d1	tropická
plodin	plodina	k1gFnPc2	plodina
pro	pro	k7c4	pro
rozvojové	rozvojový	k2eAgFnPc4d1	rozvojová
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gInSc2	jejichž
vývoje	vývoj	k1gInSc2	vývoj
nikdo	nikdo	k3yNnSc1	nikdo
nechce	chtít	k5eNaImIp3nS	chtít
investovat	investovat	k5eAaBmF	investovat
moc	moc	k6eAd1	moc
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
nejběžnějšími	běžný	k2eAgFnPc7d3	nejběžnější
transgenními	transgenní	k2eAgFnPc7d1	transgenní
plodinami	plodina	k1gFnPc7	plodina
sója	sója	k1gFnSc1	sója
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
bavlník	bavlník	k1gInSc1	bavlník
a	a	k8xC	a
řepka	řepka	k1gFnSc1	řepka
olejná	olejný	k2eAgFnSc1d1	olejná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
geneticky	geneticky	k6eAd1	geneticky
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
plodinou	plodina	k1gFnSc7	plodina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
povoleno	povolit	k5eAaPmNgNnS	povolit
komerčně	komerčně	k6eAd1	komerčně
pěstovat	pěstovat	k5eAaImF	pěstovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
kukuřice	kukuřice	k1gFnSc1	kukuřice
firmy	firma	k1gFnSc2	firma
Monsanto	Monsanta	k1gFnSc5	Monsanta
typu	typ	k1gInSc2	typ
MON	MON	kA	MON
810	[number]	k4	810
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
odrůdy	odrůda	k1gFnSc2	odrůda
byla	být	k5eAaImAgFnS	být
vložena	vložit	k5eAaPmNgFnS	vložit
sekvence	sekvence	k1gFnSc1	sekvence
genu	gen	k1gInSc2	gen
cry	cry	k?	cry
z	z	k7c2	z
bakterie	bakterie	k1gFnSc2	bakterie
Bacillus	Bacillus	k1gInSc1	Bacillus
thuringiensis	thuringiensis	k1gInSc1	thuringiensis
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
produkci	produkce	k1gFnSc4	produkce
tzv.	tzv.	kA	tzv.
Bt-toxinu	Btoxin	k1gInSc2	Bt-toxin
(	(	kIx(	(
<g/>
δ	δ	k?	δ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bt-toxin	Btoxin	k1gInSc1	Bt-toxin
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgInSc1d1	jedovatý
pro	pro	k7c4	pro
určité	určitý	k2eAgInPc4d1	určitý
druhy	druh	k1gInPc4	druh
hmyzu	hmyz	k1gInSc2	hmyz
(	(	kIx(	(
<g/>
navozuje	navozovat	k5eAaImIp3nS	navozovat
perforaci	perforace	k1gFnSc4	perforace
jejich	jejich	k3xOp3gNnPc2	jejich
střev	střevo	k1gNnPc2	střevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnSc2	místo
pěstování	pěstování	k1gNnSc4	pěstování
v	v	k7c6	v
ČR	ČR	kA	ČR
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
organizace	organizace	k1gFnSc1	organizace
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
výměr	výměra	k1gFnPc2	výměra
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
oseté	osetý	k2eAgFnSc2d1	osetá
GM	GM	kA	GM
plodinami	plodina	k1gFnPc7	plodina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
osetá	osetý	k2eAgFnSc1d1	osetá
plocha	plocha	k1gFnSc1	plocha
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
v	v	k7c6	v
EU	EU	kA	EU
po	po	k7c6	po
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
poprvé	poprvé	k6eAd1	poprvé
výměra	výměra	k1gFnSc1	výměra
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
osetá	osetý	k2eAgFnSc1d1	osetá
GM	GM	kA	GM
plodinami	plodina	k1gFnPc7	plodina
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
8	[number]	k4	8
380	[number]	k4	380
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c4	na
6	[number]	k4	6
480	[number]	k4	480
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
až	až	k9	až
na	na	k7c4	na
přibližně	přibližně	k6eAd1	přibližně
4500	[number]	k4	4500
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
poklesu	pokles	k1gInSc2	pokles
je	být	k5eAaImIp3nS	být
administrativa	administrativa	k1gFnSc1	administrativa
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
pěstováním	pěstování	k1gNnSc7	pěstování
GMO	GMO	kA	GMO
<g/>
,	,	kIx,	,
potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
odbytem	odbyt	k1gInSc7	odbyt
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc1d2	vyšší
cena	cena	k1gFnSc1	cena
osiva	osiva	k1gFnSc1	osiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
nižší	nízký	k2eAgInSc1d2	nižší
výskyt	výskyt	k1gInSc1	výskyt
škůdce	škůdce	k1gMnSc2	škůdce
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
pěstovaní	pěstovaný	k2eAgMnPc1d1	pěstovaný
GM	GM	kA	GM
kukuřice	kukuřice	k1gFnSc2	kukuřice
nevyplatí	vyplatit	k5eNaPmIp3nS	vyplatit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
schválila	schválit	k5eAaPmAgFnS	schválit
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
pěstování	pěstování	k1gNnSc2	pěstování
geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
odrůdy	odrůda	k1gFnSc2	odrůda
brambory	brambora	k1gFnSc2	brambora
Amflora	Amflora	k1gFnSc1	Amflora
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
německý	německý	k2eAgInSc1d1	německý
chemický	chemický	k2eAgInSc1d1	chemický
koncern	koncern	k1gInSc1	koncern
BASF	BASF	kA	BASF
<g/>
.	.	kIx.	.
</s>
<s>
Modifikace	modifikace	k1gFnSc1	modifikace
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vyřazení	vyřazení	k1gNnSc6	vyřazení
syntézy	syntéza	k1gFnSc2	syntéza
amylózy	amylóza	k1gFnSc2	amylóza
<g/>
,	,	kIx,	,
škrob	škrob	k1gInSc4	škrob
v	v	k7c6	v
bramboře	brambora	k1gFnSc6	brambora
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tvořen	tvořit	k5eAaImNgInS	tvořit
pouze	pouze	k6eAd1	pouze
amylopektinem	amylopektin	k1gInSc7	amylopektin
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
při	při	k7c6	při
využití	využití	k1gNnSc6	využití
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
amylóza	amylóza	k1gFnSc1	amylóza
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kvality	kvalita	k1gFnSc2	kvalita
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
Amflora	Amflora	k1gFnSc1	Amflora
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
schváleno	schválit	k5eAaPmNgNnS	schválit
i	i	k9	i
její	její	k3xOp3gNnSc1	její
použití	použití	k1gNnSc1	použití
jako	jako	k8xS	jako
krmivo	krmivo	k1gNnSc1	krmivo
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
však	však	k9	však
být	být	k5eAaImF	být
podávána	podávat	k5eAaImNgFnS	podávat
jako	jako	k9	jako
potravina	potravina	k1gFnSc1	potravina
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kritici	kritik	k1gMnPc1	kritik
varují	varovat	k5eAaImIp3nP	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
genetickou	genetický	k2eAgFnSc7d1	genetická
manipulací	manipulace	k1gFnSc7	manipulace
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
rostliny	rostlina	k1gFnSc2	rostlina
vložen	vložit	k5eAaPmNgInS	vložit
také	také	k9	také
gen	gen	k1gInSc1	gen
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
některým	některý	k3yIgNnPc3	některý
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
větší	veliký	k2eAgFnSc3d2	veliký
rezistenci	rezistence	k1gFnSc3	rezistence
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumání	zkoumání	k1gNnSc1	zkoumání
evropských	evropský	k2eAgInPc2d1	evropský
úřadů	úřad	k1gInPc2	úřad
pro	pro	k7c4	pro
léčiva	léčivo	k1gNnPc4	léčivo
a	a	k8xC	a
pro	pro	k7c4	pro
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
potravin	potravina	k1gFnPc2	potravina
však	však	k9	však
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sporný	sporný	k2eAgInSc1d1	sporný
gen	gen	k1gInSc1	gen
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
běžně	běžně	k6eAd1	běžně
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
časopise	časopis	k1gInSc6	časopis
Food	Food	k1gMnSc1	Food
and	and	k?	and
Chemical	Chemical	k1gMnSc1	Chemical
Toxicology	Toxicolog	k1gMnPc7	Toxicolog
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
studie	studie	k1gFnSc1	studie
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yRgFnSc2	který
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
myší	myš	k1gFnPc2	myš
krmených	krmený	k2eAgFnPc6d1	krmená
geneticky	geneticky	k6eAd1	geneticky
upravenou	upravený	k2eAgFnSc7d1	upravená
kukuřicí	kukuřice	k1gFnSc7	kukuřice
NK	NK	kA	NK
603	[number]	k4	603
po	po	k7c6	po
17	[number]	k4	17
měsících	měsíc	k1gInPc6	měsíc
bylo	být	k5eAaImAgNnS	být
pětkrát	pětkrát	k6eAd1	pětkrát
víc	hodně	k6eAd2	hodně
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
jedinců	jedinec	k1gMnPc2	jedinec
než	než	k8xS	než
v	v	k7c6	v
kontrolních	kontrolní	k2eAgFnPc6d1	kontrolní
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
samiček	samička	k1gFnPc2	samička
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
rakovinou	rakovina	k1gFnSc7	rakovina
prsu	prs	k1gInSc2	prs
<g/>
,	,	kIx,	,
samečci	sameček	k1gMnPc1	sameček
často	často	k6eAd1	často
měli	mít	k5eAaImAgMnP	mít
nádory	nádor	k1gInPc4	nádor
kůže	kůže	k1gFnSc2	kůže
nebo	nebo	k8xC	nebo
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Studii	studie	k1gFnSc4	studie
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
prováděl	provádět	k5eAaImAgInS	provádět
tým	tým	k1gInSc1	tým
expertů	expert	k1gMnPc2	expert
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Gillese-Erica	Gillese-Ericus	k1gMnSc2	Gillese-Ericus
Seraliniho	Seralini	k1gMnSc2	Seralini
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
sledovali	sledovat	k5eAaImAgMnP	sledovat
200	[number]	k4	200
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
podávali	podávat	k5eAaImAgMnP	podávat
geneticky	geneticky	k6eAd1	geneticky
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
kukuřici	kukuřice	k1gFnSc4	kukuřice
NK	NK	kA	NK
603	[number]	k4	603
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
Monsanto	Monsanta	k1gFnSc5	Monsanta
nebo	nebo	k8xC	nebo
běžnou	běžný	k2eAgFnSc4d1	běžná
kukuřici	kukuřice	k1gFnSc4	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Seralinimu	Seralinim	k1gInSc2	Seralinim
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
prokázáno	prokázán	k2eAgNnSc1d1	prokázáno
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
experimentu	experiment	k1gInSc2	experiment
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
studie	studie	k1gFnSc1	studie
byla	být	k5eAaImAgFnS	být
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c4	po
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
vědeckou	vědecký	k2eAgFnSc7d1	vědecká
obcí	obec	k1gFnSc7	obec
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
jako	jako	k9	jako
zmanipulovaná	zmanipulovaný	k2eAgFnSc1d1	zmanipulovaná
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
publikována	publikovat	k5eAaBmNgFnS	publikovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Transgenní	transgenní	k2eAgNnPc1d1	transgenní
zvířata	zvíře	k1gNnPc1	zvíře
–	–	k?	–
geny	gen	k1gInPc1	gen
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
do	do	k7c2	do
zvířat	zvíře	k1gNnPc2	zvíře
vloženy	vložit	k5eAaPmNgFnP	vložit
transfekcí	transfekce	k1gFnSc7	transfekce
<g/>
.	.	kIx.	.
</s>
<s>
Metody	metoda	k1gFnPc1	metoda
transfekce	transfekce	k1gFnSc2	transfekce
<g/>
:	:	kIx,	:
lipofekce	lipofekce	k1gFnSc1	lipofekce
(	(	kIx(	(
<g/>
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
micel	micela	k1gFnPc2	micela
z	z	k7c2	z
lipidů	lipid	k1gInPc2	lipid
<g/>
)	)	kIx)	)
transfekce	transfekce	k1gFnSc2	transfekce
pomocí	pomocí	k7c2	pomocí
retroviru	retrovir	k1gInSc2	retrovir
mikroinjekce	mikroinjekce	k1gFnSc2	mikroinjekce
embryonální	embryonální	k2eAgFnPc4d1	embryonální
kmenové	kmenový	k2eAgFnPc4d1	kmenová
buňky	buňka	k1gFnPc4	buňka
Všechny	všechen	k3xTgFnPc1	všechen
metody	metoda	k1gFnPc1	metoda
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
účinnost	účinnost	k1gFnSc4	účinnost
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.5	.5	k4	.5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
selekce	selekce	k1gFnSc1	selekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
zatím	zatím	k6eAd1	zatím
neuznaná	uznaný	k2eNgFnSc1d1	neuznaná
čínská	čínský	k2eAgFnSc1d1	čínská
GM	GM	kA	GM
rýže	rýže	k1gFnSc1	rýže
Bt	Bt	k1gFnSc1	Bt
<g/>
63	[number]	k4	63
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
odhalena	odhalen	k2eAgFnSc1d1	odhalena
při	při	k7c6	při
náhodných	náhodný	k2eAgFnPc6d1	náhodná
kontrolách	kontrola	k1gFnPc6	kontrola
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2006	[number]	k4	2006
a	a	k8xC	a
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
požádala	požádat	k5eAaPmAgFnS	požádat
Čínu	Čína	k1gFnSc4	Čína
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
opatření	opatření	k1gNnSc2	opatření
k	k	k7c3	k
zamezení	zamezení	k1gNnSc3	zamezení
nelegálního	legální	k2eNgInSc2d1	nelegální
exportu	export	k1gInSc2	export
této	tento	k3xDgFnSc2	tento
GM	GM	kA	GM
plodiny	plodina	k1gFnPc1	plodina
do	do	k7c2	do
EU	EU	kA	EU
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
tohoto	tento	k3xDgInSc2	tento
GMO	GMO	kA	GMO
nebyla	být	k5eNaImAgFnS	být
prověřena	prověřen	k2eAgFnSc1d1	prověřena
a	a	k8xC	a
GM	GM	kA	GM
rýže	rýže	k1gFnPc4	rýže
nebyla	být	k5eNaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Čína	Čína	k1gFnSc1	Čína
export	export	k1gInSc1	export
ilegální	ilegální	k2eAgFnSc2d1	ilegální
GM	GM	kA	GM
rýže	rýže	k1gFnSc2	rýže
nezastavila	zastavit	k5eNaPmAgFnS	zastavit
<g/>
,	,	kIx,	,
schválila	schválit	k5eAaPmAgFnS	schválit
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
opatření	opatření	k1gNnSc2	opatření
s	s	k7c7	s
platností	platnost	k1gFnSc7	platnost
od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
evropský	evropský	k2eAgInSc1d1	evropský
výbor	výbor	k1gInSc1	výbor
proto	proto	k8xC	proto
potraviny	potravina	k1gFnSc2	potravina
a	a	k8xC	a
krmiva	krmivo	k1gNnSc2	krmivo
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
na	na	k7c4	na
čínskou	čínský	k2eAgFnSc4d1	čínská
rýži	rýže	k1gFnSc4	rýže
a	a	k8xC	a
produkty	produkt	k1gInPc4	produkt
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyrobené	vyrobený	k2eAgInPc4d1	vyrobený
certifikát	certifikát	k1gInSc4	certifikát
<g/>
,	,	kIx,	,
že	že	k8xS	že
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
ilegální	ilegální	k2eAgFnSc4d1	ilegální
GM	GM	kA	GM
plodinu	plodina	k1gFnSc4	plodina
Bt	Bt	k1gFnSc1	Bt
<g/>
63	[number]	k4	63
<g/>
.	.	kIx.	.
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
komise	komise	k1gFnSc1	komise
zavedla	zavést	k5eAaPmAgFnS	zavést
tato	tento	k3xDgNnPc4	tento
opatření	opatření	k1gNnPc4	opatření
na	na	k7c4	na
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
poté	poté	k6eAd1	poté
bude	být	k5eAaImBp3nS	být
účinnost	účinnost	k1gFnSc1	účinnost
opatření	opatření	k1gNnSc2	opatření
vyhodnocena	vyhodnocen	k2eAgFnSc1d1	vyhodnocena
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
inspekce	inspekce	k1gFnSc1	inspekce
informovala	informovat	k5eAaBmAgFnS	informovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trh	trh	k1gInSc4	trh
proniklo	proniknout	k5eAaPmAgNnS	proniknout
asi	asi	k9	asi
38	[number]	k4	38
tisíc	tisíc	k4xCgInSc4	tisíc
balení	balení	k1gNnPc2	balení
rýže	rýže	k1gFnSc2	rýže
obsahující	obsahující	k2eAgFnSc4d1	obsahující
neschválenou	schválený	k2eNgFnSc4d1	neschválená
geneticky	geneticky	k6eAd1	geneticky
modifikovanou	modifikovaný	k2eAgFnSc4d1	modifikovaná
rýži	rýže	k1gFnSc4	rýže
LL	LL	kA	LL
601	[number]	k4	601
firmy	firma	k1gFnSc2	firma
Bayer	Bayer	k1gMnSc1	Bayer
Crop	Crop	k1gMnSc1	Crop
Science	Science	k1gFnSc1	Science
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výrobky	výrobek	k1gInPc4	výrobek
Euroshopper	Euroshoppra	k1gFnPc2	Euroshoppra
Rýže	rýže	k1gFnSc1	rýže
dlouhozrnná	dlouhozrnný	k2eAgFnSc1d1	dlouhozrnná
–	–	k?	–
varné	varný	k2eAgInPc4d1	varný
sáčky	sáček	k1gInPc4	sáček
(	(	kIx(	(
<g/>
balení	balení	k1gNnSc2	balení
400	[number]	k4	400
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
šarže	šarže	k1gFnSc1	šarže
2101082	[number]	k4	2101082
<g/>
,	,	kIx,	,
DMT	DMT	kA	DMT
210709	[number]	k4	210709
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rýže	rýže	k1gFnSc1	rýže
dlouhozrnná	dlouhozrnný	k2eAgFnSc1d1	dlouhozrnná
–	–	k?	–
varné	varný	k2eAgInPc4d1	varný
sáčky	sáček	k1gInPc4	sáček
(	(	kIx(	(
<g/>
balení	balení	k1gNnSc2	balení
480	[number]	k4	480
<g/>
g	g	kA	g
<g/>
,	,	kIx,	,
šarže	šarže	k1gFnSc1	šarže
2201081	[number]	k4	2201081
<g/>
,	,	kIx,	,
DMT	DMT	kA	DMT
220709	[number]	k4	220709
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inspekce	inspekce	k1gFnSc1	inspekce
zakázala	zakázat	k5eAaPmAgFnS	zakázat
prodej	prodej	k1gInSc4	prodej
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
Podravka-Lagris	Podravka-Lagris	k1gFnSc1	Podravka-Lagris
a.s.	a.s.	k?	a.s.
musela	muset	k5eAaImAgFnS	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
výrobek	výrobek	k1gInSc4	výrobek
z	z	k7c2	z
obchodní	obchodní	k2eAgFnSc2d1	obchodní
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Geneticky	geneticky	k6eAd1	geneticky
modifikovaná	modifikovaný	k2eAgFnSc1d1	modifikovaná
rýže	rýže	k1gFnSc1	rýže
LL	LL	kA	LL
601	[number]	k4	601
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
v	v	k7c6	v
EU	EU	kA	EU
povolena	povolit	k5eAaPmNgFnS	povolit
ani	ani	k8xC	ani
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
ani	ani	k8xC	ani
pro	pro	k7c4	pro
zvířecí	zvířecí	k2eAgNnPc4d1	zvířecí
krmiva	krmivo	k1gNnPc4	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
minulosti	minulost	k1gFnPc4	minulost
podobným	podobný	k2eAgInPc3d1	podobný
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
ilegální	ilegální	k2eAgFnSc7d1	ilegální
kontaminací	kontaminace	k1gFnSc7	kontaminace
GM	GM	kA	GM
rýží	rýže	k1gFnSc7	rýže
čelila	čelit	k5eAaImAgFnS	čelit
řada	řada	k1gFnSc1	řada
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
nebo	nebo	k8xC	nebo
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobných	podobný	k2eAgFnPc2d1	podobná
událostí	událost	k1gFnPc2	událost
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
přibývat	přibývat	k5eAaImF	přibývat
s	s	k7c7	s
přibývajícím	přibývající	k2eAgNnSc7d1	přibývající
množstvím	množství	k1gNnSc7	množství
transgenních	transgenní	k2eAgFnPc2d1	transgenní
odrůd	odrůda	k1gFnPc2	odrůda
legálně	legálně	k6eAd1	legálně
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
mimo	mimo	k7c4	mimo
Evropskou	evropský	k2eAgFnSc4d1	Evropská
unii	unie	k1gFnSc4	unie
a	a	k8xC	a
v	v	k7c6	v
EU	EU	kA	EU
zatím	zatím	k6eAd1	zatím
neschválených	schválený	k2eNgNnPc6d1	neschválené
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
schvalování	schvalování	k1gNnSc2	schvalování
nových	nový	k2eAgInPc2d1	nový
GMO	GMO	kA	GMO
odrůd	odrůda	k1gFnPc2	odrůda
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
až	až	k6eAd1	až
desetiletí	desetiletí	k1gNnSc4	desetiletí
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
zemích	zem	k1gFnPc6	zem
exportujících	exportující	k2eAgInPc2d1	exportující
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
komodity	komodita	k1gFnPc1	komodita
<g/>
.	.	kIx.	.
</s>
<s>
Přibývající	přibývající	k2eAgNnSc1d1	přibývající
množství	množství	k1gNnSc1	množství
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
odrůd	odrůda	k1gFnPc2	odrůda
bude	být	k5eAaImBp3nS	být
rovněž	rovněž	k9	rovněž
komplikovat	komplikovat	k5eAaBmF	komplikovat
jejich	jejich	k3xOp3gNnSc4	jejich
testování	testování	k1gNnSc4	testování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přítomnost	přítomnost	k1gFnSc1	přítomnost
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
transgenů	transgen	k1gInPc2	transgen
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
odrůdy	odrůda	k1gFnSc2	odrůda
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
testována	testovat	k5eAaImNgFnS	testovat
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
<s>
Odrůdy	odrůda	k1gFnPc1	odrůda
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
EU	EU	kA	EU
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
již	již	k9	již
v	v	k7c6	v
EU	EU	kA	EU
schváleny	schválit	k5eAaPmNgInP	schválit
ať	ať	k9	ať
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
prodej	prodej	k1gInSc4	prodej
nebo	nebo	k8xC	nebo
i	i	k9	i
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
možno	možno	k6eAd1	možno
vyhledat	vyhledat	k5eAaPmF	vyhledat
v	v	k7c6	v
databází	databáze	k1gFnPc2	databáze
GMO	GMO	kA	GMO
compass	compass	k1gInSc1	compass
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Databázi	databáze	k1gFnSc3	databáze
GMO	GMO	kA	GMO
odrůd	odrůda	k1gFnPc2	odrůda
schválených	schválený	k2eAgFnPc2d1	schválená
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
spravuje	spravovat	k5eAaImIp3nS	spravovat
kanadská	kanadský	k2eAgFnSc1d1	kanadská
firma	firma	k1gFnSc1	firma
AgBios	AgBiosa	k1gFnPc2	AgBiosa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
úniku	únik	k1gInSc2	únik
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
depeší	depeše	k1gFnPc2	depeše
<g/>
,	,	kIx,	,
zveřejněných	zveřejněný	k2eAgInPc2d1	zveřejněný
na	na	k7c4	na
WikiLeaks	WikiLeaks	k1gInSc4	WikiLeaks
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnPc1d1	americká
uvažovaly	uvažovat	k5eAaImAgFnP	uvažovat
o	o	k7c6	o
pomstě	pomsta	k1gFnSc6	pomsta
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
za	za	k7c4	za
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
geneticky	geneticky	k6eAd1	geneticky
modifikovaným	modifikovaný	k2eAgFnPc3d1	modifikovaná
potravinám	potravina	k1gFnPc3	potravina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
bylo	být	k5eAaImAgNnS	být
vypracování	vypracování	k1gNnSc1	vypracování
seznamu	seznam	k1gInSc2	seznam
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
se	se	k3xPyFc4	se
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
vyvolat	vyvolat	k5eAaPmF	vyvolat
"	"	kIx"	"
<g/>
určité	určitý	k2eAgFnPc4d1	určitá
obtíže	obtíž	k1gFnPc4	obtíž
<g/>
"	"	kIx"	"
v	v	k7c6	v
EU	EU	kA	EU
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
paradigmatu	paradigma	k1gNnSc2	paradigma
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
viny	vina	k1gFnSc2	vina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podpořit	podpořit	k5eAaPmF	podpořit
tím	ten	k3xDgNnSc7	ten
hlasy	hlas	k1gInPc4	hlas
podporující	podporující	k2eAgFnSc4d1	podporující
biotechnologii	biotechnologie	k1gFnSc4	biotechnologie
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
"	"	kIx"	"
<g/>
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
nejhorší	zlý	k2eAgMnPc4d3	nejhorší
viníky	viník	k1gMnPc4	viník
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
uváděna	uváděn	k2eAgFnSc1d1	uváděna
je	být	k5eAaImIp3nS	být
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
dovoz	dovoz	k1gInSc4	dovoz
geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnSc2d1	modifikovaná
kukuřice	kukuřice	k1gFnSc2	kukuřice
patentované	patentovaný	k2eAgFnSc2d1	patentovaná
nadnárodní	nadnárodní	k2eAgFnSc2d1	nadnárodní
korporací	korporace	k1gFnPc2	korporace
Monsanto	Monsanta	k1gFnSc5	Monsanta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
msta	msta	k1gFnSc1	msta
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
proveditelná	proveditelný	k2eAgFnSc1d1	proveditelná
po	po	k7c4	po
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
období	období	k1gNnSc4	období
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
"	"	kIx"	"
<g/>
nemůžeme	moct	k5eNaImIp1nP	moct
očekávat	očekávat	k5eAaImF	očekávat
rané	raný	k2eAgNnSc4d1	rané
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
depeše	depeše	k1gFnSc2	depeše
se	se	k3xPyFc4	se
americká	americký	k2eAgFnSc1d1	americká
strana	strana	k1gFnSc1	strana
snažila	snažit	k5eAaImAgFnS	snažit
bojovat	bojovat	k5eAaImF	bojovat
i	i	k9	i
proti	proti	k7c3	proti
odporu	odpor	k1gInSc3	odpor
katolických	katolický	k2eAgMnPc2d1	katolický
biskupů	biskup	k1gMnPc2	biskup
(	(	kIx(	(
<g/>
především	především	k9	především
rozvojových	rozvojový	k2eAgFnPc2d1	rozvojová
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
GMO	GMO	kA	GMO
též	též	k6eAd1	též
odporují	odporovat	k5eAaImIp3nP	odporovat
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zatlačila	zatlačit	k5eAaPmAgFnS	zatlačit
i	i	k9	i
na	na	k7c4	na
papeže	papež	k1gMnSc4	papež
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
pro	pro	k7c4	pro
geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnPc4d1	modifikovaná
plodiny	plodina	k1gFnPc4	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
zainteresovaný	zainteresovaný	k2eAgInSc1d1	zainteresovaný
v	v	k7c6	v
neziskové	ziskový	k2eNgFnSc6d1	nezisková
organizaci	organizace	k1gFnSc6	organizace
Earth	Earth	k1gMnSc1	Earth
Open	Open	k1gMnSc1	Open
Source	Source	k1gMnSc1	Source
shrnul	shrnout	k5eAaPmAgMnS	shrnout
některé	některý	k3yIgInPc4	některý
dostupné	dostupný	k2eAgInPc4d1	dostupný
zdroje	zdroj	k1gInPc4	zdroj
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
přínosů	přínos	k1gInPc2	přínos
(	(	kIx(	(
<g/>
avizované	avizovaný	k2eAgFnPc4d1	avizovaná
výrobci	výrobce	k1gMnPc7	výrobce
<g/>
,	,	kIx,	,
zastánci	zastánce	k1gMnPc7	zastánce
a	a	k8xC	a
propagátory	propagátor	k1gMnPc7	propagátor
GMO	GMO	kA	GMO
<g/>
)	)	kIx)	)
a	a	k8xC	a
dopadů	dopad	k1gInPc2	dopad
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgInPc2d1	modifikovaný
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
GM	GM	kA	GM
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
zpráva	zpráva	k1gFnSc1	zpráva
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
značně	značně	k6eAd1	značně
tendenční	tendenční	k2eAgFnSc1d1	tendenční
<g/>
,	,	kIx,	,
a	a	k8xC	a
mimo	mimo	k7c4	mimo
ověřených	ověřený	k2eAgInPc2d1	ověřený
vědeckých	vědecký	k2eAgInPc2d1	vědecký
zdrojů	zdroj	k1gInPc2	zdroj
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
například	například	k6eAd1	například
televizní	televizní	k2eAgFnPc4d1	televizní
reportáže	reportáž	k1gFnPc4	reportáž
nebo	nebo	k8xC	nebo
novinové	novinový	k2eAgInPc4d1	novinový
články	článek	k1gInPc4	článek
<g/>
.	.	kIx.	.
</s>
<s>
Geneticky	geneticky	k6eAd1	geneticky
modifikované	modifikovaný	k2eAgFnPc1d1	modifikovaná
plodiny	plodina	k1gFnPc1	plodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
insekticid	insekticid	k1gInSc4	insekticid
δ	δ	k?	δ
(	(	kIx(	(
<g/>
Cry	Cry	k1gMnSc1	Cry
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
stříkány	stříkán	k2eAgInPc1d1	stříkán
dodatečnými	dodatečný	k2eAgInPc7d1	dodatečný
insekticidy	insekticid	k1gInPc7	insekticid
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
oproti	oproti	k7c3	oproti
nemodifikovaným	modifikovaný	k2eNgFnPc3d1	nemodifikovaná
plodinám	plodina	k1gFnPc3	plodina
postřikovaným	postřikovaný	k2eAgFnPc3d1	postřikovaný
insekticidy	insekticid	k1gInPc7	insekticid
méně	málo	k6eAd2	málo
hmyzích	hmyzí	k2eAgMnPc2d1	hmyzí
škůdců	škůdce	k1gMnPc2	škůdce
i	i	k8xC	i
predátorů	predátor	k1gMnPc2	predátor
a	a	k8xC	a
překmity	překmit	k1gInPc1	překmit
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gInPc7	jejich
stavy	stav	k1gInPc7	stav
nezasahují	zasahovat	k5eNaImIp3nP	zasahovat
tolik	tolik	k6eAd1	tolik
do	do	k7c2	do
jejich	jejich	k3xOp3gFnSc2	jejich
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
agrárního	agrární	k2eAgMnSc2d1	agrární
analytika	analytik	k1gMnSc2	analytik
Petra	Petr	k1gMnSc2	Petr
Havla	Havel	k1gMnSc2	Havel
se	se	k3xPyFc4	se
za	za	k7c4	za
20	[number]	k4	20
let	léto	k1gNnPc2	léto
pěstování	pěstování	k1gNnSc2	pěstování
GM	GM	kA	GM
plodin	plodina	k1gFnPc2	plodina
na	na	k7c6	na
plochách	plocha	k1gFnPc6	plocha
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
jsou	být	k5eAaImIp3nP	být
GM	GM	kA	GM
plodiny	plodina	k1gFnPc4	plodina
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
<g/>
,	,	kIx,	,
snížila	snížit	k5eAaPmAgFnS	snížit
spotřeba	spotřeba	k1gFnSc1	spotřeba
pesticidů	pesticid	k1gInPc2	pesticid
o	o	k7c4	o
37	[number]	k4	37
%	%	kIx~	%
a	a	k8xC	a
výnosy	výnos	k1gInPc1	výnos
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
plodin	plodina	k1gFnPc2	plodina
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
o	o	k7c4	o
22	[number]	k4	22
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Hodnoty	hodnota	k1gFnPc1	hodnota
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
metaanalýzy	metaanalýza	k1gFnSc2	metaanalýza
147	[number]	k4	147
odborných	odborný	k2eAgNnPc2d1	odborné
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Metaanalýza	Metaanalýza	k1gFnSc1	Metaanalýza
prokazuje	prokazovat	k5eAaImIp3nS	prokazovat
přínosnost	přínosnost	k1gFnSc4	přínosnost
geneticky	geneticky	k6eAd1	geneticky
upravených	upravený	k2eAgFnPc2d1	upravená
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Zákaz	zákaz	k1gInSc1	zákaz
GMO	GMO	kA	GMO
plodin	plodina	k1gFnPc2	plodina
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Federico	Federico	k1gMnSc1	Federico
Infascelli	Infascell	k1gMnSc3	Infascell
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
provedl	provést	k5eAaPmAgMnS	provést
vědecký	vědecký	k2eAgInSc4d1	vědecký
podvod	podvod	k1gInSc4	podvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
prokázal	prokázat	k5eAaPmAgInS	prokázat
škodlivost	škodlivost	k1gFnSc4	škodlivost
GMO	GMO	kA	GMO
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
nositelů	nositel	k1gMnPc2	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
Greenpeace	Greenpeace	k1gFnSc1	Greenpeace
za	za	k7c4	za
postoj	postoj	k1gInSc4	postoj
proti	proti	k7c3	proti
GMO	GMO	kA	GMO
<g/>
.	.	kIx.	.
</s>
