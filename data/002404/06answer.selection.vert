<s>
První	první	k4xOgFnSc1	první
historicky	historicky	k6eAd1	historicky
doložená	doložený	k2eAgFnSc1d1	doložená
epidemie	epidemie	k1gFnSc1	epidemie
dýmějového	dýmějový	k2eAgInSc2d1	dýmějový
moru	mor	k1gInSc2	mor
propukla	propuknout	k5eAaPmAgFnS	propuknout
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
Justiniána	Justinián	k1gMnSc2	Justinián
I.	I.	kA	I.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
541	[number]	k4	541
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
největším	veliký	k2eAgNnSc6d3	veliký
evropském	evropský	k2eAgNnSc6d1	Evropské
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
