<s>
Death	Death	k1gInSc1	Death
Note	Not	k1gFnSc2	Not
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Zápisník	zápisník	k1gInSc1	zápisník
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
デ	デ	k?	デ
<g/>
,	,	kIx,	,
Desu	Des	k2eAgFnSc4d1	Des
Nóto	nóta	k1gFnSc5	nóta
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgNnPc4d1	Japonské
manga	mango	k1gNnPc4	mango
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Cugumi	Cugu	k1gFnPc7	Cugu
Óba	Óba	k1gFnPc4	Óba
a	a	k8xC	a
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Takeši	Takech	k1gMnPc1	Takech
Obata	Obat	k1gInSc2	Obat
(	(	kIx(	(
<g/>
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
mangy	mango	k1gNnPc7	mango
Hikaru	Hikar	k1gInSc2	Hikar
no	no	k9	no
go	go	k?	go
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gInSc4	Šúkan
šónen	šónen	k2eAgInSc1d1	šónen
Jump	Jump	k1gInSc1	Jump
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
vycházet	vycházet	k5eAaImF	vycházet
jako	jako	k9	jako
tankóbon	tankóbon	k1gInSc4	tankóbon
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
12	[number]	k4	12
díly	dílo	k1gNnPc7	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
37	[number]	k4	37
<g/>
dílné	dílný	k2eAgNnSc4d1	dílné
anime	animat	k5eAaPmIp3nS	animat
zpracování	zpracování	k1gNnSc4	zpracování
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
(	(	kIx(	(
<g/>
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
další	další	k2eAgInSc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
dva	dva	k4xCgInPc4	dva
anime	animat	k5eAaPmIp3nS	animat
filmy	film	k1gInPc1	film
a	a	k8xC	a
několik	několik	k4yIc1	několik
her	hra	k1gFnPc2	hra
pro	pro	k7c4	pro
Nintendo	Nintendo	k1gNnSc4	Nintendo
DS	DS	kA	DS
konzoli	konzoli	k6eAd1	konzoli
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
se	se	k3xPyFc4	se
manga	mango	k1gNnSc2	mango
několikrát	několikrát	k6eAd1	několikrát
umístila	umístit	k5eAaPmAgFnS	umístit
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejprodávanějších	prodávaný	k2eAgInPc2d3	nejprodávanější
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
sleduje	sledovat	k5eAaImIp3nS	sledovat
středoškolského	středoškolský	k2eAgMnSc4d1	středoškolský
studenta	student	k1gMnSc4	student
Lighta	Light	k1gMnSc4	Light
Jagamiho	Jagami	k1gMnSc4	Jagami
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
najde	najít	k5eAaPmIp3nS	najít
tajemný	tajemný	k2eAgInSc1d1	tajemný
zápisník	zápisník	k1gInSc1	zápisník
s	s	k7c7	s
nadpřirozenou	nadpřirozený	k2eAgFnSc7d1	nadpřirozená
mocí	moc	k1gFnSc7	moc
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
deník	deník	k1gInSc1	deník
původně	původně	k6eAd1	původně
patřil	patřit	k5eAaImAgInS	patřit
šinigamimu	šinigamima	k1gFnSc4	šinigamima
(	(	kIx(	(
<g/>
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
mangy	mango	k1gNnPc7	mango
od	od	k7c2	od
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
CREW	CREW	kA	CREW
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc4d1	používán
termín	termín	k1gInSc4	termín
smrtonoš	smrtonoš	k1gMnSc1	smrtonoš
<g/>
)	)	kIx)	)
Rjúkovi	Rjúek	k1gMnSc6	Rjúek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jej	on	k3xPp3gNnSc4	on
záměrně	záměrně	k6eAd1	záměrně
upustil	upustit	k5eAaPmAgMnS	upustit
do	do	k7c2	do
světa	svět	k1gInSc2	svět
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Majitel	majitel	k1gMnSc1	majitel
zápisníku	zápisník	k1gInSc2	zápisník
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
zabít	zabít	k5eAaPmF	zabít
jakéhokoliv	jakýkoliv	k3yIgMnSc4	jakýkoliv
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zná	znát	k5eAaImIp3nS	znát
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
tvář	tvář	k1gFnSc4	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gInSc1	Light
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
očistit	očistit	k5eAaPmF	očistit
svět	svět	k1gInSc4	svět
od	od	k7c2	od
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
stvořit	stvořit	k5eAaPmF	stvořit
nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
bude	být	k5eAaImBp3nS	být
vládcem	vládce	k1gMnSc7	vládce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
začne	začít	k5eAaPmIp3nS	začít
zabíjet	zabíjet	k5eAaImF	zabíjet
zločince	zločinec	k1gMnSc4	zločinec
a	a	k8xC	a
potírat	potírat	k5eAaImF	potírat
veškerou	veškerý	k3xTgFnSc4	veškerý
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnosti	činnost	k1gFnSc2	činnost
si	se	k3xPyFc3	se
však	však	k9	však
brzo	brzo	k6eAd1	brzo
všimne	všimnout	k5eAaPmIp3nS	všimnout
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
detektiv	detektiv	k1gMnSc1	detektiv
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
L.	L.	kA	L.
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
příběhu	příběh	k1gInSc2	příběh
<g/>
,	,	kIx,	,
Light	Light	k2eAgInSc1d1	Light
Jagami	Jaga	k1gFnPc7	Jaga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
inteligentní	inteligentní	k2eAgMnSc1d1	inteligentní
a	a	k8xC	a
nadaný	nadaný	k2eAgMnSc1d1	nadaný
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
student	student	k1gMnSc1	student
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gMnSc1	Light
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
našel	najít	k5eAaPmAgMnS	najít
na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
školního	školní	k2eAgInSc2d1	školní
pozemku	pozemek	k1gInSc2	pozemek
černý	černý	k2eAgInSc1d1	černý
sešit	sešit	k1gInSc1	sešit
<g/>
,	,	kIx,	,
označený	označený	k2eAgInSc1d1	označený
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Zápisník	zápisník	k1gInSc1	zápisník
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Death	Death	k1gInSc1	Death
Note	Not	k1gInPc1	Not
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
do	do	k7c2	do
světa	svět	k1gInSc2	svět
lidí	člověk	k1gMnPc2	člověk
úmyslně	úmyslně	k6eAd1	úmyslně
upustil	upustit	k5eAaPmAgMnS	upustit
šinigami	šiniga	k1gFnPc7	šiniga
(	(	kIx(	(
<g/>
v	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
českém	český	k2eAgInSc6d1	český
překladu	překlad	k1gInSc6	překlad
mangy	mango	k1gNnPc7	mango
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc1d1	používán
termín	termín	k1gInSc1	termín
smrtonoš	smrtonoš	k1gMnSc1	smrtonoš
<g/>
)	)	kIx)	)
jménem	jméno	k1gNnSc7	jméno
Rjúk	Rjúk	k1gMnSc1	Rjúk
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gInSc1	Light
zjistí	zjistit	k5eAaPmIp3nS	zjistit
(	(	kIx(	(
<g/>
z	z	k7c2	z
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
v	v	k7c6	v
zápisníku	zápisník	k1gInSc6	zápisník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
majitel	majitel	k1gMnSc1	majitel
tohoto	tento	k3xDgInSc2	tento
sešitu	sešit	k1gInSc2	sešit
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zabít	zabít	k5eAaPmF	zabít
jakéhokoliv	jakýkoliv	k3yIgMnSc4	jakýkoliv
člověka	člověk	k1gMnSc4	člověk
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zná	znát	k5eAaImIp3nS	znát
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
obličej	obličej	k1gInSc4	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvotní	prvotní	k2eAgFnSc6d1	prvotní
nedůvěře	nedůvěra	k1gFnSc6	nedůvěra
se	se	k3xPyFc4	se
Light	Light	k1gMnSc1	Light
přesvědčí	přesvědčit	k5eAaPmIp3nS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zápisník	zápisník	k1gInSc1	zápisník
skutečně	skutečně	k6eAd1	skutečně
funguje	fungovat	k5eAaImIp3nS	fungovat
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
stvořit	stvořit	k5eAaPmF	stvořit
"	"	kIx"	"
<g/>
nový	nový	k2eAgInSc4d1	nový
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
neexistuje	existovat	k5eNaImIp3nS	existovat
kriminalita	kriminalita	k1gFnSc1	kriminalita
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
zločinci	zločinec	k1gMnPc1	zločinec
jsou	být	k5eAaImIp3nP	být
okamžitě	okamžitě	k6eAd1	okamžitě
potrestáni	potrestat	k5eAaPmNgMnP	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gMnSc1	Light
proto	proto	k8xC	proto
začíná	začínat	k5eAaImIp3nS	začínat
do	do	k7c2	do
zápisníku	zápisník	k1gInSc2	zápisník
psát	psát	k5eAaImF	psát
jména	jméno	k1gNnSc2	jméno
všech	všecek	k3xTgMnPc2	všecek
těžkých	těžký	k2eAgMnPc2d1	těžký
zločinců	zločinec	k1gMnPc2	zločinec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
ohlášeni	ohlášen	k2eAgMnPc1d1	ohlášen
v	v	k7c6	v
televizních	televizní	k2eAgFnPc6d1	televizní
zprávách	zpráva	k1gFnPc6	zpráva
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
pak	pak	k6eAd1	pak
okamžitě	okamžitě	k6eAd1	okamžitě
zemřou	zemřít	k5eAaPmIp3nP	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgNnSc4d1	srdeční
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
Lightovi	Light	k1gMnSc3	Light
ukáže	ukázat	k5eAaPmIp3nS	ukázat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
majitel	majitel	k1gMnSc1	majitel
zápisníku	zápisník	k1gInSc2	zápisník
–	–	k?	–
šinigami	šiniga	k1gFnPc7	šiniga
Rjúk	Rjúka	k1gFnPc2	Rjúka
–	–	k?	–
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
zájmem	zájem	k1gInSc7	zájem
sleduje	sledovat	k5eAaImIp3nS	sledovat
každý	každý	k3xTgInSc4	každý
jeho	on	k3xPp3gInSc4	on
krok	krok	k1gInSc4	krok
za	za	k7c7	za
velkolepým	velkolepý	k2eAgInSc7d1	velkolepý
cílem	cíl	k1gInSc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ale	ale	k8xC	ale
nezvykle	zvykle	k6eNd1	zvykle
vysoký	vysoký	k2eAgInSc1d1	vysoký
počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
zločinců	zločinec	k1gMnPc2	zločinec
začne	začít	k5eAaPmIp3nS	začít
řešit	řešit	k5eAaImF	řešit
policie	policie	k1gFnSc2	policie
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
vrchní	vrchní	k2eAgMnSc1d1	vrchní
policejní	policejní	k2eAgMnSc1d1	policejní
vyšetřovatel	vyšetřovatel	k1gMnSc1	vyšetřovatel
Sóičiró	Sóičiró	k1gFnSc3	Sóičiró
Jagami	Jaga	k1gFnPc7	Jaga
<g/>
,	,	kIx,	,
Lightův	Lightův	k2eAgMnSc1d1	Lightův
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
policisty	policista	k1gMnPc7	policista
začne	začít	k5eAaPmIp3nS	začít
pátrat	pátrat	k5eAaImF	pátrat
po	po	k7c6	po
Kirovi	Kira	k1gMnSc6	Kira
(	(	kIx(	(
<g/>
přezdívka	přezdívka	k1gFnSc1	přezdívka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
začínají	začínat	k5eAaImIp3nP	začínat
sympatizovat	sympatizovat	k5eAaImF	sympatizovat
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
zabíjením	zabíjení	k1gNnSc7	zabíjení
<g/>
,	,	kIx,	,
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
Lighta	Light	k1gInSc2	Light
označili	označit	k5eAaPmAgMnP	označit
<g/>
;	;	kIx,	;
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
killer	killer	k1gInSc1	killer
<g/>
"	"	kIx"	"
-	-	kIx~	-
zabiják	zabiják	k1gInSc1	zabiják
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
záhadného	záhadný	k2eAgMnSc2d1	záhadný
L	L	kA	L
<g/>
,	,	kIx,	,
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
detektiva	detektiv	k1gMnSc4	detektiv
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
státy	stát	k1gInPc1	stát
svěřují	svěřovat	k5eAaImIp3nP	svěřovat
nejtěžší	těžký	k2eAgInPc1d3	nejtěžší
případy	případ	k1gInPc1	případ
a	a	k8xC	a
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
všechny	všechen	k3xTgMnPc4	všechen
vyřešil	vyřešit	k5eAaPmAgMnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
totiž	totiž	k9	totiž
podle	podle	k7c2	podle
první	první	k4xOgFnSc2	první
"	"	kIx"	"
<g/>
infarktové	infarktový	k2eAgFnSc2d1	infarktová
vraždy	vražda	k1gFnSc2	vražda
<g/>
"	"	kIx"	"
odhalil	odhalit	k5eAaPmAgMnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kira	Kira	k1gFnSc1	Kira
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
nechá	nechat	k5eAaPmIp3nS	nechat
zřídit	zřídit	k5eAaPmF	zřídit
centrálu	centrála	k1gFnSc4	centrála
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gMnSc1	Light
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Kira	Kira	k1gFnSc1	Kira
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
přes	přes	k7c4	přes
domácí	domácí	k2eAgFnSc4d1	domácí
síť	síť	k1gFnSc4	síť
do	do	k7c2	do
otcova	otcův	k2eAgInSc2d1	otcův
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
aktuální	aktuální	k2eAgFnPc4d1	aktuální
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
postup	postup	k1gInSc4	postup
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
daří	dařit	k5eAaImIp3nS	dařit
být	být	k5eAaImF	být
před	před	k7c7	před
vyšetřovateli	vyšetřovatel	k1gMnPc7	vyšetřovatel
o	o	k7c4	o
krok	krok	k1gInSc4	krok
napřed	napřed	k6eAd1	napřed
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
však	však	k9	však
L	L	kA	L
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
zjistí	zjistit	k5eAaPmIp3nP	zjistit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
značně	značně	k6eAd1	značně
zúží	zúžit	k5eAaPmIp3nS	zúžit
okruh	okruh	k1gInSc1	okruh
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
<g/>
...	...	k?	...
Zápisník	zápisník	k1gMnSc1	zápisník
smrti	smrt	k1gFnSc2	smrt
může	moct	k5eAaImIp3nS	moct
zabít	zabít	k5eAaPmF	zabít
všechny	všechen	k3xTgMnPc4	všechen
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
napsáno	napsat	k5eAaPmNgNnS	napsat
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
podobu	podoba	k1gFnSc4	podoba
si	se	k3xPyFc3	se
pisatel	pisatel	k1gMnSc1	pisatel
v	v	k7c6	v
momentě	moment	k1gInSc6	moment
psaní	psaní	k1gNnSc2	psaní
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
Zápisníky	zápisník	k1gInPc1	zápisník
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
stejnými	stejný	k2eAgNnPc7d1	stejné
pravidly	pravidlo	k1gNnPc7	pravidlo
jako	jako	k9	jako
ostatní	ostatní	k2eAgMnSc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Písemná	písemný	k2eAgNnPc4d1	písemné
pravidla	pravidlo	k1gNnPc4	pravidlo
Rjúk	Rjúk	k1gMnSc1	Rjúk
napsal	napsat	k5eAaBmAgMnS	napsat
pět	pět	k4xCc4	pět
základních	základní	k2eAgNnPc2d1	základní
pravidel	pravidlo	k1gNnPc2	pravidlo
o	o	k7c6	o
moci	moc	k1gFnSc6	moc
zápisníku	zápisník	k1gInSc2	zápisník
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
pravidla	pravidlo	k1gNnPc4	pravidlo
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gInSc3	on
rozumí	rozumět	k5eAaImIp3nS	rozumět
nejvíce	nejvíce	k6eAd1	nejvíce
smrtelníků	smrtelník	k1gMnPc2	smrtelník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pravidla	pravidlo	k1gNnPc1	pravidlo
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgFnPc1d1	následující
<g/>
:	:	kIx,	:
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
do	do	k7c2	do
zápisníku	zápisník	k1gInSc2	zápisník
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
pisatel	pisatel	k1gMnSc1	pisatel
nevidí	vidět	k5eNaImIp3nS	vidět
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tvář	tvář	k1gFnSc1	tvář
dané	daný	k2eAgFnPc1d1	daná
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
omylem	omylem	k6eAd1	omylem
zabit	zabit	k2eAgMnSc1d1	zabit
jiný	jiný	k2eAgMnSc1d1	jiný
člověk	člověk	k1gMnSc1	člověk
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
sekund	sekunda	k1gFnPc2	sekunda
od	od	k7c2	od
napsání	napsání	k1gNnSc2	napsání
jména	jméno	k1gNnSc2	jméno
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
specifikovat	specifikovat	k5eAaBmF	specifikovat
příčinu	příčina	k1gFnSc4	příčina
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nestane	stanout	k5eNaPmIp3nS	stanout
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
osoba	osoba	k1gFnSc1	osoba
zemře	zemřít	k5eAaPmIp3nS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
započetí	započetí	k1gNnSc6	započetí
psaní	psaní	k1gNnSc2	psaní
příčiny	příčina	k1gFnSc2	příčina
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
detaily	detail	k1gInPc7	detail
musí	muset	k5eAaImIp3nP	muset
stihnout	stihnout	k5eAaPmF	stihnout
napsat	napsat	k5eAaBmF	napsat
do	do	k7c2	do
6	[number]	k4	6
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
40	[number]	k4	40
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gInSc1	Light
do	do	k7c2	do
zápisníku	zápisník	k1gInSc2	zápisník
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
připsal	připsat	k5eAaPmAgMnS	připsat
další	další	k2eAgMnSc1d1	další
falešné	falešný	k2eAgNnSc4d1	falešné
pravidlo	pravidlo	k1gNnSc4	pravidlo
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
chtěl	chtít	k5eAaImAgInS	chtít
zmást	zmást	k5eAaPmF	zmást
policii	policie	k1gFnSc4	policie
a	a	k8xC	a
pomoct	pomoct	k5eAaPmF	pomoct
tak	tak	k6eAd1	tak
k	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
očištění	očištění	k1gNnSc3	očištění
<g/>
.	.	kIx.	.
</s>
<s>
Pravidlo	pravidlo	k1gNnSc1	pravidlo
říkalo	říkat	k5eAaImAgNnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
majitel	majitel	k1gMnSc1	majitel
do	do	k7c2	do
13	[number]	k4	13
dnů	den	k1gInPc2	den
od	od	k7c2	od
posledního	poslední	k2eAgInSc2d1	poslední
zápisu	zápis	k1gInSc2	zápis
nenapíše	napsat	k5eNaPmIp3nS	napsat
další	další	k2eAgNnSc4d1	další
jméno	jméno	k1gNnSc4	jméno
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
zemře	zemřít	k5eAaPmIp3nS	zemřít
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k5eAaPmF	Light
Jagami	Jaga	k1gFnPc7	Jaga
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
夜	夜	k?	夜
月	月	k?	月
<g/>
,	,	kIx,	,
Jagami	Jaga	k1gFnPc7	Jaga
Raito	Raito	k1gNnSc1	Raito
<g/>
)	)	kIx)	)
–	–	k?	–
Light	Light	k1gInSc1	Light
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
nadprůměrně	nadprůměrně	k6eAd1	nadprůměrně
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
a	a	k8xC	a
atleticky	atleticky	k6eAd1	atleticky
založený	založený	k2eAgInSc1d1	založený
(	(	kIx(	(
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
příběhu	příběh	k1gInSc2	příběh
sedmnáctiletý	sedmnáctiletý	k2eAgMnSc1d1	sedmnáctiletý
<g/>
)	)	kIx)	)
student	student	k1gMnSc1	student
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
najde	najít	k5eAaPmIp3nS	najít
Zápisník	zápisník	k1gInSc1	zápisník
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
světě	svět	k1gInSc6	svět
pohodil	pohodit	k5eAaPmAgMnS	pohodit
šinigami	šiniga	k1gFnPc7	šiniga
Rjúk	Rjúko	k1gNnPc2	Rjúko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
využít	využít	k5eAaPmF	využít
zápisníku	zápisník	k1gInSc2	zápisník
k	k	k7c3	k
očištění	očištění	k1gNnSc3	očištění
světa	svět	k1gInSc2	svět
od	od	k7c2	od
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
svět	svět	k1gInSc4	svět
bez	bez	k7c2	bez
zločinu	zločin	k1gInSc2	zločin
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
bude	být	k5eAaImBp3nS	být
bohem	bůh	k1gMnSc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
エ	エ	k?	エ
<g/>
,	,	kIx,	,
Eru	Eru	k1gFnSc1	Eru
<g/>
)	)	kIx)	)
–	–	k?	–
Detektiv	detektiv	k1gMnSc1	detektiv
L	L	kA	L
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
na	na	k7c6	na
Kirově	Kirův	k2eAgInSc6d1	Kirův
případu	případ	k1gInSc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
Lightovým	Lightův	k2eAgMnSc7d1	Lightův
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nejenže	nejenže	k6eAd1	nejenže
je	být	k5eAaImIp3nS	být
snad	snad	k9	snad
ještě	ještě	k6eAd1	ještě
inteligentnější	inteligentní	k2eAgMnPc1d2	inteligentnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Light	Light	k1gMnSc1	Light
nerad	nerad	k2eAgMnSc1d1	nerad
prohrává	prohrávat	k5eAaImIp3nS	prohrávat
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
brilantní	brilantní	k2eAgFnPc4d1	brilantní
dedukční	dedukční	k2eAgFnPc4d1	dedukční
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
výstředností	výstřednost	k1gFnPc2	výstřednost
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
jeho	on	k3xPp3gInSc4	on
podivný	podivný	k2eAgInSc4d1	podivný
způsob	způsob	k1gInSc4	způsob
sezení	sezení	k1gNnSc1	sezení
<g/>
,	,	kIx,	,
neustálá	neustálý	k2eAgFnSc1d1	neustálá
chuť	chuť	k1gFnSc1	chuť
na	na	k7c6	na
sladké	sladký	k2eAgFnSc6d1	sladká
a	a	k8xC	a
držení	držení	k1gNnSc6	držení
předmětů	předmět	k1gInPc2	předmět
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
používá	používat	k5eAaImIp3nS	používat
drastické	drastický	k2eAgFnPc4d1	drastická
strategie	strategie	k1gFnPc4	strategie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jedná	jednat	k5eAaImIp3nS	jednat
dokonale	dokonale	k6eAd1	dokonale
oproštěn	oprostit	k5eAaPmNgInS	oprostit
od	od	k7c2	od
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
několika	několik	k4yIc6	několik
příležitostech	příležitost	k1gFnPc6	příležitost
se	se	k3xPyFc4	se
veřejně	veřejně	k6eAd1	veřejně
zmíní	zmínit	k5eAaPmIp3nS	zmínit
o	o	k7c6	o
svých	svůj	k3xOyFgFnPc6	svůj
slabinách	slabina	k1gFnPc6	slabina
<g/>
.	.	kIx.	.
</s>
<s>
Neváhá	váhat	k5eNaImIp3nS	váhat
vsadit	vsadit	k5eAaPmF	vsadit
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Misa	Misa	k6eAd1	Misa
Amane	Aman	k1gInSc5	Aman
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
弥	弥	k?	弥
海	海	k?	海
<g/>
,	,	kIx,	,
Amane	Aman	k1gInSc5	Aman
Misa	Misum	k1gNnPc4	Misum
<g/>
)	)	kIx)	)
–	–	k?	–
Misa	Mis	k1gInSc2	Mis
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
idol	idol	k1gInSc1	idol
a	a	k8xC	a
držitelka	držitelka	k1gFnSc1	držitelka
druhého	druhý	k4xOgInSc2	druhý
Zápisníku	zápisník	k1gInSc2	zápisník
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
naivní	naivní	k2eAgFnSc1d1	naivní
a	a	k8xC	a
dětinská	dětinský	k2eAgFnSc1d1	dětinská
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
její	její	k3xOp3gInSc1	její
biologický	biologický	k2eAgInSc1d1	biologický
věk	věk	k1gInSc1	věk
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
.	.	kIx.	.
</s>
<s>
Obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
Kiru	Kira	k1gFnSc4	Kira
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zabil	zabít	k5eAaPmAgMnS	zabít
vraha	vrah	k1gMnSc4	vrah
jejích	její	k3xOp3gMnPc2	její
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zjistí	zjistit	k5eAaPmIp3nS	zjistit
Kirovu	Kirův	k2eAgFnSc4d1	Kirova
totožnost	totožnost	k1gFnSc4	totožnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
okamžitě	okamžitě	k6eAd1	okamžitě
bláznivě	bláznivě	k6eAd1	bláznivě
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Light	Light	k1gInSc1	Light
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
pouze	pouze	k6eAd1	pouze
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Misa	Misa	k1gFnSc1	Misa
má	mít	k5eAaImIp3nS	mít
kromě	kromě	k7c2	kromě
Zápisníku	zápisník	k1gInSc2	zápisník
smrti	smrt	k1gFnSc2	smrt
i	i	k8xC	i
šinigamiho	šinigamize	k6eAd1	šinigamize
oči	oko	k1gNnPc1	oko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vidět	vidět	k5eAaImF	vidět
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
věk	věk	k1gInSc4	věk
každé	každý	k3xTgFnSc2	každý
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
podívá	podívat	k5eAaImIp3nS	podívat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
začne	začít	k5eAaPmIp3nS	začít
mít	mít	k5eAaImF	mít
rád	rád	k2eAgMnSc1d1	rád
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
společně	společně	k6eAd1	společně
žít	žít	k5eAaImF	žít
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Watari	Watar	k1gFnSc3	Watar
–	–	k?	–
Záhadný	záhadný	k2eAgMnSc1d1	záhadný
L-ův	L-ův	k1gMnSc1	L-ův
prostředník	prostředník	k1gMnSc1	prostředník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
příběhu	příběh	k1gInSc2	příběh
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
může	moct	k5eAaImIp3nS	moct
L	L	kA	L
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jej	on	k3xPp3gInSc4	on
finančně	finančně	k6eAd1	finančně
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
příběhu	příběh	k1gInSc2	příběh
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
sirotčinec	sirotčinec	k1gInSc1	sirotčinec
pro	pro	k7c4	pro
nadané	nadaný	k2eAgFnPc4d1	nadaná
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Mello	Mello	k1gNnSc1	Mello
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
メ	メ	k?	メ
<g/>
,	,	kIx,	,
Mero	Mero	k1gMnSc1	Mero
<g/>
)	)	kIx)	)
–	–	k?	–
Mello	Mello	k1gNnSc1	Mello
je	být	k5eAaImIp3nS	být
sirotek	sirotek	k1gMnSc1	sirotek
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Nearem	Near	k1gInSc7	Near
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
ve	v	k7c4	v
Watariho	Watari	k1gMnSc4	Watari
sirotčinci	sirotčinec	k1gInSc3	sirotčinec
pro	pro	k7c4	pro
nadané	nadaný	k2eAgFnPc4d1	nadaná
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Near	Near	k1gInSc1	Near
je	být	k5eAaImIp3nS	být
i	i	k9	i
Mello	Mello	k1gNnSc1	Mello
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
nástupce	nástupce	k1gMnSc1	nástupce
L.	L.	kA	L.
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
L	L	kA	L
má	mít	k5eAaImIp3nS	mít
slabost	slabost	k1gFnSc4	slabost
pro	pro	k7c4	pro
sladkosti	sladkost	k1gFnPc4	sladkost
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
čokoládu	čokoláda	k1gFnSc4	čokoláda
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Mello	Mello	k1gNnSc1	Mello
taktéž	taktéž	k?	taktéž
vysoce	vysoce	k6eAd1	vysoce
inteligentní	inteligentní	k2eAgMnPc4d1	inteligentní
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
emoce	emoce	k1gFnSc1	emoce
ho	on	k3xPp3gMnSc4	on
často	často	k6eAd1	často
dokážou	dokázat	k5eAaPmIp3nP	dokázat
velmi	velmi	k6eAd1	velmi
rozrušit	rozrušit	k5eAaPmF	rozrušit
<g/>
.	.	kIx.	.
</s>
<s>
Near	Near	k1gInSc1	Near
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ニ	ニ	k?	ニ
<g/>
,	,	kIx,	,
Nia	Nia	k1gFnSc1	Nia
<g/>
)	)	kIx)	)
–	–	k?	–
Near	Near	k1gInSc1	Near
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
nástupce	nástupce	k1gMnPc4	nástupce
L.	L.	kA	L.
Má	mít	k5eAaImIp3nS	mít
i	i	k9	i
spoustu	spousta	k1gFnSc4	spousta
vlastností	vlastnost	k1gFnPc2	vlastnost
jako	jako	k8xS	jako
L.	L.	kA	L.
Například	například	k6eAd1	například
si	se	k3xPyFc3	se
často	často	k6eAd1	často
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
kostkami	kostka	k1gFnPc7	kostka
nebo	nebo	k8xC	nebo
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
vlasy	vlas	k1gInPc7	vlas
a	a	k8xC	a
hračkami	hračka	k1gFnPc7	hračka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
sedí	sedit	k5eAaImIp3nS	sedit
neobvyklým	obvyklý	k2eNgInSc7d1	neobvyklý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
L	L	kA	L
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
nepatrným	patrný	k2eNgInSc7d1	patrný
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Near	Near	k1gInSc1	Near
získá	získat	k5eAaPmIp3nS	získat
podporu	podpora	k1gFnSc4	podpora
vlády	vláda	k1gFnSc2	vláda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
speciální	speciální	k2eAgFnSc4d1	speciální
skupinu	skupina	k1gFnSc4	skupina
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Kirou	Kira	k1gFnSc7	Kira
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
SPK	SPK	kA	SPK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kapitol	kapitola	k1gFnPc2	kapitola
mangy	mango	k1gNnPc7	mango
Death	Deatha	k1gFnPc2	Deatha
Note	Not	k1gMnPc4	Not
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc4	mango
Death	Death	k1gInSc1	Death
Note	Not	k1gInSc2	Not
byla	být	k5eAaImAgNnP	být
vydávána	vydávat	k5eAaImNgNnP	vydávat
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
časopise	časopis	k1gInSc6	časopis
Šúkan	Šúkan	k1gInSc4	Šúkan
šónen	šónen	k2eAgInSc1d1	šónen
Jump	Jump	k1gInSc1	Jump
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
skončila	skončit	k5eAaPmAgFnS	skončit
se	s	k7c7	s
108	[number]	k4	108
kapitolami	kapitola	k1gFnPc7	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Šúeiša	Šúeiš	k1gInSc2	Šúeiš
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
12	[number]	k4	12
svazků	svazek	k1gInPc2	svazek
mangy	mango	k1gNnPc7	mango
formátu	formát	k1gInSc3	formát
tankóbon	tankóbon	k1gInSc1	tankóbon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
úvodní	úvodní	k2eAgInSc4d1	úvodní
svazek	svazek	k1gInSc4	svazek
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
-	-	kIx~	-
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
-	-	kIx~	-
svazek	svazek	k1gInSc4	svazek
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mangu	mango	k1gNnSc6	mango
vydává	vydávat	k5eAaImIp3nS	vydávat
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
CREW	CREW	kA	CREW
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
29	[number]	k4	29
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2015	[number]	k4	2015
vydalo	vydat	k5eAaPmAgNnS	vydat
všech	všecek	k3xTgInPc2	všecek
dvanáct	dvanáct	k4xCc4	dvanáct
svazků	svazek	k1gInPc2	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
dílů	díl	k1gInPc2	díl
seriálu	seriál	k1gInSc2	seriál
Death	Deatha	k1gFnPc2	Deatha
Note	Not	k1gFnSc2	Not
<g/>
.	.	kIx.	.
</s>
<s>
Dorama	Dorama	k?	Dorama
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Dorama	Dorama	k?	Dorama
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
–	–	k?	–
oba	dva	k4xCgMnPc1	dva
dva	dva	k4xCgInPc4	dva
režíroval	režírovat	k5eAaImAgMnS	režírovat
Šusuke	Šusuke	k1gFnSc3	Šusuke
Kaneko	Kaneko	k1gNnSc1	Kaneko
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
Nippon	Nippona	k1gFnPc2	Nippona
Television	Television	k1gInSc1	Television
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
byla	být	k5eAaImAgFnS	být
premiéra	premiéra	k1gFnSc1	premiéra
17	[number]	k4	17
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
od	od	k7c2	od
anime	aniit	k5eAaImRp1nP	aniit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
moc	moc	k6eAd1	moc
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc1	život
Lighta	Light	k1gMnSc2	Light
Jagamiho	Jagami	k1gMnSc2	Jagami
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nalezne	nalézt	k5eAaBmIp3nS	nalézt
Zápisník	zápisník	k1gInSc4	zápisník
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
neodsouzené	odsouzený	k2eNgNnSc1d1	odsouzený
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
i	i	k9	i
odsouzené	odsouzený	k2eAgMnPc4d1	odsouzený
<g/>
)	)	kIx)	)
zločince	zločinec	k1gMnPc4	zločinec
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
má	mít	k5eAaImIp3nS	mít
něco	něco	k3yInSc1	něco
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
2	[number]	k4	2
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
:	:	kIx,	:
Change	change	k1gFnSc1	change
the	the	k?	the
World	World	k1gInSc1	World
je	být	k5eAaImIp3nS	být
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
volným	volný	k2eAgNnSc7d1	volné
pokračováním	pokračování	k1gNnSc7	pokračování
série	série	k1gFnSc2	série
Death	Deatha	k1gFnPc2	Deatha
Note	Not	k1gFnSc2	Not
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
detektiv	detektiv	k1gMnSc1	detektiv
L.	L.	kA	L.
Film	film	k1gInSc1	film
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
lidí	člověk	k1gMnPc2	člověk
zastávajících	zastávající	k2eAgFnPc2d1	zastávající
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
svět	svět	k1gInSc1	svět
doznává	doznávat	k5eAaImIp3nS	doznávat
hrozných	hrozný	k2eAgFnPc2d1	hrozná
ekologických	ekologický	k2eAgFnPc2d1	ekologická
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přelidnění	přelidnění	k1gNnSc2	přelidnění
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vymýtit	vymýtit	k5eAaPmF	vymýtit
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
plno	plno	k6eAd1	plno
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Chtějí	chtít	k5eAaImIp3nP	chtít
do	do	k7c2	do
světa	svět	k1gInSc2	svět
vypustit	vypustit	k5eAaPmF	vypustit
vir	vir	k1gInSc4	vir
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zabije	zabít	k5eAaPmIp3nS	zabít
plno	plno	k6eAd1	plno
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
přelidněných	přelidněný	k2eAgFnPc6d1	přelidněná
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
po	po	k7c6	po
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Kirou	Kira	k1gFnSc7	Kira
již	již	k6eAd1	již
jen	jen	k9	jen
23	[number]	k4	23
dní	den	k1gInPc2	den
života	život	k1gInSc2	život
a	a	k8xC	a
ty	ten	k3xDgInPc4	ten
hodlá	hodlat	k5eAaImIp3nS	hodlat
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
boji	boj	k1gInSc3	boj
se	s	k7c7	s
zlem	zlo	k1gNnSc7	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
L	L	kA	L
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
o	o	k7c6	o
případu	případ	k1gInSc6	případ
toho	ten	k3xDgInSc2	ten
viru	vir	k1gInSc2	vir
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
okamžitě	okamžitě	k6eAd1	okamžitě
pouští	pouštět	k5eAaImIp3nS	pouštět
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k8xS	jako
všude	všude	k6eAd1	všude
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
shrbený	shrbený	k2eAgMnSc1d1	shrbený
<g/>
,	,	kIx,	,
bere	brát	k5eAaImIp3nS	brát
věci	věc	k1gFnPc4	věc
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
jen	jen	k6eAd1	jen
dvěma	dva	k4xCgInPc7	dva
prsty	prst	k1gInPc7	prst
a	a	k8xC	a
píše	psát	k5eAaImIp3nS	psát
typicky	typicky	k6eAd1	typicky
na	na	k7c4	na
klávesnici	klávesnice	k1gFnSc4	klávesnice
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
malý	malý	k2eAgMnSc1d1	malý
chlapec	chlapec	k1gMnSc1	chlapec
(	(	kIx(	(
<g/>
matematický	matematický	k2eAgMnSc1d1	matematický
génius	génius	k1gMnSc1	génius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
nalezl	nalézt	k5eAaBmAgMnS	nalézt
L-ův	L-ův	k1gMnSc1	L-ův
přítel	přítel	k1gMnSc1	přítel
F	F	kA	F
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
mladá	mladý	k2eAgFnSc1d1	mladá
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
částečnou	částečný	k2eAgFnSc4d1	částečná
imunitu	imunita	k1gFnSc4	imunita
vůči	vůči	k7c3	vůči
smrtícímu	smrtící	k2eAgInSc3d1	smrtící
viru	vir	k1gInSc3	vir
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Death	Deatha	k1gFnPc2	Deatha
Note	Not	k1gFnSc2	Not
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
anime	animat	k5eAaPmIp3nS	animat
série	série	k1gFnSc1	série
Anime	Anim	k1gInSc5	Anim
série	série	k1gFnSc2	série
na	na	k7c4	na
CSFD	CSFD	kA	CSFD
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Death	Death	k1gInSc1	Death
Note	Not	k1gFnSc2	Not
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
bibliografické	bibliografický	k2eAgFnSc6d1	bibliografická
databázi	databáze	k1gFnSc6	databáze
</s>
