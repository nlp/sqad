<s>
Hamburgerová	hamburgerový	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
Hamburgrová	Hamburgrová	k1gFnSc1
univerzitaHamburger	univerzitaHamburgra	k1gFnPc2
University	universita	k1gFnSc2
</s>
<s>
Motto	motto	k1gNnSc1
</s>
<s>
Learning	Learning	k1gInSc1
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
Leading	Leading	k1gInSc4
Tomorrow	Tomorrow	k1gFnSc2
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
1961	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
87	#num#	k4
<g/>
°	°	k?
<g/>
56	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
www.aboutmcdonalds.com/mcd/corporate_careers/training_and_development/hamburger_university.html	www.aboutmcdonalds.com/mcd/corporate_careers/training_and_development/hamburger_university.html	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hamburgerová	hamburgerový	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
angličtině	angličtina	k1gFnSc6
Hamburger	hamburger	k1gInSc1
University	universita	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
12	#num#	k4
000	#num#	k4
m²	m²	k?
</s>
<s>
velké	velký	k2eAgNnSc1d1
školící	školící	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
McDonald	McDonalda	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Oak	Oak	k1gFnSc6
Brook	Brook	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
západním	západní	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Chicaga	Chicago	k1gNnSc2
ve	v	k7c6
státě	stát	k1gInSc6
Illinois	Illinois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výuku	výuka	k1gFnSc4
v	v	k7c6
tomto	tento	k3xDgNnSc6
zařízení	zařízení	k1gNnSc6
absolvovalo	absolvovat	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
80	#num#	k4
000	#num#	k4
manažerů	manažer	k1gMnPc2
restaurací	restaurace	k1gFnPc2
<g/>
,	,	kIx,
středních	střední	k2eAgMnPc2d1
manažerů	manažer	k1gMnPc2
a	a	k8xC
majitelů	majitel	k1gMnPc2
<g/>
/	/	kIx~
<g/>
provozovatelů	provozovatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Areál	areál	k1gInSc1
</s>
<s>
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
Hamburgerová	hamburgerový	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
nachází	nacházet	k5eAaImIp3nS
na	na	k7c6
areálu	areál	k1gInSc6
velkém	velký	k2eAgInSc6d1
32	#num#	k4
hektarů	hektar	k1gInPc2
s	s	k7c7
19	#num#	k4
rezidentními	rezidentní	k2eAgInPc7d1
instruktory	instruktor	k1gMnPc4
na	na	k7c4
plný	plný	k2eAgInSc4d1
úvazek	úvazek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
13	#num#	k4
výukových	výukový	k2eAgFnPc2d1
místností	místnost	k1gFnPc2
se	s	k7c7
300	#num#	k4
sedadly	sedadlo	k1gNnPc7
<g/>
,	,	kIx,
12	#num#	k4
interaktivních	interaktivní	k2eAgFnPc2d1
vzdělávacích	vzdělávací	k2eAgFnPc2d1
týmových	týmový	k2eAgFnPc2d1
místností	místnost	k1gFnPc2
a	a	k8xC
třech	tři	k4xCgFnPc6
kuchyňských	kuchyňský	k2eAgFnPc2d1
laboratoří	laboratoř	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hamburgerová	hamburgerový	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
má	mít	k5eAaImIp3nS
tlumočníky	tlumočník	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
můžou	můžou	k?
poskytnout	poskytnout	k5eAaPmF
simultánní	simultánní	k2eAgNnSc4d1
tlumočení	tlumočení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
má	mít	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
učit	učit	k5eAaImF
ve	v	k7c6
28	#num#	k4
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
Ray	Ray	k1gMnSc1
Kroc	Kroc	k1gFnSc4
na	na	k7c6
začátku	začátek	k1gInSc6
dohlížel	dohlížet	k5eAaImAgMnS
na	na	k7c4
lekce	lekce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
Kroc	Kroc	k1gInSc4
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nahrávkách	nahrávka	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
Hamburgerové	hamburgerový	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
stále	stále	k6eAd1
používány	používat	k5eAaImNgFnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
Hamburgerová	hamburgerový	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
byla	být	k5eAaImAgFnS
satirizována	satirizovat	k5eAaImNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
komedií	komedie	k1gFnPc2
Hamburger	hamburger	k1gInSc4
<g/>
…	…	k?
od	od	k7c2
Motion	Motion	k1gInSc1
Picture	Pictur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
také	také	k9
zobrazena	zobrazit	k5eAaPmNgFnS
v	v	k7c4
McDonald	McDonald	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
reklamě	reklama	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
Ronald	Ronald	k1gMnSc1
McDonald	McDonald	k1gMnSc1
a	a	k8xC
roboti	robot	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
vypadají	vypadat	k5eAaImIp3nP,k5eAaPmIp3nP
jako	jako	k9
hamburgery	hamburger	k1gInPc1
<g/>
,	,	kIx,
odchází	odcházet	k5eAaImIp3nP
jako	jako	k9
vystudovaní	vystudovaný	k2eAgMnPc1d1
z	z	k7c2
této	tento	k3xDgFnSc2
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hamburger	hamburger	k1gInSc1
University	universita	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Hamburger	hamburger	k1gInSc1
University	universita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
McDonald	McDonald	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hamburger	hamburger	k1gInSc1
-	-	kIx~
The	The	k1gFnSc1
Motion	Motion	k1gInSc1
Picture	Pictur	k1gMnSc5
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BFI	BFI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
