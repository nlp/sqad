<p>
<s>
Tovaryš	tovaryš	k1gMnSc1	tovaryš
byl	být	k5eAaImAgMnS	být
učeň	učeň	k1gMnSc1	učeň
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
úspěšně	úspěšně	k6eAd1	úspěšně
splnil	splnit	k5eAaPmAgMnS	splnit
tovaryšské	tovaryšský	k2eAgFnPc4d1	tovaryšská
zkoušky	zkouška	k1gFnPc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgInS	moct
samostatně	samostatně	k6eAd1	samostatně
vykonávat	vykonávat	k5eAaImF	vykonávat
práce	práce	k1gFnPc4	práce
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
oboru	obor	k1gInSc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tovaryše	tovaryš	k1gMnSc2	tovaryš
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistr	mistr	k1gMnSc1	mistr
po	po	k7c6	po
složení	složení	k1gNnSc6	složení
mistrovské	mistrovský	k2eAgFnSc2d1	mistrovská
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
tovaryšů	tovaryš	k1gMnPc2	tovaryš
určoval	určovat	k5eAaImAgInS	určovat
cech	cech	k1gInSc1	cech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
již	již	k6eAd1	již
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
tovaryš	tovaryš	k1gMnSc1	tovaryš
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
učeň	učeň	k1gMnSc1	učeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odvozené	odvozený	k2eAgNnSc1d1	odvozené
slovo	slovo	k1gNnSc1	slovo
==	==	k?	==
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
tovaryšů	tovaryš	k1gMnPc2	tovaryš
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
tovaryšstvo	tovaryšstvo	k1gNnSc4	tovaryšstvo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejvíce	nejvíce	k6eAd1	nejvíce
známo	znám	k2eAgNnSc1d1	známo
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
názvů	název	k1gInPc2	název
pro	pro	k7c4	pro
Jezuitský	jezuitský	k2eAgInSc4d1	jezuitský
řád	řád	k1gInSc4	řád
neboli	neboli	k8xC	neboli
Tovaryšstvo	tovaryšstvo	k1gNnSc4	tovaryšstvo
Ježíšovo	Ježíšův	k2eAgNnSc4d1	Ježíšovo
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgNnSc4d1	znamenající
(	(	kIx(	(
<g/>
volným	volný	k2eAgInSc7d1	volný
překladem	překlad	k1gInSc7	překlad
<g/>
)	)	kIx)	)
učedníci	učedník	k1gMnPc1	učedník
Kristovi	Kristův	k2eAgMnPc1d1	Kristův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
slovesnost	slovesnost	k1gFnSc1	slovesnost
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Tovaryš	tovaryš	k1gMnSc1	tovaryš
<g/>
,	,	kIx,	,
s.	s.	k?	s.
339	[number]	k4	339
<g/>
-	-	kIx~	-
<g/>
440	[number]	k4	440
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
WINTER	Winter	k1gMnSc1	Winter
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Poměr	poměr	k1gInSc1	poměr
cechu	cech	k1gInSc2	cech
k	k	k7c3	k
dělnictvu	dělnictvo	k1gNnSc3	dělnictvo
<g/>
,	,	kIx,	,
s.	s.	k?	s.
725	[number]	k4	725
<g/>
-	-	kIx~	-
<g/>
769	[number]	k4	769
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Cech	cech	k1gInSc1	cech
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tovaryš	tovaryš	k1gMnSc1	tovaryš
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
