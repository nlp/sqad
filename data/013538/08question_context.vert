<s>
Od	od	k7c2
mládí	mládí	k1gNnSc2
se	se	k3xPyFc4
K.	K.	kA
Rychlík	Rychlík	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
algebře	algebra	k1gFnSc3
a	a	k8xC
teorii	teorie	k1gFnSc4
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
publikoval	publikovat	k5eAaBmAgMnS
dvě	dva	k4xCgFnPc4
učebnice	učebnice	k1gFnPc4
<g/>
:	:	kIx,
Úvod	úvod	k1gInSc1
do	do	k7c2
elementární	elementární	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
číselné	číselný	k2eAgFnSc2d1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Úvod	úvod	k1gInSc1
do	do	k7c2
analytické	analytický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
mnohostěnů	mnohostěn	k1gInPc2
s	s	k7c7
reálnými	reálný	k2eAgInPc7d1
koeficienty	koeficient	k1gInPc7
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>