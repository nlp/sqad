<s>
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
prof.	prof.	kA
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1885	#num#	k4
<g/>
Benešov	Benešov	k1gInSc1
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1968	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
83	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Praha	Praha	k1gFnSc1
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
–	–	k?
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
<g/>
Pařížská	pařížský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
–	–	k?
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
<g/>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
Povolání	povolání	k1gNnSc3
</s>
<s>
matematik	matematik	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgInSc2d1
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
–	–	k?
<g/>
1913	#num#	k4
<g/>
)	)	kIx)
<g/>
České	český	k2eAgNnSc1d1
vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
1913	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1885	#num#	k4
<g/>
,	,	kIx,
Benešov	Benešov	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1968	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
matematik	matematik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
zejména	zejména	k6eAd1
algebře	algebra	k1gFnSc3
a	a	k8xC
teorii	teorie	k1gFnSc4
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Chrudimi	Chrudim	k1gFnSc6
<g/>
,	,	kIx,
poté	poté	k6eAd1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
v	v	k7c6
Benešově	Benešov	k1gInSc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
a	a	k8xC
v	v	k7c6
červenci	červenec	k1gInSc6
roku	rok	k1gInSc2
1904	#num#	k4
maturoval	maturovat	k5eAaBmAgMnS
na	na	k7c6
Akademickém	akademický	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
již	již	k6eAd1
na	na	k7c6
střední	střední	k2eAgFnSc6d1
škole	škola	k1gFnSc6
měl	mít	k5eAaImAgInS
značný	značný	k2eAgInSc1d1
zájem	zájem	k1gInSc1
hlavně	hlavně	k9
o	o	k7c4
matematiku	matematika	k1gFnSc4
<g/>
,	,	kIx,
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
na	na	k7c4
Filozofickou	filozofický	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Karlo-Ferdinandovy	Karlo-Ferdinandův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
mohl	moct	k5eAaImAgMnS
věnovat	věnovat	k5eAaPmF,k5eAaImF
především	především	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesory	profesor	k1gMnPc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
byli	být	k5eAaImAgMnP
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
Jan	Jan	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
a	a	k8xC
od	od	k7c2
jara	jaro	k1gNnSc2
1903	#num#	k4
také	také	k9
Karel	Karel	k1gMnSc1
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
studoval	studovat	k5eAaImAgMnS
tedy	tedy	k9
matematiku	matematika	k1gFnSc4
a	a	k8xC
fyziku	fyzika	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pouze	pouze	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
studijního	studijní	k2eAgInSc2d1
roku	rok	k1gInSc2
1906	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
1907	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
dostal	dostat	k5eAaPmAgMnS
státní	státní	k2eAgNnSc4d1
stipendium	stipendium	k1gNnSc4
a	a	k8xC
na	na	k7c4
rok	rok	k1gInSc4
odjel	odjet	k5eAaPmAgMnS
za	za	k7c7
studiem	studio	k1gNnSc7
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Sorbonně	Sorbonna	k1gFnSc6
navštěvoval	navštěvovat	k5eAaImAgMnS
matematické	matematický	k2eAgFnPc4d1
přednášky	přednáška	k1gFnPc4
profesorů	profesor	k1gMnPc2
Jacqua	Jacquum	k1gNnSc2
Hadamarda	Hadamard	k1gMnSc2
<g/>
,	,	kIx,
Émila	Émil	k1gMnSc2
Picarda	Picard	k1gMnSc2
a	a	k8xC
M.	M.	kA
G.	G.	kA
Humberta	Humberta	k1gFnSc1
a	a	k8xC
současně	současně	k6eAd1
zpracovával	zpracovávat	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
disertační	disertační	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
návratu	návrat	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1908	#num#	k4
vykonal	vykonat	k5eAaPmAgMnS
zkoušku	zkouška	k1gFnSc4
učitelské	učitelský	k2eAgFnSc2d1
způsobilosti	způsobilost	k1gFnSc2
z	z	k7c2
matematiky	matematika	k1gFnSc2
a	a	k8xC
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ho	on	k3xPp3gMnSc4
opravňovala	opravňovat	k5eAaImAgFnS
k	k	k7c3
výkonu	výkon	k1gInSc3
učitelského	učitelský	k2eAgNnSc2d1
povolání	povolání	k1gNnSc2
na	na	k7c6
středních	střední	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1909	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
disertace	disertace	k1gFnSc2
O	o	k7c6
grupách	grupa	k1gFnPc6
ternárních	ternární	k2eAgFnPc2d1
kolineací	kolineace	k1gFnPc2
holoedricky	holoedricky	k6eAd1
isomorfních	isomorfní	k2eAgMnPc2d1
s	s	k7c7
alternativními	alternativní	k2eAgInPc7d1
a	a	k8xC
symetrickými	symetrický	k2eAgFnPc7d1
grupami	grupa	k1gFnPc7
permutací	permutace	k1gFnPc2
získal	získat	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
doktorát	doktorát	k1gInSc4
filozofie	filozofie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
placeným	placený	k2eAgMnSc7d1
asistentem	asistent	k1gMnSc7
matematiky	matematika	k1gFnSc2
na	na	k7c6
filozofické	filozofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
,	,	kIx,
když	když	k8xS
již	již	k6eAd1
od	od	k7c2
ledna	leden	k1gInSc2
zastával	zastávat	k5eAaImAgMnS
toto	tento	k3xDgNnSc4
místo	místo	k1gNnSc4
bezplatně	bezplatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1912	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
habilitoval	habilitovat	k5eAaBmAgMnS
s	s	k7c7
prací	práce	k1gFnSc7
Příspěvek	příspěvek	k1gInSc1
k	k	k7c3
teorii	teorie	k1gFnSc3
forem	forma	k1gFnPc2
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1913	#num#	k4
<g/>
,	,	kIx,
přešel	přejít	k5eAaPmAgMnS
jako	jako	k9
asistent	asistent	k1gMnSc1
na	na	k7c4
Českou	český	k2eAgFnSc4d1
vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
technickou	technický	k2eAgFnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
mimořádným	mimořádný	k2eAgInSc7d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
řádným	řádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byl	být	k5eAaImAgInS
penzionován	penzionovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1968	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
byl	být	k5eAaImAgInS
pohřben	pohřbít	k5eAaPmNgInS
na	na	k7c6
Olšanských	olšanský	k2eAgInPc6d1
hřbitovech	hřbitov	k1gInPc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Od	od	k7c2
mládí	mládí	k1gNnSc2
se	se	k3xPyFc4
K.	K.	kA
Rychlík	Rychlík	k1gMnSc1
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
algebře	algebra	k1gFnSc3
a	a	k8xC
teorii	teorie	k1gFnSc4
čísel	číslo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
publikoval	publikovat	k5eAaBmAgMnS
dvě	dva	k4xCgFnPc4
učebnice	učebnice	k1gFnPc4
<g/>
:	:	kIx,
Úvod	úvod	k1gInSc1
do	do	k7c2
elementární	elementární	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
číselné	číselný	k2eAgFnSc2d1
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Úvod	úvod	k1gInSc1
do	do	k7c2
analytické	analytický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
mnohostěnů	mnohostěn	k1gInPc2
s	s	k7c7
reálnými	reálný	k2eAgInPc7d1
koeficienty	koeficient	k1gInPc7
(	(	kIx(
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
i	i	k9
knihy	kniha	k1gFnPc4
Úvod	úvod	k1gInSc4
do	do	k7c2
počtu	počet	k1gInSc2
pravděpodobnosti	pravděpodobnost	k1gFnSc2
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
češtiny	čeština	k1gFnSc2
přeložil	přeložit	k5eAaPmAgInS
Rychlík	rychlík	k1gInSc1
čtyři	čtyři	k4xCgFnPc4
knihy	kniha	k1gFnPc1
ruských	ruský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledním	poslední	k2eAgNnSc6d1
období	období	k1gNnSc6
svého	svůj	k3xOyFgInSc2
života	život	k1gInSc2
se	se	k3xPyFc4
hluboce	hluboko	k6eAd1
zajímal	zajímat	k5eAaImAgMnS
o	o	k7c4
dějiny	dějiny	k1gFnPc4
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
pracemi	práce	k1gFnPc7
Bernarda	Bernard	k1gMnSc2
Bolzana	Bolzan	k1gMnSc2
<g/>
,	,	kIx,
např.	např.	kA
Theorie	theorie	k1gFnSc1
der	drát	k5eAaImRp2nS
reellen	reellna	k1gFnPc2
Zahlen	Zahlen	k2eAgInSc1d1
im	im	k?
Bolzanos	Bolzanos	k1gInSc1
handschriftlichen	handschriftlichen	k1gInSc1
Nachlasse	Nachlasse	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Hykšová	Hykšová	k1gFnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
:	:	kIx,
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
-	-	kIx~
<g/>
1968	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
matematiky	matematika	k1gFnSc2
sv.	sv.	kA
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kořínek	Kořínek	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
<g/>
:	:	kIx,
K	k	k7c3
pětasedmdesátinám	pětasedmdesátina	k1gFnPc3
Prof.	prof.	kA
Dr	dr	kA
<g/>
.	.	kIx.
Karla	Karel	k1gMnSc2
Rychlíka	Rychlík	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
pro	pro	k7c4
pěstování	pěstování	k1gNnSc4
matematiky	matematika	k1gFnSc2
<g/>
.	.	kIx.
85	#num#	k4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
492	#num#	k4
<g/>
-	-	kIx~
<g/>
498	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
farnosti	farnost	k1gFnSc2
Benešov	Benešov	k1gInSc1
↑	↑	k?
Putování	putování	k1gNnSc1
po	po	k7c6
hrobech	hrob	k1gInPc6
slavných	slavný	k2eAgMnPc2d1
matematiků	matematik	k1gMnPc2
<g/>
,	,	kIx,
fyziků	fyzik	k1gMnPc2
<g/>
,	,	kIx,
astronomů	astronom	k1gMnPc2
XII	XII	kA
<g/>
:	:	kIx,
Olšanské	olšanský	k2eAgInPc4d1
hřbitovy	hřbitov	k1gInPc4
pošesté	pošesté	k4xO
|	|	kIx~
Matfyz	Matfyza	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.matfyz.cz	www.matfyz.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Karel	Karel	k1gMnSc1
Rychlík	Rychlík	k1gMnSc1
</s>
<s>
Stránky	stránka	k1gFnPc1
věnované	věnovaný	k2eAgFnPc1d1
Karlu	Karel	k1gMnSc3
Rychlíkovi	Rychlík	k1gMnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
text	text	k1gInSc1
(	(	kIx(
<g/>
licence	licence	k1gFnSc1
CC-BY	CC-BY	k1gFnSc2
3.0	3.0	k4
Unported	Unported	k1gMnSc1
<g/>
)	)	kIx)
ze	z	k7c2
stránky	stránka	k1gFnSc2
z	z	k7c2
webu	web	k1gInSc2
Významní	významný	k2eAgMnPc1d1
matematici	matematik	k1gMnPc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autor	autor	k1gMnSc1
původního	původní	k2eAgInSc2d1
textu	text	k1gInSc2
<g/>
:	:	kIx,
Pavel	Pavel	k1gMnSc1
Šišma	Šišm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jk	jk	k?
<g/>
0	#num#	k4
<g/>
1103310	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
104898364	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1749	#num#	k4
026X	026X	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
20126193	#num#	k4
</s>
