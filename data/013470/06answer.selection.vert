<s desamb="1">
Při	při	k7c6
dirigování	dirigování	k1gNnSc6
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
zkoušek	zkouška	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1687	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Lully	Lully	k1gMnSc1
prudce	prudce	k6eAd1
udeřil	udeřit	k5eAaPmAgInS
bodcem	bodec	k1gInSc7
těžké	těžký	k2eAgFnSc2d1
barokní	barokní	k2eAgFnSc2d1
taktovky	taktovka	k1gFnSc2
do	do	k7c2
palce	palec	k1gInSc2
pravé	pravý	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
nedostatečné	dostatečný	k2eNgFnSc2d1
hygieny	hygiena	k1gFnSc2
zanítil	zanítit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>