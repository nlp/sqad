<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gMnSc2
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
de	de	k?
Lully	Lull	k1gInPc1
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnPc1
(	(	kIx(
<g/>
portrét	portrét	k1gInSc1
od	od	k7c2
Pierre	Pierr	k1gInSc5
Mignarda	Mignard	k1gMnSc2
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rodné	rodný	k2eAgFnPc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Giovanni	Giovann	k1gMnPc1
Battista	Battista	k1gMnSc1
Lulli	Lulle	k1gFnSc4
Přezdívky	přezdívka	k1gFnSc2
</s>
<s>
Baptiste	Baptist	k1gMnSc5
<g/>
,	,	kIx,
Le	Le	k1gMnSc1
grand	grand	k1gMnSc1
baladin	baladin	k2eAgMnSc1d1
<g/>
(	(	kIx(
<g/>
velký	velký	k2eAgMnSc1d1
pouliční	pouliční	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1632	#num#	k4
Florencie	Florencie	k1gFnSc2
Původ	původ	k1gInSc1
</s>
<s>
Toskánské	toskánský	k2eAgNnSc1d1
velkovévodství	velkovévodství	k1gNnSc1
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Itálie	Itálie	k1gFnSc1
<g/>
)	)	kIx)
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1687	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
54	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Paříž	Paříž	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
gangréna	gangréna	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Bazilika	bazilika	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Vítězné	vítězný	k2eAgInPc4d1
Žánry	žánr	k1gInPc4
</s>
<s>
klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
houslista	houslista	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
housle	housle	k1gFnPc4
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
BellérophonIsisThéseusMěšťák	BellérophonIsisThéseusMěšťák	k1gMnSc1
šlechticem	šlechtic	k1gMnSc7
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Madeleine	Madeleine	k1gFnSc1
Lambert	Lambert	k1gMnSc1
(	(	kIx(
<g/>
od	od	k7c2
1662	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Louis	Louis	k1gMnSc1
LullyJean-Baptiste	LullyJean-Baptist	k1gMnSc5
LullyJean-Louis	LullyJean-Louis	k1gFnPc7
Lully	Lulla	k1gFnSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1646	#num#	k4
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
de	de	k?
Lully	Lull	k1gMnPc7
(	(	kIx(
<g/>
[	[	kIx(
<g/>
ʒ	ʒ	k?
<g/>
.	.	kIx.
<g/>
ba	ba	k9
<g/>
.	.	kIx.
<g/>
tist	tist	k?
de	de	k?
ly	ly	k?
<g/>
.	.	kIx.
<g/>
li	li	k8xS
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
,	,	kIx,
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Giovanni	Giovanň	k1gMnSc3
Battista	Battista	k1gMnSc1
Lulli	Lull	k1gMnSc3
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1632	#num#	k4
Florencie	Florencie	k1gFnSc2
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1687	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
italského	italský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
od	od	k7c2
roku	rok	k1gInSc2
1662	#num#	k4
zcela	zcela	k6eAd1
ovládal	ovládat	k5eAaImAgInS
francouzskou	francouzský	k2eAgFnSc4d1
hudební	hudební	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejvýznamnějším	významný	k2eAgMnPc3d3
francouzským	francouzský	k2eAgMnPc3d1
skladatelům	skladatel	k1gMnPc3
barokního	barokní	k2eAgNnSc2d1
období	období	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
faktickým	faktický	k2eAgMnSc7d1
tvůrcem	tvůrce	k1gMnSc7
francouzské	francouzský	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
opery	opera	k1gFnSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
první	první	k4xOgFnSc2
francouzské	francouzský	k2eAgFnSc2d1
opery	opera	k1gFnSc2
Cadmus	Cadmus	k1gMnSc1
et	et	k?
Hermione	Hermion	k1gInSc5
z	z	k7c2
roku	rok	k1gInSc2
1673	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
francouzského	francouzský	k2eAgInSc2d1
typu	typ	k1gInSc2
operní	operní	k2eAgFnSc2d1
ouvertury	ouvertura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lully	Lull	k1gMnPc4
také	také	k9
rozvinul	rozvinout	k5eAaPmAgMnS
recitativ	recitativ	k1gInSc4
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Nahradil	nahradit	k5eAaPmAgMnS
recitativo	recitativa	k1gFnSc5
secco	secco	k1gNnSc1
(	(	kIx(
<g/>
suchý	suchý	k2eAgInSc1d1
recitativ	recitativ	k1gInSc1
<g/>
)	)	kIx)
novou	nový	k2eAgFnSc7d1
formou	forma	k1gFnSc7
recitativo	recitativa	k1gFnSc5
accompagnato	accompagnato	k6eAd1
(	(	kIx(
<g/>
doprovázený	doprovázený	k2eAgInSc1d1
recitativ	recitativ	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
jsou	být	k5eAaImIp3nP
typické	typický	k2eAgNnSc1d1
rytmické	rytmický	k2eAgInPc1d1
variace	variace	k1gFnPc4
a	a	k8xC
přesná	přesný	k2eAgNnPc4d1
umístění	umístění	k1gNnSc4
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vytvořil	vytvořit	k5eAaPmAgInS
nový	nový	k2eAgInSc4d1
styl	styl	k1gInSc4
deklamace	deklamace	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
přesně	přesně	k6eAd1
uzpůsoben	uzpůsobit	k5eAaPmNgMnS
pro	pro	k7c4
francouzštinu	francouzština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zpěvu	zpěv	k1gInSc6
se	se	k3xPyFc4
díky	díky	k7c3
této	tento	k3xDgFnSc3
invenci	invence	k1gFnSc3
vyrovnává	vyrovnávat	k5eAaImIp3nS
rozdíl	rozdíl	k1gInSc1
mezi	mezi	k7c7
árií	árie	k1gFnSc7
a	a	k8xC
recitativem	recitativ	k1gInSc7
<g/>
,	,	kIx,
zpěv	zpěv	k1gInSc1
pak	pak	k6eAd1
zní	znět	k5eAaImIp3nS
více	hodně	k6eAd2
kontinuálně	kontinuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Patřil	patřit	k5eAaImAgMnS
mezi	mezi	k7c4
několik	několik	k4yIc4
málo	málo	k6eAd1
důvěrných	důvěrný	k2eAgInPc2d1
přátel	přítel	k1gMnPc2
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
nejvlivnějším	vlivný	k2eAgMnSc7d3
a	a	k8xC
nejúspěšnějším	úspěšný	k2eAgMnSc7d3
skladatelem	skladatel	k1gMnSc7
v	v	k7c6
období	období	k1gNnSc6
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
a	a	k8xC
ve	v	k7c6
francouzských	francouzský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
vůbec	vůbec	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Téměř	téměř	k6eAd1
tři	tři	k4xCgNnPc4
staletí	staletí	k1gNnPc4
byl	být	k5eAaImAgInS
Lully	Lully	k1gMnSc1
znám	známý	k2eAgMnSc1d1
spíše	spíše	k9
kvůli	kvůli	k7c3
neobvyklým	obvyklý	k2eNgFnPc3d1
okolnostem	okolnost	k1gFnPc3
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
dirigování	dirigování	k1gNnSc6
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
zkoušek	zkouška	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1687	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Lully	Lully	k1gMnSc1
prudce	prudce	k6eAd1
udeřil	udeřit	k5eAaPmAgInS
bodcem	bodec	k1gInSc7
těžké	těžký	k2eAgFnSc2d1
barokní	barokní	k2eAgFnSc2d1
taktovky	taktovka	k1gFnSc2
do	do	k7c2
palce	palec	k1gInSc2
pravé	pravý	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1
se	se	k3xPyFc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
nedostatečné	dostatečný	k2eNgFnSc2d1
hygieny	hygiena	k1gFnSc2
zanítil	zanítit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
odmítal	odmítat	k5eAaImAgInS
včasnou	včasný	k2eAgFnSc4d1
amputaci	amputace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
nutná	nutný	k2eAgFnSc1d1
pro	pro	k7c4
zastavení	zastavení	k1gNnSc4
gangrény	gangréna	k1gFnSc2
<g/>
,	,	kIx,
zánět	zánět	k1gInSc1
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdní	pozdní	k2eAgFnSc1d1
amputace	amputace	k1gFnSc1
nohy	noha	k1gFnSc2
již	již	k6eAd1
nepomohla	pomoct	k5eNaPmAp3gFnS
a	a	k8xC
Lully	Lulla	k1gFnPc1
o	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
nový	nový	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
barokní	barokní	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vedl	vést	k5eAaImAgMnS
k	k	k7c3
hlubšímu	hluboký	k2eAgNnSc3d2
poznání	poznání	k1gNnSc3
Lullyho	Lully	k1gMnSc2
děl	dít	k5eAaBmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
Lullyho	Lully	k1gMnSc4
nejdůležitější	důležitý	k2eAgNnSc1d3
díla	dílo	k1gNnPc1
patří	patřit	k5eAaImIp3nP
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Moliè	Moliè	k1gMnSc7
Měšťák	měšťák	k1gMnSc1
šlechticem	šlechtic	k1gMnSc7
a	a	k8xC
Psyché	psyché	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Quinaultem	Quinault	k1gInSc7
pak	pak	k6eAd1
královská	královský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Atys	Atys	k1gInSc1
<g/>
,	,	kIx,
opera	opera	k1gFnSc1
muzikantů	muzikant	k1gMnPc2
Isis	Isis	k1gFnSc1
a	a	k8xC
mistrovské	mistrovský	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Armida	Armida	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
kompoziční	kompoziční	k2eAgInSc4d1
styl	styl	k1gInSc4
napodobovali	napodobovat	k5eAaImAgMnP
soudobí	soudobý	k2eAgMnPc1d1
hudební	hudební	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
rozsáhlému	rozsáhlý	k2eAgInSc3d1
dílu	díl	k1gInSc3
a	a	k8xC
hudebním	hudební	k2eAgFnPc3d1
invencím	invence	k1gFnPc3
ovlivnil	ovlivnit	k5eAaPmAgInS
Lully	Lulla	k1gFnPc4
i	i	k8xC
řadu	řada	k1gFnSc4
pozdějších	pozdní	k2eAgMnPc2d2
hudebních	hudební	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
G.	G.	kA
F.	F.	kA
Händela	Händel	k1gMnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
J.	J.	kA
S.	S.	kA
Bacha	Bacha	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
J.	J.	kA
<g/>
-	-	kIx~
<g/>
P.	P.	kA
Rameaua	Rameaua	k1gFnSc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Dětství	dětství	k1gNnSc4
a	a	k8xC
mládí	mládí	k1gNnSc4
</s>
<s>
Giovanni	Giovanň	k1gMnSc3
Battista	Battista	k1gMnSc1
Lulli	Lull	k1gMnSc3
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
v	v	k7c6
toskánské	toskánský	k2eAgFnSc6d1
Florencii	Florencie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giovanniho	Giovanni	k1gMnSc2
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
mlynář	mlynář	k1gMnSc1
Lorenzo	Lorenza	k1gFnSc5
di	di	k?
Maldo	Malda	k1gMnSc5
Lulli	Lull	k1gMnSc5
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc7
matkou	matka	k1gFnSc7
Caterina	Caterina	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
del	del	k?
Sera	srát	k5eAaImSgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
dětství	dětství	k1gNnSc2
projevoval	projevovat	k5eAaImAgInS
velké	velký	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
nadání	nadání	k1gNnSc4
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yQgMnSc3,k3yRgMnSc3,k3yIgMnSc3
nemusel	muset	k5eNaImAgMnS
pracovat	pracovat	k5eAaImF
v	v	k7c6
otcově	otcův	k2eAgInSc6d1
mlýně	mlýn	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
dostalo	dostat	k5eAaPmAgNnS
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
patřičného	patřičný	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
u	u	k7c2
františkánů	františkán	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
jej	on	k3xPp3gMnSc4
naučili	naučit	k5eAaPmAgMnP
hrát	hrát	k5eAaImF
na	na	k7c4
kytaru	kytara	k1gFnSc4
<g/>
,	,	kIx,
housle	housle	k1gFnPc4
a	a	k8xC
cembalo	cembalo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1646	#num#	k4
si	se	k3xPyFc3
nadaného	nadaný	k2eAgMnSc4d1
třináctiletého	třináctiletý	k2eAgMnSc4d1
chlapce	chlapec	k1gMnSc4
povšiml	povšimnout	k5eAaPmAgMnS
Roger	Roger	k1gMnSc1
Lotrinský	lotrinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
rytíř	rytíř	k1gMnSc1
de	de	k?
Guise	Guis	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
navštívil	navštívit	k5eAaPmAgMnS
Florencii	Florencie	k1gFnSc4
během	během	k7c2
místních	místní	k2eAgFnPc2d1
slavností	slavnost	k1gFnPc2
při	při	k7c6
své	svůj	k3xOyFgFnSc6
cestě	cesta	k1gFnSc6
z	z	k7c2
Malty	Malta	k1gFnSc2
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
hledal	hledat	k5eAaImAgMnS
někoho	někdo	k3yInSc4
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
konverzovat	konverzovat	k5eAaImF
v	v	k7c6
italštině	italština	k1gFnSc6
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
neteří	neteř	k1gFnSc7
Annou	Anna	k1gFnSc7
Marií	Maria	k1gFnSc7
Louisou	Louisa	k1gFnSc7
Orleánskou	orleánský	k2eAgFnSc7d1
<g/>
,	,	kIx,
dcerou	dcera	k1gFnSc7
Gastona	Gaston	k1gMnSc2
Orleánského	orleánský	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
Mladý	mladý	k1gMnSc1
Giovanni	Giovanen	k2eAgMnPc1d1
jej	on	k3xPp3gMnSc4
zaujal	zaujmout	k5eAaPmAgInS
natolik	natolik	k6eAd1
nejspíše	nejspíše	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
ovládal	ovládat	k5eAaImAgInS
zpěv	zpěv	k1gInSc4
<g/>
,	,	kIx,
tanec	tanec	k1gInSc4
i	i	k8xC
hru	hra	k1gFnSc4
na	na	k7c4
housle	housle	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytíř	Rytíř	k1gMnSc1
de	de	k?
Guise	Guise	k1gFnSc2
mu	on	k3xPp3gMnSc3
nabídl	nabídnout	k5eAaPmAgMnS
cestu	cesta	k1gFnSc4
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
pážetem	páže	k1gNnSc7
a	a	k8xC
učitelem	učitel	k1gMnSc7
italštiny	italština	k1gFnSc2
Anny	Anna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
Tuilerieském	Tuilerieský	k2eAgInSc6d1
paláci	palác	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
sídlil	sídlit	k5eAaImAgInS
dvůr	dvůr	k1gInSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
mladý	mladý	k1gMnSc1
Giovanni	Giovaneň	k1gFnSc3
rychle	rychle	k6eAd1
naučil	naučit	k5eAaPmAgMnS
francouzským	francouzský	k2eAgMnPc3d1
zvykům	zvyk	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
princezna	princezna	k1gFnSc1
Anna	Anna	k1gFnSc1
Marie	Marie	k1gFnSc1
neměla	mít	k5eNaImAgFnS
vlastní	vlastní	k2eAgMnPc4d1
hudebníky	hudebník	k1gMnPc4
či	či	k8xC
zpěváky	zpěvák	k1gMnPc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Giovanni	Giovanen	k2eAgMnPc1d1
Battista	Battista	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
kontaktu	kontakt	k1gInSc2
s	s	k7c7
předními	přední	k2eAgMnPc7d1
umělci	umělec	k1gMnPc7
z	z	k7c2
okruhu	okruh	k1gInSc2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
se	se	k3xPyFc4
zapojoval	zapojovat	k5eAaImAgInS
do	do	k7c2
jejich	jejich	k3xOp3gFnPc2
hudebních	hudební	k2eAgFnPc2d1
produkcí	produkce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
účast	účast	k1gFnSc4
na	na	k7c4
povstání	povstání	k1gNnSc4
proti	proti	k7c3
králi	král	k1gMnSc3
(	(	kIx(
<g/>
neboli	neboli	k8xC
frondě	fronda	k1gFnSc6
<g/>
)	)	kIx)
však	však	k9
musel	muset	k5eAaImAgInS
Gaston	Gaston	k1gInSc1
Orleánský	orleánský	k2eAgInSc1d1
uprchnout	uprchnout	k5eAaPmF
<g/>
,	,	kIx,
a	a	k8xC
následně	následně	k6eAd1
byla	být	k5eAaImAgFnS
i	i	k9
Anna	Anna	k1gFnSc1
Marie	Marie	k1gFnSc1
vyhoštěna	vyhoštěn	k2eAgFnSc1d1
do	do	k7c2
exilu	exil	k1gInSc2
v	v	k7c6
Saint-Fargeau	Saint-Fargeaus	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díky	díky	k7c3
svým	svůj	k3xOyFgFnPc3
známostem	známost	k1gFnPc3
s	s	k7c7
mnoha	mnoho	k4c7
významnými	významný	k2eAgMnPc7d1
hudebníky	hudebník	k1gMnPc7
dostal	dostat	k5eAaPmAgMnS
Giovanni	Giovaneň	k1gFnSc3
Battista	Battista	k1gMnSc1
v	v	k7c6
únoru	únor	k1gInSc6
1653	#num#	k4
příležitost	příležitost	k1gFnSc4
tančit	tančit	k5eAaImF
ve	v	k7c6
velkolepém	velkolepý	k2eAgNnSc6d1
Cambefortově	Cambefortův	k2eAgNnSc6d1
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
baletu	balet	k1gInSc2
Ballet	Ballet	k1gInSc1
de	de	k?
la	la	k1gNnSc1
Nuit	Nuit	k1gMnSc1
(	(	kIx(
<g/>
Balet	balet	k1gInSc1
Noci	noc	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgNnSc6,k3yIgNnSc6,k3yQgNnSc6
poprvé	poprvé	k6eAd1
na	na	k7c6
veřejnosti	veřejnost	k1gFnSc6
vystupoval	vystupovat	k5eAaImAgMnS
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
v	v	k7c6
roli	role	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
boha	bůh	k1gMnSc4
Apollóna	Apollón	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgMnSc1
si	se	k3xPyFc3
povšiml	povšimnout	k5eAaPmAgInS
Lulliho	Lulli	k1gMnSc4
výkonu	výkon	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ocenil	ocenit	k5eAaPmAgMnS
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
mladý	mladý	k2eAgMnSc1d1
Giovanni	Giovaneň	k1gFnSc6
stal	stát	k5eAaPmAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
jeho	jeho	k3xOp3gMnPc2
nejoblíbenějších	oblíbený	k2eAgMnPc2d3
<g />
.	.	kIx.
</s>
<s hack="1">
tanečníků	tanečník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
1653	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
královským	královský	k2eAgMnSc7d1
skladatelem	skladatel	k1gMnSc7
instrumentální	instrumentální	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
(	(	kIx(
<g/>
Compositeur	Compositeura	k1gFnPc2
de	de	k?
la	la	k1gNnSc4
Musique	Musiqu	k1gFnSc2
instrumentale	instrumental	k1gMnSc5
de	de	k?
la	la	k0
chambre	chambr	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
začala	začít	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc4
kariéra	kariéra	k1gFnSc1
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Dvorní	dvorní	k2eAgInSc1d1
balet	balet	k1gInSc1
a	a	k8xC
Moliè	Moliè	k1gMnSc5
</s>
<s>
Moliè	Moliè	k1gMnSc5
(	(	kIx(
<g/>
portrét	portrét	k1gInSc1
od	od	k7c2
Pierre	Pierr	k1gInSc5
Mignarda	Mignard	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
Dvorní	dvorní	k2eAgInSc1d1
balet	balet	k1gInSc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
ballet	ballet	k1gInSc1
de	de	k?
cour	cour	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
u	u	k7c2
francouzského	francouzský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
Velkého	velký	k2eAgNnSc2d1
století	století	k1gNnSc2
<g/>
[	[	kIx(
<g/>
p	p	k?
5	#num#	k4
<g/>
]	]	kIx)
velmi	velmi	k6eAd1
oblíbený	oblíbený	k2eAgInSc1d1
<g/>
;	;	kIx,
v	v	k7c6
baletech	balet	k1gInPc6
tančil	tančit	k5eAaImAgMnS
sám	sám	k3xTgMnSc1
král	král	k1gMnSc1
i	i	k8xC
dvořané	dvořan	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
6	#num#	k4
<g/>
]	]	kIx)
Lulli	Lulle	k1gFnSc6
se	se	k3xPyFc4
dvorních	dvorní	k2eAgNnPc2d1
představení	představení	k1gNnSc2
účastnil	účastnit	k5eAaImAgInS
jako	jako	k9
<g />
.	.	kIx.
</s>
<s hack="1">
tanečník	tanečník	k1gMnSc1
a	a	k8xC
také	také	k9
jako	jako	k9
hudebník	hudebník	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1658	#num#	k4
začal	začít	k5eAaPmAgMnS
hudbu	hudba	k1gFnSc4
pro	pro	k7c4
dvorní	dvorní	k2eAgInPc4d1
balety	balet	k1gInPc4
sám	sám	k3xTgInSc4
skládat	skládat	k5eAaImF
na	na	k7c4
texty	text	k1gInPc4
Isaaca	Isaaca	k1gMnSc1
de	de	k?
Benserada	Benserada	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
kterým	který	k3yQgInPc3,k3yRgInPc3,k3yIgInPc3
soustavně	soustavně	k6eAd1
spolupracoval	spolupracovat	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1669	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1661	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
superintendantem	superintendant	k1gMnSc7
královy	králův	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
(	(	kIx(
<g/>
Surintendant	Surintendant	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Musique	Musique	k1gFnPc2
de	de	k?
la	la	k1gNnSc4
Chambre	Chambr	k1gInSc5
du	du	k?
Roi	Roi	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
získal	získat	k5eAaPmAgMnS
francouzské	francouzský	k2eAgFnSc2d1
naturalizační	naturalizační	k2eAgFnSc2d1
listiny	listina	k1gFnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
si	se	k3xPyFc3
pofrancouzštil	pofrancouzštit	k5eAaPmAgMnS
jméno	jméno	k1gNnSc4
na	na	k7c4
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lullo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
lety	let	k1gInPc7
1664	#num#	k4
<g/>
–	–	k?
<g/>
1671	#num#	k4
spolupracoval	spolupracovat	k5eAaImAgInS
zejména	zejména	k9
s	s	k7c7
Moliè	Moliè	k1gInSc7
(	(	kIx(
<g/>
vlastním	vlastní	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Poquelin	Poquelin	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
společně	společně	k6eAd1
tvořili	tvořit	k5eAaImAgMnP
comédie-ballety	comédie-ballet	k2eAgFnPc4d1
(	(	kIx(
<g/>
baletní	baletní	k2eAgFnPc4d1
komedie	komedie	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přátelství	přátelství	k1gNnSc1
s	s	k7c7
Moliè	Moliè	k1gFnPc2
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
Lullyho	Lully	k1gMnSc4
pro	pro	k7c4
určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
klíčové	klíčový	k2eAgNnSc1d1
<g/>
;	;	kIx,
popularita	popularita	k1gFnSc1
Moliè	Moliè	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
s	s	k7c7
Lullyho	Lullyha	k1gFnSc5
hudbou	hudba	k1gFnSc7
byla	být	k5eAaImAgFnS
na	na	k7c6
velmi	velmi	k6eAd1
vysoké	vysoký	k2eAgFnSc3d1
úrovni	úroveň	k1gFnSc3
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
oba	dva	k4xCgMnPc1
upevňovali	upevňovat	k5eAaImAgMnP
svou	svůj	k3xOyFgFnSc4
pozici	pozice	k1gFnSc4
u	u	k7c2
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Brzy	brzy	k6eAd1
začali	začít	k5eAaPmAgMnP
být	být	k5eAaImF
známí	známý	k2eAgMnPc1d1
jako	jako	k8xS,k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Les	les	k1gInSc4
deux	deux	k1gInSc1
Jean-Baptistes	Jean-Baptistes	k1gInSc1
(	(	kIx(
<g/>
dva	dva	k4xCgMnPc1
Jean-Baptisté	Jean-Baptista	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Svou	svůj	k3xOyFgFnSc4
spolupráci	spolupráce	k1gFnSc4
dovedli	dovést	k5eAaPmAgMnP
k	k	k7c3
dokonalosti	dokonalost	k1gFnSc3
v	v	k7c6
komedii	komedie	k1gFnSc6
Le	Le	k1gFnSc2
Bourgeois	Bourgeois	k1gFnPc1
gentilhomme	gentilhomit	k5eAaPmRp1nP
(	(	kIx(
<g/>
česky	česky	k6eAd1
Měšťák	měšťák	k1gMnSc1
šlechticem	šlechtic	k1gMnSc7
<g/>
,	,	kIx,
1670	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
9	#num#	k4
<g/>
]	]	kIx)
Hra	hra	k1gFnSc1
Psyché	psyché	k1gFnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
sepsaná	sepsaný	k2eAgFnSc1d1
ve	v	k7c4
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
Philippe	Philipp	k1gInSc5
Quinaultem	Quinault	k1gInSc7
a	a	k8xC
Pierre	Pierr	k1gInSc5
Corneillem	Corneill	k1gInSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
17	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1671	#num#	k4
<g/>
,	,	kIx,
znamenala	znamenat	k5eAaImAgFnS
pro	pro	k7c4
Moliè	Moliè	k1gMnSc4
jeho	jeho	k3xOp3gMnPc3
největší	veliký	k2eAgInSc4d3
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
také	také	k9
znamenala	znamenat	k5eAaImAgFnS
konec	konec	k1gInSc4
přátelského	přátelský	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
a	a	k8xC
spolupráce	spolupráce	k1gFnSc2
mezi	mezi	k7c4
Lullym	Lullym	k1gInSc4
a	a	k8xC
Moliè	Moliè	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
následně	následně	k6eAd1
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Lullyho	Lullyha	k1gFnSc5
rivalem	rival	k1gMnSc7
Marc-Antoine	Marc-Antoin	k1gInSc5
Charpentierem	Charpentiero	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
viděl	vidět	k5eAaImAgInS
potenciál	potenciál	k1gInSc1
v	v	k7c6
něčem	něco	k3yInSc6
úplně	úplně	k6eAd1
jiném	jiný	k2eAgMnSc6d1
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
dvorní	dvorní	k2eAgInSc1d1
balet	balet	k1gInSc1
či	či	k8xC
baletní	baletní	k2eAgFnSc1d1
komedie	komedie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pierre	Pierr	k1gInSc5
Perrin	Perrin	k1gInSc1
<g/>
,	,	kIx,
básník	básník	k1gMnSc1
a	a	k8xC
libretista	libretista	k1gMnSc1
získal	získat	k5eAaPmAgMnS
privilegium	privilegium	k1gNnSc4
k	k	k7c3
založení	založení	k1gNnSc3
Académie	Académie	k1gFnSc2
royale	royala	k1gFnSc3
de	de	k?
musique	musique	k1gFnSc1
(	(	kIx(
<g/>
Královská	královský	k2eAgFnSc1d1
hudební	hudební	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
produkce	produkce	k1gFnSc1
začala	začít	k5eAaPmAgFnS
být	být	k5eAaImF
po	po	k7c6
počátečních	počáteční	k2eAgFnPc6d1
nejistotách	nejistota	k1gFnPc6
velice	velice	k6eAd1
úspěšná	úspěšný	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
přitáhlo	přitáhnout	k5eAaPmAgNnS
Lullyho	Lully	k1gMnSc4
pozornost	pozornost	k1gFnSc1
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
p	p	k?
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
zpočátku	zpočátku	k6eAd1
si	se	k3xPyFc3
totiž	totiž	k9
myslel	myslet	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
opera	opera	k1gFnSc1
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6
je	být	k5eAaImIp3nS
nesmysl	nesmysl	k1gInSc1
<g/>
,	,	kIx,
ale	ale	k8xC
postupně	postupně	k6eAd1
změnil	změnit	k5eAaPmAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
když	když	k8xS
viděl	vidět	k5eAaImAgMnS
zájem	zájem	k1gInSc4
krále	král	k1gMnSc2
i	i	k8xC
publika	publikum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tragédie	tragédie	k1gFnSc1
en	en	k?
musique	musique	k1gFnPc2
a	a	k8xC
Quinault	Quinaulta	k1gFnPc2
</s>
<s>
Perrin	Perrin	k1gInSc1
dostal	dostat	k5eAaPmAgInS
výhradní	výhradní	k2eAgNnSc4d1
právo	právo	k1gNnSc4
na	na	k7c6
uvádění	uvádění	k1gNnSc6
oper	opera	k1gFnPc2
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gMnSc7
spolupracovníkem	spolupracovník	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Robert	Robert	k1gMnSc1
Cambert	Cambert	k1gMnSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
sdílel	sdílet	k5eAaImAgMnS
post	post	k1gInSc4
ředitele	ředitel	k1gMnSc2
Académie	Académie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
12	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gFnSc1
společná	společný	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Pomone	Pomon	k1gInSc5
(	(	kIx(
<g/>
Pomona	Pomona	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
objevila	objevit	k5eAaPmAgFnS
na	na	k7c6
scéně	scéna	k1gFnSc6
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
března	březen	k1gInSc2
1671	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgInSc1d1
počin	počin	k1gInSc1
Les	les	k1gInSc1
peines	peines	k1gInSc1
et	et	k?
plaisirs	plaisirs	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
amour	amour	k1gMnSc1
sice	sice	k8xC
zaznamenaly	zaznamenat	k5eAaPmAgFnP
značný	značný	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
Lullyho	Lully	k1gMnSc4
nátlak	nátlak	k1gInSc4
na	na	k7c4
krále	král	k1gMnPc4
a	a	k8xC
bezohledné	bezohledný	k2eAgNnSc4d1
chování	chování	k1gNnSc4
Perrinových	Perrinová	k1gFnPc2
dvou	dva	k4xCgMnPc2
dalších	další	k2eAgMnPc2d1
partnerů	partner	k1gMnPc2
<g/>
,	,	kIx,
spekulantů	spekulant	k1gMnPc2
markýze	markýz	k1gMnSc2
de	de	k?
Sourdéac	Sourdéac	k1gFnSc4
a	a	k8xC
sira	sir	k1gMnSc4
de	de	k?
Champeron	Champeron	k1gMnSc1
<g/>
,	,	kIx,
mělo	mít	k5eAaImAgNnS
<g />
.	.	kIx.
</s>
<s hack="1">
za	za	k7c4
následek	následek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Perrin	Perrin	k1gInSc1
ocitl	ocitnout	k5eAaPmAgInS
ve	v	k7c6
vězení	vězení	k1gNnSc6
pro	pro	k7c4
dlužníky	dlužník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
vězení	vězení	k1gNnSc6
jej	on	k3xPp3gMnSc4
navštívil	navštívit	k5eAaPmAgMnS
Lully	Lull	k1gMnPc4
a	a	k8xC
nabídl	nabídnout	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
pomoc	pomoc	k1gFnSc4
(	(	kIx(
<g/>
doživotní	doživotní	k2eAgFnSc4d1
rentu	renta	k1gFnSc4
<g/>
)	)	kIx)
výměnou	výměna	k1gFnSc7
za	za	k7c4
převedení	převedení	k1gNnSc4
privilegií	privilegium	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
p	p	k?
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1672	#num#	k4
schválil	schválit	k5eAaPmAgMnS
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
převedení	převedení	k1gNnSc1
privilegia	privilegium	k1gNnSc2
pro	pro	k7c4
uvádění	uvádění	k1gNnSc4
oper	opera	k1gFnPc2
na	na	k7c4
Lullyho	Lully	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
převzal	převzít	k5eAaPmAgInS
Perrinovu	Perrinův	k2eAgFnSc4d1
a	a	k8xC
Cambertovu	Cambertův	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
ředitelem	ředitel	k1gMnSc7
Académie	Académie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Ihned	ihned	k6eAd1
v	v	k7c6
ní	on	k3xPp3gFnSc6
zavedl	zavést	k5eAaPmAgMnS
profesionální	profesionální	k2eAgMnSc1d1
a	a	k8xC
přísný	přísný	k2eAgInSc1d1
režim	režim	k1gInSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
zahrnoval	zahrnovat	k5eAaImAgMnS
i	i	k9
dohled	dohled	k1gInSc4
nad	nad	k7c7
osobním	osobní	k2eAgInSc7d1
životem	život	k1gInSc7
členů	člen	k1gMnPc2
souboru	soubor	k1gInSc2
–	–	k?
ti	ten	k3xDgMnPc1
si	se	k3xPyFc3
nemohli	moct	k5eNaImAgMnP
dovolit	dovolit	k5eAaPmF
nemoc	nemoc	k1gFnSc4
<g/>
,	,	kIx,
opilství	opilství	k1gNnSc4
nebo	nebo	k8xC
dokonce	dokonce	k9
těhotenství	těhotenství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k9
první	první	k4xOgMnSc1
také	také	k9
zavedl	zavést	k5eAaPmAgMnS
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
obsazování	obsazování	k1gNnSc4
žen	žena	k1gFnPc2
v	v	k7c6
tanečních	taneční	k2eAgFnPc6d1
rolích	role	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Následující	následující	k2eAgInSc4d1
čtyři	čtyři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
se	se	k3xPyFc4
Perrinovi	Perrinův	k2eAgMnPc1d1
společníci	společník	k1gMnPc1
snažili	snažit	k5eAaImAgMnP
o	o	k7c4
právní	právní	k2eAgNnSc4d1
zpochybnění	zpochybnění	k1gNnSc4
převodu	převod	k1gInSc2
privilegií	privilegium	k1gNnPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
neúspěšně	úspěšně	k6eNd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
14	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
privilegia	privilegium	k1gNnSc2
se	se	k3xPyFc4
Lully	Lulla	k1gFnSc2
stal	stát	k5eAaPmAgMnS
doslova	doslova	k6eAd1
francouzským	francouzský	k2eAgMnSc7d1
operním	operní	k2eAgMnSc7d1
monarchou	monarcha	k1gMnSc7
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
bez	bez	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
souhlasu	souhlas	k1gInSc2
nemohla	moct	k5eNaImAgFnS
být	být	k5eAaImF
ve	v	k7c6
Francii	Francie	k1gFnSc6
veřejně	veřejně	k6eAd1
uvedena	uvést	k5eAaPmNgFnS
žádná	žádný	k3yNgFnSc1
opera	opera	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
si	se	k3xPyFc3
získal	získat	k5eAaPmAgMnS
řadu	řada	k1gFnSc4
mocných	mocný	k2eAgMnPc2d1
nepřátel	nepřítel	k1gMnPc2
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1674	#num#	k4
byl	být	k5eAaImAgInS
dokonce	dokonce	k9
proveden	proveden	k2eAgInSc1d1
pokus	pokus	k1gInSc1
ho	on	k3xPp3gMnSc4
otrávit	otrávit	k5eAaPmF
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
avšak	avšak	k8xC
jakožto	jakožto	k8xS
důvěrný	důvěrný	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
krále	král	k1gMnSc4
měl	mít	k5eAaImAgMnS
zajištěnu	zajištěn	k2eAgFnSc4d1
ochranu	ochrana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tragédie	tragédie	k1gFnSc1
en	en	k?
musique	musiquat	k5eAaPmIp3nS
(	(	kIx(
<g/>
neboli	neboli	k8xC
pozdějším	pozdní	k2eAgInSc7d2
názvem	název	k1gInSc7
tragédie	tragédie	k1gFnSc2
lyrique	lyriqu	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
oficiálním	oficiální	k2eAgNnSc7d1
označením	označení	k1gNnSc7
pro	pro	k7c4
typ	typ	k1gInSc4
opery	opera	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
uvedl	uvést	k5eAaPmAgMnS
Lully	Lulla	k1gFnSc2
spolu	spolu	k6eAd1
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
spolupracovníkem	spolupracovník	k1gMnSc7
Philippe	Philipp	k1gInSc5
Quinaultem	Quinault	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Děj	děj	k1gInSc1
je	být	k5eAaImIp3nS
rozčleněn	rozčlenit	k5eAaPmNgInS
do	do	k7c2
pěti	pět	k4xCc2
dějství	dějství	k1gNnPc2
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
předchází	předcházet	k5eAaImIp3nS
prolog	prolog	k1gInSc1
(	(	kIx(
<g/>
ten	ten	k3xDgMnSc1
je	být	k5eAaImIp3nS
alegorií	alegorie	k1gFnSc7
na	na	k7c4
aktuální	aktuální	k2eAgFnSc4d1
politickou	politický	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
a	a	k8xC
oslavuje	oslavovat	k5eAaImIp3nS
největšího	veliký	k2eAgInSc2d3
z	z	k7c2
hrdinů	hrdina	k1gMnPc2
–	–	k?
Ludvíka	Ludvík	k1gMnSc4
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc7
tragédií	tragédie	k1gFnSc7
en	en	k?
musique	musique	k1gInSc4
<g/>
,	,	kIx,
uvedenou	uvedený	k2eAgFnSc4d1
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1673	#num#	k4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
opera	opera	k1gFnSc1
Cadmus	Cadmus	k1gMnSc1
et	et	k?
Hermione	Hermion	k1gInSc5
(	(	kIx(
<g/>
Kadmos	Kadmosa	k1gFnPc2
a	a	k8xC
Harmonia	harmonium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
první	první	k4xOgFnSc4
francouzskou	francouzský	k2eAgFnSc4d1
operu	opera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
provedení	provedení	k1gNnSc6
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
sklidili	sklidit	k5eAaPmAgMnP
Lully	Lulla	k1gFnPc4
s	s	k7c7
Quinaultem	Quinault	k1gInSc7
velký	velký	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
spolupráce	spolupráce	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1673	#num#	k4
uvedl	uvést	k5eAaPmAgMnS
Lully	Lulla	k1gFnSc2
každoročně	každoročně	k6eAd1
jednu	jeden	k4xCgFnSc4
operu	opera	k1gFnSc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
roku	rok	k1gInSc2
1681	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nenapsal	napsat	k5eNaPmAgMnS
žádnou	žádný	k3yNgFnSc4
<g/>
,	,	kIx,
a	a	k8xC
roku	rok	k1gInSc2
1686	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
sepsal	sepsat	k5eAaPmAgMnS
opery	opera	k1gFnSc2
dvě	dva	k4xCgFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
opera	opera	k1gFnSc1
Alceste	Alcest	k1gInSc5
<g/>
,	,	kIx,
ou	ou	k0
Le	Le	k1gMnPc7
triomphe	triomphe	k1gNnSc1
d	d	k?
<g/>
’	’	k?
<g/>
Alcide	Alcid	k1gInSc5
(	(	kIx(
<g/>
Alcesta	Alcesta	k1gFnSc1
aneb	aneb	k?
Triumf	triumf	k1gInSc1
Alcidy	Alcida	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1675	#num#	k4
<g/>
)	)	kIx)
pak	pak	k6eAd1
Thésée	Thésée	k1gNnSc1
(	(	kIx(
<g/>
Théseus	Théseus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1676	#num#	k4
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
premiéra	premiéra	k1gFnSc1
opery	opera	k1gFnSc2
Atys	Atysa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
známou	známá	k1gFnSc4
pod	pod	k7c7
přízviskem	přízvisko	k1gNnSc7
královská	královský	k2eAgFnSc1d1
opera	opera	k1gFnSc1
<g/>
;	;	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
nejoblíbenější	oblíbený	k2eAgFnSc4d3
operu	opera	k1gFnSc4
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Philippe	Philippat	k5eAaPmIp3nS
Quinault	Quinault	k1gInSc1
(	(	kIx(
<g/>
1670	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
autoři	autor	k1gMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
s	s	k7c7
Atysem	Atys	k1gInSc7
<g/>
,	,	kIx,
chtěli	chtít	k5eAaImAgMnP
zopakovat	zopakovat	k5eAaPmF
i	i	k9
následující	následující	k2eAgInSc4d1
rok	rok	k1gInSc4
s	s	k7c7
novou	nový	k2eAgFnSc7d1
operou	opera	k1gFnSc7
Isis	Isis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Náklady	náklad	k1gInPc1
na	na	k7c4
její	její	k3xOp3gFnSc4
provedení	provedení	k1gNnSc1
vystoupaly	vystoupat	k5eAaPmAgFnP
o	o	k7c4
polovinu	polovina	k1gFnSc4
výše	výše	k1gFnSc2,k1gFnSc2wB
než	než	k8xS
v	v	k7c6
případě	případ	k1gInSc6
Atyse	Atyse	k1gFnSc1
(	(	kIx(
<g/>
suma	suma	k1gFnSc1
vyplacená	vyplacený	k2eAgFnSc1d1
na	na	k7c4
premiéru	premiéra	k1gFnSc4
Isis	Isis	k1gFnSc1
činila	činit	k5eAaImAgFnS
151	#num#	k4
780	#num#	k4
livrů	livr	k1gInPc2
60	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
sous	sous	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Hudebně	hudebně	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
Lullyho	Lully	k1gMnSc4
nejlepší	dobrý	k2eAgFnSc4d3
operu	opera	k1gFnSc4
(	(	kIx(
<g/>
získala	získat	k5eAaPmAgFnS
přízvisko	přízvisko	k1gNnSc4
opera	opera	k1gFnSc1
muzikantů	muzikant	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
avšak	avšak	k8xC
u	u	k7c2
dvora	dvůr	k1gInSc2
zcela	zcela	k6eAd1
propadla	propadnout	k5eAaPmAgFnS
kvůli	kvůli	k7c3
skandálu	skandál	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
její	její	k3xOp3gFnSc4
uvedení	uvedení	k1gNnSc1
vyvolalo	vyvolat	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Již	již	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
případě	případ	k1gInSc6
Atyse	Atyse	k1gFnSc2
se	se	k3xPyFc4
objevovaly	objevovat	k5eAaImAgFnP
veřejné	veřejný	k2eAgFnPc1d1
i	i	k8xC
soukromé	soukromý	k2eAgFnPc1d1
kritiky	kritika	k1gFnPc1
libretisty	libretista	k1gMnSc2
Philippa	Philipp	k1gMnSc2
Quinaulta	Quinault	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
kulminovaly	kulminovat	k5eAaImAgFnP
právě	právě	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
premiéry	premiéra	k1gFnSc2
Isis	Isis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Nebylo	být	k5eNaImAgNnS
tedy	tedy	k9
náhodou	náhoda	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
jinak	jinak	k6eAd1
konvenční	konvenční	k2eAgFnSc6d1
zápletce	zápletka	k1gFnSc6
byly	být	k5eAaImAgFnP
v	v	k7c6
postavě	postava	k1gFnSc6
nesympatické	sympatický	k2eNgFnSc6d1
<g/>
,	,	kIx,
chorobně	chorobně	k6eAd1
žárlivé	žárlivý	k2eAgFnPc1d1
Junony	Juno	k1gFnPc1
spatřovány	spatřován	k2eAgInPc4d1
rysy	rys	k1gInPc4
madame	madame	k1gFnSc2
de	de	k?
Montespan	Montespan	k1gInSc1
a	a	k8xC
její	její	k3xOp3gFnSc1
rivalka	rivalka	k1gFnSc1
<g/>
,	,	kIx,
markýza	markýza	k1gFnSc1
de	de	k?
Ludres	Ludresa	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
tom	ten	k3xDgInSc6
čase	čas	k1gInSc6
milenkou	milenka	k1gFnSc7
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
rozpoznána	rozpoznat	k5eAaPmNgFnS
v	v	k7c6
postavě	postava	k1gFnSc6
Íó	Íó	k1gFnSc1
/	/	kIx~
Isis	Isis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
král	král	k1gMnSc1
pak	pak	k6eAd1
byl	být	k5eAaImAgMnS
přirozeně	přirozeně	k6eAd1
ztotožňován	ztotožňován	k2eAgMnSc1d1
s	s	k7c7
Jupiterem	Jupiter	k1gMnSc7
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
pojetí	pojetí	k1gNnSc1
také	také	k6eAd1
nebylo	být	k5eNaImAgNnS
jednoznačně	jednoznačně	k6eAd1
lichotivé	lichotivý	k2eAgNnSc1d1
(	(	kIx(
<g/>
podle	podle	k7c2
belgického	belgický	k2eAgMnSc2d1
historika	historik	k1gMnSc2
Manuela	Manuel	k1gMnSc2
Couvreura	Couvreur	k1gMnSc2
je	být	k5eAaImIp3nS
Jupiter	Jupiter	k1gMnSc1
v	v	k7c6
Isis	Isis	k1gFnSc1
„	„	k?
<g/>
karikaturou	karikatura	k1gFnSc7
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
směšnou	směšný	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Že	že	k8xS
byl	být	k5eAaImAgMnS
tento	tento	k3xDgInSc4
výklad	výklad	k1gInSc4
skutečně	skutečně	k6eAd1
v	v	k7c6
oběhu	oběh	k1gInSc6
<g/>
,	,	kIx,
dokládá	dokládat	k5eAaImIp3nS
například	například	k6eAd1
dobová	dobový	k2eAgFnSc1d1
korespondence	korespondence	k1gFnSc1
madame	madame	k1gFnSc2
de	de	k?
Sévigné	Sévigná	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
jak	jak	k8xC,k8xS
informoval	informovat	k5eAaBmAgMnS
Svatého	svatý	k2eAgMnSc4d1
otce	otec	k1gMnSc4
apoštolský	apoštolský	k2eAgMnSc1d1
nuncius	nuncius	k1gMnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
„	„	k?
<g/>
zalíbení	zalíbení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc4,k3yIgNnSc4,k3yRgNnSc4
madame	madame	k1gFnSc1
de	de	k?
Montespan	Montespan	k1gMnSc1
v	v	k7c6
této	tento	k3xDgFnSc6
opeře	opera	k1gFnSc6
nalezla	naleznout	k5eAaPmAgFnS,k5eAaBmAgFnS
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
<g />
.	.	kIx.
</s>
<s hack="1">
natolik	natolik	k6eAd1
malé	malý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
zhlédla	zhlédnout	k5eAaPmAgFnS
jen	jen	k9
několikrát	několikrát	k6eAd1
<g/>
;	;	kIx,
některé	některý	k3yIgFnPc4
situace	situace	k1gFnPc4
zápletky	zápletka	k1gFnSc2
nejen	nejen	k6eAd1
že	že	k8xS
neoceňuje	oceňovat	k5eNaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
dokonce	dokonce	k9
je	on	k3xPp3gInPc4
shledává	shledávat	k5eAaImIp3nS
nepříjemnými	příjemný	k2eNgFnPc7d1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
Kvůli	kvůli	k7c3
domnělým	domnělý	k2eAgFnPc3d1
narážkám	narážka	k1gFnPc3
v	v	k7c6
textu	text	k1gInSc6
na	na	k7c4
skutečné	skutečný	k2eAgFnPc4d1
osoby	osoba	k1gFnPc4
ztratil	ztratit	k5eAaPmAgMnS
libretista	libretista	k1gMnSc1
Quinault	Quinault	k1gMnSc1
dočasně	dočasně	k6eAd1
přístup	přístup	k1gInSc4
ke	k	k7c3
dvoru	dvůr	k1gInSc3
<g/>
,	,	kIx,
proto	proto	k8xC
si	se	k3xPyFc3
musel	muset	k5eAaImAgMnS
Lully	Lulla	k1gFnPc4
najít	najít	k5eAaPmF
jiného	jiný	k2eAgMnSc4d1
spolupracovníka	spolupracovník	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lully	Lullo	k1gNnPc7
také	také	k6eAd1
propustil	propustit	k5eAaPmAgMnS
svého	svůj	k3xOyFgMnSc2
pomocného	pomocný	k2eAgMnSc2d1
dirigenta	dirigent	k1gMnSc2
Jeana-Françoise	Jeana-Françoise	k1gFnSc2
Lallouetta	Lallouett	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
vychloubal	vychloubat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
nejlepších	dobrý	k2eAgFnPc2d3
částí	část	k1gFnPc2
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
uvedl	uvést	k5eAaPmAgInS
Lully	Lull	k1gInPc7
operu	opera	k1gFnSc4
Psyché	psyché	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
jako	jako	k8xS,k8xC
libreto	libreto	k1gNnSc4
použil	použít	k5eAaPmAgInS
přepracovaný	přepracovaný	k2eAgInSc1d1
text	text	k1gInSc1
z	z	k7c2
dřívější	dřívější	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
Moliè	Moliè	k1gMnSc5
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Quinaultem	Quinault	k1gInSc7
a	a	k8xC
P.	P.	kA
Corneillem	Corneillo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1671	#num#	k4
<g/>
;	;	kIx,
úpravu	úprava	k1gFnSc4
textu	text	k1gInSc2
hry	hra	k1gFnSc2
na	na	k7c4
libreto	libreto	k1gNnSc4
provedl	provést	k5eAaPmAgMnS
Thomas	Thomas	k1gMnSc1
Corneille	Corneille	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Zcela	zcela	k6eAd1
nové	nový	k2eAgNnSc4d1
libreto	libreto	k1gNnSc4
Lully	Lulla	k1gFnSc2
potřeboval	potřebovat	k5eAaImAgMnS
až	až	k6eAd1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
1679	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
případě	případ	k1gInSc6
Bellérophona	Bellérophon	k1gMnSc2
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
opět	opět	k6eAd1
sepsal	sepsat	k5eAaPmAgMnS
Thomas	Thomas	k1gMnSc1
Corneille	Corneille	k1gFnSc2
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
Bernardem	Bernard	k1gMnSc7
de	de	k?
Fontenelle	Fontenelle	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1680	#num#	k4
se	se	k3xPyFc4
již	již	k6eAd1
mohl	moct	k5eAaImAgInS
Phillippe	Phillipp	k1gInSc5
Quinault	Quinault	k2eAgInSc1d1
ke	k	k7c3
dvoru	dvůr	k1gInSc3
vrátit	vrátit	k5eAaPmF
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
veřejně	veřejně	k6eAd1
„	„	k?
<g/>
se	s	k7c7
slzami	slza	k1gFnPc7
v	v	k7c6
očích	oko	k1gNnPc6
<g/>
“	“	k?
požádal	požádat	k5eAaPmAgInS
krále	král	k1gMnSc4
o	o	k7c4
odpuštění	odpuštění	k1gNnSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
mohl	moct	k5eAaImAgMnS
tak	tak	k6eAd1
opět	opět	k6eAd1
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
Lullym	Lullym	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc7
nová	nový	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Proserpine	Proserpin	k1gInSc5
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vzorovou	vzorový	k2eAgFnSc7d1
francouzskou	francouzský	k2eAgFnSc7d1
tragédií	tragédie	k1gFnSc7
en	en	k?
musique	musique	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
16	#num#	k4
<g/>
]	]	kIx)
Sbor	sbor	k1gInSc1
zde	zde	k6eAd1
hraje	hrát	k5eAaImIp3nS
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
přítomen	přítomen	k2eAgMnSc1d1
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
polovině	polovina	k1gFnSc3
celkové	celkový	k2eAgFnSc2d1
délky	délka	k1gFnSc2
opery	opera	k1gFnSc2
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
opera	opera	k1gFnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
bohatou	bohatý	k2eAgFnSc7d1
orchestrací	orchestrace	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Až	až	k9
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1682	#num#	k4
<g/>
)	)	kIx)
uvedl	uvést	k5eAaPmAgMnS
Lully	Lull	k1gInPc4
s	s	k7c7
Quinaultem	Quinault	k1gInSc7
své	svůj	k3xOyFgNnSc4
další	další	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
operu	opera	k1gFnSc4
Persée	Persé	k1gFnSc2
(	(	kIx(
<g/>
Perseus	Perseus	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
divadle	divadlo	k1gNnSc6
Palais-Royal	Palais-Royal	k1gInSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Saint-Honoré	Saint-Honorý	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Premiéra	premiéra	k1gFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
narození	narození	k1gNnSc2
Ludvíka	Ludvík	k1gMnSc4
<g/>
,	,	kIx,
vévody	vévoda	k1gMnSc2
burgundského	burgundský	k2eAgMnSc2d1
<g/>
,	,	kIx,
vnuka	vnuk	k1gMnSc2
krále	král	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
témže	týž	k3xTgInSc6
roce	rok	k1gInSc6
se	se	k3xPyFc4
přesunul	přesunout	k5eAaPmAgInS
královský	královský	k2eAgInSc1d1
dvůr	dvůr	k1gInSc1
do	do	k7c2
zámku	zámek	k1gInSc2
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1683	#num#	k4
uskutečnila	uskutečnit	k5eAaPmAgFnS
premiéra	premiéra	k1gFnSc1
opery	opera	k1gFnSc2
Phaëton	Phaëton	k1gInSc1
(	(	kIx(
<g/>
Faethón	Faethón	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
známou	známá	k1gFnSc4
pod	pod	k7c7
označením	označení	k1gNnSc7
lidová	lidový	k2eAgFnSc1d1
opera	opera	k1gFnSc1
(	(	kIx(
<g/>
l	l	kA
<g/>
'	'	kIx"
<g/>
Opéra	Opéra	k1gMnSc1
du	du	k?
Peuple	Peuple	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Opera	opera	k1gFnSc1
Amadis	Amadis	k1gInSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gInSc4
námět	námět	k1gInSc4
vybral	vybrat	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
se	se	k3xPyFc4
vymyká	vymykat	k5eAaImIp3nS
klasickému	klasický	k2eAgInSc3d1
modelu	model	k1gInSc3
tragédie	tragédie	k1gFnSc2
en	en	k?
musique	musiqu	k1gFnSc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
námět	námět	k1gInSc1
se	se	k3xPyFc4
neopírá	opírat	k5eNaImIp3nS
o	o	k7c4
římskou	římský	k2eAgFnSc4d1
mytologii	mytologie	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
rytířský	rytířský	k2eAgInSc4d1
román	román	k1gInSc4
Amadís	Amadís	k1gInSc1
Waleský	waleský	k2eAgInSc1d1
<g/>
;	;	kIx,
Amadis	Amadis	k1gInSc1
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
v	v	k7c6
lednu	leden	k1gInSc6
1684	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Významná	významný	k2eAgFnSc1d1
árie	árie	k1gFnSc1
Bois	Boisa	k1gFnPc2
épais	épais	k1gInSc4
z	z	k7c2
opery	opera	k1gFnSc2
Amadis	Amadis	k1gFnSc2
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
uváděna	uvádět	k5eAaImNgFnS
samostatně	samostatně	k6eAd1
jako	jako	k8xC,k8xS
typická	typický	k2eAgFnSc1d1
ukázka	ukázka	k1gFnSc1
jedné	jeden	k4xCgFnSc2
z	z	k7c2
Lullyho	Lully	k1gMnSc2
nejlepších	dobrý	k2eAgFnPc2d3
monologových	monologový	k2eAgFnPc2d1
árií	árie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1685	#num#	k4
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
Roland	Roland	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
Amadis	Amadis	k1gFnSc1
není	být	k5eNaImIp3nS
dějově	dějově	k6eAd1
založen	založit	k5eAaPmNgInS
na	na	k7c6
římské	římský	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
Ariostově	Ariostův	k2eAgFnSc6d1
básni	báseň	k1gFnSc6
Orlando	Orlanda	k1gFnSc5
furioso	furioso	k1gNnSc1
(	(	kIx(
<g/>
Zuřivý	zuřivý	k2eAgInSc1d1
Roland	Roland	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
1686	#num#	k4
měla	mít	k5eAaImAgFnS
premiéru	premiéra	k1gFnSc4
Armide	Armid	k1gInSc5
(	(	kIx(
<g/>
Armida	Armida	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
Lullyho	Lully	k1gMnSc2
mistrovské	mistrovský	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
oper	opera	k1gFnPc2
Amadis	Amadis	k1gFnPc2
a	a	k8xC
Roland	Rolanda	k1gFnPc2
se	se	k3xPyFc4
děj	děj	k1gInSc1
Armidy	Armida	k1gFnSc2
neopírá	opírat	k5eNaImIp3nS
o	o	k7c4
předlohu	předloha	k1gFnSc4
z	z	k7c2
klasické	klasický	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
Tassův	Tassův	k2eAgInSc4d1
epos	epos	k1gInSc4
La	la	k1gNnSc2
Gerusalemme	Gerusalemme	k1gMnSc2
liberata	liberat	k1gMnSc2
(	(	kIx(
<g/>
Osvobozený	osvobozený	k2eAgInSc1d1
Jeruzalém	Jeruzalém	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
pravděpodobně	pravděpodobně	k6eAd1
nikdy	nikdy	k6eAd1
Armidu	Armida	k1gFnSc4
neviděl	vidět	k5eNaImAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
u	u	k7c2
něj	on	k3xPp3gMnSc2
Lully	Lulla	k1gMnSc2
upadl	upadnout	k5eAaPmAgMnS
v	v	k7c4
nemilost	nemilost	k1gFnSc4
kvůli	kvůli	k7c3
svým	svůj	k3xOyFgFnPc3
homosexuálním	homosexuální	k2eAgFnPc3d1
aférám	aféra	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
dostaly	dostat	k5eAaPmAgFnP
na	na	k7c4
veřejnost	veřejnost	k1gFnSc4
v	v	k7c6
zimě	zima	k1gFnSc6
roku	rok	k1gInSc2
1685	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
se	se	k3xPyFc4
<g/>
,	,	kIx,
patrně	patrně	k6eAd1
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Madame	madame	k1gFnSc1
de	de	k?
Maintenon	Maintenona	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
svého	svůj	k3xOyFgMnSc2
oblíbeného	oblíbený	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
distancoval	distancovat	k5eAaBmAgInS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
od	od	k7c2
Pařížské	pařížský	k2eAgFnSc2d1
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lullyho	Lullyze	k6eAd1
poslední	poslední	k2eAgFnSc1d1
opera	opera	k1gFnSc1
Achille	Achilles	k1gMnSc5
et	et	k?
Polyxè	Polyxè	k1gInSc5
(	(	kIx(
<g/>
Achilles	Achilles	k1gMnSc1
a	a	k8xC
Polyxena	Polyxena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
začal	začít	k5eAaPmAgInS
skládat	skládat	k5eAaImF
po	po	k7c6
velkém	velký	k2eAgInSc6d1
úspěchu	úspěch	k1gInSc6
Armidy	Armida	k1gFnSc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
na	na	k7c4
tento	tento	k3xDgInSc4
úspěch	úspěch	k1gInSc4
navázat	navázat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Libreto	libreto	k1gNnSc4
sepsal	sepsat	k5eAaPmAgMnS
Jean	Jean	k1gMnSc1
Galbert	Galbert	k1gMnSc1
de	de	k?
Campistron	Campistron	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
pro	pro	k7c4
Lullyho	Lully	k1gMnSc4
sepsal	sepsat	k5eAaPmAgMnS
libreto	libreto	k1gNnSc4
již	již	k6eAd1
předchozí	předchozí	k2eAgInSc1d1
rok	rok	k1gInSc1
k	k	k7c3
pastorale	pastorale	k6eAd1
héroï	héroï	k6eAd1
Acis	Acis	k1gInSc1
et	et	k?
Galatée	Galatée	k1gInSc1
(	(	kIx(
<g/>
Ákis	Ákis	k1gInSc1
a	a	k8xC
Galateia	Galateia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lully	Lulla	k1gFnPc4
před	před	k7c7
svou	svůj	k3xOyFgFnSc7
smrtí	smrt	k1gFnSc7
nestihl	stihnout	k5eNaPmAgMnS
operu	opera	k1gFnSc4
dokončit	dokončit	k5eAaPmF
<g/>
,	,	kIx,
zkomponoval	zkomponovat	k5eAaPmAgMnS
jí	jíst	k5eAaImIp3nS
pouze	pouze	k6eAd1
přibližně	přibližně	k6eAd1
jednu	jeden	k4xCgFnSc4
třetinu	třetina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
po	po	k7c4
Lullyho	Lully	k1gMnSc4
smrti	smrt	k1gFnSc2
dokončil	dokončit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
pomocný	pomocný	k2eAgMnSc1d1
dirigent	dirigent	k1gMnSc1
Pascal	Pascal	k1gMnSc1
Collasse	Collasse	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
pro	pro	k7c4
něj	on	k3xPp3gMnSc4
pracoval	pracovat	k5eAaImAgMnS
od	od	k7c2
roku	rok	k1gInSc2
1677	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opera	opera	k1gFnSc1
neměla	mít	k5eNaImAgFnS
přílišný	přílišný	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
,	,	kIx,
proto	proto	k8xC
byla	být	k5eAaImAgFnS
brzy	brzy	k6eAd1
stažena	stáhnout	k5eAaPmNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c4
dobu	doba	k1gFnSc4
patnácti	patnáct	k4xCc2
let	léto	k1gNnPc2
byl	být	k5eAaImAgMnS
Lully	Lulla	k1gFnSc2
prakticky	prakticky	k6eAd1
jediným	jediný	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
oper	opera	k1gFnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
sám	sám	k3xTgMnSc1
tedy	tedy	k9
vytvořil	vytvořit	k5eAaPmAgMnS
národní	národní	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
díla	dílo	k1gNnSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
inspirací	inspirace	k1gFnSc7
pro	pro	k7c4
mnoho	mnoho	k4c4
dalších	další	k2eAgMnPc2d1
evropských	evropský	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
<g/>
;	;	kIx,
například	například	k6eAd1
pro	pro	k7c4
Rameaua	Rameauus	k1gMnSc4
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
Purcella	Purcella	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Lullyho	Lullyze	k6eAd1
erb	erb	k1gInSc1
</s>
<s>
Lully	Lull	k1gMnPc4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Francouzem	Francouz	k1gMnSc7
v	v	k7c6
prosinci	prosinec	k1gInSc6
1661	#num#	k4
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gFnSc1
naturalizační	naturalizační	k2eAgFnSc1d1
listina	listina	k1gFnSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
pofrancouzštělé	pofrancouzštělý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
„	„	k?
<g/>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
de	de	k?
Lully	Lulla	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1662	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
kostele	kostel	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Eustacha	Eustach	k1gMnSc2
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Madeleine	Madeleine	k1gFnSc7
Lambertovou	Lambertová	k1gFnSc7
(	(	kIx(
<g/>
1642	#num#	k4
<g/>
–	–	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1720	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
dcerou	dcera	k1gFnSc7
Michela	Michel	k1gMnSc2
Lamberta	Lambert	k1gMnSc2
<g/>
,	,	kIx,
významného	významný	k2eAgMnSc2d1
francouzského	francouzský	k2eAgMnSc2d1
pěveckého	pěvecký	k2eAgMnSc2d1
mistra	mistr	k1gMnSc2
a	a	k8xC
theorbisty	theorbista	k1gMnSc2
působícího	působící	k2eAgMnSc2d1
na	na	k7c6
královském	královský	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
a	a	k8xC
Lullyho	Lully	k1gMnSc4
blízkého	blízký	k2eAgMnSc4d1
spolupracovníka	spolupracovník	k1gMnSc4
a	a	k8xC
spoluautora	spoluautor	k1gMnSc4
<g/>
,	,	kIx,
a	a	k8xC
neteří	teřet	k5eNaImIp3nP
významné	významný	k2eAgFnPc4d1
dvorní	dvorní	k2eAgFnPc4d1
zpěvačky	zpěvačka	k1gFnPc4
Hilaire	Hilair	k1gInSc5
Dupuis	Dupuis	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
souhlas	souhlas	k1gInSc4
se	s	k7c7
sňatkem	sňatek	k1gInSc7
vyjádřil	vyjádřit	k5eAaPmAgMnS
podpisem	podpis	k1gInSc7
svatební	svatební	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
i	i	k8xC
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
a	a	k8xC
další	další	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
královny	královna	k1gFnSc2
Anna	Anna	k1gFnSc1
a	a	k8xC
Marie	Marie	k1gFnSc1
Terezie	Terezie	k1gFnSc2
či	či	k8xC
ministr	ministr	k1gMnSc1
Colbert	Colbert	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
Měli	mít	k5eAaImAgMnP
spolu	spolu	k6eAd1
šest	šest	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
a	a	k8xC
tři	tři	k4xCgFnPc4
dcery	dcera	k1gFnPc4
<g/>
,	,	kIx,
narozené	narozený	k2eAgFnPc4d1
v	v	k7c6
rychlém	rychlý	k2eAgInSc6d1
sledu	sled	k1gInSc6
v	v	k7c6
letech	let	k1gInPc6
1663	#num#	k4
<g/>
–	–	k?
<g/>
1668	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
p	p	k?
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všichni	všechen	k3xTgMnPc1
tři	tři	k4xCgMnPc4
synové	syn	k1gMnPc1
(	(	kIx(
<g/>
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
Jean-Baptiste	Jean-Baptist	k1gMnSc5
<g/>
,	,	kIx,
Jean-Louis	Jean-Louis	k1gFnPc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
hudebníky	hudebník	k1gMnPc7
na	na	k7c6
královském	královský	k2eAgInSc6d1
dvoře	dvůr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lully	Lulla	k1gFnPc4
předstíral	předstírat	k5eAaImAgInS
šlechtický	šlechtický	k2eAgInSc1d1
původ	původ	k1gInSc1
již	již	k6eAd1
od	od	k7c2
příchodu	příchod	k1gInSc2
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
ten	ten	k3xDgInSc1
však	však	k9
byl	být	k5eAaImAgInS
veřejně	veřejně	k6eAd1
zpochybňován	zpochybňovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
jej	on	k3xPp3gMnSc4
však	však	k9
nepřímo	přímo	k6eNd1
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
když	když	k8xS
povolil	povolit	k5eAaPmAgInS
roku	rok	k1gInSc2
1681	#num#	k4
Lullymu	Lullym	k1gInSc2
zakoupit	zakoupit	k5eAaPmF
hodnost	hodnost	k1gFnSc4
sekretářem	sekretář	k1gMnSc7
krále	král	k1gMnSc2
(	(	kIx(
<g/>
Sécrétaire	Sécrétair	k1gInSc5
du	du	k?
roi	roi	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
spojena	spojit	k5eAaPmNgFnS
se	s	k7c7
šlechtictvím	šlechtictví	k1gNnSc7
a	a	k8xC
dávala	dávat	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
nárok	nárok	k1gInSc4
na	na	k7c6
oslovení	oslovení	k1gNnSc6
Monsieur	Monsieur	k1gMnSc1
de	de	k?
Lully	Lulla	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
podepisoval	podepisovat	k5eAaImAgMnS
jako	jako	k9
Monsieur	Monsieur	k1gMnSc1
de	de	k?
Lully	Lulla	k1gFnSc2
<g/>
,	,	kIx,
escuyer	escuyer	k1gMnSc1
<g/>
,	,	kIx,
conseiller	conseiller	k1gMnSc1
<g/>
,	,	kIx,
Secrétaire	Secrétair	k1gInSc5
du	du	k?
Roy	Roy	k1gMnSc1
<g/>
,	,	kIx,
Maison	Maison	k1gMnSc1
<g/>
,	,	kIx,
Couronne	Couronn	k1gInSc5
de	de	k?
France	Franc	k1gMnSc2
&	&	k?
de	de	k?
ses	ses	k?
Finances	Finances	k1gInSc1
<g/>
,	,	kIx,
&	&	k?
Sur-Intendant	Sur-Intendant	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
Musique	Musique	k1gNnPc2
de	de	k?
sa	sa	k?
Majesté	Majestý	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
pozdější	pozdní	k2eAgFnSc1d2
snaha	snaha	k1gFnSc1
zakoupit	zakoupit	k5eAaPmF
statek	statek	k1gInSc4
a	a	k8xC
titul	titul	k1gInSc4
hraběte	hrabě	k1gMnSc2
de	de	k?
Grignon	Grignon	k1gInSc1
vyšla	vyjít	k5eAaPmAgFnS
naprázdno	naprázdno	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
patřil	patřit	k5eAaImAgInS
mezi	mezi	k7c4
několik	několik	k4yIc4
málo	málo	k6eAd1
důvěrných	důvěrný	k2eAgInPc2d1
přátel	přítel	k1gMnPc2
krále	král	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
měl	mít	k5eAaImAgMnS
Lully	Lull	k1gInPc4
množství	množství	k1gNnSc2
sexuálních	sexuální	k2eAgMnPc2d1
i	i	k8xC
finančních	finanční	k2eAgInPc2d1
skandálů	skandál	k1gInPc2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
kterým	který	k3yQgInPc3,k3yIgInPc3,k3yRgInPc3
několikrát	několikrát	k6eAd1
upadl	upadnout	k5eAaPmAgMnS
v	v	k7c4
královu	králův	k2eAgFnSc4d1
nemilost	nemilost	k1gFnSc4
<g/>
,	,	kIx,
vždy	vždy	k6eAd1
dokázal	dokázat	k5eAaPmAgMnS
nalézt	nalézt	k5eAaBmF,k5eAaPmF
cestu	cesta	k1gFnSc4
zpět	zpět	k6eAd1
do	do	k7c2
královy	králův	k2eAgFnSc2d1
přízně	přízeň	k1gFnSc2
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gNnSc1
přátelství	přátelství	k1gNnSc1
trvalo	trvat	k5eAaImAgNnS
téměř	téměř	k6eAd1
až	až	k9
do	do	k7c2
Lullyho	Lully	k1gMnSc2
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
trvalejší	trvalý	k2eAgNnSc4d2
ochlazení	ochlazení	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
se	se	k3xPyFc4
však	však	k9
nedotklo	dotknout	k5eNaPmAgNnS
Lullyho	Lully	k1gMnSc2
postavení	postavení	k1gNnSc1
u	u	k7c2
králova	králův	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lully	Lulla	k1gFnPc1
se	se	k3xPyFc4
v	v	k7c6
intimní	intimní	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
projevoval	projevovat	k5eAaImAgMnS
jako	jako	k9
bisexuál	bisexuál	k1gMnSc1
<g/>
;	;	kIx,
jeho	jeho	k3xOp3gInPc1
sklony	sklon	k1gInPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
staly	stát	k5eAaPmAgFnP
veřejným	veřejný	k2eAgNnSc7d1
tajemstvím	tajemství	k1gNnSc7
již	již	k6eAd1
před	před	k7c7
jeho	jeho	k3xOp3gInSc7
sňatkem	sňatek	k1gInSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
svědčí	svědčit	k5eAaImIp3nS
jarmareční	jarmareční	k2eAgFnSc1d1
píseň	píseň	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1662	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Největší	veliký	k2eAgInSc1d3
skandál	skandál	k1gInSc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
propukl	propuknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1685	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vyšel	vyjít	k5eAaPmAgInS
najevo	najevo	k6eAd1
Lullyho	Lully	k1gMnSc2
milenecký	milenecký	k2eAgInSc4d1
vztah	vztah	k1gInSc4
s	s	k7c7
pážetem	páže	k1gNnSc7
jménem	jméno	k1gNnSc7
Brunet	brunet	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
u	u	k7c2
něj	on	k3xPp3gMnSc2
žil	žít	k5eAaImAgMnS
coby	coby	k?
student	student	k1gMnSc1
hudby	hudba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
neváhal	váhat	k5eNaImAgMnS
dát	dát	k5eAaPmF
najevo	najevo	k6eAd1
svou	svůj	k3xOyFgFnSc4
podporu	podpora	k1gFnSc4
skladateli	skladatel	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
tehdejší	tehdejší	k2eAgFnSc2d1
Francie	Francie	k1gFnSc2
se	se	k3xPyFc4
i	i	k9
Lully	Lulla	k1gFnPc4
hlásil	hlásit	k5eAaImAgMnS
k	k	k7c3
římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
byl	být	k5eAaImAgInS
označován	označovat	k5eAaImNgInS
za	za	k7c4
sodomitu	sodomita	k1gMnSc4
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
muže	muž	k1gMnPc4
bludařských	bludařský	k2eAgInPc2d1
sklonů	sklon	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Výhrady	výhrada	k1gFnSc2
církve	církev	k1gFnSc2
vůči	vůči	k7c3
němu	on	k3xPp3gMnSc3
ilustruje	ilustrovat	k5eAaBmIp3nS
spor	spor	k1gInSc1
z	z	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Lully	Lulla	k1gFnSc2
umíral	umírat	k5eAaImAgInS
<g/>
:	:	kIx,
kněz	kněz	k1gMnSc1
mu	on	k3xPp3gMnSc3
vyčetl	vyčíst	k5eAaPmAgMnS
zhýralý	zhýralý	k2eAgInSc4d1
život	život	k1gInSc4
a	a	k8xC
především	především	k6eAd1
odmítl	odmítnout	k5eAaPmAgMnS
poskytnout	poskytnout	k5eAaPmF
svaté	svatý	k2eAgNnSc4d1
přijímání	přijímání	k1gNnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
neslíbí	slíbit	k5eNaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
zanechá	zanechat	k5eAaPmIp3nS
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
Lullyho	Lully	k1gMnSc4
blízcí	blízký	k2eAgMnPc1d1
hrozili	hrozit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
budou	být	k5eAaImBp3nP
stěžovat	stěžovat	k5eAaImF
králi	král	k1gMnSc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
pařížský	pařížský	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
François	François	k1gFnSc2
Harlay	Harlay	k1gInPc4
de	de	k?
Champvallon	Champvallon	k1gInSc4
zpovědníkův	zpovědníkův	k2eAgInSc4d1
příkaz	příkaz	k1gInSc4
schválil	schválit	k5eAaPmAgInS
a	a	k8xC
Lully	Lulla	k1gMnSc2
se	se	k3xPyFc4
musel	muset	k5eAaImAgMnS
podřídit	podřídit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Lullyho	Lully	k1gMnSc2
tvorba	tvorba	k1gFnSc1
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
oproti	oproti	k7c3
většině	většina	k1gFnSc3
ostatních	ostatní	k2eAgMnPc2d1
barokních	barokní	k2eAgMnPc2d1
skladatelů	skladatel	k1gMnPc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
několik	několik	k4yIc4
málo	málo	k4c4
děl	dělo	k1gNnPc2
s	s	k7c7
duchovní	duchovní	k2eAgFnSc7d1
tematikou	tematika	k1gFnSc7
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
jde	jít	k5eAaImIp3nS
většinou	většina	k1gFnSc7
o	o	k7c4
dvorské	dvorský	k2eAgFnPc4d1
zakázky	zakázka	k1gFnPc4
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
nemohl	moct	k5eNaImAgMnS
vyhnout	vyhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Smrt	smrt	k1gFnSc1
</s>
<s>
Na	na	k7c4
počest	počest	k1gFnSc4
úspěšné	úspěšný	k2eAgFnSc2d1
operace	operace	k1gFnSc2
análního	anální	k2eAgInSc2d1
píštělu	píštěl	k1gInSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1686	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
následného	následný	k2eAgNnSc2d1
uzdravení	uzdravení	k1gNnSc2
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
bylo	být	k5eAaImAgNnS
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1687	#num#	k4
znovu	znovu	k6eAd1
uvedeno	uvést	k5eAaPmNgNnS
Lullyho	Lully	k1gMnSc2
Te	Te	k1gMnSc1
Deum	Deum	k1gMnSc1
v	v	k7c6
konventu	konvent	k1gInSc6
feuillantů	feuillant	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
provedení	provedení	k1gNnSc6
více	hodně	k6eAd2
než	než	k8xS
150	#num#	k4
zpěváků	zpěvák	k1gMnPc2
a	a	k8xC
instrumentalistů	instrumentalista	k1gMnPc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
dirigování	dirigování	k1gNnSc6
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
zkoušek	zkouška	k1gFnPc2
se	se	k3xPyFc4
Lully	Lull	k1gInPc7
prudce	prudko	k6eAd1
udeřil	udeřit	k5eAaPmAgInS
bodcem	bodec	k1gInSc7
těžké	těžký	k2eAgFnSc2d1
barokní	barokní	k2eAgFnSc2d1
taktovky	taktovka	k1gFnSc2
do	do	k7c2
palce	palec	k1gInSc2
pravé	pravý	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
důsledku	důsledek	k1gInSc6
nedostatečné	dostatečný	k2eNgFnSc2d1
hygieny	hygiena	k1gFnSc2
zanítil	zanítit	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
Lully	Lulla	k1gFnSc2
odmítal	odmítat	k5eAaImAgMnS
amputaci	amputace	k1gFnSc4
probodnutého	probodnutý	k2eAgInSc2d1
prstu	prst	k1gInSc2
(	(	kIx(
<g/>
a	a	k8xC
později	pozdě	k6eAd2
celé	celý	k2eAgFnPc4d1
nohy	noha	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
mu	on	k3xPp3gMnSc3
místní	místní	k2eAgFnSc1d1
i	i	k8xC
z	z	k7c2
ciziny	cizina	k1gFnSc2
přivolaní	přivolaný	k2eAgMnPc1d1
lékaři	lékař	k1gMnPc1
radili	radit	k5eAaImAgMnP
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
se	se	k3xPyFc4
zánět	zánět	k1gInSc1
rozšířil	rozšířit	k5eAaPmAgInS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
otravě	otrava	k1gFnSc3
krve	krev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zlepšení	zlepšení	k1gNnSc6
stavu	stav	k1gInSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
února	únor	k1gInSc2
nastalo	nastat	k5eAaPmAgNnS
ke	k	k7c3
konci	konec	k1gInSc3
měsíce	měsíc	k1gInSc2
rychlé	rychlý	k2eAgNnSc4d1
zhoršení	zhoršení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1687	#num#	k4
k	k	k7c3
sobě	se	k3xPyFc3
Lully	Lull	k1gMnPc4
povolal	povolat	k5eAaPmAgMnS
kněze	kněz	k1gMnSc4
se	s	k7c7
žádostí	žádost	k1gFnSc7
o	o	k7c4
udělení	udělení	k1gNnSc4
posledního	poslední	k2eAgNnSc2d1
pomazání	pomazání	k1gNnSc2
<g/>
;	;	kIx,
ten	ten	k3xDgMnSc1
souhlasil	souhlasit	k5eAaImAgMnS
s	s	k7c7
udělením	udělení	k1gNnSc7
pomazání	pomazání	k1gNnSc2
pod	pod	k7c7
podmínkou	podmínka	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k8xS,k8xC
pokání	pokání	k1gNnSc1
spálí	spálit	k5eAaPmIp3nS
rozpracovanou	rozpracovaný	k2eAgFnSc4d1
partituru	partitura	k1gFnSc4
své	svůj	k3xOyFgFnSc2
poslední	poslední	k2eAgFnSc2d1
opery	opera	k1gFnSc2
(	(	kIx(
<g/>
Achilles	Achilles	k1gMnSc1
a	a	k8xC
Polyxena	Polyxena	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lully	Lullo	k1gNnPc7
souhlasil	souhlasit	k5eAaImAgMnS
a	a	k8xC
partituru	partitura	k1gFnSc4
vhodil	vhodit	k5eAaPmAgInS
do	do	k7c2
ohně	oheň	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgInS
po	po	k7c6
odchodu	odchod	k1gInSc6
kněze	kněz	k1gMnSc2
dotázán	dotázán	k2eAgMnSc1d1
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
svých	svůj	k3xOyFgMnPc2
přátel	přítel	k1gMnPc2
<g/>
,	,	kIx,
proč	proč	k6eAd1
to	ten	k3xDgNnSc4
udělal	udělat	k5eAaPmAgMnS
<g/>
,	,	kIx,
Lully	Lull	k1gInPc4
mu	on	k3xPp3gMnSc3
prozradil	prozradit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
ještě	ještě	k9
jednu	jeden	k4xCgFnSc4
kopii	kopie	k1gFnSc4
partitury	partitura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
smrtelné	smrtelný	k2eAgFnSc6d1
posteli	postel	k1gFnSc6
<g/>
,	,	kIx,
po	po	k7c6
posledním	poslední	k2eAgNnSc6d1
pomazání	pomazání	k1gNnSc6
<g/>
,	,	kIx,
složil	složit	k5eAaPmAgMnS
Lully	Lulla	k1gFnSc2
své	svůj	k3xOyFgNnSc4
poslední	poslední	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
<g/>
,	,	kIx,
pětihlasý	pětihlasý	k2eAgInSc4d1
kánon	kánon	k1gInSc4
Il	Il	k1gMnSc1
faut	faut	k1gMnSc1
mourir	mourir	k1gMnSc1
<g/>
,	,	kIx,
pécheur	pécheur	k1gMnSc1
(	(	kIx(
<g/>
Umíráš	umírat	k5eAaImIp2nS
<g/>
,	,	kIx,
hříšníku	hříšník	k1gMnSc5
<g/>
,	,	kIx,
umíráš	umírat	k5eAaImIp2nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
sepsal	sepsat	k5eAaPmAgMnS
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnPc1
svou	svůj	k3xOyFgFnSc4
poslední	poslední	k2eAgFnSc4d1
vůli	vůle	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
mimo	mimo	k6eAd1
jiné	jiný	k2eAgFnSc6d1
odkázal	odkázat	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
operní	operní	k2eAgNnSc4d1
privilegium	privilegium	k1gNnSc4
své	svůj	k3xOyFgFnSc3
ženě	žena	k1gFnSc3
a	a	k8xC
potomkům	potomek	k1gMnPc3
a	a	k8xC
svěřil	svěřit	k5eAaPmAgMnS
řízení	řízení	k1gNnSc4
Královské	královský	k2eAgFnSc2d1
hudební	hudební	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
své	svůj	k3xOyFgFnSc3
ženě	žena	k1gFnSc3
Madelaine	Madelaine	k1gFnSc2
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
měli	mít	k5eAaImAgMnP
být	být	k5eAaImF
po	po	k7c6
ruce	ruka	k1gFnSc6
François	François	k1gFnSc4
Frichet	Frichet	k1gMnSc1
a	a	k8xC
dirigent	dirigent	k1gMnSc1
Pascal	Pascal	k1gMnSc1
Collasse	Collasse	k1gFnSc1
<g/>
;	;	kIx,
nástupnictví	nástupnictví	k1gNnSc1
ve	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
funkcích	funkce	k1gFnPc6
u	u	k7c2
dvora	dvůr	k1gInSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
měl	mít	k5eAaImAgInS
již	již	k6eAd1
zajištěno	zajištěn	k2eAgNnSc4d1
jeho	jeho	k3xOp3gNnSc4
syn	syn	k1gMnSc1
Jean-Louis	Jean-Louis	k1gFnSc2
Lully	Lulla	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Nedlouho	dlouho	k6eNd1
poté	poté	k6eAd1
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1687	#num#	k4
<g/>
,	,	kIx,
zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
následky	následek	k1gInPc4
gangrény	gangréna	k1gFnSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
hlavní	hlavní	k2eAgFnSc6d1
rezidenci	rezidence	k1gFnSc6
<g/>
,	,	kIx,
domě	dům	k1gInSc6
na	na	k7c6
pařížském	pařížský	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Ville	Ville	k1gNnPc2
l	l	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
'	'	kIx"
<g/>
Évê	Évê	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
pohřbu	pohřeb	k1gInSc6
v	v	k7c6
místně	místně	k6eAd1
příslušném	příslušný	k2eAgInSc6d1
farním	farní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
kostele	kostel	k1gInSc6
svaté	svatá	k1gFnSc2
Máří	Máří	k?
Magdalény	Magdaléna	k1gFnSc2
(	(	kIx(
<g/>
zbořen	zbořen	k2eAgInSc1d1
roku	rok	k1gInSc2
1801	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
nahrazen	nahradit	k5eAaPmNgInS
kostelem	kostel	k1gInSc7
novým	nový	k2eAgInSc7d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgNnSc6,k3yIgNnSc6,k3yRgNnSc6
zaznělo	zaznět	k5eAaImAgNnS,k5eAaPmAgNnS
jeho	jeho	k3xOp3gNnSc1
Dies	Dies	k1gInSc4
irae	ira	k1gMnSc2
(	(	kIx(
<g/>
Den	den	k1gInSc1
hněvu	hněv	k1gInSc2
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
bylo	být	k5eAaImAgNnS
jeho	jeho	k3xOp3gFnSc4
tělo	tělo	k1gNnSc1
–	–	k?
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
přáním	přání	k1gNnSc7
vyjádřeným	vyjádřený	k2eAgNnSc7d1
v	v	k7c6
závěti	závěť	k1gFnSc6
–	–	k?
uloženo	uložen	k2eAgNnSc4d1
do	do	k7c2
tomby	tomba	k1gFnSc2
v	v	k7c6
augustiniánském	augustiniánský	k2eAgInSc6d1
kostele	kostel	k1gInSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Vítězné	vítězný	k2eAgFnSc2d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
až	až	k6eAd1
na	na	k7c6
vnitřnosti	vnitřnost	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
zůstaly	zůstat	k5eAaPmAgInP
uloženy	uložit	k5eAaPmNgInP
u	u	k7c2
Máří	Máří	k?
Magdalény	Magdaléna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Lullyho	Lully	k1gMnSc2
vdova	vdova	k1gFnSc1
dala	dát	k5eAaPmAgFnS
v	v	k7c6
kostele	kostel	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
pohřben	pohřben	k2eAgInSc1d1
<g/>
,	,	kIx,
zřídit	zřídit	k5eAaPmF
výstavný	výstavný	k2eAgInSc4d1
kenotaf	kenotaf	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
dochoval	dochovat	k5eAaPmAgInS
<g/>
,	,	kIx,
avšak	avšak	k8xC
na	na	k7c6
jiném	jiný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
a	a	k8xC
bez	bez	k7c2
kovových	kovový	k2eAgFnPc2d1
a	a	k8xC
štukových	štukový	k2eAgFnPc2d1
součástí	součást	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
zničeny	zničit	k5eAaPmNgFnP
za	za	k7c2
velké	velký	k2eAgFnSc2d1
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc1
po	po	k7c6
sobě	se	k3xPyFc3
zanechal	zanechat	k5eAaPmAgMnS
čtyři	čtyři	k4xCgInPc4
domy	dům	k1gInPc4
–	–	k?
totiž	totiž	k9
svou	svůj	k3xOyFgFnSc4
rezidenci	rezidence	k1gFnSc4
ve	v	k7c6
Ville	Villo	k1gNnSc6
l	l	kA
<g/>
'	'	kIx"
<g/>
Évê	Évê	k1gInSc1
<g/>
,	,	kIx,
původní	původní	k2eAgInSc1d1
pařížský	pařížský	k2eAgInSc1d1
dům	dům	k1gInSc1
v	v	k7c6
ulici	ulice	k1gFnSc6
Sainte-Anne	Sainte-Ann	k1gInSc5
<g/>
,	,	kIx,
nájemní	nájemní	k2eAgInSc4d1
dům	dům	k1gInSc4
v	v	k7c6
ulici	ulice	k1gFnSc6
Royale	Royala	k1gFnSc6
a	a	k8xC
venkovské	venkovský	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
v	v	k7c4
Puteaux	Puteaux	k1gInSc4
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
nemalý	malý	k2eNgInSc4d1
movitý	movitý	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
,	,	kIx,
zejména	zejména	k9
překvapivé	překvapivý	k2eAgNnSc4d1
množství	množství	k1gNnSc4
hotovosti	hotovost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinové	novinový	k2eAgFnPc1d1
zprávy	zpráva	k1gFnPc1
uváděly	uvádět	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
jeho	jeho	k3xOp3gFnPc6
truhlách	truhla	k1gFnPc6
našlo	najít	k5eAaPmAgNnS
až	až	k9
760	#num#	k4
000	#num#	k4
livrů	livr	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představovalo	představovat	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
pětisetnásobek	pětisetnásobek	k1gInSc4
ročního	roční	k2eAgInSc2d1
platu	plat	k1gInSc2
průměrného	průměrný	k2eAgMnSc2d1
dvorního	dvorní	k2eAgMnSc2d1
hudebníka	hudebník	k1gMnSc2
<g/>
;	;	kIx,
pozůstalostní	pozůstalostní	k2eAgInSc1d1
soupis	soupis	k1gInSc1
však	však	k9
uvádí	uvádět	k5eAaImIp3nS
266	#num#	k4
562	#num#	k4
livrů	livr	k1gInPc2
v	v	k7c6
hotovosti	hotovost	k1gFnSc6
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
s	s	k7c7
nábytkem	nábytek	k1gInSc7
a	a	k8xC
cennostmi	cennost	k1gFnPc7
387	#num#	k4
000	#num#	k4
livrů	livr	k1gInPc2
a	a	k8xC
po	po	k7c6
připočtení	připočtení	k1gNnSc6
ceny	cena	k1gFnSc2
nemovitostí	nemovitost	k1gFnPc2
zhruba	zhruba	k6eAd1
650	#num#	k4
000	#num#	k4
livrů	livr	k1gInPc2
celkové	celkový	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
pozůstalosti	pozůstalost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
umělecké	umělecký	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
</s>
<s>
Lullyho	Lullyze	k6eAd1
pověst	pověst	k1gFnSc1
dvorního	dvorní	k2eAgMnSc2d1
intrikána	intrikán	k1gMnSc2
neblaze	blaze	k6eNd1
poškodila	poškodit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnSc4
hudební	hudební	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
čtvrtině	čtvrtina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
snaha	snaha	k1gFnSc1
o	o	k7c6
autentické	autentický	k2eAgNnSc4d1
uvádění	uvádění	k1gNnSc4
staré	starý	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
způsobila	způsobit	k5eAaPmAgFnS
znovuzrození	znovuzrození	k1gNnSc2
několika	několik	k4yIc2
polozapomenutých	polozapomenutý	k2eAgFnPc2d1
jevištních	jevištní	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Uvedení	uvedení	k1gNnSc4
opery	opera	k1gFnSc2
Atys	Atysa	k1gFnPc2
zahájilo	zahájit	k5eAaPmAgNnS
novodobý	novodobý	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
francouzskou	francouzský	k2eAgFnSc4d1
barokní	barokní	k2eAgFnSc4d1
operu	opera	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Atyse	Atyse	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
dočkal	dočkat	k5eAaPmAgInS
i	i	k9
scénického	scénický	k2eAgNnSc2d1
provedení	provedení	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
Lullyho	Lully	k1gMnSc2
oper	opera	k1gFnPc2
prováděna	provádět	k5eAaImNgFnS
pouze	pouze	k6eAd1
v	v	k7c6
koncertním	koncertní	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
<g/>
;	;	kIx,
například	například	k6eAd1
Isis	Isis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
oper	opera	k1gFnPc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
Lullyho	Lully	k1gMnSc4
významná	významný	k2eAgNnPc4d1
duchovní	duchovní	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
11	#num#	k4
malých	malý	k2eAgMnPc2d1
a	a	k8xC
11	#num#	k4
velkých	velký	k2eAgNnPc2d1
motet	moteto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
text	text	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
žalmů	žalm	k1gInPc2
(	(	kIx(
<g/>
například	například	k6eAd1
Quare	Quar	k1gInSc5
fremuerunt	fremuerunta	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
žalmu	žalm	k1gInSc2
č.	č.	k?
2	#num#	k4
<g/>
)	)	kIx)
či	či	k8xC
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
o	o	k7c4
křesťanské	křesťanský	k2eAgInPc4d1
motivy	motiv	k1gInPc4
(	(	kIx(
<g/>
Plaude	Plaud	k1gMnSc5
Laetare	Laetar	k1gMnSc5
Gallia	Gallium	k1gNnPc1
<g/>
,	,	kIx,
Raduj	radovat	k5eAaImRp2nS
se	se	k3xPyFc4
a	a	k8xC
zpívej	zpívat	k5eAaImRp2nS
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
Lullyho	Lully	k1gMnSc4
hudební	hudební	k2eAgMnSc1d1
díla	dílo	k1gNnSc2
je	být	k5eAaImIp3nS
typická	typický	k2eAgFnSc1d1
rytmická	rytmický	k2eAgFnSc1d1
preciznost	preciznost	k1gFnSc1
<g/>
,	,	kIx,
dramatický	dramatický	k2eAgInSc4d1
patos	patos	k1gInSc4
<g/>
;	;	kIx,
velké	velký	k2eAgNnSc4d1
<g/>
,	,	kIx,
jednoduché	jednoduchý	k2eAgNnSc4d1
<g/>
,	,	kIx,
výrazně	výrazně	k6eAd1
melodické	melodický	k2eAgFnSc2d1
linky	linka	k1gFnSc2
árií	árie	k1gFnPc2
<g/>
,	,	kIx,
velké	velký	k2eAgNnSc4d1
zastoupení	zastoupení	k1gNnSc4
sboru	sbor	k1gInSc2
a	a	k8xC
orchestru	orchestr	k1gInSc2
a	a	k8xC
bohatá	bohatý	k2eAgFnSc1d1
choreografie	choreografie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dochování	dochování	k1gNnSc1
<g/>
,	,	kIx,
seznam	seznam	k1gInSc1
a	a	k8xC
edice	edice	k1gFnSc1
děl	dělo	k1gNnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
děl	dít	k5eAaImAgMnS,k5eAaBmAgMnS
J.	J.	kA
B.	B.	kA
Lullyho	Lully	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
André	André	k1gMnSc1
Danican	Danican	k1gMnSc1
Philidor	Philidor	k1gMnSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
Philidor	Philidor	k1gInSc1
l	l	kA
<g/>
'	'	kIx"
<g/>
Aîné	Aîný	k2eAgFnSc2d1
</s>
<s>
Lullyho	Lullyze	k6eAd1
dílo	dílo	k1gNnSc1
se	se	k3xPyFc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jeho	jeho	k3xOp3gMnPc7
současníky	současník	k1gMnPc7
zachovalo	zachovat	k5eAaPmAgNnS
v	v	k7c6
mimořádném	mimořádný	k2eAgInSc6d1
rozsahu	rozsah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
schopný	schopný	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
si	se	k3xPyFc3
byl	být	k5eAaImAgMnS
vědom	vědom	k2eAgMnSc1d1
propagačního	propagační	k2eAgInSc2d1
významu	význam	k1gInSc2
publikace	publikace	k1gFnSc2
svých	svůj	k3xOyFgNnPc2
děl	dělo	k1gNnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
schopen	schopen	k2eAgInSc1d1
ji	on	k3xPp3gFnSc4
financovat	financovat	k5eAaBmF
<g/>
,	,	kIx,
všechna	všechen	k3xTgNnPc1
rozměrná	rozměrný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zralého	zralý	k2eAgNnSc2d1
období	období	k1gNnSc2
tak	tak	k6eAd1
jsou	být	k5eAaImIp3nP
dostupná	dostupný	k2eAgFnSc1d1
ve	v	k7c6
formě	forma	k1gFnSc6
autorských	autorský	k2eAgFnPc2d1
tištěných	tištěný	k2eAgFnPc2d1
partitur	partitura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Lullyho	Lully	k1gMnSc2
smrti	smrt	k1gFnSc6
navíc	navíc	k6eAd1
král	král	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
nařídil	nařídit	k5eAaPmAgMnS
shromáždit	shromáždit	k5eAaPmF
všechna	všechen	k3xTgNnPc1
zachovaná	zachovaný	k2eAgNnPc1d1
Lullyho	Lully	k1gMnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
úlohy	úloha	k1gFnSc2
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
skladatel	skladatel	k1gMnSc1
André	André	k1gMnSc1
Danican	Danican	k1gMnSc1
Philidor	Philidor	k1gMnSc1
(	(	kIx(
<g/>
1652	#num#	k4
<g/>
–	–	k?
<g/>
1730	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
králův	králův	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
knihovník	knihovník	k1gMnSc1
a	a	k8xC
kopista	kopista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
němu	on	k3xPp3gInSc3
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
řada	řada	k1gFnSc1
skladeb	skladba	k1gFnPc2
ze	z	k7c2
začátků	začátek	k1gInPc2
Lullyho	Lully	k1gMnSc2
působení	působení	k1gNnSc1
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
z	z	k7c2
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ojediněle	ojediněle	k6eAd1
se	se	k3xPyFc4
opisy	opis	k1gInPc7
Lullyho	Lullyha	k1gFnSc5
děl	dělo	k1gNnPc2
zachovaly	zachovat	k5eAaPmAgFnP
i	i	k8xC
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
rukopisných	rukopisný	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philidorovy	Philidorův	k2eAgInPc1d1
opisy	opis	k1gInPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
spolehlivé	spolehlivý	k2eAgInPc1d1
<g/>
:	:	kIx,
neváhal	váhat	k5eNaImAgMnS
například	například	k6eAd1
nechat	nechat	k5eAaPmF
skladby	skladba	k1gFnPc4
jen	jen	k9
torzovité	torzovitý	k2eAgNnSc1d1
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	k9
chybějící	chybějící	k2eAgInPc4d1
úseky	úsek	k1gInPc4
nebo	nebo	k8xC
hlasy	hlas	k1gInPc4
doplňoval	doplňovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
vždy	vždy	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
spolehlivé	spolehlivý	k2eAgNnSc1d1
přiřčení	přiřčení	k1gNnSc1
Lullymu	Lullym	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
některých	některý	k3yIgFnPc2
skladeb	skladba	k1gFnPc2
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
prokázáno	prokázat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
je	on	k3xPp3gMnPc4
napsali	napsat	k5eAaPmAgMnP,k5eAaBmAgMnP
jiní	jiný	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Philidor	Philidor	k1gMnSc1
i	i	k9
pozdější	pozdní	k2eAgMnPc1d2
badatelé	badatel	k1gMnPc1
občas	občas	k6eAd1
podléhali	podléhat	k5eAaImAgMnP
jednak	jednak	k8xC
tendenci	tendence	k1gFnSc4
připisovat	připisovat	k5eAaImF
slavnému	slavný	k2eAgNnSc3d1
Lullymu	Lullymum	k1gNnSc3
i	i	k8xC
skladby	skladba	k1gFnPc1
jeho	jeho	k3xOp3gMnPc2
méně	málo	k6eAd2
známých	známý	k2eAgMnPc2d1
současníků	současník	k1gMnPc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
tendenci	tendence	k1gFnSc4
spoléhat	spoléhat	k5eAaImF
na	na	k7c4
Lullyho	Lully	k1gMnSc4
pověst	pověst	k1gFnSc4
a	a	k8xC
připisovat	připisovat	k5eAaImF
mu	on	k3xPp3gMnSc3
skladby	skladba	k1gFnPc4
vyšší	vysoký	k2eAgFnSc2d2
kvality	kvalita	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
skladeb	skladba	k1gFnPc2
méně	málo	k6eAd2
zdařilých	zdařilý	k2eAgFnPc2d1
jeho	jeho	k3xOp3gInSc6
autorství	autorství	k1gNnSc6
popírat	popírat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přesto	přesto	k8xC
jsou	být	k5eAaImIp3nP
z	z	k7c2
dobových	dobový	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
známa	známo	k1gNnSc2
Lullyho	Lully	k1gMnSc2
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
nezachovala	zachovat	k5eNaPmAgNnP
<g/>
,	,	kIx,
zejména	zejména	k9
baletní	baletní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnPc4
novodobé	novodobý	k2eAgFnPc4d1
edice	edice	k1gFnPc4
Lullyho	Lully	k1gMnSc2
děl	dělo	k1gNnPc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
poslední	poslední	k2eAgFnSc2d1
čtvrtiny	čtvrtina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
se	se	k3xPyFc4
jim	on	k3xPp3gFnPc3
například	například	k6eAd1
francouzský	francouzský	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Théodor	Théodor	k1gMnSc1
de	de	k?
Lajarte	Lajart	k1gInSc5
(	(	kIx(
<g/>
edice	edice	k1gFnSc1
sedmi	sedm	k4xCc2
oper	opera	k1gFnPc2
v	v	k7c6
letech	let	k1gInPc6
1877	#num#	k4
<g/>
–	–	k?
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
německý	německý	k2eAgMnSc1d1
muzikolog	muzikolog	k1gMnSc1
Robert	Robert	k1gMnSc1
Eitner	Eitner	k1gMnSc1
(	(	kIx(
<g/>
edice	edice	k1gFnSc1
Armidy	Armida	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1885	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edice	edice	k1gFnSc1
Lullyho	Lully	k1gMnSc2
souborného	souborný	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zahájil	zahájit	k5eAaPmAgMnS
Henry	henry	k1gInSc7
Pruniè	Pruniè	k1gInSc4
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
zastavila	zastavit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc7
smrtí	smrt	k1gFnSc7
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Německý	německý	k2eAgMnSc1d1
muzikolog	muzikolog	k1gMnSc1
Herbert	Herbert	k1gMnSc1
Schneider	Schneider	k1gMnSc1
sestavil	sestavit	k5eAaPmAgMnS
seznam	seznam	k1gInSc4
Lullyho	Lully	k1gMnSc2
děl	dělo	k1gNnPc2
Lully	Lulla	k1gFnSc2
Werke	Werke	k1gFnSc1
Verzeichnis	Verzeichnis	k1gFnSc1
(	(	kIx(
<g/>
LWV	LWV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
utvořen	utvořit	k5eAaPmNgInS
chronologicky	chronologicky	k6eAd1
(	(	kIx(
<g/>
nikoli	nikoli	k9
tematicky	tematicky	k6eAd1
=	=	kIx~
podle	podle	k7c2
druhu	druh	k1gInSc2
skladby	skladba	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
BWV	BWV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
data	datum	k1gNnSc2
prvního	první	k4xOgNnSc2
uvedení	uvedení	k1gNnSc2
skladby	skladba	k1gFnSc2
(	(	kIx(
<g/>
premiéry	premiéra	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schneider	Schneider	k1gMnSc1
stojí	stát	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
Jerômem	Jerôm	k1gInSc7
de	de	k?
La	la	k1gNnSc3
Gorce	Gorce	k1gFnSc2
v	v	k7c6
čele	čelo	k1gNnSc6
nové	nový	k2eAgFnSc2d1
kritické	kritický	k2eAgFnSc2d1
edice	edice	k1gFnSc2
souborného	souborný	k2eAgMnSc2d1
Lullyho	Lully	k1gMnSc2
díla	dílo	k1gNnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
nakladatelství	nakladatelství	k1gNnSc1
Georg	Georg	k1gInSc1
Olms	Olms	k1gInSc1
Verlag	Verlag	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Objemově	objemově	k6eAd1
největší	veliký	k2eAgFnSc4d3
část	část	k1gFnSc4
Lullyho	Lully	k1gMnSc2
díla	dílo	k1gNnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnPc4
opery	opera	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nim	on	k3xPp3gInPc3
přistupují	přistupovat	k5eAaImIp3nP
balety	balet	k1gInPc1
a	a	k8xC
příbuzná	příbuzný	k2eAgNnPc1d1
jevištní	jevištní	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
(	(	kIx(
<g/>
comédie-ballet	comédie-ballet	k1gInSc1
<g/>
,	,	kIx,
tragédie-ballet	tragédie-ballet	k1gInSc1
<g/>
,	,	kIx,
divertissement	divertissement	k1gInSc1
<g/>
)	)	kIx)
vzniklá	vzniklý	k2eAgFnSc1d1
pro	pro	k7c4
dvůr	dvůr	k1gInSc4
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
zaujímá	zaujímat	k5eAaImIp3nS
duchovní	duchovní	k2eAgFnSc1d1
tvorba	tvorba	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
výhradně	výhradně	k6eAd1
ve	v	k7c6
formě	forma	k1gFnSc6
moteta	moteto	k1gNnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
velkého	velký	k2eAgInSc2d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
malého	malý	k2eAgMnSc4d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lullyho	Lully	k1gMnSc2
dílo	dílo	k1gNnSc1
dále	daleko	k6eAd2
obsahuje	obsahovat	k5eAaImIp3nS
řadu	řada	k1gFnSc4
drobných	drobný	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
vokálních	vokální	k2eAgFnPc2d1
a	a	k8xC
instrumentálních	instrumentální	k2eAgFnPc2d1
<g/>
,	,	kIx,
zejména	zejména	k9
tanečních	taneční	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachovaly	zachovat	k5eAaPmAgInP
se	se	k3xPyFc4
převážně	převážně	k6eAd1
v	v	k7c6
kopiích	kopie	k1gFnPc6
a	a	k8xC
většinu	většina	k1gFnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
nelze	lze	k6eNd1
přesně	přesně	k6eAd1
datovat	datovat	k5eAaImF
ani	ani	k8xC
určit	určit	k5eAaPmF
jejich	jejich	k3xOp3gInSc4
konkrétní	konkrétní	k2eAgInSc4d1
účel	účel	k1gInSc4
či	či	k8xC
adresáta	adresát	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Moteta	moteto	k1gNnPc1
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
duchovní	duchovní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nP
Lullyho	Lully	k1gMnSc4
příspěvek	příspěvek	k1gInSc4
soubor	soubor	k1gInSc4
motet	moteto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasické	klasický	k2eAgNnSc4d1
francouzské	francouzský	k2eAgNnSc4d1
moteto	moteto	k1gNnSc4
se	se	k3xPyFc4
konstituovalo	konstituovat	k5eAaBmAgNnS
krátce	krátce	k6eAd1
před	před	k7c7
skladatelovým	skladatelův	k2eAgInSc7d1
příchodem	příchod	k1gInSc7
do	do	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c2
vlády	vláda	k1gFnSc2
Ludvíka	Ludvík	k1gMnSc2
XIII	XIII	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
základní	základní	k2eAgInPc1d1
stylové	stylový	k2eAgInPc1d1
rysy	rys	k1gInPc1
se	se	k3xPyFc4
rychle	rychle	k6eAd1
ustálily	ustálit	k5eAaPmAgFnP
v	v	k7c6
tvorbě	tvorba	k1gFnSc6
Nicolase	Nicolasa	k1gFnSc3
Formého	Formý	k2eAgMnSc2d1
<g/>
,	,	kIx,
Guillauma	Guillaum	k1gMnSc2
Bouzignaca	Bouzignacus	k1gMnSc2
<g/>
,	,	kIx,
Thomase	Thomas	k1gMnSc2
Goberta	Gobert	k1gMnSc2
a	a	k8xC
Jeana	Jean	k1gMnSc2
Veillota	Veillot	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
definoval	definovat	k5eAaBmAgMnS
Henri	Henr	k1gMnSc3
du	du	k?
Mont	Mont	k1gMnSc1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
tvorbě	tvorba	k1gFnSc6
žánry	žánr	k1gInPc1
„	„	k?
<g/>
velkého	velký	k2eAgNnSc2d1
moteta	moteto	k1gNnSc2
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
malého	malý	k2eAgNnSc2d1
moteta	moteto	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
Lully	Lulla	k1gFnPc1
v	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
přinesl	přinést	k5eAaPmAgInS
mnohem	mnohem	k6eAd1
méně	málo	k6eAd2
inovací	inovace	k1gFnPc2
než	než	k8xS
v	v	k7c6
jevištních	jevištní	k2eAgInPc6d1
žánrech	žánr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
baletu	balet	k1gInSc2
a	a	k8xC
opery	opera	k1gFnSc2
zde	zde	k6eAd1
měl	mít	k5eAaImAgInS
také	také	k9
po	po	k7c4
celou	celá	k1gFnSc4
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
zdatné	zdatný	k2eAgMnPc4d1
konkurenty	konkurent	k1gMnPc4
<g/>
,	,	kIx,
vedle	vedle	k7c2
du	du	k?
Monta	Mont	k1gInSc2
zejména	zejména	k9
Marka-Antoina	Marka-Antoin	k2eAgFnSc1d1
Charpentiera	Charpentiera	k1gFnSc1
a	a	k8xC
Michela-Richarda	Michela-Richarda	k1gFnSc1
Delalanda	Delalanda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brána	brána	k1gFnSc1
Saint-Honoré	Saint-Honorý	k2eAgFnSc2d1
a	a	k8xC
kostel	kostel	k1gInSc1
s	s	k7c7
klášterem	klášter	k1gInSc7
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
Lully	Lulla	k1gFnSc2
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
sérií	série	k1gFnSc7
svých	svůj	k3xOyFgMnPc2
„	„	k?
<g/>
malých	malý	k2eAgNnPc2d1
motet	moteto	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rytina	rytina	k1gFnSc1
N.	N.	kA
de	de	k?
Poillyho	Poilly	k1gMnSc2
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgInPc4d1
církevní	církevní	k2eAgInPc4d1
obřady	obřad	k1gInPc4
u	u	k7c2
dvora	dvůr	k1gInSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
byly	být	k5eAaImAgFnP
pro	pro	k7c4
Lullyho	Lully	k1gMnSc4
významnou	významný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
prosadit	prosadit	k5eAaPmF
a	a	k8xC
udržet	udržet	k5eAaPmF
si	se	k3xPyFc3
svou	svůj	k3xOyFgFnSc4
pověst	pověst	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
mohlo	moct	k5eAaImAgNnS
účastnit	účastnit	k5eAaImF
i	i	k9
více	hodně	k6eAd2
posluchačů	posluchač	k1gMnPc2
než	než	k8xS
v	v	k7c6
případě	případ	k1gInSc6
dvorských	dvorský	k2eAgFnPc2d1
zábav	zábava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
se	se	k3xPyFc4
nabízelo	nabízet	k5eAaImAgNnS
využití	využití	k1gNnSc1
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
hudebníků	hudebník	k1gMnPc2
než	než	k8xS
při	při	k7c6
jakékoli	jakýkoli	k3yIgFnSc6
jiné	jiný	k2eAgFnSc6d1
příležitosti	příležitost	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
při	při	k7c6
těchto	tento	k3xDgInPc6
obřadech	obřad	k1gInPc6
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
spojit	spojit	k5eAaPmF
hudební	hudební	k2eAgInSc4d1
personál	personál	k1gInSc4
„	„	k?
<g/>
královské	královský	k2eAgFnSc2d1
kaple	kaple	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
musique	musique	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Chapelle	Chapelle	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vyhrazený	vyhrazený	k2eAgInSc1d1
pro	pro	k7c4
duchovní	duchovní	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
se	s	k7c7
světskými	světský	k2eAgNnPc7d1
tělesy	těleso	k1gNnPc7
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
musique	musique	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Chambre	Chambr	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
již	již	k9
roku	rok	k1gInSc2
1663	#num#	k4
se	s	k7c7
premiéry	premiéra	k1gFnSc2
Lullyho	Lully	k1gMnSc2
Miserere	Miserere	k1gNnSc2
účastnilo	účastnit	k5eAaImAgNnS
přes	přes	k7c4
140	#num#	k4
pěvců	pěvec	k1gMnPc2
a	a	k8xC
instrumentalistů	instrumentalista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Prvním	první	k4xOgMnSc7
Lullyho	Lullyha	k1gFnSc5
motetem	moteto	k1gNnSc7
bylo	být	k5eAaImAgNnS
zřejmě	zřejmě	k6eAd1
–	–	k?
autorství	autorství	k1gNnSc1
není	být	k5eNaImIp3nS
prokázáno	prokázat	k5eAaPmNgNnS
s	s	k7c7
naprostou	naprostý	k2eAgFnSc7d1
jistotou	jistota	k1gFnSc7
–	–	k?
Jubilate	Jubilat	k1gMnSc5
Deo	Deo	k1gMnSc5
<g/>
,	,	kIx,
zvané	zvaný	k2eAgFnPc1d1
též	též	k6eAd1
Motet	moteto	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
de	de	k?
la	la	k1gNnSc1
Paix	Paix	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
mělo	mít	k5eAaImAgNnS
premiéru	premiéra	k1gFnSc4
29	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1660	#num#	k4
na	na	k7c4
oslavu	oslava	k1gFnSc4
dosažení	dosažení	k1gNnSc2
Pyrenejského	pyrenejský	k2eAgInSc2d1
míru	mír	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
Bylo	být	k5eAaImAgNnS
brzy	brzy	k6eAd1
zapomenuto	zapomenout	k5eAaPmNgNnS,k5eAaImNgNnS
<g/>
,	,	kIx,
zato	zato	k6eAd1
následující	následující	k2eAgNnSc4d1
moteto	moteto	k1gNnSc4
Miserere	Miserere	k1gNnSc2
<g/>
,	,	kIx,
napsané	napsaný	k2eAgInPc1d1
pravděpodobně	pravděpodobně	k6eAd1
pro	pro	k7c4
Velikonoce	Velikonoce	k1gFnPc4
roku	rok	k1gInSc2
1663	#num#	k4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
opakováno	opakovat	k5eAaImNgNnS
při	při	k7c6
řadě	řada	k1gFnSc6
příležitostí	příležitost	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
i	i	k9
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
Menšího	malý	k2eAgInSc2d2
ohlasu	ohlas	k1gInSc2
dosáhly	dosáhnout	k5eAaPmAgInP
O	o	k7c4
lachrymae	lachrymae	k1gFnSc4
a	a	k8xC
Benedictus	Benedictus	k1gInSc4
<g/>
,	,	kIx,
napsané	napsaný	k2eAgInPc4d1
patrně	patrně	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1663	#num#	k4
<g/>
–	–	k?
<g/>
1664	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
Moteto	moteto	k1gNnSc1
Plaude	Plaud	k1gInSc5
laetare	laetar	k1gMnSc5
Gallia	Gallius	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1668	#num#	k4
<g/>
,	,	kIx,
napsané	napsaný	k2eAgNnSc4d1
ke	k	k7c3
<g />
.	.	kIx.
</s>
<s hack="1">
slavnostnímu	slavnostní	k2eAgInSc3d1
křtu	křest	k1gInSc3
Velkého	velký	k2eAgMnSc2d1
dauphina	dauphin	k1gMnSc2
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
nadlouho	nadlouho	k6eAd1
poslední	poslední	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
Lully	Lull	k1gMnPc7
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
jediné	jediný	k2eAgNnSc4d1
moteto	moteto	k1gNnSc4
<g/>
,	,	kIx,
zato	zato	k6eAd1
jedno	jeden	k4xCgNnSc4
ze	z	k7c2
svých	svůj	k3xOyFgNnPc2
nejslavnějších	slavný	k2eAgNnPc2d3
<g/>
:	:	kIx,
Te	Te	k1gFnSc1
Deum	Deum	k1gInSc1
ke	k	k7c3
křtu	křest	k1gInSc3
vlastního	vlastní	k2eAgMnSc2d1
syna	syn	k1gMnSc2
Louise	Louis	k1gMnSc2
na	na	k7c6
zámku	zámek	k1gInSc6
ve	v	k7c6
Fontainebleau	Fontainebleaus	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
kmotrem	kmotr	k1gMnSc7
sám	sám	k3xTgMnSc1
král	král	k1gMnSc1
(	(	kIx(
<g/>
1677	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lully	Lull	k1gMnPc4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
k	k	k7c3
motetům	moteto	k1gNnPc3
až	až	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1683	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začaly	začít	k5eAaPmAgFnP
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
pod	pod	k7c7
vlivem	vliv	k1gInSc7
Madame	madame	k1gFnSc1
de	de	k?
Maintenon	Maintenon	k1gInSc1
nad	nad	k7c7
zábavami	zábava	k1gFnPc7
převládat	převládat	k5eAaImF
projevy	projev	k1gInPc4
zbožnosti	zbožnost	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
po	po	k7c6
smrti	smrt	k1gFnSc6
královny	královna	k1gFnSc2
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Lully	Lulla	k1gFnPc4
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
udržet	udržet	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1683	#num#	k4
a	a	k8xC
1687	#num#	k4
napsal	napsat	k5eAaBmAgInS,k5eAaPmAgInS
druhou	druhý	k4xOgFnSc4
šestici	šestice	k1gFnSc4
velkých	velký	k2eAgNnPc2d1
motet	moteto	k1gNnPc2
<g/>
:	:	kIx,
De	De	k?
profundis	profundis	k1gInSc1
<g/>
,	,	kIx,
Dies	Dies	k1gInSc1
irae	irae	k1gFnSc1
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc1
se	se	k3xPyFc4
hrála	hrát	k5eAaImAgFnS
na	na	k7c6
pohřbu	pohřeb	k1gInSc6
královny	královna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Quare	Quar	k1gInSc5
fremerunt	fremerunta	k1gFnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
žalmu	žalm	k1gInSc2
2	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Domine	Domin	k1gInSc5
salvum	salvum	k1gNnSc4
fac	fac	k?
regem	regem	k1gInSc1
<g/>
,	,	kIx,
Notus	Notus	k1gInSc1
in	in	k?
Judaea	Judae	k1gInSc2
Deus	Deusa	k1gFnPc2
(	(	kIx(
<g/>
podle	podle	k7c2
žalmu	žalm	k1gInSc2
76	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Exaudiat	Exaudiat	k2eAgInSc1d1
te	te	k?
Dominus	Dominus	k1gInSc1
(	(	kIx(
<g/>
podle	podle	k7c2
žalmu	žalm	k1gInSc2
20	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Texty	text	k1gInPc4
těchto	tento	k3xDgNnPc2
velkých	velký	k2eAgNnPc2d1
motet	moteto	k1gNnPc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
latině	latina	k1gFnSc6
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
různých	různý	k2eAgInPc2d1
liturgických	liturgický	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
(	(	kIx(
<g/>
zejména	zejména	k9
z	z	k7c2
Knihy	kniha	k1gFnSc2
žalmů	žalm	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc4
však	však	k9
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
zvlášť	zvlášť	k6eAd1
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
příležitost	příležitost	k1gFnSc4
Pierre	Pierr	k1gInSc5
Perin	Perin	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
Zpívaný	zpívaný	k2eAgInSc1d1
text	text	k1gInSc1
je	být	k5eAaImIp3nS
rozložen	rozložit	k5eAaPmNgInS
mezi	mezi	k7c4
velký	velký	k2eAgInSc4d1
a	a	k8xC
malý	malý	k2eAgInSc4d1
sbor	sbor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
je	být	k5eAaImIp3nS
pětihlasý	pětihlasý	k2eAgInSc1d1
a	a	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
hlasy	hlas	k1gInPc4
„	„	k?
<g/>
dessus	dessus	k1gInSc4
<g/>
,	,	kIx,
haute-contre	haute-contr	k1gMnSc5
<g/>
,	,	kIx,
taille	taille	k1gFnSc2
<g/>
,	,	kIx,
basse-taille	basse-taille	k1gFnSc2
a	a	k8xC
basse	basse	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
soprán	soprán	k1gInSc1
<g/>
,	,	kIx,
kontratenor	kontratenor	k1gInSc1
<g/>
,	,	kIx,
tenor	tenor	k1gInSc1
<g/>
,	,	kIx,
baryton	baryton	k1gInSc1
a	a	k8xC
bas	bas	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
výhradně	výhradně	k6eAd1
mužský	mužský	k2eAgMnSc1d1
–	–	k?
soprány	soprán	k1gInPc4
zpívali	zpívat	k5eAaImAgMnP
chlapci	chlapec	k1gMnPc1
–	–	k?
a	a	k8xC
s	s	k7c7
dominancí	dominance	k1gFnSc7
nižších	nízký	k2eAgInPc2d2
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malý	malý	k2eAgInSc1d1
sbor	sbor	k1gInSc1
u	u	k7c2
Lullyho	Lully	k1gMnSc2
tvoří	tvořit	k5eAaImIp3nP
čtyři	čtyři	k4xCgFnPc4
nebo	nebo	k8xC
pět	pět	k4xCc4
sólistů	sólista	k1gMnPc2
<g/>
:	:	kIx,
tři	tři	k4xCgInPc4
mužské	mužský	k2eAgInPc4d1
hlasy	hlas	k1gInPc4
(	(	kIx(
<g/>
haute-contre	haute-contr	k1gMnSc5
<g/>
,	,	kIx,
taille	taille	k1gFnSc1
<g/>
,	,	kIx,
basse	basse	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
jeden	jeden	k4xCgInSc4
či	či	k8xC
dva	dva	k4xCgInPc4
ženské	ženský	k2eAgInPc4d1
<g/>
,	,	kIx,
totiž	totiž	k9
dessus	dessus	k1gMnSc1
či	či	k8xC
bas-dessus	bas-dessus	k1gMnSc1
(	(	kIx(
<g/>
alt	alt	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
sólisté	sólista	k1gMnPc1
byli	být	k5eAaImAgMnP
zpravidla	zpravidla	k6eAd1
profesionální	profesionální	k2eAgMnPc1d1
pěvci	pěvec	k1gMnPc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
Orchestrální	orchestrální	k2eAgInSc1d1
doprovod	doprovod	k1gInSc1
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
velký	velký	k2eAgInSc1d1
sbor	sbor	k1gInSc1
pětihlasý	pětihlasý	k2eAgMnSc1d1
a	a	k8xC
nástroje	nástroj	k1gInPc1
většinou	většinou	k6eAd1
dublují	dublovat	k5eAaImIp3nP
některý	některý	k3yIgInSc4
z	z	k7c2
hlasů	hlas	k1gInPc2
<g/>
;	;	kIx,
od	od	k7c2
Te	Te	k1gFnSc2
Deum	Deum	k1gInSc1
zařadil	zařadit	k5eAaPmAgMnS
skladatel	skladatel	k1gMnSc1
do	do	k7c2
orchestru	orchestr	k1gInSc2
trubky	trubka	k1gFnSc2
a	a	k8xC
tympány	tympán	k1gInPc4
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
v	v	k7c6
církevní	církevní	k2eAgFnSc6d1
hudbě	hudba	k1gFnSc6
nepoužívané	používaný	k2eNgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
Vedle	vedle	k7c2
zpívaných	zpívaný	k2eAgFnPc2d1
částí	část	k1gFnPc2
obsahují	obsahovat	k5eAaImIp3nP
Lullyho	Lully	k1gMnSc4
moteta	moteto	k1gNnSc2
rozměrné	rozměrný	k2eAgFnSc2d1
instrumentální	instrumentální	k2eAgFnSc2d1
části	část	k1gFnSc2
<g/>
,	,	kIx,
„	„	k?
<g/>
symphonies	symphonies	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
ritournelles	ritournelles	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
první	první	k4xOgFnSc1
velká	velká	k1gFnSc1
moteta	moteto	k1gNnSc2
měla	mít	k5eAaImAgFnS
tradičně	tradičně	k6eAd1
uměřenou	uměřený	k2eAgFnSc4d1
a	a	k8xC
vyváženou	vyvážený	k2eAgFnSc4d1
melodicko-harmonickou	melodicko-harmonický	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
,	,	kIx,
od	od	k7c2
Plaude	Plaud	k1gInSc5
laetare	laetar	k1gMnSc5
Gallia	Gallium	k1gNnPc1
Lully	Lull	k1gInPc4
výrazně	výrazně	k6eAd1
posílil	posílit	k5eAaPmAgInS
harmonickou	harmonický	k2eAgFnSc4d1
složku	složka	k1gFnSc4
a	a	k8xC
využíval	využívat	k5eAaImAgInS,k5eAaPmAgInS
kontrapunkt	kontrapunkt	k1gInSc1
a	a	k8xC
imitace	imitace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Pod	pod	k7c7
vlivem	vliv	k1gInSc7
zkušeností	zkušenost	k1gFnPc2
z	z	k7c2
jevištní	jevištní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
moteta	moteto	k1gNnSc2
stávaly	stávat	k5eAaImAgFnP
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
dál	daleko	k6eAd2
divadelnějšími	divadelní	k2eAgMnPc7d2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
zejména	zejména	k9
platí	platit	k5eAaImIp3nS
o	o	k7c6
poslední	poslední	k2eAgFnSc6d1
šestici	šestice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
použité	použitý	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
patřily	patřit	k5eAaImAgFnP
dynamické	dynamický	k2eAgInPc1d1
a	a	k8xC
harmonické	harmonický	k2eAgInPc1d1
kontrasty	kontrast	k1gInPc1
<g/>
,	,	kIx,
změny	změna	k1gFnPc1
rytmického	rytmický	k2eAgInSc2d1
základu	základ	k1gInSc2
<g/>
,	,	kIx,
čím	co	k3yInSc7,k3yRnSc7,k3yQnSc7
dále	daleko	k6eAd2
„	„	k?
<g/>
opernější	operný	k2eAgNnSc1d2
<g/>
“	“	k?
využívání	využívání	k1gNnSc1
malého	malý	k2eAgInSc2d1
sboru	sbor	k1gInSc2
k	k	k7c3
áriím	árie	k1gFnPc3
<g/>
,	,	kIx,
duetům	duet	k1gInPc3
či	či	k8xC
tercetům	tercet	k1gInPc3
<g/>
,	,	kIx,
individuálnější	individuální	k2eAgNnSc4d2
využívání	využívání	k1gNnSc4
zejména	zejména	k9
dechových	dechový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
Dramatického	dramatický	k2eAgInSc2d1
účinku	účinek	k1gInSc2
dosahoval	dosahovat	k5eAaImAgInS
také	také	k9
častými	častý	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
tempa	tempo	k1gNnSc2
–	–	k?
například	například	k6eAd1
v	v	k7c4
Dies	Dies	k1gInSc4
irae	ira	k1gInSc2
27	#num#	k4
<g/>
krát	krát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
Vliv	vliv	k1gInSc1
italské	italský	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
zřetelný	zřetelný	k2eAgInSc1d1
zejména	zejména	k9
v	v	k7c6
chromatismech	chromatismus	k1gInPc6
v	v	k7c6
pasážích	pasáž	k1gFnPc6
připomínajících	připomínající	k2eAgFnPc6d1
žalozpěv	žalozpěv	k1gInSc4
<g/>
,	,	kIx,
postupně	postupně	k6eAd1
slábne	slábnout	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Malá	Malá	k1gFnSc1
moteta	moteto	k1gNnSc2
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
často	často	k6eAd1
v	v	k7c6
pozdních	pozdní	k2eAgInPc6d1
opisech	opis	k1gInPc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
s	s	k7c7
nimi	on	k3xPp3gFnPc7
spojeny	spojen	k2eAgFnPc1d1
různé	různý	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
třinácti	třináct	k4xCc2
malých	malý	k2eAgNnPc2d1
motet	moteto	k1gNnPc2
původně	původně	k6eAd1
připisovaných	připisovaný	k2eAgNnPc2d1
Lullymu	Lullym	k1gInSc6
je	on	k3xPp3gInPc4
dnes	dnes	k6eAd1
u	u	k7c2
dvou	dva	k4xCgNnPc2
jeho	jeho	k3xOp3gNnPc2
autorství	autorství	k1gNnSc2
vyloučeno	vyloučit	k5eAaPmNgNnS
a	a	k8xC
u	u	k7c2
třetího	třetí	k4xOgNnSc2
<g/>
,	,	kIx,
Exaudi	Exaud	k1gMnPc1
Deus	Deusa	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ze	z	k7c2
stylových	stylový	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
pochybné	pochybný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesporných	sporný	k2eNgMnPc2d1
zůstává	zůstávat	k5eAaImIp3nS
deset	deset	k4xCc1
<g/>
:	:	kIx,
Anima	animo	k1gNnPc1
Christi	Christ	k1gMnPc1
<g/>
,	,	kIx,
Ave	ave	k1gNnSc6
coeli	coenout	k5eAaPmAgMnP,k5eAaImAgMnP
munus	munus	k1gMnSc1
supernum	supernum	k1gInSc4
<g/>
,	,	kIx,
Dixit	Dixit	k2eAgInSc4d1
Dominus	Dominus	k1gInSc4
<g/>
,	,	kIx,
Domine	Domin	k1gInSc5
salvum	salvum	k1gNnSc4
fac	fac	k?
regem	regem	k1gInSc1
<g/>
,	,	kIx,
Laudate	Laudat	k1gMnSc5
pueri	puer	k1gMnSc5
Domium	Domium	k1gNnSc1
<g/>
,	,	kIx,
O	o	k7c6
dulcissime	dulcissim	k1gMnSc5
Domine	Domin	k1gMnSc5
<g/>
,	,	kIx,
Omnes	Omnes	k1gMnSc1
gentes	gentes	k1gMnSc1
plaudite	plaudit	k1gInSc5
manibus	manibus	k1gInSc4
<g/>
,	,	kIx,
O	o	k7c4
sapientia	sapientius	k1gMnSc4
in	in	k?
misterio	misterio	k1gMnSc1
<g/>
,	,	kIx,
Regina	Regina	k1gFnSc1
coeli	coele	k1gFnSc4
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
Salve	Salve	k1gNnSc1
Regina	Regina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
Pocházejí	pocházet	k5eAaImIp3nP
všechna	všechen	k3xTgNnPc4
nejpravděpodobněji	pravděpodobně	k6eAd3
z	z	k7c2
let	léto	k1gNnPc2
1683	#num#	k4
<g/>
–	–	k?
<g/>
1686	#num#	k4
a	a	k8xC
podle	podle	k7c2
Philidorových	Philidorův	k2eAgInPc2d1
údajů	údaj	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1729	#num#	k4
byly	být	k5eAaImAgFnP
složeny	složit	k5eAaPmNgInP
pro	pro	k7c4
klášter	klášter	k1gInSc4
dcer	dcera	k1gFnPc2
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
jejich	jejich	k3xOp3gInSc6
kostele	kostel	k1gInSc6
se	se	k3xPyFc4
konaly	konat	k5eAaImAgFnP
slavné	slavný	k2eAgNnSc4d1
hudební	hudební	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
produkce	produkce	k1gFnSc1
<g/>
,	,	kIx,
jichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
účastnily	účastnit	k5eAaImAgFnP
i	i	k9
profesionální	profesionální	k2eAgFnPc1d1
pěvkyně	pěvkyně	k1gFnPc1
či	či	k8xC
pěvci	pěvec	k1gMnPc1
z	z	k7c2
blízké	blízký	k2eAgFnSc2d1
Opery	opera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
Lullyho	Lully	k1gMnSc4
malá	malý	k2eAgNnPc1d1
moteta	moteto	k1gNnPc1
jsou	být	k5eAaImIp3nP
složena	složit	k5eAaPmNgNnP
pro	pro	k7c4
tři	tři	k4xCgInPc4
hlasy	hlas	k1gInPc4
–	–	k?
většinou	většinou	k6eAd1
ženské	ženská	k1gFnPc1
<g/>
,	,	kIx,
soprány	soprán	k1gInPc1
(	(	kIx(
<g/>
dessus	dessus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
alty	alt	k1gInPc1
(	(	kIx(
<g/>
bas-dessus	bas-dessus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
tři	tři	k4xCgMnPc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
vyžadují	vyžadovat	k5eAaImIp3nP
i	i	k9
mužské	mužský	k2eAgInPc4d1
hlasy	hlas	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
Doprovod	doprovod	k1gInSc1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
pro	pro	k7c4
basso	bassa	k1gFnSc5
continuo	continuo	k6eAd1
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
violoncello	violoncello	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
při	při	k7c6
předehrách	předehra	k1gFnPc6
a	a	k8xC
mezihrách	mezihra	k1gFnPc6
přistupují	přistupovat	k5eAaImIp3nP
„	„	k?
<g/>
dessus	dessus	k1gInSc4
<g/>
“	“	k?
(	(	kIx(
<g/>
housle	housle	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
violon	violon	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
viola	viola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Texty	text	k1gInPc1
jsou	být	k5eAaImIp3nP
latinské	latinský	k2eAgInPc1d1
<g/>
,	,	kIx,
vzaté	vzatý	k2eAgInPc1d1
ze	z	k7c2
žalmů	žalm	k1gInPc2
či	či	k8xC
tradiční	tradiční	k2eAgFnSc2d1
liturgie	liturgie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
Více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
jiné	jiná	k1gFnSc6
Lullyho	Lully	k1gMnSc2
tvorbě	tvorba	k1gFnSc3
z	z	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
vrcholného	vrcholný	k2eAgNnSc2d1
období	období	k1gNnSc2
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gNnPc6
objevují	objevovat	k5eAaImIp3nP
italské	italský	k2eAgInPc1d1
rysy	rys	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
chromatismy	chromatismus	k1gInPc1
<g/>
,	,	kIx,
vokalízy	vokalíza	k1gFnPc1
a	a	k8xC
recitativy	recitativ	k1gInPc1
v	v	k7c6
pravidelném	pravidelný	k2eAgInSc6d1
rytmu	rytmus	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1
práce	práce	k1gFnPc1
</s>
<s>
Scénická	scénický	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
a	a	k8xC
moteta	moteto	k1gNnPc1
tvoří	tvořit	k5eAaImIp3nP
největší	veliký	k2eAgFnSc4d3
část	část	k1gFnSc4
Lullyho	Lully	k1gMnSc2
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
drobnější	drobný	k2eAgNnPc4d2
vokální	vokální	k2eAgNnPc4d1
a	a	k8xC
instrumentální	instrumentální	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
příležitostné	příležitostný	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
,	,	kIx,
komponovaná	komponovaný	k2eAgFnSc1d1
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
slavnostní	slavnostní	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
spojené	spojený	k2eAgFnPc4d1
s	s	k7c7
Lullyho	Lullyha	k1gFnSc5
funkcemi	funkce	k1gFnPc7
u	u	k7c2
královského	královský	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
pochodů	pochod	k1gInPc2
(	(	kIx(
<g/>
Premiè	Premiè	k1gMnSc5
marche	marchus	k1gMnSc5
des	des	k1gNnSc3
mousquetaires	mousquetaires	k1gInSc1
1658	#num#	k4
<g/>
,	,	kIx,
Marches	Marches	k1gMnSc1
et	et	k?
batteries	batteries	k1gMnSc1
de	de	k?
tambour	tambour	k1gMnSc1
1670	#num#	k4
<g/>
,	,	kIx,
Marches	Marches	k1gMnSc1
pour	pour	k1gMnSc1
le	le	k?
régiment	régiment	k1gMnSc1
de	de	k?
Savoie	Savoie	k1gFnSc2
<g/>
,	,	kIx,
1685	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
soubor	soubor	k1gInSc1
54	#num#	k4
trií	trio	k1gNnPc2
pro	pro	k7c4
dvoje	dvoje	k4xRgFnPc4
housle	housle	k1gFnPc4
a	a	k8xC
basso	bassa	k1gFnSc5
continuo	continuo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
například	například	k6eAd1
i	i	k9
hudba	hudba	k1gFnSc1
pro	pro	k7c4
kolotoče	kolotoč	k1gInSc2
králova	králův	k2eAgMnSc4d1
bratra	bratr	k1gMnSc4
(	(	kIx(
<g/>
Airs	Airs	k1gInSc1
pour	pour	k1gMnSc1
le	le	k?
Carrousel	Carrousel	k1gMnSc1
de	de	k?
Monseigneur	monseigneur	k1gMnSc1
<g/>
,	,	kIx,
1686	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
řady	řada	k1gFnSc2
drobných	drobný	k2eAgFnPc2d1
vokálních	vokální	k2eAgFnPc2d1
skladeb	skladba	k1gFnPc2
označených	označený	k2eAgFnPc2d1
většinou	většinou	k6eAd1
jako	jako	k9
„	„	k?
<g/>
air	air	k?
<g/>
“	“	k?
na	na	k7c4
slova	slovo	k1gNnPc4
Philippa	Philipp	k1gMnSc2
Quinaulta	Quinault	k1gMnSc2
<g/>
,	,	kIx,
Isaaca	Isaac	k1gInSc2
de	de	k?
Benserade	Benserad	k1gInSc5
<g/>
,	,	kIx,
Pierra	Pierr	k1gMnSc2
Perrina	Perrin	k1gMnSc2
a	a	k8xC
dalších	další	k2eAgMnPc2d1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
u	u	k7c2
řady	řada	k1gFnSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
tanců	tanec	k1gInPc2
(	(	kIx(
<g/>
allemande	allemand	k1gInSc5
<g/>
,	,	kIx,
bourrée	bourrée	k1gNnPc1
<g/>
,	,	kIx,
sarabande	saraband	k1gInSc5
<g/>
,	,	kIx,
chaconne	chaconn	k1gMnSc5
<g/>
,	,	kIx,
především	především	k9
však	však	k9
Ludvíkem	Ludvík	k1gMnSc7
XIV	XIV	kA
<g/>
.	.	kIx.
zvláště	zvláště	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
courante	courant	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
dochovaly	dochovat	k5eAaPmAgFnP
bez	bez	k7c2
spolehlivé	spolehlivý	k2eAgFnSc2d1
datace	datace	k1gFnSc2
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
vznikly	vzniknout	k5eAaPmAgFnP
samostatně	samostatně	k6eAd1
nebo	nebo	k8xC
jako	jako	k9
součást	součást	k1gFnSc4
dvorských	dvorský	k2eAgInPc2d1
baletů	balet	k1gInPc2
nebo	nebo	k8xC
„	„	k?
<g/>
divertissements	divertissements	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
od	od	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
stylově	stylově	k6eAd1
neodlišují	odlišovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Instrumentální	instrumentální	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
jiná	jiný	k2eAgFnSc1d1
než	než	k8xS
taneční	taneční	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
v	v	k7c6
Lullyho	Lully	k1gMnSc2
díle	díl	k1gInSc6
výjimkou	výjimka	k1gFnSc7
(	(	kIx(
<g/>
Plusieurs	Plusieurs	k1gInSc1
piè	piè	k1gInSc1
de	de	k?
symphonie	symphonie	k1gFnSc2
<g/>
,	,	kIx,
1685	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sólová	sólový	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
pro	pro	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Film	film	k1gInSc1
Král	Král	k1gMnSc1
tančí	tančit	k5eAaImIp3nS
líčí	líčit	k5eAaImIp3nS
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
studie	studie	k1gFnSc2
Lully	Lulla	k1gFnSc2
ou	ou	k0
le	le	k?
musicien	musicien	k2eAgInSc4d1
du	du	k?
soleil	soleil	k1gInSc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
:	:	kIx,
Lully	Lulla	k1gFnPc1
<g/>
,	,	kIx,
hudebník	hudebník	k1gMnSc1
Slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
od	od	k7c2
Philippe	Philipp	k1gInSc5
Beaussanta	Beaussant	k1gMnSc2
<g/>
,	,	kIx,
Lullyho	Lully	k1gMnSc2
vztah	vztah	k1gInSc4
ke	k	k7c3
králi	král	k1gMnSc3
a	a	k8xC
k	k	k7c3
Moliè	Moliè	k1gMnSc3
a	a	k8xC
rozebírá	rozebírat	k5eAaImIp3nS
Lullyho	Lully	k1gMnSc4
život	život	k1gInSc4
ve	v	k7c6
Versailles	Versailles	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
p	p	k?
18	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
filmu	film	k1gInSc6
je	být	k5eAaImIp3nS
použita	použít	k5eAaPmNgFnS
pouze	pouze	k6eAd1
Lullyho	Lully	k1gMnSc4
hudba	hudba	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
kladen	kladen	k2eAgInSc1d1
velký	velký	k2eAgInSc1d1
důraz	důraz	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Portrét	portrét	k1gInSc4
několika	několik	k4yIc2
hudebníků	hudebník	k1gMnPc2
a	a	k8xC
umělců	umělec	k1gMnPc2
(	(	kIx(
<g/>
François	François	k1gFnSc1
Puget	Puget	k?
<g/>
,	,	kIx,
1688	#num#	k4
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
Louvre	Louvre	k1gInSc1
<g/>
)	)	kIx)
Dvě	dva	k4xCgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
označovány	označovat	k5eAaImNgFnP
jako	jako	k9
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc1
a	a	k8xC
Philippe	Philipp	k1gInSc5
Quinault	Quinault	k1gInSc4
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnPc1
(	(	kIx(
<g/>
obraz	obraz	k1gInSc1
od	od	k7c2
Nicolase	Nicolasa	k1gFnSc6
Mignarda	Mignard	k1gMnSc4
<g/>
)	)	kIx)
</s>
<s>
Lullyho	Lullyze	k6eAd1
hrobka	hrobka	k1gFnSc1
v	v	k7c6
bazilice	bazilika	k1gFnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Vítězné	vítězný	k2eAgFnSc2d1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
zvanou	zvaný	k2eAgFnSc4d1
la	la	k0
Grande	grand	k1gMnSc5
Mademoiselle	Mademoisell	k1gMnSc5
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
sestřenicí	sestřenice	k1gFnSc7
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Balet	balet	k1gInSc1
Noci	noc	k1gFnSc2
byl	být	k5eAaImAgInS
kolektivním	kolektivní	k2eAgNnSc7d1
dílem	dílo	k1gNnSc7
<g/>
;	;	kIx,
jako	jako	k8xC,k8xS
autoři	autor	k1gMnPc1
jsou	být	k5eAaImIp3nP
uváděni	uvádět	k5eAaImNgMnP
zejména	zejména	k9
Jean	Jean	k1gMnSc1
de	de	k?
Cambefort	Cambefort	k1gInSc1
<g/>
,	,	kIx,
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Boësset	Boësset	k1gMnSc1
<g/>
,	,	kIx,
Michel	Michel	k1gMnSc1
Lambert	Lambert	k1gMnSc1
<g/>
,	,	kIx,
některé	některý	k3yIgInPc4
zdroje	zdroj	k1gInPc4
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
J.	J.	kA
<g/>
–	–	k?
<g/>
B.	B.	kA
Lullyho	Lully	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
tančil	tančit	k5eAaImAgMnS
v	v	k7c6
pěti	pět	k4xCc6
rolích	role	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Baletu	balet	k1gInSc6
Noci	noc	k1gFnSc2
pravděpodobně	pravděpodobně	k6eAd1
spolupracovali	spolupracovat	k5eAaImAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
tou	ten	k3xDgFnSc7
dobou	doba	k1gFnSc7
pobývali	pobývat	k5eAaImAgMnP
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veršované	veršovaný	k2eAgNnSc1d1
libreto	libreto	k1gNnSc4
sepsal	sepsat	k5eAaPmAgInS
Isaac	Isaac	k1gInSc1
de	de	k?
Benserade	Benserad	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Odtud	odtud	k6eAd1
pochází	pocházet	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc4
přízvisko	přízvisko	k1gNnSc4
Král	Král	k1gMnSc1
Slunce	slunce	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
však	však	k9
rozšířilo	rozšířit	k5eAaPmAgNnS
až	až	k9
za	za	k7c4
červencové	červencový	k2eAgFnPc4d1
monarchie	monarchie	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Na	na	k7c6
tomto	tento	k3xDgInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
postu	post	k1gInSc6
vystřídal	vystřídat	k5eAaPmAgMnS
skladatele	skladatel	k1gMnSc4
Lazarina	Lazarin	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgMnS
původem	původ	k1gInSc7
také	také	k9
Ital	Ital	k1gMnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Velké	velká	k1gFnSc2
století	století	k1gNnSc2
(	(	kIx(
<g/>
Le	Le	k1gFnSc1
Grand	grand	k1gMnSc1
Siè	Siè	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
ve	v	k7c6
francouzských	francouzský	k2eAgFnPc6d1
dějinách	dějiny	k1gFnPc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Král	Král	k1gMnSc1
dvořanům	dvořan	k1gMnPc3
nařídil	nařídit	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
povinně	povinně	k6eAd1
účastnili	účastnit	k5eAaImAgMnP
dvorních	dvorní	k2eAgMnPc2d1
baletů	balet	k1gInPc2
jako	jako	k8xS,k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
tanečníci	tanečník	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
První	první	k4xOgMnSc1
dvorní	dvorní	k2eAgInSc1d1
balet	balet	k1gInSc1
(	(	kIx(
<g/>
Mascarade	Mascarad	k1gInSc5
de	de	k?
la	la	k0
Foire	Foir	k1gInSc5
de	de	k?
St-Germain	St-Germain	k1gInSc1
<g/>
)	)	kIx)
Lully	Lulla	k1gFnPc1
zkomponoval	zkomponovat	k5eAaPmAgInS
roku	rok	k1gInSc2
1652	#num#	k4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
du	du	k?
Moustierem	Moustier	k1gInSc7
pro	pro	k7c4
Grande	grand	k1gMnSc5
Mademoiselle	Mademoiselle	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Isac	Isac	k1gInSc1
de	de	k?
Benserade	Benserad	k1gInSc5
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
Lullym	Lullym	k1gInSc4
na	na	k7c6
dvaceti	dvacet	k4xCc6
baletech	balet	k1gInPc6
v	v	k7c6
letech	let	k1gInPc6
1653	#num#	k4
<g/>
–	–	k?
<g/>
1669	#num#	k4
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1681	#num#	k4
na	na	k7c6
Le	Le	k1gFnSc6
Triomphe	Triomph	k1gInSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Amour	Amour	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnSc1
Ballet	Ballet	k1gInSc4
Royal	Royal	k1gInSc1
de	de	k?
Flore	Flor	k1gMnSc5
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
premiéru	premiér	k1gMnSc3
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1669	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
posledním	poslední	k2eAgInSc7d1
dvorním	dvorní	k2eAgInSc7d1
baletem	balet	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Hry	hra	k1gFnPc1
Moliè	Moliè	k1gMnSc2
a	a	k8xC
Lullyho	Lully	k1gMnSc2
jsou	být	k5eAaImIp3nP
až	až	k9
na	na	k7c4
výjimku	výjimka	k1gFnSc4
(	(	kIx(
<g/>
Psyché	psyché	k1gFnSc4
<g/>
)	)	kIx)
komedie	komedie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moliè	Moliè	k1gInSc5
prý	prý	k9
Lullymu	Lullymum	k1gNnSc3
často	často	k6eAd1
říkal	říkat	k5eAaImAgMnS
<g/>
:	:	kIx,
„	„	k?
<g/>
Lully	Lulla	k1gFnSc2
<g/>
,	,	kIx,
fais	fais	k6eAd1
<g/>
–	–	k?
<g/>
nous	nous	k6eAd1
rire	rirat	k5eAaPmIp3nS
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
(	(	kIx(
<g/>
„	„	k?
<g/>
Lully	Lulla	k1gFnSc2
<g/>
,	,	kIx,
pobav	pobava	k1gFnPc2
nás	my	k3xPp1nPc2
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měšťák	měšťák	k1gMnSc1
šlechticem	šlechtic	k1gMnSc7
vznikl	vzniknout	k5eAaPmAgMnS
na	na	k7c4
objednávku	objednávka	k1gFnSc4
krále	král	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
nespokojen	spokojen	k2eNgMnSc1d1
s	s	k7c7
návštěvou	návštěva	k1gFnSc7
tureckého	turecký	k2eAgMnSc2d1
vyslance	vyslanec	k1gMnSc2
<g/>
;	;	kIx,
požadavkem	požadavek	k1gInSc7
pro	pro	k7c4
autory	autor	k1gMnPc4
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
jej	on	k3xPp3gMnSc4
ve	v	k7c6
hře	hra	k1gFnSc6
zesměšnili	zesměšnit	k5eAaPmAgMnP
tureckým	turecký	k2eAgInSc7d1
baletem	balet	k1gInSc7
(	(	kIx(
<g/>
jehož	jehož	k3xOyRp3gInSc7
obsahem	obsah	k1gInSc7
je	být	k5eAaImIp3nS
zesměšňující	zesměšňující	k2eAgInSc1d1
„	„	k?
<g/>
pasování	pasování	k1gNnPc2
<g/>
“	“	k?
na	na	k7c4
šlechtice	šlechtic	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Příčinou	příčina	k1gFnSc7
roztržky	roztržka	k1gFnSc2
zřejmě	zřejmě	k6eAd1
byla	být	k5eAaImAgFnS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
Moliè	Moliè	k1gInSc5
uváděl	uvádět	k5eAaImAgMnS
Psyché	psyché	k1gFnSc1
a	a	k8xC
jiná	jiný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
s	s	k7c7
Lullyho	Lully	k1gMnSc4
hudbou	hudba	k1gFnSc7
mimo	mimo	k7c4
královský	královský	k2eAgInSc4d1
dvůr	dvůr	k1gInSc4
se	s	k7c7
značným	značný	k2eAgInSc7d1
úspěchem	úspěch	k1gInSc7
a	a	k8xC
ziskem	zisk	k1gInSc7
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
skladatel	skladatel	k1gMnSc1
měl	mít	k5eAaImAgMnS
jakýkoli	jakýkoli	k3yIgInSc4
příjem	příjem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lully	Lullo	k1gNnPc7
proto	proto	k8xC
po	po	k7c6
získání	získání	k1gNnSc6
operního	operní	k2eAgNnSc2d1
privilegia	privilegium	k1gNnSc2
na	na	k7c4
operu	opera	k1gFnSc4
usiloval	usilovat	k5eAaImAgMnS
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ostatním	ostatní	k2eAgNnPc3d1
divadlům	divadlo	k1gNnPc3
(	(	kIx(
<g/>
především	především	k6eAd1
Moliè	Moliè	k2eAgFnSc4d1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
zakázáno	zakázán	k2eAgNnSc1d1
hrát	hrát	k5eAaImF
hry	hra	k1gFnPc4
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
dvěma	dva	k4xCgFnPc7
áriemi	árie	k1gFnPc7
a	a	k8xC
s	s	k7c7
doprovodem	doprovod	k1gInSc7
více	hodně	k6eAd2
než	než	k8xS
dvou	dva	k4xCgInPc2
nástrojů	nástroj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Moliè	Moliè	k1gInSc5
zemřel	zemřít	k5eAaPmAgMnS
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Charpentier	Charpentira	k1gFnPc2
později	pozdě	k6eAd2
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
skladatel	skladatel	k1gMnSc1
Ludvíka	Ludvík	k1gMnSc2
Francouzského	francouzský	k2eAgMnSc2d1
<g/>
,	,	kIx,
syna	syn	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
Lullyho	Lully	k1gMnSc2
vlivu	vliv	k1gInSc2
však	však	k8xC
tuto	tento	k3xDgFnSc4
pozici	pozice	k1gFnSc4
ztratil	ztratit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Lully	Lull	k1gInPc1
s	s	k7c7
Perrinem	Perrin	k1gInSc7
spolupracoval	spolupracovat	k5eAaImAgInS
již	již	k9
rok	rok	k1gInSc1
před	před	k7c7
založením	založení	k1gNnSc7
Académie	Académie	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
křtinami	křtiny	k1gFnPc7
dauphina	dauphin	k1gMnSc2
Ludvíka	Ludvík	k1gMnSc2
komponoval	komponovat	k5eAaImAgMnS
hudbu	hudba	k1gFnSc4
na	na	k7c4
Perrinův	Perrinův	k2eAgInSc4d1
text	text	k1gInSc4
moteta	moteto	k1gNnSc2
Plaude	Plaud	k1gInSc5
Laetare	Laetar	k1gMnSc5
Gallia	Gallium	k1gNnPc4
(	(	kIx(
<g/>
Raduj	radovat	k5eAaImRp2nS
se	se	k3xPyFc4
a	a	k8xC
zpívej	zpívat	k5eAaImRp2nS
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Perrin	Perrin	k1gInSc1
s	s	k7c7
Cambertem	Cambert	k1gInSc7
byli	být	k5eAaImAgMnP
ředitelé	ředitel	k1gMnPc1
v	v	k7c6
letech	léto	k1gNnPc6
1669	#num#	k4
<g/>
–	–	k?
<g/>
1672	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Některé	některý	k3yIgInPc4
zdroje	zdroj	k1gInPc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nápad	nápad	k1gInSc1
na	na	k7c4
získání	získání	k1gNnSc4
privilegií	privilegium	k1gNnPc2
od	od	k7c2
Perrina	Perrino	k1gNnSc2
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
Moliè	Moliè	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moliè	Moliè	k1gInSc5
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
svěřit	svěřit	k5eAaPmF
Lullymu	Lullym	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
jej	on	k3xPp3gMnSc4
předběhl	předběhnout	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
úvahy	úvaha	k1gFnPc1
se	se	k3xPyFc4
však	však	k9
opírají	opírat	k5eAaImIp3nP
o	o	k7c4
málo	málo	k6eAd1
důvěryhodné	důvěryhodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
z	z	k7c2
doby	doba	k1gFnSc2
po	po	k7c6
smrti	smrt	k1gFnSc6
obou	dva	k4xCgMnPc2
protagonistů	protagonista	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Po	po	k7c6
Lullyho	Lully	k1gMnSc2
smrti	smrt	k1gFnSc6
v	v	k7c6
březnu	březen	k1gInSc6
1687	#num#	k4
získal	získat	k5eAaPmAgMnS
privilegium	privilegium	k1gNnSc4
Jean	Jean	k1gMnSc1
Nicolas	Nicolas	k1gMnSc1
de	de	k?
Francine	Francin	k1gInSc5
(	(	kIx(
<g/>
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
ředitelem	ředitel	k1gMnSc7
v	v	k7c6
letech	let	k1gInPc6
1687	#num#	k4
<g/>
–	–	k?
<g/>
1704	#num#	k4
a	a	k8xC
1712	#num#	k4
<g/>
–	–	k?
<g/>
1728	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
zejména	zejména	k9
o	o	k7c6
založení	založení	k1gNnSc6
oper	opera	k1gFnPc2
v	v	k7c6
Lyonu	Lyon	k1gInSc6
<g/>
,	,	kIx,
Lille	Lillo	k1gNnSc6
<g/>
,	,	kIx,
Bordeaux	Bordeaux	k1gNnSc6
a	a	k8xC
Rouenu	Roueno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francine	Francin	k1gInSc5
nebyl	být	k5eNaImAgInS
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
uváděl	uvádět	k5eAaImAgMnS
díla	dílo	k1gNnPc4
Lullyho	Lully	k1gMnSc2
syna	syn	k1gMnSc2
Louise	Louis	k1gMnSc2
<g/>
,	,	kIx,
Collasseho	Collasse	k1gMnSc2
<g/>
,	,	kIx,
Élisabeth	Élisabeth	k1gInSc4
Jacquet	Jacqueta	k1gFnPc2
de	de	k?
La	la	k1gNnSc4
Guerre	Guerr	k1gInSc5
a	a	k8xC
Charpentiera	Charpentiero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navzdory	navzdory	k7c3
podpoře	podpora	k1gFnSc3
krále	král	k1gMnSc2
upadl	upadnout	k5eAaPmAgMnS
Francine	Francin	k1gInSc5
do	do	k7c2
dluhů	dluh	k1gInPc2
a	a	k8xC
privilegium	privilegium	k1gNnSc4
musel	muset	k5eAaImAgMnS
roku	rok	k1gInSc2
1704	#num#	k4
převést	převést	k5eAaPmF
na	na	k7c4
Pierre	Pierr	k1gInSc5
Guyeneta	Guyenet	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
V	v	k7c6
opeře	opera	k1gFnSc6
Atys	Atysa	k1gFnPc2
začíná	začínat	k5eAaImIp3nS
prolog	prolog	k1gInSc4
árií	árie	k1gFnPc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
zpívá	zpívat	k5eAaImIp3nS
Čas	čas	k1gInSc1
<g/>
:	:	kIx,
Čas	čas	k1gInSc1
si	se	k3xPyFc3
stěžuje	stěžovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
marně	marně	k6eAd1
zachovával	zachovávat	k5eAaImAgInS
jména	jméno	k1gNnPc4
slavných	slavný	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
dějin	dějiny	k1gFnPc2
<g/>
:	:	kIx,
jméno	jméno	k1gNnSc1
současného	současný	k2eAgMnSc2d1
hrdiny	hrdina	k1gMnSc2
(	(	kIx(
<g/>
=	=	kIx~
Ludvíka	Ludvík	k1gMnSc4
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
všechna	všechen	k3xTgNnPc1
zastiňuje	zastiňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
i	i	k9
dvanáct	dvanáct	k4xCc4
Hodin	hodina	k1gFnPc2
(	(	kIx(
<g/>
recitativ	recitativ	k1gInSc1
a	a	k8xC
sbor	sbor	k1gInSc1
En	En	k1gMnSc1
vain	vain	k1gMnSc1
j	j	k?
<g/>
'	'	kIx"
<g/>
ai	ai	k?
respecté	respectý	k2eAgFnSc2d1
<g/>
…	…	k?
Ses	Ses	k1gMnSc1
justes	justes	k1gMnSc1
lois	lois	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Slovy	slovo	k1gNnPc7
dirigenta	dirigent	k1gMnSc4
a	a	k8xC
specialisty	specialista	k1gMnPc4
na	na	k7c4
francouzský	francouzský	k2eAgInSc4d1
barokní	barokní	k2eAgInSc4d1
repertoár	repertoár	k1gInSc4
Hervého	Hervý	k2eAgNnSc2d1
Niqueta	Niqueto	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
67	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Byli	být	k5eAaImAgMnP
to	ten	k3xDgNnSc1
Catherine-Madelaine	Catherine-Madelain	k1gInSc5
(	(	kIx(
<g/>
1663	#num#	k4
<g/>
–	–	k?
<g/>
1703	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
provdaná	provdaný	k2eAgFnSc1d1
za	za	k7c4
Jeana-Nicolase	Jeana-Nicolas	k1gInSc5
de	de	k?
Francine	Francin	k1gInSc5
<g/>
;	;	kIx,
Louis	Louis	k1gMnSc1
(	(	kIx(
<g/>
1664	#num#	k4
<g/>
–	–	k?
<g/>
1734	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Jean-Baptiste	Jean-Baptist	k1gMnSc5
(	(	kIx(
<g/>
1665	#num#	k4
<g/>
–	–	k?
<g/>
1743	#num#	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
Gabrielle-Hilaire	Gabrielle-Hilair	k1gInSc5
(	(	kIx(
<g/>
1666	#num#	k4
<g/>
–	–	k?
<g/>
1748	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
provdaná	provdaný	k2eAgFnSc1d1
za	za	k7c2
Jacquese	Jacques	k1gMnSc2
Dumoulina	Dumoulin	k2eAgMnSc2d1
<g/>
;	;	kIx,
Jean-Louis	Jean-Louis	k1gInSc1
(	(	kIx(
<g/>
1667	#num#	k4
<g/>
–	–	k?
<g/>
1688	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
a	a	k8xC
Marie-Louise	Marie-Louise	k1gFnSc1
(	(	kIx(
<g/>
1668	#num#	k4
<g/>
–	–	k?
<g/>
1715	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
později	pozdě	k6eAd2
provdaná	provdaný	k2eAgFnSc1d1
za	za	k7c4
Pierra	Pierr	k1gMnSc4
Thiersaulta	Thiersault	k1gInSc2
de	de	k?
Mérancourt	Mérancourt	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Ve	v	k7c6
filmu	film	k1gInSc6
však	však	k9
nevystupují	vystupovat	k5eNaImIp3nP
další	další	k2eAgFnPc4d1
klíčové	klíčový	k2eAgFnPc4d1
osobnosti	osobnost	k1gFnPc4
<g/>
,	,	kIx,
například	například	k6eAd1
Philippe	Philipp	k1gInSc5
Quinault	Quinault	k1gMnSc1
či	či	k8xC
Filip	Filip	k1gMnSc1
I.	I.	kA
Orleánský	orleánský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PRUNIÈ	PRUNIÈ	k1gMnSc1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
<g/>
;	;	kIx,
DE	DE	k?
LA	la	k1gNnSc1
LAURENCIE	LAURENCIE	kA
<g/>
,	,	kIx,
Lionel	Lionel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
La	la	k1gNnSc7
jeunesse	jeunesse	k1gFnSc2
de	de	k?
Lully	Lulla	k1gFnSc2
(	(	kIx(
<g/>
1632	#num#	k4
<g/>
–	–	k?
<g/>
1662	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Essai	Essa	k1gFnSc2
de	de	k?
biographie	biographie	k1gFnSc2
critique	critiqu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
français	français	k1gInSc4
de	de	k?
la	la	k1gNnSc2
Société	Sociétý	k2eAgFnSc2d1
Internationale	Internationale	k1gFnSc2
de	de	k?
Musique	Musique	k1gInSc1
(	(	kIx(
<g/>
Section	Section	k1gInSc1
de	de	k?
Paris	Paris	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1905	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
240	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gMnSc2
-	-	kIx~
French	French	k1gMnSc1
composer	composer	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
britannica	britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BURROWS	BURROWS	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7391	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
60	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
88	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Jean	Jean	k1gMnSc1
Baptiste	Baptist	k1gMnSc5
Lully	Lull	k1gInPc4
Facts	Facts	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
biography	biographa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
yourdictionary	yourdictionara	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
French	French	k1gInSc1
overture	overtur	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MICHAEL	Michaela	k1gFnPc2
KENNEDY	KENNEDY	kA
and	and	k?
JOYCE	JOYCE	kA
BOURNE	BOURNE	kA
<g/>
.	.	kIx.
"	"	kIx"
<g/>
French	French	k1gInSc1
overture	overtur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
"	"	kIx"
The	The	k1gMnSc3
Concise	Concise	k1gFnSc2
Oxford	Oxford	k1gInSc4
Dictionary	Dictionara	k1gFnSc2
of	of	k?
Music	Music	k1gMnSc1
<g/>
.	.	kIx.
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Meter	metro	k1gNnPc2
Fluctuation	Fluctuation	k1gInSc1
in	in	k?
Lully	Lulla	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Recitative	recitativ	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
library	librar	k1gInPc1
<g/>
.	.	kIx.
<g/>
yale	yale	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
(	(	kIx(
<g/>
Independent	independent	k1gMnSc1
Study	stud	k1gInPc1
in	in	k?
Music	Musice	k1gFnPc2
History	Histor	k1gMnPc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Burrows	Burrowsa	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
88.1	88.1	k4
2	#num#	k4
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnPc1
Biography	Biographa	k1gFnSc2
by	by	kYmCp3nP
Bruce	Bruce	k1gFnPc1
Lundgren	Lundgrna	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
allmusic	allmusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Some	Some	k1gFnPc2
Dramatic	Dramatice	k1gFnPc2
Works	Works	kA
of	of	k?
Lully	Lulla	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
lib	líbit	k5eAaImRp2nS
<g/>
.	.	kIx.
<g/>
rochester	rochester	k1gInSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ecce	Ecce	k1gInSc1
homo	homo	k1gNnSc1
<g/>
:	:	kIx,
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
<g/>
:	:	kIx,
Libor	Libor	k1gMnSc1
Vykoupil	vykoupit	k5eAaPmAgMnS
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Lully	Lulla	k1gFnSc2
Isis	Isis	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
operabaroque	operabaroquat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
opéra	opéra	k1gMnSc1
du	du	k?
roi	roi	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Profile	profil	k1gInSc5
of	of	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
musiced	musiced	k1gMnSc1
<g/>
.	.	kIx.
<g/>
about	about	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Opera	opera	k1gFnSc1
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Lully	Lull	k1gInPc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Armide	Armid	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
George	George	k1gNnSc4
Frideric	Friderice	k1gInPc2
Handel	Handlo	k1gNnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
britannica	britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
LITTLE	LITTLE	kA
<g/>
,	,	kIx,
Meredith	Meredith	k1gMnSc1
<g/>
;	;	kIx,
JENNE	JENNE	kA
<g/>
,	,	kIx,
Natalie	Natalie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dance	Danka	k1gFnSc6
and	and	k?
the	the	k?
Music	Music	k1gMnSc1
of	of	k?
J.	J.	kA
<g/>
S.	S.	kA
Bach	Bach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bloomington	Bloomington	k1gInSc1
<g/>
,	,	kIx,
IN	IN	kA
<g/>
:	:	kIx,
Indiana	Indiana	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
253	#num#	k4
<g/>
-	-	kIx~
<g/>
21464	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Jean-Philippe	Jean-Philippat	k5eAaPmIp3nS
Rameau	Rameaus	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
britannica	britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Biography	Biograph	k1gInPc7
of	of	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
study	stud	k1gInPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
/	/	kIx~
<g/>
academy	academa	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
La	la	k1gNnSc4
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
Lully	Lulla	k1gFnSc2
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
21	#num#	k4
<g/>
–	–	k?
<g/>
22	#num#	k4
<g/>
;	;	kIx,
Le	Le	k1gMnSc1
Cerf	Cerf	k1gMnSc1
de	de	k?
la	la	k1gNnSc4
Viéville	Viéville	k1gNnPc2
<g/>
,	,	kIx,
Comparison	Comparisona	k1gFnPc2
de	de	k?
la	la	k1gNnSc1
musique	musiqu	k1gMnSc2
italienne	italiennout	k5eAaPmIp3nS,k5eAaImIp3nS
et	et	k?
de	de	k?
la	la	k1gNnSc2
musique	musique	k1gFnSc1
françoise	françoise	k1gFnSc1
<g/>
,	,	kIx,
Brussels	Brussels	k1gInSc1
<g/>
,	,	kIx,
1705	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
II	II	kA
<g/>
,	,	kIx,
s.	s.	k?
183.1	183.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
Skladatel	skladatel	k1gMnSc1
měsíce	měsíc	k1gInSc2
<g/>
:	:	kIx,
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
casopisharmonie	casopisharmonie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
<g/>
:	:	kIx,
Lucie	Lucie	k1gFnSc1
Maňourová	Maňourový	k2eAgFnSc1d1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
La	la	k1gNnSc1
Grande	grand	k1gMnSc5
Mademoiselle	Mademoisell	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
chateauversailles	chateauversailles	k1gInSc1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
LECONTE	LECONTE	kA
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnSc1
Ballet	Ballet	k1gInSc4
royal	royal	k1gInSc1
de	de	k?
la	la	k1gNnSc1
Nuit	Nuit	k1gMnSc1
<g/>
:	:	kIx,
l	l	kA
<g/>
'	'	kIx"
<g/>
Aurore	Auror	k1gInSc5
du	du	k?
Roi-soleil	Roi-soleila	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Le	Le	k1gMnSc1
Concert	Concert	k1gMnSc1
Royal	Royal	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
Nuit	Nuit	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Fondation	Fondation	k1gInSc1
Orange	Orang	k1gInSc2
/	/	kIx~
harmonia	harmonium	k1gNnSc2
mundi	mund	k1gMnPc1
s.	s.	k?
<g/>
a.	a.	k?
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
26	#num#	k4
<g/>
–	–	k?
<g/>
29	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
ROUSOVÁ	Rousová	k1gFnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Slunce	slunce	k1gNnSc2
v	v	k7c6
boji	boj	k1gInSc6
s	s	k7c7
temnotou	temnota	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portrét	portrét	k1gInSc1
tančícího	tančící	k2eAgMnSc2d1
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
..	..	k?
S.	S.	kA
12	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc1
a	a	k8xC
současnost	současnost	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
418	#num#	k4
<g/>
-	-	kIx~
<g/>
5129	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DAUCÉ	DAUCÉ	kA
<g/>
,	,	kIx,
Sébastien	Sébastien	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quelle	Quell	k1gMnSc5
meuveilleuse	meuveilleus	k1gMnSc5
aventure	aventur	k1gMnSc5
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Le	Le	k1gMnSc1
Concert	Concert	k1gMnSc1
Royal	Royal	k1gMnSc1
de	de	k?
la	la	k1gNnPc2
Nuit	Nuit	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Fondation	Fondation	k1gInSc1
Orange	Orang	k1gInSc2
/	/	kIx~
harmonia	harmonium	k1gNnSc2
mundi	mund	k1gMnPc1
s.	s.	k?
<g/>
a.	a.	k?
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
34	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Louis	louis	k1gInSc1
XIV	XIV	kA
and	and	k?
the	the	k?
French	French	k1gInSc1
Influence	influence	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
the-ballet	the-ballet	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Král	Král	k1gMnSc1
slunce	slunce	k1gNnSc2
miloval	milovat	k5eAaImAgMnS
balet	balet	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tanecnimagazin	tanecnimagazin	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc1
ou	ou	k0
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lulli	Lull	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
larousse	larousse	k6eAd1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lully	Lulla	k1gFnSc2
<g/>
:	:	kIx,
Ballet	Ballet	k1gInSc1
Music	Musice	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
Sun	Sun	kA
King	King	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
naxos	naxos	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Dvorský	dvorský	k2eAgInSc1d1
balet	balet	k1gInSc1
v	v	k7c6
období	období	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
pol.	pol.	k?
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
postupný	postupný	k2eAgInSc1d1
zánik	zánik	k1gInSc1
v	v	k7c6
době	doba	k1gFnSc6
vrcholného	vrcholný	k2eAgNnSc2d1
baroka	baroko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
amu	amu	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Les	les	k1gInSc1
Ballets	Ballets	k1gInSc1
de	de	k?
Cour	Cour	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ballet	Ballet	k1gInSc1
de	de	k?
cour	cour	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tanec	tanec	k1gInSc1
<g/>
.	.	kIx.
<g/>
tillwoman	tillwoman	k1gMnSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Isaac	Isaac	k1gFnSc1
de	de	k?
Benserade	Benserad	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
</s>
<s>
Lully	Lulla	k1gFnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
chateauversailles	chateauversailles	k1gInSc1
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Moliè	Moliè	k1gInSc5
and	and	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
holdenhurst	holdenhurst	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Les	les	k1gInSc1
fous	fous	k?
divertissants	divertissants	k1gInSc1
aneb	aneb	k?
Veselý	veselý	k2eAgInSc1d1
blázinec	blázinec	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
baroknipodvecery	baroknipodvecer	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Concerto	Concerta	k1gFnSc5
Comique	Comiquus	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
baroknipodvecery	baroknipodvecer	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc5
Corneille-Moliè	Corneille-Moliè	k1gMnSc5
Case-Thesis	Case-Thesis	k1gFnPc3
Statement	Statement	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
corneille-moliere	corneille-molirat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
DE	DE	k?
LA	la	k1gNnSc1
GORCE	GORCE	kA
<g/>
,	,	kIx,
Jérôme	Jérôm	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnPc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
912	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
60708	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
181	#num#	k4
<g/>
–	–	k?
<g/>
182	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Marc-Antoine	Marc-Antoin	k1gInSc5
Charpentier	Charpentier	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sinfinimusic	sinfinimusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Marc-Antoine	Marc-Antoin	k1gInSc5
Charpentier	Charpentier	k1gMnSc1
-	-	kIx~
French	French	k1gMnSc1
composer	composer	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
britannica	britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Velcí	velký	k2eAgMnPc1d1
tvůrci	tvůrce	k1gMnPc1
barokní	barokní	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
<g/>
:	:	kIx,
Jean	Jean	k1gMnSc1
Baptiste	Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tretivek	tretivek	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
<g/>
:	:	kIx,
JUDr.	JUDr.	kA
Irena	Irena	k1gFnSc1
Novotná	Novotná	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DE	DE	k?
LA	la	k1gNnPc2
GORCE	GORCE	kA
<g/>
,	,	kIx,
Jérôme	Jérôm	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
912	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
60708	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
177	#num#	k4
<g/>
–	–	k?
<g/>
181	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
JEAN	Jean	k1gMnSc1
BAPTISTE	BAPTISTE	kA
LULLY	LULLY	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
totem	totem	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
De	De	k?
La	la	k1gNnSc1
Gorce	Gorce	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
181	#num#	k4
<g/>
–	–	k?
<g/>
182	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
L	L	kA
<g/>
'	'	kIx"
<g/>
Académie	Académie	k1gFnSc2
Royale	Royala	k1gFnSc3
de	de	k?
Musique	Musique	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arsmagnalucis	arsmagnalucis	k1gInSc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GROUT	GROUT	kA
<g/>
,	,	kIx,
Donal	Donal	k1gMnSc1
Jay	Jay	k1gMnSc1
<g/>
;	;	kIx,
WEIGEL	WEIGEL	kA
WILLIAMS	WILLIAMS	kA
<g/>
,	,	kIx,
Hermine	Hermin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Short	Short	k1gInSc1
History	Histor	k1gInPc1
of	of	k?
Opera	opera	k1gFnSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
:	:	kIx,
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
1042	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
231119580	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
134	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
HOLDEN	HOLDEN	kA
<g/>
,	,	kIx,
Amanda	Amanda	k1gFnSc1
<g/>
;	;	kIx,
KENYON	KENYON	kA
<g/>
,	,	kIx,
Nicholas	Nicholas	k1gMnSc1
<g/>
;	;	kIx,
WALSH	WALSH	kA
<g/>
,	,	kIx,
Stephen	Stephen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Viking	Viking	k1gMnSc1
opera	opera	k1gFnSc1
guide	guide	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ann	Ann	k1gMnSc1
Arbor	Arbor	k1gMnSc1
<g/>
:	:	kIx,
Viking	Viking	k1gMnSc1
Adult	Adult	k1gMnSc1
<g/>
,	,	kIx,
1993	#num#	k4
<g/>
.	.	kIx.
1305	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
670812929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
180	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
klasika	klasika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
wz	wz	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
192	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
L	L	kA
<g/>
'	'	kIx"
<g/>
Académie	Académie	k1gFnSc2
Royale	Royala	k1gFnSc3
de	de	k?
Musique	Musique	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
arsmagnalucis	arsmagnalucis	k1gInSc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Poisoning	Poisoning	k1gInSc1
Lully	Lulla	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
bibliolore	bibliolor	k1gMnSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BLUCHE	BLUCHE	kA
<g/>
,	,	kIx,
François	François	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
časů	čas	k1gInPc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
294	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
675	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnSc2
Shrewd	Shrewd	k1gMnSc1
master	master	k1gMnSc1
of	of	k?
French	French	k1gMnSc1
Baroque	Baroquus	k1gMnSc5
theatre	theatr	k1gMnSc5
<g/>
,	,	kIx,
church	chur	k1gFnPc6
and	and	k?
ballet	ballet	k1gInSc1
music	music	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sinfinimusic	sinfinimusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Livret	Livret	k1gInSc1
Atys	Atys	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tragédie	tragédie	k1gFnSc1
en	en	k?
musique	musique	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rateyourmusic	rateyourmusic	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Music	Music	k1gMnSc1
-	-	kIx~
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
louis	louis	k1gInSc1
<g/>
.	.	kIx.
<g/>
xiv	xiv	k?
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
William	William	k1gInSc1
Christie	Christie	k1gFnSc2
conducts	conductsa	k1gFnPc2
Atys	Atys	k1gInSc1
at	at	k?
the	the	k?
Opéra	Opéra	k1gFnSc1
Comique	Comique	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
mezzo	mezza	k1gFnSc5
<g/>
.	.	kIx.
<g/>
tv	tv	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
215	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
210	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
628	#num#	k4
<g/>
–	–	k?
<g/>
629	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DE	DE	k?
LA	la	k1gNnPc2
GORCE	GORCE	kA
<g/>
,	,	kIx,
Jérôme	Jérôm	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
912	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
60708	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
612	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
De	De	k?
La	la	k1gNnSc1
Gorce	Gorce	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
218	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KAMINSKI	KAMINSKI	kA
<g/>
,	,	kIx,
Piotr	Piotr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mille	Mille	k1gFnSc1
et	et	k?
un	un	k?
opéras	opéras	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Librairie	Librairie	k1gFnSc1
Arthè	Arthè	k1gInSc5
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
1819	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
60017	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
–	–	k?
Isis	Isis	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
809	#num#	k4
<g/>
-	-	kIx~
<g/>
810	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
223	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Psyché	psyché	k1gFnSc1
(	(	kIx(
<g/>
1678	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnSc1
magazine	magazinout	k5eAaPmIp3nS
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
opéra	opér	k1gMnSc2
baroque	baroqu	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
638	#num#	k4
<g/>
–	–	k?
<g/>
639	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
WIGGINS	WIGGINS	kA
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lully	Lulla	k1gFnSc2
<g/>
’	’	k?
<g/>
s	s	k7c7
Proserpine	Proserpin	k1gInSc5
<g/>
:	:	kIx,
a	a	k8xC
model	model	k1gInSc4
for	forum	k1gNnPc2
the	the	k?
tragédie	tragédie	k1gFnSc2
lyrique	lyriqu	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
San	San	k1gFnSc1
Lorenzo	Lorenza	k1gFnSc5
de	de	k?
el	ela	k1gFnPc2
Escorial	Escorial	k1gMnSc1
<g/>
:	:	kIx,
Glossa	Glossa	k1gFnSc1
Music	Music	k1gMnSc1
/	/	kIx~
MusiContact	MusiContact	k1gMnSc1
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
633	#num#	k4
<g/>
,	,	kIx,
643	#num#	k4
<g/>
,	,	kIx,
646	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KAMINSKI	KAMINSKI	kA
<g/>
,	,	kIx,
Piotr	Piotr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mille	Mille	k1gFnSc1
et	et	k?
un	un	k?
opéras	opéras	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Librairie	Librairie	k1gFnSc1
Arthè	Arthè	k1gInSc5
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
1819	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
2	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
60017	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc1
–	–	k?
Proserpine	Proserpin	k1gMnSc5
<g/>
,	,	kIx,
s.	s.	k?
813	#num#	k4
<g/>
-	-	kIx~
<g/>
815	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
</s>
<s>
Persee	Perseus	k1gMnSc5
<g/>
,	,	kIx,
Tragédie	tragédie	k1gFnSc1
en	en	k?
musique	musique	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
Phaeton	Phaeton	k1gInSc1
Tragedie	tragedie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
Amadis	Amadis	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
Lully	Lulla	k1gFnPc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Airs	Airsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
French	French	k1gMnSc1
or	or	k?
Italian	Italian	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
Journal	Journal	k1gFnSc2
Article	Article	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
<g/>
:	:	kIx,
James	James	k1gMnSc1
R.	R.	kA
Anthony	Anthon	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
JStor	JStor	k1gInSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
Musical	musical	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
č.	č.	k?
128	#num#	k4
(	(	kIx(
<g/>
1729	#num#	k4
<g/>
,	,	kIx,
Březen	březen	k1gInSc1
1987	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s.	s.	k?
126-129	126-129	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
</s>
<s>
Roland	Roland	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
↑	↑	k?
Armide	Armid	k1gMnSc5
Plot	plot	k1gInSc4
Summary	Summara	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
unt	unt	k?
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Armide	Armid	k1gInSc5
<g/>
:	:	kIx,
Lully	Lulla	k1gFnPc1
<g/>
’	’	k?
<g/>
s	s	k7c7
ultimate	ultimat	k1gInSc5
triumph	triumph	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
naxos	naxos	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Achille	Achilles	k1gMnSc5
et	et	k?
Polixè	Polixè	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
operabaroque	operabaroquat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jean-Philippe	Jean-Philipp	k1gInSc5
Rameau	Rameaus	k1gInSc2
French	French	k1gMnSc1
composer	composer	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
britannica	britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Henry	Henry	k1gMnSc1
Purcell	Purcell	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
pages	pages	k1gInSc1
<g/>
.	.	kIx.
<g/>
cs	cs	k?
<g/>
.	.	kIx.
<g/>
wisc	wisc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
<g/>
/	/	kIx~
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
117	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
117	#num#	k4
<g/>
–	–	k?
<g/>
118	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Madeleine	Madeleine	k1gFnSc1
Lambert	Lambert	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
geni	geni	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tradition	Tradition	k1gInSc1
musicale	musical	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
saint-eustache	saint-eustache	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
118	#num#	k4
<g/>
–	–	k?
<g/>
119	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
De	De	k?
La	la	k1gNnSc1
Gorce	Gorce	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
121	#num#	k4
<g/>
,	,	kIx,
265	#num#	k4
<g/>
,	,	kIx,
368	#num#	k4
<g/>
–	–	k?
<g/>
369	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
JAL	jmout	k5eAaPmAgMnS
<g/>
,	,	kIx,
Auguste	August	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dictionnaire	Dictionnair	k1gInSc5
critique	critiqu	k1gInPc4
de	de	k?
biographie	biographie	k1gFnSc2
et	et	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
histoire	histoir	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Henri	Henri	k1gNnSc1
Plon	Plona	k1gFnPc2
<g/>
,	,	kIx,
1872	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
814	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
361	#num#	k4
<g/>
,	,	kIx,
368	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
,	,	kIx,
257	#num#	k4
<g/>
–	–	k?
<g/>
260.1	260.1	k4
2	#num#	k4
Lully	Lullo	k1gNnPc7
Jean-Baptiste	Jean-Baptist	k1gMnSc5
1632	#num#	k4
-	-	kIx~
1687	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
musicologie	musicologie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
284	#num#	k4
<g/>
–	–	k?
<g/>
285.1	285.1	k4
2	#num#	k4
Bluche	Bluche	k1gFnPc2
<g/>
,	,	kIx,
s.	s.	k?
12	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
84	#num#	k4
<g/>
–	–	k?
<g/>
267	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
J.B.	J.B.	k1gMnSc2
Lully	Lulla	k1gMnSc2
Biography	Biographa	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
notes	notes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
119	#num#	k4
<g/>
–	–	k?
<g/>
120	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
306	#num#	k4
<g/>
–	–	k?
<g/>
315	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
de	de	k?
Lully	Lull	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
gayinfluence	gayinfluence	k1gFnSc1
<g/>
.	.	kIx.
<g/>
blogspot	blogspot	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Bisexuality	bisexualita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
rictornorton	rictornorton	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
348	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
18	#num#	k4
novembre	novembr	k1gInSc5
1686	#num#	k4
:	:	kIx,
l	l	kA
<g/>
’	’	k?
<g/>
opération	opération	k1gInSc1
dela	delus	k1gMnSc2
fistule	fistule	k1gFnSc2
est	est	k?
faite	faite	k5eAaPmIp2nP
à	à	k?
Louis	louis	k1gInSc1
XIV	XIV	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
france-pittoresque	france-pittoresque	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
342	#num#	k4
<g/>
–	–	k?
<g/>
343	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
342	#num#	k4
<g/>
,	,	kIx,
345	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
345	#num#	k4
<g/>
–	–	k?
<g/>
347	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
347	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gMnSc2
Timeless	Timelessa	k1gFnPc2
Music	Music	k1gMnSc1
–	–	k?
Controversial	Controversial	k1gInSc1
Life	Life	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
florentintise	florentintise	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
349	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
350	#num#	k4
<g/>
–	–	k?
<g/>
351	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
la	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
354.1	354.1	k4
2	#num#	k4
Le	Le	k1gMnSc2
cénotaphe	cénotaph	k1gMnSc2
de	de	k?
Lully	Lulla	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
notredamedesvictoires	notredamedesvictoires	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
350	#num#	k4
<g/>
,	,	kIx,
354	#num#	k4
<g/>
–	–	k?
<g/>
355	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
364	#num#	k4
<g/>
–	–	k?
<g/>
365	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
newadvent	newadvent	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
282	#num#	k4
<g/>
–	–	k?
<g/>
289	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
360	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GAMMOND	GAMMOND	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velcí	velký	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svojtka	Svojtka	k1gFnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7237	#num#	k4
<g/>
-	-	kIx~
<g/>
590	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
101	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lully	Lull	k1gInPc1
Atys	Atys	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
operabaroque	operabaroquat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Learning	Learning	k1gInSc1
To	ten	k3xDgNnSc1
Love	lov	k1gInSc5
Lully	Lullo	k1gNnPc7
<g/>
:	:	kIx,
'	'	kIx"
<g/>
The	The	k1gFnSc1
Grand	grand	k1gMnSc1
Motets	Motets	k1gInSc1
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
npr	npr	k?
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Les	les	k1gInSc1
Motets	Motets	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
sitelully	sitelulla	k1gFnPc1
<g/>
.	.	kIx.
<g/>
free	freat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc4
Compositeur	Compositeur	k1gMnSc1
français	français	k1gFnSc2
(	(	kIx(
<g/>
Florence	Florenc	k1gFnSc2
<g/>
,	,	kIx,
1632	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
<g/>
,	,	kIx,
1687	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
francemusique	francemusiquat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Opera	opera	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
–	–	k?
Jean	Jean	k1gMnSc1
Baptiste	Baptist	k1gMnSc5
Lully	Lulla	k1gMnSc2
(	(	kIx(
<g/>
1632	#num#	k4
<g/>
-	-	kIx~
<g/>
1687	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
dailylife	dailylif	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
K	k	k7c3
celému	celý	k2eAgInSc3d1
odstavci	odstavec	k1gInSc3
De	De	k?
La	la	k1gNnSc3
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
s.	s.	k?
376	#num#	k4
<g/>
–	–	k?
<g/>
381	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
851	#num#	k4
<g/>
–	–	k?
<g/>
855.1	855.1	k4
2	#num#	k4
SCHNEIDER	Schneider	k1gMnSc1
<g/>
,	,	kIx,
Herbert	Herbert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Catalogue	Catalogu	k1gInSc2
des	des	k1gNnSc7
œ	œ	k1gMnSc1
de	de	k?
Lully	Lulla	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SADLER	SADLER	kA
<g/>
,	,	kIx,
Graham	graham	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc1
<g/>
:	:	kIx,
Armide	Armid	k1gInSc5
<g/>
:	:	kIx,
tragédie	tragédie	k1gFnSc2
en	en	k?
musique	musiqu	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edited	Edited	k1gInSc1
by	by	kYmCp3nS
Lois	Lois	k1gInSc4
Rosow	Rosow	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edition	Edition	k1gInSc1
of	of	k?
the	the	k?
livret	livret	k1gInSc4
<g/>
:	:	kIx,
Jean-Noël	Jean-Noël	k1gInSc4
Laurenti	Laurent	k1gMnPc1
<g/>
.	.	kIx.
Œ	Œ	k1gMnSc1
complè	complè	k1gMnSc1
<g/>
,	,	kIx,
Série	série	k1gFnSc1
III	III	kA
<g/>
,	,	kIx,
Opéras	Opéras	k1gInSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hildesheim	Hildesheim	k1gInSc1
<g/>
:	:	kIx,
Georg	Georg	k1gInSc1
Olms	Olmsa	k1gFnPc2
Verlag	Verlaga	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
xxxi	xxxi	k1gNnSc1
<g/>
,	,	kIx,
333	#num#	k4
pp	pp	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3	#num#	k4
<g/>
-	-	kIx~
<g/>
487	#num#	k4
<g/>
-	-	kIx~
<g/>
12524	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
€	€	k?
<g/>
315	#num#	k4
<g/>
.	.	kIx.
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Review	Review	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Seventeenth-Century	Seventeenth-Centura	k1gFnSc2
Music	Music	k1gMnSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
13	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1089	#num#	k4
<g/>
-	-	kIx~
<g/>
747	#num#	k4
<g/>
X.	X.	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jean	Jean	k1gMnSc1
Baptiste	Baptist	k1gMnSc5
Lully	Lulla	k1gMnSc2
-	-	kIx~
Œ	Œ	k1gMnSc1
Complè	Complè	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hildesheim	Hildesheim	k1gMnSc1
<g/>
,	,	kIx,
Zürich	Zürich	k1gMnSc1
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Olms	Olms	k1gInSc1
–	–	k?
Weidmann	Weidmann	k1gInSc1
(	(	kIx(
<g/>
Georg	Georg	k1gInSc1
Olms	Olms	k1gInSc1
Verlag	Verlag	k1gInSc4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
DE	DE	k?
MONTALEMBERT	MONTALEMBERT	kA
<g/>
,	,	kIx,
Eugè	Eugè	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Guide	Guid	k1gInSc5
des	des	k1gNnSc7
genres	genres	k1gInSc1
de	de	k?
la	la	k1gNnSc7
musique	musiqu	k1gFnSc2
occidentale	occidental	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paris	Paris	k1gMnSc1
<g/>
:	:	kIx,
Fayard	Fayard	k1gMnSc1
/	/	kIx~
Henri	Henr	k1gMnSc5
Lemoine	Lemoin	k1gMnSc5
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
1310	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
213	#num#	k4
<g/>
-	-	kIx~
<g/>
63450	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
748	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
730	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
719	#num#	k4
<g/>
,	,	kIx,
732	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
737.1	737.1	k4
2	#num#	k4
De	De	k?
La	la	k1gNnSc1
Gorce	Gorce	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
736	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
736	#num#	k4
<g/>
–	–	k?
<g/>
737	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
244	#num#	k4
<g/>
–	–	k?
<g/>
246	#num#	k4
<g/>
,	,	kIx,
736	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
756	#num#	k4
<g/>
–	–	k?
<g/>
759	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
734	#num#	k4
<g/>
–	–	k?
<g/>
735	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
739	#num#	k4
<g/>
–	–	k?
<g/>
740	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
739	#num#	k4
<g/>
,	,	kIx,
741	#num#	k4
<g/>
–	–	k?
<g/>
742	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
De	De	k?
La	la	k1gNnSc1
Gorce	Gorce	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
744	#num#	k4
<g/>
–	–	k?
<g/>
748	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
743	#num#	k4
<g/>
,	,	kIx,
748	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
762	#num#	k4
<g/>
–	–	k?
<g/>
763	#num#	k4
<g/>
,	,	kIx,
768	#num#	k4
<g/>
–	–	k?
<g/>
769	#num#	k4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
749	#num#	k4
<g/>
–	–	k?
<g/>
751	#num#	k4
<g/>
,	,	kIx,
771	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
721	#num#	k4
<g/>
,	,	kIx,
854	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
721	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
722	#num#	k4
<g/>
–	–	k?
<g/>
723.1	723.1	k4
2	#num#	k4
De	De	k?
La	la	k1gNnSc1
Gorce	Gorce	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
723	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
724	#num#	k4
<g/>
–	–	k?
<g/>
725	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
726	#num#	k4
<g/>
,	,	kIx,
729	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
De	De	k?
La	la	k1gNnSc7
Gorce	Gorce	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
854	#num#	k4
<g/>
–	–	k?
<g/>
855	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnSc1
roi	roi	k?
danse	danse	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
imdb	imdb	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Král	Král	k1gMnSc1
Slunce	slunce	k1gNnSc2
tančí	tančit	k5eAaImIp3nS
<g/>
,	,	kIx,
divák	divák	k1gMnSc1
jen	jen	k9
přihlíží	přihlížet	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Ve	v	k7c6
filmu	film	k1gInSc6
Král	Král	k1gMnSc1
tančí	tančit	k5eAaImIp3nS
hraje	hrát	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
hudba	hudba	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
kultura	kultura	k1gFnSc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
BEAUSSANT	BEAUSSANT	kA
<g/>
,	,	kIx,
Philippe	Philipp	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lully	Lullo	k1gNnPc7
ou	ou	k0
Le	Le	k1gFnSc7
musicien	musiciit	k5eAaPmNgInS,k5eAaBmNgInS,k5eAaImNgInS
du	du	k?
Soleil	Soleil	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
<g/>
:	:	kIx,
Theatre	Theatr	k1gInSc5
des	des	k1gNnPc1
Champs-Elysees	Champs-Elysees	k1gMnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
893	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2070724789	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BLUCHE	BLUCHE	kA
<g/>
,	,	kIx,
François	François	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
časů	čas	k1gInPc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Argo	Argo	k1gNnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7203	#num#	k4
<g/>
-	-	kIx~
<g/>
675	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
BURROWS	BURROWS	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
.	.	kIx.
528	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7391	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
60	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
DE	DE	k?
LA	la	k1gNnSc1
GORCE	GORCE	kA
<g/>
,	,	kIx,
Jérôme	Jérôm	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paříž	Paříž	k1gFnSc1
<g/>
:	:	kIx,
Fayard	Fayard	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
910	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
2213607085	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
GAMMOND	GAMMOND	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velcí	velký	k2eAgMnPc1d1
skladatelé	skladatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svojtka	Svojtka	k1gFnSc1
&	&	k?
Co	co	k9
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
208	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7237	#num#	k4
<g/>
-	-	kIx~
<g/>
590	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
opery	opera	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gInPc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jean-Baptiste	Jean-Baptist	k1gInSc5
Lully	Lulla	k1gFnPc5
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lull	k1gMnPc7
na	na	k7c6
projektu	projekt	k1gInSc6
Musopen	Musopen	k2eAgMnSc1d1
</s>
<s>
Jean-Baptiste	Jean-Baptist	k1gMnSc5
Lully	Lulla	k1gMnSc2
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990005226	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118575287	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2099	#num#	k4
1997	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79072787	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
13099537	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79072787	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
|	|	kIx~
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
