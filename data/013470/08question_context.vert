<s desamb="1">
Protože	protože	k8xS
odmítal	odmítat	k5eAaImAgInS
včasnou	včasný	k2eAgFnSc4d1
amputaci	amputace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
byla	být	k5eAaImAgFnS
nutná	nutný	k2eAgFnSc1d1
pro	pro	k7c4
zastavení	zastavení	k1gNnSc4
gangrény	gangréna	k1gFnSc2
<g/>
,	,	kIx,
zánět	zánět	k1gInSc1
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozdní	pozdní	k2eAgFnSc1d1
amputace	amputace	k1gFnSc1
nohy	noha	k1gFnSc2
již	již	k6eAd1
nepomohla	pomoct	k5eNaPmAp3gFnS
a	a	k8xC
Lully	Lulla	k1gFnPc1
o	o	k7c4
dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Téměř	téměř	k6eAd1
tři	tři	k4xCgNnPc4
staletí	staletí	k1gNnPc4
byl	být	k5eAaImAgInS
Lully	Lully	k1gMnSc1
znám	známý	k2eAgMnSc1d1
spíše	spíše	k9
kvůli	kvůli	k7c3
neobvyklým	obvyklý	k2eNgFnPc3d1
okolnostem	okolnost	k1gFnPc3
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>