<p>
<s>
Jmelí	jmelí	k1gNnSc1	jmelí
(	(	kIx(	(
<g/>
Viscum	Viscum	k1gInSc1	Viscum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
poloparazitických	poloparazitický	k2eAgFnPc2d1	poloparazitická
keříkovitých	keříkovitý	k2eAgFnPc2d1	keříkovitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
druh	druh	k1gInSc4	druh
(	(	kIx(	(
<g/>
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jmelí	jmelí	k1gNnSc1	jmelí
bílé	bílé	k1gNnSc1	bílé
(	(	kIx(	(
<g/>
Viscum	Viscum	k1gInSc1	Viscum
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
však	však	k9	však
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
asi	asi	k9	asi
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
druhy	druh	k1gInPc1	druh
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
jednodomé	jednodomý	k2eAgFnPc1d1	jednodomá
nebo	nebo	k8xC	nebo
dvoudomé	dvoudomý	k2eAgFnPc1d1	dvoudomá
<g/>
.	.	kIx.	.
</s>
<s>
Větve	větev	k1gFnPc1	větev
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
stonku	stonek	k1gInSc6	stonek
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
vstřícně	vstřícně	k6eAd1	vstřícně
<g/>
,	,	kIx,	,
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
nebo	nebo	k8xC	nebo
v	v	k7c6	v
přeslenech	přeslen	k1gInPc6	přeslen
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
zakrnělé	zakrnělý	k2eAgInPc1d1	zakrnělý
na	na	k7c4	na
pouhé	pouhý	k2eAgFnPc4d1	pouhá
šupiny	šupina	k1gFnPc4	šupina
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
plně	plně	k6eAd1	plně
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
<g/>
.	.	kIx.	.
</s>
<s>
Vidličnaté	vidličnatý	k2eAgNnSc1d1	vidličnatý
květenství	květenství	k1gNnSc1	květenství
je	být	k5eAaImIp3nS	být
úžlabní	úžlabní	k2eAgNnSc1d1	úžlabní
nebo	nebo	k8xC	nebo
koncové	koncový	k2eAgNnSc1d1	koncové
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
1-7	[number]	k4	1-7
květů	květ	k1gInPc2	květ
<g/>
.	.	kIx.	.
</s>
<s>
Květní	květní	k2eAgFnPc1d1	květní
stopky	stopka	k1gFnPc1	stopka
jsou	být	k5eAaImIp3nP	být
zakrnělé	zakrnělý	k2eAgFnPc1d1	zakrnělá
nebo	nebo	k8xC	nebo
chybí	chybit	k5eAaPmIp3nP	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
bývají	bývat	k5eAaImIp3nP	bývat
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
<g/>
,	,	kIx,	,
oválné	oválný	k2eAgInPc1d1	oválný
či	či	k8xC	či
elipsoidní	elipsoidní	k2eAgInPc1d1	elipsoidní
<g/>
,	,	kIx,	,
oplodí	oplodí	k1gNnSc1	oplodí
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hladké	hladký	k2eAgNnSc1d1	hladké
či	či	k8xC	či
s	s	k7c7	s
mírnými	mírný	k2eAgInPc7d1	mírný
výstupky	výstupek	k1gInPc7	výstupek
<g/>
,	,	kIx,	,
nepukavé	pukavý	k2eNgInPc1d1	nepukavý
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
rozšiřované	rozšiřovaný	k2eAgMnPc4d1	rozšiřovaný
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mytologie	mytologie	k1gFnSc2	mytologie
==	==	k?	==
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
fascinováni	fascinovat	k5eAaBmNgMnP	fascinovat
touto	tento	k3xDgFnSc7	tento
rostlinou	rostlina	k1gFnSc7	rostlina
již	již	k6eAd1	již
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
<g/>
.	.	kIx.	.
</s>
<s>
Řecký	řecký	k2eAgMnSc1d1	řecký
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
vědec	vědec	k1gMnSc1	vědec
Theofrastos	Theofrastos	k1gMnSc1	Theofrastos
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
některé	některý	k3yIgInPc4	některý
základní	základní	k2eAgInPc4d1	základní
rysy	rys	k1gInPc4	rys
této	tento	k3xDgFnSc2	tento
rostliny	rostlina	k1gFnSc2	rostlina
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Keltskými	keltský	k2eAgMnPc7d1	keltský
druidy	druid	k1gMnPc7	druid
bylo	být	k5eAaImAgNnS	být
jmelí	jmelí	k1gNnSc1	jmelí
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
posvátné	posvátný	k2eAgNnSc4d1	posvátné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
drželo	držet	k5eAaImAgNnS	držet
svou	svůj	k3xOyFgFnSc4	svůj
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
každou	každý	k3xTgFnSc4	každý
chorobu	choroba	k1gFnSc4	choroba
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
jako	jako	k9	jako
protijed	protijed	k1gInSc4	protijed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antice	antika	k1gFnSc6	antika
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jmelí	jmelí	k1gNnSc1	jmelí
i	i	k9	i
jako	jako	k9	jako
kadidlo	kadidlo	k1gNnSc1	kadidlo
a	a	k8xC	a
vykuřovaly	vykuřovat	k5eAaImAgFnP	vykuřovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
jmelí	jmelí	k1gNnSc1	jmelí
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
plodnost	plodnost	k1gFnSc4	plodnost
a	a	k8xC	a
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgInPc1d1	populární
polibky	polibek	k1gInPc1	polibek
pod	pod	k7c7	pod
vánočním	vánoční	k2eAgNnSc7d1	vánoční
jmelím	jmelí	k1gNnSc7	jmelí
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
dotyčným	dotyčný	k2eAgFnPc3d1	dotyčná
přinést	přinést	k5eAaPmF	přinést
štěstí	štěstí	k1gNnSc4	štěstí
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ZÍBRT	ZÍBRT	kA	ZÍBRT
<g/>
,	,	kIx,	,
Čeněk	Čeněk	k1gMnSc1	Čeněk
<g/>
.	.	kIx.	.
</s>
<s>
Hoj	hojit	k5eAaImRp2nS	hojit
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
štědrý	štědrý	k2eAgInSc1d1	štědrý
večere	večer	k1gInSc5	večer
:	:	kIx,	:
Od	od	k7c2	od
vánoc	vánoce	k1gFnPc2	vánoce
koledou	koleda	k1gFnSc7	koleda
do	do	k7c2	do
Nového	Nového	k2eAgInSc2d1	Nového
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
-	-	kIx~	-
kapitola	kapitola	k1gFnSc1	kapitola
Jmelí	jmelí	k1gNnSc2	jmelí
<g/>
,	,	kIx,	,
s.	s.	k?	s.
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jmelí	jmelí	k1gNnSc2	jmelí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jmelí	jmelí	k1gNnSc2	jmelí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
