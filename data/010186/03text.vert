<p>
<s>
Pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
je	být	k5eAaImIp3nS	být
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
mezi	mezi	k7c7	mezi
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Svazem	svaz	k1gInSc7	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
paktu	pakt	k1gInSc2	pakt
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
příjmení	příjmení	k1gNnSc2	příjmení
dvou	dva	k4xCgMnPc2	dva
vyjednavačů	vyjednavač	k1gMnPc2	vyjednavač
<g/>
:	:	kIx,	:
Hitlerova	Hitlerův	k2eAgMnSc2d1	Hitlerův
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Joachima	Joachima	k1gFnSc1	Joachima
von	von	k1gInSc1	von
Ribbentropa	Ribbentropa	k1gFnSc1	Ribbentropa
a	a	k8xC	a
Vjačeslava	Vjačeslava	k1gFnSc1	Vjačeslava
Molotova	Molotův	k2eAgFnSc1d1	Molotova
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
lidovým	lidový	k2eAgMnSc7d1	lidový
komisařem	komisař	k1gMnSc7	komisař
(	(	kIx(	(
<g/>
ministrem	ministr	k1gMnSc7	ministr
<g/>
)	)	kIx)	)
zahraničí	zahraničí	k1gNnSc6	zahraničí
v	v	k7c6	v
stalinském	stalinský	k2eAgInSc6d1	stalinský
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepoužijí	použít	k5eNaPmIp3nP	použít
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
sílu	síla	k1gFnSc4	síla
proti	proti	k7c3	proti
druhé	druhý	k4xOgFnSc3	druhý
straně	strana	k1gFnSc3	strana
a	a	k8xC	a
nespojí	spojit	k5eNaPmIp3nS	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
jejími	její	k3xOp3gMnPc7	její
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
znamenala	znamenat	k5eAaImAgFnS	znamenat
zásadní	zásadní	k2eAgInSc4d1	zásadní
obrat	obrat	k1gInSc4	obrat
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
obou	dva	k4xCgInPc6	dva
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
dodržována	dodržován	k2eAgFnSc1d1	dodržována
až	až	k6eAd1	až
do	do	k7c2	do
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nacistické	nacistický	k2eAgNnSc1d1	nacistické
Německo	Německo	k1gNnSc1	Německo
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
(	(	kIx(	(
<g/>
operace	operace	k1gFnSc1	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uzavření	uzavření	k1gNnSc1	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
předcházelo	předcházet	k5eAaImAgNnS	předcházet
jednostranné	jednostranný	k2eAgNnSc1d1	jednostranné
německé	německý	k2eAgNnSc1d1	německé
zrušení	zrušení	k1gNnSc1	zrušení
německo-polského	německoolský	k2eAgInSc2d1	německo-polský
paktu	pakt	k1gInSc2	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
polsko-sovětský	polskoovětský	k2eAgInSc1d1	polsko-sovětský
pakt	pakt	k1gInSc1	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
však	však	k9	však
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
v	v	k7c6	v
účinnosti	účinnost	k1gFnSc6	účinnost
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
jej	on	k3xPp3gInSc4	on
jednostranně	jednostranně	k6eAd1	jednostranně
porušil	porušit	k5eAaPmAgMnS	porušit
až	až	k9	až
svým	svůj	k3xOyFgInSc7	svůj
vpádem	vpád	k1gInSc7	vpád
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedenáct	jedenáct	k4xCc1	jedenáct
dní	den	k1gInPc2	den
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
invazi	invaze	k1gFnSc6	invaze
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
týmiž	týž	k3xTgFnPc7	týž
smluvními	smluvní	k2eAgFnPc7d1	smluvní
stranami	strana	k1gFnPc7	strana
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
přátelství	přátelství	k1gNnSc6	přátelství
<g/>
,	,	kIx,	,
spolupráci	spolupráce	k1gFnSc6	spolupráce
a	a	k8xC	a
vymezení	vymezení	k1gNnSc6	vymezení
demarkační	demarkační	k2eAgFnSc2d1	demarkační
linie	linie	k1gFnSc2	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnPc1	území
připojená	připojený	k2eAgNnPc1d1	připojené
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
a	a	k8xC	a
1940	[number]	k4	1940
(	(	kIx(	(
<g/>
Severní	severní	k2eAgFnSc1d1	severní
Bukovina	Bukovina	k1gFnSc1	Bukovina
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc1d1	jižní
Besarábie	Besarábie	k1gFnSc1	Besarábie
a	a	k8xC	a
Herca	Herca	k?	Herca
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
součástí	součást	k1gFnSc7	součást
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Polsku	Polska	k1gFnSc4	Polska
byla	být	k5eAaImAgFnS	být
vrácena	vrácen	k2eAgFnSc1d1	vrácena
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Białystoku	Białystok	k1gInSc2	Białystok
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
Haliče	Halič	k1gFnSc2	Halič
východně	východně	k6eAd1	východně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
San	San	k1gFnSc2	San
kolem	kolem	k7c2	kolem
Přemyšlu	Přemyšl	k1gInSc2	Přemyšl
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
ostatní	ostatní	k2eAgNnPc1d1	ostatní
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
ta	ten	k3xDgNnPc1	ten
anektovaná	anektovaný	k2eAgNnPc1d1	anektované
Finsku	Finsko	k1gNnSc6	Finsko
(	(	kIx(	(
<g/>
Karélie	Karélie	k1gFnSc1	Karélie
<g/>
,	,	kIx,	,
Petsamo	Petsama	k1gFnSc5	Petsama
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Estonsku	Estonsko	k1gNnSc6	Estonsko
(	(	kIx(	(
<g/>
Ingrie	Ingrie	k1gFnSc1	Ingrie
a	a	k8xC	a
Petseri	Petseri	k1gNnSc1	Petseri
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lotyšsku	Lotyšsko	k1gNnSc3	Lotyšsko
(	(	kIx(	(
<g/>
Abrene	Abren	k1gMnSc5	Abren
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
součástí	součást	k1gFnSc7	součást
Ruské	ruský	k2eAgFnSc2d1	ruská
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existence	existence	k1gFnSc1	existence
tajného	tajný	k2eAgInSc2d1	tajný
dodatku	dodatek	k1gInSc2	dodatek
byla	být	k5eAaImAgFnS	být
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
vládou	vláda	k1gFnSc7	vláda
vehementně	vehementně	k6eAd1	vehementně
popírána	popírán	k2eAgFnSc1d1	popírána
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Putin	putin	k2eAgMnSc1d1	putin
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sice	sice	k8xC	sice
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pakt	pakt	k1gInSc4	pakt
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
nemorální	morální	k2eNgMnSc1d1	nemorální
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
zároveň	zároveň	k6eAd1	zároveň
obhajuje	obhajovat	k5eAaImIp3nS	obhajovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nutné	nutný	k2eAgNnSc1d1	nutné
zlo	zlo	k1gNnSc1	zlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
von	von	k1gInSc1	von
Ribbentrop	Ribbentrop	k1gInSc4	Ribbentrop
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
za	za	k7c4	za
válečné	válečný	k2eAgInPc4d1	válečný
zločiny	zločin	k1gInPc4	zločin
a	a	k8xC	a
popraven	popraven	k2eAgInSc4d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Molotov	Molotov	k1gInSc1	Molotov
zemřel	zemřít	k5eAaPmAgInS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
96	[number]	k4	96
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sovětsko-německé	sovětskoěmecký	k2eAgNnSc1d1	sovětsko-německý
sbližování	sbližování	k1gNnSc1	sbližování
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětsko-německá	sovětskoěmecký	k2eAgFnSc1d1	sovětsko-německý
spolupráce	spolupráce	k1gFnSc1	spolupráce
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Hitlera	Hitler	k1gMnSc2	Hitler
===	===	k?	===
</s>
</p>
<p>
<s>
Mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
ve	v	k7c6	v
Versailles	Versailles	k1gFnPc6	Versailles
vítěznými	vítězný	k2eAgFnPc7d1	vítězná
mocnostmi	mocnost	k1gFnPc7	mocnost
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
odsunula	odsunout	k5eAaPmAgFnS	odsunout
poražené	poražený	k2eAgNnSc4d1	poražené
Německo	Německo	k1gNnSc4	Německo
a	a	k8xC	a
bolševické	bolševický	k2eAgNnSc4d1	bolševické
Rusko	Rusko	k1gNnSc4	Rusko
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
evropského	evropský	k2eAgNnSc2d1	Evropské
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
uzavřely	uzavřít	k5eAaPmAgInP	uzavřít
v	v	k7c6	v
Rapallu	Rapall	k1gInSc6	Rapall
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
styků	styk	k1gInPc2	styk
<g/>
,	,	kIx,	,
zřekly	zřeknout	k5eAaPmAgFnP	zřeknout
se	se	k3xPyFc4	se
vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
náhrady	náhrada	k1gFnPc1	náhrada
válečných	válečný	k2eAgFnPc2d1	válečná
škod	škoda	k1gFnPc2	škoda
a	a	k8xC	a
zahájily	zahájit	k5eAaPmAgInP	zahájit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
získali	získat	k5eAaPmAgMnP	získat
moderní	moderní	k2eAgFnPc4d1	moderní
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc4	stroj
i	i	k8xC	i
těžké	těžký	k2eAgFnPc4d1	těžká
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
cvičit	cvičit	k5eAaImF	cvičit
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
tankové	tankový	k2eAgFnPc1d1	tanková
a	a	k8xC	a
dělostřelecké	dělostřelecký	k2eAgFnPc1d1	dělostřelecká
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
SSSR	SSSR	kA	SSSR
ani	ani	k8xC	ani
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
účastníkem	účastník	k1gMnSc7	účastník
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
nebyl	být	k5eNaImAgInS	být
<g/>
)	)	kIx)	)
porušením	porušení	k1gNnSc7	porušení
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
smlouvu	smlouva	k1gFnSc4	smlouva
z	z	k7c2	z
Rapallu	Rapall	k1gInSc2	Rapall
navázala	navázat	k5eAaPmAgFnS	navázat
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvala	trvat	k5eAaImAgFnS	trvat
ještě	ještě	k6eAd1	ještě
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebyla	být	k5eNaImAgFnS	být
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
smlouva	smlouva	k1gFnSc1	smlouva
dále	daleko	k6eAd2	daleko
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
rozvinutá	rozvinutý	k2eAgFnSc1d1	rozvinutá
spolupráce	spolupráce	k1gFnSc1	spolupráce
postupně	postupně	k6eAd1	postupně
značně	značně	k6eAd1	značně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zvyšujícího	zvyšující	k2eAgMnSc2d1	zvyšující
se	se	k3xPyFc4	se
izolacionismu	izolacionismus	k1gInSc2	izolacionismus
stalinského	stalinský	k2eAgInSc2d1	stalinský
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
současného	současný	k2eAgNnSc2d1	současné
zeslábnutí	zeslábnutí	k1gNnSc2	zeslábnutí
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kontroly	kontrola	k1gFnSc2	kontrola
dodržování	dodržování	k1gNnSc4	dodržování
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
snížilo	snížit	k5eAaPmAgNnS	snížit
německou	německý	k2eAgFnSc4d1	německá
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
importu	import	k1gInSc6	import
z	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
sovětský	sovětský	k2eAgInSc1d1	sovětský
import	import	k1gInSc1	import
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
223	[number]	k4	223
milionů	milion	k4xCgInPc2	milion
Říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
činil	činit	k5eAaImAgInS	činit
433	[number]	k4	433
miliónů	milión	k4xCgInPc2	milión
Říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
začala	začít	k5eAaPmAgFnS	začít
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
nejen	nejen	k6eAd1	nejen
západní	západní	k2eAgFnPc1d1	západní
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Nastalo	nastat	k5eAaPmAgNnS	nastat
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
sbližování	sbližování	k1gNnSc1	sbližování
mezi	mezi	k7c7	mezi
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ovšem	ovšem	k9	ovšem
zásadně	zásadně	k6eAd1	zásadně
narušilo	narušit	k5eAaPmAgNnS	narušit
podepsání	podepsání	k1gNnSc4	podepsání
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nebyl	být	k5eNaImAgInS	být
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
přizván	přizvat	k5eAaPmNgMnS	přizvat
(	(	kIx(	(
<g/>
nebyl	být	k5eNaImAgMnS	být
vítěznou	vítězný	k2eAgFnSc7d1	vítězná
mocností	mocnost	k1gFnSc7	mocnost
oprávněnou	oprávněný	k2eAgFnSc7d1	oprávněná
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
86	[number]	k4	86
Versailleské	versailleský	k2eAgFnSc2d1	Versailleská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
stanovit	stanovit	k5eAaPmF	stanovit
případná	případný	k2eAgNnPc4d1	případné
opatření	opatření	k1gNnPc4	opatření
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
menšin	menšina	k1gFnPc2	menšina
na	na	k7c6	na
československém	československý	k2eAgNnSc6d1	Československé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgMnSc1d1	sovětský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Maxim	Maxim	k1gMnSc1	Maxim
Maximovič	Maximovič	k1gMnSc1	Maximovič
Litvinov	Litvinov	k1gInSc4	Litvinov
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
a	a	k8xC	a
1938	[number]	k4	1938
opakovaně	opakovaně	k6eAd1	opakovaně
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g />
.	.	kIx.	.
</s>
<s>
války	válka	k1gFnPc1	válka
i	i	k9	i
přes	přes	k7c4	přes
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
neposkytne	poskytnout	k5eNaPmIp3nS	poskytnout
Československu	Československo	k1gNnSc6	Československo
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
takovou	takový	k3xDgFnSc4	takový
operaci	operace	k1gFnSc4	operace
připravena	připraven	k2eAgFnSc1d1	připravena
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
Hitlerova	Hitlerův	k2eAgFnSc1d1	Hitlerova
zuřivá	zuřivý	k2eAgFnSc1d1	zuřivá
antisovětská	antisovětský	k2eAgFnSc1d1	antisovětská
rétorika	rétorika	k1gFnSc1	rétorika
byla	být	k5eAaImAgFnS	být
dalším	další	k2eAgMnSc6d1	další
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
sovětská	sovětský	k2eAgFnSc1d1	sovětská
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
konferenci	konference	k1gFnSc6	konference
bude	být	k5eAaImBp3nS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
a	a	k8xC	a
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
obávat	obávat	k5eAaImF	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
západní	západní	k2eAgFnPc1d1	západní
země	zem	k1gFnPc1	zem
připravují	připravovat	k5eAaImIp3nP	připravovat
dohodu	dohoda	k1gFnSc4	dohoda
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko-německá	polskoěmecký	k2eAgNnPc1d1	polsko-německý
jednání	jednání	k1gNnPc1	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
mělo	mít	k5eAaImAgNnS	mít
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
z	z	k7c2	z
ledna	leden	k1gInSc2	leden
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Joachim	Joachima	k1gFnPc2	Joachima
von	von	k1gInSc1	von
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
velvyslancem	velvyslanec	k1gMnSc7	velvyslanec
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
Józefem	Józef	k1gInSc7	Józef
Lipskim	Lipskima	k1gFnPc2	Lipskima
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
úplné	úplný	k2eAgNnSc4d1	úplné
vyřešení	vyřešení	k1gNnSc4	vyřešení
všech	všecek	k3xTgFnPc2	všecek
sporných	sporný	k2eAgFnPc2d1	sporná
otázek	otázka	k1gFnPc2	otázka
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
obnovení	obnovení	k1gNnSc4	obnovení
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
smlouvy	smlouva	k1gFnSc2	smlouva
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c2	za
připojení	připojení	k1gNnSc2	připojení
svobodného	svobodný	k2eAgNnSc2d1	svobodné
města	město	k1gNnSc2	město
Svobodného	svobodný	k2eAgNnSc2d1	svobodné
města	město	k1gNnSc2	město
Gdaňsk	Gdaňsk	k1gInSc1	Gdaňsk
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
za	za	k7c2	za
povolení	povolení	k1gNnSc2	povolení
výstavby	výstavba	k1gFnSc2	výstavba
exteritoriální	exteritoriální	k2eAgFnSc2d1	exteritoriální
dálnice	dálnice	k1gFnSc2	dálnice
a	a	k8xC	a
železnice	železnice	k1gFnSc2	železnice
mezi	mezi	k7c7	mezi
Východním	východní	k2eAgNnSc7d1	východní
Pruskem	Prusko	k1gNnSc7	Prusko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
přes	přes	k7c4	přes
polský	polský	k2eAgInSc4d1	polský
koridor	koridor	k1gInSc4	koridor
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
německo-polských	německoolský	k2eAgInPc2d1	německo-polský
rozhovorů	rozhovor	k1gInPc2	rozhovor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgInP	konat
až	až	k9	až
do	do	k7c2	do
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Lipski	Lipski	k6eAd1	Lipski
pak	pak	k6eAd1	pak
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
hlásil	hlásit	k5eAaImAgMnS	hlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
Možnou	možný	k2eAgFnSc7d1	možná
oblastí	oblast	k1gFnSc7	oblast
vzájemné	vzájemný	k2eAgFnSc2d1	vzájemná
spolupráce	spolupráce	k1gFnSc2	spolupráce
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
německého	německý	k2eAgMnSc2d1	německý
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
společná	společný	k2eAgFnSc1d1	společná
akce	akce	k1gFnSc1	akce
v	v	k7c6	v
záležitostech	záležitost	k1gFnPc6	záležitost
koloniálních	koloniální	k2eAgMnPc2d1	koloniální
a	a	k8xC	a
otázka	otázka	k1gFnSc1	otázka
emigrace	emigrace	k1gFnSc2	emigrace
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
společná	společný	k2eAgFnSc1d1	společná
politika	politika	k1gFnSc1	politika
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
<g/>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
zásadách	zásada	k1gFnPc6	zásada
Paktu	pakt	k1gInSc2	pakt
proti	proti	k7c3	proti
Kominterně	Kominterna	k1gFnSc3	Kominterna
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1939	[number]	k4	1939
přijal	přijmout	k5eAaPmAgMnS	přijmout
Hitler	Hitler	k1gMnSc1	Hitler
v	v	k7c4	v
Berchtesgadenu	Berchtesgaden	k2eAgFnSc4d1	Berchtesgaden
polského	polský	k2eAgMnSc2d1	polský
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Józefa	Józef	k1gMnSc2	Józef
Becka	Becek	k1gMnSc2	Becek
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
společných	společný	k2eAgInPc6d1	společný
zájmech	zájem	k1gInPc6	zájem
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Rusku	Rusko	k1gNnSc3	Rusko
a	a	k8xC	a
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
polská	polský	k2eAgFnSc1d1	polská
divize	divize	k1gFnSc1	divize
činná	činný	k2eAgFnSc1d1	činná
proti	proti	k7c3	proti
Rusku	Rusko	k1gNnSc3	Rusko
znamená	znamenat	k5eAaImIp3nS	znamenat
ušetření	ušetření	k1gNnSc4	ušetření
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
divize	divize	k1gFnSc2	divize
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
zmíní	zmínit	k5eAaPmIp3nS	zmínit
důležitost	důležitost	k1gFnSc4	důležitost
existence	existence	k1gFnSc2	existence
silného	silný	k2eAgNnSc2d1	silné
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Beck	Beck	k1gMnSc1	Beck
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
jen	jen	k9	jen
připravenost	připravenost	k1gFnSc4	připravenost
Polska	Polsko	k1gNnSc2	Polsko
vyvázat	vyvázat	k5eAaPmF	vyvázat
Gdaňsk	Gdaňsk	k1gInSc4	Gdaňsk
zpod	zpod	k7c2	zpod
protektorátu	protektorát	k1gInSc2	protektorát
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
a	a	k8xC	a
podřídit	podřídit	k5eAaPmF	podřídit
jej	on	k3xPp3gMnSc4	on
polsko-německé	polskoěmecký	k2eAgFnPc1d1	polsko-německý
svrchovanosti	svrchovanost	k1gFnPc1	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Beckův	Beckův	k2eAgInSc4d1	Beckův
postoj	postoj	k1gInSc4	postoj
byl	být	k5eAaImAgMnS	být
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
ač	ač	k8xS	ač
je	být	k5eAaImIp3nS	být
Polsko	Polsko	k1gNnSc1	Polsko
protibolševicky	protibolševicky	k6eAd1	protibolševicky
naladěné	naladěný	k2eAgNnSc1d1	naladěné
<g/>
,	,	kIx,	,
nebude	být	k5eNaImBp3nS	být
se	se	k3xPyFc4	se
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
akcích	akce	k1gFnPc6	akce
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
SSSR	SSSR	kA	SSSR
měla	mít	k5eAaImAgFnS	mít
o	o	k7c6	o
vyjednáváních	vyjednávání	k1gNnPc6	vyjednávání
a	a	k8xC	a
polsko-německých	polskoěmecký	k2eAgInPc6d1	polsko-německý
vztazích	vztah	k1gInPc6	vztah
podrobné	podrobný	k2eAgFnPc1d1	podrobná
zprávy	zpráva	k1gFnPc1	zpráva
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
agenta	agent	k1gMnSc2	agent
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
německém	německý	k2eAgNnSc6d1	německé
vyslanectví	vyslanectví	k1gNnSc6	vyslanectví
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
německé	německý	k2eAgInPc1d1	německý
územní	územní	k2eAgInPc1d1	územní
požadavky	požadavek	k1gInPc1	požadavek
kategoricky	kategoricky	k6eAd1	kategoricky
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgMnSc4	ten
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
německo-polská	německoolský	k2eAgFnSc1d1	německo-polská
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
Hitlerem	Hitler	k1gMnSc7	Hitler
jednostranně	jednostranně	k6eAd1	jednostranně
vypovězena	vypovědět	k5eAaPmNgFnS	vypovědět
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
šíři	šíř	k1gFnSc6	šíř
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
důvodem	důvod	k1gInSc7	důvod
vypovězení	vypovězení	k1gNnSc2	vypovězení
bylo	být	k5eAaImAgNnS	být
uzavření	uzavření	k1gNnSc4	uzavření
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Polskem	Polsko	k1gNnSc7	Polsko
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sovětská	sovětský	k2eAgNnPc1d1	sovětské
jednání	jednání	k1gNnPc1	jednání
se	s	k7c7	s
Západem	západ	k1gInSc7	západ
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
postojů	postoj	k1gInPc2	postoj
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
došlo	dojít	k5eAaPmAgNnS	dojít
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
německými	německý	k2eAgFnPc7d1	německá
vojsky	vojsky	k6eAd1	vojsky
<g/>
.	.	kIx.	.
</s>
<s>
Týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Litvinov	Litvinov	k1gInSc4	Litvinov
významné	významný	k2eAgInPc4d1	významný
evropské	evropský	k2eAgInPc4d1	evropský
státy	stát	k1gInPc4	stát
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
německou	německý	k2eAgFnSc7d1	německá
agresivní	agresivní	k2eAgFnSc7d1	agresivní
politikou	politika	k1gFnSc7	politika
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
smlouvě	smlouva	k1gFnSc6	smlouva
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
připojit	připojit	k5eAaPmF	připojit
i	i	k9	i
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
premiér	premiér	k1gMnSc1	premiér
Neville	Neville	k1gNnSc2	Neville
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
však	však	k9	však
jednání	jednání	k1gNnSc4	jednání
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
předčasné	předčasný	k2eAgNnSc1d1	předčasné
<g/>
,	,	kIx,	,
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
společného	společný	k2eAgNnSc2d1	společné
prohlášení	prohlášení	k1gNnSc2	prohlášení
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
vůči	vůči	k7c3	vůči
ohrožení	ohrožení	k1gNnSc3	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
podepsat	podepsat	k5eAaPmF	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
mezi	mezi	k7c7	mezi
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
probíhala	probíhat	k5eAaImAgFnS	probíhat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
března	březen	k1gInSc2	březen
do	do	k7c2	do
konce	konec	k1gInSc2	konec
dubna	duben	k1gInSc2	duben
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnSc1	diskuze
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
případné	případný	k2eAgFnPc4d1	případná
záruky	záruka	k1gFnPc4	záruka
pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
německé	německý	k2eAgFnSc2d1	německá
agrese	agrese	k1gFnSc2	agrese
<g/>
.	.	kIx.	.
</s>
<s>
SSSR	SSSR	kA	SSSR
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
politický	politický	k2eAgInSc1d1	politický
obrat	obrat	k1gInSc1	obrat
v	v	k7c6	v
pobaltských	pobaltský	k2eAgInPc6d1	pobaltský
státech	stát	k1gInPc6	stát
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
představoval	představovat	k5eAaImAgMnS	představovat
'	'	kIx"	'
<g/>
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
agresi	agrese	k1gFnSc4	agrese
<g/>
'	'	kIx"	'
vůči	vůči	k7c3	vůči
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
tomu	ten	k3xDgMnSc3	ten
postavila	postavit	k5eAaPmAgFnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Obávala	obávat	k5eAaImAgFnS	obávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
formulace	formulace	k1gFnSc1	formulace
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
ospravedlnit	ospravedlnit	k5eAaPmF	ospravedlnit
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
intervenci	intervence	k1gFnSc4	intervence
ve	v	k7c6	v
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
pobaltských	pobaltský	k2eAgInPc6d1	pobaltský
státech	stát	k1gInPc6	stát
nebo	nebo	k8xC	nebo
přimět	přimět	k5eAaPmF	přimět
tyto	tento	k3xDgFnPc4	tento
země	zem	k1gFnPc4	zem
k	k	k7c3	k
hledání	hledání	k1gNnSc3	hledání
užších	úzký	k2eAgInPc2d2	užší
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Diskuze	diskuze	k1gFnSc1	diskuze
o	o	k7c6	o
definici	definice	k1gFnSc6	definice
'	'	kIx"	'
<g/>
nepřímé	přímý	k2eNgFnSc2d1	nepřímá
agrese	agrese	k1gFnSc2	agrese
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
politická	politický	k2eAgNnPc1d1	politické
jednání	jednání	k1gNnPc1	jednání
zastavila	zastavit	k5eAaPmAgNnP	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1939	[number]	k4	1939
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
plánované	plánovaný	k2eAgNnSc4d1	plánované
jednání	jednání	k1gNnSc4	jednání
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
britsko-francouzsko-sovětské	britskorancouzskoovětský	k2eAgInPc1d1	britsko-francouzsko-sovětský
<g/>
)	)	kIx)	)
o	o	k7c6	o
vojenských	vojenský	k2eAgFnPc6d1	vojenská
záležitostech	záležitost	k1gFnPc6	záležitost
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
definovat	definovat	k5eAaBmF	definovat
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
reakce	reakce	k1gFnPc4	reakce
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
mocností	mocnost	k1gFnPc2	mocnost
na	na	k7c4	na
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
německou	německý	k2eAgFnSc4d1	německá
agresi	agrese	k1gFnSc4	agrese
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Francouzi	Francouz	k1gMnPc1	Francouz
chtěli	chtít	k5eAaImAgMnP	chtít
vést	vést	k5eAaImF	vést
celé	celý	k2eAgNnSc4d1	celé
jednání	jednání	k1gNnSc4	jednání
spíše	spíše	k9	spíše
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
rovině	rovina	k1gFnSc6	rovina
a	a	k8xC	a
nechtěli	chtít	k5eNaImAgMnP	chtít
řešit	řešit	k5eAaImF	řešit
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
problematiku	problematika	k1gFnSc4	problematika
nasazení	nasazení	k1gNnSc2	nasazení
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
Sověti	Sovět	k1gMnPc1	Sovět
byli	být	k5eAaImAgMnP	být
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
zpravodajským	zpravodajský	k2eAgFnPc3d1	zpravodajská
službám	služba	k1gFnPc3	služba
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
informováni	informovat	k5eAaBmNgMnP	informovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc4	jaký
jsou	být	k5eAaImIp3nP	být
vyjednávací	vyjednávací	k2eAgFnPc4d1	vyjednávací
pozice	pozice	k1gFnPc4	pozice
a	a	k8xC	a
mantinely	mantinel	k1gInPc4	mantinel
britské	britský	k2eAgFnSc2d1	britská
a	a	k8xC	a
francouzské	francouzský	k2eAgFnSc2d1	francouzská
delegace	delegace	k1gFnSc2	delegace
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
naprosto	naprosto	k6eAd1	naprosto
jasnou	jasný	k2eAgFnSc4d1	jasná
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
čeho	co	k3yQnSc2	co
chtějí	chtít	k5eAaImIp3nP	chtít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žádné	žádný	k3yNgFnSc2	žádný
dohody	dohoda	k1gFnSc2	dohoda
nebyly	být	k5eNaImAgInP	být
uzavřeny	uzavřen	k2eAgInPc1d1	uzavřen
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
vydal	vydat	k5eAaPmAgMnS	vydat
Stalin	Stalin	k1gMnSc1	Stalin
pro	pro	k7c4	pro
hlavního	hlavní	k2eAgMnSc4d1	hlavní
vyjednávače	vyjednávač	k1gMnSc4	vyjednávač
<g/>
,	,	kIx,	,
maršála	maršál	k1gMnSc4	maršál
Vorošilova	Vorošilův	k2eAgMnSc4d1	Vorošilův
<g/>
,	,	kIx,	,
detailní	detailní	k2eAgFnSc4d1	detailní
směrnici	směrnice	k1gFnSc4	směrnice
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
má	mít	k5eAaImIp3nS	mít
vést	vést	k5eAaImF	vést
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
měl	mít	k5eAaImAgMnS	mít
sovětský	sovětský	k2eAgMnSc1d1	sovětský
maršál	maršál	k1gMnSc1	maršál
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zmocněn	zmocněn	k2eAgInSc1d1	zmocněn
podepsat	podepsat	k5eAaPmF	podepsat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
smlouvy	smlouva	k1gFnPc4	smlouva
a	a	k8xC	a
zeptat	zeptat	k5eAaPmF	zeptat
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgInPc2	svůj
protějšků	protějšek	k1gInPc2	protějšek
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
i	i	k9	i
oni	onen	k3xDgMnPc1	onen
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgFnPc4d1	stejná
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
past	past	k1gFnSc4	past
Sovětů	Sovět	k1gMnPc2	Sovět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc2d1	předchozí
jednání	jednání	k1gNnSc4	jednání
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
požadoval	požadovat	k5eAaImAgMnS	požadovat
Molotov	Molotov	k1gInSc4	Molotov
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vojenské	vojenský	k2eAgFnPc1d1	vojenská
smlouvy	smlouva	k1gFnPc1	smlouva
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
až	až	k9	až
společně	společně	k6eAd1	společně
se	s	k7c7	s
spojeneckou	spojenecký	k2eAgFnSc7d1	spojenecká
úmluvou	úmluva	k1gFnSc7	úmluva
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Vorošilov	Vorošilov	k1gInSc1	Vorošilov
dověděl	dovědět	k5eAaPmAgInS	dovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
pravomoci	pravomoc	k1gFnPc1	pravomoc
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
"	"	kIx"	"
<g/>
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
překvapení	překvapení	k1gNnSc4	překvapení
<g/>
,	,	kIx,	,
rozhodit	rozhodit	k5eAaPmF	rozhodit
rukama	ruka	k1gFnPc7	ruka
a	a	k8xC	a
uctivě	uctivě	k6eAd1	uctivě
se	se	k3xPyFc4	se
zeptat	zeptat	k5eAaPmF	zeptat
<g/>
,	,	kIx,	,
za	za	k7c7	za
jakým	jaký	k3yIgInSc7	jaký
účelem	účel	k1gInSc7	účel
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vláda	vláda	k1gFnSc1	vláda
poslala	poslat	k5eAaPmAgFnS	poslat
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podobném	podobný	k2eAgMnSc6d1	podobný
duchu	duch	k1gMnSc6	duch
Stalinova	Stalinův	k2eAgFnSc1d1	Stalinova
směrnice	směrnice	k1gFnSc1	směrnice
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
Vorošilov	Vorošilov	k1gInSc1	Vorošilov
měl	mít	k5eAaImAgInS	mít
svádět	svádět	k5eAaImF	svádět
rozhovory	rozhovor	k1gInPc4	rozhovor
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnPc4d1	konkrétní
věci	věc	k1gFnPc4	věc
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
západní	západní	k2eAgFnSc3d1	západní
mocnosti	mocnost	k1gFnSc3	mocnost
buď	buď	k8xC	buď
ještě	ještě	k6eAd1	ještě
nemají	mít	k5eNaImIp3nP	mít
vyřešené	vyřešený	k2eAgFnPc1d1	vyřešená
a	a	k8xC	a
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nebudou	být	k5eNaImBp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
garantovat	garantovat	k5eAaBmF	garantovat
<g/>
,	,	kIx,	,
např.	např.	kA	např.
možnost	možnost	k1gFnSc1	možnost
průchodu	průchod	k1gInSc2	průchod
sovětské	sovětský	k2eAgFnSc2d1	sovětská
armády	armáda	k1gFnSc2	armáda
skrz	skrz	k7c4	skrz
Polsko	Polsko	k1gNnSc4	Polsko
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Polska	Polsko	k1gNnSc2	Polsko
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc4	tento
prakticky	prakticky	k6eAd1	prakticky
nesplnitelné	splnitelný	k2eNgNnSc4d1	nesplnitelné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
britští	britský	k2eAgMnPc1d1	britský
a	a	k8xC	a
francouzští	francouzský	k2eAgMnPc1d1	francouzský
představitelé	představitel	k1gMnPc1	představitel
tlačili	tlačit	k5eAaImAgMnP	tlačit
na	na	k7c4	na
Poláky	Polák	k1gMnPc4	Polák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
možností	možnost	k1gFnSc7	možnost
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
nakonec	nakonec	k6eAd1	nakonec
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
povolit	povolit	k5eAaPmF	povolit
vstup	vstup	k1gInSc4	vstup
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c4	na
polské	polský	k2eAgNnSc4d1	polské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
polský	polský	k2eAgMnSc1d1	polský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Józef	Józef	k1gMnSc1	Józef
Beck	Beck	k1gMnSc1	Beck
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
poukázal	poukázat	k5eAaPmAgMnS	poukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakmile	jakmile	k8xS	jakmile
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
nemusela	muset	k5eNaImAgFnS	muset
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Vorošilov	Vorošilov	k1gInSc1	Vorošilov
potom	potom	k6eAd1	potom
směrnici	směrnice	k1gFnSc4	směrnice
přesně	přesně	k6eAd1	přesně
naplňoval	naplňovat	k5eAaImAgMnS	naplňovat
a	a	k8xC	a
nasměřoval	nasměřovat	k5eAaImAgMnS	nasměřovat
tak	tak	k6eAd1	tak
celá	celý	k2eAgNnPc4d1	celé
jednání	jednání	k1gNnPc4	jednání
ke	k	k7c3	k
krachu	krach	k1gInSc3	krach
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Sověti	Sovět	k1gMnPc1	Sovět
trojstranné	trojstranný	k2eAgInPc4d1	trojstranný
vojenské	vojenský	k2eAgInPc4d1	vojenský
rozhovory	rozhovor	k1gInPc4	rozhovor
pozastavili	pozastavit	k5eAaPmAgMnP	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Stalin	Stalin	k1gMnSc1	Stalin
ujištění	ujištění	k1gNnSc2	ujištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
by	by	kYmCp3nP	by
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
s	s	k7c7	s
tajnou	tajný	k2eAgFnSc7d1	tajná
částí	část	k1gFnSc7	část
navrhovaného	navrhovaný	k2eAgInSc2d1	navrhovaný
paktu	pakt	k1gInSc2	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
definoval	definovat	k5eAaBmAgInS	definovat
oblasti	oblast	k1gFnSc3	oblast
zájmu	zájem	k1gInSc2	zájem
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
obratem	obratem	k6eAd1	obratem
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
a	a	k8xC	a
že	že	k8xS	že
přijme	přijmout	k5eAaPmIp3nS	přijmout
německého	německý	k2eAgMnSc4d1	německý
ministra	ministr	k1gMnSc4	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Joachima	Joachima	k1gNnSc1	Joachima
von	von	k1gInSc1	von
Ribbentropa	Ribbentropa	k1gFnSc1	Ribbentropa
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Německo-sovětská	německoovětský	k2eAgNnPc1d1	německo-sovětský
jednání	jednání	k1gNnPc1	jednání
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
sovětští	sovětský	k2eAgMnPc1d1	sovětský
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
úředníci	úředník	k1gMnPc1	úředník
podávali	podávat	k5eAaImAgMnP	podávat
řadu	řada	k1gFnSc4	řada
hlášení	hlášení	k1gNnSc2	hlášení
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
potenciálu	potenciál	k1gInSc2	potenciál
zahájení	zahájení	k1gNnSc2	zahájení
politických	politický	k2eAgNnPc2d1	politické
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
žádná	žádný	k3yNgNnPc1	žádný
skutečná	skutečný	k2eAgNnPc1d1	skutečné
jednání	jednání	k1gNnPc1	jednání
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
nevedla	vést	k5eNaImAgFnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
tajně	tajně	k6eAd1	tajně
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	k9	by
sovětským	sovětský	k2eAgMnSc7d1	sovětský
diplomatům	diplomat	k1gMnPc3	diplomat
mohlo	moct	k5eAaImAgNnS	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
lepší	dobrý	k2eAgFnPc4d2	lepší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
politickou	politický	k2eAgFnSc4d1	politická
dohodu	dohoda	k1gFnSc4	dohoda
než	než	k8xS	než
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
Stalin	Stalin	k1gMnSc1	Stalin
nahradil	nahradit	k5eAaPmAgMnS	nahradit
komisaře	komisař	k1gMnPc4	komisař
Maxima	Maxim	k1gMnSc4	Maxim
Litvinova	Litvinův	k2eAgMnSc4d1	Litvinův
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
příliš	příliš	k6eAd1	příliš
prozápadního	prozápadní	k2eAgNnSc2d1	prozápadní
a	a	k8xC	a
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
navíc	navíc	k6eAd1	navíc
žid	žid	k1gMnSc1	žid
<g/>
,	,	kIx,	,
Vjačeslavem	Vjačeslav	k1gMnSc7	Vjačeslav
Molotovem	Molotov	k1gInSc7	Molotov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
sovětští	sovětský	k2eAgMnPc1d1	sovětský
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
představitelé	představitel	k1gMnPc1	představitel
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
podrobností	podrobnost	k1gFnPc2	podrobnost
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
řešit	řešit	k5eAaImF	řešit
potenciální	potenciální	k2eAgFnSc4d1	potenciální
politickou	politický	k2eAgFnSc4d1	politická
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Stalin	Stalin	k1gMnSc1	Stalin
nechtěl	chtít	k5eNaImAgMnS	chtít
uzavřít	uzavřít	k5eAaPmF	uzavřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
bude	být	k5eAaImBp3nS	být
dohodnuta	dohodnout	k5eAaPmNgFnS	dohodnout
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
politické	politický	k2eAgFnSc2d1	politická
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dělení	dělení	k1gNnSc1	dělení
Polska	Polsko	k1gNnSc2	Polsko
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
mocnosti	mocnost	k1gFnPc4	mocnost
<g/>
,	,	kIx,	,
předalo	předat	k5eAaPmAgNnS	předat
Německo	Německo	k1gNnSc1	Německo
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
dohodě	dohoda	k1gFnSc3	dohoda
zachoval	zachovat	k5eAaPmAgInS	zachovat
chladně	chladně	k6eAd1	chladně
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Německo	Německo	k1gNnSc1	Německo
předalo	předat	k5eAaPmAgNnS	předat
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
večer	večer	k1gInSc1	večer
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
si	se	k3xPyFc3	se
však	však	k9	však
vymínil	vymínit	k5eAaPmAgMnS	vymínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejprve	nejprve	k6eAd1	nejprve
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
část	část	k1gFnSc1	část
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byla	být	k5eAaImAgFnS	být
proto	proto	k8xC	proto
německo-sovětská	německoovětský	k2eAgFnSc1d1	německo-sovětská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
dohoda	dohoda	k1gFnSc1	dohoda
podepsána	podepsat	k5eAaPmNgFnS	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
dále	daleko	k6eAd2	daleko
určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
německého	německý	k2eAgMnSc2d1	německý
komisaře	komisař	k1gMnSc2	komisař
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Německo	Německo	k1gNnSc1	Německo
přijme	přijmout	k5eAaPmIp3nS	přijmout
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
část	část	k1gFnSc4	část
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
přijme	přijmout	k5eAaPmIp3nS	přijmout
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
kvůli	kvůli	k7c3	kvůli
podpisu	podpis	k1gInSc3	podpis
politické	politický	k2eAgFnSc2d1	politická
části	část	k1gFnSc2	část
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
německý	německý	k2eAgMnSc1d1	německý
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
von	von	k1gInSc4	von
Schulenburg	Schulenburg	k1gInSc1	Schulenburg
dojednává	dojednávat	k5eAaImIp3nS	dojednávat
se	s	k7c7	s
sovětským	sovětský	k2eAgMnSc7d1	sovětský
lidovým	lidový	k2eAgMnSc7d1	lidový
komisařem	komisař	k1gMnSc7	komisař
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
věci	věc	k1gFnPc4	věc
Maximem	maximum	k1gNnSc7	maximum
Litvinovem	Litvinovo	k1gNnSc7	Litvinovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tisk	tisk	k1gInSc1	tisk
a	a	k8xC	a
rozhlas	rozhlas	k1gInSc1	rozhlas
obou	dva	k4xCgFnPc2	dva
zemí	zem	k1gFnPc2	zem
na	na	k7c4	na
příště	příště	k6eAd1	příště
zdrží	zdržet	k5eAaPmIp3nS	zdržet
kritiky	kritik	k1gMnPc4	kritik
Hitlera	Hitler	k1gMnSc4	Hitler
a	a	k8xC	a
Stalina	Stalin	k1gMnSc4	Stalin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uzavření	uzavření	k1gNnSc4	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
==	==	k?	==
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
poledni	poledne	k1gNnSc6	poledne
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
přiletěl	přiletět	k5eAaPmAgMnS	přiletět
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
německý	německý	k2eAgMnSc1d1	německý
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
krátkém	krátký	k2eAgInSc6d1	krátký
odpočinku	odpočinek	k1gInSc6	odpočinek
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
závěrečnému	závěrečný	k2eAgNnSc3d1	závěrečné
jednání	jednání	k1gNnSc3	jednání
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
už	už	k9	už
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
podepsán	podepsat	k5eAaPmNgInS	podepsat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Záležitost	záležitost	k1gFnSc1	záležitost
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
formalitou	formalita	k1gFnSc7	formalita
<g/>
,	,	kIx,	,
Stalina	Stalin	k1gMnSc2	Stalin
spíše	spíše	k9	spíše
zajímala	zajímat	k5eAaImAgFnS	zajímat
otázka	otázka	k1gFnSc1	otázka
vymezení	vymezení	k1gNnSc2	vymezení
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Přál	přát	k5eAaImAgMnS	přát
si	se	k3xPyFc3	se
obnovit	obnovit	k5eAaPmF	obnovit
rozsah	rozsah	k1gInSc4	rozsah
území	území	k1gNnSc2	území
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
hranicích	hranice	k1gFnPc6	hranice
původního	původní	k2eAgNnSc2d1	původní
carského	carský	k2eAgNnSc2d1	carské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
především	především	k9	především
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
Rusko	Rusko	k1gNnSc1	Rusko
přišlo	přijít	k5eAaPmAgNnS	přijít
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Brestlitevského	brestlitevský	k2eAgInSc2d1	brestlitevský
míru	mír	k1gInSc2	mír
a	a	k8xC	a
sovětsko-polské	sovětskoolský	k2eAgFnSc2d1	sovětsko-polská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
dojednáno	dojednán	k2eAgNnSc1d1	dojednáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
bude	být	k5eAaImBp3nS	být
hranice	hranice	k1gFnSc1	hranice
vlivu	vliv	k1gInSc2	vliv
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
severní	severní	k2eAgFnSc7d1	severní
hranicí	hranice	k1gFnSc7	hranice
Litvy	Litva	k1gFnSc2	Litva
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
k	k	k7c3	k
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
připadne	připadnout	k5eAaPmIp3nS	připadnout
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
a	a	k8xC	a
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
že	že	k8xS	že
hranice	hranice	k1gFnSc1	hranice
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
bude	být	k5eAaImBp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
liniemi	linie	k1gFnPc7	linie
řek	řeka	k1gFnPc2	řeka
Narew	Narew	k1gMnPc2	Narew
<g/>
,	,	kIx,	,
Visla	Visla	k1gFnSc1	Visla
a	a	k8xC	a
San	San	k1gFnSc1	San
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
samostatného	samostatný	k2eAgInSc2d1	samostatný
polského	polský	k2eAgInSc2d1	polský
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
ještě	ještě	k6eAd1	ještě
jednat	jednat	k5eAaImF	jednat
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
vlivu	vliv	k1gInSc3	vliv
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
sovětská	sovětský	k2eAgFnSc1d1	sovětská
strana	strana	k1gFnSc1	strana
deklarovala	deklarovat	k5eAaBmAgFnS	deklarovat
svůj	svůj	k3xOyFgInSc4	svůj
zájem	zájem	k1gInSc4	zájem
v	v	k7c6	v
Besarábii	Besarábie	k1gFnSc6	Besarábie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
byl	být	k5eAaImAgInS	být
pořízen	pořízen	k2eAgInSc1d1	pořízen
přísně	přísně	k6eAd1	přísně
tajný	tajný	k2eAgInSc1d1	tajný
protokol	protokol	k1gInSc1	protokol
podepsaný	podepsaný	k2eAgInSc1d1	podepsaný
oběma	dva	k4xCgMnPc7	dva
vyjednavači	vyjednavač	k1gMnPc7	vyjednavač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
sovětu	sovět	k1gInSc2	sovět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Molotov	Molotov	k1gInSc1	Molotov
zahájil	zahájit	k5eAaPmAgInS	zahájit
sérii	série	k1gFnSc4	série
proněmeckých	proněmecký	k2eAgInPc2d1	proněmecký
projevů	projev	k1gInPc2	projev
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
dostala	dostat	k5eAaPmAgFnS	dostat
Kominterna	Kominterna	k1gFnSc1	Kominterna
nové	nový	k2eAgFnSc2d1	nová
instrukce	instrukce	k1gFnSc2	instrukce
a	a	k8xC	a
bleskurychle	bleskurychle	k6eAd1	bleskurychle
skončila	skončit	k5eAaPmAgFnS	skončit
s	s	k7c7	s
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
protifašistickou	protifašistický	k2eAgFnSc7d1	protifašistická
a	a	k8xC	a
protinacistickou	protinacistický	k2eAgFnSc7d1	protinacistická
rétorikou	rétorika	k1gFnSc7	rétorika
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
popisovat	popisovat	k5eAaImF	popisovat
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jako	jako	k8xS	jako
záležitost	záležitost	k1gFnSc1	záležitost
imperialistických	imperialistický	k2eAgFnPc2d1	imperialistická
tendencí	tendence	k1gFnPc2	tendence
kapitalistických	kapitalistický	k2eAgFnPc2d1	kapitalistická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
byli	být	k5eAaImAgMnP	být
instruováni	instruován	k2eAgMnPc1d1	instruován
komunisté	komunista	k1gMnPc1	komunista
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
od	od	k7c2	od
podpory	podpora	k1gFnSc2	podpora
zajištění	zajištění	k1gNnSc2	zajištění
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začali	začít	k5eAaPmAgMnP	začít
najednou	najednou	k6eAd1	najednou
obviňovat	obviňovat	k5eAaImF	obviňovat
Británii	Británie	k1gFnSc4	Británie
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
J.	J.	kA	J.
V.	V.	kA	V.
Stalin	Stalin	k1gMnSc1	Stalin
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rozhovoru	rozhovor	k1gInSc6	rozhovor
v	v	k7c6	v
sovětském	sovětský	k2eAgInSc6d1	sovětský
tisku	tisk	k1gInSc6	tisk
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1939	[number]	k4	1939
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nikoliv	nikoliv	k9	nikoliv
Německo	Německo	k1gNnSc1	Německo
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
naopak	naopak	k6eAd1	naopak
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Anglie	Anglie	k1gFnSc2	Anglie
napadly	napadnout	k5eAaPmAgInP	napadnout
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
na	na	k7c6	na
sebe	sebe	k3xPyFc4	sebe
převzaly	převzít	k5eAaPmAgInP	převzít
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
za	za	k7c4	za
rozpoutání	rozpoutání	k1gNnSc4	rozpoutání
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
podpisu	podpis	k1gInSc3	podpis
Dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
mezi	mezi	k7c7	mezi
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
a	a	k8xC	a
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Následky	následek	k1gInPc1	následek
smlouvy	smlouva	k1gFnSc2	smlouva
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1939	[number]	k4	1939
napadlo	napadnout	k5eAaPmAgNnS	napadnout
Německo	Německo	k1gNnSc1	Německo
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
provedlo	provést	k5eAaPmAgNnS	provést
invazi	invaze	k1gFnSc4	invaze
o	o	k7c4	o
17	[number]	k4	17
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
o	o	k7c6	o
klidu	klid	k1gInSc6	klid
zbraní	zbraň	k1gFnPc2	zbraň
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáct	jedenáct	k4xCc1	jedenáct
dní	den	k1gInPc2	den
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
invazi	invaze	k1gFnSc6	invaze
byl	být	k5eAaImAgInS	být
tajným	tajný	k2eAgInSc7d1	tajný
dodatkem	dodatek	k1gInSc7	dodatek
pakt	pakt	k1gInSc1	pakt
upraven	upravit	k5eAaPmNgInS	upravit
na	na	k7c4	na
Německo-sovětskou	německoovětský	k2eAgFnSc4d1	německo-sovětská
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Německu	Německo	k1gNnSc3	Německo
připadla	připadnout	k5eAaPmAgFnS	připadnout
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
Litva	Litva	k1gFnSc1	Litva
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
levého	levý	k2eAgInSc2d1	levý
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Scheschupe	Scheschup	k1gInSc5	Scheschup
(	(	kIx(	(
<g/>
Šešupė	Šešupė	k1gMnSc6	Šešupė
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
sovětské	sovětský	k2eAgFnSc2d1	sovětská
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Lvově	Lvov	k1gInSc6	Lvov
a	a	k8xC	a
Brestu	Brest	k1gInSc2	Brest
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
společné	společný	k2eAgFnPc1d1	společná
vojenské	vojenský	k2eAgFnPc1d1	vojenská
přehlídky	přehlídka	k1gFnPc1	přehlídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
polské	polský	k2eAgFnSc2d1	polská
armády	armáda	k1gFnSc2	armáda
byl	být	k5eAaImAgInS	být
nařízením	nařízení	k1gNnSc7	nařízení
Hitlera	Hitler	k1gMnSc2	Hitler
zřízen	zřídit	k5eAaPmNgInS	zřídit
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Polska	Polsko	k1gNnSc2	Polsko
tzv.	tzv.	kA	tzv.
Generální	generální	k2eAgNnSc1d1	generální
gouvernement	gouvernement	k1gNnSc1	gouvernement
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgFnPc1d1	zbylá
části	část	k1gFnPc1	část
(	(	kIx(	(
<g/>
Pomoří	Pomoří	k1gNnSc1	Pomoří
<g/>
,	,	kIx,	,
Velkopolsko	Velkopolsko	k1gNnSc1	Velkopolsko
<g/>
,	,	kIx,	,
Slezsko	Slezsko	k1gNnSc1	Slezsko
<g/>
,	,	kIx,	,
Kujavsko	Kujavsko	k1gNnSc1	Kujavsko
<g/>
,	,	kIx,	,
západní	západní	k2eAgNnSc1d1	západní
Mazovsko	Mazovsko	k1gNnSc1	Mazovsko
a	a	k8xC	a
části	část	k1gFnPc1	část
vojvodství	vojvodství	k1gNnSc2	vojvodství
Lodžského	lodžský	k2eAgNnSc2d1	Lodžské
<g/>
,	,	kIx,	,
Krakovského	krakovský	k2eAgMnSc2d1	krakovský
a	a	k8xC	a
Kieleckého	Kielecký	k2eAgMnSc2d1	Kielecký
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
přičleněny	přičleněn	k2eAgFnPc1d1	přičleněna
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Třetí	třetí	k4xOgFnSc3	třetí
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnPc1d1	východní
části	část	k1gFnPc1	část
Polska	Polsko	k1gNnSc2	Polsko
byly	být	k5eAaImAgFnP	být
opět	opět	k6eAd1	opět
přičleněny	přičleněn	k2eAgFnPc1d1	přičleněna
formálně	formálně	k6eAd1	formálně
k	k	k7c3	k
Ukrajině	Ukrajina	k1gFnSc3	Ukrajina
a	a	k8xC	a
Bělorusku	Běloruska	k1gFnSc4	Běloruska
<g/>
,	,	kIx,	,
Vilnius	Vilnius	k1gInSc4	Vilnius
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
získala	získat	k5eAaPmAgFnS	získat
zpět	zpět	k6eAd1	zpět
Litva	Litva	k1gFnSc1	Litva
<g/>
,	,	kIx,	,
prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
území	území	k1gNnSc6	území
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SSSR	SSSR	kA	SSSR
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
naplňovat	naplňovat	k5eAaImF	naplňovat
další	další	k2eAgInPc4d1	další
body	bod	k1gInPc4	bod
tajného	tajný	k2eAgInSc2d1	tajný
dodatku	dodatek	k1gInSc2	dodatek
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
ruského	ruský	k2eAgInSc2d1	ruský
útoku	útok	k1gInSc2	útok
proti	proti	k7c3	proti
Finsku	Finsko	k1gNnSc3	Finsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
plány	plán	k1gInPc1	plán
na	na	k7c4	na
obsazení	obsazení	k1gNnSc4	obsazení
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
zhatil	zhatit	k5eAaPmAgInS	zhatit
nečekaně	nečekaně	k6eAd1	nečekaně
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
Finů	Fin	k1gMnPc2	Fin
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tzv.	tzv.	kA	tzv.
Moskevský	moskevský	k2eAgInSc4d1	moskevský
mír	mír	k1gInSc4	mír
z	z	k7c2	z
března	březen	k1gInSc2	březen
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
bylo	být	k5eAaImAgNnS	být
Finsko	Finsko	k1gNnSc1	Finsko
nuceno	nucen	k2eAgNnSc1d1	nuceno
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
Karelské	karelský	k2eAgFnSc2d1	Karelská
šíje	šíj	k1gFnSc2	šíj
a	a	k8xC	a
části	část	k1gFnSc2	část
východní	východní	k2eAgFnSc2d1	východní
Karélie	Karélie	k1gFnSc2	Karélie
<g/>
.	.	kIx.	.
</s>
<s>
Finsko	Finsko	k1gNnSc1	Finsko
si	se	k3xPyFc3	se
ale	ale	k8xC	ale
udrželo	udržet	k5eAaPmAgNnS	udržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
omezenou	omezený	k2eAgFnSc4d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Pobaltské	pobaltský	k2eAgFnPc1d1	pobaltská
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
a	a	k8xC	a
Litva	Litva	k1gFnSc1	Litva
nedostaly	dostat	k5eNaPmAgFnP	dostat
jinou	jiný	k2eAgFnSc4d1	jiná
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
než	než	k8xS	než
podepsat	podepsat	k5eAaPmF	podepsat
tzv.	tzv.	kA	tzv.
Smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
obraně	obrana	k1gFnSc6	obrana
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
pomoci	pomoc	k1gFnSc3	pomoc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
povolila	povolit	k5eAaPmAgFnS	povolit
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
umístění	umístění	k1gNnSc2	umístění
vojenských	vojenský	k2eAgFnPc2d1	vojenská
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
SSSR	SSSR	kA	SSSR
postupně	postupně	k6eAd1	postupně
anektoval	anektovat	k5eAaBmAgMnS	anektovat
pobaltské	pobaltský	k2eAgInPc4d1	pobaltský
státy	stát	k1gInPc4	stát
(	(	kIx(	(
<g/>
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
komunistických	komunistický	k2eAgFnPc2d1	komunistická
stran	strana	k1gFnPc2	strana
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
odstoupit	odstoupit	k5eAaPmF	odstoupit
Besarábii	Besarábie	k1gFnSc4	Besarábie
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
území	území	k1gNnSc6	území
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Moldavská	moldavský	k2eAgFnSc1d1	Moldavská
SSR	SSR	kA	SSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1939	[number]	k4	1939
SSSR	SSSR	kA	SSSR
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
Československé	československý	k2eAgNnSc4d1	Československé
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
svůj	svůj	k3xOyFgInSc4	svůj
odmítavý	odmítavý	k2eAgInSc4d1	odmítavý
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
<g/>
Gestapo	gestapo	k1gNnSc1	gestapo
a	a	k8xC	a
NKVD	NKVD	kA	NKVD
si	se	k3xPyFc3	se
začaly	začít	k5eAaPmAgInP	začít
vyměňovat	vyměňovat	k5eAaImF	vyměňovat
své	svůj	k3xOyFgMnPc4	svůj
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
věznicích	věznice	k1gFnPc6	věznice
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
i	i	k9	i
řada	řada	k1gFnSc1	řada
německých	německý	k2eAgMnPc2d1	německý
komunistů	komunista	k1gMnPc2	komunista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
SSSR	SSSR	kA	SSSR
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
před	před	k7c7	před
nacisty	nacista	k1gMnPc7	nacista
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
sovětsko-německého	sovětskoěmecký	k2eAgInSc2d1	sovětsko-německý
paktu	pakt	k1gInSc2	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
normalizovat	normalizovat	k5eAaBmF	normalizovat
i	i	k9	i
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgNnSc7	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
SSSR	SSSR	kA	SSSR
Pakt	pakt	k1gInSc1	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
mělo	mít	k5eAaImAgNnS	mít
otevřené	otevřený	k2eAgFnPc4d1	otevřená
dveře	dveře	k1gFnPc4	dveře
k	k	k7c3	k
invazi	invaze	k1gFnSc3	invaze
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
dohody	dohoda	k1gFnPc1	dohoda
mu	on	k3xPp3gNnSc3	on
zajistily	zajistit	k5eAaPmAgFnP	zajistit
přísun	přísun	k1gInSc4	přísun
strategických	strategický	k2eAgFnPc2d1	strategická
surovin	surovina	k1gFnPc2	surovina
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
zajistily	zajistit	k5eAaPmAgFnP	zajistit
dodávky	dodávka	k1gFnPc1	dodávka
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
byla	být	k5eAaImAgFnS	být
zapůjčena	zapůjčen	k2eAgFnSc1d1	zapůjčena
tajná	tajný	k2eAgFnSc1d1	tajná
ponorková	ponorkový	k2eAgFnSc1d1	ponorková
základna	základna	k1gFnSc1	základna
západně	západně	k6eAd1	západně
od	od	k7c2	od
Murmansku	Murmansk	k1gInSc2	Murmansk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
spolupráce	spolupráce	k1gFnSc1	spolupráce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
sovětský	sovětský	k2eAgInSc4d1	sovětský
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
Finsku	Finsko	k1gNnSc3	Finsko
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
zmrazila	zmrazit	k5eAaPmAgFnS	zmrazit
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
své	svůj	k3xOyFgInPc4	svůj
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
USA	USA	kA	USA
reagovaly	reagovat	k5eAaBmAgInP	reagovat
embargem	embargo	k1gNnSc7	embargo
na	na	k7c4	na
dodávky	dodávka	k1gFnPc4	dodávka
vojenského	vojenský	k2eAgInSc2d1	vojenský
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
nejdůležitějším	důležitý	k2eAgMnSc7d3	nejdůležitější
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
ochotné	ochotný	k2eAgNnSc1d1	ochotné
dodávat	dodávat	k5eAaImF	dodávat
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
průmyslová	průmyslový	k2eAgNnPc4d1	průmyslové
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
techniku	technika	k1gFnSc4	technika
do	do	k7c2	do
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
<g/>
Hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
vztahy	vztah	k1gInPc4	vztah
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
změnily	změnit	k5eAaPmAgInP	změnit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
úvěru	úvěr	k1gInSc6	úvěr
ze	z	k7c2	z
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
smluv	smlouva	k1gFnPc2	smlouva
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1940	[number]	k4	1940
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
se	se	k3xPyFc4	se
německo-sovětská	německoovětský	k2eAgFnSc1d1	německo-sovětská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
spolupráce	spolupráce	k1gFnSc1	spolupráce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
za	za	k7c4	za
období	období	k1gNnSc4	období
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
svého	svůj	k3xOyFgNnSc2	svůj
minima	minimum	k1gNnSc2	minimum
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
61	[number]	k4	61
milionů	milion	k4xCgInPc2	milion
říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
už	už	k6eAd1	už
činil	činit	k5eAaImAgInS	činit
celkový	celkový	k2eAgInSc1d1	celkový
obrat	obrat	k1gInSc1	obrat
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
a	a	k8xC	a
vrcholu	vrchol	k1gInSc2	vrchol
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
činila	činit	k5eAaImAgFnS	činit
cca	cca	kA	cca
425	[number]	k4	425
milionů	milion	k4xCgInPc2	milion
říšských	říšský	k2eAgFnPc2d1	říšská
marek	marka	k1gFnPc2	marka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německo	Německo	k1gNnSc1	Německo
dodávalo	dodávat	k5eAaImAgNnS	dodávat
převážně	převážně	k6eAd1	převážně
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
asi	asi	k9	asi
6500	[number]	k4	6500
obráběcích	obráběcí	k2eAgInPc2d1	obráběcí
strojů	stroj	k1gInPc2	stroj
pro	pro	k7c4	pro
sovětský	sovětský	k2eAgInSc4d1	sovětský
válečný	válečný	k2eAgInSc4d1	válečný
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
ale	ale	k8xC	ale
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
obdržel	obdržet	k5eAaPmAgInS	obdržet
ukázky	ukázka	k1gFnPc4	ukázka
nejnovějších	nový	k2eAgFnPc2d3	nejnovější
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
23	[number]	k4	23
vojenských	vojenský	k2eAgNnPc2d1	vojenské
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
lodních	lodní	k2eAgNnPc2d1	lodní
děl	dělo	k1gNnPc2	dělo
i	i	k9	i
celého	celý	k2eAgInSc2d1	celý
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
křižníku	křižník	k1gInSc2	křižník
Lützow	Lützow	k1gFnSc2	Lützow
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
surovin	surovina	k1gFnPc2	surovina
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
odříznuté	odříznutý	k2eAgFnSc6d1	odříznutá
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
britské	britský	k2eAgFnSc2d1	britská
blokády	blokáda	k1gFnSc2	blokáda
od	od	k7c2	od
světového	světový	k2eAgInSc2d1	světový
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
vývoz	vývoz	k1gInSc1	vývoz
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
kukuřici	kukuřice	k1gFnSc3	kukuřice
<g/>
,	,	kIx,	,
bavlnu	bavlna	k1gFnSc4	bavlna
<g/>
,	,	kIx,	,
manganovou	manganový	k2eAgFnSc4d1	manganová
rudu	ruda	k1gFnSc4	ruda
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc1	chrom
<g/>
,	,	kIx,	,
fosfát	fosfát	k1gInSc1	fosfát
<g/>
,	,	kIx,	,
azbest	azbest	k1gInSc1	azbest
a	a	k8xC	a
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
rovněž	rovněž	k9	rovněž
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
obilí	obilí	k1gNnSc4	obilí
a	a	k8xC	a
1	[number]	k4	1
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
také	také	k9	také
umožnil	umožnit	k5eAaPmAgInS	umožnit
Německé	německý	k2eAgFnSc3d1	německá
říši	říš	k1gFnSc3	říš
používat	používat	k5eAaImF	používat
transsibiřskou	transsibiřský	k2eAgFnSc4d1	Transsibiřská
magistrálu	magistrála	k1gFnSc4	magistrála
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
Středním	střední	k2eAgInSc7d1	střední
a	a	k8xC	a
Dálným	dálný	k2eAgInSc7d1	dálný
východem	východ	k1gInSc7	východ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
spolupráce	spolupráce	k1gFnSc1	spolupráce
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
strany	strana	k1gFnSc2	strana
zadrhla	zadrhnout	k5eAaPmAgFnS	zadrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sovětských	sovětský	k2eAgInPc6d1	sovětský
protestech	protest	k1gInPc6	protest
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
iniciativy	iniciativa	k1gFnSc2	iniciativa
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc1	Göring
a	a	k8xC	a
založil	založit	k5eAaPmAgInS	založit
Mezirezortní	mezirezortní	k2eAgFnSc4d1	mezirezortní
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
znovu	znovu	k6eAd1	znovu
rozběhl	rozběhnout	k5eAaPmAgMnS	rozběhnout
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
německém	německý	k2eAgInSc6d1	německý
útoku	útok	k1gInSc6	útok
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1940	[number]	k4	1940
se	se	k3xPyFc4	se
dodávky	dodávka	k1gFnPc4	dodávka
sovětských	sovětský	k2eAgFnPc2d1	sovětská
surovin	surovina	k1gFnPc2	surovina
rapidně	rapidně	k6eAd1	rapidně
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
prodával	prodávat	k5eAaImAgInS	prodávat
Německu	Německo	k1gNnSc3	Německo
palivo	palivo	k1gNnSc4	palivo
a	a	k8xC	a
kukuřici	kukuřice	k1gFnSc4	kukuřice
za	za	k7c4	za
dumpingové	dumpingový	k2eAgFnPc4d1	dumpingová
ceny	cena	k1gFnPc4	cena
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
německé	německý	k2eAgFnPc1d1	německá
dodávky	dodávka	k1gFnPc1	dodávka
vázly	váznout	k5eAaImAgFnP	váznout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
problémy	problém	k1gInPc4	problém
na	na	k7c6	na
sovětsko-německé	sovětskoěmecký	k2eAgFnSc6d1	sovětsko-německý
hranici	hranice	k1gFnSc6	hranice
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Reichsbahn	Reichsbahn	k1gInSc4	Reichsbahn
měly	mít	k5eAaImAgFnP	mít
velké	velký	k2eAgFnPc1d1	velká
potíže	potíž	k1gFnPc1	potíž
toto	tento	k3xDgNnSc4	tento
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
přeložit	přeložit	k5eAaPmF	přeložit
a	a	k8xC	a
odvézt	odvézt	k5eAaPmF	odvézt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
začal	začít	k5eAaPmAgMnS	začít
připravovat	připravovat	k5eAaImF	připravovat
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
ovšem	ovšem	k9	ovšem
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
hospodářství	hospodářství	k1gNnSc2	hospodářství
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
další	další	k2eAgFnSc4d1	další
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
nyní	nyní	k6eAd1	nyní
preferoval	preferovat	k5eAaImAgInS	preferovat
dodávky	dodávka	k1gFnPc4	dodávka
obráběcích	obráběcí	k2eAgInPc2d1	obráběcí
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Stalin	Stalin	k1gMnSc1	Stalin
byl	být	k5eAaImAgMnS	být
stále	stále	k6eAd1	stále
ochoten	ochoten	k2eAgMnSc1d1	ochoten
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
a	a	k8xC	a
Göring	Göring	k1gInSc4	Göring
marně	marně	k6eAd1	marně
snažili	snažit	k5eAaImAgMnP	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
Hitlera	Hitler	k1gMnSc4	Hitler
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
zrušil	zrušit	k5eAaPmAgInS	zrušit
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
odložil	odložit	k5eAaPmAgMnS	odložit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1941	[number]	k4	1941
dodal	dodat	k5eAaPmAgInS	dodat
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
5	[number]	k4	5
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
indického	indický	k2eAgInSc2d1	indický
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
,	,	kIx,	,
což	což	k9	což
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
vyřešilo	vyřešit	k5eAaPmAgNnS	vyřešit
kritický	kritický	k2eAgInSc4d1	kritický
nedostatek	nedostatek	k1gInSc4	nedostatek
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
potřebný	potřebný	k2eAgInSc4d1	potřebný
pro	pro	k7c4	pro
operaci	operace	k1gFnSc4	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
chystali	chystat	k5eAaImAgMnP	chystat
na	na	k7c6	na
východě	východ	k1gInSc6	východ
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
,	,	kIx,	,
přejížděly	přejíždět	k5eAaImAgInP	přejíždět
hranici	hranice	k1gFnSc4	hranice
vlaky	vlak	k1gInPc4	vlak
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
obilí	obilí	k1gNnSc6	obilí
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Objev	objev	k1gInSc1	objev
tajného	tajný	k2eAgInSc2d1	tajný
dodatku	dodatek	k1gInSc2	dodatek
===	===	k?	===
</s>
</p>
<p>
<s>
Německý	německý	k2eAgInSc1d1	německý
originál	originál	k1gInSc1	originál
tajných	tajný	k2eAgInPc2d1	tajný
protokolů	protokol	k1gInPc2	protokol
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zničen	zničit	k5eAaPmNgInS	zničit
při	při	k7c6	při
bombardování	bombardování	k1gNnSc6	bombardování
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
však	však	k9	však
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
nařídil	nařídit	k5eAaPmAgInS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ty	ten	k3xDgInPc4	ten
nejtajnější	tajný	k2eAgInPc4d3	nejtajnější
záznamy	záznam	k1gInPc4	záznam
německého	německý	k2eAgInSc2d1	německý
Zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
úřadu	úřad	k1gInSc2	úřad
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
asi	asi	k9	asi
9	[number]	k4	9
800	[number]	k4	800
stran	strana	k1gFnPc2	strana
byly	být	k5eAaImAgFnP	být
přefoceny	přefocen	k2eAgFnPc1d1	přefocen
na	na	k7c4	na
mikrofilm	mikrofilm	k1gInSc4	mikrofilm
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
různé	různý	k2eAgInPc4d1	různý
útvary	útvar	k1gInPc4	útvar
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc6	zahraničí
evakuovány	evakuovat	k5eAaBmNgInP	evakuovat
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
kopie	kopie	k1gFnPc1	kopie
mikrofilmů	mikrofilm	k1gInPc2	mikrofilm
svěřeny	svěřen	k2eAgFnPc1d1	svěřena
státnímu	státní	k2eAgMnSc3d1	státní
úředníkovi	úředník	k1gMnSc3	úředník
Karlu	Karel	k1gMnSc3	Karel
von	von	k1gInSc1	von
Loeschovi	Loesch	k1gMnSc3	Loesch
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
hlavního	hlavní	k2eAgMnSc4d1	hlavní
tlumočníka	tlumočník	k1gMnSc4	tlumočník
Paula	Paul	k1gMnSc4	Paul
Otto	Otto	k1gMnSc1	Otto
Schmidta	Schmidt	k1gMnSc4	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Von	von	k1gInSc1	von
Loesch	Loesch	k1gInSc1	Loesch
dostal	dostat	k5eAaPmAgInS	dostat
rozkaz	rozkaz	k1gInSc4	rozkaz
tajné	tajný	k2eAgInPc1d1	tajný
dokumenty	dokument	k1gInPc1	dokument
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
kovovou	kovový	k2eAgFnSc4d1	kovová
nádobu	nádoba	k1gFnSc4	nádoba
s	s	k7c7	s
mikrofilmy	mikrofilm	k1gInPc7	mikrofilm
zakopat	zakopat	k5eAaPmF	zakopat
jako	jako	k8xC	jako
svou	svůj	k3xOyFgFnSc4	svůj
osobní	osobní	k2eAgFnSc4d1	osobní
pojistku	pojistka	k1gFnSc4	pojistka
pro	pro	k7c4	pro
budoucnost	budoucnost	k1gFnSc4	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
oslovil	oslovit	k5eAaPmAgInS	oslovit
von	von	k1gInSc1	von
Loesch	Loesch	k1gInSc1	Loesch
britského	britský	k2eAgMnSc2d1	britský
poručíka	poručík	k1gMnSc2	poručík
Roberta	Robert	k1gMnSc2	Robert
C.	C.	kA	C.
Thomsona	Thomson	k1gMnSc2	Thomson
s	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
předání	předání	k1gNnSc4	předání
osobního	osobní	k2eAgInSc2d1	osobní
dopisu	dopis	k1gInSc2	dopis
Duncanovi	Duncan	k1gMnSc3	Duncan
Sandysovi	Sandys	k1gMnSc3	Sandys
<g/>
,	,	kIx,	,
Churchillovu	Churchillův	k2eAgMnSc3d1	Churchillův
zeti	zeť	k1gMnSc3	zeť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
von	von	k1gInSc1	von
Loesch	Loesch	k1gInSc1	Loesch
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
ví	vědět	k5eAaImIp3nS	vědět
o	o	k7c6	o
místě	místo	k1gNnSc6	místo
uložení	uložení	k1gNnPc2	uložení
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
očekává	očekávat	k5eAaImIp3nS	očekávat
privilegované	privilegovaný	k2eAgNnSc1d1	privilegované
zacházení	zacházení	k1gNnSc1	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Plukovník	plukovník	k1gMnSc1	plukovník
Thomson	Thomson	k1gMnSc1	Thomson
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
americký	americký	k2eAgInSc1d1	americký
protějšek	protějšek	k1gInSc1	protějšek
Ralph	Ralpha	k1gFnPc2	Ralpha
Collins	Collinsa	k1gFnPc2	Collinsa
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
s	s	k7c7	s
převezením	převezení	k1gNnSc7	převezení
von	von	k1gInSc1	von
Loesche	Loesche	k1gInSc1	Loesche
do	do	k7c2	do
Marburgu	Marburg	k1gInSc2	Marburg
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
okupační	okupační	k2eAgFnSc6d1	okupační
zóně	zóna	k1gFnSc6	zóna
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ten	ten	k3xDgMnSc1	ten
předá	předat	k5eAaPmIp3nS	předat
mikrofilmy	mikrofilm	k1gInPc4	mikrofilm
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dokumenty	dokument	k1gInPc4	dokument
na	na	k7c6	na
mikrofilmech	mikrofilm	k1gInPc6	mikrofilm
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
objeveny	objevit	k5eAaPmNgFnP	objevit
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1945	[number]	k4	1945
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Wendellem	Wendell	k1gInSc7	Wendell
B.	B.	kA	B.
Blanckem	Blancko	k1gNnSc7	Blancko
<g/>
,	,	kIx,	,
vedoucím	vedoucí	k1gMnSc7	vedoucí
speciálního	speciální	k2eAgInSc2d1	speciální
útvaru	útvar	k1gInSc2	útvar
"	"	kIx"	"
<g/>
Využití	využití	k1gNnSc1	využití
německých	německý	k2eAgInPc2d1	německý
archivů	archiv	k1gInPc2	archiv
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
EGA	EGA	kA	EGA
<g/>
)	)	kIx)	)
také	také	k9	také
obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
<g/>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
oficiálního	oficiální	k2eAgInSc2d1	oficiální
ministerského	ministerský	k2eAgInSc2d1	ministerský
sborníku	sborník	k1gInSc2	sborník
Nacisticko-sovětské	nacistickoovětský	k2eAgInPc1d1	nacisticko-sovětský
vztahy	vztah	k1gInPc1	vztah
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
publikovat	publikovat	k5eAaBmF	publikovat
klíčové	klíčový	k2eAgInPc4d1	klíčový
dokumenty	dokument	k1gInPc4	dokument
o	o	k7c6	o
německo-sovětských	německoovětský	k2eAgInPc6d1	německo-sovětský
vztazích	vztah	k1gInPc6	vztah
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
protokolu	protokol	k1gInSc2	protokol
<g/>
,	,	kIx,	,
padlo	padnout	k5eAaPmAgNnS	padnout
již	již	k6eAd1	již
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1947	[number]	k4	1947
prezident	prezident	k1gMnSc1	prezident
Truman	Truman	k1gMnSc1	Truman
publikaci	publikace	k1gFnSc4	publikace
osobně	osobně	k6eAd1	osobně
schválil	schválit	k5eAaPmAgInS	schválit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
konferenci	konference	k1gFnSc3	konference
ministrů	ministr	k1gMnPc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
naplánována	naplánovat	k5eAaBmNgFnS	naplánovat
na	na	k7c4	na
prosinec	prosinec	k1gInSc4	prosinec
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
vydání	vydání	k1gNnSc1	vydání
pozdrženo	pozdržet	k5eAaPmNgNnS	pozdržet
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
jednání	jednání	k1gNnSc1	jednání
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
konferenci	konference	k1gFnSc6	konference
se	se	k3xPyFc4	se
z	z	k7c2	z
amerického	americký	k2eAgNnSc2d1	americké
hlediska	hledisko	k1gNnSc2	hledisko
nejevila	jevit	k5eNaImAgFnS	jevit
konstruktivně	konstruktivně	k6eAd1	konstruktivně
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
sborník	sborník	k1gInSc1	sborník
odeslán	odeslat	k5eAaPmNgInS	odeslat
do	do	k7c2	do
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnSc1	publikace
protokolů	protokol	k1gInPc2	protokol
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
titulků	titulek	k1gInPc2	titulek
novin	novina	k1gFnPc2	novina
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kniha	kniha	k1gFnSc1	kniha
Falsifikátoři	falsifikátor	k1gMnPc1	falsifikátor
dějin	dějiny	k1gFnPc2	dějiny
a	a	k8xC	a
popírání	popírání	k1gNnSc2	popírání
existence	existence	k1gFnSc2	existence
tajných	tajný	k2eAgInPc2d1	tajný
dodatků	dodatek	k1gInPc2	dodatek
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
kopie	kopie	k1gFnSc2	kopie
v	v	k7c6	v
západních	západní	k2eAgNnPc6d1	západní
médiích	médium	k1gNnPc6	médium
bylo	být	k5eAaImAgNnS	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
politikou	politika	k1gFnSc7	politika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
popírání	popírání	k1gNnSc2	popírání
existence	existence	k1gFnSc2	existence
tajného	tajný	k2eAgInSc2d1	tajný
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
tajného	tajný	k2eAgInSc2d1	tajný
protokolu	protokol	k1gInSc2	protokol
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přiznána	přiznán	k2eAgFnSc1d1	přiznána
až	až	k9	až
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Molotov	Molotov	k1gInSc4	Molotov
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
signatářů	signatář	k1gMnPc2	signatář
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
kategoricky	kategoricky	k6eAd1	kategoricky
odmítal	odmítat	k5eAaImAgMnS	odmítat
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
neuznávala	uznávat	k5eNaImAgFnS	uznávat
existenci	existence	k1gFnSc4	existence
tajného	tajný	k2eAgInSc2d1	tajný
protokolu	protokol	k1gInSc2	protokol
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
destalinizaci	destalinizace	k1gFnSc3	destalinizace
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
publikaci	publikace	k1gFnSc6	publikace
tajných	tajný	k2eAgInPc2d1	tajný
protokolů	protokol	k1gInPc2	protokol
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
tajných	tajný	k2eAgFnPc2d1	tajná
německo-sovětských	německoovětský	k2eAgFnPc2d1	německo-sovětská
smluv	smlouva	k1gFnPc2	smlouva
a	a	k8xC	a
dokumentů	dokument	k1gInPc2	dokument
vydalo	vydat	k5eAaPmAgNnS	vydat
sovětské	sovětský	k2eAgNnSc1d1	sovětské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
knihu	kniha	k1gFnSc4	kniha
Falzifikátoři	falzifikátor	k1gMnPc1	falzifikátor
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
podpisování	podpisování	k1gNnSc2	podpisování
Paktu	pakt	k1gInSc2	pakt
Stalin	Stalin	k1gMnSc1	Stalin
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Hitlerův	Hitlerův	k2eAgInSc4d1	Hitlerův
požadavek	požadavek	k1gInSc4	požadavek
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
rozdělení	rozdělení	k1gNnSc4	rozdělení
světa	svět	k1gInSc2	svět
na	na	k7c4	na
sféry	sféra	k1gFnPc4	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
zmínky	zmínka	k1gFnSc2	zmínka
o	o	k7c6	o
jednáních	jednání	k1gNnPc6	jednání
o	o	k7c6	o
vstupu	vstup	k1gInSc6	vstup
SSSR	SSSR	kA	SSSR
do	do	k7c2	do
Osy	osa	k1gFnSc2	osa
zcela	zcela	k6eAd1	zcela
opomíjí	opomíjet	k5eAaImIp3nS	opomíjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
také	také	k9	také
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
tajnou	tajný	k2eAgFnSc7d1	tajná
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Západem	západ	k1gInSc7	západ
a	a	k8xC	a
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
fází	fáze	k1gFnSc7	fáze
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
politice	politika	k1gFnSc6	politika
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
přetrvávala	přetrvávat	k5eAaImAgFnS	přetrvávat
v	v	k7c6	v
historických	historický	k2eAgNnPc6d1	historické
studiích	studio	k1gNnPc6	studio
<g/>
,	,	kIx,	,
oficiálních	oficiální	k2eAgNnPc6d1	oficiální
vyjádřeních	vyjádření	k1gNnPc6	vyjádření
<g/>
,	,	kIx,	,
memoárech	memoáry	k1gInPc6	memoáry
a	a	k8xC	a
učebnicích	učebnice	k1gFnPc6	učebnice
vydávaných	vydávaný	k2eAgFnPc6d1	vydávaná
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
celém	celý	k2eAgInSc6d1	celý
východním	východní	k2eAgInSc6d1	východní
bloku	blok	k1gInSc6	blok
prakticky	prakticky	k6eAd1	prakticky
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
byla	být	k5eAaImAgFnS	být
oficiální	oficiální	k2eAgFnSc7d1	oficiální
politikou	politika	k1gFnSc7	politika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
popírat	popírat	k5eAaImF	popírat
existenci	existence	k1gFnSc4	existence
tajného	tajný	k2eAgInSc2d1	tajný
protokolu	protokol	k1gInSc2	protokol
sovětsko-německého	sovětskoěmecký	k2eAgInSc2d1	sovětsko-německý
paktu	pakt	k1gInSc2	pakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Michail	Michail	k1gMnSc1	Michail
Gorbačov	Gorbačov	k1gInSc1	Gorbačov
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Jakovlev	Jakovlev	k1gMnSc1	Jakovlev
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
komisi	komise	k1gFnSc4	komise
zkoumající	zkoumající	k2eAgFnSc4d1	zkoumající
existenci	existence	k1gFnSc4	existence
takového	takový	k3xDgInSc2	takový
protokolu	protokol	k1gInSc2	protokol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1989	[number]	k4	1989
komise	komise	k1gFnSc1	komise
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
protokol	protokol	k1gInSc1	protokol
existoval	existovat	k5eAaImAgInS	existovat
a	a	k8xC	a
svá	svůj	k3xOyFgNnPc4	svůj
zjištění	zjištění	k1gNnPc4	zjištění
přednesli	přednést	k5eAaPmAgMnP	přednést
na	na	k7c6	na
Kongresu	kongres	k1gInSc6	kongres
lidových	lidový	k2eAgMnPc2d1	lidový
zástupců	zástupce	k1gMnPc2	zástupce
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
schválil	schválit	k5eAaPmAgInS	schválit
prohlášení	prohlášení	k1gNnSc4	prohlášení
potvrzující	potvrzující	k2eAgFnSc4d1	potvrzující
existenci	existence	k1gFnSc4	existence
tajných	tajný	k2eAgInPc2d1	tajný
protokolů	protokol	k1gInPc2	protokol
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc1	jejich
odsouzení	odsouzený	k2eAgMnPc1d1	odsouzený
a	a	k8xC	a
vypovězení	vypovězený	k2eAgMnPc1d1	vypovězený
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
státy	stát	k1gInPc1	stát
smluvních	smluvní	k2eAgFnPc2d1	smluvní
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
prohlásily	prohlásit	k5eAaPmAgInP	prohlásit
tajné	tajný	k2eAgInPc1d1	tajný
protokoly	protokol	k1gInPc1	protokol
neplatné	platný	k2eNgInPc1d1	neplatný
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
podepsány	podepsán	k2eAgFnPc1d1	podepsána
<g/>
.	.	kIx.	.
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc1	Německo
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1989	[number]	k4	1989
a	a	k8xC	a
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
po	po	k7c4	po
přezkoumání	přezkoumání	k1gNnSc4	přezkoumání
mikrofilmu	mikrofilm	k1gInSc2	mikrofilm
německých	německý	k2eAgMnPc2d1	německý
originálů	originál	k1gMnPc2	originál
<g/>
.	.	kIx.	.
<g/>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
kopie	kopie	k1gFnSc1	kopie
původního	původní	k2eAgInSc2d1	původní
dokumentu	dokument	k1gInSc2	dokument
byla	být	k5eAaImAgFnS	být
odtajněna	odtajnit	k5eAaPmNgFnS	odtajnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
a	a	k8xC	a
publikována	publikovat	k5eAaBmNgFnS	publikovat
ve	v	k7c6	v
vědeckém	vědecký	k2eAgInSc6d1	vědecký
časopise	časopis	k1gInSc6	časopis
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Molotov	Molotov	k1gInSc1	Molotov
<g/>
–	–	k?	–
<g/>
Ribbentrop	Ribbentrop	k1gInSc1	Ribbentrop
Pact	Pact	k1gInSc1	Pact
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
Toman	Toman	k1gMnSc1	Toman
<g/>
:	:	kIx,	:
Pakty	pakt	k1gInPc1	pakt
Stalina	Stalin	k1gMnSc4	Stalin
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
;	;	kIx,	;
Česká	český	k2eAgFnSc1d1	Česká
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-206-0209-7	[number]	k4	80-206-0209-7
(	(	kIx(	(
<g/>
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85281-06-6	[number]	k4	80-85281-06-6
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
expedice	expedice	k1gFnSc1	expedice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EBERLE	EBERLE	kA	EBERLE
<g/>
,	,	kIx,	,
Henrik	Henrik	k1gMnSc1	Henrik
–	–	k?	–
UHL	UHL	kA	UHL
<g/>
,	,	kIx,	,
Mathias	Mathias	k1gInSc1	Mathias
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Akta	akta	k1gNnPc4	akta
Hitler	Hitler	k1gMnSc1	Hitler
–	–	k?	–
tajná	tajný	k2eAgFnSc1d1	tajná
složka	složka	k1gFnSc1	složka
NKVD	NKVD	kA	NKVD
pro	pro	k7c4	pro
Josifa	Josif	k1gMnSc4	Josif
V.	V.	kA	V.
Stalina	Stalin	k1gMnSc4	Stalin
<g/>
.	.	kIx.	.
</s>
<s>
Ikar	Ikar	k1gInSc1	Ikar
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Norman	Norman	k1gMnSc1	Norman
Davies	Davies	k1gMnSc1	Davies
<g/>
:	:	kIx,	:
Evropa	Evropa	k1gFnSc1	Evropa
–	–	k?	–
dějiny	dějiny	k1gFnPc4	dějiny
jednoho	jeden	k4xCgInSc2	jeden
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NÁLEVKA	nálevka	k1gFnSc1	nálevka
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
TRITON	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7387	[number]	k4	7387
<g/>
-	-	kIx~	-
<g/>
669	[number]	k4	669
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Tajný	tajný	k1gMnSc1	tajný
dodatečný	dodatečný	k2eAgInSc4d1	dodatečný
protokol	protokol	k1gInSc4	protokol
ke	k	k7c3	k
smlouvě	smlouva	k1gFnSc3	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
mezi	mezi	k7c7	mezi
Německou	německý	k2eAgFnSc7d1	německá
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Veber	Veber	k1gMnSc1	Veber
<g/>
:	:	kIx,	:
Pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Sovětsko-nacistická	sovětskoacistický	k2eAgNnPc1d1	sovětsko-nacistický
jednání	jednání	k1gNnPc1	jednání
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
druhé	druhý	k4xOgFnPc4	druhý
světové	světový	k2eAgFnPc4d1	světová
války	válka	k1gFnPc4	válka
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
Paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
včetně	včetně	k7c2	včetně
tajného	tajný	k2eAgInSc2d1	tajný
dodatku	dodatek	k1gInSc2	dodatek
</s>
</p>
<p>
<s>
Pakt	pakt	k1gInSc1	pakt
Molotov-Ribbentrop	Molotov-Ribbentrop	k1gInSc1	Molotov-Ribbentrop
<g/>
:	:	kIx,	:
Sláva	Sláva	k1gFnSc1	Sláva
a	a	k8xC	a
pád	pád	k1gInSc1	pád
reálpolitiky	reálpolitika	k1gFnSc2	reálpolitika
</s>
</p>
