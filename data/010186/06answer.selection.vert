<s>
Pakt	pakt	k1gInSc1	pakt
Ribbentrop-Molotov	Ribbentrop-Molotovo	k1gNnPc2	Ribbentrop-Molotovo
je	být	k5eAaImIp3nS	být
smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
mezi	mezi	k7c7	mezi
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Svazem	svaz	k1gInSc7	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
