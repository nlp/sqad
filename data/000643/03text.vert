<s>
Nowa	Now	k2eAgFnSc1d1	Nowa
Huta	Huta	k1gFnSc1	Huta
(	(	kIx(	(
[	[	kIx(	[
<g/>
'	'	kIx"	'
<g/>
nɔ	nɔ	k?	nɔ
'	'	kIx"	'
<g/>
xuta	xuta	k1gMnSc1	xuta
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Nová	nový	k2eAgFnSc1d1	nová
Huť	huť	k1gFnSc1	huť
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
Krakova	krakův	k2eAgFnSc1d1	Krakova
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
projektovaná	projektovaný	k2eAgFnSc1d1	projektovaná
jako	jako	k8xC	jako
samostatné	samostatný	k2eAgNnSc1d1	samostatné
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
administrativně	administrativně	k6eAd1	administrativně
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
Krakov	Krakov	k1gInSc4	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
asi	asi	k9	asi
220	[number]	k4	220
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozloze	rozloha	k1gFnSc6	rozloha
110,7	[number]	k4	110,7
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
XIV	XIV	kA	XIV
Czyżyny	Czyżyna	k1gMnSc2	Czyżyna
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
XV	XV	kA	XV
Mistrzejowice	Mistrzejowice	k1gFnSc1	Mistrzejowice
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
XVI	XVI	kA	XVI
Bieńczyce	Bieńczyce	k1gFnSc1	Bieńczyce
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
XVII	XVII	kA	XVII
Wzgórza	Wzgórza	k1gFnSc1	Wzgórza
Krzesławickie	Krzesławickie	k1gFnSc2	Krzesławickie
Městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
XVIII	XVIII	kA	XVIII
Nowa	Nowa	k1gFnSc1	Nowa
Huta	Huta	k1gFnSc1	Huta
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
průmyslovým	průmyslový	k2eAgInSc7d1	průmyslový
podnikem	podnik	k1gInSc7	podnik
Nowe	Nowe	k1gFnSc1	Nowe
Huty	Huty	k1gInPc1	Huty
je	být	k5eAaImIp3nS	být
metalurgický	metalurgický	k2eAgInSc1d1	metalurgický
závod	závod	k1gInSc1	závod
ArcelorMittal	ArcelorMittal	k1gMnSc1	ArcelorMittal
Poland	Polanda	k1gFnPc2	Polanda
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990-2004	[number]	k4	1990-2004
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
huť	huť	k1gFnSc1	huť
Tadeusze	Tadeusze	k1gFnSc1	Tadeusze
Sendzimira	Sendzimira	k1gFnSc1	Sendzimira
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954-1990	[number]	k4	1954-1990
jako	jako	k8xC	jako
huť	huť	k1gFnSc1	huť
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Iljiče	Iljič	k1gMnSc2	Iljič
Lenina	Lenin	k1gMnSc2	Lenin
<g/>
.	.	kIx.	.
</s>
