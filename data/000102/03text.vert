<s>
Dynamit	dynamit	k1gInSc1	dynamit
je	být	k5eAaImIp3nS	být
výbušnina	výbušnina	k1gFnSc1	výbušnina
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
explozivním	explozivní	k2eAgInSc6d1	explozivní
potenciálu	potenciál	k1gInSc6	potenciál
nitroglycerínu	nitroglycerín	k1gInSc2	nitroglycerín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
absorbován	absorbován	k2eAgInSc1d1	absorbován
na	na	k7c6	na
křemelině	křemelina	k1gFnSc6	křemelina
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
klasickému	klasický	k2eAgInSc3d1	klasický
tekutému	tekutý	k2eAgInSc3d1	tekutý
nitroglycerínu	nitroglycerín	k1gInSc3	nitroglycerín
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
vyšší	vysoký	k2eAgFnSc7d2	vyšší
stabilitou	stabilita	k1gFnSc7	stabilita
oproti	oproti	k7c3	oproti
tlaku	tlak	k1gInSc3	tlak
nebo	nebo	k8xC	nebo
nárazu	náraz	k1gInSc3	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Dynamit	dynamit	k1gInSc4	dynamit
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
Alfréd	Alfréd	k1gMnSc1	Alfréd
Nobel	Nobel	k1gMnSc1	Nobel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1866	[number]	k4	1866
<g/>
,	,	kIx,	,
patent	patent	k1gInSc1	patent
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
původně	původně	k6eAd1	původně
Nobelem	Nobel	k1gMnSc7	Nobel
navrženého	navržený	k2eAgInSc2d1	navržený
názvu	název	k1gInSc2	název
Kieselgur-dynamite	Kieselgurynamit	k1gInSc5	Kieselgur-dynamit
(	(	kIx(	(
<g/>
Kieselgur	Kieselgur	k1gMnSc1	Kieselgur
je	být	k5eAaImIp3nS	být
německy	německy	k6eAd1	německy
křemelina	křemelina	k1gFnSc1	křemelina
<g/>
,	,	kIx,	,
dynamite	dynamit	k1gInSc5	dynamit
řecky	řecky	k6eAd1	řecky
plná	plný	k2eAgFnSc1d1	plná
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
také	také	k9	také
zpočátku	zpočátku	k6eAd1	zpočátku
dodáván	dodávat	k5eAaImNgInS	dodávat
jako	jako	k8xC	jako
Nobelův	Nobelův	k2eAgInSc1d1	Nobelův
výbušný	výbušný	k2eAgInSc1d1	výbušný
prach	prach	k1gInSc1	prach
<g/>
.	.	kIx.	.
</s>
<s>
Dynamit	dynamit	k1gInSc1	dynamit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
dílů	díl	k1gInPc2	díl
nitroglycerínu	nitroglycerín	k1gInSc2	nitroglycerín
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgInSc2	jeden
dílu	díl	k1gInSc2	díl
křemeliny	křemelina	k1gFnSc2	křemelina
a	a	k8xC	a
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
jedlé	jedlý	k2eAgFnSc2d1	jedlá
sody	soda	k1gFnSc2	soda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
je	být	k5eAaImIp3nS	být
tvarována	tvarovat	k5eAaImNgFnS	tvarovat
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
tyček	tyčka	k1gFnPc2	tyčka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
křemelina	křemelina	k1gFnSc1	křemelina
(	(	kIx(	(
<g/>
infusoriová	infusoriový	k2eAgFnSc1d1	infusoriový
hlinka	hlinka	k1gFnSc1	hlinka
<g/>
)	)	kIx)	)
dusičnanem	dusičnan	k1gInSc7	dusičnan
draselným	draselný	k2eAgNnSc7d1	draselné
(	(	kIx(	(
<g/>
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
dusičnanem	dusičnan	k1gInSc7	dusičnan
sodným	sodný	k2eAgInSc7d1	sodný
(	(	kIx(	(
<g/>
NaNO	NaNO	k1gFnPc2	NaNO
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
a	a	k8xC	a
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
nasákavost	nasákavost	k1gFnSc4	nasákavost
se	se	k3xPyFc4	se
přidávaly	přidávat	k5eAaImAgFnP	přidávat
suché	suchý	k2eAgFnPc1d1	suchá
dřevité	dřevitý	k2eAgFnPc1d1	dřevitá
piliny	pilina	k1gFnPc1	pilina
<g/>
.	.	kIx.	.
</s>
<s>
Dynamit	dynamit	k1gInSc1	dynamit
mrzne	mrznout	k5eAaImIp3nS	mrznout
při	při	k7c6	při
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
používal	používat	k5eAaImAgMnS	používat
v	v	k7c6	v
chladném	chladný	k2eAgNnSc6d1	chladné
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
zimní	zimní	k2eAgInSc1d1	zimní
dynamit	dynamit	k1gInSc1	dynamit
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
IZ	IZ	kA	IZ
dynamit	dynamit	k1gInSc1	dynamit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
nemrznoucí	mrznoucí	k2eNgFnPc4d1	nemrznoucí
příměsi	příměs	k1gFnPc4	příměs
jako	jako	k8xC	jako
di-nitro-glycerin	diitrolycerin	k1gInSc4	di-nitro-glycerin
nebo	nebo	k8xC	nebo
trinitrotoluen	trinitrotoluen	k1gInSc4	trinitrotoluen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výbuchu	výbuch	k1gInSc3	výbuch
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
rozbuška	rozbuška	k1gFnSc1	rozbuška
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
dynamit	dynamit	k1gInSc1	dynamit
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stálý	stálý	k2eAgInSc1d1	stálý
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
nitroglycerínu	nitroglycerín	k1gInSc2	nitroglycerín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vybuchuje	vybuchovat	k5eAaImIp3nS	vybuchovat
již	již	k6eAd1	již
při	při	k7c6	při
malém	malý	k2eAgInSc6d1	malý
nárazu	náraz	k1gInSc6	náraz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
dynamitu	dynamit	k1gInSc2	dynamit
objevit	objevit	k5eAaPmF	objevit
kapky	kapka	k1gFnPc4	kapka
nitroglycerínu	nitroglycerín	k1gInSc2	nitroglycerín
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	s	k7c7	s
starším	starý	k2eAgInSc7d2	starší
dynamitem	dynamit	k1gInSc7	dynamit
zacházet	zacházet	k5eAaImF	zacházet
extrémně	extrémně	k6eAd1	extrémně
opatrně	opatrně	k6eAd1	opatrně
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
podobně	podobně	k6eAd1	podobně
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
sebemenší	sebemenší	k2eAgInSc4d1	sebemenší
náraz	náraz	k1gInSc4	náraz
jako	jako	k8xC	jako
samotný	samotný	k2eAgInSc4d1	samotný
nitroglycerín	nitroglycerín	k1gInSc4	nitroglycerín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
první	první	k4xOgFnSc1	první
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
dynamit	dynamit	k1gInSc4	dynamit
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
v	v	k7c6	v
Zámecké	zámecký	k2eAgFnSc6d1	zámecká
rokli	rokle	k1gFnSc6	rokle
poblíž	poblíž	k7c2	poblíž
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dynamit	dynamit	k1gInSc1	dynamit
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dynamit	dynamit	k1gInSc1	dynamit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
