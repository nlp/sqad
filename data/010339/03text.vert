<p>
<s>
První	první	k4xOgFnSc1	první
vláda	vláda	k1gFnSc1	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
sestavená	sestavený	k2eAgFnSc1d1	sestavená
Mirkem	Mirek	k1gMnSc7	Mirek
Topolánkem	Topolánek	k1gMnSc7	Topolánek
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2006	[number]	k4	2006
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jednobarevná	jednobarevný	k2eAgFnSc1d1	jednobarevná
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
ODS	ODS	kA	ODS
a	a	k8xC	a
nestraníků	nestraník	k1gMnPc2	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
2006	[number]	k4	2006
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nezískala	získat	k5eNaPmAgFnS	získat
však	však	k9	však
důvěru	důvěra	k1gFnSc4	důvěra
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mirka	Mirek	k1gMnSc4	Mirek
Topolánka	Topolánek	k1gMnSc4	Topolánek
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
premiérem	premiér	k1gMnSc7	premiér
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
podle	podle	k7c2	podle
Topolánkova	Topolánkův	k2eAgInSc2d1	Topolánkův
návrhu	návrh	k1gInSc2	návrh
prezident	prezident	k1gMnSc1	prezident
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hlasování	hlasování	k1gNnSc6	hlasování
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
však	však	k9	však
vláda	vláda	k1gFnSc1	vláda
nezískala	získat	k5eNaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
pouze	pouze	k6eAd1	pouze
96	[number]	k4	96
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
99	[number]	k4	99
proti	proti	k7c3	proti
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
demisi	demise	k1gFnSc6	demise
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
čl	čl	kA	čl
<g/>
.	.	kIx.	.
62	[number]	k4	62
písm	písmo	k1gNnPc2	písmo
<g/>
.	.	kIx.	.
d	d	k?	d
<g/>
)	)	kIx)	)
Ústavy	ústava	k1gFnSc2	ústava
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
dočasně	dočasně	k6eAd1	dočasně
pověřena	pověřen	k2eAgFnSc1d1	pověřena
vykonáváním	vykonávání	k1gNnSc7	vykonávání
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
opět	opět	k6eAd1	opět
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
sestavovat	sestavovat	k5eAaImF	sestavovat
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
zástupců	zástupce	k1gMnPc2	zástupce
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
Strany	strana	k1gFnSc2	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legitimita	legitimita	k1gFnSc1	legitimita
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Legitimita	legitimita	k1gFnSc1	legitimita
vlády	vláda	k1gFnSc2	vláda
od	od	k7c2	od
občanů	občan	k1gMnPc2	občan
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
vládnutí	vládnutí	k1gNnSc2	vládnutí
–	–	k?	–
pozdější	pozdní	k2eAgFnSc2d2	pozdější
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
poslaneckých	poslanecký	k2eAgInPc6d1	poslanecký
klubech	klub	k1gInPc6	klub
nejsou	být	k5eNaImIp3nP	být
zohledněny	zohledněn	k2eAgFnPc1d1	zohledněna
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ministrů	ministr	k1gMnPc2	ministr
vlád	vláda	k1gFnPc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vláda	vláda	k1gFnSc1	vláda
Mirka	Mirka	k1gFnSc1	Mirka
Topolánka	Topolánka	k1gFnSc1	Topolánka
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Přehled	přehled	k1gInSc1	přehled
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
-	-	kIx~	-
9	[number]	k4	9
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
na	na	k7c6	na
Vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c6	o
důvěře	důvěra	k1gFnSc6	důvěra
vládě	vláda	k1gFnSc6	vláda
premiéra	premiéra	k1gFnSc1	premiéra
Mirka	Mirka	k1gFnSc1	Mirka
Topolánka	Topolánka	k1gFnSc1	Topolánka
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
-	-	kIx~	-
záznam	záznam	k1gInSc1	záznam
přímého	přímý	k2eAgInSc2d1	přímý
přenosu	přenos	k1gInSc2	přenos
</s>
</p>
