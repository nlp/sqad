<s>
Biskupská	biskupský	k2eAgFnSc1d1	biskupská
inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k8xC	jako
nedostačující	dostačující	k2eNgInSc1d1	nedostačující
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
inkviziční	inkviziční	k2eAgNnSc4d1	inkviziční
řízení	řízení	k1gNnSc4	řízení
profesionalizovat	profesionalizovat	k5eAaImF	profesionalizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
založil	založit	k5eAaPmAgMnS	založit
papežskou	papežský	k2eAgFnSc4d1	Papežská
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
.	.	kIx.	.
</s>
