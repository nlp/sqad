<p>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
inquisitio	inquisitio	k1gNnSc4	inquisitio
<g/>
;	;	kIx,	;
z	z	k7c2	z
inquaerere	inquaerrat	k5eAaPmIp3nS	inquaerrat
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
právní	právní	k2eAgFnSc1d1	právní
instituce	instituce	k1gFnSc1	instituce
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
či	či	k8xC	či
španělských	španělský	k2eAgMnPc2d1	španělský
a	a	k8xC	a
portugalských	portugalský	k2eAgMnPc2d1	portugalský
panovníků	panovník	k1gMnPc2	panovník
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
vypořádávat	vypořádávat	k5eAaImF	vypořádávat
s	s	k7c7	s
herezí	hereze	k1gFnSc7	hereze
(	(	kIx(	(
<g/>
kacířstvím	kacířství	k1gNnSc7	kacířství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
inkvizice	inkvizice	k1gFnSc2	inkvizice
ale	ale	k8xC	ale
označuje	označovat	k5eAaImIp3nS	označovat
také	také	k9	také
i	i	k9	i
samotné	samotný	k2eAgNnSc1d1	samotné
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
v	v	k7c6	v
otázkách	otázka	k1gFnPc6	otázka
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
mravů	mrav	k1gInPc2	mrav
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
příslušná	příslušný	k2eAgFnSc1d1	příslušná
církevní	církevní	k2eAgFnSc1d1	církevní
autorita	autorita	k1gFnSc1	autorita
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
biskup	biskup	k1gMnSc1	biskup
<g/>
,	,	kIx,	,
osoba	osoba	k1gFnSc1	osoba
jím	jíst	k5eAaImIp1nS	jíst
k	k	k7c3	k
vyšetřování	vyšetřování	k1gNnSc3	vyšetřování
pověřená	pověřený	k2eAgFnSc1d1	pověřená
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
inkviziční	inkviziční	k2eAgFnPc4d1	inkviziční
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Středověká	středověký	k2eAgFnSc1d1	středověká
inkvizice	inkvizice	k1gFnSc1	inkvizice
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
středověké	středověký	k2eAgFnSc2d1	středověká
inkvizice	inkvizice	k1gFnSc2	inkvizice
byl	být	k5eAaImAgInS	být
podmíněn	podmínit	k5eAaPmNgInS	podmínit
sepětím	sepětí	k1gNnSc7	sepětí
dvou	dva	k4xCgFnPc2	dva
závažných	závažný	k2eAgFnPc2d1	závažná
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
rostoucí	rostoucí	k2eAgInSc1d1	rostoucí
vliv	vliv	k1gInSc1	vliv
heretických	heretický	k2eAgNnPc2d1	heretické
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
katarů	katar	k1gInPc2	katar
(	(	kIx(	(
<g/>
od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
a	a	k8xC	a
valdenských	valdenští	k1gMnPc2	valdenští
(	(	kIx(	(
<g/>
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
některých	některý	k3yIgInPc2	některý
členů	člen	k1gInPc2	člen
komunit	komunita	k1gFnPc2	komunita
beghardů	beghard	k1gMnPc2	beghard
a	a	k8xC	a
bekyní	bekyně	k1gFnPc2	bekyně
<g/>
.	.	kIx.	.
</s>
<s>
Církev	církev	k1gFnSc1	církev
viděla	vidět	k5eAaImAgFnS	vidět
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
hnutích	hnutí	k1gNnPc6	hnutí
ohrožení	ohrožení	k1gNnSc2	ohrožení
křesťanského	křesťanský	k2eAgNnSc2d1	křesťanské
učení	učení	k1gNnSc2	učení
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dosud	dosud	k6eAd1	dosud
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
církvi	církev	k1gFnSc6	církev
svým	svůj	k3xOyFgInSc7	svůj
rozsahem	rozsah	k1gInSc7	rozsah
a	a	k8xC	a
intenzitou	intenzita	k1gFnSc7	intenzita
nebývalé	nebývalý	k2eAgNnSc1d1	nebývalý
propuknutí	propuknutí	k1gNnSc1	propuknutí
hereze	hereze	k1gFnSc2	hereze
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
navíc	navíc	k6eAd1	navíc
sešlo	sejít	k5eAaPmAgNnS	sejít
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
objevem	objev	k1gInSc7	objev
znění	znění	k1gNnSc2	znění
antického	antický	k2eAgNnSc2d1	antické
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
právo	právo	k1gNnSc1	právo
herezi	hereze	k1gFnSc4	hereze
trestalo	trestat	k5eAaImAgNnS	trestat
jako	jako	k9	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvážnějších	vážní	k2eAgInPc2d3	nejvážnější
kriminálních	kriminální	k2eAgInPc2d1	kriminální
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
středověkým	středověký	k2eAgNnSc7d1	středověké
elitám	elita	k1gFnPc3	elita
nabídlo	nabídnout	k5eAaPmAgNnS	nabídnout
praktické	praktický	k2eAgNnSc4d1	praktické
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
herezemi	hereze	k1gFnPc7	hereze
vypořádat	vypořádat	k5eAaPmF	vypořádat
<g/>
.	.	kIx.	.
</s>
<s>
Starořímské	starořímský	k2eAgNnSc1d1	starořímské
pojetí	pojetí	k1gNnSc1	pojetí
heretika	heretik	k1gMnSc2	heretik
jakožto	jakožto	k8xS	jakožto
kriminálníka	kriminálník	k1gMnSc2	kriminálník
pak	pak	k6eAd1	pak
podpořil	podpořit	k5eAaPmAgMnS	podpořit
i	i	k9	i
vlivný	vlivný	k2eAgMnSc1d1	vlivný
středověký	středověký	k2eAgMnSc1d1	středověký
myslitel	myslitel	k1gMnSc1	myslitel
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
,	,	kIx,	,
když	když	k8xS	když
srovnával	srovnávat	k5eAaImAgMnS	srovnávat
kacíře	kacíř	k1gMnSc4	kacíř
s	s	k7c7	s
vrahy	vrah	k1gMnPc4	vrah
<g/>
:	:	kIx,	:
vrah	vrah	k1gMnSc1	vrah
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
kacíře	kacíř	k1gMnSc2	kacíř
zabijí	zabít	k5eAaPmIp3nP	zabít
pouze	pouze	k6eAd1	pouze
tělo	tělo	k1gNnSc4	tělo
(	(	kIx(	(
<g/>
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
tedy	tedy	k9	tedy
druhé	druhý	k4xOgNnSc1	druhý
pouze	pouze	k6eAd1	pouze
pozemského	pozemský	k2eAgInSc2d1	pozemský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stejně	stejně	k6eAd1	stejně
jednou	jednou	k6eAd1	jednou
musí	muset	k5eAaImIp3nS	muset
skončit	skončit	k5eAaPmF	skončit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kacíř	kacíř	k1gMnSc1	kacíř
však	však	k9	však
svým	svůj	k3xOyFgInSc7	svůj
svodem	svod	k1gInSc7	svod
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
duši	duše	k1gFnSc4	duše
<g/>
,	,	kIx,	,
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
tak	tak	k9	tak
druhé	druhý	k4xOgNnSc1	druhý
do	do	k7c2	do
věčného	věčné	k1gNnSc2	věčné
zatracení	zatracení	k1gNnSc2	zatracení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
logiky	logika	k1gFnSc2	logika
Tomáš	Tomáš	k1gMnSc1	Tomáš
nenalézá	nalézat	k5eNaImIp3nS	nalézat
důvod	důvod	k1gInSc4	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
s	s	k7c7	s
kacířem	kacíř	k1gMnSc7	kacíř
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
vrahem	vrah	k1gMnSc7	vrah
duše	duše	k1gFnPc1	duše
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
nakládat	nakládat	k5eAaImF	nakládat
mírněji	mírně	k6eAd2	mírně
než	než	k8xS	než
s	s	k7c7	s
obyčejným	obyčejný	k2eAgMnSc7d1	obyčejný
vrahem	vrah	k1gMnSc7	vrah
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
<g/>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
středověké	středověký	k2eAgFnSc2d1	středověká
inkvizice	inkvizice	k1gFnSc2	inkvizice
byly	být	k5eAaImAgFnP	být
decentralizované	decentralizovaný	k2eAgFnPc1d1	decentralizovaná
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
spravování	spravování	k1gNnSc1	spravování
příslušelo	příslušet	k5eAaImAgNnS	příslušet
místním	místní	k2eAgMnPc3d1	místní
hodnostářům	hodnostář	k1gMnPc3	hodnostář
na	na	k7c6	na
základě	základ	k1gInSc6	základ
směrnic	směrnice	k1gFnPc2	směrnice
vydaných	vydaný	k2eAgFnPc2d1	vydaná
papežem	papež	k1gMnSc7	papež
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
institucemi	instituce	k1gFnPc7	instituce
inkvizice	inkvizice	k1gFnSc2	inkvizice
neexistoval	existovat	k5eNaImAgInS	existovat
vztah	vztah	k1gInSc1	vztah
nadřízenosti	nadřízenost	k1gFnSc2	nadřízenost
a	a	k8xC	a
podřízenosti	podřízenost	k1gFnSc2	podřízenost
jako	jako	k8xS	jako
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Existovalo	existovat	k5eAaImAgNnS	existovat
vícero	vícero	k1gNnSc1	vícero
druhů	druh	k1gMnPc2	druh
inkvizice	inkvizice	k1gFnSc2	inkvizice
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
působnosti	působnost	k1gFnSc6	působnost
a	a	k8xC	a
metodách	metoda	k1gFnPc6	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
je	on	k3xPp3gInPc4	on
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
biskupskou	biskupský	k2eAgFnSc4d1	biskupská
a	a	k8xC	a
papežskou	papežský	k2eAgFnSc4d1	Papežská
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
institucí	instituce	k1gFnPc2	instituce
středověké	středověký	k2eAgFnSc2d1	středověká
inkvizice	inkvizice	k1gFnSc2	inkvizice
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
biskupská	biskupský	k2eAgFnSc1d1	biskupská
inkvizice	inkvizice	k1gFnSc1	inkvizice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
ve	v	k7c6	v
Veroně	Verona	k1gFnSc6	Verona
roku	rok	k1gInSc2	rok
1184	[number]	k4	1184
bulou	bula	k1gFnSc7	bula
Ad	ad	k7c4	ad
abolendam	abolendam	k1gInSc4	abolendam
papež	papež	k1gMnSc1	papež
Lucius	Lucius	k1gMnSc1	Lucius
III	III	kA	III
<g/>
.	.	kIx.	.
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Fridrichem	Fridrich	k1gMnSc7	Fridrich
Barbarossou	Barbarossa	k1gMnSc7	Barbarossa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
náleželo	náležet	k5eAaImAgNnS	náležet
moci	moc	k1gFnSc2	moc
duchovní	duchovní	k2eAgMnPc1d1	duchovní
určit	určit	k5eAaPmF	určit
vinu	vina	k1gFnSc4	vina
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
vést	vést	k5eAaImF	vést
i	i	k8xC	i
výslechy	výslech	k1gInPc4	výslech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
světskému	světský	k2eAgNnSc3d1	světské
rameni	rameno	k1gNnSc3	rameno
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
stanovení	stanovení	k1gNnSc1	stanovení
a	a	k8xC	a
provedení	provedení	k1gNnSc1	provedení
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
katarskou	katarský	k2eAgFnSc4d1	katarská
herezi	hereze	k1gFnSc4	hereze
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zodpovědnými	zodpovědný	k2eAgFnPc7d1	zodpovědná
osobami	osoba	k1gFnPc7	osoba
byli	být	k5eAaImAgMnP	být
sídelní	sídelní	k2eAgMnPc1d1	sídelní
biskupové	biskup	k1gMnPc1	biskup
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
také	také	k9	také
biskupská	biskupský	k2eAgFnSc1d1	biskupská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
důvodů	důvod	k1gInPc2	důvod
nebyla	být	k5eNaImAgFnS	být
biskupská	biskupský	k2eAgFnSc1d1	biskupská
inkvizice	inkvizice	k1gFnSc1	inkvizice
příliš	příliš	k6eAd1	příliš
funkční	funkční	k2eAgFnSc1d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Biskupové	biskup	k1gMnPc1	biskup
často	často	k6eAd1	často
nesídlili	sídlit	k5eNaImAgMnP	sídlit
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
diecézích	diecéze	k1gFnPc6	diecéze
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
vzdálených	vzdálený	k2eAgNnPc6d1	vzdálené
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
)	)	kIx)	)
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
diecézi	diecéze	k1gFnSc4	diecéze
pouze	pouze	k6eAd1	pouze
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgFnPc2	tento
návštěv	návštěva	k1gFnPc2	návštěva
však	však	k9	však
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
kvůli	kvůli	k7c3	kvůli
dalším	další	k2eAgFnPc3d1	další
povinnostem	povinnost	k1gFnPc3	povinnost
mnoho	mnoho	k6eAd1	mnoho
prostoru	prostor	k1gInSc2	prostor
pro	pro	k7c4	pro
důkladné	důkladný	k2eAgNnSc4d1	důkladné
inkviziční	inkviziční	k2eAgNnSc4d1	inkviziční
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
inkviziční	inkviziční	k2eAgInSc1d1	inkviziční
proces	proces	k1gInSc1	proces
proto	proto	k8xC	proto
nebyl	být	k5eNaImAgInS	být
vždy	vždy	k6eAd1	vždy
efektivní	efektivní	k2eAgInSc1d1	efektivní
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
komplikací	komplikace	k1gFnSc7	komplikace
byla	být	k5eAaImAgFnS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
buly	bula	k1gFnSc2	bula
Ad	ad	k7c4	ad
abolendam	abolendam	k1gInSc4	abolendam
bylo	být	k5eAaImAgNnS	být
povinností	povinnost	k1gFnSc7	povinnost
sdělit	sdělit	k5eAaPmF	sdělit
obviněnému	obviněný	k1gMnSc3	obviněný
jméno	jméno	k1gNnSc4	jméno
žalobce	žalobce	k1gMnSc1	žalobce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
často	často	k6eAd1	často
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
mstě	msta	k1gFnSc3	msta
ještě	ještě	k9	ještě
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
soudem	soud	k1gInSc7	soud
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Počátky	počátek	k1gInPc4	počátek
papežské	papežský	k2eAgFnSc2d1	Papežská
inkvizice	inkvizice	k1gFnSc2	inkvizice
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
reformním	reformní	k2eAgInSc7d1	reformní
programem	program	k1gInSc7	program
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
lateránského	lateránský	k2eAgInSc2d1	lateránský
koncilu	koncil	k1gInSc2	koncil
(	(	kIx(	(
<g/>
1215	[number]	k4	1215
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stvrdil	stvrdit	k5eAaPmAgMnS	stvrdit
exkomunikaci	exkomunikace	k1gFnSc4	exkomunikace
valdenských	valdenští	k1gMnPc2	valdenští
papežem	papež	k1gMnSc7	papež
Inocencem	Inocenc	k1gMnSc7	Inocenc
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Valdenské	valdenský	k2eAgNnSc1d1	Valdenské
hnutí	hnutí	k1gNnSc1	hnutí
představovalo	představovat	k5eAaImAgNnS	představovat
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
velmi	velmi	k6eAd1	velmi
destabilizující	destabilizující	k2eAgInSc1d1	destabilizující
faktor	faktor	k1gInSc1	faktor
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
se	se	k3xPyFc4	se
tažením	tažení	k1gNnSc7	tažení
vůči	vůči	k7c3	vůči
herezi	hereze	k1gFnSc3	hereze
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
snažila	snažit	k5eAaImAgFnS	snažit
chránit	chránit	k5eAaImF	chránit
jak	jak	k6eAd1	jak
svou	svůj	k3xOyFgFnSc4	svůj
nauku	nauka	k1gFnSc4	nauka
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
feudální	feudální	k2eAgNnSc4d1	feudální
společenské	společenský	k2eAgNnSc4d1	společenské
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
inkvizice	inkvizice	k1gFnSc1	inkvizice
byla	být	k5eAaImAgFnS	být
pověřena	pověřit	k5eAaPmNgFnS	pověřit
výslovně	výslovně	k6eAd1	výslovně
úkolem	úkol	k1gInSc7	úkol
odhalování	odhalování	k1gNnSc2	odhalování
<g/>
,	,	kIx,	,
stíhání	stíhání	k1gNnSc1	stíhání
a	a	k8xC	a
trestání	trestání	k1gNnSc1	trestání
kacířů	kacíř	k1gMnPc2	kacíř
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
sympatizantů	sympatizant	k1gMnPc2	sympatizant
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
jmenování	jmenování	k1gNnSc4	jmenování
zvláštních	zvláštní	k2eAgMnPc2d1	zvláštní
soudců	soudce	k1gMnPc2	soudce
pověřených	pověřený	k2eAgMnPc2d1	pověřený
pronásledováním	pronásledování	k1gNnSc7	pronásledování
kacířů	kacíř	k1gMnPc2	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
neuspěla	uspět	k5eNaPmAgFnS	uspět
snaha	snaha	k1gFnSc1	snaha
biskupů	biskup	k1gMnPc2	biskup
<g/>
,	,	kIx,	,
řízením	řízení	k1gNnSc7	řízení
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
pověřovány	pověřován	k2eAgInPc4d1	pověřován
nové	nový	k2eAgInPc4d1	nový
žebravé	žebravý	k2eAgInPc4d1	žebravý
řády	řád	k1gInPc4	řád
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
počátkem	počátek	k1gInSc7	počátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dominikáni	dominikán	k1gMnPc1	dominikán
a	a	k8xC	a
františkáni	františkán	k1gMnPc1	františkán
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
inkvizitorů	inkvizitor	k1gMnPc2	inkvizitor
byli	být	k5eAaImAgMnP	být
pověřováni	pověřován	k2eAgMnPc1d1	pověřován
též	též	k9	též
papežští	papežský	k2eAgMnPc1d1	papežský
legáti	legát	k1gMnPc1	legát
a	a	k8xC	a
diecézní	diecézní	k2eAgInSc4d1	diecézní
klérus	klérus	k1gInSc4	klérus
<g/>
.	.	kIx.	.
</s>
<s>
Biskupská	biskupský	k2eAgFnSc1d1	biskupská
inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k8xC	jako
nedostačující	dostačující	k2eNgInSc1d1	nedostačující
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
inkviziční	inkviziční	k2eAgNnSc4d1	inkviziční
řízení	řízení	k1gNnSc4	řízení
profesionalizovat	profesionalizovat	k5eAaImF	profesionalizovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
založil	založit	k5eAaPmAgMnS	založit
papežskou	papežský	k2eAgFnSc4d1	Papežská
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
<g/>
,	,	kIx,	,
aby	aby	k9	aby
odhalováním	odhalování	k1gNnSc7	odhalování
a	a	k8xC	a
souzením	souzení	k1gNnSc7	souzení
kacířů	kacíř	k1gMnPc2	kacíř
byli	být	k5eAaImAgMnP	být
pověřeni	pověřen	k2eAgMnPc1d1	pověřen
náležitě	náležitě	k6eAd1	náležitě
připravení	připravený	k2eAgMnPc1d1	připravený
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
zplnomocněným	zplnomocněný	k2eAgFnPc3d1	zplnomocněná
papežských	papežský	k2eAgMnPc2d1	papežský
delegovaných	delegovaný	k2eAgMnPc2d1	delegovaný
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
papežský	papežský	k2eAgMnSc1d1	papežský
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
(	(	kIx(	(
<g/>
delegovaný	delegovaný	k2eAgMnSc1d1	delegovaný
papežský	papežský	k2eAgMnSc1d1	papežský
soudce	soudce	k1gMnSc1	soudce
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
na	na	k7c4	na
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
a	a	k8xC	a
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
roku	rok	k1gInSc2	rok
1231	[number]	k4	1231
jmenován	jmenován	k2eAgMnSc1d1	jmenován
převor	převor	k1gMnSc1	převor
dominkánského	dominkánský	k2eAgInSc2d1	dominkánský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
.	.	kIx.	.
<g/>
Inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
sobě	se	k3xPyFc3	se
spojovat	spojovat	k5eAaImF	spojovat
funkci	funkce	k1gFnSc4	funkce
duchovního	duchovní	k1gMnSc2	duchovní
pastýře	pastýř	k1gMnSc2	pastýř
a	a	k8xC	a
soudce	soudce	k1gMnSc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ideálními	ideální	k2eAgMnPc7d1	ideální
kandidáty	kandidát	k1gMnPc7	kandidát
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
inkvizitorů	inkvizitor	k1gMnPc2	inkvizitor
zdáli	zdát	k5eAaImAgMnP	zdát
být	být	k5eAaImF	být
právě	právě	k9	právě
dominikáni	dominikán	k1gMnPc1	dominikán
(	(	kIx(	(
<g/>
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
františkáni	františkán	k1gMnPc1	františkán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
vzdělaní	vzdělaný	k2eAgMnPc1d1	vzdělaný
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
podřízeni	podřízen	k2eAgMnPc1d1	podřízen
papežskému	papežský	k2eAgInSc3d1	papežský
stolci	stolec	k1gInSc3	stolec
<g/>
,	,	kIx,	,
nemajíce	mít	k5eNaImSgFnP	mít
žádné	žádný	k3yNgFnPc4	žádný
osobní	osobní	k2eAgFnPc4d1	osobní
vazby	vazba	k1gFnPc4	vazba
na	na	k7c4	na
komunitu	komunita	k1gFnSc4	komunita
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
byli	být	k5eAaImAgMnP	být
jako	jako	k9	jako
soudci	soudce	k1gMnPc1	soudce
vysláni	vyslán	k2eAgMnPc1d1	vyslán
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
byli	být	k5eAaImAgMnP	být
všeobecně	všeobecně	k6eAd1	všeobecně
oblíbeni	oblíben	k2eAgMnPc1d1	oblíben
jako	jako	k9	jako
kazatelé	kazatel	k1gMnPc1	kazatel
a	a	k8xC	a
zpovědníci	zpovědník	k1gMnPc1	zpovědník
<g/>
.	.	kIx.	.
<g/>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jak	jak	k6eAd1	jak
velkém	velký	k2eAgInSc6d1	velký
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
pokušení	pokušený	k2eAgMnPc1d1	pokušený
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
zneužít	zneužít	k5eAaPmF	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Právní	právní	k2eAgInSc1d1	právní
proces	proces	k1gInSc1	proces
inquisitio	inquisitio	k6eAd1	inquisitio
totiž	totiž	k9	totiž
spojoval	spojovat	k5eAaImAgMnS	spojovat
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
osoby	osoba	k1gFnSc2	osoba
úřad	úřad	k1gInSc1	úřad
vyšetřovatele	vyšetřovatel	k1gMnSc2	vyšetřovatel
a	a	k8xC	a
soudce	soudce	k1gMnSc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
navíc	navíc	k6eAd1	navíc
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
i	i	k9	i
duchovním	duchovní	k2eAgMnSc7d1	duchovní
pastýřem	pastýř	k1gMnSc7	pastýř
obžalovaného	obžalovaný	k1gMnSc2	obžalovaný
<g/>
.	.	kIx.	.
</s>
<s>
Procesy	proces	k1gInPc1	proces
bývaly	bývat	k5eAaImAgInP	bývat
utajené	utajený	k2eAgMnPc4d1	utajený
a	a	k8xC	a
soudce	soudce	k1gMnPc4	soudce
měl	mít	k5eAaImAgMnS	mít
velmi	velmi	k6eAd1	velmi
široké	široký	k2eAgFnPc4d1	široká
pravomoci	pravomoc	k1gFnPc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
spravedlivého	spravedlivý	k2eAgInSc2d1	spravedlivý
procesu	proces	k1gInSc2	proces
pak	pak	k6eAd1	pak
záviselo	záviset	k5eAaImAgNnS	záviset
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
charakteru	charakter	k1gInSc6	charakter
inkvizitora	inkvizitor	k1gMnSc2	inkvizitor
<g/>
.	.	kIx.	.
</s>
<s>
Zkušenosti	zkušenost	k1gFnPc1	zkušenost
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
desetiletí	desetiletí	k1gNnSc2	desetiletí
fungování	fungování	k1gNnSc2	fungování
inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
odrazily	odrazit	k5eAaPmAgInP	odrazit
v	v	k7c6	v
ustanoveních	ustanovení	k1gNnPc6	ustanovení
synody	synoda	k1gFnSc2	synoda
v	v	k7c6	v
Tarragoně	Tarragona	k1gFnSc6	Tarragona
roku	rok	k1gInSc2	rok
1242	[number]	k4	1242
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
snaha	snaha	k1gFnSc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
excesům	exces	k1gInPc3	exces
a	a	k8xC	a
dát	dát	k5eAaPmF	dát
inkvizičnímu	inkviziční	k2eAgInSc3d1	inkviziční
procesů	proces	k1gInPc2	proces
řád	řád	k1gInSc4	řád
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
neexistovala	existovat	k5eNaImAgFnS	existovat
inkvizice	inkvizice	k1gFnSc1	inkvizice
jako	jako	k8xS	jako
jediná	jediný	k2eAgFnSc1d1	jediná
instituce	instituce	k1gFnSc1	instituce
či	či	k8xC	či
organizace	organizace	k1gFnSc1	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
období	období	k1gNnSc4	období
středověku	středověk	k1gInSc2	středověk
bychom	by	kYmCp1nP	by
spíše	spíše	k9	spíše
měli	mít	k5eAaImAgMnP	mít
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
jednotlivých	jednotlivý	k2eAgMnPc6d1	jednotlivý
inkvizitorech	inkvizitor	k1gMnPc6	inkvizitor
či	či	k8xC	či
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
inkvizičních	inkviziční	k2eAgInPc6d1	inkviziční
tribunálech	tribunál	k1gInPc6	tribunál
obsazených	obsazený	k2eAgInPc2d1	obsazený
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
někteří	některý	k3yIgMnPc1	některý
byli	být	k5eAaImAgMnP	být
dominikáni	dominikán	k1gMnPc1	dominikán
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
františkáni	františkán	k1gMnPc1	františkán
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nebyli	být	k5eNaImAgMnP	být
součástí	součást	k1gFnSc7	součást
jedné	jeden	k4xCgFnSc2	jeden
instituce	instituce	k1gFnSc2	instituce
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
společné	společný	k2eAgInPc4d1	společný
znaky	znak	k1gInPc4	znak
<g/>
:	:	kIx,	:
byli	být	k5eAaImAgMnP	být
přímými	přímý	k2eAgMnPc7d1	přímý
podřízenými	podřízený	k1gMnPc7	podřízený
papeže	papež	k1gMnSc2	papež
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
vyjmuti	vyjmout	k5eAaPmNgMnP	vyjmout
z	z	k7c2	z
běžné	běžný	k2eAgFnSc2d1	běžná
biskupské	biskupský	k2eAgFnSc2d1	biskupská
pravomoci	pravomoc	k1gFnSc2	pravomoc
a	a	k8xC	a
dohledu	dohled	k1gInSc2	dohled
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemuseli	muset	k5eNaImAgMnP	muset
čekat	čekat	k5eAaImF	čekat
až	až	k9	až
bude	být	k5eAaImBp3nS	být
proti	proti	k7c3	proti
podezřelému	podezřelý	k2eAgInSc3d1	podezřelý
vzneseno	vznést	k5eAaPmNgNnS	vznést
formální	formální	k2eAgNnSc1d1	formální
obvinění	obvinění	k1gNnSc1	obvinění
a	a	k8xC	a
všechna	všechen	k3xTgFnSc1	všechen
řízení	řízení	k1gNnSc3	řízení
byla	být	k5eAaImAgFnS	být
tajná	tajný	k2eAgFnSc1d1	tajná
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Už	už	k6eAd1	už
od	od	k7c2	od
počátků	počátek	k1gInPc2	počátek
inkvizice	inkvizice	k1gFnSc2	inkvizice
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
i	i	k9	i
otázka	otázka	k1gFnSc1	otázka
trestání	trestání	k1gNnSc2	trestání
tzv.	tzv.	kA	tzv.
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
IV	IV	kA	IV
<g/>
.	.	kIx.	.
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1258	[number]	k4	1258
a	a	k8xC	a
1260	[number]	k4	1260
<g/>
)	)	kIx)	)
zakázal	zakázat	k5eAaPmAgMnS	zakázat
inkvizitorům	inkvizitor	k1gMnPc3	inkvizitor
vést	vést	k5eAaImF	vést
procesy	proces	k1gInPc4	proces
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
podezřelí	podezřelý	k2eAgMnPc1d1	podezřelý
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
,	,	kIx,	,
směrodatné	směrodatný	k2eAgNnSc1d1	směrodatné
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
obvinění	obvinění	k1gNnSc4	obvinění
ze	z	k7c2	z
zjevné	zjevný	k2eAgFnSc2d1	zjevná
hereze	hereze	k1gFnSc2	hereze
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1398	[number]	k4	1398
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
teologická	teologický	k2eAgFnSc1d1	teologická
fakulta	fakulta	k1gFnSc1	fakulta
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
univerzity	univerzita	k1gFnSc2	univerzita
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
magie	magie	k1gFnSc1	magie
jako	jako	k8xS	jako
pakt	pakt	k1gInSc1	pakt
s	s	k7c7	s
ďáblem	ďábel	k1gMnSc7	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
však	však	k9	však
papežská	papežský	k2eAgFnSc1d1	Papežská
kurie	kurie	k1gFnSc1	kurie
nezareagovala	zareagovat	k5eNaPmAgFnS	zareagovat
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1484	[number]	k4	1484
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgInS	vydat
bulu	bula	k1gFnSc4	bula
Summis	Summis	k1gInSc1	Summis
desiderantes	desiderantes	k1gMnSc1	desiderantes
affectibus	affectibus	k1gMnSc1	affectibus
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
de	de	k?	de
facto	facto	k1gNnSc4	facto
zrušil	zrušit	k5eAaPmAgInS	zrušit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
papeže	papež	k1gMnSc2	papež
Alexandra	Alexandr	k1gMnSc2	Alexandr
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
nakázal	nakázat	k5eAaBmAgMnS	nakázat
inkvizitorům	inkvizitor	k1gMnPc3	inkvizitor
podniknout	podniknout	k5eAaPmF	podniknout
rozhodné	rozhodný	k2eAgInPc4d1	rozhodný
kroky	krok	k1gInPc4	krok
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uctívají	uctívat	k5eAaImIp3nP	uctívat
ďábla	ďábel	k1gMnSc4	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Inocenc	Inocenc	k1gMnSc1	Inocenc
tak	tak	k6eAd1	tak
podpořil	podpořit	k5eAaPmAgMnS	podpořit
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
masové	masový	k2eAgNnSc1d1	masové
pronásledování	pronásledování	k1gNnSc1	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
středověk	středověk	k1gInSc1	středověk
nikdy	nikdy	k6eAd1	nikdy
nepoznal	poznat	k5eNaPmAgInS	poznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Působnost	působnost	k1gFnSc1	působnost
inkvizice	inkvizice	k1gFnSc1	inkvizice
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
inkvizice	inkvizice	k1gFnSc1	inkvizice
šířila	šířit	k5eAaImAgFnS	šířit
na	na	k7c4	na
sever	sever	k1gInSc4	sever
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
nebývala	bývat	k5eNaImAgFnS	bývat
inkvizice	inkvizice	k1gFnSc1	inkvizice
tolik	tolik	k6eAd1	tolik
přísná	přísný	k2eAgNnPc4d1	přísné
jako	jako	k8xS	jako
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jižní	jižní	k2eAgFnSc6d1	jižní
<g/>
,	,	kIx,	,
v	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
neměla	mít	k5eNaImAgFnS	mít
téměř	téměř	k6eAd1	téměř
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
nebyla	být	k5eNaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
inkvizice	inkvizice	k1gFnSc2	inkvizice
ustanovena	ustanovit	k5eAaPmNgFnS	ustanovit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Aragonském	aragonský	k2eAgNnSc6d1	Aragonské
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
center	centrum	k1gNnPc2	centrum
katarského	katarský	k2eAgNnSc2d1	Katarské
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
IX	IX	kA	IX
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
první	první	k4xOgMnPc4	první
inkvizitory	inkvizitor	k1gMnPc4	inkvizitor
pro	pro	k7c4	pro
Katalánsko	Katalánsko	k1gNnSc4	Katalánsko
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1238	[number]	k4	1238
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Kastilii	Kastilie	k1gFnSc4	Kastilie
ani	ani	k8xC	ani
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
středověká	středověký	k2eAgFnSc1d1	středověká
inkvizice	inkvizice	k1gFnSc1	inkvizice
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc4	první
inkvizitory	inkvizitor	k1gMnPc4	inkvizitor
v	v	k7c4	v
Kastilii	Kastilie	k1gFnSc4	Kastilie
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1462	[number]	k4	1462
kastilský	kastilský	k2eAgMnSc1d1	kastilský
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
představitel	představitel	k1gMnSc1	představitel
světské	světský	k2eAgFnSc2d1	světská
moci	moc	k1gFnSc2	moc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
prahu	práh	k1gInSc6	práh
novověku	novověk	k1gInSc2	novověk
rodící	rodící	k2eAgFnSc1d1	rodící
se	se	k3xPyFc4	se
španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
však	však	k9	však
nevznikla	vzniknout	k5eNaPmAgFnS	vzniknout
jednoznačně	jednoznačně	k6eAd1	jednoznačně
jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc4	nástroj
potírající	potírající	k2eAgFnSc4d1	potírající
herezi	hereze	k1gFnSc4	hereze
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
byla	být	k5eAaImAgFnS	být
výrazem	výraz	k1gInSc7	výraz
sílícího	sílící	k2eAgInSc2d1	sílící
antisemitismu	antisemitismus	k1gInSc2	antisemitismus
na	na	k7c6	na
iberském	iberský	k2eAgInSc6d1	iberský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
<g/>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
nezkoumala	zkoumat	k5eNaImAgFnS	zkoumat
všechny	všechen	k3xTgInPc4	všechen
případy	případ	k1gInPc4	případ
hereze	hereze	k1gFnSc1	hereze
či	či	k8xC	či
odchýlení	odchýlení	k1gNnSc1	odchýlení
se	se	k3xPyFc4	se
od	od	k7c2	od
katolické	katolický	k2eAgFnSc2d1	katolická
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
katolických	katolický	k2eAgFnPc6d1	katolická
zemích	zem	k1gFnPc6	zem
byla	být	k5eAaImAgFnS	být
hereze	hereze	k1gFnSc1	hereze
přečinem	přečin	k1gInSc7	přečin
i	i	k9	i
proti	proti	k7c3	proti
světskému	světský	k2eAgNnSc3d1	světské
právu	právo	k1gNnSc3	právo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pak	pak	k6eAd1	pak
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
světské	světský	k2eAgInPc4d1	světský
tresty	trest	k1gInPc4	trest
a	a	k8xC	a
staralo	starat	k5eAaImAgNnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
vykonání	vykonání	k1gNnSc4	vykonání
příslušného	příslušný	k2eAgInSc2d1	příslušný
rozsudku	rozsudek	k1gInSc2	rozsudek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Obvinění	obvinění	k1gNnPc4	obvinění
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
nebo	nebo	k8xC	nebo
otevřené	otevřený	k2eAgFnSc2d1	otevřená
podpory	podpora	k1gFnSc2	podpora
heretiků	heretik	k1gMnPc2	heretik
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
i	i	k9	i
pro	pro	k7c4	pro
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výprava	výprava	k1gFnSc1	výprava
proti	proti	k7c3	proti
languedocké	languedocký	k2eAgFnSc3d1	languedocký
šlechtě	šlechta	k1gFnSc3	šlechta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1208	[number]	k4	1208
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
tohoto	tento	k3xDgNnSc2	tento
tažení	tažení	k1gNnSc2	tažení
byla	být	k5eAaImAgFnS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
podpora	podpora	k1gFnSc1	podpora
languedocké	languedocký	k2eAgFnSc2d1	languedocký
šlechty	šlechta	k1gFnSc2	šlechta
vůči	vůči	k7c3	vůči
albigenským	albigenský	k2eAgInPc3d1	albigenský
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
podíl	podíl	k1gInSc4	podíl
na	na	k7c6	na
zavraždění	zavraždění	k1gNnSc6	zavraždění
papežského	papežský	k2eAgMnSc2d1	papežský
legáta	legát	k1gMnSc2	legát
Pierre	Pierr	k1gInSc5	Pierr
de	de	k?	de
Castelnau	Castelnaus	k1gInSc2	Castelnaus
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
a	a	k8xC	a
soud	soud	k1gInSc1	soud
===	===	k?	===
</s>
</p>
<p>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
inkvizičního	inkviziční	k2eAgInSc2d1	inkviziční
procesu	proces	k1gInSc2	proces
byl	být	k5eAaImAgInS	být
samotný	samotný	k2eAgInSc1d1	samotný
příchod	příchod	k1gInSc1	příchod
inkvizitorů	inkvizitor	k1gMnPc2	inkvizitor
do	do	k7c2	do
určeného	určený	k2eAgNnSc2d1	určené
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Shromážděnému	shromážděný	k2eAgNnSc3d1	shromážděné
duchovenstvu	duchovenstvo	k1gNnSc3	duchovenstvo
a	a	k8xC	a
lidu	lid	k1gInSc3	lid
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
okolí	okolí	k1gNnSc2	okolí
přednesli	přednést	k5eAaPmAgMnP	přednést
kázání	kázání	k1gNnSc4	kázání
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
všechny	všechen	k3xTgFnPc4	všechen
zavazovali	zavazovat	k5eAaImAgMnP	zavazovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
jim	on	k3xPp3gFnPc3	on
známé	známý	k2eAgMnPc4d1	známý
kacíře	kacíř	k1gMnPc4	kacíř
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
období	období	k1gNnSc4	období
milosti	milost	k1gFnSc2	milost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
během	během	k7c2	během
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
kacíři	kacíř	k1gMnPc1	kacíř
sami	sám	k3xTgMnPc1	sám
dobrovolně	dobrovolně	k6eAd1	dobrovolně
přiznat	přiznat	k5eAaPmF	přiznat
a	a	k8xC	a
činit	činit	k5eAaImF	činit
pokání	pokání	k1gNnSc4	pokání
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
jakýmkoli	jakýkoli	k3yIgInPc3	jakýkoli
právním	právní	k2eAgInPc3d1	právní
postihům	postih	k1gInPc3	postih
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dobrovolných	dobrovolný	k2eAgNnPc2d1	dobrovolné
doznání	doznání	k1gNnPc2	doznání
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
shromažďována	shromažďovat	k5eAaImNgFnS	shromažďovat
i	i	k8xC	i
udání	udání	k1gNnPc4	udání
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
byl	být	k5eAaImAgInS	být
vyhotovován	vyhotovován	k2eAgInSc4d1	vyhotovován
seznam	seznam	k1gInSc4	seznam
podezřelých	podezřelý	k2eAgMnPc2d1	podezřelý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
stanovéné	stanovéný	k2eAgFnSc2d1	stanovéný
doby	doba	k1gFnSc2	doba
pak	pak	k6eAd1	pak
začaly	začít	k5eAaPmAgInP	začít
výslechy	výslech	k1gInPc1	výslech
<g/>
.	.	kIx.	.
</s>
<s>
Podezřelý	podezřelý	k2eAgInSc1d1	podezřelý
byl	být	k5eAaImAgInS	být
diskrétně	diskrétně	k6eAd1	diskrétně
přiveden	přivést	k5eAaPmNgInS	přivést
před	před	k7c4	před
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
odpřísáhl	odpřísáhnout	k5eAaPmAgMnS	odpřísáhnout
na	na	k7c4	na
evangelium	evangelium	k1gNnSc4	evangelium
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mluvit	mluvit	k5eAaImF	mluvit
celou	celý	k2eAgFnSc4d1	celá
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
výslýchán	výslýchán	k2eAgInSc4d1	výslýchán
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
svědků	svědek	k1gMnPc2	svědek
byla	být	k5eAaImAgNnP	být
obviněnému	obviněný	k2eAgMnSc3d1	obviněný
(	(	kIx(	(
<g/>
z	z	k7c2	z
obavy	obava	k1gFnSc2	obava
před	před	k7c7	před
pomstou	pomsta	k1gFnSc7	pomsta
<g/>
)	)	kIx)	)
zatajena	zatajen	k2eAgFnSc1d1	zatajena
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
do	do	k7c2	do
vyšetřovacích	vyšetřovací	k2eAgInPc2d1	vyšetřovací
protokolů	protokol	k1gInPc2	protokol
byla	být	k5eAaImAgFnS	být
pečlivě	pečlivě	k6eAd1	pečlivě
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obviněný	obviněný	k1gMnSc1	obviněný
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
procesu	proces	k1gInSc2	proces
udat	udat	k5eAaPmF	udat
jména	jméno	k1gNnSc2	jméno
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
-	-	kIx~	-
ti	ten	k3xDgMnPc1	ten
pak	pak	k6eAd1	pak
bývali	bývat	k5eAaImAgMnP	bývat
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
svědků	svědek	k1gMnPc2	svědek
obvykle	obvykle	k6eAd1	obvykle
vyloučeni	vyloučit	k5eAaPmNgMnP	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výsleších	výslech	k1gInPc6	výslech
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
mučení	mučení	k1gNnSc1	mučení
(	(	kIx(	(
<g/>
schváleno	schválit	k5eAaPmNgNnS	schválit
papežem	papež	k1gMnSc7	papež
roku	rok	k1gInSc2	rok
1252	[number]	k4	1252
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obvyklé	obvyklý	k2eAgFnSc6d1	obvyklá
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
světských	světský	k2eAgInPc2d1	světský
soudů	soud	k1gInPc2	soud
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
inkvizičním	inkviziční	k2eAgInSc6d1	inkviziční
procesu	proces	k1gInSc6	proces
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
doznání	doznání	k1gNnSc1	doznání
učiněné	učiněný	k2eAgNnSc1d1	učiněné
na	na	k7c4	na
mučidlech	mučidlo	k1gNnPc6	mučidlo
zopakováno	zopakován	k2eAgNnSc4d1	zopakováno
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
násilí	násilí	k1gNnSc2	násilí
příští	příští	k2eAgInSc4d1	příští
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
bylo	být	k5eAaImAgNnS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
zahaleno	zahalen	k2eAgNnSc1d1	zahaleno
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
,	,	kIx,	,
rozsudky	rozsudek	k1gInPc1	rozsudek
byly	být	k5eAaImAgInP	být
vyhlašovány	vyhlašovat	k5eAaImNgInP	vyhlašovat
veřejně	veřejně	k6eAd1	veřejně
během	během	k7c2	během
kázání	kázání	k1gNnSc2	kázání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
celé	celý	k2eAgNnSc1d1	celé
řízení	řízení	k1gNnSc1	řízení
uzavíralo	uzavírat	k5eAaImAgNnS	uzavírat
<g/>
.	.	kIx.	.
<g/>
Obviněný	obviněný	k2eAgMnSc1d1	obviněný
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
sice	sice	k8xC	sice
svého	svůj	k3xOyFgMnSc2	svůj
advokáta	advokát	k1gMnSc2	advokát
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
usvědčen	usvědčen	k2eAgMnSc1d1	usvědčen
<g/>
,	,	kIx,	,
advokátovi	advokát	k1gMnSc3	advokát
bývalo	bývat	k5eAaImAgNnS	bývat
znemožněno	znemožněn	k2eAgNnSc1d1	znemožněno
nadále	nadále	k6eAd1	nadále
vykonávat	vykonávat	k5eAaImF	vykonávat
svou	svůj	k3xOyFgFnSc4	svůj
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
úkolem	úkol	k1gInSc7	úkol
inkvizitorů	inkvizitor	k1gMnPc2	inkvizitor
bylo	být	k5eAaImAgNnS	být
přivést	přivést	k5eAaPmF	přivést
obviněného	obviněný	k2eAgMnSc2d1	obviněný
k	k	k7c3	k
doznání	doznání	k1gNnSc3	doznání
a	a	k8xC	a
k	k	k7c3	k
odvolání	odvolání	k1gNnSc3	odvolání
dané	daný	k2eAgFnSc2d1	daná
hereze	hereze	k1gFnSc2	hereze
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Podkladem	podklad	k1gInSc7	podklad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
byla	být	k5eAaImAgFnS	být
udání	udání	k1gNnSc4	udání
nebo	nebo	k8xC	nebo
výpověď	výpověď	k1gFnSc4	výpověď
vyšetřovaného	vyšetřovaný	k1gMnSc2	vyšetřovaný
proti	proti	k7c3	proti
třetí	třetí	k4xOgFnSc3	třetí
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většinou	k6eAd1	většinou
už	už	k9	už
toto	tento	k3xDgNnSc1	tento
udání	udání	k1gNnSc1	udání
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
inkvizici	inkvizice	k1gFnSc4	inkvizice
důkazem	důkaz	k1gInSc7	důkaz
o	o	k7c6	o
vině	vina	k1gFnSc6	vina
obžalovaného	obžalovaný	k1gMnSc2	obžalovaný
<g/>
.	.	kIx.	.
</s>
<s>
Zatčený	zatčený	k1gMnSc1	zatčený
byl	být	k5eAaImAgMnS	být
umístěn	umístit	k5eAaPmNgMnS	umístit
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
izolaci	izolace	k1gFnSc6	izolace
od	od	k7c2	od
okolního	okolní	k2eAgInSc2d1	okolní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizitoři	inkvizitor	k1gMnPc1	inkvizitor
mohli	moct	k5eAaImAgMnP	moct
věznit	věznit	k5eAaImF	věznit
obžalovaného	obžalovaný	k1gMnSc2	obžalovaný
rok	rok	k1gInSc4	rok
nebo	nebo	k8xC	nebo
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
bylo	být	k5eAaImAgNnS	být
časově	časově	k6eAd1	časově
neomezeno	omezen	k2eNgNnSc1d1	neomezeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mučení	mučení	k1gNnSc2	mučení
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1252	[number]	k4	1252
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
IV	IV	kA	IV
<g/>
.	.	kIx.	.
bulou	bula	k1gFnSc7	bula
Ad	ad	k7c4	ad
extirpanda	extirpando	k1gNnPc4	extirpando
inkvizitory	inkvizitor	k1gMnPc4	inkvizitor
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
útrpného	útrpný	k2eAgNnSc2d1	útrpné
práva	právo	k1gNnSc2	právo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
obžalovaný	obžalovaný	k1gMnSc1	obžalovaný
nechtěl	chtít	k5eNaImAgMnS	chtít
vypovídat	vypovídat	k5eAaPmF	vypovídat
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
pochybnosti	pochybnost	k1gFnPc1	pochybnost
o	o	k7c6	o
pravdivosti	pravdivost	k1gFnSc6	pravdivost
jeho	jeho	k3xOp3gInPc2	jeho
výroků	výrok	k1gInPc2	výrok
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
u	u	k7c2	u
ostatních	ostatní	k2eAgInPc2d1	ostatní
soudů	soud	k1gInPc2	soud
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
zakázal	zakázat	k5eAaPmAgInS	zakázat
vynášet	vynášet	k5eAaImF	vynášet
závažné	závažný	k2eAgInPc4d1	závažný
tresty	trest	k1gInPc4	trest
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
konfiskace	konfiskace	k1gFnSc1	konfiskace
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
doživotní	doživotní	k2eAgInSc4d1	doživotní
žalář	žalář	k1gInSc4	žalář
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
<g/>
)	)	kIx)	)
proti	proti	k7c3	proti
heretikům	heretik	k1gMnPc3	heretik
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
místního	místní	k2eAgMnSc2d1	místní
biskupa	biskup	k1gMnSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Postupy	postup	k1gInPc1	postup
mučení	mučení	k1gNnPc2	mučení
však	však	k9	však
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
světskými	světský	k2eAgInPc7d1	světský
soudy	soud	k1gInPc7	soud
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
:	:	kIx,	:
bylo	být	k5eAaImAgNnS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
užívat	užívat	k5eAaImF	užívat
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
prolití	prolití	k1gNnSc3	prolití
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
zmrzačení	zmrzačení	k1gNnSc2	zmrzačení
nebo	nebo	k8xC	nebo
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
způsobů	způsob	k1gInPc2	způsob
mučení	mučení	k1gNnSc2	mučení
bylo	být	k5eAaImAgNnS	být
tzv.	tzv.	kA	tzv.
strappado	strappada	k1gFnSc5	strappada
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byly	být	k5eAaImAgInP	být
vyslýchanému	vyslýchaný	k2eAgInSc3d1	vyslýchaný
za	za	k7c4	za
zády	záda	k1gNnPc7	záda
svázány	svázán	k2eAgFnPc4d1	svázána
ruce	ruka	k1gFnPc4	ruka
<g/>
,	,	kIx,	,
za	za	k7c4	za
něž	jenž	k3xRgMnPc4	jenž
byl	být	k5eAaImAgMnS	být
vyslýchaný	vyslýchaný	k2eAgMnSc1d1	vyslýchaný
zavěšen	zavěšen	k2eAgMnSc1d1	zavěšen
a	a	k8xC	a
docházelo	docházet	k5eAaImAgNnS	docházet
tak	tak	k9	tak
k	k	k7c3	k
vymknutí	vymknutí	k1gNnSc3	vymknutí
kloubů	kloub	k1gInPc2	kloub
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
natažení	natažení	k1gNnSc2	natažení
svalů	sval	k1gInPc2	sval
i	i	k9	i
s	s	k7c7	s
trvalými	trvalý	k2eAgInPc7d1	trvalý
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Trest	trest	k1gInSc4	trest
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
soudu	soud	k1gInSc2	soud
mohlo	moct	k5eAaImAgNnS	moct
trvat	trvat	k5eAaImF	trvat
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgInS	být
rozsudek	rozsudek	k1gInSc4	rozsudek
vynesen	vynést	k5eAaPmNgMnS	vynést
<g/>
;	;	kIx,	;
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
obviněný	obviněný	k2eAgMnSc1d1	obviněný
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizitoři	inkvizitor	k1gMnPc1	inkvizitor
obvykle	obvykle	k6eAd1	obvykle
vyhlašovali	vyhlašovat	k5eAaImAgMnP	vyhlašovat
rozsudky	rozsudek	k1gInPc4	rozsudek
více	hodně	k6eAd2	hodně
případů	případ	k1gInPc2	případ
společně	společně	k6eAd1	společně
během	během	k7c2	během
veřejného	veřejný	k2eAgNnSc2d1	veřejné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
nazývaného	nazývaný	k2eAgNnSc2d1	nazývané
sermo	sermo	k6eAd1	sermo
generalis	generalis	k1gFnPc3	generalis
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
obecná	obecný	k2eAgFnSc1d1	obecná
promluva	promluva	k1gFnSc1	promluva
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
upálením	upálení	k1gNnSc7	upálení
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
hereze	hereze	k1gFnSc2	hereze
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
zákonodárství	zákonodárství	k1gNnSc6	zákonodárství
zavedl	zavést	k5eAaPmAgMnS	zavést
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
Sicilském	sicilský	k2eAgNnSc6d1	sicilské
království	království	k1gNnSc6	království
v	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
rozsudek	rozsudek	k1gInSc4	rozsudek
inkvizičního	inkviziční	k2eAgInSc2d1	inkviziční
soudu	soud	k1gInSc2	soud
těžký	těžký	k2eAgInSc4d1	těžký
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Možným	možný	k2eAgInSc7d1	možný
trestem	trest	k1gInSc7	trest
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
spíše	spíše	k9	spíše
církevním	církevní	k2eAgNnSc7d1	církevní
pokáním	pokání	k1gNnSc7	pokání
<g/>
)	)	kIx)	)
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
pouť	pouť	k1gFnSc1	pouť
na	na	k7c4	na
některé	některý	k3yIgNnSc4	některý
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nošení	nošení	k1gNnSc4	nošení
žlutého	žlutý	k2eAgInSc2d1	žlutý
kříže	kříž	k1gInSc2	kříž
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
konfiskace	konfiskace	k1gFnSc2	konfiskace
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
vypovězení	vypovězení	k1gNnSc2	vypovězení
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
veřejné	veřejný	k2eAgNnSc4d1	veřejné
odvolání	odvolání	k1gNnSc4	odvolání
nebo	nebo	k8xC	nebo
žalář	žalář	k1gInSc4	žalář
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
i	i	k9	i
doživotní	doživotní	k2eAgFnSc1d1	doživotní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
upálením	upálení	k1gNnSc7	upálení
byl	být	k5eAaImAgInS	být
určen	určen	k2eAgInSc1d1	určen
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
nejvážnější	vážní	k2eAgInPc4d3	nejvážnější
a	a	k8xC	a
zatvrzelé	zatvrzelý	k2eAgInPc4d1	zatvrzelý
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
provinili	provinit	k5eAaPmAgMnP	provinit
opakovaně	opakovaně	k6eAd1	opakovaně
nebo	nebo	k8xC	nebo
nekonali	konat	k5eNaImAgMnP	konat
pokání	pokání	k1gNnSc4	pokání
<g/>
.	.	kIx.	.
</s>
<s>
Popravu	poprava	k1gFnSc4	poprava
nevykonávala	vykonávat	k5eNaImAgFnS	vykonávat
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
světská	světský	k2eAgFnSc1d1	světská
moc	moc	k1gFnSc1	moc
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Inkvizitoři	inkvizitor	k1gMnPc1	inkvizitor
obecně	obecně	k6eAd1	obecně
nevydávali	vydávat	k5eNaImAgMnP	vydávat
heretiky	heretik	k1gMnPc4	heretik
k	k	k7c3	k
potrestání	potrestání	k1gNnSc3	potrestání
světskému	světský	k2eAgNnSc3d1	světské
rameni	rameno	k1gNnSc3	rameno
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
odvolání	odvolání	k1gNnSc3	odvolání
nepřiměli	přimět	k5eNaPmAgMnP	přimět
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
inkvizitora	inkvizitor	k1gMnSc2	inkvizitor
bylo	být	k5eAaImAgNnS	být
usvědčeného	usvědčený	k2eAgMnSc2d1	usvědčený
napravit	napravit	k5eAaPmF	napravit
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgMnSc1d1	známý
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
Bernard	Bernard	k1gMnSc1	Bernard
Gui	Gui	k1gMnSc1	Gui
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
francouzského	francouzský	k2eAgInSc2d1	francouzský
Toulouse	Toulouse	k1gInSc2	Toulouse
<g/>
,	,	kIx,	,
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
dohromady	dohromady	k6eAd1	dohromady
více	hodně	k6eAd2	hodně
než	než	k8xS	než
700	[number]	k4	700
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
<g/>
,	,	kIx,	,
z	z	k7c2	z
těch	ten	k3xDgMnPc2	ten
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
celkem	celkem	k6eAd1	celkem
čtyřicet	čtyřicet	k4xCc1	čtyřicet
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
inkvizici	inkvizice	k1gFnSc4	inkvizice
přiznání	přiznání	k1gNnSc2	přiznání
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
že	že	k8xS	že
církev	církev	k1gFnSc1	církev
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zachránit	zachránit	k5eAaPmF	zachránit
duši	duše	k1gFnSc4	duše
z	z	k7c2	z
hereze	hereze	k1gFnSc2	hereze
<g/>
;	;	kIx,	;
tzv.	tzv.	kA	tzv.
salus	salus	k1gInSc1	salus
animarum	animarum	k1gInSc1	animarum
<g/>
,	,	kIx,	,
spása	spása	k1gFnSc1	spása
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
deklarovanou	deklarovaný	k2eAgFnSc7d1	deklarovaná
motivací	motivace	k1gFnSc7	motivace
v	v	k7c6	v
působení	působení	k1gNnSc6	působení
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Během	během	k7c2	během
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Palermu	Palermo	k1gNnSc6	Palermo
inkvizicí	inkvizice	k1gFnPc2	inkvizice
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
660	[number]	k4	660
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
22	[number]	k4	22
upáleno	upálit	k5eAaPmNgNnS	upálit
<g/>
.	.	kIx.	.
</s>
<s>
Soudilo	soudit	k5eAaImAgNnS	soudit
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
za	za	k7c4	za
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
<g/>
,	,	kIx,	,
kacířství	kacířství	k1gNnSc4	kacířství
a	a	k8xC	a
bigamii	bigamie	k1gFnSc4	bigamie
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
popudu	popud	k1gInSc2	popud
krále	král	k1gMnSc2	král
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonského	aragonský	k2eAgInSc2d1	aragonský
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podléhala	podléhat	k5eAaImAgFnS	podléhat
přímo	přímo	k6eAd1	přímo
španělskému	španělský	k2eAgMnSc3d1	španělský
králi	král	k1gMnSc3	král
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
papeži	papež	k1gMnSc3	papež
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
inkvizice	inkvizice	k1gFnSc1	inkvizice
jakožto	jakožto	k8xS	jakožto
nástroj	nástroj	k1gInSc1	nástroj
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
jejího	její	k3xOp3gNnSc2	její
zacházení	zacházení	k1gNnSc2	zacházení
s	s	k7c7	s
muslimy	muslim	k1gMnPc7	muslim
<g/>
,	,	kIx,	,
židy	žid	k1gMnPc7	žid
a	a	k8xC	a
alumbrados	alumbrados	k1gMnSc1	alumbrados
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejtemnějším	temný	k2eAgFnPc3d3	nejtemnější
stránkám	stránka	k1gFnPc3	stránka
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
jejího	její	k3xOp3gInSc2	její
největšího	veliký	k2eAgInSc2d3	veliký
rozmachu	rozmach	k1gInSc2	rozmach
stál	stát	k5eAaImAgInS	stát
Tomás	Tomás	k1gInSc1	Tomás
de	de	k?	de
Torquemada	Torquemada	k1gFnSc1	Torquemada
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgMnSc7d1	velký
inkvizitorem	inkvizitor	k1gMnSc7	inkvizitor
Španělska	Španělsko	k1gNnSc2	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1487	[number]	k4	1487
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
vzešla	vzejít	k5eAaPmAgFnS	vzejít
Peruánská	peruánský	k2eAgFnSc1d1	peruánská
inkvizice	inkvizice	k1gFnSc1	inkvizice
a	a	k8xC	a
Mexická	mexický	k2eAgFnSc1d1	mexická
inkvizice	inkvizice	k1gFnSc1	inkvizice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
činná	činný	k2eAgFnSc1d1	činná
až	až	k9	až
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
byla	být	k5eAaImAgFnS	být
zrušena	zrušen	k2eAgFnSc1d1	zrušena
roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Dějinná	dějinný	k2eAgFnSc1d1	dějinná
situace	situace	k1gFnSc1	situace
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nebylo	být	k5eNaImAgNnS	být
Španělsko	Španělsko	k1gNnSc1	Španělsko
jednolitým	jednolitý	k2eAgInSc7d1	jednolitý
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
konfederací	konfederace	k1gFnSc7	konfederace
samostatných	samostatný	k2eAgInPc2d1	samostatný
administrativních	administrativní	k2eAgInPc2d1	administrativní
celků	celek	k1gInPc2	celek
<g/>
;	;	kIx,	;
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
Aragonské	aragonský	k2eAgNnSc1d1	Aragonské
království	království	k1gNnSc1	království
a	a	k8xC	a
Kastilské	kastilský	k2eAgNnSc1d1	Kastilské
království	království	k1gNnSc1	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Aragonii	Aragonie	k1gFnSc6	Aragonie
<g/>
,	,	kIx,	,
konfederaci	konfederace	k1gFnSc6	konfederace
Aragonského	aragonský	k2eAgNnSc2d1	Aragonské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
Baleár	Baleáry	k1gFnPc2	Baleáry
<g/>
,	,	kIx,	,
Katalánska	Katalánsko	k1gNnSc2	Katalánsko
a	a	k8xC	a
Valencie	Valencie	k1gFnSc2	Valencie
existovala	existovat	k5eAaImAgFnS	existovat
biskupská	biskupský	k2eAgFnSc1d1	biskupská
inkvizice	inkvizice	k1gFnSc1	inkvizice
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
Kastilii	Kastilie	k1gFnSc6	Kastilie
a	a	k8xC	a
Leónu	Leóno	k1gNnSc6	Leóno
nikoli	nikoli	k9	nikoli
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Iberského	iberský	k2eAgInSc2d1	iberský
poloostrova	poloostrov	k1gInSc2	poloostrov
obývali	obývat	k5eAaImAgMnP	obývat
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
patřila	patřit	k5eAaImAgFnS	patřit
Granada	Granada	k1gFnSc1	Granada
Maurům	Maur	k1gMnPc3	Maur
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
Sevilla	Sevilla	k1gFnSc1	Sevilla
<g/>
,	,	kIx,	,
Valladolid	Valladolid	k1gInSc1	Valladolid
a	a	k8xC	a
Barcelona	Barcelona	k1gFnSc1	Barcelona
měla	mít	k5eAaImAgFnS	mít
velké	velký	k2eAgFnPc4d1	velká
židovské	židovská	k1gFnPc4	židovská
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
tradičně	tradičně	k6eAd1	tradičně
sloužili	sloužit	k5eAaImAgMnP	sloužit
aragonským	aragonský	k2eAgMnPc3d1	aragonský
králům	král	k1gMnPc3	král
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinandův	Ferdinandův	k2eAgMnSc1d1	Ferdinandův
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Aragonský	aragonský	k2eAgInSc1d1	aragonský
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
jako	jako	k8xS	jako
svého	svůj	k3xOyFgMnSc2	svůj
dvorního	dvorní	k2eAgMnSc2d1	dvorní
astrologa	astrolog	k1gMnSc2	astrolog
Žida	Žid	k1gMnSc2	Žid
<g/>
,	,	kIx,	,
Abiathara	Abiathar	k1gMnSc2	Abiathar
Crescase	Crescasa	k1gFnSc6	Crescasa
<g/>
.	.	kIx.	.
</s>
<s>
Pedro	Pedro	k1gNnSc1	Pedro
de	de	k?	de
la	la	k1gNnSc1	la
Caballeria	Caballerium	k1gNnSc2	Caballerium
<g/>
,	,	kIx,	,
marrano	marrana	k1gFnSc5	marrana
čili	čili	k8xC	čili
židovský	židovský	k2eAgMnSc1d1	židovský
konvertita	konvertita	k1gMnSc1	konvertita
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
uzavření	uzavření	k1gNnSc6	uzavření
Ferdinandova	Ferdinandův	k2eAgNnSc2d1	Ferdinandovo
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Isabelou	Isabela	k1gFnSc7	Isabela
Kastilskou	kastilský	k2eAgFnSc7d1	Kastilská
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Vznik	vznik	k1gInSc1	vznik
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
===	===	k?	===
</s>
</p>
<p>
<s>
Aragonský	aragonský	k2eAgMnSc1d1	aragonský
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
používal	používat	k5eAaImAgMnS	používat
náboženství	náboženství	k1gNnSc4	náboženství
jako	jako	k8xS	jako
prostředek	prostředek	k1gInSc4	prostředek
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
svým	svůj	k3xOyFgNnSc7	svůj
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
vyhladit	vyhladit	k5eAaPmF	vyhladit
na	na	k7c6	na
svých	svůj	k3xOyFgNnPc6	svůj
územích	území	k1gNnPc6	území
židovské	židovská	k1gFnSc2	židovská
a	a	k8xC	a
muslimské	muslimský	k2eAgNnSc1d1	muslimské
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
–	–	k?	–
a	a	k8xC	a
jako	jako	k9	jako
prostředku	prostředek	k1gInSc2	prostředek
užil	užít	k5eAaPmAgMnS	užít
inkvizice	inkvizice	k1gFnPc4	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
Ferdinandovy	Ferdinandův	k2eAgFnSc2d1	Ferdinandova
snahy	snaha	k1gFnSc2	snaha
oslabit	oslabit	k5eAaPmF	oslabit
jeho	jeho	k3xOp3gMnPc4	jeho
hlavní	hlavní	k2eAgMnPc4d1	hlavní
politické	politický	k2eAgMnPc4d1	politický
oponenty	oponent	k1gMnPc4	oponent
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
motivy	motiv	k1gInPc4	motiv
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
vznik	vznik	k1gInSc4	vznik
byly	být	k5eAaImAgFnP	být
finanční	finanční	k2eAgFnPc1d1	finanční
<g/>
.	.	kIx.	.
</s>
<s>
Židovští	židovský	k2eAgMnPc1d1	židovský
bankéři	bankéř	k1gMnPc1	bankéř
financovali	financovat	k5eAaBmAgMnP	financovat
mnoho	mnoho	k4c4	mnoho
fondů	fond	k1gInPc2	fond
Jana	Jan	k1gMnSc2	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
bylo	být	k5eAaImAgNnS	být
užito	užít	k5eAaPmNgNnS	užít
pro	pro	k7c4	pro
uzavření	uzavření	k1gNnSc4	uzavření
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
Aragonií	Aragonie	k1gFnSc7	Aragonie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
věřitel	věřitel	k1gMnSc1	věřitel
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
soudem	soud	k1gInSc7	soud
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
dluhy	dluh	k1gInPc1	dluh
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
odpuštěny	odpustit	k5eAaPmNgInP	odpustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
byl	být	k5eAaImAgMnS	být
schopný	schopný	k2eAgMnSc1d1	schopný
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
udržoval	udržovat	k5eAaImAgMnS	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
papežským	papežský	k2eAgInSc7d1	papežský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
konsolidovat	konsolidovat	k5eAaBmF	konsolidovat
své	svůj	k3xOyFgNnSc4	svůj
panství	panství	k1gNnSc4	panství
<g/>
,	,	kIx,	,
uzavřít	uzavřít	k5eAaPmF	uzavřít
manželství	manželství	k1gNnPc4	manželství
s	s	k7c7	s
Isabellou	Isabella	k1gFnSc7	Isabella
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
tak	tak	k6eAd1	tak
jediný	jediný	k2eAgInSc1d1	jediný
stát	stát	k1gInSc1	stát
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
dědice	dědic	k1gMnSc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgMnS	moct
papež	papež	k1gMnSc1	papež
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
inkvizici	inkvizice	k1gFnSc3	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
však	však	k9	však
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
inkvizici	inkvizice	k1gFnSc4	inkvizice
zřizovat	zřizovat	k5eAaImF	zřizovat
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
,	,	kIx,	,
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
však	však	k9	však
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
trval	trvat	k5eAaImAgMnS	trvat
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
Rodriga	Rodrig	k1gMnSc4	Rodrig
Borgiu	Borgium	k1gNnSc6	Borgium
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
valencijského	valencijský	k2eAgMnSc2d1	valencijský
biskupa	biskup	k1gMnSc2	biskup
a	a	k8xC	a
papežského	papežský	k2eAgMnSc2d1	papežský
vicekancléře	vicekancléř	k1gMnSc2	vicekancléř
a	a	k8xC	a
kardinála	kardinál	k1gMnSc2	kardinál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgMnS	pomoct
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Sixtus	Sixtus	k1gMnSc1	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
povolil	povolit	k5eAaPmAgInS	povolit
zřízení	zřízení	k1gNnSc4	zřízení
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Kastilii	Kastilie	k1gFnSc6	Kastilie
<g/>
.	.	kIx.	.
</s>
<s>
Borgia	Borgia	k1gFnSc1	Borgia
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgInS	moct
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
využívat	využívat	k5eAaPmF	využívat
podpory	podpora	k1gFnPc4	podpora
Španělska	Španělsko	k1gNnSc2	Španělsko
coby	coby	k?	coby
papež	papež	k1gMnSc1	papež
Alexandr	Alexandr	k1gMnSc1	Alexandr
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sixtus	Sixtus	k1gMnSc1	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
stavět	stavět	k5eAaImF	stavět
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
španělské	španělský	k2eAgFnSc3d1	španělská
inkvizici	inkvizice	k1gFnSc3	inkvizice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
mu	on	k3xPp3gMnSc3	on
hrozil	hrozit	k5eAaImAgMnS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
podporu	podpora	k1gFnSc4	podpora
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
Sicilského	sicilský	k2eAgNnSc2d1	sicilské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Sixtus	Sixtus	k1gMnSc1	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1478	[number]	k4	1478
vydal	vydat	k5eAaPmAgMnS	vydat
bulu	bula	k1gFnSc4	bula
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
dané	daný	k2eAgNnSc4d1	dané
zřízení	zřízení	k1gNnSc4	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
kursem	kurs	k1gInSc7	kurs
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
nasadil	nasadit	k5eAaPmAgMnS	nasadit
a	a	k8xC	a
nesvolil	svolit	k5eNaPmAgMnS	svolit
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
královské	královský	k2eAgFnSc2d1	královská
inkvizice	inkvizice	k1gFnSc2	inkvizice
v	v	k7c6	v
Aragonii	Aragonie	k1gFnSc6	Aragonie
<g/>
.	.	kIx.	.
</s>
<s>
Údajně	údajně	k6eAd1	údajně
sám	sám	k3xTgMnSc1	sám
označoval	označovat	k5eAaImAgMnS	označovat
tuto	tento	k3xDgFnSc4	tento
inkvizici	inkvizice	k1gFnSc4	inkvizice
za	za	k7c4	za
Ferdinandův	Ferdinandův	k2eAgInSc4d1	Ferdinandův
prostředek	prostředek	k1gInSc4	prostředek
konfiskace	konfiskace	k1gFnSc2	konfiskace
židovského	židovský	k2eAgInSc2d1	židovský
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
udělenému	udělený	k2eAgInSc3d1	udělený
titulu	titul	k1gInSc3	titul
Nejkatoličtější	katolický	k2eAgMnSc1d3	katolický
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
pro	pro	k7c4	pro
španělskou	španělský	k2eAgFnSc4d1	španělská
korunu	koruna	k1gFnSc4	koruna
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
však	však	k9	však
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
nadále	nadále	k6eAd1	nadále
čelil	čelit	k5eAaImAgMnS	čelit
papežovým	papežův	k2eAgInPc3d1	papežův
pokusům	pokus	k1gInPc3	pokus
mít	mít	k5eAaImF	mít
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
počínání	počínání	k1gNnSc4	počínání
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
politickém	politický	k2eAgInSc6d1	politický
nátlaku	nátlak	k1gInSc6	nátlak
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
pomohla	pomoct	k5eAaPmAgFnS	pomoct
také	také	k6eAd1	také
složitá	složitý	k2eAgFnSc1d1	složitá
politická	politický	k2eAgFnSc1d1	politická
situace	situace	k1gFnSc1	situace
(	(	kIx(	(
<g/>
oslabení	oslabení	k1gNnSc1	oslabení
Benátské	benátský	k2eAgFnSc2d1	Benátská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
expanze	expanze	k1gFnSc2	expanze
Turků	Turek	k1gMnPc2	Turek
<g/>
)	)	kIx)	)
Sixtus	Sixtus	k1gMnSc1	Sixtus
nakonec	nakonec	k6eAd1	nakonec
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Ferdinandovy	Ferdinandův	k2eAgFnPc4d1	Ferdinandova
snahy	snaha	k1gFnPc4	snaha
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
tak	tak	k6eAd1	tak
získal	získat	k5eAaPmAgMnS	získat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
po	po	k7c6	po
čem	co	k3yQnSc6	co
toužil	toužit	k5eAaImAgInS	toužit
<g/>
:	:	kIx,	:
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
mohl	moct	k5eAaImAgMnS	moct
ovládat	ovládat	k5eAaImF	ovládat
s	s	k7c7	s
požehnáním	požehnání	k1gNnSc7	požehnání
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
a	a	k8xC	a
královskou	královský	k2eAgFnSc4d1	královská
pokladnu	pokladna	k1gFnSc4	pokladna
plnou	plný	k2eAgFnSc4d1	plná
ukořistěného	ukořistěný	k2eAgNnSc2d1	ukořistěné
židovského	židovský	k2eAgNnSc2d1	Židovské
a	a	k8xC	a
maurského	maurský	k2eAgNnSc2d1	maurské
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhnání	vyhnání	k1gNnSc1	vyhnání
Židů	Žid	k1gMnPc2	Žid
===	===	k?	===
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
a	a	k8xC	a
Isabella	Isabella	k1gMnSc1	Isabella
pověřili	pověřit	k5eAaPmAgMnP	pověřit
roku	rok	k1gInSc2	rok
1481	[number]	k4	1481
vyšetřováním	vyšetřování	k1gNnSc7	vyšetřování
a	a	k8xC	a
trestáním	trestání	k1gNnSc7	trestání
tzv.	tzv.	kA	tzv.
conversos	conversosa	k1gFnPc2	conversosa
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
pouze	pouze	k6eAd1	pouze
předstírat	předstírat	k5eAaImF	předstírat
svou	svůj	k3xOyFgFnSc4	svůj
konverzi	konverze	k1gFnSc4	konverze
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
setrvat	setrvat	k5eAaPmF	setrvat
u	u	k7c2	u
praxe	praxe	k1gFnSc2	praxe
původního	původní	k2eAgNnSc2d1	původní
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
Tomáse	Tomáse	k1gFnSc1	Tomáse
de	de	k?	de
Torquemada	Torquemada	k1gFnSc1	Torquemada
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
Židů	Žid	k1gMnPc2	Žid
byli	být	k5eAaImAgMnP	být
dokonce	dokonce	k9	dokonce
vysvěcení	vysvěcený	k2eAgMnPc1d1	vysvěcený
kněží	kněz	k1gMnPc1	kněz
a	a	k8xC	a
biskupové	biskup	k1gMnPc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Pomlouvači	pomlouvač	k1gMnPc1	pomlouvač
nazývali	nazývat	k5eAaImAgMnP	nazývat
tyto	tento	k3xDgMnPc4	tento
Židy	Žid	k1gMnPc4	Žid
marranos	marranosa	k1gFnPc2	marranosa
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
prasata	prase	k1gNnPc4	prase
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
conversos	conversos	k1gInSc4	conversos
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
a	a	k8xC	a
po	po	k7c6	po
"	"	kIx"	"
<g/>
úspěchu	úspěch	k1gInSc6	úspěch
<g/>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zaměřila	zaměřit	k5eAaPmAgFnS	zaměřit
na	na	k7c4	na
Córdobu	Córdoba	k1gFnSc4	Córdoba
<g/>
,	,	kIx,	,
Jaén	Jaén	k1gInSc1	Jaén
a	a	k8xC	a
Ciudad	Ciudad	k1gInSc1	Ciudad
Real	Real	k1gInSc1	Real
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
též	též	k9	též
na	na	k7c6	na
Aragonii	Aragonie	k1gFnSc6	Aragonie
<g/>
,	,	kIx,	,
Kastilii	Kastilie	k1gFnSc6	Kastilie
a	a	k8xC	a
Valencii	Valencie	k1gFnSc6	Valencie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1486	[number]	k4	1486
a	a	k8xC	a
1492	[number]	k4	1492
bylo	být	k5eAaImAgNnS	být
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
25	[number]	k4	25
autodafé	autodafé	k1gNnPc2	autodafé
jen	jen	k6eAd1	jen
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
Toledu	Toledo	k1gNnSc6	Toledo
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1481	[number]	k4	1481
a	a	k8xC	a
1826	[number]	k4	1826
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
dalších	další	k2eAgNnPc6d1	další
464	[number]	k4	464
autodafé	autodafé	k1gNnPc6	autodafé
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
na	na	k7c4	na
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1480	[number]	k4	1480
a	a	k8xC	a
1492	[number]	k4	1492
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
obviněno	obvinit	k5eAaPmNgNnS	obvinit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
tisíc	tisíc	k4xCgInPc2	tisíc
conversos	conversosa	k1gFnPc2	conversosa
<g/>
.	.	kIx.	.
</s>
<s>
Inkviziční	inkviziční	k2eAgNnSc1d1	inkviziční
úsilí	úsilí	k1gNnSc1	úsilí
proti	proti	k7c3	proti
konvertitům	konvertita	k1gMnPc3	konvertita
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
vyhnáním	vyhnání	k1gNnSc7	vyhnání
všech	všecek	k3xTgMnPc2	všecek
Židů	Žid	k1gMnPc2	Žid
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
roku	rok	k1gInSc2	rok
1492	[number]	k4	1492
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezpečné	bezpečný	k2eAgNnSc4d1	bezpečné
útočiště	útočiště	k1gNnSc4	útočiště
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
skýtalo	skýtat	k5eAaImAgNnS	skýtat
muslimské	muslimský	k2eAgNnSc1d1	muslimské
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stalo	stát	k5eAaPmAgNnS	stát
místem	místo	k1gNnSc7	místo
rozkvětu	rozkvět	k1gInSc2	rozkvět
židovské	židovský	k2eAgFnSc2d1	židovská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
intelektuálního	intelektuální	k2eAgInSc2d1	intelektuální
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Granady	Granada	k1gFnSc2	Granada
byl	být	k5eAaImAgInS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1492	[number]	k4	1492
vydán	vydán	k2eAgInSc1d1	vydán
dekret	dekret	k1gInSc1	dekret
obou	dva	k4xCgNnPc2	dva
veličenstev	veličenstvo	k1gNnPc2	veličenstvo
o	o	k7c6	o
vyhnání	vyhnání	k1gNnSc6	vyhnání
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
konce	konec	k1gInSc2	konec
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Nesměli	smět	k5eNaImAgMnP	smět
přitom	přitom	k6eAd1	přitom
odnést	odnést	k5eAaPmF	odnést
cokoli	cokoli	k3yInSc4	cokoli
cenného	cenný	k2eAgNnSc2d1	cenné
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
peněz	peníze	k1gInPc2	peníze
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
udávaným	udávaný	k2eAgInSc7d1	udávaný
v	v	k7c6	v
dekretu	dekret	k1gInSc6	dekret
bylo	být	k5eAaImAgNnS	být
obvinění	obvinění	k1gNnSc1	obvinění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stavějí	stavět	k5eAaImIp3nP	stavět
proti	proti	k7c3	proti
víře	víra	k1gFnSc3	víra
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
don	don	k1gMnSc1	don
Isaac	Isaac	k1gFnSc4	Isaac
Abravanel	Abravanela	k1gFnPc2	Abravanela
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vykoupil	vykoupit	k5eAaPmAgInS	vykoupit
480	[number]	k4	480
židovských	židovský	k2eAgInPc2d1	židovský
morisků	morisk	k1gInPc2	morisk
z	z	k7c2	z
Malagy	Malaga	k1gFnSc2	Malaga
nyní	nyní	k6eAd1	nyní
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
edikt	edikt	k1gInSc1	edikt
zvrácen	zvrátit	k5eAaPmNgInS	zvrátit
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
dlouho	dlouho	k6eAd1	dlouho
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
naléhání	naléhání	k1gNnSc4	naléhání
velkého	velký	k2eAgMnSc2d1	velký
inkvizitora	inkvizitor	k1gMnSc2	inkvizitor
Torquemady	Torquemad	k1gInPc1	Torquemad
nabídku	nabídka	k1gFnSc4	nabídka
nakonec	nakonec	k6eAd1	nakonec
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
vyhnání	vyhnání	k1gNnSc1	vyhnání
týkalo	týkat	k5eAaImAgNnS	týkat
200	[number]	k4	200
tisíc	tisíc	k4xCgInPc2	tisíc
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
však	však	k9	však
konvertovalo	konvertovat	k5eAaBmAgNnS	konvertovat
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
100	[number]	k4	100
000	[number]	k4	000
uteklo	utéct	k5eAaPmAgNnS	utéct
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
50	[number]	k4	50
000	[number]	k4	000
do	do	k7c2	do
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Turecka	Turecko	k1gNnSc2	Turecko
nebo	nebo	k8xC	nebo
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
jich	on	k3xPp3gMnPc2	on
během	běh	k1gInSc7	běh
vyhnání	vyhnání	k1gNnSc2	vyhnání
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnání	vyhnání	k1gNnSc1	vyhnání
Židů	Žid	k1gMnPc2	Žid
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
komunity	komunita	k1gFnSc2	komunita
sefardských	sefardský	k2eAgMnPc2d1	sefardský
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Dekret	dekret	k1gInSc4	dekret
nařizující	nařizující	k2eAgNnSc4d1	nařizující
vyhnání	vyhnání	k1gNnSc4	vyhnání
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
teprve	teprve	k6eAd1	teprve
roku	rok	k1gInSc2	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
měla	mít	k5eAaImAgFnS	mít
po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
Židů	Žid	k1gMnPc2	Žid
volné	volný	k2eAgFnSc2d1	volná
ruce	ruka	k1gFnPc1	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
Žid	Žid	k1gMnSc1	Žid
ve	v	k7c6	v
španělských	španělský	k2eAgNnPc6d1	španělské
královstvích	království	k1gNnPc6	království
byl	být	k5eAaImAgInS	být
buď	buď	k8xC	buď
pokřtěn	pokřtěn	k2eAgInSc1d1	pokřtěn
nebo	nebo	k8xC	nebo
vyhnán	vyhnán	k2eAgInSc1d1	vyhnán
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nadále	nadále	k6eAd1	nadále
praktikovali	praktikovat	k5eAaImAgMnP	praktikovat
židovství	židovství	k1gNnSc4	židovství
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
jako	jako	k9	jako
znovuodpadlíci	znovuodpadlík	k1gMnPc1	znovuodpadlík
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgNnSc4d1	další
počínání	počínání	k1gNnSc4	počínání
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
===	===	k?	===
</s>
</p>
<p>
<s>
Sixtův	Sixtův	k2eAgMnSc1d1	Sixtův
následník	následník	k1gMnSc1	následník
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
vydal	vydat	k5eAaPmAgMnS	vydat
dvě	dva	k4xCgFnPc4	dva
buly	bula	k1gFnPc4	bula
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1485	[number]	k4	1485
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
žádaly	žádat	k5eAaImAgFnP	žádat
větší	veliký	k2eAgNnSc4d2	veliký
milosrdenství	milosrdenství	k1gNnSc4	milosrdenství
a	a	k8xC	a
mírnost	mírnost	k1gFnSc1	mírnost
vůči	vůči	k7c3	vůči
conversos	conversos	k1gMnSc1	conversos
<g/>
.	.	kIx.	.
</s>
<s>
Nařídil	nařídit	k5eAaPmAgMnS	nařídit
však	však	k9	však
také	také	k6eAd1	také
všem	všecek	k3xTgMnPc3	všecek
katolickým	katolický	k2eAgMnPc3d1	katolický
monarchům	monarcha	k1gMnPc3	monarcha
vydat	vydat	k5eAaPmF	vydat
uprchlé	uprchlý	k2eAgMnPc4d1	uprchlý
Židy	Žid	k1gMnPc4	Žid
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inkvizici	inkvizice	k1gFnSc4	inkvizice
řídily	řídit	k5eAaImAgFnP	řídit
sice	sice	k8xC	sice
církevní	církevní	k2eAgFnPc1d1	církevní
autority	autorita	k1gFnPc1	autorita
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
obviněný	obviněný	k1gMnSc1	obviněný
opravdu	opravdu	k6eAd1	opravdu
označen	označit	k5eAaPmNgMnS	označit
za	za	k7c4	za
heretika	heretik	k1gMnSc4	heretik
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vydán	vydán	k2eAgInSc1d1	vydán
světské	světský	k2eAgFnSc3d1	světská
autoritě	autorita	k1gFnSc3	autorita
k	k	k7c3	k
potrestání	potrestání	k1gNnSc3	potrestání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vynucení	vynucení	k1gNnSc3	vynucení
doznání	doznání	k1gNnPc2	doznání
se	se	k3xPyFc4	se
užívalo	užívat	k5eAaImAgNnS	užívat
mučení	mučení	k1gNnSc1	mučení
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
hodně	hodně	k6eAd1	hodně
kruté	krutý	k2eAgNnSc1d1	kruté
<g/>
.	.	kIx.	.
</s>
<s>
Trestem	trest	k1gInSc7	trest
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
hana	hana	k1gFnSc1	hana
(	(	kIx(	(
<g/>
nosení	nosení	k1gNnSc1	nosení
potupného	potupný	k2eAgInSc2d1	potupný
šatu	šat	k1gInSc2	šat
zvaného	zvaný	k2eAgInSc2d1	zvaný
sambenito	sambenit	k2eAgNnSc1d1	sambenit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
smrt	smrt	k1gFnSc1	smrt
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Odsouzenci	odsouzenec	k1gMnPc1	odsouzenec
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
bývali	bývat	k5eAaImAgMnP	bývat
trestáni	trestat	k5eAaImNgMnP	trestat
v	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
zhanobením	zhanobení	k1gNnSc7	zhanobení
a	a	k8xC	a
upálením	upálení	k1gNnSc7	upálení
svého	svůj	k3xOyFgInSc2	svůj
portrétu	portrét	k1gInSc2	portrét
či	či	k8xC	či
sochy	socha	k1gFnSc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Vynesení	vynesení	k1gNnSc1	vynesení
rozsudků	rozsudek	k1gInPc2	rozsudek
a	a	k8xC	a
vykonání	vykonání	k1gNnSc1	vykonání
trestů	trest	k1gInPc2	trest
bylo	být	k5eAaImAgNnS	být
inscenováno	inscenován	k2eAgNnSc1d1	inscenováno
jako	jako	k8xS	jako
okázalé	okázalý	k2eAgNnSc1d1	okázalé
a	a	k8xC	a
odstrašující	odstrašující	k2eAgNnSc1d1	odstrašující
veřejné	veřejný	k2eAgNnSc1d1	veřejné
divadlo	divadlo	k1gNnSc1	divadlo
auto-da-fé	autoaé	k1gNnSc2	auto-da-fé
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mohlo	moct	k5eAaImAgNnS	moct
trvat	trvat	k5eAaImF	trvat
i	i	k9	i
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Auto-da-fé	Autoaé	k6eAd1	Auto-da-fé
byla	být	k5eAaImAgFnS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
bohoslužbami	bohoslužba	k1gFnPc7	bohoslužba
<g/>
,	,	kIx,	,
procesími	procesí	k1gNnPc7	procesí
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
církevními	církevní	k2eAgInPc7d1	církevní
obřady	obřad	k1gInPc7	obřad
<g/>
,	,	kIx,	,
cílem	cíl	k1gInSc7	cíl
auto-da-fé	autoaé	k6eAd1	auto-da-fé
bylo	být	k5eAaImAgNnS	být
předvést	předvést	k5eAaPmF	předvést
moc	moc	k1gFnSc4	moc
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
potupit	potupit	k5eAaPmF	potupit
či	či	k8xC	či
zastrašit	zastrašit	k5eAaPmF	zastrašit
její	její	k3xOp3gMnPc4	její
skutečné	skutečný	k2eAgMnPc4d1	skutečný
nebo	nebo	k8xC	nebo
domnělé	domnělý	k2eAgMnPc4d1	domnělý
protivníky	protivník	k1gMnPc4	protivník
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
často	často	k6eAd1	často
bývala	bývat	k5eAaImAgFnS	bývat
vyzdobena	vyzdoben	k2eAgFnSc1d1	vyzdobena
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
zapálení	zapálení	k1gNnSc1	zapálení
mohl	moct	k5eAaImAgInS	moct
vykonat	vykonat	k5eAaPmF	vykonat
namísto	namísto	k7c2	namísto
kata	kat	k1gMnSc2	kat
i	i	k9	i
člen	člen	k1gMnSc1	člen
královské	královský	k2eAgFnSc2d1	královská
rodiny	rodina	k1gFnSc2	rodina
či	či	k8xC	či
vysoké	vysoký	k2eAgFnSc2d1	vysoká
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Udání	udání	k1gNnSc1	udání
mnoha	mnoho	k4c2	mnoho
lidí	člověk	k1gMnPc2	člověk
přicházela	přicházet	k5eAaImAgFnS	přicházet
z	z	k7c2	z
pomsty	pomsta	k1gFnSc2	pomsta
nebo	nebo	k8xC	nebo
ze	z	k7c2	z
snahy	snaha	k1gFnSc2	snaha
získat	získat	k5eAaPmF	získat
si	se	k3xPyFc3	se
přízeň	přízeň	k1gFnSc4	přízeň
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
sám	sám	k3xTgMnSc1	sám
stál	stát	k5eAaImAgMnS	stát
za	za	k7c7	za
mnoha	mnoho	k4c7	mnoho
obviněními	obvinění	k1gNnPc7	obvinění
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
majetku	majetek	k1gInSc6	majetek
či	či	k8xC	či
zemi	zem	k1gFnSc6	zem
těchto	tento	k3xDgFnPc2	tento
conversos	conversosa	k1gFnPc2	conversosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
byla	být	k5eAaImAgFnS	být
také	také	k9	také
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c4	na
potlačování	potlačování	k1gNnSc4	potlačování
raného	raný	k2eAgNnSc2d1	rané
protestantství	protestantství	k1gNnSc2	protestantství
<g/>
,	,	kIx,	,
erasmiánství	erasmiánství	k1gNnSc2	erasmiánství
a	a	k8xC	a
alumbrados	alumbradosa	k1gFnPc2	alumbradosa
<g/>
,	,	kIx,	,
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
proti	proti	k7c3	proti
encyklopedistům	encyklopedista	k1gMnPc3	encyklopedista
a	a	k8xC	a
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
však	však	k9	však
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
spíše	spíše	k9	spíše
světských	světský	k2eAgInPc2d1	světský
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
lidových	lidový	k2eAgFnPc2d1	lidová
pověr	pověra	k1gFnPc2	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Domnělé	domnělý	k2eAgFnPc1d1	domnělá
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
byly	být	k5eAaImAgFnP	být
španělskou	španělský	k2eAgFnSc7d1	španělská
inkvizicí	inkvizice	k1gFnSc7	inkvizice
zpravidla	zpravidla	k6eAd1	zpravidla
propuštěny	propustit	k5eAaPmNgInP	propustit
nebo	nebo	k8xC	nebo
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c2	za
duševně	duševně	k6eAd1	duševně
choré	chorý	k2eAgFnSc2d1	chorá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
během	během	k7c2	během
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
nadvlády	nadvláda	k1gFnSc2	nadvláda
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
až	až	k9	až
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
zřízena	zřídit	k5eAaPmNgFnS	zřídit
Ferdinandem	Ferdinand	k1gMnSc7	Ferdinand
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
odsouzenci	odsouzenec	k1gMnSc3	odsouzenec
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
však	však	k9	však
už	už	k6eAd1	už
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
nebývali	bývat	k5eNaImAgMnP	bývat
upalování	upalování	k1gNnSc4	upalování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
popraveni	popraven	k2eAgMnPc1d1	popraven
garotou	garota	k1gFnSc7	garota
nebo	nebo	k8xC	nebo
zastřeleni	zastřelit	k5eAaPmNgMnP	zastřelit
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgMnSc7d1	poslední
odsouzeným	odsouzený	k1gMnSc7	odsouzený
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
byl	být	k5eAaImAgMnS	být
učitel	učitel	k1gMnSc1	učitel
Cayetano	Cayetana	k1gFnSc5	Cayetana
Ripoli	Ripole	k1gFnSc6	Ripole
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
popraven	popravit	k5eAaPmNgInS	popravit
garotou	garota	k1gFnSc7	garota
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1826	[number]	k4	1826
za	za	k7c4	za
hlásání	hlásání	k1gNnSc4	hlásání
osícenských	osícenský	k2eAgInPc2d1	osícenský
názorů	názor	k1gInPc2	názor
a	a	k8xC	a
údajnou	údajný	k2eAgFnSc4d1	údajná
výuku	výuka	k1gFnSc4	výuka
deismu	deismus	k1gInSc2	deismus
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
zrušena	zrušen	k2eAgFnSc1d1	zrušena
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1834	[number]	k4	1834
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mučení	mučení	k1gNnSc2	mučení
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
pořadu	pořad	k1gInSc2	pořad
BBC	BBC	kA	BBC
a	a	k8xC	a
A	A	kA	A
<g/>
&	&	k?	&
<g/>
E	E	kA	E
Network	network	k1gInSc1	network
Myths	Myths	k1gInSc1	Myths
of	of	k?	of
the	the	k?	the
Spanish	Spanish	k1gInSc1	Spanish
Inquisition	Inquisition	k1gInSc1	Inquisition
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
neexistovaly	existovat	k5eNaImAgInP	existovat
během	během	k7c2	během
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
mučírny	mučírna	k1gFnSc2	mučírna
a	a	k8xC	a
mučení	mučení	k1gNnSc1	mučení
bylo	být	k5eAaImAgNnS	být
užíváno	užívat	k5eAaImNgNnS	užívat
velmi	velmi	k6eAd1	velmi
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
ve	v	k7c6	v
Valencii	Valencie	k1gFnSc6	Valencie
ze	z	k7c2	z
7	[number]	k4	7
tisíc	tisíc	k4xCgInPc2	tisíc
dokumentovaných	dokumentovaný	k2eAgInPc2d1	dokumentovaný
případů	případ	k1gInPc2	případ
bylo	být	k5eAaImAgNnS	být
mučení	mučení	k1gNnSc1	mučení
užito	užít	k5eAaPmNgNnS	užít
u	u	k7c2	u
2	[number]	k4	2
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
ne	ne	k9	ne
na	na	k7c4	na
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
čtvrthodinu	čtvrthodina	k1gFnSc4	čtvrthodina
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednou	jednou	k6eAd1	jednou
bylo	být	k5eAaImAgNnS	být
mučení	mučení	k1gNnSc1	mučení
užito	užít	k5eAaPmNgNnS	užít
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
;	;	kIx,	;
nebyly	být	k5eNaImAgInP	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
k	k	k7c3	k
mučení	mučení	k1gNnSc3	mučení
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
postupů	postup	k1gInPc2	postup
mučení	mučení	k1gNnPc2	mučení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
španělské	španělský	k2eAgFnSc3d1	španělská
inkvizici	inkvizice	k1gFnSc3	inkvizice
připisují	připisovat	k5eAaImIp3nP	připisovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nepoužívalo	používat	k5eNaImAgNnS	používat
<g/>
:	:	kIx,	:
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
např.	např.	kA	např.
železná	železný	k2eAgFnSc1d1	železná
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
užito	užít	k5eAaPmNgNnS	užít
až	až	k9	až
v	v	k7c6	v
reformovaném	reformovaný	k2eAgNnSc6d1	reformované
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
s	s	k7c7	s
drcením	drcení	k1gNnSc7	drcení
prstů	prst	k1gInPc2	prst
–	–	k?	–
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
neužívalo	užívat	k5eNaImAgNnS	užívat
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pronásledování	pronásledování	k1gNnSc2	pronásledování
katolíků	katolík	k1gMnPc2	katolík
Williamem	William	k1gInSc7	William
Cecilem	Cecil	k1gMnSc7	Cecil
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Alžběty	Alžběta	k1gFnSc2	Alžběta
I.	I.	kA	I.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
postupů	postup	k1gInPc2	postup
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Torquemada	Torquemada	k1gFnSc1	Torquemada
<g/>
:	:	kIx,	:
např.	např.	kA	např.
vodní	vodní	k2eAgNnSc1d1	vodní
mučení	mučení	k1gNnSc1	mučení
(	(	kIx(	(
<g/>
tortura	tortura	k1gFnSc1	tortura
del	del	k?	del
agua	agua	k1gFnSc1	agua
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
vyslýchanému	vyslýchaný	k2eAgInSc3d1	vyslýchaný
lila	lila	k2eAgNnSc4d1	lila
voda	voda	k1gFnSc1	voda
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
topí	topit	k5eAaImIp3nS	topit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
užíval	užívat	k5eAaImAgInS	užívat
tzv.	tzv.	kA	tzv.
garrucha	garruch	k1gMnSc2	garruch
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
oběti	oběť	k1gFnPc4	oběť
zkrouceny	zkroucen	k2eAgFnPc4d1	zkroucena
ruce	ruka	k1gFnPc4	ruka
za	za	k7c4	za
záda	záda	k1gNnPc4	záda
a	a	k8xC	a
potom	potom	k6eAd1	potom
byla	být	k5eAaImAgFnS	být
natahována	natahován	k2eAgFnSc1d1	natahována
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
užívalo	užívat	k5eAaImAgNnS	užívat
tzv.	tzv.	kA	tzv.
španělské	španělský	k2eAgFnSc2d1	španělská
židle	židle	k1gFnSc2	židle
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
držela	držet	k5eAaImAgFnS	držet
vyslýchaného	vyslýchaný	k2eAgMnSc4d1	vyslýchaný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
škvařila	škvařit	k5eAaImAgFnS	škvařit
chodidla	chodidlo	k1gNnPc4	chodidlo
<g/>
;	;	kIx,	;
není	být	k5eNaImIp3nS	být
však	však	k9	však
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
ji	on	k3xPp3gFnSc4	on
užívala	užívat	k5eAaImAgFnS	užívat
i	i	k9	i
inkvizice	inkvizice	k1gFnSc1	inkvizice
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
Rozsudky	rozsudek	k1gInPc1	rozsudek
smrti	smrt	k1gFnSc2	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
historiografii	historiografie	k1gFnSc6	historiografie
debata	debata	k1gFnSc1	debata
o	o	k7c6	o
skutečném	skutečný	k2eAgInSc6d1	skutečný
významu	význam	k1gInSc6	význam
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
a	a	k8xC	a
přesnosti	přesnost	k1gFnPc4	přesnost
dosud	dosud	k6eAd1	dosud
udávaných	udávaný	k2eAgNnPc2d1	udávané
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
historikové	historik	k1gMnPc1	historik
udávají	udávat	k5eAaImIp3nP	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zmiňované	zmiňovaný	k2eAgInPc4d1	zmiňovaný
počty	počet	k1gInPc4	počet
trestů	trest	k1gInPc2	trest
smrti	smrt	k1gFnSc2	smrt
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
<g/>
,	,	kIx,	,
jiní	jiný	k2eAgMnPc1d1	jiný
historikové	historik	k1gMnPc1	historik
jdou	jít	k5eAaImIp3nP	jít
až	až	k9	až
do	do	k7c2	do
stovek	stovka	k1gFnPc2	stovka
tisíců	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
generálního	generální	k2eAgMnSc2d1	generální
sekretáře	sekretář	k1gMnSc2	sekretář
inkvizice	inkvizice	k1gFnSc2	inkvizice
v	v	k7c6	v
letech	let	k1gInPc6	let
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
Juana	Juan	k1gMnSc2	Juan
Antonia	Antonio	k1gMnSc2	Antonio
Llorente	Llorent	k1gInSc5	Llorent
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1480	[number]	k4	1480
a	a	k8xC	a
1808	[number]	k4	1808
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
31	[number]	k4	31
912	[number]	k4	912
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Philip	Philip	k1gMnSc1	Philip
Schaff	Schaff	k1gMnSc1	Schaff
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Christian	Christian	k1gMnSc1	Christian
Church	Church	k1gMnSc1	Church
8	[number]	k4	8
800	[number]	k4	800
obětí	oběť	k1gFnPc2	oběť
upálených	upálený	k2eAgFnPc2d1	upálená
během	během	k7c2	během
Torquemadových	Torquemadový	k2eAgInPc2d1	Torquemadový
18	[number]	k4	18
let	léto	k1gNnPc2	léto
vykonávání	vykonávání	k1gNnSc2	vykonávání
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Archivy	archiv	k1gInPc7	archiv
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
dnes	dnes	k6eAd1	dnes
historikové	historik	k1gMnPc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Geoffrey	Geoffre	k2eAgInPc1d1	Geoffre
Parker	Parker	k1gInSc1	Parker
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
49	[number]	k4	49
tisíc	tisíc	k4xCgInPc2	tisíc
případů	případ	k1gInPc2	případ
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1540	[number]	k4	1540
a	a	k8xC	a
1700	[number]	k4	1700
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
třetinu	třetina	k1gFnSc4	třetina
případů	případ	k1gInPc2	případ
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
a	a	k8xC	a
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
byl	být	k5eAaImAgInS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
u	u	k7c2	u
776	[number]	k4	776
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1	dřívější
záznamy	záznam	k1gInPc1	záznam
se	se	k3xPyFc4	se
nedochovaly	dochovat	k5eNaPmAgInP	dochovat
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodporují	podporovat	k5eNaImIp3nP	podporovat
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
krvavosti	krvavost	k1gFnSc6	krvavost
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nezachovala	zachovat	k5eNaPmAgFnS	zachovat
úplná	úplný	k2eAgFnSc1d1	úplná
dokumentace	dokumentace	k1gFnSc1	dokumentace
činnosti	činnost	k1gFnSc2	činnost
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
nezbývá	zbývat	k5eNaImIp3nS	zbývat
než	než	k8xS	než
přesnější	přesný	k2eAgInPc1d2	přesnější
počty	počet	k1gInPc1	počet
jejích	její	k3xOp3gFnPc2	její
obětí	oběť	k1gFnPc2	oběť
pouze	pouze	k6eAd1	pouze
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
.	.	kIx.	.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
vedla	vést	k5eAaImAgFnS	vést
v	v	k7c6	v
letech	let	k1gInPc6	let
1550	[number]	k4	1550
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
přes	přes	k7c4	přes
200	[number]	k4	200
tisíc	tisíc	k4xCgInSc4	tisíc
vyšetřování	vyšetřování	k1gNnPc2	vyšetřování
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nichž	jenž	k3xRgFnPc2	jenž
vydala	vydat	k5eAaPmAgFnS	vydat
celkem	celkem	k6eAd1	celkem
asi	asi	k9	asi
3	[number]	k4	3
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
4	[number]	k4	4
tisíce	tisíc	k4xCgInPc1	tisíc
rozsudků	rozsudek	k1gInPc2	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaný	odhadovaný	k2eAgInSc1d1	odhadovaný
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
u	u	k7c2	u
různých	různý	k2eAgMnPc2d1	různý
autorů	autor	k1gMnPc2	autor
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
určitě	určitě	k6eAd1	určitě
však	však	k9	však
nepřekročil	překročit	k5eNaPmAgInS	překročit
10	[number]	k4	10
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
běsnění	běsnění	k1gNnSc6	běsnění
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dvaceti	dvacet	k4xCc6	dvacet
letech	let	k1gInPc6	let
fungování	fungování	k1gNnSc2	fungování
španělské	španělský	k2eAgFnSc2d1	španělská
inkvizice	inkvizice	k1gFnSc2	inkvizice
počet	počet	k1gInSc1	počet
rozsudků	rozsudek	k1gInPc2	rozsudek
smrti	smrt	k1gFnSc2	smrt
nikdy	nikdy	k6eAd1	nikdy
nepřekročil	překročit	k5eNaPmAgInS	překročit
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
nebylo	být	k5eNaImAgNnS	být
zabíjet	zabíjet	k5eAaImF	zabíjet
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
zastrašovat	zastrašovat	k5eAaImF	zastrašovat
<g/>
.	.	kIx.	.
<g/>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
inkvizice	inkvizice	k1gFnSc1	inkvizice
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
dvousetletého	dvousetletý	k2eAgNnSc2d1	dvousetleté
působení	působení	k1gNnSc2	působení
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1760	[number]	k4	1760
<g/>
)	)	kIx)	)
vedla	vést	k5eAaImAgFnS	vést
na	na	k7c4	na
30	[number]	k4	30
ticíc	ticit	k5eAaImSgFnS	ticit
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
bylo	být	k5eAaImAgNnS	být
1	[number]	k4	1
175	[number]	k4	175
osob	osoba	k1gFnPc2	osoba
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
(	(	kIx(	(
<g/>
dalších	další	k2eAgNnPc2d1	další
633	[number]	k4	633
bylo	být	k5eAaImAgNnS	být
upáleno	upálen	k2eAgNnSc1d1	upáleno
in	in	k?	in
effigie	effigie	k1gFnSc2	effigie
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
obrazně	obrazně	k6eAd1	obrazně
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohlasy	ohlas	k1gInPc1	ohlas
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
===	===	k?	===
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
inkvizice	inkvizice	k1gFnSc1	inkvizice
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
tématem	téma	k1gNnSc7	téma
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
literatuře	literatura	k1gFnSc6	literatura
i	i	k8xC	i
filmu	film	k1gInSc6	film
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
,	,	kIx,	,
Jáma	jáma	k1gFnSc1	jáma
a	a	k8xC	a
kyvadlo	kyvadlo	k1gNnSc1	kyvadlo
</s>
</p>
<p>
<s>
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
,	,	kIx,	,
Candide	Candid	k1gMnSc5	Candid
</s>
</p>
<p>
<s>
Goya	Goya	k1gFnSc1	Goya
<g/>
:	:	kIx,	:
obrazy	obraz	k1gInPc1	obraz
</s>
</p>
<p>
<s>
skeč	skeč	k1gInSc1	skeč
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Monty	Monta	k1gFnSc2	Monta
Pythonův	Pythonův	k2eAgInSc4d1	Pythonův
létající	létající	k2eAgInSc4d1	létající
cirkus	cirkus	k1gInSc4	cirkus
–	–	k?	–
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
zlidověla	zlidovět	k5eAaPmAgFnS	zlidovět
hláška	hláška	k1gFnSc1	hláška
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
nečeká	čekat	k5eNaImIp3nS	čekat
španělskou	španělský	k2eAgFnSc4d1	španělská
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Fjodor	Fjodor	k1gMnSc1	Fjodor
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Dostojevskij	Dostojevskij	k1gMnSc1	Dostojevskij
<g/>
,	,	kIx,	,
Bratři	bratr	k1gMnPc1	bratr
Karamazovi	Karamazův	k2eAgMnPc1d1	Karamazův
<g/>
:	:	kIx,	:
podobenství	podobenství	k1gNnSc4	podobenství
o	o	k7c6	o
Velkém	velký	k2eAgMnSc6d1	velký
inkvizitorovi	inkvizitor	k1gMnSc6	inkvizitor
</s>
</p>
<p>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
Goyovy	Goyův	k2eAgInPc4d1	Goyův
přízraky	přízrak	k1gInPc4	přízrak
(	(	kIx(	(
<g/>
Goya	Goy	k2eAgFnSc1d1	Goya
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Ghosts	Ghostsa	k1gFnPc2	Ghostsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Římská	římský	k2eAgFnSc1d1	římská
inkvizice	inkvizice	k1gFnSc1	inkvizice
==	==	k?	==
</s>
</p>
<p>
<s>
Papež	Papež	k1gMnSc1	Papež
Pavel	Pavel	k1gMnSc1	Pavel
III	III	kA	III
<g/>
.	.	kIx.	.
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
roku	rok	k1gInSc2	rok
1542	[number]	k4	1542
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Svatá	svatý	k2eAgFnSc1d1	svatá
kongregace	kongregace	k1gFnSc1	kongregace
obecné	obecný	k2eAgFnSc2d1	obecná
inkvizice	inkvizice	k1gFnSc2	inkvizice
stálou	stálý	k2eAgFnSc4d1	stálá
kongregaci	kongregace	k1gFnSc4	kongregace
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
starat	starat	k5eAaImF	starat
o	o	k7c4	o
integritu	integrita	k1gFnSc4	integrita
víry	víra	k1gFnSc2	víra
a	a	k8xC	a
zkoumat	zkoumat	k5eAaImF	zkoumat
chybné	chybný	k2eAgFnPc4d1	chybná
a	a	k8xC	a
falešné	falešný	k2eAgFnPc4d1	falešná
nauky	nauka	k1gFnPc4	nauka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
úřad	úřad	k1gInSc1	úřad
byl	být	k5eAaImAgInS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
též	též	k9	též
Svaté	svatý	k2eAgNnSc4d1	svaté
oficium	oficium	k1gNnSc4	oficium
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
inkvizicí	inkvizice	k1gFnSc7	inkvizice
je	být	k5eAaImIp3nS	být
spojen	spojen	k2eAgInSc1d1	spojen
např.	např.	kA	např.
proces	proces	k1gInSc1	proces
s	s	k7c7	s
Galileem	Galileum	k1gNnSc7	Galileum
v	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nebo	nebo	k8xC	nebo
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
vypracovávala	vypracovávat	k5eAaImAgFnS	vypracovávat
a	a	k8xC	a
stále	stále	k6eAd1	stále
rozšiřovala	rozšiřovat	k5eAaImAgFnS	rozšiřovat
Index	index	k1gInSc4	index
zakázané	zakázaný	k2eAgFnSc2d1	zakázaná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
například	například	k6eAd1	například
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
zhruba	zhruba	k6eAd1	zhruba
třetina	třetina	k1gFnSc1	třetina
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Obžalovaný	obžalovaný	k1gMnSc1	obžalovaný
byl	být	k5eAaImAgMnS	být
vyslýchán	vyslýchat	k5eAaImNgMnS	vyslýchat
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
dvou	dva	k4xCgMnPc2	dva
svědků	svědek	k1gMnPc2	svědek
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
předloženo	předložen	k2eAgNnSc1d1	předloženo
shrnutí	shrnutí	k1gNnSc1	shrnutí
obžaloby	obžaloba	k1gFnSc2	obžaloba
<g/>
.	.	kIx.	.
</s>
<s>
Závěry	závěr	k1gInPc1	závěr
inkvizice	inkvizice	k1gFnSc2	inkvizice
se	se	k3xPyFc4	se
recitovaly	recitovat	k5eAaImAgFnP	recitovat
před	před	k7c7	před
publikem	publikum	k1gNnSc7	publikum
a	a	k8xC	a
kajícník	kajícník	k1gMnSc1	kajícník
odpřisáhl	odpřisáhnout	k5eAaPmAgMnS	odpřisáhnout
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
s	s	k7c7	s
rukou	ruka	k1gFnSc7	ruka
položenou	položený	k2eAgFnSc7d1	položená
na	na	k7c6	na
Bibli	bible	k1gFnSc6	bible
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
trestů	trest	k1gInPc2	trest
se	se	k3xPyFc4	se
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
od	od	k7c2	od
návštěvy	návštěva	k1gFnSc2	návštěva
kostela	kostel	k1gInSc2	kostel
přes	přes	k7c4	přes
pouti	pouť	k1gFnPc4	pouť
<g/>
,	,	kIx,	,
nesení	nesení	k1gNnSc4	nesení
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
obviněný	obviněný	k2eAgMnSc1d1	obviněný
nechtěl	chtít	k5eNaImAgMnS	chtít
přísahat	přísahat	k5eAaImF	přísahat
<g/>
,	,	kIx,	,
až	až	k8xS	až
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
se	se	k3xPyFc4	se
uskutečňovala	uskutečňovat	k5eAaImAgFnS	uskutečňovat
upálením	upálení	k1gNnSc7	upálení
u	u	k7c2	u
kůlu	kůl	k1gInSc2	kůl
a	a	k8xC	a
pokud	pokud	k8xS	pokud
obviněný	obviněný	k2eAgMnSc1d1	obviněný
zemřel	zemřít	k5eAaPmAgMnS	zemřít
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
procesu	proces	k1gInSc2	proces
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
exhumovány	exhumován	k2eAgInPc1d1	exhumován
a	a	k8xC	a
spáleny	spálen	k2eAgInPc1d1	spálen
<g/>
.	.	kIx.	.
</s>
<s>
Poprava	poprava	k1gFnSc1	poprava
a	a	k8xC	a
doživotní	doživotní	k2eAgNnSc1d1	doživotní
vězení	vězení	k1gNnSc1	vězení
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
konfiskací	konfiskace	k1gFnSc7	konfiskace
majetku	majetek	k1gInSc2	majetek
odsouzeného	odsouzený	k1gMnSc2	odsouzený
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Význam	význam	k1gInSc1	význam
inkvizice	inkvizice	k1gFnSc2	inkvizice
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zvolna	zvolna	k6eAd1	zvolna
upadal	upadat	k5eAaPmAgMnS	upadat
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
protestantskou	protestantský	k2eAgFnSc7d1	protestantská
reformací	reformace	k1gFnSc7	reformace
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
církví	církev	k1gFnPc2	církev
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
na	na	k7c6	na
církvi	církev	k1gFnSc6	církev
římské	římský	k2eAgFnSc6d1	římská
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
moci	moc	k1gFnSc2	moc
absolutistických	absolutistický	k2eAgMnPc2d1	absolutistický
panovníků	panovník	k1gMnPc2	panovník
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
již	již	k6eAd1	již
nebyli	být	k5eNaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
podřizovat	podřizovat	k5eAaImF	podřizovat
se	se	k3xPyFc4	se
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
inkvizice	inkvizice	k1gFnSc2	inkvizice
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
institucí	instituce	k1gFnPc2	instituce
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
změnil	změnit	k5eAaPmAgMnS	změnit
Pius	Pius	k1gMnSc1	Pius
X.	X.	kA	X.
kongregaci	kongregace	k1gFnSc4	kongregace
název	název	k1gInSc4	název
na	na	k7c4	na
Posvátná	posvátný	k2eAgNnPc4d1	posvátné
kongregace	kongregace	k1gFnSc2	kongregace
svatého	svatý	k2eAgInSc2d1	svatý
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
ji	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Kongregaci	kongregace	k1gFnSc4	kongregace
pro	pro	k7c4	pro
nauku	nauka	k1gFnSc4	nauka
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
omluvil	omluvit	k5eAaPmAgMnS	omluvit
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
spáchali	spáchat	k5eAaPmAgMnP	spáchat
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
včetně	včetně	k7c2	včetně
přehmatů	přehmat	k1gInPc2	přehmat
během	během	k7c2	během
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
,	,	kIx,	,
systematického	systematický	k2eAgInSc2d1	systematický
útlaku	útlak	k1gInSc2	útlak
prováděného	prováděný	k2eAgInSc2d1	prováděný
církevními	církevní	k2eAgMnPc7d1	církevní
hodnostáři	hodnostář	k1gMnPc7	hodnostář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
chráněny	chránit	k5eAaImNgFnP	chránit
ortodoxní	ortodoxní	k2eAgFnPc1d1	ortodoxní
doktríny	doktrína	k1gFnPc1	doktrína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Citát	citát	k1gInSc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GRIGULEVIČ	GRIGULEVIČ	kA	GRIGULEVIČ
<g/>
,	,	kIx,	,
I.	I.	kA	I.
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Dějiny	dějiny	k1gFnPc1	dějiny
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOXE	fox	k1gInSc5	fox
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Foxe	fox	k1gInSc5	fox
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Book	Book	k1gInSc1	Book
of	of	k?	of
Martyrs	Martyrs	k1gInSc1	Martyrs
<g/>
,	,	kIx,	,
Bridge-Logos	Bridge-Logos	k1gInSc1	Bridge-Logos
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HROCH	Hroch	k1gMnSc1	Hroch
<g/>
,	,	kIx,	,
M.	M.	kA	M.
–	–	k?	–
SKÝBOVÁ	SKÝBOVÁ	kA	SKÝBOVÁ
<g/>
,	,	kIx,	,
Anna	Anna	k1gFnSc1	Anna
<g/>
:	:	kIx,	:
Králové	Král	k1gMnPc1	Král
<g/>
,	,	kIx,	,
kacíři	kacíř	k1gMnPc1	kacíř
<g/>
,	,	kIx,	,
inkvizitoři	inkvizitor	k1gMnPc1	inkvizitor
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JOHNSON	JOHNSON	kA	JOHNSON
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
židovského	židovský	k2eAgInSc2d1	židovský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Řevnice	řevnice	k1gFnSc1	řevnice
<g/>
:	:	kIx,	:
ROZMLUVY	rozmluva	k1gFnSc2	rozmluva
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85336	[number]	k4	85336
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAMEN	kamna	k1gNnPc2	kamna
<g/>
,	,	kIx,	,
H.	H.	kA	H.
The	The	k1gFnSc1	The
Spanish	Spanish	k1gMnSc1	Spanish
Inquisition	Inquisition	k1gInSc1	Inquisition
<g/>
:	:	kIx,	:
A	a	k9	a
Historical	Historical	k1gMnPc1	Historical
Revision	Revision	k1gInSc4	Revision
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Haven	Haven	k1gInSc1	Haven
<g/>
:	:	kIx,	:
Yale	Yale	k1gInSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETERS	PETERS	kA	PETERS
<g/>
,	,	kIx,	,
E.	E.	kA	E.
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Inquisition	Inquisition	k1gInSc1	Inquisition
<g/>
.	.	kIx.	.
</s>
<s>
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
EVANS	EVANS	kA	EVANS
<g/>
,	,	kIx,	,
G.	G.	kA	G.
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Stručné	stručný	k2eAgFnPc4d1	stručná
dějiny	dějiny	k1gFnPc4	dějiny
kacířství	kacířství	k1gNnSc2	kacířství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
globator	globator	k1gInSc1	globator
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
621	[number]	k4	621
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MRÁČEK	Mráček	k1gMnSc1	Mráček
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Upalování	upalování	k1gNnSc1	upalování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
a	a	k8xC	a
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7266	[number]	k4	7266
<g/>
-	-	kIx~	-
<g/>
229	[number]	k4	229
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GIVEN	GIVEN	kA	GIVEN
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
,	,	kIx,	,
B.	B.	kA	B.
Inkvizice	inkvizice	k1gFnSc1	inkvizice
a	a	k8xC	a
středověká	středověký	k2eAgFnSc1d1	středověká
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Neratovice	Neratovice	k1gFnSc1	Neratovice
<g/>
:	:	kIx,	:
Verbum	verbum	k1gNnSc1	verbum
Publishing	Publishing	k1gInSc1	Publishing
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
903920	[number]	k4	903920
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Inkviziční	inkviziční	k2eAgNnSc1d1	inkviziční
řízení	řízení	k1gNnSc1	řízení
</s>
</p>
<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
inkvizice	inkvizice	k1gFnSc1	inkvizice
</s>
</p>
<p>
<s>
Čarodějnické	čarodějnický	k2eAgInPc1d1	čarodějnický
procesy	proces	k1gInPc1	proces
</s>
</p>
<p>
<s>
Kacířství	kacířství	k1gNnSc1	kacířství
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
velkých	velký	k2eAgMnPc2d1	velký
inkvizitorů	inkvizitor	k1gMnPc2	inkvizitor
Španělska	Španělsko	k1gNnSc2	Španělsko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Inkvizice	inkvizice	k1gFnSc2	inkvizice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
inkvizitor	inkvizitor	k1gMnSc1	inkvizitor
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
KÁNSKÝ	KÁNSKÝ	kA	KÁNSKÝ
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Inkvizice	inkvizice	k1gFnSc2	inkvizice
(	(	kIx(	(
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
KŘESŤANSKÉ	křesťanský	k2eAgNnSc1d1	křesťanské
SPOLEČENSTVÍ	společenství	k1gNnSc1	společenství
VŠZ	VŠZ	kA	VŠZ
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://www.hks.re/wiki/inkvizice	[url]	k1gFnSc2	http://www.hks.re/wiki/inkvizice
</s>
</p>
<p>
<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
inkvizice	inkvizice	k1gFnSc1	inkvizice
nebyla	být	k5eNaImAgFnS	být
tak	tak	k9	tak
špatná	špatná	k1gFnSc1	špatná
–	–	k?	–
Miloš	Miloš	k1gMnSc1	Miloš
Kaláb	Kaláb	k1gMnSc1	Kaláb
<g/>
,	,	kIx,	,
17.6	[number]	k4	17.6
<g/>
.2004	.2004	k4	.2004
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc4d1	britský
listy	list	k1gInPc4	list
</s>
</p>
<p>
<s>
Relat	Relat	k1gInSc1	Relat
<g/>
.	.	kIx.	.
podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
španělské	španělský	k2eAgFnSc6d1	španělská
inkvizici	inkvizice	k1gFnSc6	inkvizice
</s>
</p>
<p>
<s>
Pravda	pravda	k9	pravda
a	a	k8xC	a
lež	lež	k1gFnSc4	lež
o	o	k7c6	o
čarodějnických	čarodějnický	k2eAgInPc6d1	čarodějnický
procesech	proces	k1gInPc6	proces
</s>
</p>
<p>
<s>
Inkvizice	inkvizice	k1gFnSc1	inkvizice
a	a	k8xC	a
počet	počet	k1gInSc1	počet
mrtvých	mrtvý	k1gMnPc2	mrtvý
</s>
</p>
