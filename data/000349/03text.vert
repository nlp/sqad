<s>
Máj	máj	k1gFnSc1	máj
je	být	k5eAaImIp3nS	být
lyrickoepická	lyrickoepický	k2eAgFnSc1d1	lyrickoepická
skladba	skladba	k1gFnSc1	skladba
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
vydaná	vydaný	k2eAgFnSc1d1	vydaná
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
dílo	dílo	k1gNnSc4	dílo
českého	český	k2eAgInSc2d1	český
literárního	literární	k2eAgInSc2d1	literární
romantismu	romantismus	k1gInSc2	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skladbě	skladba	k1gFnSc6	skladba
zcela	zcela	k6eAd1	zcela
vybočil	vybočit	k5eAaPmAgMnS	vybočit
z	z	k7c2	z
obrozeneckého	obrozenecký	k2eAgInSc2d1	obrozenecký
programu	program	k1gInSc2	program
1	[number]	k4	1
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
pojal	pojmout	k5eAaPmAgMnS	pojmout
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xS	jako
osobní	osobní	k2eAgFnSc4d1	osobní
zpověď	zpověď	k1gFnSc4	zpověď
rozervaného	rozervaný	k2eAgMnSc4d1	rozervaný
romantického	romantický	k2eAgMnSc4d1	romantický
člověka	člověk	k1gMnSc4	člověk
plného	plný	k2eAgInSc2d1	plný
nejistot	nejistota	k1gFnPc2	nejistota
a	a	k8xC	a
otázek	otázka	k1gFnPc2	otázka
po	po	k7c6	po
smyslu	smysl	k1gInSc6	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
událostí	událost	k1gFnSc7	událost
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
byl	být	k5eAaImAgInS	být
poblíž	poblíž	k6eAd1	poblíž
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
zvaném	zvaný	k2eAgInSc6d1	zvaný
Na	na	k7c6	na
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
popraven	popraven	k2eAgInSc4d1	popraven
lámáním	lámání	k1gNnSc7	lámání
v	v	k7c6	v
kole	kolo	k1gNnSc6	kolo
Hynek	Hynek	k1gMnSc1	Hynek
(	(	kIx(	(
<g/>
Ignác	Ignác	k1gMnSc1	Ignác
<g/>
)	)	kIx)	)
Schiffner	Schiffner	k1gMnSc1	Schiffner
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zavraždil	zavraždit	k5eAaPmAgMnS	zavraždit
v	v	k7c6	v
Dubé	Dubá	k1gFnSc6	Dubá
u	u	k7c2	u
Doks	Doksy	k1gInPc2	Doksy
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
toto	tento	k3xDgNnSc4	tento
vyprávění	vyprávění	k1gNnSc4	vyprávění
zaslechl	zaslechnout	k5eAaPmAgMnS	zaslechnout
o	o	k7c4	o
šedesát	šedesát	k4xCc4	šedesát
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
některého	některý	k3yIgInSc2	některý
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
pobytů	pobyt	k1gInPc2	pobyt
u	u	k7c2	u
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Máchova	Máchův	k2eAgNnSc2d1	Máchovo
jezera	jezero	k1gNnSc2	jezero
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
známého	známý	k1gMnSc2	známý
<g/>
,	,	kIx,	,
hospodského	hospodský	k1gMnSc2	hospodský
Týce	Týc	k1gMnSc2	Týc
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
Máje	máj	k1gFnSc2	máj
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Katusicích	Katusice	k1gFnPc6	Katusice
u	u	k7c2	u
Mladé	mladý	k2eAgFnSc6d1	mladá
Boleslavi	Boleslaev	k1gFnSc6	Boleslaev
v	v	k7c6	v
pozůstalosti	pozůstalost	k1gFnSc6	pozůstalost
obrozence	obrozenec	k1gMnSc2	obrozenec
Jana	Jan	k1gMnSc2	Jan
Nepomuka	Nepomuk	k1gMnSc2	Nepomuk
Krouského	Krouský	k2eAgMnSc2d1	Krouský
<g/>
.	.	kIx.	.
</s>
<s>
Obávaný	obávaný	k2eAgMnSc1d1	obávaný
vůdce	vůdce	k1gMnSc1	vůdce
loupežníků	loupežník	k1gMnPc2	loupežník
Vilém	Vilém	k1gMnSc1	Vilém
zabil	zabít	k5eAaPmAgMnS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
soka	sok	k1gMnSc4	sok
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
však	však	k9	však
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Vězeň	vězeň	k1gMnSc1	vězeň
Vilém	Vilém	k1gMnSc1	Vilém
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
osudu	osud	k1gInSc6	osud
<g/>
,	,	kIx,	,
loučí	loučit	k5eAaImIp3nS	loučit
se	se	k3xPyFc4	se
se	s	k7c7	s
životem	život	k1gInSc7	život
a	a	k8xC	a
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
milou	milý	k2eAgFnSc4d1	Milá
Jarmilu	Jarmila	k1gFnSc4	Jarmila
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
vinu	vina	k1gFnSc4	vina
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
vidí	vidět	k5eAaImIp3nS	vidět
jako	jako	k9	jako
oběť	oběť	k1gFnSc1	oběť
sobeckého	sobecký	k2eAgMnSc2d1	sobecký
<g/>
,	,	kIx,	,
nemilujícího	milující	k2eNgMnSc2d1	nemilující
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
soucítí	soucítit	k5eAaImIp3nS	soucítit
též	též	k9	též
žalářník	žalářník	k1gMnSc1	žalářník
<g/>
,	,	kIx,	,
zdrcený	zdrcený	k2eAgMnSc1d1	zdrcený
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
mu	on	k3xPp3gMnSc3	on
o	o	k7c4	o
sobě	se	k3xPyFc3	se
Vilém	Vilém	k1gMnSc1	Vilém
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hodinách	hodina	k1gFnPc6	hodina
plných	plný	k2eAgFnPc2d1	plná
samomluv	samomluva	k1gFnPc2	samomluva
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
smrt	smrt	k1gFnSc4	smrt
jako	jako	k8xC	jako
definitivní	definitivní	k2eAgInSc4d1	definitivní
konec	konec	k1gInSc4	konec
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
otcovrah	otcovrah	k1gMnSc1	otcovrah
brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
odveden	odvést	k5eAaPmNgInS	odvést
na	na	k7c4	na
popraviště	popraviště	k1gNnSc4	popraviště
<g/>
,	,	kIx,	,
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
modlícím	modlící	k2eAgMnSc7d1	modlící
se	se	k3xPyFc4	se
davem	dav	k1gInSc7	dav
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
májové	májový	k2eAgFnSc2d1	Májová
přírody	příroda	k1gFnSc2	příroda
se	se	k3xPyFc4	se
v	v	k7c6	v
slzách	slza	k1gFnPc6	slza
loučí	loučit	k5eAaImIp3nP	loučit
s	s	k7c7	s
milovanou	milovaný	k2eAgFnSc7d1	milovaná
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
příběhu	příběh	k1gInSc3	příběh
předchází	předcházet	k5eAaImIp3nS	předcházet
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
zpěvu	zpěv	k1gInSc2	zpěv
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Jarmila	Jarmila	k1gFnSc1	Jarmila
marně	marně	k6eAd1	marně
čeká	čekat	k5eAaImIp3nS	čekat
za	za	k7c2	za
májového	májový	k2eAgInSc2d1	májový
večera	večer	k1gInSc2	večer
na	na	k7c4	na
Viléma	Vilém	k1gMnSc4	Vilém
a	a	k8xC	a
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
se	se	k3xPyFc4	se
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
osudu	osud	k1gInSc6	osud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
zpěvu	zpěv	k1gInSc6	zpěv
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
popravy	poprava	k1gFnSc2	poprava
poutník	poutník	k1gMnSc1	poutník
Hynek	Hynek	k1gMnSc1	Hynek
(	(	kIx(	(
<g/>
představující	představující	k2eAgFnSc1d1	představující
samotného	samotný	k2eAgInSc2d1	samotný
Máchu	mách	k1gInSc2	mách
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterého	který	k3yIgMnSc4	který
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
uvidí	uvidět	k5eAaPmIp3nP	uvidět
kostlivce	kostlivec	k1gMnSc4	kostlivec
v	v	k7c6	v
kole	kolo	k1gNnSc6	kolo
a	a	k8xC	a
ztotožňuje	ztotožňovat	k5eAaImIp3nS	ztotožňovat
se	se	k3xPyFc4	se
s	s	k7c7	s
Vilémem	Vilém	k1gMnSc7	Vilém
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
na	na	k7c6	na
tváři	tvář	k1gFnSc6	tvář
lehký	lehký	k2eAgInSc1d1	lehký
smích	smích	k1gInSc1	smích
<g/>
,	,	kIx,	,
hluboký	hluboký	k2eAgInSc1d1	hluboký
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
žal	žal	k1gInSc1	žal
<g/>
...	...	k?	...
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gFnSc1	máj
má	mít	k5eAaImIp3nS	mít
4	[number]	k4	4
zpěvy	zpěv	k1gInPc4	zpěv
a	a	k8xC	a
2	[number]	k4	2
intermezza	intermezzo	k1gNnSc2	intermezzo
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgFnSc3d1	celá
skladbě	skladba	k1gFnSc3	skladba
předchází	předcházet	k5eAaImIp3nS	předcházet
věnování	věnování	k1gNnSc1	věnování
pražskému	pražský	k2eAgMnSc3d1	pražský
měšťanovi	měšťan	k1gMnSc3	měšťan
Hynku	Hynek	k1gMnSc3	Hynek
Kommovi	Komm	k1gMnSc3	Komm
-	-	kIx~	-
báseň	báseň	k1gFnSc1	báseň
Čechové	Čechová	k1gFnSc2	Čechová
jsou	být	k5eAaImIp3nP	být
národ	národ	k1gInSc1	národ
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
vlastenecké	vlastenecký	k2eAgFnPc4d1	vlastenecká
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
nízké	nízký	k2eAgFnSc3d1	nízká
umělecké	umělecký	k2eAgFnSc3d1	umělecká
úrovni	úroveň	k1gFnSc3	úroveň
bývá	bývat	k5eAaImIp3nS	bývat
vykládána	vykládat	k5eAaImNgFnS	vykládat
jako	jako	k9	jako
parodie	parodie	k1gFnSc1	parodie
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
metrem	metr	k1gInSc7	metr
Máje	máj	k1gInSc2	máj
je	být	k5eAaImIp3nS	být
jamb	jamb	k1gInSc1	jamb
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
metafor	metafora	k1gFnPc2	metafora
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc4d1	častý
řetězce	řetězec	k1gInPc4	řetězec
oxymorón	oxymorón	k1gInSc1	oxymorón
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
zbortěné	zbortěný	k2eAgFnPc1d1	zbortěný
harfy	harfa	k1gFnPc1	harfa
tón	tón	k1gInSc1	tón
<g/>
,	,	kIx,	,
ztrhané	ztrhaný	k2eAgFnPc1d1	ztrhaná
strůny	strůna	k1gFnPc1	strůna
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
zašlého	zašlý	k2eAgInSc2d1	zašlý
věku	věk	k1gInSc2	věk
děj	děj	k1gInSc1	děj
<g/>
,	,	kIx,	,
umřelé	umřelý	k2eAgFnPc1d1	umřelá
hvězdy	hvězda	k1gFnPc1	hvězda
svit	svita	k1gFnPc2	svita
<g/>
,	,	kIx,	,
zašlé	zašlý	k2eAgFnPc1d1	zašlá
bludice	bludice	k1gFnPc1	bludice
pouť	pouť	k1gFnSc1	pouť
<g/>
,	,	kIx,	,
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
milenky	milenka	k1gFnPc1	milenka
cit	cit	k1gInSc1	cit
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hojné	hojný	k2eAgInPc1d1	hojný
jsou	být	k5eAaImIp3nP	být
básnické	básnický	k2eAgInPc1d1	básnický
přívlastky	přívlastek	k1gInPc1	přívlastek
<g/>
,	,	kIx,	,
spojování	spojování	k1gNnPc1	spojování
protikladných	protikladný	k2eAgFnPc2d1	protikladná
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
kontrastů	kontrast	k1gInPc2	kontrast
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
/	/	kIx~	/
<g/>
tma	tma	k1gFnSc1	tma
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
<g/>
/	/	kIx~	/
<g/>
smrt	smrt	k1gFnSc1	smrt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnSc1	kombinace
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
růžové	růžový	k2eAgNnSc4d1	růžové
nebe	nebe	k1gNnSc4	nebe
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
mlhy	mlha	k1gFnPc1	mlha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyjádření	vyjádření	k1gNnSc1	vyjádření
pohybu	pohyb	k1gInSc2	pohyb
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
napětí	napětí	k1gNnSc1	napětí
dynamickými	dynamický	k2eAgNnPc7d1	dynamické
slovesy	sloveso	k1gNnPc7	sloveso
(	(	kIx(	(
<g/>
zasvítnuto	zasvítnut	k2eAgNnSc1d1	zasvítnut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metafory	metafora	k1gFnPc1	metafora
(	(	kIx(	(
<g/>
bledá	bledý	k2eAgFnSc1d1	bledá
tvář	tvář	k1gFnSc1	tvář
luny	luna	k1gFnSc2	luna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
metonymie	metonymie	k1gFnSc1	metonymie
(	(	kIx(	(
<g/>
hrdliččin	hrdliččin	k2eAgMnSc1d1	hrdliččin
zval	zvát	k5eAaImAgMnS	zvát
ku	k	k7c3	k
lásce	láska	k1gFnSc3	láska
hlas	hlas	k1gInSc1	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
inverze	inverze	k1gFnSc1	inverze
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
holoubátko	holoubátko	k1gNnSc1	holoubátko
sněhobílé	sněhobílý	k2eAgFnPc1d1	sněhobílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
gradace	gradace	k1gFnSc1	gradace
(	(	kIx(	(
<g/>
rychlý	rychlý	k2eAgInSc1d1	rychlý
to	ten	k3xDgNnSc1	ten
člunek	člunek	k1gInSc4	člunek
<g/>
,	,	kIx,	,
blíže	blízce	k6eAd2	blízce
a	a	k8xC	a
blíže	blízce	k6eAd2	blízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kakofonie	kakofonie	k1gFnSc1	kakofonie
(	(	kIx(	(
<g/>
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eufonie	eufonie	k1gFnSc1	eufonie
a	a	k8xC	a
epizeuxis	epizeuxis	k1gFnSc1	epizeuxis
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
zpěv	zpěv	k1gInSc1	zpěv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
literárním	literární	k2eAgInSc7d1	literární
vzorem	vzor	k1gInSc7	vzor
Máje	máj	k1gFnSc2	máj
je	být	k5eAaImIp3nS	být
dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
poezie	poezie	k1gFnSc1	poezie
polských	polský	k2eAgMnPc2d1	polský
romantiků	romantik	k1gMnPc2	romantik
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
Mácha	Mácha	k1gMnSc1	Mácha
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
znal	znát	k5eAaImAgMnS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
mu	on	k3xPp3gNnSc3	on
byla	být	k5eAaImAgFnS	být
také	také	k9	také
barokní	barokní	k2eAgFnSc1d1	barokní
poezie	poezie	k1gFnSc1	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
každé	každý	k3xTgNnSc4	každý
velké	velký	k2eAgNnSc4d1	velké
umělecké	umělecký	k2eAgNnSc4d1	umělecké
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
i	i	k9	i
Máj	máj	k1gFnSc1	máj
interpretovat	interpretovat	k5eAaBmF	interpretovat
mnoha	mnoho	k4c2	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
autor	autor	k1gMnSc1	autor
vydal	vydat	k5eAaPmAgMnS	vydat
báseň	báseň	k1gFnSc4	báseň
s	s	k7c7	s
poznámkou	poznámka	k1gFnSc7	poznámka
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
účelem	účel	k1gInSc7	účel
skladby	skladba	k1gFnSc2	skladba
je	být	k5eAaImIp3nS	být
oslava	oslava	k1gFnSc1	oslava
jarní	jarní	k2eAgFnSc2d1	jarní
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
uvození	uvození	k1gNnSc4	uvození
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
zmást	zmást	k5eAaPmF	zmást
dobovou	dobový	k2eAgFnSc4d1	dobová
cenzuru	cenzura	k1gFnSc4	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
i	i	k9	i
dnes	dnes	k6eAd1	dnes
bývá	bývat	k5eAaImIp3nS	bývat
smysl	smysl	k1gInSc4	smysl
Máje	máj	k1gFnSc2	máj
často	často	k6eAd1	často
redukován	redukovat	k5eAaBmNgInS	redukovat
na	na	k7c4	na
pouhou	pouhý	k2eAgFnSc4d1	pouhá
apoteózu	apoteóza	k1gFnSc4	apoteóza
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Máchova	Máchův	k2eAgFnSc1d1	Máchova
socha	socha	k1gFnSc1	socha
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
i	i	k9	i
memorování	memorování	k1gNnSc1	memorování
úvodních	úvodní	k2eAgInPc2d1	úvodní
veršů	verš	k1gInPc2	verš
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
zaměřit	zaměřit	k5eAaPmF	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
verše	verš	k1gInPc4	verš
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
motiv	motiv	k1gInSc1	motiv
země-matky	zeměatka	k1gFnSc2	země-matka
se	s	k7c7	s
steskem	stesk	k1gInSc7	stesk
po	po	k7c6	po
uplynulém	uplynulý	k2eAgInSc6d1	uplynulý
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nejhlouběji	hluboko	k6eAd3	hluboko
však	však	k9	však
sahá	sahat	k5eAaImIp3nS	sahat
zpěv	zpěv	k1gInSc1	zpěv
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Vilém	Vilém	k1gMnSc1	Vilém
čekající	čekající	k2eAgMnSc1d1	čekající
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
nad	nad	k7c7	nad
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
a	a	k8xC	a
tajemnou	tajemný	k2eAgFnSc7d1	tajemná
vinou	vina	k1gFnSc7	vina
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
musí	muset	k5eAaImIp3nP	muset
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
subjektivně	subjektivně	k6eAd1	subjektivně
necítí	cítit	k5eNaImIp3nS	cítit
viníkem	viník	k1gMnSc7	viník
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
verše	verš	k1gInPc1	verš
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
obavu	obava	k1gFnSc4	obava
ze	z	k7c2	z
smrti	smrt	k1gFnSc2	smrt
jako	jako	k8xC	jako
prázdnoty	prázdnota	k1gFnSc2	prázdnota
a	a	k8xC	a
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
kritika	kritika	k1gFnSc1	kritika
nejvíce	hodně	k6eAd3	hodně
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
měla	mít	k5eAaImAgFnS	mít
plnit	plnit	k5eAaImF	plnit
především	především	k6eAd1	především
osvětový	osvětový	k2eAgInSc4d1	osvětový
<g/>
,	,	kIx,	,
výchovný	výchovný	k2eAgInSc4d1	výchovný
a	a	k8xC	a
vlastenecký	vlastenecký	k2eAgInSc4d1	vlastenecký
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
pochopeno	pochopit	k5eAaPmNgNnS	pochopit
a	a	k8xC	a
přijato	přijmout	k5eAaPmNgNnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
byl	být	k5eAaImAgMnS	být
obviňován	obviňovat	k5eAaImNgMnS	obviňovat
jako	jako	k8xC	jako
nihilista	nihilista	k1gMnSc1	nihilista
a	a	k8xC	a
epigon	epigon	k1gMnSc1	epigon
Byronův	Byronův	k2eAgMnSc1d1	Byronův
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Ladislav	Ladislav	k1gMnSc1	Ladislav
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Josefu	Josef	k1gMnSc3	Josef
Krasoslavu	Krasoslav	k1gMnSc3	Krasoslav
Chmelenskému	Chmelenský	k2eAgMnSc3d1	Chmelenský
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1836	[number]	k4	1836
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mně	já	k3xPp1nSc6	já
se	se	k3xPyFc4	se
všecko	všecek	k3xTgNnSc1	všecek
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
máj	máj	k1gInSc1	máj
letošní	letošní	k2eAgInSc1d1	letošní
mstí	mstít	k5eAaImIp3nS	mstít
se	se	k3xPyFc4	se
na	na	k7c6	na
nás	my	k3xPp1nPc6	my
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k9	tak
šibeničnicky	šibeničnicky	k6eAd1	šibeničnicky
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
zpíváno	zpíván	k2eAgNnSc1d1	zpíváno
<g/>
.	.	kIx.	.
</s>
<s>
Nešťastný	šťastný	k2eNgMnSc1d1	nešťastný
básník	básník	k1gMnSc1	básník
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
romantikou	romantika	k1gFnSc7	romantika
<g/>
!	!	kIx.	!
</s>
<s>
Kozla	Kozel	k1gMnSc4	Kozel
nám	my	k3xPp1nPc3	my
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
<g/>
,	,	kIx,	,
ježto	ježto	k8xS	ježto
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
tudy	tudy	k6eAd1	tudy
i	i	k9	i
víno	víno	k1gNnSc1	víno
-	-	kIx~	-
blaho	blaho	k1gNnSc1	blaho
básníků	básník	k1gMnPc2	básník
-	-	kIx~	-
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
slyším	slyšet	k5eAaImIp1nS	slyšet
<g/>
,	,	kIx,	,
na	na	k7c4	na
všecky	všecek	k3xTgFnPc4	všecek
strany	strana	k1gFnPc4	strana
pomrzlo	pomrznout	k5eAaPmAgNnS	pomrznout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
také	také	k9	také
okurky	okurek	k1gInPc1	okurek
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
všecky	všecek	k3xTgInPc4	všecek
ty	ty	k3xPp2nSc1	ty
tam	tam	k6eAd1	tam
<g/>
!	!	kIx.	!
</s>
<s>
To	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgInPc4	ten
plody	plod	k1gInPc4	plod
hrozného	hrozný	k2eAgInSc2d1	hrozný
byronismu	byronismus	k1gInSc2	byronismus
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Jan	Jan	k1gMnSc1	Jan
Slavomír	Slavomír	k1gMnSc1	Slavomír
Tomíček	Tomíček	k1gMnSc1	Tomíček
šel	jít	k5eAaImAgMnS	jít
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
když	když	k8xS	když
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
včele	včela	k1gFnSc6	včela
z	z	k7c2	z
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1836	[number]	k4	1836
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
báseň	báseň	k1gFnSc1	báseň
jest	být	k5eAaImIp3nS	být
škvára	škvára	k1gFnSc1	škvára
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
z	z	k7c2	z
vymřelé	vymřelý	k2eAgFnSc2d1	vymřelá
sopky	sopka	k1gFnSc2	sopka
vyhozena	vyhodit	k5eAaPmNgFnS	vyhodit
mezi	mezi	k7c4	mezi
květiny	květina	k1gFnPc4	květina
padla	padnout	k5eAaPmAgFnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
květinách	květina	k1gFnPc6	květina
můžeme	moct	k5eAaImIp1nP	moct
míti	mít	k5eAaImF	mít
a	a	k8xC	a
máme	mít	k5eAaImIp1nP	mít
zalíbení	zalíbení	k1gNnSc4	zalíbení
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
ale	ale	k9	ale
v	v	k7c6	v
chladném	chladný	k2eAgInSc6d1	chladný
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
meteoru	meteor	k1gInSc6	meteor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
z	z	k7c2	z
rozervaných	rozervaný	k2eAgFnPc2d1	rozervaná
útrob	útroba	k1gFnPc2	útroba
vyvržen	vyvržen	k2eAgMnSc1d1	vyvržen
byl	být	k5eAaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
nenalézáme	nalézat	k5eNaImIp1nP	nalézat
nic	nic	k3yNnSc4	nic
krásného	krásný	k2eAgNnSc2d1	krásné
<g/>
,	,	kIx,	,
oživujícího	oživující	k2eAgMnSc2d1	oživující
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc4	nic
básnického	básnický	k2eAgNnSc2d1	básnické
v	v	k7c6	v
přísném	přísný	k2eAgInSc6d1	přísný
toho	ten	k3xDgNnSc2	ten
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc4	děj
tento	tento	k3xDgInSc4	tento
mohl	moct	k5eAaImAgInS	moct
vskutku	vskutku	k9	vskutku
básnické	básnický	k2eAgNnSc1d1	básnické
nabýti	nabýt	k5eAaPmF	nabýt
ceny	cena	k1gFnPc4	cena
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
byl	být	k5eAaImAgMnS	být
básník	básník	k1gMnSc1	básník
chtěl	chtít	k5eAaImAgMnS	chtít
roztrhnouti	roztrhnout	k5eAaPmF	roztrhnout
černou	černý	k2eAgFnSc4d1	černá
oponu	opona	k1gFnSc4	opona
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
spustila	spustit	k5eAaPmAgFnS	spustit
jeho	jeho	k3xOp3gMnPc3	jeho
obrazivost	obrazivost	k1gFnSc1	obrazivost
před	před	k7c7	před
každou	každý	k3xTgFnSc7	každý
radostnější	radostný	k2eAgFnSc7d2	radostnější
vyhlídkou	vyhlídka	k1gFnSc7	vyhlídka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podobně	podobně	k6eAd1	podobně
smýšlel	smýšlet	k5eAaImAgMnS	smýšlet
o	o	k7c6	o
Máji	máj	k1gInSc6	máj
i	i	k8xC	i
výše	vysoce	k6eAd2	vysoce
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Chmelenský	Chmelenský	k2eAgMnSc1d1	Chmelenský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
napsal	napsat	k5eAaBmAgMnS	napsat
v	v	k7c6	v
Časopise	časopis	k1gInSc6	časopis
Českého	český	k2eAgNnSc2d1	české
muzea	muzeum	k1gNnSc2	muzeum
(	(	kIx(	(
<g/>
č.	č.	k?	č.
10	[number]	k4	10
<g />
.	.	kIx.	.
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Máj	máj	k1gFnSc1	máj
p.	p.	k?	p.
Máchy	Mácha	k1gMnSc2	Mácha
nechci	chtít	k5eNaImIp1nS	chtít
posuzovati	posuzovat	k5eAaImF	posuzovat
<g/>
;	;	kIx,	;
nedospělať	dospělatit	k5eNaImRp2nS	dospělatit
tato	tento	k3xDgFnSc1	tento
báseň	báseň	k1gFnSc1	báseň
ještě	ještě	k9	ještě
pro	pro	k7c4	pro
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ačkoliv	ačkoliv	k8xS	ačkoliv
připouštěl	připouštět	k5eAaImAgMnS	připouštět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
cožkoliv	cožkolit	k5eAaPmDgInS	cožkolit
p.	p.	k?	p.
Mácha	Mácha	k1gMnSc1	Mácha
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
Máji	máj	k1gInSc6	máj
podal	podat	k5eAaPmAgMnS	podat
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc1	poezie
vysvítá	vysvítat	k5eAaImIp3nS	vysvítat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Obsah	obsah	k1gInSc1	obsah
básně	báseň	k1gFnSc2	báseň
jej	on	k3xPp3gInSc2	on
však	však	k9	však
"	"	kIx"	"
<g/>
příliš	příliš	k6eAd1	příliš
urážel	urážet	k5eAaPmAgMnS	urážet
<g/>
"	"	kIx"	"
a	a	k8xC	a
pak	pak	k6eAd1	pak
"	"	kIx"	"
<g/>
co	co	k8xS	co
nám	my	k3xPp1nPc3	my
do	do	k7c2	do
Byrona	Byron	k1gMnSc2	Byron
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
Pěj	pět	k5eAaImRp2nS	pět
podruhé	podruhé	k6eAd1	podruhé
p.	p.	k?	p.
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
moc	moc	k6eAd1	moc
vyšší	vysoký	k2eAgFnSc1d2	vyšší
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
ňadrách	ňadrách	k?	ňadrách
ozývá	ozývat	k5eAaImIp3nS	ozývat
<g/>
,	,	kIx,	,
a	a	k8xC	a
neohřívej	ohřívat	k5eNaImRp2nS	ohřívat
své	svůj	k3xOyFgNnSc4	svůj
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
nerozplamenávej	rozplamenávat	k5eNaPmRp2nS	rozplamenávat
svou	svůj	k3xOyFgFnSc4	svůj
obraznost	obraznost	k1gFnSc1	obraznost
na	na	k7c6	na
nepřirozených	přirozený	k2eNgInPc6d1	nepřirozený
výtvorech	výtvor	k1gInPc6	výtvor
anglického	anglický	k2eAgMnSc4d1	anglický
lorda	lord	k1gMnSc4	lord
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
vždy	vždy	k6eAd1	vždy
bojujícího	bojující	k2eAgMnSc4d1	bojující
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Také	také	k9	také
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
uznává	uznávat	k5eAaImIp3nS	uznávat
básnický	básnický	k2eAgInSc1d1	básnický
talent	talent	k1gInSc1	talent
Máchův	Máchův	k2eAgInSc1d1	Máchův
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Pan	Pan	k1gMnSc1	Pan
Mácha	Mácha	k1gMnSc1	Mácha
jest	být	k5eAaImIp3nS	být
básník	básník	k1gMnSc1	básník
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nS	by
však	však	k9	však
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
rád	rád	k6eAd1	rád
temné	temný	k2eAgInPc4d1	temný
zvuky	zvuk	k1gInPc4	zvuk
harfy	harfa	k1gFnSc2	harfa
jeho	jeho	k3xOp3gMnSc1	jeho
jinák	jinák	k1gMnSc1	jinák
slyšel	slyšet	k5eAaImAgMnS	slyšet
zvučeti	zvučet	k5eAaImF	zvučet
<g/>
,	,	kIx,	,
jinák	jinák	k1gInSc1	jinák
<g/>
,	,	kIx,	,
aspoň	aspoň	k9	aspoň
za	za	k7c2	za
věku	věk	k1gInSc2	věk
našeho	náš	k3xOp1gInSc2	náš
ne	ne	k9	ne
tak	tak	k6eAd1	tak
nečesky	česky	k6eNd1	česky
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Není-liž	Neníiž	k1gFnSc1	Není-liž
nic	nic	k6eAd1	nic
<g />
.	.	kIx.	.
</s>
<s>
potřebnějšího	potřební	k2eAgInSc2d2	potřební
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
půvabnějšího	půvabný	k2eAgNnSc2d2	půvabnější
v	v	k7c6	v
stoletím	století	k1gNnSc7	století
našem	náš	k3xOp1gNnSc6	náš
<g/>
,	,	kIx,	,
nic	nic	k3yNnSc1	nic
důležitějšího	důležitý	k2eAgNnSc2d2	důležitější
v	v	k7c6	v
životě	život	k1gInSc6	život
než	než	k8xS	než
vrahovov	vrahovov	k1gInSc1	vrahovov
rozbírání	rozbírání	k1gNnSc2	rozbírání
neznámého	známý	k2eNgNnSc2d1	neznámé
'	'	kIx"	'
<g/>
nic	nic	k3yNnSc1	nic
<g/>
'	'	kIx"	'
-	-	kIx~	-
není-liž	níižit	k5eNaPmRp2nS	ní-ližit
i	i	k9	i
nic	nic	k3yNnSc4	nic
pěknějšího	pěkný	k2eAgNnSc2d2	pěknější
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
ideálů	ideál	k1gInPc2	ideál
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
v	v	k7c6	v
p.	p.	k?	p.
Máchovu	Máchův	k2eAgNnSc3d1	Máchovo
srdci	srdce	k1gNnSc3	srdce
byl	být	k5eAaImAgInS	být
mohl	moct	k5eAaImAgInS	moct
zpěv	zpěv	k1gInSc1	zpěv
vzejmouti	vzejmout	k5eAaPmF	vzejmout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
'	'	kIx"	'
<g/>
všemi	všecek	k3xTgFnPc7	všecek
vlastmi	vlast	k1gFnPc7	vlast
<g/>
'	'	kIx"	'
pohýbal	pohýbat	k5eAaImAgInS	pohýbat
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
Proč	proč	k6eAd1	proč
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
v	v	k7c6	v
znameních	znamení	k1gNnPc6	znamení
nejpříznivějších	příznivý	k2eAgFnPc2d3	nejpříznivější
krásné	krásný	k2eAgNnSc1d1	krásné
příští	příští	k2eAgNnSc1d1	příští
usmívá	usmívat	k5eAaImIp3nS	usmívat
<g/>
,	,	kIx,	,
kdež	kdež	k9	kdež
potřebí	potřebí	k6eAd1	potřebí
na	na	k7c6	na
všech	všecek	k3xTgFnPc6	všecek
cestách	cesta	k1gFnPc6	cesta
k	k	k7c3	k
národu	národ	k1gInSc3	národ
mluviti	mluvit	k5eAaImF	mluvit
-	-	kIx~	-
a	a	k8xC	a
kde	kde	k6eAd1	kde
pěvci	pěvec	k1gMnSc6	pěvec
otevřenť	otevřenť	k1gFnSc4	otevřenť
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
<g/>
!	!	kIx.	!
</s>
<s>
-	-	kIx~	-
proč	proč	k6eAd1	proč
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
<g/>
,	,	kIx,	,
muži	muž	k1gMnSc3	muž
mladému	mladý	k1gMnSc3	mladý
<g/>
,	,	kIx,	,
duchem	duch	k1gMnSc7	duch
bohatě	bohatě	k6eAd1	bohatě
nadanému	nadaný	k2eAgMnSc3d1	nadaný
<g/>
,	,	kIx,	,
s	s	k7c7	s
kolem	kolo	k1gNnSc7	kolo
a	a	k8xC	a
popravou	poprava	k1gFnSc7	poprava
se	se	k3xPyFc4	se
obírati	obírat	k5eAaImF	obírat
<g/>
?	?	kIx.	?
</s>
<s>
Zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
místa	místo	k1gNnPc4	místo
k	k	k7c3	k
dolíčení	dolíčení	k1gNnSc3	dolíčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
potřeba	potřeba	k6eAd1	potřeba
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
kde	kde	k6eAd1	kde
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poesie	poesie	k1gFnSc1	poesie
vznikající	vznikající	k2eAgFnSc4d1	vznikající
tázu	táza	k1gFnSc4	táza
národního	národní	k2eAgInSc2d1	národní
nabyla	nabýt	k5eAaPmAgFnS	nabýt
<g/>
....	....	k?	....
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
J.	J.	kA	J.
K.	K.	kA	K.
Tyl	Tyl	k1gMnSc1	Tyl
<g/>
,	,	kIx,	,
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
literaturu	literatura	k1gFnSc4	literatura
nejnovější	nový	k2eAgFnSc2d3	nejnovější
<g/>
,	,	kIx,	,
Květy	Květa	k1gFnSc2	Květa
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivněji	pozitivně	k6eAd2	pozitivně
byl	být	k5eAaImAgInS	být
Máj	máj	k1gInSc1	máj
přijat	přijmout	k5eAaPmNgInS	přijmout
kritikou	kritika	k1gFnSc7	kritika
německou	německý	k2eAgFnSc7d1	německá
a	a	k8xC	a
polskou	polský	k2eAgFnSc7d1	polská
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
Máj	máj	k1gFnSc4	máj
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
mj.	mj.	kA	mj.
poema	poema	k1gFnSc1	poema
Protichůdci	protichůdce	k1gMnPc7	protichůdce
Václava	Václav	k1gMnSc2	Václav
Bolemíra	Bolemír	k1gMnSc2	Bolemír
Nebeského	nebeský	k2eAgMnSc2d1	nebeský
a	a	k8xC	a
Tylova	Tylův	k2eAgFnSc1d1	Tylova
novela	novela	k1gFnSc1	novela
Rozervanec	rozervanec	k1gMnSc1	rozervanec
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
básně	báseň	k1gFnSc2	báseň
byla	být	k5eAaImAgFnS	být
doceněna	docenit	k5eAaPmNgFnS	docenit
až	až	k6eAd1	až
následující	následující	k2eAgFnSc1d1	následující
generací	generace	k1gFnSc7	generace
tzv.	tzv.	kA	tzv.
májovců	májovec	k1gMnPc2	májovec
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Mácha	Mácha	k1gMnSc1	Mácha
naopak	naopak	k6eAd1	naopak
stává	stávat	k5eAaImIp3nS	stávat
kultem	kult	k1gInSc7	kult
<g/>
.	.	kIx.	.
</s>
<s>
Máj	máj	k1gFnSc1	máj
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejčastěji	často	k6eAd3	často
vydávaných	vydávaný	k2eAgFnPc2d1	vydávaná
českých	český	k2eAgFnPc2d1	Česká
knih	kniha	k1gFnPc2	kniha
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejvydávanější	vydávaný	k2eAgFnSc4d3	nejvydávanější
českou	český	k2eAgFnSc4d1	Česká
knihu	kniha	k1gFnSc4	kniha
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
přesném	přesný	k2eAgInSc6d1	přesný
počtu	počet	k1gInSc6	počet
vydání	vydání	k1gNnSc2	vydání
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
patří	patřit	k5eAaImIp3nS	patřit
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
výborech	výbor	k1gInPc6	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
číslo	číslo	k1gNnSc1	číslo
ale	ale	k8xC	ale
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
250	[number]	k4	250
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
za	za	k7c2	za
Máchova	Máchův	k2eAgInSc2d1	Máchův
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
autor	autor	k1gMnSc1	autor
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
u	u	k7c2	u
tiskaře	tiskař	k1gMnSc2	tiskař
Jana	Jan	k1gMnSc2	Jan
Spurného	Spurný	k1gMnSc2	Spurný
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1836	[number]	k4	1836
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
počtu	počet	k1gInSc6	počet
600	[number]	k4	600
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
různých	různý	k2eAgFnPc6d1	různá
cenových	cenový	k2eAgFnPc6d1	cenová
skupinách	skupina	k1gFnPc6	skupina
podle	podle	k7c2	podle
jakosti	jakost	k1gFnSc2	jakost
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
celkového	celkový	k2eAgNnSc2d1	celkové
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
<s>
Mácha	Mácha	k1gMnSc1	Mácha
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
vydání	vydání	k1gNnSc6	vydání
napsal	napsat	k5eAaPmAgInS	napsat
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Eduardu	Eduard	k1gMnSc3	Eduard
Hindlovi	Hindl	k1gMnSc3	Hindl
ze	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1836	[number]	k4	1836
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dal	dát	k5eAaPmAgMnS	dát
jsem	být	k5eAaImIp1nS	být
to	ten	k3xDgNnSc4	ten
tisknout	tisknout	k5eAaImF	tisknout
u	u	k7c2	u
Spurného	Spurného	k2eAgFnPc2d1	Spurného
<g/>
....	....	k?	....
Žádá	žádat	k5eAaImIp3nS	žádat
sice	sice	k8xC	sice
pravda	pravda	k1gFnSc1	pravda
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c4	mnoho
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
9	[number]	k4	9
fl	fl	k?	fl
<g/>
.	.	kIx.	.
stříbra	stříbro	k1gNnSc2	stříbro
od	od	k7c2	od
archu	arch	k1gInSc2	arch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
1	[number]	k4	1
<g/>
.	.	kIx.	.
nepřetiskne	přetisknout	k5eNaPmIp3nS	přetisknout
<g />
.	.	kIx.	.
</s>
<s>
mi	já	k3xPp1nSc3	já
žádného	žádný	k3yNgInSc2	žádný
exempláře	exemplář	k1gInSc2	exemplář
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
dá	dát	k5eAaPmIp3nS	dát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
tuze	tuze	k6eAd1	tuze
pozor	pozor	k1gInSc1	pozor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
čisté	čistý	k2eAgNnSc1d1	čisté
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
třetí	třetí	k4xOgNnSc4	třetí
dá	dát	k5eAaPmIp3nS	dát
nové	nový	k2eAgNnSc4d1	nové
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yIgNnSc7	který
ještě	ještě	k9	ještě
nic	nic	k3yNnSc1	nic
nebylo	být	k5eNaImAgNnS	být
tisknuto	tisknout	k5eAaImNgNnS	tisknout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
in	in	k?	in
<g/>
:	:	kIx,	:
Miroslav	Miroslav	k1gMnSc1	Miroslav
Ivanov	Ivanov	k1gInSc4	Ivanov
<g/>
,	,	kIx,	,
Důvěrná	důvěrný	k2eAgFnSc1d1	důvěrná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
Karlu	Karel	k1gMnSc6	Karel
Hynku	Hynek	k1gMnSc6	Hynek
Máchovi	Mácha	k1gMnSc6	Mácha
<g/>
)	)	kIx)	)
Do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
exemplářů	exemplář	k1gInPc2	exemplář
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
knihovnách	knihovna	k1gFnPc6	knihovna
a	a	k8xC	a
sbírkách	sbírka	k1gFnPc6	sbírka
údajně	údajně	k6eAd1	údajně
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
přibližně	přibližně	k6eAd1	přibližně
osmdesát	osmdesát	k4xCc1	osmdesát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
jeden	jeden	k4xCgInSc4	jeden
výtisk	výtisk	k1gInSc4	výtisk
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc6	vydání
dražen	dražen	k2eAgInSc1d1	dražen
za	za	k7c4	za
vyvolávací	vyvolávací	k2eAgFnSc4d1	vyvolávací
cenu	cena	k1gFnSc4	cena
40	[number]	k4	40
000	[number]	k4	000
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Podruhé	podruhé	k6eAd1	podruhé
vyšel	vyjít	k5eAaPmAgInS	vyjít
Máj	máj	k1gInSc1	máj
r.	r.	kA	r.
1845	[number]	k4	1845
ve	v	k7c6	v
Spisech	spis	k1gInPc6	spis
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
edičně	edičně	k6eAd1	edičně
připravil	připravit	k5eAaPmAgMnS	připravit
Máchův	Máchův	k2eAgMnSc1d1	Máchův
někdejší	někdejší	k2eAgMnSc1d1	někdejší
přítel	přítel	k1gMnSc1	přítel
Karel	Karel	k1gMnSc1	Karel
Sabina	Sabina	k1gMnSc1	Sabina
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
básně	báseň	k1gFnPc4	báseň
je	být	k5eAaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
vydání	vydání	k1gNnSc6	vydání
neúplný	úplný	k2eNgMnSc1d1	neúplný
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1903	[number]	k4	1903
vyšel	vyjít	k5eAaPmAgInS	vyjít
poprvé	poprvé	k6eAd1	poprvé
přetisk	přetisk	k1gInSc1	přetisk
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
r.	r.	kA	r.
1836	[number]	k4	1836
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1910	[number]	k4	1910
vydalo	vydat	k5eAaPmAgNnS	vydat
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Spolku	spolek	k1gInSc2	spolek
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
belletristů	belletrista	k1gMnPc2	belletrista
Máje	Mája	k1gFnSc3	Mája
fotolitografický	fotolitografický	k2eAgInSc4d1	fotolitografický
otisk	otisk	k1gInSc4	otisk
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
ohlasy	ohlas	k1gInPc4	ohlas
z	z	k7c2	z
předchozích	předchozí	k2eAgNnPc2d1	předchozí
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1916	[number]	k4	1916
pak	pak	k6eAd1	pak
nakladatel	nakladatel	k1gMnSc1	nakladatel
František	František	k1gMnSc1	František
Šimáček	Šimáček	k1gMnSc1	Šimáček
vydal	vydat	k5eAaPmAgMnS	vydat
faksimile	faksimile	k1gNnSc4	faksimile
Máchova	Máchův	k2eAgInSc2d1	Máchův
rukopisu	rukopis	k1gInSc2	rukopis
Máje	máj	k1gFnSc2	máj
<g/>
,	,	kIx,	,
edičně	edičně	k6eAd1	edičně
připravené	připravený	k2eAgNnSc1d1	připravené
Václavem	Václav	k1gMnSc7	Václav
Flajšhansem	Flajšhans	k1gMnSc7	Flajšhans
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
básníkovy	básníkův	k2eAgFnSc2d1	básníkova
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
r.	r.	kA	r.
1936	[number]	k4	1936
vyšel	vyjít	k5eAaPmAgInS	vyjít
Máj	máj	k1gInSc1	máj
celkem	celkem	k6eAd1	celkem
osmnáctkrát	osmnáctkrát	k6eAd1	osmnáctkrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
válečných	válečný	k2eAgNnPc2d1	válečné
vydání	vydání	k1gNnPc2	vydání
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
cenzura	cenzura	k1gFnSc1	cenzura
vynechala	vynechat	k5eAaPmAgFnS	vynechat
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
Vůdce	vůdce	k1gMnSc1	vůdce
zhynul	zhynout	k5eAaPmAgMnS	zhynout
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nahradila	nahradit	k5eAaPmAgFnS	nahradit
obratem	obratem	k6eAd1	obratem
"	"	kIx"	"
<g/>
Pán	pán	k1gMnSc1	pán
náš	náš	k3xOp1gMnSc1	náš
zhynul	zhynout	k5eAaPmAgMnS	zhynout
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
poválečné	poválečný	k2eAgFnPc4d1	poválečná
kuriozity	kuriozita	k1gFnPc4	kuriozita
patří	patřit	k5eAaImIp3nP	patřit
vydání	vydání	k1gNnSc3	vydání
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Máj	máj	k1gInSc1	máj
/	/	kIx~	/
Necenzurovaný	cenzurovaný	k2eNgInSc1d1	necenzurovaný
deník	deník	k1gInSc1	deník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1835	[number]	k4	1835
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
a	a	k8xC	a
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
r.	r.	kA	r.
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
s	s	k7c7	s
ilustrací	ilustrace	k1gFnSc7	ilustrace
Jiřího	Jiří	k1gMnSc2	Jiří
Šlitra	Šlitr	k1gMnSc2	Šlitr
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
2000	[number]	k4	2000
vyšel	vyjít	k5eAaPmAgInS	vyjít
Máj	máj	k1gInSc1	máj
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Levných	levný	k2eAgFnPc6d1	levná
knihách	kniha	k1gFnPc6	kniha
(	(	kIx(	(
<g/>
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
svazku	svazek	k1gInSc6	svazek
s	s	k7c7	s
Kyticí	kytice	k1gFnSc7	kytice
K.	K.	kA	K.
J.	J.	kA	J.
Erbena	Erben	k1gMnSc2	Erben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
R.	R.	kA	R.
2010	[number]	k4	2010
vydala	vydat	k5eAaPmAgFnS	vydat
Obec	obec	k1gFnSc1	obec
spisovatelů	spisovatel	k1gMnPc2	spisovatel
"	"	kIx"	"
<g/>
trojitý	trojitý	k2eAgInSc1d1	trojitý
<g/>
"	"	kIx"	"
svazek	svazek	k1gInSc1	svazek
Máj	máj	k1gInSc1	máj
<g/>
,	,	kIx,	,
Máj	máj	k1gInSc1	máj
<g/>
,	,	kIx,	,
věčný	věčný	k2eAgInSc1d1	věčný
Máj	máj	k1gInSc1	máj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
reprodukci	reprodukce	k1gFnSc4	reprodukce
Máchova	Máchův	k2eAgInSc2d1	Máchův
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
,	,	kIx,	,
reprodukci	reprodukce	k1gFnSc4	reprodukce
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
r.	r.	kA	r.
1836	[number]	k4	1836
a	a	k8xC	a
text	text	k1gInSc1	text
básně	báseň	k1gFnSc2	báseň
podle	podle	k7c2	podle
kritického	kritický	k2eAgNnSc2d1	kritické
vydání	vydání	k1gNnSc2	vydání
z	z	k7c2	z
r.	r.	kA	r.
1959	[number]	k4	1959
<g/>
;	;	kIx,	;
kniha	kniha	k1gFnSc1	kniha
rovněž	rovněž	k9	rovněž
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
ediční	ediční	k2eAgFnSc4d1	ediční
poznámku	poznámka	k1gFnSc4	poznámka
Miloše	Miloš	k1gMnSc2	Miloš
Pohorského	pohorský	k2eAgMnSc2d1	pohorský
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgNnPc1d1	různé
vydání	vydání	k1gNnPc1	vydání
Máje	máj	k1gFnSc2	máj
byla	být	k5eAaImAgNnP	být
ilustrována	ilustrován	k2eAgFnSc1d1	ilustrována
a	a	k8xC	a
typograficky	typograficky	k6eAd1	typograficky
připravena	připraven	k2eAgFnSc1d1	připravena
desítkami	desítka	k1gFnPc7	desítka
výtvarníků	výtvarník	k1gMnPc2	výtvarník
včetně	včetně	k7c2	včetně
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
českých	český	k2eAgMnPc2d1	český
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Cenná	cenný	k2eAgFnSc1d1	cenná
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
vydání	vydání	k1gNnPc4	vydání
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
Viktor	Viktor	k1gMnSc1	Viktor
Oliva	Oliva	k1gMnSc1	Oliva
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Kobliha	kobliha	k1gFnSc1	kobliha
(	(	kIx(	(
<g/>
dřevoryty	dřevoryt	k1gInPc1	dřevoryt
<g/>
,	,	kIx,	,
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
1954	[number]	k4	1954
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Max	Max	k1gMnSc1	Max
Švabinský	Švabinský	k2eAgMnSc1d1	Švabinský
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mikoláš	Mikoláš	k1gMnSc1	Mikoláš
Aleš	Aleš	k1gMnSc1	Aleš
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
;	;	kIx,	;
ilustrace	ilustrace	k1gFnSc1	ilustrace
však	však	k9	však
Aleš	Aleš	k1gMnSc1	Aleš
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
už	už	k9	už
r.	r.	kA	r.
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Toyen	Toyen	k1gInSc1	Toyen
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Štyrský	Štyrský	k2eAgMnSc1d1	Štyrský
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Svolinský	Svolinský	k2eAgMnSc1d1	Svolinský
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
kresba	kresba	k1gFnSc1	kresba
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
celková	celkový	k2eAgFnSc1d1	celková
úprava	úprava	k1gFnSc1	úprava
<g/>
,	,	kIx,	,
výzdoba	výzdoba	k1gFnSc1	výzdoba
a	a	k8xC	a
návrh	návrh	k1gInSc1	návrh
vazby	vazba	k1gFnSc2	vazba
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
reprint	reprint	k1gInSc1	reprint
1981	[number]	k4	1981
<g/>
;	;	kIx,	;
litografie	litografie	k1gFnSc1	litografie
a	a	k8xC	a
typografická	typografický	k2eAgFnSc1d1	typografická
úprava	úprava	k1gFnSc1	úprava
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
Jiřincová	Jiřincový	k2eAgFnSc1d1	Jiřincová
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Konůpek	Konůpek	k1gInSc1	Konůpek
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Sivko	sivka	k1gFnSc5	sivka
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Komárek	Komárek	k1gMnSc1	Komárek
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Souček	Souček	k1gMnSc1	Souček
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
četné	četný	k2eAgInPc4d1	četný
překlady	překlad	k1gInPc4	překlad
Máchova	Máchův	k2eAgInSc2d1	Máchův
Máje	máj	k1gInSc2	máj
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
jazyků	jazyk	k1gInPc2	jazyk
patří	patřit	k5eAaImIp3nS	patřit
dva	dva	k4xCgInPc4	dva
překlady	překlad	k1gInPc4	překlad
do	do	k7c2	do
němčiny	němčina	k1gFnSc2	němčina
<g/>
:	:	kIx,	:
O.	O.	kA	O.
F.	F.	kA	F.
Bablera	Babler	k1gMnSc2	Babler
a	a	k8xC	a
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Cikána	cikán	k1gMnSc2	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
Máj	máj	k1gFnSc1	máj
přeložila	přeložit	k5eAaPmAgFnS	přeložit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
britská	britský	k2eAgFnSc1d1	britská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
Ellis	Ellis	k1gFnSc1	Ellis
Petersová	Petersová	k1gFnSc1	Petersová
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Edith	Edith	k1gInSc1	Edith
Pargeterová	Pargeterový	k2eAgFnSc1d1	Pargeterový
<g/>
,	,	kIx,	,
autorka	autorka	k1gFnSc1	autorka
mj.	mj.	kA	mj.
historických	historický	k2eAgInPc2d1	historický
detektivních	detektivní	k2eAgInPc2d1	detektivní
příběhů	příběh	k1gInPc2	příběh
o	o	k7c6	o
bratru	bratr	k1gMnSc6	bratr
Cadfaelovi	Cadfael	k1gMnSc6	Cadfael
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
překlad	překlad	k1gInSc1	překlad
oxfordského	oxfordský	k2eAgMnSc2d1	oxfordský
bohemisty	bohemista	k1gMnSc2	bohemista
Jamese	Jamese	k1gFnSc1	Jamese
Naughtona	Naughtona	k1gFnSc1	Naughtona
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vyšel	vyjít	k5eAaPmAgInS	vyjít
nový	nový	k2eAgInSc1d1	nový
anglický	anglický	k2eAgInSc1d1	anglický
překlad	překlad	k1gInSc1	překlad
Máje	máj	k1gFnSc2	máj
od	od	k7c2	od
Američanky	Američanka	k1gFnSc2	Američanka
Marcely	Marcela	k1gFnSc2	Marcela
Sulak	Sulak	k1gInSc1	Sulak
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
iliteratura	iliteratura	k1gFnSc1	iliteratura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
9	[number]	k4	9
překladů	překlad	k1gInPc2	překlad
německých	německý	k2eAgInPc2d1	německý
a	a	k8xC	a
3	[number]	k4	3
anglických	anglický	k2eAgMnPc2d1	anglický
vyšel	vyjít	k5eAaPmAgMnS	vyjít
Máj	Mája	k1gFnPc2	Mája
třikrát	třikrát	k6eAd1	třikrát
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
polsky	polsky	k6eAd1	polsky
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
,	,	kIx,	,
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
slovinsky	slovinsky	k6eAd1	slovinsky
a	a	k8xC	a
třikrát	třikrát	k6eAd1	třikrát
rusky	rusky	k6eAd1	rusky
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
překlad	překlad	k1gInSc4	překlad
slovenský	slovenský	k2eAgMnSc1d1	slovenský
<g/>
,	,	kIx,	,
bulharský	bulharský	k2eAgMnSc1d1	bulharský
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgMnSc1d1	japonský
<g/>
,	,	kIx,	,
bengálský	bengálský	k2eAgMnSc1d1	bengálský
a	a	k8xC	a
hornolužickosrbský	hornolužickosrbský	k2eAgMnSc1d1	hornolužickosrbský
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
druhého	druhý	k4xOgInSc2	druhý
španělského	španělský	k2eAgInSc2d1	španělský
překladu	překlad	k1gInSc2	překlad
z	z	k7c2	z
r.	r.	kA	r.
2010	[number]	k4	2010
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
hispanista	hispanista	k1gMnSc1	hispanista
Miloslav	Miloslav	k1gMnSc1	Miloslav
Uličný	Uličný	k2eAgMnSc1d1	Uličný
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
esperanta	esperanto	k1gNnSc2	esperanto
báseň	báseň	k1gFnSc4	báseň
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Majo	Maja	k1gFnSc5	Maja
přeložil	přeložit	k5eAaPmAgMnS	přeložit
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1946	[number]	k4	1946
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
Tomáš	Tomáš	k1gMnSc1	Tomáš
Pumpr	Pumpr	k1gMnSc1	Pumpr
(	(	kIx(	(
<g/>
vydal	vydat	k5eAaPmAgInS	vydat
Český	český	k2eAgInSc1d1	český
esperantský	esperantský	k2eAgInSc1d1	esperantský
svaz	svaz	k1gInSc1	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
znovu	znovu	k6eAd1	znovu
Josef	Josef	k1gMnSc1	Josef
Rumler	Rumler	k1gMnSc1	Rumler
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
divadelní	divadelní	k2eAgFnSc2d1	divadelní
inscenace	inscenace	k1gFnSc2	inscenace
Máj	Mája	k1gFnPc2	Mája
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
D	D	kA	D
34	[number]	k4	34
E.	E.	kA	E.
F.	F.	kA	F.
Buriana	Burian	k1gMnSc2	Burian
<g/>
.	.	kIx.	.
</s>
<s>
Zhudebněn	zhudebnit	k5eAaPmNgInS	zhudebnit
byl	být	k5eAaImAgInS	být
Máj	máj	k1gInSc1	máj
poprvé	poprvé	k6eAd1	poprvé
bratry	bratr	k1gMnPc7	bratr
Jiřím	Jiří	k1gMnSc7	Jiří
a	a	k8xC	a
Petrem	Petr	k1gMnSc7	Petr
Traxlerovými	Traxlerová	k1gFnPc7	Traxlerová
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Český	český	k2eAgInSc1d1	český
skiffle	skiffle	k1gInSc1	skiffle
<g/>
,	,	kIx,	,
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
na	na	k7c6	na
gramofonové	gramofonový	k2eAgFnSc6d1	gramofonová
desce	deska	k1gFnSc6	deska
firmy	firma	k1gFnSc2	firma
Panton	Panton	k1gInSc1	Panton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
Máj	máj	k1gInSc4	máj
upravil	upravit	k5eAaPmAgMnS	upravit
pro	pro	k7c4	pro
Divadlo	divadlo	k1gNnSc4	divadlo
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
režisér	režisér	k1gMnSc1	režisér
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Potužil	Potužil	k1gMnSc1	Potužil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
režisér	režisér	k1gMnSc1	režisér
F.	F.	kA	F.
A.	A.	kA	A.
Brabec	Brabec	k1gMnSc1	Brabec
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
knižní	knižní	k2eAgFnSc4d1	knižní
předlohu	předloha	k1gFnSc4	předloha
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
soundtrack	soundtrack	k1gInSc4	soundtrack
filmu	film	k1gInSc2	film
skupina	skupina	k1gFnSc1	skupina
Support	support	k1gInSc4	support
Lesbiens	Lesbiensa	k1gFnPc2	Lesbiensa
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
řadu	řada	k1gFnSc4	řada
pasáží	pasáž	k1gFnPc2	pasáž
básně	báseň	k1gFnSc2	báseň
<g/>
;	;	kIx,	;
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
Máj	Mája	k1gFnPc2	Mája
využívá	využívat	k5eAaImIp3nS	využívat
jako	jako	k9	jako
text	text	k1gInSc1	text
její	její	k3xOp3gInSc4	její
začátek	začátek	k1gInSc4	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
byl	být	k5eAaImAgMnS	být
známý	známý	k2eAgMnSc1d1	známý
recitátor	recitátor	k1gMnSc1	recitátor
Máje	máj	k1gFnSc2	máj
(	(	kIx(	(
<g/>
pražské	pražský	k2eAgNnSc1d1	Pražské
poetické	poetický	k2eAgNnSc1d1	poetické
divadlo	divadlo	k1gNnSc1	divadlo
Viola	Viola	k1gFnSc1	Viola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
přednes	přednes	k1gInSc1	přednes
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
na	na	k7c6	na
gramofonové	gramofonový	k2eAgFnSc6d1	gramofonová
desce	deska	k1gFnSc6	deska
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
veršů	verš	k1gInPc2	verš
z	z	k7c2	z
Máje	máj	k1gInSc2	máj
recituje	recitovat	k5eAaImIp3nS	recitovat
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Skružný	Skružný	k2eAgMnSc1d1	Skružný
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgMnSc1d1	Hrušínský
<g/>
)	)	kIx)	)
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vesničko	vesnička	k1gFnSc5	vesnička
má	mít	k5eAaImIp3nS	mít
středisková	střediskový	k2eAgFnSc1d1	středisková
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zhudebnila	zhudebnit	k5eAaPmAgFnS	zhudebnit
skupina	skupina	k1gFnSc1	skupina
WWW	WWW	kA	WWW
první	první	k4xOgNnSc1	první
intermezzo	intermezzo	k1gNnSc1	intermezzo
básně	báseň	k1gFnSc2	báseň
a	a	k8xC	a
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Máj	máj	k1gFnSc4	máj
<g/>
"	"	kIx"	"
jej	on	k3xPp3gInSc4	on
zařadila	zařadit	k5eAaPmAgFnS	zařadit
jako	jako	k8xC	jako
šestou	šestý	k4xOgFnSc4	šestý
skladbu	skladba	k1gFnSc4	skladba
na	na	k7c4	na
album	album	k1gNnSc4	album
Atomová	atomový	k2eAgFnSc1d1	atomová
včela	včela	k1gFnSc1	včela
<g/>
.	.	kIx.	.
</s>
