<p>
<s>
Vanad	vanad	k1gInSc1	vanad
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
V	V	kA	V
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Vanadium	vanadium	k1gNnSc1	vanadium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
niobem	niob	k1gInSc7	niob
a	a	k8xC	a
tantalem	tantal	k1gInSc7	tantal
členem	člen	k1gMnSc7	člen
5	[number]	k4	5
<g/>
.	.	kIx.	.
skupiny	skupina	k1gFnSc2	skupina
periodické	periodický	k2eAgFnSc2d1	periodická
tabulky	tabulka	k1gFnSc2	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Vanad	vanad	k1gInSc1	vanad
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
kovové	kovový	k2eAgInPc4d1	kovový
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
a	a	k8xC	a
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
objevu	objev	k1gInSc2	objev
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
ho	on	k3xPp3gMnSc4	on
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
A.	A.	kA	A.
M.	M.	kA	M.
del	del	k?	del
Rio	Rio	k1gMnSc1	Rio
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
mexické	mexický	k2eAgFnSc2d1	mexická
olověné	olověný	k2eAgFnSc2d1	olověná
rudy	ruda	k1gFnSc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
nesprávných	správný	k2eNgInPc2d1	nesprávný
názorů	názor	k1gInPc2	názor
H.	H.	kA	H.
V.	V.	kA	V.
Collet-Descotilse	Collet-Descotilse	k1gFnSc1	Collet-Descotilse
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
objevu	objev	k1gInSc2	objev
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
prvek	prvek	k1gInSc1	prvek
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
N.	N.	kA	N.
G.	G.	kA	G.
Sefströmem	Sefström	k1gInSc7	Sefström
ve	v	k7c6	v
švédských	švédský	k2eAgFnPc6d1	švédská
železných	železný	k2eAgFnPc6d1	železná
rudách	ruda	k1gFnPc6	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
získal	získat	k5eAaPmAgInS	získat
podle	podle	k7c2	podle
skandinávské	skandinávský	k2eAgFnSc2d1	skandinávská
bohyně	bohyně	k1gFnSc2	bohyně
krásy	krása	k1gFnSc2	krása
Vanadis	Vanadis	k1gFnSc2	Vanadis
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
kov	kov	k1gInSc1	kov
izoloval	izolovat	k5eAaBmAgInS	izolovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1867	[number]	k4	1867
H.	H.	kA	H.
E.	E.	kA	E.
Roscoe	Roscoe	k1gInSc4	Roscoe
redukcí	redukce	k1gFnPc2	redukce
chloridu	chlorid	k1gInSc2	chlorid
vanadičného	vanadičný	k2eAgMnSc4d1	vanadičný
VCl	VCl	k1gMnSc4	VCl
<g/>
5	[number]	k4	5
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Vanad	vanad	k1gInSc1	vanad
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
<g/>
,	,	kIx,	,
šedo-bílý	šedoílý	k2eAgInSc1d1	šedo-bílý
<g/>
,	,	kIx,	,
kujný	kujný	k2eAgInSc1d1	kujný
kov	kov	k1gInSc1	kov
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
5,38	[number]	k4	5,38
K	K	kA	K
je	být	k5eAaImIp3nS	být
supravodivý	supravodivý	k2eAgInSc1d1	supravodivý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přidání	přidání	k1gNnSc6	přidání
malého	malý	k2eAgNnSc2d1	malé
množství	množství	k1gNnSc2	množství
uhlíku	uhlík	k1gInSc2	uhlík
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
k	k	k7c3	k
vanadu	vanad	k1gInSc3	vanad
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
teploty	teplota	k1gFnPc1	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
varu	var	k1gInSc2	var
ještě	ještě	k6eAd1	ještě
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
odolný	odolný	k2eAgInSc1d1	odolný
jak	jak	k8xS	jak
vůči	vůči	k7c3	vůči
běžným	běžný	k2eAgFnPc3d1	běžná
kyselinám	kyselina	k1gFnPc3	kyselina
tak	tak	k8xC	tak
alkáliím	alkálie	k1gFnPc3	alkálie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
reaguje	reagovat	k5eAaBmIp3nS	reagovat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
fluorovodíkovou	fluorovodíkový	k2eAgFnSc7d1	fluorovodíková
a	a	k8xC	a
lučavkou	lučavka	k1gFnSc7	lučavka
královskou	královský	k2eAgFnSc7d1	královská
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
však	však	k9	však
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
podléhá	podléhat	k5eAaImIp3nS	podléhat
oxidaci	oxidace	k1gFnSc4	oxidace
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Vanad	vanad	k1gInSc1	vanad
se	se	k3xPyFc4	se
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
slučuje	slučovat	k5eAaImIp3nS	slučovat
také	také	k9	také
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
,	,	kIx,	,
křemíkem	křemík	k1gInSc7	křemík
<g/>
,	,	kIx,	,
arsenem	arsen	k1gInSc7	arsen
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
mocenství	mocenství	k1gNnSc2	mocenství
od	od	k7c2	od
mocenství	mocenství	k1gNnSc2	mocenství
V	v	k7c6	v
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
po	po	k7c6	po
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
komplexech	komplex	k1gInPc6	komplex
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
I	i	k9	i
a	a	k8xC	a
V-	V-	k1gMnSc1	V-
<g/>
I.	I.	kA	I.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Obsah	obsah	k1gInSc1	obsah
vanadu	vanad	k1gInSc2	vanad
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
136	[number]	k4	136
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
prvek	prvek	k1gInSc1	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
0,002	[number]	k4	0,002
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l.	l.	k?	l.
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
vanadu	vanad	k1gInSc2	vanad
na	na	k7c4	na
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Navzdory	navzdory	k7c3	navzdory
jeho	jeho	k3xOp3gNnSc3	jeho
poměrné	poměrný	k2eAgNnSc4d1	poměrné
velkému	velký	k2eAgInSc3d1	velký
průměrnému	průměrný	k2eAgInSc3d1	průměrný
obsahu	obsah	k1gInSc3	obsah
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
několik	několik	k4yIc4	několik
bohatších	bohatý	k2eAgNnPc2d2	bohatší
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vanadu	vanad	k1gInSc2	vanad
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
produkt	produkt	k1gInSc1	produkt
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
některé	některý	k3yIgFnSc2	některý
z	z	k7c2	z
asi	asi	k9	asi
60	[number]	k4	60
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
je	být	k5eAaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgInPc4d3	nejdůležitější
minerály	minerál	k1gInPc4	minerál
patří	patřit	k5eAaImIp3nP	patřit
polysulfid	polysulfid	k1gInSc4	polysulfid
patronit	patronit	k1gInSc1	patronit
VS	VS	kA	VS
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
však	však	k9	však
vanad	vanad	k1gInSc1	vanad
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sloučeniny	sloučenina	k1gFnSc2	sloučenina
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vanadinit	vanadinit	k1gInSc4	vanadinit
–	–	k?	–
podvojný	podvojný	k2eAgMnSc1d1	podvojný
chlorid-vanadičnan	chloridanadičnan	k1gMnSc1	chlorid-vanadičnan
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
PbCl	PbCl	k1gInSc4	PbCl
<g/>
2	[number]	k4	2
<g/>
·	·	k?	·
<g/>
3	[number]	k4	3
<g/>
Pb	Pb	k1gFnSc1	Pb
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
nebo	nebo	k8xC	nebo
carnotit	carnotit	k1gInSc1	carnotit
[	[	kIx(	[
<g/>
K	K	kA	K
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
UO	UO	kA	UO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2.3	[number]	k4	2.3
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
významný	významný	k2eAgInSc4d1	významný
obsah	obsah	k1gInSc4	obsah
vanadu	vanad	k1gInSc2	vanad
v	v	k7c6	v
surové	surový	k2eAgFnSc6d1	surová
ropě	ropa	k1gFnSc6	ropa
nebo	nebo	k8xC	nebo
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
vanadu	vanad	k1gInSc2	vanad
==	==	k?	==
</s>
</p>
<p>
<s>
Vanad	vanad	k1gInSc1	vanad
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pražením	pražení	k1gNnSc7	pražení
rozdrcené	rozdrcený	k2eAgFnSc2d1	rozdrcená
rudy	ruda	k1gFnSc2	ruda
nebo	nebo	k8xC	nebo
zbytků	zbytek	k1gInPc2	zbytek
kovového	kovový	k2eAgInSc2d1	kovový
vanadu	vanad	k1gInSc2	vanad
s	s	k7c7	s
chloridem	chlorid	k1gInSc7	chlorid
sodným	sodný	k2eAgInSc7d1	sodný
(	(	kIx(	(
<g/>
NaCl	NaCl	k1gInSc1	NaCl
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uhličitanem	uhličitan	k1gInSc7	uhličitan
sodným	sodný	k2eAgInSc7d1	sodný
(	(	kIx(	(
<g/>
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
CO	co	k9	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
850	[number]	k4	850
°	°	k?	°
<g/>
C.	C.	kA	C.
Tímto	tento	k3xDgInSc7	tento
procesem	proces	k1gInSc7	proces
vzniká	vznikat	k5eAaImIp3nS	vznikat
vanadičnan	vanadičnan	k1gInSc1	vanadičnan
sodný	sodný	k2eAgInSc1d1	sodný
NaVO	NaVO	k?	NaVO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
louží	loužit	k5eAaImIp3nS	loužit
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Okyselením	okyselení	k1gNnSc7	okyselení
získaného	získaný	k2eAgInSc2d1	získaný
výluhu	výluh	k1gInSc2	výluh
na	na	k7c4	na
pH	ph	kA	ph
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
vysrážení	vysrážení	k1gNnSc3	vysrážení
polyvanadičnanu	polyvanadičnan	k1gInSc2	polyvanadičnan
(	(	kIx(	(
<g/>
červený	červený	k2eAgInSc1d1	červený
koláč	koláč	k1gInSc1	koláč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
můžeme	moct	k5eAaImIp1nP	moct
tavením	tavení	k1gNnSc7	tavení
při	při	k7c6	při
700	[number]	k4	700
°	°	k?	°
<g/>
C	C	kA	C
získat	získat	k5eAaPmF	získat
černý	černý	k2eAgInSc4d1	černý
technický	technický	k2eAgInSc4d1	technický
oxid	oxid	k1gInSc4	oxid
vanadičný	vanadičný	k2eAgInSc4d1	vanadičný
V	V	kA	V
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
kroku	krok	k1gInSc6	krok
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
oxid	oxid	k1gInSc1	oxid
zredukuje	zredukovat	k5eAaPmIp3nS	zredukovat
pomocí	pomocí	k7c2	pomocí
kovového	kovový	k2eAgInSc2d1	kovový
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kovového	kovový	k2eAgInSc2d1	kovový
vanadu	vanad	k1gInSc2	vanad
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
redukcí	redukce	k1gFnSc7	redukce
VCl	VCl	k1gFnSc2	VCl
<g/>
5	[number]	k4	5
vodíkem	vodík	k1gInSc7	vodík
nebo	nebo	k8xC	nebo
hořčíkem	hořčík	k1gInSc7	hořčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Vanad	vanad	k1gInSc1	vanad
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
do	do	k7c2	do
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
ocelí	ocel	k1gFnPc2	ocel
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
chromu	chrom	k1gInSc2	chrom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tvoří	tvořit	k5eAaImIp3nP	tvořit
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
karbid	karbid	k1gInSc1	karbid
V	v	k7c6	v
<g/>
4	[number]	k4	4
<g/>
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
oceli	ocel	k1gFnSc6	ocel
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zjemňuje	zjemňovat	k5eAaImIp3nS	zjemňovat
zrnitou	zrnitý	k2eAgFnSc4d1	zrnitá
strukturu	struktura	k1gFnSc4	struktura
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
odolnější	odolný	k2eAgInSc4d2	odolnější
proti	proti	k7c3	proti
opotřebení	opotřebení	k1gNnSc3	opotřebení
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
za	za	k7c2	za
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
železných	železný	k2eAgFnPc2d1	železná
speciálních	speciální	k2eAgFnPc2d1	speciální
slitin	slitina	k1gFnPc2	slitina
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Vicalloy	Vicalloy	k1gInPc4	Vicalloy
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
9,5	[number]	k4	9,5
%	%	kIx~	%
vanadu	vanad	k1gInSc2	vanad
<g/>
,	,	kIx,	,
52	[number]	k4	52
%	%	kIx~	%
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
38,5	[number]	k4	38,5
%	%	kIx~	%
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgFnPc1d1	uvedená
nerezové	rezový	k2eNgFnPc1d1	nerezová
slitiny	slitina	k1gFnPc1	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
ocelí	ocel	k1gFnPc2	ocel
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
chirurgických	chirurgický	k2eAgInPc2d1	chirurgický
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
chemickou	chemický	k2eAgFnSc4d1	chemická
i	i	k8xC	i
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
titanem	titan	k1gInSc7	titan
a	a	k8xC	a
hliníkem	hliník	k1gInSc7	hliník
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vynikající	vynikající	k2eAgFnSc7d1	vynikající
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
odolností	odolnost	k1gFnSc7	odolnost
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
hustotou	hustota	k1gFnSc7	hustota
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
leteckých	letecký	k2eAgInPc2d1	letecký
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
speciálních	speciální	k2eAgFnPc2d1	speciální
součástek	součástka	k1gFnPc2	součástka
pro	pro	k7c4	pro
konstrukci	konstrukce	k1gFnSc4	konstrukce
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
kosmických	kosmický	k2eAgFnPc2d1	kosmická
sond	sonda	k1gFnPc2	sonda
<g/>
,	,	kIx,	,
družic	družice	k1gFnPc2	družice
a	a	k8xC	a
podobných	podobný	k2eAgFnPc2d1	podobná
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
elektrických	elektrický	k2eAgInPc2d1	elektrický
článků	článek	k1gInPc2	článek
a	a	k8xC	a
baterií	baterie	k1gFnPc2	baterie
a	a	k8xC	a
slitiny	slitina	k1gFnSc2	slitina
vanadu	vanad	k1gInSc2	vanad
s	s	k7c7	s
galliem	gallium	k1gNnSc7	gallium
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
materiálům	materiál	k1gInPc3	materiál
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
supravodivých	supravodivý	k2eAgInPc2d1	supravodivý
magnetů	magnet	k1gInPc2	magnet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vanad	vanad	k1gInSc1	vanad
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
organokovových	organokovový	k2eAgFnPc2d1	organokovová
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
je	být	k5eAaImIp3nS	být
vanadocen	vanadocen	k2eAgInSc1d1	vanadocen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
antitumorová	antitumorový	k2eAgFnSc1d1	antitumorový
aktivita	aktivita	k1gFnSc1	aktivita
derivátů	derivát	k1gInPc2	derivát
vanadocendihalogenidů	vanadocendihalogenid	k1gInPc2	vanadocendihalogenid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
později	pozdě	k6eAd2	pozdě
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k8xC	jako
chematerapeutika	chematerapeutika	k1gFnSc1	chematerapeutika
místo	místo	k7c2	místo
cis-platiny	cislatina	k1gFnSc2	cis-platina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tenká	tenký	k2eAgFnSc1d1	tenká
vrstva	vrstva	k1gFnSc1	vrstva
oxidu	oxid	k1gInSc2	oxid
vanadičitého	vanadičitý	k2eAgInSc2d1	vanadičitý
(	(	kIx(	(
<g/>
VO	VO	k?	VO
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vyloučená	vyloučený	k2eAgFnSc1d1	vyloučená
na	na	k7c6	na
skleněném	skleněný	k2eAgInSc6d1	skleněný
povrchu	povrch	k1gInSc6	povrch
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
dopadající	dopadající	k2eAgNnSc1d1	dopadající
infračervené	infračervený	k2eAgNnSc1d1	infračervené
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
současně	současně	k6eAd1	současně
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
optické	optický	k2eAgFnPc4d1	optická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
skla	sklo	k1gNnSc2	sklo
ve	v	k7c6	v
viditelné	viditelný	k2eAgFnSc6d1	viditelná
oblasti	oblast	k1gFnSc6	oblast
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Nanokrystalický	Nanokrystalický	k2eAgMnSc1d1	Nanokrystalický
VO2	VO2	k1gMnSc1	VO2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
předmětem	předmět	k1gInSc7	předmět
intenzivního	intenzivní	k2eAgInSc2d1	intenzivní
výzkumu	výzkum	k1gInSc2	výzkum
jako	jako	k8xC	jako
polovodič	polovodič	k1gInSc4	polovodič
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
skokové	skokový	k2eAgFnSc3d1	skoková
přeměně	přeměna	k1gFnSc3	přeměna
jeho	jeho	k3xOp3gFnPc2	jeho
vodivostních	vodivostní	k2eAgFnPc2d1	vodivostní
charakteristik	charakteristika	k1gFnPc2	charakteristika
z	z	k7c2	z
vodiče	vodič	k1gInSc2	vodič
na	na	k7c4	na
polovodič	polovodič	k1gInSc4	polovodič
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
přechodu	přechod	k1gInSc2	přechod
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
navíc	navíc	k6eAd1	navíc
výrazně	výrazně	k6eAd1	výrazně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
přídavky	přídavek	k1gInPc1	přídavek
stopových	stopová	k1gFnPc2	stopová
množství	množství	k1gNnSc2	množství
dalších	další	k2eAgFnPc2d1	další
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
katalyzátory	katalyzátor	k1gInPc1	katalyzátor
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
oxidu	oxid	k1gInSc2	oxid
vanadičného	vanadičný	k2eAgNnSc2d1	vanadičný
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
tzv.	tzv.	kA	tzv.
kontaktním	kontaktní	k2eAgInSc7d1	kontaktní
způsobem	způsob	k1gInSc7	způsob
při	při	k7c6	při
oxidaci	oxidace	k1gFnSc6	oxidace
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
sírový	sírový	k2eAgInSc4d1	sírový
a	a	k8xC	a
v	v	k7c6	v
syntéze	syntéza	k1gFnSc6	syntéza
některých	některý	k3yIgFnPc2	některý
organických	organický	k2eAgFnPc2d1	organická
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologický	biologický	k2eAgInSc4d1	biologický
a	a	k8xC	a
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Potravou	potrava	k1gFnSc7	potrava
se	se	k3xPyFc4	se
do	do	k7c2	do
organizmu	organizmus	k1gInSc2	organizmus
dostává	dostávat	k5eAaImIp3nS	dostávat
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
olejů	olej	k1gInPc2	olej
<g/>
,	,	kIx,	,
některých	některý	k3yIgFnPc2	některý
minerálních	minerální	k2eAgFnPc2d1	minerální
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnSc4d2	vyšší
koncentraci	koncentrace	k1gFnSc4	koncentrace
vanadu	vanad	k1gInSc2	vanad
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
i	i	k9	i
v	v	k7c6	v
rybím	rybí	k2eAgNnSc6d1	rybí
mase	maso	k1gNnSc6	maso
a	a	k8xC	a
zelenině	zelenina	k1gFnSc6	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
stavebním	stavební	k2eAgInSc7d1	stavební
kamenem	kámen	k1gInSc7	kámen
některých	některý	k3yIgInPc2	některý
enzymů	enzym	k1gInPc2	enzym
jako	jako	k9	jako
například	například	k6eAd1	například
nitrogenázy	nitrogenáza	k1gFnSc2	nitrogenáza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
mikroorganizmů	mikroorganizmus	k1gInPc2	mikroorganizmus
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
zprostředkují	zprostředkovat	k5eAaPmIp3nP	zprostředkovat
fixaci	fixace	k1gFnSc3	fixace
dusíku	dusík	k1gInSc2	dusík
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
jeho	jeho	k3xOp3gFnSc4	jeho
dostupnost	dostupnost	k1gFnSc4	dostupnost
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
výzkumy	výzkum	k1gInPc1	výzkum
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vanadu	vanad	k1gInSc2	vanad
příznivě	příznivě	k6eAd1	příznivě
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
stav	stav	k1gInSc4	stav
nemocných	mocný	k2eNgFnPc2d1	mocný
cukrovkou	cukrovka	k1gFnSc7	cukrovka
(	(	kIx(	(
<g/>
diabetes	diabetes	k1gInSc1	diabetes
mellitus	mellitus	k1gInSc1	mellitus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesný	přesný	k2eAgInSc4d1	přesný
popis	popis	k1gInSc4	popis
funkce	funkce	k1gFnSc2	funkce
vanadu	vanad	k1gInSc2	vanad
v	v	k7c6	v
metabolismu	metabolismus	k1gInSc6	metabolismus
cukrů	cukr	k1gInPc2	cukr
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
<g/>
Vanad	vanad	k1gInSc1	vanad
má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
i	i	k9	i
při	při	k7c6	při
syntéze	syntéza	k1gFnSc6	syntéza
krevního	krevní	k2eAgNnSc2d1	krevní
barviva	barvivo	k1gNnSc2	barvivo
hemoglobinu	hemoglobin	k1gInSc2	hemoglobin
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
kyslíku	kyslík	k1gInSc2	kyslík
krví	krvit	k5eAaImIp3nP	krvit
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
do	do	k7c2	do
tělesných	tělesný	k2eAgFnPc2d1	tělesná
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
nedostatek	nedostatek	k1gInSc1	nedostatek
vanadu	vanad	k1gInSc2	vanad
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
chudokrevností	chudokrevnost	k1gFnSc7	chudokrevnost
<g/>
.	.	kIx.	.
<g/>
Nadbytek	nadbytek	k1gInSc1	nadbytek
vanadu	vanad	k1gInSc2	vanad
působí	působit	k5eAaImIp3nS	působit
naopak	naopak	k6eAd1	naopak
výrazně	výrazně	k6eAd1	výrazně
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
Toxicita	toxicita	k1gFnSc1	toxicita
sloučenin	sloučenina	k1gFnPc2	sloučenina
vanadu	vanad	k1gInSc2	vanad
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
především	především	k9	především
při	při	k7c6	při
každodenní	každodenní	k2eAgFnSc6d1	každodenní
expozici	expozice	k1gFnSc6	expozice
postižených	postižený	k2eAgFnPc2d1	postižená
osob	osoba	k1gFnPc2	osoba
nadměrným	nadměrný	k2eAgFnPc3d1	nadměrná
dávkám	dávka	k1gFnPc3	dávka
tohoto	tento	k3xDgInSc2	tento
prvku	prvek	k1gInSc2	prvek
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
výrobě	výroba	k1gFnSc6	výroba
a	a	k8xC	a
metalurgickém	metalurgický	k2eAgNnSc6d1	metalurgické
zpracování	zpracování	k1gNnSc6	zpracování
nebo	nebo	k8xC	nebo
z	z	k7c2	z
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
důlních	důlní	k2eAgFnPc2d1	důlní
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
<g/>
Chronická	chronický	k2eAgFnSc1d1	chronická
otrava	otrava	k1gFnSc1	otrava
vanadem	vanad	k1gInSc7	vanad
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zvracením	zvracení	k1gNnSc7	zvracení
<g/>
,	,	kIx,	,
průjmem	průjem	k1gInSc7	průjem
<g/>
,	,	kIx,	,
bolestí	bolest	k1gFnSc7	bolest
břicha	břicho	k1gNnSc2	břicho
<g/>
,	,	kIx,	,
poklesem	pokles	k1gInSc7	pokles
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
zrychlením	zrychlení	k1gNnSc7	zrychlení
tepu	tep	k1gInSc2	tep
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
sníženou	snížený	k2eAgFnSc7d1	snížená
hladinou	hladina	k1gFnSc7	hladina
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
selháním	selhání	k1gNnSc7	selhání
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
nadledvin	nadledvina	k1gFnPc2	nadledvina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otravy	otrava	k1gFnPc1	otrava
vanadem	vanad	k1gInSc7	vanad
==	==	k?	==
</s>
</p>
<p>
<s>
Čína	Čína	k1gFnSc1	Čína
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2008	[number]	k4	2008
tři	tři	k4xCgFnPc4	tři
továrny	továrna	k1gFnPc1	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
vanadu	vanad	k1gInSc2	vanad
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
kvůli	kvůli	k7c3	kvůli
jejich	jejich	k3xOp3gInSc3	jejich
provozu	provoz	k1gInSc3	provoz
onemocnělo	onemocnět	k5eAaPmAgNnS	onemocnět
1000	[number]	k4	1000
lidí	člověk	k1gMnPc2	člověk
kožními	kožní	k2eAgFnPc7d1	kožní
chorobami	choroba	k1gFnPc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Továrny	továrna	k1gFnPc1	továrna
v	v	k7c6	v
okrsku	okrsek	k1gInSc6	okrsek
Ťien	Ťien	k1gMnSc1	Ťien
<g/>
-li	i	k?	-li
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Che-pej	Cheej	k1gMnSc2	Che-pej
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
uzavřeny	uzavřít	k5eAaPmNgFnP	uzavřít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
majitelé	majitel	k1gMnPc1	majitel
výrobu	výroba	k1gFnSc4	výroba
nelegálně	legálně	k6eNd1	legálně
obnovili	obnovit	k5eAaPmAgMnP	obnovit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Cikrt	Cikrt	k1gInSc1	Cikrt
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lener	Lenra	k1gFnPc2	Lenra
<g/>
:	:	kIx,	:
Toxické	toxický	k2eAgInPc1d1	toxický
kovy	kov	k1gInPc1	kov
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
a	a	k8xC	a
pracovním	pracovní	k2eAgNnSc6d1	pracovní
prostředí	prostředí	k1gNnSc6	prostředí
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7169-150-X	[number]	k4	80-7169-150-X
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vanad	vanad	k1gInSc1	vanad
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vanad	vanad	k1gInSc1	vanad
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
ATSDR	ATSDR	kA	ATSDR
–	–	k?	–
ToxFAQs	ToxFAQs	k1gInSc1	ToxFAQs
<g/>
:	:	kIx,	:
Vanadium	vanadium	k1gNnSc1	vanadium
</s>
</p>
