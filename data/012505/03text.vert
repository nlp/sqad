<p>
<s>
Taneční	taneční	k2eAgInSc1d1	taneční
sport	sport	k1gInSc1	sport
je	být	k5eAaImIp3nS	být
sportovní	sportovní	k2eAgFnSc1d1	sportovní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
společenského	společenský	k2eAgInSc2d1	společenský
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Taneční	taneční	k2eAgInPc1d1	taneční
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
žena	žena	k1gFnSc1	žena
či	či	k8xC	či
chlapec	chlapec	k1gMnSc1	chlapec
a	a	k8xC	a
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
tancují	tancovat	k5eAaImIp3nP	tancovat
figury	figura	k1gFnPc1	figura
zvolené	zvolený	k2eAgFnPc1d1	zvolená
podle	podle	k7c2	podle
předepsaných	předepsaný	k2eAgNnPc2d1	předepsané
pravidel	pravidlo	k1gNnPc2	pravidlo
na	na	k7c4	na
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
hudbu	hudba	k1gFnSc4	hudba
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
tanci	tanec	k1gInSc3	tanec
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
co	co	k9	co
nejlépe	dobře	k6eAd3	dobře
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
rysy	rys	k1gInPc1	rys
tance	tanec	k1gInSc2	tanec
i	i	k9	i
rytmus	rytmus	k1gInSc4	rytmus
<g/>
,	,	kIx,	,
melodii	melodie	k1gFnSc4	melodie
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Soutěží	soutěžit	k5eAaImIp3nS	soutěžit
vždy	vždy	k6eAd1	vždy
několik	několik	k4yIc4	několik
párů	pár	k1gInPc2	pár
najednou	najednou	k6eAd1	najednou
na	na	k7c6	na
parketě	parketa	k1gFnSc6	parketa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
kolech	kolo	k1gNnPc6	kolo
jsou	být	k5eAaImIp3nP	být
páry	pára	k1gFnPc1	pára
hodnoceny	hodnocen	k2eAgFnPc1d1	hodnocena
porotci	porotce	k1gMnSc3	porotce
a	a	k8xC	a
ty	ty	k3xPp2nSc5	ty
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
hodnocením	hodnocení	k1gNnSc7	hodnocení
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
porotců	porotce	k1gMnPc2	porotce
postupují	postupovat	k5eAaImIp3nP	postupovat
podle	podle	k7c2	podle
postupového	postupový	k2eAgInSc2d1	postupový
klíče	klíč	k1gInSc2	klíč
dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
určí	určit	k5eAaPmIp3nP	určit
tři	tři	k4xCgInPc1	tři
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
páry	pár	k1gInPc1	pár
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kategorií	kategorie	k1gFnPc2	kategorie
jsou	být	k5eAaImIp3nP	být
páry	pára	k1gFnPc1	pára
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
a	a	k8xC	a
výkonnostní	výkonnostní	k2eAgFnSc2d1	výkonnostní
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
přáním	přání	k1gNnPc3	přání
mnoha	mnoho	k4c2	mnoho
tanečníků	tanečník	k1gMnPc2	tanečník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
taneční	taneční	k2eAgInSc1d1	taneční
sport	sport	k1gInSc1	sport
stal	stát	k5eAaPmAgInS	stát
olympijskou	olympijský	k2eAgFnSc7d1	olympijská
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Disciplíny	disciplína	k1gFnSc2	disciplína
==	==	k?	==
</s>
</p>
<p>
<s>
Tančí	tančit	k5eAaImIp3nS	tančit
se	se	k3xPyFc4	se
odděleně	odděleně	k6eAd1	odděleně
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
tanečních	taneční	k2eAgFnPc6d1	taneční
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
standardních	standardní	k2eAgInPc6d1	standardní
a	a	k8xC	a
latinskoamerických	latinskoamerický	k2eAgInPc6d1	latinskoamerický
tancích	tanec	k1gInPc6	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
disciplína	disciplína	k1gFnSc1	disciplína
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
5	[number]	k4	5
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Standardní	standardní	k2eAgInPc4d1	standardní
tance	tanec	k1gInPc4	tanec
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
STT	STT	kA	STT
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
tance	tanec	k1gInPc1	tanec
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
uzavřeným	uzavřený	k2eAgNnSc7d1	uzavřené
párovým	párový	k2eAgNnSc7d1	párové
držením	držení	k1gNnSc7	držení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
čtyři	čtyři	k4xCgMnPc4	čtyři
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
švihový	švihový	k2eAgInSc1d1	švihový
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tango	tango	k1gNnSc1	tango
není	být	k5eNaImIp3nS	být
švihový	švihový	k2eAgInSc4d1	švihový
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Pánové	pán	k1gMnPc1	pán
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
fraky	frak	k1gInPc4	frak
(	(	kIx(	(
<g/>
příp	příp	kA	příp
<g/>
.	.	kIx.	.
vesty	vesta	k1gFnSc2	vesta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dámy	dáma	k1gFnPc1	dáma
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
šaty	šat	k1gInPc4	šat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
vývoj	vývoj	k1gInSc4	vývoj
zaznamenaly	zaznamenat	k5eAaPmAgInP	zaznamenat
během	běh	k1gInSc7	běh
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začaly	začít	k5eAaPmAgFnP	začít
vznikat	vznikat	k5eAaImF	vznikat
základní	základní	k2eAgInPc4d1	základní
popisy	popis	k1gInPc4	popis
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
standardní	standardní	k2eAgInSc1d1	standardní
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
dravostí	dravost	k1gFnSc7	dravost
<g/>
,	,	kIx,	,
obdivem	obdiv	k1gInSc7	obdiv
ke	k	k7c3	k
kráse	krása	k1gFnSc3	krása
<g/>
,	,	kIx,	,
romantičnosti	romantičnost	k1gFnSc3	romantičnost
a	a	k8xC	a
citovým	citový	k2eAgNnSc7d1	citové
prožitím	prožití	k1gNnSc7	prožití
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
standardní	standardní	k2eAgInPc4d1	standardní
tance	tanec	k1gInPc4	tanec
patří	patřit	k5eAaImIp3nS	patřit
waltz	waltz	k1gInSc1	waltz
<g/>
,	,	kIx,	,
tango	tango	k1gNnSc1	tango
<g/>
,	,	kIx,	,
valčík	valčík	k1gInSc1	valčík
<g/>
,	,	kIx,	,
slowfox	slowfox	k1gInSc1	slowfox
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
slowfoxtrot	slowfoxtrot	k1gInSc1	slowfoxtrot
<g/>
)	)	kIx)	)
a	a	k8xC	a
quickstep	quickstep	k1gInSc4	quickstep
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valčík	valčík	k1gInSc1	valčík
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
valčík	valčík	k1gInSc1	valčík
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Viennese	Viennese	k1gFnSc1	Viennese
Waltz	waltz	k1gInSc1	waltz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tango	tango	k1gNnSc1	tango
jako	jako	k8xS	jako
jediné	jediný	k2eAgNnSc1d1	jediné
ze	z	k7c2	z
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc4d1	ostatní
tance	tanec	k1gInPc4	tanec
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
waltz	waltz	k1gInSc1	waltz
<g/>
,	,	kIx,	,
slowfox	slowfox	k1gInSc1	slowfox
a	a	k8xC	a
quickstep	quickstep	k1gInSc1	quickstep
jsou	být	k5eAaImIp3nP	být
klasické	klasický	k2eAgFnPc4d1	klasická
standardní	standardní	k2eAgFnPc4d1	standardní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Latinskoamerické	latinskoamerický	k2eAgInPc4d1	latinskoamerický
tance	tanec	k1gInPc4	tanec
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
LAT	lat	k1gInSc1	lat
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
zkráceně	zkráceně	k6eAd1	zkráceně
latinské	latinský	k2eAgInPc1d1	latinský
<g/>
.	.	kIx.	.
</s>
<s>
Tance	tanec	k1gInPc1	tanec
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
jsou	být	k5eAaImIp3nP	být
uvolněnější	uvolněný	k2eAgMnPc1d2	uvolněnější
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblečení	oblečení	k1gNnSc6	oblečení
i	i	k8xC	i
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Latinské	latinský	k2eAgInPc4d1	latinský
tance	tanec	k1gInPc4	tanec
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
rytmická	rytmický	k2eAgFnSc1d1	rytmická
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
,	,	kIx,	,
temperament	temperament	k1gInSc1	temperament
<g/>
,	,	kIx,	,
smyslnost	smyslnost	k1gFnSc1	smyslnost
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
citový	citový	k2eAgInSc1d1	citový
náboj	náboj	k1gInSc1	náboj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
soutěžní	soutěžní	k2eAgInPc4d1	soutěžní
latinskoamerické	latinskoamerický	k2eAgInPc4d1	latinskoamerický
tance	tanec	k1gInPc4	tanec
patří	patřit	k5eAaImIp3nS	patřit
samba	samba	k1gFnSc1	samba
<g/>
,	,	kIx,	,
cha-cha	chaha	k1gFnSc1	cha-cha
<g/>
,	,	kIx,	,
rumba	rumba	k1gFnSc1	rumba
<g/>
,	,	kIx,	,
paso	paso	k6eAd1	paso
doble	doble	k1gInSc1	doble
a	a	k8xC	a
jive	jive	k1gInSc1	jive
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
soutěžích	soutěž	k1gFnPc6	soutěž
v	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
disciplíně	disciplína	k1gFnSc3	disciplína
u	u	k7c2	u
dětských	dětský	k2eAgFnPc2d1	dětská
kategorií	kategorie	k1gFnPc2	kategorie
přidává	přidávat	k5eAaImIp3nS	přidávat
polka	polka	k1gFnSc1	polka
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
není	být	k5eNaImIp3nS	být
latinsko-americkým	latinskomerický	k2eAgInSc7d1	latinsko-americký
tancem	tanec	k1gInSc7	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
samba	samba	k1gFnSc1	samba
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
rumba	rumba	k1gFnSc1	rumba
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
soutěžních	soutěžní	k2eAgInPc2d1	soutěžní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
cha-cha	chaha	k1gFnSc1	cha-cha
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
vznik	vznik	k1gInSc1	vznik
rumby	rumba	k1gFnSc2	rumba
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgInSc1d1	nejasný
a	a	k8xC	a
současný	současný	k2eAgInSc1d1	současný
kubánský	kubánský	k2eAgInSc1d1	kubánský
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgMnS	být
tomuto	tento	k3xDgInSc3	tento
tanci	tanec	k1gInSc3	tanec
připsán	připsán	k2eAgMnSc1d1	připsán
až	až	k6eAd1	až
po	po	k7c4	po
standardizaci	standardizace	k1gFnSc4	standardizace
pro	pro	k7c4	pro
soutěže	soutěž	k1gFnPc4	soutěž
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paso	Paso	k6eAd1	Paso
doble	doble	k6eAd1	doble
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc1d1	jediný
latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
tanec	tanec	k1gInSc1	tanec
nepochází	pocházet	k5eNaImIp3nS	pocházet
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
ze	z	k7c2	z
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
bere	brát	k5eAaImIp3nS	brát
si	se	k3xPyFc3	se
inspiraci	inspirace	k1gFnSc4	inspirace
z	z	k7c2	z
tamních	tamní	k2eAgInPc2d1	tamní
býčích	býčí	k2eAgInPc2d1	býčí
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
severoamerický	severoamerický	k2eAgInSc4d1	severoamerický
Jive	jive	k1gInSc4	jive
měl	mít	k5eAaImAgInS	mít
vliv	vliv	k1gInSc1	vliv
swing	swing	k1gInSc4	swing
<g/>
,	,	kIx,	,
boogie-woogie	boogieoogie	k1gNnSc4	boogie-woogie
i	i	k8xC	i
rock	rock	k1gInSc4	rock
and	and	k?	and
roll	roll	k1gInSc1	roll
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgMnSc3	který
je	být	k5eAaImIp3nS	být
jive	jive	k1gInSc1	jive
velmi	velmi	k6eAd1	velmi
fyzicky	fyzicky	k6eAd1	fyzicky
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
10	[number]	k4	10
tanců	tanec	k1gInPc2	tanec
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
10	[number]	k4	10
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
soutěží	soutěžit	k5eAaImIp3nP	soutěžit
páry	pár	k1gInPc1	pár
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
tancích	tanec	k1gInPc6	tanec
jak	jak	k8xS	jak
standardních	standardní	k2eAgFnPc6d1	standardní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
latinskoamerických	latinskoamerický	k2eAgFnPc6d1	latinskoamerická
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
za	za	k7c4	za
všech	všecek	k3xTgInPc2	všecek
10	[number]	k4	10
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
kategorii	kategorie	k1gFnSc6	kategorie
Děti	dítě	k1gFnPc4	dítě
I	i	k8xC	i
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Děti	dítě	k1gFnPc1	dítě
II	II	kA	II
tanců	tanec	k1gInPc2	tanec
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Formace	formace	k1gFnSc2	formace
===	===	k?	===
</s>
</p>
<p>
<s>
Soutěže	soutěž	k1gFnPc1	soutěž
skupin	skupina	k1gFnPc2	skupina
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
párů	pár	k1gInPc2	pár
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
nacvičené	nacvičený	k2eAgFnPc4d1	nacvičená
choreografie	choreografie	k1gFnPc4	choreografie
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
figury	figura	k1gFnPc1	figura
a	a	k8xC	a
prostorový	prostorový	k2eAgInSc1d1	prostorový
pohyb	pohyb	k1gInSc1	pohyb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soutěžní	soutěžní	k2eAgInSc1d1	soutěžní
systém	systém	k1gInSc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
účasti	účast	k1gFnSc6	účast
na	na	k7c6	na
postupových	postupový	k2eAgFnPc6d1	postupová
soutěžích	soutěž	k1gFnPc6	soutěž
páry	pára	k1gFnPc1	pára
získávají	získávat	k5eAaImIp3nP	získávat
tzv.	tzv.	kA	tzv.
postupové	postupový	k2eAgInPc1d1	postupový
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
základě	základ	k1gInSc6	základ
postupují	postupovat	k5eAaImIp3nP	postupovat
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
výkonnostních	výkonnostní	k2eAgFnPc2d1	výkonnostní
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
postupu	postup	k1gInSc3	postup
je	být	k5eAaImIp3nS	být
také	také	k9	také
nutná	nutný	k2eAgFnSc1d1	nutná
i	i	k8xC	i
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
získat	získat	k5eAaPmF	získat
200	[number]	k4	200
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
5	[number]	k4	5
účastí	účast	k1gFnSc7	účast
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
účast	účast	k1gFnSc1	účast
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
ještě	ještě	k9	ještě
není	být	k5eNaImIp3nS	být
rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgMnSc1d1	rozhodující
je	být	k5eAaImIp3nS	být
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
párů	pár	k1gInPc2	pár
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
tabulka	tabulka	k1gFnSc1	tabulka
níže	níže	k1gFnSc1	níže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzorec	vzorec	k1gInSc1	vzorec
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
postupových	postupový	k2eAgInPc2d1	postupový
bodů	bod	k1gInPc2	bod
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
body	bod	k1gInPc1	bod
=	=	kIx~	=
počet	počet	k1gInSc1	počet
poražených	poražený	k2eAgMnPc2d1	poražený
soupeřů	soupeř	k1gMnPc2	soupeř
+	+	kIx~	+
2	[number]	k4	2
<g/>
x	x	k?	x
počet	počet	k1gInSc1	počet
postupů	postup	k1gInPc2	postup
do	do	k7c2	do
vyššího	vysoký	k2eAgNnSc2d2	vyšší
kola	kolo	k1gNnSc2	kolo
soutěže	soutěž	k1gFnSc2	soutěž
+	+	kIx~	+
bonifikace	bonifikace	k1gFnPc1	bonifikace
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
soutěžících	soutěžící	k2eAgInPc2d1	soutěžící
párů	pár	k1gInPc2	pár
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
započítávaná	započítávaný	k2eAgNnPc4d1	započítávané
místa	místo	k1gNnPc4	místo
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Věkové	věkový	k2eAgFnSc2d1	věková
kategorie	kategorie	k1gFnSc2	kategorie
a	a	k8xC	a
výkonnostní	výkonnostní	k2eAgFnSc2d1	výkonnostní
třídy	třída	k1gFnSc2	třída
===	===	k?	===
</s>
</p>
<p>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
věkových	věkový	k2eAgFnPc2d1	věková
kategorií	kategorie	k1gFnPc2	kategorie
podle	podle	k7c2	podle
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
platí	platit	k5eAaImIp3nP	platit
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Přiřazení	přiřazení	k1gNnSc1	přiřazení
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
podle	podle	k7c2	podle
věku	věk	k1gInSc2	věk
staršího	starý	k2eAgInSc2d2	starší
z	z	k7c2	z
páru	pár	k1gInSc2	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Senioři	senior	k1gMnPc1	senior
je	on	k3xPp3gMnPc4	on
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
obrácené	obrácený	k2eAgNnSc1d1	obrácené
<g/>
,	,	kIx,	,
přiřazení	přiřazení	k1gNnSc1	přiřazení
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
řídí	řídit	k5eAaImIp3nS	řídit
podle	podle	k7c2	podle
mladšího	mladý	k2eAgMnSc2d2	mladší
z	z	k7c2	z
páru	pár	k1gInSc2	pár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
výkonnostních	výkonnostní	k2eAgFnPc2d1	výkonnostní
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
třídy	třída	k1gFnPc1	třída
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
<g/>
:	:	kIx,	:
E	E	kA	E
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
A	A	kA	A
a	a	k8xC	a
M.	M.	kA	M.
Při	při	k7c6	při
složení	složení	k1gNnSc6	složení
nového	nový	k2eAgInSc2d1	nový
páru	pár	k1gInSc2	pár
je	být	k5eAaImIp3nS	být
brán	brát	k5eAaImNgInS	brát
ohled	ohled	k1gInSc1	ohled
na	na	k7c4	na
několik	několik	k4yIc4	několik
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
měli	mít	k5eAaImAgMnP	mít
oba	dva	k4xCgMnPc1	dva
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
páru	pár	k1gInSc6	pár
stejnou	stejný	k2eAgFnSc4d1	stejná
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
přebírá	přebírat	k5eAaImIp3nS	přebírat
pár	pár	k4xCyI	pár
body	bod	k1gInPc4	bod
toho	ten	k3xDgMnSc4	ten
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
počtem	počet	k1gInSc7	počet
<g/>
.	.	kIx.	.
</s>
<s>
Finálová	finálový	k2eAgNnPc1d1	finálové
umístění	umístění	k1gNnPc1	umístění
se	se	k3xPyFc4	se
nepřebírají	přebírat	k5eNaImIp3nP	přebírat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
měli	mít	k5eAaImAgMnP	mít
rozdílnou	rozdílný	k2eAgFnSc4d1	rozdílná
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
zvolit	zvolit	k5eAaPmF	zvolit
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
rozsahu	rozsah	k1gInSc6	rozsah
(	(	kIx(	(
<g/>
partnerka	partnerka	k1gFnSc1	partnerka
měla	mít	k5eAaImAgFnS	mít
C	C	kA	C
<g/>
,	,	kIx,	,
partner	partner	k1gMnSc1	partner
A	A	kA	A
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
si	se	k3xPyFc3	se
zvolit	zvolit	k5eAaPmF	zvolit
C	C	kA	C
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřebírají	přebírat	k5eNaImIp3nP	přebírat
se	se	k3xPyFc4	se
ale	ale	k9	ale
žádné	žádný	k3yNgInPc1	žádný
body	bod	k1gInPc1	bod
a	a	k8xC	a
žádné	žádný	k3yNgNnSc1	žádný
finále	finále	k1gNnSc1	finále
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgNnSc1d1	osobní
zařazení	zařazení	k1gNnSc1	zařazení
ale	ale	k8xC	ale
nadále	nadále	k6eAd1	nadále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Člen	člen	k1gInSc1	člen
páru	pár	k1gInSc2	pár
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
osobní	osobní	k2eAgFnSc7d1	osobní
třídou	třída	k1gFnSc7	třída
získá	získat	k5eAaPmIp3nS	získat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
buď	buď	k8xC	buď
když	když	k8xS	když
celý	celý	k2eAgInSc1d1	celý
pár	pár	k1gInSc1	pár
získá	získat	k5eAaPmIp3nS	získat
novou	nový	k2eAgFnSc4d1	nová
třídu	třída	k1gFnSc4	třída
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
vytančí	vytančit	k5eAaPmIp3nS	vytančit
alespoň	alespoň	k9	alespoň
polovinu	polovina	k1gFnSc4	polovina
bodů	bod	k1gInPc2	bod
nutných	nutný	k2eAgInPc2d1	nutný
pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
do	do	k7c2	do
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pokud	pokud	k8xS	pokud
pár	pár	k4xCyI	pár
postoupí	postoupit	k5eAaPmIp3nP	postoupit
do	do	k7c2	do
druhého	druhý	k4xOgNnSc2	druhý
kola	kolo	k1gNnSc2	kolo
mistrovství	mistrovství	k1gNnSc2	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhy	druh	k1gInPc4	druh
soutěží	soutěž	k1gFnPc2	soutěž
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Postupové	postupový	k2eAgFnSc2d1	postupová
soutěže	soutěž	k1gFnSc2	soutěž
====	====	k?	====
</s>
</p>
<p>
<s>
Postupové	postupový	k2eAgFnPc1d1	postupová
soutěže	soutěž	k1gFnPc1	soutěž
jsou	být	k5eAaImIp3nP	být
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
,	,	kIx,	,
při	při	k7c6	při
nichž	jenž	k3xRgMnPc6	jenž
páry	pár	k1gInPc4	pár
získávají	získávat	k5eAaImIp3nP	získávat
body	bod	k1gInPc4	bod
za	za	k7c4	za
umístění	umístění	k1gNnSc4	umístění
a	a	k8xC	a
ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
získávají	získávat	k5eAaImIp3nP	získávat
výkonnostní	výkonnostní	k2eAgFnPc1d1	výkonnostní
třídy	třída	k1gFnPc1	třída
dle	dle	k7c2	dle
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
Finálových	finálový	k2eAgNnPc2d1	finálové
umístění	umístění	k1gNnPc2	umístění
(	(	kIx(	(
F	F	kA	F
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
200	[number]	k4	200
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
5	[number]	k4	5
<g/>
F.	F.	kA	F.
</s>
</p>
<p>
<s>
====	====	k?	====
Taneční	taneční	k2eAgFnSc1d1	taneční
liga	liga	k1gFnSc1	liga
====	====	k?	====
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
soutěží	soutěž	k1gFnPc2	soutěž
Taneční	taneční	k2eAgFnSc2d1	taneční
ligy	liga	k1gFnSc2	liga
pro	pro	k7c4	pro
kategorii	kategorie	k1gFnSc4	kategorie
Dospělí	dospělí	k1gMnPc1	dospělí
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kategorie	kategorie	k1gFnPc4	kategorie
Děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
Junioři	junior	k1gMnPc1	junior
I	I	kA	I
a	a	k8xC	a
II	II	kA	II
pak	pak	k6eAd1	pak
8	[number]	k4	8
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgInPc2	první
24	[number]	k4	24
párů	pár	k1gInPc2	pár
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Dospělí	dospělí	k1gMnPc1	dospělí
a	a	k8xC	a
prvních	první	k4xOgInPc2	první
6	[number]	k4	6
párů	pár	k1gInPc2	pár
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
Mládež	mládež	k1gFnSc1	mládež
a	a	k8xC	a
Junioři	junior	k1gMnPc1	junior
je	být	k5eAaImIp3nS	být
nasazeno	nasadit	k5eAaPmNgNnS	nasadit
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
Mistrovství	mistrovství	k1gNnPc2	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liga	liga	k1gFnSc1	liga
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
pro	pro	k7c4	pro
Dospělé	dospělý	k2eAgMnPc4d1	dospělý
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
také	také	k9	také
pro	pro	k7c4	pro
Děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
Juniory	junior	k1gMnPc4	junior
I	i	k8xC	i
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Mistrovství	mistrovství	k1gNnSc4	mistrovství
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
tanečním	taneční	k2eAgInSc6d1	taneční
sportu	sport	k1gInSc6	sport
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
pořádají	pořádat	k5eAaImIp3nP	pořádat
mistrovství	mistrovství	k1gNnSc4	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
deseti	deset	k4xCc2	deset
tancích	tanec	k1gInPc6	tanec
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
ve	v	k7c6	v
standardních	standardní	k2eAgInPc6d1	standardní
tancích	tanec	k1gInPc6	tanec
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
latinskoamerických	latinskoamerický	k2eAgInPc6d1	latinskoamerický
tancích	tanec	k1gInPc6	tanec
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc4	mistrovství
ČR	ČR	kA	ČR
družstev	družstvo	k1gNnPc2	družstvo
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
polce	polka	k1gFnSc6	polka
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
plesových	plesový	k2eAgFnPc6d1	plesová
formacích	formace	k1gFnPc6	formace
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
tanciPrvní	tanciPrvnit	k5eAaPmIp3nP	tanciPrvnit
čtyři	čtyři	k4xCgFnPc4	čtyři
soutěže	soutěž	k1gFnPc4	soutěž
pořádá	pořádat	k5eAaImIp3nS	pořádat
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
ČSTS	ČSTS	kA	ČSTS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
plesových	plesový	k2eAgFnPc6d1	plesová
formacích	formace	k1gFnPc6	formace
pořádá	pořádat	k5eAaImIp3nS	pořádat
Českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
taneční	taneční	k2eAgFnSc1d1	taneční
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
ČMTO	ČMTO	kA	ČMTO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
ČR	ČR	kA	ČR
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
polce	polka	k1gFnSc6	polka
a	a	k8xC	a
Mistrovství	mistrovství	k1gNnSc4	mistrovství
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
společenském	společenský	k2eAgInSc6d1	společenský
tanci	tanec	k1gInSc6	tanec
Svaz	svaz	k1gInSc1	svaz
učitelů	učitel	k1gMnPc2	učitel
tance	tanec	k1gInSc2	tanec
(	(	kIx(	(
<g/>
SUT	sut	k2eAgMnSc1d1	sut
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Soutěže	soutěž	k1gFnPc4	soutěž
světového	světový	k2eAgInSc2d1	světový
žebříčku	žebříček	k1gInSc2	žebříček
(	(	kIx(	(
<g/>
WRLT	WRLT	kA	WRLT
<g/>
,	,	kIx,	,
World	World	k1gInSc1	World
Ranking	Ranking	k1gInSc1	Ranking
List	list	k1gInSc1	list
Tournaments	Tournaments	k1gInSc4	Tournaments
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
Jsou	být	k5eAaImIp3nP	být
pořádány	pořádat	k5eAaImNgInP	pořádat
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
World	World	k6eAd1	World
Open	Open	k1gInSc1	Open
–	–	k?	–
1	[number]	k4	1
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
</s>
</p>
<p>
<s>
International	Internationat	k5eAaImAgMnS	Internationat
Open	Open	k1gMnSc1	Open
–	–	k?	–
nejvýše	vysoce	k6eAd3	vysoce
2	[number]	k4	2
<g/>
×	×	k?	×
ročně	ročně	k6eAd1	ročně
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
členské	členský	k2eAgFnSc6d1	členská
zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
výjimka	výjimka	k1gFnSc1	výjimka
–	–	k?	–
např.	např.	kA	např.
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
pořádá	pořádat	k5eAaImIp3nS	pořádat
3	[number]	k4	3
tyto	tento	k3xDgFnPc4	tento
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
–	–	k?	–
6	[number]	k4	6
soutěží	soutěž	k1gFnPc2	soutěž
z	z	k7c2	z
International	International	k1gFnSc2	International
Open	Opena	k1gFnPc2	Opena
</s>
</p>
<p>
<s>
Open	Open	k1gInSc1	Open
–	–	k?	–
bez	bez	k1gInSc4	bez
omezeníÚčastnit	omezeníÚčastnit	k5eAaImF	omezeníÚčastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
může	moct	k5eAaImIp3nS	moct
kdokoli	kdokoli	k3yInSc1	kdokoli
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
mít	mít	k5eAaImF	mít
minimálně	minimálně	k6eAd1	minimálně
třídu	třída	k1gFnSc4	třída
B	B	kA	B
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
státní	státní	k2eAgFnSc4d1	státní
reprezentaci	reprezentace	k1gFnSc4	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
soutěžících	soutěžící	k1gMnPc2	soutěžící
má	mít	k5eAaImIp3nS	mít
třídu	třída	k1gFnSc4	třída
M.	M.	kA	M.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
věkové	věkový	k2eAgFnSc6d1	věková
katergorii	katergorie	k1gFnSc6	katergorie
Senior	senior	k1gMnSc1	senior
I	i	k9	i
–	–	k?	–
IV	Iva	k1gFnPc2	Iva
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
jen	jen	k9	jen
soutěže	soutěž	k1gFnPc1	soutěž
typu	typ	k1gInSc2	typ
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
WRLT	WRLT	kA	WRLT
se	se	k3xPyFc4	se
párům	pár	k1gInPc3	pár
započítává	započítávat	k5eAaImIp3nS	započítávat
6	[number]	k4	6
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
výsledků	výsledek	k1gInPc2	výsledek
na	na	k7c6	na
soutěžích	soutěž	k1gFnPc6	soutěž
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
12	[number]	k4	12
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soutěže	soutěž	k1gFnSc2	soutěž
Grand	grand	k1gMnSc1	grand
Slam	sláma	k1gFnPc2	sláma
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
vysoké	vysoký	k2eAgFnPc4d1	vysoká
finanční	finanční	k2eAgFnPc4d1	finanční
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
5	[number]	k4	5
vybraných	vybraný	k2eAgFnPc2d1	vybraná
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
soutěží	soutěž	k1gFnPc2	soutěž
International	International	k1gMnSc1	International
Open	Open	k1gMnSc1	Open
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
<g/>
,	,	kIx,	,
pořádaná	pořádaný	k2eAgFnSc1d1	pořádaná
pro	pro	k7c4	pro
prvních	první	k4xOgInPc2	první
24	[number]	k4	24
párů	pár	k1gInPc2	pár
v	v	k7c6	v
aktuálním	aktuální	k2eAgNnSc6d1	aktuální
pořadí	pořadí	k1gNnSc6	pořadí
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
páry	pár	k1gInPc1	pár
získávají	získávat	k5eAaImIp3nP	získávat
dvojnásobný	dvojnásobný	k2eAgInSc4d1	dvojnásobný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc1	svaz
učitelů	učitel	k1gMnPc2	učitel
tance	tanec	k1gInSc2	tanec
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
federace	federace	k1gFnSc1	federace
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
taneční	taneční	k2eAgFnSc1d1	taneční
rada	rada	k1gFnSc1	rada
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
mistrů	mistr	k1gMnPc2	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
latinskoamerických	latinskoamerický	k2eAgInPc6d1	latinskoamerický
tancích	tanec	k1gInPc6	tanec
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
amatérských	amatérský	k2eAgMnPc2d1	amatérský
mistrů	mistr	k1gMnPc2	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
latinskoamerických	latinskoamerický	k2eAgInPc6d1	latinskoamerický
tancích	tanec	k1gInPc6	tanec
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
amatérských	amatérský	k2eAgMnPc2d1	amatérský
mistrů	mistr	k1gMnPc2	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
standardních	standardní	k2eAgInPc6d1	standardní
tancích	tanec	k1gInPc6	tanec
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
LANSFELD	LANSFELD	kA	LANSFELD
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
PLAMÍNEK	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LANSFELD	LANSFELD	kA	LANSFELD
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
PLAMÍNEK	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Technika	technika	k1gFnSc1	technika
latinsko-amerických	latinskomerický	k2eAgInPc2d1	latinsko-americký
tanců	tanec	k1gInPc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ODSTRČIL	Odstrčil	k1gMnSc1	Odstrčil
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1	sportovní
tanec	tanec	k1gInSc1	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
taneční	taneční	k2eAgInSc4d1	taneční
sport	sport	k1gInSc4	sport
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc1	svaz
učitelů	učitel	k1gMnPc2	učitel
tance	tanec	k1gInSc2	tanec
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
federace	federace	k1gFnSc1	federace
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
WDSF	WDSF	kA	WDSF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
taneční	taneční	k2eAgFnSc1d1	taneční
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
World	World	k1gInSc4	World
Dance	Danka	k1gFnSc3	Danka
Council	Council	k1gInSc1	Council
<g/>
,	,	kIx,	,
WDC	WDC	kA	WDC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Taneční	taneční	k2eAgInSc1d1	taneční
foto-portál	fotoortál	k1gInSc1	foto-portál
MajklůvSvět	MajklůvSvět	k1gInSc1	MajklůvSvět
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
WDCamateurleague	WDCamateurleague	k1gFnSc1	WDCamateurleague
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
o	o	k7c6	o
soutěžích	soutěž	k1gFnPc6	soutěž
amatérů	amatér	k1gMnPc2	amatér
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
;	;	kIx,	;
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
,	,	kIx,	,
termíny	termín	k1gInPc1	termín
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
žebříčky	žebříček	k1gInPc1	žebříček
<g/>
,	,	kIx,	,
foto	foto	k1gNnSc1	foto
a	a	k8xC	a
video	video	k1gNnSc1	video
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
DancesportInfo	DancesportInfo	k1gNnSc1	DancesportInfo
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
web	web	k1gInSc4	web
pro	pro	k7c4	pro
taneční	taneční	k2eAgInSc4d1	taneční
sport	sport	k1gInSc4	sport
s	s	k7c7	s
databází	databáze	k1gFnSc7	databáze
výsledků	výsledek	k1gInPc2	výsledek
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
žebříčkem	žebříček	k1gInSc7	žebříček
</s>
</p>
