<s>
Fed	Fed	k?	Fed
Cup	cup	k1gInSc1	cup
je	být	k5eAaImIp3nS	být
ženská	ženský	k2eAgFnSc1d1	ženská
týmová	týmový	k2eAgFnSc1d1	týmová
soutěž	soutěž	k1gFnSc1	soutěž
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Pohár	pohár	k1gInSc1	pohár
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
Federation	Federation	k1gInSc1	Federation
Cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
týmovou	týmový	k2eAgFnSc4d1	týmová
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
ženském	ženský	k2eAgInSc6d1	ženský
světovém	světový	k2eAgInSc6d1	světový
sportu	sport	k1gInSc6	sport
hranou	hrana	k1gFnSc7	hrana
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
světového	světový	k2eAgInSc2d1	světový
poháru	pohár	k1gInSc2	pohár
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
World	World	k1gMnSc1	World
Cup	cup	k1gInSc1	cup
of	of	k?	of
Tennis	Tennis	k1gInSc1	Tennis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Premiérový	premiérový	k2eAgInSc1d1	premiérový
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Nejvícekrát	Nejvícekrát	k6eAd1	Nejvícekrát
triumfovaly	triumfovat	k5eAaBmAgInP	triumfovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
17	[number]	k4	17
ročníků	ročník	k1gInPc2	ročník
a	a	k8xC	a
11	[number]	k4	11
<g/>
krát	krát	k6eAd1	krát
odešly	odejít	k5eAaPmAgInP	odejít
jako	jako	k8xS	jako
poražený	poražený	k2eAgMnSc1d1	poražený
finalista	finalista	k1gMnSc1	finalista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
probíhá	probíhat	k5eAaImIp3nS	probíhat
55	[number]	k4	55
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Trojnásobným	trojnásobný	k2eAgMnSc7d1	trojnásobný
obhájcem	obhájce	k1gMnSc7	obhájce
titulu	titul	k1gInSc2	titul
je	být	k5eAaImIp3nS	být
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
světovém	světový	k2eAgNnSc6d1	světové
finále	finále	k1gNnSc6	finále
předchozího	předchozí	k2eAgInSc2d1	předchozí
ročníku	ročník	k1gInSc2	ročník
porazila	porazit	k5eAaPmAgFnS	porazit
Francii	Francie	k1gFnSc4	Francie
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
tak	tak	k6eAd1	tak
jubilejní	jubilejní	k2eAgFnSc4d1	jubilejní
desátou	desátý	k4xOgFnSc4	desátý
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Mužskou	mužský	k2eAgFnSc7d1	mužská
obdobou	obdoba	k1gFnSc7	obdoba
je	být	k5eAaImIp3nS	být
týmová	týmový	k2eAgFnSc1d1	týmová
soutěž	soutěž	k1gFnSc1	soutěž
Davis	Davis	k1gFnSc2	Davis
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
první	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc7	třetí
a	a	k8xC	a
méně	málo	k6eAd2	málo
prestižním	prestižní	k2eAgInSc7d1	prestižní
kolektivním	kolektivní	k2eAgInSc7d1	kolektivní
turnajem	turnaj	k1gInSc7	turnaj
je	být	k5eAaImIp3nS	být
Hopmanův	Hopmanův	k2eAgInSc4d1	Hopmanův
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
smíšené	smíšený	k2eAgInPc4d1	smíšený
páry	pár	k1gInPc4	pár
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
Fed	Fed	k1gFnPc6	Fed
Cup	cup	k1gInSc1	cup
a	a	k8xC	a
Davis	Davis	k1gInSc1	Davis
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
kalendářním	kalendářní	k2eAgInSc6d1	kalendářní
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
Česko	Česko	k1gNnSc1	Česko
pak	pak	k6eAd1	pak
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
sezóně	sezóna	k1gFnSc6	sezóna
přidalo	přidat	k5eAaPmAgNnS	přidat
i	i	k9	i
triumf	triumf	k1gInSc4	triumf
na	na	k7c4	na
Hopman	Hopman	k1gMnSc1	Hopman
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
Fed	Fed	k1gFnSc2	Fed
Cupu	cup	k1gInSc2	cup
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
tenisové	tenisový	k2eAgFnSc2d1	tenisová
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
ITF	ITF	kA	ITF
<g/>
)	)	kIx)	)
uvedena	uveden	k2eAgFnSc1d1	uvedena
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
obdobou	obdoba	k1gFnSc7	obdoba
mužského	mužský	k2eAgNnSc2d1	mužské
Davis	Davis	k1gInSc1	Davis
Cupu	cup	k1gInSc6	cup
<g/>
.	.	kIx.	.
</s>
<s>
Založení	založení	k1gNnSc1	založení
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
Federation	Federation	k1gInSc1	Federation
Cupu	cup	k1gInSc2	cup
(	(	kIx(	(
<g/>
Poháru	pohár	k1gInSc2	pohár
federace	federace	k1gFnSc2	federace
<g/>
)	)	kIx)	)
odráželo	odrážet	k5eAaImAgNnS	odrážet
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
popularitu	popularita	k1gFnSc4	popularita
ženského	ženský	k2eAgInSc2d1	ženský
tenisu	tenis	k1gInSc2	tenis
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnSc4d1	související
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
podobném	podobný	k2eAgInSc6d1	podobný
typu	typ	k1gInSc6	typ
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
léta	léto	k1gNnSc2	léto
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
Fed	Fed	k1gMnSc1	Fed
Cup	cup	k1gInSc1	cup
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
platí	platit	k5eAaImIp3nS	platit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
prokázal	prokázat	k5eAaPmAgInS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
opodstatnění	opodstatnění	k1gNnSc4	opodstatnění
v	v	k7c6	v
tenisovém	tenisový	k2eAgInSc6d1	tenisový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
názvu	název	k1gInSc2	název
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
nově	nově	k6eAd1	nově
zavedeno	zavést	k5eAaPmNgNnS	zavést
pořádání	pořádání	k1gNnSc4	pořádání
finálových	finálový	k2eAgFnPc2d1	finálová
utkání	utkání	k1gNnPc4	utkání
systémem	systém	k1gInSc7	systém
domácí	domácí	k2eAgFnSc2d1	domácí
<g/>
–	–	k?	–
<g/>
hosté	host	k1gMnPc1	host
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
nehrálo	hrát	k5eNaImAgNnS	hrát
na	na	k7c6	na
předem	předem	k6eAd1	předem
vybraném	vybraný	k2eAgInSc6d1	vybraný
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
hřišti	hřiště	k1gNnSc6	hřiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
startovaly	startovat	k5eAaBmAgFnP	startovat
nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
světové	světový	k2eAgFnPc4d1	světová
tenistky	tenistka	k1gFnPc4	tenistka
jako	jako	k8xS	jako
Billie	Billie	k1gFnSc2	Billie
Jean	Jean	k1gMnSc1	Jean
Kingová	Kingový	k2eAgFnSc1d1	Kingová
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Evertová	Evertová	k1gFnSc1	Evertová
<g/>
,	,	kIx,	,
Virginia	Virginium	k1gNnPc1	Virginium
Wadeová	Wadeová	k1gFnSc1	Wadeová
<g/>
,	,	kIx,	,
Martina	Martina	k1gFnSc1	Martina
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
nebo	nebo	k8xC	nebo
Steffi	Steffi	k1gFnSc1	Steffi
Grafová	Grafová	k1gFnSc1	Grafová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
do	do	k7c2	do
poháru	pohár	k1gInSc2	pohár
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
103	[number]	k4	103
zemí	zem	k1gFnPc2	zem
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
jubilejní	jubilejní	k2eAgMnSc1d1	jubilejní
50	[number]	k4	50
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadovém	listopadový	k2eAgNnSc6d1	listopadové
finále	finále	k1gNnSc6	finále
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
areně	areně	k6eAd1	areně
střetly	střetnout	k5eAaPmAgFnP	střetnout
obhájce	obhájce	k1gMnSc1	obhájce
titulu	titul	k1gInSc2	titul
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
tým	tým	k1gInSc1	tým
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
sedmý	sedmý	k4xOgInSc4	sedmý
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc4	tři
tenistky	tenistka	k1gFnPc4	tenistka
Margaret	Margareta	k1gFnPc2	Margareta
Courtová	Courtová	k1gFnSc1	Courtová
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
Evertová	Evertová	k1gFnSc1	Evertová
a	a	k8xC	a
Billie	Billie	k1gFnSc1	Billie
Jean	Jean	k1gMnSc1	Jean
Kingová	Kingový	k2eAgFnSc1d1	Kingová
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
soutěž	soutěž	k1gFnSc1	soutěž
jako	jako	k8xS	jako
aktivní	aktivní	k2eAgFnPc1d1	aktivní
hráčky	hráčka	k1gFnPc1	hráčka
i	i	k9	i
jako	jako	k8xC	jako
nehrající	hrající	k2eNgFnPc1d1	nehrající
kapitánky	kapitánka	k1gFnPc1	kapitánka
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Pála	Pála	k1gMnSc1	Pála
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
nehrající	hrající	k2eNgMnSc1d1	nehrající
kapitán	kapitán	k1gMnSc1	kapitán
na	na	k7c6	na
pět	pět	k4xCc4	pět
titulů	titul	k1gInPc2	titul
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
vítězek	vítězka	k1gFnPc2	vítězka
Fed	Fed	k1gFnSc2	Fed
Cupu	cupat	k5eAaImIp1nS	cupat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
žebříčku	žebříček	k1gInSc2	žebříček
ITF	ITF	kA	ITF
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
zavedení	zavedení	k1gNnSc1	zavedení
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2002	[number]	k4	2002
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
pět	pět	k4xCc1	pět
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc4	bod
každého	každý	k3xTgInSc2	každý
týmu	tým	k1gInSc2	tým
ve	v	k7c6	v
Fed	Fed	k1gFnSc6	Fed
Cupu	cup	k1gInSc2	cup
jsou	být	k5eAaImIp3nP	být
kumulovány	kumulován	k2eAgInPc1d1	kumulován
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
posledních	poslední	k2eAgInPc2d1	poslední
ročníků	ročník	k1gInPc2	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc4d2	veliký
váhu	váha	k1gFnSc4	váha
mají	mít	k5eAaImIp3nP	mít
body	bod	k1gInPc1	bod
získané	získaný	k2eAgInPc1d1	získaný
nověji	nově	k6eAd2	nově
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
váha	váha	k1gFnSc1	váha
bodů	bod	k1gInPc2	bod
dosažených	dosažený	k2eAgInPc2d1	dosažený
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každém	každý	k3xTgNnSc6	každý
odehraném	odehraný	k2eAgNnSc6d1	odehrané
kole	kolo	k1gNnSc6	kolo
je	být	k5eAaImIp3nS	být
žebříček	žebříček	k1gInSc1	žebříček
aktualizován	aktualizován	k2eAgInSc1d1	aktualizován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
sumě	suma	k1gFnSc6	suma
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
100	[number]	k4	100
<g/>
%	%	kIx~	%
váhu	váha	k1gFnSc4	váha
body	bod	k1gInPc1	bod
získané	získaný	k2eAgInPc1d1	získaný
v	v	k7c6	v
posledním	poslední	k2eAgMnSc6d1	poslední
<g/>
,	,	kIx,	,
předešlém	předešlý	k2eAgInSc6d1	předešlý
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
počítané	počítaný	k2eAgFnSc2d1	počítaná
k	k	k7c3	k
datu	datum	k1gNnSc3	datum
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
součtu	součet	k1gInSc2	součet
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
přičteny	přičíst	k5eAaPmNgInP	přičíst
i	i	k9	i
body	bod	k1gInPc4	bod
získané	získaný	k2eAgInPc4d1	získaný
před	před	k7c7	před
dvěma	dva	k4xCgFnPc7	dva
<g/>
,	,	kIx,	,
třemi	tři	k4xCgNnPc7	tři
a	a	k8xC	a
čtyřmi	čtyři	k4xCgNnPc7	čtyři
lety	léto	k1gNnPc7	léto
k	k	k7c3	k
danému	daný	k2eAgNnSc3d1	dané
datu	datum	k1gNnSc3	datum
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
se	s	k7c7	s
sníženou	snížený	k2eAgFnSc7d1	snížená
váhou	váha	k1gFnSc7	váha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
váha	váha	k1gFnSc1	váha
bodů	bod	k1gInPc2	bod
dosažených	dosažený	k2eAgInPc2d1	dosažený
před	před	k7c7	před
2	[number]	k4	2
lety	léto	k1gNnPc7	léto
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
před	před	k7c7	před
3	[number]	k4	3
lety	let	k1gInPc7	let
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
před	před	k7c7	před
4	[number]	k4	4
lety	let	k1gInPc7	let
25	[number]	k4	25
%	%	kIx~	%
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
původní	původní	k2eAgFnSc2d1	původní
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
jsou	být	k5eAaImIp3nP	být
přidělovány	přidělovat	k5eAaImNgInP	přidělovat
pouze	pouze	k6eAd1	pouze
vítězným	vítězný	k2eAgInPc3d1	vítězný
týmům	tým	k1gInPc3	tým
daného	daný	k2eAgNnSc2d1	dané
kola	kolo	k1gNnSc2	kolo
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
ve	v	k7c6	v
světových	světový	k2eAgFnPc6d1	světová
skupinách	skupina	k1gFnPc6	skupina
jsou	být	k5eAaImIp3nP	být
bodováni	bodovat	k5eAaImNgMnP	bodovat
štědřeji	štědro	k6eAd2	štědro
než	než	k8xS	než
v	v	k7c6	v
kontinentálních	kontinentální	k2eAgFnPc6d1	kontinentální
zónách	zóna	k1gFnPc6	zóna
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
následná	následný	k2eAgNnPc1d1	následné
kola	kolo	k1gNnPc1	kolo
mají	mít	k5eAaImIp3nP	mít
vzestupnou	vzestupný	k2eAgFnSc4d1	vzestupná
bodovou	bodový	k2eAgFnSc4d1	bodová
gradaci	gradace	k1gFnSc4	gradace
<g/>
.	.	kIx.	.
</s>
<s>
Bonusové	bonusový	k2eAgInPc1d1	bonusový
body	bod	k1gInPc1	bod
náleží	náležet	k5eAaImIp3nP	náležet
družstvům	družstvo	k1gNnPc3	družstvo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
porazí	porazit	k5eAaPmIp3nP	porazit
výše	vysoce	k6eAd2	vysoce
postaveného	postavený	k2eAgMnSc4d1	postavený
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
soupeř	soupeř	k1gMnSc1	soupeř
umístěn	umístit	k5eAaPmNgMnS	umístit
do	do	k7c2	do
75	[number]	k4	75
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volném	volný	k2eAgInSc6d1	volný
losu	los	k1gInSc6	los
družstvo	družstvo	k1gNnSc1	družstvo
získává	získávat	k5eAaImIp3nS	získávat
standardní	standardní	k2eAgInSc4d1	standardní
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
za	za	k7c4	za
postup	postup	k1gInSc4	postup
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
kola	kolo	k1gNnSc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgInPc1	žádný
body	bod	k1gInPc1	bod
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
přidělovány	přidělovat	k5eAaImNgInP	přidělovat
z	z	k7c2	z
eventuálních	eventuální	k2eAgInPc2d1	eventuální
turnajů	turnaj	k1gInPc2	turnaj
útěchy	útěcha	k1gFnSc2	útěcha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
skupinách	skupina	k1gFnPc6	skupina
tří	tři	k4xCgFnPc2	tři
kontinentálních	kontinentální	k2eAgFnPc2d1	kontinentální
zón	zóna	k1gFnPc2	zóna
jsou	být	k5eAaImIp3nP	být
body	bod	k1gInPc1	bod
každého	každý	k3xTgInSc2	každý
týmu	tým	k1gInSc2	tým
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jeho	jeho	k3xOp3gNnSc2	jeho
výsledného	výsledný	k2eAgNnSc2d1	výsledné
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Suma	suma	k1gFnSc1	suma
bodového	bodový	k2eAgNnSc2d1	bodové
ohodnocení	ohodnocení	k1gNnSc2	ohodnocení
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
počtu	počet	k1gInSc6	počet
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
daný	daný	k2eAgInSc1d1	daný
rok	rok	k1gInSc1	rok
soutěže	soutěž	k1gFnSc2	soutěž
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
maximální	maximální	k2eAgInSc1d1	maximální
bodový	bodový	k2eAgInSc1d1	bodový
strop	strop	k1gInSc1	strop
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
skupině	skupina	k1gFnSc6	skupina
neměnný	neměnný	k2eAgMnSc1d1	neměnný
<g/>
.	.	kIx.	.
</s>
