<s>
Kobalt	kobalt	k1gInSc1	kobalt
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Co	co	k9	co
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Cobaltum	Cobaltum	k1gNnSc1	Cobaltum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
namodralý	namodralý	k2eAgMnSc1d1	namodralý
<g/>
,	,	kIx,	,
feromagnetický	feromagnetický	k2eAgInSc1d1	feromagnetický
<g/>
,	,	kIx,	,
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
pro	pro	k7c4	pro
zlepšování	zlepšování	k1gNnSc4	zlepšování
vlastností	vlastnost	k1gFnPc2	vlastnost
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
při	při	k7c6	při
barvení	barvení	k1gNnSc6	barvení
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
keramiky	keramika	k1gFnSc2	keramika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc1d1	důležitý
i	i	k9	i
biologicky	biologicky	k6eAd1	biologicky
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
kovový	kovový	k2eAgInSc1d1	kovový
ferromagnetický	ferromagnetický	k2eAgInSc1d1	ferromagnetický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1	stříbrolesklý
s	s	k7c7	s
modrým	modrý	k2eAgInSc7d1	modrý
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pevný	pevný	k2eAgInSc1d1	pevný
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
tvrdostí	tvrdost	k1gFnSc7	tvrdost
a	a	k8xC	a
pevností	pevnost	k1gFnSc7	pevnost
předčí	předčit	k5eAaBmIp3nP	předčit
ocel	ocel	k1gFnSc4	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
je	být	k5eAaImIp3nS	být
feromagnetický	feromagnetický	k2eAgInSc1d1	feromagnetický
do	do	k7c2	do
teploty	teplota	k1gFnSc2	teplota
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
teplotou	teplota	k1gFnSc7	teplota
své	svůj	k3xOyFgFnPc4	svůj
feromagnetické	feromagnetický	k2eAgFnPc4d1	feromagnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
kov	kov	k1gInSc1	kov
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
feromagnetické	feromagnetický	k2eAgFnPc4d1	feromagnetická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Curieův	Curieův	k2eAgInSc1d1	Curieův
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
především	především	k9	především
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
Co	co	k9	co
<g/>
+	+	kIx~	+
<g/>
II	II	kA	II
a	a	k8xC	a
Co	co	k9	co
<g/>
+	+	kIx~	+
<g/>
III	III	kA	III
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
Co	co	k9	co
<g/>
+	+	kIx~	+
<g/>
I	i	k9	i
<g/>
,	,	kIx,	,
Co	co	k9	co
<g/>
+	+	kIx~	+
<g/>
IV	IV	kA	IV
a	a	k8xC	a
od	od	k7c2	od
nedávna	nedávno	k1gNnSc2	nedávno
také	také	k9	také
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
nestabilními	stabilní	k2eNgFnPc7d1	nestabilní
sloučeninami	sloučenina	k1gFnPc7	sloučenina
v	v	k7c6	v
oxidačním	oxidační	k2eAgInSc6d1	oxidační
stavu	stav	k1gInSc6	stav
Co	co	k8xS	co
<g/>
+	+	kIx~	+
<g/>
V	v	k7c6	v
například	například	k6eAd1	například
Na	na	k7c6	na
<g/>
3	[number]	k4	3
<g/>
CoVO	CoVO	k1gFnSc6	CoVO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
alotropických	alotropický	k2eAgFnPc6d1	alotropický
modifikacích	modifikace	k1gFnPc6	modifikace
označovaných	označovaný	k2eAgInPc2d1	označovaný
α	α	k?	α
a	a	k8xC	a
β	β	k?	β
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgNnSc4d1	stabilní
za	za	k7c4	za
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
teploty	teplota	k1gFnPc4	teplota
a	a	k8xC	a
kobalt	kobalt	k1gInSc4	kobalt
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
těsné	těsný	k2eAgNnSc4d1	těsné
uspořádání	uspořádání	k1gNnSc4	uspořádání
v	v	k7c6	v
hexagonální	hexagonální	k2eAgFnSc6d1	hexagonální
mřížce	mřížka	k1gFnSc6	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
modifikace	modifikace	k1gFnSc1	modifikace
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
nad	nad	k7c7	nad
teplotou	teplota	k1gFnSc7	teplota
417	[number]	k4	417
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
kobalt	kobalt	k1gInSc4	kobalt
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
uspořádání	uspořádání	k1gNnSc1	uspořádání
atomů	atom	k1gInPc2	atom
v	v	k7c6	v
plošně	plošně	k6eAd1	plošně
centrované	centrovaný	k2eAgFnSc6d1	centrovaná
kubické	kubický	k2eAgFnSc6d1	kubická
mřížce	mřížka	k1gFnSc6	mřížka
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
mezi	mezi	k7c7	mezi
modifikacemi	modifikace	k1gFnPc7	modifikace
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kompaktním	kompaktní	k2eAgInSc6d1	kompaktní
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
kobalt	kobalt	k1gInSc1	kobalt
vůči	vůči	k7c3	vůči
vzduchu	vzduch	k1gInSc3	vzduch
i	i	k8xC	i
vodě	voda	k1gFnSc3	voda
stabilní	stabilní	k2eAgFnPc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jemně	jemně	k6eAd1	jemně
rozptýleném	rozptýlený	k2eAgInSc6d1	rozptýlený
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
kobalt	kobalt	k1gInSc1	kobalt
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
železo	železo	k1gNnSc1	železo
pyroforický	pyroforický	k2eAgMnSc1d1	pyroforický
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
samozápalný	samozápalný	k2eAgMnSc1d1	samozápalný
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
chlorovodíková	chlorovodíkový	k2eAgFnSc1d1	chlorovodíková
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
a	a	k8xC	a
kyselina	kyselina	k1gFnSc1	kyselina
dusičná	dusičný	k2eAgFnSc1d1	dusičná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
neochotně	ochotně	k6eNd1	ochotně
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
se	se	k3xPyFc4	se
kobalt	kobalt	k1gInSc4	kobalt
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
železo	železo	k1gNnSc1	železo
pouze	pouze	k6eAd1	pouze
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
shoří	shořet	k5eAaPmIp3nS	shořet
kobalt	kobalt	k1gInSc1	kobalt
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
kobaltnato-kobaltitý	kobaltnatoobaltitý	k2eAgInSc4d1	kobaltnato-kobaltitý
Co	co	k8xS	co
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
nad	nad	k7c7	nad
900	[number]	k4	900
°	°	k?	°
<g/>
C	C	kA	C
shoří	shořet	k5eAaPmIp3nS	shořet
kobalt	kobalt	k1gInSc4	kobalt
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoO	CoO	k1gFnSc7	CoO
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
při	při	k7c6	při
působení	působení	k1gNnSc6	působení
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
na	na	k7c4	na
kov	kov	k1gInSc4	kov
zahřátý	zahřátý	k2eAgInSc4d1	zahřátý
do	do	k7c2	do
červeného	červený	k2eAgInSc2d1	červený
žáru	žár	k1gInSc2	žár
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
je	být	k5eAaImIp3nS	být
kobalt	kobalt	k1gInSc1	kobalt
málo	málo	k6eAd1	málo
reaktivní	reaktivní	k2eAgInSc1d1	reaktivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
vyšších	vysoký	k2eAgFnPc2d2	vyšší
teplot	teplota	k1gFnPc2	teplota
se	se	k3xPyFc4	se
často	často	k6eAd1	často
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
plamene	plamen	k1gInSc2	plamen
slučuje	slučovat	k5eAaImIp3nS	slučovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
prvky	prvek	k1gInPc7	prvek
(	(	kIx(	(
<g/>
síra	síra	k1gFnSc1	síra
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
arsen	arsen	k1gInSc1	arsen
<g/>
,	,	kIx,	,
antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
bor	bor	k1gInSc1	bor
<g/>
,	,	kIx,	,
halogeny	halogen	k1gInPc1	halogen
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Neslučuje	slučovat	k5eNaImIp3nS	slučovat
se	se	k3xPyFc4	se
však	však	k9	však
s	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
a	a	k8xC	a
vodíkem	vodík	k1gInSc7	vodík
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prvním	první	k4xOgInSc6	první
používání	používání	k1gNnSc6	používání
sloučenin	sloučenina	k1gFnPc2	sloučenina
kobaltu	kobalt	k1gInSc2	kobalt
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
z	z	k7c2	z
egyptských	egyptský	k2eAgInPc2d1	egyptský
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
2600	[number]	k4	2600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
keramiku	keramik	k1gMnSc3	keramik
a	a	k8xC	a
skleněné	skleněný	k2eAgFnPc4d1	skleněná
perly	perla	k1gFnPc4	perla
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
barveny	barvit	k5eAaImNgFnP	barvit
kobaltovou	kobaltový	k2eAgFnSc7d1	kobaltová
modří	modř	k1gFnSc7	modř
do	do	k7c2	do
modra	modro	k1gNnSc2	modro
<g/>
.	.	kIx.	.
</s>
<s>
Horníci	Horník	k1gMnPc1	Horník
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k9	jako
kobalty	kobalt	k1gInPc1	kobalt
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
kovový	kovový	k2eAgInSc4d1	kovový
vzhled	vzhled	k1gInSc4	vzhled
nedaly	dát	k5eNaPmAgFnP	dát
hutnicky	hutnicky	k6eAd1	hutnicky
zpracovat	zpracovat	k5eAaPmF	zpracovat
na	na	k7c4	na
kov	kov	k1gInSc4	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
kobalt	kobalt	k1gInSc1	kobalt
má	mít	k5eAaImIp3nS	mít
základ	základ	k1gInSc4	základ
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
pojmenování	pojmenování	k1gNnSc6	pojmenování
skřítků	skřítek	k1gMnPc2	skřítek
Koboltů	kobolt	k1gMnPc2	kobolt
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
spíše	spíše	k9	spíše
zlomyslní	zlomyslný	k2eAgMnPc1d1	zlomyslný
a	a	k8xC	a
kazili	kazit	k5eAaImAgMnP	kazit
horníkům	horník	k1gMnPc3	horník
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
kobaltu	kobalt	k1gInSc2	kobalt
totiž	totiž	k9	totiž
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
zpracovatelnost	zpracovatelnost	k1gFnSc4	zpracovatelnost
vytěžených	vytěžený	k2eAgFnPc2d1	vytěžená
niklových	niklový	k2eAgFnPc2d1	niklová
rud	ruda	k1gFnPc2	ruda
a	a	k8xC	a
kobaltové	kobaltový	k2eAgFnSc2d1	kobaltová
příměsi	příměs	k1gFnSc2	příměs
i	i	k9	i
více	hodně	k6eAd2	hodně
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
jejich	jejich	k3xOp3gNnSc4	jejich
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
pražení	pražení	k1gNnSc6	pražení
uvolňovaly	uvolňovat	k5eAaImAgInP	uvolňovat
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
jedovaté	jedovatý	k2eAgInPc1d1	jedovatý
plyny	plyn	k1gInPc1	plyn
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
As	as	k1gInSc4	as
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
omezilo	omezit	k5eAaPmAgNnS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
rudy	ruda	k1gFnPc4	ruda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
obtížně	obtížně	k6eAd1	obtížně
hutnicky	hutnicky	k6eAd1	hutnicky
zpracovat	zpracovat	k5eAaPmF	zpracovat
a	a	k8xC	a
barvily	barvit	k5eAaImAgFnP	barvit
sklo	sklo	k1gNnSc4	sklo
na	na	k7c4	na
modro	modro	k1gNnSc4	modro
<g/>
.	.	kIx.	.
</s>
<s>
Kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
součástí	součást	k1gFnSc7	součást
těchto	tento	k3xDgFnPc2	tento
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
objevil	objevit	k5eAaPmAgMnS	objevit
roku	rok	k1gInSc2	rok
1735	[number]	k4	1735
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
Georg	Georg	k1gMnSc1	Georg
Bradnt	Bradnt	k1gMnSc1	Bradnt
a	a	k8xC	a
nazval	nazvat	k5eAaBmAgMnS	nazvat
jej	on	k3xPp3gMnSc4	on
cobalt	cobalt	k5eAaPmF	cobalt
rex	rex	k?	rex
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
T.	T.	kA	T.
O.	O.	kA	O.
Bergman	Bergman	k1gMnSc1	Bergman
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
příbuznému	příbuzný	k2eAgInSc3d1	příbuzný
niklu	nikl	k1gInSc3	nikl
je	být	k5eAaImIp3nS	být
zastoupení	zastoupení	k1gNnSc1	zastoupení
kobaltu	kobalt	k1gInSc2	kobalt
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
činí	činit	k5eAaImIp3nS	činit
průměrný	průměrný	k2eAgInSc4d1	průměrný
obsah	obsah	k1gInSc4	obsah
kobaltu	kobalt	k1gInSc2	kobalt
kolem	kolem	k7c2	kolem
25	[number]	k4	25
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
výskytu	výskyt	k1gInSc6	výskyt
prvků	prvek	k1gInPc2	prvek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
procentuální	procentuální	k2eAgInSc1d1	procentuální
obsah	obsah	k1gInSc1	obsah
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
29	[number]	k4	29
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
0,27	[number]	k4	0,27
mikrogramu	mikrogram	k1gInSc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
kobaltu	kobalt	k1gInSc2	kobalt
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nejsou	být	k5eNaImIp3nP	být
známa	znám	k2eAgNnPc1d1	známo
naleziště	naleziště	k1gNnPc1	naleziště
rud	ruda	k1gFnPc2	ruda
s	s	k7c7	s
převažujícím	převažující	k2eAgNnSc7d1	převažující
množstvím	množství	k1gNnSc7	množství
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
niklové	niklový	k2eAgFnPc4d1	niklová
rudy	ruda	k1gFnPc4	ruda
a	a	k8xC	a
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
jej	on	k3xPp3gInSc4	on
i	i	k9	i
jako	jako	k9	jako
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
prvek	prvek	k1gInSc1	prvek
v	v	k7c6	v
sulfidických	sulfidický	k2eAgFnPc6d1	sulfidická
rudách	ruda	k1gFnPc6	ruda
mědi	měď	k1gFnSc2	měď
nebo	nebo	k8xC	nebo
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
nerosty	nerost	k1gInPc1	nerost
kobaltu	kobalt	k1gInSc2	kobalt
jsou	být	k5eAaImIp3nP	být
smaltin	smaltin	k1gInSc4	smaltin
CoAs	CoAs	k1gInSc1	CoAs
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
linnéit	linnéit	k2eAgInSc1d1	linnéit
Co	co	k9	co
<g/>
3	[number]	k4	3
<g/>
S	s	k7c7	s
<g/>
4	[number]	k4	4
a	a	k8xC	a
kobaltit	kobaltit	k1gInSc1	kobaltit
CoAsS	CoAsS	k1gFnSc2	CoAsS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Kaledonii	Kaledonie	k1gFnSc6	Kaledonie
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
manganová	manganový	k2eAgFnSc1d1	manganová
čerň	čerň	k1gFnSc1	čerň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
kobalt	kobalt	k1gInSc4	kobalt
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
asbolan	asbolan	k1gMnSc1	asbolan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ryzím	ryzí	k2eAgInSc6d1	ryzí
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaPmF	nalézt
kobalt	kobalt	k1gInSc4	kobalt
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
2,5	[number]	k4	2,5
%	%	kIx~	%
v	v	k7c6	v
železných	železný	k2eAgInPc6d1	železný
meteoritech	meteorit	k1gInPc6	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
zásoby	zásoba	k1gFnPc1	zásoba
rud	ruda	k1gFnPc2	ruda
s	s	k7c7	s
významným	významný	k2eAgInSc7d1	významný
podílem	podíl	k1gInSc7	podíl
kobaltu	kobalt	k1gInSc2	kobalt
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Demokratické	demokratický	k2eAgFnSc6d1	demokratická
republice	republika	k1gFnSc6	republika
Kongo	Kongo	k1gNnSc4	Kongo
a	a	k8xC	a
Zambii	Zambie	k1gFnSc4	Zambie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
vytěžilo	vytěžit	k5eAaPmAgNnS	vytěžit
22	[number]	k4	22
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
kobaltových	kobaltový	k2eAgFnPc2d1	kobaltová
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kobaltu	kobalt	k1gInSc2	kobalt
tvoří	tvořit	k5eAaImIp3nP	tvořit
míšně	míšeň	k1gFnPc1	míšeň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
získávají	získávat	k5eAaImIp3nP	získávat
při	při	k7c6	při
hutnickém	hutnický	k2eAgNnSc6d1	hutnické
zpracování	zpracování	k1gNnSc6	zpracování
rud	ruda	k1gFnPc2	ruda
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
arsen	arsen	k1gInSc4	arsen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
kobalt	kobalt	k1gInSc1	kobalt
přítomen	přítomen	k2eAgInSc1d1	přítomen
zejména	zejména	k9	zejména
jako	jako	k9	jako
arsenid	arsenid	k1gInSc4	arsenid
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
míšně	míšeň	k1gFnSc2	míšeň
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
oxidy	oxid	k1gInPc1	oxid
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgFnP	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kobaltových	kobaltový	k2eAgFnPc2d1	kobaltová
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
nemusely	muset	k5eNaImAgInP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
příliš	příliš	k6eAd1	příliš
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
však	však	k9	však
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
zejména	zejména	k9	zejména
kovový	kovový	k2eAgInSc1d1	kovový
kobalt	kobalt	k1gInSc1	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
čistého	čistý	k2eAgInSc2d1	čistý
kobaltu	kobalt	k1gInSc2	kobalt
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
největší	veliký	k2eAgInSc1d3	veliký
problém	problém	k1gInSc1	problém
činí	činit	k5eAaImIp3nS	činit
odstranit	odstranit	k5eAaPmF	odstranit
nikl	nikl	k1gInSc4	nikl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
kobaltových	kobaltový	k2eAgFnPc2d1	kobaltová
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
postupuje	postupovat	k5eAaImIp3nS	postupovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rudy	ruda	k1gFnSc2	ruda
kobaltu	kobalt	k1gInSc2	kobalt
nebo	nebo	k8xC	nebo
míšně	míšeň	k1gFnSc2	míšeň
pražením	pražení	k1gNnSc7	pražení
převedou	převést	k5eAaPmIp3nP	převést
na	na	k7c4	na
směs	směs	k1gFnSc4	směs
oxidů	oxid	k1gInPc2	oxid
a	a	k8xC	a
arseničnanů	arseničnan	k1gInPc2	arseničnan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
červeného	červený	k2eAgNnSc2d1	červené
zbarvení	zbarvení	k1gNnSc2	zbarvení
nazývá	nazývat	k5eAaImIp3nS	nazývat
safor	safor	k1gInSc1	safor
nebo	nebo	k8xC	nebo
cafra	cafra	k1gFnSc1	cafra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
se	se	k3xPyFc4	se
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc2d1	chlorovodíková
a	a	k8xC	a
sulfanem	sulfan	k1gInSc7	sulfan
se	se	k3xPyFc4	se
srazí	srazit	k5eAaPmIp3nS	srazit
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
bismut	bismut	k1gInSc1	bismut
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oxidaci	oxidace	k1gFnSc6	oxidace
chlorem	chlor	k1gInSc7	chlor
se	se	k3xPyFc4	se
srazí	srazit	k5eAaPmIp3nS	srazit
arsen	arsen	k1gInSc1	arsen
a	a	k8xC	a
železo	železo	k1gNnSc4	železo
uhličitanem	uhličitan	k1gInSc7	uhličitan
vápenatým	vápenatý	k2eAgInSc7d1	vápenatý
jako	jako	k8xC	jako
hydroxid	hydroxid	k1gInSc1	hydroxid
železitý	železitý	k2eAgInSc1d1	železitý
a	a	k8xC	a
arseničnan	arseničnan	k1gInSc1	arseničnan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
chlorové	chlorový	k2eAgNnSc1d1	chlorové
vápno	vápno	k1gNnSc1	vápno
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
srazil	srazit	k5eAaPmAgMnS	srazit
jen	jen	k9	jen
kobalt	kobalt	k1gInSc4	kobalt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
sráží	srážet	k5eAaImIp3nS	srážet
přednostně	přednostně	k6eAd1	přednostně
před	před	k7c7	před
niklem	nikl	k1gInSc7	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
zůstane	zůstat	k5eAaPmIp3nS	zůstat
nikl	nikl	k1gInSc1	nikl
přítomen	přítomen	k2eAgInSc1d1	přítomen
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
a	a	k8xC	a
kobalt	kobalt	k1gInSc1	kobalt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svého	svůj	k3xOyFgInSc2	svůj
oxidu	oxid	k1gInSc2	oxid
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
od	od	k7c2	od
stop	stop	k2eAgInSc2d1	stop
niklu	nikl	k1gInSc2	nikl
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
přečistit	přečistit	k5eAaPmF	přečistit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
kobaltnato-kobaltičitý	kobaltnatoobaltičitý	k2eAgInSc1d1	kobaltnato-kobaltičitý
redukuje	redukovat	k5eAaBmIp3nS	redukovat
uhlím	uhlí	k1gNnSc7	uhlí
nebo	nebo	k8xC	nebo
koksem	koks	k1gInSc7	koks
a	a	k8xC	a
získá	získat	k5eAaPmIp3nS	získat
se	se	k3xPyFc4	se
tak	tak	k9	tak
kovový	kovový	k2eAgInSc1d1	kovový
kobalt	kobalt	k1gInSc1	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
kobaltu	kobalt	k1gInSc2	kobalt
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
poměrně	poměrně	k6eAd1	poměrně
nízkému	nízký	k2eAgInSc3d1	nízký
výskytu	výskyt	k1gInSc3	výskyt
i	i	k8xC	i
obtížnosti	obtížnost	k1gFnSc3	obtížnost
výroby	výroba	k1gFnSc2	výroba
dosti	dosti	k6eAd1	dosti
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
obdobích	období	k1gNnPc6	období
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
burzovní	burzovní	k2eAgFnSc1d1	burzovní
cena	cena	k1gFnSc1	cena
kobaltu	kobalt	k1gInSc2	kobalt
úrovně	úroveň	k1gFnSc2	úroveň
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
především	především	k9	především
v	v	k7c6	v
metalurgii	metalurgie	k1gFnSc6	metalurgie
využíván	využíván	k2eAgMnSc1d1	využíván
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
nahradit	nahradit	k5eAaPmF	nahradit
některým	některý	k3yIgInSc7	některý
levnějším	levný	k2eAgInSc7d2	levnější
kovem	kov	k1gInSc7	kov
a	a	k8xC	a
do	do	k7c2	do
slitin	slitina	k1gFnPc2	slitina
je	být	k5eAaImIp3nS	být
legován	legován	k2eAgInSc1d1	legován
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
nízkém	nízký	k2eAgNnSc6d1	nízké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ocelářském	ocelářský	k2eAgInSc6d1	ocelářský
průmyslu	průmysl	k1gInSc6	průmysl
slouží	sloužit	k5eAaImIp3nS	sloužit
kobalt	kobalt	k1gInSc1	kobalt
jako	jako	k8xS	jako
složka	složka	k1gFnSc1	složka
některých	některý	k3yIgFnPc2	některý
nástrojových	nástrojový	k2eAgFnPc2d1	nástrojová
ocelí	ocel	k1gFnPc2	ocel
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
vykazovat	vykazovat	k5eAaImF	vykazovat
vynikající	vynikající	k2eAgFnPc4d1	vynikající
mechanické	mechanický	k2eAgFnPc4d1	mechanická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
-	-	kIx~	-
tvrdost	tvrdost	k1gFnSc4	tvrdost
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ocelí	ocel	k1gFnPc2	ocel
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
přípravky	přípravek	k1gInPc1	přípravek
pro	pro	k7c4	pro
obrábění	obrábění	k1gNnSc4	obrábění
kovů	kov	k1gInPc2	kov
(	(	kIx(	(
<g/>
rychlořezná	rychlořezný	k2eAgFnSc1d1	rychlořezná
ocel	ocel	k1gFnSc1	ocel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
např.	např.	kA	např.
turbíny	turbína	k1gFnPc4	turbína
plynových	plynový	k2eAgInPc2d1	plynový
generátorů	generátor	k1gInPc2	generátor
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
vrtné	vrtný	k2eAgFnSc2d1	vrtná
hlavice	hlavice	k1gFnSc2	hlavice
pro	pro	k7c4	pro
geologický	geologický	k2eAgInSc4d1	geologický
průzkum	průzkum	k1gInSc4	průzkum
<g/>
.	.	kIx.	.
</s>
<s>
Stellity	Stellit	k2eAgFnPc1d1	Stellit
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc1	slitina
kobaltu	kobalt	k1gInSc2	kobalt
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
%	%	kIx~	%
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
27	[number]	k4	27
%	%	kIx~	%
chromu	chrom	k1gInSc2	chrom
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
wolframu	wolfram	k1gInSc2	wolfram
<g/>
,	,	kIx,	,
2,5	[number]	k4	2,5
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stopovém	stopový	k2eAgNnSc6d1	stopové
množství	množství	k1gNnSc6	množství
ještě	ještě	k6eAd1	ještě
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mangan	mangan	k1gInSc4	mangan
a	a	k8xC	a
křemík	křemík	k1gInSc4	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgFnPc1d1	tvrdá
slitiny	slitina	k1gFnPc1	slitina
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
odlévání	odlévání	k1gNnSc4	odlévání
součástí	součást	k1gFnPc2	součást
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
obráběcích	obráběcí	k2eAgInPc2d1	obráběcí
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
tvrdší	tvrdý	k2eAgFnSc2d2	tvrdší
než	než	k8xS	než
rychlořezné	rychlořezný	k2eAgFnSc2d1	rychlořezná
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
podstatně	podstatně	k6eAd1	podstatně
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
velmi	velmi	k6eAd1	velmi
silných	silný	k2eAgInPc2d1	silný
permanentních	permanentní	k2eAgInPc2d1	permanentní
magnetů	magnet	k1gInPc2	magnet
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
typ	typ	k1gInSc1	typ
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Alnico	Alnico	k6eAd1	Alnico
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc4d1	silný
permanentní	permanentní	k2eAgInSc4d1	permanentní
magnet	magnet	k1gInSc4	magnet
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
překoná	překonat	k5eAaPmIp3nS	překonat
25	[number]	k4	25
<g/>
krát	krát	k6eAd1	krát
výkon	výkon	k1gInSc4	výkon
ocelových	ocelový	k2eAgInPc2d1	ocelový
magnetů	magnet	k1gInPc2	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
lepší	dobrý	k2eAgFnPc1d2	lepší
magnetické	magnetický	k2eAgFnPc1d1	magnetická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
mají	mít	k5eAaImIp3nP	mít
slitiny	slitina	k1gFnPc4	slitina
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
samaria	samarium	k1gNnSc2	samarium
<g/>
,	,	kIx,	,
vyvinuté	vyvinutý	k2eAgFnSc2d1	vyvinutá
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vysokou	vysoký	k2eAgFnSc4d1	vysoká
koercivitu	koercivita	k1gFnSc4	koercivita
<g/>
,	,	kIx,	,
odolnost	odolnost	k1gFnSc1	odolnost
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc4d1	dobrá
teplotní	teplotní	k2eAgFnSc4d1	teplotní
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
až	až	k9	až
do	do	k7c2	do
550	[number]	k4	550
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
Curieova	Curieův	k2eAgFnSc1d1	Curieova
teplota	teplota	k1gFnSc1	teplota
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
800	[number]	k4	800
°	°	k?	°
<g/>
C.	C.	kA	C.
Co	co	k9	co
do	do	k7c2	do
magnetických	magnetický	k2eAgFnPc2d1	magnetická
vlastností	vlastnost	k1gFnPc2	vlastnost
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
komerčně	komerčně	k6eAd1	komerčně
dostupných	dostupný	k2eAgFnPc2d1	dostupná
překonávají	překonávat	k5eAaImIp3nP	překonávat
pouze	pouze	k6eAd1	pouze
neodymové	odymový	k2eNgInPc1d1	neodymový
magnety	magnet	k1gInPc1	magnet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pacienty	pacient	k1gMnPc4	pacient
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
nemohou	moct	k5eNaImIp3nP	moct
dovolit	dovolit	k5eAaPmF	dovolit
uhradit	uhradit	k5eAaPmF	uhradit
běžné	běžný	k2eAgFnPc4d1	běžná
dentální	dentální	k2eAgFnPc4d1	dentální
slitiny	slitina	k1gFnPc4	slitina
z	z	k7c2	z
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
se	se	k3xPyFc4	se
jako	jako	k9	jako
levná	levný	k2eAgFnSc1d1	levná
varianta	varianta	k1gFnSc1	varianta
používá	používat	k5eAaImIp3nS	používat
několik	několik	k4yIc4	několik
typů	typ	k1gInPc2	typ
slitin	slitina	k1gFnPc2	slitina
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
,	,	kIx,	,
molybdenu	molybden	k1gInSc2	molybden
<g/>
,	,	kIx,	,
wolframu	wolfram	k1gInSc2	wolfram
a	a	k8xC	a
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
materiál	materiál	k1gInSc1	materiál
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
odolává	odolávat	k5eAaImIp3nS	odolávat
korozním	korozní	k2eAgInPc3d1	korozní
vlivům	vliv	k1gInPc3	vliv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc4d1	vysoká
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc4d1	vysoký
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
kobalt	kobalt	k1gInSc1	kobalt
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
karbidem	karbid	k1gInSc7	karbid
wolframu	wolfram	k1gInSc2	wolfram
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
tvrdokovů	tvrdokov	k1gInPc2	tvrdokov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
třískové	třískový	k2eAgNnSc4d1	třískové
obrábění	obrábění	k1gNnSc4	obrábění
s	s	k7c7	s
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgFnSc7d2	vyšší
produktivitou	produktivita	k1gFnSc7	produktivita
než	než	k8xS	než
rychlořezné	rychlořezný	k2eAgFnSc2d1	rychlořezná
oceli	ocel	k1gFnSc2	ocel
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
i	i	k9	i
ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
součástí	součást	k1gFnPc2	součást
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
musejí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
tvrdost	tvrdost	k1gFnSc4	tvrdost
a	a	k8xC	a
otěruvzdornost	otěruvzdornost	k1gFnSc4	otěruvzdornost
i	i	k9	i
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Odolnost	odolnost	k1gFnSc1	odolnost
kovového	kovový	k2eAgInSc2d1	kovový
kobaltu	kobalt	k1gInSc2	kobalt
proti	proti	k7c3	proti
vlivům	vliv	k1gInPc3	vliv
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
atmosférická	atmosférický	k2eAgFnSc1d1	atmosférická
oxidace	oxidace	k1gFnSc1	oxidace
<g/>
,	,	kIx,	,
působení	působení	k1gNnSc1	působení
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektrolytickým	elektrolytický	k2eAgNnPc3d1	elektrolytické
vyloučením	vyloučení	k1gNnPc3	vyloučení
kobaltového	kobaltový	k2eAgInSc2d1	kobaltový
povlaku	povlak	k1gInSc2	povlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
méně	málo	k6eAd2	málo
ušlechtilého	ušlechtilý	k2eAgInSc2d1	ušlechtilý
kovu	kov	k1gInSc2	kov
jej	on	k3xPp3gMnSc4	on
ochráníme	ochránit	k5eAaPmIp1nP	ochránit
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Soli	sůl	k1gFnPc1	sůl
kobaltnaté	kobaltnatý	k2eAgFnPc1d1	kobaltnatý
i	i	k8xC	i
kobaltité	kobaltitý	k2eAgFnPc1d1	kobaltitý
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgFnPc1d1	barevná
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
modré	modrý	k2eAgFnPc1d1	modrá
nebo	nebo	k8xC	nebo
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Přídavkem	přídavek	k1gInSc7	přídavek
solí	sůl	k1gFnPc2	sůl
kobaltu	kobalt	k1gInSc2	kobalt
do	do	k7c2	do
skloviny	sklovina	k1gFnSc2	sklovina
nebo	nebo	k8xC	nebo
keramické	keramický	k2eAgFnSc2d1	keramická
hmoty	hmota	k1gFnSc2	hmota
se	se	k3xPyFc4	se
docílí	docílit	k5eAaPmIp3nP	docílit
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledný	výsledný	k2eAgInSc1d1	výsledný
výrobek	výrobek	k1gInSc1	výrobek
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
vytavení	vytavení	k1gNnSc6	vytavení
a	a	k8xC	a
vypálení	vypálení	k1gNnSc6	vypálení
trvale	trvale	k6eAd1	trvale
zbarven	zbarvit	k5eAaPmNgInS	zbarvit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
znali	znát	k5eAaImAgMnP	znát
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
již	již	k6eAd1	již
staří	starý	k2eAgMnPc1d1	starý
Egypťané	Egypťan	k1gMnPc1	Egypťan
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
keramických	keramický	k2eAgInPc2d1	keramický
materiálů	materiál	k1gInPc2	materiál
nebo	nebo	k8xC	nebo
porcelánu	porcelán	k1gInSc2	porcelán
se	se	k3xPyFc4	se
však	však	k9	však
spíše	spíše	k9	spíše
používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
glazování	glazování	k1gNnSc1	glazování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
primárně	primárně	k6eAd1	primárně
vypálený	vypálený	k2eAgInSc1d1	vypálený
střep	střep	k1gInSc1	střep
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
tekuté	tekutý	k2eAgFnSc2d1	tekutá
glazury	glazura	k1gFnSc2	glazura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jako	jako	k9	jako
barvicí	barvicí	k2eAgInPc4d1	barvicí
pigmenty	pigment	k1gInPc4	pigment
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
většinou	většina	k1gFnSc7	většina
soli	sůl	k1gFnSc2	sůl
různých	různý	k2eAgInPc2d1	různý
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Opětným	opětný	k2eAgNnSc7d1	opětné
vypálením	vypálení	k1gNnSc7	vypálení
předmětu	předmět	k1gInSc2	předmět
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
se	se	k3xPyFc4	se
glazura	glazura	k1gFnSc1	glazura
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
různých	různý	k2eAgInPc2d1	různý
směsných	směsný	k2eAgInPc2d1	směsný
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
křemičitanů	křemičitan	k1gInPc2	křemičitan
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
solí	sůl	k1gFnPc2	sůl
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
trvale	trvale	k6eAd1	trvale
zbarví	zbarvit	k5eAaPmIp3nP	zbarvit
její	její	k3xOp3gInSc4	její
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
barva	barva	k1gFnSc1	barva
glazovací	glazovací	k2eAgFnSc2d1	glazovací
směsi	směs	k1gFnSc2	směs
před	před	k7c7	před
vypálením	vypálení	k1gNnSc7	vypálení
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
po	po	k7c6	po
konečném	konečný	k2eAgNnSc6d1	konečné
tepelném	tepelný	k2eAgNnSc6d1	tepelné
zpracování	zpracování	k1gNnSc6	zpracování
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
finální	finální	k2eAgInSc4d1	finální
vzhled	vzhled	k1gInSc4	vzhled
výrobku	výrobek	k1gInSc2	výrobek
vždy	vždy	k6eAd1	vždy
otázkou	otázka	k1gFnSc7	otázka
zkušenosti	zkušenost	k1gFnSc2	zkušenost
a	a	k8xC	a
řemeslné	řemeslný	k2eAgFnSc2d1	řemeslná
dovednosti	dovednost	k1gFnSc2	dovednost
keramika	keramika	k1gFnSc1	keramika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
se	se	k3xPyFc4	se
kobalt	kobalt	k1gInSc1	kobalt
dnes	dnes	k6eAd1	dnes
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
jako	jako	k8xS	jako
bělidlo	bělidlo	k1gNnSc1	bělidlo
k	k	k7c3	k
optickému	optický	k2eAgNnSc3d1	optické
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
žlutavého	žlutavý	k2eAgInSc2d1	žlutavý
nádechu	nádech	k1gInSc2	nádech
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
železnaté	železnatý	k2eAgFnPc4d1	železnatá
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Ozářením	ozáření	k1gNnSc7	ozáření
stabilního	stabilní	k2eAgInSc2d1	stabilní
izotopu	izotop	k1gInSc2	izotop
kobaltu	kobalt	k1gInSc2	kobalt
59	[number]	k4	59
<g/>
Co	co	k9	co
energetickými	energetický	k2eAgInPc7d1	energetický
neutrony	neutron	k1gInPc7	neutron
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
jaderném	jaderný	k2eAgInSc6d1	jaderný
reaktoru	reaktor	k1gInSc6	reaktor
<g/>
)	)	kIx)	)
vzniká	vznikat	k5eAaImIp3nS	vznikat
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
60	[number]	k4	60
<g/>
Co	co	k3yInSc4	co
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
radioizotop	radioizotop	k1gInSc1	radioizotop
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
5,271	[number]	k4	5,271
<g/>
4	[number]	k4	4
let	léto	k1gNnPc2	léto
za	za	k7c4	za
uvolňování	uvolňování	k1gNnSc4	uvolňování
silného	silný	k2eAgNnSc2d1	silné
gama-záření	gamaáření	k1gNnSc2	gama-záření
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
někdy	někdy	k6eAd1	někdy
zvažován	zvažovat	k5eAaImNgInS	zvažovat
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
ve	v	k7c6	v
svrchním	svrchní	k2eAgInSc6d1	svrchní
plášti	plášť	k1gInSc6	plášť
jaderné	jaderný	k2eAgFnSc2d1	jaderná
bomby	bomba	k1gFnSc2	bomba
(	(	kIx(	(
<g/>
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
kobaltová	kobaltový	k2eAgFnSc1d1	kobaltová
bomba	bomba	k1gFnSc1	bomba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgFnSc3d1	zmíněná
transmutaci	transmutace	k1gFnSc3	transmutace
a	a	k8xC	a
zamoření	zamoření	k1gNnSc1	zamoření
oblasti	oblast	k1gFnSc2	oblast
radioizotopem	radioizotop	k1gInSc7	radioizotop
60	[number]	k4	60
<g/>
Co	co	k8xS	co
(	(	kIx(	(
<g/>
praktická	praktický	k2eAgFnSc1d1	praktická
aplikace	aplikace	k1gFnSc1	aplikace
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
60	[number]	k4	60
<g/>
Co	co	k9	co
lze	lze	k6eAd1	lze
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
připravit	připravit	k5eAaPmF	připravit
a	a	k8xC	a
manipulace	manipulace	k1gFnPc4	manipulace
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
není	být	k5eNaImIp3nS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
gama-paprsků	gamaaprsek	k1gMnPc2	gama-paprsek
pro	pro	k7c4	pro
ozařování	ozařování	k1gNnSc4	ozařování
rakovinných	rakovinný	k2eAgInPc2d1	rakovinný
nádorů	nádor	k1gInPc2	nádor
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
tkání	tkáň	k1gFnPc2	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
pro	pro	k7c4	pro
ozařování	ozařování	k1gNnSc4	ozařování
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kobaltové	kobaltový	k2eAgNnSc4d1	kobaltové
dělo	dělo	k1gNnSc4	dělo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
podstatou	podstata	k1gFnSc7	podstata
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
olověná	olověný	k2eAgFnSc1d1	olověná
ochranná	ochranný	k2eAgFnSc1d1	ochranná
schránka	schránka	k1gFnSc1	schránka
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
propouští	propouštět	k5eAaImIp3nS	propouštět
potřebné	potřebný	k2eAgNnSc4d1	potřebné
gama-záření	gamaáření	k1gNnSc4	gama-záření
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
paprsku	paprsek	k1gInSc2	paprsek
určeným	určený	k2eAgInSc7d1	určený
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Izotop	izotop	k1gInSc1	izotop
60	[number]	k4	60
<g/>
Co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
i	i	k9	i
v	v	k7c6	v
defektoskopii	defektoskopie	k1gFnSc6	defektoskopie
pro	pro	k7c4	pro
vyhledávání	vyhledávání	k1gNnSc4	vyhledávání
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
skrytých	skrytý	k2eAgFnPc2d1	skrytá
vad	vada	k1gFnPc2	vada
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněným	uvolněný	k2eAgNnSc7d1	uvolněné
zářením	záření	k1gNnSc7	záření
jsou	být	k5eAaImIp3nP	být
prozařovány	prozařován	k2eAgFnPc4d1	prozařován
kovové	kovový	k2eAgFnPc4d1	kovová
součásti	součást	k1gFnPc4	součást
důležitých	důležitý	k2eAgFnPc2d1	důležitá
aparatur	aparatura	k1gFnPc2	aparatura
-	-	kIx~	-
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgInPc1d1	chemický
reaktory	reaktor	k1gInPc1	reaktor
pro	pro	k7c4	pro
vysoké	vysoký	k2eAgInPc4d1	vysoký
tlaky	tlak	k1gInPc4	tlak
<g/>
,	,	kIx,	,
části	část	k1gFnPc4	část
kosmických	kosmický	k2eAgFnPc2d1	kosmická
raket	raketa	k1gFnPc2	raketa
apod.	apod.	kA	apod.
Citlivý	citlivý	k2eAgInSc1d1	citlivý
detektor	detektor	k1gInSc1	detektor
snímá	snímat	k5eAaImIp3nS	snímat
množství	množství	k1gNnSc1	množství
gama	gama	k1gNnSc2	gama
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
materiálem	materiál	k1gInSc7	materiál
projdou	projít	k5eAaPmIp3nP	projít
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
vady	vada	k1gFnSc2	vada
(	(	kIx(	(
<g/>
trhliny	trhlina	k1gFnSc2	trhlina
<g/>
,	,	kIx,	,
chybného	chybný	k2eAgInSc2d1	chybný
svaru	svar	k1gInSc2	svar
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
změnou	změna	k1gFnSc7	změna
intenzity	intenzita	k1gFnSc2	intenzita
měřeného	měřený	k2eAgNnSc2d1	měřené
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Ozařování	ozařování	k1gNnSc1	ozařování
gama-paprsky	gamaaprsky	k6eAd1	gama-paprsky
slouží	sloužit	k5eAaImIp3nS	sloužit
i	i	k9	i
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
hub	houba	k1gFnPc2	houba
<g/>
,	,	kIx,	,
plísní	plíseň	k1gFnPc2	plíseň
a	a	k8xC	a
dřevokazného	dřevokazný	k2eAgInSc2d1	dřevokazný
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
historicky	historicky	k6eAd1	historicky
cenných	cenný	k2eAgInPc6d1	cenný
dřevěných	dřevěný	k2eAgInPc6d1	dřevěný
předmětech	předmět	k1gInPc6	předmět
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
ošetřit	ošetřit	k5eAaPmF	ošetřit
klasickými	klasický	k2eAgInPc7d1	klasický
chemickými	chemický	k2eAgInPc7d1	chemický
přípravky	přípravek	k1gInPc7	přípravek
kvůli	kvůli	k7c3	kvůli
zachování	zachování	k1gNnSc3	zachování
jejich	jejich	k3xOp3gInSc2	jejich
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
dokáže	dokázat	k5eAaPmIp3nS	dokázat
tvořit	tvořit	k5eAaImF	tvořit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
v	v	k7c6	v
oxidačních	oxidační	k2eAgInPc6d1	oxidační
stavech	stav	k1gInPc6	stav
od	od	k7c2	od
Co	co	k9	co
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
do	do	k7c2	do
Co	co	k9	co
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
většina	většina	k1gFnSc1	většina
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
sloučenin	sloučenina	k1gFnPc2	sloučenina
kobaltu	kobalt	k1gInSc2	kobalt
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
dvojmocného	dvojmocný	k2eAgInSc2d1	dvojmocný
kobaltu	kobalt	k1gInSc2	kobalt
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
trojmocného	trojmocný	k2eAgInSc2d1	trojmocný
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organokovových	organokovový	k2eAgFnPc6d1	organokovová
sloučeninách	sloučenina	k1gFnPc6	sloučenina
může	moct	k5eAaImIp3nS	moct
kobalt	kobalt	k1gInSc1	kobalt
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
i	i	k9	i
nižších	nízký	k2eAgNnPc2d2	nižší
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anorganických	anorganický	k2eAgFnPc6d1	anorganická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
kobalt	kobalt	k1gInSc4	kobalt
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
oxidačních	oxidační	k2eAgInPc6d1	oxidační
stavech	stav	k1gInPc6	stav
od	od	k7c2	od
Co	co	k9	co
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
do	do	k7c2	do
Co	co	k9	co
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
největší	veliký	k2eAgInSc4d3	veliký
význam	význam	k1gInSc4	význam
mají	mít	k5eAaImIp3nP	mít
sloučeniny	sloučenina	k1gFnPc1	sloučenina
kobaltnaté	kobaltnatý	k2eAgFnPc1d1	kobaltnatý
a	a	k8xC	a
kobaltité	kobaltitý	k2eAgFnPc1d1	kobaltitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
oxidačních	oxidační	k2eAgInPc6d1	oxidační
stavech	stav	k1gInPc6	stav
jsou	být	k5eAaImIp3nP	být
sloučeniny	sloučenina	k1gFnPc1	sloučenina
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nemají	mít	k5eNaImIp3nP	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
kobaltnaté	kobaltnatý	k2eAgFnPc1d1	kobaltnatý
soli	sůl	k1gFnPc1	sůl
a	a	k8xC	a
komplexní	komplexní	k2eAgInPc1d1	komplexní
tvoří	tvořit	k5eAaImIp3nP	tvořit
více	hodně	k6eAd2	hodně
kobaltité	kobaltitý	k2eAgFnPc1d1	kobaltitý
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
CoO	CoO	k1gFnSc4	CoO
je	být	k5eAaImIp3nS	být
olivově	olivově	k6eAd1	olivově
zelený	zelený	k2eAgInSc1d1	zelený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
na	na	k7c4	na
kobaltnaté	kobaltnatý	k2eAgFnPc4d1	kobaltnatý
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
oxidu	oxid	k1gInSc2	oxid
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
nejčastěji	často	k6eAd3	často
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
také	také	k9	také
připravit	připravit	k5eAaPmF	připravit
žlutý	žlutý	k2eAgInSc4d1	žlutý
<g/>
,	,	kIx,	,
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
,	,	kIx,	,
hnědý	hnědý	k2eAgInSc4d1	hnědý
<g/>
,	,	kIx,	,
slabě	slabě	k6eAd1	slabě
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
namodralý	namodralý	k2eAgInSc1d1	namodralý
nebo	nebo	k8xC	nebo
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
vzniká	vznikat	k5eAaImIp3nS	vznikat
zahříváním	zahřívání	k1gNnSc7	zahřívání
hydroxidu	hydroxid	k1gInSc2	hydroxid
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
nebo	nebo	k8xC	nebo
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
za	za	k7c2	za
nepřístupu	nepřístup	k1gInSc2	nepřístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
teplot	teplota	k1gFnPc2	teplota
má	mít	k5eAaImIp3nS	mít
antiferromagnetické	antiferromagnetický	k2eAgFnPc4d1	antiferromagnetický
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Žíháním	žíhání	k1gNnSc7	žíhání
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
400-500	[number]	k4	400-500
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
oxid	oxid	k1gInSc1	oxid
kobaltnato-kobaltitý	kobaltnatoobaltitý	k2eAgInSc1d1	kobaltnato-kobaltitý
Co	co	k9	co
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
zprvu	zprvu	k6eAd1	zprvu
modrá	modrý	k2eAgFnSc1d1	modrá
sraženina	sraženina	k1gFnSc1	sraženina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	s	k7c7	s
stáním	stání	k1gNnSc7	stání
zbarvuje	zbarvovat	k5eAaImIp3nS	zbarvovat
do	do	k7c2	do
světle	světle	k6eAd1	světle
růžové	růžový	k2eAgFnSc2d1	růžová
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
na	na	k7c4	na
kobaltnaté	kobaltnatý	k2eAgFnPc4d1	kobaltnatý
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
se	se	k3xPyFc4	se
sraženina	sraženina	k1gFnSc1	sraženina
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
na	na	k7c4	na
hydratovaný	hydratovaný	k2eAgInSc4d1	hydratovaný
oxid	oxid	k1gInSc4	oxid
kobaltitý	kobaltitý	k2eAgInSc4d1	kobaltitý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
získat	získat	k5eAaPmF	získat
až	až	k9	až
hydratovaný	hydratovaný	k2eAgInSc4d1	hydratovaný
oxid	oxid	k1gInSc4	oxid
kobaltičitý	kobaltičitý	k2eAgInSc4d1	kobaltičitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
kobaltnatý	kobaltnatý	k2eAgMnSc1d1	kobaltnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
přidáním	přidání	k1gNnSc7	přidání
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
k	k	k7c3	k
roztoku	roztok	k1gInSc3	roztok
kobaltnaté	kobaltnatý	k2eAgFnSc2d1	kobaltnatý
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
CoS	cos	kA	cos
je	být	k5eAaImIp3nS	být
černý	černý	k2eAgInSc1d1	černý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
hydroxidech	hydroxid	k1gInPc6	hydroxid
ani	ani	k8xC	ani
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
v	v	k7c6	v
koncentrovaných	koncentrovaný	k2eAgFnPc6d1	koncentrovaná
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
sulfanu	sulfan	k1gInSc2	sulfan
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
kobaltnatých	kobaltnatý	k2eAgMnPc2d1	kobaltnatý
solí	solit	k5eAaImIp3nP	solit
alkalickým	alkalický	k2eAgInSc7d1	alkalický
sulfidem	sulfid	k1gInSc7	sulfid
nebo	nebo	k8xC	nebo
sulfanem	sulfan	k1gInSc7	sulfan
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoCl	CoCl	k1gInSc4	CoCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
bledě	bledě	k6eAd1	bledě
modrý	modrý	k2eAgInSc1d1	modrý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
hygroskopický	hygroskopický	k2eAgMnSc1d1	hygroskopický
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
postupnou	postupný	k2eAgFnSc7d1	postupná
hydratací	hydratace	k1gFnPc2	hydratace
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
modrofialový	modrofialový	k2eAgInSc4d1	modrofialový
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
fialový	fialový	k2eAgInSc4d1	fialový
<g/>
,	,	kIx,	,
růžově	růžově	k6eAd1	růžově
fialový	fialový	k2eAgInSc4d1	fialový
<g/>
,	,	kIx,	,
broskvově	broskvově	k6eAd1	broskvově
fialový	fialový	k2eAgInSc1d1	fialový
až	až	k9	až
k	k	k7c3	k
růžovému	růžový	k2eAgInSc3d1	růžový
hexahydrátu	hexahydrát	k1gInSc3	hexahydrát
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
barevných	barevný	k2eAgInPc2d1	barevný
pochodů	pochod	k1gInPc2	pochod
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
v	v	k7c6	v
silikagelu	silikagel	k1gInSc6	silikagel
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
jako	jako	k9	jako
indikátor	indikátor	k1gInSc4	indikátor
množství	množství	k1gNnSc2	množství
obsažené	obsažený	k2eAgFnSc2d1	obsažená
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
lihu	líh	k1gInSc6	líh
<g/>
,	,	kIx,	,
acetonu	aceton	k1gInSc6	aceton
<g/>
,	,	kIx,	,
chinolinu	chinolin	k1gInSc6	chinolin
a	a	k8xC	a
benzonitrilu	benzonitril	k1gInSc6	benzonitril
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c4	v
roztoku	roztoka	k1gFnSc4	roztoka
podvojné	podvojný	k2eAgFnPc1d1	podvojná
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
chlorokobaltnatany	chlorokobaltnatan	k1gInPc1	chlorokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
kobaltnatý	kobaltnatý	k2eAgMnSc1d1	kobaltnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
spalováním	spalování	k1gNnSc7	spalování
kobaltu	kobalt	k1gInSc2	kobalt
v	v	k7c6	v
proudu	proud	k1gInSc6	proud
chloru	chlor	k1gInSc2	chlor
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoBr	CoBr	k1gInSc4	CoBr
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
zelená	zelený	k2eAgFnSc1d1	zelená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
tvoří	tvořit	k5eAaImIp3nP	tvořit
červenou	červený	k2eAgFnSc4d1	červená
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
látku	látka	k1gFnSc4	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustnou	rozpustný	k2eAgFnSc7d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
bromokobaltnatany	bromokobaltnatan	k1gInPc1	bromokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
kobaltnatý	kobaltnatý	k2eAgMnSc1d1	kobaltnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
spalováním	spalování	k1gNnSc7	spalování
kobaltu	kobalt	k1gInSc2	kobalt
v	v	k7c6	v
parách	para	k1gFnPc6	para
bromu	brom	k1gInSc2	brom
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoI	CoI	k1gFnSc7	CoI
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
zelenošedý	zelenošedý	k2eAgInSc4d1	zelenošedý
prášek	prášek	k1gInSc4	prášek
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tmavě	tmavě	k6eAd1	tmavě
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
lihu	líh	k1gInSc6	líh
a	a	k8xC	a
acetonu	aceton	k1gInSc6	aceton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
jodokobaltnatany	jodokobaltnatan	k1gInPc1	jodokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc1	jodid
kobaltnatý	kobaltnatý	k2eAgMnSc1d1	kobaltnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
spalováním	spalování	k1gNnSc7	spalování
kobaltu	kobalt	k1gInSc2	kobalt
v	v	k7c6	v
parách	para	k1gFnPc6	para
jodu	jod	k1gInSc2	jod
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoF	CoF	k1gFnSc7	CoF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
načervenalá	načervenalý	k2eAgFnSc1d1	načervenalá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
růžová	růžový	k2eAgFnSc1d1	růžová
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
fluorokobaltnatany	fluorokobaltnatan	k1gInPc1	fluorokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
amoniaku	amoniak	k1gInSc6	amoniak
a	a	k8xC	a
uhličitanu	uhličitan	k1gInSc6	uhličitan
amonném	amonný	k2eAgInSc6d1	amonný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
komplexní	komplexní	k2eAgFnSc2d1	komplexní
sloučeniny	sloučenina	k1gFnSc2	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
kyanokobaltnatany	kyanokobaltnatan	k1gInPc1	kyanokobaltnatan
<g/>
,	,	kIx,	,
za	za	k7c4	za
vyšší	vysoký	k2eAgFnPc4d2	vyšší
teploty	teplota	k1gFnPc4	teplota
však	však	k9	však
samovolně	samovolně	k6eAd1	samovolně
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
kyanokobaltitany	kyanokobaltitan	k1gInPc4	kyanokobaltitan
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
kobaltnatý	kobaltnatý	k2eAgMnSc1d1	kobaltnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
kobaltnaté	kobaltnatý	k2eAgFnSc2d1	kobaltnatý
soli	sůl	k1gFnSc2	sůl
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
kyanidem	kyanid	k1gInSc7	kyanid
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
SCN	SCN	kA	SCN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
červenofialová	červenofialový	k2eAgFnSc1d1	červenofialová
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
velké	velký	k2eAgFnSc6d1	velká
koncentraci	koncentrace	k1gFnSc6	koncentrace
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
rozpuštěné	rozpuštěný	k2eAgFnSc2d1	rozpuštěná
soli	sůl	k1gFnSc2	sůl
tmavě	tmavě	k6eAd1	tmavě
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
při	při	k7c6	při
zřeďování	zřeďování	k1gNnSc6	zřeďování
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
červené	červená	k1gFnSc2	červená
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
v	v	k7c4	v
roztoku	roztoka	k1gFnSc4	roztoka
komplexní	komplexní	k2eAgFnSc2d1	komplexní
sloučeniny	sloučenina	k1gFnSc2	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
thiokyanatokobaltnatany	thiokyanatokobaltnatan	k1gInPc1	thiokyanatokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
Thiokyanatan	Thiokyanatan	k1gInSc1	Thiokyanatan
kobaltnatý	kobaltnatý	k2eAgMnSc1d1	kobaltnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
rhodanovodíkové	rhodanovodíková	k1gFnSc2	rhodanovodíková
nebo	nebo	k8xC	nebo
reakcí	reakce	k1gFnSc7	reakce
rhodanidu	rhodanid	k1gInSc2	rhodanid
barnatého	barnatý	k2eAgInSc2d1	barnatý
se	s	k7c7	s
síranem	síran	k1gInSc7	síran
kobaltnatým	kobaltnatý	k2eAgInSc7d1	kobaltnatý
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
analytická	analytický	k2eAgFnSc1d1	analytická
chemii	chemie	k1gFnSc4	chemie
a	a	k8xC	a
v	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc4	oxid
dusičitý	dusičitý	k2eAgInSc4d1	dusičitý
a	a	k8xC	a
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc4	síran
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoSO	CoSO	k1gFnSc7	CoSO
<g/>
4	[number]	k4	4
se	se	k3xPyFc4	se
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svého	svůj	k3xOyFgInSc2	svůj
heptahydrátu	heptahydrát	k1gInSc2	heptahydrát
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
kobaltnatá	kobaltnatý	k2eAgFnSc1d1	kobaltnatý
skalice	skalice	k1gFnSc1	skalice
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
karmínově	karmínově	k6eAd1	karmínově
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
podvojné	podvojný	k2eAgInPc4d1	podvojný
sírany	síran	k1gInPc4	síran
se	s	k7c7	s
sírany	síran	k1gInPc7	síran
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
nebo	nebo	k8xC	nebo
oxidu	oxid	k1gInSc2	oxid
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
v	v	k7c6	v
zředěné	zředěný	k2eAgFnSc6d1	zředěná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoCO	CoCO	k1gFnSc7	CoCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
podobě	podoba	k1gFnSc6	podoba
fialovočervený	fialovočervený	k2eAgInSc1d1	fialovočervený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nP	vytvářet
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
klasickém	klasický	k2eAgNnSc6d1	klasické
srážení	srážení	k1gNnSc6	srážení
kobaltnaté	kobaltnatý	k2eAgFnSc2d1	kobaltnatý
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
uhličitanu	uhličitan	k1gInSc2	uhličitan
vznikají	vznikat	k5eAaImIp3nP	vznikat
zásadité	zásaditý	k2eAgInPc1d1	zásaditý
uhličitany	uhličitan	k1gInPc1	uhličitan
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
však	však	k9	však
kobaltnatá	kobaltnatý	k2eAgFnSc1d1	kobaltnatý
sůl	sůl	k1gFnSc1	sůl
sráží	srážet	k5eAaImIp3nS	srážet
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
nasyceném	nasycený	k2eAgInSc6d1	nasycený
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
hexahydrát	hexahydrát	k1gInSc1	hexahydrát
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
Co	co	k9	co
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
hnědý	hnědý	k2eAgInSc1d1	hnědý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
na	na	k7c4	na
kobaltnaté	kobaltnatý	k2eAgFnPc4d1	kobaltnatý
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
vroucím	vroucí	k2eAgInSc6d1	vroucí
alkalickém	alkalický	k2eAgInSc6d1	alkalický
hydroxidu	hydroxid	k1gInSc6	hydroxid
na	na	k7c4	na
tetrahydroxokobaltnatany	tetrahydroxokobaltnatan	k1gInPc4	tetrahydroxokobaltnatan
nebo	nebo	k8xC	nebo
hexahydroxokobaltnatany	hexahydroxokobaltnatan	k1gInPc4	hexahydroxokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
stainierit	stainierit	k1gInSc1	stainierit
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
oxidací	oxidace	k1gFnSc7	oxidace
hydroxidu	hydroxid	k1gInSc2	hydroxid
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Žíhání	žíhání	k1gNnSc1	žíhání
na	na	k7c4	na
bezvodý	bezvodý	k2eAgInSc4d1	bezvodý
oxid	oxid	k1gInSc4	oxid
kobaltitý	kobaltitý	k2eAgInSc4d1	kobaltitý
lze	lze	k6eAd1	lze
provést	provést	k5eAaPmF	provést
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
zvláštních	zvláštní	k2eAgFnPc2d1	zvláštní
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
totiž	totiž	k9	totiž
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
k	k	k7c3	k
odštěpení	odštěpení	k1gNnSc3	odštěpení
kyslíku	kyslík	k1gInSc2	kyslík
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
kobaltnato-kobaltitého	kobaltnatoobaltitý	k2eAgInSc2d1	kobaltnato-kobaltitý
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
kobaltitý	kobaltitý	k2eAgInSc4d1	kobaltitý
CoF	CoF	k1gFnSc7	CoF
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bezvodém	bezvodý	k2eAgInSc6d1	bezvodý
stavu	stav	k1gInSc6	stav
hnědý	hnědý	k2eAgInSc1d1	hnědý
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
v	v	k7c6	v
hydratované	hydratovaný	k2eAgFnSc6d1	hydratovaná
podobě	podoba	k1gFnSc6	podoba
chromově	chromově	k6eAd1	chromově
zelený	zelený	k2eAgInSc1d1	zelený
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
fluorokobaltitany	fluorokobaltitan	k1gInPc1	fluorokobaltitan
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
nebo	nebo	k8xC	nebo
přímou	přímý	k2eAgFnSc7d1	přímá
fluorací	fluorace	k1gFnSc7	fluorace
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
Co	co	k9	co
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
ihned	ihned	k6eAd1	ihned
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnPc1d1	sírová
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nP	rozpouštět
bez	bez	k7c2	bez
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c4	v
roztoku	roztoka	k1gFnSc4	roztoka
se	s	k7c7	s
sírany	síran	k1gInPc7	síran
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
podvojné	podvojný	k2eAgInPc4d1	podvojný
sírany	síran	k1gInPc4	síran
-	-	kIx~	-
kamence	kamenec	k1gInPc4	kamenec
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
kobaltitý	kobaltitý	k2eAgMnSc1d1	kobaltitý
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
připravit	připravit	k5eAaPmF	připravit
elektrolýzou	elektrolýza	k1gFnSc7	elektrolýza
koncentrovaného	koncentrovaný	k2eAgInSc2d1	koncentrovaný
roztoku	roztok	k1gInSc2	roztok
síranu	síran	k1gInSc2	síran
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
okyseleného	okyselený	k2eAgInSc2d1	okyselený
kyselinou	kyselina	k1gFnSc7	kyselina
sírovou	sírový	k2eAgFnSc7d1	sírová
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
diafragmy	diafragma	k1gFnSc2	diafragma
a	a	k8xC	a
chlazené	chlazený	k2eAgFnSc2d1	chlazená
anody	anoda	k1gFnSc2	anoda
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
oxidační	oxidační	k2eAgNnSc1d1	oxidační
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Co	co	k9	co
<g/>
5	[number]	k4	5
<g/>
+	+	kIx~	+
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
sloučeniny	sloučenina	k1gFnPc4	sloučenina
kobaltu	kobalt	k1gInSc2	kobalt
typické	typický	k2eAgFnSc2d1	typická
a	a	k8xC	a
kobalt	kobalt	k1gInSc1	kobalt
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc4	oxid
kobaltičitý	kobaltičitý	k2eAgInSc4d1	kobaltičitý
CoO	CoO	k1gFnSc7	CoO
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
schopnost	schopnost	k1gFnSc4	schopnost
tvořit	tvořit	k5eAaImF	tvořit
soli	sůl	k1gFnPc4	sůl
se	s	k7c7	s
zásadotvornými	zásadotvorný	k2eAgInPc7d1	zásadotvorný
oxidy	oxid	k1gInPc7	oxid
-	-	kIx~	-
kobaltičitany	kobaltičitan	k1gInPc7	kobaltičitan
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
také	také	k9	také
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
oxid	oxid	k1gInSc1	oxid
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Doposud	doposud	k6eAd1	doposud
se	se	k3xPyFc4	se
podařili	podařit	k5eAaPmAgMnP	podařit
připravit	připravit	k5eAaPmF	připravit
Sr	Sr	k1gFnPc4	Sr
<g/>
2	[number]	k4	2
<g/>
CoO	CoO	k1gFnSc2	CoO
<g/>
4	[number]	k4	4
a	a	k8xC	a
Ba	ba	k9	ba
<g/>
2	[number]	k4	2
<g/>
CoO	CoO	k1gFnPc7	CoO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
nemají	mít	k5eNaImIp3nP	mít
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
oxidu	oxid	k1gInSc3	oxid
a	a	k8xC	a
chloridu	chlorid	k1gInSc3	chlorid
alkalického	alkalický	k2eAgInSc2d1	alkalický
kovu	kov	k1gInSc2	kov
nebo	nebo	k8xC	nebo
kovu	kov	k1gInSc2	kov
alkalické	alkalický	k2eAgFnSc2d1	alkalická
zeminy	zemina	k1gFnSc2	zemina
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
kobaltitým	kobaltitý	k2eAgInSc7d1	kobaltitý
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
koordinačních	koordinační	k2eAgFnPc2d1	koordinační
sloučenin	sloučenina	k1gFnPc2	sloučenina
tvoří	tvořit	k5eAaImIp3nS	tvořit
kobalt	kobalt	k1gInSc1	kobalt
v	v	k7c6	v
oxidačních	oxidační	k2eAgNnPc6d1	oxidační
číslech	číslo	k1gNnPc6	číslo
II	II	kA	II
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
neochotně	ochotně	k6eNd1	ochotně
tvoří	tvořit	k5eAaImIp3nP	tvořit
oxoaniony	oxoanion	k1gInPc1	oxoanion
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
CoVO	CoVO	k1gFnSc7	CoVO
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
a	a	k8xC	a
CoIIO	CoIIO	k1gFnSc1	CoIIO
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
kobaltu	kobalt	k1gInSc2	kobalt
s	s	k7c7	s
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
číslem	číslo	k1gNnSc7	číslo
IV	IV	kA	IV
je	být	k5eAaImIp3nS	být
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
některé	některý	k3yIgFnPc1	některý
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
fluorem	fluor	k1gInSc7	fluor
a	a	k8xC	a
smíšené	smíšený	k2eAgInPc1d1	smíšený
oxidy	oxid	k1gInPc1	oxid
kovu	kov	k1gInSc2	kov
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
málo	málo	k6eAd1	málo
čisté	čistý	k2eAgInPc1d1	čistý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
trojmocného	trojmocný	k2eAgInSc2d1	trojmocný
kobaltu	kobalt	k1gInSc2	kobalt
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
nízkospinové	nízkospinový	k2eAgFnSc2d1	nízkospinový
<g/>
,	,	kIx,	,
oktaedrické	oktaedrický	k2eAgFnSc2d1	oktaedrický
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
stabilní	stabilní	k2eAgInPc1d1	stabilní
(	(	kIx(	(
<g/>
LFSE	LFSE	kA	LFSE
=	=	kIx~	=
2,4	[number]	k4	2,4
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nestálost	nestálost	k1gFnSc1	nestálost
hexaaquakobaltitého	hexaaquakobaltitý	k2eAgInSc2d1	hexaaquakobaltitý
kationtu	kation	k1gInSc2	kation
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
jeho	jeho	k3xOp3gNnSc7	jeho
oxidačními	oxidační	k2eAgFnPc7d1	oxidační
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
roztok	roztok	k1gInSc4	roztok
kyselý	kyselý	k2eAgInSc4d1	kyselý
(	(	kIx(	(
<g/>
pH	ph	k0wR	ph
<	<	kIx(	<
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
dokonce	dokonce	k9	dokonce
vodu	voda	k1gFnSc4	voda
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
solí	sůl	k1gFnPc2	sůl
<g/>
:	:	kIx,	:
modrý	modrý	k2eAgInSc1d1	modrý
oktadekahydrát	oktadekahydrát	k1gInSc1	oktadekahydrát
síranu	síran	k1gInSc2	síran
kobaltitého	kobaltitý	k2eAgInSc2d1	kobaltitý
Co	co	k9	co
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
•	•	k?	•
<g/>
18	[number]	k4	18
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
nebo	nebo	k8xC	nebo
podvojné	podvojný	k2eAgFnSc2d1	podvojná
soli	sůl	k1gFnSc2	sůl
MCo	MCo	k1gMnSc1	MCo
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
•	•	k?	•
<g/>
12	[number]	k4	12
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
(	(	kIx(	(
<g/>
M	M	kA	M
=	=	kIx~	=
K	K	kA	K
<g/>
,	,	kIx,	,
Rb	Rb	k1gFnSc1	Rb
<g/>
,	,	kIx,	,
Cs	Cs	k1gFnSc1	Cs
a	a	k8xC	a
NH	NH	kA	NH
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysokospinové	Vysokospinový	k2eAgInPc1d1	Vysokospinový
kobaltité	kobaltitý	k2eAgInPc1d1	kobaltitý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
fluorokobaltitany	fluorokobaltitan	k1gInPc1	fluorokobaltitan
[	[	kIx(	[
<g/>
CoF	CoF	k1gFnSc1	CoF
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
a	a	k8xC	a
častěji	často	k6eAd2	často
I	i	k9	i
<g/>
[	[	kIx(	[
<g/>
CoF	CoF	k1gFnSc1	CoF
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
CoIII	CoIII	k?	CoIII
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
mnoho	mnoho	k4c1	mnoho
komplexních	komplexní	k2eAgFnPc2d1	komplexní
koordinačních	koordinační	k2eAgFnPc2d1	koordinační
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zvláště	zvláště	k6eAd1	zvláště
s	s	k7c7	s
dusíkatými	dusíkatý	k2eAgInPc7d1	dusíkatý
donorovými	donorův	k2eAgInPc7d1	donorův
ligandy	ligand	k1gInPc7	ligand
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
amoniakáty	amoniakáta	k1gFnPc1	amoniakáta
kobaltité	kobaltitý	k2eAgFnSc2d1	kobaltitý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
X	X	kA	X
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
X	X	kA	X
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
vynikají	vynikat	k5eAaImIp3nP	vynikat
pestrou	pestrý	k2eAgFnSc7d1	pestrá
barevností	barevnost	k1gFnSc7	barevnost
a	a	k8xC	a
dobrou	dobrý	k2eAgFnSc7d1	dobrá
stálostí	stálost	k1gFnSc7	stálost
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
i	i	k8xC	i
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
izomerií	izomerie	k1gFnPc2	izomerie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgInPc3d1	známý
amoniakátům	amoniakát	k1gInPc3	amoniakát
patří	patřit	k5eAaImIp3nS	patřit
amonikáty	amonikát	k1gInPc4	amonikát
chloridu	chlorid	k1gInSc2	chlorid
kobaltitého	kobaltitý	k2eAgInSc2d1	kobaltitý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
patří	patřit	k5eAaImIp3nS	patřit
oranžově	oranžově	k6eAd1	oranžově
žlutý	žlutý	k2eAgInSc1d1	žlutý
chlorid	chlorid	k1gInSc1	chlorid
hexaamminkobaltitý	hexaamminkobaltitý	k2eAgInSc1d1	hexaamminkobaltitý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
3	[number]	k4	3
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
luteochlorid	luteochlorid	k1gInSc1	luteochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
<g/>
,	,	kIx,	,
červený	červený	k2eAgInSc1d1	červený
chlorid	chlorid	k1gInSc1	chlorid
pentaammin-aquakobaltitý	pentaamminquakobaltitý	k2eAgInSc1d1	pentaammin-aquakobaltitý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gMnSc1	Cl
<g/>
3	[number]	k4	3
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
roseochlorid	roseochlorid	k1gInSc1	roseochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
<g/>
,	,	kIx,	,
purpurový	purpurový	k2eAgInSc1d1	purpurový
chlorid	chlorid	k1gInSc1	chlorid
chloro-pentaamminkobaltitý	chloroentaamminkobaltitý	k2eAgInSc1d1	chloro-pentaamminkobaltitý
[	[	kIx(	[
<g/>
CoCl	CoCl	k1gInSc1	CoCl
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
purpureochlorid	purpureochlorid	k1gInSc1	purpureochlorid
<g />
.	.	kIx.	.
</s>
<s>
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
dichloro-tetraamminkobaltitý	dichloroetraamminkobaltitý	k2eAgInSc1d1	dichloro-tetraamminkobaltitý
[	[	kIx(	[
<g/>
CoCl	CoCl	k1gInSc1	CoCl
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
izomerních	izomerní	k2eAgFnPc6d1	izomerní
formách	forma	k1gFnPc6	forma
cis	cis	k1gNnSc2	cis
a	a	k8xC	a
trans	trans	k1gInSc1	trans
<g/>
,	,	kIx,	,
v	v	k7c6	v
cis	cis	k1gNnSc6	cis
formě	forma	k1gFnSc3	forma
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
modrofialová	modrofialový	k2eAgFnSc1d1	modrofialová
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
violeochlorid	violeochlorid	k1gInSc1	violeochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
a	a	k8xC	a
v	v	k7c4	v
trans	trans	k1gInSc4	trans
formě	forma	k1gFnSc6	forma
zelená	zelený	k2eAgNnPc1d1	zelené
a	a	k8xC	a
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
praseochlorid	praseochlorid	k1gInSc1	praseochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
známé	známý	k2eAgInPc1d1	známý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
žlutě	žlutě	k6eAd1	žlutě
zbarvený	zbarvený	k2eAgInSc4d1	zbarvený
chlorid	chlorid	k1gInSc4	chlorid
pentaamin-nitrátokobaltitý	pentaaminitrátokobaltitý	k2eAgInSc4d1	pentaamin-nitrátokobaltitý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
2	[number]	k4	2
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
xanthochlorid	xanthochlorid	k1gInSc1	xanthochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
<g/>
,	,	kIx,	,
chlorid	chlorid	k1gInSc1	chlorid
tetraammin-dinitrokobaltitý	tetraammininitrokobaltitý	k2eAgInSc1d1	tetraammin-dinitrokobaltitý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gFnSc1	Cl
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
izomerních	izomerní	k2eAgFnPc6d1	izomerní
formách	forma	k1gFnPc6	forma
cis	cis	k1gNnSc2	cis
a	a	k8xC	a
trans	trans	k1gInSc1	trans
<g/>
,	,	kIx,	,
v	v	k7c6	v
cis	cis	k1gNnSc6	cis
formě	forma	k1gFnSc3	forma
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
flavochlorid	flavochlorid	k1gInSc1	flavochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
a	a	k8xC	a
v	v	k7c4	v
trans	trans	k1gInSc4	trans
formě	forma	k1gFnSc6	forma
šafránově	šafránově	k6eAd1	šafránově
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
a	a	k8xC	a
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xS	jako
croceochlorid	croceochlorid	k1gInSc1	croceochlorid
kobaltitý	kobaltitý	k2eAgInSc1d1	kobaltitý
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
staré	starý	k2eAgNnSc1d1	staré
triviální	triviální	k2eAgNnSc1d1	triviální
názvosloví	názvosloví	k1gNnSc1	názvosloví
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
základem	základ	k1gInSc7	základ
prvního	první	k4xOgNnSc2	první
názvosloví	názvosloví	k1gNnSc2	názvosloví
komplexů	komplex	k1gInPc2	komplex
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
přeneslo	přenést	k5eAaPmAgNnS	přenést
i	i	k9	i
na	na	k7c4	na
sloučeniny	sloučenina	k1gFnPc4	sloučenina
jiné	jiný	k2eAgNnSc4d1	jiné
než	než	k8xS	než
chloridů	chlorid	k1gInPc2	chlorid
a	a	k8xC	a
kobaltitého	kobaltitý	k2eAgInSc2d1	kobaltitý
kationu	kation	k1gInSc2	kation
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
barvy	barva	k1gFnPc1	barva
komplexů	komplex	k1gInPc2	komplex
jiných	jiný	k2eAgInPc2d1	jiný
kationů	kation	k1gInPc2	kation
kovů	kov	k1gInPc2	kov
neodpovídali	odpovídat	k5eNaImAgMnP	odpovídat
-	-	kIx~	-
např.	např.	kA	např.
[	[	kIx(	[
<g/>
Cr	cr	k0	cr
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
Br	br	k0	br
<g/>
3	[number]	k4	3
se	se	k3xPyFc4	se
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
roseobromid	roseobromid	k1gInSc1	roseobromid
chromitý	chromitý	k2eAgInSc1d1	chromitý
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
sloučenina	sloučenina	k1gFnSc1	sloučenina
není	být	k5eNaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bílá	bílý	k2eAgFnSc1d1	bílá
až	až	k9	až
světle	světle	k6eAd1	světle
oranžová	oranžový	k2eAgFnSc1d1	oranžová
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
stabilními	stabilní	k2eAgInPc7d1	stabilní
dusíkatými	dusíkatý	k2eAgInPc7d1	dusíkatý
komplexy	komplex	k1gInPc7	komplex
jsou	být	k5eAaImIp3nP	být
hexanitrokobaltitany	hexanitrokobaltitan	k1gInPc4	hexanitrokobaltitan
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
mají	mít	k5eAaImIp3nP	mít
žluté	žlutý	k2eAgFnPc1d1	žlutá
až	až	k8xS	až
oranžové	oranžový	k2eAgNnSc1d1	oranžové
zbarvení	zbarvení	k1gNnSc1	zbarvení
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
málo	málo	k1gNnSc4	málo
rozpustné	rozpustný	k2eAgNnSc4d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Oranžový	oranžový	k2eAgInSc1d1	oranžový
hexanitrokobaltitan	hexanitrokobaltitan	k1gInSc1	hexanitrokobaltitan
sodný	sodný	k2eAgInSc1d1	sodný
Na	na	k7c6	na
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
smísením	smísení	k1gNnSc7	smísení
roztoků	roztok	k1gInPc2	roztok
dusičnanu	dusičnan	k1gInSc2	dusičnan
kobaltitého	kobaltitý	k2eAgInSc2d1	kobaltitý
<g/>
,	,	kIx,	,
dusitanu	dusitan	k1gInSc2	dusitan
sodného	sodný	k2eAgInSc2d1	sodný
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
octové	octový	k2eAgFnSc2d1	octová
za	za	k7c4	za
provádění	provádění	k1gNnSc4	provádění
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Sodná	sodný	k2eAgFnSc1d1	sodná
sůl	sůl	k1gFnSc1	sůl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
zkoumadlo	zkoumadlo	k1gNnSc4	zkoumadlo
na	na	k7c4	na
draselné	draselný	k2eAgFnPc4d1	draselná
soli	sůl	k1gFnPc4	sůl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hexanitrokobaltitan	hexanitrokobaltitan	k1gInSc1	hexanitrokobaltitan
draselný	draselný	k2eAgInSc1d1	draselný
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Fischerova	Fischerův	k2eAgFnSc1d1	Fischerova
sůl	sůl	k1gFnSc1	sůl
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
fluoru	fluor	k1gInSc2	fluor
vzniká	vznikat	k5eAaImIp3nS	vznikat
vysokospinový	vysokospinový	k2eAgInSc1d1	vysokospinový
paramagnetický	paramagnetický	k2eAgInSc1d1	paramagnetický
hexafluorokobaltitan	hexafluorokobaltitan	k1gInSc1	hexafluorokobaltitan
draselný	draselný	k2eAgInSc1d1	draselný
K	k	k7c3	k
<g/>
3	[number]	k4	3
<g/>
[	[	kIx(	[
<g/>
CoF	CoF	k1gFnSc1	CoF
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Stálé	stálý	k2eAgInPc1d1	stálý
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
hexakyanokobaltitany	hexakyanokobaltitan	k1gInPc1	hexakyanokobaltitan
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
žluté	žlutý	k2eAgFnPc1d1	žlutá
krystalické	krystalický	k2eAgFnPc1d1	krystalická
netoxické	toxický	k2eNgFnPc1d1	netoxická
látky	látka	k1gFnPc1	látka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
oxidací	oxidace	k1gFnSc7	oxidace
kyanokobaltnatanů	kyanokobaltnatan	k1gInPc2	kyanokobaltnatan
<g/>
.	.	kIx.	.
</s>
<s>
Volnou	volný	k2eAgFnSc4d1	volná
kyselinu	kyselina	k1gFnSc4	kyselina
kyanokobaltitou	kyanokobaltita	k1gFnSc7	kyanokobaltita
získáme	získat	k5eAaPmIp1nP	získat
působením	působení	k1gNnSc7	působení
sulfanu	sulfan	k1gInSc2	sulfan
na	na	k7c4	na
sraženinu	sraženina	k1gFnSc4	sraženina
kyanokobaltitanu	kyanokobaltitan	k1gInSc2	kyanokobaltitan
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
<g/>
.	.	kIx.	.
</s>
<s>
Komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
kyslíkovými	kyslíkový	k2eAgInPc7d1	kyslíkový
donory	donor	k1gInPc7	donor
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
stálé	stálý	k2eAgFnPc1d1	stálá
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
zelené	zelená	k1gFnPc4	zelená
penta-	penta-	k?	penta-
<g/>
2,4	[number]	k4	2,4
<g/>
-dionátové	ionátový	k2eAgFnSc6d1	-dionátový
(	(	kIx(	(
<g/>
acetylacetonátové	acetylacetonátový	k2eAgFnSc6d1	acetylacetonátový
<g/>
)	)	kIx)	)
komplexy	komplex	k1gInPc1	komplex
[	[	kIx(	[
<g/>
Co	co	k3yRnSc1	co
<g/>
(	(	kIx(	(
<g/>
acac	acac	k6eAd1	acac
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
a	a	k8xC	a
oxalatokobaltitany	oxalatokobaltitan	k1gInPc1	oxalatokobaltitan
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
připravují	připravovat	k5eAaImIp3nP	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydratovaného	hydratovaný	k2eAgInSc2d1	hydratovaný
oxidu	oxid	k1gInSc2	oxid
kobaltitého	kobaltitý	k2eAgInSc2d1	kobaltitý
v	v	k7c6	v
koncentrovaném	koncentrovaný	k2eAgInSc6d1	koncentrovaný
roztoku	roztok	k1gInSc6	roztok
kyseliny	kyselina	k1gFnSc2	kyselina
šťavelové	šťavelový	k2eAgFnSc2d1	šťavelová
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
sražením	sražení	k1gNnSc7	sražení
alkalickým	alkalický	k2eAgInSc7d1	alkalický
uhličitanem	uhličitan	k1gInSc7	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
významnějším	významný	k2eAgInPc3d2	významnější
komplexům	komplex	k1gInPc3	komplex
patří	patřit	k5eAaImIp3nS	patřit
Durantova	Durantův	k2eAgFnSc1d1	Durantova
sůl	sůl	k1gFnSc1	sůl
K	k	k7c3	k
<g/>
2	[number]	k4	2
<g/>
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kobaltité	Kobaltitý	k2eAgInPc1d1	Kobaltitý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
odolné	odolný	k2eAgFnPc1d1	odolná
vůči	vůči	k7c3	vůči
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přece	přece	k9	přece
jenom	jenom	k9	jenom
dojde	dojít	k5eAaPmIp3nS	dojít
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
vícejaderné	vícejaderný	k2eAgInPc1d1	vícejaderný
komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
můstkovými	můstkový	k2eAgInPc7d1	můstkový
ligandy	ligand	k1gInPc7	ligand
OH-	OH-	k1gFnSc2	OH-
<g/>
,	,	kIx,	,
NH	NH	kA	NH
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
NH2-	NH2-	k1gFnSc4	NH2-
a	a	k8xC	a
NO	no	k9	no
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
CoII	CoII	k?	CoII
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
známých	známý	k2eAgInPc2d1	známý
aniontů	anion	k1gInPc2	anion
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
krystalizují	krystalizovat	k5eAaImIp3nP	krystalizovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hydrátů	hydrát	k1gInPc2	hydrát
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
růžový	růžový	k2eAgInSc4d1	růžový
kationt	kationt	k1gInSc4	kationt
hexaaquakolbaltnatý	hexaaquakolbaltnatý	k2eAgInSc4d1	hexaaquakolbaltnatý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
přidá	přidat	k5eAaPmIp3nS	přidat
alkalická	alkalický	k2eAgFnSc1d1	alkalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
hydroxid	hydroxid	k1gInSc1	hydroxid
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
s	s	k7c7	s
amfoterním	amfoterní	k2eAgInSc7d1	amfoterní
charakterem	charakter	k1gInSc7	charakter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
konc	konc	k1gFnSc1	konc
<g/>
.	.	kIx.	.
hydroxidů	hydroxid	k1gInPc2	hydroxid
vznikají	vznikat	k5eAaImIp3nP	vznikat
tmavě	tmavě	k6eAd1	tmavě
modré	modrý	k2eAgInPc1d1	modrý
roztoky	roztok	k1gInPc1	roztok
tetrahydroxokobaltnatanů	tetrahydroxokobaltnatan	k1gInPc2	tetrahydroxokobaltnatan
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
<s>
Pomalým	pomalý	k2eAgNnSc7d1	pomalé
srážením	srážení	k1gNnSc7	srážení
z	z	k7c2	z
roztoku	roztok	k1gInSc2	roztok
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
0	[number]	k4	0
̊	̊	k?	̊
<g/>
C	C	kA	C
vzniká	vznikat	k5eAaImIp3nS	vznikat
nestálá	stálý	k2eNgFnSc1d1	nestálá
modrá	modrý	k2eAgFnSc1d1	modrá
forma	forma	k1gFnSc1	forma
hydroxidu	hydroxid	k1gInSc2	hydroxid
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
<g/>
.	.	kIx.	.
</s>
<s>
Zahříváním	zahřívání	k1gNnSc7	zahřívání
za	za	k7c2	za
nepřístupu	nepřístup	k1gInSc2	nepřístup
vzduchu	vzduch	k1gInSc2	vzduch
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
.	.	kIx.	.
</s>
<s>
Kobaltnaté	kobaltnatý	k2eAgInPc1d1	kobaltnatý
komplexy	komplex	k1gInPc1	komplex
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
kobaltitým	kobaltitý	k2eAgFnPc3d1	kobaltitý
méně	málo	k6eAd2	málo
početné	početný	k2eAgMnPc4d1	početný
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
stabilní	stabilní	k2eAgFnPc1d1	stabilní
a	a	k8xC	a
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
oxidaci	oxidace	k1gFnSc3	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnější	běžný	k2eAgInPc1d3	nejběžnější
jsou	být	k5eAaImIp3nP	být
vysokospinové	vysokospinový	k2eAgInPc4d1	vysokospinový
oktaedrické	oktaedrický	k2eAgInPc4d1	oktaedrický
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
komplex	komplex	k1gInSc4	komplex
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
)	)	kIx)	)
<g/>
edta	edta	k1gMnSc1	edta
<g/>
]	]	kIx)	]
<g/>
-	-	kIx~	-
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
koordinační	koordinační	k2eAgNnSc1d1	koordinační
číslo	číslo	k1gNnSc1	číslo
6	[number]	k4	6
a	a	k8xC	a
chelaton	chelaton	k1gInSc1	chelaton
III	III	kA	III
(	(	kIx(	(
<g/>
edta	edta	k1gMnSc1	edta
<g/>
,	,	kIx,	,
kyselina	kyselina	k1gFnSc1	kyselina
ethylendiamintetraoctová	ethylendiamintetraoctová	k1gFnSc1	ethylendiamintetraoctová
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
jeden	jeden	k4xCgInSc1	jeden
kyslíkový	kyslíkový	k2eAgInSc1d1	kyslíkový
atom	atom	k1gInSc1	atom
nekoordinovaný	koordinovaný	k2eNgInSc1d1	nekoordinovaný
(	(	kIx(	(
<g/>
chová	chovat	k5eAaImIp3nS	chovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
pětivazný	pětivazný	k2eAgInSc1d1	pětivazný
ligand	ligand	k1gInSc1	ligand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
stálost	stálost	k1gFnSc1	stálost
vůči	vůči	k7c3	vůči
oxidaci	oxidace	k1gFnSc3	oxidace
mají	mít	k5eAaImIp3nP	mít
kationtové	kationtový	k2eAgInPc1d1	kationtový
ligandy	ligand	k1gInPc1	ligand
[	[	kIx(	[
<g/>
Co	co	k3yRnSc4	co
<g/>
(	(	kIx(	(
<g/>
L-L	L-L	k1gMnSc1	L-L
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
dvoudonorové	dvoudonorová	k1gFnPc1	dvoudonorová
dusíkaté	dusíkatý	k2eAgFnSc2d1	dusíkatá
ligandy	liganda	k1gFnSc2	liganda
jako	jako	k8xS	jako
en	en	k?	en
(	(	kIx(	(
<g/>
ethylendiamin	ethylendiamin	k1gInSc1	ethylendiamin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bipy	bipy	k1gInPc1	bipy
(	(	kIx(	(
<g/>
bipyridyl	bipyridyl	k1gInSc1	bipyridyl
<g/>
)	)	kIx)	)
a	a	k8xC	a
phen	phen	k1gInSc1	phen
(	(	kIx(	(
<g/>
fenanthrolin	fenanthrolin	k1gInSc1	fenanthrolin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
modré	modrý	k2eAgInPc4d1	modrý
tetraedrické	tetraedrický	k2eAgInPc4d1	tetraedrický
komplexy	komplex	k1gInPc4	komplex
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
při	při	k7c6	při
konfiguraci	konfigurace	k1gFnSc6	konfigurace
d	d	k?	d
<g/>
7	[number]	k4	7
jsou	být	k5eAaImIp3nP	být
výhodné	výhodný	k2eAgInPc1d1	výhodný
(	(	kIx(	(
<g/>
LFSE	LFSE	kA	LFSE
=	=	kIx~	=
6	[number]	k4	6
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
aniont	aniont	k1gInSc4	aniont
[	[	kIx(	[
<g/>
CoX	CoX	k1gFnSc1	CoX
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
=	=	kIx~	=
F	F	kA	F
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
<g/>
,	,	kIx,	,
Br	br	k0	br
<g/>
,	,	kIx,	,
I	i	k9	i
<g/>
,	,	kIx,	,
SCN	SCN	kA	SCN
a	a	k8xC	a
OH	OH	kA	OH
<g/>
.	.	kIx.	.
</s>
<s>
Chloro-	Chloro-	k?	Chloro-
<g/>
,	,	kIx,	,
bromo-	bromo-	k?	bromo-
a	a	k8xC	a
jodokobaltnatany	jodokobaltnatan	k1gInPc1	jodokobaltnatan
existují	existovat	k5eAaImIp3nP	existovat
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
v	v	k7c6	v
oktaedrickém	oktaedrický	k2eAgNnSc6d1	oktaedrický
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Oktaedrické	oktaedrický	k2eAgInPc1d1	oktaedrický
kyanokobaltnatany	kyanokobaltnatan	k1gInPc1	kyanokobaltnatan
jsou	být	k5eAaImIp3nP	být
stálé	stálý	k2eAgMnPc4d1	stálý
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
vodíku	vodík	k1gInSc2	vodík
za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Amoniakáty	Amoniakáta	k1gFnPc1	Amoniakáta
kobaltnaté	kobaltnatý	k2eAgFnPc1d1	kobaltnatý
jsou	být	k5eAaImIp3nP	být
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
komplexně	komplexně	k6eAd1	komplexně
vázané	vázaný	k2eAgFnPc1d1	vázaná
molekuly	molekula	k1gFnPc1	molekula
vody	voda	k1gFnSc2	voda
nahradily	nahradit	k5eAaPmAgFnP	nahradit
molekulami	molekula	k1gFnPc7	molekula
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
červené	červená	k1gFnSc6	červená
nebo	nebo	k8xC	nebo
o	o	k7c6	o
světle	světlo	k1gNnSc6	světlo
červené	červený	k2eAgFnSc2d1	červená
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
chlorid	chlorid	k1gInSc1	chlorid
hexaamminkobaltnatý	hexaamminkobaltnatý	k2eAgInSc1d1	hexaamminkobaltnatý
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
Cl	Cl	k1gFnSc2	Cl
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Karboxyláty	Karboxylát	k1gInPc1	Karboxylát
CoII	CoII	k1gFnSc2	CoII
<g/>
,	,	kIx,	,
např.	např.	kA	např.
červený	červený	k2eAgInSc1d1	červený
octan	octan	k1gInSc1	octan
[	[	kIx(	[
<g/>
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
OOCCH	OOCCH	kA	OOCCH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
•	•	k?	•
<g/>
4	[number]	k4	4
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
analogických	analogický	k2eAgFnPc2d1	analogická
sloučenin	sloučenina	k1gFnPc2	sloučenina
Rh	Rh	k1gFnSc1	Rh
monomerní	monomernit	k5eAaPmIp3nS	monomernit
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
acetátový	acetátový	k2eAgInSc4d1	acetátový
ligand	ligand	k1gInSc4	ligand
jednodorový	jednodorový	k2eAgInSc4d1	jednodorový
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
chemii	chemie	k1gFnSc6	chemie
při	při	k7c6	při
oxidacích	oxidace	k1gFnPc6	oxidace
<g/>
,	,	kIx,	,
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
také	také	k9	také
schnutí	schnutí	k1gNnSc1	schnutí
laků	lak	k1gInPc2	lak
a	a	k8xC	a
fermeže	fermež	k1gFnSc2	fermež
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
organickým	organický	k2eAgFnPc3d1	organická
sloučeninám	sloučenina	k1gFnPc3	sloučenina
kobaltu	kobalt	k1gInSc2	kobalt
patří	patřit	k5eAaImIp3nP	patřit
soli	sůl	k1gFnPc1	sůl
organických	organický	k2eAgFnPc2d1	organická
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
zejména	zejména	k9	zejména
organokovové	organokovový	k2eAgFnPc1d1	organokovová
sloučeniny	sloučenina	k1gFnPc1	sloučenina
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
kobaltnatý	kobaltnatý	k2eAgInSc4d1	kobaltnatý
CoC	CoC	k1gFnSc7	CoC
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
růžový	růžový	k2eAgInSc1d1	růžový
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozpustné	rozpustný	k2eAgFnPc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
srážením	srážení	k1gNnSc7	srážení
roztoku	roztok	k1gInSc2	roztok
kobaltnaté	kobaltnatý	k2eAgFnSc2d1	kobaltnatý
soli	sůl	k1gFnSc2	sůl
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
šťavelanem	šťavelan	k1gInSc7	šťavelan
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
kobaltnatý	kobaltnatý	k2eAgInSc1d1	kobaltnatý
Co	co	k9	co
<g/>
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
3	[number]	k4	3
<g/>
COO	COO	kA	COO
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
bělící	bělící	k2eAgInSc1d1	bělící
prostředek	prostředek	k1gInSc1	prostředek
a	a	k8xC	a
jako	jako	k8xC	jako
sušidlo	sušidlo	k1gNnSc1	sušidlo
do	do	k7c2	do
laků	lak	k1gInPc2	lak
a	a	k8xC	a
fermeží	fermež	k1gFnPc2	fermež
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
uhličitanu	uhličitan	k1gInSc2	uhličitan
kobaltnatého	kobaltnatý	k2eAgInSc2d1	kobaltnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgFnSc6d1	octová
<g/>
.	.	kIx.	.
</s>
<s>
Stopové	stopový	k2eAgNnSc1d1	stopové
množství	množství	k1gNnSc1	množství
kobaltu	kobalt	k1gInSc2	kobalt
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
několika	několik	k4yIc2	několik
desetin	desetina	k1gFnPc2	desetina
miligramů	miligram	k1gInPc2	miligram
kobaltu	kobalt	k1gInSc2	kobalt
na	na	k7c4	na
kilogram	kilogram	k1gInSc4	kilogram
půdy	půda	k1gFnSc2	půda
prokazatelně	prokazatelně	k6eAd1	prokazatelně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
pasoucího	pasoucí	k2eAgMnSc2d1	pasoucí
se	se	k3xPyFc4	se
dobytka	dobytek	k1gInSc2	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
členů	člen	k1gInPc2	člen
vitaminů	vitamin	k1gInPc2	vitamin
skupiny	skupina	k1gFnSc2	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
vitaminu	vitamin	k1gInSc2	vitamin
B	B	kA	B
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
kobaltem	kobalt	k1gInSc7	kobalt
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
organizmu	organizmus	k1gInSc2	organizmus
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Kobalt	kobalt	k1gInSc1	kobalt
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
organizmu	organizmus	k1gInSc2	organizmus
dostat	dostat	k5eAaPmF	dostat
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
vdechováním	vdechování	k1gNnSc7	vdechování
prachu	prach	k1gInSc2	prach
či	či	k8xC	či
aerosolu	aerosol	k1gInSc2	aerosol
nebo	nebo	k8xC	nebo
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
projeví	projevit	k5eAaPmIp3nS	projevit
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
několika	několik	k4yIc6	několik
týdnech	týden	k1gInPc6	týden
nebo	nebo	k8xC	nebo
i	i	k8xC	i
měsících	měsíc	k1gInPc6	měsíc
trvalé	trvalý	k2eAgFnSc2d1	trvalá
expozice	expozice	k1gFnSc2	expozice
vysokými	vysoký	k2eAgFnPc7d1	vysoká
dávkami	dávka	k1gFnPc7	dávka
kobaltu	kobalt	k1gInSc2	kobalt
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
vyskytnou	vyskytnout	k5eAaPmIp3nP	vyskytnout
prakticky	prakticky	k6eAd1	prakticky
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
průmyslových	průmyslový	k2eAgInPc6d1	průmyslový
provozech	provoz	k1gInPc6	provoz
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
kobaltu	kobalt	k1gInSc2	kobalt
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gFnPc2	jeho
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobé	dlouhodobý	k2eAgFnSc6d1	dlouhodobá
expozici	expozice	k1gFnSc6	expozice
prachem	prach	k1gInSc7	prach
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
podobné	podobný	k2eAgFnSc2d1	podobná
astmatu	astma	k1gNnSc3	astma
nebo	nebo	k8xC	nebo
trvalejší	trvalý	k2eAgInPc4d2	trvalejší
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
plicní	plicní	k2eAgFnSc1d1	plicní
fibróza	fibróza	k1gFnSc1	fibróza
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgInSc1d1	stálý
kontakt	kontakt	k1gInSc1	kontakt
s	s	k7c7	s
pokožkou	pokožka	k1gFnSc7	pokožka
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
podráždění	podráždění	k1gNnSc4	podráždění
a	a	k8xC	a
kožní	kožní	k2eAgFnPc4d1	kožní
vyrážky	vyrážka	k1gFnPc4	vyrážka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
mizí	mizet	k5eAaImIp3nS	mizet
<g/>
.	.	kIx.	.
</s>
<s>
Potravní	potravní	k2eAgInSc1d1	potravní
příjem	příjem	k1gInSc1	příjem
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
kobaltu	kobalt	k1gInSc2	kobalt
najednou	najednou	k6eAd1	najednou
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nebude	být	k5eNaImBp3nS	být
příliš	příliš	k6eAd1	příliš
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
nevolnost	nevolnost	k1gFnSc4	nevolnost
a	a	k8xC	a
zvracení	zvracení	k1gNnSc4	zvracení
<g/>
,	,	kIx,	,
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
příjmu	příjem	k1gInSc6	příjem
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
známy	znám	k2eAgInPc1d1	znám
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
štítnou	štítný	k2eAgFnSc7d1	štítná
žlázou	žláza	k1gFnSc7	žláza
<g/>
,	,	kIx,	,
neurologické	neurologický	k2eAgInPc4d1	neurologický
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
krevní	krevní	k2eAgFnSc2d1	krevní
hustoty	hustota	k1gFnSc2	hustota
<g/>
.	.	kIx.	.
</s>
