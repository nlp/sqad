<s>
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiota	k1gFnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Rodné	rodný	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Adrien	Adriena	k1gFnPc2
Rabiot-Provost	Rabiot-Provost	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1995	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Saint-Maurice	Saint-Maurika	k1gFnSc3
Povolání	povolání	k1gNnSc2
</s>
<s>
fotbalista	fotbalista	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
francouzská	francouzský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
ligový	ligový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
italská	italský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
Italský	italský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
Juventus	Juventus	k1gInSc1
FC	FC	kA
</s>
<s>
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiota	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1995	#num#	k4
<g/>
,	,	kIx,
Saint-Maurice	Saint-Maurika	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
francouzský	francouzský	k2eAgMnSc1d1
reprezentační	reprezentační	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
působící	působící	k2eAgMnSc1d1
v	v	k7c6
italském	italský	k2eAgInSc6d1
klubu	klub	k1gInSc6
Juventus	Juventus	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
přestoupil	přestoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Většinu	většina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
debutoval	debutovat	k5eAaBmAgMnS
v	v	k7c6
ligovém	ligový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Bordeaux	Bordeaux	k1gNnSc3
(	(	kIx(
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
získal	získat	k5eAaPmAgMnS
s	s	k7c7
ním	on	k3xPp3gInSc7
15	#num#	k4
trofejí	trofej	k1gFnPc2
včetně	včetně	k7c2
čtyř	čtyři	k4xCgFnPc2
titulů	titul	k1gInPc2
v	v	k7c6
Ligue	Ligue	k1gNnPc6
1	#num#	k4
a	a	k8xC
a	a	k8xC
treble	treble	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rabiot	Rabiot	k1gInSc1
odehrál	odehrát	k5eAaPmAgInS
53	#num#	k4
zápasů	zápas	k1gInPc2
v	v	k7c6
mládežnických	mládežnický	k2eAgInPc6d1
reprezentačních	reprezentační	k2eAgInPc6d1
výběrech	výběr	k1gInPc6
Francie	Francie	k1gFnSc2
a	a	k8xC
v	v	k7c6
seniorském	seniorský	k2eAgInSc6d1
týmu	tým	k1gInSc6
debutoval	debutovat	k5eAaBmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
v	v	k7c6
přátelském	přátelský	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Pobřeží	pobřeží	k1gNnSc3
slonoviny	slonovina	k1gFnSc2
(	(	kIx(
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Ligue	Ligue	k1gInSc1
1	#num#	k4
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Salvatore	Salvator	k1gMnSc5
Sirigu	Sirig	k1gMnSc5
•	•	k?
Nicolas	Nicolas	k1gMnSc1
Douchez	Douchez	k1gMnSc1
•	•	k?
Alphonse	Alphonse	k1gFnSc1
Areola	Areola	k1gFnSc1
•	•	k?
Ronan	Ronan	k1gMnSc1
Le	Le	k1gMnSc1
Crom	Crom	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Maxwell	maxwell	k1gInSc1
•	•	k?
Mamadou	Mamada	k1gFnSc7
Sakho	Sak	k1gMnSc2
•	•	k?
Christophe	Christophe	k1gInSc1
Jallet	Jallet	k1gInSc1
•	•	k?
Alex	Alex	k1gMnSc1
•	•	k?
Gregory	Gregor	k1gMnPc4
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Wiel	Wiel	k1gInSc1
•	•	k?
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
•	•	k?
Sylvain	Sylvain	k1gInSc1
Armand	Armand	k1gInSc1
•	•	k?
Zoumana	Zouman	k1gMnSc2
Camara	Camar	k1gMnSc2
•	•	k?
Siaka	Siaka	k1gMnSc1
Tiéné	Tiéná	k1gFnSc2
•	•	k?
Antoine	Antoin	k1gMnSc5
Conte	Cont	k1gMnSc5
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Javier	Javier	k1gInSc1
Pastore	pastor	k1gMnSc5
•	•	k?
Clément	Clément	k1gInSc1
Chantôme	Chantôm	k1gInSc5
•	•	k?
Marco	Marco	k1gNnSc4
Verratti	Verratť	k1gFnSc2
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
David	David	k1gMnSc1
Beckham	Beckham	k1gInSc1
•	•	k?
Lucas	Lucas	k1gInSc1
Moura	Moura	k?
•	•	k?
Kingsley	Kingslea	k1gFnSc2
Coman	Coman	k1gMnSc1
•	•	k?
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc1
•	•	k?
Jérémy	Jérém	k1gInPc1
Ménez	Ménez	k1gInSc1
•	•	k?
Ezequiel	Ezequiel	k1gInSc4
Lavezzi	Lavezze	k1gFnSc4
•	•	k?
Kévin	Kévin	k1gMnSc1
Gameiro	Gameiro	k1gNnSc1
•	•	k?
Mathieu	Mathieus	k1gInSc2
Bodmer	Bodmer	k1gMnSc1
•	•	k?
Mohamed	Mohamed	k1gMnSc1
Sissoko	Sissoko	k1gNnSc1
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Nenê	Nenê	k1gMnSc1
•	•	k?
Guillaume	Guillaum	k1gInSc5
Hoarau	Hoaraus	k1gInSc6
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Carlo	Carlo	k1gNnSc1
Ancelotti	Ancelotť	k1gFnSc2
</s>
<s>
Ligue	Ligue	k6eAd1
1	#num#	k4
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Salvatore	Salvator	k1gMnSc5
Sirigu	Sirig	k1gMnSc5
•	•	k?
Nicolas	Nicolas	k1gMnSc1
Douchez	Douchez	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Alex	Alex	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Silva	Silva	k1gFnSc1
•	•	k?
Gregory	Gregor	k1gMnPc4
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Wiel	Wiel	k1gInSc1
•	•	k?
Maxwell	maxwell	k1gInSc1
•	•	k?
Marquinhos	Marquinhos	k1gInSc1
•	•	k?
Lucas	Lucas	k1gInSc1
Digne	Dign	k1gInSc5
•	•	k?
Christophe	Christophe	k1gNnPc2
Jallet	Jallet	k1gInSc1
•	•	k?
Zoumana	Zouman	k1gMnSc2
Camara	Camar	k1gMnSc2
•	•	k?
Lucas	Lucas	k1gMnSc1
Moura	Moura	k?
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Thiago	Thiago	k6eAd1
Motta	motto	k1gNnSc2
•	•	k?
Javier	Javier	k1gMnSc1
Pastore	pastor	k1gMnSc5
•	•	k?
Marco	Marco	k1gMnSc1
Verratti	Verratti	k1gNnSc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Yohan	Yohan	k1gMnSc1
Cabaye	Cabay	k1gFnSc2
•	•	k?
Kingsley	Kingslea	k1gFnSc2
Coman	Coman	k1gMnSc1
•	•	k?
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gMnSc1
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Lavezzi	Lavezh	k1gMnPc1
•	•	k?
Edinson	Edinson	k1gMnSc1
Cavani	Cavan	k1gMnPc1
•	•	k?
Jérémy	Jérém	k1gInPc1
Ménez	Ménez	k1gInSc1
•	•	k?
Hervin	Hervina	k1gFnPc2
Ongenda	Ongend	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Laurent	Laurent	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Ligue	Ligue	k6eAd1
1	#num#	k4
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Salvatore	Salvator	k1gMnSc5
Sirigu	Sirig	k1gMnSc5
•	•	k?
Nicolas	Nicolas	k1gMnSc1
Douchez	Douchez	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
David	David	k1gMnSc1
Luiz	Luiz	k1gMnSc1
•	•	k?
Maxwell	maxwell	k1gInSc1
•	•	k?
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
•	•	k?
Marquinhos	Marquinhos	k1gInSc1
•	•	k?
Gregory	Gregor	k1gMnPc4
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Wiel	Wiel	k1gInSc1
•	•	k?
Lucas	Lucas	k1gInSc1
Digne	Dign	k1gInSc5
•	•	k?
Serge	Serge	k1gNnPc2
Aurier	Auriero	k1gNnPc2
•	•	k?
Zoumana	Zouman	k1gMnSc2
Camara	Camar	k1gMnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Javier	Javier	k1gInSc1
Pastore	pastor	k1gMnSc5
•	•	k?
Marco	Marco	k1gNnSc4
Verratti	Verratť	k1gFnSc2
•	•	k?
Lucas	Lucas	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Yohan	Yohany	k1gInPc2
Cabaye	Cabay	k1gInSc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Edinson	Edinson	k1gMnSc1
Cavani	Cavan	k1gMnPc1
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Lavezzi	Lavezh	k1gMnPc1
•	•	k?
Zlatan	Zlatan	k1gInSc4
Ibrahimović	Ibrahimović	k1gMnSc2
•	•	k?
Jean-Christophe	Jean-Christoph	k1gFnSc2
Bahebeck	Bahebeck	k1gMnSc1
Clément	Clément	k1gMnSc1
Chantôme	Chantôm	k1gInSc5
Hlavní	hlavní	k2eAgFnSc7d1
trenér	trenér	k1gMnSc1
</s>
<s>
Laurent	Laurent	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Ligue	Ligue	k6eAd1
1	#num#	k4
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Kevin	Kevin	k1gMnSc1
Trapp	Trapp	k1gMnSc1
•	•	k?
Salvatore	Salvator	k1gMnSc5
Sirigu	Sirig	k1gMnSc5
•	•	k?
Nicolas	Nicolas	k1gMnSc1
Douchez	Douchez	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Thiago	Thiago	k6eAd1
Silva	Silva	k1gFnSc1
•	•	k?
Marquinhos	Marquinhos	k1gInSc1
•	•	k?
Maxwell	maxwell	k1gInSc1
•	•	k?
David	David	k1gMnSc1
Luiz	Luiz	k1gMnSc1
•	•	k?
Serge	Serge	k1gNnPc2
Aurier	Auriero	k1gNnPc2
•	•	k?
Gregory	Gregor	k1gMnPc4
van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Wiel	Wiel	k1gInSc1
•	•	k?
Layvin	Layvina	k1gFnPc2
Kurzawa	Kurzawa	k1gMnSc1
•	•	k?
Presnel	Presnel	k1gMnSc1
Kimpembe	Kimpemb	k1gInSc5
•	•	k?
Kévin	Kévina	k1gFnPc2
Rimane	Riman	k1gMnSc5
•	•	k?
Lucas	Lucas	k1gMnSc1
Moura	Moura	k?
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Stambouli	Stambouli	k1gFnSc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Verratti	Verratť	k1gFnSc2
•	•	k?
Javier	Javier	k1gMnSc1
Pastore	pastor	k1gMnSc5
•	•	k?
Christopher	Christophra	k1gFnPc2
Nkunku	Nkunek	k1gInSc2
•	•	k?
Edinson	Edinson	k1gMnSc1
Cavani	Cavan	k1gMnPc1
•	•	k?
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gFnSc1
•	•	k?
Jean-Kévin	Jean-Kévina	k1gFnPc2
Augustin	Augustin	k1gMnSc1
•	•	k?
Hervin	Hervina	k1gFnPc2
Ongenda	Ongenda	k1gFnSc1
•	•	k?
Timothée	Timothé	k1gFnSc2
Taufflieb	Tauffliba	k1gFnPc2
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Lavezzi	Lavezze	k1gFnSc3
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Laurent	Laurent	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Ligue	Ligue	k6eAd1
1	#num#	k4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Alphonse	Alphonse	k1gFnSc1
Areola	Areola	k1gFnSc1
•	•	k?
Kevin	Kevin	k1gMnSc1
Trapp	Trapp	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Presnel	Presnelit	k5eAaPmRp2nS
Kimpembe	Kimpemb	k1gMnSc5
•	•	k?
Marquinhos	Marquinhos	k1gMnSc1
•	•	k?
Daniel	Daniel	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Silva	Silva	k1gFnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Meunier	Meunier	k1gMnSc1
•	•	k?
Yuri	Yur	k1gFnSc2
Berchiche	Berchiche	k1gNnSc2
•	•	k?
Layvin	Layvina	k1gFnPc2
Kurzawa	Kurzawa	k1gMnSc1
•	•	k?
Kévin	Kévin	k1gMnSc1
Rimane	Riman	k1gMnSc5
•	•	k?
Giovani	Giovan	k1gMnPc1
Lo	Lo	k1gFnSc2
Celso	Celsa	k1gFnSc5
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Julian	Julian	k1gMnSc1
Draxler	Draxler	k1gMnSc1
•	•	k?
Javier	Javier	k1gMnSc1
Pastore	pastor	k1gMnSc5
•	•	k?
Marco	Marco	k1gMnSc1
Verratti	Verratti	k1gNnSc2
•	•	k?
Christopher	Christophra	k1gFnPc2
Nkunku	Nkunek	k1gInSc2
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Lassana	Lassan	k1gMnSc2
Diarra	Diarr	k1gMnSc2
•	•	k?
Yacine	Yacin	k1gMnSc5
Adli	Adl	k1gMnSc5
•	•	k?
Stanley	Stanlea	k1gFnPc4
N	N	kA
<g/>
’	’	k?
<g/>
Soki	Sok	k1gFnSc2
•	•	k?
Edinson	Edinson	k1gMnSc1
Cavani	Cavan	k1gMnPc1
•	•	k?
Kylian	Kyliany	k1gInPc2
Mbappé	Mbappý	k2eAgFnSc2d1
•	•	k?
Neymar	Neymar	k1gMnSc1
•	•	k?
Timothy	Timotha	k1gFnSc2
Weah	Weah	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Moura	Moura	k?
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Gonçalo	Gonçala	k1gFnSc5
Guedes	Guedes	k1gInSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Unai	Unai	k1gNnSc1
Emery	Emera	k1gFnSc2
</s>
<s>
Ligue	Ligue	k6eAd1
1	#num#	k4
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Alphonse	Alphonse	k1gFnSc1
Areola	Areola	k1gFnSc1
•	•	k?
Gianluigi	Gianluigi	k1gNnPc2
Buffon	Buffon	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Daniel	Daniel	k1gMnSc1
Alves	Alves	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Bernat	Bernat	k1gMnSc1
•	•	k?
Colin	Colin	k1gMnSc1
Dagba	Dagb	k1gMnSc2
•	•	k?
Thilo	Thilo	k1gNnSc1
Kehrer	Kehrer	k1gMnSc1
•	•	k?
Presnel	Presnel	k1gMnSc1
Kimpembe	Kimpemb	k1gInSc5
•	•	k?
Layvin	Layvina	k1gFnPc2
Kurzawa	Kurzawa	k1gMnSc1
•	•	k?
Marquinhos	Marquinhos	k1gMnSc1
•	•	k?
Loï	Loï	k1gInSc1
Mbe	Mbe	k1gMnSc1
Soh	Soh	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Meunier	Meunier	k1gMnSc1
•	•	k?
Stanley	Stanlea	k1gFnSc2
N	N	kA
<g/>
’	’	k?
<g/>
Soki	Sok	k1gFnSc2
•	•	k?
Kévin	Kévin	k1gMnSc1
Rimane	Riman	k1gMnSc5
•	•	k?
Thiago	Thiago	k1gNnSc4
Silva	Silva	k1gFnSc1
•	•	k?
Julian	Julian	k1gMnSc1
Draxler	Draxler	k1gMnSc1
•	•	k?
Christopher	Christophra	k1gFnPc2
Nkunku	Nkunek	k1gInSc2
•	•	k?
Leandro	Leandra	k1gFnSc5
Paredes	Paredesa	k1gFnPc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Verratti	Verratť	k1gFnSc2
•	•	k?
Eric	Eric	k1gInSc1
Maxim	Maxim	k1gMnSc1
Choupo-Moting	Choupo-Moting	k1gInSc1
•	•	k?
Edinson	Edinson	k1gMnSc1
Cavani	Cavan	k1gMnPc1
•	•	k?
Moussa	Moussa	k1gFnSc1
Diaby	Diaba	k1gFnSc2
•	•	k?
Metehan	Metehan	k1gMnSc1
Güçlü	Güçlü	k1gMnSc1
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Kylian	Kylian	k1gMnSc1
Mbappé	Mbappý	k2eAgFnSc2d1
•	•	k?
Neymar	Neymar	k1gMnSc1
•	•	k?
Antoine	Antoin	k1gMnSc5
Bernede	Berned	k1gMnSc5
•	•	k?
Lassana	Lassan	k1gMnSc2
Diarra	Diarr	k1gMnSc2
•	•	k?
Giovani	Giovan	k1gMnPc1
Lo	Lo	k1gFnSc2
Celso	Celsa	k1gFnSc5
•	•	k?
Timothy	Timotha	k1gFnSc2
Weah	Weaha	k1gFnPc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Thomas	Thomas	k1gMnSc1
Tuchel	Tuchel	k1gMnSc1
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1
superpohár	superpohár	k1gInSc1
2015	#num#	k4
–	–	k?
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gMnSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Kevin	Kevin	k1gMnSc1
Trapp	Trapp	k1gMnSc1
•	•	k?
Salvatore	Salvator	k1gMnSc5
Sirigu	Sirig	k1gMnSc5
Hráči	hráč	k1gMnSc5
</s>
<s>
Serge	Serge	k6eAd1
Aurier	Aurier	k1gMnSc1
•	•	k?
David	David	k1gMnSc1
Luiz	Luiz	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Silva	Silva	k1gFnSc1
•	•	k?
Maxwell	maxwell	k1gInSc1
•	•	k?
Marco	Marco	k1gMnSc1
Verratti	Verratť	k1gFnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Edinson	Edinson	k1gMnSc1
Cavani	Cavan	k1gMnPc1
•	•	k?
Zlatan	Zlatan	k1gInSc1
Ibrahimović	Ibrahimović	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Moura	Moura	k?
•	•	k?
Marquinhos	Marquinhos	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Digne	Dign	k1gInSc5
•	•	k?
Benjamin	Benjamin	k1gMnSc1
Stambouli	Stambouli	k1gMnSc1
•	•	k?
Thiago	Thiago	k1gMnSc1
Motta	motto	k1gNnSc2
•	•	k?
Jean-Christophe	Jean-Christophe	k1gNnSc2
Bahebeck	Bahebecka	k1gFnPc2
•	•	k?
Jean-Kévin	Jean-Kévina	k1gFnPc2
Augustin	Augustin	k1gMnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Laurent	Laurent	k1gMnSc1
Blanc	Blanc	k1gMnSc1
</s>
<s>
Serie	serie	k1gFnSc1
A	a	k9
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
–	–	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Szczęsny	Szczęsna	k1gFnSc2
•	•	k?
Gianluigi	Gianluigi	k1gNnPc2
Buffon	Buffon	k1gInSc1
•	•	k?
Carlo	Carla	k1gMnSc5
Pinsoglio	Pinsoglia	k1gMnSc5
Hráči	hráč	k1gMnSc5
</s>
<s>
Mattia	Mattia	k1gFnSc1
De	De	k?
Sciglio	Sciglio	k6eAd1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
•	•	k?
Matthijs	Matthijsa	k1gFnPc2
de	de	k?
Ligt	Ligt	k1gMnSc1
•	•	k?
Alex	Alex	k1gMnSc1
Sandro	Sandra	k1gFnSc5
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnPc1
•	•	k?
Daniele	Daniela	k1gFnSc3
Rugani	Rugaň	k1gFnSc3
•	•	k?
Merih	Merih	k1gInSc1
Demiral	Demiral	k1gFnSc1
•	•	k?
Miralem	Miral	k1gInSc7
Pjanić	Pjanić	k1gMnSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Aaron	Aaron	k1gMnSc1
Ramsey	Ramsea	k1gFnSc2
•	•	k?
Blaise	Blaise	k1gFnSc2
Matuidi	Matuid	k1gMnPc1
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Bentancur	Bentancur	k1gMnSc1
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
•	•	k?
Paulo	Paula	k1gFnSc5
Dybala	Dybal	k1gMnSc4
•	•	k?
Douglas	Douglas	k1gMnSc1
Costa	Costa	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Cuadrado	Cuadrada	k1gFnSc5
•	•	k?
Gonzalo	Gonzalo	k1gMnSc1
Higuaín	Higuaín	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Bernardeschi	Bernardesch	k1gFnSc2
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Massimiliano	Massimiliana	k1gFnSc5
Allegri	Allegr	k1gFnSc5
</s>
<s>
Supercoppa	Supercoppa	k1gFnSc1
italiana	italiana	k1gFnSc1
2020	#num#	k4
–	–	k?
Juventus	Juventus	k1gInSc1
FC	FC	kA
Brankář	brankář	k1gMnSc1
</s>
<s>
Wojciech	Wojciech	k1gInSc1
Szczęsny	Szczęsna	k1gFnSc2
•	•	k?
Carlo	Carlo	k1gNnSc1
Pinsoglio	Pinsoglio	k1gMnSc1
•	•	k?
Gianluigi	Gianluigi	k1gNnPc2
Buffon	Buffon	k1gInSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Juan	Juan	k1gMnSc1
Cuadrado	Cuadrada	k1gFnSc5
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Bonucci	Bonucce	k1gMnPc1
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
(	(	kIx(
<g/>
c	c	k0
<g/>
)	)	kIx)
•	•	k?
Danilo	danit	k5eAaImAgNnS
•	•	k?
Weston	Weston	k1gInSc4
McKennie	McKennie	k1gFnSc2
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Bentancur	Bentancur	k1gMnSc1
•	•	k?
Arthur	Arthur	k1gMnSc1
Melo	Melo	k?
•	•	k?
Federico	Federico	k6eAd1
Chiesa	Chiesa	k1gFnSc1
•	•	k?
Dejan	Dejan	k1gInSc1
Kulusevski	Kulusevsk	k1gFnSc2
•	•	k?
Cristiano	Cristiana	k1gFnSc5
Ronaldo	Ronalda	k1gFnSc5
•	•	k?
Alessandro	Alessandra	k1gFnSc5
Di	Di	k1gMnPc7
Pardo	Pardo	k1gNnSc1
•	•	k?
Radu	rad	k1gInSc2
Drăguș	Drăguș	k1gMnSc1
•	•	k?
Gianluca	Gianluca	k1gMnSc1
Frabotta	Frabotta	k1gMnSc1
•	•	k?
Aaron	Aaron	k1gMnSc1
Ramsey	Ramsea	k1gFnSc2
•	•	k?
Adrien	Adriena	k1gFnPc2
Rabiot	Rabiot	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Bernardeschi	Bernardesch	k1gFnSc2
•	•	k?
Nicolò	Nicolò	k1gFnSc2
Fagioli	Fagiole	k1gFnSc3
•	•	k?
Filippo	Filippa	k1gFnSc5
Ranocchia	Ranocchium	k1gNnPc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Morata	Morat	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Francie	Francie	k1gFnSc1
|	|	kIx~
Fotbal	fotbal	k1gInSc1
</s>
