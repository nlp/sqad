<s>
Většinu	většina	k1gFnSc4
své	svůj	k3xOyFgFnSc2
kariéry	kariéra	k1gFnSc2
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
Paris	Paris	k1gMnSc1
Saint-Germain	Saint-Germain	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
debutoval	debutovat	k5eAaBmAgMnS
v	v	k7c6
ligovém	ligový	k2eAgInSc6d1
zápase	zápas	k1gInSc6
proti	proti	k7c3
Bordeaux	Bordeaux	k1gNnSc3
(	(	kIx(
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
získal	získat	k5eAaPmAgMnS
s	s	k7c7
ním	on	k3xPp3gInSc7
15	#num#	k4
trofejí	trofej	k1gFnPc2
včetně	včetně	k7c2
čtyř	čtyři	k4xCgFnPc2
titulů	titul	k1gInPc2
v	v	k7c6
Ligue	Ligue	k1gNnPc6
1	#num#	k4
a	a	k8xC
a	a	k8xC
treble	treble	k6eAd1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>