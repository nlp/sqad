<s>
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Pride	Prid	k1gInSc5	Prid
and	and	k?	and
Prejudice	prejudice	k1gFnSc1	prejudice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgInSc1d3	nejznámější
román	román	k1gInSc1	román
anglické	anglický	k2eAgFnSc2d1	anglická
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Jane	Jan	k1gMnSc5	Jan
Austenové	Austenová	k1gFnSc3	Austenová
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
úvodů	úvod	k1gInPc2	úvod
knihy	kniha	k1gFnSc2	kniha
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
vůbec	vůbec	k9	vůbec
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
It	It	k1gFnSc1	It
is	is	k?	is
a	a	k8xC	a
truth	truth	k1gMnSc1	truth
universally	universalla	k1gFnSc2	universalla
acknowledged	acknowledged	k1gMnSc1	acknowledged
<g/>
,	,	kIx,	,
that	that	k1gMnSc1	that
a	a	k8xC	a
single	singl	k1gInSc5	singl
man	man	k1gMnSc1	man
in	in	k?	in
possession	possession	k1gInSc1	possession
of	of	k?	of
a	a	k8xC	a
good	good	k6eAd1	good
fortune	fortun	k1gInSc5	fortun
<g/>
,	,	kIx,	,
must	mustum	k1gNnPc2	mustum
be	be	k?	be
in	in	k?	in
want	want	k1gInSc1	want
of	of	k?	of
a	a	k8xC	a
wife	wifat	k5eAaPmIp3nS	wifat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
česky	česky	k6eAd1	česky
podle	podle	k7c2	podle
překladu	překlad	k1gInSc2	překlad
Evy	Eva	k1gFnSc2	Eva
Kondrysové	Kondrysová	k1gFnSc2	Kondrysová
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Světem	svět	k1gInSc7	svět
panuje	panovat	k5eAaImIp3nS	panovat
skálopevné	skálopevný	k2eAgNnSc1d1	skálopevné
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
svobodný	svobodný	k2eAgMnSc1d1	svobodný
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
slušné	slušný	k2eAgNnSc4d1	slušné
jmění	jmění	k1gNnSc4	jmění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neobejde	obejde	k6eNd1	obejde
bez	bez	k7c2	bez
ženušky	ženuška	k1gFnSc2	ženuška
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc4	první
verzi	verze	k1gFnSc4	verze
románu	román	k1gInSc2	román
sepsala	sepsat	k5eAaPmAgFnS	sepsat
v	v	k7c6	v
letech	let	k1gInPc6	let
1795	[number]	k4	1795
–	–	k?	–
1797	[number]	k4	1797
a	a	k8xC	a
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
ji	on	k3xPp3gFnSc4	on
First	First	k1gFnSc4	First
Impresions	Impresions	k1gInSc1	Impresions
(	(	kIx(	(
<g/>
První	první	k4xOgInPc1	první
dojmy	dojem	k1gInPc1	dojem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
ale	ale	k8xC	ale
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
najít	najít	k5eAaPmF	najít
vydavatele	vydavatel	k1gMnPc4	vydavatel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jej	on	k3xPp3gInSc4	on
prozatím	prozatím	k6eAd1	prozatím
odložila	odložit	k5eAaPmAgFnS	odložit
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
románu	román	k1gInSc6	román
Rozum	rozum	k1gInSc1	rozum
a	a	k8xC	a
cit	cit	k1gInSc1	cit
(	(	kIx(	(
<g/>
Sense	Sense	k1gFnSc1	Sense
and	and	k?	and
Sensibility	sensibilita	k1gFnSc2	sensibilita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gInSc6	jehož
vydání	vydání	k1gNnSc6	vydání
(	(	kIx(	(
<g/>
1811	[number]	k4	1811
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
k	k	k7c3	k
Prvním	první	k4xOgInPc3	první
dojmům	dojem	k1gInPc3	dojem
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
,	,	kIx,	,
přepracovala	přepracovat	k5eAaPmAgFnS	přepracovat
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc4	předsudek
vydala	vydat	k5eAaPmAgFnS	vydat
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
román	román	k1gInSc1	román
rodinného	rodinný	k2eAgInSc2d1	rodinný
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
domestic	domestice	k1gFnPc2	domestice
novel	novela	k1gFnPc2	novela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
s	s	k7c7	s
ironickým	ironický	k2eAgInSc7d1	ironický
podtextem	podtext	k1gInSc7	podtext
líčí	líčit	k5eAaImIp3nS	líčit
prostředí	prostředí	k1gNnSc1	prostředí
anglického	anglický	k2eAgInSc2d1	anglický
venkova	venkov	k1gInSc2	venkov
<g/>
,	,	kIx,	,
tamní	tamní	k2eAgInPc1d1	tamní
rodinné	rodinný	k2eAgInPc1d1	rodinný
vztahy	vztah	k1gInPc1	vztah
a	a	k8xC	a
snahy	snaha	k1gFnSc2	snaha
matek	matka	k1gFnPc2	matka
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
dcery	dcera	k1gFnSc2	dcera
vhodné	vhodný	k2eAgMnPc4d1	vhodný
ženichy	ženich	k1gMnPc4	ženich
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
románu	román	k1gInSc2	román
jsou	být	k5eAaImIp3nP	být
slečna	slečna	k1gFnSc1	slečna
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Bennetová	Bennetový	k2eAgFnSc1d1	Bennetová
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
zosobnění	zosobnění	k1gNnSc1	zosobnění
autorky	autorka	k1gFnSc2	autorka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnSc2	Darca
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
paní	paní	k1gFnSc1	paní
Bennetová	Bennetový	k2eAgFnSc1d1	Bennetová
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
pěti	pět	k4xCc2	pět
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
chce	chtít	k5eAaImIp3nS	chtít
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
co	co	k3yInSc4	co
nejlépe	dobře	k6eAd3	dobře
provdat	provdat	k5eAaPmF	provdat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
sousedství	sousedství	k1gNnSc2	sousedství
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
bohatý	bohatý	k2eAgMnSc1d1	bohatý
a	a	k8xC	a
svobodný	svobodný	k2eAgMnSc1d1	svobodný
muž	muž	k1gMnSc1	muž
pan	pan	k1gMnSc1	pan
Bingley	Binglea	k1gFnSc2	Binglea
<g/>
,	,	kIx,	,
seznámí	seznámit	k5eAaPmIp3nP	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
na	na	k7c6	na
plese	pleso	k1gNnSc6	pleso
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
její	její	k3xOp3gFnSc1	její
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Jane	Jan	k1gMnSc5	Jan
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
plese	pleso	k1gNnSc6	pleso
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnSc2	Darca
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
bohatší	bohatý	k2eAgFnPc4d2	bohatší
než	než	k8xS	než
Bingley	Bingle	k2eAgFnPc4d1	Bingle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
působí	působit	k5eAaImIp3nS	působit
pyšně	pyšně	k6eAd1	pyšně
<g/>
,	,	kIx,	,
povýšeně	povýšeně	k6eAd1	povýšeně
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
pohrdavě	pohrdavě	k6eAd1	pohrdavě
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
o	o	k7c6	o
druhé	druhý	k4xOgFnSc6	druhý
nejstarší	starý	k2eAgFnSc6d3	nejstarší
dceři	dcera	k1gFnSc6	dcera
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
do	do	k7c2	do
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gNnSc3	její
postavení	postavení	k1gNnSc3	postavení
a	a	k8xC	a
popichování	popichování	k1gNnSc4	popichování
Bingleyho	Bingley	k1gMnSc2	Bingley
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
ho	on	k3xPp3gMnSc4	on
touží	toužit	k5eAaImIp3nS	toužit
vzít	vzít	k5eAaPmF	vzít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
snaží	snažit	k5eAaImIp3nS	snažit
utajit	utajit	k5eAaPmF	utajit
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
mladším	mladý	k2eAgFnPc3d2	mladší
sestrám	sestra	k1gFnPc3	sestra
seznámí	seznámit	k5eAaPmIp3nP	seznámit
s	s	k7c7	s
důstojníkem	důstojník	k1gMnSc7	důstojník
z	z	k7c2	z
pluku	pluk	k1gInSc2	pluk
Wickhamem	Wickham	k1gInSc7	Wickham
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jí	on	k3xPp3gFnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Darcyho	Darcy	k1gMnSc4	Darcy
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
zná	znát	k5eAaImIp3nS	znát
a	a	k8xC	a
že	že	k8xS	že
Darcy	Darca	k1gFnPc1	Darca
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
zachoval	zachovat	k5eAaPmAgMnS	zachovat
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
a	a	k8xC	a
připravil	připravit	k5eAaPmAgMnS	připravit
jej	on	k3xPp3gMnSc4	on
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Elizabethina	Elizabethin	k2eAgFnSc1d1	Elizabethina
antipatie	antipatie	k1gFnSc1	antipatie
vůči	vůči	k7c3	vůči
Darcymu	Darcym	k1gInSc3	Darcym
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
ještě	ještě	k6eAd1	ještě
navyšuje	navyšovat	k5eAaImIp3nS	navyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Rodinu	rodina	k1gFnSc4	rodina
Bennetových	Bennetový	k2eAgMnPc2d1	Bennetový
přijíždí	přijíždět	k5eAaImIp3nP	přijíždět
navštívit	navštívit	k5eAaPmF	navštívit
otcův	otcův	k2eAgMnSc1d1	otcův
bratranec	bratranec	k1gMnSc1	bratranec
Collins	Collinsa	k1gFnPc2	Collinsa
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
sester	sestra	k1gFnPc2	sestra
vzít	vzít	k5eAaPmF	vzít
a	a	k8xC	a
nastěhovat	nastěhovat	k5eAaPmF	nastěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
faru	fara	k1gFnSc4	fara
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
velmi	velmi	k6eAd1	velmi
bohaté	bohatý	k2eAgNnSc1d1	bohaté
a	a	k8xC	a
vysoko	vysoko	k6eAd1	vysoko
postavené	postavený	k2eAgFnPc4d1	postavená
lady	lady	k1gFnPc4	lady
Catherine	Catherin	k1gInSc5	Catherin
de	de	k?	de
Bourgh	Bourgha	k1gFnPc2	Bourgha
<g/>
,	,	kIx,	,
tety	teta	k1gFnPc1	teta
pana	pan	k1gMnSc2	pan
Darcyho	Darcy	k1gMnSc2	Darcy
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
volba	volba	k1gFnSc1	volba
padne	padnout	k5eAaPmIp3nS	padnout
na	na	k7c4	na
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
vezme	vzít	k5eAaPmIp3nS	vzít
její	její	k3xOp3gFnSc4	její
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
pan	pan	k1gMnSc1	pan
Bingley	Binglea	k1gFnSc2	Binglea
i	i	k8xC	i
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnSc2	Darca
odjíždějí	odjíždět	k5eAaImIp3nP	odjíždět
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
Jane	Jan	k1gMnSc5	Jan
má	mít	k5eAaImIp3nS	mít
zlomené	zlomený	k2eAgNnSc4d1	zlomené
srdce	srdce	k1gNnSc4	srdce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Bingleym	Bingleym	k1gInSc1	Bingleym
je	být	k5eAaImIp3nS	být
vážný	vážný	k2eAgInSc1d1	vážný
<g/>
.	.	kIx.	.
</s>
<s>
Jede	jet	k5eAaImIp3nS	jet
proto	proto	k8xC	proto
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tetě	teta	k1gFnSc3	teta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemusela	muset	k5eNaImAgFnS	muset
snášet	snášet	k5eAaImF	snášet
matčiny	matčin	k2eAgFnPc4d1	matčina
narážky	narážka	k1gFnPc4	narážka
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
se	se	k3xPyFc4	se
také	také	k9	také
vydává	vydávat	k5eAaImIp3nS	vydávat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
přítelkyni	přítelkyně	k1gFnSc3	přítelkyně
Charlotte	Charlott	k1gInSc5	Charlott
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
navštíví	navštívit	k5eAaPmIp3nS	navštívit
i	i	k9	i
zámek	zámek	k1gInSc1	zámek
lady	lady	k1gFnSc2	lady
Catherine	Catherin	k1gInSc5	Catherin
a	a	k8xC	a
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
plánuje	plánovat	k5eAaImIp3nS	plánovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
dcera	dcera	k1gFnSc1	dcera
vdá	vdát	k5eAaPmIp3nS	vdát
za	za	k7c4	za
pana	pan	k1gMnSc4	pan
Darcyho	Darcy	k1gMnSc4	Darcy
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Darcy	Darca	k1gFnSc2	Darca
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnSc7	okolnost
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
také	také	k9	také
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrancem	bratranec	k1gMnSc7	bratranec
plukovníkem	plukovník	k1gMnSc7	plukovník
Fitzwilliamem	Fitzwilliam	k1gInSc7	Fitzwilliam
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
seznámí	seznámit	k5eAaPmIp3nS	seznámit
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
jí	jíst	k5eAaImIp3nS	jíst
také	také	k9	také
nepřímo	přímo	k6eNd1	přímo
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
díky	díky	k7c3	díky
Darcymu	Darcymum	k1gNnSc3	Darcymum
Bingley	Binglea	k1gMnSc2	Binglea
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
dál	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
nestýká	stýkat	k5eNaImIp3nS	stýkat
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
se	se	k3xPyFc4	se
jeden	jeden	k4xCgInSc1	jeden
večer	večer	k1gInSc1	večer
necítí	cítit	k5eNaImIp3nS	cítit
dobře	dobře	k6eAd1	dobře
a	a	k8xC	a
nejde	jít	k5eNaImIp3nS	jít
s	s	k7c7	s
ostatními	ostatní	k1gNnPc7	ostatní
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
přijde	přijít	k5eAaPmIp3nS	přijít
ji	on	k3xPp3gFnSc4	on
navštívit	navštívit	k5eAaPmF	navštívit
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnPc4	Darca
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
překvapí	překvapit	k5eAaPmIp3nP	překvapit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
miluje	milovat	k5eAaImIp3nS	milovat
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vzít	vzít	k5eAaPmF	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
jej	on	k3xPp3gMnSc4	on
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
a	a	k8xC	a
vyčte	vyčíst	k5eAaPmIp3nS	vyčíst
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
provedl	provést	k5eAaPmAgInS	provést
panu	pan	k1gMnSc3	pan
Wickhamovi	Wickham	k1gMnSc3	Wickham
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
sestře	sestra	k1gFnSc3	sestra
a	a	k8xC	a
Bingleymu	Bingleym	k1gInSc3	Bingleym
<g/>
.	.	kIx.	.
</s>
<s>
Darcy	Darca	k1gFnPc4	Darca
odejde	odejít	k5eAaPmIp3nS	odejít
a	a	k8xC	a
ráno	ráno	k6eAd1	ráno
jí	on	k3xPp3gFnSc3	on
předá	předat	k5eAaPmIp3nS	předat
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bingleyho	Bingley	k1gMnSc4	Bingley
odvedl	odvést	k5eAaPmAgMnS	odvést
od	od	k7c2	od
její	její	k3xOp3gFnSc2	její
sestry	sestra	k1gFnSc2	sestra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
jej	on	k3xPp3gMnSc4	on
nemiluje	milovat	k5eNaImIp3nS	milovat
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
vzít	vzít	k5eAaPmF	vzít
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
penězům	peníze	k1gInPc3	peníze
a	a	k8xC	a
že	že	k8xS	že
Wickhama	Wickhama	k1gNnSc4	Wickhama
vůbec	vůbec	k9	vůbec
neošidil	ošidit	k5eNaPmAgMnS	ošidit
o	o	k7c4	o
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
on	on	k3xPp3gMnSc1	on
vydíral	vydírat	k5eAaImAgMnS	vydírat
jeho	jeho	k3xOp3gFnSc4	jeho
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
chtěl	chtít	k5eAaImAgMnS	chtít
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
sestrou	sestra	k1gFnSc7	sestra
tajně	tajně	k6eAd1	tajně
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
dopis	dopis	k1gInSc4	dopis
nejdřív	dříve	k6eAd3	dříve
dotknul	dotknout	k5eAaPmAgMnS	dotknout
a	a	k8xC	a
nechtěla	chtít	k5eNaImAgFnS	chtít
mu	on	k3xPp3gMnSc3	on
příliš	příliš	k6eAd1	příliš
věřit	věřit	k5eAaImF	věřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
musela	muset	k5eAaImAgFnS	muset
uznat	uznat	k5eAaPmF	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Darcy	Darca	k1gFnPc4	Darca
uvádí	uvádět	k5eAaImIp3nS	uvádět
argumenty	argument	k1gInPc4	argument
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
nelze	lze	k6eNd1	lze
nevěřit	věřit	k5eNaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
odjela	odjet	k5eAaPmAgFnS	odjet
zpět	zpět	k6eAd1	zpět
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Doma	doma	k6eAd1	doma
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
chystají	chystat	k5eAaImIp3nP	chystat
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
nejmladší	mladý	k2eAgFnSc1d3	nejmladší
sestra	sestra	k1gFnSc1	sestra
chce	chtít	k5eAaImIp3nS	chtít
cestovat	cestovat	k5eAaImF	cestovat
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
jí	jíst	k5eAaImIp3nS	jíst
to	ten	k3xDgNnSc4	ten
dovolí	dovolit	k5eAaPmIp3nS	dovolit
a	a	k8xC	a
Lydia	Lydia	k1gFnSc1	Lydia
odjede	odjet	k5eAaPmIp3nS	odjet
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
tetou	teta	k1gFnSc7	teta
a	a	k8xC	a
strýcem	strýc	k1gMnSc7	strýc
po	po	k7c6	po
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
přijeli	přijet	k5eAaPmAgMnP	přijet
také	také	k9	také
na	na	k7c4	na
Darcyho	Darcy	k1gMnSc4	Darcy
sídlo	sídlo	k1gNnSc1	sídlo
Pemberley	Pemberle	k2eAgInPc1d1	Pemberle
<g/>
,	,	kIx,	,
Elizabet	Elizabet	k1gInSc1	Elizabet
se	se	k3xPyFc4	se
ujistí	ujistit	k5eAaPmIp3nS	ujistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tam	tam	k6eAd1	tam
není	být	k5eNaImIp3nS	být
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
podívat	podívat	k5eAaPmF	podívat
<g/>
,	,	kIx,	,
při	při	k7c6	při
prohlídce	prohlídka	k1gFnSc6	prohlídka
parku	park	k1gInSc2	park
se	se	k3xPyFc4	se
ale	ale	k9	ale
Darcy	Darca	k1gFnPc1	Darca
nečekaně	nečekaně	k6eAd1	nečekaně
objeví	objevit	k5eAaPmIp3nP	objevit
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
ji	on	k3xPp3gFnSc4	on
pozdravit	pozdravit	k5eAaPmF	pozdravit
<g/>
,	,	kIx,	,
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
změnil	změnit	k5eAaPmAgInS	změnit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
milý	milý	k1gMnSc1	milý
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
dřívějším	dřívější	k2eAgNnSc6d1	dřívější
odmítnutí	odmítnutí	k1gNnSc6	odmítnutí
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc4	její
dřívější	dřívější	k2eAgFnSc4d1	dřívější
nenávist	nenávist	k1gFnSc4	nenávist
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
vytrácet	vytrácet	k5eAaImF	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Seznámí	seznámit	k5eAaPmIp3nS	seznámit
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
sestrou	sestra	k1gFnSc7	sestra
a	a	k8xC	a
na	na	k7c4	na
Pemberley	Pemberlea	k1gFnPc4	Pemberlea
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
návštěvě	návštěva	k1gFnSc6	návštěva
i	i	k9	i
Bingley	Bingley	k1gInPc4	Bingley
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
sestry	sestra	k1gFnSc2	sestra
stále	stále	k6eAd1	stále
zamilovaný	zamilovaný	k2eAgInSc1d1	zamilovaný
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
ale	ale	k9	ale
přijde	přijít	k5eAaPmIp3nS	přijít
dopis	dopis	k1gInSc4	dopis
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
sděleno	sdělen	k2eAgNnSc1d1	sděleno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
její	její	k3xOp3gFnSc1	její
sestra	sestra	k1gFnSc1	sestra
Lydia	Lydium	k1gNnSc2	Lydium
utekla	utéct	k5eAaPmAgFnS	utéct
s	s	k7c7	s
Wickhamem	Wickham	k1gInSc7	Wickham
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
neví	vědět	k5eNaImIp3nS	vědět
kam	kam	k6eAd1	kam
<g/>
.	.	kIx.	.
</s>
<s>
Poví	povědět	k5eAaPmIp3nS	povědět
o	o	k7c6	o
tom	ten	k3xDgMnSc6	ten
panu	pan	k1gMnSc6	pan
Darcymu	Darcym	k1gInSc6	Darcym
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
Lydii	Lydie	k1gFnSc4	Lydie
vydal	vydat	k5eAaPmAgMnS	vydat
hledat	hledat	k5eAaImF	hledat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
strýc	strýc	k1gMnSc1	strýc
vydává	vydávat	k5eAaPmIp3nS	vydávat
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
rodina	rodina	k1gFnSc1	rodina
Bennetových	Bennetový	k2eAgMnPc2d1	Bennetový
zažívá	zažívat	k5eAaImIp3nS	zažívat
velkou	velký	k2eAgFnSc4d1	velká
ostudu	ostuda	k1gFnSc4	ostuda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Lydia	Lydia	k1gFnSc1	Lydia
a	a	k8xC	a
Wickham	Wickham	k1gInSc1	Wickham
se	se	k3xPyFc4	se
nevzali	vzít	k5eNaPmAgMnP	vzít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
spolu	spolu	k6eAd1	spolu
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otcově	otcův	k2eAgInSc6d1	otcův
návratu	návrat	k1gInSc6	návrat
přichází	přicházet	k5eAaImIp3nS	přicházet
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
strýček	strýček	k1gMnSc1	strýček
Lydii	Lydie	k1gFnSc4	Lydie
našel	najít	k5eAaPmAgMnS	najít
a	a	k8xC	a
zařídil	zařídit	k5eAaPmAgMnS	zařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
pochopit	pochopit	k5eAaPmF	pochopit
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nakonec	nakonec	k6eAd1	nakonec
Wickham	Wickham	k1gInSc1	Wickham
vzal	vzít	k5eAaPmAgInS	vzít
<g/>
,	,	kIx,	,
když	když	k8xS	když
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc4	žádný
pořádné	pořádný	k2eAgNnSc4d1	pořádné
věno	věno	k1gNnSc4	věno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
Lydia	Lydia	k1gFnSc1	Lydia
s	s	k7c7	s
Wickhamem	Wickham	k1gInSc7	Wickham
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
vrátí	vrátit	k5eAaPmIp3nS	vrátit
domů	domů	k6eAd1	domů
a	a	k8xC	a
jí	on	k3xPp3gFnSc3	on
uklouzne	uklouznout	k5eAaPmIp3nS	uklouznout
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
byl	být	k5eAaImAgMnS	být
i	i	k9	i
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnSc2	Darca
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
zarazí	zarazit	k5eAaPmIp3nS	zarazit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
Darcy	Darca	k1gFnSc2	Darca
Wickhama	Wickhama	k1gNnSc1	Wickhama
nemůže	moct	k5eNaImIp3nS	moct
vystát	vystát	k5eAaPmF	vystát
<g/>
,	,	kIx,	,
napíše	napsat	k5eAaPmIp3nS	napsat
proto	proto	k8xC	proto
tetě	teta	k1gFnSc3	teta
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jí	on	k3xPp3gFnSc3	on
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Darcy	Darca	k1gFnSc2	Darca
dal	dát	k5eAaPmAgInS	dát
Wickhamovi	Wickham	k1gMnSc3	Wickham
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
Lydii	Lydie	k1gFnSc4	Lydie
vzal	vzít	k5eAaPmAgMnS	vzít
a	a	k8xC	a
odstranil	odstranit	k5eAaPmAgMnS	odstranit
tak	tak	k6eAd1	tak
ostudu	ostuda	k1gFnSc4	ostuda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c6	na
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
a	a	k8xC	a
jestli	jestli	k8xS	jestli
to	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
pořád	pořád	k6eAd1	pořád
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
se	se	k3xPyFc4	se
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
pan	pan	k1gMnSc1	pan
Bingley	Binglea	k1gFnSc2	Binglea
i	i	k8xC	i
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnSc2	Darca
a	a	k8xC	a
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Darcy	Darc	k1gMnPc4	Darc
asi	asi	k9	asi
napravil	napravit	k5eAaPmAgMnS	napravit
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
způsobil	způsobit	k5eAaPmAgMnS	způsobit
a	a	k8xC	a
Bingley	Bingley	k1gInPc4	Bingley
a	a	k8xC	a
Jane	Jan	k1gMnSc5	Jan
se	se	k3xPyFc4	se
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
Darcy	Darca	k1gFnPc4	Darca
musel	muset	k5eAaImAgInS	muset
nakrátko	nakrátko	k6eAd1	nakrátko
odjet	odjet	k5eAaPmF	odjet
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nečekaně	nečekaně	k6eAd1	nečekaně
Bennetovy	Bennetův	k2eAgFnPc1d1	Bennetova
navštívila	navštívit	k5eAaPmAgFnS	navštívit
lady	lady	k1gFnSc2	lady
Catherine	Catherin	k1gInSc5	Catherin
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
domnívala	domnívat	k5eAaImAgFnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
její	její	k3xOp3gMnSc1	její
synovec	synovec	k1gMnSc1	synovec
a	a	k8xC	a
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
jí	on	k3xPp3gFnSc3	on
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lady	lady	k1gFnSc1	lady
Catherine	Catherin	k1gInSc5	Catherin
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
naléhá	naléhat	k5eAaImIp3nS	naléhat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
s	s	k7c7	s
Darcym	Darcymum	k1gNnPc2	Darcymum
úplně	úplně	k6eAd1	úplně
přestala	přestat	k5eAaPmAgFnS	přestat
stýkat	stýkat	k5eAaImF	stýkat
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
popudí	popudit	k5eAaPmIp3nS	popudit
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
rozloučí	rozloučit	k5eAaPmIp3nS	rozloučit
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
ale	ale	k9	ale
ona	onen	k3xDgFnSc1	onen
mezitím	mezitím	k6eAd1	mezitím
do	do	k7c2	do
Darcyho	Darcy	k1gMnSc2	Darcy
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gFnSc1	jeho
teta	teta	k1gFnSc1	teta
neovlivnila	ovlivnit	k5eNaPmAgFnS	ovlivnit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ale	ale	k9	ale
nestalo	stát	k5eNaPmAgNnS	stát
a	a	k8xC	a
Darcyho	Darcy	k1gMnSc2	Darcy
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
slyšel	slyšet	k5eAaImAgMnS	slyšet
od	od	k7c2	od
lady	lady	k1gFnSc2	lady
Catherine	Catherin	k1gInSc5	Catherin
<g/>
,	,	kIx,	,
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
u	u	k7c2	u
Elizabeth	Elizabeth	k1gFnSc2	Elizabeth
šanci	šance	k1gFnSc4	šance
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
opětovném	opětovný	k2eAgInSc6d1	opětovný
příjezdu	příjezd	k1gInSc6	příjezd
se	se	k3xPyFc4	se
zasnoubili	zasnoubit	k5eAaPmAgMnP	zasnoubit
a	a	k8xC	a
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
se	se	k3xPyFc4	se
odstěhovala	odstěhovat	k5eAaPmAgFnS	odstěhovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
sídlo	sídlo	k1gNnSc4	sídlo
Pemberley	Pemberlea	k1gFnSc2	Pemberlea
<g/>
.	.	kIx.	.
</s>
<s>
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc4	předsudek
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
mnoha	mnoho	k4c2	mnoho
televizních	televizní	k2eAgInPc2d1	televizní
a	a	k8xC	a
filmových	filmový	k2eAgFnPc2d1	filmová
adaptací	adaptace	k1gFnPc2	adaptace
:	:	kIx,	:
1938	[number]	k4	1938
<g/>
:	:	kIx,	:
Pride	Prid	k1gInSc5	Prid
and	and	k?	and
Prejudice	prejudice	k1gFnPc4	prejudice
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Curigwen	Curigwen	k2eAgInSc4d1	Curigwen
Lewis	Lewis	k1gInSc4	Lewis
(	(	kIx(	(
<g/>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Bennet	Bennet	k1gMnSc1	Bennet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Andrew	Andrew	k1gMnSc1	Andrew
Osborn	Osborn	k1gMnSc1	Osborn
(	(	kIx(	(
<g/>
Darcy	Darcy	k1gInPc1	Darcy
<g/>
)	)	kIx)	)
1940	[number]	k4	1940
<g/>
:	:	kIx,	:
Pride	Prid	k1gInSc5	Prid
and	and	k?	and
Prejudice	prejudice	k1gFnPc4	prejudice
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Laurence	Laurence	k1gFnPc1	Laurence
Olivier	Olivira	k1gFnPc2	Olivira
(	(	kIx(	(
<g/>
Darcy	Darcy	k1gInPc7	Darcy
<g/>
)	)	kIx)	)
a	a	k8xC	a
Greer	Greer	k1gMnSc1	Greer
Garson	garson	k1gMnSc1	garson
(	(	kIx(	(
<g/>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1952	[number]	k4	1952
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
<g/>
.	.	kIx.	.
</s>
<s>
BBC	BBC	kA	BBC
živě	živě	k6eAd1	živě
vysílala	vysílat	k5eAaImAgFnS	vysílat
pětidílný	pětidílný	k2eAgInSc4d1	pětidílný
miniseriál	miniseriál	k1gInSc4	miniseriál
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
hráli	hrát	k5eAaImAgMnP	hrát
Ann	Ann	k1gFnSc7	Ann
Baskett	Baskett	k1gMnSc1	Baskett
(	(	kIx(	(
<g/>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
<g/>
)	)	kIx)	)
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Cushing	Cushing	k1gInSc1	Cushing
(	(	kIx(	(
<g/>
Mr	Mr	k1gFnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Darcy	Darca	k1gFnPc1	Darca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
Pride	Prid	k1gInSc5	Prid
and	and	k?	and
Prejudice	prejudice	k1gFnPc1	prejudice
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
Elizabeth	Elizabeth	k1gFnSc4	Elizabeth
Garvie	Garvie	k1gFnSc2	Garvie
a	a	k8xC	a
David	David	k1gMnSc1	David
Rintoul	Rintoula	k1gFnPc2	Rintoula
<g/>
,	,	kIx,	,
adaptovala	adaptovat	k5eAaBmAgFnS	adaptovat
Fay	Fay	k1gFnSc1	Fay
Weldonová	Weldonová	k1gFnSc1	Weldonová
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Pride	Prid	k1gInSc5	Prid
and	and	k?	and
Prejudice	prejudice	k1gFnPc1	prejudice
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
hráli	hrát	k5eAaImAgMnP	hrát
Colin	Colin	k2eAgInSc4d1	Colin
Firth	Firth	k1gInSc4	Firth
a	a	k8xC	a
Jennifer	Jennifer	k1gInSc4	Jennifer
Ehle	Ehl	k1gFnSc2	Ehl
<g/>
,	,	kIx,	,
adaptoval	adaptovat	k5eAaBmAgMnS	adaptovat
Andrew	Andrew	k1gMnSc1	Andrew
Davies	Davies	k1gMnSc1	Davies
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Pride	Prid	k1gInSc5	Prid
and	and	k?	and
Prejudice	prejudice	k1gFnSc1	prejudice
<g/>
:	:	kIx,	:
A	A	kA	A
Latter-day	Latteraa	k1gMnSc2	Latter-daa
Comedy	Comeda	k1gMnSc2	Comeda
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Bride	Brid	k1gInSc5	Brid
and	and	k?	and
Prejudice	prejudic	k1gInSc2	prejudic
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
inspirovaná	inspirovaný	k2eAgFnSc1d1	inspirovaná
bollywoodská	bollywoodský	k2eAgFnSc1d1	bollywoodská
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
Anupam	Anupam	k1gInSc4	Anupam
Kher	Khera	k1gFnPc2	Khera
<g/>
,	,	kIx,	,
Aishwarya	Aishwary	k2eAgFnSc1d1	Aishwarya
Rai	Rai	k1gFnSc1	Rai
<g/>
,	,	kIx,	,
a	a	k8xC	a
Naveen	Naveen	k2eAgInSc1d1	Naveen
Andrews	Andrews	k1gInSc1	Andrews
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
promítáno	promítán	k2eAgNnSc4d1	promítáno
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Moje	můj	k3xOp1gNnSc1	můj
velká	velký	k2eAgFnSc1d1	velká
indická	indický	k2eAgFnSc1d1	indická
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
(	(	kIx(	(
<g/>
Pride	Prid	k1gInSc5	Prid
&	&	k?	&
Prejudice	prejudice	k1gFnPc1	prejudice
<g/>
)	)	kIx)	)
hrají	hrát	k5eAaImIp3nP	hrát
Keira	Keiro	k1gNnPc1	Keiro
Knightley	Knightlea	k1gFnSc2	Knightlea
a	a	k8xC	a
Matthew	Matthew	k1gFnSc2	Matthew
MacFadyen	MacFadyna	k1gFnPc2	MacFadyna
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
tradičně	tradičně	k6eAd1	tradičně
pojatá	pojatý	k2eAgFnSc1d1	pojatá
adaptace	adaptace	k1gFnSc1	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
pohledu	pohled	k1gInSc6	pohled
byl	být	k5eAaImAgInS	být
román	román	k1gInSc1	román
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
inspirací	inspirace	k1gFnPc2	inspirace
pro	pro	k7c4	pro
podobnými	podobný	k2eAgInPc7d1	podobný
tématy	téma	k1gNnPc7	téma
se	se	k3xPyFc4	se
zabývající	zabývající	k2eAgInSc1d1	zabývající
román	román	k1gInSc1	román
Deník	deník	k1gInSc1	deník
Bridget	Bridget	k1gInSc4	Bridget
Jonesové	Jonesový	k2eAgInPc4d1	Jonesový
od	od	k7c2	od
Helen	Helena	k1gFnPc2	Helena
Fieldingové	Fieldingový	k2eAgFnPc1d1	Fieldingová
<g/>
.	.	kIx.	.
</s>
<s>
Dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
i	i	k9	i
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
filmového	filmový	k2eAgNnSc2d1	filmové
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
hlavní	hlavní	k2eAgInSc4d1	hlavní
mužskou	mužský	k2eAgFnSc4d1	mužská
postavu	postava	k1gFnSc4	postava
Marka	Marek	k1gMnSc2	Marek
Darcyho	Darcy	k1gMnSc2	Darcy
hraje	hrát	k5eAaImIp3nS	hrát
Colin	Colin	k1gMnSc1	Colin
Firth	Firth	k1gMnSc1	Firth
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
pana	pan	k1gMnSc4	pan
Darcyho	Darcy	k1gMnSc4	Darcy
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
Pýchy	pýcha	k1gFnSc2	pýcha
a	a	k8xC	a
předsudku	předsudek	k1gInSc2	předsudek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Jane	Jan	k1gMnSc5	Jan
Austenová	Austenový	k2eAgFnSc1d1	Austenová
<g/>
:	:	kIx,	:
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1191	[number]	k4	1191
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
