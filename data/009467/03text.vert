<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lajsek	Lajsek	k?	Lajsek
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1925	[number]	k4	1925
Křesetice	Křesetika	k1gFnSc6	Křesetika
–	–	k?	–
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
designér	designér	k1gMnSc1	designér
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgInS	věnovat
se	se	k3xPyFc4	se
volné	volný	k2eAgFnSc3d1	volná
tvorbě	tvorba	k1gFnSc3	tvorba
(	(	kIx(	(
<g/>
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
grafika	grafika	k1gFnSc1	grafika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
užité	užitý	k2eAgFnSc3d1	užitá
grafice	grafika	k1gFnSc3	grafika
(	(	kIx(	(
<g/>
plakáty	plakát	k1gInPc1	plakát
<g/>
,	,	kIx,	,
poutače	poutač	k1gInPc1	poutač
<g/>
,	,	kIx,	,
propagační	propagační	k2eAgInPc1d1	propagační
katalogy	katalog	k1gInPc1	katalog
<g/>
,	,	kIx,	,
propagační	propagační	k2eAgFnPc1d1	propagační
brožury	brožura	k1gFnPc1	brožura
<g/>
)	)	kIx)	)
a	a	k8xC	a
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
seskupení	seskupení	k1gNnSc2	seskupení
Skupina	skupina	k1gFnSc1	skupina
osmi	osm	k4xCc2	osm
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nositelem	nositel	k1gMnSc7	nositel
státního	státní	k2eAgNnSc2d1	státní
vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
Za	za	k7c4	za
vynikající	vynikající	k2eAgFnSc4d1	vynikající
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
přibližně	přibližně	k6eAd1	přibližně
3000	[number]	k4	3000
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1800	[number]	k4	1800
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
sbírkách	sbírka	k1gFnPc6	sbírka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1925	[number]	k4	1925
v	v	k7c6	v
Křeseticích	Křesetik	k1gMnPc6	Křesetik
poblíž	poblíž	k6eAd1	poblíž
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
vychodil	vychodit	k5eAaImAgMnS	vychodit
obecnou	obecná	k1gFnSc4	obecná
i	i	k8xC	i
měšťanskou	měšťanský	k2eAgFnSc4d1	měšťanská
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c6	na
tamní	tamní	k2eAgFnSc6d1	tamní
Vyšší	vysoký	k2eAgFnSc6d2	vyšší
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
škole	škola	k1gFnSc6	škola
strojnické	strojnický	k2eAgFnSc6d1	strojnická
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
účastnil	účastnit	k5eAaImAgMnS	účastnit
odboje	odboj	k1gInSc2	odboj
proti	proti	k7c3	proti
nacistické	nacistický	k2eAgFnSc3d1	nacistická
okupaci	okupace	k1gFnSc3	okupace
v	v	k7c6	v
organizaci	organizace	k1gFnSc6	organizace
Pěst	pěst	k1gFnSc4	pěst
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
později	pozdě	k6eAd2	pozdě
členem	člen	k1gMnSc7	člen
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
natrvalo	natrvalo	k6eAd1	natrvalo
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rok	rok	k1gInSc4	rok
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
konstrukční	konstrukční	k2eAgFnSc6d1	konstrukční
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
ve	v	k7c6	v
Svazu	svaz	k1gInSc6	svaz
československého	československý	k2eAgInSc2d1	československý
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
University	universita	k1gFnSc2	universita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
konečně	konečně	k6eAd1	konečně
sblížil	sblížit	k5eAaPmAgInS	sblížit
s	s	k7c7	s
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
tamní	tamní	k2eAgFnSc3d1	tamní
Katedře	katedra	k1gFnSc3	katedra
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
výchovy	výchova	k1gFnSc2	výchova
a	a	k8xC	a
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
profesuru	profesura	k1gFnSc4	profesura
<g/>
.	.	kIx.	.
</s>
<s>
Učit	učit	k5eAaImF	učit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohl	moct	k5eAaImAgInS	moct
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
od	od	k7c2	od
slavných	slavný	k2eAgMnPc2d1	slavný
profesorů	profesor	k1gMnPc2	profesor
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
byli	být	k5eAaImAgMnP	být
např.	např.	kA	např.
Cyril	Cyril	k1gMnSc1	Cyril
Bouda	Bouda	k1gMnSc1	Bouda
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
Salcman	Salcman	k1gMnSc1	Salcman
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Lidický	lidický	k2eAgMnSc1d1	lidický
či	či	k8xC	či
Josef	Josef	k1gMnSc1	Josef
Sejpka	Sejpka	k1gFnSc1	Sejpka
<g/>
.	.	kIx.	.
</s>
<s>
Školu	škola	k1gFnSc4	škola
úspěšně	úspěšně	k6eAd1	úspěšně
dokončil	dokončit	k5eAaPmAgMnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Aprobaci	aprobace	k1gFnSc4	aprobace
navíc	navíc	k6eAd1	navíc
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
ještě	ještě	k9	ještě
o	o	k7c4	o
zeměpis	zeměpis	k1gInSc4	zeměpis
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
získal	získat	k5eAaPmAgInS	získat
inženýrský	inženýrský	k2eAgInSc1d1	inženýrský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malířství	malířství	k1gNnSc1	malířství
se	se	k3xPyFc4	se
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
věnoval	věnovat	k5eAaPmAgInS	věnovat
jen	jen	k9	jen
víceméně	víceméně	k9	víceméně
soukromě	soukromě	k6eAd1	soukromě
<g/>
.	.	kIx.	.
</s>
<s>
Prosazovat	prosazovat	k5eAaImF	prosazovat
se	se	k3xPyFc4	se
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
spolku	spolek	k1gInSc2	spolek
Štursa	Šturs	k1gMnSc2	Šturs
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
VI	VI	kA	VI
<g/>
.	.	kIx.	.
střediska	středisko	k1gNnSc2	středisko
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
i	i	k9	i
soutěže	soutěž	k1gFnPc1	soutěž
na	na	k7c4	na
úpravu	úprava	k1gFnSc4	úprava
prostoru	prostor	k1gInSc2	prostor
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
a	a	k8xC	a
Jiráskovské	Jiráskovský	k2eAgFnSc2d1	Jiráskovská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
Svazu	svaz	k1gInSc2	svaz
československých	československý	k2eAgMnPc2d1	československý
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
též	též	k9	též
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
Střední	střední	k2eAgFnSc6d1	střední
uměleckoprůmyslové	uměleckoprůmyslový	k2eAgFnSc6d1	uměleckoprůmyslová
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
Skupiny	skupina	k1gFnSc2	skupina
osmi	osm	k4xCc2	osm
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Programem	program	k1gInSc7	program
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
pořádání	pořádání	k1gNnSc1	pořádání
výtvarně	výtvarně	k6eAd1	výtvarně
výchovných	výchovný	k2eAgFnPc2d1	výchovná
akcí	akce	k1gFnPc2	akce
mimo	mimo	k7c4	mimo
zavedená	zavedený	k2eAgNnPc4d1	zavedené
kulturní	kulturní	k2eAgNnPc4d1	kulturní
střediska	středisko	k1gNnPc4	středisko
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
a	a	k8xC	a
v	v	k7c6	v
menších	malý	k2eAgNnPc6d2	menší
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
hledání	hledání	k1gNnSc1	hledání
nových	nový	k2eAgFnPc2d1	nová
možností	možnost	k1gFnPc2	možnost
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
výstav	výstava	k1gFnPc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
navíc	navíc	k6eAd1	navíc
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
propůjčeno	propůjčen	k2eAgNnSc1d1	propůjčeno
státní	státní	k2eAgNnSc1d1	státní
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
Za	za	k7c4	za
vynikající	vynikající	k2eAgFnSc4d1	vynikající
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
76	[number]	k4	76
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lajsek	Lajsek	k?	Lajsek
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
poměrně	poměrně	k6eAd1	poměrně
velkou	velký	k2eAgFnSc7d1	velká
žánrovou	žánrový	k2eAgFnSc7d1	žánrová
všestranností	všestrannost	k1gFnSc7	všestrannost
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
jeho	jeho	k3xOp3gNnSc7	jeho
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
realismus	realismus	k1gInSc1	realismus
nebo	nebo	k8xC	nebo
květinová	květinový	k2eAgNnPc1d1	květinové
zátiší	zátiší	k1gNnPc1	zátiší
<g/>
.	.	kIx.	.
</s>
<s>
Surrealistické	surrealistický	k2eAgFnSc3d1	surrealistická
malbě	malba	k1gFnSc3	malba
či	či	k8xC	či
sakrálním	sakrální	k2eAgInPc3d1	sakrální
motivům	motiv	k1gInPc3	motiv
se	se	k3xPyFc4	se
už	už	k6eAd1	už
věnoval	věnovat	k5eAaPmAgMnS	věnovat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Bezesporu	bezesporu	k9	bezesporu
nejvíce	nejvíce	k6eAd1	nejvíce
se	se	k3xPyFc4	se
však	však	k9	však
prosadil	prosadit	k5eAaPmAgMnS	prosadit
díky	díky	k7c3	díky
krajinomalbě	krajinomalba	k1gFnSc3	krajinomalba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
dokonce	dokonce	k9	dokonce
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
tvůrce	tvůrce	k1gMnPc4	tvůrce
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Maloval	malovat	k5eAaImAgMnS	malovat
olejomalbou	olejomalba	k1gFnSc7	olejomalba
<g/>
,	,	kIx,	,
grafikou	grafika	k1gFnSc7	grafika
<g/>
,	,	kIx,	,
temperou	tempera	k1gFnSc7	tempera
i	i	k8xC	i
kombinovanou	kombinovaný	k2eAgFnSc7d1	kombinovaná
technikou	technika	k1gFnSc7	technika
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
i	i	k8xC	i
na	na	k7c4	na
papír	papír	k1gInSc4	papír
<g/>
.	.	kIx.	.
</s>
<s>
Mimoto	mimoto	k6eAd1	mimoto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
také	také	k9	také
užité	užitý	k2eAgFnSc3d1	užitá
grafice	grafika	k1gFnSc3	grafika
a	a	k8xC	a
designérské	designérský	k2eAgFnSc3d1	designérská
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
===	===	k?	===
</s>
</p>
<p>
<s>
Krajinomalba	krajinomalba	k1gFnSc1	krajinomalba
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
a	a	k8xC	a
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
součástí	součást	k1gFnSc7	součást
Lajskovy	Lajskův	k2eAgFnSc2d1	Lajskův
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Inspiraci	inspirace	k1gFnSc4	inspirace
čerpal	čerpat	k5eAaImAgInS	čerpat
zejména	zejména	k9	zejména
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
–	–	k?	–
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
kutnohorském	kutnohorský	k2eAgInSc6d1	kutnohorský
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
maloval	malovat	k5eAaImAgMnS	malovat
krajiny	krajina	k1gFnPc4	krajina
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
hory	hora	k1gFnSc2	hora
Říp	Říp	k1gInSc1	Říp
<g/>
,	,	kIx,	,
z	z	k7c2	z
Polabí	Polabí	k1gNnSc2	Polabí
a	a	k8xC	a
z	z	k7c2	z
Českého	český	k2eAgNnSc2d1	české
středohoří	středohoří	k1gNnSc2	středohoří
<g/>
.	.	kIx.	.
</s>
<s>
Četná	četný	k2eAgNnPc1d1	četné
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
díla	dílo	k1gNnPc1	dílo
z	z	k7c2	z
pražského	pražský	k2eAgNnSc2d1	Pražské
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
součástí	součást	k1gFnSc7	součást
autorovy	autorův	k2eAgFnSc2d1	autorova
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
jeho	jeho	k3xOp3gInPc4	jeho
obrazy	obraz	k1gInPc4	obraz
ze	z	k7c2	z
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
učarovaly	učarovat	k5eAaPmAgFnP	učarovat
např.	např.	kA	např.
architektonické	architektonický	k2eAgFnSc2d1	architektonická
vykopávky	vykopávka	k1gFnSc2	vykopávka
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Delf	Delfy	k1gFnPc2	Delfy
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
scenérie	scenérie	k1gFnSc1	scenérie
z	z	k7c2	z
četných	četný	k2eAgFnPc2d1	četná
cest	cesta	k1gFnPc2	cesta
do	do	k7c2	do
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Lajska	Lajska	k?	Lajska
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
díky	díky	k7c3	díky
osobitému	osobitý	k2eAgInSc3d1	osobitý
uměleckému	umělecký	k2eAgInSc3d1	umělecký
rukopisu	rukopis	k1gInSc3	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
pevná	pevný	k2eAgFnSc1d1	pevná
výstavba	výstavba	k1gFnSc1	výstavba
kompozice	kompozice	k1gFnSc2	kompozice
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
mírné	mírný	k2eAgNnSc1d1	mírné
abstrahování	abstrahování	k1gNnSc1	abstrahování
<g/>
.	.	kIx.	.
</s>
<s>
Všímal	všímat	k5eAaImAgInS	všímat
si	se	k3xPyFc3	se
především	především	k6eAd1	především
geologického	geologický	k2eAgNnSc2d1	geologické
a	a	k8xC	a
tektonického	tektonický	k2eAgNnSc2d1	tektonické
utváření	utváření	k1gNnSc2	utváření
terénu	terén	k1gInSc2	terén
a	a	k8xC	a
nalézal	nalézat	k5eAaImAgInS	nalézat
vnější	vnější	k2eAgFnSc4d1	vnější
vzhledovou	vzhledový	k2eAgFnSc4d1	vzhledová
charakteristiku	charakteristika	k1gFnSc4	charakteristika
i	i	k8xC	i
trvalou	trvalý	k2eAgFnSc4d1	trvalá
podstatu	podstata	k1gFnSc4	podstata
<g/>
,	,	kIx,	,
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
však	však	k9	však
nebránil	bránit	k5eNaImAgInS	bránit
ani	ani	k8xC	ani
volnějšímu	volný	k2eAgInSc3d2	volnější
koncepčnímu	koncepční	k2eAgInSc3d1	koncepční
<g/>
,	,	kIx,	,
kompozičnímu	kompoziční	k2eAgMnSc3d1	kompoziční
i	i	k8xC	i
rukopisnému	rukopisný	k2eAgInSc3d1	rukopisný
rozletu	rozlet	k1gInSc3	rozlet
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c6	na
zákonitosti	zákonitost	k1gFnSc6	zákonitost
výtvarného	výtvarný	k2eAgInSc2d1	výtvarný
projevu	projev	k1gInSc2	projev
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
rozmyslu	rozmysl	k1gInSc2	rozmysl
<g/>
,	,	kIx,	,
citu	cit	k1gInSc2	cit
<g/>
,	,	kIx,	,
představivosti	představivost	k1gFnSc2	představivost
a	a	k8xC	a
zvolené	zvolený	k2eAgFnSc2d1	zvolená
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
dávaly	dávat	k5eAaImAgFnP	dávat
větší	veliký	k2eAgFnSc4d2	veliký
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
rozvinutí	rozvinutí	k1gNnSc3	rozvinutí
vlastní	vlastní	k2eAgFnSc2d1	vlastní
fantazie	fantazie	k1gFnSc2	fantazie
při	při	k7c6	při
výtvarné	výtvarný	k2eAgFnSc6d1	výtvarná
výstavbě	výstavba	k1gFnSc6	výstavba
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
svébytného	svébytný	k2eAgNnSc2d1	svébytné
pojetí	pojetí	k1gNnSc2	pojetí
a	a	k8xC	a
podání	podání	k1gNnSc2	podání
vlastních	vlastní	k2eAgInPc2d1	vlastní
citových	citový	k2eAgInPc2d1	citový
zážitků	zážitek	k1gInPc2	zážitek
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
ceněn	ceněn	k2eAgInSc1d1	ceněn
i	i	k8xC	i
jeho	on	k3xPp3gInSc4	on
vyzrálý	vyzrálý	k2eAgInSc4d1	vyzrálý
umělecký	umělecký	k2eAgInSc4d1	umělecký
profil	profil	k1gInSc4	profil
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
proměnlivou	proměnlivý	k2eAgFnSc4d1	proměnlivá
působivost	působivost	k1gFnSc4	působivost
okamžiku	okamžik	k1gInSc2	okamžik
i	i	k9	i
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
duchovní	duchovní	k2eAgFnSc4d1	duchovní
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
abstraktní	abstraktní	k2eAgFnSc2d1	abstraktní
tvorby	tvorba	k1gFnSc2	tvorba
v	v	k7c6	v
krajinomalbě	krajinomalba	k1gFnSc6	krajinomalba
převažují	převažovat	k5eAaImIp3nP	převažovat
spíše	spíše	k9	spíše
svěží	svěží	k2eAgFnPc1d1	svěží
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
optimističtějšímu	optimistický	k2eAgInSc3d2	optimističtější
dojmu	dojem	k1gInSc3	dojem
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
přednesu	přednes	k1gInSc6	přednes
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
spíše	spíše	k9	spíše
poetickému	poetický	k2eAgNnSc3d1	poetické
ladění	ladění	k1gNnSc3	ladění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autorovým	autorův	k2eAgInSc7d1	autorův
častým	častý	k2eAgInSc7d1	častý
námětem	námět	k1gInSc7	námět
bylo	být	k5eAaImAgNnS	být
zejména	zejména	k9	zejména
téma	téma	k1gNnSc4	téma
čtyř	čtyři	k4xCgFnPc2	čtyři
ročních	roční	k2eAgFnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
velmi	velmi	k6eAd1	velmi
imponovala	imponovat	k5eAaImAgFnS	imponovat
proměna	proměna	k1gFnSc1	proměna
krajiny	krajina	k1gFnSc2	krajina
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
čtveřice	čtveřice	k1gFnSc1	čtveřice
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
Jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
Léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
Podzim	podzim	k1gInSc1	podzim
a	a	k8xC	a
Zima	zima	k1gFnSc1	zima
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnPc4d3	nejznámější
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
kupříkladu	kupříkladu	k6eAd1	kupříkladu
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Hořící	hořící	k2eAgInPc1d1	hořící
Lidice	Lidice	k1gInPc1	Lidice
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Akropolis	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Červená	červený	k2eAgFnSc1d1	červená
krajina	krajina	k1gFnSc1	krajina
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
Léto	léto	k1gNnSc1	léto
<g/>
,	,	kIx,	,
Podzim	podzim	k1gInSc1	podzim
<g/>
,	,	kIx,	,
Zima	zima	k1gFnSc1	zima
(	(	kIx(	(
<g/>
čtveřice	čtveřice	k1gFnSc1	čtveřice
obrazů	obraz	k1gInPc2	obraz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Abstrakce	abstrakce	k1gFnSc2	abstrakce
===	===	k?	===
</s>
</p>
<p>
<s>
Abstraktní	abstraktní	k2eAgFnSc3d1	abstraktní
malbě	malba	k1gFnSc3	malba
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
období	období	k1gNnSc6	období
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Převládají	převládat	k5eAaImIp3nP	převládat
zde	zde	k6eAd1	zde
zejména	zejména	k9	zejména
deskové	deskový	k2eAgFnPc1d1	desková
malby	malba	k1gFnPc1	malba
tmavých	tmavý	k2eAgFnPc2d1	tmavá
barev	barva	k1gFnPc2	barva
namodralých	namodralý	k2eAgInPc2d1	namodralý
tónů	tón	k1gInPc2	tón
<g/>
,	,	kIx,	,
neurčitých	určitý	k2eNgFnPc2d1	neurčitá
kontur	kontura	k1gFnPc2	kontura
a	a	k8xC	a
meditativního	meditativní	k2eAgNnSc2d1	meditativní
ladění	ladění	k1gNnSc2	ladění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
mnohdy	mnohdy	k6eAd1	mnohdy
přecházejí	přecházet	k5eAaImIp3nP	přecházet
k	k	k7c3	k
expresivní	expresivní	k2eAgFnSc3d1	expresivní
dramatičnosti	dramatičnost	k1gFnSc3	dramatičnost
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
však	však	k9	však
i	i	k9	i
četné	četný	k2eAgInPc4d1	četný
obrazy	obraz	k1gInPc4	obraz
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgNnPc6	jenž
dominují	dominovat	k5eAaImIp3nP	dominovat
světlejší	světlý	k2eAgInPc1d2	světlejší
odstíny	odstín	k1gInPc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
z	z	k7c2	z
umělcova	umělcův	k2eAgNnSc2d1	umělcovo
abstraktního	abstraktní	k2eAgNnSc2d1	abstraktní
období	období	k1gNnSc2	období
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
krajinomalby	krajinomalba	k1gFnSc2	krajinomalba
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
zejména	zejména	k9	zejména
v	v	k7c6	v
porevoluční	porevoluční	k2eAgFnSc6d1	porevoluční
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významná	významný	k2eAgNnPc4d1	významné
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Zrcadlo	zrcadlo	k1gNnSc1	zrcadlo
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgInPc4d1	ostatní
žánry	žánr	k1gInPc4	žánr
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
rozsáhlejší	rozsáhlý	k2eAgInPc4d2	rozsáhlejší
žánry	žánr	k1gInPc4	žánr
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
realismus	realismus	k1gInSc1	realismus
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yIgInSc4	který
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
např.	např.	kA	např.
v	v	k7c6	v
pražských	pražský	k2eAgFnPc6d1	Pražská
ulicích	ulice	k1gFnPc6	ulice
či	či	k8xC	či
ve	v	k7c6	v
venkovských	venkovský	k2eAgFnPc6d1	venkovská
obcích	obec	k1gFnPc6	obec
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
též	též	k6eAd1	též
malbě	malba	k1gFnSc3	malba
květin	květina	k1gFnPc2	květina
a	a	k8xC	a
grafice	grafika	k1gFnSc3	grafika
<g/>
.	.	kIx.	.
</s>
<s>
Autorova	autorův	k2eAgFnSc1d1	autorova
surrealistická	surrealistický	k2eAgFnSc1d1	surrealistická
tvorba	tvorba	k1gFnSc1	tvorba
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
objevují	objevovat	k5eAaImIp3nP	objevovat
sakrální	sakrální	k2eAgInPc4d1	sakrální
motivy	motiv	k1gInPc4	motiv
či	či	k8xC	či
výjevy	výjev	k1gInPc4	výjev
ze	z	k7c2	z
snu	sen	k1gInSc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
se	se	k3xPyFc4	se
také	také	k9	také
krátce	krátce	k6eAd1	krátce
věnoval	věnovat	k5eAaPmAgMnS	věnovat
ikonomalbě	ikonomalba	k1gFnSc3	ikonomalba
<g/>
.	.	kIx.	.
</s>
<s>
Lidi	člověk	k1gMnPc4	člověk
maloval	malovat	k5eAaImAgMnS	malovat
jen	jen	k6eAd1	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Realismus	realismus	k1gInSc1	realismus
</s>
</p>
<p>
<s>
Ráno	ráno	k6eAd1	ráno
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Květy	květ	k1gInPc1	květ
</s>
</p>
<p>
<s>
Slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Surrealismus	surrealismus	k1gInSc1	surrealismus
</s>
</p>
<p>
<s>
Smutek	smutek	k1gInSc1	smutek
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ikonomalba	Ikonomalba	k1gFnSc1	Ikonomalba
</s>
</p>
<p>
<s>
Ukřižování	ukřižování	k1gNnSc1	ukřižování
Páně	páně	k2eAgInPc1d1	páně
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgFnSc2d1	významná
realizace	realizace	k1gFnSc2	realizace
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Lajska	Lajska	k?	Lajska
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
několik	několik	k4yIc1	několik
významných	významný	k2eAgFnPc2d1	významná
architektonických	architektonický	k2eAgFnPc2d1	architektonická
či	či	k8xC	či
designérských	designérský	k2eAgFnPc2d1	designérská
prací	práce	k1gFnPc2	práce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hala	hala	k1gFnSc1	hala
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
v	v	k7c6	v
Praze-Suchdole	Praze-Suchdola	k1gFnSc6	Praze-Suchdola
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
celkové	celkový	k2eAgNnSc4d1	celkové
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
řešení	řešení	k1gNnSc4	řešení
muzea	muzeum	k1gNnSc2	muzeum
letectví	letectví	k1gNnSc2	letectví
v	v	k7c6	v
Praze-Kbelích	Praze-Kbel	k1gInPc6	Praze-Kbel
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
hala	hala	k1gFnSc1	hala
Ústavu	ústav	k1gInSc2	ústav
odborného	odborný	k2eAgNnSc2d1	odborné
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skleněná	skleněný	k2eAgFnSc1d1	skleněná
stěna	stěna	k1gFnSc1	stěna
v	v	k7c6	v
hotelu	hotel	k1gInSc6	hotel
"	"	kIx"	"
<g/>
Brčálník	Brčálník	k1gMnSc1	Brčálník
<g/>
"	"	kIx"	"
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
řešení	řešení	k1gNnSc1	řešení
prostor	prostora	k1gFnPc2	prostora
v	v	k7c6	v
Kulturním	kulturní	k2eAgInSc6d1	kulturní
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Děčíně	Děčín	k1gInSc6	Děčín
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skleněná	skleněný	k2eAgFnSc1d1	skleněná
stěna	stěna	k1gFnSc1	stěna
ve	v	k7c6	v
správní	správní	k2eAgFnSc6d1	správní
budově	budova	k1gFnSc6	budova
ČKD	ČKD	kA	ČKD
Kompresory	kompresor	k1gInPc4	kompresor
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skleněná	skleněný	k2eAgFnSc1d1	skleněná
stěna	stěna	k1gFnSc1	stěna
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Praze-Tróji	Praze-Trój	k1gFnSc6	Praze-Trój
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
řešení	řešení	k1gNnSc1	řešení
haly	hala	k1gFnSc2	hala
ZPA	ZPA	kA	ZPA
v	v	k7c6	v
Praze-Nuslích	Praze-Nusl	k1gInPc6	Praze-Nusl
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skleněná	skleněný	k2eAgFnSc1d1	skleněná
stěna	stěna	k1gFnSc1	stěna
a	a	k8xC	a
výtvarné	výtvarný	k2eAgInPc1d1	výtvarný
doplňky	doplněk	k1gInPc1	doplněk
v	v	k7c6	v
interhotelu	interhotel	k1gInSc6	interhotel
Zlatý	zlatý	k2eAgMnSc1d1	zlatý
lev	lev	k1gMnSc1	lev
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
výtvarné	výtvarný	k2eAgInPc4d1	výtvarný
doplňky	doplněk	k1gInPc4	doplněk
pro	pro	k7c4	pro
svatební	svatební	k2eAgFnSc4d1	svatební
síň	síň	k1gFnSc4	síň
v	v	k7c6	v
Benešově	Benešov	k1gInSc6	Benešov
nad	nad	k7c7	nad
Ploučnicí	Ploučnice	k1gFnSc7	Ploučnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
výstavy	výstava	k1gFnPc1	výstava
==	==	k?	==
</s>
</p>
<p>
<s>
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
-	-	kIx~	-
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1957	[number]	k4	1957
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
-	-	kIx~	-
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Komorní	komorní	k2eAgNnSc1d1	komorní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
projektový	projektový	k2eAgInSc1d1	projektový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
Kostelní	kostelní	k2eAgFnSc1d1	kostelní
ulice	ulice	k1gFnSc1	ulice
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
grafika	grafika	k1gFnSc1	grafika
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgInSc1d1	ústřední
klub	klub	k1gInSc1	klub
školství	školství	k1gNnSc2	školství
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
Na	na	k7c6	na
Příkopě	příkop	k1gInSc6	příkop
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
Díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
Vodičkova	Vodičkův	k2eAgFnSc1d1	Vodičkova
ulice	ulice	k1gFnSc1	ulice
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
-	-	kIx~	-
Benešov	Benešov	k1gInSc1	Benešov
nad	nad	k7c7	nad
Ploučnicí	Ploučnice	k1gFnSc7	Ploučnice
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
-	-	kIx~	-
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
-	-	kIx~	-
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
v	v	k7c6	v
ul	ul	kA	ul
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Senovážné	Senovážný	k2eAgNnSc1d1	Senovážné
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
-	-	kIx~	-
Kutná	kutný	k2eAgFnSc1d1	Kutná
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Sankturinovský	Sankturinovský	k2eAgInSc1d1	Sankturinovský
dům	dům	k1gInSc1	dům
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Přehlídky	přehlídka	k1gFnPc1	přehlídka
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Jízdárně	jízdárna	k1gFnSc6	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgInPc1d1	pražský
salóny	salón	k1gInPc1	salón
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Kolektivní	kolektivní	k2eAgFnPc1d1	kolektivní
výstavy	výstava	k1gFnPc1	výstava
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
Vincence	Vincenc	k1gMnSc4	Vincenc
Kramáře	kramář	k1gMnSc4	kramář
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
výstavě	výstava	k1gFnSc6	výstava
Vyznání	vyznání	k1gNnSc2	vyznání
životu	život	k1gInSc2	život
a	a	k8xC	a
míru	mír	k1gInSc2	mír
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samostatné	samostatný	k2eAgFnPc1d1	samostatná
výstavy	výstava	k1gFnPc1	výstava
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lodž	Lodž	k1gFnSc1	Lodž
<g/>
,	,	kIx,	,
Katovice	Katovice	k1gFnPc1	Katovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
výstava	výstava	k1gFnSc1	výstava
grafiků	grafik	k1gMnPc2	grafik
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Boučková	Boučková	k1gFnSc1	Boučková
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Nové	Nové	k2eAgFnPc1d1	Nové
tendence	tendence	k1gFnPc1	tendence
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
mladých	mladý	k2eAgMnPc2d1	mladý
českých	český	k2eAgMnPc2d1	český
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
:	:	kIx,	:
Východočeská	východočeský	k2eAgFnSc1d1	Východočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boučková	Boučková	k1gFnSc1	Boučková
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Soudobá	soudobý	k2eAgFnSc1d1	soudobá
česká	český	k2eAgFnSc1d1	Česká
krajina	krajina	k1gFnSc1	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
:	:	kIx,	:
Východočeská	východočeský	k2eAgFnSc1d1	Východočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlaváček	Hlaváček	k1gMnSc1	Hlaváček
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
8	[number]	k4	8
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
ČFVU	ČFVU	kA	ČFVU
–	–	k?	–
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
fond	fond	k1gInSc1	fond
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
F	F	kA	F
151350	[number]	k4	151350
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vinter	Vinter	k1gMnSc1	Vinter
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Krajiny	Krajina	k1gFnSc2	Krajina
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Lajska	Lajska	k?	Lajska
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
,	,	kIx,	,
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
5649	[number]	k4	5649
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vinter	Vinter	k1gMnSc1	Vinter
<g/>
,	,	kIx,	,
V.	V.	kA	V.
<g/>
:	:	kIx,	:
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lajsek	Lajsek	k?	Lajsek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svaz	svaz	k1gInSc1	svaz
českých	český	k2eAgMnPc2d1	český
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Štorkán	Štorkán	k1gMnSc1	Štorkán
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Oldřich	Oldřich	k1gMnSc1	Oldřich
Lajsek	Lajsek	k?	Lajsek
–	–	k?	–
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Podnik	podnik	k1gInSc1	podnik
českého	český	k2eAgInSc2d1	český
fondu	fond	k1gInSc2	fond
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgFnSc2d1	osobní
stránky	stránka	k1gFnSc2	stránka
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Lajska	Lajska	k?	Lajska
<g/>
:	:	kIx,	:
http://lajsek.cz/	[url]	k?	http://lajsek.cz/
</s>
</p>
<p>
<s>
Signatury	signatura	k1gFnPc1	signatura
malířů	malíř	k1gMnPc2	malíř
<g/>
:	:	kIx,	:
http://www.signaturymaliru.cz/painter/601.php	[url]	k1gMnSc1	http://www.signaturymaliru.cz/painter/601.php
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Lajsek	Lajsek	k?	Lajsek
Oldřich	Oldřich	k1gMnSc1	Oldřich
</s>
</p>
