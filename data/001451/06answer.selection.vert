<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1686	[number]	k4	1686
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
Newton	Newton	k1gMnSc1	Newton
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
první	první	k4xOgFnSc2	první
knihu	kniha	k1gFnSc4	kniha
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
Principia	principium	k1gNnSc2	principium
Mathematica	Mathematicum	k1gNnSc2	Mathematicum
<g/>
,	,	kIx,	,
Hooke	Hooke	k1gInSc4	Hooke
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
a	a	k8xC	a
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
myšlenky	myšlenka	k1gFnPc4	myšlenka
převzal	převzít	k5eAaPmAgMnS	převzít
Newton	Newton	k1gMnSc1	Newton
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
