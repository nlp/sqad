<s>
Paracetamol	paracetamol	k1gInSc1	paracetamol
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
acetaminofen	acetaminofen	k1gInSc1	acetaminofen
<g/>
,	,	kIx,	,
systematický	systematický	k2eAgInSc1d1	systematický
název	název	k1gInSc1	název
N-	N-	k1gFnSc2	N-
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
-hydroxyfenyl	ydroxyfenyl	k1gInSc1	-hydroxyfenyl
<g/>
)	)	kIx)	)
<g/>
ethanamid	ethanamid	k1gInSc1	ethanamid
<g/>
,	,	kIx,	,
též	též	k9	též
N-	N-	k1gFnSc1	N-
<g/>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
-hydroxyfenyl	ydroxyfenyl	k1gInSc1	-hydroxyfenyl
<g/>
)	)	kIx)	)
<g/>
acetamid	acetamid	k1gInSc1	acetamid
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
léčivo	léčivo	k1gNnSc1	léčivo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
bolestem	bolest	k1gFnPc3	bolest
a	a	k8xC	a
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
tělesné	tělesný	k2eAgFnSc6d1	tělesná
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
protizánětlivé	protizánětlivý	k2eAgNnSc1d1	protizánětlivé
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
analgetikum	analgetikum	k1gNnSc4	analgetikum
a	a	k8xC	a
antipyretikum	antipyretikum	k1gNnSc4	antipyretikum
<g/>
.	.	kIx.	.
</s>
<s>
Tlumí	tlumit	k5eAaImIp3nP	tlumit
také	také	k9	také
emoce	emoce	k1gFnPc1	emoce
-	-	kIx~	-
jak	jak	k6eAd1	jak
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Paracetamol	paracetamol	k1gInSc1	paracetamol
dobře	dobře	k6eAd1	dobře
působí	působit	k5eAaImIp3nS	působit
proti	proti	k7c3	proti
bolesti	bolest	k1gFnSc3	bolest
a	a	k8xC	a
vysoké	vysoký	k2eAgFnSc3d1	vysoká
teplotě	teplota	k1gFnSc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
při	při	k7c6	při
bolestech	bolest	k1gFnPc6	bolest
různého	různý	k2eAgInSc2d1	různý
původu	původ	k1gInSc2	původ
a	a	k8xC	a
při	při	k7c6	při
zvýšené	zvýšený	k2eAgFnSc6d1	zvýšená
teplotě	teplota	k1gFnSc6	teplota
především	především	k9	především
u	u	k7c2	u
viróz	viróza	k1gFnPc2	viróza
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
dobře	dobře	k6eAd1	dobře
tolerován	tolerován	k2eAgInSc1d1	tolerován
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
šetrný	šetrný	k2eAgInSc1d1	šetrný
k	k	k7c3	k
žaludku	žaludek	k1gInSc3	žaludek
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
ho	on	k3xPp3gMnSc4	on
užívat	užívat	k5eAaImF	užívat
již	již	k6eAd1	již
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
terapii	terapie	k1gFnSc6	terapie
se	se	k3xPyFc4	se
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
konzumovat	konzumovat	k5eAaBmF	konzumovat
alkoholické	alkoholický	k2eAgInPc4d1	alkoholický
nápoje	nápoj	k1gInPc4	nápoj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
látkou	látka	k1gFnSc7	látka
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vznik	vznik	k1gInSc4	vznik
toxinů	toxin	k1gInPc2	toxin
v	v	k7c6	v
játrech	játra	k1gNnPc6	játra
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
reakci	reakce	k1gFnSc4	reakce
jako	jako	k8xC	jako
alkohol	alkohol	k1gInSc4	alkohol
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
i	i	k9	i
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
kofeinu	kofein	k1gInSc2	kofein
(	(	kIx(	(
<g/>
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tablety	tableta	k1gFnSc2	tableta
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
užívat	užívat	k5eAaImF	užívat
po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
mechanismem	mechanismus	k1gInSc7	mechanismus
účinku	účinek	k1gInSc2	účinek
paracetamolu	paracetamol	k1gInSc2	paracetamol
je	být	k5eAaImIp3nS	být
inhibice	inhibice	k1gFnSc1	inhibice
COX-2	COX-2	k1gFnSc2	COX-2
(	(	kIx(	(
<g/>
cyklooxygenázy	cyklooxygenáza	k1gFnSc2	cyklooxygenáza
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zánětu	zánět	k1gInSc2	zánět
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vznik	vznik	k1gInSc1	vznik
prostaglandinů	prostaglandin	k1gInPc2	prostaglandin
<g/>
,	,	kIx,	,
parakrinních	parakrinní	k2eAgInPc2d1	parakrinní
hormonů	hormon	k1gInPc2	hormon
působících	působící	k2eAgInPc2d1	působící
jako	jako	k8xC	jako
lokální	lokální	k2eAgFnPc4d1	lokální
signální	signální	k2eAgFnPc4d1	signální
molekuly	molekula	k1gFnPc4	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
mediátory	mediátor	k1gInPc1	mediátor
bolesti	bolest	k1gFnSc2	bolest
<g/>
,	,	kIx,	,
horečky	horečka	k1gFnSc2	horečka
a	a	k8xC	a
zánětu	zánět	k1gInSc2	zánět
<g/>
.	.	kIx.	.
</s>
<s>
Snížení	snížení	k1gNnSc1	snížení
hladiny	hladina	k1gFnSc2	hladina
prostaglandinů	prostaglandin	k1gMnPc2	prostaglandin
v	v	k7c6	v
hypotalamu	hypotalamus	k1gInSc6	hypotalamus
má	mít	k5eAaImIp3nS	mít
antipyretický	antipyretický	k2eAgInSc4d1	antipyretický
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímo	přímo	k6eNd1	přímo
působí	působit	k5eAaImIp3nS	působit
paracetamol	paracetamol	k1gInSc4	paracetamol
také	také	k9	také
na	na	k7c4	na
serotoninové	serotoninový	k2eAgInPc4d1	serotoninový
receptory	receptor	k1gInPc4	receptor
v	v	k7c6	v
míše	mícha	k1gFnSc6	mícha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podporuje	podporovat	k5eAaImIp3nS	podporovat
jeho	on	k3xPp3gInSc4	on
analgetický	analgetický	k2eAgInSc4d1	analgetický
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Nástup	nástup	k1gInSc1	nástup
účinku	účinek	k1gInSc2	účinek
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
od	od	k7c2	od
požití	požití	k1gNnSc2	požití
<g/>
.	.	kIx.	.
</s>
<s>
Paracetamol	paracetamol	k1gInSc1	paracetamol
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
dávkách	dávka	k1gFnPc6	dávka
a	a	k8xC	a
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
hepatotoxický	hepatotoxický	k2eAgMnSc1d1	hepatotoxický
(	(	kIx(	(
<g/>
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
pro	pro	k7c4	pro
játra	játra	k1gNnPc4	játra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
dávkách	dávka	k1gFnPc6	dávka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
alergická	alergický	k2eAgFnSc1d1	alergická
kožní	kožní	k2eAgFnSc1d1	kožní
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
antidotem	antidot	k1gMnSc7	antidot
je	být	k5eAaImIp3nS	být
acetylcystein	acetylcystein	k1gMnSc1	acetylcystein
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ovšem	ovšem	k9	ovšem
neprochází	procházet	k5eNaImIp3nS	procházet
přes	přes	k7c4	přes
placentární	placentární	k2eAgFnSc4d1	placentární
bariéru	bariéra	k1gFnSc4	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
v	v	k7c6	v
případě	případ	k1gInSc6	případ
předávkování	předávkování	k1gNnSc2	předávkování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
selhání	selhání	k1gNnSc3	selhání
jater	játra	k1gNnPc2	játra
u	u	k7c2	u
matky	matka	k1gFnSc2	matka
i	i	k8xC	i
plodu	plod	k1gInSc2	plod
<g/>
.	.	kIx.	.
</s>
<s>
Acetylcystein	Acetylcystein	k1gInSc1	Acetylcystein
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
játrům	játra	k1gNnPc3	játra
plodu	plod	k1gInSc3	plod
a	a	k8xC	a
chránit	chránit	k5eAaImF	chránit
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Otrava	otrava	k1gFnSc1	otrava
způsobená	způsobený	k2eAgFnSc1d1	způsobená
předávkováním	předávkování	k1gNnSc7	předávkování
paracetamolem	paracetamol	k1gInSc7	paracetamol
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgFnPc2d3	nejčastější
příčin	příčina	k1gFnPc2	příčina
akutního	akutní	k2eAgNnSc2d1	akutní
selhání	selhání	k1gNnSc3	selhání
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Pacient	pacient	k1gMnSc1	pacient
do	do	k7c2	do
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
ho	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
pouze	pouze	k6eAd1	pouze
rychlou	rychlý	k2eAgFnSc7d1	rychlá
transplantací	transplantace	k1gFnSc7	transplantace
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgFnPc1d1	experimentální
studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
paracetamol	paracetamol	k1gInSc1	paracetamol
tlumí	tlumit	k5eAaImIp3nS	tlumit
nejen	nejen	k6eAd1	nejen
bolest	bolest	k1gFnSc4	bolest
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
negativní	negativní	k2eAgMnSc1d1	negativní
i	i	k8xC	i
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Laicky	laicky	k6eAd1	laicky
řečeno	říct	k5eAaPmNgNnS	říct
lidé	člověk	k1gMnPc1	člověk
vnímají	vnímat	k5eAaImIp3nP	vnímat
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
paracetamolu	paracetamol	k1gInSc2	paracetamol
radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
smutek	smutek	k1gInSc1	smutek
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
emoce	emoce	k1gFnPc1	emoce
méně	málo	k6eAd2	málo
výrazně	výrazně	k6eAd1	výrazně
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
mechanismem	mechanismus	k1gInSc7	mechanismus
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
emocí	emoce	k1gFnPc2	emoce
dochází	docházet	k5eAaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
také	také	k9	také
snížení	snížení	k1gNnSc1	snížení
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Prodává	prodávat	k5eAaImIp3nS	prodávat
se	se	k3xPyFc4	se
jako	jako	k9	jako
nepotahované	potahovaný	k2eNgFnPc1d1	potahovaný
bílé	bílý	k2eAgFnPc1d1	bílá
tablety	tableta	k1gFnPc1	tableta
a	a	k8xC	a
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
čípků	čípek	k1gInPc2	čípek
a	a	k8xC	a
suspenze	suspenze	k1gFnSc2	suspenze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
také	také	k9	také
jako	jako	k8xS	jako
infúze	infúze	k1gFnSc1	infúze
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Perfalgan	Perfalgana	k1gFnPc2	Perfalgana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
lék	lék	k1gInSc1	lék
schválen	schválit	k5eAaPmNgInS	schválit
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
používán	používán	k2eAgInSc1d1	používán
<g/>
,	,	kIx,	,
zakoupit	zakoupit	k5eAaPmF	zakoupit
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
i	i	k9	i
bez	bez	k7c2	bez
lékařského	lékařský	k2eAgInSc2d1	lékařský
předpisu	předpis	k1gInSc2	předpis
v	v	k7c6	v
lékárně	lékárna	k1gFnSc6	lékárna
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Paralen	paralen	k1gInSc1	paralen
<g/>
,	,	kIx,	,
výrobcem	výrobce	k1gMnSc7	výrobce
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Zentiva	Zentiva	k1gFnSc1	Zentiva
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
léky	lék	k1gInPc7	lék
obsahujícími	obsahující	k2eAgInPc7d1	obsahující
paracetamol	paracetamol	k1gInSc4	paracetamol
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
Panadol	Panadol	k1gInSc1	Panadol
<g/>
,	,	kIx,	,
Febrisan	Febrisan	k1gInSc1	Febrisan
a	a	k8xC	a
Medipyrin	Medipyrin	k1gInSc1	Medipyrin
<g/>
,	,	kIx,	,
Coldrex	Coldrex	k1gInSc1	Coldrex
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
dávkování	dávkování	k1gNnSc1	dávkování
je	být	k5eAaImIp3nS	být
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
000	[number]	k4	000
mg	mg	kA	mg
paracetamolu	paracetamol	k1gInSc2	paracetamol
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
dávce	dávka	k1gFnSc6	dávka
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
tablety	tableta	k1gFnPc4	tableta
Paralenu	paralen	k1gInSc2	paralen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
opakovat	opakovat	k5eAaImF	opakovat
po	po	k7c6	po
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
hod	hod	k1gInSc1	hod
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
však	však	k9	však
4	[number]	k4	4
g	g	kA	g
(	(	kIx(	(
<g/>
8	[number]	k4	8
tablet	tablet	k1gInSc4	tablet
Paralenu	paralen	k1gInSc2	paralen
<g/>
)	)	kIx)	)
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
obvykle	obvykle	k6eAd1	obvykle
užívají	užívat	k5eAaImIp3nP	užívat
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
své	své	k1gNnSc4	své
váhy	váha	k1gFnSc2	váha
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
pak	pak	k6eAd1	pak
50	[number]	k4	50
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těhotenství	těhotenství	k1gNnSc6	těhotenství
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
paracetamol	paracetamol	k1gInSc1	paracetamol
užíván	užíván	k2eAgInSc1d1	užíván
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
trimestrech	trimestr	k1gInPc6	trimestr
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
ale	ale	k9	ale
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
paracetamolu	paracetamol	k1gInSc2	paracetamol
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
trimestru	trimestr	k1gInSc6	trimestr
těhotenství	těhotenství	k1gNnSc2	těhotenství
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
-	-	kIx~	-
22	[number]	k4	22
<g/>
.	.	kIx.	.
týden	týden	k1gInSc4	týden
<g/>
)	)	kIx)	)
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
kryptorchismu	kryptorchismus	k1gInSc2	kryptorchismus
u	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Nemělo	mít	k5eNaImAgNnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
nicméně	nicméně	k8xC	nicméně
týkat	týkat	k5eAaImF	týkat
nárazového	nárazový	k2eAgNnSc2d1	nárazové
použití	použití	k1gNnSc2	použití
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
horečky	horečka	k1gFnSc2	horečka
nebo	nebo	k8xC	nebo
proti	proti	k7c3	proti
migréně	migréna	k1gFnSc3	migréna
<g/>
.	.	kIx.	.
</s>
<s>
Paracetamol	paracetamol	k1gInSc1	paracetamol
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
užíván	užívat	k5eAaImNgInS	užívat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
inhibitory	inhibitor	k1gInPc7	inhibitor
COX-	COX-	k1gFnSc2	COX-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nimesulidem	nimesulid	k1gInSc7	nimesulid
(	(	kIx(	(
<g/>
přípravky	přípravek	k1gInPc1	přípravek
Aulin	Aulina	k1gFnPc2	Aulina
<g/>
,	,	kIx,	,
Nimesil	Nimesil	k1gMnPc1	Nimesil
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
léky	lék	k1gInPc7	lék
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yRgNnPc6	který
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc4d1	přítomno
jiné	jiný	k2eAgNnSc4d1	jiné
vážné	vážný	k2eAgNnSc4d1	vážné
onemocnění	onemocnění	k1gNnSc4	onemocnění
jater	játra	k1gNnPc2	játra
nebo	nebo	k8xC	nebo
akutní	akutní	k2eAgInSc1d1	akutní
zánět	zánět	k1gInSc1	zánět
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
užíván	užívat	k5eAaImNgInS	užívat
při	při	k7c6	při
hemolytické	hemolytický	k2eAgFnSc6d1	hemolytická
anémii	anémie	k1gFnSc6	anémie
(	(	kIx(	(
<g/>
typ	typ	k1gInSc1	typ
chudokrevnosti	chudokrevnost	k1gFnSc2	chudokrevnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Paracetamol	paracetamol	k1gInSc1	paracetamol
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
kočky	kočka	k1gFnPc4	kočka
vysoce	vysoce	k6eAd1	vysoce
toxický	toxický	k2eAgMnSc1d1	toxický
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jim	on	k3xPp3gMnPc3	on
chybí	chybit	k5eAaPmIp3nP	chybit
enzym	enzym	k1gInSc4	enzym
glucuronyl	glucuronyl	k1gInSc1	glucuronyl
transferáza	transferáza	k1gFnSc1	transferáza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pro	pro	k7c4	pro
odbourávání	odbourávání	k1gNnSc4	odbourávání
molekuly	molekula	k1gFnSc2	molekula
paracetamolu	paracetamol	k1gInSc2	paracetamol
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInPc1d1	počáteční
příznaky	příznak	k1gInPc1	příznak
otravy	otrava	k1gFnSc2	otrava
jsou	být	k5eAaImIp3nP	být
zvracení	zvracení	k1gNnPc1	zvracení
<g/>
,	,	kIx,	,
slinění	slinění	k1gNnSc1	slinění
a	a	k8xC	a
odbarvení	odbarvení	k1gNnSc1	odbarvení
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
dásní	dáseň	k1gFnPc2	dáseň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předávkování	předávkování	k1gNnSc2	předávkování
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
poškození	poškození	k1gNnSc1	poškození
jater	játra	k1gNnPc2	játra
zřídka	zřídka	k6eAd1	zřídka
příčinou	příčina	k1gFnSc7	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
methemoglobinu	methemoglobina	k1gFnSc4	methemoglobina
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
inhibuje	inhibovat	k5eAaBmIp3nS	inhibovat
transport	transport	k1gInSc1	transport
kyslíku	kyslík	k1gInSc2	kyslík
krví	krev	k1gFnPc2	krev
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
udušení	udušení	k1gNnSc3	udušení
<g/>
..	..	k?	..
Léčba	léčba	k1gFnSc1	léčba
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
N-acetylcysteinem	Ncetylcystein	k1gInSc7	N-acetylcystein
nebo	nebo	k8xC	nebo
methylenovou	methylenový	k2eAgFnSc7d1	methylenová
modří	modř	k1gFnSc7	modř
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
obojím	obé	k1gNnSc7	obé
současně	současně	k6eAd1	současně
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
menší	malý	k2eAgFnSc2d2	menší
dávky	dávka	k1gFnSc2	dávka
paracetamolu	paracetamol	k1gInSc2	paracetamol
bývá	bývat	k5eAaImIp3nS	bývat
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
obecně	obecně	k6eAd1	obecně
panuje	panovat	k5eAaImIp3nS	panovat
mínění	mínění	k1gNnSc4	mínění
<g/>
,	,	kIx,	,
že	že	k8xS	že
paracetamol	paracetamol	k1gInSc1	paracetamol
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc4	žádný
významné	významný	k2eAgInPc4d1	významný
protizánětlivé	protizánětlivý	k2eAgInPc4d1	protizánětlivý
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
léčení	léčení	k1gNnSc6	léčení
bolestí	bolest	k1gFnPc2	bolest
pohybového	pohybový	k2eAgInSc2d1	pohybový
aparátu	aparát	k1gInSc2	aparát
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
stejně	stejně	k6eAd1	stejně
účinný	účinný	k2eAgInSc1d1	účinný
jako	jako	k8xC	jako
aspirin	aspirin	k1gInSc1	aspirin
<g/>
..	..	k?	..
Přípravek	přípravek	k1gInSc1	přípravek
s	s	k7c7	s
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Pardale-V	Pardale-V	k1gFnSc2	Pardale-V
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
paracetamol-kodein	paracetamolodein	k1gInSc1	paracetamol-kodein
byl	být	k5eAaImAgInS	být
schválen	schválit	k5eAaPmNgInS	schválit
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
na	na	k7c4	na
předpis	předpis	k1gInSc4	předpis
veterinárního	veterinární	k2eAgMnSc2d1	veterinární
lékaře	lékař	k1gMnSc2	lékař
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
však	však	k9	však
být	být	k5eAaImF	být
podáván	podáván	k2eAgInSc4d1	podáván
psům	pes	k1gMnPc3	pes
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
veterinární	veterinární	k2eAgNnSc4d1	veterinární
doporučení	doporučení	k1gNnSc4	doporučení
a	a	k8xC	a
s	s	k7c7	s
extrémní	extrémní	k2eAgFnSc7d1	extrémní
opatrností	opatrnost	k1gFnSc7	opatrnost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
efekt	efekt	k1gInSc1	efekt
toxicity	toxicita	k1gFnSc2	toxicita
u	u	k7c2	u
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
poškození	poškození	k1gNnSc1	poškození
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
výskyt	výskyt	k1gInSc1	výskyt
žaludečních	žaludeční	k2eAgInPc2d1	žaludeční
vředů	vřed	k1gInPc2	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
N-acetylcysteinem	Ncetylcysteino	k1gNnSc7	N-acetylcysteino
je	být	k5eAaImIp3nS	být
účinná	účinný	k2eAgFnSc1d1	účinná
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
během	během	k7c2	během
2	[number]	k4	2
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
paracetamolu	paracetamol	k1gInSc2	paracetamol
<g/>
.	.	kIx.	.
</s>
<s>
Paracetamol	paracetamol	k1gInSc1	paracetamol
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
hady	had	k1gMnPc4	had
smrtící	smrtící	k2eAgInSc4d1	smrtící
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
program	program	k1gInSc4	program
chemické	chemický	k2eAgFnSc2d1	chemická
kontroly	kontrola	k1gFnSc2	kontrola
výskytu	výskyt	k1gInSc2	výskyt
invazivního	invazivní	k2eAgInSc2d1	invazivní
druhu	druh	k1gInSc2	druh
úžovkovitého	úžovkovitý	k2eAgMnSc2d1	úžovkovitý
hada	had	k1gMnSc2	had
bojga	bojg	k1gMnSc2	bojg
hnědá	hnědat	k5eAaImIp3nS	hnědat
(	(	kIx(	(
Boiga	Boiga	k1gFnSc1	Boiga
irregularis	irregularis	k1gFnSc2	irregularis
)	)	kIx)	)
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Guam	Guam	k1gInSc1	Guam
<g/>
.	.	kIx.	.
</s>
<s>
Dávky	dávka	k1gFnPc1	dávka
80	[number]	k4	80
mg	mg	kA	mg
látky	látka	k1gFnSc2	látka
se	se	k3xPyFc4	se
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
myší	myš	k1gFnPc2	myš
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozptylovány	rozptylovat	k5eAaImNgInP	rozptylovat
z	z	k7c2	z
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Lincová	Lincová	k1gFnSc1	Lincová
<g/>
,	,	kIx,	,
Farghali	Farghali	k1gFnSc1	Farghali
<g/>
:	:	kIx,	:
Základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
farmakologie	farmakologie	k1gFnSc1	farmakologie
<g/>
.	.	kIx.	.
</s>
<s>
Galén	Galén	k1gInSc1	Galén
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Katzung	Katzung	k1gInSc1	Katzung
<g/>
:	:	kIx,	:
Základní	základní	k2eAgFnSc1d1	základní
a	a	k8xC	a
klinická	klinický	k2eAgFnSc1d1	klinická
farmakologie	farmakologie	k1gFnSc1	farmakologie
<g/>
.	.	kIx.	.
</s>
<s>
H	H	kA	H
<g/>
&	&	k?	&
<g/>
H	H	kA	H
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Hynie	Hynie	k1gFnSc1	Hynie
<g/>
:	:	kIx,	:
Farmakologie	farmakologie	k1gFnSc1	farmakologie
do	do	k7c2	do
kapsy	kapsa	k1gFnSc2	kapsa
<g/>
.	.	kIx.	.
</s>
<s>
Triton	triton	k1gInSc1	triton
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
