<p>
<s>
Šabakové	Šabakový	k2eAgFnPc1d1	Šabaková
jsou	být	k5eAaImIp3nP	být
etnicko-náboženská	etnickoáboženský	k2eAgFnSc1d1	etnicko-náboženský
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
vesnice	vesnice	k1gFnSc1	vesnice
Ali	Ali	k1gMnSc1	Ali
Rash	Rash	k1gMnSc1	Rash
<g/>
,	,	kIx,	,
Khazna	Khazna	k1gFnSc1	Khazna
<g/>
,	,	kIx,	,
Yangidja	Yangidja	k1gFnSc1	Yangidja
a	a	k8xC	a
Tallara	Tallara	k1gFnSc1	Tallara
v	v	k7c6	v
Sindžarském	Sindžarský	k2eAgInSc6d1	Sindžarský
distriktu	distrikt	k1gInSc6	distrikt
Ninivského	ninivský	k2eAgInSc2d1	ninivský
guvernorátu	guvernorát	k1gInSc2	guvernorát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nS	mluvit
šabačtinou	šabačtina	k1gFnSc7	šabačtina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
Íránským	íránský	k2eAgMnPc3d1	íránský
jazykům	jazyk	k1gMnPc3	jazyk
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
kurdským	kurdský	k2eAgMnPc3d1	kurdský
jazykům	jazyk	k1gMnPc3	jazyk
Gorani	Goran	k1gMnPc1	Goran
a	a	k8xC	a
Zazaki	Zazak	k1gMnPc1	Zazak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Šabakové	Šabakový	k2eAgNnSc4d1	Šabakový
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
čelili	čelit	k5eAaImAgMnP	čelit
deportacím	deportace	k1gFnPc3	deportace
během	během	k7c2	během
operace	operace	k1gFnSc2	operace
al-Anfal	al-Anfat	k5eAaPmAgMnS	al-Anfat
<g/>
,	,	kIx,	,
nucené	nucený	k2eAgFnSc6d1	nucená
asimilaci	asimilace	k1gFnSc6	asimilace
a	a	k8xC	a
arabizaci	arabizace	k1gFnSc6	arabizace
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
v	v	k7c6	v
roce	rok	k1gInSc6	rok
zase	zase	k9	zase
trpí	trpět	k5eAaImIp3nS	trpět
útoky	útok	k1gInPc4	útok
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
extremistů	extremista	k1gMnPc2	extremista
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
guvernorát	guvernorát	k1gInSc4	guvernorát
Ninive	Ninive	k1gNnPc2	Ninive
základnou	základna	k1gFnSc7	základna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Šabakismus	Šabakismus	k1gInSc4	Šabakismus
==	==	k?	==
</s>
</p>
<p>
<s>
Náboženství	náboženství	k1gNnSc1	náboženství
Šabaků	Šabak	k1gInPc2	Šabak
je	být	k5eAaImIp3nS	být
synkretické	synkretický	k2eAgNnSc1d1	synkretické
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvky	prvek	k1gInPc4	prvek
islámu	islám	k1gInSc2	islám
i	i	k8xC	i
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
kurdských	kurdský	k2eAgNnPc2d1	kurdské
náboženství	náboženství	k1gNnPc2	náboženství
alevismu	alevismus	k1gInSc2	alevismus
<g/>
,	,	kIx,	,
jezídismu	jezídismus	k1gInSc2	jezídismus
a	a	k8xC	a
jarsanismu	jarsanismus	k1gInSc2	jarsanismus
<g/>
.	.	kIx.	.
</s>
<s>
Šabakové	Šabakový	k2eAgFnPc1d1	Šabaková
konají	konat	k5eAaImIp3nP	konat
poutě	pouť	k1gFnPc1	pouť
do	do	k7c2	do
jezídských	jezídský	k2eAgFnPc2d1	jezídská
svatyní	svatyně	k1gFnPc2	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Nominálně	nominálně	k6eAd1	nominálně
se	se	k3xPyFc4	se
však	však	k9	však
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
šíitskému	šíitský	k2eAgInSc3d1	šíitský
islámu	islám	k1gInSc3	islám
<g/>
.	.	kIx.	.
</s>
<s>
Kombinují	kombinovat	k5eAaImIp3nP	kombinovat
prvky	prvek	k1gInPc4	prvek
súfismu	súfismus	k1gInSc2	súfismus
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
interpretací	interpretace	k1gFnSc7	interpretace
božské	božský	k2eAgFnSc2d1	božská
reality	realita	k1gFnSc2	realita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
vykládat	vykládat	k5eAaImF	vykládat
Korán	korán	k1gInSc4	korán
méně	málo	k6eAd2	málo
doslovně	doslovně	k6eAd1	doslovně
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
dělá	dělat	k5eAaImIp3nS	dělat
šaría	šaría	k6eAd1	šaría
<g/>
.	.	kIx.	.
</s>
<s>
Praktikují	praktikovat	k5eAaImIp3nP	praktikovat
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
a	a	k8xC	a
soukromou	soukromý	k2eAgFnSc4d1	soukromá
zpověď	zpověď	k1gFnSc4	zpověď
a	a	k8xC	a
smějí	smát	k5eAaImIp3nP	smát
pít	pít	k5eAaImF	pít
alkohol	alkohol	k1gInSc4	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Šabačtí	Šabacký	k2eAgMnPc1d1	Šabacký
duchovní	duchovní	k2eAgMnPc1d1	duchovní
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
zběhlí	zběhlý	k2eAgMnPc1d1	zběhlý
v	v	k7c6	v
rituálech	rituál	k1gInPc6	rituál
a	a	k8xC	a
modlitbách	modlitba	k1gFnPc6	modlitba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
pirové	pirové	k2eAgFnPc1d1	pirové
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
následují	následovat	k5eAaImIp3nP	následovat
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
vůdce	vůdce	k1gMnSc4	vůdce
zvaného	zvaný	k2eAgInSc2d1	zvaný
Baba	baba	k1gFnSc1	baba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
posvátná	posvátný	k2eAgFnSc1d1	posvátná
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
Buyruk	Buyruk	k1gInSc1	Buyruk
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
Kitáb	Kitáb	k1gMnSc1	Kitáb
al-Manaqib	al-Manaqib	k1gMnSc1	al-Manaqib
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
dialektu	dialekt	k1gInSc6	dialekt
jižní	jižní	k2eAgFnSc2d1	jižní
ázerbájdžánštiny	ázerbájdžánština	k1gFnSc2	ázerbájdžánština
<g/>
.	.	kIx.	.
65	[number]	k4	65
<g/>
%	%	kIx~	%
Šabaků	Šabak	k1gInPc2	Šabak
jsou	být	k5eAaImIp3nP	být
šíité	šíita	k1gMnPc1	šíita
a	a	k8xC	a
35	[number]	k4	35
<g/>
%	%	kIx~	%
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
šíitskou	šíitský	k2eAgFnSc7d1	šíitská
svatyní	svatyně	k1gFnSc7	svatyně
Šabaků	Šabak	k1gMnPc2	Šabak
je	být	k5eAaImIp3nS	být
Zeen	Zeen	k1gNnSc1	Zeen
Al-Abedeen	Al-Abedena	k1gFnPc2	Al-Abedena
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Aii	Aii	k1gMnSc1	Aii
Rash	Rash	k1gMnSc1	Rash
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Shabak	Shabak	k1gInSc1	Shabak
and	and	k?	and
the	the	k?	the
Kakais	Kakais	k1gInSc1	Kakais
<g/>
:	:	kIx,	:
Dynamics	Dynamics	k1gInSc1	Dynamics
of	of	k?	of
Ethnicity	Ethnicita	k1gFnSc2	Ethnicita
in	in	k?	in
Iraqi	Iraqi	k1gNnPc1	Iraqi
Kurdistan	Kurdistan	k1gInSc1	Kurdistan
</s>
</p>
<p>
<s>
A	a	k9	a
Kizilbash	Kizilbash	k1gMnSc1	Kizilbash
Community	Communita	k1gFnSc2	Communita
in	in	k?	in
Iraqi	Iraqi	k1gNnPc2	Iraqi
Kurdistan	Kurdistan	k1gInSc1	Kurdistan
:	:	kIx,	:
The	The	k1gMnSc1	The
Shabak	Shabak	k1gMnSc1	Shabak
</s>
</p>
