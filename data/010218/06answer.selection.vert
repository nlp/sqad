<s>
Šabakové	Šabakový	k2eAgFnPc1d1	Šabaková
jsou	být	k5eAaImIp3nP	být
etnicko-náboženská	etnickoáboženský	k2eAgFnSc1d1	etnicko-náboženský
komunita	komunita	k1gFnSc1	komunita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
vesnice	vesnice	k1gFnSc1	vesnice
Ali	Ali	k1gMnSc1	Ali
Rash	Rash	k1gMnSc1	Rash
<g/>
,	,	kIx,	,
Khazna	Khazna	k1gFnSc1	Khazna
<g/>
,	,	kIx,	,
Yangidja	Yangidja	k1gFnSc1	Yangidja
a	a	k8xC	a
Tallara	Tallara	k1gFnSc1	Tallara
v	v	k7c6	v
Sindžarském	Sindžarský	k2eAgInSc6d1	Sindžarský
distriktu	distrikt	k1gInSc6	distrikt
Ninivského	ninivský	k2eAgInSc2d1	ninivský
guvernorátu	guvernorát	k1gInSc2	guvernorát
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
