<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
(	(	kIx(
<g/>
Troyes	Troyes	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
Litografie	litografie	k1gFnSc2
náhrobku	náhrobek	k1gInSc2
Jindřicha	Jindřich	k1gMnSc2
I.	I.	kA
ze	z	k7c2
Champagne	Champagn	k1gInSc5
v	v	k7c6
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
ŠtěpánaMísto	ŠtěpánaMísta	k1gFnSc5
Stát	stát	k1gInSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
54,42	54,42	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
44,84	44,84	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Zánik	zánik	k1gInSc1
</s>
<s>
1792	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
v	v	k7c4
Troyes	Troyes	k1gInSc4
býval	bývat	k5eAaImAgInS
kolegiátní	kolegiátní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
v	v	k7c4
Troyes	Troyes	k1gInSc4
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
roku	rok	k1gInSc2
1157	#num#	k4
Jindřichem	Jindřich	k1gMnSc7
ze	z	k7c2
Champagne	Champagn	k1gInSc5
jako	jako	k9
součást	součást	k1gFnSc4
hraběcího	hraběcí	k2eAgInSc2d1
palácového	palácový	k2eAgInSc2d1
komplexu	komplex	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zamýšlen	zamýšlet	k5eAaImNgMnS
jako	jako	k9
nekropole	nekropole	k1gFnSc1
hrabat	hrabat	k5eAaImF
ze	z	k7c2
Champagne	Champagn	k1gInSc5
a	a	k8xC
k	k	k7c3
jeho	jeho	k3xOp3gMnSc3
dokončení	dokončení	k1gNnSc1
došlo	dojít	k5eAaPmAgNnS
zřejmě	zřejmě	k6eAd1
roku	rok	k1gInSc2
1174	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
existence	existence	k1gFnSc2
dynastie	dynastie	k1gFnSc2
místní	místní	k2eAgMnPc1d1
kanovníci	kanovník	k1gMnPc1
získávali	získávat	k5eAaImAgMnP
bohaté	bohatý	k2eAgInPc4d1
dary	dar	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
i	i	k8xC
palác	palác	k1gInSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zničeny	zničit	k5eAaPmNgFnP
během	během	k7c2
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
náhrobky	náhrobek	k1gInPc1
fundátora	fundátor	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xPp3gMnSc2
syna	syn	k1gMnSc2
Theobalda	Theobald	k1gMnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
byly	být	k5eAaImAgFnP
roztaveny	roztavit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.lamop-intranet.univ-paris1.fr	www.lamop-intranet.univ-paris1.fr	k1gInSc1
<g/>
.	.	kIx.
lamop-intranet	lamop-intranet	k1gInSc1
<g/>
.	.	kIx.
<g/>
univ-paris	univ-paris	k1gInSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
fr	fr	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
MCGEE	MCGEE	kA
MORGANSTERN	MORGANSTERN	kA
<g/>
,	,	kIx,
Anne	Anne	k1gFnSc1
<g/>
;	;	kIx,
A.	A.	kA
A.	A.	kA
GOODALL	GOODALL	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gothic	Gothice	k1gInPc2
Tombs	Tombs	k1gInSc4
of	of	k?
Kinship	Kinship	k1gInSc1
in	in	k?
France	Franc	k1gMnSc2
<g/>
,	,	kIx,
the	the	k?
Low	Low	k1gMnSc1
Countries	Countries	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
England	England	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Pennsylvania	Pennsylvanium	k1gNnPc4
State	status	k1gInSc5
Univ	Univa	k1gFnPc2
Press	Press	k1gInSc1
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
271018591	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Francie	Francie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
131420777	#num#	k4
</s>
