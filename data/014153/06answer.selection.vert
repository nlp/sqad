<s desamb="1">
Po	po	k7c4
celou	celý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
existence	existence	k1gFnSc2
dynastie	dynastie	k1gFnSc2
místní	místní	k2eAgMnPc1d1
kanovníci	kanovník	k1gMnPc1
získávali	získávat	k5eAaImAgMnP
bohaté	bohatý	k2eAgInPc4d1
dary	dar	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Obě	dva	k4xCgFnPc1
stavby	stavba	k1gFnPc1
<g/>
,	,	kIx,
kostel	kostel	k1gInSc1
i	i	k8xC
palác	palác	k1gInSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
zničeny	zničit	k5eAaPmNgFnP
během	během	k7c2
francouzské	francouzský	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
a	a	k8xC
náhrobky	náhrobek	k1gInPc1
fundátora	fundátor	k1gMnSc2
Jindřicha	Jindřich	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xPp3gMnSc2
syna	syn	k1gMnSc2
Theobalda	Theobald	k1gMnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
byly	být	k5eAaImAgFnP
roztaveny	roztavit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>