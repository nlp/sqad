<s>
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Červené	Červené	k2eAgFnSc2d1	Červené
lišky	liška	k1gFnSc2	liška
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
někdy	někdy	k6eAd1	někdy
U	u	k7c2	u
Srpů	srp	k1gInPc2	srp
nebo	nebo	k8xC	nebo
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
480	[number]	k4	480
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
č.	č.	k?	č.
24	[number]	k4	24
<g/>
)	)	kIx)	)
zasahující	zasahující	k2eAgInSc1d1	zasahující
svým	svůj	k3xOyFgInSc7	svůj
zadním	zadní	k2eAgInSc7d1	zadní
traktem	trakt	k1gInSc7	trakt
do	do	k7c2	do
Kožné	Kožný	k2eAgFnSc2d1	Kožný
ulice	ulice	k1gFnSc2	ulice
(	(	kIx(	(
<g/>
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
