<p>
<s>
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Červené	Červené	k2eAgFnSc2d1	Červené
lišky	liška	k1gFnSc2	liška
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
někdy	někdy	k6eAd1	někdy
U	u	k7c2	u
Srpů	srp	k1gInPc2	srp
nebo	nebo	k8xC	nebo
U	u	k7c2	u
Zlatého	zlatý	k2eAgMnSc2d1	zlatý
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dům	dům	k1gInSc1	dům
čp.	čp.	k?	čp.
480	[number]	k4	480
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Městě	město	k1gNnSc6	město
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Staroměstském	staroměstský	k2eAgNnSc6d1	Staroměstské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
č.	č.	k?	č.
24	[number]	k4	24
<g/>
)	)	kIx)	)
zasahující	zasahující	k2eAgInSc1d1	zasahující
svým	svůj	k3xOyFgInSc7	svůj
zadním	zadní	k2eAgInSc7d1	zadní
traktem	trakt	k1gInSc7	trakt
do	do	k7c2	do
Kožné	Kožný	k2eAgFnSc2d1	Kožný
ulice	ulice	k1gFnSc2	ulice
(	(	kIx(	(
<g/>
č.	č.	k?	č.
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
mezi	mezi	k7c7	mezi
domem	dům	k1gInSc7	dům
U	u	k7c2	u
Modré	modrý	k2eAgFnSc2d1	modrá
husy	husa	k1gFnSc2	husa
a	a	k8xC	a
domem	dům	k1gInSc7	dům
U	u	k7c2	u
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
koruny	koruna	k1gFnSc2	koruna
naproti	naproti	k7c3	naproti
Staroměstskému	staroměstský	k2eAgInSc3d1	staroměstský
orloji	orloj	k1gInSc3	orloj
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
románský	románský	k2eAgInSc4d1	románský
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
přestavěný	přestavěný	k2eAgMnSc1d1	přestavěný
goticky	goticky	k6eAd1	goticky
a	a	k8xC	a
poté	poté	k6eAd1	poté
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
raně	raně	k6eAd1	raně
barokně	barokně	k6eAd1	barokně
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
domu	dům	k1gInSc6wR	dům
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1401	[number]	k4	1401
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
základ	základ	k1gInSc1	základ
domu	dům	k1gInSc2	dům
je	být	k5eAaImIp3nS	být
starší	starý	k2eAgMnSc1d2	starší
<g/>
,	,	kIx,	,
románský	románský	k2eAgInSc1d1	románský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
objekt	objekt	k1gInSc1	objekt
nahrazen	nahradit	k5eAaPmNgInS	nahradit
gotickou	gotický	k2eAgFnSc7d1	gotická
novostavbou	novostavba	k1gFnSc7	novostavba
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1508	[number]	k4	1508
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
další	další	k2eAgFnSc1d1	další
středověká	středověký	k2eAgFnSc1d1	středověká
úprava	úprava	k1gFnSc1	úprava
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
byl	být	k5eAaImAgInS	být
dům	dům	k1gInSc1	dům
výrazně	výrazně	k6eAd1	výrazně
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
sporu	spor	k1gInSc3	spor
se	s	k7c7	s
sousedem	soused	k1gMnSc7	soused
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
přestavba	přestavba	k1gFnSc1	přestavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1694	[number]	k4	1694
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
přestavby	přestavba	k1gFnSc2	přestavba
bylo	být	k5eAaImAgNnS	být
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
původní	původní	k2eAgNnSc1d1	původní
domovní	domovní	k2eAgNnSc1d1	domovní
znamení	znamení	k1gNnSc1	znamení
reliéfem	reliéf	k1gInSc7	reliéf
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgFnPc2d1	další
moderních	moderní	k2eAgFnPc2d1	moderní
úprav	úprava	k1gFnPc2	úprava
se	se	k3xPyFc4	se
dům	dům	k1gInSc1	dům
dočkal	dočkat	k5eAaPmAgInS	dočkat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
např.	např.	kA	např.
zničeno	zničit	k5eAaPmNgNnS	zničit
původní	původní	k2eAgNnSc1d1	původní
barokní	barokní	k2eAgNnSc1d1	barokní
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
bylo	být	k5eAaImAgNnS	být
vestavěno	vestavěn	k2eAgNnSc1d1	vestavěno
podkroví	podkroví	k1gNnSc1	podkroví
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnSc1	průčelí
do	do	k7c2	do
Kožné	Kožný	k2eAgFnSc2d1	Kožný
ulice	ulice	k1gFnSc2	ulice
má	mít	k5eAaImIp3nS	mít
novodobou	novodobý	k2eAgFnSc4d1	novodobá
úpravu	úprava	k1gFnSc4	úprava
<g/>
,	,	kIx,	,
vjezd	vjezd	k1gInSc1	vjezd
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
objektu	objekt	k1gInSc6	objekt
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1745	[number]	k4	1745
od	od	k7c2	od
Ignáce	Ignác	k1gMnSc2	Ignác
Františka	František	k1gMnSc2	František
Platzera	Platzer	k1gMnSc2	Platzer
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
v	v	k7c6	v
domě	dům	k1gInSc6	dům
sídlila	sídlit	k5eAaImAgFnS	sídlit
pobočka	pobočka	k1gFnSc1	pobočka
Komerční	komerční	k2eAgFnSc2d1	komerční
banky	banka	k1gFnSc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
dům	dům	k1gInSc1	dům
za	za	k7c4	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
koupila	koupit	k5eAaPmAgFnS	koupit
společnost	společnost	k1gFnSc1	společnost
Coast	Coast	k1gInSc1	Coast
Capital	Capital	k1gMnSc1	Capital
Partners	Partners	k1gInSc1	Partners
(	(	kIx(	(
<g/>
CCP	CCP	kA	CCP
<g/>
)	)	kIx)	)
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
jej	on	k3xPp3gMnSc4	on
zrenovovat	zrenovovat	k5eAaPmF	zrenovovat
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
cenu	cena	k1gFnSc4	cena
přibližně	přibližně	k6eAd1	přibližně
230	[number]	k4	230
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
metr	metr	k1gInSc4	metr
čtvereční	čtvereční	k2eAgInSc4d1	čtvereční
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
dosud	dosud	k6eAd1	dosud
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
známá	známý	k2eAgFnSc1d1	známá
suma	suma	k1gFnSc1	suma
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
byl	být	k5eAaImAgMnS	být
kupec	kupec	k1gMnSc1	kupec
ochotný	ochotný	k2eAgMnSc1d1	ochotný
za	za	k7c4	za
dům	dům	k1gInSc4	dům
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
března	březen	k1gInSc2	březen
2018	[number]	k4	2018
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
domě	dům	k1gInSc6	dům
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
Muzeum	muzeum	k1gNnSc4	muzeum
iluzí	iluze	k1gFnPc2	iluze
(	(	kIx(	(
<g/>
Illusion	Illusion	k1gInSc4	Illusion
Art	Art	k1gFnSc2	Art
Museum	museum	k1gNnSc4	museum
Prague	Prague	k1gNnSc2	Prague
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgNnSc4d1	Staré
Město	město	k1gNnSc4	město
–	–	k?	–
Josefov	Josefov	k1gInSc1	Josefov
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
s.	s.	k?	s.
332	[number]	k4	332
<g/>
–	–	k?	–
<g/>
333	[number]	k4	333
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-200-0563-3	[number]	k4	80-200-0563-3
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
U	u	k7c2	u
Červené	Červené	k2eAgFnSc2d1	Červené
lišky	liška	k1gFnSc2	liška
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
