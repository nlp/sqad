<p>
<s>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Alois	Alois	k1gMnSc1	Alois
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
rytíř	rytíř	k1gMnSc1	rytíř
von	von	k1gInSc4	von
Köchel	Köchel	k1gInSc1	Köchel
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1800	[number]	k4	1800
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1877	[number]	k4	1877
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
muzikolog	muzikolog	k1gMnSc1	muzikolog
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
editor	editor	k1gInSc1	editor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
zřejmě	zřejmě	k6eAd1	zřejmě
nejznámějším	známý	k2eAgInSc7d3	nejznámější
počinem	počin	k1gInSc7	počin
je	být	k5eAaImIp3nS	být
katalogizace	katalogizace	k1gFnSc1	katalogizace
a	a	k8xC	a
uspořádání	uspořádání	k1gNnSc1	uspořádání
tvorby	tvorba	k1gFnSc2	tvorba
hudebního	hudební	k2eAgMnSc2d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Köchelův	Köchelův	k2eAgInSc1d1	Köchelův
seznam	seznam	k1gInSc1	seznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Stein	Stein	k1gMnSc1	Stein
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
učitelem	učitel	k1gMnSc7	učitel
čtyř	čtyři	k4xCgMnPc2	čtyři
synů	syn	k1gMnPc2	syn
rakouského	rakouský	k2eAgMnSc2d1	rakouský
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Köchel	Köchel	k1gMnSc1	Köchel
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
velkorysou	velkorysý	k2eAgFnSc4d1	velkorysá
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
strávit	strávit	k5eAaPmF	strávit
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
jako	jako	k8xS	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
věda	věda	k1gFnSc1	věda
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
zaujata	zaujat	k2eAgFnSc1d1	zaujata
jeho	jeho	k3xOp3gInPc7	jeho
botanickými	botanický	k2eAgInPc7d1	botanický
výzkumy	výzkum	k1gInPc7	výzkum
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
na	na	k7c6	na
Nordkappu	Nordkapp	k1gInSc6	Nordkapp
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
botaniku	botanika	k1gFnSc4	botanika
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
geologii	geologie	k1gFnSc4	geologie
a	a	k8xC	a
mineralogii	mineralogie	k1gFnSc4	mineralogie
<g/>
,	,	kIx,	,
především	především	k9	především
však	však	k9	však
o	o	k7c4	o
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1862	[number]	k4	1862
publikoval	publikovat	k5eAaBmAgMnS	publikovat
chronologický	chronologický	k2eAgMnSc1d1	chronologický
a	a	k8xC	a
tematický	tematický	k2eAgInSc1d1	tematický
seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
W.	W.	kA	W.
A.	A.	kA	A.
Mozarta	Mozart	k1gMnSc2	Mozart
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
katalog	katalog	k1gInSc1	katalog
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
roztříděním	roztřídění	k1gNnSc7	roztřídění
Mozartových	Mozartových	k2eAgNnPc2d1	Mozartových
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Mozartova	Mozartův	k2eAgNnPc1d1	Mozartovo
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
obvykle	obvykle	k6eAd1	obvykle
značena	značen	k2eAgFnSc1d1	značena
namísto	namísto	k7c2	namísto
čísla	číslo	k1gNnSc2	číslo
opusu	opus	k1gInSc2	opus
(	(	kIx(	(
<g/>
Mozart	Mozart	k1gMnSc1	Mozart
sám	sám	k3xTgMnSc1	sám
svým	svůj	k3xOyFgInSc7	svůj
skladbám	skladba	k1gFnPc3	skladba
čísla	číslo	k1gNnSc2	číslo
opusu	opus	k1gInSc2	opus
nedával	dávat	k5eNaImAgInS	dávat
<g/>
)	)	kIx)	)
číslem	číslo	k1gNnSc7	číslo
v	v	k7c6	v
Köchelově	Köchelův	k2eAgInSc6d1	Köchelův
seznamu	seznam	k1gInSc6	seznam
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
předchází	předcházet	k5eAaImIp3nS	předcházet
písmeno	písmeno	k1gNnSc4	písmeno
K	k	k7c3	k
(	(	kIx(	(
<g/>
např.	např.	kA	např.
symfonie	symfonie	k1gFnSc1	symfonie
číslo	číslo	k1gNnSc1	číslo
41	[number]	k4	41
zvaná	zvaný	k2eAgFnSc1d1	zvaná
"	"	kIx"	"
<g/>
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Köchelově	Köchelův	k2eAgInSc6d1	Köchelův
katalogu	katalog	k1gInSc6	katalog
číslo	číslo	k1gNnSc1	číslo
K	k	k7c3	k
551	[number]	k4	551
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
roztřídil	roztřídit	k5eAaPmAgMnS	roztřídit
Köchel	Köchel	k1gMnSc1	Köchel
Mozartova	Mozartův	k2eAgNnSc2d1	Mozartovo
díla	dílo	k1gNnSc2	dílo
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
čtyř	čtyři	k4xCgInPc2	čtyři
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
vydavatelstvím	vydavatelství	k1gNnSc7	vydavatelství
Breitkopf	Breitkopf	k1gMnSc1	Breitkopf
&	&	k?	&
Härtel	Härtel	k1gMnSc1	Härtel
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
úplném	úplný	k2eAgNnSc6d1	úplné
vědeckém	vědecký	k2eAgNnSc6d1	vědecké
vydání	vydání	k1gNnSc6	vydání
Mozartových	Mozartových	k2eAgNnPc2d1	Mozartových
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Alte	alt	k1gInSc5	alt
Mozart-Ausgabe	Mozart-Ausgab	k1gInSc5	Mozart-Ausgab
<g/>
)	)	kIx)	)
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1877	[number]	k4	1877
–	–	k?	–
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
částečně	částečně	k6eAd1	částečně
sponzorováno	sponzorovat	k5eAaImNgNnS	sponzorovat
Köchelem	Köchel	k1gInSc7	Köchel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ludwig	Ludwiga	k1gFnPc2	Ludwiga
Ritter	Rittra	k1gFnPc2	Rittra
von	von	k1gInSc1	von
Köchel	Köchel	k1gMnSc1	Köchel
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
</s>
</p>
<p>
<s>
Köchelův	Köchelův	k2eAgInSc1d1	Köchelův
seznam	seznam	k1gInSc1	seznam
</s>
</p>
