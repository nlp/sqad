<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Stein	Stein	k1gMnSc1	Stein
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
učitelem	učitel	k1gMnSc7	učitel
čtyř	čtyři	k4xCgMnPc2	čtyři
synů	syn	k1gMnPc2	syn
rakouského	rakouský	k2eAgMnSc2d1	rakouský
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Köchel	Köchel	k1gMnSc1	Köchel
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
také	také	k9	také
velkorysou	velkorysý	k2eAgFnSc4d1	velkorysá
finanční	finanční	k2eAgFnSc4d1	finanční
odměnu	odměna	k1gFnSc4	odměna
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
strávit	strávit	k5eAaPmF	strávit
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
jako	jako	k8xS	jako
soukromý	soukromý	k2eAgMnSc1d1	soukromý
učenec	učenec	k1gMnSc1	učenec
<g/>
.	.	kIx.	.
</s>

