<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Stein	Stein	k1gMnSc1	Stein
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
práva	právo	k1gNnPc4	právo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
učitelem	učitel	k1gMnSc7	učitel
čtyř	čtyři	k4xCgMnPc2	čtyři
synů	syn	k1gMnPc2	syn
rakouského	rakouský	k2eAgMnSc2d1	rakouský
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
<g/>
.	.	kIx.	.
</s>
