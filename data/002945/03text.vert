<s>
Univerzální	univerzální	k2eAgInSc1d1	univerzální
kvantifikátor	kvantifikátor	k1gInSc1	kvantifikátor
(	(	kIx(	(
<g/>
∀	∀	k?	∀
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
také	také	k9	také
obecný	obecný	k2eAgInSc4d1	obecný
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
či	či	k8xC	či
velký	velký	k2eAgInSc4d1	velký
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc1d1	matematický
symbol	symbol	k1gInSc1	symbol
používaný	používaný	k2eAgInSc1d1	používaný
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
predikátové	predikátový	k2eAgFnSc6d1	predikátová
logice	logika	k1gFnSc6	logika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
jazyka	jazyk	k1gInSc2	jazyk
lze	lze	k6eAd1	lze
jeho	on	k3xPp3gInSc4	on
význam	význam	k1gInSc4	význam
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
každé	každý	k3xTgInPc4	každý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Duálním	duální	k2eAgInSc7d1	duální
kvantifikátorem	kvantifikátor	k1gInSc7	kvantifikátor
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
je	být	k5eAaImIp3nS	být
existenční	existenční	k2eAgInSc1d1	existenční
kvantifikátor	kvantifikátor	k1gInSc1	kvantifikátor
s	s	k7c7	s
významem	význam	k1gInSc7	význam
"	"	kIx"	"
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
∀	∀	k?	∀
pro	pro	k7c4	pro
univerzální	univerzální	k2eAgInSc4d1	univerzální
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
vznikl	vzniknout	k5eAaPmAgMnS	vzniknout
převrácením	převrácení	k1gNnSc7	převrácení
písmena	písmeno	k1gNnPc1	písmeno
A	a	k9	a
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
All	All	k1gMnSc2	All
-	-	kIx~	-
všechno	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
univerzálního	univerzální	k2eAgInSc2d1	univerzální
kvantifikátoru	kvantifikátor	k1gInSc2	kvantifikátor
lze	lze	k6eAd1	lze
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
<g/>
"	"	kIx"	"
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
danou	daný	k2eAgFnSc4d1	daná
vlastnost	vlastnost	k1gFnSc4	vlastnost
mají	mít	k5eAaImIp3nP	mít
všechny	všechen	k3xTgInPc1	všechen
uvažované	uvažovaný	k2eAgInPc1d1	uvažovaný
objekty	objekt	k1gInPc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
větu	věta	k1gFnSc4	věta
"	"	kIx"	"
<g/>
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgMnSc1d1	smrtelný
<g/>
"	"	kIx"	"
můžeme	moct	k5eAaImIp1nP	moct
přepsat	přepsat	k5eAaPmF	přepsat
pomocí	pomocí	k7c2	pomocí
univerzálního	univerzální	k2eAgInSc2d1	univerzální
kvantifikátoru	kvantifikátor	k1gInSc2	kvantifikátor
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
(	(	kIx(	(
<g/>
∀	∀	k?	∀
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
x	x	k?	x
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
x	x	k?	x
je	být	k5eAaImIp3nS	být
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
myslitelný	myslitelný	k2eAgInSc4d1	myslitelný
objekt	objekt	k1gInSc4	objekt
x	x	k?	x
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
ne	ne	k9	ne
například	například	k6eAd1	například
kamenem	kámen	k1gInSc7	kámen
<g/>
,	,	kIx,	,
Bohem	bůh	k1gMnSc7	bůh
či	či	k8xC	či
žirafou	žirafa	k1gFnSc7	žirafa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
konečných	konečný	k2eAgInPc2d1	konečný
souborů	soubor	k1gInPc2	soubor
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nahradit	nahradit	k5eAaPmF	nahradit
univerzální	univerzální	k2eAgInSc4d1	univerzální
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
konečným	konečný	k2eAgInSc7d1	konečný
počtem	počet	k1gInSc7	počet
konjunkcí	konjunkce	k1gFnPc2	konjunkce
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
řekli	říct	k5eAaPmAgMnP	říct
"	"	kIx"	"
<g/>
Každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
narozený	narozený	k2eAgMnSc1d1	narozený
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
osoby	osoba	k1gFnPc1	osoba
vyjmenovat	vyjmenovat	k5eAaPmF	vyjmenovat
a	a	k8xC	a
o	o	k7c4	o
každé	každý	k3xTgNnSc4	každý
prohlásit	prohlásit	k5eAaPmF	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
-	-	kIx~	-
"	"	kIx"	"
<g/>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Seifert	Seifert	k1gMnSc1	Seifert
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgMnSc1d1	smrtelný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
Fidel	Fidel	k1gMnSc1	Fidel
Castro	Castro	k1gNnSc4	Castro
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
Jaromír	Jaromír	k1gMnSc1	Jaromír
Jágr	Jágr	k1gMnSc1	Jágr
je	být	k5eAaImIp3nS	být
smrtelný	smrtelný	k2eAgMnSc1d1	smrtelný
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nekonečných	konečný	k2eNgInPc2d1	nekonečný
souborů	soubor	k1gInPc2	soubor
podobnou	podobný	k2eAgFnSc4d1	podobná
záměnu	záměna	k1gFnSc4	záměna
provést	provést	k5eAaPmF	provést
nelze	lze	k6eNd1	lze
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vyjmenováváním	vyjmenovávání	k1gNnSc7	vyjmenovávání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
objektů	objekt	k1gInPc2	objekt
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nedobrali	dobrat	k5eNaPmAgMnP	dobrat
konce	konec	k1gInPc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc1	zápis
(	(	kIx(	(
<g/>
∀	∀	k?	∀
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
ε	ε	k?	ε
M	M	kA	M
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Každý	každý	k3xTgInSc1	každý
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
prvkem	prvek	k1gInSc7	prvek
množiny	množina	k1gFnSc2	množina
M	M	kA	M
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výrok	výrok	k1gInSc4	výrok
<g/>
,	,	kIx,	,
<g/>
tak	tak	k6eAd1	tak
výrok	výrok	k1gInSc1	výrok
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
¬	¬	k?	¬
<g/>
(	(	kIx(	(
<g/>
∀	∀	k?	∀
<g/>
x	x	k?	x
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
))	))	k?	))
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
∃	∃	k?	∃
<g/>
x	x	k?	x
¬	¬	k?	¬
<g/>
P	P	kA	P
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
výrok	výrok	k1gInSc1	výrok
¬	¬	k?	¬
<g/>
(	(	kIx(	(
<g/>
∀	∀	k?	∀
<g/>
xε	xε	k?	xε
-x	-x	k?	-x
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zapsat	zapsat	k5eAaPmF	zapsat
jako	jako	k8xC	jako
∃	∃	k?	∃
<g/>
xε	xε	k?	xε
-x	-x	k?	-x
<g/>
≠	≠	k?	≠
<g/>
x	x	k?	x
Kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
Existenční	existenční	k2eAgInSc4d1	existenční
kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
Kvantifikátor	kvantifikátor	k1gInSc4	kvantifikátor
jednoznačné	jednoznačný	k2eAgFnSc2d1	jednoznačná
existence	existence	k1gFnSc2	existence
</s>
