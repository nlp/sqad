<s>
Císař	Císař	k1gMnSc1	Císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
učinil	učinit	k5eAaImAgMnS	učinit
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
zřídil	zřídit	k5eAaPmAgMnS	zřídit
zde	zde	k6eAd1	zde
své	svůj	k3xOyFgNnSc4	svůj
místodržitelství	místodržitelství	k1gNnSc4	místodržitelství
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Královský	královský	k2eAgInSc1d1	královský
tribunál	tribunál	k1gInSc1	tribunál
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
sídlem	sídlo	k1gNnSc7	sídlo
všech	všecek	k3xTgInPc2	všecek
hlavních	hlavní	k2eAgInPc2d1	hlavní
královských	královský	k2eAgInPc2d1	královský
i	i	k8xC	i
zemských	zemský	k2eAgInPc2d1	zemský
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
