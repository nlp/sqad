<p>
<s>
FC	FC	kA	FC
Gillotina	Gillotina	k1gFnSc1	Gillotina
Choceň	Choceň	k1gFnSc1	Choceň
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc4d1	český
futsalový	futsalový	k2eAgInSc4d1	futsalový
klub	klub	k1gInSc4	klub
z	z	k7c2	z
Chocně	Choceň	k1gFnSc2	Choceň
<g/>
,	,	kIx,	,
hrající	hrající	k2eAgMnSc1d1	hrající
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
českou	český	k2eAgFnSc4d1	Česká
futsalovou	futsalový	k2eAgFnSc4d1	futsalová
Divizi	divize	k1gFnSc4	divize
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
D.	D.	kA	D.
</s>
<s>
Klub	klub	k1gInSc1	klub
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
vzniku	vznik	k1gInSc2	vznik
okresní	okresní	k2eAgFnSc2d1	okresní
futsalové	futsalové	k2eAgFnSc2d1	futsalové
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
.	.	kIx.	.
</s>
<s>
Zakladateli	zakladatel	k1gMnPc7	zakladatel
klubu	klub	k1gInSc2	klub
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
především	především	k9	především
hráči	hráč	k1gMnPc1	hráč
fotbalového	fotbalový	k2eAgNnSc2d1	fotbalové
Agria	Agrium	k1gNnSc2	Agrium
Choceň	Choceň	k1gFnSc1	Choceň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Premiérový	premiérový	k2eAgInSc1d1	premiérový
ročník	ročník	k1gInSc1	ročník
okresní	okresní	k2eAgFnSc2d1	okresní
futsalové	futsalové	k2eAgFnSc2d1	futsalové
ligy	liga	k1gFnSc2	liga
se	se	k3xPyFc4	se
klubu	klub	k1gInSc3	klub
povedl	povést	k5eAaPmAgInS	povést
vyhrát	vyhrát	k5eAaPmF	vyhrát
a	a	k8xC	a
postoupit	postoupit	k5eAaPmF	postoupit
tak	tak	k6eAd1	tak
následně	následně	k6eAd1	následně
do	do	k7c2	do
Divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
ročníku	ročník	k1gInSc6	ročník
Východočeské	východočeský	k2eAgFnSc2d1	Východočeská
divize	divize	k1gFnSc2	divize
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
nepostupovém	postupový	k2eNgMnSc6d1	nepostupový
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vítěz	vítěz	k1gMnSc1	vítěz
postupu	postup	k1gInSc2	postup
vzdal	vzdát	k5eAaPmAgMnS	vzdát
<g/>
,	,	kIx,	,
postoupil	postoupit	k5eAaPmAgInS	postoupit
klub	klub	k1gInSc1	klub
místo	místo	k7c2	místo
něj	on	k3xPp3gMnSc2	on
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
celostátní	celostátní	k2eAgFnSc2d1	celostátní
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
klub	klub	k1gInSc1	klub
svoji	svůj	k3xOyFgFnSc4	svůj
regionální	regionální	k2eAgFnSc4d1	regionální
skupinu	skupina	k1gFnSc4	skupina
druhé	druhý	k4xOgFnSc2	druhý
ligy	liga	k1gFnSc2	liga
suverénně	suverénně	k6eAd1	suverénně
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
a	a	k8xC	a
postoupil	postoupit	k5eAaPmAgMnS	postoupit
tak	tak	k6eAd1	tak
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
do	do	k7c2	do
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
klub	klub	k1gInSc1	klub
setrval	setrvat	k5eAaPmAgMnS	setrvat
pouhé	pouhý	k2eAgFnPc4d1	pouhá
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
–	–	k?	–
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
sestupové	sestupový	k2eAgFnSc6d1	sestupová
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
přihlásil	přihlásit	k5eAaPmAgMnS	přihlásit
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
Divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
<g/>
Své	svůj	k3xOyFgInPc4	svůj
domácí	domácí	k2eAgInPc4d1	domácí
zápasy	zápas	k1gInPc4	zápas
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
klub	klub	k1gInSc1	klub
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
sportovní	sportovní	k2eAgFnSc6d1	sportovní
hale	hala	k1gFnSc6	hala
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
.	.	kIx.	.
<g/>
Klub	klub	k1gInSc1	klub
po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2017	[number]	k4	2017
<g/>
/	/	kIx~	/
<g/>
18	[number]	k4	18
ukončil	ukončit	k5eAaPmAgInS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Umístění	umístění	k1gNnSc1	umístění
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
sezonách	sezona	k1gFnPc6	sezona
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Legenda	legenda	k1gFnSc1	legenda
<g/>
:	:	kIx,	:
Z	z	k7c2	z
–	–	k?	–
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
V	v	k7c6	v
–	–	k?	–
výhry	výhra	k1gFnPc1	výhra
<g/>
,	,	kIx,	,
R	R	kA	R
–	–	k?	–
remízy	remíza	k1gFnPc4	remíza
<g/>
,	,	kIx,	,
P	P	kA	P
–	–	k?	–
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
VG	VG	kA	VG
–	–	k?	–
vstřelené	vstřelený	k2eAgInPc1d1	vstřelený
góly	gól	k1gInPc1	gól
<g/>
,	,	kIx,	,
OG	OG	kA	OG
–	–	k?	–
obdržené	obdržený	k2eAgInPc4d1	obdržený
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
–	–	k?	–
rozdíl	rozdíl	k1gInSc1	rozdíl
skóre	skóre	k1gNnSc2	skóre
<g/>
,	,	kIx,	,
B	B	kA	B
–	–	k?	–
body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc4d1	červené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
sestup	sestup	k1gInSc1	sestup
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgNnSc4d1	zelené
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
světle	světle	k6eAd1	světle
fialové	fialový	k2eAgNnSc4d1	fialové
podbarvení	podbarvení	k1gNnSc4	podbarvení
–	–	k?	–
přesun	přesun	k1gInSc1	přesun
do	do	k7c2	do
jiné	jiný	k2eAgFnSc2d1	jiná
soutěže	soutěž	k1gFnSc2	soutěž
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
klubové	klubový	k2eAgFnPc1d1	klubová
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
fanouškovské	fanouškovský	k2eAgFnPc1d1	fanouškovská
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FC	FC	kA	FC
Gillotina	Gillotina	k1gFnSc1	Gillotina
Choceň	Choceň	k1gFnSc1	Choceň
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
