<s>
Mahátma	Mahátma	k1gFnSc1	Mahátma
Gándhí	Gándhí	k1gFnSc1	Gándhí
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Móhandás	Móhandása	k1gFnPc2	Móhandása
Karamčand	Karamčand	k1gInSc4	Karamčand
Gándhí	Gándhí	k1gFnSc4	Gándhí
(	(	kIx(	(
<g/>
v	v	k7c6	v
dévanágarí	dévanágarí	k1gFnSc6	dévanágarí
म	म	k?	म
<g/>
ो	ो	k?	ो
<g/>
ह	ह	k?	ह
<g/>
ा	ा	k?	ा
<g/>
स	स	k?	स
क	क	k?	क
<g/>
्	्	k?	्
<g/>
द	द	k?	द
ग	ग	k?	ग
<g/>
ा	ा	k?	ा
<g/>
ं	ं	k?	ं
<g/>
ध	ध	k?	ध
<g/>
ी	ी	k?	ी
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
Pórbandar	Pórbandar	k1gMnSc1	Pórbandar
<g/>
,	,	kIx,	,
Kathiawar	Kathiawar	k1gMnSc1	Kathiawar
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Indie	Indie	k1gFnSc1	Indie
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
politických	politický	k2eAgMnPc2d1	politický
a	a	k8xC	a
duchovních	duchovní	k2eAgMnPc2d1	duchovní
vůdců	vůdce	k1gMnPc2	vůdce
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
indického	indický	k2eAgNnSc2d1	indické
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Prosazoval	prosazovat	k5eAaImAgInS	prosazovat
filosofii	filosofie	k1gFnSc3	filosofie
aktivního	aktivní	k2eAgMnSc2d1	aktivní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenásilného	násilný	k2eNgInSc2d1	nenásilný
odporu	odpor	k1gInSc2	odpor
satjágraha	satjágrah	k1gMnSc2	satjágrah
<g/>
,	,	kIx,	,
založeného	založený	k2eAgInSc2d1	založený
na	na	k7c6	na
jogínském	jogínský	k2eAgInSc6d1	jogínský
principu	princip	k1gInSc6	princip
ahimsá	ahimsat	k5eAaImIp3nS	ahimsat
(	(	kIx(	(
<g/>
nenásilnost	nenásilnost	k1gFnSc1	nenásilnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
dovedl	dovést	k5eAaPmAgMnS	dovést
Indii	Indie	k1gFnSc4	Indie
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
pod	pod	k7c7	pod
přízviskem	přízvisko	k1gNnSc7	přízvisko
mahátma	mahátmum	k1gNnSc2	mahátmum
(	(	kIx(	(
<g/>
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
त	त	k?	त
<g/>
्	्	k?	्
<g/>
म	म	k?	म
<g/>
ा	ा	k?	ा
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Velký	velký	k2eAgMnSc1d1	velký
duch	duch	k1gMnSc1	duch
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
indický	indický	k2eAgMnSc1d1	indický
básník	básník	k1gMnSc1	básník
Rabíndranáth	Rabíndranáth	k1gMnSc1	Rabíndranáth
Thákur	Thákur	k1gMnSc1	Thákur
a	a	k8xC	a
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
také	také	k9	také
pod	pod	k7c7	pod
přízviskem	přízvisko	k1gNnSc7	přízvisko
bápú	bápú	k?	bápú
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
otec	otec	k1gMnSc1	otec
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
neformálně	formálně	k6eNd1	formálně
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Otec	otec	k1gMnSc1	otec
národa	národ	k1gInSc2	národ
<g/>
"	"	kIx"	"
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
jeho	jeho	k3xOp3gFnPc2	jeho
narozenin	narozeniny	k1gFnPc2	narozeniny
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
vzpomínán	vzpomínán	k2eAgMnSc1d1	vzpomínán
jako	jako	k8xS	jako
Gándhí	Gándhí	k2eAgMnSc1d1	Gándhí
džajanti	džajant	k1gMnPc1	džajant
(	(	kIx(	(
<g/>
Oslava	oslava	k1gFnSc1	oslava
Gándhího	Gándhí	k1gMnSc2	Gándhí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
přijala	přijmout	k5eAaPmAgFnS	přijmout
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
rezoluci	rezoluce	k1gFnSc3	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc4	říjen
za	za	k7c4	za
"	"	kIx"	"
<g/>
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
nenásilí	nenásilí	k1gNnSc2	nenásilí
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
občan	občan	k1gMnSc1	občan
anglické	anglický	k2eAgFnSc2d1	anglická
kolonie	kolonie	k1gFnSc2	kolonie
a	a	k8xC	a
z	z	k7c2	z
původně	původně	k6eAd1	původně
loajálního	loajální	k2eAgMnSc2d1	loajální
občana	občan	k1gMnSc2	občan
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgMnS	stát
vůdcem	vůdce	k1gMnSc7	vůdce
indického	indický	k2eAgInSc2d1	indický
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Pórbandaru	Pórbandar	k1gInSc6	Pórbandar
<g/>
,	,	kIx,	,
malém	malý	k2eAgInSc6d1	malý
přístavu	přístav	k1gInSc6	přístav
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Gudžáratu	Gudžárat	k1gInSc6	Gudžárat
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
syn	syn	k1gMnSc1	syn
obchodníka	obchodník	k1gMnSc2	obchodník
z	z	k7c2	z
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc2d1	vysoká
kasty	kasta	k1gFnSc2	kasta
banijá	banijat	k5eAaPmIp3nS	banijat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Karamčand	Karamčand	k1gInSc1	Karamčand
Gándhí	Gándhí	k1gFnSc2	Gándhí
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
nepracoval	pracovat	k5eNaImAgMnS	pracovat
jako	jako	k9	jako
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
byl	být	k5eAaImAgInS	být
v	v	k7c4	v
Pórbandaru	Pórbandara	k1gFnSc4	Pórbandara
dívánem	díván	k1gInSc7	díván
(	(	kIx(	(
<g/>
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
<g/>
)	)	kIx)	)
a	a	k8xC	a
pracoval	pracovat	k5eAaImAgMnS	pracovat
tedy	tedy	k9	tedy
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rodinném	rodinný	k2eAgInSc6d1	rodinný
domě	dům	k1gInSc6	dům
žilo	žít	k5eAaImAgNnS	žít
i	i	k9	i
pět	pět	k4xCc1	pět
otcových	otcův	k2eAgMnPc2d1	otcův
bratří	bratr	k1gMnPc2	bratr
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Gándhiové	Gándhius	k1gMnPc1	Gándhius
se	se	k3xPyFc4	se
hlásili	hlásit	k5eAaImAgMnP	hlásit
k	k	k7c3	k
višnuismu	višnuismus	k1gInSc3	višnuismus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
monoteistická	monoteistický	k2eAgFnSc1d1	monoteistická
forma	forma	k1gFnSc1	forma
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
modlitbu	modlitba	k1gFnSc4	modlitba
a	a	k8xC	a
zbožnost	zbožnost	k1gFnSc4	zbožnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
však	však	k9	však
přicházeli	přicházet	k5eAaImAgMnP	přicházet
i	i	k9	i
věřící	věřící	k1gMnPc1	věřící
jiných	jiný	k2eAgFnPc2d1	jiná
forem	forma	k1gFnPc2	forma
hinduismu	hinduismus	k1gInSc2	hinduismus
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
pársové	párs	k1gMnPc1	párs
a	a	k8xC	a
přívrženci	přívrženec	k1gMnPc1	přívrženec
džainismu	džainismus	k1gInSc2	džainismus
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
staré	starý	k2eAgNnSc1d1	staré
náboženství	náboženství	k1gNnSc1	náboženství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Gudžáratu	Gudžárat	k1gInSc6	Gudžárat
velmi	velmi	k6eAd1	velmi
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
a	a	k8xC	a
trvalo	trvat	k5eAaImAgNnS	trvat
na	na	k7c6	na
přísném	přísný	k2eAgNnSc6d1	přísné
nenásilí	nenásilí	k1gNnSc6	nenásilí
(	(	kIx(	(
<g/>
ahinsá	ahinsat	k5eAaPmIp3nS	ahinsat
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Putlíbáí	Putlíbáí	k1gFnSc2	Putlíbáí
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
zbožná	zbožný	k2eAgFnSc1d1	zbožná
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
Gándhího	Gándhí	k1gMnSc4	Gándhí
veliký	veliký	k2eAgInSc1d1	veliký
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
města	město	k1gNnSc2	město
Rajkot	Rajkot	k1gInSc4	Rajkot
<g/>
,	,	kIx,	,
politického	politický	k2eAgNnSc2d1	politické
střediska	středisko	k1gNnSc2	středisko
státu	stát	k1gInSc2	stát
Gudžárat	Gudžárat	k1gFnSc2	Gudžárat
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
otec	otec	k1gMnSc1	otec
smírčím	smírčí	k2eAgMnSc7d1	smírčí
soudcem	soudce	k1gMnSc7	soudce
a	a	k8xC	a
mediátorem	mediátor	k1gMnSc7	mediátor
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
zde	zde	k6eAd1	zde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
učil	učit	k5eAaImAgMnS	učit
se	se	k3xPyFc4	se
chápat	chápat	k5eAaImF	chápat
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vyjednávání	vyjednávání	k1gNnSc1	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgMnSc1d2	starší
muslimský	muslimský	k2eAgMnSc1d1	muslimský
přítel	přítel	k1gMnSc1	přítel
ho	on	k3xPp3gNnSc4	on
prý	prý	k9	prý
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ochutnal	ochutnat	k5eAaPmAgMnS	ochutnat
kozí	kozí	k2eAgNnSc4d1	kozí
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
višnuisté	višnuista	k1gMnPc1	višnuista
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
těžký	těžký	k2eAgInSc4d1	těžký
hřích	hřích	k1gInSc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
také	také	k9	také
víno	víno	k1gNnSc4	víno
a	a	k8xC	a
cigarety	cigareta	k1gFnPc4	cigareta
a	a	k8xC	a
bral	brát	k5eAaImAgMnS	brát
rodičům	rodič	k1gMnPc3	rodič
peníze	peníz	k1gInPc4	peníz
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
se	se	k3xPyFc4	se
pak	pak	k9	pak
velice	velice	k6eAd1	velice
styděl	stydět	k5eAaImAgMnS	stydět
a	a	k8xC	a
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
i	i	k9	i
o	o	k7c6	o
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
všechno	všechen	k3xTgNnSc4	všechen
napsal	napsat	k5eAaPmAgMnS	napsat
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
ho	on	k3xPp3gMnSc4	on
rodina	rodina	k1gFnSc1	rodina
zasnoubila	zasnoubit	k5eAaPmAgFnS	zasnoubit
s	s	k7c7	s
Kasturbou	Kasturba	k1gFnSc7	Kasturba
Maktahi	Maktah	k1gFnSc2	Maktah
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
kasty	kasta	k1gFnSc2	kasta
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
vzali	vzít	k5eAaPmAgMnP	vzít
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
jejich	jejich	k3xOp3gNnSc3	jejich
dítě	dítě	k1gNnSc1	dítě
brzy	brzy	k6eAd1	brzy
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
,	,	kIx,	,
následovali	následovat	k5eAaImAgMnP	následovat
ještě	ještě	k9	ještě
čtyři	čtyři	k4xCgMnPc1	čtyři
synové	syn	k1gMnPc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
složil	složit	k5eAaPmAgMnS	složit
Gándhí	Gándhí	k1gFnSc4	Gándhí
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úspěchem	úspěch	k1gInSc7	úspěch
maturitu	maturita	k1gFnSc4	maturita
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
studovat	studovat	k5eAaImF	studovat
medicínu	medicína	k1gFnSc4	medicína
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
ale	ale	k8xC	ale
kastovní	kastovní	k2eAgInPc1d1	kastovní
předpisy	předpis	k1gInPc1	předpis
nedovolovaly	dovolovat	k5eNaImAgInP	dovolovat
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
i	i	k9	i
nejstarší	starý	k2eAgMnSc1d3	nejstarší
bratr	bratr	k1gMnSc1	bratr
jako	jako	k8xS	jako
hlava	hlava	k1gFnSc1	hlava
rodu	rod	k1gInSc2	rod
mu	on	k3xPp3gMnSc3	on
rozmlouvali	rozmlouvat	k5eAaImAgMnP	rozmlouvat
záměr	záměr	k1gInSc4	záměr
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
po	po	k7c6	po
semestru	semestr	k1gInSc6	semestr
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
v	v	k7c6	v
Bhavanagaru	Bhavanagar	k1gInSc6	Bhavanagar
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kastě	kasta	k1gFnSc6	kasta
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
se	se	k3xPyFc4	se
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
i	i	k9	i
kastovní	kastovní	k2eAgNnSc4d1	kastovní
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
složil	složit	k5eAaPmAgMnS	složit
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
zůstane	zůstat	k5eAaPmIp3nS	zůstat
hinduistou	hinduista	k1gMnSc7	hinduista
a	a	k8xC	a
nepodlehne	podlehnout	k5eNaPmIp3nS	podlehnout
svodům	svod	k1gInPc3	svod
západního	západní	k2eAgInSc2d1	západní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
shromáždění	shromáždění	k1gNnSc2	shromáždění
mu	on	k3xPp3gMnSc3	on
cestu	cesta	k1gFnSc4	cesta
neschválilo	schválit	k5eNaPmAgNnS	schválit
a	a	k8xC	a
z	z	k7c2	z
kasty	kasta	k1gFnSc2	kasta
ho	on	k3xPp3gMnSc4	on
vyloučilo	vyloučit	k5eAaPmAgNnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
tedy	tedy	k9	tedy
nadále	nadále	k6eAd1	nadále
mimo	mimo	k7c4	mimo
kasty	kasta	k1gFnPc4	kasta
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
nesnázemi	nesnáz	k1gFnPc7	nesnáz
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
průmyslovém	průmyslový	k2eAgInSc6d1	průmyslový
a	a	k8xC	a
sociálním	sociální	k2eAgInSc6d1	sociální
rozmachu	rozmach	k1gInSc6	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
právnickou	právnický	k2eAgFnSc4d1	právnická
školu	škola	k1gFnSc4	škola
Inner	Innra	k1gFnPc2	Innra
Temple	templ	k1gInSc5	templ
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
západní	západní	k2eAgFnSc1d1	západní
společnost	společnost	k1gFnSc1	společnost
důkladně	důkladně	k6eAd1	důkladně
poznat	poznat	k5eAaPmF	poznat
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
velice	velice	k6eAd1	velice
strádal	strádat	k5eAaImAgMnS	strádat
<g/>
.	.	kIx.	.
</s>
<s>
Bydlel	bydlet	k5eAaImAgMnS	bydlet
u	u	k7c2	u
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
požadovala	požadovat	k5eAaImAgFnS	požadovat
příliš	příliš	k6eAd1	příliš
vysokou	vysoký	k2eAgFnSc4d1	vysoká
činži	činže	k1gFnSc4	činže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zejména	zejména	k9	zejména
jej	on	k3xPp3gMnSc4	on
coby	coby	k?	coby
vegetariána	vegetarián	k1gMnSc2	vegetarián
krmila	krmit	k5eAaImAgFnS	krmit
mdlou	mdlý	k2eAgFnSc7d1	mdlá
vegetariánskou	vegetariánský	k2eAgFnSc7d1	vegetariánská
stravou	strava	k1gFnSc7	strava
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nemohl	moct	k5eNaImAgMnS	moct
ani	ani	k8xC	ani
pozřít	pozřít	k5eAaPmF	pozřít
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
tak	tak	k6eAd1	tak
trpěl	trpět	k5eAaImAgInS	trpět
hladem	hlad	k1gInSc7	hlad
<g/>
,	,	kIx,	,
než	než	k8xS	než
našel	najít	k5eAaPmAgMnS	najít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
londýnských	londýnský	k2eAgFnPc2d1	londýnská
vegetariánských	vegetariánský	k2eAgFnPc2d1	vegetariánská
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
měl	mít	k5eAaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gInPc4	on
vychovávat	vychovávat	k5eAaImF	vychovávat
reformním	reformní	k2eAgInSc7d1	reformní
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
velice	velice	k6eAd1	velice
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
výchova	výchova	k1gFnSc1	výchova
byla	být	k5eAaImAgFnS	být
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
.	.	kIx.	.
</s>
<s>
Učil	učít	k5eAaPmAgMnS	učít
je	být	k5eAaImIp3nS	být
tělocviku	tělocvik	k1gInSc2	tělocvik
<g/>
,	,	kIx,	,
otužování	otužování	k1gNnSc2	otužování
i	i	k8xC	i
životosprávě	životospráva	k1gFnSc3	životospráva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
u	u	k7c2	u
Indů	Indus	k1gInPc2	Indus
značně	značně	k6eAd1	značně
zanedbaná	zanedbaný	k2eAgFnSc1d1	zanedbaná
<g/>
.	.	kIx.	.
</s>
<s>
Důkladně	důkladně	k6eAd1	důkladně
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
různými	různý	k2eAgNnPc7d1	různé
náboženstvími	náboženství	k1gNnPc7	náboženství
a	a	k8xC	a
velký	velký	k2eAgInSc1d1	velký
dojem	dojem	k1gInSc1	dojem
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
udělala	udělat	k5eAaPmAgFnS	udělat
evangelia	evangelium	k1gNnSc2	evangelium
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Ježíšova	Ježíšův	k2eAgFnSc1d1	Ježíšova
"	"	kIx"	"
<g/>
Horská	horský	k2eAgFnSc1d1	horská
řeč	řeč	k1gFnSc1	řeč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Četl	číst	k5eAaImAgMnS	číst
také	také	k9	také
Korán	korán	k2eAgMnSc1d1	korán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
základní	základní	k2eAgInSc1d1	základní
buddhistický	buddhistický	k2eAgInSc1d1	buddhistický
text	text	k1gInSc1	text
<g/>
,	,	kIx,	,
Bhagavadgíta	Bhagavadgíta	k1gMnSc1	Bhagavadgíta
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
gudžáratského	gudžáratský	k2eAgInSc2d1	gudžáratský
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
čítal	čítat	k5eAaImAgInS	čítat
pak	pak	k6eAd1	pak
denně	denně	k6eAd1	denně
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgInS	seznámit
se	se	k3xPyFc4	se
také	také	k9	také
se	s	k7c7	s
západními	západní	k2eAgInPc7d1	západní
směry	směr	k1gInPc7	směr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
liberalismus	liberalismus	k1gInSc1	liberalismus
<g/>
,	,	kIx,	,
ateismus	ateismus	k1gInSc1	ateismus
<g/>
,	,	kIx,	,
pacifismus	pacifismus	k1gInSc1	pacifismus
nebo	nebo	k8xC	nebo
socialismus	socialismus	k1gInSc1	socialismus
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
teosofií	teosofie	k1gFnSc7	teosofie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
složil	složit	k5eAaPmAgMnS	složit
advokátské	advokátský	k2eAgFnPc4d1	advokátská
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
mohl	moct	k5eAaImAgInS	moct
tedy	tedy	k9	tedy
působit	působit	k5eAaImF	působit
jako	jako	k9	jako
advokát	advokát	k1gMnSc1	advokát
kdekoli	kdekoli	k6eAd1	kdekoli
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
platilo	platit	k5eAaImAgNnS	platit
britské	britský	k2eAgNnSc4d1	Britské
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
matka	matka	k1gFnSc1	matka
mezitím	mezitím	k6eAd1	mezitím
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
ho	on	k3xPp3gMnSc4	on
poslala	poslat	k5eAaPmAgFnS	poslat
do	do	k7c2	do
jihoafrické	jihoafrický	k2eAgFnSc2d1	Jihoafrická
Pretorie	Pretorie	k1gFnSc2	Pretorie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgInS	mít
pomoci	pomoct	k5eAaPmF	pomoct
spřátelenému	spřátelený	k2eAgMnSc3d1	spřátelený
indickému	indický	k2eAgMnSc3d1	indický
kupci	kupec	k1gMnSc3	kupec
vymáhat	vymáhat	k5eAaImF	vymáhat
dlužné	dlužný	k2eAgFnPc4d1	dlužná
pohledávky	pohledávka	k1gFnPc4	pohledávka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
velikou	veliký	k2eAgFnSc4d1	veliká
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
Gándhímu	Gándhí	k1gMnSc3	Gándhí
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zprostředkovat	zprostředkovat	k5eAaPmF	zprostředkovat
mimosoudní	mimosoudní	k2eAgFnSc4d1	mimosoudní
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
však	však	k9	však
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
rasovou	rasový	k2eAgFnSc7d1	rasová
diskriminací	diskriminace	k1gFnSc7	diskriminace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
dlouho	dlouho	k6eAd1	dlouho
nechápal	chápat	k5eNaImAgInS	chápat
a	a	k8xC	a
pak	pak	k6eAd1	pak
ostře	ostro	k6eAd1	ostro
odmítal	odmítat	k5eAaImAgMnS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
fungovalo	fungovat	k5eAaImAgNnS	fungovat
tzv.	tzv.	kA	tzv.
smluvní	smluvní	k2eAgNnSc1d1	smluvní
dělnictvo	dělnictvo	k1gNnSc1	dělnictvo
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
přijeli	přijet	k5eAaPmAgMnP	přijet
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
jako	jako	k8xS	jako
smluvní	smluvní	k2eAgMnSc1d1	smluvní
dělníci	dělník	k1gMnPc1	dělník
a	a	k8xC	a
po	po	k7c6	po
jejich	jejich	k3xOp3gNnSc6	jejich
odpracování	odpracování	k1gNnSc6	odpracování
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
svobodnými	svobodný	k2eAgMnPc7d1	svobodný
občany	občan	k1gMnPc7	občan
Jihoafrického	jihoafrický	k2eAgInSc2d1	jihoafrický
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
Indové	Ind	k1gMnPc1	Ind
se	se	k3xPyFc4	se
však	však	k9	však
stali	stát	k5eAaPmAgMnP	stát
předmětem	předmět	k1gInSc7	předmět
rasové	rasový	k2eAgFnSc2d1	rasová
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
Museli	muset	k5eAaImAgMnP	muset
například	například	k6eAd1	například
platit	platit	k5eAaImF	platit
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
příjezdu	příjezd	k1gInSc6	příjezd
se	se	k3xPyFc4	se
Gándhí	Gándhí	k1gMnSc1	Gándhí
stal	stát	k5eAaPmAgMnS	stát
středem	středem	k7c2	středem
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
,	,	kIx,	,
když	když	k8xS	když
ostře	ostro	k6eAd1	ostro
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
proti	proti	k7c3	proti
zákazu	zákaz	k1gInSc3	zákaz
nošení	nošení	k1gNnSc2	nošení
turbanů	turban	k1gInPc2	turban
v	v	k7c6	v
soudní	soudní	k2eAgFnSc6d1	soudní
síni	síň	k1gFnSc6	síň
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgMnS	psát
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
stál	stát	k5eAaImAgInS	stát
si	se	k3xPyFc3	se
tvrdě	tvrdě	k6eAd1	tvrdě
za	za	k7c7	za
svým	svůj	k3xOyFgMnSc7	svůj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
Natalský	natalský	k2eAgInSc1d1	natalský
indický	indický	k2eAgInSc4d1	indický
kongres	kongres	k1gInSc4	kongres
a	a	k8xC	a
jako	jako	k9	jako
jeho	jeho	k3xOp3gMnSc1	jeho
tajemník	tajemník	k1gMnSc1	tajemník
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
rasismu	rasismus	k1gInSc3	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
vymezoval	vymezovat	k5eAaImAgMnS	vymezovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
proti	proti	k7c3	proti
útlaku	útlak	k1gInSc3	útlak
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
sdílel	sdílet	k5eAaImAgMnS	sdílet
rasové	rasový	k2eAgInPc4d1	rasový
předsudky	předsudek	k1gInPc4	předsudek
ohledně	ohledně	k7c2	ohledně
jiných	jiný	k2eAgNnPc2d1	jiné
etnik	etnikum	k1gNnPc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
dokládá	dokládat	k5eAaImIp3nS	dokládat
též	též	k9	též
následující	následující	k2eAgInSc4d1	následující
citát	citát	k1gInSc4	citát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Negři	negr	k1gMnPc1	negr
a	a	k8xC	a
čínští	čínský	k2eAgMnPc1d1	čínský
vězňové	vězeň	k1gMnPc1	vězeň
jsou	být	k5eAaImIp3nP	být
divocí	divoký	k2eAgMnPc1d1	divoký
vrahové	vrah	k1gMnPc1	vrah
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
amorální	amorální	k2eAgFnPc1d1	amorální
<g/>
.	.	kIx.	.
</s>
<s>
Negři	negr	k1gMnPc1	negr
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
vztáhnout	vztáhnout	k5eAaPmF	vztáhnout
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
<g/>
,	,	kIx,	,
necivilizovaní	civilizovaný	k2eNgMnPc1d1	necivilizovaný
a	a	k8xC	a
obvinění	obviněný	k1gMnPc1	obviněný
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgFnPc1d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
problematičtí	problematický	k2eAgMnPc1d1	problematický
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
špinaví	špinavět	k5eAaImIp3nP	špinavět
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
skoro	skoro	k6eAd1	skoro
jako	jako	k9	jako
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Časem	časem	k6eAd1	časem
však	však	k9	však
své	svůj	k3xOyFgInPc4	svůj
prvotní	prvotní	k2eAgInPc4d1	prvotní
dojmy	dojem	k1gInPc4	dojem
podrobil	podrobit	k5eAaPmAgMnS	podrobit
reflexi	reflexe	k1gFnSc4	reflexe
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
coby	coby	k?	coby
vůdce	vůdce	k1gMnSc1	vůdce
indického	indický	k2eAgNnSc2d1	indické
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k8xC	jako
odpůrce	odpůrce	k1gMnSc1	odpůrce
všeho	všecek	k3xTgInSc2	všecek
rasismu	rasismus	k1gInSc2	rasismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
lze	lze	k6eAd1	lze
jeho	jeho	k3xOp3gInSc4	jeho
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
domorodým	domorodý	k2eAgMnPc3d1	domorodý
Afričanům	Afričan	k1gMnPc3	Afričan
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
označit	označit	k5eAaPmF	označit
jako	jako	k8xS	jako
kolísavý	kolísavý	k2eAgMnSc1d1	kolísavý
<g/>
.	.	kIx.	.
</s>
<s>
Vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
na	na	k7c4	na
poměry	poměr	k1gInPc4	poměr
Indů	Ind	k1gMnPc2	Ind
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
články	článek	k1gInPc1	článek
vydaly	vydat	k5eAaPmAgInP	vydat
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
časopisy	časopis	k1gInPc1	časopis
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
rozletěla	rozletět	k5eAaPmAgFnS	rozletět
celou	celý	k2eAgFnSc7d1	celá
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
dostal	dostat	k5eAaPmAgMnS	dostat
kabelovou	kabelový	k2eAgFnSc4d1	kabelová
zprávu	zpráva	k1gFnSc4	zpráva
z	z	k7c2	z
Natalu	Natal	k1gInSc2	Natal
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
vyzývající	vyzývající	k2eAgMnPc4d1	vyzývající
ho	on	k3xPp3gInSc2	on
k	k	k7c3	k
neprodlenému	prodlený	k2eNgInSc3d1	neprodlený
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
tam	tam	k6eAd1	tam
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
parníkem	parník	k1gInSc7	parník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ten	ten	k3xDgInSc4	ten
okamžik	okamžik	k1gInSc4	okamžik
všechno	všechen	k3xTgNnSc1	všechen
začalo	začít	k5eAaPmAgNnS	začít
<g/>
.	.	kIx.	.
</s>
<s>
Gándhího	Gándhí	k1gMnSc4	Gándhí
reference	reference	k1gFnSc2	reference
o	o	k7c4	o
zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
Indy	Indus	k1gInPc7	Indus
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
zkomolením	zkomolení	k1gNnSc7	zkomolení
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Gándhí	Gándhí	k1gFnSc2	Gándhí
nepřítelem	nepřítel	k1gMnSc7	nepřítel
vlády	vláda	k1gFnSc2	vláda
i	i	k8xC	i
Indů	Indus	k1gInPc2	Indus
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
očekávali	očekávat	k5eAaImAgMnP	očekávat
ještě	ještě	k6eAd1	ještě
horší	zlý	k2eAgNnSc4d2	horší
zacházení	zacházení	k1gNnSc4	zacházení
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
parníky	parník	k1gInPc1	parník
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
úřady	úřad	k1gInPc1	úřad
za	za	k7c4	za
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Natalu	Natal	k1gInSc2	Natal
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
přistát	přistát	k5eAaImF	přistát
pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
dýmějového	dýmějový	k2eAgInSc2d1	dýmějový
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
vypukl	vypuknout	k5eAaPmAgMnS	vypuknout
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
karanténní	karanténní	k2eAgFnSc6d1	karanténní
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
dokázáno	dokázat	k5eAaPmNgNnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nemoc	nemoc	k1gFnSc4	nemoc
nevezou	vézt	k5eNaImIp3nP	vézt
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
úřady	úřad	k1gInPc1	úřad
jednat	jednat	k5eAaImF	jednat
zoufale	zoufale	k6eAd1	zoufale
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrožovaly	vyhrožovat	k5eAaImAgFnP	vyhrožovat
Indům	Ind	k1gMnPc3	Ind
svržením	svržení	k1gNnPc3	svržení
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
nezalekli	zaleknout	k5eNaPmAgMnP	zaleknout
a	a	k8xC	a
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
přistát	přistát	k5eAaPmF	přistát
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
měli	mít	k5eAaImAgMnP	mít
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
výhrůžky	výhrůžka	k1gFnPc1	výhrůžka
se	se	k3xPyFc4	se
neuskutečnily	uskutečnit	k5eNaPmAgFnP	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
přítele	přítel	k1gMnSc2	přítel
se	se	k3xPyFc4	se
k	k	k7c3	k
večeru	večer	k1gInSc3	večer
vylodil	vylodit	k5eAaPmAgMnS	vylodit
i	i	k9	i
Gándhí	Gándhí	k1gMnSc1	Gándhí
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgInS	být
poznán	poznat	k5eAaPmNgInS	poznat
a	a	k8xC	a
pronásledován	pronásledovat	k5eAaImNgInS	pronásledovat
rozzuřeným	rozzuřený	k2eAgInSc7d1	rozzuřený
davem	dav	k1gInSc7	dav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
dostihl	dostihnout	k5eAaPmAgInS	dostihnout
a	a	k8xC	a
zmlátil	zmlátit	k5eAaPmAgInS	zmlátit
takřka	takřka	k6eAd1	takřka
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
<g/>
.	.	kIx.	.
</s>
<s>
Zachránila	zachránit	k5eAaPmAgFnS	zachránit
jej	on	k3xPp3gNnSc4	on
stará	starý	k2eAgFnSc1d1	stará
známá	známý	k2eAgFnSc1d1	známá
paní	paní	k1gFnSc1	paní
Alexandrová	Alexandrová	k1gFnSc1	Alexandrová
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
neodvážil	odvážit	k5eNaPmAgInS	odvážit
napadnout	napadnout	k5eAaPmF	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
Útočiště	útočiště	k1gNnSc4	útočiště
nalezl	nalézt	k5eAaBmAgInS	nalézt
u	u	k7c2	u
přítele	přítel	k1gMnSc4	přítel
Rustomdži	Rustomdž	k1gFnSc3	Rustomdž
Šetha	Šetha	k1gFnSc1	Šetha
<g/>
.	.	kIx.	.
</s>
<s>
Leč	leč	k8xC	leč
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
obklíčen	obklíčen	k2eAgInSc1d1	obklíčen
Evropany	Evropan	k1gMnPc7	Evropan
a	a	k8xC	a
lůzou	lůza	k1gFnSc7	lůza
<g/>
.	.	kIx.	.
</s>
<s>
Vyhrožovali	vyhrožovat	k5eAaImAgMnP	vyhrožovat
podpálit	podpálit	k5eAaPmF	podpálit
dům	dům	k1gInSc4	dům
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Šetha	Šetha	k1gFnSc1	Šetha
Gándhího	Gándhí	k1gMnSc2	Gándhí
nevydá	vydat	k5eNaPmIp3nS	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
lsti	lest	k1gFnSc2	lest
a	a	k8xC	a
policie	policie	k1gFnSc2	policie
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
policejní	policejní	k2eAgFnSc2d1	policejní
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
napsal	napsat	k5eAaPmAgMnS	napsat
nový	nový	k2eAgInSc4d1	nový
článek	článek	k1gInSc4	článek
do	do	k7c2	do
místních	místní	k2eAgFnPc2d1	místní
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvedl	uvést	k5eAaPmAgMnS	uvést
zkomolené	zkomolený	k2eAgFnPc4d1	zkomolená
informace	informace	k1gFnPc4	informace
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
uznali	uznat	k5eAaPmAgMnP	uznat
svůj	svůj	k3xOyFgInSc4	svůj
omyl	omyl	k1gInSc4	omyl
a	a	k8xC	a
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
anglického	anglický	k2eAgNnSc2d1	anglické
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
obrátilo	obrátit	k5eAaPmAgNnS	obrátit
v	v	k7c4	v
dobré	dobrý	k2eAgNnSc4d1	dobré
<g/>
.	.	kIx.	.
</s>
<s>
Búrské	búrský	k2eAgFnPc1d1	búrská
války	válka	k1gFnPc1	válka
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Gándhího	Gándhí	k1gMnSc2	Gándhí
se	se	k3xPyFc4	se
po	po	k7c6	po
důkladném	důkladný	k2eAgNnSc6d1	důkladné
uvážení	uvážení	k1gNnSc6	uvážení
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
občané	občan	k1gMnPc1	občan
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Búry	Búr	k1gMnPc7	Búr
pomoci	pomoct	k5eAaPmF	pomoct
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
dokáže	dokázat	k5eAaPmIp3nS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
křivdy	křivda	k1gFnPc1	křivda
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Británie	Británie	k1gFnSc2	Británie
jsou	být	k5eAaImIp3nP	být
neoprávněné	oprávněný	k2eNgNnSc1d1	neoprávněné
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
nabídka	nabídka	k1gFnSc1	nabídka
pomoci	pomoc	k1gFnSc2	pomoc
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
a	a	k8xC	a
přijata	přijat	k2eAgFnSc1d1	přijata
a	a	k8xC	a
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
války	válka	k1gFnSc2	válka
byli	být	k5eAaImAgMnP	být
Indové	Ind	k1gMnPc1	Ind
uznáváni	uznávat	k5eAaImNgMnP	uznávat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
pomoc	pomoc	k1gFnSc1	pomoc
byla	být	k5eAaImAgFnS	být
vysloveně	vysloveně	k6eAd1	vysloveně
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
Angličanů	Angličan	k1gMnPc2	Angličan
se	se	k3xPyFc4	se
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
výpomoci	výpomoc	k1gFnSc2	výpomoc
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
ambulantních	ambulantní	k2eAgFnPc6d1	ambulantní
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
se	se	k3xPyFc4	se
i	i	k9	i
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
přidali	přidat	k5eAaPmAgMnP	přidat
k	k	k7c3	k
záložním	záložní	k2eAgInPc3d1	záložní
plukům	pluk	k1gInPc3	pluk
<g/>
.	.	kIx.	.
</s>
<s>
Proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
Gándhí	Gándhí	k1gMnSc1	Gándhí
<g/>
,	,	kIx,	,
vyznavač	vyznavač	k1gMnSc1	vyznavač
nenásilí	nenásilí	k1gNnSc2	nenásilí
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
angažovat	angažovat	k5eAaBmF	angažovat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vysvětlíme	vysvětlit	k5eAaPmIp1nP	vysvětlit
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
pravil	pravit	k5eAaImAgMnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgMnS	být
postaven	postavit	k5eAaPmNgMnS	postavit
před	před	k7c4	před
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
mezi	mezi	k7c7	mezi
zbabělostí	zbabělost	k1gFnSc7	zbabělost
a	a	k8xC	a
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
zvolil	zvolit	k5eAaPmAgMnS	zvolit
bych	by	kYmCp1nS	by
násilí	násilí	k1gNnSc3	násilí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Gándhí	Gándhí	k1gFnSc3	Gándhí
se	se	k3xPyFc4	se
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Johannesburgu	Johannesburg	k1gInSc6	Johannesburg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
právnické	právnický	k2eAgFnSc6d1	právnická
praxi	praxe	k1gFnSc6	praxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
časopis	časopis	k1gInSc1	časopis
Indian	Indiana	k1gFnPc2	Indiana
Opinion	Opinion	k1gInSc1	Opinion
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Indický	indický	k2eAgInSc1d1	indický
názor	názor	k1gInSc1	názor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
obohacovalo	obohacovat	k5eAaImAgNnS	obohacovat
a	a	k8xC	a
dávalo	dávat	k5eAaImAgNnS	dávat
mu	on	k3xPp3gMnSc3	on
možnost	možnost	k1gFnSc4	možnost
získat	získat	k5eAaPmF	získat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
přirovnával	přirovnávat	k5eAaImAgMnS	přirovnávat
situaci	situace	k1gFnSc4	situace
Indů	Indus	k1gInPc2	Indus
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
k	k	k7c3	k
Židům	Žid	k1gMnPc3	Žid
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nedotknutelní	Nedotknutelní	k?	Nedotknutelní
kulíové	kulíus	k1gMnPc1	kulíus
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
osadách	osada	k1gFnPc6	osada
podobných	podobný	k2eAgFnPc6d1	podobná
ghettům	ghetto	k1gNnPc3	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Úřady	úřad	k1gInPc1	úřad
vůbec	vůbec	k9	vůbec
nepřispívaly	přispívat	k5eNaImAgInP	přispívat
ke	k	k7c3	k
správě	správa	k1gFnSc3	správa
a	a	k8xC	a
podpoře	podpora	k1gFnSc3	podpora
těchto	tento	k3xDgFnPc2	tento
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
plné	plný	k2eAgFnPc4d1	plná
špíny	špína	k1gFnPc4	špína
a	a	k8xC	a
odpadu	odpad	k1gInSc2	odpad
<g/>
.	.	kIx.	.
</s>
<s>
Vypukl	vypuknout	k5eAaPmAgInS	vypuknout
černý	černý	k2eAgInSc1d1	černý
mor	mor	k1gInSc1	mor
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
zdroj	zdroj	k1gInSc1	zdroj
sice	sice	k8xC	sice
nebyl	být	k5eNaImAgInS	být
v	v	k7c6	v
kulíjských	kulíjský	k2eAgNnPc6d1	kulíjský
ghettech	ghetto	k1gNnPc6	ghetto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uchytil	uchytit	k5eAaPmAgMnS	uchytit
kvůli	kvůli	k7c3	kvůli
špatné	špatný	k2eAgFnSc3d1	špatná
hygieně	hygiena	k1gFnSc3	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pomoci	pomoct	k5eAaPmF	pomoct
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
a	a	k8xC	a
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
se	se	k3xPyFc4	se
černý	černý	k2eAgInSc1d1	černý
mor	mor	k1gInSc1	mor
nadále	nadále	k6eAd1	nadále
nešířil	šířit	k5eNaImAgInS	šířit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obětí	oběť	k1gFnPc2	oběť
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
incidentu	incident	k1gInSc6	incident
se	se	k3xPyFc4	se
Gándhí	Gándhí	k1gMnSc1	Gándhí
domluvil	domluvit	k5eAaPmAgMnS	domluvit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
dávným	dávný	k2eAgMnSc7d1	dávný
přítelem	přítel	k1gMnSc7	přítel
Westem	West	k1gMnSc7	West
na	na	k7c4	na
vydávání	vydávání	k1gNnSc4	vydávání
svého	svůj	k3xOyFgInSc2	svůj
časopisu	časopis	k1gInSc2	časopis
také	také	k9	také
v	v	k7c6	v
Durbanu	Durban	k1gInSc6	Durban
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
West	West	k1gMnSc1	West
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
tiskárnu	tiskárna	k1gFnSc4	tiskárna
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
založil	založit	k5eAaPmAgMnS	založit
r.	r.	kA	r.
1904	[number]	k4	1904
v	v	k7c6	v
Phoenixu	Phoenix	k1gInSc6	Phoenix
osadu	osada	k1gFnSc4	osada
určenou	určený	k2eAgFnSc7d1	určená
jen	jen	k9	jen
svým	svůj	k3xOyFgMnPc3	svůj
přívržencům	přívrženec	k1gMnPc3	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
vzešla	vzejít	k5eAaPmAgFnS	vzejít
z	z	k7c2	z
Ruskinovy	Ruskinův	k2eAgFnSc2d1	Ruskinův
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
velice	velice	k6eAd1	velice
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
a	a	k8xC	a
která	který	k3yRgFnSc1	který
změnila	změnit	k5eAaPmAgFnS	změnit
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Svému	svůj	k3xOyFgInSc3	svůj
časopisu	časopis	k1gInSc3	časopis
pak	pak	k6eAd1	pak
věnoval	věnovat	k5eAaPmAgMnS	věnovat
veškerou	veškerý	k3xTgFnSc4	veškerý
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
potýkal	potýkat	k5eAaImAgMnS	potýkat
s	s	k7c7	s
nemalými	malý	k2eNgInPc7d1	nemalý
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osadě	osada	k1gFnSc6	osada
ve	v	k7c6	v
Phoenixu	Phoenix	k1gInSc6	Phoenix
se	se	k3xPyFc4	se
Indický	indický	k2eAgInSc1d1	indický
názor	názor	k1gInSc1	názor
tiskne	tisknout	k5eAaImIp3nS	tisknout
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1906	[number]	k4	1906
zabili	zabít	k5eAaPmAgMnP	zabít
Zuluové	Zulu	k1gMnPc1	Zulu
dva	dva	k4xCgMnPc4	dva
policisty	policista	k1gMnPc4	policista
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
nově	nova	k1gFnSc3	nova
zavedené	zavedený	k2eAgFnSc3d1	zavedená
dani	daň	k1gFnSc3	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
postavil	postavit	k5eAaPmAgMnS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Británie	Británie	k1gFnSc2	Británie
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
nadějí	naděje	k1gFnSc7	naděje
jako	jako	k8xS	jako
v	v	k7c6	v
Búrských	búrský	k2eAgFnPc6d1	búrská
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Pomáhal	pomáhat	k5eAaImAgMnS	pomáhat
oběma	dva	k4xCgFnPc3	dva
stranám	strana	k1gFnPc3	strana
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
vůdcem	vůdce	k1gMnSc7	vůdce
ambulantní	ambulantní	k2eAgFnSc2d1	ambulantní
čety	četa	k1gFnSc2	četa
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
nosili	nosit	k5eAaImAgMnP	nosit
raněné	raněný	k2eAgFnPc4d1	raněná
i	i	k9	i
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
mil	míle	k1gFnPc2	míle
pěšky	pěšky	k6eAd1	pěšky
<g/>
,	,	kIx,	,
zachránili	zachránit	k5eAaPmAgMnP	zachránit
mnoho	mnoho	k4c4	mnoho
životů	život	k1gInPc2	život
a	a	k8xC	a
načerpali	načerpat	k5eAaPmAgMnP	načerpat
mnoho	mnoho	k4c4	mnoho
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
Anglii	Anglie	k1gFnSc4	Anglie
opět	opět	k6eAd1	opět
nepřinesla	přinést	k5eNaPmAgNnP	přinést
žádnou	žádný	k3yNgFnSc4	žádný
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
dnech	den	k1gInPc6	den
si	se	k3xPyFc3	se
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc7	jeho
smyslem	smysl	k1gInSc7	smysl
je	být	k5eAaImIp3nS	být
sloužit	sloužit	k5eAaImF	sloužit
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Odříká	odříkat	k5eAaImIp3nS	odříkat
si	se	k3xPyFc3	se
pozemské	pozemský	k2eAgInPc4d1	pozemský
požitky	požitek	k1gInPc4	požitek
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
slib	slib	k1gInSc4	slib
doživotní	doživotní	k2eAgFnSc4d1	doživotní
brahmačarji	brahmačarje	k1gFnSc4	brahmačarje
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gNnSc2	on
formovat	formovat	k5eAaImF	formovat
pojem	pojem	k1gInSc4	pojem
satjágrahy	satjágraha	k1gFnSc2	satjágraha
čili	čili	k8xC	čili
trvání	trvání	k1gNnSc2	trvání
na	na	k7c6	na
pravdě	pravda	k1gFnSc6	pravda
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
nahradit	nahradit	k5eAaPmF	nahradit
pojem	pojem	k1gInSc4	pojem
pasivního	pasivní	k2eAgInSc2d1	pasivní
odporu	odpor	k1gInSc2	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
totiž	totiž	k9	totiž
odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
doktrínu	doktrína	k1gFnSc4	doktrína
meče	meč	k1gInSc2	meč
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
hlásaje	hlásat	k5eAaImSgMnS	hlásat
že	že	k9	že
jediná	jediný	k2eAgFnSc1d1	jediná
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nenásilí	nenásilí	k1gNnSc6	nenásilí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
možnost	možnost	k1gFnSc1	možnost
násilí	násilí	k1gNnSc4	násilí
samo	sám	k3xTgNnSc1	sám
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
,	,	kIx,	,
ne	ne	k9	ne
v	v	k7c6	v
takovém	takový	k3xDgNnSc6	takový
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
násilí	násilí	k1gNnSc1	násilí
vykonávat	vykonávat	k5eAaImF	vykonávat
nemůže	moct	k5eNaImIp3nS	moct
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Johannesburgu	Johannesburg	k1gInSc6	Johannesburg
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
Durbanu	Durban	k1gInSc6	Durban
těžce	těžce	k6eAd1	těžce
onemocněla	onemocnět	k5eAaPmAgFnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
postoj	postoj	k1gInSc1	postoj
a	a	k8xC	a
víra	víra	k1gFnSc1	víra
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgInP	projevit
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nedovolil	dovolit	k5eNaPmAgMnS	dovolit
bych	by	kYmCp1nS	by
nikdy	nikdy	k6eAd1	nikdy
<g/>
,	,	kIx,	,
abyste	aby	kYmCp2nP	aby
mé	můj	k3xOp1gFnSc6	můj
ženě	žena	k1gFnSc6	žena
dávali	dávat	k5eAaImAgMnP	dávat
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
i	i	k8xC	i
kdyby	kdyby	kYmCp3nS	kdyby
jí	on	k3xPp3gFnSc2	on
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
ho	on	k3xPp3gInSc4	on
ovšem	ovšem	k9	ovšem
sama	sám	k3xTgFnSc1	sám
přála	přát	k5eAaImAgFnS	přát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ta	ten	k3xDgFnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
maso	maso	k1gNnSc1	maso
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
z	z	k7c2	z
lásky	láska	k1gFnSc2	láska
ke	k	k7c3	k
svému	svůj	k1gMnSc3	svůj
muži	muž	k1gMnSc3	muž
a	a	k8xC	a
Gándhi	Gándh	k1gMnSc3	Gándh
ji	on	k3xPp3gFnSc4	on
odvedl	odvést	k5eAaPmAgMnS	odvést
z	z	k7c2	z
léčby	léčba	k1gFnSc2	léčba
u	u	k7c2	u
lékaře	lékař	k1gMnSc2	lékař
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
považoval	považovat	k5eAaImAgMnS	považovat
maso	maso	k1gNnSc4	maso
za	za	k7c4	za
nezbytné	zbytný	k2eNgNnSc4d1	nezbytné
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
uzdravení	uzdravení	k1gNnSc3	uzdravení
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
Gándhi	Gándhe	k1gFnSc4	Gándhe
pravil	pravit	k5eAaImAgMnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bral	brát	k5eAaImAgMnS	brát
jsem	být	k5eAaImIp1nS	být
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
opravdu	opravdu	k6eAd1	opravdu
velkou	velký	k2eAgFnSc4d1	velká
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
důvěřoval	důvěřovat	k5eAaImAgMnS	důvěřovat
jsem	být	k5eAaImIp1nS	být
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Manželka	manželka	k1gFnSc1	manželka
Kasturba	Kasturba	k1gFnSc1	Kasturba
se	se	k3xPyFc4	se
však	však	k9	však
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
utrpení	utrpení	k1gNnSc6	utrpení
skutečně	skutečně	k6eAd1	skutečně
začala	začít	k5eAaPmAgFnS	začít
uzdravovat	uzdravovat	k5eAaImF	uzdravovat
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
začal	začít	k5eAaPmAgInS	začít
učit	učit	k5eAaImF	učit
mladé	mladý	k2eAgMnPc4d1	mladý
hochy	hoch	k1gMnPc4	hoch
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
poblíž	poblíž	k7c2	poblíž
Johanesburgu	Johanesburg	k1gInSc2	Johanesburg
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
zpětně	zpětně	k6eAd1	zpětně
mnohému	mnohé	k1gNnSc3	mnohé
přiučil	přiučit	k5eAaPmAgInS	přiučit
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
podobnou	podobný	k2eAgFnSc4d1	podobná
teorii	teorie	k1gFnSc4	teorie
jakou	jaký	k3yQgFnSc4	jaký
později	pozdě	k6eAd2	pozdě
použilo	použít	k5eAaPmAgNnS	použít
divadlo	divadlo	k1gNnSc1	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
trestání	trestání	k1gNnSc1	trestání
sama	sám	k3xTgFnSc1	sám
sebe	sebe	k3xPyFc4	sebe
za	za	k7c4	za
hříchy	hřích	k1gInPc4	hřích
svých	svůj	k3xOyFgMnPc2	svůj
svěřenců	svěřenec	k1gMnPc2	svěřenec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
to	ten	k3xDgNnSc1	ten
oproti	oproti	k7c3	oproti
komedii	komedie	k1gFnSc3	komedie
skutečně	skutečně	k6eAd1	skutečně
fungovalo	fungovat	k5eAaImAgNnS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hochů	hoch	k1gMnPc2	hoch
se	se	k3xPyFc4	se
však	však	k9	však
snažil	snažit	k5eAaImAgMnS	snažit
Gándhí	Gándhí	k1gFnSc4	Gándhí
učit	učit	k5eAaImF	učit
i	i	k9	i
lidi	člověk	k1gMnPc4	člověk
kolem	kolem	k7c2	kolem
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
tehdy	tehdy	k6eAd1	tehdy
krutě	krutě	k6eAd1	krutě
milující	milující	k2eAgMnSc1d1	milující
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
a	a	k8xC	a
maje	mít	k5eAaImSgMnS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
jejího	její	k3xOp3gMnSc4	její
učitele	učitel	k1gMnSc4	učitel
<g/>
,	,	kIx,	,
trýznil	trýznit	k5eAaImAgMnS	trýznit
jsem	být	k5eAaImIp1nS	být
ji	on	k3xPp3gFnSc4	on
ze	z	k7c2	z
samé	samý	k3xTgFnSc2	samý
slepé	slepý	k2eAgFnSc2d1	slepá
lásky	láska	k1gFnSc2	láska
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sám	sám	k3xTgMnSc1	sám
však	však	k9	však
kdysi	kdysi	k6eAd1	kdysi
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Kdo	kdo	k3yInSc1	kdo
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
druhého	druhý	k4xOgInSc2	druhý
napravovati	napravovat	k5eAaImF	napravovat
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
během	běh	k1gInSc7	běh
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
žít	žít	k5eAaImF	žít
v	v	k7c6	v
důvěrném	důvěrný	k2eAgInSc6d1	důvěrný
vztahu	vztah	k1gInSc6	vztah
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
vidíme	vidět	k5eAaImIp1nP	vidět
jistý	jistý	k2eAgInSc4d1	jistý
rozpor	rozpor	k1gInSc4	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
bylo	být	k5eAaImAgNnS	být
napjaté	napjatý	k2eAgNnSc1d1	napjaté
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
hádali	hádat	k5eAaImAgMnP	hádat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
Gándhí	Gándhí	k1gMnSc1	Gándhí
oddal	oddat	k5eAaPmAgMnS	oddat
brahmačarji	brahmačar	k6eAd2	brahmačar
však	však	k9	však
přestal	přestat	k5eAaPmAgInS	přestat
svou	svůj	k3xOyFgFnSc4	svůj
ženu	žena	k1gFnSc4	žena
trýznit	trýznit	k5eAaImF	trýznit
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
manželství	manželství	k1gNnSc1	manželství
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
symbolem	symbol	k1gInSc7	symbol
pokory	pokora	k1gFnSc2	pokora
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
první	první	k4xOgFnSc1	první
satjágraha	satjágraha	k1gFnSc1	satjágraha
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
potupné	potupný	k2eAgFnSc3d1	potupná
registraci	registrace	k1gFnSc3	registrace
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
byl	být	k5eAaImAgMnS	být
tehdy	tehdy	k6eAd1	tehdy
poprvé	poprvé	k6eAd1	poprvé
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
odvahu	odvaha	k1gFnSc4	odvaha
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
první	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
dobře	dobře	k6eAd1	dobře
zorganizován	zorganizovat	k5eAaPmNgInS	zorganizovat
a	a	k8xC	a
satjágrahja	satjágrahja	k1gFnSc1	satjágrahja
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
významu	význam	k1gInSc3	význam
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
svobodným	svobodný	k2eAgFnPc3d1	svobodná
Indům	Indus	k1gInPc3	Indus
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
daň	daň	k1gFnSc1	daň
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
vláda	vláda	k1gFnSc1	vláda
naprosto	naprosto	k6eAd1	naprosto
nečekaně	nečekaně	k6eAd1	nečekaně
zrušila	zrušit	k5eAaPmAgFnS	zrušit
platnost	platnost	k1gFnSc4	platnost
veškerých	veškerý	k3xTgInPc2	veškerý
sňatků	sňatek	k1gInPc2	sňatek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nebyly	být	k5eNaImAgInP	být
uzavřeny	uzavřít	k5eAaPmNgInP	uzavřít
podle	podle	k7c2	podle
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
rituálu	rituál	k1gInSc2	rituál
<g/>
,	,	kIx,	,
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
pravá	pravý	k2eAgFnSc1d1	pravá
satjágraha	satjágraha	k1gFnSc1	satjágraha
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
svobodné	svobodný	k2eAgFnPc4d1	svobodná
ženy	žena	k1gFnPc4	žena
usilovaly	usilovat	k5eAaImAgInP	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
zavřeny	zavřít	k5eAaPmNgFnP	zavřít
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
manželi	manžel	k1gMnPc7	manžel
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
podstoupit	podstoupit	k5eAaPmF	podstoupit
"	"	kIx"	"
<g/>
oběť	oběť	k1gFnSc4	oběť
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Citujme	citovat	k5eAaBmRp1nP	citovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mou	můj	k3xOp1gFnSc7	můj
myšlenkou	myšlenka	k1gFnSc7	myšlenka
bylo	být	k5eAaImAgNnS	být
poslat	poslat	k5eAaPmF	poslat
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
přátele	přítel	k1gMnPc4	přítel
z	z	k7c2	z
osady	osada	k1gFnSc2	osada
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
vrchol	vrchol	k1gInSc1	vrchol
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mi	já	k3xPp1nSc3	já
zbývala	zbývat	k5eAaImAgFnS	zbývat
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Všichni	všechen	k3xTgMnPc1	všechen
jako	jako	k9	jako
vždy	vždy	k6eAd1	vždy
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gFnSc2	jeho
vlastní	vlastní	k2eAgFnSc2d1	vlastní
ženy	žena	k1gFnSc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
musela	muset	k5eAaImAgFnS	muset
přihlásit	přihlásit	k5eAaPmF	přihlásit
sama	sám	k3xTgFnSc1	sám
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
dle	dle	k7c2	dle
Gándhího	Gándhí	k2eAgInSc2d1	Gándhí
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
bych	by	kYmCp1nS	by
až	až	k9	až
příliš	příliš	k6eAd1	příliš
rád	rád	k2eAgMnSc1d1	rád
<g/>
,	,	kIx,	,
kdybys	kdyby	kYmCp2nS	kdyby
šla	jít	k5eAaImAgFnS	jít
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesmělo	smět	k5eNaImAgNnS	smět
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
bys	by	kYmCp2nS	by
šla	jít	k5eAaImAgFnS	jít
na	na	k7c4	na
mé	můj	k3xOp1gNnSc4	můj
naléhání	naléhání	k1gNnSc4	naléhání
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
zatčení	zatčení	k1gNnSc1	zatčení
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
našeho	náš	k3xOp1gInSc2	náš
pohledu	pohled	k1gInSc2	pohled
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
i	i	k9	i
Gándhího	Gándhí	k2eAgNnSc2d1	Gándhí
chování	chování	k1gNnSc2	chování
zdají	zdát	k5eAaPmIp3nP	zdát
být	být	k5eAaImF	být
šílenými	šílený	k2eAgFnPc7d1	šílená
a	a	k8xC	a
špatnými	špatný	k2eAgFnPc7d1	špatná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
toto	tento	k3xDgNnSc1	tento
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
až	až	k6eAd1	až
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
Indové	Ind	k1gMnPc1	Ind
byli	být	k5eAaImAgMnP	být
zatčením	zatčení	k1gNnSc7	zatčení
pobouřeni	pobouřit	k5eAaPmNgMnP	pobouřit
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Vězňové	vězeň	k1gMnPc1	vězeň
a	a	k8xC	a
zejména	zejména	k6eAd1	zejména
ženy	žena	k1gFnPc4	žena
prokázali	prokázat	k5eAaPmAgMnP	prokázat
velkou	velký	k2eAgFnSc4d1	velká
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
Indii	Indie	k1gFnSc3	Indie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
horníci	horník	k1gMnPc1	horník
z	z	k7c2	z
Newcastlu	Newcastl	k1gInSc2	Newcastl
<g/>
.	.	kIx.	.
</s>
<s>
Satjágraha	Satjágraha	k1gFnSc1	Satjágraha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
utrpení	utrpení	k1gNnSc2	utrpení
<g/>
,	,	kIx,	,
odříkání	odříkání	k1gNnSc2	odříkání
a	a	k8xC	a
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
slepě	slepě	k6eAd1	slepě
táhli	táhnout	k5eAaImAgMnP	táhnout
za	za	k7c7	za
Gándhím	Gándhí	k1gMnSc7	Gándhí
a	a	k8xC	a
čím	co	k3yQnSc7	co
víc	hodně	k6eAd2	hodně
trpěli	trpět	k5eAaImAgMnP	trpět
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
byli	být	k5eAaImAgMnP	být
spokojenější	spokojený	k2eAgMnPc1d2	spokojenější
<g/>
.	.	kIx.	.
</s>
<s>
Členů	člen	k1gInPc2	člen
hnutí	hnutí	k1gNnSc2	hnutí
stále	stále	k6eAd1	stále
přibývalo	přibývat	k5eAaImAgNnS	přibývat
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
posílal	posílat	k5eAaImAgMnS	posílat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
i	i	k8xC	i
horníky	horník	k1gMnPc4	horník
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
družinu	družina	k1gFnSc4	družina
z	z	k7c2	z
Phoenixu	Phoenix	k1gInSc2	Phoenix
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Gándhí	Gándhí	k1gMnSc1	Gándhí
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
v	v	k7c6	v
Bloemfonteinu	Bloemfontein	k1gInSc6	Bloemfontein
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
značně	značně	k6eAd1	značně
osamocen	osamocen	k2eAgInSc1d1	osamocen
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
toho	ten	k3xDgMnSc4	ten
využít	využít	k5eAaPmF	využít
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Gándhího	Gándhí	k2eAgNnSc2d1	Gándhí
zatčení	zatčení	k1gNnSc2	zatčení
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
stávkujících	stávkující	k2eAgFnPc6d1	stávkující
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgNnSc4d2	veliký
podnícení	podnícení	k1gNnSc4	podnícení
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
obnovit	obnovit	k5eAaPmF	obnovit
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
a	a	k8xC	a
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
nepřistoupit	přistoupit	k5eNaPmF	přistoupit
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Učinila	učinit	k5eAaImAgFnS	učinit
z	z	k7c2	z
dolů	dolů	k6eAd1	dolů
vězeňská	vězeňský	k2eAgNnPc1d1	vězeňské
pracoviště	pracoviště	k1gNnPc1	pracoviště
a	a	k8xC	a
z	z	k7c2	z
bývalých	bývalý	k2eAgMnPc2d1	bývalý
dělníků	dělník	k1gMnPc2	dělník
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
otroci	otrok	k1gMnPc1	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
přes	přes	k7c4	přes
bití	bití	k1gNnSc4	bití
a	a	k8xC	a
utrpení	utrpení	k1gNnSc4	utrpení
odmítali	odmítat	k5eAaImAgMnP	odmítat
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
jižní	jižní	k2eAgFnSc2d1	jižní
Afriky	Afrika	k1gFnSc2	Afrika
i	i	k8xC	i
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Anglická	anglický	k2eAgFnSc1d1	anglická
vláda	vláda	k1gFnSc1	vláda
zahájila	zahájit	k5eAaPmAgFnS	zahájit
politiku	politika	k1gFnSc4	politika
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
krve	krev	k1gFnSc2	krev
–	–	k?	–
bránila	bránit	k5eAaImAgFnS	bránit
násilím	násilí	k1gNnSc7	násilí
ve	v	k7c6	v
stávkách	stávka	k1gFnPc6	stávka
<g/>
,	,	kIx,	,
žaláře	žalář	k1gInSc2	žalář
byly	být	k5eAaImAgFnP	být
přeplněné	přeplněný	k2eAgFnPc1d1	přeplněná
a	a	k8xC	a
ani	ani	k8xC	ani
policie	policie	k1gFnSc1	policie
nenahnala	nahnat	k5eNaPmAgFnS	nahnat
stávkujícím	stávkující	k1gMnPc3	stávkující
strach	strach	k1gInSc4	strach
<g/>
.	.	kIx.	.
</s>
<s>
Hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
chýlilo	chýlit	k5eAaImAgNnS	chýlit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
byl	být	k5eAaImAgInS	být
propuštěn	propustit	k5eAaPmNgInS	propustit
a	a	k8xC	a
revoluce	revoluce	k1gFnSc1	revoluce
mířila	mířit	k5eAaImAgFnS	mířit
k	k	k7c3	k
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1914	[number]	k4	1914
vláda	vláda	k1gFnSc1	vláda
vyhověla	vyhovět	k5eAaPmAgFnS	vyhovět
požadavkům	požadavek	k1gInPc3	požadavek
Indů	Ind	k1gMnPc2	Ind
<g/>
,	,	kIx,	,
zrušila	zrušit	k5eAaPmAgFnS	zrušit
tříliberní	tříliberní	k?	tříliberní
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
schválila	schválit	k5eAaPmAgFnS	schválit
platnost	platnost	k1gFnSc4	platnost
sňatků	sňatek	k1gInPc2	sňatek
a	a	k8xC	a
volný	volný	k2eAgInSc1d1	volný
pohyb	pohyb	k1gInSc1	pohyb
Indů	Ind	k1gMnPc2	Ind
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
se	s	k7c7	s
společníky	společník	k1gMnPc7	společník
odjel	odjet	k5eAaPmAgMnS	odjet
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
do	do	k7c2	do
Southamptonu	Southampton	k1gInSc2	Southampton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
tolikrát	tolikrát	k6eAd1	tolikrát
předtím	předtím	k6eAd1	předtím
chtěl	chtít	k5eAaImAgMnS	chtít
Gándhí	Gándhí	k1gMnSc1	Gándhí
zlepšit	zlepšit	k5eAaPmF	zlepšit
postavení	postavení	k1gNnSc4	postavení
Indů	Indus	k1gInPc2	Indus
přímo	přímo	k6eAd1	přímo
v	v	k7c4	v
Anglii	Anglie	k1gFnSc4	Anglie
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
a	a	k8xC	a
Indičtí	indický	k2eAgMnPc1d1	indický
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
budou	být	k5eAaImBp3nP	být
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
válečné	válečný	k2eAgFnSc6d1	válečná
podpoře	podpora	k1gFnSc6	podpora
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
Gándhím	Gándhí	k1gMnSc7	Gándhí
nesouhlasili	souhlasit	k5eNaImAgMnP	souhlasit
zdaleka	zdaleka	k6eAd1	zdaleka
všichni	všechen	k3xTgMnPc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
chtěli	chtít	k5eAaImAgMnP	chtít
využít	využít	k5eAaPmF	využít
situace	situace	k1gFnPc4	situace
a	a	k8xC	a
přinutit	přinutit	k5eAaPmF	přinutit
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
zlepšení	zlepšení	k1gNnSc3	zlepšení
situace	situace	k1gFnSc2	situace
Indů	Indus	k1gInPc2	Indus
pod	pod	k7c7	pod
příslibem	příslib	k1gInSc7	příslib
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
si	se	k3xPyFc3	se
však	však	k9	však
stál	stát	k5eAaImAgInS	stát
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
tedy	tedy	k9	tedy
Indové	Ind	k1gMnPc1	Ind
opět	opět	k6eAd1	opět
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
svému	svůj	k1gMnSc3	svůj
uzurpátorovi	uzurpátor	k1gMnSc3	uzurpátor
a	a	k8xC	a
Gándhí	Gándhí	k1gFnSc3	Gándhí
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
Anglie	Anglie	k1gFnSc2	Anglie
po	po	k7c4	po
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
hojně	hojně	k6eAd1	hojně
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
setrvával	setrvávat	k5eAaImAgMnS	setrvávat
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
hledal	hledat	k5eAaImAgMnS	hledat
a	a	k8xC	a
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
filosofie	filosofie	k1gFnSc1	filosofie
je	být	k5eAaImIp3nS	být
filosofií	filosofie	k1gFnSc7	filosofie
činu	čin	k1gInSc2	čin
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ze	z	k7c2	z
života	život	k1gInSc2	život
vychází	vycházet	k5eAaImIp3nS	vycházet
a	a	k8xC	a
do	do	k7c2	do
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
také	také	k9	také
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Nevynašel	vynajít	k5eNaPmAgMnS	vynajít
nic	nic	k3yNnSc1	nic
nového	nový	k2eAgNnSc2d1	nové
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oprášil	oprášit	k5eAaPmAgMnS	oprášit
to	ten	k3xDgNnSc4	ten
staré	staré	k1gNnSc4	staré
a	a	k8xC	a
uvedl	uvést	k5eAaPmAgMnS	uvést
to	ten	k3xDgNnSc4	ten
v	v	k7c4	v
život	život	k1gInSc4	život
–	–	k?	–
zejména	zejména	k9	zejména
myšlenky	myšlenka	k1gFnSc2	myšlenka
o	o	k7c6	o
etice	etika	k1gFnSc6	etika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
sílu	síla	k1gFnSc4	síla
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
i	i	k8xC	i
filozofickou	filozofický	k2eAgFnSc7d1	filozofická
čerpal	čerpat	k5eAaImAgMnS	čerpat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
z	z	k7c2	z
Bhagavad-Gíty	Bhagavad-Gíta	k1gFnSc2	Bhagavad-Gíta
<g/>
,	,	kIx,	,
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
Biblí	bible	k1gFnSc7	bible
a	a	k8xC	a
Koránem	korán	k1gInSc7	korán
a	a	k8xC	a
také	také	k9	také
esejí	esej	k1gFnPc2	esej
"	"	kIx"	"
<g/>
Občanská	občanský	k2eAgFnSc1d1	občanská
neposlušnost	neposlušnost	k1gFnSc1	neposlušnost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Civil	civil	k1gMnSc1	civil
Disobedience	Disobedience	k1gFnSc2	Disobedience
<g/>
)	)	kIx)	)
od	od	k7c2	od
H.	H.	kA	H.
D.	D.	kA	D.
Thoreaua	Thoreaua	k1gMnSc1	Thoreaua
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
a	a	k8xC	a
nejmocnější	mocný	k2eAgFnSc4d3	nejmocnější
považoval	považovat	k5eAaImAgMnS	považovat
vždy	vždy	k6eAd1	vždy
Pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
jediným	jediný	k2eAgMnSc7d1	jediný
Bohem	bůh	k1gMnSc7	bůh
(	(	kIx(	(
<g/>
myšleno	myslet	k5eAaImNgNnS	myslet
pravda	pravda	k9	pravda
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
nepravda	nepravda	k1gFnSc1	nepravda
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Bůh	bůh	k1gMnSc1	bůh
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
atomu	atom	k1gInSc6	atom
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
i	i	k8xC	i
v	v	k7c6	v
nás	my	k3xPp1nPc6	my
(	(	kIx(	(
<g/>
blízkost	blízkost	k1gFnSc4	blízkost
zenové	zenový	k2eAgFnSc2d1	zenová
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
upřednostňoval	upřednostňovat	k5eAaImAgInS	upřednostňovat
význam	význam	k1gInSc1	význam
praxe	praxe	k1gFnSc2	praxe
před	před	k7c7	před
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
jeho	jeho	k3xOp3gFnSc7	jeho
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
nenásilnost	nenásilnost	k1gFnSc1	nenásilnost
a	a	k8xC	a
neochvějná	neochvějný	k2eAgFnSc1d1	neochvějná
láska	láska	k1gFnSc1	láska
ke	k	k7c3	k
všemu	všecek	k3xTgNnSc3	všecek
a	a	k8xC	a
všem	všecek	k3xTgNnSc6	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vše	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
světě	svět	k1gInSc6	svět
bylo	být	k5eAaImAgNnS	být
rovnocenné	rovnocenný	k2eAgNnSc1d1	rovnocenné
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
ohledu	ohled	k1gInSc6	ohled
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
myšlenkou	myšlenka	k1gFnSc7	myšlenka
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odsunuje	odsunovat	k5eAaImIp3nS	odsunovat
duchovní	duchovní	k2eAgInSc4d1	duchovní
svět	svět	k1gInSc4	svět
a	a	k8xC	a
vyzývá	vyzývat	k5eAaImIp3nS	vyzývat
sám	sám	k3xTgMnSc1	sám
k	k	k7c3	k
prostotě	prostota	k1gFnSc3	prostota
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gFnSc1	Gándhí
považuje	považovat	k5eAaImIp3nS	považovat
věk	věk	k1gInSc4	věk
civilizace	civilizace	k1gFnSc2	civilizace
za	za	k7c4	za
věk	věk	k1gInSc4	věk
temnoty	temnota	k1gFnSc2	temnota
<g/>
,	,	kIx,	,
věk	věk	k1gInSc4	věk
satana	satan	k1gMnSc2	satan
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
náboženský	náboženský	k2eAgMnSc1d1	náboženský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
plný	plný	k2eAgInSc4d1	plný
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
zvěrstev	zvěrstvo	k1gNnPc2	zvěrstvo
<g/>
.	.	kIx.	.
</s>
<s>
Třídy	třída	k1gFnPc1	třída
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
diferencují	diferencovat	k5eAaImIp3nP	diferencovat
a	a	k8xC	a
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
–	–	k?	–
bohatý	bohatý	k2eAgMnSc1d1	bohatý
bude	být	k5eAaImBp3nS	být
ještě	ještě	k6eAd1	ještě
bohatší	bohatý	k2eAgMnSc1d2	bohatší
a	a	k8xC	a
chudý	chudý	k2eAgMnSc1d1	chudý
chudší	chudý	k2eAgMnSc1d2	chudší
<g/>
,	,	kIx,	,
ne	ne	k9	ne
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Trpělivý	trpělivý	k2eAgMnSc1d1	trpělivý
člověk	člověk	k1gMnSc1	člověk
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
dočká	dočkat	k5eAaPmIp3nS	dočkat
chvíle	chvíle	k1gFnSc1	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
civilizace	civilizace	k1gFnSc1	civilizace
sama	sám	k3xTgMnSc4	sám
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zásadně	zásadně	k6eAd1	zásadně
proti	proti	k7c3	proti
strojům	stroj	k1gInPc3	stroj
a	a	k8xC	a
velkým	velký	k2eAgNnPc3d1	velké
městům	město	k1gNnPc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Ideálem	ideál	k1gInSc7	ideál
jeho	on	k3xPp3gNnSc2	on
myšlení	myšlení	k1gNnSc2	myšlení
byla	být	k5eAaImAgFnS	být
venkovská	venkovský	k2eAgFnSc1d1	venkovská
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
tyto	tento	k3xDgInPc1	tento
ideály	ideál	k1gInPc1	ideál
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
nesnažil	snažit	k5eNaImAgMnS	snažit
realizovat	realizovat	k5eAaBmF	realizovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrat	návrat	k1gInSc1	návrat
do	do	k7c2	do
minulosti	minulost	k1gFnSc2	minulost
není	být	k5eNaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
často	často	k6eAd1	často
kladl	klást	k5eAaImAgInS	klást
zřetel	zřetel	k1gInSc1	zřetel
na	na	k7c4	na
protiklady	protiklad	k1gInPc4	protiklad
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
svou	svůj	k3xOyFgFnSc4	svůj
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
reformách	reforma	k1gFnPc6	reforma
se	se	k3xPyFc4	se
často	často	k6eAd1	často
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
prvotní	prvotní	k2eAgFnSc7d1	prvotní
nechutí	nechuť	k1gFnSc7	nechuť
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
jakožto	jakožto	k8xS	jakožto
reformátor	reformátor	k1gMnSc1	reformátor
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
trpělivý	trpělivý	k2eAgMnSc1d1	trpělivý
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
lid	lid	k1gInSc1	lid
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
další	další	k2eAgFnSc1d1	další
Gándhího	Gándhí	k1gMnSc2	Gándhí
silná	silný	k2eAgFnSc1d1	silná
stránka	stránka	k1gFnSc1	stránka
–	–	k?	–
trpělivost	trpělivost	k1gFnSc1	trpělivost
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
filosofii	filosofie	k1gFnSc3	filosofie
absolutní	absolutní	k2eAgFnPc4d1	absolutní
lásky	láska	k1gFnPc4	láska
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
krajně	krajně	k6eAd1	krajně
blízké	blízký	k2eAgNnSc1d1	blízké
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
oblasti	oblast	k1gFnSc6	oblast
získal	získat	k5eAaPmAgMnS	získat
velké	velká	k1gFnPc4	velká
zkušenosti	zkušenost	k1gFnPc4	zkušenost
právě	právě	k9	právě
za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
i	i	k8xC	i
nákaze	nákaz	k1gInSc6	nákaz
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
obětavost	obětavost	k1gFnSc1	obětavost
prospěla	prospět	k5eAaPmAgFnS	prospět
jeho	jeho	k3xOp3gFnSc4	jeho
reputaci	reputace	k1gFnSc4	reputace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgInP	být
důsledky	důsledek	k1gInPc1	důsledek
anglické	anglický	k2eAgFnSc2d1	anglická
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
nejpatrnější	patrný	k2eAgFnSc6d3	nejpatrnější
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
narušeno	narušen	k2eAgNnSc1d1	narušeno
zemědělství	zemědělství	k1gNnSc1	zemědělství
a	a	k8xC	a
propukla	propuknout	k5eAaPmAgFnS	propuknout
chudoba	chudoba	k1gFnSc1	chudoba
a	a	k8xC	a
hladomor	hladomor	k1gInSc1	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pětadvaceti	pětadvacet	k4xCc2	pětadvacet
let	léto	k1gNnPc2	léto
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
15	[number]	k4	15
miliónů	milión	k4xCgInPc2	milión
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Angličané	Angličan	k1gMnPc1	Angličan
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
sáli	sát	k5eAaImAgMnP	sát
bohatství	bohatství	k1gNnSc4	bohatství
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Británie	Británie	k1gFnSc1	Británie
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
Bengálsko	Bengálsko	k1gNnSc4	Bengálsko
na	na	k7c4	na
muslimskou	muslimský	k2eAgFnSc4d1	muslimská
a	a	k8xC	a
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
ožila	ožít	k5eAaPmAgFnS	ožít
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
si	se	k3xPyFc3	se
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
svou	svůj	k3xOyFgFnSc4	svůj
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
rodil	rodit	k5eAaImAgInS	rodit
se	se	k3xPyFc4	se
zárodek	zárodek	k1gInSc1	zárodek
svobodné	svobodný	k2eAgFnSc2d1	svobodná
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Gándhí	Gándhí	k1gFnSc1	Gándhí
roku	rok	k1gInSc2	rok
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgInS	muset
Anglii	Anglie	k1gFnSc4	Anglie
opustit	opustit	k5eAaPmF	opustit
kvůli	kvůli	k7c3	kvůli
vážné	vážný	k2eAgFnSc3d1	vážná
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nejkrásnější	krásný	k2eAgFnSc4d3	nejkrásnější
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domovině	domovina	k1gFnSc6	domovina
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
podporou	podpora	k1gFnSc7	podpora
a	a	k8xC	a
úctou	úcta	k1gFnSc7	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
satjágrahy	satjágraha	k1gFnSc2	satjágraha
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
být	být	k5eAaImF	být
ukvapená	ukvapený	k2eAgFnSc1d1	ukvapená
–	–	k?	–
na	na	k7c6	na
radu	rad	k1gInSc6	rad
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
G.	G.	kA	G.
K.	K.	kA	K.
Gókhalého	Gókhalý	k2eAgNnSc2d1	Gókhalý
Gándhí	Gándhí	k1gNnSc2	Gándhí
slíbil	slíbit	k5eAaPmAgMnS	slíbit
nejprve	nejprve	k6eAd1	nejprve
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
studovat	studovat	k5eAaImF	studovat
indickou	indický	k2eAgFnSc4d1	indická
politickou	politický	k2eAgFnSc4d1	politická
scénu	scéna	k1gFnSc4	scéna
<g/>
,	,	kIx,	,
než	než	k8xS	než
cokoli	cokoli	k3yInSc4	cokoli
podnikne	podniknout	k5eAaPmIp3nS	podniknout
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
neoficiálním	neoficiální	k2eAgMnSc7d1	neoficiální
členem	člen	k1gMnSc7	člen
Společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Gándhího	Gándhí	k1gMnSc2	Gándhí
se	se	k3xPyFc4	se
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
stává	stávat	k5eAaImIp3nS	stávat
daršan	daršan	k1gMnSc1	daršan
(	(	kIx(	(
<g/>
světec	světec	k1gMnSc1	světec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
ho	on	k3xPp3gMnSc4	on
vyhledávali	vyhledávat	k5eAaImAgMnP	vyhledávat
pro	pro	k7c4	pro
radu	rada	k1gFnSc4	rada
i	i	k8xC	i
útěchu	útěcha	k1gFnSc4	útěcha
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
také	také	k9	také
posvátné	posvátný	k2eAgFnSc6d1	posvátná
pouti	pouť	k1gFnSc6	pouť
Kumbh	Kumbha	k1gFnPc2	Kumbha
mélá	mélá	k1gFnSc1	mélá
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
naplnila	naplnit	k5eAaPmAgFnS	naplnit
odporem	odpor	k1gInSc7	odpor
–	–	k?	–
lidé	člověk	k1gMnPc1	člověk
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
zbytečně	zbytečně	k6eAd1	zbytečně
trpěli	trpět	k5eAaImAgMnP	trpět
a	a	k8xC	a
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
s	s	k7c7	s
obchodováním	obchodování	k1gNnSc7	obchodování
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jím	jíst	k5eAaImIp1nS	jíst
hluboce	hluboko	k6eAd1	hluboko
otřáslo	otřást	k5eAaPmAgNnS	otřást
<g/>
.	.	kIx.	.
</s>
<s>
Víc	hodně	k6eAd2	hodně
než	než	k8xS	než
kdy	kdy	k6eAd1	kdy
jindy	jindy	k6eAd1	jindy
se	se	k3xPyFc4	se
postil	postit	k5eAaImAgMnS	postit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
Phoenix	Phoenix	k1gInSc4	Phoenix
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
i	i	k9	i
zde	zde	k6eAd1	zde
osadu	osada	k1gFnSc4	osada
poblíž	poblíž	k7c2	poblíž
Ahmadábádu	Ahmadábád	k1gInSc2	Ahmadábád
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
formovat	formovat	k5eAaImF	formovat
nová	nový	k2eAgFnSc1d1	nová
příležitost	příležitost	k1gFnSc1	příležitost
k	k	k7c3	k
satjagraze	satjagraha	k1gFnSc3	satjagraha
<g/>
.	.	kIx.	.
</s>
<s>
Protest	protest	k1gInSc1	protest
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
týkat	týkat	k5eAaImF	týkat
vystěhovalectví	vystěhovalectví	k1gNnSc4	vystěhovalectví
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
satjágrahy	satjágraha	k1gFnSc2	satjágraha
nebylo	být	k5eNaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
<g/>
.	.	kIx.	.
</s>
<s>
Smluvní	smluvní	k2eAgNnSc1d1	smluvní
vystěhovalectví	vystěhovalectví	k1gNnSc1	vystěhovalectví
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1916	[number]	k4	1916
požádal	požádat	k5eAaPmAgMnS	požádat
Gándhího	Gándhí	k1gMnSc4	Gándhí
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
Rádžkumár	Rádžkumár	k1gInSc1	Rádžkumár
Šukla	šuknout	k5eAaPmAgNnP	šuknout
z	z	k7c2	z
Čamparanu	Čamparan	k1gInSc2	Čamparan
<g/>
.	.	kIx.	.
</s>
<s>
Pěstitelé	pěstitel	k1gMnPc1	pěstitel
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
velice	velice	k6eAd1	velice
strádali	strádat	k5eAaImAgMnP	strádat
pod	pod	k7c7	pod
systémem	systém	k1gInSc7	systém
thinkhaitia	thinkhaitium	k1gNnSc2	thinkhaitium
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vydal	vydat	k5eAaPmAgMnS	vydat
celou	celý	k2eAgFnSc4d1	celá
záležitost	záležitost	k1gFnSc4	záležitost
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nepřátelstvím	nepřátelství	k1gNnSc7	nepřátelství
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
ihned	ihned	k6eAd1	ihned
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
uvězněním	uvěznění	k1gNnSc7	uvěznění
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
byl	být	k5eAaImAgInS	být
Čamparan	Čamparan	k1gInSc1	Čamparan
odříznut	odříznut	k2eAgInSc1d1	odříznut
od	od	k7c2	od
světa	svět	k1gInSc2	svět
a	a	k8xC	a
Gándhího	Gándhí	k2eAgNnSc2d1	Gándhí
tam	tam	k6eAd1	tam
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neznali	znát	k5eNaImAgMnP	znát
<g/>
,	,	kIx,	,
lid	lid	k1gInSc4	lid
si	se	k3xPyFc3	se
jej	on	k3xPp3gInSc4	on
okamžitě	okamžitě	k6eAd1	okamžitě
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jak	jak	k8xC	jak
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stanul	stanout	k5eAaPmAgMnS	stanout
jsem	být	k5eAaImIp1nS	být
tváří	tvář	k1gFnSc7	tvář
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
pravdě	pravda	k1gFnSc3	pravda
a	a	k8xC	a
ahinse	ahinsa	k1gFnSc3	ahinsa
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Proti	proti	k7c3	proti
Gándhímu	Gándhí	k1gMnSc3	Gándhí
stáli	stát	k5eAaImAgMnP	stát
teď	teď	k6eAd1	teď
už	už	k6eAd1	už
jen	jen	k9	jen
majitelé	majitel	k1gMnPc1	majitel
plantáží	plantáž	k1gFnPc2	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
vláda	vláda	k1gFnSc1	vláda
musela	muset	k5eAaImAgFnS	muset
nakonec	nakonec	k6eAd1	nakonec
uznat	uznat	k5eAaPmF	uznat
zákonnost	zákonnost	k1gFnSc4	zákonnost
jeho	on	k3xPp3gNnSc2	on
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
opět	opět	k6eAd1	opět
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
komunitu	komunita	k1gFnSc4	komunita
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
vyslýchat	vyslýchat	k5eAaImF	vyslýchat
sedláky	sedlák	k1gMnPc4	sedlák
<g/>
.	.	kIx.	.
</s>
<s>
Sezval	sezvat	k5eAaPmAgInS	sezvat
kontingent	kontingent	k1gInSc1	kontingent
učitelů	učitel	k1gMnPc2	učitel
a	a	k8xC	a
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dal	dát	k5eAaPmAgInS	dát
sedlákům	sedlák	k1gMnPc3	sedlák
potřebné	potřebný	k2eAgNnSc4d1	potřebné
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
výchovou	výchova	k1gFnSc7	výchova
zdravotnickou	zdravotnický	k2eAgFnSc7d1	zdravotnická
a	a	k8xC	a
hygienickou	hygienický	k2eAgFnSc7d1	hygienická
<g/>
.	.	kIx.	.
</s>
<s>
Majitelé	majitel	k1gMnPc1	majitel
plantáží	plantáž	k1gFnPc2	plantáž
prováděli	provádět	k5eAaImAgMnP	provádět
možné	možný	k2eAgNnSc1d1	možné
i	i	k8xC	i
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Gándhího	Gándhí	k1gMnSc4	Gándhí
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
zmařili	zmařit	k5eAaPmAgMnP	zmařit
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
thinkhaitia	thinkhaitium	k1gNnSc2	thinkhaitium
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
skutečně	skutečně	k6eAd1	skutečně
díky	dík	k1gInPc1	dík
Gándhího	Gándhí	k1gMnSc2	Gándhí
neústupnosti	neústupnost	k1gFnSc2	neústupnost
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
vedl	vést	k5eAaImAgMnS	vést
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zpočátku	zpočátku	k6eAd1	zpočátku
vypadal	vypadat	k5eAaPmAgInS	vypadat
nenápadně	nápadně	k6eNd1	nápadně
<g/>
.	.	kIx.	.
</s>
<s>
Bojoval	bojovat	k5eAaImAgInS	bojovat
proti	proti	k7c3	proti
masovému	masový	k2eAgInSc3d1	masový
vývozu	vývoz	k1gInSc3	vývoz
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
nákupu	nákup	k1gInSc2	nákup
oděvů	oděv	k1gInPc2	oděv
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
oživovat	oživovat	k5eAaImF	oživovat
indické	indický	k2eAgNnSc4d1	indické
řemeslné	řemeslný	k2eAgNnSc4d1	řemeslné
tkalcovství	tkalcovství	k1gNnSc4	tkalcovství
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
problému	problém	k1gInSc3	problém
věnoval	věnovat	k5eAaPmAgMnS	věnovat
dvacet	dvacet	k4xCc1	dvacet
let	léto	k1gNnPc2	léto
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
ztrácel	ztrácet	k5eAaImAgMnS	ztrácet
přívržence	přívrženec	k1gMnSc4	přívrženec
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ruční	ruční	k2eAgFnSc1d1	ruční
výroba	výroba	k1gFnSc1	výroba
bavlny	bavlna	k1gFnSc2	bavlna
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
ekonomického	ekonomický	k2eAgNnSc2d1	ekonomické
hlediska	hledisko	k1gNnSc2	hledisko
nevýhodná	výhodný	k2eNgFnSc1d1	nevýhodná
a	a	k8xC	a
pouhá	pouhý	k2eAgFnSc1d1	pouhá
myšlenka	myšlenka	k1gFnSc1	myšlenka
duchovní	duchovní	k2eAgFnSc2d1	duchovní
očisty	očista	k1gFnSc2	očista
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
pohořel	pohořet	k5eAaPmAgMnS	pohořet
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gFnSc1	jeho
největší	veliký	k2eAgFnSc1d3	veliký
porážka	porážka	k1gFnSc1	porážka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
on	on	k3xPp3gInSc1	on
i	i	k8xC	i
jeho	jeho	k3xOp3gMnPc1	jeho
společníci	společník	k1gMnPc1	společník
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
hnutí	hnutí	k1gNnSc2	hnutí
vložili	vložit	k5eAaPmAgMnP	vložit
všechno	všechen	k3xTgNnSc4	všechen
a	a	k8xC	a
takřka	takřka	k6eAd1	takřka
zbytečně	zbytečně	k6eAd1	zbytečně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
prohře	prohra	k1gFnSc6	prohra
se	se	k3xPyFc4	se
Gándhí	Gándhí	k1gMnPc1	Gándhí
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životopise	životopis	k1gInSc6	životopis
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
leč	leč	k8xC	leč
byla	být	k5eAaImAgFnS	být
jistě	jistě	k6eAd1	jistě
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
než	než	k8xS	než
se	se	k3xPyFc4	se
stačil	stačit	k5eAaBmAgMnS	stačit
vzpamatovat	vzpamatovat	k5eAaPmF	vzpamatovat
<g/>
,	,	kIx,	,
čekal	čekat	k5eAaImAgMnS	čekat
ho	on	k3xPp3gInSc4	on
další	další	k2eAgInSc4d1	další
úkol	úkol	k1gInSc4	úkol
–	–	k?	–
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Khaira	Khairo	k1gNnSc2	Khairo
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
hlad	hlad	k1gInSc1	hlad
a	a	k8xC	a
zemědělci	zemědělec	k1gMnPc1	zemědělec
žádali	žádat	k5eAaImAgMnP	žádat
snížení	snížení	k1gNnSc4	snížení
daní	daň	k1gFnPc2	daň
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
doporučil	doporučit	k5eAaPmAgMnS	doporučit
satjágrahu	satjágraha	k1gFnSc4	satjágraha
<g/>
.	.	kIx.	.
</s>
<s>
Sepsali	sepsat	k5eAaPmAgMnP	sepsat
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jasně	jasně	k6eAd1	jasně
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
platit	platit	k5eAaImF	platit
daně	daň	k1gFnPc4	daň
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
uvěznění	uvěznění	k1gNnSc2	uvěznění
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
(	(	kIx(	(
<g/>
po	po	k7c6	po
věznění	věznění	k1gNnSc6	věznění
a	a	k8xC	a
utrpení	utrpení	k1gNnSc6	utrpení
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
pokaždé	pokaždé	k6eAd1	pokaždé
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
bojích	boj	k1gInPc6	boj
se	se	k3xPyFc4	se
Gándhí	Gándhí	k1gFnSc1	Gándhí
opět	opět	k6eAd1	opět
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
válečnou	válečný	k2eAgFnSc4d1	válečná
konferenci	konference	k1gFnSc4	konference
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
hájil	hájit	k5eAaImAgMnS	hájit
myšlenky	myšlenka	k1gFnPc4	myšlenka
Indů	Indus	k1gInPc2	Indus
<g/>
,	,	kIx,	,
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
propuštění	propuštění	k1gNnSc4	propuštění
některých	některý	k3yIgFnPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
bratrů	bratr	k1gMnPc2	bratr
Aliů	Ali	k1gInPc2	Ali
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oplátkou	oplátka	k1gFnSc7	oplátka
Britům	Brit	k1gMnPc3	Brit
opět	opět	k6eAd1	opět
slíbil	slíbit	k5eAaPmAgMnS	slíbit
válečnou	válečný	k2eAgFnSc4d1	válečná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
osobně	osobně	k6eAd1	osobně
rekrutovat	rekrutovat	k5eAaImF	rekrutovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
nevolí	nevole	k1gFnSc7	nevole
a	a	k8xC	a
pramalým	pramalý	k2eAgInSc7d1	pramalý
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těch	ten	k3xDgInPc6	ten
dnech	den	k1gInPc6	den
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
úplavicí	úplavice	k1gFnSc7	úplavice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
v	v	k7c4	v
zánět	zánět	k1gInSc4	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Odmítal	odmítat	k5eAaImAgMnS	odmítat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
i	i	k9	i
léky	lék	k1gInPc1	lék
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
vlastního	vlastní	k2eAgNnSc2d1	vlastní
zavinění	zavinění	k1gNnSc3	zavinění
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
a	a	k8xC	a
rekrutů	rekrut	k1gMnPc2	rekrut
už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
trpěl	trpět	k5eAaImAgMnS	trpět
a	a	k8xC	a
slábl	slábnout	k5eAaImAgMnS	slábnout
každým	každý	k3xTgInSc7	každý
dnem	den	k1gInSc7	den
<g/>
,	,	kIx,	,
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
propastí	propast	k1gFnSc7	propast
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přece	přece	k9	přece
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
uzdravovat	uzdravovat	k5eAaImF	uzdravovat
(	(	kIx(	(
<g/>
po	po	k7c6	po
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
zásadně	zásadně	k6eAd1	zásadně
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
vzepřít	vzepřít	k5eAaPmF	vzepřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
stále	stále	k6eAd1	stále
upoután	upoutat	k5eAaPmNgInS	upoutat
na	na	k7c4	na
lůžko	lůžko	k1gNnSc4	lůžko
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
další	další	k2eAgInSc4d1	další
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
Trnem	trn	k1gInSc7	trn
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
Rowlattský	Rowlattský	k2eAgInSc1d1	Rowlattský
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
drasticky	drasticky	k6eAd1	drasticky
zasahoval	zasahovat	k5eAaImAgInS	zasahovat
proti	proti	k7c3	proti
revolučnímu	revoluční	k2eAgNnSc3d1	revoluční
hnutí	hnutí	k1gNnSc3	hnutí
v	v	k7c6	v
Bengálsku	Bengálsko	k1gNnSc6	Bengálsko
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
<g/>
,	,	kIx,	,
elitní	elitní	k2eAgFnSc1d1	elitní
skupina	skupina	k1gFnSc1	skupina
Satjágraha	Satjágraha	k1gFnSc1	Satjágraha
Sabha	Sabha	k1gFnSc1	Sabha
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
se	se	k3xPyFc4	se
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
nutně	nutně	k6eAd1	nutně
uzdravit	uzdravit	k5eAaPmF	uzdravit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
porušil	porušit	k5eAaPmAgMnS	porušit
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
revoluce	revoluce	k1gFnSc2	revoluce
další	další	k2eAgFnSc2d1	další
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zásad	zásada	k1gFnPc2	zásada
a	a	k8xC	a
pil	pít	k5eAaImAgMnS	pít
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Uzdravil	uzdravit	k5eAaPmAgMnS	uzdravit
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
porušení	porušení	k1gNnSc1	porušení
tohoto	tento	k3xDgInSc2	tento
slibu	slib	k1gInSc2	slib
navždy	navždy	k6eAd1	navždy
nalomilo	nalomit	k5eAaBmAgNnS	nalomit
jeho	on	k3xPp3gMnSc4	on
ducha	duch	k1gMnSc4	duch
<g/>
...	...	k?	...
Gándhí	Gándhí	k1gMnSc1	Gándhí
si	se	k3xPyFc3	se
nebyl	být	k5eNaImAgMnS	být
jist	jist	k2eAgMnSc1d1	jist
postupem	postupem	k7c2	postupem
satjágrahy	satjágraha	k1gFnSc2	satjágraha
proti	proti	k7c3	proti
Rowlattskému	Rowlattský	k2eAgInSc3d1	Rowlattský
návrhu	návrh	k1gInSc3	návrh
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
uzákoněn	uzákonit	k5eAaPmNgMnS	uzákonit
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
padl	padnout	k5eAaImAgInS	padnout
a	a	k8xC	a
ještě	ještě	k9	ještě
té	ten	k3xDgFnSc2	ten
noci	noc	k1gFnSc2	noc
měl	mít	k5eAaImAgInS	mít
Gándhí	Gándhí	k1gFnSc4	Gándhí
vnuknutí	vnuknutí	k1gNnSc2	vnuknutí
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
den	den	k1gInSc1	den
modliteb	modlitba	k1gFnPc2	modlitba
<g/>
,	,	kIx,	,
hartál	hartála	k1gFnPc2	hartála
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
vskutku	vskutku	k9	vskutku
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
celá	celý	k2eAgFnSc1d1	celá
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
konce	konec	k1gInSc2	konec
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
<g/>
,	,	kIx,	,
města	město	k1gNnSc2	město
i	i	k8xC	i
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
,	,	kIx,	,
zachovávala	zachovávat	k5eAaImAgFnS	zachovávat
v	v	k7c4	v
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
úplný	úplný	k2eAgInSc1d1	úplný
hartál	hartál	k1gInSc1	hartál
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
proti	proti	k7c3	proti
této	tento	k3xDgFnSc3	tento
revoluci	revoluce	k1gFnSc3	revoluce
násilně	násilně	k6eAd1	násilně
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Gándhí	Gándhí	k1gNnSc1	Gándhí
protiútokem	protiútok	k1gInSc7	protiútok
zahájil	zahájit	k5eAaPmAgMnS	zahájit
občanskou	občanský	k2eAgFnSc4d1	občanská
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
<g/>
,	,	kIx,	,
pasivní	pasivní	k2eAgFnSc4d1	pasivní
rezistenci	rezistence	k1gFnSc4	rezistence
–	–	k?	–
satjágrahu	satjágrah	k1gInSc2	satjágrah
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
krátké	krátký	k2eAgNnSc4d1	krátké
období	období	k1gNnSc4	období
přispěl	přispět	k5eAaPmAgInS	přispět
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
hindů	hind	k1gMnPc2	hind
a	a	k8xC	a
muslimů	muslim	k1gMnPc2	muslim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
byla	být	k5eAaImAgFnS	být
složitější	složitý	k2eAgFnSc1d2	složitější
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgNnPc7	dva
náboženstvími	náboženství	k1gNnPc7	náboženství
využívala	využívat	k5eAaImAgFnS	využívat
vláda	vláda	k1gFnSc1	vláda
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
Gándhímu	Gándhí	k1gMnSc3	Gándhí
nelíbilo	líbit	k5eNaImAgNnS	líbit
a	a	k8xC	a
zahájil	zahájit	k5eAaPmAgInS	zahájit
jednadvacetidenní	jednadvacetidenní	k2eAgInSc4d1	jednadvacetidenní
půst	půst	k1gInSc4	půst
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
téměř	téměř	k6eAd1	téměř
zbytečně	zbytečně	k6eAd1	zbytečně
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
života	život	k1gInSc2	život
bude	být	k5eAaImBp3nS	být
Gándhí	Gándhí	k2eAgNnSc1d1	Gándhí
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
sjednocení	sjednocení	k1gNnSc4	sjednocení
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
hindů	hind	k1gMnPc2	hind
<g/>
.	.	kIx.	.
</s>
<s>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
s	s	k7c7	s
muslimy	muslim	k1gMnPc7	muslim
a	a	k8xC	a
hindy	hind	k1gMnPc7	hind
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
dát	dát	k5eAaPmF	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
vůči	vůči	k7c3	vůči
tomuto	tento	k3xDgNnSc3	tento
nepřátelství	nepřátelství	k1gNnSc3	nepřátelství
podstoupil	podstoupit	k5eAaPmAgInS	podstoupit
půst	půst	k1gInSc1	půst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
pramalým	pramalý	k2eAgInSc7d1	pramalý
úspěchem	úspěch	k1gInSc7	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
protentokrát	protentokrát	k6eAd1	protentokrát
bylo	být	k5eAaImAgNnS	být
spojenectví	spojenectví	k1gNnSc1	spojenectví
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
netoleroval	tolerovat	k5eNaImAgMnS	tolerovat
daň	daň	k1gFnSc4	daň
ze	z	k7c2	z
soli	sůl	k1gFnSc2	sůl
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
tisknout	tisknout	k5eAaImF	tisknout
zakázanou	zakázaný	k2eAgFnSc4d1	zakázaná
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
byla	být	k5eAaImAgFnS	být
zhypnotizována	zhypnotizovat	k5eAaPmNgFnS	zhypnotizovat
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
si	se	k3xPyFc3	se
kupovali	kupovat	k5eAaImAgMnP	kupovat
knihy	kniha	k1gFnPc4	kniha
<g/>
,	,	kIx,	,
nedbajíce	dbát	k5eNaImSgFnP	dbát
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
z	z	k7c2	z
uvěznění	uvěznění	k1gNnSc2	uvěznění
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
byl	být	k5eAaImAgMnS	být
tentokrát	tentokrát	k6eAd1	tentokrát
zatčen	zatknout	k5eAaPmNgMnS	zatknout
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
záhy	záhy	k6eAd1	záhy
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
měla	mít	k5eAaImAgFnS	mít
strach	strach	k1gInSc4	strach
z	z	k7c2	z
reakce	reakce	k1gFnSc2	reakce
lidu	lid	k1gInSc2	lid
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tehdy	tehdy	k6eAd1	tehdy
Gándhí	Gándhí	k1gMnSc1	Gándhí
narazil	narazit	k5eAaPmAgMnS	narazit
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
svůj	svůj	k3xOyFgInSc4	svůj
obrovský	obrovský	k2eAgInSc4d1	obrovský
omyl	omyl	k1gInSc4	omyl
<g/>
.	.	kIx.	.
</s>
<s>
Vzpoura	vzpoura	k1gFnSc1	vzpoura
přešla	přejít	k5eAaPmAgFnS	přejít
v	v	k7c6	v
násilí	násilí	k1gNnSc6	násilí
a	a	k8xC	a
rozlícený	rozlícený	k2eAgInSc1d1	rozlícený
dav	dav	k1gInSc1	dav
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
rozprášen	rozprášen	k2eAgInSc1d1	rozprášen
jízdní	jízdní	k2eAgFnSc7d1	jízdní
policií	policie	k1gFnSc7	policie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
přece	přece	k9	přece
Gándhí	Gándhí	k1gMnSc1	Gándhí
stále	stále	k6eAd1	stále
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lid	lid	k1gInSc1	lid
nemá	mít	k5eNaImIp3nS	mít
povahu	povaha	k1gFnSc4	povaha
násilnickou	násilnický	k2eAgFnSc4d1	násilnická
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
mírumilovnou	mírumilovný	k2eAgFnSc7d1	mírumilovná
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Gándhí	Gándhí	k1gMnSc1	Gándhí
nakonec	nakonec	k6eAd1	nakonec
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stávka	stávka	k1gFnSc1	stávka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vymkla	vymknout	k5eAaPmAgFnS	vymknout
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životopise	životopis	k1gInSc6	životopis
nezmínil	zmínit	k5eNaPmAgMnS	zmínit
asi	asi	k9	asi
nejstrašnější	strašný	k2eAgInSc4d3	nejstrašnější
důsledek	důsledek	k1gInSc4	důsledek
svého	svůj	k3xOyFgNnSc2	svůj
jednání	jednání	k1gNnSc2	jednání
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
pokojné	pokojný	k2eAgFnSc6d1	pokojná
manifestaci	manifestace	k1gFnSc6	manifestace
v	v	k7c6	v
Amritsaru	Amritsar	k1gInSc6	Amritsar
jednotkami	jednotka	k1gFnPc7	jednotka
generála	generál	k1gMnSc2	generál
Dyera	Dyero	k1gNnSc2	Dyero
zmasakrováno	zmasakrovat	k5eAaPmNgNnS	zmasakrovat
na	na	k7c6	na
tisíc	tisíc	k4xCgInSc1	tisíc
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
okolo	okolo	k7c2	okolo
jedenácti	jedenáct	k4xCc2	jedenáct
set	sto	k4xCgNnPc2	sto
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Místoguvernér	místoguvernér	k1gMnSc1	místoguvernér
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Dwyer	Dwyer	k1gMnSc1	Dwyer
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Paňdžábu	Paňdžáb	k1gInSc6	Paňdžáb
stanné	stanný	k2eAgNnSc4d1	stanné
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgFnSc1d1	následná
vlna	vlna	k1gFnSc1	vlna
represí	represe	k1gFnPc2	represe
a	a	k8xC	a
zatýkání	zatýkání	k1gNnPc2	zatýkání
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
dosud	dosud	k6eAd1	dosud
loajálních	loajální	k2eAgInPc6d1	loajální
Indech	Indus	k1gInPc6	Indus
definitivně	definitivně	k6eAd1	definitivně
zlomila	zlomit	k5eAaPmAgFnS	zlomit
důvěru	důvěra	k1gFnSc4	důvěra
v	v	k7c4	v
britskou	britský	k2eAgFnSc4d1	britská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
promluvil	promluvit	k5eAaPmAgMnS	promluvit
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
o	o	k7c6	o
skutečné	skutečný	k2eAgFnSc6d1	skutečná
podstatě	podstata	k1gFnSc6	podstata
satjágrahy	satjágraha	k1gFnSc2	satjágraha
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nespokojen	spokojen	k2eNgMnSc1d1	nespokojen
s	s	k7c7	s
násilím	násilí	k1gNnSc7	násilí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vykvetlo	vykvést	k5eAaPmAgNnS	vykvést
v	v	k7c6	v
jeho	jeho	k3xOp3gMnPc6	jeho
přívržencích	přívrženec	k1gMnPc6	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Nemělo	mít	k5eNaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
však	však	k9	však
žádný	žádný	k3yNgInSc1	žádný
účinek	účinek	k1gInSc1	účinek
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xC	tak
sám	sám	k3xTgMnSc1	sám
satjágrahu	satjágraha	k1gFnSc4	satjágraha
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
.	.	kIx.	.
</s>
<s>
Seznal	seznat	k5eAaPmAgMnS	seznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
satjágraha	satjágraha	k1gFnSc1	satjágraha
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
rozsahu	rozsah	k1gInSc6	rozsah
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
uhlídat	uhlídat	k5eAaPmF	uhlídat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
následků	následek	k1gInPc2	následek
a	a	k8xC	a
krveprolití	krveprolití	k1gNnSc2	krveprolití
svůj	svůj	k3xOyFgInSc4	svůj
omyl	omyl	k1gInSc1	omyl
veřejně	veřejně	k6eAd1	veřejně
přiznal	přiznat	k5eAaPmAgInS	přiznat
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
víc	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
nezaobíral	zaobírat	k5eNaImAgMnS	zaobírat
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
si	se	k3xPyFc3	se
neuvědomoval	uvědomovat	k5eNaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc4	některý
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
metody	metoda	k1gFnSc2	metoda
satjágrahy	satjágraha	k1gFnSc2	satjágraha
se	se	k3xPyFc4	se
příčí	příčit	k5eAaImIp3nS	příčit
muslimům	muslim	k1gMnPc3	muslim
a	a	k8xC	a
mimoděk	mimoděk	k6eAd1	mimoděk
tak	tak	k6eAd1	tak
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
komunitami	komunita	k1gFnPc7	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
velkých	velký	k2eAgInPc2d1	velký
úspěchů	úspěch	k1gInPc2	úspěch
na	na	k7c6	na
Armitranském	Armitranský	k2eAgInSc6d1	Armitranský
kongresu	kongres	k1gInSc6	kongres
<g/>
,	,	kIx,	,
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
jako	jako	k9	jako
vážený	vážený	k2eAgMnSc1d1	vážený
člen	člen	k1gMnSc1	člen
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
Indům	Ind	k1gMnPc3	Ind
velkou	velký	k2eAgFnSc4d1	velká
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
také	také	k9	také
dospěl	dochvít	k5eAaPmAgInS	dochvít
k	k	k7c3	k
podstatě	podstata	k1gFnSc3	podstata
své	svůj	k3xOyFgFnSc2	svůj
víry	víra	k1gFnSc2	víra
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
jiného	jiný	k2eAgMnSc4d1	jiný
Boha	bůh	k1gMnSc4	bůh
než	než	k8xS	než
Pravdy	pravda	k1gFnPc4	pravda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1920	[number]	k4	1920
Gándhí	Gándhí	k1gMnSc1	Gándhí
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
kampaň	kampaň	k1gFnSc4	kampaň
nespolupráce	spolupráko	k6eNd1	spolupráko
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
<g/>
,	,	kIx,	,
když	když	k8xS	když
vrátil	vrátit	k5eAaPmAgInS	vrátit
úřadům	úřad	k1gInPc3	úřad
tři	tři	k4xCgFnPc4	tři
medaile	medaile	k1gFnPc4	medaile
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
obdržel	obdržet	k5eAaPmAgInS	obdržet
za	za	k7c2	za
búrské	búrský	k2eAgFnSc2d1	búrská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
plánu	plán	k1gInSc6	plán
brzy	brzy	k6eAd1	brzy
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
na	na	k7c6	na
mimořádném	mimořádný	k2eAgInSc6d1	mimořádný
sjezdu	sjezd	k1gInSc6	sjezd
v	v	k7c6	v
Kalkatě	Kalkata	k1gFnSc6	Kalkata
také	také	k9	také
Kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
během	během	k7c2	během
několika	několik	k4yIc2	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
proměnil	proměnit	k5eAaPmAgInS	proměnit
z	z	k7c2	z
honoračního	honorační	k2eAgNnSc2d1	honorační
seskupení	seskupení	k1gNnSc2	seskupení
elit	elita	k1gFnPc2	elita
v	v	k7c4	v
masovou	masový	k2eAgFnSc4d1	masová
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
s	s	k7c7	s
jasným	jasný	k2eAgInSc7d1	jasný
programem	program	k1gInSc7	program
a	a	k8xC	a
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Gándhí	Gándhí	k1gMnSc1	Gándhí
jen	jen	k9	jen
málokdy	málokdy	k6eAd1	málokdy
zastával	zastávat	k5eAaImAgMnS	zastávat
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
nějakou	nějaký	k3yIgFnSc4	nějaký
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
platil	platit	k5eAaImAgInS	platit
za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
politického	politický	k2eAgMnSc2d1	politický
guru	guru	k1gMnSc2	guru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1921	[number]	k4	1921
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Gándhí	Gándhí	k1gMnSc1	Gándhí
hnutí	hnutí	k1gNnSc2	hnutí
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
přerůst	přerůst	k5eAaPmF	přerůst
v	v	k7c6	v
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
však	však	k8xC	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
incidentu	incident	k1gInSc3	incident
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Čaurí	Čaurí	k2eAgFnSc1d1	Čaurí
Čaura	Čaura	k1gFnSc1	Čaura
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
účastníci	účastník	k1gMnPc1	účastník
původně	původně	k6eAd1	původně
pokojné	pokojný	k2eAgFnSc2d1	pokojná
demonstrace	demonstrace	k1gFnSc2	demonstrace
zapálili	zapálit	k5eAaPmAgMnP	zapálit
místní	místní	k2eAgFnSc4d1	místní
policejní	policejní	k2eAgFnSc4d1	policejní
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
uhořelo	uhořet	k5eAaPmAgNnS	uhořet
22	[number]	k4	22
policistů	policista	k1gMnPc2	policista
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k6eAd1	Gándhí
to	ten	k3xDgNnSc4	ten
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
osobní	osobní	k2eAgNnSc4d1	osobní
politické	politický	k2eAgNnSc4d1	politické
selhání	selhání	k1gNnSc4	selhání
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc4	hnutí
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
okamžitě	okamžitě	k6eAd1	okamžitě
odvolal	odvolat	k5eAaPmAgMnS	odvolat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
posléze	posléze	k6eAd1	posléze
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
odliv	odliv	k1gInSc4	odliv
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1922	[number]	k4	1922
byl	být	k5eAaImAgMnS	být
Gándhí	Gándhí	k1gMnSc1	Gándhí
za	za	k7c2	za
naprostého	naprostý	k2eAgInSc2d1	naprostý
nezájmu	nezájem	k1gInSc2	nezájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
zatčen	zatknout	k5eAaPmNgMnS	zatknout
a	a	k8xC	a
odsouzen	odsoudit	k5eAaPmNgInS	odsoudit
k	k	k7c3	k
šestiletému	šestiletý	k2eAgNnSc3d1	šestileté
vězení	vězení	k1gNnSc3	vězení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1924	[number]	k4	1924
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
postihl	postihnout	k5eAaPmAgInS	postihnout
akutní	akutní	k2eAgInSc1d1	akutní
zánět	zánět	k1gInSc1	zánět
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
za	za	k7c2	za
dramatických	dramatický	k2eAgFnPc2d1	dramatická
okolností	okolnost	k1gFnPc2	okolnost
operován	operovat	k5eAaImNgInS	operovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
zajistilo	zajistit	k5eAaPmAgNnS	zajistit
znovu	znovu	k6eAd1	znovu
pozornost	pozornost	k1gFnSc4	pozornost
a	a	k8xC	a
přízeň	přízeň	k1gFnSc4	přízeň
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
byl	být	k5eAaImAgMnS	být
omilostněn	omilostnit	k5eAaPmNgMnS	omilostnit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
strávil	strávit	k5eAaPmAgInS	strávit
za	za	k7c2	za
mřížemi	mříž	k1gFnPc7	mříž
<g/>
,	,	kIx,	,
však	však	k9	však
národně-osvobozenecké	národněsvobozenecký	k2eAgNnSc1d1	národně-osvobozenecké
hnutí	hnutí	k1gNnSc1	hnutí
upadlo	upadnout	k5eAaPmAgNnS	upadnout
a	a	k8xC	a
ze	z	k7c2	z
sjezdu	sjezd	k1gInSc2	sjezd
Kongresu	kongres	k1gInSc2	kongres
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
Gándhí	Gándhí	k1gMnSc1	Gándhí
odešel	odejít	k5eAaPmAgMnS	odejít
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
Gándhího	Gándhí	k2eAgInSc2d1	Gándhí
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
odstranit	odstranit	k5eAaPmF	odstranit
společenskou	společenský	k2eAgFnSc4d1	společenská
diskriminaci	diskriminace	k1gFnSc4	diskriminace
nedotknutelných	nedotknutelných	k?	nedotknutelných
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgMnPc4	který
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
označení	označení	k1gNnSc4	označení
haridžané	haridžaná	k1gFnSc2	haridžaná
(	(	kIx(	(
<g/>
boží	božit	k5eAaImIp3nP	božit
lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
reformátorů	reformátor	k1gMnPc2	reformátor
se	se	k3xPyFc4	se
však	však	k9	však
Gándhí	Gándhí	k1gMnSc1	Gándhí
nesnažil	snažit	k5eNaImAgMnS	snažit
zrušit	zrušit	k5eAaPmF	zrušit
indický	indický	k2eAgInSc4d1	indický
kastovní	kastovní	k2eAgInSc4d1	kastovní
systém	systém	k1gInSc4	systém
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
společenského	společenský	k2eAgNnSc2d1	společenské
uznání	uznání	k1gNnSc2	uznání
pro	pro	k7c4	pro
nízké	nízký	k2eAgInPc4d1	nízký
a	a	k8xC	a
nedotknutelné	nedotknutelné	k?	nedotknutelné
kasty	kasta	k1gFnSc2	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
nedotknutelných	nedotknutelných	k?	nedotknutelných
Bhímráo	Bhímráo	k1gMnSc1	Bhímráo
Rámdží	Rámdž	k1gFnPc2	Rámdž
Ámbédkar	Ámbédkar	k1gMnSc1	Ámbédkar
však	však	k9	však
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Gándhího	Gándhí	k1gMnSc4	Gándhí
i	i	k9	i
další	další	k2eAgMnPc4d1	další
vůdce	vůdce	k1gMnPc4	vůdce
Indického	indický	k2eAgInSc2d1	indický
národního	národní	k2eAgInSc2d1	národní
kongresu	kongres	k1gInSc2	kongres
za	za	k7c4	za
přílišný	přílišný	k2eAgInSc4d1	přílišný
konzervatismus	konzervatismus	k1gInSc4	konzervatismus
a	a	k8xC	a
zbytečný	zbytečný	k2eAgInSc4d1	zbytečný
patos	patos	k1gInSc4	patos
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
ničeho	nic	k3yNnSc2	nic
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
sám	sám	k3xTgInSc4	sám
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
britské	britský	k2eAgFnSc3d1	britská
vládě	vláda	k1gFnSc3	vláda
systém	systém	k1gInSc4	systém
oddělené	oddělený	k2eAgFnSc2d1	oddělená
volební	volební	k2eAgFnSc2d1	volební
kurie	kurie	k1gFnSc2	kurie
pro	pro	k7c4	pro
nedotýkatelné	dotýkatelný	k2eNgNnSc4d1	dotýkatelný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
září	září	k1gNnSc6	září
1932	[number]	k4	1932
Britové	Brit	k1gMnPc1	Brit
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
návrhem	návrh	k1gInSc7	návrh
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Gándhí	Gándhí	k1gFnPc4	Gándhí
další	další	k2eAgFnPc4d1	další
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
protestních	protestní	k2eAgFnPc2d1	protestní
hladovek	hladovka	k1gFnPc2	hladovka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
by	by	kYmCp3nP	by
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
práva	právo	k1gNnPc1	právo
pro	pro	k7c4	pro
nedotknutelné	nedotknutelné	k?	nedotknutelné
rozvrátila	rozvrátit	k5eAaPmAgFnS	rozvrátit
hinduistickou	hinduistický	k2eAgFnSc4d1	hinduistická
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
donutil	donutit	k5eAaPmAgInS	donutit
Ámbédkara	Ámbédkar	k1gMnSc4	Ámbédkar
ustoupit	ustoupit	k5eAaPmF	ustoupit
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
znesvářené	znesvářený	k2eAgFnPc1d1	znesvářená
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1932	[number]	k4	1932
dohodly	dohodnout	k5eAaPmAgInP	dohodnout
na	na	k7c6	na
společném	společný	k2eAgInSc6d1	společný
kompromisu	kompromis	k1gInSc6	kompromis
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
Púnský	Púnský	k2eAgInSc4d1	Púnský
pakt	pakt	k1gInSc4	pakt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zaručil	zaručit	k5eAaPmAgMnS	zaručit
"	"	kIx"	"
<g/>
utlačovaným	utlačovaný	k2eAgFnPc3d1	utlačovaná
třídám	třída	k1gFnPc3	třída
<g/>
"	"	kIx"	"
rezervovaná	rezervovaný	k2eAgNnPc4d1	rezervované
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
zákonodárných	zákonodárný	k2eAgInPc6d1	zákonodárný
sborech	sbor	k1gInPc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1930	[number]	k4	1930
začíná	začínat	k5eAaImIp3nS	začínat
boj	boj	k1gInSc4	boj
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Indie	Indie	k1gFnSc1	Indie
–	–	k?	–
zemědělci	zemědělec	k1gMnPc1	zemědělec
<g/>
,	,	kIx,	,
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
i	i	k8xC	i
prostý	prostý	k2eAgInSc1d1	prostý
lid	lid	k1gInSc1	lid
–	–	k?	–
odmítá	odmítat	k5eAaImIp3nS	odmítat
nadále	nadále	k6eAd1	nadále
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
řídil	řídit	k5eAaImAgInS	řídit
jednašedesátiletý	jednašedesátiletý	k2eAgInSc1d1	jednašedesátiletý
Mahátma	Mahátmum	k1gNnSc2	Mahátmum
Gándhí	Gándh	k1gFnSc7	Gándh
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
když	když	k8xS	když
došel	dojít	k5eAaPmAgInS	dojít
na	na	k7c4	na
konec	konec	k1gInSc4	konec
své	svůj	k3xOyFgFnSc2	svůj
téměř	téměř	k6eAd1	téměř
čtyřicetidenní	čtyřicetidenní	k2eAgFnSc2d1	čtyřicetidenní
pouti	pouť	k1gFnSc2	pouť
<g/>
,	,	kIx,	,
porušil	porušit	k5eAaPmAgInS	porušit
zákon	zákon	k1gInSc1	zákon
o	o	k7c4	o
soli	sůl	k1gFnPc4	sůl
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
stály	stát	k5eAaImAgInP	stát
tisíce	tisíc	k4xCgInPc1	tisíc
přívrženců	přívrženec	k1gMnPc2	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
–	–	k?	–
absolutní	absolutní	k2eAgFnSc2d1	absolutní
občanské	občanský	k2eAgFnSc2d1	občanská
neposlušnosti	neposlušnost	k1gFnSc2	neposlušnost
–	–	k?	–
uvrhla	uvrhnout	k5eAaPmAgFnS	uvrhnout
vláda	vláda	k1gFnSc1	vláda
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
na	na	k7c4	na
60	[number]	k4	60
000	[number]	k4	000
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
netušil	tušit	k5eNaImAgMnS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
sedmnáct	sedmnáct	k4xCc4	sedmnáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
bude	být	k5eAaImBp3nS	být
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
osvobození	osvobození	k1gNnSc1	osvobození
a	a	k8xC	a
nezávislost	nezávislost	k1gFnSc1	nezávislost
Indie	Indie	k1gFnSc2	Indie
...	...	k?	...
Než	než	k8xS	než
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
do	do	k7c2	do
Kongresu	kongres	k1gInSc2	kongres
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
neoficiálním	neoficiální	k2eAgMnSc7d1	neoficiální
vůdcem	vůdce	k1gMnSc7	vůdce
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
postupně	postupně	k6eAd1	postupně
zlepšoval	zlepšovat	k5eAaImAgMnS	zlepšovat
její	její	k3xOp3gNnSc4	její
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
Gándhí	Gándhí	k1gMnSc1	Gándhí
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
slepě	slepě	k6eAd1	slepě
vypomáhat	vypomáhat	k5eAaImF	vypomáhat
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1941	[number]	k4	1941
přišla	přijít	k5eAaPmAgFnS	přijít
válka	válka	k1gFnSc1	válka
i	i	k9	i
na	na	k7c4	na
Východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
indický	indický	k2eAgInSc4d1	indický
Kongres	kongres	k1gInSc4	kongres
o	o	k7c6	o
příslibu	příslib	k1gInSc6	příslib
pomoci	pomoc	k1gFnSc2	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
nechtěli	chtít	k5eNaImAgMnP	chtít
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
však	však	k9	však
stála	stát	k5eAaImAgFnS	stát
okupace	okupace	k1gFnSc1	okupace
před	před	k7c7	před
branami	brána	k1gFnPc7	brána
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
Kongresu	kongres	k1gInSc2	kongres
včetně	včetně	k7c2	včetně
Gándhího	Gándhí	k1gMnSc2	Gándhí
bylo	být	k5eAaImAgNnS	být
britskou	britský	k2eAgFnSc7d1	britská
vládou	vláda	k1gFnSc7	vláda
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
<g/>
.	.	kIx.	.
</s>
<s>
Lid	lid	k1gInSc1	lid
povstal	povstat	k5eAaPmAgInS	povstat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zuřivosti	zuřivost	k1gFnSc6	zuřivost
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgInS	bojovat
proti	proti	k7c3	proti
britské	britský	k2eAgFnSc3d1	britská
vládě	vláda	k1gFnSc3	vláda
všemi	všecek	k3xTgInPc7	všecek
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
střelbou	střelba	k1gFnSc7	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Vesnice	vesnice	k1gFnPc1	vesnice
byly	být	k5eAaImAgFnP	být
bombardovány	bombardovat	k5eAaImNgFnP	bombardovat
a	a	k8xC	a
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
padlo	padnout	k5eAaImAgNnS	padnout
na	na	k7c4	na
25	[number]	k4	25
tisíc	tisíc	k4xCgInPc2	tisíc
Indů	Indus	k1gInPc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
Británie	Británie	k1gFnSc1	Británie
bojovala	bojovat	k5eAaImAgFnS	bojovat
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
i	i	k8xC	i
Východě	východ	k1gInSc6	východ
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
vraždila	vraždit	k5eAaImAgFnS	vraždit
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
společenstvo	společenstvo	k1gNnSc1	společenstvo
jejich	jejich	k3xOp3gMnSc2	jejich
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
Indů	Ind	k1gMnPc2	Ind
byl	být	k5eAaImAgInS	být
potlačen	potlačen	k2eAgInSc1d1	potlačen
<g/>
.	.	kIx.	.
</s>
<s>
Nastal	nastat	k5eAaPmAgInS	nastat
hladomor	hladomor	k1gInSc1	hladomor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přinesl	přinést	k5eAaPmAgInS	přinést
zkázu	zkáza	k1gFnSc4	zkáza
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
3,4	[number]	k4	3,4
miliónů	milión	k4xCgInPc2	milión
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
také	také	k9	také
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
Mahátmova	Mahátmův	k2eAgFnSc1d1	Mahátmův
manželka	manželka	k1gFnSc1	manželka
Kasturba	Kasturba	k1gFnSc1	Kasturba
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Gándhí	Gándhí	k1gMnSc1	Gándhí
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
doktorům	doktor	k1gMnPc3	doktor
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
podali	podat	k5eAaPmAgMnP	podat
penicilin	penicilin	k1gInSc4	penicilin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odsuzoval	odsuzovat	k5eAaImAgInS	odsuzovat
jako	jako	k9	jako
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
lék	lék	k1gInSc1	lék
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
Gándhí	Gándhí	k1gFnSc4	Gándhí
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
toho	ten	k3xDgMnSc4	ten
hodně	hodně	k6eAd1	hodně
co	co	k3yInSc1	co
napravovat	napravovat	k5eAaImF	napravovat
<g/>
.	.	kIx.	.
</s>
<s>
Okupační	okupační	k2eAgNnPc1d1	okupační
vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
stáhla	stáhnout	k5eAaPmAgNnP	stáhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
země	země	k1gFnSc1	země
krvácela	krvácet	k5eAaImAgFnS	krvácet
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
vyhrocovat	vyhrocovat	k5eAaImF	vyhrocovat
hiduisticko-muslimské	hiduistickouslimský	k2eAgInPc1d1	hiduisticko-muslimský
rozpory	rozpor	k1gInPc1	rozpor
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
vrcholil	vrcholit	k5eAaImAgInS	vrcholit
boj	boj	k1gInSc1	boj
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
už	už	k6eAd1	už
nebyla	být	k5eNaImAgFnS	být
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
problémem	problém	k1gInSc7	problém
byla	být	k5eAaImAgFnS	být
otázka	otázka	k1gFnSc1	otázka
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Kongresu	kongres	k1gInSc3	kongres
totiž	totiž	k9	totiž
nyní	nyní	k6eAd1	nyní
stála	stát	k5eAaImAgFnS	stát
Muslimská	muslimský	k2eAgFnSc1d1	muslimská
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
mocná	mocný	k2eAgFnSc1d1	mocná
frakce	frakce	k1gFnSc1	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gMnSc1	Gándhí
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
rozdělené	rozdělený	k2eAgFnSc2d1	rozdělená
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Muslimové	muslim	k1gMnPc1	muslim
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1946	[number]	k4	1946
džihád	džihád	k1gInSc4	džihád
(	(	kIx(	(
<g/>
svatou	svatý	k2eAgFnSc4d1	svatá
válku	válka	k1gFnSc4	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vraždili	vraždit	k5eAaImAgMnP	vraždit
hinduisty	hinduista	k1gMnPc4	hinduista
a	a	k8xC	a
násilně	násilně	k6eAd1	násilně
je	on	k3xPp3gFnPc4	on
nutili	nutit	k5eAaImAgMnP	nutit
k	k	k7c3	k
uznání	uznání	k1gNnSc3	uznání
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovali	reagovat	k5eAaBmAgMnP	reagovat
hinduisté	hinduista	k1gMnPc1	hinduista
masakrováním	masakrování	k1gNnSc7	masakrování
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
Gándhí	Gándh	k1gFnPc2	Gándh
nebojácně	bojácně	k6eNd1	bojácně
kráčel	kráčet	k5eAaImAgMnS	kráčet
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
už	už	k9	už
nikdo	nikdo	k3yNnSc1	nikdo
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
nemohl	moct	k5eNaImAgMnS	moct
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
už	už	k6eAd1	už
mu	on	k3xPp3gMnSc3	on
ublížit	ublížit	k5eAaPmF	ublížit
nemohli	moct	k5eNaImAgMnP	moct
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pokořen	pokořen	k2eAgMnSc1d1	pokořen
<g/>
,	,	kIx,	,
když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
zkázu	zkáza	k1gFnSc4	zkáza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přesto	přesto	k8xC	přesto
kráčel	kráčet	k5eAaImAgMnS	kráčet
a	a	k8xC	a
hlásal	hlásat	k5eAaImAgInS	hlásat
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
osvobodit	osvobodit	k5eAaPmF	osvobodit
Indii	Indie	k1gFnSc4	Indie
pod	pod	k7c7	pod
podmínkou	podmínka	k1gFnSc7	podmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
země	země	k1gFnSc1	země
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
Pákistán	Pákistán	k1gInSc4	Pákistán
a	a	k8xC	a
Hindustán	Hindustán	k1gInSc4	Hindustán
<g/>
.	.	kIx.	.
</s>
<s>
Najednou	najednou	k6eAd1	najednou
byl	být	k5eAaImAgMnS	být
Gándhí	Gándhí	k1gMnSc1	Gándhí
jen	jen	k6eAd1	jen
poutníkem	poutník	k1gMnSc7	poutník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nesouhlasil	souhlasit	k5eNaImAgInS	souhlasit
–	–	k?	–
lid	lid	k1gInSc1	lid
byl	být	k5eAaImAgInS	být
lačný	lačný	k2eAgMnSc1d1	lačný
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
svobodná	svobodný	k2eAgFnSc1d1	svobodná
Indie	Indie	k1gFnSc1	Indie
dvou	dva	k4xCgFnPc2	dva
dominií	dominie	k1gFnPc2	dominie
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhí	k1gFnPc1	Gándhí
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
77	[number]	k4	77
letech	léto	k1gNnPc6	léto
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
hluboký	hluboký	k2eAgInSc4d1	hluboký
smutek	smutek	k1gInSc4	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
co	co	k3yQnSc4	co
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
bojoval	bojovat	k5eAaImAgMnS	bojovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
mezi	mezi	k7c7	mezi
hinduisty	hinduista	k1gMnPc7	hinduista
a	a	k8xC	a
muslimy	muslim	k1gMnPc7	muslim
však	však	k9	však
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
přerostly	přerůst	k5eAaPmAgFnP	přerůst
ve	v	k7c4	v
válku	válka	k1gFnSc4	válka
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
pákistánské	pákistánský	k2eAgFnPc1d1	pákistánská
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
vtrhly	vtrhnout	k5eAaPmAgFnP	vtrhnout
do	do	k7c2	do
Kašmíru	Kašmír	k1gInSc2	Kašmír
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
právní	právní	k2eAgInSc1d1	právní
statut	statut	k1gInSc1	statut
nebyl	být	k5eNaImAgInS	být
prozatím	prozatím	k6eAd1	prozatím
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
reagovala	reagovat	k5eAaBmAgFnS	reagovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastavila	zastavit	k5eAaPmAgFnS	zastavit
převod	převod	k1gInSc4	převod
550	[number]	k4	550
milionů	milion	k4xCgInPc2	milion
rupií	rupie	k1gFnPc2	rupie
do	do	k7c2	do
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
součástí	součást	k1gFnSc7	součást
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
rozdělení	rozdělení	k1gNnSc6	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
Gándhí	Gándhit	k5eAaPmIp3nS	Gándhit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
další	další	k2eAgMnSc1d1	další
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
časově	časově	k6eAd1	časově
neomezených	omezený	k2eNgFnPc2d1	neomezená
hladovek	hladovka	k1gFnPc2	hladovka
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
nedohodnou	dohodnout	k5eNaPmIp3nP	dohodnout
a	a	k8xC	a
nesmíří	smířit	k5eNaPmIp3nP	smířit
<g/>
.	.	kIx.	.
</s>
<s>
Indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
tak	tak	k6eAd1	tak
nakonec	nakonec	k6eAd1	nakonec
zrušila	zrušit	k5eAaPmAgFnS	zrušit
své	svůj	k3xOyFgNnSc4	svůj
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
do	do	k7c2	do
Pákistánu	Pákistán	k1gInSc2	Pákistán
poslala	poslat	k5eAaPmAgFnS	poslat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Gándhí	Gándhí	k1gFnSc1	Gándhí
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
neztratil	ztratit	k5eNaPmAgMnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Transakce	transakce	k1gFnSc1	transakce
ovšem	ovšem	k9	ovšem
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
mezi	mezi	k7c7	mezi
představiteli	představitel	k1gMnPc7	představitel
hinduistické	hinduistický	k2eAgFnSc2d1	hinduistická
komunity	komunita	k1gFnSc2	komunita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
vládu	vláda	k1gFnSc4	vláda
i	i	k8xC	i
Gándhího	Gándhí	k1gMnSc4	Gándhí
za	za	k7c4	za
zbytečné	zbytečný	k2eAgInPc4d1	zbytečný
ústupky	ústupek	k1gInPc4	ústupek
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
hinduistických	hinduistický	k2eAgMnPc2d1	hinduistický
radikálů	radikál	k1gMnPc2	radikál
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c4	na
organizace	organizace	k1gFnPc4	organizace
Hindú	Hindú	k1gMnSc2	Hindú
mahásabha	mahásabh	k1gMnSc2	mahásabh
a	a	k8xC	a
Ráštríj	Ráštríj	k1gMnSc1	Ráštríj
svajamsévak	svajamsévak	k1gMnSc1	svajamsévak
sangh	sangh	k1gMnSc1	sangh
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
Gándhího	Gándhí	k1gMnSc4	Gándhí
odstranit	odstranit	k5eAaPmF	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
pumový	pumový	k2eAgInSc4d1	pumový
atentát	atentát	k1gInSc4	atentát
14	[number]	k4	14
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1948	[number]	k4	1948
byl	být	k5eAaImAgMnS	být
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
však	však	k9	však
hinduistický	hinduistický	k2eAgMnSc1d1	hinduistický
aktivista	aktivista	k1gMnSc1	aktivista
Nathuram	Nathuram	k1gInSc4	Nathuram
Gódsé	Gódsý	k2eAgFnSc2d1	Gódsý
zastihl	zastihnout	k5eAaPmAgMnS	zastihnout
Gándhího	Gándhí	k1gMnSc4	Gándhí
při	při	k7c6	při
večerní	večerní	k2eAgFnSc6d1	večerní
procházce	procházka	k1gFnSc6	procházka
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Dillí	Dillí	k1gNnSc6	Dillí
a	a	k8xC	a
usmrtil	usmrtit	k5eAaPmAgMnS	usmrtit
jej	on	k3xPp3gNnSc4	on
pistolí	pistol	k1gFnSc7	pistol
beretta	beretta	k1gMnSc1	beretta
M1934	M1934	k1gFnSc2	M1934
9	[number]	k4	9
<g/>
mm	mm	kA	mm
Browning	browning	k1gInSc4	browning
výrobní	výrobní	k2eAgNnSc4d1	výrobní
číslo	číslo	k1gNnSc4	číslo
606824	[number]	k4	606824
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Gódsé	Gódsý	k2eAgNnSc4d1	Gódsý
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
dobrovolně	dobrovolně	k6eAd1	dobrovolně
vzdal	vzdát	k5eAaPmAgMnS	vzdát
policii	policie	k1gFnSc4	policie
<g/>
,	,	kIx,	,
v	v	k7c6	v
soudním	soudní	k2eAgInSc6d1	soudní
procesu	proces	k1gInSc6	proces
byl	být	k5eAaImAgMnS	být
později	pozdě	k6eAd2	pozdě
odsouzen	odsouzet	k5eAaImNgMnS	odsouzet
k	k	k7c3	k
smrti	smrt	k1gFnSc6	smrt
oběšením	oběšení	k1gNnSc7	oběšení
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1949	[number]	k4	1949
popraven	popraven	k2eAgInSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
Móhandás	Móhandás	k6eAd1	Móhandás
Karamčand	Karamčand	k1gInSc4	Karamčand
Gándhí	Gándhí	k1gFnSc2	Gándhí
zemřel	zemřít	k5eAaPmAgMnS	zemřít
s	s	k7c7	s
modlitbou	modlitba	k1gFnSc7	modlitba
na	na	k7c6	na
rtech	ret	k1gInPc6	ret
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
78	[number]	k4	78
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
jeho	jeho	k3xOp3gFnSc7	jeho
rakví	rakev	k1gFnSc7	rakev
kráčel	kráčet	k5eAaImAgMnS	kráčet
milionový	milionový	k2eAgInSc4d1	milionový
průvod	průvod	k1gInSc4	průvod
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
popel	popel	k1gInSc1	popel
byl	být	k5eAaImAgInS	být
vhozen	vhodit	k5eAaPmNgInS	vhodit
do	do	k7c2	do
soutoku	soutok	k1gInSc2	soutok
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Gangy	Ganga	k1gFnSc2	Ganga
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
zástupců	zástupce	k1gMnPc2	zástupce
států	stát	k1gInPc2	stát
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
přijelo	přijet	k5eAaPmAgNnS	přijet
vzdát	vzdát	k5eAaPmF	vzdát
hold	hold	k1gInSc4	hold
malému	malý	k2eAgMnSc3d1	malý
velkému	velký	k2eAgMnSc3d1	velký
muži	muž	k1gMnSc3	muž
<g/>
...	...	k?	...
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
dala	dát	k5eAaPmAgFnS	dát
za	za	k7c4	za
pravdu	pravda	k1gFnSc4	pravda
jeho	jeho	k3xOp3gFnPc3	jeho
myšlenkám	myšlenka	k1gFnPc3	myšlenka
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
konečně	konečně	k6eAd1	konečně
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
svobody	svoboda	k1gFnPc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Gándhí	Gándhí	k1gMnSc1	Gándhí
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
své	svůj	k3xOyFgMnPc4	svůj
následovníky	následovník	k1gMnPc4	následovník
<g/>
,	,	kIx,	,
snad	snad	k9	snad
nejlépe	dobře	k6eAd3	dobře
demonstrují	demonstrovat	k5eAaBmIp3nP	demonstrovat
slova	slovo	k1gNnPc1	slovo
Džaváharlála	Džaváharlál	k1gMnSc2	Džaváharlál
Néhrúa	Néhrúus	k1gMnSc2	Néhrúus
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Budoucnost	budoucnost	k1gFnSc1	budoucnost
náleží	náležet	k5eAaImIp3nS	náležet
nám.	nám.	k?	nám.
Ať	ať	k9	ať
půjdeme	jít	k5eAaImIp1nP	jít
kamkoli	kamkoli	k6eAd1	kamkoli
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
jsme	být	k5eAaImIp1nP	být
jakkoli	jakkoli	k6eAd1	jakkoli
hodni	hoden	k2eAgMnPc1d1	hoden
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
ještě	ještě	k9	ještě
onu	onen	k3xDgFnSc4	onen
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nám	my	k3xPp1nPc3	my
dal	dát	k5eAaPmAgMnS	dát
náš	náš	k3xOp1gMnSc1	náš
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
síla	síla	k1gFnSc1	síla
nevychází	vycházet	k5eNaImIp3nS	vycházet
jen	jen	k9	jen
z	z	k7c2	z
něho	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
poselství	poselství	k1gNnPc2	poselství
<g/>
,	,	kIx,	,
a	a	k8xC	a
já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
i	i	k9	i
každý	každý	k3xTgInSc1	každý
další	další	k2eAgInSc1d1	další
úsvit	úsvit	k1gInSc1	úsvit
slavnostně	slavnostně	k6eAd1	slavnostně
zavazuji	zavazovat	k5eAaImIp1nS	zavazovat
k	k	k7c3	k
službě	služba	k1gFnSc3	služba
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
ideálům	ideál	k1gInPc3	ideál
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
před	před	k7c4	před
nás	my	k3xPp1nPc4	my
Gándhí	Gándhí	k2eAgMnSc1d1	Gándhí
postavil	postavit	k5eAaPmAgMnS	postavit
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
