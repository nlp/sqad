<s>
Mahátma	Mahátma	k1gFnSc1	Mahátma
Gándhí	Gándhí	k1gFnSc1	Gándhí
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Móhandás	Móhandása	k1gFnPc2	Móhandása
Karamčand	Karamčand	k1gInSc4	Karamčand
Gándhí	Gándhí	k1gFnSc4	Gándhí
(	(	kIx(	(
<g/>
v	v	k7c6	v
dévanágarí	dévanágarí	k1gFnSc6	dévanágarí
म	म	k?	म
<g/>
ो	ो	k?	ो
<g/>
ह	ह	k?	ह
<g/>
ा	ा	k?	ा
<g/>
स	स	k?	स
क	क	k?	क
<g/>
्	्	k?	्
<g/>
द	द	k?	द
ग	ग	k?	ग
<g/>
ा	ा	k?	ा
<g/>
ं	ं	k?	ं
<g/>
ध	ध	k?	ध
<g/>
ी	ी	k?	ी
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
Pórbandar	Pórbandar	k1gMnSc1	Pórbandar
<g/>
,	,	kIx,	,
Kathiawar	Kathiawar	k1gMnSc1	Kathiawar
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Indie	Indie	k1gFnSc1	Indie
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
Dillí	Dillí	k1gNnSc1	Dillí
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
politických	politický	k2eAgMnPc2d1	politický
a	a	k8xC	a
duchovních	duchovní	k2eAgMnPc2d1	duchovní
vůdců	vůdce	k1gMnPc2	vůdce
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
indického	indický	k2eAgNnSc2d1	indické
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
