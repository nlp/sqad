<s>
Paštština	Paštština	k1gFnSc1	Paštština
<g/>
,	,	kIx,	,
též	též	k9	též
paštunština	paštunština	k1gFnSc1	paštunština
či	či	k8xC	či
paštó	paštó	k?	paštó
(	(	kIx(	(
<g/>
paštunsky	paštunsky	k6eAd1	paštunsky
<g/>
:	:	kIx,	:
پ	پ	k?	پ
paštó	paštó	k?	paštó
<g/>
/	/	kIx~	/
<g/>
pachtó	pachtó	k?	pachtó
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
íránský	íránský	k2eAgInSc1d1	íránský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Pašto	Pašto	k1gNnSc1	Pašto
je	být	k5eAaImIp3nS	být
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nP	mluvit
jim	on	k3xPp3gFnPc3	on
asi	asi	k9	asi
60	[number]	k4	60
%	%	kIx~	%
Afghánců	Afghánec	k1gMnPc2	Afghánec
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
část	část	k1gFnSc1	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
upraveným	upravený	k2eAgNnSc7d1	upravené
arabským	arabský	k2eAgNnSc7d1	arabské
písmem	písmo	k1gNnSc7	písmo
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Pašto	Pašto	k1gNnSc1	Pašto
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
od	od	k7c2	od
Džalálábádu	Džalálábád	k1gInSc2	Džalálábád
po	po	k7c4	po
Kandahár	Kandahár	k1gInSc4	Kandahár
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
severozápadně	severozápadně	k6eAd1	severozápadně
k	k	k7c3	k
Sabzaváru	Sabzavár	k1gInSc3	Sabzavár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
používají	používat	k5eAaImIp3nP	používat
pašto	pašto	k1gNnSc4	pašto
obyvatelé	obyvatel	k1gMnPc1	obyvatel
sídlící	sídlící	k2eAgMnPc1d1	sídlící
v	v	k7c6	v
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
hraničních	hraniční	k2eAgFnPc2d1	hraniční
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Pandžábu	Pandžáb	k1gInSc2	Pandžáb
a	a	k8xC	a
Balúčistánu	Balúčistán	k1gInSc2	Balúčistán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mluví	mluvit	k5eAaImIp3nS	mluvit
paštsky	paštsky	k6eAd1	paštsky
35	[number]	k4	35
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pašto	Pašto	k1gNnSc1	Pašto
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
padesát	padesát	k4xCc4	padesát
kmenových	kmenový	k2eAgInPc2d1	kmenový
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
skupiny	skupina	k1gFnPc4	skupina
Kandahárský	kandahárský	k2eAgInSc1d1	kandahárský
dialekt	dialekt	k1gInSc1	dialekt
Nangahárský	Nangahárský	k2eAgInSc1d1	Nangahárský
dialekt	dialekt	k1gInSc4	dialekt
Paktijský	Paktijský	k2eAgInSc4d1	Paktijský
dialekt	dialekt	k1gInSc4	dialekt
Vlivem	vlivem	k7c2	vlivem
islámu	islám	k1gInSc2	islám
je	být	k5eAaImIp3nS	být
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
náboženského	náboženský	k2eAgInSc2d1	náboženský
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
přejatá	přejatý	k2eAgFnSc1d1	přejatá
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
z	z	k7c2	z
perštiny	perština	k1gFnSc2	perština
<g/>
,	,	kIx,	,
urdštiny	urdština	k1gFnSc2	urdština
<g/>
,	,	kIx,	,
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ruštiny	ruština	k1gFnPc1	ruština
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
:	:	kIx,	:
z	z	k7c2	z
arabštiny	arabština	k1gFnSc2	arabština
<g/>
:	:	kIx,	:
ا	ا	k?	ا
Alláh	Alláh	k1gMnSc1	Alláh
Bůh	bůh	k1gMnSc1	bůh
z	z	k7c2	z
perštiny	perština	k1gFnSc2	perština
م	م	k?	م
méz	méz	k?	méz
/	/	kIx~	/
<g/>
persky	persky	k6eAd1	persky
míz	míza	k1gFnPc2	míza
<g/>
/	/	kIx~	/
stůl	stůl	k1gInSc1	stůl
z	z	k7c2	z
urdštiny	urdština	k1gFnSc2	urdština
م	م	k?	م
/	/	kIx~	/
<g/>
م	م	k?	م
<g/>
/	/	kIx~	/
míṭ	míṭ	k?	míṭ
sladkost	sladkost	k1gFnSc4	sladkost
<g/>
,	,	kIx,	,
cukroví	cukroví	k1gNnSc4	cukroví
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
ر	ر	k?	ر
rákeṭ	rákeṭ	k?	rákeṭ
/	/	kIx~	/
<g/>
rocket	rocket	k1gMnSc1	rocket
<g/>
/	/	kIx~	/
raketa	raketa	k1gFnSc1	raketa
<g/>
,	,	kIx,	,
střela	střela	k1gFnSc1	střela
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
ک	ک	k?	ک
kalašnikov	kalašnikov	k1gInSc1	kalašnikov
název	název	k1gInSc1	název
útočné	útočný	k2eAgFnSc2d1	útočná
pušky	puška	k1gFnSc2	puška
Pašto	Pašto	k1gNnSc1	Pašto
je	být	k5eAaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
paštůnů	paštůn	k1gInPc2	paštůn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
usídleni	usídlit	k5eAaPmNgMnP	usídlit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
severozápadě	severozápad	k1gInSc6	severozápad
Pákistánu	Pákistán	k1gInSc6	Pákistán
a	a	k8xC	a
Balúčistánu	Balúčistán	k1gInSc6	Balúčistán
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
paštská	paštský	k2eAgFnSc1d1	paštský
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
psána	psát	k5eAaImNgFnS	psát
upraveným	upravený	k2eAgNnSc7d1	upravené
arabským	arabský	k2eAgNnSc7d1	arabské
písmem	písmo	k1gNnSc7	písmo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
40	[number]	k4	40
písmen	písmeno	k1gNnPc2	písmeno
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
8	[number]	k4	8
je	být	k5eAaImIp3nS	být
paštských	paštský	k2eAgFnPc2d1	paštský
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
jasné	jasný	k2eAgNnSc1d1	jasné
jakým	jaký	k3yQgNnSc7	jaký
písmem	písmo	k1gNnSc7	písmo
paštůni	paštůň	k1gMnPc7	paštůň
psali	psát	k5eAaImAgMnP	psát
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
arabské	arabský	k2eAgFnSc2d1	arabská
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
vykopávek	vykopávka	k1gFnPc2	vykopávka
a	a	k8xC	a
starých	starý	k2eAgFnPc2d1	stará
mincí	mince	k1gFnPc2	mince
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
nalezeny	naleznout	k5eAaPmNgFnP	naleznout
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
kde	kde	k6eAd1	kde
paštůni	paštůň	k1gMnPc7	paštůň
prokazatelně	prokazatelně	k6eAd1	prokazatelně
žili	žít	k5eAaImAgMnP	žít
je	on	k3xPp3gMnPc4	on
patné	patný	k2eAgMnPc4d1	patný
<g/>
,	,	kIx,	,
že	že	k8xS	že
používali	používat	k5eAaImAgMnP	používat
druh	druh	k1gInSc4	druh
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
Kharóšthí	Kharóšthí	k1gFnSc1	Kharóšthí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
paštská	paštský	k2eAgFnSc1d1	paštský
abeceda	abeceda	k1gFnSc1	abeceda
byla	být	k5eAaImAgFnS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
učencem	učenec	k1gMnSc7	učenec
Qazi	Qaz	k1gMnSc3	Qaz
Saifullahem	Saifullah	k1gInSc7	Saifullah
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Khwaja	Khwajus	k1gMnSc2	Khwajus
Hassana	Hassan	k1gMnSc2	Hassan
Maiwandiho	Maiwandi	k1gMnSc2	Maiwandi
prvního	první	k4xOgMnSc4	první
ministra	ministr	k1gMnSc4	ministr
sultána	sultán	k1gMnSc4	sultán
Mahmúda	Mahmúd	k1gMnSc4	Mahmúd
z	z	k7c2	z
Ghazni	Ghazeň	k1gFnSc6	Ghazeň
/	/	kIx~	/
999	[number]	k4	999
<g/>
–	–	k?	–
<g/>
1030	[number]	k4	1030
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
jiní	jiný	k2eAgMnPc1d1	jiný
učenci	učenec	k1gMnPc1	učenec
a	a	k8xC	a
političtí	politický	k2eAgMnPc1d1	politický
vůdci	vůdce	k1gMnPc1	vůdce
abecedu	abeceda	k1gFnSc4	abeceda
upravovali	upravovat	k5eAaImAgMnP	upravovat
až	až	k9	až
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
současné	současný	k2eAgFnPc4d1	současná
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
jazyka	jazyk	k1gInSc2	jazyk
pašto	pašto	k1gNnSc4	pašto
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
samostatného	samostatný	k2eAgInSc2d1	samostatný
afghánského	afghánský	k2eAgInSc2d1	afghánský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1747	[number]	k4	1747
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
darijštiny	darijština	k1gFnSc2	darijština
byl	být	k5eAaImAgInS	být
uznán	uznat	k5eAaPmNgInS	uznat
za	za	k7c4	za
úřední	úřední	k2eAgInSc4d1	úřední
jazyk	jazyk	k1gInSc4	jazyk
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Pašto	Pašto	k1gNnSc1	Pašto
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
upraveným	upravený	k2eAgInSc7d1	upravený
arabským	arabský	k2eAgInSc7d1	arabský
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
perským	perský	k2eAgNnSc7d1	perské
písmem	písmo	k1gNnSc7	písmo
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
arabská	arabský	k2eAgNnPc4d1	arabské
písmena	písmeno	k1gNnPc4	písmeno
ا	ا	k?	ا
ب	ب	k?	ب
ض	ض	k?	ض
ص	ص	k?	ص
ث	ث	k?	ث
ق	ق	k?	ق
ف	ف	k?	ف
غ	غ	k?	غ
ع	ع	k?	ع
ه	ه	k?	ه
خ	خ	k?	خ
ح	ح	k?	ح
ج	ج	k?	ج
د	د	k?	د
ش	ش	k?	ش
س	س	k?	س
ي	ي	k?	ي
ل	ل	k?	ل
ت	ت	k?	ت
ن	ن	k?	ن
م	م	k?	م
ط	ط	k?	ط
ظ	ظ	k?	ظ
ز	ز	k?	ز
و	و	k?	و
ة	ة	k?	ة
ر	ر	k?	ر
ء	ء	k?	ء
ک	ک	k?	ک
perská	perský	k2eAgNnPc4d1	perské
písmena	písmeno	k1gNnPc4	písmeno
گ	گ	k?	گ
چ	چ	k?	چ
ژ	ژ	k?	ژ
<g />
.	.	kIx.	.
</s>
<s>
پ	پ	k?	پ
ګ	ګ	k?	ګ
paštská	paštský	k2eAgNnPc4d1	paštský
písmena	písmeno	k1gNnPc4	písmeno
ښ	ښ	k?	ښ
ځ	ځ	k?	ځ
ۍ	ۍ	k?	ۍ
ې	ې	k?	ې
ړ	ړ	k?	ړ
ږ	ږ	k?	ږ
څ	څ	k?	څ
ډ	ډ	k?	ډ
ټ	ټ	k?	ټ
ڼ	ڼ	k?	ڼ
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
pravidla	pravidlo	k1gNnPc4	pravidlo
uspořádání	uspořádání	k1gNnSc3	uspořádání
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc2	jejich
vztahu	vztah	k1gInSc2	vztah
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Pořadí	pořadí	k1gNnSc1	pořadí
větných	větný	k2eAgMnPc2d1	větný
členů	člen	k1gMnPc2	člen
PODMĚT-	PODMĚT-	k1gFnSc2	PODMĚT-
PŘEDMĚT-	PŘEDMĚT-	k1gMnSc1	PŘEDMĚT-
PŘÍSUDEK	přísudek	k1gInSc1	přísudek
/	/	kIx~	/
SLOVESO	sloveso	k1gNnSc1	sloveso
/	/	kIx~	/
ه	ه	k?	ه
ک	ک	k?	ک
ل	ل	k?	ل
Halak	Halak	k1gInSc1	Halak
ketáb	ketáb	k1gMnSc1	ketáb
lwalí	lwale	k1gFnPc2	lwale
Chlapec	chlapec	k1gMnSc1	chlapec
čte	číst	k5eAaImIp3nS	číst
knihu	kniha	k1gFnSc4	kniha
Shodný	shodný	k2eAgInSc4d1	shodný
i	i	k8xC	i
neshodný	shodný	k2eNgInSc4d1	neshodný
přívlastek	přívlastek	k1gInSc4	přívlastek
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
před	před	k7c7	před
řídícím	řídící	k2eAgInSc7d1	řídící
členem	člen	k1gInSc7	člen
ل	ل	k?	ل
ښ	ښ	k?	ښ
Lój	Lój	k1gFnSc2	Lój
ṣ	ṣ	k?	ṣ
<g/>
̌	̌	k?	̌
<g/>
ár	ár	k?	ár
Velké	velký	k2eAgNnSc1d1	velké
město	město	k1gNnSc4	město
Příslovečné	příslovečný	k2eAgNnSc4d1	příslovečné
určení	určení	k1gNnSc4	určení
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
času	čas	k1gInSc2	čas
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgNnSc4d1	různé
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
podle	podle	k7c2	podle
důrazu	důraz	k1gInSc2	důraz
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
přívlastek	přívlastek	k1gInSc1	přívlastek
rozšířen	rozšířen	k2eAgInSc1d1	rozšířen
samostatným	samostatný	k2eAgNnSc7d1	samostatné
přivlastňovacím	přivlastňovací	k2eAgNnSc7d1	přivlastňovací
zájmenem	zájmeno	k1gNnSc7	zájmeno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
ز	ز	k?	ز
د	د	k?	د
ب	ب	k?	ب
ک	ک	k?	ک
Zmá	Zmá	k1gFnSc2	Zmá
də	də	k?	də
plór	plór	k1gInSc1	plór
kór	kór	k1gInSc1	kór
Dům	dům	k1gInSc1	dům
mého	můj	k1gMnSc2	můj
otce	otec	k1gMnSc2	otec
Tázací	tázací	k2eAgFnPc1d1	tázací
věty	věta	k1gFnPc1	věta
vyslovujeme	vyslovovat	k5eAaImIp1nP	vyslovovat
s	s	k7c7	s
intonací	intonace	k1gFnSc7	intonace
s	s	k7c7	s
otazníkem	otazník	k1gInSc7	otazník
na	na	k7c6	na
konci	konec	k1gInSc6	konec
věty	věta	k1gFnSc2	věta
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
věty	věta	k1gFnPc1	věta
uvedeny	uveden	k2eAgFnPc1d1	uvedena
částicí	částice	k1gFnSc7	částice
ájá	ájá	k?	ájá
آ	آ	k?	آ
د	د	k?	د
ا	ا	k?	ا
د	د	k?	د
ájá	ájá	k?	ájá
dá	dát	k5eAaPmIp3nS	dát
ins	ins	k?	ins
<g/>
̤	̤	k?	̤
<g/>
áf	áf	k?	áf
daj	daj	k?	daj
<g/>
?	?	kIx.	?
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spravedlivé	spravedlivý	k2eAgNnSc1d1	spravedlivé
<g/>
?	?	kIx.	?
</s>
<s>
Paštšina	Paštšina	k1gFnSc1	Paštšina
nemá	mít	k5eNaImIp3nS	mít
vztažná	vztažný	k2eAgNnPc4d1	vztažné
zájmena	zájmeno	k1gNnPc4	zájmeno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
vztažné	vztažný	k2eAgFnSc2d1	vztažná
věty	věta	k1gFnSc2	věta
využijeme	využít	k5eAaPmIp1nP	využít
částice	částice	k1gFnPc1	částice
če	če	k?	če
/	/	kIx~	/
co	co	k9	co
/	/	kIx~	/
ه	ه	k?	ه
ه	ه	k?	ه
چ	چ	k?	چ
ن	ن	k?	ن
ی	ی	k?	ی
ا	ا	k?	ا
د	د	k?	د
ز	ز	k?	ز
م	م	k?	م
د	د	k?	د
Hagha	Hagha	k1gFnSc1	Hagha
halak	halak	k1gInSc1	halak
čə	čə	k?	čə
nám	my	k3xPp1nPc3	my
jí	on	k3xPp3gFnSc3	on
Ahmad	Ahmad	k1gInSc1	Ahmad
dí	dít	k5eAaBmIp3nS	dít
zmá	zmá	k?	zmá
malgarí	malgarí	k6eAd1	malgarí
dí	dít	k5eAaBmIp3nS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Ahmad	Ahmad	k1gInSc1	Ahmad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gMnSc1	můj
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Infinitiv	infinitiv	k1gInSc1	infinitiv
nemá	mít	k5eNaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
předmětu	předmět	k1gInSc2	předmět
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
použijeme	použít	k5eAaPmIp1nP	použít
stejnou	stejný	k2eAgFnSc4d1	stejná
částici	částice	k1gFnSc4	částice
če	če	k?	če
<g/>
,	,	kIx,	,
tentokráte	tentokráte	k?	tentokráte
s	s	k7c7	s
významem	význam	k1gInSc7	význam
aby	aby	kYmCp3nS	aby
ز	ز	k?	ز
غ	غ	k?	غ
چ	چ	k?	چ
ک	ک	k?	ک
ت	ت	k?	ت
و	و	k?	و
ش	ش	k?	ش
Ze	z	k7c2	z
ghwáram	ghwáram	k1gInSc4	ghwáram
čə	čə	k?	čə
Kandahár	Kandahár	k1gInSc1	Kandahár
ta	ten	k3xDgNnPc1	ten
wlár	wlár	k1gInSc4	wlár
šam	šama	k1gFnPc2	šama
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
jet	jet	k5eAaImF	jet
do	do	k7c2	do
Kandaháru	Kandahár	k1gInSc2	Kandahár
<g/>
.	.	kIx.	.
přesněji	přesně	k6eAd2	přesně
řečeno	řečen	k2eAgNnSc1d1	řečeno
<g/>
:	:	kIx,	:
Chci	chtít	k5eAaImIp1nS	chtít
abych	aby	kYmCp1nS	aby
jel	jet	k5eAaImAgMnS	jet
do	do	k7c2	do
Kandaháru	Kandahár	k1gInSc2	Kandahár
<g/>
.	.	kIx.	.
</s>
<s>
Sloveso	sloveso	k1gNnSc1	sloveso
v	v	k7c6	v
paštštině	paštština	k1gFnSc6	paštština
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
nedokonavost	nedokonavost	k1gFnSc4	nedokonavost
<g/>
,	,	kIx,	,
dokonavost	dokonavost	k1gFnSc4	dokonavost
<g/>
,	,	kIx,	,
nejistotu	nejistota	k1gFnSc4	nejistota
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
způsobů	způsob	k1gInPc2	způsob
<g/>
:	:	kIx,	:
oznamovací	oznamovací	k2eAgInSc1d1	oznamovací
rozkazovací	rozkazovací	k2eAgInSc1d1	rozkazovací
zápor	zápor	k1gInSc1	zápor
podmiňovací	podmiňovací	k2eAgInSc1d1	podmiňovací
pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
neurčitý	určitý	k2eNgInSc1d1	neurčitý
vid	vid	k1gInSc1	vid
dokonavý	dokonavý	k2eAgInSc1d1	dokonavý
nedokonavý	dokonavý	k2eNgInSc1d1	nedokonavý
rod	rod	k1gInSc1	rod
činný	činný	k2eAgInSc1d1	činný
trpný	trpný	k2eAgInSc1d1	trpný
čas	čas	k1gInSc1	čas
přítomný	přítomný	k2eAgInSc1d1	přítomný
minulý	minulý	k2eAgInSc4d1	minulý
budoucí	budoucí	k2eAgFnSc3d1	budoucí
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
tvarosloví	tvarosloví	k1gNnSc2	tvarosloví
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
sloves	sloveso	k1gNnPc2	sloveso
<g/>
:	:	kIx,	:
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
achístə	achístə	k?	achístə
–	–	k?	–
kupovat-ا	kupovat-ا	k?	kupovat-ا
předložková	předložkový	k2eAgFnSc1d1	předložková
rátlə	rátlə	k?	rátlə
–	–	k?	–
přijít	přijít	k5eAaPmF	přijít
<g/>
/	/	kIx~	/
k	k	k7c3	k
mluvčímu	mluvčí	k1gMnSc3	mluvčí
<g/>
/	/	kIx~	/
<g/>
-	-	kIx~	-
ر	ر	k?	ر
nespojená	spojený	k2eNgNnPc1d1	nespojené
složná	složné	k1gNnPc1	složné
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
první	první	k4xOgInSc4	první
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jm	jm	k?	jm
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jm	jm	k?	jm
<g/>
.	.	kIx.	.
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
sloveso	sloveso	k1gNnSc1	sloveso
přechodné	přechodný	k2eAgFnSc2d1	přechodná
nebo	nebo	k8xC	nebo
nepřechodné	přechodný	k2eNgFnSc2d1	nepřechodná
<g/>
/	/	kIx~	/
kár	kára	k1gFnPc2	kára
kə	kə	k?	kə
–	–	k?	–
pracovat-	pracovat-	k?	pracovat-
ک	ک	k?	ک
ک	ک	k?	ک
zachmí	zachmit	k5eAaPmIp3nS	zachmit
kédə	kédə	k?	kédə
být	být	k5eAaImF	být
zraněn	zraněn	k2eAgInSc4d1	zraněn
-	-	kIx~	-
ز	ز	k?	ز
ک	ک	k?	ک
spojená	spojený	k2eAgNnPc1d1	spojené
slovesa	sloveso	k1gNnPc1	sloveso
/	/	kIx~	/
jmenná	jmenný	k2eAgFnSc1d1	jmenná
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
slovesem	sloveso	k1gNnSc7	sloveso
kewelک	kewelک	k?	kewelک
/	/	kIx~	/
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
kédal	kédal	k1gInSc1	kédal
/	/	kIx~	/
<g/>
ک	ک	k?	ک
<g/>
/	/	kIx~	/
s	s	k7c7	s
vypuštěním	vypuštění	k1gNnSc7	vypuštění
počátečního	počáteční	k2eAgInSc2d1	počáteční
k	k	k7c3	k
<g/>
/	/	kIx~	/
póhédə	póhédə	k?	póhédə
–	–	k?	–
rozumět-پ	rozumět-پ	k?	rozumět-پ
póhawə	póhawə	k?	póhawə
–	–	k?	–
informovat	informovat	k5eAaBmF	informovat
-	-	kIx~	-
پ	پ	k?	پ
Infinitiv	infinitiv	k1gInSc1	infinitiv
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
přízvučnou	přízvučný	k2eAgFnSc7d1	přízvučná
koncovkou	koncovka	k1gFnSc7	koncovka
–	–	k?	–
<g/>
ə	ə	k?	ə
/	/	kIx~	/
achístə	achístə	k?	achístə
–	–	k?	–
brát	brát	k5eAaImF	brát
<g/>
,	,	kIx,	,
koupit	koupit	k5eAaPmF	koupit
<g/>
/	/	kIx~	/
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
funkci	funkce	k1gFnSc4	funkce
podstatného	podstatný	k2eAgNnSc2d1	podstatné
jména	jméno	k1gNnSc2	jméno
slovesného	slovesný	k2eAgNnSc2d1	slovesné
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kmeni	kmen	k1gInSc3	kmen
přítomného	přítomný	k2eAgInSc2d1	přítomný
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
po	po	k7c6	po
odtržení	odtržení	k1gNnSc6	odtržení
koncovky	koncovka	k1gFnSc2	koncovka
<g/>
–	–	k?	–
ə	ə	k?	ə
/	/	kIx~	/
ل	ل	k?	ل
/	/	kIx~	/
připojí	připojit	k5eAaPmIp3nS	připojit
následující	následující	k2eAgFnPc4d1	následující
koncovky	koncovka	k1gFnPc4	koncovka
<g/>
:	:	kIx,	:
V	v	k7c6	v
paštštině	paštština	k1gFnSc6	paštština
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
a	a	k8xC	a
složené	složený	k2eAgFnPc1d1	složená
formy	forma	k1gFnPc1	forma
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
složená	složený	k2eAgFnSc1d1	složená
perfektum	perfektum	k1gNnSc1	perfektum
plusquamperfektum	plusquamperfektum	k1gNnSc4	plusquamperfektum
V	v	k7c6	v
případě	případ	k1gInSc6	případ
minulého	minulý	k2eAgInSc2d1	minulý
času	čas	k1gInSc2	čas
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
klást	klást	k5eAaImF	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
sloveso	sloveso	k1gNnSc4	sloveso
transitivní	transitivní	k2eAgFnSc2d1	transitivní
/	/	kIx~	/
přechodné	přechodný	k2eAgFnSc2d1	přechodná
<g/>
/	/	kIx~	/
nebo	nebo	k8xC	nebo
intransitivní	intransitivní	k2eAgFnPc1d1	intransitivní
/	/	kIx~	/
nepřechodné	přechodný	k2eNgFnPc1d1	nepřechodná
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
slovesa	sloveso	k1gNnPc1	sloveso
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
pašto	pašto	k1gNnSc4	pašto
nepřechodná	přechodný	k2eNgFnSc1d1	nepřechodná
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
přechodná	přechodný	k2eAgFnSc1d1	přechodná
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Věty	věta	k1gFnPc1	věta
s	s	k7c7	s
nepřechodnými	přechodný	k2eNgNnPc7d1	nepřechodné
slovesy	sloveso	k1gNnPc7	sloveso
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
jsem	být	k5eAaImIp1nS	být
šel	jít	k5eAaImAgMnS	jít
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
špatné	špatný	k2eAgNnSc1d1	špatné
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
د	د	k?	د
پ	پ	k?	پ
و	و	k?	و
م	م	k?	م
ت	ت	k?	ت
و	و	k?	و
م	م	k?	م
ه	ه	k?	ه
خ	خ	k?	خ
و	و	k?	و
Věty	věta	k1gFnPc4	věta
s	s	k7c7	s
přechodnými	přechodný	k2eAgNnPc7d1	přechodné
slovesy	sloveso	k1gNnPc7	sloveso
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
ergativní	ergativní	k2eAgFnSc6d1	ergativní
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Podmět	podmět	k1gInSc1	podmět
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
nepřímého	přímý	k2eNgInSc2d1	nepřímý
pádu	pád	k1gInSc2	pád
a	a	k8xC	a
sloveso	sloveso	k1gNnSc1	sloveso
se	se	k3xPyFc4	se
podřizuje	podřizovat	k5eAaImIp3nS	podřizovat
předmětu	předmět	k1gInSc2	předmět
ve	v	k7c6	v
větě	věta	k1gFnSc6	věta
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
takovou	takový	k3xDgFnSc4	takový
větu	věta	k1gFnSc4	věta
přeložíme	přeložit	k5eAaPmIp1nP	přeložit
doslova	doslova	k6eAd1	doslova
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
bude	být	k5eAaImBp3nS	být
vypadat	vypadat	k5eAaPmF	vypadat
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pasivní	pasivní	k2eAgFnSc4d1	pasivní
větu	věta	k1gFnSc4	věta
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
o	o	k7c4	o
pasivum	pasivum	k1gNnSc4	pasivum
nejde	jít	k5eNaImIp3nS	jít
<g/>
.	.	kIx.	.
</s>
<s>
Ergativní	Ergativní	k2eAgFnSc1d1	Ergativní
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
v	v	k7c6	v
urdštině	urdština	k1gFnSc6	urdština
a	a	k8xC	a
hindštině	hindština	k1gFnSc6	hindština
<g/>
.	.	kIx.	.
příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
پ	پ	k?	پ
پ	پ	k?	پ
ک	ک	k?	ک
ک	ک	k?	ک
د	د	k?	د
ن	ن	k?	ن
د	د	k?	د
Policie	policie	k1gFnSc1	policie
v	v	k7c6	v
Kandaháru	Kandahár	k1gInSc6	Kandahár
zatkla	zatknout	k5eAaPmAgFnS	zatknout
teroristy	terorista	k1gMnPc4	terorista
<g/>
.	.	kIx.	.
</s>
<s>
Podmět	podmět	k1gInSc1	podmět
<g/>
/	/	kIx~	/
policie	policie	k1gFnSc1	policie
<g/>
/	/	kIx~	/
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nepřímém	přímý	k2eNgInSc6d1	nepřímý
pádě	pád	k1gInSc6	pád
a	a	k8xC	a
sloveso	sloveso	k1gNnSc1	sloveso
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
předmětem	předmět	k1gInSc7	předmět
/	/	kIx~	/
teroristé	terorista	k1gMnPc1	terorista
<g/>
/	/	kIx~	/
Budoucí	budoucí	k2eAgInSc4d1	budoucí
čas	čas	k1gInSc4	čas
tvoříme	tvořit	k5eAaImIp1nP	tvořit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
přítomný	přítomný	k2eAgMnSc1d1	přítomný
pouze	pouze	k6eAd1	pouze
před	před	k7c4	před
sloveso	sloveso	k1gNnSc4	sloveso
dáme	dát	k5eAaPmIp1nP	dát
předložku	předložka	k1gFnSc4	předložka
be-	be-	k?	be-
Nedokonavý	dokonavý	k2eNgInSc4d1	nedokonavý
tvar	tvar	k1gInSc4	tvar
ز	ز	k?	ز
ب	ب	k?	ب
ل	ل	k?	ل
Zə	Zə	k1gFnSc2	Zə
bə	bə	k?	bə
líkam	líkam	k1gInSc1	líkam
Budu	být	k5eAaImBp1nS	být
psát	psát	k5eAaImF	psát
Dokonavý	dokonavý	k2eAgInSc4d1	dokonavý
tvar	tvar	k1gInSc4	tvar
ز	ز	k?	ز
ب	ب	k?	ب
و	و	k?	و
Zə	Zə	k1gMnSc1	Zə
bə	bə	k?	bə
wulíkam	wulíkam	k6eAd1	wulíkam
Napíši	napsat	k5eAaBmIp1nS	napsat
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
literárních	literární	k2eAgFnPc2d1	literární
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaBmNgFnS	napsat
jazykem	jazyk	k1gInSc7	jazyk
paštó	paštó	k?	paštó
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
Životy	život	k1gInPc4	život
světců	světec	k1gMnPc2	světec
od	od	k7c2	od
Sulejmána	Sulejmán	k1gMnSc2	Sulejmán
Makúa	Makúus	k1gMnSc2	Makúus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
století	století	k1gNnSc2	století
napsal	napsat	k5eAaBmAgMnS	napsat
Šajch	šajch	k1gMnSc1	šajch
Mali	Mali	k1gNnSc7	Mali
kroniku	kronika	k1gFnSc4	kronika
o	o	k7c6	o
bojích	boj	k1gInPc6	boj
Júsufzajů	Júsufzaj	k1gMnPc2	Júsufzaj
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
Indu	Indus	k1gInSc2	Indus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
paštúnským	paštúnský	k2eAgMnSc7d1	paštúnský
básníkem	básník	k1gMnSc7	básník
byl	být	k5eAaImAgMnS	být
Chušhál	Chušhál	k1gMnSc1	Chušhál
Chán	chán	k1gMnSc1	chán
Chattak	Chattak	k1gMnSc1	Chattak
(	(	kIx(	(
<g/>
1613	[number]	k4	1613
<g/>
–	–	k?	–
<g/>
1689	[number]	k4	1689
<g/>
)	)	kIx)	)
Chušhál	Chušhál	k1gMnSc1	Chušhál
byl	být	k5eAaImAgMnS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
básníků	básník	k1gMnPc2	básník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
své	svůj	k3xOyFgFnPc4	svůj
básně	báseň	k1gFnPc4	báseň
psali	psát	k5eAaImAgMnP	psát
kromě	kromě	k7c2	kromě
perštiny	perština	k1gFnSc2	perština
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
pašto	pašto	k1gNnSc1	pašto
<g/>
.	.	kIx.	.
</s>
<s>
Zanechal	zanechat	k5eAaPmAgMnS	zanechat
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
množství	množství	k1gNnSc4	množství
veršů	verš	k1gInPc2	verš
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ghazelů	ghazel	k1gInPc2	ghazel
a	a	k8xC	a
landejů	landej	k1gInPc2	landej
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
probouzely	probouzet	k5eAaImAgInP	probouzet
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
vlastenectví	vlastenectví	k1gNnPc2	vlastenectví
a	a	k8xC	a
povzbuzovaly	povzbuzovat	k5eAaImAgInP	povzbuzovat
je	on	k3xPp3gFnPc4	on
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Mughalům	Mughal	k1gMnPc3	Mughal
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
sokolu	sokol	k1gMnSc6	sokol
(	(	kIx(	(
<g/>
ب	ب	k?	ب
<g/>
)	)	kIx)	)
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
turbanu	turban	k1gInSc6	turban
(	(	kIx(	(
<g/>
د	د	k?	د
<g/>
)	)	kIx)	)
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c4	o
lékařství	lékařství	k1gNnSc4	lékařství
(	(	kIx(	(
<g/>
ط	ط	k?	ط
<g/>
)	)	kIx)	)
Ghazel	Ghazel	k1gMnSc1	Ghazel
(	(	kIx(	(
<g/>
غ	غ	k?	غ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
lyrická	lyrický	k2eAgFnSc1d1	lyrická
báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
5-15	[number]	k4	5-15
dvojverších	dvojverší	k1gNnPc6	dvojverší
s	s	k7c7	s
jednotným	jednotný	k2eAgNnSc7d1	jednotné
metrem	metro	k1gNnSc7	metro
a	a	k8xC	a
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
identickým	identický	k2eAgInSc7d1	identický
rýmem	rým	k1gInSc7	rým
<g/>
.	.	kIx.	.
</s>
<s>
Tematika	tematika	k1gFnSc1	tematika
bývá	bývat	k5eAaImIp3nS	bývat
milostná	milostný	k2eAgFnSc1d1	milostná
<g/>
,	,	kIx,	,
mystická	mystický	k2eAgFnSc1d1	mystická
nebo	nebo	k8xC	nebo
filosoficky	filosoficky	k6eAd1	filosoficky
reflexivní	reflexivní	k2eAgFnSc1d1	reflexivní
<g/>
.	.	kIx.	.
</s>
<s>
Landə	Landə	k?	Landə
(	(	kIx(	(
<g/>
ل	ل	k?	ل
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
paštské	paštský	k2eAgFnSc2d1	paštský
lidové	lidový	k2eAgFnSc2d1	lidová
poezie	poezie	k1gFnSc2	poezie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
veršů	verš	k1gInPc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
verš	verš	k1gInSc1	verš
má	mít	k5eAaImIp3nS	mít
devět	devět	k4xCc4	devět
slabik	slabika	k1gFnPc2	slabika
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
třináct	třináct	k4xCc4	třináct
slabik	slabika	k1gFnPc2	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Verše	verš	k1gInPc1	verš
se	se	k3xPyFc4	se
nerýmují	rýmovat	k5eNaImIp3nP	rýmovat
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
verš	verš	k1gInSc1	verš
končí	končit	k5eAaImIp3nS	končit
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
–	–	k?	–
ma	ma	k?	ma
nebo	nebo	k8xC	nebo
–	–	k?	–
na	na	k7c4	na
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
verš	verš	k1gInSc1	verš
na	na	k7c4	na
tyto	tento	k3xDgFnPc4	tento
slabiky	slabika	k1gFnPc4	slabika
nekončí	končit	k5eNaImIp3nS	končit
<g/>
,	,	kIx,	,
slabika	slabika	k1gFnSc1	slabika
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
jednoduše	jednoduše	k6eAd1	jednoduše
připojí	připojit	k5eAaPmIp3nP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
landejů	landej	k1gInPc2	landej
bývá	bývat	k5eAaImIp3nS	bývat
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
vlastenectví	vlastenectví	k1gNnSc1	vlastenectví
<g/>
.	.	kIx.	.
ګ	ګ	k?	ګ
د	د	k?	د
خ	خ	k?	خ
پ	پ	k?	پ
ک	ک	k?	ک
ز	ز	k?	ز
ل	ل	k?	ل
ت	ت	k?	ت
ګ	ګ	k?	ګ
ن	ن	k?	ن
خ	خ	k?	خ
ت	ت	k?	ت
ځ	ځ	k?	ځ
Růže	růž	k1gFnSc2	růž
vzcházejí	vzcházet	k5eAaImIp3nP	vzcházet
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Má	mít	k5eAaImIp3nS	mít
milá	milá	k1gFnSc1	milá
něžnější	něžný	k2eAgFnSc1d2	něžnější
než	než	k8xS	než
růže	růže	k1gFnSc1	růže
vchází	vcházet	k5eAaImIp3nS	vcházet
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Přísloví	přísloví	k1gNnSc2	přísloví
je	být	k5eAaImIp3nS	být
stručně	stručně	k6eAd1	stručně
stylizované	stylizovaný	k2eAgNnSc4d1	stylizované
vyjádření	vyjádření	k1gNnSc4	vyjádření
všeobecně	všeobecně	k6eAd1	všeobecně
přijaté	přijatý	k2eAgFnSc2d1	přijatá
zkušenosti	zkušenost	k1gFnSc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
metaforická	metaforický	k2eAgFnSc1d1	metaforická
gnómická	gnómický	k2eAgFnSc1d1	gnómická
(	(	kIx(	(
<g/>
která	který	k3yQgNnPc1	který
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
mravní	mravní	k2eAgNnPc1d1	mravní
ponaučení	ponaučení	k1gNnPc1	ponaučení
<g/>
)	)	kIx)	)
Příklady	příklad	k1gInPc1	příklad
<g/>
:	:	kIx,	:
چ	چ	k?	چ
د	د	k?	د
ب	ب	k?	ب
ک	ک	k?	ک
خ	خ	k?	خ
ک	ک	k?	ک
ي	ي	k?	ي
خ	خ	k?	خ
ش	ش	k?	ش
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
zničí	zničit	k5eAaPmIp3nS	zničit
dům	dům	k1gInSc4	dům
jinému	jiný	k2eAgMnSc3d1	jiný
tak	tak	k9	tak
jeho	jeho	k3xOp3gInSc1	jeho
dům	dům	k1gInSc1	dům
bude	být	k5eAaImBp3nS	být
zničen	zničit	k5eAaPmNgInS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Čə	Čə	k?	Čə
də	də	k?	də
bal	bal	k1gInSc1	bal
kór	kór	k1gInSc1	kór
charábawí	charábawit	k5eAaImIp3nS	charábawit
kór	kór	k1gInSc4	kór
jé	jé	k0	jé
charáb	charába	k1gFnPc2	charába
ší	ší	k?	ší
ک	ک	k?	ک
د	د	k?	د
د	د	k?	د
Knihy	kniha	k1gFnSc2	kniha
jsou	být	k5eAaImIp3nP	být
oceány	oceán	k1gInPc1	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Kitábúna	Kitábúen	k2eAgFnSc1d1	Kitábúen
darjábúna	darjábúna	k1gFnSc1	darjábúna
dí	dít	k5eAaBmIp3nS	dít
د	د	k?	د
و	و	k?	و
ن	ن	k?	ن
چ	چ	k?	چ
ب	ب	k?	ب
و	و	k?	و
ن	ن	k?	ن
د	د	k?	د
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
by	by	kYmCp3nS	by
nevyvrátil	vyvrátit	k5eNaPmAgInS	vyvrátit
vítr	vítr	k1gInSc1	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Dásé	Dásé	k6eAd1	Dásé
wana	wan	k2eAgFnSc1d1	wana
našta	našta	k1gFnSc1	našta
čə	čə	k?	čə
bád	bád	k?	bád
wahelaj	wahelaj	k1gMnSc1	wahelaj
na	na	k7c6	na
da	da	k?	da
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
پ	پ	k?	پ
ژ	ژ	k?	ژ
na	na	k7c4	na
paštunské	paštunské	k2eAgFnSc4d1	paštunské
Wikipedii	Wikipedie	k1gFnSc4	Wikipedie
a	a	k8xC	a
Pashto	Pashto	k1gNnSc4	Pashto
language	language	k1gFnSc2	language
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
