<s>
Koření	kořenit	k5eAaImIp3nS	kořenit
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vylepšení	vylepšení	k1gNnSc3	vylepšení
chutě	chuť	k1gFnSc2	chuť
a	a	k8xC	a
vůně	vůně	k1gFnSc2	vůně
pokrmů	pokrm	k1gInPc2	pokrm
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
sloužilo	sloužit	k5eAaImAgNnS	sloužit
k	k	k7c3	k
překrytí	překrytí	k1gNnSc3	překrytí
chutě	chuť	k1gFnSc2	chuť
a	a	k8xC	a
zápachu	zápach	k1gInSc2	zápach
zkažených	zkažený	k2eAgFnPc2d1	zkažená
potravin	potravina	k1gFnPc2	potravina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
konzervaci	konzervace	k1gFnSc3	konzervace
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
ze	z	k7c2	z
sušených	sušený	k2eAgFnPc2d1	sušená
nebo	nebo	k8xC	nebo
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
částí	část	k1gFnPc2	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
potravinářského	potravinářský	k2eAgNnSc2d1	potravinářské
hlediska	hledisko	k1gNnSc2	hledisko
koření	koření	k1gNnSc2	koření
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c4	mezi
pochutiny	pochutina	k1gFnPc4	pochutina
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
poživatiny	poživatina	k1gFnPc1	poživatina
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nemají	mít	k5eNaImIp3nP	mít
syticí	syticí	k2eAgFnSc4d1	syticí
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
ochucující	ochucující	k2eAgFnSc1d1	ochucující
<g/>
.	.	kIx.	.
</s>
<s>
Koření	koření	k1gNnSc1	koření
představuje	představovat	k5eAaImIp3nS	představovat
velmi	velmi	k6eAd1	velmi
rozmanitou	rozmanitý	k2eAgFnSc4d1	rozmanitá
skupinu	skupina	k1gFnSc4	skupina
přírodních	přírodní	k2eAgFnPc2d1	přírodní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
výraznou	výrazný	k2eAgFnSc4d1	výrazná
a	a	k8xC	a
příjemnou	příjemný	k2eAgFnSc4d1	příjemná
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
odpornou	odporný	k2eAgFnSc7d1	odporná
<g/>
)	)	kIx)	)
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
vůní	vůně	k1gFnSc7	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
mnohé	mnohý	k2eAgFnPc4d1	mnohá
kořeninové	kořeninový	k2eAgFnPc4d1	kořeninová
rostliny	rostlina	k1gFnPc4	rostlina
i	i	k8xC	i
léčivými	léčivý	k2eAgFnPc7d1	léčivá
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Kořenícími	kořenící	k2eAgFnPc7d1	kořenící
látkami	látka	k1gFnPc7	látka
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
různé	různý	k2eAgFnPc1d1	různá
silice	silice	k1gFnPc1	silice
<g/>
,	,	kIx,	,
hořčiny	hořčina	k1gFnPc1	hořčina
a	a	k8xC	a
třísloviny	tříslovina	k1gFnPc1	tříslovina
<g/>
,	,	kIx,	,
etérické	etérický	k2eAgInPc1d1	etérický
oleje	olej	k1gInPc1	olej
a	a	k8xC	a
česnekové	česnekový	k2eAgFnPc1d1	česneková
silice	silice	k1gFnPc1	silice
<g/>
.	.	kIx.	.
</s>
<s>
Koření	koření	k1gNnSc1	koření
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
jen	jen	k9	jen
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
směsi	směs	k1gFnPc1	směs
koření	kořenit	k5eAaImIp3nP	kořenit
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	on	k3xPp3gInPc4	on
používáme	používat	k5eAaImIp1nP	používat
pro	pro	k7c4	pro
dochucení	dochucení	k1gNnSc4	dochucení
jídel	jídlo	k1gNnPc2	jídlo
např	např	kA	např
<g/>
:	:	kIx,	:
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
guláše	guláš	k1gInPc1	guláš
atd.	atd.	kA	atd.
Kořenících	kořenící	k2eAgFnPc2d1	kořenící
směsí	směs	k1gFnPc2	směs
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
příklady	příklad	k1gInPc1	příklad
-	-	kIx~	-
Kari	kari	k1gNnSc1	kari
<g/>
,	,	kIx,	,
Směs	směs	k1gFnSc1	směs
pěti	pět	k4xCc2	pět
koření	koření	k1gNnPc2	koření
<g/>
,	,	kIx,	,
Kečup	kečup	k1gInSc1	kečup
<g/>
,	,	kIx,	,
Worcestrová	Worcestrový	k2eAgFnSc1d1	Worcestrový
omáčka	omáčka	k1gFnSc1	omáčka
<g/>
,	,	kIx,	,
Tabasco	Tabasco	k1gNnSc4	Tabasco
<g/>
,	,	kIx,	,
Chutney	Chutnea	k1gFnPc4	Chutnea
<g/>
,	,	kIx,	,
Bouquet	bouquet	k1gNnSc4	bouquet
garni	garen	k2eAgMnPc1d1	garen
<g/>
,	,	kIx,	,
Fines	finesa	k1gFnPc2	finesa
herbes	herbesa	k1gFnPc2	herbesa
<g/>
,	,	kIx,	,
Barbecue	Barbecue	k1gFnSc1	Barbecue
<g/>
,	,	kIx,	,
Baharat	Baharat	k1gInSc1	Baharat
<g/>
,	,	kIx,	,
Adžika	Adžika	k1gFnSc1	Adžika
<g/>
,	,	kIx,	,
Grilovací	grilovací	k2eAgNnSc1d1	grilovací
koření	koření	k1gNnSc1	koření
<g/>
,	,	kIx,	,
Harissa	Harissa	k1gFnSc1	Harissa
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
listy	list	k1gInPc1	list
některých	některý	k3yIgFnPc2	některý
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
užívat	užívat	k5eAaImF	užívat
k	k	k7c3	k
dochucování	dochucování	k1gNnSc3	dochucování
masa	maso	k1gNnSc2	maso
už	už	k6eAd1	už
před	před	k7c7	před
50.000	[number]	k4	50.000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
<g/>
,	,	kIx,	,
dokládající	dokládající	k2eAgNnSc1d1	dokládající
užití	užití	k1gNnSc1	užití
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
však	však	k9	však
pocházejí	pocházet	k5eAaImIp3nP	pocházet
až	až	k9	až
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
kořenilo	kořenit	k5eAaImAgNnS	kořenit
kmínem	kmín	k1gInSc7	kmín
a	a	k8xC	a
mákem	mák	k1gInSc7	mák
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
písemné	písemný	k2eAgFnPc1d1	písemná
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
koření	koření	k1gNnSc2	koření
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc1	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
staroegyptské	staroegyptský	k2eAgInPc1d1	staroegyptský
recepty	recept	k1gInPc1	recept
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
různé	různý	k2eAgNnSc1d1	různé
koření	koření	k1gNnSc1	koření
-	-	kIx~	-
anýz	anýz	k1gInSc1	anýz
<g/>
,	,	kIx,	,
hořčici	hořčice	k1gFnSc4	hořčice
<g/>
,	,	kIx,	,
kmín	kmín	k1gInSc4	kmín
<g/>
,	,	kIx,	,
koriandr	koriandr	k1gInSc4	koriandr
<g/>
,	,	kIx,	,
mátu	máta	k1gFnSc4	máta
<g/>
,	,	kIx,	,
pelyněk	pelyněk	k1gInSc4	pelyněk
<g/>
,	,	kIx,	,
skořici	skořice	k1gFnSc4	skořice
nebo	nebo	k8xC	nebo
šafrán	šafrán	k1gInSc4	šafrán
<g/>
.	.	kIx.	.
</s>
<s>
Sumerové	Sumer	k1gMnPc1	Sumer
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
fenykl	fenykl	k1gInSc1	fenykl
<g/>
,	,	kIx,	,
kmín	kmín	k1gInSc1	kmín
<g/>
,	,	kIx,	,
koriandr	koriandr	k1gInSc1	koriandr
<g/>
,	,	kIx,	,
šafrán	šafrán	k1gInSc1	šafrán
a	a	k8xC	a
tymián	tymián	k1gInSc1	tymián
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
staří	starý	k2eAgMnPc1d1	starý
Indové	Ind	k1gMnPc1	Ind
používali	používat	k5eAaImAgMnP	používat
kardamon	kardamon	k1gInSc4	kardamon
<g/>
,	,	kIx,	,
kurkumu	kurkuma	k1gFnSc4	kurkuma
<g/>
,	,	kIx,	,
hřebíček	hřebíček	k1gInSc4	hřebíček
<g/>
,	,	kIx,	,
muškátový	muškátový	k2eAgInSc4d1	muškátový
květ	květ	k1gInSc4	květ
<g/>
,	,	kIx,	,
pepř	pepř	k1gInSc4	pepř
a	a	k8xC	a
skořici	skořice	k1gFnSc4	skořice
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
zpráv	zpráva	k1gFnPc2	zpráva
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Jakýsi	jakýsi	k3yIgInSc1	jakýsi
zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
koření	koření	k1gNnSc2	koření
nastává	nastávat	k5eAaImIp3nS	nastávat
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
křížových	křížový	k2eAgFnPc2d1	křížová
výprav	výprava	k1gFnPc2	výprava
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
kořením	koření	k1gNnSc7	koření
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1298	[number]	k4	1298
popsal	popsat	k5eAaPmAgMnS	popsat
Marco	Marco	k1gMnSc1	Marco
Polo	polo	k6eAd1	polo
indický	indický	k2eAgInSc4d1	indický
pepřovník	pepřovník	k1gInSc4	pepřovník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
podnítila	podnítit	k5eAaPmAgFnS	podnítit
objevné	objevný	k2eAgFnPc4d1	objevná
cesty	cesta	k1gFnPc4	cesta
slavných	slavný	k2eAgMnPc2d1	slavný
mořeplavců	mořeplavec	k1gMnPc2	mořeplavec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
měly	mít	k5eAaImAgInP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
především	především	k6eAd1	především
zjednodušit	zjednodušit	k5eAaPmF	zjednodušit
a	a	k8xC	a
zlevnit	zlevnit	k5eAaPmF	zlevnit
dovoz	dovoz	k1gInSc4	dovoz
koření	koření	k1gNnSc2	koření
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
především	především	k9	především
cesta	cesta	k1gFnSc1	cesta
Kryštofa	Kryštof	k1gMnSc2	Kryštof
Kolumba	Kolumbus	k1gMnSc2	Kolumbus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
doplout	doplout	k5eAaPmF	doplout
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
objevil	objevit	k5eAaPmAgInS	objevit
nový	nový	k2eAgInSc1d1	nový
světadíl	světadíl	k1gInSc1	světadíl
a	a	k8xC	a
cesta	cesta	k1gFnSc1	cesta
Vasco	Vasco	k1gMnSc1	Vasco
da	da	k?	da
Gammy	Gamma	k1gFnSc2	Gamma
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
obeplul	obeplout	k5eAaPmAgMnS	obeplout
Afriku	Afrika	k1gFnSc4	Afrika
kolem	kolem	k7c2	kolem
mysu	mys	k1gInSc2	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
a	a	k8xC	a
r.	r.	kA	r.
1498	[number]	k4	1498
přistál	přistát	k5eAaPmAgInS	přistát
na	na	k7c6	na
malabarském	malabarský	k2eAgNnSc6d1	malabarský
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
opakované	opakovaný	k2eAgFnSc6d1	opakovaná
cestě	cesta	k1gFnSc6	cesta
v	v	k7c6	v
r.	r.	kA	r.
1502	[number]	k4	1502
doplul	doplout	k5eAaPmAgInS	doplout
na	na	k7c4	na
Cejlon	Cejlon	k1gInSc4	Cejlon
<g/>
.	.	kIx.	.
</s>
<s>
Fernã	Fernã	k?	Fernã
de	de	k?	de
Magalhã	Magalhã	k1gFnSc2	Magalhã
v	v	k7c6	v
r.	r.	kA	r.
1519	[number]	k4	1519
vyplul	vyplout	k5eAaPmAgInS	vyplout
s	s	k7c7	s
265	[number]	k4	265
námořníky	námořník	k1gMnPc4	námořník
západním	západní	k2eAgInSc7d1	západní
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
objevil	objevit	k5eAaPmAgMnS	objevit
novou	nový	k2eAgFnSc4d1	nová
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
koření	koření	k1gNnSc3	koření
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgMnS	objevit
Filipíny	Filipíny	k1gFnPc4	Filipíny
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
po	po	k7c6	po
3	[number]	k4	3
letech	léto	k1gNnPc6	léto
vrátila	vrátit	k5eAaPmAgFnS	vrátit
jediná	jediný	k2eAgFnSc1d1	jediná
loď	loď	k1gFnSc1	loď
Victoria	Victorium	k1gNnSc2	Victorium
s	s	k7c7	s
posledními	poslední	k2eAgMnPc7d1	poslední
18	[number]	k4	18
muži	muž	k1gMnPc7	muž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
palubě	paluba	k1gFnSc6	paluba
byl	být	k5eAaImAgInS	být
především	především	k9	především
hřebíček	hřebíček	k1gInSc1	hřebíček
<g/>
.	.	kIx.	.
</s>
<s>
Kapitán	kapitán	k1gMnSc1	kapitán
lodi	loď	k1gFnSc2	loď
Juan	Juan	k1gMnSc1	Juan
Sebastian	Sebastian	k1gMnSc1	Sebastian
del	del	k?	del
Cano	Cano	k1gMnSc1	Cano
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
erbu	erb	k1gInSc6	erb
měl	mít	k5eAaImAgMnS	mít
12	[number]	k4	12
hřebíčků	hřebíček	k1gInPc2	hřebíček
<g/>
,	,	kIx,	,
3	[number]	k4	3
muškátové	muškátový	k2eAgInPc1d1	muškátový
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
pruty	prut	k1gInPc1	prut
skořicovníku	skořicovník	k1gInSc2	skořicovník
<g/>
.	.	kIx.	.
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
nepříjemného	příjemný	k2eNgInSc2d1	nepříjemný
nebo	nebo	k8xC	nebo
specifického	specifický	k2eAgInSc2d1	specifický
pachu	pach	k1gInSc2	pach
prvotních	prvotní	k2eAgFnPc2d1	prvotní
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
již	již	k6eAd1	již
hotových	hotový	k2eAgNnPc2d1	hotové
či	či	k8xC	či
starších	starý	k2eAgNnPc2d2	starší
jídel	jídlo	k1gNnPc2	jídlo
k	k	k7c3	k
zesílení	zesílení	k1gNnSc3	zesílení
vnější	vnější	k2eAgFnSc2d1	vnější
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
jídla	jídlo	k1gNnSc2	jídlo
dodáním	dodání	k1gNnSc7	dodání
odpovídajícího	odpovídající	k2eAgNnSc2d1	odpovídající
zabarvení	zabarvení	k1gNnSc2	zabarvení
<g/>
,	,	kIx,	,
vůně	vůně	k1gFnSc1	vůně
k	k	k7c3	k
dodání	dodání	k1gNnSc3	dodání
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
vůně	vůně	k1gFnSc2	vůně
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
vlastní	vlastní	k2eAgFnSc3d1	vlastní
původní	původní	k2eAgFnSc3d1	původní
surovině	surovina	k1gFnSc3	surovina
a	a	k8xC	a
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
určité	určitý	k2eAgNnSc4d1	určité
koření	koření	k1gNnSc4	koření
nebo	nebo	k8xC	nebo
směs	směs	k1gFnSc4	směs
koření	koření	k1gNnSc2	koření
k	k	k7c3	k
dodání	dodání	k1gNnSc3	dodání
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
<g/>
,	,	kIx,	,
osobité	osobitý	k2eAgFnPc1d1	osobitá
chuti	chuť	k1gFnPc1	chuť
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
uchovatelnosti	uchovatelnost	k1gFnSc2	uchovatelnost
produktu	produkt	k1gInSc2	produkt
nebo	nebo	k8xC	nebo
hotových	hotový	k2eAgNnPc2d1	hotové
jídel	jídlo	k1gNnPc2	jídlo
k	k	k7c3	k
zušlechtění	zušlechtění	k1gNnSc3	zušlechtění
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
jeho	jeho	k3xOp3gFnSc2	jeho
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
koření	koření	k1gNnSc1	koření
změkčuje	změkčovat	k5eAaImIp3nS	změkčovat
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
zpevňuje	zpevňovat	k5eAaImIp3nS	zpevňovat
rozměklé	rozměklý	k2eAgNnSc4d1	rozměklé
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
činí	činit	k5eAaImIp3nS	činit
je	být	k5eAaImIp3nS	být
křehkým	křehký	k2eAgMnSc7d1	křehký
a	a	k8xC	a
stravitelnějším	stravitelný	k2eAgMnSc7d2	stravitelnější
<g/>
.	.	kIx.	.
</s>
<s>
Zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
rychlému	rychlý	k2eAgNnSc3d1	rychlé
rozvaření	rozvaření	k1gNnSc3	rozvaření
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
dobu	doba	k1gFnSc4	doba
varu	var	k1gInSc2	var
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
uchovatelnost	uchovatelnost	k1gFnSc4	uchovatelnost
vitamínu	vitamín	k1gInSc2	vitamín
<g/>
)	)	kIx)	)
dráždivé	dráždivý	k2eAgNnSc1d1	dráždivé
koření	koření	k1gNnSc1	koření
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
chuť	chuť	k1gFnSc4	chuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yRgFnSc4	který
nás	my	k3xPp1nPc2	my
připravují	připravovat	k5eAaImIp3nP	připravovat
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
</s>
