<s>
Halit	halit	k1gInSc1	halit
(	(	kIx(	(
<g/>
Glocker	Glocker	k1gInSc1	Glocker
<g/>
,	,	kIx,	,
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
NaCl	NaCla	k1gFnPc2	NaCla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
minerál	minerál	k1gInSc1	minerál
krystalizující	krystalizující	k2eAgInSc1d1	krystalizující
v	v	k7c6	v
krychlové	krychlový	k2eAgFnSc6d1	krychlová
(	(	kIx(	(
<g/>
kubické	kubický	k2eAgFnSc6d1	kubická
<g/>
)	)	kIx)	)
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgInSc1d1	složený
z	z	k7c2	z
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
halos	halos	k1gInSc1	halos
–	–	k?	–
slaný	slaný	k2eAgInSc1d1	slaný
a	a	k8xC	a
lithos	lithos	k1gInSc1	lithos
–	–	k?	–
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
halitu	halit	k1gInSc2	halit
je	být	k5eAaImIp3nS	být
sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
významná	významný	k2eAgNnPc4d1	významné
ložiska	ložisko	k1gNnPc4	ložisko
halitu	halit	k1gInSc2	halit
vznikala	vznikat	k5eAaImAgFnS	vznikat
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
vysrážením	vysrážení	k1gNnSc7	vysrážení
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
zátokách	zátoka	k1gFnPc6	zátoka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
moře	moře	k1gNnSc2	moře
odděleny	oddělen	k2eAgFnPc1d1	oddělena
hrází	hráz	k1gFnSc7	hráz
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgInP	být
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojeny	spojen	k2eAgMnPc4d1	spojen
úzkým	úzký	k2eAgInSc7d1	úzký
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
laguny	laguna	k1gFnPc1	laguna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
postupně	postupně	k6eAd1	postupně
vysychaly	vysychat	k5eAaImAgFnP	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
stoupala	stoupat	k5eAaImAgFnS	stoupat
koncentrace	koncentrace	k1gFnSc1	koncentrace
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
docházelo	docházet	k5eAaImAgNnS	docházet
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vysrážení	vysrážení	k1gNnSc3	vysrážení
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
uhličitany	uhličitan	k1gInPc1	uhličitan
(	(	kIx(	(
<g/>
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
dolomit	dolomit	k1gInSc1	dolomit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
sírany	síran	k1gInPc1	síran
(	(	kIx(	(
<g/>
sádrovec	sádrovec	k1gInSc1	sádrovec
<g/>
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
halogenidy	halogenida	k1gFnSc2	halogenida
(	(	kIx(	(
<g/>
halit	halit	k1gInSc1	halit
<g/>
,	,	kIx,	,
sylvín	sylvín	k1gInSc1	sylvín
<g/>
,	,	kIx,	,
carnallit	carnallit	k1gInSc1	carnallit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vzniká	vznikat	k5eAaImIp3nS	vznikat
vysrážením	vysrážení	k1gNnSc7	vysrážení
v	v	k7c6	v
solných	solný	k2eAgInPc6d1	solný
jezerech	jezero	k1gNnPc6	jezero
nebo	nebo	k8xC	nebo
z	z	k7c2	z
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
krystalizuje	krystalizovat	k5eAaImIp3nS	krystalizovat
ze	z	k7c2	z
sopečných	sopečný	k2eAgInPc2d1	sopečný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
vykvétá	vykvétat	k5eAaImIp3nS	vykvétat
na	na	k7c6	na
půdách	půda	k1gFnPc6	půda
v	v	k7c6	v
aridních	aridní	k2eAgFnPc6d1	aridní
(	(	kIx(	(
<g/>
suchých	suchý	k2eAgFnPc6d1	suchá
<g/>
)	)	kIx)	)
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zrnitých	zrnitý	k2eAgInPc2d1	zrnitý
nebo	nebo	k8xC	nebo
vláknitých	vláknitý	k2eAgInPc2d1	vláknitý
agregátů	agregát	k1gInPc2	agregát
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
krystaly	krystal	k1gInPc1	krystal
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
krychle	krychle	k1gFnSc2	krychle
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
celistvého	celistvý	k2eAgInSc2d1	celistvý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Lze	lze	k6eAd1	lze
rýpat	rýpat	k5eAaImF	rýpat
nehtem	nehet	k1gInSc7	nehet
(	(	kIx(	(
<g/>
má	mít	k5eAaImIp3nS	mít
tvrdost	tvrdost	k1gFnSc1	tvrdost
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
2,1	[number]	k4	2,1
<g/>
–	–	k?	–
<g/>
2,2	[number]	k4	2,2
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
,	,	kIx,	,
dokonale	dokonale	k6eAd1	dokonale
štěpný	štěpný	k2eAgMnSc1d1	štěpný
podle	podle	k7c2	podle
krychle	krychle	k1gFnSc2	krychle
<g/>
,	,	kIx,	,
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
působení	působení	k1gNnSc6	působení
tlaku	tlak	k1gInSc2	tlak
plastický	plastický	k2eAgMnSc1d1	plastický
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Barva	barva	k1gFnSc1	barva
<g/>
:	:	kIx,	:
čirý	čirý	k2eAgInSc1d1	čirý
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
(	(	kIx(	(
<g/>
od	od	k7c2	od
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
bublinek	bublinka	k1gFnPc2	bublinka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
(	(	kIx(	(
<g/>
od	od	k7c2	od
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
hematitu	hematit	k1gInSc2	hematit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šedá	šedat	k5eAaImIp3nS	šedat
(	(	kIx(	(
<g/>
jílové	jílový	k2eAgFnPc1d1	jílová
částice	částice	k1gFnPc1	částice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
od	od	k7c2	od
rozptýleného	rozptýlený	k2eAgInSc2d1	rozptýlený
kovového	kovový	k2eAgInSc2d1	kovový
sodíku	sodík	k1gInSc2	sodík
<g/>
)	)	kIx)	)
aj.	aj.	kA	aj.
Průhledný	průhledný	k2eAgInSc1d1	průhledný
až	až	k9	až
průsvitný	průsvitný	k2eAgInSc1d1	průsvitný
<g/>
,	,	kIx,	,
vryp	vryp	k1gInSc1	vryp
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
lesk	lesk	k1gInSc1	lesk
skelný	skelný	k2eAgInSc1d1	skelný
až	až	k6eAd1	až
mastný	mastný	k2eAgInSc1d1	mastný
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Složení	složení	k1gNnSc1	složení
<g/>
:	:	kIx,	:
Na	na	k7c4	na
39,34	[number]	k4	39,34
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Cl	Cl	k1gFnSc1	Cl
60,66	[number]	k4	60,66
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
příměsi	příměs	k1gFnPc4	příměs
I	I	kA	I
<g/>
,	,	kIx,	,
Br.	Br.	k1gFnSc1	Br.
Rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
solanka	solanka	k1gFnSc1	solanka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
elektricky	elektricky	k6eAd1	elektricky
vodivý	vodivý	k2eAgInSc1d1	vodivý
<g/>
.	.	kIx.	.
</s>
<s>
Hygroskopický	hygroskopický	k2eAgMnSc1d1	hygroskopický
(	(	kIx(	(
<g/>
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plamen	plamen	k1gInSc1	plamen
barví	barvit	k5eAaImIp3nS	barvit
intenzivně	intenzivně	k6eAd1	intenzivně
žlutě	žlutě	k6eAd1	žlutě
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
:	:	kIx,	:
Slaná	slaný	k2eAgFnSc1d1	slaná
chuť	chuť	k1gFnSc1	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
způsoby	způsob	k1gInPc1	způsob
těžby	těžba	k1gFnSc2	těžba
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Hornická	hornický	k2eAgFnSc1d1	hornická
těžba	těžba	k1gFnSc1	těžba
<g/>
:	:	kIx,	:
pomocí	pomocí	k7c2	pomocí
těžkých	těžký	k2eAgInPc2d1	těžký
těžebních	těžební	k2eAgInPc2d1	těžební
mechanizmů	mechanizmus	k1gInPc2	mechanizmus
se	se	k3xPyFc4	se
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
halitu	halit	k1gInSc2	halit
razí	razit	k5eAaImIp3nP	razit
štoly	štola	k1gFnPc1	štola
a	a	k8xC	a
těžební	těžební	k2eAgFnPc1d1	těžební
komory	komora	k1gFnPc1	komora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
výšku	výška	k1gFnSc4	výška
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
m.	m.	k?	m.
Pásovým	pásový	k2eAgInSc7d1	pásový
dopravníkem	dopravník	k1gInSc7	dopravník
nebo	nebo	k8xC	nebo
automobily	automobil	k1gInPc1	automobil
se	se	k3xPyFc4	se
sůl	sůl	k1gFnSc1	sůl
odváží	odvážit	k5eAaPmIp3nS	odvážit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
těžby	těžba	k1gFnSc2	těžba
je	být	k5eAaImIp3nS	být
finančně	finančně	k6eAd1	finančně
nejnákladnější	nákladný	k2eAgNnSc1d3	nejnákladnější
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
činným	činný	k2eAgInSc7d1	činný
solným	solný	k2eAgInSc7d1	solný
dolem	dol	k1gInSc7	dol
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
Kłodawa	Kłodawa	k1gFnSc1	Kłodawa
<g/>
.	.	kIx.	.
</s>
<s>
Louhování	louhování	k1gNnSc1	louhování
<g/>
:	:	kIx,	:
do	do	k7c2	do
podzemního	podzemní	k2eAgNnSc2d1	podzemní
ložiska	ložisko	k1gNnSc2	ložisko
se	se	k3xPyFc4	se
vrtem	vrt	k1gInSc7	vrt
přivede	přivést	k5eAaPmIp3nS	přivést
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Sůl	sůl	k1gFnSc1	sůl
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
rozpustí	rozpustit	k5eAaPmIp3nS	rozpustit
a	a	k8xC	a
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
roztok	roztok	k1gInSc1	roztok
<g/>
,	,	kIx,	,
solanka	solanka	k1gFnSc1	solanka
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
310	[number]	k4	310
g	g	kA	g
soli	sůl	k1gFnSc2	sůl
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
čerpá	čerpat	k5eAaImIp3nS	čerpat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odpaření	odpaření	k1gNnSc6	odpaření
vody	voda	k1gFnSc2	voda
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
krystalizaci	krystalizace	k1gFnSc3	krystalizace
a	a	k8xC	a
následnému	následný	k2eAgNnSc3d1	následné
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Dutiny	dutina	k1gFnPc4	dutina
vzniklé	vzniklý	k2eAgFnPc4d1	vzniklá
těžbou	těžba	k1gFnSc7	těžba
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xC	jako
úložiště	úložiště	k1gNnSc1	úložiště
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpařováním	odpařování	k1gNnSc7	odpařování
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
:	:	kIx,	:
technologie	technologie	k1gFnSc1	technologie
použitelná	použitelný	k2eAgFnSc1d1	použitelná
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mořská	mořský	k2eAgFnSc1d1	mořská
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
přibližně	přibližně	k6eAd1	přibližně
35	[number]	k4	35
g	g	kA	g
halitu	halit	k1gInSc6	halit
<g/>
)	)	kIx)	)
nechává	nechávat	k5eAaImIp3nS	nechávat
odpařit	odpařit	k5eAaPmF	odpařit
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
nádržích	nádrž	k1gFnPc6	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
získaná	získaný	k2eAgFnSc1d1	získaná
surovina	surovina	k1gFnSc1	surovina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
80	[number]	k4	80
<g/>
%	%	kIx~	%
halitu	halit	k1gInSc2	halit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ji	on	k3xPp3gFnSc4	on
chemicky	chemicky	k6eAd1	chemicky
vyčistit	vyčistit	k5eAaPmF	vyčistit
od	od	k7c2	od
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
životně	životně	k6eAd1	životně
důležitý	důležitý	k2eAgInSc4d1	důležitý
minerál	minerál	k1gInSc4	minerál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
potřebný	potřebný	k2eAgInSc1d1	potřebný
pro	pro	k7c4	pro
životní	životní	k2eAgFnPc4d1	životní
funkce	funkce	k1gFnPc4	funkce
většiny	většina	k1gFnSc2	většina
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
člověk	člověk	k1gMnSc1	člověk
zkonzumuje	zkonzumovat	k5eAaPmIp3nS	zkonzumovat
za	za	k7c4	za
rok	rok	k1gInSc4	rok
7,5	[number]	k4	7,5
kg	kg	kA	kg
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
220	[number]	k4	220
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
soli	sůl	k1gFnSc2	sůl
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Sůl	sůl	k1gFnSc1	sůl
kamenná	kamenný	k2eAgFnSc1d1	kamenná
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
a	a	k8xC	a
chemický	chemický	k2eAgInSc4d1	chemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
běžné	běžný	k2eAgFnSc2d1	běžná
úpravy	úprava	k1gFnSc2	úprava
potravin	potravina	k1gFnPc2	potravina
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
konzervaci	konzervace	k1gFnSc6	konzervace
masa	maso	k1gNnSc2	maso
pomocí	pomoc	k1gFnPc2	pomoc
solení	solení	k1gNnSc2	solení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
je	být	k5eAaImIp3nS	být
halit	halit	k1gInSc1	halit
důležitý	důležitý	k2eAgInSc1d1	důležitý
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
sodíku	sodík	k1gInSc2	sodík
<g/>
,	,	kIx,	,
jedlé	jedlý	k2eAgFnSc2d1	jedlá
sody	soda	k1gFnSc2	soda
<g/>
,	,	kIx,	,
chlóru	chlór	k1gInSc2	chlór
<g/>
,	,	kIx,	,
kyseliny	kyselina	k1gFnPc1	kyselina
solné	solný	k2eAgFnPc1d1	solná
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
v	v	k7c6	v
mydlovarnictví	mydlovarnictví	k1gNnSc6	mydlovarnictví
<g/>
,	,	kIx,	,
sklářství	sklářství	k1gNnSc6	sklářství
<g/>
,	,	kIx,	,
metalurgii	metalurgie	k1gFnSc6	metalurgie
a	a	k8xC	a
v	v	k7c6	v
papírenském	papírenský	k2eAgInSc6d1	papírenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
či	či	k8xC	či
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
obrovské	obrovský	k2eAgNnSc1d1	obrovské
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
strategickou	strategický	k2eAgFnSc4d1	strategická
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dostupnou	dostupný	k2eAgFnSc4d1	dostupná
surovinu	surovina	k1gFnSc4	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Slovensko	Slovensko	k1gNnSc1	Slovensko
-	-	kIx~	-
Solivar	solivar	k1gInSc1	solivar
u	u	k7c2	u
Prešova	Prešov	k1gInSc2	Prešov
Polsko	Polsko	k1gNnSc1	Polsko
(	(	kIx(	(
<g/>
Wieliczka	Wieliczka	k1gFnSc1	Wieliczka
<g/>
)	)	kIx)	)
Rakousko	Rakousko	k1gNnSc1	Rakousko
(	(	kIx(	(
<g/>
Alpy	Alpy	k1gFnPc1	Alpy
-	-	kIx~	-
Hallstatt	Hallstatt	k1gInSc1	Hallstatt
u	u	k7c2	u
Salzburgu	Salzburg	k1gInSc2	Salzburg
<g/>
)	)	kIx)	)
Německo	Německo	k1gNnSc1	Německo
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lidé	člověk	k1gMnPc1	člověk
přešli	přejít	k5eAaPmAgMnP	přejít
z	z	k7c2	z
lovu	lov	k1gInSc2	lov
na	na	k7c4	na
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
masitou	masitý	k2eAgFnSc4d1	masitá
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
bohatou	bohatý	k2eAgFnSc4d1	bohatá
na	na	k7c4	na
sůl	sůl	k1gFnSc4	sůl
<g/>
,	,	kIx,	,
nahradili	nahradit	k5eAaPmAgMnP	nahradit
obilím	obilí	k1gNnSc7	obilí
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
bílé	bílý	k2eAgFnPc4d1	bílá
krystalky	krystalka	k1gFnPc4	krystalka
velice	velice	k6eAd1	velice
žádané	žádaný	k2eAgFnPc4d1	žádaná
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
byly	být	k5eAaImAgFnP	být
vždy	vždy	k6eAd1	vždy
závislé	závislý	k2eAgFnPc1d1	závislá
na	na	k7c6	na
dovozu	dovoz	k1gInSc6	dovoz
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dopravovala	dopravovat	k5eAaImAgFnS	dopravovat
po	po	k7c6	po
solných	solný	k2eAgFnPc6d1	solná
stezkách	stezka	k1gFnPc6	stezka
-	-	kIx~	-
do	do	k7c2	do
severních	severní	k2eAgFnPc2d1	severní
Čech	Čechy	k1gFnPc2	Čechy
sůl	sůl	k1gFnSc1	sůl
saská	saský	k2eAgFnSc1d1	saská
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
pak	pak	k6eAd1	pak
alpská	alpský	k2eAgFnSc1d1	alpská
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
dopravovala	dopravovat	k5eAaImAgFnS	dopravovat
po	po	k7c6	po
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
stezce	stezka	k1gFnSc6	stezka
přes	přes	k7c4	přes
Šumavu	Šumava	k1gFnSc4	Šumava
a	a	k8xC	a
na	na	k7c6	na
nejobyčejnějším	obyčejný	k2eAgMnSc6d3	nejobyčejnější
z	z	k7c2	z
minerálů	minerál	k1gInPc2	minerál
zbohatly	zbohatnout	k5eAaPmAgFnP	zbohatnout
zejména	zejména	k9	zejména
města	město	k1gNnSc2	město
Pasov	Pasov	k1gInSc1	Pasov
a	a	k8xC	a
Prachatice	Prachatice	k1gFnPc1	Prachatice
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
něž	jenž	k3xRgInPc4	jenž
solné	solný	k2eAgInPc4d1	solný
karavany	karavan	k1gInPc4	karavan
procházely	procházet	k5eAaImAgFnP	procházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
prošlo	projít	k5eAaPmAgNnS	projít
Prachaticemi	Prachatice	k1gFnPc7	Prachatice
<g/>
,	,	kIx,	,
označovanými	označovaný	k2eAgFnPc7d1	označovaná
za	za	k7c4	za
solný	solný	k2eAgInSc4d1	solný
sklad	sklad	k1gInSc4	sklad
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
až	až	k9	až
220	[number]	k4	220
tun	tuna	k1gFnPc2	tuna
soli	sůl	k1gFnSc2	sůl
týdně	týdně	k6eAd1	týdně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
sůl	sůl	k1gFnSc1	sůl
jako	jako	k8xC	jako
konzervační	konzervační	k2eAgInSc1d1	konzervační
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
obchodnící	obchodnící	k2eAgFnSc1d1	obchodnící
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
komoditou	komodita	k1gFnSc7	komodita
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
zámožní	zámožný	k2eAgMnPc1d1	zámožný
<g/>
.	.	kIx.	.
</s>
<s>
Trh	trh	k1gInSc1	trh
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vysokých	vysoký	k2eAgFnPc2d1	vysoká
daní	daň	k1gFnPc2	daň
ovládali	ovládat	k5eAaImAgMnP	ovládat
panovníci	panovník	k1gMnPc1	panovník
<g/>
,	,	kIx,	,
na	na	k7c6	na
obchodu	obchod	k1gInSc6	obchod
se	s	k7c7	s
solí	sůl	k1gFnSc7	sůl
zbohatly	zbohatnout	k5eAaPmAgFnP	zbohatnout
Benátky	Benátky	k1gFnPc4	Benátky
<g/>
,	,	kIx,	,
Janov	Janov	k1gInSc1	Janov
a	a	k8xC	a
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
minerálů	minerál	k1gInPc2	minerál
Chlorid	chlorid	k1gInSc1	chlorid
sodný	sodný	k2eAgMnSc1d1	sodný
GABRIEL	Gabriel	k1gMnSc1	Gabriel
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Obchod	obchod	k1gInSc1	obchod
se	se	k3xPyFc4	se
solí	solit	k5eAaImIp3nS	solit
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
76	[number]	k4	76
s.	s.	k?	s.
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
halit	halit	k5eAaImF	halit
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Halit	halit	k5eAaImF	halit
na	na	k7c6	na
webu	web	k1gInSc6	web
mindat	mindat	k5eAaPmF	mindat
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Halit	halit	k5eAaImF	halit
na	na	k7c6	na
webu	web	k1gInSc6	web
Webmineral	Webmineral	k1gFnSc2	Webmineral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Halit	halit	k5eAaImF	halit
v	v	k7c6	v
Atlasu	Atlas	k1gInSc6	Atlas
minerálů	minerál	k1gInPc2	minerál
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
o	o	k7c4	o
soli	sůl	k1gFnPc4	sůl
na	na	k7c6	na
firemních	firemní	k2eAgFnPc6d1	firemní
stránkách	stránka	k1gFnPc6	stránka
Solné	solný	k2eAgFnSc2d1	solná
jeskyně	jeskyně	k1gFnSc2	jeskyně
-	-	kIx~	-
katalog	katalog	k1gInSc1	katalog
</s>
