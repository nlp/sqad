<s>
Edikt	edikt	k1gInSc1	edikt
milánský	milánský	k2eAgInSc1d1	milánský
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Milánské	milánský	k2eAgNnSc1d1	Milánské
ujednání	ujednání	k1gNnSc1	ujednání
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Milánský	milánský	k2eAgInSc1d1	milánský
reskript	reskript	k1gInSc1	reskript
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
konvenční	konvenční	k2eAgNnSc1d1	konvenční
označení	označení	k1gNnSc1	označení
dokumentu	dokument	k1gInSc2	dokument
z	z	k7c2	z
roku	rok	k1gInSc2	rok
313	[number]	k4	313
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vydal	vydat	k5eAaPmAgMnS	vydat
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
spoluvladař	spoluvladař	k1gMnSc1	spoluvladař
Licinius	Licinius	k1gMnSc1	Licinius
<g/>
.	.	kIx.	.
</s>
<s>
Správnější	správní	k2eAgInSc1d2	správnější
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
Milánské	milánský	k2eAgNnSc1d1	Milánské
ujednání	ujednání	k1gNnSc1	ujednání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
Milánský	milánský	k2eAgInSc1d1	milánský
reskript	reskript	k1gInSc1	reskript
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgNnSc1d1	moderní
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
dohodu	dohoda	k1gFnSc4	dohoda
mezi	mezi	k7c7	mezi
římskými	římský	k2eAgMnPc7d1	římský
císaři	císař	k1gMnPc7	císař
Konstantinem	Konstantin	k1gMnSc7	Konstantin
I.	I.	kA	I.
<g/>
,	,	kIx,	,
císařem	císař	k1gMnSc7	císař
Západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
a	a	k8xC	a
Luciniem	Lucinium	k1gNnSc7	Lucinium
<g/>
,	,	kIx,	,
císařem	císař	k1gMnSc7	císař
Východu	východ	k1gInSc2	východ
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
313	[number]	k4	313
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
jak	jak	k8xC	jak
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
všichni	všechen	k3xTgMnPc1	všechen
ostatní	ostatní	k2eAgMnPc1d1	ostatní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
svobodné	svobodný	k2eAgFnSc2d1	svobodná
volby	volba	k1gFnSc2	volba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
stoupenci	stoupenec	k1gMnPc7	stoupenec
jakéhokoli	jakýkoli	k3yIgNnSc2	jakýkoli
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
zvolí	zvolit	k5eAaPmIp3nP	zvolit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
ujednání	ujednání	k1gNnSc1	ujednání
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
známé	známý	k2eAgNnSc1d1	známé
hlavně	hlavně	k6eAd1	hlavně
jako	jako	k8xC	jako
Toleranční	toleranční	k2eAgInSc1d1	toleranční
edikt	edikt	k1gInSc1	edikt
milánský	milánský	k2eAgInSc1d1	milánský
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
edikt	edikt	k1gInSc4	edikt
milánský	milánský	k2eAgInSc4d1	milánský
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
správné	správný	k2eAgNnSc1d1	správné
označení	označení	k1gNnSc1	označení
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
(	(	kIx(	(
<g/>
počínaje	počínaje	k7c7	počínaje
již	již	k9	již
od	od	k7c2	od
Otta	Otta	k1gMnSc1	Otta
Seecka	Seecka	k1gFnSc1	Seecka
<g/>
)	)	kIx)	)
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
značně	značně	k6eAd1	značně
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
označení	označení	k1gNnSc1	označení
edikt	edikt	k1gInSc1	edikt
je	být	k5eAaImIp3nS	být
objektivně	objektivně	k6eAd1	objektivně
vzato	vzít	k5eAaPmNgNnS	vzít
nesprávné	správný	k2eNgNnSc1d1	nesprávné
<g/>
.	.	kIx.	.
</s>
<s>
Neexistoval	existovat	k5eNaImAgInS	existovat
žádný	žádný	k3yNgInSc1	žádný
edikt	edikt	k1gInSc1	edikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
platný	platný	k2eAgMnSc1d1	platný
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pouze	pouze	k6eAd1	pouze
určitá	určitý	k2eAgNnPc4d1	určité
ujednání	ujednání	k1gNnPc4	ujednání
týkající	týkající	k2eAgFnPc1d1	týkající
se	se	k3xPyFc4	se
politiky	politika	k1gFnSc2	politika
obou	dva	k4xCgMnPc2	dva
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
užívá	užívat	k5eAaImIp3nS	užívat
spíše	spíše	k9	spíše
výrazu	výraz	k1gInSc3	výraz
"	"	kIx"	"
<g/>
ústava	ústava	k1gFnSc1	ústava
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
protokol	protokol	k1gInSc1	protokol
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Milánské	milánský	k2eAgNnSc1d1	Milánské
ujednání	ujednání	k1gNnSc1	ujednání
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
či	či	k8xC	či
"	"	kIx"	"
<g/>
úmluva	úmluva	k1gFnSc1	úmluva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Milánské	milánský	k2eAgNnSc1d1	Milánské
ujednání	ujednání	k1gNnSc1	ujednání
představovalo	představovat	k5eAaImAgNnS	představovat
svobodu	svoboda	k1gFnSc4	svoboda
vyznání	vyznání	k1gNnSc2	vyznání
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pouze	pouze	k6eAd1	pouze
zrovnoprávnění	zrovnoprávnění	k1gNnSc1	zrovnoprávnění
křesťanství	křesťanství	k1gNnSc2	křesťanství
s	s	k7c7	s
dosavadním	dosavadní	k2eAgNnSc7d1	dosavadní
římským	římský	k2eAgNnSc7d1	římské
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
oficiální	oficiální	k2eAgNnSc4d1	oficiální
státní	státní	k2eAgNnSc4d1	státní
náboženství	náboženství	k1gNnSc4	náboženství
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
380	[number]	k4	380
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Theodosia	Theodosius	k1gMnSc4	Theodosius
I.	I.	kA	I.
Křesťanství	křesťanství	k1gNnSc1	křesťanství
ovšem	ovšem	k9	ovšem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Milánském	milánský	k2eAgNnSc6d1	Milánské
ujednání	ujednání	k1gNnSc6	ujednání
zvláště	zvláště	k6eAd1	zvláště
zvýrazněno	zvýraznit	k5eAaPmNgNnS	zvýraznit
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
právní	právní	k2eAgNnSc4d1	právní
uznání	uznání	k1gNnSc4	uznání
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
uznáno	uznat	k5eAaPmNgNnS	uznat
a	a	k8xC	a
tolerováno	tolerovat	k5eAaImNgNnS	tolerovat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
i	i	k8xC	i
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tolerančního	toleranční	k2eAgInSc2d1	toleranční
ediktu	edikt	k1gInSc2	edikt
vydaného	vydaný	k2eAgMnSc2d1	vydaný
již	již	k9	již
roku	rok	k1gInSc2	rok
311	[number]	k4	311
Galeriem	Galerium	k1gNnSc7	Galerium
<g/>
,	,	kIx,	,
mladším	mladý	k2eAgMnSc7d2	mladší
spoluvladařem	spoluvladař	k1gMnSc7	spoluvladař
císaře	císař	k1gMnSc2	císař
Diocletiana	Diocletian	k1gMnSc2	Diocletian
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Mailänder	Mailänder	k1gMnSc1	Mailänder
Vereinbarung	Vereinbarung	k1gMnSc1	Vereinbarung
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Edikt	edikt	k1gInSc1	edikt
milánský	milánský	k2eAgMnSc1d1	milánský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
