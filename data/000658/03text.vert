<s>
Sir	sir	k1gMnSc1	sir
William	William	k1gInSc4	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
Glasgow	Glasgow	k1gInSc1	Glasgow
-	-	kIx~	-
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
High	High	k1gInSc1	High
Wycombe	Wycomb	k1gInSc5	Wycomb
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
skotský	skotský	k2eAgMnSc1d1	skotský
chemik	chemik	k1gMnSc1	chemik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
za	za	k7c4	za
objev	objev	k1gInSc4	objev
vzácných	vzácný	k2eAgInPc2d1	vzácný
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
izoloval	izolovat	k5eAaBmAgInS	izolovat
ze	z	k7c2	z
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ramsay	Ramsa	k1gMnPc4	Ramsa
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
jako	jako	k8xS	jako
syn	syn	k1gMnSc1	syn
Williama	William	k1gMnSc2	William
Ramsaye	Ramsay	k1gFnSc2	Ramsay
a	a	k8xC	a
Catherine	Catherin	k1gInSc5	Catherin
Robertsonové	Robertsonové	k2eAgMnSc7d1	Robertsonové
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synovcem	synovec	k1gMnSc7	synovec
geologa	geolog	k1gMnSc2	geolog
Andrewa	Andrewus	k1gMnSc2	Andrewus
Ramsaye	Ramsay	k1gMnSc2	Ramsay
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Glasgow	Glasgow	k1gNnSc6	Glasgow
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Thomasa	Thomasa	k?	Thomasa
Andersona	Anderson	k1gMnSc2	Anderson
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
chemika	chemik	k1gMnSc2	chemik
zabývajícího	zabývající	k2eAgMnSc2d1	zabývající
se	se	k3xPyFc4	se
heterocyklickými	heterocyklický	k2eAgFnPc7d1	heterocyklická
sloučeninami	sloučenina	k1gFnPc7	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
známého	známý	k2eAgMnSc2d1	známý
analytika	analytik	k1gMnSc2	analytik
R.	R.	kA	R.
Tatlocka	Tatlocko	k1gNnSc2	Tatlocko
<g/>
;	;	kIx,	;
ve	v	k7c6	v
studiích	studio	k1gNnPc6	studio
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
Heidelbergu	Heidelberg	k1gInSc6	Heidelberg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
Roberta	Robert	k1gMnSc2	Robert
Bunsena	Bunsen	k2eAgFnSc1d1	Bunsena
<g/>
,	,	kIx,	,
po	po	k7c6	po
roce	rok	k1gInSc6	rok
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Tübingenu	Tübingen	k1gInSc2	Tübingen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
u	u	k7c2	u
profesora	profesor	k1gMnSc2	profesor
Rudolpha	Rudolph	k1gMnSc2	Rudolph
Fittiga	Fittig	k1gMnSc2	Fittig
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
disertační	disertační	k2eAgFnSc6d1	disertační
práci	práce	k1gFnSc6	práce
"	"	kIx"	"
<g/>
Orto	Orto	k1gNnSc1	Orto
-	-	kIx~	-
substituované	substituovaný	k2eAgFnPc1d1	substituovaná
kyseliny	kyselina	k1gFnPc1	kyselina
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
deriváty	derivát	k1gInPc1	derivát
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
práci	práce	k1gFnSc4	práce
obhájil	obhájit	k5eAaPmAgMnS	obhájit
roku	rok	k1gInSc2	rok
1872	[number]	k4	1872
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obhajobě	obhajoba	k1gFnSc6	obhajoba
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Glasgow	Glasgow	k1gNnSc2	Glasgow
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
na	na	k7c6	na
Andersonově	Andersonův	k2eAgFnSc6d1	Andersonova
Univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
získal	získat	k5eAaPmAgInS	získat
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
chemie	chemie	k1gFnSc2	chemie
v	v	k7c6	v
Bristolu	Bristol	k1gInSc6	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Margaret	Margareta	k1gFnPc2	Margareta
Buchananovou	Buchananová	k1gFnSc7	Buchananová
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rektorem	rektor	k1gMnSc7	rektor
Bristolské	bristolský	k2eAgFnSc2d1	Bristolská
Univerzity	univerzita	k1gFnSc2	univerzita
<g/>
;	;	kIx,	;
přitom	přitom	k6eAd1	přitom
dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
byl	být	k5eAaImAgMnS	být
profesorem	profesor	k1gMnSc7	profesor
anorganické	anorganický	k2eAgFnSc2d1	anorganická
a	a	k8xC	a
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
na	na	k7c4	na
londýnské	londýnský	k2eAgFnPc4d1	londýnská
University	universita	k1gFnPc4	universita
College	Colleg	k1gInSc2	Colleg
<g/>
;	;	kIx,	;
tam	tam	k6eAd1	tam
působil	působit	k5eAaImAgMnS	působit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
61	[number]	k4	61
roku	rok	k1gInSc2	rok
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1916	[number]	k4	1916
v	v	k7c6	v
Buckinghamshire	Buckinghamshir	k1gInSc5	Buckinghamshir
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
tamějším	tamější	k2eAgInSc6d1	tamější
kostele	kostel	k1gInSc6	kostel
Hazelmere	Hazelmer	k1gMnSc5	Hazelmer
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
pochován	pochován	k2eAgInSc1d1	pochován
William	William	k1gInSc1	William
Herschel	Herschela	k1gFnPc2	Herschela
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
hlavní	hlavní	k2eAgInPc1d1	hlavní
objevy	objev	k1gInPc1	objev
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
období	období	k1gNnSc2	období
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c4	na
University	universita	k1gFnPc4	universita
College	Colleg	k1gFnSc2	Colleg
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1894	[number]	k4	1894
začal	začít	k5eAaPmAgInS	začít
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
přednášky	přednáška	k1gFnPc4	přednáška
Lorda	lord	k1gMnSc2	lord
Rayleigha	Rayleigh	k1gMnSc2	Rayleigh
<g/>
.	.	kIx.	.
</s>
<s>
Rayleigh	Rayleigh	k1gInSc1	Rayleigh
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
hustotou	hustota	k1gFnSc7	hustota
dusíku	dusík	k1gInSc2	dusík
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
pomocí	pomocí	k7c2	pomocí
chemické	chemický	k2eAgFnSc2d1	chemická
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
dusíku	dusík	k1gInSc2	dusík
izolovaného	izolovaný	k2eAgInSc2d1	izolovaný
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
odloučením	odloučení	k1gNnSc7	odloučení
jiných	jiný	k2eAgInPc2d1	jiný
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Ramsayem	Ramsay	k1gInSc7	Ramsay
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
tuto	tento	k3xDgFnSc4	tento
anomálii	anomálie	k1gFnSc4	anomálie
budou	být	k5eAaImBp3nP	být
společně	společně	k6eAd1	společně
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určité	určitý	k2eAgFnSc6d1	určitá
době	doba	k1gFnSc6	doba
Ramsay	Ramsaa	k1gMnSc2	Ramsaa
Rayleigha	Rayleigh	k1gMnSc2	Rayleigh
informoval	informovat	k5eAaBmAgMnS	informovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
izoloval	izolovat	k5eAaBmAgInS	izolovat
doposud	doposud	k6eAd1	doposud
neznámý	známý	k2eNgInSc1d1	neznámý
inertní	inertní	k2eAgInSc1d1	inertní
prvek	prvek	k1gInSc1	prvek
<g/>
;	;	kIx,	;
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
jej	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
argon	argon	k1gInSc4	argon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
objevil	objevit	k5eAaPmAgInS	objevit
i	i	k9	i
neon	neon	k1gInSc1	neon
<g/>
,	,	kIx,	,
krypton	krypton	k1gInSc1	krypton
<g/>
,	,	kIx,	,
xenon	xenon	k1gInSc1	xenon
a	a	k8xC	a
hélium	hélium	k1gNnSc1	hélium
<g/>
;	;	kIx,	;
čáry	čára	k1gFnPc1	čára
hélia	hélium	k1gNnSc2	hélium
byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
pozorovány	pozorovat	k5eAaImNgInP	pozorovat
při	při	k7c6	při
spektrální	spektrální	k2eAgFnSc6d1	spektrální
analýze	analýza	k1gFnSc6	analýza
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
plyn	plyn	k1gInSc1	plyn
samotný	samotný	k2eAgInSc1d1	samotný
nebyl	být	k5eNaImAgInS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
izolován	izolovat	k5eAaBmNgMnS	izolovat
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
hélium	hélium	k1gNnSc1	hélium
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
rozpadu	rozpad	k1gInSc6	rozpad
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
anorganické	anorganický	k2eAgFnSc2d1	anorganická
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
A	a	k8xC	a
System	Syst	k1gInSc7	Syst
of	of	k?	of
Inorganic	Inorganice	k1gFnPc2	Inorganice
Chemistry	Chemistr	k1gMnPc7	Chemistr
<g/>
,	,	kIx,	,
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Plyny	plyn	k1gInPc1	plyn
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
jejich	jejich	k3xOp3gInSc2	jejich
objevu	objev	k1gInSc2	objev
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
gases	gases	k1gMnSc1	gases
of	of	k?	of
the	the	k?	the
atmosphere	atmosphrat	k5eAaPmIp3nS	atmosphrat
<g/>
,	,	kIx,	,
the	the	k?	the
history	histor	k1gInPc1	histor
of	of	k?	of
their	their	k1gInSc4	their
discovery	discovera	k1gFnSc2	discovera
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
Moderní	moderní	k2eAgFnSc1d1	moderní
chemie	chemie	k1gFnSc1	chemie
(	(	kIx(	(
<g/>
Modern	Modern	k1gMnSc1	Modern
chemistry	chemistr	k1gMnPc7	chemistr
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
chemie	chemie	k1gFnSc2	chemie
(	(	kIx(	(
<g/>
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc1	ten
the	the	k?	the
study	stud	k1gInPc1	stud
of	of	k?	of
physical	physicat	k5eAaPmAgInS	physicat
chemistry	chemistr	k1gMnPc4	chemistr
<g/>
,	,	kIx,	,
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
Prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
elektrony	elektron	k1gInPc1	elektron
(	(	kIx(	(
<g/>
Elements	Elements	k1gInSc1	Elements
and	and	k?	and
electrons	electrons	k1gInSc1	electrons
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gMnSc2	Ramsaa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
William	William	k1gInSc1	William
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Životopis	životopis	k1gInSc4	životopis
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
</s>
