<p>
<s>
Fialová	Fialová	k1gFnSc1	Fialová
(	(	kIx(	(
<g/>
od	od	k7c2	od
Viola	Viola	k1gFnSc1	Viola
<g/>
,	,	kIx,	,
fialka	fialka	k1gFnSc1	fialka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
mezi	mezi	k7c7	mezi
modrou	modrý	k2eAgFnSc7d1	modrá
a	a	k8xC	a
neviditelnou	viditelný	k2eNgFnSc7d1	neviditelná
ultrafialovou	ultrafialový	k2eAgFnSc7d1	ultrafialová
(	(	kIx(	(
<g/>
UV	UV	kA	UV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
380	[number]	k4	380
<g/>
–	–	k?	–
<g/>
450	[number]	k4	450
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
skládání	skládání	k1gNnSc2	skládání
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
kterákoli	kterýkoli	k3yIgFnSc1	kterýkoli
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
mezi	mezi	k7c7	mezi
červenou	červený	k2eAgFnSc7d1	červená
a	a	k8xC	a
modrou	modrý	k2eAgFnSc7d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Fialová	Fialová	k1gFnSc1	Fialová
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
podílem	podíl	k1gInSc7	podíl
červené	červená	k1gFnSc2	červená
je	být	k5eAaImIp3nS	být
purpurová	purpurový	k2eAgFnSc1d1	purpurová
a	a	k8xC	a
magenta	magenta	k1gFnSc1	magenta
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
větší	veliký	k2eAgInSc1d2	veliký
podíl	podíl	k1gInSc1	podíl
modré	modré	k1gNnSc1	modré
má	mít	k5eAaImIp3nS	mít
indigo	indigo	k1gNnSc4	indigo
nebo	nebo	k8xC	nebo
ultramarín	ultramarín	k1gInSc4	ultramarín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
církvích	církev	k1gFnPc6	církev
je	být	k5eAaImIp3nS	být
fialová	fialový	k2eAgFnSc1d1	fialová
liturgickou	liturgický	k2eAgFnSc7d1	liturgická
barvou	barva	k1gFnSc7	barva
pro	pro	k7c4	pro
advent	advent	k1gInSc4	advent
<g/>
,	,	kIx,	,
postní	postní	k2eAgFnSc4d1	postní
dobu	doba	k1gFnSc4	doba
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Fialová	Fialová	k1gFnSc1	Fialová
je	být	k5eAaImIp3nS	být
také	také	k9	také
barva	barva	k1gFnSc1	barva
biskupů	biskup	k1gMnPc2	biskup
a	a	k8xC	a
arcibiskupů	arcibiskup	k1gMnPc2	arcibiskup
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Fialovým	Fialův	k2eAgInSc7d1	Fialův
trojúhelníkem	trojúhelník	k1gInSc7	trojúhelník
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
nacistických	nacistický	k2eAgInPc6d1	nacistický
koncetračních	koncetrační	k2eAgInPc6d1	koncetrační
táborech	tábor	k1gInPc6	tábor
označováni	označován	k2eAgMnPc1d1	označován
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
fialová	fialový	k2eAgFnSc1d1	fialová
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
7	[number]	k4	7
nebo	nebo	k8xC	nebo
toleranci	tolerance	k1gFnSc4	tolerance
±	±	k?	±
<g/>
0.1	[number]	k4	0.1
<g/>
%	%	kIx~	%
</s>
</p>
