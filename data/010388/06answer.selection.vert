<s>
Fialová	Fialová	k1gFnSc1	Fialová
(	(	kIx(	(
<g/>
od	od	k7c2	od
Viola	Viola	k1gFnSc1	Viola
<g/>
,	,	kIx,	,
fialka	fialka	k1gFnSc1	fialka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
barva	barva	k1gFnSc1	barva
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
mezi	mezi	k7c7	mezi
modrou	modrý	k2eAgFnSc7d1	modrá
a	a	k8xC	a
neviditelnou	viditelný	k2eNgFnSc7d1	neviditelná
ultrafialovou	ultrafialový	k2eAgFnSc7d1	ultrafialová
(	(	kIx(	(
<g/>
UV	UV	kA	UV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
