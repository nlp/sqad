<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
<g/>
:	:	kIx,	:
Latvija	Latvija	k1gFnSc1	Latvija
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Latvijas	Latvijas	k1gInSc1	Latvijas
Republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prostřední	prostřední	k2eAgMnSc1d1	prostřední
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
Rižského	rižský	k2eAgInSc2d1	rižský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Estonskem	Estonsko	k1gNnSc7	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
okolo	okolo	k7c2	okolo
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Riga	Riga	k1gFnSc1	Riga
<g/>
.	.	kIx.	.
</s>
<s>
Lotyši	Lotyš	k1gMnPc1	Lotyš
jsou	být	k5eAaImIp3nP	být
baltským	baltský	k2eAgInSc7d1	baltský
národem	národ	k1gInSc7	národ
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
62	[number]	k4	62
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
lotyština	lotyština	k1gFnSc1	lotyština
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
jako	jako	k8xS	jako
bývalá	bývalý	k2eAgFnSc1d1	bývalá
svazová	svazový	k2eAgFnSc1d1	svazová
republika	republika	k1gFnSc1	republika
obnovila	obnovit	k5eAaPmAgFnS	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Schengenského	schengenský	k2eAgInSc2d1	schengenský
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Latvija	Latvijum	k1gNnSc2	Latvijum
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
názvu	název	k1gInSc2	název
starobylého	starobylý	k2eAgNnSc2d1	starobylé
baltského	baltský	k2eAgNnSc2d1	Baltské
(	(	kIx(	(
<g/>
indoevropského	indoevropský	k2eAgMnSc2d1	indoevropský
<g/>
)	)	kIx)	)
kmene	kmen	k1gInSc2	kmen
Latgalů	Latgal	k1gInPc2	Latgal
(	(	kIx(	(
<g/>
lotyšsky	lotyšsky	k6eAd1	lotyšsky
Latgali	Latgali	k1gMnSc2	Latgali
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
etnické	etnický	k2eAgNnSc1d1	etnické
jádro	jádro	k1gNnSc1	jádro
lotyšského	lotyšský	k2eAgInSc2d1	lotyšský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Brestlitevský	brestlitevský	k2eAgInSc1d1	brestlitevský
mír	mír	k1gInSc1	mír
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1918	[number]	k4	1918
potvrzoval	potvrzovat	k5eAaImAgInS	potvrzovat
vítězství	vítězství	k1gNnSc4	vítězství
ústředních	ústřední	k2eAgFnPc2d1	ústřední
mocností	mocnost	k1gFnPc2	mocnost
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
dobylo	dobýt	k5eAaPmAgNnS	dobýt
Pobaltí	Pobaltí	k1gNnSc4	Pobaltí
(	(	kIx(	(
<g/>
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
,	,	kIx,	,
Livonsko	Livonsko	k1gNnSc1	Livonsko
<g/>
,	,	kIx,	,
Kuronsko	Kuronsko	k1gNnSc1	Kuronsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Německu	Německo	k1gNnSc6	Německo
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
okupováno	okupovat	k5eAaBmNgNnS	okupovat
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
anektováno	anektovat	k5eAaBmNgNnS	anektovat
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
sovětské	sovětský	k2eAgFnSc2d1	sovětská
vlády	vláda	k1gFnSc2	vláda
před	před	k7c7	před
německou	německý	k2eAgFnSc7d1	německá
invazí	invaze	k1gFnSc7	invaze
byly	být	k5eAaImAgFnP	být
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
Lotyšů	Lotyš	k1gMnPc2	Lotyš
odvlečeny	odvlečen	k2eAgMnPc4d1	odvlečen
do	do	k7c2	do
sibiřských	sibiřský	k2eAgInPc2d1	sibiřský
gulagů	gulag	k1gInPc2	gulag
nebo	nebo	k8xC	nebo
povražděny	povražděn	k2eAgFnPc1d1	povražděna
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
byli	být	k5eAaImAgMnP	být
proto	proto	k8xC	proto
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
poměrně	poměrně	k6eAd1	poměrně
značnou	značný	k2eAgFnSc7d1	značná
částí	část	k1gFnSc7	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vítáni	vítán	k2eAgMnPc1d1	vítán
jako	jako	k8xC	jako
osvoboditelé	osvoboditel	k1gMnPc1	osvoboditel
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zvěrstev	zvěrstvo	k1gNnPc2	zvěrstvo
páchaných	páchaný	k2eAgFnPc2d1	páchaná
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
postavilo	postavit	k5eAaPmAgNnS	postavit
proti	proti	k7c3	proti
Němcům	Němec	k1gMnPc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
a	a	k8xC	a
místní	místní	k2eAgMnPc1d1	místní
kolaboranti	kolaborant	k1gMnPc1	kolaborant
povraždili	povraždit	k5eAaPmAgMnP	povraždit
přes	přes	k7c4	přes
60	[number]	k4	60
000	[number]	k4	000
lotyšských	lotyšský	k2eAgMnPc2d1	lotyšský
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
při	při	k7c6	při
masakrech	masakr	k1gInPc6	masakr
v	v	k7c6	v
Rumbule	Rumbula	k1gFnSc6	Rumbula
a	a	k8xC	a
v	v	k7c6	v
Liepā	Liepā	k1gFnSc6	Liepā
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgFnSc4d1	počáteční
podporu	podpora	k1gFnSc4	podpora
Němců	Němec	k1gMnPc2	Němec
později	pozdě	k6eAd2	pozdě
zneužívala	zneužívat	k5eAaImAgFnS	zneužívat
sovětská	sovětský	k2eAgFnSc1d1	sovětská
propaganda	propaganda	k1gFnSc1	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k8xC	jako
Lotyšská	lotyšský	k2eAgFnSc1d1	lotyšská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
až	až	k9	až
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
velmocí	velmoc	k1gFnSc7	velmoc
vyvážející	vyvážející	k2eAgFnPc1d1	vyvážející
potraviny	potravina	k1gFnPc1	potravina
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
přeorientováno	přeorientován	k2eAgNnSc1d1	přeorientován
na	na	k7c4	na
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
samostatnost	samostatnost	k1gFnSc1	samostatnost
byla	být	k5eAaImAgFnS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
jako	jako	k8xC	jako
přežitek	přežitek	k1gInSc1	přežitek
-	-	kIx~	-
země	země	k1gFnSc1	země
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
integrována	integrovat	k5eAaBmNgFnS	integrovat
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
sovětského	sovětský	k2eAgInSc2d1	sovětský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
či	či	k8xC	či
modernizované	modernizovaný	k2eAgNnSc1d1	modernizované
staré	staré	k1gNnSc1	staré
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
závod	závod	k1gInSc1	závod
RVR	RVR	kA	RVR
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
získaly	získat	k5eAaPmAgFnP	získat
odbytiště	odbytiště	k1gNnPc4	odbytiště
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
docházelo	docházet	k5eAaImAgNnS	docházet
také	také	k9	také
k	k	k7c3	k
výměně	výměna	k1gFnSc3	výměna
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
i	i	k8xC	i
kulturních	kulturní	k2eAgInPc2d1	kulturní
vlivů	vliv	k1gInPc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
stěhovat	stěhovat	k5eAaImF	stěhovat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
hlavně	hlavně	k6eAd1	hlavně
pak	pak	k6eAd1	pak
Rusů	Rus	k1gMnPc2	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Lotyši	Lotyš	k1gMnPc1	Lotyš
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
čtrnácti	čtrnáct	k4xCc6	čtrnáct
republikách	republika	k1gFnPc6	republika
<g/>
,	,	kIx,	,
tisíce	tisíc	k4xCgInPc1	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
domoviny	domovina	k1gFnSc2	domovina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
také	také	k9	také
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
rusifikaci	rusifikace	k1gFnSc4	rusifikace
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
lotyština	lotyština	k1gFnSc1	lotyština
zůstala	zůstat	k5eAaPmAgFnS	zůstat
oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
ruštinou	ruština	k1gFnSc7	ruština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
svazu	svaz	k1gInSc2	svaz
dominantní	dominantní	k2eAgInSc4d1	dominantní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
perestrojky	perestrojka	k1gFnSc2	perestrojka
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
reforem	reforma	k1gFnPc2	reforma
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
součástí	součást	k1gFnSc7	součást
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
zajištění	zajištění	k1gNnSc4	zajištění
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
soběstačnosti	soběstačnost	k1gFnSc2	soběstačnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
projevilo	projevit	k5eAaPmAgNnS	projevit
nárůstem	nárůst	k1gInSc7	nárůst
hojnosti	hojnost	k1gFnSc2	hojnost
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
obecně	obecně	k6eAd1	obecně
nedostatek	nedostatek	k1gInSc4	nedostatek
<g/>
,	,	kIx,	,
a	a	k8xC	a
nákupní	nákupní	k2eAgFnSc7d1	nákupní
turistikou	turistika	k1gFnSc7	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
nevěřila	věřit	k5eNaImAgFnS	věřit
faktické	faktický	k2eAgFnPc4d1	faktická
možnosti	možnost	k1gFnPc4	možnost
opuštění	opuštění	k1gNnSc2	opuštění
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
získání	získání	k1gNnSc4	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
přivádět	přivádět	k5eAaImF	přivádět
k	k	k7c3	k
životu	život	k1gInSc3	život
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
právě	právě	k9	právě
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
republikami	republika	k1gFnPc7	republika
Pobaltí	Pobaltí	k1gNnSc2	Pobaltí
<g/>
,	,	kIx,	,
k	k	k7c3	k
zděšení	zděšení	k1gNnSc3	zděšení
mnoha	mnoho	k4c3	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
ostatních	ostatní	k2eAgFnPc2d1	ostatní
republik	republika	k1gFnPc2	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
i	i	k8xC	i
jeho	on	k3xPp3gNnSc2	on
politického	politický	k2eAgNnSc2d1	politické
vedení	vedení	k1gNnSc2	vedení
<g/>
,	,	kIx,	,
osamostatnilo	osamostatnit	k5eAaPmAgNnS	osamostatnit
mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgInS	začít
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
růst	růst	k1gInSc1	růst
a	a	k8xC	a
integrace	integrace	k1gFnSc1	integrace
s	s	k7c7	s
evropskými	evropský	k2eAgInPc7d1	evropský
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
organizace	organizace	k1gFnSc2	organizace
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
země	země	k1gFnSc1	země
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
také	také	k9	také
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gInSc7	člen
NATO	NATO	kA	NATO
<g/>
.	.	kIx.	.
</s>
<s>
Odbourání	odbourání	k1gNnSc1	odbourání
celních	celní	k2eAgFnPc2d1	celní
bariér	bariéra	k1gFnPc2	bariéra
a	a	k8xC	a
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
jednotnému	jednotný	k2eAgInSc3d1	jednotný
hospodářskému	hospodářský	k2eAgInSc3d1	hospodářský
prostoru	prostor	k1gInSc3	prostor
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
pro	pro	k7c4	pro
lotyšské	lotyšský	k2eAgNnSc4d1	lotyšské
hospodářství	hospodářství	k1gNnSc4	hospodářství
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
měla	mít	k5eAaImAgFnS	mít
po	po	k7c4	po
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
v	v	k7c6	v
EU	EU	kA	EU
<g/>
,	,	kIx,	,
dopadla	dopadnout	k5eAaPmAgFnS	dopadnout
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
těžce	těžce	k6eAd1	těžce
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
velkých	velký	k2eAgFnPc2d1	velká
potíží	potíž	k1gFnPc2	potíž
a	a	k8xC	a
mluvilo	mluvit	k5eAaImAgNnS	mluvit
se	se	k3xPyFc4	se
také	také	k9	také
i	i	k9	i
o	o	k7c6	o
státním	státní	k2eAgInSc6d1	státní
bankrotu	bankrot	k1gInSc6	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
rovinatý	rovinatý	k2eAgInSc1d1	rovinatý
<g/>
,	,	kIx,	,
nížinný	nížinný	k2eAgInSc1d1	nížinný
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
s	s	k7c7	s
pahorkatinami	pahorkatina	k1gFnPc7	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
jsou	být	k5eAaImIp3nP	být
Vidzemské	Vidzemský	k2eAgInPc4d1	Vidzemský
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
bylo	být	k5eAaImAgNnS	být
modelováno	modelovat	k5eAaImNgNnS	modelovat
pleistocénním	pleistocénní	k2eAgInSc7d1	pleistocénní
ledovcem	ledovec	k1gInSc7	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Zůstaly	zůstat	k5eAaPmAgFnP	zůstat
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
morény	moréna	k1gFnPc1	moréna
a	a	k8xC	a
nánosy	nános	k1gInPc1	nános
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Kuronskou	Kuronský	k2eAgFnSc7d1	Kuronská
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Livonskou	Livonský	k2eAgFnSc4d1	Livonská
vrchovinu	vrchovina	k1gFnSc4	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
s	s	k7c7	s
Litvou	Litva	k1gFnSc7	Litva
(	(	kIx(	(
<g/>
588	[number]	k4	588
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
s	s	k7c7	s
Běloruskem	Bělorusko	k1gNnSc7	Bělorusko
(	(	kIx(	(
<g/>
161	[number]	k4	161
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
(	(	kIx(	(
<g/>
246	[number]	k4	246
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
s	s	k7c7	s
Estonskem	Estonsko	k1gNnSc7	Estonsko
(	(	kIx(	(
<g/>
343	[number]	k4	343
km	km	kA	km
<g/>
)	)	kIx)	)
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
leží	ležet	k5eAaImIp3nS	ležet
pásmo	pásmo	k1gNnSc4	pásmo
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Daugava	Daugava	k1gFnSc1	Daugava
<g/>
,	,	kIx,	,
Venta	Venta	k1gFnSc1	Venta
a	a	k8xC	a
Gauja	Gauja	k1gFnSc1	Gauja
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
Lubanas	Lubanasa	k1gFnPc2	Lubanasa
a	a	k8xC	a
Rā	Rā	k1gFnPc2	Rā
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
bažin	bažina	k1gFnPc2	bažina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
10	[number]	k4	10
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podnebím	podnebí	k1gNnSc7	podnebí
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
chladnějšího	chladný	k2eAgInSc2d2	chladnější
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
kontinentálním	kontinentální	k2eAgInSc7d1	kontinentální
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvětšující	zvětšující	k2eAgFnSc7d1	zvětšující
se	se	k3xPyFc4	se
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
se	se	k3xPyFc4	se
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
i	i	k9	i
teplotní	teplotní	k2eAgInPc4d1	teplotní
rozdíly	rozdíl	k1gInPc4	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
lednová	lednový	k2eAgFnSc1d1	lednová
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
-5	-5	k4	-5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
červencová	červencový	k2eAgFnSc1d1	červencová
17	[number]	k4	17
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
roční	roční	k2eAgInSc1d1	roční
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
630	[number]	k4	630
mm	mm	kA	mm
<g/>
,	,	kIx,	,
v	v	k7c6	v
Livonské	Livonský	k2eAgFnSc6d1	Livonská
vrchovině	vrchovina	k1gFnSc6	vrchovina
až	až	k9	až
800	[number]	k4	800
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
má	mít	k5eAaImIp3nS	mít
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
(	(	kIx(	(
<g/>
Saeima	Saeimum	k1gNnSc2	Saeimum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
do	do	k7c2	do
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
volí	volit	k5eAaImIp3nS	volit
100	[number]	k4	100
zastupitelů	zastupitel	k1gMnPc2	zastupitel
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
na	na	k7c4	na
čtyřleté	čtyřletý	k2eAgNnSc4d1	čtyřleté
období	období	k1gNnSc4	období
volí	volit	k5eAaImIp3nS	volit
prezidenta	prezident	k1gMnSc4	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
premiéra	premiéra	k1gFnSc1	premiéra
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
uchází	ucházet	k5eAaImIp3nS	ucházet
o	o	k7c4	o
důvěru	důvěra	k1gFnSc4	důvěra
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Andris	Andris	k1gFnSc7	Andris
Bē	Bē	k1gFnPc2	Bē
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
postsovětským	postsovětský	k2eAgMnSc7d1	postsovětský
prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Guntis	Guntis	k1gFnSc4	Guntis
Ulmanis	Ulmanis	k1gFnSc2	Ulmanis
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Vaira	Vaira	k1gMnSc1	Vaira
Vī	Vī	k1gMnSc1	Vī
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
Valdis	Valdis	k1gFnSc1	Valdis
Zatlers	Zatlersa	k1gFnPc2	Zatlersa
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
premiérkou	premiérka	k1gFnSc7	premiérka
ekonomka	ekonomka	k1gFnSc1	ekonomka
a	a	k8xC	a
bývala	bývat	k5eAaImAgFnS	bývat
ministryně	ministryně	k1gFnSc1	ministryně
zemědělství	zemědělství	k1gNnSc2	zemědělství
Laimdota	Laimdota	k1gFnSc1	Laimdota
Straujuma	Straujuma	k1gFnSc1	Straujuma
z	z	k7c2	z
liberálně	liberálně	k6eAd1	liberálně
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
strany	strana	k1gFnSc2	strana
Unity	unita	k1gMnSc2	unita
(	(	kIx(	(
<g/>
Jednota	jednota	k1gFnSc1	jednota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
zasedání	zasedání	k1gNnSc1	zasedání
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
živě	živě	k6eAd1	živě
vysíláno	vysílat	k5eAaImNgNnS	vysílat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Raimonds	Raimonds	k1gInSc4	Raimonds
Vē	Vē	k1gFnSc2	Vē
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
úřadu	úřad	k1gInSc2	úřad
ujal	ujmout	k5eAaPmAgInS	ujmout
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
je	být	k5eAaImIp3nS	být
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
a	a	k8xC	a
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
<g/>
:	:	kIx,	:
strojírenství	strojírenství	k1gNnSc6	strojírenství
<g/>
,	,	kIx,	,
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
<g/>
,	,	kIx,	,
radioelektronika	radioelektronika	k1gFnSc1	radioelektronika
<g/>
,	,	kIx,	,
papírenský	papírenský	k2eAgInSc4d1	papírenský
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInSc4d1	textilní
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInSc4d1	dřevozpracující
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
průmysl	průmysl	k1gInSc4	průmysl
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgNnPc1d1	důležité
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
střediska	středisko	k1gNnPc1	středisko
jsou	být	k5eAaImIp3nP	být
Riga	Riga	k1gFnSc1	Riga
<g/>
,	,	kIx,	,
Daugavpils	Daugavpils	k1gInSc1	Daugavpils
<g/>
,	,	kIx,	,
Liepā	Liepā	k1gFnSc1	Liepā
a	a	k8xC	a
Jū	Jū	k1gFnSc1	Jū
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těžby	těžba	k1gFnSc2	těžba
rašeliny	rašelina	k1gFnSc2	rašelina
nemá	mít	k5eNaImIp3nS	mít
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
zdroje	zdroj	k1gInSc2	zdroj
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
části	část	k1gFnSc2	část
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
její	její	k3xOp3gInPc1	její
produkcí	produkce	k1gFnSc7	produkce
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
a	a	k8xC	a
vodních	vodní	k2eAgFnPc6d1	vodní
elektrárnách	elektrárna	k1gFnPc6	elektrárna
a	a	k8xC	a
z	z	k7c2	z
části	část	k1gFnSc2	část
jejím	její	k3xOp3gInSc7	její
dovozem	dovoz	k1gInSc7	dovoz
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
přínos	přínos	k1gInSc1	přínos
financí	finance	k1gFnPc2	finance
do	do	k7c2	do
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
tranzitní	tranzitní	k2eAgFnSc1d1	tranzitní
přeprava	přeprava	k1gFnSc1	přeprava
ruského	ruský	k2eAgNnSc2d1	ruské
zboží	zboží	k1gNnSc2	zboží
územím	území	k1gNnSc7	území
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
a	a	k8xC	a
přes	přes	k7c4	přes
přístavy	přístav	k1gInPc4	přístav
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
převládá	převládat	k5eAaImIp3nS	převládat
živočišná	živočišný	k2eAgFnSc1d1	živočišná
produkce	produkce	k1gFnSc1	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
skotu	skot	k1gInSc2	skot
a	a	k8xC	a
prasat	prase	k1gNnPc2	prase
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
brambory	brambor	k1gInPc1	brambor
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zejména	zejména	k9	zejména
sledi	sleď	k1gMnPc1	sleď
a	a	k8xC	a
šproty	šprota	k1gFnPc1	šprota
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
mléčných	mléčný	k2eAgInPc2d1	mléčný
výrobků	výrobek	k1gInPc2	výrobek
a	a	k8xC	a
rybích	rybí	k2eAgInPc2d1	rybí
výrobků	výrobek	k1gInPc2	výrobek
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
sardinky	sardinka	k1gFnPc4	sardinka
nebo	nebo	k8xC	nebo
rybí	rybí	k2eAgFnSc1d1	rybí
moučka	moučka	k1gFnSc1	moučka
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
46	[number]	k4	46
%	%	kIx~	%
<g/>
,	,	kIx,	,
orná	orný	k2eAgFnSc1d1	orná
půda	půda	k1gFnSc1	půda
tvoří	tvořit	k5eAaImIp3nS	tvořit
27	[number]	k4	27
%	%	kIx~	%
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc1	pastvina
13	[number]	k4	13
%	%	kIx~	%
a	a	k8xC	a
ostatní	ostatní	k2eAgMnSc1d1	ostatní
14	[number]	k4	14
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
počtu	počet	k1gInSc3	počet
lesů	les	k1gInPc2	les
a	a	k8xC	a
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jantar	jantar	k1gInSc1	jantar
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
stala	stát	k5eAaPmAgFnS	stát
příkladem	příklad	k1gInSc7	příklad
negativních	negativní	k2eAgInPc2d1	negativní
důsledků	důsledek	k1gInPc2	důsledek
přehřátí	přehřátí	k1gNnSc2	přehřátí
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
a	a	k8xC	a
příjmů	příjem	k1gInPc2	příjem
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
zvýšení	zvýšení	k1gNnSc3	zvýšení
spotřebitelské	spotřebitelský	k2eAgFnSc2d1	spotřebitelská
poptávky	poptávka	k1gFnSc2	poptávka
<g/>
,	,	kIx,	,
následné	následný	k2eAgFnSc6d1	následná
inflaci	inflace	k1gFnSc6	inflace
a	a	k8xC	a
současně	současně	k6eAd1	současně
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
schodku	schodek	k1gInSc2	schodek
běžného	běžný	k2eAgInSc2d1	běžný
účtu	účet	k1gInSc2	účet
a	a	k8xC	a
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
dluhu	dluh	k1gInSc2	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
sestupnou	sestupný	k2eAgFnSc4d1	sestupná
fázi	fáze	k1gFnSc4	fáze
již	již	k6eAd1	již
před	před	k7c7	před
globální	globální	k2eAgFnSc7d1	globální
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
stačilo	stačit	k5eAaBmAgNnS	stačit
snížit	snížit	k5eAaPmF	snížit
svůj	svůj	k3xOyFgInSc4	svůj
rozpočtový	rozpočtový	k2eAgInSc4d1	rozpočtový
deficit	deficit	k1gInSc4	deficit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
dluh	dluh	k1gInSc1	dluh
zůstal	zůstat	k5eAaPmAgInS	zůstat
nad	nad	k7c7	nad
sedmdesáti	sedmdesát	k4xCc7	sedmdesát
procenty	procento	k1gNnPc7	procento
HDP	HDP	kA	HDP
a	a	k8xC	a
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
tak	tak	k9	tak
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
překonání	překonání	k1gNnSc4	překonání
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vstupu	vstup	k1gInSc2	vstup
země	zem	k1gFnSc2	zem
do	do	k7c2	do
EU	EU	kA	EU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgInS	být
lotyšský	lotyšský	k2eAgInSc1d1	lotyšský
lat	lat	k1gInSc1	lat
pevně	pevně	k6eAd1	pevně
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
přistoupilo	přistoupit	k5eAaPmAgNnS	přistoupit
jako	jako	k9	jako
18	[number]	k4	18
<g/>
.	.	kIx.	.
člen	člen	k1gMnSc1	člen
do	do	k7c2	do
eurozóny	eurozóna	k1gFnSc2	eurozóna
a	a	k8xC	a
přijalo	přijmout	k5eAaPmAgNnS	přijmout
společnou	společný	k2eAgFnSc4d1	společná
evropskou	evropský	k2eAgFnSc4d1	Evropská
měnu	měna	k1gFnSc4	měna
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Gaujas	Gaujasa	k1gFnPc2	Gaujasa
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Ventspils	Ventspilsa	k1gFnPc2	Ventspilsa
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
přístavem	přístav	k1gInSc7	přístav
a	a	k8xC	a
překladištěm	překladiště	k1gNnSc7	překladiště
zboží	zboží	k1gNnSc2	zboží
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
slaví	slavit	k5eAaImIp3nP	slavit
váleční	váleční	k2eAgMnPc1d1	váleční
veteráni	veterán	k1gMnPc1	veterán
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
lotyšský	lotyšský	k2eAgInSc4d1	lotyšský
den	den	k1gInSc4	den
legionářů	legionář	k1gMnPc2	legionář
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
oficiální	oficiální	k2eAgInSc4d1	oficiální
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
tradiční	tradiční	k2eAgInSc1d1	tradiční
pochod	pochod	k1gInSc1	pochod
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
lotyšských	lotyšský	k2eAgMnPc2d1	lotyšský
veteránů	veterán	k1gMnPc2	veterán
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
Waffen-SS	Waffen-SS	k1gMnPc2	Waffen-SS
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
divize	divize	k1gFnSc2	divize
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
členy	člen	k1gInPc7	člen
EU	EU	kA	EU
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
otevřeně	otevřeně	k6eAd1	otevřeně
takovéto	takovýto	k3xDgFnPc1	takovýto
<g/>
,	,	kIx,	,
minimálně	minimálně	k6eAd1	minimálně
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
lotyšské	lotyšský	k2eAgFnSc6d1	lotyšská
vládě	vláda	k1gFnSc6	vláda
ministr	ministr	k1gMnSc1	ministr
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
místního	místní	k2eAgInSc2d1	místní
rozvoje	rozvoj	k1gInSc2	rozvoj
Einars	Einarsa	k1gFnPc2	Einarsa
Cilinskis	Cilinskis	k1gFnSc2	Cilinskis
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
účasti	účast	k1gFnSc6	účast
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pochodu	pochod	k1gInSc6	pochod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řadách	řada	k1gFnPc6	řada
jednotek	jednotka	k1gFnPc2	jednotka
SS	SS	kA	SS
bojovalo	bojovat	k5eAaImAgNnS	bojovat
za	za	k7c2	za
války	válka	k1gFnSc2	válka
okolo	okolo	k7c2	okolo
140	[number]	k4	140
000	[number]	k4	000
Lotyšů	Lotyš	k1gMnPc2	Lotyš
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
Lotyšů	Lotyš	k1gMnPc2	Lotyš
bojoval	bojovat	k5eAaImAgInS	bojovat
proti	proti	k7c3	proti
nacistům	nacista	k1gMnPc3	nacista
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
2	[number]	k4	2
067	[number]	k4	067
887	[number]	k4	887
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
62,1	[number]	k4	62,1
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
jsou	být	k5eAaImIp3nP	být
etničtí	etnický	k2eAgMnPc1d1	etnický
Lotyši	Lotyš	k1gMnPc1	Lotyš
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
menšina	menšina	k1gFnSc1	menšina
tvořená	tvořený	k2eAgFnSc1d1	tvořená
Rusy	Rus	k1gMnPc7	Rus
(	(	kIx(	(
<g/>
cca	cca	kA	cca
26,9	[number]	k4	26,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
lotyšské	lotyšský	k2eAgNnSc4d1	lotyšské
občanství	občanství	k1gNnSc4	občanství
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
volit	volit	k5eAaImF	volit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
značným	značný	k2eAgInSc7d1	značný
tlakem	tlak	k1gInSc7	tlak
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
status	status	k1gInSc4	status
tzv.	tzv.	kA	tzv.
neobčanů	neobčan	k1gMnPc2	neobčan
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
potomci	potomek	k1gMnPc1	potomek
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Lotyška	Lotyška	k1gFnSc1	Lotyška
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
to	ten	k3xDgNnSc1	ten
však	však	k9	však
odmítá	odmítat	k5eAaImIp3nS	odmítat
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
možnosti	možnost	k1gFnSc3	možnost
také	také	k9	také
staví	stavit	k5eAaBmIp3nS	stavit
odmítavě	odmítavě	k6eAd1	odmítavě
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
přiznání	přiznání	k1gNnSc4	přiznání
statutu	statut	k1gInSc2	statut
oficiálního	oficiální	k2eAgInSc2d1	oficiální
jazyka	jazyk	k1gInSc2	jazyk
pro	pro	k7c4	pro
ruštinu	ruština	k1gFnSc4	ruština
zamítli	zamítnout	k5eAaPmAgMnP	zamítnout
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
lotyšští	lotyšský	k2eAgMnPc1d1	lotyšský
občané	občan	k1gMnPc1	občan
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
národnostní	národnostní	k2eAgFnPc1d1	národnostní
menšiny	menšina	k1gFnPc1	menšina
představují	představovat	k5eAaImIp3nP	představovat
Bělorusové	Bělorus	k1gMnPc1	Bělorus
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Litevci	Litevec	k1gMnPc1	Litevec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Příznačná	příznačný	k2eAgFnSc1d1	příznačná
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
porodnost	porodnost	k1gFnSc1	porodnost
(	(	kIx(	(
<g/>
1,17	[number]	k4	1,17
dítěte	dítě	k1gNnSc2	dítě
na	na	k7c4	na
matku	matka	k1gFnSc4	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyplývající	vyplývající	k2eAgInSc4d1	vyplývající
rychlý	rychlý	k2eAgInSc4d1	rychlý
úbytek	úbytek	k1gInSc4	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
2011	[number]	k4	2011
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
snížil	snížit	k5eAaPmAgInS	snížit
o	o	k7c4	o
alarmujících	alarmující	k2eAgInPc2d1	alarmující
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
(	(	kIx(	(
<g/>
22,5	[number]	k4	22,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
poklesu	pokles	k1gInSc6	pokles
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
projevuje	projevovat	k5eAaImIp3nS	projevovat
také	také	k9	také
emigrace	emigrace	k1gFnSc1	emigrace
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
mezi	mezi	k7c4	mezi
její	její	k3xOp3gInPc4	její
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
cíle	cíl	k1gInPc4	cíl
patří	patřit	k5eAaImIp3nS	patřit
Irsko	Irsko	k1gNnSc1	Irsko
(	(	kIx(	(
<g/>
48	[number]	k4	48
031	[number]	k4	031
Lotyšů	Lotyš	k1gMnPc2	Lotyš
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
(	(	kIx(	(
<g/>
39	[number]	k4	39
000	[number]	k4	000
Lotyšů	Lotyš	k1gMnPc2	Lotyš
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
neexistuje	existovat	k5eNaImIp3nS	existovat
žádné	žádný	k3yNgNnSc1	žádný
výrazně	výrazně	k6eAd1	výrazně
dominantní	dominantní	k2eAgNnSc1d1	dominantní
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
zastoupení	zastoupení	k1gNnSc1	zastoupení
(	(	kIx(	(
<g/>
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
vztahují	vztahovat	k5eAaImIp3nP	vztahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
luteráni	luterán	k1gMnPc1	luterán
(	(	kIx(	(
<g/>
cca	cca	kA	cca
556	[number]	k4	556
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
(	(	kIx(	(
<g/>
430	[number]	k4	430
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravoslavní	pravoslavný	k2eAgMnPc1d1	pravoslavný
(	(	kIx(	(
<g/>
350	[number]	k4	350
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
carském	carský	k2eAgNnSc6d1	carské
Rusku	Rusko	k1gNnSc6	Rusko
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Lotyšska	Lotyšsko	k1gNnSc2	Lotyšsko
žilo	žít	k5eAaImAgNnS	žít
68.3	[number]	k4	68.3
<g/>
%	%	kIx~	%
Lotyšů	Lotyš	k1gMnPc2	Lotyš
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
%	%	kIx~	%
Rusů	Rus	k1gMnPc2	Rus
<g/>
,	,	kIx,	,
7.4	[number]	k4	7.4
<g/>
%	%	kIx~	%
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
6.2	[number]	k4	6.2
<g/>
%	%	kIx~	%
baltských	baltský	k2eAgMnPc2d1	baltský
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
3.4	[number]	k4	3.4
<g/>
%	%	kIx~	%
Poláků	Polák	k1gMnPc2	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
Rainis	Rainis	k1gFnSc1	Rainis
<g/>
.	.	kIx.	.
</s>
<s>
Aleksandrs	Aleksandrs	k6eAd1	Aleksandrs
Čaks	Čaks	k1gInSc1	Čaks
je	být	k5eAaImIp3nS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k9	jako
první	první	k4xOgInSc4	první
"	"	kIx"	"
<g/>
městský	městský	k2eAgInSc4d1	městský
<g/>
"	"	kIx"	"
lotyšský	lotyšský	k2eAgMnSc1d1	lotyšský
básník	básník	k1gMnSc1	básník
<g/>
.	.	kIx.	.
</s>
<s>
Krišjā	Krišjā	k?	Krišjā
Barons	Barons	k1gInSc1	Barons
sesbíral	sesbírat	k5eAaPmAgMnS	sesbírat
stovky	stovka	k1gFnPc4	stovka
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
prozaikem	prozaik	k1gMnSc7	prozaik
je	být	k5eAaImIp3nS	být
Rū	Rū	k1gMnSc1	Rū
Blaumanis	Blaumanis	k1gFnSc2	Blaumanis
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřekládanějším	překládaný	k2eAgMnSc7d3	nejpřekládanější
lotyšským	lotyšský	k2eAgMnSc7d1	lotyšský
autorem	autor	k1gMnSc7	autor
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
je	být	k5eAaImIp3nS	být
Vilis	Vilis	k1gFnSc1	Vilis
Lā	Lā	k1gFnSc1	Lā
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
spisovatelkou	spisovatelka	k1gFnSc7	spisovatelka
Aspazija	Aspazij	k1gInSc2	Aspazij
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
výtvarnými	výtvarný	k2eAgMnPc7d1	výtvarný
umělci	umělec	k1gMnPc7	umělec
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
sochař	sochař	k1gMnSc1	sochař
Kā	Kā	k1gMnSc1	Kā
Zā	Zā	k1gMnSc1	Zā
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Vilhelms	Vilhelmsa	k1gFnPc2	Vilhelmsa
Purvī	Purvī	k1gMnSc1	Purvī
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
Philippe	Philipp	k1gInSc5	Philipp
Halsman	Halsman	k1gMnSc1	Halsman
<g/>
,	,	kIx,	,
emigrant	emigrant	k1gMnSc1	emigrant
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgMnPc2d3	nejslavnější
baletních	baletní	k2eAgMnPc2d1	baletní
tanečníků	tanečník	k1gMnPc2	tanečník
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rodák	rodák	k1gMnSc1	rodák
z	z	k7c2	z
Rigy	Riga	k1gFnSc2	Riga
Michail	Michail	k1gMnSc1	Michail
Baryšnikov	Baryšnikov	k1gInSc1	Baryšnikov
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
legendární	legendární	k2eAgMnSc1d1	legendární
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
Sergej	Sergej	k1gMnSc1	Sergej
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Ejzenštejn	Ejzenštejn	k1gMnSc1	Ejzenštejn
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
lotyšském	lotyšský	k2eAgNnSc6d1	lotyšské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Klasiky	klasika	k1gFnPc1	klasika
lotyšské	lotyšský	k2eAgFnSc2d1	lotyšská
hudby	hudba	k1gFnSc2	hudba
jsou	být	k5eAaImIp3nP	být
Jā	Jā	k1gMnSc1	Jā
Vī	Vī	k1gMnSc1	Vī
<g/>
,	,	kIx,	,
Alfrē	Alfrē	k1gMnSc1	Alfrē
Kalniņ	Kalniņ	k1gMnSc1	Kalniņ
<g/>
,	,	kIx,	,
Kā	Kā	k1gMnSc1	Kā
Baumanis	Baumanis	k1gFnSc2	Baumanis
a	a	k8xC	a
Emī	Emī	k1gFnSc2	Emī
Dā	Dā	k1gFnSc2	Dā
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgMnPc7d1	významný
hudebními	hudební	k2eAgMnPc7d1	hudební
skladateli	skladatel	k1gMnPc7	skladatel
současnosti	současnost	k1gFnSc2	současnost
jsou	být	k5eAaImIp3nP	být
Raimonds	Raimonds	k1gInSc4	Raimonds
Pauls	Paulsa	k1gFnPc2	Paulsa
a	a	k8xC	a
Pē	Pē	k1gFnPc2	Pē
Vasks	Vasksa	k1gFnPc2	Vasksa
<g/>
.	.	kIx.	.
</s>
<s>
Slavným	slavný	k2eAgMnSc7d1	slavný
houslistou	houslista	k1gMnSc7	houslista
a	a	k8xC	a
dirigentem	dirigent	k1gMnSc7	dirigent
byl	být	k5eAaImAgMnS	být
Gidon	Gidon	k1gMnSc1	Gidon
Kremer	Kremer	k1gMnSc1	Kremer
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dirigent	dirigent	k1gMnSc1	dirigent
získal	získat	k5eAaPmAgMnS	získat
věhlas	věhlas	k1gInSc4	věhlas
rovněž	rovněž	k9	rovněž
Mariss	Mariss	k1gInSc4	Mariss
Jansons	Jansonsa	k1gFnPc2	Jansonsa
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Elī	Elī	k1gFnSc1	Elī
Garanča	Garanča	k1gFnSc1	Garanča
<g/>
.	.	kIx.	.
</s>
<s>
Elektronický	elektronický	k2eAgMnSc1d1	elektronický
hudebník	hudebník	k1gMnSc1	hudebník
DJ	DJ	kA	DJ
Lethal	Lethal	k1gMnSc1	Lethal
(	(	kIx(	(
<g/>
Leors	Leors	k1gInSc1	Leors
Dimants	Dimantsa	k1gFnPc2	Dimantsa
<g/>
)	)	kIx)	)
proslul	proslout	k5eAaPmAgMnS	proslout
svou	svůj	k3xOyFgFnSc7	svůj
spoluprací	spolupráce	k1gFnSc7	spolupráce
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ostwald	Ostwald	k1gMnSc1	Ostwald
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
průkopník	průkopník	k1gMnSc1	průkopník
raketové	raketový	k2eAgFnSc2d1	raketová
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
Friedrich	Friedrich	k1gMnSc1	Friedrich
Zander	Zander	k1gMnSc1	Zander
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Waldenův	Waldenův	k2eAgInSc4d1	Waldenův
zvrat	zvrat	k1gInSc4	zvrat
objevil	objevit	k5eAaPmAgMnS	objevit
chemik	chemik	k1gMnSc1	chemik
Paul	Paul	k1gMnSc1	Paul
Walden	Waldno	k1gNnPc2	Waldno
<g/>
.	.	kIx.	.
</s>
<s>
Prestižní	prestižní	k2eAgFnSc4d1	prestižní
Turingovu	Turingův	k2eAgFnSc4d1	Turingova
cenu	cena	k1gFnSc4	cena
získal	získat	k5eAaPmAgMnS	získat
informatik	informatik	k1gMnSc1	informatik
Juris	Juris	k1gFnSc2	Juris
Hartmanis	Hartmanis	k1gFnSc2	Hartmanis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sociálních	sociální	k2eAgFnPc6d1	sociální
a	a	k8xC	a
humanitních	humanitní	k2eAgFnPc6d1	humanitní
vědách	věda	k1gFnPc6	věda
vynikl	vyniknout	k5eAaPmAgMnS	vyniknout
rižský	rižský	k2eAgMnSc1d1	rižský
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
politický	politický	k2eAgMnSc1d1	politický
filozof	filozof	k1gMnSc1	filozof
Isaiah	Isaiaha	k1gFnPc2	Isaiaha
Berlin	berlina	k1gFnPc2	berlina
a	a	k8xC	a
také	také	k9	také
sociolingvista	sociolingvista	k1gMnSc1	sociolingvista
Max	Max	k1gMnSc1	Max
Weinreich	Weinreich	k1gMnSc1	Weinreich
<g/>
.	.	kIx.	.
</s>
<s>
Cyklista	cyklista	k1gMnSc1	cyklista
Mā	Mā	k1gFnSc2	Mā
Štrombergs	Štrombergs	k1gInSc1	Štrombergs
vybojoval	vybojovat	k5eAaPmAgInS	vybojovat
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatý	k2eAgFnPc4d1	zlatá
olympijské	olympijský	k2eAgFnPc4d1	olympijská
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
olympijské	olympijský	k2eAgNnSc4d1	Olympijské
zlato	zlato	k1gNnSc4	zlato
pro	pro	k7c4	pro
samostatné	samostatný	k2eAgNnSc4d1	samostatné
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
vybojoval	vybojovat	k5eAaPmAgMnS	vybojovat
gymnasta	gymnasta	k1gMnSc1	gymnasta
Igors	Igorsa	k1gFnPc2	Igorsa
Vihrovs	Vihrovsa	k1gFnPc2	Vihrovsa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
časů	čas	k1gInPc2	čas
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
olympiádu	olympiáda	k1gFnSc4	olympiáda
oštěpařka	oštěpařka	k1gFnSc1	oštěpařka
Inese	Inese	k1gFnSc1	Inese
Jaunzeme	Jaunzeme	k1gFnSc1	Jaunzeme
či	či	k8xC	či
střelec	střelec	k1gMnSc1	střelec
Afanasijs	Afanasijsa	k1gFnPc2	Afanasijsa
Kuzmins	Kuzminsa	k1gFnPc2	Kuzminsa
<g/>
.	.	kIx.	.
</s>
<s>
Dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
z	z	k7c2	z
časů	čas	k1gInPc2	čas
SSSR	SSSR	kA	SSSR
je	být	k5eAaImIp3nS	být
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
Artū	Artū	k1gFnSc2	Artū
Irbe	Irb	k1gFnSc2	Irb
<g/>
.	.	kIx.	.
</s>
<s>
Šachistovi	šachista	k1gMnSc3	šachista
Michailu	Michail	k1gMnSc3	Michail
Talovi	Tala	k1gMnSc3	Tala
se	se	k3xPyFc4	se
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
Čaroděj	čaroděj	k1gMnSc1	čaroděj
z	z	k7c2	z
Rigy	Riga	k1gFnSc2	Riga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rize	Riga	k1gFnSc6	Riga
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
i	i	k9	i
představitel	představitel	k1gMnSc1	představitel
šachové	šachový	k2eAgFnSc2d1	šachová
moderny	moderna	k1gFnSc2	moderna
Aaron	Aaron	k1gMnSc1	Aaron
Nimcovič	Nimcovič	k1gMnSc1	Nimcovič
či	či	k8xC	či
šachista	šachista	k1gMnSc1	šachista
Alexej	Alexej	k1gMnSc1	Alexej
Širov	Širovo	k1gNnPc2	Širovo
<g/>
.	.	kIx.	.
</s>
