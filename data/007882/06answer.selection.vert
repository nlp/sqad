<s>
V	v	k7c6	v
Lotyšsku	Lotyšsko	k1gNnSc6	Lotyšsko
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
největšími	veliký	k2eAgFnPc7d3	veliký
řekami	řeka	k1gFnPc7	řeka
jsou	být	k5eAaImIp3nP	být
Daugava	Daugava	k1gFnSc1	Daugava
<g/>
,	,	kIx,	,
Venta	Venta	k1gFnSc1	Venta
a	a	k8xC	a
Gauja	Gauja	k1gFnSc1	Gauja
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
Lubanas	Lubanasa	k1gFnPc2	Lubanasa
a	a	k8xC	a
Rā	Rā	k1gFnPc2	Rā
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
bažin	bažina	k1gFnPc2	bažina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
10	[number]	k4	10
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
