<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejznámějším	známý	k2eAgInSc7d3	nejznámější
symbolem	symbol	k1gInSc7	symbol
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
