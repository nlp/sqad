<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nejznámějším	známý	k2eAgInSc7d3	nejznámější
symbolem	symbol	k1gInSc7	symbol
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Jaltské	jaltský	k2eAgFnSc2d1	Jaltská
dohody	dohoda	k1gFnSc2	dohoda
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgFnPc2	čtyři
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
pak	pak	k6eAd1	pak
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
sektorů	sektor	k1gInPc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
napětí	napětí	k1gNnSc2	napětí
mezi	mezi	k7c7	mezi
mocenskými	mocenský	k2eAgInPc7d1	mocenský
bloky	blok	k1gInPc7	blok
<g/>
:	:	kIx,	:
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
blokádě	blokáda	k1gFnSc3	blokáda
západních	západní	k2eAgInPc2d1	západní
sektorů	sektor	k1gInPc2	sektor
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc4d1	trvající
téměř	téměř	k6eAd1	téměř
jeden	jeden	k4xCgInSc1	jeden
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
západních	západní	k2eAgFnPc2d1	západní
okupačních	okupační	k2eAgFnPc2d1	okupační
zón	zóna	k1gFnPc2	zóna
založena	založit	k5eAaPmNgFnS	založit
Spolková	spolkový	k2eAgFnSc1d1	spolková
republika	republika	k1gFnSc1	republika
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1949	[number]	k4	1949
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
SRN	srna	k1gFnPc2	srna
založena	založit	k5eAaPmNgFnS	založit
Německá	německý	k2eAgFnSc1d1	německá
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
sovětský	sovětský	k2eAgInSc1d1	sovětský
sektor	sektor	k1gInSc1	sektor
Berlína	Berlín	k1gInSc2	Berlín
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gNnSc7	její
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgFnPc7	tento
událostmi	událost	k1gFnPc7	událost
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
města	město	k1gNnSc2	město
fixováno	fixován	k2eAgNnSc4d1	fixováno
politicky	politicky	k6eAd1	politicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
platil	platit	k5eAaImAgInS	platit
i	i	k9	i
nadále	nadále	k6eAd1	nadále
čtyřmocenský	čtyřmocenský	k2eAgInSc4d1	čtyřmocenský
status	status	k1gInSc4	status
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
nemělo	mít	k5eNaImAgNnS	mít
žádného	žádný	k3yNgInSc2	žádný
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
byl	být	k5eAaImAgInS	být
novou	nova	k1gFnSc7	nova
SRN	SRN	kA	SRN
jen	jen	k6eAd1	jen
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
výjimkami	výjimka	k1gFnPc7	výjimka
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
spolkovou	spolkový	k2eAgFnSc4d1	spolková
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
zde	zde	k6eAd1	zde
hrála	hrát	k5eAaImAgFnS	hrát
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
až	až	k9	až
1961	[number]	k4	1961
území	území	k1gNnPc2	území
NDR	NDR	kA	NDR
opustily	opustit	k5eAaPmAgInP	opustit
téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgInPc1	tři
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
odborníci	odborník	k1gMnPc1	odborník
s	s	k7c7	s
dobrým	dobrý	k2eAgNnSc7d1	dobré
vzděláním	vzdělání	k1gNnSc7	vzdělání
<g/>
;	;	kIx,	;
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
opustit	opustit	k5eAaPmF	opustit
NDR	NDR	kA	NDR
přes	přes	k7c4	přes
Berlín	Berlín	k1gInSc4	Berlín
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hranice	hranice	k1gFnSc1	hranice
probíhala	probíhat	k5eAaImAgFnS	probíhat
středem	středem	k7c2	středem
města	město	k1gNnSc2	město
a	a	k8xC	a
nebyla	být	k5eNaImAgNnP	být
tak	tak	k6eAd1	tak
snadno	snadno	k6eAd1	snadno
kontrolovatelná	kontrolovatelný	k2eAgFnSc1d1	kontrolovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInPc1d1	státní
a	a	k8xC	a
stranické	stranický	k2eAgInPc1d1	stranický
orgány	orgán	k1gInPc1	orgán
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
dojmem	dojem	k1gInSc7	dojem
rozhodly	rozhodnout	k5eAaPmAgFnP	rozhodnout
provést	provést	k5eAaPmF	provést
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
měla	mít	k5eAaImAgFnS	mít
nekontrolovatelným	kontrolovatelný	k2eNgInPc3d1	nekontrolovatelný
útěkům	útěk	k1gInPc3	útěk
občanů	občan	k1gMnPc2	občan
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Walter	Walter	k1gMnSc1	Walter
Ulbricht	Ulbricht	k1gMnSc1	Ulbricht
původně	původně	k6eAd1	původně
od	od	k7c2	od
Sovětů	Sovět	k1gMnPc2	Sovět
požadoval	požadovat	k5eAaImAgMnS	požadovat
obsazení	obsazení	k1gNnSc4	obsazení
západních	západní	k2eAgInPc2d1	západní
sektorů	sektor	k1gInPc2	sektor
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
Chruščov	Chruščov	k1gInSc1	Chruščov
rozhodně	rozhodně	k6eAd1	rozhodně
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
nepřál	přát	k5eNaImAgMnS	přát
konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zbývala	zbývat	k5eAaImAgFnS	zbývat
tedy	tedy	k9	tedy
jediná	jediný	k2eAgFnSc1d1	jediná
možnost	možnost	k1gFnSc1	možnost
–	–	k?	–
ohraničit	ohraničit	k5eAaPmF	ohraničit
západní	západní	k2eAgInSc4d1	západní
sektory	sektor	k1gInPc4	sektor
Berlína	Berlín	k1gInSc2	Berlín
bariérou	bariéra	k1gFnSc7	bariéra
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1961	[number]	k4	1961
dementoval	dementovat	k5eAaBmAgMnS	dementovat
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
Walter	Walter	k1gMnSc1	Walter
Ulbricht	Ulbricht	k1gMnSc1	Ulbricht
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
úmysly	úmysl	k1gInPc7	úmysl
hranici	hranice	k1gFnSc4	hranice
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
především	především	k6eAd1	především
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
N.	N.	kA	N.
S.	S.	kA	S.
Chruščova	Chruščův	k2eAgFnSc1d1	Chruščova
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
představitelů	představitel	k1gMnPc2	představitel
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
3	[number]	k4	3
<g/>
.	.	kIx.	.
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
schválila	schválit	k5eAaPmAgFnS	schválit
Lidová	lidový	k2eAgFnSc1d1	lidová
komora	komora	k1gFnSc1	komora
NDR	NDR	kA	NDR
moskevská	moskevský	k2eAgNnPc1d1	moskevské
usnesení	usnesení	k1gNnSc4	usnesení
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
ministerská	ministerský	k2eAgFnSc1d1	ministerská
rada	rada	k1gFnSc1	rada
NDR	NDR	kA	NDR
usnesení	usnesení	k1gNnSc2	usnesení
o	o	k7c4	o
nasazení	nasazení	k1gNnSc4	nasazení
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
země	zem	k1gFnSc2	zem
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
a	a	k8xC	a
o	o	k7c4	o
jejich	jejich	k3xOp3gNnSc4	jejich
uzavření	uzavření	k1gNnSc4	uzavření
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
zpravodajské	zpravodajský	k2eAgFnPc1d1	zpravodajská
služby	služba	k1gFnPc1	služba
byly	být	k5eAaImAgFnP	být
o	o	k7c6	o
plánech	plán	k1gInPc6	plán
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
informovány	informován	k2eAgInPc1d1	informován
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jejich	jejich	k3xOp3gFnSc2	jejich
vlády	vláda	k1gFnSc2	vláda
nijak	nijak	k6eAd1	nijak
nezasáhly	zasáhnout	k5eNaPmAgInP	zasáhnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
stavba	stavba	k1gFnSc1	stavba
zdi	zeď	k1gFnSc2	zeď
vlastně	vlastně	k9	vlastně
jen	jen	k6eAd1	jen
zakonzervovala	zakonzervovat	k5eAaPmAgFnS	zakonzervovat
status	status	k1gInSc4	status
quo	quo	k?	quo
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
oběma	dva	k4xCgMnPc7	dva
blokům	blok	k1gInPc3	blok
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
ozbrojenému	ozbrojený	k2eAgInSc3d1	ozbrojený
konfliktu	konflikt	k1gInSc3	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
na	na	k7c4	na
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
obsadily	obsadit	k5eAaPmAgFnP	obsadit
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
NDR	NDR	kA	NDR
(	(	kIx(	(
<g/>
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
policie	policie	k1gFnSc1	policie
<g/>
,	,	kIx,	,
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
stráž	stráž	k1gFnSc1	stráž
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
podnikových	podnikový	k2eAgFnPc2d1	podniková
milicí	milice	k1gFnPc2	milice
<g/>
)	)	kIx)	)
hranice	hranice	k1gFnSc1	hranice
k	k	k7c3	k
Západnímu	západní	k2eAgInSc3d1	západní
Berlínu	Berlín	k1gInSc3	Berlín
a	a	k8xC	a
přerušily	přerušit	k5eAaPmAgInP	přerušit
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
ostnatého	ostnatý	k2eAgInSc2d1	ostnatý
drátu	drát	k1gInSc2	drát
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
východním	východní	k2eAgInSc7d1	východní
a	a	k8xC	a
Západním	západní	k2eAgInSc7d1	západní
Berlínem	Berlín	k1gInSc7	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
reakce	reakce	k1gFnSc2	reakce
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
drát	drát	k1gInSc1	drát
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
zdí	zeď	k1gFnSc7	zeď
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštích	příští	k2eAgInPc6d1	příští
měsících	měsíc	k1gInPc6	měsíc
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
opevnění	opevnění	k1gNnSc2	opevnění
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c4	mezi
NDR	NDR	kA	NDR
a	a	k8xC	a
SRN	SRN	kA	SRN
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
nepropustnost	nepropustnost	k1gFnSc4	nepropustnost
hranic	hranice	k1gFnPc2	hranice
mezi	mezi	k7c7	mezi
státy	stát	k1gInPc7	stát
Východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
okolními	okolní	k2eAgInPc7d1	okolní
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hranice	hranice	k1gFnPc1	hranice
se	se	k3xPyFc4	se
i	i	k9	i
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
železná	železný	k2eAgFnSc1d1	železná
opona	opona	k1gFnSc1	opona
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
posestávala	posestávat	k5eAaImAgFnS	posestávat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgFnSc1d1	vlastní
zeď	zeď	k1gFnSc1	zeď
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ji	on	k3xPp3gFnSc4	on
znala	znát	k5eAaImAgFnS	znát
západní	západní	k2eAgFnSc1d1	západní
veřejnost	veřejnost	k1gFnSc1	veřejnost
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
pár	pár	k4xCyI	pár
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
hranice	hranice	k1gFnSc2	hranice
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zeď	zeď	k1gFnSc1	zeď
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
betonových	betonový	k2eAgFnPc2d1	betonová
<g/>
,	,	kIx,	,
asi	asi	k9	asi
3,6	[number]	k4	3,6
metru	metr	k1gInSc2	metr
vysokých	vysoký	k2eAgInPc2d1	vysoký
panelů	panel	k1gInPc2	panel
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
nepravidelného	pravidelný	k2eNgNnSc2d1	nepravidelné
a	a	k8xC	a
převráceného	převrácený	k2eAgNnSc2d1	převrácené
písmene	písmeno	k1gNnSc2	písmeno
T.	T.	kA	T.
Přilehlý	přilehlý	k2eAgInSc4d1	přilehlý
pozemek	pozemek	k1gInSc4	pozemek
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
východnímu	východní	k2eAgInSc3d1	východní
Berlínu	Berlín	k1gInSc3	Berlín
<g/>
,	,	kIx,	,
udržovaly	udržovat	k5eAaImAgFnP	udržovat
hlídky	hlídka	k1gFnPc1	hlídka
v	v	k7c6	v
čistotě	čistota	k1gFnSc6	čistota
po	po	k7c6	po
průchodu	průchod	k1gInSc6	průchod
kovovými	kovový	k2eAgFnPc7d1	kovová
dveřmi	dveře	k1gFnPc7	dveře
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
panelech	panel	k1gInPc6	panel
nacházely	nacházet	k5eAaImAgFnP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
zdi	zeď	k1gFnSc2	zeď
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nacházelo	nacházet	k5eAaImAgNnS	nacházet
rozdílně	rozdílně	k6eAd1	rozdílně
široké	široký	k2eAgNnSc4d1	široké
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
ochranných	ochranný	k2eAgInPc2d1	ochranný
pásů	pás	k1gInPc2	pás
<g/>
:	:	kIx,	:
ostnatý	ostnatý	k2eAgInSc1d1	ostnatý
drát	drát	k1gInSc1	drát
<g/>
,	,	kIx,	,
pás	pás	k1gInSc1	pás
pro	pro	k7c4	pro
psy	pes	k1gMnPc4	pes
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pohraniční	pohraniční	k2eAgFnSc4d1	pohraniční
stráž	stráž	k1gFnSc4	stráž
<g/>
,	,	kIx,	,
protipěchotní	protipěchotní	k2eAgFnPc1d1	protipěchotní
překážky	překážka	k1gFnPc1	překážka
i	i	k8xC	i
protitankové	protitankový	k2eAgInPc1d1	protitankový
zátarasy	zátaras	k1gInPc1	zátaras
<g/>
,	,	kIx,	,
drátěná	drátěný	k2eAgFnSc1d1	drátěná
signální	signální	k2eAgFnSc1d1	signální
stěna	stěna	k1gFnSc1	stěna
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
další	další	k2eAgFnSc1d1	další
stěna	stěna	k1gFnSc1	stěna
z	z	k7c2	z
betonových	betonový	k2eAgInPc2d1	betonový
prefabrikátů	prefabrikát	k1gInPc2	prefabrikát
tvořila	tvořit	k5eAaImAgFnS	tvořit
vnější	vnější	k2eAgInSc4d1	vnější
okraj	okraj	k1gInSc4	okraj
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
pásma	pásmo	k1gNnSc2	pásmo
mezi	mezi	k7c7	mezi
zdmi	zeď	k1gFnPc7	zeď
stály	stát	k5eAaImAgFnP	stát
v	v	k7c6	v
nepravidelných	pravidelný	k2eNgInPc6d1	nepravidelný
rozestupech	rozestup	k1gInPc6	rozestup
strážní	strážní	k2eAgFnSc2d1	strážní
věže	věž	k1gFnSc2	věž
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
betonových	betonový	k2eAgInPc2d1	betonový
prefabrikátů	prefabrikát	k1gInPc2	prefabrikát
<g/>
)	)	kIx)	)
a	a	k8xC	a
domky	domek	k1gInPc4	domek
pro	pro	k7c4	pro
pohotovostní	pohotovostní	k2eAgFnPc4d1	pohotovostní
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
zeď	zeď	k1gFnSc1	zeď
neustále	neustále	k6eAd1	neustále
modernizována	modernizovat	k5eAaBmNgFnS	modernizovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
čtyři	čtyři	k4xCgFnPc1	čtyři
generace	generace	k1gFnPc1	generace
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
stojících	stojící	k2eAgFnPc6d1	stojící
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
východoberlínské	východoberlínský	k2eAgFnSc6d1	východoberlínská
straně	strana	k1gFnSc6	strana
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
zazděny	zazděn	k2eAgFnPc4d1	zazděna
dveře	dveře	k1gFnPc4	dveře
a	a	k8xC	a
okna	okno	k1gNnPc4	okno
směřující	směřující	k2eAgNnPc4d1	směřující
k	k	k7c3	k
západním	západní	k2eAgInPc3d1	západní
sektorům	sektor	k1gInPc3	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byli	být	k5eAaImAgMnP	být
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vystěhováni	vystěhován	k2eAgMnPc1d1	vystěhován
a	a	k8xC	a
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
strženy	stržen	k2eAgInPc1d1	stržen
<g/>
.	.	kIx.	.
</s>
<s>
Zeď	zeď	k1gFnSc1	zeď
měla	mít	k5eAaImAgFnS	mít
celkovou	celkový	k2eAgFnSc4d1	celková
délku	délka	k1gFnSc4	délka
165	[number]	k4	165
km	km	kA	km
(	(	kIx(	(
<g/>
45	[number]	k4	45
km	km	kA	km
s	s	k7c7	s
hranicí	hranice	k1gFnSc7	hranice
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
120	[number]	k4	120
km	km	kA	km
čítala	čítat	k5eAaImAgFnS	čítat
hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
Západním	západní	k2eAgInSc7d1	západní
Berlínem	Berlín	k1gInSc7	Berlín
a	a	k8xC	a
Braniborskem	Braniborsko	k1gNnSc7	Braniborsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Památná	památný	k2eAgFnSc1d1	památná
je	být	k5eAaImIp3nS	být
návštěva	návštěva	k1gFnSc1	návštěva
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
J.	J.	kA	J.
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
svou	svůj	k3xOyFgFnSc4	svůj
řeč	řeč	k1gFnSc4	řeč
ukončil	ukončit	k5eAaPmAgInS	ukončit
německy	německy	k6eAd1	německy
pronesenou	pronesený	k2eAgFnSc7d1	pronesená
větou	věta	k1gFnSc7	věta
"	"	kIx"	"
<g/>
Ich	Ich	k1gFnSc1	Ich
bin	bin	k?	bin
ein	ein	k?	ein
Berliner	Berliner	k1gInSc1	Berliner
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
Berlíňan	Berlíňan	k1gMnSc1	Berlíňan
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
příštích	příští	k2eAgInPc2d1	příští
dnů	den	k1gInPc2	den
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dramatickým	dramatický	k2eAgInPc3d1	dramatický
pokusům	pokus	k1gInPc3	pokus
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
z	z	k7c2	z
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zeď	zeď	k1gFnSc1	zeď
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nebo	nebo	k8xC	nebo
chráněná	chráněný	k2eAgFnSc1d1	chráněná
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
stály	stát	k5eAaImAgInP	stát
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
zdi	zeď	k1gFnSc2	zeď
a	a	k8xC	a
kde	kde	k6eAd1	kde
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgNnP	být
zabetonována	zabetonován	k2eAgNnPc1d1	zabetonováno
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stovkám	stovka	k1gFnPc3	stovka
občanů	občan	k1gMnPc2	občan
podařil	podařit	k5eAaPmAgInS	podařit
útěk	útěk	k1gInSc4	útěk
do	do	k7c2	do
západních	západní	k2eAgInPc2d1	západní
sektorů	sektor	k1gInPc2	sektor
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
nasazením	nasazení	k1gNnSc7	nasazení
vlastního	vlastní	k2eAgInSc2d1	vlastní
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Povel	povel	k1gInSc1	povel
k	k	k7c3	k
použití	použití	k1gNnSc3	použití
střelné	střelný	k2eAgFnSc2d1	střelná
zbraně	zbraň	k1gFnSc2	zbraň
proti	proti	k7c3	proti
uprchlíkům	uprchlík	k1gMnPc3	uprchlík
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
východoněmecké	východoněmecký	k2eAgFnSc2d1	východoněmecká
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stráže	stráž	k1gFnSc2	stráž
sice	sice	k8xC	sice
existoval	existovat	k5eAaImAgInS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
nasazení	nasazení	k1gNnSc3	nasazení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
několik	několik	k4yIc1	několik
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
známému	známý	k2eAgInSc3d1	známý
případu	případ	k1gInSc3	případ
zastřelení	zastřelení	k1gNnSc4	zastřelení
utečence	utečenec	k1gMnSc2	utečenec
došlo	dojít	k5eAaPmAgNnS	dojít
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
k	k	k7c3	k
poslednímu	poslední	k2eAgNnSc3d1	poslední
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
28	[number]	k4	28
<g/>
leté	letý	k2eAgFnSc2d1	letá
existence	existence	k1gFnSc2	existence
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
zde	zde	k6eAd1	zde
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
útěk	útěk	k1gInSc4	útěk
zemřel	zemřít	k5eAaPmAgInS	zemřít
značný	značný	k2eAgInSc1d1	značný
počet	počet	k1gInSc1	počet
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vykrvácení	vykrvácení	k1gNnSc2	vykrvácení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
východoněmecká	východoněmecký	k2eAgFnSc1d1	východoněmecká
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
stráž	stráž	k1gFnSc1	stráž
nechala	nechat	k5eAaPmAgFnS	nechat
zraněné	zraněný	k1gMnPc4	zraněný
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
strážních	strážní	k2eAgInPc2d1	strážní
pásů	pás	k1gInPc2	pás
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
ležet	ležet	k5eAaImF	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dnešních	dnešní	k2eAgInPc2d1	dnešní
odhadů	odhad	k1gInPc2	odhad
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
nepovolené	povolený	k2eNgNnSc4d1	nepovolené
opuštění	opuštění	k1gNnSc4	opuštění
republiky	republika	k1gFnSc2	republika
nebo	nebo	k8xC	nebo
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
něj	on	k3xPp3gNnSc4	on
(	(	kIx(	(
<g/>
§	§	k?	§
213	[number]	k4	213
trestního	trestní	k2eAgInSc2d1	trestní
zákoníka	zákoník	k1gMnSc2	zákoník
NDR	NDR	kA	NDR
<g/>
)	)	kIx)	)
odsouzeno	odsoudit	k5eAaPmNgNnS	odsoudit
kolem	kolem	k6eAd1	kolem
75	[number]	k4	75
000	[number]	k4	000
občanů	občan	k1gMnPc2	občan
s	s	k7c7	s
tresty	trest	k1gInPc7	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
těžších	těžký	k2eAgInPc6d2	těžší
případech	případ	k1gInPc6	případ
do	do	k7c2	do
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
problematické	problematický	k2eAgFnSc2d1	problematická
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
používání	používání	k1gNnSc1	používání
hraničních	hraniční	k2eAgInPc2d1	hraniční
přechodů	přechod	k1gInPc2	přechod
mezi	mezi	k7c7	mezi
Západním	západní	k2eAgInSc7d1	západní
Berlínem	Berlín	k1gInSc7	Berlín
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
východním	východní	k2eAgInSc7d1	východní
Berlínem	Berlín	k1gInSc7	Berlín
resp.	resp.	kA	resp.
NDR	NDR	kA	NDR
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Přechodů	přechod	k1gInPc2	přechod
sice	sice	k8xC	sice
existovalo	existovat	k5eAaImAgNnS	existovat
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
užívání	užívání	k1gNnSc1	užívání
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
nejen	nejen	k6eAd1	nejen
účelem	účel	k1gInSc7	účel
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
tranzit	tranzit	k1gInSc1	tranzit
do	do	k7c2	do
SRN	SRN	kA	SRN
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
návštěva	návštěva	k1gFnSc1	návštěva
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
"	"	kIx"	"
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
občanstvím	občanství	k1gNnSc7	občanství
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
těchto	tento	k3xDgInPc2	tento
přechodů	přechod	k1gInPc2	přechod
směly	smět	k5eAaImAgInP	smět
užívat	užívat	k5eAaImF	užívat
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
na	na	k7c4	na
nečetné	četný	k2eNgFnPc4d1	nečetná
výjimky	výjimka	k1gFnPc4	výjimka
(	(	kIx(	(
<g/>
přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
Friedrichstraße	Friedrichstraße	k1gFnSc4	Friedrichstraße
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
pěšky	pěšky	k6eAd1	pěšky
a	a	k8xC	a
také	také	k9	také
ne	ne	k9	ne
autem	aut	k1gInSc7	aut
<g/>
)	)	kIx)	)
tato	tento	k3xDgFnSc1	tento
omezení	omezení	k1gNnSc3	omezení
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
-li	i	k?	-li
občan	občan	k1gMnSc1	občan
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
návštěvu	návštěva	k1gFnSc4	návštěva
ze	z	k7c2	z
SRN	srna	k1gFnPc2	srna
či	či	k8xC	či
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
-li	i	k?	-li
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
navštívit	navštívit	k5eAaPmF	navštívit
východní	východní	k2eAgInSc1d1	východní
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
přechodů	přechod	k1gInPc2	přechod
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
setkat	setkat	k5eAaPmF	setkat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
počtu	počet	k1gInSc6	počet
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
překročení	překročení	k1gNnSc6	překročení
zdi	zeď	k1gFnSc2	zeď
přišly	přijít	k5eAaPmAgFnP	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vedly	vést	k5eAaImAgFnP	vést
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
a	a	k8xC	a
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
být	být	k5eAaImF	být
i	i	k9	i
v	v	k7c4	v
budoucnosti	budoucnost	k1gFnPc4	budoucnost
obtížné	obtížný	k2eAgNnSc1d1	obtížné
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jednoznačným	jednoznačný	k2eAgInPc3d1	jednoznačný
závěrům	závěr	k1gInPc3	závěr
<g/>
:	:	kIx,	:
v	v	k7c6	v
análech	anály	k1gInPc6	anály
NDR	NDR	kA	NDR
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
incidenty	incident	k1gInPc1	incident
neobjevují	objevovat	k5eNaImIp3nP	objevovat
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
téma	téma	k1gNnSc1	téma
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
stále	stále	k6eAd1	stále
kontroverzní	kontroverzní	k2eAgInSc1d1	kontroverzní
<g/>
.	.	kIx.	.
</s>
<s>
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
prokuratura	prokuratura	k1gFnSc1	prokuratura
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
udala	udat	k5eAaPmAgFnS	udat
počet	počet	k1gInSc1	počet
86	[number]	k4	86
prokazatelně	prokazatelně	k6eAd1	prokazatelně
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
<g/>
;	;	kIx,	;
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vychází	vycházet	k5eAaImIp3nS	vycházet
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
data	datum	k1gNnSc2	datum
stavby	stavba	k1gFnSc2	stavba
zdi	zeď	k1gFnSc2	zeď
<g/>
)	)	kIx)	)
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
238	[number]	k4	238
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiná	jiný	k2eAgNnPc4d1	jiné
kritéria	kritérion	k1gNnPc4	kritérion
používá	používat	k5eAaImIp3nS	používat
Ústřední	ústřední	k2eAgInSc1d1	ústřední
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
vládní	vládní	k2eAgFnSc4d1	vládní
a	a	k8xC	a
spolkovou	spolkový	k2eAgFnSc4d1	spolková
kriminalitu	kriminalita	k1gFnSc4	kriminalita
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
uvedl	uvést	k5eAaPmAgMnS	uvést
262	[number]	k4	262
obětí	oběť	k1gFnPc2	oběť
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
očividně	očividně	k6eAd1	očividně
o	o	k7c4	o
zcela	zcela	k6eAd1	zcela
různá	různý	k2eAgNnPc4d1	různé
kritéria	kritérion	k1gNnPc4	kritérion
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
<g/>
.	.	kIx.	.
</s>
<s>
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
poté	poté	k6eAd1	poté
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
svůj	svůj	k3xOyFgInSc4	svůj
odhad	odhad	k1gInSc4	odhad
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
190	[number]	k4	190
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
bádáním	bádání	k1gNnSc7	bádání
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
poli	pole	k1gNnSc6	pole
zabývá	zabývat	k5eAaImIp3nS	zabývat
i	i	k9	i
Ústředí	ústředí	k1gNnSc4	ústředí
pro	pro	k7c4	pro
soudobý	soudobý	k2eAgInSc4d1	soudobý
historický	historický	k2eAgInSc4d1	historický
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
uveřejnilo	uveřejnit	k5eAaPmAgNnS	uveřejnit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
stav	stav	k1gInSc4	stav
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
:	:	kIx,	:
z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
268	[number]	k4	268
evidovaných	evidovaný	k2eAgMnPc2d1	evidovaný
případů	případ	k1gInPc2	případ
lze	lze	k6eAd1	lze
125	[number]	k4	125
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
smrtelné	smrtelný	k2eAgInPc4d1	smrtelný
incidenty	incident	k1gInPc4	incident
<g/>
,	,	kIx,	,
62	[number]	k4	62
případů	případ	k1gInPc2	případ
lze	lze	k6eAd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
81	[number]	k4	81
případů	případ	k1gInPc2	případ
není	být	k5eNaImIp3nS	být
ještě	ještě	k6eAd1	ještě
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vyřešeno	vyřešit	k5eAaPmNgNnS	vyřešit
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
zveřejnění	zveřejnění	k1gNnPc1	zveřejnění
pokaždé	pokaždé	k6eAd1	pokaždé
zesílila	zesílit	k5eAaPmAgNnP	zesílit
i	i	k9	i
ve	v	k7c6	v
veřejnosti	veřejnost	k1gFnSc6	veřejnost
silnou	silný	k2eAgFnSc4d1	silná
kontroverzi	kontroverze	k1gFnSc4	kontroverze
o	o	k7c6	o
metodách	metoda	k1gFnPc6	metoda
a	a	k8xC	a
politickém	politický	k2eAgNnSc6d1	politické
pozadí	pozadí	k1gNnSc6	pozadí
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
o	o	k7c6	o
sympatiích	sympatie	k1gFnPc6	sympatie
či	či	k8xC	či
antipatiích	antipatie	k1gFnPc6	antipatie
a	a	k8xC	a
o	o	k7c6	o
vyrovnání	vyrovnání	k1gNnSc6	vyrovnání
se	se	k3xPyFc4	se
s	s	k7c7	s
minulostí	minulost	k1gFnSc7	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Nejasnými	jasný	k2eNgInPc7d1	nejasný
přístupy	přístup	k1gInPc7	přístup
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
choulostivému	choulostivý	k2eAgNnSc3d1	choulostivé
tématu	téma	k1gNnSc3	téma
byl	být	k5eAaImAgInS	být
zpečetěn	zpečetit	k5eAaPmNgInS	zpečetit
i	i	k9	i
pomník	pomník	k1gInSc4	pomník
obětem	oběť	k1gFnPc3	oběť
zdi	zeď	k1gFnSc2	zeď
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
zřízen	zřízen	k2eAgInSc1d1	zřízen
Pracovní	pracovní	k2eAgFnSc7d1	pracovní
skupinou	skupina	k1gFnSc7	skupina
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc4	srpen
na	na	k7c6	na
bývalém	bývalý	k2eAgInSc6d1	bývalý
přechodu	přechod	k1gInSc6	přechod
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
protestech	protest	k1gInPc6	protest
majitele	majitel	k1gMnSc4	majitel
pozemku	pozemek	k1gInSc2	pozemek
a	a	k8xC	a
po	po	k7c6	po
diskusích	diskuse	k1gFnPc6	diskuse
a	a	k8xC	a
kompromisních	kompromisní	k2eAgInPc6d1	kompromisní
návrzích	návrh	k1gInPc6	návrh
na	na	k7c4	na
alternativní	alternativní	k2eAgInSc4d1	alternativní
pomník	pomník	k1gInSc4	pomník
byl	být	k5eAaImAgMnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2005	[number]	k4	2005
odstraněn	odstranit	k5eAaPmNgInS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc4d1	nový
pomník	pomník	k1gInSc4	pomník
<g/>
,	,	kIx,	,
věnovaný	věnovaný	k2eAgInSc4d1	věnovaný
128	[number]	k4	128
ověřeným	ověřený	k2eAgFnPc3d1	ověřená
obětem	oběť	k1gFnPc3	oběť
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
byl	být	k5eAaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
památníku	památník	k1gInSc2	památník
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
na	na	k7c4	na
Bernauer	Bernauer	k1gInSc4	Bernauer
Straße	Straß	k1gFnSc2	Straß
<g/>
.	.	kIx.	.
</s>
<s>
Pomník	pomník	k1gInSc4	pomník
s	s	k7c7	s
názvem	název	k1gInSc7	název
Fenster	Fenstra	k1gFnPc2	Fenstra
des	des	k1gNnSc2	des
Gedenkens	Gedenkensa	k1gFnPc2	Gedenkensa
(	(	kIx(	(
<g/>
Okna	okno	k1gNnSc2	okno
památky	památka	k1gFnSc2	památka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
železnou	železný	k2eAgFnSc7d1	železná
zdí	zeď	k1gFnSc7	zeď
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
vsazeny	vsadit	k5eAaPmNgFnP	vsadit
skleněné	skleněný	k2eAgFnPc1d1	skleněná
tabulky	tabulka	k1gFnPc1	tabulka
s	s	k7c7	s
fotografií	fotografia	k1gFnSc7	fotografia
oběti	oběť	k1gFnSc2	oběť
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
známa	znám	k2eAgFnSc1d1	známa
podoba	podoba	k1gFnSc1	podoba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tabulka	tabulka	k1gFnSc1	tabulka
z	z	k7c2	z
neprůhledného	průhledný	k2eNgNnSc2d1	neprůhledné
skla	sklo	k1gNnSc2	sklo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
139	[number]	k4	139
tabulek	tabulka	k1gFnPc2	tabulka
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pomníku	pomník	k1gInSc6	pomník
se	se	k3xPyFc4	se
však	však	k9	však
nachází	nacházet	k5eAaImIp3nS	nacházet
162	[number]	k4	162
okének	okénko	k1gNnPc2	okénko
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
popřípadě	popřípadě	k6eAd1	popřípadě
zjištěná	zjištěný	k2eAgNnPc1d1	zjištěné
nová	nový	k2eAgNnPc1d1	nové
jména	jméno	k1gNnPc1	jméno
bude	být	k5eAaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
doplnit	doplnit	k5eAaPmF	doplnit
<g/>
.	.	kIx.	.
</s>
<s>
Pád	Pád	k1gInSc1	Pád
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
stejně	stejně	k6eAd1	stejně
překvapivě	překvapivě	k6eAd1	překvapivě
jako	jako	k8xC	jako
její	její	k3xOp3gFnSc1	její
stavba	stavba	k1gFnSc1	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
východoněmecké	východoněmecký	k2eAgNnSc1d1	východoněmecké
stranické	stranický	k2eAgNnSc1d1	stranické
vedení	vedení	k1gNnSc1	vedení
zabývalo	zabývat	k5eAaImAgNnS	zabývat
diskusemi	diskuse	k1gFnPc7	diskuse
o	o	k7c6	o
nutných	nutný	k2eAgFnPc6d1	nutná
změnách	změna	k1gFnPc6	změna
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
diskusí	diskuse	k1gFnPc2	diskuse
o	o	k7c6	o
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
přenášené	přenášený	k2eAgNnSc1d1	přenášené
televizí	televize	k1gFnSc7	televize
<g/>
,	,	kIx,	,
předčítal	předčítat	k5eAaImAgMnS	předčítat
člen	člen	k1gMnSc1	člen
politbyra	politbyro	k1gNnSc2	politbyro
<g/>
,	,	kIx,	,
Günter	Günter	k1gMnSc1	Günter
Schabowski	Schabowsk	k1gFnSc2	Schabowsk
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
sedmou	sedmý	k4xOgFnSc7	sedmý
hodinou	hodina	k1gFnSc7	hodina
večer	večer	k6eAd1	večer
jakoby	jakoby	k8xS	jakoby
mimochodem	mimochodem	k6eAd1	mimochodem
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
poznámek	poznámka	k1gFnPc2	poznámka
usnesení	usnesení	k1gNnSc2	usnesení
ministerské	ministerský	k2eAgFnSc2d1	ministerská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgInSc2	který
byly	být	k5eAaImAgFnP	být
cesty	cesta	k1gFnPc1	cesta
i	i	k9	i
do	do	k7c2	do
západního	západní	k2eAgNnSc2d1	západní
zahraničí	zahraničí	k1gNnSc2	zahraničí
povoleny	povolen	k2eAgInPc4d1	povolen
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
se	se	k3xPyFc4	se
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
pohraniční	pohraniční	k2eAgInPc4d1	pohraniční
přechody	přechod	k1gInPc4	přechod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
od	od	k7c2	od
kdy	kdy	k6eAd1	kdy
toto	tento	k3xDgNnSc4	tento
usnesení	usnesení	k1gNnSc4	usnesení
vstoupí	vstoupit	k5eAaPmIp3nP	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
<g/>
,	,	kIx,	,
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
Schabowski	Schabowske	k1gFnSc4	Schabowske
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
podle	podle	k7c2	podle
mého	můj	k3xOp1gInSc2	můj
názoru	názor	k1gInSc2	názor
ihned	ihned	k6eAd1	ihned
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
správné	správný	k2eAgNnSc1d1	správné
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Schabowski	Schabowske	k1gFnSc4	Schabowske
dostal	dostat	k5eAaPmAgMnS	dostat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
zmatku	zmatek	k1gInSc6	zmatek
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
nevšiml	všimnout	k5eNaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
stálo	stát	k5eAaImAgNnS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
od	od	k7c2	od
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příštích	příští	k2eAgFnPc6d1	příští
hodinách	hodina	k1gFnPc6	hodina
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
hromadnému	hromadný	k2eAgInSc3d1	hromadný
návalu	nával	k1gInSc3	nával
na	na	k7c6	na
berlínských	berlínský	k2eAgInPc6d1	berlínský
hraničních	hraniční	k2eAgInPc6d1	hraniční
přechodech	přechod	k1gInPc6	přechod
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
11	[number]	k4	11
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnPc4	hodina
večer	večer	k6eAd1	večer
východoněmecká	východoněmecký	k2eAgFnSc1d1	východoněmecká
pohraniční	pohraniční	k2eAgFnSc1d1	pohraniční
stráž	stráž	k1gFnSc1	stráž
kapitulovala	kapitulovat	k5eAaBmAgFnS	kapitulovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
kolem	kolem	k7c2	kolem
půlnoci	půlnoc	k1gFnSc2	půlnoc
proudily	proudit	k5eAaImAgInP	proudit
desetitisíce	desetitisíce	k1gInPc1	desetitisíce
Východoberlíňanů	Východoberlíňan	k1gMnPc2	Východoberlíňan
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
přechody	přechod	k1gInPc4	přechod
do	do	k7c2	do
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
více	hodně	k6eAd2	hodně
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgInPc2d1	zvaný
"	"	kIx"	"
<g/>
procesů	proces	k1gInPc2	proces
se	se	k3xPyFc4	se
střelci	střelec	k1gMnPc1	střelec
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
obžalovaní	obžalovaný	k1gMnPc1	obžalovaný
byli	být	k5eAaImAgMnP	být
zodpovědní	zodpovědný	k2eAgMnPc1d1	zodpovědný
za	za	k7c4	za
povely	povel	k1gInPc4	povel
ke	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
na	na	k7c4	na
prchající	prchající	k2eAgFnPc4d1	prchající
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obžalovaným	obžalovaná	k1gFnPc3	obžalovaná
patřili	patřit	k5eAaImAgMnP	patřit
i	i	k9	i
Erich	Erich	k1gMnSc1	Erich
Honecker	Honecker	k1gMnSc1	Honecker
<g/>
,	,	kIx,	,
Egon	Egon	k1gMnSc1	Egon
Krenz	Krenz	k1gMnSc1	Krenz
<g/>
,	,	kIx,	,
Willi	Will	k1gMnPc1	Will
Stoph	Stoph	k1gMnSc1	Stoph
a	a	k8xC	a
Erich	Erich	k1gMnSc1	Erich
Mielke	Mielk	k1gFnSc2	Mielk
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
35	[number]	k4	35
obžalovaných	obžalovaný	k1gMnPc2	obžalovaný
zproštěno	zproštěn	k2eAgNnSc4d1	zproštěno
obžaloby	obžaloba	k1gFnPc4	obžaloba
<g/>
,	,	kIx,	,
44	[number]	k4	44
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
odsouzeno	odsouzet	k5eAaImNgNnS	odsouzet
k	k	k7c3	k
podmíněnému	podmíněný	k2eAgNnSc3d1	podmíněné
odnětí	odnětí	k1gNnSc3	odnětí
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
11	[number]	k4	11
k	k	k7c3	k
odnětí	odnětí	k1gNnSc3	odnětí
svobody	svoboda	k1gFnSc2	svoboda
až	až	k9	až
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
a	a	k8xC	a
půl	půl	k1xP	půl
let	let	k1gInSc1	let
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k9	i
Egon	Egon	k1gMnSc1	Egon
Krenz	Krenz	k1gMnSc1	Krenz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
odstranění	odstranění	k1gNnSc3	odstranění
většiny	většina	k1gFnSc2	většina
symbolů	symbol	k1gInPc2	symbol
staré	starý	k2eAgFnSc2d1	stará
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
osudu	osud	k1gInSc3	osud
neušla	ujít	k5eNaPmAgFnS	ujít
ani	ani	k8xC	ani
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgInPc7d1	jediný
většími	veliký	k2eAgInPc7d2	veliký
souvislými	souvislý	k2eAgInPc7d1	souvislý
zbytky	zbytek	k1gInPc7	zbytek
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
East	East	k1gInSc1	East
Side	Sid	k1gInSc2	Sid
Gallery	Galler	k1gInPc4	Galler
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Sprévy	Spréva	k1gFnSc2	Spréva
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
malé	malý	k2eAgInPc4d1	malý
úseky	úsek	k1gInPc4	úsek
–	–	k?	–
první	první	k4xOgFnSc6	první
mezi	mezi	k7c7	mezi
Postupimským	postupimský	k2eAgNnSc7d1	Postupimské
náměstím	náměstí	k1gNnSc7	náměstí
(	(	kIx(	(
<g/>
Potsdamer	Potsdamer	k1gMnSc1	Potsdamer
Platz	Platz	k1gMnSc1	Platz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
na	na	k7c6	na
Bernauer	Bernaura	k1gFnPc2	Bernaura
Straße	Straß	k1gInSc2	Straß
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
i	i	k9	i
označující	označující	k2eAgFnSc1d1	označující
dlážděná	dlážděný	k2eAgFnSc1d1	dlážděná
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
vedla	vést	k5eAaImAgFnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
historii	historie	k1gFnSc6	historie
zdi	zeď	k1gFnSc2	zeď
(	(	kIx(	(
<g/>
a	a	k8xC	a
útěků	útěk	k1gInPc2	útěk
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
se	se	k3xPyFc4	se
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
Dům	dům	k1gInSc1	dům
na	na	k7c4	na
Checkpoint	Checkpoint	k1gInSc4	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
(	(	kIx(	(
<g/>
Haus	Haus	k1gInSc1	Haus
am	am	k?	am
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
(	(	kIx(	(
<g/>
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
bývalém	bývalý	k2eAgInSc6d1	bývalý
přechodu	přechod	k1gInSc6	přechod
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
Rozdělení	rozdělení	k1gNnSc2	rozdělení
Berlína	Berlín	k1gInSc2	Berlín
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
blokáda	blokáda	k1gFnSc1	blokáda
Západní	západní	k2eAgFnSc1d1	západní
Berlín	Berlín	k1gInSc4	Berlín
Západní	západní	k2eAgNnSc4d1	západní
Německo	Německo	k1gNnSc4	Německo
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
zeď	zeď	k1gFnSc1	zeď
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Haus	Hausa	k1gFnPc2	Hausa
am	am	k?	am
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
Na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
kůži	kůže	k1gFnSc6	kůže
<g/>
:	:	kIx,	:
Vyzkoušejte	vyzkoušet	k5eAaPmRp2nP	vyzkoušet
si	se	k3xPyFc3	se
únik	únik	k1gInSc4	únik
přes	přes	k7c4	přes
Berlínskou	berlínský	k2eAgFnSc4d1	Berlínská
zeď	zeď	k1gFnSc4	zeď
–	–	k?	–
iHned	ihned	k6eAd1	ihned
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Poutnik	Poutnik	k1gInSc1	Poutnik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Hledání	hledání	k1gNnSc4	hledání
berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
Rozhlasový	rozhlasový	k2eAgInSc1d1	rozhlasový
pořad	pořad	k1gInSc1	pořad
ke	k	k7c3	k
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
pádu	pád	k1gInSc2	pád
Berlínské	berlínský	k2eAgFnSc3d1	Berlínská
zdi	zeď	k1gFnSc3	zeď
:	:	kIx,	:
Rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
