<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
každoročně	každoročně	k6eAd1	každoročně
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
dne	den	k1gInSc2	den
dětí	dítě	k1gFnPc2	dítě
jsou	být	k5eAaImIp3nP	být
připravovány	připravován	k2eAgFnPc4d1	připravována
různé	různý	k2eAgFnPc4d1	různá
společenské	společenský	k2eAgFnPc4d1	společenská
a	a	k8xC	a
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
dětí	dítě	k1gFnPc2	dítě
má	mít	k5eAaImIp3nS	mít
upozornit	upozornit	k5eAaPmF	upozornit
světovou	světový	k2eAgFnSc4d1	světová
veřejnost	veřejnost	k1gFnSc4	veřejnost
na	na	k7c4	na
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
potřeby	potřeba	k1gFnPc4	potřeba
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
by	by	kYmCp3nS	by
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
během	během	k7c2	během
Turecké	turecký	k2eAgFnSc2d1	turecká
války	válka	k1gFnSc2	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
ochránce	ochránce	k1gMnSc1	ochránce
národních	národní	k2eAgFnPc2d1	národní
práv	právo	k1gNnPc2	právo
Mustafa	Mustaf	k1gMnSc2	Mustaf
Kemal	Kemal	k1gMnSc1	Kemal
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
k	k	k7c3	k
budování	budování	k1gNnSc3	budování
nového	nový	k2eAgInSc2d1	nový
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
23	[number]	k4	23
<g/>
.	.	kIx.	.
duben	duben	k1gInSc1	duben
jako	jako	k8xS	jako
Den	den	k1gInSc1	den
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Turecka	Turecko	k1gNnSc2	Turecko
slaví	slavit	k5eAaImIp3nP	slavit
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
státem	stát	k1gInSc7	stát
uznávaným	uznávaný	k2eAgInSc7d1	uznávaný
svátkem	svátek	k1gInSc7	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
dne	den	k1gInSc2	den
oslavujícího	oslavující	k2eAgInSc2d1	oslavující
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
doporučováno	doporučovat	k5eAaImNgNnS	doporučovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
na	na	k7c6	na
Světové	světový	k2eAgFnSc6d1	světová
konferenci	konference	k1gFnSc6	konference
pro	pro	k7c4	pro
blaho	blaho	k1gNnSc4	blaho
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
dětí	dítě	k1gFnPc2	dítě
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
konference	konference	k1gFnSc1	konference
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
konal	konat	k5eAaImAgInS	konat
festival	festival	k1gInSc1	festival
Dračích	dračí	k2eAgFnPc2d1	dračí
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
oslavu	oslava	k1gFnSc4	oslava
čínský	čínský	k2eAgMnSc1d1	čínský
generální	generální	k2eAgMnSc1d1	generální
konzul	konzul	k1gMnSc1	konzul
shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
mnoho	mnoho	k4c4	mnoho
čínských	čínský	k2eAgMnPc2d1	čínský
sirotků	sirotek	k1gMnPc2	sirotek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
měla	mít	k5eAaImAgFnS	mít
více	hodně	k6eAd2	hodně
zabývat	zabývat	k5eAaImF	zabývat
blahem	blaho	k1gNnSc7	blaho
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
určily	určit	k5eAaPmAgInP	určit
datum	datum	k1gNnSc4	datum
oslav	oslava	k1gFnPc2	oslava
Dne	den	k1gInSc2	den
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
oficiálně	oficiálně	k6eAd1	oficiálně
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
federace	federace	k1gFnSc1	federace
žen	žena	k1gFnPc2	žena
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Den	den	k1gInSc4	den
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
bylo	být	k5eAaImAgNnS	být
masové	masový	k2eAgNnSc1d1	masové
zabíjení	zabíjení	k1gNnSc1	zabíjení
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
Lidicích	Lidice	k1gInPc6	Lidice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
a	a	k8xC	a
také	také	k9	také
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Ouradouru	Ouradour	k1gInSc6	Ouradour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
oslav	oslava	k1gFnPc2	oslava
bylo	být	k5eAaImAgNnS	být
ustanoveno	ustanovit	k5eAaPmNgNnS	ustanovit
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
sirotky	sirotka	k1gFnPc4	sirotka
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Francisca	k1gFnSc4	Francisca
a	a	k8xC	a
první	první	k4xOgFnSc4	první
konferenci	konference	k1gFnSc4	konference
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
blahem	blaho	k1gNnSc7	blaho
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
slavit	slavit	k5eAaImF	slavit
rok	rok	k1gInSc4	rok
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
a	a	k8xC	a
oslavy	oslava	k1gFnPc1	oslava
probíhaly	probíhat	k5eAaImAgFnP	probíhat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
převzaly	převzít	k5eAaPmAgInP	převzít
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
ze	z	k7c2	z
Dne	den	k1gInSc2	den
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
stal	stát	k5eAaPmAgMnS	stát
Den	den	k1gInSc4	den
dětí	dítě	k1gFnPc2	dítě
nebo	nebo	k8xC	nebo
také	také	k9	také
jiným	jiný	k2eAgInSc7d1	jiný
názvem	název	k1gInSc7	název
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
také	také	k6eAd1	také
převzala	převzít	k5eAaPmAgFnS	převzít
toto	tento	k3xDgNnSc4	tento
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nP	slavit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
už	už	k6eAd1	už
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
Den	den	k1gInSc1	den
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
usilovala	usilovat	k5eAaImAgFnS	usilovat
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
unie	unie	k1gFnSc1	unie
pro	pro	k7c4	pro
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
dítě	dítě	k1gNnSc4	dítě
o	o	k7c4	o
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
výhradně	výhradně	k6eAd1	výhradně
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
OSN	OSN	kA	OSN
zareagovala	zareagovat	k5eAaPmAgFnS	zareagovat
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
každý	každý	k3xTgInSc4	každý
stát	stát	k1gInSc4	stát
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
roce	rok	k1gInSc6	rok
upřel	upřít	k5eAaPmAgMnS	upřít
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
nejmladší	mladý	k2eAgMnPc4d3	nejmladší
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
byla	být	k5eAaImAgFnS	být
také	také	k9	také
podpora	podpora	k1gFnSc1	podpora
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
poskytnutí	poskytnutí	k1gNnSc4	poskytnutí
dobrých	dobrý	k2eAgFnPc2d1	dobrá
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
zdravý	zdravý	k2eAgInSc4d1	zdravý
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
přístupu	přístup	k1gInSc2	přístup
ke	k	k7c3	k
vzdělání	vzdělání	k1gNnSc3	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
OSN	OSN	kA	OSN
bylo	být	k5eAaImAgNnS	být
všem	všecek	k3xTgNnPc3	všecek
státům	stát	k1gInPc3	stát
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
oslavovaly	oslavovat	k5eAaImAgFnP	oslavovat
děti	dítě	k1gFnPc1	dítě
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Datem	datum	k1gNnSc7	datum
oslav	oslava	k1gFnPc2	oslava
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
OSN	OSN	kA	OSN
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Deklaraci	deklarace	k1gFnSc4	deklarace
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
dítěte	dítě	k1gNnSc2	dítě
a	a	k8xC	a
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
právech	právo	k1gNnPc6	právo
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
si	se	k3xPyFc3	se
i	i	k8xC	i
přesto	přesto	k8xC	přesto
ponechaly	ponechat	k5eAaPmAgInP	ponechat
datum	datum	k1gNnSc4	datum
oslav	oslava	k1gFnPc2	oslava
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
státy	stát	k1gInPc1	stát
datum	datum	k1gNnSc4	datum
oslav	oslava	k1gFnPc2	oslava
přesunuly	přesunout	k5eAaPmAgFnP	přesunout
na	na	k7c4	na
listopad	listopad	k1gInSc4	listopad
podle	podle	k7c2	podle
doporučení	doporučení	k1gNnSc2	doporučení
OSN	OSN	kA	OSN
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
si	se	k3xPyFc3	se
ponechaly	ponechat	k5eAaPmAgFnP	ponechat
červnové	červnový	k2eAgNnSc4d1	červnové
datum	datum	k1gNnSc4	datum
a	a	k8xC	a
přijaly	přijmout	k5eAaPmAgFnP	přijmout
také	také	k9	také
oslavy	oslava	k1gFnPc1	oslava
dětí	dítě	k1gFnPc2	dítě
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
připomínají	připomínat	k5eAaImIp3nP	připomínat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
významné	významný	k2eAgInPc4d1	významný
dny	den	k1gInPc4	den
věnované	věnovaný	k2eAgInPc4d1	věnovaný
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
obětí	oběť	k1gFnSc7	oběť
agrese	agrese	k1gFnSc1	agrese
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
den	den	k1gInSc4	den
za	za	k7c4	za
odstranění	odstranění	k1gNnSc4	odstranění
práce	práce	k1gFnSc2	práce
dětí	dítě	k1gFnPc2	dítě
Den	dna	k1gFnPc2	dna
afrického	africký	k2eAgNnSc2d1	africké
dítěte	dítě	k1gNnSc2	dítě
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
mládeže	mládež	k1gFnSc2	mládež
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
den	den	k1gInSc4	den
ztracených	ztracený	k2eAgFnPc2d1	ztracená
dětí	dítě	k1gFnPc2	dítě
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
dětí	dítě	k1gFnPc2	dítě
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
)	)	kIx)	)
Den	den	k1gInSc1	den
nenarozeného	narozený	k2eNgNnSc2d1	nenarozené
dítěte	dítě	k1gNnSc2	dítě
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
Významné	významný	k2eAgInPc4d1	významný
dny	den	k1gInPc4	den
</s>
