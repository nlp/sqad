<s>
Obec	obec	k1gFnSc1	obec
Slatina	slatina	k1gFnSc1	slatina
nad	nad	k7c7	nad
Zdobnicí	Zdobnice	k1gFnSc7	Zdobnice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Moorwies	Moorwies	k1gInSc1	Moorwies
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Královéhradecký	královéhradecký	k2eAgInSc1d1	královéhradecký
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
8	[number]	k4	8
km	km	kA	km
vsv	vsv	k?	vsv
<g/>
.	.	kIx.	.
od	od	k7c2	od
Vamberka	Vamberk	k1gInSc2	Vamberk
a	a	k8xC	a
8	[number]	k4	8
km	km	kA	km
sz.	sz.	k?	sz.
od	od	k7c2	od
Žamberka	Žamberka	k1gFnSc1	Žamberka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
817	[number]	k4	817
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1359	[number]	k4	1359
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
kyselých	kyselý	k2eAgInPc2d1	kyselý
močálovitých	močálovitý	k2eAgInPc2d1	močálovitý
pozemků	pozemek	k1gInPc2	pozemek
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
slatin	slatina	k1gFnPc2	slatina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
dříve	dříve	k6eAd2	dříve
hojně	hojně	k6eAd1	hojně
nalézaly	nalézat	k5eAaImAgFnP	nalézat
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Proměnění	proměnění	k1gNnSc2	proměnění
Páně	páně	k2eAgNnSc2d1	páně
-	-	kIx~	-
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1699	[number]	k4	1699
<g/>
,	,	kIx,	,
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Doroty	Dorota	k1gFnSc2	Dorota
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
nedochoval	dochovat	k5eNaPmAgInS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
devítiletá	devítiletý	k2eAgFnSc1d1	devítiletá
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
historie	historie	k1gFnSc2	historie
sahá	sahat	k5eAaImIp3nS	sahat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
první	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1789	[number]	k4	1789
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
školského	školský	k2eAgInSc2d1	školský
zákona	zákon	k1gInSc2	zákon
zřízena	zřídit	k5eAaPmNgFnS	zřídit
škola	škola	k1gFnSc1	škola
řádná	řádný	k2eAgFnSc1d1	řádná
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Doudleby	Doudleby	k1gInPc4	Doudleby
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
-	-	kIx~	-
Rokytnice	Rokytnice	k1gFnSc1	Rokytnice
v	v	k7c6	v
Orlických	orlický	k2eAgFnPc6d1	Orlická
horách	hora	k1gFnPc6	hora
Marie	Marie	k1gFnSc1	Marie
Hübnerová	Hübnerová	k1gFnSc1	Hübnerová
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
-	-	kIx~	-
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
-	-	kIx~	-
význačná	význačný	k2eAgFnSc1d1	význačná
česká	český	k2eAgFnSc1d1	Česká
divadelní	divadelní	k2eAgFnSc1d1	divadelní
herečka	herečka	k1gFnSc1	herečka
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Leoš	Leoš	k1gMnSc1	Leoš
Kubíček	Kubíček	k1gMnSc1	Kubíček
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
-	-	kIx~	-
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
-	-	kIx~	-
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
Josef	Josef	k1gMnSc1	Josef
Kubíček	Kubíček	k1gMnSc1	Kubíček
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
-	-	kIx~	-
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
-	-	kIx~	-
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
,	,	kIx,	,
žák	žák	k1gMnSc1	žák
J.	J.	kA	J.
V.	V.	kA	V.
Myslbeka	Myslbeka	k1gMnSc1	Myslbeka
Herbert	Herbert	k1gMnSc1	Herbert
Masaryk	Masaryk	k1gMnSc1	Masaryk
Antonín	Antonín	k1gMnSc1	Antonín
Slavíček	Slavíček	k1gMnSc1	Slavíček
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slatina	slatina	k1gFnSc1	slatina
nad	nad	k7c7	nad
Zdobnicí	Zdobnice	k1gFnSc7	Zdobnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
základní	základní	k2eAgFnSc2d1	základní
školy	škola	k1gFnSc2	škola
</s>
