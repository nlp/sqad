<p>
<s>
Sloboda	sloboda	k1gFnSc1	sloboda
a	a	k8xC	a
Solidarita	solidarita	k1gFnSc1	solidarita
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
SaS	Sas	k1gMnSc1	Sas
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
:	:	kIx,	:
Svoboda	svoboda	k1gFnSc1	svoboda
a	a	k8xC	a
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
liberální	liberální	k2eAgFnSc1d1	liberální
parlamentní	parlamentní	k2eAgFnSc1d1	parlamentní
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
působící	působící	k2eAgFnSc1d1	působící
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Stranu	strana	k1gFnSc4	strana
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
je	být	k5eAaImIp3nS	být
ekonom	ekonom	k1gMnSc1	ekonom
Richard	Richard	k1gMnSc1	Richard
Sulík	Sulík	k1gMnSc1	Sulík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
strana	strana	k1gFnSc1	strana
12,1	[number]	k4	12,1
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
strana	strana	k1gFnSc1	strana
oslabila	oslabit	k5eAaPmAgFnS	oslabit
<g/>
,	,	kIx,	,
obsadila	obsadit	k5eAaPmAgFnS	obsadit
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
obhájila	obhájit	k5eAaPmAgFnS	obhájit
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
po	po	k7c6	po
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
slovenského	slovenský	k2eAgInSc2d1	slovenský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
když	když	k8xS	když
získala	získat	k5eAaPmAgFnS	získat
21	[number]	k4	21
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
kolech	kolo	k1gNnPc6	kolo
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
strana	strana	k1gFnSc1	strana
podporovala	podporovat	k5eAaImAgFnS	podporovat
Ivetu	Iveta	k1gFnSc4	Iveta
Radičovou	radičův	k2eAgFnSc7d1	radičův
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
kandidátky	kandidátka	k1gFnSc2	kandidátka
byl	být	k5eAaImAgMnS	být
ekonom	ekonom	k1gMnSc1	ekonom
Ján	Ján	k1gMnSc1	Ján
Oravec	Oravec	k1gMnSc1	Oravec
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
získala	získat	k5eAaPmAgFnS	získat
4,71	[number]	k4	4,71
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
v	v	k7c6	v
Evropském	evropský	k2eAgInSc6d1	evropský
parlamentu	parlament	k1gInSc6	parlament
jí	on	k3xPp3gFnSc3	on
tak	tak	k9	tak
jen	jen	k9	jen
těsně	těsně	k6eAd1	těsně
uniklo	uniknout	k5eAaPmAgNnS	uniknout
(	(	kIx(	(
<g/>
uzavírací	uzavírací	k2eAgFnSc1d1	uzavírací
klauzule	klauzule	k1gFnSc1	klauzule
byla	být	k5eAaImAgFnS	být
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Referendum	referendum	k1gNnSc1	referendum
2010	[number]	k4	2010
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
strana	strana	k1gFnSc1	strana
začala	začít	k5eAaPmAgFnS	začít
sbírat	sbírat	k5eAaImF	sbírat
podpisy	podpis	k1gInPc4	podpis
pro	pro	k7c4	pro
vypsání	vypsání	k1gNnSc4	vypsání
referenda	referendum	k1gNnSc2	referendum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
šest	šest	k4xCc4	šest
témat	téma	k1gNnPc2	téma
<g/>
:	:	kIx,	:
zrušení	zrušení	k1gNnSc4	zrušení
koncesionářských	koncesionářský	k2eAgInPc2d1	koncesionářský
poplatků	poplatek	k1gInPc2	poplatek
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc1	omezení
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
počtu	počet	k1gInSc2	počet
poslanců	poslanec	k1gMnPc2	poslanec
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
ze	z	k7c2	z
150	[number]	k4	150
na	na	k7c4	na
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stanovení	stanovení	k1gNnSc1	stanovení
maximální	maximální	k2eAgFnPc1d1	maximální
ceny	cena	k1gFnPc1	cena
automobilů	automobil	k1gInPc2	automobil
orgánů	orgán	k1gInPc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
a	a	k8xC	a
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
přes	přes	k7c4	přes
internet	internet	k1gInSc4	internet
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
tiskového	tiskový	k2eAgInSc2d1	tiskový
zákona	zákon	k1gInSc2	zákon
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Petice	petice	k1gFnSc1	petice
na	na	k7c4	na
vypsání	vypsání	k1gNnSc4	vypsání
referenda	referendum	k1gNnSc2	referendum
získala	získat	k5eAaPmAgFnS	získat
celkem	celkem	k6eAd1	celkem
368	[number]	k4	368
tisíc	tisíc	k4xCgInPc2	tisíc
platných	platný	k2eAgInPc2d1	platný
podpisů	podpis	k1gInPc2	podpis
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
překročila	překročit	k5eAaPmAgFnS	překročit
hranici	hranice	k1gFnSc4	hranice
požadovaných	požadovaný	k2eAgInPc2d1	požadovaný
350	[number]	k4	350
tisíc	tisíc	k4xCgInPc2	tisíc
podpisů	podpis	k1gInPc2	podpis
a	a	k8xC	a
prezident	prezident	k1gMnSc1	prezident
Slovenska	Slovensko	k1gNnSc2	Slovensko
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
referendum	referendum	k1gNnSc4	referendum
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
na	na	k7c6	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Referenda	referendum	k1gNnSc2	referendum
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
pouze	pouze	k6eAd1	pouze
22,84	[number]	k4	22,84
%	%	kIx~	%
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
nebylo	být	k5eNaImAgNnS	být
referendum	referendum	k1gNnSc1	referendum
platné	platný	k2eAgNnSc1d1	platné
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
neúspěch	neúspěch	k1gInSc4	neúspěch
referenda	referendum	k1gNnSc2	referendum
se	se	k3xPyFc4	se
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
tezí	teze	k1gFnPc2	teze
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
programového	programový	k2eAgNnSc2d1	programové
prohlášení	prohlášení	k1gNnSc2	prohlášení
vlády	vláda	k1gFnSc2	vláda
Ivety	Iveta	k1gFnSc2	Iveta
Radičové	radič	k1gMnPc1	radič
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
byla	být	k5eAaImAgFnS	být
SaS	Sas	k1gMnSc1	Sas
součástí	součást	k1gFnPc2	součást
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vládní	vládní	k2eAgFnSc1d1	vládní
krize	krize	k1gFnSc1	krize
2011	[number]	k4	2011
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Ivety	Iveta	k1gFnSc2	Iveta
Radičové	radič	k1gMnPc1	radič
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
ohledně	ohledně	k6eAd1	ohledně
schválení	schválení	k1gNnSc1	schválení
posílení	posílení	k1gNnSc2	posílení
pravomocí	pravomoc	k1gFnPc2	pravomoc
Evropského	evropský	k2eAgInSc2d1	evropský
nástroje	nástroj	k1gInSc2	nástroj
finanční	finanční	k2eAgFnSc2d1	finanční
stability	stabilita	k1gFnSc2	stabilita
označovaného	označovaný	k2eAgInSc2d1	označovaný
také	také	k9	také
za	za	k7c4	za
záchranný	záchranný	k2eAgInSc4d1	záchranný
fond	fond	k1gInSc4	fond
eurozóny	eurozóna	k1gFnSc2	eurozóna
<g/>
.	.	kIx.	.
</s>
<s>
Sloboda	sloboda	k1gFnSc1	sloboda
a	a	k8xC	a
Solidarita	solidarita	k1gFnSc1	solidarita
s	s	k7c7	s
jeho	jeho	k3xOp3gNnSc7	jeho
schválením	schválení	k1gNnSc7	schválení
nesouhlasila	souhlasit	k5eNaImAgFnS	souhlasit
<g/>
,	,	kIx,	,
argumentovala	argumentovat	k5eAaImAgFnS	argumentovat
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
jde	jít	k5eAaImIp3nS	jít
proti	proti	k7c3	proti
duchu	duch	k1gMnSc3	duch
solidarity	solidarita	k1gFnSc2	solidarita
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
EU	EU	kA	EU
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chudší	chudý	k2eAgNnSc1d2	chudší
Slovensko	Slovensko	k1gNnSc1	Slovensko
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
přispívat	přispívat	k5eAaImF	přispívat
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
bohatších	bohatý	k2eAgMnPc2d2	bohatší
států	stát	k1gInPc2	stát
v	v	k7c6	v
problémech	problém	k1gInPc6	problém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Schválení	schválení	k1gNnSc4	schválení
návrhu	návrh	k1gInSc2	návrh
považovala	považovat	k5eAaImAgFnS	považovat
SaS	Sas	k1gMnSc1	Sas
také	také	k9	také
za	za	k7c4	za
porušení	porušení	k1gNnSc4	porušení
vládního	vládní	k2eAgInSc2d1	vládní
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Iveta	Iveta	k1gFnSc1	Iveta
Radičová	Radičová	k1gFnSc1	Radičová
spojila	spojit	k5eAaPmAgFnS	spojit
hlasování	hlasování	k1gNnSc4	hlasování
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
návrhu	návrh	k1gInSc6	návrh
s	s	k7c7	s
vyjádřením	vyjádření	k1gNnSc7	vyjádření
důvěry	důvěra	k1gFnSc2	důvěra
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
návrhu	návrh	k1gInSc6	návrh
se	se	k3xPyFc4	se
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Slovenské	slovenský	k2eAgFnSc6d1	slovenská
národní	národní	k2eAgFnSc6d1	národní
radě	rada	k1gFnSc6	rada
odmítnut	odmítnut	k2eAgInSc1d1	odmítnut
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
tak	tak	k6eAd1	tak
ztratila	ztratit	k5eAaPmAgFnS	ztratit
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
schválila	schválit	k5eAaPmAgFnS	schválit
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
předčasné	předčasný	k2eAgFnSc2d1	předčasná
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
volby	volba	k1gFnSc2	volba
s	s	k7c7	s
datem	datum	k1gNnSc7	datum
konání	konání	k1gNnSc2	konání
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
reakcí	reakce	k1gFnPc2	reakce
pak	pak	k6eAd1	pak
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2011	[number]	k4	2011
stranu	strana	k1gFnSc4	strana
opustila	opustit	k5eAaPmAgFnS	opustit
skupina	skupina	k1gFnSc1	skupina
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
novou	nový	k2eAgFnSc4d1	nová
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
Obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
lidé	člověk	k1gMnPc1	člověk
a	a	k8xC	a
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
2012	[number]	k4	2012
-	-	kIx~	-
2016	[number]	k4	2016
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
strana	strana	k1gFnSc1	strana
výrazně	výrazně	k6eAd1	výrazně
oslabila	oslabit	k5eAaPmAgFnS	oslabit
<g/>
,	,	kIx,	,
když	když	k8xS	když
udržela	udržet	k5eAaPmAgFnS	udržet
pouhých	pouhý	k2eAgInPc2d1	pouhý
11	[number]	k4	11
mandátů	mandát	k1gInPc2	mandát
s	s	k7c7	s
volebním	volební	k2eAgInSc7d1	volební
ziskem	zisk	k1gInSc7	zisk
méně	málo	k6eAd2	málo
než	než	k8xS	než
6	[number]	k4	6
%	%	kIx~	%
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
až	až	k9	až
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
opozice	opozice	k1gFnPc4	opozice
proti	proti	k7c3	proti
jednobarevné	jednobarevný	k2eAgFnSc3d1	jednobarevná
vládě	vláda	k1gFnSc3	vláda
Roberta	Robert	k1gMnSc2	Robert
Fica	Fico	k1gMnSc2	Fico
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
-	-	kIx~	-
2015	[number]	k4	2015
se	se	k3xPyFc4	se
volební	volební	k2eAgFnSc1d1	volební
preference	preference	k1gFnSc1	preference
strany	strana	k1gFnSc2	strana
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
pouze	pouze	k6eAd1	pouze
kolem	kolem	k6eAd1	kolem
pětiprocentní	pětiprocentní	k2eAgFnPc4d1	pětiprocentní
hranice	hranice	k1gFnPc4	hranice
nutné	nutný	k2eAgFnPc1d1	nutná
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
minimum	minimum	k1gNnSc1	minimum
3,9	[number]	k4	3,9
%	%	kIx~	%
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
a	a	k8xC	a
lednu	leden	k1gInSc3	leden
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
vyostřil	vyostřit	k5eAaPmAgInS	vyostřit
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
předsedou	předseda	k1gMnSc7	předseda
Richardem	Richard	k1gMnSc7	Richard
Sulíkem	Sulík	k1gMnSc7	Sulík
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
vyzyvatelem	vyzyvatelem	k?	vyzyvatelem
Jozefem	Jozef	k1gMnSc7	Jozef
Kollárem	Kollár	k1gMnSc7	Kollár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stranickém	stranický	k2eAgInSc6d1	stranický
kongresu	kongres	k1gInSc6	kongres
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
těsným	těsný	k2eAgInSc7d1	těsný
výsledkem	výsledek	k1gInSc7	výsledek
potvrzen	potvrzen	k2eAgMnSc1d1	potvrzen
Richard	Richard	k1gMnSc1	Richard
Sulík	Sulík	k1gMnSc1	Sulík
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
134	[number]	k4	134
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
Jozef	Jozef	k1gMnSc1	Jozef
Kollár	Kollár	k1gMnSc1	Kollár
obdržel	obdržet	k5eAaPmAgMnS	obdržet
121	[number]	k4	121
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
pak	pak	k6eAd1	pak
skupina	skupina	k1gFnSc1	skupina
kolem	kolem	k7c2	kolem
Jozefa	Jozef	k1gMnSc2	Jozef
Kollára	Kollár	k1gMnSc2	Kollár
oznámila	oznámit	k5eAaPmAgFnS	oznámit
ukončení	ukončení	k1gNnSc6	ukončení
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
ve	v	k7c6	v
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
založení	založení	k1gNnSc6	založení
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Liberální	liberální	k2eAgFnSc1d1	liberální
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
volbami	volba	k1gFnPc7	volba
2016	[number]	k4	2016
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
možnost	možnost	k1gFnSc1	možnost
volební	volební	k2eAgFnSc2d1	volební
koalice	koalice	k1gFnSc2	koalice
pravicových	pravicový	k2eAgFnPc2d1	pravicová
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
oznámila	oznámit	k5eAaPmAgFnS	oznámit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
kandidaturu	kandidatura	k1gFnSc4	kandidatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volební	volební	k2eAgNnSc4d1	volební
období	období	k1gNnSc4	období
2016	[number]	k4	2016
-	-	kIx~	-
2020	[number]	k4	2020
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
2016	[number]	k4	2016
strana	strana	k1gFnSc1	strana
obdržela	obdržet	k5eAaPmAgFnS	obdržet
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
12	[number]	k4	12
%	%	kIx~	%
platných	platný	k2eAgInPc2d1	platný
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
získala	získat	k5eAaPmAgFnS	získat
21	[number]	k4	21
mandátů	mandát	k1gInPc2	mandát
a	a	k8xC	a
umístila	umístit	k5eAaPmAgFnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
neumožnily	umožnit	k5eNaPmAgInP	umožnit
straně	strana	k1gFnSc6	strana
sestavit	sestavit	k5eAaPmF	sestavit
širokou	široký	k2eAgFnSc4d1	široká
pětičlenou	pětičlený	k2eAgFnSc4d1	pětičlená
vládu	vláda	k1gFnSc4	vláda
pravého	pravý	k2eAgInSc2d1	pravý
středu	střed	k1gInSc2	střed
a	a	k8xC	a
případný	případný	k2eAgMnSc1d1	případný
šestý	šestý	k4xOgMnSc1	šestý
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národná	národný	k2eAgFnSc1d1	národná
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
šestičlenou	šestičlený	k2eAgFnSc4d1	šestičlená
koalici	koalice	k1gFnSc4	koalice
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
SaS	Sas	k1gMnSc1	Sas
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stává	stávat	k5eAaImIp3nS	stávat
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
opoziční	opoziční	k2eAgFnSc7d1	opoziční
stranou	strana	k1gFnSc7	strana
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
sestavena	sestavit	k5eAaPmNgFnS	sestavit
třetí	třetí	k4xOgFnSc1	třetí
vláda	vláda	k1gFnSc1	vláda
Roberta	Robert	k1gMnSc4	Robert
Fica	Fico	k1gMnSc4	Fico
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Volební	volební	k2eAgInPc1d1	volební
výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Parlamentní	parlamentní	k2eAgFnPc1d1	parlamentní
volby	volba	k1gFnPc1	volba
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Evropské	evropský	k2eAgFnSc2d1	Evropská
volby	volba	k1gFnSc2	volba
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Volební	volební	k2eAgFnPc1d1	volební
preference	preference	k1gFnPc1	preference
==	==	k?	==
</s>
</p>
<p>
<s>
Volební	volební	k2eAgFnPc1d1	volební
preference	preference	k1gFnPc1	preference
podle	podle	k7c2	podle
agentury	agentura	k1gFnSc2	agentura
Focus	Focus	k1gInSc4	Focus
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
agentura	agentura	k1gFnSc1	agentura
Median	Median	k1gMnSc1	Median
<g/>
)	)	kIx)	)
a	a	k8xC	a
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
března	březen	k1gInSc2	březen
a	a	k8xC	a
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
agentura	agentura	k1gFnSc1	agentura
Polis	Polis	k1gFnSc2	Polis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tučně	tučně	k6eAd1	tučně
vyznačené	vyznačený	k2eAgFnPc1d1	vyznačená
preference	preference	k1gFnPc1	preference
znamenají	znamenat	k5eAaImIp3nP	znamenat
překročení	překročení	k1gNnSc4	překročení
hranice	hranice	k1gFnSc2	hranice
5	[number]	k4	5
<g/>
%	%	kIx~	%
potřebné	potřebné	k1gNnSc4	potřebné
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
SaS	Sas	k1gMnSc1	Sas
</s>
</p>
