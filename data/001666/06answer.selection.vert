<s>
Hans	Hans	k1gMnSc1	Hans
Albrecht	Albrecht	k1gMnSc1	Albrecht
Bethe	Bethe	k1gFnSc1	Bethe
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Štrasburk	Štrasburk	k1gInSc1	Štrasburk
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
-	-	kIx~	-
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Ithaca	Ithaca	k1gFnSc1	Ithaca
(	(	kIx(	(
<g/>
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
fyziků	fyzik	k1gMnPc2	fyzik
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
