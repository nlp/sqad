<s>
Willard	Willard	k1gMnSc1
Van	Van	k1gMnSc1
Orman	Orman	k1gMnSc1
Quine	Quine	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1908	#num#	k4
Akron	Akrona	k1gFnPc2
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
Willard	Willard	k1gMnSc1
van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
(	(	kIx(
<g/>
s	s	k7c7
malým	malý	k2eAgNnSc7d1
„	„	k?
<g/>
v	v	k7c6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
citován	citován	k2eAgInSc1d1
jako	jako	k8xC,k8xS
W.	W.	kA
V.	V.	kA
Quine	Quin	k1gInSc5
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
a	a	k8xC
logik	logik	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
analytické	analytický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
a	a	k8xC
filosofického	filosofický	k2eAgInSc2d1
naturalismu	naturalismus	k1gInSc2
<g/>
.	.	kIx.
</s>