<s>
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
nejvlivnějším	vlivný	k2eAgMnSc7d3
americkým	americký	k2eAgMnSc7d1
filosofem	filosof	k1gMnSc7
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
představitelem	představitel	k1gMnSc7
analytické	analytický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
a	a	k8xC
filosofického	filosofický	k2eAgInSc2d1
naturalizmu	naturalizmus	k1gInSc2
<g/>
?	?	kIx.
</s>