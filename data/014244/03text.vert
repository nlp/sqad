<s>
Willard	Willard	k1gMnSc1
Van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
</s>
<s>
Willard	Willard	k1gMnSc1
Van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
Narození	narození	k1gNnPc5
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1908	#num#	k4
<g/>
Akron	Akron	k1gNnSc4
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
92	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Boston	Boston	k1gInSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Harvardova	Harvardův	k2eAgFnSc1d1
univerzitaOberlin	univerzitaOberlin	k1gInSc4
College	Colleg	k1gInSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
matematik	matematik	k1gMnSc1
<g/>
,	,	kIx,
analytický	analytický	k2eAgMnSc1d1
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
epistemolog	epistemolog	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
logik	logik	k1gMnSc1
a	a	k8xC
jazykovědec	jazykovědec	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Harvardova	Harvardův	k2eAgFnSc1d1
univerzitaWesleyan	univerzitaWesleyany	k1gInPc2
University	universita	k1gFnSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Rolf	Rolf	k1gMnSc1
Schock	Schock	k1gMnSc1
Prize	Prize	k1gFnSc2
in	in	k?
Logic	Logic	k1gMnSc1
and	and	k?
Philosophy	Philosopha	k1gFnSc2
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
Kyoto	Kyoto	k1gNnSc1
Prize	Prize	k1gFnSc2
in	in	k?
Arts	Arts	k1gInSc1
and	and	k?
Philosophy	Philosopha	k1gFnSc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
<g/>
Fellow	Fellow	k1gFnSc1
of	of	k?
the	the	k?
Committee	Committe	k1gInSc2
for	forum	k1gNnPc2
Skeptical	Skeptical	k1gMnPc2
Inquiry	Inquira	k1gFnSc2
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
ateismus	ateismus	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Willard	Willard	k1gMnSc1
Van	Van	k1gMnSc1
Orman	Orman	k1gMnSc1
Quine	Quine	k1gMnSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1908	#num#	k4
Akron	Akrona	k1gFnPc2
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2000	#num#	k4
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
Willard	Willard	k1gMnSc1
van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
(	(	kIx(
<g/>
s	s	k7c7
malým	malý	k2eAgNnSc7d1
„	„	k?
<g/>
v	v	k7c6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
citován	citován	k2eAgInSc1d1
jako	jako	k8xC,k8xS
W.	W.	kA
V.	V.	kA
Quine	Quin	k1gInSc5
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
a	a	k8xC
logik	logik	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
analytické	analytický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
a	a	k8xC
filosofického	filosofický	k2eAgInSc2d1
naturalismu	naturalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quine	Quin	k1gInSc5
je	být	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
nejvlivnější	vlivný	k2eAgMnSc1d3
americký	americký	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
zejména	zejména	k9
v	v	k7c6
anglosaském	anglosaský	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Quine	Quinout	k5eAaImIp3nS,k5eAaPmIp3nS
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
především	především	k9
odmítnutím	odmítnutí	k1gNnSc7
rozlišování	rozlišování	k1gNnSc2
syntetických	syntetický	k2eAgInPc2d1
a	a	k8xC
analytických	analytický	k2eAgInPc2d1
výroků	výrok	k1gInPc2
<g/>
,	,	kIx,
důsledným	důsledný	k2eAgInSc7d1
empirismem	empirismus	k1gInSc7
a	a	k8xC
přesvědčením	přesvědčení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
filosofie	filosofie	k1gFnSc1
má	mít	k5eAaImIp3nS
užívat	užívat	k5eAaImF
výhradně	výhradně	k6eAd1
metodologii	metodologie	k1gFnSc4
přírodních	přírodní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
vývoj	vývoj	k1gInSc1
</s>
<s>
Quine	Quinout	k5eAaPmIp3nS,k5eAaImIp3nS
studoval	studovat	k5eAaImAgMnS
filosofii	filosofie	k1gFnSc4
na	na	k7c6
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
u	u	k7c2
A.	A.	kA
N.	N.	kA
Whiteheada	Whiteheada	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
doktorát	doktorát	k1gInSc4
roku	rok	k1gInSc2
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
Whitheada	Whitheada	k1gFnSc1
byl	být	k5eAaImAgInS
silně	silně	k6eAd1
ovlivněn	ovlivnit	k5eAaPmNgInS
Bertrandem	Bertrando	k1gNnSc7
Russellem	Russell	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
seznámil	seznámit	k5eAaPmAgInS
s	s	k7c7
polskými	polský	k2eAgMnPc7d1
logiky	logicus	k1gMnPc7
(	(	kIx(
<g/>
A.	A.	kA
Tarskim	Tarskim	k1gInSc1
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
)	)	kIx)
a	a	k8xC
členy	člen	k1gMnPc7
Vídeňského	vídeňský	k2eAgInSc2d1
kroužku	kroužek	k1gInSc2
okolo	okolo	k7c2
R.	R.	kA
Carnapa	Carnapa	k1gFnSc1
<g/>
,	,	kIx,
s	s	k7c7
nímž	jenž	k3xRgInSc7
roku	rok	k1gInSc2
1933	#num#	k4
diskutoval	diskutovat	k5eAaImAgMnS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Prahu	Praha	k1gFnSc4
navštívil	navštívit	k5eAaPmAgMnS
ještě	ještě	k6eAd1
jednou	jednou	k6eAd1
roku	rok	k1gInSc2
1997	#num#	k4
jako	jako	k8xC,k8xS
host	host	k1gMnSc1
Filosofického	filosofický	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
AV	AV	kA
ČR	ČR	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Během	během	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
přednášel	přednášet	k5eAaImAgMnS
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
portugalštině	portugalština	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
na	na	k7c4
Harvardovu	Harvardův	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
až	až	k9
do	do	k7c2
penzionování	penzionování	k1gNnSc2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Činný	činný	k2eAgInSc4d1
však	však	k8xC
zůstal	zůstat	k5eAaPmAgInS
i	i	k9
poté	poté	k6eAd1
<g/>
,	,	kIx,
především	především	k9
jako	jako	k9
hostující	hostující	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
dvou	dva	k4xCgNnPc2
manželství	manželství	k1gNnPc2
měl	mít	k5eAaImAgMnS
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Myšlení	myšlení	k1gNnSc1
</s>
<s>
Reformulace	reformulace	k1gFnSc1
empirismu	empirismus	k1gInSc2
</s>
<s>
Quine	Quinout	k5eAaImIp3nS,k5eAaPmIp3nS
kritizuje	kritizovat	k5eAaImIp3nS
a	a	k8xC
přezkoumává	přezkoumávat	k5eAaImIp3nS
logický	logický	k2eAgInSc4d1
empirismus	empirismus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
poznal	poznat	k5eAaPmAgInS
ve	v	k7c6
styku	styk	k1gInSc6
s	s	k7c7
Vídeňským	vídeňský	k2eAgInSc7d1
kruhem	kruh	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
dospívá	dospívat	k5eAaImIp3nS
k	k	k7c3
odlišnému	odlišný	k2eAgNnSc3d1
pojetí	pojetí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
velmi	velmi	k6eAd1
vlivném	vlivný	k2eAgInSc6d1
článku	článek	k1gInSc6
Dvě	dva	k4xCgNnPc4
dogmata	dogma	k1gNnPc4
empirismu	empirismus	k1gInSc2
zpochybňuje	zpochybňovat	k5eAaImIp3nS
smysluplnost	smysluplnost	k1gFnSc4
rozlišování	rozlišování	k1gNnSc3
mezi	mezi	k7c7
analytickými	analytický	k2eAgInPc7d1
výroky	výrok	k1gInPc7
(	(	kIx(
<g/>
tj.	tj.	kA
takovými	takový	k3xDgInPc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
subjektu	subjekt	k1gInSc2
není	být	k5eNaImIp3nS
připisováno	připisovat	k5eAaImNgNnS
víc	hodně	k6eAd2
než	než	k8xS
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
již	již	k6eAd1
je	být	k5eAaImIp3nS
obsaženo	obsáhnout	k5eAaPmNgNnS
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
pojmu	pojem	k1gInSc6
–	–	k?
např.	např.	kA
vraník	vraník	k1gMnSc1
je	být	k5eAaImIp3nS
černý	černý	k2eAgMnSc1d1
kůň	kůň	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
syntetickými	syntetický	k2eAgInPc7d1
výroky	výrok	k1gInPc7
(	(	kIx(
<g/>
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
naopak	naopak	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
k	k	k7c3
subjektu	subjekt	k1gInSc3
přidávají	přidávat	k5eAaImIp3nP
něco	něco	k3yInSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
pojmu	pojem	k1gInSc6
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
a	a	k8xC
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
popisují	popisovat	k5eAaImIp3nP
empirická	empirický	k2eAgNnPc4d1
fakta	faktum	k1gNnPc4
–	–	k?
např.	např.	kA
v	v	k7c6
kredenci	kredenc	k1gFnSc6
je	být	k5eAaImIp3nS
struhadlo	struhadlo	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
problematizuje	problematizovat	k5eAaImIp3nS
samotný	samotný	k2eAgInSc1d1
pojem	pojem	k1gInSc1
analytičnosti	analytičnost	k1gFnSc2
<g/>
;	;	kIx,
i	i	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Kant	Kant	k1gMnSc1
<g/>
,	,	kIx,
Frege	Frege	k1gFnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
neshodli	shodnout	k5eNaPmAgMnP
na	na	k7c6
významu	význam	k1gInSc6
analytičnosti	analytičnost	k1gFnSc2
<g/>
,	,	kIx,
svědčí	svědčit	k5eAaImIp3nS
dle	dle	k7c2
Quinea	Quine	k1gInSc2
pro	pro	k7c4
opuštění	opuštění	k1gNnSc4
uvedeného	uvedený	k2eAgNnSc2d1
rozlišování	rozlišování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
v	v	k7c6
převádění	převádění	k1gNnSc6
výroků	výrok	k1gInPc2
na	na	k7c6
o	o	k7c4
sobě	se	k3xPyFc3
zřejmé	zřejmý	k2eAgFnPc1d1
logické	logický	k2eAgFnPc1d1
pravdy	pravda	k1gFnPc1
spoléháme	spoléhat	k5eAaImIp1nP
na	na	k7c4
definice	definice	k1gFnPc4
<g/>
,	,	kIx,
argumentujeme	argumentovat	k5eAaImIp1nP
vlastně	vlastně	k9
kruhem	kruh	k1gInSc7
<g/>
:	:	kIx,
„	„	k?
<g/>
Jak	jak	k6eAd1
zjistíme	zjistit	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
starý	starý	k2eAgMnSc1d1
mládenec	mládenec	k1gMnSc1
definuje	definovat	k5eAaBmIp3nS
právě	právě	k6eAd1
jako	jako	k9
neženatý	ženatý	k2eNgMnSc1d1
muž	muž	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
Otevřeme	otevřít	k5eAaPmIp1nP
<g/>
-li	-li	k?
slovník	slovník	k1gInSc4
s	s	k7c7
lexikografickou	lexikografický	k2eAgFnSc7d1
definicí	definice	k1gFnSc7
<g/>
,	,	kIx,
„	„	k?
<g/>
zapřaháme	zapřahat	k5eAaImIp1nP
povoz	povoz	k1gInSc4
před	před	k7c4
koně	kůň	k1gMnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
i	i	k9
lexikograf	lexikograf	k1gMnSc1
je	být	k5eAaImIp3nS
empirický	empirický	k2eAgMnSc1d1
vědec	vědec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
registruje	registrovat	k5eAaBmIp3nS
smyslově	smyslově	k6eAd1
daná	daný	k2eAgNnPc4d1
fakta	faktum	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Máme	mít	k5eAaImIp1nP
tedy	tedy	k8xC
být	být	k5eAaImF
odkázáni	odkázat	k5eAaPmNgMnP
na	na	k7c4
analýzu	analýza	k1gFnSc4
jazykového	jazykový	k2eAgNnSc2d1
chování	chování	k1gNnSc2
(	(	kIx(
<g/>
odtud	odtud	k6eAd1
Quineův	Quineův	k2eAgInSc1d1
behaviorismus	behaviorismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Druhým	druhý	k4xOgNnSc7
dogmatem	dogma	k1gNnSc7
dosavadního	dosavadní	k2eAgInSc2d1
empirismu	empirismus	k1gInSc2
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
redukcionismus	redukcionismus	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
těsně	těsně	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
předchozím	předchozí	k2eAgInSc7d1
problémem	problém	k1gInSc7
<g/>
:	:	kIx,
jde	jít	k5eAaImIp3nS
o	o	k7c4
přesvědčení	přesvědčení	k1gNnSc4
<g/>
,	,	kIx,
že	že	k8xS
výrok	výrok	k1gInSc4
izolovaný	izolovaný	k2eAgInSc4d1
od	od	k7c2
ostatních	ostatní	k1gNnPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
potvrzen	potvrdit	k5eAaPmNgInS
nebo	nebo	k8xC
vyvrácen	vyvrátit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
„	„	k?
<g/>
tribunál	tribunál	k1gInSc1
smyslů	smysl	k1gInPc2
<g/>
“	“	k?
však	však	k9
dle	dle	k7c2
Quinea	Quine	k1gInSc2
nemají	mít	k5eNaImIp3nP
přestupovat	přestupovat	k5eAaImF
termíny	termín	k1gInPc1
či	či	k8xC
výroky	výrok	k1gInPc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
celé	celý	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Jednotkou	jednotka	k1gFnSc7
empirického	empirický	k2eAgInSc2d1
významu	význam	k1gInSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
mě	já	k3xPp1nSc4
věda	věda	k1gFnSc1
jakožto	jakožto	k8xS
celek	celek	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Tomuto	tento	k3xDgInSc3
postoji	postoj	k1gInSc3
se	se	k3xPyFc4
později	pozdě	k6eAd2
dostalo	dostat	k5eAaPmAgNnS
označení	označení	k1gNnSc3
konfirmační	konfirmační	k2eAgInSc4d1
holismus	holismus	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Ontologická	ontologický	k2eAgFnSc1d1
relativita	relativita	k1gFnSc1
</s>
<s>
Tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
Quine	Quin	k1gInSc5
domnívá	domnívat	k5eAaImIp3nS
strhnout	strhnout	k5eAaPmF
hranici	hranice	k1gFnSc4
mezi	mezi	k7c7
metafyzikou	metafyzika	k1gFnSc7
a	a	k8xC
přírodní	přírodní	k2eAgFnSc7d1
vědou	věda	k1gFnSc7
a	a	k8xC
dospívá	dospívat	k5eAaImIp3nS
k	k	k7c3
ontologickému	ontologický	k2eAgInSc3d1
relativismu	relativismus	k1gInSc3
<g/>
:	:	kIx,
fyzikální	fyzikální	k2eAgInPc1d1
objekty	objekt	k1gInPc1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
„	„	k?
<g/>
vhodné	vhodný	k2eAgMnPc4d1
zprostředkující	zprostředkující	k2eAgMnPc4d1
členy	člen	k1gMnPc4
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
neredukovatelné	redukovatelný	k2eNgInPc4d1
postuláty	postulát	k1gInPc4
epistemologicky	epistemologicky	k6eAd1
srovnatelné	srovnatelný	k2eAgInPc4d1
s	s	k7c7
Homérovými	Homérův	k2eAgMnPc7d1
bohy	bůh	k1gMnPc7
<g/>
“	“	k?
–	–	k?
obojí	oboj	k1gFnSc7
prý	prý	k9
vnímáme	vnímat	k5eAaImIp1nP
jako	jako	k9
určité	určitý	k2eAgInPc4d1
kulturní	kulturní	k2eAgInPc4d1
postuláty	postulát	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
jen	jen	k9
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
nakolik	nakolik	k6eAd1
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
vštípit	vštípit	k5eAaPmF
naší	náš	k3xOp1gFnSc3
zkušenosti	zkušenost	k1gFnSc3
„	„	k?
<g/>
zvládnutelnou	zvládnutelný	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Naturalizovaná	naturalizovaný	k2eAgFnSc1d1
epistemologie	epistemologie	k1gFnSc1
</s>
<s>
Epistemologii	epistemologie	k1gFnSc4
Quine	Quin	k1gInSc5
„	„	k?
<g/>
naturalizuje	naturalizovat	k5eAaBmIp3nS
<g/>
“	“	k?
a	a	k8xC
chápe	chápat	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
jako	jako	k9
součást	součást	k1gFnSc1
psychologie	psychologie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
u	u	k7c2
něj	on	k3xPp3gMnSc2
přírodní	přírodní	k2eAgFnSc1d1
vědou	věda	k1gFnSc7
<g/>
,	,	kIx,
opět	opět	k6eAd1
vycházející	vycházející	k2eAgFnSc1d1
ze	z	k7c2
smyslové	smyslový	k2eAgFnSc2d1
zkušenosti	zkušenost	k1gFnSc2
(	(	kIx(
<g/>
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
se	se	k3xPyFc4
tak	tak	k9
mj.	mj.	kA
proti	proti	k7c3
psychologii	psychologie	k1gFnSc3
gestaltu	gestalt	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
každá	každý	k3xTgFnSc1
věda	věda	k1gFnSc1
o	o	k7c6
vnějším	vnější	k2eAgInSc6d1
světě	svět	k1gInSc6
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
odvozena	odvodit	k5eAaPmNgFnS
čistě	čistě	k6eAd1
ze	z	k7c2
smyslové	smyslový	k2eAgFnSc2d1
evidence	evidence	k1gFnSc2
<g/>
,	,	kIx,
nemá	mít	k5eNaImIp3nS
si	se	k3xPyFc3
epistemologie	epistemologie	k1gFnSc1
nárokovat	nárokovat	k5eAaImF
postavení	postavení	k1gNnSc2
jakési	jakýsi	k3yIgFnSc2
„	„	k?
<g/>
metavědy	metavěda	k1gFnSc2
<g/>
“	“	k?
či	či	k8xC
první	první	k4xOgFnSc1
filosofie	filosofie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
filosofie	filosofie	k1gFnSc2
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
nastíněno	nastíněn	k2eAgNnSc1d1
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
knize	kniha	k1gFnSc6
Od	od	k7c2
stimulu	stimul	k1gInSc2
k	k	k7c3
vědě	věda	k1gFnSc3
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
zkoumat	zkoumat	k5eAaImF
<g/>
,	,	kIx,
„	„	k?
<g/>
jak	jak	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
z	z	k7c2
nárazů	náraz	k1gInPc2
částic	částice	k1gFnPc2
a	a	k8xC
vln	vlna	k1gFnPc2
na	na	k7c4
naše	naši	k1gMnPc4
smyslové	smyslový	k2eAgInPc1d1
receptory	receptor	k1gInPc1
zrodit	zrodit	k5eAaPmF
něco	něco	k3yInSc4
tak	tak	k9
grandiózního	grandiózní	k2eAgNnSc2d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
naše	náš	k3xOp1gFnPc4
teorie	teorie	k1gFnPc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
vyjadřující	vyjadřující	k2eAgMnSc1d1
naše	náš	k3xOp1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
poznání	poznání	k1gNnSc1
tohoto	tento	k3xDgInSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
tomu	ten	k3xDgNnSc3
slouží	sloužit	k5eAaImIp3nS
analýza	analýza	k1gFnSc1
tzv.	tzv.	kA
pozorovacích	pozorovací	k2eAgFnPc2d1
vět	věta	k1gFnPc2
<g/>
,	,	kIx,
„	„	k?
<g/>
o	o	k7c6
jejichž	jejichž	k3xOyRp3gFnSc6
pravdivosti	pravdivost	k1gFnSc6
budou	být	k5eAaImBp3nP
všichni	všechen	k3xTgMnPc1
mluvčí	mluvčí	k1gMnPc1
určitého	určitý	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
rozhodovat	rozhodovat	k5eAaImF
stejně	stejně	k6eAd1
<g/>
,	,	kIx,
budou	být	k5eAaImBp3nP
<g/>
-li	-li	k?
vystaveni	vystavit	k5eAaPmNgMnP
stejnému	stejný	k2eAgNnSc3d1
podráždění	podráždění	k1gNnSc3
(	(	kIx(
<g/>
stimulu	stimul	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Působení	působení	k1gNnSc1
</s>
<s>
Zatímco	zatímco	k8xS
dosah	dosah	k1gInSc1
Quineova	Quineův	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
zejména	zejména	k9
v	v	k7c6
analyticky	analyticky	k6eAd1
zaměřené	zaměřený	k2eAgFnSc3d1
filosofii	filosofie	k1gFnSc3
je	být	k5eAaImIp3nS
zásadní	zásadní	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
kontinentální	kontinentální	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
příliš	příliš	k6eAd1
rozruchu	rozrucha	k1gFnSc4
nevzbudil	vzbudit	k5eNaPmAgInS
<g/>
;	;	kIx,
v	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
neznámý	známý	k2eNgMnSc1d1
(	(	kIx(
<g/>
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
současných	současný	k2eAgMnPc2d1
badatelů	badatel	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc7
dílem	díl	k1gInSc7
zabývá	zabývat	k5eAaImIp3nS
a	a	k8xC
překládá	překládat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Jaroslav	Jaroslav	k1gMnSc1
Peregrin	Peregrin	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
Quineovy	Quineův	k2eAgMnPc4d1
žáky	žák	k1gMnPc4
a	a	k8xC
následovníky	následovník	k1gMnPc4
patří	patřit	k5eAaImIp3nS
např.	např.	kA
Donald	Donald	k1gMnSc1
Davidson	Davidson	k1gMnSc1
<g/>
,	,	kIx,
Jaakko	Jaakko	k1gNnSc1
Hintikka	Hintikka	k1gMnSc1
či	či	k8xC
David	David	k1gMnSc1
Lewis	Lewis	k1gFnSc2
<g/>
;	;	kIx,
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
analytické	analytický	k2eAgFnSc2d1
filosofie	filosofie	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
post-analytické	post-analytický	k2eAgFnSc6d1
filosofii	filosofie	k1gFnSc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
kritické	kritický	k2eAgInPc1d1
hlasy	hlas	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
Jaegwona	Jaegwona	k1gFnSc1
Kima	Kim	k1gInSc2
či	či	k8xC
Hilary	Hilara	k1gFnSc2
Putnama	Putnama	k?
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
1951	#num#	k4
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mathematical	Mathematical	k1gMnSc1
Logic	Logic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
674	#num#	k4
<g/>
-	-	kIx~
<g/>
55451	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Selected	Selected	k1gMnSc1
Logic	Logic	k1gMnSc1
Papers	Papers	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Web	web	k1gInSc4
of	of	k?
Belief	Belief	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Random	Random	k1gInSc1
House	house	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elementary	Elementara	k1gFnSc2
Logic	Logice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
674	#num#	k4
<g/>
-	-	kIx~
<g/>
24451	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Methods	Methods	k1gInSc1
of	of	k?
Logic	Logice	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
From	Froma	k1gFnPc2
a	a	k8xC
Logical	Logical	k1gFnPc2
Point	pointa	k1gFnPc2
of	of	k?
View	View	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
674	#num#	k4
<g/>
-	-	kIx~
<g/>
32351	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
stať	stať	k1gFnSc4
"	"	kIx"
<g/>
Two	Two	k1gMnSc1
dogmas	dogmas	k1gMnSc1
of	of	k?
Empiricism	Empiricism	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
</s>
<s>
1960	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Word	Word	kA
and	and	k?
Object	Object	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
MIT	MIT	kA
Press	Pressa	k1gFnPc2
<g/>
;	;	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
262	#num#	k4
<g/>
-	-	kIx~
<g/>
67001	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ontological	Ontological	k1gFnSc1
Relativity	relativita	k1gFnSc2
and	and	k?
Other	Other	k1gInSc1
Essays	Essays	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Columbia	Columbia	k1gFnSc1
Univ	Univa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
231	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8357	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
významné	významný	k2eAgFnPc4d1
kapitoly	kapitola	k1gFnPc4
k	k	k7c3
ontologickému	ontologický	k2eAgInSc3d1
relativismu	relativismus	k1gInSc3
<g/>
,	,	kIx,
naturalizované	naturalizovaný	k2eAgFnSc6d1
epistemologii	epistemologie	k1gFnSc6
a	a	k8xC
teorii	teorie	k1gFnSc6
přírodních	přírodní	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Set	set	k1gInSc1
Theory	Theora	k1gFnSc2
and	and	k?
Its	Its	k1gMnSc1
Logic	Logic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
(	(	kIx(
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Philosophy	Philosopha	k1gFnSc2
of	of	k?
Logic	Logic	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Time	Time	k1gFnSc1
of	of	k?
My	my	k3xPp1nPc1
Life	Life	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
His	his	k1gNnSc7
autobiography	autobiographa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Quiddities	Quiddities	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gFnSc1
Intermittently	Intermittently	k1gFnSc2
Philosophical	Philosophical	k1gFnSc2
Dictionary	Dictionara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
12522	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Humorné	humorný	k2eAgInPc1d1
<g/>
,	,	kIx,
oddechové	oddechový	k2eAgNnSc1d1
dílko	dílko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pursuit	Pursuit	k1gMnSc1
of	of	k?
Truth	Truth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvard	Harvard	k1gInSc1
Univ	Univ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátká	krátký	k2eAgFnSc1d1
syntéza	syntéza	k1gFnSc1
Quineova	Quineův	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
674	#num#	k4
<g/>
-	-	kIx~
<g/>
73951	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česky	česky	k6eAd1
1994	#num#	k4
(	(	kIx(
<g/>
Hledání	hledání	k1gNnSc1
pravdy	pravda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
From	Fro	k1gNnSc7
Stimules	Stimulesa	k1gFnPc2
to	ten	k3xDgNnSc1
Science	Science	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česky	česky	k6eAd1
2002	#num#	k4
(	(	kIx(
<g/>
Od	od	k7c2
stimulu	stimul	k1gInSc2
k	k	k7c3
vědě	věda	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Kolektiv	kolektivum	k1gNnPc2
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
Filosofický	filosofický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
Olomouc	Olomouc	k1gFnSc1
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
Willard	Willarda	k1gFnPc2
Van	vana	k1gFnPc2
Orman	Orman	k1gInSc1
Quine	Quin	k1gInSc5
<g/>
↑	↑	k?
Akademický	akademický	k2eAgInSc1d1
bulletin	bulletin	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Willard	Willard	k1gMnSc1
Van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dvě	dva	k4xCgNnPc4
dogmata	dogma	k1gNnPc4
empirismu	empirismus	k1gInSc2
<g/>
,	,	kIx,
§	§	k?
2	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dvě	dva	k4xCgNnPc4
dogmata	dogma	k1gNnPc4
empirismu	empirismus	k1gInSc2
<g/>
,	,	kIx,
§	§	k?
5	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dvě	dva	k4xCgNnPc4
dogmata	dogma	k1gNnPc4
empirismu	empirismus	k1gInSc2
<g/>
,	,	kIx,
§	§	k?
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
J.	J.	kA
Peregrin	Peregrin	k1gInSc1
<g/>
,	,	kIx,
úvod	úvod	k1gInSc1
k	k	k7c3
českému	český	k2eAgInSc3d1
překladu	překlad	k1gInSc3
Od	od	k7c2
stimulu	stimul	k1gInSc2
k	k	k7c3
vědě	věda	k1gFnSc3
<g/>
↑	↑	k?
Viz	vidět	k5eAaImRp2nS
Naturalizace	naturalizace	k1gFnPc4
epistemologie	epistemologie	k1gFnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
T.	T.	kA
Marvan	Marvan	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Willard	Willard	k1gMnSc1
Van	vana	k1gFnPc2
Orman	Orman	k1gMnSc1
Quine	Quin	k1gInSc5
<g/>
—	—	k?
<g/>
Philosopher	Philosophra	k1gFnPc2
and	and	k?
Mathematician	Mathematiciana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stránky	stránka	k1gFnSc2
spravované	spravovaný	k2eAgFnSc2d1
Quineovým	Quineův	k2eAgMnSc7d1
synem	syn	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Text	text	k1gInSc1
článku	článek	k1gInSc2
"	"	kIx"
<g/>
Two	Two	k1gMnSc1
Dogmas	Dogmas	k1gMnSc1
of	of	k?
Empiricism	Empiricism	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
Axiomy	axiom	k1gInPc1
</s>
<s>
axiom	axiom	k1gInSc1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc4
spočetného	spočetný	k2eAgInSc2d1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc4
závislého	závislý	k2eAgInSc2d1
výběru	výběr	k1gInSc2
•	•	k?
axiom	axiom	k1gInSc1
extenzionality	extenzionalita	k1gFnSc2
•	•	k?
axiom	axiom	k1gInSc1
nekonečna	nekonečno	k1gNnSc2
•	•	k?
axiom	axiom	k1gInSc1
dvojice	dvojice	k1gFnSc1
•	•	k?
axiom	axiom	k1gInSc4
potenční	potenční	k2eAgFnSc2d1
množiny	množina	k1gFnSc2
•	•	k?
Axiom	axiom	k1gInSc1
regulárnosti	regulárnost	k1gFnSc2
•	•	k?
axiom	axiom	k1gInSc1
sumy	suma	k1gFnSc2
•	•	k?
schéma	schéma	k1gNnSc1
nahrazení	nahrazení	k1gNnSc2
•	•	k?
schéma	schéma	k1gNnSc1
axiomů	axiom	k1gInPc2
vydělení	vydělení	k1gNnSc2
•	•	k?
hypotéza	hypotéza	k1gFnSc1
kontinua	kontinuum	k1gNnSc2
•	•	k?
Martinův	Martinův	k2eAgInSc4d1
axiom	axiom	k1gInSc4
•	•	k?
velké	velký	k2eAgMnPc4d1
kardinály	kardinál	k1gMnPc4
Množinové	množinový	k2eAgFnSc2d1
operace	operace	k1gFnSc2
</s>
<s>
doplněk	doplněk	k1gInSc1
•	•	k?
de	de	k?
Morganovy	morganův	k2eAgInPc4d1
zákony	zákon	k1gInPc4
•	•	k?
kartézský	kartézský	k2eAgInSc1d1
součin	součin	k1gInSc1
•	•	k?
potenční	potenční	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
průnik	průnik	k1gInSc1
•	•	k?
rozdíl	rozdíl	k1gInSc1
množin	množina	k1gFnPc2
•	•	k?
sjednocení	sjednocení	k1gNnSc6
•	•	k?
symetrická	symetrický	k2eAgFnSc1d1
diference	diference	k1gFnSc1
Koncepty	koncept	k1gInPc4
</s>
<s>
bijekce	bijekce	k1gFnSc1
•	•	k?
kardinální	kardinální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
•	•	k?
konstruovatelná	konstruovatelný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
mohutnost	mohutnost	k1gFnSc1
•	•	k?
ordinální	ordinální	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
•	•	k?
prvek	prvek	k1gInSc1
množiny	množina	k1gFnSc2
•	•	k?
rodina	rodina	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
transfinitní	transfinitní	k2eAgFnSc2d1
indukce	indukce	k1gFnSc2
•	•	k?
třída	třída	k1gFnSc1
•	•	k?
Vennův	Vennův	k2eAgInSc1d1
diagram	diagram	k1gInSc1
Množiny	množina	k1gFnSc2
</s>
<s>
Cantorova	Cantorův	k2eAgFnSc1d1
diagonální	diagonální	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
•	•	k?
fuzzy	fuzza	k1gFnSc2
množina	množina	k1gFnSc1
•	•	k?
konečná	konečný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
nekonečná	konečný	k2eNgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
nespočetná	spočetný	k2eNgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
podmnožina	podmnožina	k1gFnSc1
•	•	k?
prázdná	prázdný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
rekurzivní	rekurzivní	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
spočetná	spočetný	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
tranzitivní	tranzitivní	k2eAgFnSc1d1
množina	množina	k1gFnSc1
•	•	k?
univerzální	univerzální	k2eAgFnSc1d1
množina	množina	k1gFnSc1
Teorie	teorie	k1gFnSc1
</s>
<s>
axiomatická	axiomatický	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Cantorova	Cantorův	k2eAgFnSc1d1
věta	věta	k1gFnSc1
•	•	k?
forsing	forsing	k1gInSc1
•	•	k?
Kelleyova	Kelleyov	k1gInSc2
<g/>
–	–	k?
<g/>
Morseova	Morseův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
naivní	naivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
•	•	k?
New	New	k1gFnSc2
Foundations	Foundationsa	k1gFnPc2
•	•	k?
paradoxy	paradox	k1gInPc1
naivní	naivní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
•	•	k?
Principia	principium	k1gNnSc2
Mathematica	Mathematicus	k1gMnSc2
•	•	k?
Russellův	Russellův	k2eAgInSc4d1
paradox	paradox	k1gInSc4
•	•	k?
Suslinova	Suslinův	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
•	•	k?
Von	von	k1gInSc1
Neumannova	Neumannův	k2eAgInSc2d1
<g/>
–	–	k?
<g/>
Bernaysova	Bernaysův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Gödelova	Gödelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Zermelova	Zermelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
Zermelova	Zermelův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Fraenkelova	Fraenkelův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
množin	množina	k1gFnPc2
•	•	k?
alternativní	alternativní	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
Lidé	člověk	k1gMnPc1
</s>
<s>
Abraham	Abraham	k1gMnSc1
Fraenkel	Fraenkel	k1gMnSc1
•	•	k?
Bertrand	Bertrand	k1gInSc1
Russell	Russell	k1gMnSc1
•	•	k?
Ernst	Ernst	k1gMnSc1
Zermelo	Zermela	k1gFnSc5
•	•	k?
Georg	Georg	k1gMnSc1
Cantor	Cantor	k1gMnSc1
•	•	k?
John	John	k1gMnSc1
von	von	k1gInSc1
Neumann	Neumann	k1gMnSc1
•	•	k?
Kurt	Kurt	k1gMnSc1
Gödel	Gödel	k1gMnSc1
•	•	k?
Lotfi	Lotf	k1gFnSc2
Zadeh	Zadeh	k1gMnSc1
•	•	k?
Paul	Paul	k1gMnSc1
Bernays	Bernaysa	k1gFnPc2
•	•	k?
Paul	Paul	k1gMnSc1
Cohen	Cohen	k2eAgMnSc1d1
•	•	k?
Petr	Petr	k1gMnSc1
Vopěnka	Vopěnka	k1gFnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Dedekind	Dedekind	k1gMnSc1
•	•	k?
Thomas	Thomas	k1gMnSc1
Jech	Jech	k1gMnSc1
•	•	k?
Willard	Willard	k1gMnSc1
Quine	Quin	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Filosofie	filosofie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19992000929	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118641824	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8366	#num#	k4
4021	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
80061808	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27070554	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
80061808	#num#	k4
</s>
