<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Venezuely	Venezuela	k1gFnSc2	Venezuela
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
stejné	stejný	k2eAgFnSc2d1	stejná
šířky	šířka	k1gFnSc2	šířka
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
modra	modro	k1gNnSc2	modro
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
