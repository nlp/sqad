<s>
Strašilky	strašilka	k1gFnPc1	strašilka
(	(	kIx(	(
<g/>
Phasmatodea	Phasmatode	k2eAgFnSc1d1	Phasmatode
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
řád	řád	k1gInSc4	řád
tropického	tropický	k2eAgInSc2d1	tropický
hmyzu	hmyz	k1gInSc2	hmyz
bizarního	bizarní	k2eAgInSc2d1	bizarní
zjevu	zjev	k1gInSc2	zjev
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
vypadat	vypadat	k5eAaImF	vypadat
jako	jako	k9	jako
větvička	větvička	k1gFnSc1	větvička
<g/>
,	,	kIx,	,
starý	starý	k2eAgInSc1d1	starý
list	list	k1gInSc1	list
nebo	nebo	k8xC	nebo
kus	kus	k1gInSc1	kus
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
dokonale	dokonale	k6eAd1	dokonale
tak	tak	k6eAd1	tak
splývat	splývat	k5eAaImF	splývat
s	s	k7c7	s
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výlučně	výlučně	k6eAd1	výlučně
býložravý	býložravý	k2eAgInSc4d1	býložravý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
tropických	tropický	k2eAgFnPc6d1	tropická
a	a	k8xC	a
subtropických	subtropický	k2eAgFnPc6d1	subtropická
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
vlhčích	vlhký	k2eAgInPc6d2	vlhčí
lesních	lesní	k2eAgInPc6d1	lesní
biomech	biom	k1gInPc6	biom
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
druhů	druh	k1gInPc2	druh
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
Malajsii	Malajsie	k1gFnSc6	Malajsie
(	(	kIx(	(
<g/>
Indomalajská	Indomalajský	k2eAgFnSc1d1	Indomalajský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
osídlily	osídlit	k5eAaPmAgInP	osídlit
i	i	k9	i
teplejší	teplý	k2eAgFnPc1d2	teplejší
části	část	k1gFnPc1	část
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
zavlečené	zavlečený	k2eAgInPc1d1	zavlečený
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
přežívat	přežívat	k5eAaImF	přežívat
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
původní	původní	k2eAgInPc4d1	původní
druhy	druh	k1gInPc4	druh
pakobylek	pakobylka	k1gFnPc2	pakobylka
Bacillus	Bacillus	k1gMnSc1	Bacillus
rossius	rossius	k1gMnSc1	rossius
a	a	k8xC	a
Clonopsis	Clonopsis	k1gFnSc1	Clonopsis
gallica	gallic	k1gInSc2	gallic
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
strašilek	strašilka	k1gFnPc2	strašilka
je	být	k5eAaImIp3nS	být
větších	veliký	k2eAgFnPc2d2	veliký
než	než	k8xS	než
50	[number]	k4	50
mm	mm	kA	mm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
najdou	najít	k5eAaPmIp3nP	najít
se	se	k3xPyFc4	se
i	i	k9	i
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
až	až	k9	až
do	do	k7c2	do
velikostí	velikost	k1gFnPc2	velikost
větších	veliký	k2eAgFnPc2d2	veliký
než	než	k8xS	než
250	[number]	k4	250
mm	mm	kA	mm
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
přes	přes	k7c4	přes
600	[number]	k4	600
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
strašilek	strašilka	k1gFnPc2	strašilka
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnPc1	jejich
křídla	křídlo	k1gNnPc1	křídlo
jsou	být	k5eAaImIp3nP	být
zkrácená	zkrácený	k2eAgNnPc1d1	zkrácené
či	či	k8xC	či
zcela	zcela	k6eAd1	zcela
zakrnělá	zakrnělý	k2eAgFnSc1d1	zakrnělá
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
mohou	moct	k5eAaImIp3nP	moct
létat	létat	k5eAaImF	létat
samci	samec	k1gMnPc1	samec
a	a	k8xC	a
samice	samice	k1gFnSc1	samice
nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
nelétají	létat	k5eNaImIp3nP	létat
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
druhy	druh	k1gInPc1	druh
s	s	k7c7	s
letuschopnými	letuschopný	k2eAgFnPc7d1	letuschopná
samicemi	samice	k1gFnPc7	samice
<g/>
.	.	kIx.	.
</s>
<s>
Strašilky	strašilka	k1gFnPc1	strašilka
jsou	být	k5eAaImIp3nP	být
morfologicky	morfologicky	k6eAd1	morfologicky
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
typ	typ	k1gInSc4	typ
hůlkovitý	hůlkovitý	k2eAgInSc4d1	hůlkovitý
nebo	nebo	k8xC	nebo
větévkovitý	větévkovitý	k2eAgInSc4d1	větévkovitý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
neobyčejně	obyčejně	k6eNd1	obyčejně
úzký	úzký	k2eAgInSc1d1	úzký
<g/>
,	,	kIx,	,
štíhlý	štíhlý	k2eAgMnSc1d1	štíhlý
a	a	k8xC	a
dlouhonohý	dlouhonohý	k2eAgMnSc1d1	dlouhonohý
-	-	kIx~	-
tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
označujeme	označovat	k5eAaImIp1nP	označovat
obecně	obecně	k6eAd1	obecně
jako	jako	k9	jako
pakobylky	pakobylka	k1gFnSc2	pakobylka
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
typ	typ	k1gInSc4	typ
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
a	a	k8xC	a
robustnější	robustní	k2eAgFnSc1d2	robustnější
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
trny	trn	k1gInPc7	trn
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgInPc7d1	jiný
výrůstky	výrůstek	k1gInPc7	výrůstek
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
neobvykle	obvykle	k6eNd1	obvykle
velikými	veliký	k2eAgFnPc7d1	veliká
a	a	k8xC	a
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
typy	typ	k1gInPc1	typ
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
strašilky	strašilka	k1gFnPc1	strašilka
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
typ	typ	k1gInSc1	typ
připomíná	připomínat	k5eAaImIp3nS	připomínat
jak	jak	k8xC	jak
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
tvarem	tvar	k1gInSc7	tvar
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
mimetický	mimetický	k2eAgInSc1d1	mimetický
–	–	k?	–
takové	takový	k3xDgInPc4	takový
druhy	druh	k1gInPc4	druh
obecně	obecně	k6eAd1	obecně
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
lupenitky	lupenitka	k1gFnPc1	lupenitka
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc4d1	české
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
lupenitky	lupenitka	k1gFnSc2	lupenitka
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
kryje	krýt	k5eAaImIp3nS	krýt
s	s	k7c7	s
taxonomickým	taxonomický	k2eAgInSc7d1	taxonomický
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
označuje	označovat	k5eAaImIp3nS	označovat
přesně	přesně	k6eAd1	přesně
danou	daný	k2eAgFnSc4d1	daná
čeleď	čeleď	k1gFnSc4	čeleď
Phylliidae	Phylliida	k1gFnSc2	Phylliida
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
podčeleď	podčeleď	k1gFnSc1	podčeleď
Phylliinae	Phylliinae	k1gFnSc1	Phylliinae
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
použitého	použitý	k2eAgInSc2d1	použitý
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
označení	označení	k1gNnSc3	označení
"	"	kIx"	"
<g/>
pakobylka	pakobylka	k1gFnSc1	pakobylka
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nekryje	krýt	k5eNaImIp3nS	krýt
s	s	k7c7	s
taxonomickým	taxonomický	k2eAgInSc7d1	taxonomický
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
popisuje	popisovat	k5eAaImIp3nS	popisovat
pouze	pouze	k6eAd1	pouze
vzhled	vzhled	k1gInSc4	vzhled
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
názor	názor	k1gInSc4	názor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
strašilka	strašilka	k1gFnSc1	strašilka
a	a	k8xC	a
co	co	k3yQnSc4	co
už	už	k9	už
pakobylka	pakobylka	k1gFnSc1	pakobylka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
subjektivně	subjektivně	k6eAd1	subjektivně
různit	různit	k5eAaImF	různit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvotním	prvotní	k2eAgInSc7d1	prvotní
ochranným	ochranný	k2eAgInSc7d1	ochranný
mechanismem	mechanismus	k1gInSc7	mechanismus
strašilek	strašilka	k1gFnPc2	strašilka
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc4	jejich
kryptické	kryptický	k2eAgNnSc4d1	kryptické
zbarvení	zbarvení	k1gNnSc4	zbarvení
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgMnSc3	který
splývají	splývat	k5eAaImIp3nP	splývat
s	s	k7c7	s
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
často	často	k6eAd1	často
ještě	ještě	k6eAd1	ještě
umocňují	umocňovat	k5eAaImIp3nP	umocňovat
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
pakobylek	pakobylka	k1gFnPc2	pakobylka
(	(	kIx(	(
<g/>
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
větvičky	větvička	k1gFnPc1	větvička
<g/>
)	)	kIx)	)
přikládá	přikládat	k5eAaImIp3nS	přikládat
přední	přední	k2eAgFnPc4d1	přední
nohy	noha	k1gFnPc4	noha
k	k	k7c3	k
sobě	se	k3xPyFc3	se
natažené	natažený	k2eAgFnSc6d1	natažená
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
opticky	opticky	k6eAd1	opticky
prodlouží	prodloužit	k5eAaPmIp3nS	prodloužit
"	"	kIx"	"
<g/>
větvičku	větvička	k1gFnSc4	větvička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaPmIp3nP	vydávat
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
strašilek	strašilka	k1gFnPc2	strašilka
se	se	k3xPyFc4	se
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
pohupuje	pohupovat	k5eAaImIp3nS	pohupovat
a	a	k8xC	a
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
tak	tak	k9	tak
větvičku	větvička	k1gFnSc4	větvička
nebo	nebo	k8xC	nebo
list	list	k1gInSc4	list
ve	v	k7c6	v
větru	vítr	k1gInSc6	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvě	čerstvě	k6eAd1	čerstvě
vylíhlé	vylíhlý	k2eAgFnPc4d1	vylíhlá
nymfy	nymfa	k1gFnPc4	nymfa
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
strašilka	strašilka	k1gFnSc1	strašilka
australská	australský	k2eAgFnSc1d1	australská
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k9	i
způsobem	způsob	k1gInSc7	způsob
chůze	chůze	k1gFnSc2	chůze
mravencům	mravenec	k1gMnPc3	mravenec
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnPc4d2	starší
nymfy	nymfa	k1gFnSc2	nymfa
i	i	k8xC	i
dospělci	dospělec	k1gMnPc1	dospělec
strašilky	strašilka	k1gFnSc2	strašilka
australské	australský	k2eAgFnSc2d1	australská
připomínají	připomínat	k5eAaImIp3nP	připomínat
zase	zase	k9	zase
štíry	štír	k1gMnPc4	štír
(	(	kIx(	(
<g/>
svým	svůj	k3xOyFgInSc7	svůj
zvednutým	zvednutý	k2eAgInSc7d1	zvednutý
zatočeným	zatočený	k2eAgInSc7d1	zatočený
zadečkem	zadeček	k1gInSc7	zadeček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgMnSc1	obojí
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
odrazení	odrazení	k1gNnSc4	odrazení
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
přece	přece	k9	přece
jen	jen	k9	jen
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
<g/>
,	,	kIx,	,
strašilky	strašilka	k1gFnPc1	strašilka
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
záloze	záloha	k1gFnSc6	záloha
další	další	k2eAgInPc4d1	další
mechanismy	mechanismus	k1gInPc4	mechanismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
je	být	k5eAaImIp3nS	být
prostě	prostě	k9	prostě
spadnout	spadnout	k5eAaPmF	spadnout
z	z	k7c2	z
větve	větev	k1gFnSc2	větev
dolů	dolů	k6eAd1	dolů
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
okřídlených	okřídlený	k2eAgInPc2d1	okřídlený
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
odletět	odletět	k5eAaPmF	odletět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
okřídlených	okřídlený	k2eAgInPc2d1	okřídlený
druhů	druh	k1gInPc2	druh
může	moct	k5eAaImIp3nS	moct
ještě	ještě	k9	ještě
před	před	k7c7	před
startem	start	k1gInSc7	start
odhalit	odhalit	k5eAaPmF	odhalit
pestrou	pestrý	k2eAgFnSc4d1	pestrá
kresbu	kresba	k1gFnSc4	kresba
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
páru	pár	k1gInSc6	pár
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
predátora	predátor	k1gMnSc4	predátor
poleká	polekat	k5eAaPmIp3nS	polekat
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
zbraní	zbraň	k1gFnSc7	zbraň
strašilek	strašilka	k1gFnPc2	strašilka
je	být	k5eAaImIp3nS	být
takzvané	takzvaný	k2eAgNnSc1d1	takzvané
kousání	kousání	k1gNnSc1	kousání
zadníma	zadní	k2eAgFnPc7d1	zadní
nohama	noha	k1gFnPc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Dělají	dělat	k5eAaImIp3nP	dělat
to	ten	k3xDgNnSc1	ten
například	například	k6eAd1	například
strašilky	strašilka	k1gFnPc1	strašilka
Heteropteryx	Heteropteryx	k1gInSc1	Heteropteryx
dilata	dilata	k1gFnSc1	dilata
<g/>
,	,	kIx,	,
Extatosoma	Extatosoma	k1gNnSc1	Extatosoma
tiaratum	tiaratum	k1gNnSc1	tiaratum
(	(	kIx(	(
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
australská	australský	k2eAgFnSc1d1	australská
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
rod	rod	k1gInSc4	rod
Eurycantha	Eurycanth	k1gMnSc2	Eurycanth
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
strašilky	strašilka	k1gFnPc1	strašilka
mají	mít	k5eAaImIp3nP	mít
zadní	zadní	k2eAgFnPc1d1	zadní
nohy	noha	k1gFnPc1	noha
porostlé	porostlý	k2eAgInPc4d1	porostlý
trny	trn	k1gInPc4	trn
a	a	k8xC	a
při	při	k7c6	při
napadení	napadení	k1gNnSc6	napadení
se	se	k3xPyFc4	se
nepřítele	nepřítel	k1gMnSc4	nepřítel
snaží	snažit	k5eAaImIp3nS	snažit
sevřít	sevřít	k5eAaPmF	sevřít
mezi	mezi	k7c4	mezi
stehno	stehno	k1gNnSc4	stehno
a	a	k8xC	a
holeň	holeň	k1gFnSc4	holeň
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
druhů	druh	k1gInPc2	druh
Eurycantha	Eurycantha	k1gFnSc1	Eurycantha
calcarata	calcarata	k1gFnSc1	calcarata
nebo	nebo	k8xC	nebo
Eurycantha	Eurycantha	k1gFnSc1	Eurycantha
horrida	horrida	k1gFnSc1	horrida
mohou	moct	k5eAaImIp3nP	moct
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
dokonce	dokonce	k9	dokonce
poranit	poranit	k5eAaPmF	poranit
prst	prst	k1gInSc4	prst
člověka	člověk	k1gMnSc2	člověk
až	až	k6eAd1	až
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
strašilek	strašilka	k1gFnPc2	strašilka
mohou	moct	k5eAaImIp3nP	moct
zase	zase	k9	zase
vypouštět	vypouštět	k5eAaImF	vypouštět
páchnoucí	páchnoucí	k2eAgInSc4d1	páchnoucí
sekret	sekret	k1gInSc4	sekret
nebo	nebo	k8xC	nebo
jej	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
vystřikovat	vystřikovat	k5eAaImF	vystřikovat
na	na	k7c4	na
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
strašilek	strašilka	k1gFnPc2	strašilka
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
nepohlavního	pohlavní	k2eNgNnSc2d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
partenogeneze	partenogeneze	k1gFnSc2	partenogeneze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
partenogenetické	partenogenetický	k2eAgFnSc6d1	partenogenetická
populaci	populace	k1gFnSc6	populace
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
samci	samec	k1gMnPc1	samec
<g/>
;	;	kIx,	;
samice	samice	k1gFnSc1	samice
jen	jen	k9	jen
kladou	klást	k5eAaImIp3nP	klást
neoplodněná	oplodněný	k2eNgNnPc1d1	neoplodněné
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
zase	zase	k9	zase
jen	jen	k9	jen
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
samci	samec	k1gMnPc1	samec
téměř	téměř	k6eAd1	téměř
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
partenogenetické	partenogenetický	k2eAgFnPc1d1	partenogenetická
i	i	k8xC	i
normální	normální	k2eAgFnPc1d1	normální
bisexuální	bisexuální	k2eAgFnPc1d1	bisexuální
populace	populace	k1gFnPc1	populace
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
klimatu	klima	k1gNnSc6	klima
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
bisexuální	bisexuální	k2eAgFnPc1d1	bisexuální
<g/>
.	.	kIx.	.
</s>
<s>
Oplozené	oplozený	k2eAgFnPc1d1	oplozená
samice	samice	k1gFnPc1	samice
většinou	většinou	k6eAd1	většinou
snášejí	snášet	k5eAaImIp3nP	snášet
více	hodně	k6eAd2	hodně
vajíček	vajíčko	k1gNnPc2	vajíčko
a	a	k8xC	a
i	i	k9	i
líhnivost	líhnivost	k1gFnSc1	líhnivost
bývá	bývat	k5eAaImIp3nS	bývat
o	o	k7c4	o
něco	něco	k3yInSc4	něco
vyšší	vysoký	k2eAgMnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
strašilek	strašilka	k1gFnPc2	strašilka
připomínají	připomínat	k5eAaImIp3nP	připomínat
semínka	semínko	k1gNnPc4	semínko
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svrchním	svrchní	k2eAgNnSc6d1	svrchní
pólu	pólo	k1gNnSc6	pólo
vajíčka	vajíčko	k1gNnSc2	vajíčko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
víčko	víčko	k1gNnSc1	víčko
(	(	kIx(	(
<g/>
operculum	operculum	k1gNnSc1	operculum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
líhne	líhnout	k5eAaImIp3nS	líhnout
nymfa	nymfa	k1gFnSc1	nymfa
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
jednoduše	jednoduše	k6eAd1	jednoduše
pouštějí	pouštět	k5eAaImIp3nP	pouštět
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
(	(	kIx(	(
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
australská	australský	k2eAgFnSc1d1	australská
<g/>
)	)	kIx)	)
je	on	k3xPp3gNnSc4	on
vystřelují	vystřelovat	k5eAaImIp3nP	vystřelovat
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
vajíčka	vajíčko	k1gNnSc2	vajíčko
kladou	klást	k5eAaImIp3nP	klást
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
štěrbin	štěrbina	k1gFnPc2	štěrbina
nebo	nebo	k8xC	nebo
do	do	k7c2	do
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
je	on	k3xPp3gMnPc4	on
nalepují	nalepovat	k5eAaImIp3nP	nalepovat
na	na	k7c4	na
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nymfy	nymfa	k1gFnPc1	nymfa
strašilek	strašilka	k1gFnPc2	strašilka
se	se	k3xPyFc4	se
do	do	k7c2	do
dospělosti	dospělost	k1gFnSc2	dospělost
svlékají	svlékat	k5eAaImIp3nP	svlékat
většinou	většinou	k6eAd1	většinou
šestkrát	šestkrát	k6eAd1	šestkrát
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
svlékání	svlékání	k1gNnSc2	svlékání
větší	veliký	k2eAgMnSc1d2	veliký
či	či	k8xC	či
menší	malý	k2eAgMnSc1d2	menší
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
svlékání	svlékání	k1gNnSc4	svlékání
méně	málo	k6eAd2	málo
a	a	k8xC	a
dospívají	dospívat	k5eAaImIp3nP	dospívat
tedy	tedy	k9	tedy
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
svlékáním	svlékání	k1gNnSc7	svlékání
se	se	k3xPyFc4	se
strašilky	strašilka	k1gFnPc1	strašilka
snaží	snažit	k5eAaImIp3nP	snažit
nalézt	nalézt	k5eAaBmF	nalézt
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zavěsí	zavěsit	k5eAaPmIp3nS	zavěsit
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
pod	pod	k7c7	pod
sebou	se	k3xPyFc7	se
dost	dost	k6eAd1	dost
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
nymfa	nymfa	k1gFnSc1	nymfa
zůstane	zůstat	k5eAaPmIp3nS	zůstat
uvězněna	uvěznit	k5eAaPmNgFnS	uvěznit
ve	v	k7c6	v
staré	starý	k2eAgFnSc6d1	stará
kutikule	kutikula	k1gFnSc6	kutikula
a	a	k8xC	a
zahyne	zahynout	k5eAaPmIp3nS	zahynout
<g/>
,	,	kIx,	,
v	v	k7c6	v
lepším	dobrý	k2eAgInSc6d2	lepší
případě	případ	k1gInSc6	případ
přijde	přijít	k5eAaPmIp3nS	přijít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Strašilky	strašilka	k1gFnPc1	strašilka
však	však	k9	však
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
regenerační	regenerační	k2eAgFnSc4d1	regenerační
schopnost	schopnost	k1gFnSc4	schopnost
a	a	k8xC	a
během	během	k7c2	během
tří	tři	k4xCgNnPc2	tři
svlékání	svlékání	k1gNnPc2	svlékání
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
nahradit	nahradit	k5eAaPmF	nahradit
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
nohu	noha	k1gFnSc4	noha
či	či	k8xC	či
tykadlo	tykadlo	k1gNnSc4	tykadlo
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
strašilek	strašilka	k1gFnPc2	strašilka
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jen	jen	k9	jen
zajistit	zajistit	k5eAaPmF	zajistit
správnou	správný	k2eAgFnSc4d1	správná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
typ	typ	k1gInSc4	typ
insektária	insektárium	k1gNnSc2	insektárium
<g/>
,	,	kIx,	,
optimální	optimální	k2eAgFnSc2d1	optimální
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
přísun	přísun	k1gInSc1	přísun
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
potravy	potrava	k1gFnSc2	potrava
vhodného	vhodný	k2eAgInSc2d1	vhodný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
strašilky	strašilka	k1gFnPc1	strašilka
živí	živit	k5eAaImIp3nP	živit
různými	různý	k2eAgFnPc7d1	různá
tropickými	tropický	k2eAgFnPc7d1	tropická
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
nás	my	k3xPp1nPc4	my
těžko	těžko	k6eAd1	těžko
dostupnými	dostupný	k2eAgFnPc7d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
přijímá	přijímat	k5eAaImIp3nS	přijímat
ochotně	ochotně	k6eAd1	ochotně
některé	některý	k3yIgFnPc1	některý
naše	náš	k3xOp1gFnPc1	náš
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ostružiník	ostružiník	k1gInSc1	ostružiník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
některé	některý	k3yIgInPc1	některý
jeho	jeho	k3xOp3gInPc1	jeho
druhy	druh	k1gInPc1	druh
mají	mít	k5eAaImIp3nP	mít
listí	listí	k1gNnSc4	listí
i	i	k9	i
přes	přes	k7c4	přes
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
lze	lze	k6eAd1	lze
krmit	krmit	k5eAaImF	krmit
i	i	k9	i
jinými	jiný	k2eAgFnPc7d1	jiná
rostlinami	rostlina	k1gFnPc7	rostlina
čeledi	čeleď	k1gFnSc2	čeleď
růžovitých	růžovitý	k2eAgFnPc2d1	růžovitý
(	(	kIx(	(
<g/>
maliník	maliník	k1gInSc1	maliník
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
jahodník	jahodník	k1gInSc1	jahodník
<g/>
,	,	kIx,	,
hloh	hloh	k1gInSc1	hloh
<g/>
)	)	kIx)	)
a	a	k8xC	a
třeba	třeba	k6eAd1	třeba
i	i	k9	i
břečťanem	břečťan	k1gInSc7	břečťan
<g/>
,	,	kIx,	,
lískou	líska	k1gFnSc7	líska
<g/>
,	,	kIx,	,
dubem	dub	k1gInSc7	dub
<g/>
,	,	kIx,	,
bukem	buk	k1gInSc7	buk
<g/>
,	,	kIx,	,
břízou	bříza	k1gFnSc7	bříza
<g/>
,	,	kIx,	,
lípou	lípa	k1gFnSc7	lípa
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
přijímají	přijímat	k5eAaImIp3nP	přijímat
jako	jako	k9	jako
náhradní	náhradní	k2eAgFnSc4d1	náhradní
potravu	potrava	k1gFnSc4	potrava
pouze	pouze	k6eAd1	pouze
kapradiny	kapradina	k1gFnSc2	kapradina
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
například	například	k6eAd1	například
rododendron	rododendron	k1gInSc4	rododendron
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
samozřejmě	samozřejmě	k6eAd1	samozřejmě
i	i	k9	i
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nežerou	žrát	k5eNaImIp3nP	žrát
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
svou	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
rostlinu	rostlina	k1gFnSc4	rostlina
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
chovat	chovat	k5eAaImF	chovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
často	často	k6eAd1	často
chované	chovaný	k2eAgInPc1d1	chovaný
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
Aretaon	Aretaon	k1gMnSc1	Aretaon
asperrimus	asperrimus	k1gMnSc1	asperrimus
(	(	kIx(	(
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
drsná	drsný	k2eAgFnSc1d1	drsná
<g/>
)	)	kIx)	)
Bacillus	Bacillus	k1gMnSc1	Bacillus
rossius	rossius	k1gMnSc1	rossius
(	(	kIx(	(
<g/>
pakobylka	pakobylka	k1gFnSc1	pakobylka
vyzáblá	vyzáblý	k2eAgFnSc1d1	vyzáblá
<g/>
)	)	kIx)	)
Baculum	Baculum	k1gInSc1	Baculum
extradentatum	extradentatum	k1gNnSc1	extradentatum
(	(	kIx(	(
<g/>
pakobylka	pakobylka	k1gFnSc1	pakobylka
rohatá	rohatý	k2eAgFnSc1d1	rohatá
<g/>
)	)	kIx)	)
Carausius	Carausius	k1gMnSc1	Carausius
morosus	morosus	k1gMnSc1	morosus
(	(	kIx(	(
<g/>
pakobylka	pakobylka	k1gFnSc1	pakobylka
indická	indický	k2eAgFnSc1d1	indická
<g/>
)	)	kIx)	)
Eurycantha	Eurycantha	k1gFnSc1	Eurycantha
calcarata	calcarata	k1gFnSc1	calcarata
(	(	kIx(	(
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
ostruhatá	ostruhatý	k2eAgFnSc1d1	ostruhatá
<g/>
)	)	kIx)	)
Extatosoma	Extatosoma	k1gFnSc1	Extatosoma
tiaratum	tiaratum	k1gNnSc4	tiaratum
(	(	kIx(	(
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
australská	australský	k2eAgFnSc1d1	australská
<g/>
)	)	kIx)	)
Heteropteryx	Heteropteryx	k1gInSc1	Heteropteryx
dilatata	dilatata	k1gFnSc1	dilatata
(	(	kIx(	(
<g/>
strašilka	strašilka	k1gFnSc1	strašilka
obrovská	obrovský	k2eAgFnSc1d1	obrovská
<g/>
)	)	kIx)	)
Phyllium	Phyllium	k1gNnSc1	Phyllium
bioculatum	bioculatum	k1gNnSc1	bioculatum
(	(	kIx(	(
<g/>
lupenitka	lupenitka	k1gFnSc1	lupenitka
dvouoká	dvouoký	k2eAgFnSc1d1	dvouoká
<g/>
)	)	kIx)	)
</s>
