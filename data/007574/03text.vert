<s>
Brighton	Brighton	k1gInSc1	Brighton
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hove	Hove	k1gFnSc7	Hove
tvoří	tvořit	k5eAaImIp3nS	tvořit
souměstí	souměstí	k1gNnSc1	souměstí
Brighton	Brighton	k1gInSc1	Brighton
and	and	k?	and
Hove	Hove	k1gInSc1	Hove
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
a	a	k8xC	a
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
pobřežních	pobřežní	k2eAgNnPc2d1	pobřežní
letovisek	letovisko	k1gNnPc2	letovisko
Velké	velká	k1gFnSc2	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
je	být	k5eAaImIp3nS	být
populárním	populární	k2eAgInSc7d1	populární
turistickým	turistický	k2eAgInSc7d1	turistický
centrem	centr	k1gInSc7	centr
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
hotely	hotel	k1gInPc7	hotel
<g/>
,	,	kIx,	,
restauracemi	restaurace	k1gFnPc7	restaurace
a	a	k8xC	a
zábavními	zábavní	k2eAgNnPc7d1	zábavní
středisky	středisko	k1gNnPc7	středisko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
pořádání	pořádání	k1gNnSc4	pořádání
obchodních	obchodní	k2eAgFnPc2d1	obchodní
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
and	and	k?	and
Hove	Hove	k1gInSc1	Hove
je	být	k5eAaImIp3nS	být
také	také	k9	také
významným	významný	k2eAgNnSc7d1	významné
vzdělávacím	vzdělávací	k2eAgNnSc7d1	vzdělávací
centrem	centrum	k1gNnSc7	centrum
s	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
univerzitami	univerzita	k1gFnPc7	univerzita
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
školami	škola	k1gFnPc7	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozemkové	pozemkový	k2eAgFnSc6d1	pozemková
knize	kniha	k1gFnSc6	kniha
byl	být	k5eAaImAgInS	být
Brighton	Brighton	k1gInSc1	Brighton
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
Bristelmestune	Bristelmestun	k1gInSc5	Bristelmestun
s	s	k7c7	s
daní	daň	k1gFnSc7	daň
4	[number]	k4	4
000	[number]	k4	000
sleďů	sleď	k1gMnPc2	sleď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1514	[number]	k4	1514
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
zcela	zcela	k6eAd1	zcela
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
francouzskými	francouzský	k2eAgNnPc7d1	francouzské
piráty	pirát	k1gMnPc4	pirát
během	během	k7c2	během
války	válka	k1gFnSc2	válka
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zkáze	zkáza	k1gFnSc3	zkáza
odolal	odolat	k5eAaPmAgInS	odolat
pouze	pouze	k6eAd1	pouze
kostel	kostel	k1gInSc1	kostel
Svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
a	a	k8xC	a
ulice	ulice	k1gFnSc2	ulice
The	The	k1gMnSc1	The
Lanes	Lanes	k1gMnSc1	Lanes
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
známé	známý	k2eAgNnSc1d1	známé
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1545	[number]	k4	1545
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1740	[number]	k4	1740
až	až	k6eAd1	až
1750	[number]	k4	1750
začal	začít	k5eAaPmAgMnS	začít
lékař	lékař	k1gMnSc1	lékař
Richard	Richard	k1gMnSc1	Richard
Russell	Russell	k1gMnSc1	Russell
předepisovat	předepisovat	k5eAaImF	předepisovat
svým	svůj	k3xOyFgMnPc3	svůj
pacientům	pacient	k1gMnPc3	pacient
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
medicínské	medicínský	k2eAgNnSc1d1	medicínské
využití	využití	k1gNnSc1	využití
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
začala	začít	k5eAaPmAgFnS	začít
výstavba	výstavba	k1gFnSc1	výstavba
Regentských	regentský	k2eAgFnPc2d1	regentská
teras	terasa	k1gFnPc2	terasa
a	a	k8xC	a
původně	původně	k6eAd1	původně
rybářská	rybářský	k2eAgFnSc1d1	rybářská
vesnice	vesnice	k1gFnSc1	vesnice
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
známým	známý	k2eAgNnSc7d1	známé
přímořským	přímořský	k2eAgNnSc7d1	přímořské
lázeňským	lázeňský	k2eAgNnSc7d1	lázeňské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
princem	princ	k1gMnSc7	princ
regentem	regens	k1gMnSc7	regens
(	(	kIx(	(
<g/>
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
králem	král	k1gMnSc7	král
Jiřím	Jiří	k1gMnSc7	Jiří
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
poté	poté	k6eAd1	poté
co	co	k9	co
ho	on	k3xPp3gMnSc4	on
poprvé	poprvé	k6eAd1	poprvé
navštívil	navštívit	k5eAaPmAgMnS	navštívit
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Strávil	strávit	k5eAaPmAgMnS	strávit
zde	zde	k6eAd1	zde
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
volného	volný	k2eAgInSc2d1	volný
času	čas	k1gInSc2	čas
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
exotický	exotický	k2eAgInSc1d1	exotický
a	a	k8xC	a
nákladný	nákladný	k2eAgInSc1d1	nákladný
královský	královský	k2eAgInSc1d1	královský
pavilón	pavilón	k1gInSc1	pavilón
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
města	město	k1gNnSc2	město
železnicí	železnice	k1gFnSc7	železnice
roku	rok	k1gInSc2	rok
1841	[number]	k4	1841
přivedlo	přivést	k5eAaPmAgNnS	přivést
do	do	k7c2	do
Brightonu	Brighton	k1gInSc2	Brighton
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zde	zde	k6eAd1	zde
strávili	strávit	k5eAaPmAgMnP	strávit
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
a	a	k8xC	a
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
výrazný	výrazný	k2eAgInSc4d1	výrazný
nárůst	nárůst	k1gInSc4	nárůst
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
počtu	počet	k1gInSc2	počet
asi	asi	k9	asi
7	[number]	k4	7
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
120	[number]	k4	120
000	[number]	k4	000
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
viktoriánské	viktoriánský	k2eAgFnSc6d1	viktoriánská
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgFnPc1d1	postavena
mnohé	mnohý	k2eAgFnPc1d1	mnohá
známé	známý	k2eAgFnPc1d1	známá
stavby	stavba	k1gFnPc1	stavba
–	–	k?	–
Grand	grand	k1gMnSc1	grand
Hotel	hotel	k1gInSc1	hotel
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
West	West	k2eAgInSc1d1	West
Pier	pier	k1gInSc1	pier
(	(	kIx(	(
<g/>
1866	[number]	k4	1866
<g/>
)	)	kIx)	)
a	a	k8xC	a
Palace	Palace	k1gFnSc1	Palace
Pier	pier	k1gInSc1	pier
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
změnách	změna	k1gFnPc6	změna
hranic	hranice	k1gFnPc2	hranice
města	město	k1gNnSc2	město
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1873	[number]	k4	1873
až	až	k8xS	až
1952	[number]	k4	1952
se	se	k3xPyFc4	se
rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
rozrostla	rozrůst	k5eAaPmAgFnS	rozrůst
z	z	k7c2	z
665	[number]	k4	665
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
až	až	k6eAd1	až
na	na	k7c4	na
5	[number]	k4	5
800	[number]	k4	800
ha	ha	kA	ha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgNnPc1d1	nové
sídliště	sídliště	k1gNnPc1	sídliště
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
Moulsecoomb	Moulsecoomb	k1gMnSc1	Moulsecoomb
<g/>
,	,	kIx,	,
Bevendean	Bevendean	k1gMnSc1	Bevendean
<g/>
,	,	kIx,	,
Coldean	Coldean	k1gMnSc1	Coldean
a	a	k8xC	a
Whitehawk	Whitehawk	k1gMnSc1	Whitehawk
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
Brighton	Brighton	k1gInSc1	Brighton
tvoří	tvořit	k5eAaImIp3nS	tvořit
součást	součást	k1gFnSc4	součást
konurbace	konurbace	k1gFnSc2	konurbace
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
Národního	národní	k2eAgInSc2d1	národní
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
asi	asi	k9	asi
480	[number]	k4	480
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Hanover	Hanovra	k1gFnPc2	Hanovra
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zlikvidovány	zlikvidován	k2eAgInPc1d1	zlikvidován
slumy	slum	k1gInPc1	slum
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
obecních	obecní	k2eAgInPc2d1	obecní
bytů	byt	k1gInPc2	byt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Tarnerland	Tarnerlanda	k1gFnPc2	Tarnerlanda
od	od	k7c2	od
úpatí	úpatí	k1gNnSc2	úpatí
Albion	Albion	k1gInSc4	Albion
Hill	Hill	k1gMnSc1	Hill
až	až	k9	až
po	po	k7c4	po
výškové	výškový	k2eAgFnPc4d1	výšková
budovy	budova	k1gFnPc4	budova
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Pleasant	Pleasanta	k1gFnPc2	Pleasanta
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
změnily	změnit	k5eAaPmAgFnP	změnit
tvář	tvář	k1gFnSc4	tvář
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Lanes	Lanes	k1gInSc1	Lanes
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
,	,	kIx,	,
obytná	obytný	k2eAgFnSc1d1	obytná
a	a	k8xC	a
zábavní	zábavní	k2eAgFnSc1d1	zábavní
čtvrť	čtvrť	k1gFnSc1	čtvrť
s	s	k7c7	s
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
moři	moře	k1gNnSc3	moře
na	na	k7c6	na
jižním	jižní	k2eAgInSc6d1	jižní
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
úzkých	úzký	k2eAgFnPc2d1	úzká
alejí	alej	k1gFnPc2	alej
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ulicemi	ulice	k1gFnPc7	ulice
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
je	být	k5eAaImIp3nS	být
zřejmý	zřejmý	k2eAgInSc1d1	zřejmý
původní	původní	k2eAgInSc1d1	původní
rybářský	rybářský	k2eAgInSc1d1	rybářský
charakter	charakter	k1gInSc1	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaPmF	nalézt
klenotnictví	klenotnictví	k1gNnSc4	klenotnictví
<g/>
,	,	kIx,	,
starožitnictví	starožitnictví	k1gNnSc1	starožitnictví
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
a	a	k8xC	a
hospody	hospody	k?	hospody
<g/>
.	.	kIx.	.
</s>
<s>
Churchil	Churchil	k1gMnSc1	Churchil
Square	square	k1gInSc4	square
je	být	k5eAaImIp3nS	být
zastřešené	zastřešený	k2eAgNnSc1d1	zastřešené
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
plochou	plocha	k1gFnSc7	plocha
asi	asi	k9	asi
43	[number]	k4	43
660	[number]	k4	660
m2	m2	k4	m2
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
85	[number]	k4	85
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
5	[number]	k4	5
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
1	[number]	k4	1
600	[number]	k4	600
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
jako	jako	k8xS	jako
otevřené	otevřený	k2eAgNnSc1d1	otevřené
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
přestavěno	přestavět	k5eAaPmNgNnS	přestavět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
obchodní	obchodní	k2eAgFnPc1d1	obchodní
zóny	zóna	k1gFnPc1	zóna
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c4	na
Western	western	k1gInSc4	western
Road	Road	k1gMnSc1	Road
a	a	k8xC	a
London	London	k1gMnSc1	London
Road	Road	k1gMnSc1	Road
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
North	Northa	k1gFnPc2	Northa
Laine	Lain	k1gInSc5	Lain
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
s	s	k7c7	s
The	The	k1gFnSc7	The
Lanes	Lanes	k1gMnSc1	Lanes
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obchodní	obchodní	k2eAgFnSc1d1	obchodní
<g/>
,	,	kIx,	,
zábavná	zábavný	k2eAgFnSc1d1	zábavná
a	a	k8xC	a
obytná	obytný	k2eAgFnSc1d1	obytná
oblast	oblast	k1gFnSc1	oblast
situována	situovat	k5eAaBmNgFnS	situovat
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
The	The	k1gFnSc2	The
Lanes	Lanesa	k1gFnPc2	Lanesa
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
anglosaského	anglosaský	k2eAgNnSc2d1	anglosaské
slova	slovo	k1gNnSc2	slovo
Laine	Lain	k1gInSc5	Lain
označující	označující	k2eAgInSc1d1	označující
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
pozemek	pozemek	k1gInSc1	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mnoho	mnoho	k4c4	mnoho
malých	malý	k2eAgFnPc2d1	malá
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
,	,	kIx,	,
obchodů	obchod	k1gInPc2	obchod
s	s	k7c7	s
avantgardním	avantgardní	k2eAgNnSc7d1	avantgardní
zbožím	zboží	k1gNnSc7	zboží
a	a	k8xC	a
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
její	její	k3xOp3gInSc1	její
charakter	charakter	k1gInSc1	charakter
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
zvyšování	zvyšování	k1gNnSc2	zvyšování
cen	cena	k1gFnPc2	cena
nemovitostí	nemovitost	k1gFnPc2	nemovitost
a	a	k8xC	a
místních	místní	k2eAgFnPc2d1	místní
daní	daň	k1gFnPc2	daň
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
malých	malý	k2eAgInPc2d1	malý
obchodů	obchod	k1gInPc2	obchod
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
jinam	jinam	k6eAd1	jinam
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
obsazována	obsazován	k2eAgFnSc1d1	obsazována
podniky	podnik	k1gInPc1	podnik
velkých	velký	k2eAgFnPc2d1	velká
zábavních	zábavní	k2eAgFnPc2d1	zábavní
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
hustotu	hustota	k1gFnSc4	hustota
společností	společnost	k1gFnPc2	společnost
podnikajících	podnikající	k2eAgFnPc2d1	podnikající
v	v	k7c6	v
mediální	mediální	k2eAgFnSc6d1	mediální
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
digitální	digitální	k2eAgFnSc6d1	digitální
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
občas	občas	k6eAd1	občas
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Silicon	Silicon	kA	Silicon
Beach	Beach	k1gInSc1	Beach
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
American	American	k1gMnSc1	American
Express	express	k1gInSc1	express
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
na	na	k7c6	na
Edward	Edward	k1gMnSc1	Edward
Street	Street	k1gInSc1	Street
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
asi	asi	k9	asi
3	[number]	k4	3
000	[number]	k4	000
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
největším	veliký	k2eAgMnSc7d3	veliký
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
byly	být	k5eAaImAgInP	být
města	město	k1gNnSc2	město
Brighton	Brighton	k1gInSc1	Brighton
a	a	k8xC	a
Hove	Hov	k1gInPc1	Hov
spojeny	spojit	k5eAaPmNgInP	spojit
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
samosprávnou	samosprávný	k2eAgFnSc4d1	samosprávná
jednotku	jednotka	k1gFnSc4	jednotka
Brighton	Brighton	k1gInSc1	Brighton
and	and	k?	and
Hove	Hove	k1gInSc1	Hove
s	s	k7c7	s
udělením	udělení	k1gNnSc7	udělení
statusu	status	k1gInSc2	status
města	město	k1gNnSc2	město
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
oslav	oslava	k1gFnPc2	oslava
milénia	milénium	k1gNnSc2	milénium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
a	a	k8xC	a
Hove	Hove	k1gNnSc1	Hove
jsou	být	k5eAaImIp3nP	být
částí	část	k1gFnSc7	část
tří	tři	k4xCgInPc2	tři
obvodů	obvod	k1gInPc2	obvod
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
–	–	k?	–
Brighton	Brighton	k1gInSc1	Brighton
Kemptown	Kemptown	k1gInSc1	Kemptown
<g/>
,	,	kIx,	,
Brighton	Brighton	k1gInSc1	Brighton
Pavilion	Pavilion	k1gInSc1	Pavilion
a	a	k8xC	a
Hove	Hove	k1gFnSc7	Hove
a	a	k8xC	a
částí	část	k1gFnSc7	část
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
Jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Anglie	Anglie	k1gFnSc2	Anglie
pro	pro	k7c4	pro
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
železniční	železniční	k2eAgFnSc7d1	železniční
stanicí	stanice	k1gFnSc7	stanice
je	být	k5eAaImIp3nS	být
Brighton	Brighton	k1gInSc1	Brighton
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
časté	častý	k2eAgFnSc2d1	častá
spoje	spoj	k1gFnSc2	spoj
směrem	směr	k1gInSc7	směr
na	na	k7c6	na
Victoria	Victorium	k1gNnSc2	Victorium
Station	station	k1gInSc1	station
<g/>
,	,	kIx,	,
Letiště	letiště	k1gNnSc1	letiště
London	London	k1gMnSc1	London
Gatwick	Gatwick	k1gMnSc1	Gatwick
<g/>
,	,	kIx,	,
Portsmouth	Portsmouth	k1gMnSc1	Portsmouth
<g/>
,	,	kIx,	,
Ashford	Ashford	k1gMnSc1	Ashford
<g/>
,	,	kIx,	,
Reading	Reading	k1gInSc1	Reading
a	a	k8xC	a
Bedford	Bedford	k1gInSc1	Bedford
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
na	na	k7c4	na
Birmingham	Birmingham	k1gInSc4	Birmingham
a	a	k8xC	a
Manchester	Manchester	k1gInSc4	Manchester
a	a	k8xC	a
přes	přes	k7c4	přes
Bristol	Bristol	k1gInSc4	Bristol
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Wales	Wales	k1gInSc4	Wales
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
dvakrát	dvakrát	k6eAd1	dvakrát
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Autobusovou	autobusový	k2eAgFnSc4d1	autobusová
dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
měst	město	k1gNnPc2	město
Brighton	Brighton	k1gInSc4	Brighton
a	a	k8xC	a
Hove	Hove	k1gInSc4	Hove
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
dopravní	dopravní	k2eAgFnSc1d1	dopravní
společnost	společnost	k1gFnSc1	společnost
Brighton	Brighton	k1gInSc1	Brighton
&	&	k?	&
Hove	Hove	k1gInSc1	Hove
Bus	bus	k1gInSc1	bus
and	and	k?	and
Coach	Coach	k1gInSc1	Coach
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
asi	asi	k9	asi
300	[number]	k4	300
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
je	být	k5eAaImIp3nS	být
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
omezeném	omezený	k2eAgInSc6d1	omezený
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brightonu	Brighton	k1gInSc2	Brighton
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
nejstarší	starý	k2eAgFnSc1d3	nejstarší
elektrická	elektrický	k2eAgFnSc1d1	elektrická
železnice	železnice	k1gFnSc1	železnice
světa	svět	k1gInSc2	svět
–	–	k?	–
Volks	Volks	k1gInSc1	Volks
Electric	Electrice	k1gFnPc2	Electrice
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
Brightonu	Brighton	k1gInSc2	Brighton
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
pláže	pláž	k1gFnPc1	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
mnoha	mnoho	k4c7	mnoho
bary	bar	k1gInPc7	bar
<g/>
,	,	kIx,	,
restauracemi	restaurace	k1gFnPc7	restaurace
<g/>
,	,	kIx,	,
nočními	noční	k2eAgInPc7d1	noční
kluby	klub	k1gInPc7	klub
a	a	k8xC	a
arkádami	arkáda	k1gFnPc7	arkáda
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
atrakcemi	atrakce	k1gFnPc7	atrakce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
z	z	k7c2	z
města	město	k1gNnSc2	město
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
Londýnem	Londýn	k1gInSc7	Londýn
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Brightonské	Brightonský	k2eAgFnPc1d1	Brightonská
pláže	pláž	k1gFnPc1	pláž
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Londýňany	Londýňan	k1gMnPc4	Londýňan
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
nejpřístupnějších	přístupný	k2eAgFnPc2d3	nejpřístupnější
turistických	turistický	k2eAgFnPc2d1	turistická
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
vyhrazené	vyhrazený	k2eAgFnPc4d1	vyhrazená
pláže	pláž	k1gFnPc4	pláž
pro	pro	k7c4	pro
nudisty	nudista	k1gMnPc4	nudista
(	(	kIx(	(
<g/>
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
kempu	kemp	k1gInSc2	kemp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skutečnost	skutečnost	k1gFnSc1	skutečnost
je	být	k5eAaImIp3nS	být
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k4c4	málo
nudistických	nudistický	k2eAgFnPc2d1	nudistická
pláží	pláž	k1gFnPc2	pláž
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
obydlených	obydlený	k2eAgFnPc2d1	obydlená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
pořádají	pořádat	k5eAaImIp3nP	pořádat
Brighton	Brighton	k1gInSc4	Brighton
a	a	k8xC	a
Hove	Hove	k1gInSc4	Hove
umělecký	umělecký	k2eAgInSc4d1	umělecký
festival	festival	k1gInSc4	festival
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
organizované	organizovaný	k2eAgInPc4d1	organizovaný
průvody	průvod	k1gInPc4	průvod
například	například	k6eAd1	například
Children	Childrna	k1gFnPc2	Childrna
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Parade	Parad	k1gInSc5	Parad
<g/>
,	,	kIx,	,
akce	akce	k1gFnSc2	akce
doprovázené	doprovázený	k2eAgInPc4d1	doprovázený
ohňostroji	ohňostroj	k1gInPc7	ohňostroj
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
<g/>
,	,	kIx,	,
vystoupení	vystoupení	k1gNnSc1	vystoupení
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
umělecké	umělecký	k2eAgFnPc1d1	umělecká
akce	akce	k1gFnPc1	akce
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgFnSc7d1	úvodní
akcí	akce	k1gFnSc7	akce
tohoto	tento	k3xDgInSc2	tento
festivalu	festival	k1gInSc2	festival
je	být	k5eAaImIp3nS	být
Open	Open	k1gMnSc1	Open
Houses	Houses	k1gMnSc1	Houses
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
dílny	dílna	k1gFnPc1	dílna
a	a	k8xC	a
ateliéry	ateliér	k1gInPc1	ateliér
umělců	umělec	k1gMnPc2	umělec
otevřené	otevřený	k2eAgNnSc1d1	otevřené
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
jako	jako	k8xS	jako
galerie	galerie	k1gFnPc4	galerie
a	a	k8xC	a
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
prodej	prodej	k1gInSc1	prodej
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
majitelů	majitel	k1gMnPc2	majitel
dílen	dílna	k1gFnPc2	dílna
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
městem	město	k1gNnSc7	město
provozovaného	provozovaný	k2eAgInSc2d1	provozovaný
Brighton	Brighton	k1gInSc1	Brighton
Museum	museum	k1gNnSc4	museum
and	and	k?	and
Art	Art	k1gFnSc1	Art
Gallery	Galler	k1gInPc4	Galler
<g/>
,	,	kIx,	,
Booth	Booth	k1gMnSc1	Booth
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gInPc4	Histor
<g/>
,	,	kIx,	,
Brighton	Brighton	k1gInSc4	Brighton
Fishing	Fishing	k1gInSc1	Fishing
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
Brighton	Brighton	k1gInSc1	Brighton
Toy	Toy	k1gFnSc2	Toy
and	and	k?	and
Model	model	k1gInSc1	model
Museum	museum	k1gNnSc1	museum
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
soukromých	soukromý	k2eAgFnPc2d1	soukromá
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
arkádách	arkáda	k1gFnPc6	arkáda
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
zvykem	zvyk	k1gInSc7	zvyk
je	být	k5eAaImIp3nS	být
prodej	prodej	k1gInSc4	prodej
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
kavárnách	kavárna	k1gFnPc6	kavárna
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k8xC	i
barech	bar	k1gInPc6	bar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
divadla	divadlo	k1gNnPc4	divadlo
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
patří	patřit	k5eAaImIp3nS	patřit
nedávno	nedávno	k6eAd1	nedávno
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
Komedia	Komedium	k1gNnPc1	Komedium
a	a	k8xC	a
Theatre	Theatr	k1gInSc5	Theatr
Royal	Royal	k1gInSc4	Royal
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
divadly	divadlo	k1gNnPc7	divadlo
jsou	být	k5eAaImIp3nP	být
Marlborough	Marlborough	k1gInSc1	Marlborough
Theatre	Theatr	k1gMnSc5	Theatr
a	a	k8xC	a
Nightingale	Nightingal	k1gMnSc5	Nightingal
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nad	nad	k7c7	nad
hospodami	hospodami	k?	hospodami
a	a	k8xC	a
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
tak	tak	k6eAd1	tak
místní	místní	k2eAgMnPc4d1	místní
producenty	producent	k1gMnPc4	producent
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
bohatou	bohatý	k2eAgFnSc4d1	bohatá
filmovou	filmový	k2eAgFnSc4d1	filmová
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
Duke	Duke	k1gInSc4	Duke
of	of	k?	of
York	York	k1gInSc1	York
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Picture	Pictur	k1gMnSc5	Pictur
House	house	k1gNnSc1	house
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Rada	Rada	k1gMnSc1	Rada
města	město	k1gNnSc2	město
Brighton	Brighton	k1gInSc1	Brighton
and	and	k?	and
Hove	Hove	k1gInSc1	Hove
spravuje	spravovat	k5eAaImIp3nS	spravovat
80	[number]	k4	80
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
dvouměstí	dvouměstí	k1gNnSc6	dvouměstí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
se	s	k7c7	s
54	[number]	k4	54
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Brighton	Brighton	k1gInSc1	Brighton
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Učebny	učebna	k1gFnPc1	učebna
této	tento	k3xDgFnSc2	tento
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
jsou	být	k5eAaImIp3nP	být
rozptýleny	rozptýlen	k2eAgFnPc1d1	rozptýlena
v	v	k7c6	v
několika	několik	k4yIc6	několik
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
Falmeru	Falmer	k1gInSc6	Falmer
a	a	k8xC	a
Eastbourne	Eastbourn	k1gInSc5	Eastbourn
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gMnSc7	centr
University	universita	k1gFnSc2	universita
of	of	k?	of
Sussex	Sussex	k1gInSc1	Sussex
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezi	mezi	k7c4	mezi
Stanmer	Stanmer	k1gInSc4	Stanmer
Park	park	k1gInSc1	park
a	a	k8xC	a
Falmerem	Falmero	k1gNnSc7	Falmero
asi	asi	k9	asi
5	[number]	k4	5
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
škola	škola	k1gFnSc1	škola
má	mít	k5eAaImIp3nS	mít
si	se	k3xPyFc3	se
10	[number]	k4	10
500	[number]	k4	500
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
soukromých	soukromý	k2eAgFnPc2d1	soukromá
škol	škola	k1gFnPc2	škola
včetně	včetně	k7c2	včetně
Brighton	Brighton	k1gInSc4	Brighton
College	College	k1gFnSc3	College
<g/>
,	,	kIx,	,
Roedean	Roedean	k1gInSc4	Roedean
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
Steiner	Steiner	k1gMnSc1	Steiner
School	School	k1gInSc1	School
a	a	k8xC	a
Montessori	Montessori	k1gNnSc1	Montessori
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
<g/>
,	,	kIx,	,
státem	stát	k1gInSc7	stát
podporované	podporovaný	k2eAgFnSc2d1	podporovaná
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zařadit	zařadit	k5eAaPmF	zařadit
Dorothy	Dorotha	k1gFnPc4	Dorotha
Stringer	Stringra	k1gFnPc2	Stringra
<g/>
,	,	kIx,	,
Varndean	Varndeana	k1gFnPc2	Varndeana
<g/>
,	,	kIx,	,
Hove	Hov	k1gInPc1	Hov
Park	park	k1gInSc1	park
a	a	k8xC	a
Cardinal	Cardinal	k1gMnSc1	Cardinal
Newman	Newman	k1gMnSc1	Newman
(	(	kIx(	(
<g/>
velká	velký	k2eAgFnSc1d1	velká
katolická	katolický	k2eAgFnSc1d1	katolická
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
letních	letní	k2eAgFnPc2d1	letní
prázdnin	prázdniny	k1gFnPc2	prázdniny
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
sjíždí	sjíždět	k5eAaImIp3nS	sjíždět
mnoho	mnoho	k4c1	mnoho
mladých	mladý	k2eAgMnPc2d1	mladý
studentů	student	k1gMnPc2	student
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
aby	aby	kYmCp3nP	aby
navštěvovali	navštěvovat	k5eAaImAgMnP	navštěvovat
kursy	kurs	k1gInPc4	kurs
výuky	výuka	k1gFnSc2	výuka
anglického	anglický	k2eAgInSc2d1	anglický
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
a	a	k8xC	a
Hove	Hove	k1gFnSc6	Hove
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
specializovaná	specializovaný	k2eAgFnSc1d1	specializovaná
hudební	hudební	k2eAgFnSc1d1	hudební
škola	škola	k1gFnSc1	škola
Brighton	Brighton	k1gInSc1	Brighton
Institute	institut	k1gInSc5	institut
Of	Of	k1gFnSc2	Of
Modern	Modern	k1gMnSc1	Modern
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
sídlí	sídlet	k5eAaImIp3nS	sídlet
Brighton	Brighton	k1gInSc1	Brighton
&	&	k?	&
Hove	Hove	k1gInSc1	Hove
Albion	Albion	k1gInSc1	Albion
Football	Footballa	k1gFnPc2	Footballa
Club	club	k1gInSc1	club
a	a	k8xC	a
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
klub	klub	k1gInSc1	klub
Brighton	Brighton	k1gInSc1	Brighton
Bears	Bears	k1gInSc1	Bears
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
pořádán	pořádán	k2eAgInSc1d1	pořádán
turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
plážovém	plážový	k2eAgInSc6d1	plážový
fotbalu	fotbal	k1gInSc6	fotbal
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
utkání	utkání	k1gNnSc1	utkání
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
na	na	k7c6	na
přechodně	přechodně	k6eAd1	přechodně
postaveném	postavený	k2eAgNnSc6d1	postavené
hřišti	hřiště	k1gNnSc6	hřiště
vzniklém	vzniklý	k2eAgNnSc6d1	vzniklé
navezením	navezení	k1gNnSc7	navezení
písku	písek	k1gInSc2	písek
na	na	k7c4	na
pláž	pláž	k1gFnSc4	pláž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
je	být	k5eAaImIp3nS	být
také	také	k9	také
pořádán	pořádán	k2eAgInSc1d1	pořádán
dostihový	dostihový	k2eAgInSc1d1	dostihový
závod	závod	k1gInSc1	závod
Brighton	Brighton	k1gInSc4	Brighton
Racecourse	Racecourse	k1gFnSc2	Racecourse
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
potřebné	potřebný	k2eAgFnSc2d1	potřebná
délky	délka	k1gFnSc2	délka
trasy	trasa	k1gFnSc2	trasa
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
část	část	k1gFnSc4	část
ulice	ulice	k1gFnSc2	ulice
Wilson	Wilson	k1gInSc1	Wilson
Avenue	avenue	k1gFnSc4	avenue
položen	položen	k2eAgInSc1d1	položen
trávník	trávník	k1gInSc1	trávník
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dostihu	dostih	k1gInSc2	dostih
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
ragbyových	ragbyový	k2eAgInPc2d1	ragbyový
klubů	klub	k1gInPc2	klub
Anglie	Anglie	k1gFnSc2	Anglie
–	–	k?	–
Brighton	Brighton	k1gInSc1	Brighton
Rugby	rugby	k1gNnSc1	rugby
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
</s>
<s>
Royal	Royal	k1gInSc1	Royal
Pavilion	Pavilion	k1gInSc1	Pavilion
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgInSc1d1	bývalý
královský	královský	k2eAgInSc1d1	královský
palác	palác	k1gInSc1	palác
postavený	postavený	k2eAgInSc1d1	postavený
pro	pro	k7c4	pro
prince	princ	k1gMnSc4	princ
regenta	regens	k1gMnSc4	regens
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pozoruhodný	pozoruhodný	k2eAgInSc4d1	pozoruhodný
architekturou	architektura	k1gFnSc7	architektura
inspirovanou	inspirovaný	k2eAgFnSc7d1	inspirovaná
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
orientálním	orientální	k2eAgInSc7d1	orientální
interiérem	interiér	k1gInSc7	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Budovu	budova	k1gFnSc4	budova
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
okolní	okolní	k2eAgInSc4d1	okolní
pozemek	pozemek	k1gInSc4	pozemek
odkoupilo	odkoupit	k5eAaPmAgNnS	odkoupit
roku	rok	k1gInSc2	rok
1849	[number]	k4	1849
město	město	k1gNnSc1	město
za	za	k7c4	za
53	[number]	k4	53
000	[number]	k4	000
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Brighton	Brighton	k1gInSc1	Brighton
Pier	pier	k1gInSc1	pier
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Palace	Palace	k1gFnPc1	Palace
Pier	pier	k1gInSc1	pier
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Brighton	Brighton	k1gInSc1	Brighton
Marine	Marin	k1gInSc5	Marin
Palace	Palace	k1gFnPc4	Palace
and	and	k?	and
Pier	pier	k1gInSc1	pier
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
otevřen	otevřen	k2eAgInSc1d1	otevřen
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
molem	mol	k1gMnSc7	mol
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zábavní	zábavní	k2eAgInSc4d1	zábavní
park	park	k1gInSc4	park
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
arkádové	arkádový	k2eAgFnPc4d1	arkádová
haly	hala	k1gFnPc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
West	West	k2eAgInSc4d1	West
Pier	pier	k1gInSc4	pier
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
pro	pro	k7c4	pro
následnou	následný	k2eAgFnSc4d1	následná
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
památkově	památkově	k6eAd1	památkově
chráněnými	chráněný	k2eAgInPc7d1	chráněný
moly	mol	k1gInPc7	mol
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
dokud	dokud	k8xS	dokud
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
nebylo	být	k5eNaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
požárem	požár	k1gInSc7	požár
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
plány	plán	k1gInPc4	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
183	[number]	k4	183
m	m	kA	m
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vyhlídkové	vyhlídkový	k2eAgFnSc2d1	vyhlídková
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
postavena	postavit	k5eAaPmNgNnP	postavit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgMnSc2d1	původní
mola	mol	k1gMnSc2	mol
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Marks	Marks	k1gInSc4	Marks
Barfield	Barfield	k1gMnSc1	Barfield
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
návrhu	návrh	k1gInSc2	návrh
Londýnského	londýnský	k2eAgNnSc2d1	Londýnské
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
této	tento	k3xDgFnSc2	tento
věže	věž	k1gFnSc2	věž
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgFnP	schválit
městskou	městský	k2eAgFnSc7d1	městská
radou	rada	k1gFnSc7	rada
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Volk	Volk	k1gInSc1	Volk
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Electric	Electric	k1gMnSc1	Electric
Railway	Railwaa	k1gFnPc4	Railwaa
<g/>
,	,	kIx,	,
postavená	postavený	k2eAgFnSc1d1	postavená
roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
okraji	okraj	k1gInSc6	okraj
pláže	pláž	k1gFnSc2	pláž
od	od	k7c2	od
Brighton	Brighton	k1gInSc1	Brighton
Pier	pier	k1gInSc4	pier
až	až	k9	až
k	k	k7c3	k
Black	Black	k1gMnSc1	Black
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
provozovanou	provozovaný	k2eAgFnSc4d1	provozovaná
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
železniční	železniční	k2eAgFnSc4d1	železniční
dráhu	dráha	k1gFnSc4	dráha
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejstarší	starý	k2eAgFnSc4d3	nejstarší
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
kostel	kostel	k1gInSc1	kostel
Svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
je	být	k5eAaImIp3nS	být
také	také	k9	také
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
matka	matka	k1gFnSc1	matka
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
chrámem	chrám	k1gInSc7	chrám
je	být	k5eAaImIp3nS	být
kostel	kostel	k1gInSc1	kostel
Svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
expozice	expozice	k1gFnSc2	expozice
v	v	k7c6	v
Brightonském	Brightonský	k2eAgNnSc6d1	Brightonský
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Brighton	Brighton	k1gInSc4	Brighton
městem	město	k1gNnSc7	město
s	s	k7c7	s
nejdéle	dlouho	k6eAd3	dlouho
trvale	trvale	k6eAd1	trvale
provozovanou	provozovaný	k2eAgFnSc7d1	provozovaná
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
sítí	síť	k1gFnSc7	síť
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiná	k1gFnSc1	jiná
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
zásobována	zásobovat	k5eAaImNgFnS	zásobovat
elektřinou	elektřina	k1gFnSc7	elektřina
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
ale	ale	k8xC	ale
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
trvalé	trvalý	k2eAgNnSc4d1	trvalé
a	a	k8xC	a
nepřerušené	přerušený	k2eNgNnSc4d1	nepřerušené
provozování	provozování	k1gNnSc4	provozování
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
snadno	snadno	k6eAd1	snadno
dostupné	dostupný	k2eAgFnPc1d1	dostupná
i	i	k8xC	i
zářivě	zářivě	k6eAd1	zářivě
bílé	bílý	k2eAgInPc1d1	bílý
křídové	křídový	k2eAgInPc1d1	křídový
útesy	útes	k1gInPc1	útes
Seven	Sevna	k1gFnPc2	Sevna
Sisters	Sistersa	k1gFnPc2	Sistersa
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brighton	Brighton	k1gInSc1	Brighton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Brighton	Brighton	k1gInSc1	Brighton
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Rada	Rada	k1gMnSc1	Rada
města	město	k1gNnSc2	město
Brighton	Brighton	k1gInSc1	Brighton
and	and	k?	and
Hove	Hove	k1gNnSc7	Hove
Turistické	turistický	k2eAgFnSc2d1	turistická
informace	informace	k1gFnSc2	informace
o	o	k7c4	o
Brighton	Brighton	k1gInSc4	Brighton
and	and	k?	and
Hove	Hov	k1gInSc2	Hov
Brightonská	Brightonský	k2eAgNnPc1d1	Brightonský
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnPc1	galerie
Historie	historie	k1gFnSc2	historie
Brightonu	Brighton	k1gInSc2	Brighton
a	a	k8xC	a
Royal	Royal	k1gMnSc1	Royal
Pavilion	Pavilion	k1gInSc4	Pavilion
Výlety	výlet	k1gInPc7	výlet
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Brighton	Brighton	k1gInSc4	Brighton
a	a	k8xC	a
Hove	Hove	k1gInSc4	Hove
</s>
