<p>
<s>
Elbasan	Elbasan	k1gInSc1	Elbasan
(	(	kIx(	(
<g/>
albánsky	albánsky	k6eAd1	albánsky
<g/>
:	:	kIx,	:
Rrethi	Rrethi	k1gNnSc1	Rrethi
i	i	k8xC	i
Elbasanit	Elbasanit	k1gInSc1	Elbasanit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
okres	okres	k1gInSc1	okres
v	v	k7c6	v
Albánii	Albánie	k1gFnSc6	Albánie
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
224	[number]	k4	224
000	[number]	k4	000
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozloha	rozloha	k1gFnSc1	rozloha
1290	[number]	k4	1290
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
okresu	okres	k1gInSc2	okres
Elbasan	Elbasan	k1gInSc1	Elbasan
je	být	k5eAaImIp3nS	být
Elbasan	Elbasan	k1gInSc4	Elbasan
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
města	město	k1gNnPc1	město
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
okresu	okres	k1gInSc6	okres
jsou	být	k5eAaImIp3nP	být
Cërrik	Cërrika	k1gFnPc2	Cërrika
a	a	k8xC	a
Kërrabë	Kërrabë	k1gFnPc2	Kërrabë
<g/>
.	.	kIx.	.
</s>
</p>
