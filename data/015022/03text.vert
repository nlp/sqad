<s>
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
Hotel	hotel	k1gInSc1
Patria	Patrium	k1gNnSc2
na	na	k7c6
Štrbském	štrbský	k2eAgNnSc6d1
Plese	pleso	k1gNnSc6
v	v	k7c6
létě	léto	k1gNnSc6
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
20	#num#	k4
<g/>
°	°	k?
<g/>
13	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
1010	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc1
kraj	kraj	k7c2
</s>
<s>
Prešovský	prešovský	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Poprad	Poprad	k1gInSc1
tradiční	tradiční	k2eAgInSc4d1
region	region	k1gInSc4
</s>
<s>
Spiš	Spiš	k1gFnSc1
Administrativní	administrativní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc4
</s>
<s>
3	#num#	k4
místní	místní	k2eAgFnPc4d1
části	část	k1gFnPc4
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
360,1	360,1	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
4	#num#	k4
804	#num#	k4
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
13,3	13,3	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
město	město	k1gNnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Ján	Ján	k1gMnSc1
Mokoš	Mokoš	k1gMnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1209	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.vysoketatry.sk	www.vysoketatry.sk	k1gInSc1
E-mail	e-mail	k1gInSc1
</s>
<s>
msuvt@tatry.sk	msuvt@tatry.sk	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Mestský	Mestský	k2eAgInSc1d1
úrad	úrada	k1gFnPc2
Vysoké	vysoká	k1gFnSc2
TatryStarý	TatryStarý	k2eAgInSc4d1
Smokovec	Smokovec	k1gInSc4
1062	#num#	k4
01	#num#	k4
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
052	#num#	k4
PSČ	PSČ	kA
</s>
<s>
062	#num#	k4
01	#num#	k4
Označení	označení	k1gNnPc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
PP	PP	kA
NUTS	NUTS	kA
</s>
<s>
560103	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
jsou	být	k5eAaImIp3nP
lázeňské	lázeňský	k2eAgNnSc4d1
město	město	k1gNnSc4
na	na	k7c6
severu	sever	k1gInSc6
Slovenska	Slovensko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
jsou	být	k5eAaImIp3nP
společně	společně	k6eAd1
s	s	k7c7
Popradem	Poprad	k1gInSc7
a	a	k8xC
Zakopaným	zakopaný	k2eAgNnSc7d1
přirozeným	přirozený	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
turistického	turistický	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
rozlohou	rozloha	k1gFnSc7
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc1
rozloha	rozloha	k1gFnSc1
je	být	k5eAaImIp3nS
398	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
celé	celá	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Tatranského	tatranský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
<g/>
,	,	kIx,
ve	v	k7c6
Vysokých	vysoký	k2eAgFnPc6d1
a	a	k8xC
Belianských	Belianský	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
severozápadní	severozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Spiše	Spiš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
okresu	okres	k1gInSc2
Poprad	Poprad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
necelých	celý	k2eNgInPc2d1
5	#num#	k4
000	#num#	k4
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
k	k	k7c3
nejmenším	malý	k2eAgInPc3d3
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgFnP
Cestou	cestou	k7c2
Slobody	sloboda	k1gFnSc2
vedoucí	vedoucí	k1gFnSc2
od	od	k7c2
Podbanského	Podbanský	k2eAgMnSc2d1
do	do	k7c2
Tatranské	tatranský	k2eAgFnSc2d1
Kotliny	kotlina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tatranská	tatranský	k2eAgFnSc1d1
elektrická	elektrický	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
spojuje	spojovat	k5eAaImIp3nS
osadu	osada	k1gFnSc4
Štrbské	štrbský	k2eAgNnSc1d1
Pleso	pleso	k1gNnSc1
s	s	k7c7
Tatranskou	tatranský	k2eAgFnSc7d1
Lomnicí	Lomnice	k1gFnSc7
a	a	k8xC
přes	přes	k7c4
Starý	starý	k2eAgInSc4d1
Smokovec	Smokovec	k1gInSc4
pokračuje	pokračovat	k5eAaImIp3nS
do	do	k7c2
Popradu	Poprad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1935	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
konalo	konat	k5eAaImAgNnS
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
v	v	k7c6
klasickém	klasický	k2eAgNnSc6d1
lyžování	lyžování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
</s>
<s>
Vysoké	vysoký	k2eAgFnPc1d1
Tatry	Tatra	k1gFnPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
ze	z	k7c2
tří	tři	k4xCgFnPc2
částí	část	k1gFnPc2
(	(	kIx(
<g/>
katastrálních	katastrální	k2eAgNnPc2d1
území	území	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
dále	daleko	k6eAd2
člení	členit	k5eAaImIp3nS
na	na	k7c4
15	#num#	k4
osad	osada	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
Štrbské	štrbský	k2eAgNnSc4d1
Pleso	pleso	k1gNnSc4
(	(	kIx(
<g/>
vlastní	vlastní	k2eAgFnSc1d1
osada	osada	k1gFnSc1
je	být	k5eAaImIp3nS
součást	součást	k1gFnSc1
obce	obec	k1gFnSc2
Štrba	Štrba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Vyšné	vyšný	k2eAgFnPc1d1
Hágy	Hága	k1gFnPc1
(	(	kIx(
<g/>
z.	z.	k?
1890	#num#	k4
<g/>
,	,	kIx,
1125	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Podbanské	Podbanský	k2eAgInPc4d1
(	(	kIx(
<g/>
z.	z.	k?
1871	#num#	k4
<g/>
,	,	kIx,
940	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
vesnice	vesnice	k1gFnSc2
Pribylina	Pribylina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Starý	starý	k2eAgMnSc1d1
Smokovec	Smokovec	k1gMnSc1
</s>
<s>
Horný	horný	k1gMnSc1
Smokovec	Smokovec	k1gMnSc1
(	(	kIx(
<g/>
950	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Dolný	Dolný	k2eAgInSc1d1
Smokovec	Smokovec	k1gInSc1
(	(	kIx(
<g/>
890	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Nový	nový	k2eAgInSc1d1
Smokovec	Smokovec	k1gInSc1
(	(	kIx(
<g/>
z.	z.	k?
1875	#num#	k4
<g/>
,	,	kIx,
1000	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Starý	starý	k2eAgInSc1d1
Smokovec	Smokovec	k1gInSc1
(	(	kIx(
<g/>
z.	z.	k?
1793	#num#	k4
<g/>
,	,	kIx,
1010	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tatranská	tatranský	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
(	(	kIx(
<g/>
z.	z.	k?
1885	#num#	k4
<g/>
,	,	kIx,
1005	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tatranské	tatranský	k2eAgInPc1d1
Zruby	Zrub	k1gInPc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
srubový	srubový	k2eAgInSc1d1
výcvikový	výcvikový	k2eAgInSc1d1
tábor	tábor	k1gInSc1
čs	čs	kA
<g/>
.	.	kIx.
armády	armáda	k1gFnSc2
Vojenské	vojenský	k2eAgFnSc2d1
Zruby	Zruba	k1gFnSc2
<g/>
,	,	kIx,
995	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Nová	nový	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
(	(	kIx(
<g/>
po	po	k7c6
2	#num#	k4
<g/>
.	.	kIx.
sv.	sv.	kA
válce	válec	k1gInSc2
jako	jako	k9
vojenská	vojenský	k2eAgFnSc1d1
osada	osada	k1gFnSc1
<g/>
,	,	kIx,
1060	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tatranská	tatranský	k2eAgFnSc1d1
Lomnica	Lomnica	k1gFnSc1
</s>
<s>
Tatranská	tatranský	k2eAgFnSc1d1
Lomnica	Lomnica	k1gFnSc1
(	(	kIx(
<g/>
z.	z.	k?
1893	#num#	k4
<g/>
,	,	kIx,
850	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tatranská	tatranský	k2eAgFnSc1d1
Kotlina	kotlina	k1gFnSc1
(	(	kIx(
<g/>
z.	z.	k?
1881	#num#	k4
<g/>
,	,	kIx,
760	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tatranská	tatranský	k2eAgFnSc1d1
Lesná	lesný	k2eAgFnSc1d1
(	(	kIx(
<g/>
z.	z.	k?
1927	#num#	k4
<g/>
,	,	kIx,
915	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Kežmarské	kežmarský	k2eAgFnPc1d1
Žľaby	Žľaba	k1gFnPc1
(	(	kIx(
<g/>
z.	z.	k?
1885	#num#	k4
<g/>
,	,	kIx,
920	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Tatranské	tatranský	k2eAgNnSc1d1
Matliare	Matliar	k1gMnSc5
(	(	kIx(
<g/>
z.	z.	k?
v	v	k7c6
pol.	pol.	k?
19	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
<g/>
,	,	kIx,
885	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Hranice	hranice	k1gFnSc1
původních	původní	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
ve	v	k7c6
Vysokých	vysoký	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.statistics.sk	www.statistics.sk	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
21	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Trolejbusová	trolejbusový	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
ve	v	k7c6
Vysokých	vysoký	k2eAgFnPc6d1
Tatrách	Tatra	k1gFnPc6
</s>
<s>
Tatranské	tatranský	k2eAgFnPc1d1
elektrické	elektrický	k2eAgFnPc1d1
železnice	železnice	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Město	město	k1gNnSc1
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
KÚ	KÚ	kA
Štrbské	štrbský	k2eAgNnSc1d1
Pleso	pleso	k1gNnSc1
</s>
<s>
Podbanské	Podbanský	k2eAgFnPc1d1
•	•	k?
Vyšné	vyšný	k2eAgFnPc1d1
Hágy	Hága	k1gFnPc1
•	•	k?
Štrbské	štrbský	k2eAgNnSc4d1
Pleso	pleso	k1gNnSc4
KÚ	KÚ	kA
Starý	starý	k2eAgMnSc1d1
Smokovec	Smokovec	k1gMnSc1
</s>
<s>
Dolný	Dolný	k2eAgMnSc1d1
Smokovec	Smokovec	k1gMnSc1
•	•	k?
Horný	horný	k1gMnSc1
Smokovec	Smokovec	k1gMnSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
•	•	k?
Nový	nový	k2eAgMnSc1d1
Smokovec	Smokovec	k1gMnSc1
•	•	k?
Starý	Starý	k1gMnSc1
Smokovec	Smokovec	k1gMnSc1
•	•	k?
Tatranská	tatranský	k2eAgFnSc1d1
Polianka	Polianka	k1gFnSc1
•	•	k?
Tatranské	tatranský	k2eAgFnPc4d1
Zruby	Zruba	k1gFnPc4
KÚ	KÚ	kA
Tatranská	tatranský	k2eAgFnSc1d1
Lomnica	Lomnica	k1gFnSc1
</s>
<s>
Kežmarské	kežmarský	k2eAgFnSc2d1
Žľaby	Žľaba	k1gFnSc2
•	•	k?
Tatranská	tatranský	k2eAgFnSc1d1
Kotlina	kotlina	k1gFnSc1
•	•	k?
Tatranská	tatranský	k2eAgFnSc1d1
Lesná	lesný	k2eAgFnSc1d1
•	•	k?
Tatranská	tatranský	k2eAgFnSc1d1
Lomnica	Lomnica	k1gFnSc1
•	•	k?
Tatranské	tatranský	k2eAgFnPc4d1
Matliare	Matliar	k1gMnSc5
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
obce	obec	k1gFnSc2
okresu	okres	k1gInSc2
Poprad	Poprad	k1gInSc1
</s>
<s>
Batizovce	Batizovec	k1gInPc1
•	•	k?
Gánovce	Gánovec	k1gInSc2
•	•	k?
Gerlachov	Gerlachov	k1gInSc1
•	•	k?
Hozelec	Hozelec	k1gInSc1
•	•	k?
Hôrka	Hôrek	k1gInSc2
•	•	k?
Hranovnica	Hranovnic	k2eAgFnSc1d1
•	•	k?
Jánovce	jánovka	k1gFnSc3
•	•	k?
Kravany	Kravan	k1gMnPc4
•	•	k?
Liptovská	liptovský	k2eAgNnPc4d1
Teplička	tepličko	k1gNnPc4
•	•	k?
Lučivná	Lučivný	k2eAgFnSc1d1
•	•	k?
Mengusovce	Mengusovka	k1gFnSc3
•	•	k?
Mlynica	Mlynic	k1gInSc2
•	•	k?
Nová	nový	k2eAgFnSc1d1
Lesná	lesný	k2eAgFnSc1d1
•	•	k?
Poprad	Poprad	k1gInSc4
•	•	k?
Spišské	spišský	k2eAgInPc4d1
Bystré	bystrý	k2eAgInPc4d1
•	•	k?
Spišský	spišský	k2eAgMnSc1d1
Štiavnik	Štiavnik	k1gMnSc1
•	•	k?
Spišská	spišský	k2eAgFnSc1d1
Teplica	Teplica	k1gFnSc1
•	•	k?
Svit	svit	k1gInSc1
•	•	k?
Štôla	Štôla	k1gFnSc1
•	•	k?
Štrba	Štrba	k1gFnSc1
•	•	k?
Šuňava	Šuňava	k1gFnSc1
•	•	k?
Švábovce	Švábovec	k1gInSc2
•	•	k?
Tatranská	tatranský	k2eAgFnSc1d1
Javorina	Javorina	k1gFnSc1
•	•	k?
Veľký	Veľký	k2eAgInSc1d1
Slavkov	Slavkov	k1gInSc1
•	•	k?
Vernár	Vernár	k1gInSc1
•	•	k?
Vikartovce	Vikartovec	k1gMnSc2
•	•	k?
Vydrník	Vydrník	k1gMnSc1
•	•	k?
Vysoké	vysoký	k2eAgFnSc2d1
Tatry	Tatra	k1gFnSc2
•	•	k?
Ždiar	Ždiar	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
445535	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
238027349	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
