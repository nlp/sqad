<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
a	a	k8xC	a
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
rakouským	rakouský	k2eAgNnSc7d1	rakouské
městem	město	k1gNnSc7	město
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
politickým	politický	k2eAgNnSc7d1	politické
<g/>
,	,	kIx,	,
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
a	a	k8xC	a
kulturním	kulturní	k2eAgNnSc7d1	kulturní
centrem	centrum	k1gNnSc7	centrum
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
