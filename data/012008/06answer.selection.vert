<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pravostranný	pravostranný	k2eAgInSc4d1	pravostranný
přítok	přítok	k1gInSc4	přítok
řeky	řeka	k1gFnSc2	řeka
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
ústí	ústit	k5eAaImIp3nP	ústit
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
136,6	[number]	k4	136,6
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
299,1	[number]	k4	299,1
m.	m.	k?	m.
</s>
