<p>
<s>
Úslava	Úslava	k1gFnSc1	Úslava
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
:	:	kIx,	:
<g/>
Uslawa	Uslawa	k1gFnSc1	Uslawa
<g/>
,	,	kIx,	,
též	též	k9	též
Bradlawa	Bradlawa	k1gMnSc1	Bradlawa
<g/>
,	,	kIx,	,
Ambelsbach	Ambelsbach	k1gMnSc1	Ambelsbach
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pravostranný	pravostranný	k2eAgInSc4d1	pravostranný
přítok	přítok	k1gInSc4	přítok
řeky	řeka	k1gFnSc2	řeka
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
ústí	ústit	k5eAaImIp3nP	ústit
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
na	na	k7c6	na
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
136,6	[number]	k4	136,6
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
299,1	[number]	k4	299,1
m.	m.	k?	m.
</s>
<s>
Délka	délka	k1gFnSc1	délka
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
96,3	[number]	k4	96,3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
jejího	její	k3xOp3gNnSc2	její
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
756,6	[number]	k4	756,6
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Úslava	Úslava	k1gFnSc1	Úslava
<g/>
,	,	kIx,	,
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
nad	nad	k7c7	nad
Žinkovy	Žinkov	k1gInPc7	Žinkov
označovaná	označovaný	k2eAgFnSc1d1	označovaná
též	též	k9	též
jako	jako	k8xS	jako
Bradlava	Bradlava	k1gFnSc1	Bradlava
<g/>
,	,	kIx,	,
pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
úpatí	úpatí	k1gNnSc6	úpatí
kopce	kopec	k1gInSc2	kopec
Drkolná	Drkolný	k2eAgFnSc1d1	Drkolná
(	(	kIx(	(
<g/>
729	[number]	k4	729
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Číhaň	Číhaň	k1gFnSc2	Číhaň
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
</s>
</p>
<p>
<s>
637,2	[number]	k4	637,2
m.	m.	k?	m.
Nejprve	nejprve	k6eAd1	nejprve
teče	téct	k5eAaImIp3nS	téct
k	k	k7c3	k
východu	východ	k1gInSc3	východ
<g/>
,	,	kIx,	,
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Hnačov	Hnačov	k1gInSc1	Hnačov
napájí	napájet	k5eAaImIp3nS	napájet
velký	velký	k2eAgInSc1d1	velký
Hnačovský	Hnačovský	k2eAgInSc1d1	Hnačovský
rybník	rybník	k1gInSc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
Plánice	plánice	k1gFnSc2	plánice
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Žinkovy	Žinkov	k1gInPc7	Žinkov
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
zámku	zámek	k1gInSc2	zámek
Zelená	Zelená	k1gFnSc1	Zelená
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
stojícího	stojící	k2eAgMnSc2d1	stojící
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
kopci	kopec	k1gInSc6	kopec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
u	u	k7c2	u
města	město	k1gNnSc2	město
Nepomuk	Nepomuk	k1gInSc1	Nepomuk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
obcí	obec	k1gFnSc7	obec
Vrčeň	Vrčeň	k1gFnSc1	Vrčeň
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
<g/>
,	,	kIx,	,
protéká	protékat	k5eAaImIp3nS	protékat
městy	město	k1gNnPc7	město
Blovice	Blovice	k1gFnSc2	Blovice
a	a	k8xC	a
Starý	Starý	k1gMnSc1	Starý
Plzenec	Plzenec	k1gMnSc1	Plzenec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
těmito	tento	k3xDgNnPc7	tento
městy	město	k1gNnPc7	město
na	na	k7c6	na
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
26,1	[number]	k4	26,1
<g/>
,	,	kIx,	,
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Nezvěstice	Nezvěstika	k1gFnSc6	Nezvěstika
<g/>
,	,	kIx,	,
přijímá	přijímat	k5eAaImIp3nS	přijímat
zprava	zprava	k6eAd1	zprava
svůj	svůj	k3xOyFgInSc4	svůj
největší	veliký	k2eAgInSc4d3	veliký
přítok	přítok	k1gInSc4	přítok
říčku	říčka	k1gFnSc4	říčka
Bradavu	Bradava	k1gFnSc4	Bradava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přitéká	přitékat	k5eAaImIp3nS	přitékat
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
od	od	k7c2	od
Spáleného	spálený	k2eAgNnSc2d1	spálené
Poříčí	Poříčí	k1gNnSc2	Poříčí
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
směr	směr	k1gInSc1	směr
si	se	k3xPyFc3	se
Úslava	Úslava	k1gFnSc1	Úslava
udržuje	udržovat	k5eAaImIp3nS	udržovat
až	až	k9	až
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
Berounky	Berounka	k1gFnSc2	Berounka
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Větší	veliký	k2eAgInPc1d2	veliký
přítoky	přítok	k1gInPc1	přítok
===	===	k?	===
</s>
</p>
<p>
<s>
levé	levá	k1gFnSc3	levá
–	–	k?	–
Podhrázský	podhrázský	k2eAgInSc4d1	podhrázský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
Olešenský	Olešenský	k2eAgInSc4d1	Olešenský
potok	potok	k1gInSc4	potok
</s>
</p>
<p>
<s>
pravé	pravá	k1gFnPc1	pravá
–	–	k?	–
Mihovka	Mihovka	k1gFnSc1	Mihovka
<g/>
,	,	kIx,	,
Myslívský	Myslívský	k2eAgInSc1d1	Myslívský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
Bradava	Bradava	k1gFnSc1	Bradava
<g/>
,	,	kIx,	,
Kornatický	Kornatický	k2eAgInSc1d1	Kornatický
potok	potok	k1gInSc1	potok
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
Koterov	Koterov	k1gInSc1	Koterov
<g/>
)	)	kIx)	)
na	na	k7c6	na
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
9,1	[number]	k4	9,1
činí	činit	k5eAaImIp3nS	činit
3,52	[number]	k4	3,52
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
<g/>
Hlásné	hlásný	k2eAgInPc4d1	hlásný
profily	profil	k1gInPc4	profil
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
==	==	k?	==
</s>
</p>
<p>
<s>
Číhaň	Číhanit	k5eAaPmRp2nS	Číhanit
<g/>
,	,	kIx,	,
Hnačov	Hnačov	k1gInSc1	Hnačov
<g/>
,	,	kIx,	,
Plánice	plánice	k1gFnSc1	plánice
<g/>
,	,	kIx,	,
Újezd	Újezd	k1gInSc1	Újezd
u	u	k7c2	u
Plánice	plánice	k1gFnSc2	plánice
<g/>
,	,	kIx,	,
Mlýnské	mlýnský	k2eAgNnSc1d1	mlýnské
Struhadlo	struhadlo	k1gNnSc1	struhadlo
<g/>
,	,	kIx,	,
Žinkovy	Žinkov	k1gInPc1	Žinkov
<g/>
,	,	kIx,	,
Prádlo	prádlo	k1gNnSc1	prádlo
<g/>
,	,	kIx,	,
Klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
Vrčeň	Vrčeň	k1gFnSc1	Vrčeň
<g/>
,	,	kIx,	,
Srby	Srb	k1gMnPc4	Srb
<g/>
,	,	kIx,	,
Ždírec	Ždírec	k1gInSc1	Ždírec
<g/>
,	,	kIx,	,
Blovice	Blovice	k1gFnSc1	Blovice
<g/>
,	,	kIx,	,
Zdemyslice	Zdemyslice	k1gFnSc1	Zdemyslice
<g/>
,	,	kIx,	,
Žákava	Žákava	k1gFnSc1	Žákava
<g/>
,	,	kIx,	,
Nezvěstice	Nezvěstika	k1gFnSc6	Nezvěstika
<g/>
,	,	kIx,	,
Šťáhlavy	Šťáhlava	k1gFnPc1	Šťáhlava
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Starý	starý	k2eAgInSc1d1	starý
Plzenec	Plzenec	k1gInSc1	Plzenec
a	a	k8xC	a
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
řeky	řeka	k1gFnSc2	řeka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
řadou	řada	k1gFnSc7	řada
chyb	chyba	k1gFnPc2	chyba
při	při	k7c6	při
vydání	vydání	k1gNnSc6	vydání
knihy	kniha	k1gFnSc2	kniha
Das	Das	k1gFnSc2	Das
itzlebende	itzlebend	k1gInSc5	itzlebend
Konigreich	Konigreich	k1gInSc1	Konigreich
Bohmen	Bohmen	k2eAgMnSc1d1	Bohmen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgNnSc1d1	původní
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Bradlava	Bradlava	k1gFnSc1	Bradlava
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
jen	jen	k9	jen
její	její	k3xOp3gInSc4	její
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
fotografie	fotografia	k1gFnPc1	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Úslava	Úslava	k1gFnSc1	Úslava
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Úslava	Úslava	k1gFnSc1	Úslava
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Koterov	Koterov	k1gInSc1	Koterov
–	–	k?	–
aktuální	aktuální	k2eAgInSc1d1	aktuální
vodní	vodní	k2eAgInSc1d1	vodní
stav	stav	k1gInSc1	stav
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ČHMÚ	ČHMÚ	kA	ČHMÚ
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
charakteristiky	charakteristika	k1gFnPc1	charakteristika
toku	tok	k1gInSc2	tok
Úslava	Úslava	k1gFnSc1	Úslava
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
povodí	povodit	k5eAaPmIp3nS	povodit
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c6	na
Úslava	Úslava	k1gFnSc1	Úslava
</s>
</p>
