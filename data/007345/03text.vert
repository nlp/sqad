<s>
Podpatek	podpatek	k1gInSc1	podpatek
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
obuvi	obuv	k1gFnSc2	obuv
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
pod	pod	k7c7	pod
patou	pata	k1gFnSc7	pata
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgInPc1d1	nízký
podpatky	podpatek	k1gInPc1	podpatek
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
stabilní	stabilní	k2eAgFnSc4d1	stabilní
chůzi	chůze	k1gFnSc4	chůze
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
na	na	k7c6	na
trekingových	trekingový	k2eAgFnPc6d1	trekingová
botách	bota	k1gFnPc6	bota
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
na	na	k7c6	na
pánské	pánský	k2eAgFnSc6d1	pánská
obuvi	obuv	k1gFnSc6	obuv
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgInSc4d1	sportovní
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
<s>
Dámské	dámský	k2eAgFnPc4d1	dámská
společenské	společenský	k2eAgFnPc4d1	společenská
boty	bota	k1gFnPc4	bota
mívají	mívat	k5eAaImIp3nP	mívat
podpatky	podpatek	k1gInPc1	podpatek
vysoké	vysoká	k1gFnSc2	vysoká
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
obuv	obuv	k1gFnSc1	obuv
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
loďce	loďka	k1gFnSc3	loďka
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
lodičky	lodička	k1gFnPc4	lodička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podpatkem	podpatek	k1gInSc7	podpatek
zdaleka	zdaleka	k6eAd1	zdaleka
nejsou	být	k5eNaImIp3nP	být
moderním	moderní	k2eAgInSc7d1	moderní
vynálezem	vynález	k1gInSc7	vynález
a	a	k8xC	a
obojí	oboj	k1gFnSc7	oboj
<g/>
,	,	kIx,	,
pánské	pánský	k2eAgNnSc1d1	pánské
i	i	k8xC	i
dámské	dámský	k2eAgNnSc1d1	dámské
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
bohatou	bohatý	k2eAgFnSc4d1	bohatá
historii	historie	k1gFnSc4	historie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spatřily	spatřit	k5eAaPmAgFnP	spatřit
světlo	světlo	k1gNnSc4	světlo
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
panují	panovat	k5eAaImIp3nP	panovat
určité	určitý	k2eAgInPc4d1	určitý
dohady	dohad	k1gInPc4	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
jsou	být	k5eAaImIp3nP	být
boty	bota	k1gFnPc4	bota
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podpatkem	podpatek	k1gInSc7	podpatek
vyobrazeny	vyobrazen	k2eAgInPc4d1	vyobrazen
již	již	k6eAd1	již
na	na	k7c6	na
staroegyptských	staroegyptský	k2eAgFnPc6d1	staroegyptská
nástěnných	nástěnný	k2eAgFnPc6d1	nástěnná
malbách	malba	k1gFnPc6	malba
v	v	k7c6	v
hrobkách	hrobka	k1gFnPc6	hrobka
a	a	k8xC	a
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
dochované	dochovaný	k2eAgInPc1d1	dochovaný
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
mužích	muž	k1gMnPc6	muž
a	a	k8xC	a
ženách	žena	k1gFnPc6	žena
nosících	nosící	k2eAgInPc2d1	nosící
boty	bota	k1gFnPc4	bota
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
podpatkem	podpatek	k1gInSc7	podpatek
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
obecný	obecný	k2eAgInSc4d1	obecný
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
boty	bota	k1gFnPc1	bota
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podpatkem	podpatek	k1gInSc7	podpatek
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Medicejské	Medicejský	k2eAgFnSc2d1	Medicejská
s	s	k7c7	s
Vévodou	vévoda	k1gMnSc7	vévoda
orléanským	orléanský	k2eAgMnSc7d1	orléanský
<g/>
,	,	kIx,	,
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1533	[number]	k4	1533
<g/>
.	.	kIx.	.
</s>
<s>
Nevěsta	nevěsta	k1gFnSc1	nevěsta
měla	mít	k5eAaImAgFnS	mít
boty	bota	k1gFnPc4	bota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jí	jíst	k5eAaImIp3nS	jíst
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
zhotovili	zhotovit	k5eAaPmAgMnP	zhotovit
florentští	florentský	k2eAgMnPc1d1	florentský
ševci	švec	k1gMnPc1	švec
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
italské	italský	k2eAgFnPc1d1	italská
boty	bota	k1gFnPc1	bota
staly	stát	k5eAaPmAgFnP	stát
normou	norma	k1gFnSc7	norma
dámské	dámský	k2eAgFnPc1d1	dámská
obuvi	obuv	k1gFnPc1	obuv
na	na	k7c6	na
vévodově	vévodův	k2eAgInSc6d1	vévodův
dvoře	dvůr	k1gInSc6	dvůr
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
o	o	k7c4	o
historický	historický	k2eAgInSc4d1	historický
podvrh	podvrh	k1gInSc4	podvrh
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
podpatků	podpatek	k1gInPc2	podpatek
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
teprve	teprve	k6eAd1	teprve
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podpatkem	podpatek	k1gInSc7	podpatek
nosila	nosit	k5eAaImAgFnS	nosit
údajně	údajně	k6eAd1	údajně
i	i	k9	i
anglická	anglický	k2eAgFnSc1d1	anglická
královna	královna	k1gFnSc1	královna
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1516	[number]	k4	1516
<g/>
–	–	k?	–
<g/>
1558	[number]	k4	1558
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1660	[number]	k4	1660
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
švec	švec	k1gMnSc1	švec
Nicholas	Nicholas	k1gMnSc1	Nicholas
Lestage	Lestage	k1gInSc4	Lestage
boty	bota	k1gFnSc2	bota
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
podpatkem	podpatek	k1gInSc7	podpatek
pro	pro	k7c4	pro
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
byly	být	k5eAaImAgFnP	být
až	až	k9	až
10	[number]	k4	10
cm	cm	kA	cm
vysoké	vysoký	k2eAgFnPc4d1	vysoká
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
zdobily	zdobit	k5eAaImAgInP	zdobit
různé	různý	k2eAgInPc1d1	různý
bitevní	bitevní	k2eAgInPc1d1	bitevní
výjevy	výjev	k1gInPc1	výjev
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
tzv.	tzv.	kA	tzv.
Ludvíkovský	ludvíkovský	k2eAgInSc1d1	ludvíkovský
podpatek	podpatek	k1gInSc1	podpatek
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgInS	stát
módním	módní	k2eAgInSc7d1	módní
prvkem	prvek	k1gInSc7	prvek
dámské	dámský	k2eAgFnSc2d1	dámská
garderoby	garderoba	k1gFnSc2	garderoba
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
podpatků	podpatek	k1gInPc2	podpatek
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
nosila	nosit	k5eAaImAgFnS	nosit
Madame	madame	k1gFnSc1	madame
de	de	k?	de
Pompadour	Pompadoura	k1gFnPc2	Pompadoura
<g/>
,	,	kIx,	,
milenka	milenka	k1gFnSc1	milenka
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
též	též	k9	též
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
Pompadourky	Pompadourk	k1gInPc4	Pompadourk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trend	trend	k1gInSc1	trend
k	k	k7c3	k
nižším	nízký	k2eAgInPc3d2	nižší
podpatkům	podpatek	k1gInPc3	podpatek
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
podnícen	podnícet	k5eAaImNgMnS	podnícet
Velkou	velký	k2eAgFnSc7d1	velká
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vysoké	vysoký	k2eAgInPc1d1	vysoký
podpatky	podpatek	k1gInPc1	podpatek
staly	stát	k5eAaPmAgInP	stát
symbolem	symbol	k1gInSc7	symbol
blahobytu	blahobyt	k1gInSc2	blahobyt
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
pochopitelných	pochopitelný	k2eAgInPc2d1	pochopitelný
důvodů	důvod	k1gInPc2	důvod
snažili	snažit	k5eAaImAgMnP	snažit
nevypadat	vypadat	k5eNaImF	vypadat
jako	jako	k9	jako
bohatí	bohatý	k2eAgMnPc1d1	bohatý
<g/>
,	,	kIx,	,
zmizely	zmizet	k5eAaPmAgInP	zmizet
mužské	mužský	k2eAgInPc1d1	mužský
i	i	k8xC	i
ženské	ženský	k2eAgInPc4d1	ženský
vysoké	vysoký	k2eAgInPc4d1	vysoký
podpatky	podpatek	k1gInPc4	podpatek
rychle	rychle	k6eAd1	rychle
z	z	k7c2	z
ulic	ulice	k1gFnPc2	ulice
i	i	k9	i
z	z	k7c2	z
běžného	běžný	k2eAgInSc2d1	běžný
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
nosily	nosit	k5eAaImAgInP	nosit
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgInPc1d2	nižší
podpatky	podpatek	k1gInPc1	podpatek
než	než	k8xS	než
kdykoli	kdykoli	k6eAd1	kdykoli
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Heel	Heel	k1gInSc1	Heel
(	(	kIx(	(
<g/>
shoe	shoe	k1gInSc1	shoe
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
