<s>
Nízké	nízký	k2eAgInPc1d1	nízký
podpatky	podpatek	k1gInPc1	podpatek
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
stabilní	stabilní	k2eAgFnSc4d1	stabilní
chůzi	chůze	k1gFnSc4	chůze
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
,	,	kIx,	,
na	na	k7c6	na
trekingových	trekingový	k2eAgFnPc6d1	trekingová
botách	bota	k1gFnPc6	bota
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
na	na	k7c6	na
pánské	pánský	k2eAgFnSc6d1	pánská
obuvi	obuv	k1gFnSc6	obuv
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
sportovní	sportovní	k2eAgInSc4d1	sportovní
tanec	tanec	k1gInSc4	tanec
<g/>
.	.	kIx.	.
</s>
