<s>
Vikariáty	vikariát	k1gInPc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
Litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
je	být	k5eAaImIp3nS
označena	označit	k5eAaPmNgFnS
na	na	k7c6
mapě	mapa	k1gFnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
tmavě	tmavě	k6eAd1
modrou	modrý	k2eAgFnSc7d1
barvou	barva	k1gFnSc7
</s>
<s>
[[	[[	k?
<g/>
file	file	k1gNnPc6
<g/>
:	:	kIx,
<g/>
Diecéze_litoměřická	Diecéze_litoměřická	k1gFnSc1
<g/>
,	,	kIx,
<g/>
_vikariáty	_vikariát	k1gInPc1
<g/>
.	.	kIx.
<g/>
jpg	jpg	k?
<g/>
|	|	kIx~
<g/>
500	#num#	k4
<g/>
px	px	k?
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
<g/>
|	|	kIx~
<g/>
{{{	{{{	k?
<g/>
alt	alt	k1gInSc1
<g/>
}}}	}}}	k?
<g/>
|	|	kIx~
<g/>
]]	]]	k?
<g/>
Českolipskývikariát	Českolipskývikariát	k1gInSc1
</s>
<s>
Děčínskývikariát	Děčínskývikariát	k1gMnSc1
</s>
<s>
Krušnohorskývikariát	Krušnohorskývikariát	k1gMnSc1
</s>
<s>
Libereckývikariát	Libereckývikariát	k1gMnSc1
</s>
<s>
Litoměřickývikariát	Litoměřickývikariát	k1gMnSc1
</s>
<s>
Lounskývikariát	Lounskývikariát	k1gMnSc1
</s>
<s>
Mladoboleslavskývikariát	Mladoboleslavskývikariát	k1gMnSc1
</s>
<s>
Teplickývikariát	Teplickývikariát	k1gMnSc1
</s>
<s>
Turnovskývikariát	Turnovskývikariát	k1gMnSc1
</s>
<s>
Ústeckývikariát	Ústeckývikariát	k1gMnSc1
</s>
<s>
Litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
je	být	k5eAaImIp3nS
územně	územně	k6eAd1
členěna	členit	k5eAaImNgFnS
na	na	k7c4
10	#num#	k4
vikariátů	vikariát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
každého	každý	k3xTgInSc2
vikariátu	vikariát	k1gInSc2
stojí	stát	k5eAaImIp3nS
okrskový	okrskový	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozsahem	rozsah	k1gInSc7
jsou	být	k5eAaImIp3nP
území	území	k1gNnPc1
spravovaná	spravovaný	k2eAgFnSc1d1
okrskovými	okrskový	k2eAgMnPc7d1
vikáři	vikář	k1gMnPc7
z	z	k7c2
hlediska	hledisko	k1gNnSc2
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
podobná	podobný	k2eAgFnSc1d1
okresům	okres	k1gInPc3
v	v	k7c6
ČR	ČR	kA
<g/>
,	,	kIx,
které	který	k3yQgFnSc3,k3yIgFnSc3,k3yRgFnSc3
však	však	k9
vzhledem	vzhledem	k7c3
k	k	k7c3
historickému	historický	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
vždy	vždy	k6eAd1
ne	ne	k9
zcela	zcela	k6eAd1
kopírují	kopírovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
je	být	k5eAaImIp3nS
územně	územně	k6eAd1
členěna	členit	k5eAaImNgFnS
na	na	k7c4
10	#num#	k4
vikariátů	vikariát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jejich	jejich	k3xOp3gNnSc6
čele	čelo	k1gNnSc6
stojí	stát	k5eAaImIp3nP
okrskoví	okrskový	k2eAgMnPc1d1
vikáři	vikář	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
vikariát	vikariát	k1gInSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
farnosti	farnost	k1gFnPc4
<g/>
,	,	kIx,
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
většinou	většinou	k6eAd1
sdružené	sdružený	k2eAgInPc1d1
do	do	k7c2
farních	farní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
z	z	k7c2
důvodu	důvod	k1gInSc2
nedostatku	nedostatek	k1gInSc2
kněží	kněz	k1gMnPc2
má	mít	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
kněz	kněz	k1gMnSc1
na	na	k7c6
starosti	starost	k1gFnSc6
farností	farnost	k1gFnPc2
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
vikariátů	vikariát	k1gInPc2
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
</s>
<s>
Od	od	k7c2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1655	#num#	k4
litoměřická	litoměřický	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
co	co	k8xS
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
svého	svůj	k3xOyFgNnSc2
vnitřního	vnitřní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
několikrát	několikrát	k6eAd1
změnila	změnit	k5eAaPmAgFnS
svou	svůj	k3xOyFgFnSc4
organizační	organizační	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
měnila	měnit	k5eAaImAgFnS
také	také	k9
svou	svůj	k3xOyFgFnSc4
velikost	velikost	k1gFnSc4
a	a	k8xC
postupně	postupně	k6eAd1
se	se	k3xPyFc4
zvětšovala	zvětšovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
členila	členit	k5eAaImAgFnS
na	na	k7c4
jednotlivé	jednotlivý	k2eAgInPc4d1
vikariáty	vikariát	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgNnPc6
historických	historický	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
(	(	kIx(
<g/>
např.	např.	kA
za	za	k7c4
nacistické	nacistický	k2eAgFnPc4d1
okupace	okupace	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
krátkodobě	krátkodobě	k6eAd1
i	i	k9
na	na	k7c4
jiné	jiný	k2eAgFnPc4d1
odvozené	odvozený	k2eAgFnPc4d1
organizační	organizační	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Tam	tam	k6eAd1
kde	kde	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
objevují	objevovat	k5eAaImIp3nP
historické	historický	k2eAgInPc4d1
tituly	titul	k1gInPc4
proboštství	proboštství	k1gNnSc2
(	(	kIx(
<g/>
Mělník	Mělník	k1gInSc1
od	od	k7c2
r.	r.	kA
1896	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
arciděkanství	arciděkanství	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
případů	případ	k1gInPc2
–	–	k?
Horní	horní	k2eAgFnPc1d1
Police	police	k1gFnPc1
od	od	k7c2
roku	rok	k1gInSc2
1723	#num#	k4
<g/>
,	,	kIx,
Liberec	Liberec	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1879	#num#	k4
<g/>
,	,	kIx,
Šluknov	Šluknov	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
<g/>
,	,	kIx,
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
od	od	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
,	,	kIx,
Bílina	Bílina	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1934	#num#	k4
a	a	k8xC
Mladá	mladý	k2eAgFnSc1d1
Boleslav	Boleslav	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
děkanství	děkanství	k1gNnSc4
(	(	kIx(
<g/>
40	#num#	k4
případů	případ	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tyto	tento	k3xDgInPc1
názvy	název	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
pocházejí	pocházet	k5eAaImIp3nP
buď	buď	k8xC
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
vznikem	vznik	k1gInSc7
diecéze	diecéze	k1gFnSc2
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
významné	významný	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
získaly	získat	k5eAaPmAgInP
jako	jako	k9
čestné	čestný	k2eAgInPc1d1
tituly	titul	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejde	jít	k5eNaImIp3nS
zde	zde	k6eAd1
však	však	k9
o	o	k7c4
bývalé	bývalý	k2eAgFnPc4d1
organizační	organizační	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
vikariáty	vikariát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
těchto	tento	k3xDgInPc2
titulů	titul	k1gInPc2
a	a	k8xC
datum	datum	k1gNnSc4
jejich	jejich	k3xOp3gInSc2
vzniku	vznik	k1gInSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
najít	najít	k5eAaPmF
v	v	k7c6
seznamu	seznam	k1gInSc6
farností	farnost	k1gFnPc2
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
příklad	příklad	k1gInSc4
staršího	starý	k2eAgNnSc2d2
vikariátního	vikariátní	k2eAgNnSc2d1
členění	členění	k1gNnSc2
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
údaj	údaj	k1gInSc4
v	v	k7c6
Almanachu	almanach	k1gInSc6
katolického	katolický	k2eAgInSc2d1
kléru	klér	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1912	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
sestavil	sestavit	k5eAaPmAgMnS
P.	P.	kA
Canisius	Canisius	k1gMnSc1
Werner	Werner	k1gMnSc1
S.	S.	kA
<g/>
D.	D.	kA
<g/>
S.	S.	kA
a	a	k8xC
katecheta	katecheta	k1gMnSc1
z	z	k7c2
Rychnova	Rychnov	k1gInSc2
u	u	k7c2
Jablonce	Jablonec	k1gInSc2
nad	nad	k7c7
Nisou	Nisa	k1gFnSc7
Anton	Anton	k1gMnSc1
Ramsch	Ramsch	k1gMnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
bylo	být	k5eAaImAgNnS
26	#num#	k4
vikariátních	vikariátní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
:	:	kIx,
Auscha	Auscha	k1gFnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Aussig	Aussig	k1gInSc1
<g/>
,	,	kIx,
Bilin	Bilin	k1gInSc1
<g/>
,	,	kIx,
Brüx	Brüx	k1gInSc1
<g/>
,	,	kIx,
Friedland	Friedland	k1gInSc1
<g/>
,	,	kIx,
Gabel	Gabel	k1gMnSc1
<g/>
,	,	kIx,
Gablonz	Gablonz	k1gMnSc1
<g/>
,	,	kIx,
Hainspach	Hainspach	k1gMnSc1
<g/>
,	,	kIx,
Hirschberg	Hirschberg	k1gMnSc1
<g/>
,	,	kIx,
Leitmeritz	Leitmeritz	k1gMnSc1
<g/>
,	,	kIx,
Libochowitz	Libochowitz	k1gMnSc1
<g/>
,	,	kIx,
Jungbunzlau	Jungbunzlaus	k1gInSc2
<g/>
,	,	kIx,
Jechnitz	Jechnitza	k1gFnPc2
<g/>
,	,	kIx,
Kaaden	Kaadna	k1gFnPc2
<g/>
,	,	kIx,
Kamnitz	Kamnitza	k1gFnPc2
<g/>
,	,	kIx,
Komotau	Komotaus	k1gInSc2
<g/>
,	,	kIx,
Laun	Laun	k1gInSc1
<g/>
,	,	kIx,
Leipa	Leipa	k1gFnSc1
<g/>
,	,	kIx,
Melnik	Melnik	k1gMnSc1
<g/>
,	,	kIx,
Münchengrätz	Münchengrätz	k1gMnSc1
<g/>
,	,	kIx,
Nimburg	Nimburg	k1gMnSc1
<g/>
,	,	kIx,
Reichenberg	Reichenberg	k1gMnSc1
<g/>
,	,	kIx,
Saaz	Saaz	k1gMnSc1
<g/>
,	,	kIx,
Semil	Semily	k1gInPc2
<g/>
,	,	kIx,
Teplitz	Teplitza	k1gFnPc2
a	a	k8xC
Turnau	Turnaus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
zatím	zatím	k6eAd1
poslední	poslední	k2eAgFnSc3d1
velké	velká	k1gFnSc3
organizační	organizační	k2eAgFnSc6d1
úpravě	úprava	k1gFnSc6
co	co	k9
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
vikariátní	vikariátní	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
rámci	rámec	k1gInSc6
změn	změna	k1gFnPc2
hranic	hranice	k1gFnPc2
diecézí	diecéze	k1gFnPc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
1993	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zanikl	zaniknout	k5eAaPmAgInS
Nymburský	nymburský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
farnosti	farnost	k1gFnPc1
tohoto	tento	k3xDgInSc2
vikariátu	vikariát	k1gInSc2
byly	být	k5eAaImAgFnP
přičleněny	přičlenit	k5eAaPmNgFnP
do	do	k7c2
pražské	pražský	k2eAgFnSc2d1
arcidiecéze	arcidiecéze	k1gFnSc2
<g/>
,	,	kIx,
zbytek	zbytek	k1gInSc4
farností	farnost	k1gFnPc2
pak	pak	k6eAd1
rozdělen	rozdělit	k5eAaPmNgInS
mezi	mezi	k7c4
mladoboleslavský	mladoboleslavský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
a	a	k8xC
turnovský	turnovský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
v	v	k7c6
rámci	rámec	k1gInSc6
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
vikariátů	vikariát	k1gInPc2
se	se	k3xPyFc4
tak	tak	k6eAd1
ustálil	ustálit	k5eAaPmAgMnS
na	na	k7c6
desítce	desítka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úplně	úplně	k6eAd1
poslední	poslední	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
bylo	být	k5eAaImAgNnS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
dekretu	dekret	k1gInSc2
Kongregace	kongregace	k1gFnSc2
pro	pro	k7c4
biskupy	biskup	k1gInPc4
vyřazení	vyřazení	k1gNnSc4
farnosti	farnost	k1gFnSc2
Vejprty	Vejprta	k1gFnSc2
z	z	k7c2
plzeňské	plzeňský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
v	v	k7c6
karlovarském	karlovarský	k2eAgInSc6d1
vikariátu	vikariát	k1gInSc6
a	a	k8xC
včlenění	včlenění	k1gNnSc1
této	tento	k3xDgFnSc2
farnosti	farnost	k1gFnSc2
do	do	k7c2
vikariátu	vikariát	k1gInSc2
krušnohorského	krušnohorský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Vikariáty	vikariát	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
znaky	znak	k1gInPc1
</s>
<s>
Znak	znak	k1gInSc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
používaný	používaný	k2eAgInSc4d1
od	od	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
<g/>
Autorem	autor	k1gMnSc7
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
Břetislav	Břetislav	k1gMnSc1
Štorm	Štorm	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
jej	on	k3xPp3gMnSc4
pro	pro	k7c4
biskupa	biskup	k1gMnSc2
Štěpána	Štěpán	k1gMnSc2
Trochtu	Trochtu	k?
navrhl	navrhnout	k5eAaPmAgMnS
původně	původně	k6eAd1
takto	takto	k6eAd1
<g/>
:	:	kIx,
v	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
čtvrcený	čtvrcený	k2eAgInSc4d1
heroltský	heroltský	k2eAgInSc4d1
kříž	kříž	k1gInSc4
střídavých	střídavý	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
stříbrné	stříbrný	k2eAgFnPc4d1
a	a	k8xC
červené	červený	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Znaky	znak	k1gInPc1
vikariátů	vikariát	k1gInPc2
</s>
<s>
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2019	#num#	k4
používají	používat	k5eAaImIp3nP
vikariáty	vikariát	k1gInPc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
vlastní	vlastní	k2eAgInPc4d1
znaky	znak	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
používají	používat	k5eAaImIp3nP
za	za	k7c4
základ	základ	k1gInSc4
znak	znak	k1gInSc4
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znakem	znak	k1gInSc7
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc4d1
kříž	kříž	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Štít	štít	k1gInSc1
je	být	k5eAaImIp3nS
podložen	podložit	k5eAaPmNgInS
zlatým	zlatý	k2eAgInSc7d1
latinským	latinský	k2eAgInSc7d1
křížem	kříž	k1gInSc7
provázeným	provázený	k2eAgInSc7d1
mitrou	mitra	k1gFnSc7
a	a	k8xC
berlou	berla	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšit	k5eAaPmNgNnS
zeleným	zelený	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
šesti	šest	k4xCc7
střapci	střapec	k1gInPc7
po	po	k7c6
každé	každý	k3xTgFnSc6
straně	strana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vikariáty	vikariát	k1gInPc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
žádnými	žádný	k3yNgFnPc7
znaky	znak	k1gInPc4
nedisponovaly	disponovat	k5eNaBmAgFnP
<g/>
,	,	kIx,
užívaly	užívat	k5eAaImAgFnP
však	však	k9
razítka	razítko	k1gNnPc4
<g/>
,	,	kIx,
v	v	k7c6
jejichž	jejichž	k3xOyRp3gNnSc6
poli	pole	k1gNnSc6
se	se	k3xPyFc4
uplatňoval	uplatňovat	k5eAaImAgInS
znak	znak	k1gInSc1
litoměřického	litoměřický	k2eAgMnSc2d1
biskupa	biskup	k1gMnSc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
od	od	k7c2
episkopátu	episkopát	k1gInSc2
Josefa	Josef	k1gMnSc2
Hurdálka	Hurdálek	k1gMnSc2
(	(	kIx(
<g/>
1815	#num#	k4
<g/>
–	–	k?
<g/>
1822	#num#	k4
<g/>
)	)	kIx)
zůstal	zůstat	k5eAaPmAgInS
tento	tento	k3xDgInSc1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
prostý	prostý	k2eAgInSc1d1
latinský	latinský	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
nezměněný	změněný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
začalo	začít	k5eAaPmAgNnS
být	být	k5eAaImF
turnovským	turnovský	k2eAgInSc7d1
vikariátem	vikariát	k1gInSc7
využíváno	využíván	k2eAgNnSc4d1
razítko	razítko	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
poli	pole	k1gNnSc6
byl	být	k5eAaImAgInS
tradičně	tradičně	k6eAd1
užívaný	užívaný	k2eAgInSc1d1
znak	znak	k1gInSc1
diecéze	diecéze	k1gFnSc2
doplněný	doplněný	k2eAgInSc4d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
třemi	tři	k4xCgFnPc7
zlatými	zlatý	k2eAgFnPc7d1
mincemi	mince	k1gFnPc7
(	(	kIx(
<g/>
odkaz	odkaz	k1gInSc1
na	na	k7c4
svatomikulášské	svatomikulášský	k2eAgNnSc4d1
patrocinium	patrocinium	k1gNnSc4
turnovského	turnovský	k2eAgInSc2d1
děkanského	děkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
;	;	kIx,
autorem	autor	k1gMnSc7
znaku	znak	k1gInSc2
byl	být	k5eAaImAgMnS
slovenský	slovenský	k2eAgMnSc1d1
církevní	církevní	k2eAgMnSc1d1
heraldik	heraldik	k1gMnSc1
Zdenko	Zdenka	k1gFnSc5
G.	G.	kA
Alexy	Alexa	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Znaky	znak	k1gInPc1
farností	farnost	k1gFnPc2
odvozené	odvozený	k2eAgInPc4d1
od	od	k7c2
znaků	znak	k1gInPc2
vikariátů	vikariát	k1gInPc2
</s>
<s>
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
farností	farnost	k1gFnSc7
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
několik	několik	k4yIc1
málo	málo	k4c1
farností	farnost	k1gFnPc2
v	v	k7c6
litoměřické	litoměřický	k2eAgFnSc6d1
diecézi	diecéze	k1gFnSc6
disponuje	disponovat	k5eAaBmIp3nS
svým	svůj	k3xOyFgInSc7
historickým	historický	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začaly	začít	k5eAaPmAgFnP
vznikat	vznikat	k5eAaImF
nové	nový	k2eAgInPc4d1
znaky	znak	k1gInPc4
farností	farnost	k1gFnPc2
v	v	k7c6
turnovském	turnovský	k2eAgInSc6d1
vikariátu	vikariát	k1gInSc6
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
v	v	k7c6
nich	on	k3xPp3gFnPc6
byl	být	k5eAaImAgInS
vždy	vždy	k6eAd1
využit	využít	k5eAaPmNgInS
i	i	k8xC
vikariátní	vikariátní	k2eAgInSc1d1
znak	znak	k1gInSc1
(	(	kIx(
<g/>
autorem	autor	k1gMnSc7
této	tento	k3xDgFnSc2
koncepce	koncepce	k1gFnSc2
byl	být	k5eAaImAgInS
opět	opět	k6eAd1
Zdenko	Zdenka	k1gFnSc5
G.	G.	kA
Alexy	Alexa	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
takto	takto	k6eAd1
vytvořenou	vytvořený	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
a	a	k8xC
především	především	k9
praktičnost	praktičnost	k1gFnSc1
a	a	k8xC
jednoznačnost	jednoznačnost	k1gFnSc1
takovýchto	takovýto	k3xDgInPc2
znaků	znak	k1gInPc2
(	(	kIx(
<g/>
znak	znak	k1gInSc1
plně	plně	k6eAd1
vypovídá	vypovídat	k5eAaPmIp3nS,k5eAaImIp3nS
o	o	k7c6
příslušnosti	příslušnost	k1gFnSc6
k	k	k7c3
diecézi	diecéze	k1gFnSc3
i	i	k8xC
vikariátu	vikariát	k1gInSc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
níže	nízce	k6eAd2
uvedená	uvedený	k2eAgNnPc4d1
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
tvorbu	tvorba	k1gFnSc4
nových	nový	k2eAgInPc2d1
znaků	znak	k1gInPc2
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
farnost	farnost	k1gFnSc1
již	již	k6eAd1
disponuje	disponovat	k5eAaBmIp3nS
znakem	znak	k1gInSc7
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
je	být	k5eAaImIp3nS
jakéhokoliv	jakýkoliv	k3yIgMnSc4
staří	starý	k2eAgMnPc1d1
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
právo	právo	k1gNnSc1
jej	on	k3xPp3gMnSc4
nadále	nadále	k6eAd1
bez	bez	k7c2
omezení	omezení	k1gNnSc2
užívat	užívat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
chce	chtít	k5eAaImIp3nS
farnost	farnost	k1gFnSc4
nový	nový	k2eAgInSc4d1
znak	znak	k1gInSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
dodržet	dodržet	k5eAaPmF
následující	následující	k2eAgInSc4d1
postup	postup	k1gInSc4
<g/>
:	:	kIx,
</s>
<s>
Nový	nový	k2eAgInSc1d1
znak	znak	k1gInSc1
farnosti	farnost	k1gFnSc2
se	se	k3xPyFc4
tvoří	tvořit	k5eAaImIp3nP
tím	ten	k3xDgInSc7
způsobem	způsob	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
jedna	jeden	k4xCgFnSc1
jeho	jeho	k3xOp3gFnSc4
polovina	polovina	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
znak	znak	k1gInSc4
příslušného	příslušný	k2eAgInSc2d1
vikariátu	vikariát	k1gInSc2
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
vzniká	vznikat	k5eAaImIp3nS
za	za	k7c4
použití	použití	k1gNnSc4
heraldických	heraldický	k2eAgFnPc2d1
figur	figura	k1gFnPc2
odkazujících	odkazující	k2eAgFnPc2d1
na	na	k7c4
patrocinium	patrocinium	k1gNnSc4
farního	farní	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možnosti	možnost	k1gFnSc3
půlení	půlení	k1gNnSc1
znaku	znak	k1gInSc2
na	na	k7c4
poloviny	polovina	k1gFnPc4
jsou	být	k5eAaImIp3nP
dvě	dva	k4xCgFnPc1
<g/>
,	,	kIx,
a	a	k8xC
sice	sice	k8xC
<g/>
:	:	kIx,
</s>
<s>
tzv.	tzv.	kA
polcení	polcení	k1gNnSc1
(	(	kIx(
<g/>
tj.	tj.	kA
svislé	svislý	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
znak	znak	k1gInSc1
vikariátu	vikariát	k1gInSc2
bude	být	k5eAaImBp3nS
na	na	k7c6
heraldicky	heraldicky	k6eAd1
pravé	pravý	k2eAgFnSc6d1
straně	strana	k1gFnSc6
znaku	znak	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
tzv.	tzv.	kA
dělení	dělení	k1gNnSc1
(	(	kIx(
<g/>
tj.	tj.	kA
vodorovné	vodorovný	k2eAgNnSc1d1
rozdělení	rozdělení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
provedeno	provést	k5eAaPmNgNnS
stejnoměrně	stejnoměrně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
i	i	k9
tzv.	tzv.	kA
hlavu	hlava	k1gFnSc4
štítu	štít	k1gInSc2
(	(	kIx(
<g/>
ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
užší	úzký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
jeho	jeho	k3xOp3gFnSc1
spodní	spodní	k2eAgFnSc1d1
polovina	polovina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
znak	znak	k1gInSc1
vikariátu	vikariát	k1gInSc2
bude	být	k5eAaImBp3nS
při	při	k7c6
užití	užití	k1gNnSc3
jakékoliv	jakýkoliv	k3yIgFnSc2
z	z	k7c2
variant	varianta	k1gFnPc2
vždy	vždy	k6eAd1
v	v	k7c6
horní	horní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
znaku	znak	k1gInSc2
<g/>
;	;	kIx,
</s>
<s>
V	v	k7c6
odůvodněných	odůvodněný	k2eAgInPc6d1
případech	případ	k1gInPc6
lze	lze	k6eAd1
použít	použít	k5eAaPmF
i	i	k9
jiné	jiný	k2eAgFnPc1d1
varianty	varianta	k1gFnPc1
sloučení	sloučení	k1gNnSc2
vikariátního	vikariátní	k2eAgInSc2d1
znaku	znak	k1gInSc2
a	a	k8xC
„	„	k?
<g/>
farní	farní	k2eAgInSc1d1
<g/>
“	“	k?
heraldické	heraldický	k2eAgFnPc1d1
figury	figura	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
užitím	užití	k1gNnSc7
tzv.	tzv.	kA
volné	volný	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
s	s	k7c7
vikariátním	vikariátní	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Blason	Blason	k1gNnSc1
(	(	kIx(
<g/>
odborný	odborný	k2eAgInSc4d1
popis	popis	k1gInSc4
<g/>
)	)	kIx)
nově	nově	k6eAd1
vytvářeného	vytvářený	k2eAgInSc2d1
znaku	znak	k1gInSc2
farnosti	farnost	k1gFnSc2
(	(	kIx(
<g/>
ideálně	ideálně	k6eAd1
společně	společně	k6eAd1
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
výtvarným	výtvarný	k2eAgNnSc7d1
ztvárněním	ztvárnění	k1gNnSc7
<g/>
)	)	kIx)
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
schválen	schválit	k5eAaPmNgInS
litoměřickým	litoměřický	k2eAgInSc7d1
biskupem	biskup	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neschválený	schválený	k2eNgInSc4d1
znak	znak	k1gInSc4
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
používat	používat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přehled	přehled	k1gInSc1
vikariátů	vikariát	k1gInPc2
</s>
<s>
VikariátNáčrt	VikariátNáčrt	k1gInSc1
územíBlasony	územíBlasona	k1gFnSc2
vikariátních	vikariátní	k2eAgMnPc2d1
znakůZnak	znakůZnak	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Českolipský	českolipský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
stříbrnou	stříbrný	k2eAgFnSc7d1
lvicí	lvice	k1gFnSc7
se	s	k7c7
zlatou	zlatý	k2eAgFnSc7d1
zbrojí	zbroj	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc4d1
symbol	symbol	k1gInSc4
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c6
sv.	sv.	kA
Zdislavu	Zdislava	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
do	do	k7c2
rodu	rod	k1gInSc2
Markvarticů	Markvartiec	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
znakem	znak	k1gInSc7
byla	být	k5eAaImAgFnS
právě	právě	k9
lvice	lvice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Děčínský	děčínský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
vynikajícím	vynikající	k2eAgInSc7d1
zlatým	zlatý	k2eAgInSc7d1
latinským	latinský	k2eAgInSc7d1
křížem	kříž	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc1d1
symbol	symbol	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
patrocinium	patrocinium	k1gNnSc4
děkanského	děkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Děčíně	Děčín	k1gInSc6
a	a	k8xC
rovněž	rovněž	k9
na	na	k7c4
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c6
Šluknovsku	Šluknovsko	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
unikátní	unikátní	k2eAgInSc1d1
soubor	soubor	k1gInSc1
křížových	křížový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Krušnohorský	krušnohorský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Chomutovský	chomutovský	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
zlatou	zlatý	k2eAgFnSc7d1
lilií	lilie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Lilie	lilie	k1gFnSc1
jako	jako	k8xS,k8xC
symbol	symbol	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
množství	množství	k1gNnPc4
mariánských	mariánský	k2eAgNnPc2d1
poutních	poutní	k2eAgNnPc2d1
míst	místo	k1gNnPc2
ve	v	k7c6
vikariátu	vikariát	k1gInSc6
(	(	kIx(
<g/>
Květnov	Květnov	k1gInSc1
<g/>
,	,	kIx,
Horní	horní	k2eAgInSc1d1
Jiřetín	Jiřetín	k1gInSc1
<g/>
,	,	kIx,
Březno	Březno	k1gNnSc1
u	u	k7c2
Chomutova	Chomutov	k1gInSc2
<g/>
)	)	kIx)
i	i	k8xC
významných	významný	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
s	s	k7c7
tímto	tento	k3xDgNnSc7
zasvěcením	zasvěcení	k1gNnSc7
ve	v	k7c6
vikariátu	vikariát	k1gInSc6
(	(	kIx(
<g/>
Most	most	k1gInSc1
<g/>
,	,	kIx,
Chomutov	Chomutov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
kosmo	kosmo	k?
položenou	položený	k2eAgFnSc7d1
holí	hole	k1gFnSc7
svatého	svatý	k2eAgMnSc2d1
Antonína	Antonín	k1gMnSc2
šikmo	šikmo	k6eAd1
přeloženou	přeložený	k2eAgFnSc7d1
zlatou	zlatý	k2eAgFnSc7d1
berlou	berla	k1gFnSc7
se	s	k7c7
závitem	závit	k1gInSc7
doleva	doleva	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc4d1
symbol	symbol	k1gInSc4
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c6
sv.	sv.	kA
Antonína	Antonín	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
patrona	patrona	k1gFnSc1
libereckého	liberecký	k2eAgInSc2d1
arciděkanského	arciděkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Litoměřický	litoměřický	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
zlatým	zlatý	k2eAgInSc7d1
věncem	věnec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc4d1
symbol	symbol	k1gInSc4
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c6
sv.	sv.	kA
Štěpána	Štěpán	k1gMnSc4
prvomučedníka	prvomučedník	k1gMnSc4
<g/>
,	,	kIx,
patrona	patron	k1gMnSc4
katedrály	katedrála	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
stefanos	stefanos	k1gInSc1
je	být	k5eAaImIp3nS
řecky	řecky	k6eAd1
věnec	věnec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lounský	lounský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
zlatým	zlatý	k2eAgInSc7d1
přibývajícím	přibývající	k2eAgInSc7d1
půlměsícem	půlměsíc	k1gInSc7
provázeným	provázený	k2eAgInSc7d1
vpravo	vpravo	k6eAd1
zlatou	zlatý	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolené	zvolený	k2eAgInPc4d1
symboly	symbol	k1gInPc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
částečně	částečně	k6eAd1
znak	znak	k1gInSc4
lounského	lounský	k2eAgNnSc2d1
děkanství	děkanství	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
měsíc	měsíc	k1gInSc1
odkazem	odkaz	k1gInSc7
na	na	k7c4
město	město	k1gNnSc4
Louny	Louny	k1gInPc1
(	(	kIx(
<g/>
luna	luna	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
jsou	být	k5eAaImIp3nP
měsíc	měsíc	k1gInSc4
i	i	k9
hvězda	hvězda	k1gFnSc1
symboly	symbol	k1gInPc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
odkazující	odkazující	k2eAgNnSc1d1
na	na	k7c4
mariánská	mariánský	k2eAgNnPc4d1
poutní	poutní	k2eAgNnPc4d1
místa	místo	k1gNnPc4
ve	v	k7c6
vikariátu	vikariát	k1gInSc6
(	(	kIx(
<g/>
Liběšice	Liběšice	k1gFnSc1
u	u	k7c2
Žatce	Žatec	k1gInSc2
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInSc1d1
Ročov	Ročov	k1gInSc1
<g/>
)	)	kIx)
či	či	k8xC
patrocinium	patrocinium	k1gNnSc1
děkanského	děkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Žatci	Žatec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Mladoboleslavský	mladoboleslavský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
knížecí	knížecí	k2eAgFnSc2d1
korunou	koruna	k1gFnSc7
přirozených	přirozený	k2eAgFnPc2d1
tinktur	tinktura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc4d1
symbol	symbol	k1gInSc4
odkazuje	odkazovat	k5eAaImIp3nS
zejména	zejména	k9
na	na	k7c4
kněžnu	kněžna	k1gFnSc4
sv.	sv.	kA
Ludmilu	Ludmila	k1gFnSc4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
původ	původ	k1gInSc4
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
kladen	klást	k5eAaImNgInS
do	do	k7c2
Mělníka	Mělník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Teplický	teplický	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
hlavou	hlava	k1gFnSc7
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
položenou	položená	k1gFnSc4
na	na	k7c6
zlaté	zlatý	k2eAgFnSc6d1
míse	mísa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc1d1
symbol	symbol	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
patrocinium	patrocinium	k1gNnSc4
děkanského	děkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Teplicích	Teplice	k1gFnPc6
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
připomínkou	připomínka	k1gFnSc7
na	na	k7c4
uchovávání	uchovávání	k1gNnSc4
relikviáře	relikviář	k1gInSc2
s	s	k7c7
ostatky	ostatek	k1gInPc7
tohoto	tento	k3xDgMnSc2
světce	světec	k1gMnSc2
v	v	k7c6
oseckém	osecký	k2eAgInSc6d1
klášteře	klášter	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Turnovský	turnovský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
třemi	tři	k4xCgFnPc7
zlatými	zlatý	k2eAgFnPc7d1
mincemi	mince	k1gFnPc7
(	(	kIx(
<g/>
1	#num#	k4
<g/>
,	,	kIx,
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Zvolený	zvolený	k2eAgInSc1d1
symbol	symbol	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
svatomikulášské	svatomikulášský	k2eAgNnSc4d1
patrocinium	patrocinium	k1gNnSc4
děkanského	děkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
Turnově	Turnov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
<g/>
(	(	kIx(
<g/>
v	v	k7c6
katalogu	katalog	k1gInSc6
diecéze	diecéze	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
modrém	modrý	k2eAgInSc6d1
štítu	štít	k1gInSc6
stříbrně	stříbrně	k6eAd1
a	a	k8xC
červeně	červeně	k6eAd1
čtvrcený	čtvrcený	k2eAgInSc1d1
kříž	kříž	k1gInSc1
provázený	provázený	k2eAgInSc1d1
vpravo	vpravo	k6eAd1
nahoře	nahoře	k6eAd1
zlatou	zlatý	k2eAgFnSc7d1
pětilistou	pětilistý	k2eAgFnSc7d1
růží	růž	k1gFnSc7
se	s	k7c7
zlatým	zlatý	k2eAgInSc7d1
semeníkem	semeník	k1gInSc7
a	a	k8xC
zlatými	zlatý	k2eAgInPc7d1
okvětními	okvětní	k2eAgInPc7d1
lístky	lístek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
převýšeno	převýšen	k2eAgNnSc1d1
černým	černý	k2eAgInSc7d1
kněžským	kněžský	k2eAgInSc7d1
kloboukem	klobouk	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgInPc7
černými	černý	k2eAgInPc7d1
střapci	střapec	k1gInPc7
<g/>
.	.	kIx.
<g/>
Růže	růže	k1gFnSc1
jakožto	jakožto	k8xS
mariánský	mariánský	k2eAgInSc1d1
symbol	symbol	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
v	v	k7c6
prvé	prvý	k4xOgFnSc6
řadě	řada	k1gFnSc6
na	na	k7c4
patrocinium	patrocinium	k1gNnSc4
ústeckého	ústecký	k2eAgInSc2d1
arciděkanského	arciděkanský	k2eAgInSc2d1
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
připomínkou	připomínka	k1gFnSc7
dalšího	další	k2eAgInSc2d1
ústeckého	ústecký	k2eAgInSc2d1
kostela	kostel	k1gInSc2
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
růže	růže	k1gFnSc1
byla	být	k5eAaImAgFnS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
domnělý	domnělý	k2eAgInSc4d1
znak	znak	k1gInSc4
Slavníkovců	Slavníkovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Zaniklé	zaniklý	k2eAgInPc1d1
vikariáty	vikariát	k1gInPc1
</s>
<s>
Nymburský	nymburský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
WERNER	Werner	k1gMnSc1
<g/>
,	,	kIx,
Canisius	Canisius	k1gMnSc1
<g/>
;	;	kIx,
RAMSCH	RAMSCH	kA
<g/>
,	,	kIx,
Anton	Anton	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Almanach	almanach	k1gInSc1
des	des	k1gNnSc2
kath	katha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klerus	Klerus	k1gInSc1
Oesterreichs	Oesterreichs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Schematismus	schematismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vídeň	Vídeň	k1gFnSc1
<g/>
:	:	kIx,
Huber	Huber	k1gInSc1
&	&	k?
Lahme	Lahm	k1gMnSc5
<g/>
,	,	kIx,
1913	#num#	k4
<g/>
.	.	kIx.
552	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Personalstand	Personalstanda	k1gFnPc2
der	drát	k5eAaImRp2nS
Diözese	Diözesa	k1gFnSc3
Leitmeritz	Leitmeritz	k1gInSc1
1912	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
172	#num#	k4
<g/>
–	–	k?
<g/>
181	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
němčina	němčina	k1gFnSc1
<g/>
)	)	kIx)
↑	↑	k?
ZENGER	ZENGER	kA
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Maria	Maria	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
heraldika	heraldika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
.	.	kIx.
156	#num#	k4
s.	s.	k?
S.	S.	kA
128	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BAXANT	BAXANT	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
DAVÍDEK	Davídek	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrnice	směrnice	k1gFnSc1
o	o	k7c6
heraldických	heraldický	k2eAgInPc6d1
znacích	znak	k1gInPc6
církevněprávnických	církevněprávnický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
lit.	lit.	k?
diecéze	diecéze	k1gFnSc2
(	(	kIx(
<g/>
čj.	čj.	k?
3281	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ACEL	ACEL	kA
(	(	kIx(
<g/>
Acta	Actus	k1gMnSc2
Curiae	Curiae	k1gFnSc1
Episcopalis	Episcopalis	k1gFnSc1
Litomericensis	Litomericensis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Nymburský	nymburský	k2eAgInSc1d1
vikariát	vikariát	k1gInSc1
</s>
<s>
Seznam	seznam	k1gInSc1
farností	farnost	k1gFnPc2
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Web	web	k1gInSc1
litoměřické	litoměřický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
Celostátní	celostátní	k2eAgInSc1d1
seznam	seznam	k1gInSc1
vikariátů	vikariát	k1gInPc2
</s>
