<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodor	k1gMnSc1	Theodor
Dreyer	Dreyer	k1gMnSc1	Dreyer
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1889	[number]	k4	1889
Kodaň	Kodaň	k1gFnSc1	Kodaň
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
filmařů	filmař	k1gMnPc2	filmař
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
tvořil	tvořit	k5eAaImAgInS	tvořit
od	od	k7c2	od
desátých	desátá	k1gFnPc2	desátá
až	až	k6eAd1	až
do	do	k7c2	do
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
úzkostlivost	úzkostlivost	k1gFnSc4	úzkostlivost
<g/>
,	,	kIx,	,
pánovité	pánovitý	k2eAgInPc4d1	pánovitý
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
výstřední	výstřední	k2eAgFnPc4d1	výstřední
metody	metoda	k1gFnPc4	metoda
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
tvrdohlavé	tvrdohlavý	k2eAgNnSc4d1	tvrdohlavé
zaujetí	zaujetí	k1gNnSc4	zaujetí
vlastním	vlastní	k2eAgNnSc7d1	vlastní
uměním	umění	k1gNnSc7	umění
zapříčinily	zapříčinit	k5eAaPmAgFnP	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
až	až	k6eAd1	až
tolik	tolik	k6eAd1	tolik
nevzniklo	vzniknout	k5eNaPmAgNnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
však	však	k9	však
řada	řada	k1gFnSc1	řada
jeho	jeho	k3xOp3gInPc2	jeho
filmů	film	k1gInPc2	film
počítána	počítat	k5eAaImNgFnS	počítat
k	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
ze	z	k7c2	z
světového	světový	k2eAgNnSc2d1	světové
filmového	filmový	k2eAgNnSc2d1	filmové
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Dreyer	Dreyer	k1gMnSc1	Dreyer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
jako	jako	k8xS	jako
nemanželské	manželský	k2eNgNnSc4d1	nemanželské
dítě	dítě	k1gNnSc4	dítě
a	a	k8xC	a
již	již	k6eAd1	již
brzy	brzy	k6eAd1	brzy
osiřel	osiřet	k5eAaPmAgMnS	osiřet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
adoptivní	adoptivní	k2eAgMnPc1d1	adoptivní
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
přísní	přísný	k2eAgMnPc1d1	přísný
luteráni	luterán	k1gMnPc1	luterán
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
výchova	výchova	k1gFnSc1	výchova
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
témata	téma	k1gNnPc4	téma
mnoha	mnoho	k4c2	mnoho
pozdějších	pozdní	k2eAgInPc2d2	pozdější
Dreyerových	Dreyerův	k2eAgInPc2d1	Dreyerův
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
Dreyer	Dreyer	k1gMnSc1	Dreyer
živil	živit	k5eAaImAgMnS	živit
novinářskou	novinářský	k2eAgFnSc7d1	novinářská
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmu	film	k1gInSc3	film
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
přes	přes	k7c4	přes
titulky	titulek	k1gInPc4	titulek
němých	němý	k2eAgInPc2d1	němý
filmů	film	k1gInPc2	film
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
filmové	filmový	k2eAgInPc4d1	filmový
scénáře	scénář	k1gInPc4	scénář
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
produkční	produkční	k2eAgFnSc4d1	produkční
společnost	společnost	k1gFnSc4	společnost
Nordisk	Nordisko	k1gNnPc2	Nordisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
natočil	natočit	k5eAaBmAgMnS	natočit
svoji	svůj	k3xOyFgFnSc4	svůj
prvotinu	prvotina	k1gFnSc4	prvotina
Præ	Præ	k1gFnSc2	Præ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
Dreyerův	Dreyerův	k2eAgInSc1d1	Dreyerův
snímek	snímek	k1gInSc1	snímek
-	-	kIx~	-
Praestaenkan	Praestaenkan	k1gInSc1	Praestaenkan
-	-	kIx~	-
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
natočil	natočit	k5eAaBmAgMnS	natočit
Dreyer	Dreyer	k1gInSc4	Dreyer
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
Griffithovy	Griffithův	k2eAgFnSc2d1	Griffithova
Intolerance	intolerance	k1gFnSc2	intolerance
epizodický	epizodický	k2eAgInSc1d1	epizodický
snímek	snímek	k1gInSc1	snímek
Blade	Blad	k1gInSc5	Blad
af	af	k?	af
Satans	Satans	k1gInSc4	Satans
Bog	Bog	k1gFnSc2	Bog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
odešel	odejít	k5eAaPmAgMnS	odejít
Dreyer	Dreyer	k1gInSc4	Dreyer
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
pro	pro	k7c4	pro
společnost	společnost	k1gFnSc4	společnost
UFA	UFA	kA	UFA
a	a	k8xC	a
natočil	natočit	k5eAaBmAgInS	natočit
například	například	k6eAd1	například
výborný	výborný	k2eAgInSc4d1	výborný
snímek	snímek	k1gInSc4	snímek
Mikaël	Mikaëla	k1gFnPc2	Mikaëla
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zde	zde	k6eAd1	zde
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
klasický	klasický	k2eAgInSc4d1	klasický
snímek	snímek	k1gInSc4	snímek
-	-	kIx~	-
Utrpení	utrpení	k1gNnSc1	utrpení
Panny	Panna	k1gFnSc2	Panna
orleánské	orleánský	k2eAgFnSc2d1	Orleánská
<g/>
.	.	kIx.	.
</s>
<s>
Dreyer	Dreyer	k1gMnSc1	Dreyer
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
autentických	autentický	k2eAgInPc2d1	autentický
soudních	soudní	k2eAgInPc2d1	soudní
zápisů	zápis	k1gInPc2	zápis
a	a	k8xC	a
natočil	natočit	k5eAaBmAgMnS	natočit
výjimečně	výjimečně	k6eAd1	výjimečně
emotivní	emotivní	k2eAgNnSc4d1	emotivní
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
stejnou	stejný	k2eAgFnSc7d1	stejná
měrou	míra	k1gFnSc7wR	míra
ovlivněno	ovlivněn	k2eAgNnSc4d1	ovlivněno
realismem	realismus	k1gInSc7	realismus
a	a	k8xC	a
expresionismem	expresionismus	k1gInSc7	expresionismus
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
financí	finance	k1gFnPc2	finance
barona	baron	k1gMnSc2	baron
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
de	de	k?	de
Gunzburga	Gunzburga	k1gFnSc1	Gunzburga
mohl	moct	k5eAaImAgInS	moct
pak	pak	k6eAd1	pak
vytvořit	vytvořit	k5eAaPmF	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
další	další	k2eAgInSc4d1	další
proslavený	proslavený	k2eAgInSc4d1	proslavený
film	film	k1gInSc4	film
-	-	kIx~	-
Vampyr	Vampyr	k1gInSc4	Vampyr
-	-	kIx~	-
Der	drát	k5eAaImRp2nS	drát
Traum	Traum	k1gInSc1	Traum
des	des	k1gNnPc2	des
Allan	Allan	k1gMnSc1	Allan
Grey	Grea	k1gFnSc2	Grea
<g/>
,	,	kIx,	,
surreálnou	surreálný	k2eAgFnSc7d1	surreálná
podívanou	podívaná	k1gFnSc7	podívaná
strachu	strach	k1gInSc2	strach
<g/>
.	.	kIx.	.
</s>
<s>
Logika	logika	k1gFnSc1	logika
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
příběhu	příběh	k1gInSc6	příběh
muže	muž	k1gMnPc4	muž
chránícího	chránící	k2eAgNnSc2d1	chránící
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
před	před	k7c7	před
upírem	upír	k1gMnSc7	upír
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
stylu	styl	k1gInSc3	styl
a	a	k8xC	a
atmosféře	atmosféra	k1gFnSc3	atmosféra
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nezapomenutelné	zapomenutelný	k2eNgFnPc1d1	nezapomenutelná
scény	scéna	k1gFnPc1	scéna
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
hrdina	hrdina	k1gMnSc1	hrdina
(	(	kIx(	(
<g/>
hraný	hraný	k2eAgInSc1d1	hraný
Nicolasem	Nicolas	k1gInSc7	Nicolas
de	de	k?	de
Guznburgem	Guznburg	k1gInSc7	Guznburg
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
filmovým	filmový	k2eAgInSc7d1	filmový
pseudonymem	pseudonym	k1gInSc7	pseudonym
Julian	Julian	k1gMnSc1	Julian
West	West	k1gMnSc1	West
<g/>
)	)	kIx)	)
sní	sníst	k5eAaPmIp3nS	sníst
o	o	k7c6	o
vlastním	vlastní	k2eAgInSc6d1	vlastní
pohřbu	pohřeb	k1gInSc6	pohřeb
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
krvežíznivá	krvežíznivý	k2eAgFnSc1d1	krvežíznivá
hrdinova	hrdinův	k2eAgFnSc1d1	hrdinova
sestra	sestra	k1gFnSc1	sestra
nakažená	nakažený	k2eAgFnSc1d1	nakažená
upírem	upír	k1gMnSc7	upír
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
natáčen	natáčen	k2eAgInSc1d1	natáčen
jako	jako	k8xS	jako
němý	němý	k2eAgInSc1d1	němý
snímek	snímek	k1gInSc1	snímek
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
nadabované	nadabovaný	k2eAgInPc1d1	nadabovaný
dialogy	dialog	k1gInPc1	dialog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oba	dva	k4xCgInPc1	dva
filmy	film	k1gInPc1	film
byly	být	k5eAaImAgInP	být
komerčně	komerčně	k6eAd1	komerčně
nepříliš	příliš	k6eNd1	příliš
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
a	a	k8xC	a
Dreyer	Dreyer	k1gMnSc1	Dreyer
natočil	natočit	k5eAaBmAgMnS	natočit
vyjma	vyjma	k7c2	vyjma
jednoho	jeden	k4xCgInSc2	jeden
dokumentu	dokument	k1gInSc2	dokument
další	další	k2eAgInSc4d1	další
snímek	snímek	k1gInSc4	snímek
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Dánsko	Dánsko	k1gNnSc1	Dánsko
ovládáno	ovládán	k2eAgNnSc1d1	ovládáno
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Den	den	k1gInSc1	den
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
snímek	snímek	k1gInSc1	snímek
o	o	k7c6	o
vlivu	vliv	k1gInSc6	vliv
čarodějnických	čarodějnický	k2eAgInPc2d1	čarodějnický
procesů	proces	k1gInPc2	proces
na	na	k7c4	na
jejich	jejich	k3xOp3gMnPc4	jejich
účastníky	účastník	k1gMnPc4	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
snímkem	snímek	k1gInSc7	snímek
Dreyer	Dreyer	k1gMnSc1	Dreyer
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
svůj	svůj	k3xOyFgInSc4	svůj
budoucí	budoucí	k2eAgInSc4d1	budoucí
styl	styl	k1gInSc4	styl
mluvených	mluvený	k2eAgInPc2d1	mluvený
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
černobílou	černobílý	k2eAgFnSc7d1	černobílá
kamerou	kamera	k1gFnSc7	kamera
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
záběry	záběr	k1gInPc7	záběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
pak	pak	k6eAd1	pak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
Ordet	Ordet	k1gInSc1	Ordet
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
Kaje	kát	k5eAaImIp3nS	kát
Munka	Munka	k1gFnSc1	Munka
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
příběh	příběh	k1gInSc4	příběh
lásky	láska	k1gFnSc2	láska
na	na	k7c4	na
způsob	způsob	k1gInSc4	způsob
Romea	Romeo	k1gMnSc2	Romeo
a	a	k8xC	a
Julie	Julie	k1gFnSc2	Julie
s	s	k7c7	s
náročnou	náročný	k2eAgFnSc7d1	náročná
zkouškou	zkouška	k1gFnSc7	zkouška
víry	víra	k1gFnSc2	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
Dreyerovým	Dreyerův	k2eAgInSc7d1	Dreyerův
filmem	film	k1gInSc7	film
byla	být	k5eAaImAgFnS	být
Gertrud	Gertrud	k1gInSc4	Gertrud
podle	podle	k7c2	podle
hry	hra	k1gFnSc2	hra
Hjalmara	Hjalmara	k1gFnSc1	Hjalmara
Söderbergaz	Söderbergaz	k1gInSc1	Söderbergaz
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
snímek	snímek	k1gInSc1	snímek
o	o	k7c6	o
ženě	žena	k1gFnSc6	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přes	přes	k7c4	přes
všechny	všechen	k3xTgInPc4	všechen
své	svůj	k3xOyFgInPc4	svůj
neúspěchy	neúspěch	k1gInPc4	neúspěch
ničeho	nic	k3yNnSc2	nic
nelituje	litovat	k5eNaImIp3nS	litovat
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
méně	málo	k6eAd2	málo
významný	významný	k2eAgInSc4d1	významný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
každopádně	každopádně	k6eAd1	každopádně
tvoří	tvořit	k5eAaImIp3nP	tvořit
důležitou	důležitý	k2eAgFnSc4d1	důležitá
část	část	k1gFnSc4	část
Dreyerovy	Dreyerův	k2eAgFnSc2d1	Dreyerova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dreyerův	Dreyerův	k2eAgMnSc1d1	Dreyerův
největší	veliký	k2eAgMnSc1d3	veliký
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
nedokončený	dokončený	k2eNgInSc1d1	nedokončený
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
plánovaný	plánovaný	k2eAgInSc4d1	plánovaný
snímek	snímek	k1gInSc4	snímek
o	o	k7c6	o
Ježíši	Ježíš	k1gMnSc6	Ježíš
Kristovi	Kristus	k1gMnSc6	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Scénář	scénář	k1gInSc1	scénář
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
a	a	k8xC	a
roku	rok	k1gInSc3	rok
1968	[number]	k4	1968
i	i	k9	i
publikoval	publikovat	k5eAaBmAgMnS	publikovat
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
Dreyerovo	Dreyerův	k2eAgNnSc1d1	Dreyerovo
nejisté	jistý	k2eNgNnSc1d1	nejisté
finanční	finanční	k2eAgNnSc1d1	finanční
zázemí	zázemí	k1gNnSc1	zázemí
i	i	k8xC	i
realismus	realismus	k1gInSc1	realismus
nakonec	nakonec	k6eAd1	nakonec
natáčení	natáčení	k1gNnSc4	natáčení
nedovolily	dovolit	k5eNaPmAgFnP	dovolit
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgInSc1d1	jiný
Dreyerův	Dreyerův	k2eAgInSc1d1	Dreyerův
scénář	scénář	k1gInSc1	scénář
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
využit	využít	k5eAaPmNgInS	využít
a	a	k8xC	a
Lars	Lars	k1gInSc1	Lars
von	von	k1gInSc1	von
Trier	Trira	k1gFnPc2	Trira
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
natočil	natočit	k5eAaBmAgInS	natočit
snímek	snímek	k1gInSc4	snímek
Medea	Mede	k1gInSc2	Mede
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dreyer	Dreyer	k1gMnSc1	Dreyer
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Kodani	Kodaň	k1gFnSc6	Kodaň
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
79	[number]	k4	79
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obzvlášť	obzvlášť	k6eAd1	obzvlášť
cenným	cenný	k2eAgInSc7d1	cenný
příspěvkem	příspěvek	k1gInSc7	příspěvek
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
životnímu	životní	k2eAgNnSc3d1	životní
dílu	dílo	k1gNnSc3	dílo
je	být	k5eAaImIp3nS	být
dokument	dokument	k1gInSc1	dokument
Carl	Carl	k1gMnSc1	Carl
Th	Th	k1gMnSc1	Th
<g/>
.	.	kIx.	.
</s>
<s>
Dreyer	Dreyer	k1gMnSc1	Dreyer
<g/>
:	:	kIx,	:
Min	min	kA	min
metier	metier	k1gInSc1	metier
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Režijní	režijní	k2eAgFnSc2d1	režijní
filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
Præ	Præ	k?	Præ
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Praestaenkan	Praestaenkan	k1gInSc1	Praestaenkan
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Blade	Blade	k6eAd1	Blade
af	af	k?	af
Satans	Satans	k1gInSc1	Satans
Bog	Bog	k1gFnSc1	Bog
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Gezeichneten	Gezeichneten	k2eAgMnSc1d1	Gezeichneten
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
var	var	k1gInSc1	var
engang	engang	k1gInSc1	engang
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mikaël	Mikaël	k1gInSc1	Mikaël
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Du	Du	k?	Du
Skal	skála	k1gFnPc2	skála
Aere	Aere	k1gFnSc1	Aere
Din	din	k1gInSc1	din
Hustru	Hustr	k1gInSc2	Hustr
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Glomsdalsbruden	Glomsdalsbrudna	k1gFnPc2	Glomsdalsbrudna
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Utrpení	utrpení	k1gNnSc1	utrpení
Panny	Panna	k1gFnSc2	Panna
orleánské	orleánský	k2eAgFnSc2d1	Orleánská
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
Passion	Passion	k1gInSc1	Passion
de	de	k?	de
Jeanne	Jeann	k1gInSc5	Jeann
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arc	Arc	k1gMnSc1	Arc
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vampyr	Vampyr	k1gInSc4	Vampyr
-	-	kIx~	-
Der	drát	k5eAaImRp2nS	drát
Traum	Traum	k1gInSc1	Traum
des	des	k1gNnSc1	des
Allan	Allan	k1gMnSc1	Allan
Grey	Grea	k1gMnSc2	Grea
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mø	Mø	k?	Mø
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
hněvu	hněv	k1gInSc2	hněv
(	(	kIx(	(
<g/>
Vredens	Vredens	k1gInSc1	Vredens
dag	dag	kA	dag
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Två	Två	k?	Två
människor	människor	k1gInSc1	människor
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vandet	Vandet	k1gMnSc1	Vandet
på	på	k?	på
landet	landet	k1gMnSc1	landet
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Landsbykirken	Landsbykirken	k1gInSc1	Landsbykirken
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kampen	Kampen	k2eAgInSc1d1	Kampen
mod	mod	k?	mod
kræ	kræ	k?	kræ
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
De	De	k?	De
nå	nå	k?	nå
fæ	fæ	k?	fæ
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thorvaldsen	Thorvaldsen	k1gInSc1	Thorvaldsen
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Storstrø	Storstrø	k?	Storstrø
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
Ordet	Ordet	k1gInSc1	Ordet
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Et	Et	k?	Et
Slot	slot	k1gInSc1	slot
i	i	k8xC	i
et	et	k?	et
slot	slot	k1gInSc1	slot
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gertrud	Gertrud	k1gInSc1	Gertrud
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodora	k1gFnPc2	Theodora
Dreyer	Dreyra	k1gFnPc2	Dreyra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodor	k1gMnSc1	Theodor
Dreyer	Dreyer	k1gMnSc1	Dreyer
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodor	k1gMnSc1	Theodor
Dreyer	Dreyer	k1gMnSc1	Dreyer
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
