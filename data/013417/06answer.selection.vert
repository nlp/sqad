<s>
Carl	Carl	k1gMnSc1	Carl
Theodor	Theodor	k1gMnSc1	Theodor
Dreyer	Dreyer	k1gMnSc1	Dreyer
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1889	[number]	k4	1889
Kodaň	Kodaň	k1gFnSc1	Kodaň
–	–	k?	–
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
dánský	dánský	k2eAgMnSc1d1	dánský
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
filmařů	filmař	k1gMnPc2	filmař
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
