<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Dalibor	Dalibor	k1gMnSc1	Dalibor
Wünsch	Wünsch	k1gMnSc1	Wünsch
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1880	[number]	k4	1880
Zlonice	Zlonice	k1gFnSc2	Zlonice
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1955	[number]	k4	1955
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgInS	užívat
pseudonymy	pseudonym	k1gInPc4	pseudonym
Štěpán	Štěpána	k1gFnPc2	Štěpána
Hlína	hlína	k1gFnSc1	hlína
a	a	k8xC	a
Viktor	Viktor	k1gMnSc1	Viktor
Náchodský	náchodský	k2eAgMnSc1d1	náchodský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Wünsch	Wünsch	k1gMnSc1	Wünsch
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
skladatele	skladatel	k1gMnSc2	skladatel
Josefa	Josef	k1gMnSc2	Josef
Wünsche	Wünsch	k1gMnSc2	Wünsch
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc4d1	základní
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
tak	tak	k9	tak
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
od	od	k7c2	od
10	[number]	k4	10
let	léto	k1gNnPc2	léto
také	také	k9	také
na	na	k7c4	na
varhany	varhany	k1gInPc4	varhany
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
často	často	k6eAd1	často
měnil	měnit	k5eAaImAgMnS	měnit
své	svůj	k3xOyFgNnSc4	svůj
působiště	působiště	k1gNnSc4	působiště
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
(	(	kIx(	(
<g/>
Gospić	Gospić	k1gMnSc1	Gospić
<g/>
,	,	kIx,	,
Senj	Senj	k1gMnSc1	Senj
<g/>
)	)	kIx)	)
i	i	k9	i
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
Zásmuky	Zásmuky	k1gInPc1	Zásmuky
<g/>
,	,	kIx,	,
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
na	na	k7c4	na
varhanické	varhanický	k2eAgNnSc4d1	varhanické
oddělení	oddělení	k1gNnSc4	oddělení
Pražské	pražský	k2eAgFnSc2d1	Pražská
konzervatoře	konzervatoř	k1gFnSc2	konzervatoř
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studoval	studovat	k5eAaImAgMnS	studovat
u	u	k7c2	u
Josefa	Josef	k1gMnSc2	Josef
Kličky	klička	k1gFnSc2	klička
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Steckera	Stecker	k1gMnSc2	Stecker
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgMnSc1d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
složil	složit	k5eAaPmAgMnS	složit
ze	z	k7c2	z
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
varhan	varhany	k1gFnPc2	varhany
a	a	k8xC	a
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
hudební	hudební	k2eAgNnSc4d1	hudební
vzdělání	vzdělání	k1gNnSc4	vzdělání
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kurzech	kurz	k1gInPc6	kurz
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
Würzburgu	Würzburg	k1gInSc6	Würzburg
a	a	k8xC	a
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
rovněž	rovněž	k9	rovněž
externí	externí	k2eAgNnSc4d1	externí
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
Akademii	akademie	k1gFnSc6	akademie
pro	pro	k7c4	pro
církevní	církevní	k2eAgFnSc4d1	církevní
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
chorální	chorální	k2eAgInSc4d1	chorální
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
hudební	hudební	k2eAgFnSc6d1	hudební
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Supetaru	Supetar	k1gInSc6	Supetar
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Brač	Brač	k1gInSc1	Brač
v	v	k7c6	v
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
zde	zde	k6eAd1	zde
i	i	k9	i
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
kůru	kůr	k1gInSc2	kůr
a	a	k8xC	a
kapelník	kapelník	k1gMnSc1	kapelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1902	[number]	k4	1902
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
učil	učit	k5eAaImAgMnS	učit
v	v	k7c6	v
Náchodě	Náchod	k1gInSc6	Náchod
<g/>
,	,	kIx,	,
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Chyrově	Chyrův	k2eAgInSc6d1	Chyrův
a	a	k8xC	a
Tarnově	Tarnův	k2eAgInSc6d1	Tarnův
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
učitelském	učitelský	k2eAgInSc6d1	učitelský
ústavu	ústav	k1gInSc6	ústav
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
řídil	řídit	k5eAaImAgMnS	řídit
pěvecké	pěvecký	k2eAgNnSc4d1	pěvecké
sdružení	sdružení	k1gNnSc4	sdružení
Záboj	Záboj	k1gMnSc1	Záboj
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
dirigentem	dirigent	k1gMnSc7	dirigent
Pěveckého	pěvecký	k2eAgNnSc2d1	pěvecké
sdružení	sdružení	k1gNnSc2	sdružení
slezských	slezský	k2eAgMnPc2d1	slezský
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
veřejné	veřejný	k2eAgFnSc3d1	veřejná
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
varhanních	varhanní	k2eAgInPc2d1	varhanní
koncertů	koncert	k1gInPc2	koncert
měl	mít	k5eAaImAgInS	mít
popularizační	popularizační	k2eAgFnPc4d1	popularizační
přednášky	přednáška	k1gFnPc4	přednáška
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
<g/>
,	,	kIx,	,
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
konferencích	konference	k1gFnPc6	konference
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
řady	řada	k1gFnSc2	řada
komisí	komise	k1gFnPc2	komise
pro	pro	k7c4	pro
státní	státní	k2eAgFnPc4d1	státní
zkoušky	zkouška	k1gFnPc4	zkouška
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
učil	učit	k5eAaImAgMnS	učit
na	na	k7c6	na
konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
zejména	zejména	k9	zejména
hudební	hudební	k2eAgFnSc7d1	hudební
metodikou	metodika	k1gFnSc7	metodika
a	a	k8xC	a
didaktikou	didaktika	k1gFnSc7	didaktika
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
řadu	řada	k1gFnSc4	řada
odborných	odborný	k2eAgFnPc2d1	odborná
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
praktických	praktický	k2eAgFnPc2d1	praktická
příruček	příručka	k1gFnPc2	příručka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Repetitorium	repetitorium	k1gNnSc1	repetitorium
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
hudbě	hudba	k1gFnSc6	hudba
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Repetitorium	repetitorium	k1gNnSc1	repetitorium
nauky	nauka	k1gFnSc2	nauka
o	o	k7c4	o
sluchových	sluchový	k2eAgNnPc2d1	sluchové
a	a	k8xC	a
intonačních	intonační	k2eAgNnPc2d1	intonační
cvičení	cvičení	k1gNnPc2	cvičení
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Repetitorium	repetitorium	k1gNnSc1	repetitorium
teorie	teorie	k1gFnSc2	teorie
sborového	sborový	k2eAgInSc2d1	sborový
a	a	k8xC	a
školního	školní	k2eAgInSc2d1	školní
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Repetitorium	repetitorium	k1gNnSc1	repetitorium
nauky	nauka	k1gFnSc2	nauka
a	a	k8xC	a
hry	hra	k1gFnSc2	hra
houslové	houslový	k2eAgFnSc2d1	houslová
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
tabule	tabule	k1gFnSc1	tabule
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Katechismus	katechismus	k1gInSc1	katechismus
školského	školský	k2eAgInSc2d1	školský
zpěvu	zpěv	k1gInSc2	zpěv
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Základy	základ	k1gInPc1	základ
nauky	nauka	k1gFnSc2	nauka
o	o	k7c4	o
harmonii	harmonie	k1gFnSc4	harmonie
(	(	kIx(	(
<g/>
1937	[number]	k4	1937
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Základy	základ	k1gInPc1	základ
polyfonie	polyfonie	k1gFnSc2	polyfonie
(	(	kIx(	(
<g/>
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
rkp.	rkp.	k?	rkp.
<g/>
)	)	kIx)	)
<g/>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
tvoří	tvořit	k5eAaImIp3nP	tvořit
úpravy	úprava	k1gFnPc1	úprava
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
českých	český	k2eAgFnPc2d1	Česká
<g/>
,	,	kIx,	,
slezských	slezský	k2eAgFnPc2d1	Slezská
i	i	k8xC	i
chorvatských	chorvatský	k2eAgFnPc2d1	chorvatská
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Domovina	domovina	k1gFnSc1	domovina
zpívá	zpívat	k5eAaImIp3nS	zpívat
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Bezručova	Bezručův	k2eAgInSc2d1	Bezručův
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Slezské	slezský	k2eAgInPc1d1	slezský
nápěvy	nápěv	k1gInPc1	nápěv
</s>
</p>
<p>
<s>
Slovácké	slovácký	k2eAgInPc1d1	slovácký
nápěvy	nápěv	k1gInPc1	nápěv
</s>
</p>
<p>
<s>
České	český	k2eAgInPc1d1	český
nápěvy	nápěv	k1gInPc1	nápěv
</s>
</p>
<p>
<s>
Moravské	moravský	k2eAgInPc1d1	moravský
nápěvy	nápěv	k1gInPc1	nápěv
</s>
</p>
<p>
<s>
Lístoček	Lístočka	k1gFnPc2	Lístočka
zlatušký	zlatušký	k2eAgMnSc1d1	zlatušký
</s>
</p>
<p>
<s>
Naše	náš	k3xOp1gInPc1	náš
nápěvy	nápěv	k1gInPc1	nápěv
</s>
</p>
<p>
<s>
Zpoza	zpoza	k7c2	zpoza
Ostravice	Ostravice	k1gFnSc2	Ostravice
</s>
</p>
<p>
<s>
Slezské	slezský	k2eAgFnPc1d1	Slezská
pěsničky	pěsnička	k1gFnPc1	pěsnička
</s>
</p>
<p>
<s>
Lidové	lidový	k2eAgInPc1d1	lidový
zpěvy	zpěv	k1gInPc1	zpěv
slezské	slezský	k2eAgInPc1d1	slezský
</s>
</p>
<p>
<s>
Hrvatske	Hrvatske	k6eAd1	Hrvatske
pjesneKromě	pjesneKromě	k6eAd1	pjesneKromě
toho	ten	k3xDgMnSc4	ten
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
vlastních	vlastní	k2eAgFnPc2d1	vlastní
skladeb	skladba	k1gFnPc2	skladba
pro	pro	k7c4	pro
klavír	klavír	k1gInSc4	klavír
(	(	kIx(	(
<g/>
Naší	náš	k3xOp1gFnSc3	náš
mládeži	mládež	k1gFnSc3	mládež
<g/>
,	,	kIx,	,
Skočná	skočné	k1gNnPc1	skočné
<g/>
,	,	kIx,	,
Náčrty	náčrt	k1gInPc1	náčrt
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komponoval	komponovat	k5eAaImAgMnS	komponovat
sbory	sbor	k1gInPc4	sbor
(	(	kIx(	(
<g/>
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
balada	balada	k1gFnSc1	balada
<g/>
,	,	kIx,	,
Otče	otec	k1gMnSc5	otec
náš	náš	k3xOp1gMnSc1	náš
<g/>
,	,	kIx,	,
Ty	ty	k3xPp2nSc1	ty
skalický	skalický	k2eAgInSc1d1	skalický
kostelíčku	kostelíček	k1gInSc6	kostelíček
<g/>
,	,	kIx,	,
Ve	v	k7c4	v
Frydku	frydka	k1gFnSc4	frydka
na	na	k7c6	na
rynku	rynek	k1gInSc6	rynek
<g/>
)	)	kIx)	)
a	a	k8xC	a
příležitostné	příležitostný	k2eAgFnPc1d1	příležitostná
chrámové	chrámový	k2eAgFnPc1d1	chrámová
skladby	skladba	k1gFnPc1	skladba
(	(	kIx(	(
<g/>
2	[number]	k4	2
mše	mše	k1gFnSc2	mše
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
drobnější	drobný	k2eAgNnPc4d2	drobnější
díla	dílo	k1gNnPc4	dílo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Štěpán	Štěpán	k1gMnSc1	Štěpán
Hlína	hlína	k1gFnSc1	hlína
vydal	vydat	k5eAaPmAgInS	vydat
Vánoční	vánoční	k2eAgFnSc4d1	vánoční
mši	mše	k1gFnSc4	mše
koledovou	koledový	k2eAgFnSc4d1	Koledová
(	(	kIx(	(
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úpravy	úprava	k1gFnPc1	úprava
cizích	cizí	k2eAgFnPc2d1	cizí
skladeb	skladba	k1gFnPc2	skladba
vydával	vydávat	k5eAaImAgInS	vydávat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Viktor	Viktor	k1gMnSc1	Viktor
Náchodský	náchodský	k2eAgInSc1d1	náchodský
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rukopise	rukopis	k1gInSc6	rukopis
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
několik	několik	k4yIc1	několik
orchestrálních	orchestrální	k2eAgFnPc2d1	orchestrální
a	a	k8xC	a
komorních	komorní	k2eAgFnPc2d1	komorní
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
větších	veliký	k2eAgFnPc2d2	veliký
prací	práce	k1gFnPc2	práce
melodram	melodram	k1gInSc1	melodram
Koncert	koncert	k1gInSc1	koncert
Jankiela	Jankiel	k1gMnSc2	Jankiel
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
eposu	epos	k1gInSc2	epos
Adama	Adam	k1gMnSc2	Adam
Mickiewicze	Mickiewicze	k1gFnSc2	Mickiewicze
Pan	Pan	k1gMnSc1	Pan
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
čili	čili	k8xC	čili
poslední	poslední	k2eAgInSc1d1	poslední
nájezd	nájezd	k1gInSc1	nájezd
na	na	k7c6	na
Litvě	Litva	k1gFnSc6	Litva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Československý	československý	k2eAgInSc1d1	československý
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
–	–	k?	–
<g/>
Ž	Ž	kA	Ž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
hudební	hudební	k2eAgNnSc1d1	hudební
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Rudolf	Rudolf	k1gMnSc1	Rudolf
Wünsch	Wünsch	k1gMnSc1	Wünsch
(	(	kIx(	(
<g/>
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Díla	dílo	k1gNnPc1	dílo
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Wünsche	Wünsch	k1gMnSc2	Wünsch
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
zemské	zemský	k2eAgFnSc6d1	zemská
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
