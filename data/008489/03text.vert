<p>
<s>
Romaneto	romaneto	k1gNnSc1	romaneto
je	být	k5eAaImIp3nS	být
prozaický	prozaický	k2eAgInSc4d1	prozaický
epický	epický	k2eAgInSc4d1	epický
literární	literární	k2eAgInSc4d1	literární
žánr	žánr	k1gInSc4	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nevelké	velký	k2eNgNnSc1d1	nevelké
rozsahem	rozsah	k1gInSc7	rozsah
<g/>
,	,	kIx,	,
naplněného	naplněný	k2eAgInSc2d1	naplněný
dramatickým	dramatický	k2eAgInSc7d1	dramatický
dějem	děj	k1gInSc7	děj
směřující	směřující	k2eAgFnSc2d1	směřující
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
pointě	pointa	k1gFnSc3	pointa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
romaneta	romaneto	k1gNnSc2	romaneto
bývá	bývat	k5eAaImIp3nS	bývat
rozpor	rozpor	k1gInSc1	rozpor
mezi	mezi	k7c7	mezi
tajemnými	tajemný	k2eAgInPc7d1	tajemný
jevy	jev	k1gInPc7	jev
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
vědeckým	vědecký	k2eAgNnSc7d1	vědecké
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
literatuře	literatura	k1gFnSc6	literatura
tento	tento	k3xDgInSc1	tento
literární	literární	k2eAgInSc1d1	literární
útvar	útvar	k1gInSc1	útvar
zavedl	zavést	k5eAaPmAgMnS	zavést
Jakub	Jakub	k1gMnSc1	Jakub
Arbes	Arbes	k1gMnSc1	Arbes
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
názvu	název	k1gInSc2	název
je	být	k5eAaImIp3nS	být
však	však	k9	však
Jan	Jan	k1gMnSc1	Jan
Neruda	Neruda	k1gMnSc1	Neruda
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
romanetem	romaneto	k1gNnSc7	romaneto
byl	být	k5eAaImAgInS	být
Arbesův	Arbesův	k2eAgInSc1d1	Arbesův
Newtonův	Newtonův	k2eAgInSc1d1	Newtonův
mozek	mozek	k1gInSc1	mozek
<g/>
,	,	kIx,	,
uveřejněný	uveřejněný	k2eAgInSc1d1	uveřejněný
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Lumír	Lumír	k1gInSc1	Lumír
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
Ladislav	Ladislav	k1gMnSc1	Ladislav
Klíma	Klíma	k1gMnSc1	Klíma
(	(	kIx(	(
<g/>
romaneto	romaneto	k1gNnSc1	romaneto
Utrpení	utrpení	k1gNnSc2	utrpení
knížete	kníže	k1gMnSc2	kníže
Sternenhocha	Sternenhoch	k1gMnSc2	Sternenhoch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
už	už	k6eAd1	už
podobný	podobný	k2eAgInSc1d1	podobný
styl	styl	k1gInSc1	styl
existoval	existovat	k5eAaImAgInS	existovat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
povídky	povídka	k1gFnPc1	povídka
s	s	k7c7	s
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Romaneto	romaneto	k1gNnSc1	romaneto
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
napínavým	napínavý	k2eAgInSc7d1	napínavý
dějem	děj	k1gInSc7	děj
s	s	k7c7	s
fantastickými	fantastický	k2eAgInPc7d1	fantastický
či	či	k8xC	či
detektivními	detektivní	k2eAgInPc7d1	detektivní
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nepochopitelná	pochopitelný	k2eNgFnSc1d1	nepochopitelná
záhada	záhada	k1gFnSc1	záhada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
racionálně	racionálně	k6eAd1	racionálně
vysvětlena	vysvětlen	k2eAgFnSc1d1	vysvětlena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Romaneto	romaneto	k1gNnSc1	romaneto
je	být	k5eAaImIp3nS	být
próza	próza	k1gFnSc1	próza
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
románu	román	k1gInSc2	román
a	a	k8xC	a
novely	novela	k1gFnSc2	novela
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
pro	pro	k7c4	pro
novelu	novela	k1gFnSc4	novela
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
tendence	tendence	k1gFnSc1	tendence
k	k	k7c3	k
pointovanosti	pointovanost	k1gFnSc3	pointovanost
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
počet	počet	k1gInSc1	počet
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
vypravěč	vypravěč	k1gMnSc1	vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
román	román	k1gInSc4	román
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
romaneto	romaneto	k1gNnSc1	romaneto
obsáhlé	obsáhlý	k2eAgFnSc2d1	obsáhlá
faktografické	faktografický	k2eAgFnSc2d1	faktografická
<g/>
,	,	kIx,	,
úvahové	úvahový	k2eAgFnSc2d1	úvahová
a	a	k8xC	a
vysvětlující	vysvětlující	k2eAgFnSc2d1	vysvětlující
pasáže	pasáž	k1gFnSc2	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
Podstatné	podstatný	k2eAgInPc1d1	podstatný
jsou	být	k5eAaImIp3nP	být
protiklady	protiklad	k1gInPc1	protiklad
dokumentárnosti	dokumentárnost	k1gFnSc2	dokumentárnost
a	a	k8xC	a
fikce	fikce	k1gFnSc2	fikce
<g/>
;	;	kIx,	;
konkrétnost	konkrétnost	k1gFnSc1	konkrétnost
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
času	čas	k1gInSc2	čas
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
fantastický	fantastický	k2eAgInSc4d1	fantastický
děj	děj	k1gInSc4	děj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
výraz	výraz	k1gInSc1	výraz
romaneto	romaneto	k1gNnSc1	romaneto
či	či	k8xC	či
romanetto	romanetto	k1gNnSc1	romanetto
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
nepravou	pravý	k2eNgFnSc4d1	nepravá
výpůjčku	výpůjčka	k1gFnSc4	výpůjčka
<g/>
.	.	kIx.	.
</s>
<s>
Rejzkův	Rejzkův	k2eAgInSc1d1	Rejzkův
etymologický	etymologický	k2eAgInSc1d1	etymologický
slovník	slovník	k1gInSc1	slovník
(	(	kIx(	(
<g/>
Rejzek	Rejzek	k1gInSc1	Rejzek
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
jej	on	k3xPp3gNnSc4	on
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
hypotetického	hypotetický	k2eAgNnSc2d1	hypotetické
italského	italský	k2eAgNnSc2d1	italské
deminutiva	deminutivum	k1gNnSc2	deminutivum
romanetto	romanett	k2eAgNnSc1d1	romanett
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
malý	malý	k2eAgInSc1d1	malý
román	román	k1gInSc1	román
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
a	a	k8xC	a
lexikalizovaným	lexikalizovaný	k2eAgNnSc7d1	lexikalizované
deminutivem	deminutivum	k1gNnSc7	deminutivum
od	od	k7c2	od
it.	it.	k?	it.
romanzo	romanza	k1gFnSc5	romanza
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
román	román	k1gInSc4	román
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
však	však	k9	však
it.	it.	k?	it.
romanzetto	romanzetto	k1gNnSc1	romanzetto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
it.	it.	k?	it.
výraz	výraz	k1gInSc1	výraz
romanetto	romanetto	k1gNnSc1	romanetto
se	se	k3xPyFc4	se
sporadicky	sporadicky	k6eAd1	sporadicky
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
deminutivum	deminutivum	k1gNnSc1	deminutivum
od	od	k7c2	od
it.	it.	k?	it.
romano	romana	k1gFnSc5	romana
'	'	kIx"	'
<g/>
Říman	Říman	k1gMnSc1	Říman
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
