<p>
<s>
Pojem	pojem	k1gInSc1	pojem
kardinál	kardinál	k1gMnSc1	kardinál
(	(	kIx(	(
<g/>
cardo	cardo	k1gNnSc1	cardo
–	–	k?	–
stěžej	stěžej	k1gFnSc1	stěžej
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
otáčejí	otáčet	k5eAaImIp3nP	otáčet
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
narážka	narážka	k1gFnSc1	narážka
na	na	k7c4	na
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
)	)	kIx)	)
původně	původně	k6eAd1	původně
znamenal	znamenat	k5eAaImAgInS	znamenat
hlavního	hlavní	k2eAgMnSc4d1	hlavní
faráře	farář	k1gMnSc4	farář
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
označovali	označovat	k5eAaImAgMnP	označovat
kněží	kněz	k1gMnPc1	kněz
25	[number]	k4	25
římských	římský	k2eAgInPc2d1	římský
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
příslušníka	příslušník	k1gMnSc4	příslušník
sboru	sbor	k1gInSc2	sbor
(	(	kIx(	(
<g/>
kolegia	kolegium	k1gNnSc2	kolegium
<g/>
)	)	kIx)	)
kardinálů	kardinál	k1gMnPc2	kardinál
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
sacrum	sacrum	k1gInSc1	sacrum
collegium	collegium	k1gNnSc1	collegium
<g/>
)	)	kIx)	)
–	–	k?	–
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
hodnostářů	hodnostář	k1gMnPc2	hodnostář
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
jmenovaných	jmenovaná	k1gFnPc2	jmenovaná
(	(	kIx(	(
<g/>
kreovaných	kreovaný	k2eAgFnPc2d1	kreovaný
<g/>
)	)	kIx)	)
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úkoly	úkol	k1gInPc7	úkol
kardinálů	kardinál	k1gMnPc2	kardinál
==	==	k?	==
</s>
</p>
<p>
<s>
Kardinálové	kardinál	k1gMnPc1	kardinál
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
poradní	poradní	k2eAgInSc1d1	poradní
sbor	sbor	k1gInSc1	sbor
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgMnPc1d2	mladší
80	[number]	k4	80
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
limit	limit	k1gInSc1	limit
stanovil	stanovit	k5eAaPmAgMnS	stanovit
papež	papež	k1gMnSc1	papež
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
konkláve	konkláve	k1gNnSc3	konkláve
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
papeže	papež	k1gMnSc4	papež
volí	volit	k5eAaImIp3nS	volit
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
je	být	k5eAaImIp3nS	být
kardinály	kardinál	k1gMnPc7	kardinál
volena	volit	k5eAaImNgFnS	volit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1179	[number]	k4	1179
se	se	k3xPyFc4	se
volby	volba	k1gFnSc2	volba
účastnili	účastnit	k5eAaImAgMnP	účastnit
i	i	k9	i
další	další	k2eAgMnPc1d1	další
biskupové	biskup	k1gMnPc1	biskup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
Jana	Jan	k1gMnSc2	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosud	dosud	k6eAd1	dosud
nebyl	být	k5eNaImAgMnS	být
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
obdrží	obdržet	k5eAaPmIp3nS	obdržet
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
jmenování	jmenování	k1gNnSc6	jmenování
biskupské	biskupský	k2eAgNnSc1d1	Biskupské
svěcení	svěcení	k1gNnSc1	svěcení
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k9	ale
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nově	nově	k6eAd1	nově
jmenování	jmenování	k1gNnSc4	jmenování
kardinálové	kardinál	k1gMnPc1	kardinál
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
dispens	dispens	k1gFnSc4	dispens
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
povinnosti	povinnost	k1gFnSc2	povinnost
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jim	on	k3xPp3gMnPc3	on
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
zemřelý	zemřelý	k2eAgMnSc1d1	zemřelý
kardinál	kardinál	k1gMnSc1	kardinál
Špidlík	Špidlík	k1gMnSc1	Špidlík
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
konzistoři	konzistoř	k1gFnSc6	konzistoř
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
byli	být	k5eAaImAgMnP	být
bez	bez	k7c2	bez
biskupského	biskupský	k2eAgInSc2d1	biskupský
svěcení	svěcený	k2eAgMnPc1d1	svěcený
tito	tento	k3xDgMnPc1	tento
kardinálové	kardinál	k1gMnPc1	kardinál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Domenico	Domenico	k1gMnSc1	Domenico
kardinál	kardinál	k1gMnSc1	kardinál
Bartolucci	Bartolucce	k1gFnSc4	Bartolucce
</s>
</p>
<p>
<s>
Roberto	Roberta	k1gFnSc5	Roberta
kardinál	kardinál	k1gMnSc1	kardinál
Tucci	Tucce	k1gFnSc3	Tucce
SJ	SJ	kA	SJ
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
kardinál	kardinál	k1gMnSc1	kardinál
Vanhoye	Vanhoye	k1gFnSc3	Vanhoye
SJ	SJ	kA	SJ
</s>
</p>
<p>
<s>
==	==	k?	==
Znaky	znak	k1gInPc1	znak
kardinálské	kardinálský	k2eAgFnSc2d1	kardinálská
hodnosti	hodnost	k1gFnSc2	hodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Znakem	znak	k1gInSc7	znak
kardinála	kardinál	k1gMnSc2	kardinál
je	být	k5eAaImIp3nS	být
jasně	jasně	k6eAd1	jasně
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
–	–	k?	–
nosí	nosit	k5eAaImIp3nS	nosit
červeně	červeně	k6eAd1	červeně
lemovanou	lemovaný	k2eAgFnSc4d1	lemovaná
kleriku	klerika	k1gFnSc4	klerika
<g/>
,	,	kIx,	,
červené	červený	k2eAgNnSc4d1	červené
cingulum	cingulum	k1gNnSc4	cingulum
a	a	k8xC	a
solideo	solideo	k1gNnSc4	solideo
<g/>
.	.	kIx.	.
</s>
<s>
Slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
oděv	oděv	k1gInSc1	oděv
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
jasně	jasně	k6eAd1	jasně
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
bílé	bílý	k2eAgFnSc2d1	bílá
rochety	rocheta	k1gFnSc2	rocheta
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
bílé	bílý	k2eAgFnPc4d1	bílá
mitry	mitra	k1gFnPc4	mitra
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jeho	jeho	k3xOp3gInSc2	jeho
znaku	znak	k1gInSc2	znak
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
kardinálský	kardinálský	k2eAgInSc1d1	kardinálský
klobouk	klobouk	k1gInSc1	klobouk
s	s	k7c7	s
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
15	[number]	k4	15
střapci	střapec	k1gInPc7	střapec
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálovi	kardinál	k1gMnSc3	kardinál
přísluší	příslušet	k5eAaImIp3nS	příslušet
oslovení	oslovení	k1gNnSc4	oslovení
Vaše	váš	k3xOp2gFnSc1	váš
Eminence	eminence	k1gFnSc1	eminence
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
jmenování	jmenování	k1gNnSc6	jmenování
dostane	dostat	k5eAaPmIp3nS	dostat
kardinálský	kardinálský	k2eAgInSc4d1	kardinálský
biret	biret	k1gInSc4	biret
<g/>
,	,	kIx,	,
jmenovací	jmenovací	k2eAgInSc4d1	jmenovací
dekret	dekret	k1gInSc4	dekret
a	a	k8xC	a
kardinálský	kardinálský	k2eAgInSc4d1	kardinálský
prsten	prsten	k1gInSc4	prsten
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Diplomatický	diplomatický	k2eAgInSc1d1	diplomatický
protokol	protokol	k1gInSc1	protokol
přisuzuje	přisuzovat	k5eAaImIp3nS	přisuzovat
kardinálovi	kardinál	k1gMnSc3	kardinál
pocty	pocta	k1gFnSc2	pocta
náležející	náležející	k2eAgFnPc1d1	náležející
zástupci	zástupce	k1gMnPc7	zástupce
vladaře	vladař	k1gMnSc2	vladař
a	a	k8xC	a
následníkovi	následník	k1gMnSc3	následník
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hierarchie	hierarchie	k1gFnPc1	hierarchie
kardinálů	kardinál	k1gMnPc2	kardinál
==	==	k?	==
</s>
</p>
<p>
<s>
Kardinál-jáhen	Kardináláhen	k2eAgMnSc1d1	Kardinál-jáhen
–	–	k?	–
kardinál	kardinál	k1gMnSc1	kardinál
mající	mající	k2eAgInSc4d1	mající
titul	titul	k1gInSc4	titul
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
14	[number]	k4	14
římských	římský	k2eAgFnPc2d1	římská
chudinských	chudinský	k2eAgFnPc2d1	chudinská
kurií	kurie	k1gFnPc2	kurie
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kardinál-protojáhen	kardinálrotojáhen	k1gInSc1	kardinál-protojáhen
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	jíst	k5eAaImIp1nS	jíst
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
kardinál	kardinál	k1gMnSc1	kardinál
Renato	Renata	k1gFnSc5	Renata
Raffaele	Raffael	k1gInPc4	Raffael
Martino	Martin	k2eAgNnSc1d1	Martino
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
konkláve	konkláve	k1gNnSc2	konkláve
oznámit	oznámit	k5eAaPmF	oznámit
shromážděnému	shromážděný	k2eAgNnSc3d1	shromážděné
lidu	lido	k1gNnSc3	lido
z	z	k7c2	z
lodžie	lodžie	k1gFnSc2	lodžie
Baziliky	bazilika	k1gFnSc2	bazilika
sv.	sv.	kA	sv.
<g/>
Petra	Petr	k1gMnSc2	Petr
jméno	jméno	k1gNnSc1	jméno
nově	nově	k6eAd1	nově
zvoleného	zvolený	k2eAgMnSc4d1	zvolený
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
vyhrazeno	vyhrazen	k2eAgNnSc1d1	vyhrazeno
právo	právo	k1gNnSc4	právo
vložit	vložit	k5eAaPmF	vložit
na	na	k7c4	na
ramena	rameno	k1gNnPc4	rameno
nově	nově	k6eAd1	nově
zvolenému	zvolený	k2eAgMnSc3d1	zvolený
papeži	papež	k1gMnSc3	papež
při	při	k7c6	při
inaugurační	inaugurační	k2eAgFnSc6d1	inaugurační
mši	mše	k1gFnSc6	mše
pallium	pallium	k1gNnSc1	pallium
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c4	v
zastoupení	zastoupení	k1gNnSc4	zastoupení
papeže	papež	k1gMnSc2	papež
předávat	předávat	k5eAaImF	předávat
pallium	pallium	k1gNnSc4	pallium
nově	nově	k6eAd1	nově
jmenovaným	jmenovaný	k2eAgMnPc3d1	jmenovaný
arcibiskupům	arcibiskup	k1gMnPc3	arcibiskup
-	-	kIx~	-
metropolitům	metropolita	k1gMnPc3	metropolita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kardinál-kněz	Kardinálněz	k1gMnSc1	Kardinál-kněz
–	–	k?	–
kardinál	kardinál	k1gMnSc1	kardinál
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
vztahujícím	vztahující	k2eAgInSc7d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
římských	římský	k2eAgFnPc2d1	římská
bazilik	bazilika	k1gFnPc2	bazilika
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
kardinál-protopresbyter	kardinálrotopresbyter	k1gInSc1	kardinál-protopresbyter
(	(	kIx(	(
<g/>
první	první	k4xOgMnSc1	první
kněz	kněz	k1gMnSc1	kněz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	jíst	k5eAaImIp1nS	jíst
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
kardinál	kardinál	k1gMnSc1	kardinál
Michael	Michael	k1gMnSc1	Michael
Michai	Micha	k1gMnSc3	Micha
Kitbunchu	Kitbunch	k1gMnSc3	Kitbunch
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
přednést	přednést	k5eAaPmF	přednést
modlitbu	modlitba	k1gFnSc4	modlitba
za	za	k7c4	za
papeže	papež	k1gMnSc4	papež
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
kardinál-protodiakon	kardinálrotodiakon	k1gMnSc1	kardinál-protodiakon
mu	on	k3xPp3gMnSc3	on
vložil	vložit	k5eAaPmAgMnS	vložit
pallium	pallium	k1gNnSc4	pallium
na	na	k7c4	na
ramena	rameno	k1gNnPc4	rameno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kardinál-biskup	Kardináliskup	k1gMnSc1	Kardinál-biskup
–	–	k?	–
kardinál	kardinál	k1gMnSc1	kardinál
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
podle	podle	k7c2	podle
některé	některý	k3yIgFnSc2	některý
diecéze	diecéze	k1gFnSc2	diecéze
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
6	[number]	k4	6
<g/>
,	,	kIx,	,
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
voleni	volen	k2eAgMnPc1d1	volen
děkan	děkan	k1gMnSc1	děkan
a	a	k8xC	a
viceděkan	viceděkan	k1gMnSc1	viceděkan
posvátného	posvátný	k2eAgNnSc2d1	posvátné
kolegia	kolegium	k1gNnSc2	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Děkan	děkan	k1gMnSc1	děkan
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
viceděkan	viceděkana	k1gFnPc2	viceděkana
nemají	mít	k5eNaImIp3nP	mít
žádnou	žádný	k3yNgFnSc4	žádný
řídící	řídící	k2eAgFnSc4d1	řídící
pravomoc	pravomoc	k1gFnSc4	pravomoc
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgInSc1	první
mezi	mezi	k7c7	mezi
rovnými	rovný	k2eAgFnPc7d1	rovná
(	(	kIx(	(
<g/>
Primus	primus	k1gMnSc1	primus
inter	inter	k1gMnSc1	inter
pares	paresa	k1gFnPc2	paresa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
děkanem	děkan	k1gMnSc7	děkan
sboru	sbor	k1gInSc2	sbor
kardinálů	kardinál	k1gMnPc2	kardinál
je	být	k5eAaImIp3nS	být
Angelo	Angela	k1gFnSc5	Angela
kardinál	kardinál	k1gMnSc1	kardinál
Sodano	Sodana	k1gFnSc5	Sodana
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
svěřeno	svěřen	k2eAgNnSc4d1	svěřeno
vedení	vedení	k1gNnSc4	vedení
kardinálského	kardinálský	k2eAgNnSc2d1	kardinálské
kolegia	kolegium	k1gNnSc2	kolegium
při	při	k7c6	při
konzistořích	konzistoř	k1gFnPc6	konzistoř
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
předsedá	předsedat	k5eAaImIp3nS	předsedat
pohřbu	pohřeb	k1gInSc2	pohřeb
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
konkláve	konkláve	k1gNnSc1	konkláve
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
předává	předávat	k5eAaImIp3nS	předávat
nově	nově	k6eAd1	nově
zvolenému	zvolený	k2eAgMnSc3d1	zvolený
papeži	papež	k1gMnSc3	papež
při	při	k7c6	při
inaugurační	inaugurační	k2eAgFnSc6d1	inaugurační
mši	mše	k1gFnSc6	mše
<g/>
,	,	kIx,	,
po	po	k7c6	po
modlitbě	modlitba	k1gFnSc6	modlitba
kardinála	kardinál	k1gMnSc2	kardinál
protopresbytera	protopresbyter	k1gMnSc2	protopresbyter
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rybářský	rybářský	k2eAgInSc1d1	rybářský
prsten	prsten	k1gInSc1	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
Angelo	Angela	k1gFnSc5	Angela
Sodano	Sodana	k1gFnSc5	Sodana
kardinálem	kardinál	k1gMnSc7	kardinál
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
nejstarším	starý	k2eAgMnSc7d3	nejstarší
žijícím	žijící	k2eAgMnSc7d1	žijící
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
,	,	kIx,	,
po	po	k7c6	po
emeritním	emeritní	k2eAgMnSc6d1	emeritní
papeži	papež	k1gMnSc6	papež
Benediktu	benedikt	k1gInSc2	benedikt
XVI	XVI	kA	XVI
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
kreován	kreovat	k5eAaBmNgMnS	kreovat
papežem	papež	k1gMnSc7	papež
sv.	sv.	kA	sv.
<g/>
Pavlem	Pavel	k1gMnSc7	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kardinál	kardinál	k1gMnSc1	kardinál
Roger	Roger	k1gMnSc1	Roger
Marie	Marie	k1gFnSc1	Marie
Élie	Éli	k1gMnSc2	Éli
Etchegaray	Etchegaraa	k1gMnSc2	Etchegaraa
<g/>
,	,	kIx,	,
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
papežem	papež	k1gMnSc7	papež
sv.	sv.	kA	sv.
<g/>
Janem	Jan	k1gMnSc7	Jan
Pavlem	Pavel	k1gMnSc7	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál-děkan	Kardinálěkan	k1gMnSc1	Kardinál-děkan
Angelo	Angela	k1gFnSc5	Angela
Sodano	Sodana	k1gFnSc5	Sodana
je	on	k3xPp3gMnPc4	on
sice	sice	k8xC	sice
služebně	služebně	k6eAd1	služebně
nejstarším	starý	k2eAgInSc7d3	nejstarší
kardinálem-biskupem	kardinálemiskup	k1gInSc7	kardinálem-biskup
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byl	být	k5eAaImAgMnS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
sboru	sbor	k1gInSc2	sbor
kardinálů-biskupů	kardinálůiskup	k1gMnPc2	kardinálů-biskup
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc7	rok
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
kardinál	kardinál	k1gMnSc1	kardinál
Etchegaray	Etchegaraa	k1gFnSc2	Etchegaraa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
služebně	služebně	k6eAd1	služebně
nejstarším	starý	k2eAgMnSc7d3	nejstarší
kardinálem	kardinál	k1gMnSc7	kardinál
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
emeritního	emeritní	k2eAgMnSc4d1	emeritní
papeže	papež	k1gMnSc4	papež
Benedikta	Benedikt	k1gMnSc2	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
kardinál	kardinál	k1gMnSc1	kardinál
Etchegaray	Etchegaraa	k1gFnSc2	Etchegaraa
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gMnSc3	on
přísluší	příslušet	k5eAaImIp3nS	příslušet
titul	titul	k1gInSc1	titul
kardinál-protoepiscopus	kardinálrotoepiscopus	k1gInSc1	kardinál-protoepiscopus
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
biskup	biskup	k1gInSc1	biskup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
kardinálského	kardinálský	k2eAgInSc2d1	kardinálský
sboru	sbor	k1gInSc2	sbor
automaticky	automaticky	k6eAd1	automaticky
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
východní	východní	k2eAgMnPc1d1	východní
patriarchové	patriarcha	k1gMnPc1	patriarcha
<g/>
.	.	kIx.	.
<g/>
Výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
kolegia	kolegium	k1gNnSc2	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
mezi	mezi	k7c4	mezi
zasedáními	zasedání	k1gNnPc7	zasedání
generální	generální	k2eAgFnSc1d1	generální
kongregace	kongregace	k1gFnSc1	kongregace
kolegia	kolegium	k1gNnSc2	kolegium
v	v	k7c6	v
době	doba	k1gFnSc6	doba
papežské	papežský	k2eAgFnSc2d1	Papežská
sedisvakance	sedisvakance	k1gFnSc2	sedisvakance
je	být	k5eAaImIp3nS	být
čtyřčlenná	čtyřčlenný	k2eAgFnSc1d1	čtyřčlenná
mimořádná	mimořádný	k2eAgFnSc1d1	mimořádná
kongregace	kongregace	k1gFnSc1	kongregace
kardinálského	kardinálský	k2eAgNnSc2d1	kardinálské
kolegia	kolegium	k1gNnSc2	kolegium
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gMnPc7	její
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
kardinál-komorník	kardinálomorník	k1gMnSc1	kardinál-komorník
svaté	svatý	k2eAgFnSc2d1	svatá
římské	římský	k2eAgFnSc2d1	římská
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
kardinálové-asistenti	kardinálovésistent	k1gMnPc1	kardinálové-asistent
losovaní	losovaný	k2eAgMnPc1d1	losovaný
generální	generální	k2eAgFnSc3d1	generální
kongregací	kongregace	k1gFnPc2	kongregace
z	z	k7c2	z
kardinálů	kardinál	k1gMnPc2	kardinál
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
volit	volit	k5eAaImF	volit
papeže	papež	k1gMnSc4	papež
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
období	období	k1gNnSc6	období
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
typy	typ	k1gInPc1	typ
a	a	k8xC	a
členění	členění	k1gNnSc1	členění
kardinálů	kardinál	k1gMnPc2	kardinál
==	==	k?	==
</s>
</p>
<p>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
kuriální	kuriální	k2eAgMnSc1d1	kuriální
je	být	k5eAaImIp3nS	být
kardinál	kardinál	k1gMnSc1	kardinál
se	s	k7c7	s
sídelní	sídelní	k2eAgFnSc7d1	sídelní
povinností	povinnost	k1gFnSc7	povinnost
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
vede	vést	k5eAaImIp3nS	vést
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
vatikánských	vatikánský	k2eAgNnPc2d1	Vatikánské
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
kurie	kurie	k1gFnSc2	kurie
(	(	kIx(	(
<g/>
dikastérií	dikastérie	k1gFnPc2	dikastérie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
extrakuriální	extrakuriální	k2eAgFnSc4d1	extrakuriální
sídelní	sídelní	k2eAgFnSc4d1	sídelní
povinnost	povinnost	k1gFnSc4	povinnost
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
<g/>
Kardinál	kardinál	k1gMnSc1	kardinál
in	in	k?	in
pectore	pector	k1gMnSc5	pector
je	on	k3xPp3gFnPc4	on
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
komu	kdo	k3yInSc3	kdo
papež	papež	k1gMnSc1	papež
udělil	udělit	k5eAaPmAgMnS	udělit
hodnost	hodnost	k1gFnSc4	hodnost
kardinála	kardinál	k1gMnSc2	kardinál
tajně	tajně	k6eAd1	tajně
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
in	in	k?	in
pectore	pector	k1gMnSc5	pector
–	–	k?	–
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
<g/>
)	)	kIx)	)
a	a	k8xC	a
zveřejnění	zveřejnění	k1gNnSc1	zveřejnění
tohoto	tento	k3xDgInSc2	tento
aktu	akt	k1gInSc2	akt
si	se	k3xPyFc3	se
nechal	nechat	k5eAaPmAgMnS	nechat
na	na	k7c4	na
vhodnější	vhodný	k2eAgFnSc4d2	vhodnější
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
používáno	používán	k2eAgNnSc1d1	používáno
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
církev	církev	k1gFnSc1	církev
pronásledována	pronásledován	k2eAgFnSc1d1	pronásledována
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k9	tak
předešlo	předejít	k5eAaPmAgNnS	předejít
politickým	politický	k2eAgInPc3d1	politický
střetům	střet	k1gInPc3	střet
a	a	k8xC	a
perzekucím	perzekuce	k1gFnPc3	perzekuce
proti	proti	k7c3	proti
církvi	církev	k1gFnSc3	církev
nebo	nebo	k8xC	nebo
těmto	tento	k3xDgFnPc3	tento
osobám	osoba	k1gFnPc3	osoba
<g/>
.	.	kIx.	.
<g/>
Papabile	Papabila	k1gFnSc3	Papabila
je	být	k5eAaImIp3nS	být
neoficiální	neoficiální	k2eAgNnSc1d1	neoficiální
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kardinála	kardinál	k1gMnSc4	kardinál
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
šanci	šance	k1gFnSc4	šance
být	být	k5eAaImF	být
při	při	k7c6	při
konkláve	konkláve	k1gNnSc6	konkláve
zvolen	zvolit	k5eAaPmNgMnS	zvolit
papežem	papež	k1gMnSc7	papež
<g/>
.	.	kIx.	.
<g/>
Kardinál	kardinál	k1gMnSc1	kardinál
laik	laik	k1gMnSc1	laik
byl	být	k5eAaImAgMnS	být
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nepřijal	přijmout	k5eNaPmAgMnS	přijmout
vyšší	vysoký	k2eAgNnSc4d2	vyšší
kněžské	kněžský	k2eAgNnSc4d1	kněžské
svěcení	svěcení	k1gNnSc4	svěcení
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
vysvěcen	vysvětit	k5eAaPmNgMnS	vysvětit
na	na	k7c4	na
jáhna	jáhen	k1gMnSc4	jáhen
<g/>
,	,	kIx,	,
kněze	kněz	k1gMnSc2	kněz
nebo	nebo	k8xC	nebo
biskupa	biskup	k1gMnSc2	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
poslední	poslední	k2eAgMnSc1d1	poslední
laik	laik	k1gMnSc1	laik
jmenovaný	jmenovaný	k2eAgMnSc1d1	jmenovaný
kardinálem	kardinál	k1gMnSc7	kardinál
byl	být	k5eAaImAgMnS	být
kreován	kreovat	k5eAaBmNgMnS	kreovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1858	[number]	k4	1858
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
kardinálů	kardinál	k1gMnPc2	kardinál
==	==	k?	==
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Seznamy	seznam	k1gInPc4	seznam
kardinálůSbor	kardinálůSbor	k1gInSc1	kardinálůSbor
kardinálů	kardinál	k1gMnPc2	kardinál
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
kardinály	kardinál	k1gMnPc4	kardinál
jmenováni	jmenovat	k5eAaImNgMnP	jmenovat
i	i	k9	i
duchovní	duchovní	k1gMnPc1	duchovní
sídlící	sídlící	k2eAgMnPc1d1	sídlící
mimo	mimo	k7c4	mimo
Řím	Řím	k1gInSc4	Řím
<g/>
,	,	kIx,	,
do	do	k7c2	do
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
pohyboval	pohybovat	k5eAaImAgInS	pohybovat
okolo	okolo	k7c2	okolo
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Sixtus	Sixtus	k1gMnSc1	Sixtus
V.	V.	kA	V.
stanovil	stanovit	k5eAaPmAgMnS	stanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1586	[number]	k4	1586
<g/>
,	,	kIx,	,
že	že	k8xS	že
kardinálů	kardinál	k1gMnPc2	kardinál
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
XXIII	XXIII	kA	XXIII
<g/>
.	.	kIx.	.
tuto	tento	k3xDgFnSc4	tento
hranici	hranice	k1gFnSc4	hranice
zrušil	zrušit	k5eAaPmAgMnS	zrušit
(	(	kIx(	(
<g/>
r.	r.	kA	r.
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pavel	Pavel	k1gMnSc1	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
posléze	posléze	k6eAd1	posléze
stanovil	stanovit	k5eAaPmAgInS	stanovit
novou	nova	k1gFnSc7	nova
–	–	k?	–
120	[number]	k4	120
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
však	však	k8xC	však
novou	nový	k2eAgFnSc4d1	nová
hranici	hranice	k1gFnSc4	hranice
zrušil	zrušit	k5eAaPmAgInS	zrušit
rovněž	rovněž	k9	rovněž
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
počet	počet	k1gInSc1	počet
kardinálů	kardinál	k1gMnPc2	kardinál
na	na	k7c4	na
193	[number]	k4	193
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
není	být	k5eNaImIp3nS	být
stanovena	stanoven	k2eAgFnSc1d1	stanovena
žádná	žádný	k3yNgFnSc1	žádný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
smrti	smrt	k1gFnSc2	smrt
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
183	[number]	k4	183
žijících	žijící	k2eAgMnPc2d1	žijící
kardinálů	kardinál	k1gMnPc2	kardinál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2009	[number]	k4	2009
mělo	mít	k5eAaImAgNnS	mít
kolegium	kolegium	k1gNnSc1	kolegium
kardinálů	kardinál	k1gMnPc2	kardinál
185	[number]	k4	185
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
112	[number]	k4	112
mladších	mladý	k2eAgInPc2d2	mladší
80	[number]	k4	80
let	léto	k1gNnPc2	léto
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
volit	volit	k5eAaImF	volit
papeže	papež	k1gMnSc4	papež
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
témuž	týž	k3xTgNnSc3	týž
datu	datum	k1gNnSc3	datum
byli	být	k5eAaImAgMnP	být
kardinály	kardinál	k1gMnPc4	kardinál
dva	dva	k4xCgMnPc1	dva
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
mladší	mladý	k2eAgMnPc1d2	mladší
80	[number]	k4	80
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
kardinál	kardinál	k1gMnSc1	kardinál
Vlk	Vlk	k1gMnSc1	Vlk
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
starší	starý	k2eAgInSc1d2	starší
80	[number]	k4	80
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
kardinál	kardinál	k1gMnSc1	kardinál
Špidlík	Špidlík	k1gMnSc1	Špidlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
Čech	Čech	k1gMnSc1	Čech
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
-	-	kIx~	-
Dominik	Dominik	k1gMnSc1	Dominik
Duka	Duka	k1gMnSc1	Duka
(	(	kIx(	(
<g/>
dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2012	[number]	k4	2012
jej	on	k3xPp3gInSc4	on
při	při	k7c6	při
papežské	papežský	k2eAgFnSc6d1	Papežská
konzistoři	konzistoř	k1gFnSc6	konzistoř
konané	konaný	k2eAgFnSc6d1	konaná
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kardinál	kardinál	k1gMnSc1	kardinál
státní	státní	k2eAgMnSc1d1	státní
sekretář	sekretář	k1gMnSc1	sekretář
</s>
</p>
<p>
<s>
Konkláve	konkláve	k1gNnSc1	konkláve
</s>
</p>
<p>
<s>
Suburbikální	Suburbikální	k2eAgFnSc1d1	Suburbikální
diecéze	diecéze	k1gFnSc1	diecéze
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
žijících	žijící	k2eAgMnPc2d1	žijící
kardinálů	kardinál	k1gMnPc2	kardinál
</s>
</p>
<p>
<s>
Camerlengo	Camerlengo	k6eAd1	Camerlengo
</s>
</p>
<p>
<s>
Kardinál-protojáhen	Kardinálrotojáhen	k1gInSc1	Kardinál-protojáhen
</s>
</p>
<p>
<s>
Solideo	solideo	k1gNnSc1	solideo
</s>
</p>
<p>
<s>
Biret	biret	k1gInSc1	biret
</s>
</p>
<p>
<s>
Ornát	ornát	k1gMnSc1	ornát
</s>
</p>
<p>
<s>
Infule	infule	k1gFnSc1	infule
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kardinál	kardinál	k1gMnSc1	kardinál
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Cardinals	Cardinals	k1gInSc1	Cardinals
of	of	k?	of
the	the	k?	the
Holy	hola	k1gFnSc2	hola
Roman	Roman	k1gMnSc1	Roman
Church	Church	k1gMnSc1	Church
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
kardinálech	kardinál	k1gMnPc6	kardinál
od	od	k7c2	od
roku	rok	k1gInSc2	rok
112	[number]	k4	112
až	až	k9	až
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
tajné	tajný	k2eAgFnSc2d1	tajná
konzistoře	konzistoř	k1gFnSc2	konzistoř
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
bude	být	k5eAaImBp3nS	být
znovu	znovu	k6eAd1	znovu
rozdávat	rozdávat	k5eAaImF	rozdávat
kardinálské	kardinálský	k2eAgInPc4d1	kardinálský
birety	biret	k1gInPc4	biret
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc1	radio
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc1	zpráva
20	[number]	k4	20
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
jmenovitý	jmenovitý	k2eAgMnSc1d1	jmenovitý
autor	autor	k1gMnSc1	autor
neuveden	uveden	k2eNgMnSc1d1	neuveden
</s>
</p>
<p>
<s>
The	The	k?	The
College	College	k1gInSc1	College
of	of	k?	of
Cardinals	Cardinals	k1gInSc1	Cardinals
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgInSc1d1	oficiální
vatikánský	vatikánský	k2eAgInSc1d1	vatikánský
seznam	seznam	k1gInSc1	seznam
kardinálů	kardinál	k1gMnPc2	kardinál
</s>
</p>
