<s>
Luzzu	Luzza	k1gFnSc4
</s>
<s>
Luzzu	Luzza	k1gFnSc4
v	v	k7c6
přístavu	přístav	k1gInSc6
Marsaxlokk	Marsaxlokka	k1gFnPc2
</s>
<s>
Luzzu	Luzza	k1gFnSc4
je	být	k5eAaImIp3nS
název	název	k1gInSc1
tradičního	tradiční	k2eAgInSc2d1
maltského	maltský	k2eAgInSc2d1
člunu	člun	k1gInSc2
používaného	používaný	k2eAgInSc2d1
zejména	zejména	k9
na	na	k7c4
rybolov	rybolov	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velká	k1gFnSc2
množství	množství	k1gNnSc2
těchto	tento	k3xDgInPc2
člunů	člun	k1gInPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
rybářském	rybářský	k2eAgInSc6d1
přístavu	přístav	k1gInSc6
Marsaxlokk	Marsaxlokk	k1gInSc1
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
ostrova	ostrov	k1gInSc2
<g/>
;	;	kIx,
podobné	podobný	k2eAgInPc1d1
čluny	člun	k1gInPc1
<g/>
,	,	kIx,
poněkud	poněkud	k6eAd1
více	hodně	k6eAd2
připomínající	připomínající	k2eAgFnSc2d1
benátské	benátský	k2eAgFnSc2d1
gondoly	gondola	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
v	v	k7c6
přístavě	přístav	k1gInSc6
Grand	grand	k1gMnSc1
Harbour	Harbour	k1gMnSc1
ve	v	k7c6
Vallettě	Valletta	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
jim	on	k3xPp3gMnPc3
říká	říkat	k5eAaImIp3nS
dhajsa	dhajsa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Čluny	člun	k1gInPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
pestře	pestro	k6eAd1
pomalovány	pomalován	k2eAgInPc1d1
žlutou	žlutý	k2eAgFnSc4d1
<g/>
,	,	kIx,
červenou	červený	k2eAgFnSc4d1
<g/>
,	,	kIx,
zelenou	zelený	k2eAgFnSc4d1
a	a	k8xC
modrou	modrý	k2eAgFnSc4d1
<g/>
,	,	kIx,
na	na	k7c6
přídi	příď	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
pár	pár	k4xCyI
očí	oko	k1gNnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
přisuzovány	přisuzovat	k5eAaImNgFnP
egyptskému	egyptský	k2eAgMnSc3d1
bohu	bůh	k1gMnSc3
Usirovi	Usir	k1gMnSc3
(	(	kIx(
<g/>
v	v	k7c6
některých	některý	k3yIgInPc6
případech	případ	k1gInPc6
i	i	k8xC
bohu	bůh	k1gMnSc6
Horovi	Hora	k1gMnSc6
<g/>
)	)	kIx)
a	a	k8xC
má	mít	k5eAaImIp3nS
rybáře	rybář	k1gMnSc4
chránit	chránit	k5eAaImF
před	před	k7c7
pohromami	pohroma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
původ	původ	k1gInSc4
není	být	k5eNaImIp3nS
s	s	k7c7
určitostí	určitost	k1gFnSc7
prokázán	prokázán	k2eAgInSc4d1
<g/>
;	;	kIx,
v	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
obyčej	obyčej	k1gInSc4
starých	starý	k2eAgMnPc2d1
Féničanů	Féničan	k1gMnPc2
<g/>
,	,	kIx,
resp.	resp.	kA
i	i	k8xC
Řeků	Řek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vzhled	vzhled	k1gInSc1
člunu	člun	k1gInSc2
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
starý	starý	k2eAgInSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
navazovat	navazovat	k5eAaImF
přinejmenším	přinejmenším	k6eAd1
na	na	k7c4
loďky	loďka	k1gFnPc4
z	z	k7c2
dob	doba	k1gFnPc2
fénické	fénický	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
čluny	člun	k1gInPc1
beze	beze	k7c2
změn	změna	k1gFnPc2
přetrvaly	přetrvat	k5eAaPmAgInP
až	až	k9
do	do	k7c2
dnešních	dnešní	k2eAgFnPc2d1
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vysvětlována	vysvětlovat	k5eAaImNgFnS
skutečností	skutečnost	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
velmi	velmi	k6eAd1
stabilní	stabilní	k2eAgFnSc4d1
konstrukci	konstrukce	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
odolávala	odolávat	k5eAaImAgFnS
i	i	k9
silným	silný	k2eAgFnPc3d1
bouřím	bouř	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čluny	člun	k1gInPc4
byly	být	k5eAaImAgInP
původně	původně	k6eAd1
vybaveny	vybavit	k5eAaPmNgInP
plachtou	plachta	k1gFnSc7
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
motorizované	motorizovaný	k2eAgInPc4d1
čluny	člun	k1gInPc4
<g/>
.	.	kIx.
</s>
