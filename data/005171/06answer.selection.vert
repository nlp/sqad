<s>
Venuše	Venuše	k1gFnSc1	Venuše
má	mít	k5eAaImIp3nS	mít
nejhustší	hustý	k2eAgFnSc4d3	nejhustší
atmosféru	atmosféra	k1gFnSc4	atmosféra
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
