<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
jen	jen	k9	jen
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
Strachkvas	Strachkvas	k1gMnSc1	Strachkvas
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
prý	prý	k9	prý
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
den	den	k1gInSc4	den
smrti	smrt	k1gFnSc2	smrt
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zřejmě	zřejmě	k6eAd1	zřejmě
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
935	[number]	k4	935
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ještě	ještě	k9	ještě
929	[number]	k4	929
<g/>
.	.	kIx.	.
</s>
