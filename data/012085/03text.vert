<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Pobožný	pobožný	k2eAgMnSc1d1	pobožný
(	(	kIx(	(
<g/>
932	[number]	k4	932
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
999	[number]	k4	999
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
967	[number]	k4	967
<g/>
/	/	kIx~	/
<g/>
972	[number]	k4	972
<g/>
–	–	k?	–
<g/>
999	[number]	k4	999
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kronikáře	kronikář	k1gMnSc2	kronikář
Kosmy	Kosma	k1gMnSc2	Kosma
"	"	kIx"	"
<g/>
muž	muž	k1gMnSc1	muž
nejkřesťanštější	křesťanský	k2eAgMnSc1d3	nejkřesťanštější
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
sirotků	sirotek	k1gMnPc2	sirotek
<g/>
,	,	kIx,	,
ochránce	ochránce	k1gMnSc1	ochránce
vdov	vdova	k1gFnPc2	vdova
<g/>
,	,	kIx,	,
utěšitel	utěšitel	k1gMnSc1	utěšitel
zarmoucených	zarmoucený	k2eAgFnPc2d1	zarmoucená
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
Boleslava	Boleslav	k1gMnSc2	Boleslav
I.	I.	kA	I.
Ukrutného	ukrutný	k2eAgInSc2d1	ukrutný
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
choti	choť	k1gFnSc2	choť
Biagoty	Biagota	k1gFnSc2	Biagota
<g/>
,	,	kIx,	,
bratr	bratr	k1gMnSc1	bratr
Strachkvase	Strachkvasa	k1gFnSc3	Strachkvasa
<g/>
,	,	kIx,	,
Doubravky	Doubravka	k1gFnPc1	Doubravka
a	a	k8xC	a
Mlady	Mlady	k?	Mlady
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
jen	jen	k9	jen
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
starší	starý	k2eAgMnSc1d2	starší
než	než	k8xS	než
Strachkvas	Strachkvas	k1gMnSc1	Strachkvas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
prý	prý	k9	prý
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
den	den	k1gInSc4	den
smrti	smrt	k1gFnSc2	smrt
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zřejmě	zřejmě	k6eAd1	zřejmě
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
935	[number]	k4	935
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ještě	ještě	k9	ještě
929	[number]	k4	929
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
<g/>
,	,	kIx,	,
że	że	k?	że
Boleslav	Boleslav	k1gMnSc1	Boleslav
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
935	[number]	k4	935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
udává	udávat	k5eAaImIp3nS	udávat
kronikář	kronikář	k1gMnSc1	kronikář
Widukind	Widukind	k1gMnSc1	Widukind
z	z	k7c2	z
Corvey	Corvea	k1gFnSc2	Corvea
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
950	[number]	k4	950
<g/>
,	,	kIx,	,
když	když	k8xS	když
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
Veliký	veliký	k2eAgInSc1d1	veliký
chystal	chystat	k5eAaImAgMnS	chystat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
výpravu	výprava	k1gFnSc4	výprava
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
Boleslav	Boleslav	k1gMnSc1	Boleslav
I.	I.	kA	I.
se	s	k7c7	s
synem	syn	k1gMnSc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tím	ten	k3xDgMnSc7	ten
synem	syn	k1gMnSc7	syn
nutně	nutně	k6eAd1	nutně
byl	být	k5eAaImAgMnS	být
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
vlády	vláda	k1gFnSc2	vláda
tvoří	tvořit	k5eAaImIp3nS	tvořit
území	území	k1gNnPc1	území
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
Čechy	Čechy	k1gFnPc4	Čechy
vyjma	vyjma	k7c4	vyjma
Chebska	Chebsko	k1gNnPc4	Chebsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
přemyslovského	přemyslovský	k2eAgInSc2d1	přemyslovský
státu	stát	k1gInSc2	stát
patří	patřit	k5eAaImIp3nS	patřit
rovněž	rovněž	k9	rovněž
Olomoucko	Olomoucko	k1gNnSc1	Olomoucko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
Krakovsko	Krakovsko	k1gNnSc1	Krakovsko
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
Červené	Červené	k2eAgInPc1d1	Červené
hrady	hrad	k1gInPc1	hrad
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
Červená	červený	k2eAgFnSc1d1	červená
Rus	Rus	k1gFnSc1	Rus
<g/>
)	)	kIx)	)
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
výspy	výspa	k1gFnSc2	výspa
panství	panství	k1gNnSc2	panství
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
až	až	k9	až
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Bug	Bug	k1gFnSc2	Bug
<g/>
.	.	kIx.	.
</s>
<s>
Boleslavská	boleslavský	k2eAgFnSc1d1	Boleslavská
říše	říše	k1gFnSc1	říše
tedy	tedy	k9	tedy
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
rozsáhlejší	rozsáhlý	k2eAgFnSc3d2	rozsáhlejší
než	než	k8xS	než
dnešní	dnešní	k2eAgFnSc2d1	dnešní
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
však	však	k9	však
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
nevládli	vládnout	k5eNaImAgMnP	vládnout
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
východního	východní	k2eAgNnSc2d1	východní
směřování	směřování	k1gNnSc2	směřování
expanze	expanze	k1gFnSc1	expanze
bylo	být	k5eAaImAgNnS	být
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
obchodní	obchodní	k2eAgFnSc2d1	obchodní
stezky	stezka	k1gFnSc2	stezka
mezi	mezi	k7c7	mezi
Norimberkem	Norimberk	k1gInSc7	Norimberk
a	a	k8xC	a
Kyjevem	Kyjev	k1gInSc7	Kyjev
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
kontroly	kontrola	k1gFnSc2	kontrola
plynuly	plynout	k5eAaImAgInP	plynout
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
knížete	kníže	k1gMnSc4	kníže
velké	velká	k1gFnSc2	velká
zisky	zisk	k1gInPc4	zisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
973	[number]	k4	973
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
pražské	pražský	k2eAgNnSc1d1	Pražské
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
podřízeno	podřízen	k2eAgNnSc1d1	podřízeno
arcibiskupství	arcibiskupství	k1gNnSc1	arcibiskupství
v	v	k7c6	v
Mohuči	Mohuč	k1gFnSc6	Mohuč
<g/>
.	.	kIx.	.
</s>
<s>
Pražské	pražský	k2eAgNnSc4d1	Pražské
biskupství	biskupství	k1gNnSc4	biskupství
pro	pro	k7c4	pro
českého	český	k2eAgMnSc4d1	český
panovníka	panovník	k1gMnSc4	panovník
znamenalo	znamenat	k5eAaImAgNnS	znamenat
větší	veliký	k2eAgFnSc4d2	veliký
váhu	váha	k1gFnSc4	váha
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
politickém	politický	k2eAgNnSc6d1	politické
poli	pole	k1gNnSc6	pole
a	a	k8xC	a
také	také	k9	také
větší	veliký	k2eAgFnSc4d2	veliký
politickou	politický	k2eAgFnSc4d1	politická
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
církevní	církevní	k2eAgMnSc1d1	církevní
<g/>
)	)	kIx)	)
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
biskupem	biskup	k1gMnSc7	biskup
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
saský	saský	k2eAgMnSc1d1	saský
mnich	mnich	k1gMnSc1	mnich
Dětmar	Dětmar	k1gMnSc1	Dětmar
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
982	[number]	k4	982
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Slavníkovců	Slavníkovec	k1gInPc2	Slavníkovec
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
neustálé	neustálý	k2eAgFnPc1d1	neustálá
spory	spora	k1gFnPc1	spora
biskupa	biskup	k1gMnSc2	biskup
s	s	k7c7	s
knížecím	knížecí	k2eAgInSc7d1	knížecí
dvorem	dvůr	k1gInSc7	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
992	[number]	k4	992
nicméně	nicméně	k8xC	nicméně
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
přijal	přijmout	k5eAaPmAgMnS	přijmout
od	od	k7c2	od
knížete	kníže	k1gMnSc2	kníže
edikt	edikt	k1gInSc4	edikt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mu	on	k3xPp3gMnSc3	on
povoloval	povolovat	k5eAaImAgInS	povolovat
rozlučovat	rozlučovat	k5eAaImF	rozlučovat
manželství	manželství	k1gNnPc4	manželství
mezi	mezi	k7c7	mezi
příbuznými	příbuzný	k1gMnPc7	příbuzný
<g/>
,	,	kIx,	,
stavět	stavět	k5eAaImF	stavět
kostely	kostel	k1gInPc4	kostel
a	a	k8xC	a
vybírat	vybírat	k5eAaImF	vybírat
desátky	desátka	k1gFnPc4	desátka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Procírkevní	Procírkevní	k2eAgFnSc1d1	Procírkevní
politika	politika	k1gFnSc1	politika
Boleslava	Boleslav	k1gMnSc2	Boleslav
i	i	k8xC	i
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
vedla	vést	k5eAaImAgFnS	vést
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
prvního	první	k4xOgInSc2	první
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
-	-	kIx~	-
u	u	k7c2	u
Sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc1	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
abatyší	abatyše	k1gFnSc7	abatyše
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhá	druhý	k4xOgFnSc1	druhý
sestra	sestra	k1gFnSc1	sestra
knížete	kníže	k1gNnSc4wR	kníže
Mlada	Mlad	k1gMnSc2	Mlad
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
cestě	cesta	k1gFnSc3	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
osobní	osobní	k2eAgFnSc6d1	osobní
přímluvě	přímluva	k1gFnSc6	přímluva
u	u	k7c2	u
papeže	papež	k1gMnSc2	papež
Jana	Jan	k1gMnSc2	Jan
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
přímo	přímo	k6eAd1	přímo
zasadila	zasadit	k5eAaPmAgFnS	zasadit
o	o	k7c4	o
vznik	vznik	k1gInSc4	vznik
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
993	[number]	k4	993
také	také	k6eAd1	také
založen	založit	k5eAaPmNgInS	založit
první	první	k4xOgInSc1	první
mužský	mužský	k2eAgInSc1d1	mužský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
-	-	kIx~	-
benediktinský	benediktinský	k2eAgInSc1d1	benediktinský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Břevnově	Břevnov	k1gInSc6	Břevnov
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
založil	založit	k5eAaPmAgMnS	založit
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
ještě	ještě	k9	ještě
klášter	klášter	k1gInSc1	klášter
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
na	na	k7c6	na
Ostrově	ostrov	k1gInSc6	ostrov
u	u	k7c2	u
Davle	Davle	k1gFnSc2	Davle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
mocným	mocný	k2eAgMnSc7d1	mocný
středoevropským	středoevropský	k2eAgMnSc7d1	středoevropský
panovníkem	panovník	k1gMnSc7	panovník
a	a	k8xC	a
dlouho	dlouho	k6eAd1	dlouho
udržoval	udržovat	k5eAaImAgInS	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
polským	polský	k2eAgMnSc7d1	polský
knížetem	kníže	k1gMnSc7	kníže
Měškem	Měšek	k1gMnSc7	Měšek
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
jeho	jeho	k3xOp3gFnSc4	jeho
sestru	sestra	k1gFnSc4	sestra
Doubravku	Doubravka	k1gFnSc4	Doubravka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
ale	ale	k9	ale
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
zhoršily	zhoršit	k5eAaPmAgInP	zhoršit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
990	[number]	k4	990
byly	být	k5eAaImAgInP	být
boje	boj	k1gInPc1	boj
s	s	k7c7	s
Měškem	Měšek	k1gInSc7	Měšek
I.	I.	kA	I.
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
smírem	smír	k1gInSc7	smír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Boleslava	Boleslav	k1gMnSc4	Boleslav
Chrabrého	chrabrý	k2eAgMnSc2d1	chrabrý
na	na	k7c4	na
polský	polský	k2eAgInSc4d1	polský
trůn	trůn	k1gInSc4	trůn
se	se	k3xPyFc4	se
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
vzdal	vzdát	k5eAaPmAgMnS	vzdát
Slezska	Slezsko	k1gNnSc2	Slezsko
a	a	k8xC	a
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
981	[number]	k4	981
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
územní	územní	k2eAgFnPc4d1	územní
ztráty	ztráta	k1gFnPc4	ztráta
v	v	k7c6	v
Podněstří	Podněstří	k1gFnSc6	Podněstří
(	(	kIx(	(
<g/>
Přemyšl	Přemyšl	k1gInSc1	Přemyšl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýchodnější	východní	k2eAgNnSc4d3	nejvýchodnější
území	území	k1gNnSc4	území
pak	pak	k6eAd1	pak
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
ztratil	ztratit	k5eAaPmAgMnS	ztratit
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Velikého	veliký	k2eAgMnSc2d1	veliký
na	na	k7c4	na
ruský	ruský	k2eAgInSc4d1	ruský
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
973-974	[number]	k4	973-974
-	-	kIx~	-
Boleslav	Boleslav	k1gMnSc1	Boleslav
podpořil	podpořit	k5eAaPmAgMnS	podpořit
bavorského	bavorský	k2eAgMnSc4d1	bavorský
vévodu	vévoda	k1gMnSc4	vévoda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Svárlivého	svárlivý	k2eAgInSc2d1	svárlivý
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
Otovi	Ota	k1gMnSc3	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
o	o	k7c4	o
následnictví	následnictví	k1gNnSc4	následnictví
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
Otu	Ota	k1gMnSc4	Ota
neuznal	uznat	k5eNaPmAgMnS	uznat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
polský	polský	k2eAgMnSc1d1	polský
kníže	kníže	k1gMnSc1	kníže
Měšek	Měšek	k1gMnSc1	Měšek
I.	I.	kA	I.
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
975-978	[number]	k4	975-978
-	-	kIx~	-
další	další	k2eAgInPc1d1	další
boje	boj	k1gInPc1	boj
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Otou	Ota	k1gMnSc7	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vpády	vpád	k1gInPc1	vpád
na	na	k7c6	na
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
usmířili	usmířit	k5eAaPmAgMnP	usmířit
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
984	[number]	k4	984
-	-	kIx~	-
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Oty	Ota	k1gMnSc2	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
podpora	podpora	k1gFnSc1	podpora
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Svárlivého	svárlivý	k2eAgNnSc2d1	svárlivé
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
snaze	snaha	k1gFnSc6	snaha
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
císařem	císař	k1gMnSc7	císař
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
992	[number]	k4	992
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
III	III	kA	III
<g/>
.	.	kIx.	.
tažení	tažení	k1gNnSc1	tažení
proti	proti	k7c3	proti
Polabským	polabský	k2eAgInPc3d1	polabský
Slovanům	Slovan	k1gInPc3	Slovan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přemyslovci	Přemyslovec	k1gMnSc3	Přemyslovec
a	a	k8xC	a
Slavníkovci	Slavníkovec	k1gMnSc3	Slavníkovec
==	==	k?	==
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
995	[number]	k4	995
přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
vojska	vojsko	k1gNnSc2	vojsko
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Libici	Libice	k1gFnSc4	Libice
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
a	a	k8xC	a
vyvraždila	vyvraždit	k5eAaPmAgFnS	vyvraždit
přítomné	přítomný	k2eAgInPc4d1	přítomný
Slavníkovce	Slavníkovec	k1gInPc4	Slavníkovec
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
dovršeno	dovršen	k2eAgNnSc4d1	dovršeno
politické	politický	k2eAgNnSc4d1	politické
sjednocení	sjednocení	k1gNnSc4	sjednocení
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
Slavníkovci	Slavníkovec	k1gInSc3	Slavníkovec
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
historiografii	historiografie	k1gFnSc6	historiografie
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujícího	následující	k2eAgInSc2d1	následující
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
století	století	k1gNnSc4	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejdiskutovanějších	diskutovaný	k2eAgMnPc2d3	nejdiskutovanější
pojmů	pojem	k1gInPc2	pojem
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jím	jíst	k5eAaImIp1nS	jíst
i	i	k8xC	i
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Slavníkovce	Slavníkovka	k1gFnSc3	Slavníkovka
znal	znát	k5eAaImAgMnS	znát
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
německá	německý	k2eAgFnSc1d1	německá
historiografická	historiografický	k2eAgFnSc1d1	historiografická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
patřil	patřit	k5eAaImAgInS	patřit
Josef	Josef	k1gMnSc1	Josef
Loserth	Loserth	k1gMnSc1	Loserth
<g/>
,	,	kIx,	,
Bertold	Bertold	k1gMnSc1	Bertold
Bretholz	Bretholz	k1gMnSc1	Bretholz
nebo	nebo	k8xC	nebo
Julius	Julius	k1gMnSc1	Julius
Lippert	Lippert	k1gInSc4	Lippert
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
teorii	teorie	k1gFnSc3	teorie
dualismu	dualismus	k1gInSc2	dualismus
–	–	k?	–
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
existovaly	existovat	k5eAaImAgFnP	existovat
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
dva	dva	k4xCgInPc4	dva
mocensky	mocensky	k6eAd1	mocensky
si	se	k3xPyFc3	se
rovné	rovný	k2eAgInPc1d1	rovný
knížecí	knížecí	k2eAgInPc1d1	knížecí
rody	rod	k1gInPc1	rod
–	–	k?	–
Přemyslovci	Přemyslovec	k1gMnPc7	Přemyslovec
a	a	k8xC	a
Slavníkovci	Slavníkovec	k1gMnPc7	Slavníkovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc4	tento
spolu	spolu	k6eAd1	spolu
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
soupeřili	soupeřit	k5eAaImAgMnP	soupeřit
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
nad	nad	k7c7	nad
vznikajícím	vznikající	k2eAgInSc7d1	vznikající
českým	český	k2eAgInSc7d1	český
státem	stát	k1gInSc7	stát
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
soupeření	soupeření	k1gNnSc1	soupeření
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
násilným	násilný	k2eAgInSc7d1	násilný
vpádem	vpád	k1gInSc7	vpád
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
slavníkovských	slavníkovský	k2eAgNnPc2d1	slavníkovské
hradišť	hradiště	k1gNnPc2	hradiště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
rodu	rod	k1gInSc2	rod
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
zabita	zabit	k2eAgFnSc1d1	zabita
(	(	kIx(	(
<g/>
přežil	přežít	k5eAaPmAgMnS	přežít
pouze	pouze	k6eAd1	pouze
biskup	biskup	k1gMnSc1	biskup
sv.	sv.	kA	sv.
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
rodu	rod	k1gInSc2	rod
"	"	kIx"	"
<g/>
dux	dux	k?	dux
<g/>
"	"	kIx"	"
Soběslav	Soběslav	k1gMnSc1	Soběslav
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc4	jejich
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
bratr	bratr	k1gMnSc1	bratr
Radim	Radim	k1gMnSc1	Radim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jejich	jejich	k3xOp3gNnSc1	jejich
zničení	zničení	k1gNnSc1	zničení
ukončilo	ukončit	k5eAaPmAgNnS	ukončit
sjednocení	sjednocení	k1gNnSc4	sjednocení
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
stálo	stát	k5eAaImAgNnS	stát
u	u	k7c2	u
založení	založení	k1gNnSc2	založení
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Koncepci	koncepce	k1gFnSc4	koncepce
o	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
mocenských	mocenský	k2eAgInPc6d1	mocenský
útvarech	útvar	k1gInPc6	útvar
převzala	převzít	k5eAaPmAgFnS	převzít
Gollova	Gollův	k2eAgFnSc1d1	Gollova
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
především	především	k9	především
historik	historik	k1gMnSc1	historik
Václav	Václav	k1gMnSc1	Václav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
)	)	kIx)	)
a	a	k8xC	a
archeologicky	archeologicky	k6eAd1	archeologicky
ji	on	k3xPp3gFnSc4	on
svými	svůj	k3xOyFgInPc7	svůj
výzkumy	výzkum	k1gInPc7	výzkum
na	na	k7c6	na
hradišti	hradiště	k1gNnSc6	hradiště
Libice	Libice	k1gFnPc4	Libice
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
archeolog	archeolog	k1gMnSc1	archeolog
Rudolf	Rudolf	k1gMnSc1	Rudolf
Turek	Turek	k1gMnSc1	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
vědecké	vědecký	k2eAgFnSc2d1	vědecká
autority	autorita	k1gFnSc2	autorita
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Turka	Turek	k1gMnSc2	Turek
se	se	k3xPyFc4	se
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
teorii	teorie	k1gFnSc6	teorie
nic	nic	k3yNnSc1	nic
nezměnilo	změnit	k5eNaPmAgNnS	změnit
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
vystoupil	vystoupit	k5eAaPmAgInS	vystoupit
s	s	k7c7	s
vážnými	vážný	k2eAgFnPc7d1	vážná
pochybami	pochyba	k1gFnPc7	pochyba
jeho	jeho	k3xOp3gNnSc1	jeho
žák	žák	k1gMnSc1	žák
Jiří	Jiří	k1gMnSc1	Jiří
Sláma	Sláma	k1gMnSc1	Sláma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
mezi	mezi	k7c7	mezi
odborníky	odborník	k1gMnPc7	odborník
převládá	převládat	k5eAaImIp3nS	převládat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slavníkovci	Slavníkovec	k1gMnPc1	Slavníkovec
byli	být	k5eAaImAgMnP	být
blízcí	blízký	k2eAgMnPc1d1	blízký
příbuzní	příbuzný	k1gMnPc1	příbuzný
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
,	,	kIx,	,
Liudolfingů	Liudolfing	k1gInPc2	Liudolfing
(	(	kIx(	(
<g/>
Otonů	Oton	k1gInPc2	Oton
<g/>
)	)	kIx)	)
a	a	k8xC	a
zlických	zlický	k2eAgMnPc2d1	zlický
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dosazení	dosazený	k2eAgMnPc1d1	dosazený
správci	správce	k1gMnPc1	správce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Trstenické	Trstenický	k2eAgFnSc2d1	Trstenická
stezky	stezka	k1gFnSc2	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Trstenická	Trstenický	k2eAgFnSc1d1	Trstenická
stezka	stezka	k1gFnSc1	stezka
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
magistrálou	magistrála	k1gFnSc7	magistrála
spojující	spojující	k2eAgFnSc4d1	spojující
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
po	po	k7c4	po
které	který	k3yIgFnPc4	který
proudilo	proudit	k5eAaPmAgNnS	proudit
zboží	zboží	k1gNnSc1	zboží
(	(	kIx(	(
<g/>
kožešiny	kožešina	k1gFnPc1	kožešina
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
otroci	otrok	k1gMnPc1	otrok
<g/>
,	,	kIx,	,
luxusní	luxusní	k2eAgNnSc1d1	luxusní
zboží	zboží	k1gNnSc1	zboží
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
obchodu	obchod	k1gInSc2	obchod
musel	muset	k5eAaImAgInS	muset
mít	mít	k5eAaImF	mít
obrovské	obrovský	k2eAgInPc4d1	obrovský
zisky	zisk	k1gInPc4	zisk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Dohady	dohad	k1gInPc1	dohad
panují	panovat	k5eAaImIp3nP	panovat
i	i	k9	i
ohledně	ohledně	k7c2	ohledně
Boleslavovy	Boleslavův	k2eAgFnSc2d1	Boleslavova
manželky	manželka	k1gFnSc2	manželka
či	či	k8xC	či
manželek	manželka	k1gFnPc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Numismatik	numismatik	k1gMnSc1	numismatik
Pavel	Pavel	k1gMnSc1	Pavel
Radoměrský	Radoměrský	k2eAgMnSc1d1	Radoměrský
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
s	s	k7c7	s
hypotézou	hypotéza	k1gFnSc7	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
si	se	k3xPyFc3	se
císař	císař	k1gMnSc1	císař
Ota	Ota	k1gMnSc1	Ota
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
bral	brát	k5eAaImAgMnS	brát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
929	[number]	k4	929
nebo	nebo	k8xC	nebo
930	[number]	k4	930
Edith	Editha	k1gFnPc2	Editha
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
dcer	dcera	k1gFnPc2	dcera
anglického	anglický	k2eAgMnSc2d1	anglický
krále	král	k1gMnSc2	král
Ethelstana	Ethelstan	k1gMnSc2	Ethelstan
(	(	kIx(	(
<g/>
925	[number]	k4	925
–	–	k?	–
939	[number]	k4	939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
také	také	k6eAd1	také
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
měl	mít	k5eAaImAgMnS	mít
odvést	odvést	k5eAaPmF	odvést
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Edith	Edith	k1gInSc1	Edith
jménem	jméno	k1gNnSc7	jméno
Elfgifu	Elfgif	k1gInSc2	Elfgif
(	(	kIx(	(
<g/>
Aldgithu	Aldgith	k1gInSc2	Aldgith
<g/>
)	)	kIx)	)
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
jako	jako	k8xC	jako
nevěstu	nevěsta	k1gFnSc4	nevěsta
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
Boleslava	Boleslav	k1gMnSc4	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Radoměrský	Radoměrský	k2eAgMnSc1d1	Radoměrský
se	se	k3xPyFc4	se
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
opíral	opírat	k5eAaImAgMnS	opírat
o	o	k7c4	o
denáry	denár	k1gInPc4	denár
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
typu	typ	k1gInSc2	typ
s	s	k7c7	s
domnělým	domnělý	k2eAgInSc7d1	domnělý
nápisem	nápis	k1gInSc7	nápis
ADIVEA	ADIVEA	kA	ADIVEA
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
numismatické	numismatický	k2eAgNnSc1d1	numismatické
bádání	bádání	k1gNnSc1	bádání
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mince	mince	k1gFnSc1	mince
nejsou	být	k5eNaImIp3nP	být
anglosaského	anglosaský	k2eAgInSc2d1	anglosaský
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
hlava	hlava	k1gFnSc1	hlava
ženy	žena	k1gFnSc2	žena
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
hlava	hlava	k1gFnSc1	hlava
Krista	Krista	k1gFnSc1	Krista
a	a	k8xC	a
motiv	motiv	k1gInSc1	motiv
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
byzantizujícího	byzantizující	k2eAgNnSc2d1	byzantizující
otonského	otonský	k2eAgNnSc2d1	otonský
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
a	a	k8xC	a
nápis	nápis	k1gInSc1	nápis
je	být	k5eAaImIp3nS	být
zkomolené	zkomolený	k2eAgNnSc4d1	zkomolené
jméno	jméno	k1gNnSc4	jméno
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tzv.	tzv.	kA	tzv.
proškrtnuté	proškrtnutý	k2eAgFnPc1d1	proškrtnutá
D	D	kA	D
lze	lze	k6eAd1	lze
číst	číst	k5eAaImF	číst
jako	jako	k9	jako
B	B	kA	B
a	a	k8xC	a
V	V	kA	V
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
L.	L.	kA	L.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Další	další	k2eAgFnSc1d1	další
mezera	mezera	k1gFnSc1	mezera
Radoměrského	Radoměrský	k2eAgInSc2d1	Radoměrský
hypotézy	hypotéza	k1gFnPc1	hypotéza
tkví	tkvět	k5eAaImIp3nS	tkvět
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
svatby	svatba	k1gFnSc2	svatba
Oty	Ota	k1gMnSc2	Ota
I.	I.	kA	I.
zřejmě	zřejmě	k6eAd1	zřejmě
nebyl	být	k5eNaImAgMnS	být
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
ani	ani	k8xC	ani
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
Boleslava	Boleslav	k1gMnSc4	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
měl	mít	k5eAaImAgMnS	mít
nejspíš	nejspíš	k9	nejspíš
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
,	,	kIx,	,
budování	budování	k1gNnSc1	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
uváděni	uváděn	k2eAgMnPc1d1	uváděn
u	u	k7c2	u
neznámé	známý	k2eNgFnSc2d1	neznámá
druhé	druhý	k4xOgFnSc2	druhý
manželky	manželka	k1gFnSc2	manželka
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
manželek	manželka	k1gFnPc2	manželka
z	z	k7c2	z
předpokládaného	předpokládaný	k2eAgNnSc2d1	předpokládané
rozmezí	rozmezí	k1gNnSc2	rozmezí
dat	datum	k1gNnPc2	datum
narození	narození	k1gNnSc1	narození
Boleslavových	Boleslavův	k2eAgMnPc2d1	Boleslavův
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
zhruba	zhruba	k6eAd1	zhruba
65	[number]	k4	65
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
bazilice	bazilika	k1gFnSc6	bazilika
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
podlahou	podlaha	k1gFnSc7	podlaha
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
hlavní	hlavní	k2eAgFnSc2d1	hlavní
chrámové	chrámový	k2eAgFnSc2d1	chrámová
lodi	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
probíhal	probíhat	k5eAaImAgInS	probíhat
úpadek	úpadek	k1gInSc1	úpadek
státu	stát	k1gInSc2	stát
následně	následně	k6eAd1	následně
podpořený	podpořený	k2eAgInSc1d1	podpořený
rozkoly	rozkol	k1gInPc1	rozkol
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
třemi	tři	k4xCgInPc7	tři
syny	syn	k1gMnPc7	syn
<g/>
.	.	kIx.	.
</s>
<s>
Polský	polský	k2eAgMnSc1d1	polský
panovník	panovník	k1gMnSc1	panovník
Boleslav	Boleslav	k1gMnSc1	Boleslav
Chrabrý	chrabrý	k2eAgMnSc1d1	chrabrý
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
nejen	nejen	k6eAd1	nejen
Krakovska	Krakovsko	k1gNnSc2	Krakovsko
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
dokonce	dokonce	k9	dokonce
i	i	k9	i
samotných	samotný	k2eAgFnPc2d1	samotná
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potomci	potomek	k1gMnPc5	potomek
==	==	k?	==
</s>
</p>
<p>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
965	[number]	k4	965
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1037	[number]	k4	1037
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
∞	∞	k?	∞
N.	N.	kA	N.
<g/>
N.	N.	kA	N.
<g/>
Jaromír	Jaromír	k1gMnSc1	Jaromír
(	(	kIx(	(
<g/>
†	†	k?	†
1035	[number]	k4	1035
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
</s>
</p>
<p>
<s>
Oldřich	Oldřich	k1gMnSc1	Oldřich
(	(	kIx(	(
<g/>
†	†	k?	†
1034	[number]	k4	1034
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
kníže	kníže	k1gMnSc1	kníže
<g/>
∞	∞	k?	∞
N.	N.	kA	N.
<g/>
N.	N.	kA	N.
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
uváděna	uváděn	k2eAgFnSc1d1	uváděna
Němka	Němka	k1gFnSc1	Němka
Jutta	Jutta	k1gFnSc1	Jutta
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zapuzena	zapuzen	k2eAgFnSc1d1	zapuzena
</s>
</p>
<p>
<s>
∞	∞	k?	∞
BoženaVáclav	BoženaVáclav	k1gFnSc1	BoženaVáclav
</s>
</p>
<p>
<s>
==	==	k?	==
Genealogie	genealogie	k1gFnSc2	genealogie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
FROLÍK	FROLÍK	kA	FROLÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
PROFANTOVÁ	PROFANTOVÁ	kA	PROFANTOVÁ
<g/>
,	,	kIx,	,
Naďa	Naďa	k1gFnSc1	Naďa
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
I.	I.	kA	I.
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
800	[number]	k4	800
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
198	[number]	k4	198
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
657	[number]	k4	657
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
RYANTOVÁ	RYANTOVÁ	kA	RYANTOVÁ
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
;	;	kIx,	;
VOREL	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
940	[number]	k4	940
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
35	[number]	k4	35
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁT	Charvát	k1gMnSc1	Charvát
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
568	[number]	k4	568
<g/>
-	-	kIx~	-
<g/>
1055	[number]	k4	1055
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
263	[number]	k4	263
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
845	[number]	k4	845
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
našich	náš	k3xOp1gFnPc6	náš
dějinách	dějiny	k1gFnPc6	dějiny
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
/	/	kIx~	/
(	(	kIx(	(
<g/>
Pavel	Pavel	k1gMnSc1	Pavel
Augusta	Augusta	k1gMnSc1	Augusta
...	...	k?	...
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
94	[number]	k4	94
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
S.	S.	kA	S.
42	[number]	k4	42
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
I.	I.	kA	I.
Zrození	zrození	k1gNnSc1	zrození
státu	stát	k1gInSc2	stát
872	[number]	k4	872
<g/>
-	-	kIx~	-
<g/>
972	[number]	k4	972
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bořivoje	Bořivoj	k1gMnSc2	Bořivoj
I.	I.	kA	I.
po	po	k7c4	po
Boleslava	Boleslav	k1gMnSc4	Boleslav
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
267	[number]	k4	267
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
308	[number]	k4	308
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUTOVSKÝ	LUTOVSKÝ	kA	LUTOVSKÝ
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
prvních	první	k4xOgInPc2	první
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Léta	léto	k1gNnPc1	léto
krize	krize	k1gFnSc2	krize
a	a	k8xC	a
obnovy	obnova	k1gFnSc2	obnova
972	[number]	k4	972
<g/>
--	--	k?	--
<g/>
1012	[number]	k4	1012
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
po	po	k7c4	po
Jaromíra	Jaromír	k1gMnSc4	Jaromír
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
271	[number]	k4	271
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
231	[number]	k4	231
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MATLA-KOZŁOWSKA	MATLA-KOZŁOWSKA	k?	MATLA-KOZŁOWSKA
<g/>
,	,	kIx,	,
Marzena	Marzen	k2eAgFnSc1d1	Marzena
<g/>
.	.	kIx.	.
</s>
<s>
Pierwsi	Pierwse	k1gFnSc4	Pierwse
Przemyślidzi	Przemyślidze	k1gFnSc4	Przemyślidze
i	i	k9	i
ich	ich	k?	ich
państwo	państwo	k6eAd1	państwo
(	(	kIx(	(
<g/>
od	od	k7c2	od
X	X	kA	X
do	do	k7c2	do
połowy	połowa	k1gFnSc2	połowa
XI	XI	kA	XI
wieku	wieku	k6eAd1	wieku
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ekspansja	Ekspansj	k2eAgFnSc1d1	Ekspansj
terytorialna	terytorialna	k1gFnSc1	terytorialna
i	i	k8xC	i
jej	on	k3xPp3gMnSc4	on
polityczne	politycznout	k5eAaPmIp3nS	politycznout
uwarunkowania	uwarunkowanium	k1gNnPc5	uwarunkowanium
<g/>
.	.	kIx.	.
</s>
<s>
Poznań	Poznań	k?	Poznań
<g/>
:	:	kIx,	:
Wydawnictwo	Wydawnictwo	k1gMnSc1	Wydawnictwo
Poznańskie	Poznańskie	k1gFnSc2	Poznańskie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
576	[number]	k4	576
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
7177	[number]	k4	7177
<g/>
-	-	kIx~	-
<g/>
547	[number]	k4	547
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
dějiny	dějiny	k1gFnPc1	dějiny
I.	I.	kA	I.
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
Od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
knížete	kníže	k1gMnSc2	kníže
Oldřicha	Oldřich	k1gMnSc2	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Laichter	Laichter	k1gMnSc1	Laichter
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
-	-	kIx~	-
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
61	[number]	k4	61
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETRÁŇ	Petráň	k1gMnSc1	Petráň
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
české	český	k2eAgFnPc4d1	Česká
mince	mince	k1gFnPc4	mince
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Set	set	k1gInSc1	set
out	out	k?	out
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
239	[number]	k4	239
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902058	[number]	k4	902058
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POLANSKÝ	Polanský	k1gMnSc1	Polanský
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
;	;	kIx,	;
SLÁMA	Sláma	k1gMnSc1	Sláma
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovský	přemyslovský	k2eAgInSc1d1	přemyslovský
stát	stát	k1gInSc1	stát
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
:	:	kIx,	:
na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
knížete	kníže	k1gMnSc2	kníže
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
+	+	kIx~	+
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
999	[number]	k4	999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
377	[number]	k4	377
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
272	[number]	k4	272
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SLÁMA	Sláma	k1gMnSc1	Sláma
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Slavníkovci	Slavníkovec	k1gMnPc1	Slavníkovec
<g/>
–	–	k?	–
<g/>
významná	významný	k2eAgFnSc1d1	významná
či	či	k8xC	či
okrajová	okrajový	k2eAgFnSc1d1	okrajová
záležitost	záležitost	k1gFnSc1	záležitost
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
rozhledy	rozhled	k1gInPc1	rozhled
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
47	[number]	k4	47
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
182	[number]	k4	182
<g/>
-	-	kIx~	-
<g/>
224	[number]	k4	224
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
323	[number]	k4	323
<g/>
-	-	kIx~	-
<g/>
1267	[number]	k4	1267
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOBIESIAK	SOBIESIAK	kA	SOBIESIAK
<g/>
,	,	kIx,	,
Joanna	Joanen	k2eAgFnSc1d1	Joanna
Aleksandra	Aleksandra	k1gFnSc1	Aleksandra
<g/>
.	.	kIx.	.
</s>
<s>
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
†	†	k?	†
999	[number]	k4	999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
;	;	kIx,	;
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
:	:	kIx,	:
budování	budování	k1gNnSc1	budování
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
779	[number]	k4	779
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
a	a	k8xC	a
proč	proč	k6eAd1	proč
vyvraždil	vyvraždit	k5eAaPmAgMnS	vyvraždit
Slavníkovce	Slavníkovka	k1gFnSc3	Slavníkovka
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
14	[number]	k4	14
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
418	[number]	k4	418
<g/>
-	-	kIx~	-
<g/>
5129	[number]	k4	5129
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TŘEŠTÍK	TŘEŠTÍK	kA	TŘEŠTÍK
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
:	:	kIx,	:
vstup	vstup	k1gInSc1	vstup
Čechů	Čech	k1gMnPc2	Čech
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
(	(	kIx(	(
<g/>
530	[number]	k4	530
<g/>
–	–	k?	–
<g/>
935	[number]	k4	935
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
658	[number]	k4	658
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
138	[number]	k4	138
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VOŠAHLÍKOVÁ	Vošahlíková	k1gFnSc1	Vošahlíková
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
:	:	kIx,	:
6	[number]	k4	6
<g/>
.	.	kIx.	.
sešit	sešit	k1gInSc1	sešit
:	:	kIx,	:
Boh	Boh	k1gFnSc1	Boh
<g/>
–	–	k?	–
<g/>
Bož	Boža	k1gFnPc2	Boža
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
109	[number]	k4	109
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WIHODA	WIHODA	kA	WIHODA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
knížecí	knížecí	k2eAgFnSc1d1	knížecí
906	[number]	k4	906
<g/>
–	–	k?	–
<g/>
1197	[number]	k4	1197
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
464	[number]	k4	464
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŽEMLIČKA	Žemlička	k1gMnSc1	Žemlička
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
:	:	kIx,	:
jak	jak	k6eAd1	jak
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
vládli	vládnout	k5eAaImAgMnP	vládnout
<g/>
,	,	kIx,	,
umírali	umírat	k5eAaImAgMnP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
497	[number]	k4	497
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
759	[number]	k4	759
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Říše	říše	k1gFnSc1	říše
českých	český	k2eAgMnPc2d1	český
Boleslavů	Boleslav	k1gMnPc2	Boleslav
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Manželky	manželka	k1gFnPc1	manželka
Boleslava	Boleslav	k1gMnSc2	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
