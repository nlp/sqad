<s>
Kounicovy	Kounicův	k2eAgFnPc1d1	Kounicova
či	či	k8xC	či
Kaunicovy	Kaunicův	k2eAgFnPc1d1	Kaunicova
studentské	studentský	k2eAgFnPc1d1	studentská
koleje	kolej	k1gFnPc1	kolej
v	v	k7c6	v
Brně-Žabovřeskách	Brně-Žabovřeska	k1gFnPc6	Brně-Žabovřeska
provází	provázet	k5eAaImIp3nS	provázet
pohnutá	pohnutý	k2eAgFnSc1d1	pohnutá
historie	historie	k1gFnSc1	historie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
jen	jen	k9	jen
záležitostí	záležitost	k1gFnSc7	záležitost
kolem	kolem	k7c2	kolem
ubytování	ubytování	k1gNnSc2	ubytování
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
hrozivého	hrozivý	k2eAgNnSc2d1	hrozivé
využití	využití	k1gNnSc2	využití
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
začal	začít	k5eAaPmAgInS	začít
boj	boj	k1gInSc1	boj
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
městě	město	k1gNnSc6	město
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
českých	český	k2eAgMnPc2d1	český
vlastenců	vlastenec	k1gMnPc2	vlastenec
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Tomášem	Tomáš	k1gMnSc7	Tomáš
Garriguem	Garrigu	k1gMnSc7	Garrigu
Masarykem	Masaryk	k1gMnSc7	Masaryk
bylo	být	k5eAaImAgNnS	být
vybudování	vybudování	k1gNnSc4	vybudování
české	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tlaky	tlak	k1gInPc1	tlak
na	na	k7c4	na
její	její	k3xOp3gNnSc4	její
zřízení	zřízení	k1gNnSc4	zřízení
zesílily	zesílit	k5eAaPmAgInP	zesílit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
myšlenka	myšlenka	k1gFnSc1	myšlenka
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
vídeňského	vídeňský	k2eAgInSc2d1	vídeňský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
zřízení	zřízení	k1gNnSc3	zřízení
nové	nový	k2eAgFnSc2d1	nová
české	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
však	však	k9	však
postavili	postavit	k5eAaPmAgMnP	postavit
brněnští	brněnský	k2eAgMnPc1d1	brněnský
a	a	k8xC	a
moravští	moravský	k2eAgMnPc1d1	moravský
Němci	Němec	k1gMnPc1	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
konal	konat	k5eAaImAgInS	konat
sjezd	sjezd	k1gInSc4	sjezd
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Volkstag	Volkstag	k1gInSc1	Volkstag
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
problémem	problém	k1gInSc7	problém
české	český	k2eAgFnSc2d1	Česká
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zabýval	zabývat	k5eAaImAgMnS	zabývat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
svolala	svolat	k5eAaPmAgFnS	svolat
kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
problému	problém	k1gInSc3	problém
česká	český	k2eAgFnSc1d1	Česká
Národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
potyčkám	potyčka	k1gFnPc3	potyčka
<g/>
,	,	kIx,	,
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Němců	Němec	k1gMnPc2	Němec
byla	být	k5eAaImAgFnS	být
povolána	povolán	k2eAgFnSc1d1	povolána
policie	policie	k1gFnSc1	policie
a	a	k8xC	a
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
Brna	Brno	k1gNnSc2	Brno
zasahovalo	zasahovat	k5eAaImAgNnS	zasahovat
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
zásahu	zásah	k1gInSc6	zásah
byl	být	k5eAaImAgMnS	být
zabit	zabit	k2eAgMnSc1d1	zabit
český	český	k2eAgMnSc1d1	český
dělník	dělník	k1gMnSc1	dělník
František	František	k1gMnSc1	František
Pavlík	Pavlík	k1gMnSc1	Pavlík
z	z	k7c2	z
Ořechova	Ořechův	k2eAgNnSc2d1	Ořechův
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
odporu	odpor	k1gInSc3	odpor
Němců	Němec	k1gMnPc2	Němec
se	se	k3xPyFc4	se
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
za	za	k7c4	za
Rakouska-Uherska	Rakouska-Uhersek	k1gMnSc4	Rakouska-Uhersek
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zřídit	zřídit	k5eAaPmF	zřídit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1908	[number]	k4	1908
daroval	darovat	k5eAaPmAgMnS	darovat
český	český	k2eAgMnSc1d1	český
vlastenec	vlastenec	k1gMnSc1	vlastenec
Václav	Václav	k1gMnSc1	Václav
Robert	Robert	k1gMnSc1	Robert
Kounic	Kounice	k1gFnPc2	Kounice
svůj	svůj	k3xOyFgInSc4	svůj
dům	dům	k1gInSc4	dům
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
číslo	číslo	k1gNnSc1	číslo
617	[number]	k4	617
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
ubytování	ubytování	k1gNnSc2	ubytování
českých	český	k2eAgMnPc2d1	český
studentů	student	k1gMnPc2	student
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgMnS	zřídit
nadaci	nadace	k1gFnSc4	nadace
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
studentských	studentský	k2eAgFnPc2d1	studentská
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
výstavbě	výstavba	k1gFnSc3	výstavba
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c4	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
samostatném	samostatný	k2eAgNnSc6d1	samostatné
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
studenti	student	k1gMnPc1	student
se	se	k3xPyFc4	se
do	do	k7c2	do
kolejí	kolej	k1gFnPc2	kolej
nastěhovali	nastěhovat	k5eAaPmAgMnP	nastěhovat
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
<s>
Koleje	kolej	k1gFnPc1	kolej
sloužily	sloužit	k5eAaImAgFnP	sloužit
svému	svůj	k3xOyFgInSc3	svůj
účelu	účel	k1gInSc3	účel
až	až	k6eAd1	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
přepadeny	přepaden	k2eAgMnPc4d1	přepaden
příslušníky	příslušník	k1gMnPc4	příslušník
gestapa	gestapo	k1gNnSc2	gestapo
a	a	k8xC	a
jednotkami	jednotka	k1gFnPc7	jednotka
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Sachsenhausen	Sachsenhausna	k1gFnPc2	Sachsenhausna
bylo	být	k5eAaImAgNnS	být
odvlečeno	odvléct	k5eAaPmNgNnS	odvléct
173	[number]	k4	173
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
nájemníků	nájemník	k1gMnPc2	nájemník
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
Kounicovy	Kounicův	k2eAgFnPc1d1	Kounicova
koleje	kolej	k1gFnPc1	kolej
využívány	využíván	k2eAgFnPc1d1	využívána
jako	jako	k9	jako
věznice	věznice	k1gFnSc1	věznice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
během	během	k7c2	během
války	válka	k1gFnSc2	válka
prošlo	projít	k5eAaPmAgNnS	projít
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
českých	český	k2eAgMnPc2d1	český
vlastenců	vlastenec	k1gMnPc2	vlastenec
<g/>
,	,	kIx,	,
odbojářů	odbojář	k1gMnPc2	odbojář
a	a	k8xC	a
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
deportování	deportování	k1gNnPc4	deportování
do	do	k7c2	do
německých	německý	k2eAgInPc2d1	německý
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
byli	být	k5eAaImAgMnP	být
popravováni	popravován	k2eAgMnPc1d1	popravován
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
<g/>
.	.	kIx.	.
</s>
<s>
Popravovalo	popravovat	k5eAaImAgNnS	popravovat
se	s	k7c7	s
zastřelením	zastřelení	k1gNnSc7	zastřelení
<g/>
,	,	kIx,	,
oběšením	oběšení	k1gNnSc7	oběšení
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vraždilo	vraždit	k5eAaImAgNnS	vraždit
ranou	rána	k1gFnSc7	rána
z	z	k7c2	z
pistole	pistol	k1gFnSc2	pistol
do	do	k7c2	do
týla	týl	k1gInSc2	týl
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
vězni	vězeň	k1gMnPc1	vězeň
byli	být	k5eAaImAgMnP	být
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
či	či	k8xC	či
deportací	deportace	k1gFnSc7	deportace
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
krutě	krutě	k6eAd1	krutě
mučeni	mučen	k2eAgMnPc1d1	mučen
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
spáchali	spáchat	k5eAaPmAgMnP	spáchat
raději	rád	k6eAd2	rád
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Popravy	poprava	k1gFnPc1	poprava
v	v	k7c6	v
Kounicových	Kounicový	k2eAgFnPc6d1	Kounicová
kolejích	kolej	k1gFnPc6	kolej
byly	být	k5eAaImAgFnP	být
veřejné	veřejný	k2eAgFnPc1d1	veřejná
a	a	k8xC	a
v	v	k7c6	v
hojném	hojný	k2eAgInSc6d1	hojný
počtu	počet	k1gInSc6	počet
byly	být	k5eAaImAgFnP	být
navštěvovány	navštěvovat	k5eAaImNgFnP	navštěvovat
brněnskými	brněnský	k2eAgMnPc7d1	brněnský
Němci	Němec	k1gMnPc7	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gNnSc6	jejich
sledování	sledování	k1gNnSc6	sledování
byly	být	k5eAaImAgFnP	být
prodávány	prodáván	k2eAgFnPc1d1	prodávána
vstupenky	vstupenka	k1gFnPc1	vstupenka
za	za	k7c4	za
tři	tři	k4xCgFnPc4	tři
marky	marka	k1gFnPc4	marka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
publiku	publikum	k1gNnSc6	publikum
přitom	přitom	k6eAd1	přitom
převažoval	převažovat	k5eAaImAgInS	převažovat
podíl	podíl	k1gInSc1	podíl
německých	německý	k2eAgFnPc2d1	německá
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
během	během	k7c2	během
exekucí	exekuce	k1gFnPc2	exekuce
se	se	k3xPyFc4	se
ozýval	ozývat	k5eAaImAgInS	ozývat
smích	smích	k1gInSc1	smích
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
se	se	k3xPyFc4	se
netajili	tajit	k5eNaImAgMnP	tajit
radostí	radost	k1gFnSc7	radost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
české	český	k2eAgFnPc1d1	Česká
potvory	potvora	k1gFnPc1	potvora
a	a	k8xC	a
bestie	bestie	k1gFnPc1	bestie
<g/>
"	"	kIx"	"
zabíjeny	zabíjen	k2eAgFnPc1d1	zabíjena
<g/>
,	,	kIx,	,
či	či	k8xC	či
povzdechem	povzdech	k1gInSc7	povzdech
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
dnes	dnes	k6eAd1	dnes
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
nějak	nějak	k6eAd1	nějak
málo	málo	k4c1	málo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Těla	tělo	k1gNnPc4	tělo
popravených	popravený	k2eAgMnPc2d1	popravený
byla	být	k5eAaImAgFnS	být
spalována	spalovat	k5eAaImNgFnS	spalovat
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
popel	popel	k1gInSc4	popel
nacističtí	nacistický	k2eAgMnPc1d1	nacistický
prominenti	prominent	k1gMnPc1	prominent
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
hnojení	hnojení	k1gNnSc3	hnojení
svých	svůj	k3xOyFgFnPc2	svůj
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
popraveno	popravit	k5eAaPmNgNnS	popravit
okolo	okolo	k7c2	okolo
800	[number]	k4	800
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
i	i	k9	i
počet	počet	k1gInSc1	počet
1300	[number]	k4	1300
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
se	se	k3xPyFc4	se
koleje	kolej	k1gFnPc1	kolej
staly	stát	k5eAaPmAgFnP	stát
vězením	vězení	k1gNnSc7	vězení
pro	pro	k7c4	pro
kolaboranty	kolaborant	k1gMnPc4	kolaborant
a	a	k8xC	a
nacisty	nacista	k1gMnPc4	nacista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
stovky	stovka	k1gFnPc4	stovka
nevinných	vinný	k2eNgMnPc2d1	nevinný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
policie	policie	k1gFnSc1	policie
nebo	nebo	k8xC	nebo
armáda	armáda	k1gFnSc1	armáda
zatkla	zatknout	k5eAaPmAgFnS	zatknout
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
jenom	jenom	k9	jenom
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
nosili	nosit	k5eAaImAgMnP	nosit
bílou	bílý	k2eAgFnSc4d1	bílá
pásku	páska	k1gFnSc4	páska
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
se	se	k3xPyFc4	se
chodily	chodit	k5eAaImAgFnP	chodit
německé	německý	k2eAgFnPc1d1	německá
ženy	žena	k1gFnPc1	žena
za	za	k7c2	za
protektorátu	protektorát	k1gInSc2	protektorát
dívat	dívat	k5eAaImF	dívat
na	na	k7c4	na
popravy	poprava	k1gFnPc4	poprava
českých	český	k2eAgMnPc2d1	český
vlastenců	vlastenec	k1gMnPc2	vlastenec
v	v	k7c6	v
Kounicových	Kounicový	k2eAgFnPc6d1	Kounicová
kolejích	kolej	k1gFnPc6	kolej
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
Češi	Čech	k1gMnPc1	Čech
si	se	k3xPyFc3	se
nenechali	nechat	k5eNaPmAgMnP	nechat
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
ujít	ujít	k5eAaPmF	ujít
tuto	tento	k3xDgFnSc4	tento
příležitost	příležitost	k1gFnSc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
večer	večer	k1gInSc1	večer
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
shromažďovaly	shromažďovat	k5eAaImAgInP	shromažďovat
celé	celý	k2eAgInPc1d1	celý
davy	dav	k1gInPc1	dav
zvědavců	zvědavec	k1gMnPc2	zvědavec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
ráně	rána	k1gFnSc6	rána
radostně	radostně	k6eAd1	radostně
hulákali	hulákat	k5eAaImAgMnP	hulákat
a	a	k8xC	a
povzbuzovali	povzbuzovat	k5eAaImAgMnP	povzbuzovat
bijce	bijce	k1gMnSc4	bijce
oplzlými	oplzlý	k2eAgFnPc7d1	oplzlá
řečmi	řeč	k1gFnPc7	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
fungovala	fungovat	k5eAaImAgFnS	fungovat
věznice	věznice	k1gFnPc4	věznice
v	v	k7c6	v
Kounicových	Kounicový	k2eAgFnPc6d1	Kounicová
kolejích	kolej	k1gFnPc6	kolej
jen	jen	k9	jen
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
zde	zde	k6eAd1	zde
na	na	k7c4	na
následky	následek	k1gInPc4	následek
mučení	mučení	k1gNnPc2	mučení
nejméně	málo	k6eAd3	málo
tři	tři	k4xCgNnPc4	tři
sta	sto	k4xCgNnPc4	sto
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
hromadných	hromadný	k2eAgInPc6d1	hromadný
hrobech	hrob	k1gInPc6	hrob
na	na	k7c6	na
Ústředním	ústřední	k2eAgInSc6d1	ústřední
hřbitově	hřbitov	k1gInSc6	hřbitov
a	a	k8xC	a
v	v	k7c6	v
Purkyňově	Purkyňův	k2eAgFnSc6d1	Purkyňova
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
slouží	sloužit	k5eAaImIp3nS	sloužit
budova	budova	k1gFnSc1	budova
pro	pro	k7c4	pro
ubytování	ubytování	k1gNnSc4	ubytování
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
nacistického	nacistický	k2eAgInSc2d1	nacistický
teroru	teror	k1gInSc2	teror
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
historická	historický	k2eAgFnSc1d1	historická
expozice	expozice	k1gFnSc1	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
připomínka	připomínka	k1gFnSc1	připomínka
těchto	tento	k3xDgFnPc2	tento
tragických	tragický	k2eAgFnPc2d1	tragická
událostí	událost	k1gFnPc2	událost
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalého	bývalý	k2eAgNnSc2d1	bývalé
popraviště	popraviště	k1gNnSc2	popraviště
umístěn	umístěn	k2eAgInSc4d1	umístěn
bronzový	bronzový	k2eAgInSc4d1	bronzový
reliéf	reliéf	k1gInSc4	reliéf
Vlastenec	vlastenec	k1gMnSc1	vlastenec
a	a	k8xC	a
v	v	k7c6	v
parku	park	k1gInSc6	park
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Památník	památník	k1gInSc1	památník
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
fašismem	fašismus	k1gInSc7	fašismus
–	–	k?	–
obložený	obložený	k2eAgInSc1d1	obložený
pylon	pylon	k1gInSc1	pylon
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
byly	být	k5eAaImAgFnP	být
koleje	kolej	k1gFnPc1	kolej
prohlášeny	prohlásit	k5eAaPmNgFnP	prohlásit
za	za	k7c4	za
národní	národní	k2eAgFnSc4d1	národní
kulturní	kulturní	k2eAgFnSc4d1	kulturní
památku	památka	k1gFnSc4	památka
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Vašek	Vašek	k1gMnSc1	Vašek
-	-	kIx~	-
Vladimír	Vladimír	k1gMnSc1	Vladimír
Černý	Černý	k1gMnSc1	Černý
-	-	kIx~	-
Jan	Jan	k1gMnSc1	Jan
Břečka	břečka	k1gFnSc1	břečka
<g/>
:	:	kIx,	:
Místa	místo	k1gNnPc1	místo
zkropená	zkropený	k2eAgFnSc1d1	zkropená
krví	krev	k1gFnSc7	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kounicovy	Kounicův	k2eAgFnPc1d1	Kounicova
studentské	studentský	k2eAgFnPc1d1	studentská
koleje	kolej	k1gFnPc1	kolej
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
nacistické	nacistický	k2eAgFnSc2d1	nacistická
okupace	okupace	k1gFnSc2	okupace
1940	[number]	k4	1940
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Archiv	archiv	k1gInSc1	archiv
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86736	[number]	k4	86736
<g/>
-	-	kIx~	-
<g/>
41	[number]	k4	41
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
pod	pod	k7c7	pod
hákovým	hákový	k2eAgInSc7d1	hákový
křížem	kříž	k1gInSc7	kříž
<g/>
,	,	kIx,	,
Votobia	Votobia	k1gFnSc1	Votobia
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7220	[number]	k4	7220
<g/>
-	-	kIx~	-
<g/>
167	[number]	k4	167
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
:	:	kIx,	:
Vlastivěda	vlastivěda	k1gFnSc1	vlastivěda
moravská	moravský	k2eAgFnSc1d1	Moravská
–	–	k?	–
Svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
a	a	k8xC	a
okupace	okupace	k1gFnSc1	okupace
<g/>
,	,	kIx,	,
Muzejní	muzejní	k2eAgFnSc1d1	muzejní
a	a	k8xC	a
vlastivědná	vlastivědný	k2eAgFnSc1d1	vlastivědná
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7275	[number]	k4	7275
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kounicovy	Kounicův	k2eAgFnSc2d1	Kounicova
koleje	kolej	k1gFnSc2	kolej
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Vraždy	vražda	k1gFnSc2	vražda
"	"	kIx"	"
<g/>
na	na	k7c6	na
písečku	píseček	k1gInSc6	píseček
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
osob	osoba	k1gFnPc2	osoba
popravených	popravený	k2eAgFnPc2d1	popravená
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
Kounicových	Kounicový	k2eAgFnPc6d1	Kounicová
kolejích	kolej	k1gFnPc6	kolej
za	za	k7c2	za
I.	I.	kA	I.
a	a	k8xC	a
II	II	kA	II
<g/>
.	.	kIx.	.
stanného	stanný	k2eAgNnSc2d1	stanné
práva	právo	k1gNnSc2	právo
Kounicovy	Kounicův	k2eAgFnSc2d1	Kounicova
koleje	kolej	k1gFnSc2	kolej
</s>
