<s>
Živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
Metazoa	Metazoa	k1gMnSc1	Metazoa
<g/>
,	,	kIx,	,
Animalia	Animalia	k1gFnSc1	Animalia
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
říše	říše	k1gFnSc1	říše
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
heterotrofních	heterotrofní	k2eAgInPc2d1	heterotrofní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
na	na	k7c6	na
buněčné	buněčný	k2eAgFnSc6d1	buněčná
úrovni	úroveň	k1gFnSc6	úroveň
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
buňky	buňka	k1gFnPc1	buňka
nemají	mít	k5eNaImIp3nP	mít
plastidy	plastid	k1gInPc4	plastid
ani	ani	k8xC	ani
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
řazeni	řadit	k5eAaImNgMnP	řadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Opisthokonta	Opisthokont	k1gMnSc2	Opisthokont
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
houbami	houba	k1gFnPc7	houba
a	a	k8xC	a
některými	některý	k3yIgMnPc7	některý
prvoky	prvok	k1gMnPc7	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Říše	říše	k1gFnSc1	říše
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
pojetí	pojetí	k1gNnSc6	pojetí
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
podříší	podříše	k1gFnSc7	podříše
"	"	kIx"	"
<g/>
mnohobuněční	mnohobuněční	k2eAgMnSc1d1	mnohobuněční
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Metazoa	Metazoa	k1gFnSc1	Metazoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
tedy	tedy	k9	tedy
žádné	žádný	k3yNgMnPc4	žádný
prvoky	prvok	k1gMnPc4	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Myxozoa	Myxozo	k1gInSc2	Myxozo
je	být	k5eAaImIp3nS	být
jednobuněčná	jednobuněčný	k2eAgFnSc1d1	jednobuněčná
sekundárně	sekundárně	k6eAd1	sekundárně
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
parazitického	parazitický	k2eAgInSc2d1	parazitický
způsobu	způsob	k1gInSc2	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
ale	ale	k9	ale
do	do	k7c2	do
pravých	pravá	k1gFnPc2	pravá
Metazoa	Metazoum	k1gNnSc2	Metazoum
-	-	kIx~	-
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
její	její	k3xOp3gFnSc1	její
příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
žahavci	žahavec	k1gMnPc7	žahavec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
živočichové	živočich	k1gMnPc1	živočich
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
(	(	kIx(	(
<g/>
Holozoa	Holozoa	k1gFnSc1	Holozoa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
skupina	skupina	k1gFnSc1	skupina
Metazoa	Metazo	k1gInSc2	Metazo
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgFnPc4	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
některé	některý	k3yIgFnPc4	některý
jednobuněčné	jednobuněčný	k2eAgFnPc4d1	jednobuněčná
příbuzné	příbuzná	k1gFnPc4	příbuzná
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
o	o	k7c4	o
parafyletickou	parafyletický	k2eAgFnSc4d1	parafyletická
skupinu	skupina	k1gFnSc4	skupina
trubének	trubénka	k1gFnPc2	trubénka
(	(	kIx(	(
<g/>
Choanozoa	Choanozoa	k1gFnSc1	Choanozoa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
heterotrofní	heterotrofní	k2eAgInPc4d1	heterotrofní
organismy	organismus	k1gInPc4	organismus
jsou	být	k5eAaImIp3nP	být
živočichové	živočich	k1gMnPc1	živočich
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
autotrofních	autotrofní	k2eAgInPc6d1	autotrofní
organismech	organismus	k1gInPc6	organismus
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
na	na	k7c6	na
rostlinách	rostlina	k1gFnPc6	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
živočichové	živočich	k1gMnPc1	živočich
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
symbióze	symbióza	k1gFnSc6	symbióza
s	s	k7c7	s
autotrofními	autotrofní	k2eAgInPc7d1	autotrofní
jednobuněčnými	jednobuněčný	k2eAgInPc7d1	jednobuněčný
organismy	organismus	k1gInPc7	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
kmenů	kmen	k1gInPc2	kmen
má	mít	k5eAaImIp3nS	mít
parazitické	parazitický	k2eAgMnPc4d1	parazitický
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
jsou	být	k5eAaImIp3nP	být
výhradně	výhradně	k6eAd1	výhradně
parazitické	parazitický	k2eAgFnPc1d1	parazitická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
živočišném	živočišný	k2eAgNnSc6d1	živočišné
těle	tělo	k1gNnSc6	tělo
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
specializaci	specializace	k1gFnSc3	specializace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
tkáně	tkáň	k1gFnPc1	tkáň
<g/>
,	,	kIx,	,
orgány	orgán	k1gInPc1	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Houbovci	houbovec	k1gInSc3	houbovec
(	(	kIx(	(
<g/>
Porifera	Porifera	k1gFnSc1	Porifera
<g/>
)	)	kIx)	)
a	a	k8xC	a
vločkovci	vločkovec	k1gMnPc1	vločkovec
(	(	kIx(	(
<g/>
Placozoa	Placozoa	k1gMnSc1	Placozoa
<g/>
)	)	kIx)	)
pravé	pravý	k2eAgFnSc2d1	pravá
tkáně	tkáň	k1gFnSc2	tkáň
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
schopné	schopný	k2eAgFnSc2d1	schopná
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
nejpůvodnější	původní	k2eAgFnPc1d3	nejpůvodnější
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
houbovců	houbovec	k1gInPc2	houbovec
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
aspektech	aspekt	k1gInPc6	aspekt
kolonie	kolonie	k1gFnSc2	kolonie
některých	některý	k3yIgFnPc2	některý
trubének	trubénka	k1gFnPc2	trubénka
<g/>
.	.	kIx.	.
</s>
<s>
Tkáně	tkáň	k1gFnPc1	tkáň
a	a	k8xC	a
orgány	orgán	k1gInPc1	orgán
živočichů	živočich	k1gMnPc2	živočich
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
přiřadit	přiřadit	k5eAaPmF	přiřadit
dvěma	dva	k4xCgInPc7	dva
základním	základní	k2eAgInPc3d1	základní
zárodečným	zárodečný	k2eAgInPc3d1	zárodečný
listům	list	k1gInPc3	list
-	-	kIx~	-
vnějšímu	vnější	k2eAgInSc3d1	vnější
ektodermu	ektoderm	k1gInSc3	ektoderm
a	a	k8xC	a
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
entodermu	entoderm	k1gInSc2	entoderm
<g/>
;	;	kIx,	;
u	u	k7c2	u
vývojově	vývojově	k6eAd1	vývojově
pokročilejších	pokročilý	k2eAgFnPc2d2	pokročilejší
bilaterií	bilaterie	k1gFnPc2	bilaterie
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
přibývá	přibývat	k5eAaImIp3nS	přibývat
střední	střední	k2eAgInSc4d1	střední
mezoderm	mezoderm	k1gInSc4	mezoderm
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
ektodermální	ektodermální	k2eAgInSc4d1	ektodermální
nebo	nebo	k8xC	nebo
entodermální	entodermální	k2eAgInSc4d1	entodermální
původ	původ	k1gInSc4	původ
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
další	další	k2eAgInSc4d1	další
zárodečný	zárodečný	k2eAgInSc4d1	zárodečný
list	list	k1gInSc4	list
obratlovců	obratlovec	k1gMnPc2	obratlovec
neurální	neurální	k2eAgFnSc4d1	neurální
lištu	lišta	k1gFnSc4	lišta
(	(	kIx(	(
<g/>
s	s	k7c7	s
ektodermálním	ektodermální	k2eAgInSc7d1	ektodermální
původem	původ	k1gInSc7	původ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
přes	přes	k7c4	přes
35	[number]	k4	35
kmenů	kmen	k1gInPc2	kmen
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
je	být	k5eAaImIp3nS	být
výhradně	výhradně	k6eAd1	výhradně
mořských	mořský	k2eAgInPc2d1	mořský
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
kmeny	kmen	k1gInPc4	kmen
kromě	kromě	k7c2	kromě
drápkovců	drápkovec	k1gMnPc2	drápkovec
(	(	kIx(	(
<g/>
Onychophora	Onychophora	k1gFnSc1	Onychophora
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
vodní	vodní	k2eAgMnPc4d1	vodní
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
drápkovců	drápkovec	k1gInPc2	drápkovec
<g/>
,	,	kIx,	,
však	však	k9	však
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
fosilních	fosilní	k2eAgInPc2d1	fosilní
záznamů	záznam	k1gInPc2	záznam
existují	existovat	k5eAaImIp3nP	existovat
trojlistí	trojlistý	k2eAgMnPc1d1	trojlistý
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
Bilateria	Bilaterium	k1gNnSc2	Bilaterium
<g/>
)	)	kIx)	)
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
již	již	k6eAd1	již
přinejmenším	přinejmenším	k6eAd1	přinejmenším
585	[number]	k4	585
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
prokázala	prokázat	k5eAaPmAgFnS	prokázat
příslušnost	příslušnost	k1gFnSc1	příslušnost
části	část	k1gFnSc2	část
ediakarské	ediakarský	k2eAgFnSc2d1	ediakarská
"	"	kIx"	"
<g/>
fauny	fauna	k1gFnSc2	fauna
<g/>
"	"	kIx"	"
k	k	k7c3	k
trojlistým	trojlistý	k2eAgMnPc3d1	trojlistý
živočichům	živočich	k1gMnPc3	živočich
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
fosilní	fosilní	k2eAgInPc1d1	fosilní
záznamy	záznam	k1gInPc1	záznam
staré	stará	k1gFnSc3	stará
přibližně	přibližně	k6eAd1	přibližně
610	[number]	k4	610
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dvojlistých	dvojlistý	k2eAgMnPc2d1	dvojlistý
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
s	s	k7c7	s
fosilizací	fosilizace	k1gFnSc7	fosilizace
(	(	kIx(	(
<g/>
neexistence	neexistence	k1gFnSc1	neexistence
pevných	pevný	k2eAgFnPc2d1	pevná
schránek	schránka	k1gFnPc2	schránka
<g/>
,	,	kIx,	,
neexistence	neexistence	k1gFnSc1	neexistence
stop	stop	k2eAgInSc2d1	stop
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
dně	dno	k1gNnSc6	dno
moří	mořit	k5eAaImIp3nS	mořit
<g/>
)	)	kIx)	)
a	a	k8xC	a
přiřazení	přiřazení	k1gNnSc1	přiřazení
fosílií	fosílie	k1gFnPc2	fosílie
je	být	k5eAaImIp3nS	být
spekulativnější	spekulativní	k2eAgFnSc1d2	spekulativnější
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
nejstarší	starý	k2eAgInSc1d3	nejstarší
známý	známý	k2eAgInSc1d1	známý
nález	nález	k1gInSc1	nález
fosilií	fosilie	k1gFnPc2	fosilie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
řazeny	řadit	k5eAaImNgFnP	řadit
k	k	k7c3	k
živočichům	živočich	k1gMnPc3	živočich
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
k	k	k7c3	k
primitivním	primitivní	k2eAgInPc3d1	primitivní
houbovcům	houbovec	k1gInPc3	houbovec
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
665	[number]	k4	665
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
starých	starý	k2eAgMnPc2d1	starý
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Klasifikace	klasifikace	k1gFnSc2	klasifikace
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
zpracován	zpracovat	k5eAaPmNgInS	zpracovat
podle	podle	k7c2	podle
knihy	kniha	k1gFnSc2	kniha
Jana	Jan	k1gMnSc2	Jan
Zrzavého	zrzavý	k2eAgMnSc2d1	zrzavý
Fylogeneze	fylogeneze	k1gFnPc4	fylogeneze
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
upraven	upravit	k5eAaPmNgMnS	upravit
podle	podle	k7c2	podle
pozdějších	pozdní	k2eAgInPc2d2	pozdější
objevů	objev	k1gInPc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Taxon	taxon	k1gInSc1	taxon
kmen	kmen	k1gInSc1	kmen
se	se	k3xPyFc4	se
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
přehledu	přehled	k1gInSc6	přehled
používá	používat	k5eAaImIp3nS	používat
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
tradičním	tradiční	k2eAgInSc6d1	tradiční
smyslu	smysl	k1gInSc6	smysl
<g/>
;	;	kIx,	;
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
kmen	kmen	k1gInSc4	kmen
považují	považovat	k5eAaImIp3nP	považovat
nadřazené	nadřazený	k2eAgFnPc1d1	nadřazená
úrovně	úroveň	k1gFnPc1	úroveň
<g/>
,	,	kIx,	,
uvedené	uvedený	k2eAgFnPc1d1	uvedená
bez	bez	k7c2	bez
taxonomického	taxonomický	k2eAgInSc2d1	taxonomický
ranku	rank	k1gInSc2	rank
<g/>
.	.	kIx.	.
</s>
<s>
Neuvádí	uvádět	k5eNaImIp3nS	uvádět
se	se	k3xPyFc4	se
kmen	kmen	k1gInSc1	kmen
Salinely	Salinela	k1gFnSc2	Salinela
(	(	kIx(	(
<g/>
Monoblastozoa	Monoblastozoa	k1gMnSc1	Monoblastozoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
považovaný	považovaný	k2eAgInSc1d1	považovaný
dnes	dnes	k6eAd1	dnes
za	za	k7c4	za
hypotetický	hypotetický	k2eAgInSc4d1	hypotetický
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jediného	jediný	k2eAgMnSc2d1	jediný
popsaného	popsaný	k2eAgMnSc2d1	popsaný
zástupce	zástupce	k1gMnSc2	zástupce
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
nikdy	nikdy	k6eAd1	nikdy
opakovaně	opakovaně	k6eAd1	opakovaně
nalézt	nalézt	k5eAaBmF	nalézt
<g/>
.	.	kIx.	.
</s>
<s>
Mnohobuněčné	mnohobuněčný	k2eAgMnPc4d1	mnohobuněčný
živočichy	živočich	k1gMnPc4	živočich
můžeme	moct	k5eAaImIp1nP	moct
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc4	skupina
-	-	kIx~	-
"	"	kIx"	"
<g/>
dvojlisté	dvojlistý	k2eAgNnSc1d1	dvojlistý
<g/>
"	"	kIx"	"
a	a	k8xC	a
trojlisté	trojlistý	k2eAgFnPc1d1	trojlistá
<g/>
.	.	kIx.	.
</s>
<s>
Trojlistí	trojlistý	k2eAgMnPc1d1	trojlistý
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
tvořeni	tvořen	k2eAgMnPc1d1	tvořen
živočichy	živočich	k1gMnPc7	živočich
s	s	k7c7	s
dvoustrannou	dvoustranný	k2eAgFnSc7d1	dvoustranná
symetrií	symetrie	k1gFnSc7	symetrie
a	a	k8xC	a
proto	proto	k8xC	proto
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nazýváni	nazývat	k5eAaImNgMnP	nazývat
spíše	spíše	k9	spíše
Bilateralia	Bilateralium	k1gNnSc2	Bilateralium
či	či	k8xC	či
Bilateria	Bilaterium	k1gNnSc2	Bilaterium
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
dvojlistých	dvojlistý	k2eAgMnPc2d1	dvojlistý
živočichů	živočich	k1gMnPc2	živočich
je	být	k5eAaImIp3nS	být
nepřirozená	přirozený	k2eNgFnSc1d1	nepřirozená
(	(	kIx(	(
<g/>
parafyletická	parafyletický	k2eAgFnSc1d1	parafyletická
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
různé	různý	k2eAgNnSc1d1	různé
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
odvětvující	odvětvující	k2eAgFnPc1d1	odvětvující
vývojové	vývojový	k2eAgFnPc1d1	vývojová
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
nemající	nemající	k1gMnSc1	nemající
společného	společný	k2eAgMnSc2d1	společný
předka	předek	k1gMnSc2	předek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
by	by	kYmCp3nS	by
zároveň	zároveň	k6eAd1	zároveň
nebyl	být	k5eNaImAgInS	být
předkem	předek	k1gMnSc7	předek
trojlistých	trojlistý	k2eAgMnPc2d1	trojlistý
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Dvojlistí	dvojlistý	k2eAgMnPc1d1	dvojlistý
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Diblastica	Diblastica	k1gMnSc1	Diblastica
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Diploblasta	Diploblasta	k1gFnSc1	Diploblasta
<g/>
,	,	kIx,	,
též	též	k9	též
Radiata	Radiat	k2eAgFnSc1d1	Radiat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
láčkovci	láčkovec	k1gMnPc1	láčkovec
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Žebernatky	žebernatka	k1gFnPc1	žebernatka
(	(	kIx(	(
<g/>
Ctenophora	Ctenophora	k1gFnSc1	Ctenophora
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Houbovci	houbovec	k1gInPc7	houbovec
(	(	kIx(	(
<g/>
Porifera	Porifera	k1gFnSc1	Porifera
<g/>
)	)	kIx)	)
Parahoxozoa	Parahoxozoa	k1gFnSc1	Parahoxozoa
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Vločkovci	Vločkovec	k1gMnPc1	Vločkovec
(	(	kIx(	(
<g/>
Placozoa	Placozoa	k1gMnSc1	Placozoa
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Žahavci	Žahavec	k1gInSc6	Žahavec
(	(	kIx(	(
<g/>
Cnidaria	Cnidarium	k1gNnSc2	Cnidarium
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
druhotně	druhotně	k6eAd1	druhotně
zjednodušených	zjednodušený	k2eAgFnPc2d1	zjednodušená
výtrusenek	výtrusenka	k1gFnPc2	výtrusenka
(	(	kIx(	(
<g/>
Myxozoa	Myxozoa	k1gFnSc1	Myxozoa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
považovaných	považovaný	k2eAgFnPc6d1	považovaná
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
kmen	kmen	k1gInSc4	kmen
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
+	+	kIx~	+
Bilateria	Bilaterium	k1gNnSc2	Bilaterium
/	/	kIx~	/
trojlistí	trojlistý	k2eAgMnPc1d1	trojlistý
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
))	))	k?	))
Dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
fylogeneticky	fylogeneticky	k6eAd1	fylogeneticky
nejbazálnější	bazální	k2eAgFnSc4d3	bazální
skupinu	skupina	k1gFnSc4	skupina
živočichů	živočich	k1gMnPc2	živočich
považováni	považován	k2eAgMnPc1d1	považován
houbovci	houbovec	k1gInPc7	houbovec
(	(	kIx(	(
<g/>
Porifera	Porifera	k1gFnSc1	Porifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
zvaní	zvaní	k1gNnSc1	zvaní
(	(	kIx(	(
<g/>
živočišné	živočišný	k2eAgFnSc2d1	živočišná
<g/>
)	)	kIx)	)
houby	houba	k1gFnSc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
primitivní	primitivní	k2eAgMnSc1d1	primitivní
stavbě	stavba	k1gFnSc6	stavba
jejich	jejich	k3xOp3gNnPc2	jejich
těl	tělo	k1gNnPc2	tělo
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgMnSc1d1	připomínající
často	často	k6eAd1	často
pouhou	pouhý	k2eAgFnSc4d1	pouhá
kolonii	kolonie	k1gFnSc4	kolonie
a	a	k8xC	a
nepravou	pravý	k2eNgFnSc4d1	nepravá
tkáň	tkáň	k1gFnSc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
skupiny	skupina	k1gFnPc1	skupina
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vločkovci	vločkovec	k1gInPc7	vločkovec
s	s	k7c7	s
nejistým	jistý	k2eNgNnSc7d1	nejisté
postavením	postavení	k1gNnSc7	postavení
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
řazených	řazený	k2eAgFnPc2d1	řazená
s	s	k7c7	s
houbovci	houbovec	k1gInPc7	houbovec
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Parazoa	Parazo	k1gInSc2	Parazo
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
skupinu	skupina	k1gFnSc4	skupina
a	a	k8xC	a
nazývány	nazýván	k2eAgInPc1d1	nazýván
tkáňovci	tkáňovec	k1gInPc7	tkáňovec
(	(	kIx(	(
<g/>
Eumetazoa	Eumetazo	k2eAgNnPc1d1	Eumetazo
či	či	k8xC	či
Histozoa	Histozo	k2eAgNnPc1d1	Histozo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
analýzy	analýza	k1gFnPc1	analýza
nejprve	nejprve	k6eAd1	nejprve
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
nepřirozenost	nepřirozenost	k1gFnSc4	nepřirozenost
houbovců	houbovec	k1gInPc2	houbovec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
revolučním	revoluční	k2eAgNnSc7d1	revoluční
zjištěním	zjištění	k1gNnSc7	zjištění
pak	pak	k6eAd1	pak
přišly	přijít	k5eAaPmAgFnP	přijít
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
po	po	k7c6	po
r.	r.	kA	r.
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
na	na	k7c6	na
základě	základ	k1gInSc6	základ
molekulární	molekulární	k2eAgFnSc2d1	molekulární
analýzy	analýza	k1gFnSc2	analýza
označily	označit	k5eAaPmAgFnP	označit
za	za	k7c4	za
nejbazálnější	bazální	k2eAgFnSc4d3	bazální
skupinu	skupina	k1gFnSc4	skupina
živočichů	živočich	k1gMnPc2	živočich
žebernatky	žebernatka	k1gFnSc2	žebernatka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
přirozenosti	přirozenost	k1gFnSc2	přirozenost
tkáňovců	tkáňovec	k1gMnPc2	tkáňovec
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
i	i	k9	i
studie	studie	k1gFnSc1	studie
potvrzující	potvrzující	k2eAgFnSc4d1	potvrzující
bazálnost	bazálnost	k1gFnSc4	bazálnost
houbovců	houbovec	k1gInPc2	houbovec
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
dokonce	dokonce	k9	dokonce
rehabilitovala	rehabilitovat	k5eAaBmAgFnS	rehabilitovat
jako	jako	k9	jako
přirozené	přirozený	k2eAgFnPc4d1	přirozená
celé	celý	k2eAgFnPc4d1	celá
"	"	kIx"	"
<g/>
dvojlisté	dvojlistý	k2eAgFnPc4d1	dvojlistý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnPc1d1	poslední
studie	studie	k1gFnPc1	studie
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
v	v	k7c6	v
prestižních	prestižní	k2eAgInPc6d1	prestižní
recenzovaných	recenzovaný	k2eAgInPc6d1	recenzovaný
odborných	odborný	k2eAgInPc6d1	odborný
časopisech	časopis	k1gInPc6	časopis
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
bazální	bazální	k2eAgNnSc4d1	bazální
postavení	postavení	k1gNnSc4	postavení
žebernatek	žebernatka	k1gFnPc2	žebernatka
<g/>
,	,	kIx,	,
ba	ba	k9	ba
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
i	i	k9	i
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
neurony	neuron	k1gInPc1	neuron
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
dokonce	dokonce	k9	dokonce
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
typicky	typicky	k6eAd1	typicky
mezodermální	mezodermální	k2eAgFnPc4d1	mezodermální
buňky	buňka	k1gFnPc4	buňka
(	(	kIx(	(
<g/>
svalové	svalový	k2eAgFnSc2d1	svalová
buňky	buňka	k1gFnSc2	buňka
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgInP	moct
mít	mít	k5eAaImF	mít
fylogenetický	fylogenetický	k2eAgInSc4d1	fylogenetický
základ	základ	k1gInSc4	základ
již	již	k9	již
u	u	k7c2	u
nejpůvodnějších	původní	k2eAgMnPc2d3	nejpůvodnější
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
houbovci	houbovec	k1gInPc7	houbovec
<g/>
,	,	kIx,	,
vločkovci	vločkovec	k1gMnPc7	vločkovec
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
druhotně	druhotně	k6eAd1	druhotně
ztraceny	ztratit	k5eAaPmNgFnP	ztratit
(	(	kIx(	(
<g/>
variantou	varianta	k1gFnSc7	varianta
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
vznik	vznik	k1gInSc1	vznik
jejich	jejich	k3xOp3gFnSc2	jejich
obdoby	obdoba	k1gFnSc2	obdoba
u	u	k7c2	u
žebernatek	žebernatka	k1gFnPc2	žebernatka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
dvojlistí	dvojlistý	k2eAgMnPc1d1	dvojlistý
<g/>
"	"	kIx"	"
uváděn	uváděn	k2eAgMnSc1d1	uváděn
v	v	k7c6	v
uvozovkách	uvozovka	k1gFnPc6	uvozovka
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
studie	studie	k1gFnPc1	studie
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
i	i	k9	i
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
žebernatkách	žebernatka	k1gFnPc6	žebernatka
se	se	k3xPyFc4	se
odštěpili	odštěpit	k5eAaPmAgMnP	odštěpit
houbovci	houbovec	k1gInSc3	houbovec
(	(	kIx(	(
<g/>
možná	možná	k9	možná
v	v	k7c6	v
několika	několik	k4yIc6	několik
větvích	větev	k1gFnPc6	větev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
vločkovci	vločkovec	k1gMnPc1	vločkovec
a	a	k8xC	a
až	až	k6eAd1	až
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
oddělily	oddělit	k5eAaPmAgFnP	oddělit
linie	linie	k1gFnPc1	linie
žahavců	žahavec	k1gMnPc2	žahavec
a	a	k8xC	a
bilaterií	bilaterie	k1gFnPc2	bilaterie
<g/>
.	.	kIx.	.
</s>
<s>
Trojlistí	trojlistý	k2eAgMnPc1d1	trojlistý
(	(	kIx(	(
<g/>
Bilateria	Bilaterium	k1gNnSc2	Bilaterium
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Bilateralia	Bilateralia	k1gFnSc1	Bilateralia
<g/>
,	,	kIx,	,
zast.	zast.	k?	zast.
Triblastica	Triblastica	k1gMnSc1	Triblastica
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Triploblastica	Triploblastica	k1gFnSc1	Triploblastica
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
velké	velká	k1gFnPc4	velká
linie	linie	k1gFnSc2	linie
-	-	kIx~	-
prvoústé	prvoústý	k2eAgNnSc1d1	prvoústý
(	(	kIx(	(
<g/>
Protostomia	Protostomia	k1gFnSc1	Protostomia
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhoústé	druhoústý	k2eAgNnSc1d1	druhoústý
(	(	kIx(	(
<g/>
Deuterostomia	Deuterostomia	k1gFnSc1	Deuterostomia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvoústí	Prvoúst	k1gFnSc7	Prvoúst
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
velké	velký	k2eAgFnPc4d1	velká
přirozené	přirozený	k2eAgFnPc4d1	přirozená
skupiny	skupina	k1gFnPc4	skupina
-	-	kIx~	-
Lophotrochozoa	Lophotrochozoa	k1gMnSc1	Lophotrochozoa
a	a	k8xC	a
Ecdysozoa	Ecdysozoa	k1gMnSc1	Ecdysozoa
<g/>
.	.	kIx.	.
</s>
<s>
Lophotrochozoa	Lophotrochozoa	k1gMnSc1	Lophotrochozoa
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Spiralia	Spiralia	k1gFnSc1	Spiralia
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Ploutvenky	Ploutvenka	k1gFnPc1	Ploutvenka
(	(	kIx(	(
<g/>
Chaetognatha	Chaetognatha	k1gFnSc1	Chaetognatha
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Lilijicovci	Lilijicovec	k1gMnPc1	Lilijicovec
(	(	kIx(	(
<g/>
Myzostomida	Myzostomida	k1gFnSc1	Myzostomida
<g/>
)	)	kIx)	)
Morulovci	Morulovec	k1gMnPc1	Morulovec
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
moruškovci	moruškovec	k1gMnPc1	moruškovec
(	(	kIx(	(
<g/>
Mesozoa	Mesozoa	k1gMnSc1	Mesozoa
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Sépiovky	Sépiovka	k1gFnPc1	Sépiovka
(	(	kIx(	(
<g/>
Rhombozoa	Rhombozoa	k1gFnSc1	Rhombozoa
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Plazmodiovky	Plazmodiovka	k1gFnPc1	Plazmodiovka
(	(	kIx(	(
<g/>
Orthonectida	Orthonectida	k1gFnSc1	Orthonectida
<g/>
)	)	kIx)	)
PlatyzoaRouphozoaKmen	PlatyzoaRouphozoaKmen	k1gInSc1	PlatyzoaRouphozoaKmen
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Ploštěnci	Ploštěnec	k1gMnPc1	Ploštěnec
(	(	kIx(	(
<g/>
Platyhelminthes	Platyhelminthes	k1gMnSc1	Platyhelminthes
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Břichobrvky	Břichobrvka	k1gMnSc2	Břichobrvka
(	(	kIx(	(
<g/>
Gastrotricha	Gastrotrich	k1gMnSc2	Gastrotrich
<g/>
)	)	kIx)	)
KamptozoaKmen	KamptozoaKmen	k1gInSc1	KamptozoaKmen
<g/>
:	:	kIx,	:
Mechovnatci	Mechovnatec	k1gMnPc1	Mechovnatec
(	(	kIx(	(
<g/>
Entoprocta	Entoprocta	k1gMnSc1	Entoprocta
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Vířníkovci	Vířníkovec	k1gMnPc1	Vířníkovec
(	(	kIx(	(
<g/>
Cycliophora	Cycliophora	k1gFnSc1	Cycliophora
<g/>
)	)	kIx)	)
Čelistovci	Čelistovec	k1gMnPc1	Čelistovec
(	(	kIx(	(
<g/>
Gnathifera	Gnathifera	k1gFnSc1	Gnathifera
<g/>
)	)	kIx)	)
<g/>
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Čelistovky	Čelistovka	k1gFnPc1	Čelistovka
(	(	kIx(	(
<g/>
Gnathostomulida	Gnathostomulida	k1gFnSc1	Gnathostomulida
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Oknozubky	Oknozubka	k1gFnPc4	Oknozubka
(	(	kIx(	(
<g/>
Micrognathozoa	Micrognathozo	k2eAgNnPc4d1	Micrognathozo
<g/>
)	)	kIx)	)
Syndermata	Synderma	k1gNnPc4	Synderma
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Vířníci	vířník	k1gMnPc1	vířník
(	(	kIx(	(
<g/>
Rotifera	Rotifera	k1gFnSc1	Rotifera
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Vrtejši	Vrtejš	k1gInPc7	Vrtejš
(	(	kIx(	(
<g/>
Acanthocephala	Acanthocephal	k1gMnSc2	Acanthocephal
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Žábrovci	Žábrovec	k1gMnPc1	Žábrovec
(	(	kIx(	(
<g/>
Seisonida	Seisonida	k1gFnSc1	Seisonida
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Pásnice	pásnice	k1gFnSc1	pásnice
(	(	kIx(	(
<g/>
Nemertini	Nemertin	k2eAgMnPc1d1	Nemertin
=	=	kIx~	=
Nemertea	Nemertea	k1gMnSc1	Nemertea
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Kroužkovci	kroužkovec	k1gMnPc1	kroužkovec
(	(	kIx(	(
<g/>
Annelida	Annelida	k1gFnSc1	Annelida
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
vláknonošců	vláknonošce	k1gMnPc2	vláknonošce
(	(	kIx(	(
<g/>
Pogonophora	Pogonophora	k1gFnSc1	Pogonophora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rypohlavců	rypohlavec	k1gMnPc2	rypohlavec
(	(	kIx(	(
<g/>
Echiura	Echiura	k1gFnSc1	Echiura
<g/>
)	)	kIx)	)
a	a	k8xC	a
sumýšovců	sumýšovec	k1gMnPc2	sumýšovec
(	(	kIx(	(
<g/>
Sipunculida	Sipunculida	k1gFnSc1	Sipunculida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
často	často	k6eAd1	často
považovaných	považovaný	k2eAgFnPc2d1	považovaná
za	za	k7c4	za
samostatné	samostatný	k2eAgInPc4d1	samostatný
kmeny	kmen	k1gInPc4	kmen
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc4	kmen
<g/>
:	:	kIx,	:
Měkkýši	měkkýš	k1gMnPc1	měkkýš
(	(	kIx(	(
<g/>
Mollusca	Mollusca	k1gMnSc1	Mollusca
<g/>
)	)	kIx)	)
Lophophorata	Lophophorata	k1gFnSc1	Lophophorata
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
chapadlovci	chapadlovec	k1gMnPc7	chapadlovec
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
bez	bez	k7c2	bez
Kamptozoa	Kamptozo	k1gInSc2	Kamptozo
<g/>
)	)	kIx)	)
<g/>
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Ramenonožci	ramenonožec	k1gMnPc1	ramenonožec
(	(	kIx(	(
<g/>
Brachiopoda	Brachiopoda	k1gMnSc1	Brachiopoda
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Mechovci	Mechovec	k1gMnPc1	Mechovec
(	(	kIx(	(
<g/>
Ectoprocta	Ectoprocta	k1gMnSc1	Ectoprocta
=	=	kIx~	=
Bryozoa	Bryozoa	k1gMnSc1	Bryozoa
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Chapadlovky	Chapadlovka	k1gFnPc1	Chapadlovka
(	(	kIx(	(
<g/>
Phoronida	Phoronida	k1gFnSc1	Phoronida
<g/>
)	)	kIx)	)
Ecdysozoa	Ecdysozo	k2eAgFnSc1d1	Ecdysozo
Chobotovci	Chobotovec	k1gInPc7	Chobotovec
(	(	kIx(	(
<g/>
Scalidophora	Scalidophora	k1gFnSc1	Scalidophora
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Hlavatci	hlavatec	k1gInPc7	hlavatec
(	(	kIx(	(
<g/>
Priapulida	Priapulida	k1gFnSc1	Priapulida
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Rypečky	rypeček	k1gInPc1	rypeček
(	(	kIx(	(
<g/>
Kinorhyncha	Kinorhyncha	k1gFnSc1	Kinorhyncha
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Korzetky	Korzetek	k1gInPc1	Korzetek
(	(	kIx(	(
<g/>
Loricifera	Loricifera	k1gFnSc1	Loricifera
<g/>
)	)	kIx)	)
Nematoida	Nematoida	k1gFnSc1	Nematoida
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Strunovci	strunovec	k1gMnPc1	strunovec
(	(	kIx(	(
<g/>
Nematomorpha	Nematomorpha	k1gMnSc1	Nematomorpha
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Hlístice	hlístice	k1gFnSc1	hlístice
(	(	kIx(	(
<g/>
Nematoda	Nematoda	k1gFnSc1	Nematoda
<g/>
)	)	kIx)	)
Panarthropoda	Panarthropoda	k1gFnSc1	Panarthropoda
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Drápkovci	Drápkovec	k1gMnPc1	Drápkovec
(	(	kIx(	(
<g/>
Onychophora	Onychophora	k1gFnSc1	Onychophora
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Želvušky	Želvušek	k1gInPc1	Želvušek
(	(	kIx(	(
<g/>
Tardigrada	Tardigrada	k1gFnSc1	Tardigrada
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Členovci	členovec	k1gMnPc1	členovec
(	(	kIx(	(
<g/>
Arthropoda	Arthropoda	k1gMnSc1	Arthropoda
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
všech	všecek	k3xTgMnPc2	všecek
druhoústých	druhoústý	k2eAgMnPc2d1	druhoústý
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
Deuterostomia	Deuterostomia	k1gFnSc1	Deuterostomia
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
strunatci	strunatec	k1gMnPc1	strunatec
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
tvoří	tvořit	k5eAaImIp3nP	tvořit
linie	linie	k1gFnPc1	linie
vedoucí	vedoucí	k1gFnSc2	vedoucí
k	k	k7c3	k
praploštěncům	praploštěnec	k1gMnPc3	praploštěnec
a	a	k8xC	a
mlžojedům	mlžojed	k1gMnPc3	mlžojed
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
sesterská	sesterský	k2eAgFnSc1d1	sesterská
skupina	skupina	k1gFnSc1	skupina
Ambulacraria	Ambulacrarium	k1gNnSc2	Ambulacrarium
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
polostrunatce	polostrunatec	k1gMnSc4	polostrunatec
a	a	k8xC	a
ostnokožce	ostnokožec	k1gMnSc4	ostnokožec
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Strunatci	strunatec	k1gMnPc1	strunatec
(	(	kIx(	(
<g/>
Chordata	Chordata	k1gFnSc1	Chordata
<g/>
)	)	kIx)	)
Podkmen	podkmen	k1gInSc1	podkmen
<g/>
:	:	kIx,	:
Pláštěnci	pláštěnec	k1gMnPc1	pláštěnec
(	(	kIx(	(
<g/>
Urochordata	Urochordat	k2eAgFnSc1d1	Urochordat
=	=	kIx~	=
Tunicata	Tunicata	k1gFnSc1	Tunicata
<g/>
)	)	kIx)	)
Podkmen	podkmen	k1gInSc1	podkmen
<g/>
:	:	kIx,	:
Bezlebeční	bezlebeční	k2eAgNnPc1d1	bezlebeční
(	(	kIx(	(
<g/>
Acrania	Acranium	k1gNnPc1	Acranium
=	=	kIx~	=
Cephalochordata	Cephalochordata	k1gFnSc1	Cephalochordata
<g/>
)	)	kIx)	)
Podkmen	podkmen	k1gInSc1	podkmen
<g/>
:	:	kIx,	:
Obratlovci	obratlovec	k1gMnPc1	obratlovec
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
lebečnatí	lebečnatí	k1gMnPc5	lebečnatí
(	(	kIx(	(
<g/>
Craniata	Craniata	k1gFnSc1	Craniata
<g/>
)	)	kIx)	)
XenacoelomorphaKmen	XenacoelomorphaKmen	k1gInSc1	XenacoelomorphaKmen
<g/>
:	:	kIx,	:
Mlžojedi	Mlžojed	k1gMnPc1	Mlžojed
(	(	kIx(	(
<g/>
Xenoturbellida	Xenoturbellida	k1gFnSc1	Xenoturbellida
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Praploštěnci	Praploštěnec	k1gMnPc1	Praploštěnec
(	(	kIx(	(
<g/>
Acoelomorpha	Acoelomorpha	k1gMnSc1	Acoelomorpha
<g/>
)	)	kIx)	)
Ambulacraria	Ambulacrarium	k1gNnSc2	Ambulacrarium
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Ostnokožci	ostnokožec	k1gMnPc1	ostnokožec
(	(	kIx(	(
<g/>
Echinodermata	Echinoderma	k1gNnPc1	Echinoderma
<g/>
)	)	kIx)	)
Kmen	kmen	k1gInSc1	kmen
<g/>
:	:	kIx,	:
Polostrunatci	Polostrunatec	k1gMnPc1	Polostrunatec
(	(	kIx(	(
<g/>
Hemichordata	Hemichordata	k1gFnSc1	Hemichordata
<g/>
)	)	kIx)	)
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
kategorizován	kategorizován	k2eAgMnSc1d1	kategorizován
jako	jako	k8xC	jako
živočich	živočich	k1gMnSc1	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
jako	jako	k8xC	jako
sebeuvědomění	sebeuvědomění	k1gNnSc4	sebeuvědomění
či	či	k8xC	či
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
vcítit	vcítit	k5eAaPmF	vcítit
nejsou	být	k5eNaImIp3nP	být
totiž	totiž	k9	totiž
výhradně	výhradně	k6eAd1	výhradně
lidské	lidský	k2eAgFnPc4d1	lidská
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgMnPc1d1	další
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
rituály	rituál	k1gInPc4	rituál
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
živočich	živočich	k1gMnSc1	živočich
je	být	k5eAaImIp3nS	být
užívané	užívaný	k2eAgInPc1d1	užívaný
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
mluvě	mluva	k1gFnSc6	mluva
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
živočichy	živočich	k1gMnPc4	živočich
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
užívá	užívat	k5eAaImIp3nS	užívat
slovo	slovo	k1gNnSc4	slovo
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zvířátko	zvířátko	k1gNnSc1	zvířátko
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hromadné	hromadný	k2eAgNnSc1d1	hromadné
jméno	jméno	k1gNnSc1	jméno
pro	pro	k7c4	pro
zvířectvo	zvířectvo	k1gNnSc4	zvířectvo
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
též	též	k9	též
pojem	pojem	k1gInSc1	pojem
fauna	faun	k1gMnSc2	faun
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
není	být	k5eNaImIp3nS	být
ostře	ostro	k6eAd1	ostro
vymezen	vymezit	k5eAaPmNgInS	vymezit
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
označují	označovat	k5eAaImIp3nP	označovat
větší	veliký	k2eAgMnPc1d2	veliký
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
obratlovci	obratlovec	k1gMnPc1	obratlovec
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jím	on	k3xPp3gNnSc7	on
bývá	bývat	k5eAaImIp3nS	bývat
označen	označit	k5eAaPmNgInS	označit
i	i	k9	i
například	například	k6eAd1	například
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
větších	veliký	k2eAgMnPc2d2	veliký
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ptáků	pták	k1gMnPc2	pták
žijící	žijící	k2eAgInPc1d1	žijící
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
také	také	k9	také
slovem	slovem	k6eAd1	slovem
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
biologického	biologický	k2eAgInSc2d1	biologický
popisu	popis	k1gInSc2	popis
a	a	k8xC	a
třídění	třídění	k1gNnSc1	třídění
jsou	být	k5eAaImIp3nP	být
živočichové	živočich	k1gMnPc1	živočich
pojmenováváni	pojmenovávat	k5eAaImNgMnP	pojmenovávat
a	a	k8xC	a
popisováni	popisovat	k5eAaImNgMnP	popisovat
také	také	k6eAd1	také
podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
nebo	nebo	k8xC	nebo
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
svých	svůj	k3xOyFgInPc2	svůj
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
způsobů	způsob	k1gInPc2	způsob
využívání	využívání	k1gNnSc2	využívání
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
ovšem	ovšem	k9	ovšem
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
takováto	takovýto	k3xDgNnPc4	takovýto
hodnocení	hodnocení	k1gNnPc4	hodnocení
zpravidla	zpravidla	k6eAd1	zpravidla
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
objektivní	objektivní	k2eAgFnSc4d1	objektivní
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
platná	platný	k2eAgFnSc1d1	platná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
v	v	k7c6	v
čase	čas	k1gInSc6	čas
i	i	k8xC	i
prostoru	prostor	k1gInSc6	prostor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
co	co	k3yInSc1	co
někde	někde	k6eAd1	někde
je	být	k5eAaImIp3nS	být
zvíře	zvíře	k1gNnSc1	zvíře
užitečné	užitečný	k2eAgNnSc1d1	užitečné
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jinde	jinde	k6eAd1	jinde
škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
<g/>
,	,	kIx,	,
totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
jedno	jeden	k4xCgNnSc4	jeden
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časech	čas	k1gInPc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
úhlu	úhel	k1gInSc6	úhel
pohledu	pohled	k1gInSc2	pohled
-	-	kIx~	-
zvíře	zvíře	k1gNnSc1	zvíře
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
užitečné	užitečný	k2eAgInPc1d1	užitečný
<g/>
,	,	kIx,	,
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
<g/>
,	,	kIx,	,
chovné	chovný	k2eAgInPc1d1	chovný
i	i	k8xC	i
neužitkové	užitkový	k2eNgInPc1d1	užitkový
(	(	kIx(	(
<g/>
např.	např.	kA	např.
psa	pes	k1gMnSc2	pes
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
fakticky	fakticky	k6eAd1	fakticky
i	i	k9	i
dle	dle	k7c2	dle
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
posuzovatele	posuzovatel	k1gMnSc2	posuzovatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ledňáček	ledňáček	k1gMnSc1	ledňáček
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
nepřítele	nepřítel	k1gMnSc2	nepřítel
plevelných	plevelný	k2eAgFnPc2d1	plevelná
ryb	ryba	k1gFnPc2	ryba
-	-	kIx~	-
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
užitečný	užitečný	k2eAgMnSc1d1	užitečný
<g/>
,	,	kIx,	,
i	i	k8xC	i
za	za	k7c2	za
příležitostného	příležitostný	k2eAgMnSc2d1	příležitostný
vykradače	vykradač	k1gMnSc2	vykradač
sádek	sádka	k1gFnPc2	sádka
-	-	kIx~	-
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Medvěd	medvěd	k1gMnSc1	medvěd
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
shledán	shledat	k5eAaPmNgMnS	shledat
efektivním	efektivní	k2eAgMnSc7d1	efektivní
likvidátorem	likvidátor	k1gMnSc7	likvidátor
mršin	mršina	k1gFnPc2	mršina
(	(	kIx(	(
<g/>
užitečný	užitečný	k2eAgInSc1d1	užitečný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
nebezpečnou	bezpečný	k2eNgFnSc7d1	nebezpečná
šelmou	šelma	k1gFnSc7	šelma
(	(	kIx(	(
<g/>
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
)	)	kIx)	)
a	a	k8xC	a
vykradačem	vykradač	k1gMnSc7	vykradač
úlů	úl	k1gInPc2	úl
(	(	kIx(	(
<g/>
škodlivý	škodlivý	k2eAgInSc4d1	škodlivý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dělení	dělení	k1gNnSc1	dělení
tedy	tedy	k9	tedy
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
lidské	lidský	k2eAgFnSc2d1	lidská
užitečnosti	užitečnost	k1gFnSc2	užitečnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
používáno	používán	k2eAgNnSc1d1	používáno
především	především	k9	především
myslivci	myslivec	k1gMnPc1	myslivec
<g/>
,	,	kIx,	,
rybáři	rybář	k1gMnPc1	rybář
<g/>
,	,	kIx,	,
zemědělci	zemědělec	k1gMnPc1	zemědělec
apod.	apod.	kA	apod.
Nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
zvířata	zvíře	k1gNnPc1	zvíře
-	-	kIx~	-
zvířata	zvíře	k1gNnPc4	zvíře
ohrožující	ohrožující	k2eAgNnPc4d1	ohrožující
přímo	přímo	k6eAd1	přímo
život	život	k1gInSc4	život
nebo	nebo	k8xC	nebo
zdraví	zdravit	k5eAaImIp3nS	zdravit
člověku	člověk	k1gMnSc3	člověk
nebezpečné	bezpečný	k2eNgFnSc2d1	nebezpečná
šelmy	šelma	k1gFnSc2	šelma
(	(	kIx(	(
<g/>
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
tygr	tygr	k1gMnSc1	tygr
<g/>
,	,	kIx,	,
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
vlk	vlk	k1gMnSc1	vlk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g />
.	.	kIx.	.
</s>
<s>
zvířata	zvíře	k1gNnPc4	zvíře
(	(	kIx(	(
<g/>
např.	např.	kA	např.
někteří	některý	k3yIgMnPc1	některý
hadi	had	k1gMnPc1	had
<g/>
,	,	kIx,	,
žahavci	žahavec	k1gMnPc1	žahavec
<g/>
,	,	kIx,	,
bodavý	bodavý	k2eAgInSc4d1	bodavý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
Škodlivá	škodlivý	k2eAgNnPc1d1	škodlivé
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
škůdci	škůdce	k1gMnPc5	škůdce
-	-	kIx~	-
zvířata	zvíře	k1gNnPc1	zvíře
ohrožující	ohrožující	k2eAgFnSc2d1	ohrožující
jiné	jiná	k1gFnSc2	jiná
lidské	lidský	k2eAgInPc1d1	lidský
zájmy	zájem	k1gInPc1	zájem
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zájmy	zájem	k1gInPc4	zájem
hospodářské	hospodářský	k2eAgInPc4d1	hospodářský
(	(	kIx(	(
<g/>
snižují	snižovat	k5eAaImIp3nP	snižovat
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
<g/>
,	,	kIx,	,
lesnické	lesnický	k2eAgInPc4d1	lesnický
a	a	k8xC	a
rybářské	rybářský	k2eAgInPc4d1	rybářský
výnosy	výnos	k1gInPc4	výnos
nebo	nebo	k8xC	nebo
ničí	ničí	k3xOyNgFnPc4	ničí
lidské	lidský	k2eAgFnPc4d1	lidská
zásoby	zásoba	k1gFnPc4	zásoba
či	či	k8xC	či
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ekologické	ekologický	k2eAgNnSc4d1	ekologické
<g/>
,	,	kIx,	,
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
sama	sám	k3xTgNnPc1	sám
nejsou	být	k5eNaImIp3nP	být
nebezpečná	bezpečný	k2eNgNnPc1d1	nebezpečné
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
šířit	šířit	k5eAaImF	šířit
choroby	choroba	k1gFnPc4	choroba
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
užitkových	užitkový	k2eAgNnPc2d1	užitkové
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
saranče	saranče	k1gNnSc1	saranče
<g/>
,	,	kIx,	,
plzák	plzák	k1gMnSc1	plzák
<g/>
,	,	kIx,	,
kůrovec	kůrovec	k1gMnSc1	kůrovec
<g/>
,	,	kIx,	,
kormorán	kormorán	k1gMnSc1	kormorán
<g/>
,	,	kIx,	,
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
bobr	bobr	k1gMnSc1	bobr
<g/>
,	,	kIx,	,
termiti	termit	k1gMnPc1	termit
<g/>
,	,	kIx,	,
červotoč	červotoč	k1gMnSc1	červotoč
<g/>
,	,	kIx,	,
invazní	invazní	k2eAgInPc1d1	invazní
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
krysa	krysa	k1gFnSc1	krysa
<g/>
,	,	kIx,	,
klíště	klíště	k1gNnSc1	klíště
<g/>
,	,	kIx,	,
komár	komár	k1gMnSc1	komár
Užitečná	užitečný	k2eAgNnPc1d1	užitečné
zvířata	zvíře	k1gNnPc1	zvíře
-	-	kIx~	-
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
naopak	naopak	k6eAd1	naopak
jiné	jiný	k2eAgInPc1d1	jiný
lidské	lidský	k2eAgInPc1d1	lidský
zájmy	zájem	k1gInPc1	zájem
(	(	kIx(	(
<g/>
hospodářské	hospodářský	k2eAgFnPc1d1	hospodářská
<g/>
,	,	kIx,	,
ekologické	ekologický	k2eAgFnPc1d1	ekologická
<g/>
,	,	kIx,	,
emocionální	emocionální	k2eAgFnPc1d1	emocionální
<g/>
)	)	kIx)	)
podporují	podporovat	k5eAaImIp3nP	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ekologických	ekologický	k2eAgInPc2d1	ekologický
zájmů	zájem	k1gInPc2	zájem
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
např.	např.	kA	např.
zvířata	zvíře	k1gNnPc4	zvíře
podílející	podílející	k2eAgFnPc1d1	podílející
se	se	k3xPyFc4	se
na	na	k7c4	na
redukci	redukce	k1gFnSc4	redukce
škodlivých	škodlivý	k2eAgMnPc2d1	škodlivý
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slunéčko	slunéčko	k1gNnSc1	slunéčko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významnými	významný	k2eAgFnPc7d1	významná
skupinami	skupina	k1gFnPc7	skupina
užitečných	užitečný	k2eAgNnPc2d1	užitečné
zvířat	zvíře	k1gNnPc2	zvíře
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Užitková	užitkový	k2eAgNnPc1d1	užitkové
zvířata	zvíře	k1gNnPc1	zvíře
-	-	kIx~	-
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
něž	jenž	k3xRgInPc4	jenž
má	mít	k5eAaImIp3nS	mít
člověk	člověk	k1gMnSc1	člověk
nějaké	nějaký	k3yIgFnSc2	nějaký
záměrné	záměrná	k1gFnSc2	záměrná
využití	využití	k1gNnSc2	využití
<g/>
:	:	kIx,	:
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
-	-	kIx~	-
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
živočišných	živočišný	k2eAgInPc2d1	živočišný
produktů	produkt	k1gInPc2	produkt
(	(	kIx(	(
<g/>
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
med	med	k1gInSc1	med
<g/>
,	,	kIx,	,
hedvábí	hedvábí	k1gNnSc1	hedvábí
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
využití	využití	k1gNnSc4	využití
jejich	jejich	k3xOp3gFnSc2	jejich
síly	síla	k1gFnSc2	síla
(	(	kIx(	(
<g/>
tažná	tažný	k2eAgNnPc4d1	tažné
a	a	k8xC	a
jezdecká	jezdecký	k2eAgNnPc4d1	jezdecké
zvířata	zvíře	k1gNnPc4	zvíře
-	-	kIx~	-
kůň	kůň	k1gMnSc1	kůň
<g/>
,	,	kIx,	,
tur	tur	k1gMnSc1	tur
domácí	domácí	k1gMnSc1	domácí
<g/>
,	,	kIx,	,
osel	osel	k1gMnSc1	osel
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
sob	sob	k1gMnSc1	sob
<g/>
,	,	kIx,	,
velbloud	velbloud	k1gMnSc1	velbloud
<g/>
)	)	kIx)	)
užitková	užitkový	k2eAgNnPc1d1	užitkové
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hlídací	hlídací	k2eAgFnPc1d1	hlídací
a	a	k8xC	a
lovečtí	lovecký	k2eAgMnPc1d1	lovecký
psi	pes	k1gMnPc1	pes
<g/>
)	)	kIx)	)
laboratorní	laboratorní	k2eAgNnPc1d1	laboratorní
zvířata	zvíře	k1gNnPc1	zvíře
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
např.	např.	kA	např.
laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
myš	myš	k1gFnSc1	myš
<g/>
,	,	kIx,	,
králíci	králík	k1gMnPc1	králík
<g/>
,	,	kIx,	,
potkani	potkan	k1gMnPc1	potkan
<g/>
)	)	kIx)	)
zvířata	zvíře	k1gNnPc1	zvíře
využívaná	využívaný	k2eAgNnPc1d1	využívané
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pijavice	pijavice	k1gFnSc1	pijavice
<g/>
,	,	kIx,	,
bzučivka	bzučivka	k1gFnSc1	bzučivka
<g/>
,	,	kIx,	,
kůň	kůň	k1gMnSc1	kůň
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
Neužitková	užitkový	k2eNgNnPc1d1	užitkový
domácí	domácí	k2eAgNnPc1d1	domácí
zvířata	zvíře	k1gNnPc1	zvíře
-	-	kIx~	-
zvířata	zvíře	k1gNnPc4	zvíře
chovaná	chovaný	k2eAgNnPc4d1	chované
zejména	zejména	k9	zejména
k	k	k7c3	k
uspokojení	uspokojení	k1gNnSc3	uspokojení
emocionálních	emocionální	k2eAgFnPc2d1	emocionální
potřeb	potřeba	k1gFnPc2	potřeba
člověka	člověk	k1gMnSc4	člověk
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
papoušek	papoušek	k1gMnSc1	papoušek
<g/>
,	,	kIx,	,
akvarijní	akvarijní	k2eAgMnSc1d1	akvarijní
<g />
.	.	kIx.	.
</s>
<s>
ryby	ryba	k1gFnPc1	ryba
apod.	apod.	kA	apod.
Chovná	chovný	k2eAgNnPc4d1	chovné
zvířata	zvíře	k1gNnPc4	zvíře
-	-	kIx~	-
společné	společný	k2eAgNnSc4d1	společné
označení	označení	k1gNnSc4	označení
plemen	plemeno	k1gNnPc2	plemeno
užitkových	užitkový	k2eAgNnPc2d1	užitkové
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
neužitkových	užitkový	k2eNgMnPc2d1	užitkový
domácích	domácí	k1gMnPc2	domácí
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
určených	určený	k2eAgInPc2d1	určený
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
a	a	k8xC	a
šlechtění	šlechtění	k1gNnSc4	šlechtění
Jonathan	Jonathan	k1gMnSc1	Jonathan
Elphick	Elphick	k1gMnSc1	Elphick
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
Slovart	Slovarta	k1gFnPc2	Slovarta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7209-329-0	[number]	k4	80-7209-329-0
Eukaryota	Eukaryota	k1gFnSc1	Eukaryota
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
živočichové	živočich	k1gMnPc1	živočich
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
živočich	živočich	k1gMnSc1	živočich
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Animalia	Animalium	k1gNnSc2	Animalium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
