<s>
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
Granville	Granville	k1gFnSc1	Granville
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Montecatini	Montecatin	k2eAgMnPc1d1	Montecatin
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgMnPc2d3	nejvlivnější
francouzských	francouzský	k2eAgMnPc2d1	francouzský
i	i	k8xC	i
světových	světový	k2eAgMnPc2d1	světový
módních	módní	k2eAgMnPc2d1	módní
návrhářů	návrhář	k1gMnPc2	návrhář
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Normandii	Normandie	k1gFnSc6	Normandie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Granville	Granville	k1gFnSc2	Granville
<g/>
,	,	kIx,	,
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
hnojivy	hnojivo	k1gNnPc7	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
diplomacii	diplomacie	k1gFnSc4	diplomacie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
živit	živit	k5eAaImF	živit
jako	jako	k9	jako
galerista	galerista	k1gMnSc1	galerista
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
textilního	textilní	k2eAgMnSc4d1	textilní
magnáta	magnát	k1gMnSc4	magnát
Marcela	Marcel	k1gMnSc4	Marcel
BOUSSAC	BOUSSAC	kA	BOUSSAC
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
svůj	svůj	k3xOyFgInSc4	svůj
módní	módní	k2eAgInSc4d1	módní
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
12	[number]	k4	12
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
rozšířit	rozšířit	k5eAaPmF	rozšířit
své	svůj	k3xOyFgFnPc4	svůj
podnikatelské	podnikatelský	k2eAgFnPc4d1	podnikatelská
aktivity	aktivita	k1gFnPc4	aktivita
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
15	[number]	k4	15
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dior	Dior	k1gInSc1	Dior
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
známý	známý	k2eAgMnSc1d1	známý
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
"	"	kIx"	"
<g/>
New	New	k1gMnSc1	New
Look	Look	k1gMnSc1	Look
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
módních	módní	k2eAgFnPc6d1	módní
kreacích	kreace	k1gFnPc6	kreace
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
ženskost	ženskost	k1gFnSc1	ženskost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
šaty	šat	k1gInPc1	šat
měly	mít	k5eAaImAgInP	mít
úzká	úzký	k2eAgNnPc4d1	úzké
ramena	rameno	k1gNnPc4	rameno
<g/>
,	,	kIx,	,
zúžený	zúžený	k2eAgInSc4d1	zúžený
pas	pas	k1gInSc4	pas
<g/>
,	,	kIx,	,
zdůrazněná	zdůrazněný	k2eAgNnPc4d1	zdůrazněné
prsa	prsa	k1gNnPc4	prsa
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
<g/>
,	,	kIx,	,
širokou	široký	k2eAgFnSc4d1	široká
sukni	sukně	k1gFnSc4	sukně
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
Look	Look	k1gInSc1	Look
znamenal	znamenat	k5eAaImAgInS	znamenat
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
dámském	dámský	k2eAgNnSc6d1	dámské
odívání	odívání	k1gNnSc6	odívání
a	a	k8xC	a
opět	opět	k6eAd1	opět
udělal	udělat	k5eAaPmAgMnS	udělat
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
centrum	centrum	k1gNnSc4	centrum
světové	světový	k2eAgFnSc2d1	světová
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
eleganci	elegance	k1gFnSc4	elegance
<g/>
,	,	kIx,	,
nastiňující	nastiňující	k2eAgFnSc4d1	nastiňující
ženskost	ženskost	k1gFnSc4	ženskost
a	a	k8xC	a
krásu	krása	k1gFnSc4	krása
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
Diora	Dior	k1gMnSc2	Dior
zaměstnaný	zaměstnaný	k2eAgMnSc1d1	zaměstnaný
mladý	mladý	k2eAgMnSc1d1	mladý
Francouz	Francouz	k1gMnSc1	Francouz
Yves	Yvesa	k1gFnPc2	Yvesa
Saint	Saint	k1gMnSc1	Saint
Laurent	Laurent	k1gMnSc1	Laurent
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc1	jeho
asistent	asistent	k1gMnSc1	asistent
<g/>
.	.	kIx.	.
</s>
<s>
Diorův	Diorův	k2eAgInSc1d1	Diorův
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
hektickým	hektický	k2eAgInSc7d1	hektický
životním	životní	k2eAgInSc7d1	životní
stylem	styl	k1gInSc7	styl
<g/>
,	,	kIx,	,
neustálými	neustálý	k2eAgFnPc7d1	neustálá
hádkami	hádka	k1gFnPc7	hádka
a	a	k8xC	a
konflikty	konflikt	k1gInPc4	konflikt
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
homosexualitu	homosexualita	k1gFnSc4	homosexualita
<g/>
.	.	kIx.	.
</s>
<s>
Špatné	špatný	k2eAgInPc4d1	špatný
vztahy	vztah	k1gInPc4	vztah
měl	mít	k5eAaImAgMnS	mít
zejména	zejména	k9	zejména
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Raymondem	Raymond	k1gMnSc7	Raymond
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gInSc4	on
již	již	k9	již
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
tyranizoval	tyranizovat	k5eAaImAgMnS	tyranizovat
zavíráním	zavírání	k1gNnPc3	zavírání
do	do	k7c2	do
tmavého	tmavý	k2eAgInSc2d1	tmavý
sklepa	sklep	k1gInSc2	sklep
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
svou	svůj	k3xOyFgFnSc4	svůj
neteř	neteř	k1gFnSc4	neteř
<g/>
,	,	kIx,	,
Raymondovu	Raymondův	k2eAgFnSc4d1	Raymondova
dceru	dcera	k1gFnSc4	dcera
Françoise	Françoise	k1gFnSc2	Françoise
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
nadšená	nadšený	k2eAgFnSc1d1	nadšená
přívrženkyně	přívrženkyně	k1gFnSc1	přívrženkyně
nacismu	nacismus	k1gInSc2	nacismus
a	a	k8xC	a
obviňovala	obviňovat	k5eAaImAgFnS	obviňovat
Diorovy	Diorův	k2eAgInPc4d1	Diorův
židovské	židovský	k2eAgInPc4d1	židovský
manažery	manažer	k1gInPc4	manažer
ze	z	k7c2	z
spiknutí	spiknutí	k1gNnSc2	spiknutí
proti	proti	k7c3	proti
jejímu	její	k3xOp3gMnSc3	její
strýci	strýc	k1gMnSc3	strýc
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
francouzského	francouzský	k2eAgNnSc2d1	francouzské
dědického	dědický	k2eAgNnSc2d1	dědické
práva	právo	k1gNnSc2	právo
měl	mít	k5eAaImAgInS	mít
po	po	k7c6	po
Diorově	Diorův	k2eAgFnSc6d1	Diorova
smrti	smrt	k1gFnSc6	smrt
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
neměl	mít	k5eNaImAgMnS	mít
děti	dítě	k1gFnPc4	dítě
ani	ani	k8xC	ani
žijící	žijící	k2eAgMnPc1d1	žijící
rodiče	rodič	k1gMnPc1	rodič
<g/>
)	)	kIx)	)
jeho	jeho	k3xOp3gInSc4	jeho
majetek	majetek	k1gInSc4	majetek
dědit	dědit	k5eAaImF	dědit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
hádkách	hádka	k1gFnPc6	hádka
s	s	k7c7	s
Raymondem	Raymond	k1gInSc7	Raymond
i	i	k9	i
Françoise	Françoise	k1gFnPc1	Françoise
se	se	k3xPyFc4	se
Dior	Dior	k1gInSc1	Dior
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
vydědit	vydědit	k5eAaPmF	vydědit
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
dovolené	dovolená	k1gFnSc6	dovolená
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
dvaapadesáti	dvaapadesát	k4xCc6	dvaapadesát
letech	let	k1gInPc6	let
na	na	k7c4	na
srdeční	srdeční	k2eAgNnSc4d1	srdeční
selhání	selhání	k1gNnSc4	selhání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
Yves	Yves	k1gInSc1	Yves
Saint	Sainta	k1gFnPc2	Sainta
Laurent	Laurent	k1gMnSc1	Laurent
stal	stát	k5eAaPmAgMnS	stát
uměleckým	umělecký	k2eAgMnSc7d1	umělecký
ředitelem	ředitel	k1gMnSc7	ředitel
jeho	on	k3xPp3gInSc2	on
módního	módní	k2eAgInSc2d1	módní
domu	dům	k1gInSc2	dům
a	a	k8xC	a
zachránil	zachránit	k5eAaPmAgMnS	zachránit
ho	on	k3xPp3gMnSc4	on
před	před	k7c7	před
krachem	krach	k1gInSc7	krach
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Christiana	Christian	k1gMnSc2	Christian
Diora	Dior	k1gMnSc2	Dior
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
společností	společnost	k1gFnPc2	společnost
vyrábějících	vyrábějící	k2eAgInPc2d1	vyrábějící
módní	módní	k2eAgNnSc4d1	módní
oblečení	oblečení	k1gNnSc4	oblečení
-	-	kIx~	-
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
S.	S.	kA	S.
A.	A.	kA	A.
Společnost	společnost	k1gFnSc1	společnost
vlastní	vlastní	k2eAgFnSc4d1	vlastní
divizi	divize	k1gFnSc4	divize
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
Couture	Coutur	k1gMnSc5	Coutur
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
jedny	jeden	k4xCgInPc1	jeden
z	z	k7c2	z
nejžádanějších	žádaný	k2eAgInPc2d3	nejžádanější
produktů	produkt	k1gInPc2	produkt
haute	haut	k1gMnSc5	haut
couture	coutur	k1gMnSc5	coutur
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
konfekci	konfekce	k1gFnSc4	konfekce
(	(	kIx(	(
<g/>
prê	prê	k?	prê
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplňky	doplněk	k1gInPc4	doplněk
<g/>
,	,	kIx,	,
kosmetiku	kosmetika	k1gFnSc4	kosmetika
a	a	k8xC	a
parfémy	parfém	k1gInPc4	parfém
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
i	i	k9	i
divize	divize	k1gFnSc1	divize
Dior	Diora	k1gFnPc2	Diora
Homme	Homm	k1gInSc5	Homm
vyrábějící	vyrábějící	k2eAgFnPc4d1	vyrábějící
pánskou	pánský	k2eAgFnSc4d1	pánská
konfekci	konfekce	k1gFnSc4	konfekce
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
SA	SA	kA	SA
provozuje	provozovat	k5eAaImIp3nS	provozovat
celkem	celkem	k6eAd1	celkem
160	[number]	k4	160
butiků	butik	k1gInPc2	butik
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
plánuje	plánovat	k5eAaImIp3nS	plánovat
otevřít	otevřít	k5eAaPmF	otevřít
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
42,5	[number]	k4	42,5
<g/>
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
LVMH	LVMH	kA	LVMH
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
giganta	gigant	k1gMnSc4	gigant
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
luxusních	luxusní	k2eAgInPc2d1	luxusní
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
srdcí	srdce	k1gNnPc2	srdce
lidí	člověk	k1gMnPc2	člověk
svou	svůj	k3xOyFgFnSc7	svůj
výjimečností	výjimečnost	k1gFnSc7	výjimečnost
<g/>
,	,	kIx,	,
citem	cit	k1gInSc7	cit
pro	pro	k7c4	pro
módu	móda	k1gFnSc4	móda
a	a	k8xC	a
také	také	k9	také
vůněmi	vůně	k1gFnPc7	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
první	první	k4xOgFnSc4	první
vůni	vůně	k1gFnSc4	vůně
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
první	první	k4xOgFnSc1	první
módní	módní	k2eAgFnSc1d1	módní
kolekce	kolekce	k1gFnSc1	kolekce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
jej	on	k3xPp3gMnSc4	on
inspirovaly	inspirovat	k5eAaBmAgFnP	inspirovat
jeho	jeho	k3xOp3gFnPc4	jeho
milované	milovaný	k2eAgFnPc4d1	milovaná
květiny	květina	k1gFnPc4	květina
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
této	tento	k3xDgFnSc2	tento
kolekce	kolekce	k1gFnSc2	kolekce
předurčil	předurčit	k5eAaPmAgInS	předurčit
vznik	vznik	k1gInSc1	vznik
první	první	k4xOgFnSc2	první
vůně	vůně	k1gFnSc2	vůně
-	-	kIx~	-
Miss	miss	k1gFnSc1	miss
Dior	Diora	k1gFnPc2	Diora
<g/>
.	.	kIx.	.
</s>
<s>
Diorovým	Diorův	k2eAgNnSc7d1	Diorovo
přáním	přání	k1gNnSc7	přání
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
voněla	vonět	k5eAaImAgFnS	vonět
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
ji	on	k3xPp3gFnSc4	on
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
za	za	k7c7	za
ním	on	k3xPp3gMnSc7	on
přišla	přijít	k5eAaPmAgFnS	přijít
do	do	k7c2	do
nově	nově	k6eAd1	nově
otevřeného	otevřený	k2eAgInSc2d1	otevřený
salonu	salon	k1gInSc2	salon
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
Mitzha	Mitzha	k1gMnSc1	Mitzha
Bricard	Bricard	k1gMnSc1	Bricard
vykřikl	vykřiknout	k5eAaPmAgMnS	vykřiknout
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Podívejte	podívat	k5eAaImRp2nP	podívat
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
přichází	přicházet	k5eAaImIp3nS	přicházet
Miss	miss	k1gFnSc4	miss
Dior	Diora	k1gFnPc2	Diora
<g/>
!	!	kIx.	!
</s>
<s>
Miss	miss	k1gFnSc1	miss
Dior	Diora	k1gFnPc2	Diora
<g/>
!	!	kIx.	!
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaImF	jmenovat
první	první	k4xOgInSc4	první
parfém	parfém	k1gInSc4	parfém
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Další	další	k2eAgFnSc7d1	další
vůní	vůně	k1gFnSc7	vůně
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhou	druhý	k4xOgFnSc4	druhý
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vůně	vůně	k1gFnSc1	vůně
Diorama	Diorama	k1gFnSc1	Diorama
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
pro	pro	k7c4	pro
značku	značka	k1gFnSc4	značka
Dior	Diora	k1gFnPc2	Diora
ji	on	k3xPp3gFnSc4	on
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Edmond	Edmond	k1gMnSc1	Edmond
Roudnitska	Roudnitsk	k1gInSc2	Roudnitsk
<g/>
.	.	kIx.	.
</s>
<s>
Převrat	převrat	k1gInSc1	převrat
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
vůní	vůně	k1gFnPc2	vůně
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Edmond	Edmond	k1gMnSc1	Edmond
Roudnitska	Roudnitsk	k1gInSc2	Roudnitsk
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
pro	pro	k7c4	pro
Diora	Dior	k1gMnSc4	Dior
opět	opět	k6eAd1	opět
novou	nový	k2eAgFnSc4d1	nová
vůni	vůně	k1gFnSc4	vůně
-	-	kIx~	-
Eau	Eau	k1gFnSc1	Eau
de	de	k?	de
Cologne	Cologn	k1gMnSc5	Cologn
Fraiche	Fraichus	k1gMnSc5	Fraichus
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
první	první	k4xOgInSc4	první
unisex	unisex	k1gInSc4	unisex
vůní	vůně	k1gFnSc7	vůně
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Snem	sen	k1gInSc7	sen
Diora	Dior	k1gInSc2	Dior
byla	být	k5eAaImAgFnS	být
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
vůni	vůně	k1gFnSc4	vůně
konvalinek	konvalinka	k1gFnPc2	konvalinka
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
i	i	k9	i
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
novou	nový	k2eAgFnSc7d1	nová
vůní	vůně	k1gFnSc7	vůně
Diora	Dioro	k1gNnSc2	Dioro
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Diorissimo	Diorissima	k1gFnSc5	Diorissima
<g/>
.	.	kIx.	.
60	[number]	k4	60
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
přinesla	přinést	k5eAaPmAgFnS	přinést
poprvé	poprvé	k6eAd1	poprvé
kompletní	kompletní	k2eAgFnSc4d1	kompletní
péči	péče	k1gFnSc4	péče
pro	pro	k7c4	pro
pány	pan	k1gMnPc4	pan
-	-	kIx~	-
Eau	Eau	k1gMnPc7	Eau
Sauvage	Sauvag	k1gFnSc2	Sauvag
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
kromě	kromě	k7c2	kromě
parfému	parfém	k1gInSc2	parfém
i	i	k9	i
vodu	voda	k1gFnSc4	voda
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
holení	holení	k1gNnSc6	holení
<g/>
,	,	kIx,	,
pudr	pudr	k1gInSc4	pudr
<g/>
,	,	kIx,	,
balzám	balzám	k1gInSc4	balzám
a	a	k8xC	a
mýdlo	mýdlo	k1gNnSc4	mýdlo
<g/>
.	.	kIx.	.
1972	[number]	k4	1972
-	-	kIx~	-
vznik	vznik	k1gInSc1	vznik
nové	nový	k2eAgFnSc2d1	nová
květinové	květinový	k2eAgFnSc2d1	květinová
vůně	vůně	k1gFnSc2	vůně
Diorella	Diorella	k1gFnSc1	Diorella
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Dior	Dior	k1gInSc1	Dior
objevil	objevit	k5eAaPmAgInS	objevit
kouzlo	kouzlo	k1gNnSc4	kouzlo
orientální	orientální	k2eAgFnSc2d1	orientální
vůně	vůně	k1gFnSc2	vůně
Dioressence	Dioressence	k1gFnSc2	Dioressence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
smyslné	smyslný	k2eAgFnPc4d1	smyslná
<g/>
,	,	kIx,	,
podmanivé	podmanivý	k2eAgFnPc4d1	podmanivá
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
představil	představit	k5eAaPmAgInS	představit
Dior	Dior	k1gInSc4	Dior
druhou	druhý	k4xOgFnSc4	druhý
pánskou	pánský	k2eAgFnSc4d1	pánská
vůni	vůně	k1gFnSc4	vůně
Jules	Julesa	k1gFnPc2	Julesa
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1985	[number]	k4	1985
-	-	kIx~	-
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
o	o	k7c4	o
inovaci	inovace	k1gFnSc4	inovace
-	-	kIx~	-
nový	nový	k2eAgInSc4d1	nový
flakon	flakon	k1gInSc4	flakon
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc4d1	nové
jméno	jméno	k1gNnSc4	jméno
-	-	kIx~	-
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vůně	vůně	k1gFnSc1	vůně
Poison	Poisona	k1gFnPc2	Poisona
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
Poisonu	Poison	k1gInSc2	Poison
byl	být	k5eAaImAgInS	být
globální	globální	k2eAgInSc1d1	globální
a	a	k8xC	a
tak	tak	k9	tak
pokračování	pokračování	k1gNnSc4	pokračování
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechalo	nechat	k5eNaPmAgNnS	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
-	-	kIx~	-
k	k	k7c3	k
Poisonu	Poison	k1gMnSc3	Poison
přibyly	přibýt	k5eAaPmAgInP	přibýt
Tendre	tendr	k1gInSc5	tendr
Poison	Poisona	k1gFnPc2	Poisona
<g/>
,	,	kIx,	,
Hypnotic	Hypnotice	k1gFnPc2	Hypnotice
Poison	Poisona	k1gFnPc2	Poisona
<g/>
,	,	kIx,	,
Pure	Pure	k1gFnSc1	Pure
Poison	Poison	k1gMnSc1	Poison
a	a	k8xC	a
Midnight	Midnight	k1gMnSc1	Midnight
Poison	Poison	k1gMnSc1	Poison
<g/>
.	.	kIx.	.
</s>
<s>
Ambice	ambice	k1gFnPc1	ambice
se	se	k3xPyFc4	se
projevily	projevit	k5eAaPmAgFnP	projevit
brzy	brzy	k6eAd1	brzy
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
zrodila	zrodit	k5eAaPmAgFnS	zrodit
další	další	k2eAgFnSc1d1	další
pánská	pánský	k2eAgFnSc1d1	pánská
vůně	vůně	k1gFnSc1	vůně
-	-	kIx~	-
Fahrenheit	Fahrenheit	k1gMnSc1	Fahrenheit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
trh	trh	k1gInSc4	trh
nová	nový	k2eAgFnSc1d1	nová
vůně	vůně	k1gFnSc1	vůně
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
Dune	Dun	k1gFnSc2	Dun
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
ukrývala	ukrývat	k5eAaImAgFnS	ukrývat
harmonii	harmonie	k1gFnSc4	harmonie
s	s	k7c7	s
přírodou	příroda	k1gFnSc7	příroda
<g/>
,	,	kIx,	,
květinovou	květinový	k2eAgFnSc4d1	květinová
a	a	k8xC	a
mořskou	mořský	k2eAgFnSc4d1	mořská
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
znamenal	znamenat	k5eAaImAgInS	znamenat
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
původním	původní	k2eAgFnPc3d1	původní
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
nová	nový	k2eAgFnSc1d1	nová
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
asi	asi	k9	asi
ta	ten	k3xDgFnSc1	ten
nejznámější	známý	k2eAgFnSc1d3	nejznámější
-	-	kIx~	-
J	J	kA	J
<g/>
'	'	kIx"	'
<g/>
adore	ador	k1gMnSc5	ador
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
projektem	projekt	k1gInSc7	projekt
při	při	k7c6	při
kolekci	kolekce	k1gFnSc6	kolekce
rtěnek	rtěnka	k1gFnPc2	rtěnka
Dior	Dior	k1gMnSc1	Dior
Addict	Addict	k1gMnSc1	Addict
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vůně	vůně	k1gFnSc1	vůně
Dior	Dior	k1gMnSc1	Dior
Addict	Addict	k1gMnSc1	Addict
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
Queen	Queen	k1gInSc4	Queen
of	of	k?	of
the	the	k?	the
Night	Night	k1gInSc1	Night
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
následovala	následovat	k5eAaImAgFnS	následovat
po	po	k7c4	po
Dior	Dior	k1gInSc4	Dior
Addict	Addicta	k1gFnPc2	Addicta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
jamajský	jamajský	k2eAgInSc1d1	jamajský
květ	květ	k1gInSc1	květ
s	s	k7c7	s
kaktusy	kaktus	k1gInPc7	kaktus
<g/>
,	,	kIx,	,
doplněné	doplněná	k1gFnPc4	doplněná
o	o	k7c4	o
jemnou	jemný	k2eAgFnSc4d1	jemná
vanilku	vanilka	k1gFnSc4	vanilka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kompozice	kompozice	k1gFnSc1	kompozice
propůjčila	propůjčit	k5eAaPmAgFnS	propůjčit
parfému	parfém	k1gInSc3	parfém
jedinečnost	jedinečnost	k1gFnSc4	jedinečnost
<g/>
.	.	kIx.	.
</s>
<s>
Výročí	výročí	k1gNnSc1	výročí
narození	narození	k1gNnSc2	narození
Christiana	Christian	k1gMnSc2	Christian
Diora	Dior	k1gInSc2	Dior
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
příležitostí	příležitost	k1gFnSc7	příležitost
ke	k	k7c3	k
znovuzrození	znovuzrození	k1gNnSc3	znovuzrození
Miss	miss	k1gFnSc2	miss
Dior	Diora	k1gFnPc2	Diora
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
názvem	název	k1gInSc7	název
Miss	miss	k1gFnPc2	miss
Dior	Diora	k1gFnPc2	Diora
Chérie	Chérie	k1gFnSc2	Chérie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Christian	Christian	k1gMnSc1	Christian
Dior	Diora	k1gFnPc2	Diora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dior	Dior	k1gInSc1	Dior
<g/>
;	;	kIx,	;
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
společnosti	společnost	k1gFnSc2	společnost
Christian	Christian	k1gMnSc1	Christian
Dior	Dior	k1gMnSc1	Dior
Bag	Bag	k?	Bag
How	How	k1gFnSc2	How
to	ten	k3xDgNnSc1	ten
Spot	spot	k1gInSc4	spot
fake	fak	k1gMnSc2	fak
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
článek	článek	k1gInSc4	článek
o	o	k7c6	o
Christianu	Christian	k1gMnSc6	Christian
Dioru	Dior	k1gMnSc6	Dior
na	na	k7c6	na
MDLS	MDLS	kA	MDLS
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
