<p>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gFnSc2	Genth
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1869	[number]	k4	1869
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fotograf	fotograf	k1gMnSc1	fotograf
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
nejznámější	známý	k2eAgFnSc1d3	nejznámější
svými	svůj	k3xOyFgFnPc7	svůj
fotografiemi	fotografia	k1gFnPc7	fotografia
sanfranciské	sanfranciský	k2eAgFnPc1d1	sanfranciská
Čínské	čínský	k2eAgFnPc1d1	čínská
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
1906	[number]	k4	1906
a	a	k8xC	a
portréty	portrét	k1gInPc1	portrét
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
od	od	k7c2	od
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
společenských	společenský	k2eAgMnPc2d1	společenský
prominentů	prominent	k1gMnPc2	prominent
<g/>
,	,	kIx,	,
literárních	literární	k2eAgFnPc2d1	literární
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
celebrit	celebrita	k1gFnPc2	celebrita
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
zábavy	zábava	k1gFnSc2	zábava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Genthe	Genthat	k5eAaPmIp3nS	Genthat
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Prusku	Prusko	k1gNnSc6	Prusko
matce	matka	k1gFnSc6	matka
Louise	Louis	k1gMnSc2	Louis
Zoberové	Zoberové	k2eAgMnSc1d1	Zoberové
a	a	k8xC	a
Hermannovi	Hermann	k1gMnSc3	Hermann
Genthovi	Genth	k1gMnSc3	Genth
<g/>
,	,	kIx,	,
profesorovi	profesor	k1gMnSc3	profesor
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
řečtiny	řečtina	k1gFnSc2	řečtina
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Berlinisches	Berlinischesa	k1gFnPc2	Berlinischesa
Gymnasium	gymnasium	k1gNnSc1	gymnasium
zum	zum	k?	zum
Grauen	Grauen	k2eAgMnSc1d1	Grauen
Kloster	Kloster	k1gMnSc1	Kloster
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
následoval	následovat	k5eAaImAgInS	následovat
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
šlépějích	šlépěj	k1gFnPc6	šlépěj
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
vystudovaný	vystudovaný	k2eAgMnSc1d1	vystudovaný
odborník	odborník	k1gMnSc1	odborník
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
získal	získat	k5eAaPmAgMnS	získat
doktorát	doktorát	k1gInSc4	doktorát
z	z	k7c2	z
filologie	filologie	k1gFnSc2	filologie
na	na	k7c4	na
University	universita	k1gFnPc4	universita
of	of	k?	of
Jena	Jena	k1gMnSc1	Jena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poznal	poznat	k5eAaPmAgMnS	poznat
umělce	umělec	k1gMnSc4	umělec
Adolfa	Adolf	k1gMnSc4	Adolf
Menzela	Menzela	k1gMnSc4	Menzela
<g/>
,	,	kIx,	,
bratrance	bratranec	k1gMnPc4	bratranec
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
do	do	k7c2	do
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
a	a	k8xC	a
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
.	.	kIx.	.
</s>
<s>
Zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
jej	on	k3xPp3gNnSc4	on
čínská	čínský	k2eAgFnSc1d1	čínská
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
a	a	k8xC	a
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
její	její	k3xOp3gMnPc4	její
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
od	od	k7c2	od
dětí	dítě	k1gFnPc2	dítě
až	až	k9	až
po	po	k7c4	po
narkomany	narkoman	k1gMnPc4	narkoman
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
možnému	možný	k2eAgInSc3d1	možný
strachu	strach	k1gInSc3	strach
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc1	jejich
neochota	neochota	k1gFnSc1	neochota
se	se	k3xPyFc4	se
nechat	nechat	k5eAaPmF	nechat
fotografovat	fotografovat	k5eAaImF	fotografovat
<g/>
,	,	kIx,	,
Genthe	Genthe	k1gFnSc1	Genthe
někdy	někdy	k6eAd1	někdy
svůj	svůj	k3xOyFgInSc4	svůj
fotoaparát	fotoaparát	k1gInSc4	fotoaparát
skrýval	skrývat	k5eAaImAgMnS	skrývat
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
odstraňoval	odstraňovat	k5eAaImAgMnS	odstraňovat
důkazy	důkaz	k1gInPc7	důkaz
o	o	k7c6	o
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
oříznutím	oříznutí	k1gNnSc7	oříznutí
nebo	nebo	k8xC	nebo
retuší	retuš	k1gFnSc7	retuš
<g/>
.	.	kIx.	.
</s>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
asi	asi	k9	asi
200	[number]	k4	200
jeho	jeho	k3xOp3gFnPc2	jeho
fotografií	fotografia	k1gFnPc2	fotografia
čínské	čínský	k2eAgFnSc2d1	čínská
čtvrti	čtvrt	k1gFnSc2	čtvrt
-	-	kIx~	-
a	a	k8xC	a
tyto	tento	k3xDgFnPc1	tento
tvoří	tvořit	k5eAaImIp3nP	tvořit
jediný	jediný	k2eAgInSc4d1	jediný
známý	známý	k2eAgInSc4d1	známý
fotografický	fotografický	k2eAgInSc4d1	fotografický
záznam	záznam	k1gInSc4	záznam
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
místa	místo	k1gNnSc2	místo
před	před	k7c7	před
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
několik	několik	k4yIc4	několik
místních	místní	k2eAgInPc2d1	místní
časopisů	časopis	k1gInPc2	časopis
otisklo	otisknout	k5eAaPmAgNnS	otisknout
některé	některý	k3yIgNnSc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
v	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
otevřel	otevřít	k5eAaPmAgMnS	otevřít
si	se	k3xPyFc3	se
portrétní	portrétní	k2eAgNnSc4d1	portrétní
fotografické	fotografický	k2eAgNnSc4d1	fotografické
studio	studio	k1gNnSc4	studio
<g/>
.	.	kIx.	.
</s>
<s>
Znal	znát	k5eAaImAgMnS	znát
se	se	k3xPyFc4	se
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
váznamnými	váznamný	k2eAgFnPc7d1	váznamná
a	a	k8xC	a
bohatými	bohatý	k2eAgFnPc7d1	bohatá
osobnostmi	osobnost	k1gFnPc7	osobnost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
jak	jak	k8xS	jak
jeho	jeho	k3xOp3gFnSc1	jeho
pověst	pověst	k1gFnSc1	pověst
rostla	růst	k5eAaImAgFnS	růst
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnSc4	jeho
klientelu	klientela	k1gFnSc4	klientela
patřili	patřit	k5eAaImAgMnP	patřit
například	například	k6eAd1	například
Nance	Nanka	k1gFnSc3	Nanka
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Neil	Neil	k1gMnSc1	Neil
<g/>
,	,	kIx,	,
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardt	Bernhardt	k1gInSc1	Bernhardt
<g/>
,	,	kIx,	,
Nora	Nora	k1gFnSc1	Nora
May	May	k1gMnSc1	May
French	French	k1gMnSc1	French
nebo	nebo	k8xC	nebo
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
oheň	oheň	k1gInSc1	oheň
zničil	zničit	k5eAaPmAgInS	zničit
Genthův	Genthův	k2eAgInSc1d1	Genthův
ateliér	ateliér	k1gInSc1	ateliér
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
však	však	k9	však
znovu	znovu	k6eAd1	znovu
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
fotografie	fotografie	k1gFnSc1	fotografie
Pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
Sacramento	Sacramento	k1gNnSc4	Sacramento
Street	Street	k1gMnSc1	Street
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
stala	stát	k5eAaPmAgFnS	stát
jeho	jeho	k3xOp3gFnSc4	jeho
nejslavnější	slavný	k2eAgFnSc4d3	nejslavnější
fotografii	fotografia	k1gFnSc4	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
krátké	krátký	k2eAgFnSc2d1	krátká
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
Genthe	Genthe	k1gInSc1	Genthe
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
umělecké	umělecký	k2eAgFnSc3d1	umělecká
kolonii	kolonie	k1gFnSc3	kolonie
v	v	k7c4	v
Carmel-by-the-Sea	Carmelyhe-Se	k2eAgNnPc4d1	Carmel-by-the-Se
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
schopný	schopný	k2eAgInSc1d1	schopný
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c4	v
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
barevné	barevný	k2eAgFnSc2d1	barevná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
novém	nový	k2eAgNnSc6d1	nové
bydlišti	bydliště	k1gNnSc6	bydliště
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
cypřiše	cypřiš	k1gInPc1	cypřiš
a	a	k8xC	a
skály	skála	k1gFnPc1	skála
Point	pointa	k1gFnPc2	pointa
Lobos	Lobosa	k1gFnPc2	Lobosa
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
různé	různý	k2eAgInPc1d1	různý
západy	západ	k1gInPc1	západ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
zajímavé	zajímavý	k2eAgInPc1d1	zajímavý
stíny	stín	k1gInPc1	stín
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
nabízí	nabízet	k5eAaImIp3nS	nabízet
bohaté	bohatý	k2eAgNnSc4d1	bohaté
pole	pole	k1gNnSc4	pole
pro	pro	k7c4	pro
barevné	barevný	k2eAgInPc4d1	barevný
experimenty	experiment	k1gInPc4	experiment
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
především	především	k6eAd1	především
portrétům	portrét	k1gInPc3	portrét
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
klienti	klient	k1gMnPc1	klient
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Theodore	Theodor	k1gMnSc5	Theodor
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
<g/>
,	,	kIx,	,
Woodrow	Woodrow	k1gMnSc1	Woodrow
Wilson	Wilson	k1gMnSc1	Wilson
a	a	k8xC	a
John	John	k1gMnSc1	John
D.	D.	kA	D.
Rockefeller	Rockefeller	k1gMnSc1	Rockefeller
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
fotografiím	fotografia	k1gFnPc3	fotografia
Grety	Gret	k1gInPc1	Gret
Garbo	Garba	k1gFnSc5	Garba
se	se	k3xPyFc4	se
připisují	připisovat	k5eAaImIp3nP	připisovat
důvody	důvod	k1gInPc1	důvod
zvýšení	zvýšení	k1gNnSc1	zvýšení
její	její	k3xOp3gFnSc2	její
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
tanečníky	tanečník	k1gMnPc4	tanečník
moderního	moderní	k2eAgInSc2d1	moderní
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Anny	Anna	k1gFnSc2	Anna
Pavlovou	Pavlová	k1gFnSc4	Pavlová
<g/>
,	,	kIx,	,
Isadoru	Isador	k1gInSc6	Isador
Duncanovou	Duncanová	k1gFnSc7	Duncanová
nebo	nebo	k8xC	nebo
Ruth	Ruth	k1gFnSc7	Ruth
St.	st.	kA	st.
Denisovou	Denisový	k2eAgFnSc7d1	Denisová
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
byly	být	k5eAaImAgFnP	být
otištěny	otisknout	k5eAaPmNgFnP	otisknout
roku	rok	k1gInSc3	rok
1916	[number]	k4	1916
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
The	The	k1gMnSc1	The
Book	Book	k1gMnSc1	Book
of	of	k?	of
the	the	k?	the
Dance	Danka	k1gFnSc3	Danka
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
tanci	tanec	k1gInSc6	tanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1918	[number]	k4	1918
u	u	k7c2	u
něho	on	k3xPp3gMnSc2	on
v	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
ateliéru	ateliér	k1gInSc6	ateliér
trénovala	trénovat	k5eAaImAgFnS	trénovat
praxi	praxe	k1gFnSc4	praxe
pozdější	pozdní	k2eAgFnSc1d2	pozdější
významná	významný	k2eAgFnSc1d1	významná
fotografka	fotografka	k1gFnSc1	fotografka
Dorothea	Dorothea	k1gFnSc1	Dorothea
Langeová	Langeová	k1gFnSc1	Langeová
<g/>
.	.	kIx.	.
<g/>
Genthe	Genthe	k1gFnSc1	Genthe
také	také	k9	také
často	často	k6eAd1	často
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
autochrom	autochrom	k1gInSc1	autochrom
-	-	kIx~	-
první	první	k4xOgFnSc7	první
barevnou	barevný	k2eAgFnSc7d1	barevná
fotografií	fotografia	k1gFnSc7	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikace	publikace	k1gFnSc1	publikace
==	==	k?	==
</s>
</p>
<p>
<s>
Pictures	Pictures	k1gInSc1	Pictures
of	of	k?	of
old	old	k?	old
Chinatown	Chinatown	k1gInSc1	Chinatown
–	–	k?	–
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
Will	Will	k1gMnSc1	Will
Irwin	Irwin	k1gMnSc1	Irwin
<g/>
,	,	kIx,	,
ilustrace	ilustrace	k1gFnSc1	ilustrace
<g/>
:	:	kIx,	:
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Moffat	Moffat	k1gFnSc1	Moffat
<g/>
,	,	kIx,	,
Yard	yard	k1gInSc1	yard
and	and	k?	and
co	co	k8xS	co
<g/>
.	.	kIx.	.
1908	[number]	k4	1908
</s>
</p>
<p>
<s>
The	The	k?	The
book	book	k1gInSc1	book
of	of	k?	of
the	the	k?	the
dance	dance	k1gMnSc1	dance
–	–	k?	–
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
<g/>
;	;	kIx,	;
Boston	Boston	k1gInSc1	Boston
<g/>
,	,	kIx,	,
Mass	Mass	k1gInSc1	Mass
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
International	International	k1gFnSc1	International
Publishers	Publishersa	k1gFnPc2	Publishersa
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
c.	c.	k?	c.
1916	[number]	k4	1916
</s>
</p>
<p>
<s>
Impressions	Impressions	k6eAd1	Impressions
of	of	k?	of
Old	Olda	k1gFnPc2	Olda
New	New	k1gFnSc1	New
Orleans	Orleans	k1gInSc1	Orleans
–	–	k?	–
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
<g/>
,	,	kIx,	,
fwd	fwd	k?	fwd
by	by	kYmCp3nS	by
Grace	Grace	k1gMnSc1	Grace
King	King	k1gMnSc1	King
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
George	George	k1gInSc1	George
H.	H.	kA	H.
Doran	Doran	k1gInSc1	Doran
co	co	k3yRnSc1	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
c.	c.	k?	c.
1926	[number]	k4	1926
</s>
</p>
<p>
<s>
Isadora	Isadora	k1gFnSc1	Isadora
Duncan	Duncana	k1gFnPc2	Duncana
<g/>
:	:	kIx,	:
twenty	twenta	k1gFnSc2	twenta
four	four	k1gMnSc1	four
studies	studies	k1gMnSc1	studies
–	–	k?	–
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
M.	M.	kA	M.
Kennerley	Kennerlea	k1gFnSc2	Kennerlea
1929	[number]	k4	1929
<g/>
;	;	kIx,	;
reprinted	reprinted	k1gInSc1	reprinted
by	by	kYmCp3nP	by
Books	Books	k1gInSc4	Books
for	forum	k1gNnPc2	forum
Libraries	Libraries	k1gMnSc1	Libraries
1980	[number]	k4	1980
ISBN	ISBN	kA	ISBN
0-8369-9306-3	[number]	k4	0-8369-9306-3
</s>
</p>
<p>
<s>
As	as	k9	as
I	i	k9	i
remember	remember	k1gMnSc1	remember
–	–	k?	–
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Reynal	Reynal	k1gFnSc1	Reynal
&	&	k?	&
Hitchcock	Hitchcock	k1gInSc1	Hitchcock
c.	c.	k?	c.
1936	[number]	k4	1936
</s>
</p>
<p>
<s>
Highlights	Highlights	k1gInSc1	Highlights
and	and	k?	and
shadows	shadows	k1gInSc1	shadows
–	–	k?	–
editor	editor	k1gInSc1	editor
<g/>
:	:	kIx,	:
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gMnSc2	Genth
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Greenberg	Greenberg	k1gInSc1	Greenberg
<g/>
,	,	kIx,	,
c.	c.	k?	c.
1937	[number]	k4	1937
</s>
</p>
<p>
<s>
Genthe	Genthe	k1gInSc1	Genthe
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Photographs	Photographs	k1gInSc1	Photographs
of	of	k?	of
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Chinatown	Chinatown	k1gNnSc1	Chinatown
–	–	k?	–
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genthe	k1gNnSc1	Genthe
<g/>
,	,	kIx,	,
výběr	výběr	k1gInSc1	výběr
a	a	k8xC	a
text	text	k1gInSc1	text
<g/>
:	:	kIx,	:
John	John	k1gMnSc1	John
Tchen	Tchna	k1gFnPc2	Tchna
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Dover	Dover	k1gInSc1	Dover
Publications	Publications	k1gInSc1	Publications
1984	[number]	k4	1984
ISBN	ISBN	kA	ISBN
0-486-24592-6	[number]	k4	0-486-24592-6
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Mel	mlít	k5eAaImRp2nS	mlít
Byars	Byars	k1gInSc1	Byars
<g/>
,	,	kIx,	,
N.	N.	kA	N.
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Schlatter	Schlatter	k1gMnSc1	Schlatter
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Genthe	Genthe	k1gInSc1	Genthe
<g/>
,	,	kIx,	,
Arnold	Arnold	k1gMnSc1	Arnold
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gInSc1	American
National	National	k1gMnSc2	National
Biography	Biographa	k1gMnSc2	Biographa
Online	Onlin	k1gInSc5	Onlin
<g/>
.	.	kIx.	.
</s>
<s>
Feb	Feb	k?	Feb
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Accessed	Accessed	k1gInSc1	Accessed
September	September	k1gInSc1	September
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
subscription	subscription	k1gInSc1	subscription
required	required	k1gInSc1	required
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genth	k1gInSc2	Genth
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Arnold	Arnold	k1gMnSc1	Arnold
Genthe	Genthe	k1gFnSc7	Genthe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
:	:	kIx,	:
Prints	Prints	k1gInSc1	Prints
&	&	k?	&
Photographs	Photographs	k1gInSc1	Photographs
Division	Division	k1gInSc1	Division
<g/>
:	:	kIx,	:
Genthe	Genthe	k1gInSc1	Genthe
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
sample	sample	k6eAd1	sample
images	images	k1gInSc1	images
from	froma	k1gFnPc2	froma
collection	collection	k1gInSc1	collection
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
As	as	k1gNnPc7	as
I	i	k9	i
Remember	Remember	k1gMnSc1	Remember
<g/>
"	"	kIx"	"
Chapter	Chapter	k1gInSc1	Chapter
10	[number]	k4	10
<g/>
:	:	kIx,	:
Earthquake	Earthquak	k1gInSc2	Earthquak
and	and	k?	and
Fire	Fire	k1gInSc1	Fire
</s>
</p>
<p>
<s>
California	Californium	k1gNnPc1	Californium
Historical	Historical	k1gFnSc2	Historical
Society	societa	k1gFnSc2	societa
collection	collection	k1gInSc1	collection
</s>
</p>
<p>
<s>
SF	SF	kA	SF
MOMA	MOMA	kA	MOMA
collection	collection	k1gInSc4	collection
</s>
</p>
