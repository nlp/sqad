<s>
František	František	k1gMnSc1	František
Palacký	Palacký	k1gMnSc1	Palacký
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1798	[number]	k4	1798
Hodslavice	Hodslavice	k1gInPc4	Hodslavice
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1876	[number]	k4	1876
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
organizátor	organizátor	k1gMnSc1	organizátor
veřejného	veřejný	k2eAgInSc2d1	veřejný
kulturního	kulturní	k2eAgInSc2d1	kulturní
a	a	k8xC	a
vědeckého	vědecký	k2eAgInSc2d1	vědecký
života	život	k1gInSc2	život
v	v	k7c6	v
soudobé	soudobý	k2eAgFnSc6d1	soudobá
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
českého	český	k2eAgNnSc2d1	české
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Otec	otec	k1gMnSc1	otec
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>

