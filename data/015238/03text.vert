<s>
Andrea	Andrea	k1gFnSc1
Demirović	Demirović	k1gFnSc2
</s>
<s>
Andrea	Andrea	k1gFnSc1
Demirović	Demirović	k1gFnSc1
Andrea	Andrea	k1gFnSc1
Demirović	Demirović	k1gFnSc1
na	na	k7c6
Eurovizi	Eurovize	k1gFnSc6
2009	#num#	k4
v	v	k7c6
MoskvěZákladní	MoskvěZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
А	А	k?
Д	Д	k?
Jinak	jinak	k6eAd1
zvaná	zvaný	k2eAgFnSc1d1
</s>
<s>
Andrea	Andrea	k1gFnSc1
Narození	narození	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1985	#num#	k4
(	(	kIx(
<g/>
35	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Titograd	Titograd	k1gInSc1
<g/>
,	,	kIx,
SR	SR	kA
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
SFRJ	SFRJ	kA
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Podgorica	Podgoric	k2eAgFnSc1d1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
Žánry	žánr	k1gInPc1
</s>
<s>
Pop	pop	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
Zpěvačka	zpěvačka	k1gFnSc1
Aktivní	aktivní	k2eAgFnSc1d1
roky	rok	k1gInPc4
</s>
<s>
2002	#num#	k4
<g/>
—	—	k?
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
City	city	k1gNnSc1
Records	Recordsa	k1gFnPc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Andrea	Andrea	k1gFnSc1
Demirović	Demirović	k1gFnSc2
(	(	kIx(
<g/>
černohorsky	černohorsky	k6eAd1
cyrilicí	cyrilice	k1gFnSc7
А	А	k?
Д	Д	k?
<g/>
;	;	kIx,
*	*	kIx~
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1985	#num#	k4
Titograd	Titograd	k1gInSc1
<g/>
,	,	kIx,
SR	SR	kA
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
Socialistická	socialistický	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
Podgorica	Podgoric	k2eAgFnSc1d1
<g/>
,	,	kIx,
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
černohorská	černohorský	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
pod	pod	k7c7
svým	svůj	k3xOyFgNnSc7
jménem	jméno	k1gNnSc7
Andrea	Andrea	k1gFnSc1
(	(	kIx(
<g/>
А	А	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vešla	vejít	k5eAaPmAgFnS
ve	v	k7c4
známost	známost	k1gFnSc4
účastí	účast	k1gFnSc7
na	na	k7c6
festivalu	festival	k1gInSc6
Sunčane	Sunčan	k1gMnSc5
skale	skale	k1gNnPc3
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
účastí	účast	k1gFnSc7
v	v	k7c6
národním	národní	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
Srbska	Srbsko	k1gNnSc2
a	a	k8xC
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
do	do	k7c2
Eurovize	Eurovize	k1gFnSc2
2005	#num#	k4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
s	s	k7c7
písní	píseň	k1gFnSc7
"	"	kIx"
<g/>
Šta	Šta	k1gFnSc7
Će	Će	k1gFnSc2
Mi	já	k3xPp1nSc3
Dani	daň	k1gFnSc3
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Ш	Ш	k?
ћ	ћ	k?
м	м	k?
д	д	k?
<g/>
)	)	kIx)
a	a	k8xC
samostatné	samostatný	k2eAgFnSc2d1
Černé	Černé	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Následně	následně	k6eAd1
reprezentovala	reprezentovat	k5eAaImAgFnS
Černou	černý	k2eAgFnSc4d1
Horu	hora	k1gFnSc4
na	na	k7c4
Eurovision	Eurovision	k1gInSc4
Song	song	k1gInSc1
Contest	Contest	k1gFnSc1
2009	#num#	k4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
s	s	k7c7
písní	píseň	k1gFnSc7
"	"	kIx"
<g/>
Just	just	k6eAd1
Get	Get	k1gMnSc1
Out	Out	k1gMnSc1
Of	Of	k1gMnSc1
My	my	k3xPp1nPc1
Life	Lifus	k1gMnSc5
<g/>
"	"	kIx"
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
obsadila	obsadit	k5eAaPmAgFnS
11	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
semifinále	semifinále	k1gNnSc6
se	s	k7c7
ziskem	zisk	k1gInSc7
44	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
reprezentoval	reprezentovat	k5eAaImAgInS
Černou	černý	k2eAgFnSc4d1
Horu	hora	k1gFnSc4
na	na	k7c6
Baltic	Baltice	k1gFnPc2
Song	song	k1gInSc4
Contest	Contest	k1gFnSc4
s	s	k7c7
písněni	písněn	k2eAgMnPc1d1
"	"	kIx"
<g/>
Odlazim	Odlazi	k1gNnSc7
<g/>
"	"	kIx"
а	а	k?
"	"	kIx"
<g/>
All	All	k1gMnSc1
The	The	k1gMnSc1
Best	Best	k1gMnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Alba	alba	k1gFnSc1
</s>
<s>
2009	#num#	k4
-	-	kIx~
"	"	kIx"
<g/>
Andrea	Andrea	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
City	city	k1gNnSc1
Records	Recordsa	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Singly	singl	k1gInPc1
</s>
<s>
"	"	kIx"
<g/>
Just	just	k6eAd1
Get	Get	k1gMnSc1
Out	Out	k1gMnSc1
Of	Of	k1gMnSc1
My	my	k3xPp1nPc1
Life	Life	k1gNnPc3
<g/>
"	"	kIx"
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gFnPc2
Queen	Queno	k1gNnPc2
of	of	k?
the	the	k?
Night	Night	k1gInSc1
<g/>
"	"	kIx"
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://eurocontest.cz/headline/headline/item/3359-3359%5B%5D	http://eurocontest.cz/headline/headline/item/3359-3359%5B%5D	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
oikotimes	oikotimes	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://poplight.zitiz.se/artikel/baltic-song-contest-2013%5B%5D	http://poplight.zitiz.se/artikel/baltic-song-contest-2013%5B%5D	k4
<g/>
↑	↑	k?
http://www.karlshamn.se/sv/Ostersjofestivalen/Nyheter/Baltic-Song-Contest-2013/%5B%5D	http://www.karlshamn.se/sv/Ostersjofestivalen/Nyheter/Baltic-Song-Contest-2013/%5B%5D	k4
<g/>
↑	↑	k?
http://www.janelaesc.com/2013/06/baltic-song-contest-2013-festival.html%5B%5D	http://www.janelaesc.com/2013/06/baltic-song-contest-2013-festival.html%5B%5D	k4
<g/>
↑	↑	k?
http://www.schlagerpinglan.se/blog/2013/06/26/ostersjofestivalens-baltic-song-contest-2013/	http://www.schlagerpinglan.se/blog/2013/06/26/ostersjofestivalens-baltic-song-contest-2013/	k4
<g/>
↑	↑	k?
http://montenegro.rsspump.com/?topic=montenegro-andrea-demirovic-to-compete-at-the-2013-baltic-song-contest&	http://montenegro.rsspump.com/?topic=montenegro-andrea-demirovic-to-compete-at-the-2013-baltic-song-contest&	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Andrea	Andrea	k1gFnSc1
Demirović	Demirović	k1gFnSc2
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Videoklip	videoklip	k1gInSc1
"	"	kIx"
<g/>
Just	just	k6eAd1
Get	Get	k1gMnSc1
Out	Out	k1gMnSc1
Of	Of	k1gMnSc1
My	my	k3xPp1nPc1
Life	Life	k1gNnPc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
YouTube	YouTub	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Vystoupení	vystoupení	k1gNnSc1
na	na	k7c6
Eurovizi	Eurovize	k1gFnSc6
2009	#num#	k4
(	(	kIx(
<g/>
YouTube	YouTub	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
na	na	k7c4
Eurovision	Eurovision	k1gInSc4
Song	song	k1gInSc1
Contest	Contest	k1gFnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Stefan	Stefan	k1gMnSc1
Filipović	Filipović	k1gMnSc1
"	"	kIx"
<g/>
Zauvijek	Zauvijka	k1gFnPc2
Volim	Volim	k?
Te	Te	k1gMnSc1
<g/>
"	"	kIx"
</s>
<s>
2009	#num#	k4
Andrea	Andrea	k1gFnSc1
Demirović	Demirović	k1gFnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Rambo	Ramba	k1gFnSc5
Amadeus	Amadeus	k1gMnSc1
"	"	kIx"
<g/>
Euro	euro	k1gNnSc1
Neuro	Neuro	k1gNnSc1
<g/>
"	"	kIx"
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Černá	černý	k2eAgFnSc1d1
Hora	hora	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
3149366287685602113	#num#	k4
</s>
