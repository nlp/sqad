<p>
<s>
Nukleonové	Nukleonový	k2eAgNnSc1d1	Nukleonový
číslo	číslo	k1gNnSc1	číslo
(	(	kIx(	(
<g/>
též	též	k9	též
hmotové	hmotový	k2eAgNnSc4d1	hmotové
číslo	číslo	k1gNnSc4	číslo
nebo	nebo	k8xC	nebo
hmotnostní	hmotnostní	k2eAgNnSc4d1	hmotnostní
číslo	číslo	k1gNnSc4	číslo
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
nukleonů	nukleon	k1gInPc2	nukleon
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
<g/>
)	)	kIx)	)
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádře	jádro	k1gNnSc6	jádro
daného	daný	k2eAgInSc2d1	daný
nuklidu	nuklid	k1gInSc2	nuklid
<g/>
.	.	kIx.	.
</s>
<s>
Nukleonové	Nukleonový	k2eAgNnSc1d1	Nukleonový
číslo	číslo	k1gNnSc1	číslo
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
různé	různý	k2eAgInPc4d1	různý
izotopy	izotop	k1gInPc4	izotop
daného	daný	k2eAgInSc2d1	daný
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnSc2	značení
==	==	k?	==
</s>
</p>
<p>
<s>
Nukleonové	Nukleonový	k2eAgNnSc4d1	Nukleonový
číslo	číslo	k1gNnSc4	číslo
obvykle	obvykle	k6eAd1	obvykle
označujeme	označovat	k5eAaImIp1nP	označovat
symbolem	symbol	k1gInSc7	symbol
A	A	kA	A
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
zápisu	zápis	k1gInSc6	zápis
určitého	určitý	k2eAgInSc2d1	určitý
izotopu	izotop	k1gInSc2	izotop
daného	daný	k2eAgInSc2d1	daný
prvku	prvek	k1gInSc2	prvek
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
vlevo	vlevo	k6eAd1	vlevo
nahoru	nahoru	k6eAd1	nahoru
před	před	k7c4	před
symbol	symbol	k1gInSc4	symbol
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
238U	[number]	k4	238U
(	(	kIx(	(
<g/>
popř.	popř.	kA	popř.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
92	[number]	k4	92
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
238	[number]	k4	238
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
U	u	k7c2	u
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
~	~	kIx~	~
<g/>
92	[number]	k4	92
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
238	[number]	k4	238
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mbox	mbox	k1gInSc1	mbox
<g/>
{	{	kIx(	{
<g/>
U	u	k7c2	u
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
)	)	kIx)	)
pro	pro	k7c4	pro
izotop	izotop	k1gInSc4	izotop
uranu	uran	k1gInSc2	uran
s	s	k7c7	s
238	[number]	k4	238
nukleony	nukleon	k1gInPc7	nukleon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
slovním	slovní	k2eAgNnSc6d1	slovní
označení	označení	k1gNnSc6	označení
se	se	k3xPyFc4	se
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
přidává	přidávat	k5eAaImIp3nS	přidávat
za	za	k7c4	za
název	název	k1gInSc4	název
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atomové	atomový	k2eAgFnSc6d1	atomová
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
normami	norma	k1gFnPc7	norma
pro	pro	k7c4	pro
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
veličiny	veličina	k1gFnPc4	veličina
a	a	k8xC	a
značky	značka	k1gFnPc4	značka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
oddělovat	oddělovat	k5eAaImF	oddělovat
nukleonové	nukleonový	k2eAgNnSc1d1	nukleonový
číslo	číslo	k1gNnSc1	číslo
mezerou	mezera	k1gFnSc7	mezera
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uran	uran	k1gInSc4	uran
238	[number]	k4	238
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
(	(	kIx(	(
<g/>
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
chemickými	chemický	k2eAgFnPc7d1	chemická
názvoslovnými	názvoslovný	k2eAgFnPc7d1	názvoslovná
normami	norma	k1gFnPc7	norma
<g/>
)	)	kIx)	)
připojovat	připojovat	k5eAaImF	připojovat
nukleonové	nukleonový	k2eAgNnSc4d1	nukleonový
číslo	číslo	k1gNnSc4	číslo
k	k	k7c3	k
názvu	název	k1gInSc3	název
prvku	prvek	k1gInSc2	prvek
spojovníkem	spojovník	k1gInSc7	spojovník
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
např.	např.	kA	např.
uran-	uran-	k?	uran-
<g/>
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Nukleonové	Nukleonový	k2eAgNnSc1d1	Nukleonový
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
rovno	roven	k2eAgNnSc1d1	rovno
součtu	součet	k1gInSc6	součet
atomového	atomový	k2eAgNnSc2d1	atomové
a	a	k8xC	a
neutronového	neutronový	k2eAgNnSc2d1	neutronové
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nukleonové	Nukleonový	k2eAgNnSc1d1	Nukleonový
číslo	číslo	k1gNnSc1	číslo
je	být	k5eAaImIp3nS	být
číselně	číselně	k6eAd1	číselně
blízké	blízký	k2eAgFnSc6d1	blízká
relativní	relativní	k2eAgFnSc6d1	relativní
atomové	atomový	k2eAgFnSc6d1	atomová
hmotnosti	hmotnost	k1gFnSc6	hmotnost
daného	daný	k2eAgNnSc2d1	dané
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
hmotnostní	hmotnostní	k2eAgInSc1d1	hmotnostní
schodek	schodek	k1gInSc1	schodek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
</s>
</p>
<p>
<s>
Atomové	atomový	k2eAgNnSc1d1	atomové
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
<p>
<s>
Neutronové	neutronový	k2eAgNnSc1d1	neutronové
číslo	číslo	k1gNnSc1	číslo
</s>
</p>
