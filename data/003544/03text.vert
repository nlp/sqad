<s>
Svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Freistaat	Freistaat	k2eAgInSc1d1	Freistaat
Bayern	Bayern	k1gInSc1	Bayern
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
největší	veliký	k2eAgFnSc1d3	veliký
ze	z	k7c2	z
šestnácti	šestnáct	k4xCc2	šestnáct
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
do	do	k7c2	do
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pak	pak	k6eAd1	pak
druhá	druhý	k4xOgFnSc1	druhý
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
rakouskými	rakouský	k2eAgFnPc7d1	rakouská
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Solnohradskem	Solnohradsko	k1gNnSc7	Solnohradsko
a	a	k8xC	a
Horními	horní	k2eAgInPc7d1	horní
Rakousy	Rakousy	k1gInPc7	Rakousy
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
rakouskými	rakouský	k2eAgFnPc7d1	rakouská
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
<g/>
,	,	kIx,	,
Vorarlberskem	Vorarlbersko	k1gNnSc7	Vorarlbersko
a	a	k8xC	a
Solnohradskem	Solnohradsko	k1gNnSc7	Solnohradsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Bádenskem-Württemberskem	Bádenskem-Württembersek	k1gInSc7	Bádenskem-Württembersek
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
německými	německý	k2eAgFnPc7d1	německá
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
Hesenskem	Hesensko	k1gNnSc7	Hesensko
<g/>
,	,	kIx,	,
Svobodným	svobodný	k2eAgInSc7d1	svobodný
státem	stát	k1gInSc7	stát
Durynsko	Durynsko	k1gNnSc4	Durynsko
a	a	k8xC	a
Svobodným	svobodný	k2eAgInSc7d1	svobodný
státem	stát	k1gInSc7	stát
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgFnPc4d3	nejbohatší
spolkové	spolkový	k2eAgFnPc4d1	spolková
země	zem	k1gFnPc4	zem
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
odtud	odtud	k6eAd1	odtud
i	i	k9	i
emeritní	emeritní	k2eAgMnSc1d1	emeritní
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Mnichov	Mnichov	k1gInSc1	Mnichov
(	(	kIx(	(
<g/>
München	München	k1gInSc1	München
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Freistaat	Freistaat	k1gInSc1	Freistaat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
demokratický	demokratický	k2eAgInSc4d1	demokratický
a	a	k8xC	a
republikánský	republikánský	k2eAgInSc4d1	republikánský
charakter	charakter	k1gInSc4	charakter
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
uváděno	uvádět	k5eAaImNgNnS	uvádět
<g/>
,	,	kIx,	,
nějaké	nějaký	k3yIgNnSc4	nějaký
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
postavení	postavení	k1gNnSc4	postavení
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgFnPc7d1	ostatní
spolkovými	spolkový	k2eAgFnPc7d1	spolková
zeměmi	zem	k1gFnPc7	zem
SRN	SRN	kA	SRN
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
používá	používat	k5eAaImIp3nS	používat
ze	z	k7c2	z
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
ještě	ještě	k9	ještě
Sasko	Sasko	k1gNnSc1	Sasko
a	a	k8xC	a
Durynsko	Durynsko	k1gNnSc1	Durynsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
historické	historický	k2eAgNnSc4d1	historické
označení	označení	k1gNnSc4	označení
<g/>
;	;	kIx,	;
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
označení	označení	k1gNnSc4	označení
"	"	kIx"	"
<g/>
svobodné	svobodný	k2eAgNnSc4d1	svobodné
hanzovní	hanzovní	k2eAgNnSc4d1	hanzovní
město	město	k1gNnSc4	město
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
svobodné	svobodný	k2eAgFnPc4d1	svobodná
a	a	k8xC	a
hanzovní	hanzovní	k2eAgNnSc1d1	hanzovní
město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Freie	Freie	k1gFnSc1	Freie
Hansestadt	Hansestadt	k1gInSc1	Hansestadt
<g/>
,	,	kIx,	,
Freie	Freie	k1gFnSc1	Freie
und	und	k?	und
Hansestadt	Hansestadt	k1gInSc1	Hansestadt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
spolkové	spolkový	k2eAgFnSc2d1	spolková
země	zem	k1gFnSc2	zem
Hamburk	Hamburk	k1gInSc1	Hamburk
a	a	k8xC	a
Brémy	Brémy	k1gFnPc1	Brémy
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jiný	jiný	k2eAgInSc4d1	jiný
(	(	kIx(	(
<g/>
středověký	středověký	k2eAgInSc4d1	středověký
<g/>
)	)	kIx)	)
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
středověkými	středověký	k2eAgFnPc7d1	středověká
výsadami	výsada	k1gFnPc7	výsada
pro	pro	k7c4	pro
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
kopcovitého	kopcovitý	k2eAgInSc2d1	kopcovitý
rázu	ráz	k1gInSc2	ráz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zde	zde	k6eAd1	zde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jako	jako	k8xS	jako
památka	památka	k1gFnSc1	památka
na	na	k7c4	na
prvohorní	prvohorní	k2eAgNnSc4d1	prvohorní
hercynské	hercynský	k2eAgNnSc4d1	hercynské
vrásnění	vrásnění	k1gNnSc4	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Pahorkatiny	pahorkatina	k1gFnPc1	pahorkatina
severního	severní	k2eAgNnSc2d1	severní
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
břidlicemi	břidlice	k1gFnPc7	břidlice
<g/>
,	,	kIx,	,
granity	granit	k1gInPc7	granit
a	a	k8xC	a
rulami	rula	k1gFnPc7	rula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
místy	místy	k6eAd1	místy
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c4	v
zajímavé	zajímavý	k2eAgInPc4d1	zajímavý
skalní	skalní	k2eAgInPc4d1	skalní
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Fichtelgebirge	Fichtelgebirge	k1gFnSc4	Fichtelgebirge
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Smrčiny	Smrčiny	k1gFnPc4	Smrčiny
<g/>
)	)	kIx)	)
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Schneeberg	Schneeberg	k1gMnSc1	Schneeberg
(	(	kIx(	(
<g/>
1051	[number]	k4	1051
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Frankenwald	Frankenwald	k1gInSc1	Frankenwald
(	(	kIx(	(
<g/>
Francký	francký	k2eAgInSc1d1	francký
les	les	k1gInSc1	les
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Porost	porost	k1gInSc1	porost
býval	bývat	k5eAaImAgInS	bývat
kdysi	kdysi	k6eAd1	kdysi
jedlový	jedlový	k2eAgInSc1d1	jedlový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
nedůležitost	nedůležitost	k1gFnSc4	nedůležitost
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
smrkovým	smrkový	k2eAgInSc7d1	smrkový
porostem	porost	k1gInSc7	porost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
smrky	smrk	k1gInPc1	smrk
podléhají	podléhat	k5eAaImIp3nP	podléhat
populační	populační	k2eAgFnSc4d1	populační
explozi	exploze	k1gFnSc4	exploze
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
zřízen	zřídit	k5eAaPmNgMnS	zřídit
ve	v	k7c6	v
Frankenwaldu	Frankenwald	k1gInSc6	Frankenwald
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
972	[number]	k4	972
km2	km2	k4	km2
přírodní	přírodní	k2eAgInSc4d1	přírodní
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
lesy	les	k1gInPc1	les
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
pouze	pouze	k6eAd1	pouze
polovinu	polovina	k1gFnSc4	polovina
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
park	park	k1gInSc4	park
lákadlem	lákadlo	k1gNnSc7	lákadlo
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
vnější	vnější	k2eAgFnSc1d1	vnější
vápencová	vápencový	k2eAgFnSc1d1	vápencová
linie	linie	k1gFnSc1	linie
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
horského	horský	k2eAgInSc2d1	horský
systému	systém	k1gInSc2	systém
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
Alp	Alpy	k1gFnPc2	Alpy
(	(	kIx(	(
<g/>
nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
ovšem	ovšem	k9	ovšem
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hřeben	hřeben	k1gInSc1	hřeben
na	na	k7c6	na
bavorsko-tyrolských	bavorskoyrolský	k2eAgFnPc6d1	bavorsko-tyrolský
hranicích	hranice	k1gFnPc6	hranice
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
působení	působení	k1gNnSc2	působení
tektonických	tektonický	k2eAgFnPc2d1	tektonická
desek	deska	k1gFnPc2	deska
posunul	posunout	k5eAaPmAgInS	posunout
oproti	oproti	k7c3	oproti
původní	původní	k2eAgFnSc3d1	původní
poloze	poloha	k1gFnSc3	poloha
asi	asi	k9	asi
o	o	k7c6	o
80	[number]	k4	80
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
reliéf	reliéf	k1gInSc1	reliéf
u	u	k7c2	u
okraje	okraj	k1gInSc2	okraj
tak	tak	k6eAd1	tak
prudce	prudko	k6eAd1	prudko
mění	měnit	k5eAaImIp3nS	měnit
charakter	charakter	k1gInSc4	charakter
z	z	k7c2	z
velehor	velehora	k1gFnPc2	velehora
na	na	k7c4	na
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pleistocénu	pleistocén	k1gInSc6	pleistocén
formoval	formovat	k5eAaImAgMnS	formovat
krajinu	krajina	k1gFnSc4	krajina
Alp	Alpy	k1gFnPc2	Alpy
ledovec	ledovec	k1gInSc1	ledovec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nám	my	k3xPp1nPc3	my
zanechal	zanechat	k5eAaPmAgInS	zanechat
četná	četný	k2eAgNnPc4d1	četné
jezera	jezero	k1gNnPc4	jezero
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Chiemsee	Chiemse	k1gFnSc2	Chiemse
nebo	nebo	k8xC	nebo
Ammersee	Ammerse	k1gFnSc2	Ammerse
<g/>
.	.	kIx.	.
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
je	být	k5eAaImIp3nS	být
obdivováno	obdivován	k2eAgNnSc1d1	obdivováno
mnohými	mnohý	k2eAgMnPc7d1	mnohý
zahraničními	zahraniční	k2eAgMnPc7d1	zahraniční
návštěvníky	návštěvník	k1gMnPc7	návštěvník
díky	dík	k1gInPc7	dík
svojí	svojit	k5eAaImIp3nS	svojit
krásné	krásný	k2eAgFnSc3d1	krásná
a	a	k8xC	a
čisté	čistý	k2eAgFnSc3d1	čistá
přírodě	příroda	k1gFnSc3	příroda
<g/>
,	,	kIx,	,
pivu	pivo	k1gNnSc3	pivo
a	a	k8xC	a
klobásám	klobása	k1gFnPc3	klobása
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc6d1	tradiční
lidové	lidový	k2eAgFnSc6d1	lidová
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
jsou	být	k5eAaImIp3nP	být
lyžařská	lyžařský	k2eAgNnPc4d1	lyžařské
centra	centrum	k1gNnPc4	centrum
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
Garmisch-Partenkirchen	Garmisch-Partenkirchen	k1gInSc1	Garmisch-Partenkirchen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
krásných	krásný	k2eAgNnPc2d1	krásné
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
rakouskou	rakouský	k2eAgFnSc7d1	rakouská
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Tyrolskem	Tyrolsko	k1gNnSc7	Tyrolsko
leží	ležet	k5eAaImIp3nS	ležet
i	i	k9	i
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Německa	Německo	k1gNnSc2	Německo
Zugspitze	Zugspitze	k1gFnSc2	Zugspitze
(	(	kIx(	(
<g/>
2962	[number]	k4	2962
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Územím	území	k1gNnSc7	území
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
protéká	protékat	k5eAaImIp3nS	protékat
evropský	evropský	k2eAgInSc1d1	evropský
veletok	veletok	k1gInSc1	veletok
Dunaj	Dunaj	k1gInSc1	Dunaj
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
jiných	jiný	k2eAgFnPc2d1	jiná
řek	řeka	k1gFnPc2	řeka
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Inn	Inn	k1gMnSc1	Inn
<g/>
,	,	kIx,	,
Lech	Lech	k1gMnSc1	Lech
<g/>
,	,	kIx,	,
Mohan	Mohan	k1gInSc1	Mohan
(	(	kIx(	(
<g/>
Main	Main	k1gInSc1	Main
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
</s>
<s>
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
57,2	[number]	k4	57,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
71,9	[number]	k4	71,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
evangelické	evangelický	k2eAgFnSc2d1	evangelická
21,3	[number]	k4	21,3
%	%	kIx~	%
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
26,8	[number]	k4	26,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
muslimské	muslimský	k2eAgFnPc4d1	muslimská
2,2	[number]	k4	2,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
jiná	jiný	k2eAgNnPc1d1	jiné
náboženství	náboženství	k1gNnPc1	náboženství
a	a	k8xC	a
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
19,2	[number]	k4	19,2
%	%	kIx~	%
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Bavoři	Bavor	k1gMnPc1	Bavor
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
4	[number]	k4	4
"	"	kIx"	"
<g/>
kmeny	kmen	k1gInPc1	kmen
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
vlastní	vlastní	k2eAgMnPc1d1	vlastní
<g/>
"	"	kIx"	"
Bavoři	Bavor	k1gMnPc1	Bavor
(	(	kIx(	(
<g/>
Altbayern	Altbayern	k1gMnSc1	Altbayern
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Frankové	Frankové	k2eAgFnSc3d1	Frankové
Švábové	Švábová	k1gFnSc3	Švábová
(	(	kIx(	(
<g/>
Schwaben	Schwabna	k1gFnPc2	Schwabna
<g/>
)	)	kIx)	)
Sudetští	sudetský	k2eAgMnPc1d1	sudetský
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
;	;	kIx,	;
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
vysídlených	vysídlený	k2eAgMnPc2d1	vysídlený
především	především	k6eAd1	především
ze	z	k7c2	z
Sudet	Sudety	k1gInPc2	Sudety
<g/>
)	)	kIx)	)
Hovoří	hovořit	k5eAaImIp3nS	hovořit
se	s	k7c7	s
zde	zde	k6eAd1	zde
několika	několik	k4yIc7	několik
hornoněmeckými	hornoněmecký	k2eAgInPc7d1	hornoněmecký
dialekty	dialekt	k1gInPc7	dialekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
ke	k	k7c3	k
dvěma	dva	k4xCgFnPc3	dva
skupinám	skupina	k1gFnPc3	skupina
<g/>
:	:	kIx,	:
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
(	(	kIx(	(
<g/>
Bairisch	Bairisch	k1gMnSc1	Bairisch
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
většina	většina	k1gFnSc1	většina
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
Horním	horní	k2eAgNnSc6d1	horní
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
a	a	k8xC	a
Horní	horní	k2eAgFnSc3d1	horní
Falci	Falc	k1gFnSc3	Falc
Alemánská	alemánský	k2eAgFnSc1d1	alemánský
(	(	kIx(	(
<g/>
Alemannisch	Alemannisch	k1gMnSc1	Alemannisch
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
Švábů	Šváb	k1gMnPc2	Šváb
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
rozšířená	rozšířený	k2eAgNnPc1d1	rozšířené
i	i	k8xC	i
francká	francký	k2eAgNnPc1d1	francké
nářečí	nářečí	k1gNnPc1	nářečí
(	(	kIx(	(
<g/>
Fränkisch	Fränkisch	k1gInSc1	Fränkisch
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
asi	asi	k9	asi
3	[number]	k4	3
miliony	milion	k4xCgInPc1	milion
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
území	území	k1gNnSc6	území
Dolních	dolní	k2eAgFnPc2d1	dolní
<g/>
,	,	kIx,	,
Horních	horní	k2eAgFnPc2d1	horní
a	a	k8xC	a
Středních	střední	k2eAgFnPc2d1	střední
Frank	Frank	k1gMnSc1	Frank
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
<g/>
:	:	kIx,	:
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Císařové	Císař	k1gMnPc1	Císař
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bavor	Bavor	k1gMnSc1	Bavor
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Albrecht	Albrecht	k1gMnSc1	Albrecht
Císařovna	císařovna	k1gFnSc1	císařovna
<g/>
:	:	kIx,	:
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
Král	Král	k1gMnSc1	Král
<g/>
:	:	kIx,	:
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
,	,	kIx,	,
Ludvík	Ludvík	k1gMnSc1	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
Vévoda	vévoda	k1gMnSc1	vévoda
<g/>
:	:	kIx,	:
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Emanuel	Emanuel	k1gMnSc1	Emanuel
Diktátor	diktátor	k1gMnSc1	diktátor
<g/>
:	:	kIx,	:
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
Sportovci	sportovec	k1gMnPc1	sportovec
<g/>
:	:	kIx,	:
Bastian	Bastian	k1gInSc1	Bastian
Schweinsteiger	Schweinsteigra	k1gFnPc2	Schweinsteigra
Malíři	malíř	k1gMnPc1	malíř
<g/>
:	:	kIx,	:
Hans	Hans	k1gMnSc1	Hans
Holbein	Holbein	k1gMnSc1	Holbein
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
Dürer	Dürer	k1gMnSc1	Dürer
<g/>
,	,	kIx,	,
Albrecht	Albrecht	k1gMnSc1	Albrecht
Altdorfer	Altdorfer	k1gMnSc1	Altdorfer
<g/>
,	,	kIx,	,
Lucas	Lucas	k1gMnSc1	Lucas
Cranach	Cranach	k1gMnSc1	Cranach
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
Carl	Carl	k1gMnSc1	Carl
Spitzweg	Spitzweg	k1gMnSc1	Spitzweg
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
von	von	k1gInSc1	von
Lenbach	Lenbach	k1gInSc1	Lenbach
<g/>
,	,	kIx,	,
Franz	Franz	k1gInSc1	Franz
von	von	k1gInSc1	von
Stuck	Stuck	k1gInSc1	Stuck
a	a	k8xC	a
Franz	Franz	k1gMnSc1	Franz
Marc	Marc	k1gInSc4	Marc
Hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Orlando	Orlando	k6eAd1	Orlando
di	di	k?	di
Lasso	Lassa	k1gFnSc5	Lassa
<g/>
,	,	kIx,	,
Christoph	Christoph	k1gMnSc1	Christoph
Willibald	Willibald	k1gMnSc1	Willibald
Gluck	Gluck	k1gMnSc1	Gluck
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Wagner	Wagner	k1gMnSc1	Wagner
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Strauss	Strauss	k1gInSc1	Strauss
a	a	k8xC	a
Theobald	Theobald	k1gMnSc1	Theobald
Böhm	Böhm	k1gMnSc1	Böhm
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
moderní	moderní	k2eAgFnSc2d1	moderní
flétny	flétna	k1gFnSc2	flétna
Spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
:	:	kIx,	:
Hans	Hans	k1gMnSc1	Hans
Sachs	Sachsa	k1gFnPc2	Sachsa
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Wedekind	Wedekind	k1gMnSc1	Wedekind
<g/>
,	,	kIx,	,
Christian	Christian	k1gMnSc1	Christian
Morgenstern	Morgenstern	k1gMnSc1	Morgenstern
<g/>
,	,	kIx,	,
Oskar	Oskar	k1gMnSc1	Oskar
Maria	Maria	k1gFnSc1	Maria
Graf	graf	k1gInSc1	graf
<g/>
,	,	kIx,	,
Bertolt	Bertolt	k2eAgMnSc1d1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Lion	Lion	k1gMnSc1	Lion
Feuchtwanger	Feuchtwangra	k1gFnPc2	Feuchtwangra
Vědci	vědec	k1gMnPc1	vědec
<g/>
:	:	kIx,	:
Adam	Adam	k1gMnSc1	Adam
Ries	Riesa	k1gFnPc2	Riesa
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gInSc1	Joseph
von	von	k1gInSc1	von
Fraunhofer	Fraunhofer	k1gInSc1	Fraunhofer
<g/>
,	,	kIx,	,
Max	max	kA	max
Joseph	Joseph	k1gInSc1	Joseph
von	von	k1gInSc1	von
Pettenkofer	Pettenkofer	k1gInSc1	Pettenkofer
<g/>
,	,	kIx,	,
Georg	Georg	k1gInSc1	Georg
Simon	Simon	k1gMnSc1	Simon
Ohm	ohm	k1gInSc1	ohm
<g/>
,	,	kIx,	,
Carl	Carl	k1gInSc1	Carl
von	von	k1gInSc1	von
Lindea	Lindea	k1gFnSc1	Lindea
<g/>
,	,	kIx,	,
Sebastian	Sebastian	k1gMnSc1	Sebastian
Kneipp	Kneipp	k1gMnSc1	Kneipp
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Alzheimer	Alzheimer	k1gMnSc1	Alzheimer
<g/>
,	,	kIx,	,
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Conrad	Conrada	k1gFnPc2	Conrada
Röntgen	Röntgen	k1gInSc1	Röntgen
<g/>
,	,	kIx,	,
Werner	Werner	k1gMnSc1	Werner
Heisenberg	Heisenberg	k1gMnSc1	Heisenberg
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g />
.	.	kIx.	.
</s>
<s>
Mößbauer	Mößbauer	k1gMnSc1	Mößbauer
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Huber	Hubra	k1gFnPc2	Hubra
Vynálezci	vynálezce	k1gMnPc1	vynálezce
<g/>
:	:	kIx,	:
Martin	Martin	k1gMnSc1	Martin
Behaim	Behaim	k1gMnSc1	Behaim
<g/>
,	,	kIx,	,
Levi	Levi	k1gNnSc1	Levi
Strauss	Straussa	k1gFnPc2	Straussa
a	a	k8xC	a
Rudolf	Rudolf	k1gMnSc1	Rudolf
Diesel	diesel	k1gInSc4	diesel
Ostatní	ostatní	k2eAgInSc4d1	ostatní
<g/>
:	:	kIx,	:
členové	člen	k1gMnPc1	člen
protinacistické	protinacistický	k2eAgFnSc2d1	protinacistická
skupiny	skupina	k1gFnSc2	skupina
Bílá	bílý	k2eAgFnSc1d1	bílá
růže	růže	k1gFnSc1	růže
Svobodný	svobodný	k2eAgInSc4d1	svobodný
stát	stát	k5eAaImF	stát
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
7	[number]	k4	7
vládních	vládní	k2eAgInPc2d1	vládní
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
Regierungsbezirke	Regierungsbezirke	k1gInSc1	Regierungsbezirke
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Dolní	dolní	k2eAgNnSc1d1	dolní
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
(	(	kIx(	(
<g/>
Niederbayern	Niederbayern	k1gNnSc1	Niederbayern
<g/>
)	)	kIx)	)
Dolní	dolní	k2eAgInPc1d1	dolní
Franky	Franky	k1gInPc1	Franky
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Unterfranken	Unterfranken	k2eAgMnSc1d1	Unterfranken
<g/>
)	)	kIx)	)
Horní	horní	k2eAgNnSc1d1	horní
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
(	(	kIx(	(
<g/>
Oberbayern	Oberbayern	k1gNnSc1	Oberbayern
<g/>
)	)	kIx)	)
Horní	horní	k2eAgFnSc1d1	horní
Falc	Falc	k1gFnSc1	Falc
(	(	kIx(	(
<g/>
Oberpfalz	Oberpfalz	k1gInSc1	Oberpfalz
<g/>
)	)	kIx)	)
Horní	horní	k2eAgInPc1d1	horní
Franky	Franky	k1gInPc1	Franky
(	(	kIx(	(
<g/>
Oberfranken	Oberfranken	k1gInSc1	Oberfranken
<g/>
)	)	kIx)	)
Střední	střední	k2eAgInPc1d1	střední
Franky	Franky	k1gInPc1	Franky
(	(	kIx(	(
<g/>
Mittelfranken	Mittelfranken	k1gInSc1	Mittelfranken
<g/>
)	)	kIx)	)
Švábsko	Švábsko	k1gNnSc1	Švábsko
(	(	kIx(	(
<g/>
Schwaben	Schwabna	k1gFnPc2	Schwabna
<g/>
)	)	kIx)	)
Výše	výše	k1gFnSc1	výše
jmenovaných	jmenovaný	k2eAgInPc2d1	jmenovaný
7	[number]	k4	7
vládních	vládní	k2eAgInPc2d1	vládní
obvodů	obvod	k1gInPc2	obvod
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
71	[number]	k4	71
zemských	zemský	k2eAgInPc2d1	zemský
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
Landkreise	Landkreise	k1gFnSc2	Landkreise
<g/>
)	)	kIx)	)
a	a	k8xC	a
25	[number]	k4	25
městských	městský	k2eAgInPc2d1	městský
okresů	okres	k1gInPc2	okres
(	(	kIx(	(
<g/>
Kreisfreie	Kreisfreius	k1gMnSc5	Kreisfreius
Städte	Städt	k1gMnSc5	Städt
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
25	[number]	k4	25
bavorských	bavorský	k2eAgInPc2d1	bavorský
městských	městský	k2eAgInPc2d1	městský
okresů	okres	k1gInPc2	okres
<g/>
:	:	kIx,	:
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
má	mít	k5eAaImIp3nS	mít
jednokomorový	jednokomorový	k2eAgInSc1d1	jednokomorový
sněm	sněm	k1gInSc1	sněm
(	(	kIx(	(
<g/>
Landtag	Landtag	k1gInSc1	Landtag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
druhá	druhý	k4xOgFnSc1	druhý
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
vybráni	vybrat	k5eAaPmNgMnP	vybrat
společenskými	společenský	k2eAgFnPc7d1	společenská
a	a	k8xC	a
ekonomickými	ekonomický	k2eAgFnPc7d1	ekonomická
skupinami	skupina	k1gFnPc7	skupina
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
referendem	referendum	k1gNnSc7	referendum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
Ministerpräsident	Ministerpräsident	k1gMnSc1	Ministerpräsident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
země	zem	k1gFnSc2	zem
dominuje	dominovat	k5eAaImIp3nS	dominovat
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
strana	strana	k1gFnSc1	strana
CSU	CSU	kA	CSU
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
sesterskou	sesterský	k2eAgFnSc4d1	sesterská
<g/>
"	"	kIx"	"
stranou	stranou	k6eAd1	stranou
celoněmecké	celoněmecký	k2eAgInPc1d1	celoněmecký
CDU	CDU	kA	CDU
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
CSU	CSU	kA	CSU
v	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
nekandiduje	kandidovat	k5eNaImIp3nS	kandidovat
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
jiné	jiný	k2eAgFnSc6d1	jiná
spolkové	spolkový	k2eAgFnSc3d1	spolková
zemi	zem	k1gFnSc3	zem
<g/>
,	,	kIx,	,
CDU	CDU	kA	CDU
nikdy	nikdy	k6eAd1	nikdy
nekandiduje	kandidovat	k5eNaImIp3nS	kandidovat
v	v	k7c6	v
zemských	zemský	k2eAgFnPc6d1	zemská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
CSU	CSU	kA	CSU
nepřetržitě	přetržitě	k6eNd1	přetržitě
a	a	k8xC	a
absolutní	absolutní	k2eAgFnSc7d1	absolutní
většinou	většina	k1gFnSc7	většina
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
,	,	kIx,	,
v	v	k7c6	v
několika	několik	k4yIc6	několik
volbách	volba	k1gFnPc6	volba
podíl	podíl	k1gInSc4	podíl
hlasů	hlas	k1gInPc2	hlas
pro	pro	k7c4	pro
CSU	CSU	kA	CSU
přesáhl	přesáhnout	k5eAaPmAgInS	přesáhnout
60	[number]	k4	60
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
dokonce	dokonce	k9	dokonce
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
dvoutřetinové	dvoutřetinový	k2eAgFnSc2d1	dvoutřetinová
většiny	většina	k1gFnSc2	většina
mandátů	mandát	k1gInPc2	mandát
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
nedosáhla	dosáhnout	k5eNaPmAgFnS	dosáhnout
žádná	žádný	k3yNgFnSc1	žádný
jiná	jiný	k2eAgFnSc1d1	jiná
strana	strana	k1gFnSc1	strana
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
jiné	jiný	k2eAgFnSc6d1	jiná
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
ovšem	ovšem	k9	ovšem
absolutní	absolutní	k2eAgFnSc4d1	absolutní
většinu	většina	k1gFnSc4	většina
těsně	těsně	k6eAd1	těsně
ztratila	ztratit	k5eAaPmAgFnS	ztratit
<g/>
,	,	kIx,	,
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
ale	ale	k9	ale
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
daleko	daleko	k6eAd1	daleko
nejsilnější	silný	k2eAgFnSc7d3	nejsilnější
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Bavorština	bavorština	k1gFnSc1	bavorština
Seznam	seznam	k1gInSc1	seznam
bavorských	bavorský	k2eAgMnPc2d1	bavorský
vládců	vládce	k1gMnPc2	vládce
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
vévodství	vévodství	k1gNnSc4	vévodství
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
hymna	hymna	k1gFnSc1	hymna
Válka	válka	k1gFnSc1	válka
o	o	k7c4	o
bavorské	bavorský	k2eAgNnSc4d1	bavorské
dědictví	dědictví	k1gNnSc4	dědictví
Franky	Franky	k1gInPc1	Franky
Švábsko	Švábsko	k1gNnSc4	Švábsko
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7277-458-6	[number]	k4	978-80-7277-458-6
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc2	téma
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
www.bayern.de	www.bayern.de	k6eAd1	www.bayern.de
–	–	k?	–
Informační	informační	k2eAgInSc1d1	informační
server	server	k1gInSc1	server
bavorské	bavorský	k2eAgFnSc2d1	bavorská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
www.bayerischer-wald.de	www.bayerischerald.de	k6eAd1	www.bayerischer-wald.de
-	-	kIx~	-
Informační	informační	k2eAgInSc1d1	informační
server	server	k1gInSc1	server
oblasti	oblast	k1gFnSc2	oblast
Bavorský	bavorský	k2eAgInSc4d1	bavorský
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
