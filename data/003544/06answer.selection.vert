<s>
Svobodný	svobodný	k2eAgInSc1d1	svobodný
stát	stát	k1gInSc1	stát
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Freistaat	Freistaat	k2eAgInSc1d1	Freistaat
Bayern	Bayern	k1gInSc1	Bayern
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
největší	veliký	k2eAgFnSc1d3	veliký
ze	z	k7c2	z
šestnácti	šestnáct	k4xCc2	šestnáct
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
do	do	k7c2	do
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pak	pak	k6eAd1	pak
druhá	druhý	k4xOgFnSc1	druhý
nejlidnatější	lidnatý	k2eAgFnSc1d3	nejlidnatější
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
