<s>
Skákavka	Skákavka	k1gFnSc1
bělovlasá	bělovlasý	k2eAgFnSc1d1
</s>
<s>
Skákavka	Skákavka	k1gFnSc1
bělovlasá	bělovlasý	k2eAgFnSc1d1
Samec	samec	k1gInSc4
skákavky	skákavek	k1gInPc4
bělovlasé	bělovlasý	k2eAgInPc4d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
klepítkatci	klepítkatec	k1gMnPc1
(	(	kIx(
<g/>
Chelicerata	Chelicerat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
pavoukovci	pavoukovec	k1gMnPc1
(	(	kIx(
<g/>
Arachnida	Arachnida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pavouci	pavouk	k1gMnPc1
(	(	kIx(
<g/>
Araneae	Araneae	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
skákavkovití	skákavkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Salticidae	Salticidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
Euophrys	Euophrys	k6eAd1
Binomické	binomický	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
''	''	k?
<g/>
Euophrys	Euophrys	k1gInSc1
frontalis	frontalis	k1gFnSc2
<g/>
''	''	k?
<g/>
Walckenaer	Walckenaer	k1gMnSc1
<g/>
,	,	kIx,
1802	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Skákavka	Skákavka	k1gFnSc1
bělovlasá	bělovlasý	k2eAgFnSc1d1
(	(	kIx(
<g/>
Euophrys	Euophrys	k1gInSc1
frontalis	frontalis	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
pavouka	pavouk	k1gMnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
skákavkovití	skákavkovitý	k2eAgMnPc1d1
a	a	k8xC
jediný	jediný	k2eAgInSc4d1
druh	druh	k1gInSc4
rodu	rod	k1gInSc2
Euophrys	Euophrys	k1gInSc4
vyskytující	vyskytující	k2eAgInSc4d1
se	se	k3xPyFc4
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Samice	samice	k1gFnPc1
dorůstají	dorůstat	k5eAaImIp3nP
velikosti	velikost	k1gFnSc2
3	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
samci	samec	k1gInSc6
2	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hlavohruď	hlavohruď	k1gFnSc1
samice	samice	k1gFnSc2
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
krátká	krátký	k2eAgFnSc1d1
a	a	k8xC
žlutavá	žlutavý	k2eAgFnSc1d1
s	s	k7c7
tmavšími	tmavý	k2eAgInPc7d2
okraji	okraj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolí	okolí	k1gNnSc1
očí	oko	k1gNnPc2
je	být	k5eAaImIp3nS
hnědé	hnědý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadeček	zadeček	k1gInSc1
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
žlutohnědý	žlutohnědý	k2eAgInSc1d1
s	s	k7c7
hnědými	hnědý	k2eAgFnPc7d1
nebo	nebo	k8xC
černými	černý	k2eAgFnPc7d1
protáhlými	protáhlý	k2eAgFnPc7d1
skvrnami	skvrna	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
uprostřed	uprostřed	k6eAd1
seskupeny	seskupen	k2eAgInPc1d1
do	do	k7c2
podélných	podélný	k2eAgInPc2d1
proužků	proužek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nohy	noha	k1gFnPc1
jsou	být	k5eAaImIp3nP
u	u	k7c2
samic	samice	k1gFnPc2
žluté	žlutý	k2eAgNnSc1d1
s	s	k7c7
tmavším	tmavý	k2eAgNnSc7d2
ochlupením	ochlupení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
samců	samec	k1gMnPc2
je	být	k5eAaImIp3nS
hlavohruď	hlavohruď	k1gFnSc1
variabilně	variabilně	k6eAd1
hnědá	hnědat	k5eAaImIp3nS
<g/>
,	,	kIx,
nad	nad	k7c7
předníma	přední	k2eAgNnPc7d1
očima	oko	k1gNnPc7
světlejší	světlý	k2eAgInSc4d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgNnSc1d1
oči	oko	k1gNnPc1
jsou	být	k5eAaImIp3nP
nápadně	nápadně	k6eAd1
orámované	orámovaný	k2eAgInPc4d1
oranžovými	oranžový	k2eAgInPc7d1
nebo	nebo	k8xC
červenými	červený	k2eAgInPc7d1
chloupky	chloupek	k1gInPc7
<g/>
,	,	kIx,
hned	hned	k6eAd1
nad	nad	k7c7
nimi	on	k3xPp3gInPc7
jsou	být	k5eAaImIp3nP
chloupky	chloupek	k1gInPc1
zlatožluté	zlatožlutý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadeček	zadeček	k1gInSc1
je	být	k5eAaImIp3nS
světle	světle	k6eAd1
hnědý	hnědý	k2eAgInSc1d1
s	s	k7c7
příčnými	příčný	k2eAgFnPc7d1
tmavými	tmavý	k2eAgFnPc7d1
klikatými	klikatý	k2eAgFnPc7d1
linkami	linka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
pár	pár	k4xCyI
nohou	noha	k1gFnPc6
a	a	k8xC
femury	femura	k1gFnSc2
druhého	druhý	k4xOgInSc2
páru	pár	k1gInSc2
jsou	být	k5eAaImIp3nP
tmavé	tmavý	k2eAgFnPc1d1
<g/>
,	,	kIx,
tarzy	tarza	k1gFnSc2
prvního	první	k4xOgInSc2
páru	pár	k1gInSc2
jsou	být	k5eAaImIp3nP
bílé	bílý	k2eAgFnPc1d1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
nažloutlé	nažloutlý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
makadel	makadlo	k1gNnPc2
jsou	být	k5eAaImIp3nP
svazečky	svazeček	k1gInPc1
dlouhých	dlouhý	k2eAgInPc2d1
bílých	bílý	k2eAgInPc2d1
chlupů	chlup	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
při	při	k7c6
pohledu	pohled	k1gInSc6
zepředu	zepředu	k6eAd1
na	na	k7c4
živého	živý	k2eAgMnSc4d1
samce	samec	k1gMnSc4
v	v	k7c6
klidné	klidný	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nS
nápadné	nápadný	k2eAgNnSc1d1
bílé	bílé	k1gNnSc1
V.	V.	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kopulační	kopulační	k2eAgInPc1d1
orgány	orgán	k1gInPc1
samic	samice	k1gFnPc2
nelze	lze	k6eNd1
odlišit	odlišit	k5eAaPmF
od	od	k7c2
kopulačních	kopulační	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
druhu	druh	k1gInSc2
Euophrys	Euophrysa	k1gFnPc2
herbigrada	herbigrada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samice	samice	k1gFnPc1
skákavky	skákavka	k1gFnSc2
bělovlasé	bělovlasý	k2eAgFnPc1d1
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
palearktický	palearktický	k2eAgInSc4d1
druh	druh	k1gInSc4
s	s	k7c7
výskytem	výskyt	k1gInSc7
i	i	k9
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
hojný	hojný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Způsob	způsob	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
od	od	k7c2
nížin	nížina	k1gFnPc2
do	do	k7c2
středních	střední	k2eAgFnPc2d1
poloh	poloha	k1gFnPc2
<g/>
,	,	kIx,
vzácně	vzácně	k6eAd1
i	i	k9
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezdá	zdát	k5eNaImIp3nS,k5eNaPmIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
přesně	přesně	k6eAd1
vymezenou	vymezený	k2eAgFnSc4d1
niku	nika	k1gFnSc4
stanoviště	stanoviště	k1gNnSc2
<g/>
,	,	kIx,
obývá	obývat	k5eAaImIp3nS
skalní	skalní	k2eAgFnPc4d1
stepi	step	k1gFnPc4
<g/>
,	,	kIx,
rašeliniště	rašeliniště	k1gNnPc4
<g/>
,	,	kIx,
mýtiny	mýtina	k1gFnPc4
i	i	k8xC
lesy	les	k1gInPc4
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
okraje	okraj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
stromech	strom	k1gInPc6
<g/>
,	,	kIx,
v	v	k7c6
nízké	nízký	k2eAgFnSc6d1
vegetaci	vegetace	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
mechu	mech	k1gInSc6
<g/>
,	,	kIx,
detritu	detrit	k1gInSc6
a	a	k8xC
pod	pod	k7c7
kameny	kámen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc1d1
ve	v	k7c6
dne	den	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
dospělci	dospělec	k1gMnPc7
se	se	k3xPyFc4
lze	lze	k6eAd1
setkat	setkat	k5eAaPmF
od	od	k7c2
dubna	duben	k1gInSc2
do	do	k7c2
října	říjen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
hlavně	hlavně	k9
v	v	k7c6
květnu	květen	k1gInSc6
<g/>
,	,	kIx,
červnu	červen	k1gInSc6
a	a	k8xC
červenci	červenec	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Samec	samec	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
páření	páření	k1gNnSc2
předvádí	předvádět	k5eAaImIp3nS
svatební	svatební	k2eAgInSc4d1
tanec	tanec	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mává	mávat	k5eAaImIp3nS
doširoka	doširoka	k6eAd1
rozevřenýma	rozevřený	k2eAgFnPc7d1
předníma	přední	k2eAgFnPc7d1
nohama	noha	k1gFnPc7
a	a	k8xC
kontrastně	kontrastně	k6eAd1
zbarvenými	zbarvený	k2eAgNnPc7d1
makadly	makadlo	k1gNnPc7
a	a	k8xC
klepe	klepat	k5eAaImIp3nS
břichem	břicho	k1gNnSc7
do	do	k7c2
podkladu	podklad	k1gInSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
slyšitelné	slyšitelný	k2eAgNnSc1d1
lidským	lidský	k2eAgNnSc7d1
uchem	ucho	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
upoutal	upoutat	k5eAaPmAgMnS
pozornost	pozornost	k1gFnSc4
samice	samice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Samice	samice	k1gFnSc1
hlídá	hlídat	k5eAaImIp3nS
kokon	kokon	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
asi	asi	k9
20	#num#	k4
vajíček	vajíčko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Euophrys	Euophrysa	k1gFnPc2
frontalis	frontalis	k1gFnSc2
–	–	k?
Wiki	Wik	k1gFnSc2
der	drát	k5eAaImRp2nS
Arachnologischen	Arachnologischno	k1gNnPc2
Gesellschaft	Gesellschaft	k1gInSc1
e.	e.	k?
V.	V.	kA
<g/>
.	.	kIx.
wiki	wiki	k1gNnSc1
<g/>
.	.	kIx.
<g/>
arages	arages	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
KŮRKA	kůrka	k1gFnSc1
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
spol	spol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pavouci	pavouk	k1gMnPc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
546	#num#	k4
<g/>
–	–	k?
<g/>
547	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Summary	Summara	k1gFnPc4
for	forum	k1gNnPc2
Euophrys	Euophrysa	k1gFnPc2
frontalis	frontalis	k1gFnSc1
(	(	kIx(
<g/>
Araneae	Araneae	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
srs	srs	k?
<g/>
.	.	kIx.
<g/>
britishspiders	britishspiders	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
.	.	kIx.
<g/>
uk	uk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
box-sizing	box-sizing	k1gInSc1
<g/>
:	:	kIx,
<g/>
border-box	border-box	k1gInSc1
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
<g/>
;	;	kIx,
<g/>
width	widtha	k1gFnPc2
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
;	;	kIx,
<g/>
clear	clear	k1gInSc1
<g/>
:	:	kIx,
<g/>
both	both	k1gInSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gInSc1
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
em	em	k?
auto	auto	k1gNnSc1
0	#num#	k4
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
{	{	kIx(
<g/>
margin-top	margin-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-inner	-innra	k1gFnPc2
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
width	width	k1gMnSc1
<g/>
:	:	kIx,
<g/>
100	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
-group	-group	k1gInSc1
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-titlat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.25	0.25	k4
<g/>
em	em	k?
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
;	;	kIx,
<g/>
text-align	text-aligno	k1gNnPc2
<g/>
:	:	kIx,
<g/>
center	centrum	k1gNnPc2
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc1
<g/>
{	{	kIx(
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
;	;	kIx,
<g/>
text-align	text-align	k1gMnSc1
<g/>
:	:	kIx,
<g/>
right	right	k1gMnSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gMnSc1
<g/>
{	{	kIx(
<g/>
line-height	line-height	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1.5	1.5	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k6eAd1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gMnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-image	-imagat	k5eAaPmIp3nS
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
tr	tr	k?
<g/>
+	+	kIx~
<g/>
tr	tr	k?
<g/>
>	>	kIx)
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-list	-list	k1gInSc1
<g/>
{	{	kIx(
<g/>
border-top	border-top	k1gInSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g />
.	.	kIx.
</s>
<s hack="1">
solid	solid	k1gInSc1
#	#	kIx~
<g/>
fdfdfd	fdfdfd	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
th	th	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
e	e	k0
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
th	th	k?
<g/>
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-title	-title	k1gFnSc2
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
88	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc4d1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-group	-group	k1gInSc4
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc4
.	.	kIx.
<g/>
navbox	navbox	k1gInSc4
<g/>
2	#num#	k4
<g/>
-subgroup	-subgroup	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-abovebelow	-abovebelow	k?
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
f	f	k?
<g/>
0	#num#	k4
<g/>
f	f	k?
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
<g/>
-even	-evna	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
f	f	k?
<g/>
7	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2	#num#	k4
<g/>
-odd	-odda	k1gFnPc2
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
transparent	transparent	k1gInSc1
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
.	.	kIx.
<g/>
hlist	hlist	k1gFnSc1
td	td	k?
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
dl	dl	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ol	ol	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox	navbox	k1gInSc1
<g/>
2	#num#	k4
td	td	k?
<g/>
.	.	kIx.
<g/>
hlist	hlist	k1gMnSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.125	0.125	k4
<g/>
em	em	k?
0	#num#	k4
<g/>
}	}	kIx)
<g/>
Identifikátory	identifikátor	k1gInPc1
taxonu	taxon	k1gInSc2
</s>
<s>
Wikidata	Wikidata	k1gFnSc1
<g/>
:	:	kIx,
Q1058583	Q1058583	k1gFnSc1
</s>
<s>
Wikidruhy	Wikidruha	k1gFnPc1
<g/>
:	:	kIx,
Euophrys	Euophrys	k1gInSc1
frontalis	frontalis	k1gFnSc2
</s>
<s>
Araneae	Araneae	k1gFnSc1
<g/>
:	:	kIx,
434	#num#	k4
</s>
<s>
BioLib	BioLib	k1gInSc1
<g/>
:	:	kIx,
940	#num#	k4
</s>
<s>
BOLD	BOLD	kA
<g/>
:	:	kIx,
235630	#num#	k4
</s>
<s>
NDOP	NDOP	kA
<g/>
:	:	kIx,
328	#num#	k4
</s>
<s>
EoL	EoL	k?
<g/>
:	:	kIx,
1211556	#num#	k4
</s>
<s>
EUNIS	EUNIS	kA
<g/>
:	:	kIx,
227379	#num#	k4
</s>
<s>
Fauna	fauna	k1gFnSc1
Europaea	Europaea	k1gFnSc1
<g/>
:	:	kIx,
352600	#num#	k4
</s>
<s>
Fauna	fauna	k1gFnSc1
Europaea	Europaea	k1gFnSc1
(	(	kIx(
<g/>
new	new	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
c	c	k0
<g/>
2	#num#	k4
<g/>
b	b	k?
<g/>
784	#num#	k4
<g/>
da-cf	da-cf	k1gInSc1
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
436	#num#	k4
<g/>
c-a	c-a	k?
<g/>
7	#num#	k4
<g/>
e	e	k0
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
b	b	k?
<g/>
2	#num#	k4
<g/>
e	e	k0
<g/>
108268	#num#	k4
<g/>
c	c	k0
</s>
<s>
GBIF	GBIF	kA
<g/>
:	:	kIx,
5172110	#num#	k4
</s>
<s>
iNaturalist	iNaturalist	k1gInSc1
<g/>
:	:	kIx,
362098	#num#	k4
</s>
<s>
IRMNG	IRMNG	kA
<g/>
:	:	kIx,
10483171	#num#	k4
</s>
<s>
ITIS	ITIS	kA
<g/>
:	:	kIx,
879532	#num#	k4
</s>
<s>
NBN	NBN	kA
<g/>
:	:	kIx,
NBNSYS0000008771	NBNSYS0000008771	k1gMnSc1
</s>
<s>
NCBI	NCBI	kA
<g/>
:	:	kIx,
1317291	#num#	k4
</s>
<s>
WoRMS	WoRMS	k?
<g/>
:	:	kIx,
231972	#num#	k4
</s>
<s>
WSC	WSC	kA
<g/>
:	:	kIx,
urn	urn	k?
<g/>
:	:	kIx,
<g/>
lsid	lsid	k1gMnSc1
<g/>
:	:	kIx,
<g/>
nmbe	nmbat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
:	:	kIx,
<g/>
spidersp	spidersp	k1gMnSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
33272	#num#	k4
</s>
