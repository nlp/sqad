<s>
Samec	samec	k1gMnSc1
v	v	k7c6
době	doba	k1gFnSc6
páření	páření	k1gNnSc2
předvádí	předvádět	k5eAaImIp3nS
svatební	svatební	k2eAgInSc4d1
tanec	tanec	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
mává	mávat	k5eAaImIp3nS
doširoka	doširoka	k6eAd1
rozevřenýma	rozevřený	k2eAgFnPc7d1
předníma	přední	k2eAgFnPc7d1
nohama	noha	k1gFnPc7
a	a	k8xC
kontrastně	kontrastně	k6eAd1
zbarvenými	zbarvený	k2eAgNnPc7d1
makadly	makadlo	k1gNnPc7
a	a	k8xC
klepe	klepat	k5eAaImIp3nS
břichem	břicho	k1gNnSc7
do	do	k7c2
podkladu	podklad	k1gInSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
slyšitelné	slyšitelný	k2eAgNnSc1d1
lidským	lidský	k2eAgNnSc7d1
uchem	ucho	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
upoutal	upoutat	k5eAaPmAgMnS
pozornost	pozornost	k1gFnSc4
samice	samice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Samice	samice	k1gFnSc1
hlídá	hlídat	k5eAaImIp3nS
kokon	kokon	k1gInSc4
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
asi	asi	k9
20	#num#	k4
vajíček	vajíčko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>