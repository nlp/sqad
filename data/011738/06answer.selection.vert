<s>
Agáve	agáve	k1gFnPc1	agáve
přirozeně	přirozeně	k6eAd1	přirozeně
rostou	růst	k5eAaImIp3nP	růst
především	především	k9	především
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
najdeme	najít	k5eAaPmIp1nP	najít
je	on	k3xPp3gNnSc4	on
i	i	k9	i
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
a	a	k8xC	a
západních	západní	k2eAgInPc6d1	západní
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
