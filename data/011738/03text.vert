<p>
<s>
Agáve	agáve	k1gNnSc1	agáve
(	(	kIx(	(
<g/>
Agave	Agaev	k1gFnPc1	Agaev
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
podle	podle	k7c2	podle
Agaue	Agaue	k1gNnSc2	Agaue
<g/>
,	,	kIx,	,
jména	jméno	k1gNnSc2	jméno
matky	matka	k1gFnSc2	matka
Pentheovy	Pentheův	k2eAgFnPc1d1	Pentheův
<g/>
,	,	kIx,	,
ze	z	k7c2	z
starořeckého	starořecký	k2eAgNnSc2d1	starořecké
agauós	agauós	k6eAd1	agauós
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgFnSc1d1	znamenající
proslulá	proslulý	k2eAgFnSc1d1	proslulá
<g/>
,	,	kIx,	,
vznešená	vznešený	k2eAgFnSc1d1	vznešená
<g/>
,	,	kIx,	,
úžasně	úžasně	k6eAd1	úžasně
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
obdivuhodná	obdivuhodný	k2eAgFnSc1d1	obdivuhodná
(	(	kIx(	(
<g/>
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ušlechtilá	ušlechtilý	k2eAgFnSc1d1	ušlechtilá
<g/>
,	,	kIx,	,
urozená	urozený	k2eAgFnSc1d1	urozená
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
rod	rod	k1gInSc4	rod
sukulentních	sukulentní	k2eAgFnPc2d1	sukulentní
rostlin	rostlina	k1gFnPc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
chřestovitých	chřestovitý	k2eAgFnPc2d1	chřestovitý
(	(	kIx(	(
<g/>
Asparagaceae	Asparagaceae	k1gFnPc2	Asparagaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
mají	mít	k5eAaImIp3nP	mít
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
využití	využití	k1gNnSc1	využití
–	–	k?	–
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vlákno	vlákno	k1gNnSc4	vlákno
sisal	sisal	k1gInSc1	sisal
a	a	k8xC	a
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
modré	modrý	k2eAgFnSc2d1	modrá
agáve	agáve	k1gFnSc2	agáve
se	se	k3xPyFc4	se
pálí	pálit	k5eAaImIp3nS	pálit
tequila	tequila	k1gFnSc1	tequila
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
agáve	agáve	k1gFnSc2	agáve
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pálenka	pálenka	k1gFnSc1	pálenka
mezcal	mezcat	k5eAaPmAgMnS	mezcat
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
kvašením	kvašení	k1gNnSc7	kvašení
šťávy	šťáva	k1gFnSc2	šťáva
z	z	k7c2	z
agáví	agáví	k1gNnSc2	agáví
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
bělavý	bělavý	k2eAgInSc1d1	bělavý
a	a	k8xC	a
mazlavý	mazlavý	k2eAgInSc1d1	mazlavý
nápoj	nápoj	k1gInSc1	nápoj
pulque	pulqu	k1gInSc2	pulqu
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběn	vyráběn	k2eAgMnSc1d1	vyráběn
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oblíben	oblíbit	k5eAaPmNgMnS	oblíbit
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
Indiány	Indián	k1gMnPc7	Indián
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
českých	český	k2eAgFnPc2d1	Česká
kodifikačních	kodifikační	k2eAgFnPc2d1	kodifikační
příruček	příručka	k1gFnPc2	příručka
je	být	k5eAaImIp3nS	být
agáve	agáve	k1gNnSc1	agáve
buď	buď	k8xC	buď
neutrum	neutrum	k1gNnSc1	neutrum
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
femininum	femininum	k1gNnSc1	femininum
<g/>
,	,	kIx,	,
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
nesklonné	sklonný	k2eNgFnSc2d1	nesklonná
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
pádě	pád	k1gInSc6	pád
sg.	sg.	k?	sg.
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ASCS	ASCS	kA	ASCS
i	i	k8xC	i
NASCS	NASCS	kA	NASCS
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
akademické	akademický	k2eAgFnSc2d1	akademická
i	i	k8xC	i
školní	školní	k2eAgFnSc2d1	školní
verze	verze	k1gFnSc2	verze
PČP	PČP	kA	PČP
<g/>
)	)	kIx)	)
i	i	k9	i
variantu	varianta	k1gFnSc4	varianta
s	s	k7c7	s
agávem	agáv	k1gInSc7	agáv
<g/>
.	.	kIx.	.
</s>
<s>
Internetová	internetový	k2eAgFnSc1d1	internetová
Příručka	příručka	k1gFnSc1	příručka
ÚJČ	ÚJČ	kA	ÚJČ
uvádí	uvádět	k5eAaImIp3nS	uvádět
skloňované	skloňovaný	k2eAgInPc4d1	skloňovaný
tvary	tvar	k1gInPc4	tvar
i	i	k9	i
v	v	k7c6	v
plurálu	plurál	k1gInSc6	plurál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Agáve	agáve	k1gFnPc1	agáve
přirozeně	přirozeně	k6eAd1	přirozeně
rostou	růst	k5eAaImIp3nP	růst
především	především	k9	především
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
najdeme	najít	k5eAaPmIp1nP	najít
je	on	k3xPp3gNnSc4	on
i	i	k9	i
v	v	k7c6	v
jižních	jižní	k2eAgInPc6d1	jižní
a	a	k8xC	a
západních	západní	k2eAgInPc6d1	západní
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
a	a	k8xC	a
Střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Pěstované	pěstovaný	k2eAgInPc1d1	pěstovaný
a	a	k8xC	a
zdomácnělé	zdomácnělý	k2eAgInPc1d1	zdomácnělý
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
jinde	jinde	k6eAd1	jinde
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
s	s	k7c7	s
odpovídajícím	odpovídající	k2eAgNnSc7d1	odpovídající
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
mají	mít	k5eAaImIp3nP	mít
velkou	velký	k2eAgFnSc4d1	velká
růžici	růžice	k1gFnSc4	růžice
tlustých	tlustý	k2eAgInPc2d1	tlustý
listů	list	k1gInPc2	list
s	s	k7c7	s
ostnatým	ostnatý	k2eAgInSc7d1	ostnatý
okrajem	okraj	k1gInSc7	okraj
<g/>
,	,	kIx,	,
končících	končící	k2eAgFnPc6d1	končící
obvykle	obvykle	k6eAd1	obvykle
dalším	další	k2eAgInSc7d1	další
ostnem	osten	k1gInSc7	osten
<g/>
.	.	kIx.	.
</s>
<s>
Robustní	robustní	k2eAgInSc1d1	robustní
kmen	kmen	k1gInSc1	kmen
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
listy	list	k1gInPc1	list
vypadají	vypadat	k5eAaPmIp3nP	vypadat
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
vyrůstaly	vyrůstat	k5eAaImAgFnP	vyrůstat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
kořene	kořen	k1gInSc2	kořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
růžice	růžice	k1gFnSc1	růžice
roste	růst	k5eAaImIp3nS	růst
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
kvete	kvést	k5eAaImIp3nS	kvést
pouze	pouze	k6eAd1	pouze
jedenkrát	jedenkrát	k6eAd1	jedenkrát
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kvetení	kvetení	k1gNnSc2	kvetení
ze	z	k7c2	z
středu	střed	k1gInSc2	střed
rostliny	rostlina	k1gFnSc2	rostlina
vyraší	vyrašit	k5eAaPmIp3nS	vyrašit
vysoký	vysoký	k2eAgInSc1d1	vysoký
kmen	kmen	k1gInSc1	kmen
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
stožár	stožár	k1gInSc1	stožár
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
s	s	k7c7	s
květy	květ	k1gInPc7	květ
na	na	k7c6	na
konci	konec	k1gInSc6	konec
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
plody	plod	k1gInPc4	plod
se	s	k7c7	s
semeny	semeno	k1gNnPc7	semeno
<g/>
,	,	kIx,	,
rostlina	rostlina	k1gFnSc1	rostlina
odumře	odumřít	k5eAaPmIp3nS	odumřít
<g/>
,	,	kIx,	,
přežívají	přežívat	k5eAaImIp3nP	přežívat
ale	ale	k9	ale
přírůstky	přírůstek	k1gInPc1	přírůstek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
volně	volně	k6eAd1	volně
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
ze	z	k7c2	z
základny	základna	k1gFnSc2	základna
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Agáve	agáve	k1gFnPc1	agáve
jsou	být	k5eAaImIp3nP	být
blízce	blízce	k6eAd1	blízce
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
liliím	lilie	k1gFnPc3	lilie
a	a	k8xC	a
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c7	mezi
kaktusy	kaktus	k1gInPc7	kaktus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
agáví	agávět	k5eAaImIp3nS	agávět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Agáve	agáve	k1gFnSc1	agáve
sisalová	sisalový	k2eAgFnSc1d1	sisalová
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
agáve	agáve	k1gFnSc2	agáve
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
agáve	agáve	k1gFnSc2	agáve
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
agáve	agáve	k1gFnSc2	agáve
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Agave	Agaev	k1gFnSc2	Agaev
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Agave	Agav	k1gMnSc5	Agav
Online	Onlin	k1gMnSc5	Onlin
Herb	Herb	k1gInSc1	Herb
Guide	Guid	k1gMnSc5	Guid
</s>
</p>
