<s>
Hornopirén	Hornopirén	k1gInSc1
</s>
<s>
Hornopirén	Hornopirén	k1gInSc1
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
72	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
14	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
16	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Stát	stát	k1gInSc1
</s>
<s>
Chile	Chile	k1gNnSc1
Chile	Chile	k1gNnSc2
Region	region	k1gInSc4
</s>
<s>
Los	los	k1gInSc1
Lagos	Lagosa	k1gFnPc2
Provincie	provincie	k1gFnSc2
</s>
<s>
Palena	Palen	k2eAgFnSc1d1
obec	obec	k1gFnSc1
</s>
<s>
Hualaihué	Hualaihué	k6eAd1
</s>
<s>
Hornopirén	Hornopirén	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
3,6	3,6	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
3	#num#	k4
629	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
999,7	999,7	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hornopirén	Hornopirén	k1gInSc1
(	(	kIx(
<g/>
původně	původně	k6eAd1
Río	Río	k1gNnSc1
Negro	Negro	k1gNnSc1
–	–	k?
Hornopirén	Hornopirén	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
malé	malý	k2eAgNnSc1d1
město	město	k1gNnSc1
na	na	k7c6
jihu	jih	k1gInSc6
Chile	Chile	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
regionu	region	k1gInSc6
Los	los	k1gMnSc1
Lagos	Lagos	k1gMnSc1
<g/>
,	,	kIx,
109	#num#	k4
kilometrů	kilometr	k1gInPc2
jižně	jižně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
Puerto	Puerta	k1gFnSc5
Montt	Montt	k2eAgMnSc1d1
po	po	k7c6
silnici	silnice	k1gFnSc6
Carretera	Carreter	k1gMnSc2
Austral	Austral	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
obce	obec	k1gFnSc2
Hualaihué	Hualaihuá	k1gFnSc2
(	(	kIx(
<g/>
jde	jít	k5eAaImIp3nS
o	o	k7c4
jedno	jeden	k4xCgNnSc4
z	z	k7c2
mála	málo	k1gNnSc2
hlavních	hlavní	k2eAgNnPc2d1
měst	město	k1gNnPc2
chilských	chilský	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nemají	mít	k5eNaImIp3nP
stejný	stejný	k2eAgInSc4d1
název	název	k1gInSc4
jako	jako	k8xC,k8xS
obec	obec	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
od	od	k7c2
města	město	k1gNnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
stejnojmenný	stejnojmenný	k2eAgInSc1d1
vulkán	vulkán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
téhož	týž	k3xTgNnSc2
jména	jméno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Hornopirénu	Hornopirén	k1gInSc2
dál	daleko	k6eAd2
na	na	k7c4
jih	jih	k1gInSc4
jezdí	jezdit	k5eAaImIp3nP
trajekty	trajekt	k1gInPc1
do	do	k7c2
Calety	Caleta	k1gFnSc2
Gonzalo	Gonzalo	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pokračuje	pokračovat	k5eAaImIp3nS
Carretera	Carreter	k1gMnSc4
Austral	Austral	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
2017	#num#	k4
Chile	Chile	k1gNnSc2
census	census	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hornopirén	Hornopirén	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
