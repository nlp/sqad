<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
kotrč	kotrč	k1gInSc1	kotrč
kadeřavý	kadeřavý	k2eAgInSc1d1	kadeřavý
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gInSc1	Sparassis
crispa	crisp	k1gMnSc2	crisp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hojný	hojný	k2eAgInSc1d1	hojný
druh	druh	k1gInSc1	druh
parazitující	parazitující	k2eAgInSc1d1	parazitující
na	na	k7c6	na
borovicích	borovice	k1gFnPc6	borovice
<g/>
,	,	kIx,	,
kotrč	kotrč	k1gInSc1	kotrč
Němcův	Němcův	k2eAgInSc1d1	Němcův
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gInSc1	Sparassis
nemecii	nemecie	k1gFnSc4	nemecie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácný	vzácný	k2eAgInSc4d1	vzácný
druh	druh	k1gInSc4	druh
parazitující	parazitující	k2eAgInSc4d1	parazitující
na	na	k7c6	na
jedlích	jedle	k1gFnPc6	jedle
a	a	k8xC	a
kotrč	kotrč	k1gInSc4	kotrč
štěrbákový	štěrbákový	k2eAgInSc4d1	štěrbákový
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gFnSc1	Sparassis
laminosa	laminosa	k1gFnSc1	laminosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácný	vzácný	k2eAgInSc4d1	vzácný
druh	druh	k1gInSc4	druh
parazitující	parazitující	k2eAgInSc4d1	parazitující
na	na	k7c6	na
dubech	dub	k1gInPc6	dub
<g/>
.	.	kIx.	.
</s>
