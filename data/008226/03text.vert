<p>
<s>
Kotrč	kotrč	k1gFnSc1	kotrč
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gInSc1	Sparassis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
chorošovitých	chorošovitý	k2eAgFnPc2d1	chorošovitá
hub	houba	k1gFnPc2	houba
keříčkovitého	keříčkovitý	k2eAgInSc2d1	keříčkovitý
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
žijí	žít	k5eAaImIp3nP	žít
jako	jako	k9	jako
saproparazité	saproparazita	k1gMnPc1	saproparazita
na	na	k7c6	na
kořenech	kořen	k1gInPc6	kořen
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
<g/>
:	:	kIx,	:
kotrč	kotrč	k1gInSc1	kotrč
kadeřavý	kadeřavý	k2eAgInSc1d1	kadeřavý
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gInSc1	Sparassis
crispa	crisp	k1gMnSc2	crisp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hojný	hojný	k2eAgInSc1d1	hojný
druh	druh	k1gInSc1	druh
parazitující	parazitující	k2eAgInSc1d1	parazitující
na	na	k7c6	na
borovicích	borovice	k1gFnPc6	borovice
<g/>
,	,	kIx,	,
kotrč	kotrč	k1gInSc1	kotrč
Němcův	Němcův	k2eAgInSc1d1	Němcův
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gInSc1	Sparassis
nemecii	nemecie	k1gFnSc4	nemecie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácný	vzácný	k2eAgInSc4d1	vzácný
druh	druh	k1gInSc4	druh
parazitující	parazitující	k2eAgInSc4d1	parazitující
na	na	k7c6	na
jedlích	jedle	k1gFnPc6	jedle
a	a	k8xC	a
kotrč	kotrč	k1gInSc4	kotrč
štěrbákový	štěrbákový	k2eAgInSc4d1	štěrbákový
(	(	kIx(	(
<g/>
Sparassis	Sparassis	k1gFnSc1	Sparassis
laminosa	laminosa	k1gFnSc1	laminosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzácný	vzácný	k2eAgInSc4d1	vzácný
druh	druh	k1gInSc4	druh
parazitující	parazitující	k2eAgInSc4d1	parazitující
na	na	k7c6	na
dubech	dub	k1gInPc6	dub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc4	druh
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gFnSc1	Sparassis
crispa	crispa	k1gFnSc1	crispa
(	(	kIx(	(
<g/>
Wulfen	Wulfen	k1gInSc1	Wulfen
<g/>
)	)	kIx)	)
Fr.	Fr.	k1gFnSc1	Fr.
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gInSc1	Sparassis
brevipes	brevipes	k1gMnSc1	brevipes
Krombh	Krombh	k1gMnSc1	Krombh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gFnSc1	Sparassis
cystidiosa	cystidiosa	k1gFnSc1	cystidiosa
Desjardin	Desjardina	k1gFnPc2	Desjardina
et	et	k?	et
Zheng	Zheng	k1gMnSc1	Zheng
Wang	Wang	k1gMnSc1	Wang
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gInSc1	Sparassis
latifolia	latifolium	k1gNnSc2	latifolium
Y.C.	Y.C.	k1gFnSc2	Y.C.
Dai	Dai	k1gMnSc1	Dai
et	et	k?	et
Zheng	Zheng	k1gMnSc1	Zheng
Wang	Wang	k1gMnSc1	Wang
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gFnSc1	Sparassis
miniensis	miniensis	k1gFnSc2	miniensis
Blanco-Dios	Blanco-Dios	k1gInSc1	Blanco-Dios
et	et	k?	et
Z.	Z.	kA	Z.
Wang	Wang	k1gInSc1	Wang
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gFnSc1	Sparassis
radicata	radicat	k2eAgFnSc1d1	radicat
Weir	Weir	k1gInSc4	Weir
</s>
</p>
<p>
<s>
Sparassis	Sparassis	k1gFnSc1	Sparassis
spathulata	spathule	k1gNnPc1	spathule
(	(	kIx(	(
<g/>
Schwein	Schwein	k1gInSc1	Schwein
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Fr.	Fr.	k1gFnSc1	Fr.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kotrč	kotrč	k1gInSc1	kotrč
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
