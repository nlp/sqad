<p>
<s>
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
poblíž	poblíž	k6eAd1	poblíž
Bermud	Bermudy	k1gFnPc2	Bermudy
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
oceánskými	oceánský	k2eAgInPc7d1	oceánský
proudy	proud	k1gInPc7	proud
<g/>
:	:	kIx,	:
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Severoatlantický	severoatlantický	k2eAgInSc4d1	severoatlantický
proud	proud	k1gInSc4	proud
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Severním	severní	k2eAgInSc7d1	severní
rovníkovým	rovníkový	k2eAgInSc7d1	rovníkový
proudem	proud	k1gInSc7	proud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
slané	slaný	k2eAgNnSc1d1	slané
a	a	k8xC	a
často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
popisováno	popisován	k2eAgNnSc1d1	popisováno
jako	jako	k8xC	jako
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
drtivou	drtivý	k2eAgFnSc4d1	drtivá
většinu	většina	k1gFnSc4	většina
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
biomasy	biomasa	k1gFnSc2	biomasa
zde	zde	k6eAd1	zde
představují	představovat	k5eAaImIp3nP	představovat
řasy	řasa	k1gFnPc1	řasa
rodu	rod	k1gInSc2	rod
Sargassum	Sargassum	k1gInSc1	Sargassum
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
daly	dát	k5eAaPmAgFnP	dát
moři	moře	k1gNnSc6	moře
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
se	se	k3xPyFc4	se
také	také	k9	také
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
nedostatkem	nedostatek	k1gInSc7	nedostatek
prvku	prvek	k1gInSc2	prvek
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
úhořů	úhoř	k1gMnPc2	úhoř
===	===	k?	===
</s>
</p>
<p>
<s>
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
pro	pro	k7c4	pro
životní	životní	k2eAgInSc4d1	životní
cyklus	cyklus	k1gInSc4	cyklus
úhořů	úhoř	k1gMnPc2	úhoř
říčních	říční	k2eAgInPc2d1	říční
a	a	k8xC	a
úhořů	úhoř	k1gMnPc2	úhoř
amerických	americký	k2eAgMnPc2d1	americký
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
putují	putovat	k5eAaImIp3nP	putovat
vytřít	vytřít	k5eAaPmF	vytřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografická	geografický	k2eAgNnPc1d1	geografické
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
asi	asi	k9	asi
4	[number]	k4	4
000	[number]	k4	000
000	[number]	k4	000
km2	km2	k4	km2
</s>
</p>
<p>
<s>
Hloubka	hloubka	k1gFnSc1	hloubka
<g/>
:	:	kIx,	:
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
5000	[number]	k4	5000
m	m	kA	m
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
7	[number]	k4	7
198	[number]	k4	198
m	m	kA	m
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
:	:	kIx,	:
převážně	převážně	k6eAd1	převážně
okolo	okolo	k7c2	okolo
18	[number]	k4	18
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
Salinita	salinita	k1gFnSc1	salinita
<g/>
:	:	kIx,	:
36,5	[number]	k4	36,5
<g/>
–	–	k?	–
<g/>
37	[number]	k4	37
promile	promile	k1gNnPc2	promile
</s>
</p>
<p>
<s>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
:	:	kIx,	:
subtropické	subtropický	k2eAgNnSc1d1	subtropické
<g/>
,	,	kIx,	,
ovlivněné	ovlivněný	k2eAgNnSc1d1	ovlivněné
teplým	teplý	k2eAgInSc7d1	teplý
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
</s>
</p>
<p>
<s>
Mořské	mořský	k2eAgInPc1d1	mořský
proudy	proud	k1gInPc1	proud
<g/>
:	:	kIx,	:
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
Severoatlantický	severoatlantický	k2eAgInSc1d1	severoatlantický
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
Severní	severní	k2eAgInSc1d1	severní
rovníkový	rovníkový	k2eAgInSc1d1	rovníkový
proud	proud	k1gInSc1	proud
<g/>
,	,	kIx,	,
Kanárský	kanárský	k2eAgInSc1d1	kanárský
proud	proud	k1gInSc1	proud
</s>
</p>
<p>
<s>
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
<g/>
:	:	kIx,	:
oblast	oblast	k1gFnSc1	oblast
tření	tření	k1gNnSc2	tření
úhořů	úhoř	k1gMnPc2	úhoř
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
bermudského	bermudský	k2eAgInSc2d1	bermudský
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
</s>
</p>
<p>
<s>
Významné	významný	k2eAgInPc1d1	významný
přístavy	přístav	k1gInPc1	přístav
<g/>
:	:	kIx,	:
Hamilton	Hamilton	k1gInSc1	Hamilton
</s>
</p>
<p>
<s>
Významné	významný	k2eAgInPc1d1	významný
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
:	:	kIx,	:
Bermudy	Bermudy	k1gFnPc1	Bermudy
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Sargasové	sargasový	k2eAgNnSc1d1	Sargasové
moře	moře	k1gNnSc1	moře
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sargasové	sargasový	k2eAgFnSc2d1	sargasový
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
