<s>
Centrální	centrální	k2eAgFnSc1d1	centrální
procesorová	procesorový	k2eAgFnSc1d1	procesorová
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
CPU	CPU	kA	CPU
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
central	centrat	k5eAaPmAgInS	centrat
processing	processing	k1gInSc1	processing
unit	unit	k1gInSc1	unit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
základní	základní	k2eAgFnSc2d1	základní
elektronické	elektronický	k2eAgFnSc2d1	elektronická
součásti	součást	k1gFnSc2	součást
v	v	k7c6	v
počítači	počítač	k1gInSc6	počítač
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umí	umět	k5eAaImIp3nS	umět
vykonávat	vykonávat	k5eAaImF	vykonávat
strojové	strojový	k2eAgFnPc4d1	strojová
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
je	být	k5eAaImIp3nS	být
tvořen	tvořen	k2eAgInSc1d1	tvořen
počítačový	počítačový	k2eAgInSc1d1	počítačový
program	program	k1gInSc1	program
a	a	k8xC	a
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
jeho	jeho	k3xOp3gInPc4	jeho
vstupy	vstup	k1gInPc4	vstup
a	a	k8xC	a
výstupy	výstup	k1gInPc4	výstup
<g/>
.	.	kIx.	.
</s>
