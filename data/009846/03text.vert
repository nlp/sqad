<p>
<s>
SUV	SUV	kA	SUV
je	být	k5eAaImIp3nS	být
zkratka	zkratka	k1gFnSc1	zkratka
pro	pro	k7c4	pro
Sport	sport	k1gInSc4	sport
utility	utilita	k1gFnPc1	utilita
vehicle	vehicle	k1gFnSc2	vehicle
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
sportovní	sportovní	k2eAgNnSc4d1	sportovní
užitkové	užitkový	k2eAgNnSc4d1	užitkové
vozidlo	vozidlo	k1gNnSc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
SUV	SUV	kA	SUV
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
výhody	výhoda	k1gFnPc1	výhoda
terénních	terénní	k2eAgNnPc2d1	terénní
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
schopnost	schopnost	k1gFnSc1	schopnost
pohybu	pohyb	k1gInSc2	pohyb
i	i	k9	i
mimo	mimo	k7c4	mimo
silnice	silnice	k1gFnPc4	silnice
<g/>
,	,	kIx,	,
robustnost	robustnost	k1gFnSc4	robustnost
či	či	k8xC	či
větší	veliký	k2eAgInSc4d2	veliký
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
a	a	k8xC	a
zavazadlový	zavazadlový	k2eAgInSc4d1	zavazadlový
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
pozitivními	pozitivní	k2eAgFnPc7d1	pozitivní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
silničních	silniční	k2eAgNnPc2d1	silniční
osobních	osobní	k2eAgNnPc2d1	osobní
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
komfort	komfort	k1gInSc1	komfort
<g/>
,	,	kIx,	,
ovladatelnost	ovladatelnost	k1gFnSc1	ovladatelnost
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
běžným	běžný	k2eAgInPc3d1	běžný
automobilům	automobil	k1gInPc3	automobil
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
bývají	bývat	k5eAaImIp3nP	bývat
vozidla	vozidlo	k1gNnPc1	vozidlo
kategorie	kategorie	k1gFnSc2	kategorie
SUV	SUV	kA	SUV
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
terénními	terénní	k2eAgMnPc7d1	terénní
často	často	k6eAd1	často
mylně	mylně	k6eAd1	mylně
zaměňována	zaměňován	k2eAgFnSc1d1	zaměňována
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
SUV	SUV	kA	SUV
zcela	zcela	k6eAd1	zcela
nepřevládá	převládat	k5eNaImIp3nS	převládat
ani	ani	k8xC	ani
ryze	ryze	k6eAd1	ryze
terénní	terénní	k2eAgFnPc1d1	terénní
ani	ani	k8xC	ani
ryze	ryze	k6eAd1	ryze
sportovní	sportovní	k2eAgFnSc1d1	sportovní
či	či	k8xC	či
užitná	užitný	k2eAgFnSc1d1	užitná
složka	složka	k1gFnSc1	složka
jejich	jejich	k3xOp3gNnSc2	jejich
použití	použití	k1gNnSc2	použití
–	–	k?	–
jsou	být	k5eAaImIp3nP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
pohodlně	pohodlně	k6eAd1	pohodlně
pohybovat	pohybovat	k5eAaImF	pohybovat
jak	jak	k8xS	jak
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
prostředí	prostředí	k1gNnSc6	prostředí
tak	tak	k9	tak
mimo	mimo	k7c4	mimo
pozemní	pozemní	k2eAgFnPc4d1	pozemní
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předchůdcem	předchůdce	k1gMnSc7	předchůdce
dnešních	dnešní	k2eAgInPc2d1	dnešní
vozů	vůz	k1gInPc2	vůz
SUV	SUV	kA	SUV
byl	být	k5eAaImAgInS	být
Land	Land	k1gInSc1	Land
Rover	rover	k1gMnSc1	rover
Series	Series	k1gMnSc1	Series
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
SUV	SUV	kA	SUV
na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Chevrolet	chevrolet	k1gInSc1	chevrolet
Suburban	Suburbana	k1gFnPc2	Suburbana
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Výhody	výhoda	k1gFnPc1	výhoda
==	==	k?	==
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgInSc4d2	veliký
prostor	prostor	k1gInSc4	prostor
(	(	kIx(	(
<g/>
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
běžnými	běžný	k2eAgInPc7d1	běžný
osobními	osobní	k2eAgInPc7d1	osobní
automobily	automobil	k1gInPc7	automobil
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
větší	veliký	k2eAgInSc4d2	veliký
komfort	komfort	k1gInSc4	komfort
a	a	k8xC	a
větší	veliký	k2eAgInSc4d2	veliký
zavazadlový	zavazadlový	k2eAgInSc4d1	zavazadlový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
SUV	SUV	kA	SUV
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
řady	řada	k1gFnPc4	řada
sedadel	sedadlo	k1gNnPc2	sedadlo
<g/>
,	,	kIx,	,
pojme	pojmout	k5eAaPmIp3nS	pojmout
tak	tak	k6eAd1	tak
šest	šest	k4xCc1	šest
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
)	)	kIx)	)
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SUV	SUV	kA	SUV
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
jako	jako	k8xS	jako
tahač	tahač	k1gInSc4	tahač
přívěsů	přívěs	k1gInPc2	přívěs
a	a	k8xC	a
karavanů	karavan	k1gInPc2	karavan
<g/>
,	,	kIx,	,
některé	některý	k3yIgMnPc4	některý
SUV	SUV	kA	SUV
mohou	moct	k5eAaImIp3nP	moct
utáhnout	utáhnout	k5eAaPmF	utáhnout
3-4	[number]	k4	3-4
tuny	tuna	k1gFnPc4	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
SUV	SUV	kA	SUV
má	mít	k5eAaImIp3nS	mít
pohon	pohon	k1gInSc4	pohon
všech	všecek	k3xTgNnPc2	všecek
kol	kolo	k1gNnPc2	kolo
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
4WD	[number]	k4	4WD
nebo	nebo	k8xC	nebo
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
těšit	těšit	k5eAaImF	těšit
z	z	k7c2	z
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
toto	tento	k3xDgNnSc4	tento
technické	technický	k2eAgNnSc4d1	technické
řešení	řešení	k1gNnSc4	řešení
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
větší	veliký	k2eAgNnPc1d2	veliký
a	a	k8xC	a
robustnější	robustní	k2eAgNnPc1d2	robustnější
kola	kolo	k1gNnPc1	kolo
znamenají	znamenat	k5eAaImIp3nP	znamenat
zvýšení	zvýšení	k1gNnSc3	zvýšení
výšky	výška	k1gFnSc2	výška
celého	celý	k2eAgInSc2d1	celý
vozu	vůz	k1gInSc2	vůz
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
řidič	řidič	k1gInSc1	řidič
shlíží	shlížet	k5eAaImIp3nS	shlížet
na	na	k7c4	na
vozovku	vozovka	k1gFnSc4	vozovka
–	–	k?	–
obecně	obecně	k6eAd1	obecně
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
větší	veliký	k2eAgInSc4d2	veliký
přehled	přehled	k1gInSc4	přehled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Návrh	návrh	k1gInSc1	návrh
SUV	SUV	kA	SUV
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
robustnější	robustní	k2eAgFnSc1d2	robustnější
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
většinou	většinou	k6eAd1	většinou
znamená	znamenat	k5eAaImIp3nS	znamenat
větší	veliký	k2eAgNnSc4d2	veliký
bezpečí	bezpečí	k1gNnSc4	bezpečí
pro	pro	k7c4	pro
posádku	posádka	k1gFnSc4	posádka
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nevýhody	nevýhoda	k1gFnSc2	nevýhoda
==	==	k?	==
</s>
</p>
<p>
<s>
SUV	SUV	kA	SUV
mají	mít	k5eAaImIp3nP	mít
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
výjimky	výjimka	k1gFnSc2	výjimka
(	(	kIx(	(
<g/>
neplatí	platit	k5eNaImIp3nS	platit
u	u	k7c2	u
hybridních	hybridní	k2eAgFnPc2d1	hybridní
a	a	k8xC	a
ekologických	ekologický	k2eAgFnPc2d1	ekologická
verzí	verze	k1gFnPc2	verze
SUV	SUV	kA	SUV
<g/>
)	)	kIx)	)
větší	veliký	k2eAgFnSc4d2	veliký
spotřebu	spotřeba	k1gFnSc4	spotřeba
než	než	k8xS	než
konvenční	konvenční	k2eAgInPc4d1	konvenční
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
SUV	SUV	kA	SUV
posledních	poslední	k2eAgNnPc2d1	poslední
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
nesnižující	snižující	k2eNgMnSc1d1	snižující
se	se	k3xPyFc4	se
import	import	k1gInSc1	import
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
nevýhodami	nevýhoda	k1gFnPc7	nevýhoda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
to	ten	k3xDgNnSc1	ten
obnáší	obnášet	k5eAaImIp3nS	obnášet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SUV	SUV	kA	SUV
mají	mít	k5eAaImIp3nP	mít
obecně	obecně	k6eAd1	obecně
(	(	kIx(	(
<g/>
neplatí	platit	k5eNaImIp3nS	platit
u	u	k7c2	u
hybridních	hybridní	k2eAgFnPc2d1	hybridní
a	a	k8xC	a
ekologických	ekologický	k2eAgFnPc2d1	ekologická
verzí	verze	k1gFnPc2	verze
SUV	SUV	kA	SUV
<g/>
)	)	kIx)	)
vyšší	vysoký	k2eAgFnSc1d2	vyšší
emise	emise	k1gFnSc1	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SUV	SUV	kA	SUV
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
těžší	těžký	k2eAgMnSc1d2	těžší
než	než	k8xS	než
běžné	běžný	k2eAgInPc1d1	běžný
osobní	osobní	k2eAgInPc1d1	osobní
automobily	automobil	k1gInPc1	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stejné	stejný	k2eAgFnSc6d1	stejná
rychlosti	rychlost	k1gFnSc6	rychlost
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgFnSc4d2	veliký
hybnost	hybnost	k1gFnSc4	hybnost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
o	o	k7c4	o
trochu	trocha	k1gFnSc4	trocha
méně	málo	k6eAd2	málo
manévrovatelné	manévrovatelný	k2eAgNnSc1d1	manévrovatelné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
větším	veliký	k2eAgNnPc3d2	veliký
kolům	kolo	k1gNnPc3	kolo
je	být	k5eAaImIp3nS	být
těžiště	těžiště	k1gNnSc1	těžiště
posazeno	posadit	k5eAaPmNgNnS	posadit
výše	vysoce	k6eAd2	vysoce
a	a	k8xC	a
když	když	k8xS	když
už	už	k6eAd1	už
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
SUV	SUV	kA	SUV
větší	veliký	k2eAgFnSc4d2	veliký
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
překlopit	překlopit	k5eAaPmF	překlopit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
nebo	nebo	k8xC	nebo
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SUV	SUV	kA	SUV
představují	představovat	k5eAaImIp3nP	představovat
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
běžnými	běžný	k2eAgInPc7d1	běžný
osobními	osobní	k2eAgInPc7d1	osobní
automobily	automobil	k1gInPc7	automobil
větší	veliký	k2eAgFnSc2d2	veliký
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
účastníky	účastník	k1gMnPc4	účastník
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
značně	značně	k6eAd1	značně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
při	při	k7c6	při
sražení	sražení	k1gNnSc6	sražení
chodce	chodec	k1gMnSc4	chodec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SUV	SUV	kA	SUV
přispívají	přispívat	k5eAaImIp3nP	přispívat
větší	veliký	k2eAgFnSc7d2	veliký
měrou	míra	k1gFnSc7wR	míra
k	k	k7c3	k
opotřebování	opotřebování	k1gNnSc3	opotřebování
komunikací	komunikace	k1gFnPc2	komunikace
než	než	k8xS	než
běžné	běžný	k2eAgInPc1d1	běžný
osobní	osobní	k2eAgInPc1d1	osobní
automobily	automobil	k1gInPc1	automobil
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
vyšší	vysoký	k2eAgFnSc3d2	vyšší
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sport	sport	k1gInSc1	sport
Utility	utilita	k1gFnSc2	utilita
Vehicle	Vehicle	k1gFnSc2	Vehicle
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
