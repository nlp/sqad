<s>
Rutger	Rutger	k1gMnSc1	Rutger
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Groningen	Groningen	k1gInSc1	Groningen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
vrh	vrh	k1gInSc4	vrh
koulí	koule	k1gFnPc2	koule
a	a	k8xC	a
hod	hod	k1gInSc1	hod
diskem	disk	k1gInSc7	disk
<g/>
.	.	kIx.	.
hod	hod	k1gInSc1	hod
diskem	disk	k1gInSc7	disk
–	–	k?	–
67,63	[number]	k4	67,63
m	m	kA	m
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
Helsingborg	Helsingborg	k1gInSc1	Helsingborg
vrh	vrh	k1gInSc1	vrh
koulí	koulet	k5eAaImIp3nS	koulet
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
21,62	[number]	k4	21,62
m	m	kA	m
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Leiden	Leidna	k1gFnPc2	Leidna
vrh	vrh	k1gInSc4	vrh
koulí	koulet	k5eAaImIp3nS	koulet
(	(	kIx(	(
<g/>
hala	hala	k1gFnSc1	hala
<g/>
)	)	kIx)	)
–	–	k?	–
20,89	[number]	k4	20,89
m	m	kA	m
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
únor	únor	k1gInSc1	únor
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Gent	Gent	k2eAgInSc4d1	Gent
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rutger	Rutgra	k1gFnPc2	Rutgra
Smith	Smitha	k1gFnPc2	Smitha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Rutger	Rutger	k1gMnSc1	Rutger
Smith	Smith	k1gMnSc1	Smith
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
(	(	kIx(	(
<g/>
nizozemsky	nizozemsky	k6eAd1	nizozemsky
<g/>
)	)	kIx)	)
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
