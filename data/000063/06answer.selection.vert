<s>
Jablko	jablko	k1gNnSc1	jablko
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
jabloně	jabloň	k1gFnSc2	jabloň
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
druhy	druh	k1gInPc4	druh
ovoce	ovoce	k1gNnSc2	ovoce
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
