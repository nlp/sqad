<p>
<s>
Jablko	jablko	k1gNnSc1	jablko
je	být	k5eAaImIp3nS	být
plod	plod	k1gInSc4	plod
jabloně	jabloň	k1gFnSc2	jabloň
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgInPc4d3	nejběžnější
druhy	druh	k1gInPc4	druh
ovoce	ovoce	k1gNnSc2	ovoce
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
malvici	malvice	k1gFnSc6	malvice
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
plodu	plod	k1gInSc2	plod
hrušně	hrušeň	k1gFnSc2	hrušeň
hrušky	hruška	k1gFnSc2	hruška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
Sieversova	Sieversův	k2eAgFnSc1d1	Sieversův
je	být	k5eAaImIp3nS	být
původním	původní	k2eAgInSc7d1	původní
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
domestikován	domestikovat	k5eAaBmNgInS	domestikovat
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
.	.	kIx.	.
</s>
<s>
Jablko	jablko	k1gNnSc1	jablko
se	se	k3xPyFc4	se
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
člověka	člověk	k1gMnSc2	člověk
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
již	již	k9	již
tisíce	tisíc	k4xCgInPc1	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
prvotní	prvotní	k2eAgInSc4d1	prvotní
hřích	hřích	k1gInSc4	hřích
lidského	lidský	k2eAgNnSc2d1	lidské
pokolení	pokolení	k1gNnSc2	pokolení
<g/>
,	,	kIx,	,
utržení	utržení	k1gNnSc4	utržení
zakázaného	zakázaný	k2eAgInSc2d1	zakázaný
plodu	plod	k1gInSc2	plod
ze	z	k7c2	z
stromu	strom	k1gInSc2	strom
poznání	poznání	k1gNnSc2	poznání
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
tradičně	tradičně	k6eAd1	tradičně
spojován	spojovat	k5eAaImNgInS	spojovat
s	s	k7c7	s
jablkem	jablko	k1gNnSc7	jablko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bibli	bible	k1gFnSc6	bible
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
o	o	k7c6	o
onom	onen	k3xDgInSc6	onen
stromu	strom	k1gInSc6	strom
a	a	k8xC	a
plodu	plod	k1gInSc6	plod
utrženém	utržený	k2eAgInSc6d1	utržený
Evou	Eva	k1gFnSc7	Eva
nepíše	psát	k5eNaImIp3nS	psát
nic	nic	k3yNnSc1	nic
bližšího	blízký	k2eAgMnSc4d2	bližší
<g/>
.	.	kIx.	.
</s>
<s>
Královské	královský	k2eAgNnSc1d1	královské
jablko	jablko	k1gNnSc1	jablko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
symbolů	symbol	k1gInPc2	symbol
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
jablko	jablko	k1gNnSc1	jablko
ze	z	k7c2	z
zahrady	zahrada	k1gFnSc2	zahrada
Hesperidek	Hesperidka	k1gFnPc2	Hesperidka
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Té	ten	k3xDgFnSc2	ten
nejkrásnější	krásný	k2eAgFnSc2d3	nejkrásnější
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jablko	jablko	k1gNnSc1	jablko
sváru	svár	k1gInSc2	svár
mezi	mezi	k7c7	mezi
bohyněmi	bohyně	k1gFnPc7	bohyně
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
rozsoudit	rozsoudit	k5eAaPmF	rozsoudit
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jablko	jablko	k1gNnSc1	jablko
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
Trojské	trojský	k2eAgFnSc3d1	Trojská
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
pozdějšímu	pozdní	k2eAgInSc3d2	pozdější
pádu	pád	k1gInSc3	pád
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Pěstování	pěstování	k1gNnSc1	pěstování
<g/>
,	,	kIx,	,
konzumace	konzumace	k1gFnSc1	konzumace
a	a	k8xC	a
obchod	obchod	k1gInSc1	obchod
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Jablka	jablko	k1gNnPc1	jablko
jsou	být	k5eAaImIp3nP	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
našeho	náš	k3xOp1gInSc2	náš
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mošty	mošt	k1gInPc1	mošt
a	a	k8xC	a
džusy	džus	k1gInPc1	džus
<g/>
,	,	kIx,	,
džemy	džem	k1gInPc1	džem
<g/>
,	,	kIx,	,
kompoty	kompot	k1gInPc1	kompot
<g/>
,	,	kIx,	,
čaje	čaj	k1gInPc1	čaj
<g/>
,	,	kIx,	,
aroma	aroma	k1gNnPc1	aroma
do	do	k7c2	do
jiných	jiný	k2eAgNnPc2d1	jiné
jídel	jídlo	k1gNnPc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
nezastupitelné	zastupitelný	k2eNgNnSc4d1	nezastupitelné
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
-	-	kIx~	-
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
látek	látka	k1gFnPc2	látka
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
organismu	organismus	k1gInSc6	organismus
<g/>
,	,	kIx,	,
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
imunitu	imunita	k1gFnSc4	imunita
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
stresu	stres	k1gInSc3	stres
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
tyto	tento	k3xDgInPc4	tento
účinky	účinek	k1gInPc4	účinek
se	se	k3xPyFc4	se
v	v	k7c6	v
populární	populární	k2eAgFnSc6d1	populární
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
časopisech	časopis	k1gInPc6	časopis
často	často	k6eAd1	často
opakuje	opakovat	k5eAaImIp3nS	opakovat
doporučení	doporučení	k1gNnSc4	doporučení
konzumovat	konzumovat	k5eAaBmF	konzumovat
nejméně	málo	k6eAd3	málo
jedno	jeden	k4xCgNnSc4	jeden
jablko	jablko	k1gNnSc4	jablko
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
aromatické	aromatický	k2eAgFnPc1d1	aromatická
látky	látka	k1gFnPc1	látka
z	z	k7c2	z
jablek	jablko	k1gNnPc2	jablko
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
nezničíme	zničit	k5eNaPmIp1nP	zničit
jinými	jiný	k2eAgFnPc7d1	jiná
látkami	látka	k1gFnPc7	látka
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
tělem	tělo	k1gNnSc7	tělo
a	a	k8xC	a
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
skrze	skrze	k?	skrze
vlasy	vlas	k1gInPc1	vlas
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
pravidelní	pravidelní	k2eAgMnPc1d1	pravidelní
konzumenti	konzument	k1gMnPc1	konzument
jablek	jablko	k1gNnPc2	jablko
tak	tak	k8xS	tak
doslova	doslova	k6eAd1	doslova
voní	vonět	k5eAaImIp3nP	vonět
po	po	k7c6	po
jablíčkách	jablíčko	k1gNnPc6	jablíčko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Všudypřítomnost	všudypřítomnost	k1gFnSc4	všudypřítomnost
jabloní	jabloň	k1gFnPc2	jabloň
a	a	k8xC	a
jablek	jablko	k1gNnPc2	jablko
vedla	vést	k5eAaImAgFnS	vést
po	po	k7c6	po
r.	r.	kA	r.
1989	[number]	k4	1989
ke	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
devalvaci	devalvace	k1gFnSc3	devalvace
jejich	jejich	k3xOp3gFnSc2	jejich
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tisíce	tisíc	k4xCgInPc1	tisíc
jabloní	jabloň	k1gFnPc2	jabloň
v	v	k7c6	v
sadech	sad	k1gInPc6	sad
i	i	k8xC	i
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
Česka	Česko	k1gNnSc2	Česko
bylo	být	k5eAaImAgNnS	být
vykáceno	vykácet	k5eAaPmNgNnS	vykácet
anebo	anebo	k8xC	anebo
ošetřovatelsky	ošetřovatelsky	k6eAd1	ošetřovatelsky
zanedbáno	zanedbat	k5eAaPmNgNnS	zanedbat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
úrodnost	úrodnost	k1gFnSc1	úrodnost
klesla	klesnout	k5eAaPmAgFnS	klesnout
na	na	k7c4	na
zlomek	zlomek	k1gInSc4	zlomek
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
likvidací	likvidace	k1gFnSc7	likvidace
zařízení	zařízení	k1gNnSc2	zařízení
na	na	k7c4	na
zpracování	zpracování	k1gNnSc4	zpracování
jablek	jablko	k1gNnPc2	jablko
to	ten	k3xDgNnSc4	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
cena	cena	k1gFnSc1	cena
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
minimální	minimální	k2eAgInSc4d1	minimální
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
v	v	k7c6	v
ČR	ČR	kA	ČR
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
cena	cena	k1gFnSc1	cena
banánů	banán	k1gInPc2	banán
nebo	nebo	k8xC	nebo
pomerančů	pomeranč	k1gInPc2	pomeranč
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
řada	řada	k1gFnSc1	řada
produktů	produkt	k1gInPc2	produkt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
tradičně	tradičně	k6eAd1	tradičně
z	z	k7c2	z
padaných	padaný	k2eAgNnPc2d1	padané
jablek	jablko	k1gNnPc2	jablko
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
trhu	trh	k1gInSc2	trh
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
míry	míra	k1gFnSc2	míra
vymizely	vymizet	k5eAaPmAgFnP	vymizet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
dozrávají	dozrávat	k5eAaImIp3nP	dozrávat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
nebo	nebo	k8xC	nebo
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
tvar	tvar	k1gInSc4	tvar
(	(	kIx(	(
<g/>
některé	některý	k3yIgFnPc1	některý
odrůdy	odrůda	k1gFnPc1	odrůda
jsou	být	k5eAaImIp3nP	být
zploštělé	zploštělý	k2eAgFnPc1d1	zploštělá
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
vejčité	vejčitý	k2eAgNnSc1d1	vejčité
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
5	[number]	k4	5
až	až	k9	až
9	[number]	k4	9
cm	cm	kA	cm
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnSc2	některý
odrůdy	odrůda	k1gFnSc2	odrůda
až	až	k9	až
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Slupka	slupka	k1gFnSc1	slupka
zralého	zralý	k2eAgInSc2d1	zralý
plodu	plod	k1gInSc2	plod
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
odrůdy	odrůda	k1gFnSc2	odrůda
<g/>
,	,	kIx,	,
od	od	k7c2	od
zelené	zelená	k1gFnSc2	zelená
(	(	kIx(	(
<g/>
např.	např.	kA	např.
odrůda	odrůda	k1gFnSc1	odrůda
Granny	Granna	k1gFnSc2	Granna
Smith	Smitha	k1gFnPc2	Smitha
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
žlutou	žlutý	k2eAgFnSc4d1	žlutá
(	(	kIx(	(
<g/>
např.	např.	kA	např.
odrůdy	odrůda	k1gFnSc2	odrůda
Golden	Goldna	k1gFnPc2	Goldna
Delicious	Delicious	k1gInSc1	Delicious
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
reneta	reneta	k1gFnSc1	reneta
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
sytě	sytě	k6eAd1	sytě
červenou	červený	k2eAgFnSc4d1	červená
(	(	kIx(	(
<g/>
např.	např.	kA	např.
odrůdy	odrůda	k1gFnPc1	odrůda
Idared	Idared	k1gMnSc1	Idared
<g/>
,	,	kIx,	,
Spartan	Spartan	k?	Spartan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
častá	častý	k2eAgNnPc1d1	časté
jsou	být	k5eAaImIp3nP	být
červená	červený	k2eAgNnPc1d1	červené
"	"	kIx"	"
<g/>
líčka	líčko	k1gNnPc1	líčko
<g/>
"	"	kIx"	"
na	na	k7c6	na
žlutém	žlutý	k2eAgInSc6d1	žlutý
či	či	k8xC	či
zeleném	zelený	k2eAgInSc6d1	zelený
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Pozdní	pozdní	k2eAgFnPc1d1	pozdní
odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
dobře	dobře	k6eAd1	dobře
skladovat	skladovat	k5eAaImF	skladovat
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vhodných	vhodný	k2eAgFnPc6d1	vhodná
podmínkách	podmínka	k1gFnPc6	podmínka
vydrží	vydržet	k5eAaPmIp3nS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
jablek	jablko	k1gNnPc2	jablko
stabilně	stabilně	k6eAd1	stabilně
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
bylo	být	k5eAaImAgNnS	být
sklizeno	sklidit	k5eAaPmNgNnS	sklidit
34	[number]	k4	34
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
již	již	k6eAd1	již
přes	přes	k7c4	přes
64	[number]	k4	64
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
zdaleka	zdaleka	k6eAd1	zdaleka
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
27,5	[number]	k4	27,5
miliony	milion	k4xCgInPc4	milion
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
42	[number]	k4	42
<g/>
,	,	kIx,	,
<g/>
8	[number]	k4	8
<g/>
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
následují	následovat	k5eAaImIp3nP	následovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
<g/>
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
<g/>
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
8	[number]	k4	8
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
produkce	produkce	k1gFnSc2	produkce
se	se	k3xPyFc4	se
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
předmětem	předmět	k1gInSc7	předmět
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
obchodu	obchod	k1gInSc2	obchod
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006 7	[number]	k4	2006 7
milionu	milion	k4xCgInSc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
vývozce	vývozce	k1gMnPc4	vývozce
patřila	patřit	k5eAaImAgFnS	patřit
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
<g/>
2	[number]	k4	2
<g/>
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
vývozu	vývoz	k1gInSc2	vývoz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
<g/>
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
<g/>
9	[number]	k4	9
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
<g/>
5	[number]	k4	5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
<g/>
9	[number]	k4	9
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
dovozce	dovozce	k1gMnPc4	dovozce
pak	pak	k6eAd1	pak
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
,	,	kIx,	,
<g/>
7	[number]	k4	7
<g/>
%	%	kIx~	%
světového	světový	k2eAgInSc2d1	světový
dovozu	dovoz	k1gInSc2	dovoz
<g/>
)	)	kIx)	)
a	a	k8xC	a
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
,	,	kIx,	,
<g/>
0	[number]	k4	0
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
sklizeno	sklidit	k5eAaPmNgNnS	sklidit
z	z	k7c2	z
11,5	[number]	k4	11,5
milionů	milion	k4xCgInPc2	milion
stromů	strom	k1gInPc2	strom
154	[number]	k4	154
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
jablek	jablko	k1gNnPc2	jablko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
sklizeno	sklidit	k5eAaPmNgNnS	sklidit
z	z	k7c2	z
10,8	[number]	k4	10,8
milionů	milion	k4xCgInPc2	milion
stromů	strom	k1gInPc2	strom
105	[number]	k4	105
tisíc	tisíc	k4xCgInPc2	tisíc
tun	tuna	k1gFnPc2	tuna
jablek	jablko	k1gNnPc2	jablko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
plodů	plod	k1gInPc2	plod
==	==	k?	==
</s>
</p>
<p>
<s>
Jablka	jablko	k1gNnPc1	jablko
mají	mít	k5eAaImIp3nP	mít
pestré	pestrý	k2eAgFnPc4d1	pestrá
možnosti	možnost	k1gFnPc4	možnost
využití	využití	k1gNnSc2	využití
<g/>
:	:	kIx,	:
především	především	k6eAd1	především
se	se	k3xPyFc4	se
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
z	z	k7c2	z
jablek	jablko	k1gNnPc2	jablko
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
množství	množství	k1gNnSc1	množství
dalších	další	k2eAgInPc2d1	další
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Sušené	sušený	k2eAgNnSc4d1	sušené
ovoce	ovoce	k1gNnSc4	ovoce
===	===	k?	===
</s>
</p>
<p>
<s>
Sušení	sušení	k1gNnSc1	sušení
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
způsobům	způsob	k1gInPc3	způsob
konzervace	konzervace	k1gFnSc2	konzervace
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
opět	opět	k6eAd1	opět
nachází	nacházet	k5eAaImIp3nS	nacházet
své	svůj	k3xOyFgMnPc4	svůj
příznivce	příznivec	k1gMnPc4	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Šetrným	šetrný	k2eAgNnSc7d1	šetrné
sušením	sušení	k1gNnSc7	sušení
při	při	k7c6	při
nižších	nízký	k2eAgFnPc6d2	nižší
</s>
</p>
<p>
<s>
teplotách	teplota	k1gFnPc6	teplota
(	(	kIx(	(
<g/>
pod	pod	k7c4	pod
48	[number]	k4	48
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uchová	uchovat	k5eAaPmIp3nS	uchovat
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
způsobů	způsob	k1gInPc2	způsob
konzervace	konzervace	k1gFnSc2	konzervace
nejvíce	nejvíce	k6eAd1	nejvíce
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
minerálů	minerál	k1gInPc2	minerál
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Sušením	sušení	k1gNnPc3	sušení
se	se	k3xPyFc4	se
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
ovocné	ovocný	k2eAgInPc4d1	ovocný
cukry	cukr	k1gInPc4	cukr
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zamezí	zamezit	k5eAaPmIp3nS	zamezit
vývoji	vývoj	k1gInSc3	vývoj
nepříznivých	příznivý	k2eNgInPc2d1	nepříznivý
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k8xS	tak
si	se	k3xPyFc3	se
ovoce	ovoce	k1gNnSc1	ovoce
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
vůni	vůně	k1gFnSc4	vůně
a	a	k8xC	a
vzhled	vzhled	k1gInSc4	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Sušená	sušený	k2eAgNnPc1d1	sušené
jablka	jablko	k1gNnPc1	jablko
můžeme	moct	k5eAaImIp1nP	moct
jíst	jíst	k5eAaImF	jíst
i	i	k9	i
po	po	k7c6	po
namočení	namočení	k1gNnSc6	namočení
do	do	k7c2	do
vlažné	vlažný	k2eAgFnSc2d1	vlažná
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
mléka	mléko	k1gNnSc2	mléko
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
pečení	pečení	k1gNnSc6	pečení
i	i	k8xC	i
vaření	vaření	k1gNnSc6	vaření
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
činnost	činnost	k1gFnSc4	činnost
střev	střevo	k1gNnPc2	střevo
<g/>
,	,	kIx,	,
snižují	snižovat	k5eAaImIp3nP	snižovat
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
,	,	kIx,	,
normalizují	normalizovat	k5eAaBmIp3nP	normalizovat
střevní	střevní	k2eAgFnSc4d1	střevní
mikroflóru	mikroflóra	k1gFnSc4	mikroflóra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sušená	sušený	k2eAgNnPc1d1	sušené
jablka	jablko	k1gNnPc1	jablko
jsou	být	k5eAaImIp3nP	být
označována	označován	k2eAgNnPc1d1	označováno
jako	jako	k8xS	jako
křížaly	křížala	k1gFnSc2	křížala
<g/>
,	,	kIx,	,
v	v	k7c6	v
nářeční	nářeční	k2eAgFnSc6d1	nářeční
mluvě	mluva	k1gFnSc6	mluva
jako	jako	k9	jako
"	"	kIx"	"
<g/>
krájánky	krájánka	k1gFnSc2	krájánka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
speciálních	speciální	k2eAgFnPc6d1	speciální
manufakturách	manufaktura	k1gFnPc6	manufaktura
se	se	k3xPyFc4	se
jablka	jablko	k1gNnPc1	jablko
suší	sušit	k5eAaImIp3nP	sušit
až	až	k9	až
2	[number]	k4	2
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
postup	postup	k1gInSc4	postup
pro	pro	k7c4	pro
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
možnou	možný	k2eAgFnSc4d1	možná
kvalitu	kvalita	k1gFnSc4	kvalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jablečná	jablečný	k2eAgFnSc1d1	jablečná
šťáva	šťáva	k1gFnSc1	šťáva
===	===	k?	===
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
výtečným	výtečný	k2eAgInSc7d1	výtečný
zdrojem	zdroj	k1gInSc7	zdroj
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
přírodního	přírodní	k2eAgInSc2d1	přírodní
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
pektinu	pektin	k1gInSc2	pektin
<g/>
,	,	kIx,	,
ovocných	ovocný	k2eAgFnPc2d1	ovocná
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
přírodních	přírodní	k2eAgFnPc2d1	přírodní
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
má	mít	k5eAaImIp3nS	mít
šťáva	šťáva	k1gFnSc1	šťáva
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
lisováním	lisování	k1gNnSc7	lisování
rozemletých	rozemletý	k2eAgNnPc2d1	rozemleté
jablek	jablko	k1gNnPc2	jablko
-	-	kIx~	-
jablečné	jablečný	k2eAgFnPc1d1	jablečná
drtě	drť	k1gFnPc1	drť
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
uchování	uchování	k1gNnSc4	uchování
jablečné	jablečný	k2eAgFnSc2d1	jablečná
šťávy	šťáva	k1gFnSc2	šťáva
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
šťávu	šťáva	k1gFnSc4	šťáva
konzervovat	konzervovat	k5eAaBmF	konzervovat
vyšší	vysoký	k2eAgFnSc7d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
(	(	kIx(	(
<g/>
pasterizace	pasterizace	k1gFnSc1	pasterizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zahustit	zahustit	k5eAaPmF	zahustit
(	(	kIx(	(
<g/>
výroba	výroba	k1gFnSc1	výroba
koncentrátu	koncentrát	k1gInSc2	koncentrát
na	na	k7c6	na
odparkách	odparka	k1gFnPc6	odparka
<g/>
)	)	kIx)	)
či	či	k8xC	či
zchladit	zchladit	k5eAaPmF	zchladit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nefiltrovaná	filtrovaný	k2eNgFnSc1d1	nefiltrovaná
100	[number]	k4	100
<g/>
%	%	kIx~	%
jablečná	jablečný	k2eAgFnSc1d1	jablečná
šťáva	šťáva	k1gFnSc1	šťáva
bez	bez	k7c2	bez
přidání	přidání	k1gNnSc2	přidání
cukru	cukr	k1gInSc2	cukr
či	či	k8xC	či
jiných	jiný	k2eAgNnPc2d1	jiné
ochucovadel	ochucovadlo	k1gNnPc2	ochucovadlo
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jablečný	jablečný	k2eAgInSc1d1	jablečný
mošt	mošt	k1gInSc1	mošt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Jablečný	jablečný	k2eAgInSc1d1	jablečný
destilát	destilát	k1gInSc1	destilát
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zkvašení	zkvašení	k1gNnSc6	zkvašení
ovocných	ovocný	k2eAgInPc2d1	ovocný
cukrů	cukr	k1gInPc2	cukr
v	v	k7c6	v
rmutu	rmut	k1gInSc6	rmut
z	z	k7c2	z
drcených	drcený	k2eAgNnPc2d1	drcené
jablek	jablko	k1gNnPc2	jablko
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
jablečné	jablečný	k2eAgFnSc2d1	jablečná
šťávy	šťáva	k1gFnSc2	šťáva
vzniká	vznikat	k5eAaImIp3nS	vznikat
pomocí	pomocí	k7c2	pomocí
destilace	destilace	k1gFnSc2	destilace
ovocná	ovocný	k2eAgFnSc1d1	ovocná
pálenka	pálenka	k1gFnSc1	pálenka
-	-	kIx~	-
jablkovice	jablkovice	k1gFnSc1	jablkovice
(	(	kIx(	(
<g/>
calvados	calvados	k1gInSc1	calvados
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
jablek	jablko	k1gNnPc2	jablko
==	==	k?	==
</s>
</p>
<p>
<s>
Jablka	jablko	k1gNnPc1	jablko
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vitamín	vitamín	k1gInSc4	vitamín
C	C	kA	C
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
antioxidantů	antioxidant	k1gInPc2	antioxidant
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
DNA	dno	k1gNnPc4	dno
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
snižují	snižovat	k5eAaImIp3nP	snižovat
tak	tak	k6eAd1	tak
riziko	riziko	k1gNnSc4	riziko
vzniku	vznik	k1gInSc2	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
množství	množství	k1gNnSc4	množství
vlákniny	vláknina	k1gFnSc2	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
látky	látka	k1gFnPc1	látka
chrání	chránit	k5eAaImIp3nP	chránit
mozek	mozek	k1gInSc4	mozek
před	před	k7c7	před
Parkinsonovou	Parkinsonový	k2eAgFnSc7d1	Parkinsonová
a	a	k8xC	a
Alzheimerovou	Alzheimerův	k2eAgFnSc7d1	Alzheimerova
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
pektin	pektin	k1gInSc1	pektin
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
obsažen	obsažen	k2eAgMnSc1d1	obsažen
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
také	také	k9	také
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
poškození	poškození	k1gNnSc2	poškození
ledvin	ledvina	k1gFnPc2	ledvina
</s>
</p>
<p>
<s>
snižují	snižovat	k5eAaImIp3nP	snižovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
hladinu	hladina	k1gFnSc4	hladina
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
krevních	krevní	k2eAgInPc2d1	krevní
tuků	tuk	k1gInPc2	tuk
</s>
</p>
<p>
<s>
posilují	posilovat	k5eAaImIp3nP	posilovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
krevní	krevní	k2eAgInSc4d1	krevní
oběh	oběh	k1gInSc4	oběh
</s>
</p>
<p>
<s>
stabilizují	stabilizovat	k5eAaBmIp3nP	stabilizovat
hladinu	hladina	k1gFnSc4	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
</s>
</p>
<p>
<s>
čistí	čistý	k2eAgMnPc1d1	čistý
střeva	střevo	k1gNnSc2	střevo
</s>
</p>
<p>
<s>
posilují	posilovat	k5eAaImIp3nP	posilovat
dásně	dáseň	k1gFnPc1	dáseň
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Jablko	jablko	k1gNnSc1	jablko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
miliónů	milión	k4xCgInPc2	milión
bakterií	bakterie	k1gFnPc2	bakterie
z	z	k7c2	z
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
tisíce	tisíc	k4xCgInSc2	tisíc
mikrobiálních	mikrobiální	k2eAgInPc2d1	mikrobiální
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gFnSc1	jejich
koncentrace	koncentrace	k1gFnSc1	koncentrace
ve	v	k7c6	v
slupce	slupka	k1gFnSc6	slupka
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
kusů	kus	k1gInPc2	kus
ani	ani	k8xC	ani
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
organické	organický	k2eAgNnSc4d1	organické
nebo	nebo	k8xC	nebo
konvenční	konvenční	k2eAgNnSc4d1	konvenční
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
organické	organický	k2eAgNnSc1d1	organické
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
větší	veliký	k2eAgFnSc4d2	veliký
druhovou	druhový	k2eAgFnSc4d1	druhová
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
syrových	syrový	k2eAgNnPc6d1	syrové
jablkách	jablko	k1gNnPc6	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
podíl	podíl	k1gInSc1	podíl
vlákniny	vláknina	k1gFnSc2	vláknina
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
minerálů	minerál	k1gInPc2	minerál
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
slupce	slupka	k1gFnSc6	slupka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
loupaná	loupaný	k2eAgNnPc1d1	loupané
jablka	jablko	k1gNnPc1	jablko
mají	mít	k5eAaImIp3nP	mít
výrazně	výrazně	k6eAd1	výrazně
horší	zlý	k2eAgInSc4d2	horší
podíl	podíl	k1gInSc4	podíl
takovýchto	takovýto	k3xDgFnPc2	takovýto
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
jakostí	jakost	k1gFnPc2	jakost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgFnPc4d1	definována
následující	následující	k2eAgFnPc4d1	následující
jakosti	jakost	k1gFnPc4	jakost
jablek	jablko	k1gNnPc2	jablko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
I.	I.	kA	I.
jakost	jakost	k1gFnSc1	jakost
===	===	k?	===
</s>
</p>
<p>
<s>
Jablka	jablko	k1gNnSc2	jablko
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
vlastnosti	vlastnost	k1gFnPc1	vlastnost
typické	typický	k2eAgFnPc1d1	typická
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
odrůdu	odrůda	k1gFnSc4	odrůda
</s>
</p>
<p>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zcela	zcela	k6eAd1	zcela
nepoškozená	poškozený	k2eNgFnSc1d1	nepoškozená
</s>
</p>
<p>
<s>
Dovolena	dovolen	k2eAgFnSc1d1	dovolena
je	být	k5eAaImIp3nS	být
lehká	lehký	k2eAgFnSc1d1	lehká
vada	vada	k1gFnSc1	vada
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
či	či	k8xC	či
lehké	lehký	k2eAgNnSc1d1	lehké
poškození	poškození	k1gNnSc1	poškození
stopky	stopka	k1gFnSc2	stopka
</s>
</p>
<p>
<s>
Vady	vada	k1gFnPc1	vada
slupky	slupka	k1gFnSc2	slupka
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
poškození	poškození	k1gNnSc1	poškození
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
max	max	kA	max
2	[number]	k4	2
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnSc1d1	ostatní
poškození	poškození	k1gNnSc1	poškození
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
nejvýše	vysoce	k6eAd3	vysoce
1	[number]	k4	1
cm2	cm2	k4	cm2
</s>
</p>
<p>
<s>
Strupovitost	strupovitost	k1gFnSc1	strupovitost
povolena	povolen	k2eAgFnSc1d1	povolena
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
max.	max.	kA	max.
0,25	[number]	k4	0,25
cm2	cm2	k4	cm2
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
jablka	jablko	k1gNnSc2	jablko
minimálně	minimálně	k6eAd1	minimálně
5,5	[number]	k4	5,5
–	–	k?	–
6,5	[number]	k4	6,5
cm	cm	kA	cm
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
II	II	kA	II
<g/>
.	.	kIx.	.
jakost	jakost	k1gFnSc1	jakost
===	===	k?	===
</s>
</p>
<p>
<s>
Jablka	jablko	k1gNnPc1	jablko
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
také	také	k9	také
zachované	zachovaný	k2eAgFnPc4d1	zachovaná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
odrůdu	odrůda	k1gFnSc4	odrůda
</s>
</p>
<p>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
bez	bez	k7c2	bez
vážných	vážný	k2eAgFnPc2d1	vážná
vad	vada	k1gFnPc2	vada
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
slupka	slupka	k1gFnSc1	slupka
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
chybět	chybět	k5eAaImF	chybět
stopka	stopka	k1gFnSc1	stopka
</s>
</p>
<p>
<s>
Dovoleny	dovolen	k2eAgFnPc1d1	dovolena
jsou	být	k5eAaImIp3nP	být
vady	vada	k1gFnPc1	vada
slupky	slupka	k1gFnSc2	slupka
tentokrát	tentokrát	k6eAd1	tentokrát
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
nejvýše	vysoce	k6eAd3	vysoce
4	[number]	k4	4
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnSc1d1	ostatní
poškození	poškození	k1gNnSc1	poškození
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
ploše	plocha	k1gFnSc6	plocha
nejvýše	vysoce	k6eAd3	vysoce
2,5	[number]	k4	2,5
cm2	cm2	k4	cm2
</s>
</p>
<p>
<s>
Strupovitost	strupovitost	k1gFnSc1	strupovitost
povolena	povolit	k5eAaPmNgFnS	povolit
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
až	až	k9	až
1	[number]	k4	1
cm2	cm2	k4	cm2
</s>
</p>
<p>
<s>
Velikost	velikost	k1gFnSc1	velikost
jablka	jablko	k1gNnSc2	jablko
minimálně	minimálně	k6eAd1	minimálně
5,5	[number]	k4	5,5
–	–	k?	–
6,5	[number]	k4	6,5
cm	cm	kA	cm
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
odrůd	odrůda	k1gFnPc2	odrůda
zapsaných	zapsaný	k2eAgFnPc2d1	zapsaná
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
odrůdové	odrůdový	k2eAgFnSc6d1	odrůdová
knize	kniha	k1gFnSc6	kniha
ke	k	k7c3	k
dni	den	k1gInSc3	den
1.	[number]	k4	1.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Ústřední	ústřední	k2eAgInSc1d1	ústřední
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
a	a	k8xC	a
zkušební	zkušební	k2eAgInSc1d1	zkušební
ústav	ústav	k1gInSc1	ústav
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2008-03-04	[number]	k4	2008-03-04
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
krajové	krajový	k2eAgFnPc1d1	krajová
odrůdy	odrůda	k1gFnPc1	odrůda
ovocných	ovocný	k2eAgFnPc2d1	ovocná
dřevin	dřevina	k1gFnPc2	dřevina
-	-	kIx~	-
problematika	problematika	k1gFnSc1	problematika
a	a	k8xC	a
možnosti	možnost	k1gFnPc1	možnost
využití	využití	k1gNnSc2	využití
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rožnov	Rožnov	k1gInSc1	Rožnov
pod	pod	k7c7	pod
Radhoštěm	Radhošť	k1gMnSc7	Radhošť
<g/>
:	:	kIx,	:
ZO	ZO	kA	ZO
ČSOP	ČSOP	kA	ČSOP
Salamandr	salamandr	k1gMnSc1	salamandr
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015-07-14	[number]	k4	2015-07-14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŘÍHA	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ovoce	ovoce	k1gNnSc1	ovoce
:	:	kIx,	:
Díl	díl	k1gInSc1	díl
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jablka	jablko	k1gNnPc4	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Čs.	čs.	kA	čs.
pomolog	pomolog	k1gMnSc1	pomolog
<g/>
.	.	kIx.	.
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
1919.	[number]	k4	1919.
248	[number]	k4	248
s	s	k7c7	s
<g/>
.	.	kIx.	.
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Jabloň	jabloň	k1gFnSc1	jabloň
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
odrůd	odrůda	k1gFnPc2	odrůda
jablek	jablko	k1gNnPc2	jablko
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
jablko	jablko	k1gNnSc4	jablko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
jablko	jablko	k1gNnSc4	jablko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Jablko	jablko	k1gNnSc4	jablko
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnPc1	galerie
jablko	jablko	k1gNnSc4	jablko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
