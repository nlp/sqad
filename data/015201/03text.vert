<s>
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
KarpovcevOsobní	KarpovcevOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1970	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
SSSR	SSSR	kA
Datum	datum	k1gNnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
41	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Tunošna	Tunošna	k6eAd1
Stát	stát	k1gInSc1
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Sovětský	sovětský	k2eAgInSc1d1
svazRusko	svazRusko	k6eAd1
Rusko	Rusko	k1gNnSc4
Výška	výška	k1gFnSc1
</s>
<s>
188	#num#	k4
cm	cm	kA
Váha	váha	k1gFnSc1
</s>
<s>
91	#num#	k4
kg	kg	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Aktivní	aktivní	k2eAgInPc4d1
roky	rok	k1gInPc4
</s>
<s>
1987	#num#	k4
-	-	kIx~
2008	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
obránce	obránce	k1gMnPc4
Kluby	klub	k1gInPc1
</s>
<s>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
MoskvaNew	MoskvaNew	k1gMnSc1
York	York	k1gInSc1
RangersToronto	RangersToronto	k1gNnSc1
Maple	Maple	k1gInSc1
LeafsChicago	LeafsChicago	k1gMnSc1
BlackhawksNew	BlackhawksNew	k1gFnPc2
York	York	k1gInSc1
IslandersSibir	IslandersSibir	k1gMnSc1
NovosibirskLokomotiv	NovosibirskLokomotiv	k1gFnSc1
JaroslavlFlorida	JaroslavlFlorida	k1gFnSc1
PanthersAvangard	PanthersAvangard	k1gInSc4
Omsk	Omsk	k1gInSc1
Draft	draft	k1gInSc1
NHL	NHL	kA
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
jako	jako	k8xS,k8xC
158	#num#	k4
<g/>
.	.	kIx.
celkovětýmem	celkovětýmo	k1gNnSc7
Quebec	Quebec	k1gMnSc1
Nordiques	Nordiques	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
1993	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
2005	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
juniorů	junior	k1gMnPc2
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MSJ	MSJ	kA
1990	#num#	k4
</s>
<s>
SSSR	SSSR	kA
20	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
juniorů	junior	k1gMnPc2
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
"	"	kIx"
<g/>
18	#num#	k4
<g/>
"	"	kIx"
1988	#num#	k4
</s>
<s>
SSSR	SSSR	kA
18	#num#	k4
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
semifinále	semifinále	k1gNnSc1
</s>
<s>
SP	SP	kA
1996	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
</s>
<s>
Stanley	Stanlea	k1gFnPc1
Cup	cup	k1gInSc1
</s>
<s>
vítěz	vítěz	k1gMnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
New	New	k?
York	York	k1gInSc1
Rangers	Rangers	k1gInSc1
</s>
<s>
SSSR	SSSR	kA
<g/>
/	/	kIx~
<g/>
Rusko	Rusko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
KHL	KHL	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
Moskva	Moskva	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
SSSR	SSSR	kA
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
Moskva	Moskva	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
Moskva	Moskva	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
Moskva	Moskva	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Rusko	Rusko	k1gNnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
Moskva	Moskva	k1gFnSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
KHL	KHL	kA
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Ak	Ak	k?
Bars	Bars	k?
Kazaň	Kazaň	k1gFnSc1
(	(	kIx(
<g/>
asistent	asistent	k1gMnSc1
trenéra	trenér	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Georgijevič	Georgijevič	k1gMnSc1
Karpovcev	Karpovcev	k1gFnSc1
(	(	kIx(
<g/>
А	А	k?
Г	Г	k?
К	К	k?
<g/>
,	,	kIx,
*	*	kIx~
7	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1970	#num#	k4
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
,	,	kIx,
SSSR	SSSR	kA
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
<g/>
,	,	kIx,
Jaroslavl	Jaroslavl	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
ruský	ruský	k2eAgMnSc1d1
hokejový	hokejový	k2eAgMnSc1d1
obránce	obránce	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
působil	působit	k5eAaImAgMnS
i	i	k9
v	v	k7c6
NHL	NHL	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
letecké	letecký	k2eAgFnSc6d1
havárii	havárie	k1gFnSc6
klubu	klub	k1gInSc2
Lokomotiv	lokomotiva	k1gFnPc2
Jaroslavl	Jaroslavl	k1gFnPc2
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yIgInSc2,k3yQgInSc2,k3yRgInSc2
působil	působit	k5eAaImAgMnS
jako	jako	k9
asistent	asistent	k1gMnSc1
trenéra	trenér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
V	v	k7c6
mládežnických	mládežnický	k2eAgFnPc6d1
kategoriích	kategorie	k1gFnPc6
reprezentoval	reprezentovat	k5eAaImAgInS
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
-	-	kIx~
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
do	do	k7c2
18	#num#	k4
let	léto	k1gNnPc2
1988	#num#	k4
v	v	k7c6
Československu	Československo	k1gNnSc6
vybojoval	vybojovat	k5eAaPmAgInS
se	s	k7c7
spoluhráči	spoluhráč	k1gMnPc7
bronz	bronz	k1gInSc4
<g/>
,	,	kIx,
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
s	s	k7c7
reprezentací	reprezentace	k1gFnSc7
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
získal	získat	k5eAaPmAgMnS
stříbro	stříbro	k1gNnSc1
na	na	k7c6
světovém	světový	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
této	tento	k3xDgFnSc2
věkové	věkový	k2eAgFnSc2d1
kategorie	kategorie	k1gFnSc2
ve	v	k7c6
Finsku	Finsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dresu	dres	k1gInSc6
ruské	ruský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
hrál	hrát	k5eAaImAgInS
na	na	k7c4
MS	MS	kA
1993	#num#	k4
v	v	k7c6
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
zlato	zlato	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
1996	#num#	k4
(	(	kIx(
<g/>
semifinále	semifinále	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
MS	MS	kA
2005	#num#	k4
v	v	k7c6
Rakousku	Rakousko	k1gNnSc6
(	(	kIx(
<g/>
bronz	bronz	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reprezentační	reprezentační	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
1988SSSR	1988SSSR	k4
18ME	18ME	k4
1860224	#num#	k4
</s>
<s>
1990SSSR	1990SSSR	k4
20MS	20MS	k4
2070118	#num#	k4
</s>
<s>
1993	#num#	k4
<g/>
RuskoMS	RuskoMS	k1gFnSc1
<g/>
801110	#num#	k4
</s>
<s>
1996	#num#	k4
<g/>
RuskoSP	RuskoSP	k1gFnSc1
<g/>
10000	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
RuskoMS	RuskoMS	k1gFnSc1
<g/>
80112	#num#	k4
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
sovětské	sovětský	k2eAgFnSc6d1
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
debutoval	debutovat	k5eAaBmAgMnS
ve	v	k7c6
dvou	dva	k4xCgNnPc6
utkáních	utkání	k1gNnPc6
ročníku	ročník	k1gInSc2
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
v	v	k7c6
dresu	dres	k1gInSc6
mateřského	mateřský	k2eAgInSc2d1
klubu	klub	k1gInSc2
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
Moskva	Moskva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klubu	klub	k1gInSc6
působil	působit	k5eAaImAgMnS
až	až	k6eAd1
do	do	k7c2
úvodu	úvod	k1gInSc2
ročníku	ročník	k1gInSc2
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
klubu	klub	k1gInSc2
NHL	NHL	kA
New	New	k1gFnSc1
York	York	k1gInSc1
Rangers	Rangers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
premiérové	premiérový	k2eAgFnSc6d1
sezoně	sezona	k1gFnSc6
slavil	slavit	k5eAaImAgMnS
zisk	zisk	k1gInSc4
Stanley	Stanlea	k1gMnSc2
Cupu	cup	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sezona	sezona	k1gFnSc1
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
začala	začít	k5eAaPmAgFnS
kvůli	kvůli	k7c3
sporům	spor	k1gInPc3
majitelů	majitel	k1gMnPc2
klubů	klub	k1gInPc2
a	a	k8xC
hráčských	hráčský	k2eAgInPc2d1
odborů	odbor	k1gInPc2
později	pozdě	k6eAd2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jí	jíst	k5eAaImIp3nS
Karpovcev	Karpovcev	k1gFnSc4
rozehrál	rozehrát	k5eAaPmAgMnS
v	v	k7c6
Dynamu	dynamo	k1gNnSc6
<g/>
,	,	kIx,
po	po	k7c6
rozběhnutí	rozběhnutí	k1gNnSc6
soutěže	soutěž	k1gFnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rangers	Rangers	k1gInSc1
jej	on	k3xPp3gNnSc4
v	v	k7c6
sezoně	sezona	k1gFnSc6
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
vyměnili	vyměnit	k5eAaPmAgMnP
do	do	k7c2
celku	celek	k1gInSc2
Toronto	Toronto	k1gNnSc1
Maple	Maple	k1gFnSc2
Leafs	Leafsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
působil	působit	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úvodu	úvod	k1gInSc6
sezony	sezona	k1gFnSc2
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
neměl	mít	k5eNaImAgMnS
v	v	k7c6
NHL	NHL	kA
angažmá	angažmá	k1gNnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
odehrál	odehrát	k5eAaPmAgMnS
pět	pět	k4xCc4
utkání	utkání	k1gNnPc2
za	za	k7c4
Dynamo	dynamo	k1gNnSc4
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dohodl	dohodnout	k5eAaPmAgInS
na	na	k7c6
návratu	návrat	k1gInSc6
do	do	k7c2
zámoří	zámoří	k1gNnSc2
v	v	k7c6
dresu	dres	k1gInSc6
Chicago	Chicago	k1gNnSc4
Blackhawks	Blackhawksa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezoně	sezona	k1gFnSc6
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
jej	on	k3xPp3gNnSc4
Chicago	Chicago	k1gNnSc1
vyměnilo	vyměnit	k5eAaPmAgNnS
do	do	k7c2
New	New	k1gMnPc2
York	York	k1gInSc1
Islanders	Islandersa	k1gFnPc2
-	-	kIx~
většinu	většina	k1gFnSc4
tohoto	tento	k3xDgInSc2
ročníku	ročník	k1gInSc2
marodil	marodit	k5eAaImAgMnS
se	s	k7c7
zraněním	zranění	k1gNnSc7
kotníku	kotník	k1gInSc2
(	(	kIx(
<g/>
stejný	stejný	k2eAgInSc1d1
problém	problém	k1gInSc1
jej	on	k3xPp3gMnSc4
provázel	provázet	k5eAaImAgInS
i	i	k9
o	o	k7c4
sezonu	sezona	k1gFnSc4
dříve	dříve	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sezona	sezona	k1gFnSc1
NHL	NHL	kA
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
pro	pro	k7c4
opětovné	opětovný	k2eAgInPc4d1
spory	spor	k1gInPc4
hráčů	hráč	k1gMnPc2
a	a	k8xC
vedení	vedení	k1gNnSc2
ligy	liga	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jí	jíst	k5eAaImIp3nS
Karpovcev	Karpovcev	k1gFnSc4
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
ruské	ruský	k2eAgFnSc6d1
superlize	superliga	k1gFnSc6
-	-	kIx~
zde	zde	k6eAd1
hájil	hájit	k5eAaImAgMnS
barvy	barva	k1gFnPc4
klubů	klub	k1gInPc2
Sibir	Sibir	k1gMnSc1
Novosibirsk	Novosibirsk	k1gInSc1
a	a	k8xC
Lokomotiv	lokomotiva	k1gFnPc2
Jaroslavl	Jaroslavl	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
výluce	výluka	k1gFnSc6
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
NHL	NHL	kA
<g/>
,	,	kIx,
za	za	k7c4
Florida	Florida	k1gFnSc1
Panthers	Panthers	k1gInSc1
však	však	k9
odehrál	odehrát	k5eAaPmAgInS
jen	jen	k9
šest	šest	k4xCc4
utkání	utkání	k1gNnPc2
<g/>
,	,	kIx,
ročník	ročník	k1gInSc1
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
dokončil	dokončit	k5eAaPmAgMnS
v	v	k7c6
Novosibirsku	Novosibirsk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
klubu	klub	k1gInSc6
působil	působit	k5eAaImAgMnS
do	do	k7c2
průběhu	průběh	k1gInSc2
sezony	sezona	k1gFnSc2
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
během	během	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
Avangardu	Avangard	k1gInSc2
Omsk	Omsk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
během	během	k7c2
této	tento	k3xDgFnSc2
sezony	sezona	k1gFnSc2
ukončil	ukončit	k5eAaPmAgInS
aktivní	aktivní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
ročníku	ročník	k1gInSc6
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
zastával	zastávat	k5eAaImAgMnS
funkci	funkce	k1gFnSc4
asistenta	asistent	k1gMnSc2
trenéra	trenér	k1gMnSc2
v	v	k7c6
klubu	klub	k1gInSc6
KHL	KHL	kA
Ak	Ak	k1gMnSc1
Bars	Bars	k?
Kazaň	Kazaň	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
létě	léto	k1gNnSc6
2011	#num#	k4
v	v	k7c6
Jaroslavli	Jaroslavli	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Statistika	statistika	k1gFnSc1
</s>
<s>
Debut	debut	k1gInSc1
v	v	k7c6
NHL	NHL	kA
-	-	kIx~
9	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1993	#num#	k4
(	(	kIx(
<g/>
Pittsburgh	Pittsburgh	k1gInSc1
Penguins	Penguins	k1gInSc1
-	-	kIx~
NEW	NEW	kA
YORK	York	k1gInSc1
RANGERS	RANGERS	kA
<g/>
)	)	kIx)
</s>
<s>
První	první	k4xOgInSc4
gól	gól	k1gInSc4
v	v	k7c6
NHL	NHL	kA
(	(	kIx(
<g/>
a	a	k8xC
zároveň	zároveň	k6eAd1
první	první	k4xOgInSc4
bod	bod	k1gInSc4
<g/>
)	)	kIx)
-	-	kIx~
13	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1992	#num#	k4
(	(	kIx(
<g/>
NEW	NEW	kA
YORK	York	k1gInSc1
RANGERS	RANGERS	kA
-	-	kIx~
Quebec	Quebec	k1gMnSc1
Nordiques	Nordiques	k1gMnSc1
<g/>
,	,	kIx,
vítězný	vítězný	k2eAgInSc1d1
gól	gól	k1gInSc1
při	při	k7c6
druhém	druhý	k4xOgInSc6
startu	start	k1gInSc6
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Sezona	sezona	k1gFnSc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
část	část	k1gFnSc1
</s>
<s>
Play	play	k0
off	off	k?
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
ZGABTM	ZGABTM	kA
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc4
MoskvaSSSR	MoskvaSSSR	k1gFnSc2
<g/>
20110	#num#	k4
<g/>
—	—	k?
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc4
MoskvaSSSR	MoskvaSSSR	k1gFnSc2
<g/>
3511227	#num#	k4
<g/>
—	—	k?
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc4
MoskvaSSSR	MoskvaSSSR	k1gFnSc2
<g/>
4005515	#num#	k4
<g/>
—	—	k?
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
MoskvaRusko	MoskvaRusko	k1gNnSc1
<g/>
2832522	#num#	k4
<g/>
—	—	k?
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
MoskvaRusko	MoskvaRusko	k1gNnSc1
<g/>
3631114100	#num#	k4
<g/>
—	—	k?
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
MoskvaRusko	MoskvaRusko	k1gNnSc1
<g/>
30006	#num#	k4
<g/>
—	—	k?
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
New	New	k1gFnPc2
York	York	k1gInSc4
RangersNHL	RangersNHL	k1gFnSc4
<g/>
6731518581704412	#num#	k4
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
MoskvaRusko	MoskvaRusko	k1gNnSc1
<g/>
1302210	#num#	k4
<g/>
—	—	k?
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
New	New	k1gFnPc2
York	York	k1gInSc4
RangersNHL	RangersNHL	k1gFnSc4
<g/>
4748123081010	#num#	k4
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
New	New	k1gFnPc2
York	York	k1gInSc4
RangersNHL	RangersNHL	k1gFnSc4
<g/>
40216182660114	#num#	k4
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
New	New	k1gFnPc2
York	York	k1gInSc4
RangersNHL	RangersNHL	k1gFnSc4
<g/>
7792938591313420	#num#	k4
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
New	New	k1gMnPc2
York	York	k1gInSc4
RangersNHL	RangersNHL	k1gFnPc2
<g/>
47371048	#num#	k4
<g/>
—	—	k?
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
New	New	k1gMnPc2
York	York	k1gInSc4
RangersNHL	RangersNHL	k1gFnPc2
<g/>
21010	#num#	k4
<g/>
—	—	k?
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
Toronto	Toronto	k1gNnSc1
Maple	Mapla	k1gFnSc3
LeafsNHL	LeafsNHL	k1gFnSc1
<g/>
5622527521413412	#num#	k4
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
Toronto	Toronto	k1gNnSc4
Maple	Maple	k1gNnSc2
LeafsNHL	LeafsNHL	k1gFnSc2
<g/>
693141754110334	#num#	k4
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
HC	HC	kA
Dynamo	dynamo	k1gNnSc1
MoskvaRusko	MoskvaRusko	k1gNnSc1
<g/>
50110	#num#	k4
<g/>
—	—	k?
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc4
BlackhawksNHL	BlackhawksNHL	k1gFnSc2
<g/>
532131539	#num#	k4
<g/>
—	—	k?
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc4
BlackhawksNHL	BlackhawksNHL	k1gFnSc2
<g/>
6519104051010	#num#	k4
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc4
BlackhawksNHL	BlackhawksNHL	k1gFnSc2
<g/>
404101412	#num#	k4
<g/>
—	—	k?
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc4
BlackhawksNHL	BlackhawksNHL	k1gFnSc2
<g/>
2407714	#num#	k4
<g/>
—	—	k?
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
New	New	k1gMnPc2
York	York	k1gInSc4
IslandersNHL	IslandersNHL	k1gFnPc2
<g/>
30114	#num#	k4
<g/>
—	—	k?
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
Sibir	Sibira	k1gFnPc2
NovosibirskRusko	NovosibirskRusko	k1gNnSc1
<g/>
501116	#num#	k4
<g/>
—	—	k?
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
Lokomotiv	lokomotiva	k1gFnPc2
JaroslavlRusko	JaroslavlRusko	k1gNnSc1
<g/>
332464590000	#num#	k4
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
Florida	Florida	k1gFnSc1
PanthersNHL	PanthersNHL	k1gFnSc1
<g/>
60004	#num#	k4
<g/>
—	—	k?
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
Sibir	Sibira	k1gFnPc2
NovosibirskRusko	NovosibirskRusko	k1gNnSc1
<g/>
182133930004	#num#	k4
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
Sibir	Sibira	k1gFnPc2
NovosibirskRusko	NovosibirskRusko	k1gNnSc1
<g/>
39513189071238	#num#	k4
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
Sibir	Sibira	k1gFnPc2
NovosibirskRusko	NovosibirskRusko	k1gNnSc1
<g/>
602227	#num#	k4
<g/>
—	—	k?
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
Avangard	Avangarda	k1gFnPc2
OmskRusko	OmskRusko	k1gNnSc1
<g/>
810110	#num#	k4
<g/>
—	—	k?
</s>
<s>
Celkem	celkem	k6eAd1
NHL	NHL	kA
</s>
<s>
596	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
154	#num#	k4
</s>
<s>
188	#num#	k4
</s>
<s>
430	#num#	k4
</s>
<s>
74	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
52	#num#	k4
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Karpovcev	Karpovcev	k1gFnSc1
byl	být	k5eAaImAgInS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
manželkou	manželka	k1gFnSc7
Jannou	Janný	k2eAgFnSc7d1
měl	mít	k5eAaImAgMnS
dcery	dcera	k1gFnPc4
Dašu	Dašus	k1gInSc2
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Stacy	Staca	k1gFnSc2
(	(	kIx(
<g/>
nar	nar	kA
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
–	–	k?
statistiky	statistika	k1gFnSc2
na	na	k7c4
Hockeydb	Hockeydb	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
–	–	k?
statistiky	statistika	k1gFnSc2
na	na	k7c4
Eliteprospects	Eliteprospects	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
–	–	k?
statistiky	statistika	k1gFnSc2
na	na	k7c4
NHL	NHL	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
na	na	k7c6
MSJ	MSJ	kA
1990	#num#	k4
Kanada	Kanada	k1gFnSc1
20	#num#	k4
</s>
<s>
Brankáři	brankář	k1gMnPc1
<g/>
:	:	kIx,
Stéphane	Stéphan	k1gMnSc5
Fiset	Fiset	k1gMnSc1
•	•	k?
Trevor	Trevor	k1gMnSc1
Kidd	Kidd	k1gMnSc1
</s>
<s>
Obránci	obránce	k1gMnPc1
<g/>
:	:	kIx,
Patrice	patrice	k1gFnSc1
Brisebois	Brisebois	k1gFnSc2
•	•	k?
Kevin	Kevin	k1gMnSc1
Haller	Haller	k1gMnSc1
•	•	k?
Dan	Dan	k1gMnSc1
Ratushny	Ratushna	k1gFnSc2
•	•	k?
Stewart	Stewart	k1gMnSc1
Malgunas	Malgunas	k1gMnSc1
•	•	k?
Jason	Jason	k1gInSc1
Herter	Herter	k1gInSc1
•	•	k?
Adrien	Adriena	k1gFnPc2
Plavsic	Plavsice	k1gInPc2
</s>
<s>
Útočníci	útočník	k1gMnPc1
<g/>
:	:	kIx,
Dave	Dav	k1gInSc5
Chyzowski	Chyzowsk	k1gMnPc7
•	•	k?
Mike	Mik	k1gInSc2
Needham	Needham	k1gInSc1
•	•	k?
Mike	Mike	k1gFnSc1
Ricci	Ricec	k1gInSc3
•	•	k?
Dwayne	Dwayn	k1gInSc5
Norris	Norris	k1gInSc1
•	•	k?
Stu	sto	k4xCgNnSc3
Barnes	Barnes	k1gMnSc1
•	•	k?
Wes	Wes	k1gMnSc1
Walz	Walz	k1gMnSc1
•	•	k?
Eric	Eric	k1gInSc1
Lindros	Lindrosa	k1gFnPc2
•	•	k?
Mike	Mik	k1gInPc1
Craig	Craig	k1gInSc1
•	•	k?
Kent	Kent	k2eAgInSc1d1
Manderville	Manderville	k1gInSc1
•	•	k?
Scott	Scott	k2eAgInSc1d1
Pellerin	Pellerin	k1gInSc1
•	•	k?
Steven	Steven	k2eAgInSc1d1
Rice	Rice	k1gInSc1
•	•	k?
Kris	kris	k1gInSc1
Draper	Draper	k1gInSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
SSSR	SSSR	kA
20	#num#	k4
</s>
<s>
Brankáři	brankář	k1gMnPc1
<g/>
:	:	kIx,
Sergej	Sergej	k1gMnSc1
Tkačenko	Tkačenka	k1gFnSc5
•	•	k?
Sergej	Sergej	k1gMnSc1
Poljakov	Poljakov	k1gInSc1
</s>
<s>
Obránci	obránce	k1gMnPc1
<g/>
:	:	kIx,
Alexandr	Alexandr	k1gMnSc1
Godyňuk	Godyňuk	k1gMnSc1
•	•	k?
Igor	Igor	k1gMnSc1
Ivanov	Ivanovo	k1gNnPc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Zubov	Zubovo	k1gNnPc2
•	•	k?
Dmitrij	Dmitrij	k1gMnSc1
Juškevič	Juškevič	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Tertyšnyj	Tertyšnyj	k1gMnSc1
•	•	k?
Eugene	Eugen	k1gInSc5
Regents	Regentsa	k1gFnPc2
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcev	k1gFnSc4
</s>
<s>
Útočníci	útočník	k1gMnPc1
<g/>
:	:	kIx,
Andrej	Andrej	k1gMnSc1
Kovalenko	Kovalenka	k1gFnSc5
•	•	k?
Vjačeslav	Vjačeslav	k1gMnSc1
Kozlov	Kozlovo	k1gNnPc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Bure	Bur	k1gFnSc2
•	•	k?
Roman	Roman	k1gMnSc1
Oksjuta	Oksjut	k1gMnSc2
•	•	k?
Viktor	Viktor	k1gMnSc1
Gordyjuk	Gordyjuk	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Žamnov	Žamnovo	k1gNnPc2
•	•	k?
Vjačeslav	Vjačeslav	k1gMnSc1
Bucajev	Bucajev	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Martyňuk	Martyňuk	k1gMnSc1
•	•	k?
Andrej	Andrej	k1gMnSc1
Potajčuk	Potajčuk	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Kudašov	Kudašovo	k1gNnPc2
•	•	k?
Jan	Jan	k1gMnSc1
Kaminski	Kaminsk	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
ČSSR	ČSSR	kA
20	#num#	k4
</s>
<s>
Brankáři	brankář	k1gMnPc1
<g/>
:	:	kIx,
Roman	Roman	k1gMnSc1
Turek	Turek	k1gMnSc1
•	•	k?
Robert	Robert	k1gMnSc1
Horyna	Horyna	k1gMnSc1
</s>
<s>
Obránci	obránce	k1gMnPc1
<g/>
:	:	kIx,
Jiří	Jiří	k1gMnSc1
Šlégr	Šlégr	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Vykoukal	Vykoukal	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Kuchyňa	Kuchyňa	k1gMnSc1
•	•	k?
Jiří	Jiří	k1gMnSc1
Malinský	Malinský	k2eAgMnSc1d1
•	•	k?
Ján	Ján	k1gMnSc1
Varholík	Varholík	k1gMnSc1
•	•	k?
Miloš	Miloš	k1gMnSc1
Holaň	Holaň	k1gMnSc1
•	•	k?
Richard	Richard	k1gMnSc1
Šmehlík	Šmehlík	k1gMnSc1
</s>
<s>
Útočníci	útočník	k1gMnPc1
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Reichel	Reichel	k1gMnSc1
•	•	k?
Jaromír	Jaromír	k1gMnSc1
Jágr	Jágr	k1gMnSc1
•	•	k?
Bobby	Bobba	k1gFnSc2
Holík	Holík	k1gMnSc1
•	•	k?
Pavol	Pavol	k1gInSc1
Zůbek	Zůbek	k1gMnSc1
•	•	k?
Martin	Martin	k1gMnSc1
Procházka	Procházka	k1gMnSc1
•	•	k?
Luboš	Luboš	k1gMnSc1
Rob	roba	k1gFnPc2
•	•	k?
Ladislav	Ladislav	k1gMnSc1
Karabín	Karabín	k1gMnSc1
•	•	k?
Petr	Petr	k1gMnSc1
Bareš	Bareš	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Mach	Mach	k1gMnSc1
•	•	k?
Peter	Peter	k1gMnSc1
Zůbek	Zůbek	k1gMnSc1
•	•	k?
Marián	Marián	k1gMnSc1
Uharček	Uharček	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
MS	MS	kA
1993	#num#	k4
–	–	k?
Rusko	Rusko	k1gNnSc1
Brankáři	brankář	k1gMnPc7
</s>
<s>
Andrej	Andrej	k1gMnSc1
Trefilov	Trefilovo	k1gNnPc2
•	•	k?
Maxim	Maxim	k1gMnSc1
Michajlovskij	Michajlovskij	k1gMnSc1
•	•	k?
Andrej	Andrej	k1gMnSc1
Zujev	Zujev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obránci	obránce	k1gMnPc7
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
•	•	k?
Alexander	Alexandra	k1gFnPc2
Smirnov	Smirnovo	k1gNnPc2
•	•	k?
Ilja	Ilja	k1gMnSc1
Bjakin	Bjakin	k1gMnSc1
•	•	k?
Dmitrij	Dmitrij	k1gFnSc1
Frolov	Frolovo	k1gNnPc2
•	•	k?
Andrej	Andrej	k1gMnSc1
Sapožnikov	Sapožnikov	k1gInSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Šenděljev	Šenděljev	k1gMnSc1
•	•	k?
Dmitrij	Dmitrij	k1gMnSc1
Juškjevič	Juškjevič	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Sorokin	Sorokin	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Sergej	Sergej	k1gMnSc1
Petrenko	Petrenka	k1gFnSc5
•	•	k?
German	German	k1gMnSc1
Titov	Titovo	k1gNnPc2
•	•	k?
Andrej	Andrej	k1gMnSc1
Chomutov	Chomutov	k1gInSc1
•	•	k?
Jan	Jan	k1gMnSc1
Kaminskij	Kaminskij	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Jašin	Jašin	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Puškov	Puškov	k1gInSc1
•	•	k?
Andrej	Andrej	k1gMnSc1
Nikolišin	Nikolišin	k2eAgMnSc1d1
•	•	k?
Valerij	Valerij	k1gMnSc1
Karpov	Karpovo	k1gNnPc2
•	•	k?
Konstantin	Konstantin	k1gMnSc1
Astrachancev	Astrachancva	k1gFnPc2
•	•	k?
Vjačeslav	Vjačeslav	k1gMnSc1
Bykov	Bykovo	k1gNnPc2
–	–	k?
•	•	k?
Igor	Igor	k1gMnSc1
Varickij	Varickij	k1gMnSc1
•	•	k?
Vjačeslav	Vjačeslav	k1gMnSc1
Bucajev	Bucajev	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Boris	Boris	k1gMnSc1
Michajlov	Michajlov	k1gInSc4
</s>
<s>
MS	MS	kA
2005	#num#	k4
–	–	k?
Rusko	Rusko	k1gNnSc1
Brankáři	brankář	k1gMnPc5
</s>
<s>
Maxim	Maxim	k1gMnSc1
Sokolov	Sokolovo	k1gNnPc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Zvjagin	Zvjagin	k1gMnSc1
•	•	k?
Vasilij	Vasilij	k1gMnSc1
Košečkin	Košečkin	k1gMnSc1
Obránci	obránce	k1gMnSc3
</s>
<s>
Denis	Denisa	k1gFnPc2
Denisov	Denisovo	k1gNnPc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Gusev	Gusva	k1gFnPc2
•	•	k?
Dmitrij	Dmitrij	k1gMnSc1
Kalinin	Kalinin	k2eAgMnSc1d1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
•	•	k?
Andrej	Andrej	k1gMnSc1
Markov	Markov	k1gInSc1
•	•	k?
Vitalij	Vitalij	k1gFnSc2
Proškin	Proškin	k1gMnSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Rjazancev	Rjazancva	k1gFnPc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Vyšedkejevič	Vyšedkejevič	k1gMnSc1
Útočníci	útočník	k1gMnPc1
</s>
<s>
Maxim	Maxim	k1gMnSc1
Afinogenov	Afinogenovo	k1gNnPc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Dacjuk	Dacjuk	k1gMnSc1
•	•	k?
Fjodor	Fjodor	k1gInSc1
Fjodorov	Fjodorov	k1gInSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Charitonov	Charitonovo	k1gNnPc2
•	•	k?
Alexej	Alexej	k1gMnSc1
Jašin	Jašin	k1gMnSc1
•	•	k?
Ilja	Ilja	k1gMnSc1
Kovalčuk	Kovalčuk	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Kovaljov	Kovaljovo	k1gNnPc2
–	–	k?
•	•	k?
Viktor	Viktor	k1gMnSc1
Kozlov	Kozlovo	k1gNnPc2
•	•	k?
Jevgenij	Jevgenij	k1gMnSc1
Malkin	Malkin	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Něprjajev	Něprjajev	k1gMnSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Sjomin	Sjomin	k2eAgMnSc1d1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Ovečkin	Ovečkin	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Zinovjev	Zinovjev	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Vladimir	Vladimir	k1gInSc1
Krikunov	Krikunov	k1gInSc1
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
v	v	k7c6
ledním	lední	k2eAgInSc6d1
hokeji	hokej	k1gInSc6
1996	#num#	k4
–	–	k?
Rusko	Rusko	k1gNnSc1
Brankáři	brankář	k1gMnPc5
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Chabibulin	Chabibulin	k2eAgMnSc1d1
•	•	k?
Michail	Michail	k1gInSc1
Štalenkov	Štalenkov	k1gInSc1
•	•	k?
Andrej	Andrej	k1gMnSc1
Trefilov	Trefilov	k1gInSc4
Obránci	obránce	k1gMnSc3
</s>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1
Fetisov	Fetisovo	k1gNnPc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Gončar	Gončar	k1gMnSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
•	•	k?
Darjus	Darjus	k1gMnSc1
Kasparajtis	Kasparajtis	k1gFnSc2
•	•	k?
Vladimir	Vladimir	k1gInSc1
Malachov	Malachov	k1gInSc1
•	•	k?
Oleg	Oleg	k1gMnSc1
Tverdovskij	Tverdovskij	k1gMnSc1
•	•	k?
Igor	Igor	k1gMnSc1
Uljanov	Uljanovo	k1gNnPc2
•	•	k?
Dmitrij	Dmitrij	k1gMnSc1
Juškjevič	Juškjevič	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Žitnik	Žitnik	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Zubov	Zubov	k1gInSc4
Útočníci	útočník	k1gMnPc1
</s>
<s>
Sergej	Sergej	k1gMnSc1
Berezin	Berezina	k1gFnPc2
•	•	k?
Pavel	Pavel	k1gMnSc1
Bure	Bur	k1gFnSc2
•	•	k?
Valerij	Valerij	k1gMnSc1
Bure	Bur	k1gFnSc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Fjodorov	Fjodorovo	k1gNnPc2
•	•	k?
Andrej	Andrej	k1gMnSc1
Kovalenko	Kovalenka	k1gFnSc5
•	•	k?
Alexej	Alexej	k1gMnSc1
Kovaljov	Kovaljovo	k1gNnPc2
•	•	k?
Vjačeslav	Vjačeslav	k1gMnSc1
Kozlov	Kozlovo	k1gNnPc2
•	•	k?
Igor	Igor	k1gMnSc1
Larionov	Larionovo	k1gNnPc2
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Mogilnyj	Mogilnyj	k1gMnSc1
•	•	k?
Sergej	Sergej	k1gMnSc1
Němčinov	Němčinovo	k1gNnPc2
•	•	k?
Andrej	Andrej	k1gMnSc1
Nikolišin	Nikolišina	k1gFnPc2
•	•	k?
Alexander	Alexandra	k1gFnPc2
Semak	Semak	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Jašin	Jašin	k1gMnSc1
•	•	k?
Valerij	Valerij	k1gMnSc1
Zelepukin	Zelepukin	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Žamnov	Žamnov	k1gInSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Boris	Boris	k1gMnSc1
Michajlov	Michajlovo	k1gNnPc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Makarov	Makarovo	k1gNnPc2
•	•	k?
Jevgenij	Jevgenij	k1gMnSc1
Zimin	Zimin	k1gMnSc1
</s>
<s>
Vítěz	vítěz	k1gMnSc1
–	–	k?
New	New	k1gFnSc1
York	York	k1gInSc1
Rangers	Rangers	k1gInSc1
–	–	k?
NHL	NHL	kA
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
–	–	k?
Stanley	Stanley	k1gInPc1
Cup	cup	k1gInSc1
Brankáři	brankář	k1gMnSc3
</s>
<s>
Glenn	Glenn	k1gMnSc1
Healy	Heala	k1gFnSc2
•	•	k?
Mike	Mik	k1gFnSc2
Richter	Richter	k1gMnSc1
Obránci	obránce	k1gMnSc6
</s>
<s>
Jeff	Jeff	k1gInSc1
Beukeboom	Beukeboom	k1gInSc1
•	•	k?
Alexandr	Alexandr	k1gMnSc1
Karpovcev	Karpovcva	k1gFnPc2
•	•	k?
Brian	Brian	k1gMnSc1
Leetch	Leetch	k1gMnSc1
•	•	k?
Doug	Doug	k1gMnSc1
Lidster	Lidster	k1gMnSc1
•	•	k?
Kevin	Kevin	k1gMnSc1
Lowe	Low	k1gFnSc2
•	•	k?
Sergej	Sergej	k1gMnSc1
Zubov	Zubovo	k1gNnPc2
•	•	k?
Jay	Jay	k1gFnSc3
Wells	Wellsa	k1gFnPc2
Útočníci	útočník	k1gMnPc1
</s>
<s>
Glenn	Glenn	k1gMnSc1
Anderson	Anderson	k1gMnSc1
•	•	k?
Greg	Greg	k1gMnSc1
Gilbert	Gilbert	k1gMnSc1
•	•	k?
Adam	Adam	k1gMnSc1
Graves	Graves	k1gMnSc1
•	•	k?
Mike	Mike	k1gInSc1
Hartman	Hartman	k1gMnSc1
•	•	k?
Mike	Mike	k1gInSc1
Hudson	Hudson	k1gMnSc1
•	•	k?
Joey	Joea	k1gFnSc2
Kocur	Kocur	k1gMnSc1
•	•	k?
Alexej	Alexej	k1gMnSc1
Kovaljov	Kovaljovo	k1gNnPc2
•	•	k?
Nick	Nick	k1gMnSc1
Kypreos	Kypreos	k1gMnSc1
•	•	k?
Steve	Steve	k1gMnSc1
Larmer	Larmer	k1gMnSc1
•	•	k?
Craig	Craig	k1gMnSc1
MacTavish	MacTavish	k1gMnSc1
•	•	k?
Stephane	Stephan	k1gMnSc5
Matteau	Mattea	k1gMnSc3
•	•	k?
Mark	Mark	k1gMnSc1
Messier	Messier	k1gMnSc1
–	–	k?
•	•	k?
Sergej	Sergej	k1gMnSc1
Němčinov	Němčinovo	k1gNnPc2
•	•	k?
Brian	Brian	k1gMnSc1
Noonan	Noonan	k1gMnSc1
•	•	k?
Ed	Ed	k1gMnSc1
Olczyk	Olczyk	k1gMnSc1
•	•	k?
Esa	eso	k1gNnSc2
Tikkanen	Tikkanna	k1gFnPc2
Trenér	trenér	k1gMnSc1
</s>
<s>
Mike	Mike	k1gInSc1
Keenan	Keenany	k1gInPc2
Generální	generální	k2eAgMnSc1d1
manažér	manažér	k1gMnSc1
</s>
<s>
Neil	Neil	k1gMnSc1
Smith	Smith	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lední	lední	k2eAgInSc1d1
hokej	hokej	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
