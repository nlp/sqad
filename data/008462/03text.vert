<p>
<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
mо	mо	k?	mо
nebo	nebo	k8xC	nebo
К	К	k?	К
<g/>
;	;	kIx,	;
persky	persky	k6eAd1	persky
د	د	k?	د
م	م	k?	م
<g/>
;	;	kIx,	;
ázerbájdžánsky	ázerbájdžánsky	k6eAd1	ázerbájdžánsky
Xə	Xə	k1gMnSc2	Xə
də	də	k?	də
<g/>
;	;	kIx,	;
kazašsky	kazašsky	k6eAd1	kazašsky
К	К	k?	К
т	т	k?	т
<g/>
;	;	kIx,	;
turkmensky	turkmensky	k6eAd1	turkmensky
Hazar	Hazar	k1gMnSc1	Hazar
deňizi	deňize	k1gFnSc4	deňize
<g/>
;	;	kIx,	;
řecky	řecky	k6eAd1	řecky
Kaspion	Kaspion	k1gInSc1	Kaspion
pelagos	pelagosa	k1gFnPc2	pelagosa
<g/>
;	;	kIx,	;
latinsky	latinsky	k6eAd1	latinsky
Mare	Mare	k1gFnSc7	Mare
Caspium	Caspium	k1gNnSc4	Caspium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
endorheické	endorheický	k2eAgNnSc1d1	endorheický
jezero	jezero	k1gNnSc1	jezero
rozprostírající	rozprostírající	k2eAgMnSc1d1	rozprostírající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Evropou	Evropa	k1gFnSc7	Evropa
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
ruskou	ruský	k2eAgFnSc7d1	ruská
částí	část	k1gFnSc7	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
březích	břeh	k1gInPc6	břeh
leží	ležet	k5eAaImIp3nS	ležet
5	[number]	k4	5
států	stát	k1gInPc2	stát
<g/>
:	:	kIx,	:
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
Dagestán	Dagestán	k1gInSc1	Dagestán
<g/>
,	,	kIx,	,
Kalmycko	Kalmycko	k1gNnSc1	Kalmycko
a	a	k8xC	a
Astrachaňská	astrachaňský	k2eAgFnSc1d1	astrachaňský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
(	(	kIx(	(
<g/>
Atyrauská	Atyrauský	k2eAgFnSc1d1	Atyrauský
a	a	k8xC	a
Mangystauská	Mangystauský	k2eAgFnSc1d1	Mangystauský
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
(	(	kIx(	(
<g/>
Balkan	Balkan	k1gInSc1	Balkan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Írán	Írán	k1gInSc1	Írán
(	(	kIx(	(
<g/>
Golestán	Golestán	k1gInSc1	Golestán
<g/>
,	,	kIx,	,
Mázandarán	Mázandarán	k1gInSc1	Mázandarán
a	a	k8xC	a
Gílán	Gílán	k1gInSc1	Gílán
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ázerbájdžán	Ázerbájdžán	k1gInSc4	Ázerbájdžán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
376	[number]	k4	376
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
1	[number]	k4	1
200	[number]	k4	200
km	km	kA	km
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
a	a	k8xC	a
průměrně	průměrně	k6eAd1	průměrně
320	[number]	k4	320
km	km	kA	km
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
184	[number]	k4	184
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc1d1	maximální
hloubka	hloubka	k1gFnSc1	hloubka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
1	[number]	k4	1
025	[number]	k4	025
m.	m.	k?	m.
Objem	objem	k1gInSc1	objem
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
78	[number]	k4	78
000	[number]	k4	000
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
povodí	povodí	k1gNnSc2	povodí
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
500	[number]	k4	500
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třetinu	třetina	k1gFnSc4	třetina
činí	činit	k5eAaImIp3nS	činit
povodí	povodí	k1gNnSc1	povodí
Volhy	Volha	k1gFnSc2	Volha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hladina	hladina	k1gFnSc1	hladina
leží	ležet	k5eAaImIp3nS	ležet
28,5	[number]	k4	28,5
m	m	kA	m
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
není	být	k5eNaImIp3nS	být
mořem	moře	k1gNnSc7	moře
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
–	–	k?	–
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
slané	slaný	k2eAgNnSc1d1	slané
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
např.	např.	kA	např.
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
.	.	kIx.	.
</s>
<s>
Geologicky	geologicky	k6eAd1	geologicky
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
část	část	k1gFnSc4	část
někdejšího	někdejší	k2eAgInSc2d1	někdejší
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
oddělena	oddělen	k2eAgFnSc1d1	oddělena
vyzdvižením	vyzdvižení	k1gNnSc7	vyzdvižení
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
nejhlubších	hluboký	k2eAgFnPc2d3	nejhlubší
částí	část	k1gFnPc2	část
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
charakter	charakter	k1gInSc1	charakter
oceánské	oceánský	k2eAgFnSc2d1	oceánská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
běžných	běžný	k2eAgNnPc2d1	běžné
kontinentálních	kontinentální	k2eAgNnPc2d1	kontinentální
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
jezery	jezero	k1gNnPc7	jezero
je	být	k5eAaImIp3nS	být
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
největší	veliký	k2eAgFnSc1d3	veliký
a	a	k8xC	a
nejobjemnější	objemný	k2eAgFnSc1d3	nejobjemnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
větší	veliký	k2eAgFnSc4d2	veliký
plochu	plocha	k1gFnSc4	plocha
a	a	k8xC	a
3,5	[number]	k4	3,5
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc1d2	veliký
objem	objem	k1gInSc1	objem
než	než	k8xS	než
všechna	všechen	k3xTgFnSc1	všechen
americká	americký	k2eAgFnSc1d1	americká
Velká	velká	k1gFnSc1	velká
jezera	jezero	k1gNnSc2	jezero
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
až	až	k9	až
třetí	třetí	k4xOgFnSc4	třetí
nejhlubší	hluboký	k2eAgFnSc4d3	nejhlubší
<g/>
,	,	kIx,	,
po	po	k7c6	po
Bajkalu	Bajkal	k1gInSc6	Bajkal
a	a	k8xC	a
Tanganice	Tanganika	k1gFnSc6	Tanganika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
slanost	slanost	k1gFnSc1	slanost
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
třetinová	třetinový	k2eAgFnSc1d1	třetinová
oproti	oproti	k7c3	oproti
průměru	průměr	k1gInSc3	průměr
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nerovnoměrná	rovnoměrný	k2eNgFnSc1d1	nerovnoměrná
–	–	k?	–
severní	severní	k2eAgInSc4d1	severní
konec	konec	k1gInSc4	konec
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
velkým	velký	k2eAgInPc3d1	velký
přítokům	přítok	k1gInPc3	přítok
téměř	téměř	k6eAd1	téměř
sladkovodní	sladkovodní	k2eAgInSc1d1	sladkovodní
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jihu	jih	k1gInSc3	jih
slanost	slanost	k1gFnSc1	slanost
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
také	také	k9	také
jezerem	jezero	k1gNnSc7	jezero
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
států	stát	k1gInPc2	stát
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
–	–	k?	–
pět	pět	k4xCc4	pět
(	(	kIx(	(
<g/>
před	před	k7c7	před
rozpadem	rozpad	k1gInSc7	rozpad
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
to	ten	k3xDgNnSc4	ten
však	však	k9	však
byly	být	k5eAaImAgInP	být
jen	jen	k6eAd1	jen
dva	dva	k4xCgInPc1	dva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tři	tři	k4xCgFnPc4	tři
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
,	,	kIx,	,
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
<g/>
,	,	kIx,	,
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
"	"	kIx"	"
<g/>
moře	moře	k1gNnSc4	moře
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozený	odvozený	k2eAgInSc1d1	odvozený
od	od	k7c2	od
dávného	dávný	k2eAgInSc2d1	dávný
kmene	kmen	k1gInSc2	kmen
Kaspijců	Kaspijce	k1gMnPc2	Kaspijce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jiná	jiný	k2eAgNnPc1d1	jiné
historická	historický	k2eAgNnPc1d1	historické
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
Hyrkánské	Hyrkánský	k2eAgNnSc4d1	Hyrkánský
<g/>
,	,	kIx,	,
Chvalynské	Chvalynský	k2eAgNnSc4d1	Chvalynský
nebo	nebo	k8xC	nebo
Chazarské	chazarský	k2eAgNnSc4d1	Chazarské
moře	moře	k1gNnSc4	moře
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
historických	historický	k2eAgInPc2d1	historický
národů	národ	k1gInPc2	národ
či	či	k8xC	či
zemí	zem	k1gFnPc2	zem
situovaných	situovaný	k2eAgFnPc2d1	situovaná
kolem	kolem	k7c2	kolem
jeho	jeho	k3xOp3gInPc2	jeho
břehů	břeh	k1gInPc2	břeh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pobřeží	pobřeží	k1gNnSc2	pobřeží
===	===	k?	===
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
7000	[number]	k4	7000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
členité	členitý	k2eAgNnSc1d1	členité
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
zálivy	záliv	k1gInPc1	záliv
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Kizljarský	Kizljarský	k2eAgMnSc1d1	Kizljarský
a	a	k8xC	a
Komsomolec	komsomolec	k1gMnSc1	komsomolec
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Mangyšlakský	Mangyšlakský	k1gMnSc1	Mangyšlakský
<g/>
,	,	kIx,	,
Kenderli	Kenderl	k1gMnPc1	Kenderl
<g/>
,	,	kIx,	,
Kazašský	kazašský	k2eAgInSc1d1	kazašský
<g/>
,	,	kIx,	,
Kara-bogaz-gol	Karaogazol	k1gInSc1	Kara-bogaz-gol
a	a	k8xC	a
Krasnovodský	Krasnovodský	k2eAgInSc1d1	Krasnovodský
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Agrachanský	Agrachanský	k2eAgInSc1d1	Agrachanský
záliv	záliv	k1gInSc1	záliv
a	a	k8xC	a
Bakinská	Bakinský	k2eAgFnSc1d1	Bakinský
zátoka	zátoka	k1gFnSc1	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mělké	mělký	k2eAgFnSc2d1	mělká
laguny	laguna	k1gFnSc2	laguna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Břehy	břeh	k1gInPc1	břeh
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
jsou	být	k5eAaImIp3nP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
táhlé	táhlý	k2eAgNnSc1d1	táhlé
a	a	k8xC	a
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
množstvím	množství	k1gNnSc7	množství
vysušených	vysušený	k2eAgNnPc2d1	vysušené
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
velkými	velký	k2eAgFnPc7d1	velká
větrnými	větrný	k2eAgFnPc7d1	větrná
vlnami	vlna	k1gFnPc7	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k9	také
deltové	deltový	k2eAgInPc1d1	deltový
břehy	břeh	k1gInPc1	břeh
(	(	kIx(	(
<g/>
Volha	Volha	k1gFnSc1	Volha
<g/>
,	,	kIx,	,
Ural	Ural	k1gInSc1	Ural
<g/>
,	,	kIx,	,
Těrek	těrka	k1gFnPc2	těrka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
pobřeží	pobřeží	k1gNnSc1	pobřeží
na	na	k7c6	na
severu	sever	k1gInSc6	sever
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
plochy	plocha	k1gFnSc2	plocha
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsoben	k2eAgNnSc1d1	způsobeno
poklesem	pokles	k1gInSc7	pokles
hladiny	hladina	k1gFnSc2	hladina
a	a	k8xC	a
zvětšováním	zvětšování	k1gNnSc7	zvětšování
delt	delta	k1gFnPc2	delta
přinášeným	přinášený	k2eAgInSc7d1	přinášený
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInPc1d1	západní
břehy	břeh	k1gInPc1	břeh
také	také	k9	také
tvoří	tvořit	k5eAaImIp3nP	tvořit
převážně	převážně	k6eAd1	převážně
usazené	usazený	k2eAgInPc1d1	usazený
materiály	materiál	k1gInPc1	materiál
(	(	kIx(	(
<g/>
přesypy	přesyp	k1gInPc1	přesyp
<g/>
,	,	kIx,	,
kosy	kosa	k1gFnPc1	kosa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oddělené	oddělený	k2eAgInPc1d1	oddělený
úseky	úsek	k1gInPc1	úsek
v	v	k7c6	v
Dagestánu	Dagestán	k1gInSc6	Dagestán
a	a	k8xC	a
na	na	k7c6	na
Apšeronském	Apšeronský	k2eAgInSc6d1	Apšeronský
poloostrově	poloostrov	k1gInSc6	poloostrov
jsou	být	k5eAaImIp3nP	být
tvořené	tvořený	k2eAgInPc1d1	tvořený
rozrušenými	rozrušený	k2eAgFnPc7d1	rozrušená
horninami	hornina	k1gFnPc7	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
převládají	převládat	k5eAaImIp3nP	převládat
rozrušené	rozrušený	k2eAgInPc4d1	rozrušený
břehy	břeh	k1gInPc4	břeh
tvořené	tvořený	k2eAgInPc4d1	tvořený
vápenci	vápenec	k1gInSc3	vápenec
s	s	k7c7	s
přilehlými	přilehlý	k2eAgFnPc7d1	přilehlá
polopustinnými	polopustinný	k2eAgFnPc7d1	polopustinný
a	a	k8xC	a
pustými	pustý	k2eAgFnPc7d1	pustá
planinami	planina	k1gFnPc7	planina
<g/>
.	.	kIx.	.
</s>
<s>
Usazené	usazený	k2eAgInPc1d1	usazený
materiály	materiál	k1gInPc1	materiál
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
však	však	k9	však
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
také	také	k9	také
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
Karabogazskou	Karabogazský	k2eAgFnSc4d1	Karabogazský
kosu	kosa	k1gFnSc4	kosa
(	(	kIx(	(
<g/>
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
moře	moře	k1gNnSc2	moře
záliv	záliv	k1gInSc1	záliv
Kara-bogaz-gol	Karaogazol	k1gInSc1	Kara-bogaz-gol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Krasnovodskou	Krasnovodský	k2eAgFnSc4d1	Krasnovodský
kosu	kosa	k1gFnSc4	kosa
a	a	k8xC	a
kosu	kosa	k1gFnSc4	kosa
Kenderli	Kenderle	k1gFnSc4	Kenderle
a	a	k8xC	a
převládají	převládat	k5eAaImIp3nP	převládat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Krasnovodského	Krasnovodský	k2eAgInSc2d1	Krasnovodský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pět	pět	k4xCc1	pět
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Astrachaňský	astrachaňský	k2eAgInSc1d1	astrachaňský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Volhy	Volha	k1gFnSc2	Volha
a	a	k8xC	a
Dagestánský	dagestánský	k2eAgInSc1d1	dagestánský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Dagestánu	Dagestán	k1gInSc6	Dagestán
v	v	k7c6	v
Kizljarském	Kizljarský	k2eAgInSc6d1	Kizljarský
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
v	v	k7c6	v
Turkmenistánu	Turkmenistán	k1gInSc6	Turkmenistán
pak	pak	k6eAd1	pak
Krasnovodský	Krasnovodský	k2eAgInSc1d1	Krasnovodský
národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Krasnovodský	Krasnovodský	k2eAgInSc4d1	Krasnovodský
záliv	záliv	k1gInSc4	záliv
a	a	k8xC	a
druhá	druhý	k4xOgNnPc4	druhý
pobřeží	pobřeží	k1gNnPc4	pobřeží
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Gasan	Gasan	k1gMnSc1	Gasan
Kuli	kuli	k1gMnSc1	kuli
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
Kyzylagadžský	Kyzylagadžský	k2eAgInSc4d1	Kyzylagadžský
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
a	a	k8xC	a
Şirvanský	Şirvanský	k2eAgInSc4d1	Şirvanský
národní	národní	k2eAgInSc4d1	národní
park	park	k1gInSc4	park
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dno	dno	k1gNnSc1	dno
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
charakteru	charakter	k1gInSc2	charakter
dna	dno	k1gNnSc2	dno
můžeme	moct	k5eAaImIp1nP	moct
jezero	jezero	k1gNnSc4	jezero
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
Severní	severní	k2eAgInSc4d1	severní
Kaspik	Kaspik	k1gInSc4	Kaspik
<g/>
,	,	kIx,	,
Střední	střední	k2eAgInSc4d1	střední
Kaspik	Kaspik	k1gInSc4	Kaspik
a	a	k8xC	a
Jižní	jižní	k2eAgInSc4d1	jižní
Kaspik	Kaspik	k1gInSc4	Kaspik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
Kaspik	Kaspik	k1gInSc1	Kaspik
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejmělčí	mělký	k2eAgFnSc1d3	nejmělčí
část	část	k1gFnSc1	část
celého	celý	k2eAgNnSc2d1	celé
jezera	jezero	k1gNnSc2	jezero
s	s	k7c7	s
hloubkami	hloubka	k1gFnPc7	hloubka
4	[number]	k4	4
až	až	k9	až
8	[number]	k4	8
m.	m.	k?	m.
Představuje	představovat	k5eAaImIp3nS	představovat
okraj	okraj	k1gInSc4	okraj
Přikaspické	Přikaspický	k2eAgFnSc2d1	Přikaspický
prohlubně	prohlubeň	k1gFnSc2	prohlubeň
Východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
je	být	k5eAaImIp3nS	být
tvořené	tvořený	k2eAgNnSc1d1	tvořené
slabě	slabě	k6eAd1	slabě
zvlněnými	zvlněný	k2eAgFnPc7d1	zvlněná
usazenými	usazený	k2eAgFnPc7d1	usazená
horninami	hornina	k1gFnPc7	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
mělčin	mělčina	k1gFnPc2	mělčina
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Mangyšlacký	Mangyšlacký	k2eAgInSc1d1	Mangyšlacký
práh	práh	k1gInSc1	práh
je	být	k5eAaImIp3nS	být
strukturou	struktura	k1gFnSc7	struktura
svázán	svázat	k5eAaPmNgInS	svázat
s	s	k7c7	s
hercynským	hercynský	k2eAgInSc7d1	hercynský
ztraceným	ztracený	k2eAgInSc7d1	ztracený
valem	val	k1gInSc7	val
Karpinského	Karpinský	k2eAgNnSc2d1	Karpinský
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
s	s	k7c7	s
pohořím	pohoří	k1gNnSc7	pohoří
Mangyšlak	Mangyšlak	k1gInSc1	Mangyšlak
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Severní	severní	k2eAgMnSc1d1	severní
Kaspik	Kaspik	k1gMnSc1	Kaspik
od	od	k7c2	od
Středního	střední	k2eAgInSc2d1	střední
Kaspiku	Kaspik	k1gInSc2	Kaspik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Střední	střední	k2eAgInSc1d1	střední
Kaspik	Kaspik	k1gInSc1	Kaspik
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
přibližně	přibližně	k6eAd1	přibližně
138	[number]	k4	138
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
hlubší	hluboký	k2eAgNnSc1d2	hlubší
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dno	dno	k1gNnSc1	dno
má	mít	k5eAaImIp3nS	mít
heterogenní	heterogenní	k2eAgFnSc4d1	heterogenní
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
ponořený	ponořený	k2eAgInSc4d1	ponořený
díl	díl	k1gInSc4	díl
epihercynské	epihercynský	k2eAgFnSc2d1	epihercynský
Turanské	Turanský	k2eAgFnSc2d1	Turanská
desky	deska	k1gFnSc2	deska
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Derbentská	Derbentský	k2eAgFnSc1d1	Derbentský
propadlina	propadlina	k1gFnSc1	propadlina
(	(	kIx(	(
<g/>
hloubka	hloubka	k1gFnSc1	hloubka
788	[number]	k4	788
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
okrajová	okrajový	k2eAgFnSc1d1	okrajová
prohlubeň	prohlubeň	k1gFnSc1	prohlubeň
geosynklinály	geosynklinála	k1gFnSc2	geosynklinála
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
<s>
Šelf	šelf	k1gInSc1	šelf
a	a	k8xC	a
pevninský	pevninský	k2eAgInSc1d1	pevninský
sklon	sklon	k1gInSc1	sklon
je	být	k5eAaImIp3nS	být
komplikovaný	komplikovaný	k2eAgInSc1d1	komplikovaný
podvodními	podvodní	k2eAgInPc7d1	podvodní
sesuvy	sesuv	k1gInPc7	sesuv
<g/>
,	,	kIx,	,
kaňony	kaňon	k1gInPc1	kaňon
a	a	k8xC	a
zbytky	zbytek	k1gInPc1	zbytek
starých	starý	k1gMnPc2	starý
říčních	říční	k2eAgNnPc2d1	říční
údolí	údolí	k1gNnPc2	údolí
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Jižního	jižní	k2eAgInSc2d1	jižní
Kaspiku	Kaspik	k1gInSc2	Kaspik
oddělen	oddělit	k5eAaPmNgInS	oddělit
Apšeronským	Apšeronský	k2eAgInSc7d1	Apšeronský
prahem	práh	k1gInSc7	práh
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
mělčin	mělčina	k1gFnPc2	mělčina
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
novějším	nový	k2eAgInSc7d2	novější
vrásněním	vrásnění	k1gNnSc7	vrásnění
pohoří	pohoří	k1gNnSc4	pohoří
Kavkaz	Kavkaz	k1gInSc1	Kavkaz
a	a	k8xC	a
Kopet	Kopet	k1gInSc1	Kopet
Dag	dag	kA	dag
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
Kaspik	Kaspik	k1gInSc1	Kaspik
(	(	kIx(	(
<g/>
rozloha	rozloha	k1gFnSc1	rozloha
přibližně	přibližně	k6eAd1	přibližně
153	[number]	k4	153
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejhlubší	hluboký	k2eAgFnSc1d3	nejhlubší
(	(	kIx(	(
<g/>
1	[number]	k4	1
025	[number]	k4	025
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
podoceánským	podoceánský	k2eAgNnSc7d1	podoceánský
vytvářením	vytváření	k1gNnSc7	vytváření
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
západního	západní	k2eAgNnSc2d1	západní
a	a	k8xC	a
jižního	jižní	k2eAgNnSc2d1	jižní
(	(	kIx(	(
<g/>
íránského	íránský	k2eAgNnSc2d1	íránské
<g/>
)	)	kIx)	)
pobřeží	pobřeží	k1gNnSc2	pobřeží
má	mít	k5eAaImIp3nS	mít
úzký	úzký	k2eAgInSc1d1	úzký
šelf	šelf	k1gInSc1	šelf
<g/>
,	,	kIx,	,
u	u	k7c2	u
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
šelf	šelf	k1gInSc1	šelf
výrazně	výrazně	k6eAd1	výrazně
širší	široký	k2eAgInSc1d2	širší
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
představuje	představovat	k5eAaImIp3nS	představovat
plochou	plochý	k2eAgFnSc4d1	plochá
hlubinnou	hlubinný	k2eAgFnSc4d1	hlubinná
rovinu	rovina	k1gFnSc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
žulová	žulový	k2eAgFnSc1d1	Žulová
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
tloušťce	tloušťka	k1gFnSc6	tloušťka
až	až	k9	až
25	[number]	k4	25
km	km	kA	km
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
stáří	stáří	k1gNnSc4	stáří
prohlubně	prohlubeň	k1gFnSc2	prohlubeň
Jižního	jižní	k2eAgInSc2d1	jižní
Kaspiku	Kaspik	k1gInSc2	Kaspik
<g/>
)	)	kIx)	)
a	a	k8xC	a
pod	pod	k7c7	pod
ní	on	k3xPp3gFnSc7	on
čedičová	čedičový	k2eAgFnSc1d1	čedičová
o	o	k7c6	o
hloubce	hloubka	k1gFnSc6	hloubka
až	až	k9	až
15	[number]	k4	15
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
podvodních	podvodní	k2eAgInPc2d1	podvodní
hřbetů	hřbet	k1gInPc2	hřbet
<g/>
,	,	kIx,	,
táhnoucích	táhnoucí	k2eAgInPc2d1	táhnoucí
se	se	k3xPyFc4	se
ze	z	k7c2	z
severozápadu	severozápad	k1gInSc2	severozápad
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
.	.	kIx.	.
<g/>
Až	až	k9	až
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
miocénu	miocén	k1gInSc2	miocén
bylo	být	k5eAaImAgNnS	být
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
těsně	těsně	k6eAd1	těsně
spojené	spojený	k2eAgNnSc1d1	spojené
s	s	k7c7	s
Černým	černý	k2eAgNnSc7d1	černé
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
spojení	spojení	k1gNnSc1	spojení
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
a	a	k8xC	a
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
pliocénu	pliocén	k1gInSc6	pliocén
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antropogéně	antropogéna	k1gFnSc6	antropogéna
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
střídání	střídání	k1gNnSc2	střídání
dob	doba	k1gFnPc2	doba
ledových	ledový	k2eAgFnPc2d1	ledová
a	a	k8xC	a
meziledových	meziledový	k2eAgFnPc2d1	meziledová
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
zvedalo	zvedat	k5eAaImAgNnS	zvedat
a	a	k8xC	a
klesalo	klesat	k5eAaImAgNnS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tak	tak	k6eAd1	tak
jezerní	jezerní	k2eAgFnPc1d1	jezerní
terasy	terasa	k1gFnPc1	terasa
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
šelfu	šelf	k1gInSc6	šelf
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
terigenní	terigenní	k2eAgInPc1d1	terigenní
a	a	k8xC	a
mušlovité	mušlovitý	k2eAgInPc1d1	mušlovitý
písky	písek	k1gInPc1	písek
<g/>
,	,	kIx,	,
mušle	mušle	k1gFnPc1	mušle
<g/>
,	,	kIx,	,
oolitové	oolitový	k2eAgInPc1d1	oolitový
písky	písek	k1gInPc1	písek
<g/>
.	.	kIx.	.
</s>
<s>
Hlubinné	hlubinný	k2eAgInPc1d1	hlubinný
úseky	úsek	k1gInPc1	úsek
dna	dno	k1gNnSc2	dno
jsou	být	k5eAaImIp3nP	být
pokryté	pokrytý	k2eAgInPc1d1	pokrytý
jílovitými	jílovitý	k2eAgFnPc7d1	jílovitá
usazeninami	usazenina	k1gFnPc7	usazenina
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
karbidu	karbid	k1gInSc2	karbid
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Dno	dno	k1gNnSc1	dno
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bohatá	bohatý	k2eAgNnPc4d1	bohaté
ložiska	ložisko	k1gNnPc4	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
(	(	kIx(	(
<g/>
Apšeronský	Apšeronský	k2eAgInSc1d1	Apšeronský
práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
dagestánské	dagestánský	k2eAgNnSc4d1	dagestánský
a	a	k8xC	a
turkmenské	turkmenský	k2eAgNnSc4d1	turkmenské
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
perspektivně	perspektivně	k6eAd1	perspektivně
i	i	k9	i
Mangyšlacký	Mangyšlacký	k2eAgInSc1d1	Mangyšlacký
práh	práh	k1gInSc1	práh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Kara-bogaz-gol	Karaogazola	k1gFnPc2	Kara-bogaz-gola
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
naleziště	naleziště	k1gNnPc1	naleziště
síry	síra	k1gFnSc2	síra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrovy	ostrov	k1gInPc1	ostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
50	[number]	k4	50
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
rozloze	rozloha	k1gFnSc6	rozloha
přibližně	přibližně	k6eAd1	přibližně
350	[number]	k4	350
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poloostrovy	poloostrov	k1gInPc4	poloostrov
===	===	k?	===
</s>
</p>
<p>
<s>
Apšeronský	Apšeronský	k2eAgInSc1d1	Apšeronský
poloostrov	poloostrov	k1gInSc1	poloostrov
–	–	k?	–
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
</s>
</p>
<p>
<s>
Agrahaňský	Agrahaňský	k2eAgInSc1d1	Agrahaňský
poloostrov	poloostrov	k1gInSc1	poloostrov
–	–	k?	–
v	v	k7c6	v
Dagestánu	Dagestán	k1gInSc6	Dagestán
</s>
</p>
<p>
<s>
Miyā	Miyā	k?	Miyā
–	–	k?	–
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
</s>
</p>
<p>
<s>
Karabogazská	Karabogazský	k2eAgFnSc1d1	Karabogazský
kosa	kosa	k1gFnSc1	kosa
<g/>
,	,	kIx,	,
Krasnovodská	Krasnovodský	k2eAgFnSc1d1	Krasnovodský
kosa	kosa	k1gFnSc1	kosa
<g/>
,	,	kIx,	,
Kenderli	Kenderle	k1gFnSc4	Kenderle
a	a	k8xC	a
Krasnovodský	Krasnovodský	k2eAgInSc4d1	Krasnovodský
poloostrov	poloostrov	k1gInSc4	poloostrov
–	–	k?	–
v	v	k7c6	v
Turkmenistánu	Turkmenistán	k1gInSc6	Turkmenistán
</s>
</p>
<p>
<s>
Mangyšlak	Mangyšlak	k1gInSc1	Mangyšlak
–	–	k?	–
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
</s>
</p>
<p>
<s>
===	===	k?	===
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
===	===	k?	===
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
řeky	řeka	k1gFnPc1	řeka
ústí	ústit	k5eAaImIp3nP	ústit
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
Volha	Volha	k1gFnSc1	Volha
(	(	kIx(	(
<g/>
zdaleka	zdaleka	k6eAd1	zdaleka
největší	veliký	k2eAgInSc1d3	veliký
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
cca	cca	kA	cca
80	[number]	k4	80
%	%	kIx~	%
všeho	všecek	k3xTgInSc2	všecek
přítoku	přítok	k1gInSc2	přítok
<g/>
)	)	kIx)	)
v	v	k7c6	v
Astrachaňské	astrachaňský	k2eAgFnSc6d1	astrachaňský
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
Těrek	těrka	k1gFnPc2	těrka
na	na	k7c6	na
území	území	k1gNnSc6	území
Dagestánu	Dagestán	k1gInSc2	Dagestán
a	a	k8xC	a
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
řeka	řeka	k1gFnSc1	řeka
Ural	Ural	k1gInSc1	Ural
u	u	k7c2	u
města	město	k1gNnSc2	město
Atyrau	Atyraus	k1gInSc2	Atyraus
a	a	k8xC	a
řeka	řeka	k1gFnSc1	řeka
Emba	Emba	k1gFnSc1	Emba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
občasně	občasně	k6eAd1	občasně
vysychá	vysychat	k5eAaImIp3nS	vysychat
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
přítok	přítok	k1gInSc1	přítok
těchto	tento	k3xDgFnPc2	tento
řek	řeka	k1gFnPc2	řeka
tvoří	tvořit	k5eAaImIp3nS	tvořit
88	[number]	k4	88
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
říčního	říční	k2eAgInSc2d1	říční
přítoku	přítok	k1gInSc2	přítok
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
7	[number]	k4	7
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
západní	západní	k2eAgInPc4d1	západní
přítoky	přítok	k1gInPc4	přítok
Sulak	Sulak	k1gInSc1	Sulak
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Ázerbájdžánu	Ázerbájdžán	k1gInSc2	Ázerbájdžán
a	a	k8xC	a
v	v	k7c6	v
Ázerbájdžánu	Ázerbájdžán	k1gInSc6	Ázerbájdžán
od	od	k7c2	od
severu	sever	k1gInSc2	sever
řeky	řeka	k1gFnSc2	řeka
Samur	Samura	k1gFnPc2	Samura
<g/>
,	,	kIx,	,
Sumgait	Sumgaita	k1gFnPc2	Sumgaita
<g/>
,	,	kIx,	,
Pirsaat	Pirsaat	k1gInSc1	Pirsaat
a	a	k8xC	a
Kura	kura	k1gFnSc1	kura
<g/>
.	.	kIx.	.
</s>
<s>
Zbývajících	zbývající	k2eAgNnPc2d1	zbývající
5	[number]	k4	5
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
řeky	řeka	k1gFnPc1	řeka
íránského	íránský	k2eAgNnSc2d1	íránské
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Kyzyluzen	Kyzyluzen	k2eAgMnSc1d1	Kyzyluzen
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Gílán	Gílána	k1gFnPc2	Gílána
a	a	k8xC	a
v	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
Mázandarán	Mázandarán	k1gInSc1	Mázandarán
a	a	k8xC	a
Golestán	Golestán	k1gInSc1	Golestán
od	od	k7c2	od
západu	západ	k1gInSc2	západ
řeky	řeka	k1gFnSc2	řeka
Rúdháne-je	Rúdháne	k1gFnSc2	Rúdháne-j
Heraz	Heraz	k1gInSc1	Heraz
<g/>
,	,	kIx,	,
Sírín	Sírín	k1gMnSc1	Sírín
Rúd	Rúd	k1gMnSc1	Rúd
<g/>
,	,	kIx,	,
Rúdháne-je	Rúdhánee	k1gFnSc1	Rúdháne-je
Neká	Neká	k1gFnSc1	Neká
a	a	k8xC	a
Rúdháne-je	Rúdhánee	k1gFnSc1	Rúdháne-je
Gorgán	Gorgána	k1gFnPc2	Gorgána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
Turkmenistánu	Turkmenistán	k1gInSc2	Turkmenistán
pak	pak	k6eAd1	pak
řeka	řeka	k1gFnSc1	řeka
Atrek	Atrky	k1gFnPc2	Atrky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
žádné	žádný	k3yNgFnSc2	žádný
řeky	řeka	k1gFnSc2	řeka
nevtékají	vtékat	k5eNaImIp3nP	vtékat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
hladiny	hladina	k1gFnSc2	hladina
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
značnými	značný	k2eAgInPc7d1	značný
dlouhodobými	dlouhodobý	k2eAgInPc7d1	dlouhodobý
výkyvy	výkyv	k1gInPc7	výkyv
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
vodní	vodní	k2eAgFnSc2d1	vodní
bilance	bilance	k1gFnSc2	bilance
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
geologických	geologický	k2eAgInPc2d1	geologický
<g/>
,	,	kIx,	,
archeologických	archeologický	k2eAgInPc2d1	archeologický
<g/>
,	,	kIx,	,
historických	historický	k2eAgInPc2d1	historický
a	a	k8xC	a
geomorfologických	geomorfologický	k2eAgInPc2d1	geomorfologický
pramenů	pramen	k1gInPc2	pramen
byla	být	k5eAaImAgFnS	být
hladina	hladina	k1gFnSc1	hladina
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
m	m	kA	m
<g/>
)	)	kIx)	)
před	před	k7c7	před
4	[number]	k4	4
až	až	k8xS	až
6	[number]	k4	6
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
novokaspická	novokaspický	k2eAgFnSc1d1	novokaspický
transgrese	transgrese	k1gFnSc1	transgrese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
že	že	k8xS	že
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
úroveň	úroveň	k1gFnSc1	úroveň
hladiny	hladina	k1gFnSc2	hladina
nízká	nízký	k2eAgFnSc1d1	nízká
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
m	m	kA	m
pod	pod	k7c7	pod
současnou	současný	k2eAgFnSc7d1	současná
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r	r	kA	r
1929	[number]	k4	1929
před	před	k7c7	před
znatelným	znatelný	k2eAgInSc7d1	znatelný
poklesem	pokles	k1gInSc7	pokles
hladiny	hladina	k1gFnSc2	hladina
byla	být	k5eAaImAgFnS	být
rozloha	rozloha	k1gFnSc1	rozloha
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
422	[number]	k4	422
000	[number]	k4	000
km2	km2	k4	km2
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovalo	následovat	k5eAaImAgNnS	následovat
snižování	snižování	k1gNnSc1	snižování
hladiny	hladina	k1gFnSc2	hladina
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
kolísá	kolísat	k5eAaImIp3nS	kolísat
hladina	hladina	k1gFnSc1	hladina
okolo	okolo	k7c2	okolo
-28,5	-28,5	k4	-28,5
m.	m.	k?	m.
Příčinou	příčina	k1gFnSc7	příčina
posledního	poslední	k2eAgInSc2d1	poslední
poklesu	pokles	k1gInSc2	pokles
bylo	být	k5eAaImAgNnS	být
kromě	kromě	k7c2	kromě
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
snížení	snížení	k1gNnSc1	snížení
průtoku	průtok	k1gInSc2	průtok
přitékajících	přitékající	k2eAgFnPc2d1	přitékající
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
zvětšení	zvětšení	k1gNnSc1	zvětšení
odparu	odpar	k1gInSc2	odpar
také	také	k9	také
vybudování	vybudování	k1gNnSc4	vybudování
vodních	vodní	k2eAgFnPc2d1	vodní
nádrží	nádrž	k1gFnPc2	nádrž
na	na	k7c6	na
Volze	Volha	k1gFnSc6	Volha
<g/>
,	,	kIx,	,
využívání	využívání	k1gNnSc6	využívání
řek	řeka	k1gFnPc2	řeka
pro	pro	k7c4	pro
zavlažování	zavlažování	k1gNnSc4	zavlažování
a	a	k8xC	a
odběr	odběr	k1gInSc4	odběr
vody	voda	k1gFnSc2	voda
pro	pro	k7c4	pro
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Negativně	negativně	k6eAd1	negativně
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
také	také	k9	také
odtok	odtok	k1gInSc1	odtok
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
Kara-bogaz-gol	Karaogazola	k1gFnPc2	Kara-bogaz-gola
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hladina	hladina	k1gFnSc1	hladina
leží	ležet	k5eAaImIp3nS	ležet
asi	asi	k9	asi
o	o	k7c4	o
4	[number]	k4	4
m	m	kA	m
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
vlastní	vlastní	k2eAgNnSc4d1	vlastní
Kaspické	kaspický	k2eAgNnSc4d1	Kaspické
moře	moře	k1gNnSc4	moře
(	(	kIx(	(
<g/>
technicky	technicky	k6eAd1	technicky
vzato	vzat	k2eAgNnSc1d1	vzato
se	se	k3xPyFc4	se
tak	tak	k9	tak
vlastně	vlastně	k9	vlastně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
samostatné	samostatný	k2eAgNnSc4d1	samostatné
jezero	jezero	k1gNnSc4	jezero
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
"	"	kIx"	"
<g/>
stéká	stékat	k5eAaImIp3nS	stékat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
vodním	vodní	k2eAgInSc6d1	vodní
příjmu	příjem	k1gInSc6	příjem
v	v	k7c6	v
r	r	kA	r
1970	[number]	k4	1970
se	se	k3xPyFc4	se
podílely	podílet	k5eAaImAgFnP	podílet
srážky	srážka	k1gFnPc1	srážka
66,8	[number]	k4	66,8
km3	km3	k4	km3
<g/>
,	,	kIx,	,
přitékající	přitékající	k2eAgFnSc2d1	přitékající
řeky	řeka	k1gFnSc2	řeka
266,4	[number]	k4	266,4
km3	km3	k4	km3
a	a	k8xC	a
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
5	[number]	k4	5
km3	km3	k4	km3
a	a	k8xC	a
na	na	k7c6	na
úbytku	úbytek	k1gInSc6	úbytek
vypařování	vypařování	k1gNnPc2	vypařování
357,3	[number]	k4	357,3
km3	km3	k4	km3
<g/>
,	,	kIx,	,
odtok	odtok	k1gInSc4	odtok
do	do	k7c2	do
Kara-Bogaz-Gol	Kara-Bogaz-Gola	k1gFnPc2	Kara-Bogaz-Gola
4	[number]	k4	4
km3	km3	k4	km3
a	a	k8xC	a
čerpání	čerpání	k1gNnSc1	čerpání
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
1	[number]	k4	1
km3	km3	k4	km3
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
způsobil	způsobit	k5eAaPmAgInS	způsobit
roční	roční	k2eAgInSc4d1	roční
pokles	pokles	k1gInSc4	pokles
hladiny	hladina	k1gFnSc2	hladina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
tlakové	tlakový	k2eAgFnPc1d1	tlaková
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
určují	určovat	k5eAaImIp3nP	určovat
cirkulaci	cirkulace	k1gFnSc4	cirkulace
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
výběžek	výběžek	k1gInSc4	výběžek
asijské	asijský	k2eAgFnSc2d1	asijská
výše	výše	k1gFnSc2	výše
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
azorské	azorský	k2eAgFnSc2d1	Azorská
výše	výše	k1gFnSc2	výše
a	a	k8xC	a
jihoasijské	jihoasijský	k2eAgFnSc2d1	jihoasijská
níže	níže	k1gFnSc2	níže
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
s	s	k7c7	s
převládajícími	převládající	k2eAgFnPc7d1	převládající
anticyklonovými	anticyklonův	k2eAgFnPc7d1	anticyklonův
podmínkami	podmínka	k1gFnPc7	podmínka
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
suchými	suchý	k2eAgInPc7d1	suchý
větry	vítr	k1gInPc7	vítr
<g/>
,	,	kIx,	,
tuhou	tuha	k1gFnSc7	tuha
zimou	zima	k1gFnSc7	zima
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velkými	velký	k2eAgInPc7d1	velký
výkyvy	výkyv	k1gInPc7	výkyv
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
a	a	k8xC	a
malými	malý	k2eAgFnPc7d1	malá
srážkami	srážka	k1gFnPc7	srážka
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
a	a	k8xC	a
středních	střední	k2eAgFnPc6d1	střední
částech	část	k1gFnPc6	část
jezera	jezero	k1gNnSc2	jezero
převládají	převládat	k5eAaImIp3nP	převládat
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
větry	vítr	k1gInPc1	vítr
východních	východní	k2eAgInPc2d1	východní
směrů	směr	k1gInPc2	směr
a	a	k8xC	a
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
severozápadních	severozápadní	k2eAgInPc2d1	severozápadní
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
mají	mít	k5eAaImIp3nP	mít
větry	vítr	k1gInPc1	vítr
převážně	převážně	k6eAd1	převážně
monzunový	monzunový	k2eAgInSc4d1	monzunový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnějšími	silný	k2eAgInPc7d3	nejsilnější
větry	vítr	k1gInPc7	vítr
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
oblast	oblast	k1gFnSc1	oblast
Apšeronského	Apšeronský	k2eAgInSc2d1	Apšeronský
poloostrova	poloostrov	k1gInSc2	poloostrov
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
bakinský	bakinský	k2eAgInSc1d1	bakinský
severák	severák	k1gInSc1	severák
<g/>
"	"	kIx"	"
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
střední	střední	k2eAgFnSc2d1	střední
části	část	k1gFnSc2	část
a	a	k8xC	a
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
díle	dílo	k1gNnSc6	dílo
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc4d1	častá
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgInPc6	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
24	[number]	k4	24
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
celým	celý	k2eAgNnSc7d1	celé
jezerem	jezero	k1gNnSc7	jezero
stejná	stejná	k1gFnSc1	stejná
24	[number]	k4	24
až	až	k9	až
26	[number]	k4	26
°	°	k?	°
<g/>
C.	C.	kA	C.
Maximum	maximum	k1gNnSc1	maximum
bylo	být	k5eAaImAgNnS	být
naměřeno	naměřen	k2eAgNnSc1d1	naměřeno
nad	nad	k7c7	nad
východním	východní	k2eAgNnSc7d1	východní
pobřežím	pobřeží	k1gNnSc7	pobřeží
(	(	kIx(	(
<g/>
44	[number]	k4	44
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
různá	různý	k2eAgFnSc1d1	různá
od	od	k7c2	od
-10	-10	k4	-10
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
severu	sever	k1gInSc6	sever
do	do	k7c2	do
12	[number]	k4	12
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
jezerem	jezero	k1gNnSc7	jezero
spadne	spadnout	k5eAaPmIp3nS	spadnout
průměrně	průměrně	k6eAd1	průměrně
200	[number]	k4	200
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
400	[number]	k4	400
mm	mm	kA	mm
<g/>
,	,	kIx,	,
na	na	k7c6	na
suchém	suchý	k2eAgNnSc6d1	suché
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
90	[number]	k4	90
až	až	k9	až
100	[number]	k4	100
mm	mm	kA	mm
a	a	k8xC	a
na	na	k7c6	na
subtropickém	subtropický	k2eAgInSc6d1	subtropický
jihozápadě	jihozápad	k1gInSc6	jihozápad
až	až	k9	až
1	[number]	k4	1
700	[number]	k4	700
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vypařování	vypařování	k1gNnSc1	vypařování
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
moře	moře	k1gNnSc2	moře
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
velké	velký	k2eAgInPc4d1	velký
až	až	k9	až
1	[number]	k4	1
000	[number]	k4	000
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c6	na
Apšeronském	Apšeronský	k2eAgInSc6d1	Apšeronský
poloostrově	poloostrov	k1gInSc6	poloostrov
až	až	k9	až
1	[number]	k4	1
400	[number]	k4	400
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
vody	voda	k1gFnSc2	voda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kaspickém	kaspický	k2eAgNnSc6d1	Kaspické
moři	moře	k1gNnSc6	moře
panuje	panovat	k5eAaImIp3nS	panovat
cyklonální	cyklonální	k2eAgFnSc1d1	cyklonální
cirkulace	cirkulace	k1gFnSc1	cirkulace
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
přitékajících	přitékající	k2eAgFnPc2d1	přitékající
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
převládajících	převládající	k2eAgInPc2d1	převládající
větrů	vítr	k1gInPc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
proudí	proudit	k5eAaImIp3nS	proudit
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
podél	podél	k7c2	podél
západního	západní	k2eAgInSc2d1	západní
břehu	břeh	k1gInSc2	břeh
k	k	k7c3	k
Apšeronskému	Apšeronský	k2eAgInSc3d1	Apšeronský
poloostrovu	poloostrov	k1gInSc3	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
proud	proud	k1gInSc1	proud
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
podél	podél	k7c2	podél
břehu	břeh	k1gInSc2	břeh
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
protíná	protínat	k5eAaImIp3nS	protínat
jezero	jezero	k1gNnSc1	jezero
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
proudem	proud	k1gInSc7	proud
směřujícím	směřující	k2eAgInSc7d1	směřující
na	na	k7c4	na
sever	sever	k1gInSc4	sever
podél	podél	k7c2	podél
východního	východní	k2eAgInSc2d1	východní
břehu	břeh	k1gInSc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Kaspiku	Kaspikum	k1gNnSc6	Kaspikum
se	se	k3xPyFc4	se
také	také	k9	také
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
cyklonální	cyklonální	k2eAgFnSc1d1	cyklonální
cirkulace	cirkulace	k1gFnSc1	cirkulace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Baku	Baku	k1gNnSc7	Baku
a	a	k8xC	a
ústím	ústí	k1gNnSc7	ústí
Kury	kura	k1gFnSc2	kura
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
doplněna	doplnit	k5eAaPmNgFnS	doplnit
místní	místní	k2eAgFnSc7d1	místní
anticyklonální	anticyklonální	k2eAgFnSc7d1	anticyklonální
cirkulací	cirkulace	k1gFnSc7	cirkulace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Kaspiku	Kaspikum	k1gNnSc6	Kaspikum
převládají	převládat	k5eAaImIp3nP	převládat
nestálé	stálý	k2eNgFnPc1d1	nestálá
větrem	vítr	k1gInSc7	vítr
hnané	hnaný	k2eAgInPc4d1	hnaný
proudy	proud	k1gInPc4	proud
rozličných	rozličný	k2eAgInPc2d1	rozličný
směrů	směr	k1gInPc2	směr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
cm	cm	kA	cm
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
při	při	k7c6	při
silných	silný	k2eAgInPc6d1	silný
větrech	vítr	k1gInPc6	vítr
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
proudu	proud	k1gInSc2	proud
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
30	[number]	k4	30
<g/>
,	,	kIx,	,
40	[number]	k4	40
až	až	k9	až
100	[number]	k4	100
cm	cm	kA	cm
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Časté	častý	k2eAgNnSc1d1	časté
střídání	střídání	k1gNnSc1	střídání
klidu	klid	k1gInSc2	klid
a	a	k8xC	a
silného	silný	k2eAgInSc2d1	silný
větru	vítr	k1gInSc2	vítr
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
dní	den	k1gInPc2	den
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
vlněním	vlnění	k1gNnSc7	vlnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
vlny	vlna	k1gFnPc1	vlna
až	až	k9	až
11	[number]	k4	11
m	m	kA	m
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
Apšeronského	Apšeronský	k2eAgInSc2d1	Apšeronský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
u	u	k7c2	u
hladiny	hladina	k1gFnSc2	hladina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
24	[number]	k4	24
až	až	k9	až
26	[number]	k4	26
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
29	[number]	k4	29
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
,	,	kIx,	,
v	v	k7c6	v
Krasnovodském	Krasnovodský	k2eAgInSc6d1	Krasnovodský
zálivu	záliv	k1gInSc6	záliv
až	až	k9	až
32	[number]	k4	32
°	°	k?	°
<g/>
C.	C.	kA	C.
U	u	k7c2	u
východních	východní	k2eAgInPc2d1	východní
břehů	břeh	k1gInPc2	břeh
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
teplota	teplota	k1gFnSc1	teplota
snižuje	snižovat	k5eAaImIp3nS	snižovat
na	na	k7c4	na
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
°	°	k?	°
<g/>
C.	C.	kA	C.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsoben	k2eAgNnSc4d1	způsobeno
větrem	vítr	k1gInSc7	vítr
a	a	k8xC	a
stoupáním	stoupání	k1gNnSc7	stoupání
hlubinných	hlubinný	k2eAgFnPc2d1	hlubinná
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc1	teplota
více	hodně	k6eAd2	hodně
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
do	do	k7c2	do
0,5	[number]	k4	0,5
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
až	až	k9	až
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
jezero	jezero	k1gNnSc4	jezero
zamrzá	zamrzat	k5eAaImIp3nS	zamrzat
<g/>
.	.	kIx.	.
</s>
<s>
Tloušťka	tloušťka	k1gFnSc1	tloušťka
ledu	led	k1gInSc2	led
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
2	[number]	k4	2
m.	m.	k?	m.
Ve	v	k7c6	v
Středním	střední	k2eAgInSc6d1	střední
Kaspiku	Kaspik	k1gInSc6	Kaspik
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
teplota	teplota	k1gFnSc1	teplota
3	[number]	k4	3
až	až	k9	až
7	[number]	k4	7
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
zamrzají	zamrzat	k5eAaImIp3nP	zamrzat
zde	zde	k6eAd1	zde
mělké	mělký	k2eAgInPc1d1	mělký
zálivy	záliv	k1gInPc1	záliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižním	jižní	k2eAgNnSc6d1	jižní
Kaspiku	Kaspikum	k1gNnSc6	Kaspikum
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Nezřídka	nezřídka	k6eAd1	nezřídka
se	se	k3xPyFc4	se
ledy	led	k1gInPc1	led
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
dostávají	dostávat	k5eAaImIp3nP	dostávat
vlivem	vlivem	k7c2	vlivem
větru	vítr	k1gInSc2	vítr
podél	podél	k7c2	podél
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
ledy	led	k1gInPc1	led
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k6eAd1	až
Apšeronského	Apšeronský	k2eAgInSc2d1	Apšeronský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
zde	zde	k6eAd1	zde
škodu	škoda	k1gFnSc4	škoda
na	na	k7c6	na
hydroenergetických	hydroenergetický	k2eAgFnPc6d1	hydroenergetická
stavbách	stavba	k1gFnPc6	stavba
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
slanost	slanost	k1gFnSc1	slanost
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
12,7	[number]	k4	12,7
až	až	k9	až
12,8	[number]	k4	12,8
‰	‰	k?	‰
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Kara-bogaz-gol	Karaogazola	k1gFnPc2	Kara-bogaz-gola
<g/>
)	)	kIx)	)
u	u	k7c2	u
východního	východní	k2eAgInSc2d1	východní
břehu	břeh	k1gInSc2	břeh
13,2	[number]	k4	13,2
‰	‰	k?	‰
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgMnSc1d1	minimální
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
1	[number]	k4	1
až	až	k9	až
2	[number]	k4	2
‰	‰	k?	‰
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
soli	sůl	k1gFnSc2	sůl
se	se	k3xPyFc4	se
od	od	k7c2	od
oceánu	oceán	k1gInSc2	oceán
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
sulfátů	sulfát	k1gInPc2	sulfát
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
,	,	kIx,	,
hořčíku	hořčík	k1gInSc2	hořčík
a	a	k8xC	a
menším	malý	k2eAgInSc7d2	menší
podílem	podíl	k1gInSc7	podíl
chloridů	chlorid	k1gInPc2	chlorid
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
charakterem	charakter	k1gInSc7	charakter
přitékající	přitékající	k2eAgFnSc2d1	přitékající
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vertikální	vertikální	k2eAgNnSc1d1	vertikální
promíchávání	promíchávání	k1gNnSc1	promíchávání
vod	voda	k1gFnPc2	voda
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
celé	celý	k2eAgFnSc2d1	celá
hloubky	hloubka	k1gFnSc2	hloubka
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
200	[number]	k4	200
až	až	k9	až
300	[number]	k4	300
m	m	kA	m
hluboké	hluboký	k2eAgFnSc2d1	hluboká
vrstvy	vrstva	k1gFnSc2	vrstva
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
pak	pak	k6eAd1	pak
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
hluboké	hluboký	k2eAgFnSc6d1	hluboká
15	[number]	k4	15
až	až	k9	až
30	[number]	k4	30
m.	m.	k?	m.
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
hranici	hranice	k1gFnSc6	hranice
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
teplotní	teplotní	k2eAgInSc4d1	teplotní
skok	skok	k1gInSc4	skok
až	až	k9	až
o	o	k7c4	o
několik	několik	k4yIc4	několik
stupňů	stupeň	k1gInPc2	stupeň
na	na	k7c4	na
metr	metr	k1gInSc4	metr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
brání	bránit	k5eAaImIp3nS	bránit
průniku	průnik	k1gInSc2	průnik
tepla	teplo	k1gNnSc2	teplo
do	do	k7c2	do
větších	veliký	k2eAgFnPc2d2	veliký
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
kolísání	kolísání	k1gNnSc1	kolísání
hladiny	hladina	k1gFnSc2	hladina
až	až	k9	až
o	o	k7c4	o
2	[number]	k4	2
m	m	kA	m
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
větry	vítr	k1gInPc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
také	také	k9	také
dlouhoperiodické	dlouhoperiodický	k2eAgFnPc1d1	dlouhoperiodická
vlny	vlna	k1gFnPc1	vlna
s	s	k7c7	s
periodami	perioda	k1gFnPc7	perioda
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
až	až	k6eAd1	až
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
s	s	k7c7	s
amplitudou	amplituda	k1gFnSc7	amplituda
do	do	k7c2	do
70	[number]	k4	70
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
hladiny	hladina	k1gFnSc2	hladina
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
30	[number]	k4	30
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
a	a	k8xC	a
flóra	flóra	k1gFnSc1	flóra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
jezeře	jezero	k1gNnSc6	jezero
nežije	žít	k5eNaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tam	tam	k6eAd1	tam
žijí	žít	k5eAaImIp3nP	žít
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
500	[number]	k4	500
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
824	[number]	k4	824
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
rostlin	rostlina	k1gFnPc2	rostlina
převládají	převládat	k5eAaImIp3nP	převládat
sinice	sinice	k1gFnPc1	sinice
a	a	k8xC	a
rozsivky	rozsivka	k1gFnPc1	rozsivka
(	(	kIx(	(
<g/>
rhizosolenia	rhizosolenium	k1gNnPc1	rhizosolenium
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
mnoho	mnoho	k4c1	mnoho
červených	červený	k2eAgFnPc2d1	červená
a	a	k8xC	a
hnědých	hnědý	k2eAgFnPc2d1	hnědá
vodních	vodní	k2eAgFnPc2d1	vodní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kvetoucích	kvetoucí	k2eAgMnPc2d1	kvetoucí
jsou	být	k5eAaImIp3nP	být
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
vocha	vocha	k1gFnSc1	vocha
mořská	mořský	k2eAgFnSc1d1	mořská
a	a	k8xC	a
táhlice	táhlice	k1gFnSc1	táhlice
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
rostlinnou	rostlinný	k2eAgFnSc4d1	rostlinná
masu	masa	k1gFnSc4	masa
tvoří	tvořit	k5eAaImIp3nP	tvořit
parožnatky	parožnatka	k1gFnPc1	parožnatka
(	(	kIx(	(
<g/>
do	do	k7c2	do
30	[number]	k4	30
kg	kg	kA	kg
na	na	k7c4	na
1	[number]	k4	1
m2	m2	k4	m2
dna	dna	k1gFnSc1	dna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
původ	původ	k1gInSc4	původ
v	v	k7c6	v
neogénu	neogén	k1gInSc6	neogén
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
častých	častý	k2eAgFnPc2d1	častá
změn	změna	k1gFnPc2	změna
slanosti	slanost	k1gFnSc2	slanost
prošli	projít	k5eAaPmAgMnP	projít
značnými	značný	k2eAgFnPc7d1	značná
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jeseteři	jeseter	k1gMnPc1	jeseter
<g/>
,	,	kIx,	,
sledi	sleď	k1gMnPc1	sleď
<g/>
,	,	kIx,	,
kilky	kilka	k1gFnPc1	kilka
<g/>
,	,	kIx,	,
hlaváči	hlaváč	k1gMnPc1	hlaváč
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
druhu	druh	k1gInSc2	druh
benthophilus	benthophilus	k1gInSc1	benthophilus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
měkkýšů	měkkýš	k1gMnPc2	měkkýš
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
slávičkovití	slávičkovitý	k2eAgMnPc1d1	slávičkovitý
a	a	k8xC	a
srdcovkovití	srdcovkovitý	k2eAgMnPc1d1	srdcovkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
pak	pak	k6eAd1	pak
zástupci	zástupce	k1gMnPc1	zástupce
pontogammarus	pontogammarus	k1gMnSc1	pontogammarus
<g/>
,	,	kIx,	,
mnohoštětinatci	mnohoštětinatec	k1gMnPc1	mnohoštětinatec
<g/>
,	,	kIx,	,
houby	houby	k6eAd1	houby
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
medúz	medúza	k1gFnPc2	medúza
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
15	[number]	k4	15
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
dostaly	dostat	k5eAaPmAgFnP	dostat
z	z	k7c2	z
arktických	arktický	k2eAgNnPc2d1	arktické
a	a	k8xC	a
středozemních	středozemní	k2eAgNnPc2d1	středozemní
povodí	povodí	k1gNnPc2	povodí
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
živočichové	živočich	k1gMnPc1	živočich
sladkovodního	sladkovodní	k2eAgInSc2d1	sladkovodní
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
candát	candát	k1gMnSc1	candát
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k4c1	mnoho
endemických	endemický	k2eAgInPc2d1	endemický
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
přistěhovaly	přistěhovat	k5eAaPmAgInP	přistěhovat
do	do	k7c2	do
Kaspiku	Kaspik	k1gInSc2	Kaspik
poměrně	poměrně	k6eAd1	poměrně
nedávno	nedávno	k6eAd1	nedávno
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
na	na	k7c6	na
trupech	trup	k1gInPc6	trup
lodí	loď	k1gFnPc2	loď
(	(	kIx(	(
<g/>
slávky	slávka	k1gFnPc4	slávka
<g/>
,	,	kIx,	,
rhizosolenia	rhizosolenium	k1gNnPc4	rhizosolenium
<g/>
,	,	kIx,	,
svijonožci	svijonožec	k1gMnSc3	svijonožec
a	a	k8xC	a
také	také	k9	také
krabi	krab	k1gMnPc1	krab
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
cípal	cípal	k1gMnSc1	cípal
hlavatý	hlavatý	k2eAgMnSc1d1	hlavatý
<g/>
,	,	kIx,	,
nereidky	nereidka	k1gFnPc1	nereidka
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
syndesmya	syndesmya	k6eAd1	syndesmya
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
národní	národní	k2eAgFnSc4d1	národní
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
náklady	náklad	k1gInPc1	náklad
převážené	převážený	k2eAgInPc1d1	převážený
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
představují	představovat	k5eAaImIp3nP	představovat
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
bavlna	bavlna	k1gFnSc1	bavlna
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
a	a	k8xC	a
síra	síra	k1gFnSc1	síra
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
přístavy	přístav	k1gInPc1	přístav
jsou	být	k5eAaImIp3nP	být
Astrachaň	Astrachaň	k1gFnSc4	Astrachaň
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc4	Baku
<g/>
,	,	kIx,	,
Machačkala	Machačkala	k1gFnSc4	Machačkala
<g/>
,	,	kIx,	,
Krasnovodsk	Krasnovodsk	k1gInSc4	Krasnovodsk
a	a	k8xC	a
Aktau	Aktaa	k1gFnSc4	Aktaa
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
spojené	spojený	k2eAgInPc1d1	spojený
osobní	osobní	k2eAgFnSc7d1	osobní
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Baku	Baku	k1gNnSc7	Baku
a	a	k8xC	a
Krasnovodskem	Krasnovodsko	k1gNnSc7	Krasnovodsko
funguje	fungovat	k5eAaImIp3nS	fungovat
železniční	železniční	k2eAgInSc1d1	železniční
trajekt	trajekt	k1gInSc1	trajekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgInPc1d3	veliký
přístavy	přístav	k1gInPc1	přístav
Bandar-e	Bandar-	k1gFnSc2	Bandar-
Anzalī	Anzalī	k1gFnSc2	Anzalī
a	a	k8xC	a
Bender-Šách	Bender-Šy	k1gInPc6	Bender-Šy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Zdokumentovaná	zdokumentovaný	k2eAgNnPc4d1	zdokumentované
svědectví	svědectví	k1gNnPc4	svědectví
o	o	k7c6	o
znalostech	znalost	k1gFnPc6	znalost
Kaspiku	Kaspik	k1gInSc2	Kaspik
a	a	k8xC	a
plavbách	plavba	k1gFnPc6	plavba
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
9	[number]	k4	9
<g/>
.	.	kIx.	.
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
arabské	arabský	k2eAgInPc4d1	arabský
<g/>
,	,	kIx,	,
arménské	arménský	k2eAgInPc4d1	arménský
a	a	k8xC	a
perské	perský	k2eAgInPc4d1	perský
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
sledování	sledování	k1gNnSc1	sledování
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
za	za	k7c4	za
Petra	Petr	k1gMnSc4	Petr
Velikého	veliký	k2eAgMnSc4d1	veliký
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
letech	let	k1gInPc6	let
1714	[number]	k4	1714
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
inicioval	iniciovat	k5eAaBmAgMnS	iniciovat
expedici	expedice	k1gFnSc4	expedice
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
A.	A.	kA	A.
Bekoviče-Čerkasského	Bekoviče-Čerkasského	k2eAgFnSc1d1	Bekoviče-Čerkasského
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
prozkoumala	prozkoumat	k5eAaPmAgFnS	prozkoumat
východní	východní	k2eAgInPc4d1	východní
břehy	břeh	k1gInPc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zahájil	zahájit	k5eAaPmAgMnS	zahájit
hydrografické	hydrografický	k2eAgInPc4d1	hydrografický
průzkumy	průzkum	k1gInPc4	průzkum
moře	moře	k1gNnSc2	moře
I.	I.	kA	I.
F.	F.	kA	F.
Sojmonov	Sojmonov	k1gInSc4	Sojmonov
a	a	k8xC	a
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
I.	I.	kA	I.
V.	V.	kA	V.
Tokmačjov	Tokmačjov	k1gInSc4	Tokmačjov
a	a	k8xC	a
M.	M.	kA	M.
I.	I.	kA	I.
Vojnovič	Vojnovič	k1gMnSc1	Vojnovič
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
provedl	provést	k5eAaPmAgMnS	provést
Kolodkin	Kolodkin	k1gMnSc1	Kolodkin
první	první	k4xOgNnSc4	první
detailní	detailní	k2eAgNnSc4d1	detailní
zaměření	zaměření	k1gNnSc4	zaměření
břehů	břeh	k1gInPc2	břeh
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
díle	dílo	k1gNnSc6	dílo
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
N.	N.	kA	N.
A.	A.	kA	A.
Ivašincev	Ivašincva	k1gFnPc2	Ivašincva
<g/>
.	.	kIx.	.
</s>
<s>
Mapy	mapa	k1gFnPc1	mapa
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
měření	měření	k1gNnSc1	měření
sloužily	sloužit	k5eAaImAgInP	sloužit
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
jezera	jezero	k1gNnSc2	jezero
přinesli	přinést	k5eAaPmAgMnP	přinést
velký	velký	k2eAgInSc4d1	velký
vklad	vklad	k1gInSc4	vklad
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
vědci	vědec	k1gMnPc1	vědec
P.	P.	kA	P.
S.	S.	kA	S.
Pallas	Pallas	k1gMnSc1	Pallas
<g/>
,	,	kIx,	,
S.	S.	kA	S.
G.	G.	kA	G.
Gmelin	Gmelina	k1gFnPc2	Gmelina
<g/>
,	,	kIx,	,
G.	G.	kA	G.
S.	S.	kA	S.
Karelin	Karelina	k1gFnPc2	Karelina
<g/>
,	,	kIx,	,
K.	K.	kA	K.
M.	M.	kA	M.
Ber	brát	k5eAaImRp2nS	brát
<g/>
,	,	kIx,	,
G.	G.	kA	G.
V.	V.	kA	V.
Abich	Abich	k1gMnSc1	Abich
<g/>
,	,	kIx,	,
O.	O.	kA	O.
A.	A.	kA	A.
Grim	Grim	k1gMnSc1	Grim
<g/>
,	,	kIx,	,
N.	N.	kA	N.
I.	I.	kA	I.
Andrusov	Andrusov	k1gInSc4	Andrusov
a	a	k8xC	a
I.	I.	kA	I.
B.	B.	kA	B.
Špindler	Špindler	k1gMnSc1	Špindler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Astrachaňská	astrachaňský	k2eAgFnSc1d1	astrachaňský
vědecko-výzkumná	vědeckoýzkumný	k2eAgFnSc1d1	vědecko-výzkumná
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
hydrologické	hydrologický	k2eAgFnPc1d1	hydrologická
a	a	k8xC	a
hydrobiologické	hydrobiologický	k2eAgFnPc1d1	hydrobiologická
expedice	expedice	k1gFnPc1	expedice
N.	N.	kA	N.
M.	M.	kA	M.
Knipoviče	Knipovič	k1gMnSc2	Knipovič
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
i	i	k9	i
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
významu	význam	k1gInSc2	význam
ropná	ropný	k2eAgNnPc1d1	ropné
naleziště	naleziště	k1gNnPc1	naleziště
na	na	k7c6	na
Apšeronském	Apšeronský	k2eAgInSc6d1	Apšeronský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
prozkoumali	prozkoumat	k5eAaPmAgMnP	prozkoumat
geologové	geolog	k1gMnPc1	geolog
I.	I.	kA	I.
M.	M.	kA	M.
Gubkin	Gubkin	k1gMnSc1	Gubkin
<g/>
,	,	kIx,	,
D.	D.	kA	D.
V.	V.	kA	V.
a	a	k8xC	a
V.	V.	kA	V.
D.	D.	kA	D.
Golubjatnikové	Golubjatnik	k1gMnPc1	Golubjatnik
<g/>
,	,	kIx,	,
P.	P.	kA	P.
A.	A.	kA	A.
Pravojelavlev	Pravojelavlev	k1gMnSc1	Pravojelavlev
<g/>
,	,	kIx,	,
V.	V.	kA	V.
P.	P.	kA	P.
Baturin	Baturin	k1gInSc4	Baturin
a	a	k8xC	a
S.	S.	kA	S.
A.	A.	kA	A.
Kovalevskij	Kovalevskij	k1gMnSc1	Kovalevskij
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
průzkum	průzkum	k1gInSc4	průzkum
kolísání	kolísání	k1gNnSc2	kolísání
hladiny	hladina	k1gFnSc2	hladina
se	se	k3xPyFc4	se
zasloužili	zasloužit	k5eAaPmAgMnP	zasloužit
B.	B.	kA	B.
A.	A.	kA	A.
Appolov	Appolov	k1gInSc1	Appolov
<g/>
,	,	kIx,	,
V.	V.	kA	V.
V.	V.	kA	V.
Valedinskij	Valedinskij	k1gMnSc1	Valedinskij
<g/>
,	,	kIx,	,
K.	K.	kA	K.
P.	P.	kA	P.
Voskresenskij	Voskresenskij	k1gMnSc7	Voskresenskij
a	a	k8xC	a
L.	L.	kA	L.
S.	S.	kA	S.
Berg	Berg	k1gMnSc1	Berg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osídlení	osídlení	k1gNnSc3	osídlení
pobřeží	pobřeží	k1gNnSc3	pobřeží
==	==	k?	==
</s>
</p>
<p>
<s>
Města	město	k1gNnPc1	město
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kazachstán	Kazachstán	k1gInSc1	Kazachstán
</s>
</p>
<p>
<s>
Atyrauská	Atyrauský	k2eAgFnSc1d1	Atyrauský
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
Zaburunje	Zaburunj	k1gFnSc2	Zaburunj
<g/>
,	,	kIx,	,
Atyrau	Atyraus	k1gInSc2	Atyraus
</s>
</p>
<p>
<s>
Mangystauská	Mangystauský	k2eAgFnSc1d1	Mangystauský
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
Fort-Ševčenko	Fort-Ševčenka	k1gFnSc5	Fort-Ševčenka
<g/>
,	,	kIx,	,
Aktau	Aktaum	k1gNnSc6	Aktaum
<g/>
,	,	kIx,	,
Kuryk	Kuryk	k1gMnSc1	Kuryk
</s>
</p>
<p>
<s>
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
(	(	kIx(	(
<g/>
Bekdaš	Bekdaš	k1gMnSc1	Bekdaš
<g/>
,	,	kIx,	,
Karabogazgöl	Karabogazgöl	k1gMnSc1	Karabogazgöl
<g/>
,	,	kIx,	,
Kuuli	Kuule	k1gFnSc4	Kuule
Majak	Majak	k1gInSc4	Majak
<g/>
,	,	kIx,	,
Türkmenbaši	Türkmenbaše	k1gFnSc4	Türkmenbaše
<g/>
,	,	kIx,	,
Džanga	Džang	k1gMnSc4	Džang
<g/>
,	,	kIx,	,
<g/>
Aladža	Aladž	k2eAgMnSc4d1	Aladž
<g/>
,	,	kIx,	,
Okarem	Okar	k1gInSc7	Okar
<g/>
,	,	kIx,	,
Gasan-Kuli	Gasan-Kule	k1gFnSc4	Gasan-Kule
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Írán	Írán	k1gInSc1	Írán
</s>
</p>
<p>
<s>
provincie	provincie	k1gFnSc1	provincie
Mázandarán	Mázandarán	k1gInSc1	Mázandarán
a	a	k8xC	a
Golestán	Golestán	k1gInSc1	Golestán
–	–	k?	–
Gomíšán	Gomíšán	k1gInSc1	Gomíšán
<g/>
,	,	kIx,	,
Bandar-e	Bandar	k1gInSc1	Bandar-e
Torkemán	Torkemán	k2eAgInSc1d1	Torkemán
<g/>
,	,	kIx,	,
Hazarábád	Hazarábáda	k1gFnPc2	Hazarábáda
<g/>
,	,	kIx,	,
Bábolsar	Bábolsara	k1gFnPc2	Bábolsara
<g/>
,	,	kIx,	,
Fereidúnkenár	Fereidúnkenár	k1gInSc1	Fereidúnkenár
<g/>
,	,	kIx,	,
Mahmúdábád	Mahmúdábáda	k1gFnPc2	Mahmúdábáda
<g/>
,	,	kIx,	,
Núr	Núr	k1gFnPc2	Núr
<g/>
,	,	kIx,	,
Núšahr	Núšahra	k1gFnPc2	Núšahra
<g/>
,	,	kIx,	,
Čalús	Čalúsa	k1gFnPc2	Čalúsa
<g/>
,	,	kIx,	,
Abbásábád	Abbásábáda	k1gFnPc2	Abbásábáda
<g/>
,	,	kIx,	,
Tonekábon	Tonekábona	k1gFnPc2	Tonekábona
<g/>
,	,	kIx,	,
Rámsar	Rámsara	k1gFnPc2	Rámsara
</s>
</p>
<p>
<s>
provincie	provincie	k1gFnSc1	provincie
Gílán	Gílána	k1gFnPc2	Gílána
–	–	k?	–
Čáboksar	Čáboksar	k1gMnSc1	Čáboksar
<g/>
,	,	kIx,	,
Kaláčaj	Kaláčaj	k1gMnSc1	Kaláčaj
<g/>
,	,	kIx,	,
Rúdsar	Rúdsar	k1gMnSc1	Rúdsar
<g/>
,	,	kIx,	,
Kenár	Kenár	k1gMnSc1	Kenár
Darjá	Darjá	k1gFnSc1	Darjá
<g/>
,	,	kIx,	,
Homám	Homa	k1gFnPc3	Homa
<g/>
,	,	kIx,	,
Bandar-e	Bandar	k1gFnSc3	Bandar-e
Anzalí	Anzalý	k2eAgMnPc1d1	Anzalý
<g/>
,	,	kIx,	,
Ástára	Ástár	k1gMnSc2	Ástár
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
(	(	kIx(	(
<g/>
Astara	Astara	k1gFnSc1	Astara
<g/>
,	,	kIx,	,
Lenkoran	Lenkoran	k1gInSc1	Lenkoran
<g/>
,	,	kIx,	,
Neftčala	Neftčala	k1gFnSc1	Neftčala
<g/>
,	,	kIx,	,
Aljat	Aljat	k1gInSc1	Aljat
<g/>
,	,	kIx,	,
Gobustan	Gobustan	k1gInSc1	Gobustan
<g/>
,	,	kIx,	,
Primorsk	Primorsk	k1gInSc1	Primorsk
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc1	Baku
<g/>
,	,	kIx,	,
Mardakan	Mardakan	k1gInSc1	Mardakan
<g/>
,	,	kIx,	,
Buzovna	Buzovna	k1gFnSc1	Buzovna
<g/>
,	,	kIx,	,
Sumqayı	Sumqayı	k1gFnSc1	Sumqayı
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
</s>
</p>
<p>
<s>
Dagestán	Dagestán	k1gInSc1	Dagestán
–	–	k?	–
Derbent	Derbent	k1gInSc1	Derbent
<g/>
,	,	kIx,	,
Izberbaš	Izberbaš	k1gInSc1	Izberbaš
<g/>
,	,	kIx,	,
Kaspijsk	Kaspijsk	k1gInSc1	Kaspijsk
<g/>
,	,	kIx,	,
Machačkala	Machačkala	k1gFnSc1	Machačkala
<g/>
,	,	kIx,	,
Krajnovka	Krajnovka	k1gFnSc1	Krajnovka
</s>
</p>
<p>
<s>
Kalmycko	Kalmycko	k6eAd1	Kalmycko
–	–	k?	–
Lagaň	Lagaň	k1gMnSc1	Lagaň
</s>
</p>
<p>
<s>
Astrachaňská	astrachaňský	k2eAgFnSc1d1	astrachaňský
oblast	oblast	k1gFnSc1	oblast
–	–	k?	–
Oranžeri	Oranžeri	k1gNnSc1	Oranžeri
<g/>
,	,	kIx,	,
Astrachaň	Astrachaň	k1gFnSc1	Astrachaň
<g/>
,	,	kIx,	,
Kirovskij	Kirovskij	k1gFnSc1	Kirovskij
<g/>
,	,	kIx,	,
Makovo	Makovo	k1gNnSc1	Makovo
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Kolísání	kolísání	k1gNnSc1	kolísání
hladiny	hladina	k1gFnSc2	hladina
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1956	[number]	k4	1956
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
К	К	k?	К
у	у	k?	у
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Fedotov	Fedotov	k1gInSc1	Fedotov
P.	P.	kA	P.
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Stratigrafie	stratigrafie	k1gFnSc1	stratigrafie
čtvrtohorních	čtvrtohorní	k2eAgFnPc2d1	čtvrtohorní
usazenin	usazenina	k1gFnPc2	usazenina
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
vývoje	vývoj	k1gInSc2	vývoj
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ф	Ф	k?	Ф
П	П	k?	П
В	В	k?	В
<g/>
,	,	kIx,	,
С	С	k?	С
ч	ч	k?	ч
о	о	k?	о
и	и	k?	и
и	и	k?	и
р	р	k?	р
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Geologická	geologický	k2eAgFnSc1d1	geologická
stavba	stavba	k1gFnSc1	stavba
podvodního	podvodní	k2eAgInSc2d1	podvodní
sklonu	sklon	k1gInSc2	sklon
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Г	Г	k?	Г
с	с	k?	с
п	п	k?	п
с	с	k?	с
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Materiály	materiál	k1gInPc4	materiál
Všesovětské	Všesovětský	k2eAgFnSc2d1	Všesovětský
konference	konference	k1gFnSc2	konference
o	o	k7c6	o
problému	problém	k1gInSc6	problém
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc2	Baku
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
М	М	k?	М
В	В	k?	В
с	с	k?	с
п	п	k?	п
п	п	k?	п
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
Б	Б	k?	Б
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Zenkevič	Zenkevič	k1gInSc1	Zenkevič
<g/>
,	,	kIx,	,
O.	O.	kA	O.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Biologie	biologie	k1gFnPc1	biologie
moří	mořit	k5eAaImIp3nP	mořit
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
З	З	k?	З
Л	Л	k?	Л
А	А	k?	А
<g/>
,	,	kIx,	,
Б	Б	k?	Б
м	м	k?	м
С	С	k?	С
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Leontěv	Leontěv	k1gMnSc1	Leontěv
A.	A.	kA	A.
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Chalilov	Chalilov	k1gInSc1	Chalilov
A.	A.	kA	A.
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
podmínky	podmínka	k1gFnPc1	podmínka
formování	formování	k1gNnSc2	formování
břehů	břeh	k1gInPc2	břeh
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc2	Baku
1965	[number]	k4	1965
(	(	kIx(	(
<g/>
Л	Л	k?	Л
О	О	k?	О
К	К	k?	К
<g/>
,	,	kIx,	,
Х	Х	k?	Х
А	А	k?	А
И	И	k?	И
<g/>
,	,	kIx,	,
П	П	k?	П
у	у	k?	у
ф	ф	k?	ф
б	б	k?	б
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
Б	Б	k?	Б
<g/>
,	,	kIx,	,
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Pachomova	Pachomův	k2eAgFnSc1d1	Pachomův
A.	A.	kA	A.
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Zatučnaja	Zatučnaja	k1gMnSc1	Zatučnaja
B.	B.	kA	B.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Hydrochemie	Hydrochemie	k1gFnSc1	Hydrochemie
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
П	П	k?	П
А	А	k?	А
С	С	k?	С
<g/>
,	,	kIx,	,
З	З	k?	З
Б	Б	k?	Б
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Г	Г	k?	Г
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
Л	Л	k?	Л
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Geologie	geologie	k1gFnSc1	geologie
nalezišť	naleziště	k1gNnPc2	naleziště
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Г	Г	k?	Г
н	н	k?	н
и	и	k?	и
г	г	k?	г
м	м	k?	м
А	А	k?	А
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Komplexní	komplexní	k2eAgInSc1d1	komplexní
výzkum	výzkum	k1gInSc1	výzkum
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
statí	stať	k1gFnPc2	stať
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
К	К	k?	К
и	и	k?	и
К	К	k?	К
м	м	k?	м
<g/>
.	.	kIx.	.
С	С	k?	С
<g/>
.	.	kIx.	.
с	с	k?	с
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
в	в	k?	в
1	[number]	k4	1
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Gjul	Gjul	k1gInSc1	Gjul
K.	K.	kA	K.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Lappalajnen	Lappalajnen	k1gInSc1	Lappalajnen
T.	T.	kA	T.
N.	N.	kA	N.
<g/>
,	,	kIx,	,
Poluškin	Poluškin	k1gMnSc1	Poluškin
V.	V.	kA	V.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Г	Г	k?	Г
К	К	k?	К
К	К	k?	К
<g/>
,	,	kIx,	,
Л	Л	k?	Л
T.	T.	kA	T.
H.	H.	kA	H.
<g/>
,	,	kIx,	,
П	П	k?	П
В	В	k?	В
А	А	k?	А
<g/>
,	,	kIx,	,
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
<g/>
)	)	kIx)	)
Gjul	Gjul	k1gInSc1	Gjul
K.	K.	kA	K.
K.	K.	kA	K.
<g/>
,	,	kIx,	,
Žilo	žít	k5eAaImAgNnS	žít
P.	P.	kA	P.
V.	V.	kA	V.
<g/>
,	,	kIx,	,
Žirnov	Žirnov	k1gInSc1	Žirnov
V.	V.	kA	V.
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Bibliografická	bibliografický	k2eAgFnSc1d1	bibliografická
příručka	příručka	k1gFnSc1	příručka
o	o	k7c6	o
Kaspickém	kaspický	k2eAgNnSc6d1	Kaspické
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
Baku	Baku	k1gNnSc7	Baku
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Г	Г	k?	Г
К	К	k?	К
К	К	k?	К
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
П	П	k?	П
В	В	k?	В
<g/>
,	,	kIx,	,
Ж	Ж	k?	Ж
В	В	k?	В
M.	M.	kA	M.
<g/>
,	,	kIx,	,
Б	Б	k?	Б
а	а	k?	а
с	с	k?	с
п	п	k?	п
К	К	k?	К
м	м	k?	м
<g/>
,	,	kIx,	,
Б	Б	k?	Б
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
К	К	k?	К
mо	mо	k?	mо
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Kaspické	kaspický	k2eAgNnSc1d1	Kaspické
moře	moře	k1gNnSc1	moře
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kaspické	kaspický	k2eAgFnSc2d1	kaspická
moře	moře	k1gNnSc4	moře
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
ZELENKA	Zelenka	k1gMnSc1	Zelenka
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc1	napětí
u	u	k7c2	u
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
<g/>
:	:	kIx,	:
státy	stát	k1gInPc1	stát
zbrojí	zbrojit	k5eAaImIp3nP	zbrojit
kvůli	kvůli	k7c3	kvůli
vůni	vůně	k1gFnSc3	vůně
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Lidovky	Lidovky	k1gFnPc1	Lidovky
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2014-08-13	[number]	k4	2014-08-13
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HONOMICHL	HONOMICHL	kA	HONOMICHL
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
o	o	k7c4	o
kaspickou	kaspický	k2eAgFnSc4d1	kaspická
ropu	ropa	k1gFnSc4	ropa
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
e-polis	eolis	k1gFnSc1	e-polis
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2005-06-10	[number]	k4	2005-06-10
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
historii	historie	k1gFnSc6	historie
jmen	jméno	k1gNnPc2	jméno
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Kaspický	kaspický	k2eAgInSc1d1	kaspický
environmentální	environmentální	k2eAgInSc1d1	environmentální
program	program	k1gInSc1	program
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Cíl	cíl	k1gInSc1	cíl
<g/>
:	:	kIx,	:
Ropa	ropa	k1gFnSc1	ropa
v	v	k7c6	v
Kaspickém	kaspický	k2eAgNnSc6d1	Kaspické
moři	moře	k1gNnSc6	moře
John	John	k1gMnSc1	John
Robb	Robb	k1gMnSc1	Robb
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Změny	změna	k1gFnPc1	změna
hladiny	hladina	k1gFnSc2	hladina
Kaspického	kaspický	k2eAgNnSc2d1	Kaspické
moře	moře	k1gNnSc2	moře
</s>
</p>
