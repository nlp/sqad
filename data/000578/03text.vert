<s>
Severus	Severus	k1gMnSc1	Severus
Snape	Snap	k1gInSc5	Snap
je	být	k5eAaImIp3nS	být
literární	literární	k2eAgFnSc1d1	literární
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
románů	román	k1gInPc2	román
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
spisovatelky	spisovatelka	k1gFnPc1	spisovatelka
Joanne	Joann	k1gInSc5	Joann
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
hlavního	hlavní	k2eAgMnSc4d1	hlavní
tragického	tragický	k2eAgMnSc4d1	tragický
hrdinu	hrdina	k1gMnSc4	hrdina
celé	celý	k2eAgFnSc2d1	celá
literární	literární	k2eAgFnSc2d1	literární
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
čarodějce	čarodějka	k1gFnSc6	čarodějka
Eileen	Eileno	k1gNnPc2	Eileno
Prince-Lloydové	Prince-Lloydový	k2eAgFnSc2d1	Prince-Lloydový
<g/>
,	,	kIx,	,
provdané	provdaný	k2eAgFnSc2d1	provdaná
Snapeové	Snapeová	k1gFnSc2	Snapeová
a	a	k8xC	a
mudlovi	mudlův	k2eAgMnPc1d1	mudlův
Tobiasi	Tobias	k1gMnPc1	Tobias
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
jen	jen	k9	jen
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
čistou	čistý	k2eAgFnSc4d1	čistá
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
profesorem	profesor	k1gMnSc7	profesor
lektvarů	lektvar	k1gInPc2	lektvar
na	na	k7c6	na
bradavické	bradavický	k2eAgFnSc6d1	bradavická
Škole	škola	k1gFnSc6	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
šestnáctý	šestnáctý	k4xOgInSc4	šestnáctý
rok	rok	k1gInSc4	rok
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
Obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
profesorem	profesor	k1gMnSc7	profesor
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
chová	chovat	k5eAaImIp3nS	chovat
s	s	k7c7	s
despektem	despekt	k1gInSc7	despekt
a	a	k8xC	a
záští	zášť	k1gFnSc7	zášť
<g/>
.	.	kIx.	.
</s>
<s>
Severus	Severus	k1gMnSc1	Severus
Snape	Snap	k1gInSc5	Snap
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
popisu	popis	k1gInSc2	popis
sinalou	sinalý	k2eAgFnSc4d1	sinalá
pleť	pleť	k1gFnSc4	pleť
<g/>
,	,	kIx,	,
studené	studený	k2eAgFnSc6d1	studená
černé	černá	k1gFnSc6	černá
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
hákovitý	hákovitý	k2eAgInSc4d1	hákovitý
nos	nos	k1gInSc4	nos
<g/>
,	,	kIx,	,
mastné	mastný	k2eAgInPc4d1	mastný
černé	černý	k2eAgInPc4d1	černý
vlasy	vlas	k1gInPc4	vlas
po	po	k7c4	po
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
žluté	žlutý	k2eAgInPc4d1	žlutý
zuby	zub	k1gInPc4	zub
<g/>
.	.	kIx.	.
</s>
<s>
Obléká	oblékat	k5eAaImIp3nS	oblékat
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
do	do	k7c2	do
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
černého	černý	k2eAgInSc2d1	černý
hábitu	hábit	k1gInSc2	hábit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
"	"	kIx"	"
<g/>
přerostlého	přerostlý	k2eAgMnSc4d1	přerostlý
netopýra	netopýr	k1gMnSc4	netopýr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
ho	on	k3xPp3gInSc4	on
hraje	hrát	k5eAaImIp3nS	hrát
Alan	Alan	k1gMnSc1	Alan
Rickman	Rickman	k1gMnSc1	Rickman
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Severus	Severus	k1gMnSc1	Severus
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jména	jméno	k1gNnPc1	jméno
jiných	jiný	k2eAgFnPc2d1	jiná
postav	postava	k1gFnPc2	postava
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
přísný	přísný	k2eAgInSc1d1	přísný
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
latinských	latinský	k2eAgNnPc2d1	latinské
jmen	jméno	k1gNnPc2	jméno
šlo	jít	k5eAaImAgNnS	jít
ale	ale	k8xC	ale
o	o	k7c4	o
poměrně	poměrně	k6eAd1	poměrně
běžné	běžný	k2eAgNnSc4d1	běžné
středověké	středověký	k2eAgNnSc4d1	středověké
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Příjmení	příjmení	k1gNnSc1	příjmení
Snape	Snap	k1gInSc5	Snap
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
malé	malý	k2eAgFnSc2d1	malá
vesnice	vesnice	k1gFnSc2	vesnice
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Suffolk	Suffolka	k1gFnPc2	Suffolka
<g/>
,	,	kIx,	,
možná	možná	k9	možná
je	být	k5eAaImIp3nS	být
i	i	k9	i
souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
anglickým	anglický	k2eAgNnSc7d1	anglické
slovesem	sloveso	k1gNnSc7	sloveso
snappy	snappa	k1gFnSc2	snappa
(	(	kIx(	(
<g/>
kousavý	kousavý	k2eAgInSc4d1	kousavý
<g/>
,	,	kIx,	,
nevrlý	nevrlý	k2eAgInSc4d1	nevrlý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
o	o	k7c4	o
nejpropracovanější	propracovaný	k2eAgInSc4d3	nejpropracovanější
charakter	charakter	k1gInSc4	charakter
celé	celý	k2eAgFnSc2d1	celá
knižní	knižní	k2eAgFnSc2d1	knižní
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Severus	Severus	k1gMnSc1	Severus
Snape	Snap	k1gInSc5	Snap
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
v	v	k7c6	v
Cokeworthu	Cokeworth	k1gInSc6	Cokeworth
mudlovi	mudlův	k2eAgMnPc1d1	mudlův
Tobiasi	Tobias	k1gMnPc1	Tobias
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
a	a	k8xC	a
čarodějce	čarodějka	k1gFnSc3	čarodějka
Eileen	Eilena	k1gFnPc2	Eilena
Prince-Lloydové	Prince-Lloydová	k1gFnSc2	Prince-Lloydová
<g/>
.	.	kIx.	.
</s>
<s>
Neměl	mít	k5eNaImAgMnS	mít
příliš	příliš	k6eAd1	příliš
šťastné	šťastný	k2eAgNnSc4d1	šťastné
dětství	dětství	k1gNnSc4	dětství
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
alkoholik	alkoholik	k1gMnSc1	alkoholik
a	a	k8xC	a
násilník	násilník	k1gMnSc1	násilník
<g/>
.	.	kIx.	.
</s>
<s>
Severus	Severus	k1gMnSc1	Severus
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
spíše	spíše	k9	spíše
obracel	obracet	k5eAaImAgMnS	obracet
k	k	k7c3	k
rodině	rodina	k1gFnSc3	rodina
matky	matka	k1gFnSc2	matka
<g/>
,	,	kIx,	,
rodině	rodina	k1gFnSc3	rodina
kouzelnické	kouzelnický	k2eAgFnSc2d1	kouzelnická
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kouzelnickou	kouzelnický	k2eAgFnSc4d1	kouzelnická
přezdívku	přezdívka	k1gFnSc4	přezdívka
Princ	princa	k1gFnPc2	princa
dvojí	dvojit	k5eAaImIp3nP	dvojit
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgInS	být
základem	základ	k1gInSc7	základ
jeho	jeho	k3xOp3gFnSc2	jeho
nenávisti	nenávist	k1gFnSc2	nenávist
k	k	k7c3	k
mudlům	mudl	k1gMnPc3	mudl
a	a	k8xC	a
mudlovským	mudlovský	k2eAgMnPc3d1	mudlovský
kouzelníkům	kouzelník	k1gMnPc3	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byla	být	k5eAaImAgFnS	být
stejně	stejně	k6eAd1	stejně
stará	starý	k2eAgFnSc1d1	stará
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc1d1	místní
kouzelnice	kouzelnice	k1gFnSc1	kouzelnice
z	z	k7c2	z
mudlovské	mudlovský	k2eAgFnSc2d1	mudlovská
rodiny	rodina	k1gFnSc2	rodina
Lily	lít	k5eAaImAgFnP	lít
Evansová	Evansová	k1gFnSc1	Evansová
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
hluboce	hluboko	k6eAd1	hluboko
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
také	také	k9	také
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
měl	mít	k5eAaImAgInS	mít
malý	malý	k2eAgInSc1d1	malý
Severus	Severus	k1gInSc1	Severus
enormní	enormní	k2eAgInSc4d1	enormní
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
magii	magie	k1gFnSc4	magie
-	-	kIx~	-
podle	podle	k7c2	podle
slov	slovo	k1gNnPc2	slovo
svých	svůj	k3xOyFgMnPc2	svůj
spolužáků	spolužák	k1gMnPc2	spolužák
znal	znát	k5eAaImAgInS	znát
už	už	k6eAd1	už
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
"	"	kIx"	"
<g/>
více	hodně	k6eAd2	hodně
kleteb	kletba	k1gFnPc2	kletba
než	než	k8xS	než
půlka	půlka	k1gFnSc1	půlka
studentů	student	k1gMnPc2	student
sedmého	sedmý	k4xOgInSc2	sedmý
ročníku	ročník	k1gInSc2	ročník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
svědčí	svědčit	k5eAaImIp3nS	svědčit
také	také	k9	také
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
nadprůměrném	nadprůměrný	k2eAgNnSc6d1	nadprůměrné
nadání	nadání	k1gNnSc6	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k1gMnSc5	mladý
Snape	Snap	k1gMnSc5	Snap
byl	být	k5eAaImAgInS	být
však	však	k9	však
značně	značně	k6eAd1	značně
neoblíbený	oblíbený	k2eNgMnSc1d1	neoblíbený
a	a	k8xC	a
žárlil	žárlit	k5eAaImAgMnS	žárlit
na	na	k7c4	na
Harryho	Harry	k1gMnSc4	Harry
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
díle	díl	k1gInSc6	díl
Harry	Harra	k1gFnPc4	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
učí	učit	k5eAaImIp3nS	učit
od	od	k7c2	od
Snapea	Snape	k1gInSc2	Snape
nitrobranu	nitrobran	k1gInSc2	nitrobran
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
mysl	mysl	k1gFnSc1	mysl
nespojovala	spojovat	k5eNaImAgFnS	spojovat
s	s	k7c7	s
myslí	mysl	k1gFnSc7	mysl
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
spojit	spojit	k5eAaPmF	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
myslí	mysl	k1gFnSc7	mysl
Snapea	Snapeum	k1gNnSc2	Snapeum
a	a	k8xC	a
vidí	vidět	k5eAaImIp3nP	vidět
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gFnPc2	jeho
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
<g/>
:	:	kIx,	:
jak	jak	k8xC	jak
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
hákovitým	hákovitý	k2eAgInSc7d1	hákovitý
nosem	nos	k1gInSc7	nos
křičí	křičet	k5eAaImIp3nP	křičet
na	na	k7c4	na
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
za	za	k7c7	za
jejímiž	jejíž	k3xOyRp3gNnPc7	jejíž
zády	záda	k1gNnPc7	záda
se	se	k3xPyFc4	se
krčí	krčit	k5eAaImIp3nS	krčit
malý	malý	k2eAgMnSc1d1	malý
černovlasý	černovlasý	k2eAgMnSc1d1	černovlasý
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k1gMnSc5	mladý
Snape	Snap	k1gMnSc5	Snap
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
posteli	postel	k1gFnSc6	postel
a	a	k8xC	a
očividně	očividně	k6eAd1	očividně
se	se	k3xPyFc4	se
nudí	nudit	k5eAaImIp3nS	nudit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
sestřeluje	sestřelovat	k5eAaImIp3nS	sestřelovat
hůlkou	hůlka	k1gFnSc7	hůlka
mouchy	moucha	k1gFnSc2	moucha
ze	z	k7c2	z
stropu	strop	k1gInSc2	strop
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
nasednout	nasednout	k5eAaPmF	nasednout
na	na	k7c4	na
koště	koště	k1gNnSc4	koště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejde	jít	k5eNaImIp3nS	jít
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
a	a	k8xC	a
děvče	děvče	k1gNnSc1	děvče
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
směje	smát	k5eAaImIp3nS	smát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
díle	dílo	k1gNnSc6	dílo
se	se	k3xPyFc4	se
také	také	k9	také
Harry	Harra	k1gFnPc1	Harra
ponoří	ponořit	k5eAaPmIp3nP	ponořit
do	do	k7c2	do
myslánky	myslánka	k1gFnSc2	myslánka
se	s	k7c7	s
Snapeovými	Snapeův	k2eAgFnPc7d1	Snapeova
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
uvidí	uvidět	k5eAaPmIp3nS	uvidět
Snapea	Snapea	k1gFnSc1	Snapea
<g/>
,	,	kIx,	,
asi	asi	k9	asi
patnáctiletého	patnáctiletý	k2eAgMnSc2d1	patnáctiletý
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
píše	psát	k5eAaImIp3nS	psát
NKÚ	NKÚ	kA	NKÚ
z	z	k7c2	z
Obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
<g/>
,	,	kIx,	,
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
a	a	k8xC	a
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
hodina	hodina	k1gFnSc1	hodina
skončí	skončit	k5eAaPmIp3nS	skončit
<g/>
,	,	kIx,	,
jdou	jít	k5eAaImIp3nP	jít
tito	tento	k3xDgMnPc1	tento
čtyři	čtyři	k4xCgMnPc1	čtyři
k	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
dívky	dívka	k1gFnPc1	dívka
máčí	máčet	k5eAaImIp3nP	máčet
nohy	noha	k1gFnPc4	noha
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k8xC	i
Harryho	Harry	k1gMnSc2	Harry
matka	matka	k1gFnSc1	matka
-	-	kIx~	-
Lily	lít	k5eAaImAgFnP	lít
Evansová	Evansová	k1gFnSc1	Evansová
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jezeru	jezero	k1gNnSc3	jezero
jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
Severus	Severus	k1gInSc1	Severus
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
James	James	k1gInSc4	James
a	a	k8xC	a
Sirius	Sirius	k1gInSc4	Sirius
spatří	spatřit	k5eAaPmIp3nP	spatřit
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgFnPc4	tři
navzájem	navzájem	k6eAd1	navzájem
vrhat	vrhat	k5eAaImF	vrhat
různé	různý	k2eAgFnPc4d1	různá
kletby	kletba	k1gFnPc4	kletba
<g/>
.	.	kIx.	.
</s>
<s>
Nebelvírští	Nebelvírský	k2eAgMnPc1d1	Nebelvírský
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přesile	přesila	k1gFnSc6	přesila
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
za	za	k7c4	za
Snapea	Snapeus	k1gMnSc4	Snapeus
postaví	postavit	k5eAaPmIp3nS	postavit
Lily	lít	k5eAaImAgInP	lít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
uražený	uražený	k2eAgInSc1d1	uražený
a	a	k8xC	a
rozzuřený	rozzuřený	k2eAgInSc1d1	rozzuřený
Snape	Snap	k1gInSc5	Snap
ji	on	k3xPp3gFnSc4	on
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
nazve	nazvat	k5eAaBmIp3nS	nazvat
mudlovskou	mudlovský	k2eAgFnSc7d1	mudlovská
šmejdkou	šmejdka	k1gFnSc7	šmejdka
<g/>
.	.	kIx.	.
</s>
<s>
Lily	lít	k5eAaImAgFnP	lít
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
a	a	k8xC	a
odchází	odcházet	k5eAaImIp3nS	odcházet
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
díle	díl	k1gInSc6	díl
Harry	Harra	k1gFnPc4	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc2	relikvie
smrti	smrt	k1gFnSc2	smrt
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
ze	z	k7c2	z
Snapeovy	Snapeův	k2eAgFnSc2d1	Snapeova
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
Lily	lít	k5eAaImAgFnP	lít
znali	znát	k5eAaImAgMnP	znát
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
bydleli	bydlet	k5eAaImAgMnP	bydlet
kousek	kousek	k1gInSc4	kousek
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
její	její	k3xOp3gFnSc3	její
magii	magie	k1gFnSc3	magie
a	a	k8xC	a
seznamoval	seznamovat	k5eAaImAgMnS	seznamovat
ji	on	k3xPp3gFnSc4	on
se	s	k7c7	s
světem	svět	k1gInSc7	svět
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zná	znát	k5eAaImIp3nS	znát
díky	díky	k7c3	díky
matce	matka	k1gFnSc3	matka
lépe	dobře	k6eAd2	dobře
než	než	k8xS	než
Lily	lít	k5eAaImAgFnP	lít
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
příbuzní	příbuzný	k1gMnPc1	příbuzný
jsou	být	k5eAaImIp3nP	být
mudlové	mudlové	k2eAgMnPc1d1	mudlové
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
přátelství	přátelství	k1gNnSc1	přátelství
ale	ale	k9	ale
vadí	vadit	k5eAaImIp3nS	vadit
Lilyině	Lilyin	k2eAgFnSc3d1	Lilyina
sestře	sestra	k1gFnSc3	sestra
Petunii	Petunie	k1gFnSc3	Petunie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
žádné	žádný	k3yNgFnPc4	žádný
čarodějné	čarodějný	k2eAgFnPc4d1	čarodějná
schopnosti	schopnost	k1gFnPc4	schopnost
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
nemá	mít	k5eNaImIp3nS	mít
Petunii	Petunie	k1gFnSc4	Petunie
rád	rád	k6eAd1	rád
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
mudla	mudla	k6eAd1	mudla
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
i	i	k8xC	i
Lily	lít	k5eAaImAgFnP	lít
dostanou	dostat	k5eAaPmIp3nP	dostat
dopisy	dopis	k1gInPc4	dopis
z	z	k7c2	z
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Petunie	Petunie	k1gFnSc1	Petunie
nikdy	nikdy	k6eAd1	nikdy
nepřestane	přestat	k5eNaPmIp3nS	přestat
žárlit	žárlit	k5eAaImF	žárlit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
sestru	sestra	k1gFnSc4	sestra
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
je	být	k5eAaImIp3nS	být
Snape	Snap	k1gInSc5	Snap
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Lily	lít	k5eAaImAgFnP	lít
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
vyniká	vynikat	k5eAaImIp3nS	vynikat
v	v	k7c6	v
předmětech	předmět	k1gInPc6	předmět
Lektvary	lektvar	k1gInPc1	lektvar
a	a	k8xC	a
Obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
zmijozelskými	zmijozelský	k2eAgMnPc7d1	zmijozelský
se	se	k3xPyFc4	se
Snape	Snap	k1gInSc5	Snap
mění	měnit	k5eAaImIp3nS	měnit
<g/>
,	,	kIx,	,
žárlí	žárlit	k5eAaImIp3nS	žárlit
na	na	k7c6	na
Jamese	James	k1gInSc6	James
Pottera	Potter	k1gMnSc2	Potter
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
přátele	přítel	k1gMnPc4	přítel
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
s	s	k7c7	s
Lily	lít	k5eAaImAgFnP	lít
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
rozumějí	rozumět	k5eAaImIp3nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
mu	on	k3xPp3gNnSc3	on
jednou	jednou	k6eAd1	jednou
James	James	k1gInSc1	James
Potter	Pottra	k1gFnPc2	Pottra
zachrání	zachránit	k5eAaPmIp3nS	zachránit
život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
když	když	k8xS	když
Severus	Severus	k1gInSc1	Severus
málem	málem	k6eAd1	málem
padne	padnout	k5eAaImIp3nS	padnout
do	do	k7c2	do
léčky	léčka	k1gFnSc2	léčka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
nastražili	nastražit	k5eAaPmAgMnP	nastražit
se	s	k7c7	s
Siriusem	Sirius	k1gMnSc7	Sirius
Blackem	Black	k1gInSc7	Black
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
ho	on	k3xPp3gNnSc4	on
Snape	Snap	k1gInSc5	Snap
nenávidí	návidět	k5eNaImIp3nP	návidět
ještě	ještě	k9	ještě
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
s	s	k7c7	s
Lily	lít	k5eAaImAgFnP	lít
přestává	přestávat	k5eAaImIp3nS	přestávat
rozumět	rozumět	k5eAaImF	rozumět
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc7d1	poslední
kapkou	kapka	k1gFnSc7	kapka
je	být	k5eAaImIp3nS	být
chvíle	chvíle	k1gFnPc4	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
Snape	Snap	k1gInSc5	Snap
nazve	nazvat	k5eAaBmIp3nS	nazvat
mudlovskou	mudlovský	k2eAgFnSc7d1	mudlovská
šmejdkou	šmejdka	k1gFnSc7	šmejdka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
omlouvá	omlouvat	k5eAaImIp3nS	omlouvat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
názory	názor	k1gInPc4	názor
se	se	k3xPyFc4	se
natolik	natolik	k6eAd1	natolik
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
cesty	cesta	k1gFnPc1	cesta
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nespojí	spojit	k5eNaPmIp3nS	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Snapeova	Snapeův	k2eAgFnSc1d1	Snapeova
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
však	však	k9	však
stále	stále	k6eAd1	stále
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
stoupencem	stoupenec	k1gMnSc7	stoupenec
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Smrtijedem	Smrtijed	k1gMnSc7	Smrtijed
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
věštba	věštba	k1gFnSc1	věštba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vyslechl	vyslechnout	k5eAaPmAgMnS	vyslechnout
od	od	k7c2	od
profesorky	profesorka	k1gFnSc2	profesorka
Trellawneyové	Trellawneyová	k1gFnSc2	Trellawneyová
v	v	k7c6	v
hostinci	hostinec	k1gInSc6	hostinec
U	u	k7c2	u
Prasečí	prasečí	k2eAgFnSc2d1	prasečí
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
donesl	donést	k5eAaPmAgMnS	donést
lordu	lord	k1gMnSc3	lord
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
jejího	její	k3xOp3gMnSc2	její
syna	syn	k1gMnSc2	syn
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
všechny	všechen	k3xTgFnPc4	všechen
<g/>
,	,	kIx,	,
žádá	žádat	k5eAaImIp3nS	žádat
Voldemorta	Voldemorta	k1gFnSc1	Voldemorta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
ušetřil	ušetřit	k5eAaPmAgMnS	ušetřit
<g/>
.	.	kIx.	.
</s>
<s>
Navštíví	navštívit	k5eAaPmIp3nS	navštívit
také	také	k9	také
tajně	tajně	k6eAd1	tajně
ředitele	ředitel	k1gMnPc4	ředitel
školy	škola	k1gFnSc2	škola
Albuse	Albuse	k1gFnSc2	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
a	a	k8xC	a
prosí	prosit	k5eAaImIp3nS	prosit
ho	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
Lily	lít	k5eAaImAgFnP	lít
zachránil	zachránit	k5eAaPmAgMnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
hnusí	hnusit	k5eAaImIp3nS	hnusit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
chce	chtít	k5eAaImIp3nS	chtít
zachránit	zachránit	k5eAaPmF	zachránit
jen	jen	k9	jen
Lily	lít	k5eAaImAgFnP	lít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ona	onen	k3xDgFnSc1	onen
že	že	k8xS	že
nikdy	nikdy	k6eAd1	nikdy
nebude	být	k5eNaImBp3nS	být
bez	bez	k7c2	bez
syna	syn	k1gMnSc2	syn
a	a	k8xC	a
muže	muž	k1gMnSc2	muž
šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
Snape	Snap	k1gInSc5	Snap
poprosí	poprosit	k5eAaPmIp3nS	poprosit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
Brumbála	brumbál	k1gMnSc4	brumbál
udělá	udělat	k5eAaPmIp3nS	udělat
cokoli	cokoli	k3yInSc1	cokoli
<g/>
.	.	kIx.	.
</s>
<s>
Jenže	jenže	k8xC	jenže
Lily	lít	k5eAaImAgFnP	lít
položí	položit	k5eAaPmIp3nP	položit
život	život	k1gInSc4	život
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
je	on	k3xPp3gMnPc4	on
zcela	zcela	k6eAd1	zcela
zdrcen	zdrcen	k2eAgInSc1d1	zdrcen
<g/>
.	.	kIx.	.
</s>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
ho	on	k3xPp3gNnSc4	on
požádá	požádat	k5eAaPmIp3nS	požádat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
ochránit	ochránit	k5eAaPmF	ochránit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
za	za	k7c4	za
co	co	k3yInSc4	co
Lily	lít	k5eAaImAgFnP	lít
položila	položit	k5eAaPmAgFnS	položit
život	život	k1gInSc4	život
a	a	k8xC	a
co	co	k9	co
tu	tu	k6eAd1	tu
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
zbylo	zbýt	k5eAaPmAgNnS	zbýt
-	-	kIx~	-
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
Harry	Harr	k1gInPc1	Harr
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
slíbí	slíbit	k5eAaPmIp3nS	slíbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
kdokoli	kdokoli	k3yInSc1	kdokoli
kdykoli	kdykoli	k6eAd1	kdykoli
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
dostane	dostat	k5eAaPmIp3nS	dostat
místo	místo	k7c2	místo
profesora	profesor	k1gMnSc2	profesor
Lektvarů	lektvar	k1gInPc2	lektvar
a	a	k8xC	a
ředitele	ředitel	k1gMnSc2	ředitel
zmijozelské	zmijozelský	k2eAgFnSc2d1	zmijozelská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
Voldemortovým	Voldemortův	k2eAgInSc7d1	Voldemortův
pádem	pád	k1gInSc7	pád
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
Severus	Severus	k1gInSc4	Severus
Snape	Snap	k1gInSc5	Snap
do	do	k7c2	do
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
lituje	litovat	k5eAaImIp3nS	litovat
svých	svůj	k3xOyFgInPc2	svůj
činů	čin	k1gInPc2	čin
a	a	k8xC	a
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
mu	on	k3xPp3gMnSc3	on
věří	věřit	k5eAaImIp3nS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Voldemortova	Voldemortův	k2eAgInSc2d1	Voldemortův
pádu	pád	k1gInSc2	pád
tedy	tedy	k9	tedy
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k8xC	jako
dvojitý	dvojitý	k2eAgMnSc1d1	dvojitý
agent	agent	k1gMnSc1	agent
<g/>
.	.	kIx.	.
</s>
<s>
Vystavoval	vystavovat	k5eAaImAgInS	vystavovat
se	se	k3xPyFc4	se
smrtelnému	smrtelný	k2eAgNnSc3d1	smrtelné
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Voldemorta	Voldemort	k1gMnSc2	Voldemort
a	a	k8xC	a
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ochránil	ochránit	k5eAaPmAgMnS	ochránit
syna	syn	k1gMnSc4	syn
Lily	lít	k5eAaImAgInP	lít
Potterové	Potterová	k1gFnSc2	Potterová
-	-	kIx~	-
Harryho	Harry	k1gMnSc2	Harry
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
nepopiratelným	popiratelný	k2eNgMnSc7d1	nepopiratelný
odborníkem	odborník	k1gMnSc7	odborník
na	na	k7c4	na
lektvary	lektvar	k1gInPc4	lektvar
a	a	k8xC	a
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
kouzelníkem	kouzelník	k1gMnSc7	kouzelník
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
pedagogické	pedagogický	k2eAgFnPc1d1	pedagogická
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
značně	značně	k6eAd1	značně
rozporuplné	rozporuplný	k2eAgFnPc1d1	rozporuplná
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgMnPc3	svůj
studentům	student	k1gMnPc3	student
se	se	k3xPyFc4	se
často	často	k6eAd1	často
vysmívá	vysmívat	k5eAaImIp3nS	vysmívat
a	a	k8xC	a
ponižuje	ponižovat	k5eAaImIp3nS	ponižovat
je	on	k3xPp3gMnPc4	on
a	a	k8xC	a
viditelně	viditelně	k6eAd1	viditelně
nadržuje	nadržovat	k5eAaImIp3nS	nadržovat
své	svůj	k3xOyFgFnSc3	svůj
vlastní	vlastní	k2eAgFnSc3d1	vlastní
koleji	kolej	k1gFnSc3	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
rád	rád	k6eAd1	rád
strhává	strhávat	k5eAaImIp3nS	strhávat
body	bod	k1gInPc4	bod
nebelvírským	belvírský	k2eNgMnPc3d1	nebelvírský
studentům	student	k1gMnPc3	student
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nenávidí	návidět	k5eNaImIp3nP	návidět
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
ho	on	k3xPp3gNnSc4	on
Harry	Harr	k1gInPc1	Harr
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
podezírají	podezírat	k5eAaImIp3nP	podezírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chce	chtít	k5eAaImIp3nS	chtít
ukrást	ukrást	k5eAaPmF	ukrást
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
pro	pro	k7c4	pro
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
však	však	k9	však
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
ukrást	ukrást	k5eAaPmF	ukrást
profesor	profesor	k1gMnSc1	profesor
Quirell	Quirell	k1gMnSc1	Quirell
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
kámen	kámen	k1gInSc4	kámen
ochraňoval	ochraňovat	k5eAaImAgMnS	ochraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rok	rok	k1gInSc4	rok
po	po	k7c6	po
roce	rok	k1gInSc6	rok
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
obrany	obrana	k1gFnSc2	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
patnáct	patnáct	k4xCc4	patnáct
let	léto	k1gNnPc2	léto
ředitel	ředitel	k1gMnSc1	ředitel
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
nepřidělil	přidělit	k5eNaPmAgMnS	přidělit
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poslední	poslední	k2eAgInSc4d1	poslední
rok	rok	k1gInSc4	rok
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
získá	získat	k5eAaPmIp3nS	získat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stane	stanout	k5eAaPmIp3nS	stanout
osudným	osudný	k2eAgMnPc3d1	osudný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
prokleté	prokletý	k2eAgNnSc1d1	prokleté
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
tam	tam	k6eAd1	tam
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
vydrží	vydržet	k5eAaPmIp3nS	vydržet
jen	jen	k9	jen
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmi	velmi	k6eAd1	velmi
pozvedne	pozvednout	k5eAaPmIp3nS	pozvednout
úroveň	úroveň	k1gFnSc4	úroveň
tohoto	tento	k3xDgInSc2	tento
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
téhož	týž	k3xTgInSc2	týž
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
při	při	k7c6	při
napadení	napadení	k1gNnSc6	napadení
Bradavic	bradavice	k1gFnPc2	bradavice
smrtijedy	smrtijeda	k1gMnSc2	smrtijeda
zorganizovaném	zorganizovaný	k2eAgInSc6d1	zorganizovaný
Dracem	Draec	k1gInSc7	Draec
Malfoyem	Malfoy	k1gMnSc7	Malfoy
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
odzbrojeného	odzbrojený	k2eAgMnSc4d1	odzbrojený
Albuse	Albus	k1gInSc5	Albus
Brumbála	brumbál	k1gMnSc4	brumbál
kletbou	kletba	k1gFnSc7	kletba
Avada	Avado	k1gNnSc2	Avado
Kedavra	Kedavr	k1gInSc2	Kedavr
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
čin	čin	k1gInSc1	čin
je	být	k5eAaImIp3nS	být
však	však	k9	však
spáchán	spáchán	k2eAgInSc1d1	spáchán
na	na	k7c4	na
přímou	přímý	k2eAgFnSc4d1	přímá
žádost	žádost	k1gFnSc4	žádost
samotného	samotný	k2eAgInSc2d1	samotný
Albuse	Albuse	k1gFnPc4	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
zničení	zničení	k1gNnSc3	zničení
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
viteálů	viteál	k1gInPc2	viteál
je	být	k5eAaImIp3nS	být
Brumbál	brumbál	k1gMnSc1	brumbál
postižen	postihnout	k5eAaPmNgMnS	postihnout
kletbou	kletba	k1gFnSc7	kletba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
pomalu	pomalu	k6eAd1	pomalu
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Blízká	blízký	k2eAgFnSc1d1	blízká
smrt	smrt	k1gFnSc1	smrt
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
nevyhnutelná	vyhnutelný	k2eNgFnSc1d1	nevyhnutelná
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Draco	Draco	k1gNnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
<g/>
,	,	kIx,	,
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
si	se	k3xPyFc3	se
na	na	k7c6	na
Snapeovi	Snapeus	k1gMnSc6	Snapeus
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
slib	slib	k1gInSc1	slib
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
zabil	zabít	k5eAaPmAgInS	zabít
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
Malfoye	Malfoy	k1gFnSc2	Malfoy
<g/>
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
pak	pak	k6eAd1	pak
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
společně	společně	k6eAd1	společně
se	s	k7c7	s
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
na	na	k7c4	na
neznámé	známý	k2eNgNnSc4d1	neznámé
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
Voldemort	Voldemort	k1gInSc1	Voldemort
dostává	dostávat	k5eAaImIp3nS	dostávat
k	k	k7c3	k
plné	plný	k2eAgFnSc3d1	plná
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
bradavickým	bradavický	k2eAgMnSc7d1	bradavický
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
dobra	dobro	k1gNnSc2	dobro
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
až	až	k9	až
do	do	k7c2	do
úplného	úplný	k2eAgInSc2d1	úplný
konce	konec	k1gInSc2	konec
klamat	klamat	k5eAaImF	klamat
Pána	pán	k1gMnSc4	pán
zla	zlo	k1gNnSc2	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
se	se	k3xPyFc4	se
zabitím	zabití	k1gNnSc7	zabití
Albuse	Albuse	k1gFnSc2	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
stal	stát	k5eAaPmAgMnS	stát
právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
majitelem	majitel	k1gMnSc7	majitel
Bezové	bezový	k2eAgFnSc2d1	Bezová
hůlky	hůlka	k1gFnSc2	hůlka
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
hůlka	hůlka	k1gFnSc1	hůlka
sice	sice	k8xC	sice
patří	patřit	k5eAaImIp3nS	patřit
kouzelníkovi	kouzelník	k1gMnSc3	kouzelník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
předchozího	předchozí	k2eAgMnSc4d1	předchozí
majitele	majitel	k1gMnSc4	majitel
porazil	porazit	k5eAaPmAgMnS	porazit
nebo	nebo	k8xC	nebo
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Brumbála	brumbál	k1gMnSc4	brumbál
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
odzbrojil	odzbrojit	k5eAaPmAgMnS	odzbrojit
Malfoy	Malfo	k1gMnPc4	Malfo
a	a	k8xC	a
toho	ten	k3xDgMnSc4	ten
později	pozdě	k6eAd2	pozdě
odzbrojil	odzbrojit	k5eAaPmAgMnS	odzbrojit
Harry	Harr	k1gInPc7	Harr
<g/>
.	.	kIx.	.
</s>
<s>
Právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
majitelem	majitel	k1gMnSc7	majitel
hůlky	hůlka	k1gFnSc2	hůlka
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
Harry	Harr	k1gInPc4	Harr
a	a	k8xC	a
Snape	Snap	k1gInSc5	Snap
zemřel	zemřít	k5eAaPmAgMnS	zemřít
zbytečně	zbytečně	k6eAd1	zbytečně
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
podobá	podobat	k5eAaImIp3nS	podobat
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterého	který	k3yRgMnSc4	který
má	mít	k5eAaImIp3nS	mít
Snape	Snap	k1gInSc5	Snap
jen	jen	k9	jen
ty	ten	k3xDgFnPc4	ten
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
dva	dva	k4xCgMnPc1	dva
k	k	k7c3	k
sobě	se	k3xPyFc3	se
nikdy	nikdy	k6eAd1	nikdy
nenajdou	najít	k5eNaPmIp3nP	najít
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přetrvávající	přetrvávající	k2eAgFnSc1d1	přetrvávající
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
Lily	lít	k5eAaImAgFnP	lít
způsobí	způsobit	k5eAaPmIp3nP	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
nikdy	nikdy	k6eAd1	nikdy
Harrymu	Harrym	k1gInSc3	Harrym
doopravdy	doopravdy	k6eAd1	doopravdy
neublíží	ublížit	k5eNaPmIp3nS	ublížit
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
vždycky	vždycky	k6eAd1	vždycky
ho	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
jednoho	jeden	k4xCgInSc2	jeden
mrazivého	mrazivý	k2eAgInSc2d1	mrazivý
večera	večer	k1gInSc2	večer
poslal	poslat	k5eAaPmAgMnS	poslat
k	k	k7c3	k
Harrymu	Harrym	k1gInSc3	Harrym
svého	svůj	k3xOyFgMnSc4	svůj
patrona	patron	k1gMnSc4	patron
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
laně	laň	k1gFnSc2	laň
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
mj.	mj.	kA	mj.
také	také	k6eAd1	také
patron	patrona	k1gFnPc2	patrona
Lily	lít	k5eAaImAgInP	lít
Potterové	Potterový	k2eAgInPc1d1	Potterový
a	a	k8xC	a
předal	předat	k5eAaPmAgMnS	předat
mu	on	k3xPp3gMnSc3	on
pravý	pravý	k2eAgInSc4d1	pravý
meč	meč	k1gInSc4	meč
Godrika	Godrik	k1gMnSc2	Godrik
Nebelvíra	Nebelvír	k1gMnSc2	Nebelvír
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jím	on	k3xPp3gNnSc7	on
mohl	moct	k5eAaImAgInS	moct
zničit	zničit	k5eAaPmF	zničit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
viteálů	viteál	k1gInPc2	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Severuse	Severuse	k1gFnSc1	Severuse
Snapea	Snapea	k1gFnSc1	Snapea
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
bolestnější	bolestný	k2eAgMnSc1d2	bolestnější
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
pravou	pravý	k2eAgFnSc4d1	pravá
tvář	tvář	k1gFnSc4	tvář
tohoto	tento	k3xDgMnSc2	tento
muže	muž	k1gMnSc2	muž
poznali	poznat	k5eAaPmAgMnP	poznat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Harry	Harr	k1gMnPc4	Harr
prohlédne	prohlédnout	k5eAaPmIp3nS	prohlédnout
jeho	jeho	k3xOp3gFnPc4	jeho
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
mu	on	k3xPp3gMnSc3	on
Snape	Snap	k1gInSc5	Snap
předal	předat	k5eAaPmAgMnS	předat
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
tehdy	tehdy	k6eAd1	tehdy
Harry	Harr	k1gMnPc4	Harr
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Snape	Snap	k1gInSc5	Snap
ho	on	k3xPp3gMnSc4	on
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
chránil	chránit	k5eAaImAgMnS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Severusem	Severus	k1gMnSc7	Severus
Snapem	Snap	k1gMnSc7	Snap
odchází	odcházet	k5eAaImIp3nS	odcházet
také	také	k9	také
poslední	poslední	k2eAgNnSc1d1	poslední
spojení	spojení	k1gNnSc1	spojení
Harryho	Harry	k1gMnSc2	Harry
s	s	k7c7	s
minulostí	minulost	k1gFnSc7	minulost
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
přátel	přítel	k1gMnPc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
