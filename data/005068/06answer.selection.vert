<s>
Je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
rozprostřeného	rozprostřený	k2eAgNnSc2d1	rozprostřené
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
nechali	nechat	k5eAaPmAgMnP	nechat
patentovat	patentovat	k5eAaBmF	patentovat
hudební	hudební	k2eAgMnPc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
George	Georg	k1gMnSc2	Georg
Antheil	Antheil	k1gMnSc1	Antheil
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
rakouského	rakouský	k2eAgInSc2d1	rakouský
původu	původ	k1gInSc2	původ
Hedy	Heda	k1gFnSc2	Heda
Lamarr	Lamarra	k1gFnPc2	Lamarra
<g/>
.	.	kIx.	.
</s>
