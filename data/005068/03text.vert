<s>
Wi-Fi	Wi-Fi	k1gNnSc1	Wi-Fi
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Wi-fi	Wi	k1gMnPc1	Wi-f
<g/>
,	,	kIx,	,
WiFi	WiF	k1gMnPc1	WiF
<g/>
,	,	kIx,	,
Wifi	Wif	k1gMnPc1	Wif
<g/>
,	,	kIx,	,
wi-fi	wi	k1gMnPc1	wi-f
<g/>
,	,	kIx,	,
wifi	wif	k1gMnPc1	wif
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
standardů	standard	k1gInPc2	standard
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
popisujících	popisující	k2eAgFnPc2d1	popisující
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
komunikaci	komunikace	k1gFnSc4	komunikace
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
(	(	kIx(	(
<g/>
též	též	k9	též
Wireless	Wireless	k1gInSc1	Wireless
LAN	lano	k1gNnPc2	lano
<g/>
,	,	kIx,	,
WLAN	WLAN	kA	WLAN
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc4d1	samotný
název	název	k1gInSc4	název
WiFi	WiFi	k1gNnSc1	WiFi
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
Wireless	Wireless	k1gInSc4	Wireless
Ethernet	Ethernet	k1gInSc4	Ethernet
Compatibility	Compatibilita	k1gFnSc2	Compatibilita
Aliance	aliance	k1gFnSc1	aliance
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
využívá	využívat	k5eAaImIp3nS	využívat
tak	tak	k6eAd1	tak
zvaného	zvaný	k2eAgNnSc2d1	zvané
"	"	kIx"	"
<g/>
bezlicenčního	bezlicenční	k2eAgNnSc2d1	bezlicenční
frekvenčního	frekvenční	k2eAgNnSc2d1	frekvenční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
ideální	ideální	k2eAgFnSc1d1	ideální
pro	pro	k7c4	pro
budování	budování	k1gNnSc4	budování
levné	levný	k2eAgNnSc1d1	levné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výkonné	výkonný	k2eAgFnPc4d1	výkonná
sítě	síť	k1gFnPc4	síť
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
pokládky	pokládka	k1gFnSc2	pokládka
kabelů	kabel	k1gInPc2	kabel
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
původně	původně	k6eAd1	původně
neměl	mít	k5eNaImAgInS	mít
znamenat	znamenat	k5eAaImF	znamenat
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stala	stát	k5eAaPmAgFnS	stát
slovní	slovní	k2eAgFnSc1d1	slovní
hříčka	hříčka	k1gFnSc1	hříčka
wireless	wireless	k6eAd1	wireless
fidelity	fidelita	k1gFnSc2	fidelita
(	(	kIx(	(
<g/>
bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
)	)	kIx)	)
analogicky	analogicky	k6eAd1	analogicky
k	k	k7c3	k
Hi-Fi	Hi-Fi	k1gNnSc3	Hi-Fi
(	(	kIx(	(
<g/>
high	high	k1gInSc4	high
fidelity	fidelita	k1gFnSc2	fidelita
–	–	k?	–
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věrnost	věrnost	k1gFnSc1	věrnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wi-fi	Wii	k6eAd1	Wi-fi
jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nový	nový	k2eAgInSc4d1	nový
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
rozprostřeného	rozprostřený	k2eAgNnSc2d1	rozprostřené
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
nechali	nechat	k5eAaPmAgMnP	nechat
patentovat	patentovat	k5eAaBmF	patentovat
hudební	hudební	k2eAgMnPc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
George	Georg	k1gMnSc2	Georg
Antheil	Antheil	k1gMnSc1	Antheil
a	a	k8xC	a
herečka	herečka	k1gFnSc1	herečka
rakouského	rakouský	k2eAgInSc2d1	rakouský
původu	původ	k1gInSc2	původ
Hedy	Heda	k1gFnSc2	Heda
Lamarr	Lamarra	k1gFnPc2	Lamarra
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
zabývali	zabývat	k5eAaImAgMnP	zabývat
rádiově	rádiově	k6eAd1	rádiově
řízenými	řízený	k2eAgNnPc7d1	řízené
torpédy	torpédo	k1gNnPc7	torpédo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toto	tento	k3xDgNnSc4	tento
rádiové	rádiový	k2eAgNnSc4d1	rádiové
ovládání	ovládání	k1gNnSc4	ovládání
mohl	moct	k5eAaImAgMnS	moct
nepřítel	nepřítel	k1gMnSc1	nepřítel
rušit	rušit	k5eAaImF	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmínění	zmíněný	k2eAgMnPc1d1	zmíněný
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
ideu	idea	k1gFnSc4	idea
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
náhodná	náhodný	k2eAgFnSc1d1	náhodná
změna	změna	k1gFnSc1	změna
vysílacích	vysílací	k2eAgInPc2d1	vysílací
kanálů	kanál	k1gInPc2	kanál
snížila	snížit	k5eAaPmAgFnS	snížit
riziko	riziko	k1gNnSc4	riziko
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
rušení	rušení	k1gNnSc2	rušení
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
elektronický	elektronický	k2eAgInSc1d1	elektronický
děrný	děrný	k2eAgInSc1d1	děrný
pás	pás	k1gInSc1	pás
umožnil	umožnit	k5eAaPmAgInS	umožnit
přenos	přenos	k1gInSc4	přenos
rádiové	rádiový	k2eAgFnSc2d1	rádiová
komunikace	komunikace	k1gFnSc2	komunikace
mezi	mezi	k7c7	mezi
americkými	americký	k2eAgFnPc7d1	americká
loděmi	loď	k1gFnPc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
technologie	technologie	k1gFnSc1	technologie
využívala	využívat	k5eAaImAgFnS	využívat
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
i	i	k9	i
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
publikoval	publikovat	k5eAaBmAgInS	publikovat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
standardizační	standardizační	k2eAgInSc1d1	standardizační
institut	institut	k1gInSc1	institut
IEEE	IEEE	kA	IEEE
specifikaci	specifikace	k1gFnSc6	specifikace
standardu	standard	k1gInSc2	standard
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
pracující	pracující	k2eAgFnSc2d1	pracující
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
ISM	ISM	kA	ISM
pod	pod	k7c7	pod
označením	označení	k1gNnSc7	označení
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
standard	standard	k1gInSc1	standard
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
specifikace	specifikace	k1gFnPc4	specifikace
802.11	[number]	k4	802.11
<g/>
a	a	k8xC	a
a	a	k8xC	a
802.11	[number]	k4	802.11
<g/>
b	b	k?	b
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
organizace	organizace	k1gFnSc1	organizace
WECA	WECA	kA	WECA
(	(	kIx(	(
<g/>
Wireless	Wireless	k1gInSc1	Wireless
Ethernet	Etherneta	k1gFnPc2	Etherneta
Compatibility	Compatibilita	k1gFnSc2	Compatibilita
Alliance	Alliance	k1gFnSc2	Alliance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Wi-Fi	Wi-Fi	k1gNnSc4	Wi-Fi
Alliance	Alliance	k1gFnSc2	Alliance
<g/>
.	.	kIx.	.
</s>
<s>
Aliance	aliance	k1gFnSc1	aliance
přiděluje	přidělovat	k5eAaImIp3nS	přidělovat
po	po	k7c6	po
splnění	splnění	k1gNnSc6	splnění
podmínek	podmínka	k1gFnPc2	podmínka
logo	logo	k1gNnSc1	logo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ujišťuje	ujišťovat	k5eAaImIp3nS	ujišťovat
kupujícího	kupující	k1gMnSc4	kupující
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc2	jeho
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
zařízeními	zařízení	k1gNnPc7	zařízení
se	se	k3xPyFc4	se
stejným	stejný	k2eAgNnSc7d1	stejné
logem	logo	k1gNnSc7	logo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
prodávaných	prodávaný	k2eAgNnPc6d1	prodávané
zařízeních	zařízení	k1gNnPc6	zařízení
nejčastěji	často	k6eAd3	často
podporován	podporován	k2eAgInSc4d1	podporován
standard	standard	k1gInSc4	standard
802.11	[number]	k4	802.11
<g/>
g	g	kA	g
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
rychlost	rychlost	k1gFnSc4	rychlost
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
2,4	[number]	k4	2,4
GHz	GHz	k1gFnPc2	GHz
na	na	k7c4	na
54	[number]	k4	54
Mbps	Mbpsa	k1gFnPc2	Mbpsa
(	(	kIx(	(
<g/>
reálné	reálný	k2eAgFnPc1d1	reálná
přenosové	přenosový	k2eAgFnPc1d1	přenosová
rychlosti	rychlost	k1gFnPc1	rychlost
jsou	být	k5eAaImIp3nP	být
zhruba	zhruba	k6eAd1	zhruba
poloviční	poloviční	k2eAgInPc1d1	poloviční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Standard	standard	k1gInSc1	standard
802.11	[number]	k4	802.11
<g/>
n	n	k0	n
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
podporuje	podporovat	k5eAaImIp3nS	podporovat
technologii	technologie	k1gFnSc4	technologie
MIMO	mimo	k7c4	mimo
(	(	kIx(	(
<g/>
Multiple-input	Multiplenput	k2eAgInSc1d1	Multiple-input
multiple-output	multipleutput	k1gInSc1	multiple-output
–	–	k?	–
mnohonásobný	mnohonásobný	k2eAgInSc1d1	mnohonásobný
vstup	vstup	k1gInSc1	vstup
i	i	k8xC	i
výstup	výstup	k1gInSc1	výstup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
více	hodně	k6eAd2	hodně
vysílačů	vysílač	k1gInPc2	vysílač
a	a	k8xC	a
přijímačů	přijímač	k1gInPc2	přijímač
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
propustnost	propustnost	k1gFnSc1	propustnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
následoval	následovat	k5eAaImAgInS	následovat
standard	standard	k1gInSc1	standard
802.11	[number]	k4	802.11
<g/>
ac	ac	k?	ac
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
pak	pak	k6eAd1	pak
802.11	[number]	k4	802.11
<g/>
ad	ad	k7c4	ad
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
bezdrátové	bezdrátový	k2eAgFnSc3d1	bezdrátová
síti	síť	k1gFnSc3	síť
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
provozovatel	provozovatel	k1gMnSc1	provozovatel
od	od	k7c2	od
státu	stát	k1gInSc2	stát
patřičnou	patřičný	k2eAgFnSc4d1	patřičná
licenci	licence	k1gFnSc4	licence
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
frekvenci	frekvence	k1gFnSc6	frekvence
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
licencované	licencovaný	k2eAgNnSc1d1	licencované
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Frekvencí	frekvence	k1gFnSc7	frekvence
není	být	k5eNaImIp3nS	být
nekonečné	konečný	k2eNgNnSc4d1	nekonečné
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tato	tento	k3xDgNnPc1	tento
pásma	pásmo	k1gNnPc1	pásmo
jsou	být	k5eAaImIp3nP	být
zpoplatněna	zpoplatnit	k5eAaPmNgFnS	zpoplatnit
vysokou	vysoký	k2eAgFnSc7d1	vysoká
částkou	částka	k1gFnSc7	částka
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
majitelé	majitel	k1gMnPc1	majitel
licencí	licence	k1gFnPc2	licence
samozřejmě	samozřejmě	k6eAd1	samozřejmě
pásma	pásmo	k1gNnSc2	pásmo
chrání	chránit	k5eAaImIp3nP	chránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
nevysílal	vysílat	k5eNaImAgInS	vysílat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
rádiové	rádiový	k2eAgNnSc1d1	rádiové
vysílaní	vysílaný	k2eAgMnPc1d1	vysílaný
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
některé	některý	k3yIgInPc1	některý
přístroje	přístroj	k1gInPc1	přístroj
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mikrovlnná	mikrovlnný	k2eAgFnSc1d1	mikrovlnná
trouba	trouba	k1gFnSc1	trouba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
bezlicenční	bezlicenční	k2eAgNnSc1d1	bezlicenční
pásmo	pásmo	k1gNnSc1	pásmo
ISM	ISM	kA	ISM
(	(	kIx(	(
<g/>
2,4	[number]	k4	2,4
GHz	GHz	k1gFnPc2	GHz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
vyhrazeno	vyhradit	k5eAaPmNgNnS	vyhradit
pro	pro	k7c4	pro
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgInPc4d1	vědecký
a	a	k8xC	a
lékařské	lékařský	k2eAgInPc4d1	lékařský
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
stejné	stejný	k2eAgFnSc2d1	stejná
skupiny	skupina	k1gFnSc2	skupina
byly	být	k5eAaImAgFnP	být
postupně	postupně	k6eAd1	postupně
přidány	přidán	k2eAgFnPc4d1	přidána
další	další	k2eAgFnPc4d1	další
frekvence	frekvence	k1gFnPc4	frekvence
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
volné	volný	k2eAgNnSc4d1	volné
<g/>
"	"	kIx"	"
použití	použití	k1gNnSc4	použití
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
neoficiálně	neoficiálně	k6eAd1	neoficiálně
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
bezlicenční	bezlicenční	k2eAgNnPc1d1	bezlicenční
pásma	pásmo	k1gNnPc1	pásmo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutné	nutný	k2eAgNnSc1d1	nutné
dodržovat	dodržovat	k5eAaImF	dodržovat
podmínky	podmínka	k1gFnPc4	podmínka
stanovené	stanovený	k2eAgFnPc4d1	stanovená
Českým	český	k2eAgInSc7d1	český
telekomunikačním	telekomunikační	k2eAgInSc7d1	telekomunikační
úřadem	úřad	k1gInSc7	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
toto	tento	k3xDgNnSc4	tento
pásmo	pásmo	k1gNnSc4	pásmo
začali	začít	k5eAaPmAgMnP	začít
zajímat	zajímat	k5eAaImF	zajímat
i	i	k9	i
výrobci	výrobce	k1gMnPc1	výrobce
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zpočátku	zpočátku	k6eAd1	zpočátku
měl	mít	k5eAaImAgMnS	mít
každý	každý	k3xTgMnSc1	každý
výrobce	výrobce	k1gMnSc1	výrobce
vlastní	vlastní	k2eAgFnSc2d1	vlastní
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
výhodnost	výhodnost	k1gFnSc1	výhodnost
jednotných	jednotný	k2eAgInPc2d1	jednotný
standardů	standard	k1gInPc2	standard
(	(	kIx(	(
<g/>
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
<g/>
,	,	kIx,	,
Bluetooth	Bluetooth	k1gMnSc1	Bluetooth
<g/>
,	,	kIx,	,
WiMAX	WiMAX	k1gMnSc1	WiMAX
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
cílem	cíl	k1gInSc7	cíl
Wi-Fi	Wi-Fi	k1gNnSc2	Wi-Fi
sítí	sítí	k1gNnSc2	sítí
bylo	být	k5eAaImAgNnS	být
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
propojení	propojení	k1gNnSc4	propojení
přenosných	přenosný	k2eAgNnPc2d1	přenosné
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
jejich	jejich	k3xOp3gNnSc4	jejich
připojování	připojování	k1gNnSc4	připojování
na	na	k7c4	na
lokální	lokální	k2eAgFnSc4d1	lokální
(	(	kIx(	(
<g/>
např.	např.	kA	např.
firemní	firemní	k2eAgInSc1d1	firemní
<g/>
)	)	kIx)	)
sítě	síť	k1gFnPc1	síť
LAN	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
využívána	využívat	k5eAaImNgFnS	využívat
i	i	k9	i
k	k	k7c3	k
bezdrátovému	bezdrátový	k2eAgNnSc3d1	bezdrátové
připojení	připojení	k1gNnSc3	připojení
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
Internet	Internet	k1gInSc1	Internet
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rozsáhlejších	rozsáhlý	k2eAgFnPc2d2	rozsáhlejší
lokalit	lokalita	k1gFnPc2	lokalita
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
hotspotů	hotspot	k1gInPc2	hotspot
<g/>
.	.	kIx.	.
</s>
<s>
Wi-Fi	Wi-Fi	k6eAd1	Wi-Fi
zařízení	zařízení	k1gNnPc1	zařízení
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
přenosných	přenosný	k2eAgInPc6d1	přenosný
počítačích	počítač	k1gInPc6	počítač
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
mobilních	mobilní	k2eAgInPc6d1	mobilní
telefonech	telefon	k1gInPc6	telefon
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
Wi-Fi	Wi-Fi	k1gNnSc2	Wi-Fi
přineslo	přinést	k5eAaPmAgNnS	přinést
využívání	využívání	k1gNnSc1	využívání
bezlicenčního	bezlicenční	k2eAgNnSc2d1	bezlicenční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
negativní	negativní	k2eAgInPc4d1	negativní
důsledky	důsledek	k1gInPc4	důsledek
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
silného	silný	k2eAgNnSc2d1	silné
zarušení	zarušení	k1gNnSc2	zarušení
příslušného	příslušný	k2eAgNnSc2d1	příslušné
frekvenčního	frekvenční	k2eAgNnSc2d1	frekvenční
spektra	spektrum	k1gNnSc2	spektrum
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
častých	častý	k2eAgInPc2d1	častý
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
incidentů	incident	k1gInPc2	incident
<g/>
.	.	kIx.	.
</s>
<s>
Následníkem	následník	k1gMnSc7	následník
Wi-Fi	Wi-Fi	k1gNnSc2	Wi-Fi
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
technologie	technologie	k1gFnSc1	technologie
WiMAX	WiMAX	k1gFnSc1	WiMAX
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
zlepšení	zlepšení	k1gNnSc4	zlepšení
přenosu	přenos	k1gInSc2	přenos
signálu	signál	k1gInSc2	signál
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Wi-Fi	Wi-Fi	k6eAd1	Wi-Fi
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c6	na
spojové	spojový	k2eAgFnSc6d1	spojová
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
záležitost	záležitost	k1gFnSc4	záležitost
vyšších	vysoký	k2eAgInPc2d2	vyšší
protokolů	protokol	k1gInPc2	protokol
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Bluetooth	Bluetootha	k1gFnPc2	Bluetootha
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
služby	služba	k1gFnPc4	služba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
přenášejí	přenášet	k5eAaImIp3nP	přenášet
zapouzdřené	zapouzdřený	k2eAgInPc4d1	zapouzdřený
ethernetové	ethernetový	k2eAgInPc4d1	ethernetový
rámce	rámec	k1gInPc4	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c6	na
sdíleném	sdílený	k2eAgNnSc6d1	sdílené
médiu	médium	k1gNnSc6	médium
(	(	kIx(	(
<g/>
šíření	šíření	k1gNnSc1	šíření
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
prostorem	prostor	k1gInSc7	prostor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
protokol	protokol	k1gInSc1	protokol
CSMA	CSMA	kA	CSMA
<g/>
/	/	kIx~	/
<g/>
CA	ca	kA	ca
(	(	kIx(	(
<g/>
Ethernet	Ethernet	k1gMnSc1	Ethernet
používá	používat	k5eAaImIp3nS	používat
na	na	k7c6	na
vodičích	vodič	k1gInPc6	vodič
CSMA	CSMA	kA	CSMA
<g/>
/	/	kIx~	/
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
síť	síť	k1gFnSc1	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
požadované	požadovaný	k2eAgFnSc6d1	požadovaná
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
hraje	hrát	k5eAaImIp3nS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
identifikátor	identifikátor	k1gInSc1	identifikátor
SSID	SSID	kA	SSID
(	(	kIx(	(
<g/>
Service	Service	k1gFnSc1	Service
Set	set	k1gInSc1	set
Identifier	Identifier	k1gInSc1	Identifier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
řetězec	řetězec	k1gInSc4	řetězec
až	až	k9	až
32	[number]	k4	32
ASCII	ascii	kA	ascii
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
sítě	síť	k1gFnPc1	síť
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
<g/>
.	.	kIx.	.
</s>
<s>
SSID	SSID	kA	SSID
identifikátor	identifikátor	k1gInSc1	identifikátor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
vysílán	vysílán	k2eAgInSc4d1	vysílán
jako	jako	k9	jako
broadcast	broadcast	k1gInSc4	broadcast
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
všichni	všechen	k3xTgMnPc1	všechen
potenciální	potenciální	k2eAgMnPc1d1	potenciální
klienti	klient	k1gMnPc1	klient
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
snadno	snadno	k6eAd1	snadno
zobrazit	zobrazit	k5eAaPmF	zobrazit
dostupné	dostupný	k2eAgFnPc4d1	dostupná
bezdrátové	bezdrátový	k2eAgFnPc4d1	bezdrátová
sítě	síť	k1gFnPc4	síť
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
připojit	připojit	k5eAaPmF	připojit
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
asociovat	asociovat	k5eAaBmF	asociovat
se	se	k3xPyFc4	se
s	s	k7c7	s
přístupovým	přístupový	k2eAgInSc7d1	přístupový
bodem	bod	k1gInSc7	bod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
mohla	moct	k5eAaImAgNnP	moct
komunikovat	komunikovat	k5eAaImF	komunikovat
zařízení	zařízení	k1gNnPc1	zařízení
různých	různý	k2eAgMnPc2d1	různý
výrobců	výrobce	k1gMnPc2	výrobce
i	i	k8xC	i
různých	různý	k2eAgFnPc2d1	různá
platforem	platforma	k1gFnPc2	platforma
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
standardy	standard	k1gInPc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
specifikací	specifikace	k1gFnSc7	specifikace
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
institut	institut	k1gInSc1	institut
IEEE	IEEE	kA	IEEE
(	(	kIx(	(
<g/>
z	z	k7c2	z
angl.	angl.	k?	angl.
Institute	institut	k1gInSc5	institut
of	of	k?	of
Electrical	Electrical	k1gFnSc6	Electrical
and	and	k?	and
Electronic	Electronice	k1gFnPc2	Electronice
Engineers	Engineers	k1gInSc1	Engineers
<g/>
)	)	kIx)	)
-	-	kIx~	-
specifikace	specifikace	k1gFnSc1	specifikace
standardů	standard	k1gInPc2	standard
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
lokálních	lokální	k2eAgFnPc2d1	lokální
sítí	síť	k1gFnPc2	síť
jsou	být	k5eAaImIp3nP	být
publikovány	publikován	k2eAgInPc1d1	publikován
pod	pod	k7c7	pod
číslem	číslo	k1gNnSc7	číslo
802.11	[number]	k4	802.11
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
dále	daleko	k6eAd2	daleko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
užší	úzký	k2eAgFnPc4d2	užší
specifikace	specifikace	k1gFnPc4	specifikace
rozlišené	rozlišený	k2eAgFnPc4d1	rozlišená
revizními	revizní	k2eAgNnPc7d1	revizní
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
:	:	kIx,	:
např.	např.	kA	např.
802.11	[number]	k4	802.11
<g/>
b	b	k?	b
a	a	k8xC	a
802.11	[number]	k4	802.11
<g/>
g.	g.	k?	g.
Oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgFnPc4	tento
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
standardy	standard	k1gInPc1	standard
definují	definovat	k5eAaBmIp3nP	definovat
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
pracující	pracující	k2eAgFnSc2d1	pracující
ve	v	k7c6	v
volném	volný	k2eAgNnSc6d1	volné
pásmu	pásmo	k1gNnSc6	pásmo
2,4	[number]	k4	2,4
GHz	GHz	k1gFnPc2	GHz
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
maximální	maximální	k2eAgFnSc7d1	maximální
dosažitelnou	dosažitelný	k2eAgFnSc7d1	dosažitelná
rychlostí	rychlost	k1gFnSc7	rychlost
(	(	kIx(	(
<g/>
u	u	k7c2	u
standardu	standard	k1gInSc2	standard
802.11	[number]	k4	802.11
<g/>
b	b	k?	b
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
dosažitelnou	dosažitelný	k2eAgFnSc7d1	dosažitelná
rychlostí	rychlost	k1gFnSc7	rychlost
11	[number]	k4	11
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
u	u	k7c2	u
802.11	[number]	k4	802.11
<g/>
g	g	kA	g
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
až	až	k9	až
54	[number]	k4	54
Mb	Mb	k1gFnPc2	Mb
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
síť	síť	k1gFnSc4	síť
skrýt	skrýt	k5eAaPmF	skrýt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zamezit	zamezit	k5eAaPmF	zamezit
vysílání	vysílání	k1gNnSc4	vysílání
SSID	SSID	kA	SSID
<g/>
.	.	kIx.	.
</s>
<s>
Připojující	připojující	k2eAgMnSc1d1	připojující
se	se	k3xPyFc4	se
klient	klient	k1gMnSc1	klient
pak	pak	k6eAd1	pak
musí	muset	k5eAaImIp3nS	muset
SSID	SSID	kA	SSID
předem	předem	k6eAd1	předem
znát	znát	k5eAaImF	znát
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
k	k	k7c3	k
druhé	druhý	k4xOgFnSc3	druhý
straně	strana	k1gFnSc3	strana
připojit	připojit	k5eAaPmF	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
však	však	k9	však
SSID	SSID	kA	SSID
při	při	k7c6	při
připojování	připojování	k1gNnSc6	připojování
klienta	klient	k1gMnSc2	klient
přenášeno	přenášet	k5eAaImNgNnS	přenášet
v	v	k7c6	v
čitelné	čitelný	k2eAgFnSc6d1	čitelná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gInSc4	on
snadno	snadno	k6eAd1	snadno
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
skrytou	skrytý	k2eAgFnSc4d1	skrytá
síť	síť	k1gFnSc4	síť
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
SSID	SSID	kA	SSID
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
ad-hoc	adoc	k1gFnSc4	ad-hoc
síti	sít	k5eAaImF	sít
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
spojují	spojovat	k5eAaImIp3nP	spojovat
dva	dva	k4xCgMnPc1	dva
klienti	klient	k1gMnPc1	klient
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rovnocenné	rovnocenný	k2eAgFnSc6d1	rovnocenná
pozici	pozice	k1gFnSc6	pozice
(	(	kIx(	(
<g/>
peer-to-peer	peeroeer	k1gInSc1	peer-to-peer
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
identifikace	identifikace	k1gFnSc1	identifikace
probíhá	probíhat	k5eAaImIp3nS	probíhat
pomocí	pomocí	k7c2	pomocí
SSID	SSID	kA	SSID
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
rádiovém	rádiový	k2eAgInSc6d1	rádiový
dosahu	dosah	k1gInSc6	dosah
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
síť	síť	k1gFnSc4	síť
nebo	nebo	k8xC	nebo
příležitostné	příležitostný	k2eAgNnSc4d1	příležitostné
spojení	spojení	k1gNnSc4	spojení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
počítače	počítač	k1gInPc1	počítač
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
několika	několik	k4yIc2	několik
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
infrastrukturní	infrastrukturní	k2eAgFnSc1d1	infrastrukturní
bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
síť	síť	k1gFnSc1	síť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
přístupových	přístupový	k2eAgInPc2d1	přístupový
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
AP	ap	kA	ap
–	–	k?	–
Access	Access	k1gInSc1	Access
Point	pointa	k1gFnPc2	pointa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vysílají	vysílat	k5eAaImIp3nP	vysílat
své	svůj	k3xOyFgNnSc4	svůj
SSID	SSID	kA	SSID
<g/>
.	.	kIx.	.
</s>
<s>
Klient	klient	k1gMnSc1	klient
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
názvů	název	k1gInPc2	název
sítí	síť	k1gFnPc2	síť
vybere	vybrat	k5eAaPmIp3nS	vybrat
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
přístupových	přístupový	k2eAgInPc2d1	přístupový
bodů	bod	k1gInPc2	bod
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
stejný	stejný	k2eAgInSc4d1	stejný
SSID	SSID	kA	SSID
identifikátor	identifikátor	k1gInSc4	identifikátor
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
záležitostí	záležitost	k1gFnSc7	záležitost
klienta	klient	k1gMnSc2	klient
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
například	například	k6eAd1	například
přepojovat	přepojovat	k5eAaImF	přepojovat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
signálu	signál	k1gInSc2	signál
a	a	k8xC	a
umožňovat	umožňovat	k5eAaImF	umožňovat
tak	tak	k6eAd1	tak
klientovi	klient	k1gMnSc3	klient
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
roaming	roaming	k1gInSc1	roaming
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Access	Accessa	k1gFnPc2	Accessa
point	pointa	k1gFnPc2	pointa
<g/>
.	.	kIx.	.
</s>
<s>
Přístupový	přístupový	k2eAgInSc1d1	přístupový
bod	bod	k1gInSc1	bod
(	(	kIx(	(
<g/>
AP	ap	kA	ap
<g/>
)	)	kIx)	)
řídí	řídit	k5eAaImIp3nS	řídit
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
wi-fi	wii	k6eAd1	wi-fi
zařízeními	zařízení	k1gNnPc7	zařízení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
zapojena	zapojen	k2eAgNnPc1d1	zapojeno
v	v	k7c6	v
infrastrukturním	infrastrukturní	k2eAgInSc6d1	infrastrukturní
režimu	režim	k1gInSc6	režim
<g/>
.	.	kIx.	.
</s>
<s>
Přístupové	přístupový	k2eAgInPc4d1	přístupový
body	bod	k1gInPc4	bod
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
poskytování	poskytování	k1gNnSc4	poskytování
různých	různý	k2eAgFnPc2d1	různá
služeb	služba	k1gFnPc2	služba
pro	pro	k7c4	pro
lokální	lokální	k2eAgFnSc4d1	lokální
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
.	.	kIx.	.
</s>
<s>
Firewall	Firewall	k1gInSc1	Firewall
chrání	chránit	k5eAaImIp3nS	chránit
lokální	lokální	k2eAgFnSc4d1	lokální
síť	síť	k1gFnSc4	síť
před	před	k7c7	před
narušiteli	narušitel	k1gMnPc7	narušitel
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
omezuje	omezovat	k5eAaImIp3nS	omezovat
přístup	přístup	k1gInSc4	přístup
na	na	k7c4	na
počítač	počítač	k1gInSc4	počítač
nebo	nebo	k8xC	nebo
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
firewally	firewalla	k1gFnPc1	firewalla
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
a	a	k8xC	a
úrovně	úroveň	k1gFnPc4	úroveň
ochrany	ochrana	k1gFnSc2	ochrana
včetně	včetně	k7c2	včetně
blokování	blokování	k1gNnSc2	blokování
portů	port	k1gInPc2	port
používaných	používaný	k2eAgFnPc2d1	používaná
internetovými	internetový	k2eAgFnPc7d1	internetová
aplikacemi	aplikace	k1gFnPc7	aplikace
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
jiným	jiný	k2eAgInPc3d1	jiný
počítačům	počítač	k1gInPc3	počítač
<g/>
,	,	kIx,	,
zabránění	zabránění	k1gNnSc3	zabránění
přenosům	přenos	k1gInPc3	přenos
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gInSc2	jejich
původu	původ	k1gInSc2	původ
a	a	k8xC	a
analýzy	analýza	k1gFnSc2	analýza
a	a	k8xC	a
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
proniknutí	proniknutí	k1gNnSc4	proniknutí
na	na	k7c6	na
základě	základ	k1gInSc6	základ
určitých	určitý	k2eAgInPc2d1	určitý
modelů	model	k1gInPc2	model
podezřelých	podezřelý	k2eAgInPc2d1	podezřelý
přístupů	přístup	k1gInPc2	přístup
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
přístupových	přístupový	k2eAgInPc2d1	přístupový
bodů	bod	k1gInPc2	bod
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
firewall	firewall	k1gInSc1	firewall
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
firewallu	firewall	k1gInSc2	firewall
můžete	moct	k5eAaImIp2nP	moct
nakonfigurovat	nakonfigurovat	k5eAaPmF	nakonfigurovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
umožňovaly	umožňovat	k5eAaImAgInP	umožňovat
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
specifickým	specifický	k2eAgFnPc3d1	specifická
částem	část	k1gFnPc3	část
vaší	váš	k3xOp2gFnSc2	váš
sítě	síť	k1gFnSc2	síť
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
odepřely	odepřít	k5eAaPmAgFnP	odepřít
veškerý	veškerý	k3xTgInSc4	veškerý
přístup	přístup	k1gInSc4	přístup
zvnějšku	zvnějšku	k6eAd1	zvnějšku
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
gateway	gatewaa	k1gFnPc1	gatewaa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
počítačových	počítačový	k2eAgFnPc6d1	počítačová
sítích	síť	k1gFnPc6	síť
uzel	uzel	k1gInSc4	uzel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
sítě	síť	k1gFnPc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Brána	brána	k1gFnSc1	brána
musí	muset	k5eAaImIp3nS	muset
vykonávat	vykonávat	k5eAaImF	vykonávat
i	i	k9	i
funkci	funkce	k1gFnSc4	funkce
směrovače	směrovač	k1gInSc2	směrovač
(	(	kIx(	(
<g/>
routeru	router	k1gInSc2	router
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
ji	on	k3xPp3gFnSc4	on
řadíme	řadit	k5eAaImIp1nP	řadit
v	v	k7c6	v
posloupnosti	posloupnost	k1gFnSc6	posloupnost
síťových	síťový	k2eAgNnPc2d1	síťové
zařízení	zařízení	k1gNnPc2	zařízení
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Provozní	provozní	k2eAgInSc1d1	provozní
režim	režim	k1gInSc1	režim
WiFi	WiF	k1gFnSc2	WiF
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umí	umět	k5eAaImIp3nS	umět
kombinovat	kombinovat	k5eAaImF	kombinovat
příjem	příjem	k1gInSc4	příjem
dat	datum	k1gNnPc2	datum
přes	přes	k7c4	přes
WiFi	WiFe	k1gFnSc4	WiFe
část	část	k1gFnSc4	část
a	a	k8xC	a
nastavit	nastavit	k5eAaPmF	nastavit
ROUTER	ROUTER	kA	ROUTER
pro	pro	k7c4	pro
LAN	lano	k1gNnPc2	lano
výstupy	výstup	k1gInPc7	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Bezdrátové	bezdrátový	k2eAgFnPc1d1	bezdrátová
sítě	síť	k1gFnPc1	síť
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
běžné	běžný	k2eAgFnPc1d1	běžná
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
u	u	k7c2	u
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
tak	tak	k9	tak
u	u	k7c2	u
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
notebooků	notebook	k1gInPc2	notebook
má	mít	k5eAaImIp3nS	mít
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
karty	karta	k1gFnSc2	karta
předinstalovány	předinstalovat	k5eAaPmNgInP	předinstalovat
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
při	při	k7c6	při
přemísťování	přemísťování	k1gNnSc6	přemísťování
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgFnPc4d1	velká
výhody	výhoda	k1gFnPc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
jsou	být	k5eAaImIp3nP	být
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
bezpečnostním	bezpečnostní	k2eAgInPc3d1	bezpečnostní
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
<s>
Hackeři	hacker	k1gMnPc1	hacker
už	už	k6eAd1	už
našli	najít	k5eAaPmAgMnP	najít
bezdrátové	bezdrátový	k2eAgFnPc4d1	bezdrátová
sítě	síť	k1gFnPc4	síť
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
pronikli	proniknout	k5eAaPmAgMnP	proniknout
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
používají	používat	k5eAaImIp3nP	používat
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
nabourali	nabourat	k5eAaPmAgMnP	nabourat
do	do	k7c2	do
kabelových	kabelový	k2eAgFnPc2d1	kabelová
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podniky	podnik	k1gInPc1	podnik
definovaly	definovat	k5eAaBmAgInP	definovat
efektivní	efektivní	k2eAgFnSc4d1	efektivní
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ochrání	ochránit	k5eAaPmIp3nS	ochránit
proti	proti	k7c3	proti
neoprávněnému	oprávněný	k2eNgInSc3d1	neoprávněný
přístupu	přístup	k1gInSc3	přístup
k	k	k7c3	k
důležitým	důležitý	k2eAgInPc3d1	důležitý
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
narušení	narušení	k1gNnSc2	narušení
bezdrátových	bezdrátový	k2eAgInPc2d1	bezdrátový
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
WIPS	WIPS	kA	WIPS
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
detekce	detekce	k1gFnSc1	detekce
narušení	narušení	k1gNnSc2	narušení
bezdrátových	bezdrátový	k2eAgInPc2d1	bezdrátový
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
WIDS	WIDS	kA	WIDS
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
uplatnění	uplatnění	k1gNnSc3	uplatnění
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
bezpečnostní	bezpečnostní	k2eAgFnSc2d1	bezpečnostní
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Kdokoliv	kdokoliv	k3yInSc1	kdokoliv
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
geografického	geografický	k2eAgNnSc2d1	geografické
pásma	pásmo	k1gNnSc2	pásmo
otevřené	otevřený	k2eAgFnSc2d1	otevřená
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
nešifrované	šifrovaný	k2eNgFnSc2d1	nešifrovaná
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
může	moct	k5eAaImIp3nS	moct
"	"	kIx"	"
<g/>
čichat	čichat	k5eAaImF	čichat
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
zachytit	zachytit	k5eAaPmF	zachytit
a	a	k8xC	a
zaznamenat	zaznamenat	k5eAaPmF	zaznamenat
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
získat	získat	k5eAaPmF	získat
neoprávněný	oprávněný	k2eNgInSc4d1	neoprávněný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
interním	interní	k2eAgInPc3d1	interní
síťovým	síťový	k2eAgInPc3d1	síťový
zdrojům	zdroj	k1gInPc3	zdroj
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
použít	použít	k5eAaPmF	použít
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
zdroje	zdroj	k1gInPc4	zdroj
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
ničivých	ničivý	k2eAgInPc2d1	ničivý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nelegálních	legální	k2eNgInPc2d1	nelegální
aktů	akt	k1gInPc2	akt
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgNnPc1	takový
porušení	porušení	k1gNnPc1	porušení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vážnými	vážný	k2eAgInPc7d1	vážný
problémy	problém	k1gInPc7	problém
jak	jak	k8xS	jak
pro	pro	k7c4	pro
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
domácí	domácí	k2eAgFnSc2d1	domácí
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
routeru	router	k1gInSc2	router
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ho	on	k3xPp3gMnSc4	on
vlastník	vlastník	k1gMnSc1	vlastník
deaktivoval	deaktivovat	k5eAaImAgMnS	deaktivovat
pro	pro	k7c4	pro
větší	veliký	k2eAgNnSc4d2	veliký
pohodlí	pohodlí	k1gNnSc4	pohodlí
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
volný	volný	k2eAgInSc4d1	volný
aktivní	aktivní	k2eAgInSc4d1	aktivní
bod	bod	k1gInSc4	bod
(	(	kIx(	(
<g/>
hotspot	hotspot	k1gInSc4	hotspot
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
notebooky	notebook	k1gInPc1	notebook
mají	mít	k5eAaImIp3nP	mít
adaptér	adaptér	k1gInSc4	adaptér
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
již	již	k6eAd1	již
zabudovaný	zabudovaný	k2eAgMnSc1d1	zabudovaný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Intel	Intel	kA	Intel
"	"	kIx"	"
<g/>
Centrino	Centrino	k1gNnSc1	Centrino
<g/>
"	"	kIx"	"
technologie	technologie	k1gFnSc1	technologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
přídavný	přídavný	k2eAgInSc4d1	přídavný
adaptér	adaptér	k1gInSc4	adaptér
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
PCMCIA	PCMCIA	kA	PCMCIA
kartu	karta	k1gFnSc4	karta
nebo	nebo	k8xC	nebo
USB	USB	kA	USB
dongle	dongle	k1gInSc1	dongle
<g/>
.	.	kIx.	.
</s>
<s>
Vestavěné	vestavěný	k2eAgNnSc4d1	vestavěné
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ve	v	k7c6	v
výchozím	výchozí	k2eAgNnSc6d1	výchozí
nastavení	nastavení	k1gNnSc6	nastavení
povolené	povolený	k2eAgInPc1d1	povolený
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
majitel	majitel	k1gMnSc1	majitel
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vysílá	vysílat	k5eAaImIp3nS	vysílat
notebook	notebook	k1gInSc1	notebook
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
libovolnému	libovolný	k2eAgInSc3d1	libovolný
počítači	počítač	k1gInSc3	počítač
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc4d1	moderní
operační	operační	k2eAgInPc4d1	operační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
Mac	Mac	kA	Mac
OS	OS	kA	OS
nebo	nebo	k8xC	nebo
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
nastavit	nastavit	k5eAaPmF	nastavit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bezdrátové	bezdrátový	k2eAgInPc4d1	bezdrátový
vysílací	vysílací	k2eAgInPc4d1	vysílací
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
sdílení	sdílení	k1gNnSc2	sdílení
internetového	internetový	k2eAgNnSc2d1	internetové
připojení	připojení	k1gNnSc2	připojení
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
umožnit	umožnit	k5eAaPmF	umožnit
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
ostatním	ostatní	k2eAgMnPc3d1	ostatní
počítačům	počítač	k1gInPc3	počítač
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
přes	přes	k7c4	přes
hlavní	hlavní	k2eAgInSc4d1	hlavní
počítač	počítač	k1gInSc4	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
<g/>
,	,	kIx,	,
nedostatek	nedostatek	k1gInSc4	nedostatek
znalostí	znalost	k1gFnPc2	znalost
u	u	k7c2	u
běžných	běžný	k2eAgMnPc2d1	běžný
uživatelů	uživatel	k1gMnPc2	uživatel
ohledně	ohledně	k7c2	ohledně
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
často	často	k6eAd1	často
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ostatním	ostatní	k1gNnSc7	ostatní
snadný	snadný	k2eAgInSc1d1	snadný
přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
připojení	připojení	k1gNnSc3	připojení
<g/>
.	.	kIx.	.
</s>
<s>
Takového	takový	k3xDgMnSc4	takový
"	"	kIx"	"
Piggybacking	Piggybacking	k1gInSc4	Piggybacking
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
dosáhnuto	dosáhnut	k2eAgNnSc1d1	dosáhnuto
bez	bez	k7c2	bez
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
vlastníka	vlastník	k1gMnSc2	vlastník
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
samotný	samotný	k2eAgMnSc1d1	samotný
neoprávněný	oprávněný	k2eNgMnSc1d1	neoprávněný
uživatel	uživatel	k1gMnSc1	uživatel
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jeho	jeho	k3xOp3gInSc1	jeho
počítač	počítač	k1gInSc1	počítač
automaticky	automaticky	k6eAd1	automaticky
vybírá	vybírat	k5eAaImIp3nS	vybírat
nejbližší	blízký	k2eAgInPc4d3	Nejbližší
nezabezpečené	zabezpečený	k2eNgInPc4d1	nezabezpečený
přístupové	přístupový	k2eAgInPc4d1	přístupový
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
aspektů	aspekt	k1gInPc2	aspekt
počítačové	počítačový	k2eAgFnSc2d1	počítačová
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
organizace	organizace	k1gFnPc1	organizace
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zvláště	zvláště	k6eAd1	zvláště
citlivé	citlivý	k2eAgInPc1d1	citlivý
na	na	k7c4	na
narušení	narušení	k1gNnSc4	narušení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
způsobené	způsobený	k2eAgInPc1d1	způsobený
podvodnými	podvodný	k2eAgInPc7d1	podvodný
přístupovými	přístupový	k2eAgInPc7d1	přístupový
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
(	(	kIx(	(
<g/>
důvěryhodný	důvěryhodný	k2eAgInSc1d1	důvěryhodný
subjekt	subjekt	k1gInSc1	subjekt
<g/>
)	)	kIx)	)
využívá	využívat	k5eAaPmIp3nS	využívat
bezdrátového	bezdrátový	k2eAgInSc2d1	bezdrátový
routeru	router	k1gInSc2	router
a	a	k8xC	a
zapojí	zapojit	k5eAaPmIp3nS	zapojit
nezabezpečený	zabezpečený	k2eNgInSc1d1	nezabezpečený
switchport	switchport	k1gInSc1	switchport
<g/>
,	,	kIx,	,
celá	celý	k2eAgFnSc1d1	celá
síť	síť	k1gFnSc1	síť
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
komukoli	kdokoli	k3yInSc3	kdokoli
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
přidává	přidávat	k5eAaImIp3nS	přidávat
bezdrátové	bezdrátový	k2eAgNnSc4d1	bezdrátové
rozhraní	rozhraní	k1gNnSc4	rozhraní
na	na	k7c6	na
počítači	počítač	k1gInSc6	počítač
připojeném	připojený	k2eAgInSc6d1	připojený
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
přes	přes	k7c4	přes
otevřený	otevřený	k2eAgInSc4d1	otevřený
port	port	k1gInSc4	port
USB	USB	kA	USB
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
průlom	průlom	k1gInSc4	průlom
v	v	k7c6	v
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
umožnil	umožnit	k5eAaPmAgInS	umožnit
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
důvěrným	důvěrný	k2eAgInPc3d1	důvěrný
materiálům	materiál	k1gInPc3	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
efektivní	efektivní	k2eAgNnSc4d1	efektivní
protiopatření	protiopatření	k1gNnSc4	protiopatření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zákaz	zákaz	k1gInSc1	zákaz
otevřených	otevřený	k2eAgInPc2d1	otevřený
switchportů	switchport	k1gInPc2	switchport
během	během	k7c2	během
konfigurace	konfigurace	k1gFnSc2	konfigurace
vypínače	vypínač	k1gInSc2	vypínač
(	(	kIx(	(
<g/>
switch	switch	k1gMnSc1	switch
<g/>
)	)	kIx)	)
VLAN	VLAN	kA	VLAN
pro	pro	k7c4	pro
omezení	omezení	k1gNnSc4	omezení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ochránily	ochránit	k5eAaPmAgFnP	ochránit
sítě	síť	k1gFnPc4	síť
i	i	k8xC	i
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
takové	takový	k3xDgNnSc1	takový
protiopatření	protiopatření	k1gNnSc1	protiopatření
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
uplatňována	uplatňovat	k5eAaImNgFnS	uplatňovat
jednotně	jednotně	k6eAd1	jednotně
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
síťových	síťový	k2eAgNnPc6d1	síťové
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
zejména	zejména	k9	zejména
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
signál	signál	k1gInSc1	signál
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nS	šířit
i	i	k9	i
mimo	mimo	k7c4	mimo
zabezpečený	zabezpečený	k2eAgInSc4d1	zabezpečený
prostor	prostor	k1gInSc4	prostor
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
zdi	zeď	k1gFnPc4	zeď
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
mnoho	mnoho	k6eAd1	mnoho
uživatelů	uživatel	k1gMnPc2	uživatel
neuvědomuje	uvědomovat	k5eNaImIp3nS	uvědomovat
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezdrátová	bezdrátový	k2eAgNnPc1d1	bezdrátové
zařízení	zařízení	k1gNnPc1	zařízení
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
s	s	k7c7	s
nastavením	nastavení	k1gNnSc7	nastavení
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
zakoupení	zakoupení	k1gNnSc6	zakoupení
fungovala	fungovat	k5eAaImAgFnS	fungovat
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
zapojení	zapojení	k1gNnSc6	zapojení
do	do	k7c2	do
zásuvky	zásuvka	k1gFnSc2	zásuvka
<g/>
.	.	kIx.	.
</s>
<s>
Nezvaný	zvaný	k2eNgMnSc1d1	zvaný
host	host	k1gMnSc1	host
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
snadno	snadno	k6eAd1	snadno
připojit	připojit	k5eAaPmF	připojit
i	i	k9	i
do	do	k7c2	do
velmi	velmi	k6eAd1	velmi
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
jen	jen	k9	jen
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
směrové	směrový	k2eAgFnSc2d1	směrová
antény	anténa	k1gFnSc2	anténa
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
anténu	anténa	k1gFnSc4	anténa
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgNnPc2d1	používané
zabezpečení	zabezpečení	k1gNnPc2	zabezpečení
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
omezenou	omezený	k2eAgFnSc4d1	omezená
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
obejít	obejít	k5eAaPmF	obejít
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
postupně	postupně	k6eAd1	postupně
a	a	k8xC	a
proto	proto	k8xC	proto
starší	starý	k2eAgNnSc4d2	starší
zařízení	zařízení	k1gNnSc4	zařízení
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
jen	jen	k6eAd1	jen
omezené	omezený	k2eAgFnPc4d1	omezená
nebo	nebo	k8xC	nebo
žádné	žádný	k3yNgFnPc4	žádný
možnosti	možnost	k1gFnPc4	možnost
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
kvůli	kvůli	k7c3	kvůli
starším	starý	k2eAgNnPc3d2	starší
zařízením	zařízení	k1gNnPc3	zařízení
jsou	být	k5eAaImIp3nP	být
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
někdy	někdy	k6eAd1	někdy
zabezpečeny	zabezpečit	k5eAaPmNgInP	zabezpečit
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
na	na	k7c6	na
vyšší	vysoký	k2eAgFnSc6d2	vyšší
síťové	síťový	k2eAgFnSc6d1	síťová
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
virtuální	virtuální	k2eAgFnSc4d1	virtuální
privátní	privátní	k2eAgFnSc4d1	privátní
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
vybavený	vybavený	k2eAgInSc1d1	vybavený
směrovou	směrový	k2eAgFnSc7d1	směrová
anténou	anténa	k1gFnSc7	anténa
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
připojit	připojit	k5eAaPmF	připojit
do	do	k7c2	do
sítě	síť	k1gFnSc2	síť
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
několik	několik	k4yIc4	několik
set	set	k1gInSc4	set
metrů	metr	k1gInPc2	metr
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
několik	několik	k4yIc1	několik
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
síť	síť	k1gFnSc1	síť
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
má	mít	k5eAaImIp3nS	mít
dosah	dosah	k1gInSc4	dosah
pár	pár	k4xCyI	pár
desítek	desítka	k1gFnPc2	desítka
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
typy	typ	k1gInPc1	typ
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
starší	starý	k2eAgInPc1d2	starší
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
pouze	pouze	k6eAd1	pouze
omezené	omezený	k2eAgFnPc4d1	omezená
možnosti	možnost	k1gFnPc4	možnost
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
snadno	snadno	k6eAd1	snadno
prolomit	prolomit	k5eAaPmF	prolomit
<g/>
.	.	kIx.	.
</s>
<s>
Neznalí	znalý	k2eNgMnPc1d1	neznalý
uživatelé	uživatel	k1gMnPc1	uživatel
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
doplatit	doplatit	k5eAaPmF	doplatit
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bezdrátové	bezdrátový	k2eAgNnSc1d1	bezdrátové
zařízení	zařízení	k1gNnSc1	zařízení
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
bez	bez	k7c2	bez
nastaveného	nastavený	k2eAgNnSc2d1	nastavené
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nějakým	nějaký	k3yIgNnSc7	nějaký
výchozím	výchozí	k2eAgNnSc7d1	výchozí
nastavením	nastavení	k1gNnSc7	nastavení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
zařízení	zařízení	k1gNnPc2	zařízení
daného	daný	k2eAgInSc2d1	daný
typu	typ	k1gInSc2	typ
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
heslo	heslo	k1gNnSc1	heslo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
password	password	k1gInSc1	password
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
jsou	být	k5eAaImIp3nP	být
databáze	databáze	k1gFnPc1	databáze
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
nastavením	nastavení	k1gNnSc7	nastavení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
takto	takto	k6eAd1	takto
zabezpečenou	zabezpečený	k2eAgFnSc4d1	zabezpečená
síť	síť	k1gFnSc4	síť
dokáže	dokázat	k5eAaPmIp3nS	dokázat
prolomit	prolomit	k5eAaPmF	prolomit
i	i	k9	i
běžný	běžný	k2eAgMnSc1d1	běžný
uživatel	uživatel	k1gMnSc1	uživatel
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
hledání	hledání	k1gNnSc2	hledání
na	na	k7c4	na
googlu	googla	k1gFnSc4	googla
<g/>
.	.	kIx.	.
</s>
<s>
Bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
můžeme	moct	k5eAaImIp1nP	moct
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
šifrování	šifrování	k1gNnSc2	šifrování
=	=	kIx~	=
zabezpečení	zabezpečení	k1gNnSc1	zabezpečení
přenášených	přenášený	k2eAgNnPc2d1	přenášené
dat	datum	k1gNnPc2	datum
před	před	k7c7	před
odposlechem	odposlech	k1gInSc7	odposlech
autorizace	autorizace	k1gFnSc2	autorizace
=	=	kIx~	=
řízení	řízení	k1gNnSc2	řízení
přístupu	přístup	k1gInSc2	přístup
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
uživatelů	uživatel	k1gMnPc2	uživatel
Zablokování	zablokování	k1gNnSc2	zablokování
vysílání	vysílání	k1gNnSc2	vysílání
SSID	SSID	kA	SSID
sice	sice	k8xC	sice
porušuje	porušovat	k5eAaImIp3nS	porušovat
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
nejjednodušším	jednoduchý	k2eAgNnSc7d3	nejjednodušší
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
pomocí	pomocí	k7c2	pomocí
jejího	její	k3xOp3gNnSc2	její
zdánlivého	zdánlivý	k2eAgNnSc2d1	zdánlivé
skrytí	skrytí	k1gNnSc2	skrytí
<g/>
.	.	kIx.	.
</s>
<s>
Klienti	klient	k1gMnPc1	klient
síť	síť	k1gFnSc4	síť
nezobrazí	zobrazit	k5eNaPmIp3nP	zobrazit
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
dostupných	dostupný	k2eAgFnPc2d1	dostupná
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nepřijímají	přijímat	k5eNaImIp3nP	přijímat
broadcasty	broadcast	k1gInPc1	broadcast
se	s	k7c7	s
SSID	SSID	kA	SSID
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
při	při	k7c6	při
připojování	připojování	k1gNnSc6	připojování
klienta	klient	k1gMnSc2	klient
k	k	k7c3	k
přípojnému	přípojný	k2eAgInSc3d1	přípojný
bodu	bod	k1gInSc3	bod
je	být	k5eAaImIp3nS	být
SSID	SSID	kA	SSID
přenášen	přenášet	k5eAaImNgInS	přenášet
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
snadno	snadno	k6eAd1	snadno
zachytit	zachytit	k5eAaPmF	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zachytávání	zachytávání	k1gNnSc6	zachytávání
SSID	SSID	kA	SSID
při	při	k7c6	při
asociaci	asociace	k1gFnSc6	asociace
klienta	klient	k1gMnSc2	klient
s	s	k7c7	s
přípojným	přípojný	k2eAgInSc7d1	přípojný
bodem	bod	k1gInSc7	bod
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
provokací	provokace	k1gFnSc7	provokace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
útočník	útočník	k1gMnSc1	útočník
do	do	k7c2	do
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
vysílá	vysílat	k5eAaImIp3nS	vysílat
rámce	rámec	k1gInSc2	rámec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
přinutí	přinutit	k5eAaPmIp3nP	přinutit
klienty	klient	k1gMnPc4	klient
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
asociovali	asociovat	k5eAaBmAgMnP	asociovat
<g/>
.	.	kIx.	.
</s>
<s>
Přípojný	přípojný	k2eAgInSc1d1	přípojný
bod	bod	k1gInSc1	bod
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
seznam	seznam	k1gInSc1	seznam
MAC	Mac	kA	Mac
adres	adresa	k1gFnPc2	adresa
klientů	klient	k1gMnPc2	klient
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
se	se	k3xPyFc4	se
připojit	připojit	k5eAaPmF	připojit
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
whitelist	whitelist	k1gInSc1	whitelist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zrovna	zrovna	k6eAd1	zrovna
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nastavit	nastavit	k5eAaPmF	nastavit
blokování	blokování	k1gNnSc4	blokování
určitých	určitý	k2eAgFnPc2d1	určitá
MAC	Mac	kA	Mac
adres	adresa	k1gFnPc2	adresa
(	(	kIx(	(
<g/>
blacklist	blacklist	k1gInSc1	blacklist
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Útočník	útočník	k1gMnSc1	útočník
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vydávat	vydávat	k5eAaImF	vydávat
za	za	k7c4	za
stanici	stanice	k1gFnSc4	stanice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
do	do	k7c2	do
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
připojena	připojit	k5eAaPmNgFnS	připojit
pomocí	pomocí	k7c2	pomocí
nastavení	nastavení	k1gNnSc2	nastavení
stejné	stejný	k2eAgNnSc4d1	stejné
MAC	Mac	kA	Mac
adresy	adresa	k1gFnSc2	adresa
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
AP	ap	kA	ap
tato	tento	k3xDgFnSc1	tento
funkce	funkce	k1gFnSc1	funkce
aktivní	aktivní	k2eAgFnSc1d1	aktivní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přístupový	přístupový	k2eAgInSc1d1	přístupový
bod	bod	k1gInSc1	bod
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
autentizaci	autentizace	k1gFnSc4	autentizace
pomocí	pomocí	k7c2	pomocí
protokolu	protokol	k1gInSc2	protokol
IEEE	IEEE	kA	IEEE
802.1	[number]	k4	802.1
<g/>
X.	X.	kA	X.
Pro	pro	k7c4	pro
ověření	ověření	k1gNnSc4	ověření
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
klienta	klient	k1gMnSc2	klient
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
prosebník	prosebník	k1gMnSc1	prosebník
(	(	kIx(	(
<g/>
suplikant	suplikant	k1gMnSc1	suplikant
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterému	který	k3yIgNnSc3	který
přístupový	přístupový	k2eAgInSc4d1	přístupový
bod	bod	k1gInSc4	bod
zprostředkuje	zprostředkovat	k5eAaPmIp3nS	zprostředkovat
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
stranou	strana	k1gFnSc7	strana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ověření	ověření	k1gNnSc4	ověření
provede	provést	k5eAaPmIp3nS	provést
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
RADIUS	RADIUS	kA	RADIUS
server	server	k1gInSc1	server
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
802.1	[number]	k4	802.1
<g/>
X	X	kA	X
lze	lze	k6eAd1	lze
odstranit	odstranit	k5eAaPmF	odstranit
nedostatky	nedostatek	k1gInPc4	nedostatek
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
pomocí	pomocí	k7c2	pomocí
WEP	WEP	kA	WEP
klíčů	klíč	k1gInPc2	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
IEEE	IEEE	kA	IEEE
802.1	[number]	k4	802.1
<g/>
X.	X.	kA	X.
Šifrování	šifrování	k1gNnSc4	šifrování
komunikace	komunikace	k1gFnSc2	komunikace
pomocí	pomoc	k1gFnPc2	pomoc
statických	statický	k2eAgInPc2d1	statický
WEP	WEP	kA	WEP
klíčů	klíč	k1gInPc2	klíč
(	(	kIx(	(
<g/>
Wired	Wired	k1gMnSc1	Wired
Equivalent	Equivalent	k1gMnSc1	Equivalent
Privacy	Privaca	k1gFnSc2	Privaca
<g/>
)	)	kIx)	)
symetrické	symetrický	k2eAgFnPc1d1	symetrická
šifry	šifra	k1gFnPc1	šifra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ručně	ručně	k6eAd1	ručně
nastaveny	nastavit	k5eAaPmNgInP	nastavit
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
bezdrátového	bezdrátový	k2eAgNnSc2d1	bezdrátové
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nedostatkům	nedostatek	k1gInPc3	nedostatek
v	v	k7c6	v
protokolu	protokol	k1gInSc6	protokol
lze	lze	k6eAd1	lze
zachycením	zachycení	k1gNnSc7	zachycení
specifických	specifický	k2eAgInPc2d1	specifický
rámců	rámec	k1gInPc2	rámec
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
analýzou	analýza	k1gFnSc7	analýza
klíč	klíč	k1gInSc4	klíč
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
získat	získat	k5eAaPmF	získat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
klíčů	klíč	k1gInPc2	klíč
existují	existovat	k5eAaImIp3nP	existovat
specializované	specializovaný	k2eAgInPc1d1	specializovaný
programy	program	k1gInPc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Wired	Wired	k1gMnSc1	Wired
Equivalent	Equivalent	k1gMnSc1	Equivalent
Privacy	Privaca	k1gFnSc2	Privaca
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zpětné	zpětný	k2eAgFnSc3d1	zpětná
kompatibilitě	kompatibilita	k1gFnSc3	kompatibilita
využívá	využívat	k5eAaPmIp3nS	využívat
WPA	WPA	kA	WPA
(	(	kIx(	(
<g/>
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
Protected	Protected	k1gInSc1	Protected
Access	Access	k1gInSc1	Access
<g/>
)	)	kIx)	)
WEP	WEP	kA	WEP
klíče	klíč	k1gInPc1	klíč
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
dynamicky	dynamicky	k6eAd1	dynamicky
bezpečným	bezpečný	k2eAgInSc7d1	bezpečný
způsobem	způsob	k1gInSc7	způsob
měněny	měnit	k5eAaImNgFnP	měnit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
slouží	sloužit	k5eAaImIp3nS	sloužit
speciální	speciální	k2eAgInSc1d1	speciální
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
prosebník	prosebník	k1gMnSc1	prosebník
(	(	kIx(	(
<g/>
suplikant	suplikant	k1gMnSc1	suplikant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
i	i	k8xC	i
starší	starý	k2eAgNnSc1d2	starší
zařízení	zařízení	k1gNnSc1	zařízení
WPA	WPA	kA	WPA
vybavit	vybavit	k5eAaPmF	vybavit
<g/>
.	.	kIx.	.
</s>
<s>
Autentizace	autentizace	k1gFnSc1	autentizace
přístupu	přístup	k1gInSc2	přístup
do	do	k7c2	do
WPA	WPA	kA	WPA
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
prováděno	provádět	k5eAaImNgNnS	provádět
pomocí	pomocí	k7c2	pomocí
PSK	PSK	kA	PSK
(	(	kIx(	(
<g/>
Pre-Shared	Pre-Shared	k1gMnSc1	Pre-Shared
Key	Key	k1gMnSc1	Key
–	–	k?	–
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
používají	používat	k5eAaImIp3nP	používat
stejnou	stejný	k2eAgFnSc4d1	stejná
dostatečně	dostatečně	k6eAd1	dostatečně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
heslovou	heslový	k2eAgFnSc4d1	heslová
frázi	fráze	k1gFnSc4	fráze
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
RADIUS	RADIUS	kA	RADIUS
server	server	k1gInSc1	server
(	(	kIx(	(
<g/>
ověřování	ověřování	k1gNnSc1	ověřování
přihlašovacím	přihlašovací	k2eAgNnSc7d1	přihlašovací
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
heslem	heslo	k1gNnSc7	heslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
Protected	Protected	k1gMnSc1	Protected
Access	Access	k1gInSc1	Access
<g/>
.	.	kIx.	.
</s>
<s>
Novější	nový	k2eAgFnSc1d2	novější
WPA2	WPA2	k1gFnSc1	WPA2
přináší	přinášet	k5eAaImIp3nS	přinášet
kvalitnější	kvalitní	k2eAgNnSc4d2	kvalitnější
šifrování	šifrování	k1gNnSc4	šifrování
(	(	kIx(	(
<g/>
šifra	šifra	k1gFnSc1	šifra
AES	AES	kA	AES
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
větší	veliký	k2eAgInSc4d2	veliký
výpočetní	výpočetní	k2eAgInSc4d1	výpočetní
výkon	výkon	k1gInSc4	výkon
a	a	k8xC	a
proto	proto	k8xC	proto
nelze	lze	k6eNd1	lze
WPA2	WPA2	k1gFnSc1	WPA2
používat	používat	k5eAaImF	používat
na	na	k7c6	na
starších	starý	k2eAgNnPc6d2	starší
zařízeních	zařízení	k1gNnPc6	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
IEEE	IEEE	kA	IEEE
802.11	[number]	k4	802.11
<g/>
i.	i.	k?	i.
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
aplikovat	aplikovat	k5eAaBmF	aplikovat
specializovanou	specializovaný	k2eAgFnSc4d1	specializovaná
barvu	barva	k1gFnSc4	barva
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
a	a	k8xC	a
okenní	okenní	k2eAgFnSc3d1	okenní
fólii	fólie	k1gFnSc3	fólie
na	na	k7c6	na
místnosti	místnost	k1gFnSc6	místnost
či	či	k8xC	či
budovy	budova	k1gFnSc2	budova
k	k	k7c3	k
výraznému	výrazný	k2eAgNnSc3d1	výrazné
oslabení	oslabení	k1gNnSc3	oslabení
bezdrátového	bezdrátový	k2eAgInSc2d1	bezdrátový
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zamezí	zamezit	k5eAaPmIp3nS	zamezit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
signál	signál	k1gInSc1	signál
propagován	propagovat	k5eAaImNgInS	propagovat
mimo	mimo	k7c4	mimo
podnik	podnik	k1gInSc4	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
technika	technika	k1gFnSc1	technika
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
zlepšit	zlepšit	k5eAaPmF	zlepšit
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
hackery	hacker	k1gMnPc4	hacker
složité	složitý	k2eAgInPc1d1	složitý
zachytit	zachytit	k5eAaPmF	zachytit
signál	signál	k1gInSc4	signál
mimo	mimo	k7c4	mimo
regulované	regulovaný	k2eAgFnPc4d1	regulovaná
oblasti	oblast	k1gFnPc4	oblast
podniku	podnik	k1gInSc2	podnik
jakými	jaký	k3yQgInPc7	jaký
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
parkoviště	parkoviště	k1gNnSc4	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
bezpečnostním	bezpečnostní	k2eAgNnPc3d1	bezpečnostní
opatřením	opatření	k1gNnPc3	opatření
jako	jako	k8xS	jako
je	on	k3xPp3gNnPc4	on
šifrování	šifrování	k1gNnPc4	šifrování
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
hackeři	hacker	k1gMnPc1	hacker
stále	stále	k6eAd1	stále
schopni	schopen	k2eAgMnPc1d1	schopen
tyto	tento	k3xDgFnPc4	tento
sítě	síť	k1gFnPc4	síť
prolomit	prolomit	k5eAaPmF	prolomit
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
docílí	docílit	k5eAaPmIp3nP	docílit
pomocí	pomocí	k7c2	pomocí
rozličných	rozličný	k2eAgFnPc2d1	rozličná
technik	technika	k1gFnPc2	technika
a	a	k8xC	a
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
sítě	síť	k1gFnPc4	síť
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgMnPc1d1	domácí
uživatelé	uživatel	k1gMnPc1	uživatel
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejčastějším	častý	k2eAgInSc7d3	nejčastější
způsobem	způsob	k1gInSc7	způsob
konfigurace	konfigurace	k1gFnSc2	konfigurace
přístupových	přístupový	k2eAgNnPc2d1	přístupové
omezení	omezení	k1gNnPc2	omezení
v	v	k7c6	v
přístupových	přístupový	k2eAgInPc6d1	přístupový
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tyt	k2eAgNnSc1d1	tyto
omezení	omezení	k1gNnPc1	omezení
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
šifrování	šifrování	k1gNnSc4	šifrování
a	a	k8xC	a
kontroly	kontrola	k1gFnPc4	kontrola
MAC	Mac	kA	Mac
adresy	adresa	k1gFnSc2	adresa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
zakázat	zakázat	k5eAaPmF	zakázat
vysílání	vysílání	k1gNnSc4	vysílání
ESSID	ESSID	kA	ESSID
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
přístupový	přístupový	k2eAgInSc1d1	přístupový
bod	bod	k1gInSc1	bod
stane	stanout	k5eAaPmIp3nS	stanout
hůře	zle	k6eAd2	zle
detekovatelný	detekovatelný	k2eAgMnSc1d1	detekovatelný
pro	pro	k7c4	pro
neznámého	známý	k2eNgMnSc4d1	neznámý
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Bezdrátové	bezdrátový	k2eAgInPc1d1	bezdrátový
systémy	systém	k1gInPc1	systém
prevence	prevence	k1gFnSc2	prevence
narušení	narušení	k1gNnSc2	narušení
(	(	kIx(	(
<g/>
WIPS	WIPS	kA	WIPS
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použít	k5eAaPmNgInP	použít
k	k	k7c3	k
poskytnutí	poskytnutí	k1gNnSc3	poskytnutí
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
LAN	lano	k1gNnPc2	lano
sítě	síť	k1gFnSc2	síť
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
síťovém	síťový	k2eAgInSc6d1	síťový
modelu	model	k1gInSc6	model
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
komerční	komerční	k2eAgMnPc4d1	komerční
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
<g/>
,	,	kIx,	,
hotspoty	hotspota	k1gFnSc2	hotspota
a	a	k8xC	a
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
organizace	organizace	k1gFnSc2	organizace
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
upřednostňovaným	upřednostňovaný	k2eAgNnSc7d1	upřednostňované
řešením	řešení	k1gNnSc7	řešení
mít	mít	k5eAaImF	mít
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
a	a	k8xC	a
nešifrovanou	šifrovaný	k2eNgFnSc7d1	nešifrovaná
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
naprosto	naprosto	k6eAd1	naprosto
izolovanou	izolovaný	k2eAgFnSc4d1	izolovaná
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
zprvu	zprvu	k6eAd1	zprvu
nemají	mít	k5eNaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
Internetu	Internet	k1gInSc3	Internet
ani	ani	k8xC	ani
k	k	k7c3	k
jakýmkoli	jakýkoli	k3yIgInPc3	jakýkoli
prostředkům	prostředek	k1gInPc3	prostředek
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Komerční	komerční	k2eAgInPc1d1	komerční
poskytovalé	poskytovalý	k2eAgInPc1d1	poskytovalý
obvykle	obvykle	k6eAd1	obvykle
směřují	směřovat	k5eAaImIp3nP	směřovat
veškerý	veškerý	k3xTgInSc4	veškerý
webový	webový	k2eAgInSc4d1	webový
provoz	provoz	k1gInSc4	provoz
do	do	k7c2	do
určeného	určený	k2eAgInSc2d1	určený
portálu	portál	k1gInSc2	portál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
postará	postarat	k5eAaPmIp3nS	postarat
o	o	k7c4	o
autorizaci	autorizace	k1gFnSc4	autorizace
či	či	k8xC	či
poplatek	poplatek	k1gInSc4	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgNnSc7	druhý
řešením	řešení	k1gNnSc7	řešení
je	být	k5eAaImIp3nS	být
požadovat	požadovat	k5eAaImF	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
uživatelé	uživatel	k1gMnPc1	uživatel
pomocí	pomocí	k7c2	pomocí
zabezpečeného	zabezpečený	k2eAgNnSc2d1	zabezpečené
připojení	připojení	k1gNnSc2	připojení
spojili	spojit	k5eAaPmAgMnP	spojit
s	s	k7c7	s
privilegovanou	privilegovaný	k2eAgFnSc7d1	privilegovaná
síti	síť	k1gFnSc6	síť
použitím	použití	k1gNnSc7	použití
VPN	VPN	kA	VPN
<g/>
.	.	kIx.	.
</s>
<s>
Bezdrátové	bezdrátový	k2eAgFnPc1d1	bezdrátová
sítě	síť	k1gFnPc1	síť
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
zabezpečené	zabezpečený	k2eAgFnPc1d1	zabezpečená
než	než	k8xS	než
ty	ten	k3xDgFnPc1	ten
drátové	drátový	k2eAgFnPc1d1	drátová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
množství	množství	k1gNnSc6	množství
kanceláří	kancelář	k1gFnPc2	kancelář
mohou	moct	k5eAaImIp3nP	moct
útočníci	útočník	k1gMnPc1	útočník
snadno	snadno	k6eAd1	snadno
navštívit	navštívit	k5eAaPmF	navštívit
a	a	k8xC	a
připojit	připojit	k5eAaPmF	připojit
svůj	svůj	k3xOyFgInSc4	svůj
počítač	počítač	k1gInSc4	počítač
k	k	k7c3	k
drátové	drátový	k2eAgFnSc3d1	drátová
síti	síť	k1gFnSc3	síť
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získají	získat	k5eAaPmIp3nP	získat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
útočníci	útočník	k1gMnPc1	útočník
získají	získat	k5eAaPmIp3nP	získat
příštup	příštup	k1gInSc4	příštup
k	k	k7c3	k
síti	síť	k1gFnSc3	síť
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
pomocí	pomoc	k1gFnPc2	pomoc
'	'	kIx"	'
<g/>
zadních	zadní	k2eAgNnPc2d1	zadní
vrátek	vrátka	k1gNnPc2	vrátka
<g/>
'	'	kIx"	'
jako	jako	k8xS	jako
například	například	k6eAd1	například
Back	Back	k1gInSc4	Back
Orifice	Orifice	k1gFnSc2	Orifice
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
obecným	obecný	k2eAgInSc7d1	obecný
řešením	řešení	k1gNnSc7	řešení
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
end-to-end	endond	k1gInSc4	end-to-end
šifrování	šifrování	k1gNnSc2	šifrování
s	s	k7c7	s
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
autentizací	autentizace	k1gFnSc7	autentizace
všech	všecek	k3xTgInPc2	všecek
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
neměly	mít	k5eNaImAgInP	mít
být	být	k5eAaImF	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
veřejně	veřejně	k6eAd1	veřejně
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
namítnout	namítnout	k5eAaPmF	namítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Fyzická	fyzický	k2eAgFnSc1d1	fyzická
vrstva	vrstva	k1gFnSc1	vrstva
a	a	k8xC	a
Linková	linkový	k2eAgFnSc1d1	Linková
(	(	kIx(	(
<g/>
spojová	spojový	k2eAgFnSc1d1	spojová
<g/>
)	)	kIx)	)
vrstva	vrstva	k1gFnSc1	vrstva
šifrovací	šifrovací	k2eAgFnSc2d1	šifrovací
metody	metoda	k1gFnSc2	metoda
nejsou	být	k5eNaImIp3nP	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
cenných	cenný	k2eAgNnPc2d1	cenné
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
hesla	heslo	k1gNnPc4	heslo
a	a	k8xC	a
osobní	osobní	k2eAgInPc4d1	osobní
emaily	email	k1gInPc4	email
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
technologie	technologie	k1gFnPc1	technologie
šifrují	šifrovat	k5eAaBmIp3nP	šifrovat
jenom	jenom	k9	jenom
části	část	k1gFnPc1	část
komunikační	komunikační	k2eAgFnSc2d1	komunikační
cesty	cesta	k1gFnSc2	cesta
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
však	však	k9	však
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
sledovat	sledovat	k5eAaImF	sledovat
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
někomu	někdo	k3yInSc3	někdo
podaří	podařit	k5eAaPmIp3nS	podařit
získat	získat	k5eAaPmF	získat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
ethernetové	ethernetový	k2eAgFnSc3d1	ethernetová
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
šifrování	šifrování	k1gNnSc4	šifrování
a	a	k8xC	a
oprávnění	oprávnění	k1gNnSc4	oprávnění
v	v	k7c6	v
aplikační	aplikační	k2eAgFnSc6d1	aplikační
vrstvě	vrstva	k1gFnSc6	vrstva
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
SSL	SSL	kA	SSL
<g/>
,	,	kIx,	,
SSH	SSH	kA	SSH
<g/>
,	,	kIx,	,
GnuPG	GnuPG	k1gFnSc1	GnuPG
<g/>
,	,	kIx,	,
PGP	PGP	kA	PGP
a	a	k8xC	a
podobné	podobný	k2eAgInPc1d1	podobný
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
end-to-end	endonda	k1gFnPc2	end-to-enda
metody	metoda	k1gFnPc1	metoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemusí	muset	k5eNaImIp3nS	muset
pokrýt	pokrýt	k5eAaPmF	pokrýt
celkový	celkový	k2eAgInSc4d1	celkový
provoz	provoz	k1gInSc4	provoz
na	na	k7c6	na
síti	síť	k1gFnSc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
šifrování	šifrování	k1gNnSc6	šifrování
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
routeru	router	k1gInSc2	router
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
VPN	VPN	kA	VPN
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
switch	switch	k1gMnSc1	switch
šifruje	šifrovat	k5eAaBmIp3nS	šifrovat
veškerý	veškerý	k3xTgInSc4	veškerý
provoz	provoz	k1gInSc4	provoz
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
UDP	UDP	kA	UDP
a	a	k8xC	a
DNS	DNS	kA	DNS
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
při	při	k7c6	při
end-to-end	endonda	k1gFnPc2	end-to-enda
šifrování	šifrování	k1gNnSc4	šifrování
každá	každý	k3xTgFnSc1	každý
chráněná	chráněný	k2eAgFnSc1d1	chráněná
služba	služba	k1gFnSc1	služba
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
šifrování	šifrování	k1gNnSc4	šifrování
zapnuto	zapnut	k2eAgNnSc4d1	zapnuto
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
každé	každý	k3xTgNnSc1	každý
připojení	připojení	k1gNnSc1	připojení
zapnuto	zapnout	k5eAaPmNgNnS	zapnout
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
odesílání	odesílání	k1gNnSc4	odesílání
emailů	email	k1gInPc2	email
musí	muset	k5eAaImIp3nS	muset
každý	každý	k3xTgMnSc1	každý
příjemce	příjemce	k1gMnSc1	příjemce
podporovat	podporovat	k5eAaImF	podporovat
metodu	metoda	k1gFnSc4	metoda
šifrování	šifrování	k1gNnSc2	šifrování
<g/>
,	,	kIx,	,
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
správně	správně	k6eAd1	správně
vyměnit	vyměnit	k5eAaPmF	vyměnit
klíče	klíč	k1gInPc4	klíč
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
nabízejí	nabízet	k5eAaImIp3nP	nabízet
podporu	podpora	k1gFnSc4	podpora
protokolu	protokol	k1gInSc2	protokol
https	https	k6eAd1	https
<g/>
,	,	kIx,	,
a	a	k8xC	a
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
ano	ano	k9	ano
<g/>
,	,	kIx,	,
prohlížeč	prohlížeč	k1gMnSc1	prohlížeč
odešle	odeslat	k5eAaPmIp3nS	odeslat
IP	IP	kA	IP
adresy	adresa	k1gFnSc2	adresa
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
prostého	prostý	k2eAgInSc2d1	prostý
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Způsoby	způsob	k1gInPc1	způsob
neoprávněného	oprávněný	k2eNgInSc2d1	neoprávněný
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
linkům	linek	k1gMnPc3	linek
<g/>
,	,	kIx,	,
funkcím	funkce	k1gFnPc3	funkce
a	a	k8xC	a
datům	datum	k1gNnPc3	datum
jsou	být	k5eAaImIp3nP	být
tak	tak	k9	tak
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
samotné	samotný	k2eAgFnPc1d1	samotná
entity	entita	k1gFnPc1	entita
používají	používat	k5eAaImIp3nP	používat
kód	kód	k1gInSc4	kód
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
celý	celý	k2eAgInSc4d1	celý
rozsah	rozsah	k1gInSc4	rozsah
takové	takový	k3xDgFnSc2	takový
hrozby	hrozba	k1gFnSc2	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nějaké	nějaký	k3yIgFnSc2	nějaký
míry	míra	k1gFnSc2	míra
prevence	prevence	k1gFnSc2	prevence
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
módech	mód	k1gInPc6	mód
a	a	k8xC	a
metodách	metoda	k1gFnPc6	metoda
útoku	útok	k1gInSc2	útok
a	a	k8xC	a
relevantních	relevantní	k2eAgInPc6d1	relevantní
způsobech	způsob	k1gInPc6	způsob
potlačení	potlačení	k1gNnSc2	potlačení
aplikovaných	aplikovaný	k2eAgFnPc2d1	aplikovaná
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
každý	každý	k3xTgInSc1	každý
způsob	způsob	k1gInSc1	způsob
zásahu	zásah	k1gInSc2	zásah
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
nové	nový	k2eAgFnPc4d1	nová
příležitosti	příležitost	k1gFnPc4	příležitost
pro	pro	k7c4	pro
ohrožení	ohrožení	k1gNnSc4	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
prevence	prevence	k1gFnSc1	prevence
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
stabilní	stabilní	k2eAgFnSc2d1	stabilní
touhy	touha	k1gFnSc2	touha
pro	pro	k7c4	pro
zlepšení	zlepšení	k1gNnSc4	zlepšení
<g/>
.	.	kIx.	.
</s>
<s>
Popsané	popsaný	k2eAgInPc1d1	popsaný
způsoby	způsob	k1gInPc1	způsob
útoků	útok	k1gInPc2	útok
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
nastínění	nastínění	k1gNnSc4	nastínění
typických	typický	k2eAgFnPc2d1	typická
metod	metoda	k1gFnPc2	metoda
a	a	k8xC	a
scénářů	scénář	k1gInPc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Kompatibilitu	kompatibilita	k1gFnSc4	kompatibilita
zařízení	zařízení	k1gNnSc2	zařízení
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
certifikační	certifikační	k2eAgInSc1d1	certifikační
proces	proces	k1gInSc1	proces
<g/>
;	;	kIx,	;
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tuto	tento	k3xDgFnSc4	tento
certifikaci	certifikace	k1gFnSc4	certifikace
získala	získat	k5eAaPmAgFnS	získat
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
označena	označen	k2eAgFnSc1d1	označena
logem	log	k1gInSc7	log
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
aliance	aliance	k1gFnSc2	aliance
<g/>
.	.	kIx.	.
802.11	[number]	k4	802.11
<g/>
n	n	k0	n
-	-	kIx~	-
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
Draft	draft	k1gInSc1	draft
2.0	[number]	k4	2.0
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
prodeji	prodej	k1gInSc6	prodej
<g/>
)	)	kIx)	)
-	-	kIx~	-
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
TGn	TGn	k1gFnSc2	TGn
sync	sync	k6eAd1	sync
bude	být	k5eAaImBp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
až	až	k9	až
600	[number]	k4	600
Mbit	Mbitum	k1gNnPc2	Mbitum
<g />
.	.	kIx.	.
</s>
<s>
při	při	k7c6	při
4X4	[number]	k4	4X4
MIMO	mimo	k6eAd1	mimo
(	(	kIx(	(
<g/>
4	[number]	k4	4
streamy	stream	k1gInPc4	stream
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
450	[number]	k4	450
Mbit	Mbitum	k1gNnPc2	Mbitum
při	při	k7c6	při
3X3	[number]	k4	3X3
MIMO	mimo	k6eAd1	mimo
(	(	kIx(	(
<g/>
příklad	příklad	k1gInSc1	příklad
implementace	implementace	k1gFnSc2	implementace
<g/>
:	:	kIx,	:
Intel	Intel	kA	Intel
<g/>
®	®	k?	®
WiFi	WiFi	k1gNnSc7	WiFi
Link	Linka	k1gFnPc2	Linka
5300	[number]	k4	5300
Series	Seriesa	k1gFnPc2	Seriesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
300	[number]	k4	300
Mbit	Mbitum	k1gNnPc2	Mbitum
při	při	k7c6	při
2X2	[number]	k4	2X2
MIMO	mimo	k6eAd1	mimo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Intel	Intel	kA	Intel
<g/>
®	®	k?	®
WiFi	WiFi	k1gNnSc1	WiFi
Link	Link	k1gMnSc1	Link
<g />
.	.	kIx.	.
</s>
<s>
5100	[number]	k4	5100
Series	Series	k1gInSc1	Series
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
802.11	[number]	k4	802.11
<g/>
n	n	k0	n
-	-	kIx~	-
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
bude	být	k5eAaImBp3nS	být
zajímat	zajímat	k5eAaImF	zajímat
většinu	většina	k1gFnSc4	většina
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
při	při	k7c6	při
600	[number]	k4	600
<g/>
Mbit	Mbita	k1gFnPc2	Mbita
na	na	k7c6	na
fyzické	fyzický	k2eAgFnSc6d1	fyzická
vrstvě	vrstva	k1gFnSc6	vrstva
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
až	až	k6eAd1	až
do	do	k7c2	do
400	[number]	k4	400
<g/>
Mbit	Mbita	k1gFnPc2	Mbita
na	na	k7c4	na
MAC	Mac	kA	Mac
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
2	[number]	k4	2
-	-	kIx~	-
Layer	Layer	k1gInSc1	Layer
<g/>
2	[number]	k4	2
-	-	kIx~	-
MAC	Mac	kA	Mac
<g/>
)	)	kIx)	)
vrstvě	vrstva	k1gFnSc3	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Praktická	praktický	k2eAgFnSc1d1	praktická
rychlost	rychlost	k1gFnSc1	rychlost
bude	být	k5eAaImBp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Intel	Intel	kA	Intel
<g/>
®	®	k?	®
WiFi	WiF	k1gFnSc2	WiF
Link	Link	k1gInSc1	Link
5100	[number]	k4	5100
v	v	k7c6	v
noteboocích	notebook	k1gInPc6	notebook
běžně	běžně	k6eAd1	běžně
zvládá	zvládat	k5eAaImIp3nS	zvládat
reálné	reálný	k2eAgFnSc3d1	reálná
rychlosti	rychlost	k1gFnSc3	rychlost
nad	nad	k7c4	nad
100	[number]	k4	100
<g/>
Mbit	Mbita	k1gFnPc2	Mbita
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
elektrosmog	elektrosmog	k1gInSc4	elektrosmog
produkovaný	produkovaný	k2eAgInSc4d1	produkovaný
Wi-Fi	Wi-Fi	k1gNnPc7	Wi-Fi
negativně	negativně	k6eAd1	negativně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
některé	některý	k3yIgInPc4	některý
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nárůstu	nárůst	k1gInSc3	nárůst
počtu	počet	k1gInSc2	počet
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
také	také	k6eAd1	také
čím	co	k3yInSc7	co
dál	daleko	k6eAd2	daleko
více	hodně	k6eAd2	hodně
stává	stávat	k5eAaImIp3nS	stávat
oběťmi	oběť	k1gFnPc7	oběť
útočníků	útočník	k1gMnPc2	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
lhostejnosti	lhostejnost	k1gFnSc3	lhostejnost
uživatelů	uživatel	k1gMnPc2	uživatel
WLAN	WLAN	kA	WLAN
sítí	síť	k1gFnPc2	síť
k	k	k7c3	k
zabezpečení	zabezpečení	k1gNnSc3	zabezpečení
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
některé	některý	k3yIgInPc4	některý
"	"	kIx"	"
<g/>
útoky	útok	k1gInPc4	útok
<g/>
"	"	kIx"	"
pouze	pouze	k6eAd1	pouze
náhodné	náhodný	k2eAgNnSc1d1	náhodné
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgNnSc1d1	jiné
však	však	k9	však
účelné	účelný	k2eAgNnSc1d1	účelné
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
do	do	k7c2	do
WLAN	WLAN	kA	WLAN
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
aktivní	aktivní	k2eAgInSc4d1	aktivní
a	a	k8xC	a
pasivní	pasivní	k2eAgInSc4d1	pasivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
pasivního	pasivní	k2eAgInSc2d1	pasivní
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
aktivního	aktivní	k2eAgMnSc2d1	aktivní
<g/>
,	,	kIx,	,
útočník	útočník	k1gMnSc1	útočník
zachycená	zachycený	k2eAgNnPc4d1	zachycené
data	datum	k1gNnPc4	datum
nemodifikuje	modifikovat	k5eNaBmIp3nS	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Pasivní	pasivní	k2eAgInPc1d1	pasivní
útoky	útok	k1gInPc1	útok
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
uvedené	uvedený	k2eAgInPc4d1	uvedený
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nezjistitelné	zjistitelný	k2eNgInPc1d1	nezjistitelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
odposlech	odposlech	k1gInSc4	odposlech
bezdrátových	bezdrátový	k2eAgFnPc2d1	bezdrátová
sítí	síť	k1gFnPc2	síť
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
program	program	k1gInSc1	program
inSSIDer	inSSIDra	k1gFnPc2	inSSIDra
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
starší	starší	k1gMnPc4	starší
verze	verze	k1gFnSc2	verze
pak	pak	k6eAd1	pak
NetStumbler	NetStumbler	k1gInSc4	NetStumbler
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Linuxových	linuxový	k2eAgFnPc6d1	linuxová
distribucích	distribuce	k1gFnPc6	distribuce
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
nástroj	nástroj	k1gInSc1	nástroj
Kismet	kismet	k1gInSc1	kismet
a	a	k8xC	a
Aircrack-ng	Aircrackg	k1gInSc1	Aircrack-ng
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
aplikace	aplikace	k1gFnPc1	aplikace
běží	běžet	k5eAaImIp3nP	běžet
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
na	na	k7c6	na
notebooku	notebook	k1gInSc6	notebook
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
buď	buď	k8xC	buď
nadšenci	nadšenec	k1gMnPc1	nadšenec
nosí	nosit	k5eAaImIp3nP	nosit
po	po	k7c6	po
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
detekují	detekovat	k5eAaImIp3nP	detekovat
volně	volně	k6eAd1	volně
přístupné	přístupný	k2eAgFnPc1d1	přístupná
sítě	síť	k1gFnPc1	síť
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
autem	auto	k1gNnSc7	auto
a	a	k8xC	a
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
poté	poté	k6eAd1	poté
poskytující	poskytující	k2eAgInSc4d1	poskytující
veřejně	veřejně	k6eAd1	veřejně
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
databází	databáze	k1gFnPc2	databáze
s	s	k7c7	s
adresou	adresa	k1gFnSc7	adresa
a	a	k8xC	a
GPS	GPS	kA	GPS
pozicí	pozice	k1gFnSc7	pozice
(	(	kIx(	(
<g/>
NetStumbler	NetStumbler	k1gInSc1	NetStumbler
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
prostými	prostý	k2eAgFnPc7d1	prostá
značkami	značka	k1gFnPc7	značka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Symboly	symbol	k1gInPc1	symbol
užívané	užívaný	k2eAgInPc1d1	užívaný
při	při	k7c6	při
Warchalkingu	Warchalking	k1gInSc6	Warchalking
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
lovení	lovení	k1gNnPc2	lovení
<g/>
"	"	kIx"	"
přístupových	přístupový	k2eAgInPc2d1	přístupový
bodů	bod	k1gInPc2	bod
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Warchalking	Warchalking	k1gInSc1	Warchalking
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
Wardriving	Wardriving	k1gInSc1	Wardriving
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
spíše	spíše	k9	spíše
o	o	k7c4	o
nešťastný	šťastný	k2eNgInSc4d1	nešťastný
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
samotná	samotný	k2eAgFnSc1d1	samotná
detekce	detekce	k1gFnSc1	detekce
AP	ap	kA	ap
není	být	k5eNaImIp3nS	být
agresivní	agresivní	k2eAgNnSc1d1	agresivní
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
žádný	žádný	k3yNgInSc4	žádný
zákeřný	zákeřný	k2eAgInSc4d1	zákeřný
čin	čin	k1gInSc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Access	Access	k1gInSc1	Access
point	pointa	k1gFnPc2	pointa
je	být	k5eAaImIp3nS	být
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vysílat	vysílat	k5eAaImF	vysílat
nebo	nebo	k8xC	nebo
přijímat	přijímat	k5eAaImF	přijímat
data	datum	k1gNnPc4	datum
<g/>
.	.	kIx.	.
</s>
<s>
AP	ap	kA	ap
jsou	být	k5eAaImIp3nP	být
stěžejními	stěžejní	k2eAgInPc7d1	stěžejní
prvky	prvek	k1gInPc7	prvek
pro	pro	k7c4	pro
sítě	síť	k1gFnPc4	síť
WLAN	WLAN	kA	WLAN
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
AP	ap	kA	ap
vysílají	vysílat	k5eAaImIp3nP	vysílat
pomocí	pomocí	k7c2	pomocí
všesměrových	všesměrový	k2eAgFnPc2d1	všesměrová
nebo	nebo	k8xC	nebo
směrových	směrový	k2eAgFnPc2d1	směrová
antén	anténa	k1gFnPc2	anténa
signál	signál	k1gInSc4	signál
do	do	k7c2	do
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
signál	signál	k1gInSc1	signál
je	být	k5eAaImIp3nS	být
přijímán	přijímán	k2eAgInSc1d1	přijímán
AP	ap	kA	ap
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
pro	pro	k7c4	pro
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
příjem	příjem	k1gInSc4	příjem
nutná	nutný	k2eAgFnSc1d1	nutná
anténa	anténa	k1gFnSc1	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
všesměrové	všesměrový	k2eAgFnPc4d1	všesměrová
a	a	k8xC	a
směrové	směrový	k2eAgFnPc4d1	směrová
antény	anténa	k1gFnPc4	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Všesměrové	všesměrový	k2eAgFnPc1d1	všesměrová
antény	anténa	k1gFnPc1	anténa
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
pro	pro	k7c4	pro
pokrytí	pokrytí	k1gNnSc4	pokrytí
velké	velký	k2eAgFnSc2d1	velká
oblasti	oblast	k1gFnSc2	oblast
WiFi	WiF	k1gFnSc2	WiF
signálem	signál	k1gInSc7	signál
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pomocí	pomocí	k7c2	pomocí
směrových	směrový	k2eAgFnPc2d1	směrová
antén	anténa	k1gFnPc2	anténa
můžete	moct	k5eAaImIp2nP	moct
přenášet	přenášet	k5eAaImF	přenášet
WiFi	WiF	k1gMnPc1	WiF
signál	signál	k1gInSc4	signál
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
WiFi	WiFi	k1gNnSc1	WiFi
router	routra	k1gFnPc2	routra
v	v	k7c6	v
sobě	se	k3xPyFc3	se
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
funkci	funkce	k1gFnSc4	funkce
klasického	klasický	k2eAgInSc2d1	klasický
routeru	router	k1gInSc2	router
a	a	k8xC	a
AP.	ap.	kA	ap.
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
takový	takový	k3xDgInSc4	takový
router	router	k1gInSc4	router
vybaven	vybavit	k5eAaPmNgMnS	vybavit
jedním	jeden	k4xCgInSc7	jeden
portem	port	k1gInSc7	port
WAN	WAN	kA	WAN
(	(	kIx(	(
<g/>
Wide	Wid	k1gMnSc2	Wid
Area	area	k1gFnSc1	area
Network	network	k1gInSc1	network
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
několika	několik	k4yIc7	několik
ethernetovými	ethernetový	k2eAgInPc7d1	ethernetový
porty	port	k1gInPc7	port
a	a	k8xC	a
anténou	anténa	k1gFnSc7	anténa
nebo	nebo	k8xC	nebo
anténami	anténa	k1gFnPc7	anténa
pro	pro	k7c4	pro
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
WiFi	WiF	k1gFnSc2	WiF
routeru	router	k1gInSc2	router
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
snadno	snadno	k6eAd1	snadno
vytvořit	vytvořit	k5eAaPmF	vytvořit
svou	svůj	k3xOyFgFnSc4	svůj
domácí	domácí	k2eAgFnSc4d1	domácí
bezdrátovou	bezdrátový	k2eAgFnSc4d1	bezdrátová
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
WiFi	WiFi	k6eAd1	WiFi
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
do	do	k7c2	do
PCI	PCI	kA	PCI
nebo	nebo	k8xC	nebo
do	do	k7c2	do
notebookového	notebookový	k2eAgInSc2d1	notebookový
portu	port	k1gInSc2	port
PCMCIA	PCMCIA	kA	PCMCIA
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
počítače	počítač	k1gInSc2	počítač
nebo	nebo	k8xC	nebo
notebooku	notebook	k1gInSc2	notebook
k	k	k7c3	k
WiFi	WiF	k1gFnSc3	WiF
síti	síť	k1gFnSc3	síť
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
síťová	síťový	k2eAgFnSc1d1	síťová
karta	karta	k1gFnSc1	karta
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
připojení	připojení	k1gNnSc3	připojení
na	na	k7c6	na
LAN	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
notebooků	notebook	k1gInPc2	notebook
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přenosných	přenosný	k2eAgNnPc2d1	přenosné
zařízení	zařízení	k1gNnPc2	zařízení
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc4d1	různý
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
a	a	k8xC	a
PDA	PDA	kA	PDA
mají	mít	k5eAaImIp3nP	mít
zabudován	zabudovat	k5eAaPmNgInS	zabudovat
WiFi	WiFi	k1gNnSc3	WiFi
modul	modul	k1gInSc1	modul
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnPc1	síť
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnPc4	generace
postavené	postavený	k2eAgFnPc4d1	postavená
na	na	k7c4	na
blanket	blanket	k1gInSc4	blanket
technologii	technologie	k1gFnSc4	technologie
přináší	přinášet	k5eAaImIp3nP	přinášet
průlomový	průlomový	k2eAgInSc4d1	průlomový
zlom	zlom	k1gInSc4	zlom
v	v	k7c6	v
plánování	plánování	k1gNnSc6	plánování
a	a	k8xC	a
výstavbě	výstavba	k1gFnSc3	výstavba
WiFi	WiF	k1gFnSc2	WiF
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
všechna	všechen	k3xTgNnPc4	všechen
AP	ap	kA	ap
provozována	provozovat	k5eAaImNgNnP	provozovat
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
kanále	kanál	k1gInSc6	kanál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadné	snadný	k2eAgNnSc1d1	snadné
nalézt	nalézt	k5eAaPmF	nalézt
alespoň	alespoň	k9	alespoň
jeden	jeden	k4xCgInSc4	jeden
volný	volný	k2eAgInSc4d1	volný
kanál	kanál	k1gInSc4	kanál
i	i	k9	i
v	v	k7c6	v
zarušeném	zarušený	k2eAgNnSc6d1	zarušené
ISM	ISM	kA	ISM
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Blanket	blanket	k1gInSc4	blanket
sítě	síť	k1gFnSc2	síť
mají	mít	k5eAaImIp3nP	mít
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
mechanismus	mechanismus	k1gInSc4	mechanismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
brání	bránit	k5eAaImIp3nS	bránit
rušení	rušení	k1gNnSc4	rušení
mezi	mezi	k7c7	mezi
vlastními	vlastní	k2eAgInPc7d1	vlastní
AP	ap	kA	ap
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
provozovány	provozovat	k5eAaImNgFnP	provozovat
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
kanále	kanál	k1gInSc6	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Sítě	síť	k1gFnPc1	síť
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnPc1	generace
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
nároky	nárok	k1gInPc7	nárok
na	na	k7c4	na
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
bezdrátového	bezdrátový	k2eAgNnSc2d1	bezdrátové
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
veškerých	veškerý	k3xTgFnPc2	veškerý
citlivých	citlivý	k2eAgFnPc2d1	citlivá
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Nutným	nutný	k2eAgInSc7d1	nutný
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
hardware	hardware	k1gInSc1	hardware
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
šifrovacích	šifrovací	k2eAgInPc2d1	šifrovací
protokolů	protokol	k1gInPc2	protokol
a	a	k8xC	a
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
degradace	degradace	k1gFnSc2	degradace
přenosových	přenosový	k2eAgInPc2d1	přenosový
parametrů	parametr	k1gInPc2	parametr
a	a	k8xC	a
stability	stabilita	k1gFnSc2	stabilita
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
unikátní	unikátní	k2eAgInSc4d1	unikátní
blanket	blanket	k1gInSc4	blanket
sítě	síť	k1gFnSc2	síť
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
re-asociace	resociace	k1gFnPc4	re-asociace
klientů	klient	k1gMnPc2	klient
i	i	k8xC	i
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
provoz	provoz	k1gInSc4	provoz
i	i	k9	i
nejnáročnějších	náročný	k2eAgFnPc2d3	nejnáročnější
služeb	služba	k1gFnPc2	služba
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
zabezpečením	zabezpečení	k1gNnSc7	zabezpečení
bez	bez	k7c2	bez
výpadků	výpadek	k1gInPc2	výpadek
a	a	k8xC	a
bez	bez	k7c2	bez
snížení	snížení	k1gNnSc2	snížení
provozních	provozní	k2eAgInPc2d1	provozní
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Bezdrátová	bezdrátový	k2eAgFnSc1d1	bezdrátová
síť	síť	k1gFnSc1	síť
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.wi-fi.org	www.wii.org	k1gInSc1	www.wi-fi.org
-	-	kIx~	-
Domácí	domácí	k1gFnSc1	domácí
stránky	stránka	k1gFnSc2	stránka
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
aliance	aliance	k1gFnSc2	aliance
WiFimarketing	WiFimarketing	k1gInSc1	WiFimarketing
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Nezávislé	závislý	k2eNgFnSc2d1	nezávislá
stránky	stránka	k1gFnSc2	stránka
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
WiFi	WiF	k1gFnSc2	WiF
marketingu	marketing	k1gInSc2	marketing
Projekt	projekt	k1gInSc1	projekt
souboru	soubor	k1gInSc2	soubor
parametrů	parametr	k1gInPc2	parametr
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
Wi-Fi	Wi-F	k1gFnSc2	Wi-F
karet	kareta	k1gFnPc2	kareta
pod	pod	k7c7	pod
Linuxem	linux	k1gInSc7	linux
Užitečné	užitečný	k2eAgFnSc2d1	užitečná
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
Wi-Fi	Wi-F	k1gFnSc6	Wi-F
sítích	síť	k1gFnPc6	síť
WDS	WDS	kA	WDS
(	(	kIx(	(
<g/>
Wireless	Wireless	k1gInSc1	Wireless
Distribution	Distribution	k1gInSc1	Distribution
System	Syst	k1gMnSc7	Syst
<g/>
)	)	kIx)	)
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
Jaké	jaký	k3yIgNnSc1	jaký
jsou	být	k5eAaImIp3nP	být
útoky	útok	k1gInPc4	útok
do	do	k7c2	do
WLAN	WLAN	kA	WLAN
a	a	k8xC	a
jak	jak	k6eAd1	jak
jim	on	k3xPp3gMnPc3	on
čelit	čelit	k5eAaImF	čelit
<g/>
?	?	kIx.	?
</s>
<s>
Revoluce	revoluce	k1gFnSc1	revoluce
v	v	k7c6	v
bezdrátech	bezdrát	k1gInPc6	bezdrát
<g/>
!	!	kIx.	!
</s>
<s>
WiFi	WiFi	k6eAd1	WiFi
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
generace	generace	k1gFnPc4	generace
Bezdrátové	bezdrátový	k2eAgFnSc2d1	bezdrátová
sítě	síť	k1gFnSc2	síť
</s>
