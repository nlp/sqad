<s>
Cena	cena	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
směnné	směnný	k2eAgFnSc6d1
hodnotě	hodnota	k1gFnSc6
statku	statek	k1gInSc2
nebo	nebo	k8xC
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Cena	cena	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Cena	cena	k1gFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
směnný	směnný	k2eAgInSc4d1
poměr	poměr	k1gInSc4
mezi	mezi	k7c7
směňovanými	směňovaný	k2eAgInPc7d1
statky	statek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
obvykle	obvykle	k6eAd1
ukazuje	ukazovat	k5eAaImIp3nS
množství	množství	k1gNnSc1
peněz	peníze	k1gInPc2
potřebných	potřebný	k2eAgInPc2d1
k	k	k7c3
uskutečnění	uskutečnění	k1gNnSc3
směny	směna	k1gFnSc2
daného	daný	k2eAgInSc2d1
statku	statek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vyjádřeny	vyjádřit	k5eAaPmNgInP
též	též	k9
v	v	k7c6
ostatních	ostatní	k2eAgInPc6d1
statcích	statek	k1gInPc6
(	(	kIx(
<g/>
např.	např.	kA
1	#num#	k4
kráva	kráva	k1gFnSc1
=	=	kIx~
2	#num#	k4
ovce	ovce	k1gFnSc2
a	a	k8xC
opačně	opačně	k6eAd1
1	#num#	k4
ovce	ovce	k1gFnPc1
=	=	kIx~
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
krávy	kráva	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
dnes	dnes	k6eAd1
se	se	k3xPyFc4
již	již	k9
barterový	barterový	k2eAgInSc1d1
obchod	obchod	k1gInSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
jen	jen	k9
minimálně	minimálně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
cena	cena	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
též	též	k9
zvláštní	zvláštní	k2eAgInPc4d1
druhy	druh	k1gInPc4
odměňování	odměňování	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
mzdu	mzda	k1gFnSc4
(	(	kIx(
<g/>
cena	cena	k1gFnSc1
práce	práce	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kurz	kurz	k1gInSc1
(	(	kIx(
<g/>
cena	cena	k1gFnSc1
cizí	cizí	k2eAgFnSc2d1
měny	měna	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
úrok	úrok	k1gInSc1
(	(	kIx(
<g/>
cena	cena	k1gFnSc1
peněz	peníze	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
pojetí	pojetí	k1gNnSc2
anglické	anglický	k2eAgFnSc2d1
klasické	klasický	k2eAgFnSc2d1
školy	škola	k1gFnSc2
(	(	kIx(
<g/>
David	David	k1gMnSc1
Ricardo	Ricardo	k1gNnSc4
<g/>
,	,	kIx,
Adam	Adam	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Stuart	Stuarta	k1gFnPc2
Mill	Mill	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
statku	statek	k1gInSc2
určená	určený	k2eAgFnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
kolik	kolik	k4yQc1,k4yRc1,k4yIc1
práce	práce	k1gFnSc2
bylo	být	k5eAaImAgNnS
vynaloženo	vynaložen	k2eAgNnSc1d1
k	k	k7c3
jeho	jeho	k3xOp3gFnSc3
výrobě	výroba	k1gFnSc3
–	–	k?
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
výrobky	výrobek	k1gInPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
vytvoření	vytvoření	k1gNnSc1
je	být	k5eAaImIp3nS
pracnější	pracný	k2eAgNnSc1d2
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
tendenci	tendence	k1gFnSc4
být	být	k5eAaImF
dražší	drahý	k2eAgInPc1d2
než	než	k8xS
ty	ten	k3xDgInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
snáze	snadno	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nákladovou	nákladový	k2eAgFnSc4d1
teorii	teorie	k1gFnSc4
hodnoty	hodnota	k1gFnSc2
posléze	posléze	k6eAd1
přejal	přejmout	k5eAaPmAgMnS
i	i	k9
Karl	Karl	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
zní	znět	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
intuitivně	intuitivně	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
chybná	chybný	k2eAgFnSc1d1
a	a	k8xC
nedokáže	dokázat	k5eNaPmIp3nS
vysvětlit	vysvětlit	k5eAaPmF
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
běžných	běžný	k2eAgInPc2d1
jevů	jev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásadní	zásadní	k2eAgFnSc4d1
kritiku	kritika	k1gFnSc4
tohoto	tento	k3xDgInSc2
konceptu	koncept	k1gInSc2
přednesl	přednést	k5eAaPmAgMnS
Carl	Carl	k1gMnSc1
Menger	Menger	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
marginalistické	marginalistický	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
cena	cena	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
ze	z	k7c2
subjektivního	subjektivní	k2eAgInSc2d1
mezního	mezní	k2eAgInSc2d1
užitku	užitek	k1gInSc2
daného	daný	k2eAgInSc2d1
statku	statek	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
určena	určen	k2eAgFnSc1d1
poptávkou	poptávka	k1gFnSc7
a	a	k8xC
nabídkou	nabídka	k1gFnSc7
tohoto	tento	k3xDgInSc2
statku	statek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
se	se	k3xPyFc4
cena	cena	k1gFnSc1
výsledného	výsledný	k2eAgInSc2d1
statku	statek	k1gInSc2
nesčítá	sčítat	k5eNaImIp3nS
z	z	k7c2
cen	cena	k1gFnPc2
nákladů	náklad	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
právě	právě	k9
opačně	opačně	k6eAd1
<g/>
:	:	kIx,
spotřebitel	spotřebitel	k1gMnSc1
ohodnotí	ohodnotit	k5eAaPmIp3nS
penězi	peníze	k1gInPc7
finální	finální	k2eAgFnPc1d1
výrobek	výrobek	k1gInSc4
a	a	k8xC
následně	následně	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
formování	formování	k1gNnSc3
cen	cena	k1gFnPc2
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
výrobě	výroba	k1gFnSc3
tohoto	tento	k3xDgInSc2
statku	statek	k1gInSc2
zapotřebí	zapotřebí	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zmínit	zmínit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
tvorba	tvorba	k1gFnSc1
ceny	cena	k1gFnSc2
neprobíhá	probíhat	k5eNaImIp3nS
obecně	obecně	k6eAd1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
statků	statek	k1gInPc2
(	(	kIx(
<g/>
všechna	všechen	k3xTgFnSc1
voda	voda	k1gFnSc1
vs	vs	k?
<g/>
.	.	kIx.
všechny	všechen	k3xTgInPc4
diamanty	diamant	k1gInPc4
světa	svět	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
konkrétně	konkrétně	k6eAd1
u	u	k7c2
daných	daný	k2eAgInPc2d1
statků	statek	k1gInPc2
na	na	k7c6
daném	daný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
v	v	k7c6
daném	daný	k2eAgInSc6d1
čase	čas	k1gInSc6
(	(	kIx(
<g/>
jednokarátový	jednokarátový	k2eAgInSc4d1
diamant	diamant	k1gInSc4
vs	vs	k?
<g/>
.	.	kIx.
jeden	jeden	k4xCgInSc1
litr	litr	k1gInSc1
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Formálně	formálně	k6eAd1
jsou	být	k5eAaImIp3nP
ceny	cena	k1gFnPc4
stanoveny	stanoven	k2eAgFnPc4d1
prodejci	prodejce	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
je	on	k3xPp3gMnPc4
následně	následně	k6eAd1
upravují	upravovat	k5eAaImIp3nP
vzhledem	vzhled	k1gInSc7
k	k	k7c3
nabídce	nabídka	k1gFnSc3
a	a	k8xC
poptávce	poptávka	k1gFnSc3
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
maximalizovali	maximalizovat	k5eAaBmAgMnP
zisk	zisk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodejci	prodejce	k1gMnPc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nP
o	o	k7c4
co	co	k9
nejvyšší	vysoký	k2eAgFnPc4d3
ceny	cena	k1gFnPc4
<g/>
,	,	kIx,
kupující	kupující	k1gMnPc4
naopak	naopak	k6eAd1
o	o	k7c4
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
nejnižší	nízký	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s>
Zákon	zákon	k1gInSc1
jedné	jeden	k4xCgFnSc2
ceny	cena	k1gFnSc2
</s>
<s>
Zákon	zákon	k1gInSc1
jedné	jeden	k4xCgFnSc2
ceny	cena	k1gFnSc2
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ceny	cena	k1gFnPc4
jednoho	jeden	k4xCgInSc2
statku	statek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
na	na	k7c6
různých	různý	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
po	po	k7c6
započtení	započtení	k1gNnSc6
transakčních	transakční	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
budou	být	k5eAaImBp3nP
mít	mít	k5eAaImF
tendenci	tendence	k1gFnSc4
vyrovnat	vyrovnat	k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
ceny	cena	k1gFnSc2
</s>
<s>
informační	informační	k2eAgFnPc4d1
–	–	k?
Peněžní	peněžní	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
vzniknou	vzniknout	k5eAaPmIp3nP
na	na	k7c6
volném	volný	k2eAgInSc6d1
trhu	trh	k1gInSc6
vyjadřují	vyjadřovat	k5eAaImIp3nP
relativní	relativní	k2eAgFnSc4d1
vzácnost	vzácnost	k1gFnSc4
statků	statek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
vzniklé	vzniklý	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
tedy	tedy	k9
dávají	dávat	k5eAaImIp3nP
výrobcům	výrobce	k1gMnPc3
informace	informace	k1gFnSc2
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
zdroje	zdroj	k1gInPc1
mají	mít	k5eAaImIp3nP
využívat	využívat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
vyráběli	vyrábět	k5eAaImAgMnP
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
nejefektivněji	efektivně	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spotřebitelům	spotřebitel	k1gMnPc3
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
výrobky	výrobek	k1gInPc1
mají	mít	k5eAaImIp3nP
spotřebovávat	spotřebovávat	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
učinili	učinit	k5eAaImAgMnP,k5eAaPmAgMnP
svoji	svůj	k3xOyFgFnSc4
spotřebu	spotřeba	k1gFnSc4
co	co	k9
nejefektivnější	efektivní	k2eAgFnSc1d3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tržní	tržní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
zároveň	zároveň	k6eAd1
reflektuje	reflektovat	k5eAaImIp3nS
změny	změna	k1gFnPc4
v	v	k7c6
ekonomice	ekonomika	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
jednotlivým	jednotlivý	k2eAgMnPc3d1
aktérům	aktér	k1gMnPc3
vůbec	vůbec	k9
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
například	například	k6eAd1
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
vnější	vnější	k2eAgFnSc3d1
změně	změna	k1gFnSc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
neúroda	neúroda	k1gFnSc1
pšenice	pšenice	k1gFnSc2
<g/>
,	,	kIx,
promítne	promítnout	k5eAaPmIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
do	do	k7c2
jeho	jeho	k3xOp3gFnSc2
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
růst	růst	k1gInSc4
vyšle	vyslat	k5eAaPmIp3nS
signál	signál	k1gInSc1
výrobcům	výrobce	k1gMnPc3
i	i	k8xC
spotřebitelům	spotřebitel	k1gMnPc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
používali	používat	k5eAaImAgMnP
méně	málo	k6eAd2
pšenice	pšenice	k1gFnSc2
a	a	k8xC
nahradili	nahradit	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
alternativním	alternativní	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
cena	cena	k1gFnSc1
taktéž	taktéž	k?
informuje	informovat	k5eAaBmIp3nS
spotřebitele	spotřebitel	k1gMnPc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
má	mít	k5eAaImIp3nS
poohlédnout	poohlédnout	k5eAaPmF
po	po	k7c6
levnějším	levný	k2eAgNnSc6d2
zboží	zboží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
motivační	motivační	k2eAgInSc1d1
–	–	k?
Růst	růst	k1gInSc1
ceny	cena	k1gFnSc2
statku	statek	k1gInSc2
se	se	k3xPyFc4
též	též	k9
promítne	promítnout	k5eAaPmIp3nS
do	do	k7c2
růstu	růst	k1gInSc2
zisku	zisk	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
motivuje	motivovat	k5eAaBmIp3nS
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
více	hodně	k6eAd2
vyráběli	vyrábět	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
volném	volný	k2eAgInSc6d1
trhu	trh	k1gInSc6
tak	tak	k6eAd1
umožňuje	umožňovat	k5eAaImIp3nS
růst	růst	k1gInSc4
bohatství	bohatství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
alokační	alokační	k2eAgInSc1d1
–	–	k?
Cenové	cenový	k2eAgInPc1d1
signály	signál	k1gInPc1
motivují	motivovat	k5eAaBmIp3nP
výrobce	výrobce	k1gMnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
realokovali	realokovat	k5eAaPmAgMnP,k5eAaImAgMnP,k5eAaBmAgMnP
(	(	kIx(
<g/>
přemístili	přemístit	k5eAaPmAgMnP
<g/>
)	)	kIx)
výrobní	výrobní	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc1
vedou	vést	k5eAaImIp3nP
výrobce	výrobce	k1gMnSc4
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
alokovali	alokovat	k5eAaImAgMnP
výrobní	výrobní	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
mezi	mezi	k7c4
různá	různý	k2eAgNnPc4d1
použití	použití	k1gNnPc4
efektivně	efektivně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
distribuční	distribuční	k2eAgFnSc1d1
–	–	k?
Distribuční	distribuční	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
ceny	cena	k1gFnSc2
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
nástrojem	nástroj	k1gInSc7
rozdělování	rozdělování	k1gNnSc2
zboží	zboží	k1gNnSc2
mezi	mezi	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zboží	zboží	k1gNnSc1
se	se	k3xPyFc4
rozdělí	rozdělit	k5eAaPmIp3nS
mezi	mezi	k7c4
spotřebitele	spotřebitel	k1gMnPc4
podle	podle	k7c2
jejich	jejich	k3xOp3gFnSc2
ochoty	ochota	k1gFnSc2
platit	platit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
svém	svůj	k3xOyFgInSc6
slavném	slavný	k2eAgInSc6d1
článku	článek	k1gInSc6
Využití	využití	k1gNnSc2
znalostí	znalost	k1gFnPc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
ukázal	ukázat	k5eAaPmAgMnS
F.	F.	kA
A.	A.	kA
Hayek	Hayky	k1gFnPc2
<g/>
,	,	kIx,
že	že	k8xS
ceny	cena	k1gFnPc1
utvářené	utvářený	k2eAgFnPc1d1
na	na	k7c6
volném	volný	k2eAgInSc6d1
trhu	trh	k1gInSc6
jsou	být	k5eAaImIp3nP
klíčovým	klíčový	k2eAgInSc7d1
přenašečem	přenašeč	k1gInSc7
roztroušených	roztroušený	k2eAgFnPc2d1
informací	informace	k1gFnPc2
a	a	k8xC
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
funkce	funkce	k1gFnSc1
nikdy	nikdy	k6eAd1
nemůže	moct	k5eNaImIp3nS
nahradit	nahradit	k5eAaPmF
žádný	žádný	k3yNgMnSc1
centrální	centrální	k2eAgMnSc1d1
plánovač	plánovač	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populárnější	populární	k2eAgFnSc7d2
verzí	verze	k1gFnSc7
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
je	být	k5eAaImIp3nS
Já	já	k3xPp1nSc1
<g/>
,	,	kIx,
tužka	tužka	k1gFnSc1
od	od	k7c2
Leonarda	Leonardo	k1gMnSc2
Reada	Read	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceny	cena	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
nejsou	být	k5eNaImIp3nP
výsledkem	výsledek	k1gInSc7
tržního	tržní	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
jsou	být	k5eAaImIp3nP
stanoveny	stanovit	k5eAaPmNgFnP
státem	stát	k1gInSc7
<g/>
,	,	kIx,
nemohou	moct	k5eNaImIp3nP
plnit	plnit	k5eAaImF
svoje	svůj	k3xOyFgFnPc4
zásadní	zásadní	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
popsané	popsaný	k2eAgFnPc4d1
výše	vysoce	k6eAd2
a	a	k8xC
z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
musí	muset	k5eAaImIp3nS
v	v	k7c6
jakékoliv	jakýkoliv	k3yIgFnSc6
ekonomice	ekonomika	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
netržní	tržní	k2eNgInPc4d1
prvky	prvek	k1gInPc4
(	(	kIx(
<g/>
např.	např.	kA
smíšená	smíšený	k2eAgFnSc1d1
ekonomika	ekonomika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
docházet	docházet	k5eAaImF
k	k	k7c3
neefektivnímu	efektivní	k2eNgNnSc3d1
využívání	využívání	k1gNnSc3
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reálným	reálný	k2eAgInSc7d1
důsledkem	důsledek	k1gInSc7
je	být	k5eAaImIp3nS
potom	potom	k8xC
zaostávání	zaostávání	k1gNnSc1
takových	takový	k3xDgFnPc2
ekonomik	ekonomika	k1gFnPc2
za	za	k7c7
těmi	ten	k3xDgFnPc7
tržními	tržní	k2eAgFnPc7d1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
patrné	patrný	k2eAgNnSc1d1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Marketingový	marketingový	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Cena	cena	k1gFnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgInPc2d3
marketingových	marketingový	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
<g/>
,	,	kIx,
obsažených	obsažený	k2eAgInPc2d1
v	v	k7c6
marketingovém	marketingový	k2eAgInSc6d1
mixu	mix	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jediný	jediný	k2eAgMnSc1d1
totiž	totiž	k9
přímo	přímo	k6eAd1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závisí	záviset	k5eAaImIp3nS
například	například	k6eAd1
na	na	k7c6
platebních	platební	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
,	,	kIx,
struktuře	struktura	k1gFnSc3
trhu	trh	k1gInSc2
<g/>
,	,	kIx,
životním	životní	k2eAgInSc6d1
cyklu	cyklus	k1gInSc6
produktu	produkt	k1gInSc2
resp.	resp.	kA
úvěrových	úvěrový	k2eAgFnPc6d1
podmínkách	podmínka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Způsoby	způsob	k1gInPc1
cenové	cenový	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
</s>
<s>
Nákladově	nákladově	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
</s>
<s>
Jinak	jinak	k6eAd1
také	také	k9
nazývaná	nazývaný	k2eAgFnSc1d1
kalkulace	kalkulace	k1gFnSc1
-	-	kIx~
stanovuje	stanovovat	k5eAaImIp3nS
se	se	k3xPyFc4
součtem	součet	k1gInSc7
nákladů	náklad	k1gInPc2
na	na	k7c4
spotřebované	spotřebovaný	k2eAgFnPc4d1
suroviny	surovina	k1gFnPc4
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgInSc3,k3yIgInSc3,k3yQgInSc3
se	se	k3xPyFc4
připočte	připočíst	k5eAaPmIp3nS
přirážka	přirážka	k1gFnSc1
(	(	kIx(
<g/>
obchodní	obchodní	k2eAgFnSc1d1
marže	marže	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
všech	všecek	k3xTgInPc6
oborech	obor	k1gInPc6
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
vyčíslit	vyčíslit	k5eAaPmF
nákladovost	nákladovost	k1gFnSc4
výrobků	výrobek	k1gInPc2
(	(	kIx(
<g/>
pohostinství	pohostinství	k1gNnSc1
<g/>
,	,	kIx,
oděvnictví	oděvnictví	k1gNnSc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhodou	výhoda	k1gFnSc7
jsou	být	k5eAaImIp3nP
jasné	jasný	k2eAgInPc1d1
vstupní	vstupní	k2eAgInPc1d1
náklady	náklad	k1gInPc1
a	a	k8xC
teoreticky	teoreticky	k6eAd1
neomezená	omezený	k2eNgFnSc1d1
marže	marže	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výhody	výhoda	k1gFnPc1
<g/>
:	:	kIx,
jednoduchost	jednoduchost	k1gFnSc1
<g/>
,	,	kIx,
jasnost	jasnost	k1gFnSc1
<g/>
,	,	kIx,
využití	využití	k1gNnSc1
struktury	struktura	k1gFnSc2
vlastních	vlastní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
,	,	kIx,
záruka	záruka	k1gFnSc1
zisku	zisk	k1gInSc2
u	u	k7c2
každého	každý	k3xTgInSc2
výrobku	výrobek	k1gInSc2
<g/>
,	,	kIx,
výrobce	výrobce	k1gMnSc1
zná	znát	k5eAaImIp3nS
lépe	dobře	k6eAd2
své	svůj	k3xOyFgInPc4
náklady	náklad	k1gInPc4
než	než	k8xS
poptávku	poptávka	k1gFnSc4
<g/>
,	,	kIx,
zdání	zdání	k1gNnSc1
spravedlivosti	spravedlivost	k1gFnSc2
pro	pro	k7c4
prodávajícího	prodávající	k2eAgMnSc4d1
i	i	k8xC
kupujícícho	kupujícícho	k6eAd1
<g/>
,	,	kIx,
podnik	podnik	k1gInSc1
nemusí	muset	k5eNaImIp3nS
na	na	k7c4
změnu	změna	k1gFnSc4
poptávky	poptávka	k1gFnSc2
reagovat	reagovat	k5eAaBmF
změnou	změna	k1gFnSc7
ceny	cena	k1gFnSc2
</s>
<s>
Nevýhody	nevýhoda	k1gFnPc1
<g/>
:	:	kIx,
ignoruje	ignorovat	k5eAaImIp3nS
konkurenci	konkurence	k1gFnSc4
<g/>
,	,	kIx,
nebere	brát	k5eNaImIp3nS
v	v	k7c4
úvahu	úvaha	k1gFnSc4
reálnou	reálný	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
v	v	k7c6
poptávce	poptávka	k1gFnSc6
<g/>
,	,	kIx,
dosažení	dosažení	k1gNnSc4
plánovaného	plánovaný	k2eAgInSc2d1
zisku	zisk	k1gInSc2
závisí	záviset	k5eAaImIp3nS
na	na	k7c4
splnění	splnění	k1gNnSc4
počtu	počet	k1gInSc2
prodaných	prodaný	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
</s>
<s>
Orientovaná	orientovaný	k2eAgNnPc1d1
na	na	k7c4
konkurenci	konkurence	k1gFnSc4
</s>
<s>
Také	také	k9
konkurenční	konkurenční	k2eAgNnSc1d1
-	-	kIx~
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
předpokládaných	předpokládaný	k2eAgFnPc2d1
cen	cena	k1gFnPc2
konkurence	konkurence	k1gFnSc2
resp.	resp.	kA
dominantního	dominantní	k2eAgMnSc4d1
prodejce	prodejce	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
ale	ale	k9
stát	stát	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
výnosy	výnos	k1gInPc1
nebudou	být	k5eNaImBp3nP
dostatečné	dostatečný	k2eAgInPc1d1
a	a	k8xC
nepokryjí	pokrýt	k5eNaPmIp3nP
náklady	náklad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Orientovaná	orientovaný	k2eAgNnPc1d1
na	na	k7c4
poptávku	poptávka	k1gFnSc4
</s>
<s>
Pohyblivá	pohyblivý	k2eAgFnSc1d1
cena	cena	k1gFnSc1
-	-	kIx~
závisí	záviset	k5eAaImIp3nS
na	na	k7c6
ochotě	ochota	k1gFnSc6
kupujícího	kupující	k1gMnSc2
koupit	koupit	k5eAaPmF
produkt	produkt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinak	jinak	k6eAd1
řečeno	říct	k5eAaPmNgNnS
<g/>
:	:	kIx,
čím	co	k3yInSc7,k3yQnSc7,k3yRnSc7
více	hodně	k6eAd2
je	být	k5eAaImIp3nS
spotřebitel	spotřebitel	k1gMnSc1
ochoten	ochoten	k2eAgMnSc1d1
zaplatit	zaplatit	k5eAaPmF
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
se	se	k3xPyFc4
cena	cena	k1gFnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Druhy	druh	k1gInPc1
</s>
<s>
dumpingová	dumpingový	k2eAgFnSc1d1
–	–	k?
nepokrývá	pokrývat	k5eNaImIp3nS
náklady	náklad	k1gInPc4
<g/>
,	,	kIx,
výroba	výroba	k1gFnSc1
je	být	k5eAaImIp3nS
dotovaná	dotovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Nastavuje	nastavovat	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c7
účelem	účel	k1gInSc7
likvidace	likvidace	k1gFnSc2
konkurence	konkurence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pod	pod	k7c7
cenou	cena	k1gFnSc7
prodáme	prodat	k5eAaPmIp1nP
základní	základní	k2eAgInSc4d1
výrobek	výrobek	k1gInSc4
<g/>
,	,	kIx,
službu	služba	k1gFnSc4
atd.	atd.	kA
a	a	k8xC
spoléháme	spoléhat	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
vyděláme	vydělat	k5eAaPmIp1nP
na	na	k7c6
drahých	drahý	k2eAgInPc6d1
doplňcích	doplněk	k1gInPc6
<g/>
,	,	kIx,
náhradních	náhradní	k2eAgNnPc6d1
dílech	dílo	k1gNnPc6
<g/>
,	,	kIx,
servisu	servis	k1gInSc6
<g/>
,	,	kIx,
službách	služba	k1gFnPc6
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
konkursní	konkursní	k2eAgInSc1d1
–	–	k?
používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
společnost	společnost	k1gFnSc1
v	v	k7c6
likvidaci	likvidace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správce	správce	k1gMnSc1
musí	muset	k5eAaImIp3nS
prodat	prodat	k5eAaPmF
majetek	majetek	k1gInSc4
společnosti	společnost	k1gFnSc2
co	co	k9
nejrychleji	rychle	k6eAd3
za	za	k7c4
co	co	k9
nejvýhodnější	výhodný	k2eAgFnSc4d3
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
kapitálu	kapitál	k1gInSc2
–	–	k?
jde	jít	k5eAaImIp3nS
o	o	k7c4
výdaj	výdaj	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
musí	muset	k5eAaImIp3nS
podnik	podnik	k1gInSc1
zaplatit	zaplatit	k5eAaPmF
za	za	k7c4
získání	získání	k1gNnSc4
různých	různý	k2eAgFnPc2d1
forem	forma	k1gFnPc2
kapitálu	kapitál	k1gInSc2
<g/>
,	,	kIx,
použitých	použitý	k2eAgInPc2d1
při	při	k7c6
financování	financování	k1gNnSc6
investic	investice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyjadřuje	vyjadřovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
procentech	procento	k1gNnPc6
z	z	k7c2
hodnoty	hodnota	k1gFnSc2
vloženého	vložený	k2eAgInSc2d1
kapitálu	kapitál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
historická	historický	k2eAgFnSc1d1
–	–	k?
je	být	k5eAaImIp3nS
v	v	k7c6
účetnictví	účetnictví	k1gNnSc6
označení	označení	k1gNnSc2
pro	pro	k7c4
ocenění	ocenění	k1gNnSc4
složek	složka	k1gFnPc2
majetku	majetek	k1gInSc2
nebo	nebo	k8xC
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Použije	použít	k5eAaPmIp3nS
se	se	k3xPyFc4
zásadně	zásadně	k6eAd1
skutečná	skutečný	k2eAgFnSc1d1
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
pořízením	pořízení	k1gNnSc7
majetku	majetek	k1gInSc2
nebo	nebo	k8xC
zdrojů	zdroj	k1gInPc2
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
nich	on	k3xPp3gInPc6
poprvé	poprvé	k6eAd1
účtovalo	účtovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
aukční	aukční	k2eAgFnSc1d1
–	–	k?
jde	jít	k5eAaImIp3nS
o	o	k7c4
označení	označení	k1gNnSc4
ceny	cena	k1gFnSc2
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
bylo	být	k5eAaImAgNnS
zboží	zboží	k1gNnSc1
vydraženo	vydražit	k5eAaPmNgNnS
na	na	k7c6
aukci	aukce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
burzovní	burzovní	k2eAgFnSc1d1
–	–	k?
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
obchodovalo	obchodovat	k5eAaImAgNnS
určitým	určitý	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
nebo	nebo	k8xC
cenným	cenný	k2eAgInSc7d1
papírem	papír	k1gInSc7
v	v	k7c4
daný	daný	k2eAgInSc4d1
den	den	k1gInSc4
na	na	k7c6
určité	určitý	k2eAgFnSc6d1
burze	burza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
kilogramová	kilogramový	k2eAgFnSc1d1
–	–	k?
udává	udávat	k5eAaImIp3nS
se	se	k3xPyFc4
cenou	cena	k1gFnSc7
jednoho	jeden	k4xCgMnSc4
kg	kg	kA
určitého	určitý	k2eAgInSc2d1
výrobku	výrobek	k1gInSc2
nebo	nebo	k8xC
skupiny	skupina	k1gFnSc2
výrobků	výrobek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
práce	práce	k1gFnPc1
–	–	k?
jsou	být	k5eAaImIp3nP
náklady	náklad	k1gInPc1
firem	firma	k1gFnPc2
spojené	spojený	k2eAgFnSc2d1
se	se	k3xPyFc4
získáním	získání	k1gNnSc7
služeb	služba	k1gFnPc2
výrobního	výrobní	k2eAgInSc2d1
faktoru	faktor	k1gInSc2
práce	práce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
reprezentována	reprezentovat	k5eAaImNgFnS
mzdou	mzda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
pořízení	pořízení	k1gNnSc1
–	–	k?
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
byl	být	k5eAaImAgInS
pořízen	pořízen	k2eAgInSc1d1
majetek	majetek	k1gInSc1
<g/>
,	,	kIx,
bez	bez	k7c2
vedlejších	vedlejší	k2eAgInPc2d1
pořizovacích	pořizovací	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vyčíslena	vyčíslen	k2eAgFnSc1d1
<g/>
)	)	kIx)
DPH	DPH	kA
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
odečtena	odečíst	k5eAaPmNgFnS
na	na	k7c6
vstupu	vstup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oceňují	oceňovat	k5eAaImIp3nP
se	se	k3xPyFc4
tak	tak	k6eAd1
pohledávky	pohledávka	k1gFnSc2
<g/>
,	,	kIx,
krátkodobý	krátkodobý	k2eAgInSc4d1
finanční	finanční	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
finanční	finanční	k2eAgFnPc4d1
investice	investice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
pořizovací	pořizovací	k2eAgFnSc1d1
–	–	k?
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
byl	být	k5eAaImAgInS
pořízen	pořízen	k2eAgInSc1d1
majetek	majetek	k1gInSc1
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
cenu	cena	k1gFnSc4
pořízení	pořízení	k1gNnSc1
+	+	kIx~
vedlejší	vedlejší	k2eAgInPc1d1
pořizovací	pořizovací	k2eAgInPc1d1
náklady	náklad	k1gInPc1
(	(	kIx(
<g/>
pojištění	pojištění	k1gNnSc1
<g/>
,	,	kIx,
clo	clo	k1gNnSc1
<g/>
,	,	kIx,
dopravné	dopravné	k1gNnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oceňují	oceňovat	k5eAaImIp3nP
se	se	k3xPyFc4
tak	tak	k6eAd1
zásoby	zásoba	k1gFnSc2
<g/>
,	,	kIx,
hmotný	hmotný	k2eAgInSc4d1
a	a	k8xC
nehmotný	hmotný	k2eNgInSc4d1
dlouhodobý	dlouhodobý	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
reprodukční	reprodukční	k2eAgInSc1d1
–	–	k?
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
pro	pro	k7c4
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
by	by	kYmCp3nS
byl	být	k5eAaImAgMnS
majetek	majetek	k1gInSc4
pořízen	pořízen	k2eAgInSc4d1
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gNnSc6
účtuje	účtovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
ocenění	ocenění	k1gNnSc3
dlouhodobého	dlouhodobý	k2eAgInSc2d1
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
její	její	k3xOp3gNnSc4
stanovení	stanovení	k1gNnSc4
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc1d1
odborný	odborný	k2eAgInSc1d1
odhad	odhad	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
souboru	soubor	k1gInSc2
majetku	majetek	k1gInSc2
ocenění	ocenění	k1gNnSc6
znalcem	znalec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
smluvní	smluvní	k2eAgFnSc1d1
–	–	k?
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
sjednaná	sjednaný	k2eAgFnSc1d1
v	v	k7c6
kupní	kupní	k2eAgFnSc6d1
nebo	nebo	k8xC
jiné	jiný	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
nebo	nebo	k8xC
objednávce	objednávka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souvisí	souviset	k5eAaImIp3nS
i	i	k9
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
smluvními	smluvní	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
<g/>
,	,	kIx,
např.	např.	kA
dodací	dodací	k2eAgFnSc7d1
lhůtou	lhůta	k1gFnSc7
<g/>
,	,	kIx,
platebními	platební	k2eAgFnPc7d1
podmínkami	podmínka	k1gFnPc7
<g/>
,	,	kIx,
způsobem	způsob	k1gInSc7
přepravy	přeprava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
světová	světový	k2eAgFnSc1d1
–	–	k?
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
zboží	zboží	k1gNnSc2
na	na	k7c6
rozhodujícím	rozhodující	k2eAgInSc6d1
trhu	trh	k1gInSc6
rozhodujícího	rozhodující	k2eAgMnSc2d1
dodavatele	dodavatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typická	typický	k2eAgFnSc1d1
pro	pro	k7c4
burzovní	burzovní	k2eAgNnSc4d1
zboží	zboží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
základem	základ	k1gInSc7
pro	pro	k7c4
stanovení	stanovení	k1gNnSc4
ceny	cena	k1gFnSc2
určitých	určitý	k2eAgFnPc2d1
komodit	komodita	k1gFnPc2
mimo	mimo	k7c4
burzu	burza	k1gFnSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Většina	většina	k1gFnSc1
průmyslového	průmyslový	k2eAgNnSc2d1
zboží	zboží	k1gNnSc2
nemá	mít	k5eNaImIp3nS
světovou	světový	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
tržní	tržní	k2eAgInSc1d1
–	–	k?
je	být	k5eAaImIp3nS
cena	cena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
vyrovnanému	vyrovnaný	k2eAgInSc3d1
vztahu	vztah	k1gInSc3
mezi	mezi	k7c7
nabídkou	nabídka	k1gFnSc7
a	a	k8xC
poptávkou	poptávka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
deformována	deformován	k2eAgFnSc1d1
není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
tvořena	tvořit	k5eAaImNgFnS
za	za	k7c2
podmínek	podmínka	k1gFnPc2
volné	volný	k2eAgFnSc2d1
konkurence	konkurence	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
např.	např.	kA
vlivem	vlivem	k7c2
monopolu	monopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viz	vidět	k5eAaImRp2nS
též	též	k9
cena	cena	k1gFnSc1
obvyklá	obvyklý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
vnitropodniková	vnitropodnikový	k2eAgFnSc1d1
–	–	k?
jde	jít	k5eAaImIp3nS
o	o	k7c4
způsob	způsob	k1gInSc4
ocenění	ocenění	k1gNnSc4
výkonu	výkon	k1gInSc6
jednoho	jeden	k4xCgNnSc2
střediska	středisko	k1gNnSc2
jinému	jiný	k2eAgNnSc3d1
středisku	středisko	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
nákupní	nákupní	k2eAgFnSc1d1
–	–	k?
cena	cena	k1gFnSc1
za	za	k7c4
které	který	k3yQgNnSc4,k3yRgNnSc4,k3yIgNnSc4
zboží	zboží	k1gNnSc4
nakoupil	nakoupit	k5eAaPmAgMnS
obchodník	obchodník	k1gMnSc1
od	od	k7c2
dodavatelů	dodavatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
připočtení	připočtení	k1gNnSc6
obchodníkovy	obchodníkův	k2eAgFnSc2d1
marže	marže	k1gFnSc2
vznikne	vzniknout	k5eAaPmIp3nS
cena	cena	k1gFnSc1
maloobchodní	maloobchodní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
velkoobchodní	velkoobchodní	k2eAgFnPc4d1
–	–	k?
Zkráceně	zkráceně	k6eAd1
VOC	VOC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
zboží	zboží	k1gNnSc2
pro	pro	k7c4
velkoodběratele	velkoodběratel	k1gMnPc4
<g/>
,	,	kIx,
distributory	distributor	k1gMnPc4
<g/>
,	,	kIx,
překupníky	překupník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
oceněné	oceněný	k2eAgNnSc1d1
zboží	zboží	k1gNnSc1
je	být	k5eAaImIp3nS
vydáváno	vydávat	k5eAaImNgNnS,k5eAaPmNgNnS
přímo	přímo	k6eAd1
z	z	k7c2
továren	továrna	k1gFnPc2
nebo	nebo	k8xC
podnikových	podnikový	k2eAgInPc2d1
skladů	sklad	k1gInPc2
a	a	k8xC
čeká	čekat	k5eAaImIp3nS
jej	on	k3xPp3gMnSc4
ještě	ještě	k9
cesta	cesta	k1gFnSc1
na	na	k7c4
pulty	pult	k1gInPc4
obchodů	obchod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
maloobchodní	maloobchodní	k2eAgInSc1d1
–	–	k?
Zkráceně	zkráceně	k6eAd1
MOC	moc	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
pro	pro	k7c4
odběr	odběr	k1gInSc4
koncovými	koncový	k2eAgMnPc7d1
zákazníky	zákazník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
do	do	k7c2
ní	on	k3xPp3gFnSc2
započtena	započten	k2eAgFnSc1d1
cena	cena	k1gFnSc1
přepravy	přeprava	k1gFnSc2
z	z	k7c2
továren	továrna	k1gFnPc2
<g/>
/	/	kIx~
<g/>
podnikových	podnikový	k2eAgInPc2d1
skladů	sklad	k1gInPc2
a	a	k8xC
marže	marže	k1gFnSc1
obchodníka	obchodník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
maloobchodní	maloobchodní	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
pojit	pojit	k5eAaImF
další	další	k2eAgFnPc4d1
povinné	povinný	k2eAgFnPc4d1
položky	položka	k1gFnPc4
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
např.	např.	kA
autorský	autorský	k2eAgInSc1d1
poplatek	poplatek	k1gInSc1
nebo	nebo	k8xC
poplatek	poplatek	k1gInSc1
za	za	k7c4
elektroodpad	elektroodpad	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
proporcionální	proporcionální	k2eAgFnSc1d1
k	k	k7c3
velikosti	velikost	k1gFnSc3
výrobku	výrobek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
kie	kie	k?
<g/>
.	.	kIx.
<g/>
vse	vse	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Regulace	regulace	k1gFnSc1
cen	cena	k1gFnPc2
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
cenách	cena	k1gFnPc6
(	(	kIx(
<g/>
Cenový	cenový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Cenová	cenový	k2eAgFnSc1d1
diskriminace	diskriminace	k1gFnSc1
</s>
<s>
Cenová	cenový	k2eAgFnSc1d1
elasticita	elasticita	k1gFnSc1
poptávky	poptávka	k1gFnSc2
</s>
<s>
Cenová	cenový	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
</s>
<s>
Cenový	cenový	k2eAgInSc1d1
index	index	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Téma	téma	k1gNnSc1
Cena	cena	k1gFnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
cena	cena	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mikroekonomie	mikroekonomie	k1gFnPc1
Hlavní	hlavní	k2eAgFnPc1d1
témata	téma	k1gNnPc4
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
</s>
<s>
Dokonalá	dokonalý	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
•	•	k?
Nedokonalá	dokonalý	k2eNgFnSc1d1
konkurence	konkurence	k1gFnSc1
•	•	k?
Monopolistická	monopolistický	k2eAgFnSc1d1
konkurence	konkurence	k1gFnSc1
Náklady	náklad	k1gInPc1
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
•	•	k?
Mezní	mezní	k2eAgFnPc1d1
•	•	k?
Obětované	obětovaný	k2eAgFnPc1d1
příležitosti	příležitost	k1gFnPc1
•	•	k?
Sociální	sociální	k2eAgInSc4d1
•	•	k?
Utopené	utopený	k2eAgNnSc1d1
•	•	k?
Transakční	transakční	k2eAgInSc1d1
•	•	k?
Fixní	fixní	k2eAgInSc1d1
•	•	k?
Variabilní	variabilní	k2eAgInSc4d1
•	•	k?
Celkové	celkový	k2eAgNnSc1d1
Struktura	struktura	k1gFnSc1
trhu	trh	k1gInSc6
</s>
<s>
Monopol	monopol	k1gInSc1
•	•	k?
Monopson	Monopson	k1gInSc1
•	•	k?
Oligopol	Oligopol	k1gInSc1
(	(	kIx(
<g/>
duopol	duopol	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Oligopson	Oligopson	k1gInSc1
•	•	k?
Přirozený	přirozený	k2eAgInSc1d1
monopol	monopol	k1gInSc1
•	•	k?
Koncern	koncern	k1gInSc1
(	(	kIx(
<g/>
holding	holding	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Trust	trust	k1gInSc1
•	•	k?
Kartel	kartel	k1gInSc1
•	•	k?
Syndikát	syndikát	k1gInSc1
•	•	k?
Konsorcium	konsorcium	k1gNnSc1
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Agregace	agregace	k1gFnSc1
•	•	k?
Rozpočet	rozpočet	k1gInSc1
•	•	k?
Teorie	teorie	k1gFnSc2
spotřebitele	spotřebitel	k1gMnSc2
•	•	k?
Konvexnost	konvexnost	k1gFnSc4
•	•	k?
Nekonvexnost	Nekonvexnost	k1gFnSc1
•	•	k?
Analýza	analýza	k1gFnSc1
nákladů	náklad	k1gInPc2
a	a	k8xC
přínosů	přínos	k1gInPc2
•	•	k?
Náklady	náklad	k1gInPc1
mrtvé	mrtvý	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
•	•	k?
Distribuce	distribuce	k1gFnSc2
•	•	k?
Úspory	úspora	k1gFnSc2
z	z	k7c2
rozsahu	rozsah	k1gInSc2
•	•	k?
Úspory	úspora	k1gFnSc2
z	z	k7c2
prostoru	prostor	k1gInSc2
•	•	k?
Elasticita	elasticita	k1gFnSc1
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
•	•	k?
Obchod	obchod	k1gInSc1
•	•	k?
Externalita	externalita	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
firmy	firma	k1gFnSc2
•	•	k?
Statek	statek	k1gInSc1
•	•	k?
Služba	služba	k1gFnSc1
•	•	k?
Rodinná	rodinný	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Křivka	křivka	k1gFnSc1
příjmu	příjem	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
spotřeby	spotřeba	k1gFnSc2
•	•	k?
Informace	informace	k1gFnPc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
•	•	k?
Indiferenční	Indiferenční	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Mezičasová	Mezičasový	k2eAgFnSc1d1
volba	volba	k1gFnSc1
•	•	k?
Trh	trh	k1gInSc1
(	(	kIx(
<g/>
ekonomie	ekonomie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Selhání	selhání	k1gNnSc1
trhu	trh	k1gInSc2
•	•	k?
Paretovo	Paretův	k2eAgNnSc1d1
optimum	optimum	k1gNnSc1
•	•	k?
Preference	preference	k1gFnSc1
•	•	k?
Cena	cena	k1gFnSc1
•	•	k?
Produkce	produkce	k1gFnSc1
•	•	k?
Zisk	zisk	k1gInSc1
•	•	k?
Veřejný	veřejný	k2eAgInSc1d1
statek	statek	k1gInSc1
•	•	k?
Přídělový	přídělový	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Renta	renta	k1gFnSc1
•	•	k?
Averze	averze	k1gFnSc2
k	k	k7c3
riziku	riziko	k1gNnSc3
•	•	k?
Vzácnost	vzácnost	k1gFnSc1
•	•	k?
Nedostatek	nedostatek	k1gInSc1
•	•	k?
Substitut	substitut	k1gInSc1
•	•	k?
Substitutuční	Substitutuční	k2eAgInSc1d1
efekt	efekt	k1gInSc1
•	•	k?
Přebytek	přebytek	k1gInSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
volba	volba	k1gFnSc1
•	•	k?
Nabídka	nabídka	k1gFnSc1
a	a	k8xC
poptávka	poptávka	k1gFnSc1
•	•	k?
Nejistota	nejistota	k1gFnSc1
•	•	k?
Užitek	užitek	k1gInSc1
<g/>
(	(	kIx(
Očekávaný	očekávaný	k2eAgInSc1d1
<g/>
,	,	kIx,
Mezní	mezní	k2eAgInSc1d1
<g/>
)	)	kIx)
•	•	k?
Mzda	mzda	k1gFnSc1
</s>
<s>
Vedlejší	vedlejší	k2eAgNnPc1d1
témata	téma	k1gNnPc1
</s>
<s>
Behaviorální	behaviorální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
rozhodování	rozhodování	k1gNnSc2
•	•	k?
Ekonometrie	Ekonometrie	k1gFnSc2
•	•	k?
Evoluční	evoluční	k2eAgInSc4d1
přístup	přístup	k1gInSc4
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
•	•	k?
Eperimentální	Eperimentální	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
•	•	k?
Teorie	teorie	k1gFnSc1
her	hra	k1gFnPc2
•	•	k?
Institucionální	institucionální	k2eAgFnSc2d1
ekonomie	ekonomie	k1gFnSc2
•	•	k?
Ekonomie	ekonomie	k1gFnSc2
práce	práce	k1gFnSc2
•	•	k?
Právo	právo	k1gNnSc1
•	•	k?
Operační	operační	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
•	•	k?
Optimalizace	optimalizace	k1gFnSc1
•	•	k?
Blahobyt	blahobyt	k1gInSc1
•	•	k?
Regulace	regulace	k1gFnSc1
cen	cena	k1gFnPc2
•	•	k?
Ekonomika	ekonomik	k1gMnSc2
Robinsona	Robinson	k1gMnSc2
Crusoe	Crusoe	k1gNnSc2
•	•	k?
Engelova	Engelův	k2eAgFnSc1d1
křivka	křivka	k1gFnSc1
•	•	k?
Hranice	hranice	k1gFnSc2
produkčních	produkční	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
•	•	k?
Zákon	zákon	k1gInSc1
klesajících	klesající	k2eAgInPc2d1
výnosů	výnos	k1gInPc2
•	•	k?
Udržitelný	udržitelný	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
K	k	k7c3
vidění	vidění	k1gNnSc3
</s>
<s>
Ekonomie	ekonomie	k1gFnSc1
•	•	k?
Makroekonomie	makroekonomie	k1gFnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
ekonomie	ekonomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4047097-0	4047097-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
1327	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85106622	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85106622	#num#	k4
</s>
