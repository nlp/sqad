<s>
Krakatit	Krakatit	k1gInSc1	Krakatit
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
režisérem	režisér	k1gMnSc7	režisér
Otakarem	Otakar	k1gMnSc7	Otakar
Vávrou	Vávra	k1gMnSc7	Vávra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
filmu	film	k1gInSc2	film
–	–	k?	–
inženýr	inženýr	k1gMnSc1	inženýr
Prokop	Prokop	k1gMnSc1	Prokop
(	(	kIx(	(
<g/>
Karel	Karel	k1gMnSc1	Karel
Höger	Höger	k1gMnSc1	Höger
<g/>
)	)	kIx)	)
vynalezne	vynaleznout	k5eAaPmIp3nS	vynaleznout
výbušninu	výbušnina	k1gFnSc4	výbušnina
neobyčejné	obyčejný	k2eNgFnSc2d1	neobyčejná
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
začne	začít	k5eAaPmIp3nS	začít
zajímat	zajímat	k5eAaImF	zajímat
představitel	představitel	k1gMnSc1	představitel
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
chce	chtít	k5eAaImIp3nS	chtít
nově	nově	k6eAd1	nově
objevenou	objevený	k2eAgFnSc4d1	objevená
trhavinu	trhavina	k1gFnSc4	trhavina
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Inženýra	inženýr	k1gMnSc4	inženýr
pozve	pozvat	k5eAaPmIp3nS	pozvat
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
a	a	k8xC	a
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
objevu	objev	k1gInSc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
však	však	k9	však
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yInSc3	co
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
zneužití	zneužití	k1gNnSc2	zneužití
vědy	věda	k1gFnSc2	věda
proti	proti	k7c3	proti
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
natočil	natočit	k5eAaBmAgMnS	natočit
Otakar	Otakar	k1gMnSc1	Otakar
Vávra	Vávra	k1gMnSc1	Vávra
dobově	dobově	k6eAd1	dobově
upravený	upravený	k2eAgInSc4d1	upravený
remake	remake	k1gInSc4	remake
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
filmu	film	k1gInSc2	film
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Temné	temný	k2eAgNnSc1d1	temné
slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
