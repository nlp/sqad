<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzitaUniversita	univerzitaUniversita	k1gFnSc1
Jana	Jan	k1gMnSc2
EvangelistyPurkyně	EvangelistyPurkyně	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1
universita	universita	k1gFnSc1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
latinsky	latinsky	k6eAd1
Universitas	Universitas	k1gMnSc1
Masarykiana	Masarykian	k1gMnSc2
Logo	logo	k1gNnSc4
Zkratka	zkratka	k1gFnSc1
</s>
<s>
MU	MU	kA
<g/>
;	;	kIx,
někdy	někdy	k6eAd1
též	též	k9
Muni	Muni	k?
či	či	k8xC
MUNI	MUNI	k?
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
Rok	rok	k1gInSc1
založení	založení	k1gNnSc2
</s>
<s>
1919	#num#	k4
Typ	typ	k1gInSc1
školy	škola	k1gFnSc2
</s>
<s>
veřejná	veřejný	k2eAgFnSc1d1
Vedení	vedení	k1gNnSc1
Rektor	rektor	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
prof.	prof.	kA
MUDr.	MUDr.	kA
Martin	Martin	k1gMnSc1
Bareš	Bareš	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Kvestor	kvestor	k1gMnSc1
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Mgr.	Mgr.	kA
Marta	Marta	k1gFnSc1
Valešová	Valešová	k1gFnSc1
<g/>
,	,	kIx,
MBA	MBA	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Kancléř	kancléř	k1gMnSc1
</s>
<s>
Marián	Marián	k1gMnSc1
Kišš	Kišš	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
internacionalizaci	internacionalizace	k1gFnSc4
</s>
<s>
doc.	doc.	kA
PhDr.	PhDr.	kA
Břetislav	Břetislav	k1gMnSc1
Dančák	Dančák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
vzdělávání	vzdělávání	k1gNnSc4
a	a	k8xC
kvalitu	kvalita	k1gFnSc4
</s>
<s>
Mgr.	Mgr.	kA
Michal	Michal	k1gMnSc1
Bulant	Bulant	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
záležitosti	záležitost	k1gFnPc4
studentů	student	k1gMnPc2
a	a	k8xC
absolventů	absolvent	k1gMnPc2
</s>
<s>
doc.	doc.	kA
PhDr.	PhDr.	kA
Mgr.	Mgr.	kA
Simona	Simona	k1gFnSc1
Koryčánková	Koryčánková	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
a	a	k8xC
doktorské	doktorský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
</s>
<s>
prof.	prof.	kA
RNDr.	RNDr.	kA
Šárka	Šárka	k1gFnSc1
Pospíšilová	Pospíšilová	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
personální	personální	k2eAgFnPc4d1
a	a	k8xC
akademické	akademický	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
</s>
<s>
prof.	prof.	kA
PhDr.	PhDr.	kA
Jiří	Jiří	k1gMnSc1
Hanuš	Hanuš	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
legislativu	legislativa	k1gFnSc4
a	a	k8xC
informační	informační	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
</s>
<s>
doc.	doc.	kA
JUDr.	JUDr.	kA
Radim	Radim	k1gMnSc1
Polčák	Polčák	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Prorektor	prorektor	k1gMnSc1
pro	pro	k7c4
rozvoj	rozvoj	k1gInSc4
</s>
<s>
doc.	doc.	kA
Ing.	ing.	kA
Martin	Martin	k1gMnSc1
Kvizda	Kvizda	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Předseda	předseda	k1gMnSc1
akademického	akademický	k2eAgInSc2d1
senátu	senát	k1gInSc2
</s>
<s>
Mgr.	Mgr.	kA
Josef	Josef	k1gMnSc1
Menšík	Menšík	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
Počty	počet	k1gInPc1
akademiků	akademik	k1gMnPc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
bakalářských	bakalářský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
16	#num#	k4
257	#num#	k4
Počet	počet	k1gInSc4
magisterských	magisterský	k2eAgMnPc2d1
studentů	student	k1gMnPc2
</s>
<s>
14	#num#	k4
762	#num#	k4
Počet	počet	k1gInSc4
doktorandů	doktorand	k1gMnPc2
</s>
<s>
3	#num#	k4
218	#num#	k4
Počet	počet	k1gInSc4
studentů	student	k1gMnPc2
</s>
<s>
33	#num#	k4
259	#num#	k4
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
Počet	počet	k1gInSc1
akademických	akademický	k2eAgMnPc2d1
pracovníků	pracovník	k1gMnPc2
</s>
<s>
2	#num#	k4
233	#num#	k4
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Počet	počet	k1gInSc1
fakult	fakulta	k1gFnPc2
(	(	kIx(
<g/>
seznam	seznam	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Brno	Brno	k1gNnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Žerotínovo	Žerotínův	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
617	#num#	k4
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
602	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
55,03	55,03	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
<g/>
19,81	19,81	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kampus	kampus	k1gInSc1
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
Bohunice	Bohunice	k1gFnPc1
Členství	členství	k1gNnSc1
</s>
<s>
Utrechtská	Utrechtský	k2eAgFnSc1d1
síť	síť	k1gFnSc1
<g/>
,	,	kIx,
COMNAP	COMNAP	kA
a	a	k8xC
Asociace	asociace	k1gFnSc1
evropských	evropský	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
</s>
<s>
https://www.muni.cz	https://www.muni.cz	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Universitas	Universitas	k1gInSc1
Masarykiana	Masarykiana	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
Universita	universita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založena	založen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
jako	jako	k9
druhá	druhý	k4xOgFnSc1
česká	český	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
2	#num#	k4
<g/>
]	]	kIx)
Počtem	počet	k1gInSc7
studentů	student	k1gMnPc2
v	v	k7c6
akreditovaných	akreditovaný	k2eAgInPc6d1
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
je	být	k5eAaImIp3nS
druhou	druhý	k4xOgFnSc4
největší	veliký	k2eAgFnSc4d3
vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
deset	deset	k4xCc1
fakult	fakulta	k1gFnPc2
a	a	k8xC
provozuje	provozovat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
své	své	k1gNnSc4
Mendelovo	Mendelův	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
<g/>
,	,	kIx,
univerzitní	univerzitní	k2eAgNnSc4d1
kino	kino	k1gNnSc4
Scala	scát	k5eAaImAgFnS
<g/>
,	,	kIx,
univerzitní	univerzitní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
v	v	k7c6
Telči	Telč	k1gFnSc6
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
polární	polární	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
na	na	k7c6
Antarktidě	Antarktida	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
umísťuje	umísťovat	k5eAaImIp3nS
v	v	k7c6
žebříčku	žebříček	k1gInSc6
nejlepších	dobrý	k2eAgFnPc2d3
světových	světový	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
QS	QS	kA
TopUniversities	TopUniversities	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
je	být	k5eAaImIp3nS
jejím	její	k3xOp3gMnSc7
rektorem	rektor	k1gMnSc7
Martin	Martin	k1gMnSc1
Bareš	Bareš	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Socha	Socha	k1gMnSc1
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
před	před	k7c7
budovou	budova	k1gFnSc7
na	na	k7c4
Komenského	Komenského	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
(	(	kIx(
<g/>
bývalá	bývalý	k2eAgFnSc1d1
lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
O	o	k7c4
vznik	vznik	k1gInSc4
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
zejména	zejména	k9
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigue	k1gFnPc2
Masaryk	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
a	a	k8xC
pozdější	pozdní	k2eAgFnSc2d2
první	první	k4xOgFnSc2
prezident	prezident	k1gMnSc1
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
své	svůj	k3xOyFgFnSc2
vědecké	vědecký	k2eAgFnSc2d1
a	a	k8xC
politické	politický	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
pozornost	pozornost	k1gFnSc4
rozvoji	rozvoj	k1gInSc3
československých	československý	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
a	a	k8xC
již	již	k6eAd1
od	od	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zdůrazňoval	zdůrazňovat	k5eAaImAgInS
potřebu	potřeba	k1gFnSc4
široké	široký	k2eAgFnSc2d1
konkurence	konkurence	k1gFnSc2
ve	v	k7c6
vědecké	vědecký	k2eAgFnSc6d1
práci	práce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
poukazoval	poukazovat	k5eAaImAgInS
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
tehdejší	tehdejší	k2eAgFnSc1d1
jediná	jediný	k2eAgFnSc1d1
česká	český	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
rozvoji	rozvoj	k1gInSc3
potřebuje	potřebovat	k5eAaImIp3nS
konkurenční	konkurenční	k2eAgFnSc1d1
instituci	instituce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřízení	zřízení	k1gNnSc3
druhé	druhý	k4xOgFnSc2
české	český	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
bylo	být	k5eAaImAgNnS
dlouhá	dlouhý	k2eAgNnPc4d1
léta	léto	k1gNnPc4
jednou	jednou	k6eAd1
z	z	k7c2
jeho	jeho	k3xOp3gFnPc2
politických	politický	k2eAgFnPc2d1
priorit	priorita	k1gFnPc2
a	a	k8xC
měl	mít	k5eAaImAgMnS
v	v	k7c6
této	tento	k3xDgFnSc6
otázce	otázka	k1gFnSc6
podporu	podpor	k1gInSc2
řady	řada	k1gFnSc2
profesorů	profesor	k1gMnPc2
<g/>
,	,	kIx,
studentů	student	k1gMnPc2
i	i	k8xC
široké	široký	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
problémem	problém	k1gInSc7
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
volba	volba	k1gFnSc1
místa	místo	k1gNnSc2
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
univerzitu	univerzita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
všeobecně	všeobecně	k6eAd1
převládal	převládat	k5eAaImAgInS
názor	názor	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
založena	založit	k5eAaPmNgFnS
v	v	k7c6
zemském	zemský	k2eAgNnSc6d1
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
proti	proti	k7c3
byli	být	k5eAaImAgMnP
zejména	zejména	k9
brněnští	brněnský	k2eAgMnPc1d1
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
Brno	Brno	k1gNnSc4
politicky	politicky	k6eAd1
zcela	zcela	k6eAd1
ovládali	ovládat	k5eAaImAgMnP
a	a	k8xC
báli	bát	k5eAaImAgMnP
se	se	k3xPyFc4
oslabení	oslabení	k1gNnSc4
svého	svůj	k3xOyFgInSc2
vlivu	vliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
dokonce	dokonce	k9
zdrojem	zdroj	k1gInSc7
nacionálních	nacionální	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
vyvrcholily	vyvrcholit	k5eAaPmAgFnP
tragickými	tragický	k2eAgInPc7d1
pouličními	pouliční	k2eAgInPc7d1
střety	střet	k1gInPc7
v	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
tzv.	tzv.	kA
Volkstagu	Volkstag	k1gInSc2
<g/>
,	,	kIx,
při	při	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
přišel	přijít	k5eAaPmAgInS
o	o	k7c4
život	život	k1gInSc4
český	český	k2eAgMnSc1d1
dělník	dělník	k1gMnSc1
František	František	k1gMnSc1
Pavlík	Pavlík	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teprve	teprve	k6eAd1
konec	konec	k1gInSc1
války	válka	k1gFnSc2
a	a	k8xC
rozpad	rozpad	k1gInSc1
Rakouska-Uherska	Rakouska-Uhersko	k1gNnSc2
přinesl	přinést	k5eAaPmAgInS
příznivější	příznivý	k2eAgFnSc4d2
podmínky	podmínka	k1gFnPc4
pro	pro	k7c4
založení	založení	k1gNnSc4
nové	nový	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
její	její	k3xOp3gNnSc4
sídlo	sídlo	k1gNnSc4
byla	být	k5eAaImAgFnS
navrhována	navrhován	k2eAgFnSc1d1
např.	např.	kA
i	i	k8xC
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
přednost	přednost	k1gFnSc1
byla	být	k5eAaImAgFnS
jednoznačně	jednoznačně	k6eAd1
dána	dán	k2eAgFnSc1d1
většímu	veliký	k2eAgNnSc3d2
a	a	k8xC
významnějšímu	významný	k2eAgNnSc3d2
hlavnímu	hlavní	k2eAgNnSc3d1
zemskému	zemský	k2eAgNnSc3d1
městu	město	k1gNnSc3
–	–	k?
Brnu	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Zřízena	zřízen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
jako	jako	k9
druhá	druhý	k4xOgFnSc1
česká	český	k2eAgFnSc1d1
universita	universita	k1gFnSc1
zákonem	zákon	k1gInSc7
ze	z	k7c2
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1919	#num#	k4
č.	č.	k?
50	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
n.	n.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
měla	mít	k5eAaImAgFnS
čtyři	čtyři	k4xCgFnPc4
fakulty	fakulta	k1gFnPc4
(	(	kIx(
<g/>
právnickou	právnický	k2eAgFnSc4d1
<g/>
,	,	kIx,
lékařskou	lékařský	k2eAgFnSc4d1
<g/>
,	,	kIx,
přírodovědeckou	přírodovědecký	k2eAgFnSc4d1
a	a	k8xC
filozofickou	filozofický	k2eAgFnSc4d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
zákon	zákon	k1gInSc1
předpokládal	předpokládat	k5eAaImAgInS
výstavbu	výstavba	k1gFnSc4
nového	nový	k2eAgInSc2d1
univerzitního	univerzitní	k2eAgInSc2d1
areálu	areál	k1gInSc2
do	do	k7c2
roku	rok	k1gInSc2
1930	#num#	k4
<g/>
,	,	kIx,
podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
realizovat	realizovat	k5eAaBmF
pouze	pouze	k6eAd1
budovu	budova	k1gFnSc4
právnické	právnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc4
logo	logo	k1gNnSc4
po	po	k7c6
založení	založení	k1gNnSc6
univerzity	univerzita	k1gFnSc2
zpracoval	zpracovat	k5eAaPmAgMnS
malíř	malíř	k1gMnSc1
Eduard	Eduard	k1gMnSc1
Milén	Milén	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šlo	jít	k5eAaImAgNnS
o	o	k7c4
kaligrafické	kaligrafický	k2eAgFnPc4d1
iniciály	iniciála	k1gFnPc4
MU	MU	kA
<g/>
,	,	kIx,
umístěné	umístěný	k2eAgInPc1d1
na	na	k7c6
heraldickém	heraldický	k2eAgInSc6d1
štítu	štít	k1gInSc6
s	s	k7c7
pozadím	pozadí	k1gNnSc7
baldachýnové	baldachýnový	k2eAgFnSc2d1
drapérie	drapérie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nacistická	nacistický	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
způsobila	způsobit	k5eAaPmAgFnS
univerzitě	univerzita	k1gFnSc3
těžké	těžký	k2eAgFnPc1d1
ztráty	ztráta	k1gFnPc1
materiální	materiální	k2eAgFnPc1d1
i	i	k8xC
lidské	lidský	k2eAgFnPc1d1
(	(	kIx(
<g/>
například	například	k6eAd1
z	z	k7c2
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
byla	být	k5eAaImAgFnS
popravena	popraven	k2eAgFnSc1d1
nebo	nebo	k8xC
umučena	umučen	k2eAgFnSc1d1
čtvrtina	čtvrtina	k1gFnSc1
profesorského	profesorský	k2eAgInSc2d1
sboru	sbor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
neblahé	blahý	k2eNgInPc1d1
zásahy	zásah	k1gInPc1
následovaly	následovat	k5eAaImAgInP
ze	z	k7c2
strany	strana	k1gFnSc2
komunistické	komunistický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
po	po	k7c6
únoru	únor	k1gInSc6
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrozsáhlejší	rozsáhlý	k2eAgFnPc4d3
čistky	čistka	k1gFnPc4
proběhly	proběhnout	k5eAaPmAgFnP
na	na	k7c6
právnické	právnický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
muselo	muset	k5eAaImAgNnS
opustit	opustit	k5eAaPmF
46	#num#	k4
%	%	kIx~
studentů	student	k1gMnPc2
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1950	#num#	k4
byla	být	k5eAaImAgFnS
zrušena	zrušit	k5eAaPmNgFnS
úplně	úplně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1960	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
nesla	nést	k5eAaImAgFnS
univerzita	univerzita	k1gFnSc1
z	z	k7c2
politických	politický	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
jméno	jméno	k1gNnSc1
Universita	universita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1946	#num#	k4
byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
však	však	k9
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1953	#num#	k4
a	a	k8xC
1964	#num#	k4
stála	stát	k5eAaImAgFnS
mimo	mimo	k7c4
svazek	svazek	k1gInSc4
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1952	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
existovala	existovat	k5eAaImAgFnS
také	také	k9
farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obnovení	obnovení	k1gNnSc1
svobodných	svobodný	k2eAgInPc2d1
poměrů	poměr	k1gInPc2
po	po	k7c6
listopadu	listopad	k1gInSc6
1989	#num#	k4
umožnilo	umožnit	k5eAaPmAgNnS
univerzitě	univerzita	k1gFnSc6
další	další	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
i	i	k8xC
návrat	návrat	k1gInSc4
k	k	k7c3
původnímu	původní	k2eAgInSc3d1
názvu	název	k1gInSc2
„	„	k?
<g/>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
čtyři	čtyři	k4xCgFnPc1
nové	nový	k2eAgFnPc1d1
fakulty	fakulta	k1gFnPc1
(	(	kIx(
<g/>
ekonomicko-správní	ekonomicko-správní	k2eAgFnSc1d1
<g/>
,	,	kIx,
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
sociálních	sociální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
byl	být	k5eAaImAgMnS
vystavěn	vystavěn	k2eAgInSc4d1
univerzitní	univerzitní	k2eAgInSc4d1
kampus	kampus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1998	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
byla	být	k5eAaImAgFnS
univerzita	univerzita	k1gFnSc1
podle	podle	k7c2
přílohy	příloha	k1gFnSc2
zákona	zákon	k1gInSc2
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
z	z	k7c2
1998	#num#	k4
vedena	veden	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
se	se	k3xPyFc4
sídelní	sídelní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
,	,	kIx,
mylně	mylně	k6eAd1
uváděné	uváděný	k2eAgFnPc4d1
v	v	k7c6
názvu	název	k1gInSc6
<g/>
,	,	kIx,
opět	opět	k6eAd1
vypustilo	vypustit	k5eAaPmAgNnS
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
do	do	k7c2
září	září	k1gNnSc2
2016	#num#	k4
tak	tak	k8xS,k8xC
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
byla	být	k5eAaImAgFnS
podle	podle	k7c2
této	tento	k3xDgFnSc2
přílohy	příloha	k1gFnSc2
zákona	zákon	k1gInSc2
ze	z	k7c2
všech	všecek	k3xTgFnPc2
veřejných	veřejný	k2eAgFnPc2d1
a	a	k8xC
státních	státní	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
jedinou	jediný	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
u	u	k7c2
svého	svůj	k3xOyFgInSc2
názvu	název	k1gInSc2
neuváděla	uvádět	k5eNaImAgNnP
své	svůj	k3xOyFgNnSc4
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Univerzita	univerzita	k1gFnSc1
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
administrativu	administrativa	k1gFnSc4
studia	studio	k1gNnSc2
svůj	svůj	k3xOyFgInSc1
vlastní	vlastní	k2eAgInSc1d1
informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vývoj	vývoj	k1gInSc4
a	a	k8xC
inovace	inovace	k1gFnSc1
obdržela	obdržet	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
evropskou	evropský	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
EUNIS	EUNIS	kA
Elite	Elit	k1gInSc5
Award	Award	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezinárodní	mezinárodní	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
je	být	k5eAaImIp3nS
realizována	realizovat	k5eAaBmNgFnS
mimo	mimo	k7c4
jiné	jiný	k2eAgNnSc4d1
v	v	k7c6
rámci	rámec	k1gInSc6
Compostela	Compostela	k1gFnSc1
Group	Group	k1gInSc1
of	of	k?
Universities	Universities	k1gInSc1
a	a	k8xC
Utrecht	Utrecht	k2eAgInSc1d1
Network	network	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
vydává	vydávat	k5eAaImIp3nS,k5eAaPmIp3nS
univerzita	univerzita	k1gFnSc1
vlastní	vlastní	k2eAgFnSc2d1
zpravodajský	zpravodajský	k2eAgInSc4d1
měsíčník	měsíčník	k1gInSc4
Muni	Muni	k?
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
stal	stát	k5eAaPmAgInS
Firemním	firemní	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
roku	rok	k1gInSc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
tiskovina	tiskovina	k1gFnSc1
veřejné	veřejný	k2eAgFnSc2d1
a	a	k8xC
státní	státní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
pořádané	pořádaný	k2eAgFnSc6d1
Komorou	komora	k1gFnSc7
PR	pr	k0
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Podpisem	podpis	k1gInSc7
memoranda	memorandum	k1gNnSc2
o	o	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
spolkem	spolek	k1gInSc7
Wikimedia	Wikimedium	k1gNnSc2
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
se	s	k7c7
<g />
.	.	kIx.
</s>
<s hack="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
zavázala	zavázat	k5eAaPmAgFnS
k	k	k7c3
účasti	účast	k1gFnSc3
na	na	k7c6
rozvoji	rozvoj	k1gInSc6
internetové	internetový	k2eAgFnSc2d1
encyklopedie	encyklopedie	k1gFnSc2
Wikipedie	Wikipedie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Pravidelně	pravidelně	k6eAd1
se	se	k3xPyFc4
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
v	v	k7c6
hale	hala	k1gFnSc6
Rondo	rondo	k1gNnSc1
koná	konat	k5eAaImIp3nS
hokejový	hokejový	k2eAgInSc1d1
souboj	souboj	k1gInSc1
s	s	k7c7
výběrem	výběr	k1gInSc7
Vysokého	vysoký	k2eAgNnSc2d1
učení	učení	k1gNnSc2
technického	technický	k2eAgNnSc2d1
<g/>
,	,	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
celou	celý	k2eAgFnSc4d1
sérii	série	k1gFnSc4
vede	vést	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
příležitosti	příležitost	k1gFnSc6
blížícího	blížící	k2eAgInSc2d1
se	s	k7c7
100	#num#	k4
<g/>
.	.	kIx.
výročí	výročí	k1gNnSc2
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
se	se	k3xPyFc4
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
rozhodla	rozhodnout	k5eAaPmAgFnS
začít	začít	k5eAaPmF
od	od	k7c2
března	březen	k1gInSc2
2018	#num#	k4
používat	používat	k5eAaImF
nové	nový	k2eAgNnSc4d1
logo	logo	k1gNnSc4
a	a	k8xC
vizuální	vizuální	k2eAgInSc4d1
styl	styl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybrán	vybrán	k2eAgInSc1d1
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
pražského	pražský	k2eAgNnSc2d1
Studia	studio	k1gNnSc2
Najbrt	Najbrt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
odkazuje	odkazovat	k5eAaImIp3nS
k	k	k7c3
brněnské	brněnský	k2eAgFnSc3d1
funkcionalistické	funkcionalistický	k2eAgFnSc3d1
tradici	tradice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Logo	logo	k1gNnSc1
je	být	k5eAaImIp3nS
prostým	prostý	k2eAgNnSc7d1
provedením	provedení	k1gNnSc7
zkratkového	zkratkový	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
„	„	k?
<g/>
MUNI	MUNI	k?
<g/>
“	“	k?
v	v	k7c6
nově	nově	k6eAd1
vzniklém	vzniklý	k2eAgNnSc6d1
stejnojmenném	stejnojmenný	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fakulty	fakulta	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
fakult	fakulta	k1gFnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
deseti	deset	k4xCc2
fakult	fakulta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyři	čtyři	k4xCgMnPc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
byly	být	k5eAaImAgFnP
založeny	založit	k5eAaPmNgFnP
již	již	k6eAd1
při	při	k7c6
vzniku	vznik	k1gInSc6
univerzity	univerzita	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
zkratkou	zkratka	k1gFnSc7
PrF	PrF	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
LF	LF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
PřF	PřF	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
FF	ff	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1946	#num#	k4
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
PdF	PdF	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
založení	založení	k1gNnSc1
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
již	již	k9
při	při	k7c6
vzniku	vznik	k1gInSc6
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
zůstalo	zůstat	k5eAaPmAgNnS
tehdy	tehdy	k6eAd1
jen	jen	k9
ve	v	k7c6
stádiu	stádium	k1gNnSc6
úvah	úvaha	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
FaF	FaF	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1960	#num#	k4
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
reakci	reakce	k1gFnSc6
na	na	k7c4
změněné	změněný	k2eAgInPc4d1
politicko-hospodářské	politicko-hospodářský	k2eAgInPc4d1
poměry	poměr	k1gInPc4
k	k	k7c3
založení	založení	k1gNnSc3
Ekonomicko-správní	ekonomicko-správní	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
(	(	kIx(
<g/>
ESF	ESF	kA
<g/>
,	,	kIx,
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
se	se	k3xPyFc4
z	z	k7c2
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
vydělila	vydělit	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
informatiky	informatika	k1gFnSc2
(	(	kIx(
<g/>
FI	fi	k0
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
oddělením	oddělení	k1gNnSc7
od	od	k7c2
Filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
Fakulta	fakulta	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
FSS	FSS	kA
<g/>
)	)	kIx)
a	a	k8xC
roku	rok	k1gInSc2
2002	#num#	k4
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
dosud	dosud	k6eAd1
nejmladší	mladý	k2eAgFnSc1d3
Fakulta	fakulta	k1gFnSc1
sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
(	(	kIx(
<g/>
FSpS	FSpS	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
politických	politický	k2eAgFnPc2d1
změn	změna	k1gFnPc2
krátce	krátce	k6eAd1
po	po	k7c6
nástupu	nástup	k1gInSc6
komunismu	komunismus	k1gInSc2
roku	rok	k1gInSc2
1950	#num#	k4
zanikla	zaniknout	k5eAaPmAgNnP
a	a	k8xC
obnovena	obnoven	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1953	#num#	k4
vyčleněna	vyčleněn	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
samostatná	samostatný	k2eAgFnSc1d1
Vyšší	vysoký	k2eAgFnSc1d2
pedagogická	pedagogický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
a	a	k8xC
posléze	posléze	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1959	#num#	k4
fungovala	fungovat	k5eAaImAgFnS
jako	jako	k9
tzv.	tzv.	kA
Pedagogický	pedagogický	k2eAgInSc4d1
institut	institut	k1gInSc4
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
vrátila	vrátit	k5eAaPmAgFnS
do	do	k7c2
svazku	svazek	k1gInSc2
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
krátkém	krátký	k2eAgNnSc6d1
období	období	k1gNnSc6
let	léto	k1gNnPc2
1952	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
existovala	existovat	k5eAaImAgFnS
na	na	k7c6
Masarykově	Masarykův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
navázala	navázat	k5eAaPmAgFnS
na	na	k7c4
studium	studium	k1gNnSc4
farmacie	farmacie	k1gFnSc1
realizované	realizovaný	k2eAgNnSc4d1
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
na	na	k7c6
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byla	být	k5eAaImAgFnS
vládním	vládní	k2eAgNnSc7d1
nařízením	nařízení	k1gNnSc7
zrušena	zrušen	k2eAgNnPc1d1
<g/>
,	,	kIx,
studium	studium	k1gNnSc1
bylo	být	k5eAaImAgNnS
převedeno	převést	k5eAaPmNgNnS
do	do	k7c2
Bratislavy	Bratislava	k1gFnSc2
a	a	k8xC
Brno	Brno	k1gNnSc1
se	se	k3xPyFc4
obnovení	obnovení	k1gNnSc3
vlastní	vlastní	k2eAgFnSc2d1
farmaceutické	farmaceutický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
dočkalo	dočkat	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
,	,	kIx,
tehdy	tehdy	k6eAd1
už	už	k9
však	však	k9
jako	jako	k9
součásti	součást	k1gFnPc1
samostatné	samostatný	k2eAgFnSc2d1
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
veterinární	veterinární	k2eAgFnSc2d1
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
nesoucí	nesoucí	k2eAgInSc4d1
název	název	k1gInSc4
Veterinární	veterinární	k2eAgFnSc1d1
a	a	k8xC
farmaceutická	farmaceutický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
MU	MU	kA
byla	být	k5eAaImAgFnS
obnovena	obnovit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
fakticky	fakticky	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přechodu	přechod	k1gInSc3
fakulty	fakulta	k1gFnSc2
VFU	VFU	kA
pod	pod	k7c4
hlavičku	hlavička	k1gFnSc4
MU	MU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1990	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
byly	být	k5eAaImAgFnP
součástí	součást	k1gFnSc7
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
i	i	k9
dvě	dva	k4xCgNnPc4
nově	nova	k1gFnSc3
založené	založený	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
(	(	kIx(
<g/>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
<g/>
,	,	kIx,
Obchodně	obchodně	k6eAd1
podnikatelská	podnikatelský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
v	v	k7c6
Karviné	Karviná	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
roku	rok	k1gInSc2
1991	#num#	k4
staly	stát	k5eAaPmAgFnP
základem	základ	k1gInSc7
nově	nově	k6eAd1
zřízené	zřízený	k2eAgFnSc2d1
Slezské	slezský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Fakulty	fakulta	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejstarší	starý	k2eAgFnPc4d3
fakulty	fakulta	k1gFnPc4
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
za	za	k7c2
komunistického	komunistický	k2eAgInSc2d1
režimu	režim	k1gInSc2
byla	být	k5eAaImAgFnS
na	na	k7c4
čas	čas	k1gInSc4
zrušena	zrušit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
jediná	jediný	k2eAgFnSc1d1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
budově	budova	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
součástí	součást	k1gFnSc7
původně	původně	k6eAd1
plánovaného	plánovaný	k2eAgInSc2d1
prvorepublikového	prvorepublikový	k2eAgInSc2d1
univerzitního	univerzitní	k2eAgInSc2d1
areálu	areál	k1gInSc2
na	na	k7c4
Veveří	veveří	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
meziválečného	meziválečný	k2eAgNnSc2d1
období	období	k1gNnSc2
byla	být	k5eAaImAgFnS
významným	významný	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
právní	právní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
pěstovala	pěstovat	k5eAaImAgFnS
se	se	k3xPyFc4
zde	zde	k6eAd1
normativní	normativní	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
práva	právo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
hlavního	hlavní	k2eAgInSc2d1
pětiletého	pětiletý	k2eAgInSc2d1
magisterského	magisterský	k2eAgInSc2d1
oboru	obor	k1gInSc2
Právo	právo	k1gNnSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
navazujícího	navazující	k2eAgNnSc2d1
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
v	v	k7c6
programu	program	k1gInSc6
Teoretické	teoretický	k2eAgFnSc2d1
právní	právní	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
lze	lze	k6eAd1
na	na	k7c6
fakultě	fakulta	k1gFnSc6
studovat	studovat	k5eAaImF
různorodé	různorodý	k2eAgInPc4d1
bakalářské	bakalářský	k2eAgInPc4d1
právní	právní	k2eAgInPc4d1
obory	obor	k1gInPc4
specializované	specializovaný	k2eAgInPc4d1
na	na	k7c4
podnikání	podnikání	k1gNnSc4
<g/>
,	,	kIx,
veřejnou	veřejný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
trestní	trestní	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
nebo	nebo	k8xC
třeba	třeba	k9
nemovitosti	nemovitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Také	také	k9
lékařská	lékařský	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
první	první	k4xOgFnPc4
fakulty	fakulta	k1gFnPc4
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dříve	dříve	k6eAd2
sídlila	sídlit	k5eAaImAgFnS
na	na	k7c6
Komenského	Komenského	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
je	být	k5eAaImIp3nS
umístěna	umístit	k5eAaPmNgFnS
v	v	k7c6
univerzitním	univerzitní	k2eAgInSc6d1
kampusu	kampus	k1gInSc6
v	v	k7c6
Brně-Bohunicích	Brně-Bohunice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolupracuje	spolupracovat	k5eAaImIp3nS
s	s	k7c7
Fakultní	fakultní	k2eAgFnSc7d1
nemocnicí	nemocnice	k1gFnSc7
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
Fakultní	fakultní	k2eAgFnSc1d1
nemocnicí	nemocnice	k1gFnSc7
u	u	k7c2
sv.	sv.	kA
Anny	Anna	k1gFnSc2
a	a	k8xC
s	s	k7c7
Masarykovým	Masarykův	k2eAgInSc7d1
onkologickým	onkologický	k2eAgInSc7d1
ústavem	ústav	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Základním	základní	k2eAgInSc7d1
studijním	studijní	k2eAgInSc7d1
oborem	obor	k1gInSc7
je	být	k5eAaImIp3nS
šestiletý	šestiletý	k2eAgInSc4d1
obor	obor	k1gInSc4
Všeobecné	všeobecný	k2eAgNnSc4d1
lékařství	lékařství	k1gNnSc4
<g/>
,	,	kIx,
kromě	kromě	k7c2
něj	on	k3xPp3gNnSc2
také	také	k9
pětiletý	pětiletý	k2eAgInSc4d1
obor	obor	k1gInSc4
Zubní	zubní	k2eAgNnSc1d1
lékařství	lékařství	k1gNnSc1
a	a	k8xC
lze	lze	k6eAd1
pokračovat	pokračovat	k5eAaImF
i	i	k9
v	v	k7c6
doktorském	doktorský	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
(	(	kIx(
<g/>
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
fakulta	fakulta	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
bakalářské	bakalářský	k2eAgNnSc4d1
studium	studium	k1gNnSc4
pro	pro	k7c4
zdravotní	zdravotní	k2eAgFnPc4d1
sestry	sestra	k1gFnPc4
<g/>
,	,	kIx,
porodní	porodní	k2eAgFnPc4d1
asistentky	asistentka	k1gFnPc4
<g/>
,	,	kIx,
záchranáře	záchranář	k1gMnPc4
nebo	nebo	k8xC
se	s	k7c7
specializací	specializace	k1gFnSc7
na	na	k7c6
fyzioterapii	fyzioterapie	k1gFnSc6
<g/>
,	,	kIx,
radiologii	radiologie	k1gFnSc6
<g/>
,	,	kIx,
optometrii	optometrie	k1gFnSc6
<g/>
,	,	kIx,
nutriční	nutriční	k2eAgNnPc1d1
poradenství	poradenství	k1gNnPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
po	po	k7c6
založení	založení	k1gNnSc6
univerzity	univerzita	k1gFnSc2
zahájila	zahájit	k5eAaPmAgFnS
postupně	postupně	k6eAd1
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
byly	být	k5eAaImAgFnP
postupně	postupně	k6eAd1
adaptovány	adaptován	k2eAgFnPc1d1
prostory	prostora	k1gFnPc1
mezi	mezi	k7c7
ulicemi	ulice	k1gFnPc7
Kounicova	Kounicův	k2eAgNnPc1d1
<g/>
,	,	kIx,
Veveří	veveří	k2eAgNnPc1d1
a	a	k8xC
Kotlářská	kotlářský	k2eAgNnPc1d1
<g/>
,	,	kIx,
včetně	včetně	k7c2
botanické	botanický	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
významné	významný	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
znamenal	znamenat	k5eAaImAgInS
až	až	k9
kampus	kampus	k1gInSc1
v	v	k7c6
Brně-Bohunicích	Brně-Bohunice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
fakultě	fakulta	k1gFnSc6
se	se	k3xPyFc4
realizuje	realizovat	k5eAaBmIp3nS
celé	celý	k2eAgNnSc4d1
spektrum	spektrum	k1gNnSc4
přírodovědných	přírodovědný	k2eAgInPc2d1
programů	program	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc2
specializovaných	specializovaný	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
si	se	k3xPyFc3
tak	tak	k9
zvolit	zvolit	k5eAaPmF
mezi	mezi	k7c7
řadou	řada	k1gFnSc7
biologických	biologický	k2eAgInPc2d1
<g/>
,	,	kIx,
chemických	chemický	k2eAgInPc2d1
<g/>
,	,	kIx,
biochemických	biochemický	k2eAgInPc2d1
<g/>
,	,	kIx,
fyzikálních	fyzikální	k2eAgInPc2d1
<g/>
,	,	kIx,
matematických	matematický	k2eAgInPc2d1
<g/>
,	,	kIx,
geografických	geografický	k2eAgInPc2d1
<g/>
,	,	kIx,
antropologických	antropologický	k2eAgInPc2d1
či	či	k8xC
geologických	geologický	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
na	na	k7c4
bakalářské	bakalářský	k2eAgNnSc4d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
magisterské	magisterský	k2eAgFnPc1d1
i	i	k9
na	na	k7c6
doktorské	doktorský	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čtvrtou	čtvrtý	k4xOgFnSc7
nejstarší	starý	k2eAgFnSc7d3
fakultou	fakulta	k1gFnSc7
univerzity	univerzita	k1gFnSc2
je	být	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
filozofická	filozofický	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
dnes	dnes	k6eAd1
největší	veliký	k2eAgFnSc7d3
českou	český	k2eAgFnSc7d1
fakultou	fakulta	k1gFnSc7
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
proto	proto	k8xC
na	na	k7c6
více	hodně	k6eAd2
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
především	především	k6eAd1
ale	ale	k8xC
v	v	k7c6
komplexu	komplex	k1gInSc6
budov	budova	k1gFnPc2
na	na	k7c6
ulici	ulice	k1gFnSc6
Arne	Arne	k1gMnSc2
Nováka	Novák	k1gMnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
umístěna	umístěn	k2eAgFnSc1d1
i	i	k9
její	její	k3xOp3gFnSc1
knihovna	knihovna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
nabízí	nabízet	k5eAaImIp3nS
velmi	velmi	k6eAd1
mnoho	mnoho	k4c4
bakalářských	bakalářský	k2eAgMnPc2d1
<g/>
,	,	kIx,
magisterských	magisterský	k2eAgInPc2d1
i	i	k8xC
doktorských	doktorský	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
svých	svůj	k3xOyFgMnPc2
filologických	filologický	k2eAgMnPc2d1
(	(	kIx(
<g/>
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
<g/>
,	,	kIx,
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
a	a	k8xC
knihovnictví	knihovnictví	k1gNnSc2
<g/>
,	,	kIx,
anglistiky	anglistika	k1gFnSc2
a	a	k8xC
amerikanistiky	amerikanistika	k1gFnSc2
<g/>
,	,	kIx,
germanistiky	germanistika	k1gFnSc2
<g/>
,	,	kIx,
nordistiky	nordistika	k1gFnSc2
a	a	k8xC
nederlandistiky	nederlandistika	k1gFnSc2
<g/>
,	,	kIx,
baltistiky	baltistika	k1gFnSc2
<g/>
,	,	kIx,
románských	románský	k2eAgInPc2d1
jazyků	jazyk	k1gInPc2
a	a	k8xC
literatur	literatura	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
slavistiky	slavistika	k1gFnSc2
<g/>
,	,	kIx,
klasických	klasický	k2eAgFnPc2d1
studií	studie	k1gFnPc2
i	i	k8xC
čínských	čínský	k2eAgFnPc2d1
a	a	k8xC
japonských	japonský	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgInPc2d1
ústavů	ústav	k1gInPc2
a	a	k8xC
kateder	katedra	k1gFnPc2
(	(	kIx(
<g/>
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
archeologie	archeologie	k1gFnSc2
a	a	k8xC
muzeologie	muzeologie	k1gFnSc2
<g/>
,	,	kIx,
pomocných	pomocný	k2eAgFnPc2d1
věd	věda	k1gFnPc2
historických	historický	k2eAgFnPc2d1
a	a	k8xC
archivnictví	archivnictví	k1gNnSc2
<g/>
,	,	kIx,
evropské	evropský	k2eAgFnSc2d1
etnologie	etnologie	k1gFnSc2
<g/>
,	,	kIx,
filozofie	filozofie	k1gFnSc2
<g/>
,	,	kIx,
religionistiky	religionistika	k1gFnSc2
<g/>
,	,	kIx,
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
dějin	dějiny	k1gFnPc2
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
estetiky	estetika	k1gFnSc2
<g/>
,	,	kIx,
hudební	hudební	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
divadelních	divadelní	k2eAgNnPc2d1
studií	studio	k1gNnPc2
<g/>
,	,	kIx,
filmu	film	k1gInSc2
a	a	k8xC
audiovizuální	audiovizuální	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
nebo	nebo	k8xC
pedagogických	pedagogický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
poválečném	poválečný	k2eAgNnSc6d1
období	období	k1gNnSc6
a	a	k8xC
už	už	k6eAd1
několik	několik	k4yIc4
let	léto	k1gNnPc2
poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
na	na	k7c4
čas	čas	k1gInSc4
nahrazena	nahradit	k5eAaPmNgFnS
pouze	pouze	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
pedagogickou	pedagogický	k2eAgFnSc7d1
školou	škola	k1gFnSc7
a	a	k8xC
pedagogickým	pedagogický	k2eAgInSc7d1
institutem	institut	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
obnovení	obnovení	k1gNnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
definitivně	definitivně	k6eAd1
součástí	součást	k1gFnSc7
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
na	na	k7c6
ulici	ulice	k1gFnSc6
Poříčí	Poříčí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Studovat	studovat	k5eAaImF
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
bakalářské	bakalářský	k2eAgInPc4d1
<g/>
,	,	kIx,
magisterské	magisterský	k2eAgInPc4d1
i	i	k8xC
doktorské	doktorský	k2eAgInPc4d1
obory	obor	k1gInPc4
na	na	k7c6
katedrách	katedra	k1gFnPc6
humanitních	humanitní	k2eAgFnPc6d1
(	(	kIx(
<g/>
např.	např.	kA
anglického	anglický	k2eAgInSc2d1
<g/>
,	,	kIx,
českého	český	k2eAgInSc2d1
<g/>
,	,	kIx,
francouzského	francouzský	k2eAgMnSc2d1
<g/>
,	,	kIx,
německého	německý	k2eAgInSc2d1
či	či	k8xC
ruského	ruský	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
a	a	k8xC
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
občanské	občanský	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
speciální	speciální	k2eAgFnSc2d1
či	či	k8xC
primární	primární	k2eAgFnSc2d1
pedagogiky	pedagogika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přírodovědných	přírodovědný	k2eAgFnPc2d1
(	(	kIx(
<g/>
biologie	biologie	k1gFnSc2
<g/>
,	,	kIx,
fyziky	fyzika	k1gFnSc2
<g/>
,	,	kIx,
chemie	chemie	k1gFnSc2
<g/>
,	,	kIx,
geografie	geografie	k1gFnSc2
<g/>
,	,	kIx,
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
informační	informační	k2eAgFnSc2d1
a	a	k8xC
technické	technický	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
uměleckých	umělecký	k2eAgInPc2d1
(	(	kIx(
<g/>
hudební	hudební	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
,	,	kIx,
výtvarné	výtvarný	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
krátce	krátce	k6eAd1
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
obnovení	obnovení	k1gNnSc3
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhá	probíhat	k5eAaImIp3nS
na	na	k7c6
ní	on	k3xPp3gFnSc6
výuka	výuka	k1gFnSc1
farmacie	farmacie	k1gFnPc1
<g/>
,	,	kIx,
o	o	k7c4
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
se	se	k3xPyFc4
stará	starat	k5eAaImIp3nS
šest	šest	k4xCc1
ústavů	ústav	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
areálu	areál	k1gInSc6
Veterinární	veterinární	k2eAgFnSc2d1
a	a	k8xC
farmaceutické	farmaceutický	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
VFU	VFU	kA
<g/>
)	)	kIx)
v	v	k7c6
prostorách	prostora	k1gFnPc6
bývalé	bývalý	k2eAgFnSc2d1
Farmaceutické	farmaceutický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
VFU	VFU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
budoucnu	budoucno	k1gNnSc6
má	mít	k5eAaImIp3nS
sídlit	sídlit	k5eAaImF
v	v	k7c6
bohunickém	bohunický	k2eAgInSc6d1
kampusu	kampus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Ekonomicko-správní	ekonomicko-správní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ekonomicko-správní	ekonomicko-správní	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
mladší	mladý	k2eAgFnPc4d2
fakulty	fakulta	k1gFnPc4
patří	patřit	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
ekonomicko-správní	ekonomicko-správní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřena	zaměřen	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
na	na	k7c4
ekonomii	ekonomie	k1gFnSc4
<g/>
,	,	kIx,
ekonomické	ekonomický	k2eAgInPc4d1
informační	informační	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
finanční	finanční	k2eAgNnSc4d1
podnikání	podnikání	k1gNnSc4
<g/>
,	,	kIx,
hospodářskou	hospodářský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
management	management	k1gInSc4
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc4d1
a	a	k8xC
podnikové	podnikový	k2eAgNnSc4d1
hospodářství	hospodářství	k1gNnSc4
<g/>
,	,	kIx,
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
a	a	k8xC
správu	správa	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
nabízí	nabízet	k5eAaImIp3nS
v	v	k7c6
bakalářských	bakalářský	k2eAgInPc6d1
<g/>
,	,	kIx,
magisterských	magisterský	k2eAgInPc6d1
i	i	k8xC
doktorských	doktorský	k2eAgInPc6d1
studijních	studijní	k2eAgInPc6d1
programech	program	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
nově	nově	k6eAd1
postavené	postavený	k2eAgFnSc6d1
budově	budova	k1gFnSc6
v	v	k7c6
Brně-Pisárkách	Brně-Pisárka	k1gFnPc6
na	na	k7c6
ulici	ulice	k1gFnSc6
Lipová	lipový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
informatiky	informatika	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
informatiky	informatika	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
informatiky	informatika	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
pořadí	pořadí	k1gNnSc6
jako	jako	k8xS,k8xC
osmá	osmý	k4xOgFnSc1
<g/>
,	,	kIx,
vyčleněním	vyčlenění	k1gNnSc7
matematické	matematický	k2eAgFnSc2d1
informatiky	informatika	k1gFnSc2
z	z	k7c2
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umístěna	umístěn	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
v	v	k7c6
několika	několik	k4yIc6
vzájemně	vzájemně	k6eAd1
propojených	propojený	k2eAgFnPc6d1
budovách	budova	k1gFnPc6
na	na	k7c6
ulici	ulice	k1gFnSc6
Botanická	botanický	k2eAgFnSc1d1
a	a	k8xC
studovat	studovat	k5eAaImF
na	na	k7c6
ní	on	k3xPp3gFnSc6
lze	lze	k6eAd1
specializované	specializovaný	k2eAgInPc4d1
obory	obor	k1gInPc4
v	v	k7c6
bakalářském	bakalářský	k2eAgMnSc6d1
<g/>
,	,	kIx,
magisterském	magisterský	k2eAgInSc6d1
i	i	k8xC
doktorském	doktorský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
Informatika	informatika	k1gFnSc1
nebo	nebo	k8xC
Aplikovaná	aplikovaný	k2eAgFnSc1d1
informatika	informatika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
sociálních	sociální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
se	se	k3xPyFc4
vydělila	vydělit	k5eAaPmAgFnS
z	z	k7c2
filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
osamostatněním	osamostatnění	k1gNnSc7
oborů	obor	k1gInPc2
sociologie	sociologie	k1gFnSc2
<g/>
,	,	kIx,
psychologie	psychologie	k1gFnSc2
a	a	k8xC
politologie	politologie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
na	na	k7c4
ni	on	k3xPp3gFnSc4
možné	možný	k2eAgNnSc1d1
studovat	studovat	k5eAaImF
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k2eAgInPc7d1
obory	obor	k1gInPc7
(	(	kIx(
<g/>
např.	např.	kA
mediálních	mediální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
a	a	k8xC
žurnalistiky	žurnalistika	k1gFnSc2
<g/>
,	,	kIx,
environmentálních	environmentální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
nebo	nebo	k8xC
mezinárodních	mezinárodní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
)	)	kIx)
na	na	k7c6
bakalářské	bakalářský	k2eAgFnSc6d1
<g/>
,	,	kIx,
navazující	navazující	k2eAgFnSc6d1
magisterské	magisterský	k2eAgFnSc3d1
i	i	k8xC
doktorské	doktorský	k2eAgFnSc3d1
úrovni	úroveň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
Joštově	Joštův	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
budově	budova	k1gFnSc6
původně	původně	k6eAd1
užívané	užívaný	k2eAgNnSc1d1
německou	německý	k2eAgFnSc7d1
technikou	technika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Fakulta	fakulta	k1gFnSc1
sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Fakulta	fakulta	k1gFnSc1
sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nejmladší	mladý	k2eAgFnSc7d3
fakultou	fakulta	k1gFnSc7
je	být	k5eAaImIp3nS
fakulta	fakulta	k1gFnSc1
sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
v	v	k7c6
univerzitním	univerzitní	k2eAgInSc6d1
kampusu	kampus	k1gInSc6
v	v	k7c6
Brně-Bohunicích	Brně-Bohunice	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
její	její	k3xOp3gFnSc2
katedry	katedra	k1gFnSc2
atletiky	atletika	k1gFnSc2
<g/>
,	,	kIx,
plavání	plavání	k1gNnSc2
a	a	k8xC
sportů	sport	k1gInPc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
,	,	kIx,
gymnastiky	gymnastika	k1gFnSc2
a	a	k8xC
úpolů	úpol	k1gInPc2
<g/>
,	,	kIx,
kineziologie	kineziologie	k1gFnSc2
<g/>
,	,	kIx,
pedagogiky	pedagogika	k1gFnSc2
sportu	sport	k1gInSc2
<g/>
,	,	kIx,
podpory	podpora	k1gFnSc2
zdraví	zdraví	k1gNnSc1
<g/>
,	,	kIx,
společenských	společenský	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
managementu	management	k1gInSc2
sportu	sport	k1gInSc2
a	a	k8xC
sportovních	sportovní	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
i	i	k9
centrum	centrum	k1gNnSc1
univerzitního	univerzitní	k2eAgInSc2d1
sportu	sport	k1gInSc2
v	v	k7c6
tělocvičně	tělocvična	k1gFnSc6
Pod	pod	k7c7
Hradem	hrad	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
na	na	k7c6
této	tento	k3xDgFnSc6
fakultě	fakulta	k1gFnSc6
se	se	k3xPyFc4
studium	studium	k1gNnSc1
organizuje	organizovat	k5eAaBmIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
bakalářských	bakalářský	k2eAgInPc2d1
<g/>
,	,	kIx,
navazujících	navazující	k2eAgInPc2d1
magisterských	magisterský	k2eAgInPc2d1
a	a	k8xC
doktorských	doktorský	k2eAgInPc2d1
studijních	studijní	k2eAgInPc2d1
programů	program	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
</s>
<s>
Rektorát	rektorát	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
Brno-Bohunice	Brno-Bohunice	k1gFnSc2
</s>
<s>
Rektorát	rektorát	k1gInSc1
</s>
<s>
Kariérní	kariérní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Ústavy	ústava	k1gFnPc1
</s>
<s>
Ústav	ústav	k1gInSc1
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
(	(	kIx(
<g/>
ÚVT	ÚVT	kA
<g/>
)	)	kIx)
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
(	(	kIx(
<g/>
CEITEC	CEITEC	kA
<g/>
)	)	kIx)
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
</s>
<s>
Archiv	archiv	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Centrum	centrum	k1gNnSc1
jazykového	jazykový	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
(	(	kIx(
<g/>
CJV	CJV	kA
<g/>
)	)	kIx)
</s>
<s>
Centrum	centrum	k1gNnSc1
zahraniční	zahraniční	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
(	(	kIx(
<g/>
CZS	CZS	kA
<g/>
)	)	kIx)
</s>
<s>
Středisko	středisko	k1gNnSc1
pro	pro	k7c4
pomoc	pomoc	k1gFnSc4
studentům	student	k1gMnPc3
se	s	k7c7
specifickými	specifický	k2eAgInPc7d1
nároky	nárok	k1gInPc7
(	(	kIx(
<g/>
Teiresiás	Teiresiás	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
transfer	transfer	k1gInSc4
technologií	technologie	k1gFnPc2
(	(	kIx(
<g/>
CTT	CTT	kA
<g/>
)	)	kIx)
</s>
<s>
Institut	institut	k1gInSc1
biostatistiky	biostatistika	k1gFnSc2
a	a	k8xC
analýz	analýza	k1gFnPc2
(	(	kIx(
<g/>
IBA	iba	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Mendelovo	Mendelův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
</s>
<s>
Centrum	centrum	k1gNnSc1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
výzkumu	výzkum	k1gInSc2
a	a	k8xC
inovací	inovace	k1gFnPc2
v	v	k7c6
informačních	informační	k2eAgFnPc6d1
a	a	k8xC
komunikačních	komunikační	k2eAgFnPc6d1
technologiích	technologie	k1gFnPc6
(	(	kIx(
<g/>
CERIT	CERIT	kA
<g/>
)	)	kIx)
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
řídící	řídící	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
projektu	projekt	k1gInSc2
CEITEC	CEITEC	kA
</s>
<s>
Univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Telč	Telč	k1gFnSc1
</s>
<s>
Účelová	účelový	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
</s>
<s>
Správa	správa	k1gFnSc1
kolejí	kolej	k1gFnPc2
a	a	k8xC
menz	menza	k1gFnPc2
(	(	kIx(
<g/>
SKM	SKM	kA
<g/>
)	)	kIx)
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Správa	správa	k1gFnSc1
Univerzitního	univerzitní	k2eAgInSc2d1
kampusu	kampus	k1gInSc2
Bohunice	Bohunice	k1gFnPc1
(	(	kIx(
<g/>
UKB	UKB	kA
<g/>
)	)	kIx)
</s>
<s>
Univerzitní	univerzitní	k2eAgFnPc1d1
budovy	budova	k1gFnPc1
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
množství	množství	k1gNnSc4
budov	budova	k1gFnPc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
částech	část	k1gFnPc6
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rektorát	rektorát	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Kounicově	Kounicově	k1gMnSc6
paláci	palác	k1gInSc6
na	na	k7c6
Žerotínově	Žerotínův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
od	od	k7c2
založení	založení	k1gNnSc2
univerzity	univerzita	k1gFnSc2
se	se	k3xPyFc4
uvažovalo	uvažovat	k5eAaImAgNnS
o	o	k7c6
stavbě	stavba	k1gFnSc6
univerzitní	univerzitní	k2eAgFnSc2d1
čtvrti	čtvrt	k1gFnSc2
<g/>
,	,	kIx,
původně	původně	k6eAd1
na	na	k7c6
Kraví	kraví	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
a	a	k8xC
na	na	k7c6
souvisejících	související	k2eAgInPc6d1
pozemcích	pozemek	k1gInPc6
mezi	mezi	k7c7
Žabovřeskami	Žabovřesky	k1gFnPc7
a	a	k8xC
Veveřím	veveří	k2eAgNnPc3d1
<g/>
,	,	kIx,
projekt	projekt	k1gInSc1
však	však	k9
od	od	k7c2
počátku	počátek	k1gInSc2
provázely	provázet	k5eAaImAgInP
značné	značný	k2eAgInPc1d1
průtahy	průtah	k1gInPc1
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gMnSc2
na	na	k7c6
zamýšleném	zamýšlený	k2eAgNnSc6d1
Akademickém	akademický	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
podařilo	podařit	k5eAaPmAgNnS
realizovat	realizovat	k5eAaBmF
jen	jen	k9
budovu	budova	k1gFnSc4
právnické	právnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kampus	kampus	k1gInSc1
Bohunice	Bohunice	k1gFnPc4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Univerzitní	univerzitní	k2eAgInSc4d1
kampus	kampus	k1gInSc4
Bohunice	Bohunice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Univerzitní	univerzitní	k2eAgInSc1d1
kampus	kampus	k1gInSc1
byl	být	k5eAaImAgInS
nakonec	nakonec	k6eAd1
v	v	k7c6
Brně-Bohunicích	Brně-Bohunik	k1gInPc6
otevřen	otevřít	k5eAaPmNgInS
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
první	první	k4xOgNnSc4
rozhodnutí	rozhodnutí	k1gNnSc4
o	o	k7c6
jeho	jeho	k3xOp3gFnSc6
výstavbě	výstavba	k1gFnSc6
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
již	již	k9
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
moderní	moderní	k2eAgInSc4d1
vzdělávací	vzdělávací	k2eAgInSc4d1
a	a	k8xC
výzkumně-vývojové	výzkumně-vývojový	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
zabírající	zabírající	k2eAgFnSc4d1
plochu	plocha	k1gFnSc4
42	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
slouží	sloužit	k5eAaImIp3nS
pěti	pět	k4xCc3
tisícům	tisíc	k4xCgInPc3
studentů	student	k1gMnPc2
a	a	k8xC
tisíci	tisíc	k4xCgInPc7
pracovníků	pracovník	k1gMnPc2
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
<g/>
,	,	kIx,
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
a	a	k8xC
fakulty	fakulta	k1gFnSc2
sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Počítačové	počítačový	k2eAgFnPc1d1
studovny	studovna	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Ústav	ústava	k1gFnPc2
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
Ústavu	ústav	k1gInSc2
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
síť	síť	k1gFnSc4
studijních	studijní	k2eAgFnPc2d1
a	a	k8xC
počítačových	počítačový	k2eAgFnPc2d1
studoven	studovna	k1gFnPc2
ve	v	k7c6
většině	většina	k1gFnSc6
budov	budova	k1gFnPc2
svých	svůj	k3xOyFgFnPc2
fakult	fakulta	k1gFnPc2
nebo	nebo	k8xC
knihoven	knihovna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
vždy	vždy	k6eAd1
o	o	k7c4
prostor	prostor	k1gInSc4
s	s	k7c7
desítkami	desítka	k1gFnPc7
počítačů	počítač	k1gInPc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgFnPc3,k3yIgFnPc3,k3yQgFnPc3
se	se	k3xPyFc4
uživatelé	uživatel	k1gMnPc1
přihlašují	přihlašovat	k5eAaImIp3nP
vlastním	vlastní	k2eAgInSc7d1
účtem	účet	k1gInSc7
(	(	kIx(
<g/>
vázaným	vázaný	k2eAgInPc3d1
typicky	typicky	k6eAd1
k	k	k7c3
osobnímu	osobní	k2eAgNnSc3d1
číslu	číslo	k1gNnSc3
učo	učo	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
přístupem	přístup	k1gInSc7
k	k	k7c3
tiskovým	tiskový	k2eAgFnPc3d1
službám	služba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zavedení	zavedení	k1gNnSc6
centralizovaného	centralizovaný	k2eAgInSc2d1
způsobu	způsob	k1gInSc2
správy	správa	k1gFnSc2
a	a	k8xC
jednotné	jednotný	k2eAgFnSc2d1
instalace	instalace	k1gFnSc2
softwaru	software	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
výrazné	výrazný	k2eAgFnSc3d1
úspoře	úspora	k1gFnSc3
nákladů	náklad	k1gInPc2
a	a	k8xC
omezení	omezení	k1gNnPc2
obtíží	obtíž	k1gFnPc2
při	při	k7c6
provozu	provoz	k1gInSc6
studoven	studovna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3
počítačovou	počítačový	k2eAgFnSc7d1
studovnou	studovna	k1gFnSc7
s	s	k7c7
nepřetržitým	přetržitý	k2eNgInSc7d1
provozem	provoz	k1gInSc7
je	být	k5eAaImIp3nS
celouniverzitní	celouniverzitní	k2eAgFnSc1d1
počítačová	počítačový	k2eAgFnSc1d1
studovna	studovna	k1gFnSc1
(	(	kIx(
<g/>
CPS	CPS	kA
<g/>
)	)	kIx)
s	s	k7c7
centrálním	centrální	k2eAgNnSc7d1
umístěním	umístění	k1gNnSc7
na	na	k7c4
Komenského	Komenského	k2eAgNnSc4d1
náměstí	náměstí	k1gNnSc4
a	a	k8xC
kapacitou	kapacita	k1gFnSc7
145	#num#	k4
počítačů	počítač	k1gMnPc2
<g/>
,	,	kIx,
vybudovaná	vybudovaný	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rektor	rektor	k1gMnSc1
</s>
<s>
Úřadující	úřadující	k2eAgMnSc1d1
rektor	rektor	k1gMnSc1
Martin	Martin	k1gMnSc1
Bareš	Bareš	k1gMnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
rektorů	rektor	k1gMnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vedení	vedení	k1gNnSc6
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
se	se	k3xPyFc4
za	za	k7c4
dobu	doba	k1gFnSc4
její	její	k3xOp3gFnSc2
existence	existence	k1gFnSc2
vystřídalo	vystřídat	k5eAaPmAgNnS
již	již	k6eAd1
32	#num#	k4
rektorů	rektor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
rektorem	rektor	k1gMnSc7
byl	být	k5eAaImAgMnS
profesor	profesor	k1gMnSc1
ekonomie	ekonomie	k1gFnSc2
Karel	Karel	k1gMnSc1
Engliš	Engliš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
ke	k	k7c3
zřízení	zřízení	k1gNnSc3
brněnské	brněnský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
osobně	osobně	k6eAd1
přispěl	přispět	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkční	funkční	k2eAgFnPc4d1
období	období	k1gNnPc4
rektora	rektor	k1gMnSc2
bylo	být	k5eAaImAgNnS
zpočátku	zpočátku	k6eAd1
jednoleté	jednoletý	k2eAgNnSc1d1
<g/>
,	,	kIx,
odpovídalo	odpovídat	k5eAaImAgNnS
akademickému	akademický	k2eAgInSc3d1
roku	rok	k1gInSc3
a	a	k8xC
v	v	k7c6
úřadě	úřad	k1gInSc6
se	se	k3xPyFc4
po	po	k7c6
roce	rok	k1gInSc6
střídali	střídat	k5eAaImAgMnP
předchozí	předchozí	k2eAgMnPc1d1
děkani	děkan	k1gMnPc1
tehdejších	tehdejší	k2eAgFnPc2d1
čtyř	čtyři	k4xCgFnPc2
univerzitních	univerzitní	k2eAgFnPc2d1
fakult	fakulta	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
pořadí	pořadí	k1gNnSc6
lékařské	lékařský	k2eAgFnSc2d1
<g/>
,	,	kIx,
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
<g/>
,	,	kIx,
filozofické	filozofický	k2eAgFnSc2d1
a	a	k8xC
právnické	právnický	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
významné	významný	k2eAgInPc1d1
osobnosti	osobnost	k1gFnPc4
jako	jako	k8xS,k8xC
např.	např.	kA
právníci	právník	k1gMnPc1
František	František	k1gMnSc1
Weyr	Weyr	k1gMnSc1
a	a	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Kallab	Kallab	k1gMnSc1
nebo	nebo	k8xC
fyziolog	fyziolog	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
několika	několik	k4yIc2
dalších	další	k2eAgFnPc2d1
brněnských	brněnský	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
Edward	Edward	k1gMnSc1
Babák	Babák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
druhého	druhý	k4xOgNnSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
stávající	stávající	k2eAgMnSc1d1
rektor	rektor	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
české	český	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
Arne	Arne	k1gMnSc1
Novák	Novák	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
jeho	jeho	k3xOp3gMnSc1
protikandidát	protikandidát	k1gMnSc1
z	z	k7c2
právnické	právnický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Rudolf	Rudolf	k1gMnSc1
Dominik	Dominik	k1gMnSc1
nezískal	získat	k5eNaPmAgMnS
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
fašistickou	fašistický	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
dostatečnou	dostatečný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
a	a	k8xC
v	v	k7c6
akademickém	akademický	k2eAgInSc6d1
roce	rok	k1gInSc6
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
1940	#num#	k4
nakonec	nakonec	k6eAd1
zastával	zastávat	k5eAaImAgMnS
pouze	pouze	k6eAd1
funkci	funkce	k1gFnSc6
děkana	děkan	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
uzavření	uzavření	k1gNnSc2
českých	český	k2eAgFnPc2d1
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
nacisty	nacista	k1gMnSc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1939	#num#	k4
a	a	k8xC
smrti	smrt	k1gFnSc6
nemocného	nemocný	k2eAgMnSc2d1,k2eNgMnSc2d1
Arna	Arne	k1gMnSc2
Nováka	Novák	k1gMnSc2
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1939	#num#	k4
stál	stát	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
v	v	k7c6
čele	čelo	k1gNnSc6
univerzity	univerzita	k1gFnSc2
úřadující	úřadující	k2eAgMnSc1d1
prorektor	prorektor	k1gMnSc1
a	a	k8xC
Novákův	Novákův	k2eAgMnSc1d1
předchůdce	předchůdce	k1gMnSc1
v	v	k7c6
rektorské	rektorský	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
botaniky	botanika	k1gFnSc2
Josef	Josef	k1gMnSc1
Podpěra	podpěra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Krátce	krátce	k6eAd1
po	po	k7c6
osvobození	osvobození	k1gNnSc6
byl	být	k5eAaImAgMnS
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1945	#num#	k4
do	do	k7c2
rektorské	rektorský	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
instalován	instalován	k2eAgMnSc1d1
profesor	profesor	k1gMnSc1
patologické	patologický	k2eAgFnSc2d1
anatomie	anatomie	k1gFnSc2
a	a	k8xC
dvojnásobný	dvojnásobný	k2eAgMnSc1d1
meziválečný	meziválečný	k2eAgMnSc1d1
děkan	děkan	k1gMnSc1
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Václav	Václav	k1gMnSc1
Neumann	Neumann	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
ještě	ještě	k9
dvakrát	dvakrát	k6eAd1
volbou	volba	k1gFnSc7
potvrzen	potvrdit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ročním	roční	k2eAgInSc6d1
mandátu	mandát	k1gInSc6
profesora	profesor	k1gMnSc2
geometrie	geometrie	k1gFnSc2
a	a	k8xC
bývalého	bývalý	k2eAgMnSc2d1
dvojnásobného	dvojnásobný	k2eAgMnSc2d1
děkana	děkan	k1gMnSc2
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Ladislava	Ladislav	k1gMnSc2
Seiferta	Seifert	k1gMnSc2
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
funkce	funkce	k1gFnSc2
rektora	rektor	k1gMnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
první	první	k4xOgMnSc1
děkan	děkan	k1gMnSc1
po	po	k7c6
válce	válka	k1gFnSc6
vzniklé	vzniklý	k2eAgFnSc2d1
pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
a	a	k8xC
profesor	profesor	k1gMnSc1
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
František	František	k1gMnSc1
Trávníček	Trávníček	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
ní	on	k3xPp3gFnSc6
setrval	setrvat	k5eAaPmAgMnS
dlouhých	dlouhý	k2eAgNnPc2d1
11	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
po	po	k7c4
celou	celá	k1gFnSc4
tuto	tento	k3xDgFnSc4
dobu	doba	k1gFnSc4
vykonával	vykonávat	k5eAaImAgMnS
i	i	k9
funkci	funkce	k1gFnSc4
poslance	poslanec	k1gMnSc2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ČSR	ČSR	kA
za	za	k7c4
Komunistickou	komunistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Československa	Československo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
Trávníčka	Trávníček	k1gMnSc4
jako	jako	k9
rektor	rektor	k1gMnSc1
vystřídal	vystřídat	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
děkan	děkan	k1gMnSc1
přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
a	a	k8xC
profesor	profesor	k1gMnSc1
mikrobiologie	mikrobiologie	k1gFnSc2
Theodor	Theodor	k1gMnSc1
Martinec	Martinec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
funkci	funkce	k1gFnSc4
zastával	zastávat	k5eAaImAgMnS
rovněž	rovněž	k9
11	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
než	než	k8xS
z	z	k7c2
ní	on	k3xPp3gFnSc2
byl	být	k5eAaImAgInS
na	na	k7c6
počátku	počátek	k1gInSc6
normalizace	normalizace	k1gFnSc2
roku	rok	k1gInSc2
1970	#num#	k4
pro	pro	k7c4
své	svůj	k3xOyFgInPc4
politické	politický	k2eAgInPc4d1
postoje	postoj	k1gInPc4
v	v	k7c6
době	doba	k1gFnSc6
pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
odvolán	odvolat	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uskutečňování	uskutečňování	k1gNnSc3
normalizace	normalizace	k1gFnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
po	po	k7c6
něm	on	k3xPp3gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1970	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
řídil	řídit	k5eAaImAgMnS
jako	jako	k9
rektor	rektor	k1gMnSc1
profesor	profesor	k1gMnSc1
Jaromír	Jaromír	k1gMnSc1
Vašků	Vašek	k1gMnPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
byl	být	k5eAaImAgMnS
současně	současně	k6eAd1
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
zakladatele	zakladatel	k1gMnSc2
Engliše	Engliš	k1gMnSc2
<g/>
)	)	kIx)
prvním	první	k4xOgMnSc7
rektorem	rektor	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
před	před	k7c7
výkonem	výkon	k1gInSc7
svého	svůj	k3xOyFgInSc2
úřadu	úřad	k1gInSc2
nebyl	být	k5eNaImAgMnS
děkanem	děkan	k1gMnSc7
některé	některý	k3yIgFnSc2
z	z	k7c2
fakult	fakulta	k1gFnPc2
(	(	kIx(
<g/>
působil	působit	k5eAaImAgMnS
pouze	pouze	k6eAd1
v	v	k7c6
letech	let	k1gInPc6
1959	#num#	k4
<g/>
–	–	k?
<g/>
1962	#num#	k4
ve	v	k7c6
vedení	vedení	k1gNnSc6
lékařské	lékařský	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
jako	jako	k8xC,k8xS
proděkan	proděkan	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
obdobím	období	k1gNnSc7
postupného	postupný	k2eAgNnSc2d1
zmírňování	zmírňování	k1gNnSc2
normalizačních	normalizační	k2eAgInPc2d1
tlaků	tlak	k1gInPc2
je	být	k5eAaImIp3nS
spjat	spjat	k2eAgInSc1d1
mandát	mandát	k1gInSc1
Vojtěcha	Vojtěch	k1gMnSc2
Kubáčka	Kubáček	k1gMnSc2
<g/>
,	,	kIx,
třetího	třetí	k4xOgMnSc2
nejdéle	dlouho	k6eAd3
sloužícího	sloužící	k2eAgMnSc2d1
rektora	rektor	k1gMnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledním	poslední	k2eAgMnSc7d1
komunistickým	komunistický	k2eAgMnSc7d1
rektorem	rektor	k1gMnSc7
byl	být	k5eAaImAgMnS
profesor	profesor	k1gMnSc1
československých	československý	k2eAgFnPc2d1
dějin	dějiny	k1gFnPc2
Bedřich	Bedřich	k1gMnSc1
Čerešňák	Čerešňák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
byl	být	k5eAaImAgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
1989	#num#	k4
akademickou	akademický	k2eAgFnSc7d1
obcí	obec	k1gFnSc7
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
již	již	k6eAd1
politickými	politický	k2eAgMnPc7d1
úředníky	úředník	k1gMnPc7
<g/>
)	)	kIx)
novým	nový	k2eAgMnSc7d1
rektorem	rektor	k1gMnSc7
zvolen	zvolit	k5eAaPmNgMnS
někdejší	někdejší	k2eAgMnSc1d1
děkan	děkan	k1gMnSc1
filozofické	filozofický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
(	(	kIx(
<g/>
z	z	k7c2
počátku	počátek	k1gInSc2
šedesátých	šedesátý	k4xOgNnPc2
let	léto	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
pozdější	pozdní	k2eAgMnSc1d2
disident	disident	k1gMnSc1
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
českého	český	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
Milan	Milan	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
čtyř	čtyři	k4xCgMnPc2
prvních	první	k4xOgMnPc2
demokraticky	demokraticky	k6eAd1
zvolených	zvolený	k2eAgMnPc2d1
rektorů	rektor	k1gMnPc2
byl	být	k5eAaImAgMnS
do	do	k7c2
funkce	funkce	k1gFnSc2
jmenován	jmenován	k2eAgInSc4d1
prezidentem	prezident	k1gMnSc7
Václavem	Václav	k1gMnSc7
Havlem	Havel	k1gMnSc7
v	v	k7c6
lednu	leden	k1gInSc6
1990	#num#	k4
a	a	k8xC
setrval	setrvat	k5eAaPmAgMnS
v	v	k7c6
ní	on	k3xPp3gFnSc6
do	do	k7c2
srpna	srpen	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
coby	coby	k?
devětašedesátiletý	devětašedesátiletý	k2eAgMnSc1d1
rezignoval	rezignovat	k5eAaBmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nahradil	nahradit	k5eAaPmAgMnS
ho	on	k3xPp3gNnSc4
profesor	profesor	k1gMnSc1
fyziky	fyzika	k1gFnSc2
Eduard	Eduard	k1gMnSc1
Schmidt	Schmidt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
rektorem	rektor	k1gMnSc7
v	v	k7c6
letech	let	k1gInPc6
1992	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
něm	on	k3xPp3gInSc6
se	se	k3xPyFc4
na	na	k7c4
dvě	dva	k4xCgNnPc4
tříletá	tříletý	k2eAgNnPc4d1
funkční	funkční	k2eAgNnPc4d1
období	období	k1gNnPc4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
rektorem	rektor	k1gMnSc7
stal	stát	k5eAaPmAgMnS
někdejší	někdejší	k2eAgMnSc1d1
Jelínkův	Jelínkův	k2eAgMnSc1d1
proděkan	proděkan	k1gMnSc1
a	a	k8xC
profesor	profesor	k1gMnSc1
informatiky	informatika	k1gFnSc2
Jiří	Jiří	k1gMnSc1
Zlatuška	Zlatuška	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zastával	zastávat	k5eAaImAgMnS
funkci	funkce	k1gFnSc4
děkana	děkan	k1gMnSc2
fakulty	fakulta	k1gFnSc2
informatiky	informatika	k1gFnSc2
<g/>
,	,	kIx,
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
založil	založit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
byl	být	k5eAaImAgInS
rektorem	rektor	k1gMnSc7
profesor	profesor	k1gMnSc1
politologie	politologie	k1gFnSc2
Petr	Petr	k1gMnSc1
Fiala	Fiala	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
ministr	ministr	k1gMnSc1
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
až	až	k9
2019	#num#	k4
byl	být	k5eAaImAgInS
po	po	k7c4
dvě	dva	k4xCgFnPc4
volební	volební	k2eAgFnPc4d1
období	období	k1gNnSc6
rektorem	rektor	k1gMnSc7
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
docent	docent	k1gMnSc1
muzikologie	muzikologie	k1gFnSc2
Mikuláš	Mikuláš	k1gMnSc1
Bek	bek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
mandát	mandát	k1gInSc1
vypršel	vypršet	k5eAaPmAgInS
na	na	k7c6
konci	konec	k1gInSc6
srpna	srpen	k1gInSc2
2019	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
nástupcem	nástupce	k1gMnSc7
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2019	#num#	k4
stal	stát	k5eAaPmAgMnS
neurolog	neurolog	k1gMnSc1
Martin	Martin	k1gMnSc1
Bareš	Bareš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Rektoři	rektor	k1gMnPc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Čestné	čestný	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc4
držitelů	držitel	k1gMnPc2
čestných	čestný	k2eAgInPc2d1
doktorátů	doktorát	k1gInPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
dobu	doba	k1gFnSc4
své	svůj	k3xOyFgFnSc2
existence	existence	k1gFnSc2
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
udělila	udělit	k5eAaPmAgFnS
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
čestný	čestný	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
(	(	kIx(
<g/>
doctor	doctor	k1gInSc4
honoris	honoris	k1gFnSc2
causa	causa	k1gFnSc1
<g/>
)	)	kIx)
120	#num#	k4
osobám	osoba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
nositelem	nositel	k1gMnSc7
čestného	čestný	k2eAgInSc2d1
doktorátu	doktorát	k1gInSc2
byl	být	k5eAaImAgMnS
skladatel	skladatel	k1gMnSc1
Leoš	Leoš	k1gMnSc1
Janáček	Janáček	k1gMnSc1
<g/>
,	,	kIx,
čestný	čestný	k2eAgInSc4d1
doktorát	doktorát	k1gInSc4
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
obdrželi	obdržet	k5eAaPmAgMnP
také	také	k9
syn	syn	k1gMnSc1
T.	T.	kA
G.	G.	kA
Masaryka	Masaryk	k1gMnSc2
Jan	Jan	k1gMnSc1
Masaryk	Masaryk	k1gMnSc1
<g/>
,	,	kIx,
Masarykův	Masarykův	k2eAgMnSc1d1
spolupracovník	spolupracovník	k1gMnSc1
a	a	k8xC
nástupce	nástupce	k1gMnSc1
v	v	k7c6
prezidentské	prezidentský	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
Edvard	Edvard	k1gMnSc1
Beneš	Beneš	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Havel	Havel	k1gMnSc1
nebo	nebo	k8xC
Josef	Josef	k1gMnSc1
Škvorecký	Škvorecký	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
</s>
<s>
Úkolem	úkol	k1gInSc7
Mezinárodní	mezinárodní	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
rady	rada	k1gFnSc2
je	být	k5eAaImIp3nS
pomáhat	pomáhat	k5eAaImF
univerzitě	univerzita	k1gFnSc3
při	při	k7c6
vytváření	vytváření	k1gNnSc6
vědeckých	vědecký	k2eAgFnPc2d1
strategií	strategie	k1gFnPc2
a	a	k8xC
poskytovat	poskytovat	k5eAaImF
nezávislé	závislý	k2eNgNnSc4d1
posouzení	posouzení	k1gNnSc4
a	a	k8xC
poradenství	poradenství	k1gNnSc4
ve	v	k7c6
vědeckých	vědecký	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
rada	rada	k1gFnSc1
ve	v	k7c6
složení	složení	k1gNnSc6
profesor	profesor	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Jiřičný	Jiřičný	k2eAgMnSc1d1
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
Ústavu	ústav	k1gInSc2
molekulárního	molekulární	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
rakoviny	rakovina	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
Curych	Curych	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Peter	Peter	k1gMnSc1
Williamson	Williamson	k1gMnSc1
(	(	kIx(
<g/>
ředitel	ředitel	k1gMnSc1
studií	studie	k1gFnPc2
managementu	management	k1gInSc2
na	na	k7c4
Cambridge	Cambridge	k1gFnPc4
Judge	Judg	k1gInSc2
Business	business	k1gInSc1
School	School	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Henzinger	Henzinger	k1gMnSc1
(	(	kIx(
<g/>
Švýcarský	švýcarský	k2eAgInSc1d1
federální	federální	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
v	v	k7c6
Lausanne	Lausanne	k1gNnSc6
<g/>
)	)	kIx)
a	a	k8xC
profesorka	profesorka	k1gFnSc1
Marie-Janine	Marie-Janin	k1gInSc5
Calic	Calic	k1gMnSc1
(	(	kIx(
<g/>
historička	historička	k1gFnSc1
specializující	specializující	k2eAgFnSc1d1
se	se	k3xPyFc4
na	na	k7c4
východoevropské	východoevropský	k2eAgFnPc4d1
a	a	k8xC
jihoevropské	jihoevropský	k2eAgFnPc4d1
dějiny	dějiny	k1gFnPc4
<g/>
,	,	kIx,
Mnichovská	mnichovský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
)	)	kIx)
sešla	sejít	k5eAaPmAgFnS
17	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednatelkou	jednatelka	k1gFnSc7
rady	rada	k1gFnSc2
je	být	k5eAaImIp3nS
molekulární	molekulární	k2eAgFnSc1d1
bioložka	bioložka	k1gFnSc1
Mary	Mary	k1gFnSc2
O	o	k7c6
<g/>
’	’	k?
<g/>
Connel	Connlo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yQgNnPc1,k3yIgNnPc1
působí	působit	k5eAaImIp3nP
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
rámci	rámec	k1gInSc6
programu	program	k1gInSc2
ERA	ERA	kA
Chair	Chair	k1gInSc1
od	od	k7c2
roku	rok	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
se	se	k3xPyFc4
schází	scházet	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
jednou	jeden	k4xCgFnSc7
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
se	se	k3xPyFc4
členové	člen	k1gMnPc1
rady	rada	k1gFnSc2
opakovaně	opakovaně	k6eAd1
vyjádřili	vyjádřit	k5eAaPmAgMnP
k	k	k7c3
nastavení	nastavení	k1gNnSc3
doktorského	doktorský	k2eAgNnSc2d1
studia	studio	k1gNnSc2
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Grantová	grantový	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
</s>
<s>
Grantová	grantový	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
(	(	kIx(
<g/>
GAMU	game	k1gInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
interní	interní	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
poskytující	poskytující	k2eAgInSc4d1
finanční	finanční	k2eAgInSc4d1
prostředky	prostředek	k1gInPc4
pro	pro	k7c4
výzkum	výzkum	k1gInSc4
studentům	student	k1gMnPc3
<g/>
,	,	kIx,
vědcům	vědec	k1gMnPc3
a	a	k8xC
celým	celý	k2eAgInPc3d1
výzkumným	výzkumný	k2eAgInPc3d1
týmům	tým	k1gInPc3
z	z	k7c2
řad	řada	k1gFnPc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
i	i	k8xC
externím	externí	k2eAgMnPc3d1
vědcům	vědec	k1gMnPc3
ve	v	k7c6
všech	všecek	k3xTgNnPc6
stádiích	stádium	k1gNnPc6
jejich	jejich	k3xOp3gFnSc2
vědecké	vědecký	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
prostřednictvím	prostřednictvím	k7c2
různých	různý	k2eAgNnPc2d1
grantových	grantový	k2eAgNnPc2d1
schémat	schéma	k1gNnPc2
<g/>
:	:	kIx,
</s>
<s>
HORIZONS	HORIZONS	kA
–	–	k?
Podpora	podpora	k1gFnSc1
přípravy	příprava	k1gFnSc2
mezinárodních	mezinárodní	k2eAgInPc2d1
grantů	grant	k1gInPc2
</s>
<s>
INTERDISCIPLINARY	INTERDISCIPLINARY	kA
–	–	k?
Mezioborové	mezioborový	k2eAgInPc4d1
výzkumné	výzkumný	k2eAgInPc4d1
projekty	projekt	k1gInPc4
</s>
<s>
MASH	MASH	kA
–	–	k?
MUNI	MUNI	k?
Award	Award	k1gInSc1
in	in	k?
Science	Science	k1gFnSc1
and	and	k?
Humanities	Humanities	k1gInSc1
</s>
<s>
MASH	MASH	kA
JUNIOR	junior	k1gMnSc1
–	–	k?
MUNI	MUNI	k?
Award	Award	k1gInSc1
in	in	k?
Science	Science	k1gFnSc2
and	and	k?
Humanities	Humanities	k1gMnSc1
JUNIOR	junior	k1gMnSc1
</s>
<s>
CAREER	CAREER	kA
RESTART	RESTART	kA
–	–	k?
Podpora	podpora	k1gFnSc1
začlenění	začlenění	k1gNnSc2
vědeckých	vědecký	k2eAgInPc2d1
pracovníků	pracovník	k1gMnPc2
po	po	k7c6
přerušení	přerušení	k1gNnSc6
kariéry	kariéra	k1gFnSc2
</s>
<s>
MUNI	MUNI	k?
SCIENTIST	SCIENTIST	kA
–	–	k?
Cena	cena	k1gFnSc1
za	za	k7c4
vynikající	vynikající	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
výzkumu	výzkum	k1gInSc2
</s>
<s>
Cílem	cíl	k1gInSc7
GAMU	game	k1gInSc2
je	být	k5eAaImIp3nS
posílit	posílit	k5eAaPmF
výzkumné	výzkumný	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
na	na	k7c6
MU	MU	kA
i	i	k9
v	v	k7c6
celém	celý	k2eAgInSc6d1
Jihomoravském	jihomoravský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
podpořit	podpořit	k5eAaPmF
inovativní	inovativní	k2eAgInSc4d1
mezioborový	mezioborový	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
,	,	kIx,
zvýšit	zvýšit	k5eAaPmF
prestiž	prestiž	k1gFnSc4
vědecké	vědecký	k2eAgFnSc2d1
práce	práce	k1gFnSc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
také	také	k9
zlepšit	zlepšit	k5eAaPmF
úspěšnost	úspěšnost	k1gFnSc4
univerzity	univerzita	k1gFnSc2
v	v	k7c6
získávání	získávání	k1gNnSc6
prestižních	prestižní	k2eAgInPc2d1
zahraničních	zahraniční	k2eAgInPc2d1
grantů	grant	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Insignie	insignie	k1gFnPc1
univerzity	univerzita	k1gFnSc2
</s>
<s>
rektorský	rektorský	k2eAgInSc1d1
řetěz	řetěz	k1gInSc1
s	s	k7c7
medailí	medaile	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
vyobrazen	vyobrazen	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
Garrigue	Garrigu	k1gFnSc2
Masaryk	Masaryk	k1gMnSc1
(	(	kIx(
<g/>
avers	avers	k1gInSc1
<g/>
,	,	kIx,
revers	revers	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
rektorské	rektorský	k2eAgNnSc1d1
žezlo	žezlo	k1gNnSc1
(	(	kIx(
<g/>
obrázek	obrázek	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Zkratka	zkratka	k1gFnSc1
„	„	k?
<g/>
Muni	Muni	k?
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
MUNI	MUNI	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
používaná	používaný	k2eAgFnSc1d1
jakožto	jakožto	k8xS
internetová	internetový	k2eAgFnSc1d1
doména	doména	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
především	především	k9
v	v	k7c6
méně	málo	k6eAd2
formálních	formální	k2eAgInPc6d1
kontextech	kontext	k1gInPc6
a	a	k8xC
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
„	„	k?
<g/>
MU	MU	kA
<g/>
“	“	k?
se	se	k3xPyFc4
nevyskytuje	vyskytovat	k5eNaImIp3nS
ve	v	k7c6
statutu	statut	k1gInSc6
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Před	před	k7c7
rokem	rok	k1gInSc7
1919	#num#	k4
existovala	existovat	k5eAaImAgFnS
jen	jen	k9
pražská	pražský	k2eAgFnSc1d1
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1882	#num#	k4
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
českou	český	k2eAgFnSc4d1
a	a	k8xC
německou	německý	k2eAgFnSc4d1
univerzitu	univerzita	k1gFnSc4
<g/>
;	;	kIx,
z	z	k7c2
původní	původní	k2eAgFnSc2d1
olomoucké	olomoucký	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
zrušené	zrušený	k2eAgFnSc2d1
jako	jako	k8xC,k8xS
celek	celek	k1gInSc1
roku	rok	k1gInSc2
1860	#num#	k4
<g/>
,	,	kIx,
zbyla	zbýt	k5eAaPmAgFnS
jen	jen	k9
samostatná	samostatný	k2eAgFnSc1d1
teologická	teologický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
;	;	kIx,
ostatní	ostatní	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
působící	působící	k2eAgFnPc1d1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
nebyly	být	k5eNaImAgFnP
univerzitami	univerzita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
okamžiku	okamžik	k1gInSc6
svého	svůj	k3xOyFgNnSc2
založení	založení	k1gNnSc2
stala	stát	k5eAaPmAgFnS
druhou	druhý	k4xOgFnSc7
českou	český	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podrobněji	podrobně	k6eAd2
se	se	k3xPyFc4
tímto	tento	k3xDgNnSc7
tématem	téma	k1gNnSc7
zabývá	zabývat	k5eAaImIp3nS
sekce	sekce	k1gFnSc1
Historie	historie	k1gFnSc2
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
z	z	k7c2
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1960	#num#	k4
stanovilo	stanovit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
Název	název	k1gInSc1
university	universita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
zní	znět	k5eAaImIp3nS
Universita	universita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nová	nový	k2eAgFnSc1d1
kvestorka	kvestorka	k1gFnSc1
a	a	k8xC
prorektorka	prorektorka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muni	Muni	k?
:	:	kIx,
měsíčník	měsíčník	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
PDF	PDF	kA
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1801	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
806	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
135	#num#	k4
<g/>
,	,	kIx,
151	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Zákon	zákon	k1gInSc1
č.	č.	k?
50	#num#	k4
<g/>
/	/	kIx~
<g/>
1919	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
z.	z.	k?
a	a	k8xC
nař	nař	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
se	se	k3xPyFc4
zřizuje	zřizovat	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
česká	český	k2eAgFnSc1d1
universita	universita	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
jsme	být	k5eAaImIp1nP
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Univerzitní	univerzitní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
Telč	Telč	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
QS	QS	kA
World	World	k1gMnSc1
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc1
2013	#num#	k4
(	(	kIx(
<g/>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
QS	QS	kA
Quacquarelli	Quacquarelle	k1gFnSc4
Symonds	Symonds	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CHUMCHALOVÁ	CHUMCHALOVÁ	kA
<g/>
,	,	kIx,
Markéta	Markéta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
jsou	být	k5eAaImIp3nP
světové	světový	k2eAgFnPc1d1
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgInPc4d1
průzkumy	průzkum	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brněnský	brněnský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
REDAKCE	redakce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martin	Martin	k1gMnSc1
Bareš	Bareš	k1gMnSc1
se	se	k3xPyFc4
ujal	ujmout	k5eAaPmAgMnS
vedení	vedení	k1gNnSc4
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
em	em	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
STRNADOVÁ	Strnadová	k1gFnSc1
<g/>
,	,	kIx,
Tereza	Tereza	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
získala	získat	k5eAaPmAgFnS
mučedníka	mučedník	k1gMnSc4
<g/>
,	,	kIx,
ještě	ještě	k6eAd1
než	než	k8xS
vznikla	vzniknout	k5eAaPmAgFnS
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2009-01-28	2009-01-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tisk	tisk	k1gInSc1
375	#num#	k4
–	–	k?
Zpráva	zpráva	k1gFnSc1
školského	školský	k2eAgInSc2d1
výboru	výbor	k1gInSc2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
československého	československý	k2eAgNnSc2d1
o	o	k7c6
návrhu	návrh	k1gInSc6
posl	posl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiráska	Jirásek	k1gMnSc2
<g/>
,	,	kIx,
Dra	dřít	k5eAaImSgInS
Engliše	Engliše	k1gFnSc1
a	a	k8xC
soudr	soudr	k1gInSc1
<g/>
.	.	kIx.
na	na	k7c6
zřízení	zřízení	k1gNnSc6
university	universita	k1gFnSc2
s	s	k7c7
českou	český	k2eAgFnSc7d1
vyučovací	vyučovací	k2eAgFnSc7d1
řečí	řeč	k1gFnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
1919-01-09	1919-01-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Historie	historie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zapomenuté	zapomenutý	k2eAgNnSc4d1
logo	logo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Newsletter	Newsletter	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykovy	Masarykův	k2eAgFnPc4d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
§	§	k?
10	#num#	k4
vládního	vládní	k2eAgNnSc2d1
nařízení	nařízení	k1gNnSc2
č.	č.	k?
120	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
změnách	změna	k1gFnPc6
v	v	k7c6
organizaci	organizace	k1gFnSc6
vysokých	vysoký	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
48	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
změně	změna	k1gFnSc6
názvu	název	k1gInSc2
University	universita	k1gFnSc2
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
původnímu	původní	k2eAgInSc3d1
názvu	název	k1gInSc3
<g/>
.	.	kIx.
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-01-05	2006-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1801	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
814	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
552	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
zákon	zákon	k1gInSc1
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
některé	některý	k3yIgInPc1
další	další	k2eAgInPc1d1
zákony	zákon	k1gInPc1
(	(	kIx(
<g/>
Čl	čl	kA
<g/>
.	.	kIx.
I.	I.	kA
bod	bod	k1gInSc4
62	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
pdf	pdf	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FRÁNEK	FRÁNEK	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
nemá	mít	k5eNaImIp3nS
v	v	k7c6
názvu	název	k1gInSc6
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-01-05	2006-01-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HORÁK	Horák	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Změnil	změnit	k5eAaPmAgInS
se	se	k3xPyFc4
název	název	k1gInSc1
naší	náš	k3xOp1gFnSc2
univerzity	univerzita	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
veda	vést	k5eAaImSgMnS
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-02-22	2006-02-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1805	#num#	k4
<g/>
-	-	kIx~
<g/>
9759	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
137	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
zákon	zákon	k1gInSc1
č.	č.	k?
111	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
a	a	k8xC
doplnění	doplnění	k1gNnSc6
dalších	další	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
(	(	kIx(
<g/>
zákon	zákon	k1gInSc1
o	o	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
některé	některý	k3yIgInPc1
další	další	k2eAgInPc1d1
zákony	zákon	k1gInPc1
(	(	kIx(
<g/>
část	část	k1gFnSc4
první	první	k4xOgFnPc4
<g/>
,	,	kIx,
bod	bod	k1gInSc1
292	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
pdf	pdf	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Alternativní	alternativní	k2eAgInSc1d1
odkaz	odkaz	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
APPLICATION	APPLICATION	kA
FOR	forum	k1gNnPc2
THE	THE	kA
EUNIS	EUNIS	kA
ELITE	ELITE	kA
AWARD	AWARD	kA
2005	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fakulta	fakulta	k1gFnSc1
informatiky	informatika	k1gFnSc2
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
2005-05-20	2005-05-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Firemní	firemní	k2eAgNnSc4d1
médium	médium	k1gNnSc4
roku	rok	k1gInSc2
2012	#num#	k4
–	–	k?
vyhlášení	vyhlášení	k1gNnSc2
výsledků	výsledek	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komora	komora	k1gFnSc1
Public	publicum	k1gNnPc2
Relations	Relationsa	k1gFnPc2
<g/>
,	,	kIx,
2013-10-03	2013-10-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Výroční	výroční	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2015	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
57	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hokejový	hokejový	k2eAgInSc1d1
souboj	souboj	k1gInSc1
univerzit	univerzita	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
/	/	kIx~
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Brně	Brno	k1gNnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FOJTŮ	Fojt	k1gMnPc2
<g/>
,	,	kIx,
Martina	Martin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
mění	měnit	k5eAaImIp3nS
logo	logo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodá	dodat	k5eAaPmIp3nS
ho	on	k3xPp3gInSc4
Studio	studio	k1gNnSc1
Najbrt	Najbrt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muni	Muni	k?
:	:	kIx,
měsíčník	měsíčník	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2017-12-08	2017-12-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Farmaceutická	farmaceutický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
se	s	k7c7
dneškem	dnešek	k1gInSc7
stává	stávat	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muni	Muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2020-07-01	2020-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KUDĚLKOVÁ	KUDĚLKOVÁ	kA
<g/>
,	,	kIx,
Lenka	Lenka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nerealizovaná	realizovaný	k2eNgFnSc1d1
výstavba	výstavba	k1gFnSc1
univerzitního	univerzitní	k2eAgNnSc2d1
městečka	městečko	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
Brno	Brno	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Počítačové	počítačový	k2eAgFnSc2d1
studovny	studovna	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Menš	Menš	k1gInSc1
<g/>
,	,	kIx,
Kal	kal	k1gInSc1
<g/>
.	.	kIx.
prof.	prof.	kA
JUDr.	JUDr.	kA
Rudolf	Rudolf	k1gMnSc1
Dominik	Dominik	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
dějin	dějiny	k1gFnPc2
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-01-13	2014-01-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čestné	čestný	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
udělené	udělený	k2eAgInPc1d1
MU	MU	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WIESNEROVÁ	Wiesnerová	k1gFnSc1
<g/>
,	,	kIx,
Ema	Ema	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykově	Masarykův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
pomůže	pomoct	k5eAaPmIp3nS
nová	nový	k2eAgFnSc1d1
Mezinárodní	mezinárodní	k2eAgFnSc1d1
vědecká	vědecký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muni	Muni	k?
:	:	kIx,
měsíčník	měsíčník	k1gInSc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-10-16	2016-10-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1801	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
814	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
WIESNEROVÁ	Wiesnerová	k1gFnSc1
<g/>
,	,	kIx,
Ema	Ema	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědecká	vědecký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
:	:	kIx,
Univerzita	univerzita	k1gFnSc1
potřebuje	potřebovat	k5eAaImIp3nS
zlepšit	zlepšit	k5eAaPmF
práci	práce	k1gFnSc4
s	s	k7c7
doktorandy	doktorand	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Magazín	magazín	k1gInSc1
M	M	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2019-1-25	2019-1-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
FASORA	FASORA	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
;	;	kIx,
HANUŠ	Hanuš	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
:	:	kIx,
příběh	příběh	k1gInSc1
vzdělání	vzdělání	k1gNnSc2
a	a	k8xC
vědy	věda	k1gFnSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
271	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
4850	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HALAS	Halas	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Xaver	Xaver	k1gMnSc1
<g/>
;	;	kIx,
JORDÁN	Jordán	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumenty	dokument	k1gInPc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jiřina	Jiřina	k1gFnSc1
Štouračová	Štouračová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
442	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
861	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
HALAS	Halas	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Xaver	Xaver	k1gMnSc1
<g/>
;	;	kIx,
JORDÁN	Jordán	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokumenty	dokument	k1gInPc1
k	k	k7c3
dějinám	dějiny	k1gFnPc3
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Jiřina	Jiřina	k1gFnSc1
Štouračová	Štouračová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svazek	svazek	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
354	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
210	#num#	k4
<g/>
-	-	kIx~
<g/>
1200	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
se	se	k3xPyFc4
zřizuje	zřizovat	k5eAaImIp3nS
druhá	druhý	k4xOgFnSc1
česká	český	k2eAgFnSc1d1
universita	universita	k1gFnSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Magazín	magazín	k1gInSc1
M	M	kA
<g/>
,	,	kIx,
zprávy	zpráva	k1gFnPc1
z	z	k7c2
MUNI	MUNI	k?
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Fakulty	fakulta	k1gFnSc2
</s>
<s>
Současné	současný	k2eAgNnSc1d1
</s>
<s>
Právnická	právnický	k2eAgFnSc1d1
•	•	k?
Lékařská	lékařský	k2eAgFnSc1d1
•	•	k?
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
•	•	k?
Filozofická	filozofický	k2eAgFnSc1d1
•	•	k?
Pedagogická	pedagogický	k2eAgFnSc1d1
•	•	k?
Farmaceutická	farmaceutický	k2eAgFnSc1d1
•	•	k?
Ekonomicko-správní	ekonomicko-správní	k2eAgFnSc2d1
•	•	k?
Informatiky	informatika	k1gFnSc2
•	•	k?
Sociálních	sociální	k2eAgNnPc2d1
studií	studio	k1gNnPc2
•	•	k?
Sportovních	sportovní	k2eAgFnPc2d1
studií	studie	k1gFnPc2
Bývalé	bývalý	k2eAgNnSc1d1
</s>
<s>
Filozofická	filozofický	k2eAgFnSc1d1
v	v	k7c6
Opavě	Opava	k1gFnSc6
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
součást	součást	k1gFnSc4
Slezské	slezský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Obchodně	obchodně	k6eAd1
podnikatelská	podnikatelský	k2eAgFnSc1d1
v	v	k7c6
Karviné	Karviná	k1gFnSc6
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
součást	součást	k1gFnSc4
Slezské	slezský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Rektorát	rektorát	k1gInSc1
</s>
<s>
Rektorát	rektorát	k1gInSc1
Ústavy	ústava	k1gFnSc2
</s>
<s>
Ústav	ústav	k1gInSc1
výpočetní	výpočetní	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
•	•	k?
Středoevropský	středoevropský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
Knihovny	knihovna	k1gFnSc2
</s>
<s>
Knihovny	knihovna	k1gFnPc4
Semináře	seminář	k1gInSc2
dějin	dějiny	k1gFnPc2
umění	umění	k1gNnSc2
•	•	k?
Ústřední	ústřední	k2eAgFnSc1d1
knihovna	knihovna	k1gFnSc1
Pedagogické	pedagogický	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Jiná	jiný	k2eAgNnPc1d1
pracoviště	pracoviště	k1gNnPc1
</s>
<s>
Archiv	archiv	k1gInSc1
•	•	k?
Centrum	centrum	k1gNnSc1
jazykového	jazykový	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
•	•	k?
Centrum	centrum	k1gNnSc1
zahraniční	zahraniční	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
•	•	k?
Středisko	středisko	k1gNnSc1
pro	pro	k7c4
pomoc	pomoc	k1gFnSc4
studentům	student	k1gMnPc3
se	s	k7c7
specifickými	specifický	k2eAgInPc7d1
nároky	nárok	k1gInPc7
•	•	k?
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
transfer	transfer	k1gInSc4
technologií	technologie	k1gFnPc2
•	•	k?
Institut	institut	k1gInSc1
biostatistiky	biostatistika	k1gFnSc2
a	a	k8xC
analýz	analýza	k1gFnPc2
•	•	k?
Mendelovo	Mendelův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Centrum	centrum	k1gNnSc1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
,	,	kIx,
výzkumu	výzkum	k1gInSc2
a	a	k8xC
inovací	inovace	k1gFnPc2
pro	pro	k7c4
ICT	ICT	kA
•	•	k?
Centrální	centrální	k2eAgFnSc1d1
řídící	řídící	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
projektu	projekt	k1gInSc2
CEITEC	CEITEC	kA
•	•	k?
Univerzitní	univerzitní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
Telč	Telč	k1gFnSc1
Účelová	účelový	k2eAgFnSc1d1
zařízení	zařízení	k1gNnPc2
</s>
<s>
Správa	správa	k1gFnSc1
kolejí	kolej	k1gFnPc2
a	a	k8xC
menz	menza	k1gFnPc2
•	•	k?
Nakladatelství	nakladatelství	k1gNnSc1
•	•	k?
Univerzitní	univerzitní	k2eAgInSc4d1
kampus	kampus	k1gInSc4
Bohunice	Bohunice	k1gFnPc4
Orgány	orgán	k1gInPc1
</s>
<s>
Akademický	akademický	k2eAgInSc1d1
senát	senát	k1gInSc1
•	•	k?
Rektor	rektor	k1gMnSc1
•	•	k?
Vědecká	vědecký	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
Správní	správní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
•	•	k?
Kvestor	kvestor	k1gMnSc1
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Rektoři	rektor	k1gMnPc1
•	•	k?
Děkani	děkan	k1gMnPc1
•	•	k?
Profesoři	profesor	k1gMnPc1
•	•	k?
Čestné	čestný	k2eAgInPc1d1
doktoráty	doktorát	k1gInPc1
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Masarykovy	Masarykův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
polární	polární	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
•	•	k?
Kino	kino	k1gNnSc1
Scala	scát	k5eAaImAgFnS
•	•	k?
Informační	informační	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Knihovny	knihovna	k1gFnSc2
•	•	k?
Časopisy	časopis	k1gInPc1
•	•	k?
Filmový	filmový	k2eAgInSc1d1
festival	festival	k1gInSc1
FI	fi	k0
•	•	k?
Freedom	Freedom	k1gInSc1
Lecture	Lectur	k1gMnSc5
•	•	k?
Průšovo	Průšův	k2eAgNnSc1d1
umělecké-úchylné	umělecké-úchylný	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
•	•	k?
Spolek	spolek	k1gInSc4
absolventů	absolvent	k1gMnPc2
a	a	k8xC
přátel	přítel	k1gMnPc2
•	•	k?
Theodor	Theodora	k1gFnPc2
Herzl	Herzl	k1gMnSc1
Chair	Chair	k1gMnSc1
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
vysoké	vysoký	k2eAgFnPc1d1
školy	škola	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
Praha	Praha	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Akademie	akademie	k1gFnSc2
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Česká	český	k2eAgFnSc1d1
zemědělská	zemědělský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
České	český	k2eAgFnSc6d1
vysoké	vysoká	k1gFnSc6
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Univerzita	univerzita	k1gFnSc1
Karlova	Karlův	k2eAgFnSc1d1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
chemicko-technologická	chemicko-technologický	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1
v	v	k7c6
Praze	Praha	k1gFnSc6
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Jihočeská	jihočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
technická	technický	k2eAgFnSc1d1
a	a	k8xC
ekonomická	ekonomický	k2eAgFnSc1d1
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Západočeská	západočeský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
Liberec	Liberec	k1gInSc1
</s>
<s>
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Liberci	Liberec	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
Pardubice	Pardubice	k1gInPc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
polytechnická	polytechnický	k2eAgFnSc1d1
Jihlava	Jihlava	k1gFnSc1
Brno	Brno	k1gNnSc1
</s>
<s>
Janáčkova	Janáčkův	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
•	•	k?
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Mendelova	Mendelův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
•	•	k?
Veterinární	veterinární	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Brno	Brno	k1gNnSc1
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc4
technické	technický	k2eAgNnSc4d1
v	v	k7c6
Brně	Brno	k1gNnSc6
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Palackého	Palacký	k1gMnSc2
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Ostravská	ostravský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
•	•	k?
Vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
báňská	báňský	k2eAgFnSc1d1
–	–	k?
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
Opava	Opava	k1gFnSc1
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
Opavě	Opava	k1gFnSc6
Zlín	Zlín	k1gInSc1
</s>
<s>
Univerzita	univerzita	k1gFnSc1
Tomáše	Tomáš	k1gMnSc2
Bati	Baťa	k1gMnSc2
ve	v	k7c6
Zlíně	Zlín	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ko	ko	k?
<g/>
2007248433	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
36123-9	36123-9	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2179	#num#	k4
6261	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
91097059	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146719391	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
91097059	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Brno	Brno	k1gNnSc1
|	|	kIx~
Kultura	kultura	k1gFnSc1
</s>
