<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Koppitz	Koppitz	k1gMnSc1	Koppitz
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1884	[number]	k4	1884
Skrbovice	Skrbovice	k1gFnSc1	Skrbovice
u	u	k7c2	u
Bruntálu	Bruntál	k1gInSc2	Bruntál
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
Perchtoldsdorf	Perchtoldsdorf	k1gInSc1	Perchtoldsdorf
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
fotograf	fotograf	k1gMnSc1	fotograf
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Koppitz	Koppitz	k1gMnSc1	Koppitz
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
fotografy	fotograf	k1gMnPc7	fotograf
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
rakouského	rakouský	k2eAgMnSc4d1	rakouský
umělce	umělec	k1gMnSc4	umělec
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Bruntálu	Bruntál	k1gInSc2	Bruntál
do	do	k7c2	do
chudé	chudý	k2eAgFnSc2d1	chudá
evangelické	evangelický	k2eAgFnSc2d1	evangelická
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
odborné	odborný	k2eAgFnSc2d1	odborná
školy	škola	k1gFnSc2	škola
chodil	chodit	k5eAaImAgMnS	chodit
jen	jen	k9	jen
díky	díky	k7c3	díky
stipendiu	stipendium	k1gNnSc3	stipendium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1897	[number]	k4	1897
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
k	k	k7c3	k
fotografovi	fotograf	k1gMnSc3	fotograf
Robertu	Robert	k1gMnSc3	Robert
Rotterovi	Rotter	k1gMnSc3	Rotter
z	z	k7c2	z
Bruntálu	Bruntál	k1gInSc2	Bruntál
<g/>
,	,	kIx,	,
zkoušku	zkouška	k1gFnSc4	zkouška
vykonal	vykonat	k5eAaPmAgMnS	vykonat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Koppitz	Koppitz	k1gInSc1	Koppitz
začínal	začínat	k5eAaImAgInS	začínat
kariéru	kariéra	k1gFnSc4	kariéra
zakázkového	zakázkový	k2eAgMnSc2d1	zakázkový
fotografa	fotograf	k1gMnSc2	fotograf
v	v	k7c6	v
malých	malý	k2eAgInPc6d1	malý
ateliérech	ateliér	k1gInPc6	ateliér
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
ateliérech	ateliér	k1gInPc6	ateliér
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
a	a	k8xC	a
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
u	u	k7c2	u
C.	C.	kA	C.
Pietznere	Pietzner	k1gMnSc5	Pietzner
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
ateliéry	ateliér	k1gInPc7	ateliér
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
Sankt-Petěrburgu	Sankt-Petěrburg	k1gInSc6	Sankt-Petěrburg
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prostředí	prostředí	k1gNnSc1	prostředí
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
hrálo	hrát	k5eAaImAgNnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
prominentním	prominentní	k2eAgNnSc7d1	prominentní
publikem	publikum	k1gNnSc7	publikum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zanechat	zanechat	k5eAaPmF	zanechat
živnosti	živnost	k1gFnPc4	živnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
začal	začít	k5eAaPmAgInS	začít
opět	opět	k6eAd1	opět
chodit	chodit	k5eAaImF	chodit
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
na	na	k7c4	na
Grafický	grafický	k2eAgInSc4d1	grafický
učební	učební	k2eAgInSc4d1	učební
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
ústav	ústav	k1gInSc4	ústav
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
Koppitz	Koppitz	k1gMnSc1	Koppitz
poznal	poznat	k5eAaPmAgMnS	poznat
řadů	řad	k1gInPc2	řad
svých	svůj	k3xOyFgMnPc2	svůj
budoucích	budoucí	k2eAgMnPc2d1	budoucí
kolegů	kolega	k1gMnPc2	kolega
(	(	kIx(	(
<g/>
např.	např.	kA	např.
fotografa	fotograf	k1gMnSc2	fotograf
J.	J.	kA	J.
A.	A.	kA	A.
Trčka	Trčka	k1gMnSc1	Trčka
nebo	nebo	k8xC	nebo
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
grafika	grafik	k1gMnSc2	grafik
Egona	Egon	k1gMnSc2	Egon
Schieleho	Schiele	k1gMnSc2	Schiele
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koppitz	Koppitz	k1gMnSc1	Koppitz
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gInSc7	člen
"	"	kIx"	"
<g/>
Wiener	Wiener	k1gInSc1	Wiener
Camera	Camero	k1gNnSc2	Camero
Clubu	club	k1gInSc2	club
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
volné	volný	k2eAgFnSc6d1	volná
tvorbě	tvorba	k1gFnSc6	tvorba
používal	používat	k5eAaImAgMnS	používat
techniky	technika	k1gFnPc4	technika
uhlotisku	uhlotisk	k1gInSc2	uhlotisk
<g/>
,	,	kIx,	,
bromolejotisku	bromolejotisk	k1gInSc2	bromolejotisk
a	a	k8xC	a
gumotisku	gumotisk	k1gInSc2	gumotisk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnPc6	jeho
fotografiích	fotografia	k1gFnPc6	fotografia
jsou	být	k5eAaImIp3nP	být
zachyceny	zachycen	k2eAgFnPc1d1	zachycena
významné	významný	k2eAgFnPc1d1	významná
vídeňské	vídeňský	k2eAgFnPc1d1	Vídeňská
památky	památka	k1gFnPc1	památka
a	a	k8xC	a
náladové	náladový	k2eAgFnPc1d1	náladová
krajiny	krajina	k1gFnPc1	krajina
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
zachycena	zachycen	k2eAgFnSc1d1	zachycena
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
nezkulturněná	zkulturněný	k2eNgFnSc1d1	zkulturněný
příroda	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Koppitzovy	Koppitzův	k2eAgFnPc1d1	Koppitzův
fotografie	fotografia	k1gFnPc1	fotografia
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
hory	hora	k1gFnPc4	hora
a	a	k8xC	a
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
asistentem	asistent	k1gMnSc7	asistent
a	a	k8xC	a
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
Grafickém	grafický	k2eAgInSc6d1	grafický
institutu	institut	k1gInSc6	institut
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
své	svůj	k3xOyFgFnSc2	svůj
tvorby	tvorba	k1gFnSc2	tvorba
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
Koppitz	Koppitz	k1gInSc4	Koppitz
inscenované	inscenovaný	k2eAgInPc4d1	inscenovaný
skupinové	skupinový	k2eAgInPc4d1	skupinový
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
první	první	k4xOgFnPc1	první
práce	práce	k1gFnPc1	práce
byly	být	k5eAaImAgFnP	být
ovlivněny	ovlivnit	k5eAaPmNgFnP	ovlivnit
Gustavem	Gustav	k1gMnSc7	Gustav
Klimtem	Klimto	k1gNnSc7	Klimto
a	a	k8xC	a
japonským	japonský	k2eAgNnSc7d1	Japonské
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
před	před	k7c7	před
1	[number]	k4	1
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
fotografoval	fotografovat	k5eAaImAgInS	fotografovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
autoportréty	autoportrét	k1gInPc4	autoportrét
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
autora	autor	k1gMnSc4	autor
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
si	se	k3xPyFc3	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
harmonii	harmonie	k1gFnSc4	harmonie
a	a	k8xC	a
precizní	precizní	k2eAgFnSc4d1	precizní
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téměř	téměř	k6eAd1	téměř
k	k	k7c3	k
dokumentární	dokumentární	k2eAgFnSc3d1	dokumentární
fotografii	fotografia	k1gFnSc3	fotografia
se	se	k3xPyFc4	se
Koppitz	Koppitz	k1gMnSc1	Koppitz
dostal	dostat	k5eAaPmAgMnS	dostat
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
staly	stát	k5eAaPmAgFnP	stát
prosté	prostý	k2eAgInPc1d1	prostý
a	a	k8xC	a
přímé	přímý	k2eAgInPc1d1	přímý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
jako	jako	k8xC	jako
průzkumný	průzkumný	k2eAgMnSc1d1	průzkumný
letec	letec	k1gMnSc1	letec
fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
letadla	letadlo	k1gNnPc4	letadlo
a	a	k8xC	a
krajiny	krajina	k1gFnPc4	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
oblíbená	oblíbený	k2eAgNnPc1d1	oblíbené
témata	téma	k1gNnPc1	téma
patřily	patřit	k5eAaImAgFnP	patřit
vodní	vodní	k2eAgFnPc4d1	vodní
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
geometrické	geometrický	k2eAgInPc4d1	geometrický
prvky	prvek	k1gInPc4	prvek
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
zachycoval	zachycovat	k5eAaImAgInS	zachycovat
nejen	nejen	k6eAd1	nejen
život	život	k1gInSc4	život
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
prostý	prostý	k2eAgInSc4d1	prostý
lid	lid	k1gInSc4	lid
v	v	k7c6	v
dobových	dobový	k2eAgInPc6d1	dobový
krojích	kroj	k1gInPc6	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Koppitzovy	Koppitzův	k2eAgFnPc1d1	Koppitzův
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
dramatických	dramatický	k2eAgFnPc2d1	dramatická
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
pracoval	pracovat	k5eAaImAgMnS	pracovat
se	s	k7c7	s
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
využíval	využívat	k5eAaPmAgInS	využívat
slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
mraky	mrak	k1gInPc1	mrak
a	a	k8xC	a
mlhy	mlha	k1gFnPc1	mlha
k	k	k7c3	k
impresionistickému	impresionistický	k2eAgNnSc3d1	impresionistické
vyjádření	vyjádření	k1gNnSc3	vyjádření
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Koppitz	Koppitz	k1gMnSc1	Koppitz
patřil	patřit	k5eAaImAgMnS	patřit
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
k	k	k7c3	k
nejvýznačnějším	význačný	k2eAgMnPc3d3	nejvýznačnější
představitelům	představitel	k1gMnPc3	představitel
rakouské	rakouský	k2eAgFnSc2d1	rakouská
fotografie	fotografia	k1gFnSc2	fotografia
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
zastánců	zastánce	k1gMnPc2	zastánce
tradicionalistické	tradicionalistický	k2eAgFnSc2d1	tradicionalistická
"	"	kIx"	"
<g/>
umělecké	umělecký	k2eAgFnSc2d1	umělecká
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
malířstvím	malířství	k1gNnSc7	malířství
<g/>
,	,	kIx,	,
hudbou	hudba	k1gFnSc7	hudba
i	i	k8xC	i
divadlem	divadlo	k1gNnSc7	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
oblíbená	oblíbený	k2eAgNnPc4d1	oblíbené
témata	téma	k1gNnPc4	téma
patřily	patřit	k5eAaImAgFnP	patřit
akty	akt	k1gInPc4	akt
více	hodně	k6eAd2	hodně
postav	postava	k1gFnPc2	postava
plné	plný	k2eAgFnSc2d1	plná
symbolických	symbolický	k2eAgInPc2d1	symbolický
odkazů	odkaz	k1gInPc2	odkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Často	často	k6eAd1	často
zobrazovaným	zobrazovaný	k2eAgInSc7d1	zobrazovaný
motivem	motiv	k1gInSc7	motiv
byly	být	k5eAaImAgFnP	být
nahé	nahý	k2eAgFnPc1d1	nahá
tanečnice	tanečnice	k1gFnPc1	tanečnice
a	a	k8xC	a
zachycení	zachycení	k1gNnSc1	zachycení
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
postavy	postav	k1gInPc1	postav
stylizoval	stylizovat	k5eAaImAgMnS	stylizovat
do	do	k7c2	do
soch	socha	k1gFnPc2	socha
Augusta	August	k1gMnSc2	August
Rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
ženských	ženský	k2eAgFnPc2d1	ženská
postav	postava	k1gFnPc2	postava
zobrazovaly	zobrazovat	k5eAaImAgFnP	zobrazovat
snímky	snímek	k1gInPc4	snímek
také	také	k9	také
mužské	mužský	k2eAgInPc4d1	mužský
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
autoportréty	autoportrét	k1gInPc1	autoportrét
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celé	celý	k2eAgNnSc1d1	celé
Koppitzovo	Koppitzův	k2eAgNnSc1d1	Koppitzův
dílo	dílo	k1gNnSc1	dílo
prostupují	prostupovat	k5eAaImIp3nP	prostupovat
symbolické	symbolický	k2eAgInPc1d1	symbolický
odkazy	odkaz	k1gInPc1	odkaz
a	a	k8xC	a
znaky	znak	k1gInPc1	znak
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Koppitzových	Koppitzův	k2eAgFnPc2d1	Koppitzův
fotografií	fotografia	k1gFnPc2	fotografia
se	se	k3xPyFc4	se
proslavily	proslavit	k5eAaPmAgFnP	proslavit
zejména	zejména	k9	zejména
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
kompozičně	kompozičně	k6eAd1	kompozičně
ovlivněné	ovlivněný	k2eAgFnPc4d1	ovlivněná
zčásti	zčásti	k6eAd1	zčásti
secesí	secese	k1gFnSc7	secese
<g/>
,	,	kIx,	,
zčásti	zčásti	k6eAd1	zčásti
konstruktivismem	konstruktivismus	k1gInSc7	konstruktivismus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
radikální	radikální	k2eAgFnSc3d1	radikální
změně	změna	k1gFnSc3	změna
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
tvorbě	tvorba	k1gFnSc6	tvorba
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
ke	k	k7c3	k
krajinám	krajina	k1gFnPc3	krajina
<g/>
,	,	kIx,	,
přírodě	příroda	k1gFnSc3	příroda
a	a	k8xC	a
k	k	k7c3	k
životu	život	k1gInSc3	život
venkovských	venkovský	k2eAgMnPc2d1	venkovský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Koppitzova	Koppitzův	k2eAgFnSc1d1	Koppitzův
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
nejen	nejen	k6eAd1	nejen
stylem	styl	k1gInSc7	styl
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgInS	navázat
také	také	k9	také
na	na	k7c4	na
členy	člen	k1gMnPc4	člen
rakouského	rakouský	k2eAgInSc2d1	rakouský
Trojlístku	trojlístek	k1gInSc2	trojlístek
(	(	kIx(	(
<g/>
Heinricha	Heinrich	k1gMnSc2	Heinrich
Kühna	Kühn	k1gMnSc2	Kühn
<g/>
,	,	kIx,	,
Hanse	Hans	k1gMnSc2	Hans
Watzka	Watzka	k1gFnSc1	Watzka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Huga	Hugo	k1gMnSc4	Hugo
Henneberga	Henneberg	k1gMnSc4	Henneberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
tvorbě	tvorba	k1gFnSc6	tvorba
byl	být	k5eAaImAgInS	být
Koppitzovi	Koppitz	k1gMnSc3	Koppitz
blízký	blízký	k2eAgMnSc1d1	blízký
František	František	k1gMnSc1	František
Drtikol	Drtikola	k1gFnPc2	Drtikola
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
fotografa	fotograf	k1gMnSc2	fotograf
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Koppitze	Koppitze	k1gFnSc2	Koppitze
není	být	k5eNaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
ani	ani	k9	ani
v	v	k7c6	v
Encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
českých	český	k2eAgMnPc2d1	český
fotografů	fotograf	k1gMnPc2	fotograf
ani	ani	k8xC	ani
v	v	k7c6	v
Encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
českých	český	k2eAgFnPc2d1	Česká
výtvarným	výtvarný	k2eAgInPc3d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Koppitz	Koppitz	k1gInSc1	Koppitz
nebyl	být	k5eNaImAgInS	být
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
progresivní	progresivní	k2eAgMnSc1d1	progresivní
moderní	moderní	k2eAgMnSc1d1	moderní
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
řadil	řadit	k5eAaImAgMnS	řadit
mezi	mezi	k7c4	mezi
konzervativní	konzervativní	k2eAgMnPc4d1	konzervativní
tvůrce	tvůrce	k1gMnPc4	tvůrce
<g/>
,	,	kIx,	,
držící	držící	k2eAgNnSc4d1	držící
se	se	k3xPyFc4	se
určitých	určitý	k2eAgFnPc2d1	určitá
tradic	tradice	k1gFnPc2	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Rakouská	rakouský	k2eAgFnSc1d1	rakouská
historička	historička	k1gFnSc1	historička
umění	umění	k1gNnSc2	umění
Monika	Monika	k1gFnSc1	Monika
Faber	Fabero	k1gNnPc2	Fabero
vydala	vydat	k5eAaPmAgFnS	vydat
Koppitzovu	Koppitzův	k2eAgFnSc4d1	Koppitzův
monografii	monografie	k1gFnSc4	monografie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
provázela	provázet	k5eAaImAgFnS	provázet
jeho	jeho	k3xOp3gInPc4	jeho
výstavy	výstav	k1gInPc4	výstav
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc3	Francie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
katalog	katalog	k1gInSc4	katalog
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
ucelený	ucelený	k2eAgInSc4d1	ucelený
přehled	přehled	k1gInSc4	přehled
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
díla	dílo	k1gNnSc2	dílo
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc1	ukázka
fotografií	fotografia	k1gFnSc7	fotografia
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rudolf	Rudolf	k1gMnSc1	Rudolf
Koppitz	Koppitza	k1gFnPc2	Koppitza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
paulcava	paulcava	k1gFnSc1	paulcava
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
jmcfaber	jmcfaber	k1gInSc1	jmcfaber
<g/>
.	.	kIx.	.
<g/>
at	at	k?	at
</s>
</p>
<p>
<s>
jmcolberg	jmcolberg	k1gMnSc1	jmcolberg
</s>
</p>
<p>
<s>
artnet	artnet	k1gInSc1	artnet
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
