<s>
Vikariát	vikariát	k1gInSc1
Písek	Písek	k1gInSc1
</s>
<s>
vikariát	vikariát	k1gInSc1
PísekDiecéze	PísekDiecéza	k1gFnSc6
</s>
<s>
českobudějovická	českobudějovický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
česká	český	k2eAgFnSc1d1
Okrskový	okrskový	k2eAgMnSc5d1
vikář	vikář	k1gMnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Doležal	Doležal	k1gMnSc1
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
vikáře	vikář	k1gMnSc2
</s>
<s>
farář	farář	k1gMnSc1
<g/>
,	,	kIx,
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Písek	Písek	k1gInSc1
Kaplan	Kaplan	k1gMnSc1
pro	pro	k7c4
mládež	mládež	k1gFnSc4
</s>
<s>
P.	P.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Řehoř	Řehoř	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Žáček	Žáček	k1gMnSc1
OPraem	OPraem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Údaje	údaj	k1gInSc2
v	v	k7c6
infoboxu	infobox	k1gInSc6
aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
2017	#num#	k4
(	(	kIx(
<g/>
IX	IX	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Vikariát	vikariát	k1gInSc1
Písek	Písek	k1gInSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
z	z	k7c2
deseti	deset	k4xCc2
vikariátů	vikariát	k1gInPc2
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
37	#num#	k4
farností	farnost	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
pouze	pouze	k6eAd1
v	v	k7c6
sedmi	sedm	k4xCc6
sídlí	sídlet	k5eAaImIp3nS
farář	farář	k1gMnSc1
nebo	nebo	k8xC
administrátor	administrátor	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
duchovní	duchovní	k2eAgFnSc6d1
správě	správa	k1gFnSc6
v	v	k7c6
něm	on	k3xPp3gNnSc6
působí	působit	k5eAaImIp3nS
13	#num#	k4
kněží	kněz	k1gMnPc2
a	a	k8xC
5	#num#	k4
jáhnů	jáhen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okrskovým	okrskový	k2eAgMnSc7d1
vikářem	vikář	k1gMnSc7
je	být	k5eAaImIp3nS
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Doležal	Doležal	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
v	v	k7c6
Písku	Písek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Post	post	k1gInSc1
vikariátního	vikariátní	k2eAgMnSc2d1
sekretáře	sekretář	k1gMnSc2
není	být	k5eNaImIp3nS
obsazen	obsadit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
FarnostSprávceDalší	FarnostSprávceDalší	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
ve	v	k7c4
farnostiFarní	farnostiFarní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
</s>
<s>
Albrechtice	Albrechtice	k1gFnPc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Vítězslav	Vítězslav	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Bernartice	Bernartice	k1gFnSc1
(	(	kIx(
<g/>
u	u	k7c2
Milevska	Milevsko	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pius	Pius	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Vágner	Vágner	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Augustin	Augustin	k1gMnSc1
Jarolímek	Jarolímek	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Karel	Karel	k1gMnSc1
Sádlo	sádlo	k1gNnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
z	z	k7c2
Tours	Toursa	k1gFnPc2
</s>
<s>
Březnice	Březnice	k1gFnSc1
u	u	k7c2
Příbrami	Příbram	k1gFnSc2
</s>
<s>
P.	P.	kA
Metod	Metod	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Kozubík	Kozubík	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Ignáce	Ignác	k1gMnSc4
z	z	k7c2
Loyoly	Loyola	k1gFnSc2
a	a	k8xC
sv.	sv.	kA
Františka	František	k1gMnSc2
Xaverského	xaverský	k2eAgMnSc2d1
</s>
<s>
Bubovice	Bubovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Metod	Metod	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Kozubík	Kozubík	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Chraštice	Chraštice	k1gFnSc1
</s>
<s>
P.	P.	kA
Metod	Metod	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Kozubík	Kozubík	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Chřešťovice	Chřešťovice	k1gFnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Vítězslav	Vítězslav	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
na	na	k7c6
poušti	poušť	k1gFnSc6
</s>
<s>
Chyšky	chyška	k1gFnPc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pius	Pius	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Vágner	Vágner	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc4
</s>
<s>
Čimelice	Čimelice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
František	František	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hemala	Hemala	k1gFnSc1
CFSsS	CFSsS	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
služběBc	služběBc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Košatka	košatka	k1gFnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Čížová	Čížová	k1gFnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Doležal	Doležal	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
František	František	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hemala	Hemala	k1gFnSc1
CFSsS	CFSsS	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Peter	Peter	k1gMnSc1
Paradič	Paradič	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc4
Staršího	starší	k1gMnSc4
</s>
<s>
Drahenice	Drahenice	k1gFnSc1
</s>
<s>
P.	P.	kA
Metod	Metod	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Kozubík	Kozubík	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Neposkvrněného	poskvrněný	k2eNgNnSc2d1
početí	početí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Heřmaň	Heřmanit	k5eAaPmRp2nS
u	u	k7c2
Písku	Písek	k1gInSc6
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Mičánek	Mičánek	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc1
</s>
<s>
Hvožďany	Hvožďana	k1gFnPc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Misař	Misař	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc2
</s>
<s>
Kestřany	Kestřan	k1gMnPc4
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Peter	Peter	k1gMnSc1
Paradič	Paradič	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Kateřiny	Kateřina	k1gFnPc1
</s>
<s>
Klučenice	Klučenice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Löffelmann	Löffelmann	k1gMnSc1
OPraem	OPra	k1gMnSc7
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
a	a	k8xC
sv.	sv.	kA
Antonína	Antonín	k1gMnSc2
Velikého	veliký	k2eAgMnSc2d1
</s>
<s>
Kostelec	Kostelec	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
</s>
<s>
P.	P.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Řehoř	Řehoř	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Žáček	Žáček	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Kovářov	Kovářov	k1gInSc1
</s>
<s>
P.	P.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Řehoř	Řehoř	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Žáček	Žáček	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Všech	všecek	k3xTgMnPc2
svatých	svatý	k1gMnPc2
</s>
<s>
Krč	Krč	k1gFnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Mičánek	Mičánek	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Václava	Václav	k1gMnSc4
</s>
<s>
Květov	Květov	k1gInSc1
</s>
<s>
P.	P.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Řehoř	Řehoř	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Žáček	Žáček	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Karel	Karel	k1gMnSc1
Sádlo	sádlo	k1gNnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Lašovice	Lašovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Jindřich	Jindřich	k1gMnSc1
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
Löffelmann	Löffelmann	k1gMnSc1
OPraem	OPra	k1gMnSc7
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
jáhen	jáhen	k1gMnSc1
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Milevsko	Milevsko	k1gNnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Mikuláš	mikuláš	k1gInSc1
Selvek	Selvek	k1gInSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
P.	P.	kA
Jindřich	Jindřich	k1gMnSc1
Jan	Jan	k1gMnSc1
Nepomucký	Nepomucký	k1gMnSc1
Löffelmann	Löffelmann	k1gMnSc1
OPraem	OPraem	k1gInSc4
<g/>
,	,	kIx,
farní	farní	k2eAgInSc4d1
vikářKarel	vikářKarel	k1gInSc4
Sádlo	sádlo	k1gNnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
služběAugustin	služběAugustin	k1gMnSc1
Jarolímek	Jarolímek	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pius	Pius	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Vágner	Vágner	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Řehoř	Řehoř	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Žáček	Žáček	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Navštívení	navštívení	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Mirotice	Mirotice	k1gFnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
František	František	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hemala	Hemala	k1gFnSc1
CFSsS	CFSsS	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
služběBc	služběBc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Košatka	košatka	k1gFnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Jiljí	Jiljí	k1gMnSc1
</s>
<s>
Mirovice	Mirovice	k1gFnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Bc.	Bc.	k?
Josef	Josef	k1gMnSc1
Košatka	košatka	k1gFnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Klementa	Klement	k1gMnSc4
</s>
<s>
Myšenec	Myšenec	k1gMnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Mičánek	Mičánek	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Havla	Havel	k1gMnSc4
</s>
<s>
Nadějkov	Nadějkov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Mikuláš	mikuláš	k1gInSc1
Selvek	Selvek	k1gInSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
</s>
<s>
Orlík	Orlík	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Bc.	Bc.	k?
Josef	Josef	k1gMnSc1
Košatka	košatka	k1gFnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc4
</s>
<s>
Oslov	oslovit	k5eAaPmRp2nS
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Vítězslav	Vítězslav	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
sv.	sv.	kA
Linharta	Linhart	k1gMnSc4
</s>
<s>
Písek	Písek	k1gInSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Jan	Jan	k1gMnSc1
Doležal	Doležal	k1gMnSc1
<g/>
,	,	kIx,
farář	farář	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
František	František	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hemala	Hemala	k1gFnSc1
CFSsS	CFSsS	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgInSc1d1
vikářP	vikářP	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Peter	Peter	k1gMnSc1
Paradič	Paradič	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgInSc1d1
vikářKarel	vikářKarel	k1gInSc1
Lednický	lednický	k2eAgInSc1d1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
Narození	narození	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Písek	Písek	k1gInSc1
-	-	kIx~
sv.	sv.	kA
Kříž	Kříž	k1gMnSc1
</s>
<s>
P.	P.	kA
Dr	dr	kA
<g/>
.	.	kIx.
theol	theol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyril	Cyril	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Havel	Havel	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
rektor	rektor	k1gMnSc1
kostela	kostel	k1gInSc2
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Peter	Peter	k1gMnSc1
Paradič	Paradič	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kaplan	kaplan	k1gMnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
Pohoří	pohoří	k1gNnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
služběBc	služběBc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Košatka	košatka	k1gFnSc1
<g/>
,	,	kIx,
trvalýjáhen	trvalýjáhen	k1gInSc1
ustanovený	ustanovený	k2eAgInSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Protivín	Protivín	k1gInSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Mičánek	Mičánek	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Alžběty	Alžběta	k1gFnPc1
</s>
<s>
Putim	Putim	k1gFnSc1
</s>
<s>
P.	P.	kA
Dr	dr	kA
<g/>
.	.	kIx.
theol	theol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cyril	Cyril	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Havel	Havel	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
František	František	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hemala	Hemala	k1gFnSc1
CFSsS	CFSsS	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Tomáš	Tomáš	k1gMnSc1
Peter	Peter	k1gMnSc1
Paradič	Paradič	k1gMnSc1
CFSsS	CFSsS	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Vavřince	Vavřinec	k1gMnSc4
</s>
<s>
Radobytce	Radobytka	k1gFnSc3
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
František	František	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Hemala	Hemala	k1gFnSc1
CFSsS	CFSsS	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Bc.	Bc.	k?
Jiří	Jiří	k1gMnSc1
Kabíček	Kabíček	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
služběBc	služběBc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Košatka	košatka	k1gFnSc1
<g/>
,	,	kIx,
trvalýjáhen	trvalýjáhen	k1gInSc1
ustanovený	ustanovený	k2eAgInSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Ondřeje	Ondřej	k1gMnSc4
</s>
<s>
Sepekov	Sepekov	k1gInSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Mikuláš	mikuláš	k1gInSc1
Selvek	Selvek	k1gInSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Jména	jméno	k1gNnPc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Starý	starý	k2eAgMnSc1d1
Rožmitál	Rožmitál	k1gMnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Petr	Petr	k1gMnSc1
Misař	Misař	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
Povýšení	povýšení	k1gNnSc1
sv.	sv.	kA
Kříže	kříž	k1gInSc2
</s>
<s>
Tochovice	Tochovice	k1gFnSc1
</s>
<s>
P.	P.	kA
Metod	Metod	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Kozubík	Kozubík	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Mgr.	Mgr.	kA
Mariusz	Mariusz	k1gMnSc1
Klimczuk	Klimczuk	k1gMnSc1
<g/>
,	,	kIx,
farní	farní	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Martina	Martina	k1gFnSc1
z	z	k7c2
Tours	Toursa	k1gFnPc2
</s>
<s>
Veselíčko	veselíčko	k1gNnSc1
</s>
<s>
P.	P.	kA
Mgr.	Mgr.	kA
Pius	Pius	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Vágner	Vágner	k1gMnSc1
OPraem	OPraem	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
excurrendo	excurrendo	k6eAd1
</s>
<s>
Augustin	Augustin	k1gMnSc1
Jarolímek	Jarolímek	k1gMnSc1
<g/>
,	,	kIx,
trvalý	trvalý	k2eAgMnSc1d1
jáhen	jáhen	k1gMnSc1
ustanovený	ustanovený	k2eAgMnSc1d1
ke	k	k7c3
službě	služba	k1gFnSc3
</s>
<s>
sv.	sv.	kA
Anny	Anna	k1gFnPc1
</s>
<s>
Záhoří	Záhoří	k1gNnSc1
</s>
<s>
R.	R.	kA
<g/>
D.	D.	kA
Ing.	ing.	kA
Mgr.	Mgr.	kA
Vítězslav	Vítězslav	k1gMnSc1
Holý	Holý	k1gMnSc1
<g/>
,	,	kIx,
administrátor	administrátor	k1gMnSc1
</s>
<s>
sv.	sv.	kA
Michaela	Michael	k1gMnSc4
archanděla	archanděl	k1gMnSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vikariát	vikariát	k1gInSc1
Písek	Písek	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
na	na	k7c6
stránkách	stránka	k1gFnPc6
českobudějovického	českobudějovický	k2eAgInSc2d1
biskupství	biskupství	k1gNnSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vikariáty	vikariát	k1gInPc1
českobudějovické	českobudějovický	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
město	město	k1gNnSc4
•	•	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
–	–	k?
venkov	venkov	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
•	•	k?
Jindřichův	Jindřichův	k2eAgInSc1d1
Hradec	Hradec	k1gInSc1
•	•	k?
Pelhřimov	Pelhřimov	k1gInSc1
•	•	k?
Písek	Písek	k1gInSc1
•	•	k?
Prachatice	Prachatice	k1gFnPc4
•	•	k?
Strakonice	Strakonice	k1gFnPc4
•	•	k?
Sušice-Nepomuk	Sušice-Nepomuk	k1gMnSc1
•	•	k?
Tábor	Tábor	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
