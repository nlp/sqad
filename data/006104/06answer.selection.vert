<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
م	م	k?	م
<g/>
,	,	kIx,	,
masrí	masrí	k1gFnSc1	masrí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hovorový	hovorový	k2eAgInSc1d1	hovorový
dialekt	dialekt	k1gInSc1	dialekt
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
dialektů	dialekt	k1gInPc2	dialekt
<g/>
)	)	kIx)	)
arabštiny	arabština	k1gFnSc2	arabština
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
území	území	k1gNnSc6	území
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
