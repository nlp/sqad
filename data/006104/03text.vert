<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
(	(	kIx(	(
<g/>
م	م	k?	م
<g/>
,	,	kIx,	,
masrí	masrí	k1gFnSc1	masrí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hovorový	hovorový	k2eAgInSc1d1	hovorový
dialekt	dialekt	k1gInSc1	dialekt
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
skupina	skupina	k1gFnSc1	skupina
dialektů	dialekt	k1gInPc2	dialekt
<g/>
)	)	kIx)	)
arabštiny	arabština	k1gFnSc2	arabština
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
na	na	k7c6	na
území	území	k1gNnSc6	území
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
počátcích	počátek	k1gInPc6	počátek
byl	být	k5eAaImAgInS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
koptštinou	koptština	k1gFnSc7	koptština
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
spisovné	spisovný	k2eAgFnSc2d1	spisovná
arabštiny	arabština	k1gFnSc2	arabština
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
ve	v	k7c6	v
fonologii	fonologie	k1gFnSc6	fonologie
<g/>
,	,	kIx,	,
slovní	slovní	k2eAgFnSc6d1	slovní
zásobě	zásoba	k1gFnSc6	zásoba
<g/>
,	,	kIx,	,
morfologii	morfologie	k1gFnSc6	morfologie
i	i	k8xC	i
syntaxi	syntax	k1gFnSc6	syntax
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
fonologického	fonologický	k2eAgInSc2d1	fonologický
rysu	rys	k1gInSc2	rys
je	být	k5eAaImIp3nS	být
realizace	realizace	k1gFnSc1	realizace
hlásky	hláska	k1gFnSc2	hláska
ج	ج	k?	ج
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
arabštině	arabština	k1gFnSc6	arabština
čte	číst	k5eAaImIp3nS	číst
dž	dž	k?	dž
<g/>
,	,	kIx,	,
v	v	k7c6	v
egyptské	egyptský	k2eAgFnSc6d1	egyptská
g	g	kA	g
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dalších	další	k2eAgInPc6d1	další
dialektech	dialekt	k1gInPc6	dialekt
pak	pak	k6eAd1	pak
třeba	třeba	k6eAd1	třeba
ž	ž	k?	ž
<g/>
:	:	kIx,	:
např.	např.	kA	např.
"	"	kIx"	"
<g/>
hora	hora	k1gFnSc1	hora
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
spisovně	spisovně	k6eAd1	spisovně
řekne	říct	k5eAaPmIp3nS	říct
džabal	džabal	k1gInSc1	džabal
<g/>
,	,	kIx,	,
egyptsky	egyptsky	k6eAd1	egyptsky
gebel	gebet	k5eAaPmAgMnS	gebet
a	a	k8xC	a
marocky	marocky	k6eAd1	marocky
žbel	žbenout	k5eAaPmAgMnS	žbenout
<g/>
.	.	kIx.	.
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
arabština	arabština	k1gFnSc1	arabština
je	být	k5eAaImIp3nS	být
především	především	k9	především
hovorový	hovorový	k2eAgInSc1d1	hovorový
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
ustálenou	ustálený	k2eAgFnSc4d1	ustálená
psanou	psaný	k2eAgFnSc4d1	psaná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
psaném	psaný	k2eAgInSc6d1	psaný
projevu	projev	k1gInSc6	projev
a	a	k8xC	a
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
spisovná	spisovný	k2eAgFnSc1d1	spisovná
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
beletrii	beletrie	k1gFnSc6	beletrie
se	se	k3xPyFc4	se
egyptský	egyptský	k2eAgInSc1d1	egyptský
dialekt	dialekt	k1gInSc1	dialekt
občas	občas	k6eAd1	občas
objevuje	objevovat	k5eAaImIp3nS	objevovat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
dialozích	dialog	k1gInPc6	dialog
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takových	takový	k3xDgInPc6	takový
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
arabským	arabský	k2eAgNnSc7d1	arabské
písmem	písmo	k1gNnSc7	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Fonologické	fonologický	k2eAgFnPc1d1	fonologická
odlišnosti	odlišnost	k1gFnPc1	odlišnost
pak	pak	k6eAd1	pak
téměř	téměř	k6eAd1	téměř
nejsou	být	k5eNaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
(	(	kIx(	(
<g/>
zápis	zápis	k1gInSc1	zápis
je	být	k5eAaImIp3nS	být
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
,	,	kIx,	,
čtení	čtení	k1gNnSc1	čtení
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
odlišnosti	odlišnost	k1gFnPc1	odlišnost
(	(	kIx(	(
<g/>
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
<g/>
,	,	kIx,	,
syntaxe	syntaxe	k1gFnSc1	syntaxe
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
vidět	vidět	k5eAaImF	vidět
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
lingvistické	lingvistický	k2eAgFnSc6d1	lingvistická
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
publikacích	publikace	k1gFnPc6	publikace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
egyptskou	egyptský	k2eAgFnSc7d1	egyptská
arabštinou	arabština	k1gFnSc7	arabština
zabývají	zabývat	k5eAaImIp3nP	zabývat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
její	její	k3xOp3gInSc4	její
zápis	zápis	k1gInSc4	zápis
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
latinka	latinka	k1gFnSc1	latinka
obohacená	obohacený	k2eAgFnSc1d1	obohacená
diakritickými	diakritický	k2eAgNnPc7d1	diakritické
znaménky	znaménko	k1gNnPc7	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Oliverius	Oliverius	k1gMnSc1	Oliverius
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
:	:	kIx,	:
Egyptská	egyptský	k2eAgFnSc1d1	egyptská
hovorová	hovorový	k2eAgFnSc1d1	hovorová
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
392	[number]	k4	392
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
pedagogické	pedagogický	k2eAgNnSc1d1	pedagogické
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
