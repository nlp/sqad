<p>
<s>
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
<g/>
,	,	kIx,	,
též	též	k9	též
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xC	jako
Andělské	andělský	k2eAgInPc1d1	andělský
schody	schod	k1gInPc1	schod
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Scalinata	Scalinat	k2eAgFnSc1d1	Scalinat
di	di	k?	di
Trinita	Trinita	k1gFnSc1	Trinita
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
náměstí	náměstí	k1gNnSc2	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
di	di	k?	di
Spagna	Spagn	k1gInSc2	Spagn
<g/>
,	,	kIx,	,
Španělského	španělský	k2eAgNnSc2d1	španělské
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
)	)	kIx)	)
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgNnSc1d1	barokní
schodiště	schodiště	k1gNnSc1	schodiště
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
Santa	Sant	k1gInSc2	Sant
Trinità	Trinità	k1gFnSc2	Trinità
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1723	[number]	k4	1723
na	na	k7c4	na
popud	popud	k1gInSc4	popud
papeže	papež	k1gMnSc2	papež
Inocenta	Inocent	k1gMnSc2	Inocent
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
schodišť	schodiště	k1gNnPc2	schodiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
schodiště	schodiště	k1gNnSc2	schodiště
===	===	k?	===
</s>
</p>
<p>
<s>
Před	před	k7c7	před
výstavbou	výstavba	k1gFnSc7	výstavba
schodiště	schodiště	k1gNnSc2	schodiště
byl	být	k5eAaImAgInS	být
zarostlý	zarostlý	k2eAgInSc1d1	zarostlý
svah	svah	k1gInSc1	svah
mezi	mezi	k7c7	mezi
kostelem	kostel	k1gInSc7	kostel
Santa	Sant	k1gMnSc2	Sant
Trinità	Trinità	k1gMnSc2	Trinità
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
a	a	k8xC	a
náměstím	náměstí	k1gNnSc7	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
di	di	k?	di
Spagna	Spagn	k1gMnSc2	Spagn
pociťován	pociťován	k2eAgMnSc1d1	pociťován
jako	jako	k8xC	jako
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
zakončení	zakončení	k1gNnSc1	zakončení
nyní	nyní	k6eAd1	nyní
zastavěné	zastavěný	k2eAgFnSc2d1	zastavěná
městské	městský	k2eAgFnSc2d1	městská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
di	di	k?	di
Spagna	Spagn	k1gInSc2	Spagn
bylo	být	k5eAaImAgNnS	být
významné	významný	k2eAgNnSc1d1	významné
zejména	zejména	k9	zejména
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc1	sídlo
španělského	španělský	k2eAgNnSc2d1	španělské
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
u	u	k7c2	u
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
před	před	k7c7	před
španělským	španělský	k2eAgNnSc7d1	španělské
vyslanectvím	vyslanectví	k1gNnSc7	vyslanectví
bylo	být	k5eAaImAgNnS	být
španělské	španělský	k2eAgNnSc4d1	španělské
výsostné	výsostný	k2eAgNnSc4d1	výsostné
území	území	k1gNnSc4	území
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
cizinec	cizinec	k1gMnSc1	cizinec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tam	tam	k6eAd1	tam
pronikl	proniknout	k5eAaPmAgInS	proniknout
bez	bez	k7c2	bez
oprávnění	oprávnění	k1gNnSc2	oprávnění
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
donucen	donutit	k5eAaPmNgMnS	donutit
sloužit	sloužit	k5eAaImF	sloužit
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zde	zde	k6eAd1	zde
platilo	platit	k5eAaImAgNnS	platit
právo	právo	k1gNnSc1	právo
azylu	azyl	k1gInSc2	azyl
vůči	vůči	k7c3	vůči
Vatikánu	Vatikán	k1gInSc3	Vatikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Konflikt	konflikt	k1gInSc4	konflikt
zájmů	zájem	k1gInPc2	zájem
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
zájmů	zájem	k1gInPc2	zájem
papeže	papež	k1gMnSc2	papež
byly	být	k5eAaImAgFnP	být
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
i	i	k8xC	i
zájmy	zájem	k1gInPc1	zájem
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Schodiště	schodiště	k1gNnSc1	schodiště
mělo	mít	k5eAaImAgNnS	mít
umožnit	umožnit	k5eAaPmF	umožnit
slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
králem	král	k1gMnSc7	král
Francie	Francie	k1gFnSc2	Francie
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
XII	XII	kA	XII
<g/>
.	.	kIx.	.
financovaného	financovaný	k2eAgInSc2d1	financovaný
kostela	kostel	k1gInSc2	kostel
Trinità	Trinità	k1gFnSc2	Trinità
dei	dei	k?	dei
Monti	Monť	k1gFnSc2	Monť
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
říká	říkat	k5eAaImIp3nS	říkat
nápis	nápis	k1gInSc1	nápis
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
,	,	kIx,	,
zanechal	zanechat	k5eAaPmAgMnS	zanechat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vyslanec	vyslanec	k1gMnSc1	vyslanec
Etienne	Etienn	k1gInSc5	Etienn
Gueffier	Gueffier	k1gInSc4	Gueffier
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1661	[number]	k4	1661
značnou	značný	k2eAgFnSc4d1	značná
sumu	suma	k1gFnSc4	suma
peněz	peníze	k1gInPc2	peníze
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
však	však	k9	však
čekat	čekat	k5eAaImF	čekat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
<g/>
.	.	kIx.	.
než	než	k8xS	než
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
schodiště	schodiště	k1gNnSc1	schodiště
postaveno	postavit	k5eAaPmNgNnS	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Nápis	nápis	k1gInSc1	nápis
to	ten	k3xDgNnSc4	ten
zaobaluje	zaobalovat	k5eAaImIp3nS	zaobalovat
jako	jako	k8xS	jako
OPUS	opus	k1gInSc1	opus
AUTEM	aut	k1gInSc7	aut
VARIO	VARIO	kA	VARIO
RERUM	RERUM	kA	RERUM
INTERVENTU	intervent	k1gMnSc3	intervent
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
vmísení	vmísení	k1gNnSc1	vmísení
se	se	k3xPyFc4	se
různých	různý	k2eAgFnPc2d1	různá
věcí	věc	k1gFnPc2	věc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
konflikt	konflikt	k1gInSc1	konflikt
zájmů	zájem	k1gInPc2	zájem
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
a	a	k8xC	a
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Schody	schod	k1gInPc1	schod
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
památník	památník	k1gInSc4	památník
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
neslučovalo	slučovat	k5eNaImAgNnS	slučovat
s	s	k7c7	s
mocenskými	mocenský	k2eAgInPc7d1	mocenský
nároky	nárok	k1gInPc7	nárok
papeže	papež	k1gMnSc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Konkurence	konkurence	k1gFnSc1	konkurence
mezi	mezi	k7c7	mezi
králem	král	k1gMnSc7	král
a	a	k8xC	a
papežem	papež	k1gMnSc7	papež
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
bourbonských	bourbonský	k2eAgFnPc6d1	Bourbonská
liliích	lilie	k1gFnPc6	lilie
na	na	k7c6	na
vnějších	vnější	k2eAgInPc6d1	vnější
postranních	postranní	k2eAgInPc6d1	postranní
pilířích	pilíř	k1gInPc6	pilíř
a	a	k8xC	a
na	na	k7c6	na
orlu	orel	k1gMnSc6	orel
jako	jako	k8xS	jako
znaku	znak	k1gInSc3	znak
papeže	papež	k1gMnSc2	papež
Inocence	Inocenc	k1gMnSc2	Inocenc
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
na	na	k7c6	na
vnitřních	vnitřní	k2eAgInPc6d1	vnitřní
pilířích	pilíř	k1gInPc6	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Lilie	lilie	k1gFnSc1	lilie
a	a	k8xC	a
orel	orel	k1gMnSc1	orel
se	se	k3xPyFc4	se
opakují	opakovat	k5eAaImIp3nP	opakovat
na	na	k7c6	na
čtyřech	čtyři	k4xCgFnPc6	čtyři
koulích	koule	k1gFnPc6	koule
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Král	Král	k1gMnSc1	Král
slunce	slunce	k1gNnSc2	slunce
<g/>
"	"	kIx"	"
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
chtěl	chtít	k5eAaImAgMnS	chtít
schodiště	schodiště	k1gNnSc2	schodiště
původně	původně	k6eAd1	původně
završit	završit	k5eAaPmF	završit
oslavnou	oslavný	k2eAgFnSc7d1	oslavná
jezdeckou	jezdecký	k2eAgFnSc7d1	jezdecká
sochou	socha	k1gFnSc7	socha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
demonstrace	demonstrace	k1gFnSc1	demonstrace
síly	síla	k1gFnPc4	síla
jejich	jejich	k3xOp3gMnPc2	jejich
francouzských	francouzský	k2eAgMnPc2d1	francouzský
ochránců	ochránce	k1gMnPc2	ochránce
však	však	k9	však
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
papeže	papež	k1gMnSc4	papež
nepřijatelná	přijatelný	k2eNgFnSc1d1	nepřijatelná
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
králi	král	k1gMnPc7	král
a	a	k8xC	a
papeži	papež	k1gMnPc7	papež
nejdříve	dříve	k6eAd3	dříve
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nic	nic	k3yNnSc1	nic
nedělo	dít	k5eNaImAgNnS	dít
<g/>
.	.	kIx.	.
</s>
<s>
Benedikt	Benedikt	k1gMnSc1	Benedikt
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
pak	pak	k6eAd1	pak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1721	[number]	k4	1721
prosadil	prosadit	k5eAaPmAgInS	prosadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
schodiště	schodiště	k1gNnSc1	schodiště
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
stavět	stavět	k5eAaImF	stavět
v	v	k7c6	v
římském	římský	k2eAgInSc6d1	římský
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgInS	muset
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
pamětní	pamětní	k2eAgFnSc7d1	pamětní
deskou	deska	k1gFnSc7	deska
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Klement	Klement	k1gMnSc1	Klement
XII	XII	kA	XII
<g/>
.	.	kIx.	.
chtěl	chtít	k5eAaImAgMnS	chtít
roku	rok	k1gInSc2	rok
1733	[number]	k4	1733
schody	schod	k1gInPc4	schod
završit	završit	k5eAaPmF	završit
obeliskem	obelisk	k1gInSc7	obelisk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
konečně	konečně	k6eAd1	konečně
prokázat	prokázat	k5eAaPmF	prokázat
moc	moc	k6eAd1	moc
papežů	papež	k1gMnPc2	papež
i	i	k8xC	i
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
francouzským	francouzský	k2eAgInPc3d1	francouzský
protestům	protest	k1gInPc3	protest
však	však	k9	však
byly	být	k5eAaImAgInP	být
plány	plán	k1gInPc1	plán
prozatím	prozatím	k6eAd1	prozatím
zamítnuty	zamítnut	k2eAgInPc1d1	zamítnut
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
zbavila	zbavit	k5eAaPmAgFnS	zbavit
bourbonský	bourbonský	k2eAgInSc4d1	bourbonský
královský	královský	k2eAgInSc4d1	královský
dům	dům	k1gInSc4	dům
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
obelisk	obelisk	k1gInSc1	obelisk
být	být	k5eAaImF	být
na	na	k7c4	na
schody	schod	k1gInPc4	schod
umístěn	umístěn	k2eAgInSc1d1	umístěn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výstavba	výstavba	k1gFnSc1	výstavba
schodiště	schodiště	k1gNnSc2	schodiště
===	===	k?	===
</s>
</p>
<p>
<s>
Návrh	návrh	k1gInSc1	návrh
španělského	španělský	k2eAgNnSc2d1	španělské
schodiště	schodiště	k1gNnSc2	schodiště
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Francesco	Francesco	k1gMnSc1	Francesco
De	De	k?	De
Sanctis	Sanctis	k1gInSc1	Sanctis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vypsal	vypsat	k5eAaPmAgMnS	vypsat
Klement	Klement	k1gMnSc1	Klement
XI	XI	kA	XI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
předchůdce	předchůdce	k1gMnSc1	předchůdce
Inozenta	Inozento	k1gNnSc2	Inozento
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
konkurentem	konkurent	k1gMnSc7	konkurent
byl	být	k5eAaImAgMnS	být
Alessandro	Alessandra	k1gFnSc5	Alessandra
Specchi	Specch	k1gInPc7	Specch
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
již	již	k6eAd1	již
podobný	podobný	k2eAgInSc1d1	podobný
úkol	úkol	k1gInSc1	úkol
úspěšně	úspěšně	k6eAd1	úspěšně
vykonal	vykonat	k5eAaPmAgInS	vykonat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
později	pozdě	k6eAd2	pozdě
strženého	stržený	k2eAgInSc2d1	stržený
přístavu	přístav	k1gInSc2	přístav
Ripetta	Ripett	k1gInSc2	Ripett
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Sanctis	Sanctis	k1gInSc1	Sanctis
měl	mít	k5eAaImAgInS	mít
nemalé	malý	k2eNgFnPc4d1	nemalá
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
NA	na	k7c6	na
vstupu	vstup	k1gInSc6	vstup
schodiště	schodiště	k1gNnSc2	schodiště
<g/>
,	,	kIx,	,
budovaného	budovaný	k2eAgNnSc2d1	budované
v	v	k7c6	v
letech	let	k1gInPc6	let
1723	[number]	k4	1723
až	až	k9	až
1725	[number]	k4	1725
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
setkávají	setkávat	k5eAaImIp3nP	setkávat
dvě	dva	k4xCgFnPc1	dva
pohledové	pohledový	k2eAgFnPc1d1	pohledová
osy	osa	k1gFnPc1	osa
<g/>
:	:	kIx,	:
pohled	pohled	k1gInSc1	pohled
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
od	od	k7c2	od
Via	via	k7c4	via
del	del	k?	del
Babuino	Babuino	k1gNnSc4	Babuino
a	a	k8xC	a
od	od	k7c2	od
západu	západ	k1gInSc2	západ
k	k	k7c3	k
Via	via	k7c4	via
Condotti	Condotť	k1gFnPc4	Condotť
<g/>
.	.	kIx.	.
</s>
<s>
Schodiště	schodiště	k1gNnSc1	schodiště
začíná	začínat	k5eAaImIp3nS	začínat
centrálním	centrální	k2eAgInSc7d1	centrální
výstupem	výstup	k1gInSc7	výstup
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
paralelními	paralelní	k2eAgFnPc7d1	paralelní
bočními	boční	k2eAgFnPc7d1	boční
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třetině	třetina	k1gFnSc6	třetina
stoupání	stoupání	k1gNnSc2	stoupání
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
cesty	cesta	k1gFnPc1	cesta
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
první	první	k4xOgFnSc6	první
terase	terasa	k1gFnSc6	terasa
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
zase	zase	k9	zase
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
a	a	k8xC	a
obcházejí	obcházet	k5eAaImIp3nP	obcházet
zeď	zeď	k1gFnSc4	zeď
druhé	druhý	k4xOgFnSc2	druhý
terasy	terasa	k1gFnSc2	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
zužující	zužující	k2eAgMnSc1d1	zužující
se	se	k3xPyFc4	se
centrální	centrální	k2eAgInSc1d1	centrální
výstup	výstup	k1gInSc1	výstup
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
dělí	dělit	k5eAaImIp3nP	dělit
před	před	k7c7	před
zdí	zeď	k1gFnSc7	zeď
poslední	poslední	k2eAgFnSc2d1	poslední
terasy	terasa	k1gFnSc2	terasa
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
Trinità	Trinità	k1gFnPc2	Trinità
dei	dei	k?	dei
Monti	Monti	k1gNnSc4	Monti
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělením	rozdělení	k1gNnSc7	rozdělení
schodiště	schodiště	k1gNnSc2	schodiště
na	na	k7c4	na
třetiny	třetina	k1gFnPc4	třetina
připomněl	připomnět	k5eAaPmAgInS	připomnět
De	De	k?	De
Sanctis	Sanctis	k1gFnSc4	Sanctis
také	také	k9	také
zasvěcení	zasvěcení	k1gNnSc3	zasvěcení
kostela	kostel	k1gInSc2	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
na	na	k7c6	na
vyvýšenině	vyvýšenina	k1gFnSc6	vyvýšenina
<g/>
.	.	kIx.	.
</s>
<s>
Účinek	účinek	k1gInSc1	účinek
schodiště	schodiště	k1gNnSc2	schodiště
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
půdorys	půdorys	k1gInSc1	půdorys
některých	některý	k3yIgInPc2	některý
schodů	schod	k1gInPc2	schod
je	být	k5eAaImIp3nS	být
konvexní	konvexní	k2eAgFnSc1d1	konvexní
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
naopak	naopak	k6eAd1	naopak
konkávní	konkávní	k2eAgFnPc1d1	konkávní
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
překonává	překonávat	k5eAaImIp3nS	překonávat
rozdíl	rozdíl	k1gInSc4	rozdíl
výšky	výška	k1gFnSc2	výška
23	[number]	k4	23
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
68	[number]	k4	68
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc1d1	střední
terasa	terasa	k1gFnSc1	terasa
má	mít	k5eAaImIp3nS	mít
šířku	šířka	k1gFnSc4	šířka
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
překlenuje	překlenovat	k5eAaImIp3nS	překlenovat
vrchním	vrchní	k2eAgNnSc7d1	vrchní
dvoudílným	dvoudílný	k2eAgNnSc7d1	dvoudílné
schodištěm	schodiště	k1gNnSc7	schodiště
52	[number]	k4	52
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
atrakcím	atrakce	k1gFnPc3	atrakce
města	město	k1gNnSc2	město
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
populárním	populární	k2eAgNnSc7d1	populární
místem	místo	k1gNnSc7	místo
setkání	setkání	k1gNnSc1	setkání
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Schodiště	schodiště	k1gNnSc1	schodiště
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
138	[number]	k4	138
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
ulice	ulice	k1gFnSc2	ulice
Via	via	k7c4	via
Condotti	Condotti	k1gNnSc4	Condotti
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
schodiště	schodiště	k1gNnSc2	schodiště
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
luxusní	luxusní	k2eAgFnSc7d1	luxusní
nákupní	nákupní	k2eAgFnSc7d1	nákupní
zónou	zóna	k1gFnSc7	zóna
s	s	k7c7	s
obchody	obchod	k1gInPc7	obchod
jako	jako	k8xC	jako
Gucci	Gucec	k1gInPc7	Gucec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
Španělských	španělský	k2eAgInPc2d1	španělský
schodů	schod	k1gInPc2	schod
se	se	k3xPyFc4	se
napravo	napravo	k6eAd1	napravo
nachází	nacházet	k5eAaImIp3nS	nacházet
Muzeum	muzeum	k1gNnSc1	muzeum
Keatse	Keats	k1gMnSc2	Keats
a	a	k8xC	a
Shelleyho	Shelley	k1gMnSc2	Shelley
a	a	k8xC	a
nalevo	nalevo	k6eAd1	nalevo
Babington	Babington	k1gInSc1	Babington
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tea	Tea	k1gFnSc1	Tea
Room	Room	k1gInSc1	Room
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgMnSc1	obojí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
bývalý	bývalý	k2eAgInSc1d1	bývalý
domov	domov	k1gInSc1	domov
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Percyho	Percy	k1gMnSc2	Percy
Bysshe	Byssh	k1gMnSc2	Byssh
Shelleyho	Shelley	k1gMnSc2	Shelley
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
i	i	k9	i
John	John	k1gMnSc1	John
Keats	Keatsa	k1gFnPc2	Keatsa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
stará	starý	k2eAgFnSc1d1	stará
anglická	anglický	k2eAgFnSc1d1	anglická
čajovna	čajovna	k1gFnSc1	čajovna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
svědci	svědek	k1gMnPc1	svědek
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
uměleckého	umělecký	k2eAgNnSc2d1	umělecké
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
náměstí	náměstí	k1gNnSc2	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
di	di	k?	di
Spagna	Spagn	k1gMnSc2	Spagn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fontana	Fontan	k1gMnSc4	Fontan
della	dell	k1gMnSc4	dell
Barcaccia	Barcaccius	k1gMnSc4	Barcaccius
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
di	di	k?	di
Spagna	Spagn	k1gInSc2	Spagn
se	se	k3xPyFc4	se
před	před	k7c7	před
schodištěm	schodiště	k1gNnSc7	schodiště
nachází	nacházet	k5eAaImIp3nS	nacházet
starší	starý	k2eAgFnSc1d2	starší
fontána	fontána	k1gFnSc1	fontána
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Pietro	Pietro	k1gNnSc4	Pietro
Bernini	Bernin	k2eAgMnPc1d1	Bernin
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Lorenza	Lorenza	k?	Lorenza
Berniniho	Bernini	k1gMnSc2	Bernini
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1628	[number]	k4	1628
až	až	k9	až
1629	[number]	k4	1629
a	a	k8xC	a
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Fontana	Fontana	k1gFnSc1	Fontana
della	della	k1gFnSc1	della
Barcaccia	Barcaccia	k1gFnSc1	Barcaccia
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
povodně	povodeň	k1gFnSc2	povodeň
na	na	k7c6	na
Tibeře	Tibera	k1gFnSc6	Tibera
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
roku	rok	k1gInSc2	rok
1598	[number]	k4	1598
až	až	k9	až
sem	sem	k6eAd1	sem
prý	prý	k9	prý
doplul	doplout	k5eAaPmAgInS	doplout
člun	člun	k1gInSc1	člun
a	a	k8xC	a
uvízl	uvíznout	k5eAaPmAgMnS	uvíznout
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
když	když	k8xS	když
povodeň	povodeň	k1gFnSc1	povodeň
ustoupila	ustoupit	k5eAaPmAgFnS	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
Berniniho	Bernini	k1gMnSc4	Bernini
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
fontánu	fontána	k1gFnSc4	fontána
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
člunu	člun	k1gInSc2	člun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Spanische	Spanisch	k1gInSc2	Spanisch
Treppe	Trepp	k1gInSc5	Trepp
na	na	k7c6	na
německé	německý	k2eAgFnSc3d1	německá
Wikipedii	Wikipedie	k1gFnSc3	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Klaus	Klaus	k1gMnSc1	Klaus
Bartels	Bartels	k1gInSc1	Bartels
<g/>
:	:	kIx,	:
Roms	Roms	k1gInSc1	Roms
sprechende	sprechend	k1gMnSc5	sprechend
Steine	Stein	k1gMnSc5	Stein
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Aufl	Aufl	k1gMnSc1	Aufl
<g/>
.	.	kIx.	.
</s>
<s>
Zabern	Zabern	k1gInSc1	Zabern
<g/>
,	,	kIx,	,
Mainz	Mainz	k1gInSc1	Mainz
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8053	[number]	k4	8053
<g/>
-	-	kIx~	-
<g/>
2690	[number]	k4	2690
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marco	Marco	k6eAd1	Marco
Bussagli	Bussagl	k1gMnPc1	Bussagl
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Rom	Rom	k1gMnSc1	Rom
–	–	k?	–
Kunst	Kunst	k1gInSc1	Kunst
&	&	k?	&
Architektur	architektura	k1gFnPc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Könemann	Könemann	k1gInSc1	Könemann
<g/>
,	,	kIx,	,
Köln	Köln	k1gInSc1	Köln
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
8290	[number]	k4	8290
<g/>
-	-	kIx~	-
<g/>
2258	[number]	k4	2258
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roland	Roland	k1gInSc1	Roland
Günter	Günter	k1gInSc1	Günter
<g/>
:	:	kIx,	:
Rom	Rom	k1gMnSc1	Rom
<g/>
,	,	kIx,	,
spanische	spanischus	k1gMnSc5	spanischus
Treppe	Trepp	k1gMnSc5	Trepp
<g/>
:	:	kIx,	:
Architektur	architektura	k1gFnPc2	architektura
<g/>
,	,	kIx,	,
Erfahrungen	Erfahrungen	k1gInSc1	Erfahrungen
<g/>
,	,	kIx,	,
Lebensformen	Lebensformen	k1gInSc1	Lebensformen
<g/>
.	.	kIx.	.
</s>
<s>
Hamburg	Hamburg	k1gInSc1	Hamburg
<g/>
:	:	kIx,	:
VSA	VSA	kA	VSA
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
87975	[number]	k4	87975
<g/>
-	-	kIx~	-
<g/>
155	[number]	k4	155
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Heinz-Joachim	Heinz-Joachim	k1gMnSc1	Heinz-Joachim
Fischer	Fischer	k1gMnSc1	Fischer
<g/>
:	:	kIx,	:
Rom	Rom	k1gMnSc1	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Zweieinhalb	Zweieinhalbat	k5eAaPmRp2nS	Zweieinhalbat
Jahrtausende	Jahrtausend	k1gMnSc5	Jahrtausend
Geschichte	Geschicht	k1gMnSc5	Geschicht
<g/>
,	,	kIx,	,
Kunst	Kunst	k1gFnSc1	Kunst
und	und	k?	und
Kultur	kultura	k1gFnPc2	kultura
der	drát	k5eAaImRp2nS	drát
Ewigen	Ewigen	k1gInSc1	Ewigen
Stadt	Stadt	k2eAgMnSc1d1	Stadt
<g/>
.	.	kIx.	.
</s>
<s>
DuMont	DuMont	k1gInSc1	DuMont
<g/>
,	,	kIx,	,
Köln	Köln	k1gInSc1	Köln
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7701	[number]	k4	7701
<g/>
-	-	kIx~	-
<g/>
5607	[number]	k4	5607
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
S.	S.	kA	S.
214	[number]	k4	214
<g/>
–	–	k?	–
<g/>
217	[number]	k4	217
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Henze	Henze	k1gFnSc2	Henze
<g/>
:	:	kIx,	:
Kunstführer	Kunstführer	k1gMnSc1	Kunstführer
Rom	Rom	k1gMnSc1	Rom
<g/>
.	.	kIx.	.
</s>
<s>
Reclam	Reclam	k1gInSc1	Reclam
<g/>
,	,	kIx,	,
Stuttgart	Stuttgart	k1gInSc1	Stuttgart
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
10402	[number]	k4	10402
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
S.	S.	kA	S.
304	[number]	k4	304
<g/>
–	–	k?	–
<g/>
305	[number]	k4	305
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Lotz	Lotz	k1gMnSc1	Lotz
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc5	Die
Spanische	Spanischus	k1gMnSc5	Spanischus
Treppe	Trepp	k1gMnSc5	Trepp
<g/>
.	.	kIx.	.
</s>
<s>
Architektur	architektura	k1gFnPc2	architektura
als	als	k?	als
Mittel	Mittel	k1gMnSc1	Mittel
der	drát	k5eAaImRp2nS	drát
Diplomatie	Diplomatie	k1gFnSc1	Diplomatie
<g/>
.	.	kIx.	.
in	in	k?	in
<g/>
:	:	kIx,	:
Römisches	Römisches	k1gMnSc1	Römisches
Jahrbuch	Jahrbuch	k1gMnSc1	Jahrbuch
für	für	k?	für
Kunstgeschichte	Kunstgeschicht	k1gMnSc5	Kunstgeschicht
<g/>
,	,	kIx,	,
Band	banda	k1gFnPc2	banda
12	[number]	k4	12
<g/>
,	,	kIx,	,
Tübingen	Tübingen	k1gInSc1	Tübingen
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
S.	S.	kA	S.
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
94	[number]	k4	94
<g/>
,	,	kIx,	,
Veröffentlichungen	Veröffentlichungen	k1gInSc1	Veröffentlichungen
der	drát	k5eAaImRp2nS	drát
Bibliotheca	Bibliothecus	k1gMnSc4	Bibliothecus
Hertziana	Hertziana	k1gFnSc1	Hertziana
(	(	kIx(	(
<g/>
Max-Planck-Institut	Max-Planck-Institut	k1gInSc1	Max-Planck-Institut
<g/>
)	)	kIx)	)
in	in	k?	in
Rom	Rom	k1gMnSc1	Rom
herausgegeben	herausgegeben	k2eAgInSc4d1	herausgegeben
von	von	k1gInSc4	von
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Lotz	Lotz	k1gMnSc1	Lotz
</s>
</p>
<p>
<s>
Eckart	Eckart	k1gInSc1	Eckart
Peterich	Peterich	k1gInSc1	Peterich
<g/>
:	:	kIx,	:
Rom	Rom	k1gMnSc1	Rom
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Aufl	Aufl	k1gMnSc1	Aufl
<g/>
.	.	kIx.	.
</s>
<s>
Prestel	Prestel	k1gInSc1	Prestel
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7913	[number]	k4	7913
<g/>
-	-	kIx~	-
<g/>
2043	[number]	k4	2043
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Raffalt	Raffalt	k1gMnSc1	Raffalt
<g/>
:	:	kIx,	:
Concerto	Concerta	k1gFnSc5	Concerta
Romano	Romano	k1gMnSc5	Romano
<g/>
.	.	kIx.	.
</s>
<s>
Prestel	Prestel	k1gInSc1	Prestel
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1955	[number]	k4	1955
<g/>
;	;	kIx,	;
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
<s>
Ausg	Ausg	k1gInSc1	Ausg
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
7913	[number]	k4	7913
<g/>
-	-	kIx~	-
<g/>
2236	[number]	k4	2236
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Španělské	španělský	k2eAgInPc1d1	španělský
Schody	schod	k1gInPc1	schod
<g/>
:	:	kIx,	:
panorama	panorama	k1gNnSc1	panorama
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
www.piazzadispagna.it	www.piazzadispagna.it	k1gInSc1	www.piazzadispagna.it
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
/	/	kIx~	/
<g/>
italština	italština	k1gFnSc1	italština
<g/>
)	)	kIx)	)
</s>
</p>
