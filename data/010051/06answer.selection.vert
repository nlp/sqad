<s>
Španělské	španělský	k2eAgInPc1d1	španělský
schody	schod	k1gInPc1	schod
byly	být	k5eAaImAgInP	být
postaveny	postavit	k5eAaPmNgInP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1723	[number]	k4	1723
na	na	k7c4	na
popud	popud	k1gInSc4	popud
papeže	papež	k1gMnSc2	papež
Inocenta	Inocent	k1gMnSc2	Inocent
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
schodišť	schodiště	k1gNnPc2	schodiště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
