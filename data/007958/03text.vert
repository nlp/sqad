<s>
Bavlna	bavlna	k1gFnSc1	bavlna
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
bílá	bílý	k2eAgFnSc1d1	bílá
substance	substance	k1gFnSc1	substance
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
vláken	vlákna	k1gFnPc2	vlákna
získávaných	získávaný	k2eAgFnPc2d1	získávaná
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
rodu	rod	k1gInSc3	rod
Gossypium	Gossypium	k1gNnSc4	Gossypium
(	(	kIx(	(
<g/>
bavlník	bavlník	k1gInSc1	bavlník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
bavlna	bavlna	k1gFnSc1	bavlna
podílela	podílet	k5eAaImAgFnS	podílet
cca	cca	kA	cca
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
spotřebě	spotřeba	k1gFnSc6	spotřeba
textilních	textilní	k2eAgFnPc2d1	textilní
vláken	vlákna	k1gFnPc2	vlákna
<g/>
,	,	kIx,	,
bavlnou	bavlna	k1gFnSc7	bavlna
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
(	(	kIx(	(
<g/>
od	od	k7c2	od
pěstování	pěstování	k1gNnSc2	pěstování
bavlníku	bavlník	k1gInSc2	bavlník
až	až	k9	až
po	po	k7c4	po
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
bavlněnými	bavlněný	k2eAgInPc7d1	bavlněný
výrobky	výrobek	k1gInPc7	výrobek
<g/>
)	)	kIx)	)
asi	asi	k9	asi
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
poznatků	poznatek	k1gInPc2	poznatek
se	se	k3xPyFc4	se
pěstoval	pěstovat	k5eAaImAgInS	pěstovat
bavlník	bavlník	k1gInSc1	bavlník
na	na	k7c4	na
textilní	textilní	k2eAgNnSc4d1	textilní
vlákno	vlákno	k1gNnSc4	vlákno
už	už	k6eAd1	už
před	před	k7c7	před
několika	několik	k4yIc7	několik
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nP	svědčit
nálezy	nález	k1gInPc1	nález
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
nebo	nebo	k8xC	nebo
7000	[number]	k4	7000
let	léto	k1gNnPc2	léto
staré	starý	k2eAgFnSc2d1	stará
textilie	textilie	k1gFnSc2	textilie
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
nebo	nebo	k8xC	nebo
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Systematické	systematický	k2eAgNnSc1d1	systematické
pěstování	pěstování	k1gNnSc1	pěstování
bavlníku	bavlník	k1gInSc2	bavlník
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
prodeje	prodej	k1gInSc2	prodej
vláken	vlákna	k1gFnPc2	vlákna
bavlny	bavlna	k1gFnSc2	bavlna
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
před	před	k7c7	před
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
pochází	pocházet	k5eAaImIp3nS	pocházet
asi	asi	k9	asi
nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
exportu	export	k1gInSc6	export
bavlny	bavlna	k1gFnSc2	bavlna
–	–	k?	–
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
do	do	k7c2	do
(	(	kIx(	(
<g/>
dnešního	dnešní	k2eAgInSc2d1	dnešní
<g/>
)	)	kIx)	)
Iráku	Irák	k1gInSc2	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
prodala	prodat	k5eAaPmAgFnS	prodat
první	první	k4xOgFnSc1	první
bavlna	bavlna	k1gFnSc1	bavlna
asi	asi	k9	asi
o	o	k7c4	o
200	[number]	k4	200
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pěstování	pěstování	k1gNnSc1	pěstování
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
bavlny	bavlna	k1gFnSc2	bavlna
na	na	k7c6	na
přízi	příz	k1gFnSc6	příz
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
neexistovaly	existovat	k5eNaImAgFnP	existovat
ani	ani	k9	ani
o	o	k7c4	o
pěstování	pěstování	k1gNnSc4	pěstování
ani	ani	k8xC	ani
o	o	k7c6	o
spotřebě	spotřeba	k1gFnSc6	spotřeba
bavlny	bavlna	k1gFnSc2	bavlna
žádné	žádný	k3yNgInPc1	žádný
souhrnné	souhrnný	k2eAgInPc1d1	souhrnný
údaje	údaj	k1gInPc1	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
zhruba	zhruba	k6eAd1	zhruba
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
spotřeba	spotřeba	k1gFnSc1	spotřeba
(	(	kIx(	(
<g/>
ručně	ručně	k6eAd1	ručně
<g/>
)	)	kIx)	)
spřádané	spřádaný	k2eAgFnSc2d1	spřádaná
bavlny	bavlna	k1gFnSc2	bavlna
nepřesáhla	přesáhnout	k5eNaPmAgFnS	přesáhnout
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
ročních	roční	k2eAgFnPc2d1	roční
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
bavlny	bavlna	k1gFnSc2	bavlna
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
spotřebě	spotřeba	k1gFnSc6	spotřeba
textilních	textilní	k2eAgFnPc2d1	textilní
vláken	vlákna	k1gFnPc2	vlákna
nepřesahoval	přesahovat	k5eNaImAgInS	přesahovat
5	[number]	k4	5
%	%	kIx~	%
(	(	kIx(	(
<g/>
vlna	vlna	k1gFnSc1	vlna
77	[number]	k4	77
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
len	len	k1gInSc1	len
a	a	k8xC	a
konopí	konopí	k1gNnSc1	konopí
18	[number]	k4	18
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
zavedení	zavedení	k1gNnSc6	zavedení
strojní	strojní	k2eAgFnSc2d1	strojní
výroby	výroba	k1gFnSc2	výroba
příze	příz	k1gFnSc2	příz
a	a	k8xC	a
s	s	k7c7	s
vynálezem	vynález	k1gInSc7	vynález
vyzrňovacího	vyzrňovací	k2eAgInSc2d1	vyzrňovací
stroje	stroj	k1gInSc2	stroj
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
značnému	značný	k2eAgInSc3d1	značný
nárůstu	nárůst	k1gInSc3	nárůst
spotřeby	spotřeba	k1gFnSc2	spotřeba
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
první	první	k4xOgFnSc4	první
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
přádelna	přádelna	k1gFnSc1	přádelna
bavlny	bavlna	k1gFnSc2	bavlna
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
továrnu	továrna	k1gFnSc4	továrna
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
Rakušan	Rakušan	k1gMnSc1	Rakušan
Leitenberger	Leitenberger	k1gMnSc1	Leitenberger
ve	v	k7c6	v
Verneřicích	Verneřik	k1gInPc6	Verneřik
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1791	[number]	k4	1791
do	do	k7c2	do
konce	konec	k1gInSc2	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
spotřeba	spotřeba	k1gFnSc1	spotřeba
z	z	k7c2	z
213	[number]	k4	213
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
na	na	k7c6	na
téměř	téměř	k6eAd1	téměř
stonásobek	stonásobek	k1gInSc1	stonásobek
<g/>
.	.	kIx.	.
</s>
<s>
Zpracování	zpracování	k1gNnSc1	zpracování
bavlny	bavlna	k1gFnSc2	bavlna
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
USA	USA	kA	USA
do	do	k7c2	do
Asie	Asie	k1gFnSc2	Asie
<g/>
:	:	kIx,	:
X	X	kA	X
nepotvrzený	potvrzený	k2eNgInSc1d1	nepotvrzený
odhad	odhad	k1gInSc1	odhad
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
průměrná	průměrný	k2eAgFnSc1d1	průměrná
spotřeba	spotřeba	k1gFnSc1	spotřeba
bavlny	bavlna	k1gFnSc2	bavlna
26,2	[number]	k4	26,2
miliony	milion	k4xCgInPc4	milion
tun	tuna	k1gFnPc2	tuna
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
příze	příz	k1gFnSc2	příz
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
přemísťuje	přemísťovat	k5eAaImIp3nS	přemísťovat
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
pochází	pocházet	k5eAaImIp3nS	pocházet
surovina	surovina	k1gFnSc1	surovina
<g/>
:	:	kIx,	:
Statistika	statistika	k1gFnSc1	statistika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
uvádí	uvádět	k5eAaImIp3nS	uvádět
77	[number]	k4	77
států	stát	k1gInPc2	stát
od	od	k7c2	od
tropů	trop	k1gInPc2	trop
po	po	k7c6	po
nejteplejší	teplý	k2eAgFnSc6d3	nejteplejší
oblasti	oblast	k1gFnSc6	oblast
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
bavlník	bavlník	k1gInSc4	bavlník
na	na	k7c4	na
asi	asi	k9	asi
2,4	[number]	k4	2,4
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cca	cca	kA	cca
50	[number]	k4	50
známých	známý	k2eAgInPc2d1	známý
druhů	druh	k1gInPc2	druh
se	se	k3xPyFc4	se
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
systematicky	systematicky	k6eAd1	systematicky
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
k	k	k7c3	k
získávání	získávání	k1gNnSc3	získávání
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
výrobě	výroba	k1gFnSc3	výroba
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
textilních	textilní	k2eAgFnPc2d1	textilní
vláken	vlákna	k1gFnPc2	vlákna
následující	následující	k2eAgFnSc2d1	následující
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kulturní	kulturní	k2eAgInPc4d1	kulturní
<g/>
"	"	kIx"	"
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
bavlník	bavlník	k1gInSc1	bavlník
chlupatý	chlupatý	k2eAgInSc1d1	chlupatý
(	(	kIx(	(
<g/>
Gosypium	Gosypium	k1gNnSc1	Gosypium
hirsutum	hirsutum	k1gNnSc1	hirsutum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
jako	jako	k9	jako
jednoletý	jednoletý	k2eAgInSc1d1	jednoletý
keř	keř	k1gInSc1	keř
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
asi	asi	k9	asi
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
prakticky	prakticky	k6eAd1	prakticky
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
dodávajících	dodávající	k2eAgFnPc6d1	dodávající
bavlnu	bavlna	k1gFnSc4	bavlna
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
představuje	představovat	k5eAaImIp3nS	představovat
kolem	kolem	k7c2	kolem
90	[number]	k4	90
%	%	kIx~	%
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
bílá	bílý	k2eAgNnPc1d1	bílé
nebo	nebo	k8xC	nebo
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
<g/>
,	,	kIx,	,
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
19	[number]	k4	19
<g/>
–	–	k?	–
<g/>
38	[number]	k4	38
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Bavlník	bavlník	k1gInSc1	bavlník
keřovitý	keřovitý	k2eAgInSc1d1	keřovitý
(	(	kIx(	(
<g/>
Gossypium	Gossypium	k1gNnSc1	Gossypium
Barbadense	Barbadense	k1gFnSc2	Barbadense
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Barbados	Barbadosa	k1gFnPc2	Barbadosa
<g/>
.	.	kIx.	.
</s>
<s>
Dává	dávat	k5eAaImIp3nS	dávat
nejcennější	cenný	k2eAgInPc4d3	nejcennější
druhy	druh	k1gInPc4	druh
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
zejména	zejména	k9	zejména
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Egyptě	Egypt	k1gInSc6	Egypt
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
např.	např.	kA	např.
pod	pod	k7c7	pod
obchodními	obchodní	k2eAgInPc7d1	obchodní
názvy	název	k1gInPc7	název
Pima	Pim	k1gInSc2	Pim
<g/>
,	,	kIx,	,
Sea	Sea	k1gFnSc1	Sea
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Giza	Giz	k2eAgFnSc1d1	Giz
aj.	aj.	kA	aj.
Vlákna	vlákna	k1gFnSc1	vlákna
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
jemná	jemný	k2eAgNnPc1d1	jemné
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
od	od	k7c2	od
35	[number]	k4	35
do	do	k7c2	do
nejméně	málo	k6eAd3	málo
50	[number]	k4	50
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
sklizni	sklizeň	k1gFnSc6	sklizeň
obnáší	obnášet	k5eAaImIp3nS	obnášet
asi	asi	k9	asi
8	[number]	k4	8
<g/>
%	%	kIx~	%
xx	xx	k?	xx
xx	xx	k?	xx
Údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
podílu	podíl	k1gInSc6	podíl
8	[number]	k4	8
%	%	kIx~	%
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
např.	např.	kA	např.
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
podle	podle	k7c2	podle
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
podíl	podíl	k1gInSc4	podíl
Gossypium	Gossypium	k1gNnSc4	Gossypium
Barbadense	Barbadense	k1gFnSc2	Barbadense
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
méně	málo	k6eAd2	málo
než	než	k8xS	než
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
bavlník	bavlník	k1gInSc1	bavlník
bylinný	bylinný	k2eAgInSc1d1	bylinný
(	(	kIx(	(
<g/>
Gossypium	Gossypium	k1gNnSc1	Gossypium
herbaceum	herbaceum	k1gNnSc1	herbaceum
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc6	Pákistán
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
až	až	k6eAd1	až
nahnědlá	nahnědlý	k2eAgFnSc1d1	nahnědlá
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
mm	mm	kA	mm
<g/>
.	.	kIx.	.
bavlník	bavlník	k1gInSc1	bavlník
stromový	stromový	k2eAgInSc1d1	stromový
(	(	kIx(	(
<g/>
Gossypium	Gossypium	k1gNnSc1	Gossypium
arboreum	arboreum	k1gNnSc1	arboreum
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
víceletý	víceletý	k2eAgInSc1d1	víceletý
strom	strom	k1gInSc1	strom
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
v	v	k7c6	v
afrických	africký	k2eAgFnPc6d1	africká
zemích	zem	k1gFnPc6	zem
aj.	aj.	kA	aj.
Rostlina	rostlina	k1gFnSc1	rostlina
dává	dávat	k5eAaImIp3nS	dávat
poměrně	poměrně	k6eAd1	poměrně
hrubá	hrubý	k2eAgFnSc1d1	hrubá
vlákna	vlákna	k1gFnSc1	vlákna
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákna	k1gFnSc1	vlákna
z	z	k7c2	z
bylinného	bylinný	k2eAgInSc2d1	bylinný
a	a	k8xC	a
stromového	stromový	k2eAgInSc2d1	stromový
bavlníku	bavlník	k1gInSc2	bavlník
se	se	k3xPyFc4	se
podílejí	podílet	k5eAaImIp3nP	podílet
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
sklizni	sklizeň	k1gFnSc6	sklizeň
cca	cca	kA	cca
2	[number]	k4	2
%	%	kIx~	%
K	k	k7c3	k
růstu	růst	k1gInSc3	růst
je	být	k5eAaImIp3nS	být
nutných	nutný	k2eAgInPc2d1	nutný
150	[number]	k4	150
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
dnů	den	k1gInPc2	den
bez	bez	k7c2	bez
mrazu	mráz	k1gInSc2	mráz
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
m	m	kA	m
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
kg	kg	kA	kg
vláken	vlákna	k1gFnPc2	vlákna
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
málo	málo	k6eAd1	málo
kyselá	kyselý	k2eAgFnSc1d1	kyselá
až	až	k9	až
málo	málo	k6eAd1	málo
alkalická	alkalický	k2eAgFnSc1d1	alkalická
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
odvodněná	odvodněný	k2eAgFnSc1d1	odvodněná
nebo	nebo	k8xC	nebo
jílovitý	jílovitý	k2eAgInSc1d1	jílovitý
písek	písek	k1gInSc1	písek
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
se	se	k3xPyFc4	se
před	před	k7c7	před
setím	setí	k1gNnSc7	setí
napouštějí	napouštět	k5eAaImIp3nP	napouštět
protiplísňovými	protiplísňový	k2eAgInPc7d1	protiplísňový
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Seje	sít	k5eAaImIp3nS	sít
se	se	k3xPyFc4	se
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
30-40	[number]	k4	30-40
cm	cm	kA	cm
mezi	mezi	k7c7	mezi
rostlinami	rostlina	k1gFnPc7	rostlina
a	a	k8xC	a
asi	asi	k9	asi
1	[number]	k4	1
m	m	kA	m
mezi	mezi	k7c4	mezi
řádky	řádek	k1gInPc4	řádek
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
vysetí	vysetí	k1gNnSc6	vysetí
rostlina	rostlina	k1gFnSc1	rostlina
kvete	kvést	k5eAaImIp3nS	kvést
(	(	kIx(	(
<g/>
asi	asi	k9	asi
48	[number]	k4	48
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vláknina	vláknina	k1gFnSc1	vláknina
vyzrává	vyzrávat	k5eAaImIp3nS	vyzrávat
175	[number]	k4	175
<g/>
–	–	k?	–
<g/>
225	[number]	k4	225
dnů	den	k1gInPc2	den
po	po	k7c6	po
vysetí	vysetí	k1gNnSc6	vysetí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
bavlníku	bavlník	k1gInSc6	bavlník
narůstá	narůstat	k5eAaImIp3nS	narůstat
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
tobolek	tobolka	k1gFnPc2	tobolka
<g/>
,	,	kIx,	,
v	v	k7c6	v
tobolce	tobolka	k1gFnSc6	tobolka
je	být	k5eAaImIp3nS	být
až	až	k9	až
30	[number]	k4	30
semen	semeno	k1gNnPc2	semeno
<g/>
,	,	kIx,	,
každé	každý	k3xTgNnSc1	každý
je	být	k5eAaImIp3nS	být
obrostlé	obrostlý	k2eAgNnSc1d1	obrostlé
až	až	k6eAd1	až
7000	[number]	k4	7000
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
se	se	k3xPyFc4	se
rostliny	rostlina	k1gFnPc1	rostlina
odstraní	odstranit	k5eAaPmIp3nP	odstranit
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
hnojivo	hnojivo	k1gNnSc4	hnojivo
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
jako	jako	k8xS	jako
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
3⁄	3⁄	k?	3⁄
osevních	osevní	k2eAgFnPc2d1	osevní
ploch	plocha	k1gFnPc2	plocha
uměle	uměle	k6eAd1	uměle
zavlažováno	zavlažován	k2eAgNnSc1d1	zavlažováno
<g/>
.	.	kIx.	.
</s>
<s>
Způsoby	způsob	k1gInPc1	způsob
zavlažování	zavlažování	k1gNnSc2	zavlažování
<g/>
:	:	kIx,	:
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
95	[number]	k4	95
<g/>
%	%	kIx~	%
plantáží	plantáž	k1gFnSc7	plantáž
se	se	k3xPyFc4	se
zavlažuje	zavlažovat	k5eAaImIp3nS	zavlažovat
prostým	prostý	k2eAgNnSc7d1	prosté
zatopením	zatopení	k1gNnSc7	zatopení
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
však	však	k9	však
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
40	[number]	k4	40
<g/>
-	-	kIx~	-
<g/>
60	[number]	k4	60
<g/>
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
vypařením	vypaření	k1gNnSc7	vypaření
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
2	[number]	k4	2
<g/>
%	%	kIx~	%
ploch	plocha	k1gFnPc2	plocha
se	se	k3xPyFc4	se
zalévá	zalévat	k5eAaImIp3nS	zalévat
umělým	umělý	k2eAgInSc7d1	umělý
deštěm	dešť	k1gInSc7	dešť
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
se	se	k3xPyFc4	se
až	až	k9	až
90	[number]	k4	90
<g/>
%	%	kIx~	%
využití	využití	k1gNnSc4	využití
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
metoda	metoda	k1gFnSc1	metoda
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
nákladné	nákladný	k2eAgFnPc4d1	nákladná
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Odměřováním	odměřování	k1gNnSc7	odměřování
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
zalévání	zalévání	k1gNnSc4	zalévání
po	po	k7c6	po
kapkách	kapka	k1gFnPc6	kapka
se	se	k3xPyFc4	se
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
až	až	k9	až
98	[number]	k4	98
<g/>
%	%	kIx~	%
využití	využití	k1gNnSc2	využití
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
drahá	drahý	k2eAgFnSc1d1	drahá
metoda	metoda	k1gFnSc1	metoda
používaná	používaný	k2eAgFnSc1d1	používaná
jen	jen	k9	jen
na	na	k7c4	na
1	[number]	k4	1
<g/>
%	%	kIx~	%
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Zavodňování	zavodňování	k1gNnSc1	zavodňování
plantáží	plantáž	k1gFnPc2	plantáž
vede	vést	k5eAaImIp3nS	vést
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
regionech	region	k1gInPc6	region
k	k	k7c3	k
vysychání	vysychání	k1gNnSc3	vysychání
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
k	k	k7c3	k
zasolení	zasolení	k1gNnSc3	zasolení
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
růstu	růst	k1gInSc2	růst
bavlníku	bavlník	k1gInSc2	bavlník
se	se	k3xPyFc4	se
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
při	při	k7c6	při
konvenčních	konvenční	k2eAgFnPc6d1	konvenční
metodách	metoda	k1gFnPc6	metoda
pěstování	pěstování	k1gNnSc2	pěstování
asi	asi	k9	asi
620	[number]	k4	620
prac	prac	k1gInSc4	prac
<g/>
.	.	kIx.	.
hodin	hodina	k1gFnPc2	hodina
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
a	a	k8xC	a
při	při	k7c6	při
plné	plný	k2eAgFnSc6d1	plná
modernizaci	modernizace	k1gFnSc6	modernizace
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
125	[number]	k4	125
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Spotřeba	spotřeba	k1gFnSc1	spotřeba
chemikálií	chemikálie	k1gFnPc2	chemikálie
na	na	k7c4	na
tunu	tuna	k1gFnSc4	tuna
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
:	:	kIx,	:
400	[number]	k4	400
kg	kg	kA	kg
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
40	[number]	k4	40
kg	kg	kA	kg
insekticidů	insekticid	k1gInPc2	insekticid
<g/>
,	,	kIx,	,
1,2	[number]	k4	1,2
kg	kg	kA	kg
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
opadání	opadání	k1gNnSc4	opadání
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
před	před	k7c7	před
sklizní	sklizeň	k1gFnSc7	sklizeň
<g/>
)	)	kIx)	)
Snížení	snížení	k1gNnSc4	snížení
výnosu	výnos	k1gInSc2	výnos
vlivem	vlivem	k7c2	vlivem
škůdců	škůdce	k1gMnPc2	škůdce
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
34	[number]	k4	34
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
16	[number]	k4	16
%	%	kIx~	%
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
nemocemi	nemoc	k1gFnPc7	nemoc
a	a	k8xC	a
6	[number]	k4	6
%	%	kIx~	%
plevelem	plevel	k1gInSc7	plevel
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
podpora	podpora	k1gFnSc1	podpora
pěstitelům	pěstitel	k1gMnPc3	pěstitel
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
obnášela	obnášet	k5eAaImAgFnS	obnášet
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgInSc6d1	celosvětový
průměru	průměr	k1gInSc6	průměr
0,17	[number]	k4	0,17
US	US	kA	US
<g/>
$	$	kIx~	$
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
0,07	[number]	k4	0,07
–	–	k?	–
Egypt	Egypt	k1gInSc1	Egypt
0,05	[number]	k4	0,05
–	–	k?	–
Mali	Mali	k1gNnSc1	Mali
0,03	[number]	k4	0,03
–	–	k?	–
USA	USA	kA	USA
0,26	[number]	k4	0,26
–	–	k?	–
Řecko	Řecko	k1gNnSc1	Řecko
0,92	[number]	k4	0,92
–	–	k?	–
Španělsko	Španělsko	k1gNnSc1	Španělsko
1,12	[number]	k4	1,12
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
pěstovala	pěstovat	k5eAaImAgFnS	pěstovat
geneticky	geneticky	k6eAd1	geneticky
modifikovaná	modifikovaný	k2eAgFnSc1d1	modifikovaná
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
také	také	k9	také
hybridní	hybridní	k2eAgFnSc1d1	hybridní
nebo	nebo	k8xC	nebo
Bt-bavlna	Btavlna	k1gFnSc1	Bt-bavlna
ve	v	k7c6	v
20	[number]	k4	20
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
např.	např.	kA	např.
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
95	[number]	k4	95
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
50	[number]	k4	50
%	%	kIx~	%
sklizně	sklizeň	k1gFnSc2	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
průzkumů	průzkum	k1gInPc2	průzkum
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
u	u	k7c2	u
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgInPc2d1	modifikovaný
bavlníků	bavlník	k1gInPc2	bavlník
snížit	snížit	k5eAaPmF	snížit
spotřeba	spotřeba	k1gFnSc1	spotřeba
pesticidů	pesticid	k1gInPc2	pesticid
až	až	k9	až
o	o	k7c4	o
60	[number]	k4	60
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
však	však	k9	však
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
neúměrně	úměrně	k6eNd1	úměrně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ceny	cena	k1gFnPc4	cena
modifikovaných	modifikovaný	k2eAgNnPc2d1	modifikované
semen	semeno	k1gNnPc2	semeno
(	(	kIx(	(
<g/>
ceny	cena	k1gFnPc1	cena
osiva	osivo	k1gNnSc2	osivo
se	se	k3xPyFc4	se
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
o	o	k7c4	o
více	hodně	k6eAd2	hodně
než	než	k8xS	než
500	[number]	k4	500
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepředvídatelné	předvídatelný	k2eNgInPc4d1	nepředvídatelný
následky	následek	k1gInPc4	následek
pro	pro	k7c4	pro
klasickou	klasický	k2eAgFnSc4d1	klasická
bavlnu	bavlna	k1gFnSc4	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
Bavlník	bavlník	k1gInSc1	bavlník
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
škodlivých	škodlivý	k2eAgFnPc2d1	škodlivá
chemikálií	chemikálie	k1gFnPc2	chemikálie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
ochrany	ochrana	k1gFnSc2	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
použití	použití	k1gNnSc2	použití
bavlny	bavlna	k1gFnSc2	bavlna
pro	pro	k7c4	pro
zdravotně	zdravotně	k6eAd1	zdravotně
zaručeně	zaručeně	k6eAd1	zaručeně
nezávadné	závadný	k2eNgInPc4d1	nezávadný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
z	z	k7c2	z
organické	organický	k2eAgFnSc2d1	organická
bavlny	bavlna	k1gFnSc2	bavlna
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
např.	např.	kA	např.
textilie	textilie	k1gFnSc2	textilie
pro	pro	k7c4	pro
kojence	kojenec	k1gMnPc4	kojenec
<g/>
,	,	kIx,	,
produkty	produkt	k1gInPc1	produkt
pro	pro	k7c4	pro
zdravotnickou	zdravotnický	k2eAgFnSc4d1	zdravotnická
potřebu	potřeba	k1gFnSc4	potřeba
<g/>
,	,	kIx,	,
výplň	výplň	k1gFnSc4	výplň
matrací	matrace	k1gFnPc2	matrace
a	a	k8xC	a
také	také	k9	také
oděvy	oděv	k1gInPc4	oděv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
USA	USA	kA	USA
vydány	vydán	k2eAgInPc4d1	vydán
předpisy	předpis	k1gInPc4	předpis
pro	pro	k7c4	pro
certifikaci	certifikace	k1gFnSc4	certifikace
organicky	organicky	k6eAd1	organicky
produkované	produkovaný	k2eAgFnSc2d1	produkovaná
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgInPc2	který
-	-	kIx~	-
půda	půda	k1gFnSc1	půda
před	před	k7c7	před
nasetím	nasetí	k1gNnSc7	nasetí
musí	muset	k5eAaImIp3nP	muset
3	[number]	k4	3
roky	rok	k1gInPc4	rok
ležet	ležet	k5eAaImF	ležet
ladem	ladem	k6eAd1	ladem
nebo	nebo	k8xC	nebo
-	-	kIx~	-
se	se	k3xPyFc4	se
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
musí	muset	k5eAaImIp3nS	muset
organicky	organicky	k6eAd1	organicky
pěstovat	pěstovat	k5eAaImF	pěstovat
jiné	jiný	k2eAgFnPc4d1	jiná
rostliny	rostlina	k1gFnPc4	rostlina
než	než	k8xS	než
bavlna	bavlna	k1gFnSc1	bavlna
nebo	nebo	k8xC	nebo
-	-	kIx~	-
se	s	k7c7	s
3	[number]	k4	3
roky	rok	k1gInPc7	rok
po	po	k7c6	po
sobě	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
pěstovat	pěstovat	k5eAaImF	pěstovat
bavlna	bavlna	k1gFnSc1	bavlna
organickým	organický	k2eAgInSc7d1	organický
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Výnos	výnos	k1gInSc1	výnos
organického	organický	k2eAgInSc2d1	organický
bavlníku	bavlník	k1gInSc2	bavlník
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
polovinu	polovina	k1gFnSc4	polovina
výnosu	výnos	k1gInSc2	výnos
normálně	normálně	k6eAd1	normálně
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
vlákna	vlákno	k1gNnPc1	vlákno
jsou	být	k5eAaImIp3nP	být
kratší	krátký	k2eAgInPc1d2	kratší
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgFnSc4d2	menší
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
sklizeň	sklizeň	k1gFnSc1	sklizeň
organické	organický	k2eAgFnSc2d1	organická
bavlny	bavlna	k1gFnSc2	bavlna
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
rekordního	rekordní	k2eAgInSc2d1	rekordní
podílu	podíl	k1gInSc2	podíl
1	[number]	k4	1
%	%	kIx~	%
na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
produkci	produkce	k1gFnSc6	produkce
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
množství	množství	k1gNnSc1	množství
se	se	k3xPyFc4	se
však	však	k9	však
nezvýšilo	zvýšit	k5eNaPmAgNnS	zvýšit
ani	ani	k8xC	ani
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
důležitých	důležitý	k2eAgFnPc2d1	důležitá
chemických	chemický	k2eAgFnPc2d1	chemická
vláken	vlákna	k1gFnPc2	vlákna
má	mít	k5eAaImIp3nS	mít
pěstování	pěstování	k1gNnSc1	pěstování
bavlny	bavlna	k1gFnSc2	bavlna
z	z	k7c2	z
části	část	k1gFnSc2	část
nepříznivější	příznivý	k2eNgInSc4d2	nepříznivější
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
využívání	využívání	k1gNnSc4	využívání
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
na	na	k7c4	na
zatížení	zatížení	k1gNnSc4	zatížení
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
:	:	kIx,	:
Protože	protože	k8xS	protože
bavlna	bavlna	k1gFnSc1	bavlna
nedozrává	dozrávat	k5eNaImIp3nS	dozrávat
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
sklízet	sklízet	k5eAaImF	sklízet
ve	v	k7c6	v
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
etapách	etapa	k1gFnPc6	etapa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
u	u	k7c2	u
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
sklizně	sklizeň	k1gFnSc2	sklizeň
poměr	poměra	k1gFnPc2	poměra
ruční	ruční	k2eAgMnSc1d1	ruční
ke	k	k7c3	k
strojové	strojový	k2eAgFnSc3d1	strojová
sklizni	sklizeň	k1gFnSc3	sklizeň
70	[number]	k4	70
<g/>
/	/	kIx~	/
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
ručním	ruční	k2eAgInSc6d1	ruční
sběru	sběr	k1gInSc6	sběr
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
výkonem	výkon	k1gInSc7	výkon
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
110	[number]	k4	110
kg	kg	kA	kg
nevyzrněné	vyzrněný	k2eNgFnSc2d1	vyzrněný
bavlny	bavlna	k1gFnSc2	bavlna
/	/	kIx~	/
<g/>
prac	prac	k1gInSc1	prac
<g/>
.	.	kIx.	.
<g/>
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zhruba	zhruba	k6eAd1	zhruba
0,5	[number]	k4	0,5
<g/>
–	–	k?	–
<g/>
3,5	[number]	k4	3,5
kg	kg	kA	kg
vyzrněné	vyzrněný	k2eAgFnSc2d1	vyzrněný
bavlny	bavlna	k1gFnSc2	bavlna
za	za	k7c4	za
prac	prac	k1gFnSc4	prac
<g/>
.	.	kIx.	.
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
strojním	strojní	k2eAgInSc6d1	strojní
sběru	sběr	k1gInSc6	sběr
se	se	k3xPyFc4	se
kalkuluje	kalkulovat	k5eAaImIp3nS	kalkulovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
se	s	k7c7	s
110	[number]	k4	110
US	US	kA	US
<g/>
$	$	kIx~	$
<g/>
/	/	kIx~	/
<g/>
acre	acre	k1gInSc1	acre
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
cca	cca	kA	cca
0,30	[number]	k4	0,30
€	€	k?	€
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
vlastnostem	vlastnost	k1gFnPc3	vlastnost
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
kyselinám	kyselina	k1gFnPc3	kyselina
<g/>
,	,	kIx,	,
dobré	dobrý	k2eAgFnPc4d1	dobrá
izolační	izolační	k2eAgFnPc4d1	izolační
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
odolnost	odolnost	k1gFnSc1	odolnost
proti	proti	k7c3	proti
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
záření	záření	k1gNnSc3	záření
a	a	k8xC	a
povětrnostním	povětrnostní	k2eAgInPc3d1	povětrnostní
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Bavlna	bavlna	k1gFnSc1	bavlna
se	se	k3xPyFc4	se
lisuje	lisovat	k5eAaImIp3nS	lisovat
ve	v	k7c6	v
vyzrňovacích	vyzrňovací	k2eAgFnPc6d1	vyzrňovací
stanicích	stanice	k1gFnPc6	stanice
do	do	k7c2	do
balíků	balík	k1gInPc2	balík
(	(	kIx(	(
<g/>
ø	ø	k?	ø
cca	cca	kA	cca
200	[number]	k4	200
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
každého	každý	k3xTgMnSc2	každý
balíků	balík	k1gInPc2	balík
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odebírají	odebírat	k5eAaImIp3nP	odebírat
2	[number]	k4	2
vzorky	vzorek	k1gInPc7	vzorek
(	(	kIx(	(
<g/>
2	[number]	k4	2
x	x	k?	x
cca	cca	kA	cca
120	[number]	k4	120
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
manuálně	manuálně	k6eAd1	manuálně
testují	testovat	k5eAaImIp3nP	testovat
a	a	k8xC	a
porovnávají	porovnávat	k5eAaImIp3nP	porovnávat
s	s	k7c7	s
úředními	úřední	k2eAgInPc7d1	úřední
standardy	standard	k1gInPc7	standard
(	(	kIx(	(
<g/>
vzory	vzor	k1gInPc7	vzor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
testu	test	k1gInSc2	test
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
balík	balík	k1gInSc4	balík
zařazuje	zařazovat	k5eAaImIp3nS	zařazovat
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
jakostní	jakostní	k2eAgFnSc2d1	jakostní
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
balíků	balík	k1gInPc2	balík
s	s	k7c7	s
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
rozsahu	rozsah	k1gInSc6	rozsah
hodnocení	hodnocení	k1gNnSc2	hodnocení
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
partie	partie	k1gFnSc1	partie
(	(	kIx(	(
<g/>
lot	lot	k1gInSc1	lot
<g/>
,	,	kIx,	,
batch	batch	k1gInSc1	batch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzorky	vzorek	k1gInPc1	vzorek
se	se	k3xPyFc4	se
testují	testovat	k5eAaImIp3nP	testovat
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
státních	státní	k2eAgFnPc6d1	státní
zkušebnách	zkušebna	k1gFnPc6	zkušebna
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
světové	světový	k2eAgFnSc2d1	světová
sklizně	sklizeň	k1gFnSc2	sklizeň
bavlny	bavlna	k1gFnSc2	bavlna
testují	testovat	k5eAaImIp3nP	testovat
soukromí	soukromý	k2eAgMnPc1d1	soukromý
klasifikátoři	klasifikátor	k1gMnPc1	klasifikátor
(	(	kIx(	(
<g/>
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozlišení	rozlišení	k1gNnSc4	rozlišení
úrovně	úroveň	k1gFnSc2	úroveň
kvality	kvalita	k1gFnSc2	kvalita
vlákna	vlákno	k1gNnSc2	vlákno
používá	používat	k5eAaImIp3nS	používat
(	(	kIx(	(
<g/>
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
každá	každý	k3xTgFnSc1	každý
větší	veliký	k2eAgFnSc1d2	veliký
pěstitelská	pěstitelský	k2eAgFnSc1d1	pěstitelská
země	země	k1gFnSc1	země
své	svůj	k3xOyFgInPc4	svůj
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
klasifikační	klasifikační	k2eAgInPc4d1	klasifikační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc1d1	významný
regiony	region	k1gInPc1	region
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
řídí	řídit	k5eAaImIp3nS	řídit
americkými	americký	k2eAgInPc7d1	americký
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
poloautomatické	poloautomatický	k2eAgInPc4d1	poloautomatický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
nahradit	nahradit	k5eAaPmF	nahradit
práci	práce	k1gFnSc4	práce
klasifikátorů	klasifikátor	k1gInPc2	klasifikátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
instalováno	instalován	k2eAgNnSc1d1	instalováno
2300	[number]	k4	2300
zařízení	zařízení	k1gNnPc2	zařízení
High	Higha	k1gFnPc2	Higha
Volume	volum	k1gInSc5	volum
Instrument	instrument	k1gInSc1	instrument
(	(	kIx(	(
<g/>
HVI	HVI	kA	HVI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
klasifikují	klasifikovat	k5eAaImIp3nP	klasifikovat
v	v	k7c6	v
objektivních	objektivní	k2eAgFnPc6d1	objektivní
číselných	číselný	k2eAgFnPc6d1	číselná
hodnotách	hodnota	k1gFnPc6	hodnota
<g/>
:	:	kIx,	:
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
stejnoměrnost	stejnoměrnost	k1gFnSc4	stejnoměrnost
a	a	k8xC	a
rozdělení	rozdělení	k1gNnSc4	rozdělení
staplu	stapl	k1gInSc2	stapl
<g/>
,	,	kIx,	,
zralost	zralost	k1gFnSc4	zralost
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc4	množství
a	a	k8xC	a
velikost	velikost	k1gFnSc4	velikost
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
nahradil	nahradit	k5eAaPmAgMnS	nahradit
systém	systém	k1gInSc4	systém
HVI	HVI	kA	HVI
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
manuální	manuální	k2eAgFnSc6d1	manuální
klasifikaci	klasifikace	k1gFnSc6	klasifikace
středněvlákných	středněvlákný	k2eAgFnPc2d1	středněvlákný
bavln	bavlna	k1gFnPc2	bavlna
a	a	k8xC	a
podobným	podobný	k2eAgInSc7d1	podobný
způsobem	způsob	k1gInSc7	způsob
organizuje	organizovat	k5eAaBmIp3nS	organizovat
testování	testování	k1gNnSc1	testování
vláken	vlákna	k1gFnPc2	vlákna
např.	např.	kA	např.
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Pakistán	Pakistán	k1gInSc1	Pakistán
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Výrobce	výrobce	k1gMnSc1	výrobce
HVI	HVI	kA	HVI
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
tohoto	tento	k3xDgInSc2	tento
přístroje	přístroj	k1gInSc2	přístroj
klasifikuje	klasifikovat	k5eAaImIp3nS	klasifikovat
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
95	[number]	k4	95
%	%	kIx~	%
celosvětové	celosvětový	k2eAgFnSc2d1	celosvětová
sklizně	sklizeň	k1gFnSc2	sklizeň
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
bavlny	bavlna	k1gFnSc2	bavlna
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
v	v	k7c6	v
pěstitelských	pěstitelský	k2eAgFnPc6d1	pěstitelská
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
bavlnou	bavlna	k1gFnSc7	bavlna
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
méně	málo	k6eAd2	málo
než	než	k8xS	než
30	[number]	k4	30
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
produkce	produkce	k1gFnSc2	produkce
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hodnota	hodnota	k1gFnSc1	hodnota
obnášela	obnášet	k5eAaImAgFnS	obnášet
cca	cca	kA	cca
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Pěstitelé	pěstitel	k1gMnPc1	pěstitel
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
družstevně	družstevně	k6eAd1	družstevně
organizovaní	organizovaný	k2eAgMnPc1d1	organizovaný
<g/>
)	)	kIx)	)
nechávají	nechávat	k5eAaImIp3nP	nechávat
surovou	surový	k2eAgFnSc4d1	surová
bavlnu	bavlna	k1gFnSc4	bavlna
vyzrnit	vyzrnit	k5eAaPmF	vyzrnit
a	a	k8xC	a
balíky	balík	k1gInPc4	balík
se	s	k7c7	s
spřadatelnými	spřadatelný	k2eAgNnPc7d1	spřadatelný
vlákny	vlákno	k1gNnPc7	vlákno
prodávají	prodávat	k5eAaImIp3nP	prodávat
specializovaným	specializovaný	k2eAgMnPc3d1	specializovaný
velkoobchodníkům	velkoobchodník	k1gMnPc3	velkoobchodník
(	(	kIx(	(
<g/>
příp	příp	kA	příp
<g/>
.	.	kIx.	.
dopravcům	dopravce	k1gMnPc3	dopravce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgMnPc2	který
je	on	k3xPp3gInPc4	on
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
zpracovatelé	zpracovatel	k1gMnPc1	zpracovatel
(	(	kIx(	(
<g/>
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
výrobci	výrobce	k1gMnPc1	výrobce
příze	příz	k1gFnSc2	příz
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
obchodních	obchodní	k2eAgInPc2d1	obchodní
vzorků	vzorek	k1gInPc2	vzorek
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kap	kap	k1gInSc4	kap
<g/>
.	.	kIx.	.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
bavlny	bavlna	k1gFnSc2	bavlna
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
cenový	cenový	k2eAgInSc1d1	cenový
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
orientuje	orientovat	k5eAaBmIp3nS	orientovat
na	na	k7c6	na
každodenních	každodenní	k2eAgInPc6d1	každodenní
záznamech	záznam	k1gInPc6	záznam
Burzy	burza	k1gFnSc2	burza
komodit	komodita	k1gFnPc2	komodita
(	(	kIx(	(
<g/>
commodity	commodita	k1gFnSc2	commodita
exchange	exchang	k1gFnSc2	exchang
<g/>
)	)	kIx)	)
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
cen	cena	k1gFnPc2	cena
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
(	(	kIx(	(
<g/>
od	od	k7c2	od
předpovědi	předpověď	k1gFnSc2	předpověď
počasí	počasí	k1gNnSc2	počasí
až	až	k8xS	až
např.	např.	kA	např.
po	po	k7c4	po
cenu	cena	k1gFnSc4	cena
nafty	nafta	k1gFnSc2	nafta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
regionálních	regionální	k2eAgFnPc6d1	regionální
burzách	burza	k1gFnPc6	burza
se	se	k3xPyFc4	se
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
ceny	cena	k1gFnPc1	cena
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Brémy	Brémy	k1gFnPc1	Brémy
<g/>
,	,	kIx,	,
Izmir	Izmir	k1gInSc1	Izmir
<g/>
,	,	kIx,	,
Bombaj	Bombaj	k1gFnSc1	Bombaj
<g/>
,	,	kIx,	,
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
aj.	aj.	kA	aj.
Příklad	příklad	k1gInSc4	příklad
cen	cena	k1gFnPc2	cena
na	na	k7c6	na
Brémské	brémský	k2eAgFnSc6d1	brémská
burze	burza	k1gFnSc6	burza
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
(	(	kIx(	(
<g/>
CIF	CIF	kA	CIF
v	v	k7c6	v
€	€	k?	€
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
egyptská	egyptský	k2eAgFnSc1d1	egyptská
(	(	kIx(	(
<g/>
cca	cca	kA	cca
36	[number]	k4	36
mm	mm	kA	mm
<g/>
)	)	kIx)	)
2,63	[number]	k4	2,63
€	€	k?	€
<g/>
,	,	kIx,	,
USA	USA	kA	USA
Pima	Pima	k1gFnSc1	Pima
(	(	kIx(	(
<g/>
cca	cca	kA	cca
36	[number]	k4	36
mm	mm	kA	mm
<g/>
)	)	kIx)	)
3,33	[number]	k4	3,33
€	€	k?	€
<g/>
,	,	kIx,	,
súdánská	súdánský	k2eAgFnSc1d1	súdánská
dlouhovl	dlouhovnout	k5eAaPmAgInS	dlouhovnout
<g/>
.	.	kIx.	.
2,79	[number]	k4	2,79
€	€	k?	€
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
cca	cca	kA	cca
27	[number]	k4	27
mm	mm	kA	mm
<g/>
)	)	kIx)	)
1,98	[number]	k4	1,98
€	€	k?	€
<g/>
.	.	kIx.	.
</s>
<s>
Obchody	obchod	k1gInPc1	obchod
se	se	k3xPyFc4	se
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
zčásti	zčásti	k6eAd1	zčásti
až	až	k9	až
24	[number]	k4	24
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
dnem	den	k1gInSc7	den
dodávky	dodávka	k1gFnSc2	dodávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
bavlna	bavlna	k1gFnSc1	bavlna
používá	používat	k5eAaImIp3nS	používat
skoro	skoro	k6eAd1	skoro
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
příze	příz	k1gFnSc2	příz
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
nepatrné	patrný	k2eNgNnSc4d1	nepatrné
množství	množství	k1gNnSc4	množství
netkaných	tkaný	k2eNgFnPc2d1	netkaná
textilií	textilie	k1gFnPc2	textilie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dostupných	dostupný	k2eAgFnPc2d1	dostupná
informací	informace	k1gFnPc2	informace
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jen	jen	k9	jen
zhruba	zhruba	k6eAd1	zhruba
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
bavlny	bavlna	k1gFnSc2	bavlna
se	se	k3xPyFc4	se
směsuje	směsovat	k5eAaImIp3nS	směsovat
v	v	k7c6	v
přízích	příz	k1gFnPc6	příz
s	s	k7c7	s
chemickými	chemický	k2eAgNnPc7d1	chemické
vlákny	vlákno	k1gNnPc7	vlákno
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
s	s	k7c7	s
polyesterem	polyester	k1gInSc7	polyester
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
racionální	racionální	k2eAgFnSc4d1	racionální
výrobu	výroba	k1gFnSc4	výroba
příze	příz	k1gFnSc2	příz
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
předzásobení	předzásobení	k1gNnSc1	předzásobení
surovinou	surovina	k1gFnSc7	surovina
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
nejméně	málo	k6eAd3	málo
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
6000	[number]	k4	6000
<g/>
-	-	kIx~	-
<g/>
7000	[number]	k4	7000
balíků	balík	k1gInPc2	balík
bavlny	bavlna	k1gFnSc2	bavlna
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
střední	střední	k2eAgFnSc2d1	střední
velikosti	velikost	k1gFnSc2	velikost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Balíky	balík	k1gInPc1	balík
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
z	z	k7c2	z
několika	několik	k4yIc2	několik
různých	různý	k2eAgFnPc2d1	různá
proveniencí	provenience	k1gFnPc2	provenience
a	a	k8xC	a
lotů	lot	k1gInPc2	lot
se	se	k3xPyFc4	se
skladují	skladovat	k5eAaImIp3nP	skladovat
u	u	k7c2	u
přepravce	přepravce	k1gMnSc2	přepravce
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vlastních	vlastní	k2eAgFnPc6d1	vlastní
prostorách	prostora	k1gFnPc6	prostora
přádelny	přádelna	k1gFnSc2	přádelna
<g/>
.	.	kIx.	.
<g/>
Ze	z	k7c2	z
zásoby	zásoba	k1gFnSc2	zásoba
suroviny	surovina	k1gFnSc2	surovina
se	se	k3xPyFc4	se
sestavují	sestavovat	k5eAaImIp3nP	sestavovat
přádní	přádní	k2eAgFnPc1d1	přádní
partie	partie	k1gFnPc1	partie
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
40-50	[number]	k4	40-50
balících	balík	k1gInPc6	balík
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hodnota	hodnota	k1gFnSc1	hodnota
jakostních	jakostní	k2eAgMnPc2d1	jakostní
ukazatelů	ukazatel	k1gMnPc2	ukazatel
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
<g/>
,	,	kIx,	,
pevnost	pevnost	k1gFnSc1	pevnost
vláken	vlákna	k1gFnPc2	vlákna
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
musí	muset	k5eAaImIp3nS	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
předem	předem	k6eAd1	předem
stanovené	stanovený	k2eAgFnSc3d1	stanovená
výši	výše	k1gFnSc3	výše
<g/>
,	,	kIx,	,
hodnoty	hodnota	k1gFnPc1	hodnota
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
balíků	balík	k1gInPc2	balík
se	se	k3xPyFc4	se
smějí	smát	k5eAaImIp3nP	smát
odchylovat	odchylovat	k5eAaImF	odchylovat
od	od	k7c2	od
průměru	průměr	k1gInSc2	průměr
maximálně	maximálně	k6eAd1	maximálně
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
partie	partie	k1gFnPc1	partie
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
však	však	k9	však
dá	dát	k5eAaPmIp3nS	dát
ze	z	k7c2	z
zásoby	zásoba	k1gFnSc2	zásoba
balíků	balík	k1gInPc2	balík
sestavit	sestavit	k5eAaPmF	sestavit
více	hodně	k6eAd2	hodně
partií	partie	k1gFnPc2	partie
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gFnPc1	jejich
průměrné	průměrný	k2eAgFnPc1d1	průměrná
hodnoty	hodnota	k1gFnPc1	hodnota
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
nepatrné	nepatrný	k2eAgFnPc4d1	nepatrná
odchylky	odchylka	k1gFnPc4	odchylka
<g/>
)	)	kIx)	)
srovnatelné	srovnatelný	k2eAgFnPc4d1	srovnatelná
<g/>
,	,	kIx,	,
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
přerušení	přerušení	k1gNnSc2	přerušení
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
sledu	sled	k1gInSc6	sled
<g/>
.	.	kIx.	.
</s>
<s>
Balíky	balík	k1gInPc1	balík
se	se	k3xPyFc4	se
navážejí	navážet	k5eAaImIp3nP	navážet
do	do	k7c2	do
mísírny	mísírna	k1gFnSc2	mísírna
provozu	provoz	k1gInSc2	provoz
přádelny	přádelna	k1gFnSc2	přádelna
<g/>
,	,	kIx,	,
zbaví	zbavit	k5eAaPmIp3nS	zbavit
obalu	obal	k1gInSc2	obal
a	a	k8xC	a
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
provozech	provoz	k1gInPc6	provoz
(	(	kIx(	(
<g/>
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
odebírá	odebírat	k5eAaImIp3nS	odebírat
pojízdný	pojízdný	k2eAgInSc4d1	pojízdný
automatický	automatický	k2eAgInSc4d1	automatický
rozvolňovač	rozvolňovač	k1gInSc4	rozvolňovač
z	z	k7c2	z
každého	každý	k3xTgMnSc2	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
chomáčky	chomáček	k1gInPc1	chomáček
50-100	[number]	k4	50-100
gramů	gram	k1gInPc2	gram
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
rouna	rouno	k1gNnSc2	rouno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
čechradlech	čechradlo	k1gNnPc6	čechradlo
dále	daleko	k6eAd2	daleko
rozvolňuje	rozvolňovat	k5eAaImIp3nS	rozvolňovat
na	na	k7c4	na
vločky	vločka	k1gFnPc4	vločka
o	o	k7c6	o
váze	váha	k1gFnSc6	váha
cca	cca	kA	cca
10	[number]	k4	10
g	g	kA	g
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
asi	asi	k9	asi
3-5	[number]	k4	3-5
%	%	kIx~	%
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
příměsí	příměs	k1gFnPc2	příměs
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
do	do	k7c2	do
6-8	[number]	k4	6-8
svislých	svislý	k2eAgFnPc2d1	svislá
šachet	šachta	k1gFnPc2	šachta
mísicího	mísicí	k2eAgInSc2d1	mísicí
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
směsuje	směsovat	k5eAaImIp3nS	směsovat
celý	celý	k2eAgInSc1d1	celý
obsah	obsah	k1gInSc1	obsah
(	(	kIx(	(
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
400	[number]	k4	400
kg	kg	kA	kg
<g/>
)	)	kIx)	)
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bavlna	bavlna	k1gFnSc1	bavlna
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
šachet	šachta	k1gFnPc2	šachta
klade	klást	k5eAaImIp3nS	klást
vodorovně	vodorovně	k6eAd1	vodorovně
nad	nad	k7c4	nad
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
odvádí	odvádět	k5eAaImIp3nS	odvádět
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
na	na	k7c6	na
mykacích	mykací	k2eAgInPc6d1	mykací
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Mísení	mísení	k1gNnSc1	mísení
bavlny	bavlna	k1gFnSc2	bavlna
s	s	k7c7	s
umělými	umělý	k2eAgNnPc7d1	umělé
vlákny	vlákno	k1gNnPc7	vlákno
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
zpravidla	zpravidla	k6eAd1	zpravidla
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
na	na	k7c6	na
protahovacích	protahovací	k2eAgInPc6d1	protahovací
strojích	stroj	k1gInPc6	stroj
<g/>
.	.	kIx.	.
65-70	[number]	k4	65-70
%	%	kIx~	%
oděvy	oděv	k1gInPc1	oděv
(	(	kIx(	(
<g/>
prádlo	prádlo	k1gNnSc1	prádlo
a	a	k8xC	a
svrchní	svrchní	k2eAgNnSc1d1	svrchní
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
)	)	kIx)	)
18-20	[number]	k4	18-20
%	%	kIx~	%
bytové	bytový	k2eAgFnSc2d1	bytová
textilie	textilie	k1gFnSc2	textilie
(	(	kIx(	(
<g/>
ložní	ložní	k2eAgNnSc1d1	ložní
a	a	k8xC	a
stolní	stolní	k2eAgNnSc1d1	stolní
prádlo	prádlo	k1gNnSc1	prádlo
<g/>
,	,	kIx,	,
záclonovina	záclonovina	k1gFnSc1	záclonovina
<g/>
,	,	kIx,	,
nábytkové	nábytkový	k2eAgInPc1d1	nábytkový
potahy	potah	k1gInPc1	potah
<g/>
)	)	kIx)	)
10-12	[number]	k4	10-12
%	%	kIx~	%
technické	technický	k2eAgFnSc2d1	technická
textilie	textilie	k1gFnSc2	textilie
(	(	kIx(	(
<g/>
filtry	filtr	k1gInPc1	filtr
<g/>
,	,	kIx,	,
obvazy	obvaz	k1gInPc1	obvaz
<g/>
,	,	kIx,	,
šicí	šicí	k2eAgFnPc1d1	šicí
nitě	nit	k1gFnPc1	nit
<g/>
)	)	kIx)	)
</s>
