<s>
Bavlna	bavlna	k1gFnSc1	bavlna
je	být	k5eAaImIp3nS	být
jemná	jemný	k2eAgFnSc1d1	jemná
bílá	bílý	k2eAgFnSc1d1	bílá
substance	substance	k1gFnSc1	substance
sestávající	sestávající	k2eAgFnSc1d1	sestávající
z	z	k7c2	z
vláken	vlákna	k1gFnPc2	vlákna
získávaných	získávaný	k2eAgFnPc2d1	získávaná
ze	z	k7c2	z
semen	semeno	k1gNnPc2	semeno
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
rodu	rod	k1gInSc3	rod
Gossypium	Gossypium	k1gNnSc4	Gossypium
(	(	kIx(	(
<g/>
bavlník	bavlník	k1gInSc1	bavlník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
