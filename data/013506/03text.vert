<s>
Li	li	k8xS
Ning	Ning	k1gMnSc1
</s>
<s>
Li	li	k8xS
Ning	Ning	k1gInSc1
Narození	narození	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1963	#num#	k4
(	(	kIx(
<g/>
57	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Liou-čou	Liou-čá	k1gFnSc4
Alma	alma	k1gFnSc1
mater	mater	k1gFnPc2
</s>
<s>
Pekingská	pekingský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
podnikatel	podnikatel	k1gMnSc1
<g/>
,	,	kIx,
sportovní	sportovní	k2eAgMnSc1d1
gymnasta	gymnasta	k1gMnSc1
a	a	k8xC
obchodník	obchodník	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
honorary	honorara	k1gFnPc1
doctor	doctor	k1gInSc1
of	of	k?
the	the	k?
Hong	Hong	k1gInSc1
Kong	Kongo	k1gNnPc2
Polytechnic	Polytechnice	k1gFnPc2
University	universita	k1gFnSc2
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Čchen	Čchen	k1gInSc1
Jung-jen	Jung-jna	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
gymnastika	gymnastika	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
1984	#num#	k4
</s>
<s>
prostná	prostný	k2eAgFnSc1d1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
1984	#num#	k4
</s>
<s>
kůň	kůň	k1gMnSc1
našíř	našíř	k6eAd1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
1984	#num#	k4
</s>
<s>
kruhy	kruh	k1gInPc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
1984	#num#	k4
</s>
<s>
přeskok	přeskok	k1gInSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
1984	#num#	k4
</s>
<s>
víceboj	víceboj	k1gInSc1
–	–	k?
družstva	družstvo	k1gNnSc2
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
1984	#num#	k4
</s>
<s>
víceboj	víceboj	k1gInSc1
–	–	k?
jednotlivci	jednotlivec	k1gMnSc6
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
gymnastice	gymnastika	k1gFnSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
1983	#num#	k4
</s>
<s>
víceboj	víceboj	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
MS	MS	kA
1985	#num#	k4
</s>
<s>
kruhy	kruh	k1gInPc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1983	#num#	k4
</s>
<s>
přeskok	přeskok	k1gInSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1987	#num#	k4
</s>
<s>
kruhy	kruh	k1gInPc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1985	#num#	k4
</s>
<s>
kůň	kůň	k1gMnSc1
našíř	našíř	k6eAd1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1985	#num#	k4
</s>
<s>
víceboj	víceboj	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
MS	MS	kA
1987	#num#	k4
</s>
<s>
víceboj	víceboj	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1981	#num#	k4
</s>
<s>
víceboj	víceboj	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1983	#num#	k4
</s>
<s>
kruhy	kruh	k1gInPc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1983	#num#	k4
</s>
<s>
prostná	prostný	k2eAgFnSc1d1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
MS	MS	kA
1985	#num#	k4
</s>
<s>
prostná	prostný	k2eAgFnSc1d1
</s>
<s>
Li	li	k1gMnSc1
Ning	Ning	k1gMnSc1
(	(	kIx(
<g/>
čínsky	čínsky	k6eAd1
pchin-jinem	pchin-jinem	k1gMnSc1
Lǐ	Lǐ	k1gMnSc1
Níng	Níng	k1gInSc4
<g/>
,	,	kIx,
znaky	znak	k1gInPc4
zjednodušené	zjednodušený	k2eAgFnSc2d1
李	李	kIx~
<g/>
,	,	kIx,
*	*	kIx~
10	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1963	#num#	k4
<g/>
,	,	kIx,
Laj-pin	Laj-pin	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čínský	čínský	k2eAgMnSc1d1
podnikatel	podnikatel	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
gymnasta	gymnasta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Získal	získat	k5eAaPmAgMnS
tři	tři	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c4
Los	Los	k1gInSc6
Angeles	Angeles	k1gInSc6
roku	rok	k1gInSc2
1984	#num#	k4
(	(	kIx(
<g/>
prostná	prostný	k1gFnSc1
<g/>
,	,	kIx,
kůň	kůň	k1gMnSc1
našíř	našíř	k6eAd1
<g/>
,	,	kIx,
kruhy	kruh	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stejných	stejný	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
získal	získat	k5eAaPmAgInS
též	též	k6eAd1
dvě	dva	k4xCgNnPc4
stříbra	stříbro	k1gNnPc4
(	(	kIx(
<g/>
družstva	družstvo	k1gNnPc1
<g/>
,	,	kIx,
přeskok	přeskok	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
jeden	jeden	k4xCgInSc4
bronz	bronz	k1gInSc4
(	(	kIx(
<g/>
víceboj	víceboj	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
Čínu	Čína	k1gFnSc4
měl	mít	k5eAaImAgInS
tento	tento	k3xDgInSc1
úspěch	úspěch	k1gInSc1
velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
šlo	jít	k5eAaImAgNnS
o	o	k7c4
první	první	k4xOgFnPc4
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
<g/>
,	,	kIx,
jichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
dvě	dva	k4xCgFnPc4
zlaté	zlatá	k1gFnPc4
z	z	k7c2
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
(	(	kIx(
<g/>
družstva	družstvo	k1gNnSc2
1983	#num#	k4
<g/>
,	,	kIx,
kruhy	kruh	k1gInPc1
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
ukončil	ukončit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
a	a	k8xC
založil	založit	k5eAaPmAgInS
si	se	k3xPyFc3
firmu	firma	k1gFnSc4
Li-Ning	Li-Ning	k1gInSc4
na	na	k7c4
výrobu	výroba	k1gFnSc4
sportovního	sportovní	k2eAgNnSc2d1
oblečení	oblečení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úspěšný	úspěšný	k2eAgInSc1d1
podnik	podnik	k1gInSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
učinil	učinit	k5eAaImAgInS,k5eAaPmAgInS
407	#num#	k4
<g/>
.	.	kIx.
nejbohatšího	bohatý	k2eAgMnSc2d3
Číňana	Číňan	k1gMnSc2
(	(	kIx(
<g/>
k	k	k7c3
roku	rok	k1gInSc3
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zahajovacím	zahajovací	k2eAgInSc6d1
ceremoniálu	ceremoniál	k1gInSc6
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
v	v	k7c6
Pekingu	Peking	k1gInSc6
roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostalo	dostat	k5eAaPmAgNnS
cti	čest	k1gFnSc3
zapálit	zapálit	k5eAaPmF
olympijský	olympijský	k2eAgInSc4d1
oheň	oheň	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
ženou	žena	k1gFnSc7
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
gymnastka	gymnastka	k1gFnSc1
Čchen	Čchna	k1gFnPc2
Jung-jen	Jung-jna	k1gFnPc2
<g/>
,	,	kIx,
držitelka	držitelka	k1gFnSc1
bronzové	bronzový	k2eAgFnSc2d1
medaile	medaile	k1gFnSc2
ze	z	k7c2
stejné	stejný	k2eAgFnSc2d1
olympiády	olympiáda	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
Li	li	k9
proslavil	proslavit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Li	li	k8xS
Ning	Ninga	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Li	li	k9
Ning	Ning	k1gMnSc1
Bio	Bio	k?
<g/>
,	,	kIx,
Stats	Stats	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Results	Results	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olympics	Olympics	k1gInSc1
at	at	k?
Sports-Reference	Sports-Reference	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Li	li	k8xS
Ning	Ning	k1gMnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
gymnastice	gymnastika	k1gFnSc6
-	-	kIx~
prostná	prostný	k2eAgFnSc1d1
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
István	István	k2eAgInSc1d1
Pelle	Pelle	k1gInSc1
•	•	k?
1936	#num#	k4
<g/>
:	:	kIx,
Georges	Georges	k1gInSc1
Miez	Miez	k1gInSc1
•	•	k?
1948	#num#	k4
<g/>
:	:	kIx,
Ferenc	Ferenc	k1gMnSc1
Pataki	Patak	k1gFnSc2
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
William	William	k1gInSc1
Thoresson	Thoresson	k1gInSc1
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
Valentin	Valentina	k1gFnPc2
Muratov	Muratov	k1gInSc1
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Nobujuki	Nobujuke	k1gFnSc4
Aihara	Aihara	k1gFnSc1
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Franco	Franco	k6eAd1
Menichelli	Menichelle	k1gFnSc4
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
Sawao	Sawao	k6eAd1
Kató	Kató	k1gFnSc1
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Nikolaj	Nikolaj	k1gMnSc1
Andrianov	Andrianov	k1gInSc1
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Andrianov	Andrianov	k1gInSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Roland	Roland	k1gInSc1
Brückner	Brückner	k1gInSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Li	li	k8xS
Ning	Ning	k1gInSc1
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Sergej	Sergej	k1gMnSc1
Charkov	Charkov	k1gInSc1
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Li	li	k8xS
Siao-šuang	Siao-šuang	k1gInSc1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Ioannis	Ioannis	k1gFnSc1
Melissanidis	Melissanidis	k1gFnSc2
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Igors	Igors	k1gInSc1
Vihrovs	Vihrovs	k1gInSc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Kyle	Kyl	k1gInSc2
Shewfelt	Shewfelt	k1gInSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Cou	Cou	k1gFnSc1
Kchaj	Kchaj	k1gInSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Cou	Cou	k1gFnSc1
Kchaj	Kchaj	k1gInSc1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Max	Max	k1gMnSc1
Whitlock	Whitlock	k1gMnSc1
</s>
<s>
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
gymnastice	gymnastika	k1gFnSc6
-	-	kIx~
kůň	kůň	k1gMnSc1
našíř	našíř	k6eAd1
</s>
<s>
1896	#num#	k4
<g/>
:	:	kIx,
Louis	louis	k1gInSc2
Zutter	Zutter	k1gInSc1
•	•	k?
1900	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1904	#num#	k4
<g/>
:	:	kIx,
Anton	Anton	k1gMnSc1
Heida	Heida	k1gMnSc1
•	•	k?
1908	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1912	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1920	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1924	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Wilhelm	Wilhelm	k1gMnSc1
•	•	k?
1928	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Hermann	Hermann	k1gMnSc1
Hänggi	Hängg	k1gFnSc2
•	•	k?
1932	#num#	k4
<g/>
:	:	kIx,
István	István	k2eAgInSc1d1
Pelle	Pelle	k1gInSc1
•	•	k?
1936	#num#	k4
<g/>
:	:	kIx,
Konrad	Konrad	k1gInSc1
Frey	Frea	k1gFnSc2
•	•	k?
1948	#num#	k4
<g/>
:	:	kIx,
Paavo	Paavo	k1gNnSc4
Aaltonen	Aaltonno	k1gNnPc2
<g/>
,	,	kIx,
Veikko	Veikko	k1gNnSc1
Huhtanen	Huhtanna	k1gFnPc2
a	a	k8xC
Heikki	Heikki	k1gNnPc2
Savolainen	Savolainno	k1gNnPc2
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Čukarin	Čukarin	k1gInSc1
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
Boris	Boris	k1gMnSc1
Šachlin	Šachlina	k1gFnPc2
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Eugen	Eugen	k2eAgMnSc1d1
Ekman	Ekman	k1gMnSc1
a	a	k8xC
Boris	Boris	k1gMnSc1
Šachlin	Šachlina	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Miroslav	Miroslav	k1gMnSc1
Cerar	Cerara	k1gFnPc2
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
Miroslav	Miroslav	k1gMnSc1
Cerar	Cerara	k1gFnPc2
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Viktor	Viktor	k1gMnSc1
Klimenko	Klimenka	k1gFnSc5
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Zoltán	Zoltán	k2eAgInSc1d1
Magyar	Magyar	k1gInSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Zoltán	Zoltán	k2eAgInSc1d1
Magyar	Magyar	k1gInSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Li	li	k8xS
Ning	Ning	k1gMnSc1
a	a	k8xC
Peter	Peter	k1gMnSc1
Vidmar	Vidmara	k1gFnPc2
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Dmitrij	Dmitrij	k1gMnSc1
Bilozerčev	Bilozerčev	k1gMnSc1
<g/>
,	,	kIx,
Zsolt	Zsolt	k1gMnSc1
Borkai	Borka	k1gFnSc2
a	a	k8xC
Lubomir	Lubomir	k1gInSc1
Geraskov	Geraskov	k1gInSc1
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Pae	Pae	k1gMnSc4
Gil-Su	Gil-Sa	k1gMnSc4
a	a	k8xC
Vitalij	Vitalij	k1gMnSc4
Ščerbo	Ščerba	k1gFnSc5
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Donghua	Donghu	k1gInSc2
Li	li	k8xS
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Marius	Marius	k1gInSc1
Urzică	Urzică	k1gFnSc2
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Teng	Teng	k1gInSc4
Haibin	Haibin	k2eAgInSc4d1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Xiao	Xiao	k6eAd1
Qin	Qin	k1gFnSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Krisztián	Krisztián	k1gMnSc1
Berki	Berk	k1gFnSc2
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Max	Max	k1gMnSc1
Whitlock	Whitlock	k1gMnSc1
</s>
<s>
Olympijští	olympijský	k2eAgMnPc1d1
vítězové	vítěz	k1gMnPc1
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
gymnastice	gymnastika	k1gFnSc6
-	-	kIx~
kruhy	kruh	k1gInPc1
</s>
<s>
1896	#num#	k4
<g/>
:	:	kIx,
Ioannis	Ioannis	k1gFnSc1
Mitropulos	Mitropulos	k1gInSc1
•	•	k?
1900	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1904	#num#	k4
<g/>
:	:	kIx,
Herman	Herman	k1gMnSc1
Glass	Glassa	k1gFnPc2
•	•	k?
1908	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1912	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1920	#num#	k4
<g/>
:	:	kIx,
nebylo	být	k5eNaImAgNnS
na	na	k7c6
programu	program	k1gInSc6
LOH	LOH	kA
•	•	k?
1924	#num#	k4
<g/>
:	:	kIx,
Francesco	Francesco	k6eAd1
Martino	Martin	k2eAgNnSc4d1
•	•	k?
1928	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Leon	Leona	k1gFnPc2
Štukelj	Štukelj	k1gInSc1
•	•	k?
1932	#num#	k4
<g/>
:	:	kIx,
George	Georg	k1gInSc2
Gulack	Gulack	k1gInSc1
•	•	k?
1936	#num#	k4
<g/>
:	:	kIx,
Alois	Alois	k1gMnSc1
Hudec	Hudec	k1gMnSc1
•	•	k?
1948	#num#	k4
<g/>
:	:	kIx,
Karl	Karl	k1gMnSc1
Frei	Fre	k1gFnSc2
•	•	k?
1952	#num#	k4
<g/>
:	:	kIx,
Grant	grant	k1gInSc1
Shaginyan	Shaginyan	k1gInSc1
•	•	k?
1956	#num#	k4
<g/>
:	:	kIx,
Albert	Alberta	k1gFnPc2
Azarjan	Azarjan	k1gInSc1
•	•	k?
1960	#num#	k4
<g/>
:	:	kIx,
Albert	Alberta	k1gFnPc2
Azarjan	Azarjan	k1gInSc1
•	•	k?
1964	#num#	k4
<g/>
:	:	kIx,
Takuji	Takuji	k1gFnSc1
Hayata	Hayata	k1gFnSc1
•	•	k?
1968	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Akinori	Akinori	k1gNnSc7
Nakajama	Nakajamum	k1gNnSc2
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Akinori	Akinor	k1gFnSc3
Nakajama	Nakajamum	k1gNnSc2
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Nikolaj	Nikolaj	k1gMnSc1
Andrianov	Andrianov	k1gInSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Alexandr	Alexandr	k1gMnSc1
Diťatin	Diťatina	k1gFnPc2
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Koji	Koji	k1gNnSc1
Gushiken	Gushikna	k1gFnPc2
a	a	k8xC
Li	li	k9
Ning	Ning	k1gInSc4
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Holger	Holger	k1gInSc1
Behrendt	Behrendt	k1gInSc1
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Vitalij	Vitalij	k1gMnSc5
Ščerbo	Ščerba	k1gMnSc5
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Jury	jury	k1gFnSc1
Chechi	Chech	k1gFnSc2
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Szilveszter	Szilveszter	k1gInSc1
Csollány	Csollán	k1gInPc1
•	•	k?
2004	#num#	k4
<g/>
:	:	kIx,
Dimosthenis	Dimosthenis	k1gFnSc1
Tampakos	Tampakos	k1gInSc1
•	•	k?
2008	#num#	k4
<g/>
:	:	kIx,
Chen	Chen	k1gInSc1
Yibing	Yibing	k1gInSc1
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
Arthur	Arthur	k1gMnSc1
Zanetti	Zanetť	k1gFnSc2
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
Eleftherios	Eleftherios	k1gMnSc1
Petrounias	Petrounias	k1gMnSc1
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
ve	v	k7c6
sportovní	sportovní	k2eAgFnSc6d1
gymnastice	gymnastika	k1gFnSc6
-	-	kIx~
kruhy	kruh	k1gInPc1
</s>
<s>
MS	MS	kA
1903	#num#	k4
Josef	Josef	k1gMnSc1
Martinez	Martinez	k1gMnSc1
•	•	k?
1905	#num#	k4
Nekonalo	konat	k5eNaImAgNnS
se	s	k7c7
•	•	k?
1907	#num#	k4
Nekonalo	konat	k5eNaImAgNnS
se	s	k7c7
•	•	k?
MS	MS	kA
1909	#num#	k4
Marco	Marco	k6eAd1
Torrè	Torrè	k1gInSc1
a	a	k8xC
Giorgio	Giorgio	k1gMnSc1
Romano	Romano	k1gMnSc1
•	•	k?
MS	MS	kA
1911	#num#	k4
Ferdinand	Ferdinand	k1gMnSc1
Steiner	Steiner	k1gMnSc1
•	•	k?
MS	MS	kA
1913	#num#	k4
Marco	Marco	k6eAd1
Torrè	Torrè	k1gInSc1
<g/>
,	,	kIx,
Laurent	Laurent	k1gInSc1
Grech	Grech	k1gInSc1
<g/>
,	,	kIx,
Guido	Guido	k1gNnSc1
Boni	Bon	k1gFnSc2
<g/>
,	,	kIx,
Giorgio	Giorgio	k1gMnSc1
Zampori	Zampor	k1gFnSc2
•	•	k?
1915	#num#	k4
<g/>
–	–	k?
<g/>
1917	#num#	k4
Nekonalo	konat	k5eNaImAgNnS
se	s	k7c7
-	-	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
MS	MS	kA
1922	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Karásek	Karásek	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Malý	Malý	k1gMnSc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Šumi	Šum	k1gFnSc2
<g/>
,	,	kIx,
Leon	Leona	k1gFnPc2
Stuckelj	Stuckelj	k1gFnSc1
•	•	k?
MS	MS	kA
1926	#num#	k4
Leon	Leona	k1gFnPc2
Stuckelj	Stuckelj	k1gFnSc1
•	•	k?
MS	MS	kA
1930	#num#	k4
Emanuel	Emanuel	k1gMnSc1
Löffler	Löffler	k1gMnSc1
•	•	k?
MS	MS	kA
1934	#num#	k4
Alois	Alois	k1gMnSc1
Hudec	Hudec	k1gMnSc1
•	•	k?
MS	MS	kA
1938	#num#	k4
Alois	Alois	k1gMnSc1
Hudec	Hudec	k1gMnSc1
•	•	k?
1942	#num#	k4
Nekonalo	konat	k5eNaImAgNnS
se	se	k3xPyFc4
-	-	kIx~
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
•	•	k?
MS	MS	kA
<g />
.	.	kIx.
</s>
<s hack="1">
1950	#num#	k4
Walther	Walthra	k1gFnPc2
Lehmann	Lehmann	k1gInSc4
•	•	k?
MS	MS	kA
1954	#num#	k4
Albert	Alberta	k1gFnPc2
Azarjan	Azarjan	k1gInSc4
•	•	k?
MS	MS	kA
1958	#num#	k4
Albert	Alberta	k1gFnPc2
Azarjan	Azarjan	k1gInSc4
•	•	k?
MS	MS	kA
1962	#num#	k4
Yuri	Yuri	k1gNnPc2
Titov	Titov	k1gInSc1
•	•	k?
MS	MS	kA
1966	#num#	k4
Mikhail	Mikhaila	k1gFnPc2
Voronine	Voronin	k1gInSc5
•	•	k?
MS	MS	kA
1970	#num#	k4
Akinori	Akinor	k1gFnSc2
Nakayama	Nakayama	k1gFnSc1
•	•	k?
MS	MS	kA
1974	#num#	k4
Danuţ	Danuţ	k1gFnPc6
Grecu	Grecus	k1gInSc2
a	a	k8xC
Nikolaj	Nikolaj	k1gMnSc1
Andrianov	Andrianov	k1gInSc1
•	•	k?
MS	MS	kA
1978	#num#	k4
Nikolaj	Nikolaj	k1gMnSc1
Andrianov	Andrianov	k1gInSc1
•	•	k?
MS	MS	kA
1979	#num#	k4
Alexandr	Alexandr	k1gMnSc1
Diťatin	Diťatina	k1gFnPc2
•	•	k?
MS	MS	kA
1981	#num#	k4
Alexandr	Alexandr	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Diťatin	Diťatina	k1gFnPc2
•	•	k?
MS	MS	kA
1983	#num#	k4
Koji	Koj	k1gFnSc2
Gushiken	Gushikna	k1gFnPc2
a	a	k8xC
Dimitri	Dimitr	k1gFnSc2
Bilozertchev	Bilozertchev	k1gFnSc1
•	•	k?
MS	MS	kA
1985	#num#	k4
Li	li	k8xS
Ning	Ning	k1gInSc1
a	a	k8xC
Yuri	Yuri	k1gNnSc1
Korolev	Korolev	k1gFnSc2
•	•	k?
MS	MS	kA
1987	#num#	k4
Yuri	Yur	k1gFnSc2
Korolev	Korolev	k1gFnSc2
•	•	k?
MS	MS	kA
1989	#num#	k4
Andreas	Andreas	k1gInSc1
Aguilar	Aguilar	k1gInSc4
•	•	k?
MS	MS	kA
1991	#num#	k4
Grigory	Grigora	k1gFnSc2
Misutin	Misutina	k1gFnPc2
•	•	k?
MS	MS	kA
1992	#num#	k4
Vitali	Vitali	k1gFnSc2
Scherbo	Scherba	k1gFnSc5
•	•	k?
MS	MS	kA
1993	#num#	k4
Yuri	Yur	k1gFnSc2
Chechi	Chech	k1gFnSc2
•	•	k?
MS	MS	kA
1994	#num#	k4
Yuri	Yur	k1gFnSc2
Chechi	Chech	k1gFnSc2
•	•	k?
MS	MS	kA
1995	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Yuri	Yuri	k1gNnSc7
Chechi	Chech	k1gFnSc2
•	•	k?
MS	MS	kA
1996	#num#	k4
Yuri	Yur	k1gFnSc2
Chechi	Chech	k1gFnSc2
•	•	k?
MS	MS	kA
1997	#num#	k4
Yuri	Yur	k1gFnSc2
Chechi	Chech	k1gFnSc2
•	•	k?
MS	MS	kA
1999	#num#	k4
Dong	dong	k1gInSc1
Zhen	Zhen	k1gInSc4
•	•	k?
MS	MS	kA
2001	#num#	k4
Yordan	Yordany	k1gInPc2
Yovchev	Yovchev	k1gFnSc1
•	•	k?
MS	MS	kA
2002	#num#	k4
Szilveszter	Szilveszter	k1gInSc1
Csollány	Csollán	k1gInPc1
•	•	k?
MS	MS	kA
2003	#num#	k4
Yordan	Yordany	k1gInPc2
Yovchev	Yovchva	k1gFnPc2
a	a	k8xC
Dimosthenis	Dimosthenis	k1gFnPc2
Tampakos	Tampakos	k1gMnSc1
•	•	k?
MS	MS	kA
2005	#num#	k4
Yuri	Yur	k1gFnSc2
van	vana	k1gFnPc2
Gelder	Gelder	k1gMnSc1
•	•	k?
MS	MS	kA
2006	#num#	k4
Chen	Chen	k1gInSc1
Yibing	Yibing	k1gInSc4
•	•	k?
MS	MS	kA
2007	#num#	k4
Chen	Chen	k1gInSc1
Yibing	Yibing	k1gInSc4
•	•	k?
MS	MS	kA
2009	#num#	k4
Yan	Yan	k1gFnPc2
Mingyong	Mingyong	k1gInSc4
•	•	k?
MS	MS	kA
2010	#num#	k4
Chen	Chen	k1gInSc1
Yibing	Yibing	k1gInSc4
•	•	k?
MS	MS	kA
2011	#num#	k4
Chen	Chen	k1gInSc1
Yibing	Yibing	k1gInSc4
•	•	k?
MS	MS	kA
2013	#num#	k4
Arthur	Arthura	k1gFnPc2
Zanetti	Zanetť	k1gFnSc2
•	•	k?
MS	MS	kA
2014	#num#	k4
Liu	Liu	k1gFnPc2
Yang	Yang	k1gInSc4
•	•	k?
MS	MS	kA
2015	#num#	k4
Eleftherios	Eleftherios	k1gInSc1
Petrounias	Petrounias	k1gInSc4
•	•	k?
MS	MS	kA
2017	#num#	k4
Eleftherios	Eleftherios	k1gInSc1
Petrounias	Petrounias	k1gInSc4
•	•	k?
MS	MS	kA
2018	#num#	k4
Eleftherios	Eleftherios	k1gInSc1
Petrounias	Petrounias	k1gInSc4
•	•	k?
MS	MS	kA
2019	#num#	k4
İ	İ	k1gInSc1
Çolak	Çolak	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Čína	Čína	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
