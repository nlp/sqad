<s>
Caliban	Caliban	k1gInSc1	Caliban
je	být	k5eAaImIp3nS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
od	od	k7c2	od
planety	planeta	k1gFnSc2	planeta
Uran	Uran	k1gInSc1	Uran
<g/>
.	.	kIx.	.
</s>
<s>
Planetu	planeta	k1gFnSc4	planeta
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
7100000	[number]	k4	7100000
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
72	[number]	k4	72
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc1	hmotnost
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
7,4	[number]	k4	7,4
<g/>
×	×	k?	×
<g/>
1017	[number]	k4	1017
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
kolektivem	kolektiv	k1gInSc7	kolektiv
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
(	(	kIx(	(
<g/>
Brett	Brett	k1gMnSc1	Brett
J.	J.	kA	J.
Gladman	Gladman	k1gMnSc1	Gladman
<g/>
,	,	kIx,	,
Philip	Philip	k1gMnSc1	Philip
D.	D.	kA	D.
Nicholson	Nicholson	k1gMnSc1	Nicholson
<g/>
,	,	kIx,	,
Joseph	Joseph	k1gMnSc1	Joseph
A.	A.	kA	A.
Burns	Burns	k1gInSc1	Burns
a	a	k8xC	a
John	John	k1gMnSc1	John
J.	J.	kA	J.
Kavelaars	Kavelaars	k1gInSc1	Kavelaars
<g/>
)	)	kIx)	)
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
provizorně	provizorně	k6eAd1	provizorně
nazván	nazvat	k5eAaPmNgInS	nazvat
S	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
U	u	k7c2	u
1	[number]	k4	1
a	a	k8xC	a
poté	poté	k6eAd1	poté
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
díla	dílo	k1gNnSc2	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
jedné	jeden	k4xCgFnSc2	jeden
otáčky	otáčka	k1gFnSc2	otáčka
kolem	kolem	k7c2	kolem
osy	osa	k1gFnSc2	osa
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
