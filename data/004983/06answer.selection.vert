<s>
Čukotský	čukotský	k2eAgInSc1d1	čukotský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Čukotka	Čukotka	k1gFnSc1	Čukotka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ч	Ч	k?	Ч
п	п	k?	п
<g/>
,	,	kIx,	,
Ч	Ч	k?	Ч
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
poloostrov	poloostrov	k1gInSc4	poloostrov
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
