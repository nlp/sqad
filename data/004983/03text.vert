<s>
Čukotský	čukotský	k2eAgInSc1d1	čukotský
poloostrov	poloostrov	k1gInSc1	poloostrov
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
Čukotka	Čukotka	k1gFnSc1	Čukotka
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Ч	Ч	k?	Ч
п	п	k?	п
<g/>
,	,	kIx,	,
Ч	Ч	k?	Ч
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejvýchodnější	východní	k2eAgInSc4d3	nejvýchodnější
poloostrov	poloostrov	k1gInSc4	poloostrov
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
administrativně	administrativně	k6eAd1	administrativně
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
Čukotský	čukotský	k2eAgInSc4d1	čukotský
autonomní	autonomní	k2eAgInSc4d1	autonomní
okruh	okruh	k1gInSc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Čukotské	čukotský	k2eAgNnSc1d1	Čukotské
moře	moře	k1gNnSc1	moře
<g/>
,	,	kIx,	,
na	na	k7c4	na
jih	jih	k1gInSc4	jih
Beringovo	Beringův	k2eAgNnSc1d1	Beringovo
moře	moře	k1gNnSc1	moře
(	(	kIx(	(
<g/>
Anadyrský	Anadyrský	k2eAgInSc1d1	Anadyrský
záliv	záliv	k1gInSc1	záliv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pevnina	pevnina	k1gFnSc1	pevnina
zakončena	zakončit	k5eAaPmNgFnS	zakončit
Děžňovovým	Děžňovův	k2eAgInSc7d1	Děžňovův
mysem	mys	k1gInSc7	mys
<g/>
,	,	kIx,	,
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
Čukotku	Čukotka	k1gFnSc4	Čukotka
od	od	k7c2	od
americké	americký	k2eAgFnSc2d1	americká
Aljašky	Aljaška	k1gFnSc2	Aljaška
Beringův	Beringův	k2eAgInSc1d1	Beringův
průliv	průliv	k1gInSc1	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
49	[number]	k4	49
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
Ischodnaja	Ischodnaja	k1gFnSc1	Ischodnaja
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
1	[number]	k4	1
158	[number]	k4	158
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Osídlení	osídlení	k1gNnSc1	osídlení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídké	řídký	k2eAgNnSc1d1	řídké
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
tu	ten	k3xDgFnSc4	ten
zejména	zejména	k9	zejména
Čukčové	Čukč	k1gMnPc1	Čukč
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
přístavem	přístav	k1gInSc7	přístav
je	být	k5eAaImIp3nS	být
Providěnija	Providěnija	k1gFnSc1	Providěnija
(	(	kIx(	(
<g/>
2	[number]	k4	2
082	[number]	k4	082
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
subpolární	subpolární	k2eAgFnSc1d1	subpolární
<g/>
,	,	kIx,	,
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
tundrové	tundrový	k2eAgNnSc1d1	tundrové
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
zvěř	zvěř	k1gFnSc1	zvěř
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
wolfram	wolfram	k1gInSc1	wolfram
a	a	k8xC	a
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
