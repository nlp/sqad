<p>
<s>
Crookesův	Crookesův	k2eAgInSc1d1	Crookesův
mlýnek	mlýnek	k1gInSc1	mlýnek
je	být	k5eAaImIp3nS	být
jednoduchý	jednoduchý	k2eAgInSc1d1	jednoduchý
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
přístroj	přístroj	k1gInSc1	přístroj
<g/>
,	,	kIx,	,
demonstrující	demonstrující	k2eAgNnSc1d1	demonstrující
působení	působení	k1gNnSc1	působení
energie	energie	k1gFnSc2	energie
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
skleněné	skleněný	k2eAgFnSc2d1	skleněná
baňky	baňka	k1gFnSc2	baňka
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
vyčerpán	vyčerpat	k5eAaPmNgInS	vyčerpat
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jehlovém	jehlový	k2eAgNnSc6d1	jehlové
ložisku	ložisko	k1gNnSc6	ložisko
umístěn	umístěn	k2eAgInSc1d1	umístěn
mlýnek	mlýnek	k1gInSc1	mlýnek
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
listy	list	k1gInPc7	list
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
černé	černá	k1gFnSc2	černá
(	(	kIx(	(
<g/>
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
strany	strana	k1gFnSc2	strana
bílé	bílý	k2eAgFnSc2d1	bílá
(	(	kIx(	(
<g/>
odráží	odrážet	k5eAaImIp3nS	odrážet
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dopadající	dopadající	k2eAgNnSc1d1	dopadající
záření	záření	k1gNnSc1	záření
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tmavé	tmavý	k2eAgFnPc1d1	tmavá
strany	strana	k1gFnPc1	strana
listů	list	k1gInPc2	list
začnou	začít	k5eAaPmIp3nP	začít
pohybovat	pohybovat	k5eAaImF	pohybovat
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
mlýnku	mlýnek	k1gInSc2	mlýnek
se	se	k3xPyFc4	se
uvádějí	uvádět	k5eAaImIp3nP	uvádět
dvě	dva	k4xCgNnPc1	dva
vysvětlení	vysvětlení	k1gNnPc1	vysvětlení
závislá	závislý	k2eAgNnPc1d1	závislé
na	na	k7c6	na
stupni	stupeň	k1gInSc6	stupeň
vakua	vakuum	k1gNnSc2	vakuum
v	v	k7c6	v
baňce	baňka	k1gFnSc6	baňka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
černá	černý	k2eAgFnSc1d1	černá
plocha	plocha	k1gFnSc1	plocha
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
a	a	k8xC	a
protože	protože	k8xS	protože
teplý	teplý	k2eAgInSc1d1	teplý
vzduch	vzduch	k1gInSc1	vzduch
za	za	k7c7	za
listem	list	k1gInSc7	list
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
začne	začít	k5eAaPmIp3nS	začít
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
mlýnek	mlýnek	k1gInSc1	mlýnek
<g/>
"	"	kIx"	"
otáčet	otáčet	k5eAaImF	otáčet
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
dopadajících	dopadající	k2eAgInPc2d1	dopadající
fotonů	foton	k1gInPc2	foton
-	-	kIx~	-
tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
nastane	nastat	k5eAaPmIp3nS	nastat
při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
kolem	kolem	k7c2	kolem
1	[number]	k4	1
Pa	Pa	kA	Pa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
způsoben	způsoben	k2eAgInSc1d1	způsoben
přeneseným	přenesený	k2eAgNnSc7d1	přenesené
teplem	teplo	k1gNnSc7	teplo
nikoliv	nikoliv	k9	nikoliv
částicovým	částicový	k2eAgInSc7d1	částicový
charakterem	charakter	k1gInSc7	charakter
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tlaku	tlak	k1gInSc6	tlak
řádově	řádově	k6eAd1	řádově
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
4	[number]	k4	4
Pa	Pa	kA	Pa
se	se	k3xPyFc4	se
mlýnek	mlýnek	k1gInSc4	mlýnek
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
lesklá	lesklý	k2eAgFnSc1d1	lesklá
plocha	plocha	k1gFnSc1	plocha
odráží	odrážet	k5eAaImIp3nS	odrážet
fotony	foton	k1gInPc4	foton
<g/>
,	,	kIx,	,
foton	foton	k1gInSc4	foton
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
předá	předat	k5eAaPmIp3nS	předat
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
tmavá	tmavý	k2eAgFnSc1d1	tmavá
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
hypotéza	hypotéza	k1gFnSc1	hypotéza
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
mylná	mylný	k2eAgFnSc1d1	mylná
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
kinetická	kinetický	k2eAgFnSc1d1	kinetická
síla	síla	k1gFnSc1	síla
dopadajících	dopadající	k2eAgInPc2d1	dopadající
fotonů	foton	k1gInPc2	foton
je	být	k5eAaImIp3nS	být
příliš	příliš	k6eAd1	příliš
malá	malý	k2eAgFnSc1d1	malá
pro	pro	k7c4	pro
buzení	buzení	k1gNnSc4	buzení
takového	takový	k3xDgInSc2	takový
makroskopického	makroskopický	k2eAgInSc2d1	makroskopický
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
důkaz	důkaz	k1gInSc4	důkaz
částicového	částicový	k2eAgInSc2d1	částicový
charakteru	charakter	k1gInSc2	charakter
světla	světlo	k1gNnSc2	světlo
(	(	kIx(	(
<g/>
fotonu	foton	k1gInSc2	foton
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
Nicholsův	Nicholsův	k2eAgInSc4d1	Nicholsův
radiometr	radiometr	k1gInSc4	radiometr
<g/>
.	.	kIx.	.
</s>
<s>
Přístroj	přístroj	k1gInSc1	přístroj
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
chemik	chemik	k1gMnSc1	chemik
William	William	k1gInSc1	William
Crookes	Crookes	k1gInSc4	Crookes
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
produkt	produkt	k1gInSc4	produkt
svého	svůj	k3xOyFgInSc2	svůj
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
změřit	změřit	k5eAaPmF	změřit
působení	působení	k1gNnPc4	působení
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ovlivňovalo	ovlivňovat	k5eAaImAgNnS	ovlivňovat
jeho	jeho	k3xOp3gNnPc1	jeho
přesná	přesný	k2eAgNnPc1d1	přesné
kvantitativní	kvantitativní	k2eAgNnPc1d1	kvantitativní
chemická	chemický	k2eAgNnPc1d1	chemické
měření	měření	k1gNnPc1	měření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
zařízení	zařízení	k1gNnSc1	zařízení
je	být	k5eAaImIp3nS	být
také	také	k9	také
někdy	někdy	k6eAd1	někdy
využíváno	využívat	k5eAaImNgNnS	využívat
v	v	k7c6	v
experimentální	experimentální	k2eAgFnSc6d1	experimentální
parapsychologii	parapsychologie	k1gFnSc6	parapsychologie
v	v	k7c6	v
pokusech	pokus	k1gInPc6	pokus
snažících	snažící	k2eAgFnPc2d1	snažící
se	se	k3xPyFc4	se
prokázat	prokázat	k5eAaPmF	prokázat
existenci	existence	k1gFnSc4	existence
telekineze	telekineze	k1gFnSc2	telekineze
<g/>
.	.	kIx.	.
</s>
<s>
Testované	testovaný	k2eAgFnPc1d1	testovaná
osoby	osoba	k1gFnPc1	osoba
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
pokusit	pokusit	k5eAaPmF	pokusit
pouhou	pouhý	k2eAgFnSc7d1	pouhá
silou	síla	k1gFnSc7	síla
vůle	vůle	k1gFnSc2	vůle
roztočit	roztočit	k5eAaPmF	roztočit
mlýnek	mlýnek	k1gInSc4	mlýnek
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
než	než	k8xS	než
kterým	který	k3yQgInPc3	který
ho	on	k3xPp3gNnSc4	on
otáčí	otáčet	k5eAaImIp3nS	otáčet
světlo	světlo	k1gNnSc1	světlo
-	-	kIx~	-
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
způsoben	způsobit	k5eAaPmNgInS	způsobit
přenosem	přenos	k1gInSc7	přenos
tepla	teplo	k1gNnSc2	teplo
z	z	k7c2	z
ruky	ruka	k1gFnSc2	ruka
testované	testovaný	k2eAgFnSc2d1	testovaná
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Crookesův	Crookesův	k2eAgInSc4d1	Crookesův
mlýnek	mlýnek	k1gInSc4	mlýnek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
funguje	fungovat	k5eAaImIp3nS	fungovat
světelný	světelný	k2eAgInSc4d1	světelný
mlýnek	mlýnek	k1gInSc4	mlýnek
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
Radiometr	radiometr	k1gInSc1	radiometr
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
</s>
</p>
