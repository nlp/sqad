<p>
<s>
Mršník	mršník	k1gMnSc1	mršník
topolový	topolový	k2eAgMnSc1d1	topolový
(	(	kIx(	(
<g/>
Hololepta	Hololepta	k1gMnSc1	Hololepta
plana	plana	k?	plana
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
brouk	brouk	k1gMnSc1	brouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
mršníkovitých	mršníkovitý	k2eAgMnPc2d1	mršníkovitý
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
a	a	k8xC	a
pahorkatinách	pahorkatina	k1gFnPc6	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
dravci	dravec	k1gMnPc1	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
drobnými	drobný	k2eAgMnPc7d1	drobný
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
<g/>
,	,	kIx,	,
především	především	k9	především
larvami	larva	k1gFnPc7	larva
dvoukřídlých	dvoukřídlí	k1gMnPc2	dvoukřídlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HŮRKA	hůrka	k1gFnSc1	hůrka
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Brouci	brouk	k1gMnPc1	brouk
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Kabourek	Kabourka	k1gFnPc2	Kabourka
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
390	[number]	k4	390
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
864	[number]	k4	864
<g/>
-	-	kIx~	-
<g/>
4711	[number]	k4	4711
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
