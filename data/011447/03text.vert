<p>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1751	[number]	k4	1751
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgMnPc2d1	hlavní
autorů	autor	k1gMnPc2	autor
americké	americký	k2eAgFnSc2d1	americká
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
napsal	napsat	k5eAaBmAgInS	napsat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třetinu	třetina	k1gFnSc4	třetina
z	z	k7c2	z
85	[number]	k4	85
článků	článek	k1gInPc2	článek
zvaných	zvaný	k2eAgInPc2d1	zvaný
Listy	list	k1gInPc7	list
federalistů	federalista	k1gMnPc2	federalista
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgMnPc6	který
byla	být	k5eAaImAgFnS	být
obhajována	obhajován	k2eAgFnSc1d1	obhajována
ratifikace	ratifikace	k1gFnSc1	ratifikace
Ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
mnoho	mnoho	k4c4	mnoho
základních	základní	k2eAgInPc2d1	základní
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
prvních	první	k4xOgInPc2	první
deset	deset	k4xCc1	deset
dodatků	dodatek	k1gInPc2	dodatek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Nejzákladnější	základní	k2eAgNnSc1d3	nejzákladnější
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
Madisona	Madison	k1gMnSc2	Madison
jako	jako	k8xS	jako
politického	politický	k2eAgMnSc2d1	politický
teoretika	teoretik	k1gMnSc2	teoretik
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
republika	republika	k1gFnSc1	republika
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zabránit	zabránit	k5eAaPmF	zabránit
zvyšování	zvyšování	k1gNnPc4	zvyšování
vlivu	vliv	k1gInSc2	vliv
různých	různý	k2eAgFnPc2d1	různá
zájmových	zájmový	k2eAgFnPc2d1	zájmová
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Madison	Madisona	k1gFnPc2	Madisona
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Georgem	Georg	k1gMnSc7	Georg
Washingtonem	Washington	k1gInSc7	Washington
na	na	k7c6	na
organizování	organizování	k1gNnSc6	organizování
nové	nový	k2eAgFnSc2d1	nová
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roztržce	roztržka	k1gFnSc6	roztržka
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Hamiltonem	Hamilton	k1gInSc7	Hamilton
založil	založit	k5eAaPmAgMnS	založit
Madison	Madison	k1gInSc4	Madison
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
Jeffersonem	Jefferson	k1gInSc7	Jefferson
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
nazvali	nazvat	k5eAaPmAgMnP	nazvat
Republican	Republican	k1gInSc4	Republican
Party	parta	k1gFnSc2	parta
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Democratic-Republican	Democratic-Republican	k1gMnSc1	Democratic-Republican
Party	parta	k1gFnSc2	parta
<g/>
)	)	kIx)	)
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
vůči	vůči	k7c3	vůči
Federalistům	federalista	k1gMnPc3	federalista
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vůči	vůči	k7c3	vůči
národní	národní	k2eAgFnSc3d1	národní
bance	banka	k1gFnSc3	banka
a	a	k8xC	a
smlouvě	smlouva	k1gFnSc3	smlouva
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Jay	Jay	k1gFnSc1	Jay
Treaty	Treata	k1gFnSc2	Treata
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgInS	napsat
tajně	tajně	k6eAd1	tajně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Thomasem	Thomas	k1gMnSc7	Thomas
Jeffersonem	Jefferson	k1gMnSc7	Jefferson
Kentucky	Kentucka	k1gFnSc2	Kentucka
and	and	k?	and
Virginia	Virginium	k1gNnSc2	Virginium
Resolutions	Resolutionsa	k1gFnPc2	Resolutionsa
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
Zákonu	zákon	k1gInSc3	zákon
o	o	k7c6	o
cizincích	cizinec	k1gMnPc6	cizinec
a	a	k8xC	a
pomluvách	pomluva	k1gFnPc6	pomluva
(	(	kIx(	(
<g/>
Alien	Alina	k1gFnPc2	Alina
and	and	k?	and
Sedition	Sedition	k1gInSc1	Sedition
Acts	Acts	k1gInSc1	Acts
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
válku	válka	k1gFnSc4	válka
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gMnSc1	Madison
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
za	za	k7c2	za
jehož	jenž	k3xRgNnSc2	jenž
prezidentování	prezidentování	k1gNnSc2	prezidentování
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
dva	dva	k4xCgMnPc1	dva
viceprezidenti	viceprezident	k1gMnPc1	viceprezident
v	v	k7c6	v
úřadu	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c4	v
Port	port	k1gInSc4	port
Conway	Conwaa	k1gFnSc2	Conwaa
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
16	[number]	k4	16
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
1751	[number]	k4	1751
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
jako	jako	k9	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
devět	devět	k4xCc4	devět
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1723	[number]	k4	1723
<g/>
-	-	kIx~	-
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
tabákový	tabákový	k2eAgMnSc1d1	tabákový
plantážník	plantážník	k1gMnSc1	plantážník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Orange	Orang	k1gFnSc2	Orang
County	Counta	k1gFnSc2	Counta
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zdědil	zdědit	k5eAaPmAgInS	zdědit
po	po	k7c6	po
dovršení	dovršení	k1gNnSc6	dovršení
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
získal	získat	k5eAaPmAgMnS	získat
ještě	ještě	k9	ještě
větší	veliký	k2eAgInSc4d2	veliký
majetek	majetek	k1gInSc4	majetek
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
největším	veliký	k2eAgMnSc7d3	veliký
vlastníkem	vlastník	k1gMnSc7	vlastník
půdy	půda	k1gFnSc2	půda
(	(	kIx(	(
<g/>
5,000	[number]	k4	5,000
akrů	akr	k1gInPc2	akr
=	=	kIx~	=
cca	cca	kA	cca
20,2	[number]	k4	20,2
km2	km2	k4	km2
<g/>
)	)	kIx)	)
v	v	k7c6	v
Orange	Orange	k1gNnSc6	Orange
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Nelly	Nella	k1gMnSc2	Nella
Conway	Conwaa	k1gMnSc2	Conwaa
(	(	kIx(	(
<g/>
1731	[number]	k4	1731
<g/>
-	-	kIx~	-
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c4	v
Port	port	k1gInSc4	port
Conway	Conwaa	k1gFnSc2	Conwaa
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dcera	dcera	k1gFnSc1	dcera
významného	významný	k2eAgMnSc2d1	významný
plantážníka	plantážník	k1gMnSc2	plantážník
a	a	k8xC	a
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
tabákem	tabák	k1gInSc7	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Madisonovi	Madisonův	k2eAgMnPc1d1	Madisonův
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
vzali	vzít	k5eAaPmAgMnP	vzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1743	[number]	k4	1743
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
měli	mít	k5eAaImAgMnP	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
nejslavnější	slavný	k2eAgFnSc4d3	nejslavnější
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madison	Madison	k1gInSc1	Madison
měl	mít	k5eAaImAgInS	mít
tři	tři	k4xCgMnPc4	tři
bratry	bratr	k1gMnPc4	bratr
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
dožili	dožít	k5eAaPmAgMnP	dožít
dospělosti	dospělost	k1gFnSc3	dospělost
(	(	kIx(	(
<g/>
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
neteří	neteř	k1gFnPc2	neteř
a	a	k8xC	a
synovců	synovec	k1gMnPc2	synovec
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Francis	Francis	k1gFnSc1	Francis
Madison	Madisona	k1gFnPc2	Madisona
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
–	–	k?	–
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
plantážník	plantážník	k1gMnSc1	plantážník
v	v	k7c6	v
Orange	Orange	k1gFnSc6	Orange
County	Counta	k1gFnSc2	Counta
</s>
</p>
<p>
<s>
Ambrose	Ambrosa	k1gFnSc3	Ambrosa
Madison	Madison	k1gMnSc1	Madison
(	(	kIx(	(
<g/>
1755	[number]	k4	1755
<g/>
–	–	k?	–
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
plantážník	plantážník	k1gMnSc1	plantážník
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
Virginských	virginský	k2eAgFnPc2d1	virginská
milicí	milice	k1gFnPc2	milice
<g/>
,	,	kIx,	,
starající	starající	k2eAgFnPc1d1	starající
se	se	k3xPyFc4	se
o	o	k7c4	o
rodinné	rodinný	k2eAgInPc4d1	rodinný
zájmy	zájem	k1gInPc4	zájem
v	v	k7c6	v
Orange	Orange	k1gFnSc6	Orange
County	Counta	k1gFnSc2	Counta
Orange	Orang	k1gFnSc2	Orang
County	Counta	k1gFnSc2	Counta
<g/>
;	;	kIx,	;
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c4	po
dědečkovi	dědeček	k1gMnSc3	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
</s>
</p>
<p>
<s>
Catlett	Catlett	k2eAgInSc1d1	Catlett
Madison	Madison	k1gInSc1	Madison
(	(	kIx(	(
<g/>
1758	[number]	k4	1758
<g/>
–	–	k?	–
<g/>
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nelly	Nella	k1gFnPc1	Nella
Madison	Madisona	k1gFnPc2	Madisona
Hite	hit	k1gInSc5	hit
(	(	kIx(	(
<g/>
1760	[number]	k4	1760
<g/>
–	–	k?	–
<g/>
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Madison	Madison	k1gInSc1	Madison
(	(	kIx(	(
<g/>
1762	[number]	k4	1762
<g/>
–	–	k?	–
<g/>
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
veterán	veterán	k1gMnSc1	veterán
Revoluce	revoluce	k1gFnSc1	revoluce
a	a	k8xC	a
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
sloužil	sloužit	k5eAaImAgMnS	sloužit
ve	v	k7c6	v
Virginském	virginský	k2eAgInSc6d1	virginský
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
sboru	sbor	k1gInSc6	sbor
</s>
</p>
<p>
<s>
Sarah	Sarah	k1gFnSc1	Sarah
Catlett	Catlett	k1gMnSc1	Catlett
Madison	Madison	k1gMnSc1	Madison
Macon	Macon	k1gMnSc1	Macon
(	(	kIx(	(
<g/>
1764	[number]	k4	1764
<g/>
–	–	k?	–
<g/>
1843	[number]	k4	1843
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nepojmenované	pojmenovaný	k2eNgNnSc1d1	nepojmenované
dítě	dítě	k1gNnSc1	dítě
(	(	kIx(	(
<g/>
1766	[number]	k4	1766
<g/>
–	–	k?	–
<g/>
1766	[number]	k4	1766
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Madison	Madisona	k1gFnPc2	Madisona
(	(	kIx(	(
<g/>
1768	[number]	k4	1768
<g/>
–	–	k?	–
<g/>
1775	[number]	k4	1775
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nepojmenované	pojmenovaný	k2eNgNnSc1d1	nepojmenované
dítě	dítě	k1gNnSc1	dítě
(	(	kIx(	(
<g/>
1770	[number]	k4	1770
<g/>
–	–	k?	–
<g/>
1770	[number]	k4	1770
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Reuben	Reuben	k2eAgInSc1d1	Reuben
Madison	Madison	k1gInSc1	Madison
(	(	kIx(	(
<g/>
1771	[number]	k4	1771
<g/>
–	–	k?	–
<g/>
1775	[number]	k4	1775
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Frances	Frances	k1gInSc1	Frances
"	"	kIx"	"
<g/>
Fanny	Fanna	k1gFnSc2	Fanna
<g/>
"	"	kIx"	"
Madison	Madison	k1gMnSc1	Madison
Rose	Rose	k1gMnSc1	Rose
(	(	kIx(	(
<g/>
1774	[number]	k4	1774
<g/>
–	–	k?	–
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdělání	vzdělání	k1gNnSc1	vzdělání
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
věku	věk	k1gInSc2	věk
od	od	k7c2	od
11	[number]	k4	11
do	do	k7c2	do
16	[number]	k4	16
let	léto	k1gNnPc2	léto
mladý	mladý	k2eAgMnSc1d1	mladý
"	"	kIx"	"
<g/>
Jemmy	Jemm	k1gInPc1	Jemm
<g/>
"	"	kIx"	"
Madison	Madison	k1gMnSc1	Madison
studoval	studovat	k5eAaImAgMnS	studovat
pod	pod	k7c7	pod
Donaldem	Donald	k1gMnSc7	Donald
Robertsonem	Robertson	k1gMnSc7	Robertson
<g/>
,	,	kIx,	,
učitelem	učitel	k1gMnSc7	učitel
na	na	k7c6	na
plantáži	plantáž	k1gFnSc6	plantáž
Innes	Innesa	k1gFnPc2	Innesa
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
King	King	k1gMnSc1	King
and	and	k?	and
Queen	Queen	k1gInSc1	Queen
County	Counta	k1gFnSc2	Counta
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Robertsona	Robertson	k1gMnSc2	Robertson
se	se	k3xPyFc4	se
Madison	Madison	k1gMnSc1	Madison
naučil	naučit	k5eAaPmAgMnS	naučit
matematice	matematika	k1gFnSc3	matematika
<g/>
,	,	kIx,	,
geografii	geografie	k1gFnSc4	geografie
<g/>
,	,	kIx,	,
tradičních	tradiční	k2eAgInPc2d1	tradiční
a	a	k8xC	a
moderních	moderní	k2eAgInPc2d1	moderní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
dovedný	dovedný	k2eAgInSc1d1	dovedný
v	v	k7c6	v
latině	latina	k1gFnSc6	latina
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gMnSc1	Madison
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vděčí	vděčit	k5eAaImIp3nS	vděčit
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
talent	talent	k1gInSc4	talent
k	k	k7c3	k
učení	učení	k1gNnSc3	učení
"	"	kIx"	"
<g/>
především	především	k6eAd1	především
tomu	ten	k3xDgMnSc3	ten
muži	muž	k1gMnSc3	muž
(	(	kIx(	(
<g/>
Robertsonovi	Robertson	k1gMnSc3	Robertson
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgMnS	začít
dvouleté	dvouletý	k2eAgNnSc1d1	dvouleté
studium	studium	k1gNnSc1	studium
pod	pod	k7c7	pod
Reverendem	reverend	k1gMnSc7	reverend
Thomasem	Thomas	k1gMnSc7	Thomas
Martinem	Martin	k1gMnSc7	Martin
který	který	k3yRgMnSc1	který
Madisona	Madison	k1gMnSc4	Madison
připravoval	připravovat	k5eAaImAgMnS	připravovat
v	v	k7c6	v
Montpelieru	Montpelier	k1gInSc6	Montpelier
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1769	[number]	k4	1769
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c6	na
College	College	k1gNnSc6	College
of	of	k?	of
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
hodin	hodina	k1gFnPc2	hodina
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohly	moct	k5eAaImAgInP	moct
poškodit	poškodit	k5eAaPmF	poškodit
jeho	jeho	k3xOp3gNnSc4	jeho
křehké	křehký	k2eAgNnSc4d1	křehké
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
Madison	Madison	k1gMnSc1	Madison
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1771	[number]	k4	1771
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
latinu	latina	k1gFnSc4	latina
<g/>
,	,	kIx,	,
starořečtinu	starořečtina	k1gFnSc4	starořečtina
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
zeměpis	zeměpis	k1gInSc4	zeměpis
<g/>
,	,	kIx,	,
matematiku	matematika	k1gFnSc4	matematika
<g/>
,	,	kIx,	,
rétoriku	rétorika	k1gFnSc4	rétorika
a	a	k8xC	a
filosofii	filosofie	k1gFnSc4	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
řeč	řeč	k1gFnSc4	řeč
a	a	k8xC	a
diskusi	diskuse	k1gFnSc4	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studia	studio	k1gNnSc2	studio
Madison	Madisona	k1gFnPc2	Madisona
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c4	na
Princeton	Princeton	k1gInSc4	Princeton
studovat	studovat	k5eAaImF	studovat
hebrejštinu	hebrejština	k1gFnSc4	hebrejština
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
filosofii	filosofie	k1gFnSc4	filosofie
pod	pod	k7c7	pod
univerzitním	univerzitní	k2eAgMnSc7d1	univerzitní
prezidentem	prezident	k1gMnSc7	prezident
Johnem	John	k1gMnSc7	John
Witherspoonem	Witherspoon	k1gMnSc7	Witherspoon
před	před	k7c7	před
návratem	návrat	k1gInSc7	návrat
do	do	k7c2	do
Montpelier	Montpelira	k1gFnPc2	Montpelira
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželství	manželství	k1gNnSc1	manželství
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Dolley	Dolle	k1gMnPc7	Dolle
Madisonovou	Madisonová	k1gFnSc7	Madisonová
<g/>
,	,	kIx,	,
vdovou	vdova	k1gFnSc7	vdova
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
synem	syn	k1gMnSc7	syn
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1794	[number]	k4	1794
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
okres	okres	k1gInSc1	okres
Jefferson	Jeffersona	k1gFnPc2	Jeffersona
County	Counta	k1gFnSc2	Counta
<g/>
.	.	kIx.	.
</s>
<s>
Dolley	Dollea	k1gFnPc1	Dollea
Payne	Payn	k1gInSc5	Payn
Todd	Todd	k1gMnSc1	Todd
Madison	Madisona	k1gFnPc2	Madisona
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1768	[number]	k4	1768
v	v	k7c6	v
osadě	osada	k1gFnSc6	osada
New	New	k1gMnSc1	New
Garden	Gardna	k1gFnPc2	Gardna
Quaker	Quaker	k1gMnSc1	Quaker
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gMnPc4	její
rodiče	rodič	k1gMnPc4	rodič
John	John	k1gMnSc1	John
Payne	Payn	k1gInSc5	Payn
a	a	k8xC	a
Mary	Mary	k1gFnSc1	Mary
Coles	Colesa	k1gFnPc2	Colesa
Payne	Payn	k1gInSc5	Payn
krátce	krátce	k6eAd1	krátce
žili	žít	k5eAaImAgMnP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
Dolley	Dollea	k1gFnSc2	Dollea
Lucy	Luca	k1gFnSc2	Luca
Payne	Payn	k1gInSc5	Payn
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
Steptoe	Steptoe	k1gFnSc4	Steptoe
George	Georg	k1gMnSc4	Georg
Washingtona	Washington	k1gMnSc4	Washington
<g/>
,	,	kIx,	,
synovce	synovec	k1gMnSc4	synovec
prezidenta	prezident	k1gMnSc4	prezident
Washingtona	Washington	k1gMnSc4	Washington
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
člen	člen	k1gInSc1	člen
Kongresu	kongres	k1gInSc2	kongres
se	se	k3xPyFc4	se
Madison	Madison	k1gMnSc1	Madison
bezpochyby	bezpochyby	k6eAd1	bezpochyby
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
Dolley	Dollea	k1gFnSc2	Dollea
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1794	[number]	k4	1794
si	se	k3xPyFc3	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gMnSc4	jejich
společného	společný	k2eAgMnSc4d1	společný
přítele	přítel	k1gMnSc4	přítel
Aarona	Aaron	k1gMnSc4	Aaron
Burra	Burr	k1gMnSc4	Burr
sjednal	sjednat	k5eAaPmAgMnS	sjednat
schůzku	schůzka	k1gFnSc4	schůzka
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
zřejmě	zřejmě	k6eAd1	zřejmě
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
hladce	hladko	k6eAd1	hladko
a	a	k8xC	a
a	a	k8xC	a
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
přijala	přijmout	k5eAaPmAgFnS	přijmout
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madison	Madison	k1gMnSc1	Madison
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgFnPc4	žádný
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgMnPc4	žádný
přímé	přímý	k2eAgMnPc4d1	přímý
potomky	potomek	k1gMnPc4	potomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Raná	raný	k2eAgFnSc1d1	raná
politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
právník	právník	k1gMnSc1	právník
Madison	Madison	k1gMnSc1	Madison
obhajoval	obhajovat	k5eAaImAgMnS	obhajovat
Baptistické	baptistický	k2eAgMnPc4d1	baptistický
kazatele	kazatel	k1gMnPc4	kazatel
zatčené	zatčený	k2eAgMnPc4d1	zatčený
za	za	k7c4	za
kázání	kázání	k1gNnSc4	kázání
bez	bez	k7c2	bez
licence	licence	k1gFnSc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgMnSc4	ten
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
kazatelem	kazatel	k1gMnSc7	kazatel
Eliahem	Eliah	k1gMnSc7	Eliah
Craigem	Craig	k1gMnSc7	Craig
na	na	k7c6	na
ústavních	ústavní	k2eAgFnPc6d1	ústavní
zárukách	záruka	k1gFnPc6	záruka
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnPc1	práce
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
pomohla	pomoct	k5eAaPmAgFnS	pomoct
zformovat	zformovat	k5eAaPmF	zformovat
jeho	jeho	k3xOp3gInPc4	jeho
nápady	nápad	k1gInPc4	nápad
o	o	k7c6	o
náboženské	náboženský	k2eAgFnSc6d1	náboženská
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gMnSc1	Madison
sloužil	sloužit	k5eAaImAgMnS	sloužit
v	v	k7c6	v
zákonodárném	zákonodárný	k2eAgInSc6d1	zákonodárný
sboru	sbor	k1gInSc6	sbor
státu	stát	k1gInSc2	stát
Virginie	Virginie	k1gFnSc2	Virginie
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
)	)	kIx)	)
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
známým	známý	k2eAgMnSc7d1	známý
chráněncem	chráněnec	k1gMnSc7	chráněnec
Thomase	Thomas	k1gMnSc2	Thomas
Jeffersona	Jefferson	k1gMnSc2	Jefferson
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
výtečného	výtečný	k2eAgNnSc2d1	výtečné
postavení	postavení	k1gNnSc2	postavení
ve	v	k7c6	v
virginské	virginský	k2eAgFnSc6d1	virginská
politice	politika	k1gFnSc6	politika
pomocí	pomoc	k1gFnPc2	pomoc
při	při	k7c6	při
návrhu	návrh	k1gInSc6	návrh
Virginského	virginský	k2eAgNnSc2d1	virginské
ustanovení	ustanovení	k1gNnSc2	ustanovení
o	o	k7c6	o
svobodě	svoboda	k1gFnSc6	svoboda
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odlučuje	odlučovat	k5eAaImIp3nS	odlučovat
Anglikánskou	anglikánský	k2eAgFnSc4d1	anglikánská
církev	církev	k1gFnSc4	církev
od	od	k7c2	od
státu	stát	k1gInSc2	stát
a	a	k8xC	a
popírá	popírat	k5eAaImIp3nS	popírat
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
pravomoc	pravomoc	k1gFnSc4	pravomoc
státního	státní	k2eAgInSc2d1	státní
nátlaku	nátlak	k1gInSc2	nátlak
v	v	k7c6	v
náboženské	náboženský	k2eAgFnSc6d1	náboženská
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Zabránil	zabránit	k5eAaPmAgMnS	zabránit
plánu	plán	k1gInSc2	plán
guvernéra	guvernér	k1gMnSc2	guvernér
Virginie	Virginie	k1gFnSc2	Virginie
Patricka	Patricka	k1gFnSc1	Patricka
Henryho	Henry	k1gMnSc2	Henry
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
nutit	nutit	k5eAaImF	nutit
občany	občan	k1gMnPc4	občan
k	k	k7c3	k
placení	placení	k1gNnSc3	placení
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
svobodné	svobodný	k2eAgNnSc4d1	svobodné
shromáždění	shromáždění	k1gNnSc4	shromáždění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madisonův	Madisonův	k2eAgMnSc1d1	Madisonův
bratranec	bratranec	k1gMnSc1	bratranec
Reverend	reverend	k1gMnSc1	reverend
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
-	-	kIx~	-
<g/>
1812	[number]	k4	1812
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
univerzity	univerzita	k1gFnSc2	univerzita
The	The	k1gMnSc7	The
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
William	William	k1gInSc1	William
&	&	k?	&
Mary	Mary	k1gFnSc1	Mary
<g/>
.	.	kIx.	.
</s>
<s>
Úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Madisonem	Madison	k1gMnSc7	Madison
a	a	k8xC	a
Jeffersonem	Jefferson	k1gMnSc7	Jefferson
<g/>
,	,	kIx,	,
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
provést	provést	k5eAaPmF	provést
univerzitu	univerzita	k1gFnSc4	univerzita
složitými	složitý	k2eAgNnPc7d1	složité
obdobími	období	k1gNnPc7	období
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
oddělení	oddělení	k1gNnSc2	oddělení
jak	jak	k6eAd1	jak
od	od	k7c2	od
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
od	od	k7c2	od
Anglikánské	anglikánský	k2eAgFnSc2d1	anglikánská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
také	také	k9	také
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
státní	státní	k2eAgNnSc4d1	státní
opatření	opatření	k1gNnSc4	opatření
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyústily	vyústit	k5eAaPmAgFnP	vyústit
ve	v	k7c6	v
vytvoření	vytvoření	k1gNnSc6	vytvoření
Episkopální	episkopální	k2eAgFnSc2d1	episkopální
diecéze	diecéze	k1gFnSc2	diecéze
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
Virginii	Virginie	k1gFnSc4	Virginie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzdala	vzdát	k5eAaPmAgFnS	vzdát
svých	svůj	k3xOyFgMnPc2	svůj
nároků	nárok	k1gInPc2	nárok
na	na	k7c6	na
území	území	k1gNnSc6	území
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
<g/>
:	:	kIx,	:
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
,	,	kIx,	,
Illinois	Illinois	k1gFnSc1	Illinois
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
Wisconsin	Wisconsin	k1gInSc1	Wisconsin
a	a	k8xC	a
část	část	k1gFnSc1	část
Minnesoty	Minnesota	k1gFnSc2	Minnesota
Kontinentálnímu	kontinentální	k2eAgInSc3d1	kontinentální
Kongresu	kongres	k1gInSc3	kongres
chybí	chybět	k5eAaImIp3nS	chybět
sloveso	sloveso	k1gNnSc4	sloveso
Severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
Teritorium	teritorium	k1gNnSc1	teritorium
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Otec	otec	k1gMnSc1	otec
Ústavy	ústava	k1gFnSc2	ústava
==	==	k?	==
</s>
</p>
<p>
<s>
Madison	Madison	k1gMnSc1	Madison
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
virginského	virginský	k2eAgInSc2d1	virginský
zákonodárného	zákonodárný	k2eAgInSc2d1	zákonodárný
sboru	sbor	k1gInSc2	sbor
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
rostlo	růst	k5eAaImAgNnS	růst
jeho	jeho	k3xOp3gFnSc4	jeho
znepokojení	znepokojení	k1gNnSc1	znepokojení
o	o	k7c6	o
křehkosti	křehkost	k1gFnSc6	křehkost
Článků	článek	k1gInPc2	článek
Konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
rozdělením	rozdělení	k1gNnSc7	rozdělení
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
silně	silně	k6eAd1	silně
podporoval	podporovat	k5eAaImAgMnS	podporovat
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Filadelfském	filadelfský	k2eAgNnSc6d1	filadelfské
Shromáždění	shromáždění	k1gNnSc6	shromáždění
v	v	k7c4	v
1787	[number]	k4	1787
Madisonův	Madisonův	k2eAgInSc4d1	Madisonův
návrh	návrh	k1gInSc4	návrh
Virginského	virginský	k2eAgInSc2d1	virginský
plánu	plán	k1gInSc2	plán
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
revoluční	revoluční	k2eAgInSc1d1	revoluční
třísložkový	třísložkový	k2eAgInSc1d1	třísložkový
federální	federální	k2eAgInSc1d1	federální
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
základem	základ	k1gInSc7	základ
pro	pro	k7c4	pro
dnešní	dnešní	k2eAgFnSc4d1	dnešní
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Madison	Madison	k1gMnSc1	Madison
byl	být	k5eAaImAgMnS	být
plachý	plachý	k2eAgMnSc1d1	plachý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
přímočarých	přímočarý	k2eAgInPc2d1	přímočarý
členů	člen	k1gInPc2	člen
Kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
Kongresu	kongres	k1gInSc2	kongres
<g/>
.	.	kIx.	.
</s>
<s>
Představoval	představovat	k5eAaImAgMnS	představovat
si	se	k3xPyFc3	se
silnou	silný	k2eAgFnSc4d1	silná
federální	federální	k2eAgFnSc4d1	federální
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
zvrátit	zvrátit	k5eAaPmF	zvrátit
kroky	krok	k1gInPc4	krok
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
jako	jako	k9	jako
chybné	chybný	k2eAgNnSc4d1	chybné
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
životě	život	k1gInSc6	život
obdivoval	obdivovat	k5eAaImAgInS	obdivovat
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
plnění	plnění	k1gNnSc4	plnění
této	tento	k3xDgFnSc2	tento
úlohy	úloha	k1gFnSc2	úloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Listy	lista	k1gFnPc1	lista
Federalistů	federalista	k1gMnPc2	federalista
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
ratifikace	ratifikace	k1gFnSc2	ratifikace
Ústavy	ústava	k1gFnSc2	ústava
se	se	k3xPyFc4	se
Madison	Madison	k1gMnSc1	Madison
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Hamiltonem	Hamilton	k1gInSc7	Hamilton
a	a	k8xC	a
Johnem	John	k1gMnSc7	John
Jayem	Jay	k1gMnSc7	Jay
pro	pro	k7c4	pro
napsání	napsání	k1gNnSc4	napsání
Listů	list	k1gInPc2	list
Federalistů	federalista	k1gMnPc2	federalista
v	v	k7c6	v
letech	let	k1gInPc6	let
1787	[number]	k4	1787
a	a	k8xC	a
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgInPc7d1	jiný
příspěvky	příspěvek	k1gInPc7	příspěvek
Madison	Madisona	k1gFnPc2	Madisona
napsal	napsat	k5eAaBmAgMnS	napsat
desátou	desátá	k1gFnSc4	desátá
část	část	k1gFnSc4	část
Listů	list	k1gInPc2	list
Federalistů	federalista	k1gMnPc2	federalista
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
velká	velký	k2eAgFnSc1d1	velká
země	země	k1gFnSc1	země
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
různými	různý	k2eAgInPc7d1	různý
zájmy	zájem	k1gInPc7	zájem
by	by	kYmCp3nP	by
mohla	moct	k5eAaImAgNnP	moct
podpořit	podpořit	k5eAaPmF	podpořit
republikánské	republikánský	k2eAgFnPc4d1	republikánská
hodnoty	hodnota	k1gFnPc4	hodnota
lépe	dobře	k6eAd2	dobře
<g/>
,	,	kIx,	,
než	než	k8xS	než
malá	malý	k2eAgFnSc1d1	malá
země	země	k1gFnSc1	země
ovládaná	ovládaný	k2eAgFnSc1d1	ovládaná
několika	několik	k4yIc7	několik
speciálními	speciální	k2eAgInPc7d1	speciální
zájmy	zájem	k1gInPc7	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výklad	výklad	k1gInSc1	výklad
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
ignorován	ignorován	k2eAgMnSc1d1	ignorován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
ústřední	ústřední	k2eAgFnSc7d1	ústřední
součástí	součást	k1gFnSc7	součást
pluralitního	pluralitní	k2eAgInSc2d1	pluralitní
výkladu	výklad	k1gInSc2	výklad
americké	americký	k2eAgFnSc2d1	americká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
na	na	k7c6	na
shromáždění	shromáždění	k1gNnSc6	shromáždění
168	[number]	k4	168
delegátů	delegát	k1gMnPc2	delegát
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
(	(	kIx(	(
<g/>
Virginia	Virginium	k1gNnPc1	Virginium
Ratifying	Ratifying	k1gInSc4	Ratifying
Convention	Convention	k1gInSc1	Convention
<g/>
)	)	kIx)	)
bojoval	bojovat	k5eAaImAgInS	bojovat
za	za	k7c4	za
ratifikaci	ratifikace	k1gFnSc4	ratifikace
Ústavy	ústava	k1gFnSc2	ústava
při	při	k7c6	při
debatách	debata	k1gFnPc6	debata
s	s	k7c7	s
Patrickem	Patrick	k1gInSc7	Patrick
Henrym	Henry	k1gMnSc7	Henry
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
změny	změna	k1gFnPc4	změna
před	před	k7c7	před
ratifikací	ratifikace	k1gFnSc7	ratifikace
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gInSc1	Madison
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
a	a	k8xC	a
ratifikaci	ratifikace	k1gFnSc6	ratifikace
často	často	k6eAd1	často
nazýván	nazývat	k5eAaImNgMnS	nazývat
otcem	otec	k1gMnSc7	otec
ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
sám	sám	k3xTgMnSc1	sám
to	ten	k3xDgNnSc4	ten
odmítal	odmítat	k5eAaImAgMnS	odmítat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
nemám	mít	k5eNaImIp1nS	mít
nárok	nárok	k1gInSc1	nárok
<g/>
...	...	k?	...
Ústava	ústava	k1gFnSc1	ústava
nebyla	být	k5eNaImAgFnS	být
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bájné	bájný	k2eAgFnSc2d1	bájná
bohyně	bohyně	k1gFnSc2	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
<g/>
,	,	kIx,	,
dítětem	dítě	k1gNnSc7	dítě
jednoho	jeden	k4xCgInSc2	jeden
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
být	být	k5eAaImF	být
pohlíženo	pohlížen	k2eAgNnSc1d1	pohlíženo
jako	jako	k9	jako
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
mnoha	mnoho	k4c2	mnoho
hlav	hlava	k1gFnPc2	hlava
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Napsal	napsat	k5eAaBmAgMnS	napsat
Hamiltonovi	Hamilton	k1gMnSc3	Hamilton
na	na	k7c6	na
Newyorském	newyorský	k2eAgNnSc6d1	newyorské
ratifikačním	ratifikační	k2eAgNnSc6d1	ratifikační
shromáždění	shromáždění	k1gNnSc6	shromáždění
<g/>
,	,	kIx,	,
uvádějící	uvádějící	k2eAgFnPc1d1	uvádějící
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
ratifikace	ratifikace	k1gFnSc1	ratifikace
byla	být	k5eAaImAgFnS	být
in	in	k?	in
toto	tento	k3xDgNnSc1	tento
and	and	k?	and
for	forum	k1gNnPc2	forum
ever	ever	k1gInSc1	ever
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vcelku	vcelku	k6eAd1	vcelku
a	a	k8xC	a
navždy	navždy	k6eAd1	navždy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Virgiňské	Virgiňský	k2eAgNnSc1d1	Virgiňský
ratifikační	ratifikační	k2eAgNnSc1d1	ratifikační
shromáždění	shromáždění	k1gNnSc1	shromáždění
považovalo	považovat	k5eAaImAgNnS	považovat
podmíněnou	podmíněný	k2eAgFnSc4d1	podmíněná
ratifikaci	ratifikace	k1gFnSc4	ratifikace
horší	zlý	k2eAgMnSc1d2	horší
než	než	k8xS	než
zamítnutí	zamítnutí	k1gNnSc1	zamítnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Autor	autor	k1gMnSc1	autor
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
==	==	k?	==
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
Madison	Madison	k1gNnSc1	Madison
"	"	kIx"	"
<g/>
neústupně	ústupně	k6eNd1	ústupně
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
...	...	k?	...
že	že	k8xS	že
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Ústava	ústava	k1gFnSc1	ústava
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
je	být	k5eAaImIp3nS	být
listinou	listina	k1gFnSc7	listina
práv	práv	k2eAgInSc4d1	práv
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Madison	Madison	k1gInSc1	Madison
měl	mít	k5eAaImAgInS	mít
proti	proti	k7c3	proti
Listině	listina	k1gFnSc3	listina
práv	právo	k1gNnPc2	právo
tři	tři	k4xCgFnPc4	tři
hlavní	hlavní	k2eAgFnPc4d1	hlavní
námitky	námitka	k1gFnPc4	námitka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nebyla	být	k5eNaImAgFnS	být
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
proti	proti	k7c3	proti
silám	síla	k1gFnPc3	síla
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgInPc3	který
nebyla	být	k5eNaImAgFnS	být
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byla	být	k5eAaImAgFnS	být
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výčet	výčet	k1gInSc4	výčet
některých	některý	k3yIgNnPc2	některý
práv	právo	k1gNnPc2	právo
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
chápán	chápat	k5eAaImNgInS	chápat
jako	jako	k8xC	jako
absence	absence	k1gFnSc1	absence
jiných	jiný	k2eAgNnPc2d1	jiné
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
státní	státní	k2eAgFnSc6d1	státní
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
Listina	listina	k1gFnSc1	listina
práv	právo	k1gNnPc2	právo
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
nepoužitelná	použitelný	k2eNgFnSc1d1	nepoužitelná
"	"	kIx"	"
<g/>
papírová	papírový	k2eAgFnSc1d1	papírová
překážka	překážka	k1gFnSc1	překážka
<g/>
"	"	kIx"	"
proti	proti	k7c3	proti
vládní	vládní	k2eAgFnSc3d1	vládní
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
<g/>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
anti-Federalisté	anti-Federalista	k1gMnPc1	anti-Federalista
nabízeli	nabízet	k5eAaImAgMnP	nabízet
svou	svůj	k3xOyFgFnSc4	svůj
podporu	podpora	k1gFnSc4	podpora
Listiny	listina	k1gFnSc2	listina
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
Federalistů	federalista	k1gMnPc2	federalista
při	při	k7c6	při
ratifikaci	ratifikace	k1gFnSc6	ratifikace
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patrick	Patrick	k1gMnSc1	Patrick
Henry	Henry	k1gMnSc1	Henry
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
virginskou	virginský	k2eAgFnSc4d1	virginská
legislaturu	legislatura	k1gFnSc4	legislatura
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nezvolila	zvolit	k5eNaPmAgFnS	zvolit
Madisona	Madisona	k1gFnSc1	Madisona
jako	jako	k8xC	jako
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
jejích	její	k3xOp3gMnPc2	její
prvních	první	k4xOgMnPc2	první
Senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Madison	Madison	k1gMnSc1	Madison
byl	být	k5eAaImAgMnS	být
přímo	přímo	k6eAd1	přímo
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
důležitým	důležitý	k2eAgMnSc7d1	důležitý
předsedou	předseda	k1gMnSc7	předseda
od	od	k7c2	od
Prvního	první	k4xOgInSc2	první
Kongresu	kongres	k1gInSc2	kongres
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
po	po	k7c4	po
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
Kongres	kongres	k1gInSc4	kongres
(	(	kIx(	(
<g/>
1797	[number]	k4	1797
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Opozice	opozice	k1gFnSc1	opozice
k	k	k7c3	k
Hamiltonovi	Hamilton	k1gMnSc3	Hamilton
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
charakteristika	charakteristika	k1gFnSc1	charakteristika
Madisonova	Madisonův	k2eAgNnSc2d1	Madisonův
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Kongresu	kongres	k1gInSc6	kongres
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
síly	síla	k1gFnSc2	síla
federální	federální	k2eAgFnSc2d1	federální
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Gordon	Gordon	k1gMnSc1	Gordon
S.	S.	kA	S.
Wood	Wood	k1gMnSc1	Wood
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Madison	Madison	k1gMnSc1	Madison
nikdy	nikdy	k6eAd1	nikdy
nechtěl	chtít	k5eNaImAgMnS	chtít
národní	národní	k2eAgFnSc4d1	národní
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
aktivní	aktivní	k2eAgFnSc4d1	aktivní
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zděšený	zděšený	k2eAgInSc1d1	zděšený
objevením	objevení	k1gNnSc7	objevení
<g/>
,	,	kIx,	,
že	že	k8xS	že
Alexander	Alexandra	k1gFnPc2	Alexandra
Hamilton	Hamilton	k1gInSc4	Hamilton
a	a	k8xC	a
George	Georg	k1gMnSc4	Georg
Washington	Washington	k1gInSc4	Washington
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgInSc1d1	moderní
evropský	evropský	k2eAgInSc1d1	evropský
typ	typ	k1gInSc1	typ
vlády	vláda	k1gFnSc2	vláda
s	s	k7c7	s
byrokracií	byrokracie	k1gFnSc7	byrokracie
<g/>
,	,	kIx,	,
stálým	stálý	k2eAgNnSc7d1	stálé
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
silnou	silný	k2eAgFnSc7d1	silná
nezávislou	závislý	k2eNgFnSc7d1	nezávislá
výkonnou	výkonný	k2eAgFnSc7d1	výkonná
mocí	moc	k1gFnSc7	moc
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
vešly	vejít	k5eAaPmAgFnP	vejít
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
do	do	k7c2	do
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1793	[number]	k4	1793
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
stály	stát	k5eAaImAgInP	stát
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
stále	stále	k6eAd1	stále
platila	platit	k5eAaImAgFnS	platit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nejvíc	nejvíc	k6eAd1	nejvíc
obchodu	obchod	k1gInSc2	obchod
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Británií	Británie	k1gFnSc7	Británie
se	se	k3xPyFc4	se
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
jako	jako	k9	jako
bezprostředně	bezprostředně	k6eAd1	bezprostředně
hrozící	hrozící	k2eAgNnSc1d1	hrozící
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Britové	Brit	k1gMnPc1	Brit
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
stovek	stovka	k1gFnPc2	stovka
amerických	americký	k2eAgFnPc2d1	americká
lodí	loď	k1gFnPc2	loď
obchodujících	obchodující	k2eAgFnPc2d1	obchodující
s	s	k7c7	s
francouzskými	francouzský	k2eAgFnPc7d1	francouzská
koloniemi	kolonie	k1gFnPc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Madison	Madison	k1gNnSc1	Madison
(	(	kIx(	(
<g/>
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Jeffersonem	Jefferson	k1gInSc7	Jefferson
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dočasně	dočasně	k6eAd1	dočasně
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
soukromého	soukromý	k2eAgInSc2d1	soukromý
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
byla	být	k5eAaImAgFnS	být
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
Amerika	Amerika	k1gFnSc1	Amerika
byla	být	k5eAaImAgFnS	být
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
tak	tak	k6eAd1	tak
obchodní	obchodní	k2eAgFnSc1d1	obchodní
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
riskovali	riskovat	k5eAaBmAgMnP	riskovat
odvetu	odveta	k1gFnSc4	odveta
Britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
Američanům	Američan	k1gMnPc3	Američan
umožnit	umožnit	k5eAaPmF	umožnit
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
plně	plně	k6eAd1	plně
jejich	jejich	k3xOp3gFnSc4	jejich
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nás	my	k3xPp1nPc2	my
vázala	vázat	k5eAaImAgFnS	vázat
v	v	k7c6	v
komerčních	komerční	k2eAgNnPc6d1	komerční
poutech	pouto	k1gNnPc6	pouto
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
zblízka	zblízka	k6eAd1	zblízka
mařila	mařit	k5eAaImAgFnS	mařit
objekty	objekt	k1gInPc4	objekt
naší	náš	k3xOp1gFnSc2	náš
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Madisonova	Madisonův	k2eAgFnSc1d1	Madisonova
vláda	vláda	k1gFnSc1	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GAY	gay	k1gMnSc1	gay
<g/>
,	,	kIx,	,
Sydney	Sydney	k1gNnSc1	Sydney
Howard	Howarda	k1gFnPc2	Howarda
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
.	.	kIx.	.
</s>
<s>
Boston	Boston	k1gInSc1	Boston
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
;	;	kIx,	;
Houghton	Houghton	k1gInSc1	Houghton
<g/>
:	:	kIx,	:
Mifflin	Mifflin	k1gInSc1	Mifflin
and	and	k?	and
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HEIDEKING	HEIDEKING	kA	HEIDEKING
<g/>
,	,	kIx,	,
Jürgen	Jürgen	k1gInSc1	Jürgen
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1	americký
prezidenti	prezident	k1gMnPc1	prezident
:	:	kIx,	:
41	[number]	k4	41
portrétů	portrét	k1gInPc2	portrét
od	od	k7c2	od
Georga	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
po	po	k7c4	po
Billa	Bill	k1gMnSc4	Bill
Clintona	Clinton	k1gMnSc4	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7260	[number]	k4	7260
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KETCHAM	KETCHAM	kA	KETCHAM
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Louis	Louis	k1gMnSc1	Louis
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
:	:	kIx,	:
a	a	k8xC	a
biography	biographa	k1gFnSc2	biographa
<g/>
.	.	kIx.	.
</s>
<s>
Charlottesville	Charlottesville	k1gInSc1	Charlottesville
<g/>
:	:	kIx,	:
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc1	Press
of	of	k?	of
Virginia	Virginium	k1gNnSc2	Virginium
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8139	[number]	k4	8139
<g/>
-	-	kIx~	-
<g/>
1265	[number]	k4	1265
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MAREŠ	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Prezidenti	prezident	k1gMnPc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Práh	práh	k1gInSc1	práh
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85809	[number]	k4	85809
<g/>
-	-	kIx~	-
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MCCOY	MCCOY	kA	MCCOY
<g/>
,	,	kIx,	,
Drew	Drew	k1gMnSc1	Drew
R.	R.	kA	R.
The	The	k1gMnSc1	The
last	last	k1gMnSc1	last
of	of	k?	of
the	the	k?	the
fathers	fathers	k1gInSc1	fathers
:	:	kIx,	:
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
and	and	k?	and
the	the	k?	the
Republican	Republican	k1gInSc4	Republican
legacy	legaca	k1gFnSc2	legaca
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
36407	[number]	k4	36407
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RUTLAND	RUTLAND	kA	RUTLAND
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Allen	Allen	k1gMnSc1	Allen
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
:	:	kIx,	:
the	the	k?	the
founding	founding	k1gInSc1	founding
father	fathra	k1gFnPc2	fathra
<g/>
.	.	kIx.	.
</s>
<s>
Columbia	Columbia	k1gFnSc1	Columbia
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
Missouri	Missouri	k1gFnSc2	Missouri
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8262	[number]	k4	8262
<g/>
-	-	kIx~	-
<g/>
1141	[number]	k4	1141
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SCHÄFER	SCHÄFER	kA	SCHÄFER
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Prezidenti	prezident	k1gMnPc1	prezident
USA	USA	kA	USA
:	:	kIx,	:
Od	od	k7c2	od
Georga	Georg	k1gMnSc2	Georg
Washingtona	Washington	k1gMnSc2	Washington
po	po	k7c4	po
Billa	Bill	k1gMnSc4	Bill
Clintona	Clinton	k1gMnSc4	Clinton
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
499	[number]	k4	499
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TINDALL	TINDALL	kA	TINDALL
<g/>
,	,	kIx,	,
George	George	k1gFnSc1	George
Brown	Brown	k1gMnSc1	Brown
<g/>
;	;	kIx,	;
SHI	SHI	kA	SHI
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
E.	E.	kA	E.
Dějiny	dějiny	k1gFnPc1	dějiny
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
302	[number]	k4	302
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
James	Jamesa	k1gFnPc2	Jamesa
Madison	Madisona	k1gFnPc2	Madisona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
James	Jamesa	k1gFnPc2	Jamesa
Madison	Madison	k1gNnSc4	Madison
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
at	at	k?	at
MetaLibri	MetaLibr	k1gFnSc2	MetaLibr
</s>
</p>
