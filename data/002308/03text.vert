<s>
V	v	k7c6	v
právu	právo	k1gNnSc6	právo
se	se	k3xPyFc4	se
vraždou	vražda	k1gFnSc7	vražda
rozumí	rozumět	k5eAaImIp3nS	rozumět
zločin	zločin	k1gMnSc1	zločin
spáchaný	spáchaný	k2eAgMnSc1d1	spáchaný
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c6	na
jiné	jiný	k2eAgFnSc6d1	jiná
lidské	lidský	k2eAgFnSc6d1	lidská
bytosti	bytost	k1gFnSc6	bytost
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
způsobí	způsobit	k5eAaPmIp3nS	způsobit
smrt	smrt	k1gFnSc4	smrt
bez	bez	k7c2	bez
právního	právní	k2eAgNnSc2d1	právní
ospravedlnění	ospravedlnění	k1gNnSc2	ospravedlnění
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
bez	bez	k7c2	bez
morálního	morální	k2eAgNnSc2d1	morální
ospravedlnění	ospravedlnění	k1gNnSc2	ospravedlnění
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vykonán	vykonat	k5eAaPmNgInS	vykonat
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
zemí	zem	k1gFnSc7	zem
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejzávažnější	závažný	k2eAgInSc4d3	nejzávažnější
zločin	zločin	k1gInSc4	zločin
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
trestán	trestat	k5eAaImNgInS	trestat
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
dostupnými	dostupný	k2eAgInPc7d1	dostupný
tresty	trest	k1gInPc7	trest
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vraždí	vraždit	k5eAaImIp3nS	vraždit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vrah	vrah	k1gMnSc1	vrah
<g/>
,	,	kIx,	,
zavražděný	zavražděný	k2eAgMnSc1d1	zavražděný
pak	pak	k6eAd1	pak
oběť	oběť	k1gFnSc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
provedená	provedený	k2eAgFnSc1d1	provedená
v	v	k7c6	v
afektu	afekt	k1gInSc6	afekt
anebo	anebo	k8xC	anebo
naplánovaná	naplánovaný	k2eAgFnSc1d1	naplánovaná
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
chování	chování	k1gNnSc2	chování
vrahů	vrah	k1gMnPc2	vrah
<g/>
,	,	kIx,	,
motivů	motiv	k1gInPc2	motiv
k	k	k7c3	k
vraždám	vražda	k1gFnPc3	vražda
vedoucím	vedoucí	k2eAgFnPc3d1	vedoucí
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
pojmy	pojem	k1gInPc7	pojem
s	s	k7c7	s
vraždou	vražda	k1gFnSc7	vražda
spojenými	spojený	k2eAgMnPc7d1	spojený
se	se	k3xPyFc4	se
z	z	k7c2	z
psychologického	psychologický	k2eAgNnSc2d1	psychologické
hlediska	hledisko	k1gNnSc2	hledisko
zabývá	zabývat	k5eAaImIp3nS	zabývat
forenzní	forenzní	k2eAgFnSc1d1	forenzní
(	(	kIx(	(
<g/>
soudní	soudní	k2eAgFnSc1d1	soudní
<g/>
)	)	kIx)	)
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
(	(	kIx(	(
<g/>
římském	římský	k2eAgNnSc6d1	římské
<g/>
,	,	kIx,	,
kontinentálním	kontinentální	k2eAgNnSc6d1	kontinentální
<g/>
)	)	kIx)	)
právu	právo	k1gNnSc6	právo
je	být	k5eAaImIp3nS	být
vražda	vražda	k1gFnSc1	vražda
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xC	jako
protizákonné	protizákonný	k2eAgNnSc1d1	protizákonné
zabití	zabití	k1gNnSc1	zabití
jiného	jiný	k2eAgMnSc2d1	jiný
člověka	člověk	k1gMnSc2	člověk
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
mysli	mysl	k1gFnSc2	mysl
známým	známý	k2eAgInPc3d1	známý
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zlý	zlý	k2eAgInSc1d1	zlý
úmysl	úmysl	k1gInSc1	úmysl
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
elementy	element	k1gInPc4	element
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
přímočaré	přímočarý	k2eAgFnPc1d1	přímočará
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
koncept	koncept	k1gInSc1	koncept
"	"	kIx"	"
<g/>
zlého	zlý	k2eAgInSc2d1	zlý
úmyslu	úmysl	k1gInSc2	úmysl
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
složitější	složitý	k2eAgMnSc1d2	složitější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemusí	muset	k5eNaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
znamenat	znamenat	k5eAaImF	znamenat
předběžnou	předběžný	k2eAgFnSc4d1	předběžná
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
formy	forma	k1gFnPc1	forma
"	"	kIx"	"
<g/>
zlého	zlý	k2eAgInSc2d1	zlý
úmyslu	úmysl	k1gInSc2	úmysl
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
následující	následující	k2eAgInPc1d1	následující
stavy	stav	k1gInPc1	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
:	:	kIx,	:
úmysl	úmysl	k1gInSc4	úmysl
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
úmysl	úmysl	k1gInSc4	úmysl
způsobit	způsobit	k5eAaPmF	způsobit
vážnou	vážný	k2eAgFnSc4d1	vážná
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
újmu	újma	k1gFnSc4	újma
blízkou	blízký	k2eAgFnSc4d1	blízká
smrti	smrt	k1gFnPc4	smrt
<g/>
,	,	kIx,	,
naprostá	naprostý	k2eAgFnSc1d1	naprostá
lhostejnost	lhostejnost	k1gFnSc1	lhostejnost
vůči	vůči	k7c3	vůči
neopodstatněně	opodstatněně	k6eNd1	opodstatněně
vysokému	vysoký	k2eAgNnSc3d1	vysoké
riziku	riziko	k1gNnSc3	riziko
pro	pro	k7c4	pro
život	život	k1gInSc4	život
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úmysl	úmysl	k1gInSc4	úmysl
spáchat	spáchat	k5eAaPmF	spáchat
nebezpečný	bezpečný	k2eNgInSc4d1	nebezpečný
zločin	zločin	k1gInSc4	zločin
(	(	kIx(	(
<g/>
doktrína	doktrína	k1gFnSc1	doktrína
"	"	kIx"	"
<g/>
zločin-vražda	zločinražda	k1gFnSc1	zločin-vražda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stavu	stav	k1gInSc2	stav
mysli	mysl	k1gFnSc2	mysl
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
úmyslu	úmysl	k1gInSc2	úmysl
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
pravidlo	pravidlo	k1gNnSc1	pravidlo
smrtící	smrtící	k2eAgFnSc2d1	smrtící
zbraně	zbraň	k1gFnSc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
pokud	pokud	k8xS	pokud
obviněný	obviněný	k1gMnSc1	obviněný
úmyslně	úmyslně	k6eAd1	úmyslně
použije	použít	k5eAaPmIp3nS	použít
smrtící	smrtící	k2eAgFnSc4d1	smrtící
zbraň	zbraň	k1gFnSc4	zbraň
nebo	nebo	k8xC	nebo
nástroj	nástroj	k1gInSc4	nástroj
proti	proti	k7c3	proti
oběti	oběť	k1gFnSc3	oběť
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
použití	použití	k1gNnSc1	použití
zakládá	zakládat	k5eAaImIp3nS	zakládat
hypotézu	hypotéza	k1gFnSc4	hypotéza
úmyslu	úmysl	k1gInSc2	úmysl
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
smrtící	smrtící	k2eAgFnSc2d1	smrtící
zbraně	zbraň	k1gFnSc2	zbraň
nebo	nebo	k8xC	nebo
nástroje	nástroj	k1gInPc4	nástroj
je	být	k5eAaImIp3nS	být
střelná	střelný	k2eAgFnSc1d1	střelná
zbraň	zbraň	k1gFnSc1	zbraň
<g/>
,	,	kIx,	,
nůž	nůž	k1gInSc1	nůž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
úmyslně	úmyslně	k6eAd1	úmyslně
použit	použít	k5eAaPmNgInS	použít
ke	k	k7c3	k
sražení	sražení	k1gNnSc3	sražení
oběti	oběť	k1gFnSc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stavu	stav	k1gInSc2	stav
mysli	mysl	k1gFnSc2	mysl
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
zabití	zabití	k1gNnSc4	zabití
vzejít	vzejít	k5eAaPmF	vzejít
z	z	k7c2	z
takového	takový	k3xDgNnSc2	takový
jednání	jednání	k1gNnSc2	jednání
obviněného	obviněný	k1gMnSc2	obviněný
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
lhostejnost	lhostejnost	k1gFnSc4	lhostejnost
k	k	k7c3	k
lidskému	lidský	k2eAgInSc3d1	lidský
životu	život	k1gInSc3	život
a	a	k8xC	a
vědomou	vědomý	k2eAgFnSc4d1	vědomá
nedbalost	nedbalost	k1gFnSc4	nedbalost
ohledně	ohledně	k7c2	ohledně
bezdůvodného	bezdůvodný	k2eAgNnSc2d1	bezdůvodné
rizika	riziko	k1gNnSc2	riziko
smrti	smrt	k1gFnSc2	smrt
nebo	nebo	k8xC	nebo
vážného	vážný	k2eAgNnSc2d1	vážné
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
praxe	praxe	k1gFnSc1	praxe
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obžalována	obžalovat	k5eAaPmNgFnS	obžalovat
a	a	k8xC	a
odsouzena	odsouzet	k5eAaImNgFnS	odsouzet
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
druhého	druhý	k4xOgInSc2	druhý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
zabije	zabít	k5eAaPmIp3nS	zabít
jiného	jiný	k2eAgMnSc4d1	jiný
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
řídí	řídit	k5eAaImIp3nS	řídit
motorové	motorový	k2eAgNnSc4d1	motorové
vozidlo	vozidlo	k1gNnSc4	vozidlo
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
drog	droga	k1gFnPc2	droga
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgFnPc2d1	jiná
kontrolovaných	kontrolovaný	k2eAgFnPc2d1	kontrolovaná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
stavu	stav	k1gInSc2	stav
mysli	mysl	k1gFnSc2	mysl
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doktríny	doktrína	k1gFnPc4	doktrína
zločin-vražda	zločinraždo	k1gNnSc2	zločin-vraždo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
spáchaný	spáchaný	k2eAgInSc1d1	spáchaný
zločin	zločin	k1gInSc1	zločin
principiálně	principiálně	k6eAd1	principiálně
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
zločinem	zločin	k1gInSc7	zločin
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vloupání	vloupání	k1gNnSc2	vloupání
<g/>
,	,	kIx,	,
žhářství	žhářství	k1gNnSc2	žhářství
<g/>
,	,	kIx,	,
znásilnění	znásilnění	k1gNnSc2	znásilnění
<g/>
,	,	kIx,	,
loupež	loupež	k1gFnSc1	loupež
nebo	nebo	k8xC	nebo
únos	únos	k1gInSc1	únos
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
podkladový	podkladový	k2eAgMnSc1d1	podkladový
<g/>
"	"	kIx"	"
zločin	zločin	k1gInSc1	zločin
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
méně	málo	k6eAd2	málo
závažným	závažný	k2eAgInSc7d1	závažný
skutkem	skutek	k1gInSc7	skutek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
napadení	napadení	k1gNnSc1	napadení
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	k9	by
všechna	všechen	k3xTgNnPc1	všechen
kriminální	kriminální	k2eAgNnPc1d1	kriminální
zabití	zabití	k1gNnPc1	zabití
byla	být	k5eAaImAgNnP	být
vraždou	vražda	k1gFnSc7	vražda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechna	všechen	k3xTgNnPc1	všechen
kriminální	kriminální	k2eAgNnPc1d1	kriminální
zabití	zabití	k1gNnPc1	zabití
jsou	být	k5eAaImIp3nP	být
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
odlišné	odlišný	k2eAgInPc4d1	odlišný
právní	právní	k2eAgInPc4d1	právní
a	a	k8xC	a
morální	morální	k2eAgInPc4d1	morální
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
spory	spor	k1gInPc1	spor
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
činy	čin	k1gInPc1	čin
jsou	být	k5eAaImIp3nP	být
vraždou	vražda	k1gFnSc7	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
interrupce	interrupce	k1gFnSc1	interrupce
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
právních	právní	k2eAgInPc6d1	právní
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
a	a	k8xC	a
morálních	morální	k2eAgNnPc6d1	morální
učeních	učení	k1gNnPc6	učení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
církví	církev	k1gFnPc2	církev
či	či	k8xC	či
filosofických	filosofický	k2eAgInPc6d1	filosofický
systémech	systém	k1gInPc6	systém
<g/>
)	)	kIx)	)
různých	různý	k2eAgInPc2d1	různý
států	stát	k1gInPc2	stát
klasifikována	klasifikovat	k5eAaImNgFnS	klasifikovat
různě	různě	k6eAd1	různě
-	-	kIx~	-
od	od	k7c2	od
mimořádně	mimořádně	k6eAd1	mimořádně
odpudivé	odpudivý	k2eAgFnSc2d1	odpudivá
vraždy	vražda	k1gFnSc2	vražda
po	po	k7c4	po
nezávadné	závadný	k2eNgNnSc4d1	nezávadné
jednání	jednání	k1gNnSc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
napříč	napříč	k7c7	napříč
státy	stát	k1gInPc7	stát
a	a	k8xC	a
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
odpůrců	odpůrce	k1gMnPc2	odpůrce
interrupcí	interrupce	k1gFnPc2	interrupce
považuje	považovat	k5eAaImIp3nS	považovat
z	z	k7c2	z
morálního	morální	k2eAgNnSc2d1	morální
hlediska	hledisko	k1gNnSc2	hledisko
interrupci	interrupce	k1gFnSc4	interrupce
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
právní	právní	k2eAgInPc1d1	právní
systémy	systém	k1gInPc1	systém
jejich	jejich	k3xOp3gInPc2	jejich
států	stát	k1gInPc2	stát
ji	on	k3xPp3gFnSc4	on
jako	jako	k8xC	jako
vraždu	vražda	k1gFnSc4	vražda
nedefinují	definovat	k5eNaBmIp3nP	definovat
(	(	kIx(	(
<g/>
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc1d1	podobný
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
s	s	k7c7	s
kvalifikací	kvalifikace	k1gFnSc7	kvalifikace
euthanasie	euthanasie	k1gFnSc2	euthanasie
nebo	nebo	k8xC	nebo
udělováním	udělování	k1gNnSc7	udělování
trestu	trest	k1gInSc2	trest
smrti	smrt	k1gFnSc2	smrt
za	za	k7c4	za
některé	některý	k3yIgInPc4	některý
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
za	za	k7c4	za
odpadlictví	odpadlictví	k1gNnSc4	odpadlictví
od	od	k7c2	od
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
kodifikovaný	kodifikovaný	k2eAgInSc1d1	kodifikovaný
v	v	k7c6	v
islámském	islámský	k2eAgNnSc6d1	islámské
právu	právo	k1gNnSc6	právo
Šaría	Šarí	k1gInSc2	Šarí
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
trestány	trestat	k5eAaImNgFnP	trestat
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
hereze	hereze	k1gFnPc1	hereze
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
také	také	k9	také
dodnes	dodnes	k6eAd1	dodnes
vlastizrada	vlastizrada	k1gFnSc1	vlastizrada
<g/>
.	.	kIx.	.
</s>
<s>
Podstatou	podstata	k1gFnSc7	podstata
problému	problém	k1gInSc2	problém
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
zpochybnění	zpochybnění	k1gNnSc1	zpochybnění
definice	definice	k1gFnSc2	definice
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spory	spor	k1gInPc1	spor
a	a	k8xC	a
nejasnosti	nejasnost	k1gFnPc1	nejasnost
ohledně	ohledně	k7c2	ohledně
definice	definice	k1gFnSc2	definice
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
plod	plod	k1gInSc1	plod
stává	stávat	k5eAaImIp3nS	stávat
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
lidmi	člověk	k1gMnPc7	člověk
některá	některý	k3yIgNnPc1	některý
etnika	etnikum	k1gNnPc1	etnikum
nebo	nebo	k8xC	nebo
příslušníci	příslušník	k1gMnPc1	příslušník
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
)	)	kIx)	)
a	a	k8xC	a
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
morálního	morální	k2eAgNnSc2d1	morální
a	a	k8xC	a
právního	právní	k2eAgNnSc2d1	právní
hlediska	hledisko	k1gNnSc2	hledisko
náplní	náplň	k1gFnPc2	náplň
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
oprávněné	oprávněný	k2eAgFnPc1d1	oprávněná
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hlásání	hlásání	k1gNnSc4	hlásání
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
hrdelním	hrdelní	k2eAgInSc7d1	hrdelní
zločinem	zločin	k1gInSc7	zločin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vícenásobný	vícenásobný	k2eAgMnSc1d1	vícenásobný
vrah	vrah	k1gMnSc1	vrah
je	být	k5eAaImIp3nS	být
vrah	vrah	k1gMnSc1	vrah
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
obětí	oběť	k1gFnSc7	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
pojmem	pojem	k1gInSc7	pojem
sériová	sériový	k2eAgFnSc1d1	sériová
vražda	vražda	k1gFnSc1	vražda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
pouze	pouze	k6eAd1	pouze
jedním	jeden	k4xCgInSc7	jeden
druhem	druh	k1gInSc7	druh
vícenásobné	vícenásobný	k2eAgFnSc2d1	vícenásobná
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
probíhá	probíhat	k5eAaImIp3nS	probíhat
především	především	k9	především
empirický	empirický	k2eAgInSc1d1	empirický
výzkum	výzkum	k1gInSc1	výzkum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přináší	přinášet	k5eAaImIp3nS	přinášet
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
páchaných	páchaný	k2eAgInPc6d1	páchaný
zločinech	zločin	k1gInPc6	zločin
a	a	k8xC	a
také	také	k9	také
jejich	jejich	k3xOp3gFnPc4	jejich
nové	nový	k2eAgFnPc4d1	nová
definice	definice	k1gFnPc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
forenzní	forenzní	k2eAgFnSc1d1	forenzní
psychologie	psychologie	k1gFnSc1	psychologie
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
vícenásobných	vícenásobný	k2eAgMnPc2d1	vícenásobný
vrahů	vrah	k1gMnPc2	vrah
<g/>
:	:	kIx,	:
</s>
<s>
Masový	masový	k2eAgMnSc1d1	masový
vrah	vrah	k1gMnSc1	vrah
-	-	kIx~	-
zabije	zabít	k5eAaPmIp3nS	zabít
více	hodně	k6eAd2	hodně
obětí	oběť	k1gFnPc2	oběť
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
nepřetržitém	přetržitý	k2eNgInSc6d1	nepřetržitý
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sebevražední	sebevražedný	k2eAgMnPc1d1	sebevražedný
atentátníci	atentátník	k1gMnPc1	atentátník
<g/>
.	.	kIx.	.
</s>
<s>
Záchvatový	záchvatový	k2eAgMnSc1d1	záchvatový
vrah	vrah	k1gMnSc1	vrah
(	(	kIx(	(
<g/>
Vrah	vrah	k1gMnSc1	vrah
na	na	k7c6	na
vražedné	vražedný	k2eAgFnSc6d1	vražedná
vlně	vlna	k1gFnSc6	vlna
<g/>
)	)	kIx)	)
-	-	kIx~	-
zločiny	zločin	k1gInPc1	zločin
páchá	páchat	k5eAaImIp3nS	páchat
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
spojitém	spojitý	k2eAgInSc6d1	spojitý
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
krátkém	krátký	k2eAgInSc6d1	krátký
<g/>
)	)	kIx)	)
časovém	časový	k2eAgInSc6d1	časový
intervalu	interval	k1gInSc6	interval
a	a	k8xC	a
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
lokalitě	lokalita	k1gFnSc6	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
činů	čin	k1gInPc2	čin
se	se	k3xPyFc4	se
však	však	k9	však
něčím	něčí	k3xOyIgFnPc3	něčí
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Příznačné	příznačný	k2eAgNnSc1d1	příznačné
je	být	k5eAaImIp3nS	být
náhlé	náhlý	k2eAgNnSc1d1	náhlé
ukončení	ukončení	k1gNnSc1	ukončení
vražd	vražda	k1gFnPc2	vražda
<g/>
,	,	kIx,	,
vrah	vrah	k1gMnSc1	vrah
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
zastaví	zastavit	k5eAaPmIp3nS	zastavit
(	(	kIx(	(
<g/>
vražedná	vražedný	k2eAgFnSc1d1	vražedná
vlna	vlna	k1gFnSc1	vlna
sama	sám	k3xTgFnSc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
"	"	kIx"	"
<g/>
vyšumí	vyšumět	k5eAaPmIp3nP	vyšumět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sériový	sériový	k2eAgMnSc1d1	sériový
vrah	vrah	k1gMnSc1	vrah
-	-	kIx~	-
vraždí	vraždit	k5eAaImIp3nP	vraždit
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
časových	časový	k2eAgInPc6d1	časový
intervalech	interval	k1gInPc6	interval
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
zločin	zločin	k1gInSc1	zločin
je	být	k5eAaImIp3nS	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
vrah	vrah	k1gMnSc1	vrah
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
aktivní	aktivní	k2eAgMnSc1d1	aktivní
i	i	k9	i
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
s	s	k7c7	s
vražděním	vraždění	k1gNnSc7	vraždění
nepřestane	přestat	k5eNaPmIp3nS	přestat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
druhem	druh	k1gInSc7	druh
vraha	vrah	k1gMnSc2	vrah
je	být	k5eAaImIp3nS	být
nájemný	nájemný	k2eAgMnSc1d1	nájemný
vrah	vrah	k1gMnSc1	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgMnSc1d1	soudní
psycholog	psycholog	k1gMnSc1	psycholog
Slavomír	Slavomír	k1gMnSc1	Slavomír
Hubálek	Hubálek	k1gMnSc1	Hubálek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyšetřil	vyšetřit	k5eAaPmAgMnS	vyšetřit
cca	cca	kA	cca
50	[number]	k4	50
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
fenomén	fenomén	k1gInSc1	fenomén
nájemných	nájemný	k2eAgMnPc2d1	nájemný
vrahů	vrah	k1gMnPc2	vrah
jako	jako	k8xC	jako
(	(	kIx(	(
<g/>
ilegální	ilegální	k2eAgMnPc4d1	ilegální
<g/>
)	)	kIx)	)
nové	nový	k2eAgMnPc4d1	nový
"	"	kIx"	"
<g/>
povolání	povolání	k1gNnSc2	povolání
<g/>
"	"	kIx"	"
z	z	k7c2	z
důsledku	důsledek	k1gInSc2	důsledek
poptávky	poptávka	k1gFnSc2	poptávka
tržního	tržní	k2eAgNnSc2d1	tržní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jejich	jejich	k3xOp3gNnSc1	jejich
chování	chování	k1gNnSc1	chování
bylo	být	k5eAaImAgNnS	být
motivováno	motivovat	k5eAaBmNgNnS	motivovat
nějakou	nějaký	k3yIgFnSc7	nějaký
psychologickou	psychologický	k2eAgFnSc7d1	psychologická
indispozicí	indispozice	k1gFnSc7	indispozice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
je	on	k3xPp3gMnPc4	on
ovládala	ovládat	k5eAaImAgFnS	ovládat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toliko	toliko	k6eAd1	toliko
penězi	peníze	k1gInPc7	peníze
-	-	kIx~	-
svou	svůj	k3xOyFgFnSc4	svůj
práci	práce	k1gFnSc4	práce
mají	mít	k5eAaImIp3nP	mít
brát	brát	k5eAaImF	brát
jako	jako	k9	jako
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc1	oběť
nazývají	nazývat	k5eAaImIp3nP	nazývat
klienty	klient	k1gMnPc4	klient
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
většinou	většinou	k6eAd1	většinou
vyšší	vysoký	k2eAgInPc1d2	vyšší
IQ	iq	kA	iq
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
neurotičtí	neurotický	k2eAgMnPc1d1	neurotický
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
kriminální	kriminální	k2eAgFnSc4d1	kriminální
anamnézu	anamnéza	k1gFnSc4	anamnéza
a	a	k8xC	a
nestrádají	strádat	k5eNaImIp3nP	strádat
organickým	organický	k2eAgNnSc7d1	organické
postižením	postižení	k1gNnSc7	postižení
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
používaným	používaný	k2eAgInSc7d1	používaný
termínem	termín	k1gInSc7	termín
je	být	k5eAaImIp3nS	být
justiční	justiční	k2eAgFnSc1d1	justiční
vražda	vražda	k1gFnSc1	vražda
-	-	kIx~	-
vražda	vražda	k1gFnSc1	vražda
spáchaná	spáchaný	k2eAgFnSc1d1	spáchaná
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
soudního	soudní	k2eAgInSc2d1	soudní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
úmyslným	úmyslný	k2eAgNnSc7d1	úmyslné
vynesením	vynesení	k1gNnSc7	vynesení
rozsudku	rozsudek	k1gInSc2	rozsudek
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nevinného	vinný	k2eNgNnSc2d1	nevinné
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nedopustil	dopustit	k5eNaPmAgMnS	dopustit
zločinu	zločin	k1gInSc3	zločin
<g/>
,	,	kIx,	,
za	za	k7c4	za
nějž	jenž	k3xRgMnSc4	jenž
přísluší	příslušet	k5eAaImIp3nS	příslušet
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rakouský	rakouský	k2eAgInSc1d1	rakouský
trestní	trestní	k2eAgInSc1d1	trestní
zákon	zákon	k1gInSc1	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1852	[number]	k4	1852
(	(	kIx(	(
<g/>
platný	platný	k2eAgInSc1d1	platný
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
do	do	k7c2	do
r.	r.	kA	r.
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozlišoval	rozlišovat	k5eAaImAgMnS	rozlišovat
čtyři	čtyři	k4xCgInPc4	čtyři
druhy	druh	k1gInPc4	druh
vražd	vražda	k1gFnPc2	vražda
(	(	kIx(	(
<g/>
§	§	k?	§
135	[number]	k4	135
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
vražd	vražda	k1gFnPc2	vražda
se	se	k3xPyFc4	se
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
jazyce	jazyk	k1gInSc6	jazyk
užívají	užívat	k5eAaImIp3nP	užívat
stále	stále	k6eAd1	stále
<g/>
:	:	kIx,	:
Vražda	vražda	k1gFnSc1	vražda
úkladná	úkladný	k2eAgFnSc1d1	úkladná
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
jedem	jed	k1gInSc7	jed
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
potměšilým	potměšilý	k2eAgInSc7d1	potměšilý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
loupežná	loupežný	k2eAgFnSc1d1	loupežná
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
páše	páchat	k5eAaImIp3nS	páchat
v	v	k7c6	v
obmyslu	obmysl	k1gInSc6	obmysl
převésti	převést	k5eAaPmF	převést
na	na	k7c4	na
se	se	k3xPyFc4	se
cizí	cizí	k2eAgFnSc4d1	cizí
věc	věc	k1gFnSc4	věc
movitou	movitý	k2eAgFnSc4d1	movitá
násilnostmi	násilnost	k1gFnPc7	násilnost
proti	proti	k7c3	proti
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
zjednaná	zjednaný	k2eAgFnSc1d1	zjednaná
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc7	jenž
někdo	někdo	k3yInSc1	někdo
byl	být	k5eAaImAgMnS	být
najat	najmout	k5eAaPmNgMnS	najmout
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
od	od	k7c2	od
osoby	osoba	k1gFnSc2	osoba
třetí	třetí	k4xOgInSc1	třetí
pohnut	pohnut	k2eAgMnSc1d1	pohnut
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
prostá	prostý	k2eAgFnSc1d1	prostá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
žádnému	žádný	k3yNgInSc3	žádný
zde	zde	k6eAd1	zde
uvedenému	uvedený	k2eAgInSc3d1	uvedený
těžkému	těžký	k2eAgInSc3d1	těžký
druhu	druh	k1gInSc3	druh
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
§	§	k?	§
140	[number]	k4	140
trestního	trestní	k2eAgInSc2d1	trestní
zákoníku	zákoník	k1gInSc2	zákoník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
je	být	k5eAaImIp3nS	být
vraždou	vražda	k1gFnSc7	vražda
úmyslné	úmyslný	k2eAgNnSc1d1	úmyslné
usmrcení	usmrcení	k1gNnSc1	usmrcení
jiného	jiné	k1gNnSc2	jiné
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
hrozí	hrozit	k5eAaImIp3nS	hrozit
trest	trest	k1gInSc1	trest
odnětí	odnětí	k1gNnSc2	odnětí
svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
trestní	trestní	k2eAgFnSc6d1	trestní
sazbě	sazba	k1gFnSc6	sazba
na	na	k7c4	na
deset	deset	k4xCc4	deset
až	až	k9	až
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Kvalifikované	kvalifikovaný	k2eAgFnPc1d1	kvalifikovaná
skutkové	skutkový	k2eAgFnPc1d1	skutková
podstaty	podstata	k1gFnPc1	podstata
spočívají	spočívat	k5eAaImIp3nP	spočívat
např.	např.	kA	např.
ve	v	k7c6	v
vraždě	vražda	k1gFnSc6	vražda
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
těhotné	těhotný	k2eAgFnPc1d1	těhotná
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
dítěte	dítě	k1gNnSc2	dítě
mladšího	mladý	k2eAgNnSc2d2	mladší
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vraždě	vražda	k1gFnSc6	vražda
provedené	provedený	k2eAgFnSc6d1	provedená
zvlášť	zvlášť	k6eAd1	zvlášť
surovým	surový	k2eAgInSc7d1	surový
nebo	nebo	k8xC	nebo
trýznivým	trýznivý	k2eAgInSc7d1	trýznivý
způsobem	způsob	k1gInSc7	způsob
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
již	již	k9	již
hranice	hranice	k1gFnSc1	hranice
trestní	trestní	k2eAgFnSc2d1	trestní
sazby	sazba	k1gFnSc2	sazba
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
nejméně	málo	k6eAd3	málo
patnácti	patnáct	k4xCc2	patnáct
až	až	k9	až
do	do	k7c2	do
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ale	ale	k9	ale
také	také	k9	také
uložit	uložit	k5eAaPmF	uložit
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
samotná	samotný	k2eAgFnSc1d1	samotná
příprava	příprava	k1gFnSc1	příprava
vraždy	vražda	k1gFnSc2	vražda
je	být	k5eAaImIp3nS	být
trestná	trestný	k2eAgFnSc1d1	trestná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tohoto	tento	k3xDgInSc2	tento
trestného	trestný	k2eAgInSc2d1	trestný
činu	čin	k1gInSc2	čin
zná	znát	k5eAaImIp3nS	znát
trestní	trestní	k2eAgInSc1d1	trestní
zákoník	zákoník	k1gInSc1	zákoník
i	i	k8xC	i
trestné	trestný	k2eAgInPc1d1	trestný
činy	čin	k1gInPc1	čin
zabití	zabití	k1gNnSc2	zabití
(	(	kIx(	(
<g/>
§	§	k?	§
141	[number]	k4	141
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vražda	vražda	k1gFnSc1	vražda
novorozeného	novorozený	k2eAgNnSc2d1	novorozené
dítěte	dítě	k1gNnSc2	dítě
matkou	matka	k1gFnSc7	matka
(	(	kIx(	(
<g/>
§	§	k?	§
142	[number]	k4	142
<g/>
)	)	kIx)	)
a	a	k8xC	a
usmrcení	usmrcení	k1gNnSc1	usmrcení
z	z	k7c2	z
nedbalosti	nedbalost	k1gFnSc2	nedbalost
(	(	kIx(	(
<g/>
§	§	k?	§
143	[number]	k4	143
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hranice	hranice	k1gFnPc4	hranice
trestních	trestní	k2eAgFnPc2d1	trestní
sazeb	sazba	k1gFnPc2	sazba
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
okolnostem	okolnost	k1gFnPc3	okolnost
nižší	nízký	k2eAgInSc4d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Vražda	vražda	k1gFnSc1	vražda
je	být	k5eAaImIp3nS	být
trestná	trestný	k2eAgFnSc1d1	trestná
i	i	k9	i
podle	podle	k7c2	podle
norem	norma	k1gFnPc2	norma
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
kán	kát	k5eAaImNgInS	kát
<g/>
.	.	kIx.	.
1397	[number]	k4	1397
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInPc1d1	zvláštní
tresty	trest	k1gInPc1	trest
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
za	za	k7c4	za
vraždu	vražda	k1gFnSc4	vražda
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
biskupa	biskup	k1gMnSc2	biskup
(	(	kIx(	(
<g/>
kán	kát	k5eAaImNgInS	kát
<g/>
.	.	kIx.	.
1370	[number]	k4	1370
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kodexu	kodex	k1gInSc6	kodex
kanonického	kanonický	k2eAgNnSc2d1	kanonické
práva	právo	k1gNnSc2	právo
je	být	k5eAaImIp3nS	být
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c6	o
vraždě	vražda	k1gFnSc6	vražda
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
ustanovením	ustanovení	k1gNnSc7	ustanovení
o	o	k7c4	o
interrupci	interrupce	k1gFnSc4	interrupce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kán	kát	k5eAaImNgInS	kát
<g/>
.	.	kIx.	.
1090	[number]	k4	1090
je	být	k5eAaImIp3nS	být
překážkou	překážka	k1gFnSc7	překážka
manželství	manželství	k1gNnSc2	manželství
vražda	vražda	k1gFnSc1	vražda
nebo	nebo	k8xC	nebo
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
vraždě	vražda	k1gFnSc6	vražda
předcházejícího	předcházející	k2eAgMnSc2d1	předcházející
manžela	manžel	k1gMnSc2	manžel
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
chce	chtít	k5eAaImIp3nS	chtít
pachatel	pachatel	k1gMnSc1	pachatel
uzavřít	uzavřít	k5eAaPmF	uzavřít
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
