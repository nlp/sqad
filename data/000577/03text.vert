<s>
David	David	k1gMnSc1	David
Nykl	Nykl	k1gMnSc1	Nykl
(	(	kIx(	(
<g/>
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1967	[number]	k4	1967
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
herec	herec	k1gMnSc1	herec
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
jako	jako	k9	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Radek	Radek	k1gMnSc1	Radek
Zelenka	Zelenka	k1gMnSc1	Zelenka
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
jej	on	k3xPp3gMnSc4	on
rodiče	rodič	k1gMnPc1	rodič
odvezli	odvézt	k5eAaPmAgMnP	odvézt
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
emigrovali	emigrovat	k5eAaBmAgMnP	emigrovat
po	po	k7c6	po
sovětské	sovětský	k2eAgFnSc6d1	sovětská
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nyklovi	Nyklův	k2eAgMnPc1d1	Nyklův
se	se	k3xPyFc4	se
usadili	usadit	k5eAaPmAgMnP	usadit
ve	v	k7c6	v
Viktorii	Viktoria	k1gFnSc6	Viktoria
v	v	k7c6	v
Britské	britský	k2eAgFnSc6d1	britská
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
otec	otec	k1gMnSc1	otec
brzy	brzy	k6eAd1	brzy
našel	najít	k5eAaPmAgMnS	najít
práci	práce	k1gFnSc4	práce
jako	jako	k8xS	jako
stavební	stavební	k2eAgMnSc1d1	stavební
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
jako	jako	k8xS	jako
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
sestra	sestra	k1gFnSc1	sestra
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Davida	David	k1gMnSc2	David
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
projevovat	projevovat	k5eAaImF	projevovat
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
hraní	hraní	k1gNnSc3	hraní
<g/>
,	,	kIx,	,
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
humor	humor	k1gInSc4	humor
a	a	k8xC	a
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
všechny	všechen	k3xTgInPc4	všechen
druhy	druh	k1gInPc4	druh
umělecké	umělecký	k2eAgFnSc2d1	umělecká
realizace	realizace	k1gFnSc2	realizace
-	-	kIx~	-
televizní	televizní	k2eAgFnSc2d1	televizní
reklamy	reklama	k1gFnSc2	reklama
(	(	kIx(	(
<g/>
Adidas	Adidas	k1gInSc1	Adidas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
školní	školní	k2eAgNnSc4d1	školní
představení	představení	k1gNnSc4	představení
a	a	k8xC	a
na	na	k7c4	na
vánoční	vánoční	k2eAgFnSc4d1	vánoční
show	show	k1gFnSc4	show
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
piano	piano	k1gNnSc4	piano
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
herecké	herecký	k2eAgNnSc4d1	herecké
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
taneční	taneční	k2eAgFnSc2d1	taneční
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
a	a	k8xC	a
zrežíroval	zrežírovat	k5eAaPmAgMnS	zrežírovat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hru	hra	k1gFnSc4	hra
<g/>
:	:	kIx,	:
Vražedná	vražedný	k2eAgFnSc1d1	vražedná
místnost	místnost	k1gFnSc1	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
Kolumbijskou	kolumbijský	k2eAgFnSc4d1	kolumbijská
univerzitu	univerzita	k1gFnSc4	univerzita
ve	v	k7c6	v
Vancouveru	Vancouver	k1gInSc6	Vancouver
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
literatuře	literatura	k1gFnSc3	literatura
<g/>
,	,	kIx,	,
divadlu	divadlo	k1gNnSc3	divadlo
a	a	k8xC	a
herecké	herecký	k2eAgFnSc3d1	herecká
třídě	třída	k1gFnSc3	třída
<g/>
.	.	kIx.	.
</s>
<s>
Odpromoval	odpromovat	k5eAaPmAgMnS	odpromovat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
rok	rok	k1gInSc4	rok
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
na	na	k7c6	na
vancouverské	vancouverský	k2eAgFnSc6d1	vancouverská
divadelní	divadelní	k2eAgFnSc6d1	divadelní
scéně	scéna	k1gFnSc6	scéna
(	(	kIx(	(
<g/>
Pacific	Pacifice	k1gFnPc2	Pacifice
Theatre	Theatr	k1gInSc5	Theatr
<g/>
,	,	kIx,	,
Vancouver	Vancouver	k1gInSc1	Vancouver
Little	Little	k1gFnSc2	Little
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
,	,	kIx,	,
Vagabond	Vagabond	k?	Vagabond
Players	Players	k1gInSc1	Players
<g/>
,	,	kIx,	,
Metro	metro	k1gNnSc1	metro
Theatre	Theatr	k1gInSc5	Theatr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xS	jako
Ironworks	Ironworks	k1gInSc4	Ironworks
Productions	Productions	k1gInSc1	Productions
-	-	kIx~	-
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
přátel	přítel	k1gMnPc2	přítel
na	na	k7c6	na
Vancouverském	Vancouverský	k2eAgInSc6d1	Vancouverský
venkovním	venkovní	k2eAgInSc6d1	venkovní
shakespearovském	shakespearovský	k2eAgInSc6d1	shakespearovský
festivalu	festival	k1gInSc6	festival
<g/>
:	:	kIx,	:
Bard	bard	k1gMnSc1	bard
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
první	první	k4xOgFnSc7	první
hrou	hra	k1gFnSc7	hra
"	"	kIx"	"
<g/>
Čekání	čekání	k1gNnSc1	čekání
na	na	k7c4	na
Godota	Godot	k1gMnSc4	Godot
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
debutovala	debutovat	k5eAaBmAgFnS	debutovat
na	na	k7c4	na
Galiano	Galiana	k1gFnSc5	Galiana
Island	Island	k1gInSc1	Island
BC	BC	kA	BC
<g/>
,	,	kIx,	,
Nykl	Nykl	k1gMnSc1	Nykl
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ironworks	Ironworks	k1gInSc4	Ironworks
poprvé	poprvé	k6eAd1	poprvé
objel	objet	k5eAaPmAgMnS	objet
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
byla	být	k5eAaImAgFnS	být
až	až	k9	až
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
tohoto	tento	k3xDgInSc2	tento
vzestupu	vzestup	k1gInSc2	vzestup
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nový	nový	k2eAgInSc1d1	nový
"	"	kIx"	"
<g/>
Pohyblivý	pohyblivý	k2eAgInSc1d1	pohyblivý
svátek	svátek	k1gInSc1	svátek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Nykl	Nykl	k1gMnSc1	Nykl
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dostal	dostat	k5eAaPmAgMnS	dostat
roli	role	k1gFnSc4	role
v	v	k7c6	v
divadelním	divadelní	k2eAgInSc6d1	divadelní
spolku	spolek	k1gInSc6	spolek
Kašpar	Kašpar	k1gMnSc1	Kašpar
jako	jako	k8xC	jako
Attahualpa	Attahualpa	k1gFnSc1	Attahualpa
<g/>
,	,	kIx,	,
Inský	Inský	k2eAgMnSc1d1	Inský
bůh	bůh	k1gMnSc1	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
"	"	kIx"	"
<g/>
Královský	královský	k2eAgInSc4d1	královský
lov	lov	k1gInSc4	lov
na	na	k7c4	na
Slunce	slunce	k1gNnSc4	slunce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hemžilo	hemžit	k5eAaImAgNnS	hemžit
herci	herec	k1gMnSc3	herec
<g/>
,	,	kIx,	,
spisovateli	spisovatel	k1gMnSc3	spisovatel
a	a	k8xC	a
umělci	umělec	k1gMnSc3	umělec
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
perfektním	perfektní	k2eAgNnSc7d1	perfektní
místem	místo	k1gNnSc7	místo
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
vrhal	vrhat	k5eAaImAgInS	vrhat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
divadelní	divadelní	k2eAgFnSc2d1	divadelní
zkoušky	zkouška	k1gFnSc2	zkouška
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
Nykl	Nykl	k1gMnSc1	Nykl
objevil	objevit	k5eAaPmAgMnS	objevit
skutečné	skutečný	k2eAgNnSc4d1	skutečné
zalíbení	zalíbení	k1gNnSc4	zalíbení
v	v	k7c6	v
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
,	,	kIx,	,
filmu	film	k1gInSc6	film
a	a	k8xC	a
komerční	komerční	k2eAgFnSc3d1	komerční
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
objevil	objevit	k5eAaPmAgInS	objevit
pražskou	pražský	k2eAgFnSc4d1	Pražská
nejlépe	dobře	k6eAd3	dobře
známou	známý	k2eAgFnSc4d1	známá
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgFnSc4d1	mluvící
divadelní	divadelní	k2eAgFnSc4d1	divadelní
společnost	společnost	k1gFnSc4	společnost
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bidu	Bida	k1gFnSc4	Bida
milujíci	milujík	k1gMnPc1	milujík
společnost	společnost	k1gFnSc1	společnost
<g/>
"	"	kIx"	"
s	s	k7c7	s
Richardem	Richard	k1gMnSc7	Richard
Tothem	Toth	k1gInSc7	Toth
a	a	k8xC	a
Evženem	Evžen	k1gMnSc7	Evžen
McLarenem	McLaren	k1gMnSc7	McLaren
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
"	"	kIx"	"
<g/>
Bídu	bída	k1gFnSc4	bída
milující	milující	k2eAgFnSc4d1	milující
společnost	společnost	k1gFnSc4	společnost
<g/>
"	"	kIx"	"
vždy	vždy	k6eAd1	vždy
vyprodala	vyprodat	k5eAaPmAgFnS	vyprodat
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
byl	být	k5eAaImAgMnS	být
Nykl	Nykl	k1gMnSc1	Nykl
přímo	přímo	k6eAd1	přímo
zapojen	zapojit	k5eAaPmNgMnS	zapojit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sedmnácti	sedmnáct	k4xCc2	sedmnáct
produkcí	produkce	k1gFnPc2	produkce
<g/>
:	:	kIx,	:
jako	jako	k8xS	jako
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
domácího	domácí	k2eAgInSc2d1	domácí
Vancouveru	Vancouver	k1gInSc2	Vancouver
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
plně	plně	k6eAd1	plně
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
(	(	kIx(	(
<g/>
třeba	třeba	k6eAd1	třeba
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
reklamách	reklama	k1gFnPc6	reklama
a	a	k8xC	a
ve	v	k7c6	v
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgInS	zahrát
si	se	k3xPyFc3	se
taky	taky	k6eAd1	taky
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
sitcomu	sitcom	k1gInSc6	sitcom
<g/>
,	,	kIx,	,
okradeného	okradený	k2eAgMnSc4d1	okradený
turistu	turista	k1gMnSc4	turista
<g/>
,	,	kIx,	,
Policajti	Policajti	k?	Policajti
z	z	k7c2	z
předměstí	předměstí	k1gNnSc2	předměstí
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
díl	díl	k1gInSc1	díl
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
díle	dílo	k1gNnSc6	dílo
Sanctuary	Sanctuara	k1gFnSc2	Sanctuara
a	a	k8xC	a
jednom	jeden	k4xCgInSc6	jeden
díle	díl	k1gInSc6	díl
Agentury	agentura	k1gFnSc2	agentura
Jasno	jasno	k1gNnSc4	jasno
(	(	kIx(	(
<g/>
Shawn	Shawn	k1gInSc1	Shawn
vs	vs	k?	vs
<g/>
.	.	kIx.	.
red	red	k?	red
phantom	phantom	k1gInSc1	phantom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Radek	Radek	k1gMnSc1	Radek
Zelenka	Zelenka	k1gMnSc1	Zelenka
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
českého	český	k2eAgMnSc2d1	český
vědce	vědec	k1gMnSc2	vědec
a	a	k8xC	a
účastníka	účastník	k1gMnSc2	účastník
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
expedice	expedice	k1gFnSc2	expedice
do	do	k7c2	do
města	město	k1gNnSc2	město
antiků	antik	k1gInPc2	antik
Atlantidy	Atlantida	k1gFnSc2	Atlantida
ztvárněná	ztvárněný	k2eAgFnSc1d1	ztvárněná
Davidem	David	k1gMnSc7	David
Nyklem	Nykl	k1gMnSc7	Nykl
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
.	.	kIx.	.
</s>
<s>
Mluví	mluvit	k5eAaImIp3nP	mluvit
jako	jako	k9	jako
všichni	všechen	k3xTgMnPc1	všechen
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
<g/>
,	,	kIx,	,
při	při	k7c6	při
rozčilení	rozčilení	k1gNnSc6	rozčilení
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
řekne	říct	k5eAaPmIp3nS	říct
něco	něco	k3yInSc1	něco
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
vědců	vědec	k1gMnPc2	vědec
expedice	expedice	k1gFnSc2	expedice
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
oborem	obor	k1gInSc7	obor
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
antická	antický	k2eAgFnSc1d1	antická
technologie	technologie	k1gFnSc1	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ovládá	ovládat	k5eAaImIp3nS	ovládat
systémy	systém	k1gInPc4	systém
Puddle	Puddle	k1gFnSc2	Puddle
Jumperů	jumper	k1gInPc2	jumper
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgInPc4d1	ostatní
antické	antický	k2eAgInPc4d1	antický
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
respektován	respektovat	k5eAaImNgInS	respektovat
ostatními	ostatní	k2eAgMnPc7d1	ostatní
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
McKayem	McKay	k1gMnSc7	McKay
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
zachránil	zachránit	k5eAaPmAgInS	zachránit
život	život	k1gInSc1	život
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
Puddle	Puddle	k1gMnPc2	Puddle
Jumperů	jumper	k1gInPc2	jumper
uvízl	uvíznout	k5eAaPmAgMnS	uvíznout
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
také	také	k9	také
pomohl	pomoct	k5eAaPmAgMnS	pomoct
zabezpečit	zabezpečit	k5eAaPmF	zabezpečit
město	město	k1gNnSc4	město
Atlantis	Atlantis	k1gFnSc1	Atlantis
před	před	k7c7	před
ničivou	ničivý	k2eAgFnSc7d1	ničivá
bouřkou	bouřka	k1gFnSc7	bouřka
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
objevil	objevit	k5eAaPmAgInS	objevit
mechanismus	mechanismus	k1gInSc1	mechanismus
otevírání	otevírání	k1gNnSc2	otevírání
hangáru	hangár	k1gInSc2	hangár
Puddle	Puddle	k1gMnPc2	Puddle
Jumperů	jumper	k1gInPc2	jumper
a	a	k8xC	a
senzory	senzor	k1gInPc1	senzor
pro	pro	k7c4	pro
zkoumání	zkoumání	k1gNnSc4	zkoumání
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
nedisponuje	disponovat	k5eNaBmIp3nS	disponovat
genem	gen	k1gInSc7	gen
antiků	antik	k1gInPc2	antik
a	a	k8xC	a
gen	gen	k1gInSc1	gen
pro	pro	k7c4	pro
ovládání	ovládání	k1gNnSc4	ovládání
některých	některý	k3yIgFnPc2	některý
technologií	technologie	k1gFnPc2	technologie
antiků	antik	k1gMnPc2	antik
se	se	k3xPyFc4	se
neujal	ujmout	k5eNaPmAgInS	ujmout
ani	ani	k8xC	ani
genovou	genový	k2eAgFnSc7d1	genová
terapií	terapie	k1gFnSc7	terapie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
nakažen	nakazit	k5eAaPmNgMnS	nakazit
nanoviry	nanovir	k1gMnPc7	nanovir
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
halucinace	halucinace	k1gFnPc4	halucinace
a	a	k8xC	a
následně	následně	k6eAd1	následně
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Naštěstí	naštěstí	k6eAd1	naštěstí
nanoviry	nanovira	k1gFnPc1	nanovira
byly	být	k5eAaImAgFnP	být
zničeny	zničen	k2eAgMnPc4d1	zničen
elektromagnetickým	elektromagnetický	k2eAgInSc7d1	elektromagnetický
pulsem	puls	k1gInSc7	puls
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
Doktor	doktor	k1gMnSc1	doktor
Zelenka	Zelenka	k1gMnSc1	Zelenka
může	moct	k5eAaImIp3nS	moct
dál	daleko	k6eAd2	daleko
na	na	k7c6	na
Atlantidě	Atlantida	k1gFnSc6	Atlantida
objevovat	objevovat	k5eAaImF	objevovat
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
a	a	k8xC	a
zachraňovat	zachraňovat	k5eAaImF	zachraňovat
životy	život	k1gInPc4	život
...	...	k?	...
</s>
