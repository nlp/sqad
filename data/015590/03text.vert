<s>
Moped	moped	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
dopravním	dopravní	k2eAgInSc6d1
prostředku	prostředek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
hudební	hudební	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Moped	moped	k1gInSc1
(	(	kIx(
<g/>
hudební	hudební	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Jawa	jawa	k1gFnSc1
50	#num#	k4
typ	typ	k1gInSc1
551S	551S	k4
Jawetta	Jawett	k1gInSc2
Sport	sport	k1gInSc1
</s>
<s>
Moped	moped	k1gInSc1
Stadion	stadion	k1gInSc1
S11	S11	k1gFnSc2
</s>
<s>
Moped	moped	k1gInSc1
,	,	kIx,
<g/>
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
„	„	k?
<g/>
motor	motor	k1gInSc1
s	s	k7c7
pedály	pedál	k1gInPc7
<g/>
“	“	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
zjednodušeně	zjednodušeně	k6eAd1
řečeno	říct	k5eAaPmNgNnS
kombinace	kombinace	k1gFnSc1
motocyklu	motocykl	k1gInSc2
a	a	k8xC
bicyklu	bicykl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jednostopé	jednostopý	k2eAgNnSc4d1
motorové	motorový	k2eAgNnSc4d1
vozidlo	vozidlo	k1gNnSc4
určené	určený	k2eAgNnSc4d1
pro	pro	k7c4
jednu	jeden	k4xCgFnSc4
osobu	osoba	k1gFnSc4
a	a	k8xC
na	na	k7c6
krátké	krátký	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
s	s	k7c7
objemem	objem	k1gInSc7
motoru	motor	k1gInSc2
do	do	k7c2
50	#num#	k4
cm³	cm³	k?
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
speciální	speciální	k2eAgInSc4d1
rám	rám	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
pevnější	pevný	k2eAgInSc1d2
a	a	k8xC
má	mít	k5eAaImIp3nS
delší	dlouhý	k2eAgFnSc4d2
životnost	životnost	k1gFnSc4
než	než	k8xS
u	u	k7c2
bicyklu	bicykl	k1gInSc2
a	a	k8xC
bicyklu	bicykl	k1gInSc2
s	s	k7c7
pomocným	pomocný	k2eAgInSc7d1
motorkem	motorek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kola	kolo	k1gNnPc1
jsou	být	k5eAaImIp3nP
odpružená	odpružený	k2eAgNnPc1d1
<g/>
,	,	kIx,
neoddělitelnou	oddělitelný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
převodovky	převodovka	k1gFnSc2
jsou	být	k5eAaImIp3nP
kliky	klik	k1gInPc4
s	s	k7c7
pedály	pedál	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Typickým	typický	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
mopedu	moped	k1gInSc2
je	být	k5eAaImIp3nS
Jawa	jawa	k1gFnSc1
Babetta	Babetta	k1gFnSc1
nebo	nebo	k8xC
Stadion	stadion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1
mopedy	moped	k1gInPc1
jsou	být	k5eAaImIp3nP
přirovnatelné	přirovnatelný	k2eAgFnPc1d1
ke	k	k7c3
skútrům	skútr	k1gInPc3
a	a	k8xC
existují	existovat	k5eAaImIp3nP
verze	verze	k1gFnPc1
určené	určený	k2eAgFnPc1d1
i	i	k9
pro	pro	k7c4
dvě	dva	k4xCgFnPc4
osoby	osoba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Elektrokolo	Elektrokola	k1gFnSc5
</s>
<s>
Skútr	skútr	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
moped	moped	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
moped	moped	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Mně	já	k3xPp1nSc3
všichni	všechen	k3xTgMnPc1
chodci	chodec	k1gMnPc1
závidí	závidět	k5eAaImIp3nP
(	(	kIx(
<g/>
Dobová	dobový	k2eAgFnSc1d1
československá	československý	k2eAgFnSc1d1
reklama	reklama	k1gFnSc1
na	na	k7c4
moped	moped	k1gInSc4
–	–	k?
video	video	k1gNnSc4
na	na	k7c4
youtube	youtubat	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Cyklistika	cyklistika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4123527-7	4123527-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85087186	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85087186	#num#	k4
</s>
