<s>
Teplo	teplo	k1gNnSc1	teplo
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pozměněném	pozměněný	k2eAgInSc6d1	pozměněný
smyslu	smysl	k1gInSc6	smysl
tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termodynamická	termodynamický	k2eAgFnSc1d1	termodynamická
veličina	veličina	k1gFnSc1	veličina
vyjadřující	vyjadřující	k2eAgFnSc4d1	vyjadřující
míru	míra	k1gFnSc4	míra
změny	změna	k1gFnSc2	změna
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
podstatou	podstata	k1gFnSc7	podstata
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
práce	práce	k1gFnSc1	práce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
A_	A_	k1gFnSc7	A_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
elementární	elementární	k2eAgFnPc4d1	elementární
práce	práce	k1gFnPc4	práce
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s hack="1">
rovna	roven	k2eAgFnSc1d1	rovna
obecné	obecný	k2eAgFnSc3d1	obecná
síle	síla	k1gFnSc3	síla
skalárně	skalárně	k6eAd1	skalárně
násobené	násobený	k2eAgFnSc2d1	násobená
obecným	obecný	k2eAgNnSc7d1	obecné
posunutím	posunutí	k1gNnSc7	posunutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k9	ani
tzv.	tzv.	kA	tzv.
chemická	chemický	k2eAgFnSc1d1	chemická
práce	práce	k1gFnSc1	práce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
N	N	kA	N
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc4d1	chemický
potenciál	potenciál	k1gInSc4	potenciál
krát	krát	k6eAd1	krát
změna	změna	k1gFnSc1	změna
množství	množství	k1gNnSc2	množství
látky	látka	k1gFnSc2	látka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k6eAd1	teplo
systém	systém	k1gInSc1	systém
vyměňuje	vyměňovat	k5eAaImIp3nS	vyměňovat
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
přijímá	přijímat	k5eAaImIp3nS	přijímat
nebo	nebo	k8xC	nebo
odevzdává	odevzdávat	k5eAaImIp3nS	odevzdávat
<g/>
)	)	kIx)	)
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
systémem	systém	k1gInSc7	systém
jiné	jiný	k2eAgFnSc2d1	jiná
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tepelném	tepelný	k2eAgInSc6d1	tepelný
styku	styk	k1gInSc6	styk
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
rozhraní	rozhraní	k1gNnSc1	rozhraní
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
je	být	k5eAaImIp3nS	být
diatermického	diatermický	k2eAgInSc2d1	diatermický
charakteru	charakter	k1gInSc2	charakter
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nepředstavuje	představovat	k5eNaImIp3nS	představovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
izolaci	izolace	k1gFnSc4	izolace
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
hovoříme	hovořit	k5eAaImIp1nP	hovořit
o	o	k7c6	o
tepelné	tepelný	k2eAgFnSc6d1	tepelná
výměně	výměna	k1gFnSc6	výměna
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
popisuje	popisovat	k5eAaImIp3nS	popisovat
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
spousta	spousta	k6eAd1	spousta
chaotických	chaotický	k2eAgFnPc2d1	chaotická
"	"	kIx"	"
<g/>
mikroprací	mikroprace	k1gFnPc2	mikroprace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
srážek	srážka	k1gFnPc2	srážka
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přímo	přímo	k6eAd1	přímo
nemůžeme	moct	k5eNaImIp1nP	moct
sledovat	sledovat	k5eAaImF	sledovat
ani	ani	k8xC	ani
měřit	měřit	k5eAaImF	měřit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
práci	práce	k1gFnSc6	práce
mluvíme	mluvit	k5eAaImIp1nP	mluvit
<g/>
,	,	kIx,	,
když	když	k8xS	když
způsobenou	způsobený	k2eAgFnSc4d1	způsobená
změnu	změna	k1gFnSc4	změna
energie	energie	k1gFnSc2	energie
můžeme	moct	k5eAaImIp1nP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
součin	součin	k1gInSc1	součin
veličin	veličina	k1gFnPc2	veličina
povahy	povaha	k1gFnSc2	povaha
síla	síla	k1gFnSc1	síla
krát	krát	k6eAd1	krát
posunutí	posunutí	k1gNnSc1	posunutí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
tlak	tlak	k1gInSc4	tlak
krát	krát	k6eAd1	krát
změna	změna	k1gFnSc1	změna
objemu	objem	k1gInSc2	objem
<g/>
,	,	kIx,	,
napětí	napětí	k1gNnSc1	napětí
krát	krát	k6eAd1	krát
přenesený	přenesený	k2eAgInSc4d1	přenesený
náboj	náboj	k1gInSc4	náboj
(	(	kIx(	(
<g/>
náboj	náboj	k1gInSc4	náboj
=	=	kIx~	=
proud	proud	k1gInSc4	proud
krát	krát	k6eAd1	krát
doba	doba	k1gFnSc1	doba
<g/>
)	)	kIx)	)
apod.	apod.	kA	apod.
U	u	k7c2	u
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
změna	změna	k1gFnSc1	změna
energie	energie	k1gFnSc2	energie
jako	jako	k8xS	jako
součin	součin	k1gInSc4	součin
jiných	jiný	k2eAgInPc2d1	jiný
přímo	přímo	k6eAd1	přímo
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
veličin	veličina	k1gFnPc2	veličina
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
nedá	dát	k5eNaPmIp3nS	dát
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
součin	součin	k1gInSc4	součin
teplota	teplota	k1gFnSc1	teplota
krát	krát	k6eAd1	krát
přírůstek	přírůstek	k1gInSc4	přírůstek
entropie	entropie	k1gFnSc2	entropie
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
je	být	k5eAaImIp3nS	být
dějovou	dějový	k2eAgFnSc7d1	dějová
fyzikální	fyzikální	k2eAgFnSc7d1	fyzikální
veličinou	veličina	k1gFnSc7	veličina
popisující	popisující	k2eAgInSc1d1	popisující
termodynamický	termodynamický	k2eAgInSc1d1	termodynamický
děj	děj	k1gInSc1	děj
(	(	kIx(	(
<g/>
posloupnost	posloupnost	k1gFnSc1	posloupnost
stavů	stav	k1gInPc2	stav
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
veličinou	veličina	k1gFnSc7	veličina
stavovou	stavový	k2eAgFnSc7d1	stavová
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgInSc1d1	popisující
stav	stav	k1gInSc1	stav
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
tepla	teplo	k1gNnSc2	teplo
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Měřením	měření	k1gNnSc7	měření
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kalorimetrie	kalorimetrie	k1gFnSc1	kalorimetrie
<g/>
;	;	kIx,	;
teplo	teplo	k1gNnSc1	teplo
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nS	měřit
kalorimetrem	kalorimetr	k1gInSc7	kalorimetr
<g/>
.	.	kIx.	.
</s>
<s>
Šířením	šíření	k1gNnSc7	šíření
tepla	teplo	k1gNnSc2	teplo
bez	bez	k7c2	bez
konání	konání	k1gNnSc2	konání
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
termokinetika	termokinetika	k1gFnSc1	termokinetika
<g/>
,	,	kIx,	,
tepelnými	tepelný	k2eAgInPc7d1	tepelný
ději	děj	k1gInPc7	děj
obecně	obecně	k6eAd1	obecně
termodynamika	termodynamika	k1gFnSc1	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kinetické	kinetický	k2eAgFnSc2d1	kinetická
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
při	při	k7c6	při
tepelné	tepelný	k2eAgFnSc6d1	tepelná
výměně	výměna	k1gFnSc6	výměna
předává	předávat	k5eAaImIp3nS	předávat
energie	energie	k1gFnSc1	energie
pohybu	pohyb	k1gInSc2	pohyb
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
jak	jak	k6eAd1	jak
systém	systém	k1gInSc1	systém
teplo	teplo	k6eAd1	teplo
odevzdávající	odevzdávající	k2eAgInSc1d1	odevzdávající
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
systém	systém	k1gInSc1	systém
teplo	teplo	k6eAd1	teplo
přijímající	přijímající	k2eAgInSc1d1	přijímající
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
neuspořádaně	uspořádaně	k6eNd1	uspořádaně
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
kondenzovaném	kondenzovaný	k2eAgInSc6d1	kondenzovaný
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
uvažovat	uvažovat	k5eAaImF	uvažovat
vedle	vedle	k7c2	vedle
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
částic	částice	k1gFnPc2	částice
i	i	k9	i
energii	energie	k1gFnSc4	energie
jejich	jejich	k3xOp3gFnPc2	jejich
vzájemných	vzájemný	k2eAgFnPc2d1	vzájemná
interakcí	interakce	k1gFnPc2	interakce
a	a	k8xC	a
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
výměna	výměna	k1gFnSc1	výměna
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
mění	měnit	k5eAaImIp3nS	měnit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
fáze	fáze	k1gFnSc1	fáze
látky	látka	k1gFnSc2	látka
-	-	kIx~	-
hovoříme	hovořit	k5eAaImIp1nP	hovořit
pak	pak	k6eAd1	pak
o	o	k7c6	o
latentním	latentní	k2eAgNnSc6d1	latentní
teple	teplo	k1gNnSc6	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
výměna	výměna	k1gFnSc1	výměna
přímo	přímo	k6eAd1	přímo
nesouvisí	souviset	k5eNaImIp3nS	souviset
s	s	k7c7	s
předáváním	předávání	k1gNnSc7	předávání
částic	částice	k1gFnPc2	částice
mezi	mezi	k7c7	mezi
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
změnou	změna	k1gFnSc7	změna
jejich	jejich	k3xOp3gFnSc2	jejich
chemické	chemický	k2eAgFnSc2d1	chemická
podstaty	podstata	k1gFnSc2	podstata
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
změnami	změna	k1gFnPc7	změna
pohybového	pohybový	k2eAgInSc2d1	pohybový
stavu	stav	k1gInSc2	stav
systémů	systém	k1gInPc2	systém
či	či	k8xC	či
"	"	kIx"	"
<g/>
vnější	vnější	k2eAgFnSc1d1	vnější
<g/>
"	"	kIx"	"
potenciální	potenciální	k2eAgFnSc1d1	potenciální
energie	energie	k1gFnSc1	energie
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
tepla	teplo	k1gNnSc2	teplo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
sice	sice	k8xC	sice
formálně	formálně	k6eAd1	formálně
ekvivalentní	ekvivalentní	k2eAgFnSc3d1	ekvivalentní
určité	určitý	k2eAgFnSc3d1	určitá
mechanické	mechanický	k2eAgFnSc3d1	mechanická
práci	práce	k1gFnSc3	práce
nebo	nebo	k8xC	nebo
kinetické	kinetický	k2eAgFnSc3d1	kinetická
energii	energie	k1gFnSc3	energie
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
vibrační	vibrační	k2eAgFnSc1d1	vibrační
<g/>
,	,	kIx,	,
translační	translační	k2eAgFnSc1d1	translační
<g/>
,	,	kIx,	,
rotační	rotační	k2eAgFnSc1d1	rotační
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
identické	identický	k2eAgInPc1d1	identický
a	a	k8xC	a
fyzikálně	fyzikálně	k6eAd1	fyzikálně
se	se	k3xPyFc4	se
od	od	k7c2	od
nich	on	k3xPp3gMnPc2	on
fundamentálně	fundamentálně	k6eAd1	fundamentálně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
zvláště	zvláště	k6eAd1	zvláště
názorně	názorně	k6eAd1	názorně
projevuje	projevovat	k5eAaImIp3nS	projevovat
ve	v	k7c6	v
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnPc1	definice
tepla	teplo	k1gNnSc2	teplo
však	však	k9	však
nevylučuje	vylučovat	k5eNaImIp3nS	vylučovat
tepelné	tepelný	k2eAgInPc4d1	tepelný
děje	děj	k1gInPc4	děj
při	při	k7c6	při
současném	současný	k2eAgNnSc6d1	současné
konání	konání	k1gNnSc6	konání
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
termodynamickým	termodynamický	k2eAgInSc7d1	termodynamický
zákonem	zákon	k1gInSc7	zákon
je	být	k5eAaImIp3nS	být
teplo	teplo	k1gNnSc1	teplo
(	(	kIx(	(
<g/>
systémem	systém	k1gInSc7	systém
přijaté	přijatý	k2eAgNnSc1d1	přijaté
<g/>
)	)	kIx)	)
při	při	k7c6	při
tepelné	tepelný	k2eAgFnSc6d1	tepelná
výměně	výměna	k1gFnSc6	výměna
rovno	roven	k2eAgNnSc4d1	rovno
změně	změna	k1gFnSc3	změna
(	(	kIx(	(
<g/>
zvýšení	zvýšení	k1gNnSc1	zvýšení
<g/>
)	)	kIx)	)
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
energie	energie	k1gFnSc1	energie
systému	systém	k1gInSc2	systém
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
o	o	k7c6	o
(	(	kIx(	(
<g/>
systémem	systém	k1gInSc7	systém
vykonanou	vykonaný	k2eAgFnSc4d1	vykonaná
<g/>
)	)	kIx)	)
práci	práce	k1gFnSc4	práce
(	(	kIx(	(
<g/>
kurzívou	kurzíva	k1gFnSc7	kurzíva
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
větě	věta	k1gFnSc6	věta
vyznačeno	vyznačit	k5eAaPmNgNnS	vyznačit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
daná	daný	k2eAgFnSc1d1	daná
změna	změna	k1gFnSc1	změna
bere	brát	k5eAaImIp3nS	brát
za	za	k7c7	za
kladnou	kladný	k2eAgFnSc7d1	kladná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přeměnu	přeměna	k1gFnSc4	přeměna
mechanické	mechanický	k2eAgFnSc2d1	mechanická
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
kinetická	kinetický	k2eAgFnSc1d1	kinetická
teorie	teorie	k1gFnSc1	teorie
jako	jako	k9	jako
přeměnu	přeměna	k1gFnSc4	přeměna
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
uspořádaného	uspořádaný	k2eAgInSc2d1	uspořádaný
pohybu	pohyb	k1gInSc2	pohyb
na	na	k7c4	na
kinetickou	kinetický	k2eAgFnSc4d1	kinetická
energii	energie	k1gFnSc4	energie
neuspořádaného	uspořádaný	k2eNgInSc2d1	neuspořádaný
pohybu	pohyb	k1gInSc2	pohyb
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikálně	fyzikálně	k6eAd1	fyzikálně
se	se	k3xPyFc4	se
fundamentální	fundamentální	k2eAgInSc1d1	fundamentální
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
neuspořádaným	uspořádaný	k2eNgMnSc7d1	neuspořádaný
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
uspořádaným	uspořádaný	k2eAgInSc7d1	uspořádaný
<g/>
"	"	kIx"	"
pohybem	pohyb	k1gInSc7	pohyb
částic	částice	k1gFnPc2	částice
projevuje	projevovat	k5eAaImIp3nS	projevovat
např.	např.	kA	např.
ve	v	k7c6	v
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tepelnému	tepelný	k2eAgInSc3d1	tepelný
pohybu	pohyb	k1gInSc3	pohyb
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
šum	šum	k1gInSc1	šum
<g/>
,	,	kIx,	,
charakterizovaný	charakterizovaný	k2eAgInSc1d1	charakterizovaný
určitou	určitý	k2eAgFnSc7d1	určitá
(	(	kIx(	(
<g/>
širokospektrální	širokospektrální	k2eAgFnSc7d1	širokospektrální
<g/>
)	)	kIx)	)
distribuční	distribuční	k2eAgFnSc7d1	distribuční
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
mechanickému	mechanický	k2eAgInSc3d1	mechanický
pohybu	pohyb	k1gInSc3	pohyb
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vibračnímu	vibrační	k2eAgInSc3d1	vibrační
<g/>
)	)	kIx)	)
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
určité	určitý	k2eAgFnPc4d1	určitá
ostré	ostrý	k2eAgFnPc4d1	ostrá
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
překonané	překonaný	k2eAgFnSc2d1	překonaná
fluidové	fluidový	k2eAgFnSc2d1	fluidová
teorie	teorie	k1gFnSc2	teorie
tepla	teplo	k1gNnSc2	teplo
byla	být	k5eAaImAgFnS	být
podstatou	podstata	k1gFnSc7	podstata
tepla	teplo	k1gNnSc2	teplo
substance	substance	k1gFnSc2	substance
-	-	kIx~	-
fluidum	fluidum	k1gNnSc1	fluidum
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgInPc1d1	zvaný
calor	calor	k1gInSc1	calor
<g/>
,	,	kIx,	,
calorique	calorique	k1gInSc1	calorique
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
teplo	teplo	k1gNnSc1	teplo
bylo	být	k5eAaImAgNnS	být
množství	množství	k1gNnSc4	množství
tohoto	tento	k3xDgNnSc2	tento
fluida	fluidum	k1gNnSc2	fluidum
v	v	k7c6	v
látce	látka	k1gFnSc6	látka
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
představa	představa	k1gFnSc1	představa
plně	plně	k6eAd1	plně
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
chemické	chemický	k2eAgFnPc4d1	chemická
reakce	reakce	k1gFnPc4	reakce
a	a	k8xC	a
nevyměňuje	vyměňovat	k5eNaImIp3nS	vyměňovat
se	se	k3xPyFc4	se
práce	práce	k1gFnSc1	práce
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
kalorimetrii	kalorimetrie	k1gFnSc6	kalorimetrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tepelné	tepelný	k2eAgInPc4d1	tepelný
stroje	stroj	k1gInPc4	stroj
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
jen	jen	k9	jen
kvalitativně	kvalitativně	k6eAd1	kvalitativně
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
kvantitativně	kvantitativně	k6eAd1	kvantitativně
<g/>
:	:	kIx,	:
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
koná	konat	k5eAaImIp3nS	konat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tepelné	tepelný	k2eAgNnSc1d1	tepelné
fluidum	fluidum	k1gNnSc1	fluidum
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
teplotou	teplota	k1gFnSc7	teplota
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
nižší	nízký	k2eAgFnSc7d2	nižší
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
může	moct	k5eAaImIp3nS	moct
konat	konat	k5eAaImF	konat
práci	práce	k1gFnSc4	práce
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
vyšší	vysoký	k2eAgFnSc2d2	vyšší
polohy	poloha	k1gFnSc2	poloha
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
fluidové	fluidový	k2eAgFnSc2d1	fluidová
teorie	teorie	k1gFnSc2	teorie
však	však	k9	však
přitom	přitom	k6eAd1	přitom
fluida	fluidum	k1gNnSc2	fluidum
neubývá	ubývat	k5eNaImIp3nS	ubývat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
měření	měření	k1gNnSc1	měření
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
fluida	fluidum	k1gNnPc1	fluidum
ubude	ubýt	k5eAaPmIp3nS	ubýt
<g/>
"	"	kIx"	"
právě	právě	k6eAd1	právě
tolik	tolik	k4xDc1	tolik
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
vykoná	vykonat	k5eAaPmIp3nS	vykonat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
první	první	k4xOgInSc1	první
zákon	zákon	k1gInSc1	zákon
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
vědomím	vědomí	k1gNnSc7	vědomí
je	být	k5eAaImIp3nS	být
fluidová	fluidový	k2eAgFnSc1d1	fluidová
teorie	teorie	k1gFnSc1	teorie
cenná	cenný	k2eAgFnSc1d1	cenná
i	i	k9	i
dnes	dnes	k6eAd1	dnes
svou	svůj	k3xOyFgFnSc7	svůj
názorností	názornost	k1gFnSc7	názornost
<g/>
.	.	kIx.	.
</s>
<s>
Názorně	názorně	k6eAd1	názorně
vystihuje	vystihovat	k5eAaImIp3nS	vystihovat
zejména	zejména	k9	zejména
přenos	přenos	k1gInSc1	přenos
tepla	teplo	k1gNnSc2	teplo
(	(	kIx(	(
<g/>
kalorimetrická	kalorimetrický	k2eAgFnSc1d1	kalorimetrická
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
roli	role	k1gFnSc4	role
Laplaceovy	Laplaceův	k2eAgFnPc1d1	Laplaceova
rovnice	rovnice	k1gFnPc1	rovnice
a	a	k8xC	a
Poissonovy	Poissonův	k2eAgFnPc1d1	Poissonova
rovnice	rovnice	k1gFnPc1	rovnice
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dějích	děj	k1gInPc6	děj
<g/>
,	,	kIx,	,
v	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
tekutin	tekutina	k1gFnPc2	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Zdůrazněme	zdůraznit	k5eAaPmRp1nP	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
teple	teplo	k1gNnSc6	teplo
i	i	k8xC	i
práci	práce	k1gFnSc6	práce
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
mluvit	mluvit	k5eAaImF	mluvit
zejména	zejména	k9	zejména
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
změnami	změna	k1gFnPc7	změna
těchto	tento	k3xDgFnPc2	tento
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
nikoli	nikoli	k9	nikoli
při	při	k7c6	při
popisu	popis	k1gInSc6	popis
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
smysl	smysl	k1gInSc1	smysl
tedy	tedy	k9	tedy
nemají	mít	k5eNaImIp3nP	mít
výroky	výrok	k1gInPc1	výrok
typu	typ	k1gInSc2	typ
"	"	kIx"	"
<g/>
Po	po	k7c6	po
zahřátí	zahřátí	k1gNnSc6	zahřátí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tělese	těleso	k1gNnSc6	těleso
více	hodně	k6eAd2	hodně
tepla	tepnout	k5eAaPmAgFnS	tepnout
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
správněji	správně	k6eAd2	správně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
těleso	těleso	k1gNnSc1	těleso
má	mít	k5eAaImIp3nS	mít
po	po	k7c6	po
zahřátí	zahřátí	k1gNnSc6	zahřátí
více	hodně	k6eAd2	hodně
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
pojem	pojem	k1gInSc1	pojem
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
tepla	teplo	k1gNnSc2	teplo
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
přesný	přesný	k2eAgInSc4d1	přesný
smysl	smysl	k1gInSc4	smysl
jen	jen	k9	jen
pro	pro	k7c4	pro
systém	systém	k1gInSc4	systém
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
<g/>
-li	i	k?	-li
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
nemusí	muset	k5eNaImIp3nS	muset
jít	jít	k5eAaImF	jít
např.	např.	kA	např.
definovat	definovat	k5eAaBmF	definovat
všude	všude	k6eAd1	všude
stejný	stejný	k2eAgInSc4d1	stejný
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ani	ani	k9	ani
práci	práce	k1gFnSc4	práce
p	p	k?	p
dV	dV	k?	dV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
i	i	k9	i
fyzikálně	fyzikálně	k6eAd1	fyzikálně
přesněji	přesně	k6eAd2	přesně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
teplo	teplo	k1gNnSc1	teplo
lze	lze	k6eAd1	lze
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
situaci	situace	k1gFnSc6	situace
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
úplný	úplný	k2eAgInSc4d1	úplný
diferenciál	diferenciál	k1gInSc4	diferenciál
(	(	kIx(	(
<g/>
totální	totální	k2eAgMnSc1d1	totální
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
tzv.	tzv.	kA	tzv.
parciální	parciální	k2eAgInSc4d1	parciální
diferenciál	diferenciál	k1gInSc4	diferenciál
<g/>
.	.	kIx.	.
</s>
<s>
Parciální	parciální	k2eAgInSc1d1	parciální
diferenciál	diferenciál	k1gInSc1	diferenciál
tepla	teplo	k1gNnSc2	teplo
lze	lze	k6eAd1	lze
převést	převést	k5eAaPmF	převést
v	v	k7c4	v
diferenciál	diferenciál	k1gInSc4	diferenciál
totální	totální	k2eAgInSc4d1	totální
pomocí	pomocí	k7c2	pomocí
vynásobení	vynásobení	k1gNnPc2	vynásobení
určitým	určitý	k2eAgNnSc7d1	určité
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
integračním	integrační	k2eAgInSc7d1	integrační
faktorem	faktor	k1gInSc7	faktor
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
2	[number]	k4	2
<g/>
.	.	kIx.	.
principu	princip	k1gInSc2	princip
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
tepla	teplo	k1gNnSc2	teplo
je	být	k5eAaImIp3nS	být
tímto	tento	k3xDgInSc7	tento
integračním	integrační	k2eAgInSc7d1	integrační
faktorem	faktor	k1gInSc7	faktor
reciproká	reciproký	k2eAgFnSc1d1	reciproká
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
veličiny	veličina	k1gFnPc4	veličina
<g/>
:	:	kIx,	:
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
změnu	změna	k1gFnSc4	změna
energie	energie	k1gFnSc2	energie
tělesa	těleso	k1gNnSc2	těleso
provedenou	provedený	k2eAgFnSc4d1	provedená
jistým	jistý	k2eAgInSc7d1	jistý
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
dějová	dějový	k2eAgFnSc1d1	dějová
veličina	veličina	k1gFnSc1	veličina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
stav	stav	k1gInSc4	stav
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
stavová	stavový	k2eAgFnSc1d1	stavová
veličina	veličina	k1gFnSc1	veličina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavovou	stavový	k2eAgFnSc7d1	stavová
veličinou	veličina	k1gFnSc7	veličina
popisující	popisující	k2eAgFnSc4d1	popisující
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
výměnu	výměna	k1gFnSc4	výměna
je	být	k5eAaImIp3nS	být
entropie	entropie	k1gFnSc1	entropie
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
Q	Q	kA	Q
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
jednotka	jednotka	k1gFnSc1	jednotka
SI	si	k1gNnSc2	si
<g/>
:	:	kIx,	:
joule	joule	k1gInSc1	joule
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
"	"	kIx"	"
<g/>
J	J	kA	J
<g/>
"	"	kIx"	"
Další	další	k2eAgFnPc1d1	další
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
viz	vidět	k5eAaImRp2nS	vidět
práce	práce	k1gFnSc2	práce
Znaménko	znaménko	k1gNnSc1	znaménko
hodnoty	hodnota	k1gFnSc2	hodnota
tepla	teplo	k1gNnSc2	teplo
označuje	označovat	k5eAaImIp3nS	označovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
teplo	teplo	k1gNnSc4	teplo
přijaté	přijatý	k2eAgNnSc4d1	přijaté
nebo	nebo	k8xC	nebo
vydané	vydaný	k2eAgNnSc4d1	vydané
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
brala	brát	k5eAaImAgFnS	brát
kladně	kladně	k6eAd1	kladně
odebraná	odebraný	k2eAgFnSc1d1	odebraná
práce	práce	k1gFnSc1	práce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
W	W	kA	W
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
W	W	kA	W
<g/>
}	}	kIx)	}
a	a	k8xC	a
dodané	dodaný	k2eAgNnSc4d1	dodané
teplo	teplo	k1gNnSc4	teplo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
z	z	k7c2	z
"	"	kIx"	"
<g/>
filosofie	filosofie	k1gFnSc1	filosofie
<g />
.	.	kIx.	.
</s>
<s hack="1">
parních	parní	k2eAgMnPc2d1	parní
strojů	stroj	k1gInPc2	stroj
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
cyklus	cyklus	k1gInSc4	cyklus
zněl	znět	k5eAaImAgInS	znět
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
W	W	kA	W
=	=	kIx~	=
Δ	Δ	k?	Δ
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
W	W	kA	W
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
<g/>
}	}	kIx)	}
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
při	při	k7c6	při
stlačení	stlačení	k1gNnSc6	stlačení
plynu	plynout	k5eAaImIp1nS	plynout
byla	být	k5eAaImAgFnS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
W	W	kA	W
=	=	kIx~	=
p	p	k?	p
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
W	W	kA	W
<g/>
=	=	kIx~	=
<g/>
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Moderněji	moderně	k6eAd2	moderně
se	se	k3xPyFc4	se
bere	brát	k5eAaImIp3nS	brát
kladně	kladně	k6eAd1	kladně
veškerá	veškerý	k3xTgFnSc1	veškerý
dodaná	dodaný	k2eAgFnSc1d1	dodaná
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
možnost	možnost	k1gFnSc1	možnost
např.	např.	kA	např.
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
přenos	přenos	k1gInSc1	přenos
energie	energie	k1gFnSc2	energie
obojí	oboj	k1gFnPc2	oboj
charakter	charakter	k1gInSc1	charakter
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
polarizovaným	polarizovaný	k2eAgNnSc7d1	polarizované
světlem	světlo	k1gNnSc7	světlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
zní	znět	k5eAaImIp3nS	znět
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
W	W	kA	W
+	+	kIx~	+
Δ	Δ	k?	Δ
Q	Q	kA	Q
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
W	W	kA	W
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
,	,	kIx,	,
práce	práce	k1gFnSc1	práce
při	při	k7c6	při
stlačení	stlačení	k1gNnSc6	stlačení
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
W	W	kA	W
=	=	kIx~	=
-	-	kIx~	-
p	p	k?	p
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
W	W	kA	W
<g/>
=	=	kIx~	=
<g/>
-p	-p	k?	-p
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
tepla	teplo	k1gNnSc2	teplo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
}	}	kIx)	}
přijatého	přijatý	k2eAgMnSc2d1	přijatý
fyzikálně	fyzikálně	k6eAd1	fyzikálně
(	(	kIx(	(
<g/>
skupenství	skupenství	k1gNnSc4	skupenství
<g/>
)	)	kIx)	)
a	a	k8xC	a
chemicky	chemicky	k6eAd1	chemicky
homogenním	homogenní	k2eAgInSc7d1	homogenní
systémem	systém	k1gInSc7	systém
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
vzrůstem	vzrůst	k1gInSc7	vzrůst
teploty	teplota	k1gFnSc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
}	}	kIx)	}
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
Q	Q	kA	Q
=	=	kIx~	=
m	m	kA	m
c	c	k0	c
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
Q	Q	kA	Q
<g/>
=	=	kIx~	=
<g/>
mc	mc	k?	mc
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g />
.	.	kIx.	.
</s>
<s hack="1">
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
počáteční	počáteční	k2eAgFnSc2d1	počáteční
teploty	teplota	k1gFnSc2	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
T	T	kA	T
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnSc6	T_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
koncové	koncový	k2eAgFnPc1d1	koncová
teploty	teplota	k1gFnPc1	teplota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
T_	T_	k1gMnSc6	T_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
T_	T_	k1gMnSc1	T_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-T_	-T_	k?	-T_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
záviset	záviset	k5eAaImF	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
vztah	vztah	k1gInSc1	vztah
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
diferenciálním	diferenciální	k2eAgInSc6d1	diferenciální
tvaru	tvar	k1gInSc6	tvar
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
Q	Q	kA	Q
=	=	kIx~	=
m	m	kA	m
c	c	k0	c
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
<g/>
=	=	kIx~	=
<g/>
mc	mc	k?	mc
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
δ	δ	k?	δ
Q	Q	kA	Q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
delta	delta	k1gFnSc1	delta
Q	Q	kA	Q
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
značí	značit	k5eAaImIp3nS	značit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
tepla	teplo	k1gNnSc2	teplo
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
totální	totální	k2eAgInSc4d1	totální
diferenciál	diferenciál	k1gInSc4	diferenciál
<g/>
.	.	kIx.	.
</s>
<s>
Měřením	měření	k1gNnSc7	měření
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
kalorimetrie	kalorimetrie	k1gFnSc1	kalorimetrie
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
kalorimetrických	kalorimetrický	k2eAgFnPc2d1	kalorimetrická
úvah	úvaha	k1gFnPc2	úvaha
je	být	k5eAaImIp3nS	být
zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
znění	znění	k1gNnSc4	znění
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
tzv.	tzv.	kA	tzv.
kalorimetrická	kalorimetrický	k2eAgFnSc1d1	kalorimetrická
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
potřebné	potřebný	k2eAgNnSc1d1	potřebné
k	k	k7c3	k
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
předmětu	předmět	k1gInSc3	předmět
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
teplotní	teplotní	k2eAgInSc4d1	teplotní
stupeň	stupeň	k1gInSc4	stupeň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
C	C	kA	C
tohoto	tento	k3xDgInSc2	tento
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
potřebné	potřebný	k2eAgNnSc1d1	potřebné
k	k	k7c3	k
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
jednoho	jeden	k4xCgInSc2	jeden
kilogramu	kilogram	k1gInSc2	kilogram
látky	látka	k1gFnSc2	látka
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
teplotní	teplotní	k2eAgInSc4d1	teplotní
stupeň	stupeň	k1gInSc4	stupeň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
(	(	kIx(	(
<g/>
měrné	měrný	k2eAgNnSc1d1	měrné
teplo	teplo	k1gNnSc1	teplo
<g/>
)	)	kIx)	)
c	c	k0	c
této	tento	k3xDgFnSc2	tento
láky	láka	k1gFnSc2	láka
<g/>
.	.	kIx.	.
</s>
<s>
Teplo	teplo	k1gNnSc1	teplo
potřebné	potřebný	k2eAgNnSc1d1	potřebné
k	k	k7c3	k
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
jednoho	jeden	k4xCgInSc2	jeden
molu	mol	k1gInSc2	mol
látky	látka	k1gFnSc2	látka
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
teplotní	teplotní	k2eAgInSc4d1	teplotní
stupeň	stupeň	k1gInSc4	stupeň
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
molární	molární	k2eAgFnSc1d1	molární
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
(	(	kIx(	(
<g/>
molární	molární	k2eAgNnSc1d1	molární
teplo	teplo	k1gNnSc1	teplo
<g/>
)	)	kIx)	)
této	tento	k3xDgFnSc2	tento
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
ještě	ještě	k6eAd1	ještě
upřesnit	upřesnit	k5eAaPmF	upřesnit
děj	děj	k1gInSc4	děj
<g/>
,	,	kIx,	,
při	při	k7c6	při
jakém	jaký	k3yQgInSc6	jaký
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
měrná	měrný	k2eAgFnSc1d1	měrná
tepelná	tepelný	k2eAgFnSc1d1	tepelná
kapacita	kapacita	k1gFnSc1	kapacita
plynu	plyn	k1gInSc2	plyn
za	za	k7c2	za
stálého	stálý	k2eAgInSc2d1	stálý
objemu	objem	k1gInSc2	objem
cV	cV	k?	cV
či	či	k8xC	či
za	za	k7c2	za
stálého	stálý	k2eAgInSc2d1	stálý
tlaku	tlak	k1gInSc2	tlak
cp	cp	k?	cp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šíření	šíření	k1gNnSc4	šíření
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
z	z	k7c2	z
jednoho	jeden	k4xCgNnSc2	jeden
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
vedením	vedení	k1gNnSc7	vedení
<g/>
,	,	kIx,	,
prouděním	proudění	k1gNnSc7	proudění
nebo	nebo	k8xC	nebo
zářením	záření	k1gNnSc7	záření
(	(	kIx(	(
<g/>
sáláním	sálání	k1gNnSc7	sálání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
teplo	teplo	k6eAd1	teplo
popisovala	popisovat	k5eAaImAgFnS	popisovat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
flogistonová	flogistonový	k2eAgFnSc1d1	flogistonová
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pak	pak	k6eAd1	pak
kalorická	kalorický	k2eAgFnSc1d1	kalorická
teorie	teorie	k1gFnSc1	teorie
tvrdící	tvrdící	k2eAgFnSc1d1	tvrdící
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
tekutinu	tekutina	k1gFnSc4	tekutina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgNnPc1	ten
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
mechanistickou	mechanistický	k2eAgFnSc7d1	mechanistická
teorií	teorie	k1gFnSc7	teorie
<g/>
.	.	kIx.	.
</s>
<s>
Termodynamika	termodynamika	k1gFnSc1	termodynamika
Energie	energie	k1gFnSc2	energie
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
teplo	teplo	k6eAd1	teplo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
teplo	teplo	k1gNnSc4	teplo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
