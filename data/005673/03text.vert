<s>
Speleologie	speleologie	k1gFnSc1	speleologie
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
jeskyňářství	jeskyňářství	k1gNnSc1	jeskyňářství
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgFnSc1d1	vědní
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
výzkumem	výzkum	k1gInSc7	výzkum
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
podoborů	podobor	k1gInPc2	podobor
karsologie	karsologie	k1gFnSc2	karsologie
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
komplexním	komplexní	k2eAgInSc7d1	komplexní
výzkumem	výzkum	k1gInSc7	výzkum
krasu	kras	k1gInSc2	kras
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
termín	termín	k1gInSc1	termín
speleologie	speleologie	k1gFnSc2	speleologie
nebo	nebo	k8xC	nebo
jeskyňářství	jeskyňářství	k1gNnSc2	jeskyňářství
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
zájmovou	zájmový	k2eAgFnSc4d1	zájmová
a	a	k8xC	a
sportovní	sportovní	k2eAgFnSc4d1	sportovní
činnost	činnost	k1gFnSc4	činnost
provozovanou	provozovaný	k2eAgFnSc7d1	provozovaná
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
podzemních	podzemní	k2eAgFnPc6d1	podzemní
prostorách	prostora	k1gFnPc6	prostora
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
speleoalpinismus	speleoalpinismus	k1gInSc1	speleoalpinismus
<g/>
.	.	kIx.	.
</s>
