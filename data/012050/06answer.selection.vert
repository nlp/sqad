<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
byla	být	k5eAaImAgFnS	být
Praha	Praha	k1gFnSc1	Praha
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
českou	český	k2eAgFnSc7d1	Česká
kněžnou	kněžna	k1gFnSc7	kněžna
a	a	k8xC	a
věštkyní	věštkyně	k1gFnSc7	věštkyně
Libuší	Libuše	k1gFnPc2	Libuše
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
Přemyslem	Přemysl	k1gMnSc7	Přemysl
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
