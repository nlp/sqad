<s>
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
česko-francouzského	českorancouzský	k2eAgMnSc2d1	česko-francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Milana	Milan	k1gMnSc2	Milan
Kundery	Kundera	k1gFnSc2	Kundera
<g/>
.	.	kIx.	.
</s>
<s>
Milan	Milan	k1gMnSc1	Milan
Kundera	Kundero	k1gNnSc2	Kundero
napsal	napsat	k5eAaBmAgMnS	napsat
svůj	svůj	k3xOyFgInSc4	svůj
sedmý	sedmý	k4xOgInSc4	sedmý
román	román	k1gInSc4	román
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
až	až	k9	až
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
česky	česky	k6eAd1	česky
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
překládán	překládat	k5eAaImNgInS	překládat
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
během	během	k7c2	během
překladů	překlad	k1gInPc2	překlad
však	však	k9	však
autor	autor	k1gMnSc1	autor
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
i	i	k9	i
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
Kundera	Kunder	k1gMnSc4	Kunder
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc1	román
psal	psát	k5eAaImAgInS	psát
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
pohodě	pohoda	k1gFnSc6	pohoda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
překladům	překlad	k1gInPc3	překlad
věnoval	věnovat	k5eAaImAgMnS	věnovat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
v	v	k7c6	v
únavě	únava	k1gFnSc6	únava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
francouzsky	francouzsky	k6eAd1	francouzsky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
pod	pod	k7c7	pod
francouzským	francouzský	k2eAgInSc7d1	francouzský
titulem	titul	k1gInSc7	titul
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Immortalité	Immortalitý	k2eAgFnPc1d1	Immortalitý
<g/>
.	.	kIx.	.
</s>
<s>
Česky	česky	k6eAd1	česky
byla	být	k5eAaImAgFnS	být
kniha	kniha	k1gFnSc1	kniha
poprvé	poprvé	k6eAd1	poprvé
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poslední	poslední	k2eAgFnSc4d1	poslední
Kunderovu	Kunderův	k2eAgFnSc4d1	Kunderova
knihu	kniha	k1gFnSc4	kniha
napsanou	napsaný	k2eAgFnSc4d1	napsaná
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
román	román	k1gInSc1	román
La	la	k1gNnSc2	la
Lenteur	Lenteura	k1gFnPc2	Lenteura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Pomalost	pomalost	k1gFnSc1	pomalost
<g/>
)	)	kIx)	)
a	a	k8xC	a
všechna	všechen	k3xTgNnPc4	všechen
další	další	k2eAgNnPc4d1	další
díla	dílo	k1gNnPc4	dílo
již	již	k9	již
byla	být	k5eAaImAgFnS	být
psána	psát	k5eAaImNgFnS	psát
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
Kunderových	Kunderových	k2eAgFnSc1d1	Kunderových
prací	práce	k1gFnSc7	práce
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
kapitol	kapitola	k1gFnPc2	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
jeho	jeho	k3xOp3gFnPc2	jeho
próz	próza	k1gFnPc2	próza
je	být	k5eAaImIp3nS	být
však	však	k9	však
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
"	"	kIx"	"
<g/>
francouzský	francouzský	k2eAgMnSc1d1	francouzský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žádná	žádný	k3yNgFnSc1	žádný
Češka	Češka	k1gFnSc1	Češka
ani	ani	k8xC	ani
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
motiv	motiv	k1gInSc1	motiv
emigrace	emigrace	k1gFnSc2	emigrace
či	či	k8xC	či
české	český	k2eAgFnSc2d1	Česká
komunistické	komunistický	k2eAgFnSc2d1	komunistická
zkušenosti	zkušenost	k1gFnSc2	zkušenost
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
důležitých	důležitý	k2eAgFnPc2d1	důležitá
českých	český	k2eAgFnPc2d1	Česká
reminiscencí	reminiscence	k1gFnPc2	reminiscence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
Kunderovým	Kunderův	k2eAgInSc7d1	Kunderův
mladším	mladý	k2eAgInSc7d2	mladší
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
románem	román	k1gInSc7	román
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Identité	Identitý	k2eAgFnPc1d1	Identitý
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Totožnost	totožnost	k1gFnSc1	totožnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
románového	románový	k2eAgNnSc2d1	románové
vyprávění	vyprávění	k1gNnSc2	vyprávění
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
kapitolách	kapitola	k1gFnPc6	kapitola
vypravěč-autor	vypravěčutor	k1gInSc1	vypravěč-autor
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
autorské	autorský	k2eAgFnPc1d1	autorská
reflexe	reflexe	k1gFnPc1	reflexe
nejsou	být	k5eNaImIp3nP	být
Kunderově	Kunderův	k2eAgNnSc6d1	Kunderovo
díle	dílo	k1gNnSc6	dílo
novým	nový	k2eAgInSc7d1	nový
jevem	jev	k1gInSc7	jev
(	(	kIx(	(
<g/>
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
smíchu	smích	k1gInSc2	smích
a	a	k8xC	a
zapomnění	zapomnění	k1gNnSc2	zapomnění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nabývají	nabývat	k5eAaImIp3nP	nabývat
však	však	k9	však
zde	zde	k6eAd1	zde
nebývalého	bývalý	k2eNgInSc2d1	bývalý
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
jakési	jakýsi	k3yIgFnSc6	jakýsi
hranici	hranice	k1gFnSc6	hranice
fikce	fikce	k1gFnSc2	fikce
a	a	k8xC	a
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nám	my	k3xPp1nPc3	my
autor	autor	k1gMnSc1	autor
dokládá	dokládat	k5eAaImIp3nS	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
děj	děj	k1gInSc1	děj
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeho	on	k3xPp3gInSc4	on
výmysl	výmysl	k1gInSc4	výmysl
a	a	k8xC	a
postavy	postava	k1gFnPc4	postava
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ze	z	k7c2	z
spisovatelových	spisovatelův	k2eAgFnPc2d1	spisovatelova
metafor	metafora	k1gFnPc2	metafora
<g/>
,	,	kIx,	,
či	či	k8xC	či
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
vplétá	vplétat	k5eAaImIp3nS	vplétat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c6	na
setkání	setkání	k1gNnSc6	setkání
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
Avenariem	Avenarium	k1gNnSc7	Avenarium
i	i	k8xC	i
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
zhmotněnými	zhmotněný	k2eAgFnPc7d1	zhmotněná
postavami	postava	k1gFnPc7	postava
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Postavu	postava	k1gFnSc4	postava
autor-Kundera	autor-Kundero	k1gNnSc2	autor-Kundero
je	být	k5eAaImIp3nS	být
však	však	k9	však
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
možno	možno	k6eAd1	možno
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
fiktivní	fiktivní	k2eAgFnSc4d1	fiktivní
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vypravěč	vypravěč	k1gMnSc1	vypravěč
nevykazuje	vykazovat	k5eNaImIp3nS	vykazovat
téměř	téměř	k6eAd1	téměř
žádné	žádný	k3yNgInPc4	žádný
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
rysy	rys	k1gInPc4	rys
a	a	k8xC	a
omezuje	omezovat	k5eAaImIp3nS	omezovat
se	se	k3xPyFc4	se
na	na	k7c4	na
pozorovatele	pozorovatel	k1gMnSc4	pozorovatel
a	a	k8xC	a
komentátora	komentátor	k1gMnSc4	komentátor
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
postavou	postava	k1gFnSc7	postava
vypravěče	vypravěč	k1gMnSc2	vypravěč
myslel	myslet	k5eAaImAgMnS	myslet
sebe	sebe	k3xPyFc4	sebe
-	-	kIx~	-
několikrát	několikrát	k6eAd1	několikrát
tam	tam	k6eAd1	tam
padne	padnout	k5eAaImIp3nS	padnout
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pro	pro	k7c4	pro
román	román	k1gInSc4	román
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgMnSc1d1	typický
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
jiných	jiný	k2eAgFnPc2d1	jiná
Kunderových	Kunderův	k2eAgFnPc2d1	Kunderova
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
filozoficko-estetický	filozofickostetický	k2eAgInSc4d1	filozoficko-estetický
přesah	přesah	k1gInSc4	přesah
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c4	o
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
jejím	její	k3xOp3gInSc6	její
vztahu	vztah	k1gInSc6	vztah
pro	pro	k7c4	pro
současného	současný	k2eAgMnSc4d1	současný
člověka	člověk	k1gMnSc4	člověk
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Paula	Paul	k1gMnSc4	Paul
<g/>
,	,	kIx,	,
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
románových	románový	k2eAgFnPc2d1	románová
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
Hemingwayovy	Hemingwayův	k2eAgFnPc1d1	Hemingwayova
biografie	biografie	k1gFnPc1	biografie
důležitější	důležitý	k2eAgFnPc1d2	důležitější
než	než	k8xS	než
jeho	jeho	k3xOp3gFnPc1	jeho
vlastní	vlastní	k2eAgFnPc1d1	vlastní
literární	literární	k2eAgFnPc1d1	literární
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kundera	Kundera	k1gFnSc1	Kundera
otevírá	otevírat	k5eAaImIp3nS	otevírat
téma	téma	k1gNnSc4	téma
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
imagologie	imagologie	k1gFnSc1	imagologie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
disciplíny	disciplína	k1gFnSc2	disciplína
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vytvářením	vytváření	k1gNnSc7	vytváření
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
v	v	k7c6	v
reklamě	reklama	k1gFnSc6	reklama
<g/>
,	,	kIx,	,
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
denním	denní	k2eAgInSc6d1	denní
životě	život	k1gInSc6	život
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nahradila	nahradit	k5eAaPmAgFnS	nahradit
dřívější	dřívější	k2eAgFnSc1d1	dřívější
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
již	již	k6eAd1	již
na	na	k7c4	na
lidi	člověk	k1gMnPc4	člověk
přestala	přestat	k5eAaPmAgFnS	přestat
působit	působit	k5eAaImF	působit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
srovnání	srovnání	k1gNnSc3	srovnání
ideologie	ideologie	k1gFnSc2	ideologie
a	a	k8xC	a
imagologie	imagologie	k1gFnSc2	imagologie
chci	chtít	k5eAaImIp1nS	chtít
dodat	dodat	k5eAaPmF	dodat
ještě	ještě	k9	ještě
toto	tento	k3xDgNnSc4	tento
<g/>
:	:	kIx,	:
Ideologie	ideologie	k1gFnPc1	ideologie
byly	být	k5eAaImAgFnP	být
jako	jako	k9	jako
obrovská	obrovský	k2eAgNnPc4d1	obrovské
kola	kolo	k1gNnPc4	kolo
v	v	k7c6	v
zákulisí	zákulisí	k1gNnSc6	zákulisí
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
točila	točit	k5eAaImAgFnS	točit
a	a	k8xC	a
uváděla	uvádět	k5eAaImAgFnS	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
reformy	reforma	k1gFnSc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Imagologická	Imagologický	k2eAgNnPc1d1	Imagologický
kola	kolo	k1gNnPc1	kolo
se	se	k3xPyFc4	se
otáčejí	otáčet	k5eAaImIp3nP	otáčet
a	a	k8xC	a
na	na	k7c6	na
historii	historie	k1gFnSc6	historie
to	ten	k3xDgNnSc1	ten
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Ideologie	ideologie	k1gFnPc1	ideologie
válčily	válčit	k5eAaImAgFnP	válčit
jedna	jeden	k4xCgFnSc1	jeden
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byla	být	k5eAaImAgFnS	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
naplnit	naplnit	k5eAaPmF	naplnit
svým	svůj	k3xOyFgNnSc7	svůj
myšlením	myšlení	k1gNnSc7	myšlení
celou	celý	k2eAgFnSc4d1	celá
epochu	epocha	k1gFnSc4	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Imagologie	Imagologie	k1gFnSc1	Imagologie
organizuje	organizovat	k5eAaBmIp3nS	organizovat
sama	sám	k3xTgMnSc4	sám
mírumilovné	mírumilovný	k2eAgNnSc1d1	mírumilovné
střídání	střídání	k1gNnSc1	střídání
svých	svůj	k3xOyFgMnPc2	svůj
systémů	systém	k1gInPc2	systém
ve	v	k7c6	v
svižném	svižný	k2eAgInSc6d1	svižný
rytmu	rytmus	k1gInSc6	rytmus
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Řečeno	říct	k5eAaPmNgNnS	říct
Paulovými	Paulův	k2eAgNnPc7d1	Paulovo
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
Ideologie	ideologie	k1gFnPc1	ideologie
patřily	patřit	k5eAaImAgFnP	patřit
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
vláda	vláda	k1gFnSc1	vláda
imagologie	imagologie	k1gFnSc2	imagologie
začíná	začínat	k5eAaImIp3nS	začínat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
historie	historie	k1gFnSc1	historie
končí	končit	k5eAaImIp3nS	končit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Milan	Milan	k1gMnSc1	Milan
Kundera	Kunder	k1gMnSc2	Kunder
<g/>
:	:	kIx,	:
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
<g/>
,	,	kIx,	,
s.	s.	k?	s.
119	[number]	k4	119
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Imagologové	Imagolog	k1gMnPc1	Imagolog
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nám	my	k3xPp1nPc3	my
radí	radit	k5eAaImIp3nS	radit
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
"	"	kIx"	"
<g/>
in	in	k?	in
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgNnSc1d1	vhodné
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
předmětem	předmět	k1gInSc7	předmět
Kunderových	Kunderův	k2eAgFnPc2d1	Kunderova
úvah	úvaha	k1gFnPc2	úvaha
je	být	k5eAaImIp3nS	být
otázka	otázka	k1gFnSc1	otázka
přítomnosti	přítomnost	k1gFnSc2	přítomnost
zemřivšího	zemřivší	k2eAgMnSc4d1	zemřivší
člověka	člověk	k1gMnSc4	člověk
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
živých	živý	k2eAgFnPc2d1	živá
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jeho	jeho	k3xOp3gInSc2	jeho
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
mezi	mezi	k7c7	mezi
živými	živý	k2eAgFnPc7d1	živá
(	(	kIx(	(
<g/>
motiv	motiv	k1gInSc4	motiv
Bettiny	Bettina	k1gFnSc2	Bettina
von	von	k1gInSc1	von
Arnim	Arnim	k1gInSc1	Arnim
<g/>
,	,	kIx,	,
Johanna	Johanna	k1gFnSc1	Johanna
Wolfganga	Wolfgang	k1gMnSc2	Wolfgang
Goetha	Goeth	k1gMnSc2	Goeth
a	a	k8xC	a
Laury	Laura	k1gFnSc2	Laura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
duchovní	duchovní	k2eAgFnSc1d1	duchovní
linie	linie	k1gFnSc1	linie
dala	dát	k5eAaPmAgFnS	dát
knize	kniha	k1gFnSc6	kniha
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelností	nesmrtelnost	k1gFnSc7	nesmrtelnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
myšlen	myšlen	k2eAgInSc4d1	myšlen
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
obrazu	obraz	k1gInSc2	obraz
mrtvého	mrtvý	k2eAgInSc2d1	mrtvý
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
živých	živá	k1gFnPc2	živá
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
malá	malý	k2eAgFnSc1d1	malá
(	(	kIx(	(
<g/>
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
rodiny	rodina	k1gFnSc2	rodina
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
velká	velký	k2eAgNnPc1d1	velké
(	(	kIx(	(
<g/>
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
neznáme	neznat	k5eAaImIp1nP	neznat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
Kundera	Kundera	k1gFnSc1	Kundera
vyzdvihuje	vyzdvihovat	k5eAaImIp3nS	vyzdvihovat
jejich	jejich	k3xOp3gFnSc4	jejich
komplikovanou	komplikovaný	k2eAgFnSc4d1	komplikovaná
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
tedy	tedy	k9	tedy
nesnadnost	nesnadnost	k1gFnSc4	nesnadnost
jejich	jejich	k3xOp3gNnSc2	jejich
převyprávění	převyprávění	k1gNnSc2	převyprávění
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
filmové	filmový	k2eAgFnSc2d1	filmová
či	či	k8xC	či
jiné	jiný	k2eAgFnSc2d1	jiná
adaptace	adaptace	k1gFnSc2	adaptace
(	(	kIx(	(
<g/>
ostatně	ostatně	k6eAd1	ostatně
jako	jako	k8xC	jako
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
jeho	on	k3xPp3gMnSc2	on
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
filmová	filmový	k2eAgFnSc1d1	filmová
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
či	či	k8xC	či
divadelní	divadelní	k2eAgFnSc1d1	divadelní
adaptace	adaptace	k1gFnSc1	adaptace
zakázána	zakázán	k2eAgFnSc1d1	zakázána
-	-	kIx~	-
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nS	tvořit
Žert	žert	k1gInSc1	žert
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
<g/>
letech	léto	k1gNnPc6	léto
zfilmován	zfilmovat	k5eAaPmNgInS	zfilmovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
dějová	dějový	k2eAgFnSc1d1	dějová
linka	linka	k1gFnSc1	linka
představuje	představovat	k5eAaImIp3nS	představovat
Agnes	Agnes	k1gInSc4	Agnes
<g/>
,	,	kIx,	,
Francouzku	Francouzka	k1gFnSc4	Francouzka
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnSc4d1	žijící
v	v	k7c6	v
harmonickém	harmonický	k2eAgInSc6d1	harmonický
a	a	k8xC	a
přiměřeně	přiměřeně	k6eAd1	přiměřeně
šťastném	šťastný	k2eAgNnSc6d1	šťastné
manželství	manželství	k1gNnSc6	manželství
s	s	k7c7	s
advokátem	advokát	k1gMnSc7	advokát
Paulem	Paul	k1gMnSc7	Paul
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Samotná	samotný	k2eAgFnSc1d1	samotná
postava	postava	k1gFnSc1	postava
Agnes	Agnesa	k1gFnPc2	Agnesa
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
Kunderovy	Kunderův	k2eAgFnSc2d1	Kunderova
vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
na	na	k7c4	na
starší	starý	k2eAgFnSc4d2	starší
ženu	žena	k1gFnSc4	žena
u	u	k7c2	u
bazénu	bazén	k1gInSc2	bazén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Agnes	Agnes	k1gInSc1	Agnes
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
tísnivém	tísnivý	k2eAgInSc6d1	tísnivý
strachu	strach	k1gInSc6	strach
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
prostoupeného	prostoupený	k2eAgInSc2d1	prostoupený
imagologií	imagologie	k1gFnSc7	imagologie
<g/>
,	,	kIx,	,
ve	v	k7c6	v
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
nevědomého	vědomý	k2eNgInSc2d1	nevědomý
otisku	otisk	k1gInSc2	otisk
vlastní	vlastní	k2eAgFnSc2d1	vlastní
tváře	tvář	k1gFnSc2	tvář
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
nevědomě	vědomě	k6eNd1	vědomě
fotografovány	fotografován	k2eAgFnPc4d1	fotografována
celebrity	celebrita	k1gFnPc4	celebrita
a	a	k8xC	a
otiskovány	otiskován	k2eAgFnPc4d1	otiskována
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
touží	toužit	k5eAaImIp3nP	toužit
svůj	svůj	k3xOyFgInSc4	svůj
obraz	obraz	k1gInSc4	obraz
skrýt	skrýt	k5eAaPmF	skrýt
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
okolím	okolí	k1gNnSc7	okolí
i	i	k8xC	i
před	před	k7c7	před
budoucností	budoucnost	k1gFnSc7	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
její	její	k3xOp3gFnSc1	její
vášnivá	vášnivý	k2eAgFnSc1d1	vášnivá
a	a	k8xC	a
emocionální	emocionální	k2eAgFnSc1d1	emocionální
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Laura	Laura	k1gFnSc1	Laura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ji	on	k3xPp3gFnSc4	on
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
svou	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
stejně	stejně	k6eAd1	stejně
vehementně	vehementně	k6eAd1	vehementně
jako	jako	k8xS	jako
demonstruje	demonstrovat	k5eAaBmIp3nS	demonstrovat
nespravedlnost	nespravedlnost	k1gFnSc4	nespravedlnost
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ona	onen	k3xDgFnSc1	onen
ta	ten	k3xDgFnSc1	ten
"	"	kIx"	"
<g/>
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
"	"	kIx"	"
a	a	k8xC	a
Agnes	Agnes	k1gInSc4	Agnes
ta	ten	k3xDgFnSc1	ten
"	"	kIx"	"
<g/>
šťastná	šťastný	k2eAgFnSc1d1	šťastná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Laura	Laura	k1gFnSc1	Laura
naopak	naopak	k6eAd1	naopak
chce	chtít	k5eAaImIp3nS	chtít
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
své	svůj	k3xOyFgFnSc2	svůj
rodiny	rodina	k1gFnSc2	rodina
zůstat	zůstat	k5eAaPmF	zůstat
navždy	navždy	k6eAd1	navždy
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
nesmrtelnou	smrtelný	k2eNgFnSc4d1	nesmrtelná
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
volí	volit	k5eAaImIp3nS	volit
prostředky	prostředek	k1gInPc4	prostředek
jako	jako	k8xC	jako
citové	citový	k2eAgNnSc4d1	citové
vydírání	vydírání	k1gNnSc4	vydírání
či	či	k8xC	či
výhrůžky	výhrůžka	k1gFnSc2	výhrůžka
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
rozhoří	rozhořet	k5eAaPmIp3nS	rozhořet
<g/>
,	,	kIx,	,
když	když	k8xS	když
Laura	Laura	k1gFnSc1	Laura
začne	začít	k5eAaPmIp3nS	začít
bojovat	bojovat	k5eAaImF	bojovat
o	o	k7c4	o
Agnesina	Agnesin	k2eAgMnSc4d1	Agnesin
muže	muž	k1gMnSc4	muž
<g/>
.	.	kIx.	.
</s>
<s>
Agnes	Agnes	k1gInSc1	Agnes
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nemá	mít	k5eNaImIp3nS	mít
sílu	síla	k1gFnSc4	síla
ani	ani	k8xC	ani
chuť	chuť	k1gFnSc4	chuť
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
vítá	vítat	k5eAaImIp3nS	vítat
náhodu	náhoda	k1gFnSc4	náhoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jí	on	k3xPp3gFnSc3	on
připraví	připravit	k5eAaPmIp3nS	připravit
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
a	a	k8xC	a
šťastna	šťasten	k2eAgFnSc1d1	šťastna
odchází	odcházet	k5eAaImIp3nS	odcházet
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
ohrožuje	ohrožovat	k5eAaImIp3nS	ohrožovat
agresivní	agresivní	k2eAgFnSc1d1	agresivní
Laura	Laura	k1gFnSc1	Laura
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
nesmrtelném	smrtelný	k2eNgNnSc6d1	nesmrtelné
uchování	uchování	k1gNnSc6	uchování
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
s	s	k7c7	s
přáním	přání	k1gNnSc7	přání
nezůstat	zůstat	k5eNaPmF	zůstat
nesmrtelná	smrtelný	k2eNgNnPc1d1	nesmrtelné
<g/>
.	.	kIx.	.
</s>
<s>
Agnesino	Agnesin	k2eAgNnSc1d1	Agnesin
přání	přání	k1gNnSc1	přání
se	se	k3xPyFc4	se
splní	splnit	k5eAaPmIp3nS	splnit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
ji	on	k3xPp3gFnSc4	on
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
již	již	k6eAd1	již
nezastihne	zastihnout	k5eNaPmIp3nS	zastihnout
živou	živá	k1gFnSc4	živá
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
naposledy	naposledy	k6eAd1	naposledy
políbil	políbit	k5eAaPmAgMnS	políbit
a	a	k8xC	a
uchoval	uchovat	k5eAaPmAgMnS	uchovat
si	se	k3xPyFc3	se
její	její	k3xOp3gFnSc4	její
tvář	tvář	k1gFnSc4	tvář
navždy	navždy	k6eAd1	navždy
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
,	,	kIx,	,
mizí	mizet	k5eAaImIp3nS	mizet
Agnes	Agnes	k1gInSc1	Agnes
z	z	k7c2	z
myšlenek	myšlenka	k1gFnPc2	myšlenka
všech	všecek	k3xTgMnPc2	všecek
svých	svůj	k3xOyFgMnPc2	svůj
blízkých	blízký	k2eAgMnPc2d1	blízký
(	(	kIx(	(
<g/>
Paula	Paul	k1gMnSc2	Paul
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Laurou	Laura	k1gFnSc7	Laura
-	-	kIx~	-
ženou	žena	k1gFnSc7	žena
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jí	on	k3xPp3gFnSc3	on
nazývá	nazývat	k5eAaImIp3nS	nazývat
<g/>
,	,	kIx,	,
i	i	k8xC	i
Agnesiny	Agnesin	k2eAgFnSc2d1	Agnesin
dcery	dcera	k1gFnSc2	dcera
Brigity	Brigita	k1gFnSc2	Brigita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
Laurou	Laura	k1gFnSc7	Laura
vede	vést	k5eAaImIp3nS	vést
vytrvalé	vytrvalý	k2eAgInPc4d1	vytrvalý
boje	boj	k1gInPc4	boj
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
vrací	vracet	k5eAaImIp3nS	vracet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
úvahách	úvaha	k1gFnPc6	úvaha
autora-vypravěče	autoraypravěč	k1gMnSc2	autora-vypravěč
<g/>
.	.	kIx.	.
</s>
<s>
Paralelně	paralelně	k6eAd1	paralelně
sleduje	sledovat	k5eAaImIp3nS	sledovat
čtenář	čtenář	k1gMnSc1	čtenář
Bettinu	Bettin	k1gInSc2	Bettin
von	von	k1gInSc1	von
Arnim	Arnim	k1gInSc1	Arnim
<g/>
,	,	kIx,	,
současnici	současnice	k1gFnSc4	současnice
Goetha	Goeth	k1gMnSc2	Goeth
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
sžíravou	sžíravý	k2eAgFnSc4d1	sžíravá
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
urputnou	urputný	k2eAgFnSc4d1	urputná
jako	jako	k8xS	jako
Lauřinu	Lauřin	k2eAgFnSc4d1	Lauřina
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
nesmrtelnosti	nesmrtelnost	k1gFnSc6	nesmrtelnost
v	v	k7c6	v
životě	život	k1gInSc6	život
rodiny	rodina	k1gFnSc2	rodina
její	její	k3xOp3gFnSc2	její
sestry	sestra	k1gFnSc2	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Úděl	úděl	k1gInSc1	úděl
slavného	slavný	k2eAgMnSc2d1	slavný
mrtvého	mrtvý	k1gMnSc2	mrtvý
glosuje	glosovat	k5eAaBmIp3nS	glosovat
samotný	samotný	k2eAgInSc1d1	samotný
Goethe	Goethe	k1gInSc1	Goethe
ve	v	k7c6	v
snovém	snový	k2eAgNnSc6d1	snové
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
mrtvým	mrtvý	k1gMnSc7	mrtvý
Hemingwayem	Hemingway	k1gMnSc7	Hemingway
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
příběhem	příběh	k1gInSc7	příběh
je	být	k5eAaImIp3nS	být
bohatý	bohatý	k2eAgInSc1d1	bohatý
milostný	milostný	k2eAgInSc1d1	milostný
život	život	k1gInSc1	život
neúspěšného	úspěšný	k2eNgMnSc2d1	neúspěšný
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
nazývaného	nazývaný	k2eAgMnSc2d1	nazývaný
Rubens	Rubens	k1gInSc4	Rubens
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
náhodným	náhodný	k2eAgMnSc7d1	náhodný
milencem	milenec	k1gMnSc7	milenec
Agnes	Agnesa	k1gFnPc2	Agnesa
<g/>
.	.	kIx.	.
</s>
<s>
Rubens	Rubens	k6eAd1	Rubens
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Agnes	Agnes	k1gInSc4	Agnes
téměř	téměř	k6eAd1	téměř
neznámý	známý	k2eNgMnSc1d1	neznámý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
o	o	k7c6	o
němž	jenž	k3xRgInSc6	jenž
nic	nic	k6eAd1	nic
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
hříčka	hříčka	k1gFnSc1	hříčka
osudu	osud	k1gInSc2	osud
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
si	se	k3xPyFc3	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
její	její	k3xOp3gInSc4	její
obraz	obraz	k1gInSc4	obraz
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
paměti	paměť	k1gFnSc6	paměť
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
dále	daleko	k6eAd2	daleko
změní	změnit	k5eAaPmIp3nS	změnit
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
vyprávění	vyprávění	k1gNnSc1	vyprávění
je	být	k5eAaImIp3nS	být
protkáno	protkat	k5eAaPmNgNnS	protkat
opakujícími	opakující	k2eAgFnPc7d1	opakující
se	se	k3xPyFc4	se
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
děje	děj	k1gInPc1	děj
poutají	poutat	k5eAaImIp3nP	poutat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
a	a	k8xC	a
provazují	provazovat	k5eAaImIp3nP	provazovat
je	on	k3xPp3gFnPc4	on
navzájem	navzájem	k6eAd1	navzájem
(	(	kIx(	(
<g/>
několikeré	několikerý	k4xRyIgFnPc4	několikerý
užití	užití	k1gNnSc3	užití
brýlí	brýle	k1gFnPc2	brýle
<g/>
,	,	kIx,	,
stejných	stejný	k2eAgNnPc2d1	stejné
gest	gesto	k1gNnPc2	gesto
<g/>
,	,	kIx,	,
podobnost	podobnost	k1gFnSc1	podobnost
Laury	Laura	k1gFnSc2	Laura
a	a	k8xC	a
Bettiny	Bettina	k1gFnSc2	Bettina
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
náhodami	náhoda	k1gFnPc7	náhoda
(	(	kIx(	(
<g/>
vůz	vůz	k1gInSc1	vůz
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
kolo	kolo	k1gNnSc4	kolo
profesor	profesor	k1gMnSc1	profesor
Avenarius	Avenarius	k1gMnSc1	Avenarius
propíchne	propíchnout	k5eAaPmIp3nS	propíchnout
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
Paulovi	Paulův	k2eAgMnPc1d1	Paulův
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
tak	tak	k9	tak
zrovna	zrovna	k6eAd1	zrovna
té	ten	k3xDgFnSc2	ten
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
má	mít	k5eAaImIp3nS	mít
Agnes	Agnes	k1gMnSc1	Agnes
autonehodu	autonehoda	k1gFnSc4	autonehoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
Paul	Paul	k1gMnSc1	Paul
nedostane	dostat	k5eNaPmIp3nS	dostat
včas	včas	k6eAd1	včas
a	a	k8xC	a
nezastihne	zastihnout	k5eNaPmIp3nS	zastihnout
ji	on	k3xPp3gFnSc4	on
už	už	k6eAd1	už
živou	živá	k1gFnSc4	živá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
nám	my	k3xPp1nPc3	my
autor-vypravěč	autorypravěč	k1gMnSc1	autor-vypravěč
poodhaluje	poodhalovat	k5eAaImIp3nS	poodhalovat
zdroje	zdroj	k1gInPc4	zdroj
svých	svůj	k3xOyFgFnPc2	svůj
inspirací	inspirace	k1gFnPc2	inspirace
(	(	kIx(	(
<g/>
náhodně	náhodně	k6eAd1	náhodně
zaslechnutá	zaslechnutý	k2eAgFnSc1d1	zaslechnutá
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
dívce	dívka	k1gFnSc6	dívka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
úmyslně	úmyslně	k6eAd1	úmyslně
vystavovala	vystavovat	k5eAaImAgFnS	vystavovat
kolům	kolo	k1gNnPc3	kolo
aut	auto	k1gNnPc2	auto
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
zemřít	zemřít	k5eAaPmF	zemřít
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
příčinu	příčina	k1gFnSc4	příčina
Agnesiny	Agnesin	k2eAgFnSc2d1	Agnesin
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
i	i	k9	i
postupů	postup	k1gInPc2	postup
(	(	kIx(	(
<g/>
metafora	metafora	k1gFnSc1	metafora
jako	jako	k8xC	jako
důležitý	důležitý	k2eAgInSc1d1	důležitý
impuls	impuls	k1gInSc1	impuls
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
románové	románový	k2eAgFnSc2d1	románová
postavy	postava	k1gFnSc2	postava
v	v	k7c6	v
Kunderových	Kunderův	k2eAgFnPc6d1	Kunderova
knihách	kniha	k1gFnPc6	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kundera	Kundera	k1gFnSc1	Kundera
se	se	k3xPyFc4	se
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
snaží	snažit	k5eAaImIp3nP	snažit
propojit	propojit	k5eAaPmF	propojit
formy	forma	k1gFnPc4	forma
narace	narace	k1gFnSc2	narace
a	a	k8xC	a
esejistiky	esejistika	k1gFnSc2	esejistika
<g/>
.	.	kIx.	.
</s>
<s>
Tématem	téma	k1gNnSc7	téma
Nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
je	být	k5eAaImIp3nS	být
ovlivňování	ovlivňování	k1gNnSc1	ovlivňování
vlastního	vlastní	k2eAgNnSc2d1	vlastní
bytí	bytí	k1gNnSc2	bytí
za	za	k7c2	za
života	život	k1gInSc2	život
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
imagologové	imagolog	k1gMnPc1	imagolog
zabývají	zabývat	k5eAaImIp3nP	zabývat
tvary	tvar	k1gInPc4	tvar
aut	auto	k1gNnPc2	auto
a	a	k8xC	a
tub	tuba	k1gFnPc2	tuba
na	na	k7c4	na
zubní	zubní	k2eAgFnPc4d1	zubní
pasty	pasta	k1gFnPc4	pasta
<g/>
,	,	kIx,	,
podléhá	podléhat	k5eAaImIp3nS	podléhat
řada	řada	k1gFnSc1	řada
lidí	člověk	k1gMnPc2	člověk
imagologii	imagologie	k1gFnSc4	imagologie
vlastního	vlastní	k2eAgInSc2d1	vlastní
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
obrazu	obraz	k1gInSc2	obraz
(	(	kIx(	(
<g/>
Laura	Laura	k1gFnSc1	Laura
<g/>
,	,	kIx,	,
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Bettina	Bettina	k1gFnSc1	Bettina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
stojí	stát	k5eAaImIp3nS	stát
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
odmítají	odmítat	k5eAaImIp3nP	odmítat
nátlaku	nátlak	k1gInSc3	nátlak
imagologie	imagologie	k1gFnSc2	imagologie
podrobit	podrobit	k5eAaPmF	podrobit
(	(	kIx(	(
<g/>
Agnes	Agnes	k1gInSc4	Agnes
a	a	k8xC	a
Hemingway	Hemingway	k1gInPc4	Hemingway
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
je	být	k5eAaImIp3nS	být
krom	krom	k7c2	krom
románového	románový	k2eAgNnSc2d1	románové
vyprávění	vyprávění	k1gNnSc2	vyprávění
především	především	k9	především
hlubokou	hluboký	k2eAgFnSc7d1	hluboká
meditací	meditace	k1gFnSc7	meditace
nad	nad	k7c7	nad
různými	různý	k2eAgFnPc7d1	různá
formami	forma	k1gFnPc7	forma
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
neodvratnému	odvratný	k2eNgInSc3d1	neodvratný
zániku	zánik	k1gInSc3	zánik
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
nevěří	věřit	k5eNaImIp3nS	věřit
na	na	k7c4	na
posmrtný	posmrtný	k2eAgInSc4d1	posmrtný
život	život	k1gInSc4	život
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
věrouky	věrouka	k1gFnSc2	věrouka
<g/>
.	.	kIx.	.
</s>
<s>
Nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
je	být	k5eAaImIp3nS	být
určitým	určitý	k2eAgInSc7d1	určitý
pendantem	pendant	k1gInSc7	pendant
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
Kunderovu	Kunderův	k2eAgInSc3d1	Kunderův
románu	román	k1gInSc3	román
<g/>
,	,	kIx,	,
k	k	k7c3	k
Nesnesitelné	snesitelný	k2eNgFnSc3d1	nesnesitelná
lehkosti	lehkost	k1gFnSc3	lehkost
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
Agnes	Agnesa	k1gFnPc2	Agnesa
<g/>
,	,	kIx,	,
snažící	snažící	k2eAgFnPc4d1	snažící
se	se	k3xPyFc4	se
skrýt	skrýt	k5eAaPmF	skrýt
svou	svůj	k3xOyFgFnSc4	svůj
tvář	tvář	k1gFnSc4	tvář
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
mužem	muž	k1gMnSc7	muž
i	i	k9	i
před	před	k7c7	před
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
duchovní	duchovní	k2eAgFnSc7d1	duchovní
spřízněnkyní	spřízněnkyně	k1gFnSc7	spřízněnkyně
postavy	postava	k1gFnSc2	postava
Sabiny	Sabina	k1gFnSc2	Sabina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
"	"	kIx"	"
<g/>
lehkosti	lehkost	k1gFnSc6	lehkost
bytí	bytí	k1gNnSc4	bytí
<g/>
"	"	kIx"	"
odmítá	odmítat	k5eAaImIp3nS	odmítat
nést	nést	k5eAaImF	nést
tíhu	tíha	k1gFnSc4	tíha
zodpovědnosti	zodpovědnost	k1gFnSc2	zodpovědnost
na	na	k7c6	na
druhých	druhý	k4xOgInPc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
imagologie	imagologie	k1gFnSc2	imagologie
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
Kunderův	Kunderův	k2eAgInSc4d1	Kunderův
výklad	výklad	k1gInSc4	výklad
kýče	kýč	k1gInSc2	kýč
<g/>
.	.	kIx.	.
</s>
<s>
Struktura	struktura	k1gFnSc1	struktura
i	i	k8xC	i
hloubka	hloubka	k1gFnSc1	hloubka
Kunderovy	Kunderův	k2eAgFnSc2d1	Kunderova
knihy	kniha	k1gFnSc2	kniha
je	být	k5eAaImIp3nS	být
blízká	blízký	k2eAgFnSc1d1	blízká
dílům	dílo	k1gNnPc3	dílo
jeho	jeho	k3xOp3gNnSc1	jeho
předchůdce	předchůdce	k1gMnSc4	předchůdce
Hermanna	Hermann	k1gMnSc4	Hermann
Brocha	Broch	k1gMnSc4	Broch
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jehož	k3xOyRp3gInSc4	jehož
román	román	k1gInSc4	román
Náměsíčníci	náměsíčník	k1gMnPc1	náměsíčník
Kundera	Kundera	k1gFnSc1	Kundera
často	často	k6eAd1	často
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
<g/>
.	.	kIx.	.
</s>
