<s>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaPmNgInS	napsat
česky	česky	k6eAd1	česky
a	a	k8xC	a
následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
překládán	překládat	k5eAaImNgInS	překládat
do	do	k7c2	do
francouzštiny	francouzština	k1gFnSc2	francouzština
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
během	během	k7c2	během
překladů	překlad	k1gInPc2	překlad
však	však	k9	však
autor	autor	k1gMnSc1	autor
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
i	i	k9	i
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
(	(	kIx(	(
<g/>
sám	sám	k3xTgMnSc1	sám
Kundera	Kunder	k1gMnSc4	Kunder
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
román	román	k1gInSc1	román
psal	psát	k5eAaImAgInS	psát
rok	rok	k1gInSc4	rok
a	a	k8xC	a
půl	půl	k1xP	půl
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
pohodě	pohoda	k1gFnSc6	pohoda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
překladům	překlad	k1gInPc3	překlad
věnoval	věnovat	k5eAaImAgMnS	věnovat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
v	v	k7c6	v
únavě	únava	k1gFnSc6	únava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
