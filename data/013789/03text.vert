<s>
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
</s>
<s>
Genpor.	Genpor.	k?
Ing.	ing.	kA
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
<g/>
,	,	kIx,
CSc.	CSc.	kA
Narození	narození	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1939	#num#	k4
(	(	kIx(
<g/>
81	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Trenčín	Trenčín	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
politik	politika	k1gFnPc2
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Československa	Československo	k1gNnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
21	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc4
1939	#num#	k4
Trenčín	Trenčín	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
československý	československý	k2eAgMnSc1d1
policista	policista	k1gMnSc1
slovenské	slovenský	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
1985	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
první	první	k4xOgMnSc1
náměstek	náměstek	k1gMnSc1
federálního	federální	k2eAgMnSc2d1
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
pověřený	pověřený	k2eAgMnSc1d1
řízením	řízení	k1gNnSc7
centrálních	centrální	k2eAgFnPc2d1
kontrarozvědných	kontrarozvědný	k2eAgFnPc2d1
správ	správa	k1gFnPc2
Státní	státní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prakticky	prakticky	k6eAd1
celou	celý	k2eAgFnSc4d1
svoji	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
působil	působit	k5eAaImAgMnS
v	v	k7c6
bezpečnostních	bezpečnostní	k2eAgFnPc6d1
složkách	složka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
absolutoriu	absolutorium	k1gNnSc6
Vojenské	vojenský	k2eAgFnSc2d1
akademie	akademie	k1gFnSc2
Antonína	Antonín	k1gMnSc2
Zápotockého	Zápotockého	k2eAgMnSc2d1
v	v	k7c6
Brně	Brno	k1gNnSc6
nastoupil	nastoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
na	na	k7c4
ministerstvo	ministerstvo	k1gNnSc4
vnitra	vnitro	k1gNnSc2
ČSSR	ČSSR	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
pracoval	pracovat	k5eAaImAgMnS
ve	v	k7c6
zvláštním	zvláštní	k2eAgInSc6d1
odboru	odbor	k1gInSc6
federálního	federální	k2eAgNnSc2d1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
kryptografii	kryptografie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídil	řídit	k5eAaImAgMnS
vývoj	vývoj	k1gInSc4
šifrovacího	šifrovací	k2eAgInSc2d1
automatu	automat	k1gInSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
byl	být	k5eAaImAgMnS
autorem	autor	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svými	svůj	k3xOyFgMnPc7
nadřízenými	nadřízený	k1gMnPc7
byl	být	k5eAaImAgMnS
opakovaně	opakovaně	k6eAd1
výborně	výborně	k6eAd1
hodnocen	hodnotit	k5eAaImNgMnS
a	a	k8xC
rychle	rychle	k6eAd1
postupoval	postupovat	k5eAaImAgInS
vzhůru	vzhůru	k6eAd1
po	po	k7c6
kariérním	kariérní	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
náčelníkem	náčelník	k1gMnSc7
zvláštní	zvláštní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
SNB	SNB	kA
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
byl	být	k5eAaImAgInS
ústřední	ústřední	k2eAgInSc1d1
orgán	orgán	k1gInSc1
šifrové	šifrový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
ČSSR	ČSSR	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1980	#num#	k4
velel	velet	k5eAaImAgInS
správě	správa	k1gFnSc6
SNB	SNB	kA
hl.	hl.	k?
města	město	k1gNnSc2
Bratislavy	Bratislava	k1gFnSc2
a	a	k8xC
Západoslovenského	západoslovenský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejvyššího	vysoký	k2eAgInSc2d3
postu	post	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
Vajnara	Vajnar	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1988	#num#	k4
<g/>
)	)	kIx)
generál	generál	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
řídil	řídit	k5eAaImAgMnS
Správu	správa	k1gFnSc4
ekonomické	ekonomický	k2eAgFnSc2d1
kontrarozvědky	kontrarozvědka	k1gFnSc2
a	a	k8xC
Správu	správa	k1gFnSc4
vnitřní	vnitřní	k2eAgFnSc2d1
kontrarozvědky	kontrarozvědka	k1gFnSc2
<g/>
,	,	kIx,
též	též	k9
ale	ale	k9
Vysokou	vysoký	k2eAgFnSc4d1
školu	škola	k1gFnSc4
SNB	SNB	kA
a	a	k8xC
Technickou	technický	k2eAgFnSc4d1
správu	správa	k1gFnSc4
FMV	FMV	kA
<g/>
,	,	kIx,
Správu	správa	k1gFnSc4
vývoje	vývoj	k1gInSc2
automatizace	automatizace	k1gFnSc2
<g/>
,	,	kIx,
Zvláštní	zvláštní	k2eAgFnSc4d1
správu	správa	k1gFnSc4
(	(	kIx(
<g/>
Ústřední	ústřední	k2eAgInSc1d1
orgán	orgán	k1gInSc1
šifrové	šifrový	k2eAgFnSc2d1
služby	služba	k1gFnSc2
státu	stát	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Technickou	technický	k2eAgFnSc4d1
správu	správa	k1gFnSc4
(	(	kIx(
<g/>
vývoj	vývoj	k1gInSc4
zpravodajské	zpravodajský	k2eAgFnSc2d1
techniky	technika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídil	řídit	k5eAaImAgMnS
Státní	státní	k2eAgFnSc3d1
komisi	komise	k1gFnSc3
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
státního	státní	k2eAgNnSc2d1
tajemství	tajemství	k1gNnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
členem	člen	k1gInSc7
Vládního	vládní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
pro	pro	k7c4
obranný	obranný	k2eAgInSc4d1
průmysl	průmysl	k1gInSc4
a	a	k8xC
členem	člen	k1gMnSc7
Vládní	vládní	k2eAgFnSc2d1
havarijní	havarijní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čase	čas	k1gInSc6
Mimořádných	mimořádný	k2eAgNnPc2d1
bezpečnostních	bezpečnostní	k2eAgNnPc2d1
opatření	opatření	k1gNnPc2
byl	být	k5eAaImAgInS
pověřován	pověřovat	k5eAaImNgInS
řízením	řízení	k1gNnSc7
celostátního	celostátní	k2eAgInSc2d1
operačního	operační	k2eAgInSc2d1
štábu	štáb	k1gInSc2
na	na	k7c6
FMV	FMV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
reorganizace	reorganizace	k1gFnSc2
FMV	FMV	kA
v	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
mu	on	k3xPp3gMnSc3
byly	být	k5eAaImAgFnP
podřízeny	podřízen	k2eAgFnPc1d1
Hlavní	hlavní	k2eAgFnPc1d1
správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnPc1
<g/>
,	,	kIx,
Hlavní	hlavní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
vojenské	vojenský	k2eAgFnSc2d1
kontrarozvědky	kontrarozvědka	k1gFnSc2
a	a	k8xC
Správa	správa	k1gFnSc1
kontrarozvědky	kontrarozvědka	k1gFnSc2
v	v	k7c6
Bratislavě	Bratislava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řídil	řídit	k5eAaImAgInS
a	a	k8xC
komplexně	komplexně	k6eAd1
kontroloval	kontrolovat	k5eAaImAgMnS
protišpionážní	protišpionážní	k2eAgFnSc4d1
<g/>
,	,	kIx,
ekonomickou	ekonomický	k2eAgFnSc4d1
<g/>
,	,	kIx,
vojenskou	vojenský	k2eAgFnSc4d1
i	i	k8xC
politickou	politický	k2eAgFnSc4d1
kontrarozvědnou	kontrarozvědný	k2eAgFnSc4d1
práci	práce	k1gFnSc4
v	v	k7c6
celém	celý	k2eAgNnSc6d1
Československu	Československo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1989	#num#	k4
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
sametové	sametový	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
vydal	vydat	k5eAaPmAgMnS
generál	generál	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
rozkaz	rozkaz	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yIgMnSc2,k3yRgMnSc2,k3yQgMnSc2
nemají	mít	k5eNaImIp3nP
složky	složka	k1gFnPc1
StB	StB	k1gFnPc2
nijak	nijak	k6eAd1
zasahovat	zasahovat	k5eAaImF
do	do	k7c2
situace	situace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
rozkaz	rozkaz	k1gInSc4
později	pozdě	k6eAd2
odůvodnil	odůvodnit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nechtěl	chtít	k5eNaImAgMnS
více	hodně	k6eAd2
pobouřit	pobouřit	k5eAaPmF
občany	občan	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
nařídil	nařídit	k5eAaPmAgInS
skartaci	skartace	k1gFnSc4
mnoha	mnoho	k4c2
dokumentů	dokument	k1gInPc2
z	z	k7c2
archivu	archiv	k1gInSc2
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
revoluci	revoluce	k1gFnSc6
byla	být	k5eAaImAgFnS
StB	StB	k1gFnSc1
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
generál	generál	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
z	z	k7c2
řad	řada	k1gFnPc2
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
propuštěn	propustit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
své	svůj	k3xOyFgInPc4
činy	čin	k1gInPc4
z	z	k7c2
doby	doba	k1gFnSc2
socialismu	socialismus	k1gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1993	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Vyšším	vysoký	k2eAgInSc7d2
vojenským	vojenský	k2eAgInSc7d1
soudem	soud	k1gInSc7
v	v	k7c6
Táboře	Tábor	k1gInSc6
(	(	kIx(
<g/>
předseda	předseda	k1gMnSc1
senátu	senát	k1gInSc2
Jiří	Jiří	k1gMnSc1
Bernát	Bernát	k1gMnSc1
<g/>
)	)	kIx)
odsouzen	odsouzet	k5eAaImNgMnS,k5eAaPmNgMnS
ke	k	k7c3
čtyřletému	čtyřletý	k2eAgInSc3d1
nepodmíněnému	podmíněný	k2eNgInSc3d1
trestu	trest	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trest	trest	k1gInSc4
si	se	k3xPyFc3
však	však	k9
nikdy	nikdy	k6eAd1
neodpykal	odpykat	k5eNaPmAgMnS
a	a	k8xC
odjel	odjet	k5eAaPmAgMnS
na	na	k7c4
Slovensko	Slovensko	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
orgány	orgán	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
trestní	trestní	k2eAgInPc4d1
stíhání	stíhání	k1gNnSc6
zastavily	zastavit	k5eAaPmAgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Teprve	teprve	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
byl	být	k5eAaImAgMnS
Lorenc	Lorenc	k1gMnSc1
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
k	k	k7c3
patnácti	patnáct	k4xCc3
měsícům	měsíc	k1gInPc3
odnětí	odnětí	k1gNnSc2
svobody	svoboda	k1gFnSc2
podmíněně	podmíněně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Lorenc	Lorenc	k1gMnSc1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
překročil	překročit	k5eAaPmAgMnS
své	svůj	k3xOyFgFnPc4
pravomoci	pravomoc	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
se	se	k3xPyFc4
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
zabýval	zabývat	k5eAaImAgMnS
komerční	komerční	k2eAgFnSc7d1
poradenskou	poradenský	k2eAgFnSc7d1
činností	činnost	k1gFnSc7
v	v	k7c6
oboru	obor	k1gInSc6
IT	IT	kA
bezpečnosti	bezpečnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
několik	několik	k4yIc4
let	léto	k1gNnPc2
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
ředitel	ředitel	k1gMnSc1
firemních	firemní	k2eAgFnPc2d1
rizik	riziko	k1gNnPc2
pro	pro	k7c4
investiční	investiční	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
Penta	Pent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
o	o	k7c6
úloze	úloha	k1gFnSc6
a	a	k8xC
akcích	akce	k1gFnPc6
MV	MV	kA
a	a	k8xC
StB	StB	k1gMnSc1
vydal	vydat	k5eAaPmAgMnS
knihu	kniha	k1gFnSc4
Ministerstvo	ministerstvo	k1gNnSc1
strachu	strach	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Neskartované	skartovaný	k2eNgFnPc4d1
vzpomínky	vzpomínka	k1gFnPc4
generála	generál	k1gMnSc2
Lorence	Lorenc	k1gMnSc2
(	(	kIx(
<g/>
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
vydal	vydat	k5eAaPmAgInS
společně	společně	k6eAd1
se	s	k7c7
spisovatelem	spisovatel	k1gMnSc7
Pavolem	Pavol	k1gMnSc7
Janíkem	Janík	k1gMnSc7
další	další	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
"	"	kIx"
<g/>
Dešifrovaný	dešifrovaný	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Václav	Václav	k1gMnSc1
Bartuška	Bartuška	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
člen	člen	k1gMnSc1
parlamentní	parlamentní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
pro	pro	k7c4
dohled	dohled	k1gInSc4
na	na	k7c4
vyšetřování	vyšetřování	k1gNnSc4
událostí	událost	k1gFnPc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
<g/>
,	,	kIx,
charakterizoval	charakterizovat	k5eAaBmAgMnS
Lorence	Lorenc	k1gMnPc4
takto	takto	k6eAd1
<g/>
:	:	kIx,
„	„	k?
<g/>
Za	za	k7c4
největšího	veliký	k2eAgMnSc4d3
protivníka	protivník	k1gMnSc4
–	–	k?
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgInS
často	často	k6eAd1
nad	nad	k7c4
moje	můj	k3xOp1gFnPc4
síly	síla	k1gFnPc4
–	–	k?
jsem	být	k5eAaImIp1nS
považoval	považovat	k5eAaImAgInS
generála	generál	k1gMnSc4
Lorence	Lorenc	k1gMnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
..	..	k?
<g/>
]	]	kIx)
Mluvil	mluvit	k5eAaImAgMnS
jsem	být	k5eAaImIp1nS
s	s	k7c7
ním	on	k3xPp3gMnSc7
na	na	k7c6
jaře	jaro	k1gNnSc6
1990	#num#	k4
desítky	desítka	k1gFnSc2
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inteligentní	inteligentní	k2eAgInPc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
informovaný	informovaný	k2eAgInSc1d1
<g/>
,	,	kIx,
nebezpečný	bezpečný	k2eNgInSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
1992	#num#	k4
–	–	k?
Ministerstvo	ministerstvo	k1gNnSc1
strachu	strach	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
:	:	kIx,
neskartované	skartovaný	k2eNgFnSc2d1
spomienky	spomienka	k1gFnSc2
generála	generál	k1gMnSc2
Lorenca	Lorenca	k?
–	–	k?
Tatrapress	Tatrapress	k1gInSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85260-20-4	80-85260-20-4	k4
</s>
<s>
1992	#num#	k4
–	–	k?
Ministerstvo	ministerstvo	k1gNnSc1
strachu	strach	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
:	:	kIx,
neskartované	skartovaný	k2eNgFnSc2d1
vzpomínky	vzpomínka	k1gFnSc2
generála	generál	k1gMnSc2
Lorence	Lorenc	k1gMnSc2
–	–	k?
Tatrapress	Tatrapress	k1gInSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85260-19-0	80-85260-19-0	k4
</s>
<s>
2000	#num#	k4
–	–	k?
Dešifrovaný	dešifrovaný	k2eAgInSc4d1
svet	svet	k1gInSc4
(	(	kIx(
<g/>
rozhovory	rozhovor	k1gInPc1
s	s	k7c7
Alojzem	Alojz	k1gMnSc7
Lorencem	Lorenc	k1gMnSc7
vedl	vést	k5eAaImAgMnS
Pavol	Pavol	k1gInSc4
Janík	Janík	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
Forza	Forza	k1gFnSc1
<g/>
,	,	kIx,
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-968475-1-1	80-968475-1-1	k4
</s>
<s>
2018	#num#	k4
–	–	k?
Je	být	k5eAaImIp3nS
štátny	štátna	k1gFnPc4
prevrat	prevrat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
na	na	k7c4
víkend	víkend	k1gInSc4
domov	domov	k1gInSc4
neprídem	nepríd	k1gInSc7
(	(	kIx(
<g/>
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
Alojzem	Alojz	k1gMnSc7
Lorencem	Lorenc	k1gMnSc7
vedl	vést	k5eAaImAgMnS
Karol	Karol	k1gInSc4
Lovaš	Lovaš	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
VSSS	VSSS	kA
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-8202-054-3	978-80-8202-054-3	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
ŽÁČEK	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
:	:	kIx,
Alojz	Alojz	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Závěrečná	závěrečný	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
vyšetřovací	vyšetřovací	k2eAgFnSc2d1
komise	komise	k1gFnSc2
Federálního	federální	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
pro	pro	k7c4
objasnění	objasnění	k1gNnSc4
událostí	událost	k1gFnPc2
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1989	#num#	k4
<g/>
,	,	kIx,
část	část	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
3	#num#	k4
<g/>
)	)	kIx)
b	b	k?
<g/>
)	)	kIx)
–	–	k?
Úloha	úloha	k1gFnSc1
mocenských	mocenský	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CHURÁŇ	CHURÁŇ	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
KDO	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
BYL	být	k5eAaImAgMnS
KDO	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
v	v	k7c6
našich	náš	k3xOp1gFnPc6
dějinách	dějiny	k1gFnPc6
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Libri	Libr	k1gFnSc2
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pokyn	pokyn	k1gInSc1
I.	I.	kA
náměstka	náměstek	k1gMnSc2
MV	MV	kA
ČSSR	ČSSR	kA
ze	z	k7c2
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MÜLLEROVÁ	MÜLLEROVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
;	;	kIx,
HANZEL	HANZEL	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Albertov	Albertov	k1gInSc1
16	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
Příběhy	příběh	k1gInPc7
sametové	sametový	k2eAgFnSc2d1
revluce	revluce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7422	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Slovníček	slovníček	k1gInSc4
<g/>
,	,	kIx,
s.	s.	k?
276	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
BAROCH	BAROCH	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posledního	poslední	k2eAgMnSc2d1
náčelníka	náčelník	k1gMnSc2
StB	StB	k1gMnSc2
Lorence	Lorenc	k1gMnSc2
zaměstnala	zaměstnat	k5eAaPmAgFnS
Penta	Penta	k1gFnSc1
<g/>
.	.	kIx.
aktualne	aktualnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
On-line	On-lin	k1gInSc5
ke	k	k7c3
kauze	kauza	k1gFnSc3
StB	StB	k1gFnPc3
<g/>
:	:	kIx,
Minulost	minulost	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Češi	Čech	k1gMnPc1
zvolili	zvolit	k5eAaPmAgMnP
zapomnění	zapomnění	k1gNnSc4
<g/>
.	.	kIx.
aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
3.200	3.200	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
js	js	k?
<g/>
20020812085	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
122890671	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1085	#num#	k4
8017	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2001113422	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
117861553	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2001113422	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Komunistický	komunistický	k2eAgInSc1d1
režim	režim	k1gInSc1
v	v	k7c6
Československu	Československo	k1gNnSc6
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
