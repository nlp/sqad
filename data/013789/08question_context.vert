<s desamb="1">
Tento	tento	k3xDgInSc1
rozkaz	rozkaz	k1gInSc4
později	pozdě	k6eAd2
odůvodnil	odůvodnit	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nechtěl	chtít	k5eNaImAgMnS
více	hodně	k6eAd2
pobouřit	pobouřit	k5eAaPmF
občany	občan	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
nařídil	nařídit	k5eAaPmAgInS
skartaci	skartace	k1gFnSc4
mnoha	mnoho	k4c2
dokumentů	dokument	k1gInPc2
z	z	k7c2
archivu	archiv	k1gInSc2
StB	StB	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
revoluci	revoluce	k1gFnSc6
byla	být	k5eAaImAgFnS
StB	StB	k1gFnSc1
zrušena	zrušit	k5eAaPmNgFnS
a	a	k8xC
generál	generál	k1gMnSc1
Lorenc	Lorenc	k1gMnSc1
z	z	k7c2
řad	řada	k1gFnPc2
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
propuštěn	propustit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>