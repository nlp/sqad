<p>
<s>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
Polička	Polička	k1gFnSc1	Polička
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
české	český	k2eAgFnSc2d1	Česká
strukturální	strukturální	k2eAgFnSc2d1	strukturální
abstrakce	abstrakce	k1gFnSc2	abstrakce
a	a	k8xC	a
imaginativní	imaginativní	k2eAgFnSc2d1	imaginativní
a	a	k8xC	a
iluzivní	iluzivní	k2eAgFnSc2d1	iluzivní
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
prožil	prožít	k5eAaPmAgMnS	prožít
dramatické	dramatický	k2eAgNnSc4d1	dramatické
dětství	dětství	k1gNnSc4	dětství
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přímým	přímý	k2eAgMnSc7d1	přímý
svědkem	svědek	k1gMnSc7	svědek
válečných	válečný	k2eAgFnPc2d1	válečná
hrůz	hrůza	k1gFnPc2	hrůza
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
války	válka	k1gFnSc2	válka
zatčen	zatknout	k5eAaPmNgMnS	zatknout
gestapem	gestapo	k1gNnSc7	gestapo
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
zahynul	zahynout	k5eAaPmAgMnS	zahynout
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
války	válka	k1gFnSc2	válka
pochází	pocházet	k5eAaImIp3nS	pocházet
také	také	k9	také
iniciační	iniciační	k2eAgInSc1d1	iniciační
zážitek	zážitek	k1gInSc1	zážitek
s	s	k7c7	s
kresbou	kresba	k1gFnSc7	kresba
idylické	idylický	k2eAgFnSc2d1	idylická
krajiny	krajina	k1gFnSc2	krajina
místního	místní	k2eAgMnSc2d1	místní
malíře	malíř	k1gMnSc2	malíř
Josefa	Josef	k1gMnSc2	Josef
Václava	Václav	k1gMnSc2	Václav
Síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
potřísněnou	potřísněný	k2eAgFnSc4d1	potřísněná
mozkem	mozek	k1gInSc7	mozek
německého	německý	k2eAgMnSc4d1	německý
vojáka	voják	k1gMnSc4	voják
během	během	k7c2	během
přestřelky	přestřelka	k1gFnSc2	přestřelka
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
ředitel	ředitel	k1gMnSc1	ředitel
muzea	muzeum	k1gNnSc2	muzeum
historik	historik	k1gMnSc1	historik
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
Sionův	Sionův	k2eAgMnSc1d1	Sionův
bratranec	bratranec	k1gMnSc1	bratranec
Otokar	Otokar	k1gMnSc1	Otokar
Aleš	Aleš	k1gMnSc1	Aleš
Kukla	Kukla	k1gMnSc1	Kukla
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
půjčoval	půjčovat	k5eAaImAgMnS	půjčovat
literaturu	literatura	k1gFnSc4	literatura
o	o	k7c6	o
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
začal	začít	k5eAaPmAgInS	začít
Sion	Sion	k1gInSc1	Sion
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
Vyšší	vysoký	k2eAgFnSc6d2	vyšší
škole	škola	k1gFnSc6	škola
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
řemesel	řemeslo	k1gNnPc2	řemeslo
(	(	kIx(	(
<g/>
prof.	prof.	kA	prof.
J.	J.	kA	J.
Brukner	Brukner	k1gMnSc1	Brukner
<g/>
,	,	kIx,	,
prof.	prof.	kA	prof.
O.	O.	kA	O.
Zemina	zemina	k1gFnSc1	zemina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Tomalíkem	Tomalík	k1gMnSc7	Tomalík
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
přípravného	přípravný	k2eAgInSc2d1	přípravný
ročníku	ročník	k1gInSc2	ročník
Akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
k	k	k7c3	k
profesoru	profesor	k1gMnSc3	profesor
Vodrážkovi	Vodrážka	k1gMnSc3	Vodrážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958-1964	[number]	k4	1958-1964
studoval	studovat	k5eAaImAgInS	studovat
AVU	AVU	kA	AVU
u	u	k7c2	u
profesorů	profesor	k1gMnPc2	profesor
Karla	Karel	k1gMnSc2	Karel
Součka	Souček	k1gMnSc2	Souček
a	a	k8xC	a
Jiřího	Jiří	k1gMnSc2	Jiří
Horníka	Horník	k1gMnSc2	Horník
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
celé	celý	k2eAgFnSc2d1	celá
generace	generace	k1gFnSc2	generace
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
mimoškolní	mimoškolní	k2eAgFnSc1d1	mimoškolní
tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
schůzky	schůzka	k1gFnSc2	schůzka
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
kolem	kolem	k7c2	kolem
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
skupiny	skupina	k1gFnSc2	skupina
Konfrontace	konfrontace	k1gFnSc2	konfrontace
však	však	k9	však
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Sionův	Sionův	k2eAgInSc4d1	Sionův
malířský	malířský	k2eAgInSc4d1	malířský
vývoj	vývoj	k1gInSc4	vývoj
významnější	významný	k2eAgInSc4d2	významnější
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mikulášském	mikulášský	k2eAgInSc6d1	mikulášský
večírku	večírek	k1gInSc6	večírek
na	na	k7c4	na
AVU	AVU	kA	AVU
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
Šmidrové	Šmidr	k1gMnPc1	Šmidr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Koblasou	Koblasa	k1gFnSc7	Koblasa
<g/>
,	,	kIx,	,
Neprašem	Nepraš	k1gInSc7	Nepraš
a	a	k8xC	a
Dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
také	také	k9	také
s	s	k7c7	s
klasiky	klasik	k1gMnPc7	klasik
české	český	k2eAgFnSc2d1	Česká
poválečné	poválečný	k2eAgFnSc2d1	poválečná
abstrakce	abstrakce	k1gFnSc2	abstrakce
-	-	kIx~	-
Boudníkem	Boudník	k1gMnSc7	Boudník
<g/>
,	,	kIx,	,
Piesenem	Piesen	k1gMnSc7	Piesen
a	a	k8xC	a
Medkem	Medek	k1gMnSc7	Medek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Antonínem	Antonín	k1gMnSc7	Antonín
Tomalíkem	Tomalík	k1gMnSc7	Tomalík
se	se	k3xPyFc4	se
Sion	Sion	k1gMnSc1	Sion
představil	představit	k5eAaPmAgMnS	představit
na	na	k7c6	na
jednodenní	jednodenní	k2eAgFnSc6d1	jednodenní
neveřejné	veřejný	k2eNgFnSc6d1	neveřejná
výstavě	výstava	k1gFnSc6	výstava
Konfrontace	konfrontace	k1gFnSc2	konfrontace
II	II	kA	II
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
Aleše	Aleš	k1gMnSc2	Aleš
Veselého	Veselý	k1gMnSc2	Veselý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
neoficiálních	oficiální	k2eNgFnPc6d1	neoficiální
výstavách	výstava	k1gFnPc6	výstava
hrozilo	hrozit	k5eAaImAgNnS	hrozit
vyloučení	vyloučení	k1gNnSc1	vyloučení
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
organizátoři	organizátor	k1gMnPc1	organizátor
prvních	první	k4xOgFnPc2	první
výstav	výstava	k1gFnPc2	výstava
strukturální	strukturální	k2eAgFnSc2d1	strukturální
abstrakce	abstrakce	k1gFnSc2	abstrakce
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
síni	síň	k1gFnSc6	síň
(	(	kIx(	(
<g/>
Výstava	výstava	k1gFnSc1	výstava
D	D	kA	D
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
studenty	student	k1gMnPc4	student
nepřizvali	přizvat	k5eNaPmAgMnP	přizvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
výstavy	výstava	k1gFnSc2	výstava
Konfrontace	konfrontace	k1gFnSc2	konfrontace
III	III	kA	III
v	v	k7c6	v
Alšově	Alšův	k2eAgFnSc6d1	Alšova
síni	síň	k1gFnSc6	síň
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
směřoval	směřovat	k5eAaImAgMnS	směřovat
k	k	k7c3	k
jinému	jiný	k2eAgInSc3d1	jiný
výrazu	výraz	k1gInSc3	výraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
samostatně	samostatně	k6eAd1	samostatně
vystavoval	vystavovat	k5eAaImAgMnS	vystavovat
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Oblastní	oblastní	k2eAgFnSc6d1	oblastní
galerii	galerie	k1gFnSc6	galerie
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
významných	významný	k2eAgFnPc2d1	významná
přehlídek	přehlídka	k1gFnPc2	přehlídka
českého	český	k2eAgNnSc2d1	české
moderního	moderní	k2eAgNnSc2d1	moderní
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
(	(	kIx(	(
<g/>
kongres	kongres	k1gInSc1	kongres
AICA	AICA	kA	AICA
<g/>
,	,	kIx,	,
Phases	Phases	k1gMnSc1	Phases
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Turín	Turín	k1gInSc1	Turín
<g/>
,	,	kIx,	,
Dortmund	Dortmund	k1gInSc1	Dortmund
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1958-1962	[number]	k4	1958-1962
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
zakladatelům	zakladatel	k1gMnPc3	zakladatel
spolku	spolek	k1gInSc2	spolek
známého	známý	k2eAgInSc2d1	známý
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
Křižovnická	Křižovnický	k2eAgFnSc1d1	Křižovnická
škola	škola	k1gFnSc1	škola
čistého	čistý	k2eAgInSc2d1	čistý
humoru	humor	k1gInSc2	humor
bez	bez	k7c2	bez
vtipu	vtip	k1gInSc2	vtip
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Beranem	Beran	k1gMnSc7	Beran
<g/>
,	,	kIx,	,
Antonínem	Antonín	k1gMnSc7	Antonín
Tomalíkem	Tomalík	k1gMnSc7	Tomalík
a	a	k8xC	a
Antonínem	Antonín	k1gMnSc7	Antonín
Málkem	Málek	k1gMnSc7	Málek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hospody	hospody	k?	hospody
U	u	k7c2	u
Křižovníků	křižovník	k1gMnPc2	křižovník
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
přesunula	přesunout	k5eAaPmAgFnS	přesunout
do	do	k7c2	do
hospody	hospody	k?	hospody
U	u	k7c2	u
Svitáků	Sviták	k1gMnPc2	Sviták
a	a	k8xC	a
sehrávala	sehrávat	k5eAaImAgFnS	sehrávat
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
během	během	k7c2	během
normalizace	normalizace	k1gFnSc2	normalizace
až	až	k6eAd1	až
do	do	k7c2	do
vzniku	vznik	k1gInSc2	vznik
Charty	charta	k1gFnSc2	charta
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
byl	být	k5eAaImAgInS	být
Sion	Sion	k1gInSc1	Sion
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
jako	jako	k9	jako
čerpač	čerpač	k1gMnSc1	čerpač
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Stavební	stavební	k2eAgFnSc2d1	stavební
geologie	geologie	k1gFnSc2	geologie
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
neměl	mít	k5eNaImAgInS	mít
žádnou	žádný	k3yNgFnSc4	žádný
autorskou	autorský	k2eAgFnSc4d1	autorská
výstavu	výstava	k1gFnSc4	výstava
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
skupinových	skupinový	k2eAgFnPc6d1	skupinová
výstavách	výstava	k1gFnPc6	výstava
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
Düsseldorf	Düsseldorf	k1gMnSc1	Düsseldorf
<g/>
,	,	kIx,	,
Padova	Padova	k1gFnSc1	Padova
<g/>
,	,	kIx,	,
Konstanz	Konstanz	k1gInSc1	Konstanz
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
regionálních	regionální	k2eAgFnPc6d1	regionální
galeriích	galerie	k1gFnPc6	galerie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
a	a	k8xC	a
1977	[number]	k4	1977
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
výstavy	výstava	k1gFnSc2	výstava
pro	pro	k7c4	pro
přátele	přítel	k1gMnPc4	přítel
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
sklepním	sklepní	k2eAgInSc6d1	sklepní
ateliéru	ateliér	k1gInSc6	ateliér
na	na	k7c6	na
Vinohradech	Vinohrady	k1gInPc6	Vinohrady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
docentem	docent	k1gMnSc7	docent
v	v	k7c6	v
ateliéru	ateliér	k1gInSc6	ateliér
malby	malba	k1gFnSc2	malba
na	na	k7c6	na
AVU	AVU	kA	AVU
<g/>
,	,	kIx,	,
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
se	se	k3xPyFc4	se
sympozia	sympozion	k1gNnSc2	sympozion
věnovaného	věnovaný	k2eAgInSc2d1	věnovaný
informelu	informel	k1gInSc2	informel
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
na	na	k7c6	na
významné	významný	k2eAgFnSc6d1	významná
výstavě	výstava	k1gFnSc6	výstava
Český	český	k2eAgInSc1d1	český
informel	informel	k1gInSc1	informel
-	-	kIx~	-
průkopníci	průkopník	k1gMnPc1	průkopník
abstrakce	abstrakce	k1gFnSc2	abstrakce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgFnP	uskutečnit
retrospektivní	retrospektivní	k2eAgFnPc1d1	retrospektivní
výstavy	výstava	k1gFnPc1	výstava
Zbyška	Zbyšek	k1gMnSc2	Zbyšek
Siona	Sion	k1gMnSc2	Sion
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
<g/>
,	,	kIx,	,
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
GVŠ	GVŠ	kA	GVŠ
<g/>
)	)	kIx)	)
a	a	k8xC	a
Domě	dům	k1gInSc6	dům
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
měl	mít	k5eAaImAgInS	mít
velkou	velký	k2eAgFnSc4d1	velká
retrospektivní	retrospektivní	k2eAgFnSc4d1	retrospektivní
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
Rudolfinu	Rudolfinum	k1gNnSc6	Rudolfinum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Sion	Sion	k1gMnSc1	Sion
je	být	k5eAaImIp3nS	být
malířem	malíř	k1gMnSc7	malíř
existenciálně	existenciálně	k6eAd1	existenciálně
akcentovaného	akcentovaný	k2eAgInSc2d1	akcentovaný
tragického	tragický	k2eAgInSc2d1	tragický
pocitu	pocit	k1gInSc2	pocit
i	i	k8xC	i
drsně	drsně	k6eAd1	drsně
až	až	k9	až
krutě	krutě	k6eAd1	krutě
vyostřené	vyostřený	k2eAgFnSc2d1	vyostřená
grotesky	groteska	k1gFnSc2	groteska
Pro	pro	k7c4	pro
Zbyška	Zbyšek	k1gMnSc4	Zbyšek
Siona	Sion	k1gMnSc4	Sion
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
významným	významný	k2eAgInSc7d1	významný
inspiračním	inspirační	k2eAgInSc7d1	inspirační
zdrojem	zdroj	k1gInSc7	zdroj
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
na	na	k7c6	na
AVU	AVU	kA	AVU
četl	číst	k5eAaImAgMnS	číst
Strindberga	Strindberg	k1gMnSc4	Strindberg
a	a	k8xC	a
Kafku	Kafka	k1gMnSc4	Kafka
a	a	k8xC	a
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
Dostojevského	Dostojevský	k2eAgMnSc4d1	Dostojevský
Zločin	zločin	k1gInSc1	zločin
a	a	k8xC	a
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladé	mladý	k2eAgMnPc4d1	mladý
adepty	adept	k1gMnPc4	adept
AVU	AVU	kA	AVU
přitahovala	přitahovat	k5eAaImAgFnS	přitahovat
temnosvitná	temnosvitný	k2eAgFnSc1d1	temnosvitný
barokní	barokní	k2eAgFnSc1d1	barokní
malba	malba	k1gFnSc1	malba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sionově	Sionov	k1gInSc6	Sionov
raných	raný	k2eAgFnPc2d1	raná
kresbách	kresba	k1gFnPc6	kresba
ze	z	k7c2	z
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
dominuje	dominovat	k5eAaImIp3nS	dominovat
čerň	čerň	k1gFnSc1	čerň
a	a	k8xC	a
vedle	vedle	k7c2	vedle
hospodských	hospodský	k2eAgFnPc2d1	hospodská
scén	scéna	k1gFnPc2	scéna
se	se	k3xPyFc4	se
jako	jako	k9	jako
nejčastější	častý	k2eAgInPc1d3	nejčastější
motivy	motiv	k1gInPc1	motiv
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
lebky	lebka	k1gFnPc4	lebka
<g/>
,	,	kIx,	,
krucifi	krucif	k1gFnPc4	krucif
a	a	k8xC	a
smutné	smutný	k2eAgInPc4d1	smutný
introvertní	introvertní	k2eAgInPc4d1	introvertní
autoportréty	autoportrét	k1gInPc4	autoportrét
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
,,	,,	k?	,,
<g/>
Já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
nebyl	být	k5eNaImAgMnS	být
<g/>
'	'	kIx"	'
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Období	období	k1gNnSc1	období
tuhého	tuhý	k2eAgInSc2d1	tuhý
stalinského	stalinský	k2eAgInSc2d1	stalinský
režimu	režim	k1gInSc2	režim
konce	konec	k1gInSc2	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
plné	plný	k2eAgFnSc2d1	plná
beznaděje	beznaděj	k1gFnSc2	beznaděj
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
úzkostí	úzkost	k1gFnPc2	úzkost
<g/>
,	,	kIx,	,
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
i	i	k9	i
Sionovy	Sionov	k1gInPc1	Sionov
temně	temně	k6eAd1	temně
existenciální	existenciální	k2eAgFnSc2d1	existenciální
verze	verze	k1gFnSc2	verze
koupání	koupání	k1gNnSc2	koupání
<g/>
,	,	kIx,	,
starozákonní	starozákonní	k2eAgInPc4d1	starozákonní
motivy	motiv	k1gInPc4	motiv
a	a	k8xC	a
scény	scéna	k1gFnPc4	scéna
Ukřižování	ukřižování	k1gNnPc2	ukřižování
<g/>
,	,	kIx,	,
tematizující	tematizující	k2eAgFnSc1d1	tematizující
situace	situace	k1gFnSc1	situace
utrpení	utrpení	k1gNnSc2	utrpení
a	a	k8xC	a
oběti	oběť	k1gFnSc2	oběť
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
brněnské	brněnský	k2eAgFnSc2d1	brněnská
výstavy	výstava	k1gFnSc2	výstava
Zakladatelé	zakladatel	k1gMnPc1	zakladatel
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
roku	rok	k1gInSc2	rok
1957	[number]	k4	1957
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
kresbách	kresba	k1gFnPc6	kresba
objevuje	objevovat	k5eAaImIp3nS	objevovat
volnější	volný	k2eAgFnSc1d2	volnější
stylizace	stylizace	k1gFnSc1	stylizace
a	a	k8xC	a
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
malbě	malba	k1gFnSc3	malba
sahá	sahat	k5eAaImIp3nS	sahat
pro	pro	k7c4	pro
inspiraci	inspirace	k1gFnSc4	inspirace
ke	k	k7c3	k
Kubištovi	Kubišta	k1gMnSc3	Kubišta
<g/>
,	,	kIx,	,
expresionismu	expresionismus	k1gInSc2	expresionismus
a	a	k8xC	a
prekubismu	prekubismus	k1gInSc2	prekubismus
<g/>
.	.	kIx.	.
</s>
<s>
Hledání	hledání	k1gNnSc1	hledání
abstraktního	abstraktní	k2eAgInSc2d1	abstraktní
výrazu	výraz	k1gInSc2	výraz
bylo	být	k5eAaImAgNnS	být
společné	společný	k2eAgFnSc3d1	společná
celé	celý	k2eAgFnSc3d1	celá
skupině	skupina	k1gFnSc3	skupina
výtvarníků	výtvarník	k1gMnPc2	výtvarník
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
se	se	k3xPyFc4	se
stýkal	stýkat	k5eAaImAgMnS	stýkat
<g/>
,	,	kIx,	,
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c6	na
oficiální	oficiální	k2eAgNnSc4d1	oficiální
protežování	protežování	k1gNnSc4	protežování
a	a	k8xC	a
vnucování	vnucování	k1gNnSc4	vnucování
tzv.	tzv.	kA	tzv.
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sionův	Sionův	k2eAgInSc1d1	Sionův
přechod	přechod	k1gInSc1	přechod
k	k	k7c3	k
radikální	radikální	k2eAgFnSc3d1	radikální
abstrakci	abstrakce	k1gFnSc3	abstrakce
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
lebek	lebka	k1gFnPc2	lebka
a	a	k8xC	a
hlav	hlava	k1gFnPc2	hlava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
Veraikonem	veraikon	k1gInSc7	veraikon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
již	již	k9	již
vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
zcela	zcela	k6eAd1	zcela
vytrácí	vytrácet	k5eAaImIp3nS	vytrácet
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
pak	pak	k6eAd1	pak
Sion	Sion	k1gInSc1	Sion
tvořil	tvořit	k5eAaImAgInS	tvořit
nezobrazující	zobrazující	k2eNgFnPc4d1	zobrazující
kompozice	kompozice	k1gFnPc4	kompozice
<g/>
,	,	kIx,	,
gestické	gestický	k2eAgFnPc4d1	gestická
kresby	kresba	k1gFnPc4	kresba
a	a	k8xC	a
pastózní	pastózní	k2eAgInPc4d1	pastózní
kvaše	kvaš	k1gInPc4	kvaš
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
převažuje	převažovat	k5eAaImIp3nS	převažovat
čerň	čerň	k1gFnSc1	čerň
(	(	kIx(	(
<g/>
Bez	bez	k7c2	bez
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
Návštěvník	návštěvník	k1gMnSc1	návštěvník
<g/>
,	,	kIx,	,
Sen	sen	k1gInSc1	sen
o	o	k7c6	o
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
soubor	soubor	k1gInSc4	soubor
jeho	jeho	k3xOp3gNnPc2	jeho
privátních	privátní	k2eAgNnPc2d1	privátní
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
AMU	AMU	kA	AMU
(	(	kIx(	(
<g/>
Hradební	hradební	k2eAgFnSc1d1	hradební
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
viděli	vidět	k5eAaImAgMnP	vidět
Jiří	Jiří	k1gMnSc1	Jiří
Kolář	Kolář	k1gMnSc1	Kolář
a	a	k8xC	a
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Medek	Medek	k1gMnSc1	Medek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
školních	školní	k2eAgInPc6d1	školní
obrazech	obraz	k1gInPc6	obraz
převažovala	převažovat	k5eAaImAgNnP	převažovat
figurální	figurální	k2eAgNnPc1d1	figurální
témata	téma	k1gNnPc1	téma
a	a	k8xC	a
krajiny	krajina	k1gFnPc1	krajina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doma	doma	k6eAd1	doma
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
cyklus	cyklus	k1gInSc1	cyklus
Pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
s	s	k7c7	s
akční	akční	k2eAgFnSc7d1	akční
malbou	malba	k1gFnSc7	malba
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
s	s	k7c7	s
nemalířskými	malířský	k2eNgInPc7d1	malířský
prostředky	prostředek	k1gInPc7	prostředek
a	a	k8xC	a
technikami	technika	k1gFnPc7	technika
<g/>
,	,	kIx,	,
kombinoval	kombinovat	k5eAaImAgMnS	kombinovat
tuš	tuš	k1gFnSc4	tuš
a	a	k8xC	a
kvaš	kvaš	k1gFnSc4	kvaš
s	s	k7c7	s
kolážovanými	kolážovaný	k2eAgInPc7d1	kolážovaný
útržky	útržek	k1gInPc7	útržek
novin	novina	k1gFnPc2	novina
<g/>
,	,	kIx,	,
akrylátovým	akrylátový	k2eAgInSc7d1	akrylátový
lakem	lak	k1gInSc7	lak
<g/>
,	,	kIx,	,
pískem	písek	k1gInSc7	písek
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
materiály	materiál	k1gInPc7	materiál
(	(	kIx(	(
<g/>
Pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
<g/>
,	,	kIx,	,
koláž	koláž	k1gFnSc1	koláž
<g/>
,	,	kIx,	,
kvaš	kvaš	k1gFnSc1	kvaš
<g/>
,	,	kIx,	,
papír	papír	k1gInSc1	papír
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
OG	OG	kA	OG
Liberec	Liberec	k1gInSc1	Liberec
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
GU	GU	kA	GU
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
,	,	kIx,	,
Studie	studie	k1gFnPc1	studie
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
GMU	GMU	kA	GMU
Roudnice	Roudnice	k1gFnSc1	Roudnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrstvením	vrstvení	k1gNnSc7	vrstvení
<g/>
,	,	kIx,	,
prorýváním	prorývání	k1gNnSc7	prorývání
a	a	k8xC	a
propalováním	propalování	k1gNnSc7	propalování
různorodých	různorodý	k2eAgFnPc2d1	různorodá
hmot	hmota	k1gFnPc2	hmota
vznikaly	vznikat	k5eAaImAgFnP	vznikat
temné	temný	k2eAgFnPc1d1	temná
erupce	erupce	k1gFnPc1	erupce
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
tvarů	tvar	k1gInPc2	tvar
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
jeho	jeho	k3xOp3gInSc3	jeho
generačnímu	generační	k2eAgInSc3d1	generační
pocitu	pocit	k1gInSc3	pocit
a	a	k8xC	a
vniřnímu	vniřní	k2eAgNnSc3d1	vniřní
prožívání	prožívání	k1gNnSc3	prožívání
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1962-63	[number]	k4	1962-63
se	se	k3xPyFc4	se
Sion	Sion	k1gMnSc1	Sion
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
barvě	barva	k1gFnSc3	barva
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
paralelně	paralelně	k6eAd1	paralelně
abstraktní	abstraktní	k2eAgFnSc1d1	abstraktní
i	i	k8xC	i
figurální	figurální	k2eAgFnSc1d1	figurální
kompozice	kompozice	k1gFnSc1	kompozice
(	(	kIx(	(
<g/>
Modrá	modrý	k2eAgFnSc1d1	modrá
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
Paridův	Paridův	k2eAgInSc1d1	Paridův
soud	soud	k1gInSc1	soud
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
svůj	svůj	k3xOyFgInSc4	svůj
osobitý	osobitý	k2eAgInSc4d1	osobitý
expresivní	expresivní	k2eAgInSc4d1	expresivní
rukopis	rukopis	k1gInSc4	rukopis
i	i	k8xC	i
složitou	složitý	k2eAgFnSc4d1	složitá
kompoziční	kompoziční	k2eAgFnSc4d1	kompoziční
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
bravurní	bravurní	k2eAgFnSc2d1	bravurní
kresby	kresba	k1gFnSc2	kresba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
)	)	kIx)	)
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vedle	vedle	k7c2	vedle
dramaticky	dramaticky	k6eAd1	dramaticky
vrstvených	vrstvený	k2eAgFnPc2d1	vrstvená
materiálových	materiálový	k2eAgFnPc2d1	materiálová
struktur	struktura	k1gFnPc2	struktura
Sion	Sion	k1gMnSc1	Sion
využívá	využívat	k5eAaImIp3nS	využívat
í	í	k0	í
lazurní	lazurný	k2eAgMnPc1d1	lazurný
vrstvy	vrstva	k1gFnPc1	vrstva
a	a	k8xC	a
vymývané	vymývaný	k2eAgFnPc1d1	vymývaná
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
tušové	tušový	k2eAgFnPc1d1	tušová
perokresby	perokresba	k1gFnPc1	perokresba
a	a	k8xC	a
šrafy	šrafa	k1gFnPc1	šrafa
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
jemnými	jemný	k2eAgInPc7d1	jemný
<g/>
,	,	kIx,	,
složitě	složitě	k6eAd1	složitě
se	s	k7c7	s
překrývajícími	překrývající	k2eAgFnPc7d1	překrývající
barevnými	barevný	k2eAgFnPc7d1	barevná
ploškami	ploška	k1gFnPc7	ploška
a	a	k8xC	a
fasetami	faseta	k1gFnPc7	faseta
(	(	kIx(	(
<g/>
Apokalyptická	apokalyptický	k2eAgFnSc1d1	apokalyptická
kobylka	kobylka	k1gFnSc1	kobylka
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
AJG	AJG	kA	AJG
Hluboká	Hluboká	k1gFnSc1	Hluboká
<g/>
,	,	kIx,	,
OG	OG	kA	OG
Liberec	Liberec	k1gInSc1	Liberec
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
postupně	postupně	k6eAd1	postupně
opouští	opouštět	k5eAaImIp3nS	opouštět
abstrakci	abstrakce	k1gFnSc4	abstrakce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dynamická	dynamický	k2eAgFnSc1d1	dynamická
mozaika	mozaika	k1gFnSc1	mozaika
předjímá	předjímat	k5eAaImIp3nS	předjímat
jeho	jeho	k3xOp3gInPc4	jeho
příští	příští	k2eAgInPc4d1	příští
malířské	malířský	k2eAgInPc4d1	malířský
postupy	postup	k1gInPc4	postup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vracejí	vracet	k5eAaImIp3nP	vracet
k	k	k7c3	k
lidské	lidský	k2eAgFnSc3d1	lidská
figuře	figura	k1gFnSc3	figura
a	a	k8xC	a
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
fantaskní	fantaskní	k2eAgFnSc3d1	fantaskní
grotesce	groteska	k1gFnSc3	groteska
(	(	kIx(	(
<g/>
Kuře	kuře	k1gNnSc1	kuře
uzurpátor	uzurpátor	k1gMnSc1	uzurpátor
<g/>
,	,	kIx,	,
Švarný	švarný	k2eAgMnSc1d1	švarný
moloch	moloch	k1gMnSc1	moloch
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
<g/>
-	-	kIx~	-
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
samostatné	samostatný	k2eAgFnSc6d1	samostatná
výstavě	výstava	k1gFnSc6	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
se	se	k3xPyFc4	se
už	už	k9	už
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
představil	představit	k5eAaPmAgMnS	představit
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
přízračných	přízračný	k2eAgNnPc2d1	přízračné
monster	monstrum	k1gNnPc2	monstrum
a	a	k8xC	a
apokalyptických	apokalyptický	k2eAgFnPc2d1	apokalyptická
vizí	vize	k1gFnPc2	vize
(	(	kIx(	(
<g/>
Pád	Pád	k1gInSc1	Pád
věže	věž	k1gFnSc2	věž
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
GMU	GMU	kA	GMU
Roudnice	Roudnice	k1gFnSc1	Roudnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
srpnové	srpnový	k2eAgFnSc6d1	srpnová
okupaci	okupace	k1gFnSc6	okupace
názvy	název	k1gInPc4	název
cyklů	cyklus	k1gInPc2	cyklus
Sionových	Sionův	k2eAgInPc2d1	Sionův
obrazů	obraz	k1gInPc2	obraz
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vypovídaly	vypovídat	k5eAaPmAgInP	vypovídat
o	o	k7c6	o
pocitech	pocit	k1gInPc6	pocit
autora	autor	k1gMnSc2	autor
(	(	kIx(	(
<g/>
Proradný	proradný	k2eAgMnSc1d1	proradný
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
AJG	AJG	kA	AJG
<g/>
,	,	kIx,	,
Nezvaný	zvaný	k2eNgMnSc1d1	zvaný
host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
AJG	AJG	kA	AJG
<g/>
,	,	kIx,	,
Situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
vychovávat	vychovávat	k5eAaImF	vychovávat
klackem	klacek	k1gInSc7	klacek
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
OGV	OGV	kA	OGV
<g/>
,	,	kIx,	,
Štvanci	štvanec	k1gMnPc1	štvanec
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
GMU	GMU	kA	GMU
Roudnice	Roudnice	k1gFnSc1	Roudnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Figury	figura	k1gFnPc1	figura
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
plošné	plošný	k2eAgFnPc1d1	plošná
a	a	k8xC	a
budované	budovaný	k2eAgNnSc1d1	budované
s	s	k7c7	s
expresivní	expresivní	k2eAgFnSc7d1	expresivní
nadsázkou	nadsázka	k1gFnSc7	nadsázka
z	z	k7c2	z
množství	množství	k1gNnSc2	množství
barevných	barevný	k2eAgFnPc2d1	barevná
skvrn	skvrna	k1gFnPc2	skvrna
(	(	kIx(	(
<g/>
Portrét	portrét	k1gInSc1	portrét
slečny	slečna	k1gFnSc2	slečna
D	D	kA	D
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Sionova	Sionův	k2eAgFnSc1d1	Sionův
malba	malba	k1gFnSc1	malba
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
jakousi	jakýsi	k3yIgFnSc4	jakýsi
osobitou	osobitý	k2eAgFnSc4d1	osobitá
formu	forma	k1gFnSc4	forma
manýrismu	manýrismus	k1gInSc2	manýrismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
cituje	citovat	k5eAaBmIp3nS	citovat
známá	známá	k1gFnSc1	známá
díla	dílo	k1gNnSc2	dílo
(	(	kIx(	(
<g/>
Poslední	poslední	k2eAgFnSc1d1	poslední
snídaně	snídaně	k1gFnSc1	snídaně
v	v	k7c6	v
trávě	tráva	k1gFnSc6	tráva
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
díla	dílo	k1gNnPc4	dílo
starých	starý	k2eAgMnPc2d1	starý
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
-	-	kIx~	-
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
Vélasqueze	Vélasqueze	k1gFnPc4	Vélasqueze
<g/>
)	)	kIx)	)
a	a	k8xC	a
blíží	blížit	k5eAaImIp3nS	blížit
se	se	k3xPyFc4	se
iluzivní	iluzivní	k2eAgFnSc3d1	iluzivní
realistické	realistický	k2eAgFnSc3d1	realistická
malbě	malba	k1gFnSc3	malba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
figury	figura	k1gFnPc1	figura
získávají	získávat	k5eAaImIp3nP	získávat
objem	objem	k1gInSc4	objem
a	a	k8xC	a
realistické	realistický	k2eAgInPc4d1	realistický
detaily	detail	k1gInPc4	detail
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
perspektivně	perspektivně	k6eAd1	perspektivně
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
(	(	kIx(	(
<g/>
Amora	Amor	k1gMnSc2	Amor
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
,	,	kIx,	,
NG	NG	kA	NG
<g/>
,	,	kIx,	,
Zápasníci	zápasník	k1gMnPc1	zápasník
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
,	,	kIx,	,
GMU	GMU	kA	GMU
Roudnice	Roudnice	k1gFnSc1	Roudnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bohatá	bohatý	k2eAgFnSc1d1	bohatá
a	a	k8xC	a
symboly	symbol	k1gInPc7	symbol
přetížená	přetížený	k2eAgNnPc1d1	přetížené
obrazová	obrazový	k2eAgNnPc1d1	obrazové
dramata	drama	k1gNnPc1	drama
(	(	kIx(	(
<g/>
Pečlivý	pečlivý	k2eAgMnSc1d1	pečlivý
zahradník	zahradník	k1gMnSc1	zahradník
<g/>
,	,	kIx,	,
Skleník	skleník	k1gInSc1	skleník
s	s	k7c7	s
tajnosnubnou	tajnosnubný	k2eAgFnSc7d1	tajnosnubná
Venuší	Venuše	k1gFnSc7	Venuše
<g/>
)	)	kIx)	)
ukončují	ukončovat	k5eAaImIp3nP	ukončovat
tuto	tento	k3xDgFnSc4	tento
periodu	perioda	k1gFnSc4	perioda
Sionovy	Sionův	k2eAgFnSc2d1	Sionův
malby	malba	k1gFnSc2	malba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Sionových	Sionův	k2eAgInPc6d1	Sionův
obrazech	obraz	k1gInPc6	obraz
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
inspirace	inspirace	k1gFnSc2	inspirace
současným	současný	k2eAgInSc7d1	současný
světem	svět	k1gInSc7	svět
a	a	k8xC	a
transformovaná	transformovaný	k2eAgFnSc1d1	transformovaná
profánní	profánní	k2eAgFnSc1d1	profánní
realita	realita	k1gFnSc1	realita
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
nelítostných	lítostný	k2eNgFnPc2d1	nelítostná
moralit	moralita	k1gFnPc2	moralita
(	(	kIx(	(
<g/>
Tři	tři	k4xCgFnPc1	tři
grácie	grácie	k1gFnPc1	grácie
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Strhující	strhující	k2eAgInSc1d1	strhující
finiš	finiš	k1gInSc1	finiš
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
Rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
Bubliny	bublina	k1gFnSc2	bublina
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Sionův	Sionův	k2eAgMnSc1d1	Sionův
proces	proces	k1gInSc4	proces
pozvolného	pozvolný	k2eAgNnSc2d1	pozvolné
oprošťování	oprošťování	k1gNnSc2	oprošťování
od	od	k7c2	od
příběhu	příběh	k1gInSc2	příběh
v	v	k7c6	v
obrazu	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
od	od	k7c2	od
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
hlav	hlava	k1gFnPc2	hlava
posunul	posunout	k5eAaPmAgInS	posunout
ke	k	k7c3	k
krajinám	krajina	k1gFnPc3	krajina
(	(	kIx(	(
<g/>
Něco	něco	k6eAd1	něco
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Tato	tento	k3xDgFnSc1	tento
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
zbylo	zbýt	k5eAaPmAgNnS	zbýt
z	z	k7c2	z
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
končinou	končina	k1gFnSc7	končina
nesvolnou	svolný	k2eNgFnSc7d1	svolný
ani	ani	k8xC	ani
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pohřebištěm	pohřebiště	k1gNnSc7	pohřebiště
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
zbylo	zbýt	k5eAaPmAgNnS	zbýt
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
krajiny	krajina	k1gFnPc1	krajina
jsou	být	k5eAaImIp3nP	být
barevné	barevný	k2eAgFnPc1d1	barevná
a	a	k8xC	a
plné	plný	k2eAgFnPc1d1	plná
technických	technický	k2eAgFnPc2d1	technická
struktur	struktura	k1gFnPc2	struktura
a	a	k8xC	a
rastrů	rastr	k1gInPc2	rastr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
odlidštěnější	odlidštěný	k2eAgInSc1d2	odlidštěný
a	a	k8xC	a
chladnější	chladný	k2eAgInSc1d2	chladnější
než	než	k8xS	než
rané	raný	k2eAgFnPc1d1	raná
existenciální	existenciální	k2eAgFnPc1d1	existenciální
varianty	varianta	k1gFnPc1	varianta
informelních	informelní	k2eAgFnPc2d1	informelní
krajin	krajina	k1gFnPc2	krajina
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
Pod	pod	k7c7	pod
obzorem	obzor	k1gInSc7	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
napovídají	napovídat	k5eAaBmIp3nP	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sion	Sion	k1gMnSc1	Sion
nachází	nacházet	k5eAaImIp3nS	nacházet
témata	téma	k1gNnPc4	téma
v	v	k7c6	v
potenciálních	potenciální	k2eAgFnPc6d1	potenciální
hrozbách	hrozba	k1gFnPc6	hrozba
nové	nový	k2eAgFnSc2d1	nová
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
Genetické	genetický	k2eAgFnSc2d1	genetická
akrobacie	akrobacie	k1gFnSc2	akrobacie
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
88	[number]	k4	88
<g/>
,	,	kIx,	,
Vesmírné	vesmírný	k2eAgFnPc4d1	vesmírná
hrátky	hrátky	k1gFnPc4	hrátky
-	-	kIx~	-
Genetické	genetický	k2eAgFnPc4d1	genetická
akrobacie	akrobacie	k1gFnPc4	akrobacie
II	II	kA	II
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
obrazech	obraz	k1gInPc6	obraz
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
jako	jako	k9	jako
nový	nový	k2eAgInSc4d1	nový
prvek	prvek	k1gInSc4	prvek
objevují	objevovat	k5eAaImIp3nP	objevovat
abstraktní	abstraktní	k2eAgFnPc1d1	abstraktní
studie	studie	k1gFnPc1	studie
barevných	barevný	k2eAgFnPc2d1	barevná
struktur	struktura	k1gFnPc2	struktura
(	(	kIx(	(
<g/>
Kompas	kompas	k1gInSc1	kompas
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Mezery	mezera	k1gFnPc1	mezera
v	v	k7c6	v
trhlinách	trhlina	k1gFnPc6	trhlina
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
obrazy	obraz	k1gInPc1	obraz
komponované	komponovaný	k2eAgInPc1d1	komponovaný
jako	jako	k8xC	jako
dynamické	dynamický	k2eAgInPc1d1	dynamický
rozvinutí	rozvinutí	k1gNnSc4	rozvinutí
objektu	objekt	k1gInSc2	objekt
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
(	(	kIx(	(
<g/>
Roztančený	roztančený	k2eAgInSc4d1	roztančený
maják	maják	k1gInSc4	maják
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
často	často	k6eAd1	často
spojuje	spojovat	k5eAaImIp3nS	spojovat
vzájemně	vzájemně	k6eAd1	vzájemně
kontrastní	kontrastní	k2eAgInPc4d1	kontrastní
obrazy	obraz	k1gInPc4	obraz
do	do	k7c2	do
triptychů	triptych	k1gInPc2	triptych
plných	plný	k2eAgInPc2d1	plný
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
napětí	napětí	k1gNnSc2	napětí
(	(	kIx(	(
<g/>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
Mikuláše	Mikuláš	k1gMnSc4	Mikuláš
Medka	Medek	k1gMnSc4	Medek
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Třípatrový	třípatrový	k2eAgInSc1d1	třípatrový
obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
osobitý	osobitý	k2eAgInSc4d1	osobitý
a	a	k8xC	a
originální	originální	k2eAgInSc4d1	originální
přínos	přínos	k1gInSc4	přínos
k	k	k7c3	k
současnému	současný	k2eAgNnSc3d1	současné
modernímu	moderní	k2eAgNnSc3d1	moderní
umění	umění	k1gNnSc3	umění
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
Sionovy	Sionův	k2eAgFnPc4d1	Sionův
kombinace	kombinace	k1gFnPc4	kombinace
centrální	centrální	k2eAgFnSc2d1	centrální
abstraktní	abstraktní	k2eAgFnSc2d1	abstraktní
nebo	nebo	k8xC	nebo
figurální	figurální	k2eAgFnSc2d1	figurální
strukturované	strukturovaný	k2eAgFnSc2d1	strukturovaná
malby	malba	k1gFnSc2	malba
s	s	k7c7	s
autofrotážemi	autofrotáž	k1gFnPc7	autofrotáž
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
rámují	rámovat	k5eAaImIp3nP	rámovat
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
triptych	triptych	k1gInSc1	triptych
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
obraze	obraz	k1gInSc6	obraz
(	(	kIx(	(
<g/>
Morový	morový	k2eAgInSc4d1	morový
sloup	sloup	k1gInSc4	sloup
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
Diana	Diana	k1gFnSc1	Diana
na	na	k7c6	na
lovu	lov	k1gInSc6	lov
třikrát	třikrát	k6eAd1	třikrát
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sion	Sion	k1gInSc1	Sion
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
možnosti	možnost	k1gFnPc1	možnost
informelu	informel	k1gInSc2	informel
nebyly	být	k5eNaImAgFnP	být
zdaleka	zdaleka	k6eAd1	zdaleka
vyčerpány	vyčerpán	k2eAgFnPc1d1	vyčerpána
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
přestaly	přestat	k5eAaPmAgInP	přestat
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
historickým	historický	k2eAgFnPc3d1	historická
okolnostem	okolnost	k1gFnPc3	okolnost
hledány	hledán	k2eAgFnPc4d1	hledána
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Petra	Petr	k1gMnSc2	Petr
Nedomy	Nedoma	k1gMnSc2	Nedoma
<g/>
,	,	kIx,	,
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
trvale	trvale	k6eAd1	trvale
řeší	řešit	k5eAaImIp3nP	řešit
etickou	etický	k2eAgFnSc4d1	etická
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
zachovat	zachovat	k5eAaPmF	zachovat
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
postavený	postavený	k2eAgInSc1d1	postavený
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
realitou	realita	k1gFnSc7	realita
nebyl	být	k5eNaImAgInS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
a	a	k8xC	a
snad	snad	k9	snad
ani	ani	k8xC	ani
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
ztotožnit	ztotožnit	k5eAaPmF	ztotožnit
<g/>
.	.	kIx.	.
</s>
<s>
Klást	klást	k5eAaImF	klást
naléhavé	naléhavý	k2eAgFnPc4d1	naléhavá
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
pravdivě	pravdivě	k6eAd1	pravdivě
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
odpovídat	odpovídat	k5eAaImF	odpovídat
mohl	moct	k5eAaImAgMnS	moct
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
skryté	skrytý	k2eAgFnPc1d1	skrytá
a	a	k8xC	a
potlačované	potlačovaný	k2eAgFnPc1d1	potlačovaná
obsese	obsese	k1gFnPc1	obsese
a	a	k8xC	a
úzkosti	úzkost	k1gFnPc1	úzkost
nahrávaly	nahrávat	k5eAaImAgFnP	nahrávat
nerušeným	rušený	k2eNgNnSc7d1	nerušené
proudům	proud	k1gInPc3	proud
fantazijních	fantazijní	k2eAgInPc2d1	fantazijní
<g/>
,	,	kIx,	,
imaginativních	imaginativní	k2eAgInPc2d1	imaginativní
projevů	projev	k1gInPc2	projev
<g/>
,	,	kIx,	,
jinotajným	jinotajný	k2eAgFnPc3d1	jinotajná
narážkám	narážka	k1gFnPc3	narážka
a	a	k8xC	a
bohaté	bohatý	k2eAgFnSc3d1	bohatá
metaforice	metaforika	k1gFnSc3	metaforika
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc3	vytvoření
vlastního	vlastní	k2eAgInSc2d1	vlastní
komunikačního	komunikační	k2eAgInSc2d1	komunikační
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zastoupení	zastoupení	k1gNnPc4	zastoupení
ve	v	k7c6	v
sbírkách	sbírka	k1gFnPc6	sbírka
===	===	k?	===
</s>
</p>
<p>
<s>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
Vysočiny	vysočina	k1gFnSc2	vysočina
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Středočeského	středočeský	k2eAgInSc2d1	středočeský
kraje	kraj	k1gInSc2	kraj
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
</s>
</p>
<p>
<s>
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
</s>
</p>
<p>
<s>
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
Horácká	horácký	k2eAgFnSc1d1	Horácká
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Novém	nový	k2eAgNnSc6d1	nové
Městě	město	k1gNnSc6	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
</s>
</p>
<p>
<s>
Alšova	Alšův	k2eAgFnSc1d1	Alšova
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Hluboké	Hluboká	k1gFnSc6	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Benedikta	Benedikt	k1gMnSc2	Benedikt
Rejta	Rejt	k1gMnSc2	Rejt
v	v	k7c6	v
Lounech	Louny	k1gInPc6	Louny
</s>
</p>
<p>
<s>
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
Polička	Polička	k1gFnSc1	Polička
</s>
</p>
<p>
<s>
Galleria	Gallerium	k1gNnPc1	Gallerium
Arturo	Artura	k1gFnSc5	Artura
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
Milano	Milana	k1gFnSc5	Milana
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Schüppenhauer	Schüppenhauer	k1gInSc1	Schüppenhauer
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Volkmann	Volkmanna	k1gFnPc2	Volkmanna
<g/>
,	,	kIx,	,
Münster	Münstra	k1gFnPc2	Münstra
</s>
</p>
<p>
<s>
soukromé	soukromý	k2eAgFnPc1d1	soukromá
sbírky	sbírka	k1gFnPc1	sbírka
</s>
</p>
<p>
<s>
===	===	k?	===
Výstavy	výstava	k1gFnPc1	výstava
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Autorské	autorský	k2eAgMnPc4d1	autorský
====	====	k?	====
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
<g/>
,	,	kIx,	,
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
<g/>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Za	za	k7c7	za
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
GMU	GMU	kA	GMU
Roudnice	Roudnice	k1gFnPc1	Roudnice
byly	být	k5eAaImAgFnP	být
zakázána	zakázat	k5eAaPmNgFnS	zakázat
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Polička	Polička	k1gFnSc1	Polička
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
ZS	ZS	kA	ZS
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
kvaše	kvaš	k1gInPc1	kvaš
<g/>
,	,	kIx,	,
koláže	koláž	k1gFnPc1	koláž
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Benedikta	Benedikt	k1gMnSc2	Benedikt
Rejta	Rejt	k1gMnSc2	Rejt
<g/>
,	,	kIx,	,
Louny	Louny	k1gInPc1	Louny
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
Zbyšek	Zbyška	k1gFnPc2	Zbyška
Sion	Siona	k1gFnPc2	Siona
<g/>
:	:	kIx,	:
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Gema	gema	k1gFnSc1	gema
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
1958	[number]	k4	1958
-	-	kIx~	-
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
výstava	výstava	k1gFnSc1	výstava
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
Václava	Václav	k1gMnSc2	Václav
Špály	Špála	k1gFnSc2	Špála
se	se	k3xPyFc4	se
neuskutečnila	uskutečnit	k5eNaPmAgFnS	uskutečnit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Rané	raný	k2eAgFnPc1d1	raná
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
́	́	k?	́
<g/>
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
́	́	k?	́
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
1958	[number]	k4	1958
-	-	kIx~	-
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Rudolfinum	Rudolfinum	k1gNnSc1	Rudolfinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
<g/>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
poličské	poličský	k2eAgFnSc2d1	Poličská
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Kresby	kresba	k1gFnPc1	kresba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1955	[number]	k4	1955
-	-	kIx~	-
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Ztichlá	ztichlý	k2eAgFnSc1d1	ztichlá
klika	klika	k1gFnSc1	klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Nová	nový	k2eAgFnSc1d1	nová
síň	síň	k1gFnSc1	síň
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
Brno	Brno	k1gNnSc1	Brno
Gallery	Galler	k1gMnPc7	Galler
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
města	město	k1gNnSc2	město
Pardubic	Pardubice	k1gInPc2	Pardubice
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Sokolská	sokolská	k1gFnSc1	sokolská
26	[number]	k4	26
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Martinů	Martinů	k1gFnSc1	Martinů
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Poličce	Polička	k1gFnSc6	Polička
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
U	u	k7c2	u
mistra	mistr	k1gMnSc2	mistr
s	s	k7c7	s
palmou	palma	k1gFnSc7	palma
<g/>
,	,	kIx,	,
Náchod	Náchod	k1gInSc1	Náchod
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Společné	společný	k2eAgFnPc1d1	společná
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
<g/>
)	)	kIx)	)
====	====	k?	====
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Konfrontace	konfrontace	k1gFnSc1	konfrontace
II	II	kA	II
<g/>
,	,	kIx,	,
ateliér	ateliér	k1gInSc4	ateliér
Aleše	Aleš	k1gMnSc2	Aleš
Veselého	Veselý	k1gMnSc2	Veselý
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
Výstava	výstava	k1gFnSc1	výstava
dvanácti	dvanáct	k4xCc2	dvanáct
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
za	za	k7c7	za
branou	brána	k1gFnSc7	brána
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Celostátní	celostátní	k2eAgFnSc1d1	celostátní
výstava	výstava	k1gFnSc1	výstava
mladých	mladý	k1gMnPc2	mladý
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Mostra	Mostra	k1gFnSc1	Mostra
d	d	k?	d
<g/>
́	́	k?	́
<g/>
arte	artat	k5eAaPmIp3nS	artat
contemporanea	contemporane	k2eAgFnSc1d1	contemporane
cecoslovacca	cecoslovacca	k1gFnSc1	cecoslovacca
<g/>
,	,	kIx,	,
Castello	Castello	k1gNnSc1	Castello
del	del	k?	del
Valentino	Valentina	k1gFnSc5	Valentina
<g/>
,	,	kIx,	,
Turín	Turín	k1gInSc1	Turín
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Cinquiéme	Cinquiém	k1gInSc5	Cinquiém
Biennale	Biennale	k1gFnSc3	Biennale
des	des	k1gNnSc6	des
Jeunes	Jeunesa	k1gFnPc2	Jeunesa
<g/>
,	,	kIx,	,
Musée	Musée	k1gFnPc2	Musée
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Art	Art	k1gFnSc2	Art
Moderne	Modern	k1gInSc5	Modern
de	de	k?	de
la	la	k1gNnPc2	la
Ville	Ville	k1gNnPc2	Ville
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Fantasijní	fantasijní	k2eAgInPc1d1	fantasijní
aspekty	aspekt	k1gInPc1	aspekt
současného	současný	k2eAgNnSc2d1	současné
českého	český	k2eAgNnSc2d1	české
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
Vysočiny	vysočina	k1gFnSc2	vysočina
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Václava	Václav	k1gMnSc2	Václav
Špály	Špála	k1gMnSc2	Špála
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Künstlergruppe	Künstlergrupp	k1gInSc5	Künstlergrupp
"	"	kIx"	"
<g/>
Arche	Arche	k1gNnSc7	Arche
<g/>
"	"	kIx"	"
mit	mit	k?	mit
10	[number]	k4	10
Malern	Malern	k1gInSc1	Malern
aus	aus	k?	aus
Prag	Prag	k1gInSc1	Prag
<g/>
,	,	kIx,	,
Studio	studio	k1gNnSc1	studio
des	des	k1gNnSc2	des
Kunstkreises	Kunstkreisesa	k1gFnPc2	Kunstkreisesa
Hameln	Hameln	k1gMnSc1	Hameln
<g/>
,	,	kIx,	,
Henslerhaus	Henslerhaus	k1gMnSc1	Henslerhaus
Dortmund	Dortmund	k1gInSc1	Dortmund
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Lignano	Lignana	k1gFnSc5	Lignana
Biennale	Biennale	k1gFnPc3	Biennale
I	i	k9	i
<g/>
,	,	kIx,	,
Rassegna	Rassegen	k2eAgFnSc1d1	Rassegna
Internazionale	Internazionale	k1gFnSc1	Internazionale
d	d	k?	d
<g/>
́	́	k?	́
<g/>
arte	artat	k5eAaPmIp3nS	artat
contemporanea	contemporanea	k1gFnSc1	contemporanea
<g/>
,	,	kIx,	,
Lignano	Lignana	k1gFnSc5	Lignana
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Phases	Phases	k1gInSc1	Phases
<g/>
,	,	kIx,	,
une	une	k?	une
Internationale	Internationale	k1gMnSc5	Internationale
revolutionaire	revolutionair	k1gMnSc5	revolutionair
de	de	k?	de
l	l	kA	l
<g/>
́	́	k?	́
<g/>
Art	Art	k1gMnSc1	Art
Contemporain	Contemporain	k1gMnSc1	Contemporain
<g/>
,	,	kIx,	,
Lille	Lille	k1gFnSc1	Lille
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
300	[number]	k4	300
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
sochařů	sochař	k1gMnPc2	sochař
<g/>
,	,	kIx,	,
grafiků	grafik	k1gMnPc2	grafik
5	[number]	k4	5
generací	generace	k1gFnPc2	generace
k	k	k7c3	k
50	[number]	k4	50
létům	léto	k1gNnPc3	léto
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Sept	septum	k1gNnPc2	septum
jeunes	jeunes	k1gMnSc1	jeunes
peintres	peintres	k1gMnSc1	peintres
tchécoslovaques	tchécoslovaques	k1gMnSc1	tchécoslovaques
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Lambert	Lambert	k1gMnSc1	Lambert
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Surrealismus	surrealismus	k1gInSc1	surrealismus
in	in	k?	in
Europa	Europa	k1gFnSc1	Europa
-	-	kIx~	-
phantastische	phantastische	k1gFnSc1	phantastische
und	und	k?	und
visionäre	visionär	k1gMnSc5	visionär
Bereiche	Bereichus	k1gMnSc5	Bereichus
<g/>
,	,	kIx,	,
Baukunst-Galerie	Baukunst-Galerie	k1gFnSc1	Baukunst-Galerie
<g/>
,	,	kIx,	,
Kolín	Kolín	k1gInSc1	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Phases	Phases	k1gInSc1	Phases
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
Vysočiny	vysočina	k1gFnSc2	vysočina
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
Krajská	krajský	k2eAgFnSc1d1	krajská
galerie	galerie	k1gFnSc1	galerie
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
II	II	kA	II
<g/>
.	.	kIx.	.
pražský	pražský	k2eAgInSc1d1	pražský
salón	salón	k1gInSc1	salón
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
u	u	k7c2	u
Hybernů	hybern	k1gMnPc2	hybern
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
Phases	Phases	k1gInSc1	Phases
<g/>
,	,	kIx,	,
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Sopoty	sopot	k1gInPc1	sopot
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Surrealismi	Surrealis	k1gFnPc7	Surrealis
<g/>
,	,	kIx,	,
Taideskus	Taideskus	k1gInSc4	Taideskus
Retretti	Retretť	k1gFnSc2	Retretť
<g/>
,	,	kIx,	,
Punkaharju	Punkaharju	k1gFnSc2	Punkaharju
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
Grotesknost	grotesknost	k1gFnSc1	grotesknost
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Dialog	dialog	k1gInSc1	dialog
́	́	k?	́
<g/>
90	[number]	k4	90
<g/>
:	:	kIx,	:
Paris-Praha	Paris-Praha	k1gMnSc1	Paris-Praha
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Český	český	k2eAgInSc1d1	český
informel	informel	k1gInSc1	informel
<g/>
.	.	kIx.	.
</s>
<s>
Průkopníci	průkopník	k1gMnPc1	průkopník
abstrakce	abstrakce	k1gFnSc2	abstrakce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
radnice	radnice	k1gFnSc1	radnice
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Severočeská	severočeský	k2eAgFnSc1d1	Severočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
92	[number]	k4	92
K.Š.	K.Š.	k1gFnSc4	K.Š.
Křižovnická	Křižovnický	k2eAgFnSc1d1	Křižovnická
škola	škola	k1gFnSc1	škola
čistého	čistý	k2eAgInSc2d1	čistý
humoru	humor	k1gInSc2	humor
bez	bez	k7c2	bez
vtipu	vtip	k1gInSc2	vtip
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
Středočeská	středočeský	k2eAgFnSc1d1	Středočeská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Kabinet	kabinet	k1gInSc1	kabinet
grafiky	grafika	k1gFnSc2	grafika
<g/>
,	,	kIx,	,
Olomouc	Olomouc	k1gFnSc1	Olomouc
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Arte	Art	k1gInSc2	Art
contemporanea	contemporaneus	k1gMnSc2	contemporaneus
ceca	cecus	k1gMnSc2	cecus
e	e	k0	e
slovacca	slovaccum	k1gNnPc4	slovaccum
1950	[number]	k4	1950
-	-	kIx~	-
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Palazzo	Palazza	k1gFnSc5	Palazza
del	del	k?	del
Broletto	Broletta	k1gMnSc5	Broletta
<g/>
,	,	kIx,	,
Salone	salon	k1gInSc5	salon
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
arengo	arenga	k1gFnSc5	arenga
<g/>
,	,	kIx,	,
Novara	Novar	k1gMnSc4	Novar
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Situace	situace	k1gFnSc1	situace
92	[number]	k4	92
<g/>
,	,	kIx,	,
Mánes	Mánes	k1gMnSc1	Mánes
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
<g/>
/	/	kIx~	/
<g/>
94	[number]	k4	94
Záznam	záznam	k1gInSc4	záznam
nejrozmanitějších	rozmanitý	k2eAgInPc2d3	nejrozmanitější
faktorů	faktor	k1gInPc2	faktor
<g/>
...	...	k?	...
České	český	k2eAgNnSc1d1	české
malířství	malířství	k1gNnSc1	malířství
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
státních	státní	k2eAgFnPc2d1	státní
galerií	galerie	k1gFnPc2	galerie
<g/>
,	,	kIx,	,
Jízdárna	jízdárna	k1gFnSc1	jízdárna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Ohniska	ohnisko	k1gNnSc2	ohnisko
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
<g/>
,	,	kIx,	,
Městská	městský	k2eAgFnSc1d1	městská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
/	/	kIx~	/
<g/>
97	[number]	k4	97
Umění	umění	k1gNnSc2	umění
zastaveného	zastavený	k2eAgInSc2d1	zastavený
času	čas	k1gInSc2	čas
/	/	kIx~	/
Art	Art	k1gFnSc1	Art
when	whena	k1gFnPc2	whena
time	timat	k5eAaPmIp3nS	timat
stood	stood	k1gInSc1	stood
still	stillum	k1gNnPc2	stillum
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
výtvarná	výtvarný	k2eAgFnSc1d1	výtvarná
scéna	scéna	k1gFnSc1	scéna
1969	[number]	k4	1969
<g/>
-	-	kIx~	-
<g/>
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Černé	Černé	k2eAgFnSc2d1	Černé
Matky	matka	k1gFnSc2	matka
Boží	boží	k2eAgInSc1d1	boží
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Cheb	Cheb	k1gInSc1	Cheb
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
České	český	k2eAgNnSc1d1	české
imaginativní	imaginativní	k2eAgNnSc1d1	imaginativní
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Rudolfinum	Rudolfinum	k1gNnSc1	Rudolfinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
České	český	k2eAgInPc1d1	český
malířství	malířství	k1gNnSc2	malířství
napříč	napříč	k7c7	napříč
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
generacemi	generace	k1gFnPc7	generace
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc4	muzeum
V	v	k7c4	v
Preclíka	Preclík	k1gMnSc4	Preclík
v	v	k7c6	v
Bechyni	Bechyně	k1gFnSc6	Bechyně
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Podoby	podoba	k1gFnSc2	podoba
fantaskna	fantaskno	k1gNnSc2	fantaskno
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
výtvarném	výtvarný	k2eAgNnSc6d1	výtvarné
umění	umění	k1gNnSc6	umění
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Alšova	Alšův	k2eAgFnSc1d1	Alšova
jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Hluboké	Hluboká	k1gFnSc6	Hluboká
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Konec	konec	k1gInSc1	konec
světa	svět	k1gInSc2	svět
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
/	/	kIx~	/
<g/>
The	The	k1gMnPc7	The
End	End	k1gMnSc7	End
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
NG	NG	kA	NG
Palác	palác	k1gInSc1	palác
Kinských	Kinský	k1gMnPc2	Kinský
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Katalogy	katalog	k1gInPc4	katalog
a	a	k8xC	a
CD	CD	kA	CD
====	====	k?	====
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Práce	práce	k1gFnSc1	práce
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
1955	[number]	k4	1955
-	-	kIx~	-
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
ztichlá	ztichlý	k2eAgFnSc1d1	ztichlá
klika	klika	k1gFnSc1	klika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
poličské	poličský	k2eAgFnSc2d1	Poličská
galerie	galerie	k1gFnSc2	galerie
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
Polička	Polička	k1gFnSc1	Polička
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
1958	[number]	k4	1958
-	-	kIx~	-
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
Gema	gema	k1gFnSc1	gema
Art	Art	k1gMnSc1	Art
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Rané	raný	k2eAgFnPc1d1	raná
kresby	kresba	k1gFnPc1	kresba
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
́	́	k?	́
<g/>
60	[number]	k4	60
<g/>
/	/	kIx~	/
<g/>
́	́	k?	́
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Obrazy	obraz	k1gInPc1	obraz
1958	[number]	k4	1958
-	-	kIx~	-
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
moderního	moderní	k2eAgNnSc2d1	moderní
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Roudnici	Roudnice	k1gFnSc6	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
galerie	galerie	k1gFnSc1	galerie
Polička	Polička	k1gFnSc1	Polička
</s>
</p>
<p>
<s>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
:	:	kIx,	:
Švarná	švarný	k2eAgFnSc1d1	švarná
návštěva	návštěva	k1gFnSc1	návštěva
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
Hůla	Hůla	k1gMnSc1	Hůla
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
Sion	Sion	k1gMnSc1	Sion
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
<g/>
,	,	kIx,	,
CD	CD	kA	CD
rom	rom	k?	rom
</s>
</p>
<p>
<s>
====	====	k?	====
Publikace	publikace	k1gFnSc1	publikace
====	====	k?	====
</s>
</p>
<p>
<s>
Šmejkal	Šmejkal	k1gMnSc1	Šmejkal
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
Fantasijní	fantasijní	k2eAgMnSc1d1	fantasijní
aspekty	aspekt	k1gInPc7	aspekt
současného	současný	k2eAgNnSc2d1	současné
českého	český	k2eAgNnSc2d1	české
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
Oblastní	oblastní	k2eAgFnSc1d1	oblastní
galerie	galerie	k1gFnSc1	galerie
Vysočiny	vysočina	k1gFnSc2	vysočina
v	v	k7c6	v
Jihlavě	Jihlava	k1gFnSc6	Jihlava
<g/>
,	,	kIx,	,
1967	[number]	k4	1967
</s>
</p>
<p>
<s>
Bénamou	Bénamý	k2eAgFnSc4d1	Bénamý
Geneviè	Geneviè	k1gFnSc4	Geneviè
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
́	́	k?	́
<g/>
art	art	k?	art
aujourd	aujourd	k1gInSc1	aujourd
<g/>
́	́	k?	́
<g/>
hui	hui	k?	hui
en	en	k?	en
Tchecoslovaquie	Tchecoslovaquie	k1gFnSc2	Tchecoslovaquie
<g/>
,	,	kIx,	,
Geneviè	Geneviè	k1gMnSc7	Geneviè
Bénamou	Bénama	k1gMnSc7	Bénama
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
Anděl	Anděl	k1gMnSc1	Anděl
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Informel	Informel	k1gInSc1	Informel
<g/>
:	:	kIx,	:
Sborník	sborník	k1gInSc1	sborník
symposia	symposion	k1gNnSc2	symposion
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
sborník	sborník	k1gInSc1	sborník
71	[number]	k4	71
s.	s.	k?	s.
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc1	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
(	(	kIx(	(
<g/>
archiv	archiv	k1gInSc1	archiv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Nešlehová	Nešlehový	k2eAgFnSc1d1	Nešlehový
Mahulena	Mahulena	k1gFnSc1	Mahulena
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
informel	informel	k1gInSc1	informel
<g/>
,	,	kIx,	,
Průkopníci	průkopník	k1gMnPc1	průkopník
abstrakce	abstrakce	k1gFnSc2	abstrakce
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1957	[number]	k4	1957
<g/>
-	-	kIx~	-
<g/>
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
268	[number]	k4	268
s.	s.	k?	s.
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
SGVU	SGVU	kA	SGVU
Litoměřice	Litoměřice	k1gInPc1	Litoměřice
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Pečinková	Pečinkový	k2eAgFnSc1d1	Pečinková
Pavla	Pavla	k1gFnSc1	Pavla
<g/>
,	,	kIx,	,
Contemporary	Contemporara	k1gFnSc2	Contemporara
Czech	Czecha	k1gFnPc2	Czecha
Painting	Painting	k1gInSc1	Painting
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
Arts	Arts	k1gInSc1	Arts
International	International	k1gMnPc2	International
Limited	limited	k2eAgMnSc1d1	limited
<g/>
,	,	kIx,	,
East	East	k2eAgInSc1d1	East
Roseville	Roseville	k1gInSc1	Roseville
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
s.	s.	k?	s.
138-141	[number]	k4	138-141
</s>
</p>
<p>
<s>
Chalupecký	Chalupecký	k2eAgMnSc1d1	Chalupecký
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
umění	umění	k1gNnSc1	umění
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
a	a	k8xC	a
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
H	H	kA	H
<g/>
&	&	k?	&
<g/>
H	H	kA	H
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Jinočany	Jinočan	k1gMnPc4	Jinočan
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Bregant	Bregant	k1gMnSc1	Bregant
Michal	Michal	k1gMnSc1	Michal
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ohniska	ohnisko	k1gNnSc2	ohnisko
znovuzrození	znovuzrození	k1gNnSc2	znovuzrození
(	(	kIx(	(
<g/>
České	český	k2eAgNnSc1d1	české
umění	umění	k1gNnSc1	umění
1956	[number]	k4	1956
-	-	kIx~	-
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
807010029X	[number]	k4	807010029X
</s>
</p>
<p>
<s>
Šmejkal	Šmejkal	k1gMnSc1	Šmejkal
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
České	český	k2eAgNnSc1d1	české
imaginativní	imaginativní	k2eAgNnSc1d1	imaginativní
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Rudolfinum	Rudolfinum	k1gNnSc1	Rudolfinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
Nešlehová	Nešlehový	k2eAgFnSc1d1	Nešlehový
Mahulena	Mahulena	k1gFnSc1	Mahulena
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Poselství	poselství	k1gNnSc1	poselství
jiného	jiný	k2eAgInSc2d1	jiný
výrazu	výraz	k1gInSc2	výraz
<g/>
:	:	kIx,	:
Pojetí	pojetí	k1gNnSc1	pojetí
informelu	informel	k1gInSc2	informel
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
umění	umění	k1gNnSc6	umění
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
286	[number]	k4	286
s.	s.	k?	s.
<g/>
,	,	kIx,	,
Artefact	Artefact	k1gInSc1	Artefact
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
BASE	bas	k1gInSc6	bas
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-902481-0-1	[number]	k4	80-902481-0-1
(	(	kIx(	(
<g/>
BASE	bas	k1gInSc6	bas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
80-902160-0-5	[number]	k4	80-902160-0-5
(	(	kIx(	(
<g/>
ARetFACT	ARetFACT	k1gFnSc2	ARetFACT
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Junek	Junek	k1gMnSc1	Junek
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Konečný	Konečný	k1gMnSc1	Konečný
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Poličky	Polička	k1gFnSc2	Polička
<g/>
,	,	kIx,	,
Argo	Argo	k6eAd1	Argo
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
9788025714577	[number]	k4	9788025714577
<g/>
,	,	kIx,	,
s.	s.	k?	s.
537	[number]	k4	537
<g/>
,	,	kIx,	,
546	[number]	k4	546
</s>
</p>
<p>
<s>
====	====	k?	====
Články	článek	k1gInPc1	článek
====	====	k?	====
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Wittlich	Wittlich	k1gMnSc1	Wittlich
<g/>
,	,	kIx,	,
Barokní	barokní	k2eAgFnSc1d1	barokní
lekce	lekce	k1gFnSc1	lekce
<g/>
,	,	kIx,	,
Výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
č.	č.	k?	č.
6	[number]	k4	6
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zbyšek	Zbyška	k1gFnPc2	Zbyška
Sion	Sion	k1gNnSc4	Sion
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
<g/>
:	:	kIx,	:
Sion	Sion	k1gMnSc1	Sion
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
</s>
</p>
<p>
<s>
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
Ztichlá	ztichlý	k2eAgFnSc1d1	ztichlá
klika	klika	k1gFnSc1	klika
<g/>
,	,	kIx,	,
Pavla	Pavla	k1gFnSc1	Pavla
Pečinková	Pečinkový	k2eAgFnSc1d1	Pečinková
</s>
</p>
<p>
<s>
Výstavní	výstavní	k2eAgFnSc1d1	výstavní
síň	síň	k1gFnSc1	síň
Sokolská	sokolská	k1gFnSc1	sokolská
26	[number]	k4	26
<g/>
:	:	kIx,	:
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
Sion	Sion	k1gMnSc1	Sion
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
Informační	informační	k2eAgInSc1d1	informační
systém	systém	k1gInSc1	systém
abART	abART	k?	abART
–	–	k?	–
osoba	osoba	k1gFnSc1	osoba
<g/>
:	:	kIx,	:
<g/>
Sion	Sion	k1gMnSc1	Sion
Zbyšek	Zbyšek	k1gMnSc1	Zbyšek
</s>
</p>
