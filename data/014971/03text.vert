<s>
Josef	Josef	k1gMnSc1
Seger	Seger	k?
</s>
<s>
Josef	Josef	k1gMnSc1
SegerZákladní	SegerZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnPc4
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1716	#num#	k4
ŘepínČeské	ŘepínČeský	k2eAgNnSc4d1
království	království	k1gNnSc4
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1782	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
66	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
České	český	k2eAgNnSc1d1
království	království	k1gNnSc1
Žánry	žánr	k1gInPc7
</s>
<s>
klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
a	a	k8xC
duchovní	duchovní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
,	,	kIx,
varhaník	varhaník	k1gMnSc1
a	a	k8xC
houslista	houslista	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
varhany	varhany	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
Seger	Seger	k?
(	(	kIx(
<g/>
v	v	k7c6
matrice	matrika	k1gFnSc6
zapsán	zapsat	k5eAaPmNgMnS
jako	jako	k9
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Segert	Segert	k1gMnSc1
<g/>
,	,	kIx,
příjmení	příjmení	k1gNnSc1
je	být	k5eAaImIp3nS
psáno	psát	k5eAaImNgNnS
také	také	k9
Seeger	Seegra	k1gFnPc2
nebo	nebo	k8xC
Seegr	Seegra	k1gFnPc2
<g/>
)	)	kIx)
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1716	#num#	k4
<g/>
,	,	kIx,
Řepín	Řepín	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1782	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
,	,	kIx,
houslista	houslista	k1gMnSc1
a	a	k8xC
varhaník	varhaník	k1gMnSc1
a	a	k8xC
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Matky	matka	k1gFnSc2
Boží	božit	k5eAaImIp3nS
před	před	k7c7
Týnem	Týn	k1gInSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Řepíně	Řepína	k1gFnSc6
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
Mělníka	Mělník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
jezuitském	jezuitský	k2eAgNnSc6d1
gymnáziu	gymnázium	k1gNnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
později	pozdě	k6eAd2
na	na	k7c6
Filosofické	filosofický	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
Karlovy	Karlův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dětství	dětství	k1gNnSc2
se	se	k3xPyFc4
živil	živit	k5eAaImAgMnS
hudbou	hudba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
studií	studie	k1gFnPc2
na	na	k7c6
gymnáziu	gymnázium	k1gNnSc6
byl	být	k5eAaImAgInS
altistou	altista	k1gMnSc7
v	v	k7c6
chrámovém	chrámový	k2eAgInSc6d1
sboru	sbor	k1gInSc6
kostela	kostel	k1gInSc2
svatého	svatý	k2eAgMnSc2d1
Jakuba	Jakub	k1gMnSc2
Většího	veliký	k2eAgMnSc2d2
na	na	k7c6
Starém	starý	k2eAgNnSc6d1
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrapunkt	kontrapunkt	k1gInSc1
studoval	studovat	k5eAaImAgInS
u	u	k7c2
Felixe	Felix	k1gMnSc4
Bendy	Benda	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
hře	hra	k1gFnSc6
na	na	k7c4
varhany	varhany	k1gInPc4
byl	být	k5eAaImAgMnS
jeho	jeho	k3xOp3gMnSc7
učitelem	učitel	k1gMnSc7
Bohuslav	Bohuslav	k1gMnSc1
Matěj	Matěj	k1gMnSc1
Černohorský	Černohorský	k1gMnSc1
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
jeho	jeho	k3xOp3gMnPc7
učiteli	učitel	k1gMnPc7
byli	být	k5eAaImAgMnP
Jan	Jan	k1gMnSc1
Zach	Zach	k1gMnSc1
a	a	k8xC
František	František	k1gMnSc1
Ignác	Ignác	k1gMnSc1
Tůma	Tůma	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
Janem	Jan	k1gMnSc7
Zachem	Zach	k1gMnSc7
byl	být	k5eAaImAgMnS
také	také	k9
houslistou	houslista	k1gMnSc7
v	v	k7c6
kostele	kostel	k1gInSc6
sv.	sv.	kA
Martina	Martin	k1gMnSc2
ve	v	k7c6
zdi	zeď	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
roku	rok	k1gInSc2
1741	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
varhaníkem	varhaník	k1gMnSc7
v	v	k7c6
kostele	kostel	k1gInSc6
Matky	matka	k1gFnSc2
Boží	božit	k5eAaImIp3nS
před	před	k7c7
Týnem	Týn	k1gInSc7
a	a	k8xC
s	s	k7c7
povolením	povolení	k1gNnSc7
pražského	pražský	k2eAgInSc2d1
magistrátu	magistrát	k1gInSc2
zastával	zastávat	k5eAaImAgMnS
současně	současně	k6eAd1
i	i	k9
stejnou	stejný	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
v	v	k7c6
kostele	kostel	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
z	z	k7c2
Assisi	Assise	k1gFnSc3
(	(	kIx(
<g/>
U	u	k7c2
křižovníků	křižovník	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
nutnosti	nutnost	k1gFnSc2
se	se	k3xPyFc4
v	v	k7c6
Týnském	týnský	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
nechával	nechávat	k5eAaImAgMnS
zastupovat	zastupovat	k5eAaImF
svými	svůj	k3xOyFgMnPc7
žáky	žák	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc4
funkce	funkce	k1gFnPc4
zastával	zastávat	k5eAaImAgMnS
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1782	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rok	rok	k1gInSc1
před	před	k7c7
smrtí	smrt	k1gFnSc7
jej	on	k3xPp3gMnSc4
v	v	k7c6
Praze	Praha	k1gFnSc6
slyšel	slyšet	k5eAaImAgMnS
hrát	hrát	k5eAaImF
císař	císař	k1gMnSc1
Josef	Josef	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
nabídl	nabídnout	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
místo	místo	k1gNnSc4
u	u	k7c2
svého	svůj	k3xOyFgInSc2
dvora	dvůr	k1gInSc2
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Než	než	k8xS
však	však	k9
do	do	k7c2
Prahy	Praha	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
oficiální	oficiální	k2eAgNnSc4d1
pozvání	pozvání	k1gNnSc4
<g/>
,	,	kIx,
skladatel	skladatel	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Seger	Seger	k?
byl	být	k5eAaImAgMnS
vynikající	vynikající	k2eAgMnSc1d1
pedagog	pedagog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vychoval	vychovat	k5eAaPmAgMnS
celou	celý	k2eAgFnSc4d1
generaci	generace	k1gFnSc4
znamenitých	znamenitý	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
varhaníků	varhaník	k1gMnPc2
i	i	k8xC
skladatelů	skladatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gMnPc4
žáky	žák	k1gMnPc4
byli	být	k5eAaImAgMnP
např.	např.	kA
Karel	Karel	k1gMnSc1
Blažej	Blažej	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Koželuh	koželuh	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Křtitel	křtitel	k1gMnSc1
Kuchař	Kuchař	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
Mysliveček	Mysliveček	k1gMnSc1
<g/>
,	,	kIx,
Vincenc	Vincenc	k1gMnSc1
Mašek	Mašek	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
Pichl	Pichl	k1gMnSc1
a	a	k8xC
mnoho	mnoho	k4c4
dalších	další	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Seger	Seger	k?
byl	být	k5eAaImAgInS
velice	velice	k6eAd1
plodný	plodný	k2eAgMnSc1d1
autor	autor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkomponoval	zkomponovat	k5eAaPmAgMnS
stovky	stovka	k1gFnPc4
fug	fuga	k1gFnPc2
<g/>
,	,	kIx,
tokát	tokáta	k1gFnPc2
<g/>
,	,	kIx,
preludií	preludium	k1gFnPc2
<g/>
,	,	kIx,
chorálových	chorálový	k2eAgFnPc2d1
předeher	předehra	k1gFnPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
drobnějších	drobný	k2eAgFnPc2d2
skladeb	skladba	k1gFnPc2
pro	pro	k7c4
varhany	varhany	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
řadu	řada	k1gFnSc4
mší	mše	k1gFnPc2
<g/>
,	,	kIx,
motet	moteto	k1gNnPc2
a	a	k8xC
žalmů	žalm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skladby	skladba	k1gFnSc2
kolovaly	kolovat	k5eAaImAgInP
v	v	k7c6
rukopisech	rukopis	k1gInPc6
a	a	k8xC
jsou	být	k5eAaImIp3nP
rozšířeny	rozšířit	k5eAaPmNgInP
v	v	k7c6
pražských	pražský	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
(	(	kIx(
<g/>
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
Strahovský	strahovský	k2eAgInSc4d1
klášter	klášter	k1gInSc4
<g/>
,	,	kIx,
u	u	k7c2
křižovníků	křižovník	k1gMnPc2
<g/>
,	,	kIx,
Břevnovský	břevnovský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
mimo	mimo	k7c4
Prahu	Praha	k1gFnSc4
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
Roudnici	Roudnice	k1gFnSc6
a	a	k8xC
Žamberku	Žamberk	k1gInSc6
a	a	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
(	(	kIx(
<g/>
Vratislav	Vratislav	k1gMnSc1
<g/>
,	,	kIx,
Lipsko	Lipsko	k1gNnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
odlišení	odlišení	k1gNnSc1
skladeb	skladba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
skutečně	skutečně	k6eAd1
zkomponoval	zkomponovat	k5eAaPmAgMnS
od	od	k7c2
skladeb	skladba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mu	on	k3xPp3gNnSc3
jsou	být	k5eAaImIp3nP
pouze	pouze	k6eAd1
připisovány	připisovat	k5eAaImNgInP
<g/>
.	.	kIx.
</s>
<s>
Tiskem	tisk	k1gInSc7
vyšly	vyjít	k5eAaPmAgFnP
Segerovy	Segerův	k2eAgFnPc1d1
skladby	skladba	k1gFnPc1
až	až	k9
po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1793	#num#	k4
vydal	vydat	k5eAaPmAgMnS
D.	D.	kA
G.	G.	kA
Türk	Türk	k1gMnSc1
v	v	k7c6
Lipsku	Lipsko	k1gNnSc6
8	#num#	k4
toccat	toccata	k1gFnPc2
a	a	k8xC
fug	fuga	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1803	#num#	k4
pak	pak	k8xC
J.	J.	kA
Polt	polt	k1gInSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
publikoval	publikovat	k5eAaBmAgMnS
10	#num#	k4
preludií	preludie	k1gFnPc2
pro	pro	k7c4
varhany	varhany	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
Segerovy	Segerův	k2eAgFnPc1d1
skladby	skladba	k1gFnPc1
často	často	k6eAd1
objevují	objevovat	k5eAaImIp3nP
v	v	k7c6
různých	různý	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
varhanní	varhanní	k2eAgFnSc2d1
tvorby	tvorba	k1gFnSc2
<g/>
,	,	kIx,
učebnicích	učebnice	k1gFnPc6
kontrapunktu	kontrapunkt	k1gInSc2
a	a	k8xC
školách	škola	k1gFnPc6
hry	hra	k1gFnSc2
na	na	k7c4
varhany	varhany	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
klasicismu	klasicismus	k1gInSc2
a	a	k8xC
romantismu	romantismus	k1gInSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gNnPc4
díla	dílo	k1gNnPc4
na	na	k7c4
čas	čas	k1gInSc4
zapomenuta	zapomnět	k5eAaImNgNnP,k5eAaPmNgNnP
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
znovu	znovu	k6eAd1
objevena	objevit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Varhany	varhany	k1gFnPc1
</s>
<s>
8	#num#	k4
Toccaten	Toccaten	k2eAgInSc1d1
und	und	k?
Fugen	Fugen	k1gInSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
D.G.	D.G.	k1gMnSc1
Türk	Türk	k1gMnSc1
(	(	kIx(
<g/>
Leipzig	Leipzig	k1gMnSc1
<g/>
,	,	kIx,
1793	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
preludes	preludes	k1gInSc1
<g/>
,	,	kIx,
in	in	k?
Sammlung	Sammlung	k1gInSc1
von	von	k1gInSc1
Präludien	Präludien	k2eAgInSc1d1
<g/>
,	,	kIx,
Fugen	Fugen	k2eAgInSc1d1
<g/>
,	,	kIx,
ausgeführten	ausgeführten	k2eAgInSc1d1
Chorälen	Chorälen	k1gInSc1
…	…	k?
von	von	k1gInSc1
berühmten	berühmten	k2eAgMnSc1d1
ältern	ältern	k1gMnSc1
Meistern	Meistern	k1gMnSc1
(	(	kIx(
<g/>
Leipzig	Leipzig	k1gMnSc1
<g/>
,	,	kIx,
1795	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
10	#num#	k4
Praeludien	Praeludien	k1gInSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
J.	J.	kA
Polt	polt	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1803	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
preludes	preludesa	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
fugues	fuguesa	k1gFnPc2
<g/>
,	,	kIx,
Toccata	toccata	k1gFnSc1
<g/>
,	,	kIx,
Fughetta	Fughetta	k1gFnSc1
<g/>
,	,	kIx,
in	in	k?
Fugen	Fugen	k1gInSc1
und	und	k?
Praeludien	Praeludien	k2eAgInSc1d1
von	von	k1gInSc1
älteren	älterna	k1gFnPc2
vaterländischen	vaterländischna	k1gFnPc2
Compositoren	Compositorna	k1gFnPc2
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verein	Verein	k1gInSc1
der	drát	k5eAaImRp2nS
Kunstfreunde	Kunstfreund	k1gInSc5
für	für	k?
Kirchenmusik	Kirchenmusik	k1gInSc1
in	in	k?
Böhmen	Böhmen	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
1832	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
cca	cca	kA
70	#num#	k4
skladeb	skladba	k1gFnPc2
připisovaných	připisovaný	k2eAgFnPc2d1
Segerovi	Seger	k1gMnSc3
v	v	k7c4
Museum	museum	k1gNnSc4
für	für	k?
Orgel-Spieler	Orgel-Spieler	k1gInSc1
ed	ed	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
C.	C.	kA
<g/>
F.	F.	kA
Pitsch	Pitsch	k1gMnSc1
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Prague	Prague	k1gNnSc1
<g/>
,	,	kIx,
1832	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mše	mše	k1gFnSc1
</s>
<s>
Missa	Missa	k1gFnSc1
quadragesimalis	quadragesimalis	k1gFnSc2
F-dur	F-dura	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
a	a	k8xC
varhany	varhany	k1gInPc4
</s>
<s>
Mass	Mass	k1gInSc1
in	in	k?
D	D	kA
minor	minor	k2eAgFnSc1d1
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
<g/>
,	,	kIx,
2	#num#	k4
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
2	#num#	k4
trombóny	trombón	k1gInPc1
a	a	k8xC
varhany	varhany	k1gInPc1
</s>
<s>
Mass	Mass	k1gInSc1
in	in	k?
D	D	kA
minor	minor	k2eAgFnSc1d1
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
<g/>
,	,	kIx,
2	#num#	k4
housle	housle	k1gFnPc1
a	a	k8xC
varhany	varhany	k1gFnPc1
</s>
<s>
Missa	Missa	k1gFnSc1
choralis	choralis	k1gFnSc2
Es-dur	Es-dura	k1gFnPc2
<g/>
,	,	kIx,
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
a	a	k8xC
varhany	varhany	k1gInPc4
concertante	concertant	k1gMnSc5
</s>
<s>
Jiná	jiný	k2eAgNnPc1d1
díla	dílo	k1gNnPc1
</s>
<s>
Alma	alma	k1gFnSc1
Redemptoris	Redemptoris	k1gFnSc2
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
<g/>
,	,	kIx,
housle	housle	k1gFnPc4
<g/>
,	,	kIx,
violu	viola	k1gFnSc4
a	a	k8xC
varhany	varhany	k1gFnPc4
</s>
<s>
Audi	Audi	k1gNnSc1
filia	filium	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
a	a	k8xC
varhany	varhany	k1gInPc4
</s>
<s>
Ave	ave	k1gNnSc1
regina	regino	k1gNnSc2
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
2	#num#	k4
housle	housle	k1gFnPc1
a	a	k8xC
varhany	varhany	k1gFnPc1
</s>
<s>
Christus	Christus	k1gMnSc1
nobis	nobis	k1gFnSc2
natus	natus	k1gMnSc1
est	est	k?
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
<g/>
,	,	kIx,
smyčce	smyčec	k1gInPc4
a	a	k8xC
varhany	varhany	k1gInPc4
</s>
<s>
Compieta	Compieta	k1gFnSc1
(	(	kIx(
<g/>
comprising	comprising	k1gInSc1
Cum	Cum	k1gFnSc2
invocarem	invocar	k1gInSc7
<g/>
,	,	kIx,
In	In	k1gFnSc7
te	te	k?
Domine	Domin	k1gMnSc5
<g/>
,	,	kIx,
Qui	Qui	k1gFnSc4
habitat	habitat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
Ecce	Ecce	k1gFnSc1
nunc	nunc	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Nunc	Nunc	k1gInSc1
dimittis	dimittis	k1gInSc1
<g/>
)	)	kIx)
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
2	#num#	k4
housle	housle	k1gFnPc1
a	a	k8xC
varhany	varhany	k1gFnPc1
</s>
<s>
Litaniae	Litaniae	k1gFnSc1
de	de	k?
sanctissimo	sanctissimo	k6eAd1
sacramento	sacramento	k1gNnSc1
pro	pro	k7c4
4	#num#	k4
hlasy	hlas	k1gInPc4
2	#num#	k4
housle	housle	k1gFnPc1
a	a	k8xC
varhany	varhany	k1gFnPc1
</s>
<s>
cca	cca	kA
200	#num#	k4
cvičení	cvičení	k1gNnPc2
generálního	generální	k2eAgInSc2d1
basu	bas	k1gInSc2
vydaných	vydaný	k2eAgInPc2d1
v	v	k7c6
různých	různý	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
a	a	k8xC
známých	známá	k1gFnPc6
jako	jako	k8xC,k8xS
Fondamenta	Fondamento	k1gNnSc2
pro	pro	k7c4
organo	organa	k1gFnSc5
<g/>
,	,	kIx,
Generalbass-Übungsstücke	Generalbass-Übungsstücke	k1gFnSc5
<g/>
,	,	kIx,
Orgel-Übungsstücke	Orgel-Übungsstücke	k1gFnSc5
<g/>
,	,	kIx,
apod.	apod.	kA
</s>
<s>
Edice	edice	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Seger	Seger	k?
<g/>
:	:	kIx,
Composizioni	Composizion	k1gMnPc1
per	pero	k1gNnPc2
organo	organa	k1gFnSc5
-	-	kIx~
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Seger	Seger	k?
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Musica	Musica	k1gMnSc1
antiqua	antiqua	k1gMnSc1
Bohemica	Bohemica	k1gMnSc1
<g/>
;	;	kIx,
51	#num#	k4
<g/>
,	,	kIx,
Band	band	k1gInSc1
<g/>
:	:	kIx,
1	#num#	k4
Preludi	Prelud	k1gMnPc1
<g/>
,	,	kIx,
toccate	toccat	k1gInSc5
e	e	k0
fughe	fughat	k5eAaPmIp3nS
I-XXXVI	I-XXXVI	k1gFnSc4
<g/>
.	.	kIx.
1961	#num#	k4
<g/>
,	,	kIx,
111	#num#	k4
p.	p.	k?
</s>
<s>
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Seger	Seger	k?
<g/>
:	:	kIx,
Composizioni	Composizion	k1gMnPc1
per	pero	k1gNnPc2
organo	organa	k1gFnSc5
-	-	kIx~
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Seger	Seger	k?
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Musica	Musica	k1gMnSc1
antiqua	antiqua	k1gMnSc1
Bohemica	Bohemica	k1gMnSc1
<g/>
;	;	kIx,
56	#num#	k4
<g/>
,	,	kIx,
Band	band	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
Preludi	Prelud	k1gMnPc1
e	e	k0
fughe	fughat	k5eAaPmIp3nS
I-XXI	I-XXI	k1gFnSc4
<g/>
.	.	kIx.
1962	#num#	k4
<g/>
.	.	kIx.
126	#num#	k4
p.	p.	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Josef	Josef	k1gMnSc1
Srb	Srb	k1gMnSc1
Debrnov	Debrnov	k1gInSc1
<g/>
:	:	kIx,
Slovník	slovník	k1gInSc1
hudebních	hudební	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
slovanských	slovanský	k2eAgFnPc2d1
</s>
<s>
The	The	k?
New	New	k1gFnSc1
Grove	Groev	k1gFnSc2
Dictionary	Dictionara	k1gFnSc2
of	of	k?
Music	Music	k1gMnSc1
and	and	k?
Musicians	Musicians	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Second	Second	k1gInSc1
Edition	Edition	k1gInSc4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Tyrrell	Tyrrell	k1gMnSc1
<g/>
,	,	kIx,
29	#num#	k4
dílů	díl	k1gInPc2
<g/>
,	,	kIx,
London	London	k1gMnSc1
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Československý	československý	k2eAgInSc1d1
hudební	hudební	k2eAgInSc1d1
slovník	slovník	k1gInSc1
osob	osoba	k1gFnPc2
a	a	k8xC
institucí	instituce	k1gFnPc2
II	II	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
M	M	kA
<g/>
–	–	k?
<g/>
Ž	Ž	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1965	#num#	k4
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
hudební	hudební	k2eAgNnSc1d1
vydavatelství	vydavatelství	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Hora	hora	k1gFnSc1
<g/>
,	,	kIx,
J.	J.	kA
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Seger	Seger	k?
-	-	kIx~
Organist	Organist	k1gMnSc1
der	drát	k5eAaImRp2nS
Teynkirche	Teynkirche	k1gInSc1
in	in	k?
den	den	k1gInSc4
Jahren	Jahrna	k1gFnPc2
1741	#num#	k4
<g/>
–	–	k?
<g/>
1782	#num#	k4
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
Cerný	Cerný	k1gMnSc1
<g/>
,	,	kIx,
Jaromír	Jaromír	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Koch	Koch	k1gMnSc1
<g/>
,	,	kIx,
Klaus-Peter	Klaus-Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mitteleuropäische	Mitteleuropäisch	k1gInSc2
Aspekte	aspekt	k1gInSc5
des	des	k1gNnSc7
Orgelbaus	Orgelbaus	k1gInSc1
und	und	k?
der	drát	k5eAaImRp2nS
geistlichen	geistlichna	k1gFnPc2
Musik	musika	k1gFnPc2
in	in	k?
Prag	Prag	k1gInSc1
und	und	k?
den	den	k1gInSc4
böhmischen	böhmischen	k2eAgInSc4d1
Ländern	Ländern	k1gInSc4
<g/>
,	,	kIx,
Konferenzbericht	Konferenzbericht	k2eAgInSc4d1
Prag	Prag	k1gInSc4
17	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
September	September	k1gInSc1
2000	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
3-89564-073-5	3-89564-073-5	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Josef	Josef	k1gMnSc1
Seger	Seger	k?
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Josef	Josef	k1gMnSc1
Seger	Seger	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Josef	Josef	k1gMnSc1
Seger	Seger	k?
</s>
<s>
Volně	volně	k6eAd1
přístupné	přístupný	k2eAgFnPc4d1
partitury	partitura	k1gFnPc4
děl	dít	k5eAaBmAgMnS,k5eAaImAgMnS
od	od	k7c2
Josefa	Josef	k1gMnSc2
Segera	Seger	k1gMnSc2
v	v	k7c6
projektu	projekt	k1gInSc6
IMSLP	IMSLP	kA
</s>
<s>
Ukázky	ukázka	k1gFnPc1
skladeb	skladba	k1gFnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
mzk	mzk	k?
<g/>
2002144945	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
102499683	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1038	#num#	k4
0121	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
88008148	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
10113278	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
88008148	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
