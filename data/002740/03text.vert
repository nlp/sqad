<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
.	.	kIx.	.
</s>
<s>
Pavlovské	pavlovský	k2eAgInPc4d1	pavlovský
vrchy	vrch	k1gInPc4	vrch
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Pollauer	Pollauer	k1gInSc1	Pollauer
Berge	Berge	k1gFnPc2	Berge
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
názvy	název	k1gInPc4	název
odvozeny	odvozen	k2eAgInPc4d1	odvozen
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
Pavlov	Pavlov	k1gInSc1	Pavlov
<g/>
,	,	kIx,	,
něm.	něm.	k?	něm.
Pollau	Pollaa	k1gFnSc4	Pollaa
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vápencovým	vápencový	k2eAgNnSc7d1	vápencové
pohořím	pohoří	k1gNnSc7	pohoří
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
jihu	jih	k1gInSc6	jih
Moravy	Morava	k1gFnSc2	Morava
od	od	k7c2	od
ohbí	ohbí	k1gNnSc2	ohbí
Dyje	Dyje	k1gFnSc2	Dyje
dvacet	dvacet	k4xCc4	dvacet
kilometrů	kilometr	k1gInPc2	kilometr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
okolo	okolo	k7c2	okolo
Mikulova	Mikulov	k1gInSc2	Mikulov
až	až	k9	až
ke	k	k7c3	k
státním	státní	k2eAgFnPc3d1	státní
hranicím	hranice	k1gFnPc3	hranice
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
nedosahují	dosahovat	k5eNaImIp3nP	dosahovat
velké	velká	k1gFnPc4	velká
absolutní	absolutní	k2eAgFnSc2d1	absolutní
výšky	výška	k1gFnSc2	výška
(	(	kIx(	(
<g/>
Děvín	Děvín	k1gInSc1	Děvín
549	[number]	k4	549
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
monumentálně	monumentálně	k6eAd1	monumentálně
nad	nad	k7c4	nad
okolní	okolní	k2eAgFnSc4d1	okolní
nížinu	nížina	k1gFnSc4	nížina
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
přírodní	přírodní	k2eAgFnSc4d1	přírodní
dominantu	dominanta	k1gFnSc4	dominanta
jihu	jih	k1gInSc2	jih
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Děvín	Děvín	k1gInSc1	Děvín
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Pálavy	Pálavy	k?	Pálavy
<g/>
)	)	kIx)	)
alpinským	alpinský	k2eAgNnSc7d1	alpinské
vrásněním	vrásnění	k1gNnSc7	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
se	se	k3xPyFc4	se
hřeben	hřeben	k1gInSc1	hřeben
skládá	skládat	k5eAaImIp3nS	skládat
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
druhohorním	druhohorní	k2eAgNnSc6d1	druhohorní
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgInS	být
vyzvednut	vyzvednut	k2eAgInSc1d1	vyzvednut
do	do	k7c2	do
výše	vysoce	k6eAd2	vysoce
alpínským	alpínský	k2eAgNnSc7d1	alpínské
vrásněním	vrásnění	k1gNnSc7	vrásnění
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zachovalosti	zachovalost	k1gFnSc3	zachovalost
původní	původní	k2eAgFnSc2d1	původní
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
různě	různě	k6eAd1	různě
exponovaným	exponovaný	k2eAgNnPc3d1	exponované
stanovištím	stanoviště	k1gNnPc3	stanoviště
na	na	k7c6	na
vápencích	vápenec	k1gInPc6	vápenec
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
množství	množství	k1gNnSc1	množství
velmi	velmi	k6eAd1	velmi
vzácných	vzácný	k2eAgMnPc2d1	vzácný
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
sucho-	sucho-	k?	sucho-
a	a	k8xC	a
teplomilných	teplomilný	k2eAgInPc2d1	teplomilný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
vrchy	vrch	k1gInPc1	vrch
součástí	součást	k1gFnPc2	součást
CHKO	CHKO	kA	CHKO
Pálava	Pálava	k1gFnSc1	Pálava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
severozápadní	severozápadní	k2eAgFnSc7d1	severozápadní
částí	část	k1gFnSc7	část
Biosférické	biosférický	k2eAgFnSc2d1	biosférická
rezervace	rezervace	k1gFnSc2	rezervace
Dolní	dolní	k2eAgFnSc1d1	dolní
Morava	Morava	k1gFnSc1	Morava
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
nepřetržitě	přetržitě	k6eNd1	přetržitě
obývána	obývat	k5eAaImNgFnS	obývat
po	po	k7c4	po
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nP	svědčit
mnoho	mnoho	k4c4	mnoho
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
(	(	kIx(	(
<g/>
lovci	lovec	k1gMnPc1	lovec
mamutů	mamut	k1gMnPc2	mamut
<g/>
,	,	kIx,	,
hradiště	hradiště	k1gNnSc4	hradiště
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
Dolních	dolní	k2eAgFnPc6d1	dolní
Věstonicích	Věstonice	k1gFnPc6	Věstonice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
přírodních	přírodní	k2eAgFnPc2d1	přírodní
rezervací	rezervace	k1gFnPc2	rezervace
ochraňujících	ochraňující	k2eAgFnPc2d1	ochraňující
zdejší	zdejší	k2eAgNnSc4d1	zdejší
velmi	velmi	k6eAd1	velmi
cenné	cenný	k2eAgNnSc4d1	cenné
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
výšinách	výšina	k1gFnPc6	výšina
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nP	tyčit
středověké	středověký	k2eAgFnPc1d1	středověká
hradní	hradní	k2eAgFnPc1d1	hradní
zříceniny	zřícenina	k1gFnPc1	zřícenina
(	(	kIx(	(
<g/>
Děvičky	Děvička	k1gFnPc1	Děvička
a	a	k8xC	a
Sirotčí	sirotčí	k2eAgInSc1d1	sirotčí
hrádek	hrádek	k1gInSc1	hrádek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
úbočí	úbočí	k1gNnSc6	úbočí
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
malebné	malebný	k2eAgNnSc1d1	malebné
historické	historický	k2eAgNnSc1d1	historické
město	město	k1gNnSc1	město
Mikulov	Mikulov	k1gInSc1	Mikulov
<g/>
.	.	kIx.	.
</s>
<s>
Pavlovské	pavlovský	k2eAgInPc1d1	pavlovský
vrchy	vrch	k1gInPc1	vrch
leží	ležet	k5eAaImIp3nP	ležet
v	v	k7c6	v
nejzápadnější	západní	k2eAgFnSc6d3	nejzápadnější
části	část	k1gFnSc6	část
Karpat	Karpaty	k1gInPc2	Karpaty
a	a	k8xC	a
skládají	skládat	k5eAaImIp3nP	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
řazeno	řadit	k5eAaImNgNnS	řadit
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
masiv	masiv	k1gInSc1	masiv
Děvín	Děvín	k1gInSc1	Děvín
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
nejvyšším	vysoký	k2eAgInSc6d3	Nejvyšší
bodě	bod	k1gInSc6	bod
(	(	kIx(	(
<g/>
549	[number]	k4	549
m	m	kA	m
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vysílač	vysílač	k1gInSc1	vysílač
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
nižším	nízký	k2eAgInSc6d2	nižší
vrcholu	vrchol	k1gInSc6	vrchol
(	(	kIx(	(
<g/>
428	[number]	k4	428
<g />
.	.	kIx.	.
</s>
<s>
m	m	kA	m
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
hrad	hrad	k1gInSc4	hrad
Děvičky	Děvička	k1gFnSc2	Děvička
sedlo	sedlo	k1gNnSc4	sedlo
Soutěska	soutěska	k1gFnSc1	soutěska
<g/>
,	,	kIx,	,
oddělující	oddělující	k2eAgInSc1d1	oddělující
Děvín	Děvín	k1gInSc1	Děvín
od	od	k7c2	od
Kotle	kotel	k1gInSc2	kotel
masiv	masiv	k1gInSc1	masiv
Kotel	kotel	k1gInSc1	kotel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
vrcholy	vrchol	k1gInPc4	vrchol
Obora	obora	k1gFnSc1	obora
(	(	kIx(	(
<g/>
483	[number]	k4	483
m	m	kA	m
<g/>
)	)	kIx)	)
se	s	k7c7	s
zříceninou	zřícenina	k1gFnSc7	zřícenina
Nového	Nového	k2eAgInSc2d1	Nového
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
Pálava	Pálava	k1gFnSc1	Pálava
(	(	kIx(	(
<g/>
462	[number]	k4	462
m	m	kA	m
<g/>
)	)	kIx)	)
Stolová	stolový	k2eAgFnSc1d1	stolová
hora	hora	k1gFnSc1	hora
(	(	kIx(	(
<g/>
459	[number]	k4	459
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
nižším	nízký	k2eAgInSc6d2	nižší
vrcholu	vrchol	k1gInSc6	vrchol
(	(	kIx(	(
<g/>
425	[number]	k4	425
m	m	kA	m
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Sirotčí	sirotčí	k2eAgInSc4d1	sirotčí
hrádek	hrádek	k1gInSc4	hrádek
vápencové	vápencový	k2eAgInPc4d1	vápencový
výchozy	výchoz	k1gInPc4	výchoz
Kočičí	kočičí	k2eAgFnSc1d1	kočičí
skála	skála	k1gFnSc1	skála
(	(	kIx(	(
<g/>
362	[number]	k4	362
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kočičí	kočičí	k2eAgInSc1d1	kočičí
kámen	kámen	k1gInSc1	kámen
(	(	kIx(	(
<g/>
352	[number]	k4	352
m	m	kA	m
<g/>
)	)	kIx)	)
Turold	Turold	k1gMnSc1	Turold
<g/>
,	,	kIx,	,
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
385	[number]	k4	385
m	m	kA	m
<g/>
)	)	kIx)	)
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
Mikulova	Mikulov	k1gInSc2	Mikulov
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
<g />
.	.	kIx.	.
</s>
<s>
horou	hora	k1gFnSc7	hora
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediná	jediný	k2eAgFnSc1d1	jediná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
přístupná	přístupný	k2eAgFnSc1d1	přístupná
jeskyně	jeskyně	k1gFnSc1	jeskyně
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
-	-	kIx~	-
Na	na	k7c6	na
Turoldu	Turold	k1gInSc6	Turold
Svatý	svatý	k2eAgInSc1d1	svatý
kopeček	kopeček	k1gInSc1	kopeček
(	(	kIx(	(
<g/>
363	[number]	k4	363
m	m	kA	m
<g/>
)	)	kIx)	)
východně	východně	k6eAd1	východně
nad	nad	k7c7	nad
Mikulovem	Mikulov	k1gInSc7	Mikulov
<g/>
,	,	kIx,	,
poutní	poutní	k2eAgNnSc4d1	poutní
místo	místo	k1gNnSc4	místo
Šibeničník	šibeničník	k1gMnSc1	šibeničník
<g/>
,	,	kIx,	,
nízký	nízký	k2eAgInSc1d1	nízký
vrch	vrch	k1gInSc1	vrch
(	(	kIx(	(
<g/>
238	[number]	k4	238
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
pohraničním	pohraniční	k2eAgInSc6d1	pohraniční
úvalu	úval	k1gInSc6	úval
mezi	mezi	k7c7	mezi
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
Pavlovské	pavlovský	k2eAgInPc1d1	pavlovský
vrchy	vrch	k1gInPc4	vrch
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Vnějších	vnější	k2eAgInPc2d1	vnější
Západních	západní	k2eAgInPc2d1	západní
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Geologicky	geologicky	k6eAd1	geologicky
navazujícími	navazující	k2eAgFnPc7d1	navazující
pohořími	pohoří	k1gNnPc7	pohoří
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Ždánický	ždánický	k2eAgInSc4d1	ždánický
les	les	k1gInSc4	les
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g/>
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
)	)	kIx)	)
tzv.	tzv.	kA	tzv.
Dolnorakouské	dolnorakouský	k2eAgNnSc1d1	dolnorakouské
pásmo	pásmo	k1gNnSc1	pásmo
ostrovních	ostrovní	k2eAgFnPc2d1	ostrovní
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Weinviertelská	Weinviertelský	k2eAgFnSc1d1	Weinviertelský
pahorkatina	pahorkatina	k1gFnSc1	pahorkatina
<g/>
.	.	kIx.	.
</s>
<s>
Přesah	přesah	k1gInSc1	přesah
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
je	být	k5eAaImIp3nS	být
sporný	sporný	k2eAgInSc1d1	sporný
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
končí	končit	k5eAaImIp3nS	končit
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
)	)	kIx)	)
právě	právě	k6eAd1	právě
korytem	koryto	k1gNnSc7	koryto
hraničního	hraniční	k2eAgInSc2d1	hraniční
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nepatří	patřit	k5eNaImIp3nP	patřit
už	už	k6eAd1	už
ani	ani	k8xC	ani
Šibeničník	šibeničník	k1gMnSc1	šibeničník
<g/>
.	.	kIx.	.
</s>
