<p>
<s>
Cejn	cejn	k1gMnSc1	cejn
siný	siný	k2eAgMnSc1d1	siný
(	(	kIx(	(
<g/>
Abramis	Abramis	k1gInSc1	Abramis
ballerus	ballerus	k1gInSc1	ballerus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vzácně	vzácně	k6eAd1	vzácně
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Fakticky	fakticky	k6eAd1	fakticky
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
omezeném	omezený	k2eAgNnSc6d1	omezené
množství	množství	k1gNnSc6	množství
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
boků	bok	k1gInPc2	bok
velmi	velmi	k6eAd1	velmi
zploštělé	zploštělý	k2eAgNnSc1d1	zploštělé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
cejna	cejn	k1gMnSc2	cejn
velkého	velký	k2eAgMnSc2d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
tmavý	tmavý	k2eAgInSc4d1	tmavý
namodralý	namodralý	k2eAgInSc4d1	namodralý
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
boky	boka	k1gFnPc1	boka
stříbřité	stříbřitý	k2eAgFnPc1d1	stříbřitá
a	a	k8xC	a
žlutavé	žlutavý	k2eAgFnPc1d1	žlutavá
<g/>
,	,	kIx,	,
břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
nažloutlé	nažloutlý	k2eAgNnSc1d1	nažloutlé
<g/>
,	,	kIx,	,
ploutve	ploutev	k1gFnPc1	ploutev
šedé	šedý	k2eAgFnPc1d1	šedá
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
spodní	spodní	k2eAgNnPc4d1	spodní
ústa	ústa	k1gNnPc4	ústa
<g/>
.	.	kIx.	.
</s>
<s>
Řitní	řitní	k2eAgFnSc1d1	řitní
ploutev	ploutev	k1gFnSc1	ploutev
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgFnSc1d2	delší
než	než	k8xS	než
u	u	k7c2	u
cejna	cejn	k1gMnSc2	cejn
velkého	velký	k2eAgMnSc2d1	velký
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
dorůst	dorůst	k5eAaPmF	dorůst
délky	délka	k1gFnSc2	délka
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dožívá	dožívat	k5eAaImIp3nS	dožívat
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
dolní	dolní	k2eAgInPc1d1	dolní
toky	tok	k1gInPc1	tok
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
stojaté	stojatý	k2eAgFnSc2d1	stojatá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
úmoří	úmoří	k1gNnSc6	úmoří
Černého	černé	k1gNnSc2	černé
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Potravu	potrava	k1gFnSc4	potrava
přijímá	přijímat	k5eAaImIp3nS	přijímat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
planktonem	plankton	k1gInSc7	plankton
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
také	také	k9	také
bentosem	bentos	k1gInSc7	bentos
a	a	k8xC	a
částmi	část	k1gFnPc7	část
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Pohlavně	pohlavně	k6eAd1	pohlavně
dospívá	dospívat	k5eAaImIp3nS	dospívat
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Tření	tření	k1gNnSc1	tření
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Třou	třít	k5eAaImIp3nP	třít
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
na	na	k7c4	na
potopené	potopený	k2eAgInPc4d1	potopený
kmeny	kmen	k1gInPc4	kmen
a	a	k8xC	a
větvě	větvit	k5eAaImSgInS	větvit
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
vzácněji	vzácně	k6eAd2	vzácně
na	na	k7c6	na
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
či	či	k8xC	či
kamení	kamení	k1gNnSc1	kamení
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
samice	samice	k1gFnSc1	samice
může	moct	k5eAaImIp3nS	moct
běžně	běžně	k6eAd1	běžně
naklást	naklást	k5eAaPmF	naklást
5000	[number]	k4	5000
-	-	kIx~	-
30000	[number]	k4	30000
jiker	jikra	k1gFnPc2	jikra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cejn	cejn	k1gMnSc1	cejn
siný	siný	k2eAgMnSc1d1	siný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Ballerus	Ballerus	k1gInSc1	Ballerus
ballerus	ballerus	k1gInSc4	ballerus
ve	v	k7c4	v
WikidruzíchCejn	WikidruzíchCejn	k1gInSc4	WikidruzíchCejn
siný	siný	k2eAgInSc4d1	siný
v	v	k7c6	v
atlasu	atlas	k1gInSc6	atlas
ryb	ryba	k1gFnPc2	ryba
na	na	k7c6	na
CHYTEJ	chytat	k5eAaImRp2nS	chytat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Cejn	cejn	k1gMnSc1	cejn
siný	siný	k2eAgMnSc1d1	siný
na	na	k7c4	na
MRK	mrk	k1gInSc4	mrk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Cejn	cejn	k1gMnSc1	cejn
siný	siný	k2eAgMnSc1d1	siný
-	-	kIx~	-
atlas	atlas	k1gInSc1	atlas
ryb	ryba	k1gFnPc2	ryba
on-line	onin	k1gInSc5	on-lin
</s>
</p>
