<s>
Trypsin	trypsin	k1gInSc1	trypsin
je	být	k5eAaImIp3nS	být
trávicí	trávicí	k2eAgInSc4d1	trávicí
enzym	enzym	k1gInSc4	enzym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
dvanáctníku	dvanáctník	k1gInSc6	dvanáctník
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
pepsin	pepsin	k1gInSc1	pepsin
štěpí	štěpit	k5eAaImIp3nS	štěpit
peptidické	peptidický	k2eAgFnPc4d1	peptidická
vazby	vazba	k1gFnPc4	vazba
bílkovin	bílkovina	k1gFnPc2	bílkovina
obsažených	obsažený	k2eAgFnPc2d1	obsažená
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Trypsin	trypsin	k1gInSc1	trypsin
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
hydrolázy	hydroláza	k1gFnPc4	hydroláza
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
proteázy	proteáza	k1gFnSc2	proteáza
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
ve	v	k7c6	v
slinivce	slinivka	k1gFnSc6	slinivka
břišní	břišní	k2eAgFnSc2d1	břišní
jako	jako	k8xS	jako
proenzym	proenzym	k1gInSc1	proenzym
trypsinogen	trypsinogen	k1gInSc1	trypsinogen
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
dvanáctníku	dvanáctník	k1gInSc6	dvanáctník
aktivován	aktivován	k2eAgInSc1d1	aktivován
enzymaticky	enzymaticky	k6eAd1	enzymaticky
enterokinázou	enterokináza	k1gFnSc7	enterokináza
a	a	k8xC	a
také	také	k9	také
působením	působení	k1gNnSc7	působení
dříve	dříve	k6eAd2	dříve
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
molekul	molekula	k1gFnPc2	molekula
trypsinu	trypsin	k1gInSc2	trypsin
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
autokatalýzu	autokatalýza	k1gFnSc4	autokatalýza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
pepsinu	pepsin	k1gInSc3	pepsin
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
aktivita	aktivita	k1gFnSc1	aktivita
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
v	v	k7c6	v
silně	silně	k6eAd1	silně
kyselém	kyselý	k2eAgNnSc6d1	kyselé
prostředí	prostředí	k1gNnSc6	prostředí
žaludku	žaludek	k1gInSc2	žaludek
<g/>
,	,	kIx,	,
katalyzuje	katalyzovat	k5eAaPmIp3nS	katalyzovat
trypsin	trypsin	k1gInSc4	trypsin
proteolytické	proteolytický	k2eAgFnPc1d1	proteolytická
(	(	kIx(	(
<g/>
rozkladné	rozkladný	k2eAgFnPc1d1	rozkladná
<g/>
)	)	kIx)	)
reakce	reakce	k1gFnPc1	reakce
v	v	k7c6	v
mírně	mírně	k6eAd1	mírně
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
právě	právě	k9	právě
pro	pro	k7c4	pro
dvanáctník	dvanáctník	k1gInSc4	dvanáctník
<g/>
.	.	kIx.	.
</s>
<s>
Trypsin	trypsin	k1gInSc1	trypsin
se	se	k3xPyFc4	se
od	od	k7c2	od
pepsinu	pepsin	k1gInSc2	pepsin
liší	lišit	k5eAaImIp3nP	lišit
také	také	k9	také
větší	veliký	k2eAgFnSc7d2	veliký
specifitou	specifita	k1gFnSc7	specifita
-	-	kIx~	-
neštěpí	štěpit	k5eNaImIp3nP	štěpit
bílkoviny	bílkovina	k1gFnPc1	bílkovina
v	v	k7c6	v
náhodných	náhodný	k2eAgNnPc6d1	náhodné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
karboxylovém	karboxylový	k2eAgInSc6d1	karboxylový
konci	konec	k1gInSc6	konec
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
lysinu	lysina	k1gFnSc4	lysina
a	a	k8xC	a
argininu	arginin	k2eAgFnSc4d1	arginin
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
nenásleduje	následovat	k5eNaImIp3nS	následovat
prolin	prolina	k1gFnPc2	prolina
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaný	izolovaný	k2eAgInSc1d1	izolovaný
trypsin	trypsin	k1gInSc1	trypsin
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
v	v	k7c6	v
cytogenetice	cytogenetika	k1gFnSc6	cytogenetika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nechává	nechávat	k5eAaImIp3nS	nechávat
působit	působit	k5eAaImF	působit
na	na	k7c4	na
chromozomy	chromozom	k1gInPc4	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
má	mít	k5eAaImIp3nS	mít
trypsin	trypsin	k1gInSc4	trypsin
v	v	k7c6	v
proteomice	proteomika	k1gFnSc6	proteomika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
specifickému	specifický	k2eAgNnSc3d1	specifické
naštěpení	naštěpení	k1gNnSc3	naštěpení
proteinů	protein	k1gInPc2	protein
na	na	k7c4	na
peptidy	peptid	k1gInPc4	peptid
před	před	k7c7	před
určováním	určování	k1gNnSc7	určování
pomocí	pomocí	k7c2	pomocí
hmotnostního	hmotnostní	k2eAgInSc2d1	hmotnostní
spektrometru	spektrometr	k1gInSc2	spektrometr
<g/>
.	.	kIx.	.
</s>
