<s>
Množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
množina	množina	k1gFnSc1	množina
obsahující	obsahující	k2eAgInPc4d1	obsahující
nějaké	nějaký	k3yIgInPc4	nějaký
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
neprázdná	prázdný	k2eNgFnSc1d1	neprázdná
množina	množina	k1gFnSc1	množina
<g/>
.	.	kIx.	.
</s>
