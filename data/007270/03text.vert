<s>
Carl	Carl	k1gMnSc1	Carl
Zeiss	Zeissa	k1gFnPc2	Zeissa
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1816	[number]	k4	1816
<g/>
,	,	kIx,	,
Výmar	Výmar	k1gInSc1	Výmar
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
Jena	Jena	k1gFnSc1	Jena
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
průmyslník	průmyslník	k1gMnSc1	průmyslník
a	a	k8xC	a
optik	optik	k1gMnSc1	optik
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
zakladatel	zakladatel	k1gMnSc1	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Carl	Carl	k1gMnSc1	Carl
Zeiss	Zeissa	k1gFnPc2	Zeissa
AG	AG	kA	AG
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
ve	v	k7c6	v
Výmaru	Výmar	k1gInSc6	Výmar
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vynikajícím	vynikající	k2eAgMnSc7d1	vynikající
výrobcem	výrobce	k1gMnSc7	výrobce
fotografických	fotografický	k2eAgInPc2d1	fotografický
objektivů	objektiv	k1gInPc2	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
dostal	dostat	k5eAaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
od	od	k7c2	od
vrchního	vrchní	k2eAgInSc2d1	vrchní
stavebního	stavební	k2eAgInSc2d1	stavební
úřadu	úřad	k1gInSc2	úřad
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Jena	Jen	k1gInSc2	Jen
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
vlastní	vlastní	k2eAgFnSc2d1	vlastní
dílny	dílna	k1gFnSc2	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
osudově	osudově	k6eAd1	osudově
spojila	spojit	k5eAaPmAgFnS	spojit
dvě	dva	k4xCgNnPc4	dva
slova	slovo	k1gNnPc4	slovo
"	"	kIx"	"
<g/>
Zeiss	Zeissa	k1gFnPc2	Zeissa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jena	Jena	k1gFnSc1	Jena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
pojmem	pojem	k1gInSc7	pojem
známým	známý	k2eAgInSc7d1	známý
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Carl	Carl	k1gMnSc1	Carl
Zeiss	Zeiss	k1gInSc4	Zeiss
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
dvorního	dvorní	k2eAgMnSc2d1	dvorní
soustružnického	soustružnický	k2eAgMnSc2d1	soustružnický
mistra	mistr	k1gMnSc2	mistr
Augusta	August	k1gMnSc2	August
Zeisse	Zeiss	k1gMnSc2	Zeiss
a	a	k8xC	a
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
pátý	pátý	k4xOgMnSc1	pátý
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otci	otec	k1gMnSc6	otec
zdědil	zdědit	k5eAaPmAgInS	zdědit
cit	cit	k1gInSc1	cit
k	k	k7c3	k
řemeslu	řemeslo	k1gNnSc3	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
letech	léto	k1gNnPc6	léto
opouštěl	opouštět	k5eAaImAgMnS	opouštět
gymnázium	gymnázium	k1gNnSc4	gymnázium
s	s	k7c7	s
odhodláním	odhodlání	k1gNnSc7	odhodlání
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
mechanikem	mechanik	k1gMnSc7	mechanik
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
učení	učení	k1gNnSc2	učení
k	k	k7c3	k
doktoru	doktor	k1gMnSc3	doktor
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Körnerovi	Körner	k1gMnSc3	Körner
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
nejrůznější	různý	k2eAgFnPc4d3	nejrůznější
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
především	především	k9	především
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
mineralogie	mineralogie	k1gFnSc2	mineralogie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
uvědomoval	uvědomovat	k5eAaImAgMnS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
lepší	dobrý	k2eAgFnSc3d2	lepší
konstrukci	konstrukce	k1gFnSc3	konstrukce
optických	optický	k2eAgFnPc2d1	optická
čoček	čočka	k1gFnPc2	čočka
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
teoretický	teoretický	k2eAgInSc4d1	teoretický
základ	základ	k1gInSc4	základ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
si	se	k3xPyFc3	se
Zeiss	Zeiss	k1gInSc1	Zeiss
otevřel	otevřít	k5eAaPmAgInS	otevřít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
dílnu	dílna	k1gFnSc4	dílna
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
konstrukce	konstrukce	k1gFnSc2	konstrukce
mikroskopů	mikroskop	k1gInPc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
každý	každý	k3xTgInSc1	každý
jeho	jeho	k3xOp3gInSc1	jeho
výrobek	výrobek	k1gInSc1	výrobek
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
trochu	trochu	k6eAd1	trochu
unikát	unikát	k1gInSc4	unikát
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
stále	stále	k6eAd1	stále
chyběly	chybět	k5eAaImAgInP	chybět
teoretické	teoretický	k2eAgInPc1d1	teoretický
podklady	podklad	k1gInPc1	podklad
ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jenské	jenský	k2eAgFnSc6d1	Jenská
univerzitě	univerzita	k1gFnSc6	univerzita
působil	působit	k5eAaImAgMnS	působit
mladý	mladý	k2eAgMnSc1d1	mladý
docent	docent	k1gMnSc1	docent
Ernst	Ernst	k1gMnSc1	Ernst
Karl	Karl	k1gMnSc1	Karl
Abbe	Abbe	k1gInSc4	Abbe
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
Zeissovy	Zeissův	k2eAgInPc4d1	Zeissův
"	"	kIx"	"
<g/>
problémy	problém	k1gInPc4	problém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c4	na
spolupráci	spolupráce	k1gFnSc4	spolupráce
a	a	k8xC	a
Ernst	Ernst	k1gMnSc1	Ernst
Karl	Karl	k1gMnSc1	Karl
Abbe	Abbe	k1gFnSc4	Abbe
začal	začít	k5eAaPmAgMnS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
Zeissových	Zeissův	k2eAgFnPc6d1	Zeissova
dílnách	dílna	k1gFnPc6	dílna
<g/>
.	.	kIx.	.
</s>
<s>
Zeiss	Zeiss	k1gInSc1	Zeiss
tak	tak	k9	tak
získal	získat	k5eAaPmAgInS	získat
ke	k	k7c3	k
spolupráci	spolupráce	k1gFnSc3	spolupráce
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
problému	problém	k1gInSc6	problém
skvělého	skvělý	k2eAgMnSc2d1	skvělý
teoretika	teoretik	k1gMnSc2	teoretik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
se	se	k3xPyFc4	se
již	již	k6eAd1	již
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
teoretické	teoretický	k2eAgInPc1d1	teoretický
základy	základ	k1gInPc1	základ
pro	pro	k7c4	pro
optické	optický	k2eAgInPc4d1	optický
zobrazování	zobrazování	k1gNnSc2	zobrazování
položeny	položit	k5eAaPmNgInP	položit
a	a	k8xC	a
v	v	k7c6	v
Zeissových	Zeissův	k2eAgFnPc6d1	Zeissova
dílnách	dílna	k1gFnPc6	dílna
je	být	k5eAaImIp3nS	být
sestrojen	sestrojit	k5eAaPmNgInS	sestrojit
první	první	k4xOgInSc1	první
mikroskop	mikroskop	k1gInSc1	mikroskop
podle	podle	k7c2	podle
předem	předem	k6eAd1	předem
teoreticky	teoreticky	k6eAd1	teoreticky
vypočítaných	vypočítaný	k2eAgFnPc2d1	vypočítaná
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
však	však	k9	však
horší	zlý	k2eAgFnPc4d2	horší
zobrazovací	zobrazovací	k2eAgFnPc4d1	zobrazovací
vlastnosti	vlastnost	k1gFnPc4	vlastnost
než	než	k8xS	než
předešlé	předešlý	k2eAgInPc4d1	předešlý
mikroskopy	mikroskop	k1gInPc4	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Abbe	Abbat	k5eAaPmIp3nS	Abbat
se	se	k3xPyFc4	se
však	však	k9	však
nenechal	nechat	k5eNaPmAgMnS	nechat
zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
při	při	k7c6	při
dalších	další	k2eAgInPc6d1	další
pokusech	pokus	k1gInPc6	pokus
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
pokusech	pokus	k1gInPc6	pokus
počítal	počítat	k5eAaImAgInS	počítat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
lomem	lom	k1gInSc7	lom
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
zanedbal	zanedbat	k5eAaPmAgInS	zanedbat
ohyb	ohyb	k1gInSc1	ohyb
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
problém	problém	k1gInSc1	problém
zcela	zcela	k6eAd1	zcela
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
sestrojen	sestrojen	k2eAgInSc1d1	sestrojen
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
splňoval	splňovat	k5eAaImAgInS	splňovat
teoreticky	teoreticky	k6eAd1	teoreticky
vypočítané	vypočítaný	k2eAgFnPc4d1	vypočítaná
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Konkurence	konkurence	k1gFnSc1	konkurence
se	se	k3xPyFc4	se
Zeissovým	Zeissův	k2eAgInPc3d1	Zeissův
výrobkům	výrobek	k1gInPc3	výrobek
tvrdě	tvrdě	k6eAd1	tvrdě
bránila	bránit	k5eAaImAgFnS	bránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
široká	široký	k2eAgFnSc1d1	široká
veřejnost	veřejnost	k1gFnSc1	veřejnost
přijala	přijmout	k5eAaPmAgFnS	přijmout
tyto	tento	k3xDgInPc4	tento
výrobky	výrobek	k1gInPc4	výrobek
s	s	k7c7	s
uznáním	uznání	k1gNnSc7	uznání
<g/>
,	,	kIx,	,
konkurence	konkurence	k1gFnSc1	konkurence
zcela	zcela	k6eAd1	zcela
otočila	otočit	k5eAaPmAgFnS	otočit
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
inzerátech	inzerát	k1gInPc6	inzerát
psala	psát	k5eAaImAgFnS	psát
slogany	slogan	k1gInPc7	slogan
"	"	kIx"	"
<g/>
konstrukce	konstrukce	k1gFnSc1	konstrukce
jako	jako	k8xS	jako
v	v	k7c6	v
Jeně	Jen	k1gInSc6	Jen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Jeny	jen	k1gInPc1	jen
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
sjíždět	sjíždět	k5eAaImF	sjíždět
slavní	slavný	k2eAgMnPc1d1	slavný
vědci	vědec	k1gMnPc1	vědec
pro	pro	k7c4	pro
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
či	či	k8xC	či
se	se	k3xPyFc4	se
jen	jen	k9	jen
tak	tak	k6eAd1	tak
podívat	podívat	k5eAaImF	podívat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
začala	začít	k5eAaPmAgFnS	začít
spolupráce	spolupráce	k1gFnSc1	spolupráce
tří	tři	k4xCgFnPc2	tři
osob	osoba	k1gFnPc2	osoba
<g/>
:	:	kIx,	:
mechanika	mechanika	k1gFnSc1	mechanika
Zeisse	Zeiss	k1gMnSc2	Zeiss
<g/>
,	,	kIx,	,
fyzika	fyzik	k1gMnSc2	fyzik
Abbeho	Abbe	k1gMnSc2	Abbe
a	a	k8xC	a
sklářského	sklářský	k2eAgMnSc2d1	sklářský
technika	technik	k1gMnSc2	technik
Schotta	Schott	k1gMnSc2	Schott
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
zlepšení	zlepšení	k1gNnSc6	zlepšení
vlastností	vlastnost	k1gFnPc2	vlastnost
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yRgInSc2	který
byly	být	k5eAaImAgFnP	být
konstruovány	konstruován	k2eAgFnPc1d1	konstruována
čočky	čočka	k1gFnPc1	čočka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nenechaly	nechat	k5eNaPmAgFnP	nechat
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
snížit	snížit	k5eAaPmF	snížit
barevnou	barevný	k2eAgFnSc4d1	barevná
vadu	vada	k1gFnSc4	vada
čoček	čočka	k1gFnPc2	čočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
oslava	oslava	k1gFnSc1	oslava
desetitisícího	desetitisící	k2eAgInSc2d1	desetitisící
vyrobeného	vyrobený	k2eAgInSc2d1	vyrobený
mikroskopu	mikroskop	k1gInSc2	mikroskop
a	a	k8xC	a
Carl	Carl	k1gMnSc1	Carl
Zeiss	Zeissa	k1gFnPc2	Zeissa
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
vzdávat	vzdávat	k5eAaImF	vzdávat
své	svůj	k3xOyFgFnPc4	svůj
činnosti	činnost	k1gFnPc4	činnost
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
kvůli	kvůli	k7c3	kvůli
zhoršujícímu	zhoršující	k2eAgInSc3d1	zhoršující
se	se	k3xPyFc4	se
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
<g/>
.	.	kIx.	.
</s>
<s>
Prodělal	prodělat	k5eAaPmAgMnS	prodělat
několik	několik	k4yIc4	několik
záchvatů	záchvat	k1gInPc2	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
<g/>
.	.	kIx.	.
</s>
