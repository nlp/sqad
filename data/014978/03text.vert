<s>
341	#num#	k4
(	(	kIx(
<g/>
číslo	číslo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
←	←	k?
340	#num#	k4
</s>
<s>
341	#num#	k4
</s>
<s>
342	#num#	k4
→	→	k?
</s>
<s>
Celé	celý	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
341	#num#	k4
</s>
<s>
tři	tři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
čtyřicet	čtyřicet	k4xCc1
jedna	jeden	k4xCgFnSc1
</s>
<s>
Rozklad	rozklad	k1gInSc1
</s>
<s>
11	#num#	k4
·	·	k?
31	#num#	k4
</s>
<s>
Dělitelé	dělitel	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
11	#num#	k4
<g/>
,	,	kIx,
31	#num#	k4
<g/>
,	,	kIx,
341	#num#	k4
</s>
<s>
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
</s>
<s>
CCCXLI	CCCXLI	kA
</s>
<s>
Dvojkově	dvojkově	k6eAd1
</s>
<s>
1	#num#	k4
0101	#num#	k4
01012	#num#	k4
</s>
<s>
Trojkově	trojkově	k6eAd1
</s>
<s>
110	#num#	k4
1223	#num#	k4
</s>
<s>
Čtyřkově	Čtyřkově	k6eAd1
</s>
<s>
11	#num#	k4
1114	#num#	k4
</s>
<s>
Pětkově	pětkově	k6eAd1
</s>
<s>
23315	#num#	k4
</s>
<s>
Šestkově	šestkově	k6eAd1
</s>
<s>
13256	#num#	k4
</s>
<s>
Sedmičkově	Sedmičkově	k6eAd1
</s>
<s>
6657	#num#	k4
</s>
<s>
Osmičkově	osmičkově	k6eAd1
</s>
<s>
5258	#num#	k4
</s>
<s>
Šestnáctkově	šestnáctkově	k6eAd1
</s>
<s>
15516	#num#	k4
</s>
<s>
Tři	tři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
čtyřicet	čtyřicet	k4xCc1
jedna	jeden	k4xCgFnSc1
je	být	k5eAaImIp3nS
přirozené	přirozený	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
následuje	následovat	k5eAaImIp3nS
po	po	k7c6
čísle	číslo	k1gNnSc6
tři	tři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
čtyřicet	čtyřicet	k4xCc1
a	a	k8xC
předchází	předcházet	k5eAaImIp3nS
číslu	číslo	k1gNnSc3
tři	tři	k4xCgNnPc1
sta	sto	k4xCgNnPc1
čtyřicet	čtyřicet	k4xCc1
dva	dva	k4xCgInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Římskými	římský	k2eAgFnPc7d1
číslicemi	číslice	k1gFnPc7
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
CCCXLI	CCCXLI	kA
<g/>
.	.	kIx.
</s>
<s>
Matematika	matematika	k1gFnSc1
</s>
<s>
poloprvočíslo	poloprvočísnout	k5eAaPmAgNnS
</s>
<s>
nešťastné	šťastný	k2eNgNnSc4d1
číslo	číslo	k1gNnSc4
</s>
<s>
osmiúhelníkové	osmiúhelníkový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
součet	součet	k1gInSc1
sedmi	sedm	k4xCc2
po	po	k7c6
sobě	sebe	k3xPyFc6
jdoucích	jdoucí	k2eAgNnPc2d1
prvočísel	prvočíslo	k1gNnPc2
(	(	kIx(
<g/>
37	#num#	k4
+	+	kIx~
41	#num#	k4
+	+	kIx~
43	#num#	k4
+	+	kIx~
47	#num#	k4
+	+	kIx~
53	#num#	k4
+	+	kIx~
59	#num#	k4
+	+	kIx~
61	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
341	#num#	k4
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
vedoucí	vedoucí	k2eAgFnSc2d1
na	na	k7c6
trase	trasa	k1gFnSc6
Hrbokov	Hrbokov	k1gInSc1
–	–	k?
Heřmanův	Heřmanův	k2eAgInSc1d1
Městec	Městec	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Astronomie	astronomie	k1gFnSc1
</s>
<s>
341	#num#	k4
California	Californium	k1gNnPc4
je	být	k5eAaImIp3nS
planetka	planetka	k1gFnSc1
hlavního	hlavní	k2eAgInSc2d1
pásu	pás	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
341	#num#	k4
</s>
<s>
341	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://mapy.cz/zakladni?x=15.6847879&	https://mapy.cz/zakladni?x=15.6847879&	k?
<g/>
↑	↑	k?
https://mapy.cz/zakladni?x=15.6693383&	https://mapy.cz/zakladni?x=15.6693383&	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Přirozená	přirozený	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
300	#num#	k4
<g/>
–	–	k?
<g/>
399	#num#	k4
</s>
<s>
300	#num#	k4
•	•	k?
301	#num#	k4
•	•	k?
302	#num#	k4
•	•	k?
303	#num#	k4
•	•	k?
304	#num#	k4
•	•	k?
305	#num#	k4
•	•	k?
306	#num#	k4
•	•	k?
307	#num#	k4
•	•	k?
308	#num#	k4
•	•	k?
309	#num#	k4
•	•	k?
310	#num#	k4
•	•	k?
311	#num#	k4
•	•	k?
312	#num#	k4
•	•	k?
313	#num#	k4
•	•	k?
314	#num#	k4
•	•	k?
315	#num#	k4
•	•	k?
316	#num#	k4
•	•	k?
317	#num#	k4
•	•	k?
318	#num#	k4
•	•	k?
319	#num#	k4
•	•	k?
320	#num#	k4
•	•	k?
321	#num#	k4
•	•	k?
322	#num#	k4
•	•	k?
323	#num#	k4
•	•	k?
324	#num#	k4
•	•	k?
325	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
326	#num#	k4
•	•	k?
327	#num#	k4
•	•	k?
328	#num#	k4
•	•	k?
329	#num#	k4
•	•	k?
330	#num#	k4
•	•	k?
331	#num#	k4
•	•	k?
332	#num#	k4
•	•	k?
333	#num#	k4
•	•	k?
334	#num#	k4
•	•	k?
335	#num#	k4
•	•	k?
336	#num#	k4
•	•	k?
337	#num#	k4
•	•	k?
338	#num#	k4
•	•	k?
339	#num#	k4
•	•	k?
340	#num#	k4
•	•	k?
341	#num#	k4
•	•	k?
342	#num#	k4
•	•	k?
343	#num#	k4
•	•	k?
344	#num#	k4
•	•	k?
345	#num#	k4
•	•	k?
346	#num#	k4
•	•	k?
347	#num#	k4
•	•	k?
348	#num#	k4
•	•	k?
349	#num#	k4
•	•	k?
350	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
351	#num#	k4
•	•	k?
352	#num#	k4
•	•	k?
353	#num#	k4
•	•	k?
354	#num#	k4
•	•	k?
355	#num#	k4
•	•	k?
356	#num#	k4
•	•	k?
357	#num#	k4
•	•	k?
358	#num#	k4
•	•	k?
359	#num#	k4
•	•	k?
360	#num#	k4
•	•	k?
361	#num#	k4
•	•	k?
362	#num#	k4
•	•	k?
363	#num#	k4
•	•	k?
364	#num#	k4
•	•	k?
365	#num#	k4
•	•	k?
366	#num#	k4
•	•	k?
367	#num#	k4
•	•	k?
368	#num#	k4
•	•	k?
369	#num#	k4
•	•	k?
370	#num#	k4
•	•	k?
371	#num#	k4
•	•	k?
372	#num#	k4
•	•	k?
373	#num#	k4
•	•	k?
374	#num#	k4
•	•	k?
375	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
376	#num#	k4
•	•	k?
377	#num#	k4
•	•	k?
378	#num#	k4
•	•	k?
379	#num#	k4
•	•	k?
380	#num#	k4
•	•	k?
381	#num#	k4
•	•	k?
382	#num#	k4
•	•	k?
383	#num#	k4
•	•	k?
384	#num#	k4
•	•	k?
385	#num#	k4
•	•	k?
386	#num#	k4
•	•	k?
387	#num#	k4
•	•	k?
388	#num#	k4
•	•	k?
389	#num#	k4
•	•	k?
390	#num#	k4
•	•	k?
391	#num#	k4
•	•	k?
392	#num#	k4
•	•	k?
393	#num#	k4
•	•	k?
394	#num#	k4
•	•	k?
395	#num#	k4
•	•	k?
396	#num#	k4
•	•	k?
397	#num#	k4
•	•	k?
398	#num#	k4
•	•	k?
399	#num#	k4
0	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
99	#num#	k4
•	•	k?
100	#num#	k4
<g/>
–	–	k?
<g/>
199	#num#	k4
•	•	k?
200	#num#	k4
<g/>
–	–	k?
<g/>
299	#num#	k4
•	•	k?
300	#num#	k4
<g/>
–	–	k?
<g/>
399	#num#	k4
•	•	k?
400	#num#	k4
<g/>
–	–	k?
<g/>
499	#num#	k4
•	•	k?
500	#num#	k4
<g/>
–	–	k?
<g/>
599	#num#	k4
•	•	k?
600	#num#	k4
<g/>
–	–	k?
<g/>
699	#num#	k4
•	•	k?
700	#num#	k4
<g/>
–	–	k?
<g/>
799	#num#	k4
•	•	k?
800	#num#	k4
<g/>
–	–	k?
<g/>
899	#num#	k4
•	•	k?
900	#num#	k4
<g/>
–	–	k?
<g/>
999	#num#	k4
•	•	k?
1000	#num#	k4
<g/>
–	–	k?
<g/>
1099	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
