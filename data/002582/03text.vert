<s>
André	André	k1gMnSc1	André
Breton	Breton	k1gMnSc1	Breton
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1896	[number]	k4	1896
Tinchebray	Tinchebraa	k1gFnSc2	Tinchebraa
(	(	kIx(	(
<g/>
Orne	Orne	k1gInSc1	Orne
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1966	[number]	k4	1966
Paříž	Paříž	k1gFnSc4	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnSc4	zakladatel
a	a	k8xC	a
hlavního	hlavní	k2eAgMnSc4d1	hlavní
teoretika	teoretik	k1gMnSc4	teoretik
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
mladý	mladý	k2eAgMnSc1d1	mladý
student	student	k1gMnSc1	student
medicíny	medicína	k1gFnSc2	medicína
psal	psát	k5eAaImAgMnS	psát
lyriku	lyrika	k1gFnSc4	lyrika
<g/>
,	,	kIx,	,
ovlivněn	ovlivnit	k5eAaPmNgMnS	ovlivnit
symbolisty	symbolista	k1gMnPc7	symbolista
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
Stéphanem	Stéphan	k1gInSc7	Stéphan
Mallarmém	Mallarmý	k2eAgInSc6d1	Mallarmý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
v	v	k7c4	v
Nantes	Nantes	k1gInSc4	Nantes
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Jacquesem	Jacques	k1gMnSc7	Jacques
Vachém	Vachý	k2eAgInSc6d1	Vachý
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
vliv	vliv	k1gInSc1	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
Rimbaudem	Rimbaud	k1gInSc7	Rimbaud
a	a	k8xC	a
pracemi	práce	k1gFnPc7	práce
Sigmunda	Sigmund	k1gMnSc2	Sigmund
Freuda	Freud	k1gMnSc2	Freud
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
si	se	k3xPyFc3	se
také	také	k9	také
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
několik	několik	k4yIc4	několik
dopisů	dopis	k1gInPc2	dopis
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
navštívil	navštívit	k5eAaPmAgMnS	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Psychoanalýza	psychoanalýza	k1gFnSc1	psychoanalýza
hrála	hrát	k5eAaImAgFnS	hrát
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc4	první
možnost	možnost	k1gFnSc4	možnost
zkoušet	zkoušet	k5eAaImF	zkoušet
její	její	k3xOp3gInPc4	její
poznatky	poznatek	k1gInPc4	poznatek
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
v	v	k7c6	v
psychiatrickém	psychiatrický	k2eAgNnSc6d1	psychiatrické
středisku	středisko	k1gNnSc6	středisko
II	II	kA	II
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
v	v	k7c4	v
Saint-Dizier	Saint-Dizier	k1gInSc4	Saint-Dizier
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Guillaumem	Guillaum	k1gInSc7	Guillaum
Apollinairem	Apollinairma	k1gFnPc2	Apollinairma
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Louisem	Louis	k1gMnSc7	Louis
Aragonem	Aragon	k1gMnSc7	Aragon
a	a	k8xC	a
Philippe	Philipp	k1gInSc5	Philipp
Soupaultem	Soupault	k1gInSc7	Soupault
založil	založit	k5eAaPmAgInS	založit
revui	revue	k1gFnSc4	revue
Littérature	Littératur	k1gMnSc5	Littératur
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
přišel	přijít	k5eAaPmAgMnS	přijít
z	z	k7c2	z
Curychu	Curych	k1gInSc2	Curych
Tristan	Tristan	k1gInSc4	Tristan
Tzara	Tzaro	k1gNnSc2	Tzaro
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
začalo	začít	k5eAaPmAgNnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
dadaistické	dadaistický	k2eAgNnSc1d1	dadaistické
hnutí	hnutí	k1gNnSc1	hnutí
a	a	k8xC	a
Breton	Breton	k1gMnSc1	Breton
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
členem	člen	k1gMnSc7	člen
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
čase	čas	k1gInSc6	čas
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
jeho	jeho	k3xOp3gInPc4	jeho
prostředky	prostředek	k1gInPc4	prostředek
shledávat	shledávat	k5eAaImF	shledávat
neúčelnými	účelný	k2eNgFnPc7d1	neúčelná
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gNnSc7	on
a	a	k8xC	a
Tzarou	Tzara	k1gFnSc7	Tzara
k	k	k7c3	k
názorovým	názorový	k2eAgInPc3d1	názorový
střetům	střet	k1gInPc3	střet
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
definitivní	definitivní	k2eAgInSc1d1	definitivní
rozchod	rozchod	k1gInSc1	rozchod
s	s	k7c7	s
dadaismem	dadaismus	k1gInSc7	dadaismus
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
Soupaultem	Soupault	k1gInSc7	Soupault
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
Magnetická	magnetický	k2eAgNnPc4d1	magnetické
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
automatický	automatický	k2eAgInSc4d1	automatický
text	text	k1gInSc4	text
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Bretonova	Bretonův	k2eAgNnSc2d1	Bretonův
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
vyjádření	vyjádření	k1gNnSc2	vyjádření
první	první	k4xOgNnSc4	první
surrealistické	surrealistický	k2eAgNnSc4d1	surrealistické
dílo	dílo	k1gNnSc4	dílo
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
jako	jako	k9	jako
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vědecká	vědecký	k2eAgFnSc1d1	vědecká
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neuznává	uznávat	k5eNaImIp3nS	uznávat
aparát	aparát	k1gInSc4	aparát
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
ty	ten	k3xDgFnPc4	ten
nejniternější	niterní	k2eAgFnPc4d3	niterní
oblasti	oblast	k1gFnPc4	oblast
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
-	-	kIx~	-
Louisem	Louis	k1gMnSc7	Louis
Aragonem	Aragon	k1gMnSc7	Aragon
<g/>
,	,	kIx,	,
Paulem	Paul	k1gMnSc7	Paul
Éluardem	Éluard	k1gMnSc7	Éluard
a	a	k8xC	a
Benjaminem	Benjamin	k1gMnSc7	Benjamin
Péretem	Péret	k1gMnSc7	Péret
-	-	kIx~	-
oficiálně	oficiálně	k6eAd1	oficiálně
ustavil	ustavit	k5eAaPmAgMnS	ustavit
surrealistickou	surrealistický	k2eAgFnSc4d1	surrealistická
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Breton	Breton	k1gMnSc1	Breton
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
jejího	její	k3xOp3gNnSc2	její
trvání	trvání	k1gNnSc2	trvání
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
teoretický	teoretický	k2eAgInSc4d1	teoretický
přínos	přínos	k1gInSc4	přínos
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výjimečné	výjimečný	k2eAgFnPc4d1	výjimečná
osobnostní	osobnostní	k2eAgFnPc4d1	osobnostní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
často	často	k6eAd1	často
nahlížen	nahlížen	k2eAgMnSc1d1	nahlížen
jako	jako	k8xC	jako
autoritativní	autoritativní	k2eAgMnSc1d1	autoritativní
vůdce	vůdce	k1gMnSc1	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
také	také	k9	také
vydal	vydat	k5eAaPmAgInS	vydat
Manifest	manifest	k1gInSc1	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
ho	on	k3xPp3gMnSc4	on
definoval	definovat	k5eAaBmAgInS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
čistý	čistý	k2eAgInSc1d1	čistý
psychický	psychický	k2eAgInSc1d1	psychický
automatismus	automatismus	k1gInSc1	automatismus
<g/>
"	"	kIx"	"
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
redaktorem	redaktor	k1gMnSc7	redaktor
revui	revue	k1gFnSc3	revue
La	la	k1gNnPc2	la
Révolution	Révolution	k1gInSc1	Révolution
Surréaliste	Surréalist	k1gMnSc5	Surréalist
(	(	kIx(	(
<g/>
dvanáct	dvanáct	k4xCc4	dvanáct
čísel	číslo	k1gNnPc2	číslo
1924	[number]	k4	1924
-	-	kIx~	-
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orgánu	orgán	k1gInSc3	orgán
nové	nový	k2eAgFnSc2d1	nová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
surrealismu	surrealismus	k1gInSc6	surrealismus
viděl	vidět	k5eAaImAgMnS	vidět
syntezi	synteze	k1gFnSc4	synteze
myšlenek	myšlenka	k1gFnPc2	myšlenka
Rimbauda	Rimbaudo	k1gNnSc2	Rimbaudo
-	-	kIx~	-
"	"	kIx"	"
<g/>
přeměnit	přeměnit	k5eAaPmF	přeměnit
život	život	k1gInSc4	život
<g/>
"	"	kIx"	"
a	a	k8xC	a
Marxe	Marx	k1gMnSc4	Marx
-	-	kIx~	-
"	"	kIx"	"
<g/>
změnit	změnit	k5eAaPmF	změnit
svět	svět	k1gInSc4	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
dostávat	dostávat	k5eAaImF	dostávat
do	do	k7c2	do
ideové	ideový	k2eAgFnSc2d1	ideová
blízkosti	blízkost	k1gFnSc2	blízkost
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
zpočátku	zpočátku	k6eAd1	zpočátku
odmítal	odmítat	k5eAaImAgMnS	odmítat
výzvy	výzva	k1gFnSc2	výzva
Pierra	Pierr	k1gMnSc2	Pierr
Navilla	Navill	k1gMnSc2	Navill
přejít	přejít	k5eAaPmF	přejít
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
akci	akce	k1gFnSc3	akce
a	a	k8xC	a
důraz	důraz	k1gInSc1	důraz
kladl	klást	k5eAaImAgInS	klást
stále	stále	k6eAd1	stále
především	především	k9	především
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
ducha	duch	k1gMnSc2	duch
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
vnější	vnější	k2eAgFnSc2d1	vnější
kontroly	kontrola	k1gFnSc2	kontrola
<g/>
,	,	kIx,	,
i	i	k9	i
marxistické	marxistický	k2eAgInPc4d1	marxistický
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Louis	Louis	k1gMnSc1	Louis
Aragon	Aragon	k1gMnSc1	Aragon
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Éluard	Éluard	k1gMnSc1	Éluard
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Péret	Péret	k1gMnSc1	Péret
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
Unik	unikum	k1gNnPc2	unikum
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
napsal	napsat	k5eAaPmAgInS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
dosud	dosud	k6eAd1	dosud
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
knihu	kniha	k1gFnSc4	kniha
-	-	kIx~	-
román	román	k1gInSc4	román
Nadja	Nadj	k1gInSc2	Nadj
<g/>
.	.	kIx.	.
</s>
<s>
Nezachovává	zachovávat	k5eNaImIp3nS	zachovávat
konvenční	konvenční	k2eAgFnSc4d1	konvenční
literární	literární	k2eAgFnSc4d1	literární
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
ostatně	ostatně	k6eAd1	ostatně
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
chápána	chápat	k5eAaImNgFnS	chápat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
bodů	bod	k1gInPc2	bod
kritiky	kritika	k1gFnSc2	kritika
surrealistů	surrealista	k1gMnPc2	surrealista
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
skutečný	skutečný	k2eAgInSc4d1	skutečný
příběh	příběh	k1gInSc4	příběh
způsobem	způsob	k1gInSc7	způsob
ozřejmujícím	ozřejmující	k2eAgInSc7d1	ozřejmující
surrealistické	surrealistický	k2eAgNnSc1d1	surrealistické
vnímání	vnímání	k1gNnSc1	vnímání
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
v	v	k7c6	v
Surrealismu	surrealismus	k1gInSc6	surrealismus
a	a	k8xC	a
malířství	malířství	k1gNnSc6	malířství
(	(	kIx(	(
<g/>
po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
vydané	vydaný	k2eAgFnSc2d1	vydaná
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
snažil	snažit	k5eAaImAgMnS	snažit
o	o	k7c6	o
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
teoretického	teoretický	k2eAgInSc2d1	teoretický
základu	základ	k1gInSc2	základ
surrealistickému	surrealistický	k2eAgNnSc3d1	surrealistické
malířství	malířství	k1gNnSc3	malířství
<g/>
,	,	kIx,	,
představovanému	představovaný	k2eAgNnSc3d1	představované
dle	dle	k7c2	dle
něj	on	k3xPp3gMnSc2	on
umělci	umělec	k1gMnPc1	umělec
jako	jako	k9	jako
Max	Max	k1gMnSc1	Max
Ernst	Ernst	k1gMnSc1	Ernst
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
<g/>
,	,	kIx,	,
Joan	Joan	k1gMnSc1	Joan
Miró	Miró	k1gMnSc1	Miró
nebo	nebo	k8xC	nebo
André	André	k1gMnSc1	André
Masson	Masson	k1gMnSc1	Masson
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
uvnitř	uvnitř	k7c2	uvnitř
surrealistické	surrealistický	k2eAgFnSc2d1	surrealistická
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
nutnosti	nutnost	k1gFnSc3	nutnost
jejího	její	k3xOp3gNnSc2	její
nového	nový	k2eAgNnSc2d1	nové
vymezení	vymezení	k1gNnSc2	vymezení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Druhém	druhý	k4xOgInSc6	druhý
manifestu	manifest	k1gInSc6	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
ho	on	k3xPp3gNnSc4	on
tak	tak	k9	tak
Breton	breton	k1gInSc1	breton
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
společensko	společensko	k6eAd1	společensko
revoluční	revoluční	k2eAgNnPc1d1	revoluční
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Breton	Breton	k1gMnSc1	Breton
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
měli	mít	k5eAaImAgMnP	mít
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInPc4d2	veliký
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Éluardem	Éluard	k1gMnSc7	Éluard
a	a	k8xC	a
Crevelem	Crevel	k1gMnSc7	Crevel
<g/>
,	,	kIx,	,
vyloučen	vyloučit	k5eAaPmNgMnS	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
PCF	PCF	kA	PCF
definitivně	definitivně	k6eAd1	definitivně
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Breton	Breton	k1gMnSc1	Breton
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kritikem	kritik	k1gMnSc7	kritik
stalinismu	stalinismus	k1gInSc2	stalinismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
španělské	španělský	k2eAgFnSc2d1	španělská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
surrealisté	surrealista	k1gMnPc1	surrealista
(	(	kIx(	(
<g/>
Tristan	Tristan	k1gInSc1	Tristan
Tzara	Tzar	k1gInSc2	Tzar
<g/>
,	,	kIx,	,
Benjamin	Benjamin	k1gMnSc1	Benjamin
Péret	Péret	k1gMnSc1	Péret
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připojil	připojit	k5eAaPmAgInS	připojit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
fašismu	fašismus	k1gInSc2	fašismus
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Georgesem	Georges	k1gMnSc7	Georges
Bataillem	Bataill	k1gMnSc7	Bataill
skupinu	skupina	k1gFnSc4	skupina
levicových	levicový	k2eAgMnPc2d1	levicový
revolučních	revoluční	k2eAgMnPc2d1	revoluční
intelektuálů	intelektuál	k1gMnPc2	intelektuál
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Protiútok	protiútok	k1gInSc1	protiútok
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
si	se	k3xPyFc3	se
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
kladla	klást	k5eAaImAgFnS	klást
především	především	k9	především
rozvoj	rozvoj	k1gInSc4	rozvoj
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
revolučních	revoluční	k2eAgFnPc2d1	revoluční
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
se	se	k3xPyFc4	se
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
setkal	setkat	k5eAaPmAgMnS	setkat
u	u	k7c2	u
malíře	malíř	k1gMnSc2	malíř
Diega	Dieg	k1gMnSc2	Dieg
Rivery	Rivera	k1gFnSc2	Rivera
se	s	k7c7	s
Lvem	Lev	k1gMnSc7	Lev
Trockým	Trocký	k1gMnSc7	Trocký
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
došli	dojít	k5eAaPmAgMnP	dojít
ke	k	k7c3	k
shodě	shoda	k1gFnSc3	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
revolučního	revoluční	k2eAgInSc2d1	revoluční
potenciálu	potenciál	k1gInSc2	potenciál
umění	umění	k1gNnSc2	umění
není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
politická	politický	k2eAgFnSc1d1	politická
kontrola	kontrola	k1gFnSc1	kontrola
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
manifest	manifest	k1gInSc4	manifest
Za	za	k7c4	za
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
revoluční	revoluční	k2eAgNnSc4d1	revoluční
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vyzýval	vyzývat	k5eAaImAgInS	vyzývat
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
revolučního	revoluční	k2eAgNnSc2d1	revoluční
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
FIARI	FIARI	kA	FIARI
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
byla	být	k5eAaImAgFnS	být
předem	předem	k6eAd1	předem
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
k	k	k7c3	k
neúspěchu	neúspěch	k1gInSc3	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
surrealismus	surrealismus	k1gInSc1	surrealismus
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
za	za	k7c4	za
hranice	hranice	k1gFnPc4	hranice
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Breton	Breton	k1gMnSc1	Breton
začal	začít	k5eAaPmAgMnS	začít
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
propagátora	propagátor	k1gMnSc2	propagátor
hnutí	hnutí	k1gNnSc2	hnutí
cestovat	cestovat	k5eAaImF	cestovat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pozvání	pozvání	k1gNnSc4	pozvání
Vítězslava	Vítězslav	k1gMnSc2	Vítězslav
Nezvala	Nezval	k1gMnSc2	Nezval
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
Československo	Československo	k1gNnSc4	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
organizoval	organizovat	k5eAaBmAgMnS	organizovat
v	v	k7c6	v
Galerii	galerie	k1gFnSc6	galerie
des-Beaux-Arts	des-Beaux-Arts	k6eAd1	des-Beaux-Arts
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
výstavu	výstava	k1gFnSc4	výstava
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Francie	Francie	k1gFnSc2	Francie
německým	německý	k2eAgInSc7d1	německý
Wehrmachtem	wehrmacht	k1gInSc7	wehrmacht
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
přes	přes	k7c4	přes
Malé	Malé	k2eAgFnPc4d1	Malé
Antily	Antily	k1gFnPc4	Antily
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Marcelem	Marcel	k1gMnSc7	Marcel
Duchampem	Duchamp	k1gMnSc7	Duchamp
a	a	k8xC	a
Maxem	Max	k1gMnSc7	Max
Ernstem	Ernst	k1gMnSc7	Ernst
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
časopisu	časopis	k1gInSc6	časopis
VVV	VVV	kA	VVV
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vydal	vydat	k5eAaPmAgInS	vydat
Prolegomena	prolegomena	k1gNnPc4	prolegomena
k	k	k7c3	k
Třetímu	třetí	k4xOgInSc3	třetí
manifestu	manifest	k1gInSc3	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
naděje	naděje	k1gFnSc1	naděje
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
společenský	společenský	k2eAgInSc4d1	společenský
a	a	k8xC	a
politický	politický	k2eAgInSc4d1	politický
začátek	začátek	k1gInSc4	začátek
byla	být	k5eAaImAgFnS	být
zklamána	zklamán	k2eAgFnSc1d1	zklamána
<g/>
.	.	kIx.	.
</s>
<s>
Pořádal	pořádat	k5eAaImAgMnS	pořádat
několik	několik	k4yIc4	několik
výstav	výstava	k1gFnPc2	výstava
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
působit	působit	k5eAaImF	působit
podpůrně	podpůrně	k6eAd1	podpůrně
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
umírá	umírat	k5eAaImIp3nS	umírat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
hřbitově	hřbitov	k1gInSc6	hřbitov
Batignolles	Batignolles	k1gInSc1	Batignolles
<g/>
.	.	kIx.	.
</s>
<s>
Zastavárna	zastavárna	k1gFnSc1	zastavárna
(	(	kIx(	(
<g/>
Mont	Mont	k1gMnSc1	Mont
de	de	k?	de
piété	piétý	k2eAgNnSc1d1	piétý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
pole	pole	k1gFnSc1	pole
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Champs	Champsa	k1gFnPc2	Champsa
magnétiques	magnétiquesa	k1gFnPc2	magnétiquesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Philippem	Philipp	k1gInSc7	Philipp
Soupaultem	Soupault	k1gInSc7	Soupault
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
Svit	svit	k1gInSc1	svit
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Clair	Clair	k1gMnSc1	Clair
de	de	k?	de
Terre	Terr	k1gMnSc5	Terr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
Manifest	manifest	k1gInSc1	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Manifeste	manifest	k1gInSc5	manifest
du	du	k?	du
Surréalisme	Surréalismus	k1gInSc5	Surréalismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
Rozpustný	rozpustný	k2eAgInSc1d1	rozpustný
ryba	ryba	k1gFnSc1	ryba
(	(	kIx(	(
<g/>
Poisson	Poisson	k1gMnSc1	Poisson
Soluble	Soluble	k1gMnSc1	Soluble
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
integrální	integrální	k2eAgFnSc1d1	integrální
součást	součást	k1gFnSc1	součást
manifestu	manifest	k1gInSc2	manifest
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
Nadja	Nadjum	k1gNnSc2	Nadjum
<g/>
,	,	kIx,	,
1928-1962	[number]	k4	1928-1962
Surrealismus	surrealismus	k1gInSc4	surrealismus
a	a	k8xC	a
malířství	malířství	k1gNnSc2	malířství
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
Surréalisme	Surréalismus	k1gInSc5	Surréalismus
et	et	k?	et
la	la	k0	la
peinture	peintur	k1gMnSc5	peintur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1928-1965	[number]	k4	1928-1965
Druhý	druhý	k4xOgInSc1	druhý
manifest	manifest	k1gInSc1	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
(	(	kIx(	(
<g/>
Second	Second	k1gInSc1	Second
Manifeste	manifest	k1gInSc5	manifest
<g />
.	.	kIx.	.
</s>
<s>
du	du	k?	du
Surréalisme	Surréalismus	k1gInSc5	Surréalismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
Neposkvrněné	poskvrněný	k2eNgNnSc4d1	neposkvrněné
početí	početí	k1gNnSc4	početí
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Immaculée	Immaculée	k1gInSc1	Immaculée
Conception	Conception	k1gInSc1	Conception
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Éluardem	Éluard	k1gMnSc7	Éluard
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
Spojité	spojitý	k2eAgFnPc1d1	spojitá
nádoby	nádoba	k1gFnPc1	nádoba
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Vases	Vasesa	k1gFnPc2	Vasesa
communicants	communicantsa	k1gFnPc2	communicantsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
Revolver	revolver	k1gInSc1	revolver
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
vlasy	vlas	k1gInPc7	vlas
(	(	kIx(	(
<g/>
Le	Le	k1gFnSc1	Le
revolver	revolver	k1gInSc1	revolver
a	a	k8xC	a
cheveux	cheveux	k1gInSc1	cheveux
blancs	blancsa	k1gFnPc2	blancsa
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
Vzduch	vzduch	k1gInSc1	vzduch
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
air	air	k?	air
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
eau	eau	k?	eau
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
Šílená	šílený	k2eAgFnSc1d1	šílená
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Amour	Amour	k1gMnSc1	Amour
fou	fou	k?	fou
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1937	[number]	k4	1937
Za	za	k7c4	za
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
revoluční	revoluční	k2eAgNnSc4d1	revoluční
umění	umění	k1gNnSc4	umění
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Pour	Pour	k1gMnSc1	Pour
un	un	k?	un
art	art	k?	art
indépendant	indépendant	k1gInSc1	indépendant
révolutionnaire	révolutionnair	k1gMnSc5	révolutionnair
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
Lvem	Lev	k1gMnSc7	Lev
Trockým	Trocký	k1gMnSc7	Trocký
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
Antologie	antologie	k1gFnPc4	antologie
černého	černý	k2eAgInSc2d1	černý
humoru	humor	k1gInSc2	humor
(	(	kIx(	(
<g/>
Anthologie	Anthologie	k1gFnSc1	Anthologie
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
humour	humour	k1gMnSc1	humour
noir	noir	k1gMnSc1	noir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
Prolegomena	prolegomena	k1gNnPc1	prolegomena
k	k	k7c3	k
třetímu	třetí	k4xOgInSc3	třetí
manifestu	manifest	k1gInSc3	manifest
surrealismu	surrealismus	k1gInSc2	surrealismus
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
ne	ne	k9	ne
(	(	kIx(	(
<g/>
Prolégomè	Prolégomè	k1gFnSc1	Prolégomè
à	à	k?	à
un	un	k?	un
troisiè	troisiè	k?	troisiè
<g />
.	.	kIx.	.
</s>
<s>
manifeste	manifest	k1gInSc5	manifest
ou	ou	k0	ou
non	non	k?	non
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1942	[number]	k4	1942
Arkán	arkán	k1gInSc1	arkán
17	[number]	k4	17
(	(	kIx(	(
<g/>
Arcane	Arcan	k1gMnSc5	Arcan
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
Óda	óda	k1gFnSc1	óda
na	na	k7c4	na
Charlese	Charles	k1gMnSc4	Charles
Fouriera	Fourier	k1gMnSc4	Fourier
(	(	kIx(	(
<g/>
Ode	ode	k7c2	ode
à	à	k?	à
Charles	Charles	k1gMnSc1	Charles
Fourier	Fourier	k1gMnSc1	Fourier
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
Rozhovory	rozhovor	k1gInPc4	rozhovor
(	(	kIx(	(
<g/>
Entretiens	Entretiens	k1gInSc4	Entretiens
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1952	[number]	k4	1952
Klíč	klíč	k1gInSc1	klíč
k	k	k7c3	k
volnosti	volnost	k1gFnSc3	volnost
(	(	kIx(	(
<g/>
La	la	k1gNnSc3	la
Clé	Clé	k1gFnSc2	Clé
des	des	k1gNnSc2	des
champs	champsa	k1gFnPc2	champsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1953	[number]	k4	1953
</s>
