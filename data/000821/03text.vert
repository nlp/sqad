<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
byli	být	k5eAaImAgMnP	být
druhou	druhý	k4xOgFnSc7	druhý
českou	český	k2eAgFnSc7d1	Česká
královskou	královský	k2eAgFnSc7d1	královská
dynastií	dynastie	k1gFnSc7	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Zemím	zem	k1gFnPc3	zem
Koruny	koruna	k1gFnPc1	koruna
české	český	k2eAgMnPc4d1	český
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1310	[number]	k4	1310
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1196	[number]	k4	1196
vládli	vládnout	k5eAaImAgMnP	vládnout
v	v	k7c6	v
lucemburském	lucemburský	k2eAgNnSc6d1	lucemburské
hrabství	hrabství	k1gNnSc6	hrabství
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1387	[number]	k4	1387
získali	získat	k5eAaPmAgMnP	získat
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1373	[number]	k4	1373
<g/>
-	-	kIx~	-
<g/>
1415	[number]	k4	1415
drželi	držet	k5eAaImAgMnP	držet
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
drželi	držet	k5eAaImAgMnP	držet
říšskou	říšský	k2eAgFnSc4d1	říšská
korunu	koruna	k1gFnSc4	koruna
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1437	[number]	k4	1437
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
let	léto	k1gNnPc2	léto
1313-1346	[number]	k4	1313-1346
a	a	k8xC	a
1400	[number]	k4	1400
<g/>
-	-	kIx~	-
<g/>
1410	[number]	k4	1410
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
spisu	spis	k1gInSc2	spis
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Arrasu	Arras	k1gInSc2	Arras
odvozovali	odvozovat	k5eAaImAgMnP	odvozovat
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
bájné	bájný	k2eAgFnSc2d1	bájná
víly	víla	k1gFnSc2	víla
Meluzíny	Meluzína	k1gFnSc2	Meluzína
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc7d1	skutečná
zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
rodu	rod	k1gInSc2	rod
však	však	k9	však
byla	být	k5eAaImAgFnS	být
hraběnka	hraběnka	k1gFnSc1	hraběnka
Ermesinda	Ermesinda	k1gFnSc1	Ermesinda
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1186	[number]	k4	1186
-	-	kIx~	-
1247	[number]	k4	1247
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
potomek	potomek	k1gMnSc1	potomek
hraběte	hrabě	k1gMnSc2	hrabě
Jindřicha	Jindřich	k1gMnSc2	Jindřich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Slepého	slepý	k2eAgInSc2d1	slepý
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
manželství	manželství	k1gNnSc2	manželství
s	s	k7c7	s
Walramem	Walram	k1gInSc7	Walram
<g/>
,	,	kIx,	,
hrabětem	hrabě	k1gMnSc7	hrabě
Limburským	limburský	k2eAgFnPc3d1	Limburská
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
byli	být	k5eAaImAgMnP	být
někdy	někdy	k6eAd1	někdy
označováni	označován	k2eAgMnPc1d1	označován
i	i	k9	i
jako	jako	k9	jako
rod	rod	k1gInSc1	rod
limburský	limburský	k2eAgInSc1d1	limburský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc7d1	původní
državou	država	k1gFnSc7	država
rodu	rod	k1gInSc2	rod
bylo	být	k5eAaImAgNnS	být
nevelké	velký	k2eNgNnSc1d1	nevelké
hrabství	hrabství	k1gNnSc1	hrabství
lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
hranici	hranice	k1gFnSc6	hranice
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
území	území	k1gNnSc1	území
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
Lucemburského	lucemburský	k2eAgNnSc2d1	lucemburské
a	a	k8xC	a
belgické	belgický	k2eAgFnPc1d1	belgická
provincie	provincie	k1gFnPc1	provincie
Luxembourg	Luxembourg	k1gInSc4	Luxembourg
<g/>
.	.	kIx.	.
</s>
<s>
Mocenský	mocenský	k2eAgInSc1d1	mocenský
vzestup	vzestup	k1gInSc1	vzestup
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
souvisel	souviset	k5eAaImAgMnS	souviset
se	s	k7c7	s
strategickým	strategický	k2eAgNnSc7d1	strategické
spojenectvím	spojenectví	k1gNnSc7	spojenectví
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
zájmy	zájem	k1gInPc1	zájem
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
zastupovali	zastupovat	k5eAaImAgMnP	zastupovat
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
úspěchem	úspěch	k1gInSc7	úspěch
bylo	být	k5eAaImAgNnS	být
zvolení	zvolení	k1gNnSc1	zvolení
Balduina	Balduin	k1gMnSc2	Balduin
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
na	na	k7c4	na
stolec	stolec	k1gInSc4	stolec
trevírského	trevírský	k2eAgMnSc2d1	trevírský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
-	-	kIx~	-
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmocnějších	mocný	k2eAgMnPc2d3	nejmocnější
mužů	muž	k1gMnPc2	muž
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1308	[number]	k4	1308
se	se	k3xPyFc4	se
rodu	rod	k1gInSc6	rod
lucemburských	lucemburský	k2eAgNnPc2d1	lucemburské
hrabat	hrabě	k1gNnPc2	hrabě
dostalo	dostat	k5eAaPmAgNnS	dostat
největší	veliký	k2eAgFnSc3d3	veliký
cti	čest	k1gFnSc3	čest
<g/>
:	:	kIx,	:
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
násilné	násilný	k2eAgFnSc6d1	násilná
smrti	smrt	k1gFnSc6	smrt
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgInSc2d1	habsburský
zvolen	zvolen	k2eAgMnSc1d1	zvolen
králem	král	k1gMnSc7	král
v	v	k7c6	v
římskoněmecké	římskoněmecký	k2eAgFnSc6d1	římskoněmecká
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
jeviště	jeviště	k1gNnSc1	jeviště
dějin	dějiny	k1gFnPc2	dějiny
dramaticky	dramaticky	k6eAd1	dramaticky
roku	rok	k1gInSc2	rok
1306	[number]	k4	1306
opustili	opustit	k5eAaPmAgMnP	opustit
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
roku	rok	k1gInSc2	rok
1309	[number]	k4	1309
nabídnut	nabídnut	k2eAgInSc1d1	nabídnut
český	český	k2eAgInSc1d1	český
trůn	trůn	k1gInSc1	trůn
pro	pro	k7c4	pro
jeho	on	k3xPp3gMnSc4	on
jediného	jediný	k2eAgMnSc4d1	jediný
syna	syn	k1gMnSc4	syn
Jana	Jan	k1gMnSc4	Jan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Janovi	Jan	k1gMnSc6	Jan
Lucemburském	lucemburský	k2eAgMnSc6d1	lucemburský
se	se	k3xPyFc4	se
protnuly	protnout	k5eAaPmAgInP	protnout
hned	hned	k6eAd1	hned
tři	tři	k4xCgInPc1	tři
nástupnické	nástupnický	k2eAgInPc1d1	nástupnický
principy	princip	k1gInPc1	princip
<g/>
:	:	kIx,	:
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
udělil	udělit	k5eAaPmAgMnS	udělit
Čechy	Čechy	k1gFnPc4	Čechy
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
<g/>
,	,	kIx,	,
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Eliškou	Eliška	k1gFnSc7	Eliška
Přemyslovnou	Přemyslovna	k1gFnSc7	Přemyslovna
a	a	k8xC	a
šlechta	šlechta	k1gFnSc1	šlechta
ho	on	k3xPp3gMnSc4	on
uznala	uznat	k5eAaPmAgFnS	uznat
za	za	k7c4	za
krále	král	k1gMnSc4	král
<g/>
.	.	kIx.	.
</s>
<s>
Kariéra	kariéra	k1gFnSc1	kariéra
Janova	Janov	k1gInSc2	Janov
otce	otec	k1gMnSc2	otec
byla	být	k5eAaImAgFnS	být
krátká	krátká	k1gFnSc1	krátká
a	a	k8xC	a
oslnivá	oslnivý	k2eAgFnSc1d1	oslnivá
<g/>
:	:	kIx,	:
roku	rok	k1gInSc2	rok
1312	[number]	k4	1312
byl	být	k5eAaImAgInS	být
korunován	korunovat	k5eAaBmNgInS	korunovat
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
zůstal	zůstat	k5eAaPmAgInS	zůstat
cizincem	cizinec	k1gMnSc7	cizinec
<g/>
,	,	kIx,	,
posloužily	posloužit	k5eAaPmAgFnP	posloužit
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
jako	jako	k8xS	jako
odrazový	odrazový	k2eAgInSc4d1	odrazový
můstek	můstek	k1gInSc4	můstek
pro	pro	k7c4	pro
výrazné	výrazný	k2eAgNnSc4d1	výrazné
ovlivňování	ovlivňování	k1gNnSc4	ovlivňování
evropské	evropský	k2eAgFnSc2d1	Evropská
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Janův	Janův	k2eAgMnSc1d1	Janův
syn	syn	k1gMnSc1	syn
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
stal	stát	k5eAaPmAgMnS	stát
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vedle	vedle	k7c2	vedle
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
Paříže	Paříž	k1gFnSc2	Paříž
přední	přední	k2eAgFnSc7d1	přední
evropskou	evropský	k2eAgFnSc7d1	Evropská
metropolí	metropol	k1gFnSc7	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
založení	založení	k1gNnSc6	založení
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
Menšího	malý	k2eAgNnSc2d2	menší
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
(	(	kIx(	(
<g/>
Malé	malé	k1gNnSc1	malé
Strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
třetím	třetí	k4xOgInSc7	třetí
největším	veliký	k2eAgInSc7d3	veliký
městem	město	k1gNnSc7	město
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
po	po	k7c6	po
Římu	Řím	k1gInSc3	Řím
a	a	k8xC	a
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zde	zde	k6eAd1	zde
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
Alp	Alpy	k1gFnPc2	Alpy
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Rýna	Rýn	k1gInSc2	Rýn
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnSc6d1	římská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
panování	panování	k1gNnSc2	panování
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
znamenala	znamenat	k5eAaImAgFnS	znamenat
největší	veliký	k2eAgInSc4d3	veliký
rozkvět	rozkvět	k1gInSc4	rozkvět
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
závěti	závěť	k1gFnSc2	závěť
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
udělil	udělit	k5eAaPmAgMnS	udělit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
mladšímu	mladý	k2eAgMnSc3d2	mladší
bratrovi	bratr	k1gMnSc3	bratr
Janu	Jan	k1gMnSc3	Jan
Jindřichovi	Jindřich	k1gMnSc3	Jindřich
v	v	k7c4	v
léno	léno	k1gNnSc4	léno
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
pamatoval	pamatovat	k5eAaImAgInS	pamatovat
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
příbuzné	příbuzný	k1gMnPc4	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
příbuzenstvo	příbuzenstvo	k1gNnSc1	příbuzenstvo
početné	početný	k2eAgNnSc1d1	početné
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
mělo	mít	k5eAaImAgNnS	mít
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgMnPc4	tři
syny	syn	k1gMnPc4	syn
<g/>
:	:	kIx,	:
Václava	Václav	k1gMnSc4	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Zikmunda	Zikmund	k1gMnSc2	Zikmund
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Zhořeleckého	zhořelecký	k2eAgMnSc2d1	zhořelecký
<g/>
;	;	kIx,	;
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
rovněž	rovněž	k9	rovněž
tři	tři	k4xCgInPc4	tři
<g/>
:	:	kIx,	:
Jošta	Jošt	k1gMnSc2	Jošt
<g/>
,	,	kIx,	,
Prokopa	Prokop	k1gMnSc2	Prokop
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Soběslava	Soběslav	k1gMnSc2	Soběslav
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Karla	Karel	k1gMnSc2	Karel
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
se	se	k3xPyFc4	se
stupňovala	stupňovat	k5eAaImAgFnS	stupňovat
rivalita	rivalita	k1gFnSc1	rivalita
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
,	,	kIx,	,
moravští	moravský	k2eAgMnPc1d1	moravský
bratranci	bratranec	k1gMnPc1	bratranec
zase	zase	k9	zase
bojovali	bojovat	k5eAaImAgMnP	bojovat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Moravou	Morava	k1gFnSc7	Morava
a	a	k8xC	a
zapojovali	zapojovat	k5eAaImAgMnP	zapojovat
se	se	k3xPyFc4	se
i	i	k9	i
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
bojovaly	bojovat	k5eAaImAgInP	bojovat
obě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
zároveň	zároveň	k6eAd1	zároveň
<g/>
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Zikmund	Zikmund	k1gMnSc1	Zikmund
a	a	k8xC	a
Jošt	Jošt	k1gMnSc1	Jošt
Moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
Wittelsbach	Wittelsbach	k1gInSc4	Wittelsbach
Ruprecht	Ruprecht	k2eAgMnSc1d1	Ruprecht
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vítězem	vítěz	k1gMnSc7	vítěz
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Zikmund	Zikmund	k1gMnSc1	Zikmund
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
rivaly	rival	k1gMnPc4	rival
přežil	přežít	k5eAaPmAgMnS	přežít
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
ale	ale	k9	ale
mohl	moct	k5eAaImAgInS	moct
nastoupit	nastoupit	k5eAaPmF	nastoupit
až	až	k9	až
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
válek	válka	k1gFnPc2	válka
s	s	k7c7	s
husity	husita	k1gMnPc7	husita
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
legitimní	legitimní	k2eAgMnSc1d1	legitimní
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1437	[number]	k4	1437
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Zikmund	Zikmund	k1gMnSc1	Zikmund
za	za	k7c2	za
nástupce	nástupce	k1gMnSc2	nástupce
určil	určit	k5eAaPmAgMnS	určit
manžela	manžel	k1gMnSc4	manžel
své	svůj	k3xOyFgFnSc2	svůj
jediné	jediný	k2eAgFnSc2d1	jediná
dcery	dcera	k1gFnSc2	dcera
Albrechta	Albrecht	k1gMnSc2	Albrecht
<g/>
,	,	kIx,	,
po	po	k7c6	po
Albrechtově	Albrechtův	k2eAgFnSc6d1	Albrechtova
brzké	brzký	k2eAgFnSc6d1	brzká
smrti	smrt	k1gFnSc6	smrt
tituly	titul	k1gInPc4	titul
zdědil	zdědit	k5eAaPmAgMnS	zdědit
jejich	jejich	k3xOp3gNnSc4	jejich
jediný	jediný	k2eAgMnSc1d1	jediný
syn	syn	k1gMnSc1	syn
Ladislav	Ladislav	k1gMnSc1	Ladislav
Pohrobek	pohrobek	k1gMnSc1	pohrobek
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
koruna	koruna	k1gFnSc1	koruna
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
Habsburky	Habsburk	k1gMnPc4	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
udrželi	udržet	k5eAaPmAgMnP	udržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
a	a	k8xC	a
uherském	uherský	k2eAgInSc6d1	uherský
státě	stát	k1gInSc6	stát
aristokracie	aristokracie	k1gFnSc2	aristokracie
po	po	k7c6	po
Pohrobkově	pohrobkův	k2eAgFnSc6d1	pohrobkův
smrti	smrt	k1gFnSc6	smrt
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
nástupnictví	nástupnictví	k1gNnSc4	nástupnictví
dalších	další	k2eAgFnPc2d1	další
příbuzných	příbuzná	k1gFnPc2	příbuzná
a	a	k8xC	a
zvolila	zvolit	k5eAaPmAgFnS	zvolit
si	se	k3xPyFc3	se
krále	král	k1gMnSc4	král
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
(	(	kIx(	(
<g/>
Jiří	Jiří	k1gMnSc1	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
a	a	k8xC	a
Matyáš	Matyáš	k1gMnSc1	Matyáš
Korvín	Korvín	k1gMnSc1	Korvín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejich	jejich	k3xOp3gFnSc6	jejich
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
ale	ale	k9	ale
navrátila	navrátit	k5eAaPmAgFnS	navrátit
k	k	k7c3	k
Vladislavovi	Vladislav	k1gMnSc3	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jagellonskému	jagellonský	k2eAgMnSc3d1	jagellonský
<g/>
,	,	kIx,	,
vnukovi	vnuk	k1gMnSc3	vnuk
Albrechta	Albrecht	k1gMnSc2	Albrecht
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
pravnukovi	pravnukův	k2eAgMnPc1d1	pravnukův
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
po	po	k7c6	po
meči	meč	k1gInSc6	meč
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
markraběte	markrabě	k1gMnSc2	markrabě
Prokopa	Prokop	k1gMnSc2	Prokop
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k8xC	jako
benediktinský	benediktinský	k2eAgMnSc1d1	benediktinský
mnich	mnich	k1gMnSc1	mnich
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Monte	Mont	k1gInSc5	Mont
Cassinu	Cassina	k1gFnSc4	Cassina
roku	rok	k1gInSc2	rok
1457	[number]	k4	1457
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
levoboček	levoboček	k1gMnSc1	levoboček
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
právo	právo	k1gNnSc4	právo
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
ho	on	k3xPp3gInSc4	on
historie	historie	k1gFnSc1	historie
téměř	téměř	k6eAd1	téměř
nepřipomíná	připomínat	k5eNaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
dynastie	dynastie	k1gFnSc2	dynastie
ovšem	ovšem	k9	ovšem
existovala	existovat	k5eAaImAgFnS	existovat
další	další	k2eAgFnSc1d1	další
linie	linie	k1gFnSc1	linie
rodu	rod	k1gInSc2	rod
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
<g/>
,	,	kIx,	,
potomků	potomek	k1gMnPc2	potomek
bratrance	bratranec	k1gMnSc2	bratranec
císaře	císař	k1gMnSc2	císař
Jindřicha	Jindřich	k1gMnSc2	Jindřich
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
královskou	královský	k2eAgFnSc4d1	královská
linii	linie	k1gFnSc4	linie
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
staletí	staletí	k1gNnPc2	staletí
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
upomíná	upomínat	k5eAaImIp3nS	upomínat
například	například	k6eAd1	například
slavný	slavný	k2eAgInSc1d1	slavný
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
palác	palác	k1gInSc1	palác
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
k	k	k7c3	k
oddělení	oddělení	k1gNnSc1	oddělení
obou	dva	k4xCgFnPc2	dva
větví	větev	k1gFnPc2	větev
došlo	dojít	k5eAaPmAgNnS	dojít
dlouho	dlouho	k6eAd1	dlouho
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
Lucemburků	Lucemburk	k1gMnPc2	Lucemburk
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
větev	větev	k1gFnSc1	větev
rodu	rod	k1gInSc2	rod
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nástupnické	nástupnický	k2eAgNnSc4d1	nástupnické
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
do	do	k7c2	do
našich	náš	k3xOp1gFnPc2	náš
dějin	dějiny	k1gFnPc2	dějiny
téměř	téměř	k6eAd1	téměř
nezasáhla	zasáhnout	k5eNaPmAgFnS	zasáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vládla	vládnout	k5eAaImAgFnS	vládnout
např.	např.	kA	např.
v	v	k7c4	v
Ligny	Ligna	k1gFnPc4	Ligna
<g/>
,	,	kIx,	,
Saint-	Saint-	k1gFnPc4	Saint-
Pol	pola	k1gFnPc2	pola
nebo	nebo	k8xC	nebo
Brienne	Brienn	k1gInSc5	Brienn
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vymření	vymření	k1gNnSc3	vymření
této	tento	k3xDgFnSc2	tento
větve	větev	k1gFnSc2	větev
po	po	k7c6	po
meči	meč	k1gInSc6	meč
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1616	[number]	k4	1616
smrtí	smrtit	k5eAaImIp3nP	smrtit
Jindřicha	Jindřich	k1gMnSc4	Jindřich
<g/>
,	,	kIx,	,
hraběte	hrabě	k1gMnSc4	hrabě
z	z	k7c2	z
Ligny	Ligna	k1gFnSc2	Ligna
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
otec	otec	k1gMnSc1	otec
získal	získat	k5eAaPmAgMnS	získat
titul	titul	k1gInSc4	titul
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Piney	Pinea	k1gFnSc2	Pinea
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poslední	poslední	k2eAgMnSc1d1	poslední
mužský	mužský	k2eAgMnSc1d1	mužský
potomek	potomek	k1gMnSc1	potomek
uznaného	uznaný	k2eAgMnSc2d1	uznaný
levobočka	levoboček	k1gMnSc2	levoboček
Antoina	Antoin	k1gMnSc2	Antoin
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bastarda	bastard	k1gMnSc4	bastard
z	z	k7c2	z
Brienne	Brienn	k1gMnSc5	Brienn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Chapelle	Chapelle	k1gFnSc2	Chapelle
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
Habsburků	Habsburk	k1gInPc2	Habsburk
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
používalo	používat	k5eAaImAgNnS	používat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
rodem	rod	k1gInSc7	rod
např.	např.	kA	např.
Montmorency-Luxembourg	Montmorency-Luxembourg	k1gInSc1	Montmorency-Luxembourg
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
-	-	kIx~	-
(	(	kIx(	(
<g/>
1310	[number]	k4	1310
<g/>
-	-	kIx~	-
<g/>
1346	[number]	k4	1346
<g/>
)	)	kIx)	)
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
též	též	k9	též
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
-	-	kIx~	-
(	(	kIx(	(
<g/>
1346	[number]	k4	1346
<g/>
-	-	kIx~	-
<g/>
1378	[number]	k4	1378
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
Karlštejn	Karlštejn	k1gInSc1	Karlštejn
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
též	též	k9	též
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
-	-	kIx~	-
(	(	kIx(	(
<g/>
1378	[number]	k4	1378
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
též	též	k9	též
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
-	-	kIx~	-
(	(	kIx(	(
<g/>
1433	[number]	k4	1433
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
)	)	kIx)	)
a	a	k8xC	a
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
-	-	kIx~	-
(	(	kIx(	(
<g/>
1420	[number]	k4	1420
<g/>
-	-	kIx~	-
<g/>
1437	[number]	k4	1437
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
Jošt	Jošt	k1gMnSc1	Jošt
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
,	,	kIx,	,
též	též	k9	též
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Jan	Jan	k1gMnSc1	Jan
Soběslav	Soběslav	k1gMnSc1	Soběslav
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
Prokop	Prokop	k1gMnSc1	Prokop
Václav	Václav	k1gMnSc1	Václav
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
Balduin	Balduin	k1gInSc1	Balduin
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
a	a	k8xC	a
říšský	říšský	k2eAgMnSc1d1	říšský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Jan	Jan	k1gMnSc1	Jan
Zhořelecký	zhořelecký	k2eAgMnSc1d1	zhořelecký
Eliška	Eliška	k1gFnSc1	Eliška
Zhořelecká	zhořelecký	k2eAgFnSc1d1	Zhořelecká
Anna	Anna	k1gFnSc1	Anna
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
∞	∞	k?	∞
</s>
