<s>
Děvín	Děvín	k1gInSc1	Děvín
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
554	[number]	k4	554
metrů	metr	k1gInPc2	metr
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
Mikulovské	mikulovský	k2eAgFnSc2d1	Mikulovská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
i	i	k8xC	i
celých	celý	k2eAgInPc2d1	celý
Jihomoravských	jihomoravský	k2eAgInPc2d1	jihomoravský
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
podlouhlý	podlouhlý	k2eAgInSc1d1	podlouhlý
hřeben	hřeben	k1gInSc1	hřeben
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
vrcholy	vrchol	k1gInPc7	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vyšším	vysoký	k2eAgMnSc6d2	vyšší
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
,	,	kIx,	,
na	na	k7c6	na
nižším	nízký	k2eAgInSc6d2	nižší
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
Děvičky	Děvička	k1gFnSc2	Děvička
(	(	kIx(	(
<g/>
Dívčí	dívčí	k2eAgInPc1d1	dívčí
hrady	hrad	k1gInPc1	hrad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
jasného	jasný	k2eAgNnSc2d1	jasné
počasí	počasí	k1gNnSc2	počasí
vidět	vidět	k5eAaImF	vidět
až	až	k9	až
ke	k	k7c3	k
40	[number]	k4	40
km	km	kA	km
vzdálenému	vzdálený	k2eAgNnSc3d1	vzdálené
Brnu	Brno	k1gNnSc3	Brno
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Javořinu	Javořina	k1gFnSc4	Javořina
(	(	kIx(	(
<g/>
970	[number]	k4	970
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Děvín	Děvín	k1gInSc1	Děvín
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Národní	národní	k2eAgFnSc2d1	národní
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Děvín-Kotel-Soutěska	Děvín-Kotel-Soutěsk	k1gInSc2	Děvín-Kotel-Soutěsk
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Děvín	Děvín	k1gInSc4	Děvín
vede	vést	k5eAaImIp3nS	vést
červeně	červeně	k6eAd1	červeně
značená	značený	k2eAgFnSc1d1	značená
turistická	turistický	k2eAgFnSc1d1	turistická
cesta	cesta	k1gFnSc1	cesta
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Klentnice	Klentnice	k1gFnSc1	Klentnice
a	a	k8xC	a
Dolní	dolní	k2eAgFnPc1d1	dolní
Věstonice	Věstonice	k1gFnPc1	Věstonice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
i	i	k9	i
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Děvín	Děvín	k1gInSc1	Děvín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
vrchol	vrchol	k1gInSc4	vrchol
vede	vést	k5eAaImIp3nS	vést
zeleně	zeleně	k6eAd1	zeleně
značená	značený	k2eAgFnSc1d1	značená
odbočka	odbočka	k1gFnSc1	odbočka
z	z	k7c2	z
naučné	naučný	k2eAgFnSc2d1	naučná
stezky	stezka	k1gFnSc2	stezka
<g/>
.	.	kIx.	.
</s>
