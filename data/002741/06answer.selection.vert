<s>
Děvín	Děvín	k1gInSc1	Děvín
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
554	[number]	k4	554
metrů	metr	k1gInPc2	metr
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Pavlovských	pavlovský	k2eAgInPc2d1	pavlovský
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
Mikulovské	mikulovský	k2eAgFnSc2d1	Mikulovská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
i	i	k8xC	i
celých	celý	k2eAgInPc2d1	celý
Jihomoravských	jihomoravský	k2eAgInPc2d1	jihomoravský
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
.	.	kIx.	.
</s>
