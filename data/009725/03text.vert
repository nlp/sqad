<p>
<s>
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	Lavičková	k1gFnSc1	Lavičková
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
složení	složení	k1gNnSc6	složení
maturitních	maturitní	k2eAgFnPc2d1	maturitní
zkoušek	zkouška	k1gFnPc2	zkouška
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
do	do	k7c2	do
konkurzu	konkurz	k1gInSc2	konkurz
na	na	k7c4	na
programovou	programový	k2eAgFnSc4d1	programová
hlasatelku	hlasatelka	k1gFnSc4	hlasatelka
na	na	k7c4	na
Český	český	k2eAgInSc4d1	český
rozhlas	rozhlas	k1gInSc4	rozhlas
Vltava	Vltava	k1gFnSc1	Vltava
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
vybrána	vybrat	k5eAaPmNgFnS	vybrat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
dálkově	dálkově	k6eAd1	dálkově
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
Fakultu	fakulta	k1gFnSc4	fakulta
žurnalistiky	žurnalistika	k1gFnSc2	žurnalistika
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
působila	působit	k5eAaImAgFnS	působit
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
v	v	k7c6	v
soukromé	soukromý	k2eAgFnSc6d1	soukromá
stanici	stanice	k1gFnSc6	stanice
Rio	Rio	k1gFnSc2	Rio
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
moderovala	moderovat	k5eAaBmAgFnS	moderovat
pořad	pořad	k1gInSc4	pořad
Motorevue	Motorevu	k1gFnSc2	Motorevu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c6	na
Dvojce	dvojka	k1gFnSc6	dvojka
pořady	pořad	k1gInPc1	pořad
Dopoledne	dopoledne	k1gNnSc1	dopoledne
s	s	k7c7	s
Dvojkou	dvojka	k1gFnSc7	dvojka
a	a	k8xC	a
Výlety	výlet	k1gInPc1	výlet
s	s	k7c7	s
Dvojkou	dvojka	k1gFnSc7	dvojka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	lavičkový	k2eAgFnSc1d1	lavičková
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
namluvila	namluvit	k5eAaBmAgFnS	namluvit
hlášení	hlášení	k1gNnSc4	hlášení
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
A	a	k9	a
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
hlas	hlas	k1gInSc4	hlas
mohou	moct	k5eAaImIp3nP	moct
cestující	cestující	k1gMnPc1	cestující
slyšet	slyšet	k5eAaImF	slyšet
stále	stále	k6eAd1	stále
a	a	k8xC	a
Lavičková	Lavičková	k1gFnSc1	Lavičková
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
metru	metro	k1gNnSc6	metro
historicky	historicky	k6eAd1	historicky
nejdéle	dlouho	k6eAd3	dlouho
sloužící	sloužící	k2eAgFnSc7d1	sloužící
hlasatelkou	hlasatelka	k1gFnSc7	hlasatelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	lavičkový	k2eAgFnSc1d1	lavičková
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Dvojka	dvojka	k1gFnSc1	dvojka
</s>
</p>
<p>
<s>
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	Lavičková	k1gFnSc5	Lavičková
<g/>
:	:	kIx,	:
Jsem	být	k5eAaImIp1nS	být
nejdéle	dlouho	k6eAd3	dlouho
sloužící	sloužící	k2eAgInSc4d1	sloužící
hlas	hlas	k1gInSc4	hlas
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
</s>
</p>
<p>
<s>
Hlas	hlas	k1gInSc1	hlas
metra	metro	k1gNnSc2	metro
A	a	k8xC	a
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
namluvit	namluvit	k5eAaPmF	namluvit
nové	nový	k2eAgFnPc4d1	nová
stanice	stanice	k1gFnPc4	stanice
na	na	k7c4	na
Metro	metro	k1gNnSc4	metro
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Světlana	Světlana	k1gFnSc1	Světlana
Lavičková	Lavičková	k1gFnSc1	Lavičková
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Knop	Knop	k?	Knop
-	-	kIx~	-
Hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zná	znát	k5eAaImIp3nS	znát
každý	každý	k3xTgInSc4	každý
na	na	k7c4	na
Stream	Stream	k1gInSc4	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Takhle	takhle	k6eAd1	takhle
vypadá	vypadat	k5eAaPmIp3nS	vypadat
"	"	kIx"	"
<g/>
hlas	hlas	k1gInSc1	hlas
metra	metro	k1gNnSc2	metro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Namlouvá	namlouvat	k5eAaImIp3nS	namlouvat
nové	nový	k2eAgFnPc4d1	nová
stanice	stanice	k1gFnPc4	stanice
–	–	k?	–
fotografie	fotografia	k1gFnPc4	fotografia
a	a	k8xC	a
video	video	k1gNnSc4	video
na	na	k7c4	na
IDNES	IDNES	kA	IDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
