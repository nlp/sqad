<p>
<s>
Pchu	Pchu	k6eAd1	Pchu
I	i	k9	i
(	(	kIx(	(
<g/>
také	také	k9	také
Pchu-i	Pchu	k1gNnSc2	Pchu-i
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
<g/>
:	:	kIx,	:
愛	愛	k?	愛
<g/>
·	·	k?	·
<g/>
溥	溥	k?	溥
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gInSc1	pinyin
<g/>
:	:	kIx,	:
À	À	k?	À
Juéluó	Juéluó	k1gFnSc1	Juéluó
Pǔ	Pǔ	k1gFnSc1	Pǔ
Yí	Yí	k1gFnSc1	Yí
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Aj-sin	Ajin	k2eAgMnSc1d1	Aj-sin
Ťüe-luo	Ťüeuo	k1gMnSc1	Ťüe-luo
Pchu	Pchus	k1gInSc2	Pchus
I	I	kA	I
<g/>
;	;	kIx,	;
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc1	Peking
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc1	Peking
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
mandžuského	mandžuský	k2eAgInSc2d1	mandžuský
rodu	rod	k1gInSc2	rod
Aisin	Aisin	k1gMnSc1	Aisin
Gioro	Gioro	k1gNnSc1	Gioro
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgMnSc7d1	poslední
čínským	čínský	k2eAgMnSc7d1	čínský
císařem	císař	k1gMnSc7	císař
mandžuské	mandžuský	k2eAgFnSc2d1	mandžuská
dynastie	dynastie	k1gFnSc2	dynastie
Čching	Čching	k1gInSc1	Čching
a	a	k8xC	a
posledním	poslední	k2eAgMnSc7d1	poslední
čínským	čínský	k2eAgMnSc7d1	čínský
císařem	císař	k1gMnSc7	císař
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
formální	formální	k2eAgFnSc7d1	formální
hlavou	hlava	k1gFnSc7	hlava
japonského	japonský	k2eAgInSc2d1	japonský
loutkového	loutkový	k2eAgInSc2d1	loutkový
státu	stát	k1gInSc2	stát
Mančukuo	Mančukuo	k1gMnSc1	Mančukuo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
čínský	čínský	k2eAgMnSc1d1	čínský
císař	císař	k1gMnSc1	císař
vládl	vládnout	k5eAaImAgMnS	vládnout
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Süan-tchung	Süanchunga	k1gFnPc2	Süan-tchunga
(	(	kIx(	(
<g/>
Xuantong	Xuantong	k1gInSc1	Xuantong
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
natočen	natočit	k5eAaBmNgMnS	natočit
Bernardem	Bernard	k1gMnSc7	Bernard
Bertoluccim	Bertoluccim	k1gMnSc1	Bertoluccim
životopisný	životopisný	k2eAgInSc4d1	životopisný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
Poslední	poslední	k2eAgMnSc1d1	poslední
císař	císař	k1gMnSc1	císař
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
celkem	celek	k1gInSc7	celek
9	[number]	k4	9
oscarů	oscar	k1gInPc2	oscar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
jako	jako	k8xC	jako
syn	syn	k1gMnSc1	syn
prince	princ	k1gMnSc2	princ
Čchuna	Čchuna	k1gFnSc1	Čchuna
(	(	kIx(	(
<g/>
醇	醇	k?	醇
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Caj-feng	Cajeng	k1gMnSc1	Caj-feng
载	载	k?	载
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
necelých	celý	k2eNgNnPc6d1	necelé
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
faktické	faktický	k2eAgFnSc2d1	faktická
vládkyně	vládkyně	k1gFnSc2	vládkyně
Číny	Čína	k1gFnSc2	Čína
–	–	k?	–
císařovny	císařovna	k1gFnSc2	císařovna
vdovy	vdova	k1gFnSc2	vdova
Cch	Cch	k1gFnSc2	Cch
<g/>
'	'	kIx"	'
<g/>
-si	i	k?	-si
(	(	kIx(	(
<g/>
慈	慈	k?	慈
<g/>
;	;	kIx,	;
vládla	vládnout	k5eAaImAgFnS	vládnout
v	v	k7c6	v
letech	let	k1gInPc6	let
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Korunován	korunován	k2eAgInSc1d1	korunován
císařem	císař	k1gMnSc7	císař
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1908	[number]	k4	1908
v	v	k7c6	v
Síni	síň	k1gFnSc6	síň
Nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
harmonie	harmonie	k1gFnSc2	harmonie
v	v	k7c6	v
Zakázaném	zakázaný	k2eAgNnSc6d1	zakázané
městě	město	k1gNnSc6	město
<g/>
;	;	kIx,	;
půdu	půda	k1gFnSc4	půda
Zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
města	město	k1gNnSc2	město
pak	pak	k6eAd1	pak
až	až	k6eAd1	až
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
18	[number]	k4	18
let	léto	k1gNnPc2	léto
nikdy	nikdy	k6eAd1	nikdy
neopustil	opustit	k5eNaPmAgInS	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Slabá	slabý	k2eAgFnSc1d1	slabá
regentská	regentský	k2eAgFnSc1d1	regentská
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
dětský	dětský	k2eAgMnSc1d1	dětský
císař	císař	k1gMnSc1	císař
ale	ale	k8xC	ale
posléze	posléze	k6eAd1	posléze
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
zabránit	zabránit	k5eAaPmF	zabránit
rozbíhajícímu	rozbíhající	k2eAgInSc3d1	rozbíhající
se	se	k3xPyFc4	se
čínskému	čínský	k2eAgNnSc3d1	čínské
revolučnímu	revoluční	k2eAgNnSc3d1	revoluční
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Sinchajské	Sinchajský	k2eAgFnSc2d1	Sinchajský
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
辛	辛	k?	辛
<g/>
)	)	kIx)	)
a	a	k8xC	a
zradě	zrada	k1gFnSc6	zrada
Jüan	jüan	k1gInSc1	jüan
Š	Š	kA	Š
<g/>
'	'	kIx"	'
<g/>
-kchaje	chat	k5eAaImSgMnS	-kchat
(	(	kIx(	(
<g/>
袁	袁	k?	袁
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Pchu	Pcha	k1gFnSc4	Pcha
I	i	k9	i
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1912	[number]	k4	1912
sesazen	sesadit	k5eAaPmNgMnS	sesadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
vojsk	vojsko	k1gNnPc2	vojsko
prezidenta	prezident	k1gMnSc2	prezident
Li	li	k8xS	li
Jüan-chunga	Jüanhung	k1gMnSc2	Jüan-chung
generálem	generál	k1gMnSc7	generál
Čang	Čang	k1gMnSc1	Čang
Sünem	Sün	k1gMnSc7	Sün
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
podruhé	podruhé	k6eAd1	podruhé
korunován	korunovat	k5eAaBmNgMnS	korunovat
čínským	čínský	k2eAgMnSc7d1	čínský
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc1	tento
císařství	císařství	k1gNnSc1	císařství
nepřežilo	přežít	k5eNaPmAgNnS	přežít
ani	ani	k8xC	ani
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opuštění	opuštění	k1gNnSc2	opuštění
Zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
ránou	rána	k1gFnSc7	rána
v	v	k7c6	v
Pchu	Pchum	k1gNnSc6	Pchum
Iho	Iho	k1gFnSc1	Iho
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
smrt	smrt	k1gFnSc1	smrt
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
když	když	k8xS	když
podstoupila	podstoupit	k5eAaPmAgFnS	podstoupit
opiovou	opiový	k2eAgFnSc4d1	opiová
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Zakázaném	zakázaný	k2eAgNnSc6d1	zakázané
městě	město	k1gNnSc6	město
směl	smět	k5eAaImAgMnS	smět
žít	žít	k5eAaImF	žít
a	a	k8xC	a
pobírat	pobírat	k5eAaImF	pobírat
vládní	vládní	k2eAgFnSc4d1	vládní
penzi	penze	k1gFnSc4	penze
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
jüanů	jüan	k1gInPc2	jüan
ročně	ročně	k6eAd1	ročně
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
nato	nato	k6eAd1	nato
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
jako	jako	k8xS	jako
Palácové	palácový	k2eAgNnSc1d1	palácové
muzeum	muzeum	k1gNnSc1	muzeum
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
ze	z	k7c2	z
Zakázaného	zakázaný	k2eAgNnSc2d1	zakázané
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
řešil	řešit	k5eAaImAgInS	řešit
prodejem	prodej	k1gInSc7	prodej
cenných	cenný	k2eAgInPc2d1	cenný
sbírkových	sbírkový	k2eAgInPc2d1	sbírkový
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
císařovnou	císařovna	k1gFnSc7	císařovna
Wan	Wan	k1gMnSc2	Wan
Žung	Žunga	k1gFnPc2	Žunga
(	(	kIx(	(
<g/>
婉	婉	k?	婉
<g/>
)	)	kIx)	)
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
ženou	žena	k1gFnSc7	žena
Wen	Wen	k1gFnSc2	Wen
Siou	Sious	k1gInSc2	Sious
(	(	kIx(	(
<g/>
文	文	k?	文
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
japonské	japonský	k2eAgFnSc6d1	japonská
koncesi	koncese	k1gFnSc6	koncese
v	v	k7c6	v
Tchien-ťinu	Tchien-ťin	k1gInSc6	Tchien-ťin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Císař	Císař	k1gMnSc1	Císař
Mančukuo	Mančukuo	k1gMnSc1	Mančukuo
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
japonské	japonský	k2eAgFnSc6d1	japonská
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Mandžuska	Mandžusko	k1gNnSc2	Mandžusko
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc2	vytvoření
loutkového	loutkový	k2eAgInSc2d1	loutkový
státu	stát	k1gInSc2	stát
Mančukuo	Mančukuo	k6eAd1	Mančukuo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
formální	formální	k2eAgFnSc7d1	formální
hlavou	hlava	k1gFnSc7	hlava
tamního	tamní	k2eAgInSc2d1	tamní
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
zajat	zajmout	k5eAaPmNgInS	zajmout
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
deportován	deportován	k2eAgMnSc1d1	deportován
do	do	k7c2	do
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
válečný	válečný	k2eAgMnSc1d1	válečný
zločinec	zločinec	k1gMnSc1	zločinec
pak	pak	k6eAd1	pak
stanul	stanout	k5eAaPmAgMnS	stanout
také	také	k9	také
před	před	k7c7	před
Tokijským	tokijský	k2eAgInSc7d1	tokijský
tribunálem	tribunál	k1gInSc7	tribunál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
9	[number]	k4	9
let	léto	k1gNnPc2	léto
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
nápravném	nápravný	k2eAgNnSc6d1	nápravné
zařízení	zařízení	k1gNnSc6	zařízení
ve	v	k7c6	v
Fu-šunu	Fu-šun	k1gInSc6	Fu-šun
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Liao-ning	Liaoing	k1gInSc1	Liao-ning
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
vězeň	vězeň	k1gMnSc1	vězeň
číslo	číslo	k1gNnSc4	číslo
981	[number]	k4	981
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
jeho	jeho	k3xOp3gFnSc1	jeho
autobiografie	autobiografie	k1gFnSc1	autobiografie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xC	jako
zahradník	zahradník	k1gMnSc1	zahradník
v	v	k7c6	v
Pekingské	pekingský	k2eAgFnSc6d1	Pekingská
botanické	botanický	k2eAgFnSc6d1	botanická
zahradě	zahrada	k1gFnSc6	zahrada
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
archivář	archivář	k1gMnSc1	archivář
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
61	[number]	k4	61
let	léto	k1gNnPc2	léto
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
obyčejném	obyčejný	k2eAgInSc6d1	obyčejný
hřbitově	hřbitov	k1gInSc6	hřbitov
jako	jako	k8xC	jako
řadový	řadový	k2eAgMnSc1d1	řadový
občan	občan	k1gMnSc1	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vdova	vdova	k1gFnSc1	vdova
po	po	k7c4	po
Pchuim	Pchuim	k1gInSc4	Pchuim
vykoupila	vykoupit	k5eAaPmAgFnS	vykoupit
urnu	urna	k1gFnSc4	urna
s	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
ostatky	ostatek	k1gInPc7	ostatek
a	a	k8xC	a
nechala	nechat	k5eAaPmAgFnS	nechat
mu	on	k3xPp3gMnSc3	on
postavit	postavit	k5eAaPmF	postavit
hrobku	hrobka	k1gFnSc4	hrobka
u	u	k7c2	u
areálu	areál	k1gInSc2	areál
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
čínských	čínský	k2eAgMnPc2d1	čínský
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
4	[number]	k4	4
z	z	k7c2	z
9	[number]	k4	9
přecházejích	přecházej	k1gInPc6	přecházej
císařů	císař	k1gMnPc2	císař
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
,	,	kIx,	,
dokončena	dokončit	k5eAaPmNgFnS	dokončit
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
byla	být	k5eAaImAgFnS	být
uložena	uložen	k2eAgFnSc1d1	uložena
urna	urna	k1gFnSc1	urna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Flemmer	Flemmer	k1gMnSc1	Flemmer
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Fraus	fraus	k1gFnSc1	fraus
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7238-497-6	[number]	k4	978-80-7238-497-6
</s>
</p>
<p>
<s>
Velasco	Velasco	k1gMnSc1	Velasco
<g/>
,	,	kIx,	,
Juan	Juan	k1gMnSc1	Juan
–	–	k?	–
Vessels	Vessels	k1gInSc1	Vessels
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
National	National	k1gMnSc1	National
Geographic	Geographic	k1gMnSc1	Geographic
Česko	Česko	k1gNnSc1	Česko
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
příloha	příloha	k1gFnSc1	příloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aisin	Aisin	k1gMnSc1	Aisin
Gioro	Gioro	k1gNnSc1	Gioro
Pchu-	Pchu-	k1gMnSc1	Pchu-
<g/>
i.	i.	k?	i.
Byl	být	k5eAaImAgMnS	být
jsem	být	k5eAaImIp1nS	být
posledním	poslední	k2eAgMnSc7d1	poslední
císařem	císař	k1gMnSc7	císař
čínským	čínský	k2eAgMnSc7d1	čínský
<g/>
.	.	kIx.	.
</s>
<s>
Přel	přít	k5eAaImAgMnS	přít
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Hála	Hála	k1gMnSc1	Hála
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7038-082-9	[number]	k4	80-7038-082-9
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
čínských	čínský	k2eAgMnPc2d1	čínský
císařů	císař	k1gMnPc2	císař
</s>
</p>
<p>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Čching	Čching	k1gInSc1	Čching
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Číny	Čína	k1gFnSc2	Čína
</s>
</p>
<p>
<s>
Čínské	čínský	k2eAgNnSc1d1	čínské
císařství	císařství	k1gNnSc1	císařství
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pchu	Pchus	k1gInSc2	Pchus
I	i	k9	i
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Pchu	Pchus	k1gInSc2	Pchus
I	i	k9	i
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
