<s>
Pchu	Pchu	k6eAd1	Pchu
I	i	k9	i
(	(	kIx(	(
<g/>
také	také	k9	také
Pchu-i	Pchu	k1gNnSc2	Pchu-i
<g/>
;	;	kIx,	;
čínsky	čínsky	k6eAd1	čínsky
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
<g/>
:	:	kIx,	:
愛	愛	k?	愛
<g/>
·	·	k?	·
<g/>
溥	溥	k?	溥
<g/>
,	,	kIx,	,
pinyin	pinyin	k1gInSc1	pinyin
<g/>
:	:	kIx,	:
À	À	k?	À
Juéluó	Juéluó	k1gFnSc1	Juéluó
Pǔ	Pǔ	k1gFnSc1	Pǔ
Yí	Yí	k1gFnSc1	Yí
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
přepis	přepis	k1gInSc1	přepis
<g/>
:	:	kIx,	:
Aj-sin	Ajin	k2eAgMnSc1d1	Aj-sin
Ťüe-luo	Ťüeuo	k1gMnSc1	Ťüe-luo
Pchu	Pchus	k1gInSc2	Pchus
I	I	kA	I
<g/>
;	;	kIx,	;
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc1	Peking
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Peking	Peking	k1gInSc1	Peking
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
mandžuského	mandžuský	k2eAgInSc2d1	mandžuský
rodu	rod	k1gInSc2	rod
Aisin	Aisin	k1gMnSc1	Aisin
Gioro	Gioro	k1gNnSc1	Gioro
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
posledním	poslední	k2eAgMnSc7d1	poslední
čínským	čínský	k2eAgMnSc7d1	čínský
císařem	císař	k1gMnSc7	císař
mandžuské	mandžuský	k2eAgFnSc2d1	mandžuská
dynastie	dynastie	k1gFnSc2	dynastie
Čching	Čching	k1gInSc1	Čching
a	a	k8xC	a
posledním	poslední	k2eAgMnSc7d1	poslední
čínským	čínský	k2eAgMnSc7d1	čínský
císařem	císař	k1gMnSc7	císař
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
