<s>
František	František	k1gMnSc1
Ježek	Ježek	k1gMnSc1
(	(	kIx(
<g/>
národní	národní	k2eAgMnSc1d1
socialista	socialista	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Ježek	Ježek	k1gMnSc1
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ČSR	ČSR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1923	#num#	k4
–	–	k?
1925	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
nár	nár	k?
<g/>
.	.	kIx.
soc	soc	kA
<g/>
.	.	kIx.
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1884	#num#	k4
SoběslavRakousko-Uhersko	SoběslavRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uherska	k1gMnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1925	#num#	k4
SoběslavČeskoslovensko	SoběslavČeskoslovensko	k1gNnSc4
Československo	Československo	k1gNnSc4
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
František	František	k1gMnSc1
Ježek	Ježek	k1gMnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1884	#num#	k4
Soběslav	Soběslav	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1925	#num#	k4
Soběslav	Soběslav	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
československý	československý	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
poslanec	poslanec	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
za	za	k7c4
Československou	československý	k2eAgFnSc4d1
socialistickou	socialistický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
(	(	kIx(
<g/>
pozdější	pozdní	k2eAgMnSc1d2
národní	národní	k2eAgMnSc1d1
socialisté	socialist	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
nemanželský	manželský	k2eNgMnSc1d1
syn	syn	k1gMnSc1
služebné	služebný	k2eAgFnSc2d1
Marie	Maria	k1gFnSc2
Ježkové	Ježkové	k2eAgFnSc2d1
v	v	k7c6
Soběslavi	Soběslav	k1gFnSc6
v	v	k7c6
domě	dům	k1gInSc6
čp.	čp.	k?
141	#num#	k4
na	na	k7c6
Táborském	táborský	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pokřtěn	pokřtít	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
katolík	katolík	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1907	#num#	k4
oznámilo	oznámit	k5eAaPmAgNnS
c.	c.	k?
k.	k.	k?
okresní	okresní	k2eAgNnSc4d1
hejtmanství	hejtmanství	k1gNnSc4
na	na	k7c6
Královských	královský	k2eAgInPc6d1
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
že	že	k8xS
vystoupil	vystoupit	k5eAaPmAgMnS
z	z	k7c2
církve	církev	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Osm	osm	k4xCc4
let	léto	k1gNnPc2
pobýval	pobývat	k5eAaImAgMnS
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
organizování	organizování	k1gNnSc4
národně	národně	k6eAd1
sociálního	sociální	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
mezi	mezi	k7c7
tamní	tamní	k2eAgFnSc7d1
českou	český	k2eAgFnSc7d1
menšinou	menšina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Působil	působit	k5eAaImAgMnS
za	za	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
v	v	k7c6
antimilitaristickém	antimilitaristický	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
to	ten	k3xDgNnSc4
byl	být	k5eAaImAgMnS
dvakrát	dvakrát	k6eAd1
vězněn	věznit	k5eAaImNgMnS
(	(	kIx(
<g/>
odseděl	odsedět	k5eAaPmAgMnS
si	se	k3xPyFc3
17	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1908	#num#	k4
byl	být	k5eAaImAgMnS
úředníkem	úředník	k1gMnSc7
okresní	okresní	k2eAgFnSc2d1
nemocenské	mocenský	k2eNgFnSc2d1,k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
v	v	k7c6
Rakovníku	Rakovník	k1gInSc6
a	a	k8xC
vydával	vydávat	k5eAaImAgInS,k5eAaPmAgInS
v	v	k7c6
tomto	tento	k3xDgNnSc6
městě	město	k1gNnSc6
list	list	k1gInSc4
Rakovnický	rakovnický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
získal	získat	k5eAaPmAgMnS
poslanecké	poslanecký	k2eAgNnSc4d1
křeslo	křeslo	k1gNnSc4
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
za	za	k7c4
Československou	československý	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
socialistickou	socialistický	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Mandát	mandát	k1gInSc1
ale	ale	k8xC
získal	získat	k5eAaPmAgInS
až	až	k6eAd1
dodatečně	dodatečně	k6eAd1
roku	rok	k1gInSc2
1923	#num#	k4
jako	jako	k8xC,k8xS
náhradník	náhradník	k1gMnSc1
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
rezignoval	rezignovat	k5eAaBmAgMnS
poslanec	poslanec	k1gMnSc1
Václav	Václav	k1gMnSc1
Draxl	Draxl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
Ježek	Ježek	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
obsadil	obsadit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnSc4
křeslo	křeslo	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
poslanec	poslanec	k1gMnSc1
František	František	k1gMnSc1
Vápeník	Vápeník	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
k	k	k7c3
roku	rok	k1gInSc3
1923	#num#	k4
byl	být	k5eAaImAgInS
profesí	profes	k1gFnSc7
úředníkem	úředník	k1gMnSc7
v	v	k7c6
Rakovníku	Rakovník	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
smrtí	smrt	k1gFnSc7
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
městské	městský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
okresní	okresní	k2eAgFnSc2d1
správní	správní	k2eAgFnSc2d1
komise	komise	k1gFnSc2
v	v	k7c6
Rakovníku	Rakovník	k1gInSc6
a	a	k8xC
členem	člen	k1gMnSc7
vedení	vedení	k1gNnSc2
rakovnické	rakovnický	k2eAgFnSc2d1
spořitelny	spořitelna	k1gFnSc2
i	i	k8xC
konzumního	konzumní	k2eAgNnSc2d1
družstva	družstvo	k1gNnSc2
Zádruha	zádruha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
na	na	k7c4
plicní	plicní	k2eAgFnSc4d1
chorobu	choroba	k1gFnSc4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
červnu	červen	k1gInSc6
1925	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Poslanec	poslanec	k1gMnSc1
Ježek	Ježek	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červen	červen	k1gInSc1
1925	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
65	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
172	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Matrika	matrika	k1gFnSc1
narozených	narozený	k2eAgFnPc2d1
Soběslav	Soběslava	k1gFnPc2
<g/>
,	,	kIx,
1866	#num#	k4
<g/>
–	–	k?
<g/>
1886	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
378.1	378.1	k4
2	#num#	k4
348	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
František	František	k1gMnSc1
Ježek	Ježek	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
215	#num#	k4
<g/>
.	.	kIx.
schůze	schůze	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
k	k	k7c3
tématu	téma	k1gNnSc3
František	František	k1gMnSc1
Ježek	Ježek	k1gMnSc1
na	na	k7c4
Obalkyknih	Obalkyknih	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Slib	slib	k1gInSc1
Františka	František	k1gMnSc2
Ježka	Ježek	k1gMnSc2
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
roku	rok	k1gInSc2
1923	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
208239	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
884148122888295200007	#num#	k4
</s>
