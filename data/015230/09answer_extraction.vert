Byl	být	k5eAaImAgMnS
pokřtěn	pokřtít	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
katolík	katolík	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1907	#num#	k4
oznámilo	oznámit	k5eAaPmAgNnS
c.	c.	k?
k.	k.	k?
okresní	okresní	k2eAgNnSc4d1
hejtmanství	hejtmanství	k1gNnSc4
na	na	k7c6
Královských	královský	k2eAgInPc6d1
Vinohradech	Vinohrady	k1gInPc6
<g/>
,	,	kIx,
že	že	k8xS
vystoupil	vystoupit	k5eAaPmAgMnS
z	z	k7c2
církve	církev	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
.	.	kIx.
