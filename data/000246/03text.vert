<s>
XXVI	XXVI	kA	XXVI
<g/>
.	.	kIx.	.
letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
se	se	k3xPyFc4	se
uskutečnily	uskutečnit	k5eAaPmAgInP	uskutečnit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
až	až	k9	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
10281	[number]	k4	10281
sportovců	sportovec	k1gMnPc2	sportovec
ze	z	k7c2	z
197	[number]	k4	197
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Soutěžilo	soutěžit	k5eAaImAgNnS	soutěžit
se	se	k3xPyFc4	se
v	v	k7c6	v
271	[number]	k4	271
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volby	volba	k1gFnSc2	volba
pořadatelského	pořadatelský	k2eAgNnSc2d1	pořadatelské
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
by	by	kYmCp3nS	by
čest	čest	k1gFnSc1	čest
hostit	hostit	k5eAaImF	hostit
letní	letní	k2eAgFnSc4d1	letní
olympiádu	olympiáda	k1gFnSc4	olympiáda
neměly	mít	k5eNaImAgFnP	mít
mít	mít	k5eAaImF	mít
Athény	Athéna	k1gFnPc4	Athéna
(	(	kIx(	(
<g/>
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
stého	stý	k4xOgInSc2	stý
výročí	výročí	k1gNnSc4	výročí
prvních	první	k4xOgFnPc2	první
novodobých	novodobý	k2eAgFnPc2d1	novodobá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pro	pro	k7c4	pro
Atlantu	Atlanta	k1gFnSc4	Atlanta
<g/>
,	,	kIx,	,
americké	americký	k2eAgNnSc4d1	americké
město	město	k1gNnSc4	město
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Georgia	Georgium	k1gNnSc2	Georgium
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
tradiční	tradiční	k2eAgFnSc6d1	tradiční
formulkou	formulka	k1gFnSc7	formulka
otevřel	otevřít	k5eAaPmAgMnS	otevřít
prezident	prezident	k1gMnSc1	prezident
Bill	Bill	k1gMnSc1	Bill
Clinton	Clinton	k1gMnSc1	Clinton
a	a	k8xC	a
olympijský	olympijský	k2eAgInSc1d1	olympijský
oheň	oheň	k1gInSc1	oheň
zapálil	zapálit	k5eAaPmAgMnS	zapálit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
americký	americký	k2eAgMnSc1d1	americký
boxer	boxer	k1gMnSc1	boxer
Muhammad	Muhammad	k1gInSc4	Muhammad
Ali	Ali	k1gFnSc2	Ali
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
postižený	postižený	k2eAgMnSc1d1	postižený
Parkinsonovou	Parkinsonový	k2eAgFnSc7d1	Parkinsonová
chorobou	choroba	k1gFnSc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
olympiády	olympiáda	k1gFnSc2	olympiáda
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
teroristickým	teroristický	k2eAgInSc7d1	teroristický
útokem	útok	k1gInSc7	útok
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
výbuch	výbuch	k1gInSc1	výbuch
bomby	bomba	k1gFnSc2	bomba
v	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
parku	park	k1gInSc6	park
zabil	zabít	k5eAaPmAgMnS	zabít
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
110	[number]	k4	110
lidí	člověk	k1gMnPc2	člověk
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
visely	viset	k5eAaImAgFnP	viset
na	na	k7c4	na
půl	půl	k1xP	půl
žerdi	žerď	k1gFnSc2	žerď
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
soutěže	soutěž	k1gFnPc1	soutěž
nebyly	být	k5eNaImAgFnP	být
přerušeny	přerušit	k5eAaPmNgFnP	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepším	dobrý	k2eAgInSc7d3	nejlepší
výkonem	výkon	k1gInSc7	výkon
celých	celý	k2eAgFnPc2d1	celá
her	hra	k1gFnPc2	hra
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
stala	stát	k5eAaPmAgFnS	stát
fenomenální	fenomenální	k2eAgFnSc1d1	fenomenální
dvoustovka	dvoustovka	k1gFnSc1	dvoustovka
sprintera	sprinter	k1gMnSc2	sprinter
Michaela	Michael	k1gMnSc2	Michael
Johnsona	Johnson	k1gMnSc2	Johnson
v	v	k7c6	v
čase	čas	k1gInSc6	čas
nového	nový	k2eAgInSc2d1	nový
světového	světový	k2eAgInSc2d1	světový
rekordu	rekord	k1gInSc2	rekord
19,32	[number]	k4	19,32
s.	s.	k?	s.
Ten	ten	k3xDgInSc4	ten
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
2	[number]	k4	2
setiny	setina	k1gFnSc2	setina
sekundy	sekunda	k1gFnSc2	sekunda
(	(	kIx(	(
<g/>
19,30	[number]	k4	19,30
s.	s.	k?	s.
<g/>
)	)	kIx)	)
překonán	překonat	k5eAaPmNgInS	překonat
až	až	k9	až
o	o	k7c4	o
12	[number]	k4	12
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
jamajským	jamajský	k2eAgMnSc7d1	jamajský
sprinterem	sprinter	k1gMnSc7	sprinter
Usainem	Usain	k1gMnSc7	Usain
Boltem	Bolt	k1gMnSc7	Bolt
na	na	k7c6	na
OH	OH	kA	OH
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
<g/>
.	.	kIx.	.
</s>
<s>
Her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
sportovci	sportovec	k1gMnPc1	sportovec
z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
197	[number]	k4	197
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
24	[number]	k4	24
účastnilo	účastnit	k5eAaImAgNnS	účastnit
letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
poprvé	poprvé	k6eAd1	poprvé
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
11	[number]	k4	11
nástupnických	nástupnický	k2eAgFnPc2d1	nástupnická
zemí	zem	k1gFnPc2	zem
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
SSSR	SSSR	kA	SSSR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
závorkách	závorka	k1gFnPc6	závorka
udávají	udávat	k5eAaImIp3nP	udávat
počty	počet	k1gInPc1	počet
sportovců	sportovec	k1gMnPc2	sportovec
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Česko	Česko	k1gNnSc4	Česko
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Letní	letní	k2eAgFnSc2d1	letní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
1996	[number]	k4	1996
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.olympic.cz	www.olympic.cz	k1gInSc4	www.olympic.cz
</s>
