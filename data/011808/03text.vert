<p>
<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
di	di	k?	di
Mariano	Mariana	k1gFnSc5	Mariana
Filipepi	Filipep	k1gFnSc5	Filipep
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xC	jako
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticell	k1gMnSc6	Botticell
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1444	[number]	k4	1444
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1510	[number]	k4	1510
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
renesanční	renesanční	k2eAgMnSc1d1	renesanční
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
florentské	florentský	k2eAgFnSc2d1	florentská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Datum	datum	k1gNnSc1	datum
ani	ani	k8xC	ani
rok	rok	k1gInSc1	rok
narození	narození	k1gNnSc2	narození
Botticelliho	Botticelli	k1gMnSc2	Botticelli
nejsou	být	k5eNaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
roky	rok	k1gInPc4	rok
1444	[number]	k4	1444
a	a	k8xC	a
1445	[number]	k4	1445
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
synem	syn	k1gMnSc7	syn
koželuha	koželuh	k1gMnSc2	koželuh
Mariana	Marian	k1gMnSc2	Marian
di	di	k?	di
Vanni	Vanň	k1gMnSc3	Vanň
Filipepi	Filipep	k1gMnSc3	Filipep
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgMnS	usadit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
Smeraldou	Smeralda	k1gFnSc7	Smeralda
a	a	k8xC	a
šesti	šest	k4xCc7	šest
dětmi	dítě	k1gFnPc7	dítě
nedaleko	nedaleko	k7c2	nedaleko
kostela	kostel	k1gInSc2	kostel
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Ognissanti	Ognissant	k1gMnPc1	Ognissant
(	(	kIx(	(
<g/>
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
starších	starý	k2eAgMnPc2d2	starší
bratrů	bratr	k1gMnPc2	bratr
jménem	jméno	k1gNnSc7	jméno
Giovanni	Giovanň	k1gMnSc6	Giovanň
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
finančníkem	finančník	k1gMnSc7	finančník
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
přezdívku	přezdívka	k1gFnSc4	přezdívka
Botticelli	Botticelle	k1gFnSc4	Botticelle
(	(	kIx(	(
<g/>
Soudek	soudek	k1gInSc1	soudek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
Sandra	Sandra	k1gFnSc1	Sandra
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
měl	mít	k5eAaImAgInS	mít
osm	osm	k4xCc4	osm
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
dospělosti	dospělost	k1gFnSc3	dospělost
dožili	dožít	k5eAaPmAgMnP	dožít
jen	jen	k6eAd1	jen
čtyři	čtyři	k4xCgMnPc1	čtyři
<g/>
,	,	kIx,	,
Sandro	Sandra	k1gFnSc5	Sandra
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1458	[number]	k4	1458
si	se	k3xPyFc3	se
rodina	rodina	k1gFnSc1	rodina
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
dům	dům	k1gInSc4	dům
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Via	via	k7c4	via
della	dello	k1gNnPc4	dello
Vigna	Vign	k1gMnSc2	Vign
Nuova	Nuov	k1gMnSc2	Nuov
a	a	k8xC	a
Vanni	Vann	k1gMnPc1	Vann
Filipepi	Filipep	k1gFnSc2	Filipep
zároveň	zároveň	k6eAd1	zároveň
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
domek	domek	k1gInSc4	domek
v	v	k7c6	v
Careggi	Caregg	k1gInSc6	Caregg
severně	severně	k6eAd1	severně
od	od	k7c2	od
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
umělecká	umělecký	k2eAgFnSc1d1	umělecká
dráha	dráha	k1gFnSc1	dráha
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1459	[number]	k4	1459
a	a	k8xC	a
1460	[number]	k4	1460
ve	v	k7c6	v
zlatnické	zlatnický	k2eAgFnSc6d1	Zlatnická
dílně	dílna	k1gFnSc6	dílna
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
u	u	k7c2	u
bratra	bratr	k1gMnSc2	bratr
Antonia	Antonio	k1gMnSc2	Antonio
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1462	[number]	k4	1462
byl	být	k5eAaImAgInS	být
rodinou	rodina	k1gFnSc7	rodina
svěřen	svěřit	k5eAaPmNgInS	svěřit
do	do	k7c2	do
péče	péče	k1gFnSc2	péče
Filippa	Filipp	k1gMnSc2	Filipp
Lippiho	Lippi	k1gMnSc2	Lippi
<g/>
,	,	kIx,	,
uznávaného	uznávaný	k2eAgMnSc2d1	uznávaný
malíře	malíř	k1gMnSc2	malíř
a	a	k8xC	a
zběhlého	zběhlý	k2eAgMnSc2d1	zběhlý
mnicha	mnich	k1gMnSc2	mnich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Pratu	Prat	k1gInSc6	Prat
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
Lippiho	Lippi	k1gMnSc4	Lippi
vedením	vedení	k1gNnSc7	vedení
začal	začít	k5eAaPmAgInS	začít
Botticelli	Botticelle	k1gFnSc4	Botticelle
malovat	malovat	k5eAaImF	malovat
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
obrazy	obraz	k1gInPc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1467	[number]	k4	1467
odešel	odejít	k5eAaPmAgMnS	odejít
Lippi	Lippe	k1gFnSc4	Lippe
do	do	k7c2	do
Spoleta	Spoleta	k1gFnSc1	Spoleta
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
Botticelliho	Botticelliha	k1gFnSc5	Botticelliha
učňovská	učňovský	k2eAgNnPc4d1	učňovské
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
jeho	jeho	k3xOp3gMnPc6	jeho
dílně	dílna	k1gFnSc6	dílna
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1464	[number]	k4	1464
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
Botticelliho	Botticelli	k1gMnSc2	Botticelli
otec	otec	k1gMnSc1	otec
na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
Via	via	k7c4	via
della	dell	k1gMnSc4	dell
Porcellana	Porcellan	k1gMnSc4	Porcellan
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
Botticelli	Botticell	k1gMnPc1	Botticell
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zřídil	zřídit	k5eAaPmAgInS	zřídit
ateliér	ateliér	k1gInSc1	ateliér
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
dílnu	dílna	k1gFnSc4	dílna
a	a	k8xC	a
realizoval	realizovat	k5eAaBmAgMnS	realizovat
první	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
zakázku	zakázka	k1gFnSc4	zakázka
<g/>
,	,	kIx,	,
alegorii	alegorie	k1gFnSc4	alegorie
Fortitudo	Fortitudo	k1gNnSc1	Fortitudo
(	(	kIx(	(
<g/>
Statečnost	statečnost	k1gFnSc1	statečnost
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
florentskou	florentský	k2eAgFnSc4d1	florentská
soudní	soudní	k2eAgFnSc4d1	soudní
síň	síň	k1gFnSc4	síň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1472	[number]	k4	1472
byl	být	k5eAaImAgMnS	být
Botticelli	Botticelle	k1gFnSc4	Botticelle
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
místního	místní	k2eAgInSc2d1	místní
cechu	cech	k1gInSc2	cech
svatého	svatý	k2eAgMnSc2d1	svatý
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
,	,	kIx,	,
sdružujícího	sdružující	k2eAgMnSc2d1	sdružující
malíře	malíř	k1gMnSc2	malíř
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
umělec	umělec	k1gMnSc1	umělec
prožil	prožít	k5eAaPmAgMnS	prožít
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Botticelli	Botticell	k1gMnPc1	Botticell
ve	v	k7c6	v
městě	město	k1gNnSc6	město
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgInS	získávat
renomé	renomé	k1gNnSc4	renomé
<g/>
.	.	kIx.	.
</s>
<s>
Svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1473	[number]	k4	1473
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
Maggiore	Maggior	k1gInSc5	Maggior
během	během	k7c2	během
svátku	svátek	k1gInSc2	svátek
sv.	sv.	kA	sv.
Šebestiána	Šebestián	k1gMnSc2	Šebestián
vystaven	vystavit	k5eAaPmNgInS	vystavit
jeho	on	k3xPp3gInSc4	on
portrét	portrét	k1gInSc4	portrét
tohoto	tento	k3xDgMnSc2	tento
světce	světec	k1gMnSc2	světec
<g/>
,	,	kIx,	,
umístěný	umístěný	k2eAgInSc4d1	umístěný
dnes	dnes	k6eAd1	dnes
v	v	k7c4	v
Gemäldegalerie	Gemäldegalerie	k1gFnPc4	Gemäldegalerie
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Pisy	Pisa	k1gFnSc2	Pisa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgMnS	získat
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
fresky	freska	k1gFnPc4	freska
ve	v	k7c6	v
hřbitovní	hřbitovní	k2eAgFnSc6d1	hřbitovní
budově	budova	k1gFnSc6	budova
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
neznámého	známý	k2eNgInSc2d1	neznámý
důvodu	důvod	k1gInSc2	důvod
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
<g/>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
mezníkem	mezník	k1gInSc7	mezník
v	v	k7c6	v
Botticelliho	Botticelli	k1gMnSc2	Botticelli
životě	život	k1gInSc6	život
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
1475	[number]	k4	1475
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgMnS	získat
mecenáše	mecenáš	k1gMnPc4	mecenáš
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
nejmocnějšího	mocný	k2eAgInSc2d3	nejmocnější
florentského	florentský	k2eAgInSc2d1	florentský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
byli	být	k5eAaImAgMnP	být
Medicejové	Medicej	k1gMnPc1	Medicej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
pochází	pocházet	k5eAaImIp3nS	pocházet
Portrét	portrét	k1gInSc1	portrét
muže	muž	k1gMnSc2	muž
držícího	držící	k2eAgMnSc2d1	držící
medailon	medailon	k1gInSc4	medailon
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
spatřován	spatřován	k2eAgInSc1d1	spatřován
Cosimo	Cosima	k1gFnSc5	Cosima
ml.	ml.	kA	ml.
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
standarta	standarta	k1gFnSc1	standarta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
namaloval	namalovat	k5eAaPmAgMnS	namalovat
pro	pro	k7c4	pro
rytířský	rytířský	k2eAgInSc4d1	rytířský
turnaj	turnaj	k1gInSc4	turnaj
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Giuliana	Giulian	k1gMnSc2	Giulian
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Botticelli	Botticelle	k1gFnSc4	Botticelle
začal	začít	k5eAaPmAgMnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
i	i	k9	i
mimo	mimo	k7c4	mimo
Florencii	Florencie	k1gFnSc4	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Proslulost	proslulost	k1gFnSc1	proslulost
si	se	k3xPyFc3	se
získával	získávat	k5eAaImAgMnS	získávat
jako	jako	k8xS	jako
malíř	malíř	k1gMnSc1	malíř
náboženských	náboženský	k2eAgInPc2d1	náboženský
výjevů	výjev	k1gInPc2	výjev
i	i	k9	i
jako	jako	k9	jako
portrétista	portrétista	k1gMnSc1	portrétista
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
si	se	k3xPyFc3	se
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
římská	římský	k2eAgFnSc1d1	římská
pobočka	pobočka	k1gFnSc1	pobočka
florentské	florentský	k2eAgFnSc2d1	florentská
banky	banka	k1gFnSc2	banka
Antonio	Antonio	k1gMnSc1	Antonio
Salutati	Salutati	k1gFnSc1	Salutati
objednala	objednat	k5eAaPmAgFnS	objednat
tondo	tondo	k1gNnSc4	tondo
s	s	k7c7	s
Madonou	Madona	k1gFnSc7	Madona
<g/>
.	.	kIx.	.
</s>
<s>
Nebyli	být	k5eNaImAgMnP	být
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
jen	jen	k9	jen
Medicejští	Medicejský	k2eAgMnPc1d1	Medicejský
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
Botticelliho	Botticelli	k1gMnSc4	Botticelli
zaměstnávali	zaměstnávat	k5eAaImAgMnP	zaměstnávat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1480	[number]	k4	1480
ho	on	k3xPp3gMnSc2	on
rodina	rodina	k1gFnSc1	rodina
Vespucci	Vespucce	k1gFnSc4	Vespucce
pověřila	pověřit	k5eAaPmAgFnS	pověřit
vytvořením	vytvoření	k1gNnSc7	vytvoření
fresky	freska	k1gFnSc2	freska
sv.	sv.	kA	sv.
Augustina	Augustin	k1gMnSc2	Augustin
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
Všech	všecek	k3xTgMnPc2	všecek
Svatých	svatý	k1gMnPc2	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgMnSc1d3	nejstarší
Botticeliho	Botticeli	k1gMnSc4	Botticeli
dochovanou	dochovaný	k2eAgFnSc7d1	dochovaná
freskou	freska	k1gFnSc7	freska
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nS	tvořit
protějšek	protějšek	k1gInSc4	protějšek
Ghirlandaiově	Ghirlandaiův	k2eAgFnSc3d1	Ghirlandaiův
fresce	freska	k1gFnSc3	freska
svatého	svatý	k2eAgMnSc2d1	svatý
Jeronýma	Jeroným	k1gMnSc2	Jeroným
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1481	[number]	k4	1481
ho	on	k3xPp3gInSc4	on
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
významnými	významný	k2eAgMnPc7d1	významný
florentskými	florentský	k2eAgMnPc7d1	florentský
malíři	malíř	k1gMnPc7	malíř
Ghirlandaiem	Ghirlandaius	k1gMnSc7	Ghirlandaius
<g/>
,	,	kIx,	,
Peruginem	Perugino	k1gNnSc7	Perugino
a	a	k8xC	a
Rossellim	Rosselli	k1gNnSc7	Rosselli
povolal	povolat	k5eAaPmAgMnS	povolat
papež	papež	k1gMnSc1	papež
Sixtus	Sixtus	k1gMnSc1	Sixtus
IV	IV	kA	IV
<g/>
.	.	kIx.	.
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
výzdobě	výzdoba	k1gFnSc6	výzdoba
jím	jíst	k5eAaImIp1nS	jíst
postavené	postavený	k2eAgFnPc4d1	postavená
Sixtinské	sixtinský	k2eAgFnPc4d1	Sixtinská
kaple	kaple	k1gFnPc4	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Nástěnné	nástěnný	k2eAgFnPc1d1	nástěnná
malby	malba	k1gFnPc1	malba
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
dvěma	dva	k4xCgInPc7	dva
tematickými	tematický	k2eAgInPc7d1	tematický
celky	celek	k1gInPc7	celek
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgInSc1d1	symbolizující
Starý	starý	k2eAgInSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výjevy	výjev	k1gInPc4	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
namaloval	namalovat	k5eAaPmAgInS	namalovat
fresku	freska	k1gFnSc4	freska
Pokušení	pokušení	k1gNnSc2	pokušení
Krista	Kristus	k1gMnSc2	Kristus
<g/>
,	,	kIx,	,
do	do	k7c2	do
výjevů	výjev	k1gInPc2	výjev
ze	z	k7c2	z
života	život	k1gInSc2	život
Mojžíšova	Mojžíšův	k2eAgInSc2d1	Mojžíšův
pak	pak	k6eAd1	pak
Vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
Mojžíšovým	Mojžíšův	k2eAgInPc3d1	Mojžíšův
zákonům	zákon	k1gInPc3	zákon
a	a	k8xC	a
Scény	scéna	k1gFnPc4	scéna
z	z	k7c2	z
Mojžíšova	Mojžíšův	k2eAgInSc2d1	Mojžíšův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohřbu	pohřeb	k1gInSc2	pohřeb
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1482	[number]	k4	1482
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Botticelli	Botticelle	k1gFnSc4	Botticelle
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
až	až	k9	až
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
mu	on	k3xPp3gMnSc3	on
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
zadala	zadat	k5eAaPmAgFnS	zadat
výzdobu	výzdoba	k1gFnSc4	výzdoba
Sálu	sál	k1gInSc2	sál
lilií	lilie	k1gFnPc2	lilie
v	v	k7c6	v
Palazzo	Palazza	k1gFnSc5	Palazza
Vecchio	Vecchio	k1gNnSc1	Vecchio
<g/>
,	,	kIx,	,
žádná	žádný	k3yNgFnSc1	žádný
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
fresek	freska	k1gFnPc2	freska
se	se	k3xPyFc4	se
však	však	k9	však
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
Jaro	jaro	k1gNnSc1	jaro
(	(	kIx(	(
<g/>
Primavera	Primavera	k1gFnSc1	Primavera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
rovněž	rovněž	k9	rovněž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
Botticelliho	Botticelli	k1gMnSc4	Botticelli
návratu	návrat	k1gInSc2	návrat
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
námětem	námět	k1gInSc7	námět
malířovy	malířův	k2eAgFnSc2d1	malířova
tvorby	tvorba	k1gFnSc2	tvorba
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
si	se	k3xPyFc3	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
svatby	svatba	k1gFnSc2	svatba
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
objednal	objednat	k5eAaPmAgMnS	objednat
florentský	florentský	k2eAgMnSc1d1	florentský
kupec	kupec	k1gMnSc1	kupec
Antonio	Antonio	k1gMnSc1	Antonio
Pucci	Pucci	k?	Pucci
cyklus	cyklus	k1gInSc1	cyklus
čtyř	čtyři	k4xCgInPc2	čtyři
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
námětem	námět	k1gInSc7	námět
je	být	k5eAaImIp3nS	být
Příběh	příběh	k1gInSc1	příběh
rytíře	rytíř	k1gMnSc2	rytíř
Nastagia	Nastagius	k1gMnSc2	Nastagius
degli	degnout	k5eAaImAgMnP	degnout
Onesti	Onest	k1gMnPc1	Onest
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
ztvárněný	ztvárněný	k2eAgMnSc1d1	ztvárněný
v	v	k7c6	v
Dekameronu	Dekameron	k1gInSc6	Dekameron
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
obrazy	obraz	k1gInPc1	obraz
majetkem	majetek	k1gInSc7	majetek
madridského	madridský	k2eAgNnSc2d1	madridské
Prada	Prado	k1gNnSc2	Prado
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
dílech	dílo	k1gNnPc6	dílo
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
láska	láska	k1gFnSc1	láska
projevuje	projevovat	k5eAaImIp3nS	projevovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
její	její	k3xOp3gFnSc2	její
bohyně	bohyně	k1gFnSc2	bohyně
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
proslulém	proslulý	k2eAgNnSc6d1	proslulé
Zrození	zrození	k1gNnSc6	zrození
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
freskách	freska	k1gFnPc6	freska
do	do	k7c2	do
vily	vila	k1gFnSc2	vila
rodiny	rodina	k1gFnSc2	rodina
Lemmiů	Lemmi	k1gMnPc2	Lemmi
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
opět	opět	k6eAd1	opět
svatební	svatební	k2eAgInSc4d1	svatební
dar	dar	k1gInSc4	dar
rodičů	rodič	k1gMnPc2	rodič
synovi	syn	k1gMnSc3	syn
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
fresky	freska	k1gFnPc1	freska
přeneseny	přenesen	k2eAgFnPc1d1	přenesena
na	na	k7c4	na
plátno	plátno	k1gNnSc4	plátno
a	a	k8xC	a
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1487	[number]	k4	1487
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Botticelli	Botticelle	k1gFnSc4	Botticelle
další	další	k2eAgFnSc4d1	další
zakázku	zakázka	k1gFnSc4	zakázka
od	od	k7c2	od
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c4	na
tondo	tondo	k1gNnSc4	tondo
Madony	Madona	k1gFnSc2	Madona
pro	pro	k7c4	pro
recepční	recepční	k2eAgFnSc4d1	recepční
síň	síň	k1gFnSc4	síň
v	v	k7c6	v
Palazzo	Palazza	k1gFnSc5	Palazza
Vecchio	Vecchia	k1gFnSc5	Vecchia
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Madonu	Madona	k1gFnSc4	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
a	a	k8xC	a
šesti	šest	k4xCc6	šest
anděli	anděl	k1gMnSc5	anděl
(	(	kIx(	(
<g/>
Madonna	Madonen	k2eAgMnSc4d1	Madonen
della	dell	k1gMnSc4	dell
Melagrana	Melagran	k1gMnSc4	Melagran
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Dítě	Dítě	k2eAgFnSc6d1	Dítě
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nS	držet
granátové	granátový	k2eAgNnSc4d1	granátové
jablko	jablko	k1gNnSc4	jablko
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
v	v	k7c4	v
uložen	uložen	k2eAgInSc4d1	uložen
v	v	k7c6	v
Uffiziích	Uffizie	k1gFnPc6	Uffizie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nachází	nacházet	k5eAaImIp3nS	nacházet
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
mladší	mladý	k2eAgNnSc4d2	mladší
Zvěstování	zvěstování	k1gNnSc4	zvěstování
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
umělec	umělec	k1gMnSc1	umělec
původně	původně	k6eAd1	původně
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
pro	pro	k7c4	pro
Franceska	Franceska	k1gFnSc1	Franceska
Guardiho	Guardi	k1gMnSc2	Guardi
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
rodinné	rodinný	k2eAgFnSc2d1	rodinná
kaple	kaple	k1gFnSc2	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Kolektivním	kolektivní	k2eAgNnSc7d1	kolektivní
dílem	dílo	k1gNnSc7	dílo
významných	významný	k2eAgMnPc2d1	významný
florentinských	florentinský	k2eAgMnPc2d1	florentinský
malířů	malíř	k1gMnPc2	malíř
byla	být	k5eAaImAgFnS	být
fresková	freskový	k2eAgFnSc1d1	fresková
výzdoba	výzdoba	k1gFnSc1	výzdoba
letního	letní	k2eAgNnSc2d1	letní
sídla	sídlo	k1gNnSc2	sídlo
Lorenza	Lorenza	k?	Lorenza
I.	I.	kA	I.
Medicejského	Medicejský	k2eAgInSc2d1	Medicejský
zvaného	zvaný	k2eAgInSc2d1	zvaný
Nádherný	nádherný	k2eAgMnSc1d1	nádherný
(	(	kIx(	(
<g/>
Magnifico	Magnifico	k1gMnSc1	Magnifico
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
podporovatel	podporovatel	k1gMnSc1	podporovatel
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
humanismu	humanismus	k1gInSc2	humanismus
zadal	zadat	k5eAaPmAgInS	zadat
Botticellimu	Botticellima	k1gFnSc4	Botticellima
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc3	jeho
bývalému	bývalý	k2eAgMnSc3d1	bývalý
žáku	žák	k1gMnSc3	žák
Filippino	Filippin	k2eAgNnSc1d1	Filippino
Lippimu	Lippimo	k1gNnSc3	Lippimo
<g/>
,	,	kIx,	,
Peruginovi	Perugin	k1gMnSc3	Perugin
a	a	k8xC	a
Ghirlandaiovi	Ghirlandaius	k1gMnSc3	Ghirlandaius
výzdobu	výzdoba	k1gFnSc4	výzdoba
své	svůj	k3xOyFgFnSc2	svůj
venkovské	venkovský	k2eAgFnSc2d1	venkovská
vily	vila	k1gFnSc2	vila
Spedaletto	Spedaletto	k1gNnSc1	Spedaletto
poblíž	poblíž	k7c2	poblíž
Volterry	Volterra	k1gFnSc2	Volterra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
fresky	freska	k1gFnPc1	freska
však	však	k9	však
vzaly	vzít	k5eAaPmAgFnP	vzít
za	za	k7c4	za
své	své	k1gNnSc4	své
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
domu	dům	k1gInSc2	dům
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1491	[number]	k4	1491
byl	být	k5eAaImAgInS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
do	do	k7c2	do
poroty	porota	k1gFnSc2	porota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
zakázce	zakázka	k1gFnSc6	zakázka
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
průčelí	průčelí	k1gNnSc2	průčelí
florentského	florentský	k2eAgInSc2d1	florentský
dómu	dóm	k1gInSc2	dóm
<g/>
.	.	kIx.	.
</s>
<s>
Porota	porota	k1gFnSc1	porota
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
Lorenza	Lorenza	k?	Lorenza
Nádherného	nádherný	k2eAgMnSc2d1	nádherný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1491	[number]	k4	1491
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
převorem	převor	k1gMnSc7	převor
dominikánského	dominikánský	k2eAgInSc2d1	dominikánský
kláštera	klášter	k1gInSc2	klášter
San	San	k1gFnSc2	San
Marco	Marco	k6eAd1	Marco
fanatický	fanatický	k2eAgMnSc1d1	fanatický
kazatel	kazatel	k1gMnSc1	kazatel
Girolamo	Girolama	k1gFnSc5	Girolama
Savonarola	Savonarola	k1gFnSc1	Savonarola
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
načas	načas	k6eAd1	načas
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
vše	všechen	k3xTgNnSc1	všechen
změnilo	změnit	k5eAaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1492	[number]	k4	1492
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Medicejský	Medicejský	k2eAgMnSc1d1	Medicejský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
Savonarolu	Savonarola	k1gFnSc4	Savonarola
zprvu	zprvu	k6eAd1	zprvu
podporoval	podporovat	k5eAaImAgMnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
nato	nato	k6eAd1	nato
byli	být	k5eAaImAgMnP	být
Medicejové	Medicej	k1gMnPc1	Medicej
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Savonarola	Savonarola	k1gFnSc1	Savonarola
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Florencii	Florencie	k1gFnSc4	Florencie
za	za	k7c4	za
vyvolené	vyvolený	k2eAgNnSc4d1	vyvolené
Boží	boží	k2eAgNnSc4d1	boží
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
šířit	šířit	k5eAaImF	šířit
Boží	boží	k2eAgNnPc4d1	boží
království	království	k1gNnPc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
marnivost	marnivost	k1gFnSc4	marnivost
<g/>
,	,	kIx,	,
přepych	přepych	k1gInSc4	přepych
a	a	k8xC	a
myšlenky	myšlenka	k1gFnPc4	myšlenka
humanismu	humanismus	k1gInSc2	humanismus
<g/>
,	,	kIx,	,
hlásal	hlásat	k5eAaImAgInS	hlásat
asketismus	asketismus	k1gInSc1	asketismus
<g/>
.	.	kIx.	.
</s>
<s>
Savonarolův	Savonarolův	k2eAgInSc1d1	Savonarolův
vliv	vliv	k1gInSc1	vliv
se	se	k3xPyFc4	se
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
ani	ani	k8xC	ani
Botticellimu	Botticellima	k1gFnSc4	Botticellima
<g/>
.	.	kIx.	.
</s>
<s>
Obrazy	obraz	k1gInPc1	obraz
s	s	k7c7	s
antickou	antický	k2eAgFnSc7d1	antická
tematikou	tematika	k1gFnSc7	tematika
se	se	k3xPyFc4	se
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
vytrácejí	vytrácet	k5eAaImIp3nP	vytrácet
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
náboženských	náboženský	k2eAgMnPc2d1	náboženský
<g/>
.	.	kIx.	.
</s>
<s>
Sloh	sloh	k1gInSc1	sloh
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
hravost	hravost	k1gFnSc4	hravost
<g/>
,	,	kIx,	,
líbivost	líbivost	k1gFnSc4	líbivost
a	a	k8xC	a
uvolněnost	uvolněnost	k1gFnSc4	uvolněnost
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
exaltovaný	exaltovaný	k2eAgInSc1d1	exaltovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1493	[number]	k4	1493
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Botticelliho	Botticelli	k1gMnSc4	Botticelli
nejstarší	starý	k2eAgMnSc1d3	nejstarší
bratr	bratr	k1gMnSc1	bratr
Giovanni	Giovanň	k1gMnSc3	Giovanň
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
další	další	k2eAgMnSc1d1	další
bratr	bratr	k1gMnSc1	bratr
<g/>
,	,	kIx,	,
Simone	Simon	k1gMnSc5	Simon
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
zakoupili	zakoupit	k5eAaPmAgMnP	zakoupit
venkovskou	venkovský	k2eAgFnSc4d1	venkovská
vilu	vila	k1gFnSc4	vila
v	v	k7c6	v
Bellosguardu	Bellosguard	k1gInSc6	Bellosguard
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
na	na	k7c4	na
Arno	Arno	k1gNnSc4	Arno
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
vyhnání	vyhnání	k1gNnSc6	vyhnání
Medicejů	Medicej	k1gMnPc2	Medicej
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
Botticelli	Botticell	k1gMnPc7	Botticell
zůstával	zůstávat	k5eAaImAgMnS	zůstávat
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
sídle	sídlo	k1gNnSc6	sídlo
ve	v	k7c6	v
Ville	Villa	k1gFnSc6	Villa
del	del	k?	del
Trebbio	Trebbio	k1gNnSc1	Trebbio
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
i	i	k9	i
Michelangelo	Michelangela	k1gFnSc5	Michelangela
<g/>
.	.	kIx.	.
</s>
<s>
Savonarolova	Savonarolův	k2eAgFnSc1d1	Savonarolova
poprava	poprava	k1gFnSc1	poprava
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1498	[number]	k4	1498
Botticellim	Botticellimo	k1gNnPc2	Botticellimo
i	i	k8xC	i
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Simonem	Simon	k1gMnSc7	Simon
hluboce	hluboko	k6eAd1	hluboko
otřásla	otřást	k5eAaPmAgFnS	otřást
<g/>
,	,	kIx,	,
Simone	Simon	k1gMnSc5	Simon
dokonce	dokonce	k9	dokonce
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
aktivní	aktivní	k2eAgFnSc4d1	aktivní
podporu	podpora	k1gFnSc4	podpora
popraveného	popravený	k2eAgMnSc2d1	popravený
dominikána	dominikán	k1gMnSc2	dominikán
musel	muset	k5eAaImAgMnS	muset
z	z	k7c2	z
Florencie	Florencie	k1gFnSc2	Florencie
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1498	[number]	k4	1498
byl	být	k5eAaImAgMnS	být
Botticelli	Botticelle	k1gFnSc4	Botticelle
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
florentského	florentský	k2eAgInSc2d1	florentský
cechu	cech	k1gInSc2	cech
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
lékárníků	lékárník	k1gMnPc2	lékárník
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
malíři	malíř	k1gMnPc1	malíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1504	[number]	k4	1504
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
komisi	komise	k1gFnSc6	komise
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
rozhodovala	rozhodovat	k5eAaImAgFnS	rozhodovat
o	o	k7c4	o
umístění	umístění	k1gNnSc4	umístění
Michelangelova	Michelangelův	k2eAgMnSc2d1	Michelangelův
Davida	David	k1gMnSc2	David
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Botticelli	Botticell	k1gMnPc1	Botticell
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
institutu	institut	k1gInSc3	institut
manželství	manželství	k1gNnSc2	manželství
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rezervovaný	rezervovaný	k2eAgInSc1d1	rezervovaný
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
většina	většina	k1gFnSc1	většina
významných	významný	k2eAgMnPc2d1	významný
malířů	malíř	k1gMnPc2	malíř
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
on	on	k3xPp3gMnSc1	on
svou	svůj	k3xOyFgFnSc4	svůj
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
dílnu	dílna	k1gFnSc4	dílna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
byl	být	k5eAaImAgMnS	být
tajně	tajně	k6eAd1	tajně
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
homosexuálních	homosexuální	k2eAgInPc2d1	homosexuální
styků	styk	k1gInPc2	styk
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
stíhání	stíhání	k1gNnSc1	stíhání
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
jako	jako	k8xC	jako
neopodstatněné	opodstatněný	k2eNgNnSc4d1	neopodstatněné
zastaveno	zastaven	k2eAgNnSc4d1	zastaveno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
života	život	k1gInSc2	život
byl	být	k5eAaImAgInS	být
Botticelli	Botticelle	k1gFnSc4	Botticelle
nemocný	nemocný	k2eAgInSc1d1	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vasariho	Vasari	k1gMnSc2	Vasari
si	se	k3xPyFc3	se
při	při	k7c6	při
chůzi	chůze	k1gFnSc6	chůze
musel	muset	k5eAaImAgMnS	muset
pomáhat	pomáhat	k5eAaImF	pomáhat
dvěma	dva	k4xCgFnPc7	dva
holemi	hole	k1gFnPc7	hole
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1510	[number]	k4	1510
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Ognissanti	Ognissant	k1gMnPc1	Ognissant
(	(	kIx(	(
<g/>
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
brzy	brzy	k6eAd1	brzy
upadlo	upadnout	k5eAaPmAgNnS	upadnout
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc6	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
Znovuobjeveno	znovuobjeven	k2eAgNnSc1d1	znovuobjeven
bylo	být	k5eAaImAgNnS	být
až	až	k6eAd1	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Botticelli	Botticell	k1gMnPc1	Botticell
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
představitelům	představitel	k1gMnPc3	představitel
italského	italský	k2eAgNnSc2d1	italské
quattrocenta	quattrocento	k1gNnSc2	quattrocento
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
svébytný	svébytný	k2eAgInSc4d1	svébytný
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
vyznačující	vyznačující	k2eAgFnPc4d1	vyznačující
se	se	k3xPyFc4	se
elegancí	elegance	k1gFnSc7	elegance
provedení	provedení	k1gNnSc2	provedení
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
linii	linie	k1gFnSc4	linie
a	a	k8xC	a
detail	detail	k1gInSc4	detail
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
obrazech	obraz	k1gInPc6	obraz
někdy	někdy	k6eAd1	někdy
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
okázalá	okázalý	k2eAgNnPc1d1	okázalé
zátiší	zátiší	k1gNnPc1	zátiší
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Savonarolova	Savonarolův	k2eAgNnSc2d1	Savonarolův
vystoupení	vystoupení	k1gNnSc2	vystoupení
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
novoplatonismu	novoplatonismus	k1gInSc2	novoplatonismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
skloubit	skloubit	k5eAaPmF	skloubit
antické	antický	k2eAgInPc4d1	antický
ideály	ideál	k1gInPc4	ideál
s	s	k7c7	s
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
malba	malba	k1gFnSc1	malba
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kresebností	kresebnost	k1gFnSc7	kresebnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
obrazy	obraz	k1gInPc1	obraz
mají	mít	k5eAaImIp3nP	mít
melancholickou	melancholický	k2eAgFnSc4d1	melancholická
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
citový	citový	k2eAgInSc4d1	citový
aspekt	aspekt	k1gInSc4	aspekt
obrazů	obraz	k1gInPc2	obraz
bývá	bývat	k5eAaImIp3nS	bývat
zdůrazněn	zdůraznit	k5eAaPmNgInS	zdůraznit
dynamickými	dynamický	k2eAgFnPc7d1	dynamická
křivkami	křivka	k1gFnPc7	křivka
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pojetí	pojetí	k1gNnSc6	pojetí
krajiny	krajina	k1gFnSc2	krajina
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
vlámského	vlámský	k2eAgNnSc2d1	vlámské
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1490	[number]	k4	1490
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
věnovat	věnovat	k5eAaImF	věnovat
především	především	k6eAd1	především
náboženské	náboženský	k2eAgFnSc3d1	náboženská
tematice	tematika	k1gFnSc3	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
obrazy	obraz	k1gInPc1	obraz
odrážely	odrážet	k5eAaImAgInP	odrážet
apokalyptické	apokalyptický	k2eAgFnPc4d1	apokalyptická
nálady	nálada	k1gFnPc4	nálada
konce	konec	k1gInSc2	konec
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
ve	v	k7c6	v
svatozáři	svatozář	k1gFnSc6	svatozář
z	z	k7c2	z
cherubínů	cherubín	k1gMnPc2	cherubín
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
120	[number]	k4	120
x	x	k?	x
65	[number]	k4	65
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Galleria	Gallerium	k1gNnSc2	Gallerium
degli	degl	k1gMnPc1	degl
Uffizi	Uffih	k1gMnPc1	Uffih
<g/>
,	,	kIx,	,
1469	[number]	k4	1469
<g/>
–	–	k?	–
<g/>
1470	[number]	k4	1470
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
Botticelliových	Botticelliový	k2eAgFnPc2d1	Botticelliový
Madon	Madona	k1gFnPc2	Madona
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
ukázka	ukázka	k1gFnSc1	ukázka
jeho	jeho	k3xOp3gFnSc2	jeho
rané	raný	k2eAgFnSc2d1	raná
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ještě	ještě	k9	ještě
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Modelace	modelace	k1gFnSc1	modelace
Madoniny	Madonin	k2eAgFnSc2d1	Madonin
tváře	tvář	k1gFnSc2	tvář
využívá	využívat	k5eAaPmIp3nS	využívat
hry	hra	k1gFnPc4	hra
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
stínu	stín	k1gInSc2	stín
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
zdůrazněn	zdůrazněn	k2eAgInSc1d1	zdůrazněn
zlatým	zlatý	k2eAgNnSc7d1	Zlaté
pozadím	pozadí	k1gNnSc7	pozadí
se	s	k7c7	s
svatozáří	svatozář	k1gFnSc7	svatozář
složenou	složený	k2eAgFnSc7d1	složená
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
cherubínů	cherubín	k1gMnPc2	cherubín
<g/>
.	.	kIx.	.
</s>
<s>
Botticelli	Botticelle	k1gFnSc4	Botticelle
zde	zde	k6eAd1	zde
použil	použít	k5eAaPmAgMnS	použít
ikonografický	ikonografický	k2eAgInSc4d1	ikonografický
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
umění	umění	k1gNnSc6	umění
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
běžný	běžný	k2eAgInSc1d1	běžný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klanění	klanění	k1gNnSc1	klanění
Tří	tři	k4xCgMnPc2	tři
králů	král	k1gMnPc2	král
(	(	kIx(	(
<g/>
Del	Del	k1gFnSc1	Del
Lamovo	lamův	k2eAgNnSc1d1	Lamovo
Klanění	klanění	k1gNnSc1	klanění
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
111	[number]	k4	111
x	x	k?	x
134	[number]	k4	134
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Galleria	Gallerium	k1gNnSc2	Gallerium
degli	degl	k1gMnPc1	degl
Uffizi	Uffih	k1gMnPc1	Uffih
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1475	[number]	k4	1475
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc4	obraz
si	se	k3xPyFc3	se
objednal	objednat	k5eAaPmAgMnS	objednat
florentský	florentský	k2eAgMnSc1d1	florentský
zbohatlík	zbohatlík	k1gMnSc1	zbohatlík
Gasparre	Gasparr	k1gInSc5	Gasparr
del	del	k?	del
Lama	lama	k1gFnSc1	lama
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
byli	být	k5eAaImAgMnP	být
vyobrazeni	vyobrazen	k2eAgMnPc1d1	vyobrazen
členové	člen	k1gMnPc1	člen
rodiny	rodina	k1gFnSc2	rodina
Medici	medik	k1gMnPc1	medik
i	i	k8xC	i
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
středu	střed	k1gInSc6	střed
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dojem	dojem	k1gInSc1	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
postava	postava	k1gFnSc1	postava
vpravo	vpravo	k6eAd1	vpravo
vzadu	vzadu	k6eAd1	vzadu
je	být	k5eAaImIp3nS	být
autoportrét	autoportrét	k1gInSc1	autoportrét
autora	autor	k1gMnSc4	autor
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
dobového	dobový	k2eAgInSc2d1	dobový
zvyku	zvyk	k1gInSc2	zvyk
se	se	k3xPyFc4	se
vypodobnil	vypodobnit	k5eAaPmAgMnS	vypodobnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
diváka	divák	k1gMnSc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Botticelliho	Botticelli	k1gMnSc4	Botticelli
jediný	jediný	k2eAgMnSc1d1	jediný
známý	známý	k1gMnSc1	známý
autoportrét	autoportrét	k1gInSc1	autoportrét
<g/>
.	.	kIx.	.
</s>
<s>
Del	Del	k?	Del
Lampa	lampa	k1gFnSc1	lampa
je	být	k5eAaImIp3nS	být
šedivý	šedivý	k2eAgMnSc1d1	šedivý
prostovlasý	prostovlasý	k2eAgMnSc1d1	prostovlasý
muž	muž	k1gMnSc1	muž
uprostřed	uprostřed	k7c2	uprostřed
pravé	pravý	k2eAgFnSc2d1	pravá
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
hledící	hledící	k2eAgMnSc1d1	hledící
na	na	k7c4	na
diváka	divák	k1gMnSc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Identifikace	identifikace	k1gFnSc1	identifikace
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
rodiny	rodina	k1gFnSc2	rodina
Medicejských	Medicejský	k2eAgInPc2d1	Medicejský
není	být	k5eNaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
.	.	kIx.	.
</s>
<s>
Nepochybné	pochybný	k2eNgNnSc1d1	nepochybné
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
<g/>
,	,	kIx,	,
že	že	k8xS	že
král	král	k1gMnSc1	král
klečící	klečící	k2eAgFnSc2d1	klečící
před	před	k7c7	před
Madonou	Madona	k1gFnSc7	Madona
je	být	k5eAaImIp3nS	být
Cosimo	Cosima	k1gFnSc5	Cosima
starší	starší	k1gMnSc1	starší
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Augustin	Augustin	k1gMnSc1	Augustin
<g/>
,	,	kIx,	,
freska	freska	k1gFnSc1	freska
<g/>
,	,	kIx,	,
185	[number]	k4	185
x	x	k?	x
123	[number]	k4	123
cm	cm	kA	cm
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Ognissanti	Ognissant	k1gMnPc1	Ognissant
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1480	[number]	k4	1480
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Botticelliho	Botticelli	k1gMnSc4	Botticelli
doménou	doména	k1gFnSc7	doména
byly	být	k5eAaImAgFnP	být
malby	malba	k1gFnPc4	malba
temperou	tempera	k1gFnSc7	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k9	ani
nástěnným	nástěnný	k2eAgInPc3d1	nástěnný
obrazům	obraz	k1gInPc3	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Augustin	Augustin	k1gMnSc1	Augustin
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc4d3	nejstarší
dochovanou	dochovaný	k2eAgFnSc4d1	dochovaná
Botticelliho	Botticelliha	k1gFnSc5	Botticelliha
freskou	freska	k1gFnSc7	freska
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
erb	erb	k1gInSc1	erb
umístěný	umístěný	k2eAgInSc1d1	umístěný
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
zadavateli	zadavatel	k1gMnSc3	zadavatel
byla	být	k5eAaImAgFnS	být
rodina	rodina	k1gFnSc1	rodina
Vespucci	Vespucce	k1gFnSc4	Vespucce
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
tvoří	tvořit	k5eAaImIp3nS	tvořit
protějšek	protějšek	k1gInSc4	protějšek
ke	k	k7c3	k
Ghirlandajově	Ghirlandajův	k2eAgFnSc3d1	Ghirlandajův
obrazu	obraz	k1gInSc3	obraz
Svatý	svatý	k2eAgMnSc1d1	svatý
Jeroným	Jeroným	k1gMnSc1	Jeroným
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pravou	pravý	k2eAgFnSc7d1	pravá
rukou	ruka	k1gFnSc7	ruka
na	na	k7c6	na
srdci	srdce	k1gNnSc6	srdce
se	se	k3xPyFc4	se
sv.	sv.	kA	sv.
Augustin	Augustin	k1gMnSc1	Augustin
chápe	chápat	k5eAaImIp3nS	chápat
pera	pero	k1gNnSc2	pero
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zapsal	zapsat	k5eAaPmAgMnS	zapsat
své	svůj	k3xOyFgNnSc4	svůj
vidění	vidění	k1gNnSc4	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
dobu	doba	k1gFnSc4	doba
západu	západ	k1gInSc2	západ
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
určují	určovat	k5eAaImIp3nP	určovat
tak	tak	k6eAd1	tak
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
situace	situace	k1gFnSc1	situace
nastala	nastat	k5eAaPmAgFnS	nastat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jaro	jaro	k1gNnSc1	jaro
(	(	kIx(	(
<g/>
Primavera	Primavera	k1gFnSc1	Primavera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
topolovém	topolový	k2eAgNnSc6d1	topolové
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
230	[number]	k4	230
x	x	k?	x
314	[number]	k4	314
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Galleria	Gallerium	k1gNnSc2	Gallerium
degli	degl	k1gMnPc1	degl
Uffizi	Uffih	k1gMnPc1	Uffih
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1482	[number]	k4	1482
<g/>
.	.	kIx.	.
</s>
<s>
Interpretace	interpretace	k1gFnSc1	interpretace
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
předmětem	předmět	k1gInSc7	předmět
odborných	odborný	k2eAgFnPc2d1	odborná
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
ani	ani	k8xC	ani
datace	datace	k1gFnSc1	datace
obrazu	obraz	k1gInSc2	obraz
není	být	k5eNaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
jeho	on	k3xPp3gInSc4	on
vznik	vznik	k1gInSc4	vznik
kladou	klást	k5eAaImIp3nP	klást
až	až	k9	až
do	do	k7c2	do
let	léto	k1gNnPc2	léto
1485	[number]	k4	1485
<g/>
–	–	k?	–
<g/>
1487	[number]	k4	1487
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc4	obraz
Botticelli	Botticelle	k1gFnSc4	Botticelle
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
namaloval	namalovat	k5eAaPmAgMnS	namalovat
pro	pro	k7c4	pro
Lorenza	Lorenza	k?	Lorenza
di	di	k?	di
Pierfranceska	Pierfranceska	k1gFnSc1	Pierfranceska
<g/>
,	,	kIx,	,
bratrance	bratranec	k1gMnPc4	bratranec
Lorenza	Lorenza	k?	Lorenza
Nádherného	nádherný	k2eAgMnSc2d1	nádherný
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
má	mít	k5eAaImIp3nS	mít
nejméně	málo	k6eAd3	málo
dvě	dva	k4xCgFnPc4	dva
významové	významový	k2eAgFnPc4d1	významová
roviny	rovina	k1gFnPc4	rovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
plánu	plán	k1gInSc6	plán
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
příchodu	příchod	k1gInSc2	příchod
jara	jaro	k1gNnSc2	jaro
ve	v	k7c6	v
Venušině	Venušin	k2eAgFnSc6d1	Venušina
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
také	také	k9	také
metaforou	metafora	k1gFnSc7	metafora
pojetí	pojetí	k1gNnSc2	pojetí
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gNnSc4	on
vykládal	vykládat	k5eAaImAgMnS	vykládat
novoplatónský	novoplatónský	k2eAgMnSc1d1	novoplatónský
filosof	filosof	k1gMnSc1	filosof
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
Medicejských	Medicejský	k2eAgFnPc2d1	Medicejská
Marsilio	Marsilio	k6eAd1	Marsilio
Ficino	Ficin	k2eAgNnSc1d1	Ficin
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byla	být	k5eAaImAgFnS	být
dualitou	dualita	k1gFnSc7	dualita
pozemské	pozemský	k2eAgFnSc2d1	pozemská
<g/>
,	,	kIx,	,
tělesné	tělesný	k2eAgFnSc2d1	tělesná
touhy	touha	k1gFnSc2	touha
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
<g/>
,	,	kIx,	,
směřující	směřující	k2eAgFnSc2d1	směřující
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
třemi	tři	k4xCgInPc7	tři
základními	základní	k2eAgInPc7d1	základní
zdroji	zdroj	k1gInPc7	zdroj
<g/>
:	:	kIx,	:
Ovidiovým	Ovidiův	k2eAgInSc7d1	Ovidiův
kalendářem	kalendář	k1gInSc7	kalendář
římských	římský	k2eAgInPc2d1	římský
náboženských	náboženský	k2eAgInPc2d1	náboženský
svátků	svátek	k1gInPc2	svátek
Fasti	Fasť	k1gFnSc2	Fasť
<g/>
,	,	kIx,	,
básní	básnit	k5eAaImIp3nS	básnit
De	De	k?	De
rerum	rerum	k1gInSc1	rerum
natura	natura	k1gFnSc1	natura
(	(	kIx(	(
<g/>
O	o	k7c6	o
povaze	povaha	k1gFnSc6	povaha
věcí	věc	k1gFnPc2	věc
<g/>
)	)	kIx)	)
od	od	k7c2	od
Lucretia	Lucretium	k1gNnSc2	Lucretium
a	a	k8xC	a
verši	verš	k1gInSc6	verš
medicejského	medicejský	k2eAgMnSc2d1	medicejský
dvorního	dvorní	k2eAgMnSc2d1	dvorní
básníka	básník	k1gMnSc2	básník
Angela	angel	k1gMnSc2	angel
Poliziana	Polizian	k1gMnSc2	Polizian
–	–	k?	–
Botticelliho	Botticelli	k1gMnSc2	Botticelli
přítele	přítel	k1gMnSc2	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obrazu	obraz	k1gInSc6	obraz
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1499	[number]	k4	1499
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
visel	viset	k5eAaImAgInS	viset
ve	v	k7c6	v
florentském	florentský	k2eAgInSc6d1	florentský
paláci	palác	k1gInSc6	palác
Lorenza	Lorenza	k?	Lorenza
di	di	k?	di
Pierfranceska	Pierfranceska	k1gFnSc1	Pierfranceska
Medicejského	Medicejský	k2eAgNnSc2d1	Medicejské
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Venuše	Venuše	k1gFnSc2	Venuše
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
jara	jaro	k1gNnSc2	jaro
zdobena	zdoben	k2eAgFnSc1d1	zdobena
květinami	květina	k1gFnPc7	květina
Grácií	Grácie	k1gFnPc2	Grácie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gNnSc4	on
nazval	nazvat	k5eAaBmAgMnS	nazvat
Vasari	Vasari	k1gNnSc4	Vasari
<g/>
.	.	kIx.	.
</s>
<s>
Figury	figura	k1gFnPc4	figura
zprava	zprava	k6eAd1	zprava
<g/>
:	:	kIx,	:
okřídlené	okřídlený	k2eAgNnSc1d1	okřídlené
mýtické	mýtický	k2eAgNnSc1d1	mýtické
stvoření	stvoření	k1gNnSc1	stvoření
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
je	být	k5eAaImIp3nS	být
Vítr	vítr	k1gInSc1	vítr
Zefyros	Zefyrosa	k1gFnPc2	Zefyrosa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
unést	unést	k5eAaPmF	unést
nymfu	nymfa	k1gFnSc4	nymfa
Chlóris	Chlóris	k1gFnSc2	Chlóris
v	v	k7c6	v
průhledných	průhledný	k2eAgInPc6d1	průhledný
šatech	šat	k1gInPc6	šat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
bázlivé	bázlivý	k2eAgNnSc1d1	bázlivé
a	a	k8xC	a
odmítavé	odmítavý	k2eAgNnSc1d1	odmítavé
gesto	gesto	k1gNnSc1	gesto
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
sama	sám	k3xTgFnSc1	sám
sňatek	sňatek	k1gInSc4	sňatek
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Zefyros	Zefyrosa	k1gFnPc2	Zefyrosa
posléze	posléze	k6eAd1	posléze
litoval	litovat	k5eAaImAgMnS	litovat
svého	svůj	k3xOyFgInSc2	svůj
násilného	násilný	k2eAgInSc2d1	násilný
činu	čin	k1gInSc2	čin
a	a	k8xC	a
proměnil	proměnit	k5eAaPmAgMnS	proměnit
Chlóris	Chlóris	k1gFnSc4	Chlóris
v	v	k7c6	v
bohyni	bohyně	k1gFnSc6	bohyně
jarních	jarní	k2eAgInPc2d1	jarní
květů	květ	k1gInPc2	květ
Flóru	flór	k1gInSc2	flór
<g/>
.	.	kIx.	.
</s>
<s>
Krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
mladistvá	mladistvý	k2eAgFnSc1d1	mladistvá
Flóra	Flóra	k1gFnSc1	Flóra
<g/>
,	,	kIx,	,
rozhazující	rozhazující	k2eAgMnSc1d1	rozhazující
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
postava	postava	k1gFnSc1	postava
zprava	zprava	k6eAd1	zprava
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
uprostřed	uprostřed	k7c2	uprostřed
obrazu	obraz	k1gInSc2	obraz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bohyně	bohyně	k1gFnSc1	bohyně
lásky	láska	k1gFnSc2	láska
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
vznáší	vznášet	k5eAaImIp3nS	vznášet
její	její	k3xOp3gMnSc1	její
syn	syn	k1gMnSc1	syn
Amor	Amor	k1gMnSc1	Amor
<g/>
.	.	kIx.	.
</s>
<s>
Venuši	Venuše	k1gFnSc4	Venuše
dělají	dělat	k5eAaImIp3nP	dělat
společnost	společnost	k1gFnSc1	společnost
tři	tři	k4xCgFnPc4	tři
tančící	tančící	k2eAgFnPc4d1	tančící
nymfy	nymfa	k1gFnPc4	nymfa
–	–	k?	–
Tři	tři	k4xCgFnPc1	tři
Grácie	Grácie	k1gFnSc1	Grácie
<g/>
.	.	kIx.	.
</s>
<s>
Mladík	mladík	k1gMnSc1	mladík
úplně	úplně	k6eAd1	úplně
vlevo	vlevo	k6eAd1	vlevo
je	být	k5eAaImIp3nS	být
posel	posel	k1gMnSc1	posel
bohů	bůh	k1gMnPc2	bůh
Merkur	Merkur	k1gMnSc1	Merkur
(	(	kIx(	(
<g/>
řec.	řec.	k?	řec.
Hermés	Hermés	k1gInSc1	Hermés
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
hůl	hůl	k1gFnSc4	hůl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
rozehnal	rozehnat	k5eAaPmAgMnS	rozehnat
mraky	mrak	k1gInPc4	mrak
a	a	k8xC	a
ochránil	ochránit	k5eAaPmAgMnS	ochránit
věčný	věčný	k2eAgInSc4d1	věčný
mír	mír	k1gInSc4	mír
ve	v	k7c6	v
Venušině	Venušin	k2eAgFnSc6d1	Venušina
zahradě	zahrada	k1gFnSc6	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
obrazů	obraz	k1gInPc2	obraz
Příběh	příběh	k1gInSc1	příběh
Nastagia	Nastagium	k1gNnSc2	Nastagium
degli	degl	k1gMnPc1	degl
Onesti	Onest	k1gFnSc2	Onest
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
82	[number]	k4	82
x	x	k?	x
138	[number]	k4	138
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Museo	Museo	k6eAd1	Museo
del	del	k?	del
Prado	Prado	k1gNnSc1	Prado
<g/>
,	,	kIx,	,
1483	[number]	k4	1483
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1483	[number]	k4	1483
si	se	k3xPyFc3	se
u	u	k7c2	u
Botticelliho	Botticelli	k1gMnSc2	Botticelli
florentský	florentský	k2eAgMnSc1d1	florentský
kupec	kupec	k1gMnSc1	kupec
Antonio	Antonio	k1gMnSc1	Antonio
Pucci	Pucci	k?	Pucci
jako	jako	k8xC	jako
svatební	svatební	k2eAgInSc4d1	svatební
dar	dar	k1gInSc4	dar
pro	pro	k7c4	pro
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
objednal	objednat	k5eAaPmAgInS	objednat
soubor	soubor	k1gInSc1	soubor
čtyř	čtyři	k4xCgInPc2	čtyři
obrazů	obraz	k1gInPc2	obraz
na	na	k7c4	na
námět	námět	k1gInSc4	námět
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
povídek	povídka	k1gFnPc2	povídka
Boccacciova	Boccacciův	k2eAgInSc2d1	Boccacciův
Dekameronu	Dekameron	k1gInSc2	Dekameron
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
rytíře	rytíř	k1gMnSc4	rytíř
Nastagia	Nastagius	k1gMnSc4	Nastagius
degli	degnout	k5eAaPmAgMnP	degnout
Onesti	Onest	k1gMnPc1	Onest
z	z	k7c2	z
Ravenny	Ravenna	k1gFnSc2	Ravenna
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
reprodukovaný	reprodukovaný	k2eAgInSc4d1	reprodukovaný
obraz	obraz	k1gInSc4	obraz
má	mít	k5eAaImIp3nS	mít
pořadové	pořadový	k2eAgNnSc1d1	pořadové
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
<g/>
,	,	kIx,	,
truchlící	truchlící	k2eAgMnSc1d1	truchlící
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
milé	milá	k1gFnSc6	milá
v	v	k7c6	v
piniovém	piniový	k2eAgInSc6d1	piniový
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
vidí	vidět	k5eAaImIp3nS	vidět
nahou	nahý	k2eAgFnSc4d1	nahá
ženu	žena	k1gFnSc4	žena
pronásledovanou	pronásledovaný	k2eAgFnSc4d1	pronásledovaná
jiným	jiný	k2eAgMnSc7d1	jiný
rytířem	rytíř	k1gMnSc7	rytíř
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
psem	pes	k1gMnSc7	pes
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
žena	žena	k1gFnSc1	žena
dopadena	dopadnout	k5eAaPmNgFnS	dopadnout
<g/>
,	,	kIx,	,
vyrval	vyrvat	k5eAaPmAgMnS	vyrvat
jí	on	k3xPp3gFnSc3	on
rytíř	rytíř	k1gMnSc1	rytíř
srdce	srdce	k1gNnSc4	srdce
a	a	k8xC	a
vnitřnosti	vnitřnost	k1gFnPc4	vnitřnost
a	a	k8xC	a
nakrmil	nakrmit	k5eAaPmAgMnS	nakrmit
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
své	svůj	k3xOyFgMnPc4	svůj
psy	pes	k1gMnPc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Žena	žena	k1gFnSc1	žena
posléze	posléze	k6eAd1	posléze
opět	opět	k6eAd1	opět
nezraněna	zranit	k5eNaPmNgFnS	zranit
vstala	vstát	k5eAaPmAgFnS	vstát
a	a	k8xC	a
koloběh	koloběh	k1gInSc1	koloběh
událostí	událost	k1gFnPc2	událost
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
obrazu	obraz	k1gInSc6	obraz
cyklu	cyklus	k1gInSc2	cyklus
<g/>
,	,	kIx,	,
zachycujícím	zachycující	k2eAgInSc7d1	zachycující
Onestiho	Onesti	k1gMnSc4	Onesti
svatební	svatební	k2eAgFnSc4d1	svatební
hostinu	hostina	k1gFnSc4	hostina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vyobrazeni	vyobrazit	k5eAaPmNgMnP	vyobrazit
rovněž	rovněž	k9	rovněž
příslušníci	příslušník	k1gMnPc1	příslušník
rodiny	rodina	k1gFnSc2	rodina
Medicejských	Medicejský	k2eAgMnPc2d1	Medicejský
–	–	k?	–
roli	role	k1gFnSc4	role
prostředníka	prostředník	k1gMnSc2	prostředník
mezi	mezi	k7c4	mezi
Pucciho	Pucci	k1gMnSc4	Pucci
synem	syn	k1gMnSc7	syn
Giannozzem	Giannozz	k1gMnSc7	Giannozz
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
nevěstou	nevěsta	k1gFnSc7	nevěsta
sehrál	sehrát	k5eAaPmAgInS	sehrát
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Nádherný	nádherný	k2eAgInSc4d1	nádherný
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
visel	viset	k5eAaImAgInS	viset
ve	v	k7c6	v
svatebním	svatební	k2eAgInSc6d1	svatební
pokoji	pokoj	k1gInSc6	pokoj
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Pucciů	Pucci	k1gInPc2	Pucci
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
granátovým	granátový	k2eAgNnSc7d1	granátové
jablkem	jablko	k1gNnSc7	jablko
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc4	průměr
143,5	[number]	k4	143,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Galleria	Gallerium	k1gNnSc2	Gallerium
degli	degl	k1gMnPc1	degl
Uffizi	Uffih	k1gMnPc1	Uffih
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1485	[number]	k4	1485
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
drží	držet	k5eAaImIp3nS	držet
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
granátové	granátový	k2eAgNnSc1d1	granátové
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
Madona	Madona	k1gFnSc1	Madona
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
šesticí	šestice	k1gFnSc7	šestice
andělů	anděl	k1gMnPc2	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Jablko	jablko	k1gNnSc1	jablko
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
semen	semeno	k1gNnPc2	semeno
jeho	on	k3xPp3gInSc2	on
intenzitu	intenzita	k1gFnSc4	intenzita
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
má	mít	k5eAaImIp3nS	mít
formu	forma	k1gFnSc4	forma
tonda	tondo	k1gNnSc2	tondo
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
zvolil	zvolit	k5eAaPmAgMnS	zvolit
symetrickou	symetrický	k2eAgFnSc4d1	symetrická
kompozici	kompozice	k1gFnSc4	kompozice
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
andělích	andělí	k2eAgInPc6d1	andělí
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
starším	starý	k2eAgFnPc3d2	starší
Botticelliho	Botticelliha	k1gFnSc5	Botticelliha
obrazům	obraz	k1gInPc3	obraz
působí	působit	k5eAaImIp3nP	působit
postavy	postava	k1gFnPc4	postava
strnule	strnule	k6eAd1	strnule
a	a	k8xC	a
neživotně	životně	k6eNd1	životně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zrození	zrození	k1gNnSc1	zrození
Venuše	Venuše	k1gFnSc2	Venuše
(	(	kIx(	(
<g/>
Botticelli	Botticelle	k1gFnSc4	Botticelle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
,	,	kIx,	,
172,5	[number]	k4	172,5
x	x	k?	x
278,5	[number]	k4	278,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Galleria	Gallerium	k1gNnSc2	Gallerium
degli	degl	k1gMnPc1	degl
Uffizi	Uffih	k1gMnPc1	Uffih
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1485	[number]	k4	1485
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
namalovaný	namalovaný	k2eAgInSc1d1	namalovaný
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pro	pro	k7c4	pro
některého	některý	k3yIgMnSc4	některý
z	z	k7c2	z
Medicejských	Medicejský	k2eAgNnPc2d1	Medicejské
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
do	do	k7c2	do
venkovského	venkovský	k2eAgNnSc2d1	venkovské
sídla	sídlo	k1gNnSc2	sídlo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
umisťovaly	umisťovat	k5eAaImAgInP	umisťovat
obrazy	obraz	k1gInPc1	obraz
na	na	k7c6	na
plátně	plátno	k1gNnSc6	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Literárními	literární	k2eAgFnPc7d1	literární
inspiracemi	inspirace	k1gFnPc7	inspirace
pro	pro	k7c4	pro
obraz	obraz	k1gInSc4	obraz
byly	být	k5eAaImAgFnP	být
Homérův	Homérův	k2eAgInSc4d1	Homérův
chvalozpěv	chvalozpěv	k1gInSc4	chvalozpěv
na	na	k7c4	na
Venuši	Venuše	k1gFnSc4	Venuše
(	(	kIx(	(
<g/>
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
Afroditu	Afrodita	k1gFnSc4	Afrodita
<g/>
)	)	kIx)	)
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
také	také	k9	také
–	–	k?	–
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Primavery	Primavera	k1gFnSc2	Primavera
–	–	k?	–
báseň	báseň	k1gFnSc1	báseň
Angela	Angela	k1gFnSc1	Angela
Poliziana	Poliziana	k1gFnSc1	Poliziana
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
obrazu	obraz	k1gInSc2	obraz
je	být	k5eAaImIp3nS	být
příjezd	příjezd	k1gInSc1	příjezd
Venuše	Venuše	k1gFnSc2	Venuše
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Kythéru	Kythér	k1gInSc2	Kythér
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ho	on	k3xPp3gMnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
Homér	Homér	k1gMnSc1	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Vlevo	vlevo	k6eAd1	vlevo
se	se	k3xPyFc4	se
bůh	bůh	k1gMnSc1	bůh
větrů	vítr	k1gInPc2	vítr
Zefyros	Zefyrosa	k1gFnPc2	Zefyrosa
<g/>
,	,	kIx,	,
objevující	objevující	k2eAgFnPc1d1	objevující
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
Primaveře	Primavera	k1gFnSc6	Primavera
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
brízy	bríza	k1gFnSc2	bríza
Aurou	aura	k1gFnSc7	aura
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
dofouknout	dofouknout	k5eAaPmF	dofouknout
Venuši	Venuše	k1gFnSc4	Venuše
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
plaví	plavit	k5eAaImIp3nS	plavit
na	na	k7c6	na
lastuře	lastura	k1gFnSc6	lastura
<g/>
,	,	kIx,	,
symbolu	symbol	k1gInSc6	symbol
ženství	ženství	k1gNnSc2	ženství
<g/>
.	.	kIx.	.
</s>
<s>
Cudná	cudný	k2eAgFnSc1d1	cudná
Venušina	Venušin	k2eAgFnSc1d1	Venušina
póza	póza	k1gFnSc1	póza
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
antického	antický	k2eAgInSc2d1	antický
ikonografického	ikonografický	k2eAgInSc2d1	ikonografický
typu	typ	k1gInSc2	typ
Venuše	Venuše	k1gFnSc2	Venuše
Pudica	Pudica	k1gFnSc1	Pudica
(	(	kIx(	(
<g/>
cudná	cudný	k2eAgFnSc1d1	cudná
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vpravo	vpravo	k6eAd1	vpravo
Venuši	Venuše	k1gFnSc4	Venuše
vítá	vítat	k5eAaImIp3nS	vítat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
Hór	Hóra	k1gFnPc2	Hóra
<g/>
,	,	kIx,	,
bohyň	bohyně	k1gFnPc2	bohyně
ročních	roční	k2eAgFnPc2d1	roční
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
jí	on	k3xPp3gFnSc7	on
plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
zdobený	zdobený	k2eAgInSc1d1	zdobený
jarními	jarní	k2eAgFnPc7d1	jarní
květinami	květina	k1gFnPc7	květina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
oblohy	obloha	k1gFnPc4	obloha
se	se	k3xPyFc4	se
snášejí	snášet	k5eAaImIp3nP	snášet
růže	růž	k1gFnPc1	růž
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
vznik	vznik	k1gInSc1	vznik
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
Venušiným	Venušin	k2eAgNnSc7d1	Venušino
zrozením	zrození	k1gNnSc7	zrození
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
navštívená	navštívený	k2eAgFnSc1d1	navštívená
čtyřmi	čtyři	k4xCgNnPc7	čtyři
anděly	anděl	k1gMnPc4	anděl
a	a	k8xC	a
šesti	šest	k4xCc7	šest
svatými	svatá	k1gFnPc7	svatá
<g/>
,	,	kIx,	,
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
268	[number]	k4	268
x	x	k?	x
280	[number]	k4	280
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Galleria	Gallerium	k1gNnSc2	Gallerium
degli	degl	k1gMnPc1	degl
Uffizi	Uffih	k1gMnPc1	Uffih
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1487	[number]	k4	1487
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
namalovaný	namalovaný	k2eAgInSc1d1	namalovaný
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgInSc4d1	hlavní
oltář	oltář	k1gInSc4	oltář
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Barnabáše	Barnabáš	k1gMnSc2	Barnabáš
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
.	.	kIx.	.
</s>
<s>
Patronem	patron	k1gInSc7	patron
kostela	kostel	k1gInSc2	kostel
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
patronát	patronát	k1gInSc4	patronát
svěřilo	svěřit	k5eAaPmAgNnS	svěřit
cechu	cech	k1gInSc2	cech
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
lékárníků	lékárník	k1gMnPc2	lékárník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sdružoval	sdružovat	k5eAaImAgInS	sdružovat
i	i	k9	i
malíře	malíř	k1gMnSc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Botticelli	Botticelle	k1gFnSc4	Botticelle
obraz	obraz	k1gInSc1	obraz
namaloval	namalovat	k5eAaPmAgInS	namalovat
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
cechu	cech	k1gInSc2	cech
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
sám	sám	k3xTgMnSc1	sám
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
roku	rok	k1gInSc2	rok
1498	[number]	k4	1498
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Madona	Madona	k1gFnSc1	Madona
s	s	k7c7	s
dítětem	dítě	k1gNnSc7	dítě
a	a	k8xC	a
dvěma	dva	k4xCgMnPc7	dva
anděly	anděl	k1gMnPc7	anděl
<g/>
,	,	kIx,	,
olej	olej	k1gInSc1	olej
a	a	k8xC	a
tempera	tempera	k1gFnSc1	tempera
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc4	průměr
34	[number]	k4	34
cm	cm	kA	cm
<g/>
,	,	kIx,	,
Art	Art	k1gFnSc1	Art
Institute	institut	k1gInSc5	institut
of	of	k?	of
Chicago	Chicago	k1gNnSc4	Chicago
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
1493	[number]	k4	1493
<g/>
.	.	kIx.	.
</s>
<s>
Chicagský	chicagský	k2eAgInSc1d1	chicagský
obraz	obraz	k1gInSc1	obraz
je	být	k5eAaImIp3nS	být
ukázkou	ukázka	k1gFnSc7	ukázka
pozdního	pozdní	k2eAgMnSc4d1	pozdní
Botticelliho	Botticelli	k1gMnSc4	Botticelli
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Malíř	malíř	k1gMnSc1	malíř
postupně	postupně	k6eAd1	postupně
opouštěl	opouštět	k5eAaImAgMnS	opouštět
harmonii	harmonie	k1gFnSc4	harmonie
v	v	k7c6	v
proporcích	proporce	k1gFnPc6	proporce
postav	postava	k1gFnPc2	postava
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Madona	Madona	k1gFnSc1	Madona
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
tondu	tondo	k1gNnSc6	tondo
působí	působit	k5eAaImIp3nS	působit
vůči	vůči	k7c3	vůči
Dítěti	dítě	k1gNnSc3	dítě
a	a	k8xC	a
andělům	anděl	k1gMnPc3	anděl
předimenzovaně	předimenzovaně	k6eAd1	předimenzovaně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Codr	Codr	k1gMnSc1	Codr
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přemožitelé	přemožitel	k1gMnPc1	přemožitel
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
16	[number]	k4	16
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
Pijoan	Pijoan	k1gInSc1	Pijoan
<g/>
,	,	kIx,	,
José	José	k1gNnPc1	José
<g/>
,	,	kIx,	,
Dějiny	dějiny	k1gFnPc1	dějiny
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
5	[number]	k4	5
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
s.	s.	k?	s.
239	[number]	k4	239
<g/>
–	–	k?	–
<g/>
270	[number]	k4	270
</s>
</p>
<p>
<s>
Togner	Togner	k1gMnSc1	Togner
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticell	k1gFnSc5	Botticell
<g/>
,	,	kIx,	,
Datel	datel	k1gMnSc1	datel
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Renesanční	renesanční	k2eAgNnSc1d1	renesanční
malířství	malířství	k1gNnSc1	malířství
</s>
</p>
<p>
<s>
Italská	italský	k2eAgFnSc1d1	italská
renesance	renesance	k1gFnSc1	renesance
</s>
</p>
<p>
<s>
Florencie	Florencie	k1gFnSc1	Florencie
</s>
</p>
<p>
<s>
Florentská	florentský	k2eAgFnSc1d1	florentská
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Filippo	Filippa	k1gFnSc5	Filippa
Lippi	Lipp	k1gFnSc5	Lipp
</s>
</p>
<p>
<s>
Madona	Madona	k1gFnSc1	Madona
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticelle	k1gFnSc6	Botticelle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticelle	k1gFnSc6	Botticelle
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticell	k1gFnSc5	Botticell
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
a	a	k8xC	a
16	[number]	k4	16
reprodukcí	reprodukce	k1gFnPc2	reprodukce
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
malíři	malíř	k1gMnSc3	malíř
<g/>
,	,	kIx,	,
reprodukce	reprodukce	k1gFnSc1	reprodukce
všech	všecek	k3xTgInPc2	všecek
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
<g/>
,	,	kIx,	,
reprodukce	reprodukce	k1gFnSc1	reprodukce
<g/>
,	,	kIx,	,
bibliografie	bibliografie	k1gFnSc1	bibliografie
<g/>
,	,	kIx,	,
recepce	recepce	k1gFnSc1	recepce
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc4	heslo
na	na	k7c4	na
Encyclopaedia	Encyclopaedium	k1gNnPc4	Encyclopaedium
Brittanica	Brittanicum	k1gNnSc2	Brittanicum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Malíř	malíř	k1gMnSc1	malíř
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
věnované	věnovaný	k2eAgFnSc6d1	věnovaná
italskému	italský	k2eAgNnSc3d1	italské
renesančnímu	renesanční	k2eAgNnSc3d1	renesanční
umění	umění	k1gNnSc3	umění
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
