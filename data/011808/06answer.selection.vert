<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
di	di	k?	di
Mariano	Mariana	k1gFnSc5	Mariana
Filipepi	Filipep	k1gFnSc5	Filipep
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Sandro	Sandra	k1gFnSc5	Sandra
Botticelli	Botticell	k1gMnSc6	Botticell
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1444	[number]	k4	1444
Florencie	Florencie	k1gFnSc1	Florencie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
–	–	k?	–
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1510	[number]	k4	1510
<g/>
,	,	kIx,	,
tamtéž	tamtéž	k6eAd1	tamtéž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
renesanční	renesanční	k2eAgMnSc1d1	renesanční
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
florentské	florentský	k2eAgFnSc2d1	florentská
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
