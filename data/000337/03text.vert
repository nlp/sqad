<s>
Temelín	Temelín	k1gInSc1	Temelín
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
rozlohu	rozloha	k1gFnSc4	rozloha
50,41	[number]	k4	50,41
km2	km2	k4	km2
a	a	k8xC	a
v	v	k7c6	v
11	[number]	k4	11
místních	místní	k2eAgFnPc6d1	místní
částech	část	k1gFnPc6	část
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
822	[number]	k4	822
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
421	[number]	k4	421
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
401	[number]	k4	401
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
PSČ	PSČ	kA	PSČ
zdejší	zdejší	k2eAgFnSc2d1	zdejší
pošty	pošta	k1gFnSc2	pošta
je	být	k5eAaImIp3nS	být
373	[number]	k4	373
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
zřejmě	zřejmě	k6eAd1	zřejmě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
přivlastňovací	přivlastňovací	k2eAgFnSc7d1	přivlastňovací
příponou	přípona	k1gFnSc7	přípona
-ín	-ín	k?	-ín
z	z	k7c2	z
rodného	rodný	k2eAgNnSc2d1	rodné
jména	jméno	k1gNnSc2	jméno
Temela	Temela	k1gFnSc1	Temela
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
blízké	blízký	k2eAgNnSc4d1	blízké
staroslověnské	staroslověnský	k2eAgNnSc4d1	staroslověnské
slovo	slovo	k1gNnSc4	slovo
temelъ	temelъ	k?	temelъ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
základ	základ	k1gInSc4	základ
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
podklad	podklad	k1gInSc1	podklad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
jihoslovanských	jihoslovanský	k2eAgInPc2d1	jihoslovanský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
slova	slovo	k1gNnSc2	slovo
temelj	temelj	k1gInSc1	temelj
<g/>
,	,	kIx,	,
temeljiti	temeljit	k5eAaPmF	temeljit
<g/>
,	,	kIx,	,
temeljan	temeljan	k1gInSc1	temeljan
<g/>
,	,	kIx,	,
temeljac	temeljac	k1gInSc1	temeljac
(	(	kIx(	(
<g/>
základ	základ	k1gInSc1	základ
<g/>
,	,	kIx,	,
zakládat	zakládat	k5eAaImF	zakládat
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc1d1	základní
<g/>
)	)	kIx)	)
v	v	k7c6	v
chorvatštině	chorvatština	k1gFnSc6	chorvatština
i	i	k9	i
obdobné	obdobný	k2eAgInPc4d1	obdobný
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
již	již	k6eAd1	již
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
<g/>
)	)	kIx)	)
slovo	slovo	k1gNnSc1	slovo
temel	temela	k1gFnPc2	temela
v	v	k7c6	v
bulharštině	bulharština	k1gFnSc6	bulharština
<g/>
.	.	kIx.	.
</s>
<s>
Temelín	Temelín	k1gInSc1	Temelín
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
Okrese	okres	k1gInSc6	okres
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
asi	asi	k9	asi
5,5	[number]	k4	5,5
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
a	a	k8xC	a
asi	asi	k9	asi
24	[number]	k4	24
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
senátní	senátní	k2eAgFnPc4d1	senátní
volby	volba	k1gFnPc4	volba
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
č.	č.	k?	č.
10	[number]	k4	10
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
taky	taky	k6eAd1	taky
součástí	součást	k1gFnSc7	součást
tzv.	tzv.	kA	tzv.
Mikroregionu	mikroregion	k1gInSc2	mikroregion
Vltavotýnsko	Vltavotýnsko	k1gNnSc4	Vltavotýnsko
resp.	resp.	kA	resp.
Sdružení	sdružení	k1gNnSc1	sdružení
měst	město	k1gNnPc2	město
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
Vltava	Vltava	k1gFnSc1	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vsi	ves	k1gFnSc6	ves
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1381	[number]	k4	1381
<g/>
.	.	kIx.	.
</s>
<s>
Temelín	Temelín	k1gInSc1	Temelín
byl	být	k5eAaImAgInS	být
zmiňován	zmiňovat	k5eAaImNgInS	zmiňovat
i	i	k9	i
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Šíma	Šíma	k1gMnSc1	Šíma
<g/>
...	...	k?	...
z	z	k7c2	z
Tajna	Tajna	k?	Tajna
[	[	kIx(	[
<g/>
tedy	tedy	k8xC	tedy
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
]	]	kIx)	]
vymluvil	vymluvit	k5eAaPmAgMnS	vymluvit
z	z	k7c2	z
práva	právo	k1gNnSc2	právo
ze	z	k7c2	z
vsi	ves	k1gFnSc2	ves
Temelína	Temelín	k1gInSc2	Temelín
Václava	Václav	k1gMnSc2	Václav
Kolmánka	Kolmánek	k1gMnSc2	Kolmánek
<g/>
,	,	kIx,	,
písaře	písař	k1gMnSc2	písař
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
k	k	k7c3	k
dohledání	dohledání	k1gNnSc3	dohledání
v	v	k7c6	v
ArchČ	ArchČ	k1gFnSc6	ArchČ
<g/>
9,548	[number]	k4	9,548
(	(	kIx(	(
<g/>
1491	[number]	k4	1491
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Temelín	Temelín	k1gInSc1	Temelín
dokonce	dokonce	k9	dokonce
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Petr	Petr	k1gMnSc1	Petr
Chelčický	Chelčický	k2eAgMnSc1d1	Chelčický
v	v	k7c6	v
Sieti	Sieť	k1gFnSc6	Sieť
viery	viera	k1gFnSc2	viera
pravé	pravý	k2eAgFnSc2d1	pravá
...	...	k?	...
<g/>
domněním	domnění	k1gNnSc7	domnění
kuzedlníkuov	kuzedlníkuov	k1gInSc1	kuzedlníkuov
a	a	k8xC	a
hadačuov	hadačuov	k1gInSc1	hadačuov
hledajie	hledajie	k1gFnSc1	hledajie
[	[	kIx(	[
<g/>
postižení	postižený	k2eAgMnPc1d1	postižený
lidé	člověk	k1gMnPc1	člověk
<g/>
]	]	kIx)	]
v	v	k7c6	v
času	čas	k1gInSc2	čas
pokušení	pokušení	k1gNnPc2	pokušení
svých	svůj	k3xOyFgMnPc2	svůj
<g/>
,	,	kIx,	,
túž	túž	k?	túž
vážnost	vážnost	k1gFnSc4	vážnost
kúzedlníkóm	kúzedlníkóm	k6eAd1	kúzedlníkóm
majíce	mít	k5eAaImSgMnP	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
při	pře	k1gFnSc6	pře
jako	jako	k9	jako
k	k	k7c3	k
svatým	svatý	k2eAgFnPc3d1	svatá
<g/>
,	,	kIx,	,
též	též	k9	též
do	do	k7c2	do
Kyjova	Kyjov	k1gInSc2	Kyjov
k	k	k7c3	k
matce	matka	k1gFnSc3	matka
boží	božit	k5eAaImIp3nP	božit
<g/>
,	,	kIx,	,
do	do	k7c2	do
Temelína	Temelín	k1gInSc2	Temelín
k	k	k7c3	k
kúzedlníku	kúzedlník	k1gInSc3	kúzedlník
<g/>
...	...	k?	...
ktož	ktož	k6eAd1	ktož
by	by	kYmCp3nS	by
koli	koli	k6eAd1	koli
polehčil	polehčit	k5eAaPmAgMnS	polehčit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
k	k	k7c3	k
dohledání	dohledání	k1gNnSc3	dohledání
v	v	k7c4	v
33	[number]	k4	33
<g/>
.	.	kIx.	.
kapitole	kapitola	k1gFnSc6	kapitola
CC	CC	kA	CC
13	[number]	k4	13
b	b	k?	b
<g/>
)	)	kIx)	)
Temelín	Temelín	k1gInSc1	Temelín
i	i	k8xC	i
Temelínec	Temelínec	k1gMnSc1	Temelínec
se	se	k3xPyFc4	se
vyskytujou	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
urbáři	urbář	k1gInSc6	urbář
hlubockého	hlubocký	k2eAgNnSc2d1	hlubocké
panství	panství	k1gNnSc2	panství
(	(	kIx(	(
<g/>
ArchČ	ArchČ	k1gFnSc1	ArchČ
17,357	[number]	k4	17,357
(	(	kIx(	(
<g/>
1490	[number]	k4	1490
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
115	[number]	k4	115
domech	dům	k1gInPc6	dům
557	[number]	k4	557
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
551	[number]	k4	551
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
5	[number]	k4	5
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
505	[number]	k4	505
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
35	[number]	k4	35
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
5	[number]	k4	5
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
sousedství	sousedství	k1gNnSc6	sousedství
Temelína	Temelín	k1gInSc2	Temelín
(	(	kIx(	(
<g/>
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stávala	stávat	k5eAaImAgFnS	stávat
vesnice	vesnice	k1gFnSc1	vesnice
Temelínec	Temelínec	k1gInSc1	Temelínec
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
areál	areál	k1gInSc1	areál
Jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
JETE	JETE	kA	JETE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chladicí	chladicí	k2eAgFnPc1d1	chladicí
věže	věž	k1gFnPc1	věž
elektrárny	elektrárna	k1gFnSc2	elektrárna
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
celého	celý	k2eAgNnSc2d1	celé
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
50	[number]	k4	50
km	km	kA	km
<g/>
,	,	kIx,	,
pára	pára	k1gFnSc1	pára
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
až	až	k9	až
ze	z	k7c2	z
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
70	[number]	k4	70
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Temelíně	Temelín	k1gInSc6	Temelín
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
obce	obec	k1gFnSc2	obec
vedou	vést	k5eAaImIp3nP	vést
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
z	z	k7c2	z
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
i	i	k9	i
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
obě	dva	k4xCgFnPc1	dva
města	město	k1gNnSc2	město
spojovala	spojovat	k5eAaImAgFnS	spojovat
a	a	k8xC	a
dopravovala	dopravovat	k5eAaImAgFnS	dopravovat
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
JETE	JETE	kA	JETE
<g/>
,	,	kIx,	,
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
záměru	záměr	k1gInSc2	záměr
však	však	k9	však
sešlo	sejít	k5eAaPmAgNnS	sejít
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Temelín	Temelín	k1gInSc1	Temelín
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
192	[number]	k4	192
Číčenice	Číčenice	k1gFnSc1	Číčenice
-	-	kIx~	-
Týn	Týn	k1gInSc1	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Temelín	Temelín	k1gInSc1	Temelín
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
formálně	formálně	k6eAd1	formálně
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
částí	část	k1gFnPc2	část
na	na	k7c6	na
deseti	deset	k4xCc6	deset
katastrálních	katastrální	k2eAgNnPc6d1	katastrální
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
částí	část	k1gFnPc2	část
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
zaniklá	zaniklý	k2eAgNnPc4d1	zaniklé
sídla	sídlo	k1gNnPc4	sídlo
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
základní	základní	k2eAgFnSc2d1	základní
sídelní	sídelní	k2eAgFnSc2d1	sídelní
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
)	)	kIx)	)
Březí	březí	k1gNnSc1	březí
u	u	k7c2	u
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
(	(	kIx(	(
<g/>
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
i	i	k8xC	i
název	název	k1gInSc1	název
k.ú.	k.ú.	k?	k.ú.
<g/>
)	)	kIx)	)
Knín	Knín	k1gMnSc1	Knín
(	(	kIx(	(
<g/>
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Kočín	Kočín	k1gMnSc1	Kočín
(	(	kIx(	(
<g/>
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Křtěnov	Křtěnovo	k1gNnPc2	Křtěnovo
(	(	kIx(	(
<g/>
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Lhota	Lhota	k1gFnSc1	Lhota
pod	pod	k7c7	pod
Horami	hora	k1gFnPc7	hora
(	(	kIx(	(
<g/>
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Litoradlice	Litoradlice	k1gFnSc2	Litoradlice
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Podhájí	Podhájí	k1gNnSc1	Podhájí
(	(	kIx(	(
<g/>
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
k.	k.	k?	k.
ú.	ú.	k?	ú.
Březí	březí	k1gNnSc1	březí
u	u	k7c2	u
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
)	)	kIx)	)
Sedlec	Sedlec	k1gInSc1	Sedlec
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Sedlec	Sedlec	k1gInSc1	Sedlec
u	u	k7c2	u
Temelína	Temelín	k1gInSc2	Temelín
<g/>
)	)	kIx)	)
Temelín	Temelín	k1gInSc1	Temelín
(	(	kIx(	(
<g/>
Kaliště	kaliště	k1gNnSc1	kaliště
<g/>
,	,	kIx,	,
Temelín	Temelín	k1gInSc1	Temelín
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
i	i	k9	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Temelínec	Temelínec	k1gMnSc1	Temelínec
(	(	kIx(	(
<g/>
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
ves	ves	k1gFnSc1	ves
<g/>
,	,	kIx,	,
i	i	k8xC	i
název	název	k1gInSc1	název
k.	k.	k?	k.
ú.	ú.	k?	ú.
<g/>
)	)	kIx)	)
Zvěrkovice	Zvěrkovice	k1gFnSc1	Zvěrkovice
(	(	kIx(	(
<g/>
Záluží	Záluží	k1gNnSc1	Záluží
<g/>
,	,	kIx,	,
Zvěrkovice	Zvěrkovice	k1gFnSc1	Zvěrkovice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
k.	k.	k?	k.
ú.	ú.	k?	ú.
Zvěrkovice	Zvěrkovice	k1gFnSc2	Zvěrkovice
u	u	k7c2	u
Týna	Týn	k1gInSc2	Týn
nad	nad	k7c7	nad
Vltavou	Vltava	k1gFnSc7	Vltava
<g/>
)	)	kIx)	)
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
elektrárna	elektrárna	k1gFnSc1	elektrárna
Temelín	Temelín	k1gInSc1	Temelín
Dukovany	Dukovany	k1gInPc1	Dukovany
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Temelín	Temelín	k1gInSc1	Temelín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Temelín	Temelín	k1gInSc1	Temelín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
obce	obec	k1gFnSc2	obec
</s>
