<s>
Víla	víla	k1gFnSc1	víla
<g/>
,	,	kIx,	,
též	též	k9	též
rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
divoženka	divoženka	k1gFnSc1	divoženka
nebo	nebo	k8xC	nebo
bosorka	bosorka	k1gFnSc1	bosorka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
bytost	bytost	k1gFnSc1	bytost
ženského	ženský	k2eAgNnSc2d1	ženské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
již	již	k9	již
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obdobách	obdoba	k1gFnPc6	obdoba
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
Evropy	Evropa	k1gFnSc2	Evropa
už	už	k6eAd1	už
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
výraz	výraz	k1gInSc1	výraz
víla	víla	k1gFnSc1	víla
je	být	k5eAaImIp3nS	být
slovanský	slovanský	k2eAgMnSc1d1	slovanský
ale	ale	k8xC	ale
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
pantheonech	pantheon	k1gInPc6	pantheon
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
nixy	nixa	k1gFnSc2	nixa
(	(	kIx(	(
<g/>
u	u	k7c2	u
Germánů	Germán	k1gMnPc2	Germán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sidhe	sidhat	k5eAaPmIp3nS	sidhat
(	(	kIx(	(
<g/>
u	u	k7c2	u
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
)	)	kIx)	)
a	a	k8xC	a
nymfy	nymfa	k1gFnSc2	nymfa
(	(	kIx(	(
<g/>
u	u	k7c2	u
Řeků	Řek	k1gMnPc2	Řek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podobnými	podobný	k2eAgFnPc7d1	podobná
bytostmi	bytost	k1gFnPc7	bytost
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
mořské	mořský	k2eAgFnPc1d1	mořská
panny	panna	k1gFnPc1	panna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
používalo	používat	k5eAaImAgNnS	používat
starofrancouzské	starofrancouzský	k2eAgNnSc1d1	starofrancouzské
slovo	slovo	k1gNnSc1	slovo
fae	fae	k?	fae
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
vychází	vycházet	k5eAaImIp3nS	vycházet
dnešní	dnešní	k2eAgFnSc2d1	dnešní
anglické	anglický	k2eAgFnSc2d1	anglická
fairy	faira	k1gFnSc2	faira
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
měly	mít	k5eAaImAgFnP	mít
víly	víla	k1gFnPc1	víla
velký	velký	k2eAgInSc1d1	velký
náboženský	náboženský	k2eAgInSc1d1	náboženský
význam	význam	k1gInSc1	význam
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Slovanský	slovanský	k2eAgInSc1d1	slovanský
výraz	výraz	k1gInSc1	výraz
víla	víla	k1gFnSc1	víla
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
vila	vila	k1gFnSc1	vila
<g/>
,	,	kIx,	,
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
samovila	samovit	k5eAaImAgFnS	samovit
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
viliti	vilit	k5eAaBmF	vilit
-	-	kIx~	-
být	být	k5eAaImF	být
posedlý	posedlý	k2eAgMnSc1d1	posedlý
<g/>
,	,	kIx,	,
bláznivý	bláznivý	k2eAgInSc1d1	bláznivý
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
divoženka	divoženka	k1gFnSc1	divoženka
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
divá	divý	k2eAgFnSc1d1	divá
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
divá	divý	k2eAgFnSc1d1	divá
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
může	moct	k5eAaImIp3nS	moct
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c4	na
slovo	slovo	k1gNnSc4	slovo
diva	div	k1gMnSc2	div
nebo	nebo	k8xC	nebo
samodiva	samodiv	k1gMnSc2	samodiv
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
indoevropský	indoevropský	k2eAgInSc1d1	indoevropský
kořen	kořen	k1gInSc1	kořen
(	(	kIx(	(
<g/>
srov.	srov.	kA	srov.
keltské	keltský	k2eAgInPc1d1	keltský
dive	div	k1gMnSc5	div
<g/>
,	,	kIx,	,
divone	divon	k1gMnSc5	divon
<g/>
,	,	kIx,	,
latinské	latinský	k2eAgInPc4d1	latinský
divus	divus	k1gInSc4	divus
<g/>
,	,	kIx,	,
sanskrtské	sanskrtský	k2eAgFnPc1d1	sanskrtská
déva	déva	k6eAd1	déva
-	-	kIx~	-
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
<g/>
,	,	kIx,	,
božská	božský	k2eAgFnSc1d1	božská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
rusalka	rusalka	k1gFnSc1	rusalka
zřejmě	zřejmě	k6eAd1	zřejmě
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
buď	buď	k8xC	buď
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
rusá	rusý	k2eAgFnSc1d1	rusá
(	(	kIx(	(
<g/>
rusovlasá	rusovlasý	k2eAgFnSc1d1	rusovlasá
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
rusa	rus	k1gMnSc2	rus
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
řeka	řeka	k1gFnSc1	řeka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
známé	známý	k2eAgFnPc1d1	známá
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
vílách	víla	k1gFnPc6	víla
u	u	k7c2	u
Slovanů	Slovan	k1gInPc2	Slovan
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
psal	psát	k5eAaImAgMnS	psát
Prokopius	Prokopius	k1gMnSc1	Prokopius
o	o	k7c6	o
uctívání	uctívání	k1gNnSc6	uctívání
"	"	kIx"	"
<g/>
nymf	nymfa	k1gFnPc2	nymfa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
slovanské	slovanský	k2eAgNnSc4d1	slovanské
označení	označení	k1gNnSc4	označení
ale	ale	k8xC	ale
nezaznamenal	zaznamenat	k5eNaPmAgInS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
jsou	být	k5eAaImIp3nP	být
víly	víla	k1gFnPc1	víla
uváděny	uvádět	k5eAaImNgFnP	uvádět
již	již	k6eAd1	již
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
jejich	jejich	k3xOp3gNnSc2	jejich
uctívání	uctívání	k1gNnSc2	uctívání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
folklóru	folklór	k1gInSc2	folklór
ale	ale	k9	ale
byly	být	k5eAaImAgInP	být
vytlačeny	vytlačen	k2eAgInPc1d1	vytlačen
rusalkami	rusalka	k1gFnPc7	rusalka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
máme	mít	k5eAaImIp1nP	mít
zprávy	zpráva	k1gFnPc4	zpráva
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
jediný	jediný	k2eAgInSc1d1	jediný
starší	starý	k2eAgInSc1d2	starší
(	(	kIx(	(
<g/>
předknižní	předknižní	k2eAgInSc1d1	předknižní
<g/>
)	)	kIx)	)
doklad	doklad	k1gInSc1	doklad
o	o	k7c4	o
působení	působení	k1gNnSc4	působení
víl	víla	k1gFnPc2	víla
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
o	o	k7c6	o
Jětřichu	Jětřich	k1gInSc6	Jětřich
Berúnském	Berúnský	k2eAgInSc6d1	Berúnský
z	z	k7c2	z
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
víry	víra	k1gFnSc2	víra
ve	v	k7c4	v
víly	víla	k1gFnPc4	víla
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
;	;	kIx,	;
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
reliktem	relikt	k1gInSc7	relikt
původních	původní	k2eAgNnPc2d1	původní
božstev	božstvo	k1gNnPc2	božstvo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
uctívána	uctívat	k5eAaImNgNnP	uctívat
<g/>
,	,	kIx,	,
než	než	k8xS	než
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgInPc4d1	známý
pohanské	pohanský	k2eAgInPc4d1	pohanský
pantheony	pantheon	k1gInPc4	pantheon
nebo	nebo	k8xC	nebo
až	až	k9	až
představou	představa	k1gFnSc7	představa
pozdější	pozdní	k2eAgFnSc7d2	pozdější
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
teorie	teorie	k1gFnSc1	teorie
říká	říkat	k5eAaImIp3nS	říkat
že	že	k8xS	že
v	v	k7c6	v
původním	původní	k2eAgNnSc6d1	původní
pojetí	pojetí	k1gNnSc6	pojetí
byly	být	k5eAaImAgFnP	být
víly	víla	k1gFnPc1	víla
čarodějkami	čarodějka	k1gFnPc7	čarodějka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
až	až	k9	až
později	pozdě	k6eAd2	pozdě
získaly	získat	k5eAaPmAgFnP	získat
nadpřirozenou	nadpřirozený	k2eAgFnSc4d1	nadpřirozená
podstatu	podstata	k1gFnSc4	podstata
<g/>
.	.	kIx.	.
</s>
<s>
Představa	představa	k1gFnSc1	představa
že	že	k8xS	že
víly	víla	k1gFnPc1	víla
jsou	být	k5eAaImIp3nP	být
duše	duše	k1gFnPc4	duše
předčasně	předčasně	k6eAd1	předčasně
zemřelých	zemřelý	k1gMnPc2	zemřelý
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
těch	ten	k3xDgInPc2	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
spáchaly	spáchat	k5eAaPmAgFnP	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
či	či	k8xC	či
se	se	k3xPyFc4	se
utopily	utopit	k5eAaPmAgInP	utopit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pozdní	pozdní	k2eAgFnSc1d1	pozdní
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejspíše	nejspíše	k9	nejspíše
až	až	k9	až
pod	pod	k7c7	pod
křesťanským	křesťanský	k2eAgInSc7d1	křesťanský
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc4d1	podobný
bludičkám	bludička	k1gFnPc3	bludička
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
představou	představa	k1gFnSc7	představa
víl	víla	k1gFnPc2	víla
jako	jako	k8xS	jako
duší	duše	k1gFnPc2	duše
zemřelých	zemřelý	k1gMnPc2	zemřelý
souvisí	souviset	k5eAaImIp3nP	souviset
i	i	k9	i
bytosti	bytost	k1gFnPc1	bytost
zvané	zvaný	k2eAgFnSc2d1	zvaná
navky	navka	k1gFnSc2	navka
nebo	nebo	k8xC	nebo
mavky	mavka	k1gFnSc2	mavka
známé	známý	k2eAgFnSc2d1	známá
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
jméno	jméno	k1gNnSc4	jméno
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Staroruského	staroruský	k2eAgInSc2d1	staroruský
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
Nav	navit	k5eAaImRp2nS	navit
<g/>
́	́	k?	́
<g/>
[	[	kIx(	[
<g/>
Н	Н	k?	Н
<g/>
]	]	kIx)	]
<g/>
"	"	kIx"	"
jež	jenž	k3xRgNnSc1	jenž
označoval	označovat	k5eAaImAgInS	označovat
mrtvého	mrtvý	k1gMnSc4	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
nepokřtěných	pokřtěný	k2eNgFnPc2d1	nepokřtěná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
utopily	utopit	k5eAaPmAgFnP	utopit
vlastní	vlastní	k2eAgFnPc4d1	vlastní
matky	matka	k1gFnPc4	matka
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
podobu	podoba	k1gFnSc4	podoba
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
či	či	k8xC	či
krásných	krásný	k2eAgFnPc2d1	krásná
polonahých	polonahý	k2eAgFnPc2d1	polonahá
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ubližují	ubližovat	k5eAaImIp3nP	ubližovat
lidem	člověk	k1gMnPc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
nimi	on	k3xPp3gMnPc7	on
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
nikdo	nikdo	k3yNnSc1	nikdo
nesmiluje	smilovat	k5eNaPmIp3nS	smilovat
<g/>
,	,	kIx,	,
promění	proměnit	k5eAaPmIp3nS	proměnit
se	se	k3xPyFc4	se
v	v	k7c4	v
rusalky	rusalka	k1gFnPc4	rusalka
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
bytosti	bytost	k1gFnPc1	bytost
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
(	(	kIx(	(
<g/>
navjak	navjak	k1gMnSc1	navjak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
(	(	kIx(	(
<g/>
navje	navj	k1gInSc2	navj
<g/>
)	)	kIx)	)
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
(	(	kIx(	(
<g/>
látawec	látawec	k1gMnSc1	látawec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tradici	tradice	k1gFnSc6	tradice
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
po	po	k7c6	po
rusalkách	rusalka	k1gFnPc6	rusalka
nazývá	nazývat	k5eAaImIp3nS	nazývat
sedmý	sedmý	k4xOgInSc4	sedmý
týden	týden	k1gInSc4	týden
po	po	k7c6	po
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
rusalný	rusalný	k2eAgInSc1d1	rusalný
týden	týden	k1gInSc1	týden
<g/>
"	"	kIx"	"
a	a	k8xC	a
slavnost	slavnost	k1gFnSc1	slavnost
"	"	kIx"	"
<g/>
rusalje	rusalje	k1gFnSc1	rusalje
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
obdobou	obdoba	k1gFnSc7	obdoba
Dušiček	Dušičky	k1gFnPc2	Dušičky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pohanské	pohanský	k2eAgFnSc6d1	pohanská
době	doba	k1gFnSc6	doba
však	však	k9	však
nejspíš	nejspíš	k9	nejspíš
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgFnPc1d1	známa
víly	víla	k1gFnPc1	víla
horské	horský	k2eAgFnPc4d1	horská
(	(	kIx(	(
<g/>
vile	vila	k1gFnSc3	vila
planinkinje	planinkinj	k1gFnSc2	planinkinj
<g/>
,	,	kIx,	,
samovile	samovile	k6eAd1	samovile
samogorske	samogorske	k6eAd1	samogorske
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
žily	žít	k5eAaImAgFnP	žít
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
a	a	k8xC	a
uměly	umět	k5eAaImAgFnP	umět
se	se	k3xPyFc4	se
měnit	měnit	k5eAaImF	měnit
v	v	k7c4	v
hady	had	k1gMnPc4	had
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
obdobu	obdoba	k1gFnSc4	obdoba
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
vládkyni	vládkyně	k1gFnSc4	vládkyně
jeskyní	jeskyně	k1gFnPc2	jeskyně
a	a	k8xC	a
dolů	dol	k1gInPc2	dol
Runu	run	k1gInSc2	run
(	(	kIx(	(
<g/>
též	též	k9	též
zemná	zemný	k2eAgFnSc1d1	zemný
paní	paní	k1gFnSc1	paní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
ženu	hnát	k5eAaImIp1nS	hnát
vládce	vládce	k1gMnSc1	vládce
podzemí	podzemí	k1gNnSc2	podzemí
Kovlada	Kovlada	k1gFnSc1	Kovlada
a	a	k8xC	a
královnu	královna	k1gFnSc4	královna
permoníků	permoník	k1gMnPc2	permoník
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
byly	být	k5eAaImAgFnP	být
známy	znám	k2eAgFnPc1d1	známa
víly	víla	k1gFnPc1	víla
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
(	(	kIx(	(
<g/>
samovile	samovile	k6eAd1	samovile
oblankinje	oblankinj	k1gMnSc2	oblankinj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
pohybovaly	pohybovat	k5eAaImAgFnP	pohybovat
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
měly	mít	k5eAaImAgInP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
počasí	počasí	k1gNnSc4	počasí
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
oblačnost	oblačnost	k1gFnSc4	oblačnost
a	a	k8xC	a
bouře	bouř	k1gFnPc4	bouř
<g/>
,	,	kIx,	,
blesky	blesk	k1gInPc1	blesk
byly	být	k5eAaImAgInP	být
jejich	jejich	k3xOp3gInPc1	jejich
šípy	šíp	k1gInPc1	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Rusové	Rus	k1gMnPc1	Rus
zase	zase	k9	zase
znali	znát	k5eAaImAgMnP	znát
tři	tři	k4xCgFnPc4	tři
sestry	sestra	k1gFnPc4	sestra
větru	vítr	k1gInSc2	vítr
Burju	Burju	k1gFnPc2	Burju
(	(	kIx(	(
<g/>
bouři	bouře	k1gFnSc4	bouře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mjatel	Mjatel	k1gMnSc1	Mjatel
(	(	kIx(	(
<g/>
metelici	metelice	k1gFnSc4	metelice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vjugu	Vjuga	k1gFnSc4	Vjuga
(	(	kIx(	(
<g/>
vánici	vánice	k1gFnSc4	vánice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
zase	zase	k9	zase
Meluzínu	Meluzína	k1gFnSc4	Meluzína
či	či	k8xC	či
Větrnici	větrnice	k1gFnSc4	větrnice
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
bytost	bytost	k1gFnSc1	bytost
v	v	k7c6	v
bílém	bílý	k2eAgNnSc6d1	bílé
rouše	roucho	k1gNnSc6	roucho
naříkající	naříkající	k2eAgFnPc1d1	naříkající
v	v	k7c6	v
komínech	komín	k1gInPc6	komín
a	a	k8xC	a
na	na	k7c6	na
hrobech	hrob	k1gInPc6	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
víly	víla	k1gFnPc1	víla
lesní	lesní	k2eAgFnPc4d1	lesní
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
žínky	žínka	k1gFnPc4	žínka
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgFnPc4d1	žijící
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
popisované	popisovaný	k2eAgFnPc4d1	popisovaná
jako	jako	k8xS	jako
krásné	krásný	k2eAgFnPc4d1	krásná
průsvitné	průsvitný	k2eAgFnPc4d1	průsvitná
dívky	dívka	k1gFnPc4	dívka
v	v	k7c6	v
lehkých	lehký	k2eAgInPc6d1	lehký
šatech	šat	k1gInPc6	šat
se	s	k7c7	s
zlatými	zlatý	k2eAgInPc7d1	zlatý
či	či	k8xC	či
rusými	rusý	k2eAgInPc7d1	rusý
vlasy	vlas	k1gInPc7	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgFnP	být
zdrojem	zdroj	k1gInSc7	zdroj
jejich	jejich	k3xOp3gFnSc2	jejich
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Uměly	umět	k5eAaImAgFnP	umět
se	se	k3xPyFc4	se
měnit	měnit	k5eAaImF	měnit
v	v	k7c4	v
různá	různý	k2eAgNnPc4d1	různé
zvířata	zvíře	k1gNnPc4	zvíře
například	například	k6eAd1	například
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
vlka	vlk	k1gMnSc4	vlk
<g/>
,	,	kIx,	,
sokola	sokol	k1gMnSc4	sokol
či	či	k8xC	či
labuť	labuť	k1gFnSc4	labuť
a	a	k8xC	a
vyznaly	vyznat	k5eAaPmAgFnP	vyznat
se	se	k3xPyFc4	se
v	v	k7c4	v
léčení	léčení	k1gNnSc4	léčení
a	a	k8xC	a
věštění	věštění	k1gNnSc4	věštění
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vyjížděly	vyjíždět	k5eAaImAgInP	vyjíždět
na	na	k7c4	na
lov	lov	k1gInSc4	lov
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
či	či	k8xC	či
jelenu	jelena	k1gFnSc4	jelena
nebo	nebo	k8xC	nebo
tančily	tančit	k5eAaImAgInP	tančit
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
a	a	k8xC	a
zpívaly	zpívat	k5eAaImAgInP	zpívat
<g/>
.	.	kIx.	.
</s>
<s>
Zjevovaly	zjevovat	k5eAaImAgFnP	zjevovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
když	když	k8xS	když
padala	padat	k5eAaImAgFnS	padat
rosa	rosa	k1gFnSc1	rosa
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
tvořila	tvořit	k5eAaImAgFnS	tvořit
duha	duha	k1gFnSc1	duha
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnPc1d1	vodní
víly	víla	k1gFnPc1	víla
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xS	jako
rusalky	rusalka	k1gFnPc1	rusalka
<g/>
,	,	kIx,	,
obývají	obývat	k5eAaImIp3nP	obývat
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
řeky	řeka	k1gFnSc2	řeka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
vodníkům	vodník	k1gMnPc3	vodník
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
vlasy	vlas	k1gInPc1	vlas
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
mokré	mokrý	k2eAgFnPc4d1	mokrá
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
rusalka	rusalka	k1gFnSc1	rusalka
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
víla	víla	k1gFnSc1	víla
může	moct	k5eAaImIp3nS	moct
rozčesáváním	rozčesávání	k1gNnSc7	rozčesávání
svých	svůj	k3xOyFgInPc2	svůj
vlasů	vlas	k1gInPc2	vlas
způsobit	způsobit	k5eAaPmF	způsobit
i	i	k9	i
povodeň	povodeň	k1gFnSc1	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
také	také	k9	také
víly	víla	k1gFnPc1	víla
zlé	zlý	k2eAgFnSc2d1	zlá
zvané	zvaný	k2eAgFnSc2d1	zvaná
judy	judo	k1gNnPc7	judo
nebo	nebo	k8xC	nebo
jezinky	jezinka	k1gFnPc1	jezinka
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
sice	sice	k8xC	sice
krásné	krásný	k2eAgFnPc1d1	krásná
a	a	k8xC	a
obývaly	obývat	k5eAaImAgFnP	obývat
vodu	voda	k1gFnSc4	voda
či	či	k8xC	či
les	les	k1gInSc4	les
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lidi	člověk	k1gMnPc4	člověk
topily	topit	k5eAaImAgFnP	topit
a	a	k8xC	a
sváděly	svádět	k5eAaImAgFnP	svádět
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
bývají	bývat	k5eAaImIp3nP	bývat
nazývané	nazývaný	k2eAgFnPc4d1	nazývaná
bosorky	bosorka	k1gFnPc4	bosorka
<g/>
.	.	kIx.	.
</s>
<s>
Divoženkami	divoženka	k1gFnPc7	divoženka
či	či	k8xC	či
divými	divý	k2eAgFnPc7d1	divá
ženami	žena	k1gFnPc7	žena
bývají	bývat	k5eAaImIp3nP	bývat
nazývány	nazýván	k2eAgFnPc4d1	nazývána
ošklivé	ošklivý	k2eAgFnPc4d1	ošklivá
a	a	k8xC	a
neupravené	upravený	k2eNgFnPc4d1	neupravená
víly	víla	k1gFnPc4	víla
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
často	často	k6eAd1	často
kradly	krást	k5eAaImAgFnP	krást
lidské	lidský	k2eAgFnPc4d1	lidská
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
vyměňovaly	vyměňovat	k5eAaImAgFnP	vyměňovat
je	on	k3xPp3gNnSc4	on
za	za	k7c4	za
svoje	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Obývají	obývat	k5eAaImIp3nP	obývat
lesy	les	k1gInPc4	les
nebo	nebo	k8xC	nebo
jeskyně	jeskyně	k1gFnPc4	jeskyně
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
nazývány	nazýván	k2eAgFnPc1d1	nazývána
jeskyňky	jeskyňka	k1gFnPc1	jeskyňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1	specifická
skupinou	skupina	k1gFnSc7	skupina
víl	víla	k1gFnPc2	víla
jsou	být	k5eAaImIp3nP	být
sudičky	sudička	k1gFnPc1	sudička
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
tři	tři	k4xCgMnPc1	tři
a	a	k8xC	a
zjevují	zjevovat	k5eAaImIp3nP	zjevovat
se	se	k3xPyFc4	se
u	u	k7c2	u
novorozeněte	novorozeně	k1gNnSc2	novorozeně
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
předpovídají	předpovídat	k5eAaImIp3nP	předpovídat
osud	osud	k1gInSc4	osud
nebo	nebo	k8xC	nebo
určují	určovat	k5eAaImIp3nP	určovat
jeho	jeho	k3xOp3gFnPc4	jeho
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
sudičky	sudička	k1gFnPc4	sudička
byly	být	k5eAaImAgFnP	být
řecké	řecký	k2eAgInPc4d1	řecký
Moiry	Moir	k1gInPc4	Moir
<g/>
,	,	kIx,	,
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Římě	Řím	k1gInSc6	Řím
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
v	v	k7c6	v
germánské	germánský	k2eAgFnSc6d1	germánská
mytologii	mytologie	k1gFnSc6	mytologie
Norny	norna	k1gFnSc2	norna
<g/>
.	.	kIx.	.
</s>
<s>
Bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
s	s	k7c7	s
vílami	víla	k1gFnPc7	víla
rovněž	rovněž	k9	rovněž
souvisejí	souviset	k5eAaImIp3nP	souviset
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
polednice	polednice	k1gFnPc1	polednice
a	a	k8xC	a
klekánice	klekánice	k1gFnPc1	klekánice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
důsledků	důsledek	k1gInPc2	důsledek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vztahy	vztah	k1gInPc1	vztah
víl	víla	k1gFnPc2	víla
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
Vodní	vodní	k2eAgFnPc1d1	vodní
víly	víla	k1gFnPc1	víla
prý	prý	k9	prý
rády	rád	k2eAgFnPc1d1	ráda
topí	topit	k5eAaImIp3nP	topit
lidi	člověk	k1gMnPc4	člověk
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
vodník	vodník	k1gMnSc1	vodník
<g/>
)	)	kIx)	)
a	a	k8xC	a
žárlivě	žárlivě	k6eAd1	žárlivě
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
střeží	střežit	k5eAaImIp3nS	střežit
své	svůj	k3xOyFgFnSc2	svůj
studánky	studánka	k1gFnSc2	studánka
<g/>
.	.	kIx.	.
</s>
<s>
Rusalky	rusalka	k1gFnPc1	rusalka
jsou	být	k5eAaImIp3nP	být
nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
zejména	zejména	k9	zejména
o	o	k7c6	o
letnicích	letnice	k1gFnPc6	letnice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejenže	nejenže	k6eAd1	nejenže
topí	topit	k5eAaImIp3nP	topit
a	a	k8xC	a
ulechtávají	ulechtávat	k5eAaImIp3nP	ulechtávat
poutníky	poutník	k1gMnPc4	poutník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neuhodnou	uhodnout	k5eNaPmIp3nP	uhodnout
jejich	jejich	k3xOp3gNnSc1	jejich
hádanky	hádanka	k1gFnPc1	hádanka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
převrhují	převrhovat	k5eAaImIp3nP	převrhovat
lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
ničí	ničí	k3xOyNgInPc1	ničí
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
hráze	hráz	k1gFnPc1	hráz
a	a	k8xC	a
rybářské	rybářský	k2eAgFnPc1d1	rybářská
sítě	síť	k1gFnPc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgFnPc1d1	lesní
víly	víla	k1gFnPc1	víla
zase	zase	k9	zase
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
lákají	lákat	k5eAaImIp3nP	lákat
kolemjdoucí	kolemjdoucí	k2eAgMnPc4d1	kolemjdoucí
muže	muž	k1gMnPc4	muž
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
kola	kolo	k1gNnSc2	kolo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gMnPc4	on
mohly	moct	k5eAaImAgFnP	moct
utancovat	utancovat	k5eAaPmF	utancovat
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vílám	víla	k1gFnPc3	víla
je	být	k5eAaImIp3nS	být
také	také	k9	také
připisováno	připisovat	k5eAaImNgNnS	připisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kradou	krást	k5eAaImIp3nP	krást
lidem	člověk	k1gMnPc3	člověk
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
z	z	k7c2	z
kolébek	kolébka	k1gFnPc2	kolébka
a	a	k8xC	a
podvrhují	podvrhovat	k5eAaImIp3nP	podvrhovat
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
znetvořené	znetvořený	k2eAgFnPc1d1	znetvořená
a	a	k8xC	a
zakrslé	zakrslý	k2eAgFnPc1d1	zakrslá
jak	jak	k8xS	jak
fyzicky	fyzicky	k6eAd1	fyzicky
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
duševně	duševně	k6eAd1	duševně
(	(	kIx(	(
<g/>
v	v	k7c6	v
Litvě	Litva	k1gFnSc6	Litva
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
víly	víla	k1gFnPc1	víla
samy	sám	k3xTgFnPc1	sám
děti	dítě	k1gFnPc1	dítě
mít	mít	k5eAaImF	mít
nemohou	moct	k5eNaImIp3nP	moct
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
místo	místo	k7c2	místo
ukradeného	ukradený	k2eAgNnSc2d1	ukradené
dítěte	dítě	k1gNnSc2	dítě
zanechají	zanechat	k5eAaPmIp3nP	zanechat
v	v	k7c6	v
kolébce	kolébka	k1gFnSc6	kolébka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
umělý	umělý	k2eAgInSc1d1	umělý
<g/>
,	,	kIx,	,
kouzlem	kouzlo	k1gNnSc7	kouzlo
oživený	oživený	k2eAgInSc1d1	oživený
výtvor	výtvor	k1gInSc1	výtvor
bez	bez	k7c2	bez
duše	duše	k1gFnSc2	duše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
podvržení	podvržení	k1gNnSc2	podvržení
dítěte	dítě	k1gNnSc2	dítě
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
první	první	k4xOgInPc4	první
dny	den	k1gInPc4	den
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
(	(	kIx(	(
<g/>
u	u	k7c2	u
křesťanů	křesťan	k1gMnPc2	křesťan
konkrétně	konkrétně	k6eAd1	konkrétně
do	do	k7c2	do
křtu	křest	k1gInSc2	křest
dítěte	dítě	k1gNnSc2	dítě
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
od	od	k7c2	od
dítěte	dítě	k1gNnSc2	dítě
nesměla	smět	k5eNaImAgFnS	smět
hnout	hnout	k5eAaPmF	hnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
už	už	k6eAd1	už
dítě	dítě	k1gNnSc1	dítě
bylo	být	k5eAaImAgNnS	být
podvrženo	podvrhnout	k5eAaPmNgNnS	podvrhnout
<g/>
,	,	kIx,	,
existovaly	existovat	k5eAaImAgFnP	existovat
různé	různý	k2eAgFnPc1d1	různá
pověrečné	pověrečný	k2eAgFnPc1d1	pověrečná
rady	rada	k1gFnPc1	rada
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc4	on
zbavit	zbavit	k5eAaPmF	zbavit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
často	často	k6eAd1	často
ho	on	k3xPp3gNnSc4	on
bít	bít	k5eAaImF	bít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
víle	víla	k1gFnSc6	víla
bylo	být	k5eAaImAgNnS	být
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
dítě	dítě	k1gNnSc4	dítě
si	se	k3xPyFc3	se
přišla	přijít	k5eAaPmAgFnS	přijít
a	a	k8xC	a
ukradené	ukradený	k2eAgFnSc2d1	ukradená
vrátila	vrátit	k5eAaPmAgFnS	vrátit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
výše	vysoce	k6eAd2	vysoce
popsanými	popsaný	k2eAgInPc7d1	popsaný
vílími	vílí	k2eAgInPc7d1	vílí
skutky	skutek	k1gInPc7	skutek
nemusí	muset	k5eNaImIp3nP	muset
stát	stát	k5eAaImF	stát
jen	jen	k9	jen
zlé	zlý	k2eAgInPc1d1	zlý
úmysly	úmysl	k1gInPc1	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
víly	víla	k1gFnPc1	víla
popisovány	popisován	k2eAgFnPc1d1	popisována
jako	jako	k8xS	jako
záludné	záludný	k2eAgFnPc1d1	záludná
a	a	k8xC	a
mstivé	mstivý	k2eAgFnPc1d1	mstivá
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
ale	ale	k8xC	ale
bývá	bývat	k5eAaImIp3nS	bývat
zdůrazněna	zdůraznit	k5eAaPmNgFnS	zdůraznit
jejich	jejich	k3xOp3gFnSc1	jejich
bezstarostnost	bezstarostnost	k1gFnSc1	bezstarostnost
a	a	k8xC	a
lehkomyslnost	lehkomyslnost	k1gFnSc1	lehkomyslnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
lze	lze	k6eAd1	lze
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
víla	víla	k1gFnSc1	víla
utancuje	utancovat	k5eAaPmIp3nS	utancovat
mladého	mladý	k2eAgMnSc4d1	mladý
muže	muž	k1gMnSc4	muž
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
,	,	kIx,	,
nedělá	dělat	k5eNaImIp3nS	dělat
to	ten	k3xDgNnSc1	ten
nutně	nutně	k6eAd1	nutně
ze	z	k7c2	z
zlého	zlý	k2eAgInSc2d1	zlý
úmyslu	úmysl	k1gInSc2	úmysl
-	-	kIx~	-
prostě	prostě	k9	prostě
neví	vědět	k5eNaImIp3nS	vědět
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladík	mladík	k1gMnSc1	mladík
celonoční	celonoční	k2eAgInSc4d1	celonoční
tanec	tanec	k1gInSc4	tanec
fyzicky	fyzicky	k6eAd1	fyzicky
nevydrží	vydržet	k5eNaPmIp3nP	vydržet
...	...	k?	...
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
však	však	k9	však
mstí	mstít	k5eAaImIp3nS	mstít
za	za	k7c4	za
rušení	rušení	k1gNnSc4	rušení
svého	svůj	k3xOyFgInSc2	svůj
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
kácení	kácení	k1gNnSc1	kácení
stromů	strom	k1gInPc2	strom
či	či	k8xC	či
zabíjení	zabíjení	k1gNnPc2	zabíjení
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
máta	máta	k1gFnSc1	máta
<g/>
,	,	kIx,	,
pelyněk	pelyněk	k1gInSc1	pelyněk
a	a	k8xC	a
libeček	libeček	k1gInSc1	libeček
lékařský	lékařský	k2eAgInSc1d1	lékařský
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
víl	víla	k1gFnPc2	víla
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
často	často	k6eAd1	často
líčen	líčen	k2eAgInSc1d1	líčen
také	také	k9	také
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
víly	víla	k1gFnPc1	víla
mladým	mladý	k2eAgMnPc3d1	mladý
mužům	muž	k1gMnPc3	muž
neubližují	ubližovat	k5eNaImIp3nP	ubližovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
jim	on	k3xPp3gMnPc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nP	držet
nad	nad	k7c7	nad
nimi	on	k3xPp3gFnPc7	on
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
obdarovávají	obdarovávat	k5eAaImIp3nP	obdarovávat
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Srbsku	Srbsko	k1gNnSc6	Srbsko
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc4d1	známá
vílí	vílí	k2eAgFnPc4d1	vílí
"	"	kIx"	"
<g/>
posestriny	posestrina	k1gFnPc4	posestrina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pomyslné	pomyslný	k2eAgFnPc1d1	pomyslná
sestry	sestra	k1gFnPc1	sestra
a	a	k8xC	a
ochránkyně	ochránkyně	k1gFnSc1	ochránkyně
mladých	mladý	k2eAgMnPc2d1	mladý
junáků	junák	k1gMnPc2	junák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
provdat	provdat	k5eAaPmF	provdat
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
pověstech	pověst	k1gFnPc6	pověst
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiné	k1gNnPc6	jiné
z	z	k7c2	z
donucení	donucení	k1gNnSc2	donucení
<g/>
,	,	kIx,	,
třeba	třeba	k9	třeba
když	když	k8xS	když
jim	on	k3xPp3gMnPc3	on
jejich	jejich	k3xOp3gFnSc1	jejich
nastávající	nastávající	k1gFnSc1	nastávající
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
nějakou	nějaký	k3yIgFnSc4	nějaký
část	část	k1gFnSc4	část
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
kterého	který	k3yQgInSc2	který
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
nemohou	moct	k5eNaImIp3nP	moct
odejít	odejít	k5eAaPmF	odejít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
takový	takový	k3xDgInSc1	takový
sňatek	sňatek	k1gInSc1	sňatek
obvykle	obvykle	k6eAd1	obvykle
mívá	mívat	k5eAaImIp3nS	mívat
nějakou	nějaký	k3yIgFnSc4	nějaký
podmínku	podmínka	k1gFnSc4	podmínka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
porušena	porušit	k5eAaPmNgFnS	porušit
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
víla	víla	k1gFnSc1	víla
svého	svůj	k3xOyFgMnSc2	svůj
muže	muž	k1gMnSc2	muž
opustí	opustit	k5eAaPmIp3nS	opustit
a	a	k8xC	a
už	už	k6eAd1	už
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
nikdy	nikdy	k6eAd1	nikdy
nevrátí	vrátit	k5eNaPmIp3nP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc7	takový
podmínkou	podmínka	k1gFnSc7	podmínka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
nesmí	smět	k5eNaImIp3nS	smět
víle	víla	k1gFnSc3	víla
nikdy	nikdy	k6eAd1	nikdy
vyčíst	vyčíst	k5eAaPmF	vyčíst
její	její	k3xOp3gInSc4	její
vílí	vílí	k2eAgInSc4d1	vílí
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
ustřihnout	ustřihnout	k5eAaPmF	ustřihnout
jí	on	k3xPp3gFnSc3	on
vlasy	vlas	k1gInPc4	vlas
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
dělá	dělat	k5eAaImIp3nS	dělat
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
v	v	k7c6	v
zamčeném	zamčený	k2eAgInSc6d1	zamčený
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pověstech	pověst	k1gFnPc6	pověst
manžel	manžel	k1gMnSc1	manžel
tuto	tento	k3xDgFnSc4	tento
podmínku	podmínka	k1gFnSc4	podmínka
obvykle	obvykle	k6eAd1	obvykle
nedodrží	dodržet	k5eNaPmIp3nS	dodržet
a	a	k8xC	a
víla	víla	k1gFnSc1	víla
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gFnPc7	jejich
dětmi	dítě	k1gFnPc7	dítě
navždy	navždy	k6eAd1	navždy
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Dokud	dokud	k8xS	dokud
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
víla	víla	k1gFnSc1	víla
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
mu	on	k3xPp3gMnSc3	on
svými	svůj	k3xOyFgFnPc7	svůj
znalostmi	znalost	k1gFnPc7	znalost
či	či	k8xC	či
kouzly	kouzlo	k1gNnPc7	kouzlo
a	a	k8xC	a
manželům	manžel	k1gMnPc3	manžel
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
daří	dařit	k5eAaImIp3nS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
spolu	spolu	k6eAd1	spolu
muži	muž	k1gMnPc1	muž
a	a	k8xC	a
víly	víla	k1gFnPc1	víla
mají	mít	k5eAaImIp3nP	mít
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
neobyčejně	obyčejně	k6eNd1	obyčejně
moudré	moudrý	k2eAgFnPc1d1	moudrá
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Víly	víla	k1gFnPc1	víla
podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
mohou	moct	k5eAaImIp3nP	moct
pomáhat	pomáhat	k5eAaImF	pomáhat
i	i	k9	i
dívkám	dívka	k1gFnPc3	dívka
a	a	k8xC	a
přátelit	přátelit	k5eAaImF	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
dříve	dříve	k6eAd2	dříve
vílám	víla	k1gFnPc3	víla
přinášeli	přinášet	k5eAaImAgMnP	přinášet
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
příznivě	příznivě	k6eAd1	příznivě
naklonili	naklonit	k5eAaPmAgMnP	naklonit
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
vodních	vodní	k2eAgInPc2d1	vodní
pramenů	pramen	k1gInPc2	pramen
jim	on	k3xPp3gMnPc3	on
nechávali	nechávat	k5eAaImAgMnP	nechávat
koláče	koláč	k1gInSc2	koláč
<g/>
,	,	kIx,	,
polní	polní	k2eAgFnPc4d1	polní
plodiny	plodina	k1gFnPc4	plodina
a	a	k8xC	a
květy	květ	k1gInPc4	květ
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
jim	on	k3xPp3gMnPc3	on
rozvěšovali	rozvěšovat	k5eAaImAgMnP	rozvěšovat
pestré	pestrý	k2eAgFnPc4d1	pestrá
stužky	stužka	k1gFnPc4	stužka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Rusi	Rus	k1gFnSc6	Rus
se	se	k3xPyFc4	se
o	o	k7c6	o
letnicích	letnice	k1gFnPc6	letnice
slavila	slavit	k5eAaImAgFnS	slavit
tzv.	tzv.	kA	tzv.
rusalija	rusalija	k1gFnSc1	rusalija
<g/>
,	,	kIx,	,
rusalské	rusalský	k2eAgInPc4d1	rusalský
svátky	svátek	k1gInPc4	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
přinášely	přinášet	k5eAaImAgFnP	přinášet
oběti	oběť	k1gFnSc3	oběť
rusalkám	rusalka	k1gFnPc3	rusalka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
neklidné	klidný	k2eNgInPc1d1	neklidný
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
-	-	kIx~	-
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
vod	voda	k1gFnPc2	voda
jim	on	k3xPp3gMnPc3	on
býval	bývat	k5eAaImAgInS	bývat
obětován	obětovat	k5eAaBmNgInS	obětovat
chléb	chléb	k1gInSc1	chléb
<g/>
,	,	kIx,	,
máslo	máslo	k1gNnSc1	máslo
a	a	k8xC	a
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
na	na	k7c4	na
stromy	strom	k1gInPc4	strom
se	se	k3xPyFc4	se
věšely	věšet	k5eAaImAgInP	věšet
kusy	kus	k1gInPc1	kus
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
věnce	věnec	k1gInSc2	věnec
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
se	se	k3xPyFc4	se
házely	házet	k5eAaImAgFnP	házet
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jejich	jejich	k3xOp3gFnSc7	jejich
mocí	moc	k1gFnSc7	moc
měly	mít	k5eAaImAgInP	mít
lidi	člověk	k1gMnPc4	člověk
ochránit	ochránit	k5eAaPmF	ochránit
některé	některý	k3yIgFnPc1	některý
byliny	bylina	k1gFnPc1	bylina
(	(	kIx(	(
<g/>
máta	máta	k1gFnSc1	máta
<g/>
,	,	kIx,	,
libeček	libeček	k1gInSc1	libeček
<g/>
,	,	kIx,	,
pelyněk	pelyněk	k1gInSc1	pelyněk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
svátcích	svátek	k1gInPc6	svátek
se	se	k3xPyFc4	se
také	také	k9	také
hrály	hrát	k5eAaImAgFnP	hrát
rozpustilé	rozpustilý	k2eAgFnPc1d1	rozpustilá
rusalné	rusalný	k2eAgFnPc1d1	rusalný
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
zpívaly	zpívat	k5eAaImAgFnP	zpívat
se	se	k3xPyFc4	se
běsovské	běsovský	k2eAgFnPc1d1	běsovský
písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
hrálo	hrát	k5eAaImAgNnS	hrát
se	se	k3xPyFc4	se
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
píšťaly	píšťala	k1gFnPc4	píšťala
a	a	k8xC	a
bubny	buben	k1gInPc4	buben
a	a	k8xC	a
tančilo	tančit	k5eAaImAgNnS	tančit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
slavnosti	slavnost	k1gFnPc1	slavnost
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgFnP	doložit
i	i	k9	i
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Savině	Savin	k2eAgFnSc6d1	Savin
knize	kniha	k1gFnSc6	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
víly	víla	k1gFnPc1	víla
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
také	také	k9	také
překládány	překládán	k2eAgFnPc1d1	překládána
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	s	k7c7	s
slovanskými	slovanský	k2eAgFnPc7d1	Slovanská
vílami	víla	k1gFnPc7	víla
souvisí	souviset	k5eAaImIp3nS	souviset
jen	jen	k9	jen
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
třeba	třeba	k6eAd1	třeba
pixie	pixie	k1gFnSc2	pixie
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
i	i	k9	i
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
pixie	pixie	k1gFnPc1	pixie
a	a	k8xC	a
fairy	faira	k1gFnPc1	faira
často	často	k6eAd1	často
pletou	plést	k5eAaImIp3nP	plést
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
elfů	elf	k1gMnPc2	elf
<g/>
.	.	kIx.	.
</s>
<s>
Pixie	Pixie	k1gFnPc1	Pixie
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgFnPc1d1	malá
(	(	kIx(	(
<g/>
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
30	[number]	k4	30
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
)	)	kIx)	)
a	a	k8xC	a
létají	létat	k5eAaImIp3nP	létat
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
motýlí	motýlí	k2eAgNnPc4d1	motýlí
nebo	nebo	k8xC	nebo
blanitá	blanitý	k2eAgNnPc4d1	blanité
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Rádi	rád	k2eAgMnPc1d1	rád
lidem	člověk	k1gMnPc3	člověk
provádí	provádět	k5eAaImIp3nS	provádět
různé	různý	k2eAgInPc4d1	různý
šprýmy	šprým	k1gInPc4	šprým
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jim	on	k3xPp3gMnPc3	on
kradou	krást	k5eAaImIp3nP	krást
věci	věc	k1gFnPc4	věc
nebo	nebo	k8xC	nebo
je	on	k3xPp3gFnPc4	on
nechávají	nechávat	k5eAaImIp3nP	nechávat
bloudit	bloudit	k5eAaImF	bloudit
<g/>
.	.	kIx.	.
</s>
<s>
Kontakt	kontakt	k1gInSc1	kontakt
se	s	k7c7	s
železem	železo	k1gNnSc7	železo
je	být	k5eAaImIp3nS	být
zraňuje	zraňovat	k5eAaImIp3nS	zraňovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
jde	jít	k5eAaImIp3nS	jít
železem	železo	k1gNnSc7	železo
bránit	bránit	k5eAaImF	bránit
(	(	kIx(	(
<g/>
ochranné	ochranný	k2eAgFnPc1d1	ochranná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
podkovy	podkova	k1gFnSc2	podkova
mohou	moct	k5eAaImIp3nP	moct
vycházet	vycházet	k5eAaImF	vycházet
právě	právě	k9	právě
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
železná	železný	k2eAgFnSc1d1	železná
a	a	k8xC	a
proto	proto	k8xC	proto
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
vílami	víla	k1gFnPc7	víla
<g/>
:	:	kIx,	:
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byly	být	k5eAaImAgFnP	být
podkovy	podkova	k1gFnPc1	podkova
nejčastější	častý	k2eAgFnSc2d3	nejčastější
forma	forma	k1gFnSc1	forma
železa	železo	k1gNnSc2	železo
ke	k	k7c3	k
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
vesničan	vesničan	k1gMnSc1	vesničan
mohl	moct	k5eAaImAgMnS	moct
dostat	dostat	k5eAaPmF	dostat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
