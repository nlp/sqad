<s>
Víla	víla	k1gFnSc1	víla
<g/>
,	,	kIx,	,
též	též	k9	též
rusalka	rusalka	k1gFnSc1	rusalka
<g/>
,	,	kIx,	,
divoženka	divoženka	k1gFnSc1	divoženka
nebo	nebo	k8xC	nebo
bosorka	bosorka	k1gFnSc1	bosorka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nadpřirozená	nadpřirozený	k2eAgFnSc1d1	nadpřirozená
bytost	bytost	k1gFnSc1	bytost
ženského	ženský	k2eAgNnSc2d1	ženské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
již	již	k9	již
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obdobách	obdoba	k1gFnPc6	obdoba
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
Evropy	Evropa	k1gFnSc2	Evropa
už	už	k6eAd1	už
od	od	k7c2	od
raného	raný	k2eAgInSc2d1	raný
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
