<p>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
hlasový	hlasový	k2eAgInSc1d1	hlasový
umělecký	umělecký	k2eAgInSc1d1	umělecký
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
cíleným	cílený	k2eAgNnSc7d1	cílené
střídáním	střídání	k1gNnSc7	střídání
tónů	tón	k1gInPc2	tón
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
i	i	k9	i
rytmickou	rytmický	k2eAgFnSc7d1	rytmická
strukturou	struktura	k1gFnSc7	struktura
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
řeči	řeč	k1gFnSc2	řeč
nebo	nebo	k8xC	nebo
recitace	recitace	k1gFnSc2	recitace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
výzkumu	výzkum	k1gInSc6	výzkum
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
zpěvem	zpěv	k1gInSc7	zpěv
a	a	k8xC	a
řečí	řeč	k1gFnSc7	řeč
jsou	být	k5eAaImIp3nP	být
publikovány	publikován	k2eAgInPc1d1	publikován
články	článek	k1gInPc1	článek
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
například	například	k6eAd1	například
i	i	k9	i
rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
způsobu	způsob	k1gInSc6	způsob
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
zpěvu	zpěv	k1gInSc6	zpěv
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nádechy	nádech	k1gInPc1	nádech
nebyly	být	k5eNaImAgInP	být
přerušovány	přerušován	k2eAgFnPc4d1	přerušována
fráze	fráze	k1gFnPc4	fráze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
mluveným	mluvený	k2eAgFnPc3d1	mluvená
větám	věta	k1gFnPc3	věta
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
i	i	k9	i
rozdílnost	rozdílnost	k1gFnSc1	rozdílnost
zpěvu	zpěv	k1gInSc2	zpěv
a	a	k8xC	a
řeči	řeč	k1gFnSc2	řeč
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
funkce	funkce	k1gFnSc2	funkce
hlasového	hlasový	k2eAgInSc2d1	hlasový
aparátu	aparát	k1gInSc2	aparát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
nejpřirozenějším	přirozený	k2eAgMnSc7d3	nejpřirozenější
a	a	k8xC	a
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
hudebním	hudební	k2eAgNnSc7d1	hudební
uměním	umění	k1gNnSc7	umění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ho	on	k3xPp3gNnSc4	on
schopen	schopen	k2eAgInSc1d1	schopen
prakticky	prakticky	k6eAd1	prakticky
každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
s	s	k7c7	s
nepatrným	nepatrný	k2eAgNnSc7d1	nepatrný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
žádným	žádný	k3yNgNnSc7	žádný
nadáním	nadání	k1gNnSc7	nadání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Styly	styl	k1gInPc1	styl
zpěvu	zpěv	k1gInSc2	zpěv
==	==	k?	==
</s>
</p>
<p>
<s>
Zpěváci	Zpěvák	k1gMnPc1	Zpěvák
různých	různý	k2eAgInPc2d1	různý
hudebních	hudební	k2eAgInPc2d1	hudební
žánrů	žánr	k1gInPc2	žánr
dominující	dominující	k2eAgFnSc2d1	dominující
dnešnímu	dnešní	k2eAgInSc3d1	dnešní
hudebnímu	hudební	k2eAgInSc3d1	hudební
průmyslu	průmysl	k1gInSc3	průmysl
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vzory	vzor	k1gInPc4	vzor
svých	svůj	k3xOyFgMnPc2	svůj
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
napodobovat	napodobovat	k5eAaImF	napodobovat
v	v	k7c6	v
koupelně	koupelna	k1gFnSc6	koupelna
<g/>
,	,	kIx,	,
na	na	k7c6	na
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pěveckém	pěvecký	k2eAgInSc6d1	pěvecký
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Styly	styl	k1gInPc1	styl
zpívané	zpívaný	k2eAgFnSc2d1	zpívaná
hudby	hudba	k1gFnSc2	hudba
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
operu	opera	k1gFnSc4	opera
<g/>
,	,	kIx,	,
klasickou	klasický	k2eAgFnSc4d1	klasická
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
muzikál	muzikál	k1gInSc4	muzikál
<g/>
,	,	kIx,	,
folk	folk	k1gInSc4	folk
<g/>
,	,	kIx,	,
country	country	k2eAgMnSc1d1	country
<g/>
,	,	kIx,	,
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
pop	pop	k1gMnSc1	pop
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
včetně	včetně	k7c2	včetně
třeba	třeba	k6eAd1	třeba
narozeninového	narozeninový	k2eAgNnSc2d1	narozeninové
zpívání	zpívání	k1gNnSc2	zpívání
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgFnSc7d1	speciální
technikou	technika	k1gFnSc7	technika
zpěvu	zpěv	k1gInSc2	zpěv
je	být	k5eAaImIp3nS	být
i	i	k9	i
screaming	screaming	k1gInSc1	screaming
<g/>
/	/	kIx~	/
<g/>
growling	growling	k1gInSc1	growling
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
doprovozen	doprovozen	k2eAgInSc4d1	doprovozen
hlasy	hlas	k1gInPc4	hlas
zpěvu	zpěv	k1gInSc2	zpěv
či	či	k8xC	či
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
různé	různý	k2eAgFnSc2d1	různá
hlasitosti	hlasitost	k1gFnSc2	hlasitost
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
znít	znít	k5eAaImF	znít
i	i	k9	i
bez	bez	k7c2	bez
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc1	styl
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
zpěvák	zpěvák	k1gMnSc1	zpěvák
nemá	mít	k5eNaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlasové	hlasový	k2eAgInPc4d1	hlasový
typy	typ	k1gInPc4	typ
podle	podle	k7c2	podle
klasického	klasický	k2eAgNnSc2d1	klasické
schématu	schéma	k1gNnSc2	schéma
==	==	k?	==
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnSc1d1	základní
dělení	dělení	k1gNnSc1	dělení
hlasů	hlas	k1gInPc2	hlas
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Mužské	mužský	k2eAgInPc1d1	mužský
hlasy	hlas	k1gInPc1	hlas
===	===	k?	===
</s>
</p>
<p>
<s>
tenor	tenor	k1gInSc1	tenor
–	–	k?	–
vyšší	vysoký	k2eAgInSc1d2	vyšší
mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
baryton	baryton	k1gInSc1	baryton
-	-	kIx~	-
střední	střední	k2eAgInSc1d1	střední
mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
basbaryton	basbaryton	k1gInSc1	basbaryton
-	-	kIx~	-
mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
ležící	ležící	k2eAgInSc1d1	ležící
mezi	mezi	k7c7	mezi
basem	bas	k1gInSc7	bas
a	a	k8xC	a
barytonem	baryton	k1gInSc7	baryton
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
bas	bas	k1gInSc1	bas
–	–	k?	–
nižší	nízký	k2eAgInSc1d2	nižší
mužský	mužský	k2eAgInSc1d1	mužský
hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
===	===	k?	===
Ženské	ženský	k2eAgInPc1d1	ženský
hlasy	hlas	k1gInPc1	hlas
===	===	k?	===
</s>
</p>
<p>
<s>
soprán	soprán	k1gInSc1	soprán
–	–	k?	–
vyšší	vysoký	k2eAgInSc1d2	vyšší
ženský	ženský	k2eAgInSc1d1	ženský
hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
mezzosoprán	mezzosoprán	k1gInSc1	mezzosoprán
-	-	kIx~	-
střední	střední	k2eAgInSc4d1	střední
ženský	ženský	k2eAgInSc4d1	ženský
hlas	hlas	k1gInSc4	hlas
</s>
</p>
<p>
<s>
alt	alt	k1gInSc1	alt
–	–	k?	–
nižší	nízký	k2eAgInSc1d2	nižší
ženský	ženský	k2eAgInSc1d1	ženský
hlas	hlas	k1gInSc1	hlas
</s>
</p>
<p>
<s>
==	==	k?	==
Důležitost	důležitost	k1gFnSc4	důležitost
tréninku	trénink	k1gInSc2	trénink
hlasu	hlas	k1gInSc2	hlas
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
zpěváků	zpěvák	k1gMnPc2	zpěvák
samouků	samouk	k1gMnPc2	samouk
je	on	k3xPp3gMnPc4	on
hrdých	hrdý	k2eAgFnPc2d1	hrdá
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
přirozený	přirozený	k2eAgInSc4d1	přirozený
hlas	hlas	k1gInSc4	hlas
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
hlasový	hlasový	k2eAgInSc1d1	hlasový
trénink	trénink	k1gInSc1	trénink
může	moct	k5eAaImIp3nS	moct
jejich	jejich	k3xOp3gInSc4	jejich
hlas	hlas	k1gInSc4	hlas
jedině	jedině	k6eAd1	jedině
pokazit	pokazit	k5eAaPmF	pokazit
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
netrénovaný	trénovaný	k2eNgInSc1d1	netrénovaný
hlas	hlas	k1gInSc1	hlas
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
právě	právě	k6eAd1	právě
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nedostaví	dostavit	k5eNaPmIp3nS	dostavit
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
akusticky	akusticky	k6eAd1	akusticky
těžké	těžký	k2eAgNnSc1d1	těžké
prostředí	prostředí	k1gNnSc1	prostředí
pro	pro	k7c4	pro
zpěv	zpěv	k1gInSc4	zpěv
<g/>
,	,	kIx,	,
těžký	těžký	k2eAgInSc4d1	těžký
repertoár	repertoár	k1gInSc4	repertoár
či	či	k8xC	či
emoce	emoce	k1gFnPc1	emoce
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dobrý	dobrý	k2eAgMnSc1d1	dobrý
učitel	učitel	k1gMnSc1	učitel
zpěvu	zpěv	k1gInSc2	zpěv
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
může	moct	k5eAaImIp3nS	moct
zpěvákovu	zpěvákův	k2eAgFnSc4d1	zpěvákova
techniku	technika	k1gFnSc4	technika
ochránit	ochránit	k5eAaPmF	ochránit
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
samouk	samouk	k1gMnSc1	samouk
tyto	tento	k3xDgFnPc4	tento
věci	věc	k1gFnPc4	věc
nemusí	muset	k5eNaImIp3nS	muset
nikdy	nikdy	k6eAd1	nikdy
objevit	objevit	k5eAaPmF	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Učitel	učitel	k1gMnSc1	učitel
zpěvu	zpěv	k1gInSc2	zpěv
Richard	Richard	k1gMnSc1	Richard
Miller	Miller	k1gMnSc1	Miller
napsal	napsat	k5eAaPmAgMnS	napsat
"	"	kIx"	"
<g/>
Technika	technika	k1gFnSc1	technika
představuje	představovat	k5eAaImIp3nS	představovat
jakousi	jakýsi	k3yIgFnSc4	jakýsi
stabilizaci	stabilizace	k1gFnSc4	stabilizace
vyžadované	vyžadovaný	k2eAgFnSc2d1	vyžadovaná
koordinace	koordinace	k1gFnSc2	koordinace
při	při	k7c6	při
zpěvu	zpěv	k1gInSc6	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zakódováno	zakódovat	k5eAaPmNgNnS	zakódovat
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
těle	tělo	k1gNnSc6	tělo
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Netrénovaní	trénovaný	k2eNgMnPc1d1	netrénovaný
zpěváci	zpěvák	k1gMnPc1	zpěvák
běžně	běžně	k6eAd1	běžně
nemají	mít	k5eNaImIp3nP	mít
mnoho	mnoho	k4c1	mnoho
hlasových	hlasový	k2eAgInPc2d1	hlasový
problémů	problém	k1gInPc2	problém
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
okolo	okolo	k7c2	okolo
30	[number]	k4	30
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
hlasem	hlasem	k6eAd1	hlasem
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
protože	protože	k8xS	protože
nedělá	dělat	k5eNaImIp3nS	dělat
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
vždy	vždy	k6eAd1	vždy
dělal	dělat	k5eAaImAgInS	dělat
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nebo	nebo	k8xC	nebo
později	pozdě	k6eAd2	pozdě
mohou	moct	k5eAaImIp3nP	moct
dostavit	dostavit	k5eAaPmF	dostavit
vlivem	vliv	k1gInSc7	vliv
nevědomého	vědomý	k2eNgNnSc2d1	nevědomé
budování	budování	k1gNnSc2	budování
zlozvyků	zlozvyk	k1gInPc2	zlozvyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlasová	hlasový	k2eAgFnSc1d1	hlasová
technika	technika	k1gFnSc1	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
koordinovaném	koordinovaný	k2eAgInSc6d1	koordinovaný
přirozeném	přirozený	k2eAgInSc6d1	přirozený
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
<s>
Zkušený	zkušený	k2eAgMnSc1d1	zkušený
zpěvák	zpěvák	k1gMnSc1	zpěvák
má	mít	k5eAaImIp3nS	mít
tuto	tento	k3xDgFnSc4	tento
koordinaci	koordinace	k1gFnSc4	koordinace
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
přežívají	přežívat	k5eAaImIp3nP	přežívat
fámy	fáma	k1gFnPc1	fáma
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ona	onen	k3xDgFnSc1	onen
koordinace	koordinace	k1gFnSc1	koordinace
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
totéž	týž	k3xTgNnSc1	týž
jako	jako	k9	jako
cosi	cosi	k3yInSc1	cosi
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
vrozené	vrozený	k2eAgNnSc1d1	vrozené
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
najít	najít	k5eAaPmF	najít
učitele	učitel	k1gMnPc4	učitel
zpěvu	zpěv	k1gInSc2	zpěv
který	který	k3yIgMnSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
techniku	technika	k1gFnSc4	technika
zpěvu	zpěv	k1gInSc2	zpěv
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
třeba	třeba	k6eAd1	třeba
učit	učit	k5eAaImF	učit
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgFnPc4d1	možná
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
spoléhat	spoléhat	k5eAaImF	spoléhat
na	na	k7c4	na
cítění	cítění	k1gNnSc4	cítění
hudby	hudba	k1gFnSc2	hudba
či	či	k8xC	či
zpívané	zpívaný	k2eAgFnSc2d1	zpívaná
poezie	poezie	k1gFnSc2	poezie
pokud	pokud	k8xS	pokud
hlas	hlas	k1gInSc1	hlas
nepracuje	pracovat	k5eNaImIp3nS	pracovat
jak	jak	k8xS	jak
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zpěv	zpěv	k1gInSc1	zpěv
zvířat	zvíře	k1gNnPc2	zvíře
==	==	k?	==
</s>
</p>
<p>
<s>
Pěvci	pěvec	k1gMnPc1	pěvec
(	(	kIx(	(
<g/>
zpěvní	zpěvní	k2eAgMnPc1d1	zpěvní
ptáci	pták	k1gMnPc1	pták
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
běžně	běžně	k6eAd1	běžně
rytmický	rytmický	k2eAgInSc4d1	rytmický
zvukový	zvukový	k2eAgInSc4d1	zvukový
projev	projev	k1gInSc4	projev
<g/>
.	.	kIx.	.
</s>
<s>
Velryby	velryba	k1gFnPc1	velryba
se	se	k3xPyFc4	se
také	také	k9	také
učí	učit	k5eAaImIp3nS	učit
zpívat	zpívat	k5eAaImF	zpívat
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Screaming	Screaming	k1gInSc1	Screaming
</s>
</p>
<p>
<s>
Growling	Growling	k1gInSc1	Growling
</s>
</p>
<p>
<s>
Polyfonie	polyfonie	k1gFnSc1	polyfonie
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
</s>
</p>
<p>
<s>
Pěvecký	pěvecký	k2eAgInSc1d1	pěvecký
sbor	sbor	k1gInSc1	sbor
</s>
</p>
<p>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
</s>
</p>
<p>
<s>
Recitace	recitace	k1gFnSc1	recitace
</s>
</p>
<p>
<s>
Deklamace	deklamace	k1gFnSc1	deklamace
</s>
</p>
<p>
<s>
Rap	rap	k1gMnSc1	rap
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zpěv	zpěv	k1gInSc1	zpěv
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zpěv	zpěv	k1gInSc1	zpěv
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
