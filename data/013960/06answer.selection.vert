<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
С	С	k?	С
<g/>
,	,	kIx,	,
Sovětskij	Sovětskij	k1gMnSc1	Sovětskij
Sojuz	Sojuz	k1gInSc1	Sojuz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Svaz	svaz	k1gInSc1	svaz
sovětských	sovětský	k2eAgFnPc2d1	sovětská
socialistických	socialistický	k2eAgFnPc2d1	socialistická
republik	republika	k1gFnPc2	republika
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
С	С	k?	С
<g/>
́	́	k?	́
<g/>
з	з	k?	з
С	С	k?	С
<g/>
́	́	k?	́
<g/>
т	т	k?	т
С	С	k?	С
<g/>
́	́	k?	́
<g/>
ч	ч	k?	ч
Р	Р	k?	Р
<g/>
́	́	k?	́
<g/>
б	б	k?	б
<g/>
,	,	kIx,	,
С	С	k?	С
<g/>
,	,	kIx,	,
Sojuz	Sojuz	k1gInSc1	Sojuz
Sovětskich	Sovětskich	k1gMnSc1	Sovětskich
Socialističeskich	Socialističeskich	k1gMnSc1	Socialističeskich
Respublik	Respublik	k1gMnSc1	Respublik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
eurasijský	eurasijský	k2eAgInSc1d1	eurasijský
stát	stát	k1gInSc1	stát
se	s	k7c7	s
socialistickým	socialistický	k2eAgNnSc7d1	socialistické
zřízením	zřízení	k1gNnSc7	zřízení
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
let	léto	k1gNnPc2	léto
1922	[number]	k4	1922
až	až	k6eAd1	až
1991	[number]	k4	1991
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
dřívějšího	dřívější	k2eAgNnSc2d1	dřívější
Ruského	ruský	k2eAgNnSc2d1	ruské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
