<p>
<s>
Erich	Erich	k1gMnSc1	Erich
Tylínek	Tylínek	k1gMnSc1	Tylínek
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1908	[number]	k4	1908
Saská	saský	k2eAgFnSc1d1	saská
Kamenice	Kamenice	k1gFnSc1	Kamenice
–	–	k?	–
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
lední	lední	k2eAgMnSc1d1	lední
hokejista	hokejista	k1gMnSc1	hokejista
hrající	hrající	k2eAgMnSc1d1	hrající
na	na	k7c4	na
postu	posta	k1gFnSc4	posta
brankáře	brankář	k1gMnSc4	brankář
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
a	a	k8xC	a
především	především	k9	především
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
hokejem	hokej	k1gInSc7	hokej
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
Kladně	Kladno	k1gNnSc6	Kladno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
za	za	k7c4	za
Slávii	Slávia	k1gFnSc4	Slávia
a	a	k8xC	a
za	za	k7c4	za
Spartu	Sparta	k1gFnSc4	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hrajícím	hrající	k2eAgMnSc7d1	hrající
trenérem	trenér	k1gMnSc7	trenér
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
švýcarského	švýcarský	k2eAgMnSc2d1	švýcarský
HC	HC	kA	HC
Chur	Chura	k1gFnPc2	Chura
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
v	v	k7c6	v
HC	HC	kA	HC
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Tylínek	Tylínek	k1gMnSc1	Tylínek
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
hokejistů	hokejista	k1gMnPc2	hokejista
používajících	používající	k2eAgMnPc2d1	používající
ochrannou	ochranný	k2eAgFnSc4d1	ochranná
masku	maska	k1gFnSc4	maska
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
maskou	maska	k1gFnSc7	maska
zachycen	zachytit	k5eAaPmNgMnS	zachytit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc4	první
zdokumentované	zdokumentovaný	k2eAgNnSc4d1	zdokumentované
užití	užití	k1gNnSc4	užití
hokejové	hokejový	k2eAgFnSc2d1	hokejová
masky	maska	k1gFnSc2	maska
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
kariéru	kariéra	k1gFnSc4	kariéra
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fotografoval	fotografovat	k5eAaImAgMnS	fotografovat
zejména	zejména	k9	zejména
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
přirozeném	přirozený	k2eAgNnSc6d1	přirozené
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
chovaná	chovaný	k2eAgFnSc1d1	chovaná
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
vyšly	vyjít	k5eAaPmAgFnP	vyjít
například	například	k6eAd1	například
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
S	s	k7c7	s
kamerou	kamera	k1gFnSc7	kamera
po	po	k7c6	po
evropských	evropský	k2eAgFnPc6d1	Evropská
ZOO	zoo	k1gFnPc6	zoo
a	a	k8xC	a
S	s	k7c7	s
kamerou	kamera	k1gFnSc7	kamera
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
oceněn	ocenit	k5eAaPmNgInS	ocenit
titulem	titul	k1gInSc7	titul
zasloužilý	zasloužilý	k2eAgMnSc1d1	zasloužilý
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
