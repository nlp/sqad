<s>
Celosvětová	celosvětový	k2eAgFnSc1d1	celosvětová
populace	populace	k1gFnSc1	populace
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
500	[number]	k4	500
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
toulavých	toulavý	k2eAgMnPc2d1	toulavý
a	a	k8xC	a
opuštěných	opuštěný	k2eAgMnPc2d1	opuštěný
psů	pes	k1gMnPc2	pes
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
370	[number]	k4	370
miliónů	milión	k4xCgInPc2	milión
<g/>
,	,	kIx,	,
nezisková	ziskový	k2eNgFnSc1d1	nezisková
organizace	organizace	k1gFnSc1	organizace
600	[number]	k4	600
<g/>
million	million	k1gInSc1	million
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
počet	počet	k1gInSc4	počet
jen	jen	k9	jen
toulavých	toulavý	k2eAgMnPc2d1	toulavý
psů	pes	k1gMnPc2	pes
na	na	k7c6	na
světě	svět	k1gInSc6	svět
právě	právě	k9	právě
na	na	k7c4	na
600	[number]	k4	600
miliónů	milión	k4xCgInPc2	milión
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
