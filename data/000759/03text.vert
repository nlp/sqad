<s>
Biskupství	biskupství	k1gNnSc1	biskupství
litomyšlské	litomyšlský	k2eAgNnSc1d1	litomyšlské
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1344	[number]	k4	1344
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
povýšením	povýšení	k1gNnSc7	povýšení
pražského	pražský	k2eAgNnSc2d1	Pražské
biskupství	biskupství	k1gNnSc2	biskupství
na	na	k7c6	na
arcibiskupství	arcibiskupství	k1gNnSc6	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
jako	jako	k9	jako
biskupství	biskupství	k1gNnSc1	biskupství
titulární	titulární	k2eAgNnSc1d1	titulární
<g/>
.	.	kIx.	.
</s>
<s>
Biskupství	biskupství	k1gNnSc1	biskupství
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecnou	obecný	k2eAgFnSc7d1	obecná
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mělo	mít	k5eAaImAgNnS	mít
alespoň	alespoň	k9	alespoň
dva	dva	k4xCgMnPc4	dva
sufragány	sufragán	k1gMnPc4	sufragán
<g/>
,	,	kIx,	,
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
dojednat	dojednat	k5eAaPmF	dojednat
přechod	přechod	k1gInSc4	přechod
vratislavského	vratislavský	k2eAgNnSc2d1	vratislavské
biskupství	biskupství	k1gNnSc2	biskupství
z	z	k7c2	z
hnězdenské	hnězdenský	k2eAgFnSc2d1	Hnězdenská
církevní	církevní	k2eAgFnSc2d1	církevní
provincie	provincie	k1gFnSc2	provincie
do	do	k7c2	do
nově	nově	k6eAd1	nově
vznikající	vznikající	k2eAgFnSc2d1	vznikající
provincie	provincie	k1gFnSc2	provincie
české	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
zřídit	zřídit	k5eAaPmF	zřídit
novou	nový	k2eAgFnSc4d1	nová
diecézi	diecéze	k1gFnSc4	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Materiální	materiální	k2eAgNnSc4d1	materiální
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
nového	nový	k2eAgNnSc2d1	nové
biskupství	biskupství	k1gNnSc2	biskupství
bylo	být	k5eAaImAgNnS	být
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
premonstrátský	premonstrátský	k2eAgInSc1d1	premonstrátský
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1145	[number]	k4	1145
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
majetek	majetek	k1gInSc1	majetek
i	i	k8xC	i
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
využity	využít	k5eAaPmNgFnP	využít
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
biskupství	biskupství	k1gNnSc4	biskupství
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
klášterního	klášterní	k2eAgInSc2d1	klášterní
kostela	kostel	k1gInSc2	kostel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
katedrálou	katedrála	k1gFnSc7	katedrála
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
možnost	možnost	k1gFnSc1	možnost
využití	využití	k1gNnSc2	využití
tohoto	tento	k3xDgInSc2	tento
komplexu	komplex	k1gInSc2	komplex
byla	být	k5eAaImAgFnS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
bylo	být	k5eAaImAgNnS	být
nové	nový	k2eAgNnSc1d1	nové
biskupství	biskupství	k1gNnSc1	biskupství
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
právě	právě	k6eAd1	právě
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgMnPc1d1	dosavadní
premonstráti	premonstrát	k1gMnPc1	premonstrát
byli	být	k5eAaImAgMnP	být
odškodněni	odškodnit	k5eAaPmNgMnP	odškodnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
zakládajícími	zakládající	k2eAgMnPc7d1	zakládající
členy	člen	k1gMnPc7	člen
kapituly	kapitula	k1gFnSc2	kapitula
nového	nový	k2eAgNnSc2d1	nové
biskupství	biskupství	k1gNnSc2	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nové	nový	k2eAgNnSc4d1	nové
biskupství	biskupství	k1gNnSc4	biskupství
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
mohlo	moct	k5eAaImAgNnS	moct
převzít	převzít	k5eAaPmF	převzít
část	část	k1gFnSc4	část
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
stávajících	stávající	k2eAgFnPc2d1	stávající
českých	český	k2eAgFnPc2d1	Česká
diecézí	diecéze	k1gFnPc2	diecéze
<g/>
,	,	kIx,	,
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
ovšem	ovšem	k9	ovšem
z	z	k7c2	z
rozsáhlejší	rozsáhlý	k2eAgFnSc2d2	rozsáhlejší
diecéze	diecéze	k1gFnSc2	diecéze
pražské	pražský	k2eAgFnSc2d1	Pražská
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
jej	on	k3xPp3gInSc4	on
tvořily	tvořit	k5eAaImAgFnP	tvořit
<g/>
:	:	kIx,	:
z	z	k7c2	z
pražské	pražský	k2eAgFnSc2d1	Pražská
diecéze	diecéze	k1gFnSc2	diecéze
4	[number]	k4	4
děkanáty	děkanát	k1gInPc4	děkanát
se	s	k7c7	s
117	[number]	k4	117
farnostmi	farnost	k1gFnPc7	farnost
<g/>
:	:	kIx,	:
děkanát	děkanát	k1gInSc1	děkanát
Chrudim	Chrudim	k1gFnSc1	Chrudim
s	s	k7c7	s
45	[number]	k4	45
farnostmi	farnost	k1gFnPc7	farnost
<g/>
,	,	kIx,	,
děkanát	děkanát	k1gInSc1	děkanát
Lanškroun	Lanškroun	k1gInSc1	Lanškroun
s	s	k7c7	s
16	[number]	k4	16
farnostmi	farnost	k1gFnPc7	farnost
<g/>
,	,	kIx,	,
děkanát	děkanát	k1gInSc1	děkanát
Polička	Polička	k1gFnSc1	Polička
se	s	k7c7	s
17	[number]	k4	17
farnostmi	farnost	k1gFnPc7	farnost
<g/>
,	,	kIx,	,
děkanát	děkanát	k1gInSc4	děkanát
Vysoké	vysoká	k1gFnSc2	vysoká
Mýto	mýto	k1gNnSc4	mýto
s	s	k7c7	s
39	[number]	k4	39
farnostmi	farnost	k1gFnPc7	farnost
a	a	k8xC	a
z	z	k7c2	z
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
:	:	kIx,	:
děkanát	děkanát	k1gInSc1	děkanát
Šumperk	Šumperk	k1gInSc1	Šumperk
se	s	k7c7	s
22	[number]	k4	22
farnostmi	farnost	k1gFnPc7	farnost
a	a	k8xC	a
děkanát	děkanát	k1gInSc1	děkanát
Úsov	Úsov	k1gInSc1	Úsov
s	s	k7c7	s
8	[number]	k4	8
farnostmi	farnost	k1gFnPc7	farnost
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
tedy	tedy	k9	tedy
6	[number]	k4	6
děkanátů	děkanát	k1gInPc2	děkanát
se	s	k7c7	s
147	[number]	k4	147
farnostmi	farnost	k1gFnPc7	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
biskupství	biskupství	k1gNnSc1	biskupství
tak	tak	k6eAd1	tak
zdaleka	zdaleka	k6eAd1	zdaleka
nedosahovalo	dosahovat	k5eNaImAgNnS	dosahovat
rozsahu	rozsah	k1gInSc3	rozsah
mateřských	mateřský	k2eAgFnPc2d1	mateřská
diecézí	diecéze	k1gFnPc2	diecéze
pražské	pražský	k2eAgFnSc2d1	Pražská
či	či	k8xC	či
olomoucké	olomoucký	k2eAgFnSc2d1	olomoucká
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
jeho	jeho	k3xOp3gFnSc1	jeho
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
udržovalo	udržovat	k5eAaImAgNnS	udržovat
užší	úzký	k2eAgInSc4d2	užší
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Moravou	Morava	k1gFnSc7	Morava
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
s	s	k7c7	s
olomouckým	olomoucký	k2eAgNnSc7d1	olomoucké
biskupstvím	biskupství	k1gNnSc7	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnSc4d1	prvotní
organizaci	organizace	k1gFnSc4	organizace
nové	nový	k2eAgFnSc2d1	nová
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vymezení	vymezení	k1gNnSc4	vymezení
vztahu	vztah	k1gInSc2	vztah
mezi	mezi	k7c7	mezi
biskupem	biskup	k1gMnSc7	biskup
a	a	k8xC	a
kapitulou	kapitula	k1gFnSc7	kapitula
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
z	z	k7c2	z
pověření	pověření	k1gNnSc2	pověření
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
vratislavský	vratislavský	k2eAgMnSc1d1	vratislavský
biskup	biskup	k1gMnSc1	biskup
Přeclav	Přeclav	k1gMnSc1	Přeclav
z	z	k7c2	z
Pohořelé	pohořelý	k2eAgFnSc2d1	Pohořelá
<g/>
.	.	kIx.	.
</s>
<s>
Biskupství	biskupství	k1gNnSc1	biskupství
fakticky	fakticky	k6eAd1	fakticky
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
za	za	k7c2	za
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1425	[number]	k4	1425
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následující	následující	k2eAgMnPc1d1	následující
představitelé	představitel	k1gMnPc1	představitel
byli	být	k5eAaImAgMnP	být
biskupy	biskup	k1gMnPc4	biskup
nebo	nebo	k8xC	nebo
administrátory	administrátor	k1gMnPc4	administrátor
jen	jen	k9	jen
formálně	formálně	k6eAd1	formálně
<g/>
.	.	kIx.	.
</s>
<s>
Právně	právně	k6eAd1	právně
existovalo	existovat	k5eAaImAgNnS	existovat
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
asi	asi	k9	asi
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1554	[number]	k4	1554
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
tradici	tradice	k1gFnSc6	tradice
navázalo	navázat	k5eAaPmAgNnS	navázat
biskupství	biskupství	k1gNnSc1	biskupství
královéhradecké	královéhradecký	k2eAgNnSc1d1	královéhradecké
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
litomyšlská	litomyšlský	k2eAgFnSc1d1	Litomyšlská
diecéze	diecéze	k1gFnSc1	diecéze
obnovena	obnovit	k5eAaPmNgFnS	obnovit
nebyla	být	k5eNaImAgFnS	být
a	a	k8xC	a
titul	titul	k1gInSc1	titul
biskup	biskup	k1gMnSc1	biskup
litomyšlský	litomyšlský	k2eAgMnSc1d1	litomyšlský
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
titulárním	titulární	k2eAgMnPc3d1	titulární
biskupům	biskup	k1gMnPc3	biskup
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
titulární	titulární	k2eAgNnSc1d1	titulární
biskupství	biskupství	k1gNnSc1	biskupství
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
bulou	bula	k1gFnSc7	bula
papeže	papež	k1gMnSc2	papež
Pavla	Pavel	k1gMnSc2	Pavel
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1982	[number]	k4	1982
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
titulárním	titulární	k2eAgInSc7d1	titulární
biskupem	biskup	k1gInSc7	biskup
litomyšlským	litomyšlský	k2eAgInSc7d1	litomyšlský
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Škarvada	Škarvada	k1gFnSc1	Škarvada
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
obnovení	obnovení	k1gNnSc2	obnovení
biskupství	biskupství	k1gNnSc2	biskupství
v	v	k7c6	v
Litomyšli	Litomyšl	k1gFnSc6	Litomyšl
byla	být	k5eAaImAgFnS	být
nastolena	nastolit	k5eAaPmNgFnS	nastolit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
skupinou	skupina	k1gFnSc7	skupina
nadšenců	nadšenec	k1gMnPc2	nadšenec
a	a	k8xC	a
řešena	řešen	k2eAgFnSc1d1	řešena
s	s	k7c7	s
královéhradeckým	královéhradecký	k2eAgNnSc7d1	královéhradecké
biskupstvím	biskupství	k1gNnSc7	biskupství
jako	jako	k8xS	jako
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
biskupstvím	biskupství	k1gNnSc7	biskupství
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
vzdát	vzdát	k5eAaPmF	vzdát
největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
farností	farnost	k1gFnPc2	farnost
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
sídelní	sídelní	k2eAgMnSc1d1	sídelní
biskup	biskup	k1gMnSc1	biskup
doporučil	doporučit	k5eAaPmAgMnS	doporučit
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
zatím	zatím	k6eAd1	zatím
nenastolovat	nastolovat	k5eNaImF	nastolovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
povstala	povstat	k5eAaPmAgNnP	povstat
biskupství	biskupství	k1gNnPc1	biskupství
zcela	zcela	k6eAd1	zcela
nová	nový	k2eAgNnPc1d1	nové
<g/>
,	,	kIx,	,
Plzeňské	plzeňský	k2eAgNnSc1d1	plzeňské
a	a	k8xC	a
Ostravsko-opavské	ostravskopavský	k2eAgNnSc1d1	ostravsko-opavské
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
I.	I.	kA	I.
OPraem	OPraem	k1gInSc1	OPraem
(	(	kIx(	(
<g/>
1344	[number]	k4	1344
<g/>
-	-	kIx~	-
<g/>
1353	[number]	k4	1353
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
II	II	kA	II
<g/>
.	.	kIx.	.
ze	z	k7c2	z
Středy	středa	k1gFnSc2	středa
(	(	kIx(	(
<g/>
1353	[number]	k4	1353
<g/>
-	-	kIx~	-
<g/>
1364	[number]	k4	1364
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
)	)	kIx)	)
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
(	(	kIx(	(
<g/>
1364	[number]	k4	1364
<g/>
)	)	kIx)	)
Albrecht	Albrecht	k1gMnSc1	Albrecht
Aleš	Aleš	k1gMnSc1	Aleš
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
(	(	kIx(	(
<g/>
1364	[number]	k4	1364
<g/>
-	-	kIx~	-
<g/>
1368	[number]	k4	1368
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
biskup	biskup	k1gMnSc1	biskup
schwerinský	schwerinský	k2eAgMnSc1d1	schwerinský
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
magdeburský	magdeburský	k2eAgMnSc1d1	magdeburský
<g/>
)	)	kIx)	)
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Jelito	jelito	k1gNnSc1	jelito
(	(	kIx(	(
<g/>
1368	[number]	k4	1368
<g/>
-	-	kIx~	-
<g/>
1371	[number]	k4	1371
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
<g/>
)	)	kIx)	)
Albrecht	Albrecht	k1gMnSc1	Albrecht
Aleš	Aleš	k1gMnSc1	Aleš
ze	z	k7c2	z
Šternberka	Šternberk	k1gInSc2	Šternberk
(	(	kIx(	(
<g/>
1371	[number]	k4	1371
<g/>
-	-	kIx~	-
<g/>
1380	[number]	k4	1380
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Soběslav	Soběslav	k1gMnSc1	Soběslav
(	(	kIx(	(
<g/>
1380	[number]	k4	1380
<g/>
-	-	kIx~	-
<g/>
1387	[number]	k4	1387
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
a	a	k8xC	a
patriarcha	patriarcha	k1gMnSc1	patriarcha
akvilejský	akvilejský	k2eAgMnSc1d1	akvilejský
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
IV	IV	kA	IV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bucek	bucek	k1gMnSc1	bucek
<g/>
)	)	kIx)	)
Železný	Železný	k1gMnSc1	Železný
(	(	kIx(	(
<g/>
1388	[number]	k4	1388
<g/>
-	-	kIx~	-
<g/>
1420	[number]	k4	1420
<g/>
,	,	kIx,	,
od	od	k7c2	od
1418	[number]	k4	1418
jen	jen	k9	jen
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
biskup	biskup	k1gMnSc1	biskup
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
a	a	k8xC	a
kardinál	kardinál	k1gMnSc1	kardinál
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
z	z	k7c2	z
Březí	březí	k1gNnSc2	březí
(	(	kIx(	(
<g/>
1418	[number]	k4	1418
<g/>
/	/	kIx~	/
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
1442	[number]	k4	1442
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
)	)	kIx)	)
od	od	k7c2	od
1425	[number]	k4	1425
diecézi	diecéze	k1gFnSc4	diecéze
fakticky	fakticky	k6eAd1	fakticky
neřídil	řídit	k5eNaImAgMnS	řídit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnPc7	jeho
nástupci	nástupce	k1gMnPc7	nástupce
Matěj	Matěj	k1gMnSc1	Matěj
Kučka	Kučka	k1gMnSc1	Kučka
OPraem	OPraem	k1gInSc1	OPraem
(	(	kIx(	(
<g/>
1442	[number]	k4	1442
<g/>
-	-	kIx~	-
<g/>
1449	[number]	k4	1449
<g/>
)	)	kIx)	)
Beneš	Beneš	k1gMnSc1	Beneš
ze	z	k7c2	z
Svitav	Svitava	k1gFnPc2	Svitava
(	(	kIx(	(
<g/>
před	před	k7c7	před
1453	[number]	k4	1453
<g/>
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
<g/>
po	po	k7c4	po
1457	[number]	k4	1457
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
1458	[number]	k4	1458
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
)	)	kIx)	)
Eliáš	Eliáš	k1gMnSc1	Eliáš
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
před	před	k7c7	před
1468	[number]	k4	1468
<g/>
-	-	kIx~	-
<g/>
1474	[number]	k4	1474
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
V.	V.	kA	V.
Bavor	Bavor	k1gMnSc1	Bavor
OPraem	OPraem	k1gInSc1	OPraem
(	(	kIx(	(
<g/>
1474	[number]	k4	1474
<g/>
-	-	kIx~	-
<g/>
před	před	k7c7	před
<g />
.	.	kIx.	.
</s>
<s>
1478	[number]	k4	1478
<g/>
)	)	kIx)	)
Eliáš	Eliáš	k1gMnSc1	Eliáš
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
před	před	k7c7	před
1478	[number]	k4	1478
<g/>
-	-	kIx~	-
<g/>
po	po	k7c6	po
1492	[number]	k4	1492
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
(	(	kIx(	(
<g/>
1552	[number]	k4	1552
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
)	)	kIx)	)
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
(	(	kIx(	(
<g/>
1554	[number]	k4	1554
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Škarvada	Škarvada	k1gFnSc1	Škarvada
(	(	kIx(	(
<g/>
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1982	[number]	k4	1982
do	do	k7c2	do
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Pavel	Pavel	k1gMnSc1	Pavel
Konzbul	Konzbula	k1gFnPc2	Konzbula
<g/>
,	,	kIx,	,
jmenován	jmenován	k2eAgMnSc1d1	jmenován
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
</s>
