<p>
<s>
Athamás	Athamás	k1gInSc1	Athamás
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Α	Α	k?	Α
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Athamas	Athamas	k1gMnSc1	Athamas
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
syn	syn	k1gMnSc1	syn
theassalského	theassalský	k2eAgMnSc2d1	theassalský
krále	král	k1gMnSc2	král
Aiola	Aiol	k1gMnSc2	Aiol
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
děd	děd	k1gMnSc1	děd
byl	být	k5eAaImAgMnS	být
Hellén	Hellén	k1gInSc4	Hellén
<g/>
,	,	kIx,	,
praotec	praotec	k1gMnSc1	praotec
všech	všecek	k3xTgMnPc2	všecek
Řeků	Řek	k1gMnPc2	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
králem	král	k1gMnSc7	král
boiótského	boiótský	k2eAgInSc2d1	boiótský
Orchomenu	Orchomen	k2eAgFnSc4d1	Orchomen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
bratry	bratr	k1gMnPc7	bratr
byli	být	k5eAaImAgMnP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Krétheus	Krétheus	k1gMnSc1	Krétheus
založil	založit	k5eAaPmAgMnS	založit
město	město	k1gNnSc4	město
Iólkos	Iólkos	k1gMnSc1	Iólkos
</s>
</p>
<p>
<s>
Sísyfos	Sísyfos	k1gMnSc1	Sísyfos
založil	založit	k5eAaPmAgMnS	založit
Korinth	Korinth	k1gInSc4	Korinth
</s>
</p>
<p>
<s>
Salmóneus	Salmóneus	k1gInSc1	Salmóneus
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
Salmóně	Salmón	k1gInSc6	Salmón
</s>
</p>
<p>
<s>
Deión	Deión	k1gInSc1	Deión
vládl	vládnout	k5eAaImAgInS	vládnout
ve	v	k7c6	v
Fókidě	Fókida	k1gFnSc6	Fókida
</s>
</p>
<p>
<s>
Periérés	Periérés	k1gInSc1	Periérés
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c6	v
Messéně	Messéna	k1gFnSc6	Messéna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Magnés	Magnés	k1gInSc1	Magnés
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c4	v
Magnésii	Magnésii	k1gInSc4	Magnésii
</s>
<s>
Athamás	Athamás	k1gInSc1	Athamás
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
ženou	žena	k1gFnSc7	žena
byla	být	k5eAaImAgFnS	být
bohyně	bohyně	k1gFnSc1	bohyně
oblaků	oblak	k1gInPc2	oblak
Nefelé	Nefelý	k2eAgInPc1d1	Nefelý
<g/>
.	.	kIx.	.
</s>
<s>
Porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gNnSc3	on
syna	syn	k1gMnSc4	syn
Frixa	Frixus	k1gMnSc4	Frixus
a	a	k8xC	a
dceru	dcera	k1gFnSc4	dcera
Hellé	Hellý	k2eAgFnPc4d1	Hellý
<g/>
.	.	kIx.	.
</s>
<s>
Nešťastný	šťastný	k2eNgInSc1d1	nešťastný
osud	osud	k1gInSc1	osud
sobě	se	k3xPyFc3	se
i	i	k8xC	i
dětem	dítě	k1gFnPc3	dítě
přivodil	přivodit	k5eAaPmAgMnS	přivodit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zapudil	zapudit	k5eAaPmAgInS	zapudit
Nefelé	Nefelý	k2eAgFnPc4d1	Nefelý
a	a	k8xC	a
oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
Ínó	Ínó	k1gFnSc7	Ínó
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Kadma	Kadm	k1gMnSc2	Kadm
<g/>
,	,	kIx,	,
krále	král	k1gMnSc2	král
v	v	k7c6	v
Thébách	Théby	k1gFnPc6	Théby
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
měl	mít	k5eAaImAgMnS	mít
později	pozdě	k6eAd2	pozdě
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Learcha	Learch	k1gMnSc4	Learch
a	a	k8xC	a
Melikerta	Melikert	k1gMnSc4	Melikert
<g/>
.	.	kIx.	.
</s>
<s>
Macecha	macecha	k1gFnSc1	macecha
Ínó	Ínó	k1gFnSc2	Ínó
děti	dítě	k1gFnPc1	dítě
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
nenáviděla	návidět	k5eNaImAgFnS	návidět
až	až	k6eAd1	až
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
úklady	úklad	k1gInPc7	úklad
a	a	k8xC	a
intrikami	intrika	k1gFnPc7	intrika
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
souhlasu	souhlas	k1gInSc2	souhlas
Athamásova	Athamásův	k2eAgFnSc1d1	Athamásův
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
obětováním	obětování	k1gNnSc7	obětování
bohům	bůh	k1gMnPc3	bůh
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránila	zachránit	k5eAaPmAgFnS	zachránit
království	království	k1gNnSc4	království
od	od	k7c2	od
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
bídy	bída	k1gFnSc2	bída
a	a	k8xC	a
nepokojů	nepokoj	k1gInPc2	nepokoj
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
obětním	obětní	k2eAgInSc7d1	obětní
obřadem	obřad	k1gInSc7	obřad
matka	matka	k1gFnSc1	matka
dětí	dítě	k1gFnPc2	dítě
Nefelé	Nefelý	k2eAgFnSc2d1	Nefelý
poslala	poslat	k5eAaPmAgFnS	poslat
zlatého	zlatý	k2eAgMnSc4d1	zlatý
berana	beran	k1gMnSc4	beran
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
děti	dítě	k1gFnPc4	dítě
přenesl	přenést	k5eAaPmAgMnS	přenést
přes	přes	k7c4	přes
moře	moře	k1gNnSc4	moře
do	do	k7c2	do
daleké	daleký	k2eAgFnSc2d1	daleká
Kolchidy	Kolchida	k1gFnSc2	Kolchida
<g/>
.	.	kIx.	.
</s>
<s>
Šťastně	šťastně	k6eAd1	šťastně
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
skončilo	skončit	k5eAaPmAgNnS	skončit
pro	pro	k7c4	pro
Frixa	Frixum	k1gNnPc4	Frixum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Hellé	Hellý	k1gMnPc4	Hellý
utonula	utonout	k5eAaPmAgFnS	utonout
v	v	k7c6	v
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
Athamás	Athamás	k1gInSc4	Athamás
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c4	v
nemilost	nemilost	k1gFnSc4	nemilost
bohyně	bohyně	k1gFnSc2	bohyně
Héry	Héra	k1gFnSc2	Héra
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
malého	malý	k2eAgMnSc4d1	malý
Dionýsa	Dionýsos	k1gMnSc4	Dionýsos
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
nejvyššího	vysoký	k2eAgMnSc4d3	nejvyšší
boha	bůh	k1gMnSc4	bůh
Dia	Dia	k1gFnSc2	Dia
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
milenky	milenka	k1gFnSc2	milenka
Semely	semel	k1gInPc4	semel
<g/>
.	.	kIx.	.
</s>
<s>
Héra	Héra	k1gFnSc1	Héra
na	na	k7c6	na
Athamáse	Athamása	k1gFnSc6	Athamása
seslala	seslat	k5eAaPmAgFnS	seslat
šílenství	šílenství	k1gNnSc4	šílenství
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
zabil	zabít	k5eAaPmAgMnS	zabít
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
Learcha	Learch	k1gMnSc4	Learch
<g/>
.	.	kIx.	.
</s>
<s>
Nestačil	stačit	k5eNaBmAgInS	stačit
už	už	k6eAd1	už
ublížit	ublížit	k5eAaPmF	ublížit
druhému	druhý	k4xOgMnSc3	druhý
Melikartovi	Melikart	k1gMnSc3	Melikart
<g/>
,	,	kIx,	,
Ínó	Ínó	k1gMnSc3	Ínó
mu	on	k3xPp3gMnSc3	on
ho	on	k3xPp3gNnSc4	on
vytrhla	vytrhnout	k5eAaPmAgFnS	vytrhnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neunikla	uniknout	k5eNaPmAgFnS	uniknout
a	a	k8xC	a
vrhla	vrhnout	k5eAaImAgFnS	vrhnout
se	se	k3xPyFc4	se
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
šílený	šílený	k2eAgInSc1d1	šílený
Athamás	Athamás	k1gInSc1	Athamás
pokusil	pokusit	k5eAaPmAgInS	pokusit
zabít	zabít	k5eAaPmF	zabít
i	i	k9	i
Dionýsa	Dionýsos	k1gMnSc4	Dionýsos
<g/>
,	,	kIx,	,
skryl	skrýt	k5eAaPmAgMnS	skrýt
jej	on	k3xPp3gMnSc4	on
bůh	bůh	k1gMnSc1	bůh
Hermés	Hermés	k1gInSc1	Hermés
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konec	konec	k1gInSc1	konec
Athamantův	Athamantův	k2eAgInSc1d1	Athamantův
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
mýtech	mýtus	k1gInPc6	mýtus
doložen	doložen	k2eAgMnSc1d1	doložen
<g/>
,	,	kIx,	,
sebevražda	sebevražda	k1gFnSc1	sebevražda
je	být	k5eAaImIp3nS	být
předpokládaná	předpokládaný	k2eAgFnSc1d1	předpokládaná
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
Athamanta	Athamant	k1gMnSc2	Athamant
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
Frixa	Frixus	k1gMnSc2	Frixus
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
dalších	další	k2eAgInPc2d1	další
velkých	velký	k2eAgInPc2d1	velký
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
výpravě	výprava	k1gFnSc6	výprava
</s>
</p>
<p>
<s>
Iásona	Iáson	k1gMnSc2	Iáson
a	a	k8xC	a
Argonautů	argonaut	k1gMnPc2	argonaut
do	do	k7c2	do
Kolchidy	Kolchida	k1gFnSc2	Kolchida
pro	pro	k7c4	pro
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
rouno	rouno	k1gNnSc1	rouno
</s>
</p>
<p>
<s>
Argonauti	argonaut	k1gMnPc1	argonaut
</s>
</p>
<p>
<s>
Dionýsos	Dionýsos	k1gMnSc1	Dionýsos
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnSc2	antika
</s>
</p>
<p>
<s>
Publius	Publius	k1gMnSc1	Publius
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
,	,	kIx,	,
Starověké	starověký	k2eAgFnPc1d1	starověká
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
,	,	kIx,	,
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
</s>
</p>
<p>
<s>
Guus	Guus	k6eAd1	Guus
Houtzager	Houtzager	k1gInSc1	Houtzager
<g/>
,	,	kIx,	,
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-287-8	[number]	k4	80-7234-287-8
</s>
</p>
