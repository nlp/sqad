<s>
Amazonky	Amazonka	k1gFnPc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Amazonky	Amazonka	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Boj	boj	k1gInSc1
Řeků	Řek	k1gMnPc2
s	s	k7c7
Amazonkami	Amazonka	k1gFnPc7
<g/>
,	,	kIx,
reliéf	reliéf	k1gInSc4
na	na	k7c6
sarkofágu	sarkofág	k1gInSc6
<g/>
,	,	kIx,
Soluň	Soluň	k1gFnSc1
</s>
<s>
Amazonky	Amazonka	k1gFnPc1
(	(	kIx(
<g/>
řecky	řecky	k6eAd1
Ἀ	Ἀ	k?
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Amazones	Amazones	k1gInSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
v	v	k7c6
řecké	řecký	k2eAgFnSc6d1
mytologii	mytologie	k1gFnSc6
mytický	mytický	k2eAgInSc1d1
národ	národ	k1gInSc1
bojovných	bojovný	k2eAgFnPc2d1
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Sídlily	sídlit	k5eAaImAgFnP
pravděpodobně	pravděpodobně	k6eAd1
na	na	k7c6
severním	severní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Malé	Malé	k2eAgFnSc2d1
Asie	Asie	k1gFnSc2
nebo	nebo	k8xC
ještě	ještě	k9
východněji	východně	k6eAd2
ke	k	k7c3
Kavkazu	Kavkaz	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvádí	uvádět	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
oblast	oblast	k1gFnSc1
Krymu	Krym	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Byly	být	k5eAaImAgFnP
národem	národ	k1gInSc7
<g/>
,	,	kIx,
kterému	který	k3yRgNnSc3
vládly	vládnout	k5eAaImAgFnP
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cvičily	cvičit	k5eAaImAgInP
se	se	k3xPyFc4
v	v	k7c6
boji	boj	k1gInSc6
<g/>
,	,	kIx,
sloužily	sloužit	k5eAaImAgInP
ve	v	k7c6
vojsku	vojsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
zůstávaly	zůstávat	k5eAaImAgInP
pannami	panna	k1gFnPc7
<g/>
,	,	kIx,
teprve	teprve	k6eAd1
po	po	k7c6
skončení	skončení	k1gNnSc6
vojenské	vojenský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
se	se	k3xPyFc4
vdávaly	vdávat	k5eAaImAgFnP
a	a	k8xC
rodily	rodit	k5eAaImAgFnP
děti	dítě	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejná	veřejný	k2eAgFnSc1d1
moc	moc	k1gFnSc1
však	však	k9
zůstávala	zůstávat	k5eAaImAgFnS
v	v	k7c6
jejich	jejich	k3xOp3gFnPc6
rukou	ruka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
muži	muž	k1gMnPc1
vedli	vést	k5eAaImAgMnP
domácí	domácí	k2eAgInSc4d1
život	život	k1gInSc4
s	s	k7c7
povinnostmi	povinnost	k1gFnPc7
<g/>
,	,	kIx,
jaké	jaký	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
zastávaly	zastávat	k5eAaImAgFnP
jinde	jinde	k6eAd1
vdané	vdaný	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Jméno	jméno	k1gNnSc1
Amazonek	Amazonka	k1gFnPc2
je	být	k5eAaImIp3nS
vykládáno	vykládat	k5eAaImNgNnS
od	od	k7c2
slova	slovo	k1gNnSc2
„	„	k?
<g/>
amazoi	amazoi	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
bezprsé	bezprsý	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
kvůli	kvůli	k7c3
boji	boj	k1gInSc6
si	se	k3xPyFc3
prý	prý	k9
v	v	k7c6
mládí	mládí	k1gNnSc6
odstraňovaly	odstraňovat	k5eAaImAgInP
jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
oba	dva	k4xCgInPc4
prsy	prs	k1gInPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nepřekážely	překážet	k5eNaImAgFnP
<g/>
.	.	kIx.
</s>
<s>
Styky	styk	k1gInPc4
Řeků	Řek	k1gMnPc2
s	s	k7c7
Amazonkami	Amazonka	k1gFnPc7
byly	být	k5eAaImAgFnP
téměř	téměř	k6eAd1
bez	bez	k7c2
výjimky	výjimka	k1gFnSc2
válečné	válečný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
proti	proti	k7c3
jejich	jejich	k3xOp3gFnSc3
královně	královna	k1gFnSc3
Hippolytě	Hippolyt	k1gInSc6
vedl	vést	k5eAaImAgMnS
válečnou	válečný	k2eAgFnSc4d1
výpravu	výprava	k1gFnSc4
velký	velký	k2eAgMnSc1d1
hrdina	hrdina	k1gMnSc1
Héraklés	Héraklésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doprovázel	doprovázet	k5eAaImAgMnS
ho	on	k3xPp3gNnSc4
athénský	athénský	k2eAgMnSc1d1
král	král	k1gMnSc1
Théseus	Théseus	k1gMnSc1
a	a	k8xC
přivedl	přivést	k5eAaPmAgMnS
si	se	k3xPyFc3
zajatou	zajatý	k2eAgFnSc4d1
vůdkyni	vůdkyně	k1gFnSc4
Amazonek	Amazonka	k1gFnPc2
Antiopu	Antiop	k1gInSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
ji	on	k3xPp3gFnSc4
přišly	přijít	k5eAaPmAgFnP
její	její	k3xOp3gFnPc1
družky	družka	k1gFnPc1
osvobodit	osvobodit	k5eAaPmF
<g/>
,	,	kIx,
zamilovaná	zamilovaný	k2eAgFnSc1d1
Antiopa	Antiopa	k1gFnSc1
s	s	k7c7
manželem	manžel	k1gMnSc7
bránila	bránit	k5eAaImAgFnS
Athény	Athéna	k1gFnSc2
a	a	k8xC
v	v	k7c6
boji	boj	k1gInSc6
zahynula	zahynout	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Amazonkám	Amazonka	k1gFnPc3
táhl	táhnout	k5eAaImAgInS
také	také	k9
hrdina	hrdina	k1gMnSc1
Bellerofontés	Bellerofontésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
známý	známý	k2eAgInSc1d1
válečný	válečný	k2eAgInSc1d1
stav	stav	k1gInSc1
zažily	zažít	k5eAaPmAgFnP
Amazonky	Amazonka	k1gFnPc1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
královnou	královna	k1gFnSc7
Penthesileou	Penthesilea	k1gFnSc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přišly	přijít	k5eAaPmAgFnP
na	na	k7c4
pomoc	pomoc	k1gFnSc4
obléhané	obléhaný	k2eAgFnSc3d1
Tróji	Trója	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královna	královna	k1gFnSc1
zahynula	zahynout	k5eAaPmAgFnS
v	v	k7c6
souboji	souboj	k1gInSc6
s	s	k7c7
achajským	achajský	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
Achileem	Achileus	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amazonky	Amazonka	k1gFnPc4
dostaly	dostat	k5eAaPmAgFnP
zpět	zpět	k6eAd1
Penthesileino	Penthesilein	k2eAgNnSc4d1
mrtvé	mrtvý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
<g/>
,	,	kIx,
avšak	avšak	k8xC
přísahou	přísaha	k1gFnSc7
se	se	k3xPyFc4
musely	muset	k5eAaImAgInP
zavázat	zavázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
nikdy	nikdy	k6eAd1
proti	proti	k7c3
Řekům	Řek	k1gMnPc3
nezvednou	zvednout	k5eNaPmIp3nP
zbraně	zbraň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
umění	umění	k1gNnSc6
</s>
<s>
Bitva	bitva	k1gFnSc1
Amazonek	Amazonka	k1gFnPc2
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Paul	Paul	k1gMnSc1
Rubens	Rubensa	k1gFnPc2
<g/>
,	,	kIx,
1619	#num#	k4
</s>
<s>
Antonín	Antonín	k1gMnSc1
Tesař	Tesař	k1gMnSc1
<g/>
:	:	kIx,
Amazonka	Amazonka	k1gFnSc1
<g/>
,	,	kIx,
1993	#num#	k4
</s>
<s>
Bojové	bojový	k2eAgFnSc2d1
akce	akce	k1gFnSc2
Amazonek	Amazonka	k1gFnPc2
proti	proti	k7c3
Řekům	Řek	k1gMnPc3
byly	být	k5eAaImAgFnP
častými	častý	k2eAgMnPc7d1
náměty	námět	k1gInPc4
antických	antický	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
<g/>
,	,	kIx,
zachovaly	zachovat	k5eAaPmAgInP
se	se	k3xPyFc4
jako	jako	k9
obvykle	obvykle	k6eAd1
nejčastěji	často	k6eAd3
na	na	k7c6
vázových	vázový	k2eAgFnPc6d1
malbách	malba	k1gFnPc6
či	či	k8xC
reliéfech	reliéf	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejlepší	dobrý	k2eAgNnSc4d3
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Boj	boj	k1gInSc1
Řeků	Řek	k1gMnPc2
s	s	k7c7
Amazonkami	Amazonka	k1gFnPc7
na	na	k7c6
mauzoleu	mauzoleum	k1gNnSc6
v	v	k7c6
Halikarnassu	Halikarnass	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořili	vytvořit	k5eAaPmAgMnP
jej	on	k3xPp3gMnSc4
po	po	k7c6
roce	rok	k1gInSc6
352	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Skopás	Skopása	k1gFnPc2
<g/>
,	,	kIx,
Leocharés	Leocharésa	k1gFnPc2
<g/>
,	,	kIx,
Bryaxis	Bryaxis	k1gFnPc2
a	a	k8xC
Praxitelés	Praxitelésa	k1gFnPc2
</s>
<s>
Boj	boj	k1gInSc1
s	s	k7c7
Amazonkami	Amazonka	k1gFnPc7
je	být	k5eAaImIp3nS
také	také	k9
v	v	k7c6
Britském	britský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
</s>
<s>
římské	římský	k2eAgFnPc1d1
kopie	kopie	k1gFnPc1
Amazonek	Amazonka	k1gFnPc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
Vatikánském	vatikánský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
,	,	kIx,
originály	originál	k1gInPc1
jsou	být	k5eAaImIp3nP
údajně	údajně	k6eAd1
dílem	dílo	k1gNnSc7
Feidia	Feidium	k1gNnSc2
<g/>
,	,	kIx,
Polykleita	Polykleitum	k1gNnSc2
a	a	k8xC
Krésila	Krésila	k1gFnSc2
</s>
<s>
z	z	k7c2
novodobých	novodobý	k2eAgInPc2d1
obrazů	obraz	k1gInPc2
je	být	k5eAaImIp3nS
nejznámější	známý	k2eAgFnSc1d3
Bitva	bitva	k1gFnSc1
Amazonek	Amazonka	k1gFnPc2
od	od	k7c2
Petra	Petr	k1gMnSc2
Paula	Paul	k1gMnSc2
Rubense	Rubens	k1gMnSc2
(	(	kIx(
<g/>
z	z	k7c2
doby	doba	k1gFnSc2
kolem	kolem	k7c2
1618	#num#	k4
<g/>
,	,	kIx,
dnes	dnes	k6eAd1
v	v	k7c6
mnichovské	mnichovský	k2eAgFnSc6d1
Pinakotéce	pinakotéka	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1541	#num#	k4
Francisco	Francisco	k1gMnSc1
de	de	k?
Orellanana	Orellanan	k1gMnSc4
objevil	objevit	k5eAaPmAgMnS
na	na	k7c6
výpravě	výprava	k1gFnSc6
po	po	k7c6
toku	tok	k1gInSc6
jihoamerické	jihoamerický	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
indiánský	indiánský	k2eAgInSc1d1
kmen	kmen	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yIgInSc3,k3yQgInSc3,k3yRgInSc3
v	v	k7c6
roli	role	k1gFnSc6
náčelnice	náčelnice	k1gFnSc2
vládla	vládnout	k5eAaImAgFnS
žena	žena	k1gFnSc1
a	a	k8xC
tento	tento	k3xDgMnSc1
dobyvatel	dobyvatel	k1gMnSc1
nazval	nazvat	k5eAaPmAgMnS,k5eAaBmAgMnS
proto	proto	k8xC
tuto	tento	k3xDgFnSc4
řeku	řeka	k1gFnSc4
Amazonkou	Amazonka	k1gFnSc7
.	.	kIx.
</s>
<s>
Slovo	slovo	k1gNnSc1
amazonka	amazonka	k1gFnSc1
má	mít	k5eAaImIp3nS
přeneseně	přeneseně	k6eAd1
ještě	ještě	k6eAd1
další	další	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
bojovná	bojovný	k2eAgFnSc1d1
<g/>
,	,	kIx,
statečná	statečný	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
;	;	kIx,
mužatka	mužatka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Slovník	slovník	k1gInSc4
spisovného	spisovný	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
<g/>
.	.	kIx.
ssjc	ssjc	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ujc	ujc	k?
<g/>
.	.	kIx.
<g/>
cas	cas	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Slovník	slovník	k1gInSc1
antické	antický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Svoboda	Svoboda	k1gMnSc1
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
.	.	kIx.
717	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Členská	členský	k2eAgFnSc1d1
knižnice	knižnice	k1gFnSc1
nakl	nakla	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svoboda	Svoboda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Amazonky	Amazonka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
amazonka	amazonka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4142171-1	4142171-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85004138	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85004138	#num#	k4
</s>
