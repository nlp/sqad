<s>
Matrika	matrika	k1gFnSc1	matrika
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
matrix	matrix	k1gInSc1	matrix
<g/>
,	,	kIx,	,
matice	matice	k1gFnSc1	matice
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veřejný	veřejný	k2eAgInSc4d1	veřejný
úřední	úřední	k2eAgInSc4d1	úřední
seznam	seznam	k1gInSc4	seznam
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
seznam	seznam	k1gInSc4	seznam
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
sňatků	sňatek	k1gInPc2	sňatek
<g/>
,	,	kIx,	,
registrovaného	registrovaný	k2eAgNnSc2d1	registrované
partnerství	partnerství	k1gNnSc2	partnerství
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vedou	vést	k5eAaImIp3nP	vést
matrikáři	matrikář	k1gMnPc1	matrikář
na	na	k7c6	na
určených	určený	k2eAgInPc6d1	určený
matričních	matriční	k2eAgInPc6d1	matriční
úřadech	úřad	k1gInPc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Agenda	agenda	k1gFnSc1	agenda
matrik	matrika	k1gFnPc2	matrika
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
301	[number]	k4	301
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
a	a	k8xC	a
vyhláškou	vyhláška	k1gFnSc7	vyhláška
207	[number]	k4	207
<g/>
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
kompetence	kompetence	k1gFnSc2	kompetence
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nověji	nově	k6eAd2	nově
zavedených	zavedený	k2eAgInPc2d1	zavedený
seznamů	seznam	k1gInPc2	seznam
a	a	k8xC	a
evidencí	evidence	k1gFnPc2	evidence
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
jiné	jiný	k2eAgInPc1d1	jiný
názvy	název	k1gInPc1	název
(	(	kIx(	(
<g/>
seznam	seznam	k1gInSc1	seznam
<g/>
,	,	kIx,	,
registr	registr	k1gInSc1	registr
<g/>
,	,	kIx,	,
rejstřík	rejstřík	k1gInSc1	rejstřík
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matriky	matrika	k1gFnPc1	matrika
patrně	patrně	k6eAd1	patrně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
jako	jako	k9	jako
seznamy	seznam	k1gInPc1	seznam
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
povinností	povinnost	k1gFnPc2	povinnost
vůči	vůči	k7c3	vůči
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
dávek	dávka	k1gFnPc2	dávka
a	a	k8xC	a
počtů	počet	k1gInPc2	počet
ozbrojenců	ozbrojenec	k1gMnPc2	ozbrojenec
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
mají	mít	k5eAaImIp3nP	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
nim	on	k3xPp3gInPc3	on
přibyly	přibýt	k5eAaPmAgInP	přibýt
matriky	matrika	k1gFnPc1	matrika
studentů	student	k1gMnPc2	student
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
snad	snad	k9	snad
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
i	i	k9	i
pojmy	pojem	k1gInPc1	pojem
imatrikulace	imatrikulace	k1gFnSc2	imatrikulace
(	(	kIx(	(
<g/>
zápisu	zápis	k1gInSc2	zápis
<g/>
)	)	kIx)	)
a	a	k8xC	a
exmatrikulace	exmatrikulace	k1gFnSc1	exmatrikulace
(	(	kIx(	(
<g/>
vyřazení	vyřazení	k1gNnSc1	vyřazení
absolventa	absolvent	k1gMnSc2	absolvent
<g/>
)	)	kIx)	)
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
dnešním	dnešní	k2eAgInSc6d1	dnešní
významu	význam	k1gInSc6	význam
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupující	postupující	k2eAgFnSc7d1	postupující
byrokratizací	byrokratizace	k1gFnSc7	byrokratizace
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
rostl	růst	k5eAaImAgInS	růst
i	i	k9	i
počet	počet	k1gInSc4	počet
a	a	k8xC	a
význam	význam	k1gInSc4	význam
podobných	podobný	k2eAgInPc2d1	podobný
úředních	úřední	k2eAgInPc2d1	úřední
seznamů	seznam	k1gInPc2	seznam
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
někde	někde	k6eAd1	někde
říkalo	říkat	k5eAaImAgNnS	říkat
také	také	k9	také
matriky	matrika	k1gFnPc1	matrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgFnP	být
matriky	matrika	k1gFnPc1	matrika
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
úředních	úřední	k2eAgNnPc2d1	úřední
místních	místní	k2eAgNnPc2d1	místní
jmen	jméno	k1gNnPc2	jméno
(	(	kIx(	(
<g/>
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgFnPc1d1	církevní
matriky	matrika	k1gFnPc1	matrika
matriky	matrika	k1gFnSc2	matrika
narození	narození	k1gNnSc2	narození
<g/>
,	,	kIx,	,
pokřtění	pokřtění	k1gNnSc2	pokřtění
<g/>
,	,	kIx,	,
sňatků	sňatek	k1gInPc2	sňatek
a	a	k8xC	a
úmrtí	úmrtí	k1gNnSc2	úmrtí
místy	místy	k6eAd1	místy
vedly	vést	k5eAaImAgFnP	vést
církve	církev	k1gFnPc1	církev
už	už	k6eAd1	už
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Katolickým	katolický	k2eAgMnPc3d1	katolický
farářům	farář	k1gMnPc3	farář
to	ten	k3xDgNnSc4	ten
však	však	k9	však
povinně	povinně	k6eAd1	povinně
nařídil	nařídit	k5eAaPmAgInS	nařídit
až	až	k9	až
Tridentský	tridentský	k2eAgInSc1d1	tridentský
koncil	koncil	k1gInSc1	koncil
roku	rok	k1gInSc2	rok
1563	[number]	k4	1563
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
však	však	k9	však
toto	tento	k3xDgNnSc1	tento
nařízení	nařízení	k1gNnSc1	nařízení
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
poměrně	poměrně	k6eAd1	poměrně
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
našem	náš	k3xOp1gInSc6	náš
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
matrikou	matrika	k1gFnSc7	matrika
německy	německy	k6eAd1	německy
psaná	psaný	k2eAgFnSc1d1	psaná
matirka	matirka	k1gFnSc1	matirka
oddaných	oddaný	k2eAgMnPc2d1	oddaný
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1531	[number]	k4	1531
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1591	[number]	k4	1591
svolal	svolat	k5eAaPmAgMnS	svolat
Stanislav	Stanislav	k1gMnSc1	Stanislav
Pavlovský	pavlovský	k2eAgMnSc1d1	pavlovský
olomouckou	olomoucký	k2eAgFnSc4d1	olomoucká
diecézní	diecézní	k2eAgFnSc4d1	diecézní
synodu	synoda	k1gFnSc4	synoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
formálně	formálně	k6eAd1	formálně
přijala	přijmout	k5eAaPmAgFnS	přijmout
usnesení	usnesení	k1gNnSc4	usnesení
tridentského	tridentský	k2eAgInSc2d1	tridentský
koncilu	koncil	k1gInSc2	koncil
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1605	[number]	k4	1605
pak	pak	k6eAd1	pak
Zbyněk	Zbyněk	k1gMnSc1	Zbyněk
Berka	Berka	k1gMnSc1	Berka
z	z	k7c2	z
Dubé	Dubá	k1gFnSc2	Dubá
a	a	k8xC	a
Lipé	Lipá	k1gFnSc2	Lipá
svolal	svolat	k5eAaPmAgInS	svolat
arcidiecézní	arcidiecézní	k2eAgFnSc4d1	arcidiecézní
synodu	synoda	k1gFnSc4	synoda
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
se	se	k3xPyFc4	se
farní	farní	k2eAgFnSc2d1	farní
matriky	matrika	k1gFnSc2	matrika
běžně	běžně	k6eAd1	běžně
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
až	až	k9	až
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Reformy	reforma	k1gFnPc1	reforma
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1781	[number]	k4	1781
a	a	k8xC	a
1784	[number]	k4	1784
zavedly	zavést	k5eAaPmAgFnP	zavést
jednotné	jednotný	k2eAgNnSc4d1	jednotné
vedení	vedení	k1gNnSc4	vedení
matrik	matrika	k1gFnPc2	matrika
(	(	kIx(	(
<g/>
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
reformou	reforma	k1gFnSc7	reforma
byla	být	k5eAaImAgFnS	být
podoba	podoba	k1gFnSc1	podoba
matrik	matrika	k1gFnPc2	matrika
značně	značně	k6eAd1	značně
roztříštěná	roztříštěný	k2eAgNnPc1d1	roztříštěné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ukládaly	ukládat	k5eAaImAgInP	ukládat
také	také	k9	také
povinnost	povinnost	k1gFnSc4	povinnost
vést	vést	k5eAaImF	vést
matriky	matrika	k1gFnPc4	matrika
protestantským	protestantský	k2eAgInPc3d1	protestantský
farním	farní	k2eAgInPc3d1	farní
úřadům	úřad	k1gInPc3	úřad
a	a	k8xC	a
rabínům	rabín	k1gMnPc3	rabín
židovských	židovský	k2eAgFnPc2d1	židovská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
vedly	vést	k5eAaImAgFnP	vést
matriku	matrika	k1gFnSc4	matrika
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
obecní	obecní	k2eAgInPc4d1	obecní
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
zavedla	zavést	k5eAaPmAgFnS	zavést
obecná	obecný	k2eAgFnSc1d1	obecná
a	a	k8xC	a
povinná	povinný	k2eAgFnSc1d1	povinná
veřejná	veřejný	k2eAgFnSc1d1	veřejná
matrika	matrika	k1gFnSc1	matrika
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
a	a	k8xC	a
farní	farní	k2eAgFnPc1d1	farní
matriky	matrika	k1gFnPc1	matrika
ztratily	ztratit	k5eAaPmAgFnP	ztratit
občansko-právní	občanskorávní	k2eAgInSc4d1	občansko-právní
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jejich	jejich	k3xOp3gInSc1	jejich
kanonicko-právní	kanonickorávní	k2eAgInSc1d1	kanonicko-právní
význam	význam	k1gInSc1	význam
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
.	.	kIx.	.
</s>
<s>
Odejmutím	odejmutí	k1gNnSc7	odejmutí
matrik	matrika	k1gFnPc2	matrika
pro	pro	k7c4	pro
civilní	civilní	k2eAgInPc4d1	civilní
účely	účel	k1gInPc4	účel
byl	být	k5eAaImAgInS	být
církvím	církev	k1gFnPc3	církev
ztížen	ztížen	k2eAgInSc4d1	ztížen
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vlastním	vlastní	k2eAgInPc3d1	vlastní
záznamům	záznam	k1gInPc3	záznam
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
jakási	jakýsi	k3yIgFnSc1	jakýsi
"	"	kIx"	"
<g/>
matriční	matriční	k2eAgFnSc1d1	matriční
odluka	odluka	k1gFnSc1	odluka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
předpisech	předpis	k1gInPc6	předpis
nařízeno	nařídit	k5eAaPmNgNnS	nařídit
vést	vést	k5eAaImF	vést
matriky	matrika	k1gFnPc1	matrika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
potřeby	potřeba	k1gFnPc1	potřeba
vedly	vést	k5eAaImAgFnP	vést
(	(	kIx(	(
<g/>
vedou	vést	k5eAaImIp3nP	vést
<g/>
)	)	kIx)	)
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Matriky	matrika	k1gFnPc1	matrika
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
podle	podle	k7c2	podle
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
byly	být	k5eAaImAgInP	být
<g/>
/	/	kIx~	/
<g/>
jsou	být	k5eAaImIp3nP	být
vedeny	vést	k5eAaImNgInP	vést
<g/>
,	,	kIx,	,
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
živé	živý	k2eAgFnPc1d1	živá
a	a	k8xC	a
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
<g/>
.	.	kIx.	.
</s>
<s>
Živé	živý	k2eAgFnPc1d1	živá
matriky	matrika	k1gFnPc1	matrika
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
matričních	matriční	k2eAgInPc6d1	matriční
úřadech	úřad	k1gInPc6	úřad
(	(	kIx(	(
<g/>
100	[number]	k4	100
let	léto	k1gNnPc2	léto
od	od	k7c2	od
posledního	poslední	k2eAgInSc2d1	poslední
zápisu	zápis	k1gInSc2	zápis
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
narození	narození	k1gNnSc2	narození
a	a	k8xC	a
75	[number]	k4	75
let	léto	k1gNnPc2	léto
od	od	k7c2	od
posledního	poslední	k2eAgInSc2d1	poslední
zápisu	zápis	k1gInSc2	zápis
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
knize	kniha	k1gFnSc3	kniha
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
matriky	matrika	k1gFnPc1	matrika
pak	pak	k6eAd1	pak
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
městském	městský	k2eAgInSc6d1	městský
(	(	kIx(	(
<g/>
Archiv	archiv	k1gInSc4	archiv
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěti	pět	k4xCc2	pět
státních	státní	k2eAgMnPc2d1	státní
oblastních	oblastní	k2eAgMnPc2d1	oblastní
(	(	kIx(	(
<g/>
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Třeboni	Třeboň	k1gFnSc6	Třeboň
<g/>
,	,	kIx,	,
Zámrsku	Zámrsko	k1gNnSc6	Zámrsko
a	a	k8xC	a
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dvou	dva	k4xCgMnPc6	dva
zemských	zemský	k2eAgFnPc2d1	zemská
(	(	kIx(	(
<g/>
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Opavě	Opava	k1gFnSc6	Opava
<g/>
)	)	kIx)	)
archivech	archiv	k1gInPc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgFnPc1d1	židovská
matriky	matrika	k1gFnPc1	matrika
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgFnP	uložit
v	v	k7c6	v
Národním	národní	k2eAgInSc6d1	národní
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc1d1	vojenská
matriky	matrika	k1gFnPc1	matrika
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Vojenském	vojenský	k2eAgInSc6d1	vojenský
historickém	historický	k2eAgInSc6d1	historický
archivu	archiv	k1gInSc6	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Matriky	matrika	k1gFnPc1	matrika
mrtvé	mrtvý	k2eAgFnPc1d1	mrtvá
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgFnPc1d1	uložená
v	v	k7c6	v
archivech	archiv	k1gInPc6	archiv
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
těch	ten	k3xDgFnPc2	ten
živých	živá	k1gFnPc2	živá
přístupné	přístupný	k2eAgFnSc3d1	přístupná
široké	široký	k2eAgFnSc3d1	široká
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
živým	živý	k2eAgInPc3d1	živý
matričním	matriční	k2eAgInPc3d1	matriční
záznamům	záznam	k1gInPc3	záznam
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
pouze	pouze	k6eAd1	pouze
oprávněné	oprávněný	k2eAgFnPc1d1	oprávněná
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
