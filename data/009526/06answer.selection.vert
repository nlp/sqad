<s>
Ghent	Ghent	k1gInSc1	Ghent
Kangri	Kangr	k1gFnSc2	Kangr
(	(	kIx(	(
<g/>
také	také	k9	také
Mount	Mount	k1gMnSc1	Mount
Ghent	Ghent	k1gMnSc1	Ghent
nebo	nebo	k8xC	nebo
Ghaint	Ghaint	k1gMnSc1	Ghaint
I	I	kA	I
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Karákóram	Karákóram	k1gInSc1	Karákóram
vysoká	vysoká	k1gFnSc1	vysoká
7	[number]	k4	7
401	[number]	k4	401
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
ledovce	ledovec	k1gInSc2	ledovec
Siačen	Siačno	k1gNnPc2	Siačno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
