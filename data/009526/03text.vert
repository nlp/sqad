<p>
<s>
Ghent	Ghent	k1gInSc1	Ghent
Kangri	Kangr	k1gFnSc2	Kangr
(	(	kIx(	(
<g/>
také	také	k9	také
Mount	Mount	k1gMnSc1	Mount
Ghent	Ghent	k1gMnSc1	Ghent
nebo	nebo	k8xC	nebo
Ghaint	Ghaint	k1gMnSc1	Ghaint
I	I	kA	I
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Karákóram	Karákóram	k1gInSc1	Karákóram
vysoká	vysoká	k1gFnSc1	vysoká
7	[number]	k4	7
401	[number]	k4	401
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
ledovce	ledovec	k1gInSc2	ledovec
Siačen	Siačno	k1gNnPc2	Siačno
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
blízko	blízko	k7c2	blízko
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ghent	Ghento	k1gNnPc2	Ghento
Kangri	Kangri	k1gNnSc2	Kangri
II	II	kA	II
==	==	k?	==
</s>
</p>
<p>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
Ghent	Ghenta	k1gFnPc2	Ghenta
Kangri	Kangr	k1gFnSc2	Kangr
II	II	kA	II
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
1,3	[number]	k4	1,3
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Ghent	Ghenta	k1gFnPc2	Ghenta
Kangri	Kangr	k1gFnSc2	Kangr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vysoký	vysoký	k2eAgInSc4d1	vysoký
7	[number]	k4	7
343	[number]	k4	343
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
Ghent	Ghent	k1gInSc4	Ghent
Kangri	Kangri	k1gNnPc2	Kangri
poprvé	poprvé	k6eAd1	poprvé
vylezl	vylézt	k5eAaPmAgInS	vylézt
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Axt	Axt	k1gMnSc1	Axt
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
rakouské	rakouský	k2eAgFnSc2d1	rakouská
expedice	expedice	k1gFnSc2	expedice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byly	být	k5eAaImAgInP	být
zaznamenány	zaznamenat	k5eAaPmNgInP	zaznamenat
tři	tři	k4xCgInPc1	tři
další	další	k2eAgInPc1d1	další
výstupy	výstup	k1gInPc1	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
1980	[number]	k4	1980
a	a	k8xC	a
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ghent	Ghenta	k1gFnPc2	Ghenta
Kangri	Kangri	k1gNnPc2	Kangri
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
