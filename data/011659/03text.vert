<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
a	a	k8xC	a
pár	pár	k4xCyI	pár
facek	facka	k1gFnPc2	facka
je	být	k5eAaImIp3nS	být
pokračování	pokračování	k1gNnPc4	pokračování
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
české	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
komedie	komedie	k1gFnSc2	komedie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
Zdeňka	Zdeňka	k1gFnSc1	Zdeňka
Trošky	troška	k1gFnSc2	troška
<g/>
.	.	kIx.	.
</s>
<s>
Komedie	komedie	k1gFnSc1	komedie
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
situována	situovat	k5eAaBmNgFnS	situovat
do	do	k7c2	do
malé	malý	k2eAgFnSc2d1	malá
vesnice	vesnice	k1gFnSc2	vesnice
Hoštice	Hoštice	k1gFnSc2	Hoštice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
i	i	k9	i
natáčena	natáčen	k2eAgFnSc1d1	natáčena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Blažena	Blažena	k1gFnSc1	Blažena
a	a	k8xC	a
Miluna	Miluna	k1gFnSc1	Miluna
se	se	k3xPyFc4	se
nemají	mít	k5eNaImIp3nP	mít
příliš	příliš	k6eAd1	příliš
v	v	k7c6	v
lásce	láska	k1gFnSc6	láska
<g/>
.	.	kIx.	.
</s>
<s>
Babička	babička	k1gFnSc1	babička
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
na	na	k7c6	na
kolečkách	koleček	k1gInPc6	koleček
neustále	neustále	k6eAd1	neustále
tropí	tropit	k5eAaImIp3nP	tropit
blbosti	blbost	k1gFnPc1	blbost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
nemusela	muset	k5eNaImAgFnS	muset
vzít	vzít	k5eAaPmF	vzít
medicínu	medicína	k1gFnSc4	medicína
od	od	k7c2	od
doktora	doktor	k1gMnSc2	doktor
Kroupy	Kroupa	k1gMnSc2	Kroupa
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Stárek	Stárek	k1gMnSc1	Stárek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Venca	Venca	k1gMnSc1	Venca
Konopník	Konopník	k1gMnSc1	Konopník
si	se	k3xPyFc3	se
chce	chtít	k5eAaImIp3nS	chtít
vzít	vzít	k5eAaPmF	vzít
těhotnou	těhotný	k2eAgFnSc4d1	těhotná
Blaženu	Blažena	k1gFnSc4	Blažena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Miluna	Miluna	k1gFnSc1	Miluna
to	ten	k3xDgNnSc4	ten
chce	chtít	k5eAaImIp3nS	chtít
překazit	překazit	k5eAaPmF	překazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vsi	ves	k1gFnSc6	ves
se	se	k3xPyFc4	se
rozšíří	rozšířit	k5eAaPmIp3nP	rozšířit
fámy	fáma	k1gFnPc1	fáma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
vydá	vydat	k5eAaPmIp3nS	vydat
do	do	k7c2	do
placu	plac	k1gInSc2	plac
Miluna	Miluna	k1gFnSc1	Miluna
<g/>
,	,	kIx,	,
rozpoutají	rozpoutat	k5eAaPmIp3nP	rozpoutat
mezi	mezi	k7c7	mezi
rodinami	rodina	k1gFnPc7	rodina
Konopníkových	Konopníkův	k2eAgFnPc2d1	Konopníkův
a	a	k8xC	a
Škopkových	Škopkových	k2eAgFnPc2d1	Škopkových
pořádnou	pořádný	k2eAgFnSc4d1	pořádná
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Fámy	fáma	k1gFnPc1	fáma
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Venca	Venca	k1gMnSc1	Venca
má	mít	k5eAaImIp3nS	mít
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
Ing.	ing.	kA	ing.
Tejfarovou	Tejfarová	k1gFnSc7	Tejfarová
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
navštíví	navštívit	k5eAaPmIp3nS	navštívit
živočicháře	živočichář	k1gMnSc4	živočichář
Béďu	Béďa	k1gFnSc4	Béďa
Gábina	Gábina	k1gFnSc1	Gábina
Tejfarová	Tejfarová	k1gFnSc1	Tejfarová
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
problém	problém	k1gInSc1	problém
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
živočicháře	živočichář	k1gMnSc2	živočichář
Bédi	Béďa	k1gMnSc2	Béďa
a	a	k8xC	a
účetní	účetní	k2eAgFnSc2d1	účetní
JZD	JZD	kA	JZD
Evičky	Evička	k1gFnSc2	Evička
nenechá	nechat	k5eNaPmIp3nS	nechat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
.	.	kIx.	.
</s>
<s>
Gábina	Gábina	k1gFnSc1	Gábina
narazí	narazit	k5eAaPmIp3nS	narazit
do	do	k7c2	do
faráře	farář	k1gMnSc2	farář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jel	jet	k5eAaImAgMnS	jet
na	na	k7c6	na
kole	kolo	k1gNnSc6	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Odveze	odvézt	k5eAaPmIp3nS	odvézt
ho	on	k3xPp3gNnSc4	on
do	do	k7c2	do
vsi	ves	k1gFnSc2	ves
a	a	k8xC	a
zajede	zajet	k5eAaPmIp3nS	zajet
za	za	k7c7	za
Milunou	Miluna	k1gFnSc7	Miluna
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
fámy	fáma	k1gFnPc1	fáma
se	se	k3xPyFc4	se
dostanou	dostat	k5eAaPmIp3nP	dostat
až	až	k6eAd1	až
k	k	k7c3	k
uším	ucho	k1gNnPc3	ucho
Škopkové	Škopkové	k2eAgNnPc3d1	Škopkové
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
to	ten	k3xDgNnSc1	ten
řekne	říct	k5eAaPmIp3nS	říct
Keliška	Keliška	k1gFnSc1	Keliška
z	z	k7c2	z
fary	fara	k1gFnSc2	fara
<g/>
.	.	kIx.	.
</s>
<s>
Škopková	Škopková	k1gFnSc1	Škopková
i	i	k8xC	i
Konopnice	Konopnice	k1gFnPc1	Konopnice
jdou	jít	k5eAaImIp3nP	jít
do	do	k7c2	do
místní	místní	k2eAgFnSc2d1	místní
hospody	hospody	k?	hospody
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Maruška	Maruška	k1gFnSc1	Maruška
Škopková	Škopková	k1gFnSc1	Škopková
začne	začít	k5eAaPmIp3nS	začít
Miluně	Miluna	k1gFnSc3	Miluna
nadávat	nadávat	k5eAaImF	nadávat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k8xC	tak
si	se	k3xPyFc3	se
Škopkovi	Škopkův	k2eAgMnPc1d1	Škopkův
a	a	k8xC	a
Konopníkovi	Konopníkův	k2eAgMnPc1d1	Konopníkův
vyřizují	vyřizovat	k5eAaImIp3nP	vyřizovat
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
tu	tu	k6eAd1	tu
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Maruš	Maruš	k?	Maruš
sežene	sehnat	k5eAaPmIp3nS	sehnat
napřed	napřed	k6eAd1	napřed
pro	pro	k7c4	pro
Blaženu	Blažena	k1gFnSc4	Blažena
tlustého	tlustý	k2eAgMnSc2d1	tlustý
Josefa	Josef	k1gMnSc2	Josef
a	a	k8xC	a
pak	pak	k6eAd1	pak
doktora	doktor	k1gMnSc4	doktor
Karla	Karel	k1gMnSc4	Karel
Kroupu	Kroupa	k1gMnSc4	Kroupa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Blaženě	Blažena	k1gFnSc6	Blažena
stojí	stát	k5eAaImIp3nS	stát
řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
Hubičková	hubičkový	k2eAgFnSc1d1	Hubičková
(	(	kIx(	(
<g/>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Jirásková	Jirásková	k1gFnSc1	Jirásková
<g/>
)	)	kIx)	)
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Blažena	Blažena	k1gFnSc1	Blažena
požádá	požádat	k5eAaPmIp3nS	požádat
tlustého	tlustý	k2eAgMnSc4d1	tlustý
Josefa	Josef	k1gMnSc4	Josef
o	o	k7c4	o
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
jede	jet	k5eAaImIp3nS	jet
do	do	k7c2	do
Bavorova	Bavorův	k2eAgInSc2d1	Bavorův
pro	pro	k7c4	pro
výzdobu	výzdoba	k1gFnSc4	výzdoba
a	a	k8xC	a
Blažena	Blažena	k1gFnSc1	Blažena
má	mít	k5eAaImIp3nS	mít
jet	jet	k5eAaImF	jet
též	též	k9	též
<g/>
.	.	kIx.	.
</s>
<s>
Jede	jet	k5eAaImIp3nS	jet
také	také	k9	také
Venca	Venca	k1gMnSc1	Venca
a	a	k8xC	a
řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bavorově	Bavorův	k2eAgFnSc6d1	Bavorova
se	se	k3xPyFc4	se
provdá	provdat	k5eAaPmIp3nS	provdat
za	za	k7c4	za
Vencu	Venca	k1gMnSc4	Venca
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
Blažena	Blažena	k1gFnSc1	Blažena
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
doktora	doktor	k1gMnSc4	doktor
nevezme	vzít	k5eNaPmIp3nS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Škopková	Škopková	k1gFnSc1	Škopková
začne	začít	k5eAaPmIp3nS	začít
řádit	řádit	k5eAaImF	řádit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c6	na
svatbě	svatba	k1gFnSc6	svatba
Blaženy	Blažena	k1gFnSc2	Blažena
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
strhne	strhnout	k5eAaPmIp3nS	strhnout
pořádná	pořádný	k2eAgFnSc1d1	pořádná
bitka	bitka	k1gFnSc1	bitka
<g/>
,	,	kIx,	,
oddávající	oddávající	k2eAgNnSc1d1	oddávající
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
z	z	k7c2	z
oddacího	oddací	k2eAgInSc2d1	oddací
listu	list	k1gInSc2	list
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
a	a	k8xC	a
řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
to	ten	k3xDgNnSc4	ten
potvdila	potvdit	k5eAaPmAgFnS	potvdit
<g/>
.	.	kIx.	.
</s>
<s>
Řídící	řídící	k2eAgFnSc1d1	řídící
Václavka	václavka	k1gFnSc1	václavka
odchází	odcházet	k5eAaImIp3nS	odcházet
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
doktorem	doktor	k1gMnSc7	doktor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Blaženy	Blažena	k1gFnSc2	Blažena
košem	koš	k1gInSc7	koš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Co	co	k3yInSc1	co
pořád	pořád	k6eAd1	pořád
vidíš	vidět	k5eAaImIp2nS	vidět
na	na	k7c6	na
tom	ten	k3xDgInSc6	ten
chlastu	chlast	k1gInSc6	chlast
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Škopková	Škopková	k1gFnSc1	Škopková
<g/>
)	)	kIx)	)
-	-	kIx~	-
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
ti	ty	k3xPp2nSc3	ty
to	ten	k3xDgNnSc4	ten
řeknu	říct	k5eAaPmIp1nS	říct
a	a	k8xC	a
ty	ty	k3xPp2nSc1	ty
začneš	začít	k5eAaPmIp2nS	začít
chlastat	chlastat	k5eAaImF	chlastat
taky	taky	k6eAd1	taky
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Škopek	Škopek	k1gMnSc1	Škopek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Prosímvás	Prosímvás	k1gInSc4	Prosímvás
<g/>
,	,	kIx,	,
točí	točit	k5eAaImIp3nS	točit
to	ten	k3xDgNnSc1	ten
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
učešte	učesat	k5eAaPmRp2nP	učesat
<g/>
,	,	kIx,	,
máte	mít	k5eAaImIp2nP	mít
<g/>
-li	i	k?	-li
hřeben	hřeben	k1gInSc4	hřeben
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Škopková	Škopková	k1gFnSc1	Škopková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
se	se	k3xPyFc4	se
perou	prát	k5eAaImIp3nP	prát
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
se	se	k3xPyFc4	se
perou	prát	k5eAaImIp3nP	prát
<g/>
!	!	kIx.	!
</s>
<s>
Já	já	k3xPp1nSc1	já
to	ten	k3xDgNnSc4	ten
říkala	říkat	k5eAaImAgFnS	říkat
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
čumáky	čumák	k1gInPc1	čumák
rozbijou	rozbít	k5eAaPmIp3nP	rozbít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
babička	babička	k1gFnSc1	babička
na	na	k7c6	na
schodech	schod	k1gInPc6	schod
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Inženýrka	inženýrek	k1gMnSc2	inženýrek
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
<g/>
.	.	kIx.	.
</s>
<s>
Auto	auto	k1gNnSc1	auto
má	mít	k5eAaImIp3nS	mít
z	z	k7c2	z
Tuzexu	Tuzex	k1gInSc2	Tuzex
<g/>
,	,	kIx,	,
peněz	peníze	k1gInPc2	peníze
jako	jako	k8xC	jako
šlupek	šlupek	k?	šlupek
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Konopnice	Konopnice	k1gFnSc1	Konopnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
a	a	k8xC	a
když	když	k8xS	když
vy	vy	k3xPp2nPc1	vy
inženýrku	inženýrek	k1gMnSc5	inženýrek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
my	my	k3xPp1nPc1	my
<g/>
....	....	k?	....
To	ten	k3xDgNnSc1	ten
budete	být	k5eAaImBp2nP	být
koukat	koukat	k5eAaImF	koukat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Škopková	Škopková	k1gFnSc1	Škopková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ja	Ja	k?	Ja
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
můj	můj	k3xOp1gInSc1	můj
starej	starat	k5eAaImRp2nS	starat
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
hovno	hovno	k1gNnSc4	hovno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
můj	můj	k1gMnSc1	můj
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Škopková	Škopková	k1gFnSc1	Škopková
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
a	a	k8xC	a
pár	pár	k4xCyI	pár
facek	facka	k1gFnPc2	facka
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
seno	seno	k1gNnSc1	seno
a	a	k8xC	a
pár	pár	k4xCyI	pár
facek	facka	k1gFnPc2	facka
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
