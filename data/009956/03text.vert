<p>
<s>
LZ	LZ	kA	LZ
127	[number]	k4	127
Graf	graf	k1gInSc4	graf
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
byla	být	k5eAaImAgFnS	být
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
éry	éra	k1gFnSc2	éra
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
po	po	k7c6	po
slavném	slavný	k2eAgMnSc6d1	slavný
staviteli	stavitel	k1gMnSc6	stavitel
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
,	,	kIx,	,
hraběti	hrabě	k1gMnSc3	hrabě
Ferdinandu	Ferdinand	k1gMnSc3	Ferdinand
von	von	k1gInSc4	von
Zeppelinovi	Zeppelin	k1gMnSc6	Zeppelin
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgNnSc1d1	podobné
jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
letadlová	letadlový	k2eAgFnSc1d1	letadlová
loď	loď	k1gFnSc1	loď
Graf	graf	k1gInSc4	graf
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
Graf	graf	k1gInSc4	graf
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
dílnách	dílna	k1gFnPc6	dílna
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Friedrichshafenu	Friedrichshafen	k1gInSc6	Friedrichshafen
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Ludwiga	Ludwig	k1gMnSc2	Ludwig
Dürra	Dürr	k1gMnSc2	Dürr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
konstrukcích	konstrukce	k1gFnPc6	konstrukce
všech	všecek	k3xTgMnPc2	všecek
předchozích	předchozí	k2eAgMnPc2d1	předchozí
zeppelínů	zeppelín	k1gInPc2	zeppelín
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
hlavním	hlavní	k2eAgMnSc7d1	hlavní
prosazovatelem	prosazovatel	k1gMnSc7	prosazovatel
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
kapitánem	kapitán	k1gMnSc7	kapitán
byl	být	k5eAaImAgMnS	být
Hugo	Hugo	k1gMnSc1	Hugo
Eckener	Eckener	k1gMnSc1	Eckener
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Objem	objem	k1gInSc1	objem
vodíkové	vodíkový	k2eAgFnSc2d1	vodíková
náplně	náplň	k1gFnSc2	náplň
činil	činit	k5eAaImAgInS	činit
85	[number]	k4	85
036	[number]	k4	036
m3	m3	k4	m3
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
236	[number]	k4	236
m	m	kA	m
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgFnSc1d1	cestovní
rychlost	rychlost	k1gFnSc1	rychlost
115	[number]	k4	115
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
130	[number]	k4	130
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
dolet	dolet	k1gInSc1	dolet
12	[number]	k4	12
000	[number]	k4	000
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
byla	být	k5eAaImAgFnS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
pěti	pět	k4xCc7	pět
motory	motor	k1gInPc7	motor
Maybach	Maybach	k1gMnSc1	Maybach
VL	VL	kA	VL
2	[number]	k4	2
po	po	k7c4	po
390	[number]	k4	390
kW	kW	kA	kW
<g/>
,	,	kIx,	,
poháněnými	poháněný	k2eAgFnPc7d1	poháněná
speciálně	speciálně	k6eAd1	speciálně
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
Blauovým	Blauův	k2eAgInSc7d1	Blauův
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
nazvaným	nazvaný	k2eAgInSc7d1	nazvaný
podle	podle	k7c2	podle
vynálezce	vynálezce	k1gMnSc4	vynálezce
Hermanna	Hermann	k1gMnSc2	Hermann
Blaua	Blauus	k1gMnSc2	Blauus
z	z	k7c2	z
Augsburgu	Augsburg	k1gInSc2	Augsburg
(	(	kIx(	(
<g/>
směs	směs	k1gFnSc1	směs
propylenu	propylen	k1gInSc2	propylen
<g/>
,	,	kIx,	,
methanu	methan	k1gInSc2	methan
<g/>
,	,	kIx,	,
ethanu	ethan	k1gInSc2	ethan
<g/>
,	,	kIx,	,
ethynu	ethyn	k1gInSc2	ethyn
<g/>
,	,	kIx,	,
butylenu	butylen	k1gInSc2	butylen
a	a	k8xC	a
vodíku	vodík	k1gInSc2	vodík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
hustotu	hustota	k1gFnSc4	hustota
jako	jako	k8xC	jako
vzduch	vzduch	k1gInSc4	vzduch
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
spotřeba	spotřeba	k1gFnSc1	spotřeba
se	se	k3xPyFc4	se
tedy	tedy	k8xC	tedy
neprojevovala	projevovat	k5eNaImAgFnS	projevovat
změnou	změna	k1gFnSc7	změna
vztlaku	vztlak	k1gInSc2	vztlak
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zásoby	zásoba	k1gFnPc1	zásoba
<g/>
,	,	kIx,	,
uložené	uložený	k2eAgInPc1d1	uložený
ve	v	k7c6	v
speciálním	speciální	k2eAgInSc6d1	speciální
balonetu	balonet	k1gInSc6	balonet
<g/>
,	,	kIx,	,
činily	činit	k5eAaImAgFnP	činit
30	[number]	k4	30
000	[number]	k4	000
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
<s>
Vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
měla	mít	k5eAaImAgFnS	mít
duralovou	duralový	k2eAgFnSc4d1	duralová
tuhou	tuhý	k2eAgFnSc4d1	tuhá
kostru	kostra	k1gFnSc4	kostra
s	s	k7c7	s
centrálním	centrální	k2eAgInSc7d1	centrální
kýlem	kýl	k1gInSc7	kýl
<g/>
,	,	kIx,	,
cestující	cestující	k1gMnSc1	cestující
a	a	k8xC	a
posádka	posádka	k1gFnSc1	posádka
dleli	dlít	k5eAaImAgMnP	dlít
v	v	k7c6	v
gondole	gondola	k1gFnSc6	gondola
umístěné	umístěný	k2eAgInPc1d1	umístěný
pod	pod	k7c7	pod
přední	přední	k2eAgFnSc7d1	přední
částí	část	k1gFnSc7	část
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
provedla	provést	k5eAaPmAgFnS	provést
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
propagační	propagační	k2eAgFnSc4d1	propagační
dvoutýdenní	dvoutýdenní	k2eAgFnSc4d1	dvoutýdenní
cestu	cesta	k1gFnSc4	cesta
kolem	kolem	k7c2	kolem
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
jako	jako	k8xS	jako
první	první	k4xOgInSc4	první
létající	létající	k2eAgInSc4d1	létající
dopravní	dopravní	k2eAgInSc4d1	dopravní
prostředek	prostředek	k1gInSc4	prostředek
s	s	k7c7	s
platícími	platící	k2eAgMnPc7d1	platící
pasažéry	pasažér	k1gMnPc7	pasažér
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1930	[number]	k4	1930
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
první	první	k4xOgInSc1	první
zkušební	zkušební	k2eAgInSc1d1	zkušební
let	let	k1gInSc1	let
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
tam	tam	k6eAd1	tam
začala	začít	k5eAaPmAgFnS	začít
vzducholoď	vzducholoď	k1gFnSc4	vzducholoď
dopravovat	dopravovat	k5eAaImF	dopravovat
pasažéry	pasažér	k1gMnPc4	pasažér
na	na	k7c6	na
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
lince	linka	k1gFnSc6	linka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
Graf	graf	k1gInSc1	graf
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
polární	polární	k2eAgFnSc4d1	polární
výpravu	výprava	k1gFnSc4	výprava
mapující	mapující	k2eAgFnSc4d1	mapující
severní	severní	k2eAgNnSc1d1	severní
pobřeží	pobřeží	k1gNnSc1	pobřeží
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
letech	let	k1gInPc6	let
vzducholoď	vzducholoď	k1gFnSc1	vzducholoď
létala	létat	k5eAaImAgFnS	létat
na	na	k7c6	na
pravidelné	pravidelný	k2eAgFnSc6d1	pravidelná
lince	linka	k1gFnSc6	linka
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
provozu	provoz	k1gInSc2	provoz
urazil	urazit	k5eAaPmAgInS	urazit
Graf	graf	k1gInSc1	graf
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
za	za	k7c4	za
17	[number]	k4	17
177	[number]	k4	177
letových	letový	k2eAgFnPc2d1	letová
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
2	[number]	k4	2
roky	rok	k1gInPc4	rok
čistého	čistý	k2eAgInSc2d1	čistý
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
1	[number]	k4	1
700	[number]	k4	700
000	[number]	k4	000
km	km	kA	km
<g/>
,	,	kIx,	,
dopravil	dopravit	k5eAaPmAgMnS	dopravit
34	[number]	k4	34
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
a	a	k8xC	a
78	[number]	k4	78
tun	tuna	k1gFnPc2	tuna
pošty	pošta	k1gFnSc2	pošta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
po	po	k7c6	po
katastrofě	katastrofa	k1gFnSc6	katastrofa
vzducholodi	vzducholoď	k1gFnSc2	vzducholoď
Hindenburg	Hindenburg	k1gInSc1	Hindenburg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
byl	být	k5eAaImAgInS	být
Graf	graf	k1gInSc1	graf
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
vyřazen	vyřazen	k2eAgInSc1d1	vyřazen
z	z	k7c2	z
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
přemístěn	přemístit	k5eAaPmNgMnS	přemístit
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1940	[number]	k4	1940
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
ministra	ministr	k1gMnSc2	ministr
vzdušných	vzdušný	k2eAgFnPc2d1	vzdušná
sil	síla	k1gFnPc2	síla
Hermanna	Hermann	k1gMnSc2	Hermann
Göringa	Göring	k1gMnSc2	Göring
rozebrán	rozebrán	k2eAgInSc1d1	rozebrán
do	do	k7c2	do
šrotu	šrot	k1gInSc2	šrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zepelínů	zepelín	k1gInPc2	zepelín
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
LZ	LZ	kA	LZ
127	[number]	k4	127
Graf	graf	k1gInSc1	graf
Zeppelin	Zeppelin	k2eAgInSc1d1	Zeppelin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
