<s>
Zámek	zámek	k1gInSc1	zámek
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Château	château	k1gNnSc1	château
de	de	k?	de
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Versailles	Versailles	k1gFnSc2	Versailles
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vrcholu	vrchol	k1gInSc2	vrchol
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
