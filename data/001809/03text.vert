<s>
Zámek	zámek	k1gInSc1	zámek
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Château	château	k1gNnSc1	château
de	de	k?	de
Versailles	Versailles	k1gFnSc1	Versailles
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Versailles	Versailles	k1gFnSc2	Versailles
u	u	k7c2	u
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vrcholu	vrchol	k1gInSc2	vrchol
královské	královský	k2eAgFnSc2d1	královská
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
evropské	evropský	k2eAgInPc1d1	evropský
paláce	palác	k1gInPc1	palác
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgInP	snažit
být	být	k5eAaImF	být
jakousi	jakýsi	k3yIgFnSc7	jakýsi
kopií	kopie	k1gFnSc7	kopie
Versailles	Versailles	k1gFnSc1	Versailles
(	(	kIx(	(
<g/>
Petrodvorec	Petrodvorec	k1gMnSc1	Petrodvorec
<g/>
,	,	kIx,	,
Schönbrunn	Schönbrunn	k1gMnSc1	Schönbrunn
<g/>
,	,	kIx,	,
Eszterháza	Eszterháza	k1gFnSc1	Eszterháza
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
s	s	k7c7	s
parkem	park	k1gInSc7	park
zapsán	zapsat	k5eAaPmNgInS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc4	ocenění
za	za	k7c4	za
nejhezčí	hezký	k2eAgInSc4d3	nejhezčí
Zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1682	[number]	k4	1682
do	do	k7c2	do
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
přestávkou	přestávka	k1gFnSc7	přestávka
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
regenta	regens	k1gMnSc2	regens
Filipa	Filip	k1gMnSc2	Filip
Orleánskeho	Orleánske	k1gMnSc2	Orleánske
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc4	zámek
sídlem	sídlo	k1gNnSc7	sídlo
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
zámek	zámek	k1gInSc1	zámek
má	mít	k5eAaImIp3nS	mít
tyto	tento	k3xDgFnPc4	tento
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
vlevo	vlevo	k6eAd1	vlevo
Jižní	jižní	k2eAgNnSc4d1	jižní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Královské	královský	k2eAgNnSc1d1	královské
nádvoří	nádvoří	k1gNnSc1	nádvoří
<g/>
,	,	kIx,	,
vpravo	vpravo	k6eAd1	vpravo
Severní	severní	k2eAgNnSc4d1	severní
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
úplně	úplně	k6eAd1	úplně
vpravo	vpravo	k6eAd1	vpravo
Opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
za	za	k7c7	za
Královským	královský	k2eAgNnSc7d1	královské
nádvořím	nádvoří	k1gNnSc7	nádvoří
Mramorové	mramorový	k2eAgFnSc2d1	mramorová
nádvoří	nádvoří	k1gNnSc1	nádvoří
obklopené	obklopený	k2eAgNnSc1d1	obklopené
jádrem	jádro	k1gNnSc7	jádro
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
končí	končit	k5eAaImIp3nP	končit
Zrcadlovým	zrcadlový	k2eAgInSc7d1	zrcadlový
sálem	sál	k1gInSc7	sál
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
zámku	zámek	k1gInSc2	zámek
má	mít	k5eAaImIp3nS	mít
půdorys	půdorys	k1gInSc4	půdorys
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
písmene	písmeno	k1gNnSc2	písmeno
U.	U.	kA	U.
Jádrem	jádro	k1gNnSc7	jádro
zámku	zámek	k1gInSc2	zámek
byla	být	k5eAaImAgFnS	být
králova	králův	k2eAgFnSc1d1	králova
ložnice	ložnice	k1gFnSc1	ložnice
s	s	k7c7	s
okny	okno	k1gNnPc7	okno
vedoucími	vedoucí	k2eAgNnPc7d1	vedoucí
do	do	k7c2	do
čtvercového	čtvercový	k2eAgInSc2d1	čtvercový
dvora	dvůr	k1gInSc2	dvůr
(	(	kIx(	(
<g/>
Mramorové	mramorový	k2eAgNnSc1d1	mramorové
nádvoří	nádvoří	k1gNnSc1	nádvoří
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
scházela	scházet	k5eAaImAgFnS	scházet
francouzská	francouzský	k2eAgFnSc1d1	francouzská
šlechta	šlechta	k1gFnSc1	šlechta
ke	k	k7c3	k
královu	králův	k2eAgNnSc3d1	královo
rannímu	ranní	k2eAgNnSc3d1	ranní
"	"	kIx"	"
<g/>
lever	lever	k1gInSc4	lever
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
vstávání	vstávání	k1gNnSc1	vstávání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
královy	králův	k2eAgFnSc2d1	králova
ložnice	ložnice	k1gFnSc2	ložnice
se	se	k3xPyFc4	se
procházelo	procházet	k5eAaImAgNnS	procházet
přes	přes	k7c4	přes
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgFnPc4d1	zdobená
ložnice	ložnice	k1gFnPc4	ložnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejprostornější	prostorný	k2eAgFnSc7d3	nejprostornější
místností	místnost	k1gFnSc7	místnost
jádra	jádro	k1gNnSc2	jádro
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
vzadu	vzadu	k6eAd1	vzadu
<g/>
,	,	kIx,	,
tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
začíná	začínat	k5eAaImIp3nS	začínat
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
Zrcadlový	zrcadlový	k2eAgInSc1d1	zrcadlový
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
roku	rok	k1gInSc2	rok
1678	[number]	k4	1678
postavil	postavit	k5eAaPmAgInS	postavit
Mansart	Mansart	k1gInSc1	Mansart
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
79	[number]	k4	79
m	m	kA	m
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
překlenutý	překlenutý	k2eAgMnSc1d1	překlenutý
klenbou	klenba	k1gFnSc7	klenba
s	s	k7c7	s
bohatou	bohatý	k2eAgFnSc7d1	bohatá
malířskou	malířský	k2eAgFnSc7d1	malířská
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
.	.	kIx.	.
</s>
<s>
Okna	okno	k1gNnPc1	okno
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
zrcadlům	zrcadlo	k1gNnPc3	zrcadlo
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
protější	protější	k2eAgNnSc1d1	protější
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zažehli	zažehnout	k5eAaPmAgMnP	zažehnout
tisíce	tisíc	k4xCgInPc1	tisíc
světel	světlo	k1gNnPc2	světlo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
odrážela	odrážet	k5eAaImAgFnS	odrážet
v	v	k7c6	v
zrcadlech	zrcadlo	k1gNnPc6	zrcadlo
a	a	k8xC	a
na	na	k7c6	na
briliantech	briliant	k1gInPc6	briliant
kavalírů	kavalír	k1gMnPc2	kavalír
a	a	k8xC	a
dam	dáma	k1gFnPc2	dáma
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
zámku	zámek	k1gInSc2	zámek
obrácená	obrácený	k2eAgFnSc1d1	obrácená
do	do	k7c2	do
nádvoří	nádvoří	k1gNnSc2	nádvoří
má	mít	k5eAaImIp3nS	mít
typické	typický	k2eAgInPc4d1	typický
prvky	prvek	k1gInPc4	prvek
architektury	architektura	k1gFnSc2	architektura
počátku	počátek	k1gInSc2	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
:	:	kIx,	:
střídání	střídání	k1gNnSc1	střídání
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
otesaného	otesaný	k2eAgInSc2d1	otesaný
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
špičaté	špičatý	k2eAgFnSc2d1	špičatá
střechy	střecha	k1gFnSc2	střecha
<g/>
,	,	kIx,	,
nádvoří	nádvoří	k1gNnSc6	nádvoří
se	s	k7c7	s
sochami	socha	k1gFnPc7	socha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgNnP	konat
divadelní	divadelní	k2eAgNnPc1d1	divadelní
představení	představení	k1gNnPc1	představení
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
odlišně	odlišně	k6eAd1	odlišně
působí	působit	k5eAaImIp3nS	působit
fasáda	fasáda	k1gFnSc1	fasáda
obrácená	obrácený	k2eAgFnSc1d1	obrácená
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
poschodí	poschodí	k1gNnSc1	poschodí
člení	členit	k5eAaImIp3nS	členit
pilastry	pilastr	k1gInPc7	pilastr
a	a	k8xC	a
rytmicky	rytmicky	k6eAd1	rytmicky
vystupující	vystupující	k2eAgInPc1d1	vystupující
portiky	portikus	k1gInPc1	portikus
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
poschodí	poschodí	k1gNnSc1	poschodí
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
malými	malý	k2eAgNnPc7d1	malé
polokruhovitými	polokruhovitý	k2eAgNnPc7d1	polokruhovitý
okny	okno	k1gNnPc7	okno
<g/>
.	.	kIx.	.
poč	poč	k?	poč
17	[number]	k4	17
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
:	:	kIx,	:
Versailles	Versailles	k1gFnSc1	Versailles
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
malá	malý	k2eAgFnSc1d1	malá
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
obklopená	obklopený	k2eAgFnSc1d1	obklopená
močály	močál	k1gInPc7	močál
a	a	k8xC	a
lesy	les	k1gInPc7	les
plnými	plný	k2eAgInPc7d1	plný
zvěře	zvěř	k1gFnPc4	zvěř
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
lov	lov	k1gInSc4	lov
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1624	[number]	k4	1624
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
tady	tady	k6eAd1	tady
koupil	koupit	k5eAaPmAgMnS	koupit
pozemek	pozemek	k1gInSc4	pozemek
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
dal	dát	k5eAaPmAgMnS	dát
postavit	postavit	k5eAaPmF	postavit
venkovské	venkovský	k2eAgNnSc4d1	venkovské
sídlo	sídlo	k1gNnSc4	sídlo
(	(	kIx(	(
<g/>
lovecký	lovecký	k2eAgInSc1d1	lovecký
zámeček	zámeček	k1gInSc1	zámeček
<g/>
)	)	kIx)	)
1631	[number]	k4	1631
<g/>
:	:	kIx,	:
Gondi	Gond	k1gMnPc1	Gond
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
pařížský	pařížský	k2eAgMnSc1d1	pařížský
<g/>
,	,	kIx,	,
převedl	převést	k5eAaPmAgMnS	převést
na	na	k7c4	na
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
panství	panství	k1gNnSc1	panství
Versailles	Versailles	k1gFnPc2	Versailles
a	a	k8xC	a
Philibert	Philibert	k1gInSc1	Philibert
Leroy	Leroa	k1gFnSc2	Leroa
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1631	[number]	k4	1631
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
1634	[number]	k4	1634
královi	král	k1gMnSc3	král
přestavil	přestavit	k5eAaPmAgInS	přestavit
venkovské	venkovský	k2eAgNnSc4d1	venkovské
sídlo	sídlo	k1gNnSc4	sídlo
na	na	k7c4	na
lovecký	lovecký	k2eAgInSc4d1	lovecký
zámeček	zámeček	k1gInSc4	zámeček
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
jádro	jádro	k1gNnSc1	jádro
paláce	palác	k1gInSc2	palác
okolo	okolo	k7c2	okolo
Mramorového	mramorový	k2eAgNnSc2d1	mramorové
nádvoří	nádvoří	k1gNnSc2	nádvoří
<g/>
)	)	kIx)	)
1661	[number]	k4	1661
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Král	Král	k1gMnSc1	Král
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
králem	král	k1gMnSc7	král
a	a	k8xC	a
ihned	ihned	k6eAd1	ihned
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
snaze	snaha	k1gFnSc3	snaha
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
bouřící	bouřící	k2eAgFnSc6d1	bouřící
se	se	k3xPyFc4	se
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
vzpomínkám	vzpomínka	k1gFnPc3	vzpomínka
na	na	k7c4	na
lovecké	lovecký	k2eAgInPc4d1	lovecký
výlety	výlet	k1gInPc4	výlet
<g />
.	.	kIx.	.
</s>
<s>
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
dal	dát	k5eAaPmAgMnS	dát
upravit	upravit	k5eAaPmF	upravit
areál	areál	k1gInSc4	areál
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
mohly	moct	k5eAaImAgFnP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
zahrady	zahrada	k1gFnPc4	zahrada
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
slavnosti	slavnost	k1gFnPc4	slavnost
1668	[number]	k4	1668
-	-	kIx~	-
1710	[number]	k4	1710
<g/>
:	:	kIx,	:
zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
staveništi	staveniště	k1gNnPc7	staveniště
-	-	kIx~	-
různé	různý	k2eAgFnPc1d1	různá
přestavby	přestavba	k1gFnPc1	přestavba
a	a	k8xC	a
úpravy	úprava	k1gFnPc1	úprava
1662	[number]	k4	1662
-	-	kIx~	-
1670	[number]	k4	1670
<g/>
:	:	kIx,	:
Loius	Loius	k1gMnSc1	Loius
Le	Le	k1gMnSc1	Le
Vau	Vau	k1gMnSc1	Vau
mírně	mírně	k6eAd1	mírně
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
starý	starý	k2eAgInSc4d1	starý
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
fasády	fasáda	k1gFnPc1	fasáda
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
style	styl	k1gInSc5	styl
<g/>
;	;	kIx,	;
André	André	k1gMnSc5	André
Le	Le	k1gMnSc5	Le
Nôtre	Nôtr	k1gMnSc5	Nôtr
upravil	upravit	k5eAaPmAgMnS	upravit
zahrady	zahrada	k1gFnPc4	zahrada
(	(	kIx(	(
<g/>
i	i	k9	i
okolí	okolí	k1gNnSc2	okolí
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c4	na
soustavu	soustava	k1gFnSc4	soustava
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
fontán	fontána	k1gFnPc2	fontána
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
si	se	k3xPyFc3	se
pozval	pozvat	k5eAaPmAgMnS	pozvat
italské	italský	k2eAgMnPc4d1	italský
bratry	bratr	k1gMnPc4	bratr
Francine	Francin	k1gInSc5	Francin
1661	[number]	k4	1661
-	-	kIx~	-
1683	[number]	k4	1683
<g/>
:	:	kIx,	:
Charles	Charles	k1gMnSc1	Charles
Le	Le	k1gMnSc1	Le
Brun	Brun	k1gMnSc1	Brun
vedl	vést	k5eAaImAgMnS	vést
práce	práce	k1gFnPc4	práce
malířů	malíř	k1gMnPc2	malíř
<g/>
,	,	kIx,	,
sochařů	sochař	k1gMnPc2	sochař
<g/>
,	,	kIx,	,
dekoratérů	dekoratér	k1gMnPc2	dekoratér
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
<g/>
:	:	kIx,	:
konaly	konat	k5eAaImAgFnP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
první	první	k4xOgFnPc1	první
hýřivé	hýřivý	k2eAgFnPc1d1	hýřivá
slavnosti	slavnost	k1gFnPc1	slavnost
1668	[number]	k4	1668
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
koupil	koupit	k5eAaPmAgMnS	koupit
sousední	sousední	k2eAgFnSc4d1	sousední
vesnici	vesnice	k1gFnSc4	vesnice
Trianon	Trianon	k1gInSc1	Trianon
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
parku	park	k1gInSc2	park
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
přičlenil	přičlenit	k5eAaPmAgMnS	přičlenit
k	k	k7c3	k
panství	panství	k1gNnSc3	panství
Versailles	Versailles	k1gFnSc2	Versailles
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
zbourat	zbourat	k5eAaPmF	zbourat
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
tam	tam	k6eAd1	tam
dal	dát	k5eAaPmAgInS	dát
postavit	postavit	k5eAaPmF	postavit
zámek	zámek	k1gInSc1	zámek
Porcelánový	porcelánový	k2eAgInSc1d1	porcelánový
<g />
.	.	kIx.	.
</s>
<s>
Trianon	Trianon	k1gInSc1	Trianon
<g/>
;	;	kIx,	;
roku	rok	k1gInSc2	rok
1687	[number]	k4	1687
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
zámek	zámek	k1gInSc1	zámek
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dnešním	dnešní	k2eAgInSc7d1	dnešní
zámkem	zámek	k1gInSc7	zámek
Velký	velký	k2eAgInSc1d1	velký
Trianon	Trianon	k1gInSc1	Trianon
1678	[number]	k4	1678
-	-	kIx~	-
1708	[number]	k4	1708
<g/>
:	:	kIx,	:
Jules	Julesa	k1gFnPc2	Julesa
Hardouin-Mansart	Hardouin-Mansart	k1gInSc1	Hardouin-Mansart
podstatně	podstatně	k6eAd1	podstatně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
zámek	zámek	k1gInSc1	zámek
<g/>
:	:	kIx,	:
přidal	přidat	k5eAaPmAgInS	přidat
dnešní	dnešní	k2eAgInSc1d1	dnešní
dvě	dva	k4xCgNnPc4	dva
křídla	křídlo	k1gNnPc4	křídlo
paláce	palác	k1gInSc2	palác
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc7d1	jižní
<g/>
)	)	kIx)	)
1680	[number]	k4	1680
<g/>
:	:	kIx,	:
vznik	vznik	k1gInSc1	vznik
slavného	slavný	k2eAgInSc2d1	slavný
Zrcadlového	zrcadlový	k2eAgInSc2d1	zrcadlový
sálu	sál	k1gInSc2	sál
(	(	kIx(	(
<g/>
Galerie	galerie	k1gFnSc1	galerie
des	des	k1gNnSc2	des
Glaces	Glacesa	k1gFnPc2	Glacesa
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nahradil	nahradit	k5eAaPmAgInS	nahradit
terasu	terasa	k1gFnSc4	terasa
spojující	spojující	k2eAgFnSc2d1	spojující
ložnice	ložnice	k1gFnSc2	ložnice
krále	král	k1gMnSc2	král
a	a	k8xC	a
královny	královna	k1gFnPc1	královna
1682	[number]	k4	1682
<g/>
:	:	kIx,	:
zámek	zámek	k1gInSc1	zámek
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
sídlem	sídlo	k1gNnSc7	sídlo
královského	královský	k2eAgInSc2d1	královský
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
<g/>
;	;	kIx,	;
ještě	ještě	k9	ještě
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
tu	tu	k6eAd1	tu
však	však	k9	však
stále	stále	k6eAd1	stále
pracovalo	pracovat	k5eAaImAgNnS	pracovat
22	[number]	k4	22
000	[number]	k4	000
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
6	[number]	k4	6
000	[number]	k4	000
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
.	.	kIx.	.
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
stavebních	stavební	k2eAgFnPc2d1	stavební
pracích	prací	k2eAgFnPc2d1	prací
<g/>
;	;	kIx,	;
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
vykonal	vykonat	k5eAaPmAgMnS	vykonat
změny	změna	k1gFnPc4	změna
zejména	zejména	k9	zejména
na	na	k7c4	na
zařízení	zařízení	k1gNnSc4	zařízení
interiéru	interiér	k1gInSc2	interiér
1688	[number]	k4	1688
<g/>
:	:	kIx,	:
zámek	zámek	k1gInSc1	zámek
prakticky	prakticky	k6eAd1	prakticky
hotový	hotový	k2eAgInSc1d1	hotový
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
údržba	údržba	k1gFnSc1	údržba
stála	stát	k5eAaImAgFnS	stát
asi	asi	k9	asi
25	[number]	k4	25
%	%	kIx~	%
vládních	vládní	k2eAgInPc2d1	vládní
příjmů	příjem	k1gInPc2	příjem
Francie	Francie	k1gFnSc2	Francie
1710	[number]	k4	1710
<g/>
:	:	kIx,	:
vysvěcení	vysvěcení	k1gNnSc6	vysvěcení
kapele	kapela	k1gFnSc6	kapela
1715	[number]	k4	1715
-	-	kIx~	-
1722	[number]	k4	1722
<g/>
:	:	kIx,	:
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
regenta	regens	k1gMnSc2	regens
Filipa	Filip	k1gMnSc2	Filip
Orleánskeho	Orleánske	k1gMnSc2	Orleánske
Ludvík	Ludvík	k1gMnSc1	Ludvík
XV	XV	kA	XV
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
dočasně	dočasně	k6eAd1	dočasně
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c4	v
Vincennes	Vincennes	k1gInSc4	Vincennes
1736	[number]	k4	1736
<g/>
:	:	kIx,	:
otevření	otevření	k1gNnSc6	otevření
Herkulova	Herkulův	k2eAgInSc2d1	Herkulův
sálu	sál	k1gInSc2	sál
1761	[number]	k4	1761
-	-	kIx~	-
1768	[number]	k4	1768
<g/>
:	:	kIx,	:
u	u	k7c2	u
Velkého	velký	k2eAgInSc2d1	velký
Trianonu	Trianon	k1gInSc2	Trianon
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
zámeček	zámeček	k1gInSc4	zámeček
Malý	malý	k2eAgInSc1d1	malý
Trianon	Trianon	k1gInSc1	Trianon
1770	[number]	k4	1770
-	-	kIx~	-
1772	[number]	k4	1772
<g/>
:	:	kIx,	:
A.	A.	kA	A.
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Gabriel	Gabriel	k1gMnSc1	Gabriel
udělal	udělat	k5eAaPmAgMnS	udělat
menší	malý	k2eAgFnPc4d2	menší
úpravy	úprava	k1gFnPc4	úprava
zejména	zejména	k9	zejména
hlavního	hlavní	k2eAgInSc2d1	hlavní
zámku	zámek	k1gInSc2	zámek
-	-	kIx~	-
zejména	zejména	k9	zejména
přístavba	přístavba	k1gFnSc1	přístavba
budovy	budova	k1gFnSc2	budova
Opery	opera	k1gFnSc2	opera
1783	[number]	k4	1783
<g/>
:	:	kIx,	:
podepsán	podepsán	k2eAgInSc1d1	podepsán
versailleský	versailleský	k2eAgInSc1d1	versailleský
mír	mír	k1gInSc1	mír
ukončující	ukončující	k2eAgFnSc4d1	ukončující
Americkou	americký	k2eAgFnSc4d1	americká
revoluci	revoluce	k1gFnSc4	revoluce
1783	[number]	k4	1783
-	-	kIx~	-
1786	[number]	k4	1786
<g/>
:	:	kIx,	:
stavba	stavba	k1gFnSc1	stavba
tzv.	tzv.	kA	tzv.
Královniny	královnin	k2eAgFnSc2d1	Královnina
vesnice	vesnice	k1gFnSc2	vesnice
-	-	kIx~	-
vesnice	vesnice	k1gFnSc2	vesnice
o	o	k7c6	o
12	[number]	k4	12
domech	dům	k1gInPc6	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Marie	Marie	k1gFnSc1	Marie
Antoinetta	Antoinetta	k1gFnSc1	Antoinetta
realizovala	realizovat	k5eAaBmAgFnS	realizovat
své	svůj	k3xOyFgFnPc4	svůj
představy	představa	k1gFnPc4	představa
ideálního	ideální	k2eAgInSc2d1	ideální
selského	selský	k2eAgInSc2d1	selský
života	život	k1gInSc2	život
1789	[number]	k4	1789
(	(	kIx(	(
<g/>
poč	poč	k?	poč
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
zasedají	zasedat	k5eAaImIp3nP	zasedat
generální	generální	k2eAgInPc1d1	generální
stavy	stav	k1gInPc1	stav
<g/>
;	;	kIx,	;
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
povstalci	povstalec	k1gMnPc1	povstalec
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
donutili	donutit	k5eAaPmAgMnP	donutit
královskou	královský	k2eAgFnSc4d1	královská
rodinu	rodina	k1gFnSc4	rodina
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
;	;	kIx,	;
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
nebydleli	bydlet	k5eNaImAgMnP	bydlet
králové	král	k1gMnPc1	král
<g/>
;	;	kIx,	;
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
revoluce	revoluce	k1gFnSc2	revoluce
bylo	být	k5eAaImAgNnS	být
rozkradeno	rozkraden	k2eAgNnSc1d1	rozkradeno
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zahrada	zahrada	k1gFnSc1	zahrada
byla	být	k5eAaImAgFnS	být
zanedbána	zanedbat	k5eAaPmNgFnS	zanedbat
<g/>
,	,	kIx,	,
budovy	budova	k1gFnPc1	budova
sloužily	sloužit	k5eAaImAgFnP	sloužit
různým	různý	k2eAgInSc7d1	různý
účelům	účel	k1gInPc3	účel
1806	[number]	k4	1806
-	-	kIx~	-
1810	[number]	k4	1810
<g/>
:	:	kIx,	:
Napoleon	Napoleon	k1gMnSc1	Napoleon
dal	dát	k5eAaPmAgMnS	dát
zámek	zámek	k1gInSc4	zámek
zrestaurovat	zrestaurovat	k5eAaPmF	zrestaurovat
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
zařídil	zařídit	k5eAaPmAgMnS	zařídit
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
úplně	úplně	k6eAd1	úplně
přestavět	přestavět	k5eAaPmF	přestavět
Velký	velký	k2eAgInSc1d1	velký
Trianon	Trianon	k1gInSc1	Trianon
a	a	k8xC	a
Malý	malý	k2eAgInSc1d1	malý
Trianon	Trianon	k1gInSc1	Trianon
1814	[number]	k4	1814
-	-	kIx~	-
1824	[number]	k4	1824
<g/>
:	:	kIx,	:
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
restaurování	restaurování	k1gNnSc6	restaurování
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ložnic	ložnice	k1gFnPc2	ložnice
krále	král	k1gMnSc2	král
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Karel	Karel	k1gMnSc1	Karel
X.	X.	kA	X.
restaurování	restaurování	k1gNnSc2	restaurování
ukončil	ukončit	k5eAaPmAgInS	ukončit
1830	[number]	k4	1830
<g/>
:	:	kIx,	:
po	po	k7c6	po
červnové	červnový	k2eAgFnSc6d1	červnová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svrhla	svrhnout	k5eAaPmAgFnS	svrhnout
Karla	Karel	k1gMnSc4	Karel
X.	X.	kA	X.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
znovu	znovu	k6eAd1	znovu
zanedbáván	zanedbávat	k5eAaImNgInS	zanedbávat
1833	[number]	k4	1833
-	-	kIx~	-
1837	[number]	k4	1837
<g/>
:	:	kIx,	:
král-občan	králbčan	k1gMnSc1	král-občan
Ludvík	Ludvík	k1gMnSc1	Ludvík
Filip	Filip	k1gMnSc1	Filip
Orleánský	orleánský	k2eAgMnSc1d1	orleánský
zachránil	zachránit	k5eAaPmAgInS	zachránit
zámek	zámek	k1gInSc1	zámek
před	před	k7c7	před
zbouráním	zbourání	k1gNnSc7	zbourání
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
ho	on	k3xPp3gMnSc4	on
přestavět	přestavět	k5eAaPmF	přestavět
a	a	k8xC	a
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
prostředků	prostředek	k1gInPc2	prostředek
tam	tam	k6eAd1	tam
roku	rok	k1gInSc3	rok
<g />
.	.	kIx.	.
</s>
<s>
1838	[number]	k4	1838
otevřel	otevřít	k5eAaPmAgMnS	otevřít
muzeum	muzeum	k1gNnSc4	muzeum
věnované	věnovaný	k2eAgFnSc2d1	věnovaná
"	"	kIx"	"
<g/>
všem	všecek	k3xTgNnPc3	všecek
vítězstvím	vítězství	k1gNnSc7	vítězství
Francie	Francie	k1gFnSc2	Francie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1870	[number]	k4	1870
-	-	kIx~	-
1871	[number]	k4	1871
<g/>
:	:	kIx,	:
zámek	zámek	k1gInSc1	zámek
Versailles	Versailles	k1gFnSc2	Versailles
je	být	k5eAaImIp3nS	být
dočasně	dočasně	k6eAd1	dočasně
hlavní	hlavní	k2eAgNnSc1d1	hlavní
sídlo	sídlo	k1gNnSc1	sídlo
německé	německý	k2eAgFnSc2d1	německá
armády	armáda	k1gFnSc2	armáda
během	během	k7c2	během
prusko-francouzské	pruskorancouzský	k2eAgFnSc2d1	prusko-francouzská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1871	[number]	k4	1871
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
zrcadlovém	zrcadlový	k2eAgNnSc6d1	zrcadlové
sálu	sál	k1gInSc3	sál
<g />
.	.	kIx.	.
</s>
<s>
vyhlášeno	vyhlášen	k2eAgNnSc1d1	vyhlášeno
Německé	německý	k2eAgNnSc1d1	německé
císařství	císařství	k1gNnSc1	císařství
1920	[number]	k4	1920
<g/>
:	:	kIx,	:
Trianonská	trianonský	k2eAgFnSc1d1	Trianonská
smlouva	smlouva	k1gFnSc1	smlouva
podepsána	podepsat	k5eAaPmNgFnS	podepsat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Velký	velký	k2eAgInSc4d1	velký
Trianon	Trianon	k1gInSc4	Trianon
Univerzita	univerzita	k1gFnSc1	univerzita
Versailles	Versailles	k1gFnPc2	Versailles
Saint	Saint	k1gMnSc1	Saint
Quentin	Quentin	k1gMnSc1	Quentin
en	en	k?	en
Yvelines	Yvelines	k1gMnSc1	Yvelines
Machine	Machin	k1gInSc5	Machin
de	de	k?	de
Marly	Marl	k1gInPc1	Marl
Seznam	seznam	k1gInSc1	seznam
oficiálních	oficiální	k2eAgFnPc2d1	oficiální
návštěv	návštěva	k1gFnPc2	návštěva
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Versailles	Versailles	k1gFnSc2	Versailles
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Versailles	Versailles	k1gFnSc2	Versailles
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Palace	Palace	k1gFnSc1	Palace
and	and	k?	and
Park	park	k1gInSc1	park
of	of	k?	of
Versailles	Versailles	k1gFnSc1	Versailles
(	(	kIx(	(
<g/>
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
