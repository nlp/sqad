<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Uranu	Uran	k1gInSc2	Uran
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgFnSc7d3	nejchladnější
atmosférou	atmosféra	k1gFnSc7	atmosféra
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
minimální	minimální	k2eAgFnPc1d1	minimální
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
okolo	okolo	k7c2	okolo
49	[number]	k4	49
K.	K.	kA	K.
Její	její	k3xOp3gInSc4	její
struktura	struktura	k1gFnSc1	struktura
je	být	k5eAaImIp3nS	být
vrstevnatá	vrstevnatý	k2eAgFnSc1d1	vrstevnatá
<g/>
:	:	kIx,	:
v	v	k7c6	v
nejnižších	nízký	k2eAgNnPc6d3	nejnižší
patrech	patro	k1gNnPc6	patro
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mraky	mrak	k1gInPc1	mrak
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svrchních	svrchní	k2eAgNnPc6d1	svrchní
patrech	patro	k1gNnPc6	patro
mraky	mrak	k1gInPc1	mrak
tvořené	tvořený	k2eAgInPc1d1	tvořený
především	především	k6eAd1	především
metanem	metan	k1gInSc7	metan
<g/>
.	.	kIx.	.
</s>
