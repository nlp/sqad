<s>
Madonna	Madonna	k1gFnSc1	Madonna
Louise	Louis	k1gMnSc2	Louis
Veronica	Veronicus	k1gMnSc2	Veronicus
Ciccone	Ciccon	k1gInSc5	Ciccon
Ritchie	Ritchie	k1gFnSc1	Ritchie
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
pod	pod	k7c7	pod
mononymem	mononym	k1gInSc7	mononym
Madonna	Madonn	k1gInSc2	Madonn
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
Bay	Bay	k1gFnSc1	Bay
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
<g/>
,	,	kIx,	,
producentka	producentka	k1gFnSc1	producentka
<g/>
,	,	kIx,	,
módní	módní	k2eAgFnSc1d1	módní
ikona	ikona	k1gFnSc1	ikona
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
