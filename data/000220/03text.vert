<s>
Madonna	Madonna	k1gFnSc1	Madonna
Louise	Louis	k1gMnSc2	Louis
Veronica	Veronicus	k1gMnSc2	Veronicus
Ciccone	Ciccon	k1gInSc5	Ciccon
Ritchie	Ritchie	k1gFnSc1	Ritchie
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
pod	pod	k7c7	pod
mononymem	mononym	k1gInSc7	mononym
Madonna	Madonn	k1gInSc2	Madonn
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
Bay	Bay	k1gFnSc1	Bay
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
<g/>
,	,	kIx,	,
producentka	producentka	k1gFnSc1	producentka
<g/>
,	,	kIx,	,
módní	módní	k2eAgFnSc1d1	módní
ikona	ikona	k1gFnSc1	ikona
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
sexuální	sexuální	k2eAgInSc4d1	sexuální
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
královnu	královna	k1gFnSc4	královna
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
bulváru	bulvár	k1gInSc2	bulvár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
jako	jako	k8xC	jako
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
umělkyní	umělkyně	k1gFnSc7	umělkyně
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
jako	jako	k8xS	jako
Madonna	Madonna	k1gFnSc1	Madonna
Louise	Louis	k1gMnSc2	Louis
Veronica	Veronicus	k1gMnSc2	Veronicus
Ciccone	Ciccon	k1gInSc5	Ciccon
v	v	k7c6	v
michiganském	michiganský	k2eAgMnSc6d1	michiganský
Bay	Bay	k1gMnSc6	Bay
City	City	k1gFnSc1	City
jako	jako	k8xS	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Silvia	Silvia	k1gFnSc1	Silvia
Anthonyho	Anthony	k1gMnSc2	Anthony
Cicconeho	Ciccone	k1gMnSc2	Ciccone
a	a	k8xC	a
Madonny	Madonen	k2eAgInPc4d1	Madonen
Louisy	louis	k1gInPc4	louis
Fortinové	Fortinový	k2eAgInPc4d1	Fortinový
<g/>
.	.	kIx.	.
</s>
<s>
Prarodiče	prarodič	k1gMnPc1	prarodič
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
imigrovali	imigrovat	k5eAaBmAgMnP	imigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
italského	italský	k2eAgNnSc2d1	italské
Pacentra	Pacentrum	k1gNnSc2	Pacentrum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
matka	matka	k1gFnSc1	matka
měla	mít	k5eAaImAgFnS	mít
francouzské	francouzský	k2eAgInPc4d1	francouzský
předky	předek	k1gInPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
inženýr	inženýr	k1gMnSc1	inženýr
pro	pro	k7c4	pro
společnosti	společnost	k1gFnPc4	společnost
Chrysler	Chrysler	k1gInSc4	Chrysler
a	a	k8xC	a
General	General	k1gFnSc4	General
Motors	Motorsa	k1gFnPc2	Motorsa
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k6eAd1	Madonna
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
Anthonyho	Anthony	k1gMnSc4	Anthony
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
mladší	mladý	k2eAgMnSc1d2	mladší
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
Paulu	Paula	k1gFnSc4	Paula
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Christophera	Christophera	k1gFnSc1	Christophera
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
Melanie	Melanie	k1gFnSc1	Melanie
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
biřmování	biřmování	k1gNnSc4	biřmování
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
přijala	přijmout	k5eAaPmAgFnS	přijmout
biřmovací	biřmovací	k2eAgNnSc4d1	biřmovací
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
svaté	svatý	k2eAgFnSc6d1	svatá
Veronice	Veronika	k1gFnSc6	Veronika
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
na	na	k7c6	na
detroitské	detroitský	k2eAgFnSc6d1	detroitská
periferii	periferie	k1gFnSc6	periferie
v	v	k7c6	v
Pontiacu	Pontiacum	k1gNnSc6	Pontiacum
a	a	k8xC	a
Avon	Avon	k1gInSc1	Avon
Townshipu	Township	k1gInSc2	Township
(	(	kIx(	(
<g/>
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c4	na
Rochester	Rochester	k1gInSc4	Rochester
Hills	Hills	k1gInSc4	Hills
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
karcinom	karcinom	k1gInSc4	karcinom
prsu	prs	k1gInSc2	prs
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
začala	začít	k5eAaPmAgFnS	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
slávu	sláva	k1gFnSc4	sláva
když	když	k8xS	když
utekla	utéct	k5eAaPmAgFnS	utéct
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
odletěla	odletět	k5eAaPmAgFnS	odletět
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jela	jet	k5eAaImAgFnS	jet
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
35	[number]	k4	35
dolary	dolar	k1gInPc7	dolar
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
servírka	servírka	k1gFnSc1	servírka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
i	i	k9	i
na	na	k7c6	na
konkurzu	konkurz	k1gInSc6	konkurz
k	k	k7c3	k
filmu	film	k1gInSc3	film
Vlasy	vlas	k1gInPc4	vlas
od	od	k7c2	od
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
nahrávky	nahrávka	k1gFnPc4	nahrávka
posílala	posílat	k5eAaImAgFnS	posílat
různým	různý	k2eAgMnPc3d1	různý
producentům	producent	k1gMnPc3	producent
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
herce	herec	k1gMnSc2	herec
Seana	Sean	k1gMnSc2	Sean
Penna	Penn	k1gMnSc2	Penn
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc4d3	nejstarší
dceru	dcera	k1gFnSc4	dcera
Lourdes	Lourdesa	k1gFnPc2	Lourdesa
Maria	Maria	k1gFnSc1	Maria
Ciccone	Ciccon	k1gInSc5	Ciccon
Leon	Leona	k1gFnPc2	Leona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
osobním	osobní	k2eAgMnSc7d1	osobní
trenérem	trenér	k1gMnSc7	trenér
Carlosem	Carlos	k1gMnSc7	Carlos
Leonem	Leo	k1gMnSc7	Leo
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevzala	vzít	k5eNaPmAgFnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Guyem	Guy	k1gMnSc7	Guy
Ritchiem	Ritchius	k1gMnSc7	Ritchius
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
Rocca	Roccus	k1gMnSc4	Roccus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
Guyem	Guy	k1gInSc7	Guy
se	se	k3xPyFc4	se
ale	ale	k9	ale
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
další	další	k2eAgFnPc4d1	další
2	[number]	k4	2
děti	dítě	k1gFnPc4	dítě
Davida	David	k1gMnSc2	David
Bandu	band	k1gInSc2	band
a	a	k8xC	a
Chifundo	Chifundo	k1gNnSc4	Chifundo
Mercy	Merca	k1gFnSc2	Merca
James	Jamesa	k1gFnPc2	Jamesa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
z	z	k7c2	z
afrického	africký	k2eAgInSc2d1	africký
státu	stát	k1gInSc2	stát
Malawi	Malawi	k1gNnSc2	Malawi
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
měla	mít	k5eAaImAgFnS	mít
rodinné	rodinný	k2eAgInPc4d1	rodinný
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Roccem	Rocce	k1gMnSc7	Rocce
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nekorunovanou	korunovaný	k2eNgFnSc4d1	nekorunovaná
královnu	královna	k1gFnSc4	královna
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Guinnesovy	Guinnesův	k2eAgFnSc2d1	Guinnesova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
je	být	k5eAaImIp3nS	být
také	také	k9	také
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
příjmem	příjem	k1gInSc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2005	[number]	k4	2005
vydala	vydat	k5eAaPmAgFnS	vydat
dvanáct	dvanáct	k4xCc4	dvanáct
platinových	platinový	k2eAgFnPc2d1	platinová
alb	alba	k1gFnPc2	alba
certifikovaných	certifikovaný	k2eAgFnPc2d1	certifikovaná
společností	společnost	k1gFnPc2	společnost
RIAA	RIAA	kA	RIAA
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
prodalo	prodat	k5eAaPmAgNnS	prodat
65	[number]	k4	65
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
jejích	její	k3xOp3gFnPc2	její
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
prodala	prodat	k5eAaPmAgFnS	prodat
celosvětově	celosvětově	k6eAd1	celosvětově
přes	přes	k7c4	přes
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
115	[number]	k4	115
milionů	milion	k4xCgInPc2	milion
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
prodávanou	prodávaný	k2eAgFnSc7d1	prodávaná
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
žila	žít	k5eAaImAgFnS	žít
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
Guyem	Guy	k1gMnSc7	Guy
Ritchiem	Ritchius	k1gMnSc7	Ritchius
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
jménem	jméno	k1gNnSc7	jméno
Rocco	Rocco	k1gMnSc1	Rocco
narozeným	narozený	k2eAgMnSc7d1	narozený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
média	médium	k1gNnSc2	médium
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
rozvodu	rozvod	k1gInSc6	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
v	v	k7c6	v
africké	africký	k2eAgFnSc6d1	africká
zemi	zem	k1gFnSc6	zem
Malawi	Malawi	k1gNnSc4	Malawi
třináctiměsíčního	třináctiměsíční	k2eAgMnSc2d1	třináctiměsíční
chlapečka	chlapeček	k1gMnSc2	chlapeček
Davida	David	k1gMnSc2	David
Bandu	band	k1gInSc2	band
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
Carlosem	Carlos	k1gMnSc7	Carlos
Leonem	Leo	k1gMnSc7	Leo
má	mít	k5eAaImIp3nS	mít
Madonna	Madonna	k1gFnSc1	Madonna
ještě	ještě	k9	ještě
dceru	dcera	k1gFnSc4	dcera
Lourdes	Lourdes	k1gInSc4	Lourdes
narozenou	narozený	k2eAgFnSc4d1	narozená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
turné	turné	k1gNnSc2	turné
Confessions	Confessionsa	k1gFnPc2	Confessionsa
Tour	Toura	k1gFnPc2	Toura
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
koncerty	koncert	k1gInPc1	koncert
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
Confessions	Confessionsa	k1gFnPc2	Confessionsa
Tour	Toura	k1gFnPc2	Toura
byly	být	k5eAaImAgInP	být
okamžitě	okamžitě	k6eAd1	okamžitě
beznadějně	beznadějně	k6eAd1	beznadějně
rozprodány	rozprodán	k2eAgInPc1d1	rozprodán
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pražský	pražský	k2eAgMnSc1d1	pražský
byl	být	k5eAaImAgMnS	být
vyprodán	vyprodán	k2eAgMnSc1d1	vyprodán
za	za	k7c4	za
necelou	celý	k2eNgFnSc4d1	necelá
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejrychleji	rychle	k6eAd3	rychle
vyprodaným	vyprodaný	k2eAgInSc7d1	vyprodaný
koncertem	koncert	k1gInSc7	koncert
O2	O2	k1gFnSc2	O2
arény	aréna	k1gFnSc2	aréna
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
naší	náš	k3xOp1gFnSc2	náš
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
30.1	[number]	k4	30.1
<g/>
.2009	.2009	k4	.2009
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
uveřejněno	uveřejnit	k5eAaPmNgNnS	uveřejnit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Madonna	Madonna	k1gFnSc1	Madonna
opět	opět	k6eAd1	opět
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prodlouženého	prodloužený	k2eAgInSc2d1	prodloužený
Sticky	Stick	k1gInPc1	Stick
and	and	k?	and
Sweet	Sweet	k1gInSc1	Sweet
Tour	Tour	k1gInSc1	Tour
13	[number]	k4	13
<g/>
.	.	kIx.	.
<g/>
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Chodově	Chodov	k1gInSc6	Chodov
<g/>
.	.	kIx.	.
7.11	[number]	k4	7.11
<g/>
.	.	kIx.	.
a	a	k8xC	a
8.11	[number]	k4	8.11
<g/>
.2015	.2015	k4	.2015
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Rebel	rebel	k1gMnSc1	rebel
Heart	Hearta	k1gFnPc2	Hearta
tour	tour	k1gMnSc1	tour
v	v	k7c6	v
Pražské	pražský	k2eAgFnSc6d1	Pražská
O2	O2	k1gFnSc6	O2
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
byly	být	k5eAaImAgInP	být
ihned	ihned	k6eAd1	ihned
vyprodány	vyprodán	k2eAgInPc1d1	vyprodán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
rekord	rekord	k1gInSc4	rekord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
nepřekonala	překonat	k5eNaPmAgFnS	překonat
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
v	v	k7c6	v
USA	USA	kA	USA
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
popové	popový	k2eAgFnSc2d1	popová
královny	královna	k1gFnSc2	královna
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Hard	Hard	k1gMnSc1	Hard
Candy	Canda	k1gFnPc4	Canda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
megahit	megahit	k1gInSc4	megahit
4	[number]	k4	4
Minutes	Minutesa	k1gFnPc2	Minutesa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
oficiální	oficiální	k2eAgFnSc6d1	oficiální
americké	americký	k2eAgFnSc6d1	americká
hitparádě	hitparáda	k1gFnSc6	hitparáda
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hitové	hitový	k2eAgInPc4d1	hitový
singly	singl	k1gInPc4	singl
4	[number]	k4	4
Minutes	Minutesa	k1gFnPc2	Minutesa
<g/>
,	,	kIx,	,
Give	Give	k1gFnSc1	Give
It	It	k1gFnSc1	It
2	[number]	k4	2
Me	Me	k1gFnPc2	Me
a	a	k8xC	a
Miles	Milesa	k1gFnPc2	Milesa
Away	Awaa	k1gFnSc2	Awaa
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
Hip	hip	k0	hip
Hop	hop	k0	hop
<g/>
,	,	kIx,	,
Dance	Danka	k1gFnSc3	Danka
<g/>
,	,	kIx,	,
Pop	pop	k1gInSc4	pop
<g/>
,	,	kIx,	,
Funky	funk	k1gInPc4	funk
a	a	k8xC	a
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
B.	B.	kA	B.
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
premiéru	premiéra	k1gFnSc4	premiéra
videoklip	videoklip	k1gInSc4	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
4	[number]	k4	4
Minutes	Minutesa	k1gFnPc2	Minutesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
i	i	k9	i
Justin	Justin	k1gMnSc1	Justin
Timberlake	Timberlak	k1gFnSc2	Timberlak
a	a	k8xC	a
Timbaland	Timbalanda	k1gFnPc2	Timbalanda
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
se	se	k3xPyFc4	se
natáčelo	natáčet	k5eAaImAgNnS	natáčet
30	[number]	k4	30
<g/>
.	.	kIx.	.
-	-	kIx~	-
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
Give	Give	k1gNnSc2	Give
It	It	k1gFnSc2	It
2	[number]	k4	2
Me	Me	k1gFnSc1	Me
měl	mít	k5eAaImAgInS	mít
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
videoklipem	videoklip	k1gInSc7	videoklip
premiéru	premiéra	k1gFnSc4	premiéra
koncem	koncem	k7c2	koncem
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2009	[number]	k4	2009
natáčela	natáčet	k5eAaImAgFnS	natáčet
2	[number]	k4	2
nové	nový	k2eAgInPc4d1	nový
songy	song	k1gInPc4	song
pro	pro	k7c4	pro
desku	deska	k1gFnSc4	deska
Celebration	Celebration	k1gInSc1	Celebration
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
DVD	DVD	kA	DVD
Celebration	Celebration	k1gInSc1	Celebration
se	s	k7c7	s
47	[number]	k4	47
videoklipy	videoklip	k1gInPc7	videoklip
<g/>
.	.	kIx.	.
</s>
<s>
Hard	Hard	k1gInSc1	Hard
Candy	Canda	k1gMnSc2	Canda
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2008	[number]	k4	2008
i	i	k8xC	i
v	v	k7c6	v
limitované	limitovaný	k2eAgFnSc6d1	limitovaná
edici	edice	k1gFnSc6	edice
(	(	kIx(	(
<g/>
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
obalem	obal	k1gInSc7	obal
<g/>
,	,	kIx,	,
do	do	k7c2	do
černa	černo	k1gNnSc2	černo
zbarveným	zbarvený	k2eAgFnPc3d1	zbarvená
<g/>
)	)	kIx)	)
s	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
remixy	remix	k1gInPc7	remix
4	[number]	k4	4
Minutes	Minutesa	k1gFnPc2	Minutesa
<g/>
,	,	kIx,	,
pestrým	pestrý	k2eAgInSc7d1	pestrý
bookletem	booklet	k1gInSc7	booklet
a	a	k8xC	a
přibalenými	přibalený	k2eAgInPc7d1	přibalený
bonusy	bonus	k1gInPc7	bonus
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
kolekci	kolekce	k1gFnSc6	kolekce
hitů	hit	k1gInPc2	hit
"	"	kIx"	"
<g/>
Celebration	Celebration	k1gInSc1	Celebration
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
končícího	končící	k2eAgInSc2d1	končící
kontraktu	kontrakt	k1gInSc2	kontrakt
s	s	k7c7	s
Warner	Warner	k1gInSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
blížící	blížící	k2eAgMnSc1d1	blížící
se	se	k3xPyFc4	se
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
popové	popový	k2eAgFnSc2d1	popová
královny	královna	k1gFnSc2	královna
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Celebration	Celebration	k1gInSc1	Celebration
<g/>
"	"	kIx"	"
skutečně	skutečně	k6eAd1	skutečně
posledním	poslední	k2eAgNnSc7d1	poslední
albem	album	k1gNnSc7	album
u	u	k7c2	u
hudebního	hudební	k2eAgNnSc2d1	hudební
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
před	před	k7c7	před
sedmadvaceti	sedmadvacet	k4xCc2	sedmadvacet
lety	let	k1gInPc7	let
začínala	začínat	k5eAaImAgFnS	začínat
<g/>
.	.	kIx.	.
</s>
<s>
Nekorunovaná	korunovaný	k2eNgFnSc1d1	nekorunovaná
královna	královna	k1gFnSc1	královna
popu	pop	k1gInSc2	pop
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
MDNA	MDNA	kA	MDNA
<g/>
.	.	kIx.	.
</s>
<s>
Dvanácté	dvanáctý	k4xOgNnSc4	dvanáctý
album	album	k1gNnSc4	album
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
natočila	natočit	k5eAaBmAgFnS	natočit
především	především	k6eAd1	především
s	s	k7c7	s
evropskými	evropský	k2eAgMnPc7d1	evropský
producenty	producent	k1gMnPc7	producent
elektronické	elektronický	k2eAgFnSc2d1	elektronická
taneční	taneční	k2eAgFnSc2d1	taneční
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgNnSc1d1	předchozí
album	album	k1gNnSc1	album
Hard	Harda	k1gFnPc2	Harda
Candy	Canda	k1gFnSc2	Canda
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Give	Give	k1gFnSc1	Give
Me	Me	k1gMnSc1	Me
All	All	k1gMnSc1	All
Your	Your	k1gMnSc1	Your
Luvin	Luvin	k1gMnSc1	Luvin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
písní	píseň	k1gFnPc2	píseň
se	se	k3xPyFc4	se
přestavily	přestavit	k5eAaPmAgFnP	přestavit
hosté	host	k1gMnPc1	host
<g/>
,	,	kIx,	,
zpěvačky	zpěvačka	k1gFnPc1	zpěvačka
Nicki	Nick	k1gFnSc2	Nick
Minaj	Minaj	k1gFnSc1	Minaj
a	a	k8xC	a
MIA	MIA	kA	MIA
<g/>
,	,	kIx,	,
zahrály	zahrát	k5eAaPmAgFnP	zahrát
si	se	k3xPyFc3	se
také	také	k9	také
ve	v	k7c6	v
videoklipu	videoklip	k1gInSc6	videoklip
jako	jako	k8xC	jako
roztleskávačky	roztleskávačka	k1gFnSc2	roztleskávačka
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
359	[number]	k4	359
tisícům	tisíc	k4xCgInPc3	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
stává	stávat	k5eAaImIp3nS	stávat
osmým	osmý	k4xOgInSc7	osmý
debutující	debutující	k2eAgFnSc4d1	debutující
albem	album	k1gNnSc7	album
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
Billboard	billboard	k1gInSc4	billboard
Top	topit	k5eAaImRp2nS	topit
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
překonala	překonat	k5eAaPmAgFnS	překonat
Elvise	Elvis	k1gMnSc4	Elvis
Presleyho	Presley	k1gMnSc4	Presley
<g/>
,	,	kIx,	,
Zpěvačce	zpěvačka	k1gFnSc6	zpěvačka
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
podařilo	podařit	k5eAaPmAgNnS	podařit
s	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
alby	album	k1gNnPc7	album
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
sólovým	sólový	k2eAgMnSc7d1	sólový
interpretem	interpret	k1gMnSc7	interpret
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
UK	UK	kA	UK
Album	album	k1gNnSc1	album
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
singly	singl	k1gInPc1	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Girl	girl	k1gFnSc1	girl
Gone	Gon	k1gFnSc2	Gon
Wild	Wilda	k1gFnPc2	Wilda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Masterpiece	Masterpiece	k1gFnSc1	Masterpiece
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Turn	Turn	k1gMnSc1	Turn
Up	Up	k1gMnSc1	Up
the	the	k?	the
Radio	radio	k1gNnSc4	radio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skladbu	skladba	k1gFnSc4	skladba
"	"	kIx"	"
<g/>
Masterpiece	Masterpiece	k1gFnSc1	Masterpiece
<g/>
"	"	kIx"	"
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
k	k	k7c3	k
filmu	film	k1gInSc3	film
W.E.	W.E.	k1gFnSc2	W.E.
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislostí	souvislost	k1gFnPc2	souvislost
s	s	k7c7	s
propagaci	propagace	k1gFnSc4	propagace
MDNA	MDNA	kA	MDNA
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
své	svůj	k3xOyFgNnSc4	svůj
deváté	devátý	k4xOgNnSc4	devátý
koncertní	koncertní	k2eAgNnSc4d1	koncertní
turné	turné	k1gNnSc4	turné
nazvané	nazvaný	k2eAgNnSc4d1	nazvané
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
album	album	k1gNnSc1	album
MDNA	MDNA	kA	MDNA
Tour	Tour	k1gMnSc1	Tour
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
vystoupení	vystoupení	k1gNnSc4	vystoupení
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
v	v	k7c6	v
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
<g/>
,	,	kIx,	,
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
.	.	kIx.	.
17.12	[number]	k4	17.12
<g/>
.2014	.2014	k4	.2014
unikly	uniknout	k5eAaPmAgFnP	uniknout
na	na	k7c4	na
internet	internet	k1gInSc4	internet
demo	demo	k2eAgFnSc2d1	demo
verze	verze	k1gFnSc2	verze
písniček	písnička	k1gFnPc2	písnička
z	z	k7c2	z
alba	album	k1gNnSc2	album
Rebel	rebel	k1gMnSc1	rebel
Heart	Heart	k1gInSc1	Heart
a	a	k8xC	a
Madonna	Madonna	k1gFnSc1	Madonna
krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
na	na	k7c4	na
Youtube	Youtub	k1gInSc5	Youtub
konečné	konečná	k1gFnSc6	konečná
verze	verze	k1gFnPc1	verze
písniček	písnička	k1gFnPc2	písnička
svým	svůj	k3xOyFgMnPc3	svůj
fanouškům	fanoušek	k1gMnPc3	fanoušek
jako	jako	k8xC	jako
vánoční	vánoční	k2eAgInSc4d1	vánoční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
s	s	k7c7	s
prvním	první	k4xOgInSc7	první
singlem	singl	k1gInSc7	singl
a	a	k8xC	a
videoklipem	videoklip	k1gInSc7	videoklip
Living	Living	k1gInSc1	Living
for	forum	k1gNnPc2	forum
love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
s	s	k7c7	s
videem	video	k1gNnSc7	video
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Ghosttown	Ghosttown	k1gMnSc1	Ghosttown
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
singlem	singl	k1gInSc7	singl
a	a	k8xC	a
videem	video	k1gNnSc7	video
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
písnička	písnička	k1gFnSc1	písnička
Bitch	Bitcha	k1gFnPc2	Bitcha
I	i	k9	i
<g/>
́	́	k?	́
<g/>
m	m	kA	m
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
.	.	kIx.	.
</s>
<s>
Video	video	k1gNnSc1	video
mělo	mít	k5eAaImAgNnS	mít
během	během	k7c2	během
10	[number]	k4	10
dnů	den	k1gInPc2	den
100	[number]	k4	100
000	[number]	k4	000
000	[number]	k4	000
vzhlédnutí	vzhlédnutí	k1gNnPc2	vzhlédnutí
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
Madonniným	Madonnin	k2eAgNnSc7d1	Madonnino
videem	video	k1gNnSc7	video
na	na	k7c6	na
youtube	youtub	k1gInSc5	youtub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
své	svůj	k3xOyFgNnSc4	svůj
turné	turné	k1gNnSc4	turné
Rebel	rebel	k1gMnSc1	rebel
Heart	Hearta	k1gFnPc2	Hearta
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgInP	následovat
tři	tři	k4xCgInPc1	tři
vyprodané	vyprodaný	k2eAgInPc1d1	vyprodaný
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Madonny	Madonna	k1gFnSc2	Madonna
<g/>
.	.	kIx.	.
1979	[number]	k4	1979
A	a	k9	a
Certain	Certain	k1gInSc1	Certain
Sacrifice	Sacrific	k1gMnSc2	Sacrific
[	[	kIx(	[
Bruna	Bruno	k1gMnSc2	Bruno
<g/>
,	,	kIx,	,
Cine	Cin	k1gMnSc2	Cin
Cine	Cin	k1gFnSc2	Cin
Productions	Productionsa	k1gFnPc2	Productionsa
]	]	kIx)	]
1985	[number]	k4	1985
Vision	vision	k1gInSc1	vision
Quest	Quest	k1gInSc4	Quest
[	[	kIx(	[
Singer	Singer	k1gMnSc1	Singer
(	(	kIx(	(
<g/>
cameo	cameo	k1gMnSc1	cameo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc1	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Pictures	Pictures	k1gInSc1	Pictures
]	]	kIx)	]
1985	[number]	k4	1985
Hledám	hledat	k5eAaImIp1nS	hledat
Susan	Susan	k1gMnSc1	Susan
<g/>
,	,	kIx,	,
zn.	zn.	kA	zn.
Zoufale	zoufale	k6eAd1	zoufale
(	(	kIx(	(
<g/>
Desperately	Desperatela	k1gFnSc2	Desperatela
Seeking	Seeking	k1gInSc1	Seeking
Susan	Susan	k1gMnSc1	Susan
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
Šanghajské	šanghajský	k2eAgNnSc1d1	Šanghajské
překvapení	překvapení	k1gNnSc1	překvapení
(	(	kIx(	(
<g/>
Shanghai	Shanghai	k1gNnSc1	Shanghai
Surprise	Surprise	k1gFnSc2	Surprise
<g/>
)	)	kIx)	)
1987	[number]	k4	1987
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
holka	holka	k1gFnSc1	holka
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Who	Who	k1gMnPc2	Who
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
That	That	k1gInSc1	That
Girl	girl	k1gFnSc1	girl
<g/>
)	)	kIx)	)
1989	[number]	k4	1989
Vražda	vražda	k1gFnSc1	vražda
na	na	k7c4	na
Broadwayi	Broadwayi	k1gNnSc4	Broadwayi
(	(	kIx(	(
<g/>
Bloodhounds	Bloodhounds	k1gInSc1	Bloodhounds
Of	Of	k1gMnSc2	Of
Broadway	Broadwaa	k1gMnSc2	Broadwaa
<g/>
)	)	kIx)	)
1990	[number]	k4	1990
Dick	Dicko	k1gNnPc2	Dicko
Tracy	Traca	k1gFnSc2	Traca
[	[	kIx(	[
Breathless	Breathless	k1gInSc1	Breathless
Mahoney	Mahonea	k1gFnSc2	Mahonea
<g/>
,	,	kIx,	,
Touchstone	Touchston	k1gInSc5	Touchston
Pictures	Pictures	k1gInSc1	Pictures
<g/>
/	/	kIx~	/
<g/>
Disney	Disney	k1gInPc1	Disney
<g/>
/	/	kIx~	/
<g/>
Buena	Buena	k1gFnSc1	Buena
Vista	vista	k2eAgFnSc1d1	vista
]	]	kIx)	]
1991	[number]	k4	1991
S	s	k7c7	s
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
(	(	kIx(	(
<g/>
Truth	Truth	k1gInSc1	Truth
<g />
.	.	kIx.	.
</s>
<s>
or	or	k?	or
Dare	dar	k1gInSc5	dar
/	/	kIx~	/
In	In	k1gFnSc6	In
Bed	Beda	k1gFnPc2	Beda
with	with	k1gInSc1	with
Madonna	Madonn	k1gInSc2	Madonn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alek	alka	k1gFnPc2	alka
Keshishian	Keshishian	k1gMnSc1	Keshishian
<g/>
,	,	kIx,	,
herecký	herecký	k2eAgMnSc1d1	herecký
partner	partner	k1gMnSc1	partner
Warren	Warrna	k1gFnPc2	Warrna
Beatty	Beatta	k1gFnSc2	Beatta
[	[	kIx(	[
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
,	,	kIx,	,
LIVE	LIVE	kA	LIVE
Entertainment	Entertainment	k1gInSc1	Entertainment
<g/>
/	/	kIx~	/
<g/>
Dino	Dino	k6eAd1	Dino
de	de	k?	de
Laurentiis	Laurentiis	k1gInSc1	Laurentiis
]	]	kIx)	]
1992	[number]	k4	1992
Stíny	stín	k1gInPc1	stín
a	a	k8xC	a
mlha	mlha	k1gFnSc1	mlha
(	(	kIx(	(
<g/>
Shadows	Shadows	k1gInSc1	Shadows
and	and	k?	and
Fog	Fog	k1gFnSc2	Fog
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Woody	Wooda	k1gFnSc2	Wooda
Allen	Allen	k1gMnSc1	Allen
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Orion	orion	k1gInSc1	orion
Pictures	Pictures	k1gInSc1	Pictures
]	]	kIx)	]
1992	[number]	k4	1992
Velké	velká	k1gFnSc2	velká
vítězství	vítězství	k1gNnSc2	vítězství
(	(	kIx(	(
<g/>
A	a	k9	a
League	League	k1gNnSc6	League
of	of	k?	of
Their	Their	k1gMnSc1	Their
Own	Own	k1gMnSc1	Own
<g/>
)	)	kIx)	)
režie	režie	k1gFnSc1	režie
Penny	penny	k1gInSc1	penny
Marshal	Marshal	k1gMnSc1	Marshal
<g/>
[	[	kIx(	[
Mae	Mae	k1gMnSc5	Mae
Mordabito	Mordabita	k1gMnSc5	Mordabita
<g/>
,	,	kIx,	,
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gMnSc1	Pictures
]	]	kIx)	]
1993	[number]	k4	1993
Tělo	tělo	k1gNnSc1	tělo
jako	jako	k8xC	jako
důkaz	důkaz	k1gInSc4	důkaz
(	(	kIx(	(
<g/>
Body	bod	k1gInPc4	bod
of	of	k?	of
Evidence	evidence	k1gFnSc2	evidence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Uli	Uli	k1gMnSc1	Uli
Edel	Edel	k1gMnSc1	Edel
[	[	kIx(	[
Rebecca	Rebecca	k1gMnSc1	Rebecca
Carlson	Carlson	k1gMnSc1	Carlson
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
MGM	MGM	kA	MGM
Pictures	Pictures	k1gInSc1	Pictures
<g/>
/	/	kIx~	/
<g/>
Dino	Dino	k6eAd1	Dino
de	de	k?	de
Laurentiis	Laurentiis	k1gInSc1	Laurentiis
]	]	kIx)	]
1993	[number]	k4	1993
Nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
Dangerous	Dangerous	k1gInSc1	Dangerous
Game	game	k1gInSc1	game
/	/	kIx~	/
Snake	Snake	k1gInSc1	Snake
Eyes	Eyes	k1gInSc1	Eyes
<g/>
)	)	kIx)	)
[	[	kIx(	[
Sarah	Sarah	k1gFnSc1	Sarah
Jennings	Jennings	k1gInSc1	Jennings
<g/>
,	,	kIx,	,
Cecchi	Cecchi	k1gNnSc1	Cecchi
Gori	Gori	k1gNnSc1	Gori
<g/>
/	/	kIx~	/
<g/>
Maverick	Maverick	k1gInSc1	Maverick
Films	Films	k1gInSc1	Films
]	]	kIx)	]
1995	[number]	k4	1995
Blue	Blu	k1gFnSc2	Blu
in	in	k?	in
the	the	k?	the
Face	Face	k1gInSc1	Face
/	/	kIx~	/
Brooklyn	Brooklyn	k1gInSc1	Brooklyn
Boogie	Boogie	k1gFnSc2	Boogie
[	[	kIx(	[
Singing	Singing	k1gInSc1	Singing
Telegram	telegram	k1gInSc1	telegram
(	(	kIx(	(
<g/>
cameo	cameo	k1gNnSc1	cameo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Miramax	Miramax	k1gInSc1	Miramax
Films	Films	k1gInSc1	Films
<g/>
/	/	kIx~	/
<g/>
Buena	Buena	k1gFnSc1	Buena
Vista	vista	k2eAgFnSc1d1	vista
]	]	kIx)	]
1995	[number]	k4	1995
Four	Four	k1gInSc1	Four
Rooms	Rooms	k1gInSc4	Rooms
[	[	kIx(	[
Elspeth	Elspeth	k1gInSc1	Elspeth
<g/>
,	,	kIx,	,
Miramax	Miramax	k1gInSc1	Miramax
Films	Films	k1gInSc1	Films
<g/>
/	/	kIx~	/
<g/>
Buena	Buena	k1gFnSc1	Buena
Vista	vista	k2eAgFnSc1d1	vista
]	]	kIx)	]
1996	[number]	k4	1996
Evita	Evito	k1gNnSc2	Evito
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alan	Alan	k1gMnSc1	Alan
Parker	Parker	k1gMnSc1	Parker
-	-	kIx~	-
životní	životní	k2eAgFnSc1d1	životní
role	role	k1gFnSc1	role
<g/>
,	,	kIx,	,
<g/>
cena	cena	k1gFnSc1	cena
Zlatý	zlatý	k1gInSc1	zlatý
Glóbus	glóbus	k1gInSc1	glóbus
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
komediální	komediální	k2eAgFnSc6d1	komediální
či	či	k8xC	či
muzikální	muzikální	k2eAgFnSc6d1	muzikální
roli	role	k1gFnSc6	role
[	[	kIx(	[
<g />
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
Perón	perón	k1gInSc1	perón
<g/>
,	,	kIx,	,
Hollywood	Hollywood	k1gInSc1	Hollywood
Pictures	Pictures	k1gInSc1	Pictures
<g/>
/	/	kIx~	/
<g/>
Cinergi	Cinergi	k1gNnSc1	Cinergi
Pictures	Picturesa	k1gFnPc2	Picturesa
]	]	kIx)	]
1996	[number]	k4	1996
Girl	girl	k1gFnSc1	girl
6	[number]	k4	6
[	[	kIx(	[
Boss	boss	k1gMnSc1	boss
#	#	kIx~	#
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
cameo	cameo	k6eAd1	cameo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Fox	fox	k1gInSc1	fox
]	]	kIx)	]
2000	[number]	k4	2000
Příští	příští	k2eAgFnSc1d1	příští
správná	správný	k2eAgFnSc1d1	správná
věc	věc	k1gFnSc1	věc
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Next	Next	k1gMnSc1	Next
Best	Best	k1gMnSc1	Best
Thing	Thing	k1gMnSc1	Thing
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Johan	Johan	k1gMnSc1	Johan
Schlesinger	Schlesinger	k1gMnSc1	Schlesinger
<g />
.	.	kIx.	.
</s>
<s>
[	[	kIx(	[
Abbie	Abbie	k1gFnSc1	Abbie
Reynolds	Reynolds	k1gInSc1	Reynolds
<g/>
,	,	kIx,	,
Lakeshore	Lakeshor	k1gInSc5	Lakeshor
Entertainment	Entertainment	k1gMnSc1	Entertainment
<g/>
/	/	kIx~	/
<g/>
Paramount	Paramount	k1gMnSc1	Paramount
Pictures	Pictures	k1gMnSc1	Pictures
]	]	kIx)	]
2002	[number]	k4	2002
Trosečníci	trosečník	k1gMnPc1	trosečník
<g/>
,	,	kIx,	,
<g/>
režie	režie	k1gFnPc1	režie
Guy	Guy	k1gFnSc2	Guy
Ritchie	Ritchie	k1gFnSc2	Ritchie
-herecký	erecký	k2eAgMnSc1d1	-herecký
partner	partner	k1gMnSc1	partner
Adriano	Adriana	k1gFnSc5	Adriana
Giannini	Giannin	k2eAgMnPc1d1	Giannin
2002	[number]	k4	2002
Swept	Sweptum	k1gNnPc2	Sweptum
Away	Awaa	k1gFnSc2	Awaa
[	[	kIx(	[
Amber	ambra	k1gFnPc2	ambra
Leighton	Leighton	k1gInSc1	Leighton
<g/>
,	,	kIx,	,
Screen	Screen	k2eAgInSc1d1	Screen
Gems	Gems	k1gInSc1	Gems
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
Pictures	Pictures	k1gMnSc1	Pictures
]	]	kIx)	]
2002	[number]	k4	2002
Die	Die	k1gFnSc2	Die
Another	Anothra	k1gFnPc2	Anothra
Day	Day	k1gFnSc2	Day
[	[	kIx(	[
Verity	Verita	k1gFnSc2	Verita
(	(	kIx(	(
<g/>
cameo	cameo	k1gMnSc1	cameo
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MGM	MGM	kA	MGM
Pictures	Pictures	k1gMnSc1	Pictures
<g/>
/	/	kIx~	/
<g/>
Danjaq	Danjaq	k1gMnSc1	Danjaq
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
]	]	kIx)	]
2006	[number]	k4	2006
Arthur	Arthur	k1gMnSc1	Arthur
and	and	k?	and
the	the	k?	the
Minimoys	Minimoys	k1gInSc1	Minimoys
(	(	kIx(	(
<g/>
English	English	k1gInSc1	English
Version	Version	k1gInSc1	Version
only	onla	k1gFnSc2	onla
<g/>
)	)	kIx)	)
aka	aka	k?	aka
Arthur	Arthur	k1gMnSc1	Arthur
and	and	k?	and
the	the	k?	the
Invisibles	Invisibles	k1gInSc1	Invisibles
[	[	kIx(	[
Princess	Princess	k1gInSc1	Princess
Selenia	Selenium	k1gNnSc2	Selenium
(	(	kIx(	(
<g/>
voice	voice	k1gFnSc1	voice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EuropaCorp	EuropaCorp	k1gMnSc1	EuropaCorp
<g/>
/	/	kIx~	/
<g/>
The	The	k1gMnSc1	The
Weinstein	Weinstein	k1gMnSc1	Weinstein
Company	Compana	k1gFnSc2	Compana
<g/>
/	/	kIx~	/
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Lionsgate	Lionsgat	k1gMnSc5	Lionsgat
]	]	kIx)	]
2008	[number]	k4	2008
I	i	k8xC	i
Am	Am	k1gMnSc1	Am
Because	Because	k1gFnSc2	Because
We	We	k1gMnSc1	We
Are	ar	k1gInSc5	ar
[	[	kIx(	[
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
,	,	kIx,	,
Semtex	semtex	k1gInSc1	semtex
Films	Films	k1gInSc1	Films
<g/>
/	/	kIx~	/
<g/>
Hollydog	Hollydog	k1gInSc1	Hollydog
Films	Films	k1gInSc1	Films
]	]	kIx)	]
2009	[number]	k4	2009
Arthur	Arthur	k1gMnSc1	Arthur
and	and	k?	and
the	the	k?	the
Minimoys	Minimoys	k1gInSc1	Minimoys
2	[number]	k4	2
<g/>
:	:	kIx,	:
Vengeance	Vengeanec	k1gInSc2	Vengeanec
of	of	k?	of
Maltazard	Maltazard	k1gMnSc1	Maltazard
(	(	kIx(	(
<g/>
English	English	k1gMnSc1	English
Version	Version	k1gInSc4	Version
only	onla	k1gFnSc2	onla
<g/>
)	)	kIx)	)
[	[	kIx(	[
Princess	Princess	k1gInSc1	Princess
Selenia	Selenium	k1gNnSc2	Selenium
(	(	kIx(	(
<g/>
voice	voice	k1gFnSc1	voice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
EuropaCorp	EuropaCorp	k1gMnSc1	EuropaCorp
<g />
.	.	kIx.	.
</s>
<s>
]	]	kIx)	]
2010	[number]	k4	2010
Arthur	Arthur	k1gMnSc1	Arthur
and	and	k?	and
the	the	k?	the
Minimoys	Minimoys	k1gInSc1	Minimoys
3	[number]	k4	3
<g/>
:	:	kIx,	:
War	War	k1gMnSc1	War
Of	Of	k1gFnSc2	Of
Two	Two	k1gMnSc1	Two
Worlds	Worlds	k1gInSc1	Worlds
(	(	kIx(	(
<g/>
English	English	k1gInSc1	English
Version	Version	k1gInSc1	Version
only	onla	k1gFnSc2	onla
<g/>
)	)	kIx)	)
Filmové	filmový	k2eAgInPc1d1	filmový
dokumenty	dokument	k1gInPc1	dokument
o	o	k7c6	o
Madonně	Madonně	k1gFnSc6	Madonně
<g/>
:	:	kIx,	:
1992	[number]	k4	1992
S	s	k7c7	s
Madonnou	Madonný	k2eAgFnSc7d1	Madonný
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
2005	[number]	k4	2005
Řeknu	říct	k5eAaPmIp1nS	říct
ti	ty	k3xPp2nSc3	ty
tajemství	tajemství	k1gNnSc4	tajemství
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Going	Going	k1gMnSc1	Going
to	ten	k3xDgNnSc4	ten
Tell	Tell	k1gMnSc1	Tell
You	You	k1gMnSc1	You
a	a	k8xC	a
Secret	Secret	k1gMnSc1	Secret
<g/>
)	)	kIx)	)
-	-	kIx~	-
volné	volný	k2eAgNnSc1d1	volné
pokračování	pokračování	k1gNnSc1	pokračování
[	[	kIx(	[
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
,	,	kIx,	,
Maverick	Maverick	k1gInSc1	Maverick
Films	Films	k1gInSc1	Films
<g/>
/	/	kIx~	/
<g/>
RiverRoad	RiverRoad	k1gInSc1	RiverRoad
<g/>
/	/	kIx~	/
<g/>
Lucky	Lucka	k1gFnPc1	Lucka
Lou	Lou	k1gFnPc1	Lou
]	]	kIx)	]
V	v	k7c4	v
produkci	produkce	k1gFnSc4	produkce
HBO	HBO	kA	HBO
byl	být	k5eAaImAgInS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
natočen	natočit	k5eAaBmNgMnS	natočit
o	o	k7c6	o
Madonně	Madonně	k1gMnSc6	Madonně
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
sledující	sledující	k2eAgInPc4d1	sledující
začátky	začátek	k1gInPc4	začátek
její	její	k3xOp3gFnSc2	její
hudební	hudební	k2eAgFnSc2d1	hudební
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
o	o	k7c6	o
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
Madonna	Madonna	k1gFnSc1	Madonna
hrála	hrát	k5eAaImAgFnS	hrát
<g/>
:	:	kIx,	:
Neoficiální	neoficiální	k2eAgFnSc1d1	neoficiální
fanouškovské	fanouškovský	k2eAgFnPc4d1	fanouškovská
stránky	stránka	k1gFnPc4	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
je	být	k5eAaImIp3nS	být
též	též	k9	též
autorkou	autorka	k1gFnSc7	autorka
několika	několik	k4yIc2	několik
knížek	knížka	k1gFnPc2	knížka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
prodej	prodej	k1gInSc1	prodej
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
označit	označit	k5eAaPmF	označit
jako	jako	k9	jako
lehce	lehko	k6eAd1	lehko
nadprůměrný	nadprůměrný	k2eAgMnSc1d1	nadprůměrný
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
studentkou	studentka	k1gFnSc7	studentka
kabaly	kabala	k1gFnSc2	kabala
(	(	kIx(	(
<g/>
židovského	židovský	k2eAgInSc2d1	židovský
mysticismu	mysticismus	k1gInSc2	mysticismus
<g/>
)	)	kIx)	)
a	a	k8xC	a
studuje	studovat	k5eAaImIp3nS	studovat
v	v	k7c6	v
Kabalistickém	kabalistický	k2eAgNnSc6d1	kabalistické
centru	centrum	k1gNnSc6	centrum
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
učení	učení	k1gNnSc3	učení
už	už	k6eAd1	už
přivedla	přivést	k5eAaPmAgFnS	přivést
řadu	řada	k1gFnSc4	řada
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Gwyneth	Gwyneth	k1gMnSc1	Gwyneth
Paltrow	Paltrow	k1gMnSc1	Paltrow
<g/>
,	,	kIx,	,
Demi	Dem	k1gMnSc5	Dem
Moore	Moor	k1gMnSc5	Moor
<g/>
,	,	kIx,	,
atp.	atp.	kA	atp.
Madonna	Madonna	k1gFnSc1	Madonna
je	být	k5eAaImIp3nS	být
držitelkou	držitelka	k1gFnSc7	držitelka
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
maliny	malina	k1gFnSc2	malina
za	za	k7c4	za
nejhorší	zlý	k2eAgFnSc4d3	nejhorší
herečku	herečka	k1gFnSc4	herečka
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Trosečníci	trosečník	k1gMnPc1	trosečník
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
režíroval	režírovat	k5eAaImAgMnS	režírovat
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
Guy	Guy	k1gMnSc1	Guy
Ritchie	Ritchie	k1gFnSc1	Ritchie
<g/>
.	.	kIx.	.
</s>
