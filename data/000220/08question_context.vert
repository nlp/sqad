<s>
Madonna	Madonna	k1gFnSc1	Madonna
Louise	Louis	k1gMnSc2	Louis
Veronica	Veronicus	k1gMnSc2	Veronicus
Ciccone	Ciccon	k1gInSc5	Ciccon
Ritchie	Ritchie	k1gFnSc1	Ritchie
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
hlavně	hlavně	k9	hlavně
pod	pod	k7c7	pod
mononymem	mononym	k1gInSc7	mononym
Madonna	Madonn	k1gInSc2	Madonn
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1958	[number]	k4	1958
Bay	Bay	k1gFnSc1	Bay
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Michigan	Michigan	k1gInSc1	Michigan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
americká	americký	k2eAgFnSc1d1	americká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
skladatelka	skladatelka	k1gFnSc1	skladatelka
<g/>
,	,	kIx,	,
tanečnice	tanečnice	k1gFnSc1	tanečnice
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
<g/>
,	,	kIx,	,
producentka	producentka	k1gFnSc1	producentka
<g/>
,	,	kIx,	,
módní	módní	k2eAgFnSc1d1	módní
ikona	ikona	k1gFnSc1	ikona
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
sexuální	sexuální	k2eAgInSc4d1	sexuální
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
královnu	královna	k1gFnSc4	královna
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
bulváru	bulvár	k1gInSc2	bulvár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
v	v	k7c6	v
Guinessově	Guinessův	k2eAgFnSc6d1	Guinessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
jako	jako	k8xC	jako
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
umělkyní	umělkyně	k1gFnSc7	umělkyně
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
jako	jako	k8xS	jako
Madonna	Madonna	k1gFnSc1	Madonna
Louise	Louis	k1gMnSc2	Louis
Veronica	Veronicus	k1gMnSc2	Veronicus
Ciccone	Ciccon	k1gInSc5	Ciccon
v	v	k7c6	v
michiganském	michiganský	k2eAgMnSc6d1	michiganský
Bay	Bay	k1gMnSc6	Bay
City	City	k1gFnSc1	City
jako	jako	k8xS	jako
nejstarší	starý	k2eAgFnSc1d3	nejstarší
dcera	dcera	k1gFnSc1	dcera
Silvia	Silvia	k1gFnSc1	Silvia
Anthonyho	Anthony	k1gMnSc2	Anthony
Cicconeho	Ciccone	k1gMnSc2	Ciccone
a	a	k8xC	a
Madonny	Madonen	k2eAgInPc4d1	Madonen
Louisy	louis	k1gInPc4	louis
Fortinové	Fortinový	k2eAgInPc4d1	Fortinový
<g/>
.	.	kIx.	.
</s>
<s>
Prarodiče	prarodič	k1gMnPc1	prarodič
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
imigrovali	imigrovat	k5eAaBmAgMnP	imigrovat
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
z	z	k7c2	z
italského	italský	k2eAgNnSc2d1	italské
Pacentra	Pacentrum	k1gNnSc2	Pacentrum
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
matka	matka	k1gFnSc1	matka
měla	mít	k5eAaImAgFnS	mít
francouzské	francouzský	k2eAgInPc4d1	francouzský
předky	předek	k1gInPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
inženýr	inženýr	k1gMnSc1	inženýr
pro	pro	k7c4	pro
společnosti	společnost	k1gFnPc4	společnost
Chrysler	Chrysler	k1gInSc4	Chrysler
a	a	k8xC	a
General	General	k1gFnSc4	General
Motors	Motorsa	k1gFnPc2	Motorsa
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k6eAd1	Madonna
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
Anthonyho	Anthony	k1gMnSc4	Anthony
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
a	a	k8xC	a
Martina	Martina	k1gFnSc1	Martina
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
tři	tři	k4xCgMnPc1	tři
mladší	mladý	k2eAgMnSc1d2	mladší
sourozence	sourozenec	k1gMnSc4	sourozenec
<g/>
,	,	kIx,	,
Paulu	Paula	k1gFnSc4	Paula
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Christophera	Christophera	k1gFnSc1	Christophera
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
Melanie	Melanie	k1gFnSc1	Melanie
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
biřmování	biřmování	k1gNnSc4	biřmování
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
přijala	přijmout	k5eAaPmAgFnS	přijmout
biřmovací	biřmovací	k2eAgNnSc4d1	biřmovací
jméno	jméno	k1gNnSc4	jméno
po	po	k7c6	po
svaté	svatý	k2eAgFnSc6d1	svatá
Veronice	Veronika	k1gFnSc6	Veronika
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
na	na	k7c6	na
detroitské	detroitský	k2eAgFnSc6d1	detroitská
periferii	periferie	k1gFnSc6	periferie
v	v	k7c6	v
Pontiacu	Pontiacum	k1gNnSc6	Pontiacum
a	a	k8xC	a
Avon	Avon	k1gInSc1	Avon
Townshipu	Township	k1gInSc2	Township
(	(	kIx(	(
<g/>
přejmenovaného	přejmenovaný	k2eAgInSc2d1	přejmenovaný
na	na	k7c4	na
Rochester	Rochester	k1gInSc4	Rochester
Hills	Hills	k1gInSc4	Hills
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
karcinom	karcinom	k1gInSc4	karcinom
prsu	prs	k1gInSc2	prs
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
začala	začít	k5eAaPmAgFnS	začít
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
slávu	sláva	k1gFnSc4	sláva
když	když	k8xS	když
utekla	utéct	k5eAaPmAgFnS	utéct
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
odletěla	odletět	k5eAaPmAgFnS	odletět
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jela	jet	k5eAaImAgFnS	jet
s	s	k7c7	s
pouhými	pouhý	k2eAgInPc7d1	pouhý
35	[number]	k4	35
dolary	dolar	k1gInPc7	dolar
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
servírka	servírka	k1gFnSc1	servírka
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
i	i	k9	i
na	na	k7c6	na
konkurzu	konkurz	k1gInSc6	konkurz
k	k	k7c3	k
filmu	film	k1gInSc3	film
Vlasy	vlas	k1gInPc4	vlas
od	od	k7c2	od
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
nahrávky	nahrávka	k1gFnPc4	nahrávka
posílala	posílat	k5eAaImAgFnS	posílat
různým	různý	k2eAgMnPc3d1	různý
producentům	producent	k1gMnPc3	producent
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
herce	herec	k1gMnSc2	herec
Seana	Sean	k1gMnSc2	Sean
Penna	Penn	k1gMnSc2	Penn
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
4	[number]	k4	4
roky	rok	k1gInPc7	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc4d3	nejstarší
dceru	dcera	k1gFnSc4	dcera
Lourdes	Lourdesa	k1gFnPc2	Lourdesa
Maria	Maria	k1gFnSc1	Maria
Ciccone	Ciccon	k1gInSc5	Ciccon
Leon	Leona	k1gFnPc2	Leona
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
má	můj	k3xOp1gFnSc1	můj
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
osobním	osobní	k2eAgMnSc7d1	osobní
trenérem	trenér	k1gMnSc7	trenér
Carlosem	Carlos	k1gMnSc7	Carlos
Leonem	Leo	k1gMnSc7	Leo
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevzala	vzít	k5eNaPmAgFnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
sňatek	sňatek	k1gInSc1	sňatek
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
s	s	k7c7	s
režisérem	režisér	k1gMnSc7	režisér
Guyem	Guy	k1gMnSc7	Guy
Ritchiem	Ritchius	k1gMnSc7	Ritchius
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
měla	mít	k5eAaImAgFnS	mít
syna	syn	k1gMnSc4	syn
Rocca	Roccus	k1gMnSc4	Roccus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
s	s	k7c7	s
Guyem	Guy	k1gInSc7	Guy
se	se	k3xPyFc4	se
ale	ale	k9	ale
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
rozešla	rozejít	k5eAaPmAgFnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
další	další	k2eAgFnPc4d1	další
2	[number]	k4	2
děti	dítě	k1gFnPc4	dítě
Davida	David	k1gMnSc2	David
Bandu	band	k1gInSc2	band
a	a	k8xC	a
Chifundo	Chifundo	k1gNnSc4	Chifundo
Mercy	Merca	k1gFnSc2	Merca
James	Jamesa	k1gFnPc2	Jamesa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
adoptovala	adoptovat	k5eAaPmAgFnS	adoptovat
z	z	k7c2	z
afrického	africký	k2eAgInSc2d1	africký
státu	stát	k1gInSc2	stát
Malawi	Malawi	k1gNnSc2	Malawi
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
měla	mít	k5eAaImAgFnS	mít
rodinné	rodinný	k2eAgInPc4d1	rodinný
problémy	problém	k1gInPc4	problém
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
synem	syn	k1gMnSc7	syn
Roccem	Rocce	k1gMnSc7	Rocce
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Madonna	Madonna	k1gFnSc1	Madonna
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nekorunovanou	korunovaný	k2eNgFnSc4d1	nekorunovaná
královnu	královna	k1gFnSc4	královna
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejúspěšnější	úspěšný	k2eAgFnSc1d3	nejúspěšnější
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Guinnesovy	Guinnesův	k2eAgFnSc2d1	Guinnesova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
je	být	k5eAaImIp3nS	být
také	také	k9	také
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
příjmem	příjem	k1gInSc7	příjem
<g/>
.	.	kIx.	.
</s>

