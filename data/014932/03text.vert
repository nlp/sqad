<s>
Lom	lom	k1gInSc1
Kační	Kační	k2eAgFnSc2d1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuLom	infoboxuLom	k1gInSc4
Kační	Kační	k2eAgInSc1d1
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
lom	lom	k1gInSc4
z	z	k7c2
naučné	naučný	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
vedoucí	vedoucí	k1gMnSc1
kolemZákladní	kolemZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
294	#num#	k4
<g/>
–	–	k?
<g/>
320	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Umístění	umístění	k1gNnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
5	#num#	k4
Jinonice	Jinonice	k1gFnPc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
47,86	47,86	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
47,72	47,72	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Lom	lom	k1gInSc1
Kační	Kační	k2eAgFnSc2d1
</s>
<s>
lom	lom	k1gInSc1
Kační	Kačeň	k1gFnPc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Prahy	Praha	k1gFnSc2
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Tento	tento	k3xDgInSc4
box	box	k1gInSc4
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Lom	lom	k1gInSc1
Kační	Kační	k1gNnSc2
je	být	k5eAaImIp3nS
málo	málo	k6eAd1
známý	známý	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
lom	lom	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
na	na	k7c6
naučné	naučný	k2eAgFnSc6d1
stezce	stezka	k1gFnSc6
na	na	k7c6
začátku	začátek	k1gInSc6
Prokopského	prokopský	k2eAgNnSc2d1
údolí	údolí	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Stěna	stěna	k1gFnSc1
opuštěného	opuštěný	k2eAgInSc2d1
lomu	lom	k1gInSc2
odkrývá	odkrývat	k5eAaImIp3nS
čedičový	čedičový	k2eAgInSc4d1
výron	výron	k1gInSc4
(	(	kIx(
<g/>
žílu	žíla	k1gFnSc4
<g/>
)	)	kIx)
v	v	k7c6
usazených	usazený	k2eAgFnPc6d1
horninách	hornina	k1gFnPc6
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
další	další	k2eAgMnPc1d1
z	z	k7c2
pozůstatků	pozůstatek	k1gInPc2
pravěké	pravěký	k2eAgFnSc2d1
podmořské	podmořský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Žíla	žíla	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
vulkanickou	vulkanický	k2eAgFnSc7d1
horninou	hornina	k1gFnSc7
nazývanou	nazývaný	k2eAgFnSc7d1
basalt	basalt	k1gInSc4
(	(	kIx(
<g/>
diabas	diabas	k1gInSc4
<g/>
)	)	kIx)
a	a	k8xC
do	do	k7c2
nezpevněných	zpevněný	k2eNgFnPc2d1
usazenin	usazenina	k1gFnPc2
na	na	k7c6
dně	dno	k1gNnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
prvohorního	prvohorní	k2eAgNnSc2d1
silurského	silurský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
pronikla	proniknout	k5eAaPmAgFnS
v	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
asi	asi	k9
425	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
přímé	přímý	k2eAgFnSc6d1
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
vznikem	vznik	k1gInSc7
podmořské	podmořský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
mezi	mezi	k7c7
dnešními	dnešní	k2eAgFnPc7d1
Butovicemi	Butovice	k1gFnPc7
a	a	k8xC
Řeporyjemi	Řeporyje	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Nad	nad	k7c7
touto	tento	k3xDgFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
žílou	žíla	k1gFnSc7
(	(	kIx(
<g/>
v	v	k7c6
nadloží	nadloží	k1gNnSc6
tj.	tj.	kA
v	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
části	část	k1gFnSc6
lomu	lom	k1gInSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
obnaženy	obnažen	k2eAgFnPc1d1
břidlice	břidlice	k1gFnPc1
a	a	k8xC
vápence	vápenec	k1gInPc1
tzv.	tzv.	kA
motolského	motolský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgFnPc1
vrstvy	vrstva	k1gFnPc1
obsahovaly	obsahovat	k5eAaImAgFnP
mnoho	mnoho	k4c4
zkamenělin	zkamenělina	k1gFnPc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
nacházet	nacházet	k5eAaImF
v	v	k7c6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
ještě	ještě	k9
v	v	k7c6
lomu	lom	k1gInSc6
těžilo	těžit	k5eAaImAgNnS
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Trochu	trochu	k6eAd1
výše	vysoce	k6eAd2
nad	nad	k7c7
lomem	lom	k1gInSc7
se	se	k3xPyFc4
cesta	cesta	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
naučnou	naučný	k2eAgFnSc7d1
stezkou	stezka	k1gFnSc7
<g/>
)	)	kIx)
do	do	k7c2
vrchu	vrch	k1gInSc2
mírně	mírně	k6eAd1
ohýbá	ohýbat	k5eAaImIp3nS
a	a	k8xC
zde	zde	k6eAd1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
poněkud	poněkud	k6eAd1
mladší	mladý	k2eAgFnSc2d2
vrstvy	vrstva	k1gFnSc2
tzv.	tzv.	kA
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
naleziště	naleziště	k1gNnSc2
bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
mnoho	mnoho	k4c1
druhů	druh	k1gInPc2
zkamenělin	zkamenělina	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
zejména	zejména	k9
hlavonožců	hlavonožec	k1gMnPc2
<g/>
,	,	kIx,
plžů	plž	k1gMnPc2
a	a	k8xC
mlžů	mlž	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
A	a	k9
právě	právě	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
těchto	tento	k3xDgMnPc2
zkamenělých	zkamenělý	k2eAgMnPc2d1
mlžů	mlž	k1gMnPc2
silurského	silurský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
(	(	kIx(
<g/>
nalezený	nalezený	k2eAgInSc1d1
ve	v	k7c6
spodních	spodní	k2eAgFnPc6d1
polohách	poloha	k1gFnPc6
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
podle	podle	k7c2
obce	obec	k1gFnSc2
Butovice	Butovice	k1gFnSc1
jménem	jméno	k1gNnSc7
Butovicella	Butovicello	k1gNnSc2
migrans	migrans	k1gInSc1
(	(	kIx(
<g/>
Barrande	Barrand	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
břidlicích	břidlice	k1gFnPc6
z	z	k7c2
lomu	lom	k1gInSc2
Kační	Kační	k1gMnSc1
byl	být	k5eAaImAgMnS
nalezen	nalézt	k5eAaBmNgMnS,k5eAaPmNgMnS
i	i	k9
graptolit	graptolit	k1gMnSc1
Monograptus	Monograptus	k1gMnSc1
flemmingii	flemmingie	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sběr	sběr	k1gInSc1
zkamenělin	zkamenělina	k1gFnPc2
je	být	k5eAaImIp3nS
povolen	povolit	k5eAaPmNgInS
jen	jen	k9
v	v	k7c4
kamenné	kamenný	k2eAgFnPc4d1
suti	suť	k1gFnPc4
u	u	k7c2
paty	pata	k1gFnSc2
stěny	stěna	k1gFnSc2
lomu	lom	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
kameny	kámen	k1gInPc4
z	z	k7c2
motolského	motolský	k2eAgNnSc2d1
a	a	k8xC
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vegetace	vegetace	k1gFnSc1
</s>
<s>
Na	na	k7c6
okrajích	okraj	k1gInPc6
lomu	lom	k1gInSc2
Kační	Kační	k1gNnSc1
je	být	k5eAaImIp3nS
uchován	uchován	k2eAgInSc1d1
původní	původní	k2eAgInSc1d1
tvar	tvar	k1gInSc1
terénu	terén	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
skalkách	skalka	k1gFnPc6
zde	zde	k6eAd1
se	se	k3xPyFc4
nacházejících	nacházející	k2eAgNnPc2d1
roste	růst	k5eAaImIp3nS
původní	původní	k2eAgInSc1d1
ekosystém	ekosystém	k1gInSc1
skalní	skalní	k2eAgFnSc2d1
stepi	step	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
přizpůsoben	přizpůsoben	k2eAgMnSc1d1
životním	životní	k2eAgFnPc3d1
podmínkám	podmínka	k1gFnPc3
na	na	k7c6
povrchu	povrch	k1gInSc6
skalního	skalní	k2eAgInSc2d1
podkladu	podklad	k1gInSc2
<g/>
:	:	kIx,
nedostatku	nedostatek	k1gInSc2
vlhkosti	vlhkost	k1gFnSc2
a	a	k8xC
velkému	velký	k2eAgNnSc3d1
kolísání	kolísání	k1gNnSc3
denních	denní	k2eAgFnPc2d1
teplot	teplota	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
vzácný	vzácný	k2eAgInSc4d1
a	a	k8xC
silně	silně	k6eAd1
ohrožený	ohrožený	k2eAgInSc1d1
glaciální	glaciální	k2eAgInSc1d1
relikt	relikt	k1gInSc1
–	–	k?
česnek	česnek	k1gInSc1
tuhý	tuhý	k2eAgInSc1d1
(	(	kIx(
<g/>
Allium	Allium	k1gNnSc1
strictum	strictum	k1gNnSc1
Schrader	Schrader	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Méně	málo	k6eAd2
strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
lomu	lom	k1gInSc2
byly	být	k5eAaImAgInP
původně	původně	k6eAd1
zarostlé	zarostlý	k2eAgNnSc1d1
listnatým	listnatý	k2eAgInSc7d1
lesem	les	k1gInSc7
(	(	kIx(
<g/>
dub	dub	k1gInSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
habr	habr	k1gInSc1
<g/>
,	,	kIx,
javor	javor	k1gInSc1
<g/>
,	,	kIx,
lípa	lípa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
byly	být	k5eAaImAgFnP
odlesněny	odlesnit	k5eAaPmNgFnP
a	a	k8xC
přeměněny	přeměnit	k5eAaPmNgFnP
na	na	k7c4
pastviny	pastvina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
provádělo	provádět	k5eAaImAgNnS
namátkové	namátkový	k2eAgNnSc1d1
zalesňování	zalesňování	k1gNnSc1
jehličnany	jehličnan	k1gInPc7
(	(	kIx(
<g/>
borovice	borovice	k1gFnSc1
černá	černý	k2eAgFnSc1d1
<g/>
,	,	kIx,
modřín	modřín	k1gInSc1
<g/>
,	,	kIx,
smrk	smrk	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Listnaté	listnatý	k2eAgFnSc2d1
dřeviny	dřevina	k1gFnSc2
samovolně	samovolně	k6eAd1
zarůstají	zarůstat	k5eAaImIp3nP
postupně	postupně	k6eAd1
křovité	křovitý	k2eAgNnSc4d1
patro	patro	k1gNnSc4
lesa	les	k1gInSc2
a	a	k8xC
řízené	řízený	k2eAgInPc4d1
lidské	lidský	k2eAgInPc4d1
zásahy	zásah	k1gInPc4
do	do	k7c2
druhového	druhový	k2eAgNnSc2d1
složení	složení	k1gNnSc2
stromů	strom	k1gInPc2
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
směřují	směřovat	k5eAaImIp3nP
opět	opět	k6eAd1
k	k	k7c3
listnatým	listnatý	k2eAgInPc3d1
druhům	druh	k1gInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Lom	lom	k1gInSc1
Kační	Kační	k1gNnSc2
(	(	kIx(
<g/>
ilustrační	ilustrační	k2eAgNnPc1d1
fota	foto	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Fosilní	fosilní	k2eAgMnSc1d1
Monograptus	Monograptus	k1gMnSc1
flemingii	flemingie	k1gFnSc4
(	(	kIx(
<g/>
silur	silur	k1gInSc4
<g/>
)	)	kIx)
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
praktik	praktika	k1gFnPc2
Přírodovědecké	přírodovědecký	k2eAgFnSc2d1
fakulty	fakulta	k1gFnSc2
Univerzity	univerzita	k1gFnSc2
v	v	k7c6
Corunně	Corunně	k1gFnSc6
</s>
<s>
Česnek	česnek	k1gInSc1
tuhý	tuhý	k2eAgInSc1d1
(	(	kIx(
<g/>
Allium	Allium	k1gNnSc1
strictum	strictum	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Sběrem	sběr	k1gInSc7
v	v	k7c6
suti	suť	k1gFnSc6
na	na	k7c6
dně	dno	k1gNnSc6
lomu	lom	k1gInSc2
nalezená	nalezený	k2eAgFnSc1d1
neidentifikovaná	identifikovaný	k2eNgFnSc1d1
zkamenělina	zkamenělina	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sběrem	sběr	k1gInSc7
v	v	k7c6
suti	suť	k1gFnSc6
na	na	k7c6
dně	dno	k1gNnSc6
lomu	lom	k1gInSc2
nalezená	nalezený	k2eAgFnSc1d1
neidentifikovaná	identifikovaný	k2eNgFnSc1d1
zkamenělina	zkamenělina	k1gFnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Tento	tento	k3xDgInSc4
druh	druh	k1gInSc4
mlže	mlž	k1gMnSc2
–	–	k?
Butovicella	Butovicella	k1gMnSc1
migrans	migrans	k6eAd1
(	(	kIx(
<g/>
Cardiola	Cardiola	k1gFnSc1
migrans	migrans	k6eAd1
Barrande	Barrand	k1gInSc5
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
spolu	spolu	k6eAd1
s	s	k7c7
dalšími	další	k1gNnPc7
<g/>
)	)	kIx)
obýval	obývat	k5eAaImAgMnS
silurské	silurský	k2eAgNnSc4d1
moře	moře	k1gNnSc4
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
Švédsku	Švédsko	k1gNnSc6
<g/>
,	,	kIx,
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
Portugalsku	Portugalsko	k1gNnSc6
a	a	k8xC
byl	být	k5eAaImAgMnS
nalezen	nalézt	k5eAaBmNgMnS,k5eAaPmNgMnS
i	i	k9
ve	v	k7c6
vrtech	vrt	k1gInPc6
na	na	k7c6
Floridě	Florida	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Procházka	Procházka	k1gMnSc1
třetí	třetí	k4xOgFnSc3
–	–	k?
Ze	z	k7c2
Starých	Starých	k2eAgFnPc2d1
Butovic	Butovice	k1gFnPc2
na	na	k7c4
Zlíchov	Zlíchov	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
Orendor	Orendor	k1gInSc4
cz	cz	k?
(	(	kIx(
<g/>
zahrady	zahrada	k1gFnPc1
<g/>
,	,	kIx,
parky	park	k1gInPc1
<g/>
,	,	kIx,
krajiny	krajina	k1gFnPc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Česky	česky	k6eAd1
psaný	psaný	k2eAgInSc4d1
text	text	k1gInSc4
na	na	k7c6
veřejně	veřejně	k6eAd1
přístupné	přístupný	k2eAgFnSc6d1
informační	informační	k2eAgFnSc6d1
tabuli	tabule	k1gFnSc6
s	s	k7c7
názevem	název	k1gInSc7
<g/>
:	:	kIx,
Naučná	naučný	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
Prokopské	prokopský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
–	–	k?
Butovickým	Butovický	k2eAgNnSc7d1
hradištěm	hradiště	k1gNnSc7
<g/>
;	;	kIx,
zastávka	zastávka	k1gFnSc1
číslo	číslo	k1gNnSc1
1	#num#	k4
<g/>
;	;	kIx,
Lom	lom	k1gInSc1
„	„	k?
<g/>
Kační	Kační	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Butovicella	Butovicella	k1gFnSc1
migrans	migransa	k1gFnPc2
(	(	kIx(
<g/>
Barrande	Barrand	k1gInSc5
<g/>
,	,	kIx,
1881	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Vědecké	vědecký	k2eAgNnSc1d1
synonymum	synonymum	k1gNnSc1
<g/>
:	:	kIx,
Cardiola	Cardiola	k1gFnSc1
migrans	migrans	k6eAd1
Barrande	Barrand	k1gInSc5
<g/>
,	,	kIx,
1881	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
Biblio	Biblio	k6eAd1
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říše	říše	k1gFnSc1
Animalia	Animalia	k1gFnSc1
-	-	kIx~
živočichové	živočich	k1gMnPc1
<g/>
;	;	kIx,
kmen	kmen	k1gInSc1
Mollusca	Mollusc	k1gInSc2
-	-	kIx~
měkkýši	měkkýš	k1gMnPc1
<g/>
;	;	kIx,
třída	třída	k1gFnSc1
Bivalvia	Bivalvia	k1gFnSc1
-	-	kIx~
mlži	mlž	k1gMnPc1
<g/>
;	;	kIx,
řád	řád	k1gInSc1
Modiomorphida	Modiomorphida	k1gFnSc1
<g/>
;	;	kIx,
čeleď	čeleď	k1gFnSc1
Modiomorphidae	Modiomorphidae	k1gFnSc1
<g/>
;	;	kIx,
rod	rod	k1gInSc1
Butovicella	Butovicello	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Lokalita	lokalita	k1gFnSc1
<g/>
:	:	kIx,
Lom	lom	k1gInSc1
Kační	Kační	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
Paleoweb	Paleowba	k1gFnPc2
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadřazené	nadřazený	k2eAgFnPc1d1
lokality	lokalita	k1gFnPc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
;	;	kIx,
Čechy	Čechy	k1gFnPc1
<g/>
;	;	kIx,
Barrandien	barrandien	k1gInSc1
<g/>
;	;	kIx,
Praha	Praha	k1gFnSc1
a	a	k8xC
blízké	blízký	k2eAgNnSc1d1
okolí	okolí	k1gNnSc1
<g/>
;	;	kIx,
Prokopské	prokopský	k2eAgNnSc1d1
údolí	údolí	k1gNnSc1
<g/>
;	;	kIx,
Souřadnice	souřadnice	k1gFnSc1
GPS	GPS	kA
<g/>
:	:	kIx,
50.046	50.046	k4
<g/>
6264	#num#	k4
<g/>
N	N	kA
<g/>
,	,	kIx,
14.363	14.363	k4
<g/>
2542	#num#	k4
<g/>
E	E	kA
<g/>
;	;	kIx,
Způsob	způsob	k1gInSc1
sběru	sběr	k1gInSc2
<g/>
:	:	kIx,
Sběr	sběr	k1gInSc1
v	v	k7c6
suti	suť	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOSKOVEC	HOSKOVEC	kA
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ALLIUM	ALLIUM	kA
STRICTUM	STRICTUM	kA
Schrader	Schrader	k1gMnSc1
–	–	k?
česnek	česnek	k1gInSc1
tuhý	tuhý	k2eAgInSc1d1
(	(	kIx(
<g/>
synonyma	synonymum	k1gNnSc2
<g/>
:	:	kIx,
Allium	Allium	k1gNnSc1
lineare	linear	k1gMnSc5
L.	L.	kA
<g/>
,	,	kIx,
Allium	Allium	k1gNnSc1
volhynicum	volhynicum	k1gInSc1
Bess	Bess	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Web	web	k1gInSc1
Botany	Botana	k1gFnSc2
cz	cz	k?
<g/>
,	,	kIx,
2007-07-08	2007-07-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Butovice	Butovice	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Motolský	motolský	k2eAgInSc1d1
ordovik	ordovik	k1gInSc1
</s>
<s>
Zmrzlík	zmrzlík	k1gInSc1
(	(	kIx(
<g/>
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hemrovy	Hemrův	k2eAgFnPc1d1
skály	skála	k1gFnPc1
</s>
<s>
Albrechtův	Albrechtův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Lom	lom	k1gInSc1
Kační	Kační	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Praha	Praha	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
|	|	kIx~
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
</s>
