<s>
Sběr	sběr	k1gInSc1
zkamenělin	zkamenělina	k1gFnPc2
je	být	k5eAaImIp3nS
povolen	povolit	k5eAaPmNgInS
jen	jen	k9
v	v	k7c4
kamenné	kamenný	k2eAgFnPc4d1
suti	suť	k1gFnPc4
u	u	k7c2
paty	pata	k1gFnSc2
stěny	stěna	k1gFnSc2
lomu	lom	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
kameny	kámen	k1gInPc4
z	z	k7c2
motolského	motolský	k2eAgNnSc2d1
a	a	k8xC
kopaninského	kopaninský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>