<p>
<s>
Černá	Černá	k1gFnSc1	Černá
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
barva	barva	k1gFnSc1	barva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
oko	oko	k1gNnSc1	oko
vnímá	vnímat	k5eAaImIp3nS	vnímat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
daného	daný	k2eAgInSc2d1	daný
směru	směr	k1gInSc2	směr
nepřichází	přicházet	k5eNaImIp3nS	přicházet
žádné	žádný	k3yNgNnSc1	žádný
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
nedostatek	nedostatek	k1gInSc1	nedostatek
světla	světlo	k1gNnSc2	světlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dán	dát	k5eAaPmNgMnS	dát
jak	jak	k6eAd1	jak
neexistencí	neexistence	k1gFnSc7	neexistence
žádného	žádný	k3yNgInSc2	žádný
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
tak	tak	k9	tak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
příslušný	příslušný	k2eAgInSc1d1	příslušný
materiál	materiál	k1gInSc1	materiál
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
světlo	světlo	k1gNnSc4	světlo
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
aby	aby	kYmCp3nS	aby
některé	některý	k3yIgFnPc4	některý
barvy	barva	k1gFnPc4	barva
odrážel	odrážet	k5eAaImAgMnS	odrážet
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
pigment	pigment	k1gInSc1	pigment
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k6eAd1	také
tvořen	tvořit	k5eAaImNgInS	tvořit
směsí	směs	k1gFnSc7	směs
několika	několik	k4yIc2	několik
pigmentů	pigment	k1gInPc2	pigment
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
některé	některý	k3yIgFnPc4	některý
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
souhrnu	souhrn	k1gInSc6	souhrn
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
tak	tak	k6eAd1	tak
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
viditelného	viditelný	k2eAgNnSc2d1	viditelné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
černý	černý	k2eAgInSc4d1	černý
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
CMYK	CMYK	kA	CMYK
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
pohledy	pohled	k1gInPc1	pohled
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
definicím	definice	k1gFnPc3	definice
aditivního	aditivní	k2eAgNnSc2d1	aditivní
a	a	k8xC	a
subtraktivního	subtraktivní	k2eAgNnSc2d1	subtraktivní
míchání	míchání	k1gNnSc2	míchání
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
nejtmavší	tmavý	k2eAgInSc1d3	nejtmavší
odstín	odstín	k1gInSc1	odstín
bílé	bílý	k2eAgFnSc2d1	bílá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staročeskou	staročeský	k2eAgFnSc7d1	staročeská
variantou	varianta	k1gFnSc7	varianta
"	"	kIx"	"
<g/>
černý	černý	k2eAgMnSc1d1	černý
<g/>
"	"	kIx"	"
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
črný	črný	k?	črný
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
čirný	čirný	k2eAgMnSc1d1	čirný
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
praslovanského	praslovanský	k2eAgInSc2d1	praslovanský
*	*	kIx~	*
<g/>
čь	čь	k?	čь
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovinsky	slovinsky	k6eAd1	slovinsky
"	"	kIx"	"
<g/>
črna	črna	k6eAd1	črna
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
prabaltoslovanského	prabaltoslovanský	k2eAgMnSc2d1	prabaltoslovanský
*	*	kIx~	*
<g/>
kirsnos	kirsnos	k1gInSc1	kirsnos
(	(	kIx(	(
<g/>
např.	např.	kA	např.
litevsky	litevsky	k6eAd1	litevsky
"	"	kIx"	"
<g/>
kirsnas	kirsnas	k1gInSc1	kirsnas
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
snad	snad	k9	snad
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
indickým	indický	k2eAgMnSc7d1	indický
kršna	kršno	k1gNnPc1	kršno
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
CMYK	CMYK	kA	CMYK
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
CMY	CMY	kA	CMY
<g/>
(	(	kIx(	(
<g/>
K	K	kA	K
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
možno	možno	k6eAd1	možno
vytvořit	vytvořit	k5eAaPmF	vytvořit
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
V	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
modelu	model	k1gInSc6	model
CMY	CMY	kA	CMY
černá	černat	k5eAaImIp3nS	černat
vznikala	vznikat	k5eAaImAgFnS	vznikat
smíšením	smíšení	k1gNnSc7	smíšení
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
základních	základní	k2eAgInPc2d1	základní
pigmentů	pigment	k1gInPc2	pigment
v	v	k7c6	v
příslušném	příslušný	k2eAgInSc6d1	příslušný
poměru	poměr	k1gInSc6	poměr
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
výsledná	výsledný	k2eAgFnSc1d1	výsledná
směs	směs	k1gFnSc1	směs
pohlcovala	pohlcovat	k5eAaImAgFnS	pohlcovat
všechny	všechen	k3xTgFnPc4	všechen
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Taková	takový	k3xDgFnSc1	takový
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
nekvalitní	kvalitní	k2eNgNnSc1d1	nekvalitní
<g/>
,	,	kIx,	,
s	s	k7c7	s
barevným	barevný	k2eAgInSc7d1	barevný
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
špinavě	špinavě	k6eAd1	špinavě
hnědým	hnědý	k2eAgInSc7d1	hnědý
<g/>
)	)	kIx)	)
nádechem	nádech	k1gInSc7	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
ke	k	k7c3	k
třem	tři	k4xCgFnPc3	tři
základním	základní	k2eAgFnPc3d1	základní
pigmentům	pigment	k1gInPc3	pigment
přidal	přidat	k5eAaPmAgMnS	přidat
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
černý	černý	k2eAgInSc4d1	černý
pigment	pigment	k1gInSc4	pigment
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
právě	právě	k6eAd1	právě
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
černi	čerň	k1gFnSc2wR	čerň
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
barevný	barevný	k2eAgInSc4d1	barevný
model	model	k1gInSc4	model
CMYK	CMYK	kA	CMYK
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
černou	černý	k2eAgFnSc7d1	černá
v	v	k7c6	v
barevném	barevný	k2eAgInSc6d1	barevný
modelu	model	k1gInSc6	model
CMYK	CMYK	kA	CMYK
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
)	)	kIx)	)
–	–	k?	–
teoretická	teoretický	k2eAgFnSc1d1	teoretická
čerň	čerň	k1gFnSc1	čerň
v	v	k7c6	v
modelu	model	k1gInSc6	model
CMY	CMY	kA	CMY
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
prakticky	prakticky	k6eAd1	prakticky
používaná	používaný	k2eAgFnSc1d1	používaná
čerň	čerň	k1gFnSc1	čerň
v	v	k7c6	v
modelu	model	k1gInSc6	model
CMYK	CMYK	kA	CMYK
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
)	)	kIx)	)
–	–	k?	–
tzv.	tzv.	kA	tzv.
soutisková	soutiskový	k2eAgFnSc1d1	soutisková
černá	černá	k1gFnSc1	černá
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc7	její
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
barevných	barevný	k2eAgInPc2d1	barevný
výtažků	výtažek	k1gInPc2	výtažek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
předlohu	předloha	k1gFnSc4	předloha
tisku	tisk	k1gInSc2	tisk
</s>
</p>
<p>
<s>
==	==	k?	==
Významy	význam	k1gInPc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
černí	černý	k1gMnPc1	černý
(	(	kIx(	(
<g/>
černoši	černoch	k1gMnPc1	černoch
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
lidé	člověk	k1gMnPc1	člověk
černé	černý	k2eAgFnSc2d1	černá
rasy	rasa	k1gFnSc2	rasa
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
černá	černý	k2eAgFnSc1d1	černá
rasa	rasa	k1gFnSc1	rasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
západní	západní	k2eAgFnSc4d1	západní
tradici	tradice	k1gFnSc4	tradice
barvou	barva	k1gFnSc7	barva
s	s	k7c7	s
negativním	negativní	k2eAgInSc7d1	negativní
významem	význam	k1gInSc7	význam
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
původ	původ	k1gInSc4	původ
ve	v	k7c6	v
vnímání	vnímání	k1gNnSc6	vnímání
noci	noc	k1gFnSc2	noc
jako	jako	k8xC	jako
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
černé	černý	k2eAgInPc1d1	černý
šaty	šat	k1gInPc1	šat
nosily	nosit	k5eAaImAgInP	nosit
při	při	k7c6	při
pohřbu	pohřeb	k1gInSc6	pohřeb
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
duch	duch	k1gMnSc1	duch
zemřelého	zemřelý	k1gMnSc2	zemřelý
černě	čerň	k1gFnSc2	čerň
oděnou	oděný	k2eAgFnSc4d1	oděná
osobu	osoba	k1gFnSc4	osoba
"	"	kIx"	"
<g/>
přehlédl	přehlédnout	k5eAaPmAgMnS	přehlédnout
<g/>
"	"	kIx"	"
a	a	k8xC	a
nevzal	vzít	k5eNaPmAgMnS	vzít
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
s	s	k7c7	s
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
pohřební	pohřební	k2eAgFnSc7d1	pohřební
barvou	barva	k1gFnSc7	barva
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
barvou	barva	k1gFnSc7	barva
smutku	smutek	k1gInSc2	smutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
černý	černý	k2eAgInSc1d1	černý
den	den	k1gInSc1	den
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
několik	několik	k4yIc1	několik
smutných	smutný	k2eAgFnPc2d1	smutná
či	či	k8xC	či
tragických	tragický	k2eAgFnPc2d1	tragická
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k1gMnPc4	jiný
několik	několik	k4yIc1	několik
burzovních	burzovní	k2eAgInPc2d1	burzovní
krachů	krach	k1gInPc2	krach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
humor	humor	k1gInSc1	humor
jsou	být	k5eAaImIp3nP	být
vtipy	vtip	k1gInPc4	vtip
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
vraždách	vražda	k1gFnPc6	vražda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
komedie	komedie	k1gFnSc1	komedie
je	být	k5eAaImIp3nS	být
komediální	komediální	k2eAgInSc4d1	komediální
žánr	žánr	k1gInSc4	žánr
využívajících	využívající	k2eAgNnPc2d1	využívající
morbidních	morbidní	k2eAgNnPc2d1	morbidní
či	či	k8xC	či
tragických	tragický	k2eAgNnPc2d1	tragické
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
listina	listina	k1gFnSc1	listina
(	(	kIx(	(
<g/>
blacklist	blacklist	k1gInSc1	blacklist
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
seznam	seznam	k1gInSc4	seznam
nežádoucích	žádoucí	k2eNgFnPc2d1	nežádoucí
osob	osoba	k1gFnPc2	osoba
či	či	k8xC	či
jiných	jiný	k2eAgFnPc2d1	jiná
entit	entita	k1gFnPc2	entita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
trh	trh	k1gInSc1	trh
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
nelegální	legální	k2eNgInSc4d1	nelegální
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černá	Černá	k1gFnSc1	Černá
označuje	označovat	k5eAaImIp3nS	označovat
neznámo	neznámo	k6eAd1	neznámo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
skříňka	skříňka	k1gFnSc1	skříňka
označuje	označovat	k5eAaImIp3nS	označovat
mechanismus	mechanismus	k1gInSc4	mechanismus
s	s	k7c7	s
neznámou	známý	k2eNgFnSc7d1	neznámá
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
funkčností	funkčnost	k1gFnSc7	funkčnost
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
pouze	pouze	k6eAd1	pouze
podle	podle	k7c2	podle
vnějších	vnější	k2eAgInPc2d1	vnější
projevů	projev	k1gInPc2	projev
chování	chování	k1gNnSc2	chování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
je	být	k5eAaImIp3nS	být
astronomický	astronomický	k2eAgInSc4d1	astronomický
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
neuniká	unikat	k5eNaImIp3nS	unikat
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
<g/>
)	)	kIx)	)
žádné	žádný	k3yNgNnSc1	žádný
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
vlajka	vlajka	k1gFnSc1	vlajka
příkazem	příkaz	k1gInSc7	příkaz
řidiči	řidič	k1gMnSc3	řidič
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zajel	zajet	k5eAaPmAgMnS	zajet
do	do	k7c2	do
boxů	box	k1gInPc2	box
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
0	[number]	k4	0
</s>
</p>
<p>
<s>
V	v	k7c6	v
účetnictví	účetnictví	k1gNnSc6	účetnictví
se	se	k3xPyFc4	se
černou	černý	k2eAgFnSc7d1	černá
barvou	barva	k1gFnSc7	barva
označuje	označovat	k5eAaImIp3nS	označovat
zisk	zisk	k1gInSc1	zisk
<g/>
,	,	kIx,	,
výraz	výraz	k1gInSc1	výraz
v	v	k7c6	v
černých	černý	k2eAgNnPc6d1	černé
číslech	číslo	k1gNnPc6	číslo
označuje	označovat	k5eAaImIp3nS	označovat
kladný	kladný	k2eAgInSc4d1	kladný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
výsledek	výsledek	k1gInSc4	výsledek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
je	být	k5eAaImIp3nS	být
také	také	k9	také
barva	barva	k1gFnSc1	barva
metalu	metal	k1gInSc2	metal
nebo	nebo	k8xC	nebo
Emo	Ema	k1gFnSc5	Ema
stylu	styl	k1gInSc3	styl
<g/>
.	.	kIx.	.
<g/>
v	v	k7c6	v
politické	politický	k2eAgFnSc6d1	politická
symbolice	symbolika	k1gFnSc6	symbolika
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
barvu	barva	k1gFnSc4	barva
fašismu	fašismus	k1gInSc2	fašismus
(	(	kIx(	(
<g/>
Černá	černý	k2eAgFnSc1d1	černá
internacionála	internacionála	k1gFnSc1	internacionála
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
černou	černý	k2eAgFnSc4d1	černá
barvu	barva	k1gFnSc4	barva
používají	používat	k5eAaImIp3nP	používat
dost	dost	k6eAd1	dost
často	často	k6eAd1	často
anarchisté	anarchista	k1gMnPc1	anarchista
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
a	a	k8xC	a
šatech	šat	k1gInPc6	šat
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nS	by
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
nekončící	končící	k2eNgInSc4d1	nekončící
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
autoritám	autorita	k1gFnPc3	autorita
jako	jako	k9	jako
opak	opak	k1gInSc1	opak
k	k	k7c3	k
bílé	bílý	k2eAgFnSc3d1	bílá
barvě	barva	k1gFnSc3	barva
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
černá	černat	k5eAaImIp3nS	černat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
černý	černý	k1gMnSc1	černý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
