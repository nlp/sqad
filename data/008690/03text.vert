<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Brož	Brož	k1gMnSc1	Brož
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1981	[number]	k4	1981
Písek	Písek	k1gInSc1	Písek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
zastupitel	zastupitel	k1gMnSc1	zastupitel
města	město	k1gNnSc2	město
Písek	Písek	k1gInSc1	Písek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
střední	střední	k2eAgFnSc4d1	střední
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc4d2	vyšší
odbornou	odborný	k2eAgFnSc4d1	odborná
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
vývojový	vývojový	k2eAgMnSc1d1	vývojový
pracovník	pracovník	k1gMnSc1	pracovník
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
automatizace	automatizace	k1gFnSc2	automatizace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	let	k1gInPc6	let
začal	začít	k5eAaPmAgMnS	začít
podnikat	podnikat	k5eAaImF	podnikat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
správce	správce	k1gMnSc1	správce
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
po	po	k7c6	po
dalších	další	k2eAgNnPc6d1	další
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
založil	založit	k5eAaPmAgMnS	založit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
internetovou	internetový	k2eAgFnSc4d1	internetová
síť	síť	k1gFnSc4	síť
s	s	k7c7	s
názvem	název	k1gInSc7	název
Cryonix	Cryonix	k1gInSc1	Cryonix
Innovations	Innovationsa	k1gFnPc2	Innovationsa
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
expandovat	expandovat	k5eAaImF	expandovat
i	i	k9	i
do	do	k7c2	do
dalších	další	k2eAgInPc2d1	další
oborů	obor	k1gInPc2	obor
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
úklidové	úklidový	k2eAgFnPc1d1	úklidová
služby	služba	k1gFnPc1	služba
<g/>
,	,	kIx,	,
hodinový	hodinový	k2eAgMnSc1d1	hodinový
manžel	manžel	k1gMnSc1	manžel
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
,	,	kIx,	,
reklamní	reklamní	k2eAgFnSc1d1	reklamní
agentura	agentura	k1gFnSc1	agentura
<g/>
,	,	kIx,	,
návrhy	návrh	k1gInPc4	návrh
internetových	internetový	k2eAgFnPc2d1	internetová
stránek	stránka	k1gFnPc2	stránka
a	a	k8xC	a
také	také	k6eAd1	také
servisu	servis	k1gInSc2	servis
a	a	k8xC	a
údržby	údržba	k1gFnSc2	údržba
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Písku	Písek	k1gInSc6	Písek
<g/>
.	.	kIx.	.
</s>
<s>
Angažuje	angažovat	k5eAaBmIp3nS	angažovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
spolcích	spolek	k1gInPc6	spolek
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
Písecké	písecký	k2eAgFnSc2d1	Písecká
kočky	kočka	k1gFnSc2	kočka
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
zajímá	zajímat	k5eAaImIp3nS	zajímat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
udržitelnosti	udržitelnost	k1gFnSc2	udržitelnost
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
začal	začít	k5eAaPmAgMnS	začít
vystupovat	vystupovat	k5eAaImF	vystupovat
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
"	"	kIx"	"
<g/>
Cryoman	Cryoman	k1gMnSc1	Cryoman
<g/>
"	"	kIx"	"
jakožto	jakožto	k8xS	jakožto
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
správců	správce	k1gMnPc2	správce
Xchatu	Xchat	k1gInSc2	Xchat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2003	[number]	k4	2003
až	až	k9	až
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Českého	český	k2eAgInSc2d1	český
červeného	červený	k2eAgInSc2d1	červený
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vedl	vést	k5eAaImAgInS	vést
samostatný	samostatný	k2eAgInSc1d1	samostatný
projekt	projekt	k1gInSc1	projekt
s	s	k7c7	s
názvem	název	k1gInSc7	název
HelpTrans	HelpTrans	k1gInSc1	HelpTrans
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
aktivně	aktivně	k6eAd1	aktivně
pracoval	pracovat	k5eAaImAgInS	pracovat
s	s	k7c7	s
mentálně	mentálně	k6eAd1	mentálně
a	a	k8xC	a
zdravotně	zdravotně	k6eAd1	zdravotně
postiženými	postižený	k2eAgMnPc7d1	postižený
spoluobčany	spoluobčan	k1gMnPc7	spoluobčan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
již	již	k6eAd1	již
zaniklého	zaniklý	k2eAgNnSc2d1	zaniklé
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Cech	cech	k1gInSc1	cech
života	život	k1gInSc2	život
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
převážně	převážně	k6eAd1	převážně
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mladými	mladý	k2eAgMnPc7d1	mladý
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
vytváření	vytváření	k1gNnSc3	vytváření
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgNnSc4d1	literární
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vydal	vydat	k5eAaPmAgMnS	vydat
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
knihu	kniha	k1gFnSc4	kniha
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
C	C	kA	C
B.	B.	kA	B.
M.	M.	kA	M.
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vita	vit	k2eAgFnSc1d1	Vita
non	non	k?	non
verba	verbum	k1gNnSc2	verbum
–	–	k?	–
Život	život	k1gInSc1	život
ne	ne	k9	ne
slova	slovo	k1gNnSc2	slovo
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
9788025405222	[number]	k4	9788025405222
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
sbírkou	sbírka	k1gFnSc7	sbírka
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
nenávisti	nenávist	k1gFnSc6	nenávist
a	a	k8xC	a
smutku	smutek	k1gInSc6	smutek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
článků	článek	k1gInPc2	článek
v	v	k7c6	v
odborném	odborný	k2eAgInSc6d1	odborný
časopise	časopis	k1gInSc6	časopis
Konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
elektronika	elektronika	k1gFnSc1	elektronika
A	a	k9	a
-	-	kIx~	-
Radio	radio	k1gNnSc1	radio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
kandidát	kandidát	k1gMnSc1	kandidát
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
zelených	zelená	k1gFnPc2	zelená
do	do	k7c2	do
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
432	[number]	k4	432
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
3,51	[number]	k4	3,51
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
však	však	k9	však
nezískala	získat	k5eNaPmAgFnS	získat
potřebný	potřebný	k2eAgInSc4d1	potřebný
počet	počet	k1gInSc4	počet
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
České	český	k2eAgFnSc2d1	Česká
pirátské	pirátský	k2eAgFnSc2d1	pirátská
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
místního	místní	k2eAgNnSc2d1	místní
sdružení	sdružení	k1gNnSc2	sdružení
Piráti	pirát	k1gMnPc1	pirát
Písecko	Písecko	k1gNnSc4	Písecko
jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
post	post	k1gInSc4	post
zastupitele	zastupitel	k1gMnSc2	zastupitel
města	město	k1gNnSc2	město
Písek	Písek	k1gInSc1	Písek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dostal	dostat	k5eAaPmAgInS	dostat
1016	[number]	k4	1016
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
8,48	[number]	k4	8,48
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
zastupitele	zastupitel	k1gMnSc2	zastupitel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
na	na	k7c6	na
páté	pátá	k1gFnSc6	pátá
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgInS	získat
439	[number]	k4	439
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
6,87	[number]	k4	6,87
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
na	na	k7c6	na
kandidátní	kandidátní	k2eAgFnSc6d1	kandidátní
listině	listina	k1gFnSc6	listina
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
pirátská	pirátský	k2eAgFnSc1d1	pirátská
strana	strana	k1gFnSc1	strana
však	však	k9	však
nezískala	získat	k5eNaPmAgFnS	získat
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
hranici	hranice	k1gFnSc4	hranice
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
zastupitel	zastupitel	k1gMnSc1	zastupitel
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
kontrolního	kontrolní	k2eAgInSc2d1	kontrolní
výboru	výbor	k1gInSc2	výbor
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Centra	centrum	k1gNnSc2	centrum
Kultury	kultura	k1gFnSc2	kultura
až	až	k9	až
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
transformace	transformace	k1gFnSc2	transformace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgInS	zvolit
za	za	k7c2	za
lídra	lídr	k1gMnSc2	lídr
do	do	k7c2	do
komunálních	komunální	k2eAgFnPc2d1	komunální
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získal	získat	k5eAaPmAgInS	získat
2367	[number]	k4	2367
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
6,69	[number]	k4	6,69
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
zastupitele	zastupitel	k1gMnSc2	zastupitel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikatelské	podnikatelský	k2eAgNnSc4d1	podnikatelské
působení	působení	k1gNnSc4	působení
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
začal	začít	k5eAaPmAgInS	začít
podnikat	podnikat	k5eAaImF	podnikat
jako	jako	k8xS	jako
OSVČ	OSVČ	kA	OSVČ
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
získal	získat	k5eAaPmAgMnS	získat
ocenění	ocenění	k1gNnSc1	ocenění
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
"	"	kIx"	"
<g/>
Živnostník	živnostník	k1gMnSc1	živnostník
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vyhlašované	vyhlašovaný	k2eAgNnSc1d1	vyhlašované
Jihočeskou	jihočeský	k2eAgFnSc7d1	Jihočeská
hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
komorou	komora	k1gFnSc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
letech	let	k1gInPc6	let
rozšiřoval	rozšiřovat	k5eAaImAgMnS	rozšiřovat
působnost	působnost	k1gFnSc4	působnost
své	svůj	k3xOyFgFnSc2	svůj
firmy	firma	k1gFnSc2	firma
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
obory	obor	k1gInPc4	obor
podnikání	podnikání	k1gNnSc2	podnikání
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gFnSc1	jeho
firma	firma	k1gFnSc1	firma
další	další	k2eAgNnSc4d1	další
ocenění	ocenění	k1gNnSc4	ocenění
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
"	"	kIx"	"
<g/>
Era	Era	k1gFnSc1	Era
Živnostník	živnostník	k1gMnSc1	živnostník
roku	rok	k1gInSc2	rok
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
2012	[number]	k4	2012
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
soutěži	soutěž	k1gFnSc6	soutěž
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
ze	z	k7c2	z
441	[number]	k4	441
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
C	C	kA	C
B.	B.	kA	B.
M.	M.	kA	M.
</s>
</p>
