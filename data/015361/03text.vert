<s>
Arménská	arménský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
vlajkaPoměr	vlajkaPoměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Arménie	Arménie	k1gFnSc2
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
tří	tři	k4xCgInPc2
stejně	stejně	k6eAd1
širokých	široký	k2eAgInPc2d1
vodorovných	vodorovný	k2eAgInPc2d1
pruhů	pruh	k1gInPc2
<g/>
:	:	kIx,
červeného	červený	k2eAgMnSc2d1
<g/>
,	,	kIx,
modrého	modrý	k2eAgInSc2d1
a	a	k8xC
oranžového	oranžový	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobu	podoba	k1gFnSc4
vlajky	vlajka	k1gFnSc2
přijala	přijmout	k5eAaPmAgFnS
Nejvyšší	vysoký	k2eAgFnSc1d3
rada	rada	k1gFnSc1
Arménie	Arménie	k1gFnSc2
v	v	k7c6
zákoně	zákon	k1gInSc6
O	o	k7c6
státní	státní	k2eAgFnSc6d1
vlajce	vlajka	k1gFnSc6
Arménské	arménský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
z	z	k7c2
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1990	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
barvy	barva	k1gFnSc2
jsou	být	k5eAaImIp3nP
identické	identický	k2eAgFnPc4d1
s	s	k7c7
arménskou	arménský	k2eAgFnSc7d1
vlajkou	vlajka	k1gFnSc7
užívanou	užívaný	k2eAgFnSc7d1
v	v	k7c6
letech	let	k1gInPc6
1918	#num#	k4
<g/>
–	–	k?
<g/>
1920	#num#	k4
<g/>
,	,	kIx,
liší	lišit	k5eAaImIp3nP
se	se	k3xPyFc4
pouze	pouze	k6eAd1
poměrem	poměr	k1gInSc7
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
tehdy	tehdy	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Červená	červený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
krev	krev	k1gFnSc4
Arménů	Armén	k1gMnPc2
padlých	padlý	k1gMnPc2
v	v	k7c6
boji	boj	k1gInSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
křesťanství	křesťanství	k1gNnSc4
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
je	být	k5eAaImIp3nS
barvou	barva	k1gFnSc7
nebe	nebe	k1gNnSc2
a	a	k8xC
oranžová	oranžový	k2eAgFnSc1d1
symbolizuje	symbolizovat	k5eAaImIp3nS
přírodní	přírodní	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
a	a	k8xC
blahobyt	blahobyt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
i	i	k9
mnoho	mnoho	k4c1
jiných	jiný	k2eAgFnPc2d1
interpretací	interpretace	k1gFnPc2
významu	význam	k1gInSc2
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
však	však	k9
nemá	mít	k5eNaImIp3nS
reálný	reálný	k2eAgInSc4d1
základ	základ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
vlajka	vlajka	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
jako	jako	k8xC,k8xS
národní	národní	k2eAgInSc1d1
symbol	symbol	k1gInSc1
během	během	k7c2
krátkého	krátký	k2eAgNnSc2d1
období	období	k1gNnSc2
nezávislosti	nezávislost	k1gFnSc2
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
Arménské	arménský	k2eAgFnSc2d1
SSR	SSR	kA
byla	být	k5eAaImAgFnS
vlajka	vlajka	k1gFnSc1
nahrazena	nahradit	k5eAaPmNgFnS
vlajkou	vlajka	k1gFnSc7
standardního	standardní	k2eAgInSc2d1
sovětského	sovětský	k2eAgInSc2d1
vzoru	vzor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trikolóra	trikolóra	k1gFnSc1
byla	být	k5eAaImAgFnS
znovu	znovu	k6eAd1
navrácena	navrátit	k5eAaPmNgFnS
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1922	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
2	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Zakavkazské	zakavkazský	k2eAgFnSc2d1
SFSR	SFSR	kA
(	(	kIx(
<g/>
1922	#num#	k4
<g/>
–	–	k?
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
Arménské	arménský	k2eAgFnSc2d1
SSR	SSR	kA
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
<g/>
Poměr	poměr	k1gInSc1
stran	strana	k1gFnPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Arménská	arménský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Flags	Flags	k1gInSc4
of	of	k?
the	the	k?
World	World	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
Arménie	Arménie	k1gFnSc2
</s>
<s>
Arménská	arménský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Dějiny	dějiny	k1gFnPc1
Arménie	Arménie	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Arménská	arménský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Historie	historie	k1gFnSc1
arménské	arménský	k2eAgFnSc2d1
vlajky	vlajka	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vlajky	vlajka	k1gFnSc2
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Alandy	Aland	k1gInPc1
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Grónsko	Grónsko	k1gNnSc4
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Ostrov	ostrov	k1gInSc1
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Republika	republika	k1gFnSc1
srbská	srbský	k2eAgFnSc1d1
(	(	kIx(
<g/>
BIH	BIH	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc7
bez	bez	k7c2
plného	plný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcach	k1gInSc1
•	•	k?
Doněcká	doněcký	k2eAgFnSc1d1
lid	lid	k1gInSc1
<g/>
.	.	kIx.
rep	rep	k?
<g/>
.	.	kIx.
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Luhanská	Luhanský	k2eAgFnSc1d1
lid	lid	k1gInSc1
<g/>
.	.	kIx.
rep	rep	k?
<g/>
.	.	kIx.
•	•	k?
Podněstří	Podněstří	k1gFnSc2
•	•	k?
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
Vlajky	vlajka	k1gFnPc1
asijských	asijský	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Bhútán	Bhútán	k1gInSc1
•	•	k?
Brunej	Brunej	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Izrael	Izrael	k1gInSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc4
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc1
•	•	k?
Kambodža	Kambodža	k1gFnSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Korea	Korea	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Laos	Laos	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc2
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Mongolsko	Mongolsko	k1gNnSc1
•	•	k?
Myanmar	Myanmar	k1gMnSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Sýrie	Sýrie	k1gFnSc2
•	•	k?
Srí	Srí	k1gFnSc2
Lanka	lanko	k1gNnSc2
•	•	k?
Východní	východní	k2eAgFnSc1d1
Timor	Timor	k1gInSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc4
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Britské	britský	k2eAgNnSc1d1
indickooceánské	indickooceánský	k2eAgNnSc1d1
území	území	k1gNnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Hongkong	Hongkong	k1gInSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
•	•	k?
Kokosové	kokosový	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
•	•	k?
Macao	Macao	k1gNnSc4
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
•	•	k?
Tibet	Tibet	k1gInSc1
(	(	kIx(
<g/>
CHN	CHN	kA
<g/>
)	)	kIx)
•	•	k?
Vánoční	vánoční	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
AUS	AUS	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc7
bez	bez	k7c2
plného	plný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcacha	k1gFnPc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
