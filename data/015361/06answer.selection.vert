<s>
Červená	červený	k2eAgFnSc1d1
barva	barva	k1gFnSc1
symbolizuje	symbolizovat	k5eAaImIp3nS
krev	krev	k1gFnSc4
Arménů	Armén	k1gMnPc2
padlých	padlý	k1gMnPc2
v	v	k7c6
boji	boj	k1gInSc6
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
křesťanství	křesťanství	k1gNnSc4
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
barva	barva	k1gFnSc1
je	být	k5eAaImIp3nS
barvou	barva	k1gFnSc7
nebe	nebe	k1gNnSc2
a	a	k8xC
oranžová	oranžový	k2eAgFnSc1d1
symbolizuje	symbolizovat	k5eAaImIp3nS
přírodní	přírodní	k2eAgNnSc4d1
bohatství	bohatství	k1gNnSc4
a	a	k8xC
blahobyt	blahobyt	k1gInSc4
<g/>
.	.	kIx.
</s>