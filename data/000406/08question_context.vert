<s>
Carl	Carl	k1gMnSc1	Carl
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Scheele	Scheel	k1gInSc2	Scheel
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1742	[number]	k4	1742
Stralsund	Stralsunda	k1gFnPc2	Stralsunda
-	-	kIx~	-
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1786	[number]	k4	1786
Köping	Köping	k1gInSc1	Köping
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švédský	švédský	k2eAgMnSc1d1	švédský
chemik	chemik	k1gMnSc1	chemik
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
mnoha	mnoho	k4c2	mnoho
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
díky	díky	k7c3	díky
objevu	objev	k1gInSc3	objev
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
wolframu	wolfram	k1gInSc2	wolfram
<g/>
.	.	kIx.	.
</s>
