<s>
Oslava	oslava	k1gFnSc1	oslava
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
a	a	k8xC	a
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
levostranný	levostranný	k2eAgMnSc1d1	levostranný
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
největší	veliký	k2eAgInSc1d3	veliký
přítok	přítok	k1gInSc1	přítok
řeky	řeka	k1gFnSc2	řeka
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
toku	tok	k1gInSc2	tok
činí	činit	k5eAaImIp3nS	činit
99,6	[number]	k4	99,6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
povodí	povodí	k1gNnSc2	povodí
měří	měřit	k5eAaImIp3nS	měřit
867,2	[number]	k4	867,2
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
v	v	k7c6	v
bažinách	bažina	k1gFnPc6	bažina
okolo	okolo	k7c2	okolo
Matějovského	Matějovského	k2eAgInSc2d1	Matějovského
rybníka	rybník	k1gInSc2	rybník
a	a	k8xC	a
Babína	Babín	k1gInSc2	Babín
poblíž	poblíž	k7c2	poblíž
Nového	Nového	k2eAgNnSc2d1	Nového
Veselí	veselí	k1gNnSc2	veselí
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
cípu	cíp	k1gInSc6	cíp
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Žďárské	Žďárské	k2eAgInPc4d1	Žďárské
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
staršího	starý	k2eAgInSc2d2	starší
císařského	císařský	k2eAgInSc2d1	císařský
katastru	katastr	k1gInSc2	katastr
má	mít	k5eAaImIp3nS	mít
řeka	řeka	k1gFnSc1	řeka
vznikat	vznikat	k5eAaImF	vznikat
vléváním	vlévání	k1gNnSc7	vlévání
dvou	dva	k4xCgFnPc2	dva
menších	malý	k2eAgFnPc2d2	menší
říček	říčka	k1gFnPc2	říčka
Balinky	Balinka	k1gFnSc2	Balinka
a	a	k8xC	a
Radostínky	Radostínka	k1gFnSc2	Radostínka
u	u	k7c2	u
mostu	most	k1gInSc2	most
ve	v	k7c6	v
Velkém	velký	k2eAgNnSc6d1	velké
Meziříčí	Meziříčí	k1gNnSc6	Meziříčí
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Jihlavy	Jihlava	k1gFnSc2	Jihlava
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
zleva	zleva	k6eAd1	zleva
v	v	k7c6	v
Ivančicích	Ivančice	k1gFnPc6	Ivančice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Údolí	údolí	k1gNnSc2	údolí
Oslavy	oslava	k1gFnSc2	oslava
a	a	k8xC	a
Chvojnice	Chvojnice	k1gFnSc1	Chvojnice
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
budovat	budovat	k5eAaImF	budovat
velká	velký	k2eAgFnSc1d1	velká
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Velkého	velký	k2eAgNnSc2d1	velké
Meziříčí	Meziříčí	k1gNnSc2	Meziříčí
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
údolí	údolí	k1gNnPc4	údolí
řeky	řeka	k1gFnSc2	řeka
dálnice	dálnice	k1gFnSc2	dálnice
D1	D1	k1gFnSc2	D1
po	po	k7c6	po
mostu	most	k1gInSc2	most
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Bohdalovský	Bohdalovský	k2eAgInSc1d1	Bohdalovský
potok	potok	k1gInSc1	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
83,0	[number]	k4	83,0
Znětínský	Znětínský	k2eAgInSc4d1	Znětínský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
78,0	[number]	k4	78,0
Mastník	Mastník	k1gInSc4	Mastník
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
63,4	[number]	k4	63,4
Balinka	Balinka	k1gFnSc1	Balinka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
60,3	[number]	k4	60,3
Oslavička	Oslavička	k1gFnSc1	Oslavička
(	(	kIx(	(
<g/>
potok	potok	k1gInSc1	potok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
<g />
.	.	kIx.	.
</s>
<s>
km	km	kA	km
58,1	[number]	k4	58,1
Kundelovský	Kundelovský	k2eAgInSc4d1	Kundelovský
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
50,5	[number]	k4	50,5
Polomina	Polomin	k2eAgInSc2d1	Polomin
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
47,3	[number]	k4	47,3
Jasinka	Jasinka	k1gFnSc1	Jasinka
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
39,4	[number]	k4	39,4
Okarecký	Okarecký	k2eAgInSc4d1	Okarecký
potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
zprava	zprava	k6eAd1	zprava
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
32,6	[number]	k4	32,6
Chvojnice	Chvojnice	k1gFnSc2	Chvojnice
<g/>
,	,	kIx,	,
zleva	zleva	k6eAd1	zleva
<g/>
,	,	kIx,	,
ř.	ř.	k?	ř.
km	km	kA	km
17,6	[number]	k4	17,6
Nejvýznamnějšími	významný	k2eAgInPc7d3	nejvýznamnější
přítoky	přítok	k1gInPc7	přítok
Oslavy	oslava	k1gFnSc2	oslava
jsou	být	k5eAaImIp3nP	být
Balinka	Balinka	k1gFnSc1	Balinka
a	a	k8xC	a
Chvojnice	Chvojnice	k1gFnSc1	Chvojnice
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
Oslavanech	Oslavany	k1gInPc6	Oslavany
na	na	k7c6	na
3,30	[number]	k4	3,30
říčním	říční	k2eAgInSc6d1	říční
kilometru	kilometr	k1gInSc6	kilometr
činí	činit	k5eAaImIp3nS	činit
3,47	[number]	k4	3,47
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
Hlásné	hlásný	k2eAgInPc1d1	hlásný
profily	profil	k1gInPc1	profil
<g/>
:	:	kIx,	:
Nové	Nové	k2eAgNnSc1d1	Nové
Veselí	veselí	k1gNnSc1	veselí
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
<g/>
,	,	kIx,	,
Radostín	Radostín	k1gInSc1	Radostín
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
<g/>
,	,	kIx,	,
Velké	velký	k2eAgNnSc1d1	velké
Meziříčí	Meziříčí	k1gNnSc1	Meziříčí
<g/>
,	,	kIx,	,
Náměšť	Náměšť	k1gFnSc1	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
<g/>
,	,	kIx,	,
Oslavany	Oslavany	k1gInPc1	Oslavany
<g/>
,	,	kIx,	,
Ivančice	Ivančice	k1gFnPc1	Ivančice
Turisticky	turisticky	k6eAd1	turisticky
nejnavštěvovanější	navštěvovaný	k2eAgNnPc1d3	nejnavštěvovanější
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
pramenů	pramen	k1gInPc2	pramen
Oslavy	oslava	k1gFnSc2	oslava
-	-	kIx~	-
Matějovského	Matějovského	k2eAgInSc2d1	Matějovského
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
Babína	Babín	k1gInSc2	Babín
a	a	k8xC	a
Veselského	Veselského	k2eAgInSc2d1	Veselského
rybníka	rybník	k1gInSc2	rybník
poblíž	poblíž	k7c2	poblíž
Nového	Nového	k2eAgNnSc2d1	Nového
Veselí	veselí	k1gNnSc2	veselí
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
hluboce	hluboko	k6eAd1	hluboko
zaříznuté	zaříznutý	k2eAgNnSc1d1	zaříznuté
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
kaňonovité	kaňonovitý	k2eAgFnPc1d1	kaňonovitá
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
mezi	mezi	k7c7	mezi
Náměští	Náměšť	k1gFnSc7	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
a	a	k8xC	a
Oslavany	Oslavany	k1gInPc7	Oslavany
<g/>
.	.	kIx.	.
</s>
<s>
Skalnaté	skalnatý	k2eAgNnSc1d1	skalnaté
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zaříznuté	zaříznutý	k2eAgNnSc1d1	zaříznuté
do	do	k7c2	do
okolní	okolní	k2eAgFnSc2d1	okolní
krajiny	krajina	k1gFnSc2	krajina
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
jako	jako	k8xS	jako
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Údolí	údolí	k1gNnSc2	údolí
Oslavy	oslava	k1gFnSc2	oslava
a	a	k8xC	a
Chvojnice	Chvojnice	k1gFnSc2	Chvojnice
<g/>
,	,	kIx,	,
plošně	plošně	k6eAd1	plošně
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Náměšti	Náměšť	k1gFnSc6	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
významný	významný	k2eAgInSc1d1	významný
renesanční	renesanční	k2eAgInSc1d1	renesanční
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
v	v	k7c6	v
kaňonu	kaňon	k1gInSc6	kaňon
po	po	k7c6	po
proudu	proud	k1gInSc6	proud
řeky	řeka	k1gFnSc2	řeka
pak	pak	k6eAd1	pak
zříceniny	zřícenina	k1gFnPc1	zřícenina
hradů	hrad	k1gInPc2	hrad
Lamberka	Lamberka	k1gFnSc1	Lamberka
<g/>
,	,	kIx,	,
Sedleckého	sedlecký	k2eAgInSc2d1	sedlecký
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
Levnova	Levnov	k1gInSc2	Levnov
a	a	k8xC	a
Kraví	kraví	k2eAgFnSc2d1	kraví
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
Oslavy	oslava	k1gFnSc2	oslava
je	být	k5eAaImIp3nS	být
přehrazen	přehradit	k5eAaPmNgInS	přehradit
dvěma	dva	k4xCgInPc7	dva
velkými	velký	k2eAgInPc7d1	velký
rybníky	rybník	k1gInPc7	rybník
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
přehradní	přehradní	k2eAgFnSc7d1	přehradní
nádrží	nádrž	k1gFnSc7	nádrž
<g/>
:	:	kIx,	:
Matějovský	Matějovský	k1gMnSc1	Matějovský
rybník	rybník	k1gInSc4	rybník
Veselský	veselský	k2eAgInSc4d1	veselský
rybník	rybník	k1gInSc4	rybník
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Mostiště	Mostiště	k1gNnSc2	Mostiště
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
je	být	k5eAaImIp3nS	být
také	také	k9	také
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Čučice	Čučice	k1gFnSc1	Čučice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
zaplavit	zaplavit	k5eAaPmF	zaplavit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
údolí	údolí	k1gNnSc2	údolí
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
toku	tok	k1gInSc6	tok
Oslavy	oslava	k1gFnSc2	oslava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
petice	petice	k1gFnSc1	petice
proti	proti	k7c3	proti
postavení	postavení	k1gNnSc3	postavení
této	tento	k3xDgFnSc2	tento
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
,	,	kIx,	,
získáno	získán	k2eAgNnSc1d1	získáno
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
22	[number]	k4	22
tisíc	tisíc	k4xCgInPc2	tisíc
podpisů	podpis	k1gInPc2	podpis
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
budou	být	k5eAaImBp3nP	být
následně	následně	k6eAd1	následně
předány	předat	k5eAaPmNgInP	předat
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
technicko-ekonomická	technickokonomický	k2eAgFnSc1d1	technicko-ekonomická
studie	studie	k1gFnSc1	studie
k	k	k7c3	k
plánovnané	plánovnaný	k2eAgFnSc3d1	plánovnaný
přehradě	přehrada	k1gFnSc3	přehrada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doporučila	doporučit	k5eAaPmAgFnS	doporučit
zanechat	zanechat	k5eAaPmF	zanechat
plánovanou	plánovaný	k2eAgFnSc4d1	plánovaná
stavbu	stavba	k1gFnSc4	stavba
v	v	k7c6	v
Generelu	generel	k1gInSc6	generel
lokalit	lokalita	k1gFnPc2	lokalita
pro	pro	k7c4	pro
akumulaci	akumulace	k1gFnSc4	akumulace
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
uvažovaný	uvažovaný	k2eAgInSc1d1	uvažovaný
zábor	zábor	k1gInSc1	zábor
území	území	k1gNnSc2	území
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
na	na	k7c6	na
442,97	[number]	k4	442,97
hektaru	hektar	k1gInSc6	hektar
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
při	při	k7c6	při
zatopení	zatopení	k1gNnSc6	zatopení
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zničeno	zničit	k5eAaPmNgNnS	zničit
249	[number]	k4	249
objektů	objekt	k1gInPc2	objekt
-	-	kIx~	-
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
242	[number]	k4	242
pro	pro	k7c4	pro
rekreaci	rekreace	k1gFnSc4	rekreace
<g/>
,	,	kIx,	,
6	[number]	k4	6
objektů	objekt	k1gInPc2	objekt
k	k	k7c3	k
bydlení	bydlení	k1gNnSc3	bydlení
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
objekt	objekt	k1gInSc1	objekt
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
také	také	k9	také
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
střet	střet	k1gInSc4	střet
s	s	k7c7	s
ochranáři	ochranář	k1gMnPc7	ochranář
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodem	důvod	k1gInSc7	důvod
stavby	stavba	k1gFnSc2	stavba
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgNnSc1d1	vodní
zásobování	zásobování	k1gNnSc1	zásobování
oblasti	oblast	k1gFnSc2	oblast
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
časového	časový	k2eAgInSc2d1	časový
harmonogramu	harmonogram	k1gInSc2	harmonogram
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
přehrada	přehrada	k1gFnSc1	přehrada
napuštěna	napuštěn	k2eAgFnSc1d1	napuštěna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2034	[number]	k4	2034
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
Povodím	povodí	k1gNnSc7	povodí
Moravy	Morava	k1gFnSc2	Morava
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškeré	veškerý	k3xTgFnPc4	veškerý
plánovací	plánovací	k2eAgFnPc4d1	plánovací
práce	práce	k1gFnPc4	práce
ohledně	ohledně	k7c2	ohledně
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Čučice	Čučice	k1gFnSc2	Čučice
jsou	být	k5eAaImIp3nP	být
zastaveny	zastaven	k2eAgFnPc1d1	zastavena
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
se	se	k3xPyFc4	se
společnost	společnost	k1gFnSc1	společnost
nebude	být	k5eNaImBp3nS	být
zabývat	zabývat	k5eAaImF	zabývat
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
řeku	řeka	k1gFnSc4	řeka
vodácky	vodácky	k6eAd1	vodácky
využitelnou	využitelný	k2eAgFnSc4d1	využitelná
<g/>
.	.	kIx.	.
</s>
<s>
Nejhezčí	hezký	k2eAgInSc1d3	nejhezčí
úsek	úsek	k1gInSc1	úsek
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
Náměští	Náměšť	k1gFnSc7	Náměšť
nad	nad	k7c7	nad
Oslavou	oslava	k1gFnSc7	oslava
a	a	k8xC	a
Kuroslepy	Kuroslep	k1gMnPc7	Kuroslep
<g/>
,	,	kIx,	,
řeka	řeka	k1gFnSc1	řeka
je	být	k5eAaImIp3nS	být
sjízdná	sjízdný	k2eAgFnSc1d1	sjízdná
jen	jen	k9	jen
za	za	k7c2	za
zvýšeného	zvýšený	k2eAgInSc2d1	zvýšený
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
úsek	úsek	k1gInSc1	úsek
je	být	k5eAaImIp3nS	být
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
zkušené	zkušený	k2eAgMnPc4d1	zkušený
vodáky	vodák	k1gMnPc4	vodák
<g/>
.	.	kIx.	.
</s>
