<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
asi	asi	k9	asi
350	[number]	k4	350
000	[number]	k4	000
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
mechorostů	mechorost	k1gInPc2	mechorost
a	a	k8xC	a
kapraďorostů	kapraďorost	k1gInPc2	kapraďorost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
