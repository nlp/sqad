<s>
Bordetelóza	Bordetelóza	k1gFnSc1	Bordetelóza
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
bakteriální	bakteriální	k2eAgNnSc1d1	bakteriální
onemocnění	onemocnění	k1gNnSc1	onemocnění
horního	horní	k2eAgInSc2d1	horní
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
ptáků	pták	k1gMnPc2	pták
vyvolávané	vyvolávaný	k2eAgFnSc2d1	vyvolávaná
bakterií	bakterie	k1gFnSc7	bakterie
Bordetella	Bordetello	k1gNnSc2	Bordetello
avium	avium	k1gNnSc4	avium
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
představují	představovat	k5eAaImIp3nP	představovat
bordetely	bordetela	k1gFnPc1	bordetela
oportunní	oportunní	k2eAgFnPc1d1	oportunní
patogeny	patogen	k1gInPc4	patogen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
ojediněle	ojediněle	k6eAd1	ojediněle
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
primární	primární	k2eAgFnPc4d1	primární
respirační	respirační	k2eAgFnPc4d1	respirační
onemocněníc	onemocněnit	k5eAaImSgFnS	onemocněnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
jako	jako	k9	jako
sekundární	sekundární	k2eAgMnSc1d1	sekundární
a	a	k8xC	a
komplikující	komplikující	k2eAgInSc1d1	komplikující
faktor	faktor	k1gInSc1	faktor
jiných	jiný	k2eAgFnPc2d1	jiná
bakteriálních	bakteriální	k2eAgFnPc2d1	bakteriální
a	a	k8xC	a
virových	virový	k2eAgFnPc2d1	virová
infekcí	infekce	k1gFnPc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
bordetelóza	bordetelóza	k1gFnSc1	bordetelóza
zejména	zejména	k9	zejména
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
či	či	k8xC	či
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
chovem	chov	k1gInSc7	chov
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůťat	krůtě	k1gNnPc2	krůtě
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc1	onemocnění
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
chronickým	chronický	k2eAgInSc7d1	chronický
zánětem	zánět	k1gInSc7	zánět
respirační	respirační	k2eAgFnSc2d1	respirační
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
náhlým	náhlý	k2eAgInSc7d1	náhlý
výskytem	výskyt	k1gInSc7	výskyt
kýchání	kýchání	k1gNnSc2	kýchání
<g/>
,	,	kIx,	,
výpotkem	výpotek	k1gInSc7	výpotek
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
dýcháním	dýchání	k1gNnSc7	dýchání
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
submandibulárním	submandibulární	k2eAgInSc7d1	submandibulární
(	(	kIx(	(
<g/>
podčelistním	podčelistní	k2eAgInSc7d1	podčelistní
<g/>
)	)	kIx)	)
edémem	edém	k1gInSc7	edém
<g/>
,	,	kIx,	,
změnou	změna	k1gFnSc7	změna
hlasového	hlasový	k2eAgInSc2d1	hlasový
projevu	projev	k1gInSc2	projev
<g/>
,	,	kIx,	,
kolapsem	kolaps	k1gInSc7	kolaps
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
zakrslostí	zakrslost	k1gFnPc2	zakrslost
a	a	k8xC	a
predispozicí	predispozice	k1gFnPc2	predispozice
k	k	k7c3	k
jiným	jiný	k2eAgFnPc3d1	jiná
infekčním	infekční	k2eAgFnPc3d1	infekční
nemocem	nemoc	k1gFnPc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
rodu	rod	k1gInSc2	rod
Bordetella	Bordetello	k1gNnSc2	Bordetello
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
schopností	schopnost	k1gFnSc7	schopnost
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
řasinkový	řasinkový	k2eAgInSc4d1	řasinkový
epitel	epitel	k1gInSc4	epitel
a	a	k8xC	a
způsobovat	způsobovat	k5eAaImF	způsobovat
respirační	respirační	k2eAgNnPc4d1	respirační
onemocnění	onemocnění	k1gNnPc4	onemocnění
u	u	k7c2	u
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
bordetelózy	bordetelóza	k1gFnSc2	bordetelóza
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
bakterie	bakterie	k1gFnSc1	bakterie
Bordetella	Bordetella	k1gMnSc1	Bordetella
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
Bordetella	Bordetella	k6eAd1	Bordetella
avium	avium	k1gInSc1	avium
představuje	představovat	k5eAaImIp3nS	představovat
gramnegativní	gramnegativní	k2eAgFnPc4d1	gramnegativní
kokobakterie	kokobakterie	k1gFnPc4	kokobakterie
až	až	k8xS	až
tyčinky	tyčinka	k1gFnPc4	tyčinka
velikosti	velikost	k1gFnSc2	velikost
0,4	[number]	k4	0,4
<g/>
-	-	kIx~	-
<g/>
0,5	[number]	k4	0,5
x	x	k?	x
1-2	[number]	k4	1-2
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohyblivá	pohyblivý	k2eAgFnSc1d1	pohyblivá
<g/>
,	,	kIx,	,
opouzdřená	opouzdřený	k2eAgFnSc1d1	opouzdřená
a	a	k8xC	a
roste	růst	k5eAaImIp3nS	růst
striktně	striktně	k6eAd1	striktně
aerobně	aerobně	k6eAd1	aerobně
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
pevných	pevný	k2eAgFnPc6d1	pevná
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
MacConkey	MacConkea	k1gFnSc2	MacConkea
agaru	agar	k1gInSc2	agar
<g/>
,	,	kIx,	,
krevním	krevní	k2eAgMnSc6d1	krevní
a	a	k8xC	a
peptonovém	peptonový	k2eAgInSc6d1	peptonový
agaru	agar	k1gInSc6	agar
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgNnPc1d3	nejvhodnější
jsou	být	k5eAaImIp3nP	být
selektivní	selektivní	k2eAgNnPc1d1	selektivní
média	médium	k1gNnPc1	médium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
inhibují	inhibovat	k5eAaBmIp3nP	inhibovat
růst	růst	k1gInSc4	růst
jiných	jiný	k2eAgInPc2d1	jiný
bakteriálních	bakteriální	k2eAgInPc2d1	bakteriální
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
je	být	k5eAaImIp3nS	být
aerobní	aerobní	k2eAgFnSc1d1	aerobní
inkubace	inkubace	k1gFnSc1	inkubace
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
35-37	[number]	k4	35-37
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
24-48	[number]	k4	24-48
hodin	hodina	k1gFnPc2	hodina
<g/>
;	;	kIx,	;
teplota	teplota	k1gFnSc1	teplota
45	[number]	k4	45
°	°	k?	°
<g/>
C	C	kA	C
ji	on	k3xPp3gFnSc4	on
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
izolátů	izolát	k1gInPc2	izolát
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
drobných	drobný	k2eAgFnPc2d1	drobná
<g/>
,	,	kIx,	,
konvexních	konvexní	k2eAgFnPc2d1	konvexní
<g/>
,	,	kIx,	,
hladkých	hladký	k2eAgFnPc2d1	hladká
a	a	k8xC	a
lesklých	lesklý	k2eAgFnPc2d1	lesklá
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
po	po	k7c6	po
48	[number]	k4	48
<g/>
hodinové	hodinový	k2eAgFnSc6d1	hodinová
inkubaci	inkubace	k1gFnSc6	inkubace
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
průměrné	průměrný	k2eAgFnPc4d1	průměrná
velikosti	velikost	k1gFnPc4	velikost
1-2	[number]	k4	1-2
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
B.	B.	kA	B.
bronchiseptica	bronchiseptica	k1gFnSc1	bronchiseptica
i	i	k8xC	i
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
nefermentuje	fermentovat	k5eNaBmIp3nS	fermentovat
cukry	cukr	k1gInPc4	cukr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
oxidáza	oxidáza	k1gFnSc1	oxidáza
<g/>
,	,	kIx,	,
kataláza	kataláza	k1gFnSc1	kataláza
a	a	k8xC	a
citrát	citrát	k1gInSc1	citrát
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
ale	ale	k9	ale
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ureáza	ureáza	k1gFnSc1	ureáza
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
,	,	kIx,	,
nitráty	nitrát	k1gInPc4	nitrát
neredukuje	redukovat	k5eNaBmIp3nS	redukovat
a	a	k8xC	a
na	na	k7c6	na
krevním	krevní	k2eAgInSc6d1	krevní
agaru	agar	k1gInSc6	agar
nehemolyzuje	hemolyzovat	k5eNaBmIp3nS	hemolyzovat
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
avium-like	aviumik	k1gInPc1	avium-lik
bakterie	bakterie	k1gFnSc2	bakterie
nehemaglutinují	hemaglutinovat	k5eNaImIp3nP	hemaglutinovat
a	a	k8xC	a
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
médiu	médium	k1gNnSc6	médium
s	s	k7c7	s
6,5	[number]	k4	6,5
%	%	kIx~	%
NaCl	NaCl	k1gInSc1	NaCl
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Antigenní	antigenní	k2eAgFnSc1d1	antigenní
struktura	struktura	k1gFnSc1	struktura
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
a	a	k8xC	a
příbuzných	příbuzný	k1gMnPc2	příbuzný
bakterií	bakterie	k1gFnPc2	bakterie
byla	být	k5eAaImAgFnS	být
studována	studovat	k5eAaImNgFnS	studovat
pomocí	pomocí	k7c2	pomocí
precipitace	precipitace	k1gFnSc2	precipitace
v	v	k7c6	v
agaru	agar	k1gInSc6	agar
<g/>
,	,	kIx,	,
křížové	křížový	k2eAgFnSc2d1	křížová
aglutinace	aglutinace	k1gFnSc2	aglutinace
a	a	k8xC	a
imunoblotingem	imunobloting	k1gInSc7	imunobloting
<g/>
.	.	kIx.	.
</s>
<s>
Izoláty	izolát	k1gInPc1	izolát
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
zdrojů	zdroj	k1gInPc2	zdroj
jsou	být	k5eAaImIp3nP	být
antigenně	antigenně	k6eAd1	antigenně
podobné	podobný	k2eAgFnPc1d1	podobná
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
prokázána	prokázán	k2eAgFnSc1d1	prokázána
antigenní	antigenní	k2eAgFnSc1d1	antigenní
příbuznost	příbuznost	k1gFnSc1	příbuznost
s	s	k7c7	s
B.	B.	kA	B.
bronchiseptica	bronchiseptica	k6eAd1	bronchiseptica
<g/>
,	,	kIx,	,
Alcaligenes	Alcaligenes	k1gInSc4	Alcaligenes
denitrificans	denitrificansa	k1gFnPc2	denitrificansa
a	a	k8xC	a
Achromobacter	Achromobactra	k1gFnPc2	Achromobactra
xylosoxidans	xylosoxidans	k6eAd1	xylosoxidans
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
běžných	běžný	k2eAgInPc2d1	běžný
dezinfekčních	dezinfekční	k2eAgInPc2d1	dezinfekční
prostředků	prostředek	k1gInPc2	prostředek
v	v	k7c6	v
doporučovaných	doporučovaný	k2eAgFnPc6d1	doporučovaná
koncentracích	koncentrace	k1gFnPc6	koncentrace
zabíjí	zabíjet	k5eAaImIp3nS	zabíjet
bakterie	bakterie	k1gFnSc1	bakterie
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
Déle	dlouho	k6eAd2	dlouho
přežívá	přežívat	k5eAaImIp3nS	přežívat
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgFnPc1d1	nízká
vlhkosti	vlhkost	k1gFnPc1	vlhkost
a	a	k8xC	a
přirozeném	přirozený	k2eAgMnSc6d1	přirozený
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prachu	prach	k1gInSc6	prach
a	a	k8xC	a
trusu	trus	k1gInSc6	trus
z	z	k7c2	z
krůtích	krůtí	k2eAgFnPc2d1	krůtí
hal	hala	k1gFnPc2	hala
přežívá	přežívat	k5eAaImIp3nS	přežívat
25-33	[number]	k4	25-33
dní	den	k1gInPc2	den
při	při	k7c6	při
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
relativní	relativní	k2eAgFnSc2d1	relativní
vlhkosti	vlhkost	k1gFnSc2	vlhkost
32-58	[number]	k4	32-58
%	%	kIx~	%
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
při	při	k7c6	při
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
přežívá	přežívat	k5eAaImIp3nS	přežívat
maximálně	maximálně	k6eAd1	maximálně
než	než	k8xS	než
2	[number]	k4	2
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podestýlce	podestýlka	k1gFnSc6	podestýlka
přežívá	přežívat	k5eAaImIp3nS	přežívat
až	až	k9	až
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
izoláty	izolát	k1gInPc4	izolát
B.	B.	kA	B.
avium	avium	k1gInSc4	avium
existují	existovat	k5eAaImIp3nP	existovat
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
patogenitě	patogenita	k1gFnSc6	patogenita
<g/>
,	,	kIx,	,
produkci	produkce	k1gFnSc6	produkce
toxinů	toxin	k1gInPc2	toxin
<g/>
,	,	kIx,	,
adherenci	adherence	k1gFnSc6	adherence
k	k	k7c3	k
tracheální	tracheální	k2eAgFnSc3d1	tracheální
sliznici	sliznice	k1gFnSc3	sliznice
<g/>
,	,	kIx,	,
plasmidových	plasmidový	k2eAgInPc6d1	plasmidový
profilech	profil	k1gInPc6	profil
<g/>
,	,	kIx,	,
citlivosti	citlivost	k1gFnPc1	citlivost
k	k	k7c3	k
antibiotikům	antibiotikum	k1gNnPc3	antibiotikum
<g/>
,	,	kIx,	,
hemaglutinaci	hemaglutinace	k1gFnSc4	hemaglutinace
a	a	k8xC	a
morfologii	morfologie	k1gFnSc4	morfologie
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Nepatogenní	patogenní	k2eNgInPc1d1	nepatogenní
ptačí	ptačí	k2eAgInPc1d1	ptačí
izoláty	izolát	k1gInPc1	izolát
úzce	úzko	k6eAd1	úzko
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
s	s	k7c7	s
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
B.	B.	kA	B.
<g/>
avium-like	aviumike	k1gFnSc1	avium-like
<g/>
.	.	kIx.	.
</s>
<s>
Virulence	virulence	k1gFnSc1	virulence
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
je	být	k5eAaImIp3nS	být
podmíněna	podmínit	k5eAaPmNgFnS	podmínit
faktory	faktor	k1gInPc7	faktor
podílejícími	podílející	k2eAgMnPc7d1	podílející
se	se	k3xPyFc4	se
na	na	k7c6	na
adhezi	adheze	k1gFnSc6	adheze
<g/>
,	,	kIx,	,
na	na	k7c6	na
lokálním	lokální	k2eAgNnSc6d1	lokální
poškození	poškození	k1gNnSc6	poškození
sliznice	sliznice	k1gFnSc2	sliznice
a	a	k8xC	a
na	na	k7c6	na
systémovém	systémový	k2eAgInSc6d1	systémový
účinku	účinek	k1gInSc6	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Adheze	adheze	k1gFnSc1	adheze
na	na	k7c4	na
řasinkový	řasinkový	k2eAgInSc4d1	řasinkový
epitel	epitel	k1gInSc4	epitel
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
je	být	k5eAaImIp3nS	být
konstantním	konstantní	k2eAgInSc7d1	konstantní
rysem	rys	k1gInSc7	rys
bakterií	bakterie	k1gFnPc2	bakterie
Bordetella	Bordetello	k1gNnSc2	Bordetello
spp	spp	k?	spp
<g/>
.	.	kIx.	.
</s>
<s>
Adhezivní	Adhezivní	k2eAgInPc4d1	Adhezivní
faktory	faktor	k1gInPc4	faktor
u	u	k7c2	u
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
představují	představovat	k5eAaImIp3nP	představovat
hemaglutinin	hemaglutinin	k2eAgInSc1d1	hemaglutinin
a	a	k8xC	a
fimbrie	fimbrie	k1gFnSc1	fimbrie
(	(	kIx(	(
<g/>
pili	pít	k5eAaImAgMnP	pít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
morfologicky	morfologicky	k6eAd1	morfologicky
podobné	podobný	k2eAgFnPc1d1	podobná
fimbrie	fimbrie	k1gFnPc1	fimbrie
byly	být	k5eAaImAgFnP	být
také	také	k9	také
prokázány	prokázat	k5eAaPmNgFnP	prokázat
u	u	k7c2	u
adheze-defektních	adhezeefektní	k2eAgFnPc2d1	adheze-defektní
mutant	mutant	k1gMnSc1	mutant
B.	B.	kA	B.
avium-like	aviumike	k1gFnSc1	avium-like
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Hemaglutinace	hemaglutinace	k1gFnSc1	hemaglutinace
morčecích	morčecí	k2eAgInPc2d1	morčecí
erytrocytů	erytrocyt	k1gInPc2	erytrocyt
úzce	úzko	k6eAd1	úzko
koreluje	korelovat	k5eAaImIp3nS	korelovat
s	s	k7c7	s
virulencí	virulence	k1gFnSc7	virulence
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
lokální	lokální	k2eAgInPc1d1	lokální
účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
připisovány	připisovat	k5eAaImNgInP	připisovat
toxinům	toxin	k1gInPc3	toxin
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
Složkou	složka	k1gFnSc7	složka
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
je	být	k5eAaImIp3nS	být
endotoxin	endotoxin	k1gInSc1	endotoxin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
aktivitu	aktivita	k1gFnSc4	aktivita
jako	jako	k8xS	jako
lipopolysacharid	lipopolysacharid	k1gInSc4	lipopolysacharid
G-	G-	k1gFnSc2	G-
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
produkuje	produkovat	k5eAaImIp3nS	produkovat
dermonekrotický	dermonekrotický	k2eAgInSc1d1	dermonekrotický
termolabilní	termolabilní	k2eAgInSc1d1	termolabilní
toxin	toxin	k1gInSc1	toxin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
letální	letální	k2eAgMnSc1d1	letální
pro	pro	k7c4	pro
krůťata	krůtě	k1gNnPc4	krůtě
i	i	k8xC	i
intraperitoneálně	intraperitoneálně	k6eAd1	intraperitoneálně
inokulované	inokulovaný	k2eAgFnPc4d1	inokulovaný
myšky	myška	k1gFnPc4	myška
a	a	k8xC	a
který	který	k3yQgInSc1	který
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
hemoragicko-nekrotické	hemoragickoekrotický	k2eAgFnPc4d1	hemoragicko-nekrotický
změny	změna	k1gFnPc4	změna
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
krůťat	krůtě	k1gNnPc2	krůtě
a	a	k8xC	a
morčat	morče	k1gNnPc2	morče
po	po	k7c6	po
intradermální	intradermální	k2eAgFnSc6d1	intradermální
(	(	kIx(	(
<g/>
nitrokožní	nitrokožní	k2eAgFnSc6d1	nitrokožní
<g/>
)	)	kIx)	)
aplikaci	aplikace	k1gFnSc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
toxinem	toxin	k1gInSc7	toxin
produkovaným	produkovaný	k2eAgInSc7d1	produkovaný
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
je	být	k5eAaImIp3nS	být
tracheální	tracheální	k2eAgInSc1d1	tracheální
cytotoxin	cytotoxin	k1gInSc1	cytotoxin
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
identický	identický	k2eAgInSc4d1	identický
s	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
toxinem	toxin	k1gInSc7	toxin
produkovaným	produkovaný	k2eAgInSc7d1	produkovaný
B.	B.	kA	B.
pertussis	pertussis	k1gFnPc4	pertussis
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastňuje	zúčastňovat	k5eAaImIp3nS	zúčastňovat
se	se	k3xPyFc4	se
na	na	k7c6	na
lokálním	lokální	k2eAgNnSc6d1	lokální
poškození	poškození	k1gNnSc6	poškození
sliznice	sliznice	k1gFnSc2	sliznice
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
destruuje	destruovat	k5eAaImIp3nS	destruovat
řasinkový	řasinkový	k2eAgInSc4d1	řasinkový
epitel	epitel	k1gInSc4	epitel
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
pertussového	pertussový	k2eAgInSc2d1	pertussový
toxinu	toxin	k1gInSc2	toxin
a	a	k8xC	a
extracytoplasmatické	extracytoplasmatický	k2eAgFnSc2d1	extracytoplasmatický
adenylátcyklázy	adenylátcykláza	k1gFnSc2	adenylátcykláza
nebyly	být	k5eNaImAgFnP	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
lidského	lidský	k2eAgInSc2d1	lidský
patogenu	patogen	k1gInSc2	patogen
B.	B.	kA	B.
pertussis	pertussis	k1gFnPc1	pertussis
u	u	k7c2	u
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
prokázány	prokázán	k2eAgInPc1d1	prokázán
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
byl	být	k5eAaImAgInS	být
také	také	k6eAd1	také
prokázán	prokázat	k5eAaPmNgInS	prokázat
osteotoxin	osteotoxin	k1gInSc1	osteotoxin
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
poškození	poškození	k1gNnSc4	poškození
chrupavky	chrupavka	k1gFnSc2	chrupavka
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
změkčení	změkčení	k1gNnSc4	změkčení
a	a	k8xC	a
zhroucení	zhroucení	k1gNnSc4	zhroucení
tracheálních	tracheální	k2eAgInPc2d1	tracheální
prstenců	prstenec	k1gInPc2	prstenec
u	u	k7c2	u
postižených	postižený	k2eAgFnPc2d1	postižená
krůťat	krůtě	k1gNnPc2	krůtě
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgMnSc7d1	přirozený
hostitelem	hostitel	k1gMnSc7	hostitel
B.	B.	kA	B.
avium	avium	k1gInSc4	avium
jsou	být	k5eAaImIp3nP	být
krůty	krůta	k1gFnPc1	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
izolace	izolace	k1gFnSc1	izolace
bordetel	bordetela	k1gFnPc2	bordetela
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
známé	známý	k2eAgInPc1d1	známý
z	z	k7c2	z
kura	kur	k1gMnSc2	kur
domácího	domácí	k2eAgMnSc2d1	domácí
<g/>
,	,	kIx,	,
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
;	;	kIx,	;
i	i	k9	i
tyto	tento	k3xDgInPc1	tento
izoláty	izolát	k1gInPc1	izolát
jsou	být	k5eAaImIp3nP	být
patogenní	patogenní	k2eAgFnPc1d1	patogenní
pro	pro	k7c4	pro
krůty	krůta	k1gFnPc4	krůta
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
pštrosů	pštros	k1gMnPc2	pštros
byla	být	k5eAaImAgFnS	být
izolována	izolovat	k5eAaBmNgFnS	izolovat
B.	B.	kA	B.
bronchiseptica	bronchiseptica	k1gFnSc1	bronchiseptica
<g/>
.	.	kIx.	.
</s>
<s>
Bordetelóza	Bordetelóza	k1gFnSc1	Bordetelóza
u	u	k7c2	u
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
probíhá	probíhat	k5eAaImIp3nS	probíhat
obvykle	obvykle	k6eAd1	obvykle
mnohem	mnohem	k6eAd1	mnohem
mírněji	mírně	k6eAd2	mírně
<g/>
,	,	kIx,	,
než	než	k8xS	než
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
bordetelami	bordetela	k1gFnPc7	bordetela
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
papoušci	papoušek	k1gMnPc1	papoušek
a	a	k8xC	a
malí	malý	k2eAgMnPc1d1	malý
zpěvní	zpěvní	k2eAgMnPc1d1	zpěvní
ptáci	pták	k1gMnPc1	pták
<g/>
;	;	kIx,	;
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
však	však	k9	však
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
nejsou	být	k5eNaImIp3nP	být
specifické	specifický	k2eAgFnPc1d1	specifická
<g/>
.	.	kIx.	.
</s>
<s>
Morčata	morče	k1gNnPc1	morče
<g/>
,	,	kIx,	,
křečci	křeček	k1gMnPc1	křeček
a	a	k8xC	a
myši	myš	k1gFnPc1	myš
jsou	být	k5eAaImIp3nP	být
refrakterní	refrakterní	k2eAgMnPc4d1	refrakterní
(	(	kIx(	(
<g/>
nereagující	reagující	k2eNgMnPc4d1	nereagující
<g/>
)	)	kIx)	)
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
patogenními	patogenní	k2eAgInPc7d1	patogenní
kmeny	kmen	k1gInPc7	kmen
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
<g/>
.	.	kIx.	.
</s>
<s>
Bordetelóza	Bordetelóza	k1gFnSc1	Bordetelóza
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
kontagiózní	kontagiózní	k2eAgNnSc1d1	kontagiózní
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
přenášena	přenášet	k5eAaImNgFnS	přenášet
na	na	k7c4	na
vnímavá	vnímavý	k2eAgNnPc4d1	vnímavé
krůťata	krůtě	k1gNnPc4	krůtě
úzkým	úzký	k2eAgInSc7d1	úzký
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
infikovanými	infikovaný	k2eAgMnPc7d1	infikovaný
jedinci	jedinec	k1gMnPc7	jedinec
nebo	nebo	k8xC	nebo
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
podestýlkou	podestýlka	k1gFnSc7	podestýlka
či	či	k8xC	či
napájecí	napájecí	k2eAgFnSc7d1	napájecí
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
se	se	k3xPyFc4	se
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
aerogenně	aerogenně	k6eAd1	aerogenně
(	(	kIx(	(
<g/>
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
začíná	začínat	k5eAaImIp3nS	začínat
adhezí	adheze	k1gFnSc7	adheze
bakterií	bakterie	k1gFnPc2	bakterie
na	na	k7c4	na
řasinkový	řasinkový	k2eAgInSc4d1	řasinkový
epitel	epitel	k1gInSc4	epitel
sliznice	sliznice	k1gFnSc2	sliznice
pka	pka	k?	pka
a	a	k8xC	a
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
progresivní	progresivní	k2eAgFnSc7d1	progresivní
kolonizací	kolonizace	k1gFnSc7	kolonizace
horního	horní	k2eAgInSc2d1	horní
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
až	až	k9	až
do	do	k7c2	do
primárních	primární	k2eAgFnPc2d1	primární
průdušek	průduška	k1gFnPc2	průduška
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
ke	k	k7c3	k
stimulaci	stimulace	k1gFnSc3	stimulace
akutního	akutní	k2eAgInSc2d1	akutní
zánětu	zánět	k1gInSc2	zánět
a	a	k8xC	a
uvolňování	uvolňování	k1gNnSc1	uvolňování
hlenu	hlen	k1gInSc2	hlen
z	z	k7c2	z
pohárkovitých	pohárkovitý	k2eAgFnPc2d1	pohárkovitá
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
klinicky	klinicky	k6eAd1	klinicky
projevuje	projevovat	k5eAaImIp3nS	projevovat
kýcháním	kýchání	k1gNnSc7	kýchání
<g/>
,	,	kIx,	,
kašláním	kašlání	k1gNnSc7	kašlání
a	a	k8xC	a
ucpáním	ucpání	k1gNnSc7	ucpání
dutiny	dutina	k1gFnSc2	dutina
nosní	nosní	k2eAgFnSc2d1	nosní
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
buněk	buňka	k1gFnPc2	buňka
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
lumina	lumen	k1gNnSc2	lumen
průdušnice	průdušnice	k1gFnSc2	průdušnice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vykašlávána	vykašláván	k2eAgFnSc1d1	vykašláván
<g/>
.	.	kIx.	.
</s>
<s>
Způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yRgInSc7	jaký
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
chrupavčité	chrupavčitý	k2eAgInPc4d1	chrupavčitý
prstence	prstenec	k1gInPc4	prstenec
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
infikovaná	infikovaný	k2eAgFnSc1d1	infikovaná
ptáci	pták	k1gMnPc1	pták
mají	mít	k5eAaImIp3nP	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nižší	nízký	k2eAgFnSc1d2	nižší
schopnost	schopnost	k1gFnSc1	schopnost
odstranit	odstranit	k5eAaPmF	odstranit
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
z	z	k7c2	z
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
infekce	infekce	k1gFnSc2	infekce
pro	pro	k7c4	pro
jiné	jiný	k2eAgMnPc4d1	jiný
vnímavé	vnímavý	k2eAgMnPc4d1	vnímavý
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poklesu	pokles	k1gInSc6	pokles
a	a	k8xC	a
vymizení	vymizení	k1gNnSc6	vymizení
lokální	lokální	k2eAgFnSc2d1	lokální
imunity	imunita	k1gFnSc2	imunita
může	moct	k5eAaImIp3nS	moct
opět	opět	k6eAd1	opět
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
exacerbaci	exacerbace	k1gFnSc3	exacerbace
bordetelózy	bordetelóza	k1gFnSc2	bordetelóza
stimulací	stimulace	k1gFnPc2	stimulace
reziduální	reziduální	k2eAgFnSc2d1	reziduální
populace	populace	k1gFnSc2	populace
B.	B.	kA	B.
avium	avium	k1gInSc1	avium
v	v	k7c6	v
nosní	nosní	k2eAgFnSc6d1	nosní
dutině	dutina	k1gFnSc6	dutina
nebo	nebo	k8xC	nebo
sinusech	sinus	k1gInPc6	sinus
<g/>
.	.	kIx.	.
</s>
<s>
Bordetelóza	Bordetelóza	k1gFnSc1	Bordetelóza
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
častěji	často	k6eAd2	často
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
drůbeže	drůbež	k1gFnSc2	drůbež
s	s	k7c7	s
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
hygienou	hygiena	k1gFnSc7	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vnímavých	vnímavý	k2eAgNnPc2d1	vnímavé
krůťat	krůtě	k1gNnPc2	krůtě
vystavených	vystavený	k2eAgInPc2d1	vystavený
přímému	přímý	k2eAgNnSc3d1	přímé
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
infikovanými	infikovaný	k2eAgMnPc7d1	infikovaný
jedinci	jedinec	k1gMnPc7	jedinec
je	být	k5eAaImIp3nS	být
inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
7-10	[number]	k4	7-10
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Bordetelóza	Bordetelóza	k1gFnSc1	Bordetelóza
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
vysokou	vysoký	k2eAgFnSc7d1	vysoká
morbiditou	morbidita	k1gFnSc7	morbidita
a	a	k8xC	a
nízkou	nízký	k2eAgFnSc7d1	nízká
mortalitou	mortalita	k1gFnSc7	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
náhlým	náhlý	k2eAgInSc7d1	náhlý
výskytem	výskyt	k1gInSc7	výskyt
respiračních	respirační	k2eAgInPc2d1	respirační
příznaků	příznak	k1gInPc2	příznak
u	u	k7c2	u
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
týdenních	týdenní	k2eAgNnPc2d1	týdenní
krůťat	krůtě	k1gNnPc2	krůtě
<g/>
,	,	kIx,	,
s	s	k7c7	s
morbiditou	morbidita	k1gFnSc7	morbidita
dosahující	dosahující	k2eAgFnSc7d1	dosahující
během	během	k7c2	během
24-48	[number]	k4	24-48
hodin	hodina	k1gFnPc2	hodina
téměř	téměř	k6eAd1	téměř
100	[number]	k4	100
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůťat	krůtě	k1gNnPc2	krůtě
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
konjunktivitida	konjunktivitida	k1gFnSc1	konjunktivitida
<g/>
,	,	kIx,	,
kýchání	kýchání	k1gNnSc1	kýchání
<g/>
,	,	kIx,	,
kašlání	kašlání	k1gNnSc1	kašlání
a	a	k8xC	a
vlhké	vlhký	k2eAgInPc1d1	vlhký
tracheální	tracheální	k2eAgInPc1d1	tracheální
šelesty	šelest	k1gInPc1	šelest
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nozder	nozdra	k1gFnPc2	nozdra
lze	lze	k6eAd1	lze
vytlačit	vytlačit	k5eAaPmF	vytlačit
čirý	čirý	k2eAgInSc4d1	čirý
výpotek	výpotek	k1gInSc4	výpotek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zmnožuje	zmnožovat	k5eAaImIp3nS	zmnožovat
<g/>
,	,	kIx,	,
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
vazkým	vazký	k2eAgMnSc7d1	vazký
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zalepuje	zalepovat	k5eAaImIp3nS	zalepovat
a	a	k8xC	a
ucpává	ucpávat	k5eAaImIp3nS	ucpávat
nosní	nosní	k2eAgInPc4d1	nosní
otvory	otvor	k1gInPc4	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Vlhké	vlhký	k2eAgNnSc1d1	vlhké
peří	peří	k1gNnSc1	peří
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
u	u	k7c2	u
kořene	kořen	k1gInSc2	kořen
zobáku	zobák	k1gInSc2	zobák
je	být	k5eAaImIp3nS	být
inkrustováno	inkrustován	k2eAgNnSc4d1	inkrustován
prachem	prach	k1gInSc7	prach
a	a	k8xC	a
nečistotami	nečistota	k1gFnPc7	nečistota
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
submandibulární	submandibulární	k2eAgFnSc1d1	submandibulární
(	(	kIx(	(
<g/>
podčelistní	podčelistní	k2eAgFnSc1d1	podčelistní
<g/>
)	)	kIx)	)
edém	edém	k1gInSc1	edém
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
ptáci	pták	k1gMnPc1	pták
dýchají	dýchat	k5eAaImIp3nP	dýchat
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
u	u	k7c2	u
jiných	jiný	k1gMnPc2	jiný
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
hlasu	hlas	k1gInSc2	hlas
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
obturace	obturace	k1gFnSc2	obturace
nosní	nosní	k2eAgFnSc2d1	nosní
dutiny	dutina	k1gFnSc2	dutina
a	a	k8xC	a
horní	horní	k2eAgFnSc2d1	horní
části	část	k1gFnSc2	část
průdušnice	průdušnice	k1gFnSc2	průdušnice
hlenovitým	hlenovitý	k2eAgInSc7d1	hlenovitý
exsudátem	exsudát	k1gInSc7	exsudát
(	(	kIx(	(
<g/>
úhyn	úhyn	k1gInSc1	úhyn
udušením	udušení	k1gNnSc7	udušení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
druhým	druhý	k4xOgInSc7	druhý
týdnem	týden	k1gInSc7	týden
onemocnění	onemocnění	k1gNnSc2	onemocnění
lze	lze	k6eAd1	lze
u	u	k7c2	u
některých	některý	k3yIgNnPc2	některý
krůťat	krůtě	k1gNnPc2	krůtě
palpovat	palpovat	k5eAaBmF	palpovat
přes	přes	k7c4	přes
kůži	kůže	k1gFnSc4	kůže
změkčení	změkčení	k1gNnSc2	změkčení
chrupavčitých	chrupavčitý	k2eAgInPc2d1	chrupavčitý
prstenců	prstenec	k1gInPc2	prstenec
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
krůt	krůta	k1gFnPc2	krůta
infikovaných	infikovaný	k2eAgFnPc2d1	infikovaná
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
pouze	pouze	k6eAd1	pouze
suchý	suchý	k2eAgInSc1d1	suchý
kašel	kašel	k1gInSc1	kašel
<g/>
;	;	kIx,	;
morbidita	morbidita	k1gFnSc1	morbidita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
pod	pod	k7c7	pod
10	[number]	k4	10
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižený	k2eAgMnPc1d1	postižený
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
neteční	netečný	k2eAgMnPc1d1	netečný
<g/>
,	,	kIx,	,
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
se	se	k3xPyFc4	se
a	a	k8xC	a
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
snížený	snížený	k2eAgInSc4d1	snížený
příjem	příjem	k1gInSc4	příjem
krmiva	krmivo	k1gNnSc2	krmivo
i	i	k8xC	i
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
konkurentních	konkurentní	k2eAgFnPc6d1	konkurentní
infekcích	infekce	k1gFnPc6	infekce
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
E.	E.	kA	E.
coli	cole	k1gFnSc4	cole
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pneumovirem	pneumovir	k1gInSc7	pneumovir
<g/>
,	,	kIx,	,
pasteurelami	pasteurela	k1gFnPc7	pasteurela
<g/>
,	,	kIx,	,
chlamydiemi	chlamydie	k1gFnPc7	chlamydie
a	a	k8xC	a
jinými	jiný	k2eAgFnPc7d1	jiná
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mortalita	mortalita	k1gFnSc1	mortalita
krůťat	krůtě	k1gNnPc2	krůtě
může	moct	k5eAaImIp3nS	moct
značně	značně	k6eAd1	značně
zvýšit	zvýšit	k5eAaPmF	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
teplota	teplota	k1gFnSc1	teplota
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
špatná	špatný	k2eAgFnSc1d1	špatná
kvalita	kvalita	k1gFnSc1	kvalita
vzduchu	vzduch	k1gInSc2	vzduch
také	také	k9	také
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
ztráty	ztráta	k1gFnPc4	ztráta
úhynem	úhyn	k1gInSc7	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
zakrslíků	zakrslík	k1gMnPc2	zakrslík
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
nemoci	nemoc	k1gFnSc3	nemoc
postupně	postupně	k6eAd1	postupně
vymizí	vymizet	k5eAaPmIp3nP	vymizet
po	po	k7c6	po
2-4	[number]	k4	2-4
týdnech	týden	k1gInPc6	týden
průběhu	průběh	k1gInSc2	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Makroskopické	makroskopický	k2eAgFnPc1d1	makroskopická
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
omezeny	omezit	k5eAaPmNgFnP	omezit
na	na	k7c4	na
horní	horní	k2eAgInSc4d1	horní
respirační	respirační	k2eAgInSc4d1	respirační
trakt	trakt	k1gInSc4	trakt
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňovat	k5eAaImNgFnP	ovlivňovat
dobou	doba	k1gFnSc7	doba
trvání	trvání	k1gNnSc2	trvání
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
sekundárními	sekundární	k2eAgFnPc7d1	sekundární
infekcemi	infekce	k1gFnPc7	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
průdušnici	průdušnice	k1gFnSc6	průdušnice
<g/>
,	,	kIx,	,
charakterizované	charakterizovaný	k2eAgFnSc6d1	charakterizovaná
změkčením	změkčení	k1gNnSc7	změkčení
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
tvaru	tvar	k1gInSc2	tvar
chrupavčitých	chrupavčitý	k2eAgInPc2d1	chrupavčitý
prstenců	prstenec	k1gInPc2	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Bývají	bývat	k5eAaImIp3nP	bývat
dorzoventrálně	dorzoventrálně	k6eAd1	dorzoventrálně
oploštělé	oploštělý	k2eAgFnPc1d1	oploštělá
a	a	k8xC	a
v	v	k7c6	v
ojedinělých	ojedinělý	k2eAgInPc6d1	ojedinělý
případech	případ	k1gInPc6	případ
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
propadnutí	propadnutí	k1gNnSc3	propadnutí
dorzální	dorzální	k2eAgFnSc2d1	dorzální
stěny	stěna	k1gFnSc2	stěna
průdušnice	průdušnice	k1gFnSc2	průdušnice
do	do	k7c2	do
lumina	lumen	k1gNnSc2	lumen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
příčném	příčný	k2eAgInSc6d1	příčný
řezu	řez	k1gInSc6	řez
mají	mít	k5eAaImIp3nP	mít
prstence	prstenec	k1gInPc1	prstenec
zesílenou	zesílený	k2eAgFnSc4d1	zesílená
stěnu	stěna	k1gFnSc4	stěna
a	a	k8xC	a
zmenšený	zmenšený	k2eAgInSc4d1	zmenšený
průsvit	průsvit	k1gInSc4	průsvit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
infekci	infekce	k1gFnSc6	infekce
B.	B.	kA	B.
avium	avium	k1gInSc4	avium
reagují	reagovat	k5eAaBmIp3nP	reagovat
krůty	krůta	k1gFnPc1	krůta
tvorbou	tvorba	k1gFnSc7	tvorba
sérových	sérový	k2eAgFnPc2d1	sérová
aglutinačních	aglutinační	k2eAgFnPc2d1	aglutinační
protilátek	protilátka	k1gFnPc2	protilátka
během	během	k7c2	během
2	[number]	k4	2
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Krůťata	krůtě	k1gNnPc1	krůtě
od	od	k7c2	od
imunních	imunní	k2eAgFnPc2d1	imunní
nosnic	nosnice	k1gFnPc2	nosnice
mají	mít	k5eAaImIp3nP	mít
pasivní	pasivní	k2eAgFnPc1d1	pasivní
protilátky	protilátka	k1gFnPc1	protilátka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
je	být	k5eAaImIp3nS	být
chrání	chránit	k5eAaImIp3nS	chránit
před	před	k7c7	před
infekcí	infekce	k1gFnSc7	infekce
asi	asi	k9	asi
3	[number]	k4	3
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
vylíhnutí	vylíhnutí	k1gNnSc6	vylíhnutí
<g/>
.	.	kIx.	.
</s>
<s>
Diagnóza	diagnóza	k1gFnSc1	diagnóza
bordetelózy	bordetelóza	k1gFnSc2	bordetelóza
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
klinickém	klinický	k2eAgNnSc6d1	klinické
a	a	k8xC	a
pitevním	pitevní	k2eAgNnSc6d1	pitevní
vyšetření	vyšetření	k1gNnSc6	vyšetření
<g/>
,	,	kIx,	,
izolaci	izolace	k1gFnSc6	izolace
Bordetella	Bordetella	k1gFnSc1	Bordetella
avium	avium	k1gInSc4	avium
z	z	k7c2	z
respiračního	respirační	k2eAgInSc2d1	respirační
traktu	trakt	k1gInSc2	trakt
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
identifikaci	identifikace	k1gFnSc4	identifikace
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
sérologickým	sérologický	k2eAgNnSc7d1	sérologické
vyšetřením	vyšetření	k1gNnSc7	vyšetření
(	(	kIx(	(
<g/>
mikroaglutinace	mikroaglutinace	k1gFnSc2	mikroaglutinace
<g/>
,	,	kIx,	,
ELISA	ELISA	kA	ELISA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
diagnostiku	diagnostika	k1gFnSc4	diagnostika
lze	lze	k6eAd1	lze
také	také	k9	také
využít	využít	k5eAaPmF	využít
latexový	latexový	k2eAgInSc4d1	latexový
aglutinační	aglutinační	k2eAgInSc4d1	aglutinační
test	test	k1gInSc4	test
<g/>
,	,	kIx,	,
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
fluorescenci	fluorescence	k1gFnSc4	fluorescence
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
monoklonálních	monoklonální	k2eAgFnPc2d1	monoklonální
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
,	,	kIx,	,
plynovou	plynový	k2eAgFnSc4d1	plynová
chromatografii	chromatografie	k1gFnSc4	chromatografie
a	a	k8xC	a
PCR	PCR	kA	PCR
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Bordetelózu	Bordetelóza	k1gFnSc4	Bordetelóza
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k1gMnPc2	jiný
primárně	primárně	k6eAd1	primárně
či	či	k8xC	či
sekundárně	sekundárně	k6eAd1	sekundárně
vyvolaných	vyvolaný	k2eAgNnPc2d1	vyvolané
respiračních	respirační	k2eAgNnPc2d1	respirační
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Mykoplasmóza	Mykoplasmóza	k1gFnSc1	Mykoplasmóza
<g/>
,	,	kIx,	,
chlamydióza	chlamydióza	k1gFnSc1	chlamydióza
<g/>
,	,	kIx,	,
ornithobakteróza	ornithobakteróza	k1gFnSc1	ornithobakteróza
a	a	k8xC	a
respirační	respirační	k2eAgFnSc1d1	respirační
kryptosporidióza	kryptosporidióza	k1gFnSc1	kryptosporidióza
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
připomínat	připomínat	k5eAaImF	připomínat
bordetelózy	bordetelóza	k1gFnPc4	bordetelóza
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
virových	virový	k2eAgFnPc2d1	virová
infekcí	infekce	k1gFnPc2	infekce
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
Newcastleská	Newcastleský	k2eAgFnSc1d1	Newcastleská
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
adenoviry	adenovira	k1gFnPc1	adenovira
a	a	k8xC	a
chřipkové	chřipkový	k2eAgFnPc1d1	chřipková
infekce	infekce	k1gFnPc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Klinicky	klinicky	k6eAd1	klinicky
neodlišitelná	odlišitelný	k2eNgFnSc1d1	neodlišitelná
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
rhinotracheitida	rhinotracheitida	k1gFnSc1	rhinotracheitida
krůt	krůta	k1gFnPc2	krůta
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
pneumovirem	pneumovirem	k6eAd1	pneumovirem
Při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
bordetelózy	bordetelóza	k1gFnSc2	bordetelóza
byla	být	k5eAaImAgFnS	být
odzkoušena	odzkoušen	k2eAgFnSc1d1	odzkoušena
řada	řada	k1gFnSc1	řada
antimikrobiálních	antimikrobiální	k2eAgFnPc2d1	antimikrobiální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
omezit	omezit	k5eAaPmF	omezit
klinické	klinický	k2eAgInPc4d1	klinický
příznaky	příznak	k1gInPc4	příznak
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
mortalitu	mortalita	k1gFnSc4	mortalita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
léčby	léčba	k1gFnSc2	léčba
docházelo	docházet	k5eAaImAgNnS	docházet
opět	opět	k6eAd1	opět
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
toto	tento	k3xDgNnSc1	tento
krátkodobé	krátkodobý	k2eAgNnSc1d1	krátkodobé
zlepšení	zlepšení	k1gNnSc1	zlepšení
je	být	k5eAaImIp3nS	být
způsobováno	způsobovat	k5eAaImNgNnS	způsobovat
antibakteriálním	antibakteriální	k2eAgInSc7d1	antibakteriální
účinkem	účinek	k1gInSc7	účinek
na	na	k7c6	na
B.	B.	kA	B.
avium	avium	k1gInSc4	avium
nebo	nebo	k8xC	nebo
na	na	k7c4	na
sekundární	sekundární	k2eAgFnPc4d1	sekundární
oportunní	oportunní	k2eAgFnPc4d1	oportunní
infekce	infekce	k1gFnPc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
výskytu	výskyt	k1gInSc2	výskyt
bordetelózy	bordetelóza	k1gFnSc2	bordetelóza
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgNnPc4d3	nejdůležitější
zoohygienická	zoohygienický	k2eAgNnPc4d1	zoohygienický
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
kontagiózní	kontagiózní	k2eAgInSc1d1	kontagiózní
zejména	zejména	k9	zejména
při	při	k7c6	při
přímém	přímý	k2eAgInSc6d1	přímý
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
infikovanými	infikovaný	k2eAgMnPc7d1	infikovaný
jedinci	jedinec	k1gMnPc7	jedinec
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
kontaminovanou	kontaminovaný	k2eAgFnSc7d1	kontaminovaná
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
krmivem	krmivo	k1gNnSc7	krmivo
a	a	k8xC	a
podestýlkou	podestýlka	k1gFnSc7	podestýlka
<g/>
.	.	kIx.	.
</s>
<s>
Napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
kompletní	kompletní	k2eAgNnSc1d1	kompletní
odstraňování	odstraňování	k1gNnSc1	odstraňování
použité	použitý	k2eAgFnSc2d1	použitá
podestýlky	podestýlka	k1gFnSc2	podestýlka
<g/>
,	,	kIx,	,
omývání	omývání	k1gNnSc1	omývání
všech	všecek	k3xTgMnPc2	všecek
povrchů	povrch	k1gInPc2	povrch
<g/>
,	,	kIx,	,
dezinfekce	dezinfekce	k1gFnPc1	dezinfekce
napájecích	napájecí	k2eAgInPc2d1	napájecí
i	i	k8xC	i
krmných	krmný	k2eAgInPc2d1	krmný
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
dezinfekce	dezinfekce	k1gFnSc2	dezinfekce
prostředí	prostředí	k1gNnSc1	prostředí
formaldehydovými	formaldehydový	k2eAgFnPc7d1	formaldehydová
parami	para	k1gFnPc7	para
<g/>
.	.	kIx.	.
</s>
<s>
B.	B.	kA	B.
avium	avium	k1gNnSc1	avium
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
přenáší	přenášet	k5eAaImIp3nS	přenášet
různými	různý	k2eAgInPc7d1	různý
předměty	předmět	k1gInPc7	předmět
i	i	k8xC	i
ošetřovateli	ošetřovatel	k1gMnPc7	ošetřovatel
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
dezinfikovat	dezinfikovat	k5eAaBmF	dezinfikovat
obuv	obuv	k1gFnSc4	obuv
<g/>
,	,	kIx,	,
oděv	oděv	k1gInSc4	oděv
<g/>
,	,	kIx,	,
omezit	omezit	k5eAaPmF	omezit
pohyb	pohyb	k1gInSc1	pohyb
ošetřovatelů	ošetřovatel	k1gMnPc2	ošetřovatel
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
halami	hala	k1gFnPc7	hala
i	i	k8xC	i
návštěvy	návštěva	k1gFnSc2	návštěva
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
bordetelózy	bordetelóza	k1gFnSc2	bordetelóza
je	být	k5eAaImIp3nS	být
také	také	k9	také
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
řadou	řada	k1gFnSc7	řada
faktorů	faktor	k1gInPc2	faktor
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
mikroklima	mikroklima	k1gNnSc4	mikroklima
<g/>
)	)	kIx)	)
i	i	k8xC	i
sekundárními	sekundární	k2eAgNnPc7d1	sekundární
infekčními	infekční	k2eAgNnPc7d1	infekční
agens	agens	k1gNnPc7	agens
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
byly	být	k5eAaImAgInP	být
vyvinuty	vyvinout	k5eAaPmNgInP	vyvinout
a	a	k8xC	a
také	také	k9	také
jsou	být	k5eAaImIp3nP	být
komerčně	komerčně	k6eAd1	komerčně
využívány	využíván	k2eAgFnPc4d1	využívána
různé	různý	k2eAgFnPc4d1	různá
živé	živá	k1gFnPc4	živá
i	i	k8xC	i
inaktivované	inaktivovaný	k2eAgFnPc4d1	inaktivovaná
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
účinnost	účinnost	k1gFnSc1	účinnost
však	však	k9	však
není	být	k5eNaImIp3nS	být
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
další	další	k2eAgInSc1d1	další
výzkum	výzkum	k1gInSc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
JURAJDA	JURAJDA	kA	JURAJDA
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Nemoci	nemoc	k1gFnPc4	nemoc
drůbeže	drůbež	k1gFnSc2	drůbež
a	a	k8xC	a
ptactva	ptactvo	k1gNnSc2	ptactvo
-	-	kIx~	-
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
a	a	k8xC	a
mykotické	mykotický	k2eAgFnSc2d1	mykotická
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
ES	ES	kA	ES
VFU	VFU	kA	VFU
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7305	[number]	k4	7305
<g/>
-	-	kIx~	-
<g/>
464	[number]	k4	464
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
RITCHIE	RITCHIE	kA	RITCHIE
<g/>
,	,	kIx,	,
B.W.	B.W.	k1gFnSc1	B.W.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
..	..	k?	..
Avian	Avian	k1gInSc1	Avian
Medicine	Medicin	k1gInSc5	Medicin
<g/>
:	:	kIx,	:
Principles	Principles	k1gInSc1	Principles
and	and	k?	and
Application	Application	k1gInSc1	Application
<g/>
.	.	kIx.	.
</s>
<s>
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Wingers	Wingers	k1gInSc1	Wingers
Publ	Publ	k1gInSc1	Publ
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
1384	[number]	k4	1384
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
9636996	[number]	k4	9636996
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
SAIF	SAIF	kA	SAIF
<g/>
,	,	kIx,	,
Y.	Y.	kA	Y.
<g/>
M.	M.	kA	M.
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Diseases	Diseases	k1gInSc1	Diseases
of	of	k?	of
Poultry	Poultr	k1gMnPc7	Poultr
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Ames	Ames	k1gInSc1	Ames
<g/>
,	,	kIx,	,
USA	USA	kA	USA
:	:	kIx,	:
Iowa	Iowa	k1gMnSc1	Iowa
State	status	k1gInSc5	status
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Blackwell	Blackwell	k1gMnSc1	Blackwell
Publ	Publ	k1gMnSc1	Publ
<g/>
.	.	kIx.	.
</s>
<s>
Comp	Comp	k1gInSc1	Comp
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
1231	[number]	k4	1231
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8138	[number]	k4	8138
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
423	[number]	k4	423
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
