<s>
Ostrov	ostrov	k1gInSc1	ostrov
dravců	dravec	k1gMnPc2	dravec
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
obsahující	obsahující	k2eAgFnSc2d1	obsahující
společné	společný	k2eAgFnSc2d1	společná
české	český	k2eAgFnSc2d1	Česká
vydání	vydání	k1gNnSc2	vydání
dvou	dva	k4xCgInPc2	dva
vědeckofantastických	vědeckofantastický	k2eAgInPc2d1	vědeckofantastický
románů	román	k1gInPc2	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
Jiná	jiný	k2eAgFnSc1d1	jiná
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Une	Une	k1gMnSc5	Une
Autre	Autr	k1gMnSc5	Autr
Terre	Terr	k1gMnSc5	Terr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ostrov	ostrov	k1gInSc1	ostrov
dravců	dravec	k1gMnPc2	dravec
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Île	Île	k1gMnSc1	Île
aux	aux	k?	aux
Enragés	Enragés	k1gInSc1	Enragés
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Pierre	Pierr	k1gInSc5	Pierr
Pelot	pelota	k1gFnPc2	pelota
<g/>
.	.	kIx.	.
</s>
