<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
dravců	dravec	k1gMnPc2	dravec
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
obsahující	obsahující	k2eAgFnSc2d1	obsahující
společné	společný	k2eAgFnSc2d1	společná
české	český	k2eAgFnSc2d1	Česká
vydání	vydání	k1gNnSc2	vydání
dvou	dva	k4xCgInPc2	dva
vědeckofantastických	vědeckofantastický	k2eAgInPc2d1	vědeckofantastický
románů	román	k1gInPc2	román
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
Jiná	jiný	k2eAgFnSc1d1	jiná
země	země	k1gFnSc1	země
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Une	Une	k1gMnSc5	Une
Autre	Autr	k1gMnSc5	Autr
Terre	Terr	k1gMnSc5	Terr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ostrov	ostrov	k1gInSc1	ostrov
dravců	dravec	k1gMnPc2	dravec
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Île	Île	k1gMnSc1	Île
aux	aux	k?	aux
Enragés	Enragés	k1gInSc1	Enragés
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Pierre	Pierr	k1gInSc5	Pierr
Pelot	pelota	k1gFnPc2	pelota
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
společné	společný	k2eAgNnSc1d1	společné
vydání	vydání	k1gNnSc1	vydání
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Gustava	Gustav	k1gMnSc2	Gustav
Francla	Francl	k1gMnSc2	Francl
v	v	k7c4	v
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
Albatros	albatros	k1gMnSc1	albatros
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
jako	jako	k9	jako
160	[number]	k4	160
<g/>
.	.	kIx.	.
svazek	svazek	k1gInSc1	svazek
edice	edice	k1gFnSc2	edice
Knihy	kniha	k1gFnSc2	kniha
odvahy	odvaha	k1gFnSc2	odvaha
a	a	k8xC	a
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Romány	román	k1gInPc1	román
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
navazují	navazovat	k5eAaImIp3nP	navazovat
<g/>
,	,	kIx,	,
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
se	se	k3xPyFc4	se
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
32	[number]	k4	32
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
spojuje	spojovat	k5eAaImIp3nS	spojovat
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
Ariana	Ariana	k1gFnSc1	Ariana
Daye	Daye	k1gFnSc1	Daye
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
varování	varování	k1gNnSc1	varování
před	před	k7c7	před
zneužitím	zneužití	k1gNnSc7	zneužití
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
románů	román	k1gInPc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Jiná	jiný	k2eAgFnSc1d1	jiná
země	země	k1gFnSc1	země
===	===	k?	===
</s>
</p>
<p>
<s>
Román	Román	k1gMnSc1	Román
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dějové	dějový	k2eAgFnPc4d1	dějová
linie	linie	k1gFnPc4	linie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
spojí	spojit	k5eAaPmIp3nP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
seznamujeme	seznamovat	k5eAaImIp1nP	seznamovat
s	s	k7c7	s
Arianem	Arian	k1gMnSc7	Arian
Dayem	Day	k1gMnSc7	Day
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
lidí	člověk	k1gMnPc2	člověk
utlačovaných	utlačovaný	k2eAgFnPc2d1	utlačovaná
obludnou	obludný	k2eAgFnSc7d1	obludná
společností	společnost	k1gFnSc7	společnost
Říše	říš	k1gFnSc2	říš
pozemské	pozemský	k2eAgFnSc2d1	pozemská
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Té	ten	k3xDgFnSc3	ten
vládne	vládnout	k5eAaImIp3nS	vládnout
Elita	elita	k1gFnSc1	elita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
usměrňuje	usměrňovat	k5eAaImIp3nS	usměrňovat
životy	život	k1gInPc4	život
lidí	člověk	k1gMnPc2	člověk
k	k	k7c3	k
předem	předem	k6eAd1	předem
určenému	určený	k2eAgInSc3d1	určený
cíli	cíl	k1gInSc3	cíl
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
vrozené	vrozený	k2eAgFnPc4d1	vrozená
vlastnosti	vlastnost	k1gFnPc4	vlastnost
a	a	k8xC	a
zbavuje	zbavovat	k5eAaImIp3nS	zbavovat
je	on	k3xPp3gFnPc4	on
svobodné	svobodný	k2eAgFnPc4d1	svobodná
vůle	vůle	k1gFnPc4	vůle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arian	Arian	k1gInSc1	Arian
je	být	k5eAaImIp3nS	být
vybaven	vybavit	k5eAaPmNgInS	vybavit
falešnými	falešný	k2eAgInPc7d1	falešný
doklady	doklad	k1gInPc7	doklad
od	od	k7c2	od
Výboru	výbor	k1gInSc2	výbor
podzemní	podzemní	k2eAgFnSc2d1	podzemní
organizace	organizace	k1gFnSc2	organizace
Psanců	psanec	k1gMnPc2	psanec
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
pronikne	proniknout	k5eAaPmIp3nS	proniknout
do	do	k7c2	do
města	město	k1gNnSc2	město
Gardisu	Gardis	k1gInSc2	Gardis
na	na	k7c6	na
bývalém	bývalý	k2eAgInSc6d1	bývalý
kontinentu	kontinent	k1gInSc6	kontinent
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
Ústředny	ústředna	k1gFnSc2	ústředna
pro	pro	k7c4	pro
biologické	biologický	k2eAgNnSc4d1	biologické
a	a	k8xC	a
exobiologické	exobiologický	k2eAgNnSc4d1	exobiologický
vyzbrojení	vyzbrojení	k1gNnSc4	vyzbrojení
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zničit	zničit	k5eAaPmF	zničit
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
zárodků	zárodek	k1gInPc2	zárodek
budoucích	budoucí	k2eAgMnPc2d1	budoucí
vojáků	voják	k1gMnPc2	voják
určených	určený	k2eAgFnPc2d1	určená
pro	pro	k7c4	pro
galaktickou	galaktický	k2eAgFnSc4d1	Galaktická
válku	válka	k1gFnSc4	válka
s	s	k7c7	s
vysoce	vysoce	k6eAd1	vysoce
vyspělou	vyspělý	k2eAgFnSc7d1	vyspělá
humanoidní	humanoidní	k2eAgFnSc7d1	humanoidní
civilizací	civilizace	k1gFnSc7	civilizace
označující	označující	k2eAgFnSc2d1	označující
se	se	k3xPyFc4	se
jako	jako	k9	jako
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arianovi	Arian	k1gMnSc3	Arian
se	se	k3xPyFc4	se
úkol	úkol	k1gInSc4	úkol
podaří	podařit	k5eAaPmIp3nS	podařit
a	a	k8xC	a
zárodky	zárodek	k1gInPc1	zárodek
věčně	věčně	k6eAd1	věčně
mladých	mladý	k2eAgInPc2d1	mladý
lidských	lidský	k2eAgInPc2d1	lidský
robotů	robot	k1gInPc2	robot
<g/>
,	,	kIx,	,
imunních	imunní	k2eAgInPc2d1	imunní
proti	proti	k7c3	proti
radioaktivnímu	radioaktivní	k2eAgNnSc3d1	radioaktivní
záření	záření	k1gNnSc3	záření
a	a	k8xC	a
netečných	tečný	k2eNgFnPc2d1	netečná
vůči	vůči	k7c3	vůči
možnostem	možnost	k1gFnPc3	možnost
násilné	násilný	k2eAgFnSc2d1	násilná
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zajat	zajat	k2eAgMnSc1d1	zajat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
členové	člen	k1gMnPc1	člen
Elity	elita	k1gFnSc2	elita
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
stojí	stát	k5eAaImIp3nP	stát
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Psanců	psanec	k1gMnPc2	psanec
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
osvobodí	osvobodit	k5eAaPmIp3nP	osvobodit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
úkol	úkol	k1gInSc4	úkol
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mužem	muž	k1gMnSc7	muž
jménem	jméno	k1gNnSc7	jméno
Corian	Corian	k1gMnSc1	Corian
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
neznámé	známý	k2eNgFnSc3d1	neznámá
síle	síla	k1gFnSc3	síla
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
z	z	k7c2	z
paralelního	paralelní	k2eAgInSc2d1	paralelní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
vypravit	vypravit	k5eAaPmF	vypravit
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
zvané	zvaný	k2eAgFnSc2d1	zvaná
Lyra	lyra	k1gFnSc1	lyra
najít	najít	k5eAaPmF	najít
pro	pro	k7c4	pro
omezený	omezený	k2eAgInSc4d1	omezený
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
nové	nový	k2eAgNnSc1d1	nové
místo	místo	k1gNnSc1	místo
k	k	k7c3	k
životu	život	k1gInSc3	život
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
jim	on	k3xPp3gMnPc3	on
tak	tak	k6eAd1	tak
přežit	přežit	k2eAgInSc4d1	přežit
galaktickou	galaktický	k2eAgFnSc4d1	Galaktická
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
skončí	skončit	k5eAaPmIp3nS	skončit
zánikem	zánik	k1gInSc7	zánik
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
větší	veliký	k2eAgFnSc4d2	veliký
úspěšnost	úspěšnost	k1gFnSc4	úspěšnost
mise	mise	k1gFnSc2	mise
se	se	k3xPyFc4	se
Arian	Ariana	k1gFnPc2	Ariana
podrobí	podrobit	k5eAaPmIp3nS	podrobit
biochemické	biochemický	k2eAgFnSc3d1	biochemická
kůře	kůra	k1gFnSc3	kůra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
i	i	k8xC	i
psychickou	psychický	k2eAgFnSc4d1	psychická
dlouhověkost	dlouhověkost	k1gFnSc4	dlouhověkost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
přenosu	přenos	k1gInSc6	přenos
do	do	k7c2	do
paralelního	paralelní	k2eAgInSc2d1	paralelní
světa	svět	k1gInSc2	svět
sice	sice	k8xC	sice
Corianova	Corianův	k2eAgFnSc1d1	Corianův
loď	loď	k1gFnSc1	loď
s	s	k7c7	s
oběma	dva	k4xCgMnPc7	dva
muži	muž	k1gMnPc7	muž
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
nečekaně	nečekaně	k6eAd1	nečekaně
objeví	objevit	k5eAaPmIp3nS	objevit
mrtvý	mrtvý	k2eAgInSc4d1	mrtvý
Corian	Corian	k1gInSc4	Corian
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlasového	hlasový	k2eAgInSc2d1	hlasový
záznamu	záznam	k1gInSc2	záznam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
svírá	svírat	k5eAaImIp3nS	svírat
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Povstalci	povstalec	k1gMnPc1	povstalec
dozvědí	dozvědět	k5eAaPmIp3nP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arian	Arian	k1gInSc1	Arian
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
v	v	k7c6	v
paralelním	paralelní	k2eAgInSc6d1	paralelní
světě	svět	k1gInSc6	svět
a	a	k8xC	a
naživu	naživu	k6eAd1	naživu
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
cíle	cíl	k1gInSc2	cíl
své	svůj	k3xOyFgFnSc2	svůj
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Dozvědí	dozvědět	k5eAaPmIp3nP	dozvědět
se	se	k3xPyFc4	se
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
Corian	Corian	k1gInSc1	Corian
je	být	k5eAaImIp3nS	být
bytost	bytost	k1gFnSc4	bytost
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
paralelní	paralelní	k2eAgFnSc2d1	paralelní
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
galaxie	galaxie	k1gFnSc2	galaxie
z	z	k7c2	z
planety	planeta	k1gFnSc2	planeta
Ragam	Ragam	k1gInSc1	Ragam
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnPc1	její
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
Zemi	zem	k1gFnSc3	zem
kolonizovat	kolonizovat	k5eAaBmF	kolonizovat
a	a	k8xC	a
Corian	Corian	k1gMnSc1	Corian
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
pozemšťanů	pozemšťan	k1gMnPc2	pozemšťan
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
válku	válka	k1gFnSc4	válka
prohráli	prohrát	k5eAaPmAgMnP	prohrát
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
Corian	Corian	k1gMnSc1	Corian
prchnout	prchnout	k5eAaPmF	prchnout
a	a	k8xC	a
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podivné	podivný	k2eAgFnSc3d1	podivná
nehodě	nehoda	k1gFnSc3	nehoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
vrhla	vrhnout	k5eAaPmAgFnS	vrhnout
do	do	k7c2	do
paralelního	paralelní	k2eAgInSc2d1	paralelní
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
by	by	kYmCp3nP	by
Arianovi	Arianův	k2eAgMnPc1d1	Arianův
při	pře	k1gFnSc3	pře
jeho	jeho	k3xOp3gNnSc4	jeho
poslání	poslání	k1gNnSc4	poslání
jen	jen	k9	jen
škodil	škodit	k5eAaImAgMnS	škodit
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
neodletět	odletět	k5eNaPmF	odletět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ho	on	k3xPp3gMnSc4	on
stálo	stát	k5eAaImAgNnS	stát
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
dějové	dějový	k2eAgFnSc6d1	dějová
linii	linie	k1gFnSc6	linie
se	se	k3xPyFc4	se
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
země	zem	k1gFnSc2	zem
Oro	Oro	k1gFnSc2	Oro
u	u	k7c2	u
jezerních	jezerní	k2eAgMnPc2d1	jezerní
lidí	člověk	k1gMnPc2	člověk
probouzí	probouzet	k5eAaImIp3nS	probouzet
po	po	k7c6	po
osmdesáti	osmdesát	k4xCc2	osmdesát
osmi	osm	k4xCc2	osm
letech	léto	k1gNnPc6	léto
trvajícím	trvající	k2eAgMnSc6d1	trvající
spánku	spánek	k1gInSc2	spánek
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
nepamatuje	pamatovat	k5eNaImIp3nS	pamatovat
<g/>
.	.	kIx.	.
</s>
<s>
Jezerní	jezerní	k2eAgMnPc1d1	jezerní
lidé	člověk	k1gMnPc1	člověk
jej	on	k3xPp3gNnSc4	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
Kirža	Kiržus	k1gMnSc4	Kiržus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
bůh	bůh	k1gMnSc1	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Zjevil	zjevit	k5eAaPmAgInS	zjevit
se	se	k3xPyFc4	se
prý	prý	k9	prý
nečekaně	nečekaně	k6eAd1	nečekaně
ve	v	k7c4	v
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
lodi	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
psí	psí	k2eAgMnPc1d1	psí
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
s	s	k7c7	s
jezerním	jezerní	k2eAgInSc7d1	jezerní
lidem	lid	k1gInSc7	lid
bojovali	bojovat	k5eAaImAgMnP	bojovat
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
bytostí	bytost	k1gFnPc2	bytost
v	v	k7c6	v
zářících	zářící	k2eAgInPc6d1	zářící
nebeských	nebeský	k2eAgInPc6d1	nebeský
vozech	vůz	k1gInPc6	vůz
<g/>
,	,	kIx,	,
je	on	k3xPp3gNnPc4	on
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
do	do	k7c2	do
hor.	hor.	k?	hor.
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
psí	psí	k1gNnSc2	psí
lidé	člověk	k1gMnPc1	člověk
hory	hora	k1gFnSc2	hora
střeží	střežit	k5eAaImIp3nP	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Kirža	Kirža	k6eAd1	Kirža
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
jezerním	jezerní	k2eAgMnPc3d1	jezerní
lidem	člověk	k1gMnPc3	člověk
pomoci	pomoc	k1gFnSc2	pomoc
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tak	tak	k6eAd1	tak
získat	získat	k5eAaPmF	získat
svou	svůj	k3xOyFgFnSc4	svůj
loď	loď	k1gFnSc4	loď
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
pomůže	pomoct	k5eAaPmIp3nS	pomoct
vrátit	vrátit	k5eAaPmF	vrátit
paměť	paměť	k1gFnSc4	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Povede	vést	k5eAaImIp3nS	vést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
usmířit	usmířit	k5eAaPmF	usmířit
jezerní	jezerní	k2eAgMnPc4d1	jezerní
lidi	člověk	k1gMnPc4	člověk
se	s	k7c7	s
strážci	strážce	k1gMnPc7	strážce
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nejsou	být	k5eNaImIp3nP	být
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
osudem	osud	k1gInSc7	osud
spokojeni	spokojen	k2eAgMnPc1d1	spokojen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gMnPc1	jejich
páni	pan	k1gMnPc1	pan
si	se	k3xPyFc3	se
příjemně	příjemně	k6eAd1	příjemně
žijí	žít	k5eAaImIp3nP	žít
u	u	k7c2	u
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
jezerní	jezerní	k2eAgNnSc4d1	jezerní
město	město	k1gNnSc4	město
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kirža	Kirža	k1gMnSc1	Kirža
objeví	objevit	k5eAaPmIp3nS	objevit
svou	svůj	k3xOyFgFnSc4	svůj
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
záznamů	záznam	k1gInPc2	záznam
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
nalezne	naleznout	k5eAaPmIp3nS	naleznout
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Arian	Arian	k1gMnSc1	Arian
Day	Day	k1gMnSc1	Day
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
mise	mise	k1gFnSc1	mise
skončila	skončit	k5eAaPmAgFnS	skončit
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
během	během	k7c2	během
jeho	jeho	k3xOp3gInPc2	jeho
osmdesát	osmdesát	k4xCc4	osmdesát
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
trvajícího	trvající	k2eAgInSc2d1	trvající
spánku	spánek	k1gInSc2	spánek
jistě	jistě	k6eAd1	jistě
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
galaktické	galaktický	k2eAgFnSc3d1	Galaktická
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
Země	země	k1gFnSc1	země
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Legendy	legenda	k1gFnPc1	legenda
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kirža	Kirža	k1gFnSc1	Kirža
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
městě	město	k1gNnSc6	město
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgMnS	rozloučit
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Odjel	odjet	k5eAaPmAgMnS	odjet
sám	sám	k3xTgMnSc1	sám
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
velkém	velký	k2eAgInSc6d1	velký
plavém	plavý	k2eAgMnSc6d1	plavý
jezdeckém	jezdecký	k2eAgMnSc6d1	jezdecký
psu	pes	k1gMnSc6	pes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrov	ostrov	k1gInSc1	ostrov
dravců	dravec	k1gMnPc2	dravec
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
rybářské	rybářský	k2eAgFnSc6d1	rybářská
vesničce	vesnička	k1gFnSc6	vesnička
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
tajemný	tajemný	k2eAgMnSc1d1	tajemný
muž	muž	k1gMnSc1	muž
na	na	k7c6	na
jezdeckém	jezdecký	k2eAgMnSc6d1	jezdecký
psovi	pes	k1gMnSc6	pes
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
prokletý	prokletý	k2eAgInSc4d1	prokletý
ostrov	ostrov	k1gInSc4	ostrov
Černý	Černý	k1gMnSc1	Černý
Hig	Hig	k1gMnSc1	Hig
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
prý	prý	k9	prý
měli	mít	k5eAaImAgMnP	mít
základnu	základna	k1gFnSc4	základna
bozi	bůh	k1gMnPc1	bůh
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sestoupili	sestoupit	k5eAaPmAgMnP	sestoupit
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
v	v	k7c6	v
zářících	zářící	k2eAgInPc6d1	zářící
vozech	vůz	k1gInPc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Cizinec	cizinec	k1gMnSc1	cizinec
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Arian	Arian	k1gInSc1	Arian
Day	Day	k1gFnSc2	Day
<g/>
,	,	kIx,	,
přichází	přicházet	k5eAaImIp3nS	přicházet
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Oro	Oro	k1gFnSc2	Oro
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
země	zem	k1gFnSc2	zem
Lyra	lyra	k1gFnSc1	lyra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arianovi	Arian	k1gMnSc3	Arian
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
přemluvit	přemluvit	k5eAaPmF	přemluvit
jednoho	jeden	k4xCgMnSc4	jeden
rybáře	rybář	k1gMnSc4	rybář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
dovezl	dovézt	k5eAaPmAgMnS	dovézt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
to	ten	k3xDgNnSc4	ten
nejprve	nejprve	k6eAd1	nejprve
nechce	chtít	k5eNaImIp3nS	chtít
za	za	k7c4	za
žádnou	žádný	k3yNgFnSc4	žádný
cenu	cena	k1gFnSc4	cena
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
doposud	doposud	k6eAd1	doposud
nikdo	nikdo	k3yNnSc1	nikdo
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nechá	nechat	k5eAaPmIp3nS	nechat
přemluvit	přemluvit	k5eAaPmF	přemluvit
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arian	Arian	k1gMnSc1	Arian
má	mít	k5eAaImIp3nS	mít
schopnosti	schopnost	k1gFnPc4	schopnost
bohů	bůh	k1gMnPc2	bůh
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vlastní	vlastní	k2eAgFnSc4d1	vlastní
dezintegrační	dezintegrační	k2eAgFnSc4d1	dezintegrační
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dopraví	dopravit	k5eAaPmIp3nS	dopravit
Ariana	Ariana	k1gFnSc1	Ariana
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
spoustou	spousta	k1gFnSc7	spousta
vraků	vrak	k1gInPc2	vrak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Adriana	Adriana	k1gFnSc1	Adriana
napadne	napadnout	k5eAaPmIp3nS	napadnout
skupina	skupina	k1gFnSc1	skupina
nahých	nahý	k2eAgMnPc2d1	nahý
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
neuvěřitelně	uvěřitelně	k6eNd1	uvěřitelně
hustým	hustý	k2eAgInSc7d1	hustý
porostem	porost	k1gInSc7	porost
chlupů	chlup	k1gInPc2	chlup
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
rukou	ruka	k1gFnPc6	ruka
drápy	dráp	k1gInPc4	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
před	před	k7c7	před
nimi	on	k3xPp3gMnPc7	on
uprchnout	uprchnout	k5eAaPmF	uprchnout
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
příkré	příkrý	k2eAgFnSc2d1	příkrá
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
u	u	k7c2	u
konce	konec	k1gInSc2	konec
svých	svůj	k3xOyFgFnPc2	svůj
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
kdosi	kdosi	k3yInSc1	kdosi
mu	on	k3xPp3gMnSc3	on
podá	podat	k5eAaPmIp3nS	podat
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
pomůže	pomoct	k5eAaPmIp3nS	pomoct
mu	on	k3xPp3gNnSc3	on
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ragamský	ragamský	k2eAgMnSc1d1	ragamský
muž	muž	k1gMnSc1	muž
jménem	jméno	k1gNnSc7	jméno
Gel	gel	k1gInSc1	gel
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
Lyře	lyra	k1gFnSc6	lyra
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
na	na	k7c4	na
rodnou	rodný	k2eAgFnSc4d1	rodná
planetu	planeta	k1gFnSc4	planeta
své	svůj	k3xOyFgFnSc2	svůj
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
potřebné	potřebný	k2eAgInPc4d1	potřebný
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Černý	černý	k2eAgMnSc1d1	černý
Hig	Hig	k1gFnSc7	Hig
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
najde	najít	k5eAaPmIp3nS	najít
potřebné	potřebný	k2eAgInPc4d1	potřebný
přístroje	přístroj	k1gInPc4	přístroj
pro	pro	k7c4	pro
návrat	návrat	k1gInSc4	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
jej	on	k3xPp3gNnSc4	on
však	však	k9	však
místní	místní	k2eAgMnPc1d1	místní
divoši	divoch	k1gMnPc1	divoch
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
říká	říkat	k5eAaImIp3nS	říkat
Dravci	dravec	k1gMnSc3	dravec
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc4	život
mu	on	k3xPp3gMnSc3	on
zachránil	zachránit	k5eAaPmAgMnS	zachránit
námořník	námořník	k1gMnSc1	námořník
Duon	Duon	k1gMnSc1	Duon
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
dosud	dosud	k6eAd1	dosud
žijící	žijící	k2eAgMnSc1d1	žijící
svědek	svědek	k1gMnSc1	svědek
výsadku	výsadek	k1gInSc2	výsadek
čtyř	čtyři	k4xCgNnPc2	čtyři
set	sto	k4xCgNnPc2	sto
námořníků	námořník	k1gMnPc2	námořník
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
před	před	k7c7	před
padesáti	padesát	k4xCc7	padesát
lety	let	k1gInPc7	let
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Dravci	dravec	k1gMnPc7	dravec
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
vybili	vybít	k5eAaPmAgMnP	vybít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
Arian	Arian	k1gMnSc1	Arian
dezintegrační	dezintegrační	k2eAgFnSc4d1	dezintegrační
zbraň	zbraň	k1gFnSc4	zbraň
<g/>
,	,	kIx,	,
odváží	odvážit	k5eAaPmIp3nS	odvážit
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgFnPc1	tři
cesty	cesta	k1gFnPc1	cesta
k	k	k7c3	k
údajnému	údajný	k2eAgNnSc3d1	údajné
ragamskému	ragamský	k2eAgNnSc3d1	ragamský
skladišti	skladiště	k1gNnSc3	skladiště
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgMnSc6	který
Gelovi	Gela	k1gMnSc6	Gela
pověděl	povědět	k5eAaPmAgInS	povědět
jeden	jeden	k4xCgMnSc1	jeden
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
námořník	námořník	k1gMnSc1	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
najdou	najít	k5eAaPmIp3nP	najít
jen	jen	k6eAd1	jen
ruiny	ruina	k1gFnPc1	ruina
nějaké	nějaký	k3yIgFnSc2	nějaký
vesnice	vesnice	k1gFnSc2	vesnice
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
neustálého	neustálý	k2eAgInSc2d1	neustálý
boje	boj	k1gInSc2	boj
s	s	k7c7	s
Dravci	dravec	k1gMnPc7	dravec
pak	pak	k6eAd1	pak
ale	ale	k8xC	ale
objeví	objevit	k5eAaPmIp3nS	objevit
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
kamennou	kamenný	k2eAgFnSc4d1	kamenná
krychli	krychle	k1gFnSc4	krychle
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
přední	přední	k2eAgFnSc1d1	přední
viditelná	viditelný	k2eAgFnSc1d1	viditelná
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
šikmo	šikmo	k6eAd1	šikmo
proťata	protít	k5eAaPmNgFnS	protít
širokým	široký	k2eAgNnSc7d1	široké
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
,	,	kIx,	,
končícím	končící	k2eAgMnSc7d1	končící
uprostřed	uprostřed	k7c2	uprostřed
horní	horní	k2eAgFnSc2d1	horní
plošiny	plošina	k1gFnSc2	plošina
<g/>
.	.	kIx.	.
</s>
<s>
Proniknou	proniknout	k5eAaPmIp3nP	proniknout
dovnitř	dovnitř	k6eAd1	dovnitř
a	a	k8xC	a
objeví	objevit	k5eAaPmIp3nP	objevit
ragamské	ragamský	k2eAgFnPc1d1	ragamský
zbraně	zbraň	k1gFnPc1	zbraň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jim	on	k3xPp3gMnPc3	on
umožní	umožnit	k5eAaPmIp3nP	umožnit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
k	k	k7c3	k
vrakům	vrak	k1gInPc3	vrak
<g/>
,	,	kIx,	,
najít	najít	k5eAaPmF	najít
použitelnou	použitelný	k2eAgFnSc4d1	použitelná
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
odplout	odplout	k5eAaPmF	odplout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odhalí	odhalit	k5eAaPmIp3nS	odhalit
také	také	k9	také
tajemství	tajemství	k1gNnSc1	tajemství
původu	původ	k1gInSc2	původ
Dravců	dravec	k1gMnPc2	dravec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krychli	krychle	k1gFnSc6	krychle
totiž	totiž	k9	totiž
najdou	najít	k5eAaPmIp3nP	najít
i	i	k9	i
bomby	bomba	k1gFnPc4	bomba
s	s	k7c7	s
plynem	plyn	k1gInSc7	plyn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zničí	zničit	k5eAaPmIp3nS	zničit
během	během	k7c2	během
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
určité	určitý	k2eAgFnSc2d1	určitá
partie	partie	k1gFnSc2	partie
systému	systém	k1gInSc2	systém
ribonukleové	ribonukleový	k2eAgFnSc2d1	ribonukleová
kyseliny	kyselina	k1gFnSc2	kyselina
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
plyn	plyn	k1gInSc4	plyn
vdechnou	vdechnout	k5eAaPmIp3nP	vdechnout
<g/>
,	,	kIx,	,
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
svědomí	svědomí	k1gNnSc4	svědomí
<g/>
,	,	kIx,	,
individuální	individuální	k2eAgInSc4d1	individuální
i	i	k8xC	i
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
pocit	pocit	k1gInSc4	pocit
lidské	lidský	k2eAgFnSc2d1	lidská
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
si	se	k3xPyFc3	se
neuvědomují	uvědomovat	k5eNaImIp3nP	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stanou	stanout	k5eAaPmIp3nP	stanout
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
Dravci	dravec	k1gMnPc1	dravec
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
přihodilo	přihodit	k5eAaPmAgNnS	přihodit
původním	původní	k2eAgMnPc3d1	původní
obyvatelům	obyvatel	k1gMnPc3	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
našli	najít	k5eAaPmAgMnP	najít
skladiště	skladiště	k1gNnSc4	skladiště
a	a	k8xC	a
bomby	bomba	k1gFnPc4	bomba
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duon	Duon	k1gInSc1	Duon
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
rybářské	rybářský	k2eAgFnSc2d1	rybářská
vesničky	vesnička	k1gFnSc2	vesnička
a	a	k8xC	a
Arian	Ariana	k1gFnPc2	Ariana
s	s	k7c7	s
Gelem	gel	k1gInSc7	gel
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
do	do	k7c2	do
Lyry	lyra	k1gFnSc2	lyra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Arian	Arian	k1gMnSc1	Arian
doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
najde	najít	k5eAaPmIp3nS	najít
loď	loď	k1gFnSc1	loď
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mu	on	k3xPp3gMnSc3	on
umožní	umožnit	k5eAaPmIp3nS	umožnit
přenést	přenést	k5eAaPmF	přenést
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
http://www.kodovky.cz/kniha/160	[url]	k4	http://www.kodovky.cz/kniha/160
</s>
</p>
