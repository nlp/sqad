<p>
<s>
CODLAG	CODLAG	kA	CODLAG
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
za	za	k7c4	za
Combined	Combined	k1gInSc4	Combined
diesel-electric	diesellectric	k1gMnSc1	diesel-electric
and	and	k?	and
gas	gas	k?	gas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
CODELAG	CODELAG	kA	CODELAG
(	(	kIx(	(
Combined	Combined	k1gMnSc1	Combined
diesel-electric	diesellectric	k1gMnSc1	diesel-electric
and	and	k?	and
gas	gas	k?	gas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
námořní	námořní	k2eAgInSc1d1	námořní
pohon	pohon	k1gInSc1	pohon
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
dieselgenerátorů	dieselgenerátor	k1gInPc2	dieselgenerátor
<g/>
,	,	kIx,	,
elektromotorů	elektromotor	k1gInPc2	elektromotor
a	a	k8xC	a
plynové	plynový	k2eAgFnSc2d1	plynová
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
modifikací	modifikace	k1gFnPc2	modifikace
pohonného	pohonný	k2eAgInSc2d1	pohonný
systému	systém	k1gInSc2	systém
typu	typ	k1gInSc2	typ
CODAG	CODAG	kA	CODAG
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dieselgenerátory	Dieselgenerátor	k1gInPc1	Dieselgenerátor
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
elektřinu	elektřina	k1gFnSc4	elektřina
pro	pro	k7c4	pro
napájení	napájení	k1gNnSc4	napájení
elektromotorů	elektromotor	k1gInPc2	elektromotor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
přes	přes	k7c4	přes
hřídele	hřídel	k1gInPc4	hřídel
roztáčejí	roztáčet	k5eAaImIp3nP	roztáčet
lodní	lodní	k2eAgInPc4d1	lodní
šrouby	šroub	k1gInPc4	šroub
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
vyšší	vysoký	k2eAgFnSc2d2	vyšší
rychlosti	rychlost	k1gFnSc2	rychlost
pak	pak	k6eAd1	pak
lze	lze	k6eAd1	lze
zapojit	zapojit	k5eAaPmF	zapojit
plynovou	plynový	k2eAgFnSc4d1	plynová
turbínu	turbína	k1gFnSc4	turbína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přes	přes	k7c4	přes
převodovku	převodovka	k1gFnSc4	převodovka
a	a	k8xC	a
spojku	spojka	k1gFnSc4	spojka
roztáčí	roztáčet	k5eAaImIp3nP	roztáčet
tytéž	týž	k3xTgInPc1	týž
lodní	lodní	k2eAgInPc1d1	lodní
hřídele	hřídel	k1gInPc1	hřídel
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
není	být	k5eNaImIp3nS	být
turbína	turbína	k1gFnSc1	turbína
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
díky	díky	k7c3	díky
spojce	spojka	k1gFnSc3	spojka
odpojit	odpojit	k5eAaPmF	odpojit
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
tak	tak	k6eAd1	tak
ekonomičtější	ekonomický	k2eAgInSc4d2	ekonomičtější
pohon	pohon	k1gInSc4	pohon
elektromotory	elektromotor	k1gInPc1	elektromotor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
generovaná	generovaný	k2eAgFnSc1d1	generovaná
dieselgenerátory	dieselgenerátor	k1gMnPc7	dieselgenerátor
slouží	sloužit	k5eAaImIp3nS	sloužit
nejenom	nejenom	k6eAd1	nejenom
k	k	k7c3	k
pohonu	pohon	k1gInSc3	pohon
plavidla	plavidlo	k1gNnSc2	plavidlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
napájení	napájení	k1gNnSc3	napájení
ostatních	ostatní	k2eAgInPc2d1	ostatní
lodních	lodní	k2eAgInPc2d1	lodní
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Odpadá	odpadat	k5eAaImIp3nS	odpadat
tedy	tedy	k9	tedy
nutnost	nutnost	k1gFnSc4	nutnost
mít	mít	k5eAaImF	mít
zvlášť	zvlášť	k6eAd1	zvlášť
dieselmotory	dieselmotor	k1gInPc4	dieselmotor
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
a	a	k8xC	a
zvlášť	zvlášť	k6eAd1	zvlášť
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
i	i	k9	i
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
obsluha	obsluha	k1gFnSc1	obsluha
a	a	k8xC	a
zlevňuje	zlevňovat	k5eAaImIp3nS	zlevňovat
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Elektromotory	elektromotor	k1gInPc1	elektromotor
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
na	na	k7c4	na
hřídele	hřídel	k1gFnPc4	hřídel
napojeny	napojen	k2eAgFnPc4d1	napojena
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
převodové	převodový	k2eAgNnSc4d1	převodové
ústrojí	ústrojí	k1gNnSc4	ústrojí
a	a	k8xC	a
převodovka	převodovka	k1gFnSc1	převodovka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
výhoda	výhoda	k1gFnSc1	výhoda
CODLAG	CODLAG	kA	CODLAG
uspořádání	uspořádání	k1gNnSc1	uspořádání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dieselmotory	dieselmotor	k1gInPc1	dieselmotor
nejsou	být	k5eNaImIp3nP	být
mechanicky	mechanicky	k6eAd1	mechanicky
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
hřídelemi	hřídel	k1gFnPc7	hřídel
lodních	lodní	k2eAgMnPc2d1	lodní
šroubů	šroub	k1gInPc2	šroub
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
umístit	umístit	k5eAaPmF	umístit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
hladina	hladina	k1gFnSc1	hladina
hluku	hluk	k1gInSc2	hluk
způsobeného	způsobený	k2eAgInSc2d1	způsobený
jejich	jejich	k3xOp3gInSc7	jejich
provozem	provoz	k1gInSc7	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
zejména	zejména	k9	zejména
u	u	k7c2	u
protiponorkových	protiponorkový	k2eAgNnPc2d1	protiponorkové
plavidel	plavidlo	k1gNnPc2	plavidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
CODLAG	CODLAG	kA	CODLAG
pohonným	pohonný	k2eAgInSc7d1	pohonný
systémem	systém	k1gInSc7	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Britské	britský	k2eAgFnPc1d1	britská
fregaty	fregata	k1gFnPc1	fregata
třídy	třída	k1gFnSc2	třída
Type	typ	k1gInSc5	typ
23	[number]	k4	23
<g/>
/	/	kIx~	/
<g/>
Norfolk	Norfolk	k1gInSc1	Norfolk
</s>
</p>
<p>
<s>
Letadlové	letadlový	k2eAgFnPc1d1	letadlová
lodě	loď	k1gFnPc1	loď
britského	britský	k2eAgInSc2d1	britský
projektu	projekt	k1gInSc2	projekt
CVF	CVF	kA	CVF
<g/>
:	:	kIx,	:
HMS	HMS	kA	HMS
Queen	Queen	k1gInSc1	Queen
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
a	a	k8xC	a
HMS	HMS	kA	HMS
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
(	(	kIx(	(
<g/>
R	R	kA	R
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Německé	německý	k2eAgFnPc1d1	německá
fregaty	fregata	k1gFnPc1	fregata
třídy	třída	k1gFnSc2	třída
F125	F125	k1gFnSc2	F125
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
objednána	objednán	k2eAgFnSc1d1	objednána
stavba	stavba	k1gFnSc1	stavba
prvních	první	k4xOgFnPc2	první
čtyř	čtyři	k4xCgFnPc2	čtyři
jednotek	jednotka	k1gFnPc2	jednotka
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
GTS	GTS	kA	GTS
Finnjet	Finnjet	k1gInSc1	Finnjet
(	(	kIx(	(
<g/>
finský	finský	k2eAgInSc1d1	finský
trajekt	trajekt	k1gInSc1	trajekt
(	(	kIx(	(
<g/>
cruiseferry	cruiseferra	k1gFnPc1	cruiseferra
<g/>
))	))	k?	))
</s>
</p>
