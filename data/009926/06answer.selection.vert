<s>
CODLAG	CODLAG	kA	CODLAG
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
za	za	k7c4	za
Combined	Combined	k1gInSc4	Combined
diesel-electric	diesellectric	k1gMnSc1	diesel-electric
and	and	k?	and
gas	gas	k?	gas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
CODELAG	CODELAG	kA	CODELAG
(	(	kIx(	(
Combined	Combined	k1gMnSc1	Combined
diesel-electric	diesellectric	k1gMnSc1	diesel-electric
and	and	k?	and
gas	gas	k?	gas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kombinovaný	kombinovaný	k2eAgInSc1d1	kombinovaný
námořní	námořní	k2eAgInSc1d1	námořní
pohon	pohon	k1gInSc1	pohon
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
kombinaci	kombinace	k1gFnSc6	kombinace
dieselgenerátorů	dieselgenerátor	k1gInPc2	dieselgenerátor
<g/>
,	,	kIx,	,
elektromotorů	elektromotor	k1gInPc2	elektromotor
a	a	k8xC	a
plynové	plynový	k2eAgFnSc2d1	plynová
turbíny	turbína	k1gFnSc2	turbína
<g/>
.	.	kIx.	.
</s>
