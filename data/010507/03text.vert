<p>
<s>
Klemen	Klemen	k2eAgInSc1d1	Klemen
Bečan	Bečan	k1gInSc1	Bečan
(	(	kIx(	(
<g/>
*	*	kIx~	*
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
slovinský	slovinský	k2eAgMnSc1d1	slovinský
reprezentant	reprezentant	k1gMnSc1	reprezentant
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
<g/>
,	,	kIx,	,
juniorský	juniorský	k2eAgMnSc1d1	juniorský
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
vítěz	vítěz	k1gMnSc1	vítěz
zimních	zimní	k2eAgFnPc2d1	zimní
armádních	armádní	k2eAgFnPc2d1	armádní
světových	světový	k2eAgFnPc2d1	světová
her	hra	k1gFnPc2	hra
v	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závodům	závod	k1gInPc3	závod
ve	v	k7c6	v
sportovním	sportovní	k2eAgNnSc6d1	sportovní
lezení	lezení	k1gNnSc6	lezení
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Jure	Jur	k1gFnSc2	Jur
Bečan	Bečan	k1gMnSc1	Bečan
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nejlépe	dobře	k6eAd3	dobře
umístil	umístit	k5eAaPmAgMnS	umístit
celkově	celkově	k6eAd1	celkově
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c4	v
lezení	lezení	k1gNnSc4	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
na	na	k7c4	na
EPJ	EPJ	kA	EPJ
2006	[number]	k4	2006
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
A	a	k9	a
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
závodech	závod	k1gInPc6	závod
dospělých	dospělí	k1gMnPc2	dospělí
však	však	k8xC	však
již	již	k6eAd1	již
nezískal	získat	k5eNaPmAgMnS	získat
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výkony	výkon	k1gInPc1	výkon
a	a	k8xC	a
ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
2010,2013	[number]	k4	2010,2013
<g/>
:	:	kIx,	:
dvakrát	dvakrát	k6eAd1	dvakrát
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
lezení	lezení	k1gNnSc6	lezení
na	na	k7c4	na
obtížnost	obtížnost	k1gFnSc4	obtížnost
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
armádních	armádní	k2eAgFnPc6d1	armádní
světových	světový	k2eAgFnPc6d1	světová
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgInS	odnést
celkem	celkem	k6eAd1	celkem
čtyři	čtyři	k4xCgFnPc1	čtyři
medaile	medaile	k1gFnPc1	medaile
</s>
</p>
<p>
<s>
===	===	k?	===
Závodní	závodní	k2eAgInPc1d1	závodní
výsledky	výsledek	k1gInPc1	výsledek
===	===	k?	===
</s>
</p>
<p>
<s>
poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
nalevo	nalevo	k6eAd1	nalevo
jsou	být	k5eAaImIp3nP	být
poslední	poslední	k2eAgInPc4d1	poslední
závody	závod	k1gInPc4	závod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Klemen	Klemen	k2eAgInSc4d1	Klemen
Bečan	Bečan	k1gInSc4	Bečan
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Závodní	závodní	k2eAgInSc1d1	závodní
profil	profil	k1gInSc1	profil
Klemena	Klemen	k2eAgFnSc1d1	Klemen
Bečana	Bečana	k1gFnSc1	Bečana
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
federace	federace	k1gFnSc2	federace
sportovního	sportovní	k2eAgNnSc2d1	sportovní
lezení	lezení	k1gNnSc2	lezení
(	(	kIx(	(
<g/>
IFSC	IFSC	kA	IFSC
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Závodní	závodní	k2eAgInSc1d1	závodní
profil	profil	k1gInSc1	profil
Klemena	Klemen	k2eAgFnSc1d1	Klemen
Bečana	Bečana	k1gFnSc1	Bečana
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Digitalrock	Digitalrocko	k1gNnPc2	Digitalrocko
<g/>
.	.	kIx.	.
<g/>
de	de	k?	de
</s>
</p>
