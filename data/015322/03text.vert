<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuChráněná	infoboxuChráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
PálavaIUCN	PálavaIUCN	k1gFnSc2
kategorie	kategorie	k1gFnSc2
V	V	kA
(	(	kIx(
<g/>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
)	)	kIx)
Severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
Pálavy	Pálavy	k?
(	(	kIx(
<g/>
Kotel	kotel	k1gInSc1
<g/>
,	,	kIx,
Děvín	Děvín	k1gInSc1
<g/>
)	)	kIx)
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
83	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
20,29	20,29	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
7,54	7,54	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
73	#num#	k4
Web	web	k1gInSc1
</s>
<s>
www.palava.ochranaprirody.cz	www.palava.ochranaprirody.cz	k1gMnSc1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
ze	z	k7c2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1976	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
o	o	k7c6
rozloze	rozloha	k1gFnSc6
83	#num#	k4
km²	km²	k?
zahrnujícím	zahrnující	k2eAgMnSc7d1
hřebeny	hřeben	k1gInPc4
Pavlovských	pavlovský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
,	,	kIx,
Milovický	milovický	k2eAgInSc4d1
les	les	k1gInSc4
a	a	k8xC
snížseninu	snížsenina	k1gFnSc4
jižně	jižně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gNnSc2
až	až	k9
po	po	k7c6
státní	státní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
ochraňuje	ochraňovat	k5eAaImIp3nS
nejcennější	cenný	k2eAgInPc4d3
biotopy	biotop	k1gInPc4
druhově	druhově	k6eAd1
bohatých	bohatý	k2eAgFnPc2d1
skalních	skalní	k2eAgFnPc2d1
<g/>
,	,	kIx,
drnových	drnový	k2eAgFnPc2d1
a	a	k8xC
lučních	luční	k2eAgFnPc2d1
stepí	step	k1gFnPc2
<g/>
,	,	kIx,
lesostepí	lesostep	k1gFnPc2
<g/>
,	,	kIx,
teplomilných	teplomilný	k2eAgFnPc2d1
doubrav	doubrava	k1gFnPc2
a	a	k8xC
suťových	suťový	k2eAgInPc2d1
lesů	les	k1gInPc2
vyvinutých	vyvinutý	k2eAgInPc2d1
na	na	k7c6
vápencových	vápencový	k2eAgInPc6d1
kopcích	kopec	k1gInPc6
Pavlovských	pavlovský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lesní	lesní	k2eAgInSc4d1
komplex	komplex	k1gInSc4
Milovického	milovický	k2eAgInSc2d1
lesa	les	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
teplomilné	teplomilný	k2eAgFnPc4d1
doubravy	doubrava	k1gFnPc4
a	a	k8xC
panonské	panonský	k2eAgFnPc4d1
dubohabřiny	dubohabřina	k1gFnPc4
se	s	k7c7
dvěma	dva	k4xCgFnPc7
oborami	obora	k1gFnPc7
pro	pro	k7c4
chov	chov	k1gInSc4
zvěře	zvěř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dub	Dub	k1gMnSc1
šípák	šípák	k1gMnSc1
nad	nad	k7c7
Soutěskou	soutěska	k1gFnSc7
–	–	k?
strom	strom	k1gInSc4
byl	být	k5eAaImAgMnS
inspirací	inspirace	k1gFnSc7
pro	pro	k7c4
logo	logo	k1gNnSc4
CHKO	CHKO	kA
</s>
<s>
V	v	k7c6
nivě	niva	k1gFnSc6
Dyje	Dyje	k1gFnSc2
mezi	mezi	k7c7
Novými	nový	k2eAgInPc7d1
Mlýny	mlýn	k1gInPc7
a	a	k8xC
Bulhary	Bulhar	k1gMnPc7
se	se	k3xPyFc4
střídají	střídat	k5eAaImIp3nP
lužní	lužní	k2eAgInPc1d1
lesy	les	k1gInPc1
s	s	k7c7
jinými	jiný	k2eAgNnPc7d1
mokřadními	mokřadní	k2eAgNnPc7d1
nebo	nebo	k8xC
vodními	vodní	k2eAgNnPc7d1
společenstvy	společenstvo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
byla	být	k5eAaImAgFnS
chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
organizací	organizace	k1gFnSc7
UNESCO	Unesco	k1gNnSc1
zařazena	zařazen	k2eAgFnSc1d1
do	do	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
biosférických	biosférický	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
chránících	chránící	k2eAgFnPc2d1
ukázky	ukázka	k1gFnPc4
nejvýznačnějších	význačný	k2eAgInPc2d3
světových	světový	k2eAgInPc2d1
ekosystémů	ekosystém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
území	území	k1gNnSc6
CHKO	CHKO	kA
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
vyhlášena	vyhlášen	k2eAgFnSc1d1
ptačí	ptačí	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
populace	populace	k1gFnPc1
čápa	čáp	k1gMnSc2
bílého	bílý	k1gMnSc2
<g/>
,	,	kIx,
orla	orel	k1gMnSc2
mořského	mořský	k2eAgMnSc2d1
<g/>
,	,	kIx,
včelojeda	včelojed	k1gMnSc2
lesního	lesní	k2eAgMnSc2d1
<g/>
,	,	kIx,
strakapouda	strakapoud	k1gMnSc2
jižního	jižní	k2eAgMnSc2d1
<g/>
,	,	kIx,
strakapouda	strakapoud	k1gMnSc2
prostředního	prostřední	k2eAgMnSc2d1
<g/>
,	,	kIx,
pěnice	pěnice	k1gFnPc4
vlašské	vlašský	k2eAgFnPc4d1
<g/>
,	,	kIx,
lejska	lejsek	k1gMnSc2
bělokrkého	bělokrký	k2eAgMnSc2d1
a	a	k8xC
ťuhýka	ťuhýk	k1gMnSc2
obecného	obecný	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sídlo	sídlo	k1gNnSc4
správy	správa	k1gFnSc2
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
<g/>
:	:	kIx,
692	#num#	k4
01	#num#	k4
Mikulov	Mikulov	k1gInSc1
<g/>
,	,	kIx,
Náměstí	náměstí	k1gNnSc1
32	#num#	k4
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Děvičky	Děvička	k1gFnPc1
<g/>
,	,	kIx,
pohled	pohled	k1gInSc1
z	z	k7c2
Děvína	Děvín	k1gInSc2
(	(	kIx(
<g/>
pod	pod	k7c7
nimi	on	k3xPp3gFnPc7
novomlýnské	novomlýnský	k2eAgFnPc1d1
nádrže	nádrž	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Děvín	Děvín	k1gInSc1
</s>
<s>
Sirotčí	sirotčí	k2eAgInSc1d1
hrádek	hrádek	k1gInSc1
u	u	k7c2
Klentnice	Klentnice	k1gFnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
v	v	k7c6
Klentnici	Klentnice	k1gFnSc6
</s>
<s>
NPR	NPR	kA
Růžový	růžový	k2eAgInSc4d1
vrch	vrch	k1gInSc4
</s>
<s>
PP	PP	kA
Kočičí	kočičí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
</s>
<s>
Svatý	svatý	k2eAgInSc1d1
kopeček	kopeček	k1gInSc1
nad	nad	k7c7
Mikulovem	Mikulov	k1gInSc7
</s>
<s>
Mikulov	Mikulov	k1gInSc1
<g/>
,	,	kIx,
městská	městský	k2eAgFnSc1d1
památková	památkový	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
–	–	k?
pohled	pohled	k1gInSc4
na	na	k7c4
jih	jih	k1gInSc4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.zakonyprolidi.cz/cs/2004-682	http://www.zakonyprolidi.cz/cs/2004-682	k4
Předpis	předpis	k1gInSc1
č.	č.	k?
682	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
</s>
<s>
Nařízení	nařízení	k1gNnPc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
se	se	k3xPyFc4
vymezuje	vymezovat	k5eAaImIp3nS
Ptačí	ptačí	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ČERNÁ	Černá	k1gFnSc1
<g/>
,	,	kIx,
Anna	Anna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Mionší	Mionš	k1gFnPc2
až	až	k9
na	na	k7c4
bošilecký	bošilecký	k2eAgInSc4d1
mostek	mostek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živa	Živa	k1gFnSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
63	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
4	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
LXXVIII	LXXVIII	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
K	k	k7c3
výkladu	výklad	k1gInSc3
názvu	název	k1gInSc2
Pálava	Pálava	k1gFnSc1
<g/>
,	,	kIx,
Palava	Palava	k1gFnSc1
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
také	také	k9
z	z	k7c2
<g/>
:	:	kIx,
http://ziva.avcr.cz/files/ziva/pdf/z-mionsi-az-na-bosilecky-mostek.pdf	http://ziva.avcr.cz/files/ziva/pdf/z-mionsi-az-na-bosilecky-mostek.pdf	k1gInSc1
</s>
<s>
DANIHELKA	Danihelka	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
ed	ed	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pálava	Pálava	k1gFnSc1
na	na	k7c6
prahu	práh	k1gInSc6
třetího	třetí	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnSc2
<g/>
:	:	kIx,
[	[	kIx(
<g/>
Mikulov	Mikulov	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
9	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2001	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikulov	Mikulov	k1gInSc1
<g/>
:	:	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
138	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
1975	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
[	[	kIx(
<g/>
Sborník	sborník	k1gInSc1
z	z	k7c2
konference	konference	k1gFnSc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
</s>
<s>
DANIHELKA	Danihelka	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
CHYTIL	Chytil	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
KORDIOVSKÝ	KORDIOVSKÝ	kA
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
a	a	k8xC
biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
Pálava	Pálava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
Děvín	Děvín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikulov	Mikulov	k1gInSc1
<g/>
:	:	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
a	a	k8xC
biosférické	biosférický	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Pálava	Pálava	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
19	#num#	k4
s.	s.	k?
</s>
<s>
DANIHELKA	Danihelka	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
KORDIOVSKÝ	KORDIOVSKÝ	kA
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
a	a	k8xC
MACHÁČEK	Macháček	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
a	a	k8xC
biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
UNESCO	UNESCO	kA
Pálava	Pálava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
Svatý	svatý	k2eAgInSc4d1
kopeček	kopeček	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikulov	Mikulov	k1gInSc1
<g/>
:	:	kIx,
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
a	a	k8xC
biosférické	biosférický	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
Pálava	Pálava	k1gFnSc1
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
23	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
900189	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
<g/>
:	:	kIx,
nejkrásnější	krásný	k2eAgInPc1d3
výlety	výlet	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
87457	#num#	k4
<g/>
-	-	kIx~
<g/>
66	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
LAŠTŮVKA	laštůvka	k1gFnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Motýli	motýl	k1gMnPc1
rozšířeného	rozšířený	k2eAgNnSc2d1
území	území	k1gNnSc2
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
=	=	kIx~
Lepidoptera	Lepidopter	k1gMnSc4
of	of	k?
the	the	k?
Protected	Protected	k1gInSc1
Landscape	Landscap	k1gInSc5
Area	area	k1gFnSc1
Pálava	Pálava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fotografie	fotografie	k1gFnSc1
Petr	Petr	k1gMnSc1
MACHÁČEK	Macháček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Konvoj	konvoj	k1gInSc1
<g/>
,	,	kIx,
1994	#num#	k4
<g/>
.	.	kIx.
118	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7157	#num#	k4
<g/>
-	-	kIx~
<g/>
116	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MATUŠKA	Matuška	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgInPc1d1
milníky	milník	k1gInPc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Pálava	Pálava	k1gFnSc1
v	v	k7c6
letech	léto	k1gNnPc6
1976	#num#	k4
až	až	k9
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
RegioM	RegioM	k1gFnSc1
<g/>
:	:	kIx,
sborník	sborník	k1gInSc1
Regionálního	regionální	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Mikulově	Mikulov	k1gInSc6
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
112	#num#	k4
<g/>
–	–	k?
<g/>
127	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1211	#num#	k4
<g/>
-	-	kIx~
<g/>
5800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85088	#num#	k4
<g/>
-	-	kIx~
<g/>
50	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MATUŠKA	Matuška	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
zřízení	zřízení	k1gNnSc2
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Palava	Palava	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc4
slavnostní	slavnostní	k2eAgNnSc4d1
vyhlášení	vyhlášení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
RegioM	RegioM	k1gFnSc1
<g/>
:	:	kIx,
sborník	sborník	k1gInSc1
Regionálního	regionální	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
v	v	k7c6
Mikulově	Mikulov	k1gInSc6
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
6	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
<g/>
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85088	#num#	k4
<g/>
-	-	kIx~
<g/>
48	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
NEŠPOROVÁ	Nešporová	k1gFnSc1
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diskuse	diskuse	k1gFnSc1
ohledně	ohledně	k7c2
čárky	čárka	k1gFnSc2
vedly	vést	k5eAaImAgInP
až	až	k9
ke	k	k7c3
změně	změna	k1gFnSc3
názvu	název	k1gInSc2
týdeníku	týdeník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovnost	rovnost	k1gFnSc1
<g/>
,	,	kIx,
brněnský	brněnský	k2eAgInSc1d1
a	a	k8xC
jihomoravský	jihomoravský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2002	#num#	k4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
POKORNÝ	Pokorný	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pálava	Pálava	k1gFnSc1
nebo	nebo	k8xC
Palava	Palava	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Rovnost	rovnost	k1gFnSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1999	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
PROKEŠOVÁ	Prokešová	k1gFnSc1
<g/>
,	,	kIx,
Helena	Helena	k1gFnSc1
a	a	k8xC
CHYTRÝ	chytrý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Kryštof	Kryštof	k1gMnSc1
<g/>
.	.	kIx.
100	#num#	k4
pálavských	pálavský	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
:	:	kIx,
fotografický	fotografický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mikulov	Mikulov	k1gInSc1
<g/>
:	:	kIx,
Okrašlovací	okrašlovací	k2eAgInSc1d1
spolek	spolek	k1gInSc1
Mikulov	Mikulov	k1gInSc1
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
se	s	k7c7
Správou	správa	k1gFnSc7
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
211	#num#	k4
s.	s.	k?
<g/>
,	,	kIx,
1	#num#	k4
mapa	mapa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
270	#num#	k4
<g/>
-	-	kIx~
<g/>
7361	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ŠAUR	ŠAUR	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
původu	původ	k1gInSc6
jména	jméno	k1gNnSc2
Pálava	Pálava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Haló	haló	k0
noviny	novina	k1gFnPc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
2007	#num#	k4
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Správa	správa	k1gFnSc1
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
</s>
<s>
Pálava	Pálava	k1gFnSc1
na	na	k7c6
Příroda	příroda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Biosférická	biosférický	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
Dolní	dolní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Maloplošná	Maloplošný	k2eAgNnPc1d1
chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
CHKO	CHKO	kA
Pálava	Pálava	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Děvín	Děvín	k1gInSc1
•	•	k?
Křivé	křivý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Slanisko	slanisko	k1gNnSc1
u	u	k7c2
Nesytu	nesyta	k1gFnSc4
•	•	k?
Tabulová	tabulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Růžový	růžový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
a	a	k8xC
Kočičí	kočičí	k2eAgInSc1d1
kámen	kámen	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Kalendář	kalendář	k1gInSc1
věků	věk	k1gInPc2
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Liščí	liščí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Milovická	milovický	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgInSc1d1
kopeček	kopeček	k1gInSc1
•	•	k?
Šibeničník	šibeničník	k1gMnSc1
•	•	k?
Turold	Turold	k1gMnSc1
Přírodní	přírodní	k2eAgMnSc1d1
památky	památka	k1gFnPc4
</s>
<s>
Anenský	anenský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Kienberg	Kienberg	k1gInSc1
•	•	k?
Kočičí	kočičí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Růžový	růžový	k2eAgInSc1d1
kopec	kopec	k1gInSc1
</s>
<s>
Chráněné	chráněný	k2eAgFnPc1d1
krajinné	krajinný	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Beskydy	Beskyd	k1gInPc1
•	•	k?
Bílé	bílý	k2eAgInPc1d1
Karpaty	Karpaty	k1gInPc1
•	•	k?
Blaník	Blaník	k1gInSc1
•	•	k?
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Brdy	Brdy	k1gInPc1
•	•	k?
Broumovsko	Broumovsko	k1gNnSc4
•	•	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Český	český	k2eAgInSc1d1
kras	kras	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
•	•	k?
Jeseníky	Jeseník	k1gInPc1
•	•	k?
Jizerské	jizerský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
•	•	k?
Křivoklátsko	Křivoklátsko	k1gNnSc1
•	•	k?
Labské	labský	k2eAgInPc1d1
pískovce	pískovec	k1gInPc1
•	•	k?
Litovelské	litovelský	k2eAgNnSc1d1
Pomoraví	Pomoraví	k1gNnSc1
•	•	k?
Lužické	lužický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Moravský	moravský	k2eAgInSc4d1
kras	kras	k1gInSc4
•	•	k?
Orlické	orlický	k2eAgFnSc2d1
hory	hora	k1gFnSc2
•	•	k?
Pálava	Pálava	k1gFnSc1
•	•	k?
Poodří	Poodří	k1gNnPc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
•	•	k?
Třeboňsko	Třeboňsko	k1gNnSc1
•	•	k?
Žďárské	Žďárské	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
•	•	k?
Železné	železný	k2eAgFnSc2d1
hory	hora	k1gFnSc2
</s>
<s>
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Břeclav	Břeclav	k1gFnSc1
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Pálava	Pálava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc1d1
parky	park	k1gInPc1
</s>
<s>
Niva	niva	k1gFnSc1
Dyje	Dyje	k1gFnSc2
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Cahnov	Cahnov	k1gInSc1
–	–	k?
Soutok	soutok	k1gInSc1
•	•	k?
Děvín	Děvín	k1gInSc1
•	•	k?
Křivé	křivý	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
•	•	k?
Lednické	lednický	k2eAgInPc1d1
rybníky	rybník	k1gInPc1
•	•	k?
Pouzdřanská	Pouzdřanský	k2eAgFnSc1d1
step	step	k1gFnSc1
–	–	k?
Kolby	kolba	k1gFnSc2
•	•	k?
Ranšpurk	Ranšpurk	k1gInSc1
•	•	k?
Slanisko	slanisko	k1gNnSc1
u	u	k7c2
Nesytu	nesyta	k1gFnSc4
•	•	k?
Tabulová	tabulový	k2eAgFnSc1d1
•	•	k?
Tabulová	tabulový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Růžový	růžový	k2eAgInSc1d1
vrch	vrch	k1gInSc1
a	a	k8xC
Kočičí	kočičí	k2eAgInSc1d1
kámen	kámen	k1gInSc1
(	(	kIx(
<g/>
zrušená	zrušený	k2eAgFnSc1d1
<g/>
)	)	kIx)
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
</s>
<s>
Dunajovické	Dunajovický	k2eAgInPc1d1
kopce	kopec	k1gInPc1
•	•	k?
Kalendář	kalendář	k1gInSc1
věků	věk	k1gInPc2
•	•	k?
Kukle	kukle	k1gFnPc4
•	•	k?
Pastvisko	pastvisko	k1gNnSc1
u	u	k7c2
Lednice	Lednice	k1gFnSc2
•	•	k?
Rendez-vous	rendez-vous	k1gNnPc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Františkův	Františkův	k2eAgInSc1d1
rybník	rybník	k1gInSc1
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Lipiny	lipina	k1gFnSc2
•	•	k?
Liščí	liščí	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Milovická	milovický	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Nosperk	Nosperk	k1gInSc1
•	•	k?
Roviny	rovina	k1gFnSc2
•	•	k?
Slanisko	slanisko	k1gNnSc4
Dobré	dobrý	k2eAgFnSc2d1
Pole	pole	k1gFnSc2
•	•	k?
Slanisko	slanisko	k1gNnSc1
Novosedly	Novosedlo	k1gNnPc7
•	•	k?
Stibůrkovská	Stibůrkovský	k2eAgNnPc4d1
jezera	jezero	k1gNnPc4
•	•	k?
Svatý	svatý	k2eAgInSc1d1
kopeček	kopeček	k1gInSc1
•	•	k?
Šibeničník	šibeničník	k1gMnSc1
•	•	k?
Turold	Turold	k1gInSc1
•	•	k?
Velký	velký	k2eAgInSc1d1
Kuntínov	Kuntínov	k1gInSc1
•	•	k?
Věstonická	věstonický	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
•	•	k?
Zázmoníky	Zázmoník	k1gInPc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Anenský	anenský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Hochberk	Hochberk	k1gInSc1
•	•	k?
Jesličky	jesličky	k1gFnPc4
•	•	k?
Jezírko	jezírko	k1gNnSc1
Kutnar	Kutnar	k1gMnSc1
•	•	k?
Kamenice	Kamenice	k1gFnSc2
u	u	k7c2
Hlohovce	Hlohovec	k1gInSc2
•	•	k?
Kameníky	Kameník	k1gMnPc4
•	•	k?
Kamenný	kamenný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Kurdějova	Kurdějův	k2eAgNnSc2d1
•	•	k?
Kienberg	Kienberg	k1gInSc1
•	•	k?
Kočičí	kočičí	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Květné	květný	k2eAgNnSc4d1
jezero	jezero	k1gNnSc4
•	•	k?
Lange	Lange	k1gInSc1
Wart	Wart	k1gInSc1
•	•	k?
Lom	lom	k1gInSc1
Janičův	Janičův	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Louky	louka	k1gFnSc2
pod	pod	k7c7
Kumstátem	Kumstát	k1gInSc7
•	•	k?
Na	na	k7c6
cvičišti	cvičiště	k1gNnSc6
•	•	k?
Paví	paví	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Plácky	plácek	k1gInPc1
•	•	k?
Přední	přední	k2eAgFnSc2d1
kopaniny	kopanina	k1gFnSc2
•	•	k?
Růžový	růžový	k2eAgInSc1d1
kopec	kopec	k1gInSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Sedlece	Sedleec	k1gInSc2
•	•	k?
Stračí	stračí	k2eAgInSc1d1
•	•	k?
Studánkový	studánkový	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Trkmanec-Rybníčky	Trkmanec-Rybníček	k1gInPc4
•	•	k?
Úvalský	Úvalský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Zimarky	Zimarka	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128450	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
</s>
