<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Pálava	Pálava	k1gFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
výnosem	výnos	k1gInSc7
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
ČSR	ČSR	kA
ze	z	k7c2
dne	den	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1976	#num#	k4
na	na	k7c6
území	území	k1gNnSc6
o	o	k7c6
rozloze	rozloha	k1gFnSc6
83	#num#	k4
km²	km²	k?
zahrnujícím	zahrnující	k2eAgMnSc7d1
hřebeny	hřeben	k1gInPc4
Pavlovských	pavlovský	k2eAgInPc2d1
vrchů	vrch	k1gInPc2
<g/>
,	,	kIx,
Milovický	milovický	k2eAgInSc4d1
les	les	k1gInSc4
a	a	k8xC
snížseninu	snížsenina	k1gFnSc4
jižně	jižně	k6eAd1
od	od	k7c2
něj	on	k3xPp3gNnSc2
až	až	k9
po	po	k7c6
státní	státní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
s	s	k7c7
Rakouskem	Rakousko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>