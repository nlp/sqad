<s>
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
je	být	k5eAaImIp3nS
odvětvím	odvětví	k1gNnSc7
systematické	systematický	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
člověkem	člověk	k1gMnSc7
z	z	k7c2
hlediska	hledisko	k1gNnSc2
křesťanské	křesťanský	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k9
bytím	bytí	k1gNnSc7
člověka	člověk	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc7
určením	určení	k1gNnSc7
před	před	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>