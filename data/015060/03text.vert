<s>
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
</s>
<s>
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
je	být	k5eAaImIp3nS
odvětvím	odvětví	k1gNnSc7
systematické	systematický	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
předmětem	předmět	k1gInSc7
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
křesťanského	křesťanský	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
a	a	k8xC
z	z	k7c2
pohledu	pohled	k1gInSc2
dogmatického	dogmatický	k2eAgInSc2d1
traktátu	traktát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
<g/>
"	"	kIx"
</s>
<s>
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
je	být	k5eAaImIp3nS
odvětvím	odvětví	k1gNnSc7
systematické	systematický	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
člověkem	člověk	k1gMnSc7
z	z	k7c2
hlediska	hledisko	k1gNnSc2
křesťanské	křesťanský	k2eAgFnSc2d1
teologie	teologie	k1gFnSc2
<g/>
,	,	kIx,
zvláště	zvláště	k9
bytím	bytí	k1gNnSc7
člověka	člověk	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc7
určením	určení	k1gNnSc7
před	před	k7c7
Bohem	bůh	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
je	být	k5eAaImIp3nS
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
název	název	k1gInSc1
dogmatického	dogmatický	k2eAgInSc2d1
traktátu	traktát	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
ve	v	k7c6
světle	světlo	k1gNnSc6
dějinného	dějinný	k2eAgNnSc2d1
Božího	boží	k2eAgNnSc2d1
sebezjevení	sebezjevení	k1gNnSc2
v	v	k7c6
Ježíši	Ježíš	k1gMnSc6
Kristu	Krista	k1gFnSc4
interpretuje	interpretovat	k5eAaBmIp3nS
původ	původ	k1gInSc4
a	a	k8xC
určení	určení	k1gNnSc4
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
duchovní	duchovní	k2eAgFnSc3d1
a	a	k8xC
etické	etický	k2eAgFnSc3d1
orientaci	orientace	k1gFnSc3
v	v	k7c6
životě	život	k1gInSc6
na	na	k7c6
základě	základ	k1gInSc6
křesťanské	křesťanský	k2eAgFnSc2d1
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
poskytují	poskytovat	k5eAaImIp3nP
biblické	biblický	k2eAgInPc4d1
spisy	spis	k1gInPc4
a	a	k8xC
církevní	církevní	k2eAgFnPc4d1
tradice	tradice	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
následně	následně	k6eAd1
ptala	ptat	k5eAaImAgFnS
po	po	k7c6
základních	základní	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
týkajících	týkající	k2eAgFnPc6d1
se	se	k3xPyFc4
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgFnPc4
otázky	otázka	k1gFnPc4
patří	patřit	k5eAaImIp3nP
<g/>
:	:	kIx,
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
jsem	být	k5eAaImIp1nS
já	já	k3xPp1nSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
jsi	být	k5eAaImIp2nS
ty	ty	k3xPp2nSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
je	být	k5eAaImIp3nS
bolest	bolest	k1gFnSc1
<g/>
,	,	kIx,
zlo	zlo	k1gNnSc1
<g/>
,	,	kIx,
smrt	smrt	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jaká	jaký	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	on	k3xPp3gFnPc4
naděje	naděje	k1gFnPc4
lidstva	lidstvo	k1gNnSc2
do	do	k7c2
budoucnosti	budoucnost	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
život	život	k1gInSc4
po	po	k7c6
tomto	tento	k3xDgInSc6
životě	život	k1gInSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
apod.	apod.	kA
Součástí	součást	k1gFnPc2
teologické	teologický	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
je	být	k5eAaImIp3nS
teologická	teologický	k2eAgFnSc1d1
reflexe	reflexe	k1gFnSc1
stvoření	stvoření	k1gNnSc2
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnSc2
dokonalosti	dokonalost	k1gFnSc2
a	a	k8xC
svatosti	svatost	k1gFnSc2
<g/>
,	,	kIx,
prvotního	prvotní	k2eAgInSc2d1
hříchu	hřích	k1gInSc2
<g/>
,	,	kIx,
ospravedlnění	ospravedlnění	k1gNnSc4
člověka	člověk	k1gMnSc2
v	v	k7c6
Ježíši	Ježíš	k1gMnSc6
Kristu	Krista	k1gFnSc4
<g/>
,	,	kIx,
milosti	milost	k1gFnPc4
boží	boží	k2eAgFnSc2d1
a	a	k8xC
konečného	konečný	k2eAgInSc2d1
cíle	cíl	k1gInSc2
člověka	člověk	k1gMnSc2
(	(	kIx(
<g/>
eschatologie	eschatologie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Tematické	tematický	k2eAgInPc1d1
okruhy	okruh	k1gInPc1
teologické	teologický	k2eAgFnSc2d1
antropologie	antropologie	k1gFnSc2
</s>
<s>
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
dvou	dva	k4xCgInPc2
tematických	tematický	k2eAgInPc2d1
okruhů	okruh	k1gInPc2
<g/>
:	:	kIx,
1	#num#	k4
<g/>
)	)	kIx)
Zahrnuje	zahrnovat	k5eAaImIp3nS
apriorně	apriorně	k6eAd1
–	–	k?
transcendentální	transcendentální	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
a	a	k8xC
podmínky	podmínka	k1gFnPc4
lidské	lidský	k2eAgFnSc2d1
existence	existence	k1gFnSc2
před	před	k7c7
Bohem	bůh	k1gMnSc7
(	(	kIx(
<g/>
stvořenost	stvořenost	k1gFnSc1
<g/>
,	,	kIx,
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
Boží	boží	k2eAgInSc4d1
obraz	obraz	k1gInSc4
<g/>
,	,	kIx,
personalitu	personalita	k1gFnSc4
<g/>
,	,	kIx,
socialitu	socialita	k1gFnSc4
<g/>
,	,	kIx,
duchovost	duchovost	k1gFnSc4
<g/>
,	,	kIx,
svobodu	svoboda	k1gFnSc4
<g/>
,	,	kIx,
tělesnost	tělesnost	k1gFnSc4
<g/>
,	,	kIx,
dějinnost	dějinnost	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teologická	teologický	k2eAgFnSc1d1
antropologie	antropologie	k1gFnSc1
vede	vést	k5eAaImIp3nS
dialog	dialog	k1gInSc4
s	s	k7c7
filozofickou	filozofický	k2eAgFnSc7d1
antropologií	antropologie	k1gFnSc7
ve	v	k7c6
společné	společný	k2eAgFnSc6d1
tematizaci	tematizace	k1gFnSc6
základní	základní	k2eAgFnSc2d1
otázky	otázka	k1gFnSc2
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
(	(	kIx(
<g/>
ŽI	ŽI	kA
8,5	8,5	k4
<g/>
;	;	kIx,
GS	GS	kA
10	#num#	k4
<g/>
;	;	kIx,
E.	E.	kA
Kant	Kant	k1gMnSc1
<g/>
,	,	kIx,
Logika	logika	k1gFnSc1
[	[	kIx(
<g/>
1801	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Úvod	úvod	k1gInSc1
<g/>
,	,	kIx,
A	a	k9
25	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflektuje	reflektovat	k5eAaImIp3nS
aposteriorně	aposteriorně	k6eAd1
–	–	k?
kategoriální	kategoriální	k2eAgFnSc4d1
dějinnou	dějinný	k2eAgFnSc4d1
<g/>
,	,	kIx,
společenskou	společenský	k2eAgFnSc4d1
a	a	k8xC
přírodní	přírodní	k2eAgFnSc4d1
situovanost	situovanost	k1gFnSc4
člověka	člověk	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
konkrétním	konkrétní	k2eAgNnSc6d1
životním	životní	k2eAgNnSc6d1
světe	svět	k1gInSc5
(	(	kIx(
<g/>
původní	původní	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
přirozenosti	přirozenost	k1gFnSc2
a	a	k8xC
milosti	milost	k1gFnSc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
prvotním	prvotní	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
,	,	kIx,
porušenost	porušenost	k1gFnSc1
vztahu	vztah	k1gInSc2
k	k	k7c3
Bohu	bůh	k1gMnSc3
a	a	k8xC
ostatním	ostatní	k2eAgMnPc3d1
lidem	člověk	k1gMnPc3
skrze	skrze	k?
hřích	hřích	k1gInSc4
<g/>
,	,	kIx,
zkušenost	zkušenost	k1gFnSc4
negativity	negativita	k1gFnSc2
a	a	k8xC
deficience	deficience	k1gFnSc2
v	v	k7c6
utrpění	utrpění	k1gNnSc6
a	a	k8xC
smrti	smrt	k1gFnSc6
i	i	k9
naděje	naděje	k1gFnSc1
na	na	k7c4
úplné	úplný	k2eAgNnSc4d1
vykoupení	vykoupení	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dokumenty	dokument	k1gInPc1
učitelského	učitelský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
k	k	k7c3
antropologii	antropologie	k1gFnSc3
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstantinopolská	konstantinopolský	k2eAgFnSc1d1
synoda	synoda	k1gFnSc1
roku	rok	k1gInSc2
543	#num#	k4
v	v	k7c6
kán	kát	k5eAaImNgInS
<g/>
.	.	kIx.
1	#num#	k4
odsuzuje	odsuzovat	k5eAaImIp3nS
origenistickou	origenistický	k2eAgFnSc4d1
nauku	nauka	k1gFnSc4
o	o	k7c4
putování	putování	k1gNnSc4
duší	duše	k1gFnPc2
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
jsou	být	k5eAaImIp3nP
preexistující	preexistující	k2eAgFnPc1d1
duše	duše	k1gFnPc1
za	za	k7c4
trest	trest	k1gInSc4
uvězněny	uvězněn	k2eAgMnPc4d1
v	v	k7c6
tělech	tělo	k1gNnPc6
<g/>
:	:	kIx,
DH	DH	kA
403	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
synoda	synoda	k1gFnSc1
v	v	k7c6
Braze	Braza	k1gFnSc6
odmítá	odmítat	k5eAaImIp3nS
nauku	nauka	k1gFnSc4
o	o	k7c4
hříchem	hřích	k1gInSc7
zaviněného	zaviněný	k2eAgInSc2d1
pádu	pád	k1gInSc2
duší	duše	k1gFnPc2
do	do	k7c2
těl	tělo	k1gNnPc2
(	(	kIx(
<g/>
kán	kát	k5eAaImNgInS
<g/>
.	.	kIx.
6	#num#	k4
<g/>
;	;	kIx,
<g/>
DH	DH	kA
456	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
odsuzuje	odsuzovat	k5eAaImIp3nS
"	"	kIx"
<g/>
víru	víra	k1gFnSc4
v	v	k7c4
osud	osud	k1gInSc4
<g/>
"	"	kIx"
(	(	kIx(
<g/>
kán	kát	k5eAaImNgInS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
DH	DH	kA
459	#num#	k4
<g/>
,	,	kIx,
srov.	srov.	kA
283	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
konstantinopolský	konstantinopolský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
roku	rok	k1gInSc2
869	#num#	k4
<g/>
/	/	kIx~
<g/>
870	#num#	k4
říká	říkat	k5eAaImIp3nS
(	(	kIx(
<g/>
v	v	k7c6
opozici	opozice	k1gFnSc6
vůči	vůči	k7c3
Fotiovu	Fotiův	k2eAgNnSc3d1
pojetí	pojetí	k1gNnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
člověku	člověk	k1gMnSc6
je	být	k5eAaImIp3nS
jen	jen	k6eAd1
jediná	jediný	k2eAgFnSc1d1
duše	duše	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
utváří	utvářet	k5eAaImIp3nS,k5eAaPmIp3nS
celý	celý	k2eAgInSc4d1
duchovní	duchovní	k2eAgInSc4d1
a	a	k8xC
senzitivní	senzitivní	k2eAgInSc4d1
život	život	k1gInSc4
(	(	kIx(
<g/>
kán	kát	k5eAaImNgInS
<g/>
.	.	kIx.
10	#num#	k4
<g/>
,	,	kIx,
resp.	resp.	kA
lat.	lat.	k?
kán	kát	k5eAaImNgInS
<g/>
.	.	kIx.
11	#num#	k4
<g/>
:	:	kIx,
DH	DH	kA
657	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Viennský	Viennský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
roku	rok	k1gInSc2
1312	#num#	k4
odporuje	odporovat	k5eAaImIp3nS
františkánskému	františkánský	k2eAgMnSc3d1
spirituálovi	spirituál	k1gMnSc3
Petru	Petr	k1gMnSc3
Johannovi	Johann	k1gMnSc3
Olivimu	Olivim	k1gMnSc3
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgInSc2
se	se	k3xPyFc4
duchová	duchový	k2eAgFnSc1d1
duše	duše	k1gFnSc1
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
hmotným	hmotný	k2eAgInSc7d1
principem	princip	k1gInSc7
těla	tělo	k1gNnSc2
jen	jen	k6eAd1
zprostředkované	zprostředkovaný	k2eAgNnSc4d1
před	před	k7c4
duši	duše	k1gFnSc4
animální	animální	k2eAgFnSc1d1
a	a	k8xC
vegetativní	vegetativní	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
učí	učit	k5eAaImIp3nS
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Substance	substance	k1gFnSc1
rozumové	rozumový	k2eAgFnSc2d1
nebo	nebo	k8xC
rozumem	rozum	k1gInSc7
nadané	nadaný	k2eAgFnSc2d1
duše	duše	k1gFnSc2
je	být	k5eAaImIp3nS
vpravdě	vpravdě	k9
skrze	skrze	k?
sama	sám	k3xTgFnSc1
sebe	sebe	k3xPyFc4
a	a	k8xC
podstatně	podstatně	k6eAd1
bezprostředně	bezprostředně	k6eAd1
formou	forma	k1gFnSc7
těla	tělo	k1gNnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Fidei	Fidei	k1gNnSc1
catholicae	catholica	k1gInSc2
<g/>
"	"	kIx"
<g/>
:	:	kIx,
DH	DH	kA
902	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lateránský	lateránský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
roku	rok	k1gInSc2
1513	#num#	k4
v	v	k7c6
bule	bula	k1gFnSc6
"	"	kIx"
<g/>
Apostolici	Apostolice	k1gFnSc6
regiminis	regiminis	k1gFnPc2
<g/>
"	"	kIx"
odsuzuje	odsuzovat	k5eAaImIp3nS
"	"	kIx"
<g/>
averroisticky	averroisticky	k6eAd1
vykládaný	vykládaný	k2eAgInSc1d1
aristotelismus	aristotelismus	k1gInSc1
<g/>
"	"	kIx"
Pietra	Pietra	k1gFnSc1
Pomponazziho	Pomponazzi	k1gMnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgInSc2
je	být	k5eAaImIp3nS
rozumová	rozumový	k2eAgFnSc1d1
duše	duše	k1gFnSc1
člověka	člověk	k1gMnSc2
smrtelná	smrtelný	k2eAgNnPc4d1
a	a	k8xC
ve	v	k7c6
všech	všecek	k3xTgInPc6
lidech	lid	k1gInPc6
tatáž	týž	k3xTgFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozitivně	pozitivně	k6eAd1
se	se	k3xPyFc4
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
existuje	existovat	k5eAaImIp3nS
tolik	tolik	k4xDc1,k4yIc1
individuálních	individuální	k2eAgFnPc2d1
duší	duše	k1gFnPc2
jako	jako	k8xS,k8xC
individuálních	individuální	k2eAgNnPc2d1
lidských	lidský	k2eAgNnPc2d1
těl	tělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
individuální	individuální	k2eAgFnSc1d1
lidská	lidský	k2eAgFnSc1d1
duše	duše	k1gFnSc1
je	být	k5eAaImIp3nS
nesmrtelná	smrtelný	k2eNgFnSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
stvoření	stvoření	k1gNnSc2
Bohem	bůh	k1gMnSc7
určená	určený	k2eAgFnSc1d1
k	k	k7c3
osobnímu	osobní	k2eAgInSc3d1
věčnému	věčný	k2eAgInSc3d1
životu	život	k1gInSc3
s	s	k7c7
ním	on	k3xPp3gInSc7
(	(	kIx(
<g/>
DH	DH	kA
1440	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pius	Pius	k1gMnSc1
XII	XII	kA
<g/>
.	.	kIx.
trvá	trvat	k5eAaImIp3nS
na	na	k7c6
své	svůj	k3xOyFgFnSc6
encyklice	encyklika	k1gFnSc6
"	"	kIx"
<g/>
Humani	Humaň	k1gFnSc6
generis	generis	k1gFnPc2
<g/>
"	"	kIx"
z	z	k7c2
roku	rok	k1gInSc2
1950	#num#	k4
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
obecná	obecný	k2eAgFnSc1d1
evoluční	evoluční	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
neodporuje	odporovat	k5eNaImIp3nS
katolické	katolický	k2eAgFnSc3d1
víře	víra	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
do	do	k7c2
těla	tělo	k1gNnSc2
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
v	v	k7c6
kontinuitě	kontinuita	k1gFnSc6
se	s	k7c7
zvířecími	zvířecí	k2eAgFnPc7d1
formami	forma	k1gFnPc7
života	život	k1gInSc2
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Duše	duše	k1gFnSc1
<g/>
"	"	kIx"
však	však	k9
bezprostředně	bezprostředně	k6eAd1
stvořena	stvořit	k5eAaPmNgNnP
Bohem	bůh	k1gMnSc7
<g/>
,	,	kIx,
tj.	tj.	kA
k	k	k7c3
přirozenosti	přirozenost	k1gFnSc3
duše	duše	k1gFnSc2
patří	patřit	k5eAaImIp3nS
její	její	k3xOp3gFnSc1
osobní	osobní	k2eAgFnSc1d1
vztahovost	vztahovost	k1gFnSc1
k	k	k7c3
Bohu	bůh	k1gMnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
celkového	celkový	k2eAgInSc2d1
původu	původ	k1gInSc2
člověka	člověk	k1gMnSc2
z	z	k7c2
evoluce	evoluce	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
Boží	boží	k2eAgFnSc2d1
tvůrčí	tvůrčí	k2eAgFnSc2d1
vůle	vůle	k1gFnSc2
(	(	kIx(
<g/>
DH	DH	kA
3896	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pastorální	pastorální	k2eAgFnSc1d1
konstituce	konstituce	k1gFnSc1
"	"	kIx"
<g/>
Gaudium	gaudium	k1gNnSc1
et	et	k?
spes	spesa	k1gFnPc2
<g/>
"	"	kIx"
2	#num#	k4
<g/>
.	.	kIx.
vatikánského	vatikánský	k2eAgInSc2d1
koncilu	koncil	k1gInSc2
učí	učit	k5eAaImIp3nS
<g/>
:	:	kIx,
Člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
jednota	jednota	k1gFnSc1
duchovních	duchovní	k2eAgInPc2d1
a	a	k8xC
hmotných	hmotný	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
i	i	k9
tělesné	tělesný	k2eAgFnSc3d1
existenci	existence	k1gFnSc3
prokazovat	prokazovat	k5eAaImF
úctu	úcta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
niterností	niternost	k1gFnPc2
člověk	člověk	k1gMnSc1
přesahuje	přesahovat	k5eAaImIp3nS
univerzum	univerzum	k1gNnSc4
věcí	věc	k1gFnPc2
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
zaměřen	zaměřit	k5eAaPmNgInS
k	k	k7c3
Bohu	bůh	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc2
duše	duše	k1gFnSc2
je	být	k5eAaImIp3nS
duchová	duchový	k2eAgFnSc1d1
a	a	k8xC
nesmrtelná	smrtelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deklarace	deklarace	k1gFnSc1
o	o	k7c6
náboženské	náboženský	k2eAgFnSc6d1
svobodě	svoboda	k1gFnSc6
"	"	kIx"
<g/>
Dignitatis	Dignitatis	k1gInSc1
humanae	humana	k1gInSc2
<g/>
"	"	kIx"
2	#num#	k4
<g/>
.	.	kIx.
vatikánského	vatikánský	k2eAgInSc2d1
koncilu	koncil	k1gInSc2
říká	říkat	k5eAaImIp3nS
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
článku	článek	k1gInSc2
<g/>
:	:	kIx,
K	k	k7c3
důstojnosti	důstojnost	k1gFnSc3
lidské	lidský	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
patří	patřit	k5eAaImIp3nS
právo	právo	k1gNnSc4
na	na	k7c4
náboženskou	náboženský	k2eAgFnSc4d1
svobodu	svoboda	k1gFnSc4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
nárokem	nárok	k1gInSc7
vlastního	vlastní	k2eAgNnSc2d1
svědomí	svědomí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Muller	Muller	k1gMnSc1
<g/>
,	,	kIx,
G.L.	G.L.	k1gMnSc1
Dogmatika	dogmatik	k1gMnSc2
pro	pro	k7c4
studium	studium	k1gNnSc4
i	i	k8xC
pastoraci	pastorace	k1gFnSc4
<g/>
.	.	kIx.
z	z	k7c2
něm.	něm.	k?
přel	přít	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Frei	Free	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostelní	kostelní	k2eAgInSc1d1
Vydří	vydří	k2eAgInSc1d1
<g/>
:	:	kIx,
Karmelitánské	karmelitánský	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
888	#num#	k4
s.	s.	k?
Druhá	druhý	k4xOgFnSc1
kapitola	kapitola	k1gFnSc1
<g/>
:	:	kIx,
Člověk	člověk	k1gMnSc1
jako	jako	k8xS,k8xC
adresát	adresát	k1gMnSc1
Božího	boží	k2eAgNnSc2d1
sebesdělení	sebesdělení	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
104	#num#	k4
-	-	kIx~
107	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4059766-0	4059766-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85080322	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85080322	#num#	k4
</s>
