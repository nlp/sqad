<s>
Lepra	lepra	k1gFnSc1	lepra
(	(	kIx(	(
<g/>
též	též	k9	též
malomocenství	malomocenství	k1gNnSc1	malomocenství
či	či	k8xC	či
Hansenova	Hansenův	k2eAgFnSc1d1	Hansenova
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
infekční	infekční	k2eAgNnSc1d1	infekční
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
způsobované	způsobovaný	k2eAgNnSc1d1	způsobované
bakterií	bakterie	k1gFnSc7	bakterie
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
leprae	lepraat	k5eAaPmIp3nS	lepraat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
sklon	sklon	k1gInSc4	sklon
napadat	napadat	k5eAaImF	napadat
periferní	periferní	k2eAgInPc4d1	periferní
nervy	nerv	k1gInPc4	nerv
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
chladnější	chladný	k2eAgFnPc4d2	chladnější
oblasti	oblast	k1gFnPc4	oblast
těla	tělo	k1gNnSc2	tělo
-	-	kIx~	-
kůži	kůže	k1gFnSc3	kůže
a	a	k8xC	a
sliznice	sliznice	k1gFnSc2	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1954	[number]	k4	1954
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Světový	světový	k2eAgInSc4d1	světový
den	den	k1gInSc4	den
pomoci	pomoc	k1gFnSc2	pomoc
malomocným	malomocný	k1gMnPc3	malomocný
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
řecké	řecký	k2eAgNnSc1d1	řecké
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
lepra	lepra	k1gFnSc1	lepra
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
chorobu	choroba	k1gFnSc4	choroba
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
loupe	loupat	k5eAaImIp3nS	loupat
kůže	kůže	k1gFnSc1	kůže
(	(	kIx(	(
<g/>
od	od	k7c2	od
slovesa	sloveso	k1gNnSc2	sloveso
Λ	Λ	k?	Λ
-	-	kIx~	-
loupu	loupat	k5eAaImIp1nS	loupat
se	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
český	český	k2eAgInSc1d1	český
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
malomocenství	malomocenství	k1gNnSc1	malomocenství
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
poškození	poškození	k1gNnSc4	poškození
hmatu	hmat	k1gInSc2	hmat
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
choroba	choroba	k1gFnSc1	choroba
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
lepře	lepra	k1gFnSc6	lepra
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
cca	cca	kA	cca
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInPc1d3	nejstarší
tělesné	tělesný	k2eAgInPc1d1	tělesný
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
vykazující	vykazující	k2eAgFnSc2d1	vykazující
jasné	jasný	k2eAgFnSc2d1	jasná
známky	známka	k1gFnSc2	známka
tohoto	tento	k3xDgNnSc2	tento
onemocnění	onemocnění	k1gNnSc2	onemocnění
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
antických	antický	k2eAgFnPc6d1	antická
dobách	doba	k1gFnPc6	doba
najdeme	najít	k5eAaPmIp1nP	najít
zmínky	zmínka	k1gFnPc1	zmínka
zejména	zejména	k9	zejména
v	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
jako	jako	k9	jako
lepra	lepra	k1gFnSc1	lepra
překládá	překládat	k5eAaImIp3nS	překládat
jméno	jméno	k1gNnSc4	jméno
starozákonní	starozákonní	k2eAgFnSc2d1	starozákonní
choroby	choroba	k1gFnSc2	choroba
cara	car	k1gMnSc2	car
<g/>
'	'	kIx"	'
<g/>
at	at	k?	at
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgFnSc1d1	moderní
biblistika	biblistika	k1gFnSc1	biblistika
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
jinou	jiný	k2eAgFnSc4d1	jiná
kožní	kožní	k2eAgFnSc4d1	kožní
chorobu	choroba	k1gFnSc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgNnP	být
zavlečena	zavleknout	k5eAaPmNgNnP	zavleknout
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
hlavně	hlavně	k9	hlavně
za	za	k7c2	za
křižáckých	křižácký	k2eAgFnPc2d1	křižácká
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
významem	význam	k1gInSc7	význam
poněkud	poněkud	k6eAd1	poněkud
předčila	předčít	k5eAaPmAgFnS	předčít
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
způsobovaná	způsobovaný	k2eAgFnSc1d1	způsobovaná
příbuznými	příbuzný	k2eAgFnPc7d1	příbuzná
bakteriemi	bakterie	k1gFnPc7	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Lepra	lepra	k1gFnSc1	lepra
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
boží	boží	k2eAgInSc4d1	boží
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
malomocní	malomocnět	k5eAaImIp3nP	malomocnět
byli	být	k5eAaImAgMnP	být
soustředěni	soustředěn	k2eAgMnPc1d1	soustředěn
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
leprosáriích	leprosárium	k1gNnPc6	leprosárium
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
leprosárium	leprosárium	k1gNnSc1	leprosárium
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rumunském	rumunský	k2eAgNnSc6d1	rumunské
Tichileşti	Tichileşti	k1gNnSc6	Tichileşti
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Původce	původce	k1gMnSc1	původce
nákazy	nákaza	k1gFnSc2	nákaza
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
norský	norský	k2eAgMnSc1d1	norský
mikrobiolog	mikrobiolog	k1gMnSc1	mikrobiolog
Gerhard	Gerhard	k1gMnSc1	Gerhard
Armauer	Armauer	k1gMnSc1	Armauer
Hansen	Hansen	k2eAgMnSc1d1	Hansen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bývá	bývat	k5eAaImIp3nS	bývat
Mycobacterium	Mycobacterium	k1gNnSc1	Mycobacterium
leprae	lepraat	k5eAaPmIp3nS	lepraat
také	také	k9	také
označován	označovat	k5eAaImNgInS	označovat
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
objeviteli	objevitel	k1gMnSc6	objevitel
jako	jako	k9	jako
'	'	kIx"	'
<g/>
Hansenův	Hansenův	k2eAgInSc1d1	Hansenův
bacil	bacil	k1gInSc1	bacil
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
geneticky	geneticky	k6eAd1	geneticky
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
nezměnil	změnit	k5eNaPmAgInS	změnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
omezila	omezit	k5eAaPmAgFnS	omezit
se	se	k3xPyFc4	se
příčina	příčina	k1gFnSc1	příčina
šíření	šíření	k1gNnSc2	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přenašeče	přenašeč	k1gInPc4	přenašeč
této	tento	k3xDgFnSc2	tento
nemoci	nemoc	k1gFnSc2	nemoc
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
pásovec	pásovec	k1gMnSc1	pásovec
devítipásý	devítipásý	k2eAgMnSc1d1	devítipásý
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
často	často	k6eAd1	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
nakažlivá	nakažlivý	k2eAgFnSc1d1	nakažlivá
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
kožní	kožní	k2eAgFnSc2d1	kožní
formy	forma	k1gFnSc2	forma
vytvářející	vytvářející	k2eAgInPc4d1	vytvářející
vředy	vřed	k1gInPc7	vřed
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nakažlivá	nakažlivý	k2eAgFnSc1d1	nakažlivá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
rozvinutí	rozvinutí	k1gNnSc3	rozvinutí
její	její	k3xOp3gFnSc2	její
destruktivní	destruktivní	k2eAgFnSc2d1	destruktivní
formy	forma	k1gFnSc2	forma
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
napadá	napadat	k5eAaPmIp3nS	napadat
ve	v	k7c6	v
velkém	velký	k2eAgMnSc6d1	velký
Schwannovy	Schwannův	k2eAgFnPc4d1	Schwannova
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
makrofágy	makrofág	k1gInPc4	makrofág
v	v	k7c6	v
periferní	periferní	k2eAgFnSc6d1	periferní
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Ničí	ničit	k5eAaImIp3nS	ničit
lidský	lidský	k2eAgInSc4d1	lidský
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
znetvořeniny	znetvořenina	k1gFnPc4	znetvořenina
na	na	k7c6	na
končetinách	končetina	k1gFnPc6	končetina
a	a	k8xC	a
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Lví	lví	k2eAgInSc4d1	lví
obličej	obličej	k1gInSc4	obličej
<g/>
"	"	kIx"	"
-	-	kIx~	-
facies	facies	k1gFnSc1	facies
leontina	leontina	k1gFnSc1	leontina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lepra	lepra	k1gFnSc1	lepra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zejména	zejména	k9	zejména
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
(	(	kIx(	(
<g/>
Marana	Marana	k1gFnSc1	Marana
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Bangladéši	Bangladéš	k1gInSc3	Bangladéš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známé	známý	k2eAgInPc1d1	známý
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
i	i	k9	i
případy	případ	k1gInPc1	případ
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
tajeno	tajit	k5eAaImNgNnS	tajit
Ceauşescovým	Ceauşescův	k2eAgInSc7d1	Ceauşescův
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Výskytem	výskyt	k1gInSc7	výskyt
lepry	lepra	k1gFnSc2	lepra
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
ale	ale	k8xC	ale
i	i	k9	i
Havajské	havajský	k2eAgInPc1d1	havajský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
ostrov	ostrov	k1gInSc1	ostrov
Molokai	Molokai	k1gNnSc2	Molokai
(	(	kIx(	(
<g/>
Kalaupapa	Kalaupapa	k1gFnSc1	Kalaupapa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
ostrov	ostrov	k1gInSc1	ostrov
Spinalonga	Spinalong	k1gMnSc2	Spinalong
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
Egejském	egejský	k2eAgNnSc6d1	Egejské
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
WHO	WHO	kA	WHO
výrazně	výrazně	k6eAd1	výrazně
pokleslo	poklesnout	k5eAaPmAgNnS	poklesnout
množství	množství	k1gNnSc1	množství
lidí	člověk	k1gMnPc2	člověk
trpících	trpící	k2eAgInPc2d1	trpící
leprou	lepra	k1gFnSc7	lepra
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
asi	asi	k9	asi
z	z	k7c2	z
5,2	[number]	k4	5,2
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
přes	přes	k7c4	přes
0,8	[number]	k4	0,8
miliónů	milión	k4xCgInPc2	milión
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
asi	asi	k9	asi
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
případů	případ	k1gInPc2	případ
registrovaných	registrovaný	k2eAgInPc2d1	registrovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
nově	nově	k6eAd1	nově
nakažených	nakažený	k2eAgMnPc2d1	nakažený
nicméně	nicméně	k8xC	nicméně
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
registrováno	registrovat	k5eAaBmNgNnS	registrovat
kolem	kolem	k7c2	kolem
250	[number]	k4	250
000	[number]	k4	000
nových	nový	k2eAgInPc2d1	nový
případů	případ	k1gInPc2	případ
za	za	k7c4	za
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
případů	případ	k1gInPc2	případ
trvá	trvat	k5eAaImIp3nS	trvat
léčba	léčba	k1gFnSc1	léčba
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vyléčeny	vyléčen	k2eAgFnPc1d1	vyléčena
před	před	k7c7	před
sčítáním	sčítání	k1gNnSc7	sčítání
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
nakažených	nakažený	k2eAgFnPc2d1	nakažená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
léčbě	léčba	k1gFnSc3	léčba
malomocenství	malomocenství	k1gNnSc2	malomocenství
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
používají	používat	k5eAaImIp3nP	používat
specifická	specifický	k2eAgNnPc1d1	specifické
antibiotika	antibiotikum	k1gNnPc1	antibiotikum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rifampicin	Rifampicin	k2eAgMnSc1d1	Rifampicin
nebo	nebo	k8xC	nebo
Clofazimin	Clofazimin	k2eAgMnSc1d1	Clofazimin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
látka	látka	k1gFnSc1	látka
Thalidomid	Thalidomida	k1gFnPc2	Thalidomida
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
však	však	k9	však
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
při	při	k7c6	při
všech	všecek	k3xTgFnPc6	všecek
formách	forma	k1gFnPc6	forma
lepry	lepra	k1gFnSc2	lepra
postup	postup	k1gInSc4	postup
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
kombinace	kombinace	k1gFnSc2	kombinace
léků	lék	k1gInPc2	lék
včetně	včetně	k7c2	včetně
Dapsonu	Dapson	k1gInSc2	Dapson
(	(	kIx(	(
<g/>
např.	např.	kA	např.
kombinace	kombinace	k1gFnSc1	kombinace
Dapson	Dapson	k1gInSc1	Dapson
<g/>
,	,	kIx,	,
Clofazimin	Clofazimin	k2eAgInSc1d1	Clofazimin
a	a	k8xC	a
Rifampicin	Rifampicin	k2eAgInSc1d1	Rifampicin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
léčba	léčba	k1gFnSc1	léčba
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
až	až	k9	až
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
měsíců	měsíc	k1gInPc2	měsíc
či	či	k8xC	či
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
