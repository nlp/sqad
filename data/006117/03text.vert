<s>
Dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
takové	takový	k3xDgNnSc1	takový
dělení	dělení	k1gNnSc1	dělení
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
dělitel	dělitel	k1gMnSc1	dělitel
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapsáno	zapsat	k5eAaPmNgNnS	zapsat
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dělenec	dělenec	k1gInSc1	dělenec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
nemá	mít	k5eNaImIp3nS	mít
takové	takový	k3xDgNnSc1	takový
dělení	dělení	k1gNnSc1	dělení
smysl	smysl	k1gInSc1	smysl
–	–	k?	–
nula	nula	k1gFnSc1	nula
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
reálné	reálný	k2eAgNnSc1d1	reálné
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
nelze	lze	k6eNd1	lze
dělit	dělit	k5eAaImF	dělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
rozšířených	rozšířený	k2eAgNnPc2d1	rozšířené
o	o	k7c4	o
nekonečno	nekonečno	k1gNnSc4	nekonečno
je	být	k5eAaImIp3nS	být
definováno	definován	k2eAgNnSc1d1	definováno
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
nenulové	nulový	k2eNgInPc4d1	nenulový
dělence	dělenec	k1gInPc4	dělenec
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
infty	inft	k1gInPc7	inft
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
v	v	k7c6	v
plovoucí	plovoucí	k2eAgFnSc6d1	plovoucí
řádové	řádový	k2eAgFnSc6d1	řádová
čárce	čárka	k1gFnSc6	čárka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výsledkem	výsledek	k1gInSc7	výsledek
speciální	speciální	k2eAgFnSc1d1	speciální
hodnota	hodnota	k1gFnSc1	hodnota
not	nota	k1gFnPc2	nota
a	a	k8xC	a
number	numbra	k1gFnPc2	numbra
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
číslo	číslo	k1gNnSc1	číslo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
rozdělování	rozdělování	k1gNnSc4	rozdělování
množiny	množina	k1gFnSc2	množina
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
máme	mít	k5eAaImIp1nP	mít
deset	deset	k4xCc4	deset
kvádrů	kvádr	k1gInPc2	kvádr
a	a	k8xC	a
rozdělíme	rozdělit	k5eAaPmIp1nP	rozdělit
je	on	k3xPp3gNnSc4	on
na	na	k7c6	na
skupiny	skupina	k1gFnPc4	skupina
po	po	k7c4	po
pěti	pět	k4xCc3	pět
<g/>
,	,	kIx,	,
dostaneme	dostat	k5eAaPmIp1nP	dostat
dvě	dva	k4xCgFnPc4	dva
stejně	stejně	k9	stejně
velké	velký	k2eAgFnSc3d1	velká
části	část	k1gFnSc3	část
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
ukázka	ukázka	k1gFnSc1	ukázka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dělitel	dělitel	k1gInSc1	dělitel
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
kvádrů	kvádr	k1gInPc2	kvádr
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
části	část	k1gFnSc6	část
a	a	k8xC	a
výsledek	výsledek	k1gInSc1	výsledek
dělení	dělení	k1gNnSc2	dělení
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
mám	mít	k5eAaImIp1nS	mít
stejné	stejný	k2eAgFnSc3d1	stejná
části	část	k1gFnSc3	část
po	po	k7c6	po
5	[number]	k4	5
kusech	kus	k1gInPc6	kus
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc1	kolik
takových	takový	k3xDgFnPc2	takový
částí	část	k1gFnPc2	část
musím	muset	k5eAaImIp1nS	muset
dát	dát	k5eAaPmF	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
dostal	dostat	k5eAaPmAgMnS	dostat
10	[number]	k4	10
kusů	kus	k1gInPc2	kus
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
aplikujeme	aplikovat	k5eAaBmIp1nP	aplikovat
na	na	k7c6	na
dělení	dělení	k1gNnSc6	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
otázka	otázka	k1gFnSc1	otázka
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
mám	mít	k5eAaImIp1nS	mít
stejné	stejný	k2eAgFnSc3d1	stejná
části	část	k1gFnSc3	část
po	po	k7c6	po
0	[number]	k4	0
kusech	kus	k1gInPc6	kus
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
takových	takový	k3xDgFnPc2	takový
částí	část	k1gFnPc2	část
musím	muset	k5eAaImIp1nS	muset
dát	dát	k5eAaPmF	dát
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
dostal	dostat	k5eAaPmAgMnS	dostat
10	[number]	k4	10
kusů	kus	k1gInPc2	kus
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
nedává	dávat	k5eNaImIp3nS	dávat
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přičítáním	přičítání	k1gNnSc7	přičítání
částí	část	k1gFnPc2	část
o	o	k7c6	o
0	[number]	k4	0
prvcích	prvek	k1gInPc6	prvek
se	se	k3xPyFc4	se
deset	deset	k4xCc1	deset
kusů	kus	k1gInPc2	kus
nikdy	nikdy	k6eAd1	nikdy
nezíská	získat	k5eNaPmIp3nS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
popsat	popsat	k5eAaPmF	popsat
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
opakované	opakovaný	k2eAgNnSc1d1	opakované
odečítání	odečítání	k1gNnSc1	odečítání
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
<g/>
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
vydělit	vydělit	k5eAaPmF	vydělit
číslo	číslo	k1gNnSc4	číslo
13	[number]	k4	13
pěti	pět	k4xCc3	pět
<g/>
,	,	kIx,	,
odečteme	odečíst	k5eAaPmIp1nP	odečíst
od	od	k7c2	od
13	[number]	k4	13
dvakrát	dvakrát	k6eAd1	dvakrát
5	[number]	k4	5
a	a	k8xC	a
dostaneme	dostat	k5eAaPmIp1nP	dostat
zbytek	zbytek	k1gInSc4	zbytek
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dělitel	dělitel	k1gInSc1	dělitel
se	se	k3xPyFc4	se
odečítá	odečítat	k5eAaImIp3nS	odečítat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
zbytek	zbytek	k1gInSc1	zbytek
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
dělitel	dělitel	k1gInSc1	dělitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dělitel	dělitel	k1gMnSc1	dělitel
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
při	při	k7c6	při
opakovaném	opakovaný	k2eAgNnSc6d1	opakované
odečítání	odečítání	k1gNnSc6	odečítání
nuly	nula	k1gFnSc2	nula
od	od	k7c2	od
dělence	dělenec	k1gInSc2	dělenec
nikdy	nikdy	k6eAd1	nikdy
nedosáhneme	dosáhnout	k5eNaPmIp1nP	dosáhnout
zbytku	zbytek	k1gInSc3	zbytek
menšího	malý	k2eAgNnSc2d2	menší
než	než	k8xS	než
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
<s>
Brahmasphutasiddhanta	Brahmasphutasiddhanta	k1gFnSc1	Brahmasphutasiddhanta
od	od	k7c2	od
Brahmagupty	Brahmagupta	k1gFnSc2	Brahmagupta
(	(	kIx(	(
<g/>
598	[number]	k4	598
<g/>
–	–	k?	–
<g/>
668	[number]	k4	668
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
považoval	považovat	k5eAaImAgInS	považovat
nulu	nula	k1gFnSc4	nula
za	za	k7c4	za
normální	normální	k2eAgNnSc4d1	normální
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
definoval	definovat	k5eAaBmAgInS	definovat
operace	operace	k1gFnPc1	operace
ji	on	k3xPp3gFnSc4	on
obsahující	obsahující	k2eAgFnSc4d1	obsahující
<g/>
.	.	kIx.	.
</s>
<s>
Autorovi	autor	k1gMnSc3	autor
se	se	k3xPyFc4	se
ale	ale	k9	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
definice	definice	k1gFnSc1	definice
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
absurdním	absurdní	k2eAgInPc3d1	absurdní
algebraickým	algebraický	k2eAgInPc3d1	algebraický
závěrům	závěr	k1gInPc3	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Brahmagupta	Brahmagupta	k1gFnSc1	Brahmagupta
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
Kladné	kladný	k2eAgNnSc4d1	kladné
nebo	nebo	k8xC	nebo
záporné	záporný	k2eAgNnSc4d1	záporné
číslo	číslo	k1gNnSc4	číslo
dělené	dělený	k2eAgNnSc4d1	dělené
nulou	nula	k1gFnSc7	nula
je	být	k5eAaImIp3nS	být
zlomek	zlomek	k1gInSc1	zlomek
se	s	k7c7	s
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
<s>
Nula	nula	k1gFnSc1	nula
dělená	dělený	k2eAgFnSc1d1	dělená
záporným	záporný	k2eAgNnSc7d1	záporné
nebo	nebo	k8xC	nebo
kladným	kladný	k2eAgNnSc7d1	kladné
číslem	číslo	k1gNnSc7	číslo
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
jako	jako	k8xC	jako
zlomek	zlomek	k1gInSc1	zlomek
s	s	k7c7	s
čitatelem	čitatel	k1gInSc7	čitatel
nula	nula	k1gFnSc1	nula
a	a	k8xC	a
konečným	konečný	k2eAgNnSc7d1	konečné
množstvím	množství	k1gNnSc7	množství
jako	jako	k8xS	jako
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
<g/>
.	.	kIx.	.
</s>
<s>
Nula	nula	k1gFnSc1	nula
dělená	dělený	k2eAgFnSc1d1	dělená
nulou	nula	k1gFnSc7	nula
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
<g/>
.	.	kIx.	.
</s>
<s>
Mahavira	Mahavira	k6eAd1	Mahavira
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
830	[number]	k4	830
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
opravit	opravit	k5eAaPmF	opravit
Brahmaguptovu	Brahmaguptův	k2eAgFnSc4d1	Brahmaguptův
chybu	chyba	k1gFnSc4	chyba
<g/>
:	:	kIx,	:
Číslo	číslo	k1gNnSc4	číslo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nezměněno	změněn	k2eNgNnSc1d1	nezměněno
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
děleno	dělen	k2eAgNnSc4d1	děleno
nulou	nula	k1gFnSc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Bháskara	Bháskara	k1gFnSc1	Bháskara
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
problém	problém	k1gInSc4	problém
vyřešit	vyřešit	k5eAaPmF	vyřešit
definováním	definování	k1gNnSc7	definování
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
textstyle	textstyl	k1gInSc5	textstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
dává	dávat	k5eAaImIp3nS	dávat
určitý	určitý	k2eAgInSc4d1	určitý
smysl	smysl	k1gInSc4	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
paradoxům	paradox	k1gInPc3	paradox
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nezachází	zacházet	k5eNaImIp3nS	zacházet
opatrně	opatrně	k6eAd1	opatrně
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
textstyle	textstyl	k1gInSc5	textstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
při	při	k7c6	při
odstranění	odstranění	k1gNnSc6	odstranění
zlomků	zlomek	k1gInPc2	zlomek
vycházelo	vycházet	k5eAaImAgNnS	vycházet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nesmysl	nesmysl	k1gInSc1	nesmysl
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgInSc7d1	přirozený
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
vyložit	vyložit	k5eAaPmF	vyložit
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
definovat	definovat	k5eAaBmF	definovat
dělení	dělení	k1gNnSc4	dělení
pomocí	pomocí	k7c2	pomocí
jiných	jiný	k2eAgFnPc2d1	jiná
aritmetických	aritmetický	k2eAgFnPc2d1	aritmetická
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
standardních	standardní	k2eAgFnPc2d1	standardní
pravidel	pravidlo	k1gNnPc2	pravidlo
aritmetiky	aritmetika	k1gFnSc2	aritmetika
není	být	k5eNaImIp3nS	být
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
přirozených	přirozený	k2eAgFnPc2d1	přirozená
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
celých	celý	k2eAgNnPc2d1	celé
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
racionálních	racionální	k2eAgNnPc2d1	racionální
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
nerozšířených	rozšířený	k2eNgNnPc2d1	nerozšířené
o	o	k7c4	o
nekonečno	nekonečno	k1gNnSc4	nekonečno
<g/>
)	)	kIx)	)
definováno	definovat	k5eAaBmNgNnS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
dělení	dělení	k1gNnSc1	dělení
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
jako	jako	k8xC	jako
inverzní	inverzní	k2eAgFnSc1d1	inverzní
operace	operace	k1gFnSc1	operace
k	k	k7c3	k
operaci	operace	k1gFnSc3	operace
násobení	násobení	k1gNnSc2	násobení
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
b	b	k?	b
je	být	k5eAaImIp3nS	být
kořenem	kořen	k1gInSc7	kořen
x	x	k?	x
rovnice	rovnice	k1gFnSc1	rovnice
bx	bx	k?	bx
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
kdykoliv	kdykoliv	k6eAd1	kdykoliv
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
hodnota	hodnota	k1gFnSc1	hodnota
právě	právě	k9	právě
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
není	být	k5eNaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
definovaná	definovaný	k2eAgFnSc1d1	definovaná
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
b	b	k?	b
=	=	kIx~	=
0	[number]	k4	0
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rovnice	rovnice	k1gFnSc1	rovnice
bx	bx	k?	bx
=	=	kIx~	=
a	a	k8xC	a
napsána	napsán	k2eAgFnSc1d1	napsána
jako	jako	k8xS	jako
0	[number]	k4	0
<g/>
x	x	k?	x
=	=	kIx~	=
a	a	k8xC	a
nebo	nebo	k8xC	nebo
prostě	prostě	k9	prostě
0	[number]	k4	0
=	=	kIx~	=
a.	a.	k?	a.
Proto	proto	k8xC	proto
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
rovnice	rovnice	k1gFnSc2	rovnice
bx	bx	k?	bx
=	=	kIx~	=
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgNnSc4	žádný
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
a	a	k8xC	a
<g/>
"	"	kIx"	"
nerovná	rovný	k2eNgFnSc1d1	nerovná
0	[number]	k4	0
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c4	mnoho
řešení	řešení	k1gNnPc2	řešení
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
a	a	k8xC	a
rovná	rovnat	k5eAaImIp3nS	rovnat
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
případě	případ	k1gInSc6	případ
tedy	tedy	k9	tedy
rovnice	rovnice	k1gFnSc1	rovnice
nemá	mít	k5eNaImIp3nS	mít
právě	právě	k9	právě
jedno	jeden	k4xCgNnSc4	jeden
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
a	a	k8xC	a
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
b	b	k?	b
není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
definované	definovaný	k2eAgNnSc1d1	definované
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
mít	mít	k5eAaImF	mít
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
dělení	dělení	k1gNnSc2	dělení
nulou	nula	k1gFnSc7	nula
v	v	k7c4	v
proměnné	proměnná	k1gFnPc4	proměnná
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
falešnému	falešný	k2eAgInSc3d1	falešný
důkazu	důkaz	k1gInSc3	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
2	[number]	k4	2
=	=	kIx~	=
1	[number]	k4	1
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
reálné	reálný	k2eAgNnSc4d1	reálné
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x	-x	k?	-x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
-x	-x	k?	-x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Rozložíme	rozložit	k5eAaPmIp1nP	rozložit
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
dvěma	dva	k4xCgInPc7	dva
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
x	x	k?	x
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
(	(	kIx(	(
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
−	−	k?	−
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x-x	x	k?	x-x
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
x-x	x	k?	x-x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Vydělíme	vydělit	k5eAaPmIp1nP	vydělit
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
výrazem	výraz	k1gInSc7	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
x	x	k?	x
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x-x	x	k?	x-x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
−	−	k?	−
x	x	k?	x
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x-x	x	k?	x-x
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
1	[number]	k4	1
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
+	+	kIx~	+
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
x	x	k?	x
(	(	kIx(	(
1	[number]	k4	1
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
x	x	k?	x
=	=	kIx~	=
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
:	:	kIx,	:
Protože	protože	k8xS	protože
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
}	}	kIx)	}
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
jakýchkoliv	jakýkoliv	k3yIgFnPc2	jakýkoliv
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
,	,	kIx,	,
dosadíme	dosadit	k5eAaPmIp1nP	dosadit
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
x	x	k?	x
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
:	:	kIx,	:
Chybou	chyba	k1gFnSc7	chyba
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
/	/	kIx~	/
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
−	−	k?	−
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
x-x	x	k?	x-x
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
x-x	x	k?	x-x
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
jiná	jiný	k2eAgFnSc1d1	jiná
hodnota	hodnota	k1gFnSc1	hodnota
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
k	k	k7c3	k
0	[number]	k4	0
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
podobným	podobný	k2eAgInPc3d1	podobný
nesmyslům	nesmysl	k1gInPc3	nesmysl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
vypadá	vypadat	k5eAaPmIp3nS	vypadat
možné	možný	k2eAgNnSc1d1	možné
definovat	definovat	k5eAaBmF	definovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
jako	jako	k8xS	jako
limitu	limit	k1gInSc2	limit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}}	}}}	k?	}}}
pro	pro	k7c4	pro
b	b	k?	b
jdoucí	jdoucí	k2eAgInPc1d1	jdoucí
k	k	k7c3	k
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgFnPc4	každý
kladné	kladný	k2eAgFnPc4d1	kladná
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
záporné	záporný	k2eAgNnSc4d1	záporné
a	a	k8xC	a
platí	platit	k5eAaImIp3nS	platit
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
−	−	k?	−
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
b	b	k?	b
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
:	:	kIx,	:
Proto	proto	k8xC	proto
můžeme	moct	k5eAaImIp1nP	moct
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
definování	definování	k1gNnSc6	definování
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
jako	jako	k8xC	jako
+	+	kIx~	+
<g/>
∞	∞	k?	∞
pro	pro	k7c4	pro
kladné	kladný	k2eAgFnPc4d1	kladná
a	a	k8xC	a
a	a	k8xC	a
-	-	kIx~	-
<g/>
∞	∞	k?	∞
pro	pro	k7c4	pro
záporné	záporný	k2eAgNnSc4d1	záporné
a.	a.	k?	a.
Nicméně	nicméně	k8xC	nicméně
tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
nevyhovující	vyhovující	k2eNgFnSc1d1	nevyhovující
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgNnSc4	prvý
<g/>
:	:	kIx,	:
Kladné	kladný	k2eAgNnSc4d1	kladné
a	a	k8xC	a
záporné	záporný	k2eAgNnSc4d1	záporné
nekonečno	nekonečno	k1gNnSc4	nekonečno
nejsou	být	k5eNaImIp3nP	být
reálná	reálný	k2eAgNnPc4d1	reálné
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Takže	takže	k9	takže
pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
nedefinovali	definovat	k5eNaBmAgMnP	definovat
jsme	být	k5eAaImIp1nP	být
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nS	by
dávalo	dávat	k5eAaImAgNnS	dávat
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
chceme	chtít	k5eAaImIp1nP	chtít
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
definicí	definice	k1gFnSc7	definice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
rozšířit	rozšířit	k5eAaPmF	rozšířit
obor	obor	k1gInSc4	obor
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
<g/>
:	:	kIx,	:
Braní	braní	k1gNnPc4	braní
limity	limita	k1gFnSc2	limita
zprava	zprava	k6eAd1	zprava
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
libovolné	libovolný	k2eAgNnSc1d1	libovolné
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
vzít	vzít	k5eAaPmF	vzít
limitu	limita	k1gFnSc4	limita
zleva	zleva	k6eAd1	zleva
a	a	k8xC	a
definovat	definovat	k5eAaBmF	definovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
jako	jako	k8xC	jako
-	-	kIx~	-
<g/>
∞	∞	k?	∞
pro	pro	k7c4	pro
kladné	kladný	k2eAgFnPc4d1	kladná
a	a	k8xC	a
a	a	k8xC	a
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
∞	∞	k?	∞
pro	pro	k7c4	pro
záporné	záporný	k2eAgNnSc4d1	záporné
a.	a.	k?	a.
Toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
na	na	k7c4	na
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∞	∞	k?	∞
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
−	−	k?	−
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}	}	kIx)	}
,	,	kIx,	,
což	což	k3yRnSc1	což
nedává	dávat	k5eNaImIp3nS	dávat
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgNnSc7d1	jediné
fungujícím	fungující	k2eAgNnSc7d1	fungující
rozšířením	rozšíření	k1gNnSc7	rozšíření
je	být	k5eAaImIp3nS	být
zavedení	zavedení	k1gNnSc1	zavedení
nekonečna	nekonečno	k1gNnSc2	nekonečno
bez	bez	k7c2	bez
znaménka	znaménko	k1gNnSc2	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
zřejmá	zřejmý	k2eAgFnSc1d1	zřejmá
definice	definice	k1gFnSc1	definice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
limit	limit	k1gInSc4	limit
<g/>
.	.	kIx.	.
</s>
<s>
Limita	limita	k1gFnSc1	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
(	(	kIx(	(
a	a	k8xC	a
,	,	kIx,	,
b	b	k?	b
)	)	kIx)	)
→	→	k?	→
(	(	kIx(	(
0	[number]	k4	0
,	,	kIx,	,
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
0,0	[number]	k4	0,0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
a	a	k8xC	a
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
b	b	k?	b
<g/>
}}	}}	k?	}}
:	:	kIx,	:
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Limita	limita	k1gFnSc1	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
i	i	k9	i
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
blíží	blížit	k5eAaImIp3nS	blížit
0	[number]	k4	0
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
x	x	k?	x
blíží	blížit	k5eAaImIp3nS	blížit
0	[number]	k4	0
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
konvergovat	konvergovat	k5eAaImF	konvergovat
k	k	k7c3	k
jakékoliv	jakýkoliv	k3yIgFnSc3	jakýkoliv
hodnotě	hodnota	k1gFnSc3	hodnota
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nS	muset
konvergovat	konvergovat	k5eAaImF	konvergovat
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Hospitalovo	Hospitalův	k2eAgNnSc1d1	Hospitalovo
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Standard	standard	k1gInSc1	standard
IEEE	IEEE	kA	IEEE
pro	pro	k7c4	pro
dvojkovou	dvojkový	k2eAgFnSc4d1	dvojková
aritmetiku	aritmetika	k1gFnSc4	aritmetika
v	v	k7c6	v
plovoucí	plovoucí	k2eAgFnSc6d1	plovoucí
řádové	řádový	k2eAgFnSc6d1	řádová
čárce	čárka	k1gFnSc6	čárka
<g/>
,	,	kIx,	,
podporovaný	podporovaný	k2eAgInSc1d1	podporovaný
skoro	skoro	k6eAd1	skoro
všemi	všecek	k3xTgInPc7	všecek
moderními	moderní	k2eAgInPc7d1	moderní
procesory	procesor	k1gInPc7	procesor
<g/>
,	,	kIx,	,
specifikuje	specifikovat	k5eAaBmIp3nS	specifikovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
každá	každý	k3xTgFnSc1	každý
operace	operace	k1gFnSc1	operace
v	v	k7c6	v
plovoucí	plovoucí	k2eAgFnSc6d1	plovoucí
řádové	řádový	k2eAgFnSc6d1	řádová
čárce	čárka	k1gFnSc6	čárka
včetně	včetně	k7c2	včetně
dělení	dělení	k1gNnSc2	dělení
nulou	nula	k1gFnSc7	nula
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
definovaný	definovaný	k2eAgInSc1d1	definovaný
výsledek	výsledek	k1gInSc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
IEEE	IEEE	kA	IEEE
754	[number]	k4	754
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
÷	÷	k?	÷
0	[number]	k4	0
kladné	kladný	k2eAgNnSc1d1	kladné
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
kladné	kladný	k2eAgNnSc1d1	kladné
<g/>
,	,	kIx,	,
záporné	záporný	k2eAgNnSc1d1	záporné
nekonečno	nekonečno	k1gNnSc1	nekonečno
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
záporné	záporný	k2eAgNnSc1d1	záporné
<g/>
,	,	kIx,	,
a	a	k8xC	a
NaN	NaN	k1gMnSc1	NaN
(	(	kIx(	(
<g/>
not	nota	k1gFnPc2	nota
a	a	k8xC	a
number	numbra	k1gFnPc2	numbra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
a	a	k8xC	a
=	=	kIx~	=
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Znaménka	znaménko	k1gNnPc1	znaménko
nekonečen	nekonečno	k1gNnPc2	nekonečno
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
IEEE	IEEE	kA	IEEE
754	[number]	k4	754
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
nuly	nula	k1gFnPc1	nula
<g/>
,	,	kIx,	,
kladná	kladný	k2eAgFnSc1d1	kladná
a	a	k8xC	a
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
celočíselným	celočíselný	k2eAgNnSc7d1	celočíselné
dělením	dělení	k1gNnSc7	dělení
nulou	nula	k1gFnSc7	nula
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
zachází	zacházet	k5eAaImIp3nS	zacházet
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neexistuje	existovat	k5eNaImIp3nS	existovat
celočíselná	celočíselný	k2eAgFnSc1d1	celočíselná
reprezentace	reprezentace	k1gFnSc1	reprezentace
takového	takový	k3xDgInSc2	takový
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
procesory	procesor	k1gInPc1	procesor
vygenerují	vygenerovat	k5eAaPmIp3nP	vygenerovat
výjimku	výjimka	k1gFnSc4	výjimka
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c6	o
dělení	dělení	k1gNnSc6	dělení
nulou	nula	k1gFnSc7	nula
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnSc3d1	jiná
prostě	prostě	k9	prostě
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
a	a	k8xC	a
vygenerují	vygenerovat	k5eAaPmIp3nP	vygenerovat
nesprávný	správný	k2eNgInSc4d1	nesprávný
výsledek	výsledek	k1gInSc4	výsledek
dělení	dělení	k1gNnSc2	dělení
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nulu	nula	k1gFnSc4	nula
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nesprávným	správný	k2eNgInPc3d1	nesprávný
algebraickým	algebraický	k2eAgInPc3d1	algebraický
výsledkům	výsledek	k1gInPc3	výsledek
při	při	k7c6	při
přiřazování	přiřazování	k1gNnSc6	přiřazování
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
výsledku	výsledek	k1gInSc2	výsledek
dělení	dělení	k1gNnSc2	dělení
nulou	nula	k1gFnSc7	nula
mnoho	mnoho	k6eAd1	mnoho
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
těch	ten	k3xDgInPc2	ten
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
kalkulačkách	kalkulačka	k1gFnPc6	kalkulačka
<g/>
)	)	kIx)	)
výslovně	výslovně	k6eAd1	výslovně
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
provedení	provedení	k1gNnSc3	provedení
takové	takový	k3xDgFnSc2	takový
operace	operace	k1gFnSc2	operace
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
předčasně	předčasně	k6eAd1	předčasně
ukončit	ukončit	k5eAaPmF	ukončit
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
nulou	nula	k1gFnSc7	nula
pokouší	pokoušet	k5eAaImIp3nP	pokoušet
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
chybou	chyba	k1gFnSc7	chyba
"	"	kIx"	"
<g/>
dělení	dělení	k1gNnSc1	dělení
nulou	nula	k1gFnSc7	nula
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
programy	program	k1gInPc1	program
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
používají	používat	k5eAaImIp3nP	používat
aritmetikou	aritmetika	k1gFnSc7	aritmetika
v	v	k7c6	v
pevné	pevný	k2eAgFnSc3d1	pevná
řádové	řádový	k2eAgFnSc3d1	řádová
čárce	čárka	k1gFnSc3	čárka
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
žádný	žádný	k3yNgInSc4	žádný
speciální	speciální	k2eAgInSc4d1	speciální
hardware	hardware	k1gInSc4	hardware
na	na	k7c4	na
operace	operace	k1gFnPc4	operace
v	v	k7c6	v
plovoucí	plovoucí	k2eAgFnSc6d1	plovoucí
řádové	řádový	k2eAgFnSc6d1	řádová
čárce	čárka	k1gFnSc6	čárka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
standard	standard	k1gInSc4	standard
IEEE	IEEE	kA	IEEE
<g/>
,	,	kIx,	,
když	když	k8xS	když
používají	používat	k5eAaImIp3nP	používat
velká	velký	k2eAgNnPc4d1	velké
kladná	kladný	k2eAgNnPc4d1	kladné
nebo	nebo	k8xC	nebo
záporná	záporný	k2eAgNnPc4d1	záporné
čísla	číslo	k1gNnPc4	číslo
pro	pro	k7c4	pro
aproximaci	aproximace	k1gFnSc4	aproximace
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
vyústí	vyústit	k5eAaPmIp3nS	vyústit
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
nulou	nula	k1gFnSc7	nula
v	v	k7c4	v
nedefinované	definovaný	k2eNgNnSc4d1	nedefinované
chování	chování	k1gNnSc4	chování
<g/>
.	.	kIx.	.
</s>
<s>
NaN	NaN	k?	NaN
NULL	NULL	kA	NULL
lineární	lineární	k2eAgFnSc1d1	lineární
lomená	lomený	k2eAgFnSc1d1	lomená
funkce	funkce	k1gFnSc1	funkce
</s>
