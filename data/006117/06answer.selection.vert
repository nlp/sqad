<s>
Brahmasphutasiddhanta	Brahmasphutasiddhanta	k1gFnSc1	Brahmasphutasiddhanta
od	od	k7c2	od
Brahmagupty	Brahmagupta	k1gFnSc2	Brahmagupta
(	(	kIx(	(
<g/>
598	[number]	k4	598
<g/>
–	–	k?	–
<g/>
668	[number]	k4	668
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc1	první
známý	známý	k2eAgInSc1d1	známý
spis	spis	k1gInSc1	spis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
považoval	považovat	k5eAaImAgInS	považovat
nulu	nula	k1gFnSc4	nula
za	za	k7c4	za
normální	normální	k2eAgNnSc4d1	normální
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
definoval	definovat	k5eAaBmAgInS	definovat
operace	operace	k1gFnPc1	operace
ji	on	k3xPp3gFnSc4	on
obsahující	obsahující	k2eAgFnSc4d1	obsahující
<g/>
.	.	kIx.	.
</s>
