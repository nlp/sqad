<s>
Francovka	francovka	k1gFnSc1	francovka
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
spiritus	spiritus	k1gInSc1	spiritus
vini	vin	k1gFnSc2	vin
gallici	gallice	k1gFnSc4	gallice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ethanolový	ethanolový	k2eAgInSc1d1	ethanolový
roztok	roztok	k1gInSc1	roztok
různých	různý	k2eAgFnPc2d1	různá
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mentolu	mentol	k1gInSc2	mentol
<g/>
,	,	kIx,	,
kafru	kafr	k1gInSc2	kafr
a	a	k8xC	a
různých	různý	k2eAgInPc2d1	různý
bylinných	bylinný	k2eAgInPc2d1	bylinný
extraktů	extrakt	k1gInPc2	extrakt
a	a	k8xC	a
vonných	vonný	k2eAgFnPc2d1	vonná
přísad	přísada	k1gFnPc2	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
zevně	zevně	k6eAd1	zevně
pro	pro	k7c4	pro
potírání	potírání	k1gNnSc4	potírání
a	a	k8xC	a
masáže	masáž	k1gFnPc4	masáž
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
do	do	k7c2	do
koupelí	koupel	k1gFnPc2	koupel
a	a	k8xC	a
obkladů	obklad	k1gInPc2	obklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vnitřně	vnitřně	k6eAd1	vnitřně
inhalací	inhalace	k1gFnPc2	inhalace
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kapkách	kapka	k1gFnPc6	kapka
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
prokrvení	prokrvení	k1gNnSc1	prokrvení
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
pomoci	pomoct	k5eAaPmF	pomoct
zejména	zejména	k9	zejména
při	při	k7c6	při
svalové	svalový	k2eAgFnSc6d1	svalová
únavě	únava	k1gFnSc6	únava
(	(	kIx(	(
<g/>
po	po	k7c6	po
zátěži	zátěž	k1gFnSc6	zátěž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používání	používání	k1gNnSc1	používání
má	mít	k5eAaImIp3nS	mít
ale	ale	k8xC	ale
i	i	k9	i
své	svůj	k3xOyFgInPc4	svůj
negativní	negativní	k2eAgInPc4d1	negativní
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
obsažený	obsažený	k2eAgInSc1d1	obsažený
ve	v	k7c6	v
francovce	francovka	k1gFnSc6	francovka
vysušuje	vysušovat	k5eAaImIp3nS	vysušovat
kůži	kůže	k1gFnSc4	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
suchou	suchý	k2eAgFnSc4d1	suchá
kůži	kůže	k1gFnSc4	kůže
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
používání	používání	k1gNnSc1	používání
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
;	;	kIx,	;
při	při	k7c6	při
profesionální	profesionální	k2eAgFnSc6d1	profesionální
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
seniory	senior	k1gMnPc4	senior
se	se	k3xPyFc4	se
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Francovka	francovka	k1gFnSc1	francovka
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
a	a	k8xC	a
bezbarvé	bezbarvý	k2eAgFnSc6d1	bezbarvá
variantě	varianta	k1gFnSc6	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
výrobci	výrobce	k1gMnPc1	výrobce
bývali	bývat	k5eAaImAgMnP	bývat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
francovka	francovka	k1gFnSc1	francovka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
francovka	francovka	k1gFnSc1	francovka
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Alpa	alpa	k1gFnSc1	alpa
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
čirá	čirý	k2eAgFnSc1d1	čirá
Alpa	alpa	k1gFnSc1	alpa
má	mít	k5eAaImIp3nS	mít
dle	dle	k7c2	dle
etikety	etiketa	k1gFnSc2	etiketa
následující	následující	k2eAgNnSc4d1	následující
složení	složení	k1gNnSc4	složení
<g/>
:	:	kIx,	:
60	[number]	k4	60
<g/>
%	%	kIx~	%
denaturovaný	denaturovaný	k2eAgInSc1d1	denaturovaný
Ethanol	ethanol	k1gInSc1	ethanol
(	(	kIx(	(
<g/>
denaturační	denaturační	k2eAgNnSc1d1	denaturační
činidlo	činidlo	k1gNnSc1	činidlo
neuvedeno	uveden	k2eNgNnSc1d1	neuvedeno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mentol	mentol	k1gInSc1	mentol
<g/>
,	,	kIx,	,
Ethylacetát	Ethylacetát	k1gInSc1	Ethylacetát
<g/>
,	,	kIx,	,
Ethylformiát	Ethylformiát	k1gInSc1	Ethylformiát
<g/>
,	,	kIx,	,
Parfém	parfém	k1gInSc1	parfém
<g/>
,	,	kIx,	,
Voda	voda	k1gFnSc1	voda
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Franzbranntwein	Franzbranntweina	k1gFnPc2	Franzbranntweina
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
