<s>
Minority	minorita	k1gMnSc2	minorita
Report	report	k1gInSc1	report
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
sci-fi	scii	k1gFnSc6	sci-fi
film	film	k1gInSc4	film
režiséra	režisér	k1gMnSc2	režisér
Stevena	Steven	k2eAgFnSc1d1	Stevena
Spielberga	Spielberga	k1gFnSc1	Spielberga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
nominovaný	nominovaný	k2eAgInSc1d1	nominovaný
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Námětem	námět	k1gInSc7	námět
byla	být	k5eAaImAgFnS	být
povídka	povídka	k1gFnSc1	povídka
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
spisovatele	spisovatel	k1gMnSc2	spisovatel
Philipa	Philip	k1gMnSc2	Philip
K.	K.	kA	K.
Dicka	Dicek	k1gMnSc2	Dicek
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
otázkou	otázka	k1gFnSc7	otázka
předpovídání	předpovídání	k1gNnSc2	předpovídání
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
svobodné	svobodný	k2eAgFnSc2d1	svobodná
vůle	vůle	k1gFnSc2	vůle
a	a	k8xC	a
věcí	věc	k1gFnPc2	věc
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisejících	související	k2eAgFnPc2d1	související
(	(	kIx(	(
<g/>
determinismus	determinismus	k1gInSc1	determinismus
<g/>
,	,	kIx,	,
časový	časový	k2eAgInSc1d1	časový
paradox	paradox	k1gInSc1	paradox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2054	[number]	k4	2054
zkouší	zkoušet	k5eAaImIp3nS	zkoušet
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
jednotka	jednotka	k1gFnSc1	jednotka
policie	policie	k1gFnSc2	policie
na	na	k7c6	na
území	území	k1gNnSc6	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
USA	USA	kA	USA
Washingtonu	Washington	k1gInSc2	Washington
experimentální	experimentální	k2eAgFnSc4d1	experimentální
kriminalistickou	kriminalistický	k2eAgFnSc4d1	kriminalistická
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
lze	lze	k6eAd1	lze
zabránit	zabránit	k5eAaPmF	zabránit
vraždám	vražda	k1gFnPc3	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
filmu	film	k1gInSc2	film
je	být	k5eAaImIp3nS	být
Tom	Tom	k1gMnSc1	Tom
Cruise	Cruise	k1gFnSc2	Cruise
v	v	k7c6	v
roli	role	k1gFnSc6	role
Johna	John	k1gMnSc2	John
Andertona	Anderton	k1gMnSc2	Anderton
<g/>
,	,	kIx,	,
vedoucího	vedoucí	k1gMnSc2	vedoucí
speciálního	speciální	k2eAgNnSc2d1	speciální
oddělení	oddělení	k1gNnSc2	oddělení
Precrime	Precrim	k1gInSc5	Precrim
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
jako	jako	k8xS	jako
Předzločinný	Předzločinný	k2eAgInSc1d1	Předzločinný
oddíl	oddíl	k1gInSc1	oddíl
nebo	nebo	k8xC	nebo
Preventivní	preventivní	k2eAgFnSc1d1	preventivní
policie	policie	k1gFnSc1	policie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
tří	tři	k4xCgMnPc2	tři
tušitelů	tušitel	k1gMnPc2	tušitel
<g/>
,	,	kIx,	,
zmutovaných	zmutovaný	k2eAgMnPc2d1	zmutovaný
lidí	člověk	k1gMnPc2	člověk
schopných	schopný	k2eAgMnPc2d1	schopný
předvídat	předvídat	k5eAaImF	předvídat
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
a	a	k8xC	a
speciální	speciální	k2eAgFnPc4d1	speciální
techniky	technika	k1gFnPc4	technika
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
tým	tým	k1gInSc1	tým
Precrime	Precrim	k1gInSc5	Precrim
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
detailů	detail	k1gInPc2	detail
vztahujících	vztahující	k2eAgInPc2d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
budoucí	budoucí	k2eAgFnSc3d1	budoucí
vraždě	vražda	k1gFnSc3	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Tušitelé	Tušitel	k1gMnPc1	Tušitel
jim	on	k3xPp3gMnPc3	on
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
myšlenkách	myšlenka	k1gFnPc6	myšlenka
prozradí	prozradit	k5eAaPmIp3nS	prozradit
čas	čas	k1gInSc4	čas
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
jména	jméno	k1gNnPc4	jméno
pachatelů	pachatel	k1gMnPc2	pachatel
i	i	k8xC	i
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
týmu	tým	k1gInSc6	tým
Precrime	Precrim	k1gInSc5	Precrim
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
z	z	k7c2	z
útržkovitých	útržkovitý	k2eAgInPc2d1	útržkovitý
obrazů	obraz	k1gInPc2	obraz
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
myšlenek	myšlenka	k1gFnPc2	myšlenka
zjistit	zjistit	k5eAaPmF	zjistit
přesné	přesný	k2eAgNnSc4d1	přesné
místo	místo	k1gNnSc4	místo
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
včas	včas	k6eAd1	včas
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
a	a	k8xC	a
vraha	vrah	k1gMnSc4	vrah
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
<g/>
.	.	kIx.	.
</s>
<s>
Precrime	Precrimat	k5eAaPmIp3nS	Precrimat
neřeší	řešit	k5eNaImIp3nS	řešit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatýká	zatýkat	k5eAaImIp3nS	zatýkat
někoho	někdo	k3yInSc4	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
ještě	ještě	k9	ještě
nic	nic	k3yNnSc1	nic
neudělal	udělat	k5eNaPmAgMnS	udělat
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Anderton	Anderton	k1gInSc4	Anderton
je	být	k5eAaImIp3nS	být
respektovaným	respektovaný	k2eAgMnSc7d1	respektovaný
šéfem	šéf	k1gMnSc7	šéf
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
dobře	dobře	k6eAd1	dobře
vyzná	vyznat	k5eAaPmIp3nS	vyznat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
důvodů	důvod	k1gInPc2	důvod
zapálen	zapálen	k2eAgMnSc1d1	zapálen
<g/>
:	:	kIx,	:
před	před	k7c7	před
6	[number]	k4	6
roky	rok	k1gInPc7	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
ještě	ještě	k9	ještě
jednotka	jednotka	k1gFnSc1	jednotka
Precrime	Precrim	k1gInSc5	Precrim
nefungovala	fungovat	k5eNaImAgFnS	fungovat
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
někdo	někdo	k3yInSc1	někdo
unesl	unést	k5eAaPmAgMnS	unést
syna	syn	k1gMnSc4	syn
Seana	Sean	k1gMnSc4	Sean
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
má	mít	k5eAaImIp3nS	mít
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
syn	syn	k1gMnSc1	syn
už	už	k6eAd1	už
nežije	žít	k5eNaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Rozešel	rozejít	k5eAaPmAgMnS	rozejít
se	se	k3xPyFc4	se
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Larou	Lara	k1gFnSc7	Lara
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
brát	brát	k5eAaImF	brát
drogy	droga	k1gFnPc4	droga
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c4	o
jednotku	jednotka	k1gFnSc4	jednotka
začne	začít	k5eAaPmIp3nS	začít
zajímat	zajímat	k5eAaImF	zajímat
Danny	Dann	k1gInPc4	Dann
Witwer	Witwra	k1gFnPc2	Witwra
(	(	kIx(	(
<g/>
Colin	Colin	k1gMnSc1	Colin
Farrell	Farrell	k1gMnSc1	Farrell
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pracovník	pracovník	k1gMnSc1	pracovník
z	z	k7c2	z
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
nechce	chtít	k5eNaImIp3nS	chtít
si	se	k3xPyFc3	se
John	John	k1gMnSc1	John
připustit	připustit	k5eAaPmF	připustit
žádné	žádný	k3yNgNnSc4	žádný
pochybení	pochybení	k1gNnSc4	pochybení
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
audit	audit	k1gInSc1	audit
mu	on	k3xPp3gMnSc3	on
není	být	k5eNaImIp3nS	být
po	po	k7c6	po
chuti	chuť	k1gFnSc6	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Systému	systém	k1gInSc3	systém
plně	plně	k6eAd1	plně
důvěřuje	důvěřovat	k5eAaImIp3nS	důvěřovat
<g/>
.	.	kIx.	.
</s>
<s>
Zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
však	však	k9	však
nesrovnalosti	nesrovnalost	k1gFnPc4	nesrovnalost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
neznámé	známý	k2eNgFnSc2d1	neznámá
Anne	Ann	k1gFnSc2	Ann
Livelyové	Livelyová	k1gFnSc2	Livelyová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
mysli	mysl	k1gFnSc6	mysl
nejnadanější	nadaný	k2eAgFnSc2d3	nejnadanější
tušitelky	tušitelka	k1gFnSc2	tušitelka
Agáty	Agáta	k1gFnSc2	Agáta
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
se	se	k3xPyFc4	se
pracovník	pracovník	k1gMnSc1	pracovník
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
Witwer	Witwra	k1gFnPc2	Witwra
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
detailně	detailně	k6eAd1	detailně
s	s	k7c7	s
principem	princip	k1gInSc7	princip
fungování	fungování	k1gNnSc2	fungování
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zlom	zlom	k1gInSc1	zlom
nastane	nastat	k5eAaPmIp3nS	nastat
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
předtuch	předtucha	k1gFnPc2	předtucha
tušitelů	tušitel	k1gMnPc2	tušitel
uvidí	uvidět	k5eAaPmIp3nS	uvidět
John	John	k1gMnSc1	John
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Odvádí	odvádět	k5eAaImIp3nS	odvádět
od	od	k7c2	od
přehrávaných	přehrávaný	k2eAgFnPc2d1	přehrávaná
představ	představa	k1gFnPc2	představa
tušitelů	tušitel	k1gMnPc2	tušitel
pozornost	pozornost	k1gFnSc4	pozornost
kolegů	kolega	k1gMnPc2	kolega
Precrimu	Precrim	k1gInSc2	Precrim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
dlouho	dlouho	k6eAd1	dlouho
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
nepodaří	podařit	k5eNaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Začne	začít	k5eAaPmIp3nS	začít
utíkat	utíkat	k5eAaImF	utíkat
před	před	k7c7	před
vlastní	vlastní	k2eAgFnSc7d1	vlastní
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
nejmodernější	moderní	k2eAgFnSc2d3	nejmodernější
policejní	policejní	k2eAgFnSc2d1	policejní
techniky	technika	k1gFnSc2	technika
snaží	snažit	k5eAaImIp3nP	snažit
dopadnout	dopadnout	k5eAaPmF	dopadnout
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
chybě	chyba	k1gFnSc6	chyba
v	v	k7c6	v
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nechápe	chápat	k5eNaImIp3nS	chápat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
zabít	zabít	k5eAaPmF	zabít
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
nikdy	nikdy	k6eAd1	nikdy
neviděl	vidět	k5eNaImAgMnS	vidět
a	a	k8xC	a
o	o	k7c6	o
kterém	který	k3yIgInSc6	který
neslyšel	slyšet	k5eNaImAgMnS	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
by	by	kYmCp3nS	by
ho	on	k3xPp3gMnSc4	on
jednotka	jednotka	k1gFnSc1	jednotka
Precrime	Precrim	k1gInSc5	Precrim
dostihla	dostihnout	k5eAaPmAgNnP	dostihnout
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
by	by	kYmCp3nS	by
čas	čas	k1gInSc4	čas
na	na	k7c4	na
obhajobu	obhajoba	k1gFnSc4	obhajoba
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
by	by	kYmCp3nS	by
rovnou	rovnou	k6eAd1	rovnou
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
a	a	k8xC	a
hibernován	hibernovat	k5eAaImNgMnS	hibernovat
v	v	k7c6	v
alternativní	alternativní	k2eAgFnSc6d1	alternativní
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgInSc1d1	moderní
typ	typ	k1gInSc1	typ
vězení	vězení	k1gNnSc2	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doktorky	doktorka	k1gFnSc2	doktorka
Hinemanové	Hinemanová	k1gFnSc2	Hinemanová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tušitele	tušitel	k1gMnPc4	tušitel
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
tušitelé	tušitel	k1gMnPc1	tušitel
na	na	k7c4	na
předpovědi	předpověď	k1gFnPc4	předpověď
shodnou	shodnout	k5eAaPmIp3nP	shodnout
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
některý	některý	k3yIgMnSc1	některý
z	z	k7c2	z
tušitelů	tušitel	k1gMnPc2	tušitel
má	mít	k5eAaImIp3nS	mít
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
předtuchu	předtucha	k1gFnSc4	předtucha
než	než	k8xS	než
zbylí	zbylý	k2eAgMnPc1d1	zbylý
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
menšinovou	menšinový	k2eAgFnSc4d1	menšinová
zprávu	zpráva	k1gFnSc4	zpráva
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
minority	minorita	k1gFnPc4	minorita
report	report	k1gInSc1	report
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
fakt	fakt	k1gInSc1	fakt
existence	existence	k1gFnSc2	existence
takové	takový	k3xDgFnSc2	takový
menšinové	menšinový	k2eAgFnSc2d1	menšinová
zprávy	zpráva	k1gFnSc2	zpráva
Johna	John	k1gMnSc2	John
Andertona	Anderton	k1gMnSc2	Anderton
šokuje	šokovat	k5eAaBmIp3nS	šokovat
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgMnS	dokázat
svou	svůj	k3xOyFgFnSc4	svůj
nevinu	nevina	k1gFnSc4	nevina
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
nejnadanější	nadaný	k2eAgFnSc3d3	nejnadanější
tušitelce	tušitelka	k1gFnSc3	tušitelka
-	-	kIx~	-
Agátě	Agáta	k1gFnSc3	Agáta
-	-	kIx~	-
a	a	k8xC	a
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
intenzivně	intenzivně	k6eAd1	intenzivně
pátrá	pátrat	k5eAaImIp3nS	pátrat
bývalý	bývalý	k2eAgInSc1d1	bývalý
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vedl	vést	k5eAaImAgInS	vést
<g/>
,	,	kIx,	,
a	a	k8xC	a
pátrací	pátrací	k2eAgFnPc1d1	pátrací
metody	metoda	k1gFnPc1	metoda
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c4	na
skenování	skenování	k1gNnSc4	skenování
oční	oční	k2eAgFnSc2d1	oční
duhovky	duhovka	k1gFnSc2	duhovka
<g/>
,	,	kIx,	,
nechává	nechávat	k5eAaImIp3nS	nechávat
si	se	k3xPyFc3	se
potají	potají	k6eAd1	potají
u	u	k7c2	u
jednoho	jeden	k4xCgMnSc2	jeden
lékaře	lékař	k1gMnSc2	lékař
vyměnit	vyměnit	k5eAaPmF	vyměnit
celé	celý	k2eAgFnPc4d1	celá
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
záměně	záměna	k1gFnSc3	záměna
není	být	k5eNaImIp3nS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
policejními	policejní	k2eAgMnPc7d1	policejní
slídily	slídil	k1gMnPc7	slídil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nosí	nosit	k5eAaImIp3nP	nosit
své	svůj	k3xOyFgNnSc4	svůj
původní	původní	k2eAgNnPc4d1	původní
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
v	v	k7c6	v
pytlíku	pytlík	k1gInSc6	pytlík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dovolí	dovolit	k5eAaPmIp3nS	dovolit
mu	on	k3xPp3gMnSc3	on
bezpečnostní	bezpečnostní	k2eAgFnSc4d1	bezpečnostní
systém	systém	k1gInSc1	systém
Precrime	Precrim	k1gInSc5	Precrim
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
Agátu	Agáta	k1gFnSc4	Agáta
unést	unést	k5eAaPmF	unést
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
menšinová	menšinový	k2eAgFnSc1d1	menšinová
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
Anne	Anne	k1gFnSc1	Anne
Livelyové	Livelyové	k2eAgFnSc1d1	Livelyové
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
ji	on	k3xPp3gFnSc4	on
však	však	k8xC	však
nestihne	stihnout	k5eNaPmIp3nS	stihnout
celou	celá	k1gFnSc4	celá
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
patách	pata	k1gFnPc6	pata
Precrime	Precrim	k1gInSc5	Precrim
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Agátiným	Agátin	k2eAgFnPc3d1	Agátina
jasnovideckým	jasnovidecký	k2eAgFnPc3d1	jasnovidecká
schopnostem	schopnost	k1gFnPc3	schopnost
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
daří	dařit	k5eAaImIp3nS	dařit
jednotce	jednotka	k1gFnSc3	jednotka
unikat	unikat	k5eAaImF	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Dostanou	dostat	k5eAaPmIp3nP	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ubytovaný	ubytovaný	k2eAgMnSc1d1	ubytovaný
Leo	Leo	k1gMnSc1	Leo
Crow	Crow	k1gMnSc1	Crow
<g/>
,	,	kIx,	,
oběť	oběť	k1gFnSc1	oběť
Johnovy	Johnův	k2eAgFnSc2d1	Johnova
budoucí	budoucí	k2eAgFnSc2d1	budoucí
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
si	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
jistý	jistý	k2eAgMnSc1d1	jistý
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
důvod	důvod	k1gInSc4	důvod
Crowa	Crowum	k1gNnSc2	Crowum
zabít	zabít	k5eAaPmF	zabít
a	a	k8xC	a
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgMnS	dokázat
neplatnost	neplatnost	k1gFnSc4	neplatnost
předtuchy	předtucha	k1gFnSc2	předtucha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Agáta	Agáta	k1gFnSc1	Agáta
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
rozmlouvá	rozmlouvat	k5eAaImIp3nS	rozmlouvat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
rozrušená	rozrušený	k2eAgFnSc1d1	rozrušená
z	z	k7c2	z
blížící	blížící	k2eAgFnSc2d1	blížící
se	se	k3xPyFc4	se
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bytě	byt	k1gInSc6	byt
objeví	objevit	k5eAaPmIp3nS	objevit
John	John	k1gMnSc1	John
fotky	fotka	k1gFnSc2	fotka
unesených	unesený	k2eAgFnPc2d1	unesená
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
fotky	fotka	k1gFnPc4	fotka
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Seana	Sean	k1gMnSc2	Sean
<g/>
;	;	kIx,	;
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
je	být	k5eAaImIp3nS	být
Sean	Sean	k1gNnSc1	Sean
s	s	k7c7	s
Crowem	Crow	k1gInSc7	Crow
<g/>
.	.	kIx.	.
</s>
<s>
Johnovo	Johnův	k2eAgNnSc4d1	Johnovo
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
utrpení	utrpení	k1gNnSc4	utrpení
ze	z	k7c2	z
ztráty	ztráta	k1gFnSc2	ztráta
syna	syn	k1gMnSc2	syn
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
vztek	vztek	k1gInSc4	vztek
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
Crow	Crow	k1gFnSc1	Crow
a	a	k8xC	a
přizná	přiznat	k5eAaPmIp3nS	přiznat
se	se	k3xPyFc4	se
k	k	k7c3	k
bestiální	bestiální	k2eAgFnSc3d1	bestiální
vraždě	vražda	k1gFnSc3	vražda
Johnova	Johnův	k2eAgMnSc2d1	Johnův
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
John	John	k1gMnSc1	John
daleko	daleko	k6eAd1	daleko
k	k	k7c3	k
předpovězenému	předpovězený	k2eAgInSc3d1	předpovězený
výstřelu	výstřel	k1gInSc3	výstřel
na	na	k7c4	na
Crowa	Crowum	k1gNnPc4	Crowum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
okamžik	okamžik	k1gInSc4	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tušitelé	tušitel	k1gMnPc1	tušitel
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
nevystřelí	vystřelit	k5eNaPmIp3nS	vystřelit
a	a	k8xC	a
s	s	k7c7	s
přemáháním	přemáhání	k1gNnSc7	přemáhání
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
Crowovi	Crowův	k2eAgMnPc1d1	Crowův
obvinění	obviněný	k1gMnPc1	obviněný
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
-	-	kIx~	-
postupuje	postupovat	k5eAaImIp3nS	postupovat
standardním	standardní	k2eAgInSc7d1	standardní
policejním	policejní	k2eAgInSc7d1	policejní
postupem	postup	k1gInSc7	postup
<g/>
.	.	kIx.	.
</s>
<s>
Crow	Crow	k?	Crow
je	být	k5eAaImIp3nS	být
překvapen	překvapen	k2eAgMnSc1d1	překvapen
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gInSc4	on
Anderton	Anderton	k1gInSc4	Anderton
nezabil	zabít	k5eNaPmAgMnS	zabít
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
vraždy	vražda	k1gFnSc2	vražda
dožaduje	dožadovat	k5eAaImIp3nS	dožadovat
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Crowova	Crowův	k2eAgFnSc1d1	Crowův
rodina	rodina	k1gFnSc1	rodina
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
peníze	peníz	k1gInSc2	peníz
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
o	o	k7c6	o
objednávce	objednávka	k1gFnSc6	objednávka
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
vražda	vražda	k1gFnSc1	vražda
Crowa	Crowa	k1gFnSc1	Crowa
byla	být	k5eAaImAgFnS	být
přece	přece	k9	přece
jen	jen	k9	jen
léčka	léčka	k1gFnSc1	léčka
na	na	k7c4	na
něho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Crow	Crow	k?	Crow
nakonec	nakonec	k6eAd1	nakonec
umírá	umírat	k5eAaImIp3nS	umírat
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
předpovězen	předpovězit	k5eAaPmNgInS	předpovězit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
od	od	k7c2	od
úmyslu	úmysl	k1gInSc2	úmysl
ho	on	k3xPp3gInSc2	on
zabít	zabít	k5eAaPmF	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
Lamar	Lamar	k1gInSc1	Lamar
Burgess	Burgessa	k1gFnPc2	Burgessa
<g/>
,	,	kIx,	,
spoluvynálezce	spoluvynálezce	k1gMnSc1	spoluvynálezce
a	a	k8xC	a
ředitel	ředitel	k1gMnSc1	ředitel
Precrime	Precrim	k1gMnSc5	Precrim
<g/>
,	,	kIx,	,
zabije	zabít	k5eAaPmIp3nS	zabít
v	v	k7c6	v
Johnově	Johnův	k2eAgInSc6d1	Johnův
bytě	byt	k1gInSc6	byt
Johnovou	Johnová	k1gFnSc7	Johnová
zbraní	zbraň	k1gFnPc2	zbraň
auditora	auditor	k1gMnSc2	auditor
Witwera	Witwer	k1gMnSc2	Witwer
<g/>
.	.	kIx.	.
</s>
<s>
Využije	využít	k5eAaPmIp3nS	využít
nefunkčního	funkční	k2eNgInSc2d1	nefunkční
Precrimu	Precrim	k1gInSc2	Precrim
(	(	kIx(	(
<g/>
Agáta	Agáta	k1gFnSc1	Agáta
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
Johnem	John	k1gMnSc7	John
a	a	k8xC	a
bez	bez	k7c2	bez
ní	on	k3xPp3gFnSc2	on
Precrime	Precrim	k1gInSc5	Precrim
nepředpovídá	předpovídat	k5eNaImIp3nS	předpovídat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
umlčí	umlčet	k5eAaPmIp3nP	umlčet
Witwera	Witwer	k1gMnSc4	Witwer
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
právě	právě	k6eAd1	právě
sdělil	sdělit	k5eAaPmAgMnS	sdělit
"	"	kIx"	"
<g/>
novinku	novinka	k1gFnSc4	novinka
<g/>
"	"	kIx"	"
o	o	k7c6	o
menšinových	menšinový	k2eAgFnPc6d1	menšinová
zprávách	zpráva	k1gFnPc6	zpráva
a	a	k8xC	a
možnosti	možnost	k1gFnSc6	možnost
využití	využití	k1gNnSc2	využití
ozvěn	ozvěna	k1gFnPc2	ozvěna
předtuch	předtucha	k1gFnPc2	předtucha
(	(	kIx(	(
<g/>
při	při	k7c6	při
zvláště	zvláště	k6eAd1	zvláště
závažné	závažný	k2eAgFnSc6d1	závažná
vraždě	vražda	k1gFnSc6	vražda
se	se	k3xPyFc4	se
obraz	obraz	k1gInSc1	obraz
vraždy	vražda	k1gFnSc2	vražda
tušitelům	tušitel	k1gMnPc3	tušitel
několikrát	několikrát	k6eAd1	několikrát
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
,	,	kIx,	,
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ozvěna	ozvěna	k1gFnSc1	ozvěna
<g/>
)	)	kIx)	)
k	k	k7c3	k
zakrytí	zakrytí	k1gNnSc3	zakrytí
skutečně	skutečně	k6eAd1	skutečně
proběhlé	proběhlý	k2eAgFnSc2d1	proběhlá
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Witwer	Witwer	k1gMnSc1	Witwer
poukázal	poukázat	k5eAaPmAgMnS	poukázat
na	na	k7c4	na
předtuchu	předtucha	k1gFnSc4	předtucha
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
Anne	Ann	k1gFnSc2	Ann
Livelyové	Livelyová	k1gFnSc2	Livelyová
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
při	při	k7c6	při
nepozornosti	nepozornost	k1gFnSc6	nepozornost
pokládat	pokládat	k5eAaImF	pokládat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
zločin	zločin	k1gInSc4	zločin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
ozvěnu	ozvěna	k1gFnSc4	ozvěna
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
dvě	dva	k4xCgFnPc4	dva
předpovědi	předpověď	k1gFnPc4	předpověď
dvou	dva	k4xCgInPc2	dva
vražd	vražda	k1gFnPc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Jedné	jeden	k4xCgFnSc3	jeden
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
kryta	krýt	k5eAaImNgFnS	krýt
předpokladem	předpoklad	k1gInSc7	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
echo	echo	k1gNnSc4	echo
neuskutečněné	uskutečněný	k2eNgFnSc2d1	neuskutečněná
vraždy	vražda	k1gFnSc2	vražda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
"	"	kIx"	"
<g/>
mouše	moucha	k1gFnSc6	moucha
<g/>
"	"	kIx"	"
systému	systém	k1gInSc6	systém
už	už	k6eAd1	už
Witwer	Witwra	k1gFnPc2	Witwra
-	-	kIx~	-
díky	díky	k7c3	díky
pohotové	pohotový	k2eAgFnSc3d1	pohotová
Burgessově	Burgessův	k2eAgFnSc3d1	Burgessova
reakci	reakce	k1gFnSc3	reakce
-	-	kIx~	-
nemá	mít	k5eNaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
informovat	informovat	k5eAaBmF	informovat
vyšší	vysoký	k2eAgFnPc4d2	vyšší
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Johna	John	k1gMnSc4	John
sice	sice	k8xC	sice
Precrime	Precrim	k1gInSc5	Precrim
zadrží	zadržet	k5eAaPmIp3nP	zadržet
a	a	k8xC	a
uvězní	uvěznit	k5eAaPmIp3nP	uvěznit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
předtím	předtím	k6eAd1	předtím
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
bývalou	bývalý	k2eAgFnSc7d1	bývalá
ženou	žena	k1gFnSc7	žena
Larou	Larý	k2eAgFnSc7d1	Larý
a	a	k8xC	a
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
s	s	k7c7	s
obavou	obava	k1gFnSc7	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
obětí	oběť	k1gFnSc7	oběť
léčky	léčka	k1gFnSc2	léčka
někoho	někdo	k3yInSc4	někdo
hodně	hodně	k6eAd1	hodně
blízkého	blízký	k2eAgNnSc2d1	blízké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
neví	vědět	k5eNaImIp3nS	vědět
koho	kdo	k3yQnSc4	kdo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
filmu	film	k1gInSc2	film
vypátrá	vypátrat	k5eAaPmIp3nS	vypátrat
Laura	Laura	k1gFnSc1	Laura
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
připravil	připravit	k5eAaPmAgMnS	připravit
léčku	léčka	k1gFnSc4	léčka
na	na	k7c4	na
Johna	John	k1gMnSc4	John
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Burgess	Burgess	k1gInSc1	Burgess
<g/>
.	.	kIx.	.
</s>
<s>
Osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
Johna	John	k1gMnSc4	John
z	z	k7c2	z
vězení	vězení	k1gNnSc2	vězení
a	a	k8xC	a
podaří	podařit	k5eAaPmIp3nS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
usvědčit	usvědčit	k5eAaPmF	usvědčit
Burgesse	Burgesse	k1gFnPc4	Burgesse
z	z	k7c2	z
vraždy	vražda	k1gFnSc2	vražda
Livelyové	Livelyová	k1gFnSc2	Livelyová
<g/>
.	.	kIx.	.
</s>
<s>
Anne	Anne	k1gFnSc1	Anne
Livelyová	Livelyová	k1gFnSc1	Livelyová
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
Agáty	Agáta	k1gFnSc2	Agáta
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyléčila	vyléčit	k5eAaPmAgFnS	vyléčit
z	z	k7c2	z
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
a	a	k8xC	a
projevila	projevit	k5eAaPmAgFnS	projevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
dceru	dcera	k1gFnSc4	dcera
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
zapojenou	zapojený	k2eAgFnSc4d1	zapojená
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
Precrime	Precrim	k1gInSc5	Precrim
<g/>
.	.	kIx.	.
</s>
<s>
Burgess	Burgess	k1gInSc1	Burgess
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
nesmírné	smírný	k2eNgFnPc4d1	nesmírná
důležitosti	důležitost	k1gFnPc4	důležitost
Agáty	Agáta	k1gFnSc2	Agáta
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
svém	své	k1gNnSc6	své
projektu	projekt	k1gInSc2	projekt
<g/>
,	,	kIx,	,
využil	využít	k5eAaPmAgMnS	využít
mezery	mezera	k1gFnPc4	mezera
systému	systém	k1gInSc2	systém
a	a	k8xC	a
Agátinu	Agátin	k2eAgFnSc4d1	Agátina
matku	matka	k1gFnSc4	matka
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Burgess	Burgess	k6eAd1	Burgess
po	po	k7c6	po
prozrazení	prozrazení	k1gNnSc6	prozrazení
spáchá	spáchat	k5eAaPmIp3nS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
odsoudí	odsoudit	k5eAaPmIp3nS	odsoudit
i	i	k9	i
svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
Precrime	Precrim	k1gInSc5	Precrim
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nebere	brát	k5eNaImIp3nS	brát
v	v	k7c4	v
potaz	potaz	k1gInSc4	potaz
existence	existence	k1gFnSc2	existence
menšinových	menšinový	k2eAgFnPc2d1	menšinová
zpráv	zpráva	k1gFnPc2	zpráva
a	a	k8xC	a
který	který	k3yIgInSc4	který
lze	lze	k6eAd1	lze
zneužít	zneužít	k5eAaPmF	zneužít
pro	pro	k7c4	pro
utajení	utajení	k1gNnSc4	utajení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vraždy	vražda	k1gFnSc2	vražda
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dále	daleko	k6eAd2	daleko
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
filmu	film	k1gInSc2	film
John	John	k1gMnSc1	John
obnoví	obnovit	k5eAaPmIp3nS	obnovit
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
Larou	Lara	k1gFnSc7	Lara
a	a	k8xC	a
Lara	Lara	k1gFnSc1	Lara
znovu	znovu	k6eAd1	znovu
otěhotní	otěhotnět	k5eAaPmIp3nS	otěhotnět
...	...	k?	...
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
mnoho	mnoho	k4c1	mnoho
vynálezů	vynález	k1gInPc2	vynález
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
technických	technický	k2eAgInPc2d1	technický
<g/>
,	,	kIx,	,
lékařských	lékařský	k2eAgInPc2d1	lékařský
nebo	nebo	k8xC	nebo
biologických	biologický	k2eAgInPc2d1	biologický
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vykreslují	vykreslovat	k5eAaImIp3nP	vykreslovat
dobu	doba	k1gFnSc4	doba
roku	rok	k1gInSc2	rok
2054	[number]	k4	2054
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
zdaleka	zdaleka	k6eAd1	zdaleka
jen	jen	k9	jen
o	o	k7c4	o
tušitele	tušitel	k1gMnPc4	tušitel
a	a	k8xC	a
systém	systém	k1gInSc4	systém
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
napojený	napojený	k2eAgMnSc1d1	napojený
<g/>
.	.	kIx.	.
uchovávání	uchovávání	k1gNnSc1	uchovávání
v	v	k7c6	v
polospánku	polospánek	k1gInSc6	polospánek
ve	v	k7c6	v
vodivé	vodivý	k2eAgFnSc6d1	vodivá
lázni	lázeň	k1gFnSc6	lázeň
(	(	kIx(	(
<g/>
fotonové	fotonový	k2eAgNnSc1d1	fotonové
mléko	mléko	k1gNnSc1	mléko
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
omamných	omamný	k2eAgFnPc2d1	omamná
látek	látka	k1gFnPc2	látka
(	(	kIx(	(
<g/>
dopamin	dopamin	k1gInSc1	dopamin
<g/>
,	,	kIx,	,
endorfiny	endorfin	k1gInPc1	endorfin
<g/>
,	,	kIx,	,
serotonin	serotonin	k1gInSc1	serotonin
<g/>
)	)	kIx)	)
jejich	jejich	k3xOp3gFnSc2	jejich
předtuchy	předtucha	k1gFnSc2	předtucha
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášet	k5eAaImNgInP	přenášet
do	do	k7c2	do
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
policistům	policista	k1gMnPc3	policista
vybírat	vybírat	k5eAaImF	vybírat
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
přání	přání	k1gNnSc2	přání
určité	určitý	k2eAgFnSc2d1	určitá
scény	scéna	k1gFnSc2	scéna
prohlédnout	prohlédnout	k5eAaPmF	prohlédnout
<g/>
,	,	kIx,	,
rotovat	rotovat	k5eAaImF	rotovat
a	a	k8xC	a
jinak	jinak	k6eAd1	jinak
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
v	v	k7c6	v
reálném	reálný	k2eAgInSc6d1	reálný
čase	čas	k1gInSc6	čas
vznášedla	vznášedlo	k1gNnSc2	vznášedlo
s	s	k7c7	s
tryskovými	tryskový	k2eAgInPc7d1	tryskový
motory	motor	k1gInPc7	motor
-	-	kIx~	-
nástupci	nástupce	k1gMnPc1	nástupce
vrtulníku	vrtulník	k1gInSc2	vrtulník
kombinéza	kombinéza	k1gFnSc1	kombinéza
vybavená	vybavený	k2eAgNnPc4d1	vybavené
letovým	letový	k2eAgInSc7d1	letový
systémem	systém	k1gInSc7	systém
-	-	kIx~	-
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vertikální	vertikální	k2eAgMnSc1d1	vertikální
i	i	k8xC	i
horizontální	horizontální	k2eAgInSc1d1	horizontální
pohyb	pohyb	k1gInSc1	pohyb
prostorem	prostor	k1gInSc7	prostor
elektronická	elektronický	k2eAgFnSc1d1	elektronická
čelenka	čelenka	k1gFnSc1	čelenka
pro	pro	k7c4	pro
zadrženého	zadržený	k2eAgMnSc4d1	zadržený
místo	místo	k1gNnSc4	místo
pout	pouto	k1gNnPc2	pouto
skener	skener	k1gInSc1	skener
oční	oční	k2eAgFnSc2d1	oční
duhovky	duhovka	k1gFnSc2	duhovka
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
,	,	kIx,	,
napojený	napojený	k2eAgInSc4d1	napojený
na	na	k7c4	na
policejní	policejní	k2eAgInSc4d1	policejní
systém	systém	k1gInSc4	systém
příruční	příruční	k2eAgInSc4d1	příruční
skener	skener	k1gInSc4	skener
oční	oční	k2eAgFnSc2d1	oční
duhovky	duhovka	k1gFnSc2	duhovka
napojený	napojený	k2eAgInSc1d1	napojený
na	na	k7c6	na
policejní	policejní	k2eAgFnSc6d1	policejní
databázi	databáze	k1gFnSc6	databáze
slídil	slídil	k1gMnSc1	slídil
-	-	kIx~	-
hmyzu	hmyz	k1gInSc6	hmyz
podobný	podobný	k2eAgInSc4d1	podobný
stroj	stroj	k1gInSc4	stroj
s	s	k7c7	s
umělou	umělý	k2eAgFnSc7d1	umělá
inteligencí	inteligence	k1gFnSc7	inteligence
vybavený	vybavený	k2eAgInSc4d1	vybavený
skenerem	skener	k1gInSc7	skener
oční	oční	k2eAgFnPc4d1	oční
duhovky	duhovka	k1gFnSc2	duhovka
alternativní	alternativní	k2eAgFnSc1d1	alternativní
budoucnost	budoucnost	k1gFnSc1	budoucnost
-	-	kIx~	-
hibernace	hibernace	k1gFnSc1	hibernace
vězňů	vězeň	k1gMnPc2	vězeň
ve	v	k7c6	v
vysouvacích	vysouvací	k2eAgFnPc6d1	vysouvací
kójích	kóje	k1gFnPc6	kóje
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
hale	hala	k1gFnSc6	hala
tepelný	tepelný	k2eAgInSc4d1	tepelný
snímek	snímek	k1gInSc4	snímek
místností	místnost	k1gFnPc2	místnost
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
-	-	kIx~	-
detekuje	detekovat	k5eAaImIp3nS	detekovat
živé	živý	k2eAgInPc4d1	živý
organizmy	organizmus	k1gInPc4	organizmus
hlasově	hlasově	k6eAd1	hlasově
<g />
.	.	kIx.	.
</s>
<s>
ovládané	ovládaný	k2eAgNnSc1d1	ovládané
osvětlení	osvětlení	k1gNnSc1	osvětlení
a	a	k8xC	a
multimediální	multimediální	k2eAgNnPc1d1	multimediální
zařízení	zařízení	k1gNnPc1	zařízení
(	(	kIx(	(
<g/>
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
projektor	projektor	k1gInSc1	projektor
a	a	k8xC	a
promítací	promítací	k2eAgNnSc1d1	promítací
plátno	plátno	k1gNnSc1	plátno
<g/>
)	)	kIx)	)
trojrozměrná	trojrozměrný	k2eAgFnSc1d1	trojrozměrná
holografická	holografický	k2eAgFnSc1d1	holografická
barevná	barevný	k2eAgFnSc1d1	barevná
projekce	projekce	k1gFnSc1	projekce
průhledná	průhledný	k2eAgFnSc1d1	průhledná
tenká	tenký	k2eAgFnSc1d1	tenká
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
do	do	k7c2	do
dlaně	dlaň	k1gFnSc2	dlaň
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nosič	nosič	k1gInSc1	nosič
audiozáznamu	audiozáznam	k1gInSc2	audiozáznam
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
ambulantní	ambulantní	k2eAgFnSc1d1	ambulantní
výměna	výměna	k1gFnSc1	výměna
očí	oko	k1gNnPc2	oko
živé	živá	k1gFnSc2	živá
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
Dr	dr	kA	dr
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Iris	iris	k1gFnSc1	iris
Hinemanové	Hinemanová	k1gFnSc2	Hinemanová
<g/>
)	)	kIx)	)
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kříženci	kříženec	k1gMnPc1	kříženec
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
vzhled	vzhled	k1gInSc4	vzhled
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
pohyby	pohyb	k1gInPc4	pohyb
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
v	v	k7c6	v
typu	typ	k1gInSc6	typ
pohybu	pohyb	k1gInSc2	pohyb
podobné	podobný	k2eAgFnSc2d1	podobná
mořským	mořský	k2eAgFnPc3d1	mořská
sasankám	sasanka	k1gFnPc3	sasanka
a	a	k8xC	a
hadům	had	k1gMnPc3	had
interaktivní	interaktivní	k2eAgFnPc4d1	interaktivní
noviny	novina	k1gFnPc4	novina
a	a	k8xC	a
časopisy	časopis	k1gInPc4	časopis
-	-	kIx~	-
aktualizují	aktualizovat	k5eAaBmIp3nP	aktualizovat
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
o	o	k7c6	o
události	událost	k1gFnSc6	událost
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
novináři	novinář	k1gMnSc3	novinář
velké	velký	k2eAgFnSc2d1	velká
displeje	displej	k1gFnSc2	displej
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
<g />
.	.	kIx.	.
</s>
<s>
budov	budova	k1gFnPc2	budova
i	i	k9	i
pod	pod	k7c4	pod
mosty	most	k1gInPc4	most
(	(	kIx(	(
<g/>
propagují	propagovat	k5eAaImIp3nP	propagovat
Precrime	Precrim	k1gInSc5	Precrim
<g/>
)	)	kIx)	)
automobily	automobil	k1gInPc1	automobil
s	s	k7c7	s
automatickým	automatický	k2eAgNnSc7d1	automatické
řízením	řízení	k1gNnSc7	řízení
jezdící	jezdící	k2eAgFnSc2d1	jezdící
po	po	k7c6	po
horizontálních	horizontální	k2eAgFnPc6d1	horizontální
i	i	k8xC	i
vertikálních	vertikální	k2eAgFnPc6d1	vertikální
betonových	betonový	k2eAgFnPc6d1	betonová
drahách	draha	k1gFnPc6	draha
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
střih	střih	k1gInSc4	střih
zvuku	zvuk	k1gInSc2	zvuk
-	-	kIx~	-
Richard	Richard	k1gMnSc1	Richard
Hymns	Hymnsa	k1gFnPc2	Hymnsa
<g/>
,	,	kIx,	,
Gary	Gar	k2eAgInPc1d1	Gar
Rydstrom	Rydstrom	k1gInSc4	Rydstrom
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
Minority	minorita	k1gFnSc2	minorita
Report	report	k1gInSc1	report
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Filmové	filmový	k2eAgFnSc2d1	filmová
databáze	databáze	k1gFnSc2	databáze
</s>
