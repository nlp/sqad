<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Město	město	k1gNnSc1	město
a	a	k8xC	a
Okres	okres	k1gInSc1	okres
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nejsevernější	severní	k2eAgFnSc6d3	nejsevernější
části	část	k1gFnSc6	část
Sanfranciského	sanfranciský	k2eAgInSc2d1	sanfranciský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
úžiny	úžina	k1gFnSc2	úžina
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gate	k1gFnSc7	Gate
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
Sanfranciského	sanfranciský	k2eAgInSc2d1	sanfranciský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
kulturním	kulturní	k2eAgInSc7d1	kulturní
a	a	k8xC	a
finančním	finanční	k2eAgInSc7d1	finanční
centrem	centr	k1gInSc7	centr
Severní	severní	k2eAgFnSc2d1	severní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
a	a	k8xC	a
oblasti	oblast	k1gFnSc2	oblast
Sanfranciského	sanfranciský	k2eAgInSc2d1	sanfranciský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
825	[number]	k4	825
000	[number]	k4	000
obyvateli	obyvatel	k1gMnPc7	obyvatel
je	být	k5eAaImIp3nS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
(	(	kIx(	(
<g/>
po	po	k7c4	po
Los	los	k1gInSc4	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Diegu	Dieg	k1gInSc2	Dieg
a	a	k8xC	a
San	San	k1gFnSc2	San
José	Josá	k1gFnSc2	Josá
<g/>
)	)	kIx)	)
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgNnSc7d3	nejlidnatější
městem	město	k1gNnSc7	město
celých	celý	k2eAgInPc2d1	celý
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
je	být	k5eAaImIp3nS	být
také	také	k9	také
druhým	druhý	k4xOgInSc7	druhý
nejhustěji	husto	k6eAd3	husto
obydleným	obydlený	k2eAgNnSc7d1	obydlené
městem	město	k1gNnSc7	město
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
městské	městský	k2eAgFnSc6d1	městská
aglomeraci	aglomerace	k1gFnSc6	aglomerace
San	San	k1gMnSc1	San
José-San	José-San	k1gMnSc1	José-San
Francisco-Oakland	Francisco-Oakland	k1gInSc4	Francisco-Oakland
žije	žít	k5eAaImIp3nS	žít
8,4	[number]	k4	8,4
miliónu	milión	k4xCgInSc2	milión
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
proslulé	proslulý	k2eAgInPc4d1	proslulý
svými	svůj	k3xOyFgNnPc7	svůj
chladnými	chladný	k2eAgNnPc7d1	chladné
léty	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
mlhou	mlha	k1gFnSc7	mlha
<g/>
,	,	kIx,	,
strmými	strmý	k2eAgInPc7d1	strmý
kopci	kopec	k1gInPc7	kopec
<g/>
,	,	kIx,	,
rozmanitostí	rozmanitost	k1gFnSc7	rozmanitost
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
turistickými	turistický	k2eAgInPc7d1	turistický
cíli	cíl	k1gInPc7	cíl
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
kterými	který	k3yRgInPc7	který
je	být	k5eAaImIp3nS	být
most	most	k1gInSc1	most
Golden	Goldno	k1gNnPc2	Goldno
Gate	Gat	k1gFnSc2	Gat
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
,	,	kIx,	,
lanová	lanový	k2eAgFnSc1d1	lanová
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgNnSc1d1	bývalé
vězení	vězení	k1gNnSc1	vězení
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Alcatraz	Alcatraz	k1gInSc1	Alcatraz
a	a	k8xC	a
čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
zajímavostí	zajímavost	k1gFnPc2	zajímavost
a	a	k8xC	a
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
San	San	k1gFnSc1	San
Francisco	Francisco	k6eAd1	Francisco
vyhledávaným	vyhledávaný	k2eAgInSc7d1	vyhledávaný
cílem	cíl	k1gInSc7	cíl
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označováno	označovat	k5eAaImNgNnS	označovat
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgNnPc2d3	nejkrásnější
měst	město	k1gNnPc2	město
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgMnPc1d1	původní
Američané	Američan	k1gMnPc1	Američan
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
San	San	k1gMnSc2	San
Francisca	Franciscus	k1gMnSc2	Franciscus
usadili	usadit	k5eAaPmAgMnP	usadit
před	před	k7c7	před
10000	[number]	k4	10000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vplul	vplout	k5eAaPmAgMnS	vplout
do	do	k7c2	do
San	San	k1gFnSc2	San
Francisské	Francisský	k2eAgFnSc2d1	Francisský
zátoky	zátoka	k1gFnSc2	zátoka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
průzkumník	průzkumník	k1gMnSc1	průzkumník
Don	Don	k1gMnSc1	Don
Gaspar	Gaspar	k1gMnSc1	Gaspar
de	de	k?	de
Portola	Portola	k1gFnSc1	Portola
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1770	[number]	k4	1770
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
španělská	španělský	k2eAgFnSc1d1	španělská
misie	misie	k1gFnSc1	misie
<g/>
,	,	kIx,	,
Mission	Mission	k1gInSc1	Mission
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Asis	Asis	k1gInSc1	Asis
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
29	[number]	k4	29
<g/>
.	.	kIx.	.
<g/>
června	červen	k1gInSc2	červen
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
Presidio	Presidio	k6eAd1	Presidio
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
malá	malý	k2eAgFnSc1d1	malá
vojenská	vojenský	k2eAgFnSc1d1	vojenská
tvrz	tvrz	k1gFnSc1	tvrz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Alcatraz	Alcatraz	k1gInSc1	Alcatraz
postavili	postavit	k5eAaPmAgMnP	postavit
Španělé	Španěl	k1gMnPc1	Španěl
malou	malý	k2eAgFnSc4d1	malá
vesnici	vesnice	k1gFnSc4	vesnice
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Yerba	Yerb	k1gMnSc4	Yerb
Buena	Bueen	k2eAgFnSc1d1	Buena
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
připadlo	připadnout	k5eAaPmAgNnS	připadnout
území	území	k1gNnSc1	území
Mexičanům	Mexičan	k1gMnPc3	Mexičan
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
nezávislým	závislý	k2eNgMnSc7d1	nezávislý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
izolovaným	izolovaný	k2eAgInSc7d1	izolovaný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
byla	být	k5eAaImAgFnS	být
vesnice	vesnice	k1gFnSc1	vesnice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
San	San	k1gFnSc4	San
Francisco	Francisco	k6eAd1	Francisco
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
San	San	k1gMnSc4	San
Francisco	Francisco	k6eAd1	Francisco
malé	malý	k2eAgNnSc1d1	malé
městečko	městečko	k1gNnSc1	městečko
s	s	k7c7	s
nehostinnou	hostinný	k2eNgFnSc7d1	nehostinná
geografií	geografie	k1gFnSc7	geografie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
horečka	horečka	k1gFnSc1	horečka
do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
imigrační	imigrační	k2eAgFnSc4d1	imigrační
vlnu	vlna	k1gFnSc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1849	[number]	k4	1849
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
z	z	k7c2	z
1	[number]	k4	1
000	[number]	k4	000
na	na	k7c4	na
25	[number]	k4	25
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
bankovnictví	bankovnictví	k1gNnSc1	bankovnictví
a	a	k8xC	a
těžební	těžební	k2eAgInSc1d1	těžební
průmysl	průmysl	k1gInSc1	průmysl
přitáhl	přitáhnout	k5eAaPmAgInS	přitáhnout
do	do	k7c2	do
města	město	k1gNnSc2	město
majoritní	majoritní	k2eAgFnSc2d1	majoritní
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
4	[number]	k4	4
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
souvisejícím	související	k2eAgInSc7d1	související
požárem	požár	k1gInSc7	požár
<g/>
,	,	kIx,	,
zničilo	zničit	k5eAaPmAgNnS	zničit
80	[number]	k4	80
<g/>
%	%	kIx~	%
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Zemřelo	zemřít	k5eAaPmAgNnS	zemřít
přes	přes	k7c4	přes
3	[number]	k4	3
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
rychle	rychle	k6eAd1	rychle
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
mostů	most	k1gInPc2	most
San	San	k1gFnSc2	San
Francisco	Francisco	k1gNnSc1	Francisco
-	-	kIx~	-
Oakland	Oakland	k1gInSc1	Oakland
Bay	Bay	k1gFnSc2	Bay
Bridge	Bridge	k1gFnPc2	Bridge
a	a	k8xC	a
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gat	k1gMnSc2	Gat
Bridge	Bridg	k1gMnSc2	Bridg
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
a	a	k8xC	a
1937	[number]	k4	1937
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
dostupnější	dostupný	k2eAgMnSc1d2	dostupnější
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
prudce	prudko	k6eAd1	prudko
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
naloďovala	naloďovat	k5eAaImAgFnS	naloďovat
vojska	vojsko	k1gNnPc4	vojsko
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Tichomoří	Tichomoří	k1gNnSc6	Tichomoří
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
vedly	vést	k5eAaImAgInP	vést
přílivy	příliv	k1gInPc1	příliv
vracejících	vracející	k2eAgMnPc2d1	vracející
se	se	k3xPyFc4	se
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
masívní	masívní	k2eAgNnSc1d1	masívní
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
San	San	k1gFnSc3	San
Francisco	Francisco	k6eAd1	Francisco
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
centrem	centr	k1gInSc7	centr
liberalizačních	liberalizační	k2eAgFnPc2d1	liberalizační
snah	snaha	k1gFnPc2	snaha
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
Státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
San	San	k1gFnSc7	San
Francisco	Francisco	k1gNnSc1	Francisco
stalo	stát	k5eAaPmAgNnS	stát
magnetem	magnet	k1gInSc7	magnet
pro	pro	k7c4	pro
malíře	malíř	k1gMnPc4	malíř
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnPc4	spisovatel
a	a	k8xC	a
rockové	rockový	k2eAgMnPc4d1	rockový
muzikanty	muzikant	k1gMnPc4	muzikant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
zde	zde	k6eAd1	zde
během	během	k7c2	během
"	"	kIx"	"
<g/>
Léta	léto	k1gNnSc2	léto
lásky	láska	k1gFnSc2	láska
<g/>
"	"	kIx"	"
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
masovému	masový	k2eAgInSc3d1	masový
rozvoji	rozvoj	k1gInSc3	rozvoj
hnutí	hnutí	k1gNnSc2	hnutí
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
také	také	k9	také
stalo	stát	k5eAaPmAgNnS	stát
centrem	centr	k1gInSc7	centr
osvobození	osvobození	k1gNnSc2	osvobození
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
město	město	k1gNnSc1	město
další	další	k2eAgNnSc1d1	další
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zničeno	zničen	k2eAgNnSc4d1	zničeno
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
stran	strana	k1gFnPc2	strana
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
vodou	voda	k1gFnSc7	voda
<g/>
;	;	kIx,	;
Tichým	tichý	k2eAgInSc7d1	tichý
oceánem	oceán	k1gInSc7	oceán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
San	San	k1gFnSc2	San
Franciscou	Franciscý	k2eAgFnSc7d1	Franciscý
zátokou	zátoka	k1gFnSc7	zátoka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
i	i	k9	i
několik	několik	k4yIc1	několik
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Alcatraz	Alcatraz	k1gInSc1	Alcatraz
nebo	nebo	k8xC	nebo
Treasure	Treasur	k1gMnSc5	Treasur
Island	Island	k1gInSc1	Island
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
městu	město	k1gNnSc3	město
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
neobydlené	obydlený	k2eNgInPc1d1	neobydlený
Farallonské	Farallonský	k2eAgInPc1d1	Farallonský
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
43	[number]	k4	43
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
zhruba	zhruba	k6eAd1	zhruba
11	[number]	k4	11
<g/>
x	x	k?	x
<g/>
11	[number]	k4	11
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
je	být	k5eAaImIp3nS	být
San	San	k1gFnSc1	San
Francisco	Francisco	k6eAd1	Francisco
významnou	významný	k2eAgFnSc7d1	významná
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
San	San	k1gFnSc4	San
Francisco	Francisco	k6eAd1	Francisco
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
nerovný	rovný	k2eNgInSc1d1	nerovný
terén	terén	k1gInSc1	terén
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
více	hodně	k6eAd2	hodně
než	než	k8xS	než
padesáti	padesát	k4xCc2	padesát
kopců	kopec	k1gInPc2	kopec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
kopců	kopec	k1gInPc2	kopec
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
leží	ležet	k5eAaImIp3nP	ležet
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
městské	městský	k2eAgFnPc1d1	městská
čtvrti	čtvrt	k1gFnPc1	čtvrt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Pacific	Pacific	k1gMnSc1	Pacific
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
,	,	kIx,	,
Russian	Russian	k1gMnSc1	Russian
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Potrero	Potrero	k1gNnSc1	Potrero
Hill	Hilla	k1gFnPc2	Hilla
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Telegraph	Telegraph	k1gMnSc1	Telegraph
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
San	San	k1gFnSc4	San
Francisco	Francisco	k6eAd1	Francisco
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
mírné	mírný	k2eAgFnPc1d1	mírná
deštivé	deštivý	k2eAgFnPc1d1	deštivá
zimy	zima	k1gFnPc1	zima
a	a	k8xC	a
suchá	suchý	k2eAgNnPc1d1	suché
léta	léto	k1gNnPc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sanfranciské	sanfranciský	k2eAgNnSc1d1	sanfranciské
klima	klima	k1gNnSc1	klima
silně	silně	k6eAd1	silně
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
studenými	studený	k2eAgInPc7d1	studený
proudy	proud	k1gInPc7	proud
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přináší	přinášet	k5eAaImIp3nS	přinášet
jen	jen	k9	jen
malé	malý	k2eAgInPc4d1	malý
rozdíly	rozdíl	k1gInPc4	rozdíl
mezi	mezi	k7c7	mezi
teplotami	teplota	k1gFnPc7	teplota
během	během	k7c2	během
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
letní	letní	k2eAgFnPc1d1	letní
leploty	leplota	k1gFnPc1	leplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
do	do	k7c2	do
21	[number]	k4	21
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
San	San	k1gFnSc4	San
Franciscu	Franciscus	k1gInSc2	Franciscus
naměřena	naměřen	k2eAgFnSc1d1	naměřena
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
39	[number]	k4	39
°	°	k?	°
<g/>
C	C	kA	C
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc1	zima
jsou	být	k5eAaImIp3nP	být
mírné	mírný	k2eAgInPc1d1	mírný
s	s	k7c7	s
teplotami	teplota	k1gFnPc7	teplota
kolem	kolem	k7c2	kolem
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Teploty	teplota	k1gFnSc2	teplota
většinou	většinou	k6eAd1	většinou
nikdy	nikdy	k6eAd1	nikdy
nesestoupí	sestoupit	k5eNaPmIp3nS	sestoupit
k	k	k7c3	k
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
nejnižší	nízký	k2eAgFnSc2d3	nejnižší
zaznamenané	zaznamenaný	k2eAgFnSc2d1	zaznamenaná
teploty	teplota	k1gFnSc2	teplota
-3	-3	k4	-3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
naměřena	naměřen	k2eAgFnSc1d1	naměřena
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
sněhu	sníh	k1gInSc2	sníh
ve	v	k7c6	v
městě	město	k1gNnSc6	město
napadlo	napadnout	k5eAaPmAgNnS	napadnout
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1887	[number]	k4	1887
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
leželo	ležet	k5eAaImAgNnS	ležet
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
9,4	[number]	k4	9,4
cm	cm	kA	cm
sněhu	sníh	k1gInSc2	sníh
<g/>
,	,	kIx,	,
v	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
částech	část	k1gFnPc6	část
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
17,8	[number]	k4	17,8
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
měřitelné	měřitelný	k2eAgNnSc1d1	měřitelné
sněžení	sněžení	k1gNnSc1	sněžení
bylo	být	k5eAaImAgNnS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
napadlo	napadnout	k5eAaPmAgNnS	napadnout
2,5	[number]	k4	2,5
cm	cm	kA	cm
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
pevniny	pevnina	k1gFnSc2	pevnina
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
mlhu	mlha	k1gFnSc4	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
jarním	jarní	k2eAgNnSc6d1	jarní
a	a	k8xC	a
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
zahalit	zahalit	k5eAaPmF	zahalit
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
se	se	k3xPyFc4	se
mlha	mlha	k1gFnSc1	mlha
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
oblastech	oblast	k1gFnPc6	oblast
San	San	k1gFnSc2	San
Francisca	Francisc	k1gInSc2	Francisc
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
nejtepleji	teple	k6eAd3	teple
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
kopcovitému	kopcovitý	k2eAgInSc3d1	kopcovitý
reliéfu	reliéf	k1gInSc3	reliéf
města	město	k1gNnSc2	město
a	a	k8xC	a
vlivu	vliv	k1gInSc2	vliv
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
podnebí	podnebí	k1gNnSc1	podnebí
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
může	moct	k5eAaImIp3nS	moct
velmi	velmi	k6eAd1	velmi
lišit	lišit	k5eAaImF	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgInPc1d1	vysoký
kopce	kopec	k1gInPc1	kopec
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
až	až	k9	až
20	[number]	k4	20
<g/>
%	%	kIx~	%
rozdílem	rozdíl	k1gInSc7	rozdíl
v	v	k7c4	v
množství	množství	k1gNnSc4	množství
srážek	srážka	k1gFnPc2	srážka
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
části	část	k1gFnPc1	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kopce	kopec	k1gInPc1	kopec
také	také	k9	také
chrání	chránit	k5eAaImIp3nP	chránit
východní	východní	k2eAgFnPc4d1	východní
oblasti	oblast	k1gFnPc4	oblast
před	před	k7c7	před
mlhami	mlha	k1gFnPc7	mlha
a	a	k8xC	a
oblačností	oblačnost	k1gFnSc7	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
San	San	k1gFnPc2	San
Francisca	Franciscum	k1gNnSc2	Franciscum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
střed	střed	k1gInSc4	střed
města	město	k1gNnSc2	město
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
náměstí	náměstí	k1gNnSc4	náměstí
Union	union	k1gInSc1	union
Square	square	k1gInSc1	square
a	a	k8xC	a
jižně	jižně	k6eAd1	jižně
ležící	ležící	k2eAgInSc4d1	ležící
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
konec	konec	k1gInSc4	konec
ulice	ulice	k1gFnSc1	ulice
Market	market	k1gInSc1	market
st.	st.	kA	st.
Obchodní	obchodní	k2eAgNnSc1d1	obchodní
a	a	k8xC	a
finanční	finanční	k2eAgNnSc1d1	finanční
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nS	tvořit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Union	union	k1gInSc1	union
Square	square	k1gInSc1	square
ležící	ležící	k2eAgInSc1d1	ležící
Financial	Financial	k1gInSc4	Financial
District	District	k2eAgInSc4d1	District
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
nákupů	nákup	k1gInPc2	nákup
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
turistické	turistický	k2eAgNnSc4d1	turistické
centrum	centrum	k1gNnSc4	centrum
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
severovýchodně	severovýchodně	k6eAd1	severovýchodně
ležící	ležící	k2eAgFnSc1d1	ležící
přístavní	přístavní	k2eAgFnSc1d1	přístavní
čtvrť	čtvrť	k1gFnSc1	čtvrť
Fisherman	Fisherman	k1gMnSc1	Fisherman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wharf	Wharf	k1gInSc1	Wharf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
náměstí	náměstí	k1gNnSc1	náměstí
Union	union	k1gInSc4	union
Square	square	k1gInSc1	square
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
admirála	admirál	k1gMnSc2	admirál
Deweyho	Dewey	k1gMnSc2	Dewey
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
připomínat	připomínat	k5eAaImF	připomínat
vítězství	vítězství	k1gNnSc4	vítězství
ve	v	k7c6	v
španělsko-americké	španělskomerický	k2eAgFnSc6d1	španělsko-americká
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Náměstí	náměstí	k1gNnSc1	náměstí
ale	ale	k9	ale
nese	nést	k5eAaImIp3nS	nést
jméno	jméno	k1gNnSc1	jméno
podle	podle	k7c2	podle
volebních	volební	k2eAgNnPc2d1	volební
shromáždění	shromáždění	k1gNnPc2	shromáždění
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Americké	americký	k2eAgFnSc2d1	americká
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Union	union	k1gInSc1	union
Square	square	k1gInSc1	square
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
obchodů	obchod	k1gInPc2	obchod
<g/>
,	,	kIx,	,
obchodní	obchodní	k2eAgInPc1d1	obchodní
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
butiky	butik	k1gInPc1	butik
<g/>
,	,	kIx,	,
hotely	hotel	k1gInPc1	hotel
a	a	k8xC	a
také	také	k9	také
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
na	na	k7c4	na
Geary	Geara	k1gFnPc4	Geara
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
divadelní	divadelní	k2eAgFnSc4d1	divadelní
čtvrť	čtvrť	k1gFnSc4	čtvrť
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Geary	Geara	k1gFnSc2	Geara
St.	st.	kA	st.
<g/>
,	,	kIx,	,
na	na	k7c6	na
menším	malý	k2eAgNnSc6d2	menší
náměstí	náměstí	k1gNnSc6	náměstí
A.	A.	kA	A.
S.	S.	kA	S.
Hallidie	Hallidie	k1gFnSc1	Hallidie
Plaza	plaz	k1gMnSc2	plaz
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgFnSc1d1	výchozí
zastávka	zastávka	k1gFnSc1	zastávka
sanfranciské	sanfranciský	k2eAgFnSc2d1	sanfranciská
pozemní	pozemní	k2eAgFnSc2d1	pozemní
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Financial	Financial	k1gMnSc1	Financial
District	District	k1gMnSc1	District
leží	ležet	k5eAaImIp3nS	ležet
severně	severně	k6eAd1	severně
od	od	k7c2	od
Market	market	k1gInSc1	market
St.	st.	kA	st.
Čtvrť	čtvrť	k1gFnSc1	čtvrť
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
výškovými	výškový	k2eAgFnPc7d1	výšková
budovami	budova	k1gFnPc7	budova
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zde	zde	k6eAd1	zde
postupně	postupně	k6eAd1	postupně
rostly	růst	k5eAaImAgInP	růst
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Market	market	k1gInSc1	market
St.	st.	kA	st.
a	a	k8xC	a
Kearny	Kearna	k1gFnSc2	Kearna
St.	st.	kA	st.
najdeme	najít	k5eAaPmIp1nP	najít
fontánu	fontán	k1gInSc2	fontán
Lotta	Lott	k1gInSc2	Lott
<g/>
'	'	kIx"	'
Fountain	Fountain	k1gInSc1	Fountain
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
shromažďovali	shromažďovat	k5eAaImAgMnP	shromažďovat
po	po	k7c6	po
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
a	a	k8xC	a
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
konci	konec	k1gInSc6	konec
Market	market	k1gInSc1	market
St.	st.	kA	st.
<g/>
,	,	kIx,	,
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Sanfranciského	sanfranciský	k2eAgInSc2d1	sanfranciský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
věží	věž	k1gFnSc7	věž
Ferry	Ferro	k1gNnPc7	Ferro
Building	Building	k1gInSc1	Building
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
katedrální	katedrální	k2eAgFnSc2d1	katedrální
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
Seville	Sevilla	k1gFnSc6	Sevilla
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
budovou	budova	k1gFnSc7	budova
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
konají	konat	k5eAaImIp3nP	konat
trhy	trh	k1gInPc4	trh
Ferry	Ferro	k1gNnPc7	Ferro
Plaza	plaz	k1gMnSc2	plaz
Farmers	Farmersa	k1gFnPc2	Farmersa
<g/>
'	'	kIx"	'
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
zakoupit	zakoupit	k5eAaPmF	zakoupit
místní	místní	k2eAgInPc4d1	místní
produkty	produkt	k1gInPc4	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
Ferry	Ferro	k1gNnPc7	Ferro
Building	Building	k1gInSc1	Building
Marketplace	Marketplace	k1gFnSc1	Marketplace
leží	ležet	k5eAaImIp3nS	ležet
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
třída	třída	k1gFnSc1	třída
The	The	k1gFnSc2	The
Embarcadero	Embarcadero	k1gNnSc1	Embarcadero
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
zde	zde	k6eAd1	zde
stála	stát	k5eAaImAgFnS	stát
dvoupatrová	dvoupatrový	k2eAgFnSc1d1	dvoupatrová
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
stržena	strhnout	k5eAaPmNgFnS	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
palmami	palma	k1gFnPc7	palma
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
přístavních	přístavní	k2eAgFnPc6d1	přístavní
budovách	budova	k1gFnPc6	budova
jsou	být	k5eAaImIp3nP	být
kavárny	kavárna	k1gFnPc1	kavárna
<g/>
,	,	kIx,	,
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
galerie	galerie	k1gFnSc1	galerie
a	a	k8xC	a
komerční	komerční	k2eAgFnPc1d1	komerční
prostory	prostora	k1gFnPc1	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Finanční	finanční	k2eAgFnSc2d1	finanční
čtvrti	čtvrt	k1gFnSc2	čtvrt
pak	pak	k6eAd1	pak
najdeme	najít	k5eAaPmIp1nP	najít
nejznámější	známý	k2eAgFnSc4d3	nejznámější
výškovou	výškový	k2eAgFnSc4d1	výšková
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
San	San	k1gFnSc6	San
Fraciscu	Fracisca	k1gFnSc4	Fracisca
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Transamerica	Transamericum	k1gNnSc2	Transamericum
Pyramid	pyramida	k1gFnPc2	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
má	mít	k5eAaImIp3nS	mít
48	[number]	k4	48
pater	patro	k1gNnPc2	patro
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
260	[number]	k4	260
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gMnSc7	její
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
losangeleský	losangeleský	k2eAgMnSc1d1	losangeleský
architekt	architekt	k1gMnSc1	architekt
W.	W.	kA	W.
Pereira	Pereira	k1gMnSc1	Pereira
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
budovy	budova	k1gFnSc2	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Transamerica	Transamerica	k1gFnSc1	Transamerica
Redwood	Redwood	k1gInSc4	Redwood
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
budovy	budova	k1gFnSc2	budova
lze	lze	k6eAd1	lze
místy	místy	k6eAd1	místy
narazit	narazit	k5eAaPmF	narazit
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
zástavbu	zástavba	k1gFnSc4	zástavba
z	z	k7c2	z
červených	červený	k2eAgFnPc2d1	červená
cihel	cihla	k1gFnPc2	cihla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dnes	dnes	k6eAd1	dnes
slouží	sloužit	k5eAaImIp3nS	sloužit
především	především	k6eAd1	především
jako	jako	k9	jako
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
čtvrť	čtvrť	k1gFnSc1	čtvrť
leží	ležet	k5eAaImIp3nS	ležet
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Finanční	finanční	k2eAgFnSc2d1	finanční
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
vznikat	vznikat	k5eAaImF	vznikat
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
pol.	pol.	k?	pol.
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
migrací	migrace	k1gFnSc7	migrace
čínských	čínský	k2eAgMnPc2d1	čínský
dělníků	dělník	k1gMnPc2	dělník
a	a	k8xC	a
námořníků	námořník	k1gMnPc2	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
st.	st.	kA	st.
ji	on	k3xPp3gFnSc4	on
postupně	postupně	k6eAd1	postupně
začali	začít	k5eAaPmAgMnP	začít
doplňovat	doplňovat	k5eAaImF	doplňovat
Korejci	Korejec	k1gMnPc1	Korejec
<g/>
,	,	kIx,	,
Vietnamci	Vietnamec	k1gMnPc1	Vietnamec
<g/>
,	,	kIx,	,
Thajci	Thajce	k1gMnPc1	Thajce
a	a	k8xC	a
Laosané	Laosaný	k2eAgFnPc1d1	Laosaný
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgNnPc1d3	nejzajímavější
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Stockton	Stockton	k1gInSc4	Stockton
a	a	k8xC	a
Grant	grant	k1gInSc4	grant
St.	st.	kA	st.
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
trhy	trh	k1gInPc4	trh
s	s	k7c7	s
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
pekařství	pekařství	k1gNnSc4	pekařství
<g/>
,	,	kIx,	,
bylinkářství	bylinkářství	k1gNnSc4	bylinkářství
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
čtvrť	čtvrť	k1gFnSc1	čtvrť
nabízí	nabízet	k5eAaImIp3nS	nabízet
nejnevkusnější	vkusný	k2eNgFnPc4d3	vkusný
fasády	fasáda	k1gFnPc4	fasáda
domů	dům	k1gInPc2	dům
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
památkou	památka	k1gFnSc7	památka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
katedrála	katedrála	k1gFnSc1	katedrála
Old	Olda	k1gFnPc2	Olda
Saint	Sainta	k1gFnPc2	Sainta
Mary	Mary	k1gFnSc2	Mary
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cathedral	Cathedral	k1gFnSc7	Cathedral
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1854	[number]	k4	1854
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
a	a	k8xC	a
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
North	North	k1gMnSc1	North
Beach	Beach	k1gMnSc1	Beach
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
od	od	k7c2	od
Čínské	čínský	k2eAgFnSc2d1	čínská
čtvrti	čtvrt	k1gFnSc2	čtvrt
a	a	k8xC	a
"	"	kIx"	"
<g/>
proslavila	proslavit	k5eAaPmAgNnP	proslavit
<g/>
"	"	kIx"	"
ji	on	k3xPp3gFnSc4	on
beatnická	beatnický	k2eAgFnSc1d1	beatnická
generace	generace	k1gFnSc1	generace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
ulicí	ulice	k1gFnSc7	ulice
je	být	k5eAaImIp3nS	být
Columbus	Columbus	k1gInSc1	Columbus
Ave	ave	k1gNnSc2	ave
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
řadu	řada	k1gFnSc4	řada
kaváren	kavárna	k1gFnPc2	kavárna
a	a	k8xC	a
knihkupectví	knihkupectví	k1gNnPc2	knihkupectví
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
City	City	k1gFnSc4	City
Lights	Lightsa	k1gFnPc2	Lightsa
Bookstore	Bookstor	k1gMnSc5	Bookstor
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
paperbackové	paperbackový	k2eAgNnSc1d1	paperbackové
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Columbus	Columbus	k1gInSc4	Columbus
Ave	ave	k1gNnSc2	ave
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Italská	italský	k2eAgFnSc1d1	italská
čtvrť	čtvrť	k1gFnSc1	čtvrť
se	s	k7c7	s
známou	známý	k2eAgFnSc7d1	známá
kavárnou	kavárna	k1gFnSc7	kavárna
Café	café	k1gNnSc2	café
Trieste	Triest	k1gInSc5	Triest
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
čtvrti	čtvrt	k1gFnSc2	čtvrt
je	být	k5eAaImIp3nS	být
Washington	Washington	k1gInSc1	Washington
Square	square	k1gInSc1	square
s	s	k7c7	s
římsko-katolickým	římskoatolický	k2eAgInSc7d1	římsko-katolický
novogotickým	novogotický	k2eAgInSc7d1	novogotický
chrámem	chrám	k1gInSc7	chrám
Saints	Saintsa	k1gFnPc2	Saintsa
Peter	Peter	k1gMnSc1	Peter
and	and	k?	and
Paul	Paul	k1gMnSc1	Paul
Church	Church	k1gMnSc1	Church
<g/>
.	.	kIx.	.
</s>
<s>
Telegraph	Telegraph	k1gMnSc1	Telegraph
Hill	Hill	k1gMnSc1	Hill
leží	ležet	k5eAaImIp3nS	ležet
východně	východně	k6eAd1	východně
od	od	k7c2	od
North	Northa	k1gFnPc2	Northa
Beach	Beacha	k1gFnPc2	Beacha
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
sanfranciská	sanfranciský	k2eAgFnSc1d1	sanfranciská
čtvrť	čtvrť	k1gFnSc1	čtvrť
s	s	k7c7	s
rodinnými	rodinný	k2eAgInPc7d1	rodinný
domy	dům	k1gInPc7	dům
v	v	k7c6	v
kopcovitém	kopcovitý	k2eAgInSc6d1	kopcovitý
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
turistickým	turistický	k2eAgInSc7d1	turistický
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
Coit	Coit	k2eAgInSc1d1	Coit
Tower	Tower	k1gInSc1	Tower
<g/>
,	,	kIx,	,
64	[number]	k4	64
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
na	na	k7c6	na
vrcholku	vrcholek	k1gInSc6	vrcholek
Telegraph	Telegraph	k1gMnSc1	Telegraph
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
místa	místo	k1gNnSc2	místo
je	být	k5eAaImIp3nS	být
pěkný	pěkný	k2eAgInSc4d1	pěkný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
rybářský	rybářský	k2eAgInSc1d1	rybářský
přístav	přístav	k1gInSc1	přístav
Fisherman	Fisherman	k1gMnSc1	Fisherman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wharf	Wharf	k1gInSc1	Wharf
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
turistických	turistický	k2eAgNnPc2d1	turistické
center	centrum	k1gNnPc2	centrum
San	San	k1gMnSc2	San
Francisca	Franciscus	k1gMnSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pier	pier	k1gInSc4	pier
39	[number]	k4	39
a	a	k8xC	a
41	[number]	k4	41
odplouvá	odplouvat	k5eAaImIp3nS	odplouvat
lodní	lodní	k2eAgFnSc1d1	lodní
doprava	doprava	k1gFnSc1	doprava
po	po	k7c6	po
Sanfranciském	sanfranciský	k2eAgInSc6d1	sanfranciský
zálivu	záliv	k1gInSc6	záliv
a	a	k8xC	a
výletní	výletní	k2eAgFnPc1d1	výletní
lodě	loď	k1gFnPc1	loď
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
je	být	k5eAaImIp3nS	být
Fishermań	Fishermań	k1gFnSc1	Fishermań
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wharf	Wharf	k1gMnSc1	Wharf
centrum	centrum	k1gNnSc4	centrum
nákupů	nákup	k1gInPc2	nákup
<g/>
,	,	kIx,	,
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
rychlých	rychlý	k2eAgNnPc2d1	rychlé
občerstvení	občerstvení	k1gNnPc2	občerstvení
a	a	k8xC	a
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
suvenýry	suvenýr	k1gInPc7	suvenýr
<g/>
.	.	kIx.	.
</s>
<s>
Západně	západně	k6eAd1	západně
od	od	k7c2	od
přístavu	přístav	k1gInSc2	přístav
Fisherman	Fisherman	k1gMnSc1	Fisherman
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Wharf	Wharf	k1gInSc1	Wharf
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
původně	původně	k6eAd1	původně
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
rekreační	rekreační	k2eAgFnSc1d1	rekreační
oblast	oblast	k1gFnSc1	oblast
a	a	k8xC	a
park	park	k1gInSc1	park
Fort	Fort	k?	Fort
Mason	mason	k1gMnSc1	mason
<g/>
.	.	kIx.	.
</s>
<s>
Fort	Fort	k?	Fort
Mason	mason	k1gMnSc1	mason
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
hlavním	hlavní	k2eAgMnSc7d1	hlavní
centrem	centr	k1gMnSc7	centr
naloďování	naloďování	k1gNnSc2	naloďování
a	a	k8xC	a
přeskupování	přeskupování	k1gNnSc2	přeskupování
námořních	námořní	k2eAgFnPc2d1	námořní
sil	síla	k1gFnPc2	síla
pro	pro	k7c4	pro
boje	boj	k1gInPc4	boj
v	v	k7c6	v
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
část	část	k1gFnSc1	část
Marina	Marina	k1gFnSc1	Marina
District	District	k1gInSc4	District
tvoří	tvořit	k5eAaImIp3nS	tvořit
přístaviště	přístaviště	k1gNnSc1	přístaviště
jachet	jachta	k1gFnPc2	jachta
a	a	k8xC	a
nábřeží	nábřeží	k1gNnSc4	nábřeží
s	s	k7c7	s
travnatou	travnatý	k2eAgFnSc7d1	travnatá
plochou	plocha	k1gFnSc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
bytová	bytový	k2eAgFnSc1d1	bytová
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
výběžku	výběžek	k1gInSc6	výběžek
čtvrti	čtvrt	k1gFnSc2	čtvrt
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1915	[number]	k4	1915
(	(	kIx(	(
<g/>
rekonstruovaná	rekonstruovaný	k2eAgFnSc1d1	rekonstruovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
určená	určený	k2eAgFnSc1d1	určená
k	k	k7c3	k
výstavám	výstava	k1gFnPc3	výstava
Palace	Palace	k1gFnSc2	Palace
of	of	k?	of
Fine	Fin	k1gMnSc5	Fin
Arts	Artsa	k1gFnPc2	Artsa
<g/>
.	.	kIx.	.
</s>
<s>
Russian	Russian	k1gMnSc1	Russian
Hill	Hill	k1gMnSc1	Hill
byl	být	k5eAaImAgMnS	být
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
po	po	k7c6	po
ruských	ruský	k2eAgMnPc6d1	ruský
námořnících	námořník	k1gMnPc6	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
tvoří	tvořit	k5eAaImIp3nS	tvořit
především	především	k9	především
bytová	bytový	k2eAgFnSc1d1	bytová
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
je	být	k5eAaImIp3nS	být
ulice	ulice	k1gFnSc1	ulice
Lombard	lombard	k1gInSc1	lombard
Street	Street	k1gInSc1	Street
s	s	k7c7	s
honosnými	honosný	k2eAgInPc7d1	honosný
domy	dům	k1gInPc7	dům
a	a	k8xC	a
především	především	k9	především
velmi	velmi	k6eAd1	velmi
prudkým	prudký	k2eAgNnSc7d1	prudké
klesáním	klesání	k1gNnSc7	klesání
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
vyřešeno	vyřešit	k5eAaPmNgNnS	vyřešit
8	[number]	k4	8
prudkými	prudký	k2eAgFnPc7d1	prudká
zatáčkami	zatáčka	k1gFnPc7	zatáčka
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
také	také	k9	také
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
Art	Art	k1gMnSc1	Art
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
západně	západně	k6eAd1	západně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc1	Mississippi
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Nob	Nob	k?	Nob
Hill	Hill	k1gMnSc1	Hill
leží	ležet	k5eAaImIp3nS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Russian	Russiana	k1gFnPc2	Russiana
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc4	čtvrť
tvoří	tvořit	k5eAaImIp3nP	tvořit
především	především	k6eAd1	především
typické	typický	k2eAgInPc1d1	typický
sanfranciské	sanfranciský	k2eAgInPc1d1	sanfranciský
dvoupatrové	dvoupatrový	k2eAgInPc1d1	dvoupatrový
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
dráha	dráha	k1gFnSc1	dráha
pozemní	pozemní	k2eAgFnSc2d1	pozemní
lanovky	lanovka	k1gFnSc2	lanovka
<g/>
,	,	kIx,	,
výhledy	výhled	k1gInPc1	výhled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnPc4d1	místní
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
bohatí	bohatý	k2eAgMnPc1d1	bohatý
průmyslníci	průmyslník	k1gMnPc1	průmyslník
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1	hlavní
náměstí	náměstí	k1gNnPc1	náměstí
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nS	křížit
California	Californium	k1gNnSc2	Californium
a	a	k8xC	a
Taylor	Taylor	k1gMnSc1	Taylor
St.	st.	kA	st.
Zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
novogotická	novogotický	k2eAgFnSc1d1	novogotická
katedrála	katedrála	k1gFnSc1	katedrála
Grace	Grace	k1gFnSc1	Grace
Cathedral	Cathedral	k1gFnSc1	Cathedral
postavená	postavený	k2eAgFnSc1d1	postavená
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
francouzských	francouzský	k2eAgFnPc2d1	francouzská
katedrál	katedrála	k1gFnPc2	katedrála
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1928	[number]	k4	1928
-	-	kIx~	-
64	[number]	k4	64
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
šířku	šířka	k1gFnSc4	šířka
49	[number]	k4	49
m	m	kA	m
a	a	k8xC	a
maximální	maximální	k2eAgFnSc4d1	maximální
výšku	výška	k1gFnSc4	výška
50	[number]	k4	50
m.	m.	k?	m.
Ceněná	ceněný	k2eAgFnSc1d1	ceněná
je	být	k5eAaImIp3nS	být
také	také	k9	také
budova	budova	k1gFnSc1	budova
hotelu	hotel	k1gInSc2	hotel
Fairmont	Fairmonto	k1gNnPc2	Fairmonto
San	San	k1gMnPc2	San
Francisco	Francisco	k6eAd1	Francisco
dokončená	dokončený	k2eAgFnSc1d1	dokončená
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1906	[number]	k4	1906
-	-	kIx~	-
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k1gNnSc1	Francisco
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
39	[number]	k4	39
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
neighborhoods	neighborhoods	k1gInSc1	neighborhoods
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
čtvrtí	čtvrt	k1gFnPc2	čtvrt
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
údajů	údaj	k1gInPc2	údaj
sanfranciského	sanfranciský	k2eAgNnSc2d1	sanfranciské
Oddělení	oddělení	k1gNnSc2	oddělení
územního	územní	k2eAgNnSc2d1	územní
plánování	plánování	k1gNnSc2	plánování
(	(	kIx(	(
<g/>
SF	SF	kA	SF
Planning	Planning	k1gInSc1	Planning
Department	department	k1gInSc1	department
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bayview	Bayview	k?	Bayview
<g/>
,	,	kIx,	,
Bernal	Bernal	k1gFnSc1	Bernal
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Castro	Castro	k1gNnSc1	Castro
<g/>
/	/	kIx~	/
<g/>
Upper	Upper	k1gInSc1	Upper
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Chinatown	Chinatown	k1gInSc1	Chinatown
<g/>
,	,	kIx,	,
Crocker	Crocker	k1gInSc1	Crocker
Amazon	amazona	k1gFnPc2	amazona
<g/>
,	,	kIx,	,
Diamond	Diamonda	k1gFnPc2	Diamonda
Heights	Heightsa	k1gFnPc2	Heightsa
<g/>
,	,	kIx,	,
Downtown	Downtown	k1gNnSc4	Downtown
Civic	Civice	k1gInPc2	Civice
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
Excelsior	Excelsior	k1gMnSc1	Excelsior
<g/>
,	,	kIx,	,
Financial	Financial	k1gMnSc1	Financial
District	District	k1gMnSc1	District
<g/>
,	,	kIx,	,
Glen	Glen	k1gMnSc1	Glen
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gate	k1gFnPc2	Gate
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Haight	Haight	k1gMnSc1	Haight
Ashbury	Ashbura	k1gFnSc2	Ashbura
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Inner	Inner	k1gMnSc1	Inner
Richmond	Richmond	k1gMnSc1	Richmond
<g/>
,	,	kIx,	,
Inner	Inner	k1gMnSc1	Inner
Sunset	Sunset	k1gMnSc1	Sunset
<g/>
,	,	kIx,	,
Lakeshore	Lakeshor	k1gMnSc5	Lakeshor
<g/>
,	,	kIx,	,
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
Mission	Mission	k1gInSc1	Mission
<g/>
,	,	kIx,	,
Nob	Nob	k1gMnSc1	Nob
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Noe	Noe	k1gMnSc1	Noe
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
North	North	k1gMnSc1	North
Beach	Beach	k1gMnSc1	Beach
<g/>
,	,	kIx,	,
Ocean	Ocean	k1gMnSc1	Ocean
View	View	k1gMnSc1	View
<g/>
,	,	kIx,	,
Outer	Outer	k1gMnSc1	Outer
Mission	Mission	k1gInSc1	Mission
<g/>
,	,	kIx,	,
Outer	Outer	k1gMnSc1	Outer
Richmond	Richmond	k1gMnSc1	Richmond
<g/>
,	,	kIx,	,
Outer	Outer	k1gMnSc1	Outer
Sunset	Sunset	k1gMnSc1	Sunset
<g/>
,	,	kIx,	,
Pacific	Pacific	k1gMnSc1	Pacific
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Parkside	Parksid	k1gMnSc5	Parksid
<g/>
,	,	kIx,	,
Potrero	Potrero	k1gNnSc1	Potrero
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Presidio	Presidio	k1gMnSc1	Presidio
<g/>
,	,	kIx,	,
Presidio	Presidio	k1gMnSc1	Presidio
Heights	Heights	k1gInSc1	Heights
<g/>
,	,	kIx,	,
Russian	Russian	k1gMnSc1	Russian
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Seacliff	Seacliff	k1gMnSc1	Seacliff
<g/>
,	,	kIx,	,
South	South	k1gMnSc1	South
Bayshore	Bayshor	k1gInSc5	Bayshor
<g/>
,	,	kIx,	,
South	South	k1gInSc1	South
of	of	k?	of
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Treasure	Treasur	k1gMnSc5	Treasur
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Twin	Twin	k1gInSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
,	,	kIx,	,
Upper	Upper	k1gInSc1	Upper
Market	market	k1gInSc1	market
<g/>
,	,	kIx,	,
Visitacion	Visitacion	k1gInSc1	Visitacion
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
West	West	k2eAgInSc1d1	West
of	of	k?	of
Twin	Twin	k1gInSc1	Twin
Peaks	Peaks	k1gInSc1	Peaks
<g/>
,	,	kIx,	,
Western	Western	kA	Western
Addition	Addition	k1gInSc1	Addition
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
<g/>
.	.	kIx.	.
</s>
<s>
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gate	k1gFnSc1	Gate
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
.	.	kIx.	.
<g/>
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
<g/>
-	-	kIx~	-
<g/>
Oakland	Oakland	k1gInSc1	Oakland
Bay	Bay	k1gMnSc2	Bay
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
velmi	velmi	k6eAd1	velmi
hustou	hustý	k2eAgFnSc7d1	hustá
sítí	síť	k1gFnSc7	síť
veřejné	veřejný	k2eAgFnSc2d1	veřejná
městské	městský	k2eAgFnSc2d1	městská
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
metra	metro	k1gNnSc2	metro
zde	zde	k6eAd1	zde
funguje	fungovat	k5eAaImIp3nS	fungovat
několik	několik	k4yIc4	několik
linek	linka	k1gFnPc2	linka
historických	historický	k2eAgFnPc2d1	historická
tramvají	tramvaj	k1gFnPc2	tramvaj
a	a	k8xC	a
množství	množství	k1gNnSc2	množství
linek	linka	k1gFnPc2	linka
trolejbusů	trolejbus	k1gInPc2	trolejbus
a	a	k8xC	a
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
protíná	protínat	k5eAaImIp3nS	protínat
síť	síť	k1gFnSc4	síť
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
napojeny	napojit	k5eAaPmNgFnP	napojit
na	na	k7c4	na
mosty	most	k1gInPc4	most
překračující	překračující	k2eAgFnSc2d1	překračující
vody	voda	k1gFnSc2	voda
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
přetíná	přetínat	k5eAaImIp3nS	přetínat
úžinu	úžina	k1gFnSc4	úžina
Golden	Goldna	k1gFnPc2	Goldna
Gate	Gat	k1gInSc2	Gat
most	most	k1gInSc4	most
Golden	Goldno	k1gNnPc2	Goldno
Gate	Gat	k1gMnSc2	Gat
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
,	,	kIx,	,
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
Oaklandu	Oakland	k1gInSc2	Oakland
vede	vést	k5eAaImIp3nS	vést
most	most	k1gInSc1	most
San	San	k1gFnSc2	San
Francisco-Oakland	Francisco-Oaklanda	k1gFnPc2	Francisco-Oaklanda
Bay	Bay	k1gFnSc7	Bay
Bridge	Bridge	k1gFnPc2	Bridge
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
mostech	most	k1gInPc6	most
se	se	k3xPyFc4	se
za	za	k7c4	za
vjezd	vjezd	k1gInSc4	vjezd
do	do	k7c2	do
města	město	k1gNnSc2	město
platí	platit	k5eAaImIp3nS	platit
mýto	mýto	k1gNnSc4	mýto
<g/>
.	.	kIx.	.
</s>
<s>
Sanfranciské	sanfranciský	k2eAgNnSc1d1	sanfranciské
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
leží	ležet	k5eAaImIp3nS	ležet
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dosažitelné	dosažitelný	k2eAgInPc4d1	dosažitelný
autobusy	autobus	k1gInPc4	autobus
a	a	k8xC	a
rychlodráhou	rychlodráha	k1gFnSc7	rychlodráha
BART	BART	kA	BART
<g/>
.	.	kIx.	.
</s>
<s>
Vlakové	vlakový	k2eAgFnPc1d1	vlaková
soupravy	souprava	k1gFnPc1	souprava
společnosti	společnost	k1gFnSc2	společnost
Caltrain	Caltraina	k1gFnPc2	Caltraina
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
spojení	spojení	k1gNnPc1	spojení
se	s	k7c7	s
San	San	k1gFnSc7	San
José	Josá	k1gFnSc2	Josá
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Rychlodráha	rychlodráha	k1gFnSc1	rychlodráha
BART	BART	kA	BART
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc4	město
s	s	k7c7	s
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
letištěm	letiště	k1gNnSc7	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Podmořským	podmořský	k2eAgInSc7d1	podmořský
tunelem	tunel	k1gInSc7	tunel
vede	vést	k5eAaImIp3nS	vést
trať	trať	k1gFnSc1	trať
rychlodráhy	rychlodráha	k1gFnSc2	rychlodráha
také	také	k9	také
na	na	k7c4	na
východ	východ	k1gInSc4	východ
do	do	k7c2	do
Oaklandu	Oakland	k1gInSc2	Oakland
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
linky	linka	k1gFnPc1	linka
do	do	k7c2	do
Dublinu	Dublin	k1gInSc2	Dublin
<g/>
,	,	kIx,	,
Fremontu	Fremont	k1gInSc2	Fremont
<g/>
,	,	kIx,	,
Pittsburgu	Pittsburg	k1gInSc2	Pittsburg
a	a	k8xC	a
Richmondu	Richmond	k1gInSc2	Richmond
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
prodloužení	prodloužení	k1gNnSc1	prodloužení
systému	systém	k1gInSc2	systém
BART	BART	kA	BART
přes	přes	k7c4	přes
Fremont	Fremont	k1gInSc4	Fremont
až	až	k9	až
do	do	k7c2	do
San	San	k1gFnSc2	San
José	Josá	k1gFnSc2	Josá
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Francisco	Francisco	k6eAd1	Francisco
představuje	představovat	k5eAaImIp3nS	představovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
námořní	námořní	k2eAgFnSc4d1	námořní
centrálu	centrála	k1gFnSc4	centrála
severní	severní	k2eAgFnSc2d1	severní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
805	[number]	k4	805
235	[number]	k4	235
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
48,5	[number]	k4	48,5
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
6,1	[number]	k4	6,1
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
33,3	[number]	k4	33,3
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,4	[number]	k4	0,4
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
6,6	[number]	k4	6,6
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
4,7	[number]	k4	4,7
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
15,1	[number]	k4	15,1
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jack	Jack	k1gMnSc1	Jack
London	London	k1gMnSc1	London
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
centrem	centrum	k1gNnSc7	centrum
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
umělců	umělec	k1gMnPc2	umělec
beatnické	beatnický	k2eAgFnSc2d1	beatnická
generace	generace	k1gFnSc2	generace
a	a	k8xC	a
stopy	stopa	k1gFnPc1	stopa
jejich	jejich	k3xOp3gNnSc2	jejich
působení	působení	k1gNnSc2	působení
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
znát	znát	k5eAaImF	znát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
známé	známý	k2eAgNnSc1d1	známé
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
a	a	k8xC	a
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
básníka	básník	k1gMnSc2	básník
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
Ferlinghettiho	Ferlinghetti	k1gMnSc2	Ferlinghetti
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
beatnických	beatnický	k2eAgMnPc2d1	beatnický
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
čtvrť	čtvrť	k1gFnSc1	čtvrť
Haight-Ashbury	Haight-Ashbura	k1gFnSc2	Haight-Ashbura
se	se	k3xPyFc4	se
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
létech	léto	k1gNnPc6	léto
stala	stát	k5eAaPmAgFnS	stát
centrem	centrum	k1gNnSc7	centrum
hnutí	hnutí	k1gNnSc4	hnutí
hippies	hippies	k1gInSc1	hippies
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
tisíců	tisíc	k4xCgInPc2	tisíc
příznivců	příznivec	k1gMnPc2	příznivec
zde	zde	k6eAd1	zde
žily	žít	k5eAaImAgFnP	žít
takové	takový	k3xDgFnPc1	takový
hudební	hudební	k2eAgFnPc1d1	hudební
hvězdy	hvězda	k1gFnPc1	hvězda
jako	jako	k8xC	jako
Janis	Janis	k1gFnPc1	Janis
Joplin	Joplina	k1gFnPc2	Joplina
a	a	k8xC	a
nebo	nebo	k8xC	nebo
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Grateful	Grateful	k1gInSc1	Grateful
Dead	Dead	k1gInSc1	Dead
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
centrem	centrum	k1gNnSc7	centrum
amerického	americký	k2eAgInSc2d1	americký
liberálního	liberální	k2eAgInSc2d1	liberální
aktivismu	aktivismus	k1gInSc2	aktivismus
a	a	k8xC	a
hnutí	hnutí	k1gNnSc2	hnutí
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
homosexuálů	homosexuál	k1gMnPc2	homosexuál
<g/>
.	.	kIx.	.
</s>
<s>
Abidjan	Abidjan	k1gInSc1	Abidjan
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
Assisi	Assis	k1gMnSc3	Assis
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Burgas	Burgas	k1gInSc1	Burgas
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
Caracas	Caracasa	k1gFnPc2	Caracasa
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
Cork	Cork	k1gInSc1	Cork
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
Haifa	Haifa	k1gFnSc1	Haifa
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Ho	on	k3xPp3gMnSc4	on
Či	či	k9wB	či
Minovo	Minův	k2eAgNnSc1d1	Minovo
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Vietnam	Vietnam	k1gInSc1	Vietnam
Manila	Manila	k1gFnSc1	Manila
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc1	Filipíny
Osaka	Osaek	k1gMnSc2	Osaek
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Soul	Soul	k1gInSc1	Soul
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Sydney	Sydney	k1gNnSc2	Sydney
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Thessaloniki	Thessalonik	k1gFnSc2	Thessalonik
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
Tchaj-pej	Tchajej	k1gFnSc2	Tchaj-pej
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
Vladivostok	Vladivostok	k1gInSc1	Vladivostok
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Zürich	Züricha	k1gFnPc2	Züricha
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
San	San	k1gFnPc2	San
Francisco	Francisco	k6eAd1	Francisco
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
http://sf-police.org/	[url]	k?	http://sf-police.org/
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Policie	policie	k1gFnSc1	policie
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
http://www.ci.sf.ca.us/	[url]	k?	http://www.ci.sf.ca.us/
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
http://www.sanfrancisco.cz/	[url]	k?	http://www.sanfrancisco.cz/
-	-	kIx~	-
turistické	turistický	k2eAgFnSc2d1	turistická
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
San	San	k1gMnSc6	San
Franciscu	Francisc	k1gMnSc6	Francisc
Galerie	galerie	k1gFnSc1	galerie
San	San	k1gFnSc1	San
Francisco	Francisco	k6eAd1	Francisco
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
