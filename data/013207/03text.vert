<p>
<s>
Besselův	Besselův	k2eAgInSc1d1	Besselův
elipsoid	elipsoid	k1gInSc1	elipsoid
(	(	kIx(	(
<g/>
také	také	k9	také
Besel	Besel	k1gInSc1	Besel
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
referenční	referenční	k2eAgInSc4d1	referenční
elipsoid	elipsoid	k1gInSc4	elipsoid
Země	zem	k1gFnSc2	zem
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Besselem	Bessel	k1gMnSc7	Bessel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
deseti	deset	k4xCc6	deset
meridionálních	meridionální	k2eAgInPc6d1	meridionální
obloucích	oblouk	k1gInPc6	oblouk
a	a	k8xC	a
osmatřiceti	osmatřicet	k4xCc2	osmatřicet
přesných	přesný	k2eAgFnPc2d1	přesná
měření	měření	k1gNnPc1	měření
astro-geografické	astroeografický	k2eAgFnSc2d1	astro-geografický
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměr	k1gInPc1	rozměr
os	osa	k1gFnPc2	osa
elipsoidu	elipsoid	k1gInSc2	elipsoid
byly	být	k5eAaImAgInP	být
definovány	definovat	k5eAaBmNgInP	definovat
pomocí	pomocí	k7c2	pomocí
logaritmů	logaritmus	k1gInPc2	logaritmus
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
početními	početní	k2eAgFnPc7d1	početní
metodami	metoda	k1gFnPc7	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
z	z	k7c2	z
Besselova	Besselův	k2eAgInSc2d1	Besselův
elipsoidu	elipsoid	k1gInSc2	elipsoid
vychází	vycházet	k5eAaImIp3nS	vycházet
Jednotná	jednotný	k2eAgFnSc1d1	jednotná
trigonometrická	trigonometrický	k2eAgFnSc1d1	trigonometrická
síť	síť	k1gFnSc1	síť
katastrální	katastrální	k2eAgFnSc1d1	katastrální
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
také	také	k9	také
např.	např.	kA	např.
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
Namibii	Namibie	k1gFnSc6	Namibie
nebo	nebo	k8xC	nebo
Eritreji	Eritrea	k1gFnSc6	Eritrea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměra	k1gFnPc4	rozměra
Besselova	Besselův	k2eAgInSc2d1	Besselův
elipsoidu	elipsoid	k1gInSc2	elipsoid
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
WGS-84	WGS-84	k4	WGS-84
</s>
</p>
<p>
<s>
GPS	GPS	kA	GPS
</s>
</p>
<p>
<s>
Geoid	geoid	k1gInSc1	geoid
</s>
</p>
<p>
<s>
Elipsoid	elipsoid	k1gInSc1	elipsoid
</s>
</p>
