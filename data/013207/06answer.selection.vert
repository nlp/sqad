<s>
Besselův	Besselův	k2eAgInSc1d1	Besselův
elipsoid	elipsoid	k1gInSc1	elipsoid
(	(	kIx(	(
<g/>
také	také	k9	také
Besel	Besel	k1gInSc1	Besel
1841	[number]	k4	1841
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
referenční	referenční	k2eAgInSc4d1	referenční
elipsoid	elipsoid	k1gInSc4	elipsoid
Země	zem	k1gFnSc2	zem
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1841	[number]	k4	1841
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Besselem	Bessel	k1gMnSc7	Bessel
<g/>
.	.	kIx.	.
</s>
