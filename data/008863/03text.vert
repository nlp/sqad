<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Brazílie	Brazílie	k1gFnSc2	Brazílie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
podobě	podoba	k1gFnSc6	podoba
přijata	přijat	k2eAgFnSc1d1	přijata
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mírně	mírně	k6eAd1	mírně
upravenou	upravený	k2eAgFnSc4d1	upravená
původní	původní	k2eAgFnSc4d1	původní
vlajku	vlajka	k1gFnSc4	vlajka
užívanou	užívaný	k2eAgFnSc4d1	užívaná
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
ji	on	k3xPp3gFnSc4	on
malíř	malíř	k1gMnSc1	malíř
Decio	Decio	k1gMnSc1	Decio
Vilares	Vilares	k1gMnSc1	Vilares
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
zeleného	zelený	k2eAgInSc2d1	zelený
obdélníku	obdélník	k1gInSc2	obdélník
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
stran	strana	k1gFnPc2	strana
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
modré	modrý	k2eAgNnSc1d1	modré
kruhové	kruhový	k2eAgNnSc1d1	kruhové
pole	pole	k1gNnSc1	pole
podložené	podložený	k2eAgNnSc1d1	podložené
žlutým	žlutý	k2eAgNnSc7d1	žluté
kosočtverečním	kosočtvereční	k2eAgNnSc7d1	kosočtvereční
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
27	[number]	k4	27
bílých	bílý	k2eAgFnPc2d1	bílá
hvězd	hvězda	k1gFnPc2	hvězda
pěti	pět	k4xCc2	pět
různých	různý	k2eAgFnPc2d1	různá
velikostí	velikost	k1gFnPc2	velikost
a	a	k8xC	a
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
protnuto	protnout	k5eAaPmNgNnS	protnout
bílou	bílý	k2eAgFnSc7d1	bílá
stuhou	stuha	k1gFnSc7	stuha
se	s	k7c7	s
zeleným	zelený	k2eAgInSc7d1	zelený
nápisem	nápis	k1gInSc7	nápis
Ordem	Ordem	k1gInSc1	Ordem
e	e	k0	e
Progresso	Progressa	k1gFnSc5	Progressa
(	(	kIx(	(
<g/>
řád	řád	k1gInSc4	řád
a	a	k8xC	a
pokrok	pokrok	k1gInSc4	pokrok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
figura	figura	k1gFnSc1	figura
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
Nebe	nebe	k1gNnSc1	nebe
nad	nad	k7c7	nad
Riem	Rie	k1gNnSc7	Rie
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
i	i	k8xC	i
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Zelená	zelený	k2eAgFnSc1d1	zelená
barva	barva	k1gFnSc1	barva
bohatství	bohatství	k1gNnSc2	bohatství
přírodní	přírodní	k2eAgFnSc1d1	přírodní
<g/>
,	,	kIx,	,
kouzlo	kouzlo	k1gNnSc1	kouzlo
brazilských	brazilský	k2eAgInPc2d1	brazilský
pralesů	prales	k1gInPc2	prales
<g/>
;	;	kIx,	;
žlutá	žlutý	k2eAgFnSc1d1	žlutá
představuje	představovat	k5eAaImIp3nS	představovat
zlato	zlato	k1gNnSc4	zlato
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
důvodem	důvod	k1gInSc7	důvod
vzniku	vznik	k1gInSc2	vznik
mnoha	mnoho	k4c2	mnoho
důležitých	důležitý	k2eAgNnPc2d1	důležité
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Motto	motto	k1gNnSc1	motto
Ordem	Ordem	k1gInSc1	Ordem
e	e	k0	e
Progresso	Progressa	k1gFnSc5	Progressa
(	(	kIx(	(
<g/>
řád	řád	k1gInSc4	řád
a	a	k8xC	a
pokrok	pokrok	k1gInSc4	pokrok
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
heslem	heslo	k1gNnSc7	heslo
pozitivismu	pozitivismus	k1gInSc2	pozitivismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Auguste	August	k1gMnSc5	August
Comte	Comt	k1gMnSc5	Comt
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
amour	amour	k1gMnSc1	amour
pour	pour	k1gMnSc1	pour
principe	princip	k1gInSc5	princip
et	et	k?	et
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
ordre	ordre	k1gInSc1	ordre
pour	pour	k1gInSc1	pour
base	basa	k1gFnSc3	basa
<g/>
;	;	kIx,	;
le	le	k?	le
progrè	progrè	k?	progrè
pour	pour	k1gInSc1	pour
but	but	k?	but
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
vlajek	vlajka	k1gFnPc2	vlajka
Brazílie	Brazílie	k1gFnSc2	Brazílie
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
byla	být	k5eAaImAgFnS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1822	[number]	k4	1822
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
nezávislost	nezávislost	k1gFnSc1	nezávislost
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
první	první	k4xOgFnSc1	první
brazilská	brazilský	k2eAgFnSc1d1	brazilská
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vlajka	vlajka	k1gFnSc1	vlajka
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
Brazílie	Brazílie	k1gFnSc2	Brazílie
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
ze	z	k7c2	z
zeleného	zelený	k2eAgInSc2d1	zelený
listu	list	k1gInSc2	list
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
žlutý	žlutý	k2eAgInSc1d1	žlutý
kosočtverec	kosočtverec	k1gInSc1	kosočtverec
a	a	k8xC	a
na	na	k7c6	na
kosočtverci	kosočtverec	k1gInSc6	kosočtverec
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
císařství	císařství	k1gNnSc2	císařství
se	s	k7c7	s
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1822	[number]	k4	1822
změnila	změnit	k5eAaPmAgFnS	změnit
vlajka	vlajka	k1gFnSc1	vlajka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
koruna	koruna	k1gFnSc1	koruna
nad	nad	k7c7	nad
znakem	znak	k1gInSc7	znak
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
císařskou	císařský	k2eAgFnSc7d1	císařská
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
byla	být	k5eAaImAgFnS	být
vlajka	vlajka	k1gFnSc1	vlajka
mírně	mírně	k6eAd1	mírně
změněna	změnit	k5eAaPmNgFnS	změnit
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
byla	být	k5eAaImAgFnS	být
přidána	přidán	k2eAgFnSc1d1	přidána
hvězda	hvězda	k1gFnSc1	hvězda
reprezentující	reprezentující	k2eAgFnSc2d1	reprezentující
nový	nový	k2eAgInSc4d1	nový
stát	stát	k1gInSc4	stát
Cisplatie	Cisplatie	k1gFnSc2	Cisplatie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Uruguay	Uruguay	k1gFnSc1	Uruguay
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
monarchie	monarchie	k1gFnSc2	monarchie
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1889	[number]	k4	1889
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
zavedena	zaveden	k2eAgFnSc1d1	zavedena
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
vlajkou	vlajka	k1gFnSc7	vlajka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
platila	platit	k5eAaImAgFnS	platit
jen	jen	k9	jen
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
od	od	k7c2	od
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
republiky	republika	k1gFnSc2	republika
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
uvedení	uvedení	k1gNnSc2	uvedení
současného	současný	k2eAgInSc2d1	současný
vzoru	vzor	k1gInSc2	vzor
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
podobala	podobat	k5eAaImAgFnS	podobat
vlajce	vlajka	k1gFnSc3	vlajka
jiného	jiný	k2eAgInSc2d1	jiný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
19	[number]	k4	19
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
však	však	k9	však
Brazílie	Brazílie	k1gFnSc1	Brazílie
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
vlajce	vlajka	k1gFnSc3	vlajka
<g/>
,	,	kIx,	,
jen	jen	k9	jen
(	(	kIx(	(
<g/>
císařský	císařský	k2eAgInSc4d1	císařský
<g/>
)	)	kIx)	)
znak	znak	k1gInSc4	znak
nahradila	nahradit	k5eAaPmAgFnS	nahradit
modrou	modrý	k2eAgFnSc7d1	modrá
nebeskou	nebeský	k2eAgFnSc7d1	nebeská
sférou	sféra	k1gFnSc7	sféra
se	s	k7c7	s
stuhou	stuha	k1gFnSc7	stuha
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
Ordem	Ordem	k1gInSc1	Ordem
e	e	k0	e
progresso	progressa	k1gFnSc5	progressa
<g/>
,	,	kIx,	,
a	a	k8xC	a
žlutý	žlutý	k2eAgInSc1d1	žlutý
kosočtverec	kosočtverec	k1gInSc1	kosočtverec
byl	být	k5eAaImAgInS	být
zmenšen	zmenšen	k2eAgMnSc1d1	zmenšen
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nedotýkal	dotýkat	k5eNaImAgInS	dotýkat
okraje	okraj	k1gInPc4	okraj
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
)	)	kIx)	)
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
počtu	počet	k1gInSc3	počet
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
brazilské	brazilský	k2eAgFnSc2d1	brazilská
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
federálního	federální	k2eAgInSc2d1	federální
distriktu	distrikt	k1gInSc2	distrikt
(	(	kIx(	(
<g/>
Distrito	Distrita	k1gFnSc5	Distrita
Federal	Federal	k1gMnSc7	Federal
do	do	k7c2	do
Brasil	Brasil	k1gFnSc1	Brasil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Brasílií	Brasílie	k1gFnPc2	Brasílie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozmístění	rozmístění	k1gNnSc1	rozmístění
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
jejich	jejich	k3xOp3gFnSc1	jejich
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
od	od	k7c2	od
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
brazilské	brazilský	k2eAgInPc1d1	brazilský
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
rozdělovaly	rozdělovat	k5eAaImAgInP	rozdělovat
<g/>
)	)	kIx)	)
několikrát	několikrát	k6eAd1	několikrát
nepatrně	patrně	k6eNd1	patrně
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
11	[number]	k4	11
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hvězdy	hvězda	k1gFnPc1	hvězda
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
brazilské	brazilský	k2eAgFnSc6d1	brazilská
vlajce	vlajka	k1gFnSc6	vlajka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
27	[number]	k4	27
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prokyon	Prokyon	k1gNnSc1	Prokyon
(	(	kIx(	(
<g/>
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Malého	Malý	k1gMnSc4	Malý
psa	pes	k1gMnSc4	pes
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Velkého	velký	k2eAgMnSc2d1	velký
psa	pes	k1gMnSc2	pes
-	-	kIx~	-
5	[number]	k4	5
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
představuje	představovat	k5eAaImIp3nS	představovat
Sirius	Sirius	k1gInSc1	Sirius
</s>
</p>
<p>
<s>
Canopus	Canopus	k1gInSc1	Canopus
(	(	kIx(	(
<g/>
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Lodního	lodní	k2eAgInSc2d1	lodní
kýlu	kýl	k1gInSc2	kýl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spica	Spica	k1gFnSc1	Spica
(	(	kIx(	(
<g/>
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Panny	Panna	k1gFnSc2	Panna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Hydry	hydra	k1gFnSc2	hydra
-	-	kIx~	-
2	[number]	k4	2
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnPc4d3	veliký
představuje	představovat	k5eAaImIp3nS	představovat
Alphard	Alphard	k1gInSc1	Alphard
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Jižního	jižní	k2eAgInSc2d1	jižní
kříže	kříž	k1gInSc2	kříž
-	-	kIx~	-
5	[number]	k4	5
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
představuje	představovat	k5eAaImIp3nS	představovat
Acrux	Acrux	k1gInSc1	Acrux
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sigma	sigma	k1gNnSc1	sigma
Octantis	Octantis	k1gFnSc2	Octantis
(	(	kIx(	(
<g/>
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Oktantu	oktant	k1gInSc2	oktant
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Jižní	jižní	k2eAgFnSc1d1	jižní
Polárka	Polárka	k1gFnSc1	Polárka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Jižního	jižní	k2eAgInSc2d1	jižní
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
-	-	kIx~	-
3	[number]	k4	3
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnPc4d1	stejná
velikosti	velikost	k1gFnPc4	velikost
</s>
</p>
<p>
<s>
Souhvězdí	souhvězdí	k1gNnSc1	souhvězdí
Štíra	štír	k1gMnSc2	štír
-	-	kIx~	-
8	[number]	k4	8
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
představuje	představovat	k5eAaImIp3nS	představovat
Antares	Antares	k1gInSc1	Antares
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Brazílie	Brazílie	k1gFnSc1	Brazílie
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
26	[number]	k4	26
spolkových	spolkový	k2eAgInPc2d1	spolkový
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
estados	estados	k1gInSc1	estados
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc1d1	jednotné
číslo	číslo	k1gNnSc1	číslo
estado	estada	k1gFnSc5	estada
<g/>
)	)	kIx)	)
a	a	k8xC	a
1	[number]	k4	1
federální	federální	k2eAgInSc4d1	federální
distrikt	distrikt	k1gInSc4	distrikt
(	(	kIx(	(
<g/>
distrito	distrita	k1gFnSc5	distrita
federal	federat	k5eAaPmAgInS	federat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
brazilské	brazilský	k2eAgNnSc1d1	brazilské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Brasília	Brasílium	k1gNnSc2	Brasílium
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
útvary	útvar	k1gInPc1	útvar
mají	mít	k5eAaImIp3nP	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
statistické	statistický	k2eAgInPc4d1	statistický
účely	účel	k1gInPc4	účel
jsou	být	k5eAaImIp3nP	být
brazilské	brazilský	k2eAgInPc4d1	brazilský
státy	stát	k1gInPc4	stát
sloučeny	sloučen	k2eAgInPc4d1	sloučen
do	do	k7c2	do
pěti	pět	k4xCc2	pět
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brazílie	Brazílie	k1gFnSc2	Brazílie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
