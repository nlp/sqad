<s>
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
je	on	k3xPp3gMnPc4	on
venkovský	venkovský	k2eAgInSc1d1	venkovský
realistický	realistický	k2eAgInSc1d1	realistický
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
Rais	Rais	k1gMnSc1	Rais
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Adolf	Adolf	k1gMnSc1	Adolf
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
.	.	kIx.	.
</s>
