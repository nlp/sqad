<p>
<s>
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
je	on	k3xPp3gMnPc4	on
venkovský	venkovský	k2eAgInSc1d1	venkovský
realistický	realistický	k2eAgInSc1d1	realistický
historický	historický	k2eAgInSc1d1	historický
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
Rais	Rais	k1gMnSc1	Rais
a	a	k8xC	a
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
Adolf	Adolf	k1gMnSc1	Adolf
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
časopisecky	časopisecky	k6eAd1	časopisecky
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
ve	v	k7c6	v
Světozoru	světozor	k1gInSc6	světozor
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
u	u	k7c2	u
K.	K.	kA	K.
Šimáčka	Šimáček	k1gMnSc4	Šimáček
knižně	knižně	k6eAd1	knižně
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
zápisky	zápisek	k1gInPc4	zápisek
Věnceslava	Věnceslava	k1gFnSc1	Věnceslava
Metelky	Metelky	k?	Metelky
<g/>
,	,	kIx,	,
učitele	učitel	k1gMnSc4	učitel
v	v	k7c6	v
Pasekách	paseka	k1gFnPc6	paseka
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čermák	Čermák	k1gMnSc1	Čermák
</s>
</p>
<p>
<s>
učitel	učitel	k1gMnSc1	učitel
Václav	Václav	k1gMnSc1	Václav
Čížek	Čížek	k1gMnSc1	Čížek
</s>
</p>
<p>
<s>
farář	farář	k1gMnSc1	farář
Prokop	Prokop	k1gMnSc1	Prokop
Stehlík	Stehlík	k1gMnSc1	Stehlík
</s>
</p>
<p>
<s>
Albínka	Albínka	k1gFnSc1	Albínka
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Čermák	Čermák	k1gMnSc1	Čermák
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jako	jako	k9	jako
učitelský	učitelský	k2eAgMnSc1d1	učitelský
pomocník	pomocník	k1gMnSc1	pomocník
v	v	k7c6	v
Pozdětíně	Pozdětína	k1gFnSc6	Pozdětína
<g/>
,	,	kIx,	,
malé	malý	k2eAgFnSc6d1	malá
vesničce	vesnička	k1gFnSc6	vesnička
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
srdečně	srdečně	k6eAd1	srdečně
přijat	přijmout	k5eAaPmNgInS	přijmout
učitelem	učitel	k1gMnSc7	učitel
Čižkem	Čižek	k1gMnSc7	Čižek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
učitel	učitel	k1gMnSc1	učitel
i	i	k8xC	i
pan	pan	k1gMnSc1	pan
farář	farář	k1gMnSc1	farář
byli	být	k5eAaImAgMnP	být
muzikanty	muzikant	k1gMnPc4	muzikant
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Čermák	Čermák	k1gMnSc1	Čermák
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
muzikantem	muzikant	k1gMnSc7	muzikant
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc4	jejich
sympatie	sympatie	k1gFnPc4	sympatie
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
ještě	ještě	k9	ještě
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
<g/>
.	.	kIx.	.
</s>
<s>
Pomocník	pomocník	k1gMnSc1	pomocník
Čermák	Čermák	k1gMnSc1	Čermák
se	se	k3xPyFc4	se
při	při	k7c6	při
častých	častý	k2eAgFnPc6d1	častá
návštěvách	návštěva	k1gFnPc6	návštěva
na	na	k7c6	na
faře	fara	k1gFnSc6	fara
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Albínkou	Albínka	k1gFnSc7	Albínka
<g/>
,	,	kIx,	,
farářovou	farářův	k2eAgFnSc7d1	farářova
neteří	neteř	k1gFnSc7	neteř
a	a	k8xC	a
zamilovali	zamilovat	k5eAaPmAgMnP	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Pomocníka	pomocník	k1gMnSc4	pomocník
však	však	k9	však
sváděla	svádět	k5eAaImAgFnS	svádět
vdova	vdova	k1gFnSc1	vdova
Žaláková	Žaláková	k1gFnSc1	Žaláková
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
řekla	říct	k5eAaPmAgFnS	říct
Albínčině	Albínčin	k2eAgMnSc6d1	Albínčin
otci	otec	k1gMnSc6	otec
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Čermák	Čermák	k1gMnSc1	Čermák
nemanželské	manželský	k2eNgNnSc4d1	nemanželské
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ho	on	k3xPp3gNnSc4	on
velmi	velmi	k6eAd1	velmi
pohoršilo	pohoršit	k5eAaPmAgNnS	pohoršit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
poslal	poslat	k5eAaPmAgMnS	poslat
Albínku	Albínka	k1gFnSc4	Albínka
studovat	studovat	k5eAaImF	studovat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
slavnosti	slavnost	k1gFnSc6	slavnost
byl	být	k5eAaImAgMnS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
i	i	k9	i
inspektor	inspektor	k1gMnSc1	inspektor
<g/>
,	,	kIx,	,
pravý	pravý	k2eAgMnSc1d1	pravý
Čermákuv	Čermákuv	k1gMnSc1	Čermákuv
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
zhroutil	zhroutit	k5eAaPmAgInS	zhroutit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
učitelem	učitel	k1gMnSc7	učitel
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Čermák	Čermák	k1gMnSc1	Čermák
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Albínkou	Albínka	k1gFnSc7	Albínka
<g/>
.	.	kIx.	.
</s>
<s>
Čermák	Čermák	k1gMnSc1	Čermák
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
stane	stanout	k5eAaPmIp3nS	stanout
kantorem	kantor	k1gMnSc7	kantor
a	a	k8xC	a
Albínčin	Albínčin	k2eAgMnSc1d1	Albínčin
otec	otec	k1gMnSc1	otec
jim	on	k3xPp3gMnPc3	on
svatbu	svatba	k1gFnSc4	svatba
povolí	povolit	k5eAaPmIp3nS	povolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
podoba	podoba	k1gFnSc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
<g/>
,	,	kIx,	,
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
natočil	natočit	k5eAaBmAgMnS	natočit
režisér	režisér	k1gMnSc1	režisér
Miroslav	Miroslav	k1gMnSc1	Miroslav
Josef	Josef	k1gMnSc1	Josef
Krňanský	Krňanský	k2eAgMnSc1d1	Krňanský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Dílo	dílo	k1gNnSc1	dílo
online	onlinout	k5eAaPmIp3nS	onlinout
===	===	k?	===
</s>
</p>
<p>
<s>
RAIS	Rais	k1gMnSc1	Rais
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
K.	K.	kA	K.
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
.	.	kIx.	.
380	[number]	k4	380
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RAIS	Rais	k1gMnSc1	Rais
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Unie	unie	k1gFnSc1	unie
<g/>
,	,	kIx,	,
1909	[number]	k4	1909
<g/>
.	.	kIx.	.
461	[number]	k4	461
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Zapadlí	zapadlý	k2eAgMnPc1d1	zapadlý
vlastenci	vlastenec	k1gMnPc1	vlastenec
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
</s>
</p>
