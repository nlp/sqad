<s>
Tautologie	tautologie	k1gFnSc1	tautologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
τ	τ	k?	τ
<g/>
,	,	kIx,	,
tautologia	tautologia	k1gFnSc1	tautologia
<g/>
,	,	kIx,	,
výpověď	výpověď	k1gFnSc1	výpověď
o	o	k7c6	o
témže	týž	k3xTgInSc6	týž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
vždy	vždy	k6eAd1	vždy
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
složený	složený	k2eAgInSc1d1	složený
výrok	výrok	k1gInSc1	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pravdivý	pravdivý	k2eAgInSc1d1	pravdivý
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pravdivostní	pravdivostní	k2eAgFnSc4d1	pravdivostní
hodnotu	hodnota	k1gFnSc4	hodnota
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
takového	takový	k3xDgInSc2	takový
výroku	výrok	k1gInSc2	výrok
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
tautologie	tautologie	k1gFnSc2	tautologie
je	být	k5eAaImIp3nS	být
výrok	výrok	k1gInSc1	výrok
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Buď	buď	k8xC	buď
bude	být	k5eAaImBp3nS	být
zítra	zítra	k6eAd1	zítra
pršet	pršet	k5eAaImF	pršet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zítra	zítra	k6eAd1	zítra
pršet	pršet	k5eAaImF	pršet
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tautologií	tautologie	k1gFnSc7	tautologie
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
některé	některý	k3yIgFnPc1	některý
definice	definice	k1gFnPc1	definice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Kampanologie	Kampanologie	k1gFnSc1	Kampanologie
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
vznikem	vznik	k1gInSc7	vznik
<g/>
,	,	kIx,	,
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
akustikou	akustika	k1gFnSc7	akustika
zvonů	zvon	k1gInPc2	zvon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výrok	výrok	k1gInSc1	výrok
neříká	říkat	k5eNaImIp3nS	říkat
vlastně	vlastně	k9	vlastně
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
že	že	k8xS	že
A	A	kA	A
=	=	kIx~	=
A.	A.	kA	A.
Jiným	jiný	k2eAgInSc7d1	jiný
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
nepravé	pravý	k2eNgFnPc4d1	nepravá
tautologie	tautologie	k1gFnPc4	tautologie
<g/>
,	,	kIx,	,
výroky	výrok	k1gInPc4	výrok
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
tautologické	tautologický	k2eAgNnSc4d1	tautologické
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
kontextem	kontext	k1gInSc7	kontext
získávají	získávat	k5eAaImIp3nP	získávat
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
Až	až	k9	až
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
zhruba	zhruba	k6eAd1	zhruba
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nedá	dát	k5eNaPmIp3nS	dát
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
urychlit	urychlit	k5eAaPmF	urychlit
<g/>
,	,	kIx,	,
musíš	muset	k5eAaImIp2nS	muset
počkat	počkat	k5eAaPmF	počkat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Výrok	výrok	k1gInSc1	výrok
"	"	kIx"	"
<g/>
Děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
získá	získat	k5eAaPmIp3nS	získat
svůj	svůj	k3xOyFgInSc4	svůj
<g />
.	.	kIx.	.
</s>
<s>
význam	význam	k1gInSc1	význam
v	v	k7c6	v
dialogu	dialog	k1gInSc6	dialog
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Vysvětloval	vysvětlovat	k5eAaImAgMnS	vysvětlovat
jsem	být	k5eAaImIp1nS	být
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesmí	smět	k5eNaImIp3nS	smět
sám	sám	k3xTgMnSc1	sám
přecházet	přecházet	k5eAaImF	přecházet
silnici	silnice	k1gFnSc3	silnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vůbec	vůbec	k9	vůbec
mě	já	k3xPp1nSc4	já
neposlouchá	poslouchat	k5eNaImIp3nS	poslouchat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
-	-	kIx~	-
"	"	kIx"	"
<g/>
To	ten	k3xDgNnSc1	ten
víš	vědět	k5eAaImIp2nS	vědět
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mluvčí	mluvčí	k1gMnSc1	mluvčí
tím	ten	k3xDgInSc7	ten
má	mít	k5eAaImIp3nS	mít
namysli	namyslet	k5eAaPmRp2nS	namyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
neposlušnost	neposlušnost	k1gFnSc1	neposlušnost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
vyplývající	vyplývající	k2eAgInPc1d1	vyplývající
ze	z	k7c2	z
samotného	samotný	k2eAgNnSc2d1	samotné
označení	označení	k1gNnSc2	označení
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
výroky	výrok	k1gInPc1	výrok
"	"	kIx"	"
<g/>
doma	doma	k6eAd1	doma
je	být	k5eAaImIp3nS	být
doma	doma	k6eAd1	doma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
udělal	udělat	k5eAaPmAgMnS	udělat
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
výroky	výrok	k1gInPc1	výrok
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
"	"	kIx"	"
<g/>
A	a	k9	a
není	být	k5eNaImIp3nS	být
neA	neA	k?	neA
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
mír	mír	k1gInSc1	mír
není	být	k5eNaImIp3nS	být
válka	válka	k1gFnSc1	válka
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
černé	černý	k2eAgNnSc1d1	černé
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tautologie	tautologie	k1gFnSc1	tautologie
je	být	k5eAaImIp3nS	být
také	také	k9	také
nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
zdvojení	zdvojení	k1gNnSc1	zdvojení
téhož	týž	k3xTgInSc2	týž
významu	význam	k1gInSc2	význam
různými	různý	k2eAgNnPc7d1	různé
slovy	slovo	k1gNnPc7	slovo
nebo	nebo	k8xC	nebo
částmi	část	k1gFnPc7	část
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgInPc4d1	označovaný
též	též	k9	též
pojmem	pojem	k1gInSc7	pojem
pleonasmus	pleonasmus	k1gInSc1	pleonasmus
nebo	nebo	k8xC	nebo
perisologie	perisologie	k1gFnSc1	perisologie
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
mohou	moct	k5eAaImIp3nP	moct
patřit	patřit	k5eAaImF	patřit
např.	např.	kA	např.
výrazy	výraz	k1gInPc4	výraz
"	"	kIx"	"
<g/>
vždy	vždy	k6eAd1	vždy
a	a	k8xC	a
navěky	navěky	k6eAd1	navěky
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zcela	zcela	k6eAd1	zcela
a	a	k8xC	a
úplně	úplně	k6eAd1	úplně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
třídílná	třídílný	k2eAgFnSc1d1	třídílná
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nejoptimálnější	nejoptimálnější	k2eAgFnSc4d1	nejoptimálnější
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
Taková	takový	k3xDgFnSc1	takový
tautologie	tautologie	k1gFnPc4	tautologie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
stylistickou	stylistický	k2eAgFnSc7d1	stylistická
chybou	chyba	k1gFnSc7	chyba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výrazovým	výrazový	k2eAgInSc7d1	výrazový
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
umocnit	umocnit	k5eAaPmF	umocnit
některou	některý	k3yIgFnSc4	některý
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
daného	daný	k2eAgInSc2d1	daný
předmětu	předmět	k1gInSc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
dárek	dárek	k1gInSc1	dárek
<g/>
"	"	kIx"	"
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
představu	představa	k1gFnSc4	představa
svobodného	svobodný	k2eAgNnSc2d1	svobodné
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
dát	dát	k5eAaPmF	dát
někomu	někdo	k3yInSc3	někdo
něco	něco	k3yInSc1	něco
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
zdarma	zdarma	k6eAd1	zdarma
daný	daný	k2eAgInSc4d1	daný
dárek	dárek	k1gInSc4	dárek
<g/>
"	"	kIx"	"
chce	chtít	k5eAaImIp3nS	chtít
odlišit	odlišit	k5eAaPmF	odlišit
takový	takový	k3xDgInSc4	takový
dárek	dárek	k1gInSc4	dárek
od	od	k7c2	od
různých	různý	k2eAgInPc2d1	různý
bonusů	bonus	k1gInPc2	bonus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
také	také	k9	také
označeny	označen	k2eAgFnPc1d1	označena
"	"	kIx"	"
<g/>
zdarma	zdarma	k6eAd1	zdarma
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zákazník	zákazník	k1gMnSc1	zákazník
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
přitom	přitom	k6eAd1	přitom
zaplatí	zaplatit	k5eAaPmIp3nP	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
tautologie	tautologie	k1gFnSc2	tautologie
v	v	k7c6	v
logice	logika	k1gFnSc6	logika
je	být	k5eAaImIp3nS	být
kontradikce	kontradikce	k1gFnSc1	kontradikce
<g/>
,	,	kIx,	,
v	v	k7c6	v
lingvistickém	lingvistický	k2eAgInSc6d1	lingvistický
smyslu	smysl	k1gInSc6	smysl
oxymóron	oxymóron	k1gNnSc1	oxymóron
<g/>
.	.	kIx.	.
</s>
<s>
Pleonasmus	pleonasmus	k1gInSc1	pleonasmus
Oxymoron	Oxymoron	k1gInSc1	Oxymoron
Spor	spor	k1gInSc1	spor
(	(	kIx(	(
<g/>
logika	logika	k1gFnSc1	logika
<g/>
)	)	kIx)	)
MACHOVÁ	Machová	k1gFnSc1	Machová
<g/>
,	,	kIx,	,
Svatava	Svatava	k1gFnSc1	Svatava
<g/>
;	;	kIx,	;
ŠVEHLOVÁ	Švehlová	k1gFnSc1	Švehlová
<g/>
,	,	kIx,	,
Milena	Milena	k1gFnSc1	Milena
<g/>
.	.	kIx.	.
</s>
<s>
Sémantická	sémantický	k2eAgFnSc1d1	sémantická
&	&	k?	&
pragmatická	pragmatický	k2eAgFnSc1d1	pragmatická
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7290	[number]	k4	7290
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tautologie	tautologie	k1gFnSc2	tautologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
