<s>
Nosnou	nosný	k2eAgFnSc4d1	nosná
konstrukci	konstrukce	k1gFnSc4	konstrukce
sochy	socha	k1gFnSc2	socha
Svobody	svoboda	k1gFnSc2	svoboda
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
známý	známý	k2eAgMnSc1d1	známý
francouzský	francouzský	k2eAgMnSc1d1	francouzský
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiného	jiný	k2eAgMnSc4d1	jiný
také	také	k6eAd1	také
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
-	-	kIx~	-
Gustave	Gustav	k1gMnSc5	Gustav
Eiffel	Eifflo	k1gNnPc2	Eifflo
<g/>
.	.	kIx.	.
</s>
