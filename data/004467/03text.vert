<s>
Socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Statue	statue	k1gFnSc1	statue
of	of	k?	of
Liberty	Libert	k1gInPc7	Libert
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Liberty	Libert	k1gInPc1	Libert
Enlightening	Enlightening	k1gInSc1	Enlightening
the	the	k?	the
World	World	k1gInSc1	World
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
La	la	k1gNnSc2	la
Liberté	Liberta	k1gMnPc1	Liberta
éclairant	éclairant	k1gMnSc1	éclairant
le	le	k?	le
monde	mond	k1gInSc5	mond
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Svoboda	svoboda	k1gFnSc1	svoboda
přinášející	přinášející	k2eAgNnSc1d1	přinášející
světlo	světlo	k1gNnSc1	světlo
světu	svět	k1gInSc3	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
Ostrově	ostrov	k1gInSc6	ostrov
svobody	svoboda	k1gFnSc2	svoboda
u	u	k7c2	u
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
by	by	kYmCp3nS	by
katastrálně	katastrálně	k6eAd1	katastrálně
měl	mít	k5eAaImAgInS	mít
náležet	náležet	k5eAaImF	náležet
do	do	k7c2	do
Hudson	Hudsona	k1gFnPc2	Hudsona
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
New	New	k1gMnSc2	New
Jersey	Jersea	k1gMnSc2	Jersea
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1664	[number]	k4	1664
je	být	k5eAaImIp3nS	být
Ostrov	ostrov	k1gInSc1	ostrov
svobody	svoboda	k1gFnSc2	svoboda
součástí	součást	k1gFnSc7	součást
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
46	[number]	k4	46
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
47	[number]	k4	47
<g/>
metrovým	metrový	k2eAgInSc7d1	metrový
podstavcem	podstavec	k1gInSc7	podstavec
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vrchol	vrchol	k1gInSc1	vrchol
pochodně	pochodeň	k1gFnSc2	pochodeň
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
93	[number]	k4	93
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
váží	vážit	k5eAaImIp3nS	vážit
205	[number]	k4	205
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
má	mít	k5eAaImIp3nS	mít
10,6	[number]	k4	10,6
metru	metr	k1gInSc2	metr
<g/>
,	,	kIx,	,
ústa	ústa	k1gNnPc1	ústa
mají	mít	k5eAaImIp3nP	mít
šířku	šířka	k1gFnSc4	šířka
91	[number]	k4	91
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
paže	paže	k1gFnSc1	paže
<g/>
,	,	kIx,	,
držící	držící	k2eAgFnSc1d1	držící
pochodeň	pochodeň	k1gFnSc1	pochodeň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
12,8	[number]	k4	12,8
metru	metro	k1gNnSc6	metro
<g/>
;	;	kIx,	;
jen	jen	k9	jen
ukazováček	ukazováček	k1gInSc1	ukazováček
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
2,4	[number]	k4	2,4
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nohou	noha	k1gFnPc2	noha
Sochy	Socha	k1gMnSc2	Socha
svobody	svoboda	k1gFnSc2	svoboda
leží	ležet	k5eAaImIp3nP	ležet
rozťatá	rozťatý	k2eAgNnPc4d1	rozťatý
pouta	pouto	k1gNnPc4	pouto
tyranie	tyranie	k1gFnSc2	tyranie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
levé	levý	k2eAgFnSc6d1	levá
ruce	ruka	k1gFnSc6	ruka
drží	držet	k5eAaImIp3nS	držet
socha	socha	k1gFnSc1	socha
desky	deska	k1gFnSc2	deska
představující	představující	k2eAgNnSc4d1	představující
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
nápis	nápis	k1gInSc1	nápis
JULY	Jula	k1gMnSc2	Jula
IV	IV	kA	IV
MDCCLXXVI	MDCCLXXVI	kA	MDCCLXXVI
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
Deklarace	deklarace	k1gFnSc2	deklarace
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
její	její	k3xOp3gFnSc1	její
koruna	koruna	k1gFnSc1	koruna
se	s	k7c7	s
sedmi	sedm	k4xCc7	sedm
paprsky	paprsek	k1gInPc7	paprsek
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
má	mít	k5eAaImIp3nS	mít
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
přes	přes	k7c4	přes
sedm	sedm	k4xCc4	sedm
moří	moře	k1gNnPc2	moře
do	do	k7c2	do
sedmi	sedm	k4xCc2	sedm
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
je	být	k5eAaImIp3nS	být
vyryto	vyryt	k2eAgNnSc4d1	vyryto
poselství	poselství	k1gNnSc4	poselství
sochy	socha	k1gFnSc2	socha
samé	samý	k3xTgFnPc1	samý
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
úcta	úcta	k1gFnSc1	úcta
k	k	k7c3	k
novinářce	novinářka	k1gFnSc3	novinářka
Emmě	Emma	k1gFnSc3	Emma
Lazarusové	Lazarusový	k2eAgFnSc2d1	Lazarusový
která	který	k3yRgFnSc1	který
díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
textu	text	k1gInSc3	text
pomohla	pomoct	k5eAaPmAgFnS	pomoct
naplnit	naplnit	k5eAaPmF	naplnit
sbírku	sbírka	k1gFnSc4	sbírka
pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
podstavce	podstavec	k1gInSc2	podstavec
sochy	socha	k1gFnSc2	socha
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Dej	dát	k5eAaPmRp2nS	dát
mi	já	k3xPp1nSc3	já
své	svůj	k3xOyFgNnSc4	svůj
ubohé	ubohý	k2eAgFnPc1d1	ubohá
<g/>
,	,	kIx,	,
unavené	unavený	k2eAgFnPc1d1	unavená
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
'	'	kIx"	'
<g/>
své	svůj	k3xOyFgFnPc4	svůj
schoulené	schoulený	k2eAgFnPc4d1	schoulená
masy	masa	k1gFnPc4	masa
toužící	toužící	k2eAgFnPc1d1	toužící
volně	volně	k6eAd1	volně
dýchat	dýchat	k5eAaImF	dýchat
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
ten	ten	k3xDgInSc1	ten
žalostný	žalostný	k2eAgInSc1d1	žalostný
nadbytek	nadbytek	k1gInSc1	nadbytek
tvých	tvůj	k3xOp2gInPc2	tvůj
přeplněných	přeplněný	k2eAgInPc2d1	přeplněný
břehů	břeh	k1gInPc2	břeh
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
'	'	kIx"	'
<g/>
Pošli	poslat	k5eAaPmRp2nS	poslat
je	být	k5eAaImIp3nS	být
sem	sem	k6eAd1	sem
<g/>
,	,	kIx,	,
zmítané	zmítaný	k2eAgFnPc1d1	zmítaná
bouří	bouř	k1gFnSc7	bouř
a	a	k8xC	a
bez	bez	k7c2	bez
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
<g/>
'	'	kIx"	'
'	'	kIx"	'
<g/>
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
já	já	k3xPp1nSc1	já
pozvedám	pozvedat	k5eAaImIp1nS	pozvedat
pochodeň	pochodeň	k1gFnSc4	pochodeň
u	u	k7c2	u
zlatých	zlatý	k2eAgFnPc2d1	zlatá
dveří	dveře	k1gFnPc2	dveře
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
-Emma	-Emma	k1gFnSc1	-Emma
Lazarus	Lazarus	k1gInSc1	Lazarus
Socha	socha	k1gFnSc1	socha
Svobody	Svoboda	k1gMnSc2	Svoboda
připomíná	připomínat	k5eAaImIp3nS	připomínat
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Francie	Francie	k1gFnSc1	Francie
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
americkým	americký	k2eAgMnPc3d1	americký
osadníkům	osadník	k1gMnPc3	osadník
v	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
boji	boj	k1gInSc6	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
započatém	započatý	k2eAgInSc6d1	započatý
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
Francie	Francie	k1gFnSc1	Francie
totiž	totiž	k9	totiž
poslala	poslat	k5eAaPmAgFnS	poslat
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
chtěli	chtít	k5eAaImAgMnP	chtít
také	také	k9	také
sochou	socha	k1gFnSc7	socha
Svobody	svoboda	k1gFnSc2	svoboda
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
sympatie	sympatie	k1gFnPc4	sympatie
nové	nový	k2eAgFnSc3d1	nová
republice	republika	k1gFnSc3	republika
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
za	za	k7c7	za
Atlantským	atlantský	k2eAgInSc7d1	atlantský
oceánem	oceán	k1gInSc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Socha	socha	k1gFnSc1	socha
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
ostrůvku	ostrůvek	k1gInSc6	ostrůvek
před	před	k7c7	před
New	New	k1gMnSc7	New
Yorkem	York	k1gInSc7	York
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1886	[number]	k4	1886
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
podstavce	podstavec	k1gInSc2	podstavec
měří	měřit	k5eAaImIp3nS	měřit
po	po	k7c4	po
vrchol	vrchol	k1gInSc4	vrchol
pochodně	pochodeň	k1gFnSc2	pochodeň
46	[number]	k4	46
m	m	kA	m
<g/>
,	,	kIx,	,
s	s	k7c7	s
podstavcem	podstavec	k1gInSc7	podstavec
ční	čnět	k5eAaImIp3nP	čnět
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
93	[number]	k4	93
m.	m.	k?	m.
Dnes	dnes	k6eAd1	dnes
existuje	existovat	k5eAaImIp3nS	existovat
její	její	k3xOp3gFnSc1	její
zmenšená	zmenšený	k2eAgFnSc1d1	zmenšená
kopie	kopie	k1gFnSc1	kopie
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jako	jako	k8xC	jako
připomínka	připomínka	k1gFnSc1	připomínka
jejího	její	k3xOp3gInSc2	její
francouzského	francouzský	k2eAgInSc2d1	francouzský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Nosnou	nosný	k2eAgFnSc4d1	nosná
konstrukci	konstrukce	k1gFnSc4	konstrukce
sochy	socha	k1gFnSc2	socha
Svobody	svoboda	k1gFnSc2	svoboda
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
a	a	k8xC	a
zkonstruoval	zkonstruovat	k5eAaPmAgMnS	zkonstruovat
známý	známý	k2eAgMnSc1d1	známý
francouzský	francouzský	k2eAgMnSc1d1	francouzský
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiného	jiný	k2eAgMnSc4d1	jiný
také	také	k6eAd1	také
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Eiffelovy	Eiffelův	k2eAgFnSc2d1	Eiffelova
věže	věž	k1gFnSc2	věž
-	-	kIx~	-
Gustave	Gustav	k1gMnSc5	Gustav
Eiffel	Eifflo	k1gNnPc2	Eifflo
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
sochař	sochař	k1gMnSc1	sochař
Frédéric	Frédéric	k1gMnSc1	Frédéric
Auguste	August	k1gMnSc5	August
Bartholdi	Barthold	k1gMnPc1	Barthold
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
malý	malý	k2eAgInSc1d1	malý
hliněný	hliněný	k2eAgInSc1d1	hliněný
model	model	k1gInSc1	model
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
výšku	výška	k1gFnSc4	výška
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
model	model	k1gInSc1	model
byl	být	k5eAaImAgInS	být
vysoký	vysoký	k2eAgInSc4d1	vysoký
tři	tři	k4xCgInPc4	tři
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
model	model	k1gInSc4	model
udělali	udělat	k5eAaPmAgMnP	udělat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
socha	socha	k1gFnSc1	socha
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
model	model	k1gInSc4	model
začali	začít	k5eAaPmAgMnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
ve	v	k7c6	v
skutečné	skutečný	k2eAgFnSc6d1	skutečná
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
byla	být	k5eAaImAgFnS	být
pak	pak	k6eAd1	pak
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
socha	socha	k1gFnSc1	socha
z	z	k7c2	z
měděného	měděný	k2eAgInSc2d1	měděný
plechu	plech	k1gInSc2	plech
upevněného	upevněný	k2eAgInSc2d1	upevněný
na	na	k7c6	na
ocelové	ocelový	k2eAgFnSc6d1	ocelová
nosné	nosný	k2eAgFnSc6d1	nosná
konstrukci	konstrukce	k1gFnSc6	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
verzí	verze	k1gFnPc2	verze
byla	být	k5eAaImAgFnS	být
Bartholdimu	Bartholdim	k1gMnSc3	Bartholdim
modelem	model	k1gInSc7	model
pro	pro	k7c4	pro
ženskou	ženský	k2eAgFnSc4d1	ženská
tvář	tvář	k1gFnSc4	tvář
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
Charlotte	Charlott	k1gInSc5	Charlott
Bartholdi	Barthold	k1gMnPc1	Barthold
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
-	-	kIx~	-
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiné	jiná	k1gFnSc2	jiná
Isabella	Isabella	k1gFnSc1	Isabella
Eugenie	Eugenie	k1gFnSc1	Eugenie
Boyerová	Boyerová	k1gFnSc1	Boyerová
<g/>
,	,	kIx,	,
vdova	vdova	k1gFnSc1	vdova
po	po	k7c6	po
proslulém	proslulý	k2eAgNnSc6d1	proslulé
výrobci	výrobce	k1gMnPc1	výrobce
šicích	šicí	k2eAgInPc2d1	šicí
strojů	stroj	k1gInPc2	stroj
Isaaku	Isaak	k1gInSc2	Isaak
Singerovi	Singer	k1gMnSc3	Singer
<g/>
.	.	kIx.	.
</s>
<s>
Hotová	hotový	k2eAgFnSc1d1	hotová
socha	socha	k1gFnSc1	socha
byla	být	k5eAaImAgFnS	být
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1886	[number]	k4	1886
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
slavnostně	slavnostně	k6eAd1	slavnostně
předána	předat	k5eAaPmNgFnS	předat
americkému	americký	k2eAgMnSc3d1	americký
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
rozložena	rozložit	k5eAaPmNgFnS	rozložit
na	na	k7c4	na
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
New	New	k1gFnPc2	New
Yorku	York	k1gInSc2	York
dopravily	dopravit	k5eAaPmAgInP	dopravit
na	na	k7c4	na
francouzské	francouzský	k2eAgFnPc4d1	francouzská
lodi	loď	k1gFnPc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
newyorském	newyorský	k2eAgInSc6d1	newyorský
přístavu	přístav	k1gInSc6	přístav
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
sestavovala	sestavovat	k5eAaImAgFnS	sestavovat
<g/>
;	;	kIx,	;
měděné	měděný	k2eAgInPc1d1	měděný
kusy	kus	k1gInPc1	kus
byly	být	k5eAaImAgInP	být
obepnuty	obepnut	k2eAgInPc1d1	obepnut
kovovými	kovový	k2eAgInPc7d1	kovový
pásy	pás	k1gInPc7	pás
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
bránily	bránit	k5eAaImAgInP	bránit
poškození	poškození	k1gNnSc4	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Sestavená	sestavený	k2eAgFnSc1d1	sestavená
socha	socha	k1gFnSc1	socha
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c4	na
podstavec	podstavec	k1gInSc4	podstavec
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
americký	americký	k2eAgMnSc1d1	americký
architekt	architekt	k1gMnSc1	architekt
Richard	Richard	k1gMnSc1	Richard
Morris	Morris	k1gFnSc2	Morris
Hunt	hunt	k1gInSc1	hunt
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
socha	socha	k1gFnSc1	socha
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
uběhlo	uběhnout	k5eAaPmAgNnS	uběhnout
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
darování	darování	k1gNnSc2	darování
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
značnému	značný	k2eAgNnSc3d1	značné
poškození	poškození	k1gNnSc3	poškození
<g/>
;	;	kIx,	;
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
byla	být	k5eAaImAgNnP	být
díky	díky	k7c3	díky
veřejným	veřejný	k2eAgFnPc3d1	veřejná
sbírkám	sbírka	k1gFnPc3	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oslavě	oslava	k1gFnSc6	oslava
stého	stý	k4xOgNnSc2	stý
výročí	výročí	k1gNnSc2	výročí
vztyčení	vztyčení	k1gNnSc2	vztyčení
sochy	socha	k1gFnSc2	socha
na	na	k7c4	na
Liberty	Libert	k1gInPc4	Libert
Islandu	Island	k1gInSc2	Island
–	–	k?	–
Ostrově	ostrov	k1gInSc6	ostrov
svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
Bedloe	Bedlo	k1gInSc2	Bedlo
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Island	Island	k1gInSc1	Island
<g/>
)	)	kIx)	)
–	–	k?	–
<g/>
,	,	kIx,	,
přísahalo	přísahat	k5eAaImAgNnS	přísahat
vrchnímu	vrchní	k2eAgMnSc3d1	vrchní
soudci	soudce	k1gMnSc3	soudce
Warrenu	Warren	k1gMnSc3	Warren
E.	E.	kA	E.
Burgerovi	Burger	k1gMnSc3	Burger
věrnost	věrnost	k1gFnSc4	věrnost
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
5	[number]	k4	5
000	[number]	k4	000
nových	nový	k2eAgMnPc2d1	nový
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
chvíli	chvíle	k1gFnSc4	chvíle
stalo	stát	k5eAaPmAgNnS	stát
americkými	americký	k2eAgMnPc7d1	americký
občany	občan	k1gMnPc4	občan
díky	díky	k7c3	díky
satelitnímu	satelitní	k2eAgInSc3d1	satelitní
přenosu	přenos	k1gInSc3	přenos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
sochy	socha	k1gFnPc4	socha
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
ani	ani	k8xC	ani
zdaleka	zdaleka	k6eAd1	zdaleka
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
volně	volně	k6eAd1	volně
stojící	stojící	k2eAgFnSc7d1	stojící
postavou	postava	k1gFnSc7	postava
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
památník	památník	k1gInSc1	památník
Matka	matka	k1gFnSc1	matka
vlast	vlast	k1gFnSc1	vlast
volá	volat	k5eAaImIp3nS	volat
na	na	k7c6	na
Mamajevově	Mamajevův	k2eAgFnSc6d1	Mamajevův
mohyle	mohyla	k1gFnSc6	mohyla
ve	v	k7c6	v
Volgogradu	Volgograd	k1gInSc2	Volgograd
měří	měřit	k5eAaImIp3nS	měřit
od	od	k7c2	od
paty	pata	k1gFnSc2	pata
k	k	k7c3	k
hlavě	hlava	k1gFnSc3	hlava
bez	bez	k7c2	bez
podstavce	podstavec	k1gInSc2	podstavec
52	[number]	k4	52
m	m	kA	m
a	a	k8xC	a
po	po	k7c4	po
špičku	špička	k1gFnSc4	špička
meče	meč	k1gInSc2	meč
85	[number]	k4	85
m.	m.	k?	m.
Největší	veliký	k2eAgMnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
vůbec	vůbec	k9	vůbec
je	být	k5eAaImIp3nS	být
Ušiku	Ušika	k1gFnSc4	Ušika
Daibucu	Daibucus	k1gInSc2	Daibucus
<g/>
,	,	kIx,	,
socha	socha	k1gFnSc1	socha
Buddhy	Buddha	k1gMnSc2	Buddha
Amitábhy	Amitábha	k1gMnSc2	Amitábha
v	v	k7c6	v
japonském	japonský	k2eAgNnSc6d1	Japonské
Ušiku	Ušiko	k1gNnSc6	Ušiko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
120	[number]	k4	120
m	m	kA	m
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
podstavec	podstavec	k1gInSc1	podstavec
10	[number]	k4	10
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sochu	socha	k1gFnSc4	socha
svobody	svoboda	k1gFnSc2	svoboda
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
;	;	kIx,	;
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
New	New	k1gMnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Výhled	výhled	k1gInSc1	výhled
se	se	k3xPyFc4	se
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
naskýtá	naskýtat	k5eAaImIp3nS	naskýtat
z	z	k7c2	z
jejího	její	k3xOp3gInSc2	její
podstavce	podstavec	k1gInSc2	podstavec
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
vystoupat	vystoupat	k5eAaPmF	vystoupat
schodištěm	schodiště	k1gNnSc7	schodiště
až	až	k8xS	až
na	na	k7c4	na
čelenku	čelenka	k1gFnSc4	čelenka
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
větev	větev	k1gFnSc1	větev
schodů	schod	k1gInPc2	schod
vede	vést	k5eAaImIp3nS	vést
dokonce	dokonce	k9	dokonce
až	až	k9	až
k	k	k7c3	k
pochodni	pochodeň	k1gFnSc3	pochodeň
<g/>
.	.	kIx.	.
</s>
<s>
Výhled	výhled	k1gInSc1	výhled
z	z	k7c2	z
čelenky	čelenka	k1gFnSc2	čelenka
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
jako	jako	k8xS	jako
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
teroristické	teroristický	k2eAgInPc4d1	teroristický
útoky	útok	k1gInPc4	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
znovuotevření	znovuotevření	k1gNnSc3	znovuotevření
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
potřeba	potřeba	k6eAd1	potřeba
se	se	k3xPyFc4	se
registrovat	registrovat	k5eAaBmF	registrovat
dlouho	dlouho	k6eAd1	dlouho
(	(	kIx(	(
<g/>
i	i	k9	i
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Pochodeň	pochodeň	k1gFnSc1	pochodeň
je	být	k5eAaImIp3nS	být
veřejnosti	veřejnost	k1gFnSc3	veřejnost
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
<g/>
.	.	kIx.	.
</s>
<s>
BORŮVKOVÁ	Borůvková	k1gFnSc1	Borůvková
<g/>
,	,	kIx,	,
Vendula	Vendula	k1gFnSc1	Vendula
<g/>
.	.	kIx.	.
</s>
<s>
Symbol	symbol	k1gInSc1	symbol
nových	nový	k2eAgInPc2d1	nový
začátků	začátek	k1gInPc2	začátek
<g/>
.	.	kIx.	.
100	[number]	k4	100
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
s.	s.	k?	s.
52	[number]	k4	52
<g/>
-	-	kIx~	-
<g/>
53	[number]	k4	53
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
socha	socha	k1gFnSc1	socha
Svobody	Svoboda	k1gMnSc2	Svoboda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
oficiální	oficiální	k2eAgFnSc2d1	oficiální
informační	informační	k2eAgFnSc2d1	informační
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Statue	statue	k1gFnSc1	statue
of	of	k?	of
Liberty	Libert	k1gMnPc7	Libert
(	(	kIx(	(
<g/>
UNESCO	UNESCO	kA	UNESCO
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Statue	statue	k1gFnSc1	statue
of	of	k?	of
Libery	Libera	k1gFnSc2	Libera
Information	Information	k1gInSc1	Information
James	James	k1gMnSc1	James
Fulford	Fulford	k1gMnSc1	Fulford
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Statue	statue	k1gFnSc2	statue
of	of	k?	of
Immigration	Immigration	k1gInSc1	Immigration
<g/>
,	,	kIx,	,
or	or	k?	or
Liberty	Libert	k1gInPc1	Libert
Inviting	Inviting	k1gInSc1	Inviting
the	the	k?	the
World	World	k1gInSc1	World
</s>
