<s>
Nikotin	nikotin	k1gInSc1	nikotin
je	být	k5eAaImIp3nS	být
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
pyridinový	pyridinový	k2eAgInSc1d1	pyridinový
alkaloid	alkaloid	k1gInSc1	alkaloid
obsažený	obsažený	k2eAgInSc1d1	obsažený
v	v	k7c6	v
tabáku	tabák	k1gInSc6	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
stimulační	stimulační	k2eAgInPc4d1	stimulační
a	a	k8xC	a
uvolňující	uvolňující	k2eAgInPc4d1	uvolňující
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
formou	forma	k1gFnSc7	forma
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
žvýkání	žvýkání	k1gNnSc2	žvýkání
či	či	k8xC	či
šňupání	šňupání	k1gNnSc2	šňupání
tabáku	tabák	k1gInSc2	tabák
<g/>
,	,	kIx,	,
nověji	nově	k6eAd2	nově
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
náplní	náplň	k1gFnPc2	náplň
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
základní	základní	k2eAgFnSc4d1	základní
návykovou	návykový	k2eAgFnSc4d1	návyková
látku	látka	k1gFnSc4	látka
obsaženou	obsažený	k2eAgFnSc4d1	obsažená
v	v	k7c6	v
tabáku	tabák	k1gInSc6	tabák
<g/>
;	;	kIx,	;
váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
nikotinové	nikotinový	k2eAgInPc4d1	nikotinový
acetylcholinové	acetylcholinový	k2eAgInPc4d1	acetylcholinový
receptory	receptor	k1gInPc4	receptor
(	(	kIx(	(
<g/>
nAChR	nAChR	k?	nAChR
<g/>
)	)	kIx)	)
v	v	k7c6	v
nervové	nervový	k2eAgFnSc6d1	nervová
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
stav	stav	k1gInSc1	stav
relaxace	relaxace	k1gFnSc2	relaxace
a	a	k8xC	a
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
chvíli	chvíle	k1gFnSc4	chvíle
však	však	k9	však
také	také	k9	také
nepřirozeně	přirozeně	k6eNd1	přirozeně
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
srdeční	srdeční	k2eAgFnSc4d1	srdeční
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
především	především	k6eAd1	především
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
extrémně	extrémně	k6eAd1	extrémně
silnou	silný	k2eAgFnSc4d1	silná
závislost	závislost	k1gFnSc4	závislost
(	(	kIx(	(
<g/>
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnávykovějších	návykový	k2eAgFnPc2d3	nejnávykovější
látek	látka	k1gFnPc2	látka
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
nikotin	nikotin	k1gInSc1	nikotin
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
odborného	odborný	k2eAgInSc2d1	odborný
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
rostlinu	rostlina	k1gFnSc4	rostlina
tabák	tabák	k1gInSc1	tabák
(	(	kIx(	(
<g/>
Nicotiana	Nicotiana	k1gFnSc1	Nicotiana
tabacum	tabacum	k1gInSc1	tabacum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nikotin	nikotin	k1gInSc1	nikotin
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
surovém	surový	k2eAgInSc6d1	surový
stavu	stav	k1gInSc6	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
M.	M.	kA	M.
Vauquelin	Vauquelin	k2eAgMnSc1d1	Vauquelin
(	(	kIx(	(
<g/>
Vauquelin	Vauquelin	k2eAgMnSc1d1	Vauquelin
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Ann	Ann	k1gMnSc1	Ann
<g/>
.	.	kIx.	.
chim	chim	k1gMnSc1	chim
<g/>
.	.	kIx.	.
71	[number]	k4	71
<g/>
,	,	kIx,	,
139	[number]	k4	139
<g/>
,	,	kIx,	,
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
W.	W.	kA	W.
Posselt	Posselt	k1gInSc1	Posselt
a	a	k8xC	a
L	L	kA	L
Reimann	Reimann	k1gMnSc1	Reimann
(	(	kIx(	(
<g/>
Posselt	Posselt	k1gInSc1	Posselt
W.	W.	kA	W.
a	a	k8xC	a
Reimann	Reimann	k1gMnSc1	Reimann
L.	L.	kA	L.
<g/>
:	:	kIx,	:
Geigers	Geigers	k1gInSc1	Geigers
Mag	Maga	k1gFnPc2	Maga
<g/>
.	.	kIx.	.
</s>
<s>
Pharmac	Pharmac	k1gFnSc1	Pharmac
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
,	,	kIx,	,
138	[number]	k4	138
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sumární	sumární	k2eAgInSc1d1	sumární
vzorec	vzorec	k1gInSc1	vzorec
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
syntézu	syntéza	k1gFnSc4	syntéza
nikotinu	nikotin	k1gInSc2	nikotin
popsal	popsat	k5eAaPmAgMnS	popsat
A.	A.	kA	A.
Picket	Picket	k1gMnSc1	Picket
a	a	k8xC	a
P.	P.	kA	P.
Crė	Crė	k1gFnSc2	Crė
v	v	k7c6	v
Ber	brát	k5eAaImRp2nS	brát
<g/>
.28	.28	k4	.28
<g/>
,	,	kIx,	,
1828	[number]	k4	1828
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
kapalinu	kapalina	k1gFnSc4	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvý	čerstvý	k2eAgMnSc1d1	čerstvý
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tabákových	tabákový	k2eAgInPc6d1	tabákový
listech	list	k1gInPc6	list
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgInS	vázat
na	na	k7c4	na
kyselinu	kyselina	k1gFnSc4	kyselina
jablečnou	jablečný	k2eAgFnSc4d1	jablečná
a	a	k8xC	a
citrónovou	citrónový	k2eAgFnSc4d1	citrónová
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
tabák	tabák	k1gInSc1	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Nikotin	nikotin	k1gInSc1	nikotin
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
návykových	návykový	k2eAgFnPc2d1	návyková
látek	látka	k1gFnPc2	látka
<g/>
;	;	kIx,	;
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
např.	např.	kA	např.
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
USA	USA	kA	USA
užívala	užívat	k5eAaImAgFnS	užívat
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
)	)	kIx)	)
nikotin	nikotin	k1gInSc1	nikotin
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
kouření	kouření	k1gNnSc3	kouření
tabáku	tabák	k1gInSc2	tabák
jednou	jednou	k6eAd1	jednou
zavleklo	zavleknout	k5eAaPmAgNnS	zavleknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc2	tento
drogy	droga	k1gFnSc2	droga
dosud	dosud	k6eAd1	dosud
nezbavila	zbavit	k5eNaPmAgFnS	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
cigareta	cigareta	k1gFnSc1	cigareta
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
miligramů	miligram	k1gInPc2	miligram
nikotinu	nikotin	k1gInSc2	nikotin
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
se	se	k3xPyFc4	se
však	však	k9	však
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
nedostane	dostat	k5eNaPmIp3nS	dostat
a	a	k8xC	a
shoří	shořet	k5eAaPmIp3nS	shořet
(	(	kIx(	(
<g/>
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostane	dostat	k5eAaPmIp3nS	dostat
asi	asi	k9	asi
1	[number]	k4	1
mg	mg	kA	mg
nikotinu	nikotin	k1gInSc6	nikotin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
dávka	dávka	k1gFnSc1	dávka
šňupacího	šňupací	k2eAgInSc2d1	šňupací
tabáku	tabák	k1gInSc2	tabák
představuje	představovat	k5eAaImIp3nS	představovat
asi	asi	k9	asi
4,5	[number]	k4	4,5
<g/>
–	–	k?	–
<g/>
6,5	[number]	k4	6,5
mg	mg	kA	mg
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
množství	množství	k1gNnSc2	množství
se	se	k3xPyFc4	se
při	při	k7c6	při
šňupání	šňupání	k1gNnSc6	šňupání
postupně	postupně	k6eAd1	postupně
vstřebá	vstřebat	k5eAaPmIp3nS	vstřebat
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
kouření	kouření	k1gNnSc1	kouření
tabáku	tabák	k1gInSc2	tabák
plněného	plněný	k2eAgInSc2d1	plněný
do	do	k7c2	do
dýmek	dýmka	k1gFnPc2	dýmka
<g/>
.	.	kIx.	.
</s>
<s>
Stinnou	stinný	k2eAgFnSc7d1	stinná
stránkou	stránka	k1gFnSc7	stránka
užívání	užívání	k1gNnSc2	užívání
nikotinu	nikotin	k1gInSc2	nikotin
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kouření	kouření	k1gNnSc2	kouření
je	být	k5eAaImIp3nS	být
především	především	k9	především
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
cigaretový	cigaretový	k2eAgInSc4d1	cigaretový
kouř	kouř	k1gInSc4	kouř
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
–	–	k?	–
vyjma	vyjma	k7c2	vyjma
nikotinu	nikotin	k1gInSc2	nikotin
–	–	k?	–
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4000	[number]	k4	4000
dalších	další	k2eAgFnPc2d1	další
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c4	mnoho
jedovatých	jedovatý	k2eAgInPc2d1	jedovatý
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
rakovinotvorných	rakovinotvorný	k2eAgFnPc2d1	rakovinotvorná
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
v	v	k7c6	v
článku	článek	k1gInSc6	článek
karcinogenní	karcinogenní	k2eAgFnPc4d1	karcinogenní
složky	složka	k1gFnPc4	složka
cigaretového	cigaretový	k2eAgInSc2d1	cigaretový
kouře	kouř	k1gInSc2	kouř
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kouření	kouření	k1gNnSc6	kouření
se	se	k3xPyFc4	se
nikotin	nikotin	k1gInSc1	nikotin
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
do	do	k7c2	do
kouře	kouř	k1gInSc2	kouř
vstřebává	vstřebávat	k5eAaImIp3nS	vstřebávat
se	se	k3xPyFc4	se
např.	např.	kA	např.
skrz	skrz	k6eAd1	skrz
sliznici	sliznice	k1gFnSc4	sliznice
v	v	k7c6	v
ústech	ústa	k1gNnPc6	ústa
<g/>
,	,	kIx,	,
nose	nos	k1gInSc6	nos
či	či	k8xC	či
skrz	skrz	k7c4	skrz
stěnu	stěna	k1gFnSc4	stěna
plicních	plicní	k2eAgInPc2d1	plicní
sklípků	sklípek	k1gInPc2	sklípek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
putuje	putovat	k5eAaImIp3nS	putovat
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
nikotinové	nikotinový	k2eAgInPc4d1	nikotinový
acetylcholinové	acetylcholinový	k2eAgInPc4d1	acetylcholinový
receptory	receptor	k1gInPc4	receptor
a	a	k8xC	a
dráždí	dráždit	k5eAaImIp3nP	dráždit
je	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
stav	stav	k1gInSc4	stav
relaxace	relaxace	k1gFnSc2	relaxace
a	a	k8xC	a
stimuluje	stimulovat	k5eAaImIp3nS	stimulovat
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
však	však	k9	však
svými	svůj	k3xOyFgInPc7	svůj
efekty	efekt	k1gInPc7	efekt
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
extrémně	extrémně	k6eAd1	extrémně
silnou	silný	k2eAgFnSc4d1	silná
závislost	závislost	k1gFnSc4	závislost
(	(	kIx(	(
<g/>
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejnávykovějších	návykový	k2eAgFnPc2d3	nejnávykovější
látek	látka	k1gFnPc2	látka
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Váže	vázat	k5eAaImIp3nS	vázat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
receptory	receptor	k1gInPc4	receptor
ve	v	k7c6	v
vegetativním	vegetativní	k2eAgInSc6d1	vegetativní
nervovém	nervový	k2eAgInSc6d1	nervový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
aktivitu	aktivita	k1gFnSc4	aktivita
trávícího	trávící	k2eAgInSc2d1	trávící
traktu	trakt	k1gInSc2	trakt
<g/>
:	:	kIx,	:
vzestup	vzestup	k1gInSc1	vzestup
produkce	produkce	k1gFnSc2	produkce
slin	slina	k1gFnPc2	slina
a	a	k8xC	a
trávících	trávící	k2eAgFnPc2d1	trávící
šťáv	šťáva	k1gFnPc2	šťáva
a	a	k8xC	a
vzestup	vzestup	k1gInSc1	vzestup
aktivity	aktivita	k1gFnSc2	aktivita
hladké	hladký	k2eAgFnSc2d1	hladká
svaloviny	svalovina	k1gFnSc2	svalovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
působí	působit	k5eAaImIp3nS	působit
nikotin	nikotin	k1gInSc1	nikotin
stimulačně	stimulačně	k6eAd1	stimulačně
a	a	k8xC	a
pak	pak	k6eAd1	pak
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
fáze	fáze	k1gFnSc2	fáze
mírného	mírný	k2eAgInSc2d1	mírný
útlumu	útlum	k1gInSc2	útlum
<g/>
.	.	kIx.	.
</s>
<s>
Nikotin	nikotin	k1gInSc1	nikotin
dále	daleko	k6eAd2	daleko
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
zrychluje	zrychlovat	k5eAaImIp3nS	zrychlovat
činnost	činnost	k1gFnSc4	činnost
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
stahuje	stahovat	k5eAaImIp3nS	stahovat
cévy	céva	k1gFnPc4	céva
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
obsah	obsah	k1gInSc1	obsah
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
blokuje	blokovat	k5eAaImIp3nS	blokovat
syntézu	syntéza	k1gFnSc4	syntéza
estrogenu	estrogen	k1gInSc2	estrogen
<g/>
,	,	kIx,	,
stoupá	stoupat	k5eAaImIp3nS	stoupat
produkce	produkce	k1gFnSc1	produkce
potu	pot	k1gInSc2	pot
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
zornic	zornice	k1gFnPc2	zornice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobějším	dlouhodobý	k2eAgNnSc6d2	dlouhodobější
užívání	užívání	k1gNnSc6	užívání
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
syntézy	syntéza	k1gFnSc2	syntéza
endorfinů	endorfin	k1gInPc2	endorfin
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
vzniku	vznik	k1gInSc2	vznik
závislosti	závislost	k1gFnSc2	závislost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nikotinismus	nikotinismus	k1gInSc1	nikotinismus
<g/>
.	.	kIx.	.
</s>
<s>
Nikotin	nikotin	k1gInSc1	nikotin
je	být	k5eAaImIp3nS	být
vysoce	vysoce	k6eAd1	vysoce
návyková	návykový	k2eAgFnSc1d1	návyková
psychoaktivní	psychoaktivní	k2eAgFnSc1d1	psychoaktivní
látka	látka	k1gFnSc1	látka
(	(	kIx(	(
<g/>
farmakologický	farmakologický	k2eAgInSc1d1	farmakologický
a	a	k8xC	a
behaviorální	behaviorální	k2eAgInSc1d1	behaviorální
proces	proces	k1gInSc1	proces
determinující	determinující	k2eAgInSc4d1	determinující
vznik	vznik	k1gInSc4	vznik
závislosti	závislost	k1gFnSc2	závislost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
tabáku	tabák	k1gInSc2	tabák
podobný	podobný	k2eAgInSc4d1	podobný
jako	jako	k9	jako
u	u	k7c2	u
heroinu	heroin	k1gInSc2	heroin
a	a	k8xC	a
kokainu	kokain	k1gInSc2	kokain
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Závislost	závislost	k1gFnSc1	závislost
na	na	k7c6	na
nikotinu	nikotin	k1gInSc6	nikotin
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nutí	nutit	k5eAaImIp3nS	nutit
člověka	člověk	k1gMnSc4	člověk
kouřit	kouřit	k5eAaImF	kouřit
i	i	k9	i
přes	přes	k7c4	přes
částečné	částečný	k2eAgFnPc4d1	částečná
znalosti	znalost	k1gFnPc4	znalost
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
následků	následek	k1gInPc2	následek
a	a	k8xC	a
úsilí	úsilí	k1gNnSc2	úsilí
přestat	přestat	k5eAaPmF	přestat
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
nedostatek	nedostatek	k1gInSc4	nedostatek
vůle	vůle	k1gFnSc2	vůle
nebo	nebo	k8xC	nebo
o	o	k7c4	o
poruchu	porucha	k1gFnSc4	porucha
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
progresivní	progresivní	k2eAgNnSc4d1	progresivní
<g/>
,	,	kIx,	,
chronické	chronický	k2eAgNnSc4d1	chronické
a	a	k8xC	a
recidivující	recidivující	k2eAgNnSc4d1	recidivující
onemocnění	onemocnění	k1gNnSc4	onemocnění
(	(	kIx(	(
<g/>
diagnóza	diagnóza	k1gFnSc1	diagnóza
F	F	kA	F
17	[number]	k4	17
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
užívání	užívání	k1gNnSc6	užívání
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c4	na
nikotin	nikotin	k1gInSc4	nikotin
i	i	k8xC	i
fyzická	fyzický	k2eAgFnSc1d1	fyzická
závislost	závislost	k1gFnSc1	závislost
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odvykání	odvykání	k1gNnSc6	odvykání
od	od	k7c2	od
abúzu	abúzus	k1gInSc2	abúzus
nikotinu	nikotin	k1gInSc2	nikotin
trpí	trpět	k5eAaImIp3nS	trpět
jedinec	jedinec	k1gMnSc1	jedinec
úzkostí	úzkost	k1gFnPc2	úzkost
<g/>
,	,	kIx,	,
depresemi	deprese	k1gFnPc7	deprese
<g/>
,	,	kIx,	,
nespavostí	nespavost	k1gFnSc7	nespavost
<g/>
,	,	kIx,	,
podrážděním	podráždění	k1gNnSc7	podráždění
<g/>
,	,	kIx,	,
zhoršenou	zhoršený	k2eAgFnSc7d1	zhoršená
soustředěností	soustředěnost	k1gFnSc7	soustředěnost
<g/>
,	,	kIx,	,
pocením	pocení	k1gNnSc7	pocení
a	a	k8xC	a
bolestmi	bolest	k1gFnPc7	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
třesem	třes	k1gInSc7	třes
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
odvykání	odvykání	k1gNnSc6	odvykání
kouření	kouření	k1gNnSc2	kouření
tabáku	tabák	k1gInSc2	tabák
lze	lze	k6eAd1	lze
přechodně	přechodně	k6eAd1	přechodně
používat	používat	k5eAaImF	používat
nikotin	nikotin	k1gInSc4	nikotin
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
náplastí	náplast	k1gFnPc2	náplast
<g/>
,	,	kIx,	,
inhalace	inhalace	k1gFnSc2	inhalace
či	či	k8xC	či
žvýkaček	žvýkačka	k1gFnPc2	žvýkačka
nebo	nebo	k8xC	nebo
také	také	k6eAd1	také
kouřením	kouření	k1gNnSc7	kouření
elektronických	elektronický	k2eAgFnPc2d1	elektronická
cigaret	cigareta	k1gFnPc2	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
využít	využít	k5eAaPmF	využít
služeb	služba	k1gFnPc2	služba
odvykacích	odvykací	k2eAgFnPc2d1	odvykací
poraden	poradna	k1gFnPc2	poradna
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
rizika	riziko	k1gNnSc2	riziko
kouření	kouření	k1gNnSc2	kouření
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Nejvážnější	vážní	k2eAgNnPc1d3	nejvážnější
zdravotní	zdravotní	k2eAgNnPc1d1	zdravotní
rizika	riziko	k1gNnPc1	riziko
nejsou	být	k5eNaImIp3nP	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
nikotinem	nikotin	k1gInSc7	nikotin
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
látkami	látka	k1gFnPc7	látka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
tabák	tabák	k1gInSc4	tabák
či	či	k8xC	či
cigaretový	cigaretový	k2eAgInSc4d1	cigaretový
kouř	kouř	k1gInSc4	kouř
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejvážnějších	vážní	k2eAgMnPc2d3	nejvážnější
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
recidivující	recidivující	k2eAgInPc1d1	recidivující
záněty	zánět	k1gInPc1	zánět
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
a	a	k8xC	a
zhoubné	zhoubný	k2eAgInPc1d1	zhoubný
nádory	nádor	k1gInPc1	nádor
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
rakovina	rakovina	k1gFnSc1	rakovina
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
užívání	užívání	k1gNnSc2	užívání
tabáku	tabák	k1gInSc2	tabák
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Předávkování	předávkování	k1gNnSc1	předávkování
nikotinem	nikotin	k1gInSc7	nikotin
je	být	k5eAaImIp3nS	být
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
.	.	kIx.	.
</s>
<s>
Smrtelná	smrtelný	k2eAgFnSc1d1	smrtelná
dávka	dávka	k1gFnSc1	dávka
čistého	čistý	k2eAgInSc2d1	čistý
nikotinu	nikotin	k1gInSc2	nikotin
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
není	být	k5eNaImIp3nS	být
věrohodně	věrohodně	k6eAd1	věrohodně
stanovena	stanoven	k2eAgFnSc1d1	stanovena
<g/>
,	,	kIx,	,
starší	starší	k1gMnSc1	starší
zdroje	zdroj	k1gInSc2	zdroj
hovořící	hovořící	k2eAgFnPc1d1	hovořící
o	o	k7c6	o
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
miligramů	miligram	k1gInPc2	miligram
nikotinu	nikotin	k1gInSc6	nikotin
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
chybná	chybný	k2eAgNnPc1d1	chybné
(	(	kIx(	(
<g/>
příliš	příliš	k6eAd1	příliš
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
extrémním	extrémní	k2eAgInSc6d1	extrémní
případě	případ	k1gInSc6	případ
totiž	totiž	k9	totiž
přežil	přežít	k5eAaPmAgMnS	přežít
člověk	člověk	k1gMnSc1	člověk
požití	požití	k1gNnSc4	požití
4	[number]	k4	4
gramů	gram	k1gInPc2	gram
čistého	čistý	k2eAgInSc2d1	čistý
nikotinu	nikotin	k1gInSc2	nikotin
a	a	k8xC	a
experimentální	experimentální	k2eAgNnSc1d1	experimentální
požití	požití	k1gNnSc1	požití
několika	několik	k4yIc2	několik
stovek	stovka	k1gFnPc2	stovka
miligramů	miligram	k1gInPc2	miligram
nikotinu	nikotin	k1gInSc6	nikotin
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
nikotinovou	nikotinový	k2eAgFnSc4d1	nikotinová
intoxikaci	intoxikace	k1gFnSc4	intoxikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikoliv	nikoliv	k9	nikoliv
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Vážná	vážný	k2eAgFnSc1d1	vážná
intoxikace	intoxikace	k1gFnSc1	intoxikace
nikotinem	nikotin	k1gInSc7	nikotin
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
křečemi	křeč	k1gFnPc7	křeč
<g/>
,	,	kIx,	,
ochrnutím	ochrnutí	k1gNnSc7	ochrnutí
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
bez	bez	k7c2	bez
léčení	léčení	k1gNnSc2	léčení
nastává	nastávat	k5eAaImIp3nS	nastávat
smrt	smrt	k1gFnSc1	smrt
pro	pro	k7c4	pro
obrnu	obrna	k1gFnSc4	obrna
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
centra	centrum	k1gNnSc2	centrum
a	a	k8xC	a
následného	následný	k2eAgNnSc2d1	následné
udušení	udušení	k1gNnSc2	udušení
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
několik	několik	k4yIc1	několik
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zkoumaly	zkoumat	k5eAaImAgFnP	zkoumat
vliv	vliv	k1gInSc4	vliv
nikotinu	nikotin	k1gInSc2	nikotin
na	na	k7c4	na
neurologické	urologický	k2eNgInPc4d1	urologický
a	a	k8xC	a
nervosvalové	nervosvalový	k2eAgInPc4d1	nervosvalový
projevy	projev	k1gInPc4	projev
Alzheimerovy	Alzheimerův	k2eAgFnSc2d1	Alzheimerova
a	a	k8xC	a
Parkinsonovy	Parkinsonův	k2eAgFnSc2d1	Parkinsonova
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Majoritní	majoritní	k2eAgInSc4d1	majoritní
význam	význam	k1gInSc4	význam
však	však	k9	však
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
těchto	tento	k3xDgFnPc2	tento
nemocí	nemoc	k1gFnPc2	nemoc
nenalezl	naleznout	k5eNaPmAgMnS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Nikotin	nikotin	k1gInSc1	nikotin
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
používán	používat	k5eAaImNgInS	používat
především	především	k9	především
jako	jako	k8xC	jako
léčba	léčba	k1gFnSc1	léčba
pro	pro	k7c4	pro
závislé	závislý	k2eAgInPc4d1	závislý
na	na	k7c6	na
nikotinu	nikotin	k1gInSc6	nikotin
(	(	kIx(	(
<g/>
podávání	podávání	k1gNnSc1	podávání
nikotinu	nikotin	k1gInSc2	nikotin
formou	forma	k1gFnSc7	forma
náplastí	náplast	k1gFnSc7	náplast
<g/>
,	,	kIx,	,
sprejů	sprej	k1gInPc2	sprej
<g/>
,	,	kIx,	,
žvýkaček	žvýkačka	k1gFnPc2	žvýkačka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
se	se	k3xPyFc4	se
však	však	k9	však
konzultovat	konzultovat	k5eAaImF	konzultovat
užívání	užívání	k1gNnSc4	užívání
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
odvykání	odvykání	k1gNnSc4	odvykání
s	s	k7c7	s
lékařem	lékař	k1gMnSc7	lékař
specialistou	specialista	k1gMnSc7	specialista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívější	dřívější	k2eAgFnSc6d1	dřívější
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
nikotin	nikotin	k1gInSc1	nikotin
hojně	hojně	k6eAd1	hojně
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
insekticid	insekticid	k1gInSc1	insekticid
na	na	k7c6	na
potírání	potírání	k1gNnSc6	potírání
např.	např.	kA	např.
mšic	mšice	k1gFnPc2	mšice
<g/>
,	,	kIx,	,
třásněnek	třásněnka	k1gFnPc2	třásněnka
a	a	k8xC	a
roztočů	roztoč	k1gMnPc2	roztoč
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
nikotin	nikotin	k1gInSc1	nikotin
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
nikotinsulfát	nikotinsulfát	k5eAaPmF	nikotinsulfát
<g/>
)	)	kIx)	)
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
používán	používán	k2eAgMnSc1d1	používán
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
už	už	k6eAd1	už
spíše	spíše	k9	spíše
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
