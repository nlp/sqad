<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
touha	touha	k1gFnSc1	touha
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
napsaná	napsaný	k2eAgFnSc1d1	napsaná
americkým	americký	k2eAgMnSc7d1	americký
dramatikem	dramatik	k1gMnSc7	dramatik
Tennessee	Tennesse	k1gFnSc2	Tennesse
Williamsem	Williams	k1gMnSc7	Williams
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
drama	drama	k1gNnSc4	drama
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
