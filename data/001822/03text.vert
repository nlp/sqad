<s>
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
touha	touha	k1gFnSc1	touha
je	být	k5eAaImIp3nS	být
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
napsaná	napsaný	k2eAgFnSc1d1	napsaná
americkým	americký	k2eAgMnSc7d1	americký
dramatikem	dramatik	k1gMnSc7	dramatik
Tennessee	Tennesse	k1gFnSc2	Tennesse
Williamsem	Williams	k1gMnSc7	Williams
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yQgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
drama	drama	k1gNnSc4	drama
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
na	na	k7c6	na
Broadwayi	Broadway	k1gInSc6	Broadway
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1947	[number]	k4	1947
a	a	k8xC	a
ukončena	ukončen	k2eAgFnSc1d1	ukončena
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1949	[number]	k4	1949
v	v	k7c6	v
Ethel	Ethela	k1gFnPc2	Ethela
Barrymore	Barrymor	k1gMnSc5	Barrymor
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc1	uvedení
na	na	k7c6	na
Broadwayi	Broadway	k1gFnSc6	Broadway
režíroval	režírovat	k5eAaImAgMnS	režírovat
Elia	Elia	k1gMnSc1	Elia
Kazan	Kazany	k1gInPc2	Kazany
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
Marlon	Marlon	k1gInSc4	Marlon
Brando	Brando	k6eAd1	Brando
<g/>
,	,	kIx,	,
Jessica	Jessic	k2eAgFnSc1d1	Jessica
Tandyová	Tandyová	k1gFnSc1	Tandyová
<g/>
,	,	kIx,	,
Kim	Kim	k1gFnSc1	Kim
Hunterová	Hunterová	k1gFnSc1	Hunterová
a	a	k8xC	a
Karl	Karl	k1gMnSc1	Karl
Malden	Maldna	k1gFnPc2	Maldna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
hry	hra	k1gFnSc2	hra
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
Elia	Elia	k1gFnSc1	Elia
Kazanem	Kazan	k1gInSc7	Kazan
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
řadu	řada	k1gFnSc4	řada
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
akademické	akademický	k2eAgFnSc2d1	akademická
ceny	cena	k1gFnSc2	cena
pro	pro	k7c4	pro
Vivien	Vivien	k1gInSc4	Vivien
Leighovou	Leighová	k1gFnSc4	Leighová
za	za	k7c4	za
roli	role	k1gFnSc4	role
Blanche	Blanch	k1gFnSc2	Blanch
Duboisové	Duboisový	k2eAgFnSc2d1	Duboisová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k8xC	jako
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
opera	opera	k1gFnSc1	opera
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
André	André	k1gMnSc2	André
Previna	Previn	k2eAgMnSc2d1	Previn
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
A	A	kA	A
Streetcar	Streetcar	k1gMnSc1	Streetcar
Named	Named	k1gMnSc1	Named
Desire	Desir	k1gInSc5	Desir
(	(	kIx(	(
<g/>
play	play	k0	play
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Tramvaj	tramvaj	k1gFnSc1	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Touha	touha	k1gFnSc1	touha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Tramvaj	tramvaj	k1gFnSc4	tramvaj
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
Touha	touha	k1gFnSc1	touha
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
