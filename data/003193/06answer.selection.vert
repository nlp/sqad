<s>
Dlouhatánské	dlouhatánský	k2eAgInPc1d1	dlouhatánský
vlasy	vlas	k1gInPc1	vlas
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Weinsteina	Weinsteino	k1gNnSc2	Weinsteino
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
nejvýraznějším	výrazný	k2eAgInSc7d3	nejvýraznější
poznávacím	poznávací	k2eAgInSc7d1	poznávací
prvkem	prvek	k1gInSc7	prvek
metalové	metalový	k2eAgFnSc2d1	metalová
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Heavy	Heava	k1gFnSc2	Heava
metal	metal	k1gInSc1	metal
převzal	převzít	k5eAaPmAgInS	převzít
tento	tento	k3xDgInSc4	tento
rys	rys	k1gInSc4	rys
původně	původně	k6eAd1	původně
z	z	k7c2	z
kultury	kultura	k1gFnSc2	kultura
hippies	hippiesa	k1gFnPc2	hippiesa
<g/>
.	.	kIx.	.
</s>
