<p>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
(	(	kIx(	(
<g/>
konsonanty	konsonant	k1gInPc1	konsonant
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc4	takový
hlásky	hlásek	k1gInPc4	hlásek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šum	šum	k1gInSc1	šum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
specifickým	specifický	k2eAgNnSc7d1	specifické
postavením	postavení	k1gNnSc7	postavení
či	či	k8xC	či
pohybem	pohyb	k1gInSc7	pohyb
mluvidel	mluvidla	k1gNnPc2	mluvidla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
také	také	k9	také
počtem	počet	k1gInSc7	počet
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
:	:	kIx,	:
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
kavkazský	kavkazský	k2eAgInSc1d1	kavkazský
ubychijský	ubychijský	k2eAgInSc1d1	ubychijský
jazyk	jazyk	k1gInSc1	jazyk
jich	on	k3xPp3gInPc2	on
měl	mít	k5eAaImAgMnS	mít
84	[number]	k4	84
a	a	k8xC	a
novogvinejský	novogvinejský	k2eAgInSc1d1	novogvinejský
rotokas	rotokas	k1gInSc1	rotokas
pouhých	pouhý	k2eAgInPc2d1	pouhý
6	[number]	k4	6
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
abeceda	abeceda	k1gFnSc1	abeceda
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
písmen	písmeno	k1gNnPc2	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Artikulace	artikulace	k1gFnSc2	artikulace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
artikulace	artikulace	k1gFnSc2	artikulace
===	===	k?	===
</s>
</p>
<p>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
heterogenní	heterogenní	k2eAgFnSc7d1	heterogenní
skupinou	skupina	k1gFnSc7	skupina
zvuků	zvuk	k1gInPc2	zvuk
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
způsobů	způsob	k1gInPc2	způsob
jejich	jejich	k3xOp3gNnSc4	jejich
tvoření	tvoření	k1gNnSc4	tvoření
<g/>
.	.	kIx.	.
</s>
<s>
Společná	společný	k2eAgFnSc1d1	společná
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
souhlásky	souhláska	k1gFnPc4	souhláska
od	od	k7c2	od
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
relativní	relativní	k2eAgFnSc4d1	relativní
zavřenost	zavřenost	k1gFnSc4	zavřenost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
artikulaci	artikulace	k1gFnSc6	artikulace
jsou	být	k5eAaImIp3nP	být
ústa	ústa	k1gNnPc1	ústa
otevřena	otevřít	k5eAaPmNgNnP	otevřít
méně	málo	k6eAd2	málo
než	než	k8xS	než
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vznikají	vznikat	k5eAaImIp3nP	vznikat
překážky	překážka	k1gFnPc4	překážka
a	a	k8xC	a
turbulence	turbulence	k1gFnPc4	turbulence
při	při	k7c6	při
proudění	proudění	k1gNnSc6	proudění
vzduchu	vzduch	k1gInSc2	vzduch
–	–	k?	–
příčina	příčina	k1gFnSc1	příčina
vzniku	vznik	k1gInSc2	vznik
šumu	šum	k1gInSc2	šum
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Pulmonické	Pulmonický	k2eAgFnSc2d1	Pulmonický
souhlásky	souhláska	k1gFnSc2	souhláska
====	====	k?	====
</s>
</p>
<p>
<s>
Pulmonické	Pulmonický	k2eAgInPc1d1	Pulmonický
hlásky	hlásek	k1gInPc1	hlásek
vznikají	vznikat	k5eAaImIp3nP	vznikat
prouděním	proudění	k1gNnSc7	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgInS	uvádět
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
pomocí	pomocí	k7c2	pomocí
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
pulmonických	pulmonický	k2eAgFnPc2d1	pulmonický
souhlásek	souhláska	k1gFnPc2	souhláska
jsou	být	k5eAaImIp3nP	být
následující	následující	k2eAgInPc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Konstriktivy	konstriktiva	k1gFnSc2	konstriktiva
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Konstriktivy	konstriktiva	k1gFnPc1	konstriktiva
–	–	k?	–
úžinové	úžinový	k2eAgFnPc1d1	úžinová
souhlásky	souhláska	k1gFnPc1	souhláska
–	–	k?	–
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
těsným	těsný	k2eAgNnSc7d1	těsné
přiblížením	přiblížení	k1gNnSc7	přiblížení
dvou	dva	k4xCgInPc2	dva
artikulátorů	artikulátor	k1gInPc2	artikulátor
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
úžina	úžina	k1gFnSc1	úžina
(	(	kIx(	(
<g/>
konstrikce	konstrikce	k1gFnSc1	konstrikce
<g/>
)	)	kIx)	)
a	a	k8xC	a
silný	silný	k2eAgInSc4d1	silný
šum	šum	k1gInSc4	šum
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
artikulaci	artikulace	k1gFnSc6	artikulace
konstriktiv	konstriktiva	k1gFnPc2	konstriktiva
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přesné	přesný	k2eAgNnSc1d1	přesné
postavení	postavení	k1gNnSc1	postavení
artikulátorů	artikulátor	k1gInPc2	artikulátor
<g/>
,	,	kIx,	,
i	i	k8xC	i
malá	malý	k2eAgFnSc1d1	malá
odchylka	odchylka	k1gFnSc1	odchylka
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
slyšitelná	slyšitelný	k2eAgFnSc1d1	slyšitelná
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sluchového	sluchový	k2eAgInSc2d1	sluchový
(	(	kIx(	(
<g/>
akustického	akustický	k2eAgInSc2d1	akustický
<g/>
)	)	kIx)	)
dojmu	dojem	k1gInSc2	dojem
se	se	k3xPyFc4	se
též	též	k9	též
nazývají	nazývat	k5eAaImIp3nP	nazývat
třené	třený	k2eAgFnPc1d1	třená
souhlásky	souhláska	k1gFnPc1	souhláska
(	(	kIx(	(
<g/>
frikativy	frikativa	k1gFnPc1	frikativa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Okluzivy	Okluziv	k1gInPc4	Okluziv
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Okluzivy	Okluziva	k1gFnPc1	Okluziva
–	–	k?	–
závěrové	závěrový	k2eAgFnPc1d1	závěrová
souhlásky	souhláska	k1gFnPc1	souhláska
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
přechodným	přechodný	k2eAgNnSc7d1	přechodné
vytvořením	vytvoření	k1gNnSc7	vytvoření
překážky	překážka	k1gFnSc2	překážka
(	(	kIx(	(
<g/>
okluze	okluze	k1gFnSc1	okluze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
proudění	proudění	k1gNnSc4	proudění
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Uvolněním	uvolnění	k1gNnSc7	uvolnění
závěru	závěr	k1gInSc2	závěr
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
prudkému	prudký	k2eAgNnSc3d1	prudké
uvolnění	uvolnění	k1gNnSc3	uvolnění
přetlaku	přetlak	k1gInSc2	přetlak
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
typický	typický	k2eAgInSc1d1	typický
šum	šum	k1gInSc1	šum
(	(	kIx(	(
<g/>
exploze	exploze	k1gFnSc1	exploze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sluchového	sluchový	k2eAgInSc2d1	sluchový
dojmu	dojem	k1gInSc2	dojem
se	se	k3xPyFc4	se
též	též	k9	též
nazývají	nazývat	k5eAaImIp3nP	nazývat
ražené	ražený	k2eAgFnPc1d1	ražená
souhlásky	souhláska	k1gFnPc1	souhláska
(	(	kIx(	(
<g/>
explozivy	exploziva	k1gFnPc1	exploziva
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jen	jen	k9	jen
plozivy	ploziva	k1gFnSc2	ploziva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Semiokluzivy	semiokluziva	k1gFnSc2	semiokluziva
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Semiokluzivy	semiokluziva	k1gFnPc1	semiokluziva
–	–	k?	–
polozávěrové	polozávěrový	k2eAgFnPc1d1	polozávěrový
souhlásky	souhláska	k1gFnPc1	souhláska
–	–	k?	–
vznikají	vznikat	k5eAaImIp3nP	vznikat
prvotní	prvotní	k2eAgFnSc7d1	prvotní
krátkou	krátký	k2eAgFnSc7d1	krátká
okluzí	okluze	k1gFnSc7	okluze
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vzápětí	vzápětí	k6eAd1	vzápětí
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
a	a	k8xC	a
plynule	plynule	k6eAd1	plynule
následována	následovat	k5eAaImNgFnS	následovat
konstrikcí	konstrikce	k1gFnSc7	konstrikce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
rychlý	rychlý	k2eAgInSc4d1	rychlý
sled	sled	k1gInSc4	sled
okluzivy	okluziva	k1gFnSc2	okluziva
a	a	k8xC	a
konstriktivy	konstriktiva	k1gFnSc2	konstriktiva
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
artikulovány	artikulovat	k5eAaImNgInP	artikulovat
současně	současně	k6eAd1	současně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
lze	lze	k6eAd1	lze
zřetelně	zřetelně	k6eAd1	zřetelně
rozlišit	rozlišit	k5eAaPmF	rozlišit
současnou	současný	k2eAgFnSc4d1	současná
artikulaci	artikulace	k1gFnSc4	artikulace
u	u	k7c2	u
semiokluzivy	semiokluziva	k1gFnSc2	semiokluziva
(	(	kIx(	(
<g/>
např.	např.	kA	např.
české	český	k2eAgInPc1d1	český
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
/	/	kIx~	/
[	[	kIx(	[
<g/>
ts	ts	k0	ts
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
od	od	k7c2	od
dvou	dva	k4xCgFnPc2	dva
samostatně	samostatně	k6eAd1	samostatně
vyslovených	vyslovený	k2eAgFnPc2d1	vyslovená
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
např.	např.	kA	např.
[	[	kIx(	[
<g/>
t	t	k?	t
<g/>
]	]	kIx)	]
a	a	k8xC	a
[	[	kIx(	[
<g/>
s	s	k7c7	s
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sluchového	sluchový	k2eAgInSc2d1	sluchový
dojmu	dojem	k1gInSc2	dojem
se	se	k3xPyFc4	se
též	též	k9	též
nazývají	nazývat	k5eAaImIp3nP	nazývat
poloražené	poloražený	k2eAgFnPc1d1	poloražený
souhlásky	souhláska	k1gFnPc1	souhláska
(	(	kIx(	(
<g/>
afrikáty	afrikáta	k1gFnPc1	afrikáta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Nazály	nazála	k1gFnSc2	nazála
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Nazály	nazála	k1gFnPc1	nazála
<g/>
,	,	kIx,	,
nosovky	nosovka	k1gFnPc1	nosovka
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
vytvořením	vytvoření	k1gNnSc7	vytvoření
okluze	okluze	k1gFnSc2	okluze
(	(	kIx(	(
<g/>
závěru	závěr	k1gInSc2	závěr
<g/>
)	)	kIx)	)
v	v	k7c6	v
dutině	dutina	k1gFnSc6	dutina
ústní	ústní	k2eAgFnSc1d1	ústní
a	a	k8xC	a
uvolněním	uvolnění	k1gNnSc7	uvolnění
cesty	cesta	k1gFnSc2	cesta
vzduchu	vzduch	k1gInSc2	vzduch
nosní	nosní	k2eAgFnSc7d1	nosní
dutinou	dutina	k1gFnSc7	dutina
poklesem	pokles	k1gInSc7	pokles
měkkého	měkký	k2eAgNnSc2d1	měkké
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	s	k7c7	s
šumem	šum	k1gInSc7	šum
i	i	k8xC	i
tónem	tón	k1gInSc7	tón
(	(	kIx(	(
<g/>
sonoritou	sonorita	k1gFnSc7	sonorita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
formován	formovat	k5eAaImNgInS	formovat
rezonancí	rezonance	k1gFnSc7	rezonance
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
(	(	kIx(	(
<g/>
závislé	závislý	k2eAgFnSc2d1	závislá
na	na	k7c6	na
postavení	postavení	k1gNnSc6	postavení
artikulátorů	artikulátor	k1gInPc2	artikulátor
a	a	k8xC	a
místě	místo	k1gNnSc6	místo
vytvoření	vytvoření	k1gNnSc2	vytvoření
okluze	okluze	k1gFnSc2	okluze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
souhlásky	souhláska	k1gFnPc4	souhláska
znělé	znělý	k2eAgFnPc4d1	znělá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
artikulace	artikulace	k1gFnSc2	artikulace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
okluze	okluze	k1gFnSc2	okluze
v	v	k7c6	v
ústní	ústní	k2eAgFnSc6d1	ústní
dutině	dutina	k1gFnSc6	dutina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
též	též	k9	též
někdy	někdy	k6eAd1	někdy
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
mezi	mezi	k7c7	mezi
okluzivy	okluziv	k1gInPc7	okluziv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Aproximanty	Aproximant	k1gInPc4	Aproximant
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Aproximanty	Aproximanta	k1gFnPc4	Aproximanta
vznikají	vznikat	k5eAaImIp3nP	vznikat
přiblížením	přiblížení	k1gNnSc7	přiblížení
(	(	kIx(	(
<g/>
aproximací	aproximace	k1gFnPc2	aproximace
<g/>
)	)	kIx)	)
artikulátorů	artikulátor	k1gInPc2	artikulátor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
těsné	těsný	k2eAgNnSc4d1	těsné
jako	jako	k9	jako
u	u	k7c2	u
konstriktiv	konstriktiva	k1gFnPc2	konstriktiva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
těsnější	těsný	k2eAgInSc1d2	těsnější
než	než	k8xS	než
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
šum	šum	k1gInSc4	šum
ani	ani	k8xC	ani
tón	tón	k1gInSc4	tón
<g/>
.	.	kIx.	.
</s>
<s>
Aproximanty	Aproximanta	k1gFnPc1	Aproximanta
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
otevřené	otevřený	k2eAgFnPc1d1	otevřená
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tedy	tedy	k9	tedy
jakýsi	jakýsi	k3yIgInSc4	jakýsi
plynulý	plynulý	k2eAgInSc4d1	plynulý
přechod	přechod	k1gInSc4	přechod
k	k	k7c3	k
samohláskám	samohláska	k1gFnPc3	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Vibranty	vibranta	k1gFnSc2	vibranta
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Vibranty	vibranta	k1gFnPc1	vibranta
<g/>
,	,	kIx,	,
kmitavé	kmitavý	k2eAgFnPc1d1	kmitavá
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
aerodynamicky	aerodynamicky	k6eAd1	aerodynamicky
podmíněným	podmíněný	k2eAgInSc7d1	podmíněný
<g/>
,	,	kIx,	,
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
dotykem	dotyk	k1gInSc7	dotyk
artikulátorů	artikulátor	k1gInPc2	artikulátor
(	(	kIx(	(
<g/>
kmitáním	kmitání	k1gNnSc7	kmitání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Verberanty	Verberant	k1gMnPc7	Verberant
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Verberanty	Verberant	k1gMnPc4	Verberant
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
švihy	švih	k1gInPc1	švih
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vžitý	vžitý	k2eAgInSc1d1	vžitý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
rychlým	rychlý	k2eAgInSc7d1	rychlý
dotykem	dotyk	k1gInSc7	dotyk
dvou	dva	k4xCgInPc2	dva
artikulátorů	artikulátor	k1gInPc2	artikulátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vibrant	vibranta	k1gFnPc2	vibranta
pouze	pouze	k6eAd1	pouze
jednorázový	jednorázový	k2eAgInSc1d1	jednorázový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nedůrazné	důrazný	k2eNgInPc1d1	nedůrazný
"	"	kIx"	"
<g/>
r	r	kA	r
<g/>
"	"	kIx"	"
artikuluje	artikulovat	k5eAaImIp3nS	artikulovat
spíš	spíš	k9	spíš
jako	jako	k9	jako
verberant	verberant	k1gMnSc1	verberant
než	než	k8xS	než
jako	jako	k9	jako
vibrant	vibranta	k1gFnPc2	vibranta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Laterální	laterální	k2eAgMnSc1d1	laterální
vs	vs	k?	vs
<g/>
.	.	kIx.	.
středové	středový	k2eAgFnSc2d1	středová
souhlásky	souhláska	k1gFnSc2	souhláska
=====	=====	k?	=====
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
souhlásek	souhláska	k1gFnPc2	souhláska
vzniká	vznikat	k5eAaImIp3nS	vznikat
prouděním	proudění	k1gNnSc7	proudění
vzduchu	vzduch	k1gInSc2	vzduch
podél	podél	k7c2	podél
středové	středový	k2eAgFnSc2d1	středová
linie	linie	k1gFnSc2	linie
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
konstriktiv	konstriktiva	k1gFnPc2	konstriktiva
a	a	k8xC	a
aproximant	aproximanta	k1gFnPc2	aproximanta
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vzduch	vzduch	k1gInSc1	vzduch
proudit	proudit	k5eAaPmF	proudit
také	také	k9	také
okolo	okolo	k7c2	okolo
boků	bok	k1gInPc2	bok
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
špička	špička	k1gFnSc1	špička
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
dotýkat	dotýkat	k5eAaImF	dotýkat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
bokové	bokový	k2eAgFnPc1d1	Boková
(	(	kIx(	(
<g/>
laterální	laterální	k2eAgFnPc1d1	laterální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nepulmonické	Nepulmonický	k2eAgFnSc2d1	Nepulmonický
souhlásky	souhláska	k1gFnSc2	souhláska
====	====	k?	====
</s>
</p>
<p>
<s>
Nepulmonické	Nepulmonický	k2eAgFnPc1d1	Nepulmonický
souhlásky	souhláska	k1gFnPc1	souhláska
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
proudem	proud	k1gInSc7	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dáván	dávat	k5eAaImNgInS	dávat
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
než	než	k8xS	než
plícemi	plíce	k1gFnPc7	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
mlaskavky	mlaskavka	k1gFnPc4	mlaskavka
<g/>
,	,	kIx,	,
ejektivy	ejektiv	k1gInPc4	ejektiv
a	a	k8xC	a
implozivy	imploziva	k1gFnPc4	imploziva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
artikulace	artikulace	k1gFnSc2	artikulace
===	===	k?	===
</s>
</p>
<p>
<s>
Následující	následující	k2eAgNnSc1d1	následující
rozdělení	rozdělení	k1gNnSc1	rozdělení
souhlásek	souhláska	k1gFnPc2	souhláska
používá	používat	k5eAaImIp3nS	používat
názvosloví	názvosloví	k1gNnSc1	názvosloví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
systematické	systematický	k2eAgNnSc1d1	systematické
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
částečně	částečně	k6eAd1	částečně
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
oba	dva	k4xCgInPc4	dva
artikulátory	artikulátor	k1gInPc4	artikulátor
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
jen	jen	k9	jen
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
artikulátorů	artikulátor	k1gInPc2	artikulátor
zapojených	zapojený	k2eAgInPc2d1	zapojený
do	do	k7c2	do
tvoření	tvoření	k1gNnSc2	tvoření
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
vžité	vžitý	k2eAgNnSc1d1	vžité
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
užívané	užívaný	k2eAgNnSc1d1	užívané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sekundární	sekundární	k2eAgFnSc2d1	sekundární
modifikace	modifikace	k1gFnSc2	modifikace
výslovnosti	výslovnost	k1gFnSc2	výslovnost
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgFnPc2d1	uvedená
míst	místo	k1gNnPc2	místo
artikulace	artikulace	k1gFnSc2	artikulace
se	se	k3xPyFc4	se
do	do	k7c2	do
procesu	proces	k1gInSc2	proces
tvoření	tvoření	k1gNnSc2	tvoření
souhlásek	souhláska	k1gFnPc2	souhláska
mohou	moct	k5eAaImIp3nP	moct
zapojovat	zapojovat	k5eAaImF	zapojovat
další	další	k2eAgInPc4d1	další
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
znění	znění	k1gNnSc4	znění
souhlásky	souhláska	k1gFnSc2	souhláska
modifikují	modifikovat	k5eAaBmIp3nP	modifikovat
<g/>
.	.	kIx.	.
</s>
<s>
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
artikulace	artikulace	k1gFnSc1	artikulace
se	se	k3xPyFc4	se
v	v	k7c6	v
přepisu	přepis	k1gInSc6	přepis
výslovnosti	výslovnost	k1gFnSc2	výslovnost
označuje	označovat	k5eAaImIp3nS	označovat
pomocí	pomocí	k7c2	pomocí
horních	horní	k2eAgInPc2d1	horní
indexů	index	k1gInPc2	index
a	a	k8xC	a
diakritiky	diakritika	k1gFnSc2	diakritika
<g/>
,	,	kIx,	,
přidávaných	přidávaný	k2eAgInPc2d1	přidávaný
k	k	k7c3	k
základnímu	základní	k2eAgInSc3d1	základní
symbolu	symbol	k1gInSc3	symbol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
české	český	k2eAgFnSc6d1	Česká
výslovnosti	výslovnost	k1gFnSc6	výslovnost
se	se	k3xPyFc4	se
takovéto	takovýto	k3xDgFnPc1	takovýto
modifikace	modifikace	k1gFnPc1	modifikace
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hlasnost	hlasnost	k1gFnSc4	hlasnost
===	===	k?	===
</s>
</p>
<p>
<s>
Hlasností	hlasnost	k1gFnSc7	hlasnost
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
účast	účast	k1gFnSc1	účast
aktivní	aktivní	k2eAgMnPc1d1	aktivní
hlasivek	hlasivka	k1gFnPc2	hlasivka
(	(	kIx(	(
<g/>
fonace	fonace	k1gFnSc1	fonace
<g/>
)	)	kIx)	)
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
hlásek	hláska	k1gFnPc2	hláska
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
akusticky	akusticky	k6eAd1	akusticky
projevuje	projevovat	k5eAaImIp3nS	projevovat
znělostí	znělost	k1gFnSc7	znělost
<g/>
.	.	kIx.	.
</s>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
hlasné	hlasný	k2eAgMnPc4d1	hlasný
(	(	kIx(	(
<g/>
znělé	znělý	k2eAgMnPc4d1	znělý
<g/>
)	)	kIx)	)
a	a	k8xC	a
nehlasné	hlasný	k2eNgFnPc1d1	hlasný
(	(	kIx(	(
<g/>
neznělé	znělý	k2eNgFnPc1d1	neznělá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
páry	pár	k1gInPc1	pár
lišící	lišící	k2eAgInPc1d1	lišící
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
hlasností	hlasnost	k1gFnSc7	hlasnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
kromě	kromě	k7c2	kromě
normální	normální	k2eAgFnSc2d1	normální
(	(	kIx(	(
<g/>
modální	modální	k2eAgFnSc2d1	modální
<g/>
)	)	kIx)	)
fonace	fonace	k1gFnSc2	fonace
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
také	také	k9	také
fonace	fonace	k1gFnSc1	fonace
dyšná	dyšný	k2eAgFnSc1d1	dyšný
a	a	k8xC	a
třepená	třepený	k2eAgFnSc1d1	třepený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Iniciační	iniciační	k2eAgInSc1d1	iniciační
mechanismus	mechanismus	k1gInSc1	mechanismus
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
všechny	všechen	k3xTgFnPc1	všechen
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
pulmonické	pulmonický	k2eAgNnSc1d1	pulmonický
egresivní	egresivní	k2eAgInPc1d1	egresivní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
iniciační	iniciační	k2eAgInSc1d1	iniciační
mechanismus	mechanismus	k1gInSc1	mechanismus
<g/>
,	,	kIx,	,
dodávající	dodávající	k2eAgInSc1d1	dodávající
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnPc4	jejich
tvoření	tvoření	k1gNnPc4	tvoření
<g/>
,	,	kIx,	,
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
vytlačování	vytlačování	k1gNnSc6	vytlačování
vzduchu	vzduch	k1gInSc2	vzduch
z	z	k7c2	z
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souhlásky	souhláska	k1gFnPc1	souhláska
můžeme	moct	k5eAaImIp1nP	moct
dělit	dělit	k5eAaImF	dělit
na	na	k7c6	na
egresivní	egresivní	k2eAgFnSc6d1	egresivní
a	a	k8xC	a
ingresivní	ingresivní	k2eAgFnSc6d1	ingresivní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
tvoření	tvoření	k1gNnSc6	tvoření
egresivních	egresivní	k2eAgFnPc2d1	egresivní
hlásek	hláska	k1gFnPc2	hláska
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
mluvidel	mluvidla	k1gNnPc2	mluvidla
přetlak	přetlak	k1gInSc4	přetlak
<g/>
,	,	kIx,	,
vytlačující	vytlačující	k2eAgInSc4d1	vytlačující
vzduch	vzduch	k1gInSc4	vzduch
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
ingresivních	ingresivní	k2eAgFnPc2d1	ingresivní
hlásek	hláska	k1gFnPc2	hláska
vzniká	vznikat	k5eAaImIp3nS	vznikat
podtlak	podtlak	k1gInSc1	podtlak
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nasává	nasávat	k5eAaImIp3nS	nasávat
vzduch	vzduch	k1gInSc4	vzduch
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
přetlak	přetlak	k1gInSc4	přetlak
nebo	nebo	k8xC	nebo
podtlak	podtlak	k1gInSc4	podtlak
<g/>
,	,	kIx,	,
dělíme	dělit	k5eAaImIp1nP	dělit
souhlásky	souhláska	k1gFnPc1	souhláska
na	na	k7c4	na
pulmonické	pulmonický	k2eAgFnPc4d1	pulmonický
(	(	kIx(	(
<g/>
plíce	plíce	k1gFnPc4	plíce
a	a	k8xC	a
bránice	bránice	k1gFnPc4	bránice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
glotalické	glotalický	k2eAgFnPc1d1	glotalický
(	(	kIx(	(
<g/>
hlasivky	hlasivka	k1gFnPc1	hlasivka
<g/>
)	)	kIx)	)
a	a	k8xC	a
velarické	velarický	k2eAgNnSc1d1	velarický
(	(	kIx(	(
<g/>
měkké	měkký	k2eAgNnSc1d1	měkké
patro	patro	k1gNnSc1	patro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Akustické	akustický	k2eAgFnPc4d1	akustická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
akustického	akustický	k2eAgNnSc2d1	akustické
hlediska	hledisko	k1gNnSc2	hledisko
jsou	být	k5eAaImIp3nP	být
souhlásky	souhláska	k1gFnPc1	souhláska
dosti	dosti	k6eAd1	dosti
heterogenní	heterogenní	k2eAgFnSc2d1	heterogenní
skupinou	skupina	k1gFnSc7	skupina
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Společnou	společný	k2eAgFnSc7d1	společná
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
šumu	šum	k1gInSc3	šum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
nepravidelných	pravidelný	k2eNgInPc2d1	nepravidelný
kmitů	kmit	k1gInPc2	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
konsonantnost	konsonantnost	k1gFnSc1	konsonantnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgInSc1d1	hlavní
rozdíl	rozdíl	k1gInSc1	rozdíl
od	od	k7c2	od
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obstruenty	Obstruent	k1gInPc1	Obstruent
a	a	k8xC	a
sonory	sonora	k1gFnPc1	sonora
<g/>
,	,	kIx,	,
znělost	znělost	k1gFnSc1	znělost
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
obstruenty	obstruent	k1gInPc1	obstruent
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
nevokální	vokální	k2eNgFnPc1d1	vokální
souhlásky	souhláska	k1gFnPc1	souhláska
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
šumu	šum	k1gInSc2	šum
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
překážky	překážka	k1gFnSc2	překážka
(	(	kIx(	(
<g/>
obstrukce	obstrukce	k1gFnSc1	obstrukce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
staví	stavit	k5eAaImIp3nS	stavit
do	do	k7c2	do
proudu	proud	k1gInSc2	proud
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
jazycích	jazyk	k1gInPc6	jazyk
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
korelační	korelační	k2eAgInPc1d1	korelační
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc7	jejichž
členy	člen	k1gInPc7	člen
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
znělostí	znělost	k1gFnSc7	znělost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
explozivy	explozivo	k1gNnPc7	explozivo
(	(	kIx(	(
<g/>
okluzivy	okluziv	k1gInPc7	okluziv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
afrikáty	afrikáta	k1gFnPc4	afrikáta
(	(	kIx(	(
<g/>
semiokluzivy	semiokluziva	k1gFnPc4	semiokluziva
<g/>
)	)	kIx)	)
a	a	k8xC	a
frikativy	frikativa	k1gFnSc2	frikativa
(	(	kIx(	(
<g/>
konstriktivy	konstriktiva	k1gFnSc2	konstriktiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znělé	znělý	k2eAgFnPc1d1	znělá
pravé	pravý	k2eAgFnPc1d1	pravá
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
přítomností	přítomnost	k1gFnSc7	přítomnost
tónu	tón	k1gInSc2	tón
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
jiné	jiný	k2eAgFnPc4d1	jiná
kvality	kvalita	k1gFnPc4	kvalita
než	než	k8xS	než
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
a	a	k8xC	a
sonor	sonora	k1gFnPc2	sonora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
terminologii	terminologie	k1gFnSc6	terminologie
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
pravé	pravý	k2eAgFnPc4d1	pravá
souhlásky	souhláska	k1gFnPc4	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podskupinou	podskupina	k1gFnSc7	podskupina
frikativ	frikativa	k1gFnPc2	frikativa
jsou	být	k5eAaImIp3nP	být
sibilanty	sibilanta	k1gFnPc1	sibilanta
(	(	kIx(	(
<g/>
sykavky	sykavka	k1gFnPc1	sykavka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vyšší	vysoký	k2eAgFnSc7d2	vyšší
amplitudou	amplituda	k1gFnSc7	amplituda
a	a	k8xC	a
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
hlasitější	hlasitý	k2eAgMnPc4d2	hlasitější
než	než	k8xS	než
non-sibilanty	nonibilant	k1gMnPc4	non-sibilant
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
frikativy	frikativa	k1gFnPc4	frikativa
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
alveolárních	alveolární	k2eAgInPc2d1	alveolární
a	a	k8xC	a
postalveolárních	postalveolární	k2eAgInPc2d1	postalveolární
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
to	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
o	o	k7c6	o
asibilantách	asibilanta	k1gFnPc6	asibilanta
(	(	kIx(	(
<g/>
polosykavkách	polosykavka	k1gFnPc6	polosykavka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
afrikáty	afrikáta	k1gFnPc1	afrikáta
tvořené	tvořený	k2eAgFnPc1d1	tvořená
na	na	k7c6	na
témže	týž	k3xTgNnSc6	týž
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
sonory	sonora	k1gFnPc4	sonora
(	(	kIx(	(
<g/>
sonoranty	sonorant	k1gMnPc4	sonorant
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
znělé	znělý	k2eAgInPc1d1	znělý
(	(	kIx(	(
<g/>
netvoří	tvořit	k5eNaImIp3nP	tvořit
znělostní	znělostní	k2eAgInPc1d1	znělostní
páry	pár	k1gInPc1	pár
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
vokálností	vokálnost	k1gFnSc7	vokálnost
–	–	k?	–
přítomností	přítomnost	k1gFnSc7	přítomnost
vyšších	vysoký	k2eAgFnPc2d2	vyšší
frekvencí	frekvence	k1gFnSc7	frekvence
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
formantů	formans	k1gInPc2	formans
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
F2	F2	k1gFnSc1	F2
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
terminologii	terminologie	k1gFnSc6	terminologie
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
jedinečné	jedinečný	k2eAgFnPc1d1	jedinečná
souhlásky	souhláska	k1gFnPc1	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
jedinečným	jedinečný	k2eAgFnPc3d1	jedinečná
souhláskám	souhláska	k1gFnPc3	souhláska
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
klouzavé	klouzavý	k2eAgInPc1d1	klouzavý
hlásky	hlásek	k1gInPc1	hlásek
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
glide	glide	k6eAd1	glide
<g/>
,	,	kIx,	,
např.	např.	kA	např.
[	[	kIx(	[
<g/>
w	w	k?	w
<g/>
,	,	kIx,	,
j	j	k?	j
<g/>
,	,	kIx,	,
ʊ	ʊ	k?	ʊ
<g/>
̯	̯	k?	̯
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
polovokály	polovokál	k1gInPc7	polovokál
(	(	kIx(	(
<g/>
blízké	blízký	k2eAgFnSc2d1	blízká
samohláskám	samohláska	k1gFnPc3	samohláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nemají	mít	k5eNaImIp3nP	mít
příznak	příznak	k1gInSc4	příznak
konsonantnosti	konsonantnost	k1gFnSc2	konsonantnost
ani	ani	k8xC	ani
vokálnosti	vokálnost	k1gFnSc2	vokálnost
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
znělé	znělý	k2eAgFnPc1d1	znělá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znělost	znělost	k1gFnSc1	znělost
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
aktivní	aktivní	k2eAgFnSc7d1	aktivní
činností	činnost	k1gFnSc7	činnost
hlasivek	hlasivka	k1gFnPc2	hlasivka
(	(	kIx(	(
<g/>
fonací	fonace	k1gFnPc2	fonace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
základní	základní	k2eAgFnSc4d1	základní
frekvenci	frekvence	k1gFnSc4	frekvence
F	F	kA	F
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Znělé	znělý	k2eAgFnPc1d1	znělá
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
přítomností	přítomnost	k1gFnSc7	přítomnost
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
uvádí	uvádět	k5eAaImIp3nS	uvádět
tabulka	tabulka	k1gFnSc1	tabulka
i	i	k8xC	i
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kompaktnost	kompaktnost	k1gFnSc1	kompaktnost
–	–	k?	–
nekompaktnost	nekompaktnost	k1gFnSc1	nekompaktnost
===	===	k?	===
</s>
</p>
<p>
<s>
Kompaktní	kompaktní	k2eAgInPc1d1	kompaktní
jsou	být	k5eAaImIp3nP	být
hlásky	hlásek	k1gInPc1	hlásek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
energii	energie	k1gFnSc4	energie
soustředěnou	soustředěný	k2eAgFnSc4d1	soustředěná
uprostřed	uprostřed	k7c2	uprostřed
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
nekompaktní	kompaktní	k2eNgFnPc1d1	nekompaktní
mají	mít	k5eAaImIp3nP	mít
toto	tento	k3xDgNnSc4	tento
maximum	maximum	k1gNnSc4	maximum
posunuto	posunut	k2eAgNnSc4d1	posunuto
k	k	k7c3	k
hornímu	horní	k2eAgMnSc3d1	horní
nebo	nebo	k8xC	nebo
dolnímu	dolní	k2eAgInSc3d1	dolní
okraji	okraj	k1gInSc3	okraj
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Kompaktní	kompaktní	k2eAgFnPc1d1	kompaktní
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
ústní	ústní	k2eAgFnSc2d1	ústní
dutiny	dutina	k1gFnSc2	dutina
(	(	kIx(	(
<g/>
od	od	k7c2	od
postalveolár	postalveolár	k1gInSc4	postalveolár
až	až	k9	až
po	po	k7c4	po
glotály	glotál	k1gInPc4	glotál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
nekompaktní	kompaktní	k2eNgMnPc1d1	nekompaktní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Drsnost	drsnost	k1gFnSc1	drsnost
–	–	k?	–
matnost	matnost	k1gFnSc1	matnost
===	===	k?	===
</s>
</p>
<p>
<s>
Drsné	drsný	k2eAgFnPc1d1	drsná
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
výraznými	výrazný	k2eAgFnPc7d1	výrazná
nepravidelnostmi	nepravidelnost	k1gFnPc7	nepravidelnost
složek	složka	k1gFnPc2	složka
zvuku	zvuk	k1gInSc2	zvuk
<g/>
,	,	kIx,	,
velkým	velký	k2eAgInSc7d1	velký
třecím	třecí	k2eAgInSc7d1	třecí
šumem	šum	k1gInSc7	šum
způsobeným	způsobený	k2eAgInSc7d1	způsobený
turbulencemi	turbulence	k1gFnPc7	turbulence
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Drsné	drsný	k2eAgFnPc1d1	drsná
jsou	být	k5eAaImIp3nP	být
frikativy	frikativa	k1gFnPc1	frikativa
a	a	k8xC	a
afrikáty	afrikáta	k1gFnPc1	afrikáta
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
souhlásky	souhláska	k1gFnPc1	souhláska
jsou	být	k5eAaImIp3nP	být
matné	matný	k2eAgFnPc1d1	matná
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	s	k7c7	s
sourodým	sourodý	k2eAgNnSc7d1	sourodé
zvukovým	zvukový	k2eAgNnSc7d1	zvukové
spektrem	spektrum	k1gNnSc7	spektrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kontinuálnost	Kontinuálnost	k1gFnSc1	Kontinuálnost
–	–	k?	–
nekontinuálnost	nekontinuálnost	k1gFnSc1	nekontinuálnost
===	===	k?	===
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
časového	časový	k2eAgNnSc2d1	časové
trvání	trvání	k1gNnSc2	trvání
artikulace	artikulace	k1gFnSc2	artikulace
a	a	k8xC	a
výsledného	výsledný	k2eAgInSc2d1	výsledný
zvuku	zvuk	k1gInSc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Kontinuální	kontinuální	k2eAgFnPc4d1	kontinuální
(	(	kIx(	(
<g/>
nepřerušené	přerušený	k2eNgFnPc4d1	nepřerušená
<g/>
)	)	kIx)	)
souhlásky	souhláska	k1gFnPc4	souhláska
lze	lze	k6eAd1	lze
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
relativně	relativně	k6eAd1	relativně
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nekontinuální	kontinuální	k2eNgMnSc1d1	kontinuální
mají	mít	k5eAaImIp3nP	mít
během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
artikulace	artikulace	k1gFnSc2	artikulace
úplný	úplný	k2eAgInSc4d1	úplný
závěr	závěr	k1gInSc4	závěr
(	(	kIx(	(
<g/>
okluzi	okluze	k1gFnSc4	okluze
<g/>
)	)	kIx)	)
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
explozí	exploze	k1gFnSc7	exploze
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
explozivy	exploziv	k1gInPc4	exploziv
(	(	kIx(	(
<g/>
okluzivy	okluziva	k1gFnPc4	okluziva
<g/>
)	)	kIx)	)
orální	orální	k2eAgFnPc4d1	orální
i	i	k8xC	i
nazální	nazální	k2eAgFnPc4d1	nazální
a	a	k8xC	a
afrikáty	afrikáta	k1gFnPc4	afrikáta
(	(	kIx(	(
<g/>
semiokluzivy	semiokluziva	k1gFnPc4	semiokluziva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Gravisovost	Gravisovost	k1gFnSc1	Gravisovost
–	–	k?	–
akutovost	akutovost	k1gFnSc1	akutovost
===	===	k?	===
</s>
</p>
<p>
<s>
Gravisové	Gravisový	k2eAgFnPc4d1	Gravisový
(	(	kIx(	(
<g/>
tupé	tupý	k2eAgFnPc4d1	tupá
<g/>
)	)	kIx)	)
souhlásky	souhláska	k1gFnPc4	souhláska
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
nízké	nízký	k2eAgFnPc4d1	nízká
frekvence	frekvence	k1gFnPc4	frekvence
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
přední	přední	k2eAgMnSc1d1	přední
(	(	kIx(	(
<g/>
bilabiály	bilabiála	k1gFnPc1	bilabiála
<g/>
,	,	kIx,	,
labiodentály	labiodentála	k1gFnPc1	labiodentála
a	a	k8xC	a
dentály	dentála	k1gFnPc1	dentála
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadní	zadní	k2eAgFnPc1d1	zadní
(	(	kIx(	(
<g/>
veláry	velára	k1gFnPc1	velára
–	–	k?	–
glotály	glotála	k1gFnSc2	glotála
<g/>
)	)	kIx)	)
souhlásky	souhláska	k1gFnSc2	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Akutové	akutový	k2eAgFnPc4d1	akutový
(	(	kIx(	(
<g/>
ostré	ostrý	k2eAgFnPc4d1	ostrá
<g/>
)	)	kIx)	)
souhlásky	souhláska	k1gFnPc4	souhláska
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
spektru	spektrum	k1gNnSc6	spektrum
převážně	převážně	k6eAd1	převážně
vysoké	vysoký	k2eAgFnSc2d1	vysoká
frekvence	frekvence	k1gFnSc2	frekvence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Délka	délka	k1gFnSc1	délka
===	===	k?	===
</s>
</p>
<p>
<s>
Délkou	délka	k1gFnSc7	délka
souhlásek	souhláska	k1gFnPc2	souhláska
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
jejich	jejich	k3xOp3gNnSc7	jejich
trvání	trvání	k1gNnSc4	trvání
<g/>
.	.	kIx.	.
</s>
<s>
Fonologické	fonologický	k2eAgNnSc1d1	fonologické
rozlišování	rozlišování	k1gNnSc1	rozlišování
délky	délka	k1gFnSc2	délka
souhlásek	souhláska	k1gFnPc2	souhláska
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
časté	častý	k2eAgNnSc1d1	časté
jako	jako	k8xC	jako
u	u	k7c2	u
samohlásek	samohláska	k1gFnPc2	samohláska
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
i	i	k9	i
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
souhlásky	souhláska	k1gFnPc1	souhláska
nejsou	být	k5eNaImIp3nP	být
artikulačně	artikulačně	k6eAd1	artikulačně
jednotnou	jednotný	k2eAgFnSc7d1	jednotná
třídou	třída	k1gFnSc7	třída
segmentů	segment	k1gInPc2	segment
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
trvání	trvání	k1gNnSc2	trvání
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevovat	projevovat	k5eAaImF	projevovat
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
u	u	k7c2	u
exploziv	explozit	k5eAaPmDgInS	explozit
a	a	k8xC	a
afrikát	afrikáta	k1gFnPc2	afrikáta
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
prodloužit	prodloužit	k5eAaPmF	prodloužit
doba	doba	k1gFnSc1	doba
okluze	okluze	k1gFnSc2	okluze
(	(	kIx(	(
<g/>
závěru	závěr	k1gInSc2	závěr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
exploze	exploze	k1gFnSc1	exploze
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
podstaty	podstata	k1gFnSc2	podstata
jen	jen	k6eAd1	jen
krátkodobým	krátkodobý	k2eAgInSc7d1	krátkodobý
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Omezená	omezený	k2eAgFnSc1d1	omezená
možnost	možnost	k1gFnSc1	možnost
trvání	trvání	k1gNnSc2	trvání
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
u	u	k7c2	u
verberant	verberant	k1gMnSc1	verberant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
délka	délka	k1gFnSc1	délka
souhlásek	souhláska	k1gFnPc2	souhláska
má	mít	k5eAaImIp3nS	mít
fonologickou	fonologický	k2eAgFnSc4d1	fonologická
rozlišovací	rozlišovací	k2eAgFnSc4d1	rozlišovací
hodnotu	hodnota	k1gFnSc4	hodnota
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
nebo	nebo	k8xC	nebo
finština	finština	k1gFnSc1	finština
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
finské	finský	k2eAgFnSc6d1	finská
tuli	tul	k1gFnSc6	tul
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
oheň	oheň	k1gInSc1	oheň
<g/>
)	)	kIx)	)
a	a	k8xC	a
tulli	tulle	k1gFnSc4	tulle
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
clo	clo	k1gNnSc1	clo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
estonštině	estonština	k1gFnSc6	estonština
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
popisovány	popisovat	k5eAaImNgInP	popisovat
3	[number]	k4	3
stupně	stupeň	k1gInSc2	stupeň
délky	délka	k1gFnSc2	délka
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
souhlásky	souhláska	k1gFnPc1	souhláska
se	se	k3xPyFc4	se
také	také	k9	také
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
souhlásek	souhláska	k1gFnPc2	souhláska
je	být	k5eAaImIp3nS	být
též	též	k9	též
často	často	k6eAd1	často
dávána	dávat	k5eAaImNgFnS	dávat
do	do	k7c2	do
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
geminací	geminace	k1gFnSc7	geminace
<g/>
,	,	kIx,	,
zdvojováním	zdvojování	k1gNnSc7	zdvojování
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc1	přehled
souhlásek	souhláska	k1gFnPc2	souhláska
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Duběda	Duběda	k1gFnSc1	Duběda
T.	T.	kA	T.
Jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Univerzálie	univerzálie	k1gFnPc1	univerzálie
a	a	k8xC	a
typologie	typologie	k1gFnPc1	typologie
ve	v	k7c6	v
fonetice	fonetika	k1gFnSc6	fonetika
a	a	k8xC	a
fonologii	fonologie	k1gFnSc6	fonologie
<g/>
.	.	kIx.	.
</s>
<s>
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1073	[number]	k4	1073
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krčmová	krčmový	k2eAgFnSc1d1	Krčmová
M.	M.	kA	M.
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
fonetiky	fonetika	k1gFnSc2	fonetika
a	a	k8xC	a
fonologie	fonologie	k1gFnSc2	fonologie
pro	pro	k7c4	pro
bohemisty	bohemista	k1gMnPc4	bohemista
<g/>
.	.	kIx.	.
</s>
<s>
FF	ff	kA	ff
OU	ou	k0	ou
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7368	[number]	k4	7368
<g/>
-	-	kIx~	-
<g/>
213	[number]	k4	213
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
souhláska	souhláska	k1gFnSc1	souhláska
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
