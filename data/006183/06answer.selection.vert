<s>
Původní	původní	k2eAgNnSc1d1	původní
latinské	latinský	k2eAgNnSc1d1	latinské
jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
Lutetia	Lutetia	k1gFnSc1	Lutetia
[	[	kIx(	[
<g/>
lutecia	lutecium	k1gNnSc2	lutecium
<g/>
]	]	kIx)	]
nebo	nebo	k8xC	nebo
Lutetia	Lutetia	k1gFnSc1	Lutetia
Parisiorum	Parisiorum	k1gInSc1	Parisiorum
(	(	kIx(	(
<g/>
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
Lutè	Lutè	k1gFnSc2	Lutè
[	[	kIx(	[
<g/>
lytɛ	lytɛ	k?	lytɛ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
Lutécie	Lutécie	k1gFnSc2	Lutécie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
později	pozdě	k6eAd2	pozdě
ustoupilo	ustoupit	k5eAaPmAgNnS	ustoupit
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
jména	jméno	k1gNnSc2	jméno
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
.	.	kIx.	.
</s>
